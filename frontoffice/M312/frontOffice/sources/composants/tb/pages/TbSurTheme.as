package composants.tb.pages
{
	import mx.resources.ResourceManager;
	import composants.tb.graph.GraphFactory;
	import composants.tb.graph.GraphParameters;
	import composants.tb.graph.IGraphModel;
	import composants.tb.tableaux.ITableauModel;
	import composants.tb.tableaux.TableauAboFactoryStratey;
	import composants.tb.tableaux.TableauChangeEvent;
	import composants.tb.tableaux.TableauConsoFactoryStrategy;
	import composants.tb.tableaux.TableauFactory;
	
	import flash.display.DisplayObject;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	
	
	public class TbSurTheme extends TbSurTheme_IHM implements IPage
	{
		private var _tf : TableauFactory = new TableauFactory();
		
		private const _pageType : String = "SUR-THEME";
		
		private var _gf : GraphFactory = new GraphFactory();   
		 
		[Bindable]
		private var _tb : ITableauModel;
		
		[Bindable]
		private var _g : IGraphModel;
		
		private var _nextPageType : String; 
		
		private var _pageLibelle : String;
		
		private var _mode : String;
		private var _idmode : int;
		private var _goToNextPage : Function ;
		 
		private var _surTheme : String;
		
		private var _idSurTheme : Number = 0;
		
		private var _type : TableauChangeEvent;
		
		private var _identifiant : String;
		
		private var lastMoisFin : String = "x";
		
		private var lastMoisDebut : String = "x";		
		 
		private var moisDeb : String;
		 
		private var moisFin : String;
		
		private var perimetre : String;		
		
		private var lastIdentifiant : String = "z";
		
		private var identifiant : String;
		
		
		private var modeSelection : String;
		
		private var _total : Number;
		
		[Bindable]protected var dataproviders : ArrayCollection;
		
		/**
		* Constructeur 
		**/
		public function TbSurTheme(type : TableauChangeEvent = null)
		{
			super();
			_pageLibelle = type.LIBELLE;
			_type = type;
			_type = type;
			_mode = type.TYPE_THEME;
			_surTheme = type.SUR_THEME;
			_idSurTheme = type.IDSUR_THEME;
			_identifiant = type.IDENTIFIANT;
			_idmode = checkThemid(_mode);				
			addEventListener(FlexEvent.CREATION_COMPLETE,init);					
		}

		private function checkThemid(theme:String):int
		{
			var _idsegment		:int	= -1;
			var them			:String = theme.toUpperCase();
			var THEME_ABO		:String = ResourceManager.getInstance().getString('M312', 'Abonnements').toUpperCase();
			var THEME_CON		:String = ResourceManager.getInstance().getString('M312', 'Consommations').toUpperCase();
			
			switch(them)
			{
				case THEME_ABO 	:  _idsegment = 1;break;
				case THEME_CON:  _idsegment = 2;break;
			}
			
			return _idsegment; 
		}
				
		/**
		 * Retourne le type de la page la suit
		 * @return type le type de la page suivante
		 * */		
		public function get nextPageType():String{
			//TODO: implement function
			return _nextPageType;
		}
		
		/**
		 * Retourne le type de la page la suit
		 * @return type le type de la page suivante
		 * */		
		public function set nextPageType(type : String):void{
			_nextPageType = type;
		}
						
		/**
		* Reset le compossant, remet toutes les variables a 0, interromp les remotings en cours
		**/
		public function clean():void
		{
			//TODO: implement function
		}
		
		public function get pageType():String{
			return _pageType;
		}
		/**
		 * Permet d'exporter un rapport flash,pdf ou excel 
		 * @param format Le format (flashpaper, excel ou pdf)
		 * */
		public function exporterRapport( format: String, rSociale : String = ""):void{	
		 	var urlback : String = moduleDashbordPilotageIHM.NonSecureUrlBackoffice+ "/fr/consotel/consoview/M312/export/TableauDeBord-SurTheme.cfm";	
            var variables : URLVariables = new URLVariables();
            
            variables.format = format;
            variables.tb = _type.SEGMENT;
            variables.datedeb = moisDeb; 
            variables.datefin = moisFin;
            variables.raisonsociale = rSociale;
            variables.perimetre=this.perimetre;
            variables.identifiant=this.identifiant;
            variables.modeSelection=this.modeSelection;
            variables.SUR_THEME=this._surTheme;
            variables.IDSUR_THEME=this._idSurTheme;
            
            
            var request:URLRequest = new URLRequest(urlback);
            request.data = variables;
            request.method=URLRequestMethod.POST;
            navigateToURL(request,"_blank");
		}
		
		public function getLibelle():String
		{
			//TODO: implement function
			return _pageLibelle;
		}
		
		public function getTotal():Number
		{
			//TODO: implement function
			return _total;
		}			
					
		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...) 			
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		public function updatePerimetre(perimetre : String = null, modeSelection : String = null, idt : String= null):void
		{
			this.perimetre = perimetre;
			this.modeSelection = modeSelection;	
			this.identifiant = idt;
			
			_tb.updatePerimetre(perimetre,modeSelection);
			_g.updatePerimetre(perimetre,modeSelection,idt);			
		}
		
		public function updateSegment(segment : String = ""):void
		{
			 
			
		}
		
		/**
		 * Met à jour la periode
		 * */		 
		public function updatePeriode(moisDeb : String = null, moisFin : String = null):void{			 
			//TOTO: implement function
			this.moisDeb = moisDeb;
			this.moisFin = moisFin;			
			_tb.updatePeriode(moisDeb,moisFin);
			_g.updatePeriode(moisDeb,moisFin);
		}		
						
		public function update():void{
			if (lastMoisDebut.concat(lastMoisFin).concat(lastIdentifiant) != moisDeb.concat(moisFin).concat(identifiant)){

							
				chargerDonnees();			
									
				lastMoisDebut = moisDeb;
				lastMoisFin = moisFin;
				lastIdentifiant = identifiant;
			}		
		}
		
		//initialisation de l'IHM
		private function init(fe:FlexEvent):void{
			
			setTabFactoryStrategy();					
			
			
			_tb = _tf.createTableau(_type);
						
			//graph	
			var graphP : GraphParameters = new GraphParameters();
			graphP.TYPE_GRAPH = "COUTPARTHEME";
			graphP.SUR_THEME = _surTheme;			
			  			
			_g = _gf.createGraph(graphP);	
		
				
			//Affichage des elements	
			myTabContener.addChild(DisplayObject(_tb));		
			myTabContener.addChild(DisplayObject(_g));			
			
			
			//affectation des Handlers et des listeners			
			UIComponent(_tb).addEventListener("tableauChange",_goToNextPage);
			_tb.changeHandlerFunction = _goToNextPage; 
				
		}
		
		private function setTabFactoryStrategy():void
		{
			switch(_idmode)
			{
				case 1 : _tf.setStrategy(new TableauAboFactoryStratey());break;
				case 2 : _tf.setStrategy(new TableauConsoFactoryStrategy());break;
				default : _tf.setStrategy(new TableauAboFactoryStratey());break;
			}
			
		}
			
		public function goToNextPageHandler(f : Function):void
		{
			_goToNextPage = f;						
		}

		///Rajout Optimisation -------------------------	une seule requête pour tableaux et graphs	
		protected function chargerDonnees():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M312.surtheme.FacadeSurTheme",
																				"getListeThemesSurThemeByID",
																				chargerDonneesResultHandler,
																				null);			
			RemoteObjectUtil.callService(op,perimetre,
											modeSelection,
											identifiant,
											_idSurTheme,
											moisDeb,
											moisFin);
		}
		 
		protected function chargerDonneesResultHandler(re :ResultEvent):void
		{
		 	try
		 	{
		  		dataproviders =  re.result as ArrayCollection;
		  		dataproviders.refresh();
		  						
				_tb.dataProviders = dataproviders;			
				_g.dataProviders = dataproviders;
		  	}
		  	catch(er : Error)
		  	{		  	
		  		trace(er.message,ResourceManager.getInstance().getString('M312', 'R_partition_par_Th_mes_graph_couttheme'));
		  	}
		 }
	}
}