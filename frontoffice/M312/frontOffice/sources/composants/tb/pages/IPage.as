package composants.tb.pages
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	
	import mx.core.UIComponent;
	
	public interface IPage extends IEventDispatcher{
				 
		function clean():void;
		function getLibelle():String;
		function getTotal():Number;					
		function goToNextPageHandler(f : Function):void;

		function set nextPageType(t : String):void;		
		function get pageType():String;
				
		function update():void; 
		function updatePeriode(moisDeb : String = null, moisFin : String = null):void;
		function updateSegment(segment : String = ""):void;
		function updatePerimetre(perimetre : String = null, mode : String = null, identifiant : String= null):void;
		function exporterRapport(format: String, rSociale : String = ""):void;	 
	}
} 