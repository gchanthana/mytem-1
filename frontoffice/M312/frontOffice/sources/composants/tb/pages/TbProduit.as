package composants.tb.pages
{
    import mx.resources.ResourceManager;
    import composants.tb.tableaux.TableauChangeEvent;
    import mx.events.FlexEvent;
    import flash.display.DisplayObject;
    import mx.core.UIComponent;
    import composants.tb.tableaux.TableauAboFactoryStratey;
    import composants.tb.tableaux.TableauConsoFactoryStrategy;
    import composants.tb.graph.IGraphModel;
    import composants.tb.tableaux.ITableauModel;
    import composants.tb.tableaux.TableauFactory;
    import mx.containers.Grid;
    import mx.controls.DataGrid;
    import mx.events.DataGridEvent;
    import flash.net.URLVariables;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;
    import flash.events.Event;
    import flash.net.URLRequestMethod;
    import mx.utils.ObjectUtil;
    import mx.controls.Alert;

    public class TbProduit extends TbProduit_IHM implements IPage
    {
        private var _tf:TableauFactory = new TableauFactory();
        private const _pageType:String = "PRODUIT";
        private var _tb:ITableauModel;
        private var lastMoisFin:String = "x";
        private var lastMoisDebut:String = "x";
        private var moisDeb:String;
        private var moisFin:String;
        private var perimetre:String;
        private var modeSelection:String;
        private var _nextPageType:String;
        private var _pageLibelle:String;
        private var _mode:String;
        private var _idmode:int;
        private var _goToNextPage:Function;
        private var lastIdentifiant:String = "z";
        private var identifiant:String;
        private var _type:TableauChangeEvent;
        private var _total:Number;

        /**
         * Constructeur
         **/
        public function TbProduit(type:TableauChangeEvent = null)
        {
            super();
            _type = type;
            _mode = type.TYPE_THEME;
            _idmode = checkThemid(_mode);
            _pageLibelle = ResourceManager.getInstance().getString('M312', 'Produit___') + type.LIBELLE;
            addEventListener(FlexEvent.CREATION_COMPLETE, init);
        }

        private function checkThemid(type_theme:String):int
        {
            var _idtype_theme:int = -1;
            var _type_theme:String = type_theme.toUpperCase();
            var THEME_ABO:String = ResourceManager.getInstance().getString('M312', 'Abonnements').toUpperCase();
            var THEME_CON:String = ResourceManager.getInstance().getString('M312', 'Consommations').toUpperCase();
            switch (_type_theme)
            {
                case THEME_ABO:
					_idtype_theme = 1;
                    break;
                case THEME_CON:
					_idtype_theme = 2;
                    break;
            }
            return _idtype_theme;
        }

        /**
         * Retourne le type de la page la suit
         * @return type le type de la page suivante
         * */
        public function get nextPageType():String
        {
            //TODO: implement function
            return _nextPageType;
        }

        /**
         * Retourne le type de la page la suit
         * @return type le type de la page suivante
         * */
        public function set nextPageType(type:String):void
        {
            _nextPageType = type;
        }

        public function get pageType():String
        {
            return _pageType;
        }

        public function clean():void
        {
            //TODO: implement function
        }

        public function getLibelle():String
        {
            //TODO: implement function
            return ResourceManager.getInstance().getString('M312', 'D_tail_du_produit');
        }

        public function getTotal():Number
        {
            //TODO: implement function
            return _total;
        }

        public function updateSegment(segment:String = ""):void
        {
        }

        public function goToNextPageHandler(f:Function):void
        {
            _goToNextPage = f;
        }

        public function update():void
        {
            if (lastMoisDebut.concat(lastMoisFin).concat(lastIdentifiant) != moisDeb.concat(moisFin).concat(identifiant))
            {
                _tb.update();
                lastMoisDebut = moisDeb;
                lastMoisFin = moisFin;
                lastIdentifiant = identifiant;
            }
        }

        /**
         * Met à jour la periode
         * */
        public function updatePeriode(moisDeb:String = null, moisFin:String = null):void
        {
            //TOTO: implement function
            this.moisDeb = moisDeb;
            this.moisFin = moisFin;
            _tb.updatePeriode(moisDeb, moisFin);
        }

        /**
         * Permet d'exporter un rapport flash,pdf ou excel
         * @param format Le format (flashpaper, excel ou pdf)
         * */
        public function exporterRapport(format:String, rSociale:String = ""):void
        {
            var template:String;
            var variables:URLVariables = new URLVariables();
            
            template = "/fr/consotel/consoview/M312/pdf/produit/print_document_cat.cfm";
            variables.numero = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;            
            var urlback:String = moduleDashbordPilotageIHM.NonSecureUrlBackoffice + template;
            variables.perimetre = perimetre;
            variables.raisonsociale = rSociale;
            variables.idproduit_client = _type.IDPRODUIT;
            variables.format = format;
			variables.PATH_PRDT = 'Segment : '+_type.SEGMENT +' > '+ _type.THEME + ' > '+_pageLibelle ;
            variables.tb = _type.SEGMENT;
            variables.datedeb = moisDeb;
            variables.datefin = moisFin;
			variables.idtypeTheme = _idmode;
			variables.typeTheme = _type.TYPE_THEME;
            variables.modeCalcul = modeSelection;
            variables.mode = _mode.toUpperCase();
            var request:URLRequest = new URLRequest(urlback);
            request.data = variables;
            request.method = URLRequestMethod.POST;
            navigateToURL(request, "_blank");
        }

        /**
         * Met à jour le perimtre et le modeSelection
         * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...)
         * @param modeSelection Le mode de selection(complet ou partiel)
         * */
        public function updatePerimetre(perimetre:String = null, modeSelection:String = null, idt:String = null):void
        {
            this.perimetre = perimetre;
            this.modeSelection = modeSelection;
            this.identifiant = idt
            _tb.updatePerimetre(perimetre, modeSelection);
        }

        private function setTabFactoryStrategy():void
        {
            //TODO: implement function
            switch (_idmode)
            {
                case 1:
                    _tf.setStrategy(new TableauAboFactoryStratey());
                    break;
                case 2:
                    _tf.setStrategy(new TableauConsoFactoryStrategy());
                    break;
                default:
                    _tf.setStrategy(new TableauAboFactoryStratey());
                    break;
            }
        }

        //initialisation de l'IHM
        private function init(fe:FlexEvent):void
        {
            setTabFactoryStrategy();
            _tb = _tf.createTableau(_type);
            //Affichage des elements		
            myTabContener.addChild(DisplayObject(_tb));
            //affectation des Handlers et des listeners			
            UIComponent(_tb).addEventListener("tableauChange", _goToNextPage);
        }
    }
}