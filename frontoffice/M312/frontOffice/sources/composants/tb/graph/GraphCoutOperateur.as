package composants.tb.graph
{
	import composants.util.ConsoviewFormatter;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	public class GraphCoutOperateur extends PieGraphBase
	{
		private var op : AbstractOperation;
		
		[Bindable]
		private var _dataProviders : ArrayCollection;
		
		[Bindable]
		private var _dataProviders2 : ArrayCollection;
					
		private var _segment 	:String;  
		 
		/**
		 * Constructeur
		 * */
		public function GraphCoutOperateur(segment : String = "Complet")
		{
			
			//TODO: implement function
			super();
			setSegment(segment);
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
				
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		override public function clean():void{		
			graph.myPie.dataProvider = null;
			myGrid.dataProvider = null;
			
		}	
				
		/**
		* Permet de passer les valeurs aux tableaux du composant.		
		* @param d Une collection.
		**/						
		override public function set dataProviders(d : ArrayCollection):void{
			
			_dataProviders = _formatterDataProvider(d);
			_dataProviders2 = _formatterDataProviderPie(d);
			updateProviders();
			
		}
		
		/**
		*  Retourne le dataproviders
		**/	
		[Bindable]			
		override public function get dataProviders():ArrayCollection{
			return _dataProviders;
		}	
		
		/**
		 * Met à jour les donées du tableau
		 **/
		override public function update():void{			
			if (_segment.toUpperCase()=="COMPLET") {
				chargerDonnees(); 	
			} else {
				chargerDonneesBySegment(); 	
			}			 
		}	
				
		/*---------- PRIVATE --------------------*/
		protected override function _formatterDataProvider(d: ArrayCollection):ArrayCollection{		
			var tmpCollection : ArrayCollection = new ArrayCollection();	
			var ligne : Object;				
			
			_total = 0;
			for	(ligne in d)
			{		
				tmpCollection.addItem(formateObject(d[ligne]));					   
			}
			
			var totalObject : Object = new Object();
			totalObject[ResourceManager.getInstance().getString('M312', 'Libell_')] = "TOTAL";
			totalObject[ResourceManager.getInstance().getString('M312', 'Montant')] = ConsoviewFormatter.formatEuroCurrency(_total,2);
			tmpCollection.addItem(totalObject);	
			return tmpCollection;
		}	
		
		protected function _formatterDataProviderPie(d: ArrayCollection):ArrayCollection{		
			
			var tmpCollection : ArrayCollection = new ArrayCollection();	
			var ligne : Object;				
			var len : int = d.length;
			var obj : Object = new Object();
			var chartData : ArrayCollection = new ArrayCollection();
			
						
			_total = 0;
			
			//////////////////formatage des données
			for	(ligne in d){		
				tmpCollection.addItem(formateObjectPie(d[ligne]));					   
			}
			/////////////////
			chartData = ObjectUtil.copy(tmpCollection) as ArrayCollection;
			
			/////////////////On coupe s'il y a trop de données
			if (len > 10){
				var sum : Number  = 0;
			
				for (var i : int = 9 ; i < len; i++){
					obj = chartData.getItemAt(i);
					sum = sum + parseFloat(obj[ResourceManager.getInstance().getString('M312', 'Montant')]);					
				}
				
				var len2 : int =  chartData.length ;
				
				for (var j : int = 10; j < len2 ; j++){
					chartData.removeItemAt(10);
				} 	
										
				chartData.getItemAt(9)[ResourceManager.getInstance().getString('M312', 'Libell_')] = ResourceManager.getInstance().getString('M312', 'Autres___');
				chartData.getItemAt(9)[ResourceManager.getInstance().getString('M312', 'Montant')] = sum;					
			 
				 return chartData;
			}

			////////////////// si la somme des montant est null on retourne rien
			if(_total == 0)
				return null;
			
			////////
			return tmpCollection;
		}	
			
		
		protected function formateObject(obj : Object):Object
		{
			var o : Object = new Object();
				
			_total = _total + parseFloat(obj.montant_final);		
			
			
			switch(int(obj.idtype_theme))
			{
				case 1 : o[ResourceManager.getInstance().getString('M312', 'Libell_')] = obj.nom + " " + String(obj.type_theme).substring(0,3) + ".";break;	
				case 2 : o[ResourceManager.getInstance().getString('M312', 'Libell_')] = obj.nom + " " + String(obj.type_theme).substring(0,5) + ".";break;
			}
			
			o[ResourceManager.getInstance().getString('M312', 'Type_de_produit')] = obj.type_theme;
			o[ResourceManager.getInstance().getString('M312', 'Op_rateur')] = obj.nom;
			o[ResourceManager.getInstance().getString('M312', 'Montant')]= ConsoviewFormatter.formatEuroCurrency(obj.montant_final,2);											
			return o;
		}	
		
		protected function formateObjectPie(obj : Object):Object
		{
			var o : Object = new Object();
				
			_total = _total + parseFloat(obj.montant_final);
								
		
			switch(int(obj.idtype_theme))
			{
				case 1 : o[ResourceManager.getInstance().getString('M312', 'Libell_')] = obj.nom + " " + String(obj.type_theme).substring(0,3) + ".";break;	
				case 2 : o[ResourceManager.getInstance().getString('M312', 'Libell_')] = obj.nom + " " + String(obj.type_theme).substring(0,5) + ".";break;
			}
			
			o[ResourceManager.getInstance().getString('M312', 'Type_de_produit')] = obj.type_theme;
			o[ResourceManager.getInstance().getString('M312', 'Op_rateur')] = obj.nom;			
			o[ResourceManager.getInstance().getString('M312', 'Montant')]= Math.abs(parseFloat(obj.montant_final));							
			return o;
		}	
		
		
		private function init(fe : FlexEvent):void{
			title = ResourceManager.getInstance().getString('M312', 'R_partition_des_co_ts');	
		}
				
		private function updateProviders():void{
			myGrid.dataProvider = dataProviders;	
			DataGridColumn(myGrid.columns[1]).setStyle("textAlign","right"); 		
			graph.callLater(updatePie);
		}
		
		private function updatePie():void{
			graph.myPie.dataProvider = _dataProviders2;
		}
		
		private function setSegment(segment : String):void{
			if (segment == "null")
				_segment = "Complet";
			else
				_segment = segment;
		};
		
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
	
		// Chargement des données par remoting
		protected override function chargerDonnees():void
		{
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.M312.accueil.facade",
																			"getRepartOperateur",
																			chargerDonneesResultHandler,
																			null);
			RemoteObjectUtil.callService(op);
			
		}
			
		// Chargement des données par remoting
		protected function chargerDonneesBySegment():void
		{
			var _idsegment :int = checkSegmentid();
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.M312.accueil.facade",
																			"getRepartOperateurBySegment",
																			chargerDonneesResultHandler,
																			null);
			RemoteObjectUtil.callService(op,_segment, /* ICI */
											_idsegment);
		}
		
		private function checkSegmentid():int
		{
			var _idsegment		:int	= -1;
			var sgmtThem		:String = _segment.toUpperCase();
			var SGMT_FIXE		:String = ResourceManager.getInstance().getString('M312', 'SGMT_FIXE').toUpperCase();
			var SGMT_MOBILE		:String = ResourceManager.getInstance().getString('M312', 'SGMT_MOBILE').toUpperCase();
			var SGMT_DATA		:String = ResourceManager.getInstance().getString('M312', 'SGMT_DATA').toUpperCase();
			
			switch(sgmtThem)
			{
				case SGMT_FIXE 	:  _idsegment = 1;break;
				case SGMT_MOBILE:  _idsegment = 2;break;
				case SGMT_DATA 	:  _idsegment = 3;break;										
			}
			
			return _idsegment; 
		}	
		
		protected override function chargerDonneesResultHandler(re :ResultEvent):void
		{			 		 		 
		  	dataProviders =  re.result as ArrayCollection;			  		
		} 	
		 				
	}
}