package composants.tb.recherche
{
	import mx.containers.TitleWindow;
	import mx.resources.ResourceManager;

	public class PanelRechercheFactory
	{
		public static function createPanelRecherche(type : String, chaine :  String, datedebut : String , datefin : String):TitleWindow{
			switch(type.toUpperCase()){
				case "GROUPE" : return new ResultatRechercheGroupeRacine(chaine,datedebut,datefin);break;	
				case "GROUPELIGNE" : return new ResultatRechercheGroupeLigne(chaine,datedebut,datefin);break;
				case "GROUPEANA" : return new ResultatRechercheGroupeAnalitique(chaine,datedebut,datefin);break;				
				default : throw new PanelNotFoundError(ResourceManager.getInstance().getString('M312', 'Impossible_de_cr_er_le_panel_') + type);break; 
			}
			return null;			
		}
	}
}