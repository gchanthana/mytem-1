package composants.tb.tableaux.theme
{
	import composants.tb.tableaux.services.ThemeServices;
	import composants.util.ConsoviewFormatter;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.utils.ObjectUtil;
	
	public class ThemeFixe
	{
		
		public var themeFixeAbo		:ArrayCollection = new ArrayCollection();
		public var themeFixeConso	:ArrayCollection = new ArrayCollection();
				
		private var _thmServices	:ThemeServices;
				
		public function ThemeFixe()
		{
			themeLoaded(CvAccessManager.getSession().INFOS_DIVERS.THEME);
		}
		
		private function themeLoaded(theme:ArrayCollection):void
		{			
			themeFixeAbo	= getThemeFixeAbo(theme);
			themeFixeConso	= getThemeFixeConso(theme);
		}
		
		public function getThemeFixeAbo(values:ArrayCollection):ArrayCollection
		{
			var abo	:ArrayCollection = new ArrayCollection()
			var thm	:Object;
			var len	:int = values.length;
			
			for(var i:int = 0; i < len;i++)
			{
				if(values[i].IDSEGMENT_THEME == 1 && values[i].IDTYPE_THEME == 1)
				{
					thm = new Object();
					thm.LIBELLE 			= values[i].LIBELLE_THEME_PRODUIT;
					thm.SEGMENT 			= values[i].LIBELLE_SEGMENT_THEME;
					thm.TYPE 				= "THEME";
					thm.ORDRE_AFFICHAGE		= values[i].ORDRE_AFFICHAGE;
					thm.TYPE_THEME			= values[i].LIBELLE_TYPE_THEME; 		//--ResourceManager.getInstance().getString('M312', 'Consommations');
					thm.LIBELLE_SURTHEME 	= values[i].LIBELLE_SUR_THEME;
					thm.IDSEGMENT_THEME 	= values[i].IDSEGMENT_THEME;
					thm.IDTHEME_PRODUIT 	= values[i].IDTHEME_PRODUIT; 
					thm.IDTYPE_THEME 		= values[i].IDTYPE_THEME;
					thm.ID_LIBELLE 			= values[i].IDSUR_THEME;
					thm.QUANTITE			= "0";
					thm.NBAPPELS			= "0";
					thm.MONTANT_TOTAL		= ConsoviewFormatter.formatNumber(0,2);
					
					abo.addItem(thm);
				}
			}
			
			return sortFunction(abo, "ORDRE_AFFICHAGE");
		}
		
		public function getThemeFixeConso(values:ArrayCollection):ArrayCollection
		{
			var conso	:ArrayCollection = new ArrayCollection()
			var thm		:Object;
			var len		:int = values.length;
			
			for(var i:int = 0; i < len;i++)
			{
				if(values[i].IDSEGMENT_THEME == 1 && values[i].IDTYPE_THEME == 2)
				{
					thm = new Object();
					thm.LIBELLE 			= values[i].LIBELLE_THEME_PRODUIT;
					thm.SEGMENT 			= values[i].LIBELLE_SEGMENT_THEME;
					thm.TYPE 				= "THEME";
					thm.ORDRE_AFFICHAGE		= values[i].ORDRE_AFFICHAGE;
					thm.TYPE_THEME			= values[i].LIBELLE_TYPE_THEME; 		//--ResourceManager.getInstance().getString('M312', 'Consommations');
					thm.LIBELLE_SURTHEME 	= values[i].LIBELLE_SUR_THEME;
					thm.IDSEGMENT_THEME 	= values[i].IDSEGMENT_THEME;
					thm.IDTHEME_PRODUIT 	= values[i].IDTHEME_PRODUIT; 
					thm.IDTYPE_THEME 		= values[i].IDTYPE_THEME;
					thm.ID_LIBELLE 			= values[i].IDSUR_THEME;
					thm.QUANTITE			= "0";
					thm.NBAPPELS			= "0";
					thm.MONTANT_TOTAL		= ConsoviewFormatter.formatNumber(0,2);
					
					conso.addItem(thm);
				}
			}
			
			return sortFunction(conso, "ORDRE_AFFICHAGE");
		}
		
		private function sortFunction(value:ArrayCollection, label:String):ArrayCollection
	    {
	    	var dg			:DataGrid 		= new DataGrid();
	    	var dgColumn	:DataGridColumn = new DataGridColumn(label);
			
			dg.dataProvider = value;
	    	dg.columns = [dgColumn];
	    	dgColumn.sortCompareFunction = sortFunctionNumeric;
			
			value.refresh();
			
			return value;
	    }
	    
		private function sortFunctionNumeric(obj1:Object, obj2:Object):int
		{
		   var value1:Number = (obj1.ORDRE_AFFICHAGE == '' || obj1.ORDRE_AFFICHAGE == null) ? null : new Number(obj1.ORDRE_AFFICHAGE);
		   var value2:Number = (obj2.ORDRE_AFFICHAGE == '' || obj2.ORDRE_AFFICHAGE == null) ? null : new Number(obj2.ORDRE_AFFICHAGE);
		   
		   return ObjectUtil.numericCompare( value1, value2 );
		}

	}
}