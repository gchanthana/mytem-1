package composants.tb.tableaux
{
    import composants.tb.tableaux.info.InfosPanelView;
    import composants.util.ConsoviewAlert;
    import composants.util.ConsoviewFormatter;
    import flash.events.ContextMenuEvent;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;
    import flash.net.navigateToURL;
    import flash.ui.ContextMenu;
    import flash.ui.ContextMenuItem;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.controls.TextInput;
    import mx.controls.dataGridClasses.DataGridColumn;
    import mx.events.DataGridEvent;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.resources.ResourceManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.utils.ObjectUtil;
    import paginatedatagrid.PaginateDatagrid;
    import paginatedatagrid.event.PaginateDatagridEvent;
    import paginatedatagrid.pagination.event.PaginationEvent;
    import paginatedatagrid.pagination.vo.ItemIntervalleVO;
    import searchpaginatedatagrid.ParametresRechercheVO;
    import searchpaginatedatagrid.SearchPaginateDatagrid;
    import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;
    import univers.facturation.tb.TableauDeBord;

    public class TableauProduitConso extends TableauProduitConso_IHM implements ITableauModel
    {
        private var myContextMenu:ContextMenu;
        private var displayEvent:DisplayFacureEvent = new DisplayFacureEvent("displayFacture");
        private var _dataProviders:ArrayCollection;
        [Bindable]
        private var _changeHandlerFunction:Function;
        private var op:AbstractOperation;
        private var opEdition:AbstractOperation;
        private var _totalNbAppels:int = 0;
        private var _totalVol:int = 0;
        private var _total:Number = 0;
        private var _idproduit:int;
        private var _title:String;
        private var moisDeb:String;
        private var moisFin:String;
        private var perimetre:String;
        private var modeSelection:String;
        private var identifiant:String;
        private var segment:String;
        private var firstloading:Boolean;

        /**
         * Constructeur
         **/
        public function TableauProduitConso(ev:TableauChangeEvent = null)
        {
            _idproduit = parseInt(ev.IDPRODUIT);
            _title = ev.LIBELLE + " : " + ev.OPERATEUR;
            identifiant = ev.IDENTIFIANT;
            segment = ev.SEGMENT;
            addEventListener(FlexEvent.CREATION_COMPLETE, init);
            firstloading = true;
        }

        /**
         * Retourne le montant total
         * */
        public function get total():Number
        {
            return _total;
        }

        /**
         * Permet de passer les valeurs aux tableaux du composant.
         * <p>Pour chaque tableau de la collection, un DataGrid sera affiché à la suite du précédant</p>
         * @param d Une collection de tableau.
         **/
        public function set dataProviders(d:ArrayCollection):void
        {
            drawTabs();
        }

        /**
         *  Retourne le dataproviders
         **/
        [Bindable]
        public function get dataProviders():ArrayCollection
        {
            return _dataProviders;
        }

        /**
         * Permet d'affecter une fonction pour traiter les Clicks sur Les DataGrids du composant
         * @param changeFunction La fonction qui traite les clicks.<p> Elle prend un paramètre de type Object qui représente une ligne du DataGrid</p>
         **/
        public function set changeHandlerFunction(changeFunction:Function):void
        {
            _changeHandlerFunction = changeFunction;
        }

        /**
         *  Retourne une reference vers la fonction qui traite le data provider
         **/
        public function get changeHandlerFunction():Function
        {
            return _changeHandlerFunction;
        }
		
		
		/**
		 * Met à jour les donées du tableau
		 **/
		public function update():void
		{			
			resetPaginate();
			updatePaginate();
		}
		
		
		// reinitialiser l'interval de recherche
		// reinitialiser les totaux
		private function resetPaginate():void
		{
			myGrid.refreshPaginateDatagrid(true);
			if(collecDetailProduit) collecDetailProduit.removeAll();
			if(myGrid) myGrid.currentIntervalle = null;
			if(myFooterGrid)
			{
				myFooterGrid.columns[1].headerText = ConsoviewFormatter.formatNumber(0, 0);
				myFooterGrid.columns[2].headerText = ConsoviewFormatter.formatNumber(0, 0);
				myFooterGrid.columns[3].headerText = ConsoviewFormatter.formatEuroCurrency(0, 2);
			}
		}
		

        public function onSearchPaginateHandler(evt:SearchPaginateDatagridEvent):void
        {
			resetPaginate();
			updatePaginate();
        }

        public function onItemSelectedHandler(evt:PaginateDatagridEvent):void
        {
        }

        public function onIntervalleChangeHandler(evt:PaginationEvent):void
        {
			updatePaginate();
        }

        /**
         * Met à jour les donées du tableau
         **/
        public function updatePaginate():void
        {
            // init des champs près des boutons infos
            labelFacture.text = "";
            labelLigne.text = "";
            var interalVO:ItemIntervalleVO = myGrid.currentIntervalle;
            //chargerDonnees();
            if (!interalVO) // Si c'est une nouvelle recherche
            {
                myGrid.refreshPaginateDatagrid(true);
                interalVO = new ItemIntervalleVO();
                interalVO.indexDepart = 1;
                interalVO.tailleIntervalle = 30;
            }
            chargerDonneesPaginate(mySearchPaginate.parametresRechercheVO, interalVO);
        }

        /**
         * Efface les données, interrompt les 'remotings'
         **/
        public function clean():void
        {
            //to do
        }

        /**
         * Met à jour la periode
         * */
        public function updatePeriode(moisDeb:String = null, moisFin:String = null):void
        {
            //TOTO: implement function
            this.moisDeb = moisDeb;
            this.moisFin = moisFin;
        }

        /**
         * Met à jour le perimtre et le modeSelection
         * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...)
         * @param modeSelection Le mode de selection(complet ou partiel)
         * */
        public function updatePerimetre(perimetre:String = null, modeSelection:String = null):void
        {
            this.perimetre = perimetre;
            this.modeSelection = modeSelection;
        }

        /*---------- PRIVATE -----------------------*/ //initialisation du composant
        private function init(fe:FlexEvent):void
        {
            title = _title;
            myContextMenu = new ContextMenu();
            removeDefaultItems();
            ///listener
            myGrid.addEventListener(PaginateDatagridEvent.ON_ITEM_SELECTED, changeHandler);
            myGrid.addEventListener(DataGridEvent.ITEM_EDIT_BEGINNING, checkEditionGrid);
            myGrid.addEventListener(DataGridEvent.ITEM_EDIT_END, enregistrerDonnee);
            myContextMenu.addEventListener(ContextMenuEvent.MENU_SELECT, menuSelectHandler);
            myBtAfficherFacture.addEventListener(MouseEvent.CLICK, btAfficherHandler);
            addEventListener("displayFacture", displayFacture);
            btAfficherInfos.addEventListener(MouseEvent.CLICK, btAfficherInfosClickHandler);
            DataGridColumn(myGrid.columns[2]).headerText = (segment.toLowerCase() == "mobile") ? ResourceManager.getInstance().getString('M312', 'Collaborateur') : ResourceManager.getInstance().getString('M312', 'Fonction');
            myBtAfficherFacture.enabled = false;
            btAfficherInfos.enabled = false;
            // Paramétrages du datagrid
            mySearchPaginate.addEventListener(SearchPaginateDatagridEvent.SEARCH, onSearchPaginateHandler);
            myGrid.addEventListener(PaginationEvent.INTERVALLE_SELECTED_CHANGE, onIntervalleChangeHandler);
            myGrid.dgPaginate.setStyle("rollOverColor", "#558ED5");
            myFooterGrid.setStyle("rollOverColor", "#558ED5");
        }

        //formatage du telepone dans le grid
        private function formatDataTip(item:Object, column:DataGridColumn):String
        {
            return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);
        }

        private function drawTabs():void
        {
            myGrid.dgPaginate.dataProvider = dataProviders;
            myGrid.columns[1].labelFunction = formatDataTip;
            myGrid.columns[2].editable = false;
            myFooterGrid.columns[1].headerText = ConsoviewFormatter.formatNumber(_totalNbAppels, 0);
            myFooterGrid.columns[2].headerText = ConsoviewFormatter.formatNumber(_totalVol, 0);
            myFooterGrid.columns[3].headerText = ConsoviewFormatter.formatEuroCurrency(_total, 2);
        }

        private function isPresent(s:String, a:Array):Boolean
        {
            var OK:Boolean = false;
            for (var i:int = 0; i < a.length; i++)
            {
                if (s == a[i].toString())
                    OK = true;
            }
            return OK;
        }

        protected function calculTotal(arr:Array, colname:String):Number
        {
            var total:Number = 0;
            for (var i:int = 0; i < arr.length; i++)
            {
                total = total + parseFloat(arr[i][colname]);
            }
            return total;
        }

        private function removeDefaultItems():void
        {
            myContextMenu.hideBuiltInItems();
            myContextMenu.builtInItems.print = false;
            //var defaultItems:ContextMenuBuiltInItems = myContextMenu.builtInItems;
            //defaultItems.print = true;
        }

        private function addCustomMenuItems():void
        {
            var item1:ContextMenuItem = new ContextMenuItem("");
            var item2:ContextMenuItem = new ContextMenuItem("");
            myContextMenu.customItems.push(item1);
            myContextMenu.customItems.push(item2);
            item1.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, menuItemFactureSelectHandler);
            item2.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, menuItemProprieteSelectHandler);
        }

        private function removeCustomMenuItems():void
        {
            while (myContextMenu.customItems.length != 0)
            {
                myContextMenu.customItems.pop();
            }
        }

        private function menuSelectHandler(event:ContextMenuEvent):void
        {
            try
            {
                if (String(myGrid.selectedItem.SITE).length == 0)
                { //var item:ContextMenuItem = new ContextMenuItem(ResourceManager.getInstance().getString('M312', 'ceci_est_en_plus'));
                    addCustomMenuItems();
                    displayEvent.indexPeriode = myGrid.selectedItem.IDINVENTAIRE_PERIODE;
                    displayEvent.affichable = true;
                    displayEvent.numeroFacture = myGrid.selectedItem.FACTURE;
                    event.currentTarget.customItems[0].caption = ResourceManager.getInstance().getString('M312', 'Afficher_facture_') + myGrid.selectedItem.NUMERO_FACTURE;
                    event.currentTarget.customItems[1].caption = ResourceManager.getInstance().getString('M312', 'Propri_t_s_de_la_ligne_n_') + myGrid.selectedItem.SOUS_TETE;
                }
                else
                {
                    removeCustomMenuItems();
                }
            }
            catch (e:Error)
            {
                Alert.show(ResourceManager.getInstance().getString('M312', 'Vous_devez_s_lectionner_une_facture'));
                removeCustomMenuItems();
            }
        }

        private function menuItemFactureSelectHandler(event:ContextMenuEvent):void
        {
            dispatchEvent(displayEvent);
        }

        private function menuItemProprieteSelectHandler(cme:ContextMenuEvent):void
        {
            showInfosWindow();
        }

        //affiche la facture
        private function displayFacture(event:Event):void
        {
            var url:String = moduleDashbordPilotageIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/cfm/factures/display_Facture.cfm";
            var variables:URLVariables = new URLVariables();
            variables.ID_FACTURE = myGrid.selectedItem.IDINVENTAIRE_PERIODE;
            variables.idx_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            variables.type_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method = URLRequestMethod.POST;
            navigateToURL(request, "_blank");
        }

        //met a jour le numero de facture du labelFacture  
        private function changeHandler(ev:PaginateDatagridEvent):void
        {
            trace("changeHandler");
            if (myGrid.selectedIndex != -1)
            {
                myBtAfficherFacture.enabled = true;
                btAfficherInfos.enabled = true;
                labelFacture.text = ResourceManager.getInstance().getString('M312', 'Facture_n__') + myGrid.selectedItem.NUMERO_FACTURE;
                labelLigne.text = ResourceManager.getInstance().getString('M312', 'Ligne_n__') + String(myGrid.selectedItem.SOUS_TETE);
                myGrid.contextMenu = myContextMenu;
            }
            else
            {
                myBtAfficherFacture.enabled = false;
                btAfficherInfos.enabled = false;
                labelFacture.text = "";
                labelLigne.text = "";
                myGrid.contextMenu = null;
            }
        }
        private var infosWindow:InfosPanelView;

        private function showInfosWindow():void
        {
            if (myGrid.selectedItem != null && myGrid.selectedItem.SOUS_TETE != null)
            {
                if (infosWindow != null)
                    infosWindow = null;
                infosWindow = new InfosPanelView()
                infosWindow.addEventListener("infosWindowClosed", infosWindowCloseEventHandler);
                PopUpManager.addPopUp(infosWindow, this, true);
                PopUpManager.centerPopUp(infosWindow);
                //lance le remoting apres l'affichage du popUp
                infosWindow.ligne = myGrid.selectedItem;
            }
        }

        private function infosWindowCloseEventHandler(ev:Event):void
        {
            PopUpManager.removePopUp(infosWindow);
        }

        private function btAfficherHandler(me:MouseEvent):void
        {
            displayEvent.indexPeriode = myGrid.selectedItem.IDINVENTAIRE_PERIODE;
            displayEvent.affichable = true;
            displayEvent.numeroFacture = myGrid.selectedItem.NUMERO_FACTURE;
            dispatchEvent(displayEvent);
        }

        private function btAfficherInfosClickHandler(me:MouseEvent):void
        {
            showInfosWindow();
        }

        //-------Chargement des donnees----------- remoting ---------------------------------------------------//
        // Chargement des données par remoting
        protected function chargerDonnees():void
        {	
			
            op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M312.PerimetreE0", "getPaginateDetailProduit", chargerDonnneesResultHandler, null);
            RemoteObjectUtil.callService(op, perimetre, modeSelection, identifiant, _idproduit, moisDeb, moisFin, segment);
        }

        protected function chargerDonneesPaginate(parametresRechercheVO:ParametresRechercheVO, itemIntervalleVO:ItemIntervalleVO = null):void
        {
            // Paramétrages
            var st:String;
            var ob:String;
			
            if (!parametresRechercheVO.SEARCH_TEXT)
            {
                st = "SITE,";
            }
            else
            {
                st = parametresRechercheVO.SEARCH_TEXT;
                // on vérifie que la valeur nulle n'est pas passée en paramètre
                var ar:Array = st.split(",");
                if (ar[1] == "null")
                {
                    st = ar[0] + ", ";
                }
            }
            if (!parametresRechercheVO.ORDER_BY)
            {
                ob = "SITE,ASC";
            }
            else
            {
                ob = parametresRechercheVO.ORDER_BY;
                var arr:Array = ob.split(",");
                var order:String;
                switch (arr[1])
                {
                    case "Croissant":
                        order = "ASC";
                        break;
                    case "ASC":
                        order = "ASC";
                        break;
                    case "DESC":
                        order = "DESC";
                        break;
                    case "Décroissant":
                        order = "DESC";
                        break;
                    default:
                        order = "ASC";
                        break;
                }
                ob = String(arr[0]) + "," + order;
                st = parametresRechercheVO.SEARCH_TEXT;
                var arrr:Array = st.split(",");
                if (arrr[1] == "null")
                {
                    st = arrr[0] + ", ";
                }
            }
            if (firstloading)
            {
                st = "SITE,";
                firstloading = false;
            }
            op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M312.PerimetreE0", "getPaginateDetailProduit", chargerDonnneesResultHandler, null);
            RemoteObjectUtil.callService(op, perimetre, modeSelection, identifiant, _idproduit, moisDeb, moisFin, segment, st, ob, itemIntervalleVO.indexDepart, itemIntervalleVO.tailleIntervalle);
        }

        protected function chargerDonnneesFaultHandler(fe:FaultEvent):void
        {
            trace(fe.fault.faultString, fe.fault.name);
        }

        protected function chargerDonnneesResultHandler(re:ResultEvent):void
        {
            try
            {
                var collec:ArrayCollection = re.result as ArrayCollection;
                // affectation de la lignes des totaux globaux
                myFooterGrid.columns[1].headerText = ConsoviewFormatter.formatNumber(collec.getItemAt(0).TOTAL_NBAPPELS, 0);
                /*     myFooterGrid.columns[2].headerText = ConsoviewFormatter.formatNumber(collec.getItemAt(0).TOTAL_QUANTITE, 0);*/
                myFooterGrid.columns[2].headerText = ConsoviewFormatter.formatNumber(collec.getItemAt(0).TOTAL_VOLUME, 0);
                myFooterGrid.columns[3].headerText = ConsoviewFormatter.formatEuroCurrency(collec.getItemAt(0).TOTAL_MONTANT, 2);
                if (collec.length > 0)
                {
                    nbTotalElement = collec.getItemAt(0).NBRECORD;
                    collecDetailProduit = new ArrayCollection();
                    for (var i:int = 0; i < collec.length; i++)
                    {
                        collecDetailProduit.addItem(collec.getItemAt(i));
                    }
                }
                else
                {
                    nbTotalElement = 0;
                }
            }
            catch (er:Error)
            {
                trace(er.message, "fr.consotel.consoview.M312.produits.facade getDetailProduit");
            }
        }

        protected function checkEditionGrid(de:DataGridEvent):void
        {
            // Demande F le Blanc Incident : I-01075-VI9Z
            // Supprimer l'édition des noms des collaborateurs  
            if (myGrid.selectedItem != null)
            {
                if ((String(myGrid.selectedItem.SITE).length != 0) || (segment.toLowerCase() == ResourceManager.getInstance().getString('M312', 'mobile')))
                {
                    de.preventDefault();
                }
            }
            else
            {
                de.preventDefault();
            }
        }

        protected function enregistrerDonnee(de:DataGridEvent):void
        {
        }

        protected function enregistrerDonneeResultHandler(re:ResultEvent):void
        {
            ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M312', 'donn_es_enregistr_es'));
        }

        protected function defaultHandler(fe:FaultEvent):void
        {
            Alert.show(ResourceManager.getInstance().getString('M312', 'Erreur_Remoting'));
        }
    }
}