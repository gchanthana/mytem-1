package composants.tb.tableaux
{
	import mx.resources.ResourceManager;

	public class TableauError extends Error
	{
		public function TableauError(message:String="", id:int=0):void{
			name = "TableauError";
			message = ResourceManager.getInstance().getString('M312', 'le_tableau_n_existe_pas');
			id = 2;			
			super(message,id);
		}
	}
}