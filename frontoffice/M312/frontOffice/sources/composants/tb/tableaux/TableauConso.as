package composants.tb.tableaux
{
	import composants.tb.tableaux.surtheme.SurThemeConsommation;
	import composants.util.ConsoviewFormatter;
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.charts.HitData;
	import mx.charts.events.ChartItemEvent;
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class TableauConso extends TableauConso_IHM implements ITableauModel
	{
		 
		
		protected var opTitre : AbstractOperation; 
		
		protected var op : AbstractOperation;
				
		protected var _dataProviders : ArrayCollection; 	
		
		private var perimetre : String;
		
		private var modeSelection : String;
		
		protected var identifiant : String;
		
		private var _segmentDataProvider:ArrayCollection;
		
		protected const TYPE_SEGMENT:String = 'SEGMENT';
		protected const TYPE_SURTHEME:String = 'SURTHEME';

			
		[Bindable]
		protected var _total : Number = 0;
		[Bindable]
		protected var _totalQte : Number = 0;
		[Bindable]
		protected var _totalVol : Number = 0;
		
		
		//--------------------------------------------------------------------------------------------------------------------------------------------------		
		/**
		* Constructeur 
		**/
		public function TableauConso(){
				super();
				addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
				
		/**
		 * Retourne le montant total
		 * */
		 public function get total():Number{
		 	return _total;
		 }
		
		
		/**
		* Permet de passer les valeurs aux tableaux du composant.
		* <p>Pour chaque tableau de la collection, un DataGrid sera affiché à la suite du précédant</p>
		* @param d Une collection de tableau. 
		**/				
		public function set dataProviders(d : ArrayCollection):void
		{			
			_dataProviders = _formatterDataProvider(d);
			drawTabs();
		}
		
		/**
		*  Retourne le dataproviders
		**/	
		[Bindable]			
		public function get dataProviders():ArrayCollection{
			return _dataProviders;
		}	
				
		/**
		* Permet d'affecter une fonction pour traiter les Clicks sur Les DataGrids du composant
		* @param changeFunction La fonction qui traite les clicks.<p> Elle prend un paramètre de type Object qui représente une ligne du DataGrid</p>
		**/		
		public function set changeHandlerFunction ( changeFunction : Function ):void{
			//_changeHandlerFunction = changeFunction;
		}		
						
		/**
		*  Retourne une reference vers la fonction qui traite le data provider
		**/			
		public function get changeHandlerFunction():Function{
			return null;//_changeHandlerFunction;
		}
			
		/**
		 * Met à jour les donées du tableau
		 **/
		public function update():void
		{			
			chargerDonnees();
		}
		
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		public function clean():void{
			myGrid.dataProvider = null;
		}		
		
		/**
		 * Met à jour la periode
		 * */		 
		public function updatePeriode(moisDeb : String = null, moisFin : String = null):void{
			 
		}
		
		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...) 			
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		public function updatePerimetre(perimetre : String = null, modeSelection : String = null):void
		{
			this.perimetre = perimetre;
			this.modeSelection = modeSelection;		
		}
					
		/*---------- PRIVATE -----------------------*/
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth,unscaledHeight)
			if(myPieGraph && myPieGraph.myPie)
			{
				myPieGraph.dataProvider = segmentDataProvider;
				myPieGraph.field = "MONTANT_TOTAL_AFFICHEE";
				myPieGraph.nameField = "LIBELLE";
			}
		}

		//initialisation du composant
		protected function init(fe :  FlexEvent):void
		{	
			myPieGraph.myPie.addEventListener(ChartItemEvent.ITEM_CLICK, _myPieItemClickHandler);
			myPieGraph.labelFunction = pielabelFunction;
			myPieGraph.dataTipsFunction = pieDataTipFunction;
			//update();
			
		}
		
		
		
		
		
		private function drawTabs():void{
			rep.dataProvider = dataProviders;
			
			if (rep.dataProvider != null)	
				for (var i:int = 0; i < rep.dataProvider.length; i++){
					//myGrid[i].addEventListener(Event.CHANGE,_changeHandlerFunction);
						myGrid[i].addEventListener(Event.CHANGE,myGridChangeEventHandler);	
					
			}
			
				
			myFooterGrid.columns[1].headerText = ConsoviewFormatter.formatNumber(_totalQte,2);			
			myFooterGrid.columns[2].headerText = ConsoviewFormatter.formatNumber(_totalVol,2);	
			myFooterGrid.columns[3].headerText = ConsoviewFormatter.formatEuroCurrency(_total,2);
			var myEv :  Event = new Event("totalSet");
			dispatchEvent(myEv);
		}
		
		protected function myGridChangeEventHandler(ev :Event):void{
			
			var eventObj : TableauChangeEvent = new TableauChangeEvent("tableauChange");
			
			eventObj.LIBELLE = ev.currentTarget.selectedItem.LIBELLE;
			eventObj.MONTANT_TOTAL = ev.currentTarget.selectedItem.MONTANT_TOTAL;
			eventObj.ID_LIBELLE = ev.currentTarget.selectedItem.ID_LIBELLE;
			//eventObj.IDSEGMENT_THEME = ev.currentTarget.selectedItem.IDSEGMENT_THEME;
			eventObj.ID = ev.currentTarget.selectedItem.ID;
			eventObj.QUANTITE = ev.currentTarget.selectedItem.QUANTITE;
			eventObj.TYPE_THEME	= ev.currentTarget.selectedItem.TYPE_THEME;
			eventObj.SUR_THEME = ev.currentTarget.selectedItem.SUR_THEME;
			eventObj.IDSUR_THEME = ev.currentTarget.selectedItem.IDSUR_THEME;
			eventObj.SEGMENT = ev.currentTarget.selectedItem.SEGMENT;
			eventObj.TYPE = ev.currentTarget.selectedItem.TYPE;
			eventObj.NBAPPELS = ev.currentTarget.selectedItem.NBAPPELS;
			eventObj.SOURCE = ev.currentTarget;
			eventObj.IDENTIFIANT = identifiant;
			dispatchEvent(eventObj);
		}
		
		protected function _myPieItemClickHandler(event:ChartItemEvent):void
		{
			var eventObj : TableauChangeEvent = new TableauChangeEvent("tableauChange");
			eventObj.LIBELLE = event.hitData.item.LIBELLE;
			eventObj.SEGMENT = event.hitData.item.SEGMENT_THEME;
			eventObj.TYPE = TYPE_SEGMENT;
			eventObj.SOURCE = myPieGraph;
			eventObj.IDENTIFIANT = identifiant;
			
			dispatchEvent(eventObj);
		}

		private var _surthmeConso:SurThemeConsommation = new SurThemeConsommation();
		// permet de formatter la collection de DataProvider 
		protected function _formatterDataProvider(d: ArrayCollection):ArrayCollection
		{		
									
			var tmpCollection 	:ArrayCollection = new ArrayCollection();
			var o 				:Object;
			var cptr			:int = 0;
			var array			:ArrayCollection;
			var mobileArray 	:Array = new Array();
			var fixeArray 		:Array = new Array();
			var dataArray 		:Array = new Array();
			
			_total = 0;
			_totalQte = 0;
			_totalVol = 0;
			
			for	(o in d)
			{	
				var sgmtThem		:String = d[o].segment_theme.toUpperCase();
				var SGMT_MOBILE		:String = ResourceManager.getInstance().getString('M312', 'SGMT_MOBILE').toUpperCase();
				var SGMT_FIXE		:String = ResourceManager.getInstance().getString('M312', 'SGMT_FIXE').toUpperCase();
				var SGMT_DATA		:String = ResourceManager.getInstance().getString('M312', 'SGMT_DATA').toUpperCase();
				
				switch(sgmtThem)
				{
					case SGMT_MOBILE:  mobileArray.push(formateObject(d[o], 2));break;
					case SGMT_FIXE 	:  fixeArray.push(formateObject(d[o], 1));break;
					case SGMT_DATA 	:  dataArray.push(formateObject(d[o], 3));break;										
				}  
			}
			
			if (fixeArray.length > 0)
			{
				array = new ArrayCollection();
				var obj0 : Object = new Object();
				obj0["LIBELLE"] 		= fixeArray[0].SEGMENT;
				obj0["SEGMENT"] 		= fixeArray[0].SEGMENT;
				obj0["TYPE"] 			= "SEGMENT";	
				obj0["QUANTITE"]		=" ";
				obj0["NBAPPELS"]		=" ";
				obj0["IDSEGMENT_THEME"] = 1;
				obj0["MONTANT_TOTAL"]	= calculTotal(fixeArray,"MONTANT_TOTAL");
				array = new ArrayCollection(fixeArray);
				
				for(cptr = 0; cptr < _surthmeConso.surThemeConsommationFixe.length;cptr++)
				{
					if(!ConsoviewUtil.isIdInArray(_surthmeConso.surThemeConsommationFixe[cptr].IDSUR_THEME,"IDSUR_THEME",array.source))
					{
						array.addItemAt(_surthmeConso.surThemeConsommationFixe[cptr], cptr);
					}
						
				}
				array.addItemAt(obj0, 0);
				tmpCollection.addItem(array.source);
			}
			else
			{
				var obj01 : Object = new Object();
				obj01["LIBELLE"] 		= ResourceManager.getInstance().getString('M312', 'Fixe');
				obj01["SEGMENT"] 		= ResourceManager.getInstance().getString('M312', 'SGMT_FIXE');
				obj01["TYPE"] 			= "SEGMENT";	
				obj01["QUANTITE"]		= " ";
				obj01["NBAPPELS"]		= " ";
				obj01["IDSEGMENT_THEME"]= 1;
				obj01["MONTANT_TOTAL"]	= calculTotal(new Array,"MONTANT_TOTAL");
				array = new ArrayCollection();
				
				for(cptr = 0; cptr < _surthmeConso.surThemeConsommationFixe.length;cptr++)
				{
					array.addItemAt(_surthmeConso.surThemeConsommationFixe[cptr], cptr);
				}
				array.addItemAt(obj01, 0);
				tmpCollection.addItem(array.source);
			}
			
			if (mobileArray.length > 0)
			{
				var obj : Object 		= new Object();
				obj["LIBELLE"] 			= mobileArray[0].SEGMENT;
				obj["SEGMENT"] 			= mobileArray[0].SEGMENT;
				obj["TYPE"] 			= "SEGMENT";	
				obj["QUANTITE"]			= " ";
				obj["NBAPPELS"]			= " ";
				obj["IDSEGMENT_THEME"]	= 2;
				obj["MONTANT_TOTAL"]	= calculTotal(mobileArray,"MONTANT_TOTAL");
				array = new ArrayCollection(mobileArray);
				
				for(cptr = 0; cptr < _surthmeConso.surThemeConsommationMobile.length;cptr++)
				{
					if(!ConsoviewUtil.isIdInArray(_surthmeConso.surThemeConsommationMobile[cptr].IDSUR_THEME,"IDSUR_THEME",array.source))
					{
						array.addItemAt(_surthmeConso.surThemeConsommationMobile[cptr], cptr);
					}
				}
				array.addItemAt(obj, 0);
				tmpCollection.addItem(array.source);
			}
			else
			{
				var obj00 : Object = new Object();
				obj00["LIBELLE"] 			= ResourceManager.getInstance().getString('M312', 'Mobile');
				obj00["SEGMENT"] 			= ResourceManager.getInstance().getString('M312', 'SGMT_MOBILE');
				obj00["TYPE"] 				= "SEGMENT";	
				obj00["QUANTITE"]			=" ";
				obj00["NBAPPELS"]			=" ";
				obj00["IDSEGMENT_THEME"]	= 2;
				obj00["MONTANT_TOTAL"]		= calculTotal(new Array,"MONTANT_TOTAL");
				array = new ArrayCollection();
				
				for(cptr = 0; cptr < _surthmeConso.surThemeConsommationMobile.length;cptr++)
				{
					array.addItemAt(_surthmeConso.surThemeConsommationMobile[cptr], cptr);
				}
				array.addItemAt(obj00, 0);
				tmpCollection.addItem(array.source);
			}
						
			if (dataArray.length > 0)
			{
				array = new ArrayCollection();
				var obj1 : Object 		= new Object();
				obj1["LIBELLE"]			= dataArray[0].SEGMENT;
				obj1["SEGMENT"] 		= dataArray[0].SEGMENT;
				obj1["TYPE"] 			= "SEGMENT";
				obj1["QUANTITE"]		= " ";
				obj1["NBAPPELS"]		= " ";
				obj1["IDSEGMENT_THEME"]	= 3;
				obj1["MONTANT_TOTAL"]	= calculTotal(dataArray,"MONTANT_TOTAL");
				array = new ArrayCollection(dataArray);
				
				for(cptr = 0; cptr < _surthmeConso.surThemeConsommationData.length;cptr++)
				{
				
					if(!ConsoviewUtil.isIdInArray(_surthmeConso.surThemeConsommationData[cptr].IDSUR_THEME,"IDSUR_THEME",array.source))
					{
						array.addItemAt(_surthmeConso.surThemeConsommationData[cptr], cptr);
					}
				
				}
				array.addItemAt(obj1, 0);
				tmpCollection.addItem(array.source);				
			}
			else
			{
				array = new ArrayCollection();
				var obj10 : Object = new Object();
				obj10["LIBELLE"] 			= ResourceManager.getInstance().getString('M312', 'Data');
				obj10["SEGMENT"] 			= ResourceManager.getInstance().getString('M312', 'SGMT_DATA');
				obj10["TYPE"] 				= "SEGMENT";
				obj10["QUANTITE"]			= " ";
				obj10["NBAPPELS"]			= " ";
				obj10["IDSEGMENT_THEME"]	= 3;
				obj10["MONTANT_TOTAL"]		= calculTotal(new Array,"MONTANT_TOTAL");
				array = new ArrayCollection();
				
				
				for(cptr = 0; cptr < _surthmeConso.surThemeConsommationData.length;cptr++)
				{
					array.addItemAt(_surthmeConso.surThemeConsommationData[cptr], cptr);
				}
				array.addItemAt(obj10, 0);
				tmpCollection.addItem(array.source);
			}			
			return tmpCollection;
		}
		
		protected function formateObject(obj:Object, idsegment_theme:int = -1):Object
		{
			var o : Object = new Object();
			
			_totalVol = _totalVol + (parseInt(obj.duree_appel));	
			_totalQte = _totalQte + parseInt(obj.nombre_appel);	
			_total = _total +  parseFloat(obj.montant_final);	
			
			o["NBAPPELS"]			= ConsoviewFormatter.formatNumber(Number(obj.nombre_appel),2);
			o["LIBELLE"] 			= obj.theme_libelle;
			o["SEGMENT"] 			= obj.segment_theme;
			o["QUANTITE"] 			= ConsoviewFormatter.formatNumber((parseFloat(obj.duree_appel)),2);
			o["TYPE_THEME"] 		= obj.type_theme;
			o["SUR_THEME"] 			= obj.theme_libelle;
			o["IDSUR_THEME"] 		= obj.idsur_theme; 
			o["MONTANT_TOTAL"]		= obj.montant_final;
			o["ORDRE_AFFICHAGE"]	= obj.ordre_affichage;
			o["TYPE"] 				= "SURTHEME";	
			o["ID"] 				= obj.idtheme_produit;	
			o["ID_LIBELLE"]			= obj.idsur_theme;
			o["IDSEGMENT_THEME"]	= idsegment_theme;
			
			return o;
		}	
		
		protected function calculTotal(arr : Array, colname : String):Number{
			var total : Number = 0;
			
			for(var i:int=0; i< arr.length;i++){
				
				total = total + parseFloat(arr[i][colname]);				
			}
			return total;
		}
		
		private function filterFunc ():void{
			
		}
		
		
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		protected function getSurThemeConsos():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M312.accueil.facade",
																				"getSurThemeConsos",
																				getSurThemeConsosResultHandler);
			RemoteObjectUtil.callService(op);
		}
		
		protected function getConsoBySegment():void
		{
			var op2:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M312.accueil.facade",
																				"getTotalConsosBySegment",
																				 getConsoBySegmentResultHandler);
			RemoteObjectUtil.callService(op2);
		}
		
		protected function chargerDonnees():void
		{
			getSurThemeConsos();
			getConsoBySegment();
			//getTitre();
		}

		protected function getSurThemeConsosResultHandler(re :ResultEvent):void{
		 	try
		 	{	
		  		dataProviders =  re.result as ArrayCollection;
		  	}
		  	catch(er : Error)
		  	{		  	
		  		trace(er.message,"fr.consotel.consoview.M312.accueil.facade  getSurThemeAbos ");
		  	}
		  
		 } 
		 
		protected function getConsoBySegmentResultHandler(re :ResultEvent):void{
		 	try
		 	{	
		  		segmentDataProvider =  re.result as ArrayCollection;
		  		var cursor:IViewCursor=segmentDataProvider.createCursor();
				var valAbsolue:Number=0;
				while (!cursor.afterLast)
				{
					if (Number(cursor.current.MONTANT_TOTAL) == 0 && Number(cursor.current.QUANTITE) == 0)
					{
						cursor.remove();
					}
					else
					{
						if(cursor.current.MONTANT_TOTAL < 0)
						{
							valAbsolue = Math.abs(cursor.current.MONTANT_TOTAL);
							cursor.current.MONTANT_TOTAL_AFFICHEE = valAbsolue;
						}
						else
						{
							cursor.current.MONTANT_TOTAL_AFFICHEE = cursor.current.MONTANT_TOTAL;
						}
						cursor.moveNext();
					}

				}
				segmentDataProvider.refresh();
		  		
		  		
		  	}
		  	catch(er : Error)
		  	{		  	
		  		trace(er.message,"fr.consotel.consoview.M312.accueil.facade  getSurThemeAbos ");
		  	}
		  
		}

		 
/*		 protected function getTitre():void{
		 	opTitre = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.M312.accueil.facade",
																			"getLibelleConso",
																			getTitreResultHandler,null);
			
			
			RemoteObjectUtil.callService(opTitre,perimetre,modeSelection);
		 }			
		 
		 protected function getTitreResultHandler(re : ResultEvent):void{
		 	title = String(re.result);
		 }*/
		
		[Bindable]
		public function set segmentDataProvider(value:ArrayCollection):void
		{
			if(value != null && value != _segmentDataProvider)
			{
				_segmentDataProvider = value;
				invalidateProperties();
				invalidateSize();
				invalidateDisplayList();
			} 
		}

		public function get segmentDataProvider():ArrayCollection
		{
			return _segmentDataProvider;
		}
		
		
		protected function pielabelFunction(data:Object, field:String, index:Number, percentValue:Number):String 
		{
            var temp:String= (" " + percentValue).substr(0,6);
          
            return data.LIBELLE + ": " + '\n' + ResourceManager.getInstance().getString('M312', 'Total___') + ConsoviewFormatter.formatEuroCurrency(data.MONTANT_TOTAL,2) + '\n' + temp + "%";
        }
        
        protected function pieDataTipFunction(hitData:HitData):String
		{
			var libelle:String=hitData.item.LIBELLE;
			var montant:Number=hitData.item.MONTANT_TOTAL;
			var prop:Number=Math.abs((montant * 100 / _total));


			return ResourceManager.getInstance().getString('M312', 'Libell_____b_') + libelle + ResourceManager.getInstance().getString('M312', '__b__br_montant____b__font_color____ff00') + ConsoviewFormatter.formatEuroCurrency(montant,2) + ResourceManager.getInstance().getString('M312', '__font___b__br_Proportion___b_') + prop.toPrecision(4) + "%</b>";

		}


	}
}
