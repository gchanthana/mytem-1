//***************************************************	
//	author : samuel.divioka								*
//	date   : Mar 31, 2011								*
//	owner  : Consotel								*
//***************************************************
package event
{
	import flash.events.Event;
	
	public class InventaireEquipementIhmEvent extends Event
	{
		
		public static const MARQUE_CHANGE_EVENT:String = "marqueChangeEvent";
		public static const MODELE_CHANGE_EVENT:String = "modeleChangeEvent";
		public static const NAV_CHANGE_EVENT:String = "navChangeEvent";
		
		public function InventaireEquipementIhmEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new InventaireEquipementIhmEvent(type, bubbles, cancelable)
		}
	}
}