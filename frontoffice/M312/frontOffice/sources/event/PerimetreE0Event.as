package event
{
	import flash.events.Event;
	
	public class PerimetreE0Event extends Event
	{
		
		public static const PE0_INFOS_UPDATED:String = "pe0InfosUpdated";
		
		public function PerimetreE0Event(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new PerimetreE0Event(type, bubbles, cancelable)
		}
	}
}