//***************************************************	
//	author : samuel.divioka								*
//	date   : Apr 1, 2011								*
//	owner  : Consotel								*
//***************************************************
package event
{
	import flash.events.Event;
	
	public class HistoriqueServiceEvent extends Event
	{
		public static const HISTORIQUE_SERVICE_MODEL_HISTORIQUE_UPDATED:String = "historiqueServiceModelHistoriqueUpdated";
		
		public function HistoriqueServiceEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new HistoriqueServiceEvent(type, bubbles, cancelable)
		}
	}
}