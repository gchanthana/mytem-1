package service
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class InventaireService
	{
		
		private const _REMOTE_SERVICE_NAME:String = "fr.consotel.consoview.M312.InventaireService";
		
		public static const OP_MODELE:String = "opModele";
		public static const OP_MARQUE:String = "opMarque";
		public static const OP_EQUIPEMENT:String = "opEquipement";
		
		
		//les données
		[Bindable]public var model:InventaireModel;
		
		//le handler charger de récupérer la requête
		private var handler:InventaireHandler;	
		
		private var _opMarque:AbstractOperation;
		private var _opModele:AbstractOperation;
		private var _opEquipement:AbstractOperation;
		
		
		public function InventaireService()
		{
			model = new InventaireModel();
			handler = new InventaireHandler(model);
		}
		
		public function getMarqueEquipement(typeperimetre:String):void
		{	
			_opMarque = RemoteObjectUtil.getSilentOperation(_REMOTE_SERVICE_NAME,"getMarqueEquipement",handler.getMarqueEquipementResultHandler);
			RemoteObjectUtil.callSilentService(_opMarque,typeperimetre);
		} 
		
		public function getModeleEquepement(marque:String,typeperimetre:String):void
		{
			if(marque)
			{
				_opModele = RemoteObjectUtil.getSilentOperation(_REMOTE_SERVICE_NAME,"getModeleEquepement",handler.getModeleEquepementResultHandler);
				RemoteObjectUtil.callSilentService(_opModele,marque,typeperimetre,model.idDatePeriodeCompleteInventaire);	
			}
		}
		
		public function getListEquepement(marque:String,modele:String,typeperimetre:String):void
		{
			if(marque && modele)
			{
				_opEquipement = RemoteObjectUtil.getSilentOperation(_REMOTE_SERVICE_NAME,"getListEquepement",handler.getListEquepementResultHandler);
				RemoteObjectUtil.callSilentService(_opEquipement,marque,modele,typeperimetre,model.idDatePeriodeCompleteInventaire);
			}
		}
		
		
		public function cancelOperation(idOperation:String = 'all'):void
		{
			var _idOperation:String = '_'+idOperation;
		
			if(idOperation == 'all')
			{
				if(_opEquipement) _opEquipement.cancel();
				if(_opMarque) _opMarque.cancel();
				if(_opModele) _opModele.cancel();
			}
			else if(this.hasOwnProperty(_idOperation) && this[_idOperation] is AbstractOperation && this[_idOperation])
			{
				(this[_idOperation] as AbstractOperation).cancel();
			}	
				
		}
		
		
		
	}
}