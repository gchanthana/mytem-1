package service
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class PerimetreE0Service
	{
		
		private const _REMOTE_SERVICE_NAME:String = "fr.consotel.consoview.M312.PerimetreE0";
		
		//les données
		[Bindable]public var model:PerimetreE0Model;
		
		//le handler charger de récupérer la requête
		private var handler:PerimetreE0Handler;
		
		public function PerimetreE0Service()
		{
			model = new PerimetreE0Model();
			handler = new PerimetreE0Handler(model);
		}
		
		/**
		 *	Récupération des infos de Date de perimtre complet (on pourra ajouter d'autres infos ici)
		 */		
		public function getPerimetreInfos():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,_REMOTE_SERVICE_NAME,"getPerimetreInfos",handler.getPerimetreInfosResultHandler);
			RemoteObjectUtil.callService(op);
		}
		
		
	}
}