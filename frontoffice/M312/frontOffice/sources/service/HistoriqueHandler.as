//***************************************************	
//	author : samuel.divioka							*
//	date   : Apr 1, 2011							*
//	owner  : Consotel								*
//***************************************************
package service
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	public class HistoriqueHandler
	{
		private var _model:HistoriqueModel;
		
		public function HistoriqueHandler(model:HistoriqueModel)
		{
			_model = model;
		}
		
		internal function getEvolutionSurThemeConsoResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateEvolutionSurThemeConso(event.result as ArrayCollection)		
			}	
		}
	}
}