//***************************************************	
//	author : samuel.divioka								*
//	date   : Apr 1, 2011								*
//	owner  : Consotel								*
//***************************************************
package service
{
	import composants.util.ConsoviewUtil;
	
	import event.HistoriqueServiceEvent;
	import event.InventaireServiceEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.collections.Sort;
	import mx.collections.SortField;

	public class HistoriqueModel extends EventDispatcher
	{
		
		//les données
		[Bindable]public var evolutionSurThemeConso:ArrayCollection;
		[Bindable]public var surThemeConso:ArrayCollection;
		[Bindable]public var evolutionSurThemeConsoBrut:ArrayCollection;
		
		public function HistoriqueModel()
		{
			surThemeConso = new ArrayCollection();
			evolutionSurThemeConso = new ArrayCollection();
			evolutionSurThemeConsoBrut = new ArrayCollection();
		}
		
		
		public function resetEvolutionSurThemeConso():void
		{
			evolutionSurThemeConsoBrut.removeAll();
			evolutionSurThemeConso.removeAll();
			surThemeConso.removeAll();
		}
		
		
		internal function updateEvolutionSurThemeConso(values:ArrayCollection):void
		{
			resetEvolutionSurThemeConso();
			
			if(values)
			{
				
				var cursor:IViewCursor = values.createCursor();
				var currentIdPeriode:Number = 0;
				
				var currentObj:Object;
				while(!cursor.afterLast)
				{	
					
					if(cursor.current["saw_2"] !=  currentIdPeriode)
					{
						currentObj = {saw_2:cursor.current["saw_2"]};
						evolutionSurThemeConso.addItem(currentObj);						
						currentIdPeriode = currentObj["saw_2"];	
						currentObj[cursor.current["saw_0"]]= cursor.current["saw_3"];
					}
					else
					{	
						currentObj[cursor.current["saw_0"]]= cursor.current["saw_3"];
						currentObj["saw_1"]=cursor.current["saw_1"];
					}
					
					
					if(!ConsoviewUtil.isLabelInArray(cursor.current["saw_0"],"SUR_THEME",surThemeConso.source))
					{
						surThemeConso.addItem({SUR_THEME:cursor.current["saw_0"]})
					}
					
					
					cursor.moveNext();
				}
				
				evolutionSurThemeConsoBrut = values;
				
				var _lsort:Sort = new Sort();
				_lsort.fields = [new SortField("saw_2",false,true,true),new SortField("saw_0",true)];
				evolutionSurThemeConsoBrut.sort = _lsort;
				evolutionSurThemeConsoBrut.refresh();
				
				
				
				dispatchEvent(new HistoriqueServiceEvent(HistoriqueServiceEvent.HISTORIQUE_SERVICE_MODEL_HISTORIQUE_UPDATED));
			}	
		}
	}
}