package service
{
	import composants.access.perimetre.PerimetreEvent;
	
	import event.PerimetreE0Event;
	
	import flash.events.EventDispatcher;
	
	import vo.PerimetreE0Vo;

	
	public class PerimetreE0Model extends EventDispatcher
	{
		[bindable]public var perimetreInfos:PerimetreE0Vo;
		
		public function PerimetreE0Model()
		{	
		}
		
		internal function udpatePerimetreInfos(value:Object):void
		{
			perimetreInfos = new PerimetreE0Vo(value);
			dispatchEvent(new PerimetreE0Event(PerimetreE0Event.PE0_INFOS_UPDATED));
		}
	}
}