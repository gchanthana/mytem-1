package appli.login {
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtilEvent;

	public class LoginModel extends EventDispatcher implements ILoginModel {
		private var authInfos:Object;
		private var remoteObjectUtil:RemoteObjectUtil = null;
		
		public function LoginModel(target:IEventDispatcher=null) {
			super(target);
			
		}
		
		public function connect(loginString:String,pwdString:String):void {
			remoteObjectUtil.connect(loginString,pwdString);
		}
		
		public function getAuthInfos():Object {
			return authInfos;
		}
	}
}
