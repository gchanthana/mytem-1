package
{
    import appli.control.AppControlEvent;
    import appli.events.ConsoViewEvent;
    import appli.login.LoginEvent;
    
    import entity.CodeLangue;
    
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import interfaceClass.IModuleLogin;
    
    import modulelogin.ihm.retrievepassword.RetrievePasswordIHM;
    
    import mx.controls.Alert;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.resources.ResourceManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;

    public class Login extends LoginIHM
    {
        private var appVersion:String;
        private var authOp:AbstractOperation;
        private var sendMailOp:AbstractOperation;
        private var authInfos:Object;
        private var infosMessage:String;
        private var currentlogin:String;
        private var passwordPopup:RetrievePasswordIHM;
      	
		private var code_langue:String = "fr_FR";
		private var idLang:int = 3;
		
        private var messColor:int;
		
		public function Login(appVersionValue:String, infosMsg:String)
        {
            appVersion = appVersionValue;
            authInfos = null;
			
            sendMailOp = RemoteObjectUtil.getOperation("fr.consotel.consoprod.mail.SendMail", "sendSingleMail", mailSent);
            messColor = 0xF5930A;

			this.addEventListener(FlexEvent.CREATION_COMPLETE, afterCreationComplete);
        }

        public function getAuthInfos():Object
        {
            return authInfos;
        }

        public function setcurrentlogin(curLogin:String = ""):void
        {
            this.currentlogin = curLogin;
        }
		public function setInfosMessages(str:String = ""):void
		{
			this.infosMessage = str;
			loginMsg.text = this.infosMessage;
		}
        private function afterCreationComplete(event:FlexEvent):void
        {
            loginMsg.text = infosMessage;
            loginMsg.setStyle('color', messColor);
            loginText.text = currentlogin;
            pwdText.text = "";
            loginText.setFocus();           
            btnPwd.addEventListener(MouseEvent.CLICK, askPwd);
            stdConnectButton.addEventListener(MouseEvent.CLICK, connectItemHandler);
			loginText.addEventListener(KeyboardEvent.KEY_DOWN, connectItemHandler);
			pwdText.addEventListener(KeyboardEvent.KEY_DOWN, connectItemHandler);			
		 		
			activeControls(true);
        }
        private function activeControls(state:Boolean):Boolean
        {
            loginText.enabled = pwdText.enabled = stdConnectButton.enabled = btnPwd.enabled = state;
            return state;
        }
        private function connectItemHandler(event:Event):void
        {
            if (event is MouseEvent)
            {
                if ((loginText.text.length == 0) || pwdText.text.length == 0)
                    Alert.show(ResourceManager.getInstance().getString('M04', 'Acc_s_Refus____Login_ou_Mot_de_passe_invalide_s_'), ResourceManager.getInstance().getString('M04', 'Connexion'));
                else
					letsCOnnect();
            }
            else if (event is KeyboardEvent)
            {
                if ((event as KeyboardEvent).keyCode == 13)
                {
					letsCOnnect();
                }
            }
        }
		private function letsCOnnect():void
		{
			var cdeLg:CodeLangue = new CodeLangue(code_langue,"","",idLang,"");
			
			var objLogin:Object = new Object();
			objLogin.login = loginText.text;
			objLogin.mdp = pwdText.text;
			objLogin.codeLangue = cdeLg;
			if(ckxSeSouvenir != null)
				objLogin.boolSeSouvenir = ckxSeSouvenir.selected;
			else
				objLogin.boolSeSouvenir = false;
			
			(parentDocument as IModuleLogin).validateLogin(objLogin);
		}

        /**
         * Ouverture de la popUp de demande d'envoi
         * du mot de passe
         * @param event
         *
         */
        private function askPwd(event:Event):void
        {
            passwordPopup = new RetrievePasswordIHM();
            PopUpManager.addPopUp(passwordPopup, this, true);
            PopUpManager.centerPopUp(passwordPopup);
            
            if (loginText.text != "")
            {
                passwordPopup.tiEmail.text = loginText.text;
            }
        }

        private function mailSent(e:ResultEvent):void
        {
            Alert.show("La demande de mot de passe a été envoyée.");
        }
				
    }
}