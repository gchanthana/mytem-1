package modulelogin.services.retrievepassword
{
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.rpc.AbstractOperation;

    public class RetrievePasswordServices
    {
		//CONST
		private static const CODE_APPLICATION:Number = 51;
		
        //VARIABLES------------------------------------------------------------------------------
        public var myDatas:RetrievePasswordDatas;

        public var myHandlers:RetrievePasswordHandlers;

        public function RetrievePasswordServices()
        {
            myDatas = new RetrievePasswordDatas();
            myHandlers = new RetrievePasswordHandlers(myDatas);
        }

        //METHODE PUBLIC-------------------------------------------------------------------------
        public function getUserPassword(pEmail:String, localeChain:String):void
        {
            try
            {
                var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M04.ModuleLogin_cfc", "getUserPassword", myHandlers.getUserPasswordResultHandler);
                RemoteObjectUtil.callService(op, pEmail, localeChain,CODE_APPLICATION);
            }
            catch (e:Error)
            {
                throw new Error(" # RetrievePasswordServices-getUserPassword() Erreur :" + e);
            }
        }
    }
}