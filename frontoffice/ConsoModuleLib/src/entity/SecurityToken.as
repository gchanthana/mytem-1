package entity
{
	public class SecurityToken
	{
		public function SecurityToken()
		{
		}
		private var _ID:int;
		private var _UUID:String;
		
		
/* --------------------------- PUBLIC ---------------------------------- */
		/**
		 * <code>verify</code>
		 * 
		 * Teste le token pour vérifier sa validité
		 * 
		 * Règles de test : 
		 * - 
		 * - 
		 * - 
		 * 
		 * @return <code>true</code> si le token est valide. <code>false</code> si le token est invalide
		 * 
		 **/
		
		public function verify():Boolean
		{
//			if(...)
				return true;
//			else
//				return false;
		}
/* --------------------------- PRIVATE --------------------------------- */		
		
/* --------------------------- GETTER / SETTER ------------------------- */		
		public function get ID():int
		{
			return _ID;
		}
		public function set ID(value:int):void
		{
			_ID = value;
		}
		public function get UUID():String
		{
			return _UUID;
		}
		public function set UUID(value:String):void
		{
			_UUID = value;
		}
	}
}