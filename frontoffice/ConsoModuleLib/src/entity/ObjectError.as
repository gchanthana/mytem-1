package entity
{
	public class ObjectError
	{
		private var _codeErreur:int;
		private var _type:String;
		private var _message:String;
		private var _detail:String;
		private var _stactTrace:String;
		
		public function ObjectError()
		{
		}
		public function defineObjectError(obj:Object):void
		{
			if(obj.hasOwnProperty("CODEERREUR"))
				this._codeErreur = obj.CODEERREUR;
			else
				this._codeErreur = -999;
			
			if(obj.hasOwnProperty("CFCATCH"))
			{
				if(obj.CFCATCH.hasOwnProperty("TYPE") && obj.CFCATCH.TYPE != null)
					this._type = obj.CFCATCH.TYPE;
				else
					this._type = "";
				
				if(obj.CFCATCH.hasOwnProperty("MESSAGE") && obj.CFCATCH.MESSAGE != null)
					this._message = obj.CFCATCH.MESSAGE;
				else
					this._message = "";
				
				if(obj.CFCATCH.hasOwnProperty("DETAIL") && obj.CFCATCH.DETAIL != null)
					this._detail = obj.CFCATCH.DETAIL;
				else
					this._detail = "";
				
				if(obj.CFCATCH.hasOwnProperty("STACKTRACE") && obj.CFCATCH.STACKTRACE != null)
					this._stactTrace = obj.CFCATCH.STACKTRACE;
				else
					this._stactTrace = "";
			}
			else
			{
				this._type = "";
				this._message = "";
				this._detail = "";
				this._stactTrace = "";
			}
		}
		public function defineDefaultObjectError(codeErreur:int = -999):void
		{
			this._codeErreur = codeErreur;
			this._type = "";
			this._message = "";
			this._detail = "";
			this._stactTrace = "";
		}
		
		
		
		public function get codeErreur():int
		{
			return _codeErreur;
		}
		public function set codeErreur(value:int):void
		{
			_codeErreur = value;
		}
		public function get type():String
		{
			return _type;
		}
		public function set type(value:String):void
		{
			_type = value;
		}
		public function get message():String
		{
			return _message;
		}
		public function set message(value:String):void
		{
			_message = value;
		}
		public function get detail():String
		{
			return _detail;
		}
		public function set detail(value:String):void
		{
			_detail = value;
		}
		public function get stactTrace():String
		{
			return _stactTrace;
		}
		public function set stactTrace(value:String):void
		{
			_stactTrace = value;
		}
	}
}