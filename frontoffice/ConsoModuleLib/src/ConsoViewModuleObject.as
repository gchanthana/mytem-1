package
{
	import composants.ConsoProgressBar.ConsoProgressBar;
	
	import flash.display.DisplayObject;
	import flash.errors.IllegalOperationError;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.ProgressBarMode;
	import mx.core.Application;
	import mx.core.Container;
	import mx.core.UIComponent;
	import mx.events.ModuleEvent;
	import mx.events.StyleEvent;
	import mx.managers.PopUpManager;
	import mx.managers.SystemManager;
	import mx.modules.IModuleInfo;
	import mx.modules.ModuleManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.styles.StyleManager;
	
	[ResourceBundle("ConsoModuleLib")]
	
	public class ConsoViewModuleObject extends EventDispatcher
	{
		public static const NO_STYLE:String = "NO_STYLE";
		public static const DEFAULT_STYLE:String = "consotel";
		public static const MBP_STYLE:String= "OrangeBS";
		public static const CMDMOBILE_STYLE:String = "SFR";
		public static const versionAppli:String = "";
		public static const cvHeight:int = 784;  
		public static const cvWidth:int = 1000; 
		
		public static var H_ratio : Number= 1;
		public static var W_ratio : Number= 1;
		public static const verboseFaultHandler:Boolean = false; // Mode verbose en cas de fault remoting
		public static const REMOTING_TIMEOUT:int = 0; // TimeOut de remoting (secondes). Si 0 alors pas de timeout
		public static const remotingCancelable:Boolean = true; // Booléen cancel de la fenêtre remoting
		public static const MAX_REMOTING_COUNT:int = 10; // Nbre MAX de Remoting simultanés
		public static const AUTO_SELECT_FIRST_FUNCTION:Boolean = false; // Lors Changement Univers - Choisi auto la 1ère fonction (true)
			
		private var functionKey:String="";
		private var login:String="dev@consotel.fr";
		private var mdp:String="dev";
		private var defautUnivers:int=2458788;
		private var code_application:int=1;
		
		public var dispobj:Object=new Object();
		
		private var accessManagerTDB:CvAccessManager;
		private var remoteObjectUtil:RemoteObjectUtil;		
		
		private var styleOp:AbstractOperation;
		private var authOp:AbstractOperation;
		private var groupListOp:AbstractOperation;
		private var connectOnGroupOp:AbstractOperation;
		private var accessDataOp:AbstractOperation;
		
		private static var _singletonInstance:ConsoViewModuleObject;
		public static function singletonInstance():ConsoViewModuleObject
		{	return _singletonInstance }
		
		private var infos:IModuleInfo;	
		private var mainContent:Container;														  
		
 		private var currentUniv:String=null;
		public function get currentUnivers():String{
			return currentUniv;}

 		private var currentFunct:String=null;
		public function get currentFunction():String{
			return currentFunct;}
			
		private var nomModuleInUse:String="";		
		public function get ModuleInUse():String{
			return "module"+nomModuleInUse+"IHM.swf";}
		public function set ModuleInUse(s:String):void{
			nomModuleInUse=s;}
		
		private var progress:ConsoProgressBar;
					
/* ----------------------------------------------------------------------------------------------- */		
/* -------------------------------------- load du module ----------------------------------------- */		
/* ----------------------------------------------------------------------------------------------- */
		
		public function unloadModule():void
		{	
			if(dispobj)
			{	
				mainContent.removeAllChildren();
				dispobj=null;	
			}
			
			if(infos)
			{	
		        infos.removeEventListener(ModuleEvent.ERROR, moduleErrorHandler);
		        infos.removeEventListener(ModuleEvent.SETUP, moduleSetupHandler);
		        infos.removeEventListener(ModuleEvent.READY, moduleReadyHandler);
		        infos.unload();
		        infos=null;
			}
		}
		
		public function loadModule(tmpFunctionKey:String):void{
			var tmpGetModule:AbstractOperation=
				RemoteObjectUtil.getOperation("fr.consotel.consoview.module.ModuleManager",
											  "getConsoviewModule",getConsoViewModuleResultHandler);
				RemoteObjectUtil.invokeService(tmpGetModule,tmpFunctionKey);	
		}
		
		private function getConsoViewModuleResultHandler(e:ResultEvent):void{
			if(e.result != null || (e.result as ArrayCollection).length > 0){
				nomModuleInUse= (e.result as ArrayCollection)[0].FCT;
				infos=ModuleManager.getModule(ModuleInUse);
				if(infos)
				{
					if(dispobj)
					{	
						mainContent.removeAllChildren();
					}
					
					infos.addEventListener(ModuleEvent.ERROR, moduleErrorHandler);
			        infos.addEventListener(ModuleEvent.SETUP, moduleSetupHandler);
			        infos.addEventListener(ModuleEvent.READY, moduleReadyHandler);
			        if(progress)
			       	{
			       		if(!progress.isPopUp)
			       		{
							progress.setSource(infos);
							progress.show(SystemManager.getSWFRoot(progress));
				       	}
			       	}
					else
					{
						progress = new ConsoProgressBar(	
							'Traitement en cours',
							"",
							infos,
							false,
							0,
							0,
							false,
							ProgressBarMode.EVENT,
							false
						);
						progress.show(SystemManager.getSWFRoot(progress));
					}
					infos.load();	
				}
			}else{
				trace('[CONSOVIEW-MODULE] GetConsoViewModule : aucun résultat');
			}
		}

		private function infosReady():void
		{
			
			dispobj=new Object();
			
			progress.hide();
						
			dispobj= infos.factory.create();
			
			if(dispobj)
			{
				dispobj.percentHeight=100;
				dispobj.percentWidth=100;
				mainContent.addChild(dispobj as UIComponent);	
			}
		}
		
		private function moduleErrorHandler(event:ModuleEvent):void
		{	
			trace("!! Erreur de chargement du module !!" );
			trace(event.errorText);
			progress.hide();
		}
		
		private function moduleSetupHandler(event:ModuleEvent):void
		{	
			if (!event.module) return;
		 	
		 	var url:String  = event.module.url;
	        if (url == null) 
	        {       
				progress.hide();
	        } 
	        else 
	        {
	        	if(!progress.isPopUp)
	        	{
					progress.show(SystemManager.getSWFRoot(progress));
		       	}
	        }
		}
		
		private function moduleReadyHandler(event:ModuleEvent):void
		{	
			progress.hide();
	        infosReady();
	 }

		
/* ----------------------------------------------------------------------------------------------- */		
/* -------------------------------------- Function ----------------------------------------------- */		
/* ----------------------------------------------------------------------------------------------- */		
		
		public function getUniversKey():String{
			return CvAccessManager.CURRENT_UNIVERS;
		}
	
		public function getFunctionKey():String{
			return CvAccessManager.CURRENT_FUNCTION;
		}	
		
		public function updateCurrent(currentunivers:String,currentfunction:String):void{
			currentFunct=currentfunction;
			currentUniv=currentunivers;
		}
		
		public function afterPerimetreUpdated():void{
		 	if(dispobj != null){
				dispobj.afterPerimetreUpdated();
			} 
		}
		
		public function logoff():void{
			_singletonInstance = null;
			dispobj=null;
			ModuleInUse ="";
		}

/* ----------------------------------------------------------------------------------------------- */		
/* ------------------------------------- STYLE MANAGER ------------------------------------------- */		
/* ----------------------------------------------------------------------------------------------- */

		private static var currentStyle : String;
		
		public function changeStyle(type:String):void{			
			if (currentStyle != null){
				if (type != currentStyle){
					try{
						StyleManager.unloadStyleDeclarations('css/' + currentStyle +  '.swf');						
					} catch(e : Error) {
					}
					var ed : IEventDispatcher = StyleManager.loadStyleDeclarations( 'css/' + type +  '.swf' );
					ed.addEventListener(StyleEvent.ERROR,myErrorHandler);
				}
			} else {
				if(type == NO_STYLE)
					type = DEFAULT_STYLE;
				var ed2 : IEventDispatcher = StyleManager.loadStyleDeclarations( 'css/' + type +  '.swf' );
				ed2.addEventListener(StyleEvent.ERROR,myErrorHandler);
			}
			currentStyle = type;
		}
		
		private function myErrorHandler(e:StyleEvent):void
		{
			var ed : IEventDispatcher = StyleManager.loadStyleDeclarations( 'css/' + DEFAULT_STYLE +  '.swf' );
			ed.addEventListener(StyleEvent.ERROR,myDefaultErrorHandler);
		}
		private function myDefaultErrorHandler(e:StyleEvent):void
		{
			trace('[ConsoViewModuleObject] unable to load default STYLE');
		}	

/* ----------------------------------------------------------------------------------------------- */		
/* ----------------------------------------- INIT ------------------------------------------------ */		
/* ----------------------------------------------------------------------------------------------- */

		public function initModuleObject():void{
			RemoteObjectUtil.invokeService(styleOp);		
		}

		private function processStyle(e:ResultEvent):void{
			if(e.result!=null){
				changeStyle(e.result as String);
				RemoteObjectUtil.invokeService(authOp,login,mdp);
			}
		}

		private function authHandler(e:ResultEvent):void{
			if(e.result['AUTH_RESULT'] == 1){
				accessManagerTDB.updateUser(e.result as Object);
				RemoteObjectUtil.invokeService(groupListOp,CvAccessManager.getSession().USER.CLIENTACCESSID,code_application);		
			}else{
				trace('Erreur de login/mdp');
			}
		}
		
		public function onGroupListLoaded(e:ResultEvent):void{
			if(e.result != null){
				accessManagerTDB.updateGroupList(e.result);
				var accessId:int = CvAccessManager.getSession().USER.CLIENTACCESSID;
				var groupId:int; 
				if(CvAccessManager.getSession().GROUP_LIST.length > 0)
					groupId = CvAccessManager.getSession().GROUP_LIST.getItemAt(0).IDGROUPE_CLIENT
				else
					groupId = defautUnivers;
				RemoteObjectUtil.invokeService(connectOnGroupOp,accessId,groupId);
			}
		}
		
		public function processGroupPerimetre(e:ResultEvent):void{
			if(e.result != null){
				accessManagerTDB.updatePerimetre(e.result);
				RemoteObjectUtil.invokeService(accessDataOp,null);
			}
		}

		public function processAccessData(e:ResultEvent):void{
			if(e.result != null){
				accessManagerTDB.updateAccess(e.result as XML);
				loadModule(functionKey);
			}
		}

		
/* ----------------------------------------------------------------------------------------------- */		
/* -------------------------------------- Constructeur ------------------------------------------- */		
/* ----------------------------------------------------------------------------------------------- */
	
		public function ConsoViewModuleObject(bx:Container,functionkey:String="",LOGIN:String="", MDP:String="",isModule:Boolean=false,code_app:int=1,isMbp:Boolean=false):void{
			trace("(ConsoViewModuleObject) Singleton Creation");
			functionKey = functionkey;
			login=LOGIN;
			mdp=MDP;
			mainContent=bx;
			this.code_application = code_app;
			_singletonInstance = this;
			if(isModule){
				remoteObjectUtil=new RemoteObjectUtil();
				accessManagerTDB=new CvAccessManager();
				
				if(!isMbp){
				styleOp = RemoteObjectUtil.getOperation("fr.consotel.consoview.access.AccessManager",
																	"getLastCodeStyle",processStyle);
				authOp = RemoteObjectUtil.getOperation("fr.consotel.consoview.access.AccessManager",
																		"validateLogin",authHandler);
				groupListOp = RemoteObjectUtil.getOperation("fr.consotel.consoview.access.AccessManager",
																		"getGroupList",onGroupListLoaded);		
				connectOnGroupOp = RemoteObjectUtil.getOperation("fr.consotel.consoview.access.AccessManager",
																			"connectOnGroup",processGroupPerimetre);												
				accessDataOp = RemoteObjectUtil.getOperation("fr.consotel.consoview.access.AccessManager",
																			"getXmlAccessList",processAccessData);
				if(functionKey != "LOGIN" && functionkey != "MBP")
					initModuleObject();
				else
					loadModule(functionKey);
				}else{
					loadModule(functionKey);
				}
			}
		}
	}
}
