package {
	import entity.AppParams;
	
	import mx.collections.ArrayCollection;
	
	public interface IConsoViewSessionObject {
		function get USER():IConsoViewUserObject;
		
		function get CURRENT_PERIMETRE():IConsoViewPerimetreObject;

		function get CURRENT_ACCESS():int;
		
		function get GROUP_LIST():ArrayCollection;
		
		function get AppPARAMS():AppParams;
		
		function get INFOS_DIVERS():Object;
		function set INFOS_DIVERS(value:Object):void;
		function updateAppParams(params:AppParams):Boolean 
			
		function get CODE_APPLICATION ():Number;
		/*
		function get LISTE_GROUPES():ArrayCollection;
		
		//function get NB_NODES():int;
		
		function get LISTE_PERIMETRES():XMLList;
		

		
		function get CURRENT_UNIVERS():String
		
		function get CURRENT_FAMILLE():String;
		
		function get CURRENT_MODE():int;
		
		function get isRootGroup():Boolean;
		
		function get isClientPv():Boolean;
		
		function get hasOrgaGeo():Boolean;
		
		function get isOrgaGeo():Boolean;
		
		function get IDCLIENTPV():int;
		
		function get CODE_STYLE():String;
		*/
	}
}
