package styles {
	import flash.events.IEventDispatcher;
	
	import mx.events.StyleEvent;
	import mx.styles.StyleManager;
	
	public class ConsoViewStyleManager {
		private static var currentStyle : String;
		public static const NO_STYLE:String = "NO_STYLE";
		public static const DEFAULT_STYLE:String = "MYTEM_DEFAUT";
		public static const SFR_STYLE:String = "SFR";
		
		public static function changeStyle(type:String):void{			
			if (currentStyle != null){
				if (type != currentStyle)
				{
					try
					{
						trace("(ConsoViewStyleManager) Changing current style : " + currentStyle + " to : " + type);
						StyleManager.unloadStyleDeclarations('css/' + currentStyle +  '.swf');						
					}
					catch(e : Error) 
					{
						trace("(ConsoViewStyleManager) Echec tentative unloadStyleDeclarations " + e.getStackTrace()); 	
					}
					var ed : IEventDispatcher = StyleManager.loadStyleDeclarations( 'css/' + type +  '.swf' );
					ed.addEventListener(StyleEvent.ERROR,myErrorHandler);
				}
			} 
			else 
			{
				var ed2 : IEventDispatcher = StyleManager.loadStyleDeclarations( 'css/' + type +  '.swf' );
				ed2.addEventListener(StyleEvent.ERROR,myErrorHandler);
			}
			currentStyle = type;
			trace("(ConsoViewStyleManager) Used Style : " + type);
		}
		private static function myErrorHandler(e:StyleEvent):void
		{
			var ed : IEventDispatcher = StyleManager.loadStyleDeclarations( 'css/' + DEFAULT_STYLE +  '.swf' );
			ed.addEventListener(StyleEvent.ERROR,myDefaultErrorHandler);
		}
		private static function myDefaultErrorHandler(e:StyleEvent):void
		{
			trace('[ConsoviewStyleManager] unable to load default STYLE');
		}
	}
}