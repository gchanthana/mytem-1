package composants.access.perimetre {
	import flash.events.Event;

	public class PerimetreEvent extends Event {
		/*
		public static const PERIMETRE_MODEL_INITIALIZED:String = "PERIMETRE_MODEL INITIALIZED"; // Groupes Racines récupérés
		public static const PERIMETRE_CHANGING_GROUP:String = "CHANGING GROUP"; // Début d'un changement de groupe racine
		public static const CANCEL_PERIMETRE_CHOICE:String = "CANCEL PERIMETRE_CHOICE"; // Annulation du choix du périmètre (ListePerimetresWindow)
		*/
		
		public static const PERIMETRE_GROUP_CHANGED:String = "PERIMETRE_GROUP CHANGED EVENT";
		public static const PERIMETRE_CHILD_RESULT:String = "PERIMETRE CHILD RESULT EVENT";
		public static const NODE_INFOS_RESULT:String = " NODE INFOS RESULT EVENT";
		public static const PERIMETRE_NODE_CHANGED:String = "PERIMETRE NODE CHANGED EVENT";
		
		public function PerimetreEvent(type:String) {
			super(type,true);
		}
	}
}