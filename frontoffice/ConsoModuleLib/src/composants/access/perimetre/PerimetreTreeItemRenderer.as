package composants.access.perimetre {
	import mx.controls.treeClasses.TreeItemRenderer;
	import flash.utils.getQualifiedClassName;

	public class PerimetreTreeItemRenderer extends TreeItemRenderer {
		public function PerimetreTreeItemRenderer() {
			super();
		}
		
        public override function set data(value:Object):void {
			super.data = value;
			if(value != null) {
				if(value.@STC > 0)
	        		setStyle("color","#00BB00");
	   			else
					setStyle("color","#000000");
			}
        }

        protected override function updateDisplayList(unscaledWidth:Number,unscaledHeight:Number):void {       
            super.updateDisplayList(unscaledWidth,unscaledHeight);
			//super.label.text =  "Le libellé";
        }
	}
}
