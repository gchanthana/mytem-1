package composants.access.perimetre {
	import flash.events.IEventDispatcher;
	import mx.controls.treeClasses.ITreeDataDescriptor;

	public interface IPerimetresModel extends IEventDispatcher{
		function updatePerimetre():void;
		
		function getPerimetreTreeDataDescriptor():ITreeDataDescriptor;
		
		function get groupList():Object;
		
		function selectGroup(groupIndex:int):void;
		
		function getSelectedGroupIndex():int;
		
		function getGroupByIndex(groupIndex:int):Object;
		
		function getGroupNodes():Object;
		
		function selectAccessNode(nodeId:int):void;
		
		function getSelectedNodeInfos():Object;
		
		function clearPerimetreModel():void;
	}
}
