package composants.ui
{
	import flash.events.KeyboardEvent;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	
	import mx.containers.TitleWindow;
	import mx.core.Application;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.events.MoveEvent;
	import mx.managers.PopUpManager;
	
	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe dont heritent les classes générant une popup ou une fiche.
	 * Permet aux fenêtres affichés d'éviter de sortir des limites de la fenêtre Application
	 
	 * Version : 1.0
	 * Date : 29.05.2013
	 * 
	 * </pre></p>
	 */
	public class TitleWindowBounds extends TitleWindow
	{
		
		private var application			: UIComponent 	= Application.application as UIComponent;
		private var applicationBounds	: Rectangle 	= new Rectangle(0, 0, application.width, application.height);
		private var leftCornerMin		: Number 		= 0;
		private var leftCornerMax		: Number 		= 0;
		private var topCornerMin		: Number 		= 0;
		private var topCornerMax		: Number 		= 0;
		
		public function TitleWindowBounds()
		{
			super();
			this.setFocus();
			this.addEventListener(MoveEvent.MOVE,intoBoundsWindowHandler);
			this.addEventListener(KeyboardEvent.KEY_DOWN,keyDownToCloseHandler);
			this.addEventListener(CloseEvent.CLOSE,closeWindowHandler);
		}
		//** provoquer la fermeture de la popup en appuyant sur ESC **//
		private function keyDownToCloseHandler(evt:KeyboardEvent):void
		{
			//trace("-------------------  key down");
			if (evt.charCode == Keyboard.ESCAPE)
			{
				this.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
			}
		}
		//** fermer la popup **//
		private function closeWindowHandler(evt:CloseEvent):void
		{
			//trace("-------------------  close window");
			PopUpManager.removePopUp(evt.target as IFlexDisplayObject);
		}
		//** calculer le positionnement de la popup **//
		private function intoBoundsWindowHandler(evt:MoveEvent):void
		{
			//trace("-------------------  déplacement window");
			// position des extremités de la fenetre par rapport à l'application
			var window:UIComponent = evt.currentTarget as UIComponent;
			var windowBounds:Rectangle = window.getBounds(application);
			var top:Number = windowBounds.top;
			var left:Number = windowBounds.left;
			var right:Number = windowBounds.right;
			var bottom:Number = windowBounds.bottom;
			
			var x:Number;
			var y:Number;
			
			// variables determinant le ratio minimum visible de la fenetre à afficher
			var visibleWindowWidthMin:Number = (windowBounds.width/4);
			var visibleWindowHeightMin:Number = (windowBounds.height/4);
			
			// variables concernant le positionnement maximum en deplacement
			if(leftCornerMin == 0) leftCornerMin = 0 - (windowBounds.width - visibleWindowWidthMin);
			if(leftCornerMax == 0) leftCornerMax = applicationBounds.width - visibleWindowWidthMin;
			if(topCornerMin == 0) topCornerMin = applicationBounds.top;
			if(topCornerMax == 0) topCornerMax = applicationBounds.height - visibleWindowHeightMin;
			
			// axe des abscisses
			if(left > leftCornerMin && left < leftCornerMax)
			{
				x = window.x; // position x de la fenetre
			}
			else if(left <= leftCornerMin) // limite vers la gauche
			{
				x = leftCornerMin; // position x de la fenetre
			}
			else if(left >= leftCornerMax) // limite vers la droite
			{
				x = leftCornerMax; // position x de la fenetre
			}
			
			// axe des ordonnées
			if(top > topCornerMin && top < topCornerMax)
			{
				y = window.y; // position y de la fenetre
			}
			else if(top >= topCornerMax) // limite vers le bas
			{
				y = topCornerMax; // position y de la fenetre
			}
			else if(top <= topCornerMin) // limite vers le haut
			{
				y = topCornerMin; // position y de la fenetre
			}
			
			window.move(x, y);
		}
	}
}