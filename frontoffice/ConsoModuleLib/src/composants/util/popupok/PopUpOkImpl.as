package composants.util.popupok
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.containers.VBox;
	import mx.events.EffectEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;

	[Bindable]
	public class PopUpOkImpl extends VBox
	{
		public var message:String;
		public var duration:int = 1000
			
		private var timer:Timer;
			
		public function PopUpOkImpl()
		{
			super();
			timer = new Timer(duration,1);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, _timerTimerCompleteHandler);
			addEventListener(EffectEvent.EFFECT_END, _localeEffectEndHandler);			
			addEventListener(FlexEvent.CREATION_COMPLETE, _localeCreationCompleteHandler);
		}
		

		protected function _localeEffectEndHandler(event:EffectEvent):void
		{
			
			PopUpManager.removePopUp(this);
		}

		protected function _timerTimerCompleteHandler(event:TimerEvent):void
		{
			timer.stop();
			timer.removeEventListener(TimerEvent.TIMER_COMPLETE,_timerTimerCompleteHandler);
			timer = null;
			visible = false;
						
		}

		protected function _localeCreationCompleteHandler(event:FlexEvent):void
		{
			timer.start();
		}
	}
}