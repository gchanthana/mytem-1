package composants.util
{
    import mx.formatters.CurrencyFormatter;
    import mx.formatters.NumberBaseRoundType;
    import mx.formatters.NumberFormatter;
    import mx.formatters.PhoneFormatter;
    import mx.resources.ResourceManager;
    import mx.resources.ResourceBundle;

	[ResourceBundle("ConsoModuleLib")] 
	
    public class ConsoviewFormatter
    {
        //formate le numero de télé98phone
        public static function formatPhoneNumber(num:String):String
        {
            if (num != null)
            {
                var pf:PhoneFormatter = new PhoneFormatter();
                pf.formatString = ResourceManager.getInstance().getString('ConsoModuleLib', 'PHONE_FORMAT');
                pf.areaCode = -1;
                if (pf.format(num) == "")
                {
                    var formatedString:String = num;                    
                    return formatedString;
                }
                else
                {
                    return pf.format(num);
                }
            }
            else
                return " ";
        }

        public static function formatSimplePhoneNumber(num:String):String
        {
            var pf:PhoneFormatter = new PhoneFormatter();
            pf.formatString = ResourceManager.getInstance().getString('ConsoModuleLib', 'PHONE_FORMAT');
            pf.areaCode = -1;
            if (pf.format(num) == "")
            {
                return num
            }
            else
            {
                return pf.format(num);
            }
        }

        //formate la monnaie avec la précision passée en parametre	
        public static function formatEuroCurrency(num:Number, precision:uint):String
        {
            var cf:CurrencyFormatter = new CurrencyFormatter();
            cf.precision = ResourceManager.getInstance().getString('ConsoModuleLib', 'CURRENCY_PRECISION');
            cf.rounding = NumberBaseRoundType.NONE;
            cf.decimalSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'DECIMAL_SEPARATOR');
            cf.thousandsSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'THOUSANDS_SEPARATOR');
            cf.useThousandsSeparator = true;
            cf.useNegativeSign = true;
            cf.alignSymbol = ResourceManager.getInstance().getString('ConsoModuleLib', 'ALIGN_SYMBOL');
            cf.currencySymbol = ResourceManager.getInstance().getString('ConsoModuleLib', '_signe_de_la_devise_');
            return cf.format(num);
        }

        //formate le pourcentage avec zero ou un chiffre apres la virgule
        public static function formatePourcent(num:Number):String
        {
            var nf:NumberFormatter = new NumberFormatter();
            nf.rounding = NumberBaseRoundType.NONE;
            nf.decimalSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'DECIMAL_SEPARATOR');
            nf.thousandsSeparatorTo = " ";
            nf.useThousandsSeparator = true;
            nf.useNegativeSign = true;
            if (Math.round(num) == num)
            {
                nf.precision = 0;
            }
            else
            {
                nf.precision = 1;
            }
            return nf.format(num);
        }

        //formtae un chiffre avec la precision passée en parametre
        public static function formatNumber(num:Number, precision:int):String
        {
            var nf:NumberFormatter = new NumberFormatter();
            var v_abs:Number = Math.round(num);
            if (v_abs == num)
            {
                nf.precision = 0;
            }
            else
            {
                nf.precision = precision;
            }
            nf.rounding = NumberBaseRoundType.NONE;
            nf.decimalSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'DECIMAL_SEPARATOR');
            nf.thousandsSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'THOUSANDS_SEPARATOR');
            nf.useThousandsSeparator = true;
            nf.useNegativeSign = true;
            return nf.format(num);
        }
    }
}