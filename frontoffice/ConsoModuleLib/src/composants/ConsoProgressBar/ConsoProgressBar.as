package composants.ConsoProgressBar
{
	import composants.ConsoProgressBar.CV.CVProgressBar;
	import composants.ConsoProgressBar.CV.CVProgressBarIHM;
	import composants.ConsoProgressBar.PFGP.PFGPProgressBar;
	
	import flash.display.DisplayObject;
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.containers.Box;
	import mx.controls.ProgressBar;
	import mx.core.IFlexDisplayObject;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.managers.PopUpManagerChildList;
	import mx.managers.SystemManager;
	import mx.skins.Border;
	
	public class ConsoProgressBar extends EventDispatcher implements IConsoProgressBar
	{
		private var IsPopup:Boolean = false;
		private var progressBarToDisplay:IConsoProgressBar;
		
		public function get isPopUp():Boolean
		{
			return IsPopup;
		}
		
		public function ConsoProgressBar(titre:String, message:String, source:Object, 
										 indeterminate:Boolean, max:int, min:int, 
										 isBtAnnulervisible:Boolean,mode:String,
										 cancelable:Boolean, forcedApplication:Number = NaN)
		{
			var code_app:int;
			
			if(isNaN(forcedApplication))
			{
				code_app = CvAccessManager.getUserObject().CODEAPPLICATION;
			}
			else
			{
				code_app = forcedApplication;
			}
			switch(code_app)
			{
				// progress Bar CV	
				case 1:
					progressBarToDisplay = new CVProgressBar(titre,message,source,indeterminate,max,min,isBtAnnulervisible,mode,cancelable);
					break;
				// progress Bar PFGP
				case 51:
					progressBarToDisplay = new PFGPProgressBar();
					break;
				// progress Bar MyTEM360
				case 101:
					progressBarToDisplay = new CVProgressBar(titre,message,source,indeterminate,max,min,isBtAnnulervisible,mode,cancelable);
					break;
				// progress Bar par défaut : CV
				default:
					progressBarToDisplay = new CVProgressBar(titre,message,source,indeterminate,max,min,isBtAnnulervisible,mode,cancelable);
					break;
			}
		}
		public function show(dispObject:DisplayObject, boolCentrer:Boolean = true, boolModal:Boolean =true):void
		{
			if(dispObject != null)
			{
				PopUpManager.addPopUp(progressBarToDisplay as IFlexDisplayObject,dispObject,boolModal,PopUpManagerChildList.POPUP);
				IsPopup = true;
				if(boolCentrer)
					PopUpManager.centerPopUp(progressBarToDisplay as IFlexDisplayObject);
			}
			else
			{
				throw new IllegalOperationError("[ConsoProgressBar] DispObject n'est pas défini.");
				IsPopup = false;
			}
		}
		public function hide():void
		{
			if(progressBarToDisplay != null && IsPopup)
			{
				PopUpManager.removePopUp(progressBarToDisplay as IFlexDisplayObject);
				IsPopup = false;
			}
		}
		public function setTitle(titre:String):void
		{
			progressBarToDisplay.setTitle(titre);
		}
		public function setProgressBar(byteLoaded:int,bytetotal:int):void
		{
			progressBarToDisplay.setProgressBar(byteLoaded,bytetotal);
		}
		public function setMessage(message:String):void
		{
			progressBarToDisplay.setMessage(message);
		}
		public function setCancelableItem(cancelable:Boolean):void
		{
			progressBarToDisplay.setCancelableItem(cancelable);
		}
		public function setSource(source:Object):void
		{
			progressBarToDisplay.setSource(source);
		}
	}
}