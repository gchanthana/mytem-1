package composants.ConsoProgressBar.CV
{
	import composants.ConsoProgressBar.ConsoProgressBarEvent;
	import composants.ConsoProgressBar.IConsoProgressBar;
	
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.containers.Panel;
	
	public class CVProgressBar extends CVProgressBarIHM implements IConsoProgressBar
	{
		import mx.events.FlexEvent;
		private var indeterminate:Boolean;
		private var maximum:int;
		private var minimum:int;
		private var titre:String;
		private var message:String;
		private var source:Object;
		private var isBtAnnulerVisible:Boolean;
		private var mode:String;
		private var isBtAnnulerCancelable:Boolean;
		
		public function CVProgressBar(titre:String, message:String, source:Object, 
									  indeterminate:Boolean, max:int, min:int, 
									  isBtAnnulervisible:Boolean,mode:String,
									  cancelable:Boolean)
		{
			super();
			this.titre = titre;
			this.message = message;
			this.source = source;
			this.indeterminate = indeterminate;
			this.maximum = max;
			this.minimum = min;
			this.isBtAnnulerVisible = isBtAnnulervisible;
			this.mode = mode; 
			setCancelableItem(cancelable);
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		private function init(e:FlexEvent):void
		{
			this.title = titre;
			progress.mode = mode;
			progress.label = message;
			progress.source = source;
			progress.indeterminate = indeterminate;
			progress.maximum = maximum;
			progress.minimum = minimum;
			cancelBtn.visible = isBtAnnulerVisible;
			cancelBtn.enabled = isBtAnnulerCancelable;
			cancelBtn.addEventListener(MouseEvent.CLICK,cancel);
		}
		private function cancel(evt:MouseEvent):void
		{
			RemoteObjectUtil.cancelRemoting();
		}
		public function setTitle(titre:String):void
		{
			this.titre = titre;
		}
		public function setProgressBar(byteLoaded:int,bytetotal:int):void
		{
			if(progress != null)
			{
				progress.setProgress(byteLoaded,bytetotal);
				progress.label = int((byteLoaded / bytetotal) * 100)+' %';
			}
		}
		public function setMessage(msg:String):void
		{
			this.message = msg;
			if(progress)
				progress.label = msg;
		}
		public function setCancelableItem(cancelable:Boolean):void
		{
			this.isBtAnnulerCancelable = cancelable;
			if(cancelBtn)
				cancelBtn.enabled = cancelable;
		}
		public function setSource(source:Object):void
		{
			this.source = source;
			if(progress)
				progress.source = source;
		}
	}
}