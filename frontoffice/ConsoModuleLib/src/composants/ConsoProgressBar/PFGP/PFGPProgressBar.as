package composants.ConsoProgressBar.PFGP
{
	import composants.AnimatedGifPlayer.player.GIFPlayer;
	import composants.ConsoProgressBar.ConsoProgressBarEvent;
	import composants.ConsoProgressBar.IConsoProgressBar;
	
	import flash.net.URLRequest;
	
	import mx.events.FlexEvent;
	
	public class PFGPProgressBar extends PFGPProgressBarIHM implements IConsoProgressBar
	{
		public function PFGPProgressBar()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		private function init(e:FlexEvent):void
		{
			var request:URLRequest = new URLRequest("https://cache-pilotage.sfrbusinessteam.fr/assets/patienter_neuf.gif");
			
			var player:GIFPlayer = new GIFPlayer();
			player.load(request);
			
			spr.source =player;
		}
		public function setTitle(titre:String):void
		{
		}
		public function setProgressBar(byteLoaded:int, bytetotal:int):void
		{
		}
		public function setMessage(msg:String):void
		{
		}
		public function setCancelableItem(cancelable:Boolean):void
		{
		}
		public function setSource(source:Object):void
		{
		}
	}
}