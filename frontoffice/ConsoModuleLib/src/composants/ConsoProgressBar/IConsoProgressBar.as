package composants.ConsoProgressBar
{
	import flash.events.IEventDispatcher;

	public interface IConsoProgressBar
	{
		function setProgressBar(byteLoaded:int,bytetotal:int):void
		function setMessage(message:String):void
		function setCancelableItem(cancelable:Boolean):void
		function setSource(source:Object):void
		function setTitle(titre:String):void
	}
}