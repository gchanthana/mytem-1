package interfaceClass
{
	import appli.events.LoginEvent;
	
	import entity.AppParams;
	import entity.CodeLangue;
	
	import mx.core.Container;

	public interface IConnection
	{
		function goLogin(param:entity.AppParams,container:Container = null):void
		function MustBeDisplay():Boolean
		function CreateSession(evt:LoginEvent):void
		function validateLogin(objLogin:Object):void
		function changePerimetre(idPerimetre:int):void
		function changeGroupe(idPerimetre:int):void
		function deconnect(param:AppParams, cont:Container=null):void
		function setMessageErreur(msg:String):void
	}
}