package interfaceClass
{
	

	public interface IModuleLogin
	{
		function displayError(str:String):void;
		function validateLogin(objLogin:Object):void;
	}
}

