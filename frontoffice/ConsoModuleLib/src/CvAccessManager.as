package {
	import appli.control.AppControlEvent;
	
	import composants.access.perimetre.IPerimetreDataProxy;
	import composants.access.perimetre.PerimetreDataProxy;
	
	import entity.ObjectError;
	
	import flash.errors.IllegalOperationError;
	import flash.events.EventDispatcher;
	
	import mx.controls.treeClasses.ITreeDataDescriptor;
	
	public class CvAccessManager extends EventDispatcher {
		private var sessionObject:ConsoViewSessionObject;
		private var treeDataDescriptor:ITreeDataDescriptor;
		private var perimetreDataProxy:IPerimetreDataProxy;
		private var universMenuData:XML; // Data du menu principal
		private var universMenuData2:XML; // Data du menu principal
		public static const DEFAULT_UNIVERS_KEY:String = "HOME";
		public static const DEFAULT_UNIVERS_KEY_PFGP:String = "HOME_PILOTAGE";
		private static var _singletonInstance:CvAccessManager = null;

		public static function getPerimetreDataProxy():IPerimetreDataProxy{
			return singletonInstance.perimetreDataProxy;
		}
		public function CvAccessManager() {
			if(CvAccessManager._singletonInstance == null) {
				sessionObject = new ConsoViewSessionObject();
				perimetreDataProxy = new PerimetreDataProxy();
				treeDataDescriptor = null;
				CvAccessManager._singletonInstance = this;
			} else
				throw new IllegalOperationError("[CV-ERROR](CvAccessManager) Singleton Creation Error");
		}
		public static function getUserObject():ConsoViewUserObject{
			return CvAccessManager._singletonInstance.sessionObject.USER as ConsoViewUserObject;
		}
		public static function getSession():IConsoViewSessionObject {
			return CvAccessManager._singletonInstance.sessionObject;
		}
		public static function get CURRENT_UNIVERS():String {
			var accessData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			var selectedUniversNodeList:XMLList = accessData.UNIVERS.(@toggled == 'true');
			if(selectedUniversNodeList.length() == 1) {
				return selectedUniversNodeList[0].@KEY;
			} else {
				throw new IllegalOperationError("[CV-ERROR](CvAccessManager) Invalid Menu Data Provider - No toggled univers node");
				return CvAccessManager.DEFAULT_UNIVERS_KEY;
			}
		}
		public static function get CURRENT_FUNCTION():String {
			var accessData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			var testnode:XMLList = accessData.descendants("UNIVERS").(@toggled == 'true');
			if(testnode.length() == 1 && testnode.@SYS > 0 )
			{
				return testnode[0].@KEY;
			}
			else
			{
				var selectedFunctionNodeList:XMLList = accessData.descendants("FONCTION").(@toggled == 'true');
				if(selectedFunctionNodeList.length() == 1)
					return selectedFunctionNodeList[0].@KEY;
				else
					return null;
			}
		}
		public static function get CURRENT_UNIVERS_FUNCTION_LABEL():String {
			var accessData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			var tmpUniversList:XMLList = accessData.descendants("UNIVERS").(@KEY == CvAccessManager.CURRENT_UNIVERS);
			if(tmpUniversList.length() == 1) {
				if(CvAccessManager.CURRENT_UNIVERS != CvAccessManager.DEFAULT_UNIVERS_KEY) {
					var tmpFunctionList:XMLList =
							accessData.descendants("FONCTION").(@KEY == CvAccessManager.CURRENT_FUNCTION);
					if(tmpFunctionList.length() == 1)
						return tmpUniversList[0].@LBL + "/" + tmpFunctionList[0].@LBL;
					else
						{
							if(tmpUniversList[0].@KEY == "GEST_CONT")
							{
								return tmpUniversList[0].@LBL 
							}
							else
							{
								return tmpUniversList[0].@LBL + "/ERROR";
							}
						}
				} else { // DEFAULT Univers
					return tmpUniversList[0].@LBL;
				}
			} else
				return "ERROR";
		}
		
		public function updateUser(userInfos:Object):Boolean {
			return _singletonInstance.sessionObject.updateUserInfos(userInfos);
		}
		public function updateGroupList(groupList:Object):Boolean {
			return sessionObject.updateGroupList(groupList);
		}
		public function updateCodeApplication(codeApplication:Number):void
		{
			return sessionObject.updateCodeApplication(codeApplication);
		}
//		public function setGroupIndex(groupIdx:int):Boolean {
//			return sessionObject.setGroupIndex(groupIdx);
//		}
//		public function setTypeLogin(typelogin:int):Boolean {
//			return sessionObject.setTypeLogin(typelogin);
//		}
		public function updatePerimetre(perimetreInfos:Object):Boolean {
			return sessionObject.updatePerimetre(perimetreInfos);
		}
		public function updateAccess(accessInfos:XML):Boolean {
			return sessionObject.updateAccess(accessInfos);
		}
		public function updatePerimetreXML(perimetreInfos:XML):Boolean {
			return sessionObject.updatePerimetreXML(perimetreInfos);
		}
//		public function updatePerimetreFromNode(perimetreNodeInfos:Object):Boolean {
//			return sessionObject.updatePerimetreFromNode(perimetreNodeInfos);
//		}
		public function setDisplayDateDeb(dt:Date):void {
			sessionObject.setDisplayDateDeb(dt);
		}
		public function setDisplayDateFin(dt:Date):void {
			sessionObject.setDisplayDateFin(dt);
		}
		public function setMaxMonthToDisplay(maxMonth:Number):void {
			sessionObject.setMaxMonthToDisplay(maxMonth);
		}
		
		public function logoff():Boolean {
			treeDataDescriptor = null;
			sessionObject.clearSession();
			dispatchEvent(new AppControlEvent(AppControlEvent.LOGOFF_EVENT,true));
			return true;
		}
		public static function get singletonInstance():CvAccessManager
		{
			return _singletonInstance;
		}
		public static function deconnectSession():void
		{
			_singletonInstance.treeDataDescriptor = null;
			_singletonInstance.sessionObject.clearSession();
			_singletonInstance.dispatchEvent(new AppControlEvent(AppControlEvent.LOGOFF_EVENT,true));
		}
		public static function changePerimetre(idPerimetre:int):void
		{
			singletonInstance.dispatchEvent(new AppControlEvent(AppControlEvent.CHANGE_PERIMETRE_EVENT,idPerimetre));
		}
		public static function changeGroupe(idPerimetre:int):void
		{
			singletonInstance.dispatchEvent(new AppControlEvent(AppControlEvent.CHANGE_GROUPE_EVENT,idPerimetre));
		}
		public static function changeModule(key:String = DEFAULT_UNIVERS_KEY):void
		{
			if(key == null || key == "")
				key = DEFAULT_UNIVERS_KEY;
			singletonInstance.dispatchEvent(new AppControlEvent(AppControlEvent.CHANGE_MODULE_EVENT,key));
		}
		public static function newError(objError : ObjectError ) :void
		{
			singletonInstance.dispatchEvent(new AppControlEvent(AppControlEvent.NEW_ERROR_EVENT,objError));
		}
	}
}
