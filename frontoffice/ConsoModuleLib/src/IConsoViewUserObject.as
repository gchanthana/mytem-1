package {
	public interface IConsoViewUserObject 
	{
		function get NOM():String;
		
		function get PRENOM():String;
		
		function get EMAIL():String;
		
		function get CLIENTACCESSID():int;
		
		function get CODEAPPLICATION():int;
		
		function get GLOBALIZATION():String;
		
		function get CLIENTID():Number;
	}
}
