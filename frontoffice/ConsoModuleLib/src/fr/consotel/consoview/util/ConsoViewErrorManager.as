package fr.consotel.consoview.util {
	import flash.events.Event;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import flash.events.EventDispatcher;
	import flash.errors.IllegalOperationError;
	import mx.utils.ObjectUtil;

	public class ConsoViewErrorManager {
		private static var remotingErrorManager:EventDispatcher = null;
		public static const BACKOFFICE_REMOTING_EXCEPTION:String = "Exception BackOffice";
		public static const BACKOFFICE_SESSION_TIMEOUT:String = "BackOffice Session Timeout";
		public static const BACKOFFICE_SESSION_TIMEOUT_ERR_CODE:int = 100;
		public static const ADMIN_SESSION_DISCONNECT:String = "BackOffice Session Disconnected By Admin";
		public static const ADMIN_SESSION_DISCONNECT_ERR_CODE:int = 101;
		public static const DATABASE_VALUE_CHANGED:String = "Valeur Base Modifiée";
		public static const DATABASE_VALUE_CHANGED_ERR_CODE:int = 102;
		public static const REMOTING_SET_ERROR_MANAGER_ERROR_MSG:String =
					"ConsoViewErrorManager.setRemotingErrorManager() déjà effectué par : ";
		
		public static function setRemotingErrorManager(managerObject:EventDispatcher):void {
			if(ConsoViewErrorManager.remotingErrorManager == null)
				ConsoViewErrorManager.remotingErrorManager = managerObject;
			else
				throw new IllegalOperationError("(" + managerObject.toString() + ") " +
										ConsoViewErrorManager.REMOTING_SET_ERROR_MANAGER_ERROR_MSG +
										ConsoViewErrorManager.remotingErrorManager.toString());
		}
		
		public static function onFaultEvent(faultEvent:FaultEvent):void {
			Alert.show("ConsoViewErrorManager (SWC):\n" + faultEvent.toString());
		}
		
		public static function processRemotingFaultEvent(faultEvent:FaultEvent):void {
			var errorCode:int = parseInt(faultEvent.fault.faultString,10);
			if(errorCode == ConsoViewErrorManager.BACKOFFICE_SESSION_TIMEOUT_ERR_CODE) {
				ConsoViewErrorManager.remotingErrorManager.dispatchEvent(new Event(ConsoViewErrorManager.BACKOFFICE_SESSION_TIMEOUT));
			} else if(errorCode == ConsoViewErrorManager.ADMIN_SESSION_DISCONNECT_ERR_CODE) {
				ConsoViewErrorManager.remotingErrorManager.dispatchEvent(new Event(ConsoViewErrorManager.ADMIN_SESSION_DISCONNECT));
			} else {
				var tmpFaultEvent:FaultEvent =
						new FaultEvent(ConsoViewErrorManager.BACKOFFICE_REMOTING_EXCEPTION,false,true,faultEvent.fault);
				ConsoViewErrorManager.remotingErrorManager.dispatchEvent(tmpFaultEvent);
			}
		}
	}
}
