package fr.consotel.consoview.util.xml {
	import flash.utils.getQualifiedClassName;
	import flash.utils.getDefinitionByName;
	import flash.errors.IllegalOperationError;
	import mx.formatters.DateFormatter;
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;

	/** Structure d'un objet liste de param�tres XML
		<?xml version="1.0" encoding="utf-8" ?>
		<!-- Liste des param�tres requis par la m�thode -->
		<PARAMS>
			<!-- Chaque balise PARAM repr�sente un param�tre (au sens cfargument : Donc identifi� par un cl� unique) -->
			<PARAM>
				<KEY>IDTETE_LIGNE</KEY>
				<DESC>T�te de ligne</DESC>
				<!-- Utilisation du param�tre envoy� : METHOD (Utilis� par la m�thode),
				 LOG (Utilis� dans les logs), ALL (Utilis� dans les 2) -->
				<USE>METHOD</USE>
				<!-- Liste des valeurs du param�tre : ici identifi� par IDTETE_LIGNE -->
				<VALUES>
					<!-- Une a plusieurs valeurs DU MEME TYPE (entier, string, date) -->
					<VALUE>10351</VALUE>
				</VALUES>
			</PARAM>
			<PARAM>
				<KEY>LIST_LIGNES</KEY>
				<DESC>Liste des lignes de la t�te</DESC>
				<USE>METHOD</USE>
				<VALUES>
					<VALUE>10352</VALUE>
					<VALUE>10353</VALUE>
				</VALUES>
			</PARAM>
		</PARAMS>
	 * */

	public class XmlParamUtil {
		public static const METHOD_USE_TYPE:String = "METHOD";
		public static const LOG_USE_TYPE:String = "LOG";
		public static const ALL_USE_TYPE:String = "ALL";
		
		public static const NOT_REGISTER_PARAM_IN_LOG:int = 0;
		public static const REGISTER_PARAM_IN_LOG:int = 1;
		
		private static var dateFormater:DateFormatter = null;

		/**
		 * Cr�e un objet liste de param�tres XML vide (Donc avec z�ro param�tres).
		 * */
		public static function createXmlParamObject(registerInLog:int=XmlParamUtil.NOT_REGISTER_PARAM_IN_LOG):XML {
			XmlParamUtil.initDateFormater();
			return <PARAMS><REGISTER_IN_LOG>{registerInLog}</REGISTER_IN_LOG></PARAMS>;
		}

		/**
		 * Ajoute un param�tre � la liste de param�tres XML xmlParamObject pass�e en param�tre
		 * @xmlParamObject : Liste de param�tres XML � laquelle on ajoute le param�tre
		 * @paramKEY : Cl� identifiant le param�tre ajout� (balise KEY)
		 * @paramDESC : Description du param�tre ajout� (balise DESC)
		 * @paramVALUE : Valeur du param�tre (contenu de la balise VALUES)
		 * Une erreur de type IllegalOperationError est lev�e lorsque le type du param�tre paramVALUE n'est pas support�
		 * La liste des types support�s : int, String, Date, Array
		 * Une erreur de type IllegalOperationError est lev�e lorsque le param�tre paramVALUE est un tableau (Array) vide
		 * Une erreur de type IllegalOperationError est lev�e lorsqu'un param�tre est nul (valeur : null)
		 * */
		public static function addParam(xmlParamObject:XML,paramKEY:String,paramDESC:String,paramVALUE:Object,
										byPassIfNull:Boolean=false,useType:String=XmlParamUtil.METHOD_USE_TYPE):void {
			if((paramVALUE is int) || (paramVALUE is String) || (paramVALUE is Date)) {
				XmlParamUtil.addScalarTypeParam(xmlParamObject,paramKEY,paramDESC,paramVALUE,byPassIfNull,useType);
			} else if(paramVALUE is Array) {
				XmlParamUtil.addArrayParam(xmlParamObject,paramKEY,paramDESC,(paramVALUE as Array),byPassIfNull,useType);
			} else if(paramVALUE is ArrayCollection) {
				XmlParamUtil.addCollectionParam(xmlParamObject,paramKEY,paramDESC,(paramVALUE as ArrayCollection),byPassIfNull,useType);
			} else if(paramVALUE == null) {
				XmlParamUtil.addScalarTypeParam(xmlParamObject,paramKEY,paramDESC,null,byPassIfNull,useType);
			} else
				XmlParamUtil.unknownType(paramKEY,paramVALUE);
		}

		private static function initDateFormater():Boolean {
			if(XmlParamUtil.dateFormater == null) {
				XmlParamUtil.dateFormater = new DateFormatter();
				XmlParamUtil.dateFormater.formatString = "YYYY/MM/DD";
				return false;
			} else
				return true;
		}

		private static function createParamNode(paramKEY:String,paramDESC:String,useType:String=XmlParamUtil.METHOD_USE_TYPE):XML {
			var xmlNode:XML =
				<PARAM>
					<KEY>{paramKEY}</KEY>
					<DESC>{paramDESC}</DESC>
					<USE>{useType}</USE>
					<VALUES></VALUES>
				</PARAM>;
			return xmlNode;
		}

		private static function unknownType(paramKEY:String,paramVALUE:Object):void {
			throw new IllegalOperationError("XmlParamUtil Add Param - Unsupported type : " + paramKEY);
		}

		private static function addScalarTypeParam(xmlParamObject:XML,paramKEY:String,paramDESC:String,paramVALUE:Object,
													byPassIfNull:Boolean=false,useType:String=XmlParamUtil.METHOD_USE_TYPE):void {
			var xmlNode:XML = XmlParamUtil.createParamNode(paramKEY,paramDESC,useType);
			var valueNode:XML = null;
			var tmpValue:Object = null;
			if(paramVALUE == null) { // Value is null
				valueNode = (byPassIfNull == false) ? <VALUE></VALUE> : null;
			} else { // Value is NOT null
				tmpValue = (paramVALUE is Date) ? XmlParamUtil.dateFormater.format(paramVALUE) : paramVALUE;
				valueNode = <VALUE>{tmpValue}</VALUE>;
			}
			if(valueNode != null) {
				(xmlNode.VALUES[0] as XML).appendChild(valueNode);
				xmlParamObject.appendChild(xmlNode);
			}
		}

		private static function addArrayParam(xmlParamObject:XML,paramKEY:String,paramDESC:String,paramVALUE:Array,
												byPassIfNull:Boolean=false,useType:String=XmlParamUtil.METHOD_USE_TYPE):void {
			if(paramVALUE.length > 0) {
				var xmlNode:XML = XmlParamUtil.createParamNode(paramKEY,paramDESC,useType);
				var valueNode:XML = null;
				var tmpValue:Object = null;
				var i:int;
				for(i=0; i < paramVALUE.length; i++) {
					if(paramVALUE[i] == null) { // Current row is null
						if(byPassIfNull == false)
							valueNode = <VALUE></VALUE>;
					} else { // Current row is NOT null
						if((paramVALUE[i] is int) || (paramVALUE[i] is String) || (paramVALUE[i] is Date)) { // Current row is a scalar value
							tmpValue = (paramVALUE[i] is Date) ? XmlParamUtil.dateFormater.format(paramVALUE[i]) : paramVALUE[i];
							valueNode = <VALUE>{tmpValue}</VALUE>;
						} else if(paramVALUE[i] is Object) { // Current row has an Object type value
							if(paramVALUE[i][paramKEY] == null) { // Current row (Object) key has null value
								if(byPassIfNull == false)
									valueNode = <VALUE></VALUE>;
							} else { // Current row (Object) key has NON null value
								tmpValue = (paramVALUE[i][paramKEY] is Date) ? XmlParamUtil.dateFormater.format(paramVALUE[i][paramKEY]) : paramVALUE[i][paramKEY];
								valueNode = <VALUE>{tmpValue}</VALUE>;
							}
						} else // Current row has an unsupported value type
							XmlParamUtil.unknownType(paramKEY,paramVALUE[i]);
					}
					if(valueNode != null)
						(xmlNode.VALUES[0] as XML).appendChild(valueNode);
					valueNode = null;
					tmpValue = null;
				}
				xmlParamObject.appendChild(xmlNode);
			} else
				throw new IllegalOperationError("XmlParamUtil Add Array Param - EMPTY ARRAY : " + paramKEY);
		}
		
		private static function addCollectionParam(xmlParamObject:XML,paramKEY:String,paramDESC:String,paramVALUE:ArrayCollection,
													byPassIfNull:Boolean=false,useType:String=XmlParamUtil.METHOD_USE_TYPE):void {
			var listValue:Array = paramVALUE.source;
			XmlParamUtil.addArrayParam(xmlParamObject,paramKEY,paramDESC,listValue,byPassIfNull,useType);
		}
	}
}
