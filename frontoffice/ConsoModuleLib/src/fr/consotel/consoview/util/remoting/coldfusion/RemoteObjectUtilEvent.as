package fr.consotel.consoview.util.remoting.coldfusion {
	import flash.events.Event;

	public class RemoteObjectUtilEvent extends Event {
		public static const SUCCESS_CONNECT:String = "RemoteObjectUtil SUCCESS AUTH"; // AUTH autorisé
		public static const FAILED_CONNECT:String = "RemoteObjectUtil FAILED AUTH"; // AUTH non autorisé
		public static const CONNECT_FAULT:String = "RemoteObjectUtil CONNECT FAULT"; // FAULT du remoting de connexion
		
		public static const LOGOFF_RESULT:String = "RemoteObjectUtil LOGOFF RESULT"; // RESULT du remoting de deconnexion
		public static const LOGOFF_FAULT:String = "RemoteObjectUtil LOGOFF FAULT"; // FAULT du remoting de deconnexion
		
		public static const INVALID_OR_EXPIRED_SESSION:String = "RemoteObjectUtil INVALID SESSION"; // Session Invalide ou Expirée
		public static const DISCONNECTED_SESSION:String = "RemoteObjectUtil DISCONNECTED SESSION"; // Session Déjà Déconnectée

		public static const REMOTING_FAULT:String = "RemoteObjectUtil REMOTING FAULT"; // FAULT de remoting		
		public static const REMOTING_TIMEOUT_REACHED:String = "RemoteObjectUtil REMOTING TIMEOUT REACHED"; // TimeOut de remoting
		public static const REMOTING_CANCEL:String = "RemoteObjectUtil REMOTING CANCEL"; // Annulation des opérations de remoting
		
		public static const EMPTY_NODE_ACCESS:String = "EMPTY NODE ACCESS"; // L'arbre des périmètres est vide
		public static const FIRST_NODE_NO_ACCESS:String = "FIRST NODE NO ACCESS"; // Le 1er noeud d'accès n'a pas d'accès
		public static const BROKEN_PARENTAL_LINK:String = "BROKEN PARENTAL LINK"; // La hiérarchie des noeuds est incorrecte
		
		public static const XML_PARAM_ERROR_EVENT:String = "XML_PARAM_ERROR_EVENT"; // Erreur de param�tre XML (Remoting)
		public static const IPADDR_ACCESS_REFUSED:String = "IPADDR_ACCESS_REFUSED_EVENT"; // Adresse IP refus�e

		//public static const INVALID_GROUP_NODES:String = "INVALID GROUP_NODES"; // Requête Arbre Périmètres Invalide
		
		public function RemoteObjectUtilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
		}
		
		
		override public function clone():Event
		{
			return new RemoteObjectUtilEvent(type,bubbles,cancelable); 
		} 
	}
}
