package {
	import flash.events.Event;

	public class CvAccessManagerEvent extends Event {
		/*
		public static const USER_UPDATED:String = "Infos User mis à jour";
		public static const PERIMETRE_UPDATED:String = "Liste Périmètres mis à jour";
		public static const GROUPES_UPDATED:String = "Liste Groupes mis à jour";
		
		public static const PERIMETRE_CHANGED:String = "Le Périmètre a changé";
		public static const UNIVERS_CHANGED:String = "L'Univers a changé";
		public static const FAMILLE_CHANGED:String = "La famille a changée";
		public static const MODE_CHANGED:String = "Le mode a changé";
		
		public static const UNIVERS_NO_ACCESS:String = "Pas de droits sur univers";
		*/
		public function CvAccessManagerEvent(type:String) {
			super(type);
		}
	}
}
