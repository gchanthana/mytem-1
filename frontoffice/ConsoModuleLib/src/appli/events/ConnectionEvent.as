package appli.events
{
	import entity.ObjectError;
	
	import flash.events.Event;
	
	public class ConnectionEvent extends Event
	{
		public static const VALID_AUTH:String = "valid_auth";
		public static const VALID_ERROR:String = "valid_error";
		public static const PERIMETRE_MAJ:String = "perimetre_maj";
		
		private var _objErreur:ObjectError;
		
		public function ConnectionEvent(type:String, objErreur:ObjectError, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_objErreur = objErreur;
		}

		public function get objErreur():ObjectError
		{
			return _objErreur;
		}

	}
}