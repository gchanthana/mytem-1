package appli.control
{
	import flash.events.Event;
	
	public class AppControlEvent extends Event {
		public static const LOGOFF_EVENT:String = "USER LOGOFF";
		public static const LOGOFF_FAULT_EVENT:String = "USER LOGOFF FAULT";
		public static const BACKOFFICE_LOGOFF_EVENT:String = "BACKOFFICE USER SESSION LOGOFF";
		
		public static const CHANGE_MODULE_EVENT:String = "CHANGE_MODULE_EVENT";
		public static const CHANGE_PERIMETRE_EVENT:String = "CHANGE_PERIMETRE_EVENT";
		public static const CHANGE_GROUPE_EVENT:String = "CHANGE_GROUPE_EVENT";
		
		public static const NEW_ERROR_EVENT:String = "NEW_ERROR_EVENT";
		
		
		private var _key:Object;
		
		public function AppControlEvent(type:String, key:Object = null,bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
			this._key = key;
		}
		
		public function get key():Object
		{
			return _key;
		}
	}
}
