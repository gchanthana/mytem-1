package carol.service
{
	import carol.VO.AnalyseVO;
	import carol.VO.PerimetreVO;
	import carol.event.CarolEvent;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class CarolService extends EventDispatcher
	{
		[Bindable] public var arrDatasetCarol	:ArrayCollection 	= new ArrayCollection();
		[Bindable] public var arrListeIdmois	:ArrayCollection 	= new ArrayCollection();
		[Bindable] public var arrListeNiveauOrga:ArrayCollection 	= new ArrayCollection();
		[Bindable] public var arrListeNoeudOrga	:ArrayCollection 	= new ArrayCollection();
		[Bindable] public var arrListeOrga		:ArrayCollection 	= new ArrayCollection();							
		[Bindable] public var arrListeAnalyse	:ArrayCollection 	= new ArrayCollection();
		public var analyseVoSelected:AnalyseVO
		
		public function CarolService()
		{
			super();
		}

	/* --------------------------------------------------------------- */
	/*  	RECUPERATION DE LA LISTE DES IDMOIS						   */
	/* --------------------------------------------------------------- */
		public function getListeIdMois():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M35.CarolService",
																		"getListeIdMois", 
																		getListeIdMois_handler);
			RemoteObjectUtil.callService(op);
		}
		private function getListeIdMois_handler(e:ResultEvent):void
		{
			arrListeIdmois = e.result as ArrayCollection
			dispatchEvent(new CarolEvent(CarolEvent.CHARGE_PERIODESELECTOR));
		}

	/* --------------------------------------------------------------- */
	/*  	RECUPERATION DE LA LISTE DES ORGAS						   */
	/* --------------------------------------------------------------- */
		public function getListeOrga():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M35.CarolService",
																		"getListeOrga", 
																		getListeOrga_handler);
			RemoteObjectUtil.callService(op);
		}
		private function getListeOrga_handler(e:ResultEvent):void
		{
			/* objet contenant les propriétés suivantes : idcliche, idorga et orga (libellè de l'orga) */
			arrListeOrga = e.result as ArrayCollection
		}

	/* --------------------------------------------------------------- */
	/*  	RECUPERATION DE LA LISTE DES NIVEAUX D'ORGA				   */
	/* --------------------------------------------------------------- */
		public function getListeNiveauOrga(idorga:int,idcliche:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M35.CarolService",
																		"getListeNiveauOrga", 
																		getListeNiveauOrga_handler);
			RemoteObjectUtil.callService(op,idorga,idcliche);
		}
		private function getListeNiveauOrga_handler(e:ResultEvent):void
		{
			/* objet contenant les propriétés suivantes : niveau_choisi, libelle_colonne, idniveau_choisi */
			arrListeNiveauOrga = e.result as ArrayCollection
		}

	/* --------------------------------------------------------------- */
	/*  	RECUPERATION DE LA LISTE DES NOEUDS D'UN NIVEAU D'ORGA	   */
	/* --------------------------------------------------------------- */
		public function getListeNoeudOrga(idorga:int,idcliche:int,niveau:String,idniveau:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M35.CarolService",
																		"getListeNoeudOrga", 
																		getListeNoeudOrga_handler);
			RemoteObjectUtil.callService(op,idorga,idcliche,niveau,idniveau);
		}
		private function getListeNoeudOrga_handler(e:ResultEvent):void
		{
			/* objet contenant les propriétés suivantes : libelle_noeud, idnoeud */
			arrListeNoeudOrga = e.result as ArrayCollection
			dispatchEvent(new CarolEvent(CarolEvent.LISTE_NOEUD_RESULTEVENT));
		}
	/* --------------------------------------------------------------- */
	/*  	RECUPERATION DU DATASET    								   */
	/* --------------------------------------------------------------- */
		public function getDataset(idorga:int,idperimetre:int, niveau:String,idmoisDeb:int,idmoisFin:int,nivAggrer:String):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M35.CarolService",
																		"getDataset", 
																		getDataset_handler);
			RemoteObjectUtil.callService(op,idorga,idperimetre,niveau,idmoisDeb,idmoisFin,nivAggrer);
		}
		private function getDataset_handler(e:ResultEvent):void
		{
			if(e.result != null && e.result.length > 0)
			{
				arrDatasetCarol = new ArrayCollection()
				var unitePattern:RegExp = /(Unite="<vide>")/g
				/* objet contenant les propriétés suivantes : libelle_noeud, idnoeud */
				for each(var obj:Object in e.result)
				{
					var x:XML = new XML((obj.RESULTAT as String).replace(unitePattern,'Unite="vide"'));
					arrDatasetCarol.addItem(castObjProxy(x));
				}
				if(arrDatasetCarol.length > 0)
					dispatchEvent(new CarolEvent(CarolEvent.REFRESH_DATASET_INTERNATIONAL));
				else
					dispatchEvent(new CarolEvent(CarolEvent.FAULT_DATASET_INTERNATIONAL));
			}
			else
			{
				Alert.show("Aucune donnée n'est disponible sur ce périmètre","avertissement");
			}
		}	
		
	/* --------------------------------------------------------------- */
	/*  	RECUPERATION DE LA LISTE DES ANALYSES - DATAMART		   */
	/* --------------------------------------------------------------- */
		public function getListeAnalyse(min:int=0):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M35.CarolService",
																		"listeAnalyse", 
																		getListeAnalyse_handler);
			RemoteObjectUtil.callService(op,min,min+10);
		}
		private function getListeAnalyse_handler(e:ResultEvent):void
		{
			arrListeAnalyse = new ArrayCollection()
			
			if(analyseVoSelected.PERIMETRE.isSetUp())
			{
				var defaultAnalyse:AnalyseVO = new AnalyseVO()
				defaultAnalyse.copyAnalyseVo(analyseVoSelected)
				defaultAnalyse.setDefautAnalyse()
				defaultAnalyse.isDefaultAnalyse = true
				arrListeAnalyse.addItem(defaultAnalyse)
			}
			if(e.result != null && e.result.length > 0)
			{
				for each(var obj:Object in e.result)
				{
					var ana:AnalyseVO 					= new AnalyseVO()
					ana.LIBELLE 						= obj.LIBELLE
					ana.DESCRIPTION 					= obj.DESCRIPTION
					ana.NBRECORD						= obj.NB
					ana.IDANALYSE						= obj.IDANALYSE
					ana.isDefaultAnalyse				= false
					ana.PERIMETRE 						= new PerimetreVO()
					ana.PERIMETRE.idcliche				= obj.IDCLICHE
					ana.PERIMETRE.idorga				= obj.IDORGA
					ana.PERIMETRE.idperimetre			= obj.IDPERIMETRE
					ana.PERIMETRE.idperiodedeb			= obj.IDPERIODE_DEB
					ana.PERIMETRE.idperiodefin			= obj.IDPERIODE_FIN
					ana.PERIMETRE.idracine				= obj.IDRACINE
					ana.PERIMETRE.libelle_racine		= obj.LIBELLE_RACINE
					ana.PERIMETRE.idracinemaster		= obj.IDRACINE_MASTER
					ana.PERIMETRE.iduser				= obj.APP_LOGINID
					ana.PERIMETRE.langue				= obj.LANGUE_PAYS
					ana.PERIMETRE.libelle_orga			= obj.LIBELLE_ORGA
					ana.PERIMETRE.libelle_periodedeb	= obj.LIBELLE_PERIODE_DEB
					ana.PERIMETRE.libelle_periodefin	= obj.LIBELLE_PERIODE_FIN
					ana.PERIMETRE.niveau				= obj.NIVEAU
					ana.PERIMETRE.niveau_aggreg 		= obj.NIVEAU_AGGREG
					ana.getArrFromXml(new XML(obj.XML));
					arrListeAnalyse.addItem(ana)
				}
			}
		}
	/* --------------------------------------------------------------- */
	/*  	EDITER UNE ANALYSE - LE NOM ET LA DESCRIPTION - DATAMART   */
	/* --------------------------------------------------------------- */	
		public function renommerAnalyse(idanalyse:int,nom:String,description:String):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M35.CarolService",
																		"renommerAnalyse", 
																		renommerAnalyse_handler);
			RemoteObjectUtil.callService(op,idanalyse,nom,description);
		}
		private function renommerAnalyse_handler(e:ResultEvent):void
		{
			
		}
	/* ----------------------------------------------------------------	*/
	/*  	SUPPRIMER UNE ANALYSE - DATAMART   						   	*/
	/* ----------------------------------------------------------------	*/	
		public function supprimerAnalyse(idanalyse:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M35.CarolService",
																		"SupprimerAnalyse", 
																		SupprimerAnalyse_handler);
			RemoteObjectUtil.callService(op,idanalyse);
		}
		private function SupprimerAnalyse_handler(e:ResultEvent):void
		{
			
		}
	/* --------------------------------------------------------------- */
	/*  					METHODES PRIVATE						   */
	/* --------------------------------------------------------------- */
		// transforme une ligne XML en objectProxy
		private function castObjProxy(x:XML):ObjectProxy
		{
			var ligne:ObjectProxy 	= new ObjectProxy()
			try
			{
				ligne.n1				= x.@n1[0].toString()
			}
			catch(e:Error){}
			try
			{
				ligne.n2				= x.@n2[0].toString()
			}
			catch(e:Error){}
			try
			{
				ligne.n3				= x.@n3[0].toString()
			}
			catch(e:Error){}
			try
			{
				ligne.n4				= x.@n4[0].toString()
			}
			catch(e:Error){}
			try
			{
				ligne.n5				= x.@n5[0].toString()
			}
			catch(e:Error){}
			ligne.Mois				= x.@Mois[0].toString()
			ligne.Operateur			= x.@Operateur[0].toString()
			ligne.Segment			= x.@Segment[0].toString()
			ligne.TypedeTheme		= x.@TypedeTheme[0].toString()
			ligne.Theme				= x.@Theme[0].toString()
			ligne.Montant			= (parseFloat(x.@Montant[0].toString())).toString()
			ligne.NbAppels			= (parseFloat(x.@NbAppels[0].toString())).toString()
			ligne.VolumeConso		= (parseFloat(x.@VolumeConso[0].toString())).toString()
			ligne.QuantiteFacture	= (parseFloat(x.@QuantiteFacture[0].toString())).toString()
			ligne.Unite				= x.@Unite[0].toString()
			return ligne
		}
	}
}