package carol.popUp
{
	import carol.VO.AnalyseVO;
	import carol.VO.PerimetreVO;
	import carol.event.CarolEvent;
	
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	public class saveAsAnalyseImpl extends Box
	{
		public var txtbxName:TextInput
		public var txtAreaDescription:TextArea
		
		public var analyseVoSelected:AnalyseVO
		
		public function saveAsAnalyseImpl()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED - EVENT COMPOSANTS
		//--------------------------------------------------------------------------------------------//
		
		protected function btnValidClickHandler(e:Event):void
		{
			if(txtbxName.text != "" && txtbxName.text.length > 0)
			{
				analyseVoSelected.LIBELLE = txtbxName.text
				analyseVoSelected.DESCRIPTION = txtAreaDescription.text
				PopUpManager.removePopUp(this);
				dispatchEvent(new CarolEvent(CarolEvent.SAVE_ANALYSE_OK));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('MainCarol', 'Veuillez_entrer_un_libelle'),ResourceManager.getInstance().getString('MainCarol', 'erreur'))
			}
		}
		
		protected function btnAnnulClickHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}


	}
}
