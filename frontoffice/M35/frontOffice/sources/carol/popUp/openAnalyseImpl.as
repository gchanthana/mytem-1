package carol.popUp
{
	import carol.VO.AnalyseVO;
	import carol.event.CarolEvent;
	import carol.service.CarolService;
	
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.Text;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import paginatedatagrid.PaginateDatagrid;
	
	public class openAnalyseImpl extends Box
	{
		public var taDescription:Text
		public var analyseVoSelected:AnalyseVO
		private var analyseToDelete:AnalyseVO
		private var popup_chargement:PopupChargementPerimetreIHM
		[Bindable] public var dtg:PaginateDatagrid
		[Bindable] protected var cs:CarolService = new CarolService()
		public var boolChargerDataset:Boolean = false 
		
		public function openAnalyseImpl()
		{
		}
		
		public function init():void
		{
			if(dtg != null)
			{
				dtg.getChildAt(0).visible = false;
				(dtg.getChildAt(0) as UIComponent).includeInLayout = false;
				dtg.getChildAt(1).visible = false;
				(dtg.getChildAt(1) as UIComponent).includeInLayout = false;
			}
			dtg.selectedIndex = -1;
			cs.analyseVoSelected = this.analyseVoSelected
			cs.getListeAnalyse()
		}
		private function validatePopup(e:CarolEvent):void
		{
			cs.arrListeAnalyse[0].NBRECORD = cs.arrListeAnalyse.length
			cs.arrListeAnalyse.refresh()
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED - EVENT COMPOSANTS
		//--------------------------------------------------------------------------------------------//
		
		protected function btnValidClickHandler(e:Event):void
		{
			if(dtg.selectedIndex > -1)
			{
				analyseVoSelected.copyAnalyseVo(dtg.selectedItem as AnalyseVO);
				dispatchEvent(new CarolEvent(CarolEvent.OPEN_ANALYSE));
			}
			PopUpManager.removePopUp(this);
		}
		
		protected function btnAnnulClickHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function intervalleChangeHandler():void
		{
			cs.getListeAnalyse(dtg.currentIntervalle.indexDepart)
		}
		
		public function descriptionClickHandler(ana:AnalyseVO):void
		{
			var popup:popupDescription = new popupDescription()
			popup.Description = ana.DESCRIPTION
			PopUpManager.addPopUp(popup,this);
			PopUpManager.centerPopUp(popup);
		}
		public function ouvrirClickHandler(ana:AnalyseVO):void
		{
			if(dtg.selectedIndex > -1)
			{
				if(analyseVoSelected.PERIMETRE.isSetUp())
				{
					if(analyseVoSelected.comparePerimetreVoExceptPeriode(dtg.selectedItem as AnalyseVO))
					{
						if(analyseVoSelected.comparePerimetreVo(dtg.selectedItem as AnalyseVO))
						{
							analyseVoSelected.copyAnalyseVo(dtg.selectedItem as AnalyseVO);
							boolChargerDataset = false
							dispatchEvent(new CarolEvent(CarolEvent.OPEN_ANALYSE));
							PopUpManager.removePopUp(this);
						}
						else
						{
							var popup:PopupChargementPerimetreIHM = new PopupChargementPerimetreIHM()
							popup.addEventListener(CarolEvent.OPEN_ANALYSE_DATECHANGED,openAnalyseDateChangedHandler)
							popup.addEventListener(CarolEvent.OPEN_ANALYSE_DATENOTCHANGED,openAnalyseDateChangedHandler)
							PopUpManager.addPopUp(popup,this)
							PopUpManager.centerPopUp(popup)	
						}
					}
					else
					{
						analyseVoSelected.copyAnalyseVo(dtg.selectedItem as AnalyseVO);
						boolChargerDataset = true
						dispatchEvent(new CarolEvent(CarolEvent.OPEN_ANALYSE));
					}
				}
				else
				{
					analyseVoSelected.copyAnalyseVo(dtg.selectedItem as AnalyseVO);
					boolChargerDataset = true
					dispatchEvent(new CarolEvent(CarolEvent.OPEN_ANALYSE));
				}
			}
		}
		private function openAnalyseDateChangedHandler(e:CarolEvent):void
		{
			if(e.type == CarolEvent.OPEN_ANALYSE_DATENOTCHANGED)
			{
				analyseVoSelected.copyAnalyseVoExceptDate(dtg.selectedItem as AnalyseVO);
				boolChargerDataset = false
			}
			else if(e.type == CarolEvent.OPEN_ANALYSE_DATECHANGED)
			{
				analyseVoSelected.copyAnalyseVo(dtg.selectedItem as AnalyseVO);
				boolChargerDataset = true
			}	
			PopUpManager.removePopUp(this)
			dispatchEvent(new CarolEvent(CarolEvent.OPEN_ANALYSE));
		}
		public function renommerClickHandler(ana:AnalyseVO):void
		{
			if(dtg.selectedIndex > -1)
			{
				var popup:popupRenommer = new popupRenommer()
				popup.addEventListener(CarolEvent.POPUP__VALIDATE,validatePopup);
				popup.ana = ana
				PopUpManager.addPopUp(popup,this)
				PopUpManager.centerPopUp(popup)
			}
		}
		public function supprimerClickHandler(ana:AnalyseVO):void
		{
			analyseToDelete = ana
			Alert.show("Vous êtes sur le point de supprimer une analyse. Voulez vous continuer?","Confirmation",Alert.YES | Alert.NO,null,supprCloseHandler);
		}
		private function supprCloseHandler(e:CloseEvent):void
		{
			if(e.detail == Alert.YES)
			{
				cs.supprimerAnalyse(dtg.selectedItem.IDANALYSE);
				cs.arrListeAnalyse.removeItemAt(cs.arrListeAnalyse.getItemIndex(analyseToDelete));
				if(cs.arrListeAnalyse.length > 0)
					cs.arrListeAnalyse[0].NBRECORD = cs.arrListeAnalyse.length
				cs.arrListeAnalyse.refresh()
			}
		}

	}
}