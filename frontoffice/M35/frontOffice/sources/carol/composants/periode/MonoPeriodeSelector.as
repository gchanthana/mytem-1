package carol.composants.periode
{	
	import composants.util.DateFunction;
	
	import mx.events.FlexEvent;
	import mx.events.SliderEvent;
	
	 

	public class MonoPeriodeSelector extends PeriodeSelector_IHM
	{
		private var aMonth : AMonth;
		private static const millisecondsPerMinute:int = 1000 * 60;
		private static const millisecondsPerHour:int = 1000 * 60 * 60;
		private static const millisecondsPerDay:int = 1000 * 60 * 60 * 24;	
		private var _firstDay : Boolean = true;
		
		public function MonoPeriodeSelector()
		{				
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}

		public function onPerimetreChange():void {
			getPeriode();
		}
		
		public final function getSelectorItemCount():int {
			return slider.thumbCount;
		}
			
		/**
		 * 
		 * Fonction qui permet d effacer les donnees du composant
		 * 
		 * 
		 * */
		 public function clean():void{
		 	monthData = null;
		 }
		
		/**
		 * Fonction qui retourne un tableau avec les dates de la periode [mois actuelle -13,mois actuelle-1]
		 * 
		 * @return  monthData : Array un tableau contenant les deates incluses dans la periode [mois actuelle -13,mois actuelle-1];
		 * 
		 * */
		public function getTabPeriode():Array{
			return monthData;
		}
		
		/**
		 * 
		 * Fonction qui permet affecter un traitement au CHANGE_EVENT sur le SLIDER
		 *
		 * @param  methode : Function la fonction qui effectue le traitement  	 
		 * 
		 * 
		 * */	
		public function affectChangeHandler(methode:Function):void {
			slider.addEventListener(SliderEvent.CHANGE,methode);
		}	
		
		/**
		 *  
		 * Fonction qui permet de fixer la periode initiale du slider
		 * 
		 * */
		public function setMini():void{
			slider.values = [monthData.length - 2, monthData.length - 1];
		}
		
		/** 
		 * Fonction qui permet de de fixer le tooltip sur le premier jour ou le dernier jour du mois
		 * Par defaut c'est le premier jour du mois qui est afficher
		 * @param fd si <code>true</code> on affiche le premier jour du mois. Si <code>false</code> on affiche le dernier
		 * */
		 [Inspectable(enumeration="true,false")]
		 public function set firstDay(fd : Boolean):void{
		 	_firstDay = fd; 	
		 }
		
		/*------------------------------------ PRIVATE ----------------------------------------------*/
		[Bindable]
		private var moisDebut : String = "";	
		[Bindable]
		private var moisFin : String = "";	
				
		//tableau contenant les mois dispo.
		[Bindable]
		private var monthData:Array; 
		 
		// formate les tooltips du slider	
		private function getSliderLabelFirstDay(value:Number):String{
	 		return (monthData[value] as AMonth).getDateDebut();
		}
		private function getSliderLabelLastDay(value:Number):String{
	 		return (monthData[value] as AMonth).getDateFin();
		}
		
		// Charge la periode (debut = mois dernier fin = 12 mois avant)
		private function getPeriode():void{
						
			var tabPeriode : Array = new Array();
			var firstDate : Date = new Date(CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateDeb);
			firstDate.setDate(1);
			
			tabPeriode[0]= new AMonth(firstDate);

			// sets the invoice date to today's date
			var invoiceDate:Date = new Date();
			// remove 30 days to get the end date of the interval
			var endDate:Date = new Date(CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateFin.getFullYear(),
										CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateFin.getMonth());
		  	var startDate:Date = new Date(CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateDeb.getFullYear(),
										CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateDeb.getMonth()-1);
			var diffMois:Number=DateFunction.dateDiff("m",endDate,startDate);
		  	var j :int = 0;

		  	var aMonth : AMonth;
		  	
		  	for (var i:int = 0 ; i < diffMois; i++){		
		  		var newDate:Date = new Date(endDate.getFullYear(),endDate.getMonth() - i,1);			
			
				newDate.setDate(1);	
				aMonth = new AMonth(newDate);
				tabPeriode.unshift(aMonth);
		  	}
			
			monthData = tabPeriode;  
		   	slider.maximum = monthData.length - 2;
			slider.values = [monthData.length - 2, monthData.length - 1];
			
		                
		}
		
		private function periodeChange(e:SliderEvent):void{					
			if (_firstDay){			
				moisDebut = AMonth(getTabPeriode()[e.currentTarget.values[0]]).getDateDebut();
				moisFin = AMonth(getTabPeriode()[e.currentTarget.values[1]-1]).getDateFin();	
			}else{
				moisDebut = AMonth(getTabPeriode()[e.currentTarget.values[0]]).getDateFin();
				moisFin = AMonth(getTabPeriode()[e.currentTarget.values[1]-1]).getDateFin();	
			}
										
			var periodeEvent : PeriodeEvent = new PeriodeEvent("periodeChange");			
			
			periodeEvent.moisDeb = moisDebut;
			periodeEvent.moisFin = moisFin;	
		 	
			dispatchEvent(periodeEvent);
		}
				
		private function periodeIsChanging(se : SliderEvent):void{			
			if (slider.getThumbAt(1) != null) {
				if (se.thumbIndex == 1){				
					if (slider.getThumbAt(0).hitTestObject(slider.getThumbAt(1))){
						slider.setThumbValueAt(1,slider.values[0]+1);
					}							
				}else if (se.thumbIndex == 0){				
					if (slider.getThumbAt(0).hitTestObject(slider.getThumbAt(1))){					
						slider.setThumbValueAt(0,slider.values[1]-1);
						
					}				 
				}	
			}								
		}
		
		 //Fonction qui initilaise le composant. Charge les donnees (périodes);		  
		 private function initIHM(fe:FlexEvent):void{ 	
		 			 	
		 	affectChangeHandler(periodeChange);        
		    slider.addEventListener(SliderEvent.THUMB_DRAG,periodeIsChanging);
	   		slider.thumbCount=1;
	   		slider.setStyle("showTrackHighlight",false);
		    getPeriode();	
		    
		    moisDebut = AMonth(getTabPeriode()[int(getTabPeriode().length)-2]).getDateDebut();
		   		   	 
			moisFin = AMonth(getTabPeriode()[int(getTabPeriode().length)-2]).getDateFin();
			
			if (_firstDay)
		    	slider.dataTipFormatFunction = getSliderLabelFirstDay; 
		    else
		   		slider.dataTipFormatFunction = getSliderLabelLastDay;
		   		   
		   		   
		   	slider.getThumbAt(0).name = "leftthumb";
		   	slider.getThumbAt(1).name = "rightthumb";
		   	   
		    slider.getThumbAt(0).setStyle("fillColors",["#909587","#909587"]);
			slider.getThumbAt(1).setStyle("fillColors",["#909587","#909587"]);	    		   	  	
		   		   
		   	var periodeEvent : PeriodeEvent = new PeriodeEvent("periodeChange");
		   	
			periodeEvent.moisDeb = moisDebut;
			periodeEvent.moisFin = moisFin;		
			
			dispatchEvent(periodeEvent);	                
		}	
		
		
	}
}


