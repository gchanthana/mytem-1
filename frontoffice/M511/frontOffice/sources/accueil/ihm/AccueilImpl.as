package accueil.ihm
{
	import accueil.event.AccueilEvent;
	import accueil.ihm.graph.EvolutionCoutBySegmentIHM;
	import accueil.ihm.graph.IntegrationDesFacturesIHM;
	import accueil.ihm.graph.RepartitionCoutsSegmentIHM;
	import accueil.service.perimetre.PerimetreService;
	import accueil.utils.AccueilMenuTree;
	
	import appli.events.ConsoViewEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Button;
	import mx.events.FlexEvent;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	[Bindable]
	public class AccueilImpl extends Canvas
	{
		
		public var menuTree:AccueilMenuTree;
		public var btnExportOrga:Button;
		public var btnExportOrgaSt:Button;
		
		public var comp1:EvolutionCoutBySegmentIHM;
		public var comp2:IntegrationDesFacturesIHM;
		public var comp3:RepartitionCoutsSegmentIHM;	
		
		
		public var perimetreE0:PerimetreService;
		public var remote:RemoteObject;
		public var remo1:RemoteObject;
		public var remo2:RemoteObject;
		public var remo3:RemoteObject;
		public var dateDebut	:String = "";
		public var dateFin		:String = "";
		
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		private var _data1:ArrayCollection;
		private var _data2:ArrayCollection;
		private var _data3:ArrayCollection;	

		public function set data1(value:ArrayCollection):void
		{
			_data1 = value;
		}

		public function get data1():ArrayCollection
		{
			return _data1;
		}
		public function set data2(value:ArrayCollection):void
		{
			_data2 = value;
		}

		public function get data2():ArrayCollection
		{
			return _data2;
		}
		public function set data3(value:ArrayCollection):void
		{
			_data3 = value;
		}

		public function get data3():ArrayCollection
		{
			return _data3;
		}
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		 	
		public function AccueilImpl() 
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
			
			remote = new RemoteObject("ColdFusion");
			remote.source="fr.consotel.consoview.M511.AccueilService";
			remote.addEventListener(ResultEvent.RESULT,getDataResultHandler,false,0,true);
			remote.addEventListener(FaultEvent.FAULT,faultHandler,false,0,true);
			
			remo1 = new RemoteObject("ColdFusion");
			remo1.source="fr.consotel.consoview.M511.AccueilService";
			remo1.addEventListener(ResultEvent.RESULT,getData1ResultHandler,false,0,true);
			remo1.addEventListener(FaultEvent.FAULT,faultHandler,false,0,true);
			
			remo2 = new RemoteObject("ColdFusion");
			remo2.source="fr.consotel.consoview.M511.AccueilService";
			remo2.addEventListener(ResultEvent.RESULT,getData2ResultHandler,false,0,true);
			remo2.addEventListener(FaultEvent.FAULT,faultHandler,false,0,true);
			
			remo3 = new RemoteObject("ColdFusion");
			remo3.source="fr.consotel.consoview.M511.AccueilService";
			remo3.addEventListener(ResultEvent.RESULT,getData3ResultHandler,false,0,true);
			remo3.addEventListener(FaultEvent.FAULT,faultHandler,false,0,true);
		 	
        }
		
		protected function afterCreationComplete(event:FlexEvent):void 
		{
			menuTree.addEventListener(ConsoViewEvent.TREE_MENU_FUNCTION_CHANGED,handlePeriodControlEvents);
			afterPerimetreUpdated();
		}
        
		public function getUniversKey():String 
		{
			return "HOME_E0";
		}
		
		/**
		 * Univers Accueil do not have Function (Particular case).
		 * IMPORTANT :
		 * Other ConsoView Function classes SHOULD NOT return NULL for this method !!!!
		 * */
		public function getFunctionKey():String 
		{
			return null;
		}
		
		public function afterPerimetreUpdated():void 
		{
			if(perimetreE0)
			{				
				perimetreE0.handler.removeEventListener(AccueilEvent.PERIMETRE_CHANGE, _perimetreE0handlerPerimetreChangeHandler);
				perimetreE0.handler.removeEventListener(AccueilEvent.PERIMETRE_NOT_CHANGE, _perimetreE0handlerPerimetreChangeHandler);
				perimetreE0.handler = null;
				perimetreE0.model = null;
				perimetreE0 = null;				
			}
			
			perimetreE0 = new PerimetreService();
			perimetreE0.handler.addEventListener(AccueilEvent.PERIMETRE_CHANGE
						, _perimetreE0handlerPerimetreChangeHandler,false,0,true);
			perimetreE0.handler.addEventListener(AccueilEvent.PERIMETRE_NOT_CHANGE
						, _perimetreE0handlerPerimetreChangeHandler,false,0,true);
															
			perimetreE0.setUpPerimetre();
			
			menuTree.afterPerimetreUpdated();
		}
		
	
		private function handlePeriodControlEvents(evt:ConsoViewEvent):void {
			switch(evt.type) {
				case ConsoViewEvent.PERIOD_DATE_DEB_CHANGED :
					//trace("(UniversAccueil) PERIOD_DEB : " + ObjectUtil.toString(getSelectedDateDEB()));
					dispatchEvent(new ConsoViewEvent(ConsoViewEvent.PERIOD_DATE_DEB_CHANGED));
					break;
				case ConsoViewEvent.PERIOD_DATE_FIN_CHANGED :
					//trace("(UniversAccueil) PERIOD_FIN : " + ObjectUtil.toString(getSelectedDateFIN()));
					dispatchEvent(new ConsoViewEvent(ConsoViewEvent.PERIOD_DATE_FIN_CHANGED));
					break;
				case ConsoViewEvent.TREE_MENU_FUNCTION_CHANGED : 
					changeUniversFunction();
					break
				default :
					break;
			}
		}
		
		
		private function changeUniversFunction():void {
			var i:int;
			var eventToDispatchType:String = null;
			var tmpCurrentUniversKey:String = CvAccessManager.CURRENT_UNIVERS;
			var node:XML = menuTree.selectedItem as XML;
			var menuDataProvider:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			var toggledNodes:XMLList = menuDataProvider.descendants("*").(@toggled == 'true');
			
			if(node.@toggled == "false") { // Univers or Function CHANGE
				for(i=0; i < toggledNodes.length(); i++)
					toggledNodes[i].@toggled = false;
				if((node as XML).parent().@KEY != "ROOT") { // Node is a FUNCTION
					(node as XML).parent().@toggled = true;
					if((node as XML).parent().@KEY == tmpCurrentUniversKey) { // Univers is the same - Function changes
						eventToDispatchType = ConsoViewEvent.FUNCTION_CHANGED;
					} else { // Univers changes (Thus function changes
						eventToDispatchType = ConsoViewEvent.UNIVERS_FUNCTION_CHANGED;
					}
				} else { // Node is DEFAULT Univers
					eventToDispatchType = ConsoViewEvent.DEFAULT_UNIVERS_CHANGED;
				}
				node.@toggled = true;
				dispatchEvent(new ConsoViewEvent(eventToDispatchType));
			}
		}
		

		protected function _perimetreE0handlerPerimetreChangeHandler(event:AccueilEvent):void
		{
			
			
			if(perimetreE0)
			{
				perimetreE0.handler.removeEventListener(AccueilEvent.PERIMETRE_CHANGE,    _perimetreE0handlerPerimetreChangeHandler);
				perimetreE0.handler.removeEventListener(AccueilEvent.PERIMETRE_NOT_CHANGE,_perimetreE0handlerPerimetreChangeHandler);				
			}
			
			comp1.stopLoader();
			comp2.stopLoader();
			comp3.stopLoader();
			
			data1 = null;
			data2 = null;
			data3 = null;
			
			comp1.launchLoader();
			comp2.launchLoader();
			comp3.launchLoader();
			
			remote.getData();
			/*
			remo1.getData1();
			remo2.getData2();
			remo3.getData3(); 
			*/
		}	
		
		public function getDataResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				
				data1 = evt.result[0] as ArrayCollection;
				if(data1.length > 0)
				{
					dateDebut 	= data1[0].saw_10;
					dateFin 	= data1[data1.length-1].saw_10;
				}
				
				data2 = evt.result[1] as ArrayCollection;
				
				data3 = evt.result[2] as ArrayCollection;
				var tot	:Number = 0;
				for(var i:int = 0; i < data3.length;i++)
				{
					tot = tot + Number(data3[i].saw_7);
				}
				comp3.total = tot;
				comp3.dateToday = dateDebut + " - " + dateFin;	
				
			}
		}
		
		public function getData1ResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				data1 = evt.result as ArrayCollection;
				if(data1.length > 0)
				{
					dateDebut 	= data1[0].saw_10;
					dateFin 	= data1[data1.length-1].saw_10;
				}	
			}
			
		}
		
		public function getData2ResultHandler(evt:ResultEvent):void
		{	
			if(evt.result)
			{
				data2 = evt.result as ArrayCollection;	
			}							
			
							
		}
		
		public function getData3ResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				data3 = evt.result as ArrayCollection;
				var tot	:Number = 0;
				for(var i:int = 0; i < data3.length;i++)
				{
					tot = tot + Number(data3[i].saw_7);
				}
				comp3.total = tot;
				comp3.dateToday = dateDebut + " - " + dateFin;	
			}			
		}
		
		public function faultHandler(evt:FaultEvent):void
		{
			trace(evt.fault.getStackTrace());
		}
		
		
	}
}