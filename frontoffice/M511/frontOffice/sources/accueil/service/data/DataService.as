package accueil.service.data
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	
	[Bindable]
	public class DataService
	{
		public var theModele:DataModel;
		public var theHandler:DataHandlers;
		
		private var op1:AbstractOperation;
		private var op2:AbstractOperation;
		private var op3:AbstractOperation; 
		
		public function DataService()
		{
			theModele = new DataModel();
			theHandler = new DataHandlers(theModele);
		}
		
		public function reset():void
		{
			op1 = null;
			op2 = null;
			op3 = null;
			
			theModele.data1 = null;
			theModele.data2 = null;
			theModele.data3 = null;
		}
		
		public function getData():void
		{
			reset();
			op1 = RemoteObjectUtil.getSilentOperation(
				"fr.consotel.consoview.M511.AccueilService",
				"getData",
				theHandler.getDataResultHandler)
			RemoteObjectUtil.callSilentService(op1);
		}
		
		public function getData1():void
		{
			if(theModele.data1)
			{
				theModele.data1 = null;
				op1 = null;
			}
			
			op1 = RemoteObjectUtil.getSilentOperation(
																				"fr.consotel.consoview.M511.AccueilService",
																				"getData1",
																				theHandler.getData1ResultHandler)
			
			RemoteObjectUtil.callSilentService(op1);
														
		}
		
		public function getData2():void
		{
			if(theModele.data2)
			{
				theModele.data2 = null;
				op2 = null;
			}
			
			op2 = RemoteObjectUtil.getSilentOperation(
																				"fr.consotel.consoview.M511.AccueilService",
																				"getData2",
																				theHandler.getData2ResultHandler)
			
			RemoteObjectUtil.callSilentService(op2);
														
		}
		
		
		public function getData3():void
		{
			if(theModele.data3)
			{
				theModele.data3 = null;
				op3 = null;
			}
			
			op3 = RemoteObjectUtil.getSilentOperation(
																				"fr.consotel.consoview.M511.AccueilService",
																				"getData2",
																				theHandler.getData3ResultHandler)
			
			RemoteObjectUtil.callSilentService(op3);
														
		}
		
		

	}
}