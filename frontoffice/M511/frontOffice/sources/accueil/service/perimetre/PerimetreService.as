package accueil.service.perimetre
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	public class PerimetreService
	{
		public var model:PerimetreModel;
		public var handler:PerimetreHandlers;
		
		public function PerimetreService()
		{
			model = new PerimetreModel();
			handler = new PerimetreHandlers(model);
		}
		
		public function setUpPerimetre():void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation(
																				"fr.consotel.consoview.M511.PerimetreE0",
																				"setUpPerimetre",
																				handler.setUpPerimetreResultHandler);
																				
			var idNewPerimetre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			
			RemoteObjectUtil.callSilentService(op,idNewPerimetre);
		}
		
		
	}
}