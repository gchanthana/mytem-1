package accueil.utils
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Tree;
	import mx.events.FlexEvent;

	public class AccueilMenuTree extends Tree {
		public function getSelectedNode():XML {
			return this.selectedItem as XML;
		}
		
		
		public function AccueilMenuTree() {
			super();
			trace("(AccueilMenuTree) Instance Creation");
			dataDescriptor = new TreeMenuDataDescriptor();
			//this.itemRenderer = new ClassFactory(univers.accueil.menu.TreeMenuItemRenderer);
			addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
		}
		
		override protected function commitProperties():void{
			super.commitProperties();
			labelField = "@LBL";
		}
		
		private function afterCreationComplete(event:Event):void {
			trace("(AccueilMenuTree) Perform IHM Initialization");
			doubleClickEnabled = false;
			addEventListener(MouseEvent.CLICK,handleTreeMenuEvents);
			addEventListener(MouseEvent.DOUBLE_CLICK,handleTreeMenuEvents);
			//labelField = "@LBL";
			afterPerimetreUpdated();
		}
		
		public function afterPerimetreUpdated():void {
			trace("(AccueilMenuTree) Perform After Perimetre Updated Tasks");
			doubleClickEnabled = false;
			dataProvider = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData.UNIVERS;
			callLater(afterDataProviderSet);
		}
		
		private function afterDataProviderSet():void {
			trace("(AccueilMenuTree) Perform After Perimetre Updated Tasks");
			doubleClickEnabled = true;			
			
			///Ouvrir l'arbre
			for (var i:int = 0; i < dataProvider.length; i ++){
            	expandChildrenOf(dataProvider[i], true)
        	}	
		}
		
		private function handleTreeMenuEvents(event:MouseEvent):void {
			if(selectedIndex != -1)
			{
				var treeMenuItem:XML = this.selectedItem as XML;
				var qName:QName = treeMenuItem.name() as QName;
				
				// si le tree est cliquable et permet de charger un module
				if(treeMenuItem.@SYS == '1'){
					CvAccessManager.changeModule(treeMenuItem.@KEY);
				}
//				switch(event.type) {
//					case MouseEvent.CLICK :
//						if(qName.localName != "FONCTION")
//							if(treeMenuItem.@KEY == "GEST_CONT")
//								dispatchEvent(new ConsoViewEvent(ConsoViewEvent.TREE_MENU_FUNCTION_CHANGED));
//							else
//								expandItem(treeMenuItem,(! isItemOpen(treeMenuItem))); // Negation of isItemOpen(treeMenuItem) !!!
//						else{
//							if(qName.localName == "FONCTION") {
//								if(parseInt(treeMenuItem.@USR,10) > 0) {
//									trace("(AccueilMenuTree) FONCTION : " + treeMenuItem.parent().@KEY + "/" + treeMenuItem.@KEY);
//									dispatchEvent(new ConsoViewEvent(ConsoViewEvent.TREE_MENU_FUNCTION_CHANGED));
//								}
//							} else {
//								if(treeMenuItem.@KEY == "HOME")
//									trace("(AccueilMenuTree) DEFAULT : " + treeMenuItem.@KEY);
//								dispatchEvent(new ConsoViewEvent(ConsoViewEvent.TREE_MENU_FUNCTION_CHANGED));
//							}
//						}
//						break;
//					case MouseEvent.DOUBLE_CLICK :
//						if(qName.localName == "FONCTION") {
//							if(parseInt(treeMenuItem.@USR,10) > 0) {
//								trace("(AccueilMenuTree) FONCTION : " + treeMenuItem.parent().@KEY + "/" + treeMenuItem.@KEY);
//								dispatchEvent(new ConsoViewEvent(ConsoViewEvent.TREE_MENU_FUNCTION_CHANGED));
//							}
//						} else {
//							if(treeMenuItem.@KEY == "HOME")
//								trace("(AccueilMenuTree) DEFAULT : " + treeMenuItem.@KEY);
//								dispatchEvent(new ConsoViewEvent(ConsoViewEvent.TREE_MENU_FUNCTION_CHANGED));
//						}
//						break;
//					default :
//						break;
//				}
			}	
		}
			
	}
}