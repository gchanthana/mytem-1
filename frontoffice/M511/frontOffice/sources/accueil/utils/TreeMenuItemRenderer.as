package accueil.utils
{
	import mx.controls.treeClasses.TreeItemRenderer;

	public class TreeMenuItemRenderer extends TreeItemRenderer{
		public function TreeMenuItemRenderer(){
			super();
		}
		
        public override function set data(value:Object):void{
			super.data = value;
			if(value != null) {
				if(value.@USR > 0)	setStyle("color","#00AB00");
	   			else setStyle("color","#FF0000");
			}
			invalidateDisplayList();
        }

        protected override function updateDisplayList(unscaledWidth:Number,unscaledHeight:Number):void {       
            super.updateDisplayList(unscaledWidth,unscaledHeight);
			
        }
	}
}