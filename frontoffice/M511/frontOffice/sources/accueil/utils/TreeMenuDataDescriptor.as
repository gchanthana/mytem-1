package accueil.utils
{
	import mx.collections.ICollectionView;
	import mx.collections.XMLListCollection;
	import mx.controls.treeClasses.ITreeDataDescriptor;

	public class TreeMenuDataDescriptor implements ITreeDataDescriptor {
		public function TreeMenuDataDescriptor() {
			trace("(TreeMenuDataDescriptor) Instance Creation");
		}

		public function getChildren(node:Object, model:Object=null):ICollectionView {
			return new XMLListCollection((node as XML).children());
		}
		
		public function hasChildren(node:Object, model:Object=null):Boolean {
			return ((node as XML).children().length() > 0);
		}
		
		public function isBranch(node:Object, model:Object=null):Boolean {
			if(node is XML) {
				return ((node as XML).children().length() > 0);
			} else {
				return false;
			}
		}
		
		public function getData(node:Object, model:Object=null):Object {
			return null;
		}
		
		public function addChildAt(parent:Object, newChild:Object, index:int, model:Object=null):Boolean {
			return false;
		}
		
		public function removeChildAt(parent:Object, child:Object, index:int, model:Object=null):Boolean {
			return false;
		}
	}
}