package event.fonction.fctGestionCatalogue
{
	import flash.events.Event;
	
	public class FctGestionCatalogueEvent extends Event
	{
		public static const LISTE_EQPICECAT_EVENT : String = "LISTE_EQPICECAT_EVENT";
		public static const LISTE_EQPFOURNISSEUR_EVENT : String = "LISTE_EQPFOURNISSEUR_EVENT";
		public static const GET_DATA_EVENT : String = "GET_DATA_EVENT";
		public static const GIVE_AUTORISATION_EVENT : String = "GIVE_AUTORISATION_EVENT";
		public static const INJECTER_EQP_EVENT : String = "INJECTER_EQP_EVENT";
		public static const LIER_EQP_EVENT : String = "LIER_EQP_EVENT";
		
		public function FctGestionCatalogueEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}