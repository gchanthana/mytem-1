package entity.GestionCollecte.vo
{
    import flash.events.EventDispatcher;
    import flash.xml.XMLDocument;
    import flash.xml.XMLNode;
    import mx.rpc.xml.SimpleXMLDecoder;
    import mx.rpc.xml.SimpleXMLEncoder;

    public class ValueObject extends EventDispatcher implements IValueObject
    {
        public function ValueObject()
        {
        }

        public function deserialize(object:Object):IValueObject
        {
            return null;
        }

        public function serialize(object:IValueObject):Object
        {
            return null;
        }

        public override function toString():String
        {
            return "ValueObject to override";
        }

        public function toXML():XML
        {
            var xml:XML = voToXml(this, "ValueObject");
            return xml;
        }

        public function voToXml(vo:Object, qName:String):XML
        {
            var qn:QName = new QName(qName);
            var xmlDocument:XMLDocument = new XMLDocument();
            var simpleXMLEncoder:SimpleXMLEncoder = new SimpleXMLEncoder(xmlDocument);
            var xmlNode:XMLNode = simpleXMLEncoder.encodeValue(vo, qn, xmlDocument);
            var xml:XML = new XML(xmlDocument.toString());
            return xml;
        }

        public function xmlToVo(xml:XML):IValueObject
        {
            var xmlDoc:XMLDocument = new XMLDocument(xml);
            var simpleXML:SimpleXMLDecoder = new SimpleXMLDecoder(true);
            var o:Object = simpleXML.decodeXML(xmlDoc) as Object;
            return decode(o) as IValueObject
        }

        public function decode(object:Object):IValueObject
        {
            // to override
            return this;
        }

        public function getDateFromXmlString(str:String):Date
        {
            var reg:RegExp = new RegExp(/-/g);
            var dateStr:String = (str).replace(reg, '/');
            dateStr = dateStr.slice(0, 10);
            return new Date(Date.parse(dateStr));
        }
    }
}