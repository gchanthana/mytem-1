package entity.GestionCollecte.vo
{

    public interface IValueObject
    {
        function deserialize(object:Object):IValueObject;
        function serialize(object:IValueObject):Object;
        function decode(object:Object):IValueObject
        function toString():String;
    }
}