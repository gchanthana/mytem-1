package entity
{
	import mx.utils.ObjectUtil;

	[Bindable]
	public class PartenaireSimpleVO
	{
		private var _idPartenaire					:int;
		private var _nomPartenaire					:String;
		private var _libelleCertification			:String;
		private var _dateDebutContrat				:Date;
		private var _dateFinContrat					:Date;
		private var _dateRenouvellement				:Date;
		private var _libellePlanTarifaire			:String;
		private var _dateDebutPlanTarifaire			:Date;
		private var _dateFinPlanTarifaire			:Date;
		private var _isActive						:Boolean;
		
		public function PartenaireSimpleVO(partn:PartenaireVO)
		{
			this.idPartenaire  						= partn.ID;
			this.nomPartenaire 						= partn.NOM;
			this.libelleCertification 				= partn.objContrat.objCertification.libelle;
			this.dateDebutContrat		 			= partn.objContrat.dateDebut;
			this.dateFinContrat 					= partn.objContrat.dateFin;
			this.dateRenouvellement 				= partn.objContrat.dateRenouvellement;
			this.libellePlanTarifaire				= partn.objContrat.objPlanTarifaire.objTypePlanTarif.libelle;
			this.dateDebutPlanTarifaire				= partn.objContrat.objPlanTarifaire.dateDebut;
			this.dateFinPlanTarifaire				= partn.objContrat.objPlanTarifaire.dateFin;
			this.isActive							= partn.isActive
		}
		
		public function get isActive():Boolean { return _isActive; }
		
		public function set isActive(value:Boolean):void
		{
			if (_isActive == value)
				return;
			_isActive = value;
		}
		
		public function get dateFinPlanTarifaire():Date { return _dateFinPlanTarifaire; }
		
		public function set dateFinPlanTarifaire(value:Date):void
		{
			if (_dateFinPlanTarifaire == value)
				return;
			_dateFinPlanTarifaire = value;
		}
		
		public function get dateDebutPlanTarifaire():Date { return _dateDebutPlanTarifaire; }
		
		public function set dateDebutPlanTarifaire(value:Date):void
		{
			if (_dateDebutPlanTarifaire == value)
				return;
			_dateDebutPlanTarifaire = value;
		}
		
		public function get libellePlanTarifaire():String { return _libellePlanTarifaire; }
		
		public function set libellePlanTarifaire(value:String):void
		{
			if (_libellePlanTarifaire == value)
				return;
			_libellePlanTarifaire = value;
		}
		
		public function get dateRenouvellement():Date { return _dateRenouvellement; }
		
		public function set dateRenouvellement(value:Date):void
		{
			if (_dateRenouvellement == value)
				return;
			_dateRenouvellement = value;
		}
		
		public function get dateFinContrat():Date { return _dateFinContrat; }
		
		public function set dateFinContrat(value:Date):void
		{
			if (_dateFinContrat == value)
				return;
			_dateFinContrat = value;
		}
		
		public function get dateDebutContrat():Date { return _dateDebutContrat; }
		
		public function set dateDebutContrat(value:Date):void
		{
			if (_dateDebutContrat == value)
				return;
			_dateDebutContrat = value;
		}
		
		public function get libelleCertification():String { return _libelleCertification; }
		
		public function set libelleCertification(value:String):void
		{
			if (_libelleCertification == value)
				return;
			_libelleCertification = value;
		}
		
		public function get nomPartenaire():String { return _nomPartenaire; }
		
		public function set nomPartenaire(value:String):void
		{
			if (_nomPartenaire == value)
				return;
			_nomPartenaire = value;
		}
		
		public function get idPartenaire():int { return _idPartenaire; }
		
		public function set idPartenaire(value:int):void
		{
			if (_idPartenaire == value)
				return;
			_idPartenaire = value;
		}
	}
}