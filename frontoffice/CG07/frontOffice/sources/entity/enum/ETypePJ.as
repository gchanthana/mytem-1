package entity.enum
{
    import mx.collections.ArrayCollection;

    public class ETypePJ extends Enumeratio
    {
        public static const PRESTA_PERIFACTURATION:ETypePJ = new ETypePJ("PRESTA_PERIFACTURATION", "presta_perifacturation");
        public static const SOUSCRIPTION_CONTRAT:ETypePJ = new ETypePJ("SOUSCRIPTION_CONTRAT", "souscription_contrat");
        public static const ROIC:ETypePJ = new ETypePJ("ROIC", "Roic");
        public static const ROCF:ETypePJ = new ETypePJ("ROCF", "Rocf");

        public function ETypePJ(value:String, label:String)
        {
            super(value, label);
        }

        public static function getCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(PRESTA_PERIFACTURATION, SOUSCRIPTION_CONTRAT, ROIC, ROCF));
        }
    }
}