package entity
{
	public class NiveauVO
	{
		public function NiveauVO()
		{
		}
		
		private var _Id				:String;
		private var _libelle		:String;
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDNIVEAU"))
				{
					this._Id = obj.IDNIVEAU;	
				}
				
				if(obj.hasOwnProperty("NOM_NIVEAU"))
				{
					this._libelle = obj.NOM_NIVEAU;
				}
			}
			catch(e:Error)
			{
				trace("##initialisation du niveau erronée");
			}
		}//End fill
		
		public function get libelle():String
		{
			return _libelle;
		}
		
		public function set libelle(value:String):void
		{
			_libelle = value;
		}
		
		public function get Id():String
		{
			return _Id;
		}
		
		public function set Id(value:String):void
		{
			_Id = value;
		}
	}
}