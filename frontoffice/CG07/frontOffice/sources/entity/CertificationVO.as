package entity
{
	[Bindable]
	public class CertificationVO
	{
		private var _id:int;
		private var _libelle:String;
		private var _description:String;
		
		public function CertificationVO()
		{
		}

		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDFC_CERTIFICATION"))
					this.id = obj.IDFC_CERTIFICATION;
				
				if(obj.hasOwnProperty("LIBELLE_CERTIFICATION"))
					this.libelle = obj.LIBELLE_CERTIFICATION;
				
				if(obj.hasOwnProperty("DESCRIPTION_CERTIFICATION"))
					this.description = obj.DESCRIPTION_CERTIFICATION;
			
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet CertificationVO erroné ");
			}
				
			
		}
		public function get description():String { return _description; }
		
		public function set description(value:String):void
		{
			if (_description == value)
				return;
			_description = value;
		}
		
		public function get libelle():String { return _libelle; }
		
		public function set libelle(value:String):void
		{
			if (_libelle == value)
				return;
			_libelle = value;
		}
		
		public function get id():int { return _id; }
		
		public function set id(value:int):void
		{
			if (_id == value)
				return;
			_id = value;
		}
	}
}