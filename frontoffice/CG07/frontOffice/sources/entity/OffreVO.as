package entity
{
	[Bindable]
	public class OffreVO
	{
		private var _ID:int;
		private var _LABEL:String;
		private var _APPLICATION_ID:int;
		private var _APPLICATION_NAME:String;
		
		public function OffreVO()
		{
		}
		
		public function fill(obj:Object):Boolean
		{
			try
			{
				if(obj.hasOwnProperty("OFFRE_ID"))
					this._ID = obj.OFFRE_ID;
				if(obj.hasOwnProperty("OFFRE_NAME"))
					this._LABEL = obj.OFFRE_NAME;
				if(obj.hasOwnProperty("APPLICATION_ID"))
					this._APPLICATION_ID = obj.APPLICATION_ID;
				if(obj.hasOwnProperty("APPLICATION_NAME"))
					this._APPLICATION_NAME = obj.APPLICATION_NAME;
				return true;
			}
			catch(e:Error)
			{
				return false
			}
			return false
		}
		
		public function get APPLICATION_ID():int { return _APPLICATION_ID; }
		public function set APPLICATION_ID(value:int):void
		{
			if (_APPLICATION_ID == value)
				return;
			_APPLICATION_ID = value;
		}
		public function get LABEL():String { return _LABEL; }
		public function set LABEL(value:String):void
		{
			if (_LABEL == value)
				return;
			_LABEL = value;
		}
		public function get ID():int { return _ID; }
		public function set ID(value:int):void
		{
			if (_ID == value)
				return;
			_ID = value;
		}
		public function get APPLICATION_NAME():String { return _APPLICATION_NAME; }
		public function set APPLICATION_NAME(value:String):void
		{
			if (_APPLICATION_NAME == value)
				return;
			_APPLICATION_NAME = value;
		}
	}
}