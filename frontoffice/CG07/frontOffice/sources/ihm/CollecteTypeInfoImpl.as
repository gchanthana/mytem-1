package ihm
{
    import entity.vo.CollecteTypeVO;
    
    import flash.events.Event;
    
    import mx.containers.VBox;


    [Bindable]
    public class CollecteTypeInfoImpl extends VBox
    {
        private var _currentCollectTypeVO:CollecteTypeVO;
        public static const W_FI:Number = 150;

        public function CollecteTypeInfoImpl()
        {
            super();
            currentCollectTypeVO = new CollecteTypeVO();
        }

        public function init(event:Event):void
        {
        }

        public function get currentCollectTypeVO():CollecteTypeVO
        {
            return _currentCollectTypeVO;
        }

        public function set currentCollectTypeVO(value:CollecteTypeVO):void
        {
            _currentCollectTypeVO = value;
        }
    }
}