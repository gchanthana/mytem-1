package ihm
{    
    import mx.containers.TabNavigator;
    import mx.containers.TitleWindow;
    import mx.containers.VBox;
    import mx.events.FlexEvent;
    import mx.resources.ResourceManager;
    
    import utils.abstract.AbstractFonction;
	
	/**
	 * Classe pilotant le composant d'<b>entree</b> de la gestion des collectes
	 * 2 panels :
	 *  Gestion des templates : Gérer les Fiches de collectes opérateurs /
	 *  Gestion des collectes : Gérer les Collectes clientes 
	 * 
	 * 
	 */	
    public class FctGestionCollecteImpl extends AbstractFonction //TitleWindow
    {
		private var _niveauUtilisateur:Number = Number(CvAccessManager.getSession().INFOS_DIVERS.CG.USER.IDNIVEAU);
		[Bindable]
		public var tabNGC:TabNavigator;
		[Bindable]
		public var gestionTemplateCollecte:VBox;
        public function FctGestionCollecteImpl()
        {
            super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
			
        }

        /**
         * Initialisation générale de cette classe posant les écouteurs captant
         * la fin de l'initialisation de tous les composants enfants.
         */
        public function init(e:FlexEvent):void
        {
			if (_niveauUtilisateur != 1)
				tabNGC.removeChild(gestionTemplateCollecte);
			
//            this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
			
        }

        /**
         * Initialisation générale de cette classe.
         */
        public function onCreationCompleteHandler(event:FlexEvent):void
        {
			
        }
		
		public function set niveauUtilisateur(value:Number):void
		{
			_niveauUtilisateur = value;
		}
		public function get niveauUtilisateur():Number
		{
			return _niveauUtilisateur;
		}

    }
}