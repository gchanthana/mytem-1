package service
{
    import composants.util.ConsoviewAlert;
    
    import entity.vo.CollecteClientVO;
    import entity.vo.CollecteTypeVO;
    import entity.vo.OperateurVO;
    import entity.vo.SimpleEntityVO;
    
    import event.FctGestionCollecteEvent;
    
    import flash.events.EventDispatcher;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.rpc.events.ResultEvent;
    import mx.utils.StringUtil;
    
    import utils.MessageAlerte;
	
    [Bindable]
    public class FctGestionCollecteDatas extends EventDispatcher
    {
        private var _collecteTypeVo:CollecteTypeVO;
        private var _collecteTypeVoList:ArrayCollection;
        private var _operateurList:ArrayCollection;
        private var _operateurWithTemplateList:ArrayCollection;
        private var _dataTypeList:ArrayCollection;
        private var _recupModeList:ArrayCollection;
        private var _uid:String = "";
        // CLIENT
        private var _racineList:ArrayCollection;
        private var _publishedTemplateList:ArrayCollection;
        private var _currentTemplateClientList:ArrayCollection;
        private var _collecteClientList:ArrayCollection;
        private var _identificationSuccess:Boolean;

        public function FctGestionCollecteDatas()
        {
            new CollecteTypeVO();
        }

		public function treatOperateurList(retour:Object):void
		{
			trace("Entrée dans treatOperateurList");
			this._operateurList = new ArrayCollection();
			var resultCollec:ArrayCollection = retour.RESULT as ArrayCollection;
			var currentVo:OperateurVO = new OperateurVO();
			if(resultCollec.length > 0)
			{
				for(var i:int = 0; i < resultCollec.length; i++)
				{
					currentVo = new OperateurVO();
					currentVo = currentVo.deserialize(resultCollec.getItemAt(i)) as OperateurVO;
					this._operateurList.addItem(currentVo);
				}
				this._operateurList.refresh();
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_SUCCESS));
			}
			else
			{
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_ERROR));
				trace("Erreur dans la récupération de la liste des opérateurs de collecte");
			}
			
		}
		
		public function treatOperateurWithTemplateList(retour:Object):void
		{
			this._operateurWithTemplateList = new ArrayCollection();
			var resultCollec:ArrayCollection = ArrayCollection(retour.RESULT);
			var currentVo:OperateurVO = new OperateurVO();
			if(resultCollec.length > 0)
			{
				for(var i:int = 0; i < resultCollec.length; i++)
				{
					currentVo = new OperateurVO();
					currentVo = currentVo.deserialize(resultCollec.getItemAt(i)) as OperateurVO;
					this._operateurWithTemplateList.addItem(currentVo);
				}
				this._operateurWithTemplateList.refresh();
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_SUCCESS));
			}
			else
			{
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_ERROR));
				trace("Erreur dans la récupération de la liste des opérateurs avec template ");
			}
		}
		
		public function treatDataTypeList(retour:Object):void
		{
			this._dataTypeList = new ArrayCollection();
			var resultCollec:ArrayCollection = ArrayCollection(retour.RESULT);
			var currentVo:SimpleEntityVO = new SimpleEntityVO();
			if(resultCollec.length > 0)
			{
				for(var i:int = 0; i < resultCollec.length; i++)
				{
					currentVo = new SimpleEntityVO();
					currentVo.type = "DataType";
					currentVo.label = resultCollec.getItemAt(i).LIBELLE_TYPE as String
					currentVo.value = new Number(resultCollec.getItemAt(i).IDCOLLECT_TYPE)
					this._dataTypeList.addItem(currentVo);
				}
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.DATATYPE_LIST_LOADED_SUCCESS));
			}
			else
			{
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.DATATYPE_LIST_LOADED_SUCCESS));
				trace("Erreur dans la récupération de la liste des types de données de collecte");
			}
		}
		
		public function treatRecupModeList(retour:Object):void
		{
			this._recupModeList = new ArrayCollection();
			var resultCollec:ArrayCollection = ArrayCollection(retour.RESULT);
			var currentVo:SimpleEntityVO = new SimpleEntityVO();
			if(resultCollec.length > 0)
			{
				for(var i:int = 0; i < resultCollec.length; i++)
				{
					currentVo = new SimpleEntityVO();
					currentVo.type = "RecupMode";
					currentVo.label = resultCollec.getItemAt(i).LIBELLE_MODE as String;
					currentVo.value = new Number(resultCollec.getItemAt(i).IDCOLLECT_MODE);
					this._recupModeList.addItem(currentVo);
				}
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_SUCCESS));
			}
			else
			{
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_ERROR));
				trace("Erreur dans la récupération de la liste des types de données de collecte");
			}	
		}
		
		public function treatCollecteTypeList(retour:Object):void
		{
			if(retour)
			{
				this.collecteTypeVoList = new ArrayCollection();
				var resultCollec:ArrayCollection = ArrayCollection(retour.RESULT);
				var currentVo:CollecteTypeVO = new CollecteTypeVO();
				if(resultCollec.length > 0)
				{
					for(var i:int = 0; i < resultCollec.length; i++)
					{
						currentVo = new CollecteTypeVO();
						currentVo = currentVo.deserialize(resultCollec.getItemAt(i)) as CollecteTypeVO;
						this._collecteTypeVoList.addItem(currentVo);
					}
					dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_SUCCESS));
				}
				else
				{
					dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_ERROR));
				}
			}
			else
			{
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_ERROR));
				trace("Erreur dans la récupération des Templates");
			}
		}
		
		public function treatEditCollecteTypeVo(retour:Object):void
		{
			if(retour.RESULT)
			{
				var num:Number = retour.RESULT as Number;
				switch(num)
				{
					case -10:
						trace("Champs obligatoires vides");
						dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, num));
						break;
					case -11:
						trace("Template existant");
						dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, num));
						break;
					case -12:
						trace("Impossible de publier");
						dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, num));
						break;
					default:
						dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_SUCCESS, num));
						break;
				}
			}
			else
			{
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR));
				trace("Erreur dans l'édition,création du template de collecte");
			}
		}
		
		public function treatRacineList(retour:Object):void
		{
			this._racineList = new ArrayCollection();
			var resultCollec:ArrayCollection = ArrayCollection(retour.RESULT);
			var item:Object = new Object();
			if(retour.RESULT)
			{
				for(var i:int = 0; i < resultCollec.length; i++)
				{
					item = new Object();
					item.RACINE = resultCollec.getItemAt(i).RACINE as String;
					item.IDRACINE = new Number(resultCollec.getItemAt(i).IDRACINE);
					item.NB_COLLECTES = new Number(resultCollec.getItemAt(i).NB_COLLECTES);
					this._racineList.addItem(item);
				}
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.RACINE_LIST_LOADED_SUCCESS));
			}
			else
			{
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.RACINE_LIST_LOADED_ERROR));
				trace("Erreur dans la récupération de la liste des racine");
			}
		}
		
		public function treatCollecteClientList(retour:Object):void
		{
			this._collecteClientList = new ArrayCollection();
			if(retour.RESULT)
			{
				//myDatas.collecteClientList = new ArrayCollection();
				var resultCollec:ArrayCollection = ArrayCollection(retour.RESULT);
				var currentVo:CollecteClientVO = new CollecteClientVO();
				if(resultCollec.length > 0)
				{
					for(var i:int = 0; i < resultCollec.length; i++)
					{
						currentVo = new CollecteClientVO();
						currentVo = currentVo.deserialize(resultCollec.getItemAt(i)) as CollecteClientVO;
						this._collecteClientList.addItem(currentVo);
					}
					dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS));
				}
				else
				{
					// cas : pas de collectes pour la racine
					dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS));
				}
			}
			else
			{
				// cas : pas de collectes pour la racine
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS));
				trace("Pas de collecte pour cette racine");
			}	
		}
		
		public function treatPublishedTemplate(retour:Object):void
		{
			if(retour.RESULT)
			{
				this._publishedTemplateList = new ArrayCollection();
				var resultCollec:ArrayCollection = ArrayCollection(retour.RESULT);
				var currentVo:CollecteTypeVO = new CollecteTypeVO();
				if(resultCollec.length > 0)
				{
					for(var i:int = 0; i < resultCollec.length; i++)
					{
						currentVo = new CollecteTypeVO();
						currentVo = currentVo.deserialize(resultCollec.getItemAt(i)) as CollecteTypeVO;
						this._publishedTemplateList.addItem(currentVo);
					}
					dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_SUCCESS));
				}
				else
				{
					dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_ERROR));
				}
			}
			else
			{
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_ERROR));
				trace("Erreur dans la récupération des Templates");
			}
		}
		
		public function treatEditCollecteClientVo(retour:Object):void
		{
			if(retour.RESULT > 0)
			{
				var idCollecte:Number = retour.RESULT as Number;
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_SUCCESS, idCollecte));
			}
			else
			{
				dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_ERROR));
				trace("Erreur dans l'édition,création de la collecte");
			}
		}
        public function get collecteTypeVo():CollecteTypeVO
        {
            return _collecteTypeVo;
        }

        public function set collecteTypeVO(value:CollecteTypeVO):void
        {
            _collecteTypeVo = value;
        }

        public function get collecteTypeVoList():ArrayCollection
        {
            return _collecteTypeVoList;
        }

        public function set collecteTypeVoList(value:ArrayCollection):void
        {
            _collecteTypeVoList = value;
        }

        public function get operateurList():ArrayCollection
        {
            return _operateurList;
        }

        public function set operateurList(value:ArrayCollection):void
        {
            _operateurList = value;
        }

        public function get dataTypeList():ArrayCollection
        {
            return _dataTypeList;
        }

        public function set dataTypeList(value:ArrayCollection):void
        {
            _dataTypeList = value;
        }

        public function get recupModeList():ArrayCollection
        {
            return _recupModeList;
        }

        public function set recupModeList(value:ArrayCollection):void
        {
            _recupModeList = value;
        }

        public function get currentTemplateClientList():ArrayCollection
        {
            return _currentTemplateClientList;
        }

        public function set currentTemplateClientList(value:ArrayCollection):void
        {
            _currentTemplateClientList = value;
        }

        public function get collecteClientList():ArrayCollection
        {
            return _collecteClientList;
        }

        public function set collecteClientList(value:ArrayCollection):void
        {
            _collecteClientList = value;
        }

        public function get publishedTemplateList():ArrayCollection
        {
            return _publishedTemplateList;
        }

        public function set publishedTemplateList(value:ArrayCollection):void
        {
            _publishedTemplateList = value;
        }

        public function get identificationSuccess():Boolean
        {
            return _identificationSuccess;
        }

        public function set identificationSuccess(value:Boolean):void
        {
            _identificationSuccess = value;
        }

        public function get racineList():ArrayCollection
        {
            return _racineList;
        }

        public function set racineList(value:ArrayCollection):void
        {
            _racineList = value;
        }

        public function get uid():String
        {
            return _uid;
        }

        public function set uid(value:String):void
        {
            _uid = value;
        }

        public function get operateurWithTemplateList():ArrayCollection
        {
            return _operateurWithTemplateList;
        }

        public function set operateurWithTemplateList(value:ArrayCollection):void
        {
            _operateurWithTemplateList = value;
        }
    }
}