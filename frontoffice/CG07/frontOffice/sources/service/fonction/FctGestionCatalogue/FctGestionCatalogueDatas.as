package service.fonction.FctGestionCatalogue
{
	import entity.EqpFournisseurVo;
	import entity.EqpIcecatVo;
	
	import event.fonction.fctGestionCatalogue.FctGestionCatalogueEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class FctGestionCatalogueDatas extends EventDispatcher
	{
		private var _LISTEEQPICECAT:ArrayCollection = new ArrayCollection();
		private var _LISTEEQPFOURNISSEUR:ArrayCollection = new ArrayCollection();
		private var _NB_EQPICECAT:int = -1;
		private var _NB_EQUIPEMENT:int = -1;
		
		public function treatProcessGetData(objResult:Object):void
		{
			if(objResult.hasOwnProperty("LISTEEQPICECAT"))
				treatProcessGetListeEqpIcecat(objResult.LISTEEQPICECAT,false);
			if(objResult.hasOwnProperty("LISTEEQPFOURNISSEUR"))
				treatProcessGetListeEqpFournisseur(objResult.LISTEEQPFOURNISSEUR,false);	
			
			this.dispatchEvent(new FctGestionCatalogueEvent(FctGestionCatalogueEvent.GET_DATA_EVENT));
		}
		public function treatProcessGetListeEqpIcecat(objResult:ArrayCollection,sendEvent:Boolean=true):void
		{
			_LISTEEQPICECAT.removeAll();
			for each(var obj:Object in objResult)
			{
				var eqp:EqpIcecatVo = new EqpIcecatVo();
				eqp.fill(obj);
				LISTEEQPICECAT.addItem(eqp);
				if(NB_EQPICECAT != obj.NB_ROW)
					NB_EQPICECAT = obj.NB_ROW;
			}
			if(sendEvent)
				this.dispatchEvent(new FctGestionCatalogueEvent(FctGestionCatalogueEvent.LISTE_EQPICECAT_EVENT));
		}
		public function treatProcessGetListeEqpFournisseur(objResult:ArrayCollection,sendEvent:Boolean=true):void
		{
			_LISTEEQPFOURNISSEUR.removeAll();
			for each(var obj:Object in objResult)
			{
				var eqp:EqpFournisseurVo = new EqpFournisseurVo();
				eqp.fill(obj);
				LISTEEQPFOURNISSEUR.addItem(eqp);
				if(NB_EQUIPEMENT != obj.NB_ROW)
					NB_EQUIPEMENT = obj.NB_ROW;
			}
			if(sendEvent)
				this.dispatchEvent(new FctGestionCatalogueEvent(FctGestionCatalogueEvent.LISTE_EQPFOURNISSEUR_EVENT));
		}
		public function treatProcessGiveAutorisation():void
		{
			this.dispatchEvent(new FctGestionCatalogueEvent(FctGestionCatalogueEvent.GIVE_AUTORISATION_EVENT));
		}
		public function treatProcessInjecterEqp():void
		{
			this.dispatchEvent(new FctGestionCatalogueEvent(FctGestionCatalogueEvent.INJECTER_EQP_EVENT));
		}
		public function treatProcessLierEqp():void
		{
			this.dispatchEvent(new FctGestionCatalogueEvent(FctGestionCatalogueEvent.LIER_EQP_EVENT));
		}
		
		public function get LISTEEQPFOURNISSEUR():ArrayCollection { return _LISTEEQPFOURNISSEUR; }
		public function set LISTEEQPFOURNISSEUR(value:ArrayCollection):void
		{
			if (_LISTEEQPFOURNISSEUR == value)
				return;
			_LISTEEQPFOURNISSEUR = value;
		}
		public function get LISTEEQPICECAT():ArrayCollection { return _LISTEEQPICECAT; }
		public function set LISTEEQPICECAT(value:ArrayCollection):void
		{
			if (_LISTEEQPICECAT == value)
				return;
			_LISTEEQPICECAT = value;
		}
		public function get NB_EQUIPEMENT():int { return _NB_EQUIPEMENT; }
		public function set NB_EQUIPEMENT(value:int):void
		{
			if (_NB_EQUIPEMENT == value)
				return;
			_NB_EQUIPEMENT = value;
		}
		public function get NB_EQPICECAT():int { return _NB_EQPICECAT; }
		public function set NB_EQPICECAT(value:int):void
		{
			if (_NB_EQPICECAT == value)
				return;
			_NB_EQPICECAT = value;
		}
		
	}
}