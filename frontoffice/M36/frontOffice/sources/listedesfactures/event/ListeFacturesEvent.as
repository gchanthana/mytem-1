package listedesfactures.event
{
	import flash.events.Event;
	
	/**
	 * <b>Classe <code>ListeFacturesEvent</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */	
	
	public class ListeFacturesEvent extends Event
	{
		public static var LF_REQUEST_ERROR:String = "lfRequestError"; 
		public static var LF_REQUEST_COMPELTE:String = "lfRequestComplete";
		public static var LISTE_FACTURE_COMPELTE:String = "lfRequestComplete";
		
		/**
		 * <b>Constructeur <code>ListeFacturesEvent()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe ListeFacturesEvent.
		 * </pre></p>
		 *
		 */
		public function ListeFacturesEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		/**
		 * <b>Fonction <code>clone()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * appel un nouveau objet ListeFacturesEvent
		 * </pre></p>
		 * 
		 * @return ListeFacturesEvent
		 * 
		 */	
		override public function clone():Event
		{
			return new ListeFacturesEvent(type,bubbles,cancelable);
		}
	}
}