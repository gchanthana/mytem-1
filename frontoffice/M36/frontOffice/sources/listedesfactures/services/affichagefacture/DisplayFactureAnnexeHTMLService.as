package listedesfactures.services.affichagefacture
{
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import listedesfactures.entity.FactureDisplayParamsVo;
	
	/**
	 * <b>Classe <code>DisplayFactureAnnexeHTMLService</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 01.09.2010
	 * </pre></p> 
	 * 
	 */
	public class DisplayFactureAnnexeHTMLService implements IDisplayFacture
	{
		private var _params:FactureDisplayParamsVo;
		
		/**
		 * <b>Constructeur <code>DisplayFactureAnnexeHTMLService()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe DisplayFactureAnnexeHTMLService.
		 * </pre></p>
		 * 
		 * @param params : <code>FactureDisplayParamsVo</code>
		 * 
		 */	
		public function DisplayFactureAnnexeHTMLService(params:FactureDisplayParamsVo)
		{
			_params = params;
		}
		
		/**
		 * 
		 *<b>Fonction <code>displayFacture()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Ouvre une fenêtre pour afficher l'annexe de la facture
		 * </pre></p> 
		 * 
		 */		
		public function displayFacture():void
		{
			//TODO: implement function
			var url:String=moduleListeDesFacturesIHM.urlBackoffice + "/fr/consotel/consoview/cfm/factures/display_Facture.cfm";
			
			var variables:URLVariables=new URLVariables();
			
			variables.IDX_PERIMETRE=_params.perimetre_id;
			variables.type_perimetre=_params.perimetre_type;
			
			variables.ID_FACTURE=_params.facture.IDINVENTAIRE_PERIODE;
			
			var request:URLRequest=new URLRequest(url);
			request.data=variables;
			request.method="POST";
			navigateToURL(request, "_blank");
		}
	}
}