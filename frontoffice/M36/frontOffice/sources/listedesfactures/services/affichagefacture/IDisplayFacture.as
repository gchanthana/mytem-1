package listedesfactures.services.affichagefacture
{
	import listedesfactures.entity.FactureDisplayParamsVo;

	public interface IDisplayFacture
	{
		function displayFacture():void;
	}
}