package listedesfactures.services.operateurs
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	/**
	 * <b>Classe <code>OperateurHandler</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */
	
	[Bindable]
	public class OperateurHandler
	{	
		private var _model:OperateursModel;
		
		/**
		 * <b>Constructeur <code>OperateurHandler(model:ListeFacturesModel)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe OperateurHandler.
		 * </pre></p>
		 */
		public function OperateurHandler(model:OperateursModel)
		{
			_model = model;
		}
		
		/**
		 *<b>Fonction <code>getListeOperateursResultHandler(event:ResultEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Fait appel à la méthode updateListeOperateurs de la classe OperateursModel.
		 * </pre></p>
		 *  
		 * @param event
		 * 
		 */	
		public function getListeOperateursResultHandler(event:ResultEvent):void{
			_model.updateListeOperateurs(event.result as ArrayCollection);
		}
	}
}