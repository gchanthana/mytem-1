package listedesfactures.services.operateurs
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import listedesfactures.entity.OperateurVo;
	
	import mx.rpc.AbstractOperation;
	
	/**
	 * <b>Classe <code>OperateursService</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */
	
	[Bindable]
	public class OperateursService
	{
		public var model:OperateursModel;
		public var handlers:OperateurHandler;
		
		/**
		 * <b>Constructeur <code>OperateursService()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe OperateursService.
		 * </pre></p>
		 */	
		public function OperateursService()
		{
			model = new OperateursModel();
			handlers = new OperateurHandler(model);
		}
		
		/**
		 * <b>Fonction <code>getListeOperateurs()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Appel le service ListeFactureService.getListeOperateur
		 * </pre></p>
		 * 
		 */	
		public function getListeOperateurs():void{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
											"fr.consotel.consoview.M36.ListeFactureService",
											"getListeOperateur",
											handlers.getListeOperateursResultHandler);
			opData.makeObjectsBindable = true;
			RemoteObjectUtil.callService(opData);
			
		}
	}
}