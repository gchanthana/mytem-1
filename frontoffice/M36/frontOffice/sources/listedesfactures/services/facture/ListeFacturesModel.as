package listedesfactures.services.facture
{
	import flash.events.EventDispatcher;
	
	import listedesfactures.entity.FactureVo;
	import listedesfactures.event.ListeFacturesEvent;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * <b>Classe <code>ListeFacturesModel</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */
	
	[Bindable]
	public class ListeFacturesModel extends EventDispatcher
	{
		public var listeFacture:ArrayCollection;
		
		private var _nbTotalItem:Number = 0;
		
		
		/**
		 * <b>Constructeur <code>ListeFacturesModel()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe ListeFacturesModel.
		 * </pre></p>
		 */
		public function ListeFacturesModel()
		{
			
		}
		
		/**
		 * <b>Fonction <code>updateListeFacture(values:ArrayCollection)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Remplir le Paginatedatagrid. 
		 * Dispatche deux evenements : ListeFacturesEvent.LF_REQUEST_COMPELTE et ListeFacturesEvent.LISTE_FACTURE_COMPELTE.
		 * </pre></p>
		 *  
		 * @param values : <code>ArrayCollection</code>
		 * 
		 */	
		public function updateListeFacture(values:ArrayCollection):void
		{		
			
			var tmplisteFacture:ArrayCollection = values;
			
			listeFacture = null;
			listeFacture = new ArrayCollection();
			
			if(tmplisteFacture.length > 0){
				nbTotalItem = tmplisteFacture[0].ROWN_TOTAL;
			}
			else{
				nbTotalItem = 0;
			}
			
			var fvo:FactureVo;
			for (var i:int=0; i < tmplisteFacture.length; i++)
			{
				fvo = new FactureVo();
				fvo.fill(tmplisteFacture[i]);
				listeFacture.addItem(fvo);
			}
			
			dispatchEvent(new ListeFacturesEvent(ListeFacturesEvent.LF_REQUEST_COMPELTE));
			dispatchEvent(new ListeFacturesEvent(ListeFacturesEvent.LISTE_FACTURE_COMPELTE,true));
			
		}

		/**
		 * <b>Fonction <code>nbTotalItem():Number</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Le getter de nbTotalItem
		 * </pre></p>
		 *  
		 * @return _nbTotalItem : <code>Number</code>
		 * 
		 */	
		public function get nbTotalItem():Number
		{
			return _nbTotalItem;
		}
		
		/**
		 * <b>Fonction <code>nbTotalItem(value:Number)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Le setter de nbTotalItem
		 * </pre></p>
		 * 
		 * @param value : <code>Number</code>
		 * 
		 */		
		public function set nbTotalItem(value:Number):void
		{
			_nbTotalItem = value;
		}

	}
}