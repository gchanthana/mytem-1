package listedesfactures.services.facture
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	/**
	 * <b>Classe <code>ListeFacturesHandler</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */
	
	[Bindable]
	public class ListeFacturesHandler
	{
		
		private var _model:ListeFacturesModel;
		
		/**
		 * <b>Constructeur <code>ListeFacturesHandler(model:ListeFacturesModel)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe ListeFacturesHandler.
		 * </pre></p>
		 */
		public function ListeFacturesHandler(model:ListeFacturesModel)
		{
			_model = model;
		}
		
		/**
		 *<b>Fonction <code>getListeFactureResultHandler(event:ResultEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Fait appel à la méthode updateListeFacture de la classe ListeFacturesModel.
		 * </pre></p>
		 *  
		 * @param event
		 * 
		 */		
		public function getListeFactureResultHandler(event:ResultEvent):void
		{
			_model.updateListeFacture(event.result as ArrayCollection);
		}
		
	}
}