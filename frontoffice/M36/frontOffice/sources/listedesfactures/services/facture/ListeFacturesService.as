package listedesfactures.services.facture
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import listedesfactures.entity.CriteresRechercheVo;
	
	import mx.rpc.AbstractOperation;
	
	/**
	 * <b>Classe <code>ListeFacturesService</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */
	
	[Bindable]
	public class ListeFacturesService
	{
		public var model:ListeFacturesModel;
		public var handlers:ListeFacturesHandler;
		
		/**
		 * <b>Constructeur <code>ListeFacturesService()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe ListeFacturesService.
		 * </pre></p>
		 */		
		public function ListeFacturesService()
		{
			model = new ListeFacturesModel();
			handlers = new ListeFacturesHandler(model);
		}
		
		/**
		 * <b>Fonction <code>getListeFacture(values:CriteresRechercheVo)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Exporter la liste des factures séléctionnée en format XLS, CSV ou PDF.
		 * </pre></p>
		 * 
		 * @param values : <code>CriteresRechercheVo</code>
		 * 
		 */		
		public function getListeFacture(values:CriteresRechercheVo):void
		{	
			
			var array_searchable:Array=values.cle.split(",");
			var array_orderable:Array=values.tri.split(",");
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.M36.ListeFactureService",
									"getPaginateListeFacture",
									handlers.getListeFactureResultHandler);
			
			opData.makeObjectsBindable = true;
			RemoteObjectUtil.callService(opData,
										values.dateDebut,
										values.dateFin,
										values.operateurId,
										array_searchable[0],
										array_searchable[1],
										array_orderable[0],
										array_orderable[1],
										values.offset,
										values.limit);	
		
		}
	}
}