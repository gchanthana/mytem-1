package listedesfactures.services.exports
{
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import listedesfactures.entity.CriteresRechercheVo;
	
	/**
	 * <b>Classe <code>ExportsService</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */
	
	[Bindable]
	public class ExportsService
	{	
		/**
		 * <b>Constructeur <code>ExportsService()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe ExportsService.
		 * </pre></p>
		 */
		public function ExportsService()
		{
		}
		
		/**
		 * <b>Fonction <code>getParameter()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Exporter la liste des factures séléctionnée en format XLS, CSV ou PDF.
		 * </pre></p>
		 * 
		 * @param format : <code>String</code> le format d'export
		 * @param datedebut : <code>String</code> début de la période
		 * @param datefin : <code>String</code>
		 * @param nbTotalItem : <code>Number</code> fin de la période
		 * @param params : <code>CriteresRechercheVo</code> paramètres de recherche
		 * 
		 */		
		public function ExportsListeFactures(format:String, datedebut:String, datefin:String, nbTotalItem:Number, params:CriteresRechercheVo):void{
			
			var array_searchable:Array=params.cle.split(",");
			var array_orderable:Array=params.tri.split(",");
			
			var perimetreChar:String = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			
			var url:String=moduleListeDesFacturesIHM.urlBackoffice + "/fr/consotel/consoview/M36/exports/ExportListeFactures"+perimetreChar+".cfm";
			
			var variables:URLVariables=new URLVariables();
			
			variables.OPERATEURID=params.operateurId;
			variables.DATEDEB=datedebut;
			variables.DATEFIN=datefin;
			variables.SEARCH_TEXT_COLONNE=array_searchable[0];
			variables.SEARCH_TEXT=array_searchable[1];
			variables.ORDER_BY_TEXTE_COLONNE=array_orderable[0];
			variables.ORDER_BY_TEXTE=array_orderable[1];
			variables.OFFSET=params.offset;
			variables.LIMIT=nbTotalItem;
			variables.FORMAT=format.toUpperCase();
			
			
			var request:URLRequest=new URLRequest(url);
			request.data=variables;
			request.method="POST";
			navigateToURL(request, "_blank");
			
		}
		
	}
}