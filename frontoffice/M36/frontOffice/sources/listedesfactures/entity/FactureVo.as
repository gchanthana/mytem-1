package listedesfactures.entity
{
	import mx.collections.ArrayCollection;
	
	/**
	 * <b>Classe <code>FactureVo</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */
	
	[Bindable]
	public class FactureVo
	{
		
		public var NBRECORD:Number;
		public var ROWN_TOTAL:Number;
		public var NOM:String;
		public var BY_CODE_SITE:String;
		public var NUMERO_FACTURE:String;
		public var DATEDEB:Date;
		public var DATEFIN:Date;
		public var BY_COMMUNE:String;
		public var IDINVENTAIRE_PERIODE:String;
		public var MONTANT:String;
		public var BY_ADRESSE1:String;
		public var BY_ADRESSE2:String;
		public var BY_ZIPCODE:String;
		public var COMPTE_FACTURATION:String;
		public var ROWN:Number;
		public var LIBELLE:String;
		public var IDREF_CLIENT:Number;
		public var DATE_EMISSION:Date;
		
		public var STR_DATE_EMISSION:String="";
		public var STR_DATEDEB:String="";
		public var STR_DATEFIN:String="";
		
		/**
		 * <b>Constructeur <code>FactureVo()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe FactureVo.
		 * </pre></p>
		 */
		public function FactureVo()
		{
		}
		
		/**
		 * <b>Fonction <code>fill(value:Object)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Set de FactureVo
		 * </pre></p>
		 *
		 * @value Object 
		 *
		 */
		public function fill(value:Object):void
		{
			this.NBRECORD = value.ROWN_TOTAL;
			this.ROWN_TOTAL = value.ROWN_TOTAL;
			this.NOM = value.NOM;
			this.BY_CODE_SITE = value.BY_CODE_SITE;
			this.NUMERO_FACTURE = value.NUMERO_FACTURE;
			this.DATEDEB = value.DATEDEB;
			this.BY_COMMUNE = value.BY_COMMUNE;
			this.IDINVENTAIRE_PERIODE = value.IDINVENTAIRE_PERIODE;
			this.MONTANT = value.MONTANT;
			this.DATEFIN = value.DATEFIN;
			this.BY_ADRESSE1 = value.BY_ADRESSE1;
			this.BY_ADRESSE2 = value.BY_ADRESSE2;
			this.BY_ZIPCODE = value.BY_ZIPCODE;
			this.COMPTE_FACTURATION = value.COMPTE_FACTURATION;
			this.ROWN = value.ROWN;
			this.LIBELLE = value.LIBELLE;
			this.IDREF_CLIENT = value.IDREF_CLIENT;
			this.DATE_EMISSION = value.DATE_EMISSION;
			this.STR_DATE_EMISSION = value.STR_DATE_EMISSION;
			this.STR_DATEDEB = value.STR_DATEDEB;
			this.STR_DATEFIN = value.STR_DATEFIN;
		}

	}
}