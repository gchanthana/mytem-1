package listedesfactures.entity
{	
	/**
	 * <b>Classe <code>FactureDisplayParamsVo</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */
	
	[Bindable]
	public class FactureDisplayParamsVo
	{
		
		public var format:String;
		public var type:Number;
		public var perimetre_id:Number;
		public var perimetre_type:String;
		public var perimetre_libelle:String;
		public var facture:FactureVo;
		
		/**
		 * <b>Constructeur <code>FactureDisplayParamsVo()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe FactureDisplayParamsVo.
		 * </pre></p>
		 */
		public function FactureDisplayParamsVo(facture:FactureVo,format:String,type:Number):void{
			this.format=format;
			this.type=type;
			this.perimetre_id=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			this.perimetre_type=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
			this.perimetre_libelle=CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			this.facture=facture;
		}
		
		/**
		 * <b>Fonction <code>getParameter()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Get de FactureDisplayParamsVo
		 * </pre></p>
		 *
		 */
		public function getParameters():FactureDisplayParamsVo{
			return this;
		}
		
	}
}