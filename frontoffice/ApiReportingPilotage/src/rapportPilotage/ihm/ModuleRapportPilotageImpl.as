package rapportPilotage.ihm
{
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import rapportPilotage.event.RapportIHMEvent;
	import rapportPilotage.event.RapportSelectedEvent;
	import rapportPilotage.utils.ManagerPopUp;
	import rapportPilotage.vo.ApiReportingSingleton;
	import rapportPilotage.vo.Rapport;

	[Bindable]
	public class ModuleRapportPilotageImpl extends ManagerPopUp
	{
		public var selectedRapport: Rapport;			
		public var rapportPilotageIHM :ListRapportPilotageIHM;
		public var paramRapport:ParametreRapportPilotageIHM;

		
		public function modulerapportpilotageimpl1_creationCompleteHandler(event:RapportSelectedEvent):void
		{
			systemManager.addEventListener(RapportSelectedEvent.DATA_TRANSFER_RAPPORT,handleDataTransfer, true);
		}
		
		public function handleDataTransfer(event:RapportSelectedEvent):void
		{
			selectedRapport=event.selectedRapport;
			paramRapport.initData(selectedRapport);
		}
		
		/*-----------------------------------------------------------------------------------*/
			/* lors que le composant est ajouté à son containre on déclenche l'evenement*/
		/*----------------------------------------------------------------------------------*/
		
		public function ModuleRapportPilotageImpl()
		{		
			addEventListener(FlexEvent.ADD,addHandler); // évènements standard ADD: Le composant a été ajouté à un conteneur
		}
		
		/*-----------------------------------------------------------------------*/
				/* tester si le module est une popup ou un composant*/ 
		/*-----------------------------------------------------------------------*/
		
		private function addHandler(evt:FlexEvent):void
		{
			if(isPopUp) // une fonction native flex 
			{
				width=946; 
				styleName="popUpBox";
			
				ApiReportingSingleton.nbInstanciationPopUP+=1;
				showCloseButtonRapport(true);
			}
			else
			{
				width=1000;
				this.setStyle('borderStyle','none');
				this.setStyle('headerHeight','0');
				this.setStyle('textIndent','0');
				ApiReportingSingleton.nbInstanciation+=1;
				hideCloseButtonRapport(false);
			}
		}
		
		/*------------------------------------------------------------------------*/
				/* permert d'afficher la croix ( pour fermer la fênetre )*/
		/*------------------------------------------------------------------------*/
		
		public function showCloseButtonRapport(value: Boolean):void
		{
			this.showCloseButton=value;
			
			if(paramRapport != null )
			{	
				paramRapport.resetDataIHMSingleton();
				rapportPilotageIHM.getAllListeRapport(); // chercher  la liste des rapports 
			}
		}
		
		/*------------------------------------------------------------------------*/
			/* permert de masquer la croix qui ferme la fênetre de la popup */
		/*------------------------------------------------------------------------*/
		
		public function hideCloseButtonRapport(value:Boolean):void
		{
			this.showCloseButton=value;
			
			if(ApiReportingSingleton.nbInstanciation >1 ||  ApiReportingSingleton.nbInstanciationPopUP >=1)
			{			
				paramRapport.resetDataIHMSingleton();
				rapportPilotageIHM.getAllListeRapport(); // chercher  la liste des rapports 
			}
		}
		
		/*------------------------------------------------------------------------*/
								/* fermer la popup*/
		/*------------------------------------------------------------------------*/
		
		public function closePopUPRapportPilotage(event:RapportIHMEvent):void
		{
			PopUpManager.removePopUp(this);	// fermer le popup	
			this.x=0;
			this.y=0;			
		}
			
	}
}