package rapportPilotage.ihm
{
	import composants.util.ConsoviewAlert;
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import mx.collections.ArrayCollection;
	import mx.containers.Form;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.TextArea;
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.service.chercherselector.RechercherSelectorService;
	import rapportPilotage.service.executerrapport.ExecuterRapportService;
	import rapportPilotage.utils.SelectorAbstract;
	import rapportPilotage.utils.SimpleFactory;
	import rapportPilotage.vo.Rapport;
	
	[Bindable]
	public class ParametreRapportPilotageImpl extends VBox
	{
		public var executerRapportService : ExecuterRapportService;
		public var rechercherSelectorService :RechercherSelectorService;
		public var allSelector :ArrayCollection;
		public var selectedRapport:Rapport;
		public var descriptionRapport:TextArea;
		
		public var newComposant:SelectorAbstract;
		public var btn_valider_execution:Button;
		public var formComposants:Form;
		public var tabComposantSelector:Array = new Array();
		
		
		public function ParametreRapportPilotageImpl()
		{
			super();
		}
		
		/*-----------------------------------------------------------------------------*/
			// permet d'effacer les paramètres  du rapport et sa description
		/*-----------------------------------------------------------------------------*/
		
		public function resetDataIHMSingleton():void
		{
			while(formComposants.numChildren >0)
			{
				var child:DisplayObject=formComposants.getChildAt(0); // l'index de l'element reste toujours 0 après chaque suppression
				formComposants.removeChildAt(0);
				child=null;		
			}
			this.selectedRapport = null;
			btn_valider_execution.enabled=false;
		}
		
		public function btn_valider_execution_clickHandler(event:MouseEvent):void
		{
			this.validerClickHandler();
		}

		
		/*--------------------------------------------------------------------------------*/
		// appeler le service permettant de récupérer les paramètres (selecteurs)
		/*--------------------------------------------------------------------------------*/
		
		public function initData(selectedRapport:Rapport):void
		{
			if(allSelector!=null) // supprimer les composants ajoutés au rapportParam.formComposants par le factory
			{
				while(formComposants.numChildren >0)
				{
					var child:DisplayObject= formComposants.getChildAt(0); // l'index de l'element reste toujours 0 apères chaque suppression
					formComposants.removeChildAt(0);
					child=null;	
				}
				this.tabComposantSelector=[];
			}
			btn_valider_execution.enabled=true;
			
			rechercherSelectorService= new RechercherSelectorService();	
			rechercherSelectorService.model.addEventListener(RapportEvent.USER_SELECTOR_UPDATED_EVENT,buildIHMHandler)
			rechercherSelectorService.getSelector(selectedRapport.id);
			this.selectedRapport=selectedRapport;
		}
		
		/*--------------------------------------------------------------------------------*/
		// creer IHM (un composant par selecteur )
		/*--------------------------------------------------------------------------------*/
		
		public function buildIHMHandler(event:RapportEvent):void
		{
			allSelector=rechercherSelectorService.model.selectors;// contient tous les selecteur retourné du back office
			var simpleFactory:SimpleFactory=new SimpleFactory();
			
			
			for(var i:int=0; i<allSelector.length;i++) // creer les selector 		
			{
				newComposant=simpleFactory.createComposants(allSelector[i]);// creer le selecteur
				newComposant.initRapport(this.selectedRapport);
				
				if(!newComposant.is_hiddenComposant)//ajouter le composant au IHM 
				{
					formComposants.addChild(newComposant)
				}	
				
				tabComposantSelector.push(newComposant);// ajouter le composant dans un tableau 
			}	
		}
		
		/*--------------------------------------------------------------------------------*/
		//fontion qui permet d'envoyer les paramètres du rapport en vu de son execution 
		/*--------------------------------------------------------------------------------*/
		
		private function validerClickHandler():void
		{
			var tabParametre:Array = []; 
			var isOk:Boolean=true;
			var errorMessage:String="";
			
			for(var i:int=0; i<allSelector.length;i++) 
			{
				tabParametre[i] = (tabComposantSelector[i] as SelectorAbstract).getSelectedValue();// récupérer les valeurs de chaque selector
				isOk=isOk && (tabComposantSelector[i] as SelectorAbstract).isOk;
				if(!isOk)
				{
					errorMessage=errorMessage+"\n"+(tabComposantSelector[i] as SelectorAbstract).errorMessage
				}
			}
			if(isOk)// pas d'erreur, on execute le rapport
			{
				executerRapportService=new ExecuterRapportService();
				executerRapportService.executeRapport(tabParametre,selectedRapport);// executer le rapport
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo(errorMessage,'Alerte',null);
			}
		}
	}
}