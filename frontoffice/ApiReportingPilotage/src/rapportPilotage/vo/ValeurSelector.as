package rapportPilotage.vo
{
	public class ValeurSelector
	{
		
		private var _id:int;
		private var _valeur: String;
		private var _libelle:String;
		
		private var _idPeriodeMois: int; // l'id dans la base par exemple 133 date debut , 
		private var _dispo_mois:String;
		private var _libelle_periode : String
		
		private var _U_IDPERIOD_DEB:int; // pour la combobox periode 
		private var _U_IDPERIOD_FIN:int ;// pour la combobox periode 
		
		public function ValeurSelector(){}
		

		

		public function fill(valeurSelector:Object):void
		{
			/*for (var i: String in valeurSelector) 
			{ 
				trace(i + ": " + valeurSelector[i]); 
			} */
			
			this._id=valeurSelector.ID;
			this._valeur=valeurSelector.VALEUR;
			this._libelle=valeurSelector.LIBELLE;
			
			this._idPeriodeMois=valeurSelector.IDPERIODE_MOIS;
			this._dispo_mois=valeurSelector.DISP_MOIS;
			this._libelle_periode=valeurSelector.LIBELLE_PERIODE ;
			
			this.U_IDPERIOD_DEB=valeurSelector.P_DEB;
			this.U_IDPERIOD_FIN=valeurSelector.P_FIN;
		}
		
		public function get libelle():String
		{
			return _libelle;
		}

		public function set libelle(value:String):void
		{
			_libelle = value;
		}
		
		public function get id():int
		{
			return _id;
		}
		
		public function set id(value:int):void
		{
			_id = value;
		}

		public function get valeur():String
		{
			return _valeur;
		}

		public function set valeur(value:String):void
		{
			_valeur = value;
		}
		
		public function get idPeriodeMois():int
		{
			return _idPeriodeMois;
		}
		
		public function set idPeriodeMois(value:int):void
		{
			_idPeriodeMois = value;
		}
		
		public function get U_IDPERIOD_FIN():int
		{
			return _U_IDPERIOD_FIN;
		}
		
		public function set U_IDPERIOD_FIN(value:int):void
		{
			_U_IDPERIOD_FIN = value;
		}
		
		public function get U_IDPERIOD_DEB():int
		{
			return _U_IDPERIOD_DEB;
		}
		
		public function set U_IDPERIOD_DEB(value:int):void
		{
			_U_IDPERIOD_DEB = value;
		}
		
		public function get dispo_mois():String
		{
			return _dispo_mois;
		}
		
		public function set dispo_mois(value:String):void
		{
			_dispo_mois = value;
		}
		
		public function get libelle_periode():String
		{
			return _libelle_periode;
		}
		
		public function set libelle_periode(value:String):void
		{
			_libelle_periode = value;
		}
		
	}
}