package rapportPilotage.service.poolsselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class PoolSelectorHandler
	{
		private var _model: PoolSelectorModel;
		
		public function PoolSelectorHandler(_model:PoolSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValuesPoolHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateValues(event.result as ArrayCollection);
			}
		}	
	}
}