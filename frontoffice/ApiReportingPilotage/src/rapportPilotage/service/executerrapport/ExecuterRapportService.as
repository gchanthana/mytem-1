package rapportPilotage.service.executerrapport
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.managers.CursorManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import rapportPilotage.service.executerrapport.*;
	import rapportPilotage.vo.Rapport;

	public class ExecuterRapportService
	{
		public function executeRapport(tabParametre:Array,selectedRapport :Rapport):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M332.service.ExecuterRapportService","executeRapport",getResultHandler); 
			
			RemoteObjectUtil.callService(op,tabParametre,selectedRapport);	// appel au service coldfusion 
		}
		
		private function getResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				ConsoviewAlert.afficherOKImage("Votre demande de rapport a bien été prise en compte");
			}
			else
			{
				//ConsoviewAlert.afficherError("Une erreur s'est produit lors de la génération de votre rapport",'Erreur d\'execution');
				trace('erreur execution rapport');
			}

		}
	}
}