package rapportPilotage.service.formattemplatetelector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import rapportPilotage.service.formattemplatetelector.*;

	public class FormatTemplateSelectorService
	{
		private var _model:FormatTemplateSelectorModel;
		public var handler:FormatTemplateSelectorHandler;
				
		public function FormatTemplateSelectorService()
		{
			this._model = new FormatTemplateSelectorModel();
			this.handler = new FormatTemplateSelectorHandler(model);
		}		
		
		public function get model():FormatTemplateSelectorModel
		{
			return this._model;
		}
		
		public function getValuesFormatTemplate(idRapportRacine:int,idTemplate :int):void
		{				
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M332.service.selector.FormatTemplateSelectorService","getValuesFormatTemplate",handler.getValuesFormatHandler); 
			
			RemoteObjectUtil.callService(op,idRapportRacine,idTemplate);// appel au service coldfusion qui contient des fonctions pour les opérations de service
		}
	}
}