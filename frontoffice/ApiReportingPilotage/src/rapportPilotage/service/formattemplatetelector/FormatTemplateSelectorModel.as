package rapportPilotage.service.formattemplatetelector
{
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.vo.ValeurSelector;

	internal class FormatTemplateSelectorModel  extends EventDispatcher
	{
		private var _formats :ArrayCollection;// array collection pour récupérer les formatst
		
		public function FormatTemplateSelectorModel()
		{
			_formats = new ArrayCollection();
		}
		public function get formats():ArrayCollection
		{
			return _formats;
		}
		
		internal function updateValues(value:ArrayCollection):void
		{	
			_formats.removeAll();// vider l'ArrayCollection		
			var valeurSelector:ValeurSelector;
	
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelector();
				valeurSelector.fill(value[i]);
				_formats.addItem(valeurSelector); 
			}
			this.dispatchEvent(new RapportEvent(RapportEvent.USER_FORMAT_SELECTOR_EVENT));
		}
	}
}