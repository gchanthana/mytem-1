package rapportPilotage.service.topselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class TopSelectorHandler
	{
		private var _model: TopSelectorModel;
		
		public function TopSelectorHandler(_model:TopSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValuesTopHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateValues(event.result as ArrayCollection);
			}
		}	
	}
}