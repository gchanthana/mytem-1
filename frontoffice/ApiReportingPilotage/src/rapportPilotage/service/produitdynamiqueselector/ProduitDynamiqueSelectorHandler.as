package rapportPilotage.service.produitdynamiqueselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ProduitDynamiqueSelectorHandler
	{
		private var  _model:ProduitDynamiqueSelectorModel;
		
		public function ProduitDynamiqueSelectorHandler(_model:ProduitDynamiqueSelectorModel)
		{
			this._model = _model;
		}
		
		internal function getListeProduitResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateListeProduits(event.result as ArrayCollection);
			}
			
		}
	}
}