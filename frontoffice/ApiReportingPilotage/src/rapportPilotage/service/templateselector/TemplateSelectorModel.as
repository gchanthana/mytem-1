package rapportPilotage.service.templateselector
{
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.vo.ValeurSelector;

	internal class TemplateSelectorModel  extends EventDispatcher
	{
		private var _templates :ArrayCollection;
		
		public function TemplateSelectorModel()
		{
			_templates = new ArrayCollection();	
		}
		public function get templates():ArrayCollection
		{
			return _templates;
		}
		
		internal function updateValues(value:ArrayCollection):void
		{	
			_templates.removeAll();// vider l'ArrayCollection
			var valeurSelector:ValeurSelector;
			
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelector();
				valeurSelector.fill(value[i]);
				_templates.addItem(valeurSelector); 
			}
			this.dispatchEvent(new RapportEvent(RapportEvent.USER_TEMPLATE_SELECTOR_EVENT));
		}
	}
}