package rapportPilotage.service.periodeselector
{
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.vo.ValeurSelector;

	internal class PeriodeSelectorModel  extends EventDispatcher
	{
		private var _valuesPeriode :ArrayCollection;
		
		public function PeriodeSelectorModel():void
		{
			_valuesPeriode=new ArrayCollection();
		}
		public function get getValuesPeriode():ArrayCollection
		{
			return _valuesPeriode;
		}
		
		internal function updateValues(value:ArrayCollection):void
		{
			_valuesPeriode.removeAll();
		
			var valeurSelector:ValeurSelector;
					
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelector();
				valeurSelector.fill(value[i]);
				_valuesPeriode.addItem(valeurSelector); 
			}
	
			this.dispatchEvent(new RapportEvent(RapportEvent.USER_PERIODE_SELECTOR_EVENT));
		}
	}
}