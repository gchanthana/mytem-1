package rapportPilotage.service.chercherlisterapport
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.vo.Rapport;
		
	public class ChercherListeRapportModel extends EventDispatcher
	{
		private var _rapports :ArrayCollection;
		
		public function ChercherListeRapportModel()
		{
			_rapports = new ArrayCollection();	
		}
		
		public function get rapports():ArrayCollection
		{
			return _rapports;
		}
		
		internal function updateListeRapports(value:ArrayCollection):void
		{
			_rapports.removeAll();// vider l'ArrayCollection
			
			var newRapport:Rapport;	
			
			if(value.length > 0)
			{
				for(var i:int=0;i<value.length;i++)
				{
					newRapport = new Rapport();
					newRapport.fill(value[i]);
					_rapports.addItem(newRapport); 	
				}	
			}
			
			dispatchEvent(new RapportEvent(RapportEvent.USER_LIST_RAPPORT_EVENT));
			
		}
	}
}