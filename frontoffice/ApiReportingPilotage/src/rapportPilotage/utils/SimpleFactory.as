package rapportPilotage.utils
{
	import mx.controls.Alert;
	
	import rapportPilotage.utils.formattemplateselector.FormatTemplateSelectorIHM;
	import rapportPilotage.utils.monoperiodeselector.MonoPeriodeSelectorIHM;
	import rapportPilotage.utils.noSelector.NoSelector;
	import rapportPilotage.utils.operatorselector.OperatorSfr1438IHM;
	import rapportPilotage.utils.perimetreselector.PerimetreOrgaSelectorIHM;
	import rapportPilotage.utils.perimetreselector.PerimetreSelectorIHM;
	import rapportPilotage.utils.periodecomboboxselector.PeriodeComboBoxSelectorIHM;
	import rapportPilotage.utils.periodeselector.PeriodeSelectorIHM;
	import rapportPilotage.utils.poolsselector.PoolsSelectorIHM;
	import rapportPilotage.utils.produitdynamiqueselector.ProduitDynamiqueSelectorIHM;
	import rapportPilotage.utils.templateselector.TemplateSelectorIHM;
	import rapportPilotage.utils.topselector.TopSelectorIHM;
	import rapportPilotage.vo.SelectorParametre;
	
	public class SimpleFactory
	{
		public function SimpleFactory()	{}					
        private var returnSelector:SelectorAbstract;
	 
        public function createComposants(selector:Object):SelectorAbstract
		{
            switch(selector.typeSelecteur)
			{	          
	             case "PeriodeSelector": returnSelector =  new PeriodeSelectorIHM();(returnSelector as PeriodeSelectorIHM).setSelector(selector as SelectorParametre);
														   break;	
				
				 case "MonoPeriodeSelector": returnSelector = new rapportPilotage.utils.monoperiodeselector.MonoPeriodeSelectorIHM;
					 										 (returnSelector as MonoPeriodeSelectorIHM).setSelector(selector as SelectorParametre);
													          break;	
				 
				 case "TemplateSelector": returnSelector =  new  TemplateSelectorIHM();(returnSelector as TemplateSelectorIHM).setSelector(selector as SelectorParametre);
															break;	
				 
				 case "FormatTemplateSelector": returnSelector = new  FormatTemplateSelectorIHM();(returnSelector as FormatTemplateSelectorIHM).setSelector(selector as SelectorParametre);
										 			         	 break;
				 
				 case "PerimetreSelector": returnSelector = new PerimetreSelectorIHM();(returnSelector as PerimetreSelectorIHM).setSelector(selector as SelectorParametre);
															break;
				 
				 case "PerimetreOragSelector": returnSelector = new PerimetreOrgaSelectorIHM();(returnSelector as PerimetreOrgaSelectorIHM).setSelector(selector as SelectorParametre);
															    break;
				
				 case "PeriodeComboBoxSelector": returnSelector = new PeriodeComboBoxSelectorIHM();(returnSelector as PeriodeComboBoxSelectorIHM).setSelector(selector as SelectorParametre);
																  break;	
				
				 case "PoolSelector": returnSelector = new PoolsSelectorIHM();(returnSelector as PoolsSelectorIHM).setSelector(selector as SelectorParametre);
													   break;
				 
				 case "TopSelector": returnSelector = new TopSelectorIHM();(returnSelector as TopSelectorIHM).setSelector(selector as SelectorParametre);
													   break;
				 
				 case "ProduitDynamiqueSelector": returnSelector = new ProduitDynamiqueSelectorIHM();(returnSelector as ProduitDynamiqueSelectorIHM).setSelector(selector as SelectorParametre);
					                                               break;
				/* case "OperatorSfr1438Selector": returnSelector = new OperatorSfr1438IHM();(returnSelector as OperatorSfr1438IHM).setSelector(selector as SelectorParametre);
				                                                   break;*/
				 
				 default :  case "NoSelector": returnSelector = new NoSelector();(returnSelector as NoSelector).setSelector(selector as SelectorParametre);
															    break;	

		     }
			return returnSelector;
		 }		    				
	}
}