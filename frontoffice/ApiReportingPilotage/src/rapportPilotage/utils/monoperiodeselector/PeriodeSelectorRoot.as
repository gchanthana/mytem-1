package rapportPilotage.utils.monoperiodeselector
{
	import mx.containers.Box;
	import mx.controls.HSlider;
	import mx.events.SliderEvent;
	import rapportPilotage.event.RapportIHMEvent;

	public class PeriodeSelectorRoot extends Box
	{
		[Bindable]public var slider : HSlider;
		
		public function PeriodeSelectorRoot()
		{
			super();
		}
		
		protected function slider_changeHandler(event:SliderEvent):void
		{
			dispatchEvent(new RapportIHMEvent(RapportIHMEvent.CHANGER_VALUE_HSLIDER_EVENT));
		}

	}
}