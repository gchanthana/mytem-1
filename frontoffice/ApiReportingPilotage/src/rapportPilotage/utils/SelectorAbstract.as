package rapportPilotage.utils
{
	import mx.containers.FormItem;
	import mx.containers.HBox;
	import rapportPilotage.ihm.ImgHelp;
	import rapportPilotage.vo.Rapport;
	import rapportPilotage.vo.SelectorParametre;

	public class SelectorAbstract extends FormItem
	{
		
		[Bindable]public var descriptionComposant :String;
		[Bindable]public var messageAideComposant :String;
		[Bindable]public var selectedRapport :Rapport;
		[Bindable]public var is_systemComposant :Boolean;
		[Bindable]public var is_hiddenComposant :Boolean;
		[Bindable]public var typeParametreComposant :String;
		[Bindable]public var CleAndLibelleParametreComposant :String;
		[Bindable]public var idRapportParamtre :int; // l'id du selector
		[Bindable]protected var _isOk :Boolean=true; 
		[Bindable]protected var _errorMessage :String=""; 
			
		public var imgHelp : ImgHelp=new ImgHelp();
		public var selectorSelected:SelectorParametre;
		
		
		public function get isOk():Boolean
		{
			return _isOk
		}
		
		public function get errorMessage():String
		{
			return _errorMessage;
		}
	
		public function SelectorAbstract(selector:Object=null)
		{
			if(selector!=null)
			{
				//this.labelComposant=selector.LIBELLE;
				//this.descriptionComposant=selector.DESCRIPTION;
			}
		}
		
		public function showHelp():void
		{
			if(selectorSelected.hasOwnProperty("messageAide") && selectorSelected.messageAide!=null && selectorSelected.messageAide!="")
			{
				// il faut toujours avoir un Hbox sinon il faut redéfinir cette methode
				(this.getChildAt(0) as HBox).addChild(imgHelp);
				imgHelp.text=selectorSelected.messageAide;// text est un setter sur l'objet
			}
			
		}
		public function setSelector(selector:SelectorParametre):void
		{
			this.label=selector.libelle+" : ";
			this.selectorSelected=selector;
		}
		public function getSelectedValue():Array{return null}
		public function initRapport(selectedRappot:Rapport):void
		{
			this.selectedRapport=selectedRappot;
		}
	}
}