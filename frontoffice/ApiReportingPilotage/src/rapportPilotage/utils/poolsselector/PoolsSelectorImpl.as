package rapportPilotage.utils.poolsselector
{
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.service.poolsselector.PoolSelectorService;
	import rapportPilotage.utils.SelectorAbstract;
	import rapportPilotage.utils.serialization.json.*;
	import rapportPilotage.vo.Parametre;
	import rapportPilotage.vo.SelectorParametre;
	import rapportPilotage.vo.ValeurSelector;
	
	public class PoolsSelectorImpl extends SelectorAbstract
	{
		[Bindable]public var poolSelectorService:PoolSelectorService;
		[Bindable]public var comboBoxPool:ComboBox;
		
		public var arrayCleLibelle :Object;
		
		
		public function PoolsSelectorImpl(selector:Object=null)
		{
			super(selector);
		}
		
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
				
			}
		}
		
		/* appeler le service pour initialiser le dataprovider de comboBox */
		
		public function poolsselector_creationCompleteHandler(event:FlexEvent):void
		{
			poolSelectorService=new PoolSelectorService();
			poolSelectorService.model.addEventListener(RapportEvent.USER_POOL_SELECTOR_EVENT,fillData);
			poolSelectorService.getValuesPool();
		}
		
		/* initialiser le dataprovider puis afficher l'image de help */
		
		private function fillData(event:RapportEvent):void
		{
			comboBoxPool.dataProvider=poolSelectorService.model.pools;
			
			if(poolSelectorService.model.pools.length<2) 
			{
				visible =false;
				includeInLayout = false;
			}
			else
			{
				visible = true;
				includeInLayout = true;
			}
			showHelp();
		}
		
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
					
			var idPool: int;
		
			var labelSelected:String;
			var is_system:Boolean;
			var valueParam:Array=[];
			
			idPool=(comboBoxPool.selectedItem as ValeurSelector).id;	
			labelSelected=comboBoxPool.selectedLabel;
		
			valueParam.push(idPool);
			valueParam.push(labelSelected);
			
			for(var i : int = 0 ; i< arrayCleLibelle.length ; i++)
			{
				var parametre :Parametre=  new Parametre();
				
				if(arrayCleLibelle[i].IS_SYSTEM== 1)
					
					is_system=true;
				else
					is_system=false;
				
				parametre.value=[valueParam[i]];
				parametre.cle =arrayCleLibelle[i].NOM_CLE;
				parametre.libelle=arrayCleLibelle[i].LIBELLE_CLE;
				parametre.is_system=is_system;
				
				parametre.typeValue=this.typeParametreComposant.toUpperCase();
				
				tabValeur[i]=parametre;
			}
			return tabValeur;
		}
	}
}