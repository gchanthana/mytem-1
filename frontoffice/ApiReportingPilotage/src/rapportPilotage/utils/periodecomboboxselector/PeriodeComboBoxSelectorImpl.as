package rapportPilotage.utils.periodecomboboxselector
{
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.service.periodecomboboxselector.PeriodeComboBoxSelectorService;
	import rapportPilotage.utils.SelectorAbstract;
	import rapportPilotage.utils.serialization.json.*;
	import rapportPilotage.vo.Parametre;
	import rapportPilotage.vo.SelectorParametre;
	import rapportPilotage.vo.ValeurSelector;
	
	public class PeriodeComboBoxSelectorImpl extends SelectorAbstract
	{
		
		[Bindable]public var periodeComboBoxSelectorService:PeriodeComboBoxSelectorService;
		[Bindable]public var comboBoxPeriode:ComboBox;
		public var arrayCleLibelle :Array;
		
		public function PeriodeComboBoxSelectorImpl(selector:Object=null)
		{
			super(selector);		
		}
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.idRapportParamtre=selector.idRapportParam;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
			}
		}		
		protected function periodecombobox_creationCompleteHandler(event:FlexEvent):void
		{
			periodeComboBoxSelectorService=new PeriodeComboBoxSelectorService();
			periodeComboBoxSelectorService.model.addEventListener(RapportEvent.USER_PERIODE_SELECTOR_EVENT,fillData);
			periodeComboBoxSelectorService.getValuesPeriode(this.idRapportParamtre);
		}
		
		private function fillData(event:RapportEvent):void
		{
			comboBoxPeriode.dataProvider=periodeComboBoxSelectorService.model.periodes;
			
			if(periodeComboBoxSelectorService.model.periodes.length<2)
			{
				visible = false;
				includeInLayout = false;				
			}
			else
			{
				visible = true;
				includeInLayout = true;
			}			
			showHelp();
		}
		
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
			
			var valueUperiodeDebut: int;
			var valueUperiodeFin: int;
			var labelSelectedDebut :String;
			var labelSelectedFin :String;
			var is_system:Boolean;
			var valueParam:Array=[];
		
			valueUperiodeDebut=(comboBoxPeriode.selectedItem as ValeurSelector).U_IDPERIOD_DEB;
			valueUperiodeFin=(comboBoxPeriode.selectedItem as ValeurSelector).U_IDPERIOD_FIN;
			
			labelSelectedDebut=comboBoxPeriode.selectedLabel;
			labelSelectedFin=comboBoxPeriode.selectedLabel;
			
			valueParam.push(valueUperiodeDebut);
			valueParam.push(valueUperiodeFin);
			valueParam.push(labelSelectedDebut);
			valueParam.push(labelSelectedFin);
			
			for(var i : int = 0 ; i< arrayCleLibelle.length ; i++)
			{
				var parametre :Parametre=  new Parametre();
				
				if(arrayCleLibelle[i].IS_SYSTEM== 1)
					is_system=true;
				else
					is_system=false;
								
				parametre.value=[valueParam[i]];	//arrayCleLibelle[i].NOM_CLE=U_IDPERIODE_DEBUT
				parametre.is_system = is_system;
				parametre.cle =arrayCleLibelle[i].NOM_CLE;
				parametre.libelle=arrayCleLibelle[i].LIBELLE_CLE;
				parametre.typeValue=this.typeParametreComposant.toUpperCase();
				
				tabValeur.push(parametre);
			}
			return tabValeur;

		}
	}
}