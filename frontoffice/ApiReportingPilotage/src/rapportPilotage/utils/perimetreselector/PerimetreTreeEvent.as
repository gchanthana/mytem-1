package rapportPilotage.utils.perimetreselector  {
	import flash.events.Event;

	public class PerimetreTreeEvent extends Event 
	{
		public static const DATA_PROVIDER_SET:String = "PERIMETRE DATA_PROVIDER SET"; // Obsolète
		public static const NODE_INFOS_RESULT:String = "PERIMETRE NODE_INFOS RESULT";
		public static const SELECTED_ITEM_CHANGE:String = "SELECTED_ITEM CHANGE";
		
		public function PerimetreTreeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
		}
	}
}
