package rapportPilotage.utils.perimetreselector 
{
	import flash.events.Event;
	import mx.collections.XMLListCollection;
	import mx.controls.Tree;
	import mx.controls.treeClasses.ITreeDataDescriptor;
	import mx.core.ClassFactory;
	import mx.events.FlexEvent;
	import mx.events.TreeEvent;
	import mx.utils.ObjectUtil;


	public class GenericPerimetreTree extends Tree {
		
		private var currentOpenNode:Object;
		private var _nodeToExpand:Object;
		
		public function GenericPerimetreTree() {
			super();
			trace("(GenericPerimetreTree) Instance Creation");
			this.dataDescriptor = new PerimetreTreeDataDescriptor();
			(dataDescriptor as PerimetreTreeDataDescriptor).addEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, onChildDataAdded);
			this.itemRenderer = new ClassFactory( rapportPilotage.utils.perimetreselector.PerimetreTreeItemRenderer);
			this.addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
		}
		
		

		protected function afterCreationComplete(event: Event):void {
			trace("(GenericPerimetreTree) Performing IHM Initialization");
			currentOpenNode = null;
			addEventListener(TreeEvent.ITEM_OPENING,onItemOpening);
			addEventListener(TreeEvent.ITEM_OPEN,onItemOpen);
			setStyleProps();
		}
		
		protected function setStyleProps():void {
			setStyle("rollOverColor","#DDEEDD");
			setStyle("selectionColor","#C1C1C1");
		}

		public function updateDataProvider(xmlDataProvider:XML,dataLabel:String):void {
			this.enabled = false;
			dataProvider = null;
			labelField = "@" + dataLabel;
			dataProvider = xmlDataProvider;
			callLater(dataProviderUpdated);
		}
		
		public function setDataProvider(nodesDataSet:Object,dataLabel:String,
												dataDesc:ITreeDataDescriptor):void {
			//this.visible = false;
			dataDescriptor = null;
			dataDescriptor = dataDesc;
			dataProvider = null;
			dataProvider = nodesDataSet;
			labelField = "@" + dataLabel;
			callLater(dataProviderUpdated);
		}

		/**
		 * Handler utilisé par le callLater de ce Tree
		 * */
		protected function dataProviderUpdated():void {
			//this.visible = true;
			this.enabled = true;
			dispatchEvent(new PerimetreTreeEvent(PerimetreTreeEvent.DATA_PROVIDER_SET));
		}
		
		/**
		 * Ne pas mettre en protected car n'est utilisé que dans cette classe
		 * */
		protected function onItemOpening(event: TreeEvent):void {
			var node:XML = (event.item as XML);
			var childrenList:XMLList = node.children();
			if(parseInt(node.@NTY,10) > 0) {
				if(childrenList.length() == 0) {
					expandItem(node,false);
					currentOpenNode = node;
					(dataDescriptor as PerimetreTreeDataDescriptor).loadNodeChild(event.item);
				}
			}
		}
		
		protected function onItemOpen(event: TreeEvent):void {
			var node:XML = (event.item as XML);
			var childrenList:XMLList = node.children();
			if(childrenList.length() == 0)
				expandItem(node,false);
			else
				expandItem(node,true);
		}
		
		protected function onChildDataAdded(event:PerimetreEvent):void {
			trace("*====================================*");
			trace("*GenericPerimetreTree.dataProvider = *");
			trace("*====================================*");
			trace(ObjectUtil.toString(dataProvider));
			
			expandItem(currentOpenNode,true);
			currentOpenNode = null;
		}
		
		/**
		 * Séléctionne le noeud ayant le nodeId passé en paramètre puis déroule le chemin
		 * jusqu'à ce noeud
		 * */
		public function selectNodeById(nodeId:int):Boolean {
			try{
				var node:XML = (dataProvider as XMLListCollection).getItemAt(0) as XML;
				if(parseInt(node.@NID,10) == nodeId) {
					selectedItem = node;
					return true;
				} else {
					var resultList:XMLList = node.descendants().(@NID == nodeId);
					if(resultList.length() > 0) {
						var targetNode:XML = resultList[0] as XML;
						var tmpParentNode:Object = targetNode.parent();
						while(tmpParentNode != null) {
							if(isItemOpen(tmpParentNode) == false)
								expandItem(tmpParentNode,true);
							tmpParentNode = (tmpParentNode as XML).parent();
						}
						selectedItem = targetNode;
						scrollToIndex(selectedIndex);
						return true;
					} else
						return false;
				}
			} catch(te : TypeError) {
				return false
			}
			return false
		}
		
		
		
		public function get nodeToExpand():Object
		{			
			return _nodeToExpand;
		}
		
		[Bindable]
		public function set nodeToExpand(value:Object):void
		{
			_nodeToExpand = value;
			if(_nodeToExpand != null) this.expandItem(_nodeToExpand,true);
		}
		
		
	}
	
	
}
