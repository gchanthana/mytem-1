package rapportPilotage.utils.perimetreselector 
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	

	public class SimplePerimetreTree extends PerimetreTree implements IPerimetreTreeWindow {
		public static const TYPE_ROOT:String = ConsoViewPerimetreObject.TYPE_ROOT;
		public static const TYPE_OPE:String = ConsoViewPerimetreObject.TYPE_OPE;
		public static const TYPE_GEO:String = ConsoViewPerimetreObject.TYPE_GEO;
		public static const TYPE_CUS:String = ConsoViewPerimetreObject.TYPE_CUS;
		public static const TYPE_SAV:String = ConsoViewPerimetreObject.TYPE_SAV;
		
		/**
		 * Si false alors sélectionne et affiche la racine
		 * Si true alors la racine n'est pas affichée et c'est le 1er enfant de la racine qui est sélectionné
		 * */
		private var _connectedNodeIsRoot:Boolean = true;
		/**
		 * Si true alors à la séléction d'un noeud l'arbre récupère les infos
		 * concernant le noeud en question.
		 * Les infos du noeud sont obtenues par appel à get nodeInfos()
		 * Si false alors la séléction ne fait rien et la get nodeInfos() renvoie null
		 * */
		private var _loadDataOnSelection:Boolean = false;
		
		/**
		 * Si true alors le groupe racine est toujours affiché
		 * Sinon c'est le 1er noeud disponible dans l'organisation opérateur qui est
		 * affiché comme racine et qui est séléctionné
		 * */
		private var _showGroupeRacine:Boolean = true;
		
		private var _restrictToOrgaOp:Boolean = false;
		
		
		/**
		 * Infos concernant le noeud séléctionné
		 * */
		private var nodeInfosData:INodeInfos;
		
		private var getNodeInfosOp:AbstractOperation;
		
		public function get connectedNodeIsRoot():Boolean {
			return _connectedNodeIsRoot;
		}
		
		public function set connectedNodeIsRoot(value:Boolean):void {
			_connectedNodeIsRoot = value;
		}
		
		public function get loadDataOnSelection():Boolean {
			return _loadDataOnSelection;
		}
		
		public function set loadDataOnSelection(value:Boolean):void {
			_loadDataOnSelection = value;
		}
		
		public function get showGroupeRacine():Boolean {
			return _showGroupeRacine;
		}
		
		public function set showGroupeRacine(value:Boolean):void {
			_showGroupeRacine = value;
		}
		
		/**
		 *si true alors, ne sont affichées que les organisations de type opérateur  
		 *option ignorée si la racine est affichée.
		 */
		public function get restrictToOrgaOp():Boolean
		{
			return _restrictToOrgaOp;
		}
		
		/**
		 * @private
		 */
		public function set restrictToOrgaOp(value:Boolean):void
		{
			_restrictToOrgaOp = value;
		}
		
		public function SimplePerimetreTree() {
			super();
			trace("(SimplePerimetreTree) Instance Creation");
			nodeInfosData = null;
			getNodeInfosOp = RemoteObjectUtil.getOperation("fr.consotel.consoview.access.PerimetreManager",
				"getNodeInfos",processNodeInfos);
		}
		
		protected override function afterCreationComplete(event:Event):void {
			super.afterCreationComplete(event);
			trace("(SimplePerimetreTree) Performing IHM Initialization");
			
			this.addEventListener(ListEvent.CHANGE,onPerimetreTreeSelection);
			this.addEventListener(MouseEvent.DOUBLE_CLICK,changePerimetre);
			onPerimetreChange();
		}
		
		public function onPerimetreChange():void 
		{
			if(this.initialized) {
				trace("(SimplePerimetreTree) onPerimetreChange");
				selectedItem = null;
				initTree();
			}
		}
		
		public function initTree():void {
			
			if(connectedNodeIsRoot == false) 
			{ 			
				showRoot = showGroupeRacine;				
				expandItem(CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList,true)
				updateDataProvider(CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList,"LBL");
				
			} 
			else 
			{
				var xmlPerimetreData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;
				
				this.openItems=xmlPerimetreData; // permert de ouvrir le tree
				
				var currentTypeLogique : String = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_LOGIQUE;
				var typeRoot : String = ConsoViewPerimetreObject.TYPE_ROOT;
				
				if(CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_LOGIQUE ==
					ConsoViewPerimetreObject.TYPE_ROOT)
				{	
					showRoot = showGroupeRacine;						
					var __xmlData:XML = xmlPerimetreData;
					
					if(restrictToOrgaOp)
					{
						var __xmlListData:XMLList = xmlPerimetreData.NODE.(@NID == -2);
						__xmlData = (__xmlListData.length() > 0)?__xmlListData[0]:<node><![CDATA[No data found]]></node> ;
						}
						
						updateDataProvider(__xmlData,"LBL");
						
						} 
						else 
						{ 
						showRoot = true;
						var idNodePerimetre : Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;				
						var resultList:XMLList = xmlPerimetreData.descendants("NODE").(@NID == CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
						
						if(resultList.length() > 0)
							{
							updateDataProvider(resultList[0],"LBL");//le noeud est déja en mémoire	
							}
							else
							{
							callLater(initTree);
							}	
							}
							}
							}
							
							
							
							private function changePerimetre(event:Event):void {
							if(this.selectedItem.@NID != CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX) {
							if(this.selectedItem.@STC > 0) {
								dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_NODE_CHANGED));
								}
								}
								}
								
								public function getSelectedItem():Object {
								return selectedItem;
								}
								
								/**
								 * Donne les infos supplémentatires concernant le noeud séléctionné
								 * Cette fonction renvoie toujours null si loadDataOnSelection = false
								 * */
								public function get nodeInfos():INodeInfos {
								if(loadDataOnSelection == false)
									return null;
								else
									return nodeInfosData;
								}
								
								/**
								 * Renvoie true si le noeud séléctionné est connectable.
								 * C'est à dire qu'on a au moins un accès dessus
								 * Sinon renvoie false
								 * */
								public function get nodeIsConnectable():Boolean {
								if(selectedItem == null)
									return false;
								else
									return (selectedItem.@STC > 0);
										}
										
										/**
										 * Renvoie le nodeId du noeud séléctionné
										 * */
										public function get nodeId():int {
										if(selectedItem == null)
											return -100;
										else
											return selectedItem.@NID;
										}
										
										/**
										 * Renvoie le libellé du noeud séléctionné
										 * */
										public function get nodeLabel():String {
										if(selectedItem == null)
											return null;
										else
											return selectedItem.@LBL;
										}
										
										/**
										 * Séléctionne le noeud ayant le nodeId passé en paramètre puis déroule le chemin
										 * jusqu'à ce noeud
										 * */
										public override function selectNodeById(nodeId:int):Boolean {
										var boolResult:Boolean = super.selectNodeById(nodeId);
										
										if(selectedItem != null && loadDataOnSelection == true) {
										if(selectedItem.@NID != CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX)
											loadNodeInfos(selectedItem.@NID);
										else
											updateNodeInfos(null);
										}
										
										dispatchEvent(new PerimetreTreeEvent(PerimetreTreeEvent.SELECTED_ITEM_CHANGE));
										return boolResult;
										}
										
										protected override function dataProviderUpdated():void {
										super.dataProviderUpdated();
										selectFirstValidNode();
										}
										
										protected function selectFirstValidNode():void {
										var nodeAppId:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
										var xmlPerimetreData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;
										if(nodeAppId == CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX) { // Si c'est le groupe racine
										if(showGroupeRacine == false) { // Si on n'affiche pas le groupe racine
										nodeAppId =
										((xmlPerimetreData[0] as XML).children().(@NTY > 0)[0] as XML).children()[0].@NID;
											}
											}
											selectNodeById(nodeAppId);
											}
											
											protected function onPerimetreTreeSelection(event:ListEvent):void {
											var nodeAppId:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
											var xmlPerimetreData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;
											if(selectedItem == null) { // Si la séléction est vide
											selectFirstValidNode();
											if(selectedItem.@NID != CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX) {
											if(loadDataOnSelection == true)
												loadNodeInfos(selectedItem.@NID);
											} else { // On récupère les infos dans la session si on est déjà connecté sur le noeud
											if(loadDataOnSelection == true)
												updateNodeInfos(null);
											}
											} else { // Si la séléction n'est pas vide
											if(selectedItem.@STC > 0) { // Noeud sur lequel on peut se connecter
												if(selectedItem.@NID != CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX) {
												if(loadDataOnSelection == true)
													loadNodeInfos(selectedItem.@NID);
												} else { // On récupère les infos dans la session si on est déjà connecté sur le noeud
												if(loadDataOnSelection == true)
													updateNodeInfos(null);
												}
												} else { // Noeud sur lequel on ne peut pas se connecter
												if(selectedItem.@NID > 0) { // Noeud périmètre
													if(loadDataOnSelection == true)
														loadNodeInfos(selectedItem.@NID);
													} else { // Libellé des catégories d'orga (Orga Opérateurs, Orga Clients, etc...)
													selectFirstValidNode();
													if(selectedItem.@NID != CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX) {
													if(loadDataOnSelection == true)
														loadNodeInfos(selectedItem.@NID);
													} else { // On récupère les infos dans la session si on est déjà connecté sur le noeud
													if(loadDataOnSelection == true)
														updateNodeInfos(null);
													}
													}
													}
													}
													dispatchEvent(new PerimetreTreeEvent(PerimetreTreeEvent.SELECTED_ITEM_CHANGE));
													}
													
													protected function loadNodeInfos(nodeId:int):void {
													trace("(SimplePerimetreTree) Loading Node Infos - NID : " + nodeId);
													RemoteObjectUtil.invokeService(getNodeInfosOp,
														CvAccessManager.getSession().USER.CLIENTACCESSID,nodeId);
													}
													
													protected function processNodeInfos(event:ResultEvent):void {
													trace("(SimplePerimetreTree) Processing Node Infos");
													updateNodeInfos(event.result as Object);
													}
													
													protected function updateNodeInfos(nodeInfosDataObject:Object = null):void {
													trace("(SimplePerimetreTree) Updating Node Infos");
													
													var tmpNodeInfos:Object = nodeInfosDataObject;
													
													if(tmpNodeInfos == null) {
													tmpNodeInfos = new Object();
													tmpNodeInfos.NID = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
													tmpNodeInfos.LBL = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
													tmpNodeInfos.STC = 1; // C'est forcément que le noeud est connectable
													
													tmpNodeInfos.TYPE_PERIMETRE = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
													if(selectedItem.@NID == CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX)
														tmpNodeInfos.TYPE_LOGIQUE = ConsoViewPerimetreObject.TYPE_ROOT;
													else
														tmpNodeInfos.TYPE_LOGIQUE = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_LOGIQUE;
													
													tmpNodeInfos.MODULE_FIXE_DATA = "0";
													
													tmpNodeInfos.MODULE_GESTION_LOGIN = "0";
													
													tmpNodeInfos.MODULE_MOBILE = "0";
													
													tmpNodeInfos.DROIT_GESTION_FOURNIS = "0";
													
													tmpNodeInfos.INFOS_STATUS = 0;
													
													tmpNodeInfos.CODE_STYLE = "0";
													
													tmpNodeInfos.MODULE_FACTURATION = "0";
													
													tmpNodeInfos.MODULE_GESTION_ORG = "0";
													
													tmpNodeInfos.MODULE_WORKFLOW = "0";
													
													tmpNodeInfos.MODULE_USAGE = "0";
													
													tmpNodeInfos.GEST =  0;
													
													tmpNodeInfos.STRUCT =  0; 
													
													tmpNodeInfos.FACT = "0";
													
													tmpNodeInfos.OPERATEURID = "0";
													}
													
													nodeInfosData = new NodeInfos(tmpNodeInfos);
													trace("(SimplePerimetreTree) Node Infos Updated");
													if(loadDataOnSelection == true)
														dispatchEvent(new PerimetreTreeEvent(PerimetreTreeEvent.NODE_INFOS_RESULT));
													}
													
													
													
													}
}
