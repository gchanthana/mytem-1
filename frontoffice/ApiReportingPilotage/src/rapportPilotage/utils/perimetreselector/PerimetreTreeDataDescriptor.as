package rapportPilotage.utils.perimetreselector
{

	import appli.events.PerimetreDataProxyEvent;
	import composants.access.perimetre.IPerimetreDataProxy;
	import composants.access.perimetre.PerimetreDataProxy;
	import flash.events.EventDispatcher;
	import mx.collections.ICollectionView;
	import mx.collections.XMLListCollection;
	import mx.controls.treeClasses.ITreeDataDescriptor;

	public class PerimetreTreeDataDescriptor extends EventDispatcher implements ITreeDataDescriptor {
		//private var perimetreModel:PerimetreModel;
		private var perimetreDataProxy:IPerimetreDataProxy;

		//public function PerimetreTreeDataDescriptor(perimetreModelObject:IPerimetresModel) {
		public function PerimetreTreeDataDescriptor() 
		{
			super(this);
			
			trace("(PerimetreTreeDataDescriptor) Instance Creation");
			
			perimetreDataProxy = new PerimetreDataProxy();
			
			(perimetreDataProxy as PerimetreDataProxy).addEventListener(PerimetreDataProxyEvent.NODE_CHILD_LOADED,onChildEvent);
			//perimetreModel = perimetreModelObject as PerimetreModel;
			//perimetreModel.addEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT,onChildEvent);
		}
		
		public function getData(node:Object,model:Object=null):Object {
			return null;
		}
		
		public function hasChildren(node:Object,model:Object=null):Boolean {
			var childrenList:XMLList = (node as XML).children();
			if(childrenList.length() > 0)
				return true;
			else
				return false;
		}
		
		public function addChildAt(parent:Object,newChild:Object,index:int,model:Object=null):Boolean {
			return true;
		}
		
		public function isBranch(node:Object,model:Object=null):Boolean {
			var hasChild:int = parseInt(node.@NTY,10);
			return (hasChild > 0);
		}
		
		public function removeChildAt(parent:Object,child:Object,index:int,model:Object=null):Boolean {
			return true;
		}
		
		public function getChildren(node:Object,model:Object=null):ICollectionView {
			var childrenList:XMLList = (node as XML).children();
			return new XMLListCollection(childrenList);
		}
		
		public function loadNodeChild(node:Object):void {
			//perimetreModel.getNodeChild(node);
			perimetreDataProxy.loadNodeChildren((node as XML));
		}
		
		private function onChildEvent(event:PerimetreDataProxyEvent):void {
			dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_CHILD_RESULT));
		}
	}
}
