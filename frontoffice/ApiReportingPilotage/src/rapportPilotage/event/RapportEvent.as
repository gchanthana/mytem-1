package rapportPilotage.event
{
	import flash.events.Event;
	
	public class RapportEvent extends Event
	{
		public static const USER_MODULE_UPDATED:String 	  = 'UserModuleUpdated'; 
		public static const USER_SELECTOR_UPDATED_EVENT:String = "UserSelectorUpdatedEvent"; 
		public static const USER_FORMAT_SELECTOR_EVENT:String = "UserFormatSelectordEvent"; 
		public static const USER_TEMPLATE_SELECTOR_EVENT:String="UserTemplateSelectorEvent";
		public static const USER_MONO_PERIODE_SELECTOR_EVENT :String="UserMonoPeriodeSelectorEvent";
		public static const USER_PERIODE_SELECTOR_EVENT :String="UserPeriodeSelectorEvent";
		public static const USER_LIST_RAPPORT_EVENT :String="UserListRapportEvent";
		public static const USER_LIST_FORMAT_BYTEMPLATE_EVENT :String="UserListFormatByTemplateEvent";
		public static const USER_SELECTED_IDMODULE_EVENT :String="UserSelectedIdModuleEvent";
		public static const USER_POOL_SELECTOR_EVENT :String="UserPoolSelectorEvent";
		public static const USER_TOPS_SELECTOR_EVENT :String="UserTopSelectorEvent";
		public static const USER_URL_PDF_RAPPORT_EVENT :String="UserUrlPdfRapportEvent";
		public static const USER_LIST_PRODUIT_EVENT :String="UserListProduitEvent";
		public static const USER_LIST_OPERATOR_EVENT :String="userListOperatorEvent";
		
		
		public function RapportEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new RapportEvent(type,bubbles,cancelable);
		}
	}
}