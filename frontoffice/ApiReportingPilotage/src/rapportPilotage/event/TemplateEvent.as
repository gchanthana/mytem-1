package rapportPilotage.event
{
	import flash.events.Event;
	
	public class TemplateEvent extends Event
	{
		
		public var idTemplate:int;
		
		public static const DATA_TRANSFER:String = "data_transfer";
		
		public function TemplateEvent(type:String,idTemplate:int,bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable)
			this.idTemplate=idTemplate;
		}
		
		override public function clone():Event
		{
			return new TemplateEvent(type,idTemplate,bubbles,cancelable);
		}
	}
}