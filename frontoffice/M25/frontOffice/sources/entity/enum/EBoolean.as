package entity.enum
{
    import mx.resources.ResourceManager;
	import mx.collections.ArrayCollection;

    /**
     *
     * Objet de type <code>Enumeratio</code> regroupant
     * des items d'objets accessibles publiquement et composés
     * d'un champs <b>label</b> et <b>value</b>
     * ex :<code>
     * var bool:EBoolean = EBoolean.TRUE;
     * bool.label = "Oui";
     * bool.value = 1 ;
     * </code>
     *
     */
    public class EBoolean extends Enumeratio
    {
        public static const TRUE:EBoolean = new EBoolean(1, ResourceManager.getInstance().getString('M25', 'Oui'));
        public static const FALSE:EBoolean = new EBoolean(0, ResourceManager.getInstance().getString('M25', 'Non'));

        public function EBoolean(value:Number, label:String)
        {
            super(value, label);
        }

        /**
         * Retourne la collection d'items EBoolean ( peuplant
         * en générale des combo de choix)
         */
        public static function getCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(FALSE, TRUE));
        }

        /**
         * Retourne l'index de l'item dans l'énumeration correspondant
         * à la valeur passé en paramètre ( utile pour l'initialisation des combobox )
         */
        public static function getbool(value:Number):Boolean
        {
            if(value == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /**
         * Retourne un objet de type <code>EBoolean</code>  correspondant
         * à la valeur passé en paramètre ( utile pour l'initialisation des combobox )
         */
        public static function getEntityFor(bool:Number):EBoolean
        {
            if(bool == 1)
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
    }
}