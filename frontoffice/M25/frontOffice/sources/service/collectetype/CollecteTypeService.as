package service.collectetype
{
    import entity.ModelLocator;
    import entity.vo.CollecteClientVO;
    import entity.vo.CollecteTypeVO;
    import entity.vo.FilterCollecteTypeVO;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;

    [Bindable]
    public class CollecteTypeService
    {
        public static const COLDFUSION_FOLDER_PATH:String = "fr.consotel.consoview.M25.CollecteService";
        public var myDatas:CollecteTypeData;
        public var myHandlers:CollecteTypeHandler;
        
        public function CollecteTypeService()
        {
          
            myDatas = new CollecteTypeData();
            myHandlers = new CollecteTypeHandler(myDatas);
        }

        /******************************************************************************
         *                         Admin et Client : Procédures Communes
         ****************************************************************************/ /**
         *  Récupère la liste des <b>opérateurs</b> depuis la base
         * @see fr.consotel.consoview.M25.CollecteService.getOperateurList()
         */
        public function getOperateurList():void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getOperateurList",
                                                                                 myHandlers.getOperateurListResultHandler);
            RemoteObjectUtil.callService(op1);
        }

        /**
         *  Récupère la liste des <b>opérateurs</b> depuis la base qui possède un template
         * @see fr.consotel.consoview.M25.CollecteService.getOperateurList()
         */
        public function getOperateurWithTemplateList():void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getOperateurWithTemplate",
                                                                                 myHandlers.getOperateurWithTemplateListResultHandler);
            RemoteObjectUtil.callService(op1);
        }

        /**
         *  Récupère la liste des <b>types de données</b> depuis la base
         * @see fr.consotel.consoview.M25.CollecteService.getDatatypeList()
         */
        public function getDataTypeList():void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getDatatypeList",
                                                                                 myHandlers.getDataTypeListResultHandler);
            RemoteObjectUtil.callService(op1);
        }

        /**
         *  Récupère la liste des <b>modes de récupération </b>  depuis la base
         * @see fr.consotel.consoview.M25.CollecteService.getRecupModeList()
         */
        public function getRecupModeList():void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getRecupModeList",
                                                                                 myHandlers.getRecupModeListResultHandler);
            RemoteObjectUtil.callService(op1);
        }

        /******************************************************************************
         *                         Admin : Gestion des Templates
         ****************************************************************************/ /**
         *  Retourne la <b>liste des Templates </b> de collecte disponibles
         * @param p_filter : VO  filtre la liste attendue en fonction de :
         *  l'opérateur (id) : si null tous
         *  le type de données : si null tout type
         *  le mode de récupération : si null tout mode
         * @see fr.consotel.consoview.M25.CollecteService.getCollecteTypeList()
         */
        public function getCollecteTypeList(p_filter:FilterCollecteTypeVO):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getCollecteTypeList",
                                                                                 myHandlers.getCollecteTypeListResultHandler);
            RemoteObjectUtil.callService(op1, p_filter.selectedOperateurId, p_filter.selectedDataTypeId, p_filter.selectedRecuperationModeId);
        }

        /**
         *  Crée ou met à jour un template de collecte
         *  @param vo : le Value Object du template
         *  si vo.id = 0 : Mode CREATION
         *  si vo.id !=0 : Mode UPDATE
         * @see fr.consotel.consoview.M25.CollecteService.createEditCollecteTypeVo()
         */
        public function createEditCollecteTypeVo(vo:CollecteTypeVO):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "createEditCollecteTypeVo",
                                                                                 myHandlers.createEditCollecteTypeVoResultHandler);
            RemoteObjectUtil.callService(op1, vo.idcollecteTemplate, vo.uidcollecteTemplate, vo.idcollectType, vo.idcollectMode, vo.operateurId, vo.libelleTemplate,
                                         vo.isPublished.value as Number, vo.isCompleted.value as Number, vo.prestaPerifacturation, vo.instructionsPath,
                                         vo.modeSouscription, vo.modeSouscriptionPath, vo.modeSouscriptionUrl, vo.delaiDispo, "info delai", vo.contraintes.
                                         value as Number, vo.contraintesInfos, vo.anteriorite.value as Number, vo.anterioriteInfos, vo.couts, vo.coutsInfos,
                                         vo.contact, vo.contactInfos, vo.libelleRoic, vo.recuperationRoic, vo.roicInfos, vo.localisationRoic, vo.localisationRoicPath,
                                         vo.libelleRocf, vo.localisationRocf, vo.localisationRocfPath, vo.iduserCreate, vo.iduserModif);
        }

        /**
         *  Crée un identifiant unique nécéssaire au stockage des
         *  documents attachés à un template et génère l'arborescence
         *  de stockage
         *
         */
        public function createUIDStorage():void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "createUIDStorage",
                                                                                 myHandlers.createUIDStorageResultHandler, null);
            RemoteObjectUtil.callService(op1);
        }

        /**
         *  Retourne la liste des groupes racines (libelle, idRacine , nbre de collectes)
         */
        public function getRacineList():void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getRacineList",
                                                                                 myHandlers.getRacineListResultHandler, null);
            RemoteObjectUtil.callService(op1);
        }

        /**
         *  Detruit le répertoire UID et ses docs enfants
         *  associé après l'annulation de la création d'un template
         *  @see fr.consotel.consoview.M25.CollecteService.createEditCollecteTypeVo()
         *
         */
        public function destroyLastUID(uid:String):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "destroyLastUID",
                                                                                 myHandlers.destroyLastUIDResultHandler);
            RemoteObjectUtil.callService(op1, uid);
        }

        /******************************************************************************
         *                         Client : Gestion des Collectes
         ****************************************************************************/ /**
         *
         * Retourne la liste des Templates de collecte disponible
         * pour l'édition/création d'une collecte par le client
         * (dépend de l'opérateur, du type de données et du mode
         * de récupération sélectionnés)
         * @see fr.consotel.consoview.M25.CollecteService.getCollecteTypeList()
         *
         */
        public function getPublishedTemplate(p_filter:FilterCollecteTypeVO):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getPublishedTemplate",
                                                                                 myHandlers.getPublishedTemplateResultHandler);
            RemoteObjectUtil.callService(op1, p_filter.selectedOperateurId, p_filter.selectedDataTypeId, p_filter.selectedRecuperationModeId);
        }

        /**
         * Retourne la <b>liste des collectes</b> pour un client donné
         * @see fr.consotel.consoview.M25.CollecteService.getCollecteClientList
         *
         */
        public function getCollecteClientList(idRacine:Number):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getCollecteClientList",
                                                                                 myHandlers.getCollecteClientListResultHandler);
            RemoteObjectUtil.callService(op1, idRacine);
        }

        /**
         *  Crée ou met à jour une collecte cliente
         *  @param vo : le Value Object du template
         *  si vo.id = 0 : Mode CREATION
         *  si vo.id !=0 : Mode UPDATE
         * @see fr.consotel.consoview.M25.CollecteService.createEditCollecteClientVo()
         */
        public function createEditCollecteClientVo(vo:CollecteClientVO):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "createEditCollecteClientVo",
                                                                                 myHandlers.createEditCollecteClientVoResultHandler);
            RemoteObjectUtil.callService(op1, vo.idRacine, vo.idcollectClient, vo.template.idcollecteTemplate, vo.libelleCollect, vo.libelleRoic, vo.identifiant,
                                         vo.mdp, vo.plageImport, vo.retroactivite.value, vo.activationAlerte.value, vo.delaiAlerte, vo.mailAlerte, vo.
                                         isActif.value, vo.userCreate, vo.userModif);
        }

        /**
         *  Test les login/mdp  de connexion à l'intranet pour la récuperation
         *  des données
         *
         */
        public function testIdentification(intranet_url:String, login:String, password:String):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "testIdentification",
                                                                                 myHandlers.testIdentificationResultHandler);
            RemoteObjectUtil.callService(op1, intranet_url, login, password);
        }
    }
}