package service.collectetype
{
    import entity.vo.CollecteTypeVO;
    import mx.collections.ArrayCollection;

    [Bindable]
    public class CollecteTypeData
    {
        private var _collecteTypeVo:CollecteTypeVO;
        private var _collecteTypeVoList:ArrayCollection;
        private var _operateurList:ArrayCollection;
        private var _operateurWithTemplateList:ArrayCollection;
        private var _dataTypeList:ArrayCollection;
        private var _recupModeList:ArrayCollection;
        private var _uid:String = "";
        // CLIENT
        private var _racineList:ArrayCollection;
        private var _publishedTemplateList:ArrayCollection;
        private var _currentTemplateClientList:ArrayCollection;
        private var _collecteClientList:ArrayCollection;
        private var _identificationSuccess:Boolean;

        public function CollecteTypeData()
        {
            new CollecteTypeVO();
        }

        public function get collecteTypeVo():CollecteTypeVO
        {
            return _collecteTypeVo;
        }

        public function set collecteTypeVO(value:CollecteTypeVO):void
        {
            _collecteTypeVo = value;
        }

        public function get collecteTypeVoList():ArrayCollection
        {
            return _collecteTypeVoList;
        }

        public function set collecteTypeVoList(value:ArrayCollection):void
        {
            _collecteTypeVoList = value;
        }

        public function get operateurList():ArrayCollection
        {
            return _operateurList;
        }

        public function set operateurList(value:ArrayCollection):void
        {
            _operateurList = value;
        }

        public function get dataTypeList():ArrayCollection
        {
            return _dataTypeList;
        }

        public function set dataTypeList(value:ArrayCollection):void
        {
            _dataTypeList = value;
        }

        public function get recupModeList():ArrayCollection
        {
            return _recupModeList;
        }

        public function set recupModeList(value:ArrayCollection):void
        {
            _recupModeList = value;
        }

        public function get currentTemplateClientList():ArrayCollection
        {
            return _currentTemplateClientList;
        }

        public function set currentTemplateClientList(value:ArrayCollection):void
        {
            _currentTemplateClientList = value;
        }

        public function get collecteClientList():ArrayCollection
        {
            return _collecteClientList;
        }

        public function set collecteClientList(value:ArrayCollection):void
        {
            _collecteClientList = value;
        }

        public function get publishedTemplateList():ArrayCollection
        {
            return _publishedTemplateList;
        }

        public function set publishedTemplateList(value:ArrayCollection):void
        {
            _publishedTemplateList = value;
        }

        public function get identificationSuccess():Boolean
        {
            return _identificationSuccess;
        }

        public function set identificationSuccess(value:Boolean):void
        {
            _identificationSuccess = value;
        }

        public function get racineList():ArrayCollection
        {
            return _racineList;
        }

        public function set racineList(value:ArrayCollection):void
        {
            _racineList = value;
        }

        public function get uid():String
        {
            return _uid;
        }

        public function set uid(value:String):void
        {
            _uid = value;
        }

        public function get operateurWithTemplateList():ArrayCollection
        {
            return _operateurWithTemplateList;
        }

        public function set operateurWithTemplateList(value:ArrayCollection):void
        {
            _operateurWithTemplateList = value;
        }
    }
}