package composants.parametres.perimetres
{
	import mx.collections.ArrayCollection;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	import flash.events.ContextMenuEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import flash.events.Event;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import composants.util.ConsoviewFormatter;
	
	public class specialGrid extends DataGrid
	{
		private var _currentRollOverItem:Object;
		private var _dataArray:ArrayCollection;
		
		public function specialGrid():void
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		protected function initIHM(event:Event):void
		{
			allowMultipleSelection = true;
			var ligneColumn:DataGridColumn = new DataGridColumn();
			ligneColumn.dataField = "SOUS_TETE";
			ligneColumn.headerText = "Ligne";
			ligneColumn.labelFunction = formateLigne;
			
			var typeColumn:DataGridColumn = new DataGridColumn();
			typeColumn.dataField = "LIBELLE_TYPE_LIGNE";
			typeColumn.headerText = "Type";
			
			var fonctionColumn : DataGridColumn = new DataGridColumn();
			fonctionColumn.dataField = "COMMENTAIRES";
			fonctionColumn.headerText = "Fonction/Collaborateur";
			
			var arr:Array = new Array();
			arr.push(ligneColumn);
			arr.push(fonctionColumn);
			arr.push(typeColumn);
			
			this.columns = arr;

			this.addEventListener(ListEvent.ITEM_ROLL_OVER, itemRollover);
		    this.addEventListener(ListEvent.ITEM_ROLL_OUT, itemRollout);
		 	this.addEventListener(ListEvent.ITEM_CLICK, itemRollover);

		}
		
		protected function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}
		
		private function itemRollover(event:ListEvent):void
		{
			_currentRollOverItem = event.itemRenderer.data;
		}
		
		private function itemRollout(event:ListEvent):void
		{
			_currentRollOverItem = null;
		}
		
		public function setContextMenu(data:ArrayCollection):void
		{
			_dataArray = data;
			var cm:ContextMenu = new ContextMenu();
			cm.hideBuiltInItems();
			
			for (var i:int = 0; i < data.length; i++)
			{
				var item:ContextMenuItem = new ContextMenuItem(data.getItemAt(i).label);
				if (data.getItemAt(i).separate == true)
					item.separatorBefore = true;
				item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, data.getItemAt(i).callback);
				cm.customItems.push(item);
			}
			this.contextMenu = cm;

			cm.addEventListener(ContextMenuEvent.MENU_SELECT, displayContextOptions);
		}
		
		private function displayContextOptions(event:Event):void
		{
			var activate:Boolean = true;
			if (_currentRollOverItem == null)
			{
				this.selectedIndex = -1;
				activate= false;
			}
			else
			{
				//this.selectedItem = _currentRollOverItem;
				activate= true;
			}

			for (var i:int = 0; i < _dataArray.length; i++)
			{
				event.currentTarget.customItems[i].enabled=activate;
				if (_dataArray[i].forceEnabled == true)
					event.currentTarget.customItems[i].enabled= false;
			}
					
		}
		
	}
}