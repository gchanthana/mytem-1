package composants.tb.tableaux
{
	import flash.events.Event;

	public class TableauChangeEvent extends Event
	{
		public var LIBELLE : String;
		public var SEGMENT : String;
		public var QUANTITE : String;
		public var VOLUME : String;
		public var TYPE_THEME : String;
		public var SUR_THEME : String;
		public var MONTANT_TOTAL : String;
		public var TYPE : String;
		public var ID : String;
		public var THEME : String;
		public var NBAPPELS : String;
		public var IDPRODUIT :String;
		public var OPERATEUR : String;
		public var SOURCE : Object;
		public var IDENTIFIANT : String;
				
		public function TableauChangeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
			
		}
		
	}
}