 package composants.tb.tableaux
{
	import composants.tb.tableaux.info.InfosPanelView;
	import composants.util.ConsoviewFormatter;
	
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.IFlexDisplayObject;
	import mx.events.DataGridEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.CvAccessManager;
	import univers.facturation.tb.TableauDeBord;
	
	public class TableauProduitAbo extends TableauProduitAbo_IHM implements ITableauModel
	{
		private var myContextMenu:ContextMenu;
		 
     	private var opEdition : AbstractOperation;
		
		
		private var displayEvent : DisplayFacureEvent = new DisplayFacureEvent("displayFacture");
		
		private var _dataProviders : ArrayCollection; 	
		
		
		
		[Bindable]	
		private var _changeHandlerFunction : Function;				
		
		[Bindable]
		private var _totalQte : int =0;	
		
		[Bindable]
		private var _total : Number = 0;		
		
		private var _idproduit : int ;
		
		private var moisDeb : String;
		
		private var moisFin : String;
		
		private var perimetre : String;
		
		private var modeSelection : String;
		
		private var identifiant : String;
		
		private var segment : String;
		
		[Bindable]
		private var _title : String;
								
		/**
		 * Constructeur 
		 **/
		public function TableauProduitAbo(ev : TableauChangeEvent = null){
			 
			
			_idproduit = parseInt(ev.IDPRODUIT);
			_title = ev.LIBELLE + " : "+ev.OPERATEUR;
			identifiant = ev.IDENTIFIANT;
			segment = ev.SEGMENT;
			addEventListener(FlexEvent.CREATION_COMPLETE,init);	
		}
					
		/**
		 * Retourne le montant total
		 * */
		public function get total():Number{
			return _total;
		}
		
		/**
		* Permet de passer les valeurs aux tableaux du composant.
		* <p>Pour chaque tableau de la collection, un DataGrid sera affiché à la suite du précédant</p>
		* @param d Une collection de tableau. 
		**/				
		public function set dataProviders(d : ArrayCollection):void{

			_dataProviders = _formatterDataProvider(d);			
			drawTabs();
		}
		
		/**
		*  Retourne le dataproviders
		**/	
		[Bindable]			
		public function get dataProviders():ArrayCollection{
			return _dataProviders;
		}	
				
		/**
		* Permet d'affecter une fonction pour traiter les Clicks sur Les DataGrids du composant
		* @param changeFunction La fonction qui traite les clicks.<p> Elle prend un paramètre de type Object qui représente une ligne du DataGrid</p>
		**/		
		public function set changeHandlerFunction ( changeFunction : Function ):void{
			_changeHandlerFunction = changeFunction;
		}		
		
		
		/**
		*  Retourne une reference vers la fonction qui traite l'evenement change du tableau
		**/			
		public function get changeHandlerFunction():Function{
			return _changeHandlerFunction;
		}	
		
		/**
		 * Met à jour les donées du tableau
		 **/
		public function update():void{							
			chargerDonnees();
		}
		
		
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		public function clean():void{
			//to do
		}
		
		/**
		 * Met à jour la periode
		 * */		 
		public function updatePeriode(moisDeb : String = null, moisFin : String = null):void{
			//TOTO: implement function
			this.moisDeb = moisDeb;
			this.moisFin = moisFin;
		}		
		
		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...) 			
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		public function updatePerimetre(perimetre : String = null, modeSelection : String = null):void
		{
			this.perimetre = perimetre;
			this.modeSelection = modeSelection;				
		}
			
		/*---------- PRIVATE -----------------------*/
		
		//initialisation du composant
		private function init(fe :  FlexEvent):void{				
			title = _title;			
			myContextMenu = new ContextMenu();   			
            removeDefaultItems();                
            
            
            ///listener				
            myGrid.addEventListener(Event.CHANGE,changeHandler);
          
            myContextMenu.addEventListener(ContextMenuEvent.MENU_SELECT, menuSelectHandler);
            myBtAfficherFacture.addEventListener(MouseEvent.CLICK,btAfficherHandler);
            addEventListener("displayFacture",displayFacture);    
           	btAfficherInfos.addEventListener(MouseEvent.CLICK,btAfficherInfosClickHandler);
           
            myGrid.addEventListener(DataGridEvent.ITEM_EDIT_BEGINNING,checkEditionGrid);
            myGrid.addEventListener(DataGridEvent.ITEM_EDIT_END,enregistrerDonnee);
            
            DataGridColumn(myHeaderGrid.columns[2]).headerText = (segment.toLowerCase() == "mobile")?"Collaborateur":"Fonction";
            DataGridColumn(myHeaderGrid.columns[2]).showDataTips = true;
            myBtAfficherFacture.enabled = false;   
            btAfficherInfos.enabled = false; 	
		}		
		
		//formatage du telepone dans le grid
		private function formatDataTip(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);
			
		}
		
		 
		private function drawTabs():void{				
			myGrid.dataProvider = dataProviders	
			myGrid.columns[1].labelFunction = formatDataTip;
			myGrid.columns[2].editable = TableauDeBord.editable;		
			myFooterGrid.columns[1].headerText = nf.format(_totalQte);
			myFooterGrid.columns[2].headerText = cf.format(_total);
		}
		
		
		// permet de formatter la collection de DataProvider 
		private function _formatterDataProvider(d: ArrayCollection):ArrayCollection{
			_total = 0;
			_totalQte = 0;
			var ligne : Object;
			var col : ArrayCollection = new ArrayCollection();
			
		 
			for (ligne in d){
				col.addItem(formateObject (d[ligne]));
			}
		 
			return col;
		}
		
		private function isPresent(s:String , a : Array):Boolean{			
			var OK:Boolean = false;
			
			for (var i:int = 0; i < a.length; i++){				 
					 if ( s == a[i].toString() ) OK = true;				 
			}
			return OK;
		}
				
		protected function formateObject(obj : Object):Object{
			var o : Object = new Object();
			_total = _total + Number(obj.MONTANT_FINAL);
			_totalQte = _totalQte + int(obj.QTE);
			
					
			o["SITE"] =  obj.NOM_SITE;
			o["FACTURE"] = obj.NUMERO_FACTURE;
			
			o["QUANTITE"] = nf.format(obj.QTE);	
			
			o["TYPE_THEME"] = obj.TYPE_THEME;
			o["IDINVENTAIRE_PERIODE"] = obj.IDINVENTAIRE_PERIODE;						
			o["COMMENTAIRES"] = obj.COMMENTAIRES;			
			o["LIGNE"] = obj.SOUS_TETE;
			if ( obj.NOM_SITE != null && String(obj.NOM_SITE).length == 0){
				o["MONTANT_TOTAL"]= cf.format(Number(obj.MONTANT_FINAL));	
			}
			o["TYPE"] = "";	
			o["IDSOUS_TETE"] = obj.IDSOUS_TETE;				
			o["ID_SITE"] = obj.SITEID;
			o["FONCTION"] = "";	
			o["ID"] = obj.IDPRODUIT_CATALOGUE;	
				
			return o;
		}	
		
		protected function calculTotal(arr : Array, colname : String):Number{
			var total : Number = 0;
			
			for(var i:int=0; i< arr.length;i++){
				
				total = total + parseFloat(arr[i][colname]);
			}
			return total;
		}
		
		
		private function removeDefaultItems():void {
	            myContextMenu.hideBuiltInItems();
	            myContextMenu.builtInItems.print = false;
	            //var defaultItems:ContextMenuBuiltInItems = myContextMenu.builtInItems;
	            //defaultItems.print = true;
	    }
		
		private function addCustomMenuItems():void {
	            var item1:ContextMenuItem = new ContextMenuItem("");
	            var item2:ContextMenuItem = new ContextMenuItem("");
	            
	            myContextMenu.customItems.push(item1);
	            myContextMenu.customItems.push(item2);
	            
	            item1.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, menuItemFactureSelectHandler);
				item2.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, menuItemProprieteSelectHandler);
				
	    }
	    
	    private function removeCustomMenuItems():void {
           while(myContextMenu.customItems.length != 0){
           		myContextMenu.customItems.pop();
           }
	    }
		
		private function menuSelectHandler(event:ContextMenuEvent):void {
	        try{	
	        	if (String(myGrid.selectedItem.SITE).length == 0){//var item:ContextMenuItem = new ContextMenuItem("ceci est en plus");
	        		addCustomMenuItems();
	        		displayEvent.indexPeriode = myGrid.selectedItem.IDINVENTAIRE_PERIODE;
		        	displayEvent.affichable = true;        	
		        	displayEvent.numeroFacture = myGrid.selectedItem.FACTURE;	        	
		            event.currentTarget.customItems[0].caption = "Afficher facture n°"+myGrid.selectedItem.FACTURE;	        	
		            event.currentTarget.customItems[1].caption = "Propriétés de la ligne n°" + myGrid.selectedItem.LIGNE;	        	
		            
	         	}else{
	         		removeCustomMenuItems();
	  			}
  			}catch(e : Error){  					
	  				Alert.show("Vous devez sélectionner une facture");
	  				removeCustomMenuItems();
	  		}
        }

        private function menuItemFactureSelectHandler(event:ContextMenuEvent):void {      	
        	
        	dispatchEvent(displayEvent);            
        }
        
        private function menuItemProprieteSelectHandler(cme : ContextMenuEvent):void{
        	showInfosWindow();
        }
        
        
		
		//affiche la facture
		private function displayFacture(event:Event):void {
			 
				var url:String = ConsoViewModuleObject.NonSecureUrlBackoffice+"/fr/consotel/consoview/cfm/factures/display_Facture.cfm";
	            var variables:URLVariables = new URLVariables();
	            variables.ID_FACTURE = myGrid.selectedItem.IDINVENTAIRE_PERIODE;
	            variables.idx_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
	            variables.type_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE// perimetre;
	            
	            var request:URLRequest = new URLRequest(url);	           
	            request.data = variables;
	            request.method=URLRequestMethod.POST;
	            navigateToURL(request,"_blank");           
        }			
        //met a jour le numero de facture du labelFacture  
        private function changeHandler(ev : Event):void{
        	if (myGrid.selectedIndex != -1){
        		if (String(myGrid.selectedItem.SITE).length == 0){
	        		
	        		myBtAfficherFacture.enabled = true;
	        		btAfficherInfos.enabled = true;
	        		
	        		labelFacture.text = "Facture n° " + String(myGrid.selectedItem.FACTURE);
	        		labelLigne.text = "Ligne n° " + String(myGrid.selectedItem.LIGNE);
	        		myGrid.contextMenu = myContextMenu;
	        		
	        	}else{
	        		myBtAfficherFacture.enabled = false;
	        		btAfficherInfos.enabled = false;
	        		labelFacture.text = "";
	        		labelLigne.text = "";
	        		myGrid.contextMenu = null;
	        	}      
        	}else{
        		myBtAfficherFacture.enabled = false;
        		btAfficherInfos.enabled = false
	        	labelFacture.text = "";
	        	labelLigne.text = "";
	        	myGrid.contextMenu = null;
        	}
        	  	
        }
        
		
		private var infosWindow : InfosPanelView;		
		private function showInfosWindow():void{
			if(myGrid.selectedItems != null && myGrid.selectedItem.LIGNE != null){
				
				if (infosWindow != null) infosWindow = null;
				infosWindow = new InfosPanelView()
				infosWindow.addEventListener("infosWindowClosed",infosWindowCloseEventHandler);
				PopUpManager.addPopUp(infosWindow,this,true);
				PopUpManager.centerPopUp(infosWindow);
				infosWindow.ligne = myGrid.selectedItem;
				infosWindow.setFocus();
			}
		}
		
		private function infosWindowCloseEventHandler(ev : Event):void{
			btAfficherInfos.setFocus();
			PopUpManager.removePopUp(ev.currentTarget as IFlexDisplayObject);
		}
        
        
        private function btAfficherHandler(me : MouseEvent):void{
         		
    		displayEvent.indexPeriode = myGrid.selectedItem.IDINVENTAIRE_PERIODE;	        		
        	displayEvent.affichable = true;        	
        	displayEvent.numeroFacture = myGrid.selectedItem.FACTURE;	        	
    		dispatchEvent(displayEvent);     
        }
                
        private function btAfficherInfosClickHandler(me : MouseEvent):void{
        	showInfosWindow(); 
        }
        
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		protected function chargerDonnees():void{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.produits.facade",
																			"getDetailProduit",
																			chargerDonnneesResultHandler,null);
			
			RemoteObjectUtil.callService(op,
										perimetre,
										modeSelection,
										identifiant,
										_idproduit,
										moisDeb,
										moisFin,
										segment);
			
		}
		 private  function chargerDonnneesFaultHandler(fe :FaultEvent):void{
		 	trace(fe.fault.faultString,fe.fault.name);	
		 	 	
		 }     
		 
		 private function chargerDonnneesResultHandler(re :ResultEvent):void{
		 	try{
		 		 
		  		dataProviders =  re.result as ArrayCollection;	
		  	}catch(er : Error){		  	
		  		trace(er.message,"fr.consotel.consoview.tb.produits.facade getDetailProduit");
		  	}
		  
		 }  
		 
		 
		 
		 protected function checkEditionGrid(de : DataGridEvent):void{
		 	// Demande F le Blanc Incident : I-01075-VI9Z
            // Supprimer l'édition des noms des collaborateurs  
            if (myGrid.selectedItem != null){
            	if ((String(myGrid.selectedItem.SITE).length != 0) || (segment.toLowerCase() == "mobile")){
			 		de.preventDefault();
		 		}
            }else{
            	de.preventDefault();
            }
		 		 	
		 }
		 
		 
		 protected function enregistrerDonnee(de : DataGridEvent):void{		
		 		// Demande F le Blanc Incident : I-01075-VI9Z
	            // Supprimer l'édition des noms des collaborateurs             	
				if ((String(myGrid.itemEditorInstance.data.SITE).length == 0)&& (TableauDeBord.editable) && (segment.toLowerCase() != "mobile")){					
					
					 var myEditor:TextInput = TextInput(de.currentTarget.itemEditorInstance);
                
	                // Get the new value from the editor.
	                var newVal:String = myEditor.text;
	                
	                // Get the old value.
	                var oldVal:String = de.currentTarget.editedItemRenderer.data[de.dataField];                          
	
					opEdition  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.produits.facade",
																			"updateFunction",
																			enregistrerDonneeResultHandler);
	
					RemoteObjectUtil.callService(opEdition,
												myGrid.selectedItem.IDSOUS_TETE,
												newVal,
												segment); 
										 					
				}else{	
							
					de.preventDefault();					
				}   
				
		 }		
		 
		 protected function enregistrerDonneeResultHandler(re :ResultEvent):void{
		 	//Alert.show("ok");
		 }  								
		 
		 protected function defaultHandler(fe : FaultEvent):void{
		 	Alert.show("Erreur Remoting");
		 }
	}
}
