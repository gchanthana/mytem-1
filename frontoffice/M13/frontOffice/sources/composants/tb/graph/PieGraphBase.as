package composants.tb.graph
{
	import mx.events.FlexEvent;
	import composants.tb.graph.charts.PieGraph_IHM;
	import mx.charts.HitData;
	import mx.managers.PopUpManager;
	 
	import mx.core.IFlexDisplayObject;
	import flash.events.MouseEvent;
	import mx.controls.Alert;
	import flash.geom.Point;
	import flash.geom.Matrix;
	
	public class PieGraphBase extends GraphBase
	{
		 
		protected var graph : PieGraph_IHM = new PieGraph_IHM();
		 
		
		public function PieGraphBase()
		{
			//TODO: implement function
			 
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/*---------- PRIVATE ----------------*/
		private function init(fe : FlexEvent):void{

			graph.x = 0;
			
			graph.y = 0;
						
			myGraphContener.addChild(graph);
			
			graph.myPie.dataTipFunction = formatDataTip;		
			
						
		}
		 
		//formate les tootips du graph des themes
		protected function formatDataTip(obj:HitData):String{
		    var libelle :String = obj.item["Libellé"]; 
		    var montant:Number = obj.item["Montant"];
		  	var prop : Number = Math.abs((montant *  100 / _total));  
		  	
   	  
	    	return "Libellé : <b>"+libelle+
	    	"</b><br>montant : <b><font color ='#ff0000'>"+cf.format(montant)+"</font></b><br>Proportion :<b>"+prop.toPrecision(4)+"%</b>";;
		    
		}		
		
				
	}
}