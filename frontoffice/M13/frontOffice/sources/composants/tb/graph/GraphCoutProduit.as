package composants.tb.graph
{
	import mx.events.FlexEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	import composants.tb.graph.charts.PieGraph_IHM;
	import mx.rpc.events.ResultEvent;
	import mx.controls.Alert;
	 
	import mx.rpc.events.FaultEvent;
	import mx.controls.dataGridClasses.DataGridColumn;
	
	public class GraphCoutProduit extends PieGraphBase
	{
		private var op : AbstractOperation;
		
		[Bindable]
		private var _dataProviders : ArrayCollection;
		
		[Bindable]
		private var _dataProviders2 : ArrayCollection;
		
		private var _idTheme : int;						
		
		 
							
		public function GraphCoutProduit(idtheme : String = null){ 
			super();			
			 
			_idTheme =parseInt(idtheme);
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/**
		* Permet de passer les valeurs au composant.		
		* @param d Une collection d'objets. 
		**/						
		override public function set dataProviders(d : ArrayCollection):void{
			
			_dataProviders = _formatterDataProvider(d);// _formatterDataProvider(d);
			_dataProviders2 = _formatterDataProviderPie(d);			
			updateProviders();			
		}
		
		/**
		*  Retourne le dataproviders
		**/	
		[Bindable]			
		override public function get dataProviders():ArrayCollection{
			return _dataProviders;
		}	
	
					
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		override public function clean():void{		
			graph.myPie.dataProvider = null;
			myGrid.dataProvider = null;				
		}
				
		/**
		 * Met à jour les donées du tableau
		 **/
		override public function update():void{			
			//chargerDonnees(); deprecated		
			 
		}	
				
		/*------------- PROTECTED ------------------------------------------------------------*/
		
		//formate les donnees
		protected override function _formatterDataProvider(d: ArrayCollection):ArrayCollection{		
			var tmpCollection : ArrayCollection = new ArrayCollection();	
			var ligne : Object;				
			
			_total = 0;
			for	(ligne in d){					
					tmpCollection.addItem(formateObject(d[ligne]));					   
			}
			
			var totalObject : Object = new Object();
			totalObject["Libellé"] = "TOTAL";
			totalObject["Montant"] = cf.format(_total);
			tmpCollection.addItem(totalObject);			
			
			 		
			return tmpCollection;
		}	
		
		//formate les donnees
		protected function _formatterDataProviderPie(d: ArrayCollection):ArrayCollection{		
			
			var tmpCollection : ArrayCollection = new ArrayCollection();	
			var ligne : Object;				
			var len : int = d.length;
			var obj : Object = new Object();
			var chartData : ArrayCollection = new ArrayCollection();
			
						
			_total = 0;
			
			//////////////////formatage des données
			for	(ligne in d){		
				tmpCollection.addItem(formateObjectPie(d[ligne]));					   
			}
			/////////////////
			chartData = ObjectUtil.copy(tmpCollection) as ArrayCollection;
			
			/////////////////On coupe s'il y a trop de données
			if (len > 10){
				var sum : Number  = 0;
			
				for (var i : int = 9 ; i < len; i++){
					obj = chartData.getItemAt(i);
					sum = sum + parseFloat(obj["Montant"]);					
				}
				
				var len2 : int =  chartData.length ;
				
				for (var j : int = 10; j < len2 ; j++){
					chartData.removeItemAt(10);
				} 	
										
				chartData.getItemAt(9)["Libellé"] = "Autres...";
				chartData.getItemAt(9)["Montant"] = sum;					
			  
				
				 return chartData;
			}
			
			
			
			////////////////// si la somme des montant est null on retourne rien
			if(_total == 0){
				return null;
			}
			
			////////
			return tmpCollection;
		}	
					
		protected function formateObject(obj : Object):Object{
			var o : Object = new Object();
				
			_total = _total +  parseFloat(obj.MONTANT_FINAL);	
						
			o["Libellé"] = obj.LIBELLE_PRODUIT;		
			o["Type de produit"] = obj.TYPE_THEME;				
			o["Montant"]= cf.format(obj.MONTANT_FINAL);											
			
			return o;
		}	
		
		protected function formateObjectPie(obj : Object):Object{
			var o : Object = new Object();
				
			_total = _total +  parseFloat(obj.MONTANT_FINAL);		
			
			if ( String(obj.LIBELLE_PRODUIT).length > 45 ) 	o["Libellé"] = String(obj.LIBELLE_PRODUIT).substr(0,40)+"....";
			else o["Libellé"] = obj.LIBELLE_PRODUIT;			
			
			o["Type de produit"] = obj.TYPE_THEME;					
			o["Montant"]= Math.abs(parseFloat(obj.MONTANT_FINAL));							
			return o;
		}	
		
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		override protected function chargerDonnees():void{
			/* var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.theme.facade",
																			"getListeProduitsTheme",
																			chargerDonneesResultHandler,null);			
		
			RemoteObjectUtil.callService(op,
										perimetre,
										modeSelection,
										identifiant,
										_idTheme,
										moisDeb,
										moisFin); */
			
		}
		 
		 
		override protected function chargerDonneesResultHandler(re :ResultEvent):void{
		 	/* try{
		 		 
		  		dataProviders =  re.result as ArrayCollection;	
		  		 
		  	 
		  		
		  	}catch(er : Error){		  	
		  		trace(er.message,"Répartition par Thèmes graph cout produit");
		  	} */
		  
		 }	
			
		/*--------------- PRIVATE ----------------*/
		
		//initialisation du composant
		private function init(fe :FlexEvent):void{
			title = "Répartition par produits";
			myViewStack.width = 530;
			/* myViewStack.height = 235;
			myGraphContener.height = 320;
			myGridContener.height = 320; */
		}
			
		private function updateProviders():void{
			myGrid.dataProvider = dataProviders;//dataProviders;		
			DataGridColumn(myGrid.columns[1]).setStyle("textAlign","right"); 		
			graph.callLater(updatePie);
		}
		
		private function updatePie():void{
			graph.myPie.dataProvider = _dataProviders2;
			graph.myLegend.visible = false;		
		}
		
	}
} 