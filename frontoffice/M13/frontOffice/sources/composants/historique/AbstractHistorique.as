package composants.historique {
	import mx.controls.DataGrid;
	import univers.CvAccessManager;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.ResultEvent;

	public class AbstractHistorique extends DataGrid {
		private var dateDebField:Date;
		private var dateFinField:Date;
		private var monthColumnArray:Array;
		private var dataGridColumnArray:Array;
		
		public function AbstractHistorique() {
			super();
			var tmpDateDeb:Date = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateDeb;
			var tmpDateFin:Date = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateFin;
			dateDebField = new Date(tmpDateDeb.fullYear,tmpDateDeb.month);
			dateFinField = new Date(tmpDateFin.fullYear,tmpDateFin.month);
			buildColumnArray(dateDebField,dateFinField);
			draggableColumns = false;
		}
		
		private function buildColumnArray(dateDeb:Date,dateFin:Date):void {
			var monthDiff:int = 0;
			monthColumnArray = null;
			monthColumnArray = new Array();
			monthColumnArray.push(("M" + dateDeb.fullYear.toString() +
									(dateDeb.month + 1).toString())); // Car Janvier = 0, Décembre = 11
			while(dateDeb.getTime() != dateFin.getTime()) {
				dateDeb.month = dateDeb.month + 1;
				monthColumnArray.push(("M" + dateDeb.fullYear.toString() +
										(dateDeb.month + 1).toString())); // Car Janvier = 0, Décembre = 11
				monthDiff = monthDiff + 1;
			}
			monthDiff = monthDiff + 1; // Pour inclure le mois de départ
			trace("AbstractHistorique buildColumnArray() : " + "Month Diff = " + monthDiff.toString());
			var tmpColumn:DataGridColumn;
			dataGridColumnArray = columns; // Imposé par la documentation de Flex (GET)
			tmpColumn = new DataGridColumn("Société");
			tmpColumn.dataField = "@LBL";
			dataGridColumnArray.push(tmpColumn);
			tmpColumn = new DataGridColumn("Opérateur");
			tmpColumn.dataField = "@OPE";
			//tmpColumn.width = 150;
			dataGridColumnArray.push(tmpColumn);
			tmpColumn = new DataGridColumn("Offre");
			tmpColumn.dataField = "@OFR";
			//tmpColumn.width = 150;
			dataGridColumnArray.push(tmpColumn);
			var i:int;
			for(i = 0; i < monthDiff; i++) {
				tmpColumn = new DataGridColumn();
				tmpColumn.headerText = monthColumnArray[i];
				tmpColumn.dataField = "@" + monthColumnArray[i];
				tmpColumn.width = 60;
				tmpColumn.resizable = false;
				//tmpColumn.sortable = false;
				dataGridColumnArray.push(tmpColumn);
			}
			columns = dataGridColumnArray; // Imposé par la documentation de Flex (SET)
			getHistoData(null);
			trace("AbstractHistorique buildColumnArray() : " + columns.length.toString() + " - " + monthDiff.toString());
		}
		
		private function getHistoData(event:ResultEvent):void {
			if(event == null) { // Appel
				var typePerimetre:String = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
				var idPerimetre:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				var histoOp:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													"fr.consotel.consoview.facturation.optimisation.historique.HistoriqueContext",
													"getHistoData",getHistoData);
				var tmpDateDeb:Date = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateDeb;
				var tmpDateFin:Date = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateFin;
				RemoteObjectUtil.callService(histoOp,idPerimetre,typePerimetre,tmpDateDeb,tmpDateFin);
			} else { // Résultat
				var xmlHistoData:XML = event.result as XML;
				dataProvider = xmlHistoData..R;
				trace("1 :\n" + (xmlHistoData as XML).toXMLString());
				trace("2 :\n" + (xmlHistoData..R as XMLList).toXMLString());
			}
		}
		
		public function onPerimetreChange():void {
			trace("AbstractHistorique onPerimetreChange()");
		}
	}
}
