package composants.util {
	import mx.controls.HSlider;
	import mx.events.FlexEvent;
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import flash.ui.Mouse;
	import mx.events.SliderEvent;

	public class MonthPeriodHSlider extends HSlider {
		public static const sourceMonthMap:ArrayCollection =
					new ArrayCollection([{label: 'Jan', value: '01', fullLabel: 'Janvier', lastDay: '31'},
												{label: 'Fév', value: '02', fullLabel: 'Février', lastDay: '28'},
												{label: 'Mars', value: '03', fullLabel: 'Mars', lastDay: '31'},
												{label: 'Avril', value: '04', fullLabel: 'Avril', lastDay: '30'},
												{label: 'Mai', value: '05', fullLabel: 'Mai', lastDay: '31'},
												{label: 'Juin', value: '06', fullLabel: 'Juin', lastDay: '30'},
												{label: 'Juil', value: '07', fullLabel: 'Juillet', lastDay: '31'},
												{label: 'Aout', value: '08', fullLabel: 'Aout', lastDay: '31'},
												{label: 'Sept', value: '09', fullLabel: 'Septembre', lastDay: '30'},
												{label: 'Oct', value: '10', fullLabel: 'Octobre', lastDay: '31'},
												{label: 'Nov', value: '11', fullLabel: 'Novembre', lastDay: '30'},
												{label: 'Déc', value: '12', fullLabel: 'Décembre', lastDay: '31'},
												]);
		private var labelsArray:Array = null;
		private var currentYear:int = 0;
		private var startIndex:int = 0;
		private var dateDeb:String = null;
		private var dateFin:String = null;
		
		public function MonthPeriodHSlider() {
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:FlexEvent):void {
			this.tickInterval = 1;
			this.snapInterval = 1;
			this.minimum = 0;
			this.maximum = 11;
			this.value = 0;
			this.dataTipFormatFunction = getPeriodLabel;
			setStartMonth(new Date());
			if(thumbCount == 2) {
				this.getThumbAt(0).setStyle("fillColors",['#00FF00','#00FF00']); // Mois de début
				this.getThumbAt(1).setStyle("fillColors",['#FF0000','#FF0000']); // Mois de fin
			}
			else
				this.getThumbAt(0).setStyle("fillColors",['#FF0000','#FF0000']);
		}
		
		public function setThumbCount(thumbNum:int):void {
			this.thumbCount = thumbNum;
		}
		
		public function getSelectedMonth():String { // YYYY/MM
			var tmpIndex:int = this.value;
			if(tmpIndex < 0)
				return  (currentYear - 1) + "/" + sourceMonthMap[12 + tmpIndex].value;
			else
				return  currentYear+ "/" + sourceMonthMap[tmpIndex].value;
		}
		
		public function getDateDeb():String { // YYYY/MM
			return dateDeb;
		}

		public function getDateFin():String { // YYYY/MM
			return dateFin;
		}
		
		public function getSelectedMonthByThumbIndex(thumbIndex:int):String {
			var tmpIndex:int = this.values[thumbIndex];
			var tmpString:String = "";
			if(tmpIndex < 0)
				return  (currentYear - 1) + "/" + sourceMonthMap[12 + tmpIndex].value;
			else
				return  currentYear+ "/" + sourceMonthMap[tmpIndex].value;
		}
		
		public function setStartMonth(startDate:Date):void {
			currentYear = startDate.getFullYear();
			startIndex = startDate.getMonth() - 1; // Mois précédent le mois du jour
			this.maximum = startIndex;
			if(thumbCount == 2) { // Cas avec mois de début et mois de fin
				this.minimum = startIndex - 12; // Ex : 01 Juillet 06 au 31 Juin 07 (13 mois) Mois du jour : Juillet 07
				values[0] = startIndex - 1; // Mois de début
				if((values[0] + 1) < 0)
					dateDeb = (currentYear - 1).toString() + "/" + sourceMonthMap[(12 + values[0] + 1)].value + "/" + (12 + values[0] + 1);
				else
					dateDeb = currentYear.toString() + "/" + sourceMonthMap[values[0] + 1].value + "/" + (values[0] + 1);
				values[1] = startIndex; // Mois de fin
				if(values[1] < 0)
					dateFin = (currentYear - 1).toString() + "/" + sourceMonthMap[(12 + values[1])].value + "/" + (12 + values[1]);
				else
					dateFin = currentYear.toString() + "/" + sourceMonthMap[(values[1])].value + "/" + (values[1]);
			} else { // Cas avec seulement mois de début
				this.minimum = startIndex - 11; // Ex : Juillet 06 à Juin 07 (12 mois) Mois du jour : Juillet 07
				this.value = startIndex;
			}
			var i:int = 0;
			var tmpIndex:int = startIndex;
			if(thumbCount == 2) { // Cas avec mois de début et mois de fin
				labelsArray = new Array(13);
				labelsArray[12] = "";
			} else // Cas avec seulement le mois de début
				labelsArray = new Array(12);
			for(i = 11; i >= 0 ; i--) { // Tableau des libellé de l'IHM (non pas des tooltips)
				if(tmpIndex < 0)
					labelsArray[i] = sourceMonthMap[12 + tmpIndex].label + "/" + (currentYear - 1).toString().substr(2,2);
				else
					labelsArray[i] = sourceMonthMap[tmpIndex].label + "/" + currentYear.toString().substr(2,2);
				tmpIndex = tmpIndex - 1;
			}
			this.labels = labelsArray;
		}
		
		private function getPeriodLabel(value:String):String {
			var tmpYear:int = currentYear;
			var tmpMonthIndex:int = 0;
			if(thumbCount == 2)
				tmpMonthIndex = parseInt(value,10) + 1;
			else
				tmpMonthIndex = parseInt(value,10);
				
			if(thumbCount == 2) { // Cas avec 2 mois (début et fin)
				var yearToUse:int = 0;
				if(Math.abs(getThumbAt(1).mouseX) < Math.abs(getThumbAt(0).mouseX)) { // Mois de fin
					var debMonthIndex:int = tmpMonthIndex - 1; // Mois précédent
					if(debMonthIndex < 0) {
						debMonthIndex = 12 + debMonthIndex;
						yearToUse = tmpYear - 1;
					} else
						yearToUse = tmpYear;
					dateFin = yearToUse.toString() + "/" + sourceMonthMap[debMonthIndex].value;
					return sourceMonthMap[debMonthIndex].lastDay + " " +
						sourceMonthMap[debMonthIndex].label + " " + yearToUse.toString().substr(2,2);
				} else if(Math.abs(getThumbAt(0).mouseX) < Math.abs(getThumbAt(1).mouseX)) { // Mois de début
					if(tmpMonthIndex < 0) {
						tmpMonthIndex = 12 + tmpMonthIndex;
						yearToUse = tmpYear - 1;
					} else
						yearToUse = tmpYear;
					dateDeb = yearToUse.toString() + "/" + sourceMonthMap[tmpMonthIndex].value;
					return "01 " + sourceMonthMap[tmpMonthIndex].label + " " + yearToUse.toString().substr(2,2);
				} else {
					this.setThumbValueAt(0,values[0]);
					this.setThumbValueAt(1,values[1]);
					return "Choisir 2 valeurs différentes";
				}
			} else { // Cas avec 1 seul mois
				if(tmpMonthIndex < 0) {
					tmpMonthIndex = 12 + tmpMonthIndex;
					yearToUse = tmpYear - 1;
				} else
					yearToUse = tmpYear;
				return sourceMonthMap[tmpMonthIndex].label + " " + yearToUse.toString().substr(2,2);
			}
		}
	}
}
