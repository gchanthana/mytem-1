package composants.util
{
	
	import mx.containers.VBox;
	 

	public class ExportCSVIcon extends VBox
	{

		private var _textLabel : String;
		
		[Bindable]
		public function get textLabel():String{			
			return _textLabel;
		}		
		public function set textLabel(s : String):void{
			_textLabel = s;
		}
		
	
		public function ExportCSVIcon()
		{
			super();
		}
		
	}
}