package composants.util.sav
{
	public class Ticket
	{
		
			public var IDSAV: Number = 0;
		    public var IDEQUIPEMENT: Number = 0;
		    public var IDCONTRAT: Number = 0;
		    public var DATE_DEB: String = "";
		    public var DESCRIPTION: String ="";
		    public var GARANTIE_APPLICABLE: Number = 0;
		    public var IDFOURNISSEUR: Number = 0;
		    public var IDSITE: String = "";
		    public var IDUSER: String = "";
		    public var COMMENTAIRES: String = "";
		    public var NUMERO_SAV: Number = 0;
		

	}
}