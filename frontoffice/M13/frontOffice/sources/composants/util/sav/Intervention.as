package univers.inventaire.equipements.gestionflottemobile.fiches.system.sav
{
	[Bindable]
	public class Intervention
	{
			public var IDINTERVENTION: Number = 0;
			public var IDSAV: Number = 0;
		    public var IDMOTIF: Number = 0;
		    public var IDTYPE: Number = 0;
		    public var LIBELLE: String ="";
		    public var REFERENCE: String ="";
		    public var CODE_INTERNE: Number = 0;
		    public var DATE_DEB: Date = new Date();
		    public var DATE_FIN: Date = new Date();
		    public var DUREE: String = "";
		    public var COUT_INTERVENTION: Number = 0;
		    public var COMMENTAIRE: String = "";
		   
		

	}
}