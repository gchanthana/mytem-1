package univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts
{
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.PanelRechercheContact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.PanelRechercheSociete;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Contact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
	import composants.util.ConsoviewAlert;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	import mx.events.CloseEvent;
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	import mx.core.UIComponent;
	import mx.controls.Label;
	import flash.display.DisplayObject;
	
	
	/**
	 * Classe gérant le formulaire de saisi du contact
	 * 
	 * */
	public class GestionContacts extends Canvas
	{
		
		
		/**		
		 * Constant definissant le type de l'evenement dispatché lors d'une selection de société
		 * */
		public static const SOCIETE_CHANGED : String = "societeChanged";
		
		/**		
		 * Constant definissant le type de l'evenement dispatché lors de la fin du remplissage de la combo societe
		 * */
		public static const COMBO_SOCIETE_READY : String = "cmbSocieteReady";
		
		/**
		 * Reference vers la ComboBox de selection de l'Agence
		 * */
		public var cmbAgence : ComboBox; 
		
		/**
		 * Reference vers la ComboBox de selection du distributeur
		 * */
		public var cmbDistributeur : ComboBox;
		
		/**
		 * Reference vers la ComboBox de selection du contact
		 * */
		public var cmbContact : ComboBox;
	
		
		/**
		 * Reference vers l'Image permettant d'afficher la recherche de société
		 * */
		public var imgSearchSociete : Image;
		
		
		/**
		 * Reference vers l'Image permettant d'afficher la recherche de constact
		 * */
		public var imgSearchContact : Image;
		
		/**
		 * Reference vers le label affichant le titre
		 * */
		public var lblTitre : Label;
		
		/**
		 * Réference locale vers le contact
		 * */ 
		protected var contacts : Contact;
		
		/**
		 * Réference locale vers le distributeur
		 * */ 
		protected var distribs : Societe;
		
		//Reference vers la recherche de Contact	
		private var rechercheContact : PanelRechercheContact;

		//Reference vers la recherche de Société
		private var rechercheSociete : PanelRechercheSociete;
		
		//Reference locale vers la commande
		private var _commande : Commande;		
		
		//Reference locale vers le panier de la commande		
		private var _panier : ElementCommande;
		
		//Reference locale vers l'idgroupe sur lequel ont est 
		private var _idGroupe : int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
		
		//Booleen à true si un contact est selectionné
		private var selectContact : Boolean = false;
		
		//Booleen à true si une agence est selectionnée
		private var selectAgence : Boolean = false;
		
		//Booleen à true si une agence est selectionnée autoSelectSelectContact
		private var selectAgence2 : Boolean = false;
		
		//Booleen à true si une agence est selectionnée via autoSelectSelectContact
		private var selectContact2 : Boolean = false;
		
		//ref vers le contact selectionne
		private var contactSelected : Contact;
		
		//Etape pour l'auto selection du contact voir la fonction 'autoSelectContact'
		private var step : int = 0;
	
		/**
		 * Constructeur
		 * */
		public function GestionContacts()
		{	
			super();
		}
		
		
		/**
		 * Affecte le panier et la commande
		 * @param cmde la commande
		 * @param panier le panier
		 * */
		public function setCommande(cmde : Commande, panier : ElementCommande):void{
			_commande = cmde;
			_panier = panier;
		}
		
		/**
		 * suprrime tte les fenêtres surgissantes
		 * (fait le ménage)
		 * */
		public function clean():void{
			if (rechercheContact != null) rechercheContact.clean();
			rechercheContact = null;
			if (rechercheSociete != null) rechercheSociete.clean();
			rechercheSociete = null;
		} 
		
		/**
		 * Affect le contact à la commande
		 * @param cmde la commande
		 * */
		public function affecterContactCommande(cmde : Commande):void{
			
			if (cmbContact.selectedIndex != -1){
				_commande.contactID = Contact(cmbContact.selectedItem).contactID;					
			}
			
			if (cmbAgence.selectedIndex != -1){				
				_commande.societeID = Societe(cmbAgence.selectedItem).societeID;	
			} 
			
			if (cmbDistributeur.selectedIndex != -1){		
				if (_commande.societeID <= 0){
					_commande.societeID = Societe(cmbDistributeur.selectedItem).societeID;		
				}		
			}
		}
		
		/**
		 * Retourne l'identifiant de la societe ou de l'agence ou ou
		 * @return SocieteID
		 * */
		public function getSociete():int{
			
					
			if (cmbAgence.selectedIndex != -1){				
				if (Societe(cmbAgence.selectedItem).societeID > 0)
					return Societe(cmbAgence.selectedItem).societeID				
			} 
			
			if (cmbDistributeur.selectedIndex != -1){		
				if (Societe(cmbDistributeur.selectedItem).societeID > 0){
					return Societe(cmbDistributeur.selectedItem).societeID;		
				}		
			}
			
			return 0;
			
		}
		
		/**
		* Retourne l'identifiant du contact ou 0
		* @return ContactID
		* */
		public function getContact():int{
		 	if (cmbContact.selectedIndex != -1){		
				return 	Contact(cmbContact.selectedItem).contactID;	
			}
			return 0;
		}
		
		/**
		 * Selectionne le distributeur, l'agence et le contact, on doit attendre l'event COMBO_SOCIETE_READY
		 * avant d'utilisé cette méthode
		 * 
		 * @param Societe le distributeur
		 * @param Societe l'agence
		 * @param Contact le contact
		 * */
		public function autoSelectContact(distrib : Societe, agence : Societe, ct : Contact):void{							
			 
			switch (step){
				case 0 : {
					if(ct){
						if (ct.contactID > 0){
							contactSelected = ct;				
							autoSelectSelectionnerContact(ct);	
						}else{
							if(distrib)
								if (distrib.societeID > 0){
									cmbDistributeur.selectedIndex = 
										ConsoviewUtil.getIndexById(cmbDistributeur.dataProvider as ArrayCollection,
																		"societeID",distrib.societeID);
																		
									if(agence){
										if (agence.societeID > 0){
											distribs = agence;
											selectAgence = true;								
										}
									}									
									Societe(cmbDistributeur.selectedItem).addEventListener(Societe.LISTE_AGENCE_COMPLETE,autoSelectRemplirComboAgence);								
									Societe(cmbDistributeur.selectedItem).getAgences();			
								}		
						}	
					}else
					if(distrib)
						if (distrib.societeID > 0){
							cmbDistributeur.selectedIndex = 
								ConsoviewUtil.getIndexById(cmbDistributeur.dataProvider as ArrayCollection,
																"societeID",distrib.societeID);
																
							if(agence){
								if (agence.societeID > 0){
									distribs = agence;
									selectAgence = true;								
								}
							}									
							Societe(cmbDistributeur.selectedItem).addEventListener(Societe.LISTE_AGENCE_COMPLETE,autoSelectRemplirComboAgence);								
							Societe(cmbDistributeur.selectedItem).getAgences();			
						}
					
					break
				}
				case 1 : {
					cmbAgence.labelField = "raisonSociale";	
					cmbAgence.dataProvider = Societe(cmbDistributeur.selectedItem).listeAgences;								
					
					cmbAgence.selectedIndex = 
								ConsoviewUtil.getIndexById(cmbAgence.dataProvider as ArrayCollection,
																"societeID",distribs.societeID); 
					selectAgence = false;		
					cmbAgence.dispatchEvent(new ListEvent(ListEvent.CHANGE));
									
					
					break
				}
				
			}
					
		}
		
		private function fooAgence(agence : Societe):void{
			if (agence.societeID > 0){
				cmbAgence.selectedIndex = 
					ConsoviewUtil.getIndexById(cmbAgence.dataProvider as ArrayCollection,
													"societeID",agence.societeID);
				cmbAgence.dispatchEvent(new ListEvent(ListEvent.CHANGE));	
				//cmbDistributeur.callLater(fooAgence);															
			}
		}
		
		//Initialisation du distributeur et du contact
		protected function init():void{
			distribs = new Societe();			
			distribs.addEventListener(Societe.LISTE_COMPLETE,remplirComboSociete);			
			distribs.prepareList(_idGroupe);
			
			contacts = new Contact();
			contacts.addEventListener(Contact.LISTE_SOCIETE_COMPELETE,remplirComboContact);							
		}		
		
		
		/**
		 * Affiche la fenetre de recherche de contact		 
		 * */
		protected function afficherRechercheContact(me :MouseEvent):void{
			rechercheContact = new PanelRechercheContact();	
			rechercheContact.addEventListener(PanelRechercheContact.CREER,rafraichirContact);
			rechercheContact.addEventListener(PanelRechercheContact.EFFACER,rafraichirContact);
			rechercheContact.addEventListener(PanelRechercheContact.VALIDER,selectionnerContact);		
			
			ConsoviewUtil.scale(DisplayObject(rechercheContact),ConsoViewModuleObject.W_ratio,ConsoViewModuleObject.H_ratio);
			
			PopUpManager.addPopUp(rechercheContact,UIComponent(parentApplication),true);	
			PopUpManager.centerPopUp(rechercheContact);
		}
		
		
		/**
		 * Rafraichit la liste des contacts pour le groupe client courrant
		 * */
		protected function rafraichirContact(ev : Event):void{
			contacts.prepareList(_idGroupe);
		}
		
		
		/**
		 * Affiche la fenetre de recherche de société		 
		 * */
		protected function afficherRechercheSociete(me : MouseEvent):void{
			rechercheSociete = new PanelRechercheSociete();
			rechercheSociete.addEventListener(PanelRechercheSociete.CREER,rafraichirSociete);
			rechercheSociete.addEventListener(PanelRechercheSociete.EFFACER,rafraichirSociete);
			rechercheSociete.addEventListener(PanelRechercheSociete.VALIDER,selectionnerSociete);
			ConsoviewUtil.scale(DisplayObject(rechercheSociete),ConsoViewModuleObject.W_ratio,ConsoViewModuleObject.H_ratio);		
			PopUpManager.addPopUp(rechercheSociete,UIComponent(parentApplication),true);	
			PopUpManager.centerPopUp(rechercheSociete);		
		}
		
		/**
		 * rafraichit la liste des distributeurs pour le groupe client courrant
		 * */
		protected function rafraichirSociete(ezv : Event):void{
			distribs.prepareList(_idGroupe);
		}
		
		/**
		 * Selectionne le contact provenant de la recherche dans la ComboBox Contact
		 * selectionne la société du contact 
		 * */
		protected function selectionnerContact(ev : Event):void{			
			var myContact : Contact = rechercheContact.contactSelectionne;	
			var indexOfSocieteCtc  : int = ConsoviewUtil.getIndexById(cmbDistributeur.dataProvider as ArrayCollection,"societeID",myContact.societeID);
			var mySociete : Societe = Societe(cmbDistributeur.dataProvider[indexOfSocieteCtc]);
			
			if (mySociete.societeParenteID > 0){
				var indexOfDistrib : int = ConsoviewUtil.getIndexById(cmbDistributeur.dataProvider as ArrayCollection,"societeID",mySociete.societeParenteID);    				
				var myDistrib : Societe = Societe(cmbDistributeur.dataProvider[indexOfDistrib]); 	
				cmbDistributeur.selectedIndex = indexOfDistrib;
				selectAgence = true;
				cmbDistributeur.dispatchEvent(new ListEvent(ListEvent.CHANGE));	
				
				
			}else{
				cmbDistributeur.selectedIndex = indexOfSocieteCtc;				
				cmbDistributeur.dispatchEvent(new ListEvent(ListEvent.CHANGE));	
			}
			
			selectContact = true;
		}  
		
		
		/**
		 * Selectionne la société, provenant de la recherche, dans la ComboBox Contact
		 * */
		protected function selectionnerSociete(ev : Event):void{
			cmbDistributeur.selectedIndex = ConsoviewUtil.getIndexById(cmbDistributeur.dataProvider as ArrayCollection,"societeID",rechercheSociete.societeSelectionneeID);				
			cmbDistributeur.dispatchEvent(new ListEvent(ListEvent.CHANGE));
		}
		
		/**
		 * Selectionn la société et l'agence du contact
		 * (Ne fait rien pour le moment)
		 * */
		protected function selectSocieteNAgenceFromContact(le : ListEvent):void{
			
			if (cmbContact.selectedIndex != -1){
				
				/* cmbDistributeur.selectedIndex = ConsoviewUtil.getIndexById(cmbDistributeur.dataProvider as ArrayCollection,
											"societeID",Contact(cmbContact.selectedItem).societeID);																				 */
			}
			
			
		}		
		
		/**
		 * Charge les agences d'un distributeur
		 * */
		protected function selectAgenceFromDistrib(le : ListEvent):void{
			Societe(cmbDistributeur.selectedItem).addEventListener(Societe.LISTE_AGENCE_COMPLETE,remplirComboAgence);								
			Societe(cmbDistributeur.selectedItem).getAgences();			
		}
		
		 
		/* protected function selectAgenceByDistrib(le : ListEvent):void{
			if (cmbDistributeur.selectedIndex != -1){
				Societe(cmbDistributeur.selectedItem).getAgences(); 
				//cmbAgence.dispatchEvent(new ListEvent(ListEvent.CHANGE));
			}			
		} */
		
		
		
		/**
		 * Dispatch un évenement signifiant que la société vient de changer		 
		 * */
		protected function societeChangeHandler(ev : ListEvent):void{
			var evtObj : ContactChangeEvent = new ContactChangeEvent(GestionContacts.SOCIETE_CHANGED);
			var idSociete : int;
			if (cmbAgence.selectedIndex != -1){		
						
				if(Societe(cmbAgence.selectedItem).societeID > 0){
					evtObj.societe = Societe(cmbAgence.selectedItem);	
						
					chargerSocieteContacts(Societe(cmbAgence.selectedItem).societeID);									
				} 
				else{
					evtObj.societe = Societe(cmbDistributeur.selectedItem);
					chargerSocieteContacts(Societe(cmbDistributeur.selectedItem).societeID);
				} 
			}else if (cmbDistributeur.selectedIndex != -1){
				if(Societe(cmbDistributeur.selectedItem).societeID > 0){
					evtObj.societe = Societe(cmbDistributeur.selectedItem);		
					chargerSocieteContacts(Societe(cmbDistributeur.selectedItem).societeID);									
				}					
			}
						
			dispatchEvent(evtObj);
		}
		
		/* protected function operateurChangedHandler(ev : ListEvent):void{				
			
		} */
		
		
		// Remplit la ComboContact avec la liste des contacts		 
		private function remplirComboContact(ev : Event):void{	
			var blank_ctc : Contact = new Contact();
			blank_ctc.contactID = 0;
			blank_ctc.nom = "---";
			blank_ctc.prenom = "---";	
			 
			cmbContact.labelField = "nom";			
			contacts.liste.addItemAt(blank_ctc,0);
			cmbContact.dataProvider = contacts.liste;		
			if (selectContact){
				cmbContact.selectedIndex = ConsoviewUtil.getIndexById(cmbContact.dataProvider as ArrayCollection,"contactID",rechercheContact.contactSelectionne.contactID);			
				cmbContact.dispatchEvent(new ListEvent(ListEvent.CHANGE));
				selectContact = false;
			}else if (selectContact2){
				cmbContact.selectedIndex = ConsoviewUtil.getIndexById(cmbContact.dataProvider as ArrayCollection,"contactID",contactSelected.contactID);			
				cmbContact.dispatchEvent(new ListEvent(ListEvent.CHANGE));
				selectContact2 = false;
			}else{
				cmbContact.selectedItem = 0;	
			}
		}
		
		
		//Remplit la ComboSociete avec la liste des Societes(Distributeurs)		
		private function remplirComboSociete(ev : Event):void{
			cmbDistributeur.labelField = "raisonSociale";
			cmbDistributeur.dataProvider = distribs.liste; 
			dispatchEvent(new Event(COMBO_SOCIETE_READY));				
		}
		
		
		//Remplit la ComboAgence avec la liste des Agences
		private function remplirComboAgence(ev : Event):void{						 
			cmbAgence.labelField = "raisonSociale";		
						
			cmbAgence.dataProvider = Societe(cmbDistributeur.selectedItem).listeAgences;								
			cmbAgence.selectedIndex = 0;
			
			if (selectAgence){
				cmbAgence.selectedIndex = ConsoviewUtil.getIndexById(cmbAgence.dataProvider as ArrayCollection,"societeID",rechercheContact.contactSelectionne.societeID);				
				selectAgence = false;
				dispatchEvent(new ListEvent(ListEvent.CHANGE));
			}else if (selectAgence2){
				cmbAgence.selectedIndex = ConsoviewUtil.getIndexById(cmbAgence.dataProvider as ArrayCollection,"societeID",contactSelected.societeID);				
				selectAgence2 = false;
				dispatchEvent(new ListEvent(ListEvent.CHANGE));
			}else{
				cmbContact.selectedItem = 0;	
			}
			cmbAgence.dispatchEvent(new ListEvent(ListEvent.CHANGE));			
		}
		
		//Remplit la ComboAgence avec la liste des Agences de la societe quand autoSelectContact
		private function autoSelectRemplirComboAgence(ev : Event):void{						 			
			step = 1;
			autoSelectContact(null,null,null);		
		}
		
		//
		// Selectionne le contact provenant de la recherche dans la ComboBox Contact
		// selectionne la société du contact 
		// quand autoSelectContac
		private function autoSelectSelectionnerContact(ct : Contact):void{				 	
			var myContact : Contact = ct;
			var indexOfSocieteCtc  : int = ConsoviewUtil.getIndexById(cmbDistributeur.dataProvider as ArrayCollection,"societeID",myContact.societeID);
			var mySociete : Societe = Societe(cmbDistributeur.dataProvider[indexOfSocieteCtc]);
			
			if (mySociete.societeParenteID > 0){
				var indexOfDistrib : int = ConsoviewUtil.getIndexById(cmbDistributeur.dataProvider as ArrayCollection,"societeID",mySociete.societeParenteID);    				
				var myDistrib : Societe = Societe(cmbDistributeur.dataProvider[indexOfDistrib]); 	
				cmbDistributeur.selectedIndex = indexOfDistrib;
				selectAgence2 = true;
				cmbDistributeur.dispatchEvent(new ListEvent(ListEvent.CHANGE));	
				
				
			}else{
				cmbDistributeur.selectedIndex = indexOfSocieteCtc;				
				cmbDistributeur.dispatchEvent(new ListEvent(ListEvent.CHANGE));	
			}
			selectContact2 = true;
		}  
		
		
		//Charge la liste des contact d'une société
		//param idSociete l'identifiant de la société 
		private function chargerSocieteContacts(idSociete : int):void{
			contacts.prepareSocieteList(idSociete);			
		}
		
		
	}
}