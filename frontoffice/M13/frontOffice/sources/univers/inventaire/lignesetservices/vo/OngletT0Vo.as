package univers.inventaire.lignesetservices.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	[Bindable]
	public class OngletT0Vo extends OngletLAVo implements ItoXMLParams
	{		
		public var NBRE_CANAUX:Number=0;
		
		public function OngletT0Vo()
		{
			super();
		}
		
		override public function toXMLParams(registerInLog:int=0):XML
		{
			_xmlParams = super.toXMLParams();
			XmlParamUtil.addParam(_xmlParams,"NBRE_CANAUX","Le nombre de canaux",NBRE_CANAUX);		
			return _xmlParams;
		}
		
	}
}