package univers.inventaire.lignesetservices.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	[Bindable]
	public class TrancheSDAVo implements ItoXMLParams
	{
		protected var _xmlParams : XML;
		
		public var IDSOUS_TETE:Number=0;
		public var IDTRANCHE_SDA:Number=0;
		public var TRANCHE_SDA:String="";
		public var NBRE_SDA:String="";
		public var NUM_DEPART:String="";
		public var NUM_FIN:String="";
		public var TYPE_TRANCHE:String=""; 
		
		public function TrancheSDAVo(){
			
		}	
			
		public function toXMLParams(registerInLog:int=0):XML
		{
			_xmlParams = XmlParamUtil.createXmlParamObject();
			
			XmlParamUtil.addParam(_xmlParams,"IDSOUS_TETE","l'identifiant de la ligne",IDSOUS_TETE);			
			XmlParamUtil.addParam(_xmlParams,"IDTRANCHE_SDA","l'identifiant de la tranche",IDTRANCHE_SDA);			
			XmlParamUtil.addParam(_xmlParams,"TRANCHE_SDA","le libellé de la tranche",TRANCHE_SDA);
			XmlParamUtil.addParam(_xmlParams,"NBRE_SDA","le nombre de SDA dans la tranche",NBRE_SDA);
			XmlParamUtil.addParam(_xmlParams,"NUM_DEPART","le numéro de départ de la tranche",NUM_DEPART);
			XmlParamUtil.addParam(_xmlParams,"NUM_FIN","le dernier numéro de la tranche",NUM_FIN);
			XmlParamUtil.addParam(_xmlParams,"TYPE_TRANCHE","le type de tranche",TYPE_TRANCHE);
			
			return _xmlParams;
		}
		
	}
}