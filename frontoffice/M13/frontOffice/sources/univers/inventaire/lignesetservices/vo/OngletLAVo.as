package univers.inventaire.lignesetservices.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class OngletLAVo implements ItoXMLParams
	{
		protected var _xmlParams : XML;
		
		
		public var IDSOUS_TETE:Number=0;
		
		public var NOM_SITE_INSTALL:String="-";
		public var ADRESSE1_INSTALL:String="-";
		public var ADRESSE2_INSTALL:String="-";
		public var ZIPCODE_INSTALL:String="-";
		public var VILLE_INSTALL:String="-";
		
		
		public var BAT:String="-";
		public var ETAGE:String="-";
		public var SALLE:String="-";
		public var NOEUD:String="-";
		
		
		public var REGLETTE1:String="-";
		public var PAIRE1:String="-";
		public var REGLETTE2:String="-";
		public var PAIRE2:String="-";
	
	
		public var AMORCE:String="-";
		public var NOMBRE_POSTES:Number=0;
			
			
		public var CARTE_PABX:String="-";
		public var SECRET_IDENTIFIANT:Number=0;
		public var PUBLICATION_ANNUAIRE:Number=0;
		public var DISCRIMINATION:String="-";
		
		public var COMMENTAIRE_CABLAGE:String="-";
		
		public var V7:String="-";		
		public var V8:String="-";
		
		
		public function OngletLAVo(){
			
		}
		
		public function toXMLParams(registerInLog : int=0):XML{
			_xmlParams = XmlParamUtil.createXmlParamObject();
			
			XmlParamUtil.addParam(_xmlParams,"IDSOUS_TETE","l'identifiant de la ligne",IDSOUS_TETE);
			XmlParamUtil.addParam(_xmlParams,"NOM_SITE_INSTALL","le nom du site d'installation",NOM_SITE_INSTALL);
			XmlParamUtil.addParam(_xmlParams,"ADRESSE1_INSTALL","l'addresse du site d'installation",ADRESSE1_INSTALL);
			XmlParamUtil.addParam(_xmlParams,"ADRESSE2_INSTALL","complément d'addresse du site d'installation",ADRESSE2_INSTALL);
			XmlParamUtil.addParam(_xmlParams,"ZIPCODE_INSTALL","le code postal",ZIPCODE_INSTALL);
			XmlParamUtil.addParam(_xmlParams,"VILLE_INSTALL","la ville d'installation",VILLE_INSTALL);
			
			XmlParamUtil.addParam(_xmlParams,"BAT","localisation lbâtiment",BAT);
			XmlParamUtil.addParam(_xmlParams,"ETAGE","localisation  étage",ETAGE);
			XmlParamUtil.addParam(_xmlParams,"SALLE","localisation salle",SALLE);
			XmlParamUtil.addParam(_xmlParams,"NOEUD","localisation noeud",NOEUD);
			
			XmlParamUtil.addParam(_xmlParams,"REGLETTE1","reglette1",REGLETTE1);
			XmlParamUtil.addParam(_xmlParams,"PAIRE1","paire1",PAIRE1);
			XmlParamUtil.addParam(_xmlParams,"REGLETTE2","reglette2",REGLETTE2);
			XmlParamUtil.addParam(_xmlParams,"PAIRE2","paire2",PAIRE2);
			
			XmlParamUtil.addParam(_xmlParams,"AMORCE","Amorce",AMORCE);
			XmlParamUtil.addParam(_xmlParams,"NOMBRE_POSTES","Nombre de postes",NOMBRE_POSTES);
						
			XmlParamUtil.addParam(_xmlParams,"CARTE_PABX","la carte PABX",CARTE_PABX);
			XmlParamUtil.addParam(_xmlParams,"SECRET_IDENTIFIANT","secret identifiant ",SECRET_IDENTIFIANT);
			XmlParamUtil.addParam(_xmlParams,"PUBLICATION_ANNUAIRE","publication annuaire",PUBLICATION_ANNUAIRE);
			XmlParamUtil.addParam(_xmlParams,"DISCRIMINATION","Discrimination",DISCRIMINATION);			
			
			XmlParamUtil.addParam(_xmlParams,"COMMENTAIRE_CABLAGE","commentaires",COMMENTAIRE_CABLAGE);
			XmlParamUtil.addParam(_xmlParams,"V7","Valeurs du champ perso 7",V7);
			XmlParamUtil.addParam(_xmlParams,"V8","Valeurs du champ perso 8",V8);
			
			return _xmlParams;
		}
		
		
	}
}