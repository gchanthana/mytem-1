package univers.inventaire.lignesetservices.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	[Bindable]
	public class OngletLSVo implements ItoXMLParams
	{
		public var IDSOUS_TETE:Number = 0;
		
		public var IDLL : String="-";
		public var LS_LEGA_TETE : String="-";
		public var LS_LEGA_ELEM_ACTIF : String="-";
		public var LS_LEGA_NOMSITE : String="-";
		public var LS_LEGA_ADRESSE1 : String="-";
		public var LS_LEGA_ADRESSE2 : String="-";
		public var LS_LEGA_ZIPCODE : String="-";
		public var LS_LEGA_COMMUNE : String="-";
		public var LS_LEGA_ELEM_ACTIF_AMORCE_E : String="-";
		public var LS_LEGA_ELEM_ACTIF_AMORCE_R : String="-";
		public var LS_LEGA_TETE_AMORCE_E : String="-";
		public var LS_LEGA_TETE_AMORCE_R : String="-";
		public var LS_NATURE : String="-";
		public var LS_DEBIT2 : String="-";
		public var LS_DISTANCE2 : String="-";
		public var LS_LEGB_TETE : String="-";
		public var LS_LEGB_ELEM_ACTIF : String="-";
		public var LS_LEGB_NOMSITE : String="-";
		public var LS_LEGB_ADRESSE1 : String="-";
		public var LS_LEGB_ADRESSE2 : String="-";
		public var LS_LEGB_ZIPCODE : String="-";
		public var LS_LEGB_COMMUNE : String="-";
		public var LS_LEGB_ELEM_ACTIF_AMORCE_E : String="-";
		public var LS_LEGB_ELEM_ACTIF_AMORCE_R : String="-";
		public var LS_LEGB_TETE_AMORCE_E : String="-";
		public var LS_LEGB_TETE_AMORCE_R : String="-";
		
		
		public var V7 : String="-";
		public var V8 : String="-";
	
		protected var _xmlParams : XML;
		
		public function OngletLSVo()
		{
		}
		
		public function toXMLParams(registerInLog : int=0):XML{
			_xmlParams = XmlParamUtil.createXmlParamObject(registerInLog);
			
			XmlParamUtil.addParam(_xmlParams,"IDSOUS_TETE","l'identifiant de la ligne",IDSOUS_TETE);
			XmlParamUtil.addParam(_xmlParams,"IDLL","",IDLL);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGA_TETE","",LS_LEGA_TETE);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGA_ELEM_ACTIF","",LS_LEGA_ELEM_ACTIF);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGA_NOMSITE","",LS_LEGA_NOMSITE);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGA_ADRESSE1","",LS_LEGA_ADRESSE1);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGA_ADRESSE2","",LS_LEGA_ADRESSE2);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGA_ZIPCODE","",LS_LEGA_ZIPCODE);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGA_COMMUNE","",LS_LEGA_COMMUNE);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGA_ELEM_ACTIF_AMORCE_E","",LS_LEGA_ELEM_ACTIF_AMORCE_E);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGA_ELEM_ACTIF_AMORCE_R","",LS_LEGA_ELEM_ACTIF_AMORCE_R);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGA_TETE_AMORCE_E","",LS_LEGA_TETE_AMORCE_E);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGA_TETE_AMORCE_R","",LS_LEGA_TETE_AMORCE_R);
			XmlParamUtil.addParam(_xmlParams,"LS_NATURE","",LS_NATURE);
			XmlParamUtil.addParam(_xmlParams,"LS_DEBIT2","",LS_DEBIT2);
			XmlParamUtil.addParam(_xmlParams,"LS_DISTANCE2","",LS_DISTANCE2);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGB_TETE","",LS_LEGB_TETE);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGB_ELEM_ACTIF","",LS_LEGB_ELEM_ACTIF);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGB_NOMSITE","",LS_LEGB_NOMSITE);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGB_ADRESSE1","",LS_LEGB_ADRESSE1);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGB_ADRESSE2","",LS_LEGB_ADRESSE2);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGB_ZIPCODE","",LS_LEGB_ZIPCODE);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGB_COMMUNE","",LS_LEGB_COMMUNE);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGB_ELEM_ACTIF_AMORCE_E","",LS_LEGB_ELEM_ACTIF_AMORCE_E);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGB_ELEM_ACTIF_AMORCE_R","",LS_LEGB_ELEM_ACTIF_AMORCE_R);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGB_TETE_AMORCE_E","",LS_LEGB_TETE_AMORCE_E);
			XmlParamUtil.addParam(_xmlParams,"LS_LEGB_TETE_AMORCE_R","",LS_LEGB_TETE_AMORCE_R);
			XmlParamUtil.addParam(_xmlParams,"V7","Valeurs du champ perso 7",V7);
			XmlParamUtil.addParam(_xmlParams,"V8","Valeurs du champ perso 8",V8);
			return _xmlParams;
		}
	}
}