package univers.inventaire.lignesetservices.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	import mx.collections.ArrayCollection;
	
	/* [RemoteClass(alias="fr.consotel.consoview.inventaire.equipement.lignes.OngletGeneralVo")] */

	[Bindable]
	public class OngletGeneralVo implements ItoXMLParams
	{
		protected var _xmlParams : XML;
		
		public var IDSOUS_TETE:Number=0;
		
		//infos updatables
		public var USAGE:String = "";		
		public var IDTYPE_LIGNE:Number = 0;
		public var IDTYPE_RACCORDEMENT:Number = 0;
		public var CODE_INTERNE:String = "";
		public var IDENTIFIANT_COMPLEMENTAIRE:String = "";
		public var BOOL_PRINCIPAL:Number = 1;
		public var BOOL_BACKUP:Number = 0;
		public var POPULATION:String = "";
		public var COMMENTAIRES_GENERAL:String = "";
		public var V1:String = "";
		public var V2:String = "";		
						
		public var LIBELLE_TYPE_LIGNE:String = "";		
		public var SOUS_TETE:String = "";
		public var IDTETE_LIGNE:Number = 0;
		public var TETE_LIGNE:String = "";
		
		public var LIBELLE_RACCORDEMENT:String = "DIRECT";
		public var NBR_LIGNE:Number = 0;
		
		public var LIGNES_GROUPEMENT:ArrayCollection;
		public var LIGNES_GROUPEMENT_TOUPDATE:ArrayCollection;
		

		public function OngletGeneralVo()
		{
		}
		
		
		public function toXMLParams(registerInLog : int=0):XML{
			_xmlParams = XmlParamUtil.createXmlParamObject();
			
			XmlParamUtil.addParam(_xmlParams,"IDSOUS_TETE","l'id de la sous_tete",IDSOUS_TETE);
						
			XmlParamUtil.addParam(_xmlParams,"USAGE","l'usage de la ligne",USAGE);			
			XmlParamUtil.addParam(_xmlParams,"IDTYPE_LIGNE","l'id du type de ligne",IDTYPE_LIGNE);
			XmlParamUtil.addParam(_xmlParams,"IDTETE_LIGNE","l'id de la tete de ligne",IDTETE_LIGNE);
			XmlParamUtil.addParam(_xmlParams,"IDTYPE_RACCORDEMENT","l'identifiant du type de raccordement",IDTYPE_RACCORDEMENT);
			XmlParamUtil.addParam(_xmlParams,"CODE_INTERNE","le code interne",CODE_INTERNE);					
			XmlParamUtil.addParam(_xmlParams,"IDENTIFIANT_COMPLEMENTAIRE","l'identifiant interne",IDENTIFIANT_COMPLEMENTAIRE);
			XmlParamUtil.addParam(_xmlParams,"BOOL_PRINCIPAL","Booléen déterminant si on a affaire à une ligne principale",BOOL_PRINCIPAL);			
			XmlParamUtil.addParam(_xmlParams,"BOOL_BACKUP","Booléen déterminant si on a affaire à une ligne de back-up",BOOL_BACKUP);			
			XmlParamUtil.addParam(_xmlParams,"POPULATION","le type de population",POPULATION);			
			XmlParamUtil.addParam(_xmlParams,"COMMENTAIRES_GENERAL","Commentaires",COMMENTAIRES_GENERAL);
			XmlParamUtil.addParam(_xmlParams,"V1","Valeurs du champ perso 1",V1);
			XmlParamUtil.addParam(_xmlParams,"V2","Valeurs du champ perso 2",V2);
			XmlParamUtil.addParam(_xmlParams,"LIGNES_GROUPEMENT_TOUPDATE","liste des lignes dont on met la tête à jour (idsousTete,idTete) idTete est nullable",LIGNES_GROUPEMENT_TOUPDATE);
									
			return _xmlParams;
		}

	}
}