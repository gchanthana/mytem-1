package univers.inventaire.lignesetservices.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	[Bindable]
	dynamic public class Ligne implements ItoXMLParams
	{	
		

		public var DATE_COMMANDE:Date = null;
		public var DATE_RESILIATION:Date = null;
		public var DATE_SUSPENSION:Date = null;
		public var DATE_MISE_EN_SERV:Date = null;
		
		public var ETAT:String = "-";
		public var IDSOUS_TETE:Number = 0;
		public var IDTETE_LIGNE:Number = 0;
		public var IDTYPE_LIGNE:Number = 0;
		public var IDTYPE_RACCORDEMENT:Number = 0;
		public var SOUS_TETE:String = "-";
		public var TETE_LIGNE:String = "";
		public var TYPE_RACCORDEMENT:String = "-";
		public var TYPE_FICHELIGNE:String = "-";
		public var LIBELLE_TYPE_LIGNE:String = "-";
		public var USAGE:String = "-";
		public var NOM:String = "-";
		public var PRENOM:String = "-";
		public var COLLABORATEURID:Number=0;	
		public var MAJ_AUTO_COLLABORATEUR:Number;
		public var COMPTE:String = "-";
		
		public var OPERATEUR_ACCES_CIBLE:String="-";
		public var OPERATEURID_ACCES_CIBLE:Number=0;
		//Facturation
		public var OPERATEUR_ACCES:String="-";
		public var OPERATEURID_ACCES:Number=0;
		
		
		protected var _xmlParams : XML;
			
		
				
		public function Ligne()
		{	
			super();
		}
		
		public function toXMLParams(registerInLog:int=0):XML
		{
			_xmlParams = XmlParamUtil.createXmlParamObject();
			
			XmlParamUtil.addParam(_xmlParams,"IDSOUS_TETE","",IDSOUS_TETE);			
			XmlParamUtil.addParam(_xmlParams,"DATE_COMMANDE","",DATE_COMMANDE);			
			XmlParamUtil.addParam(_xmlParams,"DATE_RESILIATION","",DATE_RESILIATION);
			XmlParamUtil.addParam(_xmlParams,"DATE_SUSPENSION","",DATE_SUSPENSION);
			XmlParamUtil.addParam(_xmlParams,"DATE_MISE_EN_SERV","",DATE_MISE_EN_SERV);
			
			XmlParamUtil.addParam(_xmlParams,"ETAT","",ETAT);			
			XmlParamUtil.addParam(_xmlParams,"IDTETE_LIGNE","",IDTETE_LIGNE);			
			XmlParamUtil.addParam(_xmlParams,"IDTYPE_LIGNE","",IDTYPE_LIGNE);
			XmlParamUtil.addParam(_xmlParams,"IDTYPE_RACCORDEMENT","",IDTYPE_RACCORDEMENT);
			XmlParamUtil.addParam(_xmlParams,"USAGE","",USAGE);
			
			 
			
			
			
			return _xmlParams;
		}
		
	}
}