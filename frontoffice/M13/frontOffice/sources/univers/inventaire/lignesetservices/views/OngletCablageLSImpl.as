package univers.inventaire.lignesetservices.views
{
	import composants.controls.TextInputLabeled;
	
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	
	import mx.containers.FormItem;
	import mx.controls.Button;
	import mx.controls.TextArea;
	
	import univers.inventaire.lignesetservices.vo.OngletCabalageVo;
	
	[Bindable]
	public class OngletCablageLSImpl extends BaseView implements IOngletCablage
	{
		public var IDLL : TextInputLabeled;
		public var LS_LEGA_TETE : TextInputLabeled;
		public var LS_LEGA_ELEM_ACTIF : TextInputLabeled;
		public var LS_LEGA_NOMSITE : TextInputLabeled;
		public var LS_LEGA_ADRESSE1 : TextInputLabeled;
		public var LS_LEGA_ADRESSE2 : TextInputLabeled;
		public var LS_LEGA_ZIPCODE : TextInputLabeled;
		public var LS_LEGA_COMMUNE : TextInputLabeled;
		public var LS_LEGA_ELEM_ACTIF_AMORCE_E : TextInputLabeled;
		public var LS_LEGA_ELEM_ACTIF_AMORCE_R : TextInputLabeled;
		public var LS_LEGA_TETE_AMORCE_E : TextInputLabeled;
		public var LS_LEGA_TETE_AMORCE_R : TextInputLabeled;
		public var LS_NATURE : TextInputLabeled;
		public var LS_DEBIT2 : TextInputLabeled;
		public var LS_DISTANCE2 : TextInputLabeled;
		public var LS_LEGB_TETE : TextInputLabeled;
		public var LS_LEGB_ELEM_ACTIF : TextInputLabeled;
		public var LS_LEGB_NOMSITE : TextInputLabeled;
		public var LS_LEGB_ADRESSE1 : TextInputLabeled;
		public var LS_LEGB_ADRESSE2 : TextInputLabeled;
		public var LS_LEGB_ZIPCODE : TextInputLabeled;
		public var LS_LEGB_COMMUNE : TextInputLabeled;
		public var LS_LEGB_ELEM_ACTIF_AMORCE_E : TextInputLabeled;
		public var LS_LEGB_ELEM_ACTIF_AMORCE_R : TextInputLabeled;
		public var LS_LEGB_TETE_AMORCE_E : TextInputLabeled;
		public var LS_LEGB_TETE_AMORCE_R : TextInputLabeled;
		
		public var COMMENTAIRE_CABLAGE : TextArea;
		
		public var libelle7 : FormItem;
		public var libelle8 : FormItem;		
		public var V7 : TextInputLabeled;
		public var V8 : TextInputLabeled;
		
		public var btUpdate : Button;
		
		public function OngletCablageLSImpl()
		{
			super();
		}
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
		}
		
		//======================== HANDLERS ==========================================
		protected function btUpdateClickHandler(me : MouseEvent):void{
			doMapping();
			gestionTechnique.gestionCablage.majInfosOnglet();
		}
		//=================== FIN HANDLERS ===========================================
		
		
		//======================== PROTECTED =========================================
		protected function doMapping():void{
			if(gestionTechnique != null){
				var item : ItoXMLParams = gestionTechnique.gestionCablage.ongletVo;
								
				OngletCabalageVo(item).IDLL  = IDLL.text;
				OngletCabalageVo(item).LS_LEGA_TETE = LS_LEGA_TETE.text;
				OngletCabalageVo(item).LS_LEGA_ELEM_ACTIF = LS_LEGA_ELEM_ACTIF.text;
				OngletCabalageVo(item).LS_LEGA_NOMSITE = LS_LEGA_NOMSITE.text;
				OngletCabalageVo(item).LS_LEGA_ADRESSE1 = LS_LEGA_ADRESSE1.text;
				OngletCabalageVo(item).LS_LEGA_ADRESSE2 = LS_LEGA_ADRESSE2.text;
				OngletCabalageVo(item).LS_LEGA_ZIPCODE = LS_LEGA_ZIPCODE.text;
				OngletCabalageVo(item).LS_LEGA_COMMUNE = LS_LEGA_COMMUNE.text;
				OngletCabalageVo(item).LS_LEGA_ELEM_ACTIF_AMORCE_E = LS_LEGA_ELEM_ACTIF_AMORCE_E.text;
				OngletCabalageVo(item).LS_LEGA_ELEM_ACTIF_AMORCE_R = LS_LEGA_ELEM_ACTIF_AMORCE_R.text;
				OngletCabalageVo(item).LS_LEGA_TETE_AMORCE_E = LS_LEGA_TETE_AMORCE_E.text;
				OngletCabalageVo(item).LS_LEGA_TETE_AMORCE_R = LS_LEGA_TETE_AMORCE_R.text;
				OngletCabalageVo(item).LS_NATURE = LS_NATURE.text;
				OngletCabalageVo(item).LS_DEBIT2 = LS_DEBIT2.text;
				OngletCabalageVo(item).LS_DISTANCE2 = LS_DISTANCE2.text;
				OngletCabalageVo(item).LS_LEGB_TETE = LS_LEGB_TETE.text;
				OngletCabalageVo(item).LS_LEGB_ELEM_ACTIF = LS_LEGB_ELEM_ACTIF.text;
				OngletCabalageVo(item).LS_LEGB_NOMSITE = LS_LEGB_NOMSITE.text;
				OngletCabalageVo(item).LS_LEGB_ADRESSE1 = LS_LEGB_ADRESSE1.text;
				OngletCabalageVo(item).LS_LEGB_ADRESSE2 = LS_LEGB_ADRESSE2.text;
				OngletCabalageVo(item).LS_LEGB_ZIPCODE = LS_LEGB_ZIPCODE.text;
				OngletCabalageVo(item).LS_LEGB_COMMUNE = LS_LEGB_COMMUNE.text;
				OngletCabalageVo(item).LS_LEGB_ELEM_ACTIF_AMORCE_E = LS_LEGB_ELEM_ACTIF_AMORCE_E.text;
				OngletCabalageVo(item).LS_LEGB_ELEM_ACTIF_AMORCE_R = LS_LEGB_ELEM_ACTIF_AMORCE_R.text;
				OngletCabalageVo(item).LS_LEGB_TETE_AMORCE_E = LS_LEGB_TETE_AMORCE_E.text;
				OngletCabalageVo(item).LS_LEGB_TETE_AMORCE_R = LS_LEGB_TETE_AMORCE_R.text;
				OngletCabalageVo(item).COMMENTAIRE_CABLAGE = COMMENTAIRE_CABLAGE.text;
						
				OngletCabalageVo(item).V7 = V7.text;
				OngletCabalageVo(item).V8 = V8.text; 	
			}
			
		}
		//==================== FIN PROTECTED =========================================
		
	}
}