package univers.inventaire.lignesetservices.views
{
	import mx.containers.VBox;
	import univers.inventaire.lignesetservices.applicatif.GestionTechniqueLignesApp;
	import mx.controls.Button;
	import composants.controls.TextInputLabeled;
	import mx.containers.FormItem;
	import flash.events.MouseEvent;

	[Bindable]
	public class OngletFluxApplicatifsImpl extends BaseView
	{
		
		
		public var btChampPerso1 : Button;
		public var btChampPerso2 : Button;
		public var btChampPerso3 : Button;
		public var btChampPerso4 : Button;
		public var btChampPerso5 : Button;
		public var btChampPerso6 : Button;
		public var btChampPerso7 : Button;
		public var btChampPerso8 : Button;
		public var btChampPerso9 : Button;
		public var btChampPerso10 : Button;
		
		public var V_10 : TextInputLabeled;
		public var V_9 : TextInputLabeled;
		public var V_8 : TextInputLabeled;
		public var V_7 : TextInputLabeled;
		public var V_6 : TextInputLabeled;
		public var V_5 : TextInputLabeled;
		public var V_4 : TextInputLabeled;
		public var V_3 : TextInputLabeled;
		public var V_2 : TextInputLabeled;
		public var V_1 : TextInputLabeled;

		public var c10 : FormItem;
		public var c9 : FormItem;
		public var c8 : FormItem;
		public var c7 : FormItem;
		public var c6 : FormItem;
		public var c5 : FormItem;
		public var c4 : FormItem;
		public var c3 : FormItem;
		public var c2 : FormItem;
		public var c1 : FormItem;
		
		public var btUpdate : Button;
			
		public function OngletFluxApplicatifsImpl()
		{
			//TODO: implement function
			super();
		}
		
		/* private var _gestion : GestionTechniqueLignesApp;
		public function get gestionTechnique():GestionTechniqueLignesApp
		{
			return _gestion;
		}		
		public function set gestionTechnique(gestion:GestionTechniqueLignesApp):void
		{
			_gestion = gestion;
		} */
		
		override protected function commitProperties():void{
			super.commitProperties();
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
		}
		
				
		//======================== HANDLERS ==========================================
		protected function btUpdateClickHandler(me : MouseEvent):void{
			
		}
		//======================== FIN HANDLERS ======================================
		
		
	}
}