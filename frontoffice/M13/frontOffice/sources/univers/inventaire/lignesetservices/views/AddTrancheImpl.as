package univers.inventaire.lignesetservices.views
{
	import composants.controls.TextInputLabeled;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.lignesetservices.vo.TrancheSDAVo;
	
	
	
	
	[Event(name='annulerClicked')]
	[Event(name='validerClicked')]
	
	
	
	
	[Bindable]
	public class AddTrancheImpl extends BaseView
	{
		public static const ANNULER_CLICKED:String = "annulerClicked";
		public static const VALIDER_CLICKED:String = "validerClicked";
		
		
		public var LIBELLE_TRANCHE : TextInputLabeled;
		public var NBRE_SDA : Label;
		public var NUM_DEPART : TextInputLabeled;
		public var NUM_FIN : TextInputLabeled;
		public var TYPE_TRANCHE : ComboBox;		
		
		public var btValider : Button;
		public var btAnnuler : Button;
		
		//--- POPERTIES -------------------------------------------------------------------
		private var _trancheSDA : TrancheSDAVo;
		public function get trancheSDA():TrancheSDAVo{
			return _trancheSDA;
		} 
		public function set trancheSDA(tranche : TrancheSDAVo):void{
			_trancheSDA = tranche;
		}
		//--
		
		//--- FIN POPERTIES ---------------------------------------------------------------
		
		
		
		//--- HANDLERS --------------------------------------------------------------------
		protected function creationCompleteHandler(fe : FlexEvent):void{
			PopUpManager.centerPopUp(this);
		}
		
		protected function btAnnulerClickHandler(me : MouseEvent):void{
			dispatchEvent(new Event(ANNULER_CLICKED));
			PopUpManager.removePopUp(this);
		}
		
		protected function btValiderClickHandler(me : MouseEvent):void{
			if (trancheSDA != null){				
				enregistrer();
			}	
		}
		//--- FIN HANDLERS --------------------------------------------------------------------
		
		
		//--- PUBLIC --------------------------------------------------------------------------
		
		//--- FIN PUBLIC ----------------------------------------------------------------------
		
		//--- PROTECTED --------------------------------------------------------------------------
		override protected function commitProperties():void{
			super.commitProperties();
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			btAnnuler.addEventListener(MouseEvent.CLICK,btAnnulerClickHandler);
			btValider.addEventListener(MouseEvent.CLICK,btValiderClickHandler);
		}
		
		override protected function enregistrer():void{
			
			var boolOk : Boolean = true;
			var message : String = "\n";
			
			if (LIBELLE_TRANCHE.text.length == 0){
				boolOk = false;
				message = message + "- Vous devez saisir un libellé pour cette tranche.\n";
			}
			
			var numDep : Number = parseInt(NUM_DEPART.text);
			var numFin : Number = parseInt(NUM_FIN.text);  
			if ( numDep > numFin ){
				boolOk = false;
				message = message + "- Le numéro de départ doit être avant le numéro de fin.\n";
			}
			
			if (!NUM_DEPART.text){
				boolOk = false;
				message = message + "- Vous devez saisir un numéro de départ.\n";
			}
			
			if (!NUM_DEPART.text){
				boolOk = false;
				message = message + "- Vous devez saisir un numéro de fin.\n";
			}
			
			if (boolOk){
				
				trancheSDA.NUM_DEPART = NUM_DEPART.text;
				trancheSDA.NUM_FIN = NUM_FIN.text;
				trancheSDA.TRANCHE_SDA = LIBELLE_TRANCHE.text;
				trancheSDA.TYPE_TRANCHE = TYPE_TRANCHE.selectedItem.label;
				
				dispatchEvent(new Event(VALIDER_CLICKED));
			}else{
				Alert.okLabel = "Fermer";
				Alert.show(message,"Erreur",Alert.OK);
			}
		}
		//--- FIN PROTECTED ----------------------------------------------------------------------
		
		
		//--- PRIVATE --------------------------------------------------------------------------
		
		//--- FIN PRIVATE ----------------------------------------------------------------------
		
		
	}//--FIN
}