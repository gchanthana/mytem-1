package univers.inventaire.lignesetservices.views.popups
{
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.lignesetservices.applicatif.GestionEtatRaccordement;
	import univers.inventaire.lignesetservices.vo.ModeRaccordementVo;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;

	[Bindable]
	public class SelectRaccoImpl extends DoActionRaccoImpl
	{
		public var dgInventaire : DataGrid;	
		public var cbModeRacco : ComboBox;
		public var cbOperateur : ComboBox;
		public var lblSpecifierOperateur : Label;
		
		
		
		
		override public function set app(appli:GestionEtatRaccordement):void{
			_app = appli;			
		}
		
		
		public function SelectRaccoImpl()
		{
			super();
		}
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			cbModeRacco.addEventListener(ListEvent.CHANGE,cbModeRaccoChangeHangler);
			cbOperateur.addEventListener(ListEvent.CHANGE,cbOperateurChangeHandle);
			dgInventaire.addEventListener(ListEvent.CHANGE,dgInventaireChangeHandler);
		}
		
		override protected function verifierDonnees():void{
			var boolOK : Boolean = true;
			var message : String = "\n";
			
			if(cbModeRacco.selectedIndex == -1){
				boolOK = false;
				message = message + "- Vous devez sélectionner un mode de raccordement.\n";
			}
			
			if(cbOperateur.visible && cbOperateur.selectedIndex == -1){
				boolOK = false;
				message = message + "- Vous devez sélectionner un opérateur indirect.\n";
			}else{
				operateurIndirect = new OperateurVO();
				operateurIndirect.id =  OperateurVO(cbOperateur.selectedItem).id;
				operateurIndirect.nom = OperateurVO(cbOperateur.selectedItem).nom;
			}
			
			if(dcDateRacco.selectedDate == null){
				boolOK = false;
				message = message + "- Vous devez sélectionner une date de raccordement.\n";
			}
			
			if(boolOK){
				
				modeRacco = new ModeRaccordementVo();
				modeRacco.BOOL_DIRECT = ModeRaccordementVo(cbModeRacco.selectedItem).BOOL_DIRECT;
				modeRacco.IDTYPE_RACCORDEMENT = ModeRaccordementVo(cbModeRacco.selectedItem).IDTYPE_RACCORDEMENT;
				modeRacco.LIBELLE_RACCORDEMENT = ModeRaccordementVo(cbModeRacco.selectedItem).LIBELLE_RACCORDEMENT;
				
				ressourcesSelectionees = null;
				for(var i : Number = 0; i < (dgInventaire.dataProvider as ArrayCollection).length; i ++){
					if ((dgInventaire.dataProvider[i].ENTRER == true) || (dgInventaire.dataProvider[i].SORTIR == true) ){
						ressourcesSelectionees.push(dgInventaire.dataProvider[i]);
					}
				}
				
				 
				
				dispatchEvent(new Event(VALIDER_CLICKED));
				PopUpManager.removePopUp(this);
			}else{
				Alert.okLabel = "Fermer";
				Alert.show(message,"Erreur",Alert.OK);			
			}
		}
		
		//======== FORMATTERS =========================================================================================
		protected function formateBoolean(item : Object, column : DataGridColumn):String{
			if (item != null) return (Number(item[column.dataField]) == 1)?"OUI":"NON";
			else return "-";
		}
		
		//======== FIN FORMATTERS =========================================================================================
		//======== HANDLERS =========================================================================================
		protected function creationCompleteHandler(fe : FlexEvent):void{
			if(_app.historiqueActions != null ){
				_app.getListeModeRaccordement();
				_app.getListeOperateurs();
				if(app.historiqueActions.length > 0){
				configurerDateSelector();
				}
			}
		}
		
		protected function cbModeRaccoChangeHangler(le : ListEvent):void{
			if (cbModeRacco.selectedIndex != -1){
				if(cbModeRacco.selectedItem.BOOL_DIRECT){
					lblSpecifierOperateur.text = "Spécifier l'opérateur d'acces cible";
				}else{
					lblSpecifierOperateur.text = "Spécifier l'opérateur indirect cible";
				}
				
				if (cbOperateur.selectedIndex != -1){
						operateurIndirect = cbOperateur.selectedItem as OperateurVO;	
				}
				modeRacco = cbModeRacco.selectedItem as ModeRaccordementVo;
			}
		}
		
		protected function cbOperateurChangeHandle(le : ListEvent):void{
			if (cbOperateur.selectedIndex != -1){
				operateurIndirect = cbOperateur.selectedItem as OperateurVO;	
			}
		}
		
		protected function dgInventaireChangeHandler(le : ListEvent):void{
			//ressourcesSelectionees.source = dgInventaire.selectedItems;
		}
		//======== FIN HANDLERS =====================================================================================
	
	}
}