package univers.inventaire.lignesetservices.views.popups
{
	import composants.util.ConsoviewAlert;
	
	import flash.errors.IllegalOperationError;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	
	import univers.inventaire.lignesetservices.applicatif.GestionEtatRaccordement;
	import univers.inventaire.lignesetservices.vo.ModeRaccordementVo;
	
	[Bindable]
	public class ResilierImpl extends DoActionRaccoImpl
	{
		public var dgInventaire : DataGrid;
		public var cbSelectAll : CheckBox;
		
		public function ResilierImpl()
		{
			super();
			modeRacco = new ModeRaccordementVo();
			modeRacco.BOOL_DIRECT = 1;
			modeRacco.IDTYPE_RACCORDEMENT = 11;
			modeRacco.LIBELLE_RACCORDEMENT = "Résilié";
		}
		
		override public function set app(appli:GestionEtatRaccordement):void{
			_app = appli;			
			_app.getListeModeRaccordement();
		}
		
		
		//======== FORMATTERS =========================================================================================
		protected function formateBoolean(item : Object, column : DataGridColumn):String{
			if (item != null) return (Number(item[column.dataField]) == 1)?"OUI":"NON";
			else return "-";
		}
		
		//======== FIN FORMATTERS =========================================================================================
		
		override protected function verifierDonnees():void{
			var boolOK : Boolean = true;
			var message : String = "\n";
			
			if(dcDateRacco.selectedDate == null){
				boolOK = false;
				message = message + "- Vous devez sélectionner une date de résiliation.\n";
			}
			
			if(modeRacco == null){
				throw IllegalOperationError("Le mode de raccordement n'est pas spécifié");
			}
			
			if(boolOK){
				
				ressourcesSelectionees = null;
				for(var i : Number = 0; i < (dgInventaire.dataProvider as ArrayCollection).length; i ++){
					if (dgInventaire.dataProvider[i].SORTIR == true){
						ressourcesSelectionees.push(dgInventaire.dataProvider[i]);
					}
				}
				
				ConsoviewAlert.afficherAlertConfirmation("Etes vous sur de voulouir résilier cette ligne?","Confirmation",confirmationHandler);
				
			}else{
				Alert.okLabel = "Fermer";
				Alert.show(message,"Erreur",Alert.OK);			
			}
		}
		
		override protected function commitProperties():void{
			super.commitProperties();
			cbSelectAll.addEventListener(MouseEvent.CLICK,cbSelectAllClickHandler);
		}
		
		protected function cbSelectAllClickHandler(me : MouseEvent):void{			
			if (app.ressourcesLigne != null){
				for (var i : Number = 0; i < app.ressourcesLigne.source.length; i++){
					app.ressourcesLigne.source[i].SORTIR = cbSelectAll.selected;
				}	
			}
		}
		
		private function confirmationHandler(ce : CloseEvent):void{
			if(ce.detail == Alert.OK){
				super.verifierDonnees();	
			}
		}
		
		
	}
}