package univers.inventaire.lignesetservices.views.popups
{
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.List;
	import mx.events.CloseEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.lignesetservices.applicatif.GestionEtatRaccordement;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	
	[Bindable]
	public class AjouterOperateurIndirectImpl extends PopUpImpl
	{
		
		
		
		private var NB_MAX_OPE : Number = GestionEtatRaccordement.MAX_OPERATEUR_INDIRECT;
		
		public var liOperateurs : List;
		public var liOperateursIndirects : List;		
		
		public var imgAddOperateur:Image;
		public var imgSuppOperateur:Image;
		
		public var txtFiltre : TextInputLabeled;
				
		public var lblInfo : Label;
		
		protected var _app:GestionEtatRaccordement;
		public function get app():GestionEtatRaccordement{
			return _app;
		} 
		public function set app(appli :GestionEtatRaccordement):void{
			_app = appli;
		} 
		
		
		public function get selectedOperateursIndirects():Array{
			return listeOperateursIndirects.source;
		}		
		
		
		public var listeOperateurs : ArrayCollection = new ArrayCollection();
		public var listeOperateursIndirects : ArrayCollection = new ArrayCollection(); 
		
		
		public function AjouterOperateurIndirectImpl()
		{
			super();
		}
		
		override protected function commitProperties():void{
			super.commitProperties();
			lblInfo.text = "Le nombre maximum d'opérateurs indirects est " + NB_MAX_OPE.toString();
			
			listeOperateursIndirects = ObjectUtil.copy(app.operateursIndirects) as ArrayCollection;
			
			getListeOperateurs();
						
			
			imgAddOperateur.addEventListener(MouseEvent.CLICK,imgAddOperateurClickHandler);
			imgSuppOperateur.addEventListener(MouseEvent.CLICK,imgSuppOperateurClickHandler);
			
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
		}
		
		override protected function verifierDonnees():void{
			var nbOpIdirect : Number = listeOperateursIndirects.length;
			
			var message : String = "\n";
			var boolOk : Boolean = true;
			if(nbOpIdirect > NB_MAX_OPE){
				boolOk = false;
				message = message + "-Vous avez dépassé le nombre maximum d'opérateurs indirects.\n";  	
			}
			
			if(boolOk ){
				
				if (listeOperateursIndirects.source.length == 0 && app.operateursIndirects.length > 0){
					ConsoviewAlert.afficherAlertConfirmation("Il n'y a plus d'opérateur indirect dans la liste.\nLa ligne sera automatiquement raccordée à l'opérateur historique.\nVoulez-vous continuer ?"
						,"Info.",confirmationHandler);	
				}else{
					super.verifierDonnees();	
				}
			}else{
				Alert.okLabel = "Fermer";
				Alert.show(message,"Erreur",Alert.OK);
			}
			
		} 
		
		//====== HANDLERS ======================================================================//
		protected function confirmationHandler(ce : CloseEvent):void{
			if(ce.detail == Alert.OK){
				super.verifierDonnees();
			}
		}
		
		protected function txtFiltreChangeHandler(ev : Event):void{
			if (listeOperateurs != null) listeOperateurs.refresh();
		}
		
		protected function imgAddOperateurClickHandler(me : MouseEvent):void{
			listeOperateurs.refresh();
			var nbOperateursTotal : Number = listeOperateursIndirects.length;
			if(liOperateurs.selectedItems.length > 0){
				if(nbOperateursTotal < NB_MAX_OPE){
					var len : Number = Number(ObjectUtil.copy(liOperateurs.selectedItems.length));
					for (var i:Number = 0; i< len; i++){
						listeOperateursIndirects.addItem(
							listeOperateurs.removeItemAt(
								listeOperateurs.getItemIndex(
									liOperateurs.selectedItems[0])));
					}			
				}	
			}	
		}
		
		protected function imgSuppOperateurClickHandler(me : MouseEvent):void{
			if(liOperateursIndirects.selectedItems.length > 0){
				var len : Number = Number(ObjectUtil.copy(liOperateursIndirects.selectedItems.length));
				for (var i:Number = 0; i< len; i++){
					listeOperateurs.addItem(listeOperateursIndirects.removeItemAt(listeOperateursIndirects.getItemIndex(liOperateursIndirects.selectedItems[0])));
				}
			}
		}
		
		//====== FIN HANDLERS ======================================================================//
		
		private function filtrerLalisteDesOperateurs(item : Object):Boolean{
			if(item.nom.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) return true else return false;
		}
		
		private  function getListeOperateurs():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getListeOperateurs",
																				getListeOperateursResultHandler);
			RemoteObjectUtil.callService(op);
		}
		private function getListeOperateursResultHandler(re : ResultEvent):void{
			if(re.result){
				
				var col : ArrayCollection = re.result as ArrayCollection;
				var len : Number = col.length;
				
				for (var i : Number = 0; i <  len ; i++){
					var ope : OperateurVO = new OperateurVO();
					if(!ConsoviewUtil.isIdInArray(col[i].OPERATEURID,"id",listeOperateursIndirects.source)){
						ope.id =  col[i].OPERATEURID;
						ope.nom = col[i].NOM;
						listeOperateurs.addItem(ope);
					}								
				}
				listeOperateurs.refresh();
				
				listeOperateurs.filterFunction = filtrerLalisteDesOperateurs;					
			}
		}			
	}
}