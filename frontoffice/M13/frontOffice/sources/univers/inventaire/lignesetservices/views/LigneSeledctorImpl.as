package univers.inventaire.lignesetservices.views
{
	import composants.controls.TextInputLabeled;
	import composants.util.CustomToolTipeInfo;
	import composants.util.GeoSearchTree;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.ToolTip;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	
	import univers.inventaire.lignesetservices.vo.Ligne;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.cible.InvSearchTree;
	
	
	
	
	//Une ligne à été selectionnée
	[Event("LigneSelected")]
	
	
	
	
	
	
	
	
	[Bindable]
	public class LigneSeledctorImpl extends BaseView
	{
		
		
		
		
		
		
		
		
	 	
		public var treeGeographique : GeoSearchTree;
		public var dgLignes : DataGrid;
		public var btRechercher : Image;		
		public var txtFiltre : TextInputLabeled;
		public var txtRechercher : TextInputLabeled;
		public var lblChaine : Label;
		public var lblPerimetre : Label;
		public var btFiche : Button;
		public var imgCSV : Image;
		
		
		
		
		
		
		
		
		private var _selectedLigne : Object;
		public function get selectedLigne():Object{
			return _selectedLigne;
		}
		public function set selectedLigne(obj : Object):void{
			_selectedLigne = obj;
		}
		
		
		//le tooltip de la recherche
	    private var myTip:ToolTip;
	    
	    //le message d'aide pour le tooltip
	    private var helpMessage : String = "La recherche scanne les champs suivant :\n- Numéro de ligne\n- Fonction/Collaborateur(Usage) \n- Tête de ligne.";
		
		
			
		/**
		 * Crée le message d'aide du composant
		 * */
		protected function createBigTip():void {			
 			myTip  = CustomToolTipeInfo.createBigTip(txtRechercher,helpMessage,CustomToolTipeInfo.BELOW);
		}
		
		
		/**
		 * formatte le datatip de la colonne collaborateur
		 **/
		 protected function opeAccesCibleDataTipFunction(item:Object):String{
		 	return (item.OPERATEURID_ACCES_CIBLE > 0)?item.OPERATEUR_ACCES_CIBLE:item.OPERATEUR_ACCES;
		 }
		 
		 /**
		 * formatte le datatip de la colonne collaborateur
		 **/
		 protected function opeAccesCibleLabelFunction(item : Object, column : DataGridColumn):String{
		 	return (item.OPERATEURID_ACCES_CIBLE > 0)?item.OPERATEUR_ACCES_CIBLE:item.OPERATEUR_ACCES;
		 }
		 
		
		/**
		 * Formate le nom du collaborateur dans le datagrid
		 * */
		protected function collaborateurLabelFunction(item : Object, column : DataGridColumn):String{
			var patro : String;
			
			if (item.NOM != null && item.NOM != "-"){
				patro = item.NOM;
			}
			
			if (patro != null && item.PRENOM != null && item.NOM != "-"){
				patro = patro + ' ' + item.PRENOM;	
			}else if (item.PRENOM != null && item.NOM != "-"){
				patro = item.PRENOM;
			}
			
			return (patro != null)?patro:"-";	
		}
		
		/**
		 * formatte le datatip de la colonne collaborateur
		 **/
		 protected function collaborateurDataTipFunction(item:Object):String{
		 	var patro : String;
			
			if (item.NOM != null && item.NOM != "-"){
				patro = item.NOM;
			}
			
			if (patro != null && item.PRENOM != null && item.NOM != "-"){
				patro = patro + ' ' + item.PRENOM;	
			}else if (item.PRENOM != null && item.NOM != "-"){
				patro = item.PRENOM;
			}
			
			return (patro != null)?patro:"-";	
		 }
		
		
		
		/**
		 * Détruit le message d'aide du composant
		 * */
		protected function destroyBigTip():void {
		 CustomToolTipeInfo.destroyBigTip(myTip);
		}
		
		
		
		
		
		
		
		
		
		public function LigneSeledctorImpl()
		{
			//TODO: implement function
			super();
		}
		
		
		
		
		
		
		override public function onPerimetreChange():void{
			dgLignes.selectedIndex = -1;
			txtRechercher.text = "";
			txtFiltre.text = "";
			lblPerimetre.text = "";
			lblChaine.text = "";
			treeGeographique.onPerimetreChange();
		}
		
		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			dgLignes.addEventListener(ListEvent.ITEM_DOUBLE_CLICK,dgLignesItemDoubleClickHandler);
			dgLignes.addEventListener(ListEvent.CHANGE,dgLignesChangeHandler);			
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			btRechercher.addEventListener(MouseEvent.CLICK,btRechercherClickHandler);
			txtRechercher.addEventListener(FlexEvent.ENTER,txtRechercherEnterHandler);
			treeGeographique.addEventListener(InvSearchTree.NODE_CHANGED,treeGeographiqueChangeHandler);
			selectedLigne = dgLignes.selectedItem;
		}
		
		
		
		
		//======================== HANDLERS ====================================================
		protected function imgCSVClickHandler(me : MouseEvent):void{
			if (gestionTechnique.listeLignes != null){
				gestionTechnique.exporterListeLignes();				
			}
		}
		//private var last
		protected function dgLignesChangeHandler(le : ListEvent):void{
			if(le.currentTarget.selectedItem != null){
				btFiche.enabled = true;
			}else{
				btFiche.enabled = false;
			}			
		}
		
		
		protected function dgLignesItemDoubleClickHandler(le : ListEvent):void{
			if (le.currentTarget.selectedItem != null){
				gestionTechnique.ligne = dgLignes.selectedItem as  Ligne;
				dispatchEvent(new Event("LigneSelected"));
				btFiche.enabled = false;
			}else{
				gestionTechnique.ligne = null;
				le.preventDefault();
			}
		}
		
		protected function txtFiltreChangeHandler(ev : Event):void{
			if (gestionTechnique.listeLignes != null){
				gestionTechnique.listeLignes.filterFunction = filtrerLeDataGird;
				gestionTechnique.listeLignes.refresh();
			}
		}
			
			
		protected function btRechercherClickHandler(me : MouseEvent):void{
		 	if (treeGeographique.nodeInfo != null){
		 		lblChaine.text = txtRechercher.text;
		 		lblPerimetre.text = treeGeographique.nodeInfo.LBL;
		 		gestionTechnique.listeLignes = null;		 		
				gestionTechnique.getListeLignes(treeGeographique.nodeInfo.NID,txtRechercher.text);	
			}else{
				gestionTechnique.getListeLignes(0,txtFiltre.text);	
			}
		}
		
		
		protected function txtRechercherEnterHandler(fe : FlexEvent):void{
			if (treeGeographique.nodeInfo != null){
		 		lblChaine.text = txtRechercher.text;
		 		lblPerimetre.text = treeGeographique.nodeInfo.LBL;
		 		gestionTechnique.listeLignes = null;		 		
				gestionTechnique.getListeLignes(treeGeographique.nodeInfo.NID,txtRechercher.text);	
			}else{
				gestionTechnique.getListeLignes(0,txtFiltre.text);	
			} 
		}
				
		protected function treeGeographiqueChangeHandler(ev : Event):void{
			//lblPerimetre.text = treeGeographique.nodeInfo.LBL;
		}
		
		protected function btFicheClickHandler(me : MouseEvent):void{
			if (dgLignes.selectedItem != null){
				gestionTechnique.ligne = dgLignes.selectedItem as  Ligne;
				btFiche.enabled = false;
				dispatchEvent(new Event("LigneSelected"));
			}else{
				gestionTechnique.ligne = null;
				me.preventDefault();
			}
		}
		//==================== FIN HANDLERS ====================================================
		
		private function filtrerLeDataGird(item : Object):Boolean{
			if ((item.SOUS_TETE.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				(item.USAGE.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				|| 
				(item.TETE_LIGNE.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				(item.LIBELLE_TYPE_LIGNE.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				(item.TYPE_RACCORDEMENT.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				(String(item.NOM+' '+item.PRENOM).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				((item.OPERATEUR_ACCES_CIBLE != null) && item.OPERATEUR_ACCES_CIBLE.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				((item.OPERATEUR_ACCES != null)  && item.OPERATEUR_ACCES.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				){
				return true
			}else{
				return false;
			}
		}
	}
}