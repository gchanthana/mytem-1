package univers.inventaire.lignesetservices.views.popups
{
	import univers.inventaire.lignesetservices.applicatif.GestionEtatRaccordement;
	
	[Bindable]
	public class ReactiverImpl extends DoActionRaccoImpl
	{
		public function ReactiverImpl()
		{	
			super();
			
		}
		
		override public function set app(appli:GestionEtatRaccordement):void{
			_app = appli;
			_app.getModeRaccordementPrecedent();
		}  
		
		override protected function verifierDonnees():void{			
			modeRacco = app.selectedModeRaccordement; 
			super.verifierDonnees();
		}
		
	}
}