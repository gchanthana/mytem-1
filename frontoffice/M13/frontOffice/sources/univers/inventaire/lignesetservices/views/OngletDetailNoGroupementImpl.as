package univers.inventaire.lignesetservices.views
{
	import appli.events.EventData;
	
	import composants.util.CustomToolTipeInfo;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.ToolTip;
	
	import univers.inventaire.lignesetservices.applicatif.GestionOngletGenerale;
	
	public class OngletDetailNoGroupementImpl extends OngletDetailImpl
	{
		public function OngletDetailNoGroupementImpl()
		{
			super();
		}
		
		override public function getData(typeLigne : String = "Default"):void{						
			 
			gestionTechnique.gestionOngletGenerale.getInfosLigne();
			gestionTechnique.gestionAffectationStrategy.getInfosAffectation();
			gestionTechnique.gestionOngletGenerale.addEventListener(GestionOngletGenerale.USAGE_READY,putUsage)
		}
		
		override protected function txtFiltreLigneChangeHandler(ev : Event):void{
			//Muted
		}
		
		override protected function imgSetTeteClickHandler (me : MouseEvent):void{
			//Muted
		}
		
		
		override protected function imgCleanTeteClickHandler (me : MouseEvent):void{
			//muted			
		}
		
		
		override protected function imgAddLigneGptClickHandler (me : MouseEvent):void{			
			//Muted 
		}		
		
		override protected function imgRemoveLigneGptClickHandler (me : MouseEvent):void{
			//Muted 
		}
		
		override protected function listeTeteLigneCompleteHandler(ev : Event):void{
			//Muted
		}
		//======================== FIN HANDLERS ======================================		
		
		
		override protected function commitProperties():void{
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
		}
		
		//le tooltip de la recherche
	    private var myTip:ToolTip;
	    private var helpMessage:String 					= "Les caratères spéciaux ne sont pas acceptés";
	    [Bindable]
//	    public var restrictStringNoGroupement:String 	= "A-Z,a-z,0-9,/,_";
	    public var restrictStringNoGroupement:String 	= "a-zA-Z0-9\\-\\._";

		
		/**
		 * Détruit le message d'aide du composant
		 * */
		override public function destroyBigTip():void 
		{
			CustomToolTipeInfo.destroyBigTip(myTip);
		}
		
		/**
		 * Crée le message d'aide du composant
		 * */
/* 		override public function createBigTip():void 
		{			
 			myTip  = CustomToolTipeInfo.createBigTip(USAGE_LIGNE,helpMessage,CustomToolTipeInfo.BELOW);
		} */
		
		private function putUsage(evt:EventData):void
		{
			cb_usage.dataProvider = evt.objData.theDataProvider;
			cb_usage.selectedIndex = evt.objData.index;
		}
	}
}