package univers.inventaire.lignesetservices.views
{
	import appli.events.EventData;
	
	import composants.controls.TextInputLabeled;
	import composants.usage.PopUpUsageIHM;
	import composants.usage.PopUpUsageImpl;
	import composants.util.CustomToolTipeInfo;
	
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Form;
	import mx.containers.FormItem;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.List;
	import mx.controls.RadioButton;
	import mx.controls.TextArea;
	import mx.controls.ToolTip;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.lignesetservices.applicatif.GestionOngletGenerale;
	import univers.inventaire.lignesetservices.util.ShowEditWindow;
	import univers.inventaire.lignesetservices.vo.Ligne;
	import univers.inventaire.lignesetservices.vo.OngletGeneralVo;

	[Bindable]
	public class OngletDetailImpl extends BaseView
	{
		public var LIBELLE_TYPE_LIGNE : ComboBox;
		public var SOUS_TETE : TextInputLabeled;
		
		
		//Groupement de lignes
		public var TETE_GROUPEMENT : DataGrid;
		public var TETES_GROUPEMENT_LIST : List;
		public var TETE_GROUPEMENT_TXT : TextInputLabeled;		
		public var imgSetTete : Image;		
		public var imgCleanTete : Image;		
		public var imgAddLigneGpt : Image;		
		public var imgRemoveLigneGpt : Image;
		public var bxAutreLignes : HBox;
		
		
		public var NBR_LIGNE : Label;
		public var IDENTIFIANT_COMPLEMENTAIRE : TextInputLabeled;
		public var CODE_INTERNE : TextInputLabeled;
		public var LIBELLE_RACCORDEMENT : Label;
		
		
		public var BOOL_PRINCIPAL : RadioButton;
		public var BOOL_BACKUP : RadioButton;
		
		public var COMMENTAIRES_GENERAL : TextArea;
		//public var USAGE_LIGNE : AutoComplete;
		//public var USAGES_LIGNES : ComboBox;
		public var cb_usage : ComboBox;
		
		public var POPULATION : TextInputLabeled;	
		public var lblUsage : FormItem;
		  
		public var leftForm : Form;
		
		
		public var txtFiltreLigne : TextInputLabeled;
		
		public var libelle1 : FormItem;
		public var libelle2 : FormItem;	
		
		public var V1 : TextInputLabeled;
		public var V2 : TextInputLabeled;
		
		public var btChampPerso1 : Button;
		public var btChampPerso2 : Button;
		public var btUpdate : Button;
		//Test
		public var btReset : Button;
		
		protected var donnees : OngletGeneralVo;
				
		
		public function OngletDetailImpl()
		{	
			super();
		}
		
		override public function getData(typeLigne : String = "Default"):void{						
			super.getData();
			txtFiltreLigne.text = "";
			gestionTechnique.gestionOngletGenerale.addEventListener(GestionOngletGenerale.USAGE_READY,putUsage)
			
		}
		
	
				
		//======================== HANDLERS ==========================================		
		 
		
/* 		protected function USAGE_LIGNEChangeHandler(event : Event):void{
			if (USAGE_LIGNE.text.length > 149){
				
				var textCache : String = USAGE_LIGNE.text.substr(0,150);
				USAGE_LIGNE.text = "";
				USAGE_LIGNE.text = textCache;
				
				trace("Limite attteind");
			}
		} */
		
		protected function btUpdateClickHandler(me : MouseEvent):void{
			enregistrer();
		}
		
		protected function clickHandlerNewUsage():void
		{
			var popupUsage:PopUpUsageIHM=new PopUpUsageIHM();
			popupUsage.addEventListener(PopUpUsageImpl.POPUP_USAGE_VALIDED, addUsage);
			PopUpManager.addPopUp(popupUsage, this, true);
			PopUpManager.centerPopUp(popupUsage);
		}
		
		private function addUsage(evt:EventData):void
		{
			(cb_usage.dataProvider as ArrayCollection).addItem(evt.objData);
			cb_usage.selectedIndex=(cb_usage.dataProvider as ArrayCollection).length - 1;
		}		
		
		protected function LIBELLE_TYPE_LIGNEUpdateHandler(fe : Event):void{
			if (LIBELLE_TYPE_LIGNE.initialized)
				if (fe.currentTarget.selectedIndex != -1){
					lblUsage.label = "Fonction";					
				}
		}
		
		protected function LIBELLE_TYPE_LIGNEChangeHandler(le : ListEvent):void{
			_hasChanged = true
			if (le.currentTarget.selectedIndex != -1){				
				lblUsage.label = "Fonction";
			}else{
				le.preventDefault();
			}
		}
		
		protected function LIBELLE_RACCORDEMENTChangeHandler(le : ListEvent):void{
			if (le.currentTarget.selectedIndex != -1){				
			}else{
				le.preventDefault();
			}			
		}
		
		protected function btChampPerso1ClickHandler(me : MouseEvent):void{						
			var showWindow : ShowEditWindow = new ShowEditWindow(gestionTechnique,libelle1);
			showWindow.showEditWindow();
		}
		
		
		protected function btChampPerso2ClickHandler(me : MouseEvent):void{
			var showWindow : ShowEditWindow = new ShowEditWindow(gestionTechnique,libelle2);
			showWindow.showEditWindow();
		}
		
		protected function btResetClickHandler(me : MouseEvent):void{			
			executeBindings(true);
		}
		
		
		protected function txtFiltreLigneChangeHandler(ev : Event):void{
			if(gestionTechnique.gestionOngletGenerale.listeLigneCr != null){
				gestionTechnique.gestionOngletGenerale.listeLigneCr.filterFunction = filtrerListeTetes;
				gestionTechnique.gestionOngletGenerale.listeLigneCr.refresh();
			}
		}
		
		protected function imgSetTeteClickHandler (me : MouseEvent):void{
			if (TETE_GROUPEMENT.selectedItems.length > 1){
				Alert.show("Vous ne devez sélectionner qu'une seule ligne","Info");
			}else if (TETE_GROUPEMENT.selectedItems.length == 1){
				var teteligne : Ligne = new Ligne();
				teteligne.IDSOUS_TETE =  TETE_GROUPEMENT.selectedItem.IDTETE_LIGNE;
				teteligne.SOUS_TETE = TETE_GROUPEMENT.selectedItem.TETE;	
				teteligne.ISTETE = 	TETE_GROUPEMENT.selectedItem.ISTETE;					
				gestionTechnique.gestionOngletGenerale.setTeteLigne(gestionTechnique.ligne,teteligne);
			}else{
				Alert.show("Vous devez sélectionner des lignes","Info");
			}
		}
		
		
		protected function imgCleanTeteClickHandler (me : MouseEvent):void
		{
			if (gestionTechnique.ligne.IDTETE_LIGNE>0)
			{
				gestionTechnique.gestionOngletGenerale.setTeteLigne(gestionTechnique.ligne,new Ligne());
			}else{
				trace("pas de tete");
			}
			
		}
		
		
		protected function imgAddLigneGptClickHandler (me : MouseEvent):void{			
			if (gestionTechnique.ligne.IDTETE_LIGNE){
				if(TETE_GROUPEMENT.selectedIndex != -1){					
				
					var tabSousTete : Array = new Array();
					for (var i:Number = 0; i < TETE_GROUPEMENT.selectedItems.length; i++){					
						var ligne : Ligne = new Ligne();
						ligne.IDSOUS_TETE =  TETE_GROUPEMENT.selectedItems[i].IDTETE_LIGNE;
						ligne.SOUS_TETE = TETE_GROUPEMENT.selectedItems[i].TETE;
						
						if (ligne.SOUS_TETE != TETE_GROUPEMENT_TXT.text){
							tabSousTete.push(ligne);
						}
					}
					
											
					var tete : Ligne = new Ligne();
					tete.IDSOUS_TETE =  gestionTechnique.ligne.IDTETE_LIGNE;
					tete.SOUS_TETE = gestionTechnique.ligne.TETE_LIGNE;
							
					gestionTechnique.gestionOngletGenerale.setTeteLignePourXLignes(tabSousTete,tete);
					
					/* if (ligne.SOUS_TETE != TETE_GROUPEMENT_TXT.text){
						
						var tete : Ligne = new Ligne();
						tete.IDSOUS_TETE =  gestionTechnique.ligne.IDTETE_LIGNE;
						tete.SOUS_TETE = gestionTechnique.ligne.TETE_LIGNE;
						
						gestionTechnique.gestionOngletGenerale.setTeteLigne(ligne,tete);
						
					}else{
						Alert.okLabel = "Fermer";
						Alert.show("Vous avez sélectionné la tête de ligne","Info",Alert.OK);
					} */
					
					
				}else{
					Alert.okLabel = "Fermer";
					Alert.show("Vous devez sélectionner une ligne","Info",Alert.OK);
				}
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Vous devez indiquer la tête de groupement avant d'ajouter des lignes au groupe","Info",Alert.OK);
			} 
		}		
		
		protected function imgRemoveLigneGptClickHandler (me : MouseEvent):void{
			if(TETES_GROUPEMENT_LIST.selectedItem !=null ){
				gestionTechnique.gestionOngletGenerale.setTeteLignePourXLignes(TETES_GROUPEMENT_LIST.selectedItems,new Ligne());
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Vous devez sélectionner des lignes","Info",Alert.OK);
			} 
		}
		
		protected function listeTeteLigneCompleteHandler(ev : Event):void{
			txtFiltreLigne.text = "";	
			TETE_GROUPEMENT.setFocus();
		}
		//======================== FIN HANDLERS ======================================		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
			imgSetTete.addEventListener(MouseEvent.CLICK,imgSetTeteClickHandler);
			imgCleanTete.addEventListener(MouseEvent.CLICK,imgCleanTeteClickHandler);
			imgAddLigneGpt.addEventListener(MouseEvent.CLICK,imgAddLigneGptClickHandler);
			imgRemoveLigneGpt.addEventListener(MouseEvent.CLICK,imgRemoveLigneGptClickHandler);
			gestionTechnique.gestionOngletGenerale.addEventListener(GestionOngletGenerale.LISTETETESLIGNE_COMPLETE,listeTeteLigneCompleteHandler);
			//USAGE_LIGNE.addEventListener(Event.CHANGE,USAGE_LIGNEChangeHandler);
		}
	
		
		//Enregistre le formulaire
		override protected function enregistrer():void{
			var boolOk : Boolean = true;
			if(LIBELLE_TYPE_LIGNE.selectedItem == null){
				boolOk = false
				LIBELLE_TYPE_LIGNE.setStyle("borderColor","red");				
			}			
			/* if(LIBELLE_RACCORDEMENT.selectedItem == null){
				boolOk = false
				LIBELLE_RACCORDEMENT.setStyle("borderColor","red");				
			} */
			
			if (boolOk){ 
				mapData(donnees = new OngletGeneralVo());
				gestionTechnique.gestionOngletGenerale.updateInfosLigne(donnees);
				LIBELLE_RACCORDEMENT.clearStyle("borderColor");
				LIBELLE_TYPE_LIGNE.clearStyle("borderColor");
				_hasChanged = false;
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Les champs en rouge sont obligatoires","Erreur",Alert.OK);
			}
		}
		
		private function mapData(item : OngletGeneralVo):void{
			if (gestionTechnique != null){		
				item.IDSOUS_TETE = gestionTechnique.ligne.IDSOUS_TETE;						
				item.LIBELLE_TYPE_LIGNE = LIBELLE_TYPE_LIGNE.selectedItem.LIBELLE_TYPE_LIGNE;
				item.IDTYPE_LIGNE = LIBELLE_TYPE_LIGNE.selectedItem.IDTYPE_LIGNE;
				item.IDTETE_LIGNE = gestionTechnique.ligne.IDTETE_LIGNE;
				item.TETE_LIGNE = gestionTechnique.ligne.TETE_LIGNE;
				/* item.LIBELLE_RACCORDEMENT = (LIBELLE_RACCORDEMENT.selectedItem != null)?LIBELLE_RACCORDEMENT.selectedItem.LIBELLE_RACCORDEMENT:"";
				item.IDTYPE_RACCORDEMENT = (LIBELLE_RACCORDEMENT.selectedItem != null)?LIBELLE_RACCORDEMENT.selectedItem.IDTYPE_RACCORDEMENT:0; */	
				
				item.IDTYPE_RACCORDEMENT = gestionTechnique.ligne.IDTYPE_RACCORDEMENT;
				item.LIBELLE_RACCORDEMENT = gestionTechnique.ligne.TYPE_RACCORDEMENT;
						
				item.IDENTIFIANT_COMPLEMENTAIRE = IDENTIFIANT_COMPLEMENTAIRE.text;
				item.CODE_INTERNE = CODE_INTERNE.text;
				item.BOOL_PRINCIPAL = (BOOL_PRINCIPAL.selected)?1:0;
			 
				item.COMMENTAIRES_GENERAL = COMMENTAIRES_GENERAL.text;
				if(cb_usage.selectedItem)
				{
					item.USAGE = cb_usage.selectedItem.toString();
				}
				else
				{
					item.USAGE = "-";
				}
				item.POPULATION = POPULATION.text;
				item.V1 = V1.text;
				item.V2 = V2.text;	
				
				
				
			}else{
				throw (new IllegalOperationError("Pas de controlleur pour l'application"))	
			}			
		}
		
		private function filtrerListeTetes(item :Object):Boolean{
			if (String(item.TETE).toLowerCase().search(txtFiltreLigne.text) != -1) return true
			else return false;
		}
		
		//le tooltip de la recherche
	    private var myTip:ToolTip;
	    private var helpMessage:String 			= "Les caratères spéciaux ne sont pas acceptés";
//	    public var restrictStringDetail:String 	= "A-Z,a-z,0-9,/,_";
	    public var restrictStringDetail:String 	= "a-zA-Z0-9\\-\\._";

		
		/**
		 * Détruit le message d'aide du composant
		 * */
		public function destroyBigTip():void 
		{
			CustomToolTipeInfo.destroyBigTip(myTip);
		}
		
		/**
		 * Crée le message d'aide du composant
		 * */
	/* 	public function createBigTip():void 
		{			
 			myTip  = CustomToolTipeInfo.createBigTip(USAGE_LIGNE,helpMessage,CustomToolTipeInfo.BELOW);
		} */

		private function putUsage(evt:EventData):void
		{
			cb_usage.dataProvider = evt.objData.theDataProvider;
			cb_usage.selectedIndex = evt.objData.index;
		}
		
	}
}
                                                                                                                                                                                                         
