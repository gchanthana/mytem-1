package univers.inventaire.lignesetservices.views
{
	import composants.controls.TextInputLabeled;
	
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	
	import univers.inventaire.lignesetservices.vo.OngletT2Vo;
	
	[Bindable]
	public class OngletCablageT2Impl extends OngletCablageT0Impl implements IOngletCablage
	{
		
		
		//==================================================================================================
		
		public var CANAUX_MIXTE_CAN_DEB:TextInputLabeled;
		public var CANAUX_MIXTE_CAN_FIN:TextInputLabeled;
		public var CANAUX_ENTREE_CAN_DEB:TextInputLabeled;
		public var CANAUX_ENTREE_CAN_FIN:TextInputLabeled;
		public var CANAUX_SORTIE_CAN_DEB:TextInputLabeled;
		public var CANAUX_SORTIE_CAN_FIN:TextInputLabeled;
								
		
		
		
		//================================================================================
				
		
		public function OngletCablageT2Impl()
		{
			super();
		}
		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
		}
		
		override protected function doMapping():void{
			super.doMapping();
			if(gestionTechnique != null){
				var item : ItoXMLParams = gestionTechnique.gestionCablage.ongletVo;
				OngletT2Vo(item).CANAUX_MIXTE_CAN_DEB = CANAUX_MIXTE_CAN_DEB.text;
				OngletT2Vo(item).CANAUX_MIXTE_CAN_FIN = CANAUX_MIXTE_CAN_FIN.text;
				OngletT2Vo(item).CANAUX_ENTREE_CAN_DEB = CANAUX_ENTREE_CAN_DEB.text;
				OngletT2Vo(item).CANAUX_ENTREE_CAN_FIN = CANAUX_ENTREE_CAN_FIN.text;
				OngletT2Vo(item).CANAUX_SORTIE_CAN_DEB = CANAUX_SORTIE_CAN_DEB.text;
				OngletT2Vo(item).CANAUX_SORTIE_CAN_FIN = CANAUX_SORTIE_CAN_FIN.text;
			}
		}	
		
	}
}