package univers.inventaire.lignesetservices.views
{
 	
	import composants.controls.TextInputLabeled;
	
	import flash.events.MouseEvent;
	
	import mx.containers.FormItem;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DateField;
	import mx.controls.TextArea;
	
	import univers.inventaire.lignesetservices.applicatif.GestionTechniqueLignesApp;
	import univers.inventaire.lignesetservices.vo.OngletEtatVo;
	import univers.inventaire.lignesetservices.util.ShowEditWindow;
	import mx.events.ListEvent;
	import flash.events.Event;
	import composants.util.CvDateChooser;
	 
	
	
	[Bindable]
	public class OngletEtatImpl extends BaseView
	{
		
		public var ETAT: TextInputLabeled;
		public var DATE_COMMANDE:CvDateChooser;
		public var REF_COMMANDE:TextInputLabeled;
		public var TITULAIRE:ComboBox;
		public var PAYEUR:ComboBox;
		public var COMMENTAIRE_ETAT:TextArea;
		public var DATE_MISE_EN_SERV:CvDateChooser;
		public var REF_MISE_EN_SERV:TextInputLabeled;
		public var GEST_MISE_EN_SERV:TextInputLabeled;
		public var DATE_PRESELECTION:CvDateChooser;
		public var REF_PRESELECTION:TextInputLabeled;
		public var GEST_PRESELECTION:TextInputLabeled;
		public var DATE_SUSPENSION:CvDateChooser;
		public var REF_SUSPENSION:TextInputLabeled;
		public var GEST_SUSPENSION:TextInputLabeled;
		public var DATE_RESILIATION:CvDateChooser;
		public var REF_RESILIATION:TextInputLabeled;
		public var GEST_RESILIATION:TextInputLabeled;
		public var btUpdate : Button;
		public var libelle3 : FormItem;
		public var libelle4 : FormItem;	
		public var V3 : TextInputLabeled;
		public var V4 : TextInputLabeled;
		public var btChampPerso1 : Button;
		public var btChampPerso2 : Button;
				
		
		
		public function OngletEtatImpl()
		{
			//TODO: implement function
			super();
		}
		
			
		protected var donnees : OngletEtatVo;
				
		//======================== HANDLERS ==========================================
		protected function TITULAIRECHangeHandler(le : ListEvent):void{
			if (TITULAIRE.selectedIndex == -1){
				TITULAIRE.selectedIndex = 1;
			}
		}
		
		protected function PAYEURCHangeHandler(le : ListEvent):void{
			if (PAYEUR.selectedIndex == -1){
				PAYEUR.selectedIndex = 1;
			}
		}
		
		protected function ETATChangeHandler(ev : Event):void{
			ETAT.clearStyle("borderColor");
		}
		
		protected function btUpdateClickHandler(me : MouseEvent):void{
			enregistrer();
		}
		
		protected function btChampPerso1ClickHandler(me : MouseEvent):void{						
			var showWindoW : ShowEditWindow = new ShowEditWindow(gestionTechnique,libelle3);
			showWindoW.showEditWindow();
		}
		
		
		protected function btChampPerso2ClickHandler(me : MouseEvent):void{
			var showWindow : ShowEditWindow = new ShowEditWindow(gestionTechnique,libelle4);
			showWindow.showEditWindow();
		}
		//======================== FIN HANDLERS ======================================		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
		}
		
		
		override protected function enregistrer():void{
			var boolOk : Boolean = true;
			if(ETAT.text.length <1){
				boolOk = false
				ETAT.setStyle("borderColor","red");		
			}
			if(boolOk){
				mapData(donnees = new OngletEtatVo());
//				gestionTechnique.updateEtatLigne(donnees);
				_hasChanged = false;
				ETAT.clearStyle("borderColor");
			}else{
				Alert.show("Les champs en rouge sont obligatoires","Erreur");
			}
		}
		
		
		
		private function mapData(item : OngletEtatVo):void{			
			item.IDSOUS_TETE		= gestionTechnique.ligne.IDSOUS_TETE;
			/* item.ETAT 				= ETAT.text;
			item.DATE_COMMANDE		= DATE_COMMANDE.selectedDate;
			item.REF_COMMANDE		= REF_COMMANDE.text; */
			item.BOOL_TITULAIRE		= TITULAIRE.selectedItem.value;
			item.BOOL_PAYEUR		= PAYEUR.selectedItem.value;
			/* item.COMMENTAIRE_ETAT	= COMMENTAIRE_ETAT.text;
			item.DATE_MISE_EN_SERV	= DATE_MISE_EN_SERV.selectedDate;
			item.REF_MISE_EN_SERV	= REF_MISE_EN_SERV.text;
			item.GEST_MISE_EN_SERV	= GEST_MISE_EN_SERV.text;
			item.DATE_PRESELECTION	= DATE_PRESELECTION.selectedDate; 
			item.REF_PRESELECTION	= REF_PRESELECTION.text;
			item.GEST_PRESELECTION	= GEST_PRESELECTION.text;
			item.DATE_SUSPENSION	= DATE_SUSPENSION.selectedDate; 
			item.REF_SUSPENSION		= REF_SUSPENSION.text;
			item.GEST_SUSPENSION	= GEST_SUSPENSION.text;
			item.DATE_RESILIATION	= DATE_RESILIATION.selectedDate;
			item.REF_RESILIATION	= REF_RESILIATION.text;
			item.GEST_RESILIATION	= GEST_RESILIATION.text; */
			item.V3			= V3.text;
			item.V4			= V4.text;
		}
		
		
		
	}
}