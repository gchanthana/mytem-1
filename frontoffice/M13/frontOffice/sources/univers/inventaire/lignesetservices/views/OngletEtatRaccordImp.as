package univers.inventaire.lignesetservices.views
{
	import composants.controls.TextInputLabeled;
	import composants.tb.effect.EffectProvider;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.List;
	import mx.controls.PopUpMenuButton;
	import mx.controls.TextArea;
	import mx.events.CloseEvent;
	import mx.events.ListEvent;
	import mx.events.MenuEvent;
	import mx.managers.PopUpManager;
	import mx.managers.PopUpManagerChildList;
	
	import univers.inventaire.lignesetservices.applicatif.ActionRaccordementValues;
	import univers.inventaire.lignesetservices.applicatif.GestionEtatRaccordement;
	import univers.inventaire.lignesetservices.views.popups.AjouterOperateurIndirectView;
	import univers.inventaire.lignesetservices.views.popups.DoActionInventaireView;
	import univers.inventaire.lignesetservices.views.popups.DoActionRaccoImpl;
	import univers.inventaire.lignesetservices.views.popups.PopUpImpl;
	import univers.inventaire.lignesetservices.views.popups.RaccordAutoView;
	import univers.inventaire.lignesetservices.views.popups.ReactiverView;
	import univers.inventaire.lignesetservices.views.popups.ResilierView;
	import univers.inventaire.lignesetservices.views.popups.SelectRaccoView;
	import univers.inventaire.lignesetservices.views.popups.SuspendreView;
	import univers.inventaire.lignesetservices.vo.Action;
	import univers.inventaire.lignesetservices.vo.ModeRaccordementVo;
	import univers.inventaire.lignesetservices.vo.OngletEtatVo;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	
	[Bindable]
	public class OngletEtatRaccordImp extends BaseView
	{
		public function OngletEtatRaccordImp()
		{
			super();
			
		}
		
				
		//--------------- CONSTANTES -----------------------------------------------								
		//-------------FIN  CONSTANTES ---------------------------------------------			
		
		//------------- VARIABLES --------------------------------------------------
	
		private var _selectedActionInventaire:Action;
		private var _selectedActionRacco:Action;
		
		
		//-cmp
		
		private var doActionInventaireView : DoActionInventaireView;
		
		public var dgHistoriqueAction : DataGrid;
		public var pmbActionRacco : PopUpMenuButton;
		private var selectRaccoView : DoActionRaccoImpl;
								
		public var liOperateursIndirect : List;
		public var btAjouterOperateursIndirects : Button;	
		private var ajouterOperateursIndirectsView : AjouterOperateurIndirectView;
		
		public var pmbActionInventaire : PopUpMenuButton;
			
		public var lblFacture : Label;
		public var cbFiltreInventaire : ComboBox;
		
		public var dgListeProduitsSecondaires : DataGrid;
		//public var dgProduitsAcces : DataGrid;
		
		
		public var btUpdate : Button;		
		public var txtCommentaires :TextArea;
		public var V3 : TextInputLabeled;
		public var V4 : TextInputLabeled;
		public var PAYEUR : ComboBox;
		public var TITULAIRE : ComboBox;
		
		
		
		//---------- FIN VARIABLES -------------------------------------------------
	
		//---------- ACCESORS ------------------------------------------------------
		
		
		//-------FIN ACCESORS ------------------------------------------------------
	
		//======================== HANDLERS ========================================
		public function liOperateursIndirectMouseOverHandler(me : MouseEvent):void{
		}
		
		protected function pmbActionRaccoMenuItemClickHandler(me : MenuEvent):void{
			_selectedActionRacco = Action(me.item);
		}
		
		
		protected function pmbActionRaccoClickHandler(me : MouseEvent):void{			
			
			if (selectRaccoView != null) selectRaccoView = null;
	 
				
			
				if(_selectedActionRacco != null){
					
					//Si l'action est de raccordé à l'opérateur historique 
					if ( _selectedActionRacco.identifiant == ActionRaccordementValues.RACCORDER_OPHISTORIQUE){
						
						//Si on était en raccord indirect via prefixe, on vide les opérateur indirect.
						if (gestionTechnique.gestionEtat.etatVo.IDTYPE_RACCORDEMENT == GestionEtatRaccordement.PRESELECTION){					
							ConsoviewAlert.afficherAlertConfirmation("Attention! Cette action va supprimer les opérateurs indirects.\nVoulez vous continuer?","Confirmez",confirmerRaccordOpeHistoriqueHandler);
						}else{
							raccorderOpHistorique(_selectedActionRacco);	
						}
						return;
					}
					
					
					
					if ( gestionTechnique.gestionEtat.listeActions != null &&  gestionTechnique.gestionEtat.listeActions.length > 0){
						
						selectRaccoView = createPopUpActionRacco(_selectedActionRacco);
						selectRaccoView.action = _selectedActionRacco;
						selectRaccoView.app = gestionTechnique.gestionEtat;
						selectRaccoView.addEventListener(PopUpImpl.VALIDER_CLICKED,selectRaccoViewValiderClickHandler);
						
						PopUpManager.addPopUp(selectRaccoView,this,true);
						PopUpManager.centerPopUp(selectRaccoView);	
					}else{
						Alert.okLabel = "Fermer";
						Alert.buttonWidth = 100;
						Alert.show("Aucune action n'est autorisée","Info.");
					}
				}
		
		}
		
		protected function confirmerRaccordOpeHistoriqueHandler(ce : CloseEvent):void{
			if (ce.detail == Alert.OK){
				gestionTechnique.gestionEtat.supprimerOperateursIndirect(new Array(5));	
			}
		}
		
				
		protected function selectRaccoViewValiderClickHandler(ev : Event):void{
			gestionTechnique.gestionEtat.selectedActionRAcco = selectRaccoView.action; 
			gestionTechnique.gestionEtat.selectedModeRaccordement = selectRaccoView.modeRacco;
			gestionTechnique.gestionEtat.selectedOperateursIndirects = new ArrayCollection([selectRaccoView.operateurIndirect]);
			gestionTechnique.gestionEtat.ressourcesSelectionnees = selectRaccoView.ressourcesSelectionees;
			gestionTechnique.gestionEtat.faireActionSurLigne();
			selectRaccoView = null;
		}
		
		private function pmbActionInventaireMenuItemClickHandler(me : MenuEvent):void{
			 
				_selectedActionInventaire = Action(me.item);	
			 
		}
		
		protected function pmbActionInventaireClickHandler(me : MouseEvent):void{
			if (doActionInventaireView != null) doActionInventaireView = null;
			if(_selectedActionInventaire != null){
					
				 
				/* if (dgListeProduitsSecondaires.selectedItems.length == 1){
					doActionInventaireView = new DoActionInventaireView();	
					doActionInventaireView.app = gestionTechnique.gestionEtat;
					doActionInventaireView.action = _selectedActionInventaire;
					doActionInventaireView.addEventListener(PopUpImpl.VALIDER_CLICKED,doActionInventaireViewValiderClickHandler);
					PopUpManager.addPopUp(doActionInventaireView,this,true);
					PopUpManager.centerPopUp(doActionInventaireView);	
				}else  */if (dgListeProduitsSecondaires.selectedItems.length > 0){
					// verifier que tous les produits selectionné soit dans le même état et que aucun des produits sélectionné soit dans une opération
					var boolOk : Boolean = true;
					var boolEtatOk : Boolean = true;
					var boolPasEncours : Boolean = true;
					
					var idEtatRef : String = dgListeProduitsSecondaires.selectedItems[0].LIBELLE_ETAT;
					var len : Number = dgListeProduitsSecondaires.selectedItems.length;
													
					for(var i:Number = 0; i < len; i++){
						if(dgListeProduitsSecondaires.selectedItems[i].LIBELLE_ETAT != idEtatRef){
							 boolEtatOk = false;						 
							 break;	
						}
					}
					
					for(var k:Number = 0; k < len; k++){
						if(dgListeProduitsSecondaires.selectedItems[k].EN_COURS > 0){
							 boolPasEncours = false;						 
							 break;	
						}
					}
					
					
					var boolAcces : Boolean = false;
					for(var j:Number = 0; j < len; j++){
						if(dgListeProduitsSecondaires.selectedItems[j].BOOL_ACCES > 0){
							 boolAcces = true;						 
							 break;	
						}
					}
					
					
					boolOk = boolEtatOk && boolPasEncours;	
					
					//si oui alors
					if (boolOk){
						
						//si on a selectionné un produit d'acces 
						if (boolAcces){
							//alors on demande si on changer le mode de raccordement de la ligne
							ConsoviewAlert.afficherAlertConfirmation("Vous tentez de faire une action sur un produit d'accès.\nVoulez vous changer le mode de raccordement de la ligne?","Action sur un produit d'accès",informationHandler);
						}else{
							//sinon on fait l'action sur le produit 
							doActionInventaireView = new DoActionInventaireView();	
							doActionInventaireView.app = gestionTechnique.gestionEtat;
							doActionInventaireView.action = _selectedActionInventaire;
							doActionInventaireView.addEventListener(PopUpImpl.VALIDER_CLICKED,doActionInventaireViewValiderClickHandler);
							PopUpManager.addPopUp(doActionInventaireView,this,true);
							PopUpManager.centerPopUp(doActionInventaireView);	
							
						}	
					}
					//sinon
					else{
						Alert.okLabel = "Fermer";
						if (!boolEtatOk && boolPasEncours){
							Alert.show("Les ressources sélectionnées doivent être dans le même état","Info.");
						}else if (!boolPasEncours && boolEtatOk) {
							Alert.show("Les produits marqués par une croix appartiennent à une opération ouverte.","Info.");
						}else{
							Alert.show("Les ressources sélectionnées doivent être dans le même état.\nLes produits marqués par une croix appartiennent à une opération ouverte.","Info.");
						}
						
					}
				} else{
					Alert.okLabel = "Fermer";
					Alert.buttonWidth = 100;
					Alert.show("Vous devez sélectionner au moins un produit.","Info.");	
				}
			}else{
				Alert.okLabel = "Fermer";
				Alert.buttonWidth = 100;
				Alert.show("Vous devez sélectionner un action","Info.");
			}
			
		}
		
		
		protected function informationHandler(ce : CloseEvent):void{
			if(ce.detail == Alert.OK){
				EffectProvider.FadeThat(pmbActionRacco);
				pmbActionRacco.setFocus();
				
				
			}else{
				doActionInventaireView = new DoActionInventaireView();	
				doActionInventaireView.app = gestionTechnique.gestionEtat;
				doActionInventaireView.action = _selectedActionInventaire;
				doActionInventaireView.addEventListener(PopUpImpl.VALIDER_CLICKED,doActionInventaireViewValiderClickHandler);
				PopUpManager.addPopUp(doActionInventaireView,this,true);
				PopUpManager.centerPopUp(doActionInventaireView);	
			}
		}
		
		
		protected function doActionInventaireViewValiderClickHandler(ev : Event):void{
			var action : Action = doActionInventaireView.action
			if (_selectedActionInventaire.identifiant == 61) gestionTechnique.gestionEtat.sortirDeInventaire(action.dateAction,action.commentaire)
			else if (_selectedActionInventaire.identifiant == 62) gestionTechnique.gestionEtat.entrerDansInventaire(action.dateAction,action.commentaire)
			doActionInventaireView = null;
			_selectedActionInventaire = null;
		}
		
		
		protected function btAjouterOperateursIndirectsClickHandler(me : MouseEvent):void{
			if (ajouterOperateursIndirectsView != null) ajouterOperateursIndirectsView = null;			
			ajouterOperateursIndirectsView = new AjouterOperateurIndirectView();			
			ajouterOperateursIndirectsView.app = gestionTechnique.gestionEtat;
			ajouterOperateursIndirectsView.addEventListener(PopUpImpl.VALIDER_CLICKED,ajouterOperateursIndirectsViewValiderClickHandler);
			
			PopUpManager.addPopUp(ajouterOperateursIndirectsView,this,true);
			PopUpManager.centerPopUp(ajouterOperateursIndirectsView);
		}
		
		protected function ajouterOperateursIndirectsViewValiderClickHandler(ev : Event):void{
			gestionTechnique.gestionEtat.
				ajouterOperateursIndirect(ajouterOperateursIndirectsView.selectedOperateursIndirects);
				ajouterOperateursIndirectsView = null;
		}
		
		protected function cbFiltreInventaireChangeHandler(le : ListEvent):void{
			gestionTechnique.gestionEtat.filtrerInventaireRessources(cbFiltreInventaire.selectedItem.value);	
		}
		
		protected function dgHistoriqueActionChangeHandler(le : ListEvent):void{
									
		}
			
		
		protected function dgListeProduitsSecondairesChangeHandler(le : ListEvent):void{
			if (dgListeProduitsSecondaires.selectedIndex != -1){	
				if(dgListeProduitsSecondaires.selectedItems.length == 1){
					gestionTechnique.gestionEtat.ressourcesSelectionnees = dgListeProduitsSecondaires.selectedItems;
					gestionTechnique.gestionEtat.getHistoriqueInventaire();
					gestionTechnique.gestionEtat.getActionsInventairesPossibles();
					pmbActionInventaire.enabled = modeEcriture && true;
						
				}else if(dgListeProduitsSecondaires.selectedItems.length > 1){
					gestionTechnique.gestionEtat.ressourcesSelectionnees = dgListeProduitsSecondaires.selectedItems;
					gestionTechnique.gestionEtat.getHistoriqueInventaire();
					gestionTechnique.gestionEtat.getActionsInventairesPossibles();
					pmbActionInventaire.enabled = modeEcriture && true;
				}else{
					pmbActionInventaire.enabled = false;
				}
			}
		}
		
		protected function btUpdateClickHandler(me : MouseEvent):void{
			enregistrer();	
		}
		
		
		protected function raccorderOpHistoriqueHandler(ev : Event):void{
			
			_selectedActionRacco = new Action();
			_selectedActionRacco.identifiant = ActionRaccordementValues.RACCORDER_OPHISTORIQUE;
			_selectedActionRacco.libelle = "Raccorder opé. historiques";	
			
			raccorderOpHistorique(_selectedActionRacco);
		}
		//======================== FIN HANDLERS ====================================	
		
		
		
		//---- METHODES ------------------------------------------------------------
		
		//======================== PUBLIC ==========================================	
		//======================== FIN PUBLIC ======================================
		
		//======================== PROTECTED ========================================
		override protected function commitProperties():void{
			super.commitProperties();
			pmbActionRacco.addEventListener(MouseEvent.CLICK,pmbActionRaccoClickHandler);
			pmbActionRacco.addEventListener(MenuEvent.ITEM_CLICK,pmbActionRaccoMenuItemClickHandler);
			
			pmbActionInventaire.addEventListener(MouseEvent.CLICK,pmbActionInventaireClickHandler);
			pmbActionInventaire.addEventListener(MenuEvent.ITEM_CLICK,pmbActionInventaireMenuItemClickHandler);
			btAjouterOperateursIndirects.addEventListener(MouseEvent.CLICK,btAjouterOperateursIndirectsClickHandler);
			liOperateursIndirect.addEventListener(MouseEvent.MOUSE_OVER,liOperateursIndirectMouseOverHandler);
			cbFiltreInventaire.addEventListener(ListEvent.CHANGE,cbFiltreInventaireChangeHandler);
			dgListeProduitsSecondaires.addEventListener(ListEvent.CHANGE,dgListeProduitsSecondairesChangeHandler);
			//dgProduitsAcces.addEventListener(ListEvent.CHANGE,dgProduitsAccesChangeHandler);
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);	
			gestionTechnique.gestionEtat.addEventListener(GestionEtatRaccordement.RACCORDER_OPE_HISTORIQUE,raccorderOpHistoriqueHandler);
		}
		
		override protected function enregistrer():void{
			gestionTechnique.gestionEtat.etatVo.BOOL_PAYEUR = (PAYEUR.selectedItem.value == 1)?1:0;
			gestionTechnique.gestionEtat.etatVo.BOOL_TITULAIRE = (TITULAIRE.selectedItem.value == 1)?1:0;
			gestionTechnique.gestionEtat.etatVo.V3 = V3.text;
			gestionTechnique.gestionEtat.etatVo.V4 = V4.text;
			
			
			//Temp ----------------------
			gestionTechnique.gestionEtat.etatVo.IDENTIFIANT_AGENCE = isNaN(gestionTechnique.gestionEtat.etatVo.IDENTIFIANT_AGENCE)?-1:gestionTechnique.gestionEtat.etatVo.IDENTIFIANT_AGENCE;
			gestionTechnique.gestionEtat.etatVo.IDOPERATEUR_CREATEUR_NUMERO = isNaN(gestionTechnique.gestionEtat.etatVo.IDOPERATEUR_CREATEUR_NUMERO)?-1:gestionTechnique.gestionEtat.etatVo.IDOPERATEUR_CREATEUR_NUMERO;
			//----------------------------
			
			
			gestionTechnique.gestionEtat.etatVo.COMMENTAIRE_ETAT = txtCommentaires.text;
			gestionTechnique.gestionEtat.udpateInfosEtat();
			
		}
		
		protected function createPopUpActionRacco(act : Action):DoActionRaccoImpl{
			switch(act.identifiant){
				case ActionRaccordementValues.CHANGER_RACCORDEMENT:{
					return new SelectRaccoView();
				}break;
				case ActionRaccordementValues.RACCORDER:{
					return new RaccordAutoView();
				}break;
				case ActionRaccordementValues.REACTIVER:{
					return new ReactiverView();
				}break;
				case ActionRaccordementValues.RESILIER:{
					return new ResilierView();
				}break;
				case ActionRaccordementValues.SUSPENDRE:{
					return new SuspendreView();
				}break;
				case ActionRaccordementValues.RACCORDER_OPHISTORIQUE:{
					return new RaccordAutoView();
				}break;
				default: return new RaccordAutoView();
			}
		}
		
		protected function commentairesDataTipFunction(item : Object):String{
			return (item.COMMENTAIRES != null)?item.COMMENTAIRES:"";
		}
		
		
		protected function raccorderOpHistorique(act : Action):void{
			if (selectRaccoView != null) selectRaccoView = null;
			
								
			selectRaccoView = createPopUpActionRacco(_selectedActionRacco);						
			selectRaccoView.action = act;
			
			
			var modeRacc : ModeRaccordementVo = new ModeRaccordementVo();
			
			modeRacc.BOOL_DIRECT = 1;
			modeRacc.IDTYPE_RACCORDEMENT = 4;
			modeRacc.LIBELLE_RACCORDEMENT = "Raccordé Op historique";
			modeRacc.COMMENTAIRE_RACCORDEMENT = "Raccordée à France Telecom";
			
			selectRaccoView.modeRacco = modeRacc;
			selectRaccoView.app = gestionTechnique.gestionEtat;
			
			selectRaccoView.addEventListener(PopUpImpl.VALIDER_CLICKED,selectRaccoViewValiderClickHandler);
			
			PopUpManager.addPopUp(selectRaccoView,this,true);
			PopUpManager.centerPopUp(selectRaccoView);
			
			
		}
		//======================== FIN PROTECTED ====================================
					
		//======================== PRIVATE ==========================================
		
		private function enregistrerInfoDivers(ligne : OngletEtatVo):void{
			
		}
		private function getHistoriqueInventaire(produit : Object):void{
			
		}
		private function getInventaire(ligne : OngletEtatVo):void{
			
		}
		private function getOperateursIndirect(ligne : OngletEtatVo):void{
				
		}
		private function getListeOperateursFacturants(ligne : OngletEtatVo):void{
				
		}
		private function ajouterOperateurIndirect(operateur : OperateurVO):void{
			//si raccordé indirect via préfixe ok sinon !ok
		}
		private function depreselectionnerOperateur(operateur : OperateurVO):void{
			//si etat = raccordé indirect via préfix ou présélection
			//si la liste est vide alors et Etat -> Raccordé op Historique
		}
		
		//======================== FIN PRIVATE ======================================
		
	}
}