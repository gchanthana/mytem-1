package univers.inventaire.lignesetservices.applicatif
{
	import univers.inventaire.lignesetservices.vo.Ligne;
	import univers.inventaire.lignesetservices.vo.OngletAffectationVo;

	public class GestionAffectationDefaut extends GestionAffectationStrategy
	{
		public function GestionAffectationDefaut(ligne : Ligne)
		{	
			super(ligne);
		}
				
		override public function getInfosAffectation():void{
			trace("GestionAffectationDefaut.getInfosAffectation");
		}		
		
		override public function updateInfosAffectation(data : OngletAffectationVo):void{
			trace("GestionAffectationDefaut.updateInfosAffectation");
		}
		
	}
}