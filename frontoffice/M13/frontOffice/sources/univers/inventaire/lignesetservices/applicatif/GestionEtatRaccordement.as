package univers.inventaire.lignesetservices.applicatif
{
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	import univers.inventaire.lignesetservices.util.viewsHelpers;
	import univers.inventaire.lignesetservices.vo.Action;
	import univers.inventaire.lignesetservices.vo.HistoriqueActionVo;
	import univers.inventaire.lignesetservices.vo.HistoriqueInventaireLigneVo;
	import univers.inventaire.lignesetservices.vo.InventaireLigneVo;
	import univers.inventaire.lignesetservices.vo.Ligne;
	import univers.inventaire.lignesetservices.vo.ModeRaccordementVo;
	import univers.inventaire.lignesetservices.vo.OngletEtatVo;
	
	[Event(name="raccorderOpHistorique")]
	
	
	[Bindable]
	public class GestionEtatRaccordement extends EventDispatcher
	{
		
		public static const DIRECT_VIA_PREFIXE : Number = 9;
		public static const PRESELECTION : Number = 1;
		
		public static var MAX_OPERATEUR_INDIRECT : Number = 5;		
		public static const RACCORDER_OPE_HISTORIQUE : String = "raccorderOpHistorique";
				
		private static const SORTIR_INVENTAIRE : Number = 61;
		private static const ENTRER_INVENTAIRE : Number = 62;
						
		//la ligne selectioné
		private var _ligne : Ligne
		public function get ligne(): Ligne{ 
			return _ligne;
		} 
		public function set ligne(li : Ligne):void{
			_ligne = li;
		}
		
		//les données sur l'état de la ligne
		private var _etatVo : OngletEtatVo
		public function get etatVo(): OngletEtatVo{
			return _etatVo;
		} 
		public function set etatVo(etat : OngletEtatVo):void{
			_etatVo = etat;
		}
		
		
		///L'historique des action sur un ligne
		private var _historiqueActions : ArrayCollection
		public function get historiqueActions(): ArrayCollection{ 
			return _historiqueActions;
		} 
		public function set historiqueActions(historique : ArrayCollection):void{
			_historiqueActions = historique;
		}
		
		
		///Liste des mode de taccordement
		private var _modesRaccordement : ArrayCollection;
		public function get modesRaccordement(): ArrayCollection{ 
			return _modesRaccordement;
		} 
		public function set modesRaccordement(modes : ArrayCollection):void{
			_modesRaccordement = modes;
		}
		
		
		///liste exhaustive des opérateurs 
		private var _operateurs : ArrayCollection;
		public function get operateurs(): ArrayCollection{
			return _operateurs;
		} 
		public function set operateurs(opes : ArrayCollection):void{
			_operateurs = opes;
		}
		
		
		///Liste des opérateurs indirects
		private var _operateursIndirects : ArrayCollection;
		public function get operateursIndirects():ArrayCollection{ 
			if (_operateursIndirects == null) _operateursIndirects = new ArrayCollection();
			return _operateursIndirects;
		} 
		public function set operateursIndirects(opIndirects : ArrayCollection):void{
			_operateursIndirects = opIndirects;
		}
		
		
		///Liste des ressources pour une ligne
		private var _ressourcesLigne : ArrayCollection;
		public function get ressourcesLigne():ArrayCollection{ 
			return _ressourcesLigne;
		} 
		public function set ressourcesLigne(ressources: ArrayCollection):void{
			_ressourcesLigne = ressources;
			_ressourcesLigne.refresh();
		}
		
		//La valeur du filtre
		private var filtreInventaireValue : Number = 1;
		
		///Produit d'acces
		private var _ressourceAcces : ArrayCollection;
		public function get ressourceAcces():ArrayCollection{ 
			return _ressourceAcces;
		} 
		public function set ressourceAcces(ressources: ArrayCollection):void{
			_ressourceAcces = ressources;	
			
		}
		
		//Historique des actions sur une ressource
		private var _historiqueRessource: ArrayCollection;
		public function get historiqueRessource():ArrayCollection{ 
			return _historiqueRessource;
		} 
		public function set historiqueRessource(historique : ArrayCollection):void{
			_historiqueRessource = historique;
		}
				
					
		///Liste des actions possible suivant état
		private var _listeActions: ArrayCollection;
		public function get listeActions():ArrayCollection{ 
			return _listeActions;
		} 
		public function set listeActions(liste : ArrayCollection):void{
			_listeActions = liste;
		}
		
		///Liste d'opérateurs sélectionnés
		private var _listeOperateursSelectionnes: ArrayCollection;
		public function get listeOperateursSelectionnes():ArrayCollection{ 
			return _listeOperateursSelectionnes;
		} 
		public function set listeOperateursSelectionnes(liste : ArrayCollection):void{
							
			if (_listeOperateursSelectionnes != null) _listeOperateursSelectionnes = null;
			
			_listeOperateursSelectionnes = liste;
		}
		
		//la ressource selectionné
		private var _ressourcesSelectionnees : Array;
		public function get ressourcesSelectionnees():Array{ 
			return _ressourcesSelectionnees;
		} 
		public function set ressourcesSelectionnees(inv : Array):void{								
			if (_ressourcesSelectionnees != null) _ressourcesSelectionnees = null;				
			_ressourcesSelectionnees = inv;
		}
		
		//la ressource selectionné
		private var _actionsRessourcesPossible : ArrayCollection;
		public function get actionsRessourcesPossible():ArrayCollection{ 
			return _actionsRessourcesPossible;
		} 
		public function set actionsRessourcesPossible(liste : ArrayCollection):void{
			_actionsRessourcesPossible = liste;
		}
		
		
		//l'action sélectionné
		private var _selectedActionRAcco : Action;
		public function get selectedActionRAcco():Action{
			return _selectedActionRAcco;
		}
		public function set selectedActionRAcco(act:Action):void{
			_selectedActionRAcco =  act;
		}
		
		//le mode de raccordement selectionné
		public var _selectedModeRaccordement : ModeRaccordementVo;
		public function get selectedModeRaccordement():ModeRaccordementVo{
			return _selectedModeRaccordement;
		}
		public function set selectedModeRaccordement(mode:ModeRaccordementVo):void{
			_selectedModeRaccordement =  mode;
		}
		
		//les opérateurs indirects sélectionnés
		private var _selectedOperateursIndirects : ArrayCollection;
		public function get selectedOperateursIndirects():ArrayCollection{
			return _selectedOperateursIndirects;
		}
		public function set selectedOperateursIndirects(liste : ArrayCollection):void{
			_selectedOperateursIndirects =  liste;
		}
		
		//aide
		private var _helpers : viewsHelpers;
		public function get helpers():viewsHelpers{
			return _helpers;
		}
		public function set helpers(help : viewsHelpers):void{
			_helpers =  help;
		}
		
		private var _boolWarningOpeAccess : Boolean;
		public function get boolWarningOpeAccess():Boolean{
			return _boolWarningOpeAccess;
		}
		public function set boolWarningOpeAccess(bool : Boolean):void{
			_boolWarningOpeAccess = bool;
		}
		
		private var _boolDirect : Boolean = true;
		public function get boolDirect():Boolean{
			return _boolDirect;
		}
		public function set boolDirect(bool : Boolean):void{
			_boolDirect = bool;
		}
		
		//le nombre ope indirect avant la mise à jour
		private var lastNbOperateur : Number = 0;
					
		public function GestionEtatRaccordement()		
		{
			
		}
		
		
			
			
			
		//Fournit la liste des modes de raccordement
		public function getListeModeRaccordement():void{				
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getListeModeRaccordement",
																				getListeModeRaccordementResultHandler);
			RemoteObjectUtil.callService(op);
		}
		private function getListeModeRaccordementResultHandler(re : ResultEvent):void{
			if(re.result){
				
				var col : ArrayCollection = re.result as ArrayCollection;
				var len : Number = col.length;
				
				modesRaccordement = new ArrayCollection();
				
				for (var i : Number = 0; i <  len ; i++){
					if((String(re.result[i].LIBELLE_RACCORDEMENT).toLowerCase().search("suspendu") != -1)||
					(String(re.result[i].LIBELLE_RACCORDEMENT).toLowerCase().search("résilié") != -1)||
					(String(re.result[i].LIBELLE_RACCORDEMENT).toLowerCase().search("non raccordé") != -1))					
					{
							
					}else{
						var mode : ModeRaccordementVo = new ModeRaccordementVo();																	
						mode.BOOL_DIRECT =  col[i].BOOL_DIRECT;
						mode.COMMENTAIRE_RACCORDEMENT = col[i].COMMENTAIRE_RACCORDEMENT; 
						mode.IDTYPE_RACCORDEMENT = col[i].IDTYPE_RACCORDEMENT;
						mode.LIBELLE_RACCORDEMENT = col[i].LIBELLE_RACCORDEMENT;
						modesRaccordement.addItem(mode);
					}
				}
			}
		}
		
		public function getModeRaccordementPrecedent():void{				
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getModeRaccordementPrecedent",
																				getModeRaccordementPrecedentResultHandler);
			RemoteObjectUtil.callService(op,historiqueActions.source[0].IDACTION_PREC);
		}
		private function getModeRaccordementPrecedentResultHandler(re : ResultEvent):void{
			if(re.result){
				var col : ArrayCollection = re.result as ArrayCollection;
				selectedModeRaccordement = new ModeRaccordementVo();
				selectedModeRaccordement.BOOL_DIRECT =  col[0].BOOL_DIRECT;
				selectedModeRaccordement.COMMENTAIRE_RACCORDEMENT = col[0].COMMENTAIRE_RACCORDEMENT; 
				selectedModeRaccordement.IDTYPE_RACCORDEMENT = col[0].IDTYPE_RACCORDEMENT;
				selectedModeRaccordement.LIBELLE_RACCORDEMENT = col[0].LIBELLE_RACCORDEMENT;
			}
		}
		
		
		
		//Fournit la liste des opérateurs
		public function getListeOperateurs():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getListeOperateurs",
																				getListeOperateursResultHandler);
			RemoteObjectUtil.callService(op);
		}
		
		public function getListeOperateursResultHandler(re : ResultEvent):void{
			if(re.result){
				
				var col : ArrayCollection = re.result as ArrayCollection;
				var len : Number = col.length;
				
				operateurs = new ArrayCollection();
				
				for (var i : Number = 0; i <  len ; i++){
					var ope : OperateurVO = new OperateurVO();
																	
					ope.id =  col[i].OPERATEURID;
					ope.nom = col[i].NOM;
					operateurs.addItem(ope);
				}					
			}
		}			
		
		//Fournit la liste des opérateurs indirects pour une sous_tête (les opérateurs autres que l'opérateur d'accès qui facturent des produits sur la ligne)
		public function getlisteOperateurIndirect():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getlisteOperateurIndirect",
																				getlisteOperateurIndirectResultHandler);
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE);
		}
		private function getlisteOperateurIndirectResultHandler(re : ResultEvent):void{
			if(re.result){					
				var col : ArrayCollection = re.result as ArrayCollection;
				var len : Number = col.length;
				
				
				operateursIndirects = new ArrayCollection();
				
				for (var i : Number = 0; i <  len ; i++){
					var ope : OperateurVO = new OperateurVO();
																	
					ope.id =  col[i].OPERATEURID;
					ope.nom = col[i].OPNOM;
					
					operateursIndirects.addItem(ope);
				}
				
				if ((lastNbOperateur > 0)&&(len == 0)){
					//faire action raccordement opHistorique					
					dispatchEvent(new Event(RACCORDER_OPE_HISTORIQUE));
					
					lastNbOperateur = 0;
					
				}else{
					lastNbOperateur = len;
				}				
									
			}
		}	
		
		//Ajoute une liste d'opérateurs indirects à la ligne
		public function ajouterOperateursIndirect(listeOperateurs : Array):void{				
			
			var idop1 : Number = (listeOperateurs[0] != null)?listeOperateurs[0].id : -1;
			var idop2 : Number = (listeOperateurs[1] != null)?listeOperateurs[1].id : -1;
			var idop3 : Number = (listeOperateurs[2] != null)?listeOperateurs[2].id : -1;
			var idop4 : Number = (listeOperateurs[3] != null)?listeOperateurs[3].id : -1;
			var idop5 : Number = (listeOperateurs[4] != null)?listeOperateurs[4].id : -1;
			
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"majOperateursIndirects",
																				ajouterOperateursIndirectResultHandler);
																				
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE,idop1,idop2,idop3,idop4,idop5);
		}
		public function ajouterOperateursIndirectResultHandler(re : ResultEvent):void{
			if(re.result > 0){
				getlisteOperateurIndirect();
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Probleme lors de l'ajout.","Erreur");
			}
		}
		
		
		
		//Supprime une liste d'opérateurs indirects à la ligne ------ NON UTILISE
		public function supprimerOperateursIndirect(listeOperateurs : Array):void{
			
			var idop1 : Number = (listeOperateurs[0] != null)?listeOperateurs[0].id : -1;
			var idop2 : Number = (listeOperateurs[1] != null)?listeOperateurs[1].id : -1;
			var idop3 : Number = (listeOperateurs[2] != null)?listeOperateurs[2].id : -1;
			var idop4 : Number = (listeOperateurs[3] != null)?listeOperateurs[3].id : -1;
			var idop5 : Number = (listeOperateurs[4] != null)?listeOperateurs[4].id : -1;
			
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"majOperateursIndirects",
																				supprimerOperateursIndirectResultHandler);
		
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE,idop1,idop2,idop3,idop4,idop5);
		}
		public function supprimerOperateursIndirectResultHandler(re : ResultEvent):void{
			if(re.result > 0){
				getlisteOperateurIndirect();
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Probleme lors de la suppression.","Erreur");
			}
		}
		
			
		//Fournit la liste des ressources d'une sous tete avec l'état de la ressource et un flag indicant si la ressource est dans une opération ouverte.
		public function getListeInventaireSousTete():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getListeInventaireSousTete",
																				getListeInventaireSousTeteResultHandler);
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE);	
		}
		public function getListeInventaireSousTeteResultHandler(re : ResultEvent):void{
			if(re.result){					
				var col : ArrayCollection = re.result as ArrayCollection;
				var len : Number = col.length;
				
				ressourcesLigne = new ArrayCollection();
				ressourceAcces = new ArrayCollection();
				
				ressourceAcces.filterFunction = dofiltrerInventaire;
				ressourcesLigne.filterFunction =  dofiltrerInventaire;
				
				for (var i : Number = 0; i <  len ; i++){
					var inv:InventaireLigneVo=new InventaireLigneVo();
					inv.DANS_INVENTAIRE = col[i].DANS_INVENTAIRE;
					inv.EN_COURS = col[i].EN_COURS;
					inv.ETAT_ACTUEL = col[i].ETAT_ACTUEL;
					inv.IDINV_ACTIONS = col[i].IDINV_ACTIONS;
					inv.IDINV_OP_ACTION = col[i].IDINV_OP_ACTION;
					inv.IDINV_OPERATIONS = col[i].IDINV_OPERATIONS;
					inv.IDINVENTAIRE_PRODUIT = col[i].IDINVENTAIRE_PRODUIT;
					inv.BOOL_ACCES = col[i].BOOL_ACCES;
					inv.LIBELLE_ETAT = col[i].LIBELLE_ETAT;
					inv.LIBELLE_PRODUIT = col[i].LIBELLE_PRODUIT;
					inv.OPERATEURID = col[i].OPERATEURID;
					inv.OPNOM = col[i].OPNOM;
					inv.QTE = col[i].QTE;
					inv.ENTRER = false;
					inv.SORTIR = false;
					
					ressourcesLigne.addItem(inv);
					if(inv.BOOL_ACCES) ressourceAcces.addItem(inv);
				}					
			}
		}
		
		public function filtrerInventaireRessources(value : Number):void{
			filtreInventaireValue = value;
			if (ressourcesLigne != null){
				ressourcesLigne.refresh();
			}
		}
		
		
		private function dofiltrerInventaire(item : Object):Boolean{
			if ((filtreInventaireValue == 1)||
				((filtreInventaireValue == 2) && (item.DANS_INVENTAIRE == 1))||
				((filtreInventaireValue == 3) && (item.DANS_INVENTAIRE == 0))){
				return true;
			}else{
				return false;
			}
		}
		
		//Fournit l'historique des actions menées sur une ressource de l'inventaire
		public function getHistoriqueInventaire():void{
			 
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																				"getHistoriqueActionRessource",
																				getHistoriqueInventaireResultHandler);
			RemoteObjectUtil.callService(op,ressourcesSelectionnees[0].IDINVENTAIRE_PRODUIT);	
		}
		
		public function getHistoriqueInventaireResultHandler(re : ResultEvent):void{
			if(re.result){
				var col : ArrayCollection = re.result as ArrayCollection;
				var len : Number = col.length;
				
				historiqueRessource = new ArrayCollection();
				
				for (var i : Number = 0; i <  len ; i++){
					var histo : HistoriqueInventaireLigneVo = new HistoriqueInventaireLigneVo();
																							
					histo.IDINV_OP_ACTION = col[i].IDINV_OP_ACTION;
					histo.IDINV_ETAT = col[i].IDINV_ETAT;
					histo.LIBELLE_ETAT = col[i].LIBELLE_ETAT;						
					histo.COMMENTAIRE_ETAT = col[i].COMMENTAIRE_ETAT;
					histo.DANS_OPERATION = col[i].DANS_OPERATION;
					histo.DANS_INVENTAIRE = col[i].DANS_INVENTAIRE;
					histo.DATE_ACTION = col[i].DATE_ACTION;
					histo.DATE_INSERT = col[i].DATE_INSERT;
					histo.DUREE_ETAT = col[i].DUREE_ETAT;
					histo.LOGIN_EMAIL = col[i].LOGIN_EMAIL;
					histo.UTILISATEUR = col[i].UTILISATEUR;
					
					historiqueRessource.addItem(histo);
				}					
			}
		}
		
		public function getActionsInventairesPossibles():void{
			// verifier que tous les produits selectionné soit dans le même état 
			var boolOk : Boolean = true;
			var idEtatRef : String = ressourcesSelectionnees[0].LIBELLE_ETAT;
			var len : Number = ressourcesSelectionnees.length;
			
			
					
			for(var i:Number = 0; i < len; i++){
				if(ressourcesSelectionnees[i].LIBELLE_ETAT != idEtatRef || ressourcesSelectionnees[i].EN_COURS > 0){
					 boolOk = false;
					 break;
				}
			}
			
			//si oui alors
			if (boolOk){
				if(ressourcesSelectionnees != null && ressourcesSelectionnees.length > 0){
					actionsRessourcesPossible = new ArrayCollection();
					var action : Action = new Action();
					if (ressourcesSelectionnees[0].DANS_INVENTAIRE){
						action.libelle = "Sortir de l'Inventaire";
						action.identifiant = SORTIR_INVENTAIRE;
					}else{
						action.libelle = "Rentrer dans Inventaire";
						action.identifiant = ENTRER_INVENTAIRE;
					}	
					action.changerEtatInventaire = true;
					actionsRessourcesPossible.addItem(action);					
				}else{
					actionsRessourcesPossible = null;
				}		
			}
			//sinon
			else{
				actionsRessourcesPossible = null;
			}
				
		}
		
		private function getActionsInventairesPossiblesResultHandler(re : ResultEvent):void{
			
		}
		
		
		
		//sortir la ressource de l'inventaire
		public function sortirDeInventaire(date : Date, comms : String = ""):void{
			var lastAction : int = ressourcesSelectionnees[0].IDINV_ACTIONS;								 
										 
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																				"doActionSurRessource",
																				sortirDeInventaireResultHandler);
			RemoteObjectUtil.callService(op,
										 ConsoviewUtil.extractIDs("IDINVENTAIRE_PRODUIT",ressourcesSelectionnees),
										 SORTIR_INVENTAIRE,
										 lastAction,
										 DateFunction.formatDateAsString(date),
										 comms,
										 1,
										 ressourcesSelectionnees[0].DANS_INVENTAIRE,
										 ressourcesSelectionnees[0].QTE)
			 
		}
		public function sortirDeInventaireResultHandler(re : ResultEvent):void{
			if(re.result > 0){			
				getHistoriqueInventaire();
				getListeInventaireSousTete();
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Problèmes : "+re.result+"","Erreur");
			}
		}
		
		
		//entrer les ressources dans l'inventaire
		public function entrerDansInventaire(date : Date, comms : String = ""):void{
			var lastAction : int = ressourcesSelectionnees[0].IDINV_ACTIONS;
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																				"doActionSurRessource",
																				entrerDansInventaireResultHandler);
			RemoteObjectUtil.callService(op,
										 ConsoviewUtil.extractIDs("IDINVENTAIRE_PRODUIT",ressourcesSelectionnees),
										 ENTRER_INVENTAIRE,
										 lastAction,
										 DateFunction.formatDateAsString(date),
										 comms,
										 1,
										 ressourcesSelectionnees[0].DANS_INVENTAIRE,
										 ressourcesSelectionnees[0].QTE)
			
		}
		
		public function entrerDansInventaireResultHandler(re : ResultEvent):void{
			if(re.result > 0){					
				getHistoriqueInventaire();
				getListeInventaireSousTete();
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Problèmes : "+re.result+"","Erreur");
			}
		}
		//--
		
		
		
		
		//Fournit l'historique des actions de racciordement sur une ligne
		public function getHistoriqueActionsRaccordement():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getHistoriqueActionsRaccordement",
																				getHistoriqueActionsRaccordementResultHandler);
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE);
		}
		public function getHistoriqueActionsRaccordementResultHandler(re : ResultEvent):void{
			if(re.result){					
				var col : ArrayCollection = re.result as ArrayCollection;
				var len : Number = col.length;
				
				historiqueActions = new ArrayCollection();
				
				for (var i : Number = 0; i <  len ; i++){
					var action : HistoriqueActionVo = new HistoriqueActionVo();			
					action.IDACTION = col[i].IDRACCO_HISTO;
					action.IDACTION_PREC = col[i].IDRACCO_HISTO_PREC; 
					action.OPNOM = col[i].OPNOM;
					action.OPERATEURID = col[i].OPERATEURID; 
					action.IDSOUS_TETE = ligne.IDSOUS_TETE;
					action.IDTYPE_RACCORDEMENT = col[i].IDTYPE_RACCORDEMENT;
					
					action.LIBELLE_ACTION = col[i].LIBELLE_ACTION;
					action.TYPE_RACCORDEMENT = col[i].LIBELLE_RACCORDEMENT;
					
					action.REFERENCE = col[i].REFERENCE;
					action.DATE_EFFET = col[i].DATE_EFFET;
					action.COMMENTAIRES = col[i].COMMENTAIRES;
					
					action.USER = col[i].NOM_COMPLET;
					
					historiqueActions.addItem(action);
				}					
			}
		}
		
		
		//Fournit des infos sur l'état d'une ligne	
		public function getInfosEtat():void{				
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getInfosEtat",
																				getInfosEtatResultHandler);
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE);
		}
		
		public function getInfosEtatResultHandler(re : ResultEvent):void{
			if(re.result){
				var col : ArrayCollection = re.result as ArrayCollection;
				var len : Number = col.length;
				if (len > 0){
					etatVo = new OngletEtatVo();
					etatVo.BOOL_PAYEUR = col[0].BOOL_PAYEUR;
					etatVo.BOOL_TITULAIRE = col[0].BOOL_TITULAIRE;
					etatVo.COMMENTAIRE_ETAT = col[0].COMMENTAIRE_ETAT;
					etatVo.COMPTE_FACTURATION_ACTUEL = col[0].COMPTE_FACTURATION;
					etatVo.IDCOMPTE_FACTURATION_ACTUEL = col[0].IDCOMPTE_FACTURATION;
					etatVo.IDENTIFIANT_AGENCE = col[0].IDENTIFIANT_AGENCE ;
					
					ligne.OPERATEURID_ACCES_CIBLE = (col[0].OPERATEURID_ACCES_CIBLE > 0)?col[0].OPERATEURID_ACCES_CIBLE:col[0].OPERATEURID_ACCES;
					ligne.OPERATEURID_ACCES = col[0].OPERATEURID_ACCES;
					
					etatVo.IDOPERATEUR_ACCES = (col[0].OPERATEURID_ACCES_CIBLE > 0)?col[0].OPERATEURID_ACCES_CIBLE:col[0].OPERATEURID_ACCES;
					etatVo.IDOPERATEUR_ACCES_FACTURANT = col[0].OPERATEURID_ACCES;
					
					etatVo.IDOPERATEUR_CREATEUR_NUMERO = col[0].OPERATEURID_CREATEUR_NUMERO;
					etatVo.IDSOUS_COMPTE_ACTUEL = col[0].IDSOUS_COMPTE;
					etatVo.IDSOUS_TETE = ligne.IDSOUS_TETE;
					etatVo.IDTYPE_RACCORDEMENT = ligne.IDTYPE_RACCORDEMENT;
					
					
					ligne.OPERATEUR_ACCES_CIBLE = (col[0].OPERATEURID_ACCES_CIBLE > 0)?col[0].OPERATEUR_ACCES_CIBLE:col[0].OPERATEUR_ACCES;
					ligne.OPERATEUR_ACCES = col[0].OPERATEUR_ACCES;
					
					etatVo.OPERATEUR_ACCES = (col[0].OPERATEURID_ACCES_CIBLE > 0)?col[0].OPERATEUR_ACCES_CIBLE:col[0].OPERATEUR_ACCES;
					etatVo.OPERATEUR_ACCES_FACTURANT = col[0].OPERATEUR_ACCES;
					
					etatVo.OPERATEUR_CREATEUR_NUMERO = col[0].OPERATEUR_CREATEUR_NUMERO;
					etatVo.SOUS_COMPTE_ACTUEL = col[0].SOUS_COMPTE;
					etatVo.TYPE_RACCORDEMENT = ligne.TYPE_RACCORDEMENT;
					etatVo.V3 = col[0].V3;
					etatVo.V4 = col[0].V4;
					
					
					var index : Number = ConsoviewUtil.getIndexById(helpers.listeTypeRaccordement,"IDTYPE_RACCORDEMENT",ligne.IDTYPE_RACCORDEMENT);
					var modeActuel : Object =  helpers.listeTypeRaccordement.getItemAt(index);
					trace("----------->  modeActuel" + ObjectUtil.toString(modeActuel));
					 
					if (modeActuel.BOOL_DIRECT == 1){
						boolDirect = true;	
					}else{
						boolDirect = false;
					}
					
					if(etatVo.IDOPERATEUR_ACCES == etatVo.IDOPERATEUR_ACCES_FACTURANT){
						boolWarningOpeAccess  = false;
					}else{
						boolWarningOpeAccess  = true;
					}
					
					
					switch(etatVo.IDTYPE_RACCORDEMENT){
						case DIRECT_VIA_PREFIXE:{
							MAX_OPERATEUR_INDIRECT = 1;
							break;
						}
						case PRESELECTION:{
							MAX_OPERATEUR_INDIRECT = 5;
							break;
						}
						default:{
							MAX_OPERATEUR_INDIRECT = 0;
							break;
						}
					}
					
					getActionsPossiblesByEtat();
				}				
			}
		}
		
		public function init():void{
			getInfosEtat();
			getHistoriqueActionsRaccordement();
			getListeInventaireSousTete();
			getlisteOperateurIndirect();
		}
		
		
		//Met à jour les infos sur l'etat d'un ligne
		public function udpateInfosEtat():void{
			
		
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"udpateInfosEtat",
																				udpateInfosEtatResultHandler);
			RemoteObjectUtil.callService(op,
										ligne.IDSOUS_TETE,
										etatVo.COMMENTAIRE_ETAT,
										etatVo.V3,
										etatVo.V4,
										etatVo.BOOL_PAYEUR,
										etatVo.BOOL_TITULAIRE,
										etatVo.IDOPERATEUR_CREATEUR_NUMERO,
										etatVo.IDENTIFIANT_AGENCE
										);
		}
		
		public function udpateInfosEtatResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				ConsoviewAlert.afficherOKImage("Champs mis à jour");
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Erreur lors de la sauvegarde","Erreur",Alert.OK);
			}
		}
		
		
		//Insere une action
		public function faireActionSurLigne():void{
			
			var listeRessourceASortir : Array = new Array();
			var listeRessourceAEntrer : Array = new Array();
			
			for (var i : Number = 0; i < ressourcesSelectionnees.length; i++){
				if((ressourcesSelectionnees[i].ENTRER == true) && (ressourcesSelectionnees[i].DANS_INVENTAIRE == 0) && (ressourcesSelectionnees[i].EN_COURS == 0)){
					listeRessourceAEntrer.push(ressourcesSelectionnees[i])
				}
				if((ressourcesSelectionnees[i].SORTIR == true) && (ressourcesSelectionnees[i].DANS_INVENTAIRE == 1) && (ressourcesSelectionnees[i].EN_COURS == 0)){
					listeRessourceASortir.push(ressourcesSelectionnees[i])
				}
			}
		
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"doActionRaccordement",
																				faireActionSurLigneResultHandler);
			
			RemoteObjectUtil.callService(op,
										ligne.IDSOUS_TETE,
										selectedActionRAcco.identifiant,
										(historiqueActions != null && historiqueActions.source.length > 0)?historiqueActions.source[0].IDACTION:-1,
										selectedModeRaccordement.IDTYPE_RACCORDEMENT,
										DateFunction.formatDateAsInverseString(selectedActionRAcco.dateAction),
										selectedActionRAcco.reference,
										selectedActionRAcco.commentaire,
										(selectedOperateursIndirects[0]!= null)?selectedOperateursIndirects[0].id :-1,
										ConsoviewUtil.extractIDs("IDINVENTAIRE_PRODUIT",listeRessourceASortir),
										ConsoviewUtil.extractIDs("IDINVENTAIRE_PRODUIT",listeRessourceAEntrer)										
										);	
		}
		public function faireActionSurLigneResultHandler(re : ResultEvent):void{
			Alert.okLabel = "Fermer";
			switch(Number(re.result)){
				case -1:{
					Alert.show("Erreur lors de la sauvegarde","Erreur",Alert.OK);
					break;
				}
				case 0:{
					Alert.show("Erreur lors de l'entrée et de la sortie des produits de l'inventaire : " + 0,"Erreur",Alert.OK);
					getHistoriqueActionsRaccordement();		
					getInfosEtat();		
					getListeInventaireSousTete();
					getlisteOperateurIndirect();
					ligne.TYPE_RACCORDEMENT = selectedModeRaccordement.LIBELLE_RACCORDEMENT;
					ligne.IDTYPE_RACCORDEMENT = selectedModeRaccordement.IDTYPE_RACCORDEMENT;
					break;
				}
				case 1:{
					Alert.show("Erreur lors de l'entrée des produits dans l'inventaire : " + 1,"Erreur",Alert.OK);
					getHistoriqueActionsRaccordement();		
					getInfosEtat();		
					getListeInventaireSousTete();
					getlisteOperateurIndirect();
					ligne.TYPE_RACCORDEMENT = selectedModeRaccordement.LIBELLE_RACCORDEMENT;
					ligne.IDTYPE_RACCORDEMENT = selectedModeRaccordement.IDTYPE_RACCORDEMENT;
					break;
				}
				case 2:{
					Alert.show("Erreur lors de la sortie des produits de l'inventaire : " + 2,"Erreur",Alert.OK);
					getHistoriqueActionsRaccordement();		
					getInfosEtat();		
					getListeInventaireSousTete();
					getlisteOperateurIndirect();
					ligne.TYPE_RACCORDEMENT = selectedModeRaccordement.LIBELLE_RACCORDEMENT;
					ligne.IDTYPE_RACCORDEMENT = selectedModeRaccordement.IDTYPE_RACCORDEMENT;
					break;
				}
				case 3:{
					getHistoriqueActionsRaccordement();		
					getInfosEtat();		
					getListeInventaireSousTete();
					getlisteOperateurIndirect();
					ligne.TYPE_RACCORDEMENT = selectedModeRaccordement.LIBELLE_RACCORDEMENT;
					ligne.IDTYPE_RACCORDEMENT = selectedModeRaccordement.IDTYPE_RACCORDEMENT;
					break;
				}
			}
		}
		
		//Insere une action
		public function getActionsPossiblesByEtat():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getActionsPossiblesByEtat",
																				getActionsPossiblesByEtatResultHandler);
			RemoteObjectUtil.callService(op,
										etatVo.IDTYPE_RACCORDEMENT);	
		}
		public function getActionsPossiblesByEtatResultHandler(re : ResultEvent):void{
			if (re.result){
				var col : ArrayCollection = re.result as ArrayCollection;
				var len : Number = col.length;
				listeActions = new ArrayCollection();
				for (var i : Number = 0 ; i < len ; i++){
					var act : Action = new Action();
					act.identifiant = re.result[i].IDRACCO_ACTION;
  					act.changerEtatInventaire = re.result[i].FLAG_ENTREE_INVENTAIRE;
  					act.libelle = re.result[i].LIBELLE_ACTION;
  					listeActions.addItem(act);
				}
			}	
		}
	}
}