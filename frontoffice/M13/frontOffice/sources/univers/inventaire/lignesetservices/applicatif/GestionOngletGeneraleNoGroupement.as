package univers.inventaire.lignesetservices.applicatif
{
	import univers.inventaire.lignesetservices.vo.Ligne;

	public class GestionOngletGeneraleNoGroupement extends GestionOngletGenerale
	{
		public function GestionOngletGeneraleNoGroupement(ligne:Ligne)
		{
			super(ligne);
		}
		override public function getInfosLigne():void{
			getDataOngletGenerale();
		}
	}
} 