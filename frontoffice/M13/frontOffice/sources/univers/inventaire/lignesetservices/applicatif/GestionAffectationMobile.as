package univers.inventaire.lignesetservices.applicatif
{
	import composants.mail.MailVO;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	import flash.utils.describeType;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.lignesetservices.vo.Ligne;
	import univers.inventaire.lignesetservices.vo.OngletAffectationVo;
	
	[Bindable]
	public class GestionAffectationMobile extends GestionAffectationStrategy
	{
		private var _envoyerMail : Boolean = false;
		public function get envoyerMail():Boolean{
			return _envoyerMail;
		}
		public function set envoyerMail(bool : Boolean):void{
			_envoyerMail = bool;
		}
		
		
		private var _serviceClientOperateur : Object;
		public function get serviceClientOperateur():Object{
			return _serviceClientOperateur;
		}
		public function set serviceClientOperateur(destObj : Object):void{
			_serviceClientOperateur = destObj;
		}
		
		private var _oldValue : Ligne;
		public function get oldValue():Ligne{
			return _oldValue;	
		}		
		public function set oldValue(lg :Ligne):void{
			_oldValue = lg;
		}
		
		
		
		
		public static const MAIL_ENVOYE : String = "mailEnvoyer";
		
		
		public function GestionAffectationMobile(myLigne : Ligne)
		{	
			super(myLigne);
		}
				
		
		override public function getInfosAffectation():void{			
			var op :AbstractOperation = 
						RemoteObjectUtil.
							getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
									"getInfosCollaborateurSIOP",
									getInfosAffectationResultHandler);
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE);
		}
		
		
		private function getInfosAffectationResultHandler(re :ResultEvent):void{
			dataAffectation = new OngletAffectationVo();
			
			
			if(re.result && (re.result as ArrayCollection).length > 0){
				var data : Object = re.result[0];
				dataAffectation.COLLABORATEURID = data.COLLABORATEURID;				
				dataAffectation.MAJ_AUTO_COLLABORATEUR = data.MAJ_AUTO_COLLABORATEUR;
				dataAffectation.NOM = data.NOM;
				dataAffectation.PRENOM = data.PRENOM;
			}
			
			ligne.COLLABORATEURID = dataAffectation.COLLABORATEURID;
			ligne.COMPTE = dataAffectation.COMPTE;
			if ((CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE) && (
			dataAffectation.MAJ_AUTO_COLLABORATEUR == 0)){
				ligne.MAJ_AUTO_COLLABORATEUR = 0;	
			}else{
				ligne.MAJ_AUTO_COLLABORATEUR = 1;
			}
			
			ligne.NOM =  dataAffectation.NOM;
			ligne.PRENOM = dataAffectation.PRENOM;
			dataAffectation.SI_OPERATEUR = true;
										
			dispatchEvent(new Event(GETAFFECTATION_COMPLETE));
		}
		
		
		override public function updateInfosAffectation(data : OngletAffectationVo):void{
			ligne.COMPTE = data.COMPTE;
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"updateCollaborateurSIOP",
																				updateAffectationResultHandler);																																																												
			RemoteObjectUtil.callService(op,data.COLLABORATEURID,data.NOM,data.PRENOM,ligne.IDSOUS_TETE);
		}
		
		
		protected function updateAffectationResultHandler(re : ResultEvent):void{
			if (re.result > 0){			
				
				ligne.NOM = String(re.token.message.body[1]);
				ligne.PRENOM =	String(re.token.message.body[2]);
				 
				ConsoviewAlert.afficherOKImage("Champs mis à jour");
				if (envoyerMail) envoyerUnMail();
			}else{
				Alert.show("Une erreur s'est produite lors de la mise à jour","Erreur Remoting");
			}
		}
		
		public function getInfosLigneCollaborateur():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getInfosCollaborateurSIOP",
																				getInfosLigneCollaborateurResultHandler);																																																												
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE);
		}
		
		private function getInfosLigneCollaborateurResultHandler(re : ResultEvent):void{
			if (re.result && re.result.length > 0){
				ligne.COLLABORATEURID = (re.result[0].COLLABORATEURID > 0)?re.result[0].COLLABORATEURID:0;
				ligne.NOM = re.result[0].NOM;
				ligne.PRENOM = re.result[0].PRENOM;
				ligne.MAJ_AUTO_COLLABORATEUR = re.result[0].MAJ_AUTO_COLLABORATEUR;
			}else{
				ligne.COLLABORATEURID = 0;
				ligne.NOM = " ";
				ligne.PRENOM = " ";
				ligne.MAJ_AUTO_COLLABORATEUR =0;
			}
		}
		
		
		
		override public function getListeSousComptes():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getListeSousComptes",
																				getListeCompteResultHandler);																																																												
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE);
		}
		
		
		protected function getListeCompteResultHandler(re : ResultEvent):void{
			if (re.result){			
				listeCompte = re.result	as ArrayCollection;
				listeCompte.refresh();
			}
		}
		
		
		
			
		private function doMapping(src : Object, dest : Object):void{
			var classInfo : XML = describeType(dest);		
            for each (var v:XML in classInfo..accessor) {
               if (src.hasOwnProperty(v.@name) && src[v.@name] != null){
               		dest[v.@name] = src[v.@name];
               }else{
              // 	trace("-----> echec mapping ["+v.@name+"] ou valeur null" );
               }
            }            				 		
		}
		
		private function envoyerUnMail():void{
			var _mail : MailVO = new	MailVO();		
			_mail.expediteur = CvAccessManager.getSession().USER.EMAIL;
			_mail.destinataire = serviceClientOperateur.EMAIL;
			_mail.module = "Gestion technique des lignes";			
			_mail.copiePourExpediteur = "YES";
			_mail.copiePourOperateur = "NO";//copie vers opérateur annuaire -> inutile ici			
			_mail.sujet = "Mis à jour du patronyme du collaborateur";
			_mail.message = messageToOperateur();

			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.parametres.perimetres.mail.MailSender",
								"envoyer",
								sendTheMailResultHandler);			
								
			RemoteObjectUtil.callService(op,_mail);		
		}
		
		//Envoyer mail result
		//param in
		//	 ResultEvent
		private function sendTheMailResultHandler(re : ResultEvent):void{	
			if (re.result > 0){
				dispatchEvent(new Event(MAIL_ENVOYE));
			}else{
				Alert.show("erreur " + re.result , "Erreur");
			}
		}
		
		private function messageToOperateur():String{
			var raison_sociale : String = CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE;
			var gestionnaire : String = CvAccessManager.getSession().USER.PRENOM + " " + CvAccessManager.getSession().USER.NOM;
		
			return 	"<p>Société : <b>"+raison_sociale+"</b></p>"					
					+"<br/>"
					+"<br/>"
					+"<p>Bonjour,</p>"	
					+"<br/>"				
					+"<p>Sur le compte client n°"+ligne.COMPTE +", je vous remercie de bien vouloir effectuer la modification sur la ligne mentionnée ci-dessous</p>"
					+"<p> - <b>"+ ConsoviewFormatter.formatPhoneNumber(ligne.SOUS_TETE) +"</b></p>" 
					+"<p> L'utilisateur n'est plus : </p>"
					+"<br/> Nom : <b>"+oldValue.NOM + "</b>"
					+"<br/> Prenom : <b>"+oldValue.PRENOM + "</b>"
					+"<br/>"				
					+"<p>mais</p>"
					+"<br/>"
					+"<br/> Nom : <b>"+ligne.NOM + "</b>"
					+"<br/> Prenom : <b>"+ligne.PRENOM + "</b>"
					+"<br/>"
					+"<br/>Merci de procéder à la mise en jour.</p>"	
					+"<br/>"
					+"<br/>"
					+"<p>Cordialement,</p>"
					+"<br/>"
					+"<b>"+gestionnaire+ ".</b>"
					+"<br>"
					+"<b>"+raison_sociale+"</b>";
		}
		
	}
}