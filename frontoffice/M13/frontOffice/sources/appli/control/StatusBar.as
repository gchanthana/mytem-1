package appli.control {
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.events.FlexEvent;
	
	public class StatusBar extends StatusBarIHM {		
		/* Read Icon */
	 	[Embed(source='/assets/images/lock.png',mimeType='image/png')]
		private var Imglock:Class;
		/* Write Icon */
	 	[Embed(source='/assets/images/lock_open.png',mimeType='image/png')]
		private var ImglockOpen:Class;
		
		public function StatusBar() {
			trace("(StatusBar) Instance Creation");
			this.addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
		}
		
		protected function afterCreationComplete(event:Event):void {
			trace("(StatusBar) Performing IHM Initialization");
			lblUser.text = ConsoViewModuleObject.versionAppli + " / Utilisateur : " + CvAccessManager.getSession().USER.PRENOM +
																	" " + CvAccessManager.getSession().USER.NOM;
			logoffBtn.addEventListener(MouseEvent.CLICK,logoff);
			logoffBtn.enabled = true;
		}

		private function logoff(event:MouseEvent):void {
			logoffBtn.enabled = false;
			lblUser.text = "";
			dispatchEvent(new AppControlEvent(AppControlEvent.LOGOFF_EVENT));
		}
		
		public function updatePerimetre():void {
			trace("(StatusBar) Updating Perimetre");
			setImage(CvAccessManager.getSession().CURRENT_ACCESS);
		}

		public function afterUniversFunctionUpdated(actionType:String):void {
			trace("(StatusBar) Performing After Univers Function Updated Tasks - " + actionType);
			setImage(CvAccessManager.getSession().CURRENT_ACCESS);
		}

		private function setImage(accessMode:int):void {
			switch(accessMode) {
				case ConsoViewSessionObject.READ_MODE :
					imgLock.source = Imglock;
					imgLock.toolTip="Mode Consultation";
					imgLock.visible = true;
					break;
					
				case ConsoViewSessionObject.WRITE_MODE :
					imgLock.source = ImglockOpen;
					imgLock.toolTip="Mode Modification";
					imgLock.visible = true;
					break;
					
				default :
					imgLock.visible = false;
					break;
			}
		}
		
		//==============================================================================================
		
		/*
		public function universChanged():void {
			setImage();
		}
		
		public function onUniversChanged():void {
			setImage();
		}

		private function changeStyle(ev : Event):void{
			ConsoViewStyleManager.changeStyle(cmbStyle.selectedItem.label);		
		}
		*/
	}
}
