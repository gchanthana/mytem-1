package service.fonction.FctListeClient
{
	import entity.ApplicationVO;
	import entity.ClientPartenaireVO;
	import entity.OffreVO;
	import entity.PartenaireVO;
	
	import event.fonction.fctListeClient.FctListeClientEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	public class FctListeClientDatas extends EventDispatcher
	{
		private var _listeClientContractuel:ArrayCollection	= new ArrayCollection();
		private var _listeApplication:ArrayCollection	= new ArrayCollection();;
		private var _listeOffre:ArrayCollection	= new ArrayCollection();
		private var _listePartenaire:ArrayCollection	= new ArrayCollection();;
		
		public function treatProcessGetListeClient(objResult:ArrayCollection):void
		{
			_listeClientContractuel.removeAll();
			
			for(var i:int = 0; i < objResult.length;i++)
			{
				var obj:ClientPartenaireVO = new ClientPartenaireVO();
				obj.fill(objResult[i]);
				_listeClientContractuel.addItem(obj);
			}
			this.dispatchEvent(new FctListeClientEvent(FctListeClientEvent.proc_LISTE_CLIENT_CONTRACTUEL_EVENT));
		}
		public function treatProcessGetData(objResult:Object):void
		{
			if(objResult.hasOwnProperty("LISTEAPPLICATION"))
			{
				setListeApplication(objResult.LISTEAPPLICATION);
			}
			if(objResult.hasOwnProperty("LISTEOFFRE"))
			{
				setListeOffre(objResult.LISTEOFFRE);
			}
			if(objResult.hasOwnProperty("LISTEPARTENAIRE"))
			{
				setListePartenaire(objResult.LISTEPARTENAIRE);
			}
			this.dispatchEvent(new FctListeClientEvent(FctListeClientEvent.proc_GETDATA_EVENT));
		}
		public function treatProcessgetListePartenaireOffreFromApplication(objResult:Object):void
		{
			if(objResult.hasOwnProperty("LISTEOFFRE"))
			{
				setListeOffre(objResult.LISTEOFFRE);
			}
			if(objResult.hasOwnProperty("LISTEPARTENAIRE"))
			{
				setListePartenaire(objResult.LISTEPARTENAIRE);
			}
			this.dispatchEvent(new FctListeClientEvent(FctListeClientEvent.proc_GETDATA_EVENT));
		}
		private function setListePartenaire(objResult:Object):void
		{
			_listePartenaire.removeAll();
			
			var parDefault:PartenaireVO = new PartenaireVO();
			parDefault.ID = 0;
			parDefault.NOM = ResourceManager.getInstance().getString("M28","Tous_les_partenaires_");
			_listePartenaire.addItemAt(parDefault,0);
			
			for(var k:int = 0; k < objResult.length;k++)
			{
				var part:PartenaireVO = new PartenaireVO();
				part.fill(objResult[k]);
				_listePartenaire.addItem(part);
			}
		}
		private function setListeOffre(objResult:Object):void
		{
			_listeOffre.removeAll();
			
			var offDefault:OffreVO = new OffreVO();
			offDefault.ID = 0;
			offDefault.LABEL = ResourceManager.getInstance().getString("M28","Toutes_les_offres_");
			_listeOffre.addItemAt(offDefault,0);
			
			for(var j:int = 0; j < objResult.length;j++)
			{
				var off:OffreVO = new OffreVO();
				off.fill(objResult[j]);
				_listeOffre.addItem(off);
			}
		}
		private function setListeApplication(objResult:Object):void
		{
			_listeApplication.removeAll();
			
			var appDefault:ApplicationVO = new ApplicationVO();
			appDefault.CODEAPP = 0;
			appDefault.LABEL = ResourceManager.getInstance().getString("M28","Toutes_les_applications");
			_listeApplication.addItemAt(appDefault,0);
			
			for(var i:int = 0; i < objResult.length;i++)
			{
				var app:ApplicationVO = new ApplicationVO();
				app.fill(objResult[i]);
				_listeApplication.addItem(app);
			}
		}
		
		public function get listeClientContractuel():ArrayCollection { return _listeClientContractuel; }
		public function set listeClientContractuel(value:ArrayCollection):void
		{
			if (_listeClientContractuel == value)
				return;
			_listeClientContractuel = value;
		}
		public function get listeApplication():ArrayCollection{ return _listeApplication; }
		public function set listeApplication(value:ArrayCollection):void
		{
			if (_listeApplication == value)
				return;
			_listeApplication = value;
		}
		public function get listeOffre():ArrayCollection { return _listeOffre; }
		public function set listeOffre(value:ArrayCollection):void
		{
			if (_listeOffre == value)
				return;
			_listeOffre = value;
		}
		public function get listePartenaire():ArrayCollection { return _listePartenaire; }
		public function set listePartenaire(value:ArrayCollection):void
		{
			if (_listePartenaire == value)
				return;
			_listePartenaire = value;
		}
	}
	
}