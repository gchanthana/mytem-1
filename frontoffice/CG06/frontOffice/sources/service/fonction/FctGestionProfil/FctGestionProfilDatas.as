package service.fonction.FctGestionProfil
{
	import entity.ClientVO;
	import entity.ProfilVO;
	
	import event.fonction.FctGestionProfil.FctGestionProfilEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Traite les données reçues aux retours des procedures appelées dans <code>FctGestionProfilServices</code>.
	 * 
	 * Version 	: 1.0
	 * Date 	: 06.05.2013
	 * </pre></p>
	 */
	public class FctGestionProfilDatas extends EventDispatcher
	{
		// VARIBLES ------------------------------------------------------------------------
		
		private var _listeClients	:ArrayCollection = new ArrayCollection;
		private var _listeProfils	:ArrayCollection = new ArrayCollection;
		
		// PUBLIC FUNCTIONS ----------------------------------------------------------------
		
		public function treatGetClientsParUtilisateur(retour:Object):void
		{
			_listeClients.source = new Array();
			var longResult:uint = (retour.LISTE_CLIENTS as ArrayCollection).length;
			for(var i:uint = 0; i<longResult; i ++)
			{
				var client	:ClientVO = new ClientVO;
				client.fill(retour.LISTE_CLIENTS[i]);
				_listeClients.addItem(client);
			}
			this.dispatchEvent(new FctGestionProfilEvent(FctGestionProfilEvent.LISTE_CLIENTS_EVENT));
			
		}
		
		public function treatGetProfilsParClients(ret:Object):void
		{
			_listeProfils.source = new Array();
			var longResult:uint = (ret.RESULT as ArrayCollection).length;
			for(var i:uint = 0; i<longResult; i++)
			{
				var profil:ProfilVO = new ProfilVO();
				profil.fill(ret.RESULT[i]);
				_listeProfils.addItem(profil);
			}
			
			this.dispatchEvent(new FctGestionProfilEvent(FctGestionProfilEvent.LISTE_PROFILS_EVENT));
		}
		
		public function treatCreerProfil(ret:Object):void
		{
			this.dispatchEvent(new FctGestionProfilEvent(FctGestionProfilEvent.CREATION_PROFIL_EVENT));
		}
		
		public function treatSupprimerProfil(ret:Object):void
		{
			this.dispatchEvent(new FctGestionProfilEvent(FctGestionProfilEvent.SUPPRESSION_PROFIL_EVENT));
			
		}
		
		public function treatModifierProfil(ret:Object):void
		{
			this.dispatchEvent(new FctGestionProfilEvent(FctGestionProfilEvent.MODIFICATION_PROFIL_EVENT));
			
		}
		
		public function treatListeFonctionUser(ret:Object):void
		{
			this.dispatchEvent(new FctGestionProfilEvent(FctGestionProfilEvent.LISTE_FCT_USER_EVENT));
		}
		
		public function treatListeFonctionProfil(ret:Object):void
		{
			this.dispatchEvent(new FctGestionProfilEvent(FctGestionProfilEvent.LISTE_FCT_PROFIL_EVENT));
		}
		
		
		// GETTERS et SETTERS -------------------------------------------------------------------------
		
		[Bindable]
		public function get listeClients():ArrayCollection
		{
			return _listeClients;
		}
		public function set listeClients(value:ArrayCollection):void
		{
			_listeClients = value;
		}
		
		[Bindable]
		public function get listeProfils():ArrayCollection
		{
			return _listeProfils;
		}
		
		public function set listeProfils(value:ArrayCollection):void
		{
			_listeProfils = value;
		}
		
	}
}