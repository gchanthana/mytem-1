package service.fonction.FctGestionCatalogue
{
	import mx.rpc.AbstractOperation;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class FctGestionCatalogueServices
	{
		[Bindable] public var myDatas 		: FctGestionCatalogueDatas;
		[Bindable] public var myHandlers 	: FctGestionCatalogueHandlers;
		
		public function FctGestionCatalogueServices()
		{
			myDatas 	= new FctGestionCatalogueDatas();
			myHandlers 	= new FctGestionCatalogueHandlers(myDatas);
		}
		public function getData():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M28.fct.fctGestionCatalogue", 
				"getData",
				myHandlers.getDataHandler);
			
			RemoteObjectUtil.callService(op);
		}
		public function getListeEqpIcecat(str:String,index:int,nbItem:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M28.fct.fctGestionCatalogue", 
				"listeEqpIcecat",
				myHandlers.getListeEqpIcecatHandler);
			
			RemoteObjectUtil.callService(op,str,index,nbItem);
		}
		public function getListeFournisseur(str:String,index:int,nbItem:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M28.fct.fctGestionCatalogue", 
				"listeEqpFournisseur",
				myHandlers.getListeEqpFournisseurHandler);
			
			RemoteObjectUtil.callService(op,str,index,nbItem);
		}
		public function giveAutorisation(lstEqpIcecat:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M28.fct.fctGestionCatalogue", 
				"giveAutorisation",
				myHandlers.giveAutorizationHandler);
			
			RemoteObjectUtil.callService(op);
		}
		public function injecterEqpIcecat(listeEqpIcecat:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M28.fct.fctGestionCatalogue", 
				"injecterEqp",
				myHandlers.injecterEqpIcecatHandler);
			
			RemoteObjectUtil.callService(op);
		}
		public function lierEqp(idEqpIcecat:int, idEqpCatFournisseur:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M28.fct.fctGestionCatalogue", 
				"lierEqp",
				myHandlers.lierEqpHandler);
			
			RemoteObjectUtil.callService(op);
		}
	}
}