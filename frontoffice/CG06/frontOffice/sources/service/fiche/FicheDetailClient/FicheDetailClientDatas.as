package service.fiche.FicheDetailClient
{
	import entity.Client;
	
	import event.fiche.ficheDetailClient.FicheDetailClientEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class FicheDetailClientDatas extends EventDispatcher
	{
		private var _client:Client;
		
		public function treatProcessGetDetailClient(objResult:ArrayCollection):void
		{
			_client = new Client();
			_client.fill(objResult.getItemAt(0),objResult); // en attendant la V2
//			_client.fill(objResult.getItemAt(0),objResult.getItemAt(1));  // pour la V2
			this.dispatchEvent(new FicheDetailClientEvent(FicheDetailClientEvent.proc_DETAIL_CLIENT_CONTRACTUEL_EVENT));
		}
		
		
		public function get client():Client { return _client; }
		public function set client(value:Client):void
		{
			if (_client == value)
				return;
			_client = value;
		}
	}
	
}