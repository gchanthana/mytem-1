package event.fonction.FctGestionProfil
{
	import flash.events.Event;

	public class FctGestionProfilEvent extends Event
	{
		public static const LISTE_CLIENTS_EVENT 		:String = "LISTE_CLIENTS_EVENT";
		public static const LISTE_PROFILS_EVENT 		:String = "LISTE_PROFILS_EVENT";
		public static const CREATION_PROFIL_EVENT 		:String = "CREATION_PROFIL_EVENT";
		public static const SUPPRESSION_PROFIL_EVENT	:String = "SUPPRESSION_PROFIL_EVENT";
		public static const MODIFICATION_PROFIL_EVENT	:String = "MODIFICATION_PROFIL_EVENT";
		public static const LISTE_FCT_USER_EVENT		:String = "LISTE_FCT_USER_EVENT";
		public static const LISTE_FCT_PROFIL_EVENT		:String = "LISTE_FCT_PROFIL_EVENT";
		
		public static const CLICK_DETAIL_PROFIL			:String = "CLICK_DETAIL_PROFIL";
		public static const CLICK_EDITION_PROFIL		:String = "CLICK_EDITION_PROFIL";
		public static const CLICK_DELETE_PROFIL			:String = "CLICK_DELETE_PROFIL";
		
		
		
		public function FctGestionProfilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}