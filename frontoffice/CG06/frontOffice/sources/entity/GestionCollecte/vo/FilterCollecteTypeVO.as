package entity.GestionCollecte.vo
{
    import entity.GestionCollecte.vo.SimpleEntityVO;
    
    import mx.resources.ResourceManager;
    /**
     *
     * Value Object permettant de <b>filtrer une liste </b>de collectes ou templates
     * en fonction d'un operateur,d'un type de données,d'une mode de récup
     * et de l'état de completion
     *
     */
    public class FilterCollecteTypeVO
    {
        private var _selectedDataTypeId:Number = 0;
        private var _selectedOperateurId:Number = 0;
        private var _selectedRecuperationModeId:Number = 0;
		private var _selectedInfosManquantesId:Number = 0;
		private var _selectedEtatCollectesId:Number = 2;
        private var _templateComplete:Number = 2;

        public function FilterCollecteTypeVO()
        {
        }

        public function toString():String
        {
            return ResourceManager.getInstance().getString('M28','FilterCollectypeVO_Recherche_de_Template') + ResourceManager.getInstance().getString('M28','Types_de_donn_es__') + selectedDataTypeId.toString() + "\n" + ResourceManager.getInstance().getString('M28','Op_rateur__') +
                selectedOperateurId.toString() + "\n" + ResourceManager.getInstance().getString('M28','Mode_de_r_cup__') + selectedRecuperationModeId.toString() + "\n";
        }

        public function get templateComplete():Number
        {
            return _templateComplete;
        }

        /**
         * Indique l'état de complétion des templates que l'on souhaite
         * retourner :
         *  0 -> template incomplets
         *  1 -> template complets
         *  2 -> tous les templates
         * @param value
         *
         */
        public function set templateComplete(value:Number):void
        {
            _templateComplete = value;
        }

        public function get selectedDataTypeId():Number
        {
            return _selectedDataTypeId;
        }

        /**
         * Identifiant du type de données ( Facturation , Usages ...)
         *
         */
        public function set selectedDataTypeId(value:Number):void
        {
            _selectedDataTypeId = value;
        }

        public function get selectedOperateurId():Number
        {
            return _selectedOperateurId;
        }

        /**
         * Identifiant de l'opérateur
         *
         */
        public function set selectedOperateurId(value:Number):void
        {
            _selectedOperateurId = value;
        }

        public function get selectedRecuperationModeId():Number
        {
            return _selectedRecuperationModeId;
        }

        /**
         * Identifiant du mode de récupération ( Pull-MAIL , PUSH-FTP etc ...)
         *
         */
        public function set selectedRecuperationModeId(value:Number):void
        {
            _selectedRecuperationModeId = value;
        }
		
		/**
		 * Identifiant des infos manquantes :
		 * 0 => Toutes les collectes
		 * 1 => Les collectes sans les identifiants nécéssaires
		 * 2 => Les collectes sans date d'ouverture de la plage d'import
		 * 3 => Les collectes sans la référence opérateur Id Client
		 */
		public function get selectedInfosManquantesId():Number
		{
			return _selectedInfosManquantesId;
		}
		public function set selectedInfosManquantesId(value:Number):void
		{
			_selectedInfosManquantesId = value;
		}
		/**
		 * Identifiant des infos manquantes :
		 * 0 => Toutes les collectes
		 * 1 => Les collectes inactives
		 * 2 => Les collectes actives
		 * 3 => Les collectes avec 1er import en retard
		 * 4 => Les collectes futures
		 */
		public function get selectedEtatCollectesId():Number
		{
			return _selectedEtatCollectesId;
		}
		public function set selectedEtatCollectesId(value:Number):void
		{
			_selectedEtatCollectesId = value;
		}
    }
}