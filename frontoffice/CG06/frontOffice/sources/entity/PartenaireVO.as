package entity
{
	[Bindable]
	public class PartenaireVO
	{
		private var _NOM					:String;
		private var _ADRESSE				:String;
		private var _ORIGINE				:int;
		private var _ID						:int;
		private var _objContrat				:ContratVO = new ContratVO();
		private var _objInterlocuteur		:InterlocuteurVO = new InterlocuteurVO();
		private var _objFormation			:FormationVO = new FormationVO();
		private var _isProspect				:int;
		private var _isActive				:Boolean;
				
		public function PartenaireVO()
		{
		}
		
		public function fill(obj:Object):Boolean
		{
			try
			{
				if(obj.hasOwnProperty("PARTENAIRE_ADRESSE"))
					this._ADRESSE = obj.PARTENAIRE_ADRESSE;
				if(obj.hasOwnProperty("PARTENAIRE_ORIGINE"))
					this._ORIGINE = obj.PARTENAIRE_ORIGINE;
				if(obj.hasOwnProperty("PARTENAIRE_NOM"))
					this._NOM = obj.PARTENAIRE_NOM;
				if(obj.hasOwnProperty("PARTENAIRE_ID"))
					this._ID = obj.PARTENAIRE_ID;
				if(obj.hasOwnProperty("PARTENAIREID"))
					this._ID = obj.PARTENAIREID;
				if(obj.hasOwnProperty("NOM"))
					this.NOM = obj.NOM;
				if(obj.hasOwnProperty("IS_ACTIVE"))
					this.isActive = (obj.IS_ACTIVE == 1)?true:false;
				
				this._objContrat.fill(obj);
				
				this._objInterlocuteur.fill(obj);
				
				this._objFormation.fill(obj);
				
				return true;
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet PartenaireVO erroné ");
				return false
			}
			return false
		}
		
		public function get isActive():Boolean { return _isActive; }
		
		public function set isActive(value:Boolean):void
		{
			if (_isActive == value)
				return;
			_isActive = value;
		}
		
		public function get isProspect():int { return _isProspect; }
		
		public function set isProspect(value:int):void
		{
			if (_isProspect == value)
				return;
			_isProspect = value;
		}
		
		public function get ADRESSE():String { return _ADRESSE; }
		public function set ADRESSE(value:String):void
		{
			if (_ADRESSE == value)
				return;
			_ADRESSE = value;
		}
		public function get ORIGINE():int { return _ORIGINE; }
		public function set ORIGINE(value:int):void
		{
			if (_ORIGINE == value)
				return;
			_ORIGINE = value;
		}
		public function get NOM():String { return _NOM; }
		public function set NOM(value:String):void
		{
			if (_NOM == value)
				return;
			_NOM = value;
		}
		public function get ID():int { return _ID; }
		public function set ID(value:int):void
		{
			if (_ID == value)
				return;
			_ID = value;
		}
		
		public function get objInterlocuteur():InterlocuteurVO { return _objInterlocuteur; }
		
		public function set objInterlocuteur(value:InterlocuteurVO):void
		{
			if (_objInterlocuteur == value)
				return;
			_objInterlocuteur = value;
		}
		
		public function get objContrat():ContratVO { return _objContrat; }
		
		public function set objContrat(value:ContratVO):void
		{
			if (_objContrat == value)
				return;
			_objContrat = value;
		}
		
		public function get objFormation():FormationVO { return _objFormation; }
		
		public function set objFormation(value:FormationVO):void
		{
			if (_objFormation == value)
				return;
			_objFormation = value;
		}
	}
}