package entity
{
	import mx.messaging.SubscriptionInfo;

	public class LangueVO
	{
		// VARIABLES PRIVEES
		private var _CODELANGUE			:String;
		private var _IDLANGUE			:int;
		private var _LABEL				:String;
		private var _SELECTED			:Boolean;
		private var _LANGUE_PRINCIPALE	:Boolean;
		private var _SHORT_CODE 		:String;
		private var _labelByCountry	:String;
		
		// FONCTIONS PUBLIQUES		
		/* */
		public function LangueVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("CODELANGUE"))
					this._CODELANGUE = obj.CODELANGUE;
				
				if(obj.hasOwnProperty("IDLANGUE"))
					this._IDLANGUE = obj.IDLANGUE;
				
				if(obj.hasOwnProperty("LABEL"))
					this._LABEL = obj.LABEL;
				
				if(obj.hasOwnProperty("SHORT_CODE"))
					this.SHORT_CODE = obj.SHORT_CODE ;
				
				if( (obj.hasOwnProperty("LABEL")) && (obj.hasOwnProperty("SHORT_CODE")) )
				{
					this.labelByCountry = this.LABEL + " (" + (this.CODELANGUE).substring(3,5) + ")";
				}
			
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet LangueVO erroné ");
			}

			
		}
		
		public function get CODELANGUE():String
		{
			return _CODELANGUE;
		}

		public function set CODELANGUE(value:String):void
		{
			_CODELANGUE = value;
		}

		public function get IDLANGUE():int
		{
			return _IDLANGUE;
		}

		public function set IDLANGUE(value:int):void
		{
			_IDLANGUE = value;
		}

		public function get LABEL():String
		{
			return _LABEL;
		}

		public function set LABEL(value:String):void
		{
			_LABEL = value;
		}

		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}

		public function set SELECTED(value:Boolean):void
		{
			_SELECTED = value;
		}

		public function get LANGUE_PRINCIPALE():Boolean
		{
			return _LANGUE_PRINCIPALE;
		}

		public function set LANGUE_PRINCIPALE(value:Boolean):void
		{
			_LANGUE_PRINCIPALE = value;
		}
		
		public function get SHORT_CODE ():String { return _SHORT_CODE ; }
		
		public function set SHORT_CODE (value:String):void
		{
			if (_SHORT_CODE  == value)
				return;
			_SHORT_CODE  = value;
		}
		
		public function get labelByCountry():String { return _labelByCountry; }
		
		public function set labelByCountry(value:String):void
		{
			if (_labelByCountry == value)
				return;
			_labelByCountry = value;
		}

	}
}