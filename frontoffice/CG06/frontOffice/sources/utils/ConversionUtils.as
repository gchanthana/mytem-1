package utils
{
    import composants.util.ConsoviewFormatter;
    import composants.util.DateFunction;
    
    import flash.utils.describeType;
    import flash.utils.getDefinitionByName;
    import flash.utils.getQualifiedClassName;
    
    import mx.controls.dataGridClasses.DataGridColumn;
    import mx.resources.ResourceManager;

    public class ConversionUtils
    {
        public function ConversionUtils()
        {
        }

        public static function formateDates(item:Object, column:DataGridColumn):String
        {
            var ladate:Date = new Date(item[column.dataField]);
            return DateFunction.formatDateAsString(ladate);
        }

        public static function formateDate(date:Date):String
        {
            if(date)
            {
                return DateFunction.formatDateAsString(date);
            }
            else
            {
                return "";
            }
        }

        public static function setPeriodString(d1:Date, d2:Date):String
        {
            return ResourceManager.getInstance().getString('M28', 'du_') + d1.toDateString() + ResourceManager.getInstance().getString('M28', '_au_') + d2.toTimeString();
        }

        public static function formateEuros(item:Object, column:DataGridColumn):String
        {
            return ConsoviewFormatter.formatEuroCurrency(item[column.dataField], 2);
        }

        public static function formateLigne(item:Object, column:DataGridColumn):String
        {
            return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);
        }

        public static function formatePourControle(item:Object, column:DataGridColumn):String
        {
            return (item[column.dataField] == true) ? ResourceManager.getInstance().getString('M28', 'OUI') : ResourceManager.getInstance().getString('M28', 'NON');
        }

        public static function formateNumber(item:Object, column:DataGridColumn):String
        {
            return ConsoviewFormatter.formatNumber(Number(item[column.dataField]), 2);
        }

        public static function getFileNameFromPath(path:String):String
        {
            var fileStr:String = "";
            if(path)
            {
                var strArr:Array = path.split("/");
                if(strArr.length > 1)
                {
                    fileStr = strArr[strArr.length - 1];
                }
            }
            return fileStr;
        }

        /**
         * Teste la validité d'une URL passée en paramètre
         */
        public static function urlIsValid(urlStr:String):Boolean
        {
            var regex:RegExp = /^http(s)?:\/\/((\d+\.\d+\.\d+\.\d+)|(([\w-]+\.)+([a-z,A-Z][\w-]*)))(:[1-9][0-9]*)?(\/([\w-.\/:%+@&=]+[\w- .\/?:%+@&=]*)?)?(#(.*))?$/i;
            return regex.test(urlStr);
        }

        // Clonage 
        public static function newSibling(sourceObj:Object):*
        {
            if(sourceObj)
            {
                var objSibling:*;
                try
                {
                    var classOfSourceObj:Class = getDefinitionByName(getQualifiedClassName(sourceObj)) as Class;
                    objSibling = new classOfSourceObj();
                }
                catch(e:Object)
                {
                }
                return objSibling;
            }
            return null;
        }

        public static function clone(source:Object):Object
        {
            var clone:Object;
            if(source)
            {
                clone = newSibling(source);
                if(clone)
                {
                    copyData(source, clone);
                }
            }
            return clone;
        }

        public static function copyData(source:Object, destination:Object):void
        {
            //copies data from commonly named properties and getter/setter pairs
            if((source) && (destination))
            {
                try
                {
                    var sourceInfo:XML = describeType(source);
                    var prop:XML;
                    for each(prop in sourceInfo.variable)
                    {
                        if(destination.hasOwnProperty(prop.@name))
                        {
                            destination[prop.@name] = source[prop.@name];
                        }
                    }
                    for each(prop in sourceInfo.accessor)
                    {
                        if(prop.@access == "readwrite")
                        {
                            if(destination.hasOwnProperty(prop.@name))
                            {
                                destination[prop.@name] = source[prop.@name];
                            }
                        }
                    }
                }
                catch(err:Object)
                {;
                }
            }
        }
    }
}