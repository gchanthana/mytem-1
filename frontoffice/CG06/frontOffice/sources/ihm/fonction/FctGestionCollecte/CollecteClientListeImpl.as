package ihm.fonction.FctGestionCollecte
{
    import composants.util.ConsoviewUtil;
    
    import entity.GestionCollecte.ModelLocator;
    import entity.GestionCollecte.StaticData;
    import entity.GestionCollecte.vo.CollecteClientVO;
    import entity.GestionCollecte.vo.FilterCollecteTypeVO;
    import entity.GestionCollecte.vo.OperateurVO;
    import entity.GestionCollecte.vo.SimpleEntityVO;
    
    import event.fonction.fctGestionCollecte.FctGestionCollecteEvent;
    
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.ui.Keyboard;
    
    import ihm.fonction.FctGestionCollecte.popup.PopupChooseRacineIHM;
    import ihm.fonction.FctGestionCollecte.popup.PopupCollecteClientFicheIHM;
    import ihm.fonction.FctGestionCollecte.popup.PopupCollecteClientFicheImpl;
    import ihm.fonction.FctGestionCollecte.popup.PopupTemplateCollecteFicheImpl;
    
    import mx.collections.ArrayCollection;
    import mx.containers.VBox;
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.ComboBox;
    import mx.controls.DataGrid;
    import mx.controls.Image;
    import mx.controls.TextInput;
    import mx.core.IToolTip;
    import mx.events.CloseEvent;
    import mx.events.DataGridEvent;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.resources.ResourceManager;
    import mx.utils.ObjectUtil;

    [Bindable]
    public class CollecteClientListeImpl extends VBox
    {
        // INDEX -----------------------
        public static const LIST_INDEX:int = 0;
        public static const FICHE_INDEX:int = 1;
        public var racineId:Number = 641;
		//Popups
		public var popupChooseRacine:PopupChooseRacineIHM;
        // IHM -------------------------
        public var i_dgListCollecteClient:DataGrid;
        public var i_vbSearch:VBox;
        public var i_cbxOperateur:ComboBox;
        public var i_cbxDataType:ComboBox;
        public var i_cbxRecupMode:ComboBox;
		public var i_cbxRechercheRapide:ComboBox;
		public var i_cbxVue:ComboBox;
        public var i_btnNewCollecte:Button;
        public var i_btnSearchCollectes:Button;
        public var i_btnExport:Button;
        public var i_txiIdUser:TextInput;
		public var i_txiSearchCollectes:TextInput;
        public var i_cbxRacineList:ComboBox;
        protected var imageEdit:Class;
        protected var imageNew:Class;
        protected var imageDelete:Class;
        public var currentToolTip:IToolTip;
		public var btImgChooseRacine:Image;
        // objets metier
        private var filterVO:FilterCollecteTypeVO;
        private var _collecteVOListeAll:ArrayCollection;
        private var _collecteVOListeFiltered:ArrayCollection;
        private var _racineList:ArrayCollection;
        public var dataLocator:ModelLocator;
        // gestion du grid
        private var col:String;
        private var colIndex:int;
        private var indexRacineCombo:Number = -1;

        public function CollecteClientListeImpl()
        {
            super();
        }

        public function init():void
        {
            this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
            imageEdit = StaticData.imgEdit;
            imageDelete = StaticData.imgDelete;
            imageNew = StaticData.imgNew;
			btImgChooseRacine.source="assets/images/view.png";
			btImgChooseRacine.buttonMode=true;
			btImgChooseRacine.mouseEnabled=true;
        }

        public function onCreationCompleteHandler(e:FlexEvent):void
        {
            this.removeEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
            dataLocator = ModelLocator.getInstance();
			dataLocator.initGlobalDataApplication();
            initDisplay();
            initData();
            initListeners();
        }

        private function initDisplay():void
        {

        }

        private function initListeners():void
        {
            // datagrid
            i_dgListCollecteClient.addEventListener(DataGridEvent.HEADER_RELEASE, i_dgListCollecteClient_headerRelease);
            // Module de recherche
            i_cbxDataType.addEventListener(Event.CHANGE, i_cbxDataType_changeHandler);
            i_cbxOperateur.addEventListener(Event.CHANGE, i_cbxOperateur_changeHandler);
            i_cbxRecupMode.addEventListener(Event.CHANGE, i_cbxRecupMode_changeHandler);
            i_cbxRacineList.addEventListener(Event.CHANGE, i_cbxRacineList_changeHandler);
			i_cbxRechercheRapide.addEventListener(Event.CHANGE, i_cbxRechercheRapide_changeHandler);
			i_cbxVue.addEventListener(Event.CHANGE, i_cbxVue_changeHandler);
            // Boutons
            i_btnNewCollecte.addEventListener(MouseEvent.CLICK, i_btnNewCollecte_clickHandler);
            //i_btnSearchCollectes.addEventListener(MouseEvent.CLICK, i_btnSearchCollectes_clickHandler);
            i_btnExport.addEventListener(MouseEvent.CLICK, i_btnExport_clickHandler);
            // Model de données
            dataLocator.addEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS, collecteList_loadHandler);
            dataLocator.addEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_ERROR, collecteList_loadHandler);
            dataLocator.addEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_SUCCESS, racineList_loadHandler);
            dataLocator.addEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_ERROR, racineList_loadHandler);
			//i_txiSearchCollectes.addEventListener(KeyboardEvent.KEY_DOWN, testKey);
			btImgChooseRacine.addEventListener(MouseEvent.CLICK, chooseRacine_clickHandler);
		}

        private function initData():void
        {
            // collecteVOListeAll = ModelLocator.getInstance().generateTestCCList();
            collecteVOListeFiltered = ModelLocator.getInstance().collecteClientList;
            // initialisation du filtre
            filterVO = new FilterCollecteTypeVO();
        }

        /**
         * Ouverture d'une Fiche popup de création d'une nouvelle collecte
         *
         */
        protected function i_btnNewCollecte_clickHandler(event:MouseEvent):void
        {
            if(i_cbxRacineList.selectedItem.IDRACINE == 0)
            {
                dataLocator.consalerte(this, ResourceManager.getInstance().getString('M28','Veuillez_selectionner_une_racine_sp_cifique_'), ResourceManager.getInstance().getString('M28','Creation_d_une_nouvelle_collecte'), StaticData.imgNoValid);
            }
            else
            {
                var popUp:PopupCollecteClientFicheIHM = new PopupCollecteClientFicheIHM();
                popUp.currentCollectVO = new CollecteClientVO();
                popUp.mode = PopupTemplateCollecteFicheImpl.MODE_CREATION;
                popUp.idRacine = racineId;
                PopUpManager.addPopUp(popUp, this, true);
                PopUpManager.centerPopUp(popUp);
                popUp.updateDisplay();
                popUp.addEventListener(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_SUCCESS, onCollecteCreateUpdate_resultHandler);
                popUp.addEventListener(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_ERROR, onCollecteCreateUpdate_resultHandler);
                popUp.addEventListener(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_CANCEL, onCollecteCreateUpdate_resultHandler);
            }
        }

		/**
		 * Ouverture de la popup de recherche de racine
		 **/
		protected function chooseRacine_clickHandler(event:MouseEvent):void
		{
			var racines:ArrayCollection = new ArrayCollection();
			racines = ModelLocator.getInstance().racineList;
			popupChooseRacine = new PopupChooseRacineIHM();
			popupChooseRacine.addEventListener("VALID_CHOIX_RACINE",getItemRacineHandler);
			popupChooseRacine.listeRacine = racines as ArrayCollection;
			
			PopUpManager.addPopUp(popupChooseRacine,this,true);
			PopUpManager.centerPopUp(popupChooseRacine);
		}
        protected function i_btnSearchCollectes_clickHandler(event:MouseEvent):void
        {
			/**
			 * Code de la barre de recherche ici
			 */
			var text:String = i_txiSearchCollectes.text;
			var racines:ArrayCollection = new ArrayCollection();
			racines = ModelLocator.getInstance().racineList;
			for (var i:Number = 0; i < racines.length; i++)
			{
				if (racines[i].RACINE.indexOf(text) == 0)
				{
					i_cbxRacineList.selectedIndex = i;
					i_cbxRacineList.dispatchEvent(new Event(Event.CHANGE));
					return;
				}
			}
			for (i = 0; i < racines.length; i++)
			{
				if (racines[i].RACINE.indexOf(text) != -1)
				{
					i_cbxRacineList.selectedIndex = i;
					i_cbxRacineList.dispatchEvent(new Event(Event.CHANGE));
					return;
				}
			}
			Alert.show(ResourceManager.getInstance().getString('M28','Racine_introuvable_'));
        }
		
		protected function getItemRacineHandler(e:Event):void
		{
			popupChooseRacine.removeEventListener("VALID_CHOIX_RACINE",getItemRacineHandler);
			i_cbxRacineList.selectedIndex = popupChooseRacine.itemCurrent.ID;
			i_cbxRacineList.dispatchEvent(new Event(Event.CHANGE));
		}
		
		protected function testKey(evt:KeyboardEvent):void
		{
			if (evt.keyCode == Keyboard.ENTER)
			{
				i_btnSearchCollectes.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
			}
		}
		
        /**
         * Recherche des collectes appartenant au client dont l'id est passé en référence
         *
         */
        protected function i_btnExport_clickHandler(event:MouseEvent):void
        {
            // à décommenter dès mise en place de l'export coté BI
            dataLocator.exportListeCollecteClient(racineId);
            //dataLocator.consalerte(this, "Export en cours d'implémentation", "Export de la liste des collectes", StaticData.imgTodo);
        }

        /**
         * Recherche des collectes appartenant au client dont l'id est passé en référence
         *
         */
        protected function i_cbxRacineList_changeHandler(event:Event):void
        {
            indexRacineCombo = i_cbxRacineList.selectedIndex;
            if(i_cbxRacineList.selectedIndex != -1)
            {
                racineId = new Number(i_cbxRacineList.selectedItem.IDRACINE);
                refreshCollecteList(racineId);
            }
        }

        protected function onCollecteCreateUpdate_resultHandler(event:FctGestionCollecteEvent):void
        {
            switch(FctGestionCollecteEvent(event).type)
            {
                case FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_ERROR:
                    Alert.show(ResourceManager.getInstance().getString('M28','Erreur_dans_la_mise___jour_de_la_collect'));
                    break;
                case FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_SUCCESS:
                    refreshCollecteList(racineId);
                    refreshRacineList();
                    break;
                case FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_CANCEL:
                    refreshCollecteList(racineId);
                    break;
            }
            PopupCollecteClientFicheIHM(FctGestionCollecteEvent(event).target).removeEventListener(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_SUCCESS, onCollecteCreateUpdate_resultHandler);
            PopupCollecteClientFicheIHM(FctGestionCollecteEvent(event).target).removeEventListener(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_ERROR, onCollecteCreateUpdate_resultHandler);
            PopupCollecteClientFicheIHM(FctGestionCollecteEvent(event).target).removeEventListener(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_CANCEL, onCollecteCreateUpdate_resultHandler);
        }

        /**
         *  Lance les appels distant pour récupérer la liste des collectes
         *  depuis la base ( utiles après une maj/ajout/supp d'une collecte)
         *  en fonction de l'id racine passée en ref
         * @return
         *
         */
        public function refreshCollecteList(idRacine:Number):void
        {
            dataLocator.feedClientCollecte(idRacine);
            dataLocator.addEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS, collecteList_loadHandler);
            dataLocator.addEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS, collecteList_loadHandler);
        }

        /**
         *  Lance les appels distant pour récupérer la liste des racines
         *  depuis la base ( utiles après une maj/ajout/supp d'une collecte)
         *  en fonction de l'id racine passée en ref
         * @return
         *
         */
        private function refreshRacineList():void
        {
            dataLocator.feedRacineList();
            dataLocator.addEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_SUCCESS, racineList_loadHandler);
            dataLocator.addEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_ERROR, racineList_loadHandler);
        }

        /**
         *
         *
         */
        public function onbtnEditSelectedCollecte_clickHandler(event:MouseEvent):void
        {
        	var localSelectedCollect:CollecteClientVO = i_dgListCollecteClient.selectedItem as CollecteClientVO;
            var popUp:PopupCollecteClientFicheIHM = new PopupCollecteClientFicheIHM();                        
            PopUpManager.addPopUp(popUp, this, true);
            PopUpManager.centerPopUp(popUp);
            popUp.mode = PopupCollecteClientFicheImpl.MODE_EDITION;
            popUp.idRacine = localSelectedCollect.idRacine;
            popUp.currentCollectVO = localSelectedCollect;
            popUp.addEventListener(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_SUCCESS, onCollecteCreateUpdate_resultHandler);
            popUp.addEventListener(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_ERROR, onCollecteCreateUpdate_resultHandler);
            popUp.addEventListener(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_CANCEL, onCollecteCreateUpdate_resultHandler);
            popUp.updateDisplay();
        }

        /**
         * Handler captant le succés du (re)chargement
         * de la liste des collectes depuis la base
         * Applique le filtre sur datagrid pour rafraichir la vue
         *
         */
        private function collecteList_loadHandler(event:FctGestionCollecteEvent):void
        {
            dataLocator.removeEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS, collecteList_loadHandler);
            dataLocator.removeEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_ERROR, collecteList_loadHandler);
            switch(FctGestionCollecteEvent(event).type)
            {
                case FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS:
                    break;
                case FctGestionCollecteEvent.COLLECTE_LIST_LOADED_ERROR:
                    break;
                default:
                    break;
            }
            if(dataLocator.collecteClientList.length == 0)
            {
                dataLocator.consalerte(this, ResourceManager.getInstance().getString('M28','Aucune_collecte_associ_e___la_racine_') + i_cbxRacineList.selectedItem.RACINE.toString(), "", StaticData.
                                       imgInfo32);
            }
            if(!filterVO)
            {
                filterVO = new FilterCollecteTypeVO();
            }
            filterDg(filterVO);
        }

        /**
         * Handler captant le succés du (re)chargement
         * de la liste des racines clientes
         *
         */
        private function racineList_loadHandler(event:FctGestionCollecteEvent):void
        {
            racineList = dataLocator.racineList;
            i_cbxRacineList.dataProvider = racineList;
            i_cbxRacineList.selectedIndex = indexRacineCombo;
            dataLocator.removeEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_SUCCESS, racineList_loadHandler);
            dataLocator.removeEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_ERROR, racineList_loadHandler);
        }

        /**
         * Capte le changement de selection du combo de filtre 'Type de données'.
         * Filtre le Datagrid en conséquence
         */
        protected function i_cbxDataType_changeHandler(event:Event):void
        {
            if((i_cbxDataType.selectedItem as SimpleEntityVO).value as Number != 0)
            {
                filterVO.selectedDataTypeId = SimpleEntityVO(i_cbxDataType.selectedItem).value as Number;
            }
            else
            {
                filterVO.selectedDataTypeId = 0;
            }
            filterDg(filterVO);
        }

        /**
         * Capte le changement de selection du combo de filtre 'Opérareur'.
         * Filtre le Datagrid en conséquence
         */
        protected function i_cbxOperateur_changeHandler(event:Event):void
        {
            if((i_cbxOperateur.selectedItem as OperateurVO).value as Number != 0)
            {
                filterVO.selectedOperateurId = OperateurVO(i_cbxOperateur.selectedItem).value as Number;
            }
            else
            {
                filterVO.selectedOperateurId = 0;
            }
            filterDg(filterVO);
        }

        /**
         * Capte le changement de selection du combo de filtre 'Mode de récupération'.
         * Filtre le Datagrid en conséquence
         */
        protected function i_cbxRecupMode_changeHandler(event:Event):void
        {
            if((i_cbxRecupMode.selectedItem as SimpleEntityVO).value as Number != 0)
            {
                filterVO.selectedRecuperationModeId = SimpleEntityVO(i_cbxRecupMode.selectedItem).value as Number;
            }
            else
            {
                filterVO.selectedRecuperationModeId = 0;
            }
            filterDg(filterVO);
        }
		
		/**
		 * Capte le changement de selection du combo de filtre 'Recherches rapides Collectes infos manquantes'.
		 * Filtre le Datagrid en conséquence
		 */
		protected function i_cbxRechercheRapide_changeHandler(event:Event):void
		{
			if(i_cbxRechercheRapide.selectedIndex != 0)
			{
				filterVO.selectedInfosManquantesId = i_cbxRechercheRapide.selectedIndex;
			}
			else
			{
				filterVO.selectedInfosManquantesId = 0;
			}
			filterDg(filterVO);
		}
		
		/**
		 * Capte le changement de selection du combo de filtre 'Recherches rapides Etat des Collectes'.
		 * Filtre le Datagrid en conséquence
		 */
		protected function i_cbxVue_changeHandler(event:Event):void
		{
			if(i_cbxVue.selectedIndex != 0)
			{
				filterVO.selectedEtatCollectesId = i_cbxVue.selectedIndex;
			}
			else
			{
				filterVO.selectedEtatCollectesId = 0;
			}
			filterDg(filterVO);
		}
		
        /**
         * Filtre en locale la collection de collectes peuplant le Datagrid
         * à partir des filtres Operateurs,Type de données et Mode de récupération.
         * @param filterVO
         */
        private function filterDg(filterVO:FilterCollecteTypeVO):void
        {
            collecteVOListeFiltered = new ArrayCollection();
            var collec:ArrayCollection = ModelLocator.getInstance().collecteClientList;
            // si la valeur du filtre est à null , retour de tous les champs
			var d:Date = new Date();
			var today:Number = d.getTime();
			var validFilter:Number = 1;
            for(var i:int = 0; i < collec.length; i++)
            {
                var vo:CollecteClientVO = collec.getItemAt(i) as CollecteClientVO;
                if((filterVO.selectedDataTypeId == 0 || vo.template.idcollectType == filterVO.selectedDataTypeId) && (filterVO.selectedOperateurId == 0 ||
                    vo.template.operateurId == filterVO.selectedOperateurId) && (filterVO.selectedRecuperationModeId == 0 || vo.template.idcollectMode ==
                    filterVO.selectedRecuperationModeId))
                {
					/**
					 * Premier filtre
					 * 1 => sans les identifiants nécéssaires
					 * 2 => sans date d'ouverture de la plage d'import (SUPPRIME)
					 * 3 => sans le ROIC (SUPPRIME)
					 * Deuxième filtre
					 * 1 => Collectes inactives
					 * 2 => Collectes actives
					 * 3 => Les collectes avec premier import en retard
					 * 4 => Les collectes futures
					 */
					if (filterVO.selectedInfosManquantesId == 1)
					{
						validFilter = 0;
						if (dataLocator.getRecupCollecteLabel(vo.template.idcollectMode) == "PULL-Robot" && vo.identifiant == null || dataLocator.getRecupCollecteLabel(vo.template.idcollectMode) == "PULL-Robot" && vo.mdp == null)
							validFilter = 1;
					}
					if (validFilter == 1)
					{
						switch(filterVO.selectedEtatCollectesId)
						{
							case 0:
							{//Tout
								collecteVOListeFiltered.addItem(vo);
								break;
							}
							case 1:
							{
								if (vo.isActif.value == 0)
									collecteVOListeFiltered.addItem(vo);
								break;
							}
							case 2:
							{
								if (vo.isActif.value == 1)
									collecteVOListeFiltered.addItem(vo);
								break;
							}
							case 3:
							{
								/**
								 * Pour les collectes avec premier import en retard, il est nécéssaire de faire évoluer l'application avant 
								 * (affichage du statut des collectes... à définir plus en détails lors d'une réunion)
								 */
								break;
							}
							case 4:
							{
								if (vo.plageImport.getTime() > today)
									collecteVOListeFiltered.addItem(vo);
								break;
							}
							default:
							{
								break;
							}
						}
					}
					

                }
            }
            i_dgListCollecteClient.dataProvider = collecteVOListeFiltered;
            (i_dgListCollecteClient.dataProvider as ArrayCollection).refresh();
        }

        /**
         * Capte le click sur le header du datagrid de la liste des collectes
         */
        private function i_dgListCollecteClient_headerRelease(event:DataGridEvent):void
        {
            colIndex = DataGridEvent(event).columnIndex;
            col = i_dgListCollecteClient.columns[colIndex].dataField;
        }

        /**
         * Retourne le libelle racine approprié peuplant la combobox :
         *   libelle_racine ( nbCollectes collectes)
         */
        public function labelRacineFunction(item:Object):String
        {
            return item.RACINE + "   -  " + item.NB_COLLECTES.toString() + ResourceManager.getInstance().getString('M28','_collecte_s_');
        }

        /**
         * Assure le Tri numérique d'une colonne :
         * <code> sortCompareFunction = "numericSortFunction"</code>
         */
        public function numericSortFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.numericCompare(itemA[col], itemB[col]);
        }

        /**
         * Assure le Tri d'item de type <code>Date</code> d'une colonne :
         * <code> sortCompareFunction = "dateSortFunction"</code>
         */
        public function dateSortFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.dateCompare(itemA[col] as Date, itemB[col] as Date);
        }

        /**
         * Assure le Tri alphabétique  d'une colonne :
         * <code> sortCompareFunction = "stringSortFunction"</code>
         */
        public function stringSortFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.stringCompare(itemA[col], itemB[col]);
        }

        /**
         * Assure le Tri alphabétique d'items comprenant un champs label d'une colonne :
         * <code> sortCompareFunction = "stringLabelSortFunction"</code>
         */
        public function stringLabelSortFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.stringCompare(itemA[col].label, itemB[col].label);
        }

        /**
         * Assure le Tri alphabétique des operateurs (referencé dans l'objet template
         * de la collecte  d'une colonne :
         * <code> sortCompareFunction = "stringTemplateOpSortFunction"</code>
         */
        public function stringTemplateOpSortFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.stringCompare(itemA[col].operateurNom, itemB[col].operateurNom);
        }

        /**
         * Assure le Tri numerique des types de données (id) (referencé dans l'objet template
         * de la collecte  d'une colonne :
         * <code> sortCompareFunction = "numericTemplateTypeSortFunction"</code>
         */
        public function numericTemplateTypeSortFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.numericCompare(itemA[col].idcollectType as Number, itemB[col].idcollectType as Number);
        }

        /**
         * Assure le Tri alphabétique des mode de récup (id) (referencé dans l'objet template
         * de la collecte  d'une colonne :
         * <code> sortCompareFunction = "numericTemplateModeSortFunction"</code>
         */
        public function numericTemplateModeSortFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.numericCompare(itemA[col].idcollectMode, itemB[col].idcollectMode);
        }

        public function get collecteVOListeAll():ArrayCollection
        {
            return _collecteVOListeAll;
        }

        public function set collecteVOListeAll(value:ArrayCollection):void
        {
            _collecteVOListeAll = value;
        }

        public function get collecteVOListeFiltered():ArrayCollection
        {
            return _collecteVOListeFiltered;
        }

        public function set collecteVOListeFiltered(value:ArrayCollection):void
        {
            _collecteVOListeFiltered = value;
        }

        public function get racineList():ArrayCollection
        {
            return _racineList;
        }

        public function set racineList(value:ArrayCollection):void
        {
            _racineList = value;
        }
    }
}