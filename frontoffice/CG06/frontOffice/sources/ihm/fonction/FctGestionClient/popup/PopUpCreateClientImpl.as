package ihm.fonction.FctGestionClient.popup
{
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class PopUpCreateClientImpl extends TitleWindowBounds
	{
		public function PopUpCreateClientImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		protected function init(event:FlexEvent):void
		{
			// TODO Auto-generated method stub
			
		}
		
		protected function btnValider_clickHandler(event:MouseEvent):void
		{
			if(validateEnteredInfosClient() == true)
			{
				ConsoviewAlert.afficherAlertConfirmation("Voulez-vous confirmer la création de client.", "Nouveau client", alertCreateContrat);
			}
		}
		
		private function validateEnteredInfosClient():Boolean
		{
			// TODO Auto Generated method stub
			return false;
		}
		
		private function alertCreateContrat(evt:CloseEvent):void
		{
			if (evt.detail == Alert.OK)
			{
				/*VAR NVCONTRAT:CONTRATVO = NEW CONTRATVO(); 
				NVCONTRAT.ID = 0;// 0 POUR UN NOUVEAU CONTRAT
				NVCONTRAT.OBJCERTIFICATION.ID = (COMBO_CERTIFICATIONS.SELECTEDITEM AS CERTIFICATIONVO).ID;
				NVCONTRAT.DATEDEBUT = DATEFIELD.STRINGTODATE(DF_DEBUTCONTRAT.TEXT, RESOURCEMANAGER.GETINSTANCE().GETSTRING('M28', '_DD_MM_YYYY_'));
				NVCONTRAT.DATEFIN = DATEFIELD.STRINGTODATE(DF_FINCONTRAT.TEXT, RESOURCEMANAGER.GETINSTANCE().GETSTRING('M28', '_DD_MM_YYYY_'));
				NVCONTRAT.OBJPLANTARIFAIRE.OBJTYPEPLANTARIF.ID = (COMBO_TYPEPLANTARIF.SELECTEDITEM AS TYPEPLANTARIFVO).ID;
				NVCONTRAT.OBJPLANTARIFAIRE.DATEDEBUT = DATEFIELD.STRINGTODATE(DF_DEBUTPT.TEXT, RESOURCEMANAGER.GETINSTANCE().GETSTRING('M28', '_DD_MM_YYYY_'));
				NVCONTRAT.OBJPLANTARIFAIRE.DATEFIN = DATEFIELD.STRINGTODATE(DF_FINPT.TEXT, RESOURCEMANAGER.GETINSTANCE().GETSTRING('M28', '_DD_MM_YYYY_'));
				
				THIS.ISERVICEGESTPARTENAIRE.AJOUTERCONTRAT(CVACCESSMANAGER.CURRENT_FUNCTION, CODEACTION.CREATE_CONTRAT, IDSELECTEDPARTN, NVCONTRAT);*/
				this.close_clickHandler(evt);
			}
		}
		
		protected function close_clickHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
			
		}
	}
}