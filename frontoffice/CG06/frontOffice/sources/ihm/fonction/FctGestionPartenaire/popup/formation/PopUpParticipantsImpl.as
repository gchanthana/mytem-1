package ihm.fonction.FctGestionPartenaire.popup.formation
{
	import composants.ui.TitleWindowBounds;
	
	import entity.CodeAction;
	import entity.FormationVO;
	import entity.ParticipantFormationVO;
	
	import event.fonction.fctGestionPartenaire.FctGestionPartenaireEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.CheckBox;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import service.fonction.FctGestionPartenaire.FctGestionPartenaireService;
	
	public class PopUpParticipantsImpl extends TitleWindowBounds
	{
		private var _iServiceGestPartenaire		:FctGestionPartenaireService;
		private var _selectedFormation			:FormationVO;
		private var _idSelectedPartn			:int;
		private var _nbSelectedNonParticipants	:int = 0;
		private var _nbSelectedParticipants		:int = 0;
		private var _dpParticipants				:ArrayCollection = new ArrayCollection();
		private var _dpNonParticipants			:ArrayCollection = new ArrayCollection();
		
		[Bindable]public var cbx_participants		:CheckBox;
		[Bindable]public var cbx_non_participants	:CheckBox;
		
		public function PopUpParticipantsImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		protected function init(event:FlexEvent):void
		{
			initListeners();
			initData();
		}
		
		private function initData():void
		{
			iServiceGestPartenaire.getListeParticipantsFormation(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_PARTICIPANTS, idSelectedPartn, selectedFormation.id);
		}
		
		private function initListeners():void
		{
			iServiceGestPartenaire.myDatas.addEventListener(FctGestionPartenaireEvent.LISTE_PARTICIPANTS_FORMATION, getInfosParticipants);
		}
		
		protected function getInfosParticipants(event:Event):void
		{
			var result:ArrayCollection = this.iServiceGestPartenaire.myDatas.listeParticipants;
			
			for(var i:int = 0; i< result.length; i++)
			{
				if((result[i] as ParticipantFormationVO).participated == 1)
					dpParticipants.addItem(result[i]);
				else
					dpNonParticipants.addItem(result[i]);
			}
		}
		
		protected function close_clickHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnValider_clickHandler(event:MouseEvent):void
		{
			var listeLoginID:String = "";
			
			for(var i:int = 0; i<dpParticipants.length; i++)
			{
				if(listeLoginID != "")
					listeLoginID = listeLoginID + "," + String((dpParticipants[i] as ParticipantFormationVO).id);
				else
					listeLoginID = String((dpParticipants[i] as ParticipantFormationVO).id);
			}
			
			iServiceGestPartenaire.ajouterEditerParticipantsFormation(CvAccessManager.CURRENT_FUNCTION, CodeAction.UPD_PARTICIPANTS_FORMA, selectedFormation, listeLoginID);
			this.close_clickHandler(event);
			
		}
		
		protected function onAjouterParticipantsClickHandler(event:MouseEvent):void
		{
			var suppIndices:Array =  new Array();
			
			for each (var npf:ParticipantFormationVO in dpNonParticipants) 
			{
				if(npf.SELECTED)
				{
					npf.SELECTED = false;
					dpParticipants.addItem(npf);
					suppIndices.push(dpNonParticipants.getItemIndex(npf));
				}
			}
			
			suppIndices.sort(Array.DESCENDING);
			
			for(var i:int=0 ; i < suppIndices.length; i++) 
			{
				dpNonParticipants.removeItemAt(suppIndices[i]);
			}
			
			nbSelectedNonParticipants = getSelectedItems(dpNonParticipants);
		}
		
		protected function onRetirerParticipantsClickHandler(event:MouseEvent):void
		{
			var suppIndices:Array =  new Array();
			
			for each (var npf:ParticipantFormationVO in dpParticipants) 
			{
				if(npf.SELECTED)
				{
					npf.SELECTED = false;
					dpNonParticipants.addItem(npf);
					suppIndices.push(dpParticipants.getItemIndex(npf));
				}
			}
			
			suppIndices.sort(Array.DESCENDING);
			
			for(var i:int=0 ; i < suppIndices.length; i++) 
			{
				dpParticipants.removeItemAt(suppIndices[i]);
			}
			
			nbSelectedParticipants = getSelectedItems(dpParticipants);
			
		}
		
		protected function onSelectAllNonParticipantsHandler(event:Event):void
		{
			var ns:int = 0;
			for(var i:int=0; i < dpNonParticipants.length; i++)
			{
				(dpNonParticipants[i] as ParticipantFormationVO).SELECTED = cbx_non_participants.selected;
				if((dpNonParticipants[i] as ParticipantFormationVO).SELECTED)
					ns++;
			}
			nbSelectedNonParticipants = ns;
			dpNonParticipants.refresh();
		}
		
		protected function onSelectAllParticipantsHandler(event:Event):void
		{
			var ns:int = 0;
			for(var i:int=0; i < dpParticipants.length; i++)
			{
				(dpParticipants[i] as ParticipantFormationVO).SELECTED = cbx_participants.selected;
				if((dpParticipants[i] as ParticipantFormationVO).SELECTED)
					ns++;
			}
			
			nbSelectedParticipants = ns;
			dpParticipants.refresh();
		}
		
		private function getSelectedItems(items:ArrayCollection):int
		{
			var nbrSelected:int = 0;
			var long:int = items.length;
			for(var i:int = 0; i< long; i++)
			{
				if((items[i] as ParticipantFormationVO).SELECTED)
					nbrSelected++;
			}
			return nbrSelected;
		}
		

		[Bindable]
		public function get selectedFormation():FormationVO { return _selectedFormation; }
		
		public function set selectedFormation(value:FormationVO):void
		{
			if (_selectedFormation == value)
				return;
			_selectedFormation = value;
		}
		
		public function get iServiceGestPartenaire():FctGestionPartenaireService { return _iServiceGestPartenaire; }
		
		public function set iServiceGestPartenaire(value:FctGestionPartenaireService):void
		{
			if (_iServiceGestPartenaire == value)
				return;
			_iServiceGestPartenaire = value;
		}
		
		public function get idSelectedPartn():int { return _idSelectedPartn; }
		
		public function set idSelectedPartn(value:int):void
		{
			if (_idSelectedPartn == value)
				return;
			_idSelectedPartn = value;
		}
		
		[Bindable]
		public function get dpNonParticipants():ArrayCollection { return _dpNonParticipants; }
		
		public function set dpNonParticipants(value:ArrayCollection):void
		{
			if (_dpNonParticipants == value)
				return;
			_dpNonParticipants = value;
		}
		
		[Bindable]
		public function get dpParticipants():ArrayCollection { return _dpParticipants; }
		
		public function set dpParticipants(value:ArrayCollection):void
		{
			if (_dpParticipants == value)
				return;
			_dpParticipants = value;
		}
		
		[Bindable]
		public function get nbSelectedParticipants():int { return _nbSelectedParticipants; }
		
		public function set nbSelectedParticipants(value:int):void
		{
			if (_nbSelectedParticipants == value)
				return;
			_nbSelectedParticipants = value;
		}
		
		[Bindable]
		public function get nbSelectedNonParticipants():int { return _nbSelectedNonParticipants; }
		
		public function set nbSelectedNonParticipants(value:int):void
		{
			if (_nbSelectedNonParticipants == value)
				return;
			_nbSelectedNonParticipants = value;
		}
		
	}
}