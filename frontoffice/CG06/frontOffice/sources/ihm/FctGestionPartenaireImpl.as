package ihm
{
	import entity.CodeAction;
	
	import event.FctGestionPartenaireEvent;
	
	import flash.events.Event;
	
	import ihm.composant.FicheListePartenairesIHM;
	import ihm.composant.FichePartenaireIHM;
	
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.events.FlexEvent;
	
	import service.FctGestionPartenaireService;
	
	import utils.abstract.AbstractFonction;
	
	public class FctGestionPartenaireImpl extends AbstractFonction
	{
		private var viewSWD					:FicheListePartenairesIHM;
		private var viewAMP					:FichePartenaireIHM;
		private var _iServiceGestPartenaire	:FctGestionPartenaireService;
		
		private var _niveauUtilisateur:Number;
		
		private var fiche:FichePartenaireIHM;
		
		public var vb_views:VBox;
		
		public var btn_viewSwd:Button;
		public var btn_viewAMP:Button;
		
		public function FctGestionPartenaireImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		private function init(event:FlexEvent):void
		{
			iServiceGestPartenaire = new FctGestionPartenaireService();
			
			// initialisdation de l'acces selon le niveau
			initAcces();
			
			initListeners();
			initDatas();
			
		}
		
		private function initDatas():void
		{
			//appel du service pour récupérer la liste des langues, les codes langues et labels langue
			this.iServiceGestPartenaire.getListeLangues(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_LANGUES_PARTN);
			
			// appel du service pour récupérer la liste des niveaux de certification
			this.iServiceGestPartenaire.getNiveauxCertification(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_CERTIFICATIONS);
			
			// appel du service pour récupérer la liste des civilités
			this.iServiceGestPartenaire.getListeCivilites(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_CIVILITES);
			
			// appel du service pour récupérer la liste des types de plan tarifaires
			this.iServiceGestPartenaire.getTypesPlanTarifaire(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_TYPES_PLAN_TARIF);
			
			// appel du service pour récupérer la liste des modules pour les formations
			iServiceGestPartenaire.getListeSujetsFormation(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_SUJETS_FORMATION);
			
			// appel du service pour récupérer la liste des lieux de formations
			iServiceGestPartenaire.getListeLieuxFormation(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_LIEUX_FORMATION);
		}
		
		private function initListeners():void
		{
			this.addEventListener(FctGestionPartenaireEvent.CLICK_DETAIL_PARTENAIRE,	clickDetailPartenaireHandler);
			this.addEventListener(FctGestionPartenaireEvent.MAJ_CONTRAT, 				refreshPartnHandler);
			
			this.iServiceGestPartenaire.myDatas.addEventListener(FctGestionPartenaireEvent.LISTE_LANGUES, 			getListeLanguesHandler);
			this.iServiceGestPartenaire.myDatas.addEventListener(FctGestionPartenaireEvent.LISTE_CIVILITES, 		getListeCivilitesHandler);
			this.iServiceGestPartenaire.myDatas.addEventListener(FctGestionPartenaireEvent.NIVEAUX_CERTIFICATION,	getListeCertificationsHandler);
			this.iServiceGestPartenaire.myDatas.addEventListener(FctGestionPartenaireEvent.TYPES_PLAN_TARIFAIRE,	getListeTypesPlanTarifaireHandler);
			this.iServiceGestPartenaire.myDatas.addEventListener(FctGestionPartenaireEvent.LISTE_SUJETS_FORMATION, 	getListeSujetsFormationHandler);
			this.iServiceGestPartenaire.myDatas.addEventListener(FctGestionPartenaireEvent.LISTE_LIEUX_FORMATION, 	getListeLieuxFormationHandler);
		}
		
		protected function refreshPartnHandler(event:Event):void
		{
			if(viewSWD != null)
			{
				viewSWD.refreshListeParn();
			}
			
		}
		
		protected function getListeLanguesHandler(event:Event):void
		{
//			trace("## listeLangues : " + ObjectUtil.toString(Constantes.listeLangues));
		}
		
		protected function getListeCertificationsHandler(event:Event):void
		{
//			trace("## listeCertifications : " + ObjectUtil.toString(Constantes.listeCertifications));
		}
		
		protected function getListeTypesPlanTarifaireHandler(event:Event):void
		{
//			trace("## listeTypesPlanTarifaire : " + ObjectUtil.toString(Constantes.listeTypesPlanTarifaire));
		}
		
		protected function getListeCivilitesHandler(event:Event):void
		{
//			trace("## listeCivilites : " + ObjectUtil.toString(Constantes.listeCivilites));
		}
		
		protected function getListeSujetsFormationHandler(event:Event):void
		{
//			trace("## listeSujetsFormation : " + ObjectUtil.toString(Constantes.listeSujetsFormation));
		}
		
		protected function getListeLieuxFormationHandler(event:Event):void
		{
//			trace("## listeLieuxFormation : " + ObjectUtil.toString(Constantes.listeLieuxFormation));
		}
		
		protected function clickDetailPartenaireHandler(evt:FctGestionPartenaireEvent):void
		{
			fiche = new FichePartenaireIHM();
			fiche.idSelectedPtn = evt.idPtn;
			fiche.nomSelectedPtn = evt.nomPtn;
			
			var tailleChildren:int = this.getChildren().length;
			if(tailleChildren == 2)
			{
				this.removeChildAt(1);
				this.addChild(fiche);
			} 
			else
			{
				this.addChild(fiche);
			}
		}
		
		private function initAcces():void
		{
			_niveauUtilisateur = Number(CvAccessManager.getSession().INFOS_DIVERS.CG.USER.IDNIVEAU);

			switch(_niveauUtilisateur)
			{
				case 1:
				{
					// Vue Saaswedo (SWD)
					viewSWD = new FicheListePartenairesIHM();
					
					// Fiche detail partenaire vide 
					viewAMP = new FichePartenaireIHM();
					viewAMP.idSelectedPtn = -1;
					viewAMP.nomSelectedPtn = '';
					
					this.addChild(viewSWD);
					this.addChild(viewAMP);
					
					break;
				}
					
				case 2:
				{
					// Vue Application Master ou Partenaire (AMP)
					viewAMP = new FichePartenaireIHM();
					viewAMP.idSelectedPtn = CvAccessManager.getSession().INFOS_DIVERS.CG.USER.IDPARTENAIRE;
					viewAMP.nomSelectedPtn = CvAccessManager.getSession().INFOS_DIVERS.CG.USER.NOM_PARTENAIRE;
					
					/*viewAMP.idSelectedPtn = 1;
					viewAMP.nomSelectedPtn = "SFR Business Team";*/
					
					this.addChild(viewAMP);					
					break;
				}
					
				default :
				{
					// Comportement à préciser : Quel message ?
					// un email ? deconnexion ?
					break;
				}
			}
		}// end initAcces
		
		public function get iServiceGestPartenaire():FctGestionPartenaireService { return _iServiceGestPartenaire; }
		
		public function set iServiceGestPartenaire(value:FctGestionPartenaireService):void
		{
			if (_iServiceGestPartenaire == value)
				return;
			_iServiceGestPartenaire = value;
		}
		
		
	}
}