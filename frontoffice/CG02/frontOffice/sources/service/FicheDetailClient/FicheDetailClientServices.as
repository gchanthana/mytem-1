package service.FicheDetailClient
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	import utils.abstract.AbstractRemoteService;

	public class FicheDetailClientServices extends AbstractRemoteService
	{
		[Bindable] public var myDatas 		: FicheDetailClientDatas;
		[Bindable] public var myHandlers 	: FicheDetailClientHandlers;
		
		public function FicheDetailClientServices()
		{
			myDatas 	= new FicheDetailClientDatas();
			myHandlers 	= new FicheDetailClientHandlers(myDatas);
		}
		public function getDetailClientContractuel(clientId:int):void
		{
			 var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				 "fr.consotel.consoview.M28.fiche.FctListeClient", 
				 "getDetailClientContractuel",
				 myHandlers.getDetailClientHandler);
			 
			 RemoteObjectUtil.callService(op,clientId);
		}
	}
}