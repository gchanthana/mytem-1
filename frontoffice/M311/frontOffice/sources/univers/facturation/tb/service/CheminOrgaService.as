package univers.facturation.tb.service
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class CheminOrgaService
	{
		private var _model:CheminOrgaModel;
		public var handler:CheminOrgaHandler;
		
		public function CheminOrgaService()
		{
			_model = new CheminOrgaModel();
			handler = new CheminOrgaHandler(_model);
		}
		
		public function get model():CheminOrgaModel
		{
			return _model;
		}
		
		public function set model(value:CheminOrgaModel):void
		{
			_model = value;
		}
		
		public function getCheminOrga(idSous_tete:String):void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M311.CheminLigne",
				"getCheminLigne",
				handler.getCheminOrga);
			
			RemoteObjectUtil.callService(op,idSous_tete);		
		}
		
	}
}