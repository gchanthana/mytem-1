package univers.facturation.tb.event
{
	import event.UserAccessModelEvent;
	
	import flash.events.Event;

	public class CheminOrgaEvent extends Event
	{
		public static const ORGA:String = "ORGA";
		
		public function CheminOrgaEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new UserAccessModelEvent(type,bubbles,cancelable);
		}
	}
}