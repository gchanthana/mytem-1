package services.ligne
{
	import event.LigneEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;

	public class LigneDatas extends EventDispatcher
	{
		private var _nbrLigneFixe:Number;
		private var _nbrLigneMobile:Number;
		private var _nbrLigneData:Number;

		public function LigneDatas()
		{
		}
		
		public function get nbrLigneData():Number
		{
			return _nbrLigneData;
		}

		public function set nbrLigneData(value:Number):void
		{
			_nbrLigneData = value;
		}

		public function get nbrLigneMobile():Number
		{
			return _nbrLigneMobile;
		}

		public function set nbrLigneMobile(value:Number):void
		{
			_nbrLigneMobile = value;
		}

		public function get nbrLigneFixe():Number
		{
			return _nbrLigneFixe;
		}

		public function set nbrLigneFixe(value:Number):void
		{
			_nbrLigneFixe = value;
		}

		public function treatNbreLigneParSegment(objResult:Object):void
		{
			nbrLigneFixe = Number(objResult.FIXE);
			nbrLigneMobile = Number(objResult.MOBILE);
			nbrLigneData = Number(objResult.DATA);
			
			this.dispatchEvent(new LigneEvent(LigneEvent.LIGNES_PAR_SEGMENT_RETURNED,true));
		}
		
	}
}