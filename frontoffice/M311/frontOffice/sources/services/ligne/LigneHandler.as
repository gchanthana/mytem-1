package services.ligne
{
	import composants.util.ConsoviewAlert;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;

	public class LigneHandler
	{
		private var _model:LigneDatas;
		
		public function LigneHandler(model:LigneDatas)
		{
			_model = model;
		}
		
		public function getNbreLigneParSegmentResultHandler(ev:ResultEvent):void
		{
			if(ev.result !=null){ 
				_model.treatNbreLigneParSegment(ev.result);
			}else{
				ConsoviewAlert.afficherError("##getNbreLigneParSegmentResultHandler:: Result is null", "Erreur");
			}
		}
		
		public function faultHandler(ev:FaultEvent):void
		{
			ConsoviewAlert.afficherError("##getNbreLigneParSegmentResultHandler:: Fault result error", "Erreur");
		}
		
	}
}