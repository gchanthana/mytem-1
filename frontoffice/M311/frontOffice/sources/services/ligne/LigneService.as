package services.ligne
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.remoting.RemoteObject;

	public class LigneService
	{
		public var  myDatas		:LigneDatas;
		public var myHandlers	:LigneHandler;
		
		public function LigneService()
		{
			myDatas	= new LigneDatas();
			myHandlers = new LigneHandler(myDatas);
		}
		
		public function getNbreLigneParSerment():void
		{
			var ligneRO:RemoteObject = new  RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
						
			ligneRO.source =  "fr.consotel.consoview.M311.LigneService";
			
//			ligneRO.getNbreLigneParSegment(moisDeb, moisFin);
			ligneRO.getNbreLigneParSegment();
			ligneRO.addEventListener("result", myHandlers.getNbreLigneParSegmentResultHandler);
			ligneRO.addEventListener("fault", myHandlers.faultHandler);
				
		}
		
	}
}