package appli.control {
	import mx.collections.ICollectionView;
	import mx.collections.XMLListCollection;
	import mx.controls.menuClasses.IMenuDataDescriptor;

	public class SimpleMenuDataDescriptor implements IMenuDataDescriptor {
		public function SimpleMenuDataDescriptor() {
			trace("(SimpleMenuDataDescriptor) Instance Creation");
		}

		public function isBranch(node:Object, model:Object=null):Boolean {
			return ((node as XML).children().length() > 0);
		}

		public function hasChildren(node:Object, model:Object=null):Boolean {
			return ((node as XML).children().length() > 0);
		}
		
		public function getGroupName(node:Object):String {
			return node.@groupName;
		}
		
		public function getChildren(node:Object, model:Object=null):ICollectionView {
			return new XMLListCollection((node as XML).children());
		}
		
		public function isEnabled(node:Object):Boolean {
			return (node.@USR > 0);
		}
		
		public function setEnabled(node:Object, value:Boolean):void {
			//Nothing to do here. Cannot change enabled attribute
		}
		
		public function isToggled(node:Object):Boolean {
			if(node.@toggled == "true")
				return true;
			else
				return false;
		}
		
		public function setToggled(node:Object, value:Boolean):void {
			// Nothing to do here. It is CtrlBar which updates @toggled value using dispatched MenuEvent object
		}
		
		public function getType(node:Object):String {
			return node.@type;
		}
		
		/*=============== Méthodes inutilisées mais obligatoire car on implémente une interface !!! ===============*/
		public function addChildAt(parent:Object, newChild:Object, index:int, model:Object=null):Boolean {
			return false;
		}
		
		public function removeChildAt(parent:Object, child:Object, index:int, model:Object=null):Boolean {
			return false;
		}
		
		public function getData(node:Object, model:Object=null):Object {
			return null;
		}
	}
}
