package appli.control {
	import mx.resources.ResourceManager;
	import appli.events.ConsoViewEvent;
	
	import flash.events.Event;
	
	import mx.events.FlexEvent;
	
	public class DisplayFactureBoxImpl extends DisplayFactureBox {
		private var periodStart:Date; // Borne DEBUT
		private var periodEnd:Date; // Borne FIN
		private var selectedDeb:Date; // Borne DEBUT
		private var selectedFin:Date; // Borne FIN
		private var zoneDate:Object = new Object(); // Pour le range des dates chooser
		private var periodInfos:Object = new Object(); // Infos sur les périodes envoyées dans les EVENT
		private static const monthNamesArray:Array =
			[ResourceManager.getInstance().getString('M311', 'Janvier'),ResourceManager.getInstance().getString('M311', 'F_vrier'),ResourceManager.getInstance().getString('M311', 'Mars'),ResourceManager.getInstance().getString('M311', 'Avril'),ResourceManager.getInstance().getString('M311', 'Mai'),ResourceManager.getInstance().getString('M311', 'Juin'),ResourceManager.getInstance().getString('M311', 'Juillet'),ResourceManager.getInstance().getString('M311', 'Ao_t'),ResourceManager.getInstance().getString('M311', 'Septembre'),ResourceManager.getInstance().getString('M311', 'Octobre'),ResourceManager.getInstance().getString('M311', 'Novembre'),ResourceManager.getInstance().getString('M311', 'D_cembre')];
		
		public function getSelectedDateDEB():Date {
			return dtDeb.selectedDate;
		}
		
		public function getSelectedDateFIN():Date {
			return dtFin.selectedDate;
		}
		
		public function DisplayFactureBoxImpl() {
			super();
			trace("(DisplayFactureBoxImpl) Instance Creation");
			periodStart = new Date();
			periodEnd = new Date();
			selectedDeb = new Date();
			selectedFin = new Date();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:FlexEvent):void {
			trace("(DisplayFactureBoxImpl) Perform IHM Initialization");
			afterPerimetreUpdated();
			//initDateDisplay();
		}
		
		private function initdate():void {
			// Date de Début :
			var valDateDebFact:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.dateFirstFacture.getTime();
			var valDateDebDisplay:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateDeb.getTime();
			periodStart = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateDeb; // On prend borne DEBUT
			periodEnd = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateFin; // On prend borne FIN
			// Limites des intervales :
			zoneDate.rangeStart = periodStart;
			zoneDate.rangeEnd = periodEnd;
			// Dates Choisies :
			selectedDeb = periodStart;
			selectedFin = periodEnd;
			if(this.initialized == true)
				initDateDisplay();
		}
		
		private function initDateDisplay():void {
			dtDeb.selectableRange = zoneDate;
			dtFin.selectableRange = zoneDate;
			dtDeb.minYear = periodStart.getFullYear();
			dtDeb.maxYear = periodEnd.getFullYear();
			dtFin.minYear = periodStart.getFullYear();
			dtFin.maxYear = periodEnd.getFullYear();
			
			dtDeb.selectedDate = selectedDeb;
			dtFin.selectedDate = selectedFin;
			
			dtDeb.addEventListener(Event.CHANGE,chooseDatedeb);
			dtFin.addEventListener(Event.CHANGE,chooseDatefin);
		}
		
		private function chooseDatedeb(event:Event):void {
			selectedDeb = dtDeb.selectedDate;
			periodInfos.PERIOD_DEB = dtDeb.selectedDate;
			trace("(DisplayFactureBoxImpl) Changing DATE_DEB : " + dtDeb.selectedDate.toLocaleDateString());
			//dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.FACTURE_DEB_CHANGED,periodInfos));
			dispatchEvent(new ConsoViewEvent(ConsoViewEvent.PERIOD_DATE_DEB_CHANGED));
		}
		
		private function chooseDatefin(event:Event):void {
			selectedFin = dtFin.selectedDate;
			periodInfos.PERIOD_FIN = dtFin.selectedDate;
			trace("(DisplayFactureBoxImpl) Changing DATE_FIN : " + dtFin.selectedDate.toLocaleDateString());
			//dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.FACTURE_FIN_CHANGED,periodInfos));
			dispatchEvent(new ConsoViewEvent(ConsoViewEvent.PERIOD_DATE_FIN_CHANGED));
		}
		
		public function afterPerimetreUpdated():void {
			trace("(DisplayFactureBoxImpl) Perform After Perimetre Updated Tasks");
			initdate();
		}

		public function getPeriodString():String {
			var periodString:String = "";
			periodString = ResourceManager.getInstance().getString('M311', 'Factures___') + selectedDeb.date.toString() + " " +
							monthNamesArray[selectedDeb.month] + " " +
							selectedDeb.fullYear.toString() + ResourceManager.getInstance().getString('M311', '___') +
							selectedFin.date.toString() + " " +
							monthNamesArray[selectedFin.month] + " " +
							selectedFin.fullYear.toString();
			return periodString;
		}
		
		/*
		public function closeBox(event:MouseEvent):void {
			var hasPeriodChanged:Boolean = false;
			if(dtDeb.selectedDate.getTime() != selectedDeb.getTime())
				hasPeriodChanged = true;
			if(dtFin.selectedDate.getTime() != selectedFin.getTime())
				hasPeriodChanged = true;
			if(hasPeriodChanged == true) {
				periodInfos.PERIOD_DEB = dtDeb.selectedDate;
				periodInfos.PERIOD_FIN = dtFin.selectedDate;
				dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.FACTURE_PERIOD_CHANGED,periodInfos));
			}
			PopUpManager.removePopUp(this);
		}
		*/
		
		/*
		public function logoff():void {
			trace("DisplayFactureBoxImpl logoff()");
		}
		*/
	}
}
