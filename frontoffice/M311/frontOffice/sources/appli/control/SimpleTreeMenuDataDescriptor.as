package appli.control {
	import mx.collections.ICollectionView;
	import mx.controls.treeClasses.ITreeDataDescriptor;
	import mx.collections.XMLListCollection;

	public class SimpleTreeMenuDataDescriptor implements ITreeDataDescriptor {
		public function SimpleTreeMenuDataDescriptor() {
			trace("(SimpleTreeMenuDataDescriptor) Instance Creation");
		}

		public function hasChildren(node:Object, model:Object=null):Boolean {
			var tmpNode:XML = node as XML;
			if((tmpNode.@groupName == "menuUniversGroup") && (tmpNode.children().length() > 0))
				return true;
			else
				return false;
		}
		
		public function isBranch(node:Object, model:Object=null):Boolean {
			var tmpNode:XML = node as XML;
			if((tmpNode.@groupName == "menuUniversGroup") && (tmpNode.children().length() > 0))
				return true;
			else
				return false;
		}
		
		public function getChildren(node:Object, model:Object=null):ICollectionView {
			return new XMLListCollection((node as XML).children());
		}
		
		// Méthodes inutilisées mais obligatoire car on implémente une interface !!!
		public function getData(node:Object, model:Object=null):Object {
			return null;
		}
		
		public function addChildAt(parent:Object, newChild:Object, index:int, model:Object=null):Boolean {
			return false;
		}
		
		public function removeChildAt(parent:Object, child:Object, index:int, model:Object=null):Boolean {
			return false;
		}
	}
}
