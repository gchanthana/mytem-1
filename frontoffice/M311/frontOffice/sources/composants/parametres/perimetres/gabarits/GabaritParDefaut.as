package composants.parametres.perimetres.gabarits
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewFormatter;
	
	public class GabaritParDefaut extends AbstractGabarit
	{
		public function GabaritParDefaut()
		{
			super();
		}
		
		override public function getGabarit(infoObject: Object):String{
			var listeLignes : String = printListeSousTete(infoObject.LIGNES);
			return	ResourceManager.getInstance().getString('M311', '_p_Soci_t_____b_')+infoObject.raison_sociale+"</b></p>"					
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M311', '_p_Bonjour___p_')	
					+"<br/>"				
					+ResourceManager.getInstance().getString('M311', '_p_Les_lignes_ci_dessous_font_l_objet_d_')
					+"<br/>"
					+ResourceManager.getInstance().getString('M311', 'Origine_des_lignes__br__')
					+ infoObject.INFOS_DEST[2][0].TYPE_NIVEAU + " : <b>" +infoObject.INFOS_DEST[2][0].LIBELLE_GROUPE_CLIENT  + ResourceManager.getInstance().getString('M311', '__b___Gestionnaire____b_') + infoObject.gestionnaire + "</b>. <br/>"
					+ResourceManager.getInstance().getString('M311', 'Destination__br__') 
					+ infoObject.INFOS_DEST[1][0].TYPE_NIVEAU + " : <b>" +infoObject.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT  + ResourceManager.getInstance().getString('M311', '__b___Gestionnaire____b_') + infoObject.gestionnaireCible + "</b>. <br/>"
					+"<br/>"
					+ listeLignes
					+"<br/>_______________________________________________________________________________________________"
					+ResourceManager.getInstance().getString('M311', '_br___font_color___FF1200___b_Message___')+ infoObject.gestionnaireCible +".</b></font>"
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M311', '_p_Merci_de_prendre_bonne_note_de_ce_tra')+ infoObject.INFOS_DEST[1][0].TYPE_NIVEAU + " " +infoObject.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT + ResourceManager.getInstance().getString('M311', '___b__par__b_') + infoObject.gestionnaire + "</b></p>"
					+ResourceManager.getInstance().getString('M311', '_p_En_cas_d_erreur_merci_de_prendre_cont')+ infoObject.gestionnaire + "</b> "+ infoObject.usermail + ".</p>"
					+"<br/>_______________________________________________________________________________________________"
					+ResourceManager.getInstance().getString('M311', '_br___font_color___FF1200___b_Message___')
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M311', '_p_1_____Merci_de_proc_der___la_correcti')+infoObject.raison_sociale+ResourceManager.getInstance().getString('M311', '__b__de_ces_lignes__si_renseign_____p_')	
					+ResourceManager.getInstance().getString('M311', '_p_2_____Merci_de_proc_der_au_changement')	
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M311', '_p_Cordialement___p_')
					+"<br/>"
					+"<b>"+infoObject.gestionnaire+ ".</b>"
					+"<br>"
					+"<b>"+infoObject.raison_sociale+"</b>";
		}
		
		override protected function printListeSousTete(liste : Array):String{
			var output : String = "<br/>";
			if (liste != null){
				for (var i:Number=0; i<liste.length;i++){
					var titulaire: String = "";
					if (liste[i].COMMENTAIRES != null && String(liste[i].COMMENTAIRES).length > 0) titulaire = " (" + liste[i].COMMENTAIRES + ")";
					output = output + liste[i].LIBELLE_TYPE_LIGNE + " : <b>"+ ConsoviewFormatter.formatPhoneNumber(liste[i].SOUS_TETE) + titulaire  + ResourceManager.getInstance().getString('M311', '__b__t_t_t_tcompte_actuel_______________') + ResourceManager.getInstance().getString('M311', 'compte_cible_________________br__');
				}
			}
			return output;
		}
		
	}
}