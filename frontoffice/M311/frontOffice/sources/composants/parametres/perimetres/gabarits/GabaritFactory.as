package composants.parametres.perimetres.gabarits
{
	import flash.utils.getDefinitionByName;
	
	public class GabaritFactory
	{
		static public function getGabarit(idGroupe : Number, infos : Object):String{
			
			Gabarit477;
			var gabarit : AbstractGabarit;
			
			try{
				var myClass : Class = getDefinitionByName("composants.parametres.perimetres.gabarits.Gabarit" + idGroupe.toString()) as Class;
				gabarit = new myClass();
				return gabarit.getGabarit(infos);
			}catch(e : Error){				
				gabarit = new GabaritParDefaut();
			}finally{
				return gabarit.getGabarit(infos);
			}
		}	
		
	}
}