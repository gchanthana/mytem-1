package composants.tb.rapports
{
	import flash.events.MouseEvent;
	
	import mx.events.FlexEvent;
	
	public class RapportCps extends RapportCps_IHM
	{
		public function RapportCps() 
		{
			//TODO: implement function
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		//initialisation
		private function init(fe : FlexEvent):void{
			myPdfLink.addEventListener(MouseEvent.CLICK,dispatchInfo);
			myExcelLink.addEventListener(MouseEvent.CLICK,dispatchInfo);
			myCsvLink.addEventListener(MouseEvent.CLICK,dispatchInfo);
			/*myFlashpaperLink.addEventListener(MouseEvent.CLICK,dispatchInfo);*/					
			enableExport();
		}
		
		//diffusion de l'evenement
		private function dispatchInfo(me : MouseEvent):void{
			var rapportEvent : RapportEvent = new RapportEvent("export");
			rapportEvent.format = me.currentTarget.toolTip;
			dispatchEvent(rapportEvent);
		}
		
		/**
		 *  Pour rendre inactif les boutons d'export
		 * */
		public function disableExport(excel:Boolean=false,pdf:Boolean=false,csv:Boolean=false):void{
			myExcelLink.visible = excel;
		/*myFlashpaperLink.enabled = false;*/
			myPdfLink.visible = pdf;
			myCsvLink.visible = csv;
		}
		
		/**
		 *  Pour rendre actif les boutons d'export
		 * */
		public function enableExport(excel:Boolean=true,pdf:Boolean=true,csv:Boolean=true):void{
			myExcelLink.visible = excel;
			/*myFlashpaperLink.enabled = true;*/
			myPdfLink.visible = pdf;
			myCsvLink.visible = csv;
		}
	}
}