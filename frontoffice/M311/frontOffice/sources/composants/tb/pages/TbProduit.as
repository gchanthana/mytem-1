package composants.tb.pages
{
    import composants.tb.graph.IGraphModel;
    import composants.tb.service.ExportService;
    import composants.tb.tableaux.ITableauModel;
    import composants.tb.tableaux.TableauAboFactoryStratey;
    import composants.tb.tableaux.TableauChangeEvent;
    import composants.tb.tableaux.TableauConsoFactoryStrategy;
    import composants.tb.tableaux.TableauFactory;
    
    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;
    import flash.net.navigateToURL;
    
    import mx.containers.Grid;
    import mx.controls.Alert;
    import mx.controls.DataGrid;
    import mx.core.UIComponent;
    import mx.events.DataGridEvent;
    import mx.events.FlexEvent;
    import mx.resources.ResourceManager;
    import mx.utils.ObjectUtil;

    public class TbProduit extends TbProduit_IHM implements IPage
    {
        private var _tf:TableauFactory = new TableauFactory();
        private const _pageType:String = "PRODUIT";
		private const _niveauProduit : Number = NiveauProduit.PRODUIT;
        private var _tb:ITableauModel;
        private var lastMoisFin:String = "x";
        private var lastMoisDebut:String = "x";
        private var moisDeb:String;
        private var moisFin:String;
        private var perimetre:String;
        private var modeSelection:String;
        private var _nextPageType:String;
        private var _pageLibelle:String;
        private var _mode:String;
        private var _idmode:int;
        private var _goToNextPage:Function;
        private var lastIdentifiant:String = "z";
        private var identifiant:String;
        private var _type:TableauChangeEvent;
        private var _total:Number;

        /**
         * Constructeur
         **/
        public function TbProduit(type:TableauChangeEvent = null)
        {
            super();
            _type = type;
            _mode = type.TYPE_THEME;
            _idmode = checkThemid(_mode);
            _pageLibelle = ResourceManager.getInstance().getString('M311', 'Produit___') + type.LIBELLE;
			_type.NIVEAU_PRODUIT=_niveauProduit;
            addEventListener(FlexEvent.CREATION_COMPLETE, init);
        }

        private function checkThemid(theme:String):int
        {
            var _idsegment:int = -1;
            var them:String = theme.toUpperCase();
            var THEME_ABO:String = ResourceManager.getInstance().getString('M311', 'Abonnements').toUpperCase();
            var THEME_CON:String = ResourceManager.getInstance().getString('M311', 'Consommations').toUpperCase();
            switch (them)
            {
                case THEME_ABO:
                    _idsegment = 1;
                    break;
                case THEME_CON:
                    _idsegment = 2;
                    break;
            }
            return _idsegment;
        }

        /**
         * Retourne le type de la page la suit
         * @return type le type de la page suivante
         * */
        public function get nextPageType():String
        {
            //TODO: implement function
            return _nextPageType;
        }

        /**
         * Retourne le type de la page la suit
         * @return type le type de la page suivante
         * */
        public function set nextPageType(type:String):void
        {
            _nextPageType = type;
        }

        public function get pageType():String
        {
            return _pageType;
        }

        public function clean():void
        {
            //TODO: implement function
        }

        public function getLibelle():String
        {
            //TODO: implement function
            return ResourceManager.getInstance().getString('M311', 'D_tail_du_produit');
        }

        public function getTotal():Number
        {
            //TODO: implement function
            return _total;
        }

        public function updateSegment(segment:String = ""):void
        {
        }

        public function goToNextPageHandler(f:Function):void
        {
            _goToNextPage = f;
        }

        public function update():void
        {
            if (lastMoisDebut.concat(lastMoisFin).concat(lastIdentifiant) != moisDeb.concat(moisFin).concat(identifiant))
            {
                _tb.update();
                lastMoisDebut = moisDeb;
                lastMoisFin = moisFin;
                lastIdentifiant = identifiant;
            }
        }

        /**
         * Met à jour la periode
         * */
        public function updatePeriode(moisDeb:String = null, moisFin:String = null):void
        {
            //TOTO: implement function
            this.moisDeb = moisDeb;
            this.moisFin = moisFin;
            _tb.updatePeriode(moisDeb, moisFin);
        }

        /**
         * Permet d'exporter un rapport flash,pdf ou excel
         * @param format Le format (flashpaper, excel ou pdf)
         * */
        public function exporterRapport(format:String, rSociale:String = ""):void
        {
            var variables:URLVariables = new URLVariables();
			
            if (perimetre.toLowerCase() == "soustete")
            {
                variables.numero = _type.IDENTIFIANT;
            }
            else
            {
                variables.numero = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            }
			
            variables.perimetre = perimetre;
            variables.raisonsociale = rSociale;
            variables.idproduit_client = _type.IDPRODUIT;
            variables.format = format;
            variables.tb = _type.SEGMENT;
            variables.datedeb = moisDeb;
            variables.datefin = moisFin;
			variables.PATH_PRDT = _type.LIBELLE + " : " + _type.OPERATEUR;
            variables.modeCalcul = modeSelection;
            variables.mode = _mode.toUpperCase();
			
			var serviceExport : ExportService = new ExportService();
			serviceExport.getExport(variables);
			
        }

        /**
         * Met à jour le perimtre et le modeSelection
         * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...)
         * @param modeSelection Le mode de selection(complet ou partiel)
         * */
        public function updatePerimetre(perimetre:String = null, modeSelection:String = null, idt:String = null):void
        {
            this.perimetre = perimetre;
            this.modeSelection = modeSelection;
            this.identifiant = idt
            _tb.updatePerimetre(perimetre, modeSelection);
        }

        private function setTabFactoryStrategy():void
        {
            //TODO: implement function
            switch (_idmode)
            {
                case 1:
                    _tf.setStrategy(new TableauAboFactoryStratey());
                    break;
                case 2:
                    _tf.setStrategy(new TableauConsoFactoryStrategy());
                    break;
                default:
                    _tf.setStrategy(new TableauAboFactoryStratey());
                    break;
            }
        }

        //initialisation de l'IHM
        private function init(fe:FlexEvent):void
        {
            setTabFactoryStrategy();
            _tb = _tf.createTableau(_type);
            //Affichage des elements		
            myTabContener.addChild(DisplayObject(_tb));
            //affectation des Handlers et des listeners			
            UIComponent(_tb).addEventListener("tableauChange", _goToNextPage);
        }
    }
}