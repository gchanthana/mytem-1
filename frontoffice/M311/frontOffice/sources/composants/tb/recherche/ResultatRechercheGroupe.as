package composants.tb.recherche
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Tree;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;	
	
	public class ResultatRechercheGroupe extends ResultatRechercheGroupe_IHM
	{
		private const SELECTABLE : int = 1;
		
		private var op : AbstractOperation;		
		
		//recherche
		private var dept:XMLList;//liste des noeuds	qui "match" la recherche
        private var _searchItemIndex : int = 0;  //l'index du noeud qui "match" la recherche dans l'arbre      
		
		//params pour le remoting		
		protected var  perimetre : String; // le périmetre de la recherche	
		private var _chaine : String; //  la chaine recherchée
		private var _datedebut : String; // la date de debut pou la recherche
		private var _datefin : String; //  la date de fin pour la recherche
		
			
		//le noeuds sur lequel on click 
		private var nodeId : int; //l'identifiant du noeud 
		private var nodeLibelle : String; //le libellé du noeud
		private var nodePerimetre : String;	//le type de perimetre pour le noeud
		private var nodeFlag : int; //pour voir si correspond à la recherche
		
		
		//la ligne sur laquelle on click (dans le datagrid)
		private var sousTete : String; //le numero
		private var sousTeteId : int; // l'identifiant
		
				
		[Bindable]
		private var datagridData : ArrayCollection = new ArrayCollection();// les donnees pour le DataGrid
		
		[Bindable]
		private var treeData : XMLList = new XMLList();//les donnees pour le Tree
		 
		
		/**
		 * Constructeur
		 * @param chaine La chaine recherchée
		 * @param datedebut La date de début de la periode sur laquelle on cherche
		 * @param datefin La date de fin de la periode
		 * */
		public function ResultatRechercheGroupe(chaine :  String , datedebut : String , datefin : String)
		{
			//TODO: implement function
			super();	
			perimetre = "Groupe";		
			_datedebut = datedebut;
			_datefin = datefin;			
			_chaine = chaine;
			addEventListener(FlexEvent.CREATION_COMPLETE,init);			
		}
		
		
		/**
		 * Retourne la chaine recherchée pour ce composant
		 * @return chaine La chaine recherchée
		 * */
		public function get chaine():String{
			return _chaine;
		}
		
		
		/**
		 * Interrompt les remotings en cours
		 * */ 
		public function clean():void{
			
		}
			
		//-------- Handlers ----------------------------------------
		//initialise la fenêtre
		private function init(fe : FlexEvent):void{			
			//listener
			addEventListener(Event.CLOSE,closeEventHandler);		
						
			//bt suivant
			myImgSuivant.addEventListener(MouseEvent.CLICK,allerAuNoeudSuivant);
			
			//bt precedant
			myImgPrecedant.addEventListener(MouseEvent.CLICK,allerAuNoeudPrecedant);
			
			btAfficherNoeud.addEventListener(MouseEvent.CLICK,afficherTbNoeud);
						 
			btAfficherNoeud.enabled = false;
			
			myImgPrecedant.enabled = true;
			myImgSuivant.enabled = true;
			
		
			
			myTree.dataProvider = treeData;
			myTree.addEventListener(Event.CHANGE,chargerListeSousTete);
			myTree.doubleClickEnabled = true;			
			myTree.addEventListener(MouseEvent.DOUBLE_CLICK,ddClickEventHandler);				
			
			cbFiltre.addEventListener(Event.CHANGE,filtrerLeDataGrid);				
			cbFiltre.selected = false;
			cbFiltre.enabled = false;
			 
			myGrid.addEventListener(Event.CHANGE,changeEventHandler);	
			DataGridColumn(myGrid.columns[0]).labelFunction = formatDataTip;		
			lancerLaRecherche();
		}
			
		//formatage du telepone dans le grid
		private function formatDataTip(item : Object, column : DataGridColumn):String
		{
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);		
		}   
		
		//gere le double click sur un noeud
		private function ddClickEventHandler(e : Event):void{
			 
			
			
			var selectedNode:Tree = e.target as Tree;
			var nodeType:int = nodeFlag;
			var nodeTypePerimetre : String = nodePerimetre;
			
			
			
			
			if(nodeType == SELECTABLE){
				var evt : Event = new Event(Event.CHANGE);	
				var clevt : CloseEvent = new CloseEvent(Event.CLOSE);			
				var meevt : MouseEvent = new MouseEvent(MouseEvent.CLICK);
				myTree.dispatchEvent(evt);
				dispatchEvent(clevt);
				btAfficherNoeud.dispatchEvent(meevt);	
			}
			
		}
		
		//gere le click sur le bouton "afficher noeud"
		private function afficherTbNoeud(me : MouseEvent):void{
			getSessionListePerimetreNodId(nodeId);				
		}
		
		//gere les click sur l'arbre
		private function chargerListeSousTete(e : Event):void{
			
			var selectedNode:Tree = e.target as Tree;
			var nodeType:int = parseInt(selectedNode.selectedItem.@FLAG,10);
			var nodeTypePerimetre : String = selectedNode.selectedItem.@TYPE_PERIMETRE;
			trace("nodeType= "+nodeType);
			trace("nodeTypePerimetre= "+nodeTypePerimetre );
			
			if(nodeType == SELECTABLE){			
				btAfficherNoeud.enabled = true;				
				myImgPrecedant.enabled = true;
				myImgSuivant.enabled = true;
				
				myTree.scrollToIndex(selectedNode.selectedIndex);			
				setLocalNodeParams(selectedNode.selectedItem);
				
				if (e.currentTarget.selectedItem.@FLAG == 1){
					_searchItemIndex = getNodeIndexInDept(parseInt(selectedNode.selectedItem.@IDGROUPE_CLIENT));
					formatLblNoeud();
				}
								
				getLinesFromNode(e.currentTarget.selectedItem.@LEVEL);
				
			}else{
				btAfficherNoeud.enabled = false;
			
				myImgPrecedant.enabled = false;
				myImgSuivant.enabled = false;
				
				myGrid.dataProvider = null; 
				unSetLocalNodeParams();
			}			
		}
		//retourne l'index du noeud (dans dept) du NODE_ID passé en parametre, retourne -1 en cas d'echec
		private function getNodeIndexInDept(id : int):int{
			for(var i:int = 0;i< dept.length();i++)
				if (dept[i].@IDGROUPE_CLIENT == id) return i;				
			return -1;
		}
		
		
		//met à jour les params pour mis a jour du tb
		private function setLocalNodeParams(node : Object):void{			
			
			nodeId = node.@IDGROUPE_CLIENT;
			nodeLibelle = node.@LABEL;
			nodePerimetre = node.@TYPE_PERIMETRE;			
			nodeFlag = node.@FLAG;		
		}
		
		//reset les parametre locale		
		private function unSetLocalNodeParams():void{
			
			nodeId = -1;
			nodeLibelle = "";
			nodePerimetre = "";	
			nodeFlag = -1;	
		}
		
		//tronc le texte s'il est trop long
		private function formateLabel(s : String, len : int):String{
			if (s.length > len)
				return s.substr(0,len - 5)+"...";
			else 
				return s;
		}
		
		
		//ferme le fenetre
		private function closeEventHandler(ev : Event):void{
			var eventObj : ResultatRechercheEvent = new ResultatRechercheEvent("fermeturePanelRecherche");
			eventObj.windowHandler = this;		
			dispatchEvent(new ResultatRechercheEvent("fermeturePanelRecherche"));
			clean();			
			PopUpManager.removePopUp(this);			
		}
		
		//gere le click sur une ligne du grid
		private function changeEventHandler(ev : Event):void{
			if (ev.currentTarget.selectedIndex != -1){
				var resultatEvent : ResultatRechercheEvent = new ResultatRechercheEvent("ResultatClickEvent");
				resultatEvent.ID = ev.currentTarget.selectedItem.IDSOUS_TETES	;
				sousTeteId = ev.currentTarget.selectedItem.IDSOUS_TETES;
				sousTete = ev.currentTarget.selectedItem.SOUS_TETE;
			
				resultatEvent.perimetreRecherche = "SousTete";	
				resultatEvent.libelle = ev.currentTarget.selectedItem.SOUS_TETE;			
				dispatchEvent(resultatEvent);
			 
			}
		}
		
		//gere le click sur le bouton "afficher Ligne"
		private function afficherTbLigne(me : MouseEvent):void{
			var resultatEvent : ResultatRechercheEvent = new ResultatRechercheEvent("ResultatClickEvent");
			
			resultatEvent.ID = sousTeteId.toString();	
			resultatEvent.perimetreRecherche = "SousTete";	
			resultatEvent.libelle = sousTete;  
							
			dispatchEvent(resultatEvent);  
		}
		
		//filtre le grid
		private function filtrerLeDataGrid(e : Event):void{
			datagridData.filterFunction = processfitrerLeDataGrid;
			datagridData.refresh();
			var sNbLigne :String = (Number(datagridData.length) > 1)?'Lignes':'Ligne' ;
			dcolSousTete.headerText = ResourceManager.getInstance().getString('M311', sNbLigne);
		}
	
		//filtre pour le grid des sous-tete
		private function processfitrerLeDataGrid(value : Object):Boolean{
			if (cbFiltre.selected){		
				if (String(value.SOUS_TETE.toLowerCase()).search(_chaine.toLowerCase()) != -1) {
					return (true);
				} else {
					return (false);
				}
			}else{
				return true;
			}
		}		
		
		//aller au noeud suivant
		private function allerAuNoeudSuivant(me :MouseEvent):void{
			selectNextNode();
			
		}
		
		//aller au noeud precedant
		private function allerAuNoeudPrecedant(me : MouseEvent):void{
			selectPreviousNode();
		}
		
		//ouvre l'arbre sur la premiere occurence trouvée
		private function expandTree():void{		
			selectNodeByAttribute("FLAG",_chaine);				
		}
		
		/* Select the next found node */
        private function selectNextNode():void{        
           _searchItemIndex++;           
           if(_searchItemIndex >= dept.length())
              	 _searchItemIndex = 0;
			
		   formatLblNoeud();       	
           myTree.selectedItem = dept[_searchItemIndex];
           myTree.dispatchEvent(new Event(Event.CHANGE)); 
        }
        
        /* Select the previous found node */
        private function selectPreviousNode():void{        
           _searchItemIndex--;           
           if(_searchItemIndex <  0 )
              	 _searchItemIndex = (dept.length()-1);

           formatLblNoeud();	
           myTree.selectedItem = dept[_searchItemIndex];
           myTree.dispatchEvent(new Event(Event.CHANGE)); 
        }
        
        private function formatLblNoeud():void{
        	 lblNbNoeud.text = "("+(_searchItemIndex+1).toString()+"/"+dept.length().toString()+")";
        }
		
		//selectione les noeuds qui "match" la paire (attribut,valeur) passée en parametre
		private function selectNodeByAttribute(attribute:String, value:String):void{           								
	    	dept = treeData[0].node.descendants().(@[attribute] == 1);	    		    	
	        if (dept[0] == null)			
	          	return ;
			
			title = dept.length().toString() + ResourceManager.getInstance().getString('M311', '_noeud_s__trouv__s___pour__') + _chaine +")";		
	        var pr:Object;
	        
			for (var obj : String in dept){
				pr = dept[obj];
					while ((pr = pr.parent()) != null)			
	          			myTree.expandItem(pr, true);
			}
	        
	        formatLblNoeud();	        
	       	myTree.selectedItem = dept[_searchItemIndex];    
	       	myTree.dispatchEvent(new Event(Event.CHANGE));   	
		}


		
		//-------------------Remotings -----------------------------------------------------------------
		private function lancerLaRecherche():void{
			op  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													"fr.consotel.consoview.M311.recherche.facade",
													"rechercher",
													lancerLaRechercheResultHandler);
													
			RemoteObjectUtil.callService(op,
										perimetre,
										CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,
										_chaine,
										_datedebut,
										_datefin
										);
		} 
		
		private function lancerLaRechercheResultHandler(re : ResultEvent):void{		
			trace(XMLList(re.result));	
			var len : Number = XMLList(re.result).length();
			if (len == 1 && XMLList(re.result).hasComplexContent()){
				treeData = XMLList(re.result);
				myTree.dataProvider = treeData;
				myTree.labelField = "@LABEL";	
				callLater(expandTree);			
			}else if (XMLList(re.result).children().hasComplexContent()){ // or len = 1
				treeData = XMLList(re.result);
				myTree.dataProvider = treeData;
				myTree.labelField = "@LABEL";	
				callLater(expandTree);			
			}else{
				title = ResourceManager.getInstance().getString('M311', 'Pas_de_r_sultat_pour___') + _chaine + " )";			
				height = 20;
				setStyle("roundedBottomCorners","true");
				
			}	

				
		}
		
		private function lancerLaRechercheFaultHandler(fe : FaultEvent):void
		{
			title = ResourceManager.getInstance().getString('M311', 'Pas_de_r_sultat_pour_') + _chaine;
			trace("Erreur de Remoting" + fe.message,fe.fault,fe.fault.faultDetail,fe.fault.faultString,fe.fault.getStackTrace());	
		}
					
		//------------------------------------
		//récupere les lignes du noeud selectioné							
		private function getLinesFromNode(level:String):void
		{
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.M311.recherche.facade",
												  "rechercherListeLigne",
												  getLinesFromNodeResultHandler,null);
												  
			RemoteObjectUtil.callService(op,perimetre,										
											nodeId,
											level,
											_chaine,
											_datedebut,
											_datefin); 
		}
		
		private function getLinesFromNodeResultHandler(re : ResultEvent):void{
			datagridData = re.result as ArrayCollection;
			myGrid.dataProvider = datagridData;
			var sNbLigne :String = (Number(datagridData.length) > 1)?'Lignes':'Ligne' ;
			dcolSousTete.headerText = ResourceManager.getInstance().getString('M311', sNbLigne);
			
			if (datagridData.length > 0) {
				cbFiltre.enabled = true;
				cbFiltre.selected = false;
				trace(datagridData.length.toString());				
			}
			else {
				cbFiltre.selected = false;
				cbFiltre.enabled = false;
			}
			
		}
		
		private function getLinesFromNodeFaultHandler(fe : FaultEvent):void{
			trace("error remoting");
		}
		//-----------------------------------transcode le nodeId entre session.rechercheTb et session.liste_perimetre_query-----------------------------------------------------------
		private function getSessionListePerimetreNodId(id : int):void{
			var resultatEvent : ResultatRechercheEvent = new ResultatRechercheEvent("ResultatClickEvent");			
			resultatEvent.ID = String(id);
			resultatEvent.perimetreRecherche = nodePerimetre;	
			resultatEvent.libelle = nodeLibelle; 							
			dispatchEvent(resultatEvent);  
		}
		
		
	}
}