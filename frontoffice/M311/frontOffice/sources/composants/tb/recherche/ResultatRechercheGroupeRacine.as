package composants.tb.recherche
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;	
	
	public class ResultatRechercheGroupeRacine extends ResultatRechercheGroupeRacine_IHM
	{
		private var op : AbstractOperation;		
	
		private var _chaine : String;
		private var _datedebut : String;
		private var _datefin : String;
		
		protected var  perimetre : String;
	
		private var lineSelected : Boolean; //une ligne a ete selectionnee
			
		private var sousTete : String;
		private var sousTeteId : int;
				
		[Bindable]
		private var datagridData : ArrayCollection = new ArrayCollection();// les donnees pour le DataGrid
									 
		public function ResultatRechercheGroupeRacine(chaine :  String , datedebut : String , datefin : String)
		{
			//TODO: implement function
			super();	
			perimetre = "Groupe";		
			_datedebut = datedebut;
			_datefin = datefin;			
			_chaine = chaine;
			addEventListener(FlexEvent.CREATION_COMPLETE,init);			
		}
		
		public function get chaine():String{
			return _chaine;
		}
		
		public function clean():void{
			
		}
			
		//-------- Handlers ----------------------------------------
		private function init(fe : FlexEvent):void{
			title = ResourceManager.getInstance().getString('M311', 'R_sultat_de_la_recherche_pour__') + _chaine +")";		
			//listener
			
			addEventListener(Event.CLOSE,closeEventHandler);	
				
			
			btFermer.addEventListener(MouseEvent.CLICK,closeEventHandler);
				
			txtFiltre.addEventListener(Event.CHANGE,filtrerLeDataGrid);				
			
			myGrid.addEventListener(Event.CHANGE,changeEventHandler);				
			
			DataGridColumn(myGrid.columns[0]).labelFunction = formatDataTip;		
			getLinesFromNode();
		}
			
		//formatage du telepone dans le grid
		private function formatDataTip(item : Object, column : DataGridColumn):String
		{
				return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);
		}
				
		//gere le click sur le bouton "afficher Ligne"
		private function afficherTbLigne(me : MouseEvent):void{
			var resultatEvent : ResultatRechercheEvent = new ResultatRechercheEvent("ResultatClickEvent");
			
			resultatEvent.ID = sousTeteId.toString();	
			resultatEvent.perimetreRecherche = "SOUSTETE";	
			resultatEvent.libelle = sousTete;  
							
			dispatchEvent(resultatEvent);  
		}
						
		//ferme le fenetre
		private function closeEventHandler(ev : Event):void{
			/* var eventObj : ResultatRechercheEvent = new ResultatRechercheEvent("fermeturePanelRecherche");
			eventObj.windowHandler = this;	 */		
			dispatchEvent(new ResultatRechercheEvent("fermeturePanelRecherche"));
			clean();			
			PopUpManager.removePopUp(this);			
		}
		
		
		
		
		//gere le click sur une ligne du grid
		private function changeEventHandler(ev : Event):void{
			
			if (ev.currentTarget.selectedIndex != -1){
				var resultatEvent : ResultatRechercheEvent = new ResultatRechercheEvent("ResultatClickEvent");
				resultatEvent.ID = ev.currentTarget.selectedItem.IDSOUS_TETE	;
				sousTeteId = ev.currentTarget.selectedItem.IDSOUS_TETE;
				sousTete = ev.currentTarget.selectedItem.SOUS_TETE;			
				resultatEvent.perimetreRecherche = "SousTete";	
				resultatEvent.libelle = ev.currentTarget.selectedItem.SOUS_TETE;			
				dispatchEvent(resultatEvent);	
					
			}
				
		}
		
		
		private function filtrerLeDataGrid(e : Event):void{
			datagridData.filterFunction = processfitrerLeDataGrid;
			datagridData.refresh();
			var sNbLigne :String = (Number(datagridData.length) > 1)?'Lignes':'Ligne' ;
			dcolSousTete.headerText = ResourceManager.getInstance().getString('M311', sNbLigne);
		}
	
		//filtre pour le grid des sous-tete
		private function processfitrerLeDataGrid(value : Object):Boolean{
		
				if (String(value.SOUS_TETE.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1) {
					return (true);
				} else {
					return (false);
				}
			
		}		
		
		
		
		
		//-------------------Remotings -----------------------------------------------------------------
										
		private function getLinesFromNode():void{
			
			
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.M311.recherche.facade",
												  "rechercherListeLigneGroupeRacine",
												  getLinesFromNodeResultHandler,
												  null);
												  
			RemoteObjectUtil.callService(op,perimetre,										
											CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,
											_chaine,
											_datedebut,
											_datefin); 
		}
		
		private function getLinesFromNodeResultHandler(re : ResultEvent):void{
			
			datagridData = re.result as ArrayCollection;
			var sNbLigne :String = (Number(datagridData.length) > 1)?'Lignes':'Ligne' ;
			dcolSousTete.headerText = ResourceManager.getInstance().getString('M311', sNbLigne);
			
			if (datagridData.length>0){
				myGrid.dataProvider = datagridData;						
				title = datagridData.length + ResourceManager.getInstance().getString('M311', '_r_sultat_s__pour__')+_chaine+")";
				
			}	
			else {
				this.height = 40;
				title = ResourceManager.getInstance().getString('M311', 'Aucun_r_sultat_trouv__pour__')+_chaine+")";
			}
		}
		
		private function getLinesFromNodeFaultHandler(fe : FaultEvent):void{
			trace("error remoting");
		}			
	}
}