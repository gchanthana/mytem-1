package composants.tb.ligne.event
{
	import event.UserAccessModelEvent;
	
	import flash.events.Event;

	public class FicheEvent extends Event
	{
		public static const FICHELIGNE:String = "FICHELIGNE";
		public static const FICHEINFO:String = "FICHEINFO";
		public static const FICHEPRODUIT:String = "FICHEPRODUIT";
		public static const FICHEEVOLUTION:String = "FICHEEVOLUTION";
		public static const FICHETICKET:String = "FICHETICKET";
		
		public function FicheEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new UserAccessModelEvent(type,bubbles,cancelable);
		}
		
	}
}