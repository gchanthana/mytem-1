package composants.tb.ligne.entity
{
	[Bindable]
	public class FicheTicketVO
	{
		
		public var PAYS_APPELANT:String = "";
		public var COUT_OP:Number = 0;
		public var DATE_APPEL:Date = null;
		public var ZONE_APPELEE:String = "";
		public var ZONE_APPELANT:String = "";
		public var APPELE:String = "";
		public var COUT_OP_REMISE:Number = 0;
		public var NBRECORD:Number = 0;
		public var UNITE_VOLUME:String = "";
		public var PAYS_APPELE:String = "";
		public var DUREE_FACTURE:Number = 0;
		public var ROWN:Number = 0;
		public var TYPE_APPEL:String = "";
		public var HEURE_APPEL:String = "";
		public var REMISE:Number = 0;
		public var MONTANT_REMISE:Number = 0;
		public var COUT_OP_TOTAL:Number = 0;
		public var MONTANT_REMISE_TOTAL:Number = 0;
		public var COUT_OP_REMISE_TOTAL:Number = 0;
		public var DUREE_FACTURE_TOTAL:Number = 0;
		
			
		public function FicheTicketVO()
		{
		}
		
		public function fill(value:Object):void
		{
			this.PAYS_APPELANT = (value.PAYS_APPELANT!=null) ? value.PAYS_APPELANT : '';
			this.COUT_OP = value.COUT_OP;
			this.DATE_APPEL = value.DATE_APPEL;
			this.ZONE_APPELEE = (value.ZONE_APPELEE) ? value.ZONE_APPELEE : '';
			this.ZONE_APPELANT = (value.ZONE_APPELANT) ? value.ZONE_APPELANT : '';
			this.APPELE = (value.APPELE) ? value.APPELE : '';
			this.COUT_OP_REMISE = value.COUT_OP_REMISE;
			this.NBRECORD = value.NBRECORD;
			this.UNITE_VOLUME = value.UNITE_VOLUME;
			this.PAYS_APPELE = (value.PAYS_APPELE) ? value.PAYS_APPELE : '';
			this.DUREE_FACTURE = value.DUREE_FACTUREE;
			this.ROWN = value.ROWN;
			this.TYPE_APPEL = value.TYPE_APPEL;
			this.HEURE_APPEL = value.HEURE_APPEL;
			this.REMISE = value.MONTANT_REMISE;
			this.MONTANT_REMISE = value.COUT_OP_REMISE;
			this.COUT_OP_TOTAL = value.COUT_OP_TOTAL;
			this.MONTANT_REMISE_TOTAL = value.MONTANT_REMISE_TOTAL;
			this.COUT_OP_REMISE_TOTAL = value.COUT_OP_REMISE_TOTAL;
			this.DUREE_FACTURE_TOTAL = value.DUREE_FACTURE_TOTAL;
		}
		
	}
}