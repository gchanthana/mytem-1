package composants.tb.ligne.entity
{
	[Bindable]
	public class FicheProduitVO
	{
		public var PRODUIT:String = "";
		public var THEME:String = "";
		public var TYPE:String = "";
		public var QUANTITE:Number = 0;
		public var NBAPPEL:Number = 0;
		public var VOLUME:Number = 0;
		public var UNITE:String = "";
		public var MONTANT:Number = 0;
		public var FACTURE:String = "";
		public var ORDRE_AFFICHAGE:Number = 0;
		public var IDTYPE:Number = 0;		

		public function FicheProduitVO()
		{
		}
		
		public function fill(value:Object):void
		{
			this.PRODUIT = value.LIBELLE_PRODUIT;
			this.THEME = value.THEME;
			this.TYPE = value.TYPE;
			this.IDTYPE = value.IDTYPE;
			this.QUANTITE = value.QTE;
			this.NBAPPEL = value.NOMBRE_APPEL;
			this.VOLUME = parseInt(value.VOLUME_MN);
			this.UNITE = value.UNITE_VOL_MN;
			this.MONTANT = value.MONTANT_LOC;
			this.FACTURE = value.NUMERO_FACTURE;
			this.ORDRE_AFFICHAGE = value.ORDRE_AFFICHAGE;

		}
	}
}