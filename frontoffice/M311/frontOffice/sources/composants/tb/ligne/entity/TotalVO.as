package composants.tb.ligne.entity
{
	[Bindable]
	public class TotalVO
	{
		
		public var QUANTITE:Number=0;
		public var NBAPPEL:Number=0;
		public var VOLUME:Number=0;
		public var MONTANT:Number=0;
		
		public function TotalVO()
		{
		}
		
	}
}