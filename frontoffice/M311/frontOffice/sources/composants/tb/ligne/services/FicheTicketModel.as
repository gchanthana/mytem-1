package composants.tb.ligne.services
{
	import composants.tb.ligne.entity.FicheTicketVO;
	import composants.tb.ligne.event.FicheEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class FicheTicketModel extends EventDispatcher
	{
		
		private var _nbTotalItem:Number = 0;
		private var _ficheTicket:ArrayCollection;
		
		public function FicheTicketModel()
		{
			_ficheTicket = new ArrayCollection();
		}

		public function get nbTotalItem():Number
		{
			return _nbTotalItem;
		}

		public function set nbTotalItem(value:Number):void
		{
			_nbTotalItem = value;
		}

		public function get ficheTicket():ArrayCollection
		{
			return _ficheTicket;
		}

		public function set ficheTicket(value:ArrayCollection):void
		{
			_ficheTicket = value;
		}
		
		internal function updateFicheTicket(value:ArrayCollection):void
		{		
			
			_ficheTicket = new ArrayCollection();
			
			if(value.length > 0){
				nbTotalItem = value[0].NBRECORD;
			}
			else{
				nbTotalItem = 0;
			}
			
			var temp:FicheTicketVO;
			for (var i:int=0; i < value.length; i++)
			{
				temp = new FicheTicketVO();
				temp.fill(value[i]);
				_ficheTicket.addItem(temp);
			}
			
			dispatchEvent(new FicheEvent(FicheEvent.FICHETICKET));
			
		}

	}
}