package composants.tb.ligne.services
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class FicheProduitService
	{
		private var _model:FicheProduitModel;
		public var handler:FicheProduitHandler;
		
		public function FicheProduitService()
		{
			_model = new FicheProduitModel();
			handler = new FicheProduitHandler(_model);
		}

		public function get model():FicheProduitModel
		{
			return _model;
		}

		public function set model(value:FicheProduitModel):void
		{
			_model = value;
		}
		
		public function getFicheProduit(idSous_tete:Number):void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M311.ficheligne.FicheLigne",
				"getProduit",
				handler.getFicheProduitResultHandler);
			
			RemoteObjectUtil.callService(op,idSous_tete);		
		}

	}
}