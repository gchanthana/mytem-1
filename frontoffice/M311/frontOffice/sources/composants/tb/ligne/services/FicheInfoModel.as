package composants.tb.ligne.services
{
	import composants.tb.ligne.entity.FicheInfoVO;
	import composants.tb.ligne.entity.OperateurVO;
	import composants.tb.ligne.event.FicheEvent;
	
	import flash.events.EventDispatcher;
	import flash.utils.describeType;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	public class FicheInfoModel extends EventDispatcher
	{
		
		private var _ficheInfo:FicheInfoVO;
		
		public function FicheInfoModel()
		{
			_ficheInfo = new FicheInfoVO();
		}
		
		public function get ficheInfo():FicheInfoVO
		{
			return _ficheInfo;
		}

		public function set ficheInfo(value:FicheInfoVO):void
		{
			_ficheInfo = value;
		}
		
		internal function updateFicheInfo(value:Array):void
		{		
			if(value.length!=0){
				doMapping(value[0][0]);
				doArring(value[1]);
			}
				
			dispatchEvent(new FicheEvent(FicheEvent.FICHEINFO));
		}
		

		
		private function doMapping(src : Object):void
		{
			var classInfo : XML = describeType(_ficheInfo);
			
			for each (var v:XML in classInfo..accessor) {
				if (src.hasOwnProperty(v.@name) && src[v.@name] != null){               		
					_ficheInfo[v.@name] = src[v.@name];
				}
			}            				 		
		}
		
		private function doArring(src : ArrayCollection):void
		{
			var operateur:OperateurVO;
			for(var i:int=0;i<src.length;i++)
			{
				operateur = new OperateurVO();
				operateur.fill(src.getItemAt(i)) 
				ficheInfo.ARRAY_OPERATEUR.addItem(operateur);
			}
		}

	}
}