package composants.tb.ligne.services
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	public class FicheEvolutionHandler
	{
		private var _model:FicheEvolutionModel;
		
		public function FicheEvolutionHandler(model:FicheEvolutionModel)
		{
			_model = model;
		}

		public function get model():FicheEvolutionModel
		{
			return _model;
		}

		public function set model(value:FicheEvolutionModel):void
		{
			_model = value;
		}
		
		internal function getFicheEvolutionResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateFicheEvolution(event.result as ArrayCollection);
			}
		}

	}
}