package composants.tb.ligne.services
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	public class FicheProduitHandler
	{
		private var _model:FicheProduitModel;
		
		public function FicheProduitHandler(model:FicheProduitModel)
		{
			_model = model;
		}
		
		public function get model():FicheProduitModel
		{
			return _model;
		}

		public function set model(value:FicheProduitModel):void
		{
			_model = value;
		}

		internal function getFicheProduitResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateFicheProduit(event.result as ArrayCollection);
			}
		}
	}
}