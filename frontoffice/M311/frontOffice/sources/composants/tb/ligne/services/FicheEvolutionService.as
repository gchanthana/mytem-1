package composants.tb.ligne.services
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class FicheEvolutionService
	{
		
		private var _model:FicheEvolutionModel;
		public var handler:FicheEvolutionHandler;
		
		public function FicheEvolutionService()
		{
			_model = new FicheEvolutionModel();
			handler = new FicheEvolutionHandler(_model);
		}
		
		public function get model():FicheEvolutionModel
		{
			return _model;
		}

		public function set model(value:FicheEvolutionModel):void
		{
			_model = value;
		}

		public function getFicheEvolution(idSous_tete:Number):void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M311.ficheligne.FicheLigne",
				"getEvolution",
				handler.getFicheEvolutionResultHandler);
			
			RemoteObjectUtil.callService(op,idSous_tete);		
		}
		
	}
}