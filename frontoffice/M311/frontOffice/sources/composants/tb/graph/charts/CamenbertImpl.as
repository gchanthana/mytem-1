package composants.tb.graph.charts
{
	import mx.collections.ICollectionView;
	import mx.containers.Canvas;

	[Bindable]
	public class CamenbertImpl extends Canvas
	{
		public function CamenbertImpl()
		{
			super();
		}
		
		//Pour la compatibilité
		public var selectedIndex:Number = -1;
		
		private var _dataProvider:ICollectionView;
		private var _field:String;
		private var _nameField:String;
		private var _dataTipsFunction:Function;
		private var _showDataTips:Boolean = false;
		private var _labelFunction:Function;
		
		
		public function set field(value:String):void
		{
			_field = value;
		}

		public function get field():String
		{
			return _field;
		}

		public function set nameField(value:String):void
		{
			_nameField = value;
		} 

		public function get nameField():String
		{
			return _nameField;
		}
		
		public function set dataProvider(value:ICollectionView):void
		{
			_dataProvider = value;
		}

		public function get dataProvider():ICollectionView
		{
			return _dataProvider;
		}
		
		

		public function set dataTipsFunction(value:Function):void
		{
			_dataTipsFunction = value;
		}

		public function get dataTipsFunction():Function
		{
			return _dataTipsFunction;
		}

		public function set showDataTips(value:Boolean):void
		{
			_showDataTips = value;
		}

		public function get showDataTips():Boolean
		{
			return _showDataTips;
		}

		public function set labelFunction(value:Function):void
		{
			_labelFunction = value;
		}

		public function get labelFunction():Function
		{
			return _labelFunction;
		}
	}
}