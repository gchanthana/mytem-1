package composants.tb.graph
{
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;

	public class GraphDureeConso extends PieGraphBase
	{
		private var op:AbstractOperation;

		[Embed(source="/assets/images/suivant.gif")]
		private var icnSuivant:Class;

		[Bindable]
		private var _dataProviders:ArrayCollection;

		[Bindable]
		private var _dataProviders2:ArrayCollection;

		private var _segment:String;
		private var _idsegment_theme:Number;

		public function GraphDureeConso(idsegment_theme:Number, segment:String="Complet")
		{
			super();
			setSegment(segment);
			_idsegment_theme=idsegment_theme;
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		override public function clean():void
		{
			graph.myPie.dataProvider=null;
			myGrid.dataProvider=null;

		}

		/**
		* Permet de passer les valeurs au composant.
		* @param d Une collection de tableau.
		**/
		override public function set dataProviders(d:ArrayCollection):void
		{
			_dataProviders=_formatterDataProvider(d);
			_dataProviders2=_formatterDataProviderPie(d);
			updateProviders();
		}

		/**
		*  Retourne le dataproviders
		**/
		[Bindable]
		override public function get dataProviders():ArrayCollection
		{
			return _dataProviders;
		}

		/**
		 * Met à jour les donées du tableau
		 **/
		override public function update():void
		{
			if (_segment.toUpperCase() == "COMPLET")
			{
				chargerDonnees();
			}
			else
			{
				chargerDonneesBySegment();
			}

		}

		protected override function _formatterDataProvider(d:ArrayCollection):ArrayCollection
		{
			var tmpCollection:ArrayCollection=new ArrayCollection();
			var ligne:Object;

			_total=0;
			for (ligne in d)
			{
				tmpCollection.addItem(formateObject(d[ligne]));
			}

			var totalObject:Object=new Object();
			totalObject[ResourceManager.getInstance().getString('M311', 'Op_rateur')]='TOTAL';
			totalObject[ResourceManager.getInstance().getString('M311', 'Temps_Consomm___min_')]=ConsoviewFormatter.formatNumber(_total, 2);
			tmpCollection.addItem(totalObject);

			return tmpCollection;
		}

		protected function _formatterDataProviderPie(d:ArrayCollection):ArrayCollection
		{
			var tmpCollection:ArrayCollection=new ArrayCollection();
			var ligne:Object;
			var len:int=d.length;
			var obj:Object=new Object();
			var chartData:ArrayCollection=new ArrayCollection();

			_total=0;

			for (ligne in d)
			{
				tmpCollection.addItem(formateObjectPie(d[ligne]));
			}
			chartData=ObjectUtil.copy(tmpCollection) as ArrayCollection;

			//On coupe s'il y a trop de données
			if (len > 10)
			{
				var sum:Number=0;

				for (var i:int=9; i < len; i++)
				{
					obj=chartData.getItemAt(i);
					sum=sum + parseFloat(obj[ResourceManager.getInstance().getString('M311', 'Montant')]);
				}

				var len2:int=chartData.length;

				for (var j:int=10; j < len2; j++)
				{
					chartData.removeItemAt(10);
				}

				chartData.getItemAt(9)[ResourceManager.getInstance().getString('M311', 'Libell_')]=ResourceManager.getInstance().getString('M311', 'Autres___');
				chartData.getItemAt(9)[ResourceManager.getInstance().getString('M311', 'Montant')]=sum;

				return chartData;
			}

			if (_total == 0)
			{
				return null;
			}
			return tmpCollection;
		}

		protected function formateObject(obj:Object):Object
		{
			var o:Object=new Object();

			_total=_total + parseFloat(obj.duree_appel);

			o[ResourceManager.getInstance().getString('M311', 'Op_rateur')]=obj.nom;
			o[ResourceManager.getInstance().getString('M311', 'Temps_Consomm___min_')]=ConsoviewFormatter.formatNumber(obj.duree_appel, 2);
			return o;
		}

		protected function formateObjectPie(obj:Object):Object
		{
			var o:Object=new Object();

			_total=_total + parseFloat(obj.duree_appel);

			o[ResourceManager.getInstance().getString('M311', 'Libell_')]=obj.nom;
			o[ResourceManager.getInstance().getString('M311', 'Montant')]=obj.duree_appel;
			return o;
		}

		// Chargement des données par remoting
		protected override function chargerDonnees():void
		{
			op=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M311.accueil.facade",
				"getRepartDuree", chargerDonneesResultHandler, null);

			RemoteObjectUtil.callService(op);

		}

		// Chargement des données par remoting
		protected function chargerDonneesBySegment():void
		{
			op=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M311.accueil.facade",
				"getRepartDureeBySegment", chargerDonneesResultHandler, null);
			
			RemoteObjectUtil.callService(op, _segment, _idsegment_theme);
		}

		protected override function chargerDonneesResultHandler(re:ResultEvent):void
		{
			dataProviders=re.result as ArrayCollection;
		}

		//formate les tootips du graph des themes
		override protected function formatDataTip(obj:Object):String
		{
			if (obj)
			{
				try
				{
					var libelle:String=obj.item[ResourceManager.getInstance().getString('M311', 'Libell_')];
					var montant:Number=obj.item[ResourceManager.getInstance().getString('M311', 'Montant')];
					var prop:Number=(montant * 100 / _total);

					return ResourceManager.getInstance().getString('M311', 'Op_rateur____b_') + libelle + ResourceManager.getInstance().getString('M311', '__b__br_Dur_e____b__font_color____ff0000') + ConsoviewFormatter.formatNumber(montant, 2) + ResourceManager.getInstance().getString('M311', '_min___font___b__br_Proportion___b_') + prop.toPrecision(4) + "%</b>";
				}
				catch (e:Error)
				{
					trace(e.getStackTrace())
					return "";
				}
			}
			else
			{
				return "";
			}
			return "";
		}

		private function init(fe:FlexEvent):void
		{
			title=ResourceManager.getInstance().getString('M311', '______R_partition_des_dur_es_des_consomm');
			//showCloseButton = true;

			//Image(titleIcon = icnSuivant).addEventListener(MouseEvent.CLICK,ss);
			titleBar.enabled=true;
			titleBar.addEventListener(MouseEvent.CLICK, closeIt);
			titleBar.useHandCursor=true;
			titleBar.buttonMode=true;
			titleBar.mouseChildren=false;
			//addEventListener(CloseEvent.CLOSE,closeIt);
		}

		private function closeIt(ev:Event):void
		{
			dispatchEvent(new Event("switchGraphEvent"));
		}

		private function updateProviders():void
		{
			myGrid.dataProvider=dataProviders;
			graph.callLater(updatePie);
		}

		private function updatePie():void
		{
			graph.myPie.dataProvider=_dataProviders2;
		}

		private function setSegment(segment:String):void
		{
			if (segment == "null")
				_segment="Complet";
			else
				_segment=segment;
		}
	}
}
