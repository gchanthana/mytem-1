package composants.tb.graph
{
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import composants.tb.graph.charts.PieGraph_IHM;
	import composants.util.ConsoviewFormatter;
	
	public class PieGraphBase extends GraphBase
	{
		 
		protected var graph : PieGraph_IHM = new PieGraph_IHM();
		
		public function PieGraphBase()
		{
			//TODO: implement function
			 
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/*---------- PRIVATE ----------------*/
		private function init(fe : FlexEvent):void{

			graph.x = 0;
			
			graph.y = 0;
						
			myGraphContener.addChild(graph);
			
			graph.myPie.dataTipFunction = formatDataTip;		
			
						
		}
		
			
		//formate les tootips du graph des themes
		protected function formatDataTip(obj:Object):String
		{
			if(obj)
			{
				try
				{
					var libelle :String = obj.item[ResourceManager.getInstance().getString('M311', 'Libell_')]; 
				    var montant:Number = obj.item[ResourceManager.getInstance().getString('M311', 'Montant')];
				  	var prop : Number = Math.abs(montant) *  100 / getTotalAbs(obj.element.items);
				  	return ResourceManager.getInstance().getString('M311', 'Libell_____b_')+libelle+
		    		ResourceManager.getInstance().getString('M311', '__b__br_montant____b__font_color____ff00')+moduleTableauDeBordE0IHM.formatNumberCurrency(montant,2)+ResourceManager.getInstance().getString('M311', '__font___b__br_Proportion___b_')+prop.toPrecision(4)+"%</b>";	
				}
				catch(e:Error)
				{
					trace(e.getStackTrace())
					return "";
				}
		 	}
		 	else
		 	{
		 		return "";
		 	}
		    return "";
		}		
		
		protected function getTotalAbs(items:Array):Number
		{
			var _item : Object;
			var _totalAbs:Number = 0;
			
			for	(_item in items){		
				_totalAbs = _totalAbs + Math.abs(items[_item].value); 					   
			}	
			return _totalAbs;
		}
		
				
	}
}