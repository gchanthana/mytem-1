package composants.tb.graph
{

	/**
	 * A pour vocation de creer des objets de type 'Graphique'
	 * @creator Samuel DIVIOKA pour la scociété CONSOTEL
	 **/
	public class GraphFactory
	{
		public static const EVOLUTION:String="EVOLUTION";
		public static const COUTPAROPERATEUR:String="COUTPAROPERATEUR";
		public static const DUREEPAROPERATEUR:String="DUREEPAROPERATEUR";
		public static const COUTCONSOPAROPERATEUR:String="COUTCONSOPAROPERATEUR";
		public static const COUTPARTHEME:String="COUTPARTHEME";
		public static const COUTPARPRODUIT:String="COUTPARPRODUIT";

		public function GraphFactory()
		{
		}

		/**
		 * Retourne un Objet Graphique suivant le type passé en paramètre
		 * @param t Le type de page à créer
		 * @throw GarphError le Graphique n'existe pas ou n'a pu êtres créé.
		 * modifié par samer
		 **/
		public function createGraph(parameters:GraphParameters):IGraphModel
		{
			try
			{
				var type:String=parameters.TYPE_GRAPH;
				var segment:String=parameters.SEGMENT;
				var idsegment_theme:Number=parameters.IDSEGMENT_THEME;
				var surtheme:String=parameters.SUR_THEME;
				var idtheme:String=parameters.ID_THEME;
				var idproduit:String=parameters.ID_PRODUIT;

				switch (type.toUpperCase())
				{
					case "EVOLUTION":return new GraphEvolution(segment);break;
					case "COUTPAROPERATEUR":return new GraphCoutOperateur(idsegment_theme, segment);break;
					case "DUREEPAROPERATEUR":return new GraphDureeConso(idsegment_theme, segment);	break;
					case "COUTCONSOPAROPERATEUR":return new GarphCoutConso(idsegment_theme, segment);break;
					case "COUTPARTHEME":return new GraphCoutTheme(surtheme);break;
					case "COUTPARPRODUIT":return new GraphCoutProduit(idtheme);	break;

					default:throw new GraphError();	break;
				}
			}
			catch (e:Error)
			{
				throw new GraphError();
			}
			return null;
		}
	}
}