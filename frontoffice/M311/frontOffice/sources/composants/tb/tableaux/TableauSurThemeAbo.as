package composants.tb.tableaux
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import composants.util.ConsoviewFormatter;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil; 
	
	public class TableauSurThemeAbo extends TableauSurThemeAbo_IHM implements ITableauModel
	{
		private var _dataProviders : ArrayCollection; 	
		
		[Bindable]	
		private var _changeHandlerFunction : Function;				
		
		[Bindable]
		private var _totalQte : int = 0;
		
		[Bindable]
		private var _surTheme : String;
		
		[Bindable]
		private var _idSurTheme : Number = 0;
	
		[Bindable]
		private var _total : Number = 0;
		
		[Bindable]
		private var _title : String;		
		
		private var moisDeb : String;		
	
		private var moisFin : String;
		
		private var perimetre : String;
		
		private var modeSelection : String;
		
		private var identifiant : String;
		
		private var segment : String;
								
		/**
		 * Constructeur 
		 **/
		public function TableauSurThemeAbo(ev : TableauChangeEvent = null){

			_title = ev.SUR_THEME;			
			_surTheme = ev.SUR_THEME;
			identifiant = ev.IDENTIFIANT;
			segment = ev.SEGMENT;
			addEventListener(FlexEvent.CREATION_COMPLETE,init);	
		}
				
		/**
		 * Retourne le montant total
		 * */
		public function get total():Number{
			return _total;
		}
		
		
		/**
		* Permet de passer les valeurs aux tableaux du composant.
		* <p>Pour chaque tableau de la collection, un DataGrid sera affiché à la suite du précédant</p>
		* @param d Une collection de tableau. 
		**/				
		public function set dataProviders(d : ArrayCollection):void{
			
			_dataProviders = _formatterDataProvider(d);
			drawTabs();
			
		}
		
		/**
		*  Retourne le dataproviders
		**/	
		[Bindable]			
		public function get dataProviders():ArrayCollection{
			return _dataProviders;
		}	
		
		/**
		* Permet d'affecter une fonction pour traiter les Clicks sur Les DataGrids du composant
		* @param changeFunction La fonction qui traite les clicks.<p> Elle prend un paramètre de type Object qui représente une ligne du DataGrid</p>
		**/		
		public function set changeHandlerFunction ( changeFunction : Function ):void{
			_changeHandlerFunction = changeFunction;
		}		
		
		/**
		*  Retourne une reference vers la fonction qui traite le data provider
		**/			
		public function get changeHandlerFunction():Function{
			return _changeHandlerFunction;
		}
		
		/**
		 * Met à jour les donées du tableau
		 **/
		public function update():void{							
			//chargerDonnees(); deprecated
		}
		
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		public function clean():void{
			//to do
		}	
		
		/**
		 * Met à jour la periode
		 * */		 
		public function updatePeriode(moisDeb : String = null, moisFin : String = null):void{
			//TOTO: implement function
			this.moisDeb = moisDeb;
			this.moisFin = moisFin;
		}	
		
		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...) 			
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		public function updatePerimetre(perimetre : String = null, modeSelection : String = null):void
		{
			this.perimetre = perimetre;
			this.modeSelection = modeSelection;				
		}		
				
		/*---------- PRIVATE -----------------------*/
		
		//initialisation du composant
		private function init(fe :  FlexEvent):void{	
			title = _title;	
						
			update();
		}				
		
		private function drawTabs():void{
			/* rep.dataProvider = dataProviders;			
			if (rep.dataProvider != null)	
				for (var i:int = 0; i < rep.dataProvider.length; i++){
					myGrid[i].addEventListener(Event.CHANGE,foo);									
				}  		 */	
				
			myGrid.dataProvider = dataProviders;
			myGrid.addEventListener(Event.CHANGE,foo);
			myGrid.dataTipFunction = getColInfo;	
			myFooterGrid.columns[1].headerText = ConsoviewFormatter.formatNumber(_totalQte,2);
			myFooterGrid.columns[2].headerText = moduleTableauDeBordE0IHM.formatNumberCurrency(_total,2);
		}	
		
		private function foo(ev :Event):void{
			var eventObj : TableauChangeEvent = new TableauChangeEvent("tableauChange");
			
			eventObj.LIBELLE = ev.currentTarget.selectedItem.LIBELLE;
			eventObj.MONTANT_TOTAL = ev.currentTarget.selectedItem.MONTANT_TOTAL;
			eventObj.ID = ev.currentTarget.selectedItem.ID;
			eventObj.QUANTITE = ev.currentTarget.selectedItem.QUANTITE;
			eventObj.TYPE_THEME	= ev.currentTarget.selectedItem.TYPE_THEME;
			eventObj.SUR_THEME = ev.currentTarget.selectedItem.SUR_THEME;
			eventObj.SEGMENT = ev.currentTarget.selectedItem.SEGMENT;
			eventObj.TYPE = ev.currentTarget.selectedItem.TYPE;
			eventObj.SOURCE = ev.currentTarget;
			eventObj.IDENTIFIANT = identifiant;
			dispatchEvent(eventObj);			
		}
		
		// permet de formatter la collection de DataProvider 
		private function _formatterDataProvider(d: ArrayCollection):ArrayCollection{									
			var tmpCollection : ArrayCollection = new ArrayCollection();
			var o : Object;
		
			var surThemeArray : Array = new Array();
			
			_total = 0;
			_totalQte  = 0;
			
			for	(o in d){		
				//surThemeArray.push(formateObject(d[o]));							   
				tmpCollection.addItem(formateObject(d[o]));
			}
							
			//tmpCollection.addItem(surThemeArray);
			 
			
		  			
			return tmpCollection;
		}  	
		
		private function filterFunc(element:*, index:int, arr:Array):Object{
			return element.SEGMENT.toUpperCase() == _surTheme;
		}
		
		private function getColInfo(item : Object):String{
			//var type: String = item["TYPE_THEME"];
			var libelle : String = item["LIBELLE"];
			//var qte : String = item["QUANTITE"];
			//var mt : String = item["MONTANT_TOTAL"];
			
			//var label : Label = new Label();
			/* label.setStyle("color","red");
			label.text = "Type: "+type+
								"\nTheme: "+libelle+
								"\nQuantité :"+qte+
								"\nMontant :"+cf.format(mt); */
			
			
			return libelle;//label.text;
		}
		private  function formateObject(obj : Object):Object{
			var o : Object = new Object();		
						
			_total = _total + parseFloat(obj.MONTANT_FINAL);	
			_totalQte = _totalQte + parseInt(obj.QTE);	
			
			o["LIBELLE"] = obj.THEME_LIBELLE;
			o["SEGMENT"] = segment;
			o["QUANTITE"] = ConsoviewFormatter.formatNumber(obj.QTE,2);
			o["TYPE_THEME"] = obj.TYPE_THEME;
			o["SUR_THEME"] = "";
			o["MONTANT_TOTAL"]= moduleTableauDeBordE0IHM.formatNumberCurrency(obj.MONTANT_FINAL,2);
			o["TYPE"] = "THEME";	
			o["ID"] = obj.IDTHEME_PRODUIT;				
			return o;
		}	
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		protected function chargerDonnees():void{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.M311.surtheme.FacadeSurTheme",
																			"getListeThemesSurThemeByID",
																			chargerDonnneesResultHandler,null);
			
			
			RemoteObjectUtil.callService(op,
										perimetre,
										modeSelection,
										identifiant,
										_surTheme,
										moisDeb,
										moisFin);
			
		}
		 protected function chargerDonnneesFaultHandler(fe :FaultEvent):void{
		 	trace(fe.fault.faultString,fe.fault.name);	
		 	  	
		 }     
		 
		 protected function chargerDonnneesResultHandler(re :ResultEvent):void{
		 	try{
		 		 
		  		dataProviders  =  re.result as ArrayCollection;	
		  	}catch(er : Error){		  	
		  		trace(er.message,"ici");
		  	}
		  
		 }  
				  								
	}
}