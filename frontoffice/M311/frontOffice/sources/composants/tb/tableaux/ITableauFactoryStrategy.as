package composants.tb.tableaux
{
	/**
	 * Interface de la 'stratgy' permettant de créer des tableaux
	 * 
	 **/
	public interface ITableauFactoryStrategy
	{
		function createTableau(ev : TableauChangeEvent):ITableauModel;
	}
}