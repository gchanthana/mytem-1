package composants.tb.tableaux.renderer
{
	import mx.containers.Canvas;
	import mx.controls.Label;
	import mx.resources.ResourceManager;
	
	public class CellFieldConso extends Canvas
	{
		// Define the constructor and set properties.
		private var libelle : Label;
		public function CellFieldConso() {
			libelle = new Label(); 
			libelle.percentWidth = 100;          
			setStyle("borderStyle", "none");  
		}
		
		// Override the set method for the data property.
		override public function set data(value:Object):void {
			super.data = value;
			
			if (value) 
			{				
				super.data = value;			
				if (value.TYPE.toUpperCase() == "SEGMENT" ){
					
					setStyle("color", "#336699");
					setStyle("fontFamily", "verdana");
					setStyle("fontWeight", "bold");
					
					libelle.text  =  ResourceManager.getInstance().getString('M311', 'Segment_') + value.SEGMENT;
					
				}else{
					libelle.text  = value.LIBELLE;
				}
			}
			super.invalidateDisplayList();
			addChild(libelle);
		}
	}
}
