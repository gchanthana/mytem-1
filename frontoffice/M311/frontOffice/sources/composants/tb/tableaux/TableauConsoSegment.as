package composants.tb.tableaux
{
	import composants.tb.tableaux.theme.ThemeData;
	import composants.tb.tableaux.theme.ThemeFixe;
	import composants.tb.tableaux.theme.ThemeMobile;
	import composants.util.ConsoviewFormatter;
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.charts.events.ChartItemEvent;
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class TableauConsoSegment extends TableauConso
	{
		private var _segment:String;
		private var _idSegment:String;
		private var _tiltle:String;
		private var _thmFixe:ThemeFixe;
		private var _thmMobile:ThemeMobile;
		private var _thmData:ThemeData;

		public function TableauConsoSegment(ev:TableauChangeEvent)
		{
			super();
			_segment=ev.SEGMENT;
			_idSegment=ev.IDSEGMENT_THEME;
			_tiltle=ResourceManager.getInstance().getString('M311', 'Consommations_Segment_') + ev.SEGMENT.toLowerCase();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		override protected function measure():void
		{
			super.measure();
			if (myTabContener.height < myPieGraph.height)
			{
				myPieGraph.percentHeight=100;
			}
		}

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight)
			myPieGraph.percentHeight=100;
		}

		//initialisation du composant
		protected override function init(fe:FlexEvent):void
		{
			title=_tiltle;
			myPieGraph.myPie.addEventListener(ChartItemEvent.ITEM_CLICK, _myPieItemClickHandler);
			myPieGraph.labelFunction=pielabelFunction;
			myPieGraph.dataTipsFunction=pieDataTipFunction;
			//myTabContener.addChildAt(t, 1);
			//update();
		}

		protected function drawTabs():void
		{
			rep.dataProvider=dataProviders;
			if (rep.dataProvider != null)
				for (var i:int=0; i < rep.dataProvider.length; i++)
				{
					//myGrid[i].addEventListener(Event.CHANGE,_changeHandlerFunction);
					myGrid[i].addEventListener(Event.CHANGE, myGridChangeEventHandler);
					myGrid[i].columns[0].itemRenderer=null;
					myGrid[i].columns[3].itemRenderer=null;
				}
			myFooterGrid.columns[1].headerText=ConsoviewFormatter.formatNumber(_totalQte, 2);
			myFooterGrid.columns[2].headerText=ConsoviewFormatter.formatNumber(_totalVol, 2);
			myFooterGrid.columns[3].headerText=ConsoviewFormatter.formatNumber(_total, 2);
		}

		//formatte les donnees
		protected override function _formatterDataProvider(d:ArrayCollection):ArrayCollection
		{
			var cptr:int=0;
			var array:ArrayCollection;
			var tmpCollection:ArrayCollection=new ArrayCollection();
			var o:Object;

			var segmentArray:Array=new Array();

			_total=0;
			_totalQte=0;
			_totalVol=0;

			for (o in d)
			{
				segmentArray.push(formateObject(d[o]));
			}

			if (_idSegment == Segment.fixe.toString())
			{
				_thmFixe=new ThemeFixe();
				if (segmentArray.length > 0)
				{
					array=new ArrayCollection(segmentArray);
					for (cptr=0; cptr < _thmFixe.themeFixeConso.length; cptr++)
					{
						if (!ConsoviewUtil.isIdInArray(_thmFixe.themeFixeConso[cptr].IDTHEME_PRODUIT, "IDTHEME_PRODUIT", array.source))
						{
							array.addItemAt(_thmFixe.themeFixeConso[cptr], cptr);
						}
					}
				}
				else
				{
					array=new ArrayCollection();
					for (cptr=0; cptr < _thmFixe.themeFixeConso.length; cptr++)
					{
						array.addItem(_thmFixe.themeFixeConso[cptr]);
					}
				}
			}
			if (_idSegment == Segment.data.toString())
			{
				_thmData=new ThemeData();
				if (segmentArray.length > 0)
				{
					array=new ArrayCollection(segmentArray);
					for (cptr=0; cptr < _thmData.themeDataConso.length; cptr++)
					{
						if (!ConsoviewUtil.isIdInArray(_thmData.themeDataConso[cptr].IDTHEME_PRODUIT, "IDTHEME_PRODUIT", array.source))
						{
							array.addItemAt(_thmData.themeDataConso[cptr], cptr);
						}
					}
				}
				else
				{
					array=new ArrayCollection();
					for (cptr=0; cptr < _thmData.themeDataConso.length; cptr++)
					{
						array.addItem(_thmData.themeDataConso[cptr]);
					}
				}
			}
			
			if (_idSegment == Segment.mobile.toString())
			{
				_thmMobile=new ThemeMobile();
				if (segmentArray.length > 0)
				{
					array=new ArrayCollection(segmentArray);
					for (cptr=0; cptr < _thmMobile.themeMobileConso.length; cptr++)
					{
						if (!ConsoviewUtil.isIdInArray(_thmMobile.themeMobileConso[cptr].IDTHEME_PRODUIT, "IDTHEME_PRODUIT", array.source))
						{
							array.addItemAt(_thmMobile.themeMobileConso[cptr], cptr);
						}
					}
				}
				else
				{
					array=new ArrayCollection();
					for (cptr=0; cptr < _thmMobile.themeMobileConso.length; cptr++)
					{
						array.addItem(_thmMobile.themeMobileConso[cptr]);
					}
				}
			}

			tmpCollection.addItem(array.source);

			return tmpCollection;
		}

		protected override function formateObject(obj:Object, idsegment_theme:int=-1):Object
		{
			_totalVol=_totalVol + (parseFloat(obj.duree_appel));
			_totalQte=_totalQte + parseInt(obj.nombre_appel);
			_total=_total + parseFloat(obj.montant_final);

			var o:Object=new Object();
			o["NBAPPELS"]=ConsoviewFormatter.formatNumber(obj.nombre_appel, 2);
			o["LIBELLE"]=obj.theme_libelle;
			o["SEGMENT"]=obj.segment_theme;
			o["QUANTITE"]=ConsoviewFormatter.formatNumber((parseFloat(obj.duree_appel)), 2);
			o["TYPE_THEME"]=obj.type_theme;
			o["SUR_THEME"]=obj.sur_theme;
			o["MONTANT_TOTAL"]=obj.montant_final;
			o["TYPE"]="THEME";
			o["ORDRE_AFFICHAGE"]=obj.ordre_affichage;
			o["IDTHEME_PRODUIT"]=obj.idtheme_produit;
			o["ID"]=obj.idtheme_produit;
			return o;
		}

		override protected function _myPieItemClickHandler(evt:ChartItemEvent):void
		{
			var eventObj:TableauChangeEvent=new TableauChangeEvent("tableauChange");
			eventObj.LIBELLE=evt.hitData.item.LIBELLE;
			eventObj.ORDRE_AFFICHAGE=evt.hitData.item.ORDRE_AFFICHAGE;
			eventObj.ID=evt.hitData.item.ID;
			eventObj.QUANTITE=evt.hitData.item.QUANTITE;
			eventObj.TYPE_THEME=evt.hitData.item.TYPE_THEME;
			eventObj.SUR_THEME=evt.hitData.item.SUR_THEME;
			eventObj.MONTANT_TOTAL=evt.hitData.item.MONTANT_TOTAL;
			eventObj.SEGMENT=evt.hitData.item.SEGMENT;
			eventObj.TYPE=evt.hitData.item.TYPE;
			eventObj.SOURCE=myPieGraph;
			eventObj.IDENTIFIANT=identifiant;

			dispatchEvent(eventObj);
		}

		// Chargement des données par remoting
		override protected function getConsoBySegment():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M311.accueil.facade",
				"getConsoBySegment", getSurThemeConsosResultHandler);

			RemoteObjectUtil.callService(op, _segment, int(_idSegment));
		}

		override protected function getSurThemeConsosResultHandler(re:ResultEvent):void
		{
			try
			{
				dataProviders=re.result as ArrayCollection;
				segmentDataProvider=new ArrayCollection((dataProviders[0] as Array).slice());
				var cursor:IViewCursor=segmentDataProvider.createCursor();
				var valAbsolue:Number=0;
				while (!cursor.afterLast)
				{
					if (Number(cursor.current.MONTANT_TOTAL) == 0 && Number(cursor.current.QUANTITE) == 0)
					{
						cursor.remove();
					}
					else
					{
						if (cursor.current.MONTANT_TOTAL < 0)
						{
							valAbsolue=Math.abs(cursor.current.MONTANT_TOTAL);
							cursor.current.MONTANT_TOTAL_AFFICHEE=valAbsolue;
						}
						else
						{
							cursor.current.MONTANT_TOTAL_AFFICHEE=cursor.current.MONTANT_TOTAL;
						}
						cursor.moveNext();
					}
				}
				segmentDataProvider.refresh();
			}
			catch (er:Error)
			{
				trace(er.message, "fr.consotel.consoview.M311.accueil.facade  getSurThemeAbos ");
			}
		}

		override protected function chargerDonnees():void
		{
			getConsoBySegment();
			//getTitre();
		}

		override protected function pielabelFunction(data:Object, field:String, index:Number, percentValue:Number):String
		{
			var temp:String=(" " + percentValue).substr(0, 6);
			return data.LIBELLE + ": " + '\n' + ResourceManager.getInstance().getString('M311', 'Total___') + data.MONTANT_TOATAL + '\n' + temp + "%";
		}
	}
}
