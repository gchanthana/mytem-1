 package composants.tb.tableaux
{
	import composants.util.ConsoviewFormatter;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class TableauThemeConso extends TableauThemeConso_IHM implements ITableauModel
	{
		private var _dataProviders : ArrayCollection; 	
		
		[Bindable]
		private var _total : Number = 0;
		[Bindable]
		private var _totalVol : Number = 0;
		[Bindable]
		private var _totalNbAppels : Number = 0;
		
		private var _idthemeproduit : int;
				
		private var moisDeb : String;
				
		private var moisFin : String;
		
		private var perimetre : String;
		
		private var modeSelection : String;
		
		private var identifiant : String;

		[Bindable]
		private var _title : String;		
		
		private var segment : String;
						
		/**
		 * Constructeur 
		 **/
		public function TableauThemeConso(ev : TableauChangeEvent = null){
			_idthemeproduit = parseInt(ev.ID);
			_title = ev.LIBELLE;	
			identifiant = ev.IDENTIFIANT;		
			segment = ev.SEGMENT;
			addEventListener(FlexEvent.CREATION_COMPLETE,init);	
		}
				
		/**
		 * Retourne le montant total
		 * */
		 public function get total():Number{
		 	return _total;
		 }
		
		
		/**
		* Permet de passer les valeurs aux tableaux du composant.
		* <p>Pour chaque tableau de la collection, un DataGrid sera affiché à la suite du précédant</p>
		* @param d Une collection de tableau. 
		**/				
		public function set dataProviders(d : ArrayCollection):void{
						
			_dataProviders = _formatterDataProvider(d);	
			drawTabs();		
		}
		
		/**
		*  Retourne le dataproviders
		**/	
		[Bindable]			
		public function get dataProviders():ArrayCollection{
			return _dataProviders;
		}	
		
		/**
		* Permet d'affecter une fonction pour traiter les Clicks sur Les DataGrids du composant
		* @param changeFunction La fonction qui traite les clicks.<p> Elle prend un paramètre de type Object qui représente une ligne du DataGrid</p>
		**/		
		public function set changeHandlerFunction ( changeFunction : Function ):void{
			//_changeHandlerFunction = changeFunction;
		}		
		
		
		/**
		*  Retourne une reference vers la fonction qui traite le data provider
		**/			
		public function get changeHandlerFunction():Function{
			return null;//_changeHandlerFunction;
		}	
		
		/**
		 * Met à jour les donées du tableau
		 **/
		public function update():void{							
			//// chargerDonnees(); deprecated	
		}
		
		
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		public function clean():void{
			//to do
		}	
		
		/**
		 * Met à jour la periode
		 * */		 
		public function updatePeriode(moisDeb : String = null, moisFin : String = null):void{
			//TOTO: implement function
			this.moisDeb = moisDeb;
			this.moisFin = moisFin;
		}		
		
		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...) 			
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		public function updatePerimetre(perimetre : String = null, modeSelection : String = null):void
		{
			this.perimetre = perimetre;
			this.modeSelection = modeSelection;				
		}
					
		/*---------- PRIVATE -----------------------*/
		
		//initialisation du composant
		private function init(fe :  FlexEvent):void{				
			title = _title;
			//update();
		}		
	
		//dessine le tableau
		private function drawTabs():void{
			//rep.dataProvider = dataProviders;			
			/* if (rep.dataProvider != null)	
				for (var i:int = 0; i < rep.dataProvider.length; i++){
					myGrid[i].addEventListener(Event.CHANGE,foo);									
				}  	 */

			myGrid.dataProvider = dataProviders;
			myGrid.addEventListener(Event.CHANGE,foo);
			 
					
			myFooterGrid.columns[1].headerText = ConsoviewFormatter.formatNumber(_totalNbAppels,2);
			myFooterGrid.columns[2].headerText = ConsoviewFormatter.formatNumber(_totalVol,2);
			myFooterGrid.columns[3].headerText = moduleTableauDeBordE0IHM.formatNumberCurrency(_total,2);	
		}
		
		private function foo(ev :Event):void{
			var eventObj : TableauChangeEvent = new TableauChangeEvent("tableauChange");
			
			eventObj.LIBELLE = ev.currentTarget.selectedItem.LIBELLE;
			eventObj.MONTANT_TOTAL = ev.currentTarget.selectedItem.MONTANT_TOTAL;
			eventObj.ID = ev.currentTarget.selectedItem.IDTHEME_PRODUIT;
			eventObj.QUANTITE = ev.currentTarget.selectedItem.QUANTITE;
			eventObj.TYPE_THEME	= ev.currentTarget.selectedItem.TYPE_THEME;
			eventObj.SUR_THEME = ev.currentTarget.selectedItem.SUR_THEME;
			eventObj.SEGMENT = ev.currentTarget.selectedItem.SEGMENT;
			eventObj.TYPE = ev.currentTarget.selectedItem.TYPE;
			eventObj.IDPRODUIT = ev.currentTarget.selectedItem.ID_PRODUIT_CLIENT;
			eventObj.OPERATEUR = ev.currentTarget.selectedItem.OPERATEUR;
			eventObj.SEGMENT = ev.currentTarget.selectedItem.SEGMENT;
			eventObj.SOURCE = ev.currentTarget;
			
			
			eventObj.IDENTIFIANT = identifiant;
			dispatchEvent(eventObj);			
		}
				
		// permet de formatter la collection de DataProvider 
		private function _formatterDataProvider(d: ArrayCollection):ArrayCollection{		
			var tmpCollection : ArrayCollection = new ArrayCollection();
			var o : Object;
		
			var produitsArray : Array = new Array();
			
			_total = 0;
			_totalVol = 0;
			_totalNbAppels = 0;			
			
			for	(o in d){		
				//produitsArray.push(formateObject(d[o]));	
				tmpCollection.addItem(formateObject(d[o]));										   
			}
								
			//tmpCollection.addItem(produitsArray);
			
			return tmpCollection;
			
		}
		
		private function calculTotal(arr : Array, colname : String):Number{
			var total : Number = 0;
			
			for(var i:int=0; i< arr.length;i++){
				
				total = total + parseFloat(arr[i][colname]);
			}
			return total;
		}		
		
		private function formateObject(obj : Object):Object{
			var o : Object = new Object();	
			
			_total = _total + parseFloat(obj.MONTANT_FINAL);		
			_totalVol = _totalVol + (parseFloat(obj.DUREE_APPEL));	
			_totalNbAppels = _totalNbAppels + parseInt(obj.NOMBRE_APPEL);
			
			o["OPERATEUR"] = obj.NOM;
			o["LIBELLE"] = obj.LIBELLE_PRODUIT;
			o["SEGMENT"] = segment;
			o["QUANTITE"] = ConsoviewFormatter.formatNumber((parseFloat(obj.DUREE_APPEL)),2);
			o["TYPE_THEME"] = obj.TYPE_THEME;
			o["THEME_LIBELLE"]= obj.THEME_LIBELLE;
			o["SUR_THEME"] = "";
			o["NBAPPEL"] = obj.NOMBRE_APPEL;
			o["MONTANT_TOTAL"]= moduleTableauDeBordE0IHM.formatNumberCurrency(obj.MONTANT_FINAL,2);
			o["TYPE"] = "PRODUIT";				
			o["ID_PRODUIT"] = obj.IDPRODUIT;					
			o["ID_PRODUIT_CLIENT"] = obj.IDPRODUIT_CLIENT;
			o["DUREE"] = obj.DUREE_APPEL;
						
			return o;
		}		
		 
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		protected function chargerDonnees():void{
			/* var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.M311.theme.facade",
																			"getListeProduitsTheme",
																			chargerDonnneesResultHandler,
																			chargerDonnneesFaultHandler
																			);
			
			
			RemoteObjectUtil.callService(op,
										perimetre,
										modeSelection,
										identifiant,
										_idthemeproduit,
										moisDeb,
										moisFin); */
		}
		 protected function chargerDonnneesFaultHandler(fe :FaultEvent):void{
		 /* 	trace(fe.fault.faultString,fe.fault.name);	 */
		 	 	
		 }     
		 
		 protected function chargerDonnneesResultHandler(re :ResultEvent):void{
		 	/* try{
		 		 
		  		dataProviders =  re.result as ArrayCollection;	
		  	}catch(er : Error){		  	
		  		trace(er.message,"tableauThemeConso");
		  	} */
		  
		 }  			
	}
}