package composants.access.perimetre.tree {
	import flash.events.IEventDispatcher;

	public interface ISearchPerimetreWindow extends IEventDispatcher {
		function performSearch(nodeId:int,searchKeyword:String):void;
		
		function onPerimetreChange():void;
	}
}
