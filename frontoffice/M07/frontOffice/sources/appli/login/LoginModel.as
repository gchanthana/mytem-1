package appli.login {
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtilEvent;

	public class LoginModel extends EventDispatcher implements ILoginModel {
		private var authInfos:Object;
		private var remoteObjectUtil:RemoteObjectUtil = null;
		
		public function LoginModel(target:IEventDispatcher=null) {
			super(target);
			/* remoteObjectUtil = RemoteObjectUtil.getInstance();
			remoteObjectUtil.addEventListener(RemoteObjectUtilEvent.SUCCESS_CONNECT,authHandler);
			remoteObjectUtil.addEventListener(RemoteObjectUtilEvent.FAILED_CONNECT,authHandler);
			remoteObjectUtil.addEventListener(RemoteObjectUtilEvent.IPADDR_ACCESS_REFUSED,authHandler);
			remoteObjectUtil.addEventListener(RemoteObjectUtilEvent.CONNECT_FAULT,authHandler);
			*/
		}
		
		public function connect(loginString:String,pwdString:String):void {
			remoteObjectUtil.connect(loginString,pwdString);
		}
		
		private function authHandler(event:RemoteObjectUtilEvent):void {
			if(event.type == RemoteObjectUtilEvent.SUCCESS_CONNECT) {
				authInfos = remoteObjectUtil.getCredentialsObjectCopy();
				dispatchEvent(new LoginEvent(LoginEvent.VALIDE));
			} else if(event.type == RemoteObjectUtilEvent.FAILED_CONNECT){
				authInfos = null;
				dispatchEvent(new LoginEvent(LoginEvent.INVALIDE));
			} else if(event.type == RemoteObjectUtilEvent.IPADDR_ACCESS_REFUSED){
				authInfos = null;
				dispatchEvent(new LoginEvent(LoginEvent.IP_ADDRESS_INVALIDE));
			} else if(event.type == RemoteObjectUtilEvent.CONNECT_FAULT){
				authInfos = null;
				dispatchEvent(new RemoteObjectUtilEvent(RemoteObjectUtilEvent.CONNECT_FAULT));
			}
		}
		
		public function getAuthInfos():Object {
			return authInfos;
		}
	}
}
