package datalert.event
{
	import flash.events.Event;

	public class DatalertEvent extends Event
	{
		
		public static const PARAM_SEUILLIGNE_EVENT:String = 'Param_seuilLigne';
		public static const REVOKELINE_EVENT: String='revoque_Ligne';
		public static const UPDATE_SEUILLIGNE_EVENT: String='update_seuilLigne';
		public static const DEFAULTMESSAGE_EVENT: String='default_message';		
		public static const SAVE_UPDATE_MESSAGE_EVENT: String='update_save_Message';
		public static const DEPLOY_SERVICE_EVENT: String='deploy_service';
		public static const SEND_MESSAGE_EVENT: String='send_Message';
		public static const DELETE_MESSAGE_EVENT: String='delete_Message';		
		public static const REFRESH_BILLINGDAY_EVENT: String='refresh_BillingDay';
		public static const REFRESH_DOMESTICDATA_EVENT: String='refresh_DomesticData';
		public static const REFRESH_ROAMINGDATA_EVENT: String='refresh_RoamingData';
		public static const NOTIFY_MANAGER_EVENT:String= "notify_Managers";
		public static const REFRESH_PARENT_DATA:String= "refreshParentData";
		public static const UPDATE_DATA_CHART_EVENT:String = "updateDataChartEvent";
		public static const SEARCH_LIST_WITHOUT_LIMIT_EVENT:String = "searchListWithoutLimit";
		public static const UPDATE_ALLSEARCH_EVENT:String = "updateAllSearch";
		
		private var _objectReturn:Object;
		
		public function DatalertEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,objectReturn : Object = null)
		{
			this.objectReturn = objectReturn;
			super(type, bubbles, cancelable);
		}
		
		
		public function set objectReturn(value:Object):void
		{
			_objectReturn = value;
		}
		
		public function get objectReturn():Object
		{
			return _objectReturn;
		}
		
		override public function clone():Event
		{
			return new DatalertEvent(type,bubbles,cancelable,objectReturn)
		}
	}
}