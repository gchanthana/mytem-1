package datalert.event
{
	import flash.events.Event;
	
	public class AnnuaireServiceEvent extends Event
	{
		public static var ADD_ITEM:String 	= "ASE_ADD_ITEM";
		public static var AREAS_CODE:String = "AREAS_CODE";
		public function AnnuaireServiceEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}