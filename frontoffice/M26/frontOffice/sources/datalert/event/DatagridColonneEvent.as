package datalert.event
{
	import flash.events.Event;
	
	public class DatagridColonneEvent extends Event
	{
		public static const SELECT_LINE:String = 'lineselect';
		
		public var item:Object;
		
		public function DatagridColonneEvent(type : String,item : Object, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.item = item;
		}
		
		override public function clone():Event
		{
			return new  DatagridColonneEvent(type,item,bubbles,cancelable);
		}
		
	}
}