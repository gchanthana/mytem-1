package datalert.event
{
	import flash.events.Event;

	public class TSyncServerEvent  extends Event
	{
		public static const TSYNCSERVER_REFRESH_CLICK:String  = "TSSE_REFRESH_CLICK";
		public function TSyncServerEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}