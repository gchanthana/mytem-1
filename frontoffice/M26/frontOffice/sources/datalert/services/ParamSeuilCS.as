package datalert.services
{
	import composants.util.ConsoviewFormatter;
	
	import datalert.event.DatalertEvent;
	import datalert.vo.LigneVO;
	import datalert.vo.SeuilLigneVO;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	[Bindable]
	public class ParamSeuilCS extends EventDispatcher
	{
		public var seuilLigneVo : SeuilLigneVO;
		
		public function ParamSeuilCS()
		{
		}
		
		public function rechercheSeuilLigne(idSous_tete:int):void
		{			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.Datalert",
				"rechercheSeuilLigne",
				rechercheSeuilLigne_handler,null);
			
			RemoteObjectUtil.callService(opData,idSous_tete);
		}
		
		public function updateParamSeuilLigne(listLigne: ArrayCollection, ligne : LigneVO,seuil:SeuilLigneVO):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.Datalert",
				"updateParamSeuilLigne",
				updateParamSeuilLigne_handler,null);
			var listLigneString:String = listLigne[0];
			var i:int = 1;
			while(i < listLigne.length)
			{
				listLigneString = listLigneString + ',' + listLigne[i];
				i++;
			}
			trace("Longueur de l'argument : " + listLigneString.length.toString());
			RemoteObjectUtil.callService(opData, listLigneString,ligne.JOURFACTURATION,
												 (ligne.DOMESTIQUEPLAN==-1)?-1:((ligne.FORMATDOMESTIQUEPLAN==1)?ligne.DOMESTIQUEPLAN*1024000:ligne.DOMESTIQUEPLAN*1024),//ligne.DOMESTIQUEPLAN*1048576
												 (ligne.ROAMINGPLAN==-1)?-1:((ligne.FORMATROAMINGPLAN==1)?ligne.ROAMINGPLAN*1024000:ligne.ROAMINGPLAN*1024),
												 ligne.BOOLFAIRUSEDOM,
												 seuil.BOOLDRTDOM,
												 seuil.BOOLDRTROAM,
												 seuil.BOOLAUTOREFRESH);
		}
		
		public function updateParamAllSearch(listLigne: ArrayCollection, ligne : LigneVO,seuil:SeuilLigneVO, jsonSent:String):void
		{
			var ro:RemoteObject = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			ro.source = "fr.consotel.consoview.M26.Datalert";
			ro.addEventListener(ResultEvent.RESULT, updateParamAllSearch_handler);
			ro.addEventListener(FaultEvent.FAULT, updateParamAllSearch_faultHandler);

			var listLigneString:String = listLigne[0];
			var i:int = 1;
			while(i < listLigne.length)
			{
				listLigneString = listLigneString + ',' + listLigne[i];
				i++;
			}
			trace("Longueur de l'argument : " + listLigneString.length.toString());
			ro.updateParamAllSearch(listLigneString,ligne.JOURFACTURATION,
				(ligne.DOMESTIQUEPLAN==-1)?-1:((ligne.FORMATDOMESTIQUEPLAN==1)?ligne.DOMESTIQUEPLAN*1024000:ligne.DOMESTIQUEPLAN*1024),
				(ligne.ROAMINGPLAN==-1)?-1:((ligne.FORMATROAMINGPLAN==1)?ligne.ROAMINGPLAN*1024000:ligne.ROAMINGPLAN*1024),
				ligne.BOOLFAIRUSEDOM,
				seuil.BOOLDRTDOM,
				seuil.BOOLDRTROAM,
				seuil.BOOLAUTOREFRESH,
				jsonSent);	//AMOD
		}
		
		public function refreshBillingDay(idSous_tete:int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.Datalert",
				"refreshBillingDay",
				refreshBillingDay_handler,null);
			
			RemoteObjectUtil.callService(opData, idSous_tete);					
		}
		
		public function refreshDomesticData(idSous_tete:int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.Datalert",
				"refreshDomesticData",
				refreshDomesticData_handler,null);
			
			RemoteObjectUtil.callService(opData, idSous_tete);					
		}	
		
		public function refreshRoamingData(idSous_tete:int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.Datalert",
				"refreshRoamingData",
				refreshRoamingData_handler,null);
			
			RemoteObjectUtil.callService(opData, idSous_tete);					
		}
		
		private function rechercheSeuilLigne_handler(re : ResultEvent):void
		{
			var tmp:Array = (re.result as ArrayCollection).source;
			seuilLigneVo = new SeuilLigneVO();	
			
			for(var i : int=0; i < tmp.length;i++)
			{						
				seuilLigneVo.COMPTEFACTURATION=tmp[i].COMPTEFACTURATION;
				seuilLigneVo.BOOLDRTDOM=tmp[i].BOOLDRTDOM;
				seuilLigneVo.BOOLDRTROAM=tmp[i].BOOMDRTROAM;
				seuilLigneVo.BOOLAUTOREFRESH=tmp[i].BOOLAUTOREFRESH;
				seuilLigneVo.BOOLMESSAGEDOM=tmp[i].BOOLMESSAGEDOM;
				seuilLigneVo.IDMESSAGEDOM=tmp[i].IDMESSAGEDOM;
				seuilLigneVo.BOOLMESSAGEROAM=tmp[i].BOOLMESSAGEROAM;
				seuilLigneVo.IDMESSAGEROAM=tmp[i].IDMESSAGEROAM;
			}
			dispatchEvent(new DatalertEvent(DatalertEvent.PARAM_SEUILLIGNE_EVENT,true));
		}
		
		private function updateParamSeuilLigne_handler(evt : ResultEvent):void
		{
			trace("Resultat de updateParamSeuilLigne : " + evt.result.toString());
			dispatchEvent(new DatalertEvent(DatalertEvent.UPDATE_SEUILLIGNE_EVENT,true,false,evt.result));			
		}
		
		private function updateParamAllSearch_handler(evt : ResultEvent):void
		{
			dispatchEvent(new DatalertEvent(DatalertEvent.UPDATE_ALLSEARCH_EVENT, true, false, evt.result));
		}
		
		private function updateParamAllSearch_faultHandler(evt : FaultEvent):void
		{
			dispatchEvent(new DatalertEvent(DatalertEvent.UPDATE_ALLSEARCH_EVENT, true, false, evt.fault));
		}
		
		private function refreshBillingDay_handler(evt : ResultEvent):void
		{
			dispatchEvent(new DatalertEvent(DatalertEvent.REFRESH_BILLINGDAY_EVENT,true,false,evt.result));			
		}
		
		private function refreshDomesticData_handler(evt : ResultEvent):void
		{
			dispatchEvent(new DatalertEvent(DatalertEvent.REFRESH_DOMESTICDATA_EVENT,true,false,evt.result));			
		}
		
		private function refreshRoamingData_handler(evt : ResultEvent):void
		{
			dispatchEvent(new DatalertEvent(DatalertEvent.REFRESH_ROAMINGDATA_EVENT,true,false,evt.result));			
		}
	}
}