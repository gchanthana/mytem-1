package datalert.ihm.fiches.detailsFicheLigne{	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import datalert.ihm.actions.SendMessageIHM;
	import datalert.ihm.chartUsage.ChartUsageDayIHM;
	import datalert.ihm.chartUsage.ChartUsageMonthIHM;
	import datalert.ihm.chartUsage.ChartUsageWeekIHM;
	import datalert.vo.LigneVO;
		public class UsageRendererImpl extends VBox	{		[Bindable]		public var ligne:LigneVO;				[Bindable]		public var mesure:ArrayCollection = new ArrayCollection();				[Bindable]public var chartUsageDay:ChartUsageDayIHM;		[Bindable]public var chartUsageWeek:ChartUsageWeekIHM;		[Bindable]public var chartUsageMonth:ChartUsageMonthIHM;		[Bindable]public var dgRoaming:DataGrid;		[Bindable]public var dgUsage:DataGrid;				public function UsageRendererImpl()		{			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);		}				public function setCurrentData(currentInfos:LigneVO):void		{			ligne = currentInfos;			ligne.LIBELLE_DOM = ResourceManager.getInstance().getString('M26','Domestique_Plan');			ligne.LIBELLE_ALERTE = ResourceManager.getInstance().getString('M26','Alerte');			ligne.LIBELLE_ROAMING = ResourceManager.getInstance().getString('M26','Roaming_Plan');			mesure.addItem(ligne);					}				private function formatSeuilAllert(data:LigneVO):void
		{
			var domestique:Object = new Object();			domestique.LIBELLE = ResourceManager.getInstance().getString('M26','Domestique_Plan');			domestique.DOMESTIQUEPLAN = data.DOMESTIQUEPLAN; 			roaming.LIBELLE_ALERTE = ResourceManager.getInstance().getString('M26','Alerte')			domestique.DOMESTIQUEDATA = data.DOMESTIQUEDATA;			domestique.DOMESTIQUEALERT = data.DOMESTIQUEALERT;												var roaming:Object = new Object();			roaming.LIBELLE = ResourceManager.getInstance().getString('M26','Roaming_Plan');			roaming.ROAMINGDATA = data.ROAMINGDATA; 			roaming.LIBELLE_ALERTE = ResourceManager.getInstance().getString('M26','Alerte');			roaming.ROAMINGPLAN = data.ROAMINGPLAN;			roaming.ROAMINGALERT = data.ROAMINGALERT;			
		}				private function creationCompleteHandler(fe:FlexEvent):void		{						}				protected function sendMessage():void		{						if (ligne.BOOLDRT==1)			{				var item:Object = new Object(); 									item.numLigne = ligne.LIGNE;				item.UUID = ligne.UUID;				item.IDSOUSTETE = ligne.IDSOUSTETE;				item.DOMESTIQUEDATA = ligne.DOMESTIQUEDATA;				item.DOMESTIQUEPLAN = ligne.DOMESTIQUEPLAN;				item.DOMESTIQUEALERT = ligne.DOMESTIQUEALERT;				item.ROAMINGDATA = ligne.ROAMINGDATA;				item.ROAMINGPLAN = ligne.ROAMINGPLAN;				item.ROAMINGALERT = ligne.ROAMINGALERT;										var message:SendMessageIHM=new SendMessageIHM();				message.listMessageLigne.addItem(item);				PopUpManager.addPopUp(message, this.parent.parent, true);				PopUpManager.centerPopUp(message);			}			else			{				Alert.show(ResourceManager.getInstance().getString('M26', 'IL_faut_activer_le_service_avant_d_envoy'),ResourceManager.getInstance().getString('M26', 'Attention'));			}					}	}}