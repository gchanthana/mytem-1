package datalert.ihm.fiches.importDeMasse
{

	import com.as3xls.xls.ExcelFile;
	import com.as3xls.xls.Sheet;
	
	import composants.renderer.CustomToolTip;
	import composants.util.ConsoviewAlert;
	import composants.util.importDeMasse.DataGridUtils;
	
	import datalert.event.AnnuaireServiceEvent;
	import datalert.event.NewLigneEvent;
	import datalert.ihm.popup.ImportExcelIHM;
	import datalert.ihm.popup.PopupChooseIndicatifIHM;
	import datalert.ihm.tooltip.CustomToolTip;
	import datalert.ihm.tooltip.WhiteToolTip;
	import datalert.services.AnnuaireService;
	import datalert.vo.AnnauireVO;
	import datalert.vo.NewLigneVO;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.events.FlexEvent;
	import mx.events.ToolTipEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import rapport.utils.serialization.json.JSON;

	[Bindable]
	public class ImportDeMasseImpl extends TitleWindow
	{
		public var dgImportLigne:DataGrid;
		public var dgImportLigneDP:ArrayCollection=new ArrayCollection();
		public var xlsImporter:ImportExcelIHM;
		private var annuService:AnnuaireService;
		public var btValider:Button;
		public var imgE164:Image;
		private var arrayNomColonnes:Array=[ResourceManager.getInstance().getString('M26', 'Matricule'), ResourceManager.getInstance().getString('M26', 'Nom'), ResourceManager.getInstance().getString('M26', 'Pr_nom'), ResourceManager.getInstance().getString('M26', 'Email'), ResourceManager.getInstance().getString('M26', 'T_l_phone_mobile')];
		public var requestsPending:int=0;
		public var cbIndicatif:ComboBox;
		public var listeIndicatifs:ArrayCollection = new ArrayCollection();
		public var popupChooseIndicatif:PopupChooseIndicatifIHM;

		public function ImportDeMasseImpl()
		{

		}

		protected function importdemasseimpl1_creationCompleteHandler(event:FlexEvent):void
		{
			dgImportLigne.initialize();
			annuService=new AnnuaireService();
			annuService.addEventListener(AnnuaireServiceEvent.ADD_ITEM, close);
			annuService.addEventListener(AnnuaireServiceEvent.AREAS_CODE, areasCodeHandler);
			annuService.getAreasCode();
			addEventListener("CHECK_VALIDATION", allowValidation);
			addEventListener("CHECK_SINGLE_DOUBLONS", checkDoublonsForOneValue);

			var firstRow:NewLigneVO=new NewLigneVO("", "", "", "", "");
			firstRow.ISITEM=false;
			dgImportLigneDP.addItemAt(firstRow, 0);
			
			systemManager.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyPressed);
		}

		protected function imgE164_toolTipCreateHandler(e:ToolTipEvent):void
		{
			e.toolTip = new WhiteToolTip();
		}
		
		private function handleKeyPressed(ke:KeyboardEvent):void
		{
			if ((ke.ctrlKey || ke.altKey) && ke.charCode == 0 && (ke.target is DataGrid))
			{
				//---> AJOUT D'UN OBJET TEXTFIELD INVISIBLE POUR LE DATAGRID
				var textField:TextField	= new TextField();
				textField.name		= 'clipboardProxy';
				textField.visible	= false;
				textField.type		= TextFieldType.INPUT;
				textField.multiline	= true;
				textField.text 		= '';//---> METTRE DANS LE TEXTFIELD LES DONNÉES COPIÉES DEPUIS LE FORMAT TSV 
				
				dgImportLigne.addChild(textField);
				
				textField.setSelection(0, textField.text.length - 1);
				
				textField.addEventListener(TextEvent.TEXT_INPUT, handleTextPasted, false, 0, true);
				//textField.addEventListener(TextEvent.TEXT_INPUT, handleTextPasted);
				//---> METTRE LE FOCUS AU TEXTFIELD
				this.systemManager.stage.focus = textField;
			}
		}
		
		protected function handleTextPasted(te:TextEvent):void
		{
			var columnText	:Array = DataGridUtils.getItemsFromText(te.text);
			var lenColumn	:int = columnText.length;
			var lenElements	:int = dgImportLigneDP.length - 1;
			var i			:int = 0;
			
			var dgDPProperties:Array = ['MATRICULE', 'NOM', 'PRENOM', 'EMAIL', 'TEL'];
			if(lenColumn > 0)
			{
				if(columnText[0].hasOwnProperty('col5'))
				{
					var isOk:Boolean = checkHeader(columnText[0], false);
					
					if(isOk)
					{
						var _DPToAdd:ArrayCollection = new ArrayCollection();
						
						for (i = 1;i < lenColumn;i++)
						{
							_DPToAdd.addItem(new NewLigneVO(columnText[i]['col1'], columnText[i]['col2'],
																columnText[i]['col3'], columnText[i]['col4'], columnText[i]['col5']));
						}
						
						dgImportLigneDP.addAllAt(_DPToAdd, 0);
					}
					else
					{
						ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M26', 'Les_en_t_tes_des_colonnes_ne_sont_pas_en'), 'Consoview');
					}
				}
				else
				{
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M26', 'Les_en_t_tes_des_colonnes_ne_sont_pas_en'), 'Consoview');
				}
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M26', 'Les_en_t_tes_des_colonnes_ne_sont_pas_en'), 'Consoview');
			}
		}
		
		/** Click Handlers
		 */

		public function addRow():void
		{
			dgImportLigneDP[dgImportLigneDP.length - 1].ISITEM=true;
			var newRow:NewLigneVO=new NewLigneVO("", "", "", "", "");
			newRow.ISITEM=false;
			dgImportLigneDP.addItemAt(newRow, dgImportLigneDP.length);
		}

		protected function btRemoveAll_clickHandler(event:MouseEvent):void
		{
			var o:Object=dgImportLigneDP[dgImportLigneDP.length - 1];
			dgImportLigneDP.source=new Array();
			dgImportLigneDP.addItem(o);
		}

		protected function btImport_clickHandler(event:MouseEvent):void
		{
			xlsImporter=new ImportExcelIHM();

			xlsImporter.addEventListener('POPUP_CLOSED_UPLOADED', popUpClosedHandler);

			PopUpManager.addPopUp(xlsImporter, this, true);
			PopUpManager.centerPopUp(xlsImporter);
		}
		
		protected function searchIndicatif(e:MouseEvent):void
		{
			popupChooseIndicatif = new PopupChooseIndicatifIHM();
			adaptListeIndicatifs();
			popupChooseIndicatif.listeIndicatifs = this.listeIndicatifs;
			popupChooseIndicatif.addEventListener('VALID_CHOIX_INDICATIF', indicatifChosen);
			PopUpManager.addPopUp(popupChooseIndicatif, this, true);
			PopUpManager.centerPopUp(popupChooseIndicatif);
			
			
			//créer la popup
			//envoyer la listeIndicatifs (this) vers une variable de la popup
			//mettre un listener sur l'event de validation de la popup 
			//afficher la popup
		}
		
		public function indicatifChosen(e:Event):void	//On sélectionne sur la combo l'indicatif choisi dans la popup, lors de sa fermeture.
		{
			var o:Object = popupChooseIndicatif.itemCurrent;
			var i:int = 0;
			while (i < listeIndicatifs.length)
			{
				if (o.label == listeIndicatifs[i].label)
				{
					cbIndicatif.selectedIndex = i;
					break;
				}
				i++;
			}
		}
		
		private function adaptListeIndicatifs():void	//On ajoute une variable SELECTED à chaque objet de la liste pour qu'elle s'adapte au fonctionnement de la popup de recherche.
		{
			var i:int = 0;
			while (i < listeIndicatifs.length)
			{
				if (i == cbIndicatif.selectedIndex)
				{
					listeIndicatifs[i].SELECTED = true;
				}
				else
				{
					listeIndicatifs[i].SELECTED = false;
				}
				i++;
			}
		}
		/**
		 */
		
		protected function cbIndicatif_changeHandler(e:Event):void
		{
			
			if (cbIndicatif.selectedItem.label != "")
			{
				
			}
			//En cas de changement, si le label sélectionné est != "",
			//on applique l'indicatif sélectionné à tous les numéros existants, sous la forme d'un label qui précède.
			
			//Il faudra ensuite modifier la fonction d'envoi des données en rajoutant le label d'indicatif avant chaque numéro.
		}

		public function areasCodeHandler(e:AnnuaireServiceEvent):void
		{
			listeIndicatifs = new ArrayCollection();
			
			var i:int = 0;
			while (i < annuService.areasCode.length)
			{
				var o:Object = new Object();
				
				o.label = annuService.areasCode[i].NOM + " (" + annuService.areasCode[i].PREFIX_TELEPHONE + ")";
				o.data 	= annuService.areasCode[i].PREFIX_TELEPHONE;
				listeIndicatifs.addItem(o);
				i++;
			}
			
			//Selon la langue choisie au login, on adapte automatiquement l'indicatif pré-sélectionné
			i = 0;
			var stringToFind:String;
			
			switch (CvAccessManager.getSession().USER.GLOBALIZATION)
			{
				case "fr_FR":
					stringToFind = "+33";
					break;
				case "es_ES":
					stringToFind = "+34";
					break;
				case "nl_NL":
					stringToFind = "+45";
					break;
				case "en_CH":
					break;
				case "en_EU":
					stringToFind = "+44";
					break;
				case "en_US":
					stringToFind = "+1";
					break;
				case "en_GB":
					stringToFind = "+44";
					break;
				default:
					stringToFind = "+1";
					break;
			}

			while (i < listeIndicatifs.length)
			{
				if (listeIndicatifs[i].data == stringToFind)
				{
					cbIndicatif.selectedIndex = i;
				}	
				i++;
			}
		
		}
		
		public function popUpClosedHandler(e:Event):void
		{
			var fileXLS:ExcelFile=new ExcelFile();
			fileXLS.loadFromByteArray(xlsImporter.fileUploaded);

			var sheet:Sheet=fileXLS.sheets[0]; //1ere feuille

			if (checkHeader(sheet.values[0])) //ligne des intitulés de la matrice importée
				fillDG(sheet.values);

		}

		public function checkHeader(myObj:Object, comesFromFile:Boolean = true):Boolean
		{
			var longueur:int=arrayNomColonnes.length;
			if(!comesFromFile)//Si le header a été récupéré d'un copier/coller
			{
				if (myObj)
				{
					var myAC:ArrayCollection = new ArrayCollection();
					myAC = fillACWithObjFromCC(myObj);
					if (myAC.length > 0)
					{
						for (var index:int=0; index < longueur; index++)
						{
							if (myAC[index].toString().toUpperCase() != arrayNomColonnes[index].toString().toUpperCase())
							{
								return false;
							}
						}
						return true;
					}
					else
					{
						return false;
					}
					return false;
				}
				return false;
			}
			else //Si le header a été récupéré d'un fichier Excel
			{
				if (myObj)
				{
					if (myObj.length > 0)
					{
						for (var idx:int=0; idx < longueur; idx++)
						{
							if (myObj[idx]['value'].toString().toUpperCase() != arrayNomColonnes[idx].toString().toUpperCase())
							{
								Alert.show(ResourceManager.getInstance().getString('M26', 'Le_fichier_n_a_pas_le_format_d_une_matri'), ResourceManager.getInstance().getString('M26', 'Erreur'));
								return false;
							}
						}
						return true;
					}
					else
					{
						Alert.show(ResourceManager.getInstance().getString('M26', 'Le_fichier_n_a_pas_le_format_d_une_matri'), ResourceManager.getInstance().getString('M26', 'Erreur'));
						return false;
					}
					return false;
				}
				return false;
			}
		}

		public function fillACWithObjFromCC(myObj:Object):ArrayCollection
		{
			var myAC:ArrayCollection = new ArrayCollection();
			
			if (myObj.hasOwnProperty('col1'))
			{
				myAC.addItem(myObj.col1);
				if (myObj.hasOwnProperty('col2'))
				{
					myAC.addItem(myObj.col2);
					if (myObj.hasOwnProperty('col3'))
					{
						myAC.addItem(myObj.col3);
						if (myObj.hasOwnProperty('col4'))
						{
							myAC.addItem(myObj.col4);
							if (myObj.hasOwnProperty('col5'))
							{
								myAC.addItem(myObj.col5);
							}
						}
					}
				}
			}
			return myAC;
		}
		
		public function fillDG(myAC:ArrayCollection):void
		{
			var i:int=1;
			var nbLignesAjoutees:int=0;
			var nbLignes:int=myAC.length - 1;
			var j:int=0;
			var newArray:ArrayCollection=new ArrayCollection();
			var newLigne:NewLigneVO=new NewLigneVO("", "", "", "", "");
			if (nbLignes + dgImportLigneDP.length > 100)
			{
				ConsoviewAlert.afficherError(resourceManager.getString('M26', 'Impossible_d_ins_rer_plus_de_100_lignes___la_fois_'), resourceManager.getString('M26', 'Erreur'));
				return;
			}
			while (i < nbLignes)
			{
				j=0;
				while (j < 5)
				{
					switch (j)
					{
						case 0:
							newLigne.MATRICULE=myAC[i][j].value;
							break;
						case 1:
							newLigne.NOM=myAC[i][j].value;
							break;
						case 2:
							newLigne.PRENOM=myAC[i][j].value;
							break;
						case 3:
							newLigne.EMAIL=myAC[i][j].value;
							break;
						case 4://Si les numéros ne commencent pas par +, on insère
							//Sinon si les numéros commencent par cbIndicatif.selectedItem.data, on supprime cette partie dans le numéro et on insère
							//Sinon on insère tout en vrac et on se démerde pour afficher le warning rouge.
							if (myAC[i][j].value.toString().length > 0)
							{
								if (myAC[i][j].value.toString().charAt(0) == '0')
								{
									newLigne.TEL = myAC[i][j].value.toString().substr(1);//On insère tout le numéro à partir du deuxième caractère.
								}
								else if (myAC[i][j].value.toString().charAt(0) != '+')
								{
									newLigne.TEL = myAC[i][j].value.toString(); //OK
								}
								else if (myAC[i][j].value.toString().search(cbIndicatif.selectedItem.data) == 0)//Si l'indicatif se trouve dans le numéro a insérer
								{
									newLigne.TEL = myAC[i][j].value.toString().substr((cbIndicatif.selectedItem.data as String).length - 1);//On copie tout le numéro en retirant l'indicatif qui est déjà en label.
								}
								else	//Sinon, on insère tout y compris le faux indicatif et c'est à l'utilisateur de corriger.
									newLigne.TEL = myAC[i][j].value.toString();
							}
							else
								newLigne.TEL = myAC[i][j].value.toString();
							break;
						default:
							break;
					}
					j++;
				}
				if (newLigne.MATRICULE != "")
				{
					newArray.addItemAt(new NewLigneVO(newLigne.MATRICULE, newLigne.NOM, newLigne.PRENOM, newLigne.EMAIL, newLigne.TEL), nbLignesAjoutees);
					nbLignesAjoutees++;
				}
				i++;
			}
			dgImportLigneDP.addAllAt(newArray, dgImportLigneDP.length - 1); //on prend en compte le fait qu'on a un élément factice : on le supprime et on le recrée à la suite

			checkDoublonsForAllValues();
		}

		public function checkDoublonsForAllValues():void
		{
			var count:int=0;
			for (var i:int=0; i < dgImportLigneDP.length; i++)
			{
				for (var j:int=i + 1; j < dgImportLigneDP.length; j++)
				{
					if (dgImportLigneDP[i].TEL == dgImportLigneDP[j].TEL)
					{
						dgImportLigneDP[i].TELISDOUBLON=true;
						dgImportLigneDP[j].TELISDOUBLON=true;
						count++;
					}
				}
			}
			if (count == 0)
			{
				for (var k:int=0; k < dgImportLigneDP.length; k++)
				{
					dgImportLigneDP[k].TELISDOUBLON=false; //Pour l'import
				}
			}
		}

		public function checkDoublonsForOneValue(e:NewLigneEvent):void
		{
			e.objectReturn.TELISDOUBLON=false;
			
			if (e.objectReturn.TELISVALID)
				e.target.imgStatut.source=e.target.imgValid;
			
			for (var i:int=0; i < dgImportLigneDP.length; i++)
			{
				if (e.objectReturn !== dgImportLigneDP[i] && e.objectReturn.TEL == dgImportLigneDP[i].TEL && dgImportLigneDP[i].TEL!="")
				{
					e.objectReturn.TELISDOUBLON=true;
					e.target.imgStatut.source=e.target.imgInvalid;
					e.target.imgStatut.toolTip=resourceManager.getString('M26', 'Cette_ligne_est_en_doublon');
					dgImportLigneDP[i].TELISDOUBLON=true; //Pour le onChange
				}
			}
		}

		public function allowValidation(e:Event):void
		{
			var boolValid:Boolean=true;
			var i:int=0;
			btValider.enabled=false;

			//checkDoublonsForAllValues();
			while (i < dgImportLigneDP.length - 1)
			{
				var item:NewLigneVO=dgImportLigneDP[i];
				if (!item.MATRICULEISVALID || !item.NOMISVALID || !item.PRENOMISVALID || !item.EMAILISVALID || !item.TELISVALID || item.TELISDOUBLON)
				{
					boolValid=false;
					break;
				}
				i++;
			}
			/*for each (var item:NewLigneVO in dgImportLigneDP)
			{
			if (!item.MATRICULEISVALID || !item.NOMISVALID || !item.PRENOMISVALID || !item.EMAILISVALID || !item.TELISVALID || item.TELISDOUBLON)
			{
			boolValid = false;
			break;
			}
			}*/
			if (boolValid && dgImportLigneDP.length > 1)
			{
				btValider.enabled=true;
			}
		}

		public function imgDeleteClickHandler():void
		{
			if (dgImportLigne.selectedIndex != -1)
				dgImportLigneDP.removeItemAt(dgImportLigne.selectedIndex);
		}



		private function prepareForMultiThreading(arrayToSplit:Array):Array
		{
			var arrayToReturn:Array=new Array();
			if (arrayToSplit.length <= 5)
			{
				arrayToReturn[0]=JSON.encode(arrayToSplit);
				return arrayToReturn;
			}

			var i:int=0;
			var j:int=0;
			var k:int=0;
			var fragment:Array=new Array(); //fragment de tableau à convertir en JSON et à envoyer dans l'array de retour : ne contiendra jamais plus de 5 éléments
			while (i < arrayToSplit.length)
			{
				fragment[j]=arrayToSplit[i];
				j++;
				i++;
				if (j == 5)
				{
					j=0;
					arrayToReturn[k]=JSON.encode(fragment); //ajout d'une string JSON dans l'array de JSON
					fragment=new Array();
					k++;
				}

			}
			if (j > 0)
			{
				arrayToReturn[k]=JSON.encode(fragment);
				fragment=new Array();
			}
			this.requestsPending=arrayToReturn.length;
			return arrayToReturn;
		}

		protected function valider(event:Event):void
		{
			var myAC:ArrayCollection=new ArrayCollection();
			var i:int=0;

			for each (var item:NewLigneVO in dgImportLigneDP)
			{
				myAC.addItemAt(new AnnauireVO(item.MATRICULE, item.NOM, item.PRENOM, item.EMAIL, (cbIndicatif.selectedItem.data as String) + item.TEL), i);
				i++;
			}
			
			var ro:RemoteObject = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			ro.source = "fr.consotel.consoview.M26.services.annuaire.AnnuaireService";
			ro.addEventListener(ResultEvent.RESULT, sendEndingEvent);

			myAC.removeItemAt(myAC.length - 1);
			ro.addItems(prepareForMultiThreading(myAC.source), 0); //un array de JSON est envoye


			ConsoviewAlert.afficherOKImage(resourceManager.getString('M26', 'Demande_d_insertion_de_lignes_en_cours'));
			dispatchEvent(new NewLigneEvent(NewLigneEvent.REQUESTS_LAUNCHED, true, false, this.requestsPending));
			close();
		}
		
		public function sendEndingEvent(o:Object):void
		{
			var NLE:NewLigneEvent = new NewLigneEvent(NewLigneEvent.REQUESTS_FINISHED, false, false, o.result.BODY);
			dispatchEvent(NLE);
		}
		
		protected function downloadMatrice(e:Event):void
		{
			var _file:FileReference=new FileReference();
			if (CvAccessManager.getSession().USER.GLOBALIZATION == "fr_FR")
				_file.download(new URLRequest(RevisionM26.urlBackoffice + "/fichierModeleExcel/M26/Matrice_Import_Ligne_Datalert_FR.xls"));
			else if (CvAccessManager.getSession().USER.GLOBALIZATION == "en_US" || CvAccessManager.getSession().USER.GLOBALIZATION == "en_EU" || CvAccessManager.getSession().USER.GLOBALIZATION == "en_CH" || CvAccessManager.getSession().USER.GLOBALIZATION == "en_GB")
				_file.download(new URLRequest(RevisionM26.urlBackoffice + "/fichierModeleExcel/M26/Line_Import_Matrix_EN.xls"));
			else if (CvAccessManager.getSession().USER.GLOBALIZATION == "es_ES")
				_file.download(new URLRequest(RevisionM26.urlBackoffice + "/fichierModeleExcel/M26/Matriz_Import_Linea_Datalert_ES.xls"));
		}

		public function addItemsHandler(re:ResultEvent):void
		{
			if (re.result > 0)
			{
				ConsoviewAlert.afficherOKImage(resourceManager.getString('M26', 'Les_lignes_ont_bien__t__ajout_es'));
				close();
			}
			else
			{
				ConsoviewAlert.afficherError(resourceManager.getString('M26', 'Echec_de_l_insertion_des_lignes'), resourceManager.getString('M26', 'Erreur'));
			}
		}

		public function closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		public function close():void
		{
			PopUpManager.removePopUp(this);
		}
	}
}
