package datalert.ihm.actions
{
	import datalert.event.DatalertEvent;
	import datalert.services.DatalertCS;
	import datalert.vo.NotificationVO;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import rapport.utils.serialization.json.JSON;
	import rapport.utils.serialization.json.JSONEncoder;

	
	
	public class NotificationManagerImpl extends TitleWindow
	{
		[Bindable] public var paramNotif:NotificationVO;
		public	var listeMails:Array;		
	
		public var notifYes:		RadioButton;
		public var cbAlertNational:	CheckBox;
		public var cbAlertRoaming:	CheckBox;
		public var boxmail:			VBox;
		public var langBox:			VBox;
		
		public var datalertCS:DatalertCS;
		
		public function NotificationManagerImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE , creationCompleteHandler);
		}
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{	
			datalertCS= new DatalertCS();			
		}
				
		public function ValidNotifyMangaer():void
		{
			listeMails=new Array();			
			var mailChildren:Array = boxmail.getChildren();
			var langChildren:Array = langBox.getChildren();			
			for( var i:int=1;i <= mailChildren.length; i++)
			{					
				var item:Object= new Object();				
				if(TextInput(boxmail.getChildByName("txtmail"+i)).text!="")
				{
					item.lang=(ComboBox(FormItem(langChildren[i-1]).getChildByName("cbmail"+i)).selectedItem as Object).lang;
					item.email=TextInput(boxmail.getChildByName("txtmail"+i)).text;			
										
					listeMails[i-1]=item;
				}				
			}
			
			var encodedObjectString:String=JSON.encode(listeMails); 
			paramNotif.BOOLACTIVENOTIF=(notifYes.selected)?1:0;
			paramNotif.BOOLNOTIFDOM=(cbAlertNational.selected)?1:0;
			paramNotif.BOOLNOTIFROAM=(cbAlertRoaming.selected)?1:0;	
			
			datalertCS.addEventListener(DatalertEvent.NOTIFY_MANAGER_EVENT,updateNotificationManagerHandler);
			datalertCS.notifyManagers(encodedObjectString,paramNotif.BOOLACTIVENOTIF,paramNotif.BOOLNOTIFDOM,paramNotif.BOOLNOTIFROAM); 
		}
		
		private function updateNotificationManagerHandler(evt : DatalertEvent ):void
		{
			if(evt.objectReturn>0)
			{
				PopUpManager.removePopUp(this);
				dispatchEvent(new DatalertEvent(DatalertEvent.REFRESH_PARENT_DATA));
			}
		}
		
		protected function closeHandler(ce:Event):void
		{			
			PopUpManager.removePopUp(this);
		}
	}
}