package datalert.ihm.actions.editParam
{
	import composants.util.ConsoviewAlert;
	
	import datalert.event.DatalertEvent;
	import datalert.ihm.actions.DeployServiceIHM;
	import datalert.services.DatalertCS;
	import datalert.services.ParamSeuilCS;
	import datalert.vo.LigneVO;
	import datalert.vo.MessageVO;
	import datalert.vo.SeuilLigneVO;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.TitleWindow;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	public class EditSeuilsLigneImpl extends Box
	{
		
		[Bindable] public var cbxmessageDomDataProvider:ArrayCollection=new ArrayCollection();
		[Bindable] public var cbxmessageRoamDataProvider:ArrayCollection=new ArrayCollection();
		
		public var boolDeployService:Boolean=false;
		
		[Bindable] public var txtExplicParam:String = "";
		[Bindable] public var paramSeuilLigne:SeuilLigneVO;
		[Bindable] public var ligneseuil:LigneVO;		
		[Bindable] public var listLigne:ArrayCollection;
		[Bindable] public var listIdSoustete:ArrayCollection;		
		public var paramseuilCS:ParamSeuilCS;
		public var datalertCS:DatalertCS;
		
		[Bindable] public var cbIllimiteDom:CheckBox;
		[Bindable] public var cbIllimiteRoam:CheckBox;		
		public var cbFairUseDom:CheckBox;
		public var txtplanRoam:TextInput;
		public var txtplanDom:TextInput;
		public var txtseuilRoam:TextInput;
		public var txtseuilDom:TextInput;
		public var cbJourFacturaion:ComboBox;
		public var cbxMessageDom:ComboBox;
		public var cbxMessageRoam:ComboBox;
		public var cbxFormatData1:ComboBox;
		public var cbxFormatData2:ComboBox;
		
		[Bindable]			
		public var cbxFormatDataProvider:ArrayCollection = new ArrayCollection([
			{label:ResourceManager.getInstance().getString('M26', 'Mo'), data:"0"},
			{label:ResourceManager.getInstance().getString('M26', 'Go'), data:"1"}
		]);
		
		
		public function EditSeuilsLigneImpl()
		{
			datalertCS= new DatalertCS();
			datalertCS.searchDefaultsMessages(0,0);
			datalertCS.addEventListener(DatalertEvent.DEFAULTMESSAGE_EVENT,getListMessageDom);
		}
		
		private function getListMessageDom(e:DatalertEvent):void
		{
			var x:MessageVO=new MessageVO();
			x.LIBELLE=ResourceManager.getInstance().getString('M26', 'Select_message');
			x.IDMESSAGE=0;	
			cbxmessageDomDataProvider.addItem(x);
			cbxmessageRoamDataProvider.addItem(x);			
			for(var i : int=0; i < datalertCS.listeMessage.length;i++)
			{ 
				if((datalertCS.listeMessage.getItemAt(i) as MessageVO).BOOLMESSEAGEDOM == 1)
					cbxmessageDomDataProvider.addItem(datalertCS.listeMessage.getItemAt(i));
				
				if((datalertCS.listeMessage.getItemAt(i) as MessageVO).BOOLMESSEAGEROAM == 1)
					cbxmessageRoamDataProvider.addItem(datalertCS.listeMessage.getItemAt(i));
			}
		}
		
		public function setCurrentData(paramSeuilLigne:SeuilLigneVO, paramSeuilCS:ParamSeuilCS, boolDeployService:Boolean, ligneseuil:LigneVO, listIdSoustete:ArrayCollection):void
		{
			//infos à modifier ici
			//paramSeuilLigne="{this.paramSeuilLigne}" paramseuilCS="{this.paramseuilCS}" 
			//boolDeployService="{this.boolDeployService}" ligneseuil="{this.ligneseuil}" listIdSoustete="{this.listIdSoustete}"
			this.ligneseuil = ligneseuil;
			this.paramSeuilLigne = paramSeuilLigne;
			this.paramseuilCS = paramSeuilCS;
			this.boolDeployService = boolDeployService;
			this.listIdSoustete = listIdSoustete;
			
		}
		
		/**
		 * Mise à jour des parametres Seuil		 
		 */
		
		public function updatePlanSeuil():void
		{				
			ligneseuil.JOURFACTURATION=Number(cbJourFacturaion.selectedIndex+1);
			ligneseuil.DOMESTIQUEPLAN=((cbIllimiteDom.selected) && !(cbFairUseDom.selected))?-1:Number(txtplanDom.text);
			ligneseuil.FORMATDOMESTIQUEPLAN = cbxFormatData1.selectedIndex;
			
			ligneseuil.ROAMINGPLAN=(cbIllimiteRoam.selected)?-1:Number(txtplanRoam.text);
			ligneseuil.FORMATROAMINGPLAN = cbxFormatData2.selectedIndex;
			
			ligneseuil.BOOLFAIRUSEDOM=(cbFairUseDom.selected)?1:0;
			
			
			/*var o:Object = new Object();
			o.listIdSoustete = listIdSoustete;
			o.ligneseuil = ligneseuil;
			o.paramSeuilLigne = paramSeuilLigne;
			o.domIllimite = cbIllimiteDom.selected;
			o.romIllimite = cbIllimiteRoam.selected;
			dispatchEvent(new DatalertEvent("MASSEPARAM_NEXT", false, false, o));
			/*********************
			paramseuilCS.addEventListener(DatalertEvent.UPDATE_SEUILLIGNE_EVENT,update_seuilLigne_handler);
			paramseuilCS.updateParamSeuilLigne(listIdSoustete,ligneseuil,paramSeuilLigne);*/
		}
		
		private function update_seuilLigne_handler(evt : DatalertEvent):void
		{			
			if(evt.objectReturn> 0)
			{				
				if(boolDeployService)
				{
					var service:DeployServiceIHM= new DeployServiceIHM();
					service.listeLigne=listLigne;
					PopUpManager.addPopUp(service, this, true);
					PopUpManager.centerPopUp(service);	
				}else
				{
					ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M26', 'Ligne_modifi_e'));				
				}
				dispatchEvent(new DatalertEvent(DatalertEvent.REFRESH_PARENT_DATA));
			}			
			PopUpManager.removePopUp(this);
		}
		
		protected function closeHandler(ce:Event):void
		{		
			PopUpManager.removePopUp(this);
		}
	}
}