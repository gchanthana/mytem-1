package datalert.ihm.actions.editParam
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.RadioButton;
	import mx.controls.Text;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import datalert.event.DatalertEvent;
	import datalert.ihm.datalertRtIHM;
	import datalert.ihm.actions.DeployServiceIHM;
	import datalert.itemRenderer.BlueArrow;
	import datalert.services.DatalertCS;
	import datalert.services.ParamAlertesService;
	import datalert.services.ParamSeuilCS;
	import datalert.vo.AlertesVO;
	import datalert.vo.LigneVO;
	import datalert.vo.SeuilLigneVO;
	
	import rapport.utils.serialization.json.JSON;
	
	[Bindable]
	public class EditAlertesLigneImpl extends Box
	{
		public var datalertCSDom:DatalertCS;
		public var datalertCSRoam:DatalertCS;
		public var libellesMessagesDom:ArrayCollection = new ArrayCollection();
		public var libellesMessagesRoam:ArrayCollection = new ArrayCollection();
		public var ligne:LigneVO;
		
		/*Récupéré depuis la partie forfaits de la popup*/
		public var listIdSoustete:ArrayCollection;
		public var ligneseuil:LigneVO;
		public var paramSeuilLigne:SeuilLigneVO;
		public var boolDeployService:Boolean;
		public var boolAllSearch:Boolean;
		public var boolIsMassUpdating:Boolean=false;
		public var listLigne:ArrayCollection;
		
		public var paramseuilCS:ParamSeuilCS;
		
		
		public var chbMessageUserDom1:CheckBox;
		public var cbMessageUserDom1:ComboBox;
		public var chbMessageUserDom2:CheckBox;
		public var cbMessageUserDom2:ComboBox;
		public var chbMessageUserDom3:CheckBox;
		public var cbMessageUserDom3:ComboBox;
		public var chbMessageUserRoam1:CheckBox;
		public var cbMessageUserRoam1:ComboBox;
		public var chbMessageUserRoam2:CheckBox;
		public var cbMessageUserRoam2:ComboBox;
		public var chbMessageUserRoam3:CheckBox;
		public var cbMessageUserRoam3:ComboBox;
		public var cbActivAlerteDom1:CheckBox;
		public var cbActivAlerteDom2:CheckBox;
		public var cbActivAlerteDom3:CheckBox;
		public var cbActivAlerteRoam1:CheckBox;
		public var cbActivAlerteRoam2:CheckBox;
		public var cbActivAlerteRoam3:CheckBox;
		public var tiMontantData1:TextInput;
		public var tiMontantData2:TextInput;
		public var tiMontantData3:TextInput;
		public var tiMontantDataRoam1:TextInput;
		public var tiMontantDataRoam2:TextInput;
		public var tiMontantDataRoam3:TextInput;
		public var cbxFormatData1:ComboBox;
		public var cbxFormatData2:ComboBox;
		public var cbxFormatData3:ComboBox;
		public var cbxFormatDataRoam1:ComboBox;
		public var cbxFormatDataRoam2:ComboBox;
		public var cbxFormatDataRoam3:ComboBox;
		public var cbFrequenceAlerte1:ComboBox;
		public var cbFrequenceAlerte2:ComboBox;
		public var cbFrequenceAlerte3:ComboBox;
		public var cbFrequenceAlerteRoam1:ComboBox;
		public var cbFrequenceAlerteRoam2:ComboBox;
		public var cbFrequenceAlerteRoam3:ComboBox;
		public var chbMessageGestDom1:CheckBox;
		public var chbMessageGestDom2:CheckBox;
		public var chbMessageGestDom3:CheckBox;
		public var chbMessageGestRoam1:CheckBox;
		public var chbMessageGestRoam2:CheckBox;
		public var chbMessageGestRoam3:CheckBox;
		public var chbCouperData1:CheckBox;
		public var chbCouperData2:CheckBox;
		public var chbCouperData3:CheckBox;
		public var chbCouperDataRoam1:CheckBox;
		public var chbCouperDataRoam2:CheckBox;
		public var chbCouperDataRoam3:CheckBox;
		public var chbActiverControleAucuneInfo:CheckBox;
		public var tiTempsSansInfo:TextInput;
		public var rbgTempsSansInfo1:RadioButton;
		public var rbgTempsSansInfo2:RadioButton;
		public var chbMessageUserSansInfo:CheckBox;
		public var cbMessageUserSansInfo:ComboBox;
		public var chbMessageGestSansInfo:CheckBox;
		public var chbFrequenceSansInfo:CheckBox;
		public var tiFrequenceActions:TextInput;
		public var rbFrequenceActions1:RadioButton;
		public var rbFrequenceActions2:RadioButton;
		public var chbActiverControleReactivation:CheckBox;
		public var cbMessageUserReactivation:ComboBox;
		public var chbMessageUserReactivation:CheckBox;
		public var chbMessageGestReactivation:CheckBox;
		public var chbCouperDataReactivation:CheckBox;
		public var chbFrequenceAlerte:CheckBox;
		public var tiFrequenceAlerte:TextInput;
		public var rbFrequenceAlerte1:RadioButton;
		public var rbFrequenceAlerte2:RadioButton;
		public var blueArrowDom:BlueArrow;
		public var blueArrowRoam:BlueArrow;
		public var TAUdd:Text;
		public var TAUdr:Text;
		
		//les 5 var privées servent lors du remplissage du JSON avant envoi
		private var typePeriode:Number;//0 : quotidien, 1 : Hebdomadaire, 2 : Mensuel
		private var limit:Number; //en ko, limite de données utilisables avant lancement d'alertes
		private var typeAlerte:String;// C pour %, U pour Mo ou Go
		private var idmessage_user:Number;
		private var o:Object;
		
		private var paramAlertesService:ParamAlertesService = new ParamAlertesService();
		
		public var paramAlertes:AlertesVO = new AlertesVO();
		
					
		public var cbxFormatDataDomProvider:ArrayCollection = new ArrayCollection([
			{label:ResourceManager.getInstance().getString('M26', 'Mo'), data:"0"},
			{label:ResourceManager.getInstance().getString('M26', 'Go'), data:"1"},
			{label:"%", data:"2"}
		]);
					
		public var cbxFormatDataRoamProvider:ArrayCollection = new ArrayCollection([
			{label:ResourceManager.getInstance().getString('M26', 'Mo'), data:"0"},
			{label:ResourceManager.getInstance().getString('M26', 'Go'), data:"1"},
			{label:"%", data:"2"}
		]);
		
		public var cbFrequenceAlerteProvider:ArrayCollection = new ArrayCollection([
			{label:ResourceManager.getInstance().getString('M26', 'Jour'), data:"0"},
			{label:ResourceManager.getInstance().getString('M26', 'Semaine'), data:"1"},
			{label:ResourceManager.getInstance().getString('M26', 'Mois_sing'), data:"2"}
		]);
		
		public function EditAlertesLigneImpl()
		{
			
		}
		
		protected function creationCompleteHandler(event:FlexEvent):void
		{
			paramAlertesService.ligne = ligne;
			paramAlertesService.addEventListener("JSONRECIEVED", jsonRecieved_Handler);
			
			//Pour les messages en domestique
			datalertCSDom = new DatalertCS();
			datalertCSDom.addEventListener(DatalertEvent.DEFAULTMESSAGE_EVENT, fillCBMessagesDomHandler);
			fillCBMessagesDom();
			//Pour les messages en roaming
			datalertCSRoam = new DatalertCS();
			datalertCSRoam.addEventListener(DatalertEvent.DEFAULTMESSAGE_EVENT, fillCBMessagesRoamHandler);
			fillCBMessagesRoam();			
			this.addEventListener(FlexEvent.INITIALIZE, init);
			this.dispatchEvent(new FlexEvent(FlexEvent.INITIALIZE));
			
		}	
		
		protected function init(event:FlexEvent):void
		{
			//paramAlertesService.retrieveParams();//appel à la BDD pour récupérer les données, s'il y en a
			
			cbActivAlerteDom1_ChangeHandler();
			cbActivAlerteDom2_ChangeHandler();
			cbActivAlerteDom3_ChangeHandler();
			cbActivAlerteRoam1_ChangeHandler();
			cbActivAlerteRoam2_ChangeHandler();
			cbActivAlerteRoam3_ChangeHandler();
			chbActiverControleAucuneInfo_ChangeHandler();
			chbActiverControleReactivation_ChangeHandler();	
			TAUdd.height = (TAUdd.textHeight) + TAUdd.getLineMetrics(0).height + 6;
			TAUdr.height = (TAUdr.textHeight) + TAUdr.getLineMetrics(0).height + 6;
		}
		
		public function setCurrentData(value:ArrayCollection):void
		{
			
		}
		
		public function fillCBMessagesDom():void
		{
			datalertCSDom.searchDefaultsMessages(1,0);			
		}
		
		public function fillCBMessagesDomHandler(e:DatalertEvent):void
		{
			for each (var o:Object in datalertCSDom.listeMessage)
			{
				libellesMessagesDom.addItem(o.LIBELLE);
			}
		}
		public function fillCBMessagesRoam():void
		{
			datalertCSRoam.searchDefaultsMessages(0,1);
		}
		public function fillCBMessagesRoamHandler(e:DatalertEvent):void
		{
			for each (var o:Object in datalertCSRoam.listeMessage)
			{
				libellesMessagesRoam.addItem(o.LIBELLE);
			}
		}
		
		
		private function fillSeuils():void
		{
			if (cbActivAlerteDom1.selected) fillDom1();
			if (cbActivAlerteDom2.selected) fillDom2();
			if (cbActivAlerteDom3.selected)	fillDom3();
			if (cbActivAlerteRoam1.selected) fillRoam1();
			if (cbActivAlerteRoam2.selected) fillRoam2();
			if (cbActivAlerteRoam3.selected) fillRoam3();
		}
		public function fillParamAlertes():void
		{//fonction servant a préparer le JSON pour envoi à la BDD
			fillSeuils();
			//On passe aux deux formulaires du bas
			var delai:Number;
			if (rbgTempsSansInfo1.selected) delai = parseInt(tiTempsSansInfo.text);
			else if (rbgTempsSansInfo2.selected) delai = parseInt(tiTempsSansInfo.text) * 24;
			for each (o in datalertCSDom.listeMessage)
			{
				if (cbMessageUserSansInfo.selectedItem == o.LIBELLE)
				{
					idmessage_user = o.IDMESSAGE;
					break;
				}
			}
			var frequence:Number;
			if (rbFrequenceActions1.selected) frequence = parseInt(tiFrequenceActions.text);
			else if (rbFrequenceActions2.selected) frequence = parseInt(tiFrequenceActions.text) * 24;
			paramAlertes.fillControleAcces(chbActiverControleAucuneInfo.selected?1:0, delai,
				chbMessageUserSansInfo.selected?idmessage_user:undefined, chbMessageGestSansInfo.selected?1:0, chbFrequenceSansInfo.selected?frequence:undefined);
			
			// On re-recupère les valeurs à envoyer au préalable, on recycle les variables
			for each (o in datalertCSDom.listeMessage)
			{
				if (cbMessageUserReactivation.selectedItem == o.LIBELLE)
				{
					idmessage_user = o.IDMESSAGE;
					break;
				}
			}
			if (rbFrequenceAlerte1.selected) frequence = parseInt(tiFrequenceAlerte.text);
			else if (rbFrequenceAlerte2.selected) frequence = parseInt(tiFrequenceAlerte.text) * 24;
			paramAlertes.fillControleData(chbActiverControleReactivation.selected?1:0, chbMessageUserReactivation.selected?idmessage_user:undefined, chbMessageGestReactivation.selected?1:0, 
				chbCouperDataReactivation.selected?1:0, chbFrequenceAlerte.selected?frequence:undefined);
		}
		
		public function encodeJSON(o:Object):String
		{
			trace(JSON.encode(o));
			
			return (JSON.encode(o));
		}
		
		private function jsonRecieved_Handler(e:Event):void
		{
			//on décode le JSON puis on affiche ce que l'object obtenu contient
			writeParams(JSON.decode(paramAlertesService.jsonRecieved));
			//on applique les règles de gestion aux modifications
			cbActivAlerteDom1_ChangeHandler();
			cbActivAlerteDom2_ChangeHandler();
			cbActivAlerteDom3_ChangeHandler();
			cbActivAlerteRoam1_ChangeHandler();
			cbActivAlerteRoam2_ChangeHandler();
			cbActivAlerteRoam3_ChangeHandler();
			chbActiverControleAucuneInfo_ChangeHandler();
			chbActiverControleReactivation_ChangeHandler();	
		}
		private function writeParamsControleAcces(o:Object):void
		{
			//partie o.controle_acces (gauche)
			if (o.controle_acces.is_actif == 1)
				chbActiverControleAucuneInfo.selected = true;
			if (o.controle_acces.delai != null)
			{
				if (parseInt(o.controle_acces.delai) % 24 == 0)
				{
					tiTempsSansInfo.text = String(parseInt(o.controle_acces.delai) / 24);
					rbgTempsSansInfo2.selected = true;
				}
				else 
				{
					tiTempsSansInfo.text = o.controle_acces.delai;
					rbgTempsSansInfo1.selected = true;
				}
			}
			if (o.controle_acces.send_message_gest == 1)
				chbMessageGestSansInfo.selected = true;
			if (o.controle_acces.idmessage_user != null)
			{
				chbMessageUserSansInfo.selected = true;
				for each (var ob:Object in datalertCSDom.listeMessage)
				{
					if (o.controle_acces.idmessage_user == ob.IDMESSAGE)
					{
						cbMessageUserSansInfo.selectedItem = ob.LIBELLE;
						break;
					}
				}
			}
			if (o.controle_acces.repeat != null)
			{
				chbFrequenceSansInfo.selected = true;
				if (parseInt(o.controle_acces.repeat) % 24 == 0)
				{
					tiFrequenceActions.text = String(parseInt(o.controle_acces.repeat) / 24);
					rbFrequenceActions2.selected = true;
				}
				else
				{
					tiFrequenceActions.text = o.controle_acces.repeat;
					rbFrequenceActions1.selected = true;
				}
			}
		}
		
		private function writeParamsControleData(o:Object):void
		{
			//partie o.controle_data (droite)
			if (o.controle_data.is_actif == 1)
				chbActiverControleReactivation.selected = true;
			if (o.controle_data.send_message_gest == 1)
				chbMessageGestReactivation.selected = true;
			if (o.controle_data.idmessage_user != null)
			{
				chbMessageUserReactivation.selected = true;
				for each (var ob:Object in datalertCSDom.listeMessage)
				{
					if (o.controle_data.idmessage_user == ob.IDMESSAGE)
					{
						cbMessageUserReactivation.selectedItem = ob.LIBELLE;
						break;
					}
				}
			}
			if (o.controle_data.repeat != null)
			{
				chbFrequenceAlerte.selected = true;
				if(parseInt(o.controle_data.repeat) % 24 == 0)
				{
					tiFrequenceAlerte.text = String(parseInt(o.controle_data.repeat) / 24);
					rbFrequenceAlerte2.selected = true;
				}
				else
				{
					tiFrequenceAlerte.text = o.controle_data.repeat;
					rbFrequenceAlerte1.selected = true;
				}
			}
			if (o.controle_data.disable_data == 1)
				chbCouperDataReactivation.selected = true;
		}
		private function writeParams(o:Object):void
		{//fonction permettant l'affichage après réception des données de la BDD. o contient l'object après parsing du JSON.
			var i:Number;
			var numDom:Number = 1;
			var numRoam:Number = 1;
			writeParamsControleAcces(o);
			writeParamsControleData(o);
			if (o.hasOwnProperty("seuils"))
			{
				if (o.seuils.length > 0)
				{
					i = 0;
					while (i < o.seuils.length)
					{
						if (o.seuils[i].is_roaming == 1)
						{
							switch (numRoam)
							{
								case 1:
									loadRoam1(o.seuils[i]);
									break;
								case 2:
									loadRoam2(o.seuils[i]);
									break;
								case 3:
									loadRoam3(o.seuils[i]);
									break;
								default:
									break;
							}
							numRoam++;
						}
						else
						{
							switch (numDom)
							{
								case 1:
									loadDom1(o.seuils[i]);
									break;
								case 2:
									loadDom2(o.seuils[i]);
									break;
								case 3:
									loadDom3(o.seuils[i]);
									break;
								default:
									break;
							}
							numDom++;
						}
						i++;
					}	
				}
			}
		}
		private function loadRoam1(o:Object):void
		{
			cbActivAlerteRoam1.selected = true; //si fonction appelée, c'est que ça doit etre coché
			if (o.type_alerte == "C")
			{
				cbxFormatDataRoam1.selectedIndex = 2;
				tiMontantDataRoam1.text = o.limit.toString();
			}
			else //"U"
			{//on essaie de savoir si c'était Mo ou Go de sélectionné selon la taille des données
				if (parseInt(o.limit) > 1048576)
				{//Go
					cbxFormatDataRoam1.selectedIndex = 1;
					tiMontantDataRoam1.text = String(parseInt(o.limit) / 1048576);
				}
				else 
				{//Mo
					cbxFormatDataRoam1.selectedIndex = 0;
					tiMontantDataRoam1.text = String(parseInt(o.limit) / 1024); 
				}
			}
			if(o.idmessage_user != null)
			{
				chbMessageUserRoam1.selected = true;
				for each (var ob:Object in datalertCSRoam.listeMessage)
				{
					if (o.idmessage_user == ob.IDMESSAGE)
					{
						cbMessageUserRoam1.selectedItem = ob.LIBELLE;
						break;
					}
				}
			}
			cbFrequenceAlerteRoam1.selectedIndex = o.type_periode;
			if(o.send_message_gest == 1) chbMessageGestRoam1.selected = true;
			if(o.disable_data == 1) chbCouperDataRoam1.selected = true;
		}
		private function loadRoam2(o:Object):void
		{
			cbActivAlerteRoam2.selected = true; //si fonction appelée, c'est que ça doit etre coché
			if (o.type_alerte == "C")
			{
				cbxFormatDataRoam2.selectedIndex = 2;
				tiMontantDataRoam2.text = o.limit.toString();
			}
			else //"U"
			{//on essaie de savoir si c'était Mo ou Go de sélectionné selon la taille des données
				if (parseInt(o.limit) > 1048576)
				{//Go
					cbxFormatDataRoam2.selectedIndex = 1;
					tiMontantDataRoam2.text = String(parseInt(o.limit) / 1048576);
				}
				else 
				{//Mo
					cbxFormatDataRoam2.selectedIndex = 0;
					tiMontantDataRoam2.text = String(parseInt(o.limit) / 1024); 
				}
			}
			if(o.idmessage_user != null)
			{
				chbMessageUserRoam2.selected = true;
				for each (var ob:Object in datalertCSRoam.listeMessage)
				{
					if (o.idmessage_user == ob.IDMESSAGE)
					{
						cbMessageUserRoam2.selectedItem = ob.LIBELLE;
						break;
					}
				}
			}
			cbFrequenceAlerteRoam2.selectedIndex = o.type_periode;
			if(o.send_message_gest == 1) chbMessageGestRoam2.selected = true;
			if(o.disable_data == 1) chbCouperDataRoam2.selected = true;
		}
		private function loadRoam3(o:Object):void
		{
			cbActivAlerteRoam3.selected = true; //si fonction appelée, c'est que ça doit etre coché
			if (o.type_alerte == "C")
			{
				cbxFormatDataRoam3.selectedIndex = 2;
				tiMontantDataRoam3.text = o.limit.toString();
			}
			else //"U"
			{//on essaie de savoir si c'était Mo ou Go de sélectionné selon la taille des données
				if (parseInt(o.limit) > 1048576)
				{//Go
					cbxFormatDataRoam3.selectedIndex = 1;
					tiMontantDataRoam3.text = String(parseInt(o.limit) / 1048576);
				}
				else 
				{//Mo
					cbxFormatDataRoam3.selectedIndex = 0;
					tiMontantDataRoam3.text = String(parseInt(o.limit) / 1024); 
				}
			}
			if(o.idmessage_user != null)
			{
				chbMessageUserRoam3.selected = true;
				for each (var ob:Object in datalertCSRoam.listeMessage)
				{
					if (o.idmessage_user == ob.IDMESSAGE)
					{
						cbMessageUserRoam3.selectedItem = ob.LIBELLE;
						break;
					}
				}
			}
			cbFrequenceAlerteRoam3.selectedIndex = o.type_periode;
			if(o.send_message_gest == 1) chbMessageGestRoam3.selected = true;
			if(o.disable_data == 1) chbCouperDataRoam3.selected = true;
		}
		private function loadDom1(o:Object):void
		{
			cbActivAlerteDom1.selected = true; //si fonction appelée, c'est que ça doit etre coché
			if (o.type_alerte == "C")
			{
				cbxFormatData1.selectedIndex = 2;
				tiMontantData1.text = o.limit.toString();
			}
			else //"U"
			{//on essaie de savoir si c'était Mo ou Go de sélectionné selon la taille des données
				if (parseInt(o.limit) > 1048576)
				{//Go
					cbxFormatData1.selectedIndex = 1;
					tiMontantData1.text = String(parseInt(o.limit) / 1048576);
				}
				else 
				{//Mo
					cbxFormatData1.selectedIndex = 0;
					tiMontantData1.text = String(parseInt(o.limit) / 1024); 
				}
			}
			if(o.idmessage_user != null)
			{
				chbMessageUserDom1.selected = true;
				for each (var ob:Object in datalertCSDom.listeMessage)
				{
					if (o.idmessage_user == ob.IDMESSAGE)
					{
						cbMessageUserDom1.selectedItem = ob.LIBELLE;
						break;
					}
				}
			}
			cbFrequenceAlerte1.selectedIndex = o.type_periode;
			if(o.send_message_gest == 1) chbMessageGestDom1.selected = true;
			if(o.disable_data == 1) chbCouperData1.selected = true;
		}
		private function loadDom2(o:Object):void
		{
			cbActivAlerteDom2.selected = true; //si fonction appelée, c'est que ça doit etre coché
			if (o.type_alerte == "C")
			{
				cbxFormatData2.selectedIndex = 2;
				tiMontantData2.text = o.limit.toString();
			}
			else //"U"
			{//on essaie de savoir si c'était Mo ou Go de sélectionné selon la taille des données
				if (parseInt(o.limit) > 1048576)
				{//Go
					cbxFormatData2.selectedIndex = 1;
					tiMontantData2.text = String(parseInt(o.limit) / 1048576);
				}
				else 
				{//Mo
					cbxFormatData2.selectedIndex = 0;
					tiMontantData2.text = String(parseInt(o.limit) / 1024); 
				}
			}
			if(o.idmessage_user != null)
			{
				chbMessageUserDom2.selected = true;
				for each (var ob:Object in datalertCSDom.listeMessage)
				{
					if (o.idmessage_user == ob.IDMESSAGE)
					{
						cbMessageUserDom2.selectedItem = ob.LIBELLE;
						break;
					}
				}
			}
			cbFrequenceAlerte2.selectedIndex = o.type_periode;
			if(o.send_message_gest == 1) chbMessageGestDom2.selected = true;
			if(o.disable_data == 1) chbCouperData2.selected = true;
		}
		private function loadDom3(o:Object):void
		{
			cbActivAlerteDom3.selected = true; //si fonction appelée, c'est que ça doit etre coché
			if (o.type_alerte == "C")
			{
				cbxFormatData3.selectedIndex = 2;
				tiMontantData3.text = o.limit.toString();
			}
			else //"U"
			{//on essaie de savoir si c'était Mo ou Go de sélectionné selon la taille des données
				if (parseInt(o.limit) > 1048576)
				{//Go
					cbxFormatData3.selectedIndex = 1;
					tiMontantData3.text = String(parseInt(o.limit) / 1048576);
				}
				else 
				{//Mo
					cbxFormatData3.selectedIndex = 0;
					tiMontantData3.text = String(parseInt(o.limit) / 1024); 
				}
			}
			if(o.idmessage_user != null)
			{
				chbMessageUserDom3.selected = true;
				for each (var ob:Object in datalertCSDom.listeMessage)
				{
					if (o.idmessage_user == ob.IDMESSAGE)
					{
						cbMessageUserDom3.selectedItem = ob.LIBELLE;
						break;
					}
				}
			}
			cbFrequenceAlerte3.selectedIndex = o.type_periode;
			if(o.send_message_gest == 1) chbMessageGestDom3.selected = true;
			if(o.disable_data == 1) chbCouperData3.selected = true;
		}
		/*****************************************************************
		 * ChangeHandlers
		 ****************************************************************/
		
		public function cbActivAlerteDom1_ChangeHandler():void
		{
			if (cbActivAlerteDom1.selected == false)
			{
				tiMontantData1.enabled = false;
				blueArrowDom.changeGrey1b();
				cbxFormatData1.enabled = false;
				cbFrequenceAlerte1.enabled = false;
				chbMessageUserDom1.enabled = false;
				cbMessageUserDom1.enabled = false;
				chbMessageGestDom1.enabled = false;
				chbCouperData1.enabled = false;	
			}
			else
			{
				tiMontantData1.enabled = true;
				blueArrowDom.changeWhite1b();
				cbxFormatData1.enabled = true;
				cbFrequenceAlerte1.enabled = true;
				chbMessageUserDom1.enabled = true;
				cbMessageUserDom1.enabled = true;
				chbMessageGestDom1.enabled = true;
				chbCouperData1.enabled = true;
				chbMessageUser_ChangeHandler();
				tiMontantData1.setFocus();
			}
		}
		public function cbActivAlerteDom2_ChangeHandler():void
		{
			if (cbActivAlerteDom2.selected == false)
			{
				tiMontantData2.enabled = false;
				blueArrowDom.changeGrey2b();
				cbxFormatData2.enabled = false;
				cbFrequenceAlerte2.enabled = false;
				chbMessageUserDom2.enabled = false;
				cbMessageUserDom2.enabled = false;
				chbMessageGestDom2.enabled = false;
				chbCouperData2.enabled = false;	
			}
			else
			{
				tiMontantData2.enabled = true;
				blueArrowDom.changeWhite2b();
				cbxFormatData2.enabled = true;
				cbFrequenceAlerte2.enabled = true;
				chbMessageUserDom2.enabled = true;
				cbMessageUserDom2.enabled = true;
				chbMessageGestDom2.enabled = true;
				chbCouperData2.enabled = true;
				chbMessageUser_ChangeHandler();
				tiMontantData2.setFocus();
			}
		}
		public function cbActivAlerteDom3_ChangeHandler():void
		{
			if (cbActivAlerteDom3.selected == false)
			{
				tiMontantData3.enabled = false;
				blueArrowDom.changeGrey3b();
				cbxFormatData3.enabled = false;
				cbFrequenceAlerte3.enabled = false;
				chbMessageUserDom3.enabled = false;
				cbMessageUserDom3.enabled = false;
				chbMessageGestDom3.enabled = false;
				chbCouperData3.enabled = false;	
			}
			else
			{
				tiMontantData3.enabled = true;
				blueArrowDom.changeWhite3b();
				cbxFormatData3.enabled = true;
				cbFrequenceAlerte3.enabled = true;
				chbMessageUserDom3.enabled = true;
				cbMessageUserDom3.enabled = true;
				chbMessageGestDom3.enabled = true;
				chbCouperData3.enabled = true;
				chbMessageUser_ChangeHandler();
				tiMontantData3.setFocus();
			}
		}
		public function cbActivAlerteRoam1_ChangeHandler():void
		{
			if (cbActivAlerteRoam1.selected == false)
			{
				tiMontantDataRoam1.enabled = false;
				blueArrowRoam.changeGrey1b();
				cbxFormatDataRoam1.enabled = false;
				cbFrequenceAlerteRoam1.enabled = false;
				chbMessageUserRoam1.enabled = false;
				cbMessageUserRoam1.enabled = false;
				chbMessageGestRoam1.enabled = false;
				chbCouperDataRoam1.enabled = false;	
			}
			else
			{
				tiMontantDataRoam1.enabled = true;
				blueArrowRoam.changeWhite1b();
				cbxFormatDataRoam1.enabled = true;
				cbFrequenceAlerteRoam1.enabled = true;
				chbMessageUserRoam1.enabled = true;
				cbMessageUserRoam1.enabled = true;
				chbMessageGestRoam1.enabled = true;
				chbCouperDataRoam1.enabled = true;
				chbMessageUserRoam_ChangeHandler();
				tiMontantDataRoam1.setFocus();
			}
		}
		public function cbActivAlerteRoam2_ChangeHandler():void
		{
			if (cbActivAlerteRoam2.selected == false)
			{
				tiMontantDataRoam2.enabled = false;
				blueArrowRoam.changeGrey2b();
				cbxFormatDataRoam2.enabled = false;
				cbFrequenceAlerteRoam2.enabled = false;
				chbMessageUserRoam2.enabled = false;
				cbMessageUserRoam2.enabled = false;
				chbMessageGestRoam2.enabled = false;
				chbCouperDataRoam2.enabled = false;	
			}
			else
			{
				tiMontantDataRoam2.enabled = true;
				blueArrowRoam.changeWhite2b();
				cbxFormatDataRoam2.enabled = true;
				cbFrequenceAlerteRoam2.enabled = true;
				chbMessageUserRoam2.enabled = true;
				cbMessageUserRoam2.enabled = true;
				chbMessageGestRoam2.enabled = true;
				chbCouperDataRoam2.enabled = true;
				chbMessageUserRoam_ChangeHandler();
				tiMontantDataRoam2.setFocus();
			}
		}
		public function cbActivAlerteRoam3_ChangeHandler():void
		{
			if (cbActivAlerteRoam3.selected == false)
			{
				tiMontantDataRoam3.enabled = false;
				blueArrowRoam.changeGrey3b();
				cbxFormatDataRoam3.enabled = false;
				cbFrequenceAlerteRoam3.enabled = false;
				chbMessageUserRoam3.enabled = false;
				cbMessageUserRoam3.enabled = false;
				chbMessageGestRoam3.enabled = false;
				chbCouperDataRoam3.enabled = false;	
			}
			else
			{
				tiMontantDataRoam3.enabled = true;
				blueArrowRoam.changeWhite3b();
				cbxFormatDataRoam3.enabled = true;
				cbFrequenceAlerteRoam3.enabled = true;
				chbMessageUserRoam3.enabled = true;
				cbMessageUserRoam3.enabled = true;
				chbMessageGestRoam3.enabled = true;
				chbCouperDataRoam3.enabled = true;
				chbMessageUserRoam_ChangeHandler();
				tiMontantDataRoam3.setFocus();
			}
		}
		public function chbActiverControleAucuneInfo_ChangeHandler():void
		{
			if (chbActiverControleAucuneInfo.selected == false)
			{
				tiTempsSansInfo.enabled = false;
				rbgTempsSansInfo1.enabled = false;
				rbgTempsSansInfo2.enabled = false;
				chbMessageUserSansInfo.enabled = false;
				cbMessageUserSansInfo.enabled = false;
				chbMessageGestSansInfo.enabled = false;
				chbFrequenceSansInfo.enabled = false;
				tiFrequenceActions.enabled = false;
				rbFrequenceActions1.enabled = false;
				rbFrequenceActions2.enabled = false;
			}
			else
			{
				tiTempsSansInfo.enabled = true;
				rbgTempsSansInfo1.enabled = true;
				rbgTempsSansInfo2.enabled = true;
				chbMessageUserSansInfo.enabled = true;
				cbMessageUserSansInfo.enabled = true;
				chbMessageGestSansInfo.enabled = true;
				chbFrequenceSansInfo.enabled = true;
				tiFrequenceActions.enabled = true;
				rbFrequenceActions1.enabled = true;
				rbFrequenceActions2.enabled = true;
				chbMessageUserSansInfo_ChangeHandler();
			}
		}
		public function chbActiverControleReactivation_ChangeHandler():void
		{
			if (chbActiverControleReactivation.selected == false)
			{
				cbMessageUserReactivation.enabled = false;
				chbMessageUserReactivation.enabled = false;
				chbMessageGestReactivation.enabled = false;
				chbCouperDataReactivation.enabled = false;
				chbFrequenceAlerte.enabled = false;
				tiFrequenceAlerte.enabled = false;
				rbFrequenceAlerte1.enabled = false;
				rbFrequenceAlerte2.enabled = false;
			}
			else
			{
				cbMessageUserReactivation.enabled = true;
				chbMessageUserReactivation.enabled = true;
				chbMessageGestReactivation.enabled = true;
				chbCouperDataReactivation.enabled = true;
				chbFrequenceAlerte.enabled = true;
				tiFrequenceAlerte.enabled = true;
				rbFrequenceAlerte1.enabled = true;
				rbFrequenceAlerte2.enabled = true;
				chbMessageUserReactivation_ChangeHandler();
			}
		}
		public function chbMessageUser_ChangeHandler():void
		{
			if(!chbMessageUserDom1.selected) cbMessageUserDom1.enabled = false;
			else cbMessageUserDom1.enabled = true;
			if(!chbMessageUserDom2.selected) cbMessageUserDom2.enabled = false;
			else cbMessageUserDom2.enabled = true;
			if(!chbMessageUserDom3.selected) cbMessageUserDom3.enabled = false;
			else cbMessageUserDom3.enabled = true;
		}
		
		public function chbMessageUserRoam_ChangeHandler():void
		{
			if(!chbMessageUserRoam1.selected) cbMessageUserRoam1.enabled = false;
			else cbMessageUserRoam1.enabled = true;
			if(!chbMessageUserRoam2.selected) cbMessageUserRoam2.enabled = false;
			else cbMessageUserRoam2.enabled = true;
			if(!chbMessageUserRoam3.selected) cbMessageUserRoam3.enabled = false;
			else cbMessageUserRoam3.enabled = true;
		}
		public function chbMessageUserSansInfo_ChangeHandler():void
		{
			if (!chbMessageUserSansInfo.selected) cbMessageUserSansInfo.enabled = false;
			else cbMessageUserSansInfo.enabled = true;
		}
		public function chbMessageUserReactivation_ChangeHandler():void
		{
			if (!chbMessageUserReactivation.selected) cbMessageUserReactivation.enabled = false;
			else cbMessageUserReactivation.enabled = true;
		}
		/****************************************************************
		 * ClickHandlers
		 ****************************************************************/
		
		public function btValider_ClickHandler():void
		{
			fillParamAlertes();
			
			
			if (!boolAllSearch)
			{
				paramseuilCS.addEventListener(DatalertEvent.UPDATE_SEUILLIGNE_EVENT,update_seuilLigne_handler);
				paramseuilCS.updateParamSeuilLigne(listIdSoustete,ligneseuil,paramSeuilLigne);
			}
			else
			{
				dispatchEvent(new Event("MASS_UPDATING_TRUE"));
				
				paramseuilCS.addEventListener(DatalertEvent.UPDATE_ALLSEARCH_EVENT,update_allSearch_handler);
				paramseuilCS.updateParamAllSearch(listIdSoustete,ligneseuil,paramSeuilLigne, encodeJSON(paramAlertes));
				boolAllSearch = false;
				parentDocument.document.boolAllSearch = false;
				paramAlertes.seuils = new Array();
				PopUpManager.removePopUp(parentDocument.document);
			}
			//
			/*paramAlertesService.addEventListener("ALERTSREGISTERED", AlertsRegistered_Handler);
			paramAlertesService.registerParams(encodeJSON(paramAlertes), listIdSoustete);*/

			//On vide "seuils" car sinon on se retrouve avec un tableau qui s'agrandit à chaque click sur valider, à l'infini
			/*paramAlertes.seuils = new Array();*/
		}
		/****************************************************************
		 * Fonctions annexes
		 ***************************************************************/
		
		public function update_seuilLigne_handler(evt:DatalertEvent):void
		{
			if(evt.objectReturn> 0)
			{				
				
				paramAlertesService.addEventListener("ALERTSREGISTERED", AlertsRegistered_Handler);
				if (!boolAllSearch)
					paramAlertesService.registerParams(encodeJSON(paramAlertes), listIdSoustete);

			}			
			
		}
		
		public function update_allSearch_handler(evt:DatalertEvent):void
		{
			if(!evt.objectReturn.hasOwnProperty("faultString"))
			{
				paramAlertes.seuils = new Array();
				if(boolDeployService)
				{
					var service:DeployServiceIHM= new DeployServiceIHM();
					service.listeLigne=listLigne;
					PopUpManager.addPopUp(service, this, true);
					PopUpManager.centerPopUp(service);	
				}
				dispatchEvent(new DatalertEvent(DatalertEvent.REFRESH_PARENT_DATA));		
				dispatchEvent(new Event("MASS_UPDATING_FALSE"));
			}
			else
			{
				Alert.show(evt.objectReturn.faultString);
				dispatchEvent(new Event("MASS_UPDATING_FALSE"));
			}
		}
		
		public function AlertsRegistered_Handler(e:Event):void
		{
			paramAlertes.seuils = new Array();
			if(boolDeployService)
			{
				var service:DeployServiceIHM= new DeployServiceIHM();
				service.listeLigne=listLigne;
				PopUpManager.addPopUp(service, this, true);
				PopUpManager.centerPopUp(service);	
			}
			dispatchEvent(new DatalertEvent(DatalertEvent.REFRESH_PARENT_DATA));
			//
			/*paramseuilCS.addEventListener(DatalertEvent.UPDATE_SEUILLIGNE_EVENT,update_seuilLigne_handler);
			paramseuilCS.updateParamSeuilLigne(listIdSoustete,ligneseuil,paramSeuilLigne);*/
		}
		
		private function fillDom1():void
		{
			switch(cbFrequenceAlerte1.selectedItem.label)
			{
				case ResourceManager.getInstance().getString('M26','Jour') :
					typePeriode = 0;
					break;
				case ResourceManager.getInstance().getString('M26','Semaine') :
					typePeriode = 1;
					break;
				case ResourceManager.getInstance().getString('M26', 'Mois_sing') :
					typePeriode = 2;
					break;
				default :
					typePeriode = -1;
					break;
			}
			switch(cbxFormatData1.selectedItem.label)
			{//la 'limit' à envoyer est en kilooctets.
				case ResourceManager.getInstance().getString('M26','Mo') :
					limit = parseInt(tiMontantData1.text) * 1024;
					typeAlerte = "U";
					break;
				case ResourceManager.getInstance().getString('M26','Go') :
					limit = parseInt(tiMontantData1.text) * 1048576;
					typeAlerte = "U";
					break;
				case "%" :
					limit = parseInt(tiMontantData1.text);
					typeAlerte = "C";
					if (limit > 100) dispatchEvent(new Event("INVALID_VALUE"));
					break;
				default :
					limit = -1;
					typeAlerte = "";
					break;
			}
			for each (o in datalertCSDom.listeMessage)
			{
				if (cbMessageUserDom1.selectedItem == o.LIBELLE)
				{
					idmessage_user = o.IDMESSAGE;
					break;
				}
			}//le passage en argument de undefined équivaut à null en JSON, tandis que null équivaut à une chaîne vide
			paramAlertes.addToSeuils(typePeriode, 0, limit, typeAlerte, chbMessageUserDom1.selected?idmessage_user:undefined, chbMessageGestDom1.selected?1:0, chbCouperData1.selected?1:0);
		}
		private function fillDom2():void
		{
			switch(cbFrequenceAlerte2.selectedItem.label)
			{
				case ResourceManager.getInstance().getString('M26','Jour') :
					typePeriode = 0;
					break;
				case ResourceManager.getInstance().getString('M26','Semaine') :
					typePeriode = 1;
					break;
				case ResourceManager.getInstance().getString('M26', 'Mois_sing') :
					typePeriode = 2;
					break;
				default :
					typePeriode = -1;
					break;
			}
			switch(cbxFormatData2.selectedItem.label)
			{
				case ResourceManager.getInstance().getString('M26','Mo') :
					limit = parseInt(tiMontantData2.text) * 1024;
					typeAlerte = "U";
					break;
				case ResourceManager.getInstance().getString('M26','Go') :
					limit = parseInt(tiMontantData2.text) * 1048576;
					typeAlerte = "U";
					break;
				case "%" :
					limit = parseInt(tiMontantData2.text);
					typeAlerte = "C";
					if (limit > 100) dispatchEvent(new Event("INVALID_VALUE"));
					break;
				default :
					limit = -1;
					typeAlerte = "";
					break;
			}
			for each (o in datalertCSDom.listeMessage)
			{
				if (cbMessageUserDom2.selectedItem == o.LIBELLE)
				{
					idmessage_user = o.IDMESSAGE;
					break;
				}
			}//le passage en argument de undefined équivaut à null en JSON, tandis que null équivaut à une chaîne vide
			paramAlertes.addToSeuils(typePeriode, 0, limit, typeAlerte, chbMessageUserDom2.selected?idmessage_user:undefined, chbMessageGestDom2.selected?1:0, chbCouperData2.selected?1:0);
		}
		private function fillDom3():void
		{
			switch(cbFrequenceAlerte3.selectedItem.label)
			{
				case ResourceManager.getInstance().getString('M26','Jour') :
					typePeriode = 0;
					break;
				case ResourceManager.getInstance().getString('M26','Semaine') :
					typePeriode = 1;
					break;
				case ResourceManager.getInstance().getString('M26', 'Mois_sing') :
					typePeriode = 2;
					break;
				default :
					typePeriode = -1;
					break;
			}
			switch(cbxFormatData3.selectedItem.label)
			{
				case ResourceManager.getInstance().getString('M26','Mo') :
					limit = parseInt(tiMontantData3.text) * 1024;
					typeAlerte = "U";
					break;
				case ResourceManager.getInstance().getString('M26','Go') :
					limit = parseInt(tiMontantData3.text) * 1048576;
					typeAlerte = "U";
					break;
				case "%" :
					limit = parseInt(tiMontantData3.text);
					typeAlerte = "C";
					if (limit > 100) dispatchEvent(new Event("INVALID_VALUE"));
					break;
				default :
					limit = -1;
					typeAlerte = "";
					break;
			}
			for each (o in datalertCSDom.listeMessage)
			{
				if (cbMessageUserDom3.selectedItem == o.LIBELLE)
				{
					idmessage_user = o.IDMESSAGE;
					break;
				}
			}//le passage en argument de undefined équivaut à null en JSON, tandis que null équivaut à une chaîne vide
			paramAlertes.addToSeuils(typePeriode, 0, limit, typeAlerte, chbMessageUserDom3.selected?idmessage_user:undefined, chbMessageGestDom3.selected?1:0, chbCouperData3.selected?1:0);
		}
		private function fillRoam1():void
		{
			switch(cbFrequenceAlerteRoam1.selectedItem.label)
			{
				case ResourceManager.getInstance().getString('M26','Jour') : 
					typePeriode = 0;
					break;
				case ResourceManager.getInstance().getString('M26','Semaine') :
					typePeriode = 1;
					break;
				case ResourceManager.getInstance().getString('M26', 'Mois_sing') :
					typePeriode = 2;
					break;
				default :
					typePeriode = -1;
					break;
			}
			switch(cbxFormatDataRoam1.selectedItem.label)
			{
				case ResourceManager.getInstance().getString('M26','Mo') :
					limit = parseInt(tiMontantDataRoam1.text) * 1024;
					typeAlerte = "U";
					break;
				case ResourceManager.getInstance().getString('M26','Go') :
					limit = parseInt(tiMontantDataRoam1.text) * 1048576;
					typeAlerte = "U";
					break;
				case "%" :
					limit = parseInt(tiMontantDataRoam1.text);
					typeAlerte = "C";
					if (limit > 100) dispatchEvent(new Event("INVALID_VALUE"));
					break;
				default :
					limit = -1;
					typeAlerte = "";
					break;
			}
			for each (o in datalertCSRoam.listeMessage)
			{
				if (cbMessageUserRoam1.selectedItem == o.LIBELLE)
				{
					idmessage_user = o.IDMESSAGE;
					break;
				}
			}//le passage en argument de undefined équivaut à null en JSON, tandis que null équivaut à une chaîne vide
			paramAlertes.addToSeuils(typePeriode, 1, limit, typeAlerte, chbMessageUserRoam1.selected?idmessage_user:undefined, chbMessageGestRoam1.selected?1:0, chbCouperDataRoam1.selected?1:0);
		}
		private function fillRoam2():void
		{
			switch(cbFrequenceAlerteRoam2.selectedItem.label)
			{
				case ResourceManager.getInstance().getString('M26','Jour') :
					typePeriode = 0;
					break;
				case ResourceManager.getInstance().getString('M26','Semaine') :
					typePeriode = 1;
					break;
				case ResourceManager.getInstance().getString('M26', 'Mois_sing') :
					typePeriode = 2;
					break;
				default :
					typePeriode = -1;
					break;
			}
			switch(cbxFormatDataRoam2.selectedItem.label)
			{
				case ResourceManager.getInstance().getString('M26','Mo') :
					limit = parseInt(tiMontantDataRoam2.text) * 1024;
					typeAlerte = "U";
					break;
				case ResourceManager.getInstance().getString('M26','Go') :
					limit = parseInt(tiMontantDataRoam2.text) * 1048576;
					typeAlerte = "U";
					break;
				case "%" :
					limit = parseInt(tiMontantDataRoam2.text);
					typeAlerte = "C";
					if (limit > 100) dispatchEvent(new Event("INVALID_VALUE"));
					break;
				default :
					limit = -1;
					typeAlerte = "";
					break;
			}
			for each (o in datalertCSRoam.listeMessage)
			{
				if (cbMessageUserRoam2.selectedItem == o.LIBELLE)
				{
					idmessage_user = o.IDMESSAGE;
					break;
				}
			}//le passage en argument de undefined équivaut à null en JSON, tandis que null équivaut à une chaîne vide
			paramAlertes.addToSeuils(typePeriode, 1, limit, typeAlerte, chbMessageUserRoam2.selected?idmessage_user:undefined, chbMessageGestRoam2.selected?1:0, chbCouperDataRoam2.selected?1:0);
		}
		private function fillRoam3():void
		{
			switch(cbFrequenceAlerteRoam3.selectedItem.label)
			{
				case ResourceManager.getInstance().getString('M26','Jour'):
					typePeriode = 0;
					break;
				case ResourceManager.getInstance().getString('M26','Semaine'):
					typePeriode = 1;
					break;
				case ResourceManager.getInstance().getString('M26', 'Mois_sing'):
					typePeriode = 2;
					break;
				default:
					typePeriode = -1;
					break;
			}
			switch(cbxFormatDataRoam3.selectedItem.label)
			{
				case ResourceManager.getInstance().getString('M26','Mo'):
					limit = parseInt(tiMontantDataRoam3.text) * 1024;
					typeAlerte = "U";
					break;
				case ResourceManager.getInstance().getString('M26','Go'):
					limit = parseInt(tiMontantDataRoam3.text) * 1048576;
					typeAlerte = "U";
					break;
				case "%":
					limit = parseInt(tiMontantDataRoam3.text);
					typeAlerte = "C";
					if (limit > 100) dispatchEvent(new Event("INVALID_VALUE"));
					break;
				default:
					limit = -1;
					typeAlerte = "";
					break;
			}
			for each (o in datalertCSRoam.listeMessage)
			{
				if (cbMessageUserRoam3.selectedItem == o.LIBELLE)
				{
					idmessage_user = o.IDMESSAGE;
					break;
				}
			}//le passage en argument de undefined équivaut à null en JSON, tandis que null équivaut à une chaîne vide
			paramAlertes.addToSeuils(typePeriode, 1, limit, typeAlerte, chbMessageUserRoam3.selected?idmessage_user:undefined, chbMessageGestRoam3.selected?1:0, chbCouperDataRoam3.selected?1:0);
		}
	}
}