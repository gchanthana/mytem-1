package datalert.ihm.popup
{
	import composants.periodeSelector.PeriodeEvent;
	import composants.periodeSelector.PeriodeSelector;
	import composants.searchpagindatagrid.ParametresRechercheVO;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class ExportDatalertFlowImpl extends TitleWindow
	{
		//Données de la recherche
		[Bindable] public var parametreRechercheVO:ParametresRechercheVO;
		//Données du dateSelector
		public var dateDebut:Date;
		public var dateFin:Date;
		
		[Bindable] public var btnValider:Button;
		[Bindable] public var btnAnnuler:Button;
		[Bindable] public var mySelector:PeriodeSelector;
		[Bindable] public var myPeriodeLbl:Label;
		
		[Bindable]
		private var _moisDeb : String ;//le debut de la periode 
		[Bindable]
		private var _moisFin : String ; //la fin de la periode
		
		private var myDateFormatter : DateFormatter;
		
		public function ExportDatalertFlowImpl()
		{
		}
		
		public function onCreationComplete(e:Event):void
		{
			mySelector.addEventListener("periodeChange",updatePeriode); 
			var pe:PeriodeEvent = new PeriodeEvent("periodeChange");
			pe.dateDeb = mySelector.dateDebut;
			pe.dateFin = mySelector.dateFin;
			updatePeriode(pe);
		}
		
		/** Fonctions spécifiques du PeriodeSelector
		 *
		 * 
		 * 
		 */
		private function updatePeriode(pe : PeriodeEvent):void
		{	
			// a ne pas toucher avec l'internationlisation !!!
			var myFormatter : DateFormatter = new DateFormatter();
			myFormatter.formatString= "DD/MM/YYYY" ;
			
			_moisDeb = myFormatter.format(pe.dateDeb);
			_moisFin = myFormatter.format(pe.dateFin);			
			setSelectedPeriode(_moisDeb,_moisFin);
			
			//myRechercheCps.setParameters(_moisDeb,_moisFin);	
		}
		
		public function setSelectedPeriode(moisDebut:String, moisFin:String):void
		{
			myPeriodeLbl.text = ResourceManager.getInstance().getString('M26', 'P_riode_trait_e___') + setGoodDateLocale(moisDebut) + ResourceManager.getInstance().getString('M26', '_au_') + setGoodDateLocale(moisFin);					
		}
		
		private function setGoodDateLocale(myDate:String):String
		{
			var jourD : String = myDate.substr(0,2);
			var moisD : String = myDate.substr(3,2);
			var anneeD : String = myDate.substr(6,4);
			
			var tmp : Number = new Number(moisD);
			tmp--;
			
			var myDateTmp : Date = new Date(anneeD,tmp,jourD);
			
			myDateFormatter = new DateFormatter();
			myDateFormatter.formatString = ResourceManager.getInstance().getString('M26','DD_MMMM_YYYY');
			
			var myDateRes : String = myDateFormatter.format(myDateTmp);
			
			switch(moisD)
			{
				case "01" : 
					myDateRes = myDateRes.replace("January",ResourceManager.getInstance().getString('M26','MMMM_January'));
					break;
				case "02" : 
					myDateRes = myDateRes.replace("February",ResourceManager.getInstance().getString('M26','MMMM_February'));
					break;
				case "03" : 
					myDateRes = myDateRes.replace("March",ResourceManager.getInstance().getString('M26','MMMM_March'));
					break;
				case "04" : 
					myDateRes = myDateRes.replace("April",ResourceManager.getInstance().getString('M26','MMMM_April'));
					break;
				case "05" : 
					myDateRes = myDateRes.replace("May",ResourceManager.getInstance().getString('M26','MMMM_May'));
					break;
				case "06" : 
					myDateRes = myDateRes.replace("June",ResourceManager.getInstance().getString('M26','MMMM_June'));
					break;
				case "07" : 
					myDateRes = myDateRes.replace("July",ResourceManager.getInstance().getString('M26','MMMM_July'));
					break;
				case "08" : 
					myDateRes = myDateRes.replace("August",ResourceManager.getInstance().getString('M26','MMMM_August'));
					break;
				case "09" : 
					myDateRes = myDateRes.replace("September",ResourceManager.getInstance().getString('M26','MMMM_September'));
					break;
				case "10" : 
					myDateRes = myDateRes.replace("October",ResourceManager.getInstance().getString('M26','MMMM_October'));
					break;
				case "11" : 
					myDateRes = myDateRes.replace("November",ResourceManager.getInstance().getString('M26','MMMM_November'));
					break;
				case "12" : 
					myDateRes = myDateRes.replace("December",ResourceManager.getInstance().getString('M26','MMMM_December'));
					break;
			}
			
			return myDateRes;
		}
		
		public function btnAnnuler_clickHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		public function btnValider_clickHandler(e:Event):void
		{
			dateDebut = mySelector.dateDebut;
			dateFin = mySelector.dateFin;
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.Datalert",
				"exportRawFlow",
				exportFlowResultHandler,null);
			
			RemoteObjectUtil.callService(opData,
			dateDebut,
			dateFin,
			parametreRechercheVO);
			PopUpManager.removePopUp(this);
		}
		
		public function exportFlowResultHandler(e:Event):void
		{
			if(e is ResultEvent)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M26', 'Votre_demande_de_rapport____t__prise_en_'));
			}
			else
				trace("The event caught in exportFlowResultHandler is not a ResultEvent");
		}
	}
}