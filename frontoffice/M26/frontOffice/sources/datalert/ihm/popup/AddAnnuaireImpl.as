package datalert.ihm.popup
{
	import composants.util.TextInputLabeled;
	
	import datalert.event.AnnuaireServiceEvent;
	import datalert.services.AnnuaireService;
	import datalert.vo.AnnauireVO;
	
	import flash.events.MouseEvent;
	import flash.utils.describeType;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.managers.PopUpManager;
	
	public class AddAnnuaireImpl extends TitleWindow
	{
		
		private var annuaireItem:AnnauireVO;
		private var annuService:AnnuaireService;
		
		[Bindable]public var btAjouter:Button;
		public var btAnnuler:Button;
		
		[Bindable]public var txtEmail:TextInputLabeled;
		[Bindable]public var txtNom:TextInputLabeled;
		[Bindable]public var txtPrenom:TextInputLabeled;
		[Bindable]public var txtLigne:TextInputLabeled;
		[Bindable]public var txtMatricule:TextInputLabeled;
		
		public function AddAnnuaireImpl()
		{
			super();
			annuaireItem = new AnnauireVO();
			annuService = new AnnuaireService();
			annuService.addEventListener(AnnuaireServiceEvent.ADD_ITEM,anServiceAddHandler);
		}
		
		
		
		public function change():void
		{
			annuaireItem.email = ":str:"+txtEmail.text;
			annuaireItem.nom = ":str:"+txtNom.text;
			annuaireItem.prenom = ":str:"+txtPrenom.text;
			annuaireItem.num_ligne = ":str:"+txtLigne.text;
			annuaireItem.matricule = ":str:"+txtMatricule.text;
			
			var test:Boolean = true;
			var description:XML = describeType(annuaireItem);
			for each (var a:XML in description.variable) 
			{					
				if(annuaireItem[a.@name].length < 9)
				{
					test = test && false;
				}
			}
			btAjouter.enabled = test;
		}
		
		protected function btAnnulerClickHandler(evt:MouseEvent):void
		{
			close();	
		}
		
		protected function btAjouterClickHandler(evt:MouseEvent):void
		{	
			send();
		}		
		
		protected function btCloseClickHandler():void
		{
			close();
		}
		
		protected function anServiceAddHandler(evt:AnnuaireServiceEvent):void
		{	
			close();
		}
		
		private function close():void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function send():void
		{
			
			annuService.addItem(annuaireItem);
		}
		
		
	}
}