package datalert.vo
{
	public class AnnauireVO
	{
		public var nom:String="";
		public var prenom:String="";
		public var email:String="";
		public var matricule:String="";
		public var num_ligne:String="";
		
		public function AnnauireVO(matricule:String, nom:String, prenom:String, email:String, num_ligne:String)
		{
			this.matricule = matricule;
			this.nom = nom;
			this.prenom = prenom;
			this.email = email;
			this.num_ligne = num_ligne;
		}
	}
}