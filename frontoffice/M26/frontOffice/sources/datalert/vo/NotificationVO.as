package datalert.vo
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class NotificationVO
	{
		public var mailList:ArrayCollection=new ArrayCollection();
		public var BOOLACTIVENOTIF:Number;
		public var BOOLNOTIFDOM:Number;
		public var BOOLNOTIFROAM:Number;
		public var langue_pays:ArrayCollection=new ArrayCollection()
		
		public function NotificationVO()
		{
		}
	}
}