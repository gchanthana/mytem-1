package datalert.vo
{
	[Bindable]
	public class NewLigneVO
	{
		private var _ISITEM:Boolean = true;
		private var _MATRICULE:String = "";
		private var _NOM:String = "";
		private var _PRENOM:String = "";
		private var _EMAIL:String = "";
		private var _TEL:String = "";
		private var _MATRICULEISVALID:Boolean = false;
		private var _NOMISVALID:Boolean = false;
		private var _PRENOMISVALID:Boolean = false;
		private var _EMAILISVALID:Boolean = false;
		private var _TELISVALID:Boolean = false;
		private var _MATRICULEISDOUBLON:Boolean = false;
		private var _NOMISDOUBLON:Boolean = false;
		private var _PRENOMISDOUBLON:Boolean = false;
		private var _EMAILISDOUBLON:Boolean = false;
		private var _TELISDOUBLON:Boolean = false;
		
		public function NewLigneVO(matricule:String, nom:String, prenom:String, email:String, tel:String)
		{
			this._MATRICULE = matricule;
			this._NOM = nom;
			this._PRENOM = prenom;
			this._EMAIL = email;
			this._TEL = tel;
		}
		
		public function set ISITEM(value:Boolean):void
		{
			_ISITEM = value;
		}
		
		public function get ISITEM():Boolean
		{
			return _ISITEM;
		}
		
		public function set MATRICULE(value:String):void
		{
			_MATRICULE = value;
		}
		
		public function get MATRICULE():String
		{
			return _MATRICULE;
		}
		
		public function set NOM(value:String):void
		{
			_NOM = value;
		}
		
		public function get NOM():String
		{
			return _NOM;
		}
		
		public function set PRENOM(value:String):void
		{
			_PRENOM = value;
		}
		
		public function get PRENOM():String
		{
			return _PRENOM;
		}
		
		public function set EMAIL(value:String):void
		{
			_EMAIL = value;
		}
		
		public function get EMAIL():String
		{
			return _EMAIL;
		}
		
		public function set TEL(value:String):void
		{
			_TEL = value;
		}
		
		public function get TEL():String
		{
			return _TEL;
		}
		
		public function set MATRICULEISVALID(value:Boolean):void
		{
			_MATRICULEISVALID = value;
		}
		
		public function get MATRICULEISVALID():Boolean
		{
			return _MATRICULEISVALID;
		}
		
		public function set NOMISVALID(value:Boolean):void
		{
			_NOMISVALID = value;
		}
		
		public function get NOMISVALID():Boolean
		{
			return _NOMISVALID;
		}
		public function set PRENOMISVALID(value:Boolean):void
		{
			_PRENOMISVALID = value;
		}
		
		public function get PRENOMISVALID():Boolean
		{
			return _PRENOMISVALID;
		}
		public function set EMAILISVALID(value:Boolean):void
		{
			_EMAILISVALID = value;
		}
		
		public function get EMAILISVALID():Boolean
		{
			return _EMAILISVALID;
		}
		
		public function set TELISVALID(value:Boolean):void
		{
			_TELISVALID = value;
		}
		
		public function get TELISVALID():Boolean
		{
			return _TELISVALID;
		}
		
		public function set MATRICULEISDOUBLON(value:Boolean):void
		{
			_MATRICULEISDOUBLON = value;
		}
		
		public function get MATRICULEISDOUBLON():Boolean
		{
			return _MATRICULEISDOUBLON;
		}
		
		public function set NOMISDOUBLON(value:Boolean):void
		{
			_NOMISDOUBLON = value;
		}
		
		public function get NOMISDOUBLON():Boolean
		{
			return _NOMISDOUBLON;
		}
		public function set PRENOMISDOUBLON(value:Boolean):void
		{
			_PRENOMISDOUBLON = value;
		}
		
		public function get PRENOMISDOUBLON():Boolean
		{
			return _PRENOMISDOUBLON;
		}
		public function set EMAILISDOUBLON(value:Boolean):void
		{
			_EMAILISDOUBLON = value;
		}
		
		public function get EMAILISDOUBLON():Boolean
		{
			return _EMAILISDOUBLON;
		}
		
		public function set TELISDOUBLON(value:Boolean):void
		{
			_TELISDOUBLON = value;
		}
		
		public function get TELISDOUBLON():Boolean
		{
			return _TELISDOUBLON;
		}
	}
}
