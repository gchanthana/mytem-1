package datalert.vo
{
	[Bindable] 
	public class MessageVO
	{
		
		public var LIBELLE:String=null;
		public var IDMESSAGE:Number;
		public var BOOLMESSEAGEDOM:Number;
		public var BOOLMESSEAGEROAM:Number;
		public var BOOLMESSEAGEACTIF:Number;
		public var GESTIONNAIRE:String=null;
		public var MEDIA:Number;
		public var MESSAGETXT:String=null;
		public var DATEMESSAGE:String;
		public var DOMESTIQUEDATA:Number;
		public var DOMESTIQUEPLAN:Number;
		public var DOMESTIQUEALERT:Number;
		public var ROAMINGDATA:Number;
		public var ROAMINGPLAN:Number;
		public var ROAMINGALERT:Number;
		public var SELECTED:Boolean;
		
		public function MessageVO()
		{
		}
	}
}