package datalert.vo
{
	[Bindable]
	public class SeuilLigneVO
	{
		private var _COMPTEFACTURATION:String="";
		private var _BOOLMESSAGEDOM:Number=0;
		private var _BOOLMESSAGEROAM:Number=0;
		private var _BOOLDRTDOM:Number=0;
		private var _BOOLDRTROAM:Number=0;
		private var _BOOLAUTOREFRESH:Number=1;
		private var _IDMESSAGEDOM:Number=0;
		private var _IDMESSAGEROAM:Number=0;
		
		
		
		
		public function SeuilLigneVO()
		{
		}

		public function get COMPTEFACTURATION():String
		{
			return _COMPTEFACTURATION;
		}

		public function set COMPTEFACTURATION(value:String):void
		{
			_COMPTEFACTURATION = value;
		}		

		public function get BOOLDRTDOM():Number
		{
			return _BOOLDRTDOM;
		}

		public function set BOOLDRTDOM(value:Number):void
		{
			_BOOLDRTDOM = value;
		}

		public function get BOOLDRTROAM():Number
		{
			return _BOOLDRTROAM;
		}

		public function set BOOLDRTROAM(value:Number):void
		{
			_BOOLDRTROAM = value;
		}

		public function get BOOLMESSAGEDOM():Number
		{
			return _BOOLMESSAGEDOM;
		}

		public function set BOOLMESSAGEDOM(value:Number):void
		{
			_BOOLMESSAGEDOM = value;
		}
		
		public function get IDMESSAGEDOM():Number
		{
			return _IDMESSAGEDOM;
		}
		
		public function set IDMESSAGEDOM(value:Number):void
		{
			_IDMESSAGEDOM = value;
		}

		public function get BOOLMESSAGEROAM():Number
		{
			return _BOOLMESSAGEROAM;
		}

		public function set BOOLMESSAGEROAM(value:Number):void
		{
			_BOOLMESSAGEROAM = value;
		}
		
		public function get IDMESSAGEROAM():Number
		{
			return _IDMESSAGEROAM;
		}
		
		public function set IDMESSAGEROAM(value:Number):void
		{
			_IDMESSAGEROAM = value;
		}

		public function get BOOLAUTOREFRESH():Number
		{
			return _BOOLAUTOREFRESH;
		}

		public function set BOOLAUTOREFRESH(value:Number):void
		{
			_BOOLAUTOREFRESH = value;
		}

	}
}