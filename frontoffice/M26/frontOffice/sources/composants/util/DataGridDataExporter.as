/**
	_________________________________________________________________________________________________________________

	DataGridDataExporter is a util-class to export DataGrid's data into different format.	
	@class DataGridDataExporter (public)
	@author Abdul Qabiz (mail at abdulqabiz dot com) 
	@version 0.01 (2/8/2007)
	@availability 9.0+
	@usage<code>DataGridDataExporter.<staticMethod> (dataGridReference)</code>
	@example
		<code>
			var csvData:String = DataGridDataExporter.exportCSV (dg);
		</code>
	__________________________________________________________________________________________________________________

**/
package composants.util
{
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.collections.IViewCursor;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.resources.ResourceManager;

	public class DataGridDataExporter
	{

		public static function getCSVString(dg:DataGrid,dataprovider : Object,csvSeparator:String="\t", lineSeparator:String="\n", onlySelectedElement:Boolean = false):String
		{
			var data:String = "";
			var columns:Array = dg.columns;
			var columnCount:int = columns.length-2;
			var column:DataGridColumn;
			var header:String = "";
			var headerGenerated:Boolean = false;
			var dataProvider:Object = dataprovider;

			var rowCount:int = dataProvider.length;
			var dp:Object = null;

		
			var cursor:IViewCursor = dataProvider.createCursor ();
			var j:int = 0;
			
			var condition:Boolean = true;
			
			
			//loop through rows
			while (!cursor.afterLast)
			{
				if(onlySelectedElement && !cursor.current.SELECTED)
				{		
				 	cursor.moveNext();	
				}
				else
				{
					var obj:Object = null;
					obj = cursor.current;
					
					//loop through all columns for the row
					for(var k:int = 0; k < columnCount; k++)
					{
						
						column = columns[k];
						
						
						//Exclude column data which is invisible (hidden) and no header
						if(!column.visible || !(column).headerText)
						{
							continue;
						}
						

						
						var myPattern:RegExp = /€/g;
						if(column.dataField == "DOMESTIQUEALERTS" || column.dataField == "ROAMINGALERTS")
						{
							data += "\"" + treatAlerts(column.dataField, obj, header, csvSeparator) +  "\"";
						}
						else
							data += "\""+ column.itemToLabel(obj).replace(myPattern,"")+ "\"";
	
						if(k < (columnCount -1))
						{
							data += csvSeparator;
						}
	
						//generate header of CSV, only if it's not genereted yet
						if (!headerGenerated && column.dataField != "DOMESTIQUEALERTS" && column.dataField != "ROAMINGALERTS")
						{
							header += "\"" + column.headerText + "\"";
							if (k < columnCount - 1)
							{
								header += csvSeparator;
							}
						}
						else if(!headerGenerated && (column.dataField == "DOMESTIQUEALERTS" || column.dataField == "ROAMINGALERTS"))
						{
							header += "\"" + ResourceManager.getInstance().getString('M26', 'Alerte') + " 1" + "\"" + csvSeparator;
							header += "\"" + ResourceManager.getInstance().getString('M26', 'Alerte') + " 2" + "\"" + csvSeparator;
							header += "\"" + ResourceManager.getInstance().getString('M26', 'Alerte') + " 3" + "\"" + csvSeparator;
						}
					}
					
					headerGenerated = true;
	
					if (j < (rowCount - 1))
					{
						data += lineSeparator;
					}
	
					j++;
					cursor.moveNext ();
			
				}
				
			}
			
			//set references to null:
			dataProvider = null;
			columns = null;
			column = null;

			
			return (header + "\r\n" + data);
		}
		
		public static function treatAlerts(alertType:String, info:Object, header:String, csvSeparator:String):String
		{
			var retour:String = "";
			var temp:Number = 0;
			var o:Object = new Object();
			var numAlert:Number = 1;
			if(alertType == "DOMESTIQUEALERTS" && info.DOMESTIQUEALERTS != null)
			{
				for each (o in info.DOMESTIQUEALERTS)
				{
					if(o.type_alerte == "U")//Mo
					{
						if(o.type_periode == "0")
						{
							temp = info.CURRENTDOMESTICVOLUMEDAY;
							retour += ConsoviewFormatter.formatNumber(temp, 2) + " / " + ConsoviewFormatter.formatNumber((o.limit / 1024), 0) + " " + ResourceManager.getInstance().getString('M26', 'Mo');
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_jour');
						}
						else if(o.type_periode == "1")
						{
							temp = info.CURRENTDOMESTICVOLUMEWEEK;
							retour += ConsoviewFormatter.formatNumber(temp, 2) + " / " + ConsoviewFormatter.formatNumber((o.limit / 1024), 0) + " " + ResourceManager.getInstance().getString('M26', 'Mo');
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_semaine');
						}
						else if(o.type_periode == "2")
						{
							temp = info.DOMESTIQUEDATA;
							retour += ConsoviewFormatter.formatNumber(temp, 2) + " / " + ConsoviewFormatter.formatNumber((o.limit / 1024), 0) + " " + ResourceManager.getInstance().getString('M26', 'Mo');
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_mois');
						}
					}
					else if(o.type_alerte == "C")//%
					{
						if(o.type_periode == "0")
						{
							temp = info.CURRENTDOMESTICVOLUMEDAY/info.DOMESTIQUEPLAN;
							retour += ConsoviewFormatter.formatNumber(temp * 100, 2) + " / " + o.limit.toString() + " %";
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_jour');
						}
						else if(o.type_periode == "1")
						{
							temp = info.CURRENTDOMESTICVOLUMEWEEK/info.DOMESTIQUEPLAN;
							retour += ConsoviewFormatter.formatNumber(temp * 100, 2) + " / " + o.limit.toString() + " %";
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_semaine');
						}
						else if(o.type_periode == "2")
						{
							temp = info.DOMESTIQUEDATA/info.DOMESTIQUEPLAN;
							retour += ConsoviewFormatter.formatNumber(temp * 100, 2) + " / " + ConsoviewFormatter.formatNumber(o.limit, 0) + " %";
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_mois');
						}
					}
					if (numAlert < 3)
						retour += "\"" + csvSeparator + "\"";
					numAlert++;
				}
				
			}
			else if(alertType == "ROAMINGALERTS")
			{
				for each (o in info.ROAMINGALERTS)
				{
					if(o.type_alerte == "U")//Mo
					{
						if(o.type_periode == "0")
						{
							temp = info.CURRENTROAMINGVOLUMEDAY;
							retour += ConsoviewFormatter.formatNumber(temp, 2) + " / " + ConsoviewFormatter.formatNumber((o.limit / 1024), 0) + " " + ResourceManager.getInstance().getString('M26', 'Mo');
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_jour');
						}
						else if(o.type_periode == "1")
						{
							temp = info.CURRENTROAMINGVOLUMEWEEK;
							retour += ConsoviewFormatter.formatNumber(temp, 2) + " / " + ConsoviewFormatter.formatNumber((o.limit / 1024), 0) + " " + ResourceManager.getInstance().getString('M26', 'Mo');
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_semaine');
						}
						else if(o.type_periode == "2")
						{
							temp = info.ROAMINGDATA;
							retour += ConsoviewFormatter.formatNumber(temp, 2) + " / " + ConsoviewFormatter.formatNumber((o.limit / 1024), 0) + " " + ResourceManager.getInstance().getString('M26', 'Mo');
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_mois');
						}
					}
					else if(o.type_alerte == "C")//%
					{
						if(o.type_periode == "0")
						{
							temp = info.CURRENTROAMINGVOLUMEDAY/info.ROAMINGPLAN;
							retour += ConsoviewFormatter.formatNumber(temp * 100, 2) + " / " + o.limit.toString() + " %";
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_jour');
						}
						else if(o.type_periode == "1")
						{
							temp = info.CURRENTROAMINGVOLUMEWEEK/info.ROAMINGPLAN;
							retour += ConsoviewFormatter.formatNumber(temp * 100, 2) + " / " + o.limit.toString() + " %";
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_semaine');
						}
						else if(o.type_periode == "2")
						{
							temp = info.ROAMINGDATA/info.ROAMINGPLAN;
							retour += ConsoviewFormatter.formatNumber(temp * 100, 2) + " / " + ConsoviewFormatter.formatNumber(o.limit, 0) + " %";
							retour += " " + ResourceManager.getInstance().getString('M26', 'par_mois');
						}
					}
					if (numAlert < 3)
						retour += "\"" + csvSeparator + "\"";
					numAlert++;
				}
			}
			while (numAlert < 3)
			{
				retour += "\"" + csvSeparator + "\"";
				numAlert++;
			}
			return retour;
		}
		
		public static function exportCSVString(urlString:String,csvString:String,fileName:String):void
		 {	
			var url:String = urlString;
	        var request:URLRequest = new URLRequest(url);         
	        var variables:URLVariables = new URLVariables();
	        variables.FILE_NAME = fileName;
	        variables.DATA = csvString;
	        request.data = variables;
	        request.method = URLRequestMethod.POST;
            navigateToURL(request,"_blank");
		 }	
	}

}
