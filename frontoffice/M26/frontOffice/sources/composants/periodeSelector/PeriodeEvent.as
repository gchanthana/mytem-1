package composants.periodeSelector
{
	import flash.events.Event;
	
	public class PeriodeEvent extends Event
	{
		public function PeriodeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		//la date de debut au format jj/mm/aaaa		
		private var _jourDeb : String;
		
		//la date de fin au format jj/mm/aaaa
		private var _jourFin : String;
		
		
		//la date de debut de periode
		private var _dateDeb : Date;
		//la date de fin de periode
		private var _dateFin : Date;
		/**
		 * Affecte la date de debut de la periode
		 * @param mois Une date au format jj/mm/aaaa
		 **/
		public function set jourDeb(mois : String):void{
			_jourDeb = mois;
		}
		
		public function get jourDeb():String{
			return _jourDeb;
		}
		
		/**
		 * Affecte la de fin de la periode
		 * @param mois Une date au format jj/mm/aaaa
		 **/		
		public function set jourFin(mois : String):void{
			_jourFin = mois;
		}
		
		public function get jourFin():String{
			return _jourFin;
		}
		
		/**
		 * Affecte la date de debut de la periode
		 * @param Date
		 **/
		public function set dateDeb(date : Date):void{
			_dateDeb = date;
		}
		
		public function get dateDeb():Date{
			return _dateDeb;
		}
		
		/**
		 * Affecte la de fin de la periode
		 * @param Date
		 **/		
		public function set dateFin(date : Date):void{
			_dateFin = date;
		}
		
		public function get dateFin():Date{
			return _dateFin;
		}
		
		
		
	}
}