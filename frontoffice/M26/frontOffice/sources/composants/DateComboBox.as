package composants
{
	import composants.util.DateFunction;
	
	import mx.collections.ArrayCollection;
	import mx.controls.ComboBox;
	import mx.formatters.DateFormatter;
	
	public class DateComboBox extends ComboBox {
		
		public function DateComboBox() {
			super();
			var formatter:DateFormatter = new DateFormatter();
			formatter.formatString = "MMMM YYYY";
			dataProvider = new ArrayCollection();
			for (var i:int=0; i<(13+DateFunction.NOW_DATE.month); i++) {
				var date:Date = new Date(DateFunction.NOW_DATE.fullYear-1,i,1);
				ArrayCollection(dataProvider).addItem(
					{ label: formatODBCDATEToFrenchLiteralDate(date), data: date }
				);
			}
			selectedIndex = 12+DateFunction.NOW_DATE.month;
		}
		
		public function get selectedMonth():Date {
			return selectedItem.data as Date;
		}
		public function set selectedMonth(value:Date):void {
			selectedIndex = value.month;
		}
		
		public static function formatODBCDATEToFrenchLiteralDate(d:Date):String{	
			var mois:String=(d.getMonth()).toString();
			var annee:String=d.getFullYear().toString();
			var v:String= DateFunction.moisEnLettre(parseInt(mois))+" "+annee;
			return  v;
		}
		
	}
}