package composants
{
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.NumberBaseRoundType;
	import mx.formatters.NumberFormatter;
	import mx.resources.ResourceManager;
	
	public class Format
	{
		public function Format()
		{
		}
		
		public static function toCurrencyString(value:Number,precision:Number,fixe:Boolean = true):String
		{
			var formattedValue:String = "";
			var cf:CurrencyFormatter = new CurrencyFormatter();
			cf.precision = precision;
			cf.rounding = NumberBaseRoundType.NONE;
			cf.decimalSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'DECIMAL_SEPARATOR');
			cf.thousandsSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'THOUSANDS_SEPARATOR');
			cf.useThousandsSeparator = true;
			cf.useNegativeSign = true;
			cf.alignSymbol = ResourceManager.getInstance().getString('ConsoModuleLib', 'ALIGN_SYMBOL');
			cf.currencySymbol = ' ' + moduleSuiviFacturationIHM.currencySymbol;
			
			if(fixe)
			{
				formattedValue = cf.format(value.toFixed(precision));	
			}
			else
			{
				formattedValue = cf.format(value);
			}
			return formattedValue;			 
		}
		
		public static function toNumber(value:Number,precision:Number,fixe:Boolean = true):String
		{
			var formattedValue:String = "";
			var cf:CurrencyFormatter = new CurrencyFormatter();
			cf.precision = precision;
			cf.rounding = NumberBaseRoundType.NONE;
			cf.decimalSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'DECIMAL_SEPARATOR');
			cf.thousandsSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'THOUSANDS_SEPARATOR');
			cf.useThousandsSeparator = true;
			cf.useNegativeSign = true;
			cf.alignSymbol = ResourceManager.getInstance().getString('ConsoModuleLib', 'ALIGN_SYMBOL');
			cf.currencySymbol = '';
			
			if(fixe)
			{
				formattedValue = cf.format(value.toFixed(precision));	
			}
			else
			{
				formattedValue = cf.format(value);
			}
			return formattedValue;			 
		}
		
		//formtae un chiffre avec la precision passÃ©e en parametre
		public static function formatePourcentPrecis(value:Number, precision:int, fixe:Boolean = true):String
		{
			var formattedValue:String = "";
			var cf:CurrencyFormatter = new CurrencyFormatter();
			cf.precision = precision;
			cf.rounding = NumberBaseRoundType.NONE;
			cf.decimalSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'DECIMAL_SEPARATOR');
			cf.thousandsSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'THOUSANDS_SEPARATOR');
			cf.useThousandsSeparator = true;
			cf.useNegativeSign = true;
			cf.alignSymbol = "right";
			cf.currencySymbol = " %";
			
			if(fixe)
			{
				formattedValue = cf.format(value.toFixed(precision));	
			}
			else
			{
				formattedValue = cf.format(value);
			}
			return formattedValue;
		}
		
	}
}