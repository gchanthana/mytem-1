package composants.tb.periode
{
	import mx.resources.ResourceManager;
	import mx.formatters.DateFormatter;
	
	public class AMonth
	{		
		private var _debut : String;
		private var _fin : String;
		private var df : DateFormatter;
		
		private var _dateFin : Date;
		private var _dateDeb : Date;
		
		
		public function AMonth(thisDay : Date){			
			var thisMonth : Date = new Date(thisDay.getFullYear(),thisDay.getMonth());
			
			df = new DateFormatter();
			
			df.formatString = ResourceManager.getInstance().getString('M32', 'DD_MM_YYYY');			
			
			_dateFin = new Date(thisMonth.getFullYear(),thisMonth.getMonth()+1,thisMonth.getDate()-1);
			_dateDeb = thisMonth;
			
			_debut = df.format(thisMonth);				
			_fin = df.format(new Date(thisMonth.getFullYear(),thisMonth.getMonth()+1,thisMonth.getDate()-1));
		}
				
		public function getDateDebut():String{
			return _debut;			
		}
		
		public function get dateDebut() : Date{
			return _dateDeb;
		}
		
		
		public function getDateFin():String{
			return _fin;
		}
		
		public function get dateFin() : Date{
			return _dateFin;
		}
	}
}