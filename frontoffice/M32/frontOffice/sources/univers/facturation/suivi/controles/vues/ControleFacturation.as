package univers.facturation.suivi.controles.vues
{
    import composants.controls.TextInputLabeled;
    import composants.tb.periode.AMonth;
    import composants.tb.periode.PeriodeEvent;
    import composants.tb.periode.PeriodeSelector;
    import composants.util.ConsoviewAlert;
    import composants.util.ConsoviewFormatter;
    import composants.util.DateFunction;
    
    import flash.display.DisplayObject;
    import flash.events.ContextMenuEvent;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.events.TimerEvent;
    import flash.ui.ContextMenu;
    import flash.utils.Timer;
    
    import mx.collections.ArrayCollection;
    import mx.containers.HBox;
    import mx.containers.Panel;
    import mx.containers.VBox;
    import mx.containers.ViewStack;
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.CheckBox;
    import mx.controls.ComboBox;
    import mx.controls.DataGrid;
    import mx.controls.Image;
    import mx.controls.Label;
    import mx.controls.dataGridClasses.DataGridColumn;
    import mx.effects.Resize;
    import mx.events.CloseEvent;
    import mx.events.CollectionEvent;
    import mx.events.CollectionEventKind;
    import mx.events.FlexEvent;
    import mx.events.ListEvent;
    import mx.managers.PopUpManager;
    import mx.managers.PopUpManagerChildList;
    import mx.resources.ResourceManager;
    
    import univers.facturation.suivi.controles.controls.AbstractControleFacture;
    import univers.facturation.suivi.controles.controls.ConfigButtons;
    import univers.facturation.suivi.controles.controls.ControleFacture;
    import univers.facturation.suivi.controles.controls.formatsclients.LogControl;
    import univers.facturation.suivi.controles.controls.formatsclients.LogControles;
    import univers.facturation.suivi.controles.services.InventaireService;
    import univers.facturation.suivi.controles.vo.Facture;
    import univers.facturation.suivi.controles.vo.InventaireVO;
    import univers.facturation.suivi.controles.vo.Operateur;
    import univers.facturation.suivi.factures.event.FactureEvent;
	
	
    public class ControleFacturation extends HBox
    {
		
		//import des icones
		[Embed(source="/assets/images/note.png")]
		public static const note:Class;
		
		[Embed(source="/assets/images/note_off.png")]
		public static const note_off:Class;
		
		[Embed(source="/assets/images/note_view.png")]
		public static const noteView:Class;
		
		[Embed(source="/assets/images/note_view_off.png")]
		public static const noteView_off:Class;
		
        public var vsSwitchEcran:ViewStack;
        public var vbFactures:VBox;
        public var vbDetailFacture:VBox;
		[Bindable] public var dgFactures:DataGrid;
        public var txtChaine:TextInputLabeled;
        public var txtFiltreFact:TextInputLabeled;
        public var lblPeriode:Label;
        public var psPeriode:PeriodeSelector;
        public var pnResult:Panel;
        public var cbVisees:ComboBox;
        public var cbViseesAna:ComboBox;        
		public var cbControlees:ComboBox;
		public var cbControleesInv:ComboBox;
        public var cbExportees:ComboBox;
		
        public var cmbOperateur:ComboBox;
        public var imgFactureCat:Image;
        public var imgFacture:Image;
        public var imgXLS:Image;
        public var imgCVS:Image;
        public var btRechercher:Button;
        public var btConfig:Button;
        public var btControler:Button;		
        private const annulAnasize:int = 160;
        private const maxsize:int = 150;
        private const intermediairesize:int = 140;
        private const decisionsize:int = 100;
        private const minsize:int = 0;
		/*boolcalc variable qui permet de savoir si l'export se fait avec le calcul ou non*/
		public var boolcalc:int = 0;
        
		[Bindable]public var btViserAna:Button;
		[Bindable]public var btEditVisaAnalytique:Button;
		[Bindable]public var btAnnulVisaAnalytique:Button;
		[Bindable]public var btViser:Button;
		[Bindable]public var btAnnulViser:Button;
		[Bindable]public var btValiderControle:Button;
		[Bindable]public var btAnnulerControle:Button;
		[Bindable]public var btValiderControleInv:Button;
		[Bindable]public var btAnnulerControleInv:Button;
		
		[Bindable]public var btDecisionERP:Button;
		[Bindable]public var btAnnulerDecisionERP:Button;
		
		//		public var btExporter : Button.
		//		public var btNePasExporter : Button.
		//		******
		
        private var btnAnnulAna:Boolean = false;
		[Bindable]
        protected var _lgCtrl:LogControl = new LogControl();
		[Bindable]
		protected var _lgCtrles:LogControles = new LogControles(_lgCtrl);
        private var _configbtn:ConfigButtons = new ConfigButtons();
		private var _inventaire:Inventaire = new Inventaire();
        //******
        public var lblSeuil:Label;
        public var lblCumulRatio:Label;
        [Bindable]
        public var ckbEvolution:CheckBox;
        //effet
        public var expand:Resize;
        public var contract:Resize;
        private var dateDebut:Date;
        private var dateFin:Date;
        private var confirmBox:ConfirmBox;
        public var fenetreDeControle:ControleDeLaFactureIHM;
		public var inventaire:InventaireIHM;
        private var fenetreDeCongig:ConfigRatioWindowIHM;
		private var alert:Alert;
        [Bindable]
        protected var ctrlFactures:AbstractControleFacture;
		
		[Bindable]
		public var inventaireService:InventaireService= new InventaireService();
		
		[Bindable]
		public var paramInventaire:InventaireVO = new InventaireVO();
		
	
		
        //[Bindable]
        //protected var ctrlButtons : ConfigButtons;
        [Bindable]
        public var myContextMenu:ContextMenu;
		
        public function ControleFacturation()
        {
            super();
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
        }

        private function initIHM(event:FlexEvent):void
        {
            txtChaine.addEventListener(KeyboardEvent.KEY_DOWN, btRechercherClickHandler);
        }
		
        public function onPerimetreChange():void
        {
            vsSwitchEcran.selectedIndex = 0;
            ckbEvolution.selected = false;
            ctrlFactures = new ControleFacture();
			ctrlFactures.listeFactures.addEventListener(CollectionEvent.COLLECTION_CHANGE,listeFactureCollectionChangeHandler);
            ctrlFactures.boolInventaire = CvAccessManager.getSession().CURRENT_PERIMETRE.BOOLINVENTAIRE;
            ctrlFactures.getListeOperateurs();
            psPeriode.onPerimetreChange();
            pnResult.visible = true;
            initPeriode();
            _lgCtrles.getUserConnectedProfile();
        }  

        //LABEL FUNCTION POUR DATAGRIDS		
        protected function formateDates(item:Object, column:DataGridColumn):String
        {
            if (item != null)
            {
                var ladate:Date = new Date(item[column.dataField]);
                return DateFunction.formatDateAsString(ladate);
            }
            else
                return "-";
        }

        protected function formateEuros(item:Object, column:DataGridColumn):String
        {
            if (item != null)
                return moduleSuiviFacturationIHM.formatNumberCurrency(item[column.dataField], 2);
            else
                return "-";
        }

        protected function formateNumber(item:Object, column:DataGridColumn):String
        {
            if (item != null)
			{
				if (CvAccessManager.getSession().USER.GLOBALIZATION == "en_GB" || CvAccessManager.getSession().USER.GLOBALIZATION == "en_US")
					return moduleSuiviFacturationIHM.currencySymbol + moduleSuiviFacturationIHM.formatNumber(Number(item[column.dataField]), 2);
				else
                	return moduleSuiviFacturationIHM.formatNumber(Number(item[column.dataField]), 2) + " " + moduleSuiviFacturationIHM.currencySymbol;
			}
            else
                return "-";
        }

        protected function formateLigne(item:Object, column:DataGridColumn):String
        {
            if (item != null)
                return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);
            else
                return "-";
        }

        protected function formateBoolean(item:Object, column:DataGridColumn):String
        {
            if (item != null)
                return (Number(item[column.dataField]) == 1) ? "OUI" : "NON";
            else
                return "-";
        }

        //Handlers
        protected function init(fe:FlexEvent):void
        {
            ctrlFactures = new ControleFacture();
            ctrlFactures.boolInventaire = CvAccessManager.getSession().CURRENT_PERIMETRE.BOOLINVENTAIRE;
			ctrlFactures.listeFactures.addEventListener(CollectionEvent.COLLECTION_CHANGE,listeFactureCollectionChangeHandler);
            ctrlFactures.getListeOperateurs();
            pnResult.visible = true;
            
			//initPeriode();
			
            psPeriode.addEventListener("SPECIAL_DATE_SELECTOR_READY", onSpecialDateSelectorReadyHandler);
            _lgCtrles.getUserConnectedProfile();			
        }
		
		protected function listeFactureCollectionChangeHandler(evt:CollectionEvent):void
		{
			if(evt.kind == CollectionEventKind.UPDATE)
			{
				updateGrid();
			}
		}
		
		protected function listeFactureCollectionChangeHandler1(evt:FactureEvent):void
		{
			updateGrid();		
		}

        protected function btConfigClickHandler(me:MouseEvent):void
        {
            fenetreDeCongig = new ConfigRatioWindowIHM();
            fenetreDeCongig.controleFacture = ctrlFactures;
            PopUpManager.addPopUp(fenetreDeCongig, DisplayObject(parentApplication), true);
            PopUpManager.centerPopUp(confirmBox);
        }

        /*protected function btRechercherClickHandler(me : MouseEvent):void{
           ctrlFactures.setParamsRecherches(Number(cbControlees.selectedItem.value),
           Number(cbExportees.selectedItem.value),
           Number(cbVisees.selectedItem.value),
           Operateur(cmbOperateur.selectedItem).id,
           txtChaine.text,
           dateDebut,
           dateFin);

        }*/
		
        protected function btRechercherClickHandler(event:Event):void
        {
			imgFactureCat.source = note_off;
			imgFactureCat.buttonMode = false;
			imgFacture.source = noteView_off;
			imgFacture.buttonMode = false;
			boolcalc = 0;
            if (event is MouseEvent)
            {
                ctrlFactures.listeFactures.removeAll();
                ctrlFactures.setParamsRecherches(Number(cbControlees.selectedItem.value),Number(cbControleesInv.selectedItem.value), Number(cbExportees.selectedItem.value), Number(cbVisees.selectedItem.value), Number(cbViseesAna.selectedItem.value), Operateur(cmbOperateur.selectedItem).id, txtChaine.text, dateDebut, dateFin);
            }
            else if (event is KeyboardEvent)
            {
                if ((event as KeyboardEvent).keyCode == 13)
                {
                    ctrlFactures.listeFactures.removeAll();
                    ctrlFactures.setParamsRecherches(Number(cbControlees.selectedItem.value), Number(cbControleesInv.selectedItem.value), Number(cbExportees.selectedItem.value), Number(cbVisees.selectedItem.value), Number(cbViseesAna.selectedItem.value), Operateur(cmbOperateur.selectedItem).id, txtChaine.text, dateDebut, dateFin);
                }
            }
            ctrlFactures.addEventListener("FACTURE_LISTED", txtFiltreFactChangeHandler);
        }
		
		//Retourne un rapport des produits en erreur
		protected function rapportProduitErreur(event:MouseEvent):void
		{
			
			ctrlFactures.rapportProduitErreur();
			
		}

        //*********************************************************************************
        //Handler du Change pour le Grid liste des Factures
        protected function dgFacturesChangeHandler(le:ListEvent):void
        {
            updateGrid();
        }

        private function updateGrid():void
        {
            var facture:Facture;
            var array:Array;
			
			var verif:Boolean;
			if( !moduleSuiviFacturationIHM.userAcess.hasOwnProperty('D_FACT') || (moduleSuiviFacturationIHM.userAcess.hasOwnProperty('D_FACT') && (moduleSuiviFacturationIHM.userAcess.D_FACT>0)) )
			{verif=true;}
			else
			{verif=false;}
			
            if (dgFactures.selectedIndex != -1) 
            {
                facture = dgFactures.selectedItem as Facture;
                ctrlFactures.selectedFacture = facture;
                array = [ true ];
                callLater(activerBouttons, array);
				//activerBouttons(true);
				imgFactureCat.source = verif ? note : note_off;
				imgFactureCat.buttonMode = verif;
				imgFactureCat.enabled = verif;
				imgFacture.source = verif ? noteView : noteView_off;
				imgFacture.buttonMode =verif;
				imgFacture.enabled = verif;
            }
            else
            {
                facture = new Facture();
                ctrlFactures.selectedFacture = facture;
                array = [ false ];
                callLater(activerBouttons, array);
				imgFactureCat.source = note_off;
				imgFactureCat.buttonMode = false;
				imgFactureCat.enabled = false;
				imgFacture.source = noteView_off;
				imgFacture.buttonMode = false;
				imgFacture.enabled = false;
            }
			
            array = [ dgFactures.selectedItem as Facture ];
            callLater(updateBtValiderControleLabel, array);
			callLater(updateBtValiderControleInvLabel, array);
            callLater(updateBtViseeLabel, array);
            callLater(updateBtViseeAnaLabel, array);
            callLater(updateBtExporterLabel, array);
        }

        //handler de l'update pour le Grid liste des factures;
        protected function dgFacturesUpdateCompleteHandler(ev:Event):void
        {
            if (dgFactures.initialized && ctrlFactures != null && (dgFactures.selectedIndex == -1))
            {
                updateGrid();
            }
        }
		
		protected function onClick_imgCalcul(e:MouseEvent):void
		{
				if (ctrlFactures.listeFactures.length>0)
				ConsoviewAlert.afficherAlertConfirmation(resourceManager.getString('M32','_calc_prend_qlq_min'),"", alertClickHandler);
				
		}		
		
		 //permet de capter la réponse de l'utilisateur dans la popup d'alert et lance le calcul
		
		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				var facture:Facture;
				var tabperiod:Array= new Array();
				var i:int=0;
				
				for each (facture in ctrlFactures.listeFactures)
				{
					ctrlFactures.selectedFacture =facture;
					tabperiod[i]=facture.periodeId;
					i++;
				}
				ctrlFactures.calculMontantFacture(tabperiod);
				boolcalc = 1;
			}
		}

        //handler du click sur l'image imgFactureCat
        protected function imgFactureCatClickHandler(me:MouseEvent):void
        {
			var verif:Boolean = (moduleSuiviFacturationIHM.userAcess.hasOwnProperty('D_FACT'))? !(moduleSuiviFacturationIHM.userAcess.D_FACT == 0) : true;
			
            if ((dgFactures.selectedIndex != -1) && verif)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                ctrlFactures.selectedFacture = facture;
                ctrlFactures.exporterLaFacture(AbstractControleFacture.PDF_FACTURECAT,0);
            }
        }

        //handler du click sur l'image imgFacture
        protected function imgFactureClickHandler(me:MouseEvent):void
        {
			var verif:Boolean = (moduleSuiviFacturationIHM.userAcess.hasOwnProperty('D_FACT'))? !(moduleSuiviFacturationIHM.userAcess.D_FACT == 0) : true;
			
            if ((dgFactures.selectedIndex != -1) && verif)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                ctrlFactures.selectedFacture = facture;
                ctrlFactures.exporterLaFacture(AbstractControleFacture.PDF_FACTURE,0);
            }
        }

        //handler du click sur l'image imgCVS
        protected function imgCVSClickHandler(me:MouseEvent):void
        {
            if (ctrlFactures.listeFactures.length > 0)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                ctrlFactures.selectedFacture = facture;
                ctrlFactures.exporterLaFacture(AbstractControleFacture.CSV_LISTEFACTURE,boolcalc);
            }
        }

        //handler du click sur l'image imgXLS
        protected function imgXLSClickHandler(me:MouseEvent):void
        {            
			
			if (ctrlFactures.listeFactures.length > 0)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                ctrlFactures.selectedFacture = facture;
                ctrlFactures.exporterLaFacture(AbstractControleFacture.XLS_LISTEFACTURE,boolcalc);
            }
        }

        //Ouvre un PopUp de commentaire lors du clic sur le bouton 'Viser la facture'
        protected function btViserClickHandler(me:MouseEvent):void
        {
            if (dgFactures.selectedIndex != -1)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                var message:String = "";
                if (facture.visee == 0)
                {
                    message = resourceManager.getString('M32','_nFacture_vis_e_') + ctrlFactures.construireSignature();
                }
                else
                {
                    message = facture.commentaireViser + resourceManager.getString('M32','_nVisa_annul__') + ctrlFactures.construireSignature();
                }
                demanderConfirmation(btViser.label, message, executerBtViserClick);
            }
        }

        //Execute l'action du click sur le bouton taguer apres confirmation et envoi le log
        private function executerBtViserClick():void
        {
            var facture:Facture = dgFactures.selectedItem as Facture;
            facture.commentaireViser = confirmBox.txRapport + "\n" + confirmBox.commentaire + "\n\n";
            ctrlFactures.selectedFacture = facture;
            ctrlFactures.updateVisaFacture();
            if (facture.visee == 0)
            {
                _lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId, _lgCtrl.whatIsThisButton(0, 0), confirmBox.txtCommentaire.text);
            }
            else
            {
                _lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId, _lgCtrl.whatIsThisButton(0, -1), confirmBox.txtCommentaire.text);
            }
            confirmBox = null;
        }

        //Ouvre un PopUp de commentaire lors du clic sur le bouton 'Valider la facture' et ouvre un PopUp de vérification de la facture
        protected function btControlerClickHandler(me:MouseEvent):void
        {
            if (dgFactures.selectedIndex != -1)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                ctrlFactures.selectedFacture = facture;
				paramInventaire.idInventaire = ctrlFactures.selectedFacture.periodeId;
				
				inventaireService.getInventaireFacture(paramInventaire);
				inventaireService.getInventaireNonFacture(paramInventaire);
				inventaireService.getLibelleCompte(facture.operateurId);
                callLater(afficherFenetreDeControle);
            }
        }

        //Ouvre un PopUp de commentaire lors du clic sur le bouton 'Decision ERP'
        protected function btDecisionERPClickHandler(me:MouseEvent):void
        {
            if (dgFactures.selectedIndex != -1)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                var message:String = "";
                var popup:PopupERPIHM = new PopupERPIHM();
                _lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId, resourceManager.getString('M32','D_cision_ERP'), "");
                PopUpManager.addPopUp(popup, this, true);
                popup.signature = ctrlFactures.construireSignature();
                popup.getsObject(_lgCtrles, facture, dgFactures, ctrlFactures);
                PopUpManager.centerPopUp(popup);
            }
        }

        //Ouvre un PopUp de commentaire lors du clic sur le bouton 'Annuler Decision ERP'
        protected function btAnnulerDecisionERPClickHandler(me:MouseEvent):void
        {
            if (dgFactures.selectedIndex != -1)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                var message:String = "";
                var libelle:String = "";
                if (facture.exportee == 3 || facture.exportee == 2)
                {
                    message = resourceManager.getString('M32','D_cision_ERP_annule_') + ctrlFactures.construireSignature();
                    libelle = resourceManager.getString('M32','_nAnnuler_d_cision_ERP_');
                }
                else
                {
                    message = " ";
                }
                demanderConfirmation(libelle, message, executerbtAnnulerDecisionERPClick);
            }
        }

        //Execute l'action du click sur le bouton 'Exporter' apres confirmation et envoie le log
        private function executerbtAnnulerDecisionERPClick():void
        {
            var facture:Facture = dgFactures.selectedItem as Facture;
            facture.commentaireExporter = confirmBox.txRapport + "\n" + confirmBox.commentaire + "\n\n";
            ctrlFactures.selectedFacture = facture;
            facture.exportee = 0;
            ctrlFactures.updateVisaExporteFacture();
            _lgCtrles.setDecisionExport((dgFactures.selectedItem as Facture).periodeId, 0, confirmBox.txtCommentaire.text);
            _lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId, resourceManager.getString('M32','D_cision_ERP_annule_'), confirmBox.txtCommentaire.text);
            confirmBox = null;
        }

//		//Ouvre un PopUp de commentaire lors du clic sur le bouton 'Exporter'
//		protected function  btExporterClickHandler(me : MouseEvent):void{ 
//			if (dgFactures.selectedIndex != -1){	
//				var facture : Facture = dgFactures.selectedItem as Facture;
//				var message : String = "";
//				var libelle : String = "";
//				
//				if (facture.exportee == 3){
//					message = "\nExporter."+ ctrlFactures.construireSignature();
//					libelle = "Exporter";
//				}else if (facture.exportee == 2){
//					message = facture.commentaireExporter + "\nNe pas exporter."+ctrlFactures.construireSignature();
//					libelle = "Ne pas exporter";
//				}else{
//					message = " ";
//				}
//				demanderConfirmation(libelle,message,executerBtExporterClick);
//			}
//		}
        //Ouvre un PopUp de commentaire lors du clic sur le bouton 'Controler la facture' sans vérifier de la facture		
        protected function btValiderControleClickHandler(me:MouseEvent):void
        {
            if (dgFactures.selectedIndex != -1)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                var message:String = "";
                if (facture.controlee == 0)
                {
                    message = resourceManager.getString('M32','_nContr_le_valid__') + ctrlFactures.construireSignature();
                }
                else
                {
                    message = facture.commentaireControler + resourceManager.getString('M32','_nContr_le_annul__') + ctrlFactures.construireSignature();
                }
                demanderConfirmation(btValiderControle.label, message, executerBtValiderControleClick);
            }
        }

        //Execute l'action du click sur le bouton taguer apres confirmation
        private function executerBtValiderControleClick():void
        {
            var facture:Facture = dgFactures.selectedItem as Facture;
            facture.commentaireControler = confirmBox.txRapport + "\n" + confirmBox.commentaire + "\n\n";
            ctrlFactures.selectedFacture = facture;
            ctrlFactures.updateVisaControleFacture();
            if (facture.controlee == 0)
            {
                _lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId, _lgCtrl.whatIsThisButton(2, 0), confirmBox.txtCommentaire.text);
            }
            else
            {
                _lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId, _lgCtrl.whatIsThisButton(2, -1), confirmBox.txtCommentaire.text);
            }
            confirmBox = null;
        }
		
		protected function btValiderControleInvClickHandler(me:MouseEvent):void
		{
			if (dgFactures.selectedIndex != -1)
			{
				var facture:Facture = dgFactures.selectedItem as Facture;
				var message:String = "";
				if (facture.controleeInv == 0)
				{
					message = resourceManager.getString('M32','_nContr_le_valid__') + ctrlFactures.construireSignature();
				}
				else
				{
					message = facture.commentaireControlerInv + resourceManager.getString('M32','_nContr_le_annul__') + ctrlFactures.construireSignature();
				}
				demanderConfirmation(btValiderControleInv.label, message, executerBtValiderControleInvClick);
			}
		}
		
		//Execute l'action du click sur le bouton taguer apres confirmation
		private function executerBtValiderControleInvClick():void
		{
			var facture:Facture = dgFactures.selectedItem as Facture;
			facture.commentaireControlerInv = confirmBox.txRapport + "\n" + confirmBox.commentaire + "\n\n";
			ctrlFactures.selectedFacture = facture;
			ctrlFactures.updateVisaControleFactureInventaire();
			if (facture.controleeInv == 0)
			{
				_lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId, _lgCtrl.whatIsThisButton(2, 0), confirmBox.txtCommentaire.text);
			}
			else
			{
				_lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId, _lgCtrl.whatIsThisButton(2, -1), confirmBox.txtCommentaire.text);
			}
			confirmBox = null;
		}
		
        private var twControleAnalitique:ControleAnalitiqueView;

        protected function btViserAnaClickHandler(me:MouseEvent):void
        {
            if (dgFactures.selectedIndex != -1)
            {
                if (twControleAnalitique != null)
                    twControleAnalitique = null;
                var facture:Facture = dgFactures.selectedItem as Facture;
                if (facture.viseeAna == 0)
                {
                    _lgCtrl.EtatEditerVisaAnalytique = 0;
                    ctrlFactures.selectedFacture = facture;
                    ctrlFactures.montantfactureEclateeByOrga = null;
                    ctrlFactures.totalfactureByOrga = 0;
                    ctrlFactures.percentMontantViseAnalytique = 0;
                    ctrlFactures.montantNonAffecte = 0;
                    ctrlFactures.arrayNumber = 0;
                    ControleAnalitiqueImpl.zReturnLogCtrl(_lgCtrl);
                    ctrlFactures.visaAnalytiqueleSaveList = new ArrayCollection();
                    twControleAnalitique = new ControleAnalitiqueView();
                    twControleAnalitique.addEventListener(PopUpBaseImpl.VALIDER_CLICKED, twControleAnalitiqueValiderClickHandler);
                    twControleAnalitique.controleFacture = ctrlFactures;
                    ctrlFactures.getListeOrganisations();
                    PopUpManager.addPopUp(twControleAnalitique, this, true, PopUpManagerChildList.PARENT);
                    PopUpManager.centerPopUp(twControleAnalitique);
                    btnAnnulAna = false;
                }
                else
                {
                    if (facture.commentaireViserAna)
                    {
                        btnAnnulAna = true;
                        var message:String = facture.commentaireViserAna + "\nVisa analytique annulé." + ctrlFactures.construireSignature();
                        demanderConfirmation(btViserAna.label, message, executerTwControleAnalitiqueValiderClickHandler);
                    }
                }
            }
        }

        protected function twControleAnalitiqueValiderClickHandler(ev:Event):void
        {
            if (dgFactures.selectedIndex != -1)
            {
                demanderConfirmation(btViserAna.label, ctrlFactures.construireMemoControleAnalytique(), executerTwControleAnalitiqueValiderClickHandler);
            }
        }

        //Execute l'action du click sur le bouton taguer l'analytique apres confirmation		
        protected function executerTwControleAnalitiqueValiderClickHandler():void
        {
            var facture:Facture = dgFactures.selectedItem as Facture;
            facture.commentaireViserAna = confirmBox.txRapport + "\n" + confirmBox.commentaire + "\n\n";
            ctrlFactures.selectedFacture = facture;
			ctrlFactures.addEventListener(FactureEvent.LISTED_FACTURE_CHANGE,listeFactureCollectionChangeHandler1);
            ctrlFactures.updateVisaAnaFacture();
            if (btnAnnulAna == false)
            {
                _lgCtrles.setLogsControleFactureAnalytique(_lgCtrl.logControlAna_ArrayListFacturesAnalytique, (dgFactures.selectedItem as Facture).periodeId, _lgCtrl.whatIsThisButton(1, btViserAna.width.valueOf()), confirmBox.txtCommentaire.text);
                    //_lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId,_lgCtrl.whatIsThisButton(1, btViserAna.width.valueOf()),confirmBox.txtCommentaire.text);
            }
            else
            {
                _lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId, _lgCtrl.whatIsThisButton(1, btViserAna.width.valueOf()), confirmBox.txtCommentaire.text);
            }
            confirmBox = null;
            PopUpManager.removePopUp(twControleAnalitique);
            btnAnnulAna = false;
        }

        protected function btEditerAnaClickHandler(me:MouseEvent):void
        {
            if (dgFactures.selectedIndex >= 0)
            {
                if (twControleAnalitique != null)
                    twControleAnalitique = null;
                var facture:Facture = dgFactures.selectedItem as Facture;
                if (facture.viseeAna == 1)
                {
//					_lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId,_lgCtrl.whatIsThisButton(3, btEditVisaAnalytique.width.valueOf()),"");
                    _lgCtrl.EtatEditerVisaAnalytique = 1;
                    _lgCtrl.LogControles_PeriodeID = (dgFactures.selectedItem as Facture).periodeId;
                    ctrlFactures.selectedFacture = facture;
                    ctrlFactures.montantfactureEclateeByOrga = null;
                    ctrlFactures.totalfactureByOrga = 0;
                    ctrlFactures.percentMontantViseAnalytique = 0;
                    ctrlFactures.montantNonAffecte = 0;
                    ctrlFactures.arrayNumber = 0;
                    ControleAnalitiqueImpl.zReturnLogCtrl(_lgCtrl);
                    ctrlFactures.visaAnalytiqueleSaveList = new ArrayCollection();
                    twControleAnalitique = new ControleAnalitiqueView();
                    twControleAnalitique.addEventListener(PopUpBaseImpl.VALIDER_CLICKED, twControleAnalitiqueEditerClickHandler);
                    twControleAnalitique.controleFacture = ctrlFactures;
                    ctrlFactures.getListeOrganisations();
                    PopUpManager.addPopUp(twControleAnalitique, this, true, PopUpManagerChildList.PARENT);
                    PopUpManager.centerPopUp(twControleAnalitique);
                }
            }
        }

        protected function twControleAnalitiqueEditerClickHandler(ev:Event):void
        {
            if (dgFactures.selectedIndex != -1)
            {
                demanderConfirmation(btViserAna.label, ctrlFactures.construireMemoControleAnalytique(), executerTwControleAnalitiqueEditerClickHandler);
            }
        }

        //Execute l'action du click sur le bouton taguer l'analytique apres confirmation		
        protected function executerTwControleAnalitiqueEditerClickHandler():void
        {
            var facture:Facture = dgFactures.selectedItem as Facture;
            facture.commentaireViserAna = confirmBox.txRapport + "\n" + confirmBox.commentaire + "\n\n";
            ctrlFactures.selectedFacture = facture;
            ctrlFactures.updateVisaAnaFactureEditer();
            if (_lgCtrl.logControlAna_ArrayListFacturesAnalytique.length != 0)
            {
                _lgCtrles.setLogsControleFactureAnalytique(_lgCtrl.logControlAna_ArrayListFacturesAnalytique, (dgFactures.selectedItem as Facture).periodeId, _lgCtrl.whatIsThisButton(1, btEditVisaAnalytique.width.valueOf()), confirmBox.txtCommentaire.text);
            }
            else
            {
//					_lgCtrles.setLogsControleFacture((dgFactures.selectedItem as Facture).periodeId,_lgCtrl.whatIsThisButton(3, btEditVisaAnalytique.width.valueOf()),
//													confirmBox.txtCommentaire.text);
            }
            confirmBox = null;
            PopUpManager.removePopUp(twControleAnalitique);
        }

        protected function fenetreDeControleValiderHandler(ev:Event):void
        {
        }

        protected function fenetreDeControleRetourHandler(ev:Event):void
        {
            fenetreDeControle.reset();
			inventaire.initInventaire();
            vsSwitchEcran.selectedChild = vbFactures;
        }

        protected function txtFiltreFactChangeHandler(e:Event):void
        {
            if (dgFactures.dataProvider != null)
            {
                (dgFactures.dataProvider as ArrayCollection).filterFunction = filtrerDgFactures;
                (dgFactures.dataProvider as ArrayCollection).refresh();
            }
        }

        //Mise à jour de la période
        protected function periodeEventHandler(pe:PeriodeEvent):void
        {
            dateDebut = pe.dateDeb;
            dateFin = pe.dateFin;
            setSelectedPeriode(dateDebut, dateFin);
        }

        //Evolution des coûts
        protected function ckbEvolutionClickHandler(me:MouseEvent):void
        {
            ctrlFactures.boolEvolution = ckbEvolution.selected;
        }

        ///----------
        //filtre le tableau des factures
        private function filtrerDgFactures(facture:Facture):Boolean
        {
            if ((facture.operateurLibelle.toLowerCase().search(txtFiltreFact.text.toLowerCase()) != -1) || (facture.numero.toLowerCase().search(txtFiltreFact.text.toLowerCase()) != -1) || (facture.compteFacturation.toLowerCase().search(txtFiltreFact.text.toLowerCase()) != -1) || (facture.dateEmission.toDateString().toLowerCase().search(txtFiltreFact.text.toLowerCase()) != -1) || (facture.dateDebut.toDateString().search(txtFiltreFact.text.toLowerCase()) != -1) || (facture.dateFin.toDateString().toLowerCase().search(txtFiltreFact.text.toLowerCase()) != -1) || (facture.montant.toString().toLowerCase().search(txtFiltreFact.text.toLowerCase()) != -1))
                return true;
            else
                return false;
        }

        //affiche la fenetre de controle de la facture
        private function afficherFenetreDeControle():void
        {
            if (dgFactures.selectedItem != null)
            {
                vsSwitchEcran.selectedChild = vbDetailFacture;
                if (fenetreDeControle != null)
                {
                    ctrlFactures.initSectionControle();
                    ctrlFactures.controlerFacture();
                    ctrlFactures.initSectionInventaire();
                    callLater(fenetreDeControle.reset);
                    if (ctrlFactures.boolInventaire)
                    {
                        getRessourcesHorsInventaire();
                    }
                }
            }
        }
        private var t:Timer = new Timer(1001, 1);

        //recuprer les infos pour l'inventaire apres une attente de une seconde
        //pour palier au 'bundlage' par le flashplayer des requêtes émises dans la mm frameSet		
        public function getRessourcesHorsInventaire():void
        {
            t.start();
            t.addEventListener(TimerEvent.TIMER_COMPLETE, _tTimerCompleteHandler);
        }

        protected function _tTimerCompleteHandler(event:TimerEvent):void
        {
            ctrlFactures.getDetailRessourcesHorsInventaire();
            t.stop();
            t.removeEventListener(TimerEvent.TIMER_COMPLETE, _tTimerCompleteHandler);
        }

        protected function fenetreDeControleCreationCompleteHandler(fe:FlexEvent):void
        {
            fenetreDeControle.addEventListener(ControleDeLaFacture.VALIDER, fenetreDeControleValiderHandler);
            fenetreDeControle.addEventListener(ControleDeLaFacture.RETOUR, fenetreDeControleRetourHandler);
			inventaire.addEventListener(Inventaire.RETOUR, fenetreDeControleRetourHandler);
        }

        //********************************************************************************************************************************************
        private function updateBtViseeLabel(facture:Facture):void
        {
            if (facture != null)
            {
                if (facture.visee)
                {
                    //btViser.label = "Annuler le visa facture papier";
                    btViser.width = minsize;
                    btViser.visible = false;
					btViser.includeInLayout=false;
                    btAnnulViser.width = intermediairesize;
                    btAnnulViser.visible = true;
					btAnnulViser.includeInLayout=true;
                        //ConfigButtons.btnViserFacturePapier=false;
                        //ConfigButtons.btnAnnulerLeVisaPapier=true;
                }
                else
                {
                    //btViser.label = "Viser la facture papier";
                    btViser.width = intermediairesize;
                    btViser.visible = true;
					btViser.includeInLayout=true;
                    btAnnulViser.width = minsize;
                    btAnnulViser.visible = false;
					btAnnulViser.includeInLayout=false;
                        //ConfigButtons.btnViserFacturePapier=true;
                        //ConfigButtons.btnAnnulerLeVisaPapier=false;
                }
            }
            else
            {
                //btViser.label = "Viser  la facture papier";
                btViser.width = intermediairesize;
                btViser.visible = true;
				btViser.includeInLayout=true;
                btAnnulViser.width = minsize;
                btAnnulViser.visible = false;
				btAnnulViser.includeInLayout=false;
                    //ConfigButtons.btnViserFacturePapier=false;
                    //ConfigButtons.btnAnnulerLeVisaPapier=true;
            }
        }

        private function updateBtViseeAnaLabel(facture:Facture):void
        {
            if (facture != null)
            {
                if (facture.viseeAna && btAnnulVisaAnalytique.width == annulAnasize)
                {
                    btViserAna.width = minsize;
                    btViserAna.visible = false;
					btViserAna.includeInLayout=false;
                    btEditVisaAnalytique.width = maxsize;
                    btEditVisaAnalytique.visible = true;
					btEditVisaAnalytique.includeInLayout=true;
                    btAnnulVisaAnalytique.width = annulAnasize;
                    btAnnulVisaAnalytique.visible = true;
					btAnnulVisaAnalytique.includeInLayout=true;
                }
                if (facture.viseeAna)
                {
                    //btViserAna.label = "Annuler le visa analytique";
                    btViserAna.width = minsize;
                    btViserAna.visible = false;
					btViserAna.includeInLayout=false;
                    btEditVisaAnalytique.width = maxsize;
                    btEditVisaAnalytique.visible = true;
					btEditVisaAnalytique.includeInLayout=true;
                    btAnnulVisaAnalytique.width = annulAnasize;
                    btAnnulVisaAnalytique.visible = true;
					btAnnulVisaAnalytique.includeInLayout=true;
                        //btAnnulVisaAnalytique.width=maxsize;
                        //ConfigButtons.btnViserFacturePapier=false;
                        //ConfigButtons.btnEditerAnalytique=true;
                        //ConfigButtons.btnAnnulerVisaAnalytique=true;
                }
                else
                {
                    //btViserAna.label = "Viser l'analytique";
                    btViserAna.width = intermediairesize;
                    btViserAna.visible = true;
					btViserAna.includeInLayout=true;
                    btEditVisaAnalytique.width = minsize;
                    btEditVisaAnalytique.visible = false;
					btEditVisaAnalytique.includeInLayout=false;
                    btAnnulVisaAnalytique.width = minsize;
                    btAnnulVisaAnalytique.visible = false;
					btAnnulVisaAnalytique.includeInLayout=false;
                        //btAnnulVisaAnalytique.width=minsize;
                        //ConfigButtons.btnViserFacturePapier=true;
                        //ConfigButtons.btnEditerAnalytique=false;
                        //ConfigButtons.btnAnnulerVisaAnalytique=false;
                }
            }
            else
            {
                //btViserAna.label = "Viser l'analytique";
                btViserAna.width = intermediairesize;
                btViserAna.visible = true;
				btViserAna.includeInLayout=true;
                btEditVisaAnalytique.width = minsize;
                btEditVisaAnalytique.visible = false;
				btEditVisaAnalytique.includeInLayout=false;
                btAnnulVisaAnalytique.width = minsize;
                btAnnulVisaAnalytique.visible = false;
				btAnnulVisaAnalytique.includeInLayout=false;
                    //btAnnulVisaAnalytique.width=minsize;
                    //ConfigButtons.btnViserFacturePapier=true;
                    //ConfigButtons.btnEditerAnalytique=false;
                    //ConfigButtons.btnAnnulerVisaAnalytique=false;
            }
        }

        private function updateBtValiderControleLabel(facture:Facture):void
        {
            if (facture != null)
            {
                if (facture.controlee)
                {
                    //btValiderControle.label = "Annuler le contrôle";
                    btValiderControle.width = minsize;
                    btValiderControle.visible = false;
					btValiderControle.includeInLayout=false;
                    btAnnulerControle.width = intermediairesize;
                    btAnnulerControle.visible = true;
					btAnnulerControle.includeInLayout=true;
                        //ConfigButtons.btnValiderControle=false;
                        //ConfigButtons.btnAnnulerControle=true;
                }
                else
                {
                    //btValiderControle.label = "Valider le contrôle";
                    btValiderControle.width = intermediairesize;
                    btValiderControle.visible = true;
					btValiderControle.includeInLayout=true;
                    btAnnulerControle.width = minsize;
                    btAnnulerControle.visible = false;
					btAnnulerControle.includeInLayout=false;
                        //ConfigButtons.btnValiderControle=true;
                        //ConfigButtons.btnAnnulerControle=false;
                }
            }
            else
            {
                //btValiderControle.label = "Valider le contrôle";
                btValiderControle.width = intermediairesize;
                btValiderControle.visible = true;
				btValiderControle.includeInLayout=true;
                btAnnulerControle.width = minsize;
                btAnnulerControle.visible = false;
				btAnnulerControle.includeInLayout=false;
                    //ConfigButtons.btnValiderControle=true;
                    //ConfigButtons.btnAnnulerControle=false;
            }
        }
		
		private function updateBtValiderControleInvLabel(facture:Facture):void
		{
			if (facture != null)
			{
				if (facture.controleeInv)
				{
					btValiderControleInv.width = minsize;
					btValiderControleInv.visible = false;
					btValiderControleInv.includeInLayout=false;
					btAnnulerControleInv.width = maxsize;
					btAnnulerControleInv.visible = true;
					btAnnulerControleInv.includeInLayout=true;
				}
				else{
					btValiderControleInv.width = maxsize;
					btValiderControleInv.visible = true;
					btValiderControleInv.includeInLayout=true;
					btAnnulerControleInv.width = minsize;
					btAnnulerControleInv.visible = false;
					btAnnulerControleInv.includeInLayout=false;
				}
			}
			else{
				btValiderControleInv.width = maxsize;
				btValiderControleInv.visible = true;
				btValiderControleInv.includeInLayout=true;
				btAnnulerControleInv.width = minsize;
				btAnnulerControleInv.visible = false;
				btAnnulerControleInv.includeInLayout=false;
			}
		}
			

        private function updateBtExporterLabel(facture:Facture):void
        {
            if (facture != null)
            {
                btDecisionERP.width = minsize;
                btDecisionERP.visible = false;
				btDecisionERP.includeInLayout=false;
                btAnnulerDecisionERP.width = minsize;
                btAnnulerDecisionERP.visible = false;
				btAnnulerDecisionERP.includeInLayout=false;
                switch (facture.exportee)
                {
                    case 0:
                        zVisibleOrNot(btDecisionERP, true);
                        break;
                    case AbstractControleFacture.BLOQUER_ACTION_FACTURE:
                        zDecisionVisibleOrNot();
                        break;
                    case 2:
                        zVisibleOrNot(btAnnulerDecisionERP, false);
                        break;
                    case 3:
                        zVisibleOrNot(btAnnulerDecisionERP, false);
                        break;
                }
            }
            else
            {
                btDecisionERP.width = decisionsize;
            }
            //BtExporterEtat(facture);
        }

        private function zVisibleOrNot(btn:Button, bool:Boolean):void
        {
            if (bool)
            {
                btn.width = decisionsize;
                btn.visible = true;
				btn.includeInLayout=true;
            }
            else
            {
                btn.width = maxsize;
                btn.visible = true;
				btn.includeInLayout=true;
            }
        }

        private function zDecisionVisibleOrNot():void
        {
            btDecisionERP.enabled = false;
            btDecisionERP.visible = true;
            btDecisionERP.width = decisionsize;
			btDecisionERP.includeInLayout=true;
            btAnnulerDecisionERP.enabled = false;
            btAnnulerDecisionERP.visible = false;
            btAnnulerDecisionERP.width = minsize;
			btAnnulerDecisionERP.includeInLayout=false;
        }

        private function BtExporterEtat(facture:Facture):void
        {
            if (facture != null)
            {
                if (facture.exportee == 0)
                {
                    btAnnulerDecisionERP.width = minsize;
                    btDecisionERP.width = decisionsize;
                }
                else if (facture.exportee == 2 || facture.exportee == 3)
                {
                    btAnnulerDecisionERP.width = maxsize;
                    btDecisionERP.width = minsize;
                }
            }
            else
            {
                btAnnulerDecisionERP.width = minsize;
                btDecisionERP.width = decisionsize;
            }
        }

        /*
         * Fonction qui permet d'affecter la période sur laquelle on souhaite travailler
         *
         * @param moisDebut : String La date de debut de la période
         * @param moisFin : String La date de fin de la période
         *
         * */  
        private function setSelectedPeriode(moisDebut:Date, moisFin:Date):void
        {				
			lblPeriode.text = 
				ResourceManager.getInstance().getString('M32', 'Factures__mises_entre_le__') 
				+ "01 "
				+ DateFunction.moisEnLettre(moisDebut.month)
				+ " "
				+ moisDebut.fullYear.toString()
				+ ResourceManager.getInstance().getString('M32', '_et_le_')
				+ moisFin.date.toString()
				+ " "
				+ DateFunction.moisEnLettre(moisFin.month)
				+ " "
				+ moisFin.fullYear.toString();
		}

        //quand les dates mini et maxi ont été ramenées par la procédure distante
        private function onSpecialDateSelectorReadyHandler(e:Event):void
        {
            initPeriode();
        }

        //Initialisation de la periode
        private function initPeriode():void
        {
            var periodeArray:Array = psPeriode.getTabPeriode();
            var len:int = periodeArray.length;
            var month:AMonth;
            month = periodeArray[len - 2];
            dateDebut = month.dateDebut;
            dateFin = month.dateFin;
            setSelectedPeriode(dateDebut, dateFin);
        }

        //actiavtion des boutons celon le profil réception par la procédure
        private function activerBouttons(bool:Boolean):void
        {
            if (bool && ((ctrlFactures.selectedFacture != null) && (ctrlFactures.selectedFacture.exportee != 1)) )
            {
                _configbtn.getEtatButtons((dgFactures.selectedItem as Facture).periodeId, _lgCtrl.LogControles_ProfileID);
                _configbtn.addEventListener("goToActiveBouttons", zActiveBouttons);
            }
            else
            {
                //btControler.enabled = false;
                btValiderControle.enabled = false;
                btViser.enabled = false;
                btAnnulViser.enabled = false;
                btViserAna.enabled = false;
                btEditVisaAnalytique.enabled = false;
                btAnnulVisaAnalytique.enabled = false;
                btValiderControle.enabled = false;
                btAnnulerControle.enabled = false;
				btValiderControleInv.enabled = false;
				btAnnulerControleInv.enabled = false;
                btDecisionERP.enabled = false;
                btAnnulerDecisionERP.enabled = false;
                if (dgFactures.selectedItem != null)
                {
                    _configbtn.getEtatButtons((dgFactures.selectedItem as Facture).periodeId, _lgCtrl.LogControles_ProfileID);
                    _configbtn.addEventListener("goToActiveBouttons", zActiveBouttons);
                }
            }
        }

        //***************************************************************************************************************************************//
        private function zActiveBouttons(evt:Event):void
        {
			_configbtn.removeEventListener("goToActiveBouttons", zActiveBouttons);
           	zActive();
        }

        private function zActive():void
        {
            if (ctrlFactures.selectedFacture.exportee != 1)
            {
				
				//btControler.enabled 			= ConfigButtons.btnValiderControle;
				
				btViser.enabled = ConfigButtons.btnViserFacturePapier;                
				btAnnulViser.enabled = ConfigButtons.btnAnnulerLeVisaPapier;
				
                btViserAna.enabled = ConfigButtons.btnViserAnalytique;                
				btEditVisaAnalytique.enabled = ConfigButtons.btnEditerAnalytique;
                btAnnulVisaAnalytique.enabled = ConfigButtons.btnAnnulerVisaAnalytique;
                
				
				btValiderControle.enabled = ConfigButtons.btnValiderControle;
                btAnnulerControle.enabled = ConfigButtons.btnAnnulerControle;
				
				
				btValiderControleInv.enabled = ConfigButtons.btnValiderControleInv;
				btAnnulerControleInv.enabled = ConfigButtons.btnAnnulerControleInv;
				
				
				 
				btDecisionERP.enabled = ConfigButtons.btnDecisionERP;
                btAnnulerDecisionERP.enabled = ConfigButtons.btnAnnulerDecisionERP;
								
            }
            
			if((ctrlFactures.selectedFacture.exportee == 1 || ctrlFactures.selectedFacture.exportee == 2 || ctrlFactures.selectedFacture.exportee == 3) && btEditVisaAnalytique.visible == true)
            {
                if (ConfigButtons.btnEditerAnalytique)
                {
                    btEditVisaAnalytique.enabled = true;
                }
            }
        }

///------------------------------------//context menu//-----------------------------------------//
        private function removeDefaultItems():void
        {
            myContextMenu.hideBuiltInItems();
            myContextMenu.builtInItems.print = false;
        }

        /*private function addCustomMenuItems():void
        {
            // Affichage par Catégorie de produits
            // var item:ContextMenuItem = new ContextMenuItem(ResourceManager.getInstance().getString('M32', 'Premi_re_page_de_la_facture'));
            // myContextMenu.customItems.push(item);
            //item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, displayFactureCat);
            // Affichage d'une facture
            //item = new ContextMenuItem(ResourceManager.getInstance().getString('M32', 'Afficher_la_Facture'));
            // myContextMenu.customItems.push(item);
            // item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, displayFacture);
            // Export d'une facture
            // item = new ContextMenuItem(ResourceManager.getInstance().getString('M32', 'Exporter_le_tableau_vers_Excel'));
            //myContextMenu.customItems.push(item);
            //item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, exportXLS);
            // Export de toutes les factures
        item = new ContextMenuItem("Exporter la Facture en PDF");
           myContextMenu.customItems.push(item);
         item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, exportFacturePDF);
        }*/

        /*private function modifyCustomMenuItems(e:Event):void
        {
          if (dgFactures.selectedIndex == -1) {
           e.currentTarget.customItems[0].enabled=false;
           e.currentTarget.customItems[1].enabled=false;
           } else {
           e.currentTarget.customItems[0].caption="Afficher la première page de la facture " + ctrlFactures.selectedFacture.numero;
           e.currentTarget.customItems[0].enabled=true;

           e.currentTarget.customItems[1].caption="Afficher la Facture " + ctrlFactures.selectedFacture.numero;
           e.currentTarget.customItems[1].enabled=true;
           }

           if (grdFacture.dataProvider != null && grdFacture.dataProvider.length==0){
           e.currentTarget.customItems[2].enabled=false;
           } else {
           e.currentTarget.customItems[2].enabled=true;
         } 
        }*/

        protected function itemMenuSelectCatClickHandler(cme:ContextMenuEvent):void
        {
            if (dgFactures.selectedIndex != -1)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                ctrlFactures.selectedFacture = facture;
                ctrlFactures.exporterLaFacture(AbstractControleFacture.PDF_FACTURECAT,0);
            }
        }

        //handler du click sur l'image imgFacture
        protected function itemMenuSelectFactureClickHandler(cme:ContextMenuEvent):void
        {
            if (dgFactures.selectedIndex != -1)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                ctrlFactures.selectedFacture = facture;
                ctrlFactures.exporterLaFacture(AbstractControleFacture.PDF_FACTURE,0);
            }
        }

        //handler du click sur l'image imgXLS
        protected function itemMenuSelectXLSClickHandler(cme:ContextMenuEvent):void
        {
            if (ctrlFactures.listeFactures.length > 0)
            {
                var facture:Facture = dgFactures.selectedItem as Facture;
                ctrlFactures.selectedFacture = facture;
                ctrlFactures.exporterLaFacture(AbstractControleFacture.XLS_LISTEFACTURE,0);
            }
        }

        public function demanderConfirmation(libelle:String, message:String, confirmFonction:Function):void
        {
            confirmBox = new ConfirmBoxIHM();
            confirmBox.facture = dgFactures.selectedItem as Facture;
            confirmBox.txRapport = message;
            confirmBox.libelle = libelle;
            confirmBox.doFunction = confirmFonction;
            PopUpManager.addPopUp(confirmBox, DisplayObject(parentApplication), true, PopUpManagerChildList.PARENT);
            PopUpManager.centerPopUp(confirmBox);
        }
    }
}
