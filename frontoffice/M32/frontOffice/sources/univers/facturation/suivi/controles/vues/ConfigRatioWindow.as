package univers.facturation.suivi.controles.vues
{
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.NumericStepper;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import univers.facturation.suivi.controles.controls.AbstractControleFacture;

	public class ConfigRatioWindow extends TitleWindow
	{
		public var btValider : Button;
		public var btRetour : Button;
		
		public var nsCumulRatio : NumericStepper;
		public var nsSeuil : NumericStepper;
		
		public function ConfigRatioWindow()
		{
			//TODO: implement function
			super();
		}
		
		private var _ctrlFact : AbstractControleFacture;		
		
		[Bindable]
		public function set controleFacture(cf : AbstractControleFacture):void{
			_ctrlFact = cf;
		}
		
		public function get controleFacture():AbstractControleFacture{
			return _ctrlFact;
		}
		
		protected function btValiderClickHandler(me : MouseEvent):void{
			controleFacture.seuil = nsSeuil.value;
			controleFacture.cumulRatio = nsCumulRatio.value;						
			PopUpManager.removePopUp(this);
		}
		
		protected function btRetourClickHandler(me : MouseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		protected function closeEventHandler(ce : CloseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		
		
		
		
	}
}