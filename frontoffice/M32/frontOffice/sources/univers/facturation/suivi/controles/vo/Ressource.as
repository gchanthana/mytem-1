package univers.facturation.suivi.controles.vo
{
	//[RemoteClass(alias="fr.consotel.consoview.facturation.suiviFacturation.controles.Ressource")]
	
	
	 
	[Bindable]
	public class Ressource
	{
		public var libelleProduit:String = "";
		public var produitClientId:Number = 0;
		public var sousTeteId:Number = 0;
		public var sousTete:String = "";
		public var compteFacturation:String = "";
		public var sousCompte:String = "";
		public var prixUnitaire:Number = 0;
		public var dateFin:Date = null;
		public var dateDebut:Date = null;
		public var dateEntree:Date = null;
		public var dateSortie:Date = null;
		public var typeTheme:String = "";
		public var quantite:Number = 0;
		public var montant:Number = 0;
		public var volume:Number = 0;


		public function Ressource()
		{
		}

	}
}