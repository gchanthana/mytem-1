package univers.facturation.suivi.controles.vo
{
	import mx.resources.ResourceManager;
	
	public class OptionAbonnement
	{
		public var IDINVENTAIRE_PRODUIT	:int 	= 0;		//-----------------/**/POUR AVOIR LES MEMES PROPRIETES QUE LES EQUIPEMENTS
		public var LIBELLE_PRODUIT		:String = "";		//nom de l'option
		public var IDPRODUIT_CATALOGUE	:int 	= 0;		//id de l'option
		public var TYPE_EQUIPEMENT		:String = "";		//-----------------/**/POUR AVOIR LES MEMES PROPRIETES QUE LES EQUIPEMENTS
		public var THEME_LIBELLE		:String = "";		//libelle du theme
		public var SUR_THEME			:String = "";		//libelle du sur  theme
		public var IDTHEME_PRODUIT		:int 	= 0;		//id du theme
		public var BOOL_ACCES			:int 	= 1;		//acces a cette option ou non
		public var BONUS				:int 	= 0;		//?
		public var FAVORI				:Boolean= false;	//appartient au favori ou non
		public var SELECTED				:Boolean= false;	//options selectionné ou non
		public var PRIX_UNIT			:String = "n.c";	//-----------------/**/POUR AVOIR LES MEMES PROPRIETES QUE LES EQUIPEMENTS (aucun prix)
		public var PRIX					:String = "";		//-----------------/**/POUR AVOIR LES MEMES PROPRIETES QUE LES EQUIPEMENTS (aucun prix)
		
		public var TYPE_THEME			:String = "";
		public var TYPE_ABO				:String = "";
		public var SEGMENT_THEME		:String = "";
		
		public var BOOL_ERASE 			:Boolean= false;	//?
		
		public var BOOL_IN_FACTURE 		:int 	= 0;		//?
		public var BOOL_IN_INVENTAIRE 	:int 	= 0;		//?
		public var SOUS_TETE 			:String	= "";		//?
		public var IDSOUS_TETE 			:int 	= 0;		//?
		
		public var IDRESSOURCE	 		:int 	= 0;		//?

		
		public var TYPE_ACTION			:String = "Ajout";		//-----------------/**/POUR AVOIR LES MEMES PROPRIETES QUE LES EQUIPEMENTS (aucun prix)
		public var AJOUTER				:int 	= 1;	//-----------------/**/POUR AVOIR LES MEMES PROPRIETES QUE LES EQUIPEMENTS (aucun prix)

		public function OptionAbonnement()
		{
		}

	}
}