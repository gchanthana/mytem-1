package univers.facturation.suivi.controles.services
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import univers.facturation.suivi.controles.event.InventaireEvent;
	import univers.facturation.suivi.controles.vo.ProduitVO;
	import univers.facturation.suivi.controles.vo.TotalVo;
	
	[Bindable]
	public class InventaireModel extends EventDispatcher
	{
		public var factures:ArrayCollection = new ArrayCollection();
		public var inventaireNonFacture:ArrayCollection = new ArrayCollection();
		public var inventaireFacture:ArrayCollection = new ArrayCollection();
		
		public var totalFacture:Number=0;
		public var totalInventaireFacture:Number=0;
		public var totalInventaireNonFacture:Number=0;
		
		public var libelleCompte:String=ResourceManager.getInstance().getString('M32', 'Compte_de_facturation');
		public var libelleSousCompte:String=ResourceManager.getInstance().getString('M32', 'Sous_Compte');
		public var libelleDernivOpe:String='-';
		
		public function InventaireModel()
		{
		}
		
		public function updateInventaireFacture(values:ArrayCollection):void
		{			
			factures.source = new Array();
			var produit:ProduitVO;
			for(var i:int=0;i<values.length;i++)
			{
				produit = new ProduitVO();
				produit.addProduit(values[i]);
				factures.addItem(produit);
			}
			totalFacture=values.length;
			dispatchEvent(new InventaireEvent(InventaireEvent.INVENTAIRE_FACTURE_COMPELTE));
		}
		
		public function updateInventaireNonFacture(values:ArrayCollection):void
		{	
			inventaireFacture.filterFunction = null;
			inventaireFacture.refresh();
			inventaireFacture.removeAll();
			
			inventaireNonFacture.filterFunction = null;
			inventaireNonFacture.refresh();
			inventaireNonFacture.removeAll();
			
			var produit:ProduitVO;
			for(var i:int=0;i<values.length;i++){
				produit = new ProduitVO();
				produit.addProduit(values[i]);
				if((produit.cfCommande!="")&&(produit.compte!=""))
				{
					inventaireFacture.addItem(produit);
				}
				else{
					inventaireNonFacture.addItem(produit);
				}				
			}
			totalInventaireFacture=inventaireNonFacture.length;
			totalInventaireNonFacture=inventaireFacture.length;
			dispatchEvent(new InventaireEvent(InventaireEvent.INVENTAIRE_NON_FACTURE_COMPELTE));
		}
		
		public function updategetLibelleCompte(values:Object):void
		{
			if (values[0] != null && values[0].length > 0)
			{
				if(values[0][0].COMPTE != null){libelleCompte=values[0][0].COMPTE;}
				if(values[0][0].SOUS_COMPTE != null){libelleSousCompte=values[0][0].SOUS_COMPTE;}
			}
			if (values[1] != null && values[1].length > 0)
			{
				if(values[1][0].TYPE_NIVEAU != null){libelleDernivOpe=values[1][0].TYPE_NIVEAU;}
			}
		}
		
	}
}