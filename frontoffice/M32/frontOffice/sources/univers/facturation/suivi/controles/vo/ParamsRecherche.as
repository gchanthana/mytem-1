package univers.facturation.suivi.controles.vo
{
	
	[Bindable]
	public class ParamsRecherche
	{
		public var dateDebut:Date = null;
		public var dateFin:Date = null;
		public var chaine:String = "";
		public var mode:Number = 0;
		public var operateurId:Number = 0;
		public var themeId:Number = 0;
		public var etatVise:Number = 1;
		public var etatViseAna:Number = 1;
		public var etatControle:Number = 1;
		public var etatInventaire:Number = 1;
		public var etatExporte:Number = 1;
		public var societeId:Number = 0;
		public var catalogueClient:Number = 1;
		public var operateurNom:String="";


		public function ParamsRecherche()
		{
		}

	}
}