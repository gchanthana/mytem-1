package univers.facturation.suivi.controles.services
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	[Bindable]
	public class ActeGestionService
	{
		
		public var model:ActeGestionModel;
		public var handler:ActeGestionHandler;
		
		public function ActeGestionService()
		{
			model = new ActeGestionModel();
			handler = new ActeGestionHandler(model);
		}
		
		public function getActeGestion(idProduit:Number):void
		{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.GetActeGestion",
				"getActeGestion",
				handler.getActeGestionHandler);
			RemoteObjectUtil.callService(opData,idProduit);
			
		}
		
	}
}