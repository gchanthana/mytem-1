package univers.facturation.suivi.controles.vues
{
	import composants.util.DateFunction;
	
	import mx.containers.VBox;
	import mx.controls.dataGridClasses.DataGridColumn;
	
	import univers.facturation.suivi.controles.vo.CommandeVO;

	public class CommandeHistorique extends VBox
	{
		[Bindable]
		public var commande:CommandeVO = new CommandeVO();
		
		public function CommandeHistorique()
		{
		}
		
		protected function formatDate(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{
				
				return DateFunction.formatDateAsString(item[column.dataField]);
			}
			else
				return "-";
			
		}
		
	}
}