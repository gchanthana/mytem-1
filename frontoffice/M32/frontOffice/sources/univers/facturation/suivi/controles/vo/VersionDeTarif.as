package univers.facturation.suivi.controles.vo
{
	//[RemoteClass(alias="fr.consotel.consoview.facturation.suiviFacturation.controles.VersionDeTarif")]
	 
	[Bindable]
	public class VersionDeTarif
	{

		public var idProduit:Number = 0;
		public var libelleProduit:String = "";
		public var themeId:Number = 0;
		public var themeLibelle:String = "";
		public var operateurId:Number = 0;
		public var operateurLibelle:String = "";
		public var typeTheme:String = "";
		public var typeTrafic:String = "";
		public var typeTraficId:String = "";
		public var poid:Number = 0;
		public var pourControle:Boolean = false;
		public var idVersionTarif:Number = 0;
		public var prixUnitaire:Number = 0;
		public var prixRemise:Number = 0;
		public var remiseContrat:Number = 0;
		public var dateDebut:Date = new Date();
		public var dateFin:Date = null;
		public var deviseLibelle:String = "";
		public var deviseId:Number = 0;
		public var frequenceFactureLibelle:String = "";
		public var frequenceFactureLibelleId:Number = 0;
		public var qteMini:Number = 0;


		public function VersionDeTarif()
		{
		}

	}
}