package univers.facturation.suivi.controles.vo
{
	//[RemoteClass(alias="fr.consotel.consoview.facturation.suiviFacturation.controles.DetailFacture")]
	
	 
	[Bindable]
	public class DetailFacture
	{

		public var compteFacturation:String = "";
		public var sousCompte:String = "";
		public var libelleProduit:String = "";
		public var sousTete:String = "";
		public var montant:Number = 0;
		public var nombreAppel:Number = 0;
		public var dureeAppel:Number = 0;
		public var quantite:Number = 0;
		public var libelle:Date = null;
		public var gamme:String = "";
		public var prixUnitaire:Number = 0;
		public var dateDebut:Date = null;
		public var dateFin:Date = null;
		public var dateEmission:Date = null;
		public var numeroFacture:String = "";
		public var nomSite:String = "";
		public var adresse1:String = "";
		public var adresse2:String = "";
		public var zipCode:String = "";
		public var commune:String = "";
		public var typeLigne:String = "";
		public var codeTva:Number = 0;


		public function DetailFacture()
		{
		}

	}
}