package univers.facturation.suivi.controles.services
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	public class HistoriqueActionRessourceHandler
	{
		
		private var _model:HistoriqueActionRessourceModel;
		
		public function HistoriqueActionRessourceHandler(model:HistoriqueActionRessourceModel):void
		{
			_model = model;
		}
		
		public function getHistoriqueActionRessourceHandler(event:ResultEvent):void
		{
			_model.updateHistoriqueActionRessource(event.result as ArrayCollection);
		}
		
	}
}