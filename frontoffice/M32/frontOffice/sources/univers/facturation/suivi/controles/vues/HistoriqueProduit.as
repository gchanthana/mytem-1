package univers.facturation.suivi.controles.vues
{
	import composants.util.DateFunction;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;

	public class HistoriqueProduit extends TitleWindow
	{
		
		[Bindable]
		public var dataProduit:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		public var ligne:String = "";
		
		[Bindable]
		public var produit:String = "";
		
		public function HistoriqueProduit()
		{
		}
		
		protected function formatDate(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{
				
				return DateFunction.formatDateAsString(item[column.dataField]);
			}
			else
				return "-";
			
		}
		
	}
}