package univers.facturation.suivi.controles.event
{
	import flash.events.Event;
	
	public class FicheLigneEvent extends Event
	{
		public static var FICHELIGNE_COMPELTE:String = "FicheLigneComplete"; 
		public static var LIBELLEPERSO_COMPELTE:String = "LibellePersoComplete"; 
		
		public function FicheLigneEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new FicheLigneEvent(type,bubbles,cancelable);
		}
	}
}