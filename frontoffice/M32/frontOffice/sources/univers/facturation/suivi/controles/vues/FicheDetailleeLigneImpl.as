package univers.facturation.suivi.controles.vues
{
	import composants.popuputils.ConfirmerOrAnnulerMsgIHM;
	import composants.popuputils.ConfirmerOrAnnulerMsgImpl;
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparcmobile.entity.LigneVO;
	import gestionparcmobile.entity.OperateurVO;
	import gestionparcmobile.entity.OrgaVO;
	import gestionparcmobile.entity.RevendeurVO;
	import gestionparcmobile.entity.SIMVO;
	import gestionparcmobile.event.CheckBoxChangeEvent;
	import gestionparcmobile.event.ContentFicheEvent;
	import gestionparcmobile.event.FicheSiteEvent;
	import gestionparcmobile.event.GestionParcMobileEvent;
	import gestionparcmobile.ihm.fiches.contentTabNav.ContentContratOperateurLigneIHM_V2;
	import gestionparcmobile.ihm.fiches.contentTabNav.ContentDiversIHM;
	import gestionparcmobile.ihm.fiches.contentTabNav.ContentTabNavSiteIHM;
	import gestionparcmobile.ihm.fiches.contentTabNav.TabModelIHM;
	import gestionparcmobile.ihm.fiches.popup.PopUpAboOptIHM;
	import gestionparcmobile.ihm.fiches.popup.PopUpSimIHM;
	import gestionparcmobile.ihm.fiches.popup.PopUpUsageIHM;
	import gestionparcmobile.services.cache.CacheDatas;
	import gestionparcmobile.services.cache.CacheServices;
	import gestionparcmobile.services.ligne.LigneServices;
	import gestionparcmobile.services.organisation.OrganisationServices;
	import gestionparcmobile.utils.GestionParcMobileConstantes;
	import gestionparcmobile.utils.GestionParcMobileMessages;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Form;
	import mx.containers.FormItem;
	import mx.containers.TabNavigator;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.CloseEvent;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	import mx.validators.Validator;
	
	import univers.facturation.suivi.controles.fiches.contentTabNav.ContentTabNavSiteIHM;
	
	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de la fiche ligne.
	 * 
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 1.09.2010
	 * 
	 * </pre></p>
	 * 
	 */
	[Bindable]
	public class FicheDetailleeLigneImpl extends TitleWindow
	{
		// VARIABLES------------------------------------------------------------------------------
		
		// IHM components
		public var tiSousTete				:TextInput;
		public var tiNumContrat				:TextInput;
		
		public var lbPosition				:Label;
		public var lbAboPrincipal			:Label;
		public var lbTypeLigne				:Label;
		public var lbOperateur				:Label;
		public var lbCompte					:Label;
		public var lbSousCompte				:Label;
		public var lbNomPrenomCollab		:Label;
		public var lbMatriculeCollab		:Label;
		public var lbNumSim					:Label;
		
		public var taCommentaires			:TextArea;
		
		public var valSousTete				:Validator;
		public var valNumContrat			:Validator;
		public var valTypeLigne				:Validator;
		public var valOperateur				:Validator;
		public var valCompte				:Validator;
		public var valSousCompte			:Validator;
		public var valTypeCmd				:Validator;
		public var valDistributeur			:Validator;
		
		public var cbEtat					:ComboBox;
		public var cbFonction				:ComboBox;
		public var cbTitulaire				:ComboBox;
		public var cbPayeur					:ComboBox;
		public var cbOrganisation			:ComboBox;
		public var cbTypeLigne				:ComboBox;
		public var cbOperateur				:ComboBox;
		public var cbCompte					:ComboBox;
		public var cbSousCompte				:ComboBox;
		public var cbTypeCmd				:ComboBox;
		public var cbDistributeur			:ComboBox;
		
		public var tnLigne					:TabNavigator;
		public var tncSite					:ContentTabNavSiteIHM;
		public var tncDivers				:ContentDiversIHM;		
		public var tncAboOption				:TabModelIHM;
		public var tncContratOperateur		:ContentContratOperateurLigneIHM_V2;	
		
		public var foCollab					:Form;
		public var fiTypeLigne				:FormItem;
		public var fiOperateur				:FormItem;
		public var fiTypeCmd				:FormItem;
		public var fiDistributeur			:FormItem;
		
		public var btValid					:Button;
		public var btCancel					:Button;
		public var btModifierOrga			:Button;
		public var btAddSim					:Button;
		
		public var imgEditFonction			:Image;
		public var imgEditSim				:Image;
		public var imgDelSim				:Image;
		
		// publics variables 
		public var idSousTete				:int = 0;
		public var idGpeClient				:int = 0;
		public var isNewLigneFiche			:Boolean = false;
		
		public var matriculeCollab			:String = "";
		public var nomPrenomCollab			:String = "";
		public var myServiceCache			:CacheServices = null;
		
		// privates variables
		private var myServiceLigne			:LigneServices = null;
		private var myServiceOrga 			:OrganisationServices = null;
		
		private var ficheOrga				:FicheOrgaIHM = null;
		private var myDataProviderAboOpt	:ArrayCollection = null;
		
		private var indexTypeCmd			:int = 0;
		private var indexOperateur			:int = 0;
		
		private var strOldUsage				:String = "";
		
		private var pin1					:String = "";
		private var pin2					:String = "";
		private var puk1					:String = "";
		private var puk2					:String = "";
		
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe FicheDetailleeLigneImpl_V2.
		 * </pre></p>
		 */	
		public function FicheDetailleeLigneImpl_V2()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		// PRIVATE FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation générale de cette classe  :
		 * - Instanciation du service lié aux lignes.
		 * - Instanciation du service lié aux lignes si ce n'est pas un ajout de lignes.
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see #initListener()
		 * @see #initDisplay()
		 * 
		 */
		private function init(event:FlexEvent):void
		{
			myServiceLigne = new LigneServices();
			
			if(!isNewLigneFiche)
			{
				var objToServiceGet : Object = new Object();				
				objToServiceGet.idSousTete = idSousTete;
				objToServiceGet.idGpeClient = idGpeClient;
				
				myServiceLigne.getInfosFicheLigne(objToServiceGet);	
			}
			else
			{
				myServiceOrga = new OrganisationServices();
				
				if(CacheDatas.listeOrganisation == null)
				{
					myServiceOrga.getListOrgaCliente();
				}
				else
				{
					listeOragLoadedHandler(null);
				}
				
				if(CacheDatas.listeProfilEqpt == null)
				{
					myServiceCache.getListProfilEquipements();
				}
				else
				{
					cacheListeProfilEqptLoadedHandler(null);
				}
				
				if(CacheDatas.listeOperateur == null)
				{
					myServiceCache.getListeOperateurs();	
				}
				else
				{
					cacheListeOperateurLoadedHandler(null);
				}
				
				if(CacheDatas.listeTypesLigne == null)
				{
					myServiceCache.getTypeLigne();	
				}
				else
				{
					cacheListeTypeLigneLoadedHandler(null);
				}
			}
			
			initListener();
			initDisplay();
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise les écouteurs.
		 * </pre></p>
		 * 
		 * @return void
		 * 
		 */	
		private function initListener():void
		{
			// listeners lié aux boutons de FicheDetailleeLigneImpl_V2
			this.addEventListener(CloseEvent.CLOSE,closePopup);
			
			btCancel.addEventListener(MouseEvent.CLICK,cancelPopup);
			btValid.addEventListener(MouseEvent.CLICK,validClickHandler);
			
			imgEditFonction.addEventListener(MouseEvent.CLICK,imgEditFonctionClickHandler);
			
			// listeners permettant l'activation du bouton "valider"
			tiNumContrat.addEventListener(Event.CHANGE,activeValidButton);
			tiSousTete.addEventListener(Event.CHANGE,activeValidButton);
			
			taCommentaires.addEventListener(Event.CHANGE,activeValidButton);
			
			cbEtat.addEventListener(ListEvent.CHANGE,activeValidButton);
			cbFonction.addEventListener(ListEvent.CHANGE,activeValidButton);
			cbOrganisation.addEventListener(ListEvent.CHANGE,activeValidButton);
			cbPayeur.addEventListener(ListEvent.CHANGE,activeValidButton);
			cbTitulaire.addEventListener(ListEvent.CHANGE,activeValidButton);
			
			// listeners liés au tabNavigator
			tncSite.cb_site.addEventListener(ListEvent.CHANGE,activeValidButton);
			tncDivers.ti_chp1.addEventListener(Event.CHANGE,activeValidButton);
			tncDivers.ti_chp2.addEventListener(Event.CHANGE,activeValidButton);
			tncDivers.ti_chp3.addEventListener(Event.CHANGE,activeValidButton);
			tncDivers.ti_chp4.addEventListener(Event.CHANGE,activeValidButton);
			tncAboOption.dataDatagrid.addEventListener(CollectionEvent.COLLECTION_CHANGE,activeValidButton);
			tncContratOperateur.cbDureeContrat.addEventListener(ListEvent.CHANGE,activeValidButton);
			tncContratOperateur.cbDureeEligibilite.addEventListener(ListEvent.CHANGE,activeValidButton);
			tncContratOperateur.dcDateOuverture.addEventListener(CalendarLayoutChangeEvent.CHANGE,activeValidButton);
			tncContratOperateur.dcDateRenouvellement.addEventListener(CalendarLayoutChangeEvent.CHANGE,activeValidButton);
			tncContratOperateur.dcDateResiliation.addEventListener(CalendarLayoutChangeEvent.CHANGE,activeValidButton);
			this.addEventListener(ContentFicheEvent.AFFICHER_HISTORIQUE,displayHistoriqueHandler);
			
			// listeners liés à l'organisation
			cbOrganisation.addEventListener(ListEvent.CHANGE,changeOrgaHandler);
			btModifierOrga.addEventListener(MouseEvent.CLICK,clickModifierOrgaHandler);
			
			// listeners liés à l'ajout d'une nouvelle ligne
			if(isNewLigneFiche)
			{
				// listeners lié aux boutons de FicheDetailleeLigneImpl_V2
				btAddSim.addEventListener(MouseEvent.CLICK,btAddSimClickHandler);
				
				cbTypeCmd.addEventListener(ListEvent.CHANGE,cbTypeCmdListEventChange);
				cbOperateur.addEventListener(ListEvent.CHANGE,cbOperateurListEventChange);
				cbCompte.addEventListener(Event.CHANGE,cbCompteChangeHandler);
				
				imgEditSim.addEventListener(MouseEvent.CLICK,imgEditSimClickHandler);
				imgDelSim.addEventListener(MouseEvent.CLICK,imgDelSimClickHandler);
				
				// listeners liés au tabNavigator
				tncAboOption.addEventListener(ContentFicheEvent.AJOUTER_ELT_NAV, tncAboOptionContentFicheEventAjouterEltNavHandler);
				
				// listeners liés aux services
				myServiceOrga.myDatas.addEventListener(GestionParcMobileEvent.LISTE_ORGA_LOADED,listeOragLoadedHandler);
				myServiceCache.myDatas.addEventListener(GestionParcMobileEvent.LISTE_PROFIL_EQPT_LOADED,cacheListeProfilEqptLoadedHandler);
				myServiceCache.myDatas.addEventListener(GestionParcMobileEvent.LISTE_OPERATEUR_LOADED,cacheListeOperateurLoadedHandler);
				myServiceCache.myDatas.addEventListener(GestionParcMobileEvent.LISTE_TYPES_LIGNE_LOADED,cacheListeTypeLigneLoadedHandler);
				myServiceCache.myDatas.addEventListener(GestionParcMobileEvent.LISTE_REVENDEUR_PROF_EQPT_LOADED,cacheListeRevendeurProfilEqptLoadedHandler);
				myServiceCache.myDatas.addEventListener(GestionParcMobileEvent.LISTE_COMPTES_SOUSCOMPTES_LOADED,cacheListeComptesSousComptesLoadedHandler);
				myServiceCache.myDatas.addEventListener(GestionParcMobileEvent.INFO_OP_LOADED,cacheInfoOpLoadedHandler);
			}
			else
			{
				myServiceLigne.myDatas.addEventListener(GestionParcMobileEvent.INFOS_FICHE_LIGNE_LOADED,infoFicheLigneLoadedHandler);
				tncAboOption.addEventListener(CheckBoxChangeEvent.CHBX_SELECTION_CHANGE,activeValidButton);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise l'affichage de cette IHM.
		 * </pre></p>
		 * 
		 * @return void
		 * 
		 */	
		private function initDisplay():void
		{
			// tant que tous les champs requit ne sont pas renseignés ou qu'aucune modif n'a été réalisée
			btValid.enabled = false;
			
			tncSite.cb_site.dataProvider = CacheDatas.siteLivraison;
			cbFonction.dataProvider		 = CacheDatas.listeUsages;
			
			lbNomPrenomCollab.text		 = (nomPrenomCollab)?nomPrenomCollab:"-";
			lbMatriculeCollab.text		 = (matriculeCollab)?matriculeCollab:"-";
			
			if(isNewLigneFiche)
			{
				this.height += 100;
				lbOperateur.visible = false;
				lbOperateur.includeInLayout = false;
				lbTypeLigne.visible = false;
				lbTypeLigne.includeInLayout = false;
				lbCompte.visible = false;
				lbCompte.includeInLayout = false;
				lbSousCompte.visible = false;
				lbSousCompte.includeInLayout = false;
				foCollab.visible = false;
				foCollab.includeInLayout = false;
				lbNumSim.visible = false;
				lbNumSim.includeInLayout = false;
				imgEditSim.visible = false;
				imgEditSim.includeInLayout = false;
				imgDelSim.visible = false;
				imgDelSim.includeInLayout = false;
				fiOperateur.required = true;
				fiTypeLigne.required = true;
				tncAboOption.buttonActionVisible = true;
				tncAboOption.buttonActionLabel = ResourceManager.getInstance().getString('M11','Abo_et_options');
				tncDivers.fi1.label	 = CacheDatas.libellesPersoFicheLigne1;
				tncDivers.fi2.label	 = CacheDatas.libellesPersoFicheLigne2;
				tncDivers.fi3.label	 = CacheDatas.libellesPersoFicheLigne3;
				tncDivers.fi4.label	 = CacheDatas.libellesPersoFicheLigne4;
				tncSite.enabled = false;
			}
			else
			{
				cbOperateur.visible = false;
				cbOperateur.includeInLayout = false;
				cbTypeLigne.visible = false;
				cbTypeLigne.includeInLayout = false;
				cbCompte.visible = false;
				cbCompte.includeInLayout = false;
				cbSousCompte.visible = false;
				cbSousCompte.includeInLayout = false;
				if(idGpeClient != 0) // ligne associée à un collab
				{
					btModifierOrga.visible = false;
					btModifierOrga.includeInLayout = false;
				}
				else // ligne pas associée à un collab
				{
					foCollab.visible = false;
					foCollab.includeInLayout = false;
				}
				btAddSim.visible = false;
				btAddSim.includeInLayout = false;
				imgEditSim.visible = false;
				imgEditSim.includeInLayout = false;
				imgDelSim.visible = false;
				imgDelSim.includeInLayout = false;
				fiOperateur.required = false;
				fiTypeLigne.required = false;
				fiTypeCmd.visible = false;
				fiTypeCmd.includeInLayout = false;
				fiDistributeur.visible = false;
				fiDistributeur.includeInLayout = false;
				tncAboOption.addColonnes("ligne");
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Charge les données qui viennent d'être collectées de la base dans l'IHM.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see gestionparcmobile.event
		 * 
		 */
		private function infoFicheLigneLoadedHandler(event:GestionParcMobileEvent):void
		{
			trace("données fiche ligne chargées");
			
			// maj des infos concernant la ligne
			tiSousTete.text				 = myServiceLigne.myDatas.ligne.sousTete;
			tiNumContrat.text			 = myServiceLigne.myDatas.ligne.numContrat;
			lbAboPrincipal.text			 = myServiceLigne.myDatas.ligne.aboPrincipale;
			lbTypeLigne.text			 = myServiceLigne.myDatas.ligne.typeLigne;
			lbOperateur.text			 = myServiceLigne.myDatas.ligne.operateur.nom;
			lbCompte.text				 = myServiceLigne.myDatas.ligne.compte;
			lbSousCompte.text			 = myServiceLigne.myDatas.ligne.sousCompte;
			taCommentaires.text			 = myServiceLigne.myDatas.ligne.commentaires;
			lbNumSim.text				 = myServiceLigne.myDatas.ligne.simRattache.libelleEquipement;
			cbTitulaire.selectedIndex 	 = (myServiceLigne.myDatas.ligne.titulaire)?1:0;
			cbPayeur.selectedIndex 		 = (myServiceLigne.myDatas.ligne.payeur)?1:0;
			
			var lenListeUsage : int = CacheDatas.listeUsages.length;
			for(var j : int = 0 ; j < lenListeUsage ; j++)
			{
				if(CacheDatas.listeUsages[j].USAGE == myServiceLigne.myDatas.ligne.fonction)
				{
					cbFonction.selectedIndex = j;
					break;
				}
			}
			
			switch(myServiceLigne.myDatas.ligne.etat)
			{
				case 1 :
				{
					cbEtat.selectedIndex = 0;
					break;
				}
				case 2 :
				{
					cbEtat.selectedIndex = 1;
					break;
				}
				case 3 :
				{
					cbEtat.selectedIndex = 2;
					break;
				}
			}
			
			// maj orga
			var lenListOrga : int = myServiceLigne.myDatas.ligne.listeOrganisation.length;
			var myPattern : RegExp = /\//g;
			
			// boucle pour remplacer les "/" des anciens chemins par des ";"
			for(var k : int = 0 ; k < lenListOrga ; k++)
			{
				if((myServiceLigne.myDatas.ligne.listeOrganisation[k] as OrgaVO).chemin)
				{
					(myServiceLigne.myDatas.ligne.listeOrganisation[k] as OrgaVO).chemin = (myServiceLigne.myDatas.ligne.listeOrganisation[k] as OrgaVO).chemin.replace(myPattern,";");
				}
			}
			cbOrganisation.dataProvider = myServiceLigne.myDatas.ligne.listeOrganisation;
			
			// selectionne le dernier chemin modifié
			var dateLastModifPosition : Date = new Date(0);
			var lenListeOrga : int = myServiceLigne.myDatas.ligne.listeOrganisation.length;
			
			for(var i : int = 0; i < lenListeOrga; i++)
			{
				if((myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).position == 1 && (myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).dateModifPosition != null)
				{
					if(ObjectUtil.dateCompare((myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).dateModifPosition,dateLastModifPosition) == 1)
					{
						lbPosition.text = (myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).chemin;
						cbOrganisation.selectedIndex = i;
						dateLastModifPosition = (myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).dateModifPosition;
					}
				}
			}
			
			// maj onglet SITE 
			if(myServiceLigne.myDatas.ligne.idSite > 0)
			{
				var longueurTabSite : int = CacheDatas.siteLivraison.length;
				for(var index : int = 0 ; index < longueurTabSite ; index++)
				{
					if(CacheDatas.siteLivraison[index].IDSITE_PHYSIQUE == myServiceLigne.myDatas.ligne.idSite)
					{
						tncSite.cb_site.selectedIndex = index;
						tncSite.adresse = CacheDatas.siteLivraison[index].SP_ADRESSE1;
						tncSite.codePostal = CacheDatas.siteLivraison[index].SP_CODE_POSTAL;
						tncSite.ville = CacheDatas.siteLivraison[index].SP_COMMUNE;
						break;
					}
				}
			}
			else
			{
				tncSite.cb_site.selectedIndex = -1;
			}
			
			// pas de site possible si pas de sim
			if(!lbNumSim.text)
			{
				tncSite.enabled = false;	
			}
			else
			{
				tncSite.enabled = true;
			}
			
			// maj onglet Abo et options
			tncAboOption.dataDatagrid = myServiceLigne.myDatas.ligne.listeAboOption;
			
			// maj onglet contrat opérateur
			tncContratOperateur.lbOperateur.text 			= myServiceLigne.myDatas.ligne.operateur.nom;
			
			switch(myServiceLigne.myDatas.ligne.dureeContrat)
			{
				case 12 :
				{
					tncContratOperateur.cbDureeContrat.selectedIndex = 0 ;
					break;
				}
				case 18 :
				{
					tncContratOperateur.cbDureeContrat.selectedIndex = 1 ;
					break;
				}
				case 24 :
				{
					tncContratOperateur.cbDureeContrat.selectedIndex = 2 ;
					break;
				}
				case 36 :
				{
					tncContratOperateur.cbDureeContrat.selectedIndex = 3 ;
					break;
				}
				case 48 :
				{
					tncContratOperateur.cbDureeContrat.selectedIndex = 4 ;	
					break;
				}
			}
			
			tncContratOperateur.dcDateOuverture.selectedDate 		= myServiceLigne.myDatas.ligne.dateOuverture;
			tncContratOperateur.dcDateRenouvellement.selectedDate	= myServiceLigne.myDatas.ligne.dateRenouvellement;
			tncContratOperateur.dcDateResiliation.selectedDate		= myServiceLigne.myDatas.ligne.dateResiliation;
			tncContratOperateur.cbDureeEligibilite.selectedIndex	= myServiceLigne.myDatas.ligne.dureeEligibilite - 1;
			tncContratOperateur.lbDateEligibilite.text				= DateFunction.formatDateAsString(myServiceLigne.myDatas.ligne.dateEligibilite);
			tncContratOperateur.lbDateFpc.text						= DateFunction.formatDateAsString(myServiceLigne.myDatas.ligne.dateFPC);
			tncContratOperateur.lbDateLastFacture.text	 			= DateFunction.formatDateAsString(myServiceLigne.myDatas.ligne.dateDerniereFacture);
			
			// maj onglet DIVERS
			tncDivers.fi1.label	 = myServiceLigne.myDatas.ligne.libelleChampPerso1;
			tncDivers.fi2.label	 = myServiceLigne.myDatas.ligne.libelleChampPerso2;
			tncDivers.fi3.label	 = myServiceLigne.myDatas.ligne.libelleChampPerso3;
			tncDivers.fi4.label	 = myServiceLigne.myDatas.ligne.libelleChampPerso4;
			tncDivers.champ1	 = myServiceLigne.myDatas.ligne.texteChampPerso1;
			tncDivers.champ2	 = myServiceLigne.myDatas.ligne.texteChampPerso2;
			tncDivers.champ3	 = myServiceLigne.myDatas.ligne.texteChampPerso3;
			tncDivers.champ4	 = myServiceLigne.myDatas.ligne.texteChampPerso4;
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Charge les données qui viennent d'être collectées de la base dans l'IHM.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */
		private function tncAboOptionContentFicheEventAjouterEltNavHandler(event:ContentFicheEvent):void
		{
			if(cbOperateur.selectedItem && cbOperateur.selectedItem != -1 && cbOperateur.selectedItem.OPERATEURID)
			{
				var popupAboOpt : PopUpAboOptIHM = new PopUpAboOptIHM();
				popupAboOpt.addEventListener(GestionParcMobileEvent.POPUP_ABO_OPT_VALIDATED, updateGridAboOpt);
				popupAboOpt.myServices = myServiceLigne;
				popupAboOpt.idOperateur = cbOperateur.selectedItem.OPERATEURID;
				popupAboOpt.idProfilEquipement = cbTypeCmd.selectedItem.IDPROFIL_EQUIPEMENT;
				
				if(tncAboOption.dataDatagrid.length > 0)
				{
					popupAboOpt.isAbonnement = true;
				}
				
				PopUpManager.addPopUp(popupAboOpt, this, true);
				PopUpManager.centerPopUp(popupAboOpt);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Met à jour le tableau des abonnements et options selon le choix effectué par l'utilisateur dans la popup PopUpAboOptIHM.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see gestionparcmobile.event
		 * 
		 */
		private function updateGridAboOpt(event:GestionParcMobileEvent):void
		{
			myDataProviderAboOpt = new ArrayCollection();
			
			// pour l'abonnement
			for each(var obj:* in myServiceLigne.myDatas.listeAbo)
			{
				if(obj.SELECTED)
				{
					// creation d'un attribut "libelleProduit" pour l'affichage dans le tableau
					obj.libelleProduit = obj.LIBELLE_PRODUIT;
					// creation d'un attribut "operateur" pour l'affichage dans le tableau 
					// cet attribut est inutil pour les lignes fixe car toujours la meme valeur mais cela sera different pour les lignes fixes
					obj.operateur = cbOperateur.selectedItem.LIBELLE;
					myDataProviderAboOpt.addItem(obj);
					lbAboPrincipal.text = obj.LIBELLE_PRODUIT;
				}
			}
			// pour les options
			for each(var obj2:* in myServiceLigne.myDatas.listeOpt)
			{
				if(obj2.SELECTED)
				{
					// creation d'un attribut "libelleProduit" pour l'affichage dans le tableau
					obj2.libelleProduit = obj2.LIBELLE_PRODUIT;
					// creation d'un attribut "operateur" pour l'affichage dans le tableau 
					// cet attribut est inutil pour les lignes fixe car toujours la meme valeur mais cela sera different pour les lignes fixes
					obj2.operateur = cbOperateur.selectedItem.LIBELLE;
					myDataProviderAboOpt.addItem(obj2);
				}
			}
			
			tncAboOption.dataDatagrid = myDataProviderAboOpt;
			tncAboOption.dataDatagrid.refresh();
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Charge les données concernat les orga qui viennent d'être collectées de la base dans l'IHM.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see gestionparcmobile.event
		 * 
		 */
		private function listeOragLoadedHandler(event:GestionParcMobileEvent):void
		{
			myServiceLigne.myDatas.ligne = new LigneVO();
			myServiceLigne.myDatas.ligne.listeOrganisation = CacheDatas.listeOrganisation;
			myServiceOrga.myDatas.listeOrga = CacheDatas.listeOrganisation; 
			
			cbOrganisation.dataProvider = myServiceLigne.myDatas.ligne.listeOrganisation;
		}
		
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see #closePopup()
		 * 
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ferme la popup courante.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Valide la fiche ligne. 
		 * Si c'est une nouvelle ligne alors il en résulte sa création.
		 * Si la ligne existait auparavant, alors il en résulte de sa mise à jour. 
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */			
		private function validClickHandler(event:MouseEvent):void
		{
			// test les validators
			if(runValidators())
			{
				myServiceLigne.setInfosFicheLigne(createObjToServiceSet());
				myServiceLigne.myDatas.addEventListener(GestionParcMobileEvent.INFOS_FICHE_LIGNE_UPLOADED,ficheLigneUploadHandler);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Créé l'objet non typé que l'on va passer au service
		 * </pre></p>
		 * 
		 * @return Object
		 * 
		 */			
		private function createObjToServiceSet():Object
		{
			var obj : Object = new Object();
			var hasSite : int = 0;
			if(!isNewLigneFiche)
			{
				hasSite = (myServiceLigne.myDatas.ligne.idSite != -1 )?1:0;
			}
			
			obj.idTypeCmd = (isNewLigneFiche)?cbTypeCmd.selectedItem.IDPROFIL_EQUIPEMENT:-1;
			obj.idGpeClient = idGpeClient;
			obj.idPool = CacheDatas.idPool;
			obj.hasSite = hasSite;
			setInfoLigneVO();
			obj.myLigne = myServiceLigne.myDatas.ligne;
			obj.xmlListeAboOpt = createXmlListeAboOpt();
			obj.xmlListeOrga = createXmlListeOrga();
			
			return obj;	
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Dispatch un évenement pour dire que les informations concernant la ligne ont été mise à jour. 
		 * </pre></p>
		 * 
		 * @param event:<code>GestionParcMobileEvent</code>.
		 * @return void.
		 */			
		private function ficheLigneUploadHandler(event:GestionParcMobileEvent):void
		{
			closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M11','Modifications_enregistr_es'));
			this.dispatchEvent(new GestionParcMobileEvent(GestionParcMobileEvent.INFOS_FICHE_LIGNE_UPLOADED));
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Met à jour les données du collaborateurs ou en créé un nouveau si c'est une demande d'ajout.
		 * </pre></p>
		 * 
		 * @return void
		 * 
		 */			
		private function setInfoLigneVO():void
		{
			if(isNewLigneFiche)
			{
				myServiceLigne.myDatas.ligne.idSousTete = -1;
				myServiceLigne.myDatas.ligne.operateur = new OperateurVO();
				myServiceLigne.myDatas.ligne.operateur.idOperateur = cbOperateur.selectedItem.OPERATEURID;
				myServiceLigne.myDatas.ligne.idCompte = (cbCompte.selectedItem)?cbCompte.selectedItem.IDCOMPTE:-1;
				myServiceLigne.myDatas.ligne.idSousCompte = (cbSousCompte.selectedItem)?cbSousCompte.selectedItem.IDSOUS_COMPTE:-1;
				myServiceLigne.myDatas.ligne.typeLigne = cbTypeLigne.selectedItem.LIBELLE_TYPE_LIGNE;
				myServiceLigne.myDatas.ligne.idTypeLigne = cbTypeLigne.selectedItem.IDTYPE_LIGNE;
				myServiceLigne.myDatas.ligne.fournisseur = new RevendeurVO();
				myServiceLigne.myDatas.ligne.fournisseur.idRevendeur = (cbDistributeur.selectedItem)?cbDistributeur.selectedItem.IDREVENDEUR:-1;
				myServiceLigne.myDatas.ligne.simRattache = new SIMVO();
				myServiceLigne.myDatas.ligne.simRattache.idEquipement = -1;
				if(lbNumSim.text)
				{
					myServiceLigne.myDatas.ligne.simRattache.libelleEquipement = lbNumSim.text;
					myServiceLigne.myDatas.ligne.simRattache.PIN1 = pin1;
					myServiceLigne.myDatas.ligne.simRattache.PIN2 = pin2;
					myServiceLigne.myDatas.ligne.simRattache.PUK1 = puk1;
					myServiceLigne.myDatas.ligne.simRattache.PUK2 = puk2;
				}
			}
			
			if(cbFonction.selectedItem)
			{
				if(cbFonction.selectedItem.hasOwnProperty("USAGE")) // c'est une ancienne fonction
				{
					myServiceLigne.myDatas.ligne.fonction = cbFonction.selectedItem.USAGE;
				}
				else // c'est une nouvelle fonction
				{
					myServiceLigne.myDatas.ligne.fonction = (cbFonction.selectedItem as String);
				}
			}
			myServiceLigne.myDatas.ligne.sousTete = tiSousTete.text;
			myServiceLigne.myDatas.ligne.payeur	= (cbPayeur.selectedItem)? cbPayeur.selectedItem.data : 0;
			myServiceLigne.myDatas.ligne.titulaire = (cbTitulaire.selectedItem)? cbTitulaire.selectedItem.data : 0;
			myServiceLigne.myDatas.ligne.idSite	= (tncSite.cb_site.selectedItem)? tncSite.cb_site.selectedItem.IDSITE_PHYSIQUE : -1;
			myServiceLigne.myDatas.ligne.texteChampPerso1 = tncDivers.ti_chp1.text;
			myServiceLigne.myDatas.ligne.texteChampPerso2 = tncDivers.ti_chp2.text;
			myServiceLigne.myDatas.ligne.texteChampPerso3 = tncDivers.ti_chp3.text;
			myServiceLigne.myDatas.ligne.texteChampPerso4 = tncDivers.ti_chp4.text;
			myServiceLigne.myDatas.ligne.numContrat = tiNumContrat.text;
			myServiceLigne.myDatas.ligne.commentaires = taCommentaires.text;
			myServiceLigne.myDatas.ligne.etat = (cbEtat.selectedItem)?cbEtat.selectedItem.data:0;
			myServiceLigne.myDatas.ligne.dureeContrat = (tncContratOperateur.cbDureeContrat.selectedItem)?tncContratOperateur.cbDureeContrat.selectedItem.value:0;
			myServiceLigne.myDatas.ligne.dateOuverture = (tncContratOperateur.dcDateOuverture.selectedDate)?tncContratOperateur.dcDateOuverture.selectedDate:null;
			myServiceLigne.myDatas.ligne.dateRenouvellement = (tncContratOperateur.dcDateRenouvellement.selectedDate)?tncContratOperateur.dcDateRenouvellement.selectedDate:null;
			myServiceLigne.myDatas.ligne.dateResiliation = (tncContratOperateur.dcDateResiliation.selectedDate)?tncContratOperateur.dcDateResiliation.selectedDate:null;
			myServiceLigne.myDatas.ligne.dureeEligibilite = (tncContratOperateur.cbDureeEligibilite.selectedItem)?tncContratOperateur.cbDureeEligibilite.selectedItem.value:0;
			myServiceLigne.myDatas.ligne.dateFPC = (tncContratOperateur.lbDateFpc.text)?new Date(tncContratOperateur.lbDateFpc.text):null;
			
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Créé le XML correspondant à la liste des organisations avec les chemins sélectionnés par l'utilisateur.
		 * </pre></p>
		 * 
		 * @return String
		 * 
		 */	
		private function createXmlListeOrga():String
		{
			var tmpListeOrga : ArrayCollection = new ArrayCollection();
			var lenListeOrga : int = myServiceLigne.myDatas.ligne.listeOrganisation.length;
			var myXML : String = "";
			
			// trier la liste des orga par date de modification (pour pas changer procedure dba)
			for(var j : int = 0; j < lenListeOrga; j++)
			{
				if(tmpListeOrga.length == 0)
				{
					tmpListeOrga.addItem((myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO));
				}
				else
				{
					var index : int = 0;
					var longueur : int = tmpListeOrga.length;
					
					if((myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO).dateModifPosition)
					{
						// tant la date de l'item de tmpListeOrga est plus recente que la date de l'item de listeOrganisation
						while((index < longueur)
							&& ((tmpListeOrga[index] as OrgaVO).dateModifPosition) 
							&& ((tmpListeOrga[index] as OrgaVO).dateModifPosition.getTime() > (myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO).dateModifPosition.getTime()))
						{
							index++;
						}
						tmpListeOrga.addItemAt((myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO),index);
					}
					else
					{
						index++;
						tmpListeOrga.addItemAt((myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO),index);
					}
				}
			}
			
			// création de l'XML
			myXML += "<Orgas>";
			for(var i : int = 0; i < lenListeOrga; i++)
			{
				if((tmpListeOrga[i] as OrgaVO).chemin != null && (tmpListeOrga[i] as OrgaVO).idCible != 0)
				{
					myXML += "<Orga>";
					myXML += "<idOrga>" + (tmpListeOrga[i] as OrgaVO).idOrga + "</idOrga>"
					myXML += "<chemin>" + (tmpListeOrga[i] as OrgaVO).chemin + "</chemin>";
					myXML += "<idCible>" + (tmpListeOrga[i] as OrgaVO).idCible + "</idCible>";
					myXML += "</Orga>";
				}
			}
			myXML += "</Orgas>";
			
			if(myXML != "<Orgas></Orgas>")
			{
				return myXML;
			}
			else
			{
				return "";
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Créé le XML correspondant à la liste des abonnements et options.
		 * Pour l'ajout :
		 * <aboOption>
		 * 		<abo>
		 * 			<idProduitCatalogue>...</idProduitCatalogue>
		 * 		</abo>
		 * 		<options>
		 *			 <option>
		 *           		<idProduitCatalogue>...</idProduitCatalogue>
		 *			 </option>
		 * 		</options>
		 * </aboOption>
		 * 
		 * Pour l'edition :
		 * <aboOpts>
		 * 		<aboOpt>
		 * 			<idInventaire>...</idInventaire>
		 * 			<boolInInventaire>...</boolInInventaire>
		 * 			<aEntrer>...</aEntrer>
		 * 			<aSortir>...</aSortir>
		 * 		</aboOpt>
		 * </aboOpts>
		 * 
		 * </pre></p>
		 * 
		 * @return String
		 * 
		 */	
		private function createXmlListeAboOpt():String
		{
			var myXML : String = "";
			
			if(isNewLigneFiche)
			{
				var lenDataGridAboOpt : int = tncAboOption.dataDatagrid.length;
				
				myXML += "<aboOption>";
				// boucle pour l'abonnement
				for(var j : int = 0 ; j < lenDataGridAboOpt ; j++)
				{
					if(tncAboOption.dataDatagrid[j].IDTHEME_PRODUIT == GestionParcMobileConstantes.ABO_DATA
						|| tncAboOption.dataDatagrid[j].IDTHEME_PRODUIT == GestionParcMobileConstantes.ABO_VOIX)
					{
						myXML += "<abo><idProduitCatalogue>" + tncAboOption.dataDatagrid[j].IDPRODUIT_CATALOGUE + "</idProduitCatalogue></abo>";
						break;
					}
				}
				// boucles pour les options
				myXML += "<options>";
				for(var k : int = 0 ; k < lenDataGridAboOpt ; k++)
				{
					if(tncAboOption.dataDatagrid[k].IDTHEME_PRODUIT == GestionParcMobileConstantes.OPT_DATA
						|| tncAboOption.dataDatagrid[k].IDTHEME_PRODUIT == GestionParcMobileConstantes.OPT_DIV
						|| tncAboOption.dataDatagrid[k].IDTHEME_PRODUIT == GestionParcMobileConstantes.OPT_PUSH
						|| tncAboOption.dataDatagrid[k].IDTHEME_PRODUIT == GestionParcMobileConstantes.OPT_VOIX)
					{
						myXML += "<option><idProduitCatalogue>" + tncAboOption.dataDatagrid[k].IDPRODUIT_CATALOGUE + "</idProduitCatalogue></option>";
					}
				}
				myXML += "</options></aboOption>";
				
				if(myXML != "<aboOption><options></options></aboOption>")
				{
					return myXML;
				}
				else
				{
					return "";
				}
			}
			else
			{
				var lenListeAboOpt : int = myServiceLigne.myDatas.ligne.listeAboOption.length;
				
				myXML += "<aboOpts>";
				for(var i : int = 0 ; i < lenListeAboOpt ; i++)
				{
					if((myServiceLigne.myDatas.ligne.listeAboOption[i].dansInventaire == "1" && myServiceLigne.myDatas.ligne.listeAboOption[i].aSortir == 1)
						|| (myServiceLigne.myDatas.ligne.listeAboOption[i].dansInventaire == "0" && myServiceLigne.myDatas.ligne.listeAboOption[i].aEntrer == 1))
					{
						myXML += "<aboOpt>";
						myXML += "<idInventaire>" + myServiceLigne.myDatas.ligne.listeAboOption[i].idInventaireProduit + "</idInventaire>"
						myXML += "<boolInInventaire>" + myServiceLigne.myDatas.ligne.listeAboOption[i].dansInventaire + "</boolInInventaire>";
						myXML += "<aEntrer>" + myServiceLigne.myDatas.ligne.listeAboOption[i].aEntrer + "</aEntrer>";
						myXML += "<aSortir>" + myServiceLigne.myDatas.ligne.listeAboOption[i].aSortir + "</aSortir>";
						myXML += "</aboOpt>";
						
					}
				}
				myXML += "</aboOpts>";
				
				if(myXML != "<aboOpts></aboOpts>")
				{
					return myXML;
				}
				else
				{
					return "";
				}
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Active le bouton "Valider".
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */	
		private function activeValidButton(event:Event):void
		{
			btValid.enabled = true;
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Affiche la popup des organisations
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */			
		public function clickModifierOrgaHandler(event:MouseEvent):void 
		{
			ficheOrga = new FicheOrgaIHM();
			ficheOrga.libelleOrga = (cbOrganisation.selectedItem as OrgaVO).libelleOrga;
			ficheOrga.idOrga = (cbOrganisation.selectedItem as OrgaVO).idOrga;
			ficheOrga.addEventListener(GestionParcMobileEvent.VALIDE_FICHE_ORGA,updateOrga);
			PopUpManager.addPopUp(ficheOrga, this, true);
			PopUpManager.centerPopUp(ficheOrga);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Sélectionne le "chemin cible" (=position) s'il existe.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */	
		private function changeOrgaHandler(event:ListEvent):void
		{
			if((cbOrganisation.selectedItem as OrgaVO).position == 1)
			{
				lbPosition.text = (cbOrganisation.selectedItem as OrgaVO).chemin;
			}
			else
			{
				lbPosition.text = "";
			}
			activeValidButton(null);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Mise à jour des données pour l'organisation sélectionnée,
		 * après avoir choisi un chemin dans la fiche Organisation.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */	
		private function updateOrga(event:GestionParcMobileEvent):void
		{
			var lenListeOrga : int = myServiceLigne.myDatas.ligne.listeOrganisation.length;
			
			for(var i : int = 0; i < lenListeOrga; i++)
			{
				if((myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).idOrga == ficheOrga.myService.myDatas.cheminSelected.idOrga)
				{
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).idCible = ficheOrga.myService.myDatas.cheminSelected.idFeuille;
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).chemin = ficheOrga.myService.myDatas.cheminSelected.chemin;
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).position = 1;
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).dateModifPosition = new Date();
				}
			}
			lbPosition.text = ficheOrga.myService.myDatas.cheminSelected.chemin;
			activeValidButton(null);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Affiche la pop up de l'historique du produit selectionné
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */	
		private function displayHistoriqueHandler(event:ContentFicheEvent):void
		{
			var ficheHistorique : FicheHistoriqueIHM_V2 = new FicheHistoriqueIHM_V2();
			
			ficheHistorique.idRessource	= event.idInventaireProduit;
			
			PopUpManager.addPopUp(ficheHistorique,this,true);
			PopUpManager.centerPopUp(ficheHistorique);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Affiche la pop up pour l'edition d'une fonction (aussi appelé usage).
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */	
		private function imgEditFonctionClickHandler(event:MouseEvent):void
		{
			var popupUsage : PopUpUsageIHM = new PopUpUsageIHM();
			if(cbFonction.selectedItem)
			{
				if(cbFonction.selectedItem.hasOwnProperty("USAGE")) // c'est une ancienne fonction
				{
					popupUsage.oldUsage = cbFonction.selectedItem.USAGE;
				}
				else // c'est une nouvelle fonction
				{
					popupUsage.oldUsage = (cbFonction.selectedItem as String);
					strOldUsage = (cbFonction.selectedItem as String);
				}
			}
			popupUsage.addEventListener(GestionParcMobileEvent.POPUP_USAGE_VALIDED, addUsage);
			PopUpManager.addPopUp(popupUsage, this, true);
			PopUpManager.centerPopUp(popupUsage);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ajoute une fonction à la liste des fonctions.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */	
		private function addUsage(event:GestionParcMobileEvent):void
		{
			if(strOldUsage) // si ce n'est pas le premiere edition de fonction
			{
				var lenListFonction : int = (cbFonction.dataProvider as ArrayCollection).length;
				
				for(var i : int = 0 ; i < lenListFonction ; i++)
				{
					if(cbFonction.dataProvider[i] == strOldUsage)
					{
						(cbFonction.dataProvider as ArrayCollection).removeItemAt(i);
						break;
					}
				}
			}
			
			(cbFonction.dataProvider as ArrayCollection).addItem(event.obj);
			cbFonction.selectedIndex = (cbFonction.dataProvider as ArrayCollection).length - 1;
			activeValidButton(null);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Affiche la pop up pour l'ajout l'affectation d'une carte SIM
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */	
		private function btAddSimClickHandler(event:MouseEvent):void
		{
			var popupSIM : PopUpSimIHM = new PopUpSimIHM();
			popupSIM.addEventListener(GestionParcMobileEvent.POPUP_SIM_VALIDATED, popupSimpopupSimValidatedHandler);
			PopUpManager.addPopUp(popupSIM, this, true);
			PopUpManager.centerPopUp(popupSIM);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Mise à jour des infos de la SIM a ajouter
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */	
		private function popupSimpopupSimValidatedHandler(event:GestionParcMobileEvent):void
		{
			lbNumSim.text = event.obj.numSim;
			pin1 = event.obj.pin1;
			pin2 = event.obj.pin2;
			puk1 = event.obj.puk1;
			puk2 = event.obj.puk2;
			lbNumSim.visible = true;
			lbNumSim.includeInLayout = true;
			btAddSim.visible = false;
			btAddSim.includeInLayout = false;
			imgEditSim.visible = true;
			imgEditSim.includeInLayout = true;
			imgDelSim.visible = true;
			imgDelSim.includeInLayout = true;
			
			// pas de site possible si pas de sim
			if(!lbNumSim.text)
			{
				tncSite.enabled = false;	
			}
			else
			{
				tncSite.enabled = true;
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Affiche la pop up pour l'edition de la SIM.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */	
		private function imgEditSimClickHandler(event:MouseEvent):void
		{
			var popupSIM : PopUpSimIHM = new PopUpSimIHM();
			var mySIM : SIMVO = new SIMVO();
			mySIM.libelleEquipement = lbNumSim.text;
			mySIM.PIN1 = pin1;
			mySIM.PIN2 = pin2;
			mySIM.PUK1 = puk1;
			mySIM.PUK2 = puk2;
			popupSIM.carteSim = mySIM;
			popupSIM.addEventListener(GestionParcMobileEvent.POPUP_SIM_VALIDATED, popupSimpopupSimValidatedHandler);
			PopUpManager.addPopUp(popupSIM, this, true);
			PopUpManager.centerPopUp(popupSIM);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Supprime la SIM qui devait etre creee a la validation.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */	
		private function imgDelSimClickHandler(event:MouseEvent):void
		{
			pin1 = "";
			pin2 = "";
			puk1 = "";
			puk2 = "";
			lbNumSim.text = "";
			btAddSim.visible = true;
			btAddSim.includeInLayout = true;
			lbNumSim.visible = false;
			lbNumSim.includeInLayout = false;
			imgDelSim.visible = false;
			imgDelSim.includeInLayout = false;
			imgEditSim.visible = false;
			imgEditSim.includeInLayout = false;
			
			// pas de site possible si pas de sim
			if(!lbNumSim.text)
			{
				tncSite.enabled = false;	
			}
			else
			{
				tncSite.enabled = true;
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite à l'écoute de l'événement GestionParcMobileEvent.LISTE_REVENDEUR_PROF_EQPT_LOADED.
		 * Cette fonction met à jour la comboBox "cbDistributeur".
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see gestionparcmobile.event
		 * @see gestionparcmobile.services.cache.CacheDatas
		 * 
		 */	
		private function cacheListeRevendeurProfilEqptLoadedHandler(event:GestionParcMobileEvent):void
		{
			cbDistributeur.dataProvider = CacheDatas.listeRevendeurForProfEqpt;
			cbDistributeur.selectedIndex = 0;
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite à l'écoute de l'événement GestionParcMobileEvent.LISTE_COMPTES_SOUSCOMPTES_LOADED.
		 * Cette fonction met à jour la comboBox "cbCompte" et la la comboBox "cbSousCompte".
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see gestionparcmobile.event
		 * @see gestionparcmobile.services.cache.CacheDatas
		 * 
		 */	
		private function cacheListeComptesSousComptesLoadedHandler(event:GestionParcMobileEvent):void
		{
			cbCompte.dataProvider = CacheDatas.listeCompteSousCompte;
			if((cbCompte.dataProvider as ArrayCollection).length > 0)
			{
				cbCompte.selectedIndex = 0;
				cbSousCompte.dataProvider = cbCompte.dataProvider[cbCompte.selectedIndex].TAB_SOUS_COMPTE;
				cbSousCompte.selectedIndex = 0;
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite à l'écoute de l'événement GestionParcMobileEvent.INFO_OP_LOADED.
		 * Cette fonction reinitialise le contrat operateur.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see gestionparcmobile.event
		 * @see reinitiliseContratOperateur()
		 * 
		 */	
		private function cacheInfoOpLoadedHandler(event:GestionParcMobileEvent):void
		{
			var dureeEligibilite : int = CacheDatas.listeInfosOperateur[0].DUREE_ELLIGIBILITE;
			reinitiliseContratOperateur(dureeEligibilite-1); // -1 car l'index commence de 0
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite à l'écoute de l'événement GestionParcMobileEvent.LISTE_PROFIL_EQPT_LOADED.
		 * Cette fonction met à jour la comboBox "cbTypeCmd" et appelles la procedure permettant de recuperer la liste des revendeurs pour un profil d'équipement.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see gestionparcmobile.event
		 * @see gestionparcmobile.services.cache.CacheDatas
		 * 
		 */	
		private function cacheListeProfilEqptLoadedHandler(event:GestionParcMobileEvent):void
		{
			cbTypeCmd.dataProvider = CacheDatas.listeProfilEqpt;
			cbTypeCmd.selectedIndex = 0;
			if(cbTypeCmd.selectedItem)
			{
				var objToServiceGet : Object = new Object();				
				objToServiceGet.idPool = CacheDatas.idPool;
				objToServiceGet.idProfilEquipement = cbTypeCmd.selectedItem.IDPROFIL_EQUIPEMENT;
				objToServiceGet.clefRecherche = "";
				
				myServiceCache.getListRevendeurForProfEqpt(objToServiceGet);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite au changement de la valeur sélectionnée dans la comboBox "cbTypeCmd".
		 * Cette fonction affiche une popup informant l'utilisateur que son changement entrenera la reinitialisation
		 * des donnees sur les abonnements, options et sur le contrat opérateur.		 
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */
		private function cbTypeCmdListEventChange(event:ListEvent):void
		{
			var popUpChangeType : ConfirmerOrAnnulerMsgIHM = new ConfirmerOrAnnulerMsgIHM();
			popUpChangeType.msg = ResourceManager.getInstance().getString('M11', 'Si_vous_modifiez_le_type_de_commande__tous_vos_c');
			popUpChangeType.btConfirmerLabel = ResourceManager.getInstance().getString('M11', 'Confirmer') ;
			popUpChangeType.btAnnulerLabel = ResourceManager.getInstance().getString('M11', 'Annuler') ;
			popUpChangeType.addEventListener(ConfirmerOrAnnulerMsgImpl.CONFIRMER, changeTypeCmdConfirmer);
			popUpChangeType.addEventListener(ConfirmerOrAnnulerMsgImpl.ANNULER, changeTypeCmdAnnuler);
			PopUpManager.addPopUp(popUpChangeType, this, true);
			PopUpManager.centerPopUp(popUpChangeType);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Cette fonction reinitialise la liste des abonnements et des options.
		 * </pre></p>
		 * 
		 * @return void
		 * 
		 */
		private function reinitiliseAboOpt():void
		{
			myServiceLigne.myDatas.listeAbo = new ArrayCollection();
			myServiceLigne.myDatas.listeAboFavoris = new ArrayCollection();
			myServiceLigne.myDatas.listeOpt = new ArrayCollection();
			myServiceLigne.myDatas.listeOptFavoris = new ArrayCollection();
			
			tncAboOption.dataDatagrid.removeAll();
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Cette fonction reinitialise le contrat operateur.
		 * Si il n'y a pas de dureeEligibilite par defaut c'est 18 mois.
		 * </pre></p>
		 * 
		 * @param dureeEligibilite
		 * @return void
		 * 
		 */
		private function reinitiliseContratOperateur(dureeEligibilite:int=3):void
		{
			tncContratOperateur.lbOperateur.text = cbOperateur.selectedItem.LIBELLE;
			tncContratOperateur.dcDateOuverture.selectedDate = new Date();
			tncContratOperateur.cbDureeContrat.selectedIndex = 2; // 24 mois
			tncContratOperateur.cbDureeEligibilite.selectedIndex = dureeEligibilite;
			if(tncContratOperateur.cbDureeEligibilite.selectedIndex != -1)
			{
				tncContratOperateur.lbDateEligibilite.text = DateFunction.formatDateAsString(DateFunction.dateAdd("m",new Date(),tncContratOperateur.cbDureeEligibilite.selectedIndex));
			}
			if(tncContratOperateur.cbDureeContrat.selectedIndex != -1)
			{
				tncContratOperateur.lbDateFpc.text = DateFunction.formatDateAsString(DateFunction.dateAdd("m",new Date(),tncContratOperateur.cbDureeContrat.selectedIndex));
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite à la confirmation du changement de type de commande.
		 * Cette fonction appelle la fonction permettant la reinitialisation des abonnements, des options et du contrat operateur
		 * et le service permettant de recuperer la liste des revendeurs pour un profil d'equipement.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see reinitiliseAboOpt()
		 * @see gestionparcmobile.services.cache.CacheServices#getListRevendeurForProfEqpt()
		 * 
		 */
		private function changeTypeCmdConfirmer(event:Event):void
		{
			reinitiliseAboOpt();
			
			indexTypeCmd = cbTypeCmd.selectedIndex;
			
			var objToServiceGet : Object = new Object();				
			objToServiceGet.idPool = CacheDatas.idPool;
			objToServiceGet.idProfilEquipement = cbTypeCmd.selectedItem.IDPROFIL_EQUIPEMENT;
			objToServiceGet.clefRecherche = "";
			
			myServiceCache.getListRevendeurForProfEqpt(objToServiceGet);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite à l'annulation du changement de type de commande.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */
		private function changeTypeCmdAnnuler(event:Event):void
		{
			cbTypeCmd.selectedIndex = indexTypeCmd;
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite à l'écoute de l'événement GestionParcMobileEvent.LISTE_OPERATEUR_LOADED.
		 * Cette fonction met à jour la comboBox "cbOperateur" et appelle le service lié à
		 * la liste des comptes et sous comptes ainsi que le service lié aux informations d'un operateur.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see gestionparcmobile.event
		 * @see gestionparcmobile.services.cache.CacheDatas
		 * @see gestionparcmobile.services.cache.CacheServices#getListeComptesOperateurByLoginAndPool()
		 * @see gestionparcmobile.services.cache.CacheServices#getInfoClientOp()
		 * 
		 */	
		private function cacheListeOperateurLoadedHandler(event:GestionParcMobileEvent):void
		{
			cbOperateur.dataProvider = CacheDatas.listeOperateur;
			cbOperateur.selectedIndex = 0;
			if(cbOperateur.selectedItem)
			{
				var objToServiceGet : Object = new Object();				
				objToServiceGet.idPool = CacheDatas.idPool;
				objToServiceGet.idOperateur = cbOperateur.selectedItem.OPERATEURID;
				
				myServiceCache.getListeComptesOperateurByLoginAndPool(objToServiceGet);
				myServiceCache.getInfoClientOp(objToServiceGet);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite au changement de la valeur sélectionnée dans la comboBox "cbOperateur".
		 * Cette fonction affiche une popup informant l'utilisateur que son changement entrenera la reinitialisation
		 * des donnees sur les abonnements, options et sur le contrat opérateur.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */
		private function cbOperateurListEventChange(event:ListEvent):void
		{
			var popUpChangeType : ConfirmerOrAnnulerMsgIHM = new ConfirmerOrAnnulerMsgIHM();
			popUpChangeType.msg = ResourceManager.getInstance().getString('M11', 'Si_vous_modifiez_l_op_rateur__tous_vos_c');
			popUpChangeType.btConfirmerLabel = ResourceManager.getInstance().getString('M11', 'Confirmer') ;
			popUpChangeType.btAnnulerLabel = ResourceManager.getInstance().getString('M11', 'Annuler') ;
			popUpChangeType.addEventListener(ConfirmerOrAnnulerMsgImpl.CONFIRMER, changeOperateurConfirmer);
			popUpChangeType.addEventListener(ConfirmerOrAnnulerMsgImpl.ANNULER, changeOperateurAnnuler);
			PopUpManager.addPopUp(popUpChangeType, this, true);
			PopUpManager.centerPopUp(popUpChangeType);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite à la confirmation du changement d'operateur.
		 * Cette fonction appelle la reinitialisation des abonnements, des options et du contrat operateur
		 * et le service permettant de recuperer la liste des comptes et sous comptes,
		 * ainsi que le service lié aux informations d'un operateur.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see reinitiliseAboOpt()
		 * @see gestionparcmobile.services.cache#getListeComptesOperateurByLoginAndPool()
		 * @see gestionparcmobile.services.cache.CacheServices#getInfoClientOp()
		 * 
		 */
		private function changeOperateurConfirmer(event:Event):void
		{
			reinitiliseAboOpt();
			
			indexOperateur = cbOperateur.selectedIndex;
			
			var objToServiceGet : Object = new Object();				
			objToServiceGet.idPool = CacheDatas.idPool;
			objToServiceGet.idOperateur = cbOperateur.selectedItem.OPERATEURID;
			
			myServiceCache.getListeComptesOperateurByLoginAndPool(objToServiceGet);
			myServiceCache.getInfoClientOp(objToServiceGet);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite à l'annulation du changement d'operateur.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */
		private function changeOperateurAnnuler(event:Event):void
		{
			cbOperateur.selectedIndex = indexOperateur;
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite à l'écoute de l'événement GestionParcMobileEvent.LISTE_TYPE_LIGNE_LOADED.
		 * Cette fonction met à jour la comboBox "cbTypeLigne".
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see gestionparcmobile.event
		 * @see gestionparcmobile.services.cache.CacheDatas
		 * 
		 */	
		private function cacheListeTypeLigneLoadedHandler(event:GestionParcMobileEvent):void
		{
			cbTypeLigne.dataProvider = CacheDatas.listeTypesLigne;
			
			// pour selectionner automatiquement le type "ligne mobile"
			var longueur : int = (cbTypeLigne.dataProvider as ArrayCollection).length;
			var i : int = 0;
			for(; i < longueur ; i++)
			{
				if(cbTypeLigne.dataProvider[i].IDTYPE_LIGNE == 707)
					break;
			}
			cbTypeLigne.selectedIndex = i;
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appelée suite à l'écoute de l'événement Event.CHANGE de la comboBox "cbCompte".
		 * Cette fonction met à jour la comboBox "cbSousCompte".
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 */	
		private function cbCompteChangeHandler(event:Event):void
		{
			cbSousCompte.dataProvider = cbCompte.dataProvider[cbCompte.selectedIndex].TAB_SOUS_COMPTE;
			cbSousCompte.selectedIndex = 0;
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Teste les validateurs et permet ainsi la cohérence des valeurs attendues.
		 * </pre></p>
		 * 
		 * @return Boolean
		 * 
		 */	
		private function runValidators():Boolean
		{
			var validators:Array = null;
			var results:Array = null;
			
			if(isNewLigneFiche)
			{
				validators = [valSousTete,valNumContrat,valTypeLigne,valOperateur,valCompte,valSousCompte,valTypeCmd,valDistributeur];
				results = Validator.validateAll(validators);
			}
			else
			{
				validators = [valSousTete,valNumContrat];
				results = Validator.validateAll(validators);
			}
			
			if(results.length > 0) 
			{
				ConsoviewAlert.afficherSimpleAlert(GestionParcMobileMessages.RENSEIGNER_CHAMPS_OBLI, ResourceManager.getInstance().getString('M11', 'Attention__'));
				return false;
			}
			else 
			{
				return true;
			}
		}
		
		// PRIVATE FUNCTIONS-------------------------------------------------------------------------
		
	}
}