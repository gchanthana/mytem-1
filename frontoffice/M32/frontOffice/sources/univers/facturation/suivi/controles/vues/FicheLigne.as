package univers.facturation.suivi.controles.vues
{
	import mx.resources.ResourceManager;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Label;
	import mx.managers.PopUpManager;
	
	import univers.facturation.suivi.controles.event.HistoriqueActionRessourceEvent;
	import univers.facturation.suivi.controles.event.SiteEvent;
	import univers.facturation.suivi.controles.services.HistoriqueActionRessourceService;
	import univers.facturation.suivi.controles.services.SiteService;
	import univers.facturation.suivi.controles.vo.LigneVO;
	import univers.facturation.suivi.controles.vo.SiteVO;
	
	[Bindable]
	public class FicheLigne extends TitleWindow
	{
		
		public var fiche:LigneVO;
		
		public var site:SiteVO;
		
		public var historiqueActionRessourceService : HistoriqueActionRessourceService;
		
		public var siteService : SiteService;
		
		public var historiqueProduitInventaire : HistoriqueProduitInventaireIHM;
			
		public function FicheLigne()
		{
		}
		
		public function init():void{
			historiqueActionRessourceService = new HistoriqueActionRessourceService();
			historiqueActionRessourceService.model.addEventListener(HistoriqueActionRessourceEvent.HISTORIQUEACTIONRESSOURCE_COMPELTE, historiqueProduit);
			siteService = new SiteService();
			if(fiche.idSite!=-1){
				siteService.getSite(fiche.idSite);
			}
			siteService.model.addEventListener(SiteEvent.SITE_COMPELTE, afficheSite);
		}
		
		public function historiqueProduit(event:HistoriqueActionRessourceEvent):void{
			if((historiqueActionRessourceService.model.historique.length)>0){
				if((!historiqueProduitInventaire)||(historiqueProduitInventaire && !historiqueProduitInventaire.isPopUp))
				{
					historiqueProduitInventaire = new HistoriqueProduitInventaireIHM();
					historiqueProduitInventaire.dataProduit = historiqueActionRessourceService.model.historique;
					PopUpManager.addPopUp(historiqueProduitInventaire, this, true);
					PopUpManager.centerPopUp(historiqueProduitInventaire);
				}
			}
			else{
				Alert.show(ResourceManager.getInstance().getString('M32', 'Il_n_y_a_pas_d_historique_sur_ce_produit'));
			}
		}
		
		public function afficheSite(event:SiteEvent):void{
			site=siteService.model.produit;
		}
		
		public function historiqueInventaire(idInventaireProduit:Number):void {
			historiqueActionRessourceService.getHistoriqueInventaire(idInventaireProduit);
		}
		
	}
}