package univers.facturation.suivi.controles.vues
{
	import composants.Format;
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.sampler.NewObjectSample;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.NumericStepper;
	import mx.controls.RadioButton;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.events.ListEvent;
	import mx.events.ToolTipEvent;
	import mx.managers.PopUpManager;
	import mx.managers.PopUpManagerChildList;
	import mx.resources.ResourceManager;
	
	import univers.facturation.suivi.controles.controls.AbstractControleFacture;
	import univers.facturation.suivi.controles.controls.formatsclients.LogControl;
	import univers.facturation.suivi.controles.controls.formatsclients.LogControles;
	import univers.facturation.suivi.controles.event.ActeGestionEvent;
	import univers.facturation.suivi.controles.event.CommandeEvent;
	import univers.facturation.suivi.controles.event.FicheLigneEvent;
	import univers.facturation.suivi.controles.event.HistoriqueEvent;
	import univers.facturation.suivi.controles.event.InventaireEvent;
	import univers.facturation.suivi.controles.event.RessourceEvent;
	import univers.facturation.suivi.controles.renderer.tooltip.CustomToolTip;
	import univers.facturation.suivi.controles.services.ActeGestionService;
	import univers.facturation.suivi.controles.services.CommandeService;
	import univers.facturation.suivi.controles.services.ExportService;
	import univers.facturation.suivi.controles.services.FicheLigneService;
	import univers.facturation.suivi.controles.services.HistoriqueService;
	import univers.facturation.suivi.controles.services.InventaireService;
	import univers.facturation.suivi.controles.services.RessourceService;
	import univers.facturation.suivi.controles.vo.InventaireVO;
	import univers.facturation.suivi.controles.vo.Produit;
	import univers.facturation.suivi.controles.vo.ProduitVO;
	import univers.facturation.suivi.controles.vo.StatutVO;
	import univers.facturation.suivi.controles.vo.TotalVo;

	public class Inventaire extends VBox
	{

		public static const VALIDER:String=ResourceManager.getInstance().getString('M32', 'valider');
		public static const RETOUR:String=ResourceManager.getInstance().getString('M32', 'retour');

		public var ctrl:AbstractControleFacture;
		public var txtFiltreInventaire:TextInputLabeled;
		public var abo:CheckBox;
		public var option:CheckBox;
		public var nonTraite:CheckBox;
		public var reclamation:CheckBox;
		public var cloture:CheckBox;
		public var effect:NumericStepper;
		public var gestion:ComboBox;
		public var dtgFactures:DataGrid;
		[Bindable]
		public var dtgInventaire:DataGrid;
		public var chbox:DataGridColumn;
		[Bindable]
		public var cbxAgirHorsInventaire:ComboBox;
		[Bindable]
		public var cbxAgirDansInventaire:ComboBox;
		[Bindable]
		public var cbChamps:ComboBox;
		public var valideControle:Button;
		public var btRapprocher:Button;
		public var inventaireNonFacture:RadioButton;
		public var inventaireFacture:RadioButton;
		public var lblPrixHorsInv:Label;
		public var lblDansInvNonFact:Label;
		
		[Bindable]
		public var totaldgInventaire:String;
		
		public var arrayFiltre:Array = new Array();
		public var colonneBar:String = '';

		public var HIF1:DataGridColumn;
		public var HIF2:DataGridColumn;
		public var HIF3:DataGridColumn;
		public var HIF4:DataGridColumn;
		public var HIF5:DataGridColumn;
		public var HIF6:DataGridColumn;
		public var HIF7:DataGridColumn;
		public var HIF8:DataGridColumn;
		public var HIF9:DataGridColumn;
		public var HIF10:DataGridColumn;

		public var INF1:DataGridColumn;
		public var INF2:DataGridColumn;
		public var INF3:DataGridColumn;
		public var INF4:DataGridColumn;
		public var INF5:DataGridColumn;
		public var INF6:DataGridColumn;
		public var INF7:DataGridColumn;
		public var INF8:DataGridColumn;
		public var INF9:DataGridColumn;
		public var INF10:DataGridColumn;

		public var currentTooltip:CustomToolTip;

		public var lgCtrl:LogControl;
		public var lgCtrles:LogControles;

		[Bindable]
		public var inventaireService:InventaireService;

		[Bindable]
		public var historiqueService:HistoriqueService;

		[Bindable]
		public var acteGestionService:ActeGestionService;

		[Bindable]
		public var ficheLigneService:FicheLigneService;

		[Bindable]
		public var commandeService:CommandeService;

		[Bindable]
		public var exportService:ExportService;

		[Bindable]
		public var historiqueProduit:HistoriqueProduitIHM;

		[Bindable]
		public var actGestion:ActeGestionIHM;

		[Bindable]
		public var ficheBox:FicheLigneIHM;

		[Bindable]
		public var commandeBox:CommandeIHM;

		[Bindable]
		public var paramInventaire:InventaireVO;

		[Bindable]
		public var totalFactures:TotalVo=new TotalVo();

		[Bindable]
		public var totalInventaire:TotalVo=new TotalVo();
		
		[Bindable]
		public var totalInventaireAffiche:TotalVo=new TotalVo();

		[Bindable]
		public var boolckbtous:Boolean=false;

		[Bindable]
		public var boolckbtousInv:Boolean=false;

		[Bindable]
		public var rapprochementEnabled:Boolean=false;




		//passage inventaire
		private var popUpPasserDansInv:EntrerProduitDansInventaireIHM;
		private var popUpPasserHorsInv:SortirProduitInventaireIHM;
		[Bindable]
		public var ressourceService:RessourceService;

		public function Inventaire()
		{
			ressourceService=new RessourceService();
			popUpPasserDansInv=new EntrerProduitDansInventaireIHM();
			popUpPasserHorsInv=new SortirProduitInventaireIHM();
		}
		
		private var _btValiderEnabled:Boolean = false;
		
		[Bindable]
		public function get btValiderEnabled():Boolean
		{
			return _btValiderEnabled;
		}

		public function set btValiderEnabled(value:Boolean):void
		{
			_btValiderEnabled = value;
		}

		public function initInventaire():void
		{
			boolckbtous=false;
			boolckbtousInv=false;
			totalFactures=new TotalVo();
			totalInventaire=new TotalVo();
			historiqueService=new HistoriqueService();
			acteGestionService=new ActeGestionService();
			ficheLigneService=new FicheLigneService();
			commandeService=new CommandeService();
			exportService=new ExportService();
			
			inventaireService.model.addEventListener(InventaireEvent.INVENTAIRE_FACTURE_COMPELTE, inventaireFactureRequestCompleteHandler);
			inventaireService.model.addEventListener(InventaireEvent.INVENTAIRE_NON_FACTURE_COMPELTE, inventaireNonFactureRequestCompleteHandler);
			historiqueService.model.addEventListener(HistoriqueEvent.HISTORIQUE_COMPELTE, histoProduit);
			acteGestionService.model.addEventListener(ActeGestionEvent.ACTEGESTION_COMPELTE, actComplete);
			ficheLigneService.model.addEventListener(FicheLigneEvent.FICHELIGNE_COMPELTE, ficheLigneComplete);
			ficheLigneService.model.addEventListener(FicheLigneEvent.LIBELLEPERSO_COMPELTE, libelleperso);
			commandeService.model.addEventListener(CommandeEvent.COMMANDE_GENERALE_COMPELTE, commandreGeneraleComplete);
			
			popUpPasserDansInv.addEventListener(RessourceEvent.UPD_INV_FACTURE_SUCCES, updateInventaireFactureeHandler);
			popUpPasserHorsInv.addEventListener(RessourceEvent.UPD_INV_NON_FACTURE_SUCCES, updateInventaireNonFactureeHandler);
		}
		
		public function applicationComplete_handler():void
		{
			if (CvAccessManager.getSession().USER.GLOBALIZATION == "en_GB" || CvAccessManager.getSession().USER.GLOBALIZATION == "en_US")
				lblPrixHorsInv.text = (totalFactures.ligne)+resourceManager.getString('M32','ligne_s__s_lectionn_e_s_')+ moduleSuiviFacturationIHM.currencySymbol + (totalFactures.montant) + ' / ' + (inventaireService.model.totalFacture)+resourceManager.getString('M32','enregistrement_s_')+inventaireService.model.factures.length+resourceManager.getString('M32','enregistrement_s__affich__s_');
			else
				lblPrixHorsInv.text = (totalFactures.ligne)+resourceManager.getString('M32','ligne_s__s_lectionn_e_s_')+(totalFactures.montant)+ ' ' + moduleSuiviFacturationIHM.currencySymbol + ' / ' + (inventaireService.model.totalFacture)+resourceManager.getString('M32','enregistrement_s_')+inventaireService.model.factures.length+resourceManager.getString('M32','enregistrement_s__affich__s_');
			
			if (CvAccessManager.getSession().USER.GLOBALIZATION == "en_GB" || CvAccessManager.getSession().USER.GLOBALIZATION == "en_US")
				lblDansInvNonFact.text = (totalInventaire.ligne)+resourceManager.getString('M32','ligne_s__s_lectionn_e_s_')+ moduleSuiviFacturationIHM.currencySymbol + (totalInventaire.montant) + ' / ' +(totaldgInventaire)+resourceManager.getString('M32','enregistrement_s_')+totalInventaireAffiche.ligne+resourceManager.getString('M32','enregistrement_s__affich__s_');
			else
				lblDansInvNonFact.text = (totalInventaire.ligne)+resourceManager.getString('M32','ligne_s__s_lectionn_e_s_')+(totalInventaire.montant)+ ' ' + moduleSuiviFacturationIHM.currencySymbol + ' / ' +(totaldgInventaire)+resourceManager.getString('M32','enregistrement_s_')+totalInventaireAffiche.ligne+resourceManager.getString('M32','enregistrement_s__affich__s_');
		}
		
		public function inventaireFactureRequestCompleteHandler(event:InventaireEvent):void
		{
			boolckbtous=false;
			totalFactures=new TotalVo();
			boolckbtousInv=false;
			totalInventaire=new TotalVo();
			modifParam(event);
		}

		public function inventaireNonFactureRequestCompleteHandler(event:InventaireEvent):void
		{
			boolckbtousInv=false;
			totalInventaire=new TotalVo();
			modifParam(event);
			totaldgInventaire=inventaireService.model.totalInventaireFacture.toString();
			
		}
		
		public function updateInventaireFactureeHandler(event:RessourceEvent):void
		{
			inventaireService.getInventaireFacture(paramInventaire);
		}
		
		public function updateInventaireNonFactureeHandler(event:RessourceEvent):void
		{
			inventaireService.getInventaireNonFacture(paramInventaire);
		}

		public function histoProduit(event:HistoriqueEvent):void
		{
			if ((!historiqueProduit) || (historiqueProduit && !historiqueProduit.isPopUp))
			{
				if ((historiqueService.model.historique.length) > 0)
				{
					historiqueProduit=new HistoriqueProduitIHM();
					historiqueProduit.dataProduit=historiqueService.model.historique;
					PopUpManager.addPopUp(historiqueProduit, this, true);
					PopUpManager.centerPopUp(historiqueProduit);
				}
				else
				{
					Alert.show(ResourceManager.getInstance().getString('M32', 'Il_n_y_a_pas_d_historique_sur_ce_produit'));
				}
			}
		}

		public function actComplete(event:ActeGestionEvent):void
		{
			if ((!actGestion) || (actGestion && !actGestion.isPopUp))
			{
				actGestion=new ActeGestionIHM();
				actGestion.dataActeGestion=acteGestionService.model.acteGestion;
				PopUpManager.addPopUp(actGestion, this, true);
				PopUpManager.centerPopUp(actGestion);
			}
		}

		public function ficheLigneComplete(event:FicheLigneEvent):void
		{
			if ((!ficheBox) || (ficheBox && !ficheBox.isPopUp))
			{				
				ficheBox.fiche=ficheLigneService.model.ficheLigne;
				PopUpManager.addPopUp(ficheBox, this, true);
				PopUpManager.centerPopUp(ficheBox);
			}
		}

		public function libelleperso(event:FicheLigneEvent):void
		{
			
		}

		public function commandreGeneraleComplete(event:CommandeEvent):void
		{
			if ((!commandeBox) || (commandeBox && !commandeBox.isPopUp))
			{
				commandeBox=new CommandeIHM();
				commandeBox.commande=commandeService.model.commande;
				PopUpManager.addPopUp(commandeBox, this, true);
				PopUpManager.centerPopUp(commandeBox);
			}
		}

		[Bindable]
		public function set ctrlFacture(ctrlFactApp:AbstractControleFacture):void
		{
			ctrl=ctrlFactApp;
			ctrl.boolInventaire=CvAccessManager.getSession().CURRENT_PERIMETRE.BOOLINVENTAIRE;
		}

		public function get ctrlFacture():AbstractControleFacture
		{
			return ctrl;
		}

		public function rezise(tableau:String):void
		{
			if (tableau == 'HIF')
			{
				INF1.width=HIF1.width;
				INF2.width=HIF2.width;
				INF3.width=HIF3.width;
				INF4.width=HIF4.width;
				INF5.width=HIF5.width;
				INF6.width=HIF6.width;
				INF7.width=HIF7.width;
				INF8.width=HIF8.width;
				INF9.width=HIF9.width;
				INF10.width=HIF10.width;
			}
			else
			{
				HIF1.width=INF1.width;
				HIF2.width=INF2.width;
				HIF3.width=INF3.width;
				HIF4.width=INF4.width;
				HIF5.width=INF5.width;
				HIF6.width=INF6.width;
				HIF7.width=INF7.width;
				HIF8.width=INF8.width;
				HIF9.width=INF9.width;
				HIF10.width=INF10.width;
			}
		}

		public function filtreProduitFactureChangeHandler(event:Event):void
		{
			if ((dtgFactures.dataProvider != null) || (dtgInventaire.dataProvider != null))
			{
				(dtgFactures.dataProvider as ArrayCollection).filterFunction=filtre;
				(dtgFactures.dataProvider as ArrayCollection).refresh();
				(dtgInventaire.dataProvider as ArrayCollection).filterFunction=filtre;
				(dtgInventaire.dataProvider as ArrayCollection).refresh();
				totalInventaireAffiche.ligne = (dtgInventaire.dataProvider as ArrayCollection).length.toString();
			}
		}
		
		public function filtreColumn(event:Event):void
		{
			var colonne:Number = cbChamps.selectedItem.value;

			switch(colonne)
			{
				case 0:
					colonneBar = '';
					break;
				case 1:
					colonneBar = 'sousCompte';
					break;
				case 2:
					colonneBar = 'noeudOrga';
					break;
				case 3:
					colonneBar = 'ligne';
					break;
				case 4:
					colonneBar = 'sousCompte diff';
					break;
				case 5:
					colonneBar = 'noeudOrga diff';
					break;
				case 6:
					colonneBar = 'ligne diff';
					break;
			}
			
			arrayFiltre=getColumnFilter(colonneBar)
			
			filtreProduitFactureChangeHandler(event);
			
		}
		
		private function getColumnFilter(colonne:String):Array
		{
			
			var arrayFiltreFacture:Array = new Array();
			var arrayFiltreInventaire:Array = new Array();
			
			var j:int=0;
			var arrayResult:Array = new Array();
			
			arrayFiltreFacture = getColumnFilterProduit(colonne,1);
			arrayFiltreInventaire = getColumnFilterProduit(colonne,2);
			
			for(var i:int=0;i<arrayFiltreFacture.length;i++)
			{
				if(isInArray(arrayFiltreFacture[i],arrayFiltreInventaire))
				{
					arrayResult[j]=arrayFiltreFacture[i];
					j++;
				}
			}
			
			return arrayResult;
			
		}
		
		private function getColumnFilterProduit(colonne:String,dataBase:Number):Array
		{
			var taille:int = (dataBase==1) ? (dtgFactures.dataProvider as ArrayCollection).source.length : (dtgInventaire.dataProvider as ArrayCollection).source.length;
			var arrayFiltre:Array = new Array();
			
			var j:int = 0;
			var verif:String = '';
			
			if(colonne!=''){
				for(var i:int=0;i<taille;i++){
					switch(colonne)
					{
						case 'sousCompte':
							verif = (dataBase==1) ? ((dtgFactures.dataProvider as ArrayCollection).source[i] as ProduitVO).sousCompte : ((dtgInventaire.dataProvider as ArrayCollection).source[i] as ProduitVO).sousCompte;
							break;
						case 'noeudOrga':
							verif = (dataBase==1) ? ((dtgFactures.dataProvider as ArrayCollection).source[i] as ProduitVO).feuille : ((dtgInventaire.dataProvider as ArrayCollection).source[i] as ProduitVO).feuille;
							break;
						case 'ligne':
							verif = (dataBase==1) ? ((dtgFactures.dataProvider as ArrayCollection).source[i] as ProduitVO).ligne : ((dtgInventaire.dataProvider as ArrayCollection).source[i] as ProduitVO).ligne;
							break;
						case 'sousCompte diff':
							verif = (dataBase==1) ? ((dtgFactures.dataProvider as ArrayCollection).source[i] as ProduitVO).sousCompte : ((dtgInventaire.dataProvider as ArrayCollection).source[i] as ProduitVO).sousCompte;
							break;
						case 'noeudOrga diff':
							verif = (dataBase==1) ? ((dtgFactures.dataProvider as ArrayCollection).source[i] as ProduitVO).feuille : ((dtgInventaire.dataProvider as ArrayCollection).source[i] as ProduitVO).feuille;
							break;
						case 'ligne diff':
							verif = (dataBase==1) ? ((dtgFactures.dataProvider as ArrayCollection).source[i] as ProduitVO).ligne : ((dtgInventaire.dataProvider as ArrayCollection).source[i] as ProduitVO).ligne;
							break;
					}
					if(!isInArray(verif,arrayFiltre)){arrayFiltre[j]=verif;j++;}
				}
			}
			
			return arrayFiltre.sort();
			
		}

		private function filtre(produit:ProduitVO):Boolean
		{
			var result:Boolean=((txtFiltre(produit)) && (typeFiltre(produit)) && (statutFiltre(produit)) && (gestionFiltre(produit)) && (columnsFiltre(produit)));

			return result;
		}

		private function txtFiltre(produit:ProduitVO):Boolean
		{
			
			if (((produit.compte) && (produit.compte.toLowerCase().search(txtFiltreInventaire.text.toLowerCase()) != -1)) || ((produit.sousCompte) && (produit.sousCompte.toLowerCase().search(txtFiltreInventaire.text.toLowerCase()) != -1)) || ((produit.feuille) && (produit.feuille.toLowerCase().search(txtFiltreInventaire.text.toLowerCase()) != -1)) || (produit.ligne.toLowerCase().search(txtFiltreInventaire.text.toLowerCase()) != -1) || (produit.libelleProduit.toLowerCase().search(txtFiltreInventaire.text.toLowerCase()) != -1))
				return true;
			else
				return false;
			
		}

		private function typeFiltre(produit:ProduitVO):Boolean
		{

			if (((paramInventaire.abonnement == 1) && (produit.boolAcces == 1)) || ((paramInventaire.option == 1) && (produit.boolAcces == 0)))
				return true;
			else
				return false;

		}

		private function statutFiltre(produit:ProduitVO):Boolean
		{

			if (((paramInventaire.nonTraite == 1) && (produit.statut == 1)) || ((paramInventaire.reclamation == 1) && (produit.statut == 2)) || ((paramInventaire.cloture == 1) && (produit.statut == 0)))
				return true;
			else
				return false;

		}

		private function gestionFiltre(produit:ProduitVO):Boolean
		{

			if (((paramInventaire.gestion == -1)) || ((paramInventaire.gestion == 0) && (produit.actGestion == 0)) || ((paramInventaire.gestion == 1) && (produit.actGestion != 0)))
				return true;
			else
				return false;

		}
		
		private function columnsFiltre(produit:ProduitVO):Boolean
		{
			
			var verif:Boolean = false;
			
			if(colonneBar == 'sousCompte diff' || colonneBar == 'noeudOrga diff' || colonneBar == 'ligne diff'){verif = true;}
			
			if(colonneBar!=''){
				for(var i:int=0;i<arrayFiltre.length;i++)
				{
					switch(colonneBar)
					{
						case 'sousCompte':
							if(produit.sousCompte==arrayFiltre[i]){verif=true;}
							break;
						case 'noeudOrga':
							if(produit.feuille==arrayFiltre[i]){verif=true;}
							break;
						case 'ligne':
							if(produit.ligne==arrayFiltre[i]){verif=true;}
							break;
						case 'sousCompte diff':
							if(produit.sousCompte==arrayFiltre[i]){verif=false;}
							break;
						case 'noeudOrga diff':
							if(produit.feuille==arrayFiltre[i]){verif=false;}
							break;
						case 'ligne diff':
							if(produit.ligne==arrayFiltre[i]){verif=false;}
							break;
						
					}
				}
			}
			else
			{
				verif=true;
			}
			
			return verif;
			
		}

		public function modifParam(event:Event):void
		{
			paramInventaire.filtre=txtFiltreInventaire.text;
			paramInventaire.abonnement=(abo.selected) ? 1 : 0;
			paramInventaire.option=(option.selected) ? 1 : 0;
			paramInventaire.nonTraite=(nonTraite.selected) ? 1 : 0;
			paramInventaire.reclamation=(reclamation.selected) ? 1 : 0;
			paramInventaire.cloture=(cloture.selected) ? 1 : 0;
			paramInventaire.gestion=gestion.selectedItem.value;
			filtreProduitFactureChangeHandler(event);
		}

		public function recherche(e:Event):void
		{
			paramInventaire.effectif=Math.abs(effect.value);
			inventaireService.getInventaireFacture(paramInventaire);
			inventaireService.getInventaireNonFacture(paramInventaire);
		}

		public function historique(idProduit:Number):void
		{
			historiqueService.getHistorique(idProduit);
		}

		public function acteGestion(actGestion:Number, idProduit:Number):void
		{
			if (actGestion > 0)
			{
				acteGestionService.getActeGestion(idProduit);
			}
		}

		public function ficheLigne(idSousTete:Number):void
		{
			ficheBox=new FicheLigneIHM();
			ficheLigneService.getFicheLigne(idSousTete);
			ficheLigneService.getLibellePerso();
		}

		public function getCommande(idCommande:Number):void
		{
			if (idCommande > 0)
			{
				commandeService.getCommande(idCommande);
			}

		}

		public function exportHIF(format:String):void
		{
			exportService.getExportHIF(paramInventaire, format);
		}

		public function exportINF(format:String):void
		{
			exportService.getExportINF(paramInventaire, format);
		}

		public function selectionClickFactureHandler(event:MouseEvent):void
		{
			(dtgFactures.selectedItem as ProduitVO).check((event.currentTarget as CheckBox).selected);
			selectionTotalFacture();
		}

		public function selectionTotalFacture():void
		{

			var nbLigne:Number=0;
			var nbSomme:Number=0;

			for (var i:int=0; i < inventaireService.model.factures.length; i++)
			{
				if ((inventaireService.model.factures[i] as ProduitVO).selected)
				{
					nbLigne++;
					nbSomme+=((inventaireService.model.factures[i] as ProduitVO).montant as Number);
				}
				else
				{
					boolckbtous=false;
				}
			}

			if (inventaireService.model.factures.source.length == nbLigne)
			{
				boolckbtous=true;
			}

			totalFactures.ligne=nbLigne.toString();
			totalFactures.montant=ConsoviewFormatter.formatNumber(nbSomme, 2);

		}

		public function checkAllFacture(event:MouseEvent):void
		{

			var nbLigne:Number=0;
			var nbSomme:Number=0;

			boolckbtous=(event.currentTarget as CheckBox).selected;

			for (var i:int=0; i < inventaireService.model.factures.length; i++)
			{
				if ((event.currentTarget as CheckBox).selected)
				{
					(inventaireService.model.factures[i] as ProduitVO).selected=true;
					nbLigne++;
					nbSomme+=((inventaireService.model.factures[i] as ProduitVO).montant as Number);
				}
				else
				{
					(inventaireService.model.factures[i] as ProduitVO).selected=false;
				}
			}
			totalFactures.ligne=nbLigne.toString();
			totalFactures.montant=ConsoviewFormatter.formatNumber(nbSomme, 2);

		}

		public function selectionClickInventaireHandler(event:MouseEvent):void
		{
			(dtgInventaire.selectedItem as ProduitVO).check((event.currentTarget as CheckBox).selected);
			selectionTotalInventaire();
		}

		public function selectionTotalInventaire():void
		{

			var nbLigne:Number=0;
			var nbSomme:Number=0;

			var inventaire:ArrayCollection=new ArrayCollection();

			if (inventaireNonFacture.selected)
			{
				inventaire=inventaireService.model.inventaireNonFacture;
			}
			else
			{
				inventaire=inventaireService.model.inventaireFacture;
			}

			for (var i:int=0; i < inventaire.length; i++)
			{
				if ((inventaire[i] as ProduitVO).selected)
				{
					nbLigne++;
					nbSomme+=((inventaire[i] as ProduitVO).montant as Number);
				}
				else
				{
					boolckbtousInv=false;
				}
			}

			if (inventaire.source.length == nbLigne)
			{
				boolckbtousInv=true;
			}


			totalInventaire.ligne=nbLigne.toString();
			totalInventaire.montant=ConsoviewFormatter.formatNumber(nbSomme, 2);

		}

		public function checkAllInventaire(event:MouseEvent):void
		{

			var nbLigne:Number=0;
			var nbSomme:Number=0;

			var inventaire:ArrayCollection=new ArrayCollection();

			if (inventaireNonFacture.selected)
			{
				inventaire=inventaireService.model.inventaireNonFacture;
			}
			else
			{
				inventaire=inventaireService.model.inventaireFacture;
			}

			boolckbtousInv=(event.currentTarget as CheckBox).selected;

			for (var i:int=0; i < inventaire.length; i++)
			{
				if ((event.currentTarget as CheckBox).selected)
				{
					nbLigne++;
					nbSomme+=((inventaire[i] as ProduitVO).montant as Number);
				}
				(inventaire[i] as ProduitVO).selected=(event.currentTarget as CheckBox).selected;
			}

			totalInventaire.ligne=nbLigne.toString();
			totalInventaire.montant=ConsoviewFormatter.formatNumber(nbSomme, 2);

		}


		public function btValiderClickHandler(me:MouseEvent):void
		{
			var message:String="";
			message=ResourceManager.getInstance().getString('M32', 'Contr_le_inventaire_valid__') + ctrlFacture.construireSignature() + "\n";
			var titre:String=ResourceManager.getInstance().getString('M32', 'Valider_le_contr_le_de_l_inventaire');
			demanderConfirmation(titre, message, executerBtValiderClick);
		}

		public var confirmBox:ConfirmBox;

		private function demanderConfirmation(libelle:String, message:String, confirmFonction:Function):void
		{
			confirmBox=new ConfirmBoxIHM();
			confirmBox.facture=ctrlFacture.selectedFacture;
			confirmBox.txRapport=message;
			confirmBox.libelle=libelle;
			confirmBox.doFunction=confirmFonction;
			PopUpManager.addPopUp(confirmBox, DisplayObject(parentApplication), true, PopUpManagerChildList.PARENT);
			PopUpManager.centerPopUp(confirmBox);
		}

		//Execute l'action du click sur le bouton taguer apres confirmation
		private function executerBtValiderClick():void
		{


			ctrl.selectedFacture.controleeInv=0;
			ctrl.selectedFacture.commentaireControlerInv=confirmBox.txRapport + "\n" + confirmBox.commentaire + "\n\n";
			ctrl.updateVisaControleFactureInventaire();
			if (ctrl.selectedFacture.controleeInv == 0)
			{
				lgCtrles.setLogsControleFacture(ctrl.selectedFacture.periodeId, lgCtrl.whatIsThisButton(0, 0), confirmBox.txtCommentaire.text);
			}
			else
			{
				lgCtrles.setLogsControleFacture(ctrl.selectedFacture.periodeId, lgCtrl.whatIsThisButton(0, -1), confirmBox.txtCommentaire.text);
			}
			confirmBox=null;
			retour();
		}


		protected function formatDate(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{

				return DateFunction.formatDateAsString(item[column.dataField]);
			}
			else
				return "-";

		}

		protected function formateEuros(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return Format.toCurrencyString(item[column.dataField], 4);
			else
				return "-";
		}

		protected function btRetourClickHandler(me:MouseEvent):void
		{
			retour();
		}

		private function retour():void
		{
			dispatchEvent(new Event(RETOUR));
		}

		///handler----------------

		protected function dtgFacturesChangeHandler(event:ListEvent):void
		{
			activerRapprochement(dtgFactures.selectedIndex != -1 && dtgInventaire.selectedIndex != -1)
		}

		protected function dtgInventaireChangeHandler(event:ListEvent):void
		{
			activerRapprochement(dtgFactures.selectedIndex != -1 && dtgInventaire.selectedIndex != -1)
		}

		protected function btCbxAgirHorsInventaireClickHandler(event:MouseEvent):void
		{

			var selectionList:Array=inventaireService.model.factures.source.filter(isSelected);

			if (selectionList.length > 0)
			{
				var choix:Number=cbxAgirHorsInventaire.selectedItem.value;

				traiterChoixComboAgir(choix, selectionList)
			}
		}

		protected function btCbxAgirDansInventaireClickHandler(event:MouseEvent):void
		{

			var inventaire:ArrayCollection=new ArrayCollection();

			if (inventaireNonFacture.selected)
			{
				inventaire=inventaireService.model.inventaireNonFacture;
			}
			else
			{
				inventaire=inventaireService.model.inventaireFacture;
			}

			var selectionList:Array=inventaire.source.filter(isSelected);

			if (selectionList.length > 0)
			{
				var choix:Number=cbxAgirDansInventaire.selectedItem.value;

				traiterChoixComboAgir(choix, selectionList)
			}
		}

		public function cbxInventaireChangeStatusHandler(event:ListEvent):void
		{
			if (event.currentTarget.selectedItem)
			{
				changerStatut([(dtgInventaire.selectedItem as ProduitVO)], event.currentTarget.selectedItem as StatutVO)
			}
		}

		public function cbxFactureChangeStatusHandler(event:ListEvent):void
		{
			if (event.currentTarget.selectedItem)
			{
				changerStatut([(dtgFactures.selectedItem as ProduitVO)], event.currentTarget.selectedItem as StatutVO)
			}
		}


		protected function btRapprocherClickHandler(event:MouseEvent):void
		{
			if (rapprochementEnabled)
			{
				var ressourceInventaire:ProduitVO=dtgInventaire.selectedItem as ProduitVO;
				var ressourceHorsInventaire:ProduitVO=dtgFactures.selectedItem as ProduitVO;
				var message:String=formatMessageRapprochement(ressourceHorsInventaire, ressourceInventaire);
				var handler2:Function=function _modelMergeProductEventHandler(event:RessourceEvent):void
				{
					ressourceService.model.removeEventListener(RessourceEvent.MERGE_PRODUCT_EVENT, handler2);
					if (ressourceService.model.lastMergeProductResult > 0)
					{
						var index_p1:Number=inventaireService.model.factures.getItemIndex(dtgFactures.selectedItem);
						if (index_p1 >= 0)
						{
							inventaireService.model.factures.removeItemAt(index_p1);
						}

						var index_p2:Number=-1;
						if (inventaireNonFacture.selected)
						{
							index_p2=inventaireService.model.inventaireNonFacture.getItemIndex(dtgInventaire.selectedItem);
							if (index_p2 >= 0)
							{
								inventaireService.model.inventaireNonFacture.removeItemAt(index_p2);
							}
						}
						else
						{
							index_p2=inventaireService.model.inventaireFacture.getItemIndex(dtgInventaire.selectedItem);
							if (index_p2 >= 0)
							{
								inventaireService.model.inventaireFacture.removeItemAt(index_p2);
							}
						}

						selectionTotalFacture();
						selectionTotalInventaire();
					}
				}

				var handler:Function=function f(event:CloseEvent):void
				{
					if (event.detail == Alert.OK)
					{
						ressourceService.model.addEventListener(RessourceEvent.MERGE_PRODUCT_EVENT, handler2);
						ressourceService.mergeProduct(ressourceHorsInventaire, ressourceInventaire);
					}
				}

				ConsoviewAlert.afficherAlertConfirmation(message, "Confirmation", handler);

			}
		}

		public function typeInvantaire():void
		{

			if (inventaireNonFacture.selected)
			{
				dtgInventaire.dataProvider=inventaireService.model.inventaireNonFacture;
				initialiserInventaire(inventaireService.model.inventaireFacture);
				totaldgInventaire=inventaireService.model.totalInventaireFacture.toString();
			}
			else
			{
				dtgInventaire.dataProvider=inventaireService.model.inventaireFacture;
				initialiserInventaire(inventaireService.model.inventaireNonFacture);
				totaldgInventaire=inventaireService.model.totalInventaireNonFacture.toString();
			}

			boolckbtousInv=false;
			totalInventaire.ligne="0";
			totalInventaire.montant="0";
			
			
			
			(dtgInventaire.dataProvider as ArrayCollection).filterFunction=filtre;
			(dtgInventaire.dataProvider as ArrayCollection).refresh();
			
			totalInventaireAffiche.ligne=(dtgInventaire.dataProvider as ArrayCollection).length.toString();

		}

		public function initialiserInventaire(inventaire:ArrayCollection):void
		{
			for (var i:int=0; i < inventaire.length; i++)
			{
				(inventaire[i] as ProduitVO).selected=false;
			}
		}

		///fonction privée
		private function isSelected(element:*, index:int, arr:Array):Boolean
		{
			return (element.selected == true);
		}


		private function afficherPopUpPasserHorsInventaire(pdata:Array):void
		{
			popUpPasserHorsInv.reset();
			popUpPasserHorsInv.dataProvider=new ArrayCollection(pdata);
			PopUpManager.addPopUp(popUpPasserHorsInv, this);
			PopUpManager.centerPopUp(popUpPasserHorsInv);
		}

		private function afficherPopUpPasserDansInventaire(pdata:Array):void
		{
			popUpPasserDansInv.addEventListener('UPDATE_DATAPROVIDER', updateDataproviderHandler);
			popUpPasserDansInv.reset();
			popUpPasserDansInv.dataProvider=new ArrayCollection(pdata);
			PopUpManager.addPopUp(popUpPasserDansInv, this);
			PopUpManager.centerPopUp(popUpPasserDansInv);
		}

		private function updateDataproviderHandler(e:Event):void
		{
			var currentFactures		:ArrayCollection = inventaireService.model.factures;
			var facturesSelected	:ArrayCollection = popUpPasserDansInv.dataProvider;
			
			var lenCF	:int = currentFactures.length;
			var lenFS	:int = facturesSelected.length;

			for(var i:int = 0;i < lenFS;i++)
			{
				var currentProduit:ProduitVO = facturesSelected[i] as ProduitVO;
				
				for(var j:int = 0;j < lenCF;j++)
				{
					if((currentFactures[j] as ProduitVO).idProduit == currentProduit.idProduit)
					{
						currentFactures.removeItemAt(j);
						
						lenCF = currentFactures.length;
						j--;
					}
				}
			}
		}
		
		//idsatut : 0=cloturé, 1= non traité, 2= en cours de reclamation
		private function changerStatut(pdata:Array, satut:StatutVO, boolConfirmation:Boolean=false):void
		{
			var message:String=formaterMessageConfirmationEtat(pdata);
			if (pdata && pdata.length > 0)
			{
				if (boolConfirmation)
				{
					var handler:Function=function changerStatutHandler(event:CloseEvent):void
					{
						if (event.detail == Alert.OK)
						{

							ressourceService.model.addEventListener(RessourceEvent.UPDATE_STATUS_EVENT, function _modelUpdateStatusEventHandler(event:RessourceEvent):void
							{
								//ressourceService.model.removeEventListener(RessourceEvent.UPDATE_STATUS_EVENT,_modelUpdateStatusEventHandler);
								for (var k:Number=0; k < pdata.length; k++)
								{
									(pdata[k] as ProduitVO).statut=satut.ID;
								}
								ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M32', 'Statut_mis___jour'));
							});
							ressourceService.updateStatus(pdata, satut);
						}
					}
					ConsoviewAlert.afficherAlertConfirmation(message, "Confirmation", handler);
				}
				else
				{

					var handler3:Function=function _modelUpdateStatusEventHandler(event:RessourceEvent):void
					{

						ressourceService.model.removeEventListener(RessourceEvent.UPDATE_STATUS_EVENT, handler3);

						for (var k:Number=0; k < pdata.length; k++)
						{
							(pdata[k] as ProduitVO).statut=satut.ID;
						}
						ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M32', 'Statut_mis___jour'));
					}

					ressourceService.model.addEventListener(RessourceEvent.UPDATE_STATUS_EVENT, handler3);
					ressourceService.updateStatus(pdata, satut);

				}
			}
		}

		private function formaterMessageConfirmationEtat(pdata:Array):String
		{
			var message:String=ResourceManager.getInstance().getString('M32', '_tes_vous_sur_de_vouloir_changer_le_stat');
			for (var i:Number=0; i < pdata.length; i++)
			{
				message=message + "- " + (pdata[i] as ProduitVO).libelleProduit + "\n";
			}
			return message;
		}

		private function traiterChoixComboAgir(choix:Number, selectionList:Array):void
		{
			switch (choix)
			{
				case 0:
				{
					changerStatut(selectionList, new StatutVO(choix, ResourceManager.getInstance().getString('M32', 'Cl_tur_')), true);
					break;
				}

				case 1:
				{
					changerStatut(selectionList, new StatutVO(choix, ResourceManager.getInstance().getString('M32', 'Non_Trait_')), true);
					break;
				}

				case 2:
				{
					changerStatut(selectionList, new StatutVO(choix, ResourceManager.getInstance().getString('M32', 'En_cours_de_r_clamation')), true);
					break;
				}

				case 3:
				{
					afficherPopUpPasserHorsInventaire(selectionList);
					break;
				}

				case 4:
				{
					afficherPopUpPasserDansInventaire(selectionList);
					break;
				}
			}
		}

		//Activation/Desactivation du boutton rapprocher
		private function activerRapprochement(bool:Boolean):void
		{
			rapprochementEnabled=bool;
		}

		//formation du message de confirmatin pour le rapprochement
		private function formatMessageRapprochement(ressHorsInv:ProduitVO, ressDansInv:ProduitVO):String
		{
			var message:String=ResourceManager.getInstance().getString('M32', '_tes_vous_s_r_de_vouloir_rapprocher_les_') + ressHorsInv.ligne.toString() + "/" + ressHorsInv.libelleProduit + ResourceManager.getInstance().getString('M32', '__hors_inventaire_factur___net_n') + ressDansInv.ligne.toString() + "/" + ressDansInv.libelleProduit + ResourceManager.getInstance().getString('M32', '__dans_l_inventaire_et_non_factur__');

			return message;
		}


		public function createTip(event:ToolTipEvent):void
		{

			var type:String="info";

			// on créé le tooltip
			event.toolTip=new CustomToolTip(type);
			currentTooltip=event.toolTip as CustomToolTip;
			// on lui affecte le texte de la propriété tooltip de l'objet
			event.toolTip.text=(event.target as UIComponent).toolTip;

		}

		public function positionToolTip(event:ToolTipEvent):void
		{

			event.toolTip.x=(((event.target) as UIComponent) as UIComponent).contentToGlobal(new Point(event.target.x, event.target.y)).x - event.toolTip.width + 16;
			event.toolTip.y=(((event.target) as UIComponent) as UIComponent).contentToGlobal(new Point(event.target.x, event.target.y)).y - event.toolTip.height;

			if (event.toolTip.x < 0)
			{
				event.toolTip.x=0;
			}

		}
		
		private function isInArray(value:String,array:Array):Boolean
		{
			
			var verif:Boolean=false;
			
			if(array.length!=0){
				for(var i:int=0;(i<array.length)&&!verif;i++)
				{
					if(array[i]==value)
					{
						verif=true;
					}
				}
			}
			
			return verif;
			
		}

	}

}
