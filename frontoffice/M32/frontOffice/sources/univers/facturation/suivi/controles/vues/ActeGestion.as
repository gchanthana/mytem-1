package univers.facturation.suivi.controles.vues
{
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.dataGridClasses.DataGridColumn;

	public class ActeGestion extends TitleWindow
	{
		
		[Bindable]
		public var dataActeGestion:ArrayCollection = new ArrayCollection();
		
		public function ActeGestion()
		{
		}
		
		
		protected function formatDate(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{
				return DateFunction.formatDateAsString(item[column.dataField]);
			}
			else
				return "-";
			
		}
		
		protected function formateEuros(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return ConsoviewFormatter.formatEuroCurrency(item[column.dataField], 2);
			else
				return "-";
		}
		
	}
}