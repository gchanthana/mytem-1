package univers.facturation.suivi.controles.vues
{
	import composants.controls.TextInputLabeled;
	import composants.util.DateFunction;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	
	import univers.facturation.suivi.controles.vo.HistoriqueProduitInventaireVO;

	public class HistoriqueProduitInventaire extends TitleWindow
	{
		[Bindable]
		public var dataProduit:ArrayCollection = new ArrayCollection();
		
		public var dtgProduit:DataGrid;
		
		public var txtFiltreHistoriqueProduit:TextInputLabeled;
		
		public function HistoriqueProduitInventaire()
		{
		}
		
		public function filtreHistoriqueProduitChangeHandler(event:Event):void
		{
			if(dtgProduit.dataProvider != null){
				(dtgProduit.dataProvider as ArrayCollection).filterFunction = filtre;
				(dtgProduit.dataProvider as ArrayCollection).refresh();
			}
		}
		
		private function filtre(produit:HistoriqueProduitInventaireVO):Boolean
		{
			var result:Boolean = (txtFiltre(produit));
			
			return result;
		}
		
		private function txtFiltre(produit:HistoriqueProduitInventaireVO):Boolean
		{
			
			if (
				((produit.dateEffet) && (produit.dateEffet.toString().search(txtFiltreHistoriqueProduit.text.toLowerCase()) != -1))
				||
				((produit.description) && (produit.description.toLowerCase().search(txtFiltreHistoriqueProduit.text.toLowerCase()) != -1))
				||
				((produit.quand) && (produit.quand.toString().search(txtFiltreHistoriqueProduit.text.toLowerCase()) != -1))
				||
				((produit.qui) && (produit.qui.toLowerCase().search(txtFiltreHistoriqueProduit.text.toLowerCase()) != -1))
			)
				return true;
			else
				return false;
			
		}
		
		protected function formatDate(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{
				
				return DateFunction.formatDateAsString(item[column.dataField]);
			}
			else
				return "-";
			
		}
		
	}
}