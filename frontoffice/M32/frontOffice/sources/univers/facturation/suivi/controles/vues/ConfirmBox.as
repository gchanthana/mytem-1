package univers.facturation.suivi.controles.vues
{
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.Text;
	import mx.controls.TextArea;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import univers.facturation.suivi.controles.vo.Facture;
	import univers.facturation.suivi.factures.event.FactureEvent;

	public class ConfirmBox extends TitleWindow	
	{
			
		public static var OK : String = "ok";
		public static var CANCEL : String = "cancel";
				
		public var txtCommentaire : TextArea;
		public var btValider : Button;
		public var btAnnuler : Button;
		public var txtRapport : Text;
		
		private var _txRapport : String;
		
		private var _libelle : String;
		private var _facture : Facture;		
		
		private var _doFunction : Function;
		
		
		public function ConfirmBox(){
			super()			
		}
		
		public function set doFunction(f : Function):void{
			_doFunction = f;	
		}
		
		
		[Bindable]
		public function set libelle(l : String):void{
			_libelle = l;	
		}
		public function get libelle():String{
			return _libelle;	
		}
		
		[Bindable]
		public function set facture(f : Facture):void{
			_facture = f;	
		}
		public function get facture():Facture{
			return _facture;	
		}
		
		
		[Bindable]
		public function set txRapport(txt : String):void{
			_txRapport = txt;	
		}
		public function get txRapport():String{
			return _txRapport;	
		}
		
		public function get commentaire():String{
			if (this.initialized){
				return txtCommentaire.text;	
			}else{
				return null;
			}
		}
		
		
		protected function closeEventHandler(ce : CloseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		protected function btAnnulerClickHandler(me : MouseEvent):void{
			PopUpManager.removePopUp(this);	
		}		
		
		protected function btValiderClickHandler(me : MouseEvent):void{			
			_doFunction();
			PopUpManager.removePopUp(this);
		}
	}
}