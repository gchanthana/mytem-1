//***************************************************	
//	author : samuel.divioka								*
//	date   : Apr 20, 2011								*
//	owner  : Consotel								*
//***************************************************
package univers.facturation.suivi.controles.event
{
	import flash.events.Event;
	
	public class RessourceEvent extends Event
	{
		public static const UPDATE_STATUS_EVENT:String = "updateStatusEvent";
		public static const UPDATE_INVENTORY_STATE_EVENT:String = "updateInventoryStateEvent";
		public static const MERGE_PRODUCT_EVENT:String = "mergeProductEvent";
		
		public static const UPD_INV_FACTURE_SUCCES:String = 'UPD_INV_FACTURE_SUCCES';
		public static const UPD_INV_NON_FACTURE_SUCCES:String = 'UPD_INV_NON_FACTURE_SUCCES';
		
		public function RessourceEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new RessourceEvent(type, bubbles, cancelable);
		}
		
	}
}