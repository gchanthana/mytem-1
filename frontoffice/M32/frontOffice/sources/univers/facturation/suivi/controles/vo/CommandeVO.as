package univers.facturation.suivi.controles.vo
{
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;

	[Bindable]
	public class CommandeVO
	{
		
		public var numCommande:String="";
		public var refOperateur:String="";
		public var libelleCommande:String="";
		public var idTypeCommande:Number=0;
		public var refClientTxt1:String="";
		public var refClientTxt2:String="";
		public var refClientLibelle1:String="";
		public var refClientLibelle2:String="";
		public var refClientActive1:Boolean=false;
		public var refClientActive2:Boolean=false;
		public var attention:String="";
		public var dateLivraison:Date=null;
		public var commentaire:String="";
		public var idOperateur:Number=0;
		public var operateurLibelle:String="";
		public var compte:String="";
		public var sousCompte:String="";
		public var compteLibelle:String="";
		public var sousCompteLibelle:String="";
		public var distributeurLibelle:String="";
		public var transporteurID:Number=0;
		public var transporteurLibelle:String="";
		public var numeroTracking:String="";
		public var siteLivraison:String="";
		public var siteFacturation:String="";
		public var montantTotal:Number=0;
		
		public var commandHistorique:ArrayCollection = new ArrayCollection();
		
		public var listeArticles:ArrayCollection = new ArrayCollection();
		
		public var listePiecesJointes:ArrayCollection = new ArrayCollection();
				
		public function CommandeVO()
		{
		}
		
		public function addCommande(value:Object):void
		{
			this.numCommande = value.NUMERO_OPERATION;
			this.refOperateur = value.REF_REVENDEUR;
			this.libelleCommande = value.LIBELLE;
			this.idTypeCommande = value.IDTYPE_OPERATION;
			this.refClientTxt1 = value.REF_CLIENT;
			this.refClientTxt2 = value.REF_CLIENT_2;
			this.attention = value.A_L_ATTENTION;
			this.dateLivraison = value.LIVRE_LE;
			this.commentaire = value.COMMENTAIRES ;
			this.idOperateur = value.OPERATEURID;
			this.operateurLibelle = value.LIBELLE_OPERATEUR;
			this.compte = value.LIBELLE_COMPTE;
			this.sousCompte = value.LIBELLE_SOUSCOMPTE;
			this.distributeurLibelle = value.LIBELLE_REVENDEUR;
			this.transporteurID = value.IDTRANSPORTEUR;
			this.numeroTracking = value.NUMERO_TRACKING;
			this.siteLivraison = value.LIBELLE_SITELIVRAISON;
			this.siteFacturation = value.SITE_COMMUNE;
		}
		
		public function addRefLibelle(value:Object):void
		{
			this.refClientLibelle1 = value.champ1_libelle;
			if(value.champ1_actif==1){
				this.refClientActive1 = true;
			}
			this.refClientLibelle2 = value.champ2_libelle;
			if(value.champ2_actif==1){
				this.refClientActive2 = true;
			}
		}
		
		public function addCompteLibelle(value:Object):void
		{
			this.compteLibelle = value.COMPTE;
			this.sousCompteLibelle = value.SOUS_COMPTE;
		}
		
		public function addFournisseurLibelle(value:Object):void
		{
			for(var i:int=0;i<value.length;i++){
				if(value[i].IDTRANSPORTEUR==this.transporteurID){
					this.transporteurLibelle = value[i].LIBELLE_TRANSPORTEUR;
				}
			}
		}
		
	}
}