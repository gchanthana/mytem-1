package univers.facturation.suivi.controles.services
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	import univers.facturation.suivi.controles.vo.InventaireVO;

	[Bindable]
	public class ExportService
	{
		
		public var handler:ExportHandler;
		
		public function ExportService()
		{
			handler = new ExportHandler();
		}
		
		public function getExportHIF(paramInventaire:InventaireVO,format:String):void
		{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.export.Export",
				"getExportHIF",
				handler.getExportInventaireHandler);
			RemoteObjectUtil.callService(opData,format,paramInventaire.idInventaire,paramInventaire.effectif);
			
		}
		
		public function getExportINF(paramInventaire:InventaireVO,format:String):void
		{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.export.Export",
				"getExportINF",
				handler.getExportInventaireHandler);
			RemoteObjectUtil.callService(opData,format,paramInventaire.idInventaire,paramInventaire.effectif,paramInventaire.solution);
			
		}
		
	}
}