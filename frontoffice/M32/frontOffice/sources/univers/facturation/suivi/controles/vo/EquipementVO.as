package univers.facturation.suivi.controles.vo
{
	/**
	 * <b>Class <code>EquipementVO</code></b>
	 * <p><pre>
	 * <u>Description :</u>
	 * Value Object représentant les propriétés pour un équipement.
	 * 
	 * Auteur	: Guiou Nicolas
	 * Version 	: 1.0
	 * Date 	: 15.07.2010
	 * </pre></p>
	 *
	 */
	[Bindable]
	public class EquipementVO
	{
		public var idEquipement			: int 				= 0; 
		public var libelleEquipement	: String			= ""; 
		
		public var idTypeEquipement		: int				= 0;
		public var typeEquipement		: String			= "";
		
		public var collabRattache		: CollaborateurVO	= null;
		
	}
}