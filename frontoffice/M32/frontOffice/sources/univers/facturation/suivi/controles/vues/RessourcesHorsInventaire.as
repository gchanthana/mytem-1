package univers.facturation.suivi.controles.vues
{
	
	import mx.resources.ResourceManager;
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.HBox;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.utils.ObjectUtil;
	
	import univers.facturation.suivi.controles.controls.AbstractControleFacture;
	
	[Bindable]
	public class RessourcesHorsInventaire extends HBox
	{
		
		public var lblQuantiteAbos : Label;
		public var lblVolumeConsos : Label;
		public var lblMontantAbos : Label;
		public var lblMontantConsos : Label; 
		
		
		
		public var btExpAbos : Button;
		public var btExpConsos : Button;
		public var btExpTout : Button;
		public var txtFiltre : TextInputLabeled;
		
		public var ckbAbos : CheckBox;
		public var ckbConsos : CheckBox;
		public var imgCSV : Image;
		
		
		private var _helpMessage : String = ResourceManager.getInstance().getString('M32', 'Il_s_agit_des_produits_de_la_facture_qui');
		public function get helpMessage():String{
			return _helpMessage;
		}
		public function set helpMessage(message : String):void{
			_helpMessage = message;
		}
		
		public function RessourcesHorsInventaire()
		{	
			//TODO: implement function
			super();
		}
		
		
		
		protected function formateDates(item : Object, column : DataGridColumn):String{			
			if (item[column.dataField] != null){
				var ladate:Date = new Date(item[column.dataField]);
				return DateFunction.formatDateAsShortString(ladate);				
			}else return "-";
		}
		
		protected function formateEuros(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2);	
		}
		
		protected function formateType(item : Object, column : DataGridColumn):String{
			if(String(item[column.dataField]).toLowerCase().search("abo") != -1) return "Abo"
			else return "Conso"
		}
		
		protected function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}
		
		protected function formateNumber(item : Object, column : DataGridColumn):String{
			return ConsoviewFormatter.formatNumber(Number(item[column.dataField]),2);
		}
		
		protected function formateEurosP4(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],4);	
		}
		
		protected function dateEntreeSortCompareFunction(itemA:Object, itemB:Object):int{
			return ObjectUtil.dateCompare(itemA.DATE_ENTREE,itemB.DATE_ENTREE);
		}
		
		protected function dateSortieSortCompareFunction(itemA:Object, itemB:Object):int{
			return ObjectUtil.dateCompare(itemA.DATE_SORTIE,itemB.DATE_SORTIE);
		}
		
		private var _ctrlFactureApp : AbstractControleFacture;		
		public function set ctrlFactureApp(ctrl : AbstractControleFacture):void{
			_ctrlFactureApp = ctrl;		
		}
		public function get ctrlFactureApp():AbstractControleFacture{
			return _ctrlFactureApp;
		}
		
		protected function imgCSVClickHandler(me : MouseEvent):void{
			if(ctrlFactureApp.listeRessourcesHInv.length > 0){
				if(ckbAbos.selected && ckbConsos.selected){
					ctrlFactureApp.exportContent("HORS_INVENTAIRE");
					//ctrlFactureApp.exporterDetailRessourcesHorsInventaire();
					return;
				}else if (ckbAbos.selected){
					ctrlFactureApp.exportContent("HORS_INVENTAIRE",AbstractControleFacture.TYPE_THEME_ABO);
					//ctrlFactureApp.exporterDetailRessourcesHorsInventaire(AbstractControleFacture.TYPE_THEME_ABO);
					return;
				}else if (ckbConsos.selected){
					ctrlFactureApp.exportContent("HORS_INVENTAIRE",AbstractControleFacture.TYPE_THEME_CONSO);
					//ctrlFactureApp.exporterDetailRessourcesHorsInventaire(AbstractControleFacture.TYPE_THEME_CONSO);
					return;
				}else{
					
				}
			}
		}
			
		
		protected function txtFiltreChangeHandler(ev : Event):void{
			if (ctrlFactureApp.listeRessourcesHInv != null){
				ctrlFactureApp.listeRessourcesHInv.filterFunction = filtrerLeGrid;
				ctrlFactureApp.listeRessourcesHInv.refresh();
			}
		}
		
		protected function ckbAbosClickHandler(ev : Event):void{
			txtFiltreChangeHandler(ev);
		}
		
		protected function ckbConsosClickHandler(ev : Event):void{
			txtFiltreChangeHandler(ev);
		}
		
		//Verifie si le text entree dans le textInput correspond
		private function isTxtMatching(item : Object):Boolean{
			if ((item.TYPE_THEME.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
			   	||
			   (item.SOUS_TETE.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
			   	||
			   (item.LIBELLE_PRODUIT.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
			   	||
			   (item.SOUS_COMPTE.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
			   	||   	
			   (item.MONTANT.toString().toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)){
			   	return true;			   	
		   	}else{
		   		return false;
		   	}
		}
		
		private function filtrerLeGrid(item : Object):Boolean{
		 	if (ckbAbos.selected && ckbConsos.selected && isTxtMatching(item)) return true;
			if ((ckbAbos.selected)&&(isTxtMatching(item))&&(item.TYPE_THEME == AbstractControleFacture.TYPE_THEME_ABO)) return true;			
		   	if ((ckbConsos.selected)&&(isTxtMatching(item))&&(item.TYPE_THEME == AbstractControleFacture.TYPE_THEME_CONSO)) return true;
		   	return false;
		}
		
		
		
	}
}