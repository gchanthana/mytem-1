package univers.facturation.suivi.controles.vues
{
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	
	import mx.containers.VBox;
	import mx.controls.dataGridClasses.DataGridColumn;
	
	import univers.facturation.suivi.controles.vo.CommandeVO;

	public class DetailsItemREnderer extends VBox
	{
		
		[Bindable]
		public var commande:CommandeVO = new CommandeVO();
		
		[Bindable]
		public var total:Number = 0;
		
		[Bindable]
		public var ConfigNumero:int = 0;
		
		public function DetailsItemREnderer()
		{
		}
		
		public function creationCompleteHandler():void
		{
			if(data != null)
			{
				for(var i:int = 0; i < data.ITEMS.length;i++)
				{
					total = total + Number(data.ITEMS[i].TOTAL);
				}
				ConfigNumero++;
			}
		}
		
		public function ConfigNumeroNumberReturn():String
		{
			return ConfigNumero.toString();
		}
		
		public function btnSuppConfigClickHandler():void
		{
			if(data != null)
			{
				dispatchEvent(new Event("eraseThisConfig",true));
			}
		}
		
		public function formatTotal(totalNbr:Object, column:DataGridColumn):String
		{
			var field:String =  column.dataField;
			
			if(totalNbr[field] == " - ")
			{
				return "n.c";
			}
			if(totalNbr[field] > 0)
			{
				return "";
			}
			if(totalNbr[field] == 0)
			{
				return "";
			}
			return ConsoviewFormatter.formatEuroCurrency(totalNbr[field],2);
		}
		
		public function formatTotal2(totalNbr:Object, column:DataGridColumn):String
		{
			var field:String =  column.dataField;
			
			
			if(totalNbr[field] == " - ")
			{
				return "";
			}
			if(totalNbr[field] == 0)
			{
				if(totalNbr.CODE_IHM == "Accessoire" || totalNbr.CODE_IHM == "Mobile")
				{
					return ConsoviewFormatter.formatEuroCurrency(totalNbr[field],2);
				}
				else
				{
					return " - " ;
				}
			}
			else
			{
				return ConsoviewFormatter.formatEuroCurrency(totalNbr[field],2);
			}
		}
		
		public function formatTotalPrix(totalNbr:Number):String
		{
			return ConsoviewFormatter.formatEuroCurrency(totalNbr,2);
		}
		
	}
}