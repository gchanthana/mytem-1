package univers.facturation.suivi.controles.vues {

    // myComponents/CellField.as
    import mx.controls.*;
    import mx.controls.dataGridClasses.DataGridListData;   
   

    public class EtatFactureItemRenderer extends Image
    {
    	[Embed(source="/assets/images/delete2.png",mimeType='image/png')]
		private var imgFalse :Class;
		[Embed(source="/assets/images/check2.png",mimeType='image/png')]
		private var imgTrue  :Class;
				
		
        
        // Define the constructor and set properties.
        public function EtatFactureItemRenderer() {
        	
            super();            
           	setStyle("horizontalAlign","center");
           	setStyle("verticalAlign","middle");            	
        }

        // Override the set method for the data property.
        override public function set data(value:Object):void {
            super.data = value;            
       
            if (value != null)
            {
            	var valeur : Number = Number(value[DataGridListData(listData).dataField]);
                 
                if(valeur > 0)
                {   	
                	source = imgTrue;                	
                	
                }else{
                	source = imgFalse;
                }
               width = 12;
               height = 12; 
            }
            super.invalidateDisplayList();
        }
    }
}

