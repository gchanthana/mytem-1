package univers.facturation.suivi.controles.vues
{
	import mx.containers.HBox;
	
	import univers.facturation.suivi.controles.vo.CommandeVO;

	public class CommandeGenerale extends HBox
	{
		[Bindable]
		public var commande:CommandeVO = new CommandeVO();
		
		public function CommandeGenerale()
		{
		}
		
	}
}