package univers.facturation.suivi.controles.event
{
	import flash.events.Event;

	public class HistoriqueActionRessourceEvent extends Event
	{
		
		public static var HISTORIQUEACTIONRESSOURCE_COMPELTE:String = "HitoriqueComplete"; 
		
		public function HistoriqueActionRessourceEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new HistoriqueActionRessourceEvent(type,bubbles,cancelable);
		}
		
	}
}