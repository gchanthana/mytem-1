package univers.facturation.suivi.controles.services
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	[Bindable]
	public class HistoriqueActionRessourceService
	{
		
		public var model:HistoriqueActionRessourceModel;
		public var handler:HistoriqueActionRessourceHandler;
		
		public function HistoriqueActionRessourceService()
		{
			model = new HistoriqueActionRessourceModel();
			handler = new HistoriqueActionRessourceHandler(model);
		}
		
		public function getHistoriqueInventaire(idInventaireProduit:Number):void {
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.GetHistoriqueActionRessource",
				"getHistoriqueActionRessource",
				handler.getHistoriqueActionRessourceHandler);
			RemoteObjectUtil.callService(op, idInventaireProduit);
		}
		
	}
}