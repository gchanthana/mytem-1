package univers.facturation.suivi.controles.vo
{
	//[RemoteClass(alias="fr.consotel.consoview.facturation.suiviFacturation.controles.GroupeProduitControle")]
	 
	[Bindable]
	public class GroupeProduitControle
	{

		public var id:Number = 0;
		public var libelle:String = "";
		public var segment:String = "";
		public var operateurId:Number = 0;
		public var operateurLibelle:String = "";
		public var poids:Number = 0;
		public var pourControle:Boolean = false;
		public var prixUnitaire:Number = 0;
		public var prixRemise:Number = 0;
		public var montant:Number = 0;
		public var remiseContrat:Number = 0;
		public var dateDebut:Date = null;
		public var dateFin:Date = null;
		public var nbVersions:Number = 0;
		public var type:String = "Abo";


		public function GroupeProduitControle()
		{
		}

	}
}