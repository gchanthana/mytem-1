package univers.facturation.suivi.controles.event
{
	import flash.events.Event;

	public class InventaireEvent extends Event
	{
		
		public static var INVENTAIRE_FACTURE_COMPELTE:String = "InventaireFactureComplete"; 
		public static var INVENTAIRE_NON_FACTURE_COMPELTE:String = "InventaireNonFactureComplete"; 
		
		public function InventaireEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new InventaireEvent(type,bubbles,cancelable);
		}
		
	}
}