package univers.facturation.suivi.controles.vues
{
	import mx.resources.ResourceManager;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparcmobile.GestionParcMobileImpl;
	import gestionparcmobile.event.ContentFicheEvent;
	import gestionparcmobile.ihm.fiches.comp.combobox.AutoSelectComboBox;
	import gestionparcmobile.ihm.fiches.comp.combobox.AutoSelectComboboxEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.Text;
	import mx.events.ListEvent;
	
	[Bindable]
	public class ContentTabNavSite extends VBox
	{
		public var cb_site:AutoSelectComboBox;
		public var txt_adresse:Text;
		public var lb_code_postal:Label;
		public var lb_ville:Label;
		public var lb_pays:Label;
		
		public var bt_save:Button;
		
		public var selectedID:Number= -1;
		
		private var _idSitePhysique:Number;
		
		public static const MODIFICATION_INFOS_SITE:String = "modificationInfosSite";
		
		public function ContentTabNavSite()
		{
			super();
		}
		
		
		
		public function itemAutoSelectedEventHandler(evt:AutoSelectComboboxEvent):void {
			//enregistrer l'id du site physique
			_idSitePhysique = evt.currentTarget.selectedItem.IDSITE_PHYSIQUE;
			
			adresse = evt.currentTarget.selectedItem.SP_ADRESSE1;
			codePostal = evt.currentTarget.selectedItem.SP_CODE_POSTAL;
			ville = evt.currentTarget.selectedItem.SP_COMMUNE;
			
		}
		
		public function cbListSiteChangeHandler(evt:ListEvent):void {
			if(evt.currentTarget.selectedItem)
			{
				//enregistrer l'id du site physique			
				_idSitePhysique = evt.currentTarget.selectedItem.IDSITE_PHYSIQUE;
				
				adresse = evt.currentTarget.selectedItem.SP_ADRESSE1;
				codePostal = evt.currentTarget.selectedItem.SP_CODE_POSTAL;
				ville = evt.currentTarget.selectedItem.SP_COMMUNE;
			
				buttonEnabled = true;
				this.dispatchEvent(new Event(MODIFICATION_INFOS_SITE));
			}
		}
		
		public function clickButtonHandler(evt:MouseEvent):void {
			checkAffectationSite();
		}
		
		
		public function set buttonEnabled(value:Boolean):void {
			bt_save.enabled = value;
		}
		
		public function get buttonEnabled():Boolean {
			return bt_save.enabled;
		}
		
		private function checkAffectationSite():void {
			if(adresse == "" && codePostal == "" && ville == "") {
				Alert.show(ResourceManager.getInstance().getString('M11', 'Vous_devez_s_lectionner_un_site_'), ResourceManager.getInstance().getString('M11', 'Attention__'));
			}
			else {
				if(GestionParcMobileImpl.ClassFiche.NAV_SELECTED)
				{
					this.dispatchEvent(new ContentFicheEvent(ContentFicheEvent.ENREGISTRER_SITE, true, GestionParcMobileImpl.ClassFiche.NAV_SELECTED, _idSitePhysique));
				}
			}
			
		}
		
		
		//////////////////////////////////////////////////////
		//
		//		GETTERS / SETTERS
		//
		//////////////////////////////////////////////////////
		
		public function set listSites(value:ArrayCollection):void {
			cb_site.dataProvider = value;
		}
		
		public function get listSites():ArrayCollection {
			return cb_site.dataProvider as ArrayCollection;
		}
		
		public function set adresse(value:String):void {
			txt_adresse.text = value;
		}
		
		public function set codePostal(value:String):void {
			lb_code_postal.text = value;
		}
		
		public function set ville(value:String):void {
			lb_ville.text = value;
		}
		
		public function set pays(value:String):void {
			lb_pays.text = value;
		}
		
		public function get adresse():String {
			return txt_adresse.text;
		}
		
		public function get codePostal():String {
			return lb_code_postal.text;
		}
		
		public function get ville():String {
			return lb_ville.text;
		}
		
		public function get pays():String {
			return lb_pays.text;
		}
		
		public function get idSitePhysique():Number {
			return _idSitePhysique;
		}
		
	}
}