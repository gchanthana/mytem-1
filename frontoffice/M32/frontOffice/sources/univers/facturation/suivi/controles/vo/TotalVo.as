package univers.facturation.suivi.controles.vo
{
	[Bindable]
	public class TotalVo
	{
		
		public var ligne:String='0';
		public var montant:String='0';
		
		public function TotalVo()
		{
		}
					
		public function changeTotal(value:Object):void{
			this.ligne=value.QTE_TOTAL;
			this.montant=value.MONTANT_TOTAL;
		}
		
	}
}