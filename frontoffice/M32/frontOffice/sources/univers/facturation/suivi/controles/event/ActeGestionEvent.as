package univers.facturation.suivi.controles.event
{
	import flash.events.Event;

	public class ActeGestionEvent extends Event
	{
		public static var ACTEGESTION_COMPELTE:String = "ActeGestionComplete"; 
		
		public function ActeGestionEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ActeGestionEvent(type,bubbles,cancelable);
		}
	}
}