package univers.facturation.suivi.controles.services
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import univers.facturation.suivi.controles.event.HistoriqueActionRessourceEvent;
	import univers.facturation.suivi.controles.vo.HistoriqueProduitInventaireVO;

	[Bindable]
	public class HistoriqueActionRessourceModel extends EventDispatcher
	{
		
		public var historique:ArrayCollection;
		
		public function HistoriqueActionRessourceModel()
		{
		}
		
		public function updateHistoriqueActionRessource(values:ArrayCollection):void
		{
			
			historique = new ArrayCollection();
			
			var histo:HistoriqueProduitInventaireVO;
			for(var i:int=0;i<values.length;i++){
				histo = new HistoriqueProduitInventaireVO();
				histo.fillHistorique(values[i]);
				historique.addItem(histo);
			}
			
			dispatchEvent(new HistoriqueActionRessourceEvent(HistoriqueActionRessourceEvent.HISTORIQUEACTIONRESSOURCE_COMPELTE));
			
		}
	}
}