package univers.facturation.suivi.controles.services
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	public class ActeGestionHandler
	{
		
		private var _model:ActeGestionModel;
		
		public function ActeGestionHandler(model:ActeGestionModel):void
		{
			_model = model;
		}
		
		public function getActeGestionHandler(event:ResultEvent):void
		{
			_model.updateActeGestion(event.result as ArrayCollection);
		}
		
	}
	
}