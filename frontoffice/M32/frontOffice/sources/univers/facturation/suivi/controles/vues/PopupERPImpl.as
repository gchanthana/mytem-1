package univers.facturation.suivi.controles.vues
{
	import mx.resources.ResourceManager;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.TextArea;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.facturation.suivi.controles.controls.AbstractControleFacture;
	import univers.facturation.suivi.controles.controls.ConfigButtons;
	import univers.facturation.suivi.controles.controls.formatsclients.LogControles;
	import univers.facturation.suivi.controles.vo.Facture;

	public class PopupERPImpl extends TitleWindow
	{
		private var _cbtn : ConfigButtons;
		private var _lgCtrles : LogControles;
		public var _dgFactures : DataGrid;
		private var _facture : Facture;
		private var _ctrlFactures : AbstractControleFacture;
		private var facture : Facture;
		
		public var cmtr_DecisionERP : TextArea;
		public var btnExporter_DecisionERP : Button;
		public var btnNePasExporter_DecisionERP : Button;

		public function PopupERPImpl()
		{
			super();
		}
		
		protected function Close(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		private var _signature : String;
		public function get signature(): String
		{
			return _signature;
		} 
		
		public function set signature(signed : String):void
		{
			_signature = signed;
		}
		
		protected function init(fe : FlexEvent):void
		{
			btnExporter_DecisionERP.enabled = ConfigButtons.btnExporter;
			btnNePasExporter_DecisionERP.enabled = ConfigButtons.btnNePasExporter;
		}
		
		//Permet de récupérer des 'Object' sans en créer un autre
		public function getsObject(lgCtrles:LogControles,fact:Facture,dgfact:DataGrid,ctrlFact:AbstractControleFacture):void
		{
			_lgCtrles = lgCtrles;
			_dgFactures = dgfact;
			_facture = fact;
			_ctrlFactures = ctrlFact;
		}
				
		protected function btnExporter_DecisionERPClickHandler(me :MouseEvent):void
		{
//			_cbtn.btnExporteOrNot = 1;
			btnExporterHandler();
			PopUpManager.removePopUp(this);
		}
		
		protected function btnNePasExporter_DecisionERPClickHandler(me :MouseEvent):void
		{
//			_cbtn.btnExporteOrNot = 0;
			btnNePasExporterHandler();
			PopUpManager.removePopUp(this);
		}
		
		protected function btAnnuler_DecisionERPClickHandler(me :MouseEvent):void
		{
//			_cbtn.btnExporteOrNot = -1;
			PopUpManager.removePopUp(this);
		}
		
		protected function  btnExporterHandler():void
		{ 	
			if(_dgFactures != null)
			{
				facture = _dgFactures.selectedItem as Facture;			
				facture.exportee = 2;
				executerBtExporterClick(ResourceManager.getInstance().getString('M32', 'Facture_export_'), 1);
			}else{Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'),"Consoview")};
		}
		
		protected function btnNePasExporterHandler():void
		{ 	
			if(_dgFactures != null)
			{
				facture = _dgFactures.selectedItem as Facture;
				facture.exportee = 3;
				executerBtExporterClick(ResourceManager.getInstance().getString('M32', 'Facture_non_export_'),0);
			}else{Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'),"Consoview")};
		}
		
		//Execute l'action du click sur le bouton 'Exporter' apres confirmation et envoie le log
		private function executerBtExporterClick(whatIsThisButton:String, etatExport:int):void{
				var facture : Facture = _dgFactures.selectedItem as Facture;
				_facture.commentaireExporter = "\n" + whatIsThisButton +  signature;
				_ctrlFactures.selectedFacture = facture;
				_ctrlFactures.updateVisaExporteFacture();
				_lgCtrles.setDecisionExport((_dgFactures.selectedItem as Facture).periodeId,etatExport, cmtr_DecisionERP.text);
				_lgCtrles.setLogsControleFacture((_dgFactures.selectedItem as Facture).periodeId,whatIsThisButton, cmtr_DecisionERP.text);
		}
	}	
}