package univers.facturation.suivi.controles.services
{
	import flash.events.EventDispatcher;
	
	import univers.facturation.suivi.controles.event.FicheLigneEvent;
	import univers.facturation.suivi.controles.vo.LigneVO;

	[Bindable]
	public class FicheLigneModel extends EventDispatcher
	{	
		private var _ficheLigne:LigneVO;
		
		public function FicheLigneModel()
		{
			ficheLigne = new LigneVO();
		}
		
		public function get ficheLigne():LigneVO
		{
			return _ficheLigne;
		}

		protected function set ficheLigne(value:LigneVO):void
		{
			_ficheLigne = value;
		}

		public function updateFicheLigne(obj:Object):void
		{	
			ficheLigne = ficheLigne.fillLigneVO(obj);
			dispatchEvent(new FicheLigneEvent(FicheLigneEvent.FICHELIGNE_COMPELTE));		
		}
		
		public function updateLibellePerso(obj:Object):void
		{
			ficheLigne.addLibellePerso(obj);
			dispatchEvent(new FicheLigneEvent(FicheLigneEvent.LIBELLEPERSO_COMPELTE));
		}
		
	}
}