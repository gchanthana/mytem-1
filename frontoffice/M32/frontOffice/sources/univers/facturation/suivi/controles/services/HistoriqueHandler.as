package univers.facturation.suivi.controles.services
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	public class HistoriqueHandler
	{
			
		private var _model:HistoriqueModel;
		
		public function HistoriqueHandler(model:HistoriqueModel):void
		{
			_model = model;
		}
		
		public function getHistoriqueHandler(event:ResultEvent):void
		{
			_model.updateHistorique(event.result as ArrayCollection);
		}
			
	}
}