package univers.facturation.suivi.controles.vo
{
	public class CommandeHistoriqueVO
	{
		
		public var actionLibelle:String="";
		public var actionDate:Date=null;
		public var nom:String="";
		public var etat:String="";
		
		public function CommandeHistoriqueVO()
		{
		}
		
		public function addCommandeHistorique(value:Object):void
		{
			this.actionLibelle = value.LIBELLE_ACTION;
			this.actionDate = value.DATE_ACTION;
			this.nom = value.NOM;
			this.etat = value.LIBELLE_ETAT;
		}
		
	}
}