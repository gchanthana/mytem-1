package univers.facturation.suivi.controles.controls
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class ConfigButtons extends EventDispatcher
	{

		public function ConfigButtons()
		{}
		
		//Appel de procédure permettant de récupérer l'acces aux boutons suivant l'utilisateur connecté
		public function getEtatButtons(idPeriode:int,idProfile:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
										"fr.consotel.consoview.facturation.suiviFacturation.controles.LogsControles",
										"getEtatButtons",
										zgetEtatButtonsResultHandler);
		
			RemoteObjectUtil.callService(op,idProfile,idPeriode);
		}
		
		//Affecte l'état des boutons après en avoir validé un
		private function zgetEtatButtonsResultHandler(re:ResultEvent):void
		{	
			var i:int=0;	
								
			if(re.result!=null)
			{
				for(i = 0;i < 13;i++)
				{
					zAttributeActionButtons(re.result[i].CODE,Number(re.result[i].ETAT));
				}
				
			}
			
			dispatchEvent(new Event("goToActiveBouttons"));
		}
		
		//Attribut à chaque boutons sont état
		private function zAttributeActionButtons(btnNumber:String,IsActive:Number):void
		{
			var ok:Boolean = false;
			if(IsActive != 0)
			{
				ok = true; 
			}
			
			switch (btnNumber){
				
					case "B1":
					{
						ConfigButtons.btnViserFacturePapier = ok;
						
						break;
					}
					case "B2":
					{
						ConfigButtons.btnAnnulerLeVisaPapier = ok;
						
						break;
					}
					
					case "B3":
					{
						ConfigButtons.btnValiderControle = ok;
						
						break;
					}
					case "B4":
					{
						ConfigButtons.btnAnnulerControle = ok;
						
						break;
					}
					
					case "B5":
					{
						ConfigButtons.btnViserAnalytique = ok;
						
						break;
					}
					case "B6":
					{
						ConfigButtons.btnEditerAnalytique = ok;
						
						break;
					}
					case "B7":
					{
						ConfigButtons.btnAnnulerVisaAnalytique = ok;
						
						break;
					}
					
					case "B8":
					{
						ConfigButtons.btnExporter = ok;
						
						break;
					}
					case "B9":
					{
						ConfigButtons.btnNePasExporter = ok;
						
						break;
					}
					case "B10":
					{
						ConfigButtons.btnDecisionERP = ok;
						
						break;
					}
					case "B11":
					{
						ConfigButtons.btnAnnulerDecisionERP = ok;
						
						break;
					}
					
					case "B12":
					{
						ConfigButtons.btnValiderControleInv = ok;
						
						break;
					}
					case "B13":
					{
						ConfigButtons.btnAnnulerControleInv = ok;
						
						break;
					}
					
					default:
					{
						ConfigButtons.btnViserFacturePapier = 
						ConfigButtons.btnAnnulerLeVisaPapier = 
						ConfigButtons.btnViserAnalytique = 
						ConfigButtons.btnEditerAnalytique = 
						ConfigButtons.btnAnnulerVisaAnalytique = 
						ConfigButtons.btnValiderControle =
						ConfigButtons.btnAnnulerControle = 
						ConfigButtons.btnDecisionERP = 
						ConfigButtons.btnExporter = 
						ConfigButtons.btnNePasExporter = 
						ConfigButtons.btnAnnulerDecisionERP = 
						ConfigButtons.btnValiderControleInv = 
						ConfigButtons.btnAnnulerControleInv = true;
						
						break;
					}
						
				
			}
			
			trace(btnNumber,IsActive,ok);
		}
		
//		//Permet de récupérer l'état du bouton 'Exporter' et 'Ne pas exporter' : 0-> 'Ne pas exporter' 1-> 'Exporter'
//		private var _btnExporteOrNot : int;
//		public function get btnExporteOrNot(): int
//		{
//			return _btnExporteOrNot;
//		} 
//		
//		public function set btnExporteOrNot(ExporteOrNot : int):void
//		{
//			_btnExporteOrNot = ExporteOrNot;
//		}
		
		static private var _btnViserFacturePapier:Boolean = false;
		static public  function get btnViserFacturePapier():Boolean
		{
			return _btnViserFacturePapier;
		}
		
		static public function set btnViserFacturePapier(btnValue:Boolean):void
		{
			_btnViserFacturePapier=	btnValue;
		}
		
		static private var _btnAnnulerLeVisaPapier:Boolean = false;
		static public  function get btnAnnulerLeVisaPapier():Boolean
		{
			return _btnAnnulerLeVisaPapier;
		}
		
		static public function set btnAnnulerLeVisaPapier(btnValue:Boolean):void
		{
			_btnAnnulerLeVisaPapier = btnValue;
		}
		
		static private var _btnViserAnalytique:Boolean = false;
		static public  function get btnViserAnalytique():Boolean
		{
			return _btnViserAnalytique;
		}
		
		static public function set btnViserAnalytique(btnValue:Boolean):void
		{
			_btnViserAnalytique = btnValue;
		}
		
		static private var _btnEditerAnalytique:Boolean = false;
		static public  function get btnEditerAnalytique():Boolean
		{
			return _btnEditerAnalytique;
		}
		
		static public function set btnEditerAnalytique(btnValue:Boolean):void
		{
			_btnEditerAnalytique = btnValue;
		}
		
		static private var _btnAnnulerVisaAnalytique:Boolean = false;
		static public  function get btnAnnulerVisaAnalytique():Boolean
		{
			return _btnAnnulerVisaAnalytique;
		}
		
		static public function set btnAnnulerVisaAnalytique(btnValue:Boolean):void
		{
			_btnAnnulerVisaAnalytique = btnValue;
		}
		
		static private var _btnValiderControle:Boolean = false;
		static public  function get btnValiderControle():Boolean
		{
			return _btnValiderControle;
		}
		
		static public function set btnValiderControle(btnValue:Boolean):void
		{
			_btnValiderControle = btnValue;
		}
		
		static private var _btnAnnulerControle:Boolean = false;
		static public  function get btnAnnulerControle():Boolean
		{
			return _btnAnnulerControle;
		}
		
		static public function set btnAnnulerControle(btnValue:Boolean):void
		{
			_btnAnnulerControle = btnValue;
		}
		
		static private var _btnValiderControleInv:Boolean = false;
		static public  function get btnValiderControleInv():Boolean
		{			
			return _btnValiderControleInv;
		}
		
		static public function set btnValiderControleInv(btnValue:Boolean):void
		{
			_btnValiderControleInv = btnValue;
		}
		
		static private var _btnAnnulerControleInv:Boolean = false;
		static public  function get btnAnnulerControleInv():Boolean
		{
			return _btnAnnulerControleInv;
		}
		
		static public function set btnAnnulerControleInv(btnValue:Boolean):void
		{
			_btnAnnulerControleInv = btnValue;
		}
		
		static private var _btnDecisionERP:Boolean = false;
		static public  function get btnDecisionERP():Boolean
		{
			return _btnDecisionERP;
		}
		
		static public function set btnDecisionERP(btnValue:Boolean):void
		{
			_btnDecisionERP = btnValue;
		}
		
		static private var _btnAnnulerDecisionERP:Boolean = false;
		static public  function get btnAnnulerDecisionERP():Boolean
		{
			return _btnAnnulerDecisionERP;
		}
		
		static public function set btnAnnulerDecisionERP(btnValue:Boolean):void
		{
			_btnAnnulerDecisionERP = btnValue;
		}
		
			
		static private var _btnExporter:Boolean = false;
		static public  function get btnExporter():Boolean
		{
			return _btnExporter;
		}
		
		static public function set btnExporter(btnValue:Boolean):void
		{
			_btnExporter = btnValue;
		}
		
		static private var _btnNePasExporter:Boolean = false;
		static public  function get btnNePasExporter():Boolean
		{
			return _btnNePasExporter;
		}

		static public function set btnNePasExporter(btnValue:Boolean):void
		{
			_btnNePasExporter = btnValue;
		}
	}
}