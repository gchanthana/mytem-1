package univers.facturation.suivi.controles.vues
{
	import composants.Format;
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Panel;
	import mx.containers.TabNavigator;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.managers.PopUpManagerChildList;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import univers.facturation.grilletarif.entity.GroupeProduitVO;
	import univers.facturation.grilletarif.event.GrilleTarifEvent;
	import univers.facturation.grilletarif.ihm.EditionVersionTarifIHM;
	import univers.facturation.grilletarif.ihm.ProduitOfGroupeIHM;
	import univers.facturation.grilletarif.services.GrilleTarifService;
	import univers.facturation.grilletarif.utils.DataGridDataExporter;
	import univers.facturation.suivi.controles.controls.AbstractControleFacture;
	import univers.facturation.suivi.controles.controls.formatsclients.LogControl;
	import univers.facturation.suivi.controles.controls.formatsclients.LogControles;
	import univers.facturation.suivi.controles.vo.LigneFacturation;
	

	public class ControleDeLaFacture extends Panel
	{
		public static const VALIDER : String = ResourceManager.getInstance().getString('M32', 'valider');
		public static const RETOUR : String = ResourceManager.getInstance().getString('M32', 'retour');
		
		public var dgTarifsAbos : BackGroundAdvancedDataGrid;
		public var dgTarifsConsos : BackGroundAdvancedDataGrid;
		
		public var lblClipBoard : Label;
		
		public var A1 : AdvancedDataGridColumn;
		public var A2 : AdvancedDataGridColumn;
		public var A3 : AdvancedDataGridColumn;
		public var A4 : AdvancedDataGridColumn;
		public var A5 : AdvancedDataGridColumn;
		public var A6 : AdvancedDataGridColumn;
		public var A7 : AdvancedDataGridColumn;
		public var A8 : AdvancedDataGridColumn;
		public var A9 : AdvancedDataGridColumn;
		
		public var C1 : AdvancedDataGridColumn;
		public var C2 : AdvancedDataGridColumn;
		public var C3 : AdvancedDataGridColumn;
		public var C4 : AdvancedDataGridColumn;
		public var C5 : AdvancedDataGridColumn;
		public var C6 : AdvancedDataGridColumn;
		public var C7 : AdvancedDataGridColumn;
		public var C8 : AdvancedDataGridColumn;
		public var C9 : AdvancedDataGridColumn;
		
		public var T1 : AdvancedDataGridColumn;
		public var T5 : AdvancedDataGridColumn;
		public var T6 : AdvancedDataGridColumn;
		public var T7 : AdvancedDataGridColumn;
		public var T8 : AdvancedDataGridColumn;
		public var T9 : AdvancedDataGridColumn;
		
		public var btValider : Button;
		public var btRetour : Button;
		public var btJustificatif : Button;
		
		public var txtFiltreLignesFactAbos : TextInputLabeled;
		public var txtFiltreLignesFactConsos : TextInputLabeled;
		
		public var cpHorsInventaire : RessourcesHorsInventaireIHM;
		public var cpInventaire : RessourcesInventaireIHM;
		public var tnInventaire : TabNavigator;
		
		public var lgCtrl:LogControl;
		public var lgCtrles:LogControles;
		
		private var ctrl : AbstractControleFacture;
		
		private var _btValiderEnabled:Boolean = false;
		
		
		public function ControleDeLaFacture()
		{
			//TODO: implement function
			super();
			addEventListener(GrilleTarifEvent.SHOW_DETAIL_TARIF,showDetailTarifHandler);
		}
		
		
		
		[Bindable]
		public function get btValiderEnabled():Boolean
		{
			return _btValiderEnabled;
		}

		public function set btValiderEnabled(value:Boolean):void
		{
			_btValiderEnabled = value;
		}

		private function showDetailTarifHandler(evt : GrilleTarifEvent):void
		{
			var gp : GroupeProduitVO = new GroupeProduitVO;
			gp.id_groupe_produit = evt.objectReturn.id;
			gp.libelle = evt.objectReturn.libelleProduit;
			gp.isPublic = (evt.objectReturn.boolPublic == 1);	
			gp.typeLibelle = evt.objectReturn.typeTheme;
								
			var service : GrilleTarifService = new GrilleTarifService();
			var idRacine : Number = 0;
			
			if(!gp.isPublic)
			{
				idRacine = 	CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			}
			
			service.get_version_general(idRacine,gp.id_groupe_produit);	
			
						
			var popEditTarif : EditionVersionTarifIHM = new EditionVersionTarifIHM();
			
			
			popEditTarif.serviceGrilleTarif = service;
			popEditTarif.groupeProduits = gp;
			//popEditTarif.operateur_current = (comboOP.selectedItem 	as OperateurVO);
			popEditTarif.col_version_general = service.col_version_tarif_general;
			popEditTarif.idRacine = idRacine;
			popEditTarif.modeLecture = true;
			PopUpManager.addPopUp(popEditTarif,this,true);
			PopUpManager.centerPopUp(popEditTarif);
		}
		
		public function onPerimetreChange():void{
			retour();
		}
		
		public function rezise(tableau:String):void
		{
			if(tableau=='A')
			{
				C1.width = A1.width;
				C2.width = A2.width;
				C3.width = A3.width;
				C4.width = A4.width;
				C5.width = A5.width;
				C6.width = A6.width;
				C7.width = A7.width;
				C8.width = A8.width;
				C9.width = A9.width;
			}
			else
			{
				A1.width = C1.width;
				A2.width = C2.width;
				A3.width = C3.width;
				A4.width = C4.width;
				A5.width = C5.width;
				A6.width = C6.width;
				A7.width = C7.width;
				A8.width = C8.width;
				A9.width = C9.width;
			}
			T1.width = A1.width+A2.width+A3.width+A4.width;
			T5.width = A5.width;
			T6.width = A6.width;
			T7.width = A7.width;
			T8.width = A8.width;
			T9.width = A9.width;
		}
		
		//initialisation
		public function init(fe : FlexEvent):void{
			if (ctrl != null){
				ctrl.initSectionControle();				
				ctrl.initSectionInventaire();
				ctrl.controlerFacture();
				
				if(ctrl.boolInventaire)
				{
					getRessourcesHorsInventaire();
				}
			}
		}
		
		private var t:Timer  = new Timer(1001,1);
		
		//recuprer les infos pour l'inventaire apres une attente de une seconde
		//pour palier au 'bundlage' par le flashplayer des requêtes émises dans la mm frameSet		
		public function getRessourcesHorsInventaire():void
		{	
			t.start();
			t.addEventListener(TimerEvent.TIMER_COMPLETE, _tTimerCompleteHandler);			
		}
		
		protected function _tTimerCompleteHandler(event:TimerEvent):void
		{
			ctrl.getDetailRessourcesHorsInventaire();
			t.stop();
			t.removeEventListener(TimerEvent.TIMER_COMPLETE, _tTimerCompleteHandler);
		}
		
		
		public function showProduitOfGroupe(obj : LigneFacturation):void
		{
				var pop : ProduitOfGroupeIHM = new ProduitOfGroupeIHM();
				
				var gp : GroupeProduitVO = new GroupeProduitVO;
				gp.id_groupe_produit = obj.id;
				gp.libelle = obj.libelleProduit;
				pop.groupeProduits = gp;
				pop.serviceGrilleTarif = new GrilleTarifService();
				PopUpManager.addPopUp(pop,this,true);
				PopUpManager.centerPopUp(pop);
			
		}
		public function reset():void{
			if (initialized){
				if (cpInventaire != null ){
					cpInventaire.boolInit = true;
				}
				tnInventaire.selectedChild = cpHorsInventaire;	
			}
			
			if (ctrl != null){
				ctrl.initSectionControle();
				ctrl.initSectionInventaire();
			}
			
		}
		
		//le controleur
		[Bindable]
		public function set ctrlFacture(ctrlFactApp : AbstractControleFacture):void
		{
			ctrl = ctrlFactApp;
			ctrl.boolInventaire = CvAccessManager.getSession().CURRENT_PERIMETRE.BOOLINVENTAIRE;
		}
		
		public function get ctrlFacture():AbstractControleFacture{
			return ctrl;
			
		}
		
		//LABEL FUNCTION POUR DATAGRIDS		
		protected function formateDates(item : Object, column : AdvancedDataGridColumn):String{
			var ladate:Date = new Date(item[column.dataField]);
			return DateFunction.formatDateAsShortString(ladate);			
		}
		
		protected function formateEuros(item : Object, column : AdvancedDataGridColumn):String{
			
			return Format.toCurrencyString(item[column.dataField],2,true);	
		}
		
		protected function formateEurosP3(item : Object, column : AdvancedDataGridColumn):String{
			
			return Format.toCurrencyString(item[column.dataField],3,true);	
		}
		
		protected function formateEurosP4(item : Object, column : AdvancedDataGridColumn):String{
			
			return Format.toCurrencyString(item[column.dataField],4,true);	
		}
		
		protected function formateNumberPrec(item : Object, column : AdvancedDataGridColumn):String{
			
			if (CvAccessManager.getSession().USER.GLOBALIZATION == "en_GB" || CvAccessManager.getSession().USER.GLOBALIZATION == "en_US")
				return moduleSuiviFacturationIHM.currencySymbol + Format.toNumber(item[column.dataField], 4, true);
			else
				return Format.toNumber(item[column.dataField], 4, true) + " " + moduleSuiviFacturationIHM.currencySymbol;
		}
		
		protected function formateLigne(item : Object, column : AdvancedDataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}
		
		protected function formatePourControle(item : Object, column : AdvancedDataGridColumn):String{
			
			return (item[column.dataField] == true)?ResourceManager.getInstance().getString('M32', 'OUI'):ResourceManager.getInstance().getString('M32', 'NON');			
		}
		
		protected function formateNumber(item : Object, column : AdvancedDataGridColumn):String{
			return ConsoviewFormatter.formatNumber(Number(item[column.dataField]),4);
		}
		
		protected function formateEcartNumber(item : Object, column : AdvancedDataGridColumn):String{
			var ratio : Number = LigneFacturation(item).poidsDifference;
			return ConsoviewFormatter.formatNumber(ratio,2)+ "%";
		}
		//-----------
		//Handler
		protected function txtFiltreLignesFactAbosChangeHandler(ev : Event):void{
			if(dgTarifsAbos.dataProvider != null){ 
				(dgTarifsAbos.dataProvider as ArrayCollection).filterFunction = filtrerGridsAbos;
				(dgTarifsAbos.dataProvider as ArrayCollection).refresh();
			}
		}
		
		protected function txtFiltreLignesFactConsosChangeHandler(ev : Event):void{
			if(dgTarifsConsos.dataProvider != null){ 
				(dgTarifsConsos.dataProvider as ArrayCollection).filterFunction = filtrerGridsConsos;
				(dgTarifsConsos.dataProvider as ArrayCollection).refresh();
			}
		}
		
		protected function dgTarifsAbosChangeHandler(le : ListEvent):void{
			dgTarifsConsos.selectedIndex = -1;
			
			callLater(configbtJustificatif)			
			
		}
		
		protected function dgTarifsAbosUpdateCompleteHandler(ev : Event):void{
			if(ctrlFacture != null){
				callLater(configbtJustificatif)			
			}
		}
		
		protected function dgTarifsConsosUpdateCompleteHandler(ev : Event):void{
			if(ctrlFacture != null){
				callLater(configbtJustificatif);			
			}
		}
		
		private function configbtJustificatif():void{
			if (((dgTarifsConsos.selectedIndex == -1)&&(dgTarifsAbos.selectedIndex == -1))){
				btJustificatif.enabled = false;
			}else{
				btJustificatif.enabled = true;
			}
		}
		
		protected function dgTarifsConsosChangeHandler(le : ListEvent):void{			
			dgTarifsAbos.selectedIndex = -1;
			callLater(configbtJustificatif);
		}
		
		protected function btValiderClickHandler(me : MouseEvent):void{
			var message : String = "";
			message = ResourceManager.getInstance().getString('M32', '_nContr_le_valid__')+ ctrlFacture.construireSignature();
			demanderConfirmation( btValider.label,message,executerBtValiderClick);
		}
		
		private var confirmBox : ConfirmBox;
		protected function demanderConfirmation(libelle : String,message : String,confirmFonction : Function):void{
			confirmBox = new ConfirmBoxIHM();
			confirmBox.facture = ctrlFacture.selectedFacture;
			confirmBox.txRapport = message;
			confirmBox.libelle = libelle;
			confirmBox.doFunction = confirmFonction;
			PopUpManager.addPopUp(confirmBox,DisplayObject(parentApplication),true,PopUpManagerChildList.PARENT);
			PopUpManager.centerPopUp(confirmBox);	
		}
		
		//Execute l'action du click sur le bouton taguer apres confirmation
		private function executerBtValiderClick():void{
			ctrl.selectedFacture.controlee = 0;
			ctrl.selectedFacture.commentaireControler = confirmBox.txRapport + "\n" + confirmBox.commentaire +"\n\n";
			ctrl.updateVisaControleFacture();
			if (ctrl.selectedFacture.controlee == 0)
			{
				lgCtrles.setLogsControleFacture(ctrl.selectedFacture.periodeId , lgCtrl.whatIsThisButton(0, 0), confirmBox.txtCommentaire.text);
			}
			else
			{
				lgCtrles.setLogsControleFacture(ctrl.selectedFacture.periodeId, lgCtrl.whatIsThisButton(0, -1), confirmBox.txtCommentaire.text);
			}
			confirmBox = null;
			retour();
		}
		
		protected function btRetourClickHandler(me : MouseEvent):void{
			retour();
		}
		
		
		//fournit un justificatif avec la ligne séléctionné en évidence
		protected function btJustificatifClickHandler(me : MouseEvent):void{
			if (dgTarifsAbos.selectedIndex != -1){
				var ligfactAbo : LigneFacturation = dgTarifsAbos.selectedItem as LigneFacturation;
				ctrl.selectedLigneFacturation = ligfactAbo;
				ctrl.prixReference = Number(dgTarifsAbos.selectedItem.prixUnitaireRemise);
				ctrl.getJustificatif();
			}else if (dgTarifsConsos.selectedIndex != -1){
				var ligfactConso : LigneFacturation = dgTarifsConsos.selectedItem as LigneFacturation;
				ctrl.selectedLigneFacturation = ligfactConso;
				ctrl.prixReference = Number(dgTarifsConsos.selectedItem.prixUnitaireRemise);
				ctrl.getJustificatif();
			}else{
				ctrl.selectedLigneFacturation = new LigneFacturation();
			}	
		}
		
	    
	    //Filtre
	    private function filtrerGridsAbos(ligneFactAbo : LigneFacturation):Boolean{
	    	if ((ligneFactAbo.libelleProduit.toLowerCase().search(txtFiltreLignesFactAbos.text.toLowerCase()) != -1)
	    			||	    		
	    		(ligneFactAbo.prixUnitaire.toString().toLowerCase().search(txtFiltreLignesFactAbos.text.toLowerCase()) != -1)	    		
	    			||
	    		(ligneFactAbo.prixUnitaireRemise.toString().toLowerCase().search(txtFiltreLignesFactAbos.text.toLowerCase()) != -1)
	    			||
	    		(ligneFactAbo.poidsDifference.toString().toLowerCase().search(txtFiltreLignesFactAbos.text.toLowerCase()) != -1)
	    			||	    		
	    		(ligneFactAbo.montantMoyen.toString().toLowerCase().search(txtFiltreLignesFactAbos.text.toLowerCase()) != -1)
	    			||
	    		(ligneFactAbo.montantTotal.toString().toLowerCase().search(txtFiltreLignesFactAbos.text.toLowerCase()) != -1)
	    			||
	    		(ligneFactAbo.difference.toString().toLowerCase().search(txtFiltreLignesFactAbos.text.toLowerCase()) != -1)
	    			||
	    		(ligneFactAbo.poidsDifference.toString().toLowerCase().search(txtFiltreLignesFactAbos.text.toLowerCase()) != -1))
	    		return true;
	    	else
	    		return false;
	    }
	    
	    private function filtrerGridsConsos(ligneFactConso : LigneFacturation):Boolean{
	    	if ((ligneFactConso.libelleProduit.toLowerCase().search(txtFiltreLignesFactConsos.text.toLowerCase()) != -1)
	    			||	    		
	    		(ligneFactConso.prixUnitaire.toString().toLowerCase().search(txtFiltreLignesFactConsos.text.toLowerCase()) != -1)
	    			||
	    		(ligneFactConso.poidsDifference.toString().toLowerCase().search(txtFiltreLignesFactConsos.text.toLowerCase()) != -1)
	    			||	 
	    		(ligneFactConso.prixUnitaireRemise.toString().toLowerCase().search(txtFiltreLignesFactConsos.text.toLowerCase()) != -1)
	    			||	    		  		
	    		(ligneFactConso.montantMoyen.toString().toLowerCase().search(txtFiltreLignesFactConsos.text.toLowerCase()) != -1)
	    			||
	    		(ligneFactConso.montantTotal.toString().toLowerCase().search(txtFiltreLignesFactConsos.text.toLowerCase()) != -1)
	    			||	    		
	    		(ligneFactConso.difference.toString().toLowerCase().search(txtFiltreLignesFactConsos.text.toLowerCase()) != -1)
	    			||
	    		(ligneFactConso.poidsDifference.toString().toLowerCase().search(txtFiltreLignesFactConsos.text.toLowerCase()) != -1))
	    		return true;
	    	else
	    		return false;
	    }
	    
	    private function retour():void{	    	
			dispatchEvent(new Event(RETOUR));
	    }
		
		protected function onClickExportAbo(event:MouseEvent):void
		{
			if(dgTarifsAbos.dataProvider!=null && dgTarifsAbos.dataProvider.length > 0) 
			{     
				var dataToExport  :String = DataGridDataExporter.getCSVStringFromAdvDG(dgTarifsAbos, dgTarifsAbos.dataProvider,";","\n",false);
				var url :String = moduleSuiviFacturationIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M32/exportCSVString.cfm";
				DataGridDataExporter.exportCSVString(url, dataToExport, "_export_abbonements");
			}
			else
			{
				ConsoviewAlert.afficherError('No Data to export', 'Error');
			}		
			
		}
		
		protected function onClickExportConso(event:MouseEvent):void
		{
			if(dgTarifsConsos.dataProvider!=null && dgTarifsConsos.dataProvider.length > 0) 
			{     
				var dataToExport  :String = DataGridDataExporter.getCSVStringFromAdvDG(dgTarifsConsos, dgTarifsConsos.dataProvider,";","\n",false);
				var url :String = moduleSuiviFacturationIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M32/exportCSVString.cfm";
				DataGridDataExporter.exportCSVString(url, dataToExport, "_export_consommations");
			}
			else
			{
				ConsoviewAlert.afficherError('No Data to export', 'Error');
			}		
			
		}
		
		

		
	}
}
