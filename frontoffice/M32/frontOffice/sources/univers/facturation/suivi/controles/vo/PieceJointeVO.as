package univers.facturation.suivi.controles.vo
{
	import mx.collections.ArrayCollection;

	public class PieceJointeVO
	{
		
		public var LABEL_PIECE		:String		= "";
		public var OLD_LABEL_PIECE	:String		= "";
		public var AJOUT_PAR		:String		= "";
		public var MODIF_PAR		:String		= "";
		public var FORMAT			:String		= "";
		public var ACTION			:String		= "";
		public var DATE_AJOUT_STRG	:Date		= null;
		public var UUID				:String		= "";
		
		public var DATE_AJOUT_DATE	:Date		= new Date();
		public var DATE_MODIF_DATE	:Date		= new Date();
	
		public var JOINDRE			:Boolean	= true;
		public var BTN_DL_VISIBLE	:Boolean	= false;
		
		public var JOINDRE_CFVALUE	:int		= 1;
		public var IDOCMMANDE		:int		= -1;
		public var SIZE				:int		= -1;
		public var IDCLIENT			:int		= -1;
		public var ID_PIECE			:int		= -1;
		
		public function PieceJointeVO()
		{
		}
		
		public function mappingAttachement(values:ArrayCollection):ArrayCollection
		{
			var attach	:ArrayCollection;
			var pjvo	:PieceJointeVO;
			
			if(values == null) return attach;
			
			var len		:int = values.length;
			
			attach = new ArrayCollection();
			
			for(var i:int = 0;i < len;i++)
			{
				pjvo 					= new PieceJointeVO();
				pjvo.LABEL_PIECE 		= values[i].FILE_NAME;
				pjvo.AJOUT_PAR 			= values[i].CREATE_NAME;
				pjvo.MODIF_PAR 			= values[i].MODIFY_NAME;
				pjvo.FORMAT 			= values[i].FORMAT;
				pjvo.UUID 				= values[i].PATH;
				pjvo.JOINDRE_CFVALUE 	= values[i].JOIN_MAIL;
				pjvo.JOINDRE		 	= (pjvo.JOINDRE_CFVALUE==1)? true : false;
				pjvo.ID_PIECE 			= values[i].FILEID;
				pjvo.IDOCMMANDE			= values[i].IDCOMMANDE;
				pjvo.SIZE 				= values[i].FILE_SIZE;
				pjvo.DATE_AJOUT_DATE 	= values[i].DATE_CREATE;
				pjvo.DATE_MODIF_DATE 	= values[i].DATE_MODIFY;
				pjvo.DATE_AJOUT_STRG	= pjvo.DATE_AJOUT_DATE;
				pjvo.BTN_DL_VISIBLE		= true;
				
				attach.addItem(pjvo);
			}
			
			return attach;
		}
		

	}
}