package univers.facturation.suivi.controles.vues
{
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import univers.facturation.suivi.controles.controls.AbstractControleFacture;
	import univers.facturation.suivi.controles.controls.formatsclients.LogControl;
	import univers.facturation.suivi.controles.controls.formatsclients.LogControles;
	import univers.facturation.suivi.controles.vo.Facture;

	
	
	[Bindable]
	public class ControleAnalitiqueImpl extends PopUpBaseImpl
	{	

		protected var ctrlFactures : AbstractControleFacture;
		
		private var btnEditerAna:Boolean= false;
		static private var _lgCtrl:LogControl;
		private var _lgCtrles:LogControles = new LogControles(_lgCtrl);
		
		public var dgMontantFactureByOrga : DataGrid;
		public var txtTotal : TextInput;
		public var txtPercent : TextInput;
		public var txtDiff : TextInput;
		public var frmTotal : FormItem;
		public var frmPercent : FormItem;
		public var cbOrganisations : ComboBox;
		public var dgControleSauver : DataGrid;
		public var imgSaveControle : Image;
		public var lblOrganisation : Label;
		public var btLancerControle : Button;
		public var btSupprimer : Button;
		public var txtFiltreOrgas : TextInputLabeled;
		public var imgCSV : Image;
		public var imgPDF : Image;
		public var imgXLS : Image;
		
		public var imgDetailCSV : Image;
		public var imgDetailPDF : Image;
		public var imgDetailXLS : Image;
		
		public var imgCommentaire : Image;
		public var checkVisa:CheckBox;
		
		private var confirmBox : ConfirmBox;
	
		private static const MAX_SAVED_ORGA : Number = 2;
		
		private var _controleFacture : AbstractControleFacture;
		public function get controleFacture(): AbstractControleFacture
		{
			return _controleFacture;
		} 
		
		public function set controleFacture(c : AbstractControleFacture):void
		{
			_controleFacture = c;
		}
		
		
		
		public function ControleAnalitiqueImpl()
		{
			super();
		}
		
		static public function zReturnLogCtrl(logctrl:LogControl):void
		{
			_lgCtrl = logctrl;
		}
		
		//======================== FORMATTEURS =================================================
		//LABEL FUNCTION POUR DATAGRIDS		
		protected function formateDates(item : Object, column : DataGridColumn):String
		{
			if (item != null && item[column.dataField] != null){
				return DateFunction.formatDateAsString(item[column.dataField]);				
			}else return "-";
		}
		
		protected function formateEuros(item : Object, column : DataGridColumn):String
		{			
			if (item != null) return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2);	
			else return "-";
		}
		
		protected function formatTxtEuroCurrency(num : Number):String
		{
			return ConsoviewFormatter.formatEuroCurrency(num,2);
		}
		
		protected function formatTxtPourCent(num : Number):String
		{
			return ConsoviewFormatter.formatePourcent(num);
		}
		
		
		protected function formateNumber(item : Object, column : DataGridColumn):String
		{
			if (item != null) return ConsoviewFormatter.formatNumber(Number(item[column.dataField]),2);
			else return "-";
		}
		
		protected function formateLigne(item : Object, column : DataGridColumn = null):String
		{			
			if (item != null && column != null) return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
			else if (item != null && item.hasOwnProperty("TETE")){
				return ConsoviewFormatter.formatPhoneNumber(item.TETE);
			}else if (item != null && item.hasOwnProperty("SOUS_TETE")){
				return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);
			}else return "-";
		}
		
		// === FIN FORMATEURS ================================================================================
		
		// === SORT FUNCTIOINS ===============================================================================
		protected function numericSortFunction(itemA:Object, itemB:Object):int
		{
			return ObjectUtil.numericCompare(itemA.MONTANT,itemB.MONTANT);
		}
		// === FIN SORT FUNCTIOINS ===========================================================================
		
		//***********************************************************************************************************************************
		 override protected function commitProperties():void
		{
			super.commitProperties();
			cbOrganisations.addEventListener(ListEvent.CHANGE,cbOrganisationsChangeHandler);
			txtFiltreOrgas.addEventListener(Event.CHANGE,txtFiltreOrgaChangeHandler);
			
			imgCSV.addEventListener(MouseEvent.CLICK,imgCSVClickHandler);
			imgPDF.addEventListener(MouseEvent.CLICK,imgPDFClickHandler);
			imgXLS.addEventListener(MouseEvent.CLICK,imgXLSClickHandler);
			//imgCommentaire.addEventListener(MouseEvent.CLICK,imgCommentaireClickHandler);
			imgDetailCSV.addEventListener(MouseEvent.CLICK,imgDetailCSVClickHandler);
			imgDetailPDF.addEventListener(MouseEvent.CLICK,imgDetailPDFClickHandler);
			imgDetailXLS.addEventListener(MouseEvent.CLICK,imgDetailXLSClickHandler);
		}
		
		//Handlers******************************************************************************************************************
		
		//Attribut au datagrid ('dgControle') les différents éléments déja viser dès que la requête est fini
		protected function init(fe : FlexEvent):void
		{
			btnEditerAna = false;
			if(_lgCtrl.EtatEditerVisaAnalytique == 1)
			{ 	
				btnEditerAna = true; 
				_lgCtrl.ArrayQueryCtrlAna = new ArrayCollection(null);	
				_lgCtrles.getLogsControleFactureAnalytique(_lgCtrl.LogControles_PeriodeID);
				_lgCtrles.addEventListener("goToPrint", zPrintContolAnalytiqueHandler);
			}
 		}
 		
 		//Attribut au datagrid ('dgControle') les différents éléments déja viser dès que l'évènement 'goToPrint' est dispatché
 		private function zPrintContolAnalytiqueHandler(evt:Event):void
 		{
 				var myCollection:ArrayCollection = _lgCtrl.ArrayQueryCtrlAna;
				var i:int = 0;

				for (i = 0; i <myCollection.length ;i++)
				{
					controleFacture.visaAnalytiqueleSaveList.addItem(myCollection[i]);
				}
 		}
 		
 		/*private function zSearchDoublon(myArrayCollection:ArrayCollection):void
 		{
// 			if(myArrayCollection[i].IDLOG
 		}*/
		
		protected function updateColor(ev : Event):void
		{
			if (controleFacture.montantNonAffecte != 0)
			{
				txtPercent.setStyle("color","red");			
				txtPercent.setStyle("borderColor","red");	
				txtDiff.setStyle("color","red");			
				txtDiff.setStyle("borderColor","red");	
			}else{
				txtPercent.setStyle("color","green");			
				txtPercent.setStyle("borderColor","green");	
				txtDiff.setStyle("color","green");			
				txtDiff.setStyle("borderColor","green");	
			}
		}
		
		protected function filtrerListOrgas():void
		{
			if (controleFacture.listeOrganistaion != null)
			{
				controleFacture.listeOrganistaion.filterFunction = filtrerLalisteDesOrgas;
				controleFacture.listeOrganistaion.refresh()	
			}
			
		}
		
		protected function filtrerLalisteDesOrgas(item : Object):Boolean
		{
			if (item.LIBELLE_GROUPE_CLIENT != null && String(item.LIBELLE_GROUPE_CLIENT).toLowerCase().search(txtFiltreOrgas.text.toLowerCase()) != -1)
			{
				if (cbOrganisations.selectedIndex == -1) cbOrganisations.selectedItem = item;
				return true
			} 
			else {
				if (cbOrganisations.selectedItem == item)
				{
					 cbOrganisations.selectedIndex = -1;	
				}
				return false;
			}
		}
		
		//== HADNLERS ========================================================================================================
		protected function txtFiltreOrgaChangeHandler(ev : Event):void
		{
			filtrerListOrgas();
		}
		
		protected function btLancerControleClickHandler(me : MouseEvent):void
		{
			if (cbOrganisations.selectedIndex != -1)
			{
				controleFacture.seletcedOrganisation = cbOrganisations.selectedItem;
				controleFacture.chargerResultatAnalityque(controleFacture.seletcedOrganisation);
				lblOrganisation.text = ResourceManager.getInstance().getString('M32', 'Organisation_s_lectionn_e___')+ cbOrganisations.selectedItem.LIBELLE_GROUPE_CLIENT;				
			}else{
				controleFacture.seletcedOrganisation = null;
				lblOrganisation.text = "";
			}
		}
		
		protected function cbOrganisationsChangeHandler(le : ListEvent):void
		{
			//controleFacture.seletcedOrganisation = cbOrganisations.selectedItem;
		}
		
		protected function txtFiltreEnterHandler(ev : FlexEvent):void
		{
			if(cbOrganisations.selectedIndex != -1)
			{
				controleFacture.seletcedOrganisation = cbOrganisations.selectedItem;
				controleFacture.chargerResultatAnalityque(controleFacture.seletcedOrganisation);
				lblOrganisation.text = ResourceManager.getInstance().getString('M32', 'Organisation_s_lectionn_e___')+ cbOrganisations.selectedItem.LIBELLE_GROUPE_CLIENT;
			}else{
				controleFacture.seletcedOrganisation = null;
				lblOrganisation.text = "";
			}
		}
		
		protected function dgControleSauverDoubleClickHandler(le : ListEvent):void
		{
			if(dgControleSauver.selectedIndex != -1)
			{
				var orga : Object = new Object();
				orga.LIBELLE_GROUPE_CLIENT = dgControleSauver.selectedItem.LIBELLE_GROUPE_CLIENT;
				orga.IDGROUPE_CLIENT = dgControleSauver.selectedItem.IDGROUPE_CLIENT;
				controleFacture.seletcedOrganisation  = orga;
				controleFacture.chargerResultatAnalityque(dgControleSauver.selectedItem);
				lblOrganisation.text = ResourceManager.getInstance().getString('M32', 'Organisation_s_lectionn_e___')+ dgControleSauver.selectedItem.LIBELLE_GROUPE_CLIENT;
			}
//			zrelaunchControle();
		}
		
		/*private function zrelaunchControle():void
		{
		if (cbOrganisations.selectedIndex != -1)
			{
				controleFacture.seletcedOrganisation = cbOrganisations.selectedItem;
				controleFacture.chargerResultatAnalityque(controleFacture.seletcedOrganisation);
				lblOrganisation.text = ResourceManager.getInstance().getString('M32', 'Organisation_s_lectionn_e___')+ cbOrganisations.selectedItem.LIBELLE_GROUPE_CLIENT;				
			}else{
				controleFacture.seletcedOrganisation = null;
				lblOrganisation.text = "";
			}
		}*/
		

		
		//**********************************************************************************************************************************************************
		
		//Récupère les factures visées analytiquement lors du clic sur le bouton 'Valider' et les envoie dans un tableeau
		override protected function btValiderClickHandler(me:MouseEvent):void
		{
			var nbOrgaSave : Number = 0;
			var message : String = "";
					
			_lgCtrl.logControlAna_ArrayListFacturesAnalytique = new Array();
			
			for (var i : Number = 0; i< controleFacture.visaAnalytiqueleSaveList.length;i++)
			{
				if(controleFacture.visaAnalytiqueleSaveList[i].GARDER) 
				{
					_lgCtrl.logControlAna_ArrayListFacturesAnalytique[controleFacture.arrayNumber] = controleFacture.visaAnalytiqueleSaveList[i].IDGROUPE_CLIENT;
					controleFacture.arrayNumber++;
					_lgCtrl.logControlAna_ArrayListFacturesAnalytique[controleFacture.arrayNumber] = controleFacture.visaAnalytiqueleSaveList[i].TOTAL_ANALYTIQUE;
					controleFacture.arrayNumber++;
					_lgCtrl.logControlAna_ArrayListFacturesAnalytique[controleFacture.arrayNumber] = controleFacture.visaAnalytiqueleSaveList[i].TOTAL_NONAFFECTE;
					controleFacture.arrayNumber++;
					_lgCtrl.logControlAna_ArrayListFacturesAnalytique[controleFacture.arrayNumber] = zFormatePourcent(controleFacture.visaAnalytiqueleSaveList[i].PERCENT);
					controleFacture.arrayNumber++;
					
					nbOrgaSave++;
				}
			}
         	controleFacture.arrayNumber = 0;
         	if(btnEditerAna == false)
         	{
				if(nbOrgaSave == 0)
				{
					 message = ResourceManager.getInstance().getString('M32', 'Merci_de_cocher_le_resultat_que_vous_souhaitez_conserver__Visa__'); 
					Alert.show(message,ResourceManager.getInstance().getString('M32', 'Erreur'));
					return;
				}
			}
			if(nbOrgaSave > MAX_SAVED_ORGA)
			{
				message = ResourceManager.getInstance().getString('M32', 'Vous_ne_pouvez_s_lectionner_que_') + MAX_SAVED_ORGA.toString() + ResourceManager.getInstance().getString('M32', '_organisations_au_maximum'); 
				Alert.show(message,ResourceManager.getInstance().getString('M32', 'Erreur'));
				return;
			}
			
			dispatchEvent(new Event(PopUpBaseImpl.VALIDER_CLICKED));
			
		}
		
		//Converti le pourcentage avec 2 chiffres après la virgule
		private function zFormatePourcent(pourcent:Number):Number
		{
			var tempPourcent:Number = pourcent as Number;
			 
			if(pourcent < 100)
			{tempPourcent = new Number(tempPourcent.toPrecision(4)); }
			else
			{ tempPourcent = new Number(tempPourcent.toPrecision(5)); }

			return	tempPourcent;
		}
		
		protected function SendDataToDatabase():void
		{
			var facture : Facture = dgControleSauver.selectedItem as Facture;
			ctrlFactures.selectedFacture = facture;
			ctrlFactures.updateVisaFacture();
			var IdPeriode : Number = (dgControleSauver.selectedItem as Facture).periodeId;
		}
		
		public function dispatcheEventPopUp():void
		{
			dispatchEvent(new Event(PopUpBaseImpl.VALIDER_CLICKED));
		}
		
		protected function imgCSVClickHandler(me : MouseEvent):void
		{	
			if(controleFacture.seletcedOrganisation != null)
			{
				controleFacture.exporterFactureByOrga(AbstractControleFacture.CSV);
			}
		}
		
		protected function imgPDFClickHandler(me : MouseEvent):void
		{
			if(controleFacture.seletcedOrganisation != null)
			{
				controleFacture.exporterFactureByOrga(AbstractControleFacture.CSV);
			}
		}
		
		protected function imgXLSClickHandler(me : MouseEvent):void
		{
			if(controleFacture.seletcedOrganisation != null)
			{
				controleFacture.exporterFactureByOrga(AbstractControleFacture.XLS);	
			}
		}
		
		
		
		protected function imgDetailCSVClickHandler(me : MouseEvent):void
		{	
			if(controleFacture.seletcedOrganisation != null)
			{
				controleFacture.exporterDetailFactureByOrga(AbstractControleFacture.CSV);	
			}
		}
		
		protected function imgDetailPDFClickHandler(me : MouseEvent):void
		{
			if(controleFacture.seletcedOrganisation != null)
			{
				controleFacture.exporterDetailFactureByOrga(AbstractControleFacture.PDF);
			}
		}
		
		protected function imgDetailXLSClickHandler(me : MouseEvent):void
		{
			if(controleFacture.seletcedOrganisation != null)
			{
				controleFacture.exporterDetailFactureByOrga(AbstractControleFacture.XLS);	
			}
		}
		
		
		protected function imgPDF2ClickHAndler(event:MouseEvent):void
		{
			if(controleFacture.seletcedOrganisation != null)
			{
				controleFacture.exporterLignesNonAffecteesCA(AbstractControleFacture.PDF);
			}				
		}
		
		public function imgCommentaireClickHandler(event:MouseEvent):void
		{
			if(controleFacture.seletcedOrganisation != null)
			{
				dispatchEvent(new Event(PopUpBaseImpl.VALIDER_CLICKED));
			}		
		}
		// ==== FIN HANDLERS ================================================================================================ 
				
	}
}