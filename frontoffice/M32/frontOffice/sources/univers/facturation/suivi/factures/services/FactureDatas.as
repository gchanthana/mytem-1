package univers.facturation.suivi.factures.services
{
    import mx.collections.ArrayCollection;

    public class FactureDatas
    {
        //VARIABLES------------------------------------------------------------------------------
        private var _listeProduct:ArrayCollection = new ArrayCollection();
        private var _listeProductFavoris:ArrayCollection = new ArrayCollection();

        //VARIABLES------------------------------------------------------------------------------
        public function FactureDatas()
        {
        }

        //PROPRIETEES PUBLIC (GETTER/SETTER)-----------------------------------------------------
        public function get listeProduct():ArrayCollection
        {
            return _listeProduct;
        }

        public function set listeProduct(value:ArrayCollection):void
        {
            _listeProduct = value;
        }

        public function get listeProductFavoris():ArrayCollection
        {
            return _listeProductFavoris;
        }

        public function set listeProductFavoris(value:ArrayCollection):void
        {
            _listeProductFavoris = value;
        }
    }
}