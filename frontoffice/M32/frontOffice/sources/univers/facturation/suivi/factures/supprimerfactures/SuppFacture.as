package univers.facturation.suivi.factures.supprimerfactures
{
    import mx.resources.ResourceManager;
	import composants.controls.TextInputLabeled;
    import composants.util.ConsoviewFormatter;
    import composants.util.ConsoviewUtil;
    import composants.util.DateFunction;
    
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.ui.ContextMenu;
    
    import mx.collections.ArrayCollection;
    import mx.containers.Panel;
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.DataGrid;
    import mx.controls.Image;
    import mx.controls.dataGridClasses.DataGridColumn;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    
    import univers.facturation.suivi.controles.controls.AbstractControleFacture;
    import univers.facturation.suivi.controles.vo.Facture;

    public class SuppFacture extends Panel
    {
        //Reference vers l'applicatif qui gére la suppression des factures
        private var _suppFactureApp:SuppFactureApp;

        [Bindable]
        public function get suppFactureApp():SuppFactureApp
        {
            return _suppFactureApp;
        }

        public function set suppFactureApp(app:SuppFactureApp):void
        {
            _suppFactureApp = app;
        }
        //L'icon export CSV
        public var imgCSV:Image;
        public var imgXLS:Image;
        /* [Bindable]
           public function get imgCSV():Image
           {
           return _imgCSV;
         }*/ /*
           public function set imgCSV(img:Image):void
           {
           _imgCSV = img;
         }*/ //L'icon export facture 1er page
		
		
		//import des icones
		[Embed(source="/assets/images/note.png")]
		public static const note:Class;
		
		[Embed(source="/assets/images/note_off.png")]
		public static const note_off:Class;
		
		[Embed(source="/assets/images/note_view.png")]
		public static const noteView:Class;
		
		[Embed(source="/assets/images/note_view_off.png")]
		public static const noteView_off:Class;
		
		
        private var _imgFactureCat:Image;

        [Bindable]
        public function get imgFactureCat():Image
        {
            return _imgFactureCat;
        }

        public function set imgFactureCat(img:Image):void
        {
            _imgFactureCat = img;
        }
        //L'icon export facture 1er page
        private var _imgFacture:Image;
		
        [Bindable]
        public function get imgFacture():Image
        {
            return _imgFacture;
        }

        public function set imgFacture(img:Image):void
        {
            _imgFacture = img;
        }
		
		
		
        //La liste des factures
        private var _dgFactures:DataGrid;

        [Bindable]
        public function get dgFactures():DataGrid
        {
            return _dgFactures;
        }

        public function set dgFactures(dg:DataGrid):void
        {
            _dgFactures = dg;
        }
        //Le boutton de suppresion des facture
        private var _btSuppFacture:Button;

        [Bindable]
        public function get btSuppFacture():Button
        {
            return _btSuppFacture
        }

        public function set btSuppFacture(bt:Button):void
        {
            _btSuppFacture = bt;
        }
        //Le bouton retour
        private var _btRetour:Button;

        [Bindable]
        public function get btRetour():Button
        {
            return _btRetour
        }

        public function set btRetour(bt:Button):void
        {
            _btRetour = bt;
        }
        //le filtre
        private var _txtFiltre:TextInputLabeled;

        [Bindable]
        public function get txtFiltre():TextInputLabeled
        {
            return _txtFiltre;
        }

        public function set txtFiltre(txt:TextInputLabeled):void
        {
            _txtFiltre = txt;
        }
        //Le menu contextuel du grid des factures
        private var _dgFacturesContextMenu:ContextMenu;

        //Constructeur
        public function SuppFacture()
        {
            super();
        }

        //Ajouter les 'Listeners'
        override protected function commitProperties():void
        {
            super.commitProperties();
            _dgFactures.addEventListener(FlexEvent.UPDATE_COMPLETE, dgFacturesUpdateCompleteHandler);
            _dgFactures.addEventListener(Event.CHANGE, dgFacturesChangHandler);
            _dgFactures.dataProvider = suppFactureApp.listeFactures;
            _btSuppFacture.addEventListener(MouseEvent.CLICK, btSuppFactureClickHandler);
            _btRetour.addEventListener(MouseEvent.CLICK, btRetourClickHandler);
            _txtFiltre.addEventListener(Event.CHANGE, txtFiltreChangeHandler);
            //  _imgCSV.addEventListener(MouseEvent.CLICK, imgCSVClickHandler);
            imgFacture.addEventListener(MouseEvent.CLICK, imgFactureClickHandler);
            imgFactureCat.addEventListener(MouseEvent.CLICK, imgFactureCatClickHandler);
        }

        //================= FORMATTEURS =====================================
        protected function formateDates(item:Object, column:DataGridColumn):String
        {
            if (item != null)
            {
                var ladate:Date = new Date(item[column.dataField]);
                return DateFunction.formatDateAsShortString(ladate);
            }
            else
                return "-";
        }

        protected function formateEuros(item:Object, column:DataGridColumn):String
        {
            if (item != null)
                return ConsoviewFormatter.formatEuroCurrency(item[column.dataField], 2);
            else
                return "-";
        }

        protected function formateNumber(item:Object, column:DataGridColumn):String
        {
            if (item != null)
                return ConsoviewFormatter.formatNumber(Number(item[column.dataField]), 2);
            else
                return "-";
        }

        protected function formateLigne(item:Object, column:DataGridColumn):String
        {
            if (item != null)
                return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);
            else
                return "-";
        }

        protected function formateBoolean(item:Object, column:DataGridColumn):String
        {
            if (item != null)
                return (Number(item[column.dataField]) == 1) ? ResourceManager.getInstance().getString('M32', 'OUI') : ResourceManager.getInstance().getString('M32', 'NON');
            else
                return "-";
        }

        //================= FORMATTEURS =====================================
        //===================== HANDLERS ====================================//
        //Handler du bouton supprimer la facture
        private function btSuppFactureClickHandler(me:MouseEvent):void
        {
            var handler:Function = function traitement(ce:CloseEvent):void
                {
                    if (ce.detail == Alert.OK)
                    {
                        suppFactureApp.supprimerLaFacture(_dgFactures.selectedItems);
                    }
                }
            Alert.okLabel = ResourceManager.getInstance().getString('M32', 'Oui');
            Alert.cancelLabel = ResourceManager.getInstance().getString('M32', 'Annuler');
            if (suppFactureApp != null)
            {
                var tablisteFacture:Array = _dgFactures.selectedItems;
                var message:String;
                var listeFacture:String = ConsoviewUtil.arrayToList(new ArrayCollection(_dgFactures.selectedItems), "numero", ",");
                if (tablisteFacture.length > 1)
                    message = ResourceManager.getInstance().getString('M32', '_tes_vous_sur_de_vouloir_supprimer_les_f') + listeFacture;
                else
                    message = ResourceManager.getInstance().getString('M32', '_tes_vous_sur_de_vouloir_supprimer_la_fa') + listeFacture;
                Alert.show(message, ResourceManager.getInstance().getString('M32', 'Confirmer_'), Alert.OK | Alert.CANCEL, this, handler);
            }
        }

        //Handler du bouton retour
        private function btRetourClickHandler(me:MouseEvent):void
        {
            if (suppFactureApp != null)
            {
            }
        }

        //Handler du filtre
        private function txtFiltreChangeHandler(ev:Event):void
        {
            if (suppFactureApp != null)
            {
                suppFactureApp.filtrerLaListe(_txtFiltre.text);
            }
        }

        private function dgFacturesChangHandler(ev:Event):void
        {
            if ((_dgFactures.selectedItems != null) && (_dgFactures.selectedItems.length > 0))
            {
                activerBoutons(true);
            }
            else
            {
                activerBoutons(false);
            }
        }

        //Handler de la mise à jour du DataGrid
        private function dgFacturesUpdateCompleteHandler(fe:FlexEvent):void
        {
            if (_dgFactures.initialized)
            {
                if ((_dgFactures.dataProvider != null) && ((_dgFactures.dataProvider as ArrayCollection).length > 0))
                {
                    if (imgCSV && imgXLS)
                    {
                        imgCSV.visible = true;
                        imgXLS.visible = true;
                    }
                }
                else
                {
                    if (imgCSV && imgXLS)
                    {
                        imgCSV.visible = false;
                        imgXLS.visible = false;
                    }
                }
                if (_dgFactures.selectedIndex == -1)
                {
                    activerBoutons(false);
                }
                else
                {
                    activerBoutons(true);
                }
            }
        }

        //Handler de l'export du tableau
        public function imgCVSClickHandler(me:MouseEvent):void
        {
            if ((_dgFactures.dataProvider as ArrayCollection).length > 0)
            {
                var afficheFacture:AfficheFacture = new AfficheFacture();
                afficheFacture.paramsRecherches = suppFactureApp.paramsRecherche;
                afficheFacture.exporterLaFacture(AbstractControleFacture.CSV_LISTEFACTURE,0);
            }
        }

        //Handler de l'export du tableau
        public function imgXLSClickHandler(me:MouseEvent):void
        {
            if ((_dgFactures.dataProvider as ArrayCollection).length > 0)
            {
				 var afficheFacture:AfficheFacture = new AfficheFacture();
                  afficheFacture.paramsRecherches = suppFactureApp.paramsRecherche;
                  afficheFacture.exporterLaFacture(AbstractControleFacture.XLS_LISTEFACTURESUPP,0);
              
            }
        }

        //Handler de l'affichage du détail de la facture
        private function imgFactureClickHandler(me:MouseEvent):void
        {
			var verif:Boolean = (moduleSuiviFacturationIHM.userAcess.hasOwnProperty('D_FACT'))? !(moduleSuiviFacturationIHM.userAcess.D_FACT == 0) : true;
			
            if ((_dgFactures.selectedItem != null) && verif)
            {
                var afficheFacture:AfficheFacture = new AfficheFacture();
                afficheFacture.selectedFacture = _dgFactures.selectedItem as Facture;
                afficheFacture.exporterLaFacture(AbstractControleFacture.PDF_FACTURE,0);
            }
        }

        //Handler de l'affichage de la premiere page de la facture
        private function imgFactureCatClickHandler(me:MouseEvent):void
        {
			var verif:Boolean = (moduleSuiviFacturationIHM.userAcess.hasOwnProperty('D_FACT'))? !(moduleSuiviFacturationIHM.userAcess.D_FACT == 0) : true;
			
            if ((_dgFactures.selectedItem != null) && verif)
            {
                var afficheFacture:AfficheFacture = new AfficheFacture()
                afficheFacture.selectedFacture = _dgFactures.selectedItem as Facture;
                afficheFacture.exporterLaFacture(AbstractControleFacture.PDF_FACTURECAT,0);
            }
        }

        //===================== HANDLERS FIN ==================================//
        //==================== METHODES ========================================//
        private function activerBoutons(bool:Boolean):void
        {
            btSuppFacture.enabled = bool;
            //imgFacture.visible = bool;
            //imgFactureCat.visible = bool;
			
			var verif:Boolean = (moduleSuiviFacturationIHM.userAcess.hasOwnProperty('D_FACT'))? !(moduleSuiviFacturationIHM.userAcess.D_FACT == 0) : true;
			
			if(bool && verif){
				imgFactureCat.source=note;
				imgFacture.source=noteView;
				imgFactureCat.buttonMode=true;
				imgFacture.buttonMode=true;
			}
			else{
				imgFactureCat.source=note_off;
				imgFacture.source=noteView_off;
				imgFactureCat.buttonMode=false;
				imgFacture.buttonMode=false;
			}
			
        }
        //==================== METHODES FIN ====================================//
    }
}