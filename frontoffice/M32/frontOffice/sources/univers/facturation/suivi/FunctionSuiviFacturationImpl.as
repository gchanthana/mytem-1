package univers.facturation.suivi
{
	import mx.containers.VBox;
	import mx.containers.ViewStack;
	import mx.controls.LinkBar;
	import mx.events.FlexEvent;
	
	import univers.facturation.grilletarif.ihm.GrilleTarifIHM;
	import univers.facturation.suivi.controles.vues.ControleFacturationIHM;
	import univers.facturation.suivi.factures.Index;
	
	[Bindable]
	public class FunctionSuiviFacturationImpl extends VBox
	{
		//--- composants -------------------------------
		public var myFact:Index;
		public var myControleFacture : ControleFacturationIHM;
		public var instanceGrilleTarifIHM : GrilleTarifIHM ;
		public var menuSuivi : LinkBar;
		public var myVs : ViewStack;
				
		//-----------------------------------------------
		
		public function FunctionSuiviFacturationImpl()
		{
			trace("(Suivi de facturation) Instance Creation");
			addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
			
		}
			
		protected  function afterCreationComplete(event:FlexEvent):void {
			activerControleFacturation(true);
			initSaisieTarif();
			trace("(FunctionListeAppels) Perform IHM Initialization (" + getUniversKey() + "," + getFunctionKey() + ")");
			
		}
		
		public  function getUniversKey():String {
			return "FACT";
		}
		
		public  function getFunctionKey():String {
			return "FACT_SUIVI";
		}
		
		public  function afterPerimetreUpdated():void 
		{
			if(myFact && myFact.initialized) myFact.onPerimetreChange();
			if(instanceGrilleTarifIHM && instanceGrilleTarifIHM.initialized) instanceGrilleTarifIHM.onPerimetreChange();
			if(myControleFacture && myControleFacture.initialized)	myControleFacture.onPerimetreChange();
			activerControleFacturation(true);
		}
		
		//PRIVATE FUNCTIONS ===============================================================================================================
		private function activerControleFacturation(bool : Boolean):void{	
			if (!bool)
			{
				myVs.removeChild(myControleFacture);
				myVs.selectedIndex = 0;				
			}else{
				
				
				if (CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX != CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX ) {
					if(myVs.getChildByName("instanceGrilleTarifIHM") != null ) 
					{                   
						myVs.removeChild(instanceGrilleTarifIHM);                             
					}
				}

				if(myVs.getChildByName("myControleFacture") == null)
				{					
					myVs.addChild(myControleFacture);
				} 
			}
			
		}
		//FIN PRIVATE FUNCTIONS ===============================================================================================================
		
		private function initSaisieTarif():void {
			if(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX == CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX){
				instanceGrilleTarifIHM.initData();
			}else{
				instanceGrilleTarifIHM.includeInLayout = false;
				instanceGrilleTarifIHM.visible = false
			}
			
		}
		
	}
}
