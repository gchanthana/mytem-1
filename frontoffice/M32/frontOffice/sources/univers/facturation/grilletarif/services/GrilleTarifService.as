package univers.facturation.grilletarif.services
{
    import flash.events.EventDispatcher;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;
    import mx.utils.ObjectUtil;
    
    import univers.facturation.grilletarif.entity.CompteSousCompteVO;
    import univers.facturation.grilletarif.entity.GroupeProduitVO;
    import univers.facturation.grilletarif.entity.OperateurVO;
    import univers.facturation.grilletarif.entity.ParametreRechercheVO;
    import univers.facturation.grilletarif.entity.ProduitVO;
    import univers.facturation.grilletarif.entity.VersionTarifVO;
    import univers.facturation.grilletarif.event.GrilleTarifEvent;
    import univers.facturation.grilletarif.utils.GestionSuppressionTarif;
    import univers.facturation.grilletarif.utils.MessageGrilleTarif;

    [Bindable]
    public class GrilleTarifService extends EventDispatcher
    {
        // import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
        public var col_operateur:ArrayCollection = new ArrayCollection();
        public var col_version_tarif_general:ArrayCollection = new ArrayCollection();
        public var col_compte_sousCompte:ArrayCollection = new ArrayCollection();
        public var col_produit_of_groupe:ArrayCollection = new ArrayCollection();
        public var col_groupe_produit:ArrayCollection = new ArrayCollection();
        public var col_exception_valide:ArrayCollection = new ArrayCollection();
        public var col_version_exception:ArrayCollection = new ArrayCollection();
        public var col_data_export:ArrayCollection = new ArrayCollection();
        public var collecIsFilled:Boolean = false;

        public function GrilleTarifService()
        {
        }

        public function get_col_operateur(idRacine:Number):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "L_OpeRacine", get_col_operateur_handler);
            RemoteObjectUtil.callService(op, idRacine);
        }

        public function save_X_version_tarif_general(col:ArrayCollection):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "save_X_version_tarif_general", save_X_version_tarif_general_handler);
            RemoteObjectUtil.callService(op, ObjectUtil.copy(new ArrayCollection(col.source).source));
        }

        private function save_X_version_tarif_general_handler(re:ResultEvent):void
        {
        }

        public function get_opeLibelles(operateur:OperateurVO):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "L_OpeLibelles", L_OpeLibelles_handler);
            RemoteObjectUtil.callService(op, operateur.idOperateur, operateur);
        }

        public function get_produit_of_groupe(idGroupeProduit:Number):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "L_Produit_groupe", get_produit_of_groupe_handler);
            RemoteObjectUtil.callService(op, idGroupeProduit);
        }

        public function get_tarif_exception(idRacine:int, idGP:Number):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "getTarifException", get_tarif_exception_handler);
            RemoteObjectUtil.callService(op, idRacine, idGP);
        }

        public function get_version_tarif_exception(idRacine:int, idgroupeProd:Number, typeTarif:String, idCompte:int, idSScompte:int):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "getVersionTarifException", get_version_tarif_exception_handler);
            RemoteObjectUtil.callService(op, idRacine, idgroupeProd, typeTarif, idCompte, idSScompte);
        }

        public function get_version_general(idRacine:Number, idGroupeProduit:Number):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "L_Versions_generales", get_version_general_handler);
            RemoteObjectUtil.callService(op, idRacine, idGroupeProduit);
        }

        public function updateIsControle(idGroupeProduit:Number, idRacine:Number):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "taguer_grp_ctl", updateIsControle_handler);
            RemoteObjectUtil.callService(op, idGroupeProduit, idRacine);
        }

        public function removeVersionTarif(idVersion:Number, methode:String, removeUniqueVersion:Boolean, typeSuppression:String):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "removeVersionTarif", removeVersionTarif_handler);
            RemoteObjectUtil.callService(op, idVersion, methode, removeUniqueVersion, typeSuppression);
        }

        public function removeAllVersionTarif(p_idgrp_ctl:int, p_idracine:int, p_idcompte_facturation:int, p_idsous_compte:int, p_type_tarif:String):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "removeAllVersionTarif", removeAllVersionTarif_handler);
            RemoteObjectUtil.callService(op, p_idgrp_ctl, p_idracine, p_idcompte_facturation, // 0 pour null
                                         p_idsous_compte, //0 pour null
                                         p_type_tarif // 'R', -- 'C', 'S'
                                         );
        }

        public function searchcompte_sous_compte(idRacine:Number, idOperateur:Number, numero:String, typeTarif:String, idGroupeProduit:int):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "Searchcompte_sous_compte", searchcompte_sous_compte_handler);
            RemoteObjectUtil.callService(op, idRacine, idOperateur, numero, typeTarif, idGroupeProduit);
        }

        public function addVersionTarif(p_idgrp_ctl:int, p_idracine:int, p_type_tarif:String, p_idcompte_facturation:int, p_idsous_compte:int, p_datedeb:Date, p_tarif_brut:Number, p_tarif_remise:Number, p_pct_remise:Number):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "addVersionTarif", addVersionTarif_handler);
            RemoteObjectUtil.callService(op, p_idgrp_ctl, p_idracine, p_type_tarif, // 'R', -- 'C', 'S'
                                         p_idcompte_facturation, // 0 pour null
                                         p_idsous_compte, //0 pour null
                                         p_datedeb, p_tarif_brut, p_tarif_remise, p_pct_remise);
        }

        public function editDateFin(idVersion:int, p_datedeb:Date):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "updateDateVersionTarif", editDateFin_handler);
            RemoteObjectUtil.callService(op, idVersion, p_datedeb);
        }

        public function rechercherGroupeProduit(parametreRecherche:ParametreRechercheVO):void
        {
            var handlerFunction:String = 'rechercherGroupeProduit_handler';
            if (parametreRecherche.callForExport)
            {
                handlerFunction = 'exportCSV_handler';
                parametreRecherche.index_debut = 1;
            }
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.grilletarif", "SearchGroupe", this[handlerFunction]);
            RemoteObjectUtil.callService(op, parametreRecherche.idracine, // racine
                                         parametreRecherche.operateurid, // id de l'operateur
                                         parametreRecherche.code_type_tarif, // 0=tous; 1=public; 2=client
                                         parametreRecherche.type_grp_ctl, // 0=tous; 1=abonnements; 2=consommations
                                         parametreRecherche.controler, // -1=tous; 1=oui; 0=non
                                         parametreRecherche.grp_segment, // 0=tous; 1=fixe; 2=Mobile; 3 = data
                                         parametreRecherche.avec_exception, // -1=tous; 1=avec; 0=sans
                                         parametreRecherche.etat_exception, // -2=périmé; 3=en cours; 4=a venir
                                         parametreRecherche.libelle_grp, // libelle du groupe à rechercher
                                         parametreRecherche.avec_calcul, // 0 OU 1 POUR calculer le montant 6 dernier mois
                                         parametreRecherche.index_debut, // pagination debut
                                         parametreRecherche.number_of_records // pagination nombre de lignes
                                         );
        }

        private function removeAllVersionTarif_handler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                var obj:Object = new Object()
                obj.removeUniqueVersion = true;
                obj.typeSuppression = GestionSuppressionTarif.TARIF_EXCEPTION;
                dispatchEvent(new GrilleTarifEvent(GrilleTarifEvent.REMOVE_VERSION_TARIF_COMPLETE, true, false, obj));
            }
        }

        private function editDateFin_handler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                dispatchEvent(new GrilleTarifEvent(GrilleTarifEvent.EDITE_DATE_FIN_COMPLETE));
            }
            else
            {
                trace('erreur editDateFin_handler');
            }
        }

        private function updateIsControle_handler(re:ResultEvent):void
        {
            if (re)
            {
                //Alert.show('update ok');
            }
        }

        private function removeVersionTarif_handler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                var obj:Object = new Object()
                obj.removeUniqueVersion = re.token.message.body[2] as Boolean;
                obj.typeSuppression = re.token.message.body[3] as String;
                dispatchEvent(new GrilleTarifEvent(GrilleTarifEvent.REMOVE_VERSION_TARIF_COMPLETE, true, false, obj));
            }
        }

        private function addVersionTarif_handler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                dispatchEvent(new GrilleTarifEvent(GrilleTarifEvent.ADD_VERSION_TARIF_COMPLETE, false, false, 1));
            }
            else
            {
                Alert.show(MessageGrilleTarif.MESSAGE_ECHEC_TARIF, MessageGrilleTarif.TITRE_INFORMATION);
                    //dispatchEvent(new GrilleTarifEvent(GrilleTarifEvent.ADD_VERSION_TARIF_COMPLETE,false,false,-1));
            }
        }

        private function searchcompte_sous_compte_handler(re:ResultEvent):void
        {
            col_compte_sousCompte.removeAll();
            var compteSousCompte:CompteSousCompteVO;
            var size:int = (re.result as ArrayCollection).length;
            //  (size > 0) ? collecIsFilled = true : collecIsFilled = false;
            for (var i:int; i < size; i++)
            {
                //IDCOMPTE_FACTURATION
                //ID
                compteSousCompte = new CompteSousCompteVO();
                compteSousCompte.libelle = re.result[i].COMPTE_EXCEPTION;
                compteSousCompte.cleType = re.result[i].TYPE_TARIF;
                compteSousCompte.idCompteSousCompte = re.result[i].ID;
                compteSousCompte.restriction = (re.result[i].EXCEPTION_EXISTANTE > 0);
                col_compte_sousCompte.addItem(compteSousCompte);
            }
            dispatchEvent(new GrilleTarifEvent(GrilleTarifEvent.LISTE_COMPTE_COMPTE_COMPLETE));
        }
		
		
        private function rechercherGroupeProduit_handler(re:ResultEvent):void
        {
            col_groupe_produit.removeAll();
            var size:int = (re.result as ArrayCollection).length;
            (size > 0) ? collecIsFilled = true : collecIsFilled = false;
            for (var i:int; i < size; i++)
            {
                col_groupe_produit.addItem(groupeProduitFactory(re.result[i]));
            }
        }
		
		
        private function get_version_general_handler(re:ResultEvent):void
        {
            var size:int = (re.result as ArrayCollection).length;
            col_version_tarif_general.removeAll();
            //Pour éviter de rappeler la recherche de groupe de produit (longue), on met à jour manuellement les tarif du groupe de produit
            //  (size > 0) ? collecIsFilled = true : collecIsFilled = false;
            for (var i:int; i < size; i++)
            {
                col_version_tarif_general.addItem(versionTarifFactory(re.result[i]));
                //si le tarif valide a changé il faut mettre a jour les tarif dans le grid groupe de produit
                var version:VersionTarifVO = col_version_tarif_general.getItemAt(i) as VersionTarifVO;
                //On se place sur la version de tarif valide (= au gp selectionné)
                if (version.isValide && version.cleType != 'P')
                {
                    //on cherche le groupe de prod de la version et on met ses tarif à jour:
                    for (var e:int = 0; e < col_groupe_produit.length; e++)
                    {
                        if ((col_groupe_produit.getItemAt(e) as GroupeProduitVO).id_groupe_produit == version.idGroupeProduit && !(col_groupe_produit.getItemAt(e) as GroupeProduitVO).isPublic)
                        {
                            (col_groupe_produit.getItemAt(e) as GroupeProduitVO).prixUnitaire = version.prixUnitaire;
                            (col_groupe_produit.getItemAt(e) as GroupeProduitVO).prixUnitaireRemise = version.prixUnitaireRemise;
                            (col_groupe_produit.getItemAt(e) as GroupeProduitVO).remisePercent = version.remisePercent;
                            (col_groupe_produit.getItemAt(e) as GroupeProduitVO).idVersion = version.idVersionTarif;
							
							//si c'est un nouveau tarif et qu'il n y en avait pas avant (idVersionTarif du groupe de produit  == 0)
							//alors on regarde si le groupe de produit était ou pas en exception 
							//s'il ne l'était pas alors on coche la case a controler.
							var aucune_version : Boolean = (col_groupe_produit.getItemAt(e) as GroupeProduitVO).aucune_version;
							if(aucune_version)
							{
								(col_groupe_produit.getItemAt(e) as GroupeProduitVO).isControle = (col_groupe_produit.getItemAt(e) as GroupeProduitVO).isControle || (col_groupe_produit.getItemAt(e) as GroupeProduitVO).isReelControl;
							}
                            (col_groupe_produit.getItemAt(e) as GroupeProduitVO).aucune_version = false;
							col_groupe_produit.itemUpdated(col_groupe_produit.getItemAt(e));
							break;
                            //(col_groupe_produit.getItemAt(e) as GroupeProduitVO).isPublic = false;
                        }
                    }
                }
				//break;
            }
        }

        private function get_version_tarif_exception_handler(re:ResultEvent):void
        {
            col_version_exception.removeAll();
            if (re)
            {
                var version:VersionTarifVO;
                var size:int = (re.result as ArrayCollection).length;
                (size > 0) ? collecIsFilled = true : collecIsFilled = false;
                for (var i:int; i < size; i++)
                {
                    col_version_exception.addItem(versionTarifFactory(re.result[i]));
                }
            }
        }

        private function get_tarif_exception_handler(re:ResultEvent):void
        {
            col_exception_valide.removeAll();
            col_version_exception.removeAll();
            if (re.result)
            {
                var version:VersionTarifVO;
                var size:int = (re.result as ArrayCollection).length;
                for (var i:int; i < size; i++)
                {
                    col_exception_valide.addItem(versionTarifFactory(re.result[i]));
                }
                dispatchEvent(new GrilleTarifEvent(GrilleTarifEvent.GET_EXCEPTION_COMPLETE));
            }
        }

        private function get_produit_of_groupe_handler(re:ResultEvent):void
        {
            var op:ProduitVO;
            col_produit_of_groupe.removeAll();
            ((re.result as ArrayCollection).length > 0) ? collecIsFilled = true : collecIsFilled = false;
            for (var i:int; i < (re.result as ArrayCollection).length; i++)
            {
                op = new ProduitVO();
                op.libelleProduit = re.result[i].LIBELLE_PRODUIT;
                col_produit_of_groupe.addItem(op);
            }
        }

        private function L_OpeLibelles_handler(re:ResultEvent):void
        {
            var op:OperateurVO = re.token.message.body[1];
            var size:int = (re.result as ArrayCollection).length;
            for (var i:int; i < size; i++)
            {
                if (re.result[i].CORRESPONDANCE_TABLE == "compte_facturation")
                {
                    op.libelleCF = re.result[i].TYPE_NIVEAU;
                }
                if (re.result[i].CORRESPONDANCE_TABLE == "sous_compte")
                {
                    op.libelleSCpte = re.result[i].TYPE_NIVEAU;
                }
            }
            dispatchEvent(new GrilleTarifEvent(GrilleTarifEvent.GET_LIBELLE_COMPTE_COMPLETE, false, false, op));
        }

        private function get_col_operateur_handler(re:ResultEvent):void
        {
            var op:OperateurVO;
            var size:int = (re.result as ArrayCollection).length;
            for (var i:int; i < size; i++)
            {
                op = new OperateurVO();
                op.idOperateur = re.result[i].OPERATEURID;
                op.libelleOperateur = re.result[i].NOM;
                col_operateur.addItem(op);
            }
        }

        
		private function tempDubug():void
        {
            //Debug:
            (col_groupe_produit.getItemAt(2) as GroupeProduitVO).isPublic = true;
            //Debug
            (col_groupe_produit.getItemAt(1) as GroupeProduitVO).aucune_version = true;
            (col_groupe_produit.getItemAt(1) as GroupeProduitVO).isControle = false;
            (col_groupe_produit.getItemAt(1) as GroupeProduitVO).prixUnitaire = 0;
            (col_groupe_produit.getItemAt(1) as GroupeProduitVO).remisePercent = 0;
            (col_groupe_produit.getItemAt(1) as GroupeProduitVO).prixUnitaireRemise = 0;
        }
		

        private function exportCSV_handler(re:ResultEvent):void
        {
            col_data_export.removeAll();
            var size:int = (re.result as ArrayCollection).length;
            for (var i:int; i < size; i++)
            {
                col_data_export.addItem(groupeProduitFactory(re.result[i]));
            }
            dispatchEvent(new GrilleTarifEvent(GrilleTarifEvent.DATA_EXPORT_COMPLETE));
        }

       private function groupeProduitFactory(objetDB:Object):GroupeProduitVO
        {
            var groupeProduit:GroupeProduitVO = new GroupeProduitVO();
            groupeProduit.libelle = objetDB.GRP_CTL;
            groupeProduit.typeLibelle = objetDB.TYPE_GRP;
            groupeProduit.partFacturation = objetDB.PART_FACTURATION;
            groupeProduit.lastMontant = objetDB.MONTANT_PERIODE;
            groupeProduit.prixUnitaire = objetDB.TARIF_BRUT;
            groupeProduit.remisePercent = objetDB.PCT_REMISE;
            groupeProduit.prixUnitaireRemise = objetDB.TARIF_REMISE;
            groupeProduit.idVersion = objetDB.IDVERSION_GRP_CTL;
            groupeProduit.libelleTypeException = objetDB.ETAT_EXCEPTION;
            groupeProduit.id_groupe_produit = objetDB.IDGRP_CTL;
            groupeProduit.isPublic = (objetDB.TARIF_PUBLIC == 1);
            groupeProduit.aucune_version = (objetDB.IDVERSION_GRP_CTL < 1);
            groupeProduit.isControle = (objetDB.A_CONTROLER == 1);
			groupeProduit.isReelControl = (objetDB.A_CONTROLER_REEL == 1);
            groupeProduit.NBRECORD = objetDB.ROWN_TOTAL;
            groupeProduit.publicVersionExiste = (objetDB.PUBLIC_EXISTANT > 0);
            if (groupeProduit.isPublic)
            {
                groupeProduit.libelleTypeException = 'Non';
            }
            return groupeProduit;
        }

        private function versionTarifFactory(objetDB:Object):VersionTarifVO
        {
            var version:VersionTarifVO = new VersionTarifVO();
            version.dateDebut = objetDB.DATEDEB;
            version.dateFin = objetDB.DATEFIN;
            version.idVersionTarif = objetDB.IDVERSION_GRP_CTL;
            version.prixUnitaire = objetDB.TARIF_BRUT;
            version.prixUnitaireRemise = objetDB.TARIF_REMISE;
            version.remisePercent = objetDB.PCT_REMISE;
            version.libelleCompteSousCompte = objetDB.COMPTE_EXCEPTION;
            version.idGroupeProduit = objetDB.IDGRP_CTL;
            version.libelle = objetDB.GRP_CTL;
            version.cleType = objetDB.TYPE_TARIF;
            if (version.cleType == "C")
            {
                version.idCompte = objetDB.IDCOMPTE_FACTURATION;
            }
            if (version.cleType == "S")
            {
                version.idSousCompte = objetDB.IDSOUS_COMPTE;
            }
            if (objetDB.VERSION_COURANTE > 0)
            {
                version.isValide = true;
            }
            return version;
        }
    }
}