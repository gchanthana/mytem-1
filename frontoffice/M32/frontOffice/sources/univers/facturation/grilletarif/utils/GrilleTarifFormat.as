package univers.facturation.grilletarif.utils
{
	import composants.Format;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.utils.ObjectUtil;
	
	import univers.facturation.grilletarif.entity.GroupeProduitVO;
	
	public class GrilleTarifFormat
	{
		public function GrilleTarifFormat()
		{
		}
		
		/**
		 * Mise en forme des colonnes affichant des tarifs. La précision est de 2 chiffres après la virgule
		 * Le formatage est sensible à la localisation
		 * ex : 14 122,45 € ou $ 14,122.45		 
		 * @param item une réference vers la ligne du DataGrid.
		 * @param column une référence vers la colonne a mettre en forme.
		 * @return la donnée formatée.
		 * */
		public static function eurosFormat(item : Object, column : DataGridColumn):String
		{
			if(item[column.dataField])
			{
				if (CvAccessManager.getSession().USER.GLOBALIZATION == "en_GB" || CvAccessManager.getSession().USER.GLOBALIZATION == "en_US")
					return moduleSuiviFacturationIHM.currencySymbol + " " + Format.toCurrencyString(item[column.dataField],4);
				else
					return Format.toCurrencyString(item[column.dataField],4) + " " + moduleSuiviFacturationIHM.currencySymbol;
			}
			else
			{
				return "-";
			}
		}
		
		
		/**
		 * Mise en forme des colonnes affichant des tarifs. La précision est de 3 chiffres après la virgule
		 * Le formatage est sensible à la localisation
		 * ex : 14 122,457 € ou $ 14,122.457 
		 * @param item une réference vers la ligne du DataGrid.
		 * @param column une référence vers la colonne a mettre en forme.
		 * @return la donnée formatée.
		 * */
		public static function eurosFormatP3(item : Object, column : DataGridColumn):String
		{
			
			if(item[column.dataField])
			{
				return Format.toCurrencyString(item[column.dataField],3);
			}
			else
			{
				return "-";
			}	
		}
		
		
		
		/**
		 * Mise en forme des colonnes affichant des tarifs. La précision est de 4 chiffres après la virgule 
		 * pour les consos sinon elle est de 2 chiffres
		 * Le test est fait sur des libellés 'Consommations' (pour l'international il faudrat ajouter des id ex: 1 pour Abo et 2 pour Conso) 
		 * Le formatage est sensible à la localisation
		 * ex : 14 122,45 € ou $ 14,122.45 pour abo et 14 122,4545 € ou $ 14,122.4545 pour conso  
		 * @param item une réference vers la ligne du DataGrid.
		 * @param column une référence vers la colonne a mettre en forme.
		 * @return la donnée formatée.
		 * */
		public static function eurosFormatGroupeProduitVO(item : GroupeProduitVO, column : DataGridColumn):String
		{
			
			if(item[column.dataField])
			{
				var digit:Number = 4;
				
				//TO DO changer le test et prendre des id
				if(item.typeLibelle.toLowerCase().search('consommation') != -1)
				{
					digit = 4;	
				}
				if (CvAccessManager.getSession().USER.GLOBALIZATION == "en_GB" || CvAccessManager.getSession().USER.GLOBALIZATION == "en_US")
					return moduleSuiviFacturationIHM.currencySymbol + " " + Format.toNumber(item[column.dataField],digit)
				else
					return Format.toNumber(item[column.dataField],digit) + " " + moduleSuiviFacturationIHM.currencySymbol;
				
			}
			else
			{
				return "-";
			}	
		}
		

		public static function toNumber(value:Number,precision:Number,fixe:Boolean = true):String
		{
			if (value)
			{
				if (CvAccessManager.getSession().USER.GLOBALIZATION == "en_GB" || CvAccessManager.getSession().USER.GLOBALIZATION == "en_US")
					return moduleSuiviFacturationIHM.currencySymbol + " " + Format.toNumber(value, precision, fixe);
				else
					return Format.toNumber(value, precision, fixe) + " " + moduleSuiviFacturationIHM.currencySymbol;		 
			}
			else
				return "-";
		}
		
		
		
		/**
		 * Mise en forme des colonnes affichant des pourcentages. La précision est de 4 chiffres après la virgule
		 * Le formatage est sensible à la localisation
		 * ex : 14 122,4570 % ou 14,122.4570 %  
		 * @param item une réference vers la ligne du DataGrid.
		 * @param column une référence vers la colonne a mettre en forme.
		 * @return la donnée formatée.
		 * */
		public static function percentFormat( item:Object, column:DataGridColumn):String
		{	
			if(item[column.dataField])
			{
				return Format.formatePourcentPrecis(item[column.dataField],2);
			}
			else
			{
				return "-";
			}
		}
		
		
		/**
		 * Mise en forme des colonnes affichant des dates. 
		 * Le formatage est sensible à la localisation
		 * ex : 22/12/2007 fr_FR ou 12/22/2007 pour en_US  
		 * @param item une réference vers la ligne du DataGrid.
		 * @param column une référence vers la colonne a mettre en forme.
		 * @return la donnée formatée.
		 * */
		public static function formateDates(item : Object, column : DataGridColumn):String{			
			if (item[column.dataField] != null){
				var ladate:Date = new Date(item[column.dataField]);				
				return DateFunction.formatDateAsShortString(ladate);				
			}else return "-";
			
		}  	

	}
}