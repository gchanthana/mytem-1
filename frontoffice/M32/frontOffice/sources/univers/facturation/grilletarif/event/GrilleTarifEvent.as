package univers.facturation.grilletarif.event
{
	import flash.events.Event;

	public class GrilleTarifEvent extends Event
	{
		public static const GET_LIBELLE_COMPTE_COMPLETE : String="getLibelleCompteCOmplete";
		public static const REMOVE_VERSION_TARIF_GENERAL : String="removeVersionGeneral";
		public static const GET_EXCEPTION_COMPLETE : String="getExceptionComplete";
		public static const DATA_EXPORT_COMPLETE : String="dataExportComplete";
		public static const ADD_VERSION_TARIF_COMPLETE : String="addVersionTarifComplete";
		public static const OPEN_POP_CREATE_VERSION_TARIF : String="openPopCreateVersionTarif";
		public static const OPEN_POP_EDIT_TARIF : String="openPopEditVersionTarif";
		public static const LISTE_COMPTE_COMPTE_COMPLETE : String="listeCompteComplete";
		public static const REMOVE_VERSION_TARIF: String="removeVersionTarifTarif";
		public static const REFRESHGRIDTARIF: String="refreashGridTarif";
		public static const OPEN_POP_EDITE_DATE_FIN: String="openPopDateFin";
		public static const EDITE_DATE_FIN_COMPLETE: String="editeDateFinComplete";
		public static const SHOW_DETAIL_TARIF: String="showDetailTarif";
		
		public static const REMOVE_VERSION_TARIF_COMPLETE: String="removeVersionTarifTarifComplete";
			
		private var _objectReturn:Object;
		
		public function GrilleTarifEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,objectReturn : Object = null)
		{
			this.objectReturn = objectReturn;
			super(type, bubbles, cancelable);
		}
		public function set objectReturn(value:Object):void
		{
			_objectReturn = value;
		}
		public function get objectReturn():Object
		{
			return _objectReturn;
		}
	}
}