package univers.facturation.grilletarif.entity
{
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class GroupeProduitVO
	{
		
		private var _libelle:String="";
		private var	_typeLibelle:String;
		private var _partFacturation:Number;
		private var _lastMontant:Number;
		private var _prixUnitaire:Number=0;
		private var _remisePercent:Number=0;
		private var _idVersion:Number;
		private var _prixUnitaireRemise:Number=0;
		private var _isControle : Boolean;
		private var _aucune_version:Boolean;
		private var _isPublic:Boolean;
		private var _remiseMontant:Number=0;
		private var _libelleTypeException:String;
		private var _id_groupe_produit:int;
		private var _col_version_tarif:ArrayCollection;
		private var _publicVersionExiste : Boolean;
		private var  _isReelControl:Boolean;
		
		public var NBRECORD : int;
		
		public function GroupeProduitVO()
		{
		}
		public function get publicVersionExiste():Boolean
		{
			return _publicVersionExiste;
		}

		public function set publicVersionExiste(value:Boolean):void
		{
			_publicVersionExiste = value;
		}
		public function get id_groupe_produit():int
		{
			return _id_groupe_produit;
		}
		
		public function set id_groupe_produit(value:int):void
		{
			_id_groupe_produit = value;
		}

		public function get typeLibelle():String
		{
			return _typeLibelle;
		}

		public function set typeLibelle(value:String):void
		{
			_typeLibelle = value;
		}

		public function get remisePercent():Number
		{
			return _remisePercent;
		}
		public function set remisePercent(value:Number):void
		{
			if(value)
			{
				_remisePercent = value;
			}
			else
			{
				_remisePercent = 0;
			}
			
		}
		public function get idVersion():Number
		{
			return _idVersion;
		}
		public function set idVersion(value:Number):void
		{
			_idVersion = value;
		}

		public function get libelle():String
		{
			return _libelle;
		}

		public function set libelle(value:String):void
		{
			_libelle = value;
		}

		public function get remiseMontant():Number
		{
			return _remiseMontant;
		}

		public function set remiseMontant(value:Number):void
		{
			_remiseMontant = value;
		}

		public function get partFacturation():Number
		{
			return _partFacturation;
		}

		public function set partFacturation(value:Number):void
		{
			_partFacturation = value;
		}

		public function get lastMontant():Number
		{
			
			return _lastMontant;
		
			
		}

		public function set lastMontant(value:Number):void
		{
			_lastMontant = value;
		}

		public function get prixUnitaire():Number
		{
			return _prixUnitaire;
		}

		public function set prixUnitaire(value:Number):void
		{
			_prixUnitaire = value;
		}

		public function get prixUnitaireRemise():Number
		{
			return _prixUnitaireRemise;
		}

		public function set prixUnitaireRemise(value:Number):void
		{
			_prixUnitaireRemise = value;
		}

		public function get isControle():Boolean
		{
			return _isControle;
		}

		public function set isControle(value:Boolean):void
		{
			_isControle = value;
		}
		public function get isPublic():Boolean
		{
			return _isPublic;
		}

		public function set isPublic(value:Boolean):void
		{
			_isPublic = value;
		}
		public function get aucune_version():Boolean
		{
			return _aucune_version;
		}

		public function set aucune_version(value:Boolean):void
		{
			_aucune_version = value;
		}

		public function get libelleTypeException():String
		{
			return _libelleTypeException;
		}

		public function set libelleTypeException(value:String):void
		{
			_libelleTypeException = value;
		}

		public function get col_version_tarif():ArrayCollection
		{
			return _col_version_tarif;
		}

		public function set col_version_tarif(value:ArrayCollection):void
		{
			_col_version_tarif = value;
		}
		public function allowEditionGroupeProduit():Boolean
		{
			if(isPublic || aucune_version) // si il est pas public et si il a au moins une version
			{
				return false;
			}
			return true;
		}
		public function boolAddVersion():Boolean
		{
			
			if(!isPublic && aucune_version)
			{
				return true
			}
			return false;
		}

		public function get isReelControl():Boolean
		{
			return _isReelControl;
		}

		public function set isReelControl(value:Boolean):void
		{
			_isReelControl = value;
		}
		
		
	}
}