package univers.facturation.grilletarif.entity
{
	public class ParametreRechercheVO
	{
		public var idracine :int=-1; // racine
		public var operateurid :int = 190; // id de l'operateur
		public var code_type_tarif :int = 2; // 0=tous; 1=public; 2=client
		public var type_grp_ctl :int = 1; // 0=tous; 1=abonnements; 2=consommations
		public var controler :int = -1; // -1=tous; 1=oui; 0=non
		public var grp_segment :int = -1; // 0=tous; 1=fixe; 2=Mobile; 3 = data
		public var avec_exception :int = -1; // -1=tous; 1=avec; 0=sans
		public var etat_exception :int=-1; // -2=périmé; 3=en cours; 4=a venir
		public var _libelle_grp :String=""; // libelle du groupe à rechercher
		public var avec_calcul :int=0; // 0
		public var index_debut :int = 1; // pagination debut
		public var number_of_records :int; // pagination nombre de lignes
		
		public var callForExport :Boolean; // boolean pour gérer l'esport en csv des données
		
		public function ParametreRechercheVO()
		{
		}
		public function get libelle_grp():String
		{
			if(_libelle_grp){
				return _libelle_grp;
			}
			else
			{
				return "";
			}
			
		}
		public function set libelle_grp(value:String):void
		{
			_libelle_grp = value;
		}

	}
}