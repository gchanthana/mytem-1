package univers.facturation.grilletarif.entity
{
	public class CompteSousCompteVO
	{
		private var _libelle:String="";
		private var	_restriction:Boolean;
		private var _idCompteSousCompte:Number;
		private var _cleType : String;
		
		public function CompteSousCompteVO()
		{
		}
		public function get cleType():String
		{
			return _cleType;
		}
		
		public function set cleType(value:String):void
		{
			_cleType = value;
		}
		public function get libelle():String
		{
			return _libelle;
		}
		
		public function set libelle(value:String):void
		{
			_libelle = value;
		}
		public function get restriction():Boolean
		{
			return _restriction;
		}
		public function set restriction(value:Boolean):void
		{
			_restriction = value;
		}
		public function get idCompteSousCompte():Number
		{
			return _idCompteSousCompte;
		}
		
		public function set idCompteSousCompte(value:Number):void
		{
			_idCompteSousCompte = value;
		}

	}
}