package univers.facturation.grilletarif.entity
{
	[Bindable]
	public class OperateurVO
	{
		private var _idOperateur:int;
		private var _libelleOperateur:String;
		private var _libelleCF:String;
		private var _libelleSCpte:String;
		
		
		public function OperateurVO()
		{
		}

		public function get idOperateur():int
		{
			return _idOperateur;
		}

		public function set idOperateur(value:int):void
		{
			_idOperateur = value;
		}

		public function get libelleOperateur():String
		{
			return _libelleOperateur;
		}

		public function set libelleOperateur(value:String):void
		{
			_libelleOperateur = value;
		}

		public function get libelleCF():String
		{
			return _libelleCF;
		}

		public function set libelleCF(value:String):void
		{
			_libelleCF = value;
		}

		public function get libelleSCpte():String
		{
			return _libelleSCpte;
		}

		public function set libelleSCpte(value:String):void
		{
			_libelleSCpte = value;
		}


	}
}