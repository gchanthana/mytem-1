package rapport.event
{
	import flash.events.Event;
	
	public class RapportIHMEvent extends Event
	{
		public static const RESET_CLICK_EVENT:String	  = "ResetClickEvent"; 
		public static const SEARCH_CLICK_EVENT:String 	  = "SeachClickEvent"; 
		public static const NEXT_CLICK_EVENT:String 	  = "NextClickEvent"; 
		public static const CLOSE_CLICK_EVENT:String 	  = "CloseClickEvent";
		public static const INITIALIZE_CLICK_EVENT:String = "InitializeClickEvent";
		public static const VALIDER_CLICK_EVENT :String= "ValiderClickEvent";
		public static const VALIDER_TOUCHE_ENTREE_EVENT :String= "ValiderToucheEntreeEvent";
		public static const VALIDER_RETOUR_EVENT :String= "ValiderRetourEvent";
		public static const CHANGER_INDEXDEPART_RAPPORT_EVENT :String= "ChangerIndexDepartEvent";
		public static const CHANGER_VALUE_HSLIDER_EVENT :String= "ChangerValueHsliderEvent";
		public static const CHANGER_VALUE_HSLIDER_BIS_EVENT :String= "ChangerValueHsliderEvent";
		public static const UPDTAE_BUTTON_RECHERCHER_EVENT :String= "UpdateButtonRechercherEvent";
		public static const INIT_INDEXDEPART_RAPPORT_EVENT :String= "InitIndexDepartEvent";
		
		public function RapportIHMEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new RapportIHMEvent(type,bubbles,cancelable);
		}
	}
}