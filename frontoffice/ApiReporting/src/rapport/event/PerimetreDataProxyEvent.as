package rapport.event {
	import flash.events.Event;

	public class PerimetreDataProxyEvent extends Event {
		public static const NODE_CHILD_LOADED:String = "NODE CHILD LOADED EVENT";
		
		public function PerimetreDataProxyEvent(type:String) {
			super(type);
		}
	}
}