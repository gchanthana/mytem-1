package rapport.utils.consommationselector
{
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.validators.NumberValidator;
	import mx.validators.Validator;
	import mx.resources.ResourceManager;
	import rapport.utils.SelectorAbstract;
	import rapport.utils.serialization.json.*;
	import rapport.vo.Parametre;
	import rapport.vo.SelectorParametre;
	
	public class consommationSelectorImpl extends SelectorAbstract
	{
		public var arrayCleLibelle :Object;
		[Bindable]public var textInputconso:TextInput;
		public var validatorNumeric:NumberValidator;
		public var validNumeric:NumberValidator;
		
		public function consommationSelectorImpl(selector:Object=null)
		{
			super(selector);
		}
		
		/**
		** permert de redefinir la methode setSelector afin d'initialiser le selecteur de consommation
		** selector : un VO 
		*/
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
				
			}
		}
		protected function creationCompleteHandler(event:FlexEvent):void
		{
			showHelp();
		}
				
		/**
		** permet de récuperer la valeur selectionné par l'utilisateur et preparer un objet de type parametre
		** retour : un tableau des paramètres
		*/
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
			var parametre :Parametre=  new Parametre();
			
			parametre.value=[textInputconso.text];					
			parametre.cle =arrayCleLibelle[0].NOM_CLE;
			parametre.libelle=arrayCleLibelle[0].LIBELLE_CLE;
			parametre.is_system=arrayCleLibelle[0].IS_SYSTEM;
			parametre.typeValue=this.typeParametreComposant.toUpperCase();
			
			tabValeur[0]=parametre;
			return tabValeur;
		}
		
		/**
		* Permet de verifier que l'user a entré une valeur numerique
		*/
		public function validateValueNumeric():void
		{
			/*  validateAll : valide si tous les validateurs sont validés  , cette methodes prends les id des validateurs comme paramètres sous form d'un tableau*/
			var validationResult:Array=Validator.validateAll([validNumeric]);
			
			if(validationResult.length!=0) // la methode  validateAll renvoi un tableau vide si tout est ok
			{
				this._isOk=false;
				_errorMessage=ResourceManager.getInstance().getString('ApiReporting', 'Veuillez_corriger_les_erreurs_en_rouge');
			}
			else
			{
				this._isOk=true;
			}	
		}
	}
}