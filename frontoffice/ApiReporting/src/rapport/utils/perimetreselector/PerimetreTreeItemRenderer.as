package  rapport.utils.perimetreselector  {
	import mx.controls.treeClasses.TreeItemRenderer;

	public class PerimetreTreeItemRenderer extends TreeItemRenderer {
		public function PerimetreTreeItemRenderer() {
			super();
		}
		
        public override function set data(value:Object):void {
			super.data = value;
			if(value != null) {
				if(value.@STC > 0)
	        		setStyle("color","#00BB00");
	   			else
					setStyle("color","#000000");
			}
        }

        protected override function updateDisplayList(unscaledWidth:Number,unscaledHeight:Number):void {       
            super.updateDisplayList(unscaledWidth,unscaledHeight);
			//super.label.text =  "Le libellé";
        }
	}
}
