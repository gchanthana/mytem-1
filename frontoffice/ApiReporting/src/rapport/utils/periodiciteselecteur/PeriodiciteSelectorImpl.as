package rapport.utils.periodiciteselecteur
{
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	
	import rapport.event.RapportEvent;
	import rapport.service.periodiciteselector.PeriodiciteSelectorService;
	import rapport.utils.SelectorAbstract;
	import rapport.utils.serialization.json.*;
	import rapport.vo.Parametre;
	import rapport.vo.SelectorParametre;
	import rapport.vo.ValeurSelector;
	
	public class PeriodiciteSelectorImpl extends SelectorAbstract
	{
		
		[Bindable]public var PeriodiciteComboBoxSelectorService:PeriodiciteSelectorService
		[Bindable]public var comboBoxPeriodicite:ComboBox;
		public var arrayCleLibelle :Array;
		
		
		public function PeriodiciteSelectorImpl(selector:Object=null)
		{
			super(selector);
		}
		
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.idRapportParamtre=selector.idRapportParam;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
			}
		}
				
		protected function creationCompleteHandler(event:FlexEvent):void
		{
			PeriodiciteComboBoxSelectorService=new PeriodiciteSelectorService();
			PeriodiciteComboBoxSelectorService.model.addEventListener(RapportEvent.USER_PERIODICITE_SELECTOR_EVENT,fillData);
			PeriodiciteComboBoxSelectorService.getValuesPeriodicite(this.idRapportParamtre);
			
		}
		
		private function fillData(event:RapportEvent):void
		{
			comboBoxPeriodicite.dataProvider=PeriodiciteComboBoxSelectorService.model.periodicite;
			showHelp();
		}
		
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
			var parametre :Parametre=  new Parametre();
			parametre.value=[(comboBoxPeriodicite.selectedItem as ValeurSelector).valeur];					
			parametre.cle =arrayCleLibelle[0].NOM_CLE;
			parametre.libelle=arrayCleLibelle[0].LIBELLE_CLE;
			parametre.is_system=arrayCleLibelle[0].IS_SYSTEM;
			parametre.typeValue=this.typeParametreComposant.toUpperCase();
			
			tabValeur[0]=parametre;
			return tabValeur;
		}

	}
}