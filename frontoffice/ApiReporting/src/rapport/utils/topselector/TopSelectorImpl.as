package rapport.utils.topselector
{
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	
	import rapport.event.RapportEvent;
	import rapport.service.topselector.TopSelectorService;
	import rapport.utils.SelectorAbstract;
	import rapport.utils.serialization.json.*;
	import rapport.vo.Parametre;
	import rapport.vo.SelectorParametre;
	import rapport.vo.ValeurSelector;
	
	
	public class TopSelectorImpl extends SelectorAbstract
	{
		
		[Bindable]public var topSelectorService :TopSelectorService;
		[Bindable]public var comboBoxTop:ComboBox;
		 
		public var arrayCleLibelle :Object;
		
		public function TopSelectorImpl(selector:Object=null)
		{
			super(selector);
		}
		
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
			}
		}
		
		protected function topselector_creationCompleteHandler(event:FlexEvent):void
		{
			topSelectorService=new TopSelectorService();
			topSelectorService.model.addEventListener(RapportEvent.USER_TOPS_SELECTOR_EVENT,fillData);
			topSelectorService.getValuesTop();
		}
				
		private function fillData(event:RapportEvent):void
		{
			comboBoxTop.dataProvider=topSelectorService.model.tops;
			
			// tester si le comboBox contient au moin de 2 elements 
			if(topSelectorService.model.tops.length<2)
			{
				visible =false;
				includeInLayout = false;
			}
			else
			{
				visible = true;
				includeInLayout = true;
			}
			showHelp();
		}
		
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
			var parametre :Parametre=  new Parametre();
			parametre.value=[(comboBoxTop.selectedItem as ValeurSelector).valeur];					
			parametre.cle =arrayCleLibelle[0].NOM_CLE;
			parametre.libelle=arrayCleLibelle[0].LIBELLE_CLE;
			parametre.is_system=arrayCleLibelle[0].IS_SYSTEM;
			parametre.typeValue=this.typeParametreComposant.toUpperCase();
			
			tabValeur[0]=parametre;
			return tabValeur;
		}
	}
}