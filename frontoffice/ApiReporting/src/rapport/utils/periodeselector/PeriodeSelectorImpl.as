package rapport.utils.periodeselector
{
	import mx.collections.ArrayCollection;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;
	
	import rapport.event.RapportEvent;
	import rapport.event.RapportIHMEvent;
	import rapport.service.periodeselector.PeriodeSelectorService;
	import rapport.utils.SelectorAbstract;
	import rapport.utils.serialization.json.JSON;
	import rapport.vo.Parametre;
	import rapport.vo.SelectorParametre;
	
	public class PeriodeSelectorImpl extends SelectorAbstract
	{
	    [Bindable]public var periode:PeriodeSelector;
		[Bindable]public var periodeSelectorService:PeriodeSelectorService;
		[Bindable]public var clesPeriode:ArrayCollection;
		[Bindable]public var myPeriodeLbl:Label;
		
		public var arrayCleLibelle :Object;
		
		[Bindable]public var selectedLibelleMoisDebut:String;
		[Bindable]public var selectedLibelleMoisFin:String;
		
		
		public function PeriodeSelectorImpl(selector:Object=null)
		{
			super(selector);		
		}
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
			}
		}
		protected function periode_creationCompleteHandler(event:FlexEvent):void
		{
			periodeSelectorService=new PeriodeSelectorService();
			periodeSelectorService.model.addEventListener(RapportEvent.USER_PERIODE_SELECTOR_EVENT,fillData);
			periode.addEventListener(RapportIHMEvent.CHANGER_VALUE_HSLIDER_BIS_EVENT,getLibelleSelectedPeriode);
			periodeSelectorService.getValuesPeriode();
			showHelp();
			
		}
		private function fillData(event:RapportEvent):void
		{
			var valuesPeriode: ArrayCollection=periodeSelectorService.model.getValuesPeriode;
			periode.setValuePeriode(valuesPeriode);// permert d'initialiser les valeurs du selecteur
			getLibelleSelectedPeriode(null);// pour afficher le libelle lors de la creation
		}
		
		
		private function getLibelleSelectedPeriode(event:RapportIHMEvent):void
		{	
			selectedLibelleMoisDebut = periode.getSelectedLibellePeriode().datePeriodeDebut;
			selectedLibelleMoisFin =periode.getSelectedLibellePeriode().datePeriodeFin; 
			myPeriodeLbl.text=ResourceManager.getInstance().getString('ApiReporting', 'Du__') 
								+ ' ' + selectedLibelleMoisDebut 
								+ ' ' + ResourceManager.getInstance().getString('ApiReporting', '_au_') 
								+ ' ' + selectedLibelleMoisFin;
		}
		
		private function foramterDateFr(dateAformater :String):String
		{
			var firstDate:Date
			var array:Array;
			
			array = dateAformater.split('/');
			
			firstDate = new Date(array[2],array[1]);
			
			var thisMonth : Date = new Date(firstDate.getFullYear(),firstDate.getMonth()-1);
			
			var df: DateFormatter = new DateFormatter();
			
			df.formatString = "MM-YYYY";			
			
			return df.format(thisMonth);
		}
		
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
			var is_system:Boolean;
			var valueParam:Array=[];
			
			var selectedDateDebut:String = periode.getSelectedDate().IdMoisDebut;
			var selectedDateFin:String = periode.getSelectedDate().IdMoisFin;
			
			var selectedMoisDebut :String = foramterDateFr(selectedLibelleMoisDebut);
			var selectedMoisFin :String =foramterDateFr(selectedLibelleMoisFin); 
			
			
			var periode :String = selectedMoisDebut+ '-' + selectedMoisFin;
			
			valueParam.push(selectedDateDebut);
			valueParam.push(selectedDateFin);
			valueParam.push(periode);
			
			for(var i : int = 0 ; i< arrayCleLibelle.length ; i++)
			{
				var parametre :Parametre=  new Parametre();
				
				if(arrayCleLibelle[i].IS_SYSTEM== 1)
					is_system=true;
				else
					is_system=false;
				
				parametre.value=[valueParam[i]];	//arrayCleLibelle[i].NOM_CLE=U_IDPERIODE_DEBUT
				parametre.is_system = is_system;
				parametre.cle =arrayCleLibelle[i].NOM_CLE;
				parametre.libelle=arrayCleLibelle[i].LIBELLE_CLE;
				parametre.typeValue=this.typeParametreComposant.toUpperCase();
				
				tabValeur.push(parametre);
			}
			return tabValeur;
		}
	}
}