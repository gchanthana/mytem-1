package rapport.utils.noSelector
{
	import rapport.utils.SelectorAbstract;
	import rapport.utils.serialization.json.*;
	import rapport.vo.Parametre;
	import rapport.vo.SelectorParametre;
	
	public class NoSelector extends SelectorAbstract
	{
		public var arrayCleLibelle :Object;
		
		public var valueDefaut: String;
		
		public function NoSelector(selector:Object=null)
		{
			super(selector);	
		}
		
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				valueDefaut=selector.valueDefaut;
				
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
			}
		}
		
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
			
			var parametre :Parametre=  new Parametre();
			
			parametre.value=[valueDefaut];
			parametre.cle =arrayCleLibelle[0].NOM_CLE;
			parametre.libelle=arrayCleLibelle[0].LIBELLE_CLE;
			parametre.is_system=arrayCleLibelle[0].IS_SYSTEM;
			parametre.typeValue=this.typeParametreComposant.toUpperCase();
			
			tabValeur[0]=parametre;
					
			return tabValeur;
		}
	}
}