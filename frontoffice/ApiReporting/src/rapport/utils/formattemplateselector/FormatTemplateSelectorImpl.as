package rapport.utils.formattemplateselector
{
	import mx.containers.HBox;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import rapport.event.RapportEvent;
	import rapport.event.TemplateEvent;
	import rapport.service.formattemplatetelector.FormatTemplateSelectorService;
	import rapport.utils.SelectorAbstract;
	import rapport.utils.serialization.json.*;
	import rapport.vo.Parametre;
	import rapport.vo.SelectorParametre;
	
	public class FormatTemplateSelectorImpl extends SelectorAbstract   
	{	
		[Bindable]public var formatTemplateSelectorService :FormatTemplateSelectorService;
		[Bindable]public var comboBoxFormatTemplate:ComboBox;
		public var hBoxFormatTemplate :HBox;
		
		private var id_rapport:String;
		
		public var idTemplate:int=-1;
		
		public var arrayCleLibelle :Object;
		
		public function FormatTemplateSelectorImpl(selector:Object=null)
		{
			super(selector);	
		}
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
			}
		}
		
		protected function creationCompleteHandler(event:FlexEvent):void
		{
			formatTemplateSelectorService=new FormatTemplateSelectorService();
			systemManager.addEventListener(TemplateEvent.DATA_TRANSFER,handleDataTransfer, true);
			formatTemplateSelectorService.model.addEventListener(RapportEvent.USER_FORMAT_SELECTOR_EVENT,fillData);
			formatTemplateSelectorService.getValuesFormatTemplate(selectedRapport.idRapportRacine,idTemplate);
		}
		
		private function handleDataTransfer(evt:TemplateEvent):void
		{
			idTemplate=evt.idTemplate;
			formatTemplateSelectorService.getValuesFormatTemplate(selectedRapport.id,idTemplate);
		}
		
		private function fillData(event:RapportEvent):void
		{
			comboBoxFormatTemplate.dataProvider=formatTemplateSelectorService.model.formats;
						
			showHelp();
		}
		
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
			var parametre :Parametre=  new Parametre();
			
			parametre.value=[comboBoxFormatTemplate.selectedItem.valeur];					
			parametre.cle =arrayCleLibelle[0].NOM_CLE;
			parametre.libelle=arrayCleLibelle[0].LIBELLE_CLE;
			parametre.is_system=arrayCleLibelle[0].IS_SYSTEM;
			parametre.typeValue=this.typeParametreComposant.toUpperCase();
			
			tabValeur[0]=parametre;
			return tabValeur;
		}
		
	}
}