package rapport.vo
{
	[Bindable]
	public class SelectorParametre
	{
		private var _libelle : String;
		private var _typeSelecteur:String;
		private var _description : String;
		private var _messageAide: String;
		private var _is_system :Boolean ;
		private var _cleJson:String;
		private var _typeParametre:String;
		private var _idRapportParam:int;
		private var _is_hidden:Boolean;
		private var _valueDefaut: String;
		
		public function SelectorParametre(){}
		
	
		public function fill(selector:Object):void
		{
			this._libelle=selector.LIBELLE_IHM;
			this._typeSelecteur=selector.TYPE_SELECTEUR;
			this._description=selector.DESCRIPTION_IHM;
			this._messageAide=selector.MESSAGE_AIDE_IHM;
			this._cleJson=selector.CLE_JSON;
			this._valueDefaut=selector.DEFAULT_VALUE;
			
			if(selector.IS_SYSTEM == 1)
				this._is_system=true;
			else
				this._is_system=false;
			
			if(selector.IS_HIDDEN== 1)
				this._is_hidden=true;
			else
				this._is_hidden=false;
			
			this._typeParametre=selector.TYPE_PARAMETRE ;
			this._idRapportParam=selector.IDRAPPORT_PARAMETRE ;
		}
		
		public function get libelle():String
		{
			return _libelle;
		}

		public function set libelle(value:String):void
		{
			_libelle = value;
		}

		public function get typeSelecteur():String
		{
			return _typeSelecteur;
		}

		public function set typeSelecteur(value:String):void
		{
			_typeSelecteur = value;
		}

		public function get description():String
		{
			return _description;
		}

		public function set description(value:String):void
		{
			_description = value;
		}

		public function get messageAide():String
		{
			return _messageAide;
		}

		public function set messageAide(value:String):void
		{
			_messageAide = value;
		}
		
		public function get is_system():Boolean
		{
			return _is_system;
		}
		
		public function set is_system(value:Boolean):void
		{
			_is_system = value;
		}
		public function get typeParametre():String
		{
			return _typeParametre;
		}
		
		public function set typeParametre(value:String):void
		{
			_typeParametre = value;
		}
		
		public function get idRapportParam():int
		{
			return _idRapportParam;
		}
		
		public function set idRapportParam(value:int):void
		{
			_idRapportParam = value;
		}
		
		public function get cleJson():String
		{
			return _cleJson;
		}
		
		public function set cleJson(value:String):void
		{
			_cleJson = value;
		}
		public function get is_hidden():Boolean
		{
			return _is_hidden;
		}
		
		public function set is_hidden(value:Boolean):void
		{
			_is_hidden = value;
		}
		
		public function get valueDefaut():String
		{
			return _valueDefaut;
		}
		
		public function set valueDefaut(value:String):void
		{
			_valueDefaut = value;
		}
	}
}