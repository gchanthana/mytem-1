package rapport.vo
{
	[Bindable]
	public class Produit
	{
		
		private var _id:Number;
		private var _label:String;
		private var _isSelected:Boolean;
		private var _typeTheme:Number; // 1 type Abo , 2 type conso
		private var _labelTypeTheme:String;
		private var _theme:String;
		
		public function Produit(){}

		public function fill(produit:Object):void
		{
			this._id=produit.IDPRODUIT_CATALOGUE;
			this._label=produit.LIBELLE_PRODUIT;
			this._labelTypeTheme=produit.TYPE_THEME;
			this._theme=produit.THEME;
			
			if(_labelTypeTheme=='Consommations')
			{
				this._typeTheme=2;
			}
			else
			{
				this._typeTheme=1	
			}
		}
		
		public function get typeTheme():Number
		{
			return _typeTheme;
		}

		public function set typeTheme(value:Number):void
		{
			_typeTheme = value;
		}

		public function get id():Number
		{
			return _id;
		}

		public function set id(value:Number):void
		{
			_id = value;
		}

		public function get label():String
		{
			return _label;
		}

		public function set label(value:String):void
		{
			_label = value;
		}
		public function get isSelected():Boolean
		{
			return _isSelected;
		}
		
		public function set isSelected(value:Boolean):void
		{
			_isSelected = value;
		}
		
		public function get theme():String
		{
			return _theme;
		}
		
		public function set theme(value:String):void
		{
			_theme = value;
		}
		
		public function get labelTypeTheme():String
		{
			return _labelTypeTheme;
		}
		
		public function set labelTypeTheme(value:String):void
		{
			_labelTypeTheme = value;
		}
	}
}