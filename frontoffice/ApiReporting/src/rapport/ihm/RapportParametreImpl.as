package rapport.ihm
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import mx.containers.Form;
	import mx.containers.VBox;
	import mx.controls.Button;
	
	import rapport.event.RapportIHMEvent;
	import rapport.utils.templateselector.TemplateSelectorIHM;
	
	public class RapportParametreImpl extends VBox
	{
		public var formComposants:Form;	
		public var valider_execution:Button;
		public var templateSelector :TemplateSelectorIHM;
	
		public function RapportParametreImpl()
		{
			super();
		}
		public function button1_clickHandler(event:MouseEvent):void
		{
			this.dispatchEvent(new RapportIHMEvent(RapportIHMEvent.VALIDER_CLICK_EVENT));
		}
		
		//  initialiser les selecteurs des paramètres sur l'etape deux
		
		public function resetDataIHMEtape2():void
		{	
			while(formComposants.numChildren >0)
			{
				var child:DisplayObject=formComposants.getChildAt(0); // l'index de l'element reste toujours 0 apères chaque suppression
				formComposants.removeChildAt(0);
				child=null;
				valider_execution.enabled=false;	
			}
			// pour initialiser la data grid des rapports
			dispatchEvent(new RapportIHMEvent(RapportIHMEvent.INITIALIZE_CLICK_EVENT));
		}
	   
	}
}