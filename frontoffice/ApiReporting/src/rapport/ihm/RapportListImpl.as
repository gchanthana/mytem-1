package rapport.ihm
{
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	
	import paginatedatagrid.PaginateDatagrid;
	import paginatedatagrid.event.PaginateDatagridEvent;
	
	import rapport.event.RapportEvent;
	import rapport.event.RapportIHMEvent;
	import rapport.service.chercherurlpdf.RechercherUrlPdfService;
	import rapport.vo.Rapport;
	
	public class RapportListImpl extends HBox
	{
		[Bindable]public var dataProviderRapport:ArrayCollection=new ArrayCollection();
		[Bindable]public var rapportDataGrid:PaginateDatagrid;
		[Bindable]public var rechercherUrlPdf :RechercherUrlPdfService;
		
		public static var urlPdf:String;
		
		private var _offset :int=1;
		
		public function RapportListImpl()
		{
			super();
		}
		
		
		protected function rapportlistimpl_creationCompleteHandler(event:FlexEvent):void
		{
			rechercherUrlPdf=new RechercherUrlPdfService();
			rechercherUrlPdf.model.addEventListener(RapportEvent.USER_URL_PDF_RAPPORT_EVENT,getUrlPdf);
			rechercherUrlPdf.getUrlPdf();
			
		}
		
		private function getUrlPdf(event:RapportEvent):void
		{
			RapportListImpl.urlPdf=rechercherUrlPdf.model.urlPDF;
		}
		
		// vider la data grid 

		public function initialiserDataGrid():void
		{
			if(rapportDataGrid.dataprovider!=null)
			{
				rapportDataGrid.txtFiltre.text="";
				rapportDataGrid.dataprovider.removeAll();
				rapportDataGrid.selectedItem=null;
				rapportDataGrid.paginationBox.selectFirstIntervalle();// remettre l'index de départ à 0
	
			}
		}
		public function getRapportSelected():int
		{
			var IdRapport:int=(rapportDataGrid.selectedItem as Rapport).id;
			return IdRapport;
		}
		public function initPagination(nbRec:Number):void
		{
			rapportDataGrid.paginationBox.initPagination(nbRec,10); // le nombre des items dans la liste des page 1-10
		}
		
		protected function onIntervalleChangeHandler(evt:PaginateDatagridEvent):void
		{
			if(rapportDataGrid.currentIntervalle!=null)
			{
				if (rapportDataGrid.currentIntervalle.indexDepart == 0)
				{
					offset=1;
				}
				else
				{		
					offset=rapportDataGrid.currentIntervalle.indexDepart+1;
				}
				if(!ModuleRapport.isClose)
				{
					dispatchEvent(new RapportIHMEvent(RapportIHMEvent.CHANGER_INDEXDEPART_RAPPORT_EVENT));
				}
			}
		}
		
		public function get offset():int
		{
			return _offset;
		}
		
		public function set offset(value:int):void
		{
			_offset = value;
		}
	}
}