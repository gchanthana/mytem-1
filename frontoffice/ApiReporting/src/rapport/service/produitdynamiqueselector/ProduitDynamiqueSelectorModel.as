package rapport.service.produitdynamiqueselector
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import rapport.event.RapportEvent;
	import rapport.vo.Produit;

	public class ProduitDynamiqueSelectorModel extends EventDispatcher
	{
		
		private var _produits :ArrayCollection;
		
		public function ProduitDynamiqueSelectorModel()
		{
			_produits = new ArrayCollection();	
		}
		
		public function get produits():ArrayCollection
		{
			return _produits;
		}
		
		internal function updateListeProduits(value:ArrayCollection):void
		{
			_produits.removeAll();
			
			var newProduit:Produit;	
			
			if(value.length > 0)
			{
				for(var i:int=0;i<value.length;i++)
				{
					newProduit = new Produit();
					newProduit.fill(value[i]);
					_produits.addItem(newProduit); 	
				}	
			}
			
			dispatchEvent(new RapportEvent(RapportEvent.USER_LIST_PRODUIT_EVENT));	
		}
	}
}