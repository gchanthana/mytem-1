package rapport.service.produitdynamiqueselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ProduitDynamiqueSelectorHandler
	{
		private var  _model:ProduitDynamiqueSelectorModel;
		
		public function ProduitDynamiqueSelectorHandler(_model:ProduitDynamiqueSelectorModel)
		{
			this._model = _model;
		}
		
		internal function getListeProduitResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateListeProduits(evt.result as ArrayCollection);
			}
			
		}
	}
}