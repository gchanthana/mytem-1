package rapport.service.produitdynamiqueselector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	public class ProduitDynamiqueSelectorService
	{
		
		private var _model:ProduitDynamiqueSelectorModel;
		public var handler:ProduitDynamiqueSelectorHandler;
		
		public function ProduitDynamiqueSelectorService()
		{
			this._model=new ProduitDynamiqueSelectorModel();
			this.handler=new ProduitDynamiqueSelectorHandler(_model);
		}

		public function get model():ProduitDynamiqueSelectorModel
		{
			return _model;
		}
		
		public function getListeProduit():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.selector.GetListeProduit","getListeProduits",handler.getListeProduitResultHandler); 
			
			RemoteObjectUtil.callService(op);// appel au service coldfusion
			
		}
		

	}
}