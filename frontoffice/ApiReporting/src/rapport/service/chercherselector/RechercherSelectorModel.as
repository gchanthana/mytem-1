package  rapport.service.chercherselector
{
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import rapport.event.RapportEvent;
	import rapport.vo.SelectorParametre;

	internal class RechercherSelectorModel  extends EventDispatcher
	{
		private var _selectors :ArrayCollection;
		
		public function RechercherSelectorModel()
		{
			_selectors = new ArrayCollection();	
		}
		public function get selectors():ArrayCollection
		{
			return _selectors;
		}
		
		internal function updateSelector(value:ArrayCollection):void
		{	
			_selectors.removeAll();// vider l'ArrayCollection
			
			var selectorParam:SelectorParametre;			
			
			for(var i:int=0;i<value.length;i++)
			{
				selectorParam = new SelectorParametre();
				selectorParam.fill(value[i]);
				_selectors.addItem(selectorParam); 
			}
			this.dispatchEvent(new RapportEvent(RapportEvent.USER_SELECTOR_UPDATED_EVENT));
		}
	}
}