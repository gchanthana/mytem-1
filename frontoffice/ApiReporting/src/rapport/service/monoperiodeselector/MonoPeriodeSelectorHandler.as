package rapport.service.monoperiodeselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class MonoPeriodeSelectorHandler
	{
		private var _model: MonoPeriodeSelectorModel;
		
		public function MonoPeriodeSelectorHandler(_model:MonoPeriodeSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValuesMonoPeriodeHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateValues(event.result as ArrayCollection);
			}
		}	
	}
}