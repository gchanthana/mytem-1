package rapport.service.periodeselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class PeriodeSelectorHandler
	{
		private var _model: PeriodeSelectorModel;
		
		public function PeriodeSelectorHandler(_model:PeriodeSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValuesPeriodeHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateValues(event.result as ArrayCollection);
			}
		}
	}
}