package rapport.service.periodeselector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import rapport.service.module.*;

	public class PeriodeSelectorService
	{
		
		private var _model:PeriodeSelectorModel;
		public var handler:PeriodeSelectorHandler;
		
		public function PeriodeSelectorService()
		{
			this._model = new PeriodeSelectorModel();
			this.handler = new PeriodeSelectorHandler(model);
		}		
		public function get model():PeriodeSelectorModel
		{
			return this._model;
		}
		
		public function getValuesPeriode():void
		{				
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.selector.MonoPeriodeSelectorService","getValuesMonoPeriode",handler.getValuesPeriodeHandler); 
			
			RemoteObjectUtil.callService(op);// appel au service coldfusion qui contient des fonctions pour les opérations de service
		}
	}
}