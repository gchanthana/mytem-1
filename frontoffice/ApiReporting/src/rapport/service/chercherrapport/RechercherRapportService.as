package rapport.service.chercherrapport
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	
	import rapport.service.chercherrapport.*;

	public class RechercherRapportService
	{
		private var _model:RechercherRapportModel;
		public var handler:RechercherRapportHandler;	
		private var _showAll:int=1;
		
		public function RechercherRapportService()
		{
			this._model  = new RechercherRapportModel();
			this.handler = new RechercherRapportHandler(model);
		}		
		
		public function get showAll():int
		{
			return _showAll;
		}

		public function set showAll(value:int):void
		{
			_showAll = value;
		}

		public function get model():RechercherRapportModel
		{
			return this._model;
		}
		/*
		** recupérer les rapports selon les critères choisi
		** idModule : l'id de module
		** mot : le texte tapé dans la zone de  texte
		** segment : fixe, mobile ,
		** typeRapport : le type de rapport (standart ou sepécifique)
		*/	
		public function getRapport(idModule:Number,text_filtre:String,segment:String,typeRapport:Number,listeTypeAnalyse :ArrayCollection,
								   listeTypeAgregation:ArrayCollection,indexDepart:int):void
		{	
			// RechercherRapportService : nom du fichier cfc 
			//getRapport nom fonction dans le fichier cfc
			//getRechercherRapportResultHandler : une methode declarer dans le handler 
	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.RechercherRapportService","getRapport",handler.getRechercherRapportResultHandler); 
			
			RemoteObjectUtil.callService(op,idModule,text_filtre,segment,typeRapport,listeTypeAnalyse.source,listeTypeAgregation.source,indexDepart,showAll);// appel au service coldfusion
			
		}
	}
}