package rapport.service.periodecomboboxselector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import rapport.service.module.*;

	public class PeriodeComboBoxSelectorService
	{
		
		private var _model:PeriodeComboBoxSelectorModel;
		public var handler:PeriodeComboBoxSelectorHandler;
		
		public function PeriodeComboBoxSelectorService():void
		{
			this._model = new PeriodeComboBoxSelectorModel();
			this.handler = new PeriodeComboBoxSelectorHandler(model);
		}		
		public function get model():PeriodeComboBoxSelectorModel
		{
			return this._model;
		}
		
		public function getValuesPeriode(idRapportParametre:int):void
		{				
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.selector.PeriodeComboBoxSelectorService","getValuesPeriode",handler.getValuesPeriodeHandler); 
			
			RemoteObjectUtil.callService(op,idRapportParametre);// appel au service coldfusion qui contient des fonctions pour les opérations de service
		}
	}
}