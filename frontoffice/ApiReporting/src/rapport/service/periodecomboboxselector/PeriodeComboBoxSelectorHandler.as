package rapport.service.periodecomboboxselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class PeriodeComboBoxSelectorHandler
	{
		private var _model: PeriodeComboBoxSelectorModel;
		
		public function PeriodeComboBoxSelectorHandler(_model:PeriodeComboBoxSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValuesPeriodeHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateValues(event.result as ArrayCollection);
			}
		}
		
	}
}