package rapport.service.topselector
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import rapport.event.RapportEvent;
	import rapport.vo.ValeurSelector;

	internal class TopSelectorModel  extends EventDispatcher
	{
		private var _tops :ArrayCollection;
		
		public function TopSelectorModel():void
		{
			_tops = new ArrayCollection();	
		}
		public function get tops():ArrayCollection
		{
			return _tops;
		}
		
		internal function updateValues(value:ArrayCollection):void
		{	
			_tops.removeAll();// vider l'ArrayCollection
			var valeurSelector:ValeurSelector;
			
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelector();
				valeurSelector.fill(value[i]);
				_tops.addItem(valeurSelector); 
			}
			this.dispatchEvent(new RapportEvent(RapportEvent.USER_TOPS_SELECTOR_EVENT));
		}
	}
}