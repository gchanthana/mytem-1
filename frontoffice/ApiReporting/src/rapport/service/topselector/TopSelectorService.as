package rapport.service.topselector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import rapport.service.formattemplatetelector.*;

	public class TopSelectorService
	{
		
		private var _model:TopSelectorModel;
		public var handler:TopSelectorHandler;
		
		public function TopSelectorService():void
		{
			this._model = new TopSelectorModel();
			this.handler = new TopSelectorHandler(model);
		}		
		
		
		public function get model():TopSelectorModel
		{
			return this._model;
		}
		
		public function getValuesTop():void
		{				
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.selector.TopSelectorService","getValuesTop",handler.getValuesTopHandler); 
			
			RemoteObjectUtil.callService(op);// appel au service coldfusion qui contient des fonctions pour les opérations de service
		}
		
		
	}
}