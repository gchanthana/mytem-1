package rapport.service.templateselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class TemplateSelectorHandler
	{
		private var _model: TemplateSelectorModel;
		
		public function TemplateSelectorHandler(_model:TemplateSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValuesTemplateHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateValues(event.result as ArrayCollection);
			}
		}	
	}
}