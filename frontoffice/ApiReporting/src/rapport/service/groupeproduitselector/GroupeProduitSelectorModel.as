package rapport.service.groupeproduitselector
{
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import rapport.event.RapportEvent;
	import rapport.vo.ValeurSelector;

	internal class GroupeProduitSelectorModel  extends EventDispatcher
	{
		private var _produits :ArrayCollection;
		
		public function GroupeProduitSelectorModel():void
		{
			_produits = new ArrayCollection();
			
		}
		public function get produits():ArrayCollection
		{
			return _produits;
		}
		
		//permert de remplir l'array collection _produits 
		
		internal function updateValues(value:ArrayCollection):void
		{	
			_produits.removeAll();// vider l'ArrayCollection
		
			var valeurSelector:ValeurSelector;			
			
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelector();
				valeurSelector.fill(value[i]);
				_produits.addItem(valeurSelector); 
			}	
			this.dispatchEvent(new RapportEvent(RapportEvent.USER_GROUPE_PRODUIT_SELECTOR_EVENT));
		}
	}
}