package rapport.service.groupeproduitselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class GroupeProduitSelectorHandler
	{
		private var _model: GroupeProduitSelectorModel;
		
		public function GroupeProduitSelectorHandler(_model:GroupeProduitSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValuesGroupeProduitHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateValues(event.result as ArrayCollection);
			}
		}
		
	}
}