package rapport.service.periodiciteselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class PeriodiciteSelectorHandler
	{
		private var _model: PeriodiciteSelectorModel;
		
		public function PeriodiciteSelectorHandler(_model:PeriodiciteSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValuesPeriodiciteHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateValues(event.result as ArrayCollection);
			}
		}
		
	}
}