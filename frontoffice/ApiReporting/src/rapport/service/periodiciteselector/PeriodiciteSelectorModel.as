package rapport.service.periodiciteselector
{
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import rapport.event.RapportEvent;
	import rapport.vo.ValeurSelector;

	internal class PeriodiciteSelectorModel  extends EventDispatcher
	{
		private var _periodicite :ArrayCollection;
		
		public function PeriodiciteSelectorModel():void
		{
			_periodicite = new ArrayCollection();
			
		}
		public function get periodicite():ArrayCollection
		{
			return _periodicite;
		}
		
		//permert de remplir l'array collection _produits 
		
		internal function updateValues(value:ArrayCollection):void
		{	
			_periodicite.removeAll();// vider l'ArrayCollection
		
			var valeurSelector:ValeurSelector;			
			
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelector();
				valeurSelector.fill(value[i]);
				_periodicite.addItem(valeurSelector); 
			}	
			this.dispatchEvent(new RapportEvent(RapportEvent.USER_PERIODICITE_SELECTOR_EVENT));
		}
	}
}