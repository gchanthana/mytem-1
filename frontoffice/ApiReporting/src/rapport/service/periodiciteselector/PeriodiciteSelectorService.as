package rapport.service.periodiciteselector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	import rapport.service.module.*;

	public class PeriodiciteSelectorService
	{
		
		private var _model:PeriodiciteSelectorModel;
		public var handler:PeriodiciteSelectorHandler;
		
		public function PeriodiciteSelectorService():void
		{
			this._model = new PeriodiciteSelectorModel();
			this.handler = new PeriodiciteSelectorHandler(model);
		}		
		public function get model():PeriodiciteSelectorModel
		{
			return this._model;
		}
		
		public function getValuesPeriodicite(idRapportRacine:int):void
		{				
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.selector.PeriodiciteSelectorService","getValuesPeriodicite",handler.getValuesPeriodiciteHandler); 
			
			RemoteObjectUtil.callService(op,idRapportRacine);// appel au service coldfusion qui contient des fonctions pour les opérations de service
		}
	}
}