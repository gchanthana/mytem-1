package rapport.service.module
{
	import mx.rpc.events.ResultEvent;

	internal class ModuleHandler
	{
		private var _model:ModuleModel;
		
		public function ModuleHandler(_model:ModuleModel):void
		{
			this._model = _model;
		}
		
		internal function getModuleResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateModule(event.result as Array);
			}
		}	
	}
}