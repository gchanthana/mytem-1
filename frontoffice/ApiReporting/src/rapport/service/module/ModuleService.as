package rapport.service.module
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import rapport.service.module.*;

	public class ModuleService
	{
		private var _model:ModuleModel;
		public var handler:ModuleHandler;
		
		
		public function ModuleService()
		{
			this._model = new ModuleModel();
			this.handler = new ModuleHandler(model);
		}		
		
		public function get model():ModuleModel
		{
			return this._model;
		}
		
		public function getModule():void
		{	
			// ModuleService : nom du fichier cfc 
			//getModule nom fonction dans le fichier cfc
			//getModuleResultHandler : une methode declarer dans le handler 
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.ModuleService","getModule",handler.getModuleResultHandler); 
			
			RemoteObjectUtil.callService(op);// appel au service coldfusion qui contient des fonctions pour les opérations de service
		}		
	}
}