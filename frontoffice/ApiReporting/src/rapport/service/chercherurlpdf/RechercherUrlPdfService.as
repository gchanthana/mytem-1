package rapport.service.chercherurlpdf
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;

	public class RechercherUrlPdfService
	{
		private var _model:RechercherUrlPdfModel;
		public var handler:RechercherUrlPdfHandler;	
		
		public function RechercherUrlPdfService()
		{
			this._model  = new RechercherUrlPdfModel();
			this.handler = new RechercherUrlPdfHandler(model);
		}		
		
		public function get model():RechercherUrlPdfModel
		{
			return this._model;
		}
		
		public function getUrlPdf():void
		{	
	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.RechercherUrlPdfService","getUrlPdf",handler.getUrlPdfResultHandler); 
			
			RemoteObjectUtil.callService(op);// appel au service coldfusion
			
		}
	}
}