package rapport.service.chercherurlpdf 
{
	import mx.rpc.events.ResultEvent;

	internal class RechercherUrlPdfHandler
	{
		private var _model:RechercherUrlPdfModel;
		
		public function RechercherUrlPdfHandler(_model:RechercherUrlPdfModel):void
		{
			this._model = _model;
		}
		
		internal function getUrlPdfResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateUrlPdf(event.result as String);
			}
		}	
	}
}