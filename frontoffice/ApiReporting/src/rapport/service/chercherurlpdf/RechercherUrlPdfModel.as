package rapport.service.chercherurlpdf
{
	import flash.events.EventDispatcher;
	import rapport.event.RapportEvent;
	import rapport.vo.Rapport;

	internal class RechercherUrlPdfModel  extends EventDispatcher
	{
		private var _urlPDF :String;
		
		public function RechercherUrlPdfModel()
		{
			_urlPDF = new String();	
		}
		public function get urlPDF():String
		{
			return _urlPDF;
		}
		
		internal function updateUrlPdf(value:String):void
		{	
						
			var newRapport:Rapport;			
			
			if(value.length > 0)
			{
				_urlPDF=value;
			}
			else
			{
				_urlPDF="";
			}
			
			dispatchEvent(new RapportEvent(RapportEvent.USER_URL_PDF_RAPPORT_EVENT));
		}
	}
}