package service.destinataires.destinataireprofil
{
	import mx.rpc.events.ResultEvent;
	

	internal class DestinataireProfilHandler
	{
		
		private var _model:DestinataireProfilModel;
		
		public function DestinataireProfilHandler(_model:DestinataireProfilModel):void
		{
			this._model = _model;
		}
		
		internal function getAssocierResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.getResultHandler(event);
			}
		}	
		internal function getRemoveResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.getRemoveResultHandler(event);
			}
		}
	}
}