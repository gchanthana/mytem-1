package service.destinataires.destinataireprofil
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class DestinataireProfilService
	{
		private var _model:DestinataireProfilModel;
		public var handler:DestinataireProfilHandler;	
		
		public function DestinataireProfilService()
		{
			this._model  = new DestinataireProfilModel();
			this.handler = new DestinataireProfilHandler(model);
		}		
		
		public function get model():DestinataireProfilModel
		{
			return this._model;
		}
		

		public function associerDestinataireProfil(idDestinataire:int,idProfil:int,idNoeud:int):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M22.service.destinataires.destProfilService","associerProfilDest",handler.getAssocierResultHandler); 
			
			RemoteObjectUtil.callService(op,idDestinataire,idProfil,idNoeud);
		}
		
		public function removeDestinataireProfil(idDestinataire:int,idProfil:int,idNoeud:int):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M22.service.destinataires.destProfilService","removeProfilDest",handler.getRemoveResultHandler); 
			
			RemoteObjectUtil.callService(op,idDestinataire,idProfil,idNoeud);
		}
	}
}