package service.destinataires.ajoutermodifierdestinataire
{
	import mx.rpc.events.ResultEvent;
	

	internal class AjouterModifierDestinataireHandler
	{
		private var _model:AjouterModifierDestinataireModel;
		
		public function AjouterModifierDestinataireHandler(_model:AjouterModifierDestinataireModel):void
		{
			this._model = _model;
		}
		
		internal function ajouterModifierResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.ajouterModifierResult(event);
			}
		}	
		internal function supprimerResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.supprimerResult(event);
			}
		}
		internal function supprimerPlusieursResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.supprimerPlusieursResult(event);
			}
		}
		
	}
}