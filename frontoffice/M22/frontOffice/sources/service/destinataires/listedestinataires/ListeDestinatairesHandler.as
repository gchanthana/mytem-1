package service.destinataires.listedestinataires 
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ListeDestinatairesHandler
	{
		private var _model:ListeDestinatairesModel;
		
		public function ListeDestinatairesHandler(_model:ListeDestinatairesModel):void
		{
			this._model = _model;
		}
		
		internal function getListeDestinatairesResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.listeDestinataires(event.result as ArrayCollection);
			}
		}	
	}
}