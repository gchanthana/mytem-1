package service.destinataires.listedestinatairenoeud
{
	import event.DestinataireEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.DestinataireVo;
	

	internal class ListeDestinatairesNoeudModel  extends EventDispatcher
	{
		private var _destinatairesNoeud :ArrayCollection;
		
		public function ListeDestinatairesNoeudModel()
		{
			_destinatairesNoeud = new ArrayCollection();	
		}
		public function get destinatairesNoeud():ArrayCollection
		{
			return _destinatairesNoeud;
		}
		
		internal function listeDestinatairesNoeud(value:ArrayCollection):void
		{	
			_destinatairesNoeud.removeAll();// vider l'ArrayCollection
			
			var unDestinataire:DestinataireVo;	
			
			if(value.length > 0)
			{
				for(var i:int=0;i<value.length;i++)
				{
					unDestinataire = new DestinataireVo();
					unDestinataire.fill(value[i]);
					_destinatairesNoeud.addItem(unDestinataire); 
				}
			}			
			this.dispatchEvent(new DestinataireEvent(DestinataireEvent.LISTE_DESTINATAIRE_NOEUD_EVENT));
		}
	}
}