package service.destinataires.associedestinatairenoeud
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	import mx.rpc.AbstractOperation;

	public class AssocierDestinataireNoeudService
	{
		private var _model:AssocierDestinataireNoeudModel;
		public var handler:AssocierDestinataireNoeudHandler;

		public function AssocierDestinataireNoeudService()
		{
			this._model=new AssocierDestinataireNoeudModel();
			this.handler=new AssocierDestinataireNoeudHandler(model);
		}

		public function get model():AssocierDestinataireNoeudModel
		{
			return this._model;
		}

		public function associerDestinataireNoeud(idDestinataire:int, idNoeud:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M22.service.destinataires.associerDestNoeudService", "associerDestNoeud", handler.getAssocierResultHandler);

			RemoteObjectUtil.callService(op, idDestinataire, idNoeud); // appel au service coldfusion 
		}

		public function associerPlusieursDestinataireNoeud(listeIdDest:String, idNoeud:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M22.service.destinataires.associerDestNoeudService", "associerPlusieurDestNoeud", handler.getAssociationResultHandler);

			RemoteObjectUtil.callService(op, listeIdDest, idNoeud); // appel au service coldfusion 
		}


		public function removeDestinataireNoeud(idDestinataire:int, idNoeud:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M22.service.destinataires.associerDestNoeudService", "deleteDestNoeud", handler.getRemoveResultHandler);

			RemoteObjectUtil.callService(op, idDestinataire, idNoeud);
		}
	}
}
