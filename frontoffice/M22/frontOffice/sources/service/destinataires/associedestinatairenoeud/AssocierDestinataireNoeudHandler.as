package service.destinataires.associedestinatairenoeud
{
	import mx.rpc.events.ResultEvent;


	internal class AssocierDestinataireNoeudHandler
	{

		private var _model:AssocierDestinataireNoeudModel;

		public function AssocierDestinataireNoeudHandler(_model:AssocierDestinataireNoeudModel):void
		{
			this._model=_model;
		}

		internal function getAssocierResultHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.getResultHandler(event);
			}
		}

		internal function getAssociationResultHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.getAssociationResultHandler(event);
			}
		}

		internal function getRemoveResultHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.getRemoveResultHandler(event);
			}
		}
	}
}
