package service.destinataires.listedestinataireprofil
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;


	public class ListeDestinatairesProfilService
	{
		private var _model:ListeDestinatairesProfilModel;
		public var handler:ListeDestinatairesProfilHandler;	
		
		public function ListeDestinatairesProfilService()
		{
			this._model  = new ListeDestinatairesProfilModel();
			this.handler = new ListeDestinatairesProfilHandler(model);
		}		
		
		public function get model():ListeDestinatairesProfilModel
		{
			return this._model;
		}

		public function getListeDestinatairesProfil(idDestinataire:Number):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M22.service.destinataires.listeDestinataireProfil","ListeDestProfil",handler.getListeDestinatairesProfilResultHandler); 
			
			RemoteObjectUtil.callService(op,idDestinataire);// appel au service coldfusion
			
		}
	}
}