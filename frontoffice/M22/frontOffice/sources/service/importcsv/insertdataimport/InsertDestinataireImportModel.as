package service.importcsv.insertdataimport
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	import event.DestinataireEvent;
	import event.ImportEvent;
	import flash.events.EventDispatcher;
	import mx.rpc.events.ResultEvent;

	public class InsertDestinataireImportModel extends EventDispatcher
	{
		public function InsertDestinataireImportModel()
		{
		}
		
		internal function getResultImportHandler(re:ResultEvent):void
		{
			if(re.result==1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Votre_import_s_est_bien_d_roul_'));
				dispatchEvent(new DestinataireEvent(DestinataireEvent.USER_LISTE_DESTINATRAIRE_EVENT));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M22', 'Une_erreur_s_est_produit_lors_de_l_impor'),ResourceManager.getInstance().getString('M22', 'Import_de_masse'));
			}
		}
	}
}