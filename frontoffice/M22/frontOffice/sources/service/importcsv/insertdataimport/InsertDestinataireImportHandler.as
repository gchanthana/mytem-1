package service.importcsv.insertdataimport
{
	import mx.rpc.events.ResultEvent;

	public class InsertDestinataireImportHandler
	{
	
		private var _model:InsertDestinataireImportModel;
		
		public function InsertDestinataireImportHandler(_model:InsertDestinataireImportModel):void
		{
			this._model = _model;
		}
		
		internal function getResultImportHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.getResultImportHandler(event);
			}
		}	
	}
}