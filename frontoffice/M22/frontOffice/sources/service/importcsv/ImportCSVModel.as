package service.importcsv
{
	import composants.util.ConsoviewAlert;
	import event.ImportEvent;
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	import vo.DestinataireImport;
	

	internal class ImportCSVModel  extends EventDispatcher
	{
		private var _importDestinataires :ArrayCollection;
		
		public function ImportCSVModel()
		{
			_importDestinataires = new ArrayCollection();	
		}
		public function get importDestinataires():ArrayCollection
		{
			return _importDestinataires;
		}
		
		private var _UUID: String;
		
					
		public function get UUID():String
		{
			return _UUID;
		}

		public function set UUID(value:String):void
		{
			_UUID = value;
		}

		internal function getInfosCSVResult(value:ArrayCollection):void
		{
			_importDestinataires.removeAll();// vider l'ArrayCollection
						
			if(value.length > 0)
			{
				for(var i:int=1;i<value.length;i++)
				{
					var isFind:Boolean=false;
					
					// verifier si le mail existe déja dans l'array collection
					
					for each (var obj :DestinataireImport in _importDestinataires)
					{
						if(value[i].col_3 ==obj.mail)
						{
							isFind=true;// le mail existe déjà 
							break;
						}
					}
					if(!isFind)
					{
						var unImportDestinataire :DestinataireImport = new DestinataireImport();
						unImportDestinataire.fill(value[i]);
						if(unImportDestinataire.mail !="" && unImportDestinataire.mail !=null)
						{
							_importDestinataires.addItem(unImportDestinataire);
						}
					} 
			    }
			}
			this.dispatchEvent(new ImportEvent(ImportEvent.INFOS_CSV_EVENT));		
		}
		
		internal function getUUIDResult(re:ResultEvent):void
		{
			if(re.result)
			{
				this._UUID= (re.result) as String;
				this.dispatchEvent(new ImportEvent(ImportEvent.UUID_EVENT));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M22', 'UUID_non_valide'),ResourceManager.getInstance().getString('M22', 'Erreur_UUID___import_fichier_CSV'));
			}
		}
	}
}