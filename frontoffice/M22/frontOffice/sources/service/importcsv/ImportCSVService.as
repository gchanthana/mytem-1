package service.importcsv
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class ImportCSVService 
	{
		
		public var _model:ImportCSVModel;
		public var handler:ImportCSVHandler;	
		
		public function ImportCSVService()
		{
			this._model  = new ImportCSVModel();
			this.handler = new ImportCSVHandler(model);
		}
		public function get model():ImportCSVModel
		{
			return this._model;
		}	
		public function importInfosCSV(uuid:String, fileName:String): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M22.service.import.importCSVService","importInfosCSV",handler.getInfosCVSResultHandler); 
				
			RemoteObjectUtil.callService(op,uuid,fileName);	
		}
		
		public function getUUID(): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M22.service.import.importCSVService","getUUID",handler.getUUIDResultHandler); 
			
			RemoteObjectUtil.callService(op);	
		}
		

	}
}