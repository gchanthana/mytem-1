package service.importcsv
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	import service.destinataires.ajoutermodifierdestinataire.AjouterModifierDestinataireModel;

	internal class ImportCSVHandler
	{
		private var _model:ImportCSVModel;
		
		public function ImportCSVHandler(_model:ImportCSVModel):void
		{
			this._model = _model;
		}
		
		internal function getInfosCVSResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.getInfosCSVResult(event.result as ArrayCollection);
			}
		}
		internal function getUUIDResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.getUUIDResult(event);
			}
		}	
		
		
		
	}
}