package univers.inventaire.equipements.emplacement
{
	import composants.util.GenericSearchTree;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.containers.Panel;
	import mx.controls.Button;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.core.ClassFactory;
	import mx.core.IFactory;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.emplacement.customitemrenderer.TreeEmplacementItemRenderer;
	
	public class ChoixEmplacement extends Panel{
	
		public var myMainBox : Box;
		public var hbConteneurBt : HBox;
		public var imgRefresh : Image;
		public var lblRafraichir : Label;
		public var btnAddItem : Button;
		public var btnDeleteItem : Button;
	
		// Icon des Sites
	 	[Embed(source='/assets/images/office-building.png',mimeType='image/png')]
		private var imgSite:Class;
		
		// Icon des Emplacements
	 	[Embed(source='/assets/images/Money_Box_In.png',mimeType='image/png')]
		private var imgEmplacement:Class;
		
		public var myEmp:GenericSearchTree;
		private var opData:AbstractOperation = new AbstractOperation();
		public var _treeData:XMLList;
		
		private var treeEmpItemRenderer : IFactory;
		//private var _sitePhysique:String;
		private var _isSiteSelectable : Boolean = true;
		
		
		[Event(name="imgRefreshClicked")]
		
		public static const IMGREFRECH_CLICK : String = "imgRefreshClicked";
		
		
		
		public function ChoixEmplacement()
		{
			super();
			
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		[Inspectable(defaultValue=true, type = Boolean)]
		[Bindable]
		public function get siteSelectable():Boolean{
			return _isSiteSelectable;
		}
		
		public function set siteSelectable(b : Boolean):void{
			_isSiteSelectable = b;
		}
		
		protected function initIHM(event:FlexEvent):void {
			treeEmpItemRenderer = new ClassFactory(TreeEmplacementItemRenderer);
			myEmp=new GenericSearchTree(null,"LABEL");			
			myMainBox.addChildAt(myEmp,0);
			//myEmp.myTree.showRoot=false;
			myEmp.addEventListener(FlexEvent.CREATION_COMPLETE,manageSiteSelcectabel);
			btnAddItem.addEventListener(MouseEvent.CLICK, onbtnAddItemClicked);
			btnDeleteItem.addEventListener(MouseEvent.CLICK, onbtnDeleteItemClicked);
			imgRefresh.addEventListener(MouseEvent.CLICK,imgRefreshClickHandler);
		}
		
		private function imgRefreshClickHandler(me : MouseEvent):void{
			dispatchEvent(new Event(IMGREFRECH_CLICK));
		}
		
		
		
		//Active ou désactive la séléction des site
		//param in FlexEvent
		private function manageSiteSelcectabel(fe : FlexEvent):void{
			if (!_isSiteSelectable){
				//myEmp.myTree.addEventListener(ListEvent.CHANGE,unselectSite);
			}
		}
		
		//Deselectione le noeud si c'est un site
		private function unselectSite(le : ListEvent):void{
			if (myEmp.myTree.selectedItem != null){
				if (Number(myEmp.myTree.selectedItem.@IDTYPE_EMPLACEMENT) == 0){
					//myEmp.myTree.selectedIndex = 0;					
				}
					
			}
			
		}
		
		
		//Initialisation du renderer de l'arbre des emplacements
		//Ainsi que ses icon et le ToolTip
		private function initTreeRenderer():void{
			myEmp.myTree.itemRenderer =  treeEmpItemRenderer;
			myEmp.myTree.iconFunction = myTreeIconFunction;
			myEmp.myTree.dataTipFunction = myTreeDataTipFunction;
		}
		
		//fonction qui assigne les icon de l'arbre de Emplacement
		private function myTreeIconFunction(item:Object):Class{
			if (Number(item.@IDTYPE_EMPLACEMENT) > 0){			
            	return	imgEmplacement; 
            }else{            	
            	return imgSite;
            }
		}
		
		private function myTreeDataTipFunction(item : Object):String{
			if (Number(item.@IDTYPE_EMPLACEMENT) > 0){            	
            	return  "Emplacement";
            }else{
            	return  "Site";
            }
		}
			 	
		protected function onbtnAddItemClicked(event:MouseEvent):void
		{
			try
			{
				var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				var sitePhysique:String = myEmp.getSelectedItem().@IDSITE_PHYSIQUE;
				var idEmplacementParent:String = myEmp.getSelectedItem().@VALUE;
				var idTypeParent:Number = parseInt(myEmp.getSelectedItem().@IDTYPE_EMPLACEMENT) + 1;
				if (idTypeParent > 5)
					idTypeParent = 5;
				var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.emplacement",
																					"addEmplacement",
																					addEmplacementResult);
				RemoteObjectUtil.callService(op, sitePhysique, idTypeParent.toString(), idEmplacementParent, idGroupeMaitre.toString());
				addNodeinTree(idEmplacementParent, sitePhysique, idTypeParent.toString());
			}
			catch (error:Error) {}
		}
		
		protected function onbtnDeleteItemClicked(event:MouseEvent):void
		{
			try
			{
				var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				var idEmplacement:String = myEmp.getSelectedItem().@VALUE;
				
				var dept:XMLList = _treeData.descendants().(@VALUE == idEmplacement);
				var parent : Object = dept.parent();
				
				
				myEmp.myTree.expandItem(parent,false);
				delete dept[0];
				
				if ((parent as XML).length() > 0)
					myEmp.myTree.expandItem(parent,true);
					
				 
				var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.emplacement",
																					"deleteEmplacement",
																					deleteEmplacementResult);
				RemoteObjectUtil.callService(op, idEmplacement, idGroupeMaitre.toString());
			}
			catch (error:Error) {}
		}
		
		public static const EMPLACEMENT_DELETED : String = "EmplacementDeleted";
		private function deleteEmplacementResult(event:ResultEvent):void
		{
			btnAddItem.enabled = false;
			btnDeleteItem.enabled = false;
			dispatchEvent(new Event(EMPLACEMENT_DELETED));
		}
		
		
		private function addNodeinTree(IDParent:String, idsite_physique:String, idType_emplacement:String):void
		{
			var newNode:XML = <node VALUE="-1" LABEL="untitled" ACTIF="1" />;			
			newNode.@IDSITE_PHYSIQUE = idsite_physique;
			newNode.@IDTYPE_EMPLACEMENT = idType_emplacement;
			
			var dept:XMLList;
			if (IDParent != "")
				dept = _treeData.descendants().(@VALUE == IDParent);
			else
				dept = _treeData.((@IDSITE_PHYSIQUE == idsite_physique && @IDTYPE_EMPLACEMENT=="0"));
			myEmp.myTree.expandItem(myEmp.myTree.selectedItem,false);
			dept[0].appendChild(newNode);
			myEmp.myTree.expandItem(myEmp.myTree.selectedItem,true);
			//myEmp.myTree.selectedItem = newNode;
			
		}
		
		private function addEmplacementResult(event:ResultEvent):void
		{
			var dept:XMLList = _treeData.descendants().(@VALUE == "-1");
			dept[0].@VALUE = event.result;
		}  
		
		public function getEmplacement(node:String): void {

			opData = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.inventaire.equipement.emplacement",
					"getEmplacementxml", processGetEmp);
			RemoteObjectUtil.callService(opData,node);
		}
		
		public function updateLibelle(obj:Object):void
		{
			myEmp.getSelectedItem().@LABEL = obj.LIBELLE_EMPLACEMENT;
		}
		
		public function updateButtons():void
		{
			var active:Boolean = false;
			if (myEmp.myTree.selectedIndex >= 0)
				active = true;
			btnAddItem.enabled = active;
			btnDeleteItem.enabled = active;
			if (myEmp.myTree.selectedIndex >= 0 && myEmp.getSelectedItem().@IDTYPE_EMPLACEMENT == "0")
				btnDeleteItem.enabled = false;
		}
		
		
		public static const EMPLACEMENT_FINDED : String = "EmpFinded";
		public function processGetEmp(e:ResultEvent):void {
			_treeData = XML(e.result).children();
			myEmp.setDp(e.result);
			dispatchEvent(new Event(EMPLACEMENT_FINDED));
			callLater(updateButtons);
			callLater(initTreeRenderer);
			
		}
		
	}
}