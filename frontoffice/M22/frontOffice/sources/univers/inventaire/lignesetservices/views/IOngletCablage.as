package univers.inventaire.lignesetservices.views
{
	import flash.display.DisplayObject;
	
	public interface IOngletCablage
	{
		function get displayObject():DisplayObject;
		
	}
}