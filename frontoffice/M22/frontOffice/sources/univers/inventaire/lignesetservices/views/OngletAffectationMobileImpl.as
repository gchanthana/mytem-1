package univers.inventaire.lignesetservices.views
{
	import mx.resources.ResourceManager;
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.FormItem;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.equipements.lignes.mail.SelectContact;
	import univers.inventaire.equipements.lignes.mail.SelectContactView;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Contact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
	import univers.inventaire.lignesetservices.applicatif.GestionAffectationMobile;
	import univers.inventaire.lignesetservices.vo.Ligne;
	import univers.inventaire.lignesetservices.vo.OngletAffectationVo;
	
	[Bindable]
	public class OngletAffectationMobileImpl extends BaseView
	{
		public var NOM_COLLABORATEUR_SI_OPERATEUR : TextInputLabeled;
		public var PRENOM_COLLABORATEUR_SI_OPERATEUR : TextInputLabeled;
		public var imgSelectOperateur : Image;
		public var OPERATEUR : TextInputLabeled;
		public var SOUS_COMPTES : ComboBox;
		public var COMPTE : TextInputLabeled;
		
		public var frmCompte :  FormItem;
		
		public var imgCancel : Image;
		
		public var oldNOM : String = "";
		public var oldPRENOM : String = "";
		
		public var txtFiltre : TextInputLabeled;
		
		public var libelle5 : FormItem;    
		public var libelle6 : FormItem;
			
		public var V5 : TextInputLabeled;
		public var V6 : TextInputLabeled;
		
		public var btUpdate : Button;
		
		protected var tmpcollaborateur : OngletAffectationVo = new  OngletAffectationVo();
		
		protected var donnees : OngletAffectationVo;
		
		protected var emailSelected : Boolean = false;
		
		//gere le click sur le bouton Mail
		//Ouvre une fentre Gestioncontact
		protected var _contactWindow : SelectContactView;
		
		//reference locale ves le contact 
		protected var _contact : Contact;
		
		//reference locale vers l'Agence
		protected var _societe : Societe;
		
		//reference locale vers le distributeur
		protected var _distrib : Societe;
		
		//reference locale vers l'opérateur
		protected var _operateur : OperateurVO;
		
		
		public function OngletAffectationMobileImpl()
		{	
			super();
		}
		
		
		
		
		
		
		override protected function commitProperties():void{
			super.commitProperties();		
			
			
			imgSelectOperateur.addEventListener(MouseEvent.CLICK,imgSelectOperateurClickHandler);
			imgSelectOperateur.visible = (gestionTechnique.ligne.MAJ_AUTO_COLLABORATEUR == 0); 
			imgCancel.visible = (gestionTechnique.ligne.MAJ_AUTO_COLLABORATEUR == 0);
			 
			imgCancel.addEventListener(MouseEvent.CLICK,imgCancelClickHandler);
			frmCompte.addEventListener(FlexEvent.SHOW,frmCompteShowHandler);			
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
			
			NOM_COLLABORATEUR_SI_OPERATEUR.addEventListener(Event.CHANGE,NOM_COLLABORATEUR_SI_OPERATEURChangeHandler);
			PRENOM_COLLABORATEUR_SI_OPERATEUR.addEventListener(Event.CHANGE,PRENOM_COLLABORATEUR_SI_OPERATEURChangeHandler);
			NOM_COLLABORATEUR_SI_OPERATEUR.editable = (gestionTechnique.ligne.MAJ_AUTO_COLLABORATEUR == 0); 
			PRENOM_COLLABORATEUR_SI_OPERATEUR.editable = (gestionTechnique.ligne.MAJ_AUTO_COLLABORATEUR == 0);
			
			COMPTE.editable = (gestionTechnique.ligne.MAJ_AUTO_COLLABORATEUR == 0);
			
			NOM_COLLABORATEUR_SI_OPERATEUR.editable = (gestionTechnique.ligne.MAJ_AUTO_COLLABORATEUR == 0);
			btUpdate.enabled = (gestionTechnique.ligne.MAJ_AUTO_COLLABORATEUR == 0);
			
		}
		
		//======================== HANDLERS ==========================================
		protected function imgCancelClickHandler(me : MouseEvent):void{
			 OPERATEUR.text = '';
			 COMPTE.text = '';
			 emailSelected = false;
		}
		
		
		protected function btUpdateClickHandler(me : MouseEvent):void{
			if(!emailSelected){
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M22', 'Etes_vous_sur_de_ne_pas_vouloir_envoyer_'),ResourceManager.getInstance().getString('M22', 'Confirmation'),confirmationHandler);	
			}else{
				enregistrer();
			}	
		}
		
		protected function confirmationHandler(ce : CloseEvent):void{
			if(ce.detail == Alert.OK){
				enregistrer();
			}
		}
		
		
		protected function  NOM_COLLABORATEUR_SI_OPERATEURValueCommitHandler(fe : FlexEvent):void{
			if (NOM_COLLABORATEUR_SI_OPERATEUR.initialized){
				if (oldNOM == ""){
					oldNOM = ObjectUtil.copy(gestionTechnique.ligne.NOM).toString();
				}
				tmpcollaborateur.COMPTE = COMPTE.text;
				tmpcollaborateur.COLLABORATEURID = gestionTechnique.ligne.COLLABORATEURID;
				tmpcollaborateur.NOM = (NOM_COLLABORATEUR_SI_OPERATEUR.text != null)?NOM_COLLABORATEUR_SI_OPERATEUR.text :"";	
			}
		}		
		
		protected function  PRENOM_COLLABORATEUR_SI_OPERATEURValueCommitHandler(fe : FlexEvent):void{
			if (PRENOM_COLLABORATEUR_SI_OPERATEUR.initialized){
				if (oldPRENOM == ""){ 
					oldPRENOM =  ObjectUtil.copy(gestionTechnique.ligne.PRENOM).toString();
				}
				tmpcollaborateur.COMPTE = COMPTE.text;
				tmpcollaborateur.COLLABORATEURID = gestionTechnique.ligne.COLLABORATEURID;
				tmpcollaborateur.PRENOM = (PRENOM_COLLABORATEUR_SI_OPERATEUR.text != null)?PRENOM_COLLABORATEUR_SI_OPERATEUR.text:"";	
			}
		}
		
		protected function NOM_COLLABORATEUR_SI_OPERATEURChangeHandler(ev : Event):void{
			tmpcollaborateur.COMPTE = COMPTE.text;
			tmpcollaborateur.COLLABORATEURID = gestionTechnique.ligne.COLLABORATEURID;
			tmpcollaborateur.NOM = NOM_COLLABORATEUR_SI_OPERATEUR.text;
			
		}
		
		protected function PRENOM_COLLABORATEUR_SI_OPERATEURChangeHandler(ev : Event):void{
			tmpcollaborateur.COMPTE = COMPTE.text;
			tmpcollaborateur.COLLABORATEURID = gestionTechnique.ligne.COLLABORATEURID;
			tmpcollaborateur.PRENOM = PRENOM_COLLABORATEUR_SI_OPERATEUR.text;
		}
		
				
		protected function frmCompteShowHandler(fe : FlexEvent):void{
			//
		}
		//======================== FIN HANDLERS ======================================
		
		
		
		
		//===== PRIVATE		
		//Enregistre les donnees
		override protected function enregistrer():void{
			var boolOk : Boolean = true;
			tmpcollaborateur.COMPTE = COMPTE.text;
			tmpcollaborateur.NOM = NOM_COLLABORATEUR_SI_OPERATEUR.text;
			tmpcollaborateur.PRENOM = PRENOM_COLLABORATEUR_SI_OPERATEUR.text;
			tmpcollaborateur.COLLABORATEURID = gestionTechnique.ligne.COLLABORATEURID;
			if(NOM_COLLABORATEUR_SI_OPERATEUR.text.length == 0){
				boolOk = false
				NOM_COLLABORATEUR_SI_OPERATEUR.setStyle("borderColor","red");				
			}
			
			
				
			if(emailSelected){
				GestionAffectationMobile(gestionTechnique.gestionAffectationStrategy).oldValue = new Ligne();
				GestionAffectationMobile(gestionTechnique.gestionAffectationStrategy).oldValue.NOM = oldNOM;
				GestionAffectationMobile(gestionTechnique.gestionAffectationStrategy).oldValue.PRENOM = oldPRENOM;
				GestionAffectationMobile(gestionTechnique.gestionAffectationStrategy).envoyerMail = true;
				GestionAffectationMobile(gestionTechnique.gestionAffectationStrategy).serviceClientOperateur = new Object();
				GestionAffectationMobile(gestionTechnique.gestionAffectationStrategy).serviceClientOperateur.EMAIL = _contact.email;
				GestionAffectationMobile(gestionTechnique.gestionAffectationStrategy).serviceClientOperateur.NOM = _contact.nom;
				GestionAffectationMobile(gestionTechnique.gestionAffectationStrategy).serviceClientOperateur.PRENOM = _contact.prenom;
				GestionAffectationMobile(gestionTechnique.gestionAffectationStrategy).serviceClientOperateur.OPERATEUR = (_contact.societeNom != "")?_contact.societeNom:_distrib.raisonSociale;
			}else{
				GestionAffectationMobile(gestionTechnique.gestionAffectationStrategy).envoyerMail = false;
			}
			
			if (boolOk){ 
				mapData(donnees = new OngletAffectationVo());
				gestionTechnique.gestionAffectationStrategy.updateInfosAffectation(donnees);
				NOM_COLLABORATEUR_SI_OPERATEUR.clearStyle("borderColor");
				//PRENOM_COLLABORATEUR_SI_OPERATEUR.clearStyle("borderColor");
				_hasChanged = false;
			}else{
				Alert.show("Les champs en rouge sont obligatoires","Erreur");
			}
		}
		
		
		protected function mapData(item : OngletAffectationVo):void{
			if (tmpcollaborateur != null){						
				item.NOM = tmpcollaborateur.NOM;
				item.PRENOM = tmpcollaborateur.PRENOM;
				item.COLLABORATEURID = tmpcollaborateur.COLLABORATEURID;			
				item.SI_OPERATEUR = true;				
				item.COMPTE = tmpcollaborateur.COMPTE;
				/* item.V5 = V5.text;
				item.V6 = V6.text;	 */
			}else{
				throw (new IllegalOperationError(ResourceManager.getInstance().getString('M22', 'Pas_de_controlleur_pour_l_application')))	
			}			
		}
		//==== FIN PRIVATE	
		
		
		
		
		
		protected function imgSelectOperateurClickHandler(me : MouseEvent):void{
			if (_contactWindow != null) _contactWindow = null;
			_contactWindow = new SelectContactView();			
			_contactWindow.addEventListener(SelectContact.CONTACT_SELECTED,constactSelectedHandler);
			_contactWindow.contact = _contact;
			_contactWindow.agence = _societe;
			_contactWindow.distributeur = _distrib;
			_contactWindow.operateur = _operateur;

			PopUpManager.addPopUp(_contactWindow,UIComponent(parentApplication),true);
			PopUpManager.centerPopUp(_contactWindow);
		}		
		
		protected function constactSelectedHandler(ev : Event):void{
			if (_contactWindow.contact.email != null && _contactWindow.contact.email.length > 0) emailSelected  = true;
			else  emailSelected  = false;
			
			OPERATEUR.text = _contactWindow.contact.email;		
			_contact = _contactWindow.contact;
			_distrib = _contactWindow.distributeur;
			GestionAffectationMobile(gestionTechnique.gestionAffectationStrategy).getListeSousComptes();
		}
	}
}