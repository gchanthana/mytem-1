package univers.inventaire.lignesetservices.views
{
	import composants.controls.TextInputLabeled;
	
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	
	import univers.inventaire.lignesetservices.vo.OngletT0Vo;
	
	[Bindable]
	public class OngletCablageT0Impl extends OngletCablageLAImpl implements IOngletCablage
	{
		
		
		public var NBRE_CANAUX : TextInputLabeled;
		
		
		public function OngletCablageT0Impl()
		{
			//TODO: implement function
			super();
		}
		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
		}
				
		override protected function doMapping():void{
			super.doMapping();
			if(gestionTechnique != null){
				var item : ItoXMLParams = gestionTechnique.gestionCablage.ongletVo;
				OngletT0Vo(item).NBRE_CANAUX = Number(NBRE_CANAUX.text);
			}
		}
		
		
		
		
	}
}