package univers.inventaire.lignesetservices.vo
{
	[Bindable]
	public class Action
	{
		public var libelle:String = "";
		public var identifiant:Number=0;
		public var dateAction:Date =null;
		public var commentaire: String = "";
		public var reference:String = "";
		[Inspectable(enumeration="true,false")] public var changerEtatInventaire:Boolean=false;
		public function Action()
		{
		}

	}
}