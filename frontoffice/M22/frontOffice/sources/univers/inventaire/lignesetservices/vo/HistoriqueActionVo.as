package univers.inventaire.lignesetservices.vo
{
	[Bindable]
	public class HistoriqueActionVo
	{	
		public var IDSOUS_TETE:Number=0;
			
		public var IDACTION:Number=0;
		public var LIBELLE_ACTION:String="-";
		
		public var IDACTION_PREC:Number=0;
					
		public var IDTYPE_RACCORDEMENT:Number=0;
		public var TYPE_RACCORDEMENT:String="-";
		
		public var DATE_EFFET:Date=null;			
		public var REFERENCE:String="-";
		public var COMMENTAIRES:String="-";
		
		public var USER : String="-";
		
		public var OPNOM:String="-";
		public var OPERATEURID:Number = 0;
		
		
		public function HistoriqueActionVo()
		{	
		}
		
	}
}