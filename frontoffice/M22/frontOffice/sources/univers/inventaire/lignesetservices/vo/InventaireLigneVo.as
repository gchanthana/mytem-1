package univers.inventaire.lignesetservices.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	[Bindable]
	public class InventaireLigneVo
	{
		public var LIBELLE_PRODUIT:String="";
		public var IDINVENTAIRE_PRODUIT:Number=0;
		public var OPNOM:String="";
		public var OPERATEURID:Number=0;
		public var EN_COURS:Number=0;
		public var DANS_INVENTAIRE:Number=0;
		public var IDINV_ACTIONS:Number=0;
		public var LIBELLE_ETAT:String="";
		public var ETAT_ACTUEL:Number=0;
		public var QTE:Number=0;
		public var IDINV_OP_ACTION:Number=0;
		public var IDINV_OPERATIONS:Number=0;
		public var BOOL_ACCES:Number=0;
		public var ENTRER:Boolean = false;
		public var SORTIR:Boolean = false;
		
		public function InventaireLigneVo()
		{			
		}
	}
}