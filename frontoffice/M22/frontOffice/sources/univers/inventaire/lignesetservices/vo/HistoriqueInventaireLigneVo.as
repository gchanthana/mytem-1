package univers.inventaire.lignesetservices.vo
{
	[Bindable]
	public class HistoriqueInventaireLigneVo
	{
		public var IDINV_OP_ACTION:Number=0;	
		public var LIBELLE_ETAT:String="";	
		public var DATE_ACTION:Date=null;	
		public var DATE_INSERT:Date=null;	
		public var COMMENTAIRE_ETAT:String="";	
		public var DANS_INVENTAIRE:Number=0;	
		public var DANS_OPERATION:Number=0;	
		public var DUREE_ETAT:Number=0;	
		public var IDINV_ETAT:Number=0;	
		public var LOGIN_EMAIL:String="";	
		public var UTILISATEUR:String="";	
		
		
		public function HistoriqueInventaireLigneVo()
		{	
		}
	}
}