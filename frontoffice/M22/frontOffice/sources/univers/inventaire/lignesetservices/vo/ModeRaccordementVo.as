package univers.inventaire.lignesetservices.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	[Bindable]
	public class ModeRaccordementVo implements ItoXMLParams
	{
		
		protected var _xmlParams : XML;
	
		public var IDTYPE_RACCORDEMENT:Number=0;
		public var COMMENTAIRE_RACCORDEMENT:String="";
		public var LIBELLE_RACCORDEMENT:String="";
		public var BOOL_DIRECT:Number=1;
			
		public function ModeRaccordementVo()
		{
		
		}
			
		
		
		public function toXMLParams(registerInLog : int=0):XML{
			_xmlParams = XmlParamUtil.createXmlParamObject();
			XmlParamUtil.addParam(_xmlParams,"IDTYPE_RACCORDEMENT","l'identifiant",IDTYPE_RACCORDEMENT);
			XmlParamUtil.addParam(_xmlParams,"COMMENTAIRE_RACCORDEMENT","le commentaire",COMMENTAIRE_RACCORDEMENT);
			XmlParamUtil.addParam(_xmlParams,"LIBELLE_RACCORDEMENT","le libelle",LIBELLE_RACCORDEMENT);
			XmlParamUtil.addParam(_xmlParams,"BOOL_DIRECT","sagit-il d'un raccordement direct",BOOL_DIRECT);
			return _xmlParams;
		}
		

	}
}