package univers.inventaire.lignesetservices.applicatif
{
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.lignesetservices.views.OngletCablageImpl;
	import univers.inventaire.lignesetservices.vo.Ligne;
	import univers.inventaire.lignesetservices.vo.OngletCabalageVo;
	import univers.inventaire.lignesetservices.vo.TrancheSDAVo;
	
	[Bindable]
	public class GestionCablage
	{
		public function GestionCablage()
		{
		}
		
		//La ligne sélectionnée
		private var _selectedLigne : Ligne;
		public function get selectedLigne():Ligne{
			return _selectedLigne;
		}
		public function set selectedLigne(lig : Ligne):void{
			_selectedLigne = lig;
		}
		
		//les données de l'onglet cablage
		private var _ongletVo : ItoXMLParams;
		public function get ongletVo():ItoXMLParams{
			return _ongletVo;
		}
		public function set ongletVo(vo : ItoXMLParams):void{
			_ongletVo = vo;
		}
		
		//la liste des tranches sda
		private var _listeTranches : ArrayCollection
		public function get listeTranches():ArrayCollection{
			if (_listeTranches == null) _listeTranches = new ArrayCollection();
			return _listeTranches;
		}
		public function set listeTranches(liste : ArrayCollection):void{
			_listeTranches = liste;
		}
		
		//La tranche sélectionnée
		private var _selectedTrancheSda : TrancheSDAVo;
		public function get selectedTrancheSda():TrancheSDAVo{
			return _selectedTrancheSda;
		}
		public function set selectedTrancheSda(tranche : TrancheSDAVo):void{
			_selectedTrancheSda = tranche;
		}
		
		
		//========================GESTION DES DONNEES DE L'ONGLET ==========================================================
		public function getInfosOnglet():void{			
			var op :AbstractOperation = 
						RemoteObjectUtil.
							getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
									"getInfosCablage",
									getInfosOngletResultHandler);
			RemoteObjectUtil.callService(op,selectedLigne.IDSOUS_TETE);
		}
		
		private function getInfosOngletResultHandler(re : ResultEvent):void{
			if (re.result){
				if ((re.result as ArrayCollection).length > 0){
					doMapping(re.result[0]);		
				}else{
					doMapping(new OngletCabalageVo());
				}
				getListeTranches();
			}
		}
		
		
		
		private function doMapping(item : Object):void{
			switch(selectedLigne.TYPE_FICHELIGNE){
			 	case OngletCablageImpl.LA : case OngletCablageImpl.MOBILE : 
			 	case OngletCablageImpl.GSM : case OngletCablageImpl.T0 : 
			 	case OngletCablageImpl.T2: {
			 		
					ongletVo = new OngletCabalageVo();
					
					OngletCabalageVo(ongletVo).ADRESSE1_INSTALL = item.ADRESSE1_INSTALL;
					OngletCabalageVo(ongletVo).ADRESSE2_INSTALL = item.ADRESSE2_INSTALL;
					OngletCabalageVo(ongletVo).AMORCE = item.AMORCE;
					OngletCabalageVo(ongletVo).BAT = item.BAT;
					OngletCabalageVo(ongletVo).CARTE_PABX = item.CARTE_PABX;
					OngletCabalageVo(ongletVo).COMMENTAIRE_CABLAGE = item.COMMENTAIRE_CABLAGE;
					OngletCabalageVo(ongletVo).DISCRIMINATION = item.DISCRIMINATION;
					OngletCabalageVo(ongletVo).ETAGE = item.ETAGE;
					OngletCabalageVo(ongletVo).IDSOUS_TETE = selectedLigne.IDSOUS_TETE;
					OngletCabalageVo(ongletVo).NBRE_CANAUX = item.NBRE_CANAUX;
					OngletCabalageVo(ongletVo).NOEUD = item.NOEUD;
					OngletCabalageVo(ongletVo).NOM_SITE_INSTALL = item.NOM_SITE_INSTALL;
					OngletCabalageVo(ongletVo).NOMBRE_POSTES = item.NOMBRE_POSTES;
					OngletCabalageVo(ongletVo).PAIRE1 = item.PAIRE1;
					OngletCabalageVo(ongletVo).PAIRE2 = item.PAIRE2;
					OngletCabalageVo(ongletVo).PUBLICATION_ANNUAIRE = item.PUBLICATION_ANNUAIRE; 
					OngletCabalageVo(ongletVo).REGLETTE1 = item.REGLETTE1;
					OngletCabalageVo(ongletVo).REGLETTE2 = item.REGLETTE2;
					OngletCabalageVo(ongletVo).SALLE = item.SALLE;
					OngletCabalageVo(ongletVo).SECRET_IDENTIFIANT = item.SECRET_IDENTIFIANT;
					OngletCabalageVo(ongletVo).V7 = item.V7;
					OngletCabalageVo(ongletVo).V8 = item.V8;
					OngletCabalageVo(ongletVo).VILLE_INSTALL = item.VILLE_INSTALL;
					OngletCabalageVo(ongletVo).ZIPCODE_INSTALL = item.ZIPCODE_INSTALL;
					
					if (item.hasOwnProperty("CANAUX_ENTREE_CAN_DEB")){
						OngletCabalageVo(ongletVo).CANAUX_ENTREE_CAN_DEB = item.CANAUX_ENTREE_CAN_DEB;
						OngletCabalageVo(ongletVo).CANAUX_ENTREE_CAN_FIN = item.CANAUX_ENTREE_CAN_FIN;
						OngletCabalageVo(ongletVo).CANAUX_MIXTE_CAN_DEB = item.CANAUX_MIXTE_CAN_DEB;
						OngletCabalageVo(ongletVo).CANAUX_MIXTE_CAN_FIN = item.CANAUX_MIXTE_CAN_FIN;
						OngletCabalageVo(ongletVo).CANAUX_SORTIE_CAN_DEB = item.CANAUX_SORTIE_CAN_DEB;
						OngletCabalageVo(ongletVo).CANAUX_SORTIE_CAN_FIN = item.CANAUX_SORTIE_CAN_FIN;	
					} 		
					break;
					
					
				}				
				case  OngletCablageImpl.LS : {
					ongletVo = new OngletCabalageVo();
					OngletCabalageVo(ongletVo).IDLL = item.IDLL;
					OngletCabalageVo(ongletVo).IDSOUS_TETE = selectedLigne.IDSOUS_TETE;
					OngletCabalageVo(ongletVo).LS_DEBIT2 = item.LS_DEBIT2;
					OngletCabalageVo(ongletVo).LS_DISTANCE2 = item.LS_DISTANCE2;
					OngletCabalageVo(ongletVo).LS_LEGA_ADRESSE1 = item.LS_LEGA_ADRESSE1;
					OngletCabalageVo(ongletVo).LS_LEGA_ADRESSE2 = item.LS_LEGA_ADRESSE2;
					OngletCabalageVo(ongletVo).LS_LEGA_COMMUNE = item.LS_LEGA_COMMUNE;
					OngletCabalageVo(ongletVo).LS_LEGA_ELEM_ACTIF = item.LS_LEGA_ELEM_ACTIF;
					OngletCabalageVo(ongletVo).LS_LEGA_ELEM_ACTIF_AMORCE_E = item.LS_LEGA_ELEM_ACTIF_AMORCE_E;
					OngletCabalageVo(ongletVo).LS_LEGA_ELEM_ACTIF_AMORCE_R = item.LS_LEGA_ELEM_ACTIF_AMORCE_R;
					OngletCabalageVo(ongletVo).LS_LEGA_NOMSITE = item.LS_LEGA_NOMSITE;
					OngletCabalageVo(ongletVo).LS_LEGA_TETE = item.LS_LEGA_TETE;
					OngletCabalageVo(ongletVo).LS_LEGA_TETE_AMORCE_E = item.LS_LEGA_TETE_AMORCE_E;
					OngletCabalageVo(ongletVo).LS_LEGA_TETE_AMORCE_R= item.LS_LEGA_TETE_AMORCE_R;
					OngletCabalageVo(ongletVo).LS_LEGA_ZIPCODE = item.LS_LEGA_ZIPCODE;
					OngletCabalageVo(ongletVo).LS_LEGB_ADRESSE1 = item.LS_LEGB_ADRESSE1;
					OngletCabalageVo(ongletVo).LS_LEGB_ADRESSE2 = item.LS_LEGB_ADRESSE2;
					OngletCabalageVo(ongletVo).LS_LEGB_COMMUNE = item.LS_LEGB_COMMUNE;
					OngletCabalageVo(ongletVo).LS_LEGB_ELEM_ACTIF = item.LS_LEGB_ELEM_ACTIF;
					OngletCabalageVo(ongletVo).LS_LEGB_ELEM_ACTIF_AMORCE_E = item.LS_LEGB_ELEM_ACTIF_AMORCE_E;
					OngletCabalageVo(ongletVo).LS_LEGB_ELEM_ACTIF_AMORCE_R = item.LS_LEGB_ELEM_ACTIF_AMORCE_R;
					OngletCabalageVo(ongletVo).LS_LEGB_NOMSITE = item.LS_LEGB_NOMSITE;
					OngletCabalageVo(ongletVo).LS_LEGB_TETE = item.LS_LEGB_TETE;
					OngletCabalageVo(ongletVo).LS_LEGB_TETE_AMORCE_E = item.LS_LEGB_TETE_AMORCE_E;
					OngletCabalageVo(ongletVo).LS_LEGB_TETE_AMORCE_R = item.LS_LEGB_TETE_AMORCE_R;
					OngletCabalageVo(ongletVo).LS_LEGB_ZIPCODE = item.LS_LEGB_ZIPCODE;
					OngletCabalageVo(ongletVo).LS_NATURE = item.LS_NATURE;
					OngletCabalageVo(ongletVo).COMMENTAIRE_CABLAGE = item.COMMENTAIRE_CABLAGE;
					OngletCabalageVo(ongletVo).V7 = item.V7;
					OngletCabalageVo(ongletVo).V8 = item.V8;
					break;
				};
				default : ongletVo = null;
			 }
		}
		
		public function majInfosOnglet():void{
			var op :AbstractOperation = 
						RemoteObjectUtil.
							getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
									"majInfoCablage",
									majInfosOngletResultHandler);
			RemoteObjectUtil.callService(op,ongletVo.toXMLParams());
		}
		private function majInfosOngletResultHandler(re : ResultEvent):void{
			if(re.result > 0){
				ConsoviewAlert.afficherOKImage("Champs mis à jour");
			}else{
				Alert.show("Erreur : "+re.result.toString(),"Erreur");
			}
		}
		//====================FIN GESTION DES DONNEES DE L'ONGLET ==========================================================
				
		//========= GESTION DES SDAs =======================================================================================
		
			
		public function getListeTranches():void{
			listeTranches = null;
			var op :AbstractOperation = 
						RemoteObjectUtil.
							getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
									"getListeTranches",
									getListeTranchesResultHandler);
			RemoteObjectUtil.callService(op,selectedLigne.toXMLParams());
		}
		private function getListeTranchesResultHandler(re : ResultEvent):void{
			if(re.result){
				var col : ArrayCollection = re.result as ArrayCollection;				
				var len : Number = col.length;
				for (var i:Number=0; i<len; i++){
					var tranche : TrancheSDAVo = new TrancheSDAVo();
	
					tranche.IDSOUS_TETE = selectedLigne.IDSOUS_TETE;
					tranche.IDTRANCHE_SDA = col[i].IDTRANCHE_SDA;
					tranche.NBRE_SDA = (Number(col[i].NB_TRANCHE) + 1).toString();
					tranche.NUM_DEPART = col[i].TRANCHE_DEB;
					tranche.NUM_FIN = col[i].TRANCHE_FIN;
					tranche.TRANCHE_SDA = col[i].LIBELLE_TRANCHE;
					tranche.TYPE_TRANCHE = col[i].TYPE_TRANCHE;
					listeTranches.addItem(tranche);
				} 
			}
		}
		public function majTranche():void{
			var op :AbstractOperation = 
						RemoteObjectUtil.
							getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
									"majTranche",
									majTrancheResultHandler);
			RemoteObjectUtil.callService(op,selectedTrancheSda.toXMLParams());
		}
		private function majTrancheResultHandler(re : ResultEvent):void{
			if(re.result > 0){
				selectedTrancheSda.NBRE_SDA = calculNbTranche(selectedTrancheSda.NUM_DEPART,selectedTrancheSda.NUM_FIN).toString();
				listeTranches.itemUpdated(selectedTrancheSda);
			}else{
				Alert.show("Erreur : "+re.result.toString(),"Erreur");
			}
		}
		public  function addTranche():void{
			var op :AbstractOperation = 
						RemoteObjectUtil.
							getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
									"addTranche",
									addTrancheResulthandler);
			RemoteObjectUtil.callService(op,selectedTrancheSda.toXMLParams());
		}
		private function addTrancheResulthandler(re : ResultEvent):void{
			if(re.result > 0){
				selectedTrancheSda.IDSOUS_TETE = selectedLigne.IDSOUS_TETE;
				selectedTrancheSda.IDTRANCHE_SDA = Number(re.result);				
				selectedTrancheSda.NBRE_SDA = calculNbTranche(selectedTrancheSda.NUM_DEPART,selectedTrancheSda.NUM_FIN).toString();
				listeTranches.addItem(selectedTrancheSda);
			}else{
				Alert.show("Erreur : "+re.result.toString(),"Erreur");
			}
		}		
		public function delTranche():void{
			var op :AbstractOperation = 
						RemoteObjectUtil.
							getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
									"delTranche",
									delTrancheResultHandler);
			RemoteObjectUtil.callService(op,selectedTrancheSda.toXMLParams());
		}
		
		private function delTrancheResultHandler(re : ResultEvent):void{
			if(re.result > 0){
				listeTranches.removeItemAt(listeTranches.getItemIndex(selectedTrancheSda));
				listeTranches.refresh();
			}else{
				Alert.show("Erreur : "+re.result.toString(),"Erreur");
			}
		}
		
		private function calculNbTranche(numDebut : String, numFin : String):Number{
			var num1 : Number = parseInt(numDebut); 
			var num2 : Number = parseInt(numFin);		
			return (num2 - num1)+1;
		}
		//=========FIN GESTION DES SDAs ====================================================================================
	}
}