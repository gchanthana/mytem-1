package univers.inventaire.lignesetservices.applicatif
{
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	
	import flash.events.EventDispatcher;
	import flash.utils.describeType;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.lignesetservices.util.viewsHelpers;
	import univers.inventaire.lignesetservices.vo.Ligne;
	import univers.inventaire.lignesetservices.vo.OngletGeneralVo;
	
	[Bindable]
	public class GestionOngletGenerale  extends EventDispatcher
	{
		public static const LISTETETESLIGNE_COMPLETE : String = "listeTeteligne_Complete";
		
		public function GestionOngletGenerale(ligne : Ligne)
		{
			_ligne = ligne;	
		}
		
		private var _listeLignesGroupement : ArrayCollection;
		public function get listeLignesGroupement():ArrayCollection{
			return _listeLignesGroupement;
		}
		public function set listeLignesGroupement(data : ArrayCollection):void{
			_listeLignesGroupement = data;
		}
		
		private var _helpers : viewsHelpers;
		public function get helpers():viewsHelpers{
			return _helpers;
		}
		public function set helpers(hp : viewsHelpers):void{
			_helpers = hp;
		}
		
		private var _ligne : Ligne;
		
		//Liste des des lignes du meme compte de rattachement
		private var _listeLigneCr : ArrayCollection;
		public function get listeLigneCr():ArrayCollection{
			return _listeLigneCr;
		}
		public function set listeLigneCr(data : ArrayCollection):void{
			_listeLigneCr = data;
		}
		
		//Les données de l'onglet générale from IHM
		private var _newDataGenerale : OngletGeneralVo;
		public function get newDataGenerale():OngletGeneralVo{
			return _newDataGenerale;
		}
		public function set newDataGenerale(data : OngletGeneralVo):void{
			_newDataGenerale = data;
		}
		
		//Les données de l'onglet générale from DB		
		private var _dataGenerale : OngletGeneralVo;
		public function get dataGenerale():OngletGeneralVo{
			return _dataGenerale;
		}
		public function set dataGenerale(data : OngletGeneralVo):void{
			_dataGenerale = data;
		}
		
		public function getInfosLigne():void{
			getlisteLigneCr(_ligne.IDSOUS_TETE);
			getlisteLignesGroupement(_ligne.IDTETE_LIGNE);
		}
				
		//Liste des tete de lignes
		private function getlisteLigneCr(idSousTete : Number):void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getTeteLigne",
																				getlisteLigneCrResultHandler);
			RemoteObjectUtil.callService(op,idSousTete);			
		}
				
		public function getlisteLigneCrResultHandler(re : ResultEvent):void{			
			if(re.result){
				listeLigneCr= re.result as ArrayCollection;
				listeLigneCr.refresh();
			}else{
				listeLigneCr = null;
			}		
			dispatchEvent(new Event(LISTETETESLIGNE_COMPLETE));
			getDataOngletGenerale();
		}
		
		protected function getDataOngletGenerale():void{
			var op :AbstractOperation = 
						RemoteObjectUtil.
							getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
									"getInfosLigne",
									getInfosLigneResultHandler);
									
			RemoteObjectUtil.callService(op,_ligne.IDSOUS_TETE);
		}
		
		private function getInfosLigneResultHandler(re :ResultEvent):void{
			if(re.result && (re.result as ArrayCollection).length > 0){
				var data : Object = re.result[0];
				dataGenerale = new OngletGeneralVo();
				doMapping(data,dataGenerale);
			}else{
				dataGenerale = null;
			}
		}
		
		
		//Liste des lignes du même groupement de ligne
		private function getlisteLignesGroupement(idSousTete : Number):void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getLignesGroupement",
																				getlisteLignesGroupementResultHandler);
			RemoteObjectUtil.callService(op,idSousTete);			
		}
				
		public function getlisteLignesGroupementResultHandler(re : ResultEvent):void{			
			 if(re.result){
				listeLignesGroupement = re.result as ArrayCollection;
			}else{
				listeLignesGroupement = null;
			}
			dispatchEvent(new Event(LISTETETESLIGNE_COMPLETE));
		}
		
		
		public function updateInfosLigne(data : OngletGeneralVo):void{
			newDataGenerale = data;
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"updateInfosLigne",
																				updateInfosLigneResultHandler);
			RemoteObjectUtil.callService(op,data.toXMLParams(0));
		}
		
		private function updateInfosLigneResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				ConsoviewAlert.afficherOKImage("Champs mis à jour");
				//doMapping(_dataGenerale,_dataGenerale);
				
				_ligne.LIBELLE_TYPE_LIGNE = _newDataGenerale.LIBELLE_TYPE_LIGNE;
				_ligne.IDTYPE_LIGNE = _newDataGenerale.IDTYPE_LIGNE;
				_ligne.TETE_LIGNE = _newDataGenerale.TETE_LIGNE;
				_ligne.IDTETE_LIGNE = _newDataGenerale.IDTETE_LIGNE;
				_ligne.IDTYPE_RACCORDEMENT = _newDataGenerale.IDTYPE_RACCORDEMENT;
				_ligne.TYPE_RACCORDEMENT = _newDataGenerale.LIBELLE_RACCORDEMENT;
				_ligne.USAGE = _newDataGenerale.USAGE;
				
				if (!ConsoviewUtil.isLabelInArray(_newDataGenerale.USAGE,"USAGE",_helpers.listeUsages.source)){
					var item : Object = new Object();
					item.USAGE = _newDataGenerale.USAGE;					
					_helpers.listeUsages.addItem(item);
					_helpers.listeUsages.refresh();	
				}
				//_helpers.getListeUsages();
			}else{
				Alert.show("Une erreur s'est produite lors de la mise à jour","Erreur Remoting");
			}
		}
		
		
		//--- locale vars
		
		private var sousTete : Ligne;
		private var teteLigne : Ligne;
		private var _listeLignes : ArrayCollection;
		public function set listeLignes(liste : ArrayCollection):void{
			_listeLignes = liste;
		}
		
		public function setTeteLigne(sous_tete : Ligne,tete : Ligne):void
		{
			//liste des lignes à mettre à jour
			var listeToUpdate:Array = [];			
			
			sousTete = sous_tete;
			teteLigne = tete;
			
			//si tete.IDSOUS_TETE > 0 alors on veut indiquer la tete pour la ligne
			//sinon on veut la supprimer
			if(tete.IDSOUS_TETE > 0)
			{
				listeToUpdate = [sousTete];
				 
				
				//On tague la futur tête si ce n'est pas encore le cas
				if(tete.ISTETE < 1)
				{
					listeToUpdate.push(teteLigne);	
					
				} 
				
				setTeteLignePourXLignes(listeToUpdate,teteLigne);
				
			} 
			else // on veut supprimer la tête
			{
				if(sousTete.IDSOUS_TETE == sousTete.IDTETE_LIGNE) //si la ligne est une tête, alors on supprime la tête des autres lignes du groupement
				{
					if(_listeLignesGroupement != null)					
					{
						ConsoviewAlert.afficherAlertConfirmation("Êtes vous sûr de vouloir effectuer cette action ? \n Cette action supprimera la tête de chaque ligne du groupement.","Attention !"
						,function deleteHandler(event:CloseEvent):void
						{
							if(event.detail == Alert.OK)
							{
									_listeLignesGroupement.source.push(sousTete)
									listeToUpdate = _listeLignesGroupement.source;
									setTeteLignePourXLignes(listeToUpdate,teteLigne);
									
							}
							else
							{
								return;
							}
							
						})
						
					}			
				}
				else
				{
					listeToUpdate = [sousTete]; 
					setTeteLignePourXLignes(listeToUpdate,teteLigne);
				}
			}	
			
			
		}
		
		
		protected function deleteTeteHandler(event:CloseEvent):void
		{
			
		}
		
		public function setTeteLigneResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				var index : Number = ConsoviewUtil.getIndexByIdInArray(_listeLignes.source,"IDSOUS_TETE",sousTete.IDSOUS_TETE); 
				if (index > -1)
				{ 
					var ligneToUpdate : Ligne = Ligne(_listeLignes.source[index]);
					ligneToUpdate.IDTETE_LIGNE = teteLigne.IDSOUS_TETE;
					ligneToUpdate.TETE_LIGNE = teteLigne.SOUS_TETE;
					_listeLignes.itemUpdated(ligneToUpdate);
				}
			}
			getInfosLigne()
		}
		//*----------
		private var sousTeteArray : Array;
		
		
		public function setTeteLignePourXLignes(liste_sous_tetes : Array,tete : Ligne):void{
			
			sousTeteArray = liste_sous_tetes;
			teteLigne = tete;
			 
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"setTeteLignePourXLignes",
																				setTeteLignePourXLignesResultHandler);
																				
			RemoteObjectUtil.callService(op,ConsoviewUtil.extractIDs("IDSOUS_TETE",sousTeteArray), teteLigne.IDSOUS_TETE);
		}
		
		public function setTeteLignePourXLignesResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				
				for (var i:Number = 0;i < sousTeteArray.length; i++){
					var index : Number = ConsoviewUtil.getIndexByIdInArray(_listeLignes.source,"IDSOUS_TETE",sousTeteArray[i].IDSOUS_TETE);
					if (index > -1){
						var ligneToUpdate : Ligne = Ligne(_listeLignes.source[index]);
						ligneToUpdate.IDTETE_LIGNE = teteLigne.IDSOUS_TETE;
						ligneToUpdate.TETE_LIGNE = teteLigne.SOUS_TETE;
						_listeLignes.itemUpdated(ligneToUpdate);
					}	
				}
			}
			getInfosLigne()
		}
		
		//------------
		
		private function doMapping(src : Object, dest : OngletGeneralVo):void{
			var classInfo : XML = describeType(dest);
			for each (var v:XML in classInfo..accessor) {
            	if (src.hasOwnProperty(v.@name) && src[v.@name] != null){
               		dest[v.@name] = src[v.@name];
               }else if (!src.hasOwnProperty(v.@name)){
               		//trace("-----> echec mapping ["+v.@name+"]" );
               }else if (src.hasOwnProperty(v.@name) && src[v.@name] == null){
               		//trace("-----> ["+v.@name+"] valeur null" );               		
               }else{
               		//trace("-----> erreur");
               } 
   			}      
             /*   dest.COLLABORATEURID = (src.COLLABORATEURID != null)?src.COLLABORATEURID:0;
               dest.COMPTE = (src.COMPTE != null)?src.COMPTE:"-";
               dest.IDOPERATEUR_ACCES = (src.IDOPERATEUR_ACCES!= null)?src.IDOPERATEUR_ACCES:0; 
               dest.IDOPERATEUR_ACCES_FACTURANT = (src.IDOPERATEUR_ACCES_FACTURANT!= null)?src.IDOPERATEUR_ACCES_FACTURANT:0;
               dest.IDSOUS_TETE = (src.IDSOUS_TETE!= null)?src.IDSOUS_TETE:0;
               dest.IDTETE_LIGNE = (src.IDTETE_LIGNE!= null)?src.IDTETE_LIGNE:0; 
               dest.IDTYPE_LIGNE = (src.IDTYPE_LIGNE!= null)?src.IDTYPE_LIGNE:0; 
               dest.IDTYPE_RACCORDEMENT = (src.IDTYPE_RACCORDEMENT != null)?src.IDTYPE_RACCORDEMENT:0;
               dest.LIBELLE_TYPE_LIGNE = (src.LIBELLE_TYPE_LIGNE!= null)?src.LIBELLE_TYPE_LIGNE:"-"; 
               dest.MAJ_AUTO_COLLABORATEUR = (src.MAJ_AUTO_COLLABORATEUR!= null)?src.MAJ_AUTO_COLLABORATEUR:1;
               dest.NOM = (src.NOM != null)?src.NOM:"-";
               dest.OPERATEUR_ACCES = (src.OPERATEUR_ACCES_CIBLE!= null)?src.OPERATEUR_ACCES_CIBLE:"-";
               dest.OPERATEUR_ACCES_FACTURANT = (src.OPERATEUR_ACCES!= null)?src.OPERATEUR_ACCES:"-";
               dest.PRENOM = (src.PRENOM!= null)?src.PRENOM:"-";
               dest.SOUS_TETE = (src.SOUS_TETE!= null)?src.SOUS_TETE:"-";
               dest.TETE_LIGNE = (src.TETE_LIGNE!= null)?src.TETE_LIGNE:"-";
               dest.TYPE_FICHELIGNE = (src.TYPE_FICHELIGNE!= null)?src.TYPE_FICHELIGNE:"-";
               dest.TYPE_RACCORDEMENT = (src.TYPE_RACCORDEMENT!= null)?src.TYPE_RACCORDEMENT:"-";
               dest.USAGE = (src.USAGE!= null)?src.USAGE:"-"; */
                       				 		
		}

	}
}