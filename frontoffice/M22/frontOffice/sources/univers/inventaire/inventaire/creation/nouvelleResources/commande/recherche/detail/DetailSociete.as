package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail
{
	import composants.util.ConsoviewUtil;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.PanelRechercheSociete;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.PaysVO;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
	
	/**
	 * Classe permettant de gerer l'écran 'Déatil d'une socété'
	 * */
	public class DetailSociete extends DetailSocieteIHM
	{		
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la validation du formulaire
		 * */
		public static const VALIDER : String = "societeCreated";		
		
		
		//Reference vers la methode distante qui charge la liste des opérateurs
		private var opOperateur : AbstractOperation;	
		
		//Reference vers la methode distante qui charge la liste des pays
		private var opPays : AbstractOperation;		
		
		//Reference vers la societe créée
		private var societe : Societe;
		
		//Reference vers l'objet gérant les sociétés parentes		
		private var societes : Societe;
		
		//Reference vers l'identifiant du groupe sur lequel on est logué
		private var idGroupe : Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
		
		//Reference vers la liste des opérateurs 
		[Bindable]
		private var operateurs : ArrayCollection;
		
		//Reference vers la liste des opérateurs à ajouter
		[Bindable]
		private var operateursAjout : ArrayCollection;
		
		//Reference vers la liste des pays
		private var pays : ArrayCollection ;// = [{nom : "France"},{nom : "Espagne"},{nom : "Royaume Uni"},{nom : "Allemagne"},{nom : "Belgique"}];
		
		//l'identifiant de la france dans la base consotel
		private const FRANCE_ID : int = 1577;
		
		//Reference vers l'ecran de recherche d'une societe
		private var rechercheSociete : PanelRechercheSociete;
		
		//Reference locale vers l'id de la societe créée
		private var _newSocieteID : int;
		
		/**
		 * Retourne l'identifiant de la société créée
		 * @return _newSocieteID 
		 * */
		public function get newSocieteID():int{
			return _newSocieteID;
		}
		
		/**
		 * Constructeur
		 * @param mySociete la societe que l'on veut afficher
		 * */
		public function DetailSociete(mySociete : Societe =  null)
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			if (mySociete != null) {
				societe = mySociete;													
			}else{
				societe = new Societe();
			}			
		}
		
		
		/**
		 * Ne fait rien
		 * */
		public function clean():void{
			if (rechercheSociete != null) rechercheSociete.clean();
			rechercheSociete = null;
			societe.clean();
			PopUpManager.removePopUp(this); 
		}
		
		//Initilaistation de l'IHM
		private function initIHM(fe : FlexEvent):void{
			addEventListener(CloseEvent.CLOSE,fermerFenetre);			
			cmbPays.dataProvider = pays;
			cmbPays.labelField = "nom";			
			btSortir.addEventListener(MouseEvent.CLICK,sortir);	
			btValider.addEventListener(MouseEvent.CLICK,sauverSociete);		
			btSupOpe.addEventListener(MouseEvent.CLICK,supprimerOpe);
			imgSearchSociete.addEventListener(MouseEvent.CLICK,afficherRechercheSociete);	
			getListeOperateur();
			getListeSocietesParentes();
			getListePays();
			initSociete(isNaN(societe.societeID));
		}
		
		
		//Affiche une fenêtre de recherche de societe
		private function afficherRechercheSociete(me : MouseEvent):void{
			rechercheSociete = new PanelRechercheSociete(false);						
			rechercheSociete.addEventListener(PanelRechercheSociete.VALIDER,selectionnerSociete);		
			ConsoviewUtil.scale(DisplayObject(rechercheSociete),ConsoViewModuleObject.W_ratio,ConsoViewModuleObject.H_ratio);
			PopUpManager.addPopUp(rechercheSociete,this,true);	
			PopUpManager.centerPopUp(rechercheSociete);		
		}
		
		//Sélectionne la société que l'on vient de rechercher dans la combo 'Société Parente'
		private function selectionnerSociete(ev : Event):void{
			cmbParent.selectedIndex = ConsoviewUtil.getIndexById(cmbParent.dataProvider as ArrayCollection,"societeID",rechercheSociete.societeSelectionneeID);			
		}
		
		//Charge la liste des sociétés parentes (toutes les sociétés pour un client)
		private function getListeSocietesParentes():void{
			societes = new Societe();
			societes.addEventListener(Societe.LISTE_COMPLETE,afficherSocieteParente);	
			societes.prepareList(idGroupe);
		}
		
		//Initialisation du composant suivant le parametre passé
		//si le param est à 'true' initialise le composant pour une creation, si non pour une mise à jour
		//param in isNew un Boléen
		private function initSociete(isNew : Boolean):void{
			if (isNew){
				initNewSociete();
				trace("initNewSociete");
			}else{
				initSocieteForUpdate();
			}
		}
		
		//Initialise le composant pour une mise à jour de société
		private function initSocieteForUpdate():void{
			societe.addEventListener(Societe.UPDATE_COMPLETE,updateListOperateurs);
			societe.addEventListener(Societe.LISTE_OPERATEUR_SAVED,societeCree);
			afficherSociete();
			btValider.label = "Mettre à jour";
			btAddOpe.visible = true;
			btAddOpe.addEventListener(MouseEvent.CLICK,ajouterOperateur);
		}
		
		//Initialise le composant pour une création de société
		private function initNewSociete():void{						
			societe.addEventListener(Societe.SAVE_COMPLETE,savegarderOperateurs);			
			societe.addEventListener(Societe.LISTE_OPERATEUR_SAVED,societeCree);
			btValider.label = "Valider";
		}
		
		
		//Fermer la fenêtre suite à un clique sur la croix
		private function fermerFenetre(ce : CloseEvent):void{
			clean();
			PopUpManager.removePopUp(this);
		}
		
		//Fermer la fenêtre suite à un clique sur le bouton sortir
		private function sortir(me : MouseEvent):void{
			clean();
			PopUpManager.removePopUp(this);
		}
		
		
		//Affiche la societé dans le formulaire		
		private function afficherSociete():void{			
			if(societe.adresse1 != null)txtAdresse1.text = societe.adresse1;
			if(societe.adresse2 != null)txtAdresse2.text = societe.adresse2;
			if(societe.commune != null)txtCommune.text = societe.commune;
			if(societe.raisonSociale != null)txtNom.text = societe.raisonSociale;
			if(societe.siretSiren != null)txtSiret.text = societe.siretSiren;
			if(societe.codePsotal!= null)txtZipcode.text = societe.codePsotal;
			if(societe.telephone != null)txtTelephone.text = societe.telephone;
			if(societe.fax != null)txtFax.text = societe.fax;			
			
			cmbPays.selectedItem = societe.pays;
			
			cmbParent.selectedIndex = ConsoviewUtil.getIndexById(cmbParent.dataProvider as ArrayCollection,
																 "societeID",
																 societe.societeParenteID);
			liOperateur.labelField = "NOM"; 													 
			liOperateur.dataProvider = societe.listeOperateurs;
		}
		
		
		//Affcihe la liste des opérateurs pour un ajout
		private function ajouterOperateur(me : MouseEvent):void{
			currentState = "StateAjoutOpe";
			btAddOpe.removeEventListener(MouseEvent.CLICK,ajouterOperateur);
			btAddOpe.label = "^";
			btAddOpe.addEventListener(MouseEvent.CLICK,monterOperateur);
			getListeOperateur(true);
		}
		
		//Prend l'opérateur selectionné dans la liste des ajouts et le met dans la liste des opérateurs de la société.
		private function monterOperateur(me : MouseEvent):void{			
			(liOperateur.dataProvider as ArrayCollection).addItem(liOperateurAajouter.selectedItem);	
		}
		
		//Affiche la liste des société du client dans la combo des 'Sociétés Parentes'
		private function afficherSocieteParente(ev : Event):void{
			cmbParent.labelField = "raisonSociale";
			var blancObject : Societe = new Societe();
			blancObject.adresse1 = "";			
			blancObject.societeID = 0;
			blancObject.societeParenteID = 0;
			blancObject.raisonSociale = "-";
			societes.liste.addItem(blancObject);
			cmbParent.dataProvider = societes.liste;
			
		}
		
		//Enregistre la société
		private function sauverSociete(MouseEvent : Event):void{			
			if(txtNom.text.length > 0){
				societe.adresse1 = txtAdresse1.text;
				societe.adresse2 = txtAdresse2.text;
				societe.commune = txtCommune.text;
				societe.raisonSociale = txtNom.text;
				societe.siretSiren = txtSiret.text;
				societe.codePsotal = txtZipcode.text;
				societe.telephone = txtTelephone.text;
				societe.fax = txtFax.text;			
				
				try{
					societe.societeParenteID = Societe(cmbParent.selectedItem).societeID;	
				}catch(e: Error){
					
				}
				
				societe.pays = PaysVO(cmbPays.selectedItem).nom;
				societe.paysId = PaysVO(cmbPays.selectedItem).id;
				
				if (societe.societeID > 0){					
					societe.updateSociete();
					
				}else {
					societe.sauvegarder(idGroupe);
				}
			}else{
				Alert.show("Vous devez indiquer la raison sociale","Attention !!!");
			}
		}
		
		//dispatch un évènement de type 'DetailSociete.VALIDER' signifiant que l'on vient de créer un société
		//ferme la fenêtre de création de société
		private function societeCree(ev : Event):void{
			_newSocieteID = societe.societeID;	
			dispatchEvent(new Event(DetailSociete.VALIDER));
			PopUpManager.removePopUp(this);
		}
		
		
		
		//Met à jour la liste des opérateur d'une société
		private function updateListOperateurs(ev : Event):void{
			societe.saveOperateur(societe.societeID,ConsoviewUtil.extractIDs("OPERATEURID",(liOperateur.dataProvider as ArrayCollection).source));
		}
		
		//Enregistre la liste des opérateurs distribués par une société
		private function savegarderOperateurs(ev : Event):void{			
			if (liOperateur.selectedItems.length > 0){		
					trace(liOperateur.selectedItems.length);	
					societe.saveOperateur(societe.societeID,ConsoviewUtil.extractIDs("OPERATEURID",liOperateur.selectedItems));				
			}else{
					Alert.show("Vous devez sélèctionner un ou plusieurs opérateurs","Attention !!!");
			}	
		}
		//Supprime la liste d'opérateurs selectionnés pour une société
		private function supprimerOpe(me : MouseEvent):void{
			if (liOperateur.selectedIndices.length > 0){
				liOperateur.selectedItems.forEach(supprimerCetteOperateur);
				(liOperateur.dataProvider as ArrayCollection).refresh();
			}
		}
		//Supprime un opérateur pour la société
		//voir la propriété forEach de la classe Array
		private function supprimerCetteOperateur(element : *, index : int, arr : Array):void{
			(liOperateur.dataProvider as ArrayCollection).removeItemAt(ConsoviewUtil.getIndexById((liOperateur.dataProvider as ArrayCollection)
																										,"OPERATEURID",arr[0].OPERATEURID));
		}
		
		//charge la liste des opérateur suivant le param modeAjout
		//si modeAjout est à true charge la liste des opérateur à ajouter
		//si non charge la liste complete des opérateurs 
		//param in modeAjout un booleen, placé a false par défaut
		private function getListeOperateur(modeAjout : Boolean = false):void
		{
			var typeGroupe : String = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;			
			var methode : String = "getOperateurlist";
			var callBackMethode : Function;
			var societeId : int = societe.societeID;
			
			if (societeId > 0){					
				
				if (modeAjout){
					methode = "getOperateurlist";
					callBackMethode = getListeOperateurAjoutResultHandler;
					societeId = 0;
				} else {
					methode = "getOperateurlistSociete";
					callBackMethode = getListeOperateurResultHandler;
					societeId = societe.societeID;
				} 
			}else{
				societeId = 0;
				methode = "getOperateurlist";
				callBackMethode = getListeOperateurResultHandler;
			}
				
			opOperateur  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Societe",
																				methode,
																				callBackMethode);	
			
			RemoteObjectUtil.callService(opOperateur,societeId);		
		}
		
		//Handler de la méthode getListeOperateur, affiche la liste des opérateurs
		private function getListeOperateurResultHandler(re :ResultEvent):void{
			var blancObject:Object = new Object();
			blancObject.OPERATEURID = 0;
			blancObject.NOM = "---";
			
			(re.result as ArrayCollection).addItem(blancObject);
			operateurs = re.result as ArrayCollection;
			liOperateur.labelField = "NOM"; 
			liOperateur.dataProvider = operateurs;			
			
		}
		
		//Supprime les opérateurs déja enregistrer pour une société, de la liste des opérateur à ajouter
		//param in source un ArrayCollection contenant la liste de tous les opérateurs
		//param out newColl un ArrayCollection contenant la liste des opérateurs pour l'ajout
		private function formatOperateurToAdd(source : ArrayCollection):ArrayCollection{
			
			
			var newColl : ArrayCollection = new ArrayCollection();
			for(var i : int = 0; i < source.length; i++){
				if(!ConsoviewUtil.isIdInArray(source[i].OPERATEURID,
													"OPERATEURID",
														operateurs.source)){
					newColl.addItem(source[i]);
				}
			}
			return newColl;			
		}
		
		//Handler de la méthode getListeOperateur, affiche la liste des opérateurs que l'on peut ajouter
		private function getListeOperateurAjoutResultHandler(re :ResultEvent):void{
			var blancObject:Object = new Object();
			blancObject.OPERATEURID = 0;
			blancObject.NOM = "---";
			
			(re.result as ArrayCollection).addItem(blancObject);
			operateursAjout = formatOperateurToAdd(re.result as ArrayCollection);
			liOperateurAajouter.labelField = "NOM"; 
			liOperateurAajouter.dataProvider = operateursAjout;			
			
		}
		
		//Charge la liste des pays de la base Consotel	
		private function getListePays():void{
			opPays  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Societe",
																				"getlistePays",
																				getListePaysResultHandler);	
			
			RemoteObjectUtil.callService(opPays);	
		}
		
		//Transforme la liste des pays en paysVO
		//param in source un ArrayCollection contenant des objets pays
		//param	out newColl un ArrayCollection contenant des paysVO
		private function formatePays(source : ArrayCollection):ArrayCollection{
			var newColl : ArrayCollection = new ArrayCollection();
			
			for (var i : int = 0; i < source.length; i++){
				var lePays : PaysVO = new PaysVO(source[i]);	
				newColl.addItem(lePays);
			}
			return newColl;
		}
		
		//Handler de la méthode getListePays affiche la liste des pays dans la combo 'Pays'
		private function getListePaysResultHandler(re : ResultEvent):void{
			pays = formatePays(re.result as ArrayCollection);
			pays.refresh();
			cmbPays.labelField = "nom";
			cmbPays.dataProvider = pays; 		 
			cmbPays.selectedIndex = ConsoviewUtil.getIndexById(pays,"id",FRANCE_ID);	
			cmbPays.enabled = false;
		}
		
	}
}