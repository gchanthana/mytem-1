package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets
{
	/**
	 * Classe Pays
	 * */
	public class PaysVO
	{
		/**
		 * Constructeur
		 * @param pays un objet pays avec les proriétés PAYSCONSOTELID, PAYS
		 * */
		public function PaysVO(pays : Object){
			_id = pays.PAYSCONSOTELID;
			_nom = pays.PAYS;
		}
		
		//le nom du pays
		private var _nom : String = "";
		
		/**
		 * Retourne le nom du pays
		 * @return _nom, le nom du pays
		 * */
		public function get nom():String{
			return _nom;
		}
		
		//l'identifiant du pays dans la base consotel
		private var _id : int = 0;		
		
		
		/**
		 * Retourne l'id du pays 
		 * @return _id, le l'id du pays
		 * */
		public function get id():int{
			return _id;
		}
	}
}