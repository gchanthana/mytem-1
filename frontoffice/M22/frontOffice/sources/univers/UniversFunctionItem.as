package univers {
	import flash.events.IEventDispatcher;

	public interface UniversFunctionItem extends IEventDispatcher {
		function onPerimetreChange():void;
	}
}
