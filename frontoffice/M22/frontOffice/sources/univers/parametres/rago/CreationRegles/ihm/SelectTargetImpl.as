package  univers.parametres.rago.CreationRegles.ihm
{
	import mx.resources.ResourceManager;
	import composants.datagrid.RowColorDataGrid;
	import composants.tree.event.SearchTreeEvent;
	
	import flash.events.Event;
	import flash.system.System;
	
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.DataGridEvent;
	import mx.events.ListEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.parametres.rago.CreationRegles.system.Wizzard;
	import univers.parametres.rago.entity.Regle;
	import univers.parametres.rago.entity.Rules;
	import univers.parametres.rago.entity.TypeRegle;
	import univers.parametres.rago.inventaire.system.GestionRegles;
	
	[Bindable]
	public class SelectTargetImpl extends MainCreateRegleImpl
	{
		
//VARIABLES GLOBALES----
	
		public var selectedRegle:Regle;
		public var gestionRegles:GestionRegles;
		
		public var rbg:RadioButtonGroup;
		public var dgListe:RowColorDataGrid;
		public var txtnbrdeline:Label;
		
		
		public var btnFindTargeteInTree:Button;
		public var btnFindTargetInDatagrid:Button;
		public var findItTarget:TextInput;
		
		private var _rules:Rules = new Rules();
		public var _targetWizzard:Wizzard;
		
		public var ruleExist:Boolean = false;

		private var _clientResult:Object;
		private var _operateurResult:Object;
		
//FIN VARIABLES GLOBALES----

//PROPIETEES PUBLICS----
		
		public function set clientResult(value:Object):void
		{ _clientResult = value; }

		public function get clientResult():Object
		{ return _clientResult; }

		public function set operateurResult(value:Object):void
		{ _operateurResult = value; }

		public function get operateurResult():Object
		{ return _operateurResult; }

//FIN PROPIETEES PUBLICS----

//METHODES PUBLICS----

		//Constructeur				
		public function SelectTargetImpl()
		{  }
		
//METHODES PUBLICS----

//METHODES PROTECTED----
		
		//Affiche la feuille sélectionnée dans le label 'lblTargetSelected'
		protected function choixTarget(event:Event):void
		{
			//btPre.enabled = true;
			initialiseRegle(event);
		}
		
		//List les régles contenant les même soureces et cible que la règle que l'on crée
		protected function initialiseRegle(event:Event):void//------------------------------------------------C'EST ICI QU'IL FAUT MODIFIER LA PROCEDURE*************
		{	
			if(_targetWizzard != null)
			{
				ruleExist = false;
				_rules.listRulesRacine(_targetWizzard.myRegle.IDSOURCE,_targetWizzard.myRegle.IDCIBLE,"");
				_rules.addEventListener("listRacine",executeAttributInDatagrid);
			}
		}

		//Rafraichie la liste les régles contenant les même soureces et cible que la règle que l'on crée
		//lors du click sur rafraichir
		protected function btnRefreshClickHandler(event:Event):void//-----------------------------------------C'EST ICI QU'IL FAUT MODIFIER LA PROCEDURE**********
		{
			ruleExist = false;
			_rules.listRulesRacine(_targetWizzard.myRegle.IDSOURCE,_targetWizzard.myRegle.IDCIBLE,"");
			_rules.addEventListener("listRacine",executeAttributInDatagrid);
		}

		private function lenghtFormator(txt:String):String
		{
			var returnStrg:String = "";
			var lastIndex:int = txt.lastIndexOf("/");
			
			returnStrg = txt.substring(lastIndex+1);
			if(returnStrg.length > 50)
			{
				returnStrg = returnStrg.substr(0,50);
			}
			return returnStrg;
		}

		//On ne fais rien lorsque l'on click sur annuler dans les popUp arbre et datagrid
		protected function pasChoixTarget(event:Event):void
		{  }
		
		protected function initSelectTargetIHM(event:Event):void
		{  }
		
		//Affecte 'BOOL_SUIVRE_AUTO' à true ou false
		protected function valider():void
		{
			if(_targetWizzard.myRegle)
			{
				if(rbg.selectedValue == "OUI")
				{ _targetWizzard.myRegle.BOOL_SUIVRE_AUTO = true; }
				else
				{ _targetWizzard.myRegle.BOOL_SUIVRE_AUTO = false; }
			}
		}
		
		protected function _localeNodeSeletcedHandler(event:SearchTreeEvent):void
		{	
			_targetWizzard.feuilleCible = event.node;
			if(_targetWizzard.feuilleCible == null) return;
			
			if(_targetWizzard.feuilleCible.hasOwnProperty("@label"))
			{ libelleNodeSelected =	event.node.@label; }
		}
		
		//Formateur de statut
		protected function formatStatut(item:Object,col:DataGridColumn):String
		{
			if(item == null) return '';
			
			if(item.BOOL_ACTIVE)
			{ return "Actif"; }
			else if(item.BOOL_ACTIVE == false)  
			{ return "Inactif"; }
			else
			{ return ""; }
		}
		
		//Formateur de type de règle
		protected function formatType(item:Object,col:DataGridColumn):String
		{
			if(item == null) return '';
			
			switch(item.TYPE_REGLE)
			{
				case TypeRegle.REGLE_ANNUAIRE:
					return ResourceManager.getInstance().getString('M22', 'Annuaire'); 
				case TypeRegle.REGLE_OPERATEUR:
					return ResourceManager.getInstance().getString('M22', 'Op_rateur'); 
				case TypeRegle.REGLE_CLIENT:
					return ResourceManager.getInstance().getString('M22', 'Cliente_'); 
				default:
					return ""; 
			}
		}
		
		//Formateur de date
		protected function formatColonneDate(item:Object,col:DataGridColumn):String
		{	
			if(item == null) return '';
			var df:DateFormatter = new DateFormatter();
			df.formatString = 'DD/MM/YYYY';
			return df.format(item[col.dataField]);
		}
		
		//Formateur de mise en forme dans le tableau
		protected function rowColorFunction(datagrid:DataGrid, rowIndex:int, color:uint):uint
		{
			var rColor:uint;
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			if(item == null) return color;
			
			if (!item.BOOL_ACTIVE)
			{ rColor = 0xF5E49A; }
			else rColor = color;
			
			return rColor;
		}

//------------------------------------------- SORTS ---------------------------------------------------------------------
		
		//Comparateur de date
		protected function dateCompareFunction(itemA:Object,itemB:Object):int
		{ return ObjectUtil.dateCompare(itemA.CREATE_DATE,itemB.CREATE_DATE); }
		
		//Dès que un click ce fait sur une ligne on affecte l'objet sélectionné à la règle sélectionnée
		protected function _dgListeChangeHandler(event:ListEvent):void
		{ selectedRegle = dgListe.selectedItem as Regle; }
		
		//Lorsque l'on click sur le datagrid
		protected function _dgListeEditBeginningHandler(event : DataGridEvent):void
		{			
		     if (event.itemRenderer != null && event.itemRenderer.data != null && event.dataField != null)
		     {
		     	if(event.itemRenderer.data[event.dataField]!=null)
		     	{
			     var txt : String = event.itemRenderer.data[event.dataField];
		    	 System.setClipboard(txt); 
		    	 trace("Presse papier : " + txt);
       			}
       		}
       		event.preventDefault();
	    }
		
		//Ouvre la popUp pour la sélection de la cible dans un grid
		protected function findTargetInDatagridClickHandler(event:Event):void
		{
			var p:PopUpTargetDatagridIHM = new PopUpTargetDatagridIHM();
			
			p._wizzard = _targetWizzard;
			p.resultClient = clientResult;
			p._strgToSearch = findItTarget;
			p.addEventListener("getChoiceTarget", choixTarget);
			p.addEventListener("AnnulChoiceTarget", pasChoixTarget);
			PopUpManager.addPopUp(p,this,true);
			PopUpManager.centerPopUp(p);
		}
		
		//Ouvre la popUp pour la sélection de la cible dans un arbre
		protected function findTargetInTreeClickHandler(event:Event):void
		{
			var p:PopUpTargetTreeIHM = new PopUpTargetTreeIHM();
			
			p._wizzard = _targetWizzard;
			p.resultClient = clientResult;
			p._resultOpe = operateurResult;
			p.addEventListener("getChoiceTarget", choixTarget);
			p.addEventListener("AnnulChoiceTarget", pasChoixTarget);
			PopUpManager.addPopUp(p,this,true);
			PopUpManager.centerPopUp(p);
		}
		
		//Enabled ou non selon l'état du radiobtn (enabled = false la partie arbre)
		protected function rdgTreeChangeHandler(event:Event):void
		{ zAttributEnabledOrNot(false,false,true); }
		
		//Enabled ou non selon l'état du radiobtn (enabled = false la partie datagrid)
		protected function rdgNoTreeChangeHandler(event:Event):void
		{ zAttributEnabledOrNot(true,true,false); }
		
//FIN METHODES PROTECTED----		

//METHODES PRIVATE----
		
		//Attribut au datagrid les différents champs retourné par la procédure
		private function executeAttributInDatagrid(event:Event):void
		{	
			ruleExist = false;
			if(_rules.listRegle.length > 0)
			{
				ruleExist = true;
			}
			callLater(refreshDatagrid);
		}
		
		//Rafraichi le grid
		private function refreshDatagrid():void
		{
			dgListe.dataProvider = _rules.listRegle;
			txtnbrdeline.text = _rules.listRegle.length.toString();
		}
		
		//Fonction : Enabled ou non selon l'état du radiobtn
		private function zAttributEnabledOrNot(bool1:Boolean,bool2:Boolean,bool3:Boolean):void
		{
			btnFindTargetInDatagrid.enabled = bool1;
			findItTarget.enabled = bool2;
			btnFindTargeteInTree.enabled = bool3;
		}

//FIN METHODES PRIVATE----
		
	}
}