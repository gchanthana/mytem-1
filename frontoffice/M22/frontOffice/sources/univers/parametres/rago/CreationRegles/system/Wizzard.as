package univers.parametres.rago.CreationRegles.system
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import univers.parametres.rago.entity.Regle;

	public class Wizzard extends EventDispatcher
	{
		private var _myRegle:Regle = new Regle();
		// le noeud source		
		private var _noeudSource:Object;
		
		// la feuille cible
		private var _feuilleCible:Object;
		
		private var _userName:String;
		
		//liste des organisations sources (orgas Opérateur / orga Annuaire);
		private var _listeOrganisationsSources:ArrayCollection = new ArrayCollection();
		
		//liste des organisations cibles (orgas clientes);
		private var _listeOrganisationsCibles:ArrayCollection = new ArrayCollection();
		
		//liste des exceptions
		private var _listeExceptions:ArrayCollection = new ArrayCollection();
		
		private var _exception:Object;
		
		public var _nbrOfStage:int = 0;
		
		public var _endStageOrNot:Boolean = false;
		
		//l'orga source sélectionnée
		private var _organisationSource:Object;
        
        //l'orga cible selectionnéé
		private var _organisationCible:Object;
		
		private var _jumpException:Boolean = false;
		public function get jumpException():Boolean { return _jumpException; }
		
		public function set jumpException(value:Boolean):void
		{
			if (_jumpException == value)
				return;
			_jumpException = value;
		}
		
				
		public function Wizzard(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		/// ---------------------- ACCESORS --------------------------------------------
		[Bindable]public function set myRegle(value:Regle):void
		{
			_myRegle = value;
		}

		public function get myRegle():Regle
		{
			return _myRegle;
		}
		
		[Bindable]public function set userName(value:String):void
		{
			_userName = value;
		}

		public function get userName():String
		{
			return _userName;
		}

		[Bindable]public function set noeudSource(value:Object):void
		{
			if(value != _noeudSource)
			{
				listeExceptions = null;
				listeExceptions = new ArrayCollection();
			}
			_noeudSource = value;
			
		}

		public function get noeudSource():Object
		{
			return _noeudSource;
		}

		[Bindable]public function set feuilleCible(value:Object):void
		{
			_feuilleCible = value;
		}
		

		public function get feuilleCible():Object
		{
			return _feuilleCible;
		}

		[Bindable]public function set listeOrganisationsSources(value:ArrayCollection):void
		{
			_listeOrganisationsSources = value;
		}

		public function get listeOrganisationsSources():ArrayCollection
		{
			return _listeOrganisationsSources;
		}
		
		[Bindable]public function set listeOrganisationsCibles(value:ArrayCollection):void
		{
			_listeOrganisationsCibles = value;
		}

		public function get listeOrganisationsCibles():ArrayCollection
		{
			return _listeOrganisationsCibles;
		}
		
		[Bindable]public function set organisationSource(value:Object):void
		{
			if(value != _organisationSource)
			{
				noeudSource = null;
			}
			_organisationSource = value;
		}

		public function get organisationSource():Object
		{
			return _organisationSource;
		}
		
		[Bindable]public function set organisationCible(value:Object):void
		{
			_organisationCible = value;
		}

		public function get organisationCible():Object
		{
			return _organisationCible;
		}

		[Bindable]public function set listeExceptions(value:ArrayCollection):void
		{
			_listeExceptions = value;
		}

		public function get listeExceptions():ArrayCollection
		{
			return _listeExceptions;
		}
		
		//----------------------------------------------------------------------------------
		
		//ajoute
		public function addException(exception:Object):void
		{
			if(exception == null) return;
			
			listeExceptions.addItem(exception);	
		}
		
		public function removeException(exception:Object):void
		{
			if(exception == null) return;
						
			listeExceptions.removeItemAt(listeExceptions.getItemIndex(exception));
		}
	}
}