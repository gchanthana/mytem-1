package  univers.parametres.rago.CreationRegles.ihm
{
	import composants.tree.event.SearchTreeEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.List;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	
	import univers.parametres.rago.CreationRegles.system.Wizzard;
	
	[Bindable]
	public class SelectExceptionsImpl  extends MainCreateRegleImpl
	{

//VARIABLES GLOBALES----

		public var btAdd:Button;
		public var btSupp:Button;
		public var searchabletreeihm2:destinataireOrga = new destinataireOrga();
		public var listeExceptions:List;
		public var txtFiltre:TextInput;
		public var txtFiltreSelected:TextInput;
		public var dgListSelectExceptions:DataGrid;
		public var dgListSelectedExceptions:DataGrid;
		public var nbrExceptionSelect:Label;
		public var nbrExceptionSelected:Label;
		public var ckbSelect:CheckBox;
		public var ckbSelected:CheckBox;
		public var checkBoxSelectAll:CheckBox;
		public var checkBoxSelectedAll:CheckBox;
		
		public var _exceptionWizzard:Wizzard;
		
		private var _dataToAddIndex:ArrayCollection = new ArrayCollection();
		private var _dataToRemoveIndex:ArrayCollection = new ArrayCollection();
		private var _dataToAdd:ArrayCollection = new ArrayCollection();
		private var _dataToRemove:ArrayCollection = new ArrayCollection();
		
		private var _dataSelectItems:ArrayCollection = new ArrayCollection();
		private var _dataSelectedItems:ArrayCollection = new ArrayCollection();
			
		
		private var noeudException:Object;
		private var _listChildException:ArrayCollection = new ArrayCollection();
		public var _arrayCollectionSelected:ArrayCollection = new ArrayCollection();

		public var _nbrException:int;
		public var _nbrExceptionSelected:int;
		
		private var nbrRemoved:int = 0;
		private var nbrMoved:int = 0;
		
		private var oldValue:String = "";

//FIN VARIABLES GLOBALES----

//PROPRIETEES PUBLICS----
		
		//Récupère toutes les feuilles d'un noeuds origine
		public function set listChildException(value:ArrayCollection):void
		{ _listChildException = value; }

		public function get listChildException():ArrayCollection
		{ return _listChildException; }

		//Recupère les feuille à mettre en exceptions
		public function set dataToAdd(value:ArrayCollection):void
		{ _dataToAdd = value; }

		public function get dataToAdd():ArrayCollection
		{ return _dataToAdd; }

//FIN PROPRIETEES PUBLICS----

//METHODES PUBLICS----
		
		//Constructeur
		public function SelectExceptionsImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, _localeCreationCompleteHandler);
			addEventListener(SearchTreeEvent.NODE_SELETCED, _localeNodeSeletcedHandler);
			addEventListener("thisNodeIsAnExceptionSelect", thisNodeIsAnExceptionSelect);
			addEventListener("thisNodeIsNotAnExceptionSelect", thisNodeIsNotAnExceptionSelect);
			addEventListener("thisNodeIsAnExceptionSelected", thisNodeIsAnExceptionSelected);
			addEventListener("thisNodeIsNotAnExceptionSelected", thisNodeIsNotAnExceptionSelected);
//			searchabletreeihm2.addEventListener("finishToRefresh",searchData);
		}
		
		//Affiche l'arbre selon la source selectionnée
		public function zListNoeud():void
		{ if(_exceptionWizzard.noeudSource!=null) { executePrintTree(_exceptionWizzard.noeudSource); } }
		
//FIN METHODES PUBLICS----

//METHODES PROTECTED----
		
		//Sélectionne tous les éléments du grid de gauche
		protected function selectAll(event:Event):void
		{
			var _arrayTempCollectionSelect:ArrayCollection = new ArrayCollection();

			dgListSelectExceptions.dataProvider = treatementInverseArrayCollection(_arrayTempCollectionSelect,_arrayCollectionSelected,checkBoxSelectAll.selected);
		}
		
		//Sélectionne tous les éléments du grid de droite
		protected function selectedAll(event:Event):void
		{
			var _arrayTempCollectionSelect:ArrayCollection = new ArrayCollection();

			dgListSelectedExceptions.dataProvider = treatementInverseArrayCollection(_arrayTempCollectionSelect,dataToAdd,checkBoxSelectedAll.selected);
		}
		
		//Dès que l'on sélectionne un noeuds à mettre en exception
		protected function thisNodeIsAnExceptionSelect(event:Event):void
		{  }
	
		//Dès que l'on désélectionne un noeuds à mettre en exception
		protected function thisNodeIsNotAnExceptionSelect(event:Event):void
		{  }
		
		//Dès que l'on sélectionne une exception à mettre dans le grid de gauche
		protected function thisNodeIsAnExceptionSelected(event:Event):void
		{  }
		
		//Dès que l'on désélectionne une exception à mettre dans le grid de gauche
		protected function thisNodeIsNotAnExceptionSelected(event:Event):void
		{  }
		
		//Dès que le viewstack 'SelectExceptionsIHM' est visible affiche dans le grid de gauche les enfants du noeuds sources sélectionné
		protected function whenThisVisible(event:Event):void
		{ callLater(searchData); }
		
		//Lors de l'initialisation de l'IHM affiche dans le grid de gauche les enfants du noeuds sources sélectionné
		protected function initSelectExceptionIHM(event:Event):void
		{ searchData(); }
		
		override protected function _localeCreationCompleteHandler(event:FlexEvent):void
		{ super._localeCreationCompleteHandler(event); }
		
//============================================== HANDLERS ===========================================================================//
		
		//Appel a la fonction addExceptions pour mettre dans le dataprovider de droite les exceptions selectionnees
		protected function _btAddClickHandler(event:MouseEvent):void
		{ executeBtnAdd(); }

		//Appel a la fonction suppExceptions pour mettre dans le dataprovider de gauche les exceptions selectionnees
		protected function _btSuppClickHandler(event:MouseEvent):void
		{ executeBtnSupp(); }

		protected function _localeNodeSeletcedHandler(event:SearchTreeEvent):void
		{ noeudException = event.node; }
		
		//Réinitialise le textimput et le dataprovider 
		protected function searchClickHandler(event:Event):void
		{
			if(_arrayCollectionSelected != null)
			{
				txtFiltre.text = "";
				dgListSelectExceptions.dataProvider = _arrayCollectionSelected;
				nbrExceptionSelect.text = _arrayCollectionSelected.length.toString();
			}
		}
		
		//Lorsque le filtre change
		protected function filtreChangeHandler(event:Event):void
		{
			var arrayCollectionSearch:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0;i < _arrayCollectionSelected.length;i++)
			{
				if(_arrayCollectionSelected[i].LABEL.toString().toLowerCase().indexOf(txtFiltre.text.toLowerCase()) != -1)
				{ arrayCollectionSearch.addItem(_arrayCollectionSelected[i]); }
			}
			
			if(_arrayCollectionSelected.length == 0)
			{ dgListSelectExceptions.dataProvider = null; }
			else
			{ dgListSelectExceptions.dataProvider = arrayCollectionSearch; }
			nbrExceptionSelect.text = arrayCollectionSearch.length.toString();
		}
		
		//Réinitialise le textimput et le dataprovider 
		protected function searchSelectedClickHandler(event:Event):void
		{
			if(dataToAdd != null)
			{
				txtFiltreSelected.text = "";
				dgListSelectedExceptions.dataProvider = dataToAdd;
				nbrExceptionSelected.text = dataToAdd.length.toString();
			}
		}
		
		//Lorsque le filtre change
		protected function filtreSelectedChangeHandler(event:Event):void
		{
			var arrayCollectionSearch:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0;i < dataToAdd.length;i++)
			{
				if(dataToAdd[i].LABEL.toString().toLowerCase().indexOf(txtFiltreSelected.text.toLowerCase()) != -1)
				{ arrayCollectionSearch.addItem(dataToAdd[i]); }
			}
			
			if(arrayCollectionSearch.length == 0)
			{ dgListSelectedExceptions.dataProvider = null; }
			else
			{ dgListSelectedExceptions.dataProvider = arrayCollectionSearch; }
							
			nbrExceptionSelected.text = arrayCollectionSearch.length.toString();
		}

//FIN METHODES PROTECTED----

//METHODES PRIVATE----
		
		//Récupére l'ancienne valeur de LIBELLE_SOURCE
		private function recupOldValue():String
		{ return _exceptionWizzard.myRegle.LIBELLE_SOURCE; }
		
		//Attribut les 2 tableaux (dataToAdd et listChildExceptions) au dataprovider correspondant
		//Fonction permettant de créer une indépendance entre les tableaux et les dataprovider
		private function treatementInverseArrayCollection(array0:ArrayCollection,array1:ArrayCollection,bool:Boolean):ArrayCollection
		{
			for(var i:int = 0;i < array1.length;i++)
			{
				array1[i].SELECTED = bool;
				array0.addItem(array1[i]);
			}
			
			return array0;
		}
		
		//Mise à jour du datagrid
		private function searchData():void
		{		
			if(listChildException != null && listChildException.length != 0)
			{ 
				var _arrayTempCollectionSelected:ArrayCollection = new ArrayCollection();
				if(oldValue == "")
				{
					oldValue = recupOldValue();
					_exceptionWizzard.myRegle.LIST_EXCEPTION = new ArrayCollection();
					_arrayCollectionSelected = new ArrayCollection();
					dataToAdd = new ArrayCollection();
					_arrayCollectionSelected = new ArrayCollection();
					dgListSelectExceptions.dataProvider = null;
					search();

					dgListSelectExceptions.dataProvider = treatementArrayCollection(_arrayCollectionSelected,_arrayTempCollectionSelected);
				}
				else
				{
					if(oldValue != _exceptionWizzard.myRegle.LIBELLE_SOURCE)
					{
						oldValue = recupOldValue();
						_exceptionWizzard.myRegle.LIST_EXCEPTION = new ArrayCollection();
						_arrayCollectionSelected = new ArrayCollection();
						dataToAdd = new ArrayCollection();
						_arrayCollectionSelected = new ArrayCollection();
						dgListSelectExceptions.dataProvider = null;
						search();
						
						dgListSelectExceptions.dataProvider = treatementArrayCollection(_arrayCollectionSelected,_arrayTempCollectionSelected);
					}
				}
			
			}
		}
		
		//Traitement du retour de procédure en XML en tableau (affichage de tous les noeuds et feuilles) et l'affiche dans le grid de gauche
		private function search():void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			var dataSource:ArrayCollection = new ArrayCollection();
			var dataFound:ArrayCollection = new ArrayCollection();
			var dataFoundConcat:ArrayCollection = new ArrayCollection();
			var grandParents:XMLListCollection = listChildException[0] as XMLListCollection;
			//recupere le dossier maitre
			var parents:XML;
			//Recupere le dossier parents
//			var mere:XMLList = parents.children();
//			var mere:XMLList = listChildException as XMLList;
			var obj:Object = new Object();
		try
		{
			for(var h:int = 0;h < grandParents.length;h++)
			{
				obj = new Object();
				parents = grandParents[h] as XML;

				dataFound.addItem(parents);
				
				obj.VALUE = parents.@VALUE;
				obj.NIV = parents.@NIV;
				obj.LABEL = parents.@LABEL;
				obj.SELECTED = false;
				dataFoundConcat.addItem(obj);
				
				var mere:XMLList = parents.children();

			for(var i:int = 0;i < mere.length();i++)
			{	
				obj = new Object();
				dataFound.addItem(mere[i]);
				
				obj.VALUE = mere[i].@VALUE;
				obj.NIV = mere[i].@NIV;
				obj.LABEL = parents.@LABEL + "/" + mere[i].@LABEL;
				obj.SELECTED = false;
				dataFoundConcat.addItem(obj);

				var enfants0:XMLList = mere[i].children();
				for(var j:int = 0;j < enfants0.length();j++)
				{	
					obj = new Object();
					dataFound.addItem(enfants0[j]);
					
					obj.VALUE = enfants0[j].@VALUE;
					obj.NIV = enfants0[j].@NIV;
					obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL;
					obj.SELECTED = false;
					dataFoundConcat.addItem(obj);

					var enfants1:XMLList = enfants0[j].children();
					for(var k:int = 0;k < enfants1.length();k++)
					{
						obj = new Object();
						dataFound.addItem(enfants1[k]);
						
						obj.VALUE = enfants1[k].@VALUE;
						obj.NIV = enfants1[k].@NIV;
						obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL;
						obj.SELECTED = false;
						dataFoundConcat.addItem(obj);
			
						var enfants2:XMLList = enfants1[k].children();
						for(var l:int = 0;l < enfants2.length();l++)
						{
							obj = new Object();
							dataFound.addItem(enfants2[l]);
							
							obj.VALUE = enfants2[l].@VALUE;
							obj.NIV = enfants2[l].@NIV;
							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL;
							obj.SELECTED = false;
							dataFoundConcat.addItem(obj);
			
							var enfants3:XMLList = enfants2.children();
							for(var m:int = 0;m < enfants3.length();m++)
							{
								obj = new Object();
								dataFound.addItem(enfants3[m]);
								
								obj.VALUE = enfants3[m].@VALUE;
								obj.NIV = enfants3[m].@NIV;
								obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL;
								obj.SELECTED = false;
								dataFoundConcat.addItem(obj);
					
								var enfants4:XMLList = enfants3.children();
								for(var n:int = 0;n < enfants4.length();n++)
								{
									obj = new Object();
									dataFound.addItem(enfants4[n]);
									
									obj.VALUE = enfants4[n].@VALUE;
									obj.NIV = enfants4[n].@NIV;
									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL;
									obj.SELECTED = false;
									dataFoundConcat.addItem(obj);
							
									var enfants5:XMLList = enfants4.children();
									for(var o:int = 0;o < enfants5.length();o++)
									{
										obj = new Object();
										dataFound.addItem(enfants5[o]);
										
										obj.VALUE = enfants5[o].@VALUE;
										obj.NIV = enfants5[o].@NIV;
										obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL;
										obj.SELECTED = false;
										dataFoundConcat.addItem(obj);
									
										var enfants6:XMLList = enfants5.children();
										for(var p:int = 0;p < enfants6.length();p++)
										{
											obj = new Object();
											dataFound.addItem(enfants6[o]);
										
											obj.VALUE = enfants6[p].@VALUE;
											obj.NIV = enfants6[p].@NIV;
											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL;
											obj.SELECTED = false;
											dataFoundConcat.addItem(obj);
											
										}
									}
								}
							}
						}
					}
				
				}
			}
			}
			
			if(dataFound.length != 0)
			{	
				listChildException = new ArrayCollection();
				
				for(var cpt:int = 0;cpt < dataFoundConcat.length;cpt++)
				{
					_arrayCollectionSelected.addItem(dataFoundConcat[cpt]);
				}
				
				dgListSelectExceptions.dataProvider = treatementArrayCollection(dataFoundConcat,listChildException);
				dgListSelectedExceptions.dataProvider = null;
				nbrExceptionSelect.text = dataFoundConcat.length.toString();
				nbrExceptionSelected.text = "0";
			}
		}
		catch (error:Error){
				// code dans le cas d'une erreur non-spécifique
			}
		}
		
		//Affiche l'arbre selon la source selectionnée
		private function executePrintTree(objectSelected:Object):void
		{ 
			listChildException = new ArrayCollection();
			searchabletreeihm2.fillTree(objectSelected);
			listChildException.addItem(searchabletreeihm2.treeDataArray);
		}

		//Appel de la fonction d'envoie des exceptions
		private function executeBtnAdd():void
		{ removeDatagrid(); }
		
		//Appel de la fonction de retour des exceptions
		private function executeBtnSupp():void
		{ addDatagrid(); }
		
		//Envoie UNIQUEMENTS les éléments sélectionnés dans le datagrid des exceptions lors du click sur le bouton 'add exception'
		private function removeDatagrid():void
		{
			var _arrayCollectTemp:ArrayCollection = new ArrayCollection();
			var arrayPlace:Array = new Array();
			var cptr:int = 0;
		
			for(var i:int = 0;i < _arrayCollectionSelected.length;i++)
			{ 
				if(_arrayCollectionSelected[i].SELECTED)
				{
					_arrayCollectionSelected[i].SELECTED = false;
					dataToAdd.addItem(_arrayCollectionSelected[i]);
					arrayPlace[arrayPlace.length] = i;
//					_arrayCollectionSelected.removeItemAt(i);
					cptr++;
				}
			}
			
			var cptrRemove:int = 0;
			for(var j:int = 0;j < arrayPlace.length;j++)
			{ 
				var tempCpt:int = arrayPlace[j]-cptrRemove;
				_arrayCollectionSelected.removeItemAt(tempCpt);
				cptrRemove++;
			}
			
			if(cptr > 0)
			{
				dgListSelectExceptions.dataProvider = treatementArrayCollection(_arrayCollectionSelected,_arrayCollectTemp);
				dgListSelectedExceptions.dataProvider = treatementArrayCollection(dataToAdd,_arrayCollectTemp);
			
				_exceptionWizzard.myRegle.LIST_EXCEPTION = dataToAdd;
			
				nbrExceptionSelect.text = _arrayCollectionSelected.length.toString();
				nbrExceptionSelected.text = dataToAdd.length.toString();
				checkBoxSelectAll.selected = false;
			}
		}
		
		//Retourne UNIQUEMENT les éléments mis en exceptions dans le tableau de dépert lors du click sur le bouton 'retour arrière'
		private function addDatagrid():void
		{	
			var _arrayCollectTemp:ArrayCollection = new ArrayCollection();
			var arrayPlace:Array = new Array();
			var cptr:int = 0;
			
			for(var i:int = 0;i < dataToAdd.length;i++)
			{ 
				if(dataToAdd[i].SELECTED)
				{
					dataToAdd[i].SELECTED = false;
					_arrayCollectionSelected.addItem(dataToAdd[i]);
					arrayPlace[arrayPlace.length] = i;
//					dataToAdd.removeItemAt(i);
					cptr++;
				}
			}
			
			var cptrRemove:int = 0;
			for(var j:int = 0;j < arrayPlace.length;j++)
			{ 
				var tempCpt:int = arrayPlace[j]-cptrRemove;
				dataToAdd.removeItemAt(tempCpt);
				cptrRemove++;
			}
			
			if(cptr > 0)
			{
				dgListSelectExceptions.dataProvider = treatementArrayCollection(_arrayCollectionSelected,_arrayCollectTemp);
				dgListSelectedExceptions.dataProvider = treatementArrayCollection(dataToAdd,_arrayCollectTemp);
			
				_exceptionWizzard.myRegle.LIST_EXCEPTION = dataToAdd;
			
				nbrExceptionSelect.text = _arrayCollectionSelected.length.toString();
				nbrExceptionSelected.text = dataToAdd.length.toString();
				checkBoxSelectedAll.selected = false;
			}
		}
		
		//Attribut les 2 tableaux (dataToAdd et listChildExceptions) au dataprovider correspondant
		//Fonction permettant de créer une indépendance entre les tableaux et les dataprovider
		private function treatementArrayCollection(array0:ArrayCollection,array1:ArrayCollection):ArrayCollection
		{
			array1 = new ArrayCollection();
			for(var i:int = 0;i < array0.length;i++)
			{
				array0[i].SELECTED = false;
				array1.addItem(array0[i]);
			}
			return array1;
		}
		
		
	}
}