package univers.parametres.rago.CreationRegles.ihm
{
	import flash.events.Event;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.managers.PopUpManager;
	
	import univers.parametres.rago.CreationRegles.system.Wizzard;
	import univers.parametres.rago.entity.Rules;
	
	[Bindable]
	public class PopUpSourceTreeImpl extends TitleWindow
	{

//VARIABLES GLOBALES----
		
		private var _rules:Rules;
		
		public var _wizzard:Wizzard;
		public var searchabletreeihm1:destinataireOrga;
		
		public var btChoisir:Button;
		public var btCancel:Button;
		
		private var _resultOpeAnnu:Object;
		
//FIN VARIABLES GLOBALES----		

//PROPRIETEES PUBLICS----

		public function set resultOpeAnnu(value:Object):void
		{ _resultOpeAnnu = value; }

		public function get resultOpeAnnu():Object
		{ return _resultOpeAnnu; }
		
//FIN PROPRIETEES PUBLICS----

//METHODES PUBLICS----
			
		public function PopUpSourceTreeImpl()
		{ addEventListener("closePopUpTree",sourceTreeAnnul); }

//METHODES PUBLICS----

//METHODES PROTECTED----
		
		protected function _closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		//Récupération de l'XML pour l'afficher dans l'arbre
		protected function creationPopUpSourceTreeIHM(event:Event):void
		{
			searchabletreeihm1.clearSearch();			
			searchabletreeihm1._selectedOrga = resultOpeAnnu.IDGROUPE_CLIENT;
			searchabletreeihm1.refreshTree();
		}
		
		//Dipatch un event lors du click sur le bouton 'valider'
		protected function btChoisirClickHandler(event:Event):void
		{ 
			dispatchEvent(new Event("getChoiceSource",true));
			PopUpManager.removePopUp(this); 
		}
		
		//Dipatch un event lors du click sur le bouton 'annuler'
		protected function btCancelClickHandler(event:Event):void
		{ dispatchEvent(new Event("closePopUpTree")); }
		
		//Récupère ce qui a été sélectionné dans l'arbre et regarde si ce que l'on a choisi 
		//est une feuille ou non pour pouvoir sauter l'étape des exception ou non
		protected function searchabletreeihm1_SourceClickHandler(event:Event):void
		{ 
			_wizzard.noeudSource = searchabletreeihm1.getSelectedItem();
			if(_wizzard.noeudSource!=null)	
			{
				btChoisir.enabled = true;
				_wizzard.noeudSource =  searchabletreeihm1.getSelectedItem();
				
				try
				{
					_wizzard._endStageOrNot = (searchabletreeihm1.getSelectedItem() as XML).hasSimpleContent(); }
				catch(error:Error) { _wizzard._endStageOrNot = false; }
				_wizzard.myRegle.LIBELLE_SOURCE = searchabletreeihm1.getSelectedItem().@LABEL;
				_wizzard.myRegle.IDSOURCE = searchabletreeihm1.getSelectedItem().@VALUE;
			}
		}

//FIN METHODES PROTECTED----

//METHODES PRIVATE----
	
		//Dipatch un event lors du click sur le bouton 'annuler'
		private function sourceTreeAnnul(event:Event):void
		{ 
			dispatchEvent(new Event("AnnulChoiceSource",true));
			PopUpManager.removePopUp(this); 
		}
		
//FIN METHODES PRIVATE----	

	}
}