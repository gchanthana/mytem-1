package univers.parametres.rago.entity
{
    import composants.access.perimetre.tree.INodeInfos;
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.resources.ResourceManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;

    [Bindable]
    public class Rules extends EventDispatcher
    {
/////////VARIABLES GLOBALES************************************************************
        public var _linesType:Number;
        private var nodeToLoadChildren:XML = new XML();
        /**
         * Infos concernant le noeud séléctionné
         * */
        private var nodeInfosData:INodeInfos;
        public var concatNameSurname:String = "";
        private var _listRegleRacine:ArrayCollection = new ArrayCollection(null);
        private var _listOrgaCliente:ArrayCollection = new ArrayCollection(null);
        private var _listOrgaOpe:ArrayCollection = new ArrayCollection(null);
        private var _listOrgaAnu:ArrayCollection = new ArrayCollection(null);
        private var _listOrgaOpeAnu:ArrayCollection = new ArrayCollection(null);
        private var _listExecption:ArrayCollection = new ArrayCollection(null);
        private var _arbre:ArrayCollection = new ArrayCollection(null);
        private var _orga:ArrayCollection = new ArrayCollection(null);
        private var _listRegle:ArrayCollection = new ArrayCollection(null);
        private var _listRegleActive:ArrayCollection = new ArrayCollection(null);
        private var _listRegleInactive:ArrayCollection = new ArrayCollection(null);
        private var _listRegleExceptions:ArrayCollection = new ArrayCollection(null);
        private var _idRegleSend:int;

/////////FIN VARIABLES GLOBALES********************************************************
/////////PROPRIETES********************************************************************	
        //Recupère la liste des exceptions  d'une règle
        public function get listRegleExceptions():ArrayCollection
        {
            return _listRegleExceptions;
        }

        public function set listRegleExceptions(listRules:ArrayCollection):void
        {
            _listRegleExceptions = listRules;
        }

        //Recupère la liste des règles
        public function get ListRegleRacine():ArrayCollection
        {
            return _listRegleRacine;
        }

        public function set ListRegleRacine(listRules:ArrayCollection):void
        {
            _listRegleRacine = new ArrayCollection(null);
            _listRegleRacine = listRules;
        }

        //Recupère la liste des organisations Cliente
        public function get ListOrgaCliente():ArrayCollection
        {
            return _listOrgaCliente;
        }

        public function set ListOrgaCliente(listOrga:ArrayCollection):void
        {
            _listOrgaCliente = new ArrayCollection(null);
            _listOrgaCliente = listOrga;
        }

        //Recupère la liste des organisations Opérateur
        public function get ListOrgaOpe():ArrayCollection
        {
            return _listOrgaOpe;
        }

        public function set ListOrgaOpe(listOrga:ArrayCollection):void
        {
            _listOrgaOpe = new ArrayCollection(null);
            _listOrgaOpe = listOrga;
        }

        //Recupère la liste des organisations Annuaire
        public function get ListOrgaAnu():ArrayCollection
        {
            return _listOrgaAnu;
        }

        public function set ListOrgaAnu(listOrga:ArrayCollection):void
        {
            _listOrgaAnu = new ArrayCollection(null);
            _listOrgaAnu = listOrga;
        }

        //Recupère la liste des organisations Annuaire et Opérateur
        public function get ListOrgaOpeAnu():ArrayCollection
        {
            return _listOrgaOpeAnu;
        }

        public function set ListOrgaOpeAnu(listOrga:ArrayCollection):void
        {
            _listOrgaOpeAnu = new ArrayCollection(null);
            _listOrgaOpeAnu = listOrga;
        }

        //Recupere la liste des exceptions
        public function get ListExecption():ArrayCollection
        {
            return _listExecption;
        }

        public function set ListExecption(listExcep:ArrayCollection):void
        {
            _listExecption = new ArrayCollection();
            _listExecption = listExcep;
        }

        //Recupere un arbre
        public function get Arbre():ArrayCollection
        {
            return _arbre;
        }

        public function set Arbre(listArbre:ArrayCollection):void
        {
            _arbre = listArbre;
        }

        //Recupere les oraganisation
        public function get Orgatemp():ArrayCollection
        {
            return _orga;
        }

        public function set Orgatemp(organisations:ArrayCollection):void
        {
            _orga = organisations;
        }

        //Recupere l'id regle orga envoyée
        public function get idRegleSend():int
        {
            return _idRegleSend;
        }

        public function set idRegleSend(value:int):void
        {
            _idRegleSend = value;
        }

        //Recupere l'id regle orga envoyée
        public function get listRegle():ArrayCollection
        {
            return _listRegle;
        }

        public function set listRegle(value:ArrayCollection):void
        {
            _listRegle = new ArrayCollection();
            _listRegle = value;
        }
        public var RegleListerDAtagrid:ArrayCollection = new ArrayCollection()

        //Recupere les regles actives
        public function get listRegleActive():ArrayCollection
        {
            return _listRegleActive;
        }

        public function set listRegleActive(value:ArrayCollection):void
        {
            _listRegleActive = value;
        }

        //Recupere les regles inactives
        public function get listRegleInactive():ArrayCollection
        {
            return _listRegleInactive;
        }

        public function set listRegleInactive(value:ArrayCollection):void
        {
            _listRegleInactive = value;
        }

/////////FIN PROPRIETES****************************************************************
/////////PUBLIC************************************************************************		
        public function Rules()
        {
        }

        //Acitve une règle (boolactive = 0-> désactivée, boolactive = 1-> activée) <<<<<< N'EST PAS UTILISEE >>>>>>
        public function activeRules(idregle:int, regleNom:String, typeRegle:String, regleComment:String, idSource:int, idCible:int, boolactive:int, suivreAuto:int):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "activeRules", activesRulesResultHandler);
            RemoteObjectUtil.callService(op, idregle, regleNom, typeRegle, regleComment, idSource, idCible, boolactive, suivreAuto);
        }

        //Exécute la règle
        public function executeRules(idregle:int):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "executeRules", executeRulesResultHandler);
            RemoteObjectUtil.callService(op, idregle);
        }

        //Obtient l'ID de l'utilisateur
        public function getUserID():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "getUserID", zreturnUserID);
            RemoteObjectUtil.callService(op);
        }

        private function zreturnUserID(re:ResultEvent):void
        {
            var IDUSER:String = re.result.toString();
        }

        //Supprime la règle mais reste dans la base et elle n'est pas visible
        public function eraseRules(idregle:int):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "eraseRules", eraseRulesResultHandler);
            RemoteObjectUtil.callService(op, idregle);
        }

        //Liste des règles ayant pour source un nœud de l’organisation …annuaire ou 
        //opérateurs spécifiés et aussi liste des règles ayant pour cible une feuille 
        //de l’organisation cliente spécifiée (return array)
        public function listRulesRacine(idSource:int, idCible:int, regleNom:String, bool:Boolean = false):void
        {
            var functionName:String = "getReglesOrga";
            if(bool)
            {
                functionName = "getReglesOrga_v2";
            }
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                functionName, listRulesRacineResultHandler);
            RemoteObjectUtil.callService(op, idSource, idCible, regleNom);
        }

        //Créer une règle, et l’exécuter si spécifié (BOOL_EXECUTE_NOW).
        //On enregistre la date de création et l’utilisateur qui a créé la règle.
        //SI IDREGLE_ORGA>0 est spécifié alors on met à jour la règle. 
        public function IURulesOrganisation(idRegleOrga:int, regleNom:String, typeRegle:String, regleComment:String, idSource:int, idCible:int, active:int,
                                            suivreAuto:int):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "getIURulesOrga", IURulesOrganisationResultHandler);
            RemoteObjectUtil.callService(op, idRegleOrga, regleNom, typeRegle, regleComment, idSource, idCible, active, suivreAuto);
        }

        /*private function zFormatAnnuaire(value:String):String
        {
            if(value != "OPE")
            {
                value = "ANU";
            }
            return value;
        }*/

        //Insérer la liste des exceptions d’une règle
        public function IURulesOrganisationException(idRegleOrga:int, nbarray:int, validOrNot:int, idListException:Array):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "getIURulesOrgaException", IURulesOrganisationExceptionResultHandler);
            RemoteObjectUtil.callService(op, idRegleOrga, nbarray, validOrNot, idListException);
        }

        //Récuperer la liste des exceptions d’une règle
        public function GetRegleException(idRegleOrga:int):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "getRegleException", getRegleExceptionResultHandler);
            RemoteObjectUtil.callService(op, idRegleOrga);
        }

        //Liste de toutes les organisations clientes
        public function ListOraganisationCliente():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "getListOrgaCliente", ListOraganisationClienteResultHandler);
            RemoteObjectUtil.callService(op);
        }

        //Liste de toutes les organisations opérateurs et annuaires
        public function ListOrganisationOperateurAnnuaire():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "getListOrgaOpeAnu", ListOrganisationOperateurAnnuaireResultHandler);
            RemoteObjectUtil.callService(op);
        }

        //Structure de l’organisation spécifiée
        public function structureOrganisation(idOrga:int):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "getStructureOrga", structureOrganisationResultHandler);
            RemoteObjectUtil.callService(op, idOrga);
        }

        //Recupère toutes les organisations ('ANU','OPE','CUS')
        public function fillOrganisation():void
        {
            var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "getOrganisation", fillOrganisationResult);
            RemoteObjectUtil.callService(op, idGroupeMaitre);
        }

        //Obtient le nom et le prenom de la pressonne connectée
        public function getNameAndSurname():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules",
                                                                                "getNameAndSurname", getNameAndSurnameResultHandler);
            RemoteObjectUtil.callService(op);
        }

/////////FIN PUBLIC********************************************************************
/////////PRIVATE***********************************************************************
        //Traitement de l'activation d'une règle (1-> envoyé, -1-> échec)
        private function activesRulesResultHandler(re:ResultEvent):void
        {
            if(re.result != null)
            {
                if(re.result != -1)
                {
                    dispatchEvent(new Event("active"));
                }
                else
                {
                    Alert.show(ResourceManager.getInstance().getString('M22', 'Erreur_d_activation_de_la_r_gle_'), "Consoview");
//				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Erreur_d_activation_de_la_r_gle_'),this);
                }
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M22', 'Erreur_d_activation_de_la_r_gle_'), "Consoview");
//			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Erreur_d_activation_de_la_r_gle_'),this);
            }
        }

        //Traitement du retour de l'exécution de la règle (Nombre de lignes impactées-> envoyer, -1->échec)
        private function executeRulesResultHandler(re:ResultEvent):void
        {
//			if(re.result!=null)
//			{
//				if(re.result!=-1)
//				{ 
//					
//					dispatchEvent(new Event("executed")); 	
//					
//				}else{ Alert.show(ResourceManager.getInstance().getString('M22', 'Erreur_d_ex_cution_de_la_r_gle'),"Consoview"); }
//			}
            dispatchEvent(new Event("executed"));
        }

        //Traitement du retour de la suppression de la règle (1-> envoyé, -1-> échec)
        private function eraseRulesResultHandler(re:ResultEvent):void
        {
            if(re.result != null)
            {
                if(re.result != -1)
                {
                    dispatchEvent(new Event("erase"));
                }
                else
                {
                    Alert.show(ResourceManager.getInstance().getString('M22', 'Erreur_d_effacement_de_la_r_gle_'), "Consoview");
//				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Erreur_d_effacement_de_la_r_gle_'),this);
                }
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M22', 'Erreur_d_effacement_de_la_r_gle_'), "Consoview");
//				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Erreur_d_effacement_de_la_r_gle_'),this);
            }
        }

        //Traitement du retour de la demande de la liste des règles d'une racine (retour : Array)
        private function listRulesRacineResultHandler(re:ResultEvent):void
        {
            if(re.result != null)
            {
                listRegle = new ArrayCollection(null);
                ListRegleRacine = new ArrayCollection(null);
                listRegleActive = new ArrayCollection(null);
                listRegleInactive = new ArrayCollection(null);
                var tempTabRsltQuery:Object = re.result;
                ListRegleRacine = re.result as ArrayCollection;
                for(var i:int = 0; i < tempTabRsltQuery.length; i++)
                {
                    var obje:Regle = new Regle();
                    obje.BOOL_ACTIVE = zFormateIntToBool(tempTabRsltQuery[i].BOOL_ACTIVE);
                    obje.CHAINE_BOOL_ACTIVE = tempTabRsltQuery[i].CHAINE_BOOL_ACTIVE;
                    obje.OPERATEUR = tempTabRsltQuery[i].CHAINE_TYPE_REGLE;
                    obje.BOOL_SUIVRE_AUTO = zFormateIntToBool(tempTabRsltQuery[i].BOOL_SUIVRE_AUTO);
                    obje.CREATE_DATE = zformateDate(tempTabRsltQuery[i].CREATE_DATE).toString();
                    obje.NOM_USER_CREATE = nameConcatSurname(tempTabRsltQuery[i], false).toString().toUpperCase();
                    obje.IDREGLE_ORGA = tempTabRsltQuery[i].IDREGLE_ORGA;
                    obje.LAST_EXECUTED = zformateDate(tempTabRsltQuery[i].LAST_EXECUTED).toString();
                    obje.NOM_USER_MODIF = nullOrNot(tempTabRsltQuery[i]).toString().toUpperCase();
                    obje.MODIF_DATE = zformateDate(tempTabRsltQuery[i].MODIF_DATE).toString();
                    obje.LIBELLE_CIBLE = tempTabRsltQuery[i].NOEUD_CIBLE;
                    obje.LIBELLE_SOURCE = tempTabRsltQuery[i].NOEUD_SOURCE;
                    obje.IDSOURCE = tempTabRsltQuery[i].IDNOEUD_SOURCE;
                    obje.IDCIBLE = tempTabRsltQuery[i].IDNOEUD_CIBLE;
                    obje.ORGA_CIBLE = tempTabRsltQuery[i].ORGA_CIBLE;
                    obje.ORGA_SOURCE = tempTabRsltQuery[i].ORGA_SOURCE;
                    obje.CHEMIN_CIBLE = tempTabRsltQuery[i].CHEMIN_CIBLE;
                    obje.CHEMIN_SOURCE = tempTabRsltQuery[i].CHEMIN_SOURCE;
                    obje.REGLE_COMMENT = tempTabRsltQuery[i].REGLE_COMMENT;
                    obje.REGLE_NOM = tempTabRsltQuery[i].REGLE_NOM;
                    obje.TYPE_REGLE = tempTabRsltQuery[i].TYPE_REGLE;
					obje.LIBELLE_TYPE_REGLE = tempTabRsltQuery[i].CHAINE_TYPE_REGLE;
                    listRegle.addItem(obje);
                    if(obje.BOOL_ACTIVE)
                    {
                        listRegleActive.addItem(obje);
                    }
                    else
                    {
                        listRegleInactive.addItem(obje);
                    }
                }
                dispatchEvent(new Event("listRacine"));
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_de_r_cup_rer_la_liste_des_r_gles_'), "Consoview");
//			ConsoviewAlert.afficherOKImage("Impossible de récupérer la liste des règles.",this);
            }
        }

        private function nameConcatSurname(value:Object, modified:Boolean):String
        {
            var valueModified:Object = new Object();
            var strgName:String = "";
            var nomModif:String = (value.PRENOM_MODIF) ? value.PRENOM_MODIF : " ";
            var prenomModif:String = (value.PRENOM_MODIF) ? value.PRENOM_MODIF : " ";
            var nomCreate:String = (value.NOM_CREATEUR) ? value.NOM_CREATEUR : " ";
            var prenomCreate:String = (value.PRENOM_CREATEUR) ? value.PRENOM_CREATEUR : " ";
            if(!modified)
            {
                strgName = nomCreate + " " + prenomCreate;
            }
            else
            {
                strgName = nomModif + " " + value.prenomModif;
            }
            return strgName;
        }

        private function nullOrNot(obj:Object):String
        {
            var strg:String = "";
            if(obj.NOM_MODIF == null || obj.PRENOM_CREATEUR == null)
            {
                strg = "-";
            }
            else
            {
                strg = nameConcatSurname(obj, true);
            }
            return strg;
        }

        private function zFormateIntToBool(intReceived:int):Boolean
        {
            if(intReceived == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Formate la date en JJ/MM/AAAA
        private function zformateDate(dateToFormat:Object):String
        {
            if(dateToFormat == "")
                return "";
            if(dateToFormat == null)
                return "";
            var realDate:Object = new Object();
            realDate = { date:dateToFormat.date + "/", month:zformateMonth(dateToFormat.month) + "/", fullyear:dateToFormat.fullYear };
            var dayInString:String = realDate.date.toString();
            var monthInString:String = realDate.month.toString();
            var yearInString:String = realDate.fullyear.toString();
            if(dayInString.length == 2)
            {
                dayInString = "0" + dayInString;
            }
            var realDateInString:String = dayInString + monthInString + yearInString;
            realDate = { date:realDateInString };
            return realDateInString;
        }

        //Formate le mois 0->01(janvier), 1->02(février), ..., 11->12(décembre)
        private function zformateMonth(monthToFormate:int):String
        {
            var month:String = null;
            switch(monthToFormate)
            {
                case 0:
                    month = "01";
                    break;
                case 1:
                    month = "02";
                    break;
                case 2:
                    month = "03";
                    break;
                case 3:
                    month = "04";
                    break;
                case 4:
                    month = "05";
                    break;
                case 5:
                    month = "06";
                    break;
                case 6:
                    month = "07";
                    break;
                case 7:
                    month = "08";
                    break;
                case 8:
                    month = "09";
                    break;
                case 9:
                    month = "10";
                    break;
                case 10:
                    month = "11";
                    break;
                case 11:
                    month = "12";
                    break;
            }
            return month;
        }

        //Traitement de l'envoi d'une règle d'une racine source (1-> envoyé, -1-> échec)
        private function IURulesOrganisationResultHandler(re:ResultEvent):void
        {
            if(re.result != null)
            {
                if(re.result != -1)
                {
                    idRegleSend = int(re.result);
                    dispatchEvent(new Event("envoiRegle"));
                }
                else
                {
                    Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_d_envoyer_la_r_gle'), "Consoview");
//				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Erreur_d_envoi_de_la_r_gle_'),this);
                }
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_d_envoyer_la_r_gle'), "Consoview");
//				 ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Erreur_d_envoi_de_la_r_gle_'),this);
            }
        }

        //Traitement de l'insertion des execptions dans la base
        private function IURulesOrganisationExceptionResultHandler(re:ResultEvent):void
        {
            if(re.result != null)
            {
                if(re.result != -1)
                {
                    dispatchEvent(new Event("IUOrgaException"));
                }
                else
                {
                    Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_de_cr_er_la_r_gle_et_de_l_ex_'), "Consoview");
//				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_de_cr_er_la_r_gle_et_de_l_ex_'),this);
                }
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_de_cr_er_la_r_gle_et_de_l_ex_'), "Consoview");
//			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_de_cr_er_la_r_gle_et_de_l_ex_'),this);
            }
        }

        //Traitement de la liste des organisations clientes
        private function ListOraganisationClienteResultHandler(re:ResultEvent):void
        {
            if(re.result != null)
            {
                if(re.result != -1)
                {
                    dispatchEvent(new Event("IUOrgaException"));
                }
                else
                {
                    Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_de_cr_er_la_r_gle_et_de_l_ex_'), "Consoview");
//				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_de_cr_er_la_r_gle_et_de_l_ex_'),this);
                }
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_de_cr_er_la_r_gle_et_de_l_ex_'), "Consoview");
//			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_de_cr_er_la_r_gle_et_de_l_ex_'),this);
            }
        }

        //Traitement de la liste des organisations opérateurs et annuaire
        private function ListOrganisationOperateurAnnuaireResultHandler(re:ResultEvent):void
        {
            if(re.result != null)
            {
                if(re.result != -1)
                {
                    dispatchEvent(new Event("IUOrgaException"));
                }
                else
                {
                    Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_de_r_cup_rer_les_organisation'), "Consoview");
//				ConsoviewAlert.afficherOKImage("Impossible de récupérer les organisations clientes et opérateur.",this);
                }
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_de_r_cup_rer_les_organisation'), "Consoview");
//			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_de_r_cup_rer_les_organisation'),this);
            }
        }

        //Traitement de la création d'une règle et de son exécution
        private function structureOrganisationResultHandler(re:ResultEvent):void
        {
            if(re.result != null)
            {
                if(re.result != -1)
                {
                    dispatchEvent(new Event("IUOrgaException"));
                }
                else
                {
                    Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_de_r_cup_rer_la_structure_cli'), "Consoview");
//				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_de_r_cup_rer_la_structure_cli'),this);
                }
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_de_r_cup_rer_la_structure_cli'), "Consoview");
//			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_de_r_cup_rer_la_structure_cli'),this);
            }
        }

        //Attribut a chaque tableaux correspondant le type d'oraganisation
        private function fillOrganisationResult(re:ResultEvent):void
        {
            if(re.result != null)
            {
                var i:int = 0;
                ListOrgaAnu = new ArrayCollection();
                ListOrgaOpe = new ArrayCollection();
                ListOrgaCliente = new ArrayCollection();
                ListOrgaOpeAnu = new ArrayCollection();
                for(i = 0; i < re.result.length; i++)
                {
                    if(re.result[i].TYPE_ORGA == "ANU" || re.result[i].TYPE_ORGA == "OPE")
                    {
                        ListOrgaOpeAnu.addItem(re.result[i])
                    }
                    switch(re.result[i].TYPE_ORGA)
                    {
                        case "ANU":
                            ListOrgaAnu.addItem(re.result[i]);
                            break;
                        case "OPE":
                            ListOrgaOpe.addItem(re.result[i]);
                            break;
                        case "CUS":
                            ListOrgaCliente.addItem(re.result[i]);
                            break;
                    }
                }
                dispatchEvent(new Event("OrgaFind"));
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_d_obtenir_les_organisation_'), "Consoview")
//			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_d_obtenir_les_organisations_'),this);
            }
        }

        //Obtient le nom et le prenom de la pressonne connectée
        private function getNameAndSurnameResultHandler(re:ResultEvent):void
        {
            if(re.result != null)
            {
                if(re.result != -1)
                {
                    concatNameSurname = re.result.toString();
                    dispatchEvent(new Event("NameAndSurname"));
                }
                else
                {
                    Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_d_obtenir_le_nom_et_le_pr_nom'), "Consoview");
//				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_d_obtenir_le_nom_et_le_pr_nom'),this);
                }
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_d_obtenir_le_nom_et_le_pr_nom'), "Consoview");
//				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_d_obtenir_le_nom_et_le_pr_nom'),this);
            }
        }

        private function getRegleExceptionResultHandler(re:ResultEvent):void
        {
            if(re.result != null)
            {
                if(re.result != -1)
                {
//					var arrayC:ArrayCollection = new ArrayCollection();
//					var arrayC2:ArrayCollection = new ArrayCollection();
//					arrayC = re.result as ArrayCollection;
//					arrayC2.addItem(arrayC[0]);
//					for(var i:int = 1;i < arrayC.length;i++)
//					{
//						arrayC2.addItem(arrayC[i]);
//						
//						for(var j:int = 0;j < arrayC2.length;j++)
//						{
//							if(arrayC[i] != arrayC2[j])
//							{
//								listRegleExceptions.addItem(arrayC2[j]);
//							}
//						}
//					
//					}
//		listRegleExceptions = re.result as ArrayCollection;
//			var arrayCollectionSearch:ArrayCollection = new ArrayCollection();
//			var arrayTemp:ArrayCollection = new ArrayCollection();
//			var arrayTemp2:ArrayCollection = new ArrayCollection();
//			var arrayTemp3:ArrayCollection = new ArrayCollection();			
//			
////			listRegleExceptions = re.result as ArrayCollection;
//			arrayTemp = re.result as ArrayCollection;
//			arrayTemp2  = re.result as ArrayCollection;
//			var temparray:Array = new Array();
//			var tempvarInt:int = 0;
//			
//					for(var i:int = 0;i < arrayTemp.length;i++)
//					{
//						temparray = new Array();
//						tempvarInt = 0;
//						for(var j:int = 0;j < arrayTemp2.length;j++)
//						{
//							
//							if(arrayTemp[i].LIBELLE_GROUPE_CLIENT == arrayTemp2[j].LIBELLE_GROUPE_CLIENT)
//							{ 
//								temparray[tempvarInt] = j;
//								tempvarInt++;
//							}
//							else
//							{
//							
//							}
//						}
//						tempvarInt = 0;						
//						for(var k:int = 1;k-1 < temparray.length;k++)
//						{
//							arrayTemp.removeItemAt(temparray[k] - tempvarInt);
//							tempvarInt = tempvarInt+1;
//						}
//						if(arrayTemp.length!=0)
//						{arrayTemp3.addItem(arrayTemp[0]);}
//						
//					}
                    listRegleExceptions = re.result as ArrayCollection;
                    dispatchEvent(new Event("receivedExceptions"));
                }
                else
                {
                    Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_d_obtenir_les_exceptions_'), "Consoview");
//				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_d_obtenir_les_exceptions_'),this);
                }
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M22', 'Impossible_d_obtenir_les_exceptions_'), "Consoview");
//			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Impossible_d_obtenir_les_exceptions_'),this);
            }
        }
/////////FIN PRIVATE*******************************************************************
    }
}