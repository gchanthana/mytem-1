package univers.parametres.rago
{
	import flash.events.Event;
	
	import mx.containers.HBox;
	import mx.containers.ViewStack;
	import mx.events.FlexEvent;
	
	import univers.parametres.rago.CreationRegles.event.WizardEvent;
	import univers.parametres.rago.CreationRegles.ihm.MainCreateRegleIHM;
	import univers.parametres.rago.inventaire.event.InventaireReglesEvent;
	import univers.parametres.rago.inventaire.ihm.InventaireReglesIHM;

	public class ApplicationRagoImpl extends HBox
	{
		public var myViewStack:ViewStack;
		public var cpMainRegleIhm:MainCreateRegleIHM;
		public var cpInventaireIhm:InventaireReglesIHM;
		
		
		private var _selectedIndex:int;
			
			
		public function set selectedIndex(value:int):void
		{
			_selectedIndex = value;
		}

		public function get selectedIndex():int
		{
			return _selectedIndex;
		}	
			
			
		public function ApplicationRagoImpl()
		{
			//TODO: implement function
			super();
			addEventListener(FlexEvent.APPLICATION_COMPLETE, _localeApplicationCompleteHandler);
			addEventListener("viewNewRegle",_viewMainCreateRegleIHM);
			addEventListener("noViewNewRegle",_noviewMainCreateRegleIHM);
			addEventListener(WizardEvent.ANNULER_CREATION_REGLE,_noviewMainCreateRegleIHM);
			addEventListener(WizardEvent.VALIDER_CREATION_ET_EXECUTER_REGLE,_localValiderCreationEtExecuterRegleHandler);
			addEventListener(WizardEvent.VALIDER_CREATION_REGLE,_localValiderCreationHandler);
			addEventListener("regleCreated",_localDispatchEvent);
		}
		
		protected function creationCompleteHandler(event:FlexEvent):void
		{ }

		protected function _localeApplicationCompleteHandler(event:FlexEvent):void
		{
		
		}
		
		
		
		protected function _viewMainCreateRegleIHM(event:Event):void
		{		
			myViewStack.removeAllChildren();
			try
			{
				cpMainRegleIhm = new MainCreateRegleIHM();
				myViewStack.addChild(cpMainRegleIhm);
				myViewStack.selectedChild = cpMainRegleIhm;
			}
			catch(e:Error)
			{
				myViewStack.selectedChild = cpInventaireIhm;
			}
		}
		
		protected function _localDispatchEvent(event:Event):void
		{
			dispatchEvent(new Event(InventaireReglesEvent.REFRESH_DATAGRID,true));
			if(cpInventaireIhm != null)
			{ cpInventaireIhm.dispatchEvent(new Event(InventaireReglesEvent.REFRESH_DATAGRID,true)); }
			_noviewMainCreateRegleIHM(event);
		}
		
		protected function _noviewMainCreateRegleIHM(event:Event):void
		{
			myViewStack.removeAllChildren();
			try
			{
				cpInventaireIhm = new InventaireReglesIHM();
				myViewStack.addChild(cpInventaireIhm);
				myViewStack.selectedChild = cpInventaireIhm;
				cpInventaireIhm.dispatchEvent(new Event("regleIsModified"));
			}
			catch(e:Error)
			{
				myViewStack.selectedChild = cpInventaireIhm;
			}
		}
		
		protected function _localValiderCreationEtExecuterRegleHandler(event:Event):void
		{
			var bool:Boolean = false;
		}
		
		protected function _localValiderCreationHandler(event:Event):void
		{
			var bool:Boolean = false;
		}
		
	}
}