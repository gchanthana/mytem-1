package univers.parametres.rago.inventaire.ihm
{
	
		import flash.events.Event;
		import flash.events.MouseEvent;
		
		import mx.containers.HBox;
		import mx.controls.Image;
	
	[Bindable]
	public class AjouterSupprimerItemRendererImpl extends HBox
	{
		public var imgCreate:Image;
		public var imgDelete:Image; 

		private var boolData:Boolean = false;
		
		public function AjouterSupprimerItemRendererImpl()
		{
			super();
			addEventListener(MouseEvent.CLICK, _localeClickHandler);
		}
		
		override protected function commitProperties():void
		{	
			if(boolData)
			{
				currentState = ''
			}
			else
			{
				currentState = ''
			}
		}
		
		override public function set data(value:Object):void
		{
			boolData = false;
			
			if(value)
			{
				super.data = value;
				
				if(!data.hasOwnProperty("IDREGELE_ORGA"))
				{
					boolData = false;	
				}
				else
				{
					boolData = true;	
				}		
			}
			
			
			invalidateProperties();
		}

		protected function _localeClickHandler(event:MouseEvent):void
		{
			if(event == null) return;			
		
			switch(event.target.id)
			{
				case "imgDelete":
				{
					dispatchEvent(new Event("eraseException",true));
					break;
				}
				default:
				{
					//do nothing	
				}
			}
		}
		
		private var _createRules:Boolean = true; 
		public function set createRules(value:Boolean):void
		{
			_createRules = value;
		}

		public function get createRules():Boolean
		{
			return _createRules;
		}
		
		private var _deleteRules:Boolean = true; 
		public function set deleteRules(value:Boolean):void
		{
			_deleteRules = value;
		}

		public function get deleteRules():Boolean
		{
			return _deleteRules;
		}
		
		public function invertDelete():Boolean
		{
//			if(createRules)
//			{deleteRules = false;}
//			else{deleteRules = true;}
//			return deleteRules;
			return true;
		}
		
	}
}