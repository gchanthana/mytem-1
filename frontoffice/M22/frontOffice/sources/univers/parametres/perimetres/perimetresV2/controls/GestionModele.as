package univers.parametres.perimetres.perimetresV2.controls
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	
	
	[Event(name="listeTypesPossiblesUpdated")]
	
	
	
	
	[Bindable]
	public class GestionModele extends EventDispatcher
	{
		private var _listeTypesPossibles : ArrayCollection = new ArrayCollection();
		public function get listeTypesPossibles():ArrayCollection{
			return _listeTypesPossibles;
		}
		public function set listeTypesPossibles(liste : ArrayCollection):void{
			_listeTypesPossibles =  liste;
		}
		 
		public function getTypePossible(idNiveauPere : Number):void{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionmodele.GestionModeleGateWay",
																				"getTypePossible",
																				getTypePossibleResultHandler);
			RemoteObjectUtil.callService(op, idNiveauPere);			
		}
		private function getTypePossibleResultHandler(re : ResultEvent):void{
			if (re.result){
				_listeTypesPossibles.source = (re.result as ArrayCollection).source;
			}else{
				_listeTypesPossibles.source = new Array();
			}
			dispatchEvent(new Event("listeTypesPossiblesUpdated"));
		}
		
		
	
		
	}
}