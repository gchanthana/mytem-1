package univers.parametres.perimetres.gestiontechnique
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.lignesetservices.vo.Ligne;

	public class MajWindowImpl extends TitleWindow
	{
		public var cmp : UIComponent;
		public var btFermer : Button;
		

		private var _ligne : Ligne;
		public function get ligne():Ligne{
			return _ligne;
		}
		public function set ligne(st : Ligne):void{
			_ligne = st;
		}
		
		public static const SAVE_CLICKED : String = "modificationEffectue";
		
		
		
		
		public function MajWindowImpl()
		{	
			super();
		}
		
		
		
		
		override protected function commitProperties():void{
			MajCollaborateurView(cmp).ligne = ligne;
			cmp.addEventListener(MajCollaborateurImpl.SAVE_CLICK,saveClickHandler);
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			addEventListener(CloseEvent.CLOSE,closeHandler);
			btFermer.addEventListener(MouseEvent.CLICK,btFermerClickHandler);		
		}
		
		//---- HANDLERS ----------------------------------------------------
		protected function creationCompleteHandler(fe : FlexEvent):void{
				
		}
		
		protected function btFermerClickHandler(me : MouseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		protected function closeHandler(ce : CloseEvent):void{
			PopUpManager.removePopUp(this);	
		}
		
		protected function saveClickHandler(ev : Event):void{
			dispatchEvent(new Event(SAVE_CLICKED))
			PopUpManager.removePopUp(this);
		}
		//---- FIN HANDLERS ------------------------------------------------		
	}
}