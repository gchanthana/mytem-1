package univers.parametres.perimetres
{
	import flash.events.Event;
	
	import mx.events.FlexEvent;
	
	import univers.UniversFunctionItem;
	import univers.parametres.perimetres.mainComponent.refreshTreeEvent;
	
	public class Perimetres extends PerimetresIHM implements UniversFunctionItem {
		public var mainSide:main =  new main();
		public var rightSide:right = new right();		
	
		public function Perimetres()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			rightSide.addEventListener("viewRAGOIHM",afficherFenetreRAGO);
			addEventListener("noVIewRAGO",executeFenetreRAGOretour);
		}
		
		protected function initIHM(event:Event):void
		{		
			PanelMain.addChild(mainSide); 
			PanelRight.addChild(rightSide);
			rightSide.setMainRef(mainSide);
			mainSide.setRightRef(rightSide);
			mainSide.addEventListener("treeRefresh", onTreeRefresh);
		
		}
		
		protected function onTreeRefresh(e:refreshTreeEvent):void
		{
			trace(" Organisation Refresh : " + e.orgaID);
			var ev:refreshTreeEvent = new refreshTreeEvent();
			ev.orgaID = e.orgaID;
			this.dispatchEvent(ev);
		}
		
		public function displayRightSide():void
		{ PanelRight.visible = true; } 
		
		public function onPerimetreChange():void {
			//perimetreComplet.onPerimetreChange();
			try	{
				if (mainSide != null)
					mainSide.onPerimetreChange();
				if (rightSide != null)
					rightSide.onPerimetreChange();
			}
			catch (error:Error) {}
		}
		
		private function afficherFenetreRAGO(event:Event):void
		{ 
			myViewStack.selectedChild = fenetreRAGO; 
			fenetreRAGO.cpInventaireIhm.dispatchEvent(new Event("regleIsModified"));
		}
		
		private function executeFenetreRAGOretour(event:Event):void
		{ 
			myViewStack.selectedChild = vbUsual;
			rightSide.lbView.selectedIndex = 0;
		}
		

	}
}
