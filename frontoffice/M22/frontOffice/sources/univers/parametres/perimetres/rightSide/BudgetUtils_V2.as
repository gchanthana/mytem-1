package univers.parametres.perimetres.rightSide
{
	import composants.util.ConsoviewFormatter;
	
	import flash.events.FocusEvent;
	
	import mx.formatters.DateFormatter;
	
	public class BudgetUtils_V2
	{

		/**************************************
		 *			   CONSTANTES
		 **************************************/
		
		public static const PARENT_INIT_COMPLETED : String = "parent_init_completed";
		
		
		/**************************************
		 *			 FONCTIONS PRIVEES
		 **************************************/
		 
		private var _myBudget_Enfant_V2:BudgetEnfantImpl_V2;

		public function BudgetUtils_V2(instanceMyBudgetEnfant_V2 : BudgetEnfantImpl_V2)
		{
			myBudget_Enfant_V2 = instanceMyBudgetEnfant_V2;
		}
		
		/**************************************
		 *			GETTER / SETTER
		 **************************************/

		public function set myBudget_Enfant_V2(value:BudgetEnfantImpl_V2):void
		{
			_myBudget_Enfant_V2 = value;
		}

		public function get myBudget_Enfant_V2():BudgetEnfantImpl_V2
		{
			return _myBudget_Enfant_V2;
		}
				 		
		public function get modeEcriture():Boolean
		{ 
			//return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)
			return true;
		}		
		
		/**************************************
		 *			FONCTION PUBLIQUE 
		 **************************************/		
		
		public function formatAmount(value:Number):String
		{
			return formatTxtInput(value.toString());
		}
		
		public function formatTxtInput(value:String):String
		{
			return ConsoviewFormatter.formatEuroCurrency(parseInt(value), 0);
		}
				
		public function getDateWithFormat(date: Date):String
		{
			var myDateFormatter : DateFormatter = new DateFormatter();
			myDateFormatter.formatString="YYYY/MM/DD";
			return myDateFormatter.format(date);
		}
		
		// appelé par service getExerciceDataResult
		public function refreshExerciceView():void
		{
			if (myBudget_Enfant_V2.currentExercice >= 0)
			{
				myBudget_Enfant_V2.lbName.text = myBudget_Enfant_V2.resultData[myBudget_Enfant_V2.currentExercice].LIBELLE_EXERCICE;
				myBudget_Enfant_V2.btLeft.enabled = true;
				myBudget_Enfant_V2.btRight.enabled = true;
				myBudget_Enfant_V2.myBudgetService.getNodeBudget();
				myBudget_Enfant_V2.btLeft.enabled = false;
				if (myBudget_Enfant_V2.currentExercice >= myBudget_Enfant_V2.resultData.length -1)
				{
					myBudget_Enfant_V2.btRight.enabled = false;
				}
			}
		}
		
		public function refreshBudgetItems():Boolean
		{
			var activate:Boolean = true;
			
			if (myBudget_Enfant_V2.mainRef.getSelectedNode() < 0 )
			{
				activate = false;
			}

			myBudget_Enfant_V2.tiCurrentbudget.enabled = activate;
			myBudget_Enfant_V2.btTakeRest.enabled = activate;
			myBudget_Enfant_V2.btTakeChild.enabled = activate;
			myBudget_Enfant_V2.btDistribute.enabled = activate;
			myBudget_Enfant_V2.btMAJNode.enabled = activate;
			myBudget_Enfant_V2.btNewExercice.enabled = modeEcriture;
			myBudget_Enfant_V2.btEditExercice.enabled = modeEcriture;
			myBudget_Enfant_V2.fiCurrentNode.label = "-";
			return activate;
		}
		
		public function refreshTxtValues():void
		{
			myBudget_Enfant_V2.tiParentbudget.text = formatAmount(getAroundValue(0, "MONTANTBUDGET"));
			myBudget_Enfant_V2.tiRestbudget.text = formatAmount(getAroundValue(0, "MONTANTBUDGET") 
								- getAroundValue(1, "MONTANTBUDGET")
								- getInputValue(myBudget_Enfant_V2.tiCurrentbudget.text));
			
			myBudget_Enfant_V2.tiChildbudget.text =  formatAmount(getAroundValue(3, "MONTANTBUDGET"));
		}
		
		public function cleanTxtInput(value:String):String
		{
		 	value = value.replace(" ", ""); 	
			return value.replace("€", "");
		}
		
		public function getAroundValue(type:Number, field:String):Number
		{
			try 
			{
				return myBudget_Enfant_V2.nodeData[type][0][field];
			}
			catch (error:Error)	{	}
			return 0;
		}
		
		public function disableFormatTxt(event:FocusEvent):void
		{
			 event.target.text = cleanTxtInput(event.target.text);
		}
		
		public function enableFormatTxt(event:FocusEvent):void
		{
			event.target.text = ConsoviewFormatter.formatEuroCurrency(event.target.text, 0);
		}
		
		/**************************************
		 *			FONCTION PRIVEES 
		 **************************************/
		 	
		private function getInputValue(txt:String):Number
		{
			var res:Number = parseInt(cleanTxtInput(txt));
			if (isNaN(res))
				return 0;
			return res;
		}
		
		
	}
}