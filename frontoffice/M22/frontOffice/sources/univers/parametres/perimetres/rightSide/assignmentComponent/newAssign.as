package univers.parametres.perimetres.rightSide.assignmentComponent
{
	import mx.resources.ResourceManager;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.IndexChangedEvent;
	import mx.managers.PopUpManager;
	
	public class newAssign extends newAssignIHM
	{
		private var _data:ArrayCollection;
		private var _sdaList:ArrayCollection;
		private var _sdaListCopy:ArrayCollection;
		
		//public var _nodeID:Number;
		public var _lineID:Number;
		public var _orgaID:Number;
		public var _currentDate:Date;
		public var _winTitle:String;
		
		public var _newNode:Object;
		
	
		
		public function newAssign()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		public function setDataID( lineID:Number):void
		{
			//_nodeID = nodeID;
			_lineID = lineID;
		}
		
		public function updateWinTitle(value:String):void
		{
			_winTitle = ResourceManager.getInstance().getString('M22', 'Tableau_des_affectations___') + value;
		}
		
		public function setOrgaID(orgaID:Number):void
		{
			_orgaID = orgaID;
		}
				
		protected function initIHM(event:Event):void
		{
			/*manuelAssignView =new manuelAssign();
			autoAssignView =new autoAssign();*/
			
			this.title = _winTitle;
			this.x = 150;
			this.y = 100;
			//psDate.visible = false;
			//PopUpManager.centerPopUp();
			//_currentDate = psDate.getSelectedDate();
			//psDate.setCallBack(periodeChange);
			/* fillGridResult(); */
			
			this.addEventListener(CloseEvent.CLOSE, closeWin);
			btnValid.addEventListener(MouseEvent.CLICK, btnValidClicked);
			btnClose.addEventListener(MouseEvent.CLICK, btnCloseClicked);
			//viewarea.addEventListener(IndexChangedEvent.CHANGE, viewChange);
			cbAuto.addEventListener(Event.CHANGE, modeChanged);
			//periodeChange(new SliderEvent("init"));
		}
		
		protected function modeChanged(event:Event):void
		{
			if (cbAuto.selected == true)
			{
				viewarea.selectedIndex = 1;
				//psDate.visible = true;
			}
			else
			{
				viewarea.selectedIndex = 0;
				//psDate.visible = false;
			}
		}
		
		private function closeWin(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
			
		}
		
		private function viewChange(event:IndexChangedEvent):void
		{
			//periodeChange(new SliderEvent("init"));
		}
		
		protected function btnValidClicked(event:MouseEvent):void
		{
			if (cbAuto.selected == true)
			;
			else
				manuelAssignView.saveData();
		}
		
		protected function btnCloseClicked(event:MouseEvent):void
		{
			closeWin(new CloseEvent("close"));
		}
		
		
		public function activateBt(value:Boolean):void
		{
			btnValid.enabled = value;
		}
		
/*		public function periodeChange(event: SliderEvent):void
		{
			//var d:Date =  psDate.getSelectedDate();
			//_currentDate = psDate.getSelectedDate();
			
			/* txtPeriod.text = df.format(d);
			
			getSdabyDate(df.format(d)); 
			switch (viewarea.selectedIndex)
			{
				case 0:
					manuelAssignView.setDate(_currentDate);
					break;
				case 1:
					autoAssignView.setDate(_currentDate);
					break;
			}	
		}*/
		
		public function addNodeAssign(obj:Object):void
		{
			_newNode = obj;
			//manuelAssignView.addNodeAssign(obj);
			//autoAssignView.addNodeAssign(obj);
		}
		
		public function displayAuto():void
		{
			cbAuto.visible = true;	
			cbAuto.selected = true;
			viewarea.selectedIndex = 1;
		}
	}
}