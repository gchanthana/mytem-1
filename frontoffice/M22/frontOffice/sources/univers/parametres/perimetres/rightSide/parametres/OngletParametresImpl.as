package univers.parametres.perimetres.rightSide.parametres
{
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.perimetres.main;
	
	public class OngletParametresImpl extends OngletParametresIHM
	{
		//public function get modeEcriture ():Boolean{ return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)}	
		
		private var _mode : Boolean = false;			
		public function get modeEcriture ():Boolean{ return _mode}		
		public function set modeEcriture (mode : Boolean):void{_mode = mode}
		
		public var _mainRef:main;
		private var _myOrganisation: ArrayCollection;
		private var _myModele: ArrayCollection;
		
		private var _initCompelte:Boolean = false;
		private var _refSet:Boolean = false;
		
		[Bindable]
		private var _modeleStructure : XML;
		
		public function mainOrganisationChanged():void
		{	 
		}
		
		public function mainTreeSelectionChanged():void
		{
			
		}
		
		public function OngletParametresImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		protected function initIHM(event:Event):void
		{	
			parentDocument.addEventListener("organisationChanged", organisationChanged);  
			_initCompelte = true;
		}
		
		private function organisationChanged(e:Event):void
		{
			var idOrg:Number = _mainRef.getSelectedOrganisation();
			getInfosOrganisation(idOrg);
		}
		
		private function getInfosOrganisation(id:Number):void
		{
			var idOrganisation:Number = id;
				    	
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.parametres.perimetres.OngletOrganisation",
					"getInfosOrganisation", getInfosOrganisationResultHandler);
			RemoteObjectUtil.callService(opData, idOrganisation);
		}
		
		private function getInfosOrganisationResultHandler(re:ResultEvent):void
		{
			if (re.result && (re.result as ArrayCollection).length > 0){
		    	var source:ArrayCollection = re.result as ArrayCollection;
	           	var cursor:IViewCursor = source.createCursor();
	           	myOrganisation = new ArrayCollection();   	
	            while (!cursor.afterLast)
	            {
	            	var currentObj:Object = cursor.current;
	            	myOrganisation.addItem(currentObj);
	            	cursor.moveNext();
	          	}
	          	fillInfosOrganization();  
		  }else{
		  		myOrganisation = new ArrayCollection(); 
		  }        	
		}
		
		private function getInfosModele(id:Number):void
		{
			var idOrgaModele:Number = id;
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.parametres.perimetres.gestionmodele.GestionModeleGateWay",
					"getInfosModele", getInfosModeleResultHandler);
			RemoteObjectUtil.callService(opData, idOrgaModele);			
		}
		
		private function getInfosModeleResultHandler(re:ResultEvent):void
		{
			if (re.result && (re.result as ArrayCollection).length > 0){
		    	var source:ArrayCollection = re.result as ArrayCollection;
	           	var cursor:IViewCursor = source.createCursor();
	           	myModele = new ArrayCollection();   	
	            while (!cursor.afterLast)
	            {
	            	var currentObj:Object = cursor.current;
	            	myModele.addItem(currentObj);
	            	cursor.moveNext();
	          	}
	          	fillInfosModele();
		  	}else{
		  		myModele = new ArrayCollection();
		  	}
		}
		
		private function getModeleStructure(id:Number):void
		{
			var idOrgaModele:Number = id;
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.parametres.perimetres.gestionmodele.GestionModeleGateWay",
					"getModeleStructure", getModeleStructureResultHandler);
			RemoteObjectUtil.callService(opData, idOrgaModele);			
		}
		
		private function getModeleStructureResultHandler(re:ResultEvent):void
		{
			if (re.result && (re.result as ArrayCollection).length > 0)
			{
				_modeleStructure = arrayToXml((re.result as ArrayCollection).source);									
				
				treeModeleStructure.dataProvider = _modeleStructure;
				treeModeleStructure.validateNow();				
				treeModeleStructure.openItems = _modeleStructure.descendants();
				treeModeleStructure.expandItem(_modeleStructure,true);
			}
			else
			{
				_modeleStructure = null;
				treeModeleStructure.dataProvider = null;
			}		
		}
		
		private function arrayToXml(array : Array):XML{
			var rootNode : XML = <node/>;
			rootNode.@TYPE_NIVEAU = array[0].TYPE_NIVEAU;
			rootNode.@NUMERO_NIVEAU = array[0].NUMERO_NIVEAU;
			rootNode.@IDORGA_NIVEAU = array[0].IDORGA_NIVEAU;
			rootNode.@IDORGA_MODELE = array[0].IDORGA_MODELE;
			
			var parentNode: XML = rootNode;
			
			for (var i:Number = 1;i<array.length;i++){
				var node : XML = <node/>;				
				node.@TYPE_NIVEAU = array[i].TYPE_NIVEAU;
				node.@NUMERO_NIVEAU = array[i].NUMERO_NIVEAU;
				node.@IDORGA_NIVEAU = array[i].IDORGA_NIVEAU;
				node.@IDORGA_MODELE = array[i].IDORGA_MODELE;
				parentNode.appendChild(node);
				parentNode = node;
			}
			
			return rootNode;
			
		   /*var monXML:XML= new XML(<root></root>);
		   for each (var organe:Object in array) 
		   {
		        var organeXML:XML = new XML(<organe></organe>);
		        for (var prop:String in organe) 
		        {
		             organeXML.appendChild("<"+prop+">"+organe[prop]+"</"+prop+">");
		        }
		        monXML.appendChild(organeXML);
		   }         
		   return monXML;*/

		}
		
		private function fillInfosModele():void
		{
			modLibelle.text = myModele[0].LIBELLE_MODELE;
			modComments.text = myModele[0].COMMENTAIRES;
		}
		

		
		private function fillInfosOrganization():void
		{
			clearInfoOrganization();
			orgLibelle.text = myOrganisation[0].LIBELLE_GROUPE_CLIENT;
			orgType.text = myOrganisation[0].TYPE_ORGA;
			orgNbrLigneaffectees.text = myOrganisation[0].NB_LIGNES;
			orgCreatedat.text = df.format(myOrganisation[0].DATE_CREATION);
			orgUpdatedat.text = df.format(myOrganisation[0].DATE_MODIF);
			orgComments.text = myOrganisation[0].COMMENTAIRES;
			getInfosModele(myOrganisation[0].IDORGA_MODELE);
			getModeleStructure(myOrganisation[0].IDORGA_MODELE);
		}
		
		private function clearInfoOrganization():void
		{
			orgLibelle.text = "";
			orgType.text = "";
			orgNbrLigneaffectees.text = "";
			orgCreatedat.text = "";
			orgUpdatedat.text = "";
			orgComments.text = "";
			
			modLibelle.text = "";
			modComments.text = "";
		}
		
		
		//--------------- setter getter
		override protected function commitProperties():void
		{
			if(_initCompelte)
			{
				if(_refSet)
				{
					var idOrg:Number = _mainRef.getSelectedOrganisation();			
					getInfosOrganisation(idOrg);
				}				
					
			}
		}
		
		public function setMainRef(ref:main):void
		{
			_mainRef = ref;
			
			if(_mainRef != null)
			{
				_refSet = true;	
			}
			else
			{
			 	_refSet = false;
			}
			
			invalidateProperties();
		}
		

	    public function get myOrganisation(): ArrayCollection
	    {
	    	return _myOrganisation;
	    }
	    public function set myOrganisation(org:ArrayCollection): void
	    {
	    	_myOrganisation = org;
	    }
	    
	    public function get myModele(): ArrayCollection
	    {
	    	return _myModele;
	    }
	    public function set myModele(mod:ArrayCollection): void
	    {
	    	_myModele = mod;
	    }	    
		
		
	}
}