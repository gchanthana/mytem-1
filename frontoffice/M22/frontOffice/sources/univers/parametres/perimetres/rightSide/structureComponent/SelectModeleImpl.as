package univers.parametres.perimetres.rightSide.structureComponent
{
    import mx.resources.ResourceManager;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    import mx.collections.ArrayCollection;
    import mx.containers.VBox;
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.ComboBox;
    import mx.controls.Label;
    import mx.controls.Tree;
    import mx.events.CloseEvent;
    import mx.events.ListEvent;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;
    import mx.utils.ObjectUtil;

    [Event(name="ModeleExistantAnnuler")]
    [Event(name="ModeleExistantValider")]
    [Bindable]
    public class SelectModeleImpl extends VBox
    {
        public var treeModeleStructure:Tree;
        public var cmbModele:ComboBox;
        public var btValider:Button;
        public var btAnnuler:Button;
        public var lblError:Label;
        public var btSupprimerModele:Button;
        private var _listeModele:ArrayCollection;
        private var _modeleStructure:XML;
        private var _modele:Object = new Object();

        public function get nouveauModele():Object
        {
            return _modele;
        }

        public function SelectModeleImpl()
        {
            super();
        }

        override protected function commitProperties():void
        {
            super.commitProperties();
            btSupprimerModele.addEventListener(MouseEvent.CLICK, btSupprimerModeleClickHandler);
            btAnnuler.addEventListener(MouseEvent.CLICK, btAnnulerClickHandler);
            btValider.addEventListener(MouseEvent.CLICK, btValiderClickHandler);
            cmbModele.addEventListener(ListEvent.CHANGE, cmbModeleChangeHandler);
            treeModeleStructure.dataProvider = _modeleStructure;
            treeModeleStructure.labelField = "@TYPE_NIVEAU";
            cmbModele.dataProvider = _listeModele;
            cmbModele.labelField = "LIBELLE_MODELE";
        }

        //================== HANDLERS =====================================
        protected function btAnnulerClickHandler(me:MouseEvent):void
        {
            dispatchEvent(new Event("ModeleExistantAnnuler"));
        }

        protected function btValiderClickHandler(me:MouseEvent):void
        {
            if(cmbModele.selectedItem != null)
            {
                _modele = _modeleStructure;
                dispatchEvent(new Event("ModeleExistantValider"));
            }
        }

        protected function cmbModeleChangeHandler(le:ListEvent):void
        {
            if(cmbModele.selectedItem != null)
            {
                getModeleStructure(cmbModele.selectedItem.IDORGA_MODELE);
            }
            else
            {
                treeModeleStructure.dataProvider = null;
            }
        }

        protected function btSupprimerModeleClickHandler(me:MouseEvent):void
        {
            var closeFunction:Function = function traitement(ce:CloseEvent):void
                {
                    if(ce.detail == Alert.OK)
                    {
                        supprimerModele(Number(cmbModele.selectedItem.IDORGA_MODELE));
                    }
                }
            if(cmbModele.selectedItem != null)
            {
                Alert.okLabel = resourceManager.getString('M22', 'Oui');
                Alert.cancelLabel = resourceManager.getString('M22', 'Annuler');
                Alert.show(resourceManager.getString('M22', 'Supprimer_le_Mod_le_'), resourceManager.getString('M22', 'Attention'), Alert.OK | Alert.CANCEL,
                           this, closeFunction);
            }
        }

        //============ FIN = HANDLERS ===================================
        public function getListeModelesExistant():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionmodele.GestionModeleGateWay",
                                                                                "getListeModelesExistant", getListeModelesExistantResultHandler);
            RemoteObjectUtil.callService(op);
        }

        //============ PRIVATE METHODE ==================================		
        private function getListeModelesExistantResultHandler(re:ResultEvent):void
        {
            lblError.text = "";
            if(re.result)
            {
                _listeModele = re.result as ArrayCollection;
                _listeModele.refresh();
                cmbModele.dataProvider = _listeModele;
                cmbModele.dispatchEvent(new ListEvent(ListEvent.CHANGE));
            }
            else
            {
                _listeModele = new ArrayCollection();
                _listeModele.refresh();
                cmbModele.dispatchEvent(new ListEvent(ListEvent.CHANGE));
                lblError.text = ResourceManager.getInstance().getString('M22', 'Pas_de_mod_le');
            }
        }

        private function getModeleStructure(idModele:Number):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionmodele.GestionModeleGateWay",
                                                                                "getModeleStructure", getModeleStructureResultHandler);
            RemoteObjectUtil.callService(op, idModele);
        }

        private function getModeleStructureResultHandler(re:ResultEvent):void
        {
            lblError.text = "";
            if(re.result && (re.result as ArrayCollection).length > 0)
            {
                _modeleStructure = arrayToXml((re.result as ArrayCollection).source);
                treeModeleStructure.dataProvider = _modeleStructure;
                treeModeleStructure.openItems = _modeleStructure.descendants();
                treeModeleStructure.expandItem(_modeleStructure, true);
            }
            else
            {
                _modeleStructure = null;
            }
        }

        private function arrayToXml(array:Array):XML
        {
            var rootNode:XML = <node/>
            rootNode.@TYPE_NIVEAU = array[0].TYPE_NIVEAU;
            rootNode.@NUMERO_NIVEAU = array[0].NUMERO_NIVEAU;
            rootNode.@IDORGA_NIVEAU = array[0].IDORGA_NIVEAU;
            rootNode.@IDORGA_MODELE = array[0].IDORGA_MODELE;
            var parentNode:XML = rootNode;
            for(var i:Number = 1; i < array.length; i++)
            {
                var node:XML = <node/>;
                node.@TYPE_NIVEAU = array[i].TYPE_NIVEAU;
                node.@NUMERO_NIVEAU = array[i].NUMERO_NIVEAU;
                node.@IDORGA_NIVEAU = array[i].IDORGA_NIVEAU;
                node.@IDORGA_MODELE = array[i].IDORGA_MODELE;
                parentNode.appendChild(node);
                parentNode = node;
            }
            trace(ObjectUtil.toString(rootNode));
            return rootNode;
        }

        private function supprimerModele(idOrgaModele:Number):void
        {
            lblError.text = "";
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionmodele.GestionModeleGateWay",
                                                                                "supprimerModele", supprimerModeleResultHandler);
            RemoteObjectUtil.callService(op, idOrgaModele);
        }

        private function supprimerModeleResultHandler(re:ResultEvent):void
        {
            if(re.result > 0)
            {
                getListeModelesExistant();
            }
            else if(re.result == -1)
            {
                lblError.text = ResourceManager.getInstance().getString('M22', 'Une_Organisation_est_associ_e_au_mod_le');
            }
            else
            {
                lblError.text = ResourceManager.getInstance().getString('M22', 'Error');
            }
        }
        //=================================================================
    }
}