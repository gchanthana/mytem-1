package univers.parametres.perimetres.rightSide.assignmentComponent
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.perimetres.rightSide.assignment;
	
	public class newAssignSimple extends newAssignSimpleIHM
	{
		private var _parentRef:assignment;
		private var _mainID:Number;
		private var _idListLine:String;
		
		public function newAssignSimple()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		public function setParentRef(ref:assignment):void
		{
			_parentRef = ref;	
		}
		
		protected function initIHM(event:Event):void
		{
			this.addEventListener(CloseEvent.CLOSE, closeWin);
			PopUpManager.centerPopUp(this);
			btnValid.addEventListener(MouseEvent.CLICK, onbtnValidClicked);
			btnCancel.addEventListener(MouseEvent.CLICK, onbtnCancelClicked);
			
		}
		
		private function closeWin(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function onbtnValidClicked(event:MouseEvent):void
		{
			var month:String = dateMC.text.substr(0, 2);
			var year:String = dateMC.text.substr(3, 4);
			
			var tmpDate:Date = new Date(year, parseInt(month));
			
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
																				"simpleAssign",
																				simpleAssignResult, throwError);
			RemoteObjectUtil.callService(op, _mainID, _idListLine, dateF.format(tmpDate));	
		}
		
		private function simpleAssignResult(event:ResultEvent):void
		{
			_parentRef.assignLineResult(new ResultEvent("refresh"));
			closeWin(new CloseEvent("close"));
		}
		
		protected function onbtnCancelClicked(event:MouseEvent):void
		{
			closeWin(new CloseEvent("close"));
		}
		
		public function setDataID(mainID:Number, idListLine:String):void
		{
			_mainID = mainID;
			_idListLine = idListLine;	
		}
		
		private function throwError(result:FaultEvent):void
		{
			
		}	
		
		
		
	}
}