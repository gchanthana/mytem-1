package univers.parametres.perimetres.rightSide.destinataireComponent
{
	import flash.events.Event;

	public class createProfilEvent extends Event
	{
		public var libelle:String;
		public var codeInterne:String;
		public var commentaire:String;
		
		public function createProfilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}