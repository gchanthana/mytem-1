package univers.parametres.perimetres.rightSide.destinataireComponent
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	 
	public class createProfil extends createProfilIHM
	{
		public function get modeEcriture():Boolean{return true}		
		public function createProfil()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		protected function initIHM(e:Event):void
		{
			addEventListener(CloseEvent.CLOSE, closeWindow);
			btValid.visible = modeEcriture;
			txtCodeInterne.editable = modeEcriture;
			txtCommentaire.editable = modeEcriture;
			txtLibelle.editable = modeEcriture;
			btValid.addEventListener(MouseEvent.CLICK, btValidClicked);
			btAnnuler.addEventListener(MouseEvent.CLICK, closeWindow);
		}
		
		protected function btValidClicked(e:MouseEvent):void
		{
			var ev:createProfilEvent = new createProfilEvent("addItem");
			ev.libelle = txtLibelle.text;
			ev.codeInterne = txtCodeInterne.text;
			ev.commentaire = txtCommentaire.text;
			dispatchEvent(ev);
			
			closeWindow(new Event(""));
		}
		
		
		protected function closeWindow(e:Event):void
		{
			PopUpManager.removePopUp(this);	
		}
	}
}