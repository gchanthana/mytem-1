package univers.parametres.perimetres.mainComponent
{
	import flash.events.Event;
	
	public class refreshTreeEvent extends Event
	{
		public var orgaID:Number;
		
		public function refreshTreeEvent():void
		{
			super("treeRefresh");
		}
	}
}