package univers.parametres.perimetres.mainComponent
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class WinAssignLines extends WinAssignLinesIHM
	{
		public var _ref:Object;
		
		public function WinAssignLines()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		public function setParentRef(ref:Object):void
		{
			_ref = ref	
		}
		
		public function setTexte(txt:String):void
		{
			txtMessage.text = txt;
		}
		
		public function initIHM(event:Event):void
		{
			PopUpManager.centerPopUp(this);
			btAccept.addEventListener(MouseEvent.CLICK, acceptRequest);
			btRefus.addEventListener(MouseEvent.CLICK, resfusRequest);
		}
		
		public function closeWin():void
		{
			PopUpManager.removePopUp(this);
		}
		
		public function resfusRequest(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		public function acceptRequest(event: MouseEvent):void
		{
			//_ref.acceptAddNodeWithLines();
			dispatchEvent(new Event("acceptRequest"));
			PopUpManager.removePopUp(this);
		}
		
	}
}