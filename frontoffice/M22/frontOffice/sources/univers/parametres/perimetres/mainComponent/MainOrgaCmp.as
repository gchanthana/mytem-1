package univers.parametres.perimetres.mainComponent
{
    import composants.parametres.perimetres.OrgaStructureMain;
    
    import flash.events.Event;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;
    
    import univers.parametres.perimetres.main;

    public class MainOrgaCmp extends OrgaStructureMain
    {
        public function MainOrgaCmp()
        {
        }
        private var privateNodeName:String = "-";
        private var _treeEditedItemNodeId:Number;

        public function get treeEditedItemNodeId():Number
        {
            return _treeEditedItemNodeId;
        }

        public function set treeEditedItemNodeId(idEditedNode:Number):void
        {
            _treeEditedItemNodeId = idEditedNode;
        }

        override public function searchNode(nodeName:String):void
        {
            /** Todo
             * Change the request
             * */
            parentDocument.clearGridLines();
            var selectedNodeID:Number = treePerimetre.selectedItem.@NID;
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "searchNode", searchNodeResult);
            privateNodeName = nodeName;
            RemoteObjectUtil.callService(op, selectedNodeID, nodeName, 0, (parentDocument as main).getSelectedDate());
        }

        private function searchNodeResult(event:ResultEvent):void
        {
            /** ToDo
             * Get the result of the search : ok
             * */
            if ((event.result != null) && ((event.result as ArrayCollection).length > 0))
            {
                setDataResult((event.result as ArrayCollection).source);
            }
            else
            {
                var message:String = (privateNodeName != null) ? privateNodeName : "-";
                Alert.show(message, "Pas de résultat pour :");
            }
        }

        private function expandFirstElement():void
        {
            //treePerimetre.expandItem(_treeNodes[0], true);
            treePerimetre.openItems();
        }

        /** Set the tree selected item editable */
        public function setSelectedTreeItemEditable(status:Boolean):void
        {
            treePerimetre.editable = status;
            treeEditedItemNodeId = -1;
            if (status)
            {
                treeEditedItemNodeId = Number(treePerimetre.selectedItem.@NID);
                treePerimetre.validateNow();
                treePerimetre.selectedItem = treePerimetre.selectedItems[0]; //pour recalculer la position du noeud selectionné
                treePerimetre.editedItemPosition = { columnIndex: 0, rowIndex: treePerimetre.selectedIndex };
            }
        }

        /** Add node to nodeParent in the tree */
        public function addItemToTree(node:Object):void
        {
            var newNode:XML = <node/>;
            newNode.@NID = node.NID;
            newNode.@LBL = node.LBL;
            newNode.@IDORGA_NIVEAU = node.IDORGA_NIVEAU;
            newNode.@NUMERO_NIVEAU = node.NUMERO_NIVEAU;
            newNode.@NTY = 0;
            newNode.@STC = 1;
            var dept:XMLList;
            if (_treeNodes.@NID != node.parentID)
                dept = _treeNodes.descendants().(@NID == node.parentID);
            else
                dept = _treeNodes.(@NID == node.parentID);
            XML(dept[0]).prependChild(newNode);
            var pr:Object = dept[0];
            pr.@NTY = 1;
            treePerimetre.selectedItem = newNode;
            //treePerimetre.addEventListener(Event.RENDER,renderTreePerimetre);
        }

        private function renderTreePerimetre(ev:Event):void
        {
            treePerimetre.invalidateList();
            treePerimetre.validateNow();
            treePerimetre.removeEventListener(Event.RENDER, renderTreePerimetre);
        }

        public function addNewOrganisation(item:Object):void
        {
            var newNode:XML = <NODE/>;
            newNode.@LBL = item.LIBELLE_GROUPE_CLIENT;
            newNode.@NID = item.IDGROUPE_CLIENT;
            newNode.@NTY = item.NTY;
            newNode.@STC = item.STC;
            var dataset:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;
            var parent:XMLList = dataset.children().(@NID == -3);
            XML(parent).appendChild(newNode);
        }

        /* Update the value of the last created node */
        public function updateNewItemValue(value:Number):void
        {
            var dept:XMLList = _treeNodes.descendants().(@NID == "-1");
            dept[0].@NID = value;
        }

        public function deleteTreeItem(id:Number):void
        {
            var dept:XMLList = _treeNodes.descendants().(@NID == id);
            if (dept != null)
            {
                var pr:Object = dept[0];
                var item:XML = pr.parent();
                if (item.descendants().length() <= 1)
                    item.@NTY = 0;
                delete dept[0];
            }
            treePerimetre.addEventListener(Event.RENDER, renderTreePerimetre);
        }
    }
}