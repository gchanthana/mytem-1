package univers.parametres.perimetres.mainComponent
{
	import flash.events.Event;
	
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class ArbreEmplacement extends ArbreEmplacementIHM
	{
		public var _idsitephysique:String;
		
		public function ArbreEmplacement()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
	
		protected function initIHM(event:Event):void
		{
			myArbreEmplacement.getEmplacement(_idsitephysique);
			this.addEventListener(CloseEvent.CLOSE, closeWin);
			myArbreEmplacement.myEmp.myTree.addEventListener(Event.CHANGE, empChanged);	
		}
		
		protected function empChanged(e:Event):void
		{
			var newe:Event = new Event(Event.CHANGE);
			dispatchEvent(newe);
		}
		
		
		protected function closeWin(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
	}
}