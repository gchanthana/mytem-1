package composants.parametres.perimetres
{
    import composants.access.perimetre.PerimetreTreeDataDescriptor;
    import composants.access.perimetre.PerimetreTreeItemRenderer_V2;
    import composants.parametres.perimetres.tree.FilteredTreeDataDescriptor;
    import composants.util.ConsoviewUtil;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.net.URLRequest;
    import flash.net.URLVariables;
    import flash.net.navigateToURL;
    import mx.collections.ArrayCollection;
    import mx.collections.ICollectionView;
    import mx.collections.XMLListCollection;
    import mx.controls.Alert;
    import mx.controls.Menu;
    import mx.controls.Tree;
    import mx.core.ClassFactory;
    import mx.core.IFactory;
    import mx.events.DynamicEvent;
    import mx.events.FlexEvent;
    import mx.events.ItemClickEvent;
    import mx.events.MenuEvent;
    import univers.parametres.perimetres.search.SpecialSearch;

    public class OrgaStructureMain extends OrgaStructureMainIHM
    {
        private var _dataOraganisation:ArrayCollection;
        private var _refreshTreeFunc:Function;
        private var _searchNodeFunc:Function;
        private var _searchResult:Array;
        private var _lastSelectedItem:Number;
        private var _filterMenu:Menu;
        private var menuFilterData:XML;
        public var _treeNodes:XMLList;
        public var _searchExecQuery:Boolean;
        public var _searchItemIndex:Number;
        public var _contextList:ArrayCollection;
        public var _searchTreeResult:XMLList;
        public var _parentRef:Object;
        private var perimetreTreeItemRenderer:IFactory;
        private var modeActif:String;
        public static const MODE_ACTIF:String = "actif";
        public static const MODE_ALL:String = "all";
        public static const MODE_INACTIF:String = "inactif";
        private var _modeRestreint:Boolean = (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE.toUpperCase() == "GROUPELIGNE");

        // private var filterFunc:Function = new Function();
        public function OrgaStructureMain()
        {
            this._searchExecQuery = true;
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
            perimetreTreeItemRenderer = new ClassFactory(composants.access.perimetre.PerimetreTreeItemRenderer_V2);
        }

        public function initIHM(event:Event):void
        {
            btnValiderRecherche.addEventListener(MouseEvent.CLICK, onbtValiderRechercheClicked);
            txtRechercheOrga.addEventListener(Event.CHANGE, onRechercheOrgaChange);
            cmbOrganisation.addEventListener(Event.CHANGE, onOrganisationChange);
            treePerimetre.addEventListener(Event.CHANGE, onTreeSelectionChange);
            btnFilterCmb.visible = (_modeRestreint == false);
            btnFilterCmb.enabled = btnFilterCmb.visible;
            txtRechercheOrga.addEventListener(FlexEvent.ENTER, onbtValiderRechercheClicked);
            // ecouteur sur les radiobuttons de types d'orga
            rdBtnGrp.addEventListener(ItemClickEvent.ITEM_CLICK, rdBtnGrpChangeHandler);
            treePerimetre.dataDescriptor = new FilteredTreeDataDescriptor(fitrerByStatut);
            modeActif = MODE_ALL;
            // gestion de l'export
            btnExportOrga.addEventListener(MouseEvent.CLICK, onBtExportClickedHandler);
            // btnExportLignes.addEventListener(MouseEvent.CLICK, onBtExportLignesClickedHandler);
        }

        /***
         *
         *  Export de l'arborescence en fonction du radiobutton activé
         *
         *
         * */
        private function onBtExportClickedHandler(evt:MouseEvent):void
        {
            exportOrgaCSV(evt);
        }

        /***
         *
         *  Export des lignes associées à l'organisation sélectionnées
         *
         *
         * */
        private function onBtExportLignesClickedHandler(evt:MouseEvent):void
        {
            exportOrgaStCSV(evt);
        }

        /***
         *  Export des organisations selon 3 modes possibles
         *  ( actif/inactif/tous )
         *
         * */
        private function exportOrgaCSV(event:MouseEvent):void
        {
            if(cmbOrganisation.selectedIndex != -1)
            {
                var url:String = modulePerimetreIHM.urlBackoffice + "/fr/consotel/consoview/cfm/tools/exportOrgaCSVmode.cfm";
                var variables:URLVariables = new URLVariables();
                // variables.id_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
                // variables.libelle_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
                // par défaut renvoie de toutes les orga
                var mode:int = -1;
                var modeLibel:String = "";
                if(modeActif == MODE_ACTIF)
                {
                    mode = 1;
                    modeLibel = "_Actifs";
                }
                if(modeActif == MODE_INACTIF)
                {
                    mode = 0;
                    modeLibel = "_Inactifs";
                }
                variables.mode = mode;
                ///Gestion du perimètre
                if(CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_LOGIQUE == ConsoViewPerimetreObject.TYPE_ROOT)
                {
                    if(cmbOrganisation.selectedItem.hasOwnProperty("IDGROUPE_CLIENT") && cmbOrganisation.selectedItem.IDGROUPE_CLIENT != null)
                    {
                        variables.id_perimetre = cmbOrganisation.selectedItem.IDGROUPE_CLIENT
                        variables.libelle_perimetre = cmbOrganisation.selectedItem.LIBELLE_GROUPE_CLIENT + modeLibel
                    }
                }
                else
                {
                    variables.id_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
                    variables.libelle_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE + modeLibel;
                }
                var request:URLRequest = new URLRequest(url);
                request.data = variables;
                request.method = "POST";
                navigateToURL(request, "_blank");
            }
        }

        private function exportOrgaStCSV(event:MouseEvent):void
        {
            var url:String = modulePerimetreIHM.urlBackoffice + "/fr/consotel/consoview/cfm/tools/exportOrgaStCSV.cfm";
            var variables:URLVariables = new URLVariables();
            /*             if (cmbOrganisation.selectedItem != -1)
               {
               if (cmbOrganisation.selectedItem.hasOwnProperty("IDGROUPE_CLIENT") && cmbOrganisation.selectedItem.IDGROUPE_CLIENT != null)
               {
               variables.id_perimetre = cmbOrganisation.selectedItem.IDGROUPE_CLIENT;
               variables.libelle_perimetre = cmbOrganisation.selectedItem.LIBELLE_GROUPE_CLIENT;
               }
             } */
            if(CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_LOGIQUE == ConsoViewPerimetreObject.TYPE_ROOT)
            {
                if(cmbOrganisation.selectedItem.hasOwnProperty("IDGROUPE_CLIENT") && cmbOrganisation.selectedItem.IDGROUPE_CLIENT != null)
                {
                    variables.id_perimetre = cmbOrganisation.selectedItem.IDGROUPE_CLIENT
                    variables.libelle_perimetre = cmbOrganisation.selectedItem.LIBELLE_GROUPE_CLIENT
                }
            }
            else
            {
                variables.id_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
                variables.libelle_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
            }
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method = "POST";
            navigateToURL(request, "_blank");
        }

        /** ######################### EVENTS #########################*/
        protected function onTreeSelectionChange(event:Event):void
        {
            parentDocument.treeSelectionChanged();
        }

        /** Refresh the data of the tree */
        protected function onOrganisationChange(e:Event):void
        {
            treePerimetre.dataProvider = null;
            treePerimetre.selectedItem = null;
            onRechercheOrgaChange(new Event("refresh"));
            if(_parentRef != null)
                _parentRef.organisationChanged();
            refreshTree();
        }

        /** Send a request to serverside with the selected Node Value and The input value */
        protected function onbtValiderRechercheClicked(result:Event):void
        {
            if(_searchExecQuery)
            {
                var name_groupe:String = txtRechercheOrga.text;
                searchNode(name_groupe);
            }
            else if(_searchResult)
            {
                selectNextNode();
            }
        }

        /**
         * Capte de le changement des radiobuttons ( tous/inactif/actifs )
         * pour générer la liste des organisations
         *
         * */
        protected function rdBtnGrpChangeHandler(evt:ItemClickEvent):void
        {
            // clearSearch();
            // refreshTree(null);
            switch(evt.currentTarget.selectedValue)
            {
                case "ALL":
                    btnExportOrga.toolTip = resourceManager.getString('M22', 'Exporter_l_organisation_de_tous_les_n__uds_en_CSV');
                    modeActif = MODE_ALL;
                    break;
                case "ACTIF":
                    btnExportOrga.toolTip = resourceManager.getString('M22', 'Exporter_l_organisation_des_n__uds_actifs_en_CSV');
                    modeActif = MODE_ACTIF;
                    break;
                case "INACTIF":
                    btnExportOrga.toolTip = resourceManager.getString('M22', 'Exporter_l_organisation_des_n__uds_inactifs_en_CSV');
                    modeActif = MODE_INACTIF;
                    break;
                default:
                    modeActif = MODE_ALL;
                    break;
            }
            //refreshTree(null);
        }

        /**
         *  Filtre  l'arbre en fonction du statut de l'item
         *  Tous/actif/inactifs
         *
         * */
        public function fitrerByStatut(item:Object):ICollectionView
        {
            var mode:String;
            var xmlListCollex:XMLListCollection;
            var xml:XML = item as XML;
            xmlListCollex = new XMLListCollection(new XMLList(xml.children()));
            // Cas : on affiche les actifs
            if(xml && modeActif == MODE_ACTIF)
            {
                if(xml.node.IS_ACTIVE)
                {
                    xmlListCollex = new XMLListCollection(xml.children().(@["IS_ACTIVE"] == "1" || (hasOwnProperty("HAS_ACTIVE") && @["HAS_ACTIVE"] ==
                                                          "1")));
                }
            }
            // Cas : on affiche les inactifs
            else if(modeActif == MODE_INACTIF)
            {
                if(xml.node.IS_ACTIVE)
                {
                    xmlListCollex.filterFunction = filterByInactif;
                    xmlListCollex.refresh();
                }
            }
            return xmlListCollex;
        }

        /**
         *  test si le noeud passé en paramètre est inactif
         *  les dossiers même actifs sont affichés pour
         *  respecter l'arborescence
         *
         * */
        private function filterByInactif(node:Object):Boolean
        {
            var xml:XML = new XML();
            var visible:Boolean = true;
            // si c'est un dossier
            if((node.@NTY && node.@NTY == "1" && node.@HAS_INACTIVE == "1") || (node.@IS_ACTIVE == "0"))
            {
                visible = true;
                    // treePerimetre.expandChildrenOf(node, true);
            }
            else
            {
                visible = false;
            }
            return visible;
        }

        /** ####################### END EVENTS #######################*/ /** used to select the last Select item before refreshing the tree */
        private function renderDone(event:Event):void
        {
            if(_lastSelectedItem > 0)
            {
                selectNodeByAttribute("NID", _lastSelectedItem.toString());
                _lastSelectedItem = -1;
            }
        }

        /** ##################### PUBLIC METHODS #####################*/
        public function getTreeRef():Tree
        {
            return treePerimetre;
        }

        public function setParentRef(ref:Object):void
        {
            _parentRef = ref;
        }

        /** used to find nodes by sending request */
        public function searchNode(nodeName:String):void
        {
        }

        /** used to get the nodes of the tree by sending request */ /*public function refreshTree():void { }*/
        public function setContextMenu(data:ArrayCollection):void
        {
            treePerimetre.setContextMenu(data);
        }

        /** Set an array corresponding to the result of the search */
        public function setDataResult(data:Array):void
        {
            _searchResult = data;
            _searchItemIndex = 0;
            if(_searchResult.length > 0)
                selectNextNode();
            _searchExecQuery = false;
        }

        /** Select the next found node */
        public function selectNextNode():void
        {
            if(_searchItemIndex >= _searchResult.length)
                return;
            selectNodeByAttribute("NID", _searchResult[_searchItemIndex].IDGROUPE_CLIENT);
            //trace(txtRechercheOrga.text + " -- " + getSelectedItem().@LABEL);
            _searchItemIndex++;
            if(_searchItemIndex >= _searchResult.length)
                _searchItemIndex = 0;
            try
            {
                if(getSelectedItem().@LABEL.search(txtRechercheOrga.text) <= -1)
                    parentDocument.onbtAfficherLignesClicked(new Event(""));
            }
            catch(error:Error)
            {
            }
        }

        /** Select an item in the tree by the value of his attribute */
        public function selectNodeByAttribute(attribute:String, value:String):void
        {
            try
            {
                var dept:XMLList;
                dept = _treeNodes.descendants().(@[attribute] == value);
                //Si le noeud recherche n'est pas encore chargé alors on fait un recherche spéciale		
                if(dept[0] == null && _searchResult)
                {
                    var search:SpecialSearch;
                    search = new SpecialSearch(this);
                    treePerimetre.addEventListener(FlexEvent.UPDATE_COMPLETE, treePerimetreUpdateCompleteHandler);
                    search.initSearch(treePerimetre, Number(value));
                    return;
                }
                //recherche dans les noeuds déjà en mémoire
                var pr:Object = dept[0];
                while((pr = pr.parent()) != null)
                    treePerimetre.expandItem(pr, true);
                treePerimetre.selectedItem = dept[0];
                treePerimetre.scrollToIndex(treePerimetre.selectedIndex);
                parentDocument.treeSelectionChanged();
                /* 	var dept:XMLList;
                   dept = _treeNodes.descendants().(@[attribute] == value);
                   if (dept[0] == null)
                   return ;
                   var pr:Object = dept[0];
                   while ((pr = pr.parent()) != null)
                   treePerimetre.expandItem(pr,true);
                   treePerimetre.selectedItem = dept[0];
                   treePerimetre.scrollToIndex(treePerimetre.selectedIndex);
                 parentDocument.treeSelectionChanged(); */
            }
            catch(e:Error)
            {
                trace(e.getStackTrace());
            }
        }

        private function treePerimetreUpdateCompleteHandler(fe:FlexEvent):void
        {
            if(treePerimetre.initialized)
                parentDocument.treeSelectionChanged();
        }

        public function getSelectedItem():Object
        {
            return treePerimetre.selectedItem;
        }

        public function getSelectedTreeItemID():Number
        {
            try
            {
                return parseInt(treePerimetre.selectedItem.@ID);
            }
            catch(error:Error)
            {
                //Alert.show("Aucun noeud n'est sélectionné.");
            }
            return -1;
        }

        public function getSelectedTreeItemValue():Number
        {
            try
            {
                // trace('getSelectedTreeItemValue -------- ' + Number(treePerimetre.selectedItem.@NID));
                return parseInt(treePerimetre.selectedItem.@NID);
            }
            catch(error:Error)
            {
                //Alert.show("Aucun noeud n'est sélectionné.");
            }
            return -1;
        }
		
		public function getNomSelectedNode():String
		{
			try
			{
				return treePerimetre.selectedItem.@LBL;
			}
			catch(error:Error)
			{
				//Alert.show("Aucun noeud n'est sélectionné.");
			}
			return "-1";
		}
		

        public function isLastSelectedChild():Boolean
        {
            try
            {
                if(treePerimetre.selectedItem.@NTY == "1")
                    return false;
                else
                    return true;
            }
            catch(error:Error)
            {
                //Alert.show("Aucun noeud n'est sélectionné.");
            }
            return false;
        }

        public function getSelectedOrganisationValue():Number
        {
            try
            {
                return parseInt(cmbOrganisation.selectedItem.IDGROUPE_CLIENT);
            }
            catch(error:Error)
            {
            }
            return -1;
        }

        public function getSelectedOrganisation():Object
        {
            try
            {
                return cmbOrganisation.selectedItem;
            }
            catch(error:Error)
            {
            }
            return -1;
        }

        public function getSelectedTreeIndex():Number
        {
            return treePerimetre.selectedIndex;
        }

        public function addOrganisationInArray(item:Object):void
        {
            _dataOraganisation.addItem(item);
        }

        public function clearSearch():void
        {
            _searchExecQuery = false;
            txtRechercheOrga.text = "";
        }

        public function getSelectedTypeOrga():String
        {
            try
            {
                return cmbOrganisation.selectedItem.TYPE_ORGA;
            }
            catch(e:Error)
            {
            }
            return null;
        }

        public function onRechercheOrgaChange(event:Event):void
        {
            _searchExecQuery = true;
        }

        /** Set the organisation list from the request in the dataProvider of the combo */
        public function setCmbData(data:ArrayCollection, label:String):void
        {
            _modeRestreint = (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE.toUpperCase() == "GROUPELIGNE");
            btnFilterCmb.visible = (_modeRestreint == false);
            btnFilterCmb.enabled = btnFilterCmb.visible;
            _dataOraganisation = data;
            if(_modeRestreint)
            {
                _dataOraganisation.filterFunction = fitrerLeComboDesOrgasParMode;
                _dataOraganisation.refresh();
            }
            else
            {
                cmbOrganisation.dataProvider.filterFunction = filterDataprovider;
                cmbOrganisation.dataProvider.refresh();
            }
            cmbOrganisation.dataProvider = _dataOraganisation;
            cmbOrganisation.prompt = resourceManager.getString('M22', 'S_lectionnez_une_organisation');
            cmbOrganisation.selectedIndex = -1;
            cmbOrganisation.labelField = label;
            if(data != null && data.length > 0)
                cmbOrganisation.selectedIndex = 0;
            callLater(refreshTree);
        }

        public function initSetCmbData(data:ArrayCollection, label:String):void
        {
            _modeRestreint = (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE.toUpperCase() == "GROUPELIGNE");
            btnFilterCmb.visible = (_modeRestreint == false);
            btnFilterCmb.enabled = btnFilterCmb.visible;
            _dataOraganisation = data;
            if(_modeRestreint)
            {
                _dataOraganisation.filterFunction = fitrerLeComboDesOrgasParMode;
                _dataOraganisation.refresh();
            }
            else
            {
                cmbOrganisation.dataProvider.filterFunction = filterDataprovider;
                cmbOrganisation.dataProvider.refresh();
            }
            cmbOrganisation.prompt = resourceManager.getString('M22', 'S_lectionnez_une_organisation');
            cmbOrganisation.selectedIndex = -1;
            cmbOrganisation.dataProvider = _dataOraganisation;
            cmbOrganisation.labelField = label;
            if(data != null && data.length > 0)
                cmbOrganisation.selectedIndex = -1;
        }

        public function fitrerLeComboDesOrgasParMode(item:Object):Boolean
        {
            var rootId:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            if(item.IDGROUPE_CLIENT == rootId)
                return true;
            else
            {
                var dataset:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;
                var node:XML = dataset.descendants("NODE").(@NID == rootId)[0];
                var pr:* = node.parent();
                while(pr != null)
                {
                    if(item.IDGROUPE_CLIENT == pr.@NID)
                        return true;
                    pr = node.parent();
                    node = pr;
                }
                return false;
            }
            return false;
        }

        /** Set the tree xml data in the dataprovider of the tree */
        public function refreshTree(myData:Object = null):void
        {
            var rootId:int;
            if(_modeRestreint)
            {
                rootId = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            }
            else
            {
                rootId = getSelectedOrganisationValue();
            }
            var dataset:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;
            //trace("OrgaStructureMain-refreshTree dataset :" + dataset.toString());
            if(myData == null)
            {
                var resultList:XMLList = dataset.descendants().(@NID == rootId);
                // Cas : Selection de tous les noeuds
                _treeNodes = resultList;
                //trace("resultList -----> " + resultList.toXMLString());
                if(resultList.length() == 1)
                {
                    treePerimetre.setTreeDataProvider(resultList[0], "LBL", new FilteredTreeDataDescriptor(fitrerByStatut));
                    treePerimetre.itemRenderer = perimetreTreeItemRenderer;
                    if(treePerimetre.selectedIndex > 0)
                    {
                        _lastSelectedItem = treePerimetre.selectedItem.@NID;
                        arrOpenItems = treePerimetre.openItems;
                        treePerimetre.addEventListener(Event.RENDER, renderDone);
                    }
                }
            }
        }
        private var arrOpenItems:Object;

        /** Add node to nodeParent in the tree */
        public function addMovedItemToTree(node:Object, IDparent:Number):void
        {
            var movedNode:Object = node;
            deleteOldTreeItem(movedNode.@NID);
            arrOpenItems = treePerimetre.openItems;
            var dept:XMLList;
            if(_treeNodes.@NID != IDparent)
            {
                dept = _treeNodes[0].descendants().(@NID == IDparent);
            }
            else
            {
                dept = _treeNodes[0].(@NID == IDparent);
            }
            XML(dept[0]).prependChild(movedNode);
            var pr:Object = dept[0];
            if(pr != null)
            {
                movedNode.@STC = pr.@STC;
                pr.@NTY = 1;
            }
            else
            {
                movedNode.@STC = 1;
            }
            _lastSelectedItem = movedNode.@NID;
            treePerimetre.validateNow();
            treePerimetre.openItems = arrOpenItems;
            treePerimetre.selectNodeById(_lastSelectedItem)
        }

        public function formateNode(node:Object):Object
        {
            var newNode:XML = <NODE/>
            newNode.@LBL = node.@LABEL;
            newNode.@NID = node.@VALUE;
            newNode.@NTY = (XML(node).hasComplexContent()) ? 1 : 0;
            return newNode;
        }

        public function deleteOldTreeItem(id:Number):void
        {
            var dept:XMLList = _treeNodes.descendants().(@NID == id);
            var pr:Object = dept[0];
            if(pr != null)
            {
                var item:XML = pr.parent();
                //var x:XMLList = tmp.descendants();
                if(item.descendants().length() <= 1)
                    item.@NTY = 0;
                delete dept[0];
            }
        }

        public function setXMLFilter(value:XML):void
        {
            menuFilterData = value;
            initFilterContextMenu();
            btnFilterCmb.addEventListener(MouseEvent.CLICK, onfilterCmbClicked);
            _filterMenu.addEventListener(MenuEvent.ITEM_CLICK, onfilterCmbChange);
        }

        protected function onfilterCmbClicked(event:MouseEvent):void
        {
            var point1:Point = new Point();
            point1.x = btnFilterCmb.x;
            point1.y = btnFilterCmb.y;
            point1 = btnFilterCmb.localToGlobal(point1);
            _filterMenu.show(point1.x, point1.y + 22);
        }

        protected function onfilterCmbChange(event:MenuEvent):void
        {
            for(var i:Number = 0; i < _filterMenu.dataProvider.length; i++)
                _filterMenu.dataProvider[i].@enabled = true;
            _filterMenu.dataDescriptor.setToggled(_filterMenu.selectedItem, false);
            if(checkTypesOrgas(event.item.@eventName) || (event.item.@eventName == "ALL"))
            {
                /*  for (var i:Number = 0; i < _filterMenu.dataProvider.length; i++)
                 _filterMenu.dataProvider[i].@enabled = true; */
                _filterMenu.dataDescriptor.setToggled(event.currentTarget.selectedItem, true);
                _filterMenu.dataDescriptor.setEnabled(event.currentTarget.selectedItem, false);
                //event.item.@enabled = false;		
                (cmbOrganisation.dataProvider as ArrayCollection).filterFunction = filterDataprovider;
                cmbOrganisation.dataProvider.refresh();
                cmbOrganisation.selectedIndex = -1;
                onOrganisationChange(new Event("refresh"));
            }
            else
            {
                Alert.show("Pas d'organisation de type " + event.item.@label, "Info.");
                _filterMenu.selectedIndex = 0;
                _filterMenu.dataDescriptor.setToggled(event.currentTarget.selectedItem, true);
                _filterMenu.dataDescriptor.setEnabled(event.currentTarget.selectedItem, false);
                event.preventDefault();
            }
        }

        private function checkTypesOrgas(type_orga:String):Boolean
        {
            if(ConsoviewUtil.isLabelInArray(type_orga, "TYPE_ORGA", (cmbOrganisation.dataProvider as ArrayCollection).source))
                return true;
            else
                return false;
        }

        private function filterDataprovider(item:Object):Boolean
        {
            if(_filterMenu.selectedItem != null)
            {
                var filter:String = _filterMenu.selectedItem.@eventName;
                if(filter == "ALL")
                    return true;
                /* if (filter == "ANU" && item.TYPE_ORGA == "ANU")return true;
                 if (filter == "GEO"&& item.TYPE_ORGA != "GEO")return true; */
                if(String(item.TYPE_ORGA).toUpperCase() == filter.toUpperCase())
                    return true;
                return false;
            }
            else
            {
                return false;
            }
            return false;
        }

        private function initFilterContextMenu():void
        {
            _filterMenu = Menu.createMenu(this, menuFilterData, false);
            _filterMenu.labelField = "@label";
        }
    /** ################### END PUBLIC METHODS ###################*/
    }
}