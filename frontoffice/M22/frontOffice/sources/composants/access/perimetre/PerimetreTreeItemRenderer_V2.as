package composants.access.perimetre
{
    import mx.controls.treeClasses.TreeItemRenderer;
    import flash.utils.getQualifiedClassName;

    public class PerimetreTreeItemRenderer_V2 extends TreeItemRenderer
    {
        public function PerimetreTreeItemRenderer_V2()
        {
            super();
        }

        public override function set data(value:Object):void
        {
            super.data = value;
            if (value != null)
            {
                if (value.@STC > 0)
                {
                    setStyle("color", "#00BB00");
                }
                else
                {
                    setStyle("color", "#000000");
                }
                // couleur rouge
                if (value.@IS_ACTIVE)
                {
                    if (value.@IS_ACTIVE == 0)
                    {
                        setStyle("color", "#FF0000");
                    }
                    if (value.@IS_ACTIVE == 0 && value.@NTY == 0)
                    {
                        setStyle("color", "#FF0000");
                    }
                }
                // cas : le dossier contient des noeuds inactifs : orange ou reste rouge s'il ne contient que des inactifs
                if (value.@HAS_INACTIVE)
                {
                    if ((value.@HAS_INACTIVE == 1 && value.@NTY == 1))
                    {
                        setStyle("color", "#FC9403");
                    }
                }
                //<NODE HAS_ACTIVE="0" HAS_INACTIVE="1" IDORGA_NIVEAU="5995" IS_ACTIVE="0" LBL="221701" NID="3790759" NTY="1" NUMERO_NIVEAU="G" STC="1"/>
                if (value.@IS_ACTIVE == 0 && value.@NTY == 1 && value.@HAS_INACTIVE == 1 && value.@HAS_ACTIVE == 0)
                {
                    setStyle("color", "#FF0000");
                }
            }
        }

        protected override function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            //super.label.text =  "Le libellé";
        }
    }
}