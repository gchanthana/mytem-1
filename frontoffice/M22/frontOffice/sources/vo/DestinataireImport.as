package vo
{
	[Bindable]
	public class DestinataireImport
	{
	
		private var _nom:String;
		private var _prenom:String;
		private var _mail:String;
		
		public function DestinataireImport()
		{
		}
		
		/**
		 * permert de faire le mapping entre le front et le back  
		 */
		public function fill(unDestinataire:Object):void
		{
			this._mail=unDestinataire.col_3;
			this._nom=unDestinataire.col_1;
			this._prenom=unDestinataire.col_2;		
		}
			
		public function get nom():String
		{
			return _nom;
		}
		
		public function set nom(value:String):void
		{
			_nom = value;
		}
		
		public function get prenom():String
		{
			return _prenom;
		}
		
		public function set prenom(value:String):void
		{
			_prenom = value;
		}
		
		public function get mail():String
		{
			return _mail;
		}
		
		public function set mail(value:String):void
		{
			_mail = value;
		}
	}
}