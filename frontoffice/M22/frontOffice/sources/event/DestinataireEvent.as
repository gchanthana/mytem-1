package event
{
	import flash.events.Event;

	public class DestinataireEvent extends Event
	{
		public static const USER_LISTE_DESTINATRAIRE_EVENT:String='UserListeDestinataireEvent';
		public static const MODIFIER_DESTINATAIRE_EVENT:String='ModifierDestinataireEvent';
		public static const DELETE_DESTINATAIRE_EVENT:String='DeleteDestinataireEvent';
		public static const LISTE_DESTINATAIRE_NOEUD_EVENT:String='ListeDestinataireNoeudEvent';
		public static const LISTE_DESTINATAIRE_PROFIL_EVENT:String='ListeDestinataireProfilEvent';
		public static const LISTE_PROFIL_EVENT:String='ListeProfilEvent';
		public static const DELETE_DESTINATAIRE_PROFIL_EVENT:String='DeleteDestinataireProfilEvent';
		public static const ACTION_MASSE_EVENT:String='ActionMasseEvent';
		
		public static const ASSOCIER_DESTINATAIRES_EVENT:String='AssocierDestinatairesEvent';

		public function DestinataireEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new DestinataireEvent(type, bubbles, cancelable);
		}
	}
}
