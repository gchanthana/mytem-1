package event
{
	import flash.events.Event;

	public class ProfilEvent extends Event
	{

		public static const MODIFIER_PROFIL_EVENT:String='ModifierProfilEvent';
		public static const DELETE_PROFIL_EVENT:String='DeleteProfilEvent';
				
		public function ProfilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ProfilEvent(type, bubbles, cancelable);
		}
	}
}