package composants.DroitModule
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.List;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.managers.PopUpManagerChildList;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import univers.parametres.parametrelogin.event.AccessEvent;
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.ihm.PopUpAccessIHM;
	import univers.parametres.parametrelogin.service.AccessService;
	import univers.parametres.parametrelogin.service.LoginService;

	[Bindable]
	public class CompDroitModuleImpl extends VBox
	{
		private var _idRacine: int = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		private var _idUser  : int = 0;
		private var _idNoeud : int = 0;
		private var _code_app  : int = CvAccessManager.getUserObject().CODEAPPLICATION;
		private var _boolIsRacine:Boolean = false		// Si le composant est utilisé pour définir les droits des modules d'une racine et non pas d'un login client
		public var loginService : LoginService= new LoginService();
		public var rp:List;
		public var tiFiltre:TextInput;
		private var SEARCH_STRING:String = "";
		public var popUpAccess : PopUpAccessIHM;
		
		public var accessService : AccessService = new AccessService();
		
	/* GETTER SETTER*/
				
		public function get boolIsRacine():Boolean
		{
			return _boolIsRacine
		}
		public function set boolIsRacine(obj:Boolean):void
		{
			_boolIsRacine = obj
		}
		public function get idRacine():int
		{
			return _idRacine
		}
		public function set idRacine(obj:int):void
		{
			_idRacine = obj
		}
		public function get idNoeud():int
		{
			return _idNoeud
		}
		public function set idNoeud(obj:int):void
		{
			_idNoeud = obj
		}
		public function get code_app():int
		{
			return _code_app
		}
		public function set code_app(obj:int):void
		{
			_code_app = obj
		}
		public function get idUser():int
		{
			return _idUser
		}
		public function set idUser(obj:int):void
		{
			_idUser = obj
		}
	/* CONSTRUCTEUR */	
		public function CompDroitModuleImpl():void
		{
			addEventListener(FlexEvent.CREATION_COMPLETE,getData)
		}
	/* FONCTION */
		
		public function init():void
		{
			accessService.model.addEventListener(AccessEvent.ACCESS_COMPLETE,accessComplete);
			loginService.addEventListener(GestionLoginEvent.LISTE_MODULE_COMPLETE,listeModuleCompleteHandler);
		}
		public function accessComplete(event:AccessEvent):void
		{
			
			if(accessService.model.module==''){
				for(var i:int=0;i<loginService.arrListeModule.length;i++){
					loginService.arrListeModule[i].HAS_PARAMS=0;
					for(var j:int=0;(j<accessService.model.accessTotal.length)&&(loginService.arrListeModule[i].HAS_PARAMS==0);j++)
					{
						if(loginService.arrListeModule[i].CODE_MODULE == accessService.model.accessTotal[j].codeModule)
						{
							if(accessService.model.accessTotal[j].isActifRacineParam!=-1){
								
								if(accessService.model.accessTotal[j].isActifRacineParam == 1
										&&
										loginService.arrListeModule[i].ACCESS_USER == 2)
								{
									loginService.arrListeModule[i].HAS_PARAMS=2
								}else if(accessService.model.accessTotal[j].isActifRacineParam == 1)
								{
									loginService.arrListeModule[i].HAS_PARAMS=1
								}
								else
								{
									loginService.arrListeModule[i].HAS_PARAMS=0
								}
								
							}
						}
					}
				}
			}
			
		}
		public function refresh():void
		{
			getData();
		}
		public function getData(e:FlexEvent=null):void
		{
			if(!boolIsRacine){
				loginService.ListeModule(idUser,idNoeud,code_app);
				
			}
		}
		private function getListeModuleEcriture():String
		{
			var str:String = ""
			for each(var obj:Object in loginService.arrListeModule)
			{
				if(obj.ACCESS_USER == 2)
					str+=","+obj.MODULE_CONSOTELID
			}
			str = str.substr(1,str.length);
			if(str.length == 0)
				str = "0"
			return str
		}
		private function getListeModuleLecture():String
		{
			var str:String = ""
			for each(var obj:Object in loginService.arrListeModule)
			{
				if(obj.ACCESS_USER == 1)
					str+=","+obj.MODULE_CONSOTELID
			}
			str = str.substr(1,str.length);
			if(str.length == 0)
				str = "0"
			return str
		}
		public function sauvegarderDroit():void
		{
			var listeModEcriture:String = getListeModuleEcriture()
			var listeModLecture:String  = getListeModuleLecture()
				
			if(!loginService.hasEventListener(GestionLoginEvent.SAUVEGARDER_DROIT_COMPLETE)){
				loginService.addEventListener(GestionLoginEvent.SAUVEGARDER_DROIT_COMPLETE,save_handler);
			}
			if(loginService.hasEventListener(GestionLoginEvent.SAUVEGARDER_DROIT_ERROR)){
				loginService.addEventListener(GestionLoginEvent.SAUVEGARDER_DROIT_ERROR,error_handler);
			}
			
			loginService.SauvegarderDroit(idUser,idNoeud,listeModEcriture,listeModLecture);
		}
		private function save_handler(e:GestionLoginEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Sauvegarde_r_ussie_avec_succ_s'),this);
			accessService.model.module = '';
			updateModuleAcceess()
		}
		
		private function listeModuleCompleteHandler(ev:GestionLoginEvent):void
		{
			accessService.getAccess('',idUser,idNoeud);
		}
		
		
		
		private function updateModuleAcceess():void
		{
			accessComplete(null);
		}
		private function error_handler(e:GestionLoginEvent):void
		{
			ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M21', 'Impossible_de_sauvegarder_les_droits'),ResourceManager.getInstance().getString('M21', 'Une_erreur_est_survenue'));
		}
				
		public function modifierAcces(nomModule:String):void
		{
			accessService.getAccess(nomModule,idUser,idNoeud);
			popUpAccess = new PopUpAccessIHM();
			popUpAccess.accessService = accessService;
			PopUpManager.addPopUp(popUpAccess,DisplayObject(parentApplication),true,PopUpManagerChildList.PARENT);
			PopUpManager.centerPopUp(popUpAccess);
		}
		
		/* HANDLER */
		public function radiobutton_clickHandler(event:MouseEvent):void
		{
			if(event != null)
			{
				for each(var obj:Object in loginService.arrListeModule)
				{
					if(obj.MODULE_CONSOTELID == parseInt((event.target as RadioButton).groupName))
					{
						obj.ACCESS_USER = (event.target as RadioButton).value
						break
					}
				}
			}
		}
		protected function tiFiltre_changeHandler(event:Event):void
		{
			filtrer(tiFiltre.text);
		}
		public function filtrer(str:String):void
		{
			SEARCH_STRING = str;
			loginService.arrListeModule.filterFunction = filtreModule;
			loginService.arrListeModule.refresh();
		}
		// PRIVATE	
		private function filtreModule(item:Object):Boolean
		{
			if(((item.CODE_MODULE as String).toLowerCase()).search(SEARCH_STRING.toLowerCase()) != -1 
				||
				((item.NOM_MODULE as String).toLowerCase()).search(SEARCH_STRING.toLowerCase()) != -1 )
			{
				return (true);
			}
			else
			{
				return (false);
			}
		}
	}
}