package univers.parametres.parametrecompte.util
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import multizone.utils.Formator;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.parametrecompte.event.EventData;
	import univers.parametres.parametrecompte.vo.Matricule;
	
	public class LibellesPersos extends EventDispatcher
	{
		public static const LIBELLES_COLLAB_LOADED : String = 'libelles_collab_loaded';
		public static const LIBELLES_LIGNE_LOADED : String = 'libelles_ligne_loaded';
		
		public function LibellesPersos() 
		{
		}

		public function updateAllLibelles(c1:String,c2:String,c3:String,c4:String,c5:String,c6:String,c7:String,c8:String,isAuto:Boolean, prefixe:String, digit:int, lastvalue:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				'fr.consotel.consoview.inventaire.employes.EmployesUtils',
																				'Upd_Champs_Perso',
																				updateAllLibellesResultHandler);
			RemoteObjectUtil.callService(op,c1,c2,c3,c4);
			
			var op2:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				'fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes',
																				'updateCustomFieldsLabel',
																				updateAllLibellesResultHandler);
			RemoteObjectUtil.callService(op2,c5,c6,c7,c8,'','','','','','');
			
			var op3:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				'fr.consotel.consoview.inventaire.employes.EmployesUtils',
																				'setInfosMatriculeAuto',
																				updateAllLibellesResultHandler);
			RemoteObjectUtil.callService(op3,Formator.convertBoolToInt(isAuto), prefixe, digit, lastvalue);
		}
		
		private function updateAllLibellesResultHandler(re : ResultEvent):void
		{			
			if(re.result > 0)
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Champs_mis___jour'));				
			else
				Alert.show(ResourceManager.getInstance().getString('M21', 'Une_erreur_s_est_produite_lors_de_la_mis'),ResourceManager.getInstance().getString('M21', 'Erreur'));
		}				
		
		public function getInfosMatriculeAuto():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				'fr.consotel.consoview.inventaire.employes.EmployesUtils',
																				'getInfosMatriculeAuto',
																				getInfosMatriculeAutoResultHandler);
			RemoteObjectUtil.invokeService(op)
		}
		
		public var myMatriculeInfos:Matricule;
		
		private function getInfosMatriculeAutoResultHandler(re:ResultEvent):void
		{			
			if(re.result)
			{
				myMatriculeInfos = new Matricule();
				
				myMatriculeInfos = Matricule.mapping(re.result);
				
				dispatchEvent(new Event('GET_INFOS_MATRICULE'));
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Erreur_de_r_cup_ration_des_informations_'), 'Consoview', null);
		}
		
		public function getLibellesPersosCollab():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				'fr.consotel.consoview.inventaire.employes.EmployesUtils',
																				'Liste_Champs_Perso',
																				getLibellesPersosCollabResultHandler);
			RemoteObjectUtil.invokeService(op)
		}
		
		private function getLibellesPersosCollabResultHandler(re : ResultEvent):void
		{			
			if((re.result is ArrayCollection) && (re.result as ArrayCollection).length > 0)
			{
				var tabResult : ArrayCollection = new ArrayCollection();
				for (var i:int=1; i<5; i++)
				{	
					tabResult.addItem(re.result[0]['C'+i]);	
				}
				dispatchEvent(new EventData(LIBELLES_COLLAB_LOADED,false,false,tabResult));
			}
		}

		public function getLibellesPersosLigne():void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				'fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes',
																				'getCustomFieldsLabel',
																				getLibellesPersosLigneResultHandler);
			RemoteObjectUtil.invokeService(op)
		}
		
		private function getLibellesPersosLigneResultHandler(re : ResultEvent):void
		{			
			if((re.result is ArrayCollection) && (re.result as ArrayCollection).length > 0)
			{
				var tabResult : ArrayCollection = new ArrayCollection();
				for (var i:int=1; i<5; i++)
				{	
					tabResult.addItem(re.result[0]['C'+i]);	
				}
				dispatchEvent(new EventData(LIBELLES_LIGNE_LOADED,false,false,tabResult));
			}
		}
	
	}
}