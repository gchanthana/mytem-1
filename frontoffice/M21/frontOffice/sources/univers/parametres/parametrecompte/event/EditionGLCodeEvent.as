package univers.parametres.parametrecompte.event
{
	import flash.events.Event;
	
	public class EditionGLCodeEvent extends Event
	{
		public static const EDITION_CODE		:String = "EDITION_CODE";
		public static const CHANGE_CODE			:String = "CHANGE_CODE";
		
		private var _labelGlCode				:String;
		private var _idTheme					:Number;
		
		public function EditionGLCodeEvent(type:String, idTheme_:Number, label_GlCode_:String ,bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this._idTheme = idTheme_;
			this._labelGlCode = label_GlCode_;
		}
		
		public function get labelGlCode():String 
		{ 
			return _labelGlCode; 
		}
		
		public function get idTheme():Number 
		{ 
			return _idTheme; 
		}
	}
}