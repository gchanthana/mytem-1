package univers.parametres.parametrecompte.event
{
	import flash.events.Event;

	public class GestionCollaborateursEvent extends Event
	{
		public static const GESTIONCOLLABORATEURS_COMPLETE:String= "gestionCollaborateurs_complete";
		public static const GESTIONCOLLABORATEURS_INIT:String= "gestionCollaborateurs_INIT";
				
		public function GestionCollaborateursEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}