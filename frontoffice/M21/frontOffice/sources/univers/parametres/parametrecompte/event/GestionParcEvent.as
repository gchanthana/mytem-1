package univers.parametres.parametrecompte.event
{
	import flash.events.Event;

	public class GestionParcEvent extends Event
	{
		public static const CREATE_PROFIL_EVENT:String = "CREATE_PROFIL_EVENT"
		public static const MAJ_PROFIL_EVENT:String = "MAJ_PROFIL_EVENT"
		public static const SUPPRIMER_PROFIL_EVENT:String = "SUPPRIMER_PROFIL_EVENT"
		public static const AFFECTER_PROFIL_EVENT:String = "AFFECTER_PROFIL_EVENT"
		
		public function GestionParcEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}