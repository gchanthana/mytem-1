package univers.parametres.parametrecompte.event
{
	import flash.events.Event;
	
	public class ProductGLCodeEvent extends Event
	{
		public static const LISTE_PRODUCT_GL_CODE 	:String = "LISTE_PRODUCT_GL_CODE";
		public static const EDITION_PRODUCT_GL_CODE :String = "EDITION_PRODUCT_GL_CODE";
		
		public static const COUNT_SELECTED :String = "COUNT_SELECTED";
		public static const CHANGE_SELECTED_ELEMENTS :String = "CHANGE_SELECTED_ELEMENTS";
		
		private var _idGlCode						:Number;
		private var _labelGlCode					:String;
		
		public function ProductGLCodeEvent(type:String, idCodeGL:Number = -1, labelCodeGL:String = '', bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			idGlCode = idCodeGL;
			labelGlCode = labelCodeGL;
		}
		
		public function get labelGlCode():String { return _labelGlCode; }
		
		public function set labelGlCode(value:String):void
		{
			if (_labelGlCode == value)
				return;
			_labelGlCode = value;
		}
		
		public function get idGlCode():Number { return _idGlCode; }
		
		public function set idGlCode(value:Number):void
		{
			if (_idGlCode == value)
				return;
			_idGlCode = value;
		}
	}
}