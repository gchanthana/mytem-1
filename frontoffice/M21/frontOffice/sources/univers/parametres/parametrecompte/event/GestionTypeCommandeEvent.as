package univers.parametres.parametrecompte.event
{
	import flash.events.Event;
	
	public class GestionTypeCommandeEvent extends Event
	{
		
		public static const UPDATE_INFO_TYPE_CMD_COMPLETE:String= "updateInfoTypeCMDComplete";
		public static const PARAMETRE_TYPE_CMD_COMPLETE:String= "parametreTypeCMDComplete";
		public static const UPDATE_TYPE_COMMANDE_LOGIN:String= "updateTYpeCommandeLogin";		
		public static const LISTE_CONTRAINTE_COMPLETE:String= "listeContrainteComplete";	
		public static const REMOVE_COMPLETE:String= "removeComplete";	
		public static const TYPE_COMMANDE_CHANGE :String = 'typeCOmmandeChange'; 
		
		
		public var idTypeCommande:int;
		
		public function GestionTypeCommandeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,idTypeCommande:int = -1)
		{
			super(type, bubbles, cancelable);
			this.idTypeCommande = idTypeCommande;
		}

	}
}