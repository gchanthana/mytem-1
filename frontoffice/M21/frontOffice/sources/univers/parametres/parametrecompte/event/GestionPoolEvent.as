package univers.parametres.parametrecompte.event
{
	import flash.events.Event;

	public class GestionPoolEvent extends Event
	{
		public static const LISTE_POOL_COMPLETE:String= "ListePoolComplete";
		public static const LISTE_SITE_COMPLETE:String= "ListeSiteComplete";
		public static const LISTE_TYPECMD_COMPLETE:String= "ListeTypeCmdComplete";
		public static const LISTE_SITE_OF_POOL_COMPLETE:String= "ListeSIteOfPoolComplete";
		public static const LISTE_REVENDEUR_LIBRE_COMPLETE:String= "ListeRevendeurComplete";
		public static const LISTE_CONTRAINTE_COMPLETE:String= "listeContrainteHandler";
		public static const REMOVE_POOL_COMPLETE:String= "removePoolComplete";
		public static const POOL_INFO_CHANGE_COMPLETE:String= "poolInfoChangeComplete"; //Changement des infos(libellé,code interne...)
		public static const POOL_ATTRIBUT_CHANGE_COMPLETE:String= "poolAttributChangeComplete";//Changement des sites afectés, de type de commandes affectés...)
		public static const PARAMETRE_POOL_COMPLETE:String= "parametrePoolComplete";
		public static const UPDATE_PROFIL_ON_POOL:String= "updateProfilOnPool";
		public static const LISTE_COMPTE_OPERATEUR_COMPLETE:String= "listeCompteOperateurComplete";
		public static const LISTE_COMPTE_OPERATEUR_CHOIX_UNIQUE_COMPLETE:String= "listeCompteOperateurChoixUniqueComplete";
		public static const UPDATE_PROFIL_LOGIN:String= "updateProfilLogin";
		public static const LISTE_ALL_REVENDEUR_COMPLETE:String= "listeAllRevendeurComplete";
		public static const UPDATE_LISTE_OPERATEUR:String= "updateListeOperateur";
	
		public var idPool:int;
		
		public function GestionPoolEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,idPool:int = -1)
		{
			super(type, bubbles, cancelable);
			this.idPool = idPool;
		}
		
	}
}