package univers.parametres.parametrecompte.vo
{
	[Bindable]
	public class OperateurVO
	{
		public var idOp : int;
		public var libelleOp : String;
		public var boolSelected:Boolean = false;

		public function OperateurVO()
		{
			
		}
	}
}
		