package univers.parametres.parametrecompte.vo
{
	public class ActionGPVo
	{
		public var idAction:int
		public var libelle:String
		public var type:String
		public var selected:Boolean
		
		public function ActionGPVo()
		{
		}

	}
}