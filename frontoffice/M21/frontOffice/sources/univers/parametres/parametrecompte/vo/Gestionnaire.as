package univers.parametres.parametrecompte.vo
{
	[Bindable]
	public class Gestionnaire
	{
		private var _id : int;
		private var _nom : String;
		private var _prenom : String;
		private var _nom_prenom : String;
		private var _login : String;
		public var libellePool : String;
		public var idRevendeur : int;
		public var idPool : int;
		/**
		 1 pour consotel (à comprendre comme type admin)
  		 2 pour distributeur
   		 3 pour client
		 * */
		public var idTypeLogin : int;
		private var _idRacine : int;
		private var _idRacineRat : int;
		
		public var cp : String;
		public var adresse :  String;
		public var ville :  String;
		public var tel :  String;
		public var direction :  String;
		public var mail :  String;
		public var password :  String;
		
		
		public var bool_acces_perimetre : Number = 0;
		public var nb_failed_connection : Number = 0;
		public var idstatut : Number = 0;
		
		
		public var restrictIP : Boolean = false;
		
		public var selected:Boolean = false;
		
		private var _attribut:String;
		
		public function Gestionnaire()
		{
			
		}
		
		public function fillObject(value:Object):Gestionnaire
		{
			this.id = value.id;
			this.nom = value.nom;
			this.prenom = value.prenom;
			this.mail = value.mail;
			this.selected = value.selected;
			
			return this;
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------GET------------------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
		
		public function get id():int
		{	
			return _id;
		}
	
		public function get login():String
		{	
			return _login;
		}
		public function get nom():String
		{	
			return _nom;
		}
		public function get prenom():String
		{	
			return _prenom;
		}
		public function get nom_prenom():String
		{	
			_nom_prenom=_nom+" "+_prenom; 
			return _nom_prenom;
		}
		public function get idRacine():int
		{	
			return _idRacine;
		}
		public function get idRacineRat():int
		{	
			return _idRacineRat;
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------SET------------------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
		public function set id(id:int):void
		{	
			this._id = id;
		}
		
		public function set nom(nom:String):void
		{	
			this._nom =nom ;
		}
		public function set prenom(prenom:String):void
		{	
			this._prenom =prenom ;
		}
		public function set login(login:String):void
		{	
			this._login =login ;
		}
		public function set idRacine(idRacine:int):void
		{	
			this._idRacine = idRacine;
		}
		public function set idRacineRat(value:int):void
		{	
			this._idRacineRat = value;
		}
		
		public function get attribut():String { return _attribut; }
		
		public function set attribut(value:String):void
		{
			if (_attribut == value)
				return;
			_attribut = value;
		}
		

	}
}