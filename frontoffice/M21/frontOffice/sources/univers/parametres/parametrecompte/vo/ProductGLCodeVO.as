package univers.parametres.parametrecompte.vo
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class ProductGLCodeVO
	{
		private var _idGLCode		:Number;
		private var _libelleGLCode	:String = '';
		private var _idThemeProduit	:Number;
		private var _segmentTheme	:String = '';
		private var _libelleTheme	:String = '';
		private var _typeTheme		:String = '';
		private var _codesGL		:ArrayCollection;
		private var _index			:Number;
		private var _valid			:Boolean;
		private var _showEdit		:Boolean = false;
		private var _SELECTED		:Boolean = false;
		
		public function ProductGLCodeVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty('IDGL_CODE'))
					this.idGLCode = obj.IDGL_CODE;
				
				if(obj.hasOwnProperty('GL_CODE'))
					this.libelleGLCode = (obj.GL_CODE)?obj.GL_CODE:'';
				
				if(obj.hasOwnProperty('IDTHEME_PRODUIT'))
					this.idThemeProduit = obj.IDTHEME_PRODUIT;
				
				if(obj.hasOwnProperty('SEGMENT_THEME'))
					this.segmentTheme = obj.SEGMENT_THEME;
				
				if(obj.hasOwnProperty('THEME_LIBELLE'))
					this.libelleTheme = obj.THEME_LIBELLE;
				
				if(obj.hasOwnProperty('TYPE_THEME'))
					this.typeTheme = obj.TYPE_THEME;
			}
			catch(e:Error)
			{
				
			}
		}
		
		public function get idGLCode():Number { return _idGLCode; }
		
		public function set idGLCode(value:Number):void
		{
			if (_idGLCode == value)
				return;
			_idGLCode = value;
		}
		
		public function get libelleGLCode():String { return _libelleGLCode; }
		
		public function set libelleGLCode(value:String):void
		{
			if (_libelleGLCode == value)
				return;
			_libelleGLCode = value;
		}
		
		public function get typeTheme():String { return _typeTheme; }
		
		public function set typeTheme(value:String):void
		{
			if (_typeTheme == value)
				return;
			_typeTheme = value;
		}
		
		public function get libelleTheme():String { return _libelleTheme; }
		
		public function set libelleTheme(value:String):void
		{
			if (_libelleTheme == value)
				return;
			_libelleTheme = value;
		}
		
		public function get segmentTheme():String { return _segmentTheme; }
		
		public function set segmentTheme(value:String):void
		{
			if (_segmentTheme == value)
				return;
			_segmentTheme = value;
		}
		
		public function get idThemeProduit():Number { return _idThemeProduit; }
		
		public function set idThemeProduit(value:Number):void
		{
			if (_idThemeProduit == value)
				return;
			_idThemeProduit = value;
		}
		public function get showEdit():Boolean { return _showEdit; }
		
		public function set showEdit(value:Boolean):void
		{
			if (_showEdit == value)
				return;
			_showEdit = value;
		}
		
		public function get index():Number { return _index; }
		
		public function set index(value:Number):void
		{
			if (_index == value)
				return;
			_index = value;
		}
		
		public function get valid():Boolean { return _valid; }
		
		public function set valid(value:Boolean):void
		{
			if (_valid == value)
				return;
			_valid = value;
		}
		
		public function get codesGL():ArrayCollection { return _codesGL; }
		
		public function set codesGL(value:ArrayCollection):void
		{
			if (_codesGL == value)
				return;
			_codesGL = value;
		}
		
		public function get SELECTED():Boolean { return _SELECTED; }
		
		public function set SELECTED(value:Boolean):void
		{
			if (_SELECTED == value)
				return;
			_SELECTED = value;
		}
	}
}