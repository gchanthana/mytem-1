package univers.parametres.parametrecompte.vo
{
	[Bindable]
	public class TypeCommande
	{
		public var idTypeCmd : int;
		public var libelleTypeCmd : String;
		public var codeTypeCmd: String;
		public var commentaire: String;
		public var nbPool : int;
		public var nbGestionnaire : int;
		public var isInPool:Boolean = false;
		public var boolSelected:Boolean = false;
		public var segmentFixe:Boolean = true;
		public var segmentMobile:Boolean = true;
		public var segmentReseau:Boolean = true;

		public function TypeCommande()
		{
			
		}
	}
}
		