package univers.parametres.parametrecompte.ihm
{
	import composants.util.ConsoviewAlert;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.RadioButtonGroup;
	import mx.resources.ResourceManager;
	
	import univers.parametres.parametrecompte.event.GestionCollaborateursEvent;
	import univers.parametres.parametrecompte.service.GestionCollaborateursService;

	public class GestionCollaborateurs extends VBox
	{
		
		public var rbGestCollab:RadioButtonGroup;
		public var gestionCollaborateursService:GestionCollaborateursService;
		
		public function GestionCollaborateurs()
		{
		}
		
		public function init():void{
			rbGestCollab.selectedValue=0;
			gestionCollaborateursService = new GestionCollaborateursService();
			gestionCollaborateursService.model.addEventListener(GestionCollaborateursEvent.GESTIONCOLLABORATEURS_INIT, initGestionCollaborateursHandler);
			gestionCollaborateursService.getRacineGestCollab();
		}
		
		private function initGestionCollaborateursHandler(event:GestionCollaborateursEvent):void
		{
			rbGestCollab.selectedValue = gestionCollaborateursService.model.type;
		}
		
		public function checkRadio(value:int):void
		{
			rbGestCollab.selectedValue = value;
		}
		
		protected function rbGestCollab_itemClickHandler():void
		{
			var choix:Number = Number(rbGestCollab.selectedValue);
			
			gestionCollaborateursService.getGestionCollaborateurs(choix);
						
		}
		
	}
}