package univers.parametres.parametrecompte.ihm
{
	import mx.containers.VBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	
	import univers.parametres.parametrecompte.event.GestionTypeCommandeEvent;
	import univers.parametres.parametrecompte.service.GestionPoolService;
	
	[Bindable]
	public class ListeOperateur extends VBox
	{
		public var gestionPoolService : GestionPoolService= new GestionPoolService();
		
		
		public var idLogin : int;
		public var groupeIndex : int;
		
		//COMPONANT
		public var dg_pool_gestionnaire: DataGrid;
		public var comboDistrib : ComboBox;
		
		
		public function ListeOperateur()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		public function init(evt : FlexEvent):void
		{
			initData();
			//typeCommandeService.addEventListener(GestionTypeCommandeEvent.UPDATE_TYPE_COMMANDE_LOGIN,update_type_commande_handler);
		}
		public function initData():void
		{
			gestionPoolService.getRevendeur();
		}
		public function comboRevHandler():void
		{
			if(comboDistrib.selectedItem)
			{
				gestionPoolService.listOpDistrib(comboDistrib.selectedItem.IDFOURNISSEUR);
			}
		}
		public function onItemChanged(item : Object):void
		{
			var boolExiste : Boolean = false;
			for(var i:int=0;i<gestionPoolService.col_operateur_selected.length;i++)
			{
				if(gestionPoolService.col_operateur_selected[i].idOp==item.idOp)
				{
					gestionPoolService.col_operateur_selected.removeItemAt(i);
					gestionPoolService.updateOpDistrib(comboDistrib.selectedItem.IDFOURNISSEUR,item.idOp,0);
					boolExiste = true;
				}
			}
			if(!boolExiste) //Si l'item n'est pas déja dans la liste
			{
				gestionPoolService.col_operateur_selected.addItem(item.idOp);
				gestionPoolService.updateOpDistrib(comboDistrib.selectedItem.IDFOURNISSEUR,item.idOp,1);
			}
			gestionPoolService.col_operateur_selected.refresh();
		}
		private function update_type_commande_handler(evt : GestionTypeCommandeEvent):void
		{
			dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.TYPE_COMMANDE_CHANGE,true));	
		}
		
	}
}