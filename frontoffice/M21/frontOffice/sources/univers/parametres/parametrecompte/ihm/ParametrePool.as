package univers.parametres.parametrecompte.ihm
{ 
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.net.registerClassAlias;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;

	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.service.CompteOperateurService;
	import univers.parametres.parametrecompte.service.GestionPoolService;
	import univers.parametres.parametrecompte.service.SiteService;
	import univers.parametres.parametrecompte.service.TypeCommandeService;
	import univers.parametres.parametrecompte.util.CompteHelper;
	import univers.parametres.parametrecompte.vo.CompteOperateur;
	import univers.parametres.parametrecompte.vo.PoolGestionnaire;
	import univers.parametres.parametrecompte.vo.Site;
	import univers.parametres.parametrecompte.vo.TypeCommande;

	[Bindable]
	public class ParametrePool extends TitleWindow
	{
		public var poolGestionnaire : PoolGestionnaire = new PoolGestionnaire();
		public var gestionPoolService : GestionPoolService = new GestionPoolService();
		public var siteService : SiteService = new SiteService();
		public var typeCommandeService : TypeCommandeService = new TypeCommandeService();
		public var compteOPerateurService : CompteOperateurService= new CompteOperateurService();
		public var groupeIndex : int;
		public var nbSousCompte : int;
		public var nbSousCompteTotal : int;
		public var comboOperateurSelectedIndex:Number=-1;
		//UI COMPONANT
		public var cbAll : CheckBox;
		public var cbAllTypeCMD : CheckBox;
		public var cbAllCompteOp : CheckBox;
		public var comboRevendeur :  ComboBox;
		public var combo_revendeur_commande : ComboBox;
		public var combo_operateur : ComboBox;
		public var txtFiltre : TextInput;
		public var txtFiltreSite : TextInput;
		public var ongletTypeCommande : VBox;
		public var ckRevendeur : CheckBox;
		
		
		
		public function ParametrePool()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			gestionPoolService.addEventListener(GestionPoolEvent.LISTE_REVENDEUR_LIBRE_COMPLETE,reselectComboRevendeur);
		
		}
		public function init(evt : FlexEvent):void
		{
			gestionPoolService.getRevendeur(poolGestionnaire.IDPool);
			if(poolGestionnaire.IDPool>0)
			{
				initMode_updatePool();
			}
			else
			{
				initMode_createPool();
			}
			
			typeCommandeService.addEventListener(GestionPoolEvent.LISTE_TYPECMD_COMPLETE,listeTypecmdCompleteHandler);
			compteOPerateurService.addEventListener(GestionPoolEvent.LISTE_COMPTE_OPERATEUR_CHOIX_UNIQUE_COMPLETE,choixUnique_handler);
		}
		
		public function choixUnique_handler(event : GestionPoolEvent):void
		{
			comboOperateurSelectedIndex=0;
			compteOPerateurService.addEventListener(GestionPoolEvent.LISTE_COMPTE_OPERATEUR_COMPLETE,getCompteOpePool_handler);
			compteOPerateurService.getCompteOpePool(poolGestionnaire.IDPool,compteOPerateurService.col_operateur[0].OPERATEURID);
		}
		
		private function listeTypecmdCompleteHandler(evt:GestionPoolEvent):void{
			cbAllTypeCMD.selected=(typeCommandeService.col_typeCMD.length==typeCommandeService.tab_idTypeCmd_of_pool.length)&&(typeCommandeService.col_typeCMD.length!=0);
		}
		private function initMode_updatePool():void
		{
			registerClassAlias("univers.parametres.parametrecompte.vo.PoolGestionnaire",PoolGestionnaire);  
			poolGestionnaire= PoolGestionnaire(ObjectUtil.copy(poolGestionnaire))  // delete les ref sur le grid
			initAttribut();
		}
		private function initAttribut():void
		{
			siteService.getSitePool(groupeIndex,poolGestionnaire.IDPool);
			compteOPerateurService.fournirListeOperateursClient(groupeIndex);
		}
		private function liste_all_revendeur_handler(evt : GestionPoolEvent):void
		{
			//pour mettre ajour l'état de l'onglet type de commande
			comboRevendeur.dispatchEvent(new ListEvent(ListEvent.CHANGE));
		}
		private function initMode_createPool():void
		{
			//poolGestionnaire = new PoolGestionnaire();
			//typeCommandeService.liste_type_cmd(groupeIndex);
		}
		private function reselectComboRevendeur(evt : GestionPoolEvent):void
		{
			if(poolGestionnaire.IDPoolRevendeur>0) // Si c'est un pool revendeur et qu'on modifit le pool, il faut reselect le revendeur
			{
				for (var i : int = 0; i<gestionPoolService.col_revendeur.length;i++)
				{
					if(poolGestionnaire.IDPoolRevendeur == gestionPoolService.col_revendeur.getItemAt(i).IDFOURNISSEUR)
					{
						comboRevendeur.selectedIndex = i;
						break;
					}
				}
			}
			//Ces fonctions sont appelé ici car il faut que la liste des revendeur dispo soit définit avant la liste de tous les revendeurs
			gestionPoolService.addEventListener(GestionPoolEvent.LISTE_ALL_REVENDEUR_COMPLETE,liste_all_revendeur_handler);
			gestionPoolService.getAllRevendeur(groupeIndex); 
		}
		public function cbAllHandler(evt : Event):void
		{
			siteService.tab_idSite_of_pool = new ArrayCollection();
			var tmpArray : ArrayCollection = new ArrayCollection();
			for each (var item:Site in siteService.col_site_of_pool){
				item.isInPool = cbAll.selected;
				if(item.isInPool){
					siteService.tab_idSite_of_pool.addItem(item.id_site);
				}
				tmpArray.addItem(item.id_site);
			}
			if(cbAll.selected)
			{
				siteService.updateSiteOfPool(poolGestionnaire.IDPool,tmpArray.source,1);
			}
			else
			{
				siteService.updateSiteOfPool(poolGestionnaire.IDPool,tmpArray.source,0);
			}
			siteService.col_site_of_pool.refresh();
		}
		public function cbAllTypeCmdHandler(evt : Event):void
		{
			typeCommandeService.tab_idTypeCmd_of_pool = new ArrayCollection();
			
			var tmpArray : ArrayCollection = new ArrayCollection();
			
			for each (var item:TypeCommande in typeCommandeService.col_typeCMD){
				item.isInPool = cbAllTypeCMD.selected;
				if(item.isInPool){
					typeCommandeService.tab_idTypeCmd_of_pool.addItem(item.idTypeCmd);
				}
				tmpArray.addItem(item.idTypeCmd);
			}
			if(cbAllTypeCMD.selected)
			{
				 typeCommandeService.updateTypeCommandeOfPool(poolGestionnaire.IDPool,tmpArray.source,combo_revendeur_commande.selectedItem.IDREVENDEUR,1);
			}
			else
			{
				typeCommandeService.updateTypeCommandeOfPool(poolGestionnaire.IDPool,tmpArray.source,combo_revendeur_commande.selectedItem.IDREVENDEUR,0);
			}
			typeCommandeService.col_typeCMD.refresh();
		}
		public function onItemChanged(item : Object):void
		{
			var boolExiste : Boolean = false;
			for(var i:int=0;i<siteService.tab_idSite_of_pool.length;i++)
			{
				if(siteService.tab_idSite_of_pool[i]==item.id_site)
				{
					siteService.tab_idSite_of_pool.removeItemAt(i);
					
					siteService.updateSiteOfPool(poolGestionnaire.IDPool,[item.id_site],0);
					
					boolExiste = true;
				}
			}
			if(!boolExiste) //Si l'item n'est pas déja dans la liste
			{
				siteService.tab_idSite_of_pool.addItem(item.id_site);
				
				siteService.updateSiteOfPool(poolGestionnaire.IDPool,[item.id_site],1);
			}
			siteService.tab_idSite_of_pool.refresh();
			cbAll.selected=(siteService.col_site_of_pool.length==siteService.tab_idSite_of_pool.length)&&(siteService.col_site_of_pool.length!=0);
		}
		public function onItemTypeCmdChanged(item : Object):void
		{
			var boolExiste : Boolean = false;
			for(var i:int=0;i<typeCommandeService.tab_idTypeCmd_of_pool.length;i++)
			{
				if(typeCommandeService.tab_idTypeCmd_of_pool[i]==item.idTypeCmd)
				{
					typeCommandeService.tab_idTypeCmd_of_pool.removeItemAt(i);
					boolExiste = true;
					typeCommandeService.updateTypeCommandeOfPool(poolGestionnaire.IDPool,[item.idTypeCmd],combo_revendeur_commande.selectedItem.IDREVENDEUR,0);
				}
			}
			if(!boolExiste) //Si l'item n'est pas déja dans la liste
			{
				typeCommandeService.tab_idTypeCmd_of_pool.addItem(item.idTypeCmd);
				typeCommandeService.updateTypeCommandeOfPool(poolGestionnaire.IDPool,[item.idTypeCmd],combo_revendeur_commande.selectedItem.IDREVENDEUR,1);
			}
			typeCommandeService.tab_idTypeCmd_of_pool.refresh() ;
			cbAllTypeCMD.selected=(typeCommandeService.col_typeCMD.length==typeCommandeService.tab_idTypeCmd_of_pool.length)&&(typeCommandeService.col_typeCMD.length!=0);
		}
		public function fermer(event : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		public function valider():void
		{
			var idRevendeur:int = -1
			if(ckRevendeur.selected)
			{
				idRevendeur = comboRevendeur.selectedItem.IDFOURNISSEUR;
			}
			gestionPoolService.addEventListener(GestionPoolEvent.POOL_INFO_CHANGE_COMPLETE,pool_info_change_handler);
			if(poolGestionnaire.IDPool>0)
			{
				//update
				gestionPoolService.update_pool_gestionnaire(poolGestionnaire.IDPool,poolGestionnaire.libelle_pool,poolGestionnaire.codeInterne_pool,poolGestionnaire.commentaire_pool,CvAccessManager.getSession().USER.CLIENTACCESSID,idRevendeur);
			}
			else
			{
				//add
				gestionPoolService.add_pool_gestionnaire(poolGestionnaire.libelle_pool,poolGestionnaire.codeInterne_pool,poolGestionnaire.commentaire_pool,CvAccessManager.getSession().USER.CLIENTACCESSID,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,idRevendeur);
			}
		}
		private function pool_info_change_handler(evt : GestionPoolEvent):void
		{
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.PARAMETRE_POOL_COMPLETE));
			var msg : String;
			if(poolGestionnaire.IDPool>0)
			{
				msg = ResourceManager.getInstance().getString('M21', 'Les_modifications_ont_bien__t__enregistr');
				ConsoviewAlert.afficherOKImage(msg);
				PopUpManager.removePopUp(this);
			}
			else
			{
				poolGestionnaire.IDPool = evt.idPool;
				initAttribut();
			}
		}
		public function combo_revendeur_commande_handler(evt : Event):void
		{
			cbAllTypeCMD.selected = false;
			if(combo_revendeur_commande.selectedIndex!=-1)
			{
				typeCommandeService.getTypeCommandePool(groupeIndex,combo_revendeur_commande.selectedItem.IDREVENDEUR,poolGestionnaire.IDPool);
				combo_revendeur_commande.errorString = "";
			}
			else
			{
				combo_revendeur_commande.errorString = ResourceManager.getInstance().getString('M21', 'S_lectionnez_un_revendeur');
				typeCommandeService.col_typeCMD.removeAll();
			}
			
		}
		public function combo_operateur_handler(evt : Event):void
		{
			cbAllCompteOp.selected = false;
			
			if(combo_operateur.selectedIndex!=-1)
			{
				compteOPerateurService.addEventListener(GestionPoolEvent.LISTE_COMPTE_OPERATEUR_COMPLETE,getCompteOpePool_handler);
				compteOPerateurService.getCompteOpePool(poolGestionnaire.IDPool,combo_operateur.selectedItem.OPERATEURID);
				//combo_operateur.errorString = "";
			}
			else
			{
				//combo_operateur.errorString = ResourceManager.getInstance().getString('M21', 'S_lectionnez_un_op_rateur');
				compteOPerateurService.col_compteOP_pool = null;
			}
		}
		private function getCompteOpePool_handler(evt : GestionPoolEvent):void
		{
			filtreHandler();
			compterCompteTotal();
			compterCompte();
			
		}
		protected function txtFiltreSitefiltreHandler(e:Event = null):void{
			siteService.col_site_of_pool.filterFunction=filtreGridSite;
			siteService.col_site_of_pool.refresh();
		}
		private function filtreGridSite(item:Site):Boolean{
			if(txtFiltreSite && txtFiltreSite.text){
				if (
					(item.libelle_site && (((item.libelle_site as String).toLowerCase()).search(txtFiltreSite.text.toLowerCase())!=-1 ))
					||
					(item.ref_site && (((item.ref_site as String).toLowerCase()).search(txtFiltreSite.text.toLowerCase())!=-1 ))
					||
					(item.code_interne_site && (((item.code_interne_site as String).toLowerCase()).search(txtFiltreSite.text.toLowerCase())!=-1 ))
				)	
				{
					return true;
				}else{
					return false;
				}
			}
			else{
				return true;
			}
			
		}

		protected function filtreHandler(e:Event = null):void{
			compteOPerateurService.col_compteOP_pool.filterFunction= filtreGrid;
			compteOPerateurService.col_compteOP_pool.refresh();
		}
		private function filtreGrid(item:Object):Boolean{
			if(txtFiltre){
				if (((item.libelle as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1 )return true;
				else return false;
			}
			else{
				return true;
			}
			
		}
		public function cbAllHandlerCompteOp(evt : Event):void
		{
			var action : Number; // 1 pour affecter et 0 pour desaffecter
			if(cbAllCompteOp.selected)
			{
				action = 1;
			}
			else
			{
				action = 0;
			}
			
			var colCompte : ArrayCollection = compteOPerateurService.col_compteOP_pool;
			var arr : Array = new Array();
			for(var a:int=0;a<colCompte.source.length;a++)
			{
				var curentObj : CompteOperateur = colCompte.source[a] as CompteOperateur;
				if(!curentObj.isCompteMaitre)
				{
					arr.push(curentObj.idSousCompteOP);
				}
				curentObj.boolSelected = cbAllCompteOp.selected;
				colCompte.itemUpdated(curentObj);
			}
			if(action==1)
			{
				compteOPerateurService.nbCompteOpSelected_Pool =  compteOPerateurService.col_compteOP_pool.source.length
			}
			else
			{
				compteOPerateurService.nbCompteOpSelected_Pool = 0;
			}
			compteOPerateurService.updateXCompteOpePool(poolGestionnaire.IDPool,arr,action);
			compterCompte();
			cbAllCompteOp.selected=(nbSousCompte==nbSousCompteTotal)&&(nbSousCompteTotal!=0);
		}
		public function onItemCompteOpChanged(item : Object):void
		{
			var action : Number; // 1 pour affecter et 0 pour desaffecter
			var compteHelper : CompteHelper = new CompteHelper();
			var arrIDSousCompte : Array = new Array();
			
			if(item.boolSelected)
			{
				action = 1;
			}
			else
			{
				action = 0;
			}
			//Si c'est un compte on selectionne tous les sous comptes : 
			if(item.isCompteMaitre)
			{
				arrIDSousCompte = compteHelper.getIDSousCompteOfCompteNotFormated(item as CompteOperateur,compteOPerateurService.col_compteOP_pool,action==1)
			}
			//Sinon on ne prend que le compte
			else
			{
				arrIDSousCompte.push(item.idSousCompteOP);
			}
			if(action==1)
			{
				compteOPerateurService.nbCompteOpSelected_Pool = compteOPerateurService.nbCompteOpSelected_Pool + arrIDSousCompte.length
			}
			else
			{
				compteOPerateurService.nbCompteOpSelected_Pool = compteOPerateurService.nbCompteOpSelected_Pool - arrIDSousCompte.length
			}
			compteHelper.formatListeCompte(compteOPerateurService.col_compteOP_pool);
			compteOPerateurService.updateXCompteOpePool(poolGestionnaire.IDPool,arrIDSousCompte,action);
			compterCompte();
			cbAllCompteOp.selected=(nbSousCompte==nbSousCompteTotal)&&(nbSousCompteTotal!=0);
		}
		public function comboRevendeurChange(evt : Event):void
		{
			
			if(ckRevendeur.selected)
			{
				ongletTypeCommande.enabled=false;
			}
			else
			{
				ongletTypeCommande.enabled=true;
			}
			if(comboRevendeur.selectedIndex != -1)
			{
				for(var i:int=0;i<gestionPoolService.col_all_revendeur.length;i++)
				{
					if(gestionPoolService.col_all_revendeur.getItemAt(i).IDREVENDEUR==comboRevendeur.selectedItem.IDFOURNISSEUR)
					{
						combo_revendeur_commande.selectedIndex = i;
						combo_revendeur_commande.dispatchEvent(new ListEvent(ListEvent.CHANGE));
						combo_revendeur_commande.enabled = false;
						combo_revendeur_commande.errorString ="";
						ongletTypeCommande.enabled=true;
					}
				}
			}
			else
			{
				combo_revendeur_commande.selectedIndex = -1;
				combo_revendeur_commande.dispatchEvent(new ListEvent(ListEvent.CHANGE));
				combo_revendeur_commande.enabled = true;
				combo_revendeur_commande.errorString =ResourceManager.getInstance().getString('M21', 'S_lectionnez_un_revendeur');
				
			}
		}
		public function ckRevendeurChange(evt : Event ):void
		{
			comboRevendeur.selectedIndex = -1;
			comboRevendeur.dispatchEvent(new ListEvent(ListEvent.CHANGE));
		}
		public function compterCompte():void
		{
			nbSousCompte=0;
			for(var i:int=0;i<compteOPerateurService.col_compteOP_pool.length;i++)
			{
				if(!compteOPerateurService.col_compteOP_pool[i].isCompteMaitre){
					if(compteOPerateurService.col_compteOP_pool[i].boolSelected){
						nbSousCompte++
					}
				}
			}
			
		}
		public function compterCompteTotal():void
		{
			nbSousCompteTotal=0;
			for(var i:int=0;i<compteOPerateurService.col_compteOP_pool.length;i++)
			{
				if(!compteOPerateurService.col_compteOP_pool[i].isCompteMaitre){
					nbSousCompteTotal++
				}
			}
			
		}
	}
}