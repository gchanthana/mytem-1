package univers.parametres.parametrecompte.ihm
{
	import mx.containers.VBox;
	import mx.controls.DataGrid;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrecompte.event.GestionParcEvent;
	import univers.parametres.parametrecompte.service.GestionParcService;
	import univers.parametres.parametrecompte.vo.ProfilGestionParc;

	public class FicheProfilGestionParcImpl extends VBox
	{
		[Bindable] public var gpservice:GestionParcService
		public var dg_profil:DataGrid
		
		public function FicheProfilGestionParcImpl()
		{
			super();
			gpservice = new GestionParcService()
		}
	// CREATION COMPLETE HANDLER
		public function init():void
		{
		}
		public function initData():void
		{
			gpservice.getListeProfilGestionParc()
		}
	// CREATION D'UN NOUVEAU PROFIL	
		public function newProfil():void
		{
			var popup:ParametreProfilGestionParcIHM = new ParametreProfilGestionParcIHM()
			popup.boolReadOnly = false
			popup.boolNewProfil = true
			popup.profil = new ProfilGestionParc()
			popup.gpservice.addEventListener(GestionParcEvent.CREATE_PROFIL_EVENT,refresh)
			popup.gpservice.addEventListener(GestionParcEvent.MAJ_PROFIL_EVENT,refresh)
			PopUpManager.addPopUp(popup,this)
			PopUpManager.centerPopUp(popup)
		}
	// VOIR LES USERS 
		public function btNbUserHandler(profil:ProfilGestionParc):void
		{
			var popUpUserOfProfilIHM:ListeUtilisateurProfilGestionParcIHM = new ListeUtilisateurProfilGestionParcIHM();
			popUpUserOfProfilIHM.profil = dg_profil.selectedItem as ProfilGestionParc;
			PopUpManager.addPopUp(popUpUserOfProfilIHM,this)
			PopUpManager.centerPopUp(popUpUserOfProfilIHM)
		}	
	// MODIFIER UN PROGIL GESTION DE PARC
		public function modifier(profil:ProfilGestionParc):void
		{
			var popup:ParametreProfilGestionParcIHM = new ParametreProfilGestionParcIHM()
			popup.boolReadOnly = false
			popup.profil =  dg_profil.selectedItem as ProfilGestionParc;
			popup.gpservice.addEventListener(GestionParcEvent.CREATE_PROFIL_EVENT,refresh)
			popup.gpservice.addEventListener(GestionParcEvent.MAJ_PROFIL_EVENT,refresh)
			PopUpManager.addPopUp(popup,this)
			PopUpManager.centerPopUp(popup)
		}
	// SUPPRIMER UN PROFIL
		public function supprimer(profil:ProfilGestionParc):void
		{
			gpservice.addEventListener(GestionParcEvent.SUPPRIMER_PROFIL_EVENT,refresh)
			gpservice.SupprimerProfilGestionParc(profil.idProfil);
		}
	// REFRESH
		private function refresh(e:GestionParcEvent):void
		{
			gpservice.getListeProfilGestionParc()
		}
	}
}