package univers.parametres.parametrecompte.ihm
{
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.RadioButtonGroup;
	import mx.events.FlexEvent;
	
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.service.LoginService;
	
	
	[Bindable]
	public class ListeDroitCompte extends VBox
	{
		public var loginService : LoginService = new LoginService();
				//UI COMPONANT
		
		public var groupe_checkAdmin : CheckBox;
				
		public function ListeDroitCompte()
		{
			
		}
	
		public function init_data_handler(evt : GestionLoginEvent):void
		{	
			
			groupe_checkAdmin.enabled=false;			
			if(loginService.objGroupeClient.DROIT_GESTION_FOURNIS == "1"){
				groupe_checkAdmin.selected = true;
			}
			else
			{
				groupe_checkAdmin.selected = false;
			}
			
			
			//Gauche

		}
	}
}