package univers.parametres.parametrecompte.ihm
{
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import multizone.utils.Formator;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import univers.parametres.parametrecompte.event.EventData;
	import univers.parametres.parametrecompte.util.LibellesPersos;
	import univers.parametres.parametrecompte.vo.Matricule;
	
	[Bindable]
	public class PersonnalisationLibelle extends VBox
	{
		public var btAppliquer:Button;
		public var libelle1:TextInputLabeled;
		public var libelle2:TextInputLabeled;
		public var libelle3:TextInputLabeled;
		public var libelle4:TextInputLabeled;
		public var libelle5:TextInputLabeled;
		public var libelle6:TextInputLabeled;
		public var libelle7:TextInputLabeled;
		public var libelle8:TextInputLabeled;
		
		public var tbxPrefixe	:TextInput;
//		public var tbxDigit		:TextInput;
		
		public var ckxAuto		:CheckBox;
		
		public var cbxDigit		:ComboBox;

		private var _libellesPersos:LibellesPersos;
		
		public var digitNumber	:ArrayCollection = new ArrayCollection([
																			{libelle:'3', nb:3},
																			{libelle:'4', nb:4},
																			{libelle:'5', nb:5},
																			{libelle:'6', nb:6},
																		]);
		
		public function PersonnalisationLibelle()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			libellesPersos = new LibellesPersos();
			
			initLibelles();
			initListener();
		}
		
		// INITIALISATION
		
		private function initListener():void
		{
			btAppliquer.addEventListener(MouseEvent.CLICK , btAppliquerHandler);
			
			libellesPersos.addEventListener(LibellesPersos.LIBELLES_COLLAB_LOADED, treatLibellesCollabLoaded);
			libellesPersos.addEventListener(LibellesPersos.LIBELLES_LIGNE_LOADED, treatLibellesLigneLoaded);
			libellesPersos.addEventListener('GET_INFOS_MATRICULE', infosMatriculeHandler);
		}
		
		private function initLibelles():void
		{
			libellesPersos.getLibellesPersosCollab();
			libellesPersos.getLibellesPersosLigne();
			libellesPersos.getInfosMatriculeAuto();
		}
		
		// HANDLER
		
		private function btAppliquerHandler(me:MouseEvent):void
		{
			var c1 :  String = (libelle1 != null)?libelle1.text+" :":ResourceManager.getInstance().getString('M21', 'Libell__1__');
			var c2 :  String = (libelle2 != null)?libelle2.text+" :":ResourceManager.getInstance().getString('M21', 'Libell__2__');
			var c3 :  String = (libelle3 != null)?libelle3.text+" :":ResourceManager.getInstance().getString('M21', 'Libell__3__');
			var c4 :  String = (libelle4 != null)?libelle4.text+" :":ResourceManager.getInstance().getString('M21', 'Libell__4__');
			var c5 :  String = (libelle5 != null)?libelle5.text+" :":ResourceManager.getInstance().getString('M21', 'Libell__1__');
			var c6 :  String = (libelle6 != null)?libelle6.text+" :":ResourceManager.getInstance().getString('M21', 'Libell__2__');
			var c7 :  String = (libelle7 != null)?libelle7.text+" :":ResourceManager.getInstance().getString('M21', 'Libell__3__');
			var c8 :  String = (libelle8 != null)?libelle8.text+" :":ResourceManager.getInstance().getString('M21', 'Libell__4__');
			
			if(ckxAuto.selected)
				libellesPersos.updateAllLibelles(c1,c2,c3,c4,c5,c6,c7,c8, ckxAuto.selected, tbxPrefixe.text, cbxDigit.selectedItem.nb, _matriculeInfos.LAST_VALUE);
			else
			{
				var pre	:String = 'MAT';
				
				if(tbxPrefixe.text.length > 2)
					pre = tbxPrefixe.text;
					
				libellesPersos.updateAllLibelles(c1,c2,c3,c4,c5,c6,c7,c8, ckxAuto.selected, pre,  cbxDigit.selectedItem.nb, _matriculeInfos.LAST_VALUE);
			}
		}
		
		private function treatLibellesCollabLoaded(evtData : EventData):void
		{
			if(evtData.objData)
			{
				libelle1.text = (evtData.objData[0] != null)?(evtData.objData[0] as String).substr(0,(evtData.objData[0] as String).length-2):ResourceManager.getInstance().getString('M21', 'Libell__1__');
				libelle2.text = (evtData.objData[1] != null)?(evtData.objData[1] as String).substr(0,(evtData.objData[1] as String).length-2):ResourceManager.getInstance().getString('M21', 'Libell__2__');
				libelle3.text = (evtData.objData[2] != null)?(evtData.objData[2] as String).substr(0,(evtData.objData[2] as String).length-2):ResourceManager.getInstance().getString('M21', 'Libell__3__');
				libelle4.text = (evtData.objData[3] != null)?(evtData.objData[3] as String).substr(0,(evtData.objData[3] as String).length-2):ResourceManager.getInstance().getString('M21', 'Libell__4__');
			}
		}

		private function treatLibellesLigneLoaded(evtData : EventData):void
		{
			if(evtData.objData)
			{
				libelle5.text = (evtData.objData[0] != null)?(evtData.objData[0] as String).substr(0,(evtData.objData[0] as String).length-2):ResourceManager.getInstance().getString('M21', 'Libell__1__');
				libelle6.text = (evtData.objData[1] != null)?(evtData.objData[1] as String).substr(0,(evtData.objData[1] as String).length-2):ResourceManager.getInstance().getString('M21', 'Libell__2__');
				libelle7.text = (evtData.objData[2] != null)?(evtData.objData[2] as String).substr(0,(evtData.objData[2] as String).length-2):ResourceManager.getInstance().getString('M21', 'Libell__3__');
				libelle8.text = (evtData.objData[3] != null)?(evtData.objData[3] as String).substr(0,(evtData.objData[3] as String).length-2):ResourceManager.getInstance().getString('M21', 'Libell__4__');
			}
		}
		
		private var _matriculeInfos:Matricule;
		
		private function infosMatriculeHandler(e:Event):void
		{
			_matriculeInfos = new Matricule();
			_matriculeInfos = libellesPersos.myMatriculeInfos;
			
			ckxAuto.selected 		= Formator.convertIntToBool(libellesPersos.myMatriculeInfos.IS_ACTIF);
			
			tbxPrefixe.text  		= libellesPersos.myMatriculeInfos.PREFIXE;
			
			cbxDigit.selectedIndex 	= getDigitNumber(libellesPersos.myMatriculeInfos.NB_DIGIT);
		}
		
		private function getDigitNumber(max:int):int
		{
			var index:int = 1;
			
			switch(max)
			{
				case 3 :  index = 0; break;
				case 4 :  index = 1; break;
				case 5 :  index = 2; break;
				case 6 :  index = 3; break;
				default : index = 1; break;
			}
			
			return index;
		}
		
		// GETTER / SETTER
		
		public function set libellesPersos(value:LibellesPersos):void
		{
			_libellesPersos = value;
		}

		public function get libellesPersos():LibellesPersos
		{
			return _libellesPersos;
		}
		
		public var PREFIXE	:String = 'MAT';
		public var DIGIT	:String = 'XXX';
		
		
	}
}