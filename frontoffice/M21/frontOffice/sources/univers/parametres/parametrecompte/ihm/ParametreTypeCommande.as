package univers.parametres.parametrecompte.ihm
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.net.registerClassAlias;
	
	import mx.containers.TitleWindow;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.event.GestionTypeCommandeEvent;
	import univers.parametres.parametrecompte.service.TypeCommandeService;
	import univers.parametres.parametrecompte.vo.TypeCommande;
	[Bindable]
	public class ParametreTypeCommande extends TitleWindow
	{
		private var groupeIndex : int;
		public var typeCommande : TypeCommande = new TypeCommande();
		public var typeCommandeService : TypeCommandeService= new TypeCommandeService();
		public var boolReadOnly:Boolean = false
		
		
		//UI COMPONANT
		public var cbAll : CheckBox;
		public var comboPays : ComboBox;
		public var combo_revendeur_commande : ComboBox;
		
		public function ParametreTypeCommande()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		public function init(evt : FlexEvent):void
		{
			if(typeCommande.idTypeCmd>0)
			{
				initMode_updateSite();
			}
			else
			{
				initMode_createSite();
			}
		}
		private function initMode_createSite():void
		{
			//Aucune modif à faire en mode création
		}
		private function initMode_updateSite():void
		{
			registerClassAlias("univers.parametres.parametrecompte.vo.TypeCommande",TypeCommande);  
			typeCommande= TypeCommande(ObjectUtil.copy(typeCommande))  // delete les ref sur le grid
			initAttribut();
		}
		private function initAttribut():void
		{
			//typeCommandeService.getPoolOfTypeCommande(groupeIndex,typeCommande.idTypeCmd);
		}
		public function valider():void
		{
			if(boolReadOnly)
			{
				PopUpManager.removePopUp(this)
			}
			else
			{
				typeCommandeService.addEventListener(GestionTypeCommandeEvent.UPDATE_INFO_TYPE_CMD_COMPLETE,update_type_cmd_complete);
				var bfixe:int = 0 
				var bmobile:int = 0
				var breseau:int = 0
				if(typeCommande.segmentFixe==1)
					bfixe = 1
				if(typeCommande.segmentMobile==1)
					bmobile = 1
				if(typeCommande.segmentReseau==1)
					breseau = 1
				
				if(typeCommande.idTypeCmd>0)
				{
					typeCommandeService.updateInfoTypeCommande(typeCommande.idTypeCmd,typeCommande.libelleTypeCmd,typeCommande.codeTypeCmd,typeCommande.commentaire,bfixe,bmobile,breseau);
				}
				else
				{
					typeCommandeService.addTypeCommande(groupeIndex,typeCommande.libelleTypeCmd,typeCommande.codeTypeCmd, typeCommande.commentaire,bfixe,bmobile,breseau);
				}
			}
		}
		private function update_type_cmd_complete(evt : GestionTypeCommandeEvent):void
		{
			dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.PARAMETRE_TYPE_CMD_COMPLETE));
			var msg : String;
			if(typeCommande.idTypeCmd>0)
			{
				msg = ResourceManager.getInstance().getString('M21', 'Les_modifications_ont_bien__t__enregistr');
			}
			else
			{
				msg = ResourceManager.getInstance().getString('M21', 'La_cr_ation_du_type_de_commande_a_bien__');
			}
			ConsoviewAlert.afficherOKImage(msg);
			PopUpManager.removePopUp(this);
		}
		private function parametrePoolCOmpleteHandler(evt : GestionPoolEvent):void
		{
			initAttribut();
		}
		public function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
	
	}
}