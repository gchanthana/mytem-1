package univers.parametres.parametrecompte.ihm
{
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.DataGrid;
	
	import univers.parametres.parametrelogin.vo.ChampsPersosVO;
	
	
	[Bindable]
	public class FicheECFixeReseauImpl extends VBox
	{
		public var DATAPROVIDER:ArrayCollection = new ArrayCollection();
		private var listeID:ArrayCollection = new ArrayCollection([1,3,5,7,9,11,14,21,28]);
		public function FicheECFixeReseauImpl()
		{
			super();
		}
		public function init():void
		{
		}
		 public function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint
         {
            var rColor:uint;
            var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
            var obj:int = listeID.getItemIndex((item as ChampsPersosVO).ordreAffichage)
            if(obj > -1)
            	rColor = 0xFFFF99
            else
            	rColor = color
            return rColor;
         }
         public function Reset(cp:ChampsPersosVO):void
     	{
     		cp.CP_Libelle_Client = ""
     		DATAPROVIDER.refresh()
     	}
	}
}