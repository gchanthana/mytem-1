package univers.parametres.parametrecompte.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import univers.parametres.parametrecompte.event.ProductGLCodeEvent;
	import univers.parametres.parametrecompte.ihm.popup.PopUpEditGLCodeIHM;
	import univers.parametres.parametrecompte.service.glcode.ProductGLCodeCodeService;
	import univers.parametres.parametrecompte.vo.ProductGLCodeVO;
	
	public class GLCodeImpl extends VBox
	{
		private var _serviceProductGLCode	:ProductGLCodeCodeService;
		private var _listeProduitGLCode		:ArrayCollection;
		private var _nbreSelectedElements	:int;
		public var ti_filtre				:TextInput;
		public var dg_products				:DataGrid;
		public var comboTypeTheme			:ComboBox;
		public var comboSegment				:ComboBox;
		public var cbx_allItems				:CheckBox;
		public var img_refresh				:Image;
		public var btn_affect				:Button;
		public var dgc_cgl					:DataGridColumn;
		
		public function GLCodeImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		protected function creationCompleteHandler(event:FlexEvent):void
		{
			serviceProductGLCode = new ProductGLCodeCodeService();
			initListeners();
			initData();
		}
		
		private function initData():void
		{
			getListProductCode();
		}
		
		private function getListProductCode():void
		{
			serviceProductGLCode.getListProductCode();
			
		}
		
		private function initListeners():void
		{
			serviceProductGLCode.myDatas.addEventListener(ProductGLCodeEvent.LISTE_PRODUCT_GL_CODE, getListeProduitGLCode);
			
			addEventListener(ProductGLCodeEvent.COUNT_SELECTED,		 countSelectedItemsHandler);

			cbx_allItems.addEventListener(MouseEvent.CLICK, 		selectAllElementssHandler);
			img_refresh.addEventListener(MouseEvent.CLICK, 			imgActualiserHandler);
			btn_affect.addEventListener(MouseEvent.CLICK, 			affectGLCodeClickHandler);
			
			comboSegment.addEventListener(Event.CHANGE, 			onChangeFiltertHandler);
			comboTypeTheme.addEventListener(Event.CHANGE, 			onChangeFiltertHandler);
			ti_filtre.addEventListener(Event.CHANGE, 				onChangeFiltertHandler);
				
		}
		
		protected function countSelectedItemsHandler(event:ProductGLCodeEvent):void
		{
			calculerNbreSelectedElement();
			var taille:int = listeProduitGLCode.length;
			if(nbreSelectedElements < taille)
				cbx_allItems.selected = false;
		}
		
		private function calculerNbreSelectedElement():void
		{
			var ns:int = 0;
			
			for each (var prodVo:ProductGLCodeVO in listeProduitGLCode.source) 
			{
				if(prodVo.SELECTED == true)
					ns ++;
			}
			
			nbreSelectedElements = ns;
		}
		
		protected function selectAllElementssHandler(event:MouseEvent):void
		{
			var ns:int = 0;
			var taille:int = listeProduitGLCode.length
			for each (var prodVo:ProductGLCodeVO in listeProduitGLCode.source) 
			{
				prodVo.SELECTED = cbx_allItems.selected;
				if(prodVo.SELECTED)
					ns ++;
			}
			
			this.nbreSelectedElements = ns;
			if(cbx_allItems.selected == true)
			{
				serviceProductGLCode.myDatas.addEventListener(ProductGLCodeEvent.EDITION_PRODUCT_GL_CODE, editGLCodeHandler);
			}
			else if(cbx_allItems.selected == false)
			{
				serviceProductGLCode.myDatas.removeEventListener(ProductGLCodeEvent.EDITION_PRODUCT_GL_CODE, editGLCodeHandler);
			}
		}
		
		protected function editGLCodeHandler(event:Event):void
		{
			imgActualiserHandler(null);
			serviceProductGLCode.myDatas.removeEventListener(ProductGLCodeEvent.EDITION_PRODUCT_GL_CODE, editGLCodeHandler);
		}
		
		protected function getListeProduitGLCode(event:ProductGLCodeEvent):void
		{
			listeProduitGLCode = serviceProductGLCode.myDatas.listGLCodeProduct;
			
			comboTypeTheme.dataProvider = [{typeTheme:resourceManager.getString('M21','no_filter')}].concat(serviceProductGLCode.myDatas.allTypeThemes.source);
			
			comboSegment.dataProvider = [{segmentTheme:resourceManager.getString('M21','no_filter')}].concat(serviceProductGLCode.myDatas.allSegments.source);
			
			calculerNbreSelectedElement();
		}
		
		
		private function filterByComboSegment(item:Object):Boolean
		{
			var ret:Boolean = true;
			
			if(comboSegment.selectedIndex != -1 && item.segmentTheme != comboSegment.selectedLabel )
			{
				return false;
			}
			
			return ret;
		}
		
		private function filterByComboTypeTheme(item:Object):Boolean
		{
			var ret:Boolean = true;
			
			if(comboTypeTheme.selectedIndex != -1 && item.typeTheme != comboTypeTheme.selectedLabel)
			{
				return false;
			}
			
			return ret;
		}
		
		private function filterByText(item:Object):Boolean
		{
			var ret:Boolean = false;
			
			if (  (item.segmentTheme != null && (item.segmentTheme as String).toLowerCase().search((ti_filtre.text).toLowerCase()) != -1)
				||(item.typeTheme != null && (item.typeTheme as String).toLowerCase().search((ti_filtre.text).toLowerCase()) != -1)
				||(item.libelleTheme != null && (item.libelleTheme as String).toLowerCase().search((ti_filtre.text).toLowerCase()) != -1)
				||(item.libelleGLCode != null && (item.libelleGLCode as String).toLowerCase().search((ti_filtre.text).toLowerCase()) != -1)
			)
			{
				ret =  true;
			}
				
			
			
			return ret;
		}
		
		private function dofilter(item:Object):Boolean
		{
			var ret:Boolean = filterByComboSegment(item) && filterByComboTypeTheme(item) && filterByText(item);
			
			return ret;
		}
		
		private function onChangeFiltertHandler(event:Event):void
		{
			if(comboSegment.selectedIndex == 0)
			{
				comboSegment.selectedIndex = -1
			}
			if(comboTypeTheme.selectedIndex == 0)
			{
				comboTypeTheme.selectedIndex = -1
			}
			
			listeProduitGLCode.filterFunction = dofilter;
			listeProduitGLCode.refresh();
		}

		
		
		protected function imgActualiserHandler(event:MouseEvent):void
		{
			// texte de recherche vide
			ti_filtre.text = '';
			//déselect all
			cbx_allItems.selected = false;
			
			// init nombre d'élements sélectionnés 
			nbreSelectedElements = 0;
			// init des combo
			comboTypeTheme.selectedIndex = -1;
			comboSegment.selectedIndex = -1;
			// supprimer les filtres dans les combo et texte de recherche
			
			listeProduitGLCode.filterFunction = null;
			listeProduitGLCode.refresh();
			// appel du service pour recupérer la liste initiale
			getListProductCode();
		}
		
		protected function affectGLCodeClickHandler(event:MouseEvent):void
		{
			if(nbreSelectedElements > 1)
			{
				var popup:PopUpEditGLCodeIHM	= new PopUpEditGLCodeIHM();
				popup.serviceProductGLCode 		= this.serviceProductGLCode;
				popup.selectedElemnts		 	= this.getSelectedElements(); 
				popup.x = 200;	popup.y = 300;
				PopUpManager.addPopUp(popup, this, true);
				PopUpManager.bringToFront(popup);
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo('Affect GL code to selected items: \nSelect at least two items', 
					ResourceManager.getInstance().getString('M21','Alerte'), null);
			}
		}
		
		private function getSelectedElements():ArrayCollection
		{
			var selectedElements:ArrayCollection = new ArrayCollection();
			var taille:int = listeProduitGLCode.length;
			for each (var prodVo:ProductGLCodeVO in listeProduitGLCode.source) 
			{
				if(prodVo.SELECTED)
					selectedElements.addItem(prodVo);
				
			}
			return selectedElements; 
		}
		
		[Bindable]
		public function get listeProduitGLCode():ArrayCollection { return _listeProduitGLCode; }
		
		public function set listeProduitGLCode(value:ArrayCollection):void
		{
			if (_listeProduitGLCode == value)
				return;
			_listeProduitGLCode = value;
		}
		
		[Bindable]
		public function get nbreSelectedElements():int { return _nbreSelectedElements; }
		
		public function set nbreSelectedElements(value:int):void
		{
			if (_nbreSelectedElements == value)
				return;
			_nbreSelectedElements = value;
		}

		public function get serviceProductGLCode():ProductGLCodeCodeService
		{
			return _serviceProductGLCode;
		}

		public function set serviceProductGLCode(value:ProductGLCodeCodeService):void
		{
			_serviceProductGLCode = value;
		}
		
		protected function themeCompareFunc(obj1:Object, obj2:Object):int
		{
			return ObjectUtil.stringCompare(obj1.libelleTheme, obj2.libelleTheme, true);
			
		}
	}
}