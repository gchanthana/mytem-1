package univers.parametres.parametrecompte.ihm
{
	import mx.containers.VBox;
	import mx.controls.DataGrid;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrecompte.event.ValidationFacturationEvent;
	import univers.parametres.parametrecompte.service.GestionValidationFacturationService;
	import univers.parametres.parametrecompte.vo.ProfilValidationFacturation;

	public class FicheProfilValidationFacturationImpl extends VBox
	{
		[Bindable] public var vfservice:GestionValidationFacturationService
		public var dg_profil:DataGrid
		
		public function FicheProfilValidationFacturationImpl()
		{
			super();
			vfservice = new GestionValidationFacturationService()
		}
	// CREATION COMPLETE HANDLER
		public function init():void
		{
		}
		public function initData():void
		{
			vfservice.getListeProfilValidationFacturation()
		}
	// CREATION D'UN NOUVEAU PROFIL	
		public function newProfil():void
		{
			var popup:ParametreProfilValidationFacturationIHM = new ParametreProfilValidationFacturationIHM()
			popup.boolReadOnly = false
			popup.boolNewProfil = true
			popup.profil = new ProfilValidationFacturation()
			popup.vfservice.addEventListener(ValidationFacturationEvent.CREATE_PROFIL_EVENT,refresh)
			popup.vfservice.addEventListener(ValidationFacturationEvent.MAJ_PROFIL_EVENT,refresh)
			PopUpManager.addPopUp(popup,this)
			PopUpManager.centerPopUp(popup)
		}
	// VOIR LES USERS 
		public function btNbUserHandler(profil:ProfilValidationFacturation):void
		{
			var popUpUserOfProfilIHM:ListeUtilisateurProfilValidationFacturationIHM = new ListeUtilisateurProfilValidationFacturationIHM();
			popUpUserOfProfilIHM.profil = dg_profil.selectedItem as ProfilValidationFacturation;
			PopUpManager.addPopUp(popUpUserOfProfilIHM,this)
			PopUpManager.centerPopUp(popUpUserOfProfilIHM)
		}	
	// MODIFIER UN PROGIL GESTION DE PARC
		public function modifier(profil:ProfilValidationFacturation):void
		{
			var popup:ParametreProfilValidationFacturationIHM = new ParametreProfilValidationFacturationIHM()
			popup.boolReadOnly = false
			popup.boolNewProfil = false
			popup.profil = dg_profil.selectedItem as ProfilValidationFacturation
			popup.vfservice.addEventListener(ValidationFacturationEvent.CREATE_PROFIL_EVENT,refresh)
			popup.vfservice.addEventListener(ValidationFacturationEvent.MAJ_PROFIL_EVENT,refresh)
			PopUpManager.addPopUp(popup,this)
			PopUpManager.centerPopUp(popup)
		}
	// SUPPRIMER UN PROFIL
		public function supprimer(profil:ProfilValidationFacturation):void
		{
			vfservice.addEventListener(ValidationFacturationEvent.SUPPRIMER_PROFIL_EVENT,refresh)
			vfservice.SupprimerProfilValidationFacturation(profil.idProfil)
		}
	// REFRESH
		public function refresh(e:ValidationFacturationEvent):void
		{
			vfservice.getListeProfilValidationFacturation()
		}
	}
}