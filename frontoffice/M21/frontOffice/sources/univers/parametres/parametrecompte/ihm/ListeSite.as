package univers.parametres.parametrecompte.ihm
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.RadioButton;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.parametres.parametrecompte.event.ContrainteSuppressionEvent;
	import univers.parametres.parametrecompte.event.GestionSiteEvent;
	import univers.parametres.parametrecompte.service.SiteService;
	import univers.parametres.parametrecompte.vo.Site;
	
	[Bindable]
	public class ListeSite extends VBox
	{
		public var siteService : SiteService = new SiteService();
		
		
		//COMPONANT
		public var dg_site: DataGrid;
		private var popupParamSite : ParametreSiteIHM;
		private var popContrainteSuppressionIHM : ContrainteSuppressionIHM;
	
		
		public var radioAll : RadioButton;
		public var radioNotAll : RadioButton;
		
		
		public function ListeSite()
		{
			
		}
		public function init(evt : FlexEvent):void
		{
			siteService.listeSite(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,null,0);
		}
		public function modifier(site : Site):void
		{
			popupParamSite = new ParametreSiteIHM();
			popupParamSite.site = site;;
			addPop();
		}
		public function addSite(event:MouseEvent):void
		{
			
			popupParamSite = new ParametreSiteIHM();
			addPop();
		}
		

		private function addPop():void
		{
			popupParamSite.addEventListener(GestionSiteEvent.PARAMETRE_SITE_COMPLETE,parametreSiteCompleteHandler);
			PopUpManager.addPopUp(popupParamSite,this,true);
			PopUpManager.centerPopUp(popupParamSite);
			
		}
		public function filtre():void
		{
			var allResult : int;
			if(radioAll.selected)
			{
				allResult = 1;
			}
			else if(radioNotAll.selected)
			{
				allResult = 0
			}
			siteService.listeSite(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,null,allResult);
		}
		private function parametreSiteCompleteHandler(evt : GestionSiteEvent):void
		{
			filtre();
		}
		public function supprimer(data : Site):void
		{
			//ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M21', '_tes_vous_s_r_de_vouloir_supprimer_le_si')+data.libelle_site+ResourceManager.getInstance().getString('M21', '__'),ResourceManager.getInstance().getString('M21', 'Confirmation'),supprimerBtnValiderCloseEvent);
				siteService.addEventListener(GestionSiteEvent.LISTE_CONTRAINTE_SITE_COMPLETE,listeContrainteOfSiteHandler);
				siteService.listeContrainteOfSite((dg_site.selectedItem as Site).id_site);
		}

		/*private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				siteService.addEventListener(GestionSiteEvent.LISTE_CONTRAINTE_SITE_COMPLETE,listeContrainteOfSiteHandler);
				siteService.listeContrainteOfSite((dg_site.selectedItem as Site).id_site);
			}
		}*/
		private function listeContrainteOfSiteHandler(evt : GestionSiteEvent):void
		{
			if(siteService.col_contrainte_of_site.length>0) // il existe des contraintes qui empêche la suppression
			{
				popContrainteSuppressionIHM = new ContrainteSuppressionIHM();
				popContrainteSuppressionIHM.addEventListener(ContrainteSuppressionEvent.VALIDATE_SUPPRESSION_POOL,supprimerSiteSelected);
				popContrainteSuppressionIHM.listeContraintes = siteService.col_contrainte_of_site;
				if(siteService.col_contrainte_of_site.length>1)
				{
					popContrainteSuppressionIHM.messageDecription = 'Des éléments sont rattachés au site '+(dg_site.selectedItem as Site).libelle_site+'. La suppression de ce site entrainera la suppression de ces liens.';
				}
				else
				{
					popContrainteSuppressionIHM.messageDecription = 'Un élément est rattaché au site '+(dg_site.selectedItem as Site).libelle_site+'. La suppression de ce site entrainera la suppression de ce lien.';
				}
				
				popContrainteSuppressionIHM.messageConfirmation = 'Êtes vous sûr de vouloir supprimer le site ?';
				PopUpManager.addPopUp(popContrainteSuppressionIHM,this,true);
				PopUpManager.centerPopUp(popContrainteSuppressionIHM);
			}
			else // pas de contrainte on peut supprimer
			{
				supprimerSiteSelected();
			}
		}
		private function supprimerSiteSelected(evt : ContrainteSuppressionEvent=null):void
		{
			//TODO REQUETE DE SUPPRESSION
			siteService.addEventListener(GestionSiteEvent.REMOVE_SITE_COMPLETE,remove_complete_handler);
			siteService.removeSite((dg_site.selectedItem as Site).id_site);
			
		}
		private function remove_complete_handler(evt : GestionSiteEvent=null):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'le_site_')+(dg_site.selectedItem as Site).libelle_site+ResourceManager.getInstance().getString('M21', '_a_bien__t__supprim__'));
			filtre();
		}
		
		public static function formatBoolToInt(value:Boolean):int
		{
			var boolFormated:int = -1;
			
			if(value)
				boolFormated = 1;
			else
				boolFormated = 0;
			
			return boolFormated;
		}
		
		public var chbxLivAll	:CheckBox;
		public var chbxFacAll	:CheckBox;
		
		
		protected function chbxLivAllChangeHandler(e:Event):void
		{
			if(chbxLivAll.selected)
			{
				 sendMajAllSite(0,1);
			}
			else
			{
				 sendMajAllSite(0,0);
			}
		}
		
		protected function chbxFacAllChangeHandler(e:Event):void
		{
			if(chbxFacAll.selected)
			{
				 sendMajAllSite(1,1);
			}
			else
			{
				 sendMajAllSite(1,0);
			}
		}
		
		private function sendMajAllSite(isLivraison:int, isActive:int):void
		{
			var listeSites:Array = formatSiteVoInObject(dg_site.dataProvider as ArrayCollection);
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M21.Site",
																				"updateSites", 
																				sendMajSiteResultHandler);
			RemoteObjectUtil.callService(op,listeSites, 
											isLivraison, 
											isActive);
		}
		
		private function formatSiteVoInObject(value:ArrayCollection):Array
		{
			var listeSites	:ArrayCollection = new ArrayCollection();
			var len			:int 			 = value.length;
			
			for(var i:int = 0;i < len;i++)
			{
				listeSites.addItem(ObjectUtil.copy(value[i] as Object));
			}
			
			return listeSites.source;
		}
		
		private function sendMajSiteResultHandler(re:ResultEvent):void
		{
			var OK:Boolean = true;
			
			if(re.result as Array)
			{
				var value	:Array 				= re.result as Array;
				var site	:ArrayCollection 	= dg_site.dataProvider as ArrayCollection
				var len		:int 				= value.length;
				
				if(len > 0)
				{
					for(var i:int = 0;i < len;i++)
					{
						if(value[i] < 0)
						{
							OK = false;
						}
					}
				}
				
				if(OK)
					ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Modifi_'), this);
				else
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M21', 'Tous_les_sites_n_ont_pas_pu__tre_mis___j'), ResourceManager.getInstance().getString('M21', 'Consoview'));
			}
			else
			{
				OK = false;
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M21', 'Tous_les_sites_n_ont_pas_pu__tre_mis___j'), ResourceManager.getInstance().getString('M21', 'Consoview'));
			}

			siteService.listeSite(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX, null, 0);
		}
		
	}
}