package univers.parametres.parametrecompte.service.glcode
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	public class ProductGLCodeCodeService
	{
		[Bindable]
		public var myDatas 				: ProductGLCodeDatas;
		public var myHandlers 			: ProductGLCodeHandlers;
		
		public var _CfcGestionClient	:String ="fr.consotel.consoview.M21.ProductCodeGLService";
		
		public function ProductGLCodeCodeService()
		{
			myDatas=new ProductGLCodeDatas();
			myHandlers=new ProductGLCodeHandlers(myDatas);
		}
		
		public function getListProductCode():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_CfcGestionClient,
				"getListProductGLCode",
				myHandlers.getListProductCodeHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		public function affecterGLCode(idTheme:Number, glCode:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_CfcGestionClient,
				"affectProductGlCode",
				myHandlers.affecterProductCodeHandler);
			RemoteObjectUtil.callSilentService(op, idTheme, glCode);
		}
		
		public function affecterPlusieursElements(selectedIDThemes:Array, enteredGlCode:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_CfcGestionClient,
				"affectManyGLCode",
				myHandlers.affecterPlusieursElementsHandler);
			
			RemoteObjectUtil.callSilentService(op,enteredGlCode, selectedIDThemes);
		}
	}
}