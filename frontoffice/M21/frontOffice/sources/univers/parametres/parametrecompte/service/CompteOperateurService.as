package univers.parametres.parametrecompte.service
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.vo.CompteOperateur;

	[Bindable]
	public class CompteOperateurService extends EventDispatcher
	{
		public var col_compteOP_compte:ArrayCollection=new ArrayCollection();
		public var col_compteOP_compte_orig:ArrayCollection=new ArrayCollection();
		public var col_compteOP_login:ArrayCollection=new ArrayCollection();
		public var col_compteOP_login_orig:ArrayCollection=new ArrayCollection();
		public var col_compteOP_pool:ArrayCollection=new ArrayCollection();
		public var col_compteOP_pool_orig:ArrayCollection=new ArrayCollection();

		//public var tab_idCompteOP_compte : ArrayCollection = new ArrayCollection();

		public var nbCompteOpSelected_Compte:Number=0;
		public var nbCompteOpSelected_Login:Number=0;
		public var nbCompteOpSelected_Pool:Number=0;

		public var col_operateur:ArrayCollection=new ArrayCollection();

		public function CompteOperateurService()
		{
		}

		public function getCompteOpeCompte(idCompte:int, idOPerateur:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService", "getCompteOpeCompte", getCompteOpeCompte_handler, getCompteOpeCompte_fault_handler);
			RemoteObjectUtil.callService(op, idCompte, idOPerateur);
			//RemoteObjectUtil.callService(op);
		}

		public function fournirListeOperateursClient(idRacine:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService", "getOperateursActif", fournirListeOperateursClient_handler, fournirListeOperateursClient_fault_handler);

			RemoteObjectUtil.callService(op, idRacine);
		}

		public function getCompteOpeLogin(idLogin:int, idOPerateur:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService", "getCompteOpeLogin", getCompteOpeLogin_handler, getCompteOpeLogin_fault_handler);
			RemoteObjectUtil.callService(op, idLogin, idOPerateur);

		}

		public function getCompteOpePool(idPool:int, idOPerateur:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService", "getCompteOpePool", getCompteOpePool_handler, getCompteOpePool_fault_handler);
			RemoteObjectUtil.callService(op, idPool, idOPerateur);

		}

		//-----Maj 
		public function updateXCompteOpeCompte(idGroupe:int, tabIdCompteOP:Array, value:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService", "updateXCompteOpeCompte", updateXCompteOpeCompte_handler, updateXCompteOpeCompte_fault_handler);
			RemoteObjectUtil.callService(op, idGroupe, tabIdCompteOP, value);
		}

		public function updateXCompteOpeLogin(idLogin:int, tabIdCompteOP:Array, value:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService", "updateXCompteOpeLogin", updateXCompteOpeLogin_handler, updateXCompteOpeLogin_fault_handler);
			RemoteObjectUtil.callService(op, idLogin, tabIdCompteOP, value);
		}

		public function updateXCompteOpePool(idPool:int, tabIdCompteOP:Array, value:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService", "updateXCompteOpePool", updateXCompteOpePool_handler, updateXCompteOpePool_fault_handler);
			RemoteObjectUtil.callService(op, idPool, tabIdCompteOP, value);
		}

		private function fournirListeOperateursClient_handler(re:ResultEvent):void
		{
			if (col_operateur)
			{
				col_operateur=null;
			}

			col_operateur=new ArrayCollection();

			if (re.result)
			{
				var object:Object;
				var cursor:IViewCursor=(re.result as ArrayCollection).createCursor();

				while (!cursor.afterLast)
				{
					object={LIBELLE: cursor.current.NOM, NOM: cursor.current.NOM, NOM_OP: cursor.current.NOM, OPERATEURID: cursor.current.OPERATEURID}
					col_operateur.addItem(object);
					cursor.moveNext();
				}
				if(col_operateur.length==1){
					dispatchEvent(new GestionPoolEvent(GestionPoolEvent.LISTE_COMPTE_OPERATEUR_CHOIX_UNIQUE_COMPLETE));
				}
			}
		}

		private function updateXCompteOpeCompte_handler(re:ResultEvent):void
		{}

		private function updateXCompteOpeLogin_handler(re:ResultEvent):void
		{}

		private function updateXCompteOpePool_handler(re:ResultEvent):void
		{}

		private function getCompteOpeCompte_handler(re:ResultEvent):void
		{
			var tmpArr:Array=(re.result as ArrayCollection).source;
			var i:int;
			col_compteOP_compte_orig=new ArrayCollection();
			col_compteOP_compte=new ArrayCollection();
			var libelleCompteMaitre:String;
			var idLastMaitre:int=0;

			for (i=0; i < tmpArr.length; i++)
			{
				var item:CompteOperateur=new CompteOperateur();
				item.idCompteOP=tmpArr[i].IDCOMPTE_FACTURATION;
				item.idSousCompteOP=tmpArr[i].IDSOUS_COMPTE;
				item.libelle=tmpArr[i].LABEL;

				if (tmpArr[i].SELECTED == '1')
				{
					item.boolSelected=true;
				}

				if (libelleCompteMaitre != tmpArr[i].COMPTE_FACTURATION)
				{
					var compteMaitre:CompteOperateur=new CompteOperateur();
					libelleCompteMaitre=tmpArr[i].COMPTE_FACTURATION;
					compteMaitre.libelle=tmpArr[i].COMPTE_FACTURATION;
					compteMaitre.idCompteOP=tmpArr[i].IDCOMPTE_FACTURATION;
					compteMaitre.idSousCompteOP=tmpArr[i].IDSOUS_COMPTE;
					compteMaitre.isCompteMaitre=true;
					compteMaitre.boolSelected=true;
					col_compteOP_compte.addItem(compteMaitre);
					idLastMaitre=col_compteOP_compte.getItemIndex(compteMaitre);
				}

				if (!item.boolSelected)
				{
					col_compteOP_compte.getItemAt(idLastMaitre).boolSelected=false;
				}

				col_compteOP_compte.addItem(item);
				col_compteOP_compte_orig.addItem(item);
			}
			refreshTheNbSelected_OP();
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.LISTE_COMPTE_OPERATEUR_COMPLETE));
		}

		private function getCompteOpeLogin_handler(re:ResultEvent):void
		{
			var tmpArr:Array=(re.result as ArrayCollection).source;
			var i:int;

			col_compteOP_login=new ArrayCollection();
			col_compteOP_login_orig=new ArrayCollection();

			var libelleCompteMaitre:String;
			var idLastMaitre:int=0;

			for (i=0; i < tmpArr.length; i++)
			{
				var item:CompteOperateur=new CompteOperateur();
				item.idCompteOP=tmpArr[i].IDCOMPTE_FACTURATION;
				item.idSousCompteOP=tmpArr[i].IDSOUS_COMPTE;
				item.libelle=tmpArr[i].LABEL;

				if (tmpArr[i].SELECTED == '1')
				{
					item.boolSelected=true;
				}

				if (libelleCompteMaitre != tmpArr[i].COMPTE_FACTURATION)
				{
					var compteMaitre:CompteOperateur=new CompteOperateur();
					libelleCompteMaitre=tmpArr[i].COMPTE_FACTURATION;
					compteMaitre.libelle=tmpArr[i].COMPTE_FACTURATION;
					compteMaitre.idCompteOP=tmpArr[i].IDCOMPTE_FACTURATION;
					compteMaitre.idSousCompteOP=tmpArr[i].IDSOUS_COMPTE;
					compteMaitre.isCompteMaitre=true;
					compteMaitre.boolSelected=true;
					col_compteOP_login.addItem(compteMaitre);
					idLastMaitre=col_compteOP_login.getItemIndex(compteMaitre);
				}

				if (!item.boolSelected)
				{
					col_compteOP_login.getItemAt(idLastMaitre).boolSelected=false;
				}
				col_compteOP_login_orig.addItem(item);
				col_compteOP_login.addItem(item);
			}
			refreshTheNbSelected_Login();
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.LISTE_COMPTE_OPERATEUR_COMPLETE));
		}

		private function getCompteOpePool_handler(re:ResultEvent):void
		{
			var tmpArr:Array=(re.result as ArrayCollection).source;
			var i:int;
			var libelleCompteMaitre:String;
			var idLastMaitre:int=0;
			col_compteOP_pool=new ArrayCollection();
			col_compteOP_pool_orig=new ArrayCollection();

			for (i=0; i < tmpArr.length; i++)
			{
				var item:CompteOperateur=new CompteOperateur();
				item.idCompteOP=tmpArr[i].IDCOMPTE_FACTURATION;
				item.idSousCompteOP=tmpArr[i].IDSOUS_COMPTE;
				item.libelle=tmpArr[i].LABEL;

				if (tmpArr[i].SELECTED == '1')
				{
					item.boolSelected=true;
				}
				
				if (libelleCompteMaitre != tmpArr[i].COMPTE_FACTURATION)
				{
					var compteMaitre:CompteOperateur=new CompteOperateur();
					libelleCompteMaitre=tmpArr[i].COMPTE_FACTURATION;
					compteMaitre.libelle=tmpArr[i].COMPTE_FACTURATION;
					compteMaitre.idCompteOP=tmpArr[i].IDCOMPTE_FACTURATION;
					compteMaitre.idSousCompteOP=tmpArr[i].IDSOUS_COMPTE;
					compteMaitre.isCompteMaitre=true;
					compteMaitre.boolSelected=true;
					col_compteOP_pool.addItem(compteMaitre);
					idLastMaitre=col_compteOP_pool.getItemIndex(compteMaitre);
				}

				if (!item.boolSelected)
				{
					col_compteOP_pool.getItemAt(idLastMaitre).boolSelected=false;
				}
				col_compteOP_pool_orig.addItem(item);
				col_compteOP_pool.addItem(item);

			}
			refreshTheNbSelected_Pool();
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.LISTE_COMPTE_OPERATEUR_COMPLETE));
		}
		
		public function refreshTheNbSelected_OP():void
		{
			nbCompteOpSelected_Compte=0;
			for each(var tmp:Object in col_compteOP_compte_orig)
			{
				if(tmp.boolSelected)
				{
					nbCompteOpSelected_Compte++;
				}
			} 
		}

		public function refreshTheNbSelected_Login():void
		{
			nbCompteOpSelected_Login=0;
			for each(var tmp:Object in col_compteOP_login_orig)
			{
				if(tmp.boolSelected)
				{
					nbCompteOpSelected_Login++;
				}
			} 
		}

		public function refreshTheNbSelected_Pool():void
		{
			nbCompteOpSelected_Pool=0;
			for each(var tmp:Object in col_compteOP_pool_orig)
			{
				if(tmp.boolSelected)
				{
					
					nbCompteOpSelected_Pool++;
					
				}
			} 
		}

		//----Fault handler
		private function updateXCompteOpeCompte_fault_handler(evt:FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateXCompteOpeCompte_fault_handler' + evt.toString());
		}

		private function updateXCompteOpeLogin_fault_handler(evt:FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateXCompteOpeLogin_fault_handler' + evt.toString());
		}

		private function updateXCompteOpePool_fault_handler(evt:FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateXCompteOpePool_fault_handler' + evt.toString());
		}

		private function getCompteOpeCompte_fault_handler(evt:FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getCompteOpeCompte_fault_handler' + evt.toString());
		}

		private function getCompteOpeLogin_fault_handler(evt:FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getCompteOpeLogin_fault_handler' + evt.toString());
		}

		private function getCompteOpePool_fault_handler(evt:FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getCompteOpePool_fault_handler' + evt.toString());
		}

		private function fournirListeOperateursClient_fault_handler(evt:FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.fournirListeOperateursClient_fault_handler' + evt.toString());
		}

	}
}