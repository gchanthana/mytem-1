package univers.parametres.parametrecompte.service
{
	import composants.util.ConsoviewAlert;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;

	[Bindable]
	public class GestionCollaborateursHandler
	{
		
		private var _model:GestionCollaborateursModel;
		
		public function GestionCollaborateursHandler(model:GestionCollaborateursModel)
		{
			_model = model;
		}
		
		public function get model():GestionCollaborateursModel
		{
			return _model;
		}

		public function set model(value:GestionCollaborateursModel):void
		{
			_model = value;
		}

		public function gestionCollaborateursHandler(event:ResultEvent):void
		{
			if(event.result > 0)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Champs_mis___jour'));				
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'Une_erreur_s_est_produite_lors_de_la_mis'),ResourceManager.getInstance().getString('M21', 'Erreur'));
			}
		}
		
		public function initgestionCollaborateursHandler(event:ResultEvent):void
		{
			_model.updateRacineGestCollab(event.result as ArrayCollection);
		}
		
	}
}