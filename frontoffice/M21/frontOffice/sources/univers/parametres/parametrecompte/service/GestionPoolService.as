package univers.parametres.parametrecompte.service
{
	import mx.resources.ResourceManager;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.vo.OperateurVO;
	import univers.parametres.parametrecompte.vo.PoolGestionnaire;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	[Bindable]
	public class GestionPoolService extends EventDispatcher
	{
		public var col_pool_gestionnaire : ArrayCollection = new ArrayCollection();
		public var col_pool_gestionnaire_of_login : ArrayCollection = new ArrayCollection();
		public var tab_pool_gestionnaire_of_login_selected : ArrayCollection = new ArrayCollection;
		
		public var col_revendeur : ArrayCollection = new ArrayCollection();
		public var col_all_revendeur : ArrayCollection = new ArrayCollection();
		public var col_contrainte_of_pool : ArrayCollection = new ArrayCollection();
		
		public var col_all_profil : ArrayCollection = new ArrayCollection();
		
		public var col_operateur : ArrayCollection = new ArrayCollection();
		public var col_operateur_selected : ArrayCollection = new ArrayCollection();
		public function GestionPoolService()
		{
		}
		public function listePool(groupeIndex:int,recherche : String=''):void
		{
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getPoolGestionnaire",
																		listePool_handler,listePool_fault_handler);
			RemoteObjectUtil.callService(op,groupeIndex,recherche);
		}
		public function listePool_of_login(groupeIndex:int,idLogin : int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getPoolLogin",
																		listePool_of_login_handler,listePool_of_login_fault_handler);
			RemoteObjectUtil.callService(op,groupeIndex,idLogin);
		}
		public function updatePoolOfLogin(idGroupe:int,idLogin:int,tab_idPool : Array,value : int,idprofil:int = -1):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"updateXPoolLogin",
																		updatePoolOfLogin_handler,updatePoolOfLogin_fault_handler);
			
			RemoteObjectUtil.callService(op,idGroupe,idLogin,tab_idPool, value,idprofil);
		}
		private function updatePoolOfLogin_handler(re : ResultEvent):void
		{
			if(re.result>0)
			{
				dispatchEvent(new GestionPoolEvent(GestionPoolEvent.UPDATE_PROFIL_ON_POOL));
				
			}
		}
		private function listePool_of_login_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_pool_gestionnaire_of_login.removeAll();//Vider la collection
			tab_pool_gestionnaire_of_login_selected.removeAll();
			for(i=0; i < tmpArr.length;i++){	
								
				var  item : PoolGestionnaire = new PoolGestionnaire();
				item.IDPool=tmpArr[i].IDPOOL;
				item.libelle_pool=tmpArr[i].LIBELLE;
				item.id_profilgestionparc = tmpArr[i].IDPROFIL_GEST_FLOTTE;				
				
				if(tmpArr[i].BOOLFOURNISSEUR == 0){
					item.isRevendeurString=ResourceManager.getInstance().getString('M21', 'Non');
				}
				else
				{
					item.isRevendeurString=ResourceManager.getInstance().getString('M21', 'Oui');
				}
				
				item.id_profil=tmpArr[i].IDPROFIL_COMMANDE;
				
				for(var a : int = 0; a<col_all_profil.length;a++)
				{
					if(col_all_profil.getItemAt(a).idProfil == item.id_profil)
					{
						item.indexOfProfil = a;
					}
				}
				
				if(tmpArr[i].SELECTED =='1')
				{
					tab_pool_gestionnaire_of_login_selected.addItem(tmpArr[i].IDPOOL);
					item.boolSelected = true;
				}							
				col_pool_gestionnaire_of_login.addItem(item);
			}
		}
		public function getRevendeur(idPool:int=-1):void // -1 pour tous sinon que les dispo pour ce pool
		{
				if(idPool<=0)
				{
					idPool = -1;
				}
				var op:AbstractOperation;
				op =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getRevendeur",
							 											liste_revendeur_handler,liste_revendeur_fault_handler);
				RemoteObjectUtil.callService(op,idPool);
		}
		public function listOpDistrib(idDistrib:int):void 
		{
			
				var op:AbstractOperation;
				op =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"listOpDistrib",
							 											listOpDistrib_handler);
				RemoteObjectUtil.callService(op,idDistrib);
		}
		public function updateOpDistrib(idDistrib:int,idOp:int,action:int):void 
		{
				var op:AbstractOperation;
				op =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"updateOpDistrib",
							 											updateOpDistrib_handler);
				RemoteObjectUtil.callService(op,idDistrib,idOp,action);
		}
		private function updateOpDistrib_handler(re : ResultEvent):void
		{
			//Alert.show(ObjectUtil.toString(re.result));
		}
		
		private function listOpDistrib_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_operateur.removeAll();//Vider la collection
			col_operateur_selected.removeAll();//Vider la collection
			for(i=0; i < tmpArr.length;i++){	
								
				var  item : OperateurVO = new OperateurVO();
				item.idOp=tmpArr[i].OPERATEURID;
				item.libelleOp=tmpArr[i].LIBELLE;
				
				if(item.boolSelected=tmpArr[i].STATUT == 1)
				{
					item.boolSelected = true;
					col_operateur_selected.addItem(item);
				}
				
				
				
				col_operateur.addItem(item);
			}
					
		}
		public function getAllRevendeur(groupeid:int):void
		{
			/*var op:AbstractOperation  =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getAllRevendeur",
							 											liste_all_revendeur_handler,liste_all_revendeur_fault_handler);
							 											
								
				RemoteObjectUtil.callService(op,groupeid,"",0);*/
				
				var op:AbstractOperation  =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"get_catalogue_revendeur_actif",
							 											liste_all_revendeur_handler,liste_all_revendeur_fault_handler);
							 											
								
				RemoteObjectUtil.callService(op,groupeid,"",CvAccessManager.getSession().USER.CLIENTACCESSID,0);
		}
		private function liste_all_revendeur_handler(evt : ResultEvent):void
		{
			col_all_revendeur.removeAll();
			var cursor:IViewCursor = (evt.result as ArrayCollection).createCursor();
						
			while(!cursor.afterLast)
			{
				if(cursor.current.LIBELLE ==null)
				{
					cursor.current.LIBELLE  = 'non renseigné';
				}
				col_all_revendeur.addItem(cursor.current);	
				cursor.moveNext();
			}
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.LISTE_ALL_REVENDEUR_COMPLETE));
			
		}
			
		public function listeContrainteOfPool(idPool:int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getContraintesOfPool",
																		getContrainteOfPool_handler,getContrainteOfPool_fault_handler);
																						
			RemoteObjectUtil.callService(op,idPool);
		}
		public function remove_pool_gestionnaire(idPool:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		 "fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService", 
																		 "remove_pool_gestionnaire_contrainte", 
																		remove_pool_gestionnaire_handler,remove_pool_gestionnaire_fault_handler);
			RemoteObjectUtil.callService(op, idPool);
		}
		public function update_pool_gestionnaire(IDPool:int,libelle_pool:String,codeInterne_pool:String,commentaire_pool:String,clientid:int,IDPoolRevendeur:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService", 
																		"updatePoolGestionnaire", 
																		update_pool_gestionnaire_handler,update_pool_gestionnaire_fault_handler);

			RemoteObjectUtil.callService(op, IDPool, libelle_pool, codeInterne_pool, commentaire_pool, clientid, IDPoolRevendeur);
		}

		public function add_pool_gestionnaire(libelle_pool:String, codeInterne_pool:String,commentaire_pool:String,clientid:int,idGroupe:int,IDPoolRevendeur:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService", 
																		"addPoolGestionnaire", 
																		add_pool_gestionnaire_handler,add_pool_gestionnaire_fault_handler);
			RemoteObjectUtil.callService(op, libelle_pool, codeInterne_pool, commentaire_pool, clientid, idGroupe, IDPoolRevendeur);
		}
		private function update_pool_gestionnaire_handler(evt : ResultEvent):void
		{
			if(evt)
			{
				dispatchEvent(new GestionPoolEvent(GestionPoolEvent.POOL_INFO_CHANGE_COMPLETE,false,false,evt.result as int));		
			}
		}
		private function add_pool_gestionnaire_handler(evt : ResultEvent):void
		{
			if(evt.result>0)
			{
				dispatchEvent(new GestionPoolEvent(GestionPoolEvent.POOL_INFO_CHANGE_COMPLETE,false,false,evt.result as int));
			}
		}

		private function remove_pool_gestionnaire_handler(evt : ResultEvent):void
		{
			if(evt)
			{
				dispatchEvent(new GestionPoolEvent(GestionPoolEvent.REMOVE_POOL_COMPLETE));		
			}
		}
		private function getContrainteOfPool_handler(evt : ResultEvent):void
		{
			col_contrainte_of_pool.removeAll();
			
			var cursor:IViewCursor = (evt.result as ArrayCollection).createCursor();
						
			while(!cursor.afterLast)
			{
				col_contrainte_of_pool.addItem(cursor.current);	
				cursor.moveNext();
			}
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.LISTE_CONTRAINTE_COMPLETE));	
		}
		private function liste_revendeur_handler(evt : ResultEvent):void
		{
			col_revendeur.removeAll();
			var cursor:IViewCursor = (evt.result as ArrayCollection).createCursor();
						
			while(!cursor.afterLast)
			{
				/*if(cursor.current.NOM_FOURNISSEUR==null)
				{
					cursor.current.NOM_FOURNISSEUR = 'non renseigné';
				}
				col_revendeur.addItem(cursor.current);	*/
				if(cursor.current.NOM_FOURNISSEUR!=null)
				{
					col_revendeur.addItem(cursor.current);
				}
				cursor.moveNext();
			}
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.LISTE_REVENDEUR_LIBRE_COMPLETE));	
		}
		private function listePool_handler (re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_pool_gestionnaire.removeAll();//Vider la collection
			for(i=0; i < tmpArr.length;i++){	
								
				var  item : PoolGestionnaire = new PoolGestionnaire();
				item.IDPool=tmpArr[i].IDPOOL;
				item.libelle_pool=tmpArr[i].LIBELLE;
				item.codeInterne_pool=tmpArr[i].CODE_INTERNE;
				item.commentaire_pool=tmpArr[i].COMMENTAIRES;
				item.IDPoolRevendeur=tmpArr[i].IDREVENDEUR;				
				item.nbGestionnaire=tmpArr[i].NB_GEST;
				item.nbTypeCmd=tmpArr[i].NB_TYPE_CDE;
				item.libelleRevendeur = tmpArr[i].NOM_FOURNISSEUR;
				col_pool_gestionnaire.addItem(item);
			}
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.LISTE_POOL_COMPLETE));			
		}
		private function liste_revendeur_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.liste_revendeur_fault_handler'+evt.toString());
		}
		private function listePool_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listePool_fault_handler'+evt.toString());
		}
		private function getContrainteOfPool_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getContrainteOfPool_fault_handler'+evt.toString());
		}
		private function remove_pool_gestionnaire_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.remove_pool_gestionnaire_fault_handler'+evt.toString());
		}
		private function update_pool_gestionnaire_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.update_pool_gestionnaire_fault_handler'+evt.toString());
		}
		private function add_pool_gestionnaire_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.add_pool_gestionnaire_fault_handler'+evt.toString());
		}
		private function liste_all_revendeur_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.liste_all_revendeur_fault_handler'+evt.toString());
		}
		private function listePool_of_login_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listePool_of_login_fault_handler'+evt.toString());
		}
		private function updatePoolOfLogin_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updatePoolOfLogin_fault_handler'+evt.toString());
		}
	}
}