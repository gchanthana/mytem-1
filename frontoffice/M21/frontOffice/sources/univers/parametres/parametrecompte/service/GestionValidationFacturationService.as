package univers.parametres.parametrecompte.service
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.parametrecompte.event.ValidationFacturationEvent;
	import univers.parametres.parametrecompte.vo.ActionVFVo;
	import univers.parametres.parametrecompte.vo.ProfilValidationFacturation;
	
	public class GestionValidationFacturationService extends EventDispatcher
	{
		[Bindable] public var listeProfilValidationFacturation:ArrayCollection = new ArrayCollection();
		[Bindable] public var listeAction:ArrayCollection = new ArrayCollection();
		[Bindable] public var idProfilSelected:int = 0
		
		public function GestionValidationFacturationService()
		{
		}
	// LISTE PROFIL VALIDATION FACTURATION	
		public function getListeProfilValidationFacturation():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M21.m21service","ListeProfilValidationFacturation",
																		getListeProfilValidationFacturation_handler);
			RemoteObjectUtil.callService(op);
		}
		private function getListeProfilValidationFacturation_handler(e:ResultEvent):void
		{
			listeProfilValidationFacturation = new ArrayCollection()
			if(e.result != null)
			{
				var Pgp:ProfilValidationFacturation
				for each(var obj:Object in e.result)
				{
					Pgp = new ProfilValidationFacturation()
					Pgp.idProfil = obj.IDPROFIL;
					Pgp.libelle = obj.NAME;
					Pgp.nbUser = obj.NBUSER;
					listeProfilValidationFacturation.addItem(Pgp);
				}
			}
			listeProfilValidationFacturation.refresh()
		}
	// RECUPERATION DES ACTIONS D'UN PROFIL VALIDATION FACTURATION
		private var boolNewProfil:Boolean = false
		public function getListeActionProfilValidationFacturation(idprofil:int, isnewprofil:Boolean = false):void
		{
			boolNewProfil = isnewprofil
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M21.m21service","ListeActValidFact",
																		RecupererActionProfilValidationFacturation_handler);
			RemoteObjectUtil.callService(op,idprofil);
		}
		private function RecupererActionProfilValidationFacturation_handler(e:ResultEvent):void
		{
			if(e.result != null)
			{
				var act:ActionVFVo = new ActionVFVo()
				for each(var obj:Object in e.result)
				{
					act = new ActionVFVo()
					act.idbutton = obj.IDBOUTON
					act.libelle = obj.BT_NAME
					act.code = obj.BT_CODE
					if(boolNewProfil)
						act.selected = false
					else
					{
						if(obj.BTN_RIGHT == 1)
							act.selected = true
						else
							act.selected = false
					}	
					listeAction.addItem(act);
				}
				listeAction.refresh()
			}
		}
	// CREER UN PROFIL
		public function createProfilValidationFacturation(listeID:String, libelle:String):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M21.m21service","CreerProfValidFact",
																		createProfilValidationFacturation_handler);
			RemoteObjectUtil.callService(op,listeID,libelle);
		}
		private function createProfilValidationFacturation_handler(e:ResultEvent):void
		{
			dispatchEvent(new ValidationFacturationEvent(ValidationFacturationEvent.CREATE_PROFIL_EVENT,true));
		}
	// METTRE A JOUR UN PROFIL
		public function MAJProfilValidationFacturation(idprofil:int, listeID:String, libelle:String):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M21.m21service","MAJProfilValidationFacturation",
																		MAJProfilValidationFacturation_handler);
			RemoteObjectUtil.callService(op,idprofil,listeID,libelle);
		}
		private function MAJProfilValidationFacturation_handler(e:ResultEvent):void
		{
			dispatchEvent(new ValidationFacturationEvent(ValidationFacturationEvent.MAJ_PROFIL_EVENT,true));
		}
	// SUPPRIMER PROFIL
		public function SupprimerProfilValidationFacturation(idprofil:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M21.m21service","SupProfilValidFact",
																		SupprimerProfilValidationFacturation_handler);
			RemoteObjectUtil.callService(op,idprofil);
		}
		private function SupprimerProfilValidationFacturation_handler(e:ResultEvent):void
		{
			dispatchEvent(new ValidationFacturationEvent(ValidationFacturationEvent.SUPPRIMER_PROFIL_EVENT,true));
		}
	// AFFECTER UN PROFIL
		public function AffecterProfilValidationFacturationToClient(apploginid:int, idprofil:int, idracine:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M21.m21service","AffProfValidFactToClient",
																		AffecterProfilValidationFacturationToClient_handler);
			RemoteObjectUtil.callService(op,apploginid,idprofil,idracine);
		}
		private function AffecterProfilValidationFacturationToClient_handler(e:ResultEvent):void
		{
			dispatchEvent(new ValidationFacturationEvent(ValidationFacturationEvent.MAJ_PROFIL_EVENT));
		}
	// RECUPERER L'IDPROFIL D'UN UTILISATEUR
		public function getProfilValFact(applogin:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M21.m21service","getProfilValFact",
																		getProfilValFact_handler);
			RemoteObjectUtil.callService(op,applogin);
		}
		private function getProfilValFact_handler(e:ResultEvent):void
		{
			var result:ArrayCollection = e.result as ArrayCollection;
			
			if(result != null  && result.length > 0)
			{
				idProfilSelected = parseInt((e.result as ArrayCollection).getItemAt(0).IDPROFIL.toString())
				dispatchEvent(new ValidationFacturationEvent(ValidationFacturationEvent.PROFIL_SELECTED_EVENT));
			}
		}
	}
}