package univers.parametres.parametrecompte.service.glcode
{
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.utils.ObjectUtil;
	
	import univers.parametres.parametrecompte.event.ProductGLCodeEvent;
	import univers.parametres.parametrecompte.vo.ProductGLCodeVO;
	
	public class ProductGLCodeDatas extends EventDispatcher
	{
		private var _listGLCodeProduct:ArrayCollection = new ArrayCollection();
		private var _allCodes:ArrayCollection = new ArrayCollection;
		private var _allSegments:ArrayCollection = new ArrayCollection();
		private var _allTypeThemes:ArrayCollection = new ArrayCollection();
		
		private var keysCodes:Object;
		private var keysSegments:Object;
		private var keysTypeThemes:Object;
		
		public function ProductGLCodeDatas()
		{
		}
		
		public function treatListProductCode(ret:Object):void
		{
			var longResult:int = (ret as ArrayCollection).length;
			listGLCodeProduct.source = [];
			allCodes.source = [];
			allSegments.source = [];
			allTypeThemes.source = [];
			for(var i:uint=0; i<longResult; i++)
			{
				var productVo:ProductGLCodeVO = new ProductGLCodeVO();
				productVo.fill(ret[i]);
				this.listGLCodeProduct.addItem(productVo);
				
				if(productVo.idGLCode)
				{
					var codeVO:Object = new Object();
					codeVO.libelleGLCode = productVo.libelleGLCode;
					
					if((productVo.libelleGLCode.length > 0) && (! ConsoviewUtil.isLabelInArray( productVo.libelleGLCode, 'libelleGLCode',allCodes.source)))						
					{
						codeVO.idGLCode = productVo.idGLCode;
						allCodes.addItem(codeVO);
					}
					
				}
				
				this.allTypeThemes.addItem(productVo.typeTheme);
				this.allSegments.addItem(productVo.segmentTheme);
			}
			
			var sort:Sort = new Sort();
			sort.fields = [new SortField('libelleGLCode', true)];
			allCodes.sort = sort;
			allCodes.refresh();
			for each (var prodGL:ProductGLCodeVO in listGLCodeProduct.source) 
			{
				prodGL.codesGL = allCodes;	
			}
			
			keysSegments = {};
			allSegments.source = allSegments.source.filter(removedDuplicatesSegments);
			keysTypeThemes = {};
			allTypeThemes.source = allTypeThemes.source.filter(removedDuplicatesTypeThemes);
			
			this.dispatchEvent(new ProductGLCodeEvent(ProductGLCodeEvent.LISTE_PRODUCT_GL_CODE));
		}
		
		private function removedDuplicatesCodes(item:Object, idx:uint, arr:Array):Boolean 
		{
			if (keysCodes.hasOwnProperty(item.idGLCode)) 
			{
				/* If the keys Object already has this property,
				return false and discard this item. */
				return false;
			}
			else 
			{
				/* Else the keys Object does *NOT* already have
				this key, so add this item to the new data
				provider. */
				keysCodes[item.idGLCode] = item;
				return true;
			}
		}
		
		private function removedDuplicatesSegments(item:Object, idx:uint, arr:Array):Boolean 
		{
			if (keysSegments.hasOwnProperty(item)) 
			{
				return false;
			}
			else 
			{
				keysSegments[item] = item;
				return true;
			}
		}
		
		private function removedDuplicatesTypeThemes(item:Object, idx:uint, arr:Array):Boolean 
		{
			if (keysTypeThemes.hasOwnProperty(item)) 
			{
				return false;
			}
			else 
			{
				keysTypeThemes[item] = item;
				return true;
			}
		}
		
		public function treatAffecterProductCode(ret:Object):void
		{
			var long_list:int = listGLCodeProduct.length;
			var enteredGLCode:Object = new Object();
			enteredGLCode.idGLCode = ret.IDGLCODE;
			enteredGLCode.libelleGLCode = ret.LIBELLEGLCODE;
			
			var exist:Boolean = ConsoviewUtil.isIdInArray(enteredGLCode.idGLCode, 'idGLCode', allCodes.source);
			if( (! exist) && (enteredGLCode.libelleGLCode.length > 0)) 
			{
				allCodes.addItem(enteredGLCode);
				
				var sort:Sort = new Sort();
				sort.fields = [new SortField('libelleGLCode', true)];
				allCodes.sort = sort;
				allCodes.refresh();
				for each (var prodGL:ProductGLCodeVO in listGLCodeProduct.source) 
				{
					prodGL.codesGL = allCodes;	
				}
			}
			
			this.dispatchEvent(new ProductGLCodeEvent(ProductGLCodeEvent.EDITION_PRODUCT_GL_CODE, enteredGLCode.idGLCode, enteredGLCode.libelleGLCode));
		}
		
		public function treatAffecterPlusieursElements(ret:Object):void
		{
			var long_list:int = listGLCodeProduct.length;
			var enteredGLCode:Object = new Object();
			enteredGLCode.idGLCode = ret.IDGLCODE;
			enteredGLCode.libelleGLCode = ret.LIBELLEGLCODE;
			
			var exist:Boolean = ConsoviewUtil.isIdInArray(enteredGLCode.idGLCode, 'idGLCode', allCodes.source);
			if( (! exist) && (enteredGLCode.libelleGLCode.length > 0)) 
			{
				allCodes.addItem(enteredGLCode);
				
				var sort:Sort = new Sort();
				sort.fields = [new SortField('libelleGLCode', true)];
				allCodes.sort = sort;
				allCodes.refresh();
				for each (var prodGL:ProductGLCodeVO in listGLCodeProduct.source) 
				{
					prodGL.codesGL = allCodes;	
				}
			}
			
			this.dispatchEvent(new ProductGLCodeEvent(ProductGLCodeEvent.EDITION_PRODUCT_GL_CODE, enteredGLCode.idGLCode, enteredGLCode.libelleGLCode));
		}
		
		[Bindable]
		public function get allCodes():ArrayCollection { return _allCodes; }
		
		public function set allCodes(value:ArrayCollection):void
		{
			if (_allCodes == value)
				return;
			_allCodes = value;
		}
		
		[Bindable]
		public function get listGLCodeProduct():ArrayCollection { return _listGLCodeProduct; }
		
		public function set listGLCodeProduct(value:ArrayCollection):void
		{
			if (_listGLCodeProduct == value)
				return;
			_listGLCodeProduct = value;
		}
		
		[Bindable]
		public function get allTypeThemes():ArrayCollection { return _allTypeThemes; }
		
		public function set allTypeThemes(value:ArrayCollection):void
		{
			if (_allTypeThemes == value)
				return;
			_allTypeThemes = value;
		}
		
		[Bindable]
		public function get allSegments():ArrayCollection { return _allSegments; }
		
		public function set allSegments(value:ArrayCollection):void
		{
			if (_allSegments == value)
				return;
			_allSegments = value;
		}
	}
}