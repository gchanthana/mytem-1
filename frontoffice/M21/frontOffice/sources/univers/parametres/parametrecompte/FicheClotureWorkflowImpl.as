package univers.parametres.parametrecompte.ihm
{
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.controls.CheckBox;
	import mx.controls.NumericStepper;
	import mx.events.FlexEvent;
	
	import univers.parametres.parametrecompte.service.WorkflowCloture;
	
	[Bindable]
	public class FicheClotureWorkflowImpl extends Box
	{
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMS
			//--------------------------------------------------------------------------------------------//
		
		public var ckxAuto		:CheckBox;
		
		public var stpJours		:NumericStepper;
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		
		public var wflwCloture	:WorkflowCloture = new WorkflowCloture();
		
			//--------------------------------------------------------------------------------------------//
			//					VARIABLES
			//--------------------------------------------------------------------------------------------//	
		
		public var nbrJours		:Number = 60;
		
		private var csteStep	:int = 15;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function FicheClotureWorkflowImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
		public function setInfosCloture():void
		{
			setInfosClotureAutomatique();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					LISTENERS EVENTS
			//--------------------------------------------------------------------------------------------//
		
		protected function stpJoursChangeHandler(e:Event):void
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATE
		//--------------------------------------------------------------------------------------------//
			
			//--------------------------------------------------------------------------------------------//
			//					LISTENERS EVENTS
			//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			getInfosClotureAutomatique();
		}
		
			//--------------------------------------------------------------------------------------------//
			//					LISTENERS EVENTS PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		private function infosClotureAutomatiqueHandler(e:Event):void
		{
			if(e.type == 'GET_INFOS_CLOTURE_WFLW')
			{
				ckxAuto.selected = wflwCloture.isClotureAuto;
				
				stpJours.value = wflwCloture.nbrJoiursCloture;
			}
			else if(e.type == 'SET_INFOS_CLOTURE_WFLW')
				{
					//DO NOTHING
				}
		}
		
			//--------------------------------------------------------------------------------------------//
			//					APPELS PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		private function getInfosClotureAutomatique():void
		{
			wflwCloture.getInfosClotureAutomatique();
			wflwCloture.addEventListener('GET_INFOS_CLOTURE_WFLW', infosClotureAutomatiqueHandler);
		}
		
		private function setInfosClotureAutomatique():void
		{
			wflwCloture.setInfosClotureAutomatique(ckxAuto.selected, stpJours.value);
			wflwCloture.addEventListener('SET_INFOS_CLOTURE_WFLW', infosClotureAutomatiqueHandler);
		}
	}
}