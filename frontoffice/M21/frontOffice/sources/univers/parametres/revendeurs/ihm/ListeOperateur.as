package univers.parametres.revendeurs.ihm
{
    import mx.resources.ResourceManager;
	import mx.containers.TitleWindow;
    import mx.controls.DataGrid;
    import mx.controls.Label;
    import mx.controls.TextInput;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import univers.parametres.parametrecompte.event.GestionTypeCommandeEvent;
    import univers.parametres.parametrecompte.service.GestionPoolService;
    import univers.parametres.parametrecompte.vo.OperateurVO;
    import univers.parametres.revendeurs.system.Revendeur;

    [Bindable]
    public class ListeOperateur extends TitleWindow
    {
        public var gestionPoolService:GestionPoolService = new GestionPoolService();
        public var idLogin:int;
        public var groupeIndex:int;
        public var txtFiltre:TextInput;
        public var txt_nb_select:Label;
        //COMPONANT
        public var dg_pool_gestionnaire:DataGrid;
        //public var comboDistrib : ComboBox;
        public var revendeur:Revendeur;

        public function ListeOperateur()
        {
            groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
        }

        public function init(evt:FlexEvent):void
        {
            initData();
        }

        public function filtreHandler(e:Event = null):void
        {
            gestionPoolService.col_operateur.filterFunction = filtreGrid;
            gestionPoolService.col_operateur.refresh();
        }

        private function filtreGrid(item:Object):Boolean
        {
            if (((item.libelleOp as String).toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
                return true;
            else
                return false;
        }

        public function initData():void
        {
            if (revendeur.ID > 0)
            {
                gestionPoolService.listOpDistrib(revendeur.ID);
            }
        }

        public function close():void
        {
            PopUpManager.removePopUp(this);
        }

        public function creationCompleteHandler():void
        {
        }

        public function onItemChanged(item:Object):void
        {
            var boolExiste:Boolean = false;
            for (var i:int = 0; i < gestionPoolService.col_operateur_selected.length; i++)
            {
                /* if (gestionPoolService.col_operateur_selected[i].hasOwnProperty("idOp") && item.hasOwnProperty("idOp"))
                 {*/
                if (parseInt(gestionPoolService.col_operateur_selected[i].idOp) == parseInt(item.idOp))
                {
                    gestionPoolService.col_operateur_selected.removeItemAt(i);
                    gestionPoolService.col_operateur_selected.refresh();
                    gestionPoolService.updateOpDistrib(revendeur.ID, item.idOp, 0);
                    boolExiste = true;
                }
                /* }*/
            }
            if (!boolExiste) //Si l'item n'est pas déja dans la liste
            {
                gestionPoolService.col_operateur_selected.addItem(item as OperateurVO);
                gestionPoolService.updateOpDistrib(revendeur.ID, item.idOp, 1);
            }
            gestionPoolService.col_operateur_selected.refresh();
            txt_nb_select.text = gestionPoolService.col_operateur.length + ResourceManager.getInstance().getString('M21', '_op_rateurs___') + gestionPoolService.col_operateur_selected.length + ResourceManager.getInstance().getString('M21', '_op_rateurs_s_lectionn__s_')
        }

        private function update_type_commande_handler(evt:GestionTypeCommandeEvent):void
        {
            dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.TYPE_COMMANDE_CHANGE, true));
        }
    }
}