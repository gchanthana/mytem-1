package univers.parametres.revendeurs.system
{
    import flash.sampler.DeleteObjectSample;

    /**
     *
     * @author daisy.bachelin
     */
    [Bindable]
    public class SlaVO
    {
        private var _revendeurId:int;
        private var _operateurId:int;
        private var _delai_livraison:int = 2;
        private var _delai_option:int = 2;
        private var _delai_resiliation:int = 2;
        private var _delai_suppression:int = 2;
        private var _delai_reactivation:int = 2;
        private var _delai_hour_livraison:String;
        private var _delai_hour_option:String;
        private var _delai_hour_resiliation:String;
        private var _delai_hour_suppression:String;
        private var _delai_hour_reactivation:String;

        /**
         *  VO regroupant les différents délais
         *  qui lie un revendeur avec son opérateur
         *  ( ex : 2 jours de delai de livraisons
         *   si commande effectuées avt 16h00 )
         *
         * **/
        public function SlaVO()
        {
        }

        /**
         *
         * @param o
         * @return
         */
        public static function serialize(o:Object):SlaVO
        {
            trace("Slavo ------------------serialize");
            var sla:SlaVO = new SlaVO();
            sla.revendeurId = o.IDFOURNISSEUR;
            sla.operateurId = o.IDOPERATEUR;
            sla.delai_livraison = o.DELAI_LIVRAISON;
            sla.delai_option = o.DELAI_OPTION;
            sla.delai_resiliation = o.DELAI_RESILIATION;
            sla.delai_suppression = o.DELAI_REACTIVATION;	//-> inversement suppression/reactivation
            sla.delai_reactivation = o.DELAI_SUPPRESSION;	//-> inversement suppression/reactivation
            sla.delai_hour_livraison = o.DELAI_HOUR_LIVRAISON;
            sla.delai_hour_option = o.DELAI_HOUR_OPTION;
            sla.delai_hour_resiliation = o.DELAI_HOUR_RESILIATION;
            sla.delai_hour_suppression = o.DELAI_HOUR_SUPPRESSION;
            sla.delai_hour_reactivation = o.DELAI_HOUR_REACTIVATION;
            return sla;
        }

        /**
         *
         * @return
         */
        public function get revendeurId():int
        {
            return _revendeurId;
        }

        /**
         * @private
         */
        public function set revendeurId(value:int):void
        {
            _revendeurId = value;
        }

        /**
         *
         * @return
         */
        public function get operateurId():int
        {
            return _operateurId;
        }

        /**
         *
         * @param value
         */
        public function set operateurId(value:int):void
        {
            _operateurId = value;
        }

        /**
         *
         * @return
         */
        public function get delai_livraison():int
        {
            return _delai_livraison;
        }

        /**
         *
         * @param value
         */
        public function set delai_livraison(value:int):void
        {
            _delai_livraison = value;
        }

        /**
         *
         * @return
         */
        public function get delai_resiliation():int
        {
            return _delai_resiliation;
        }

        /**
         *
         * @param value
         */
        public function set delai_resiliation(value:int):void
        {
            _delai_resiliation = value;
        }

        /**
         *
         * @return
         */
        public function get delai_option():int
        {
            return _delai_option;
        }

        /**
         *
         * @param value
         */
        public function set delai_option(value:int):void
        {
            _delai_option = value;
        }

        /**
         *
         * @return
         */
        public function get delai_suppression():int
        {
            return _delai_suppression;
        }

        /**
         *
         * @param value
         */
        public function set delai_suppression(value:int):void
        {
            _delai_suppression = value;
        }

        /**
         *
         * @return
         */
        public function get delai_reactivation():int
        {
            return _delai_reactivation;
        }

        /**
         *
         * @param value
         */
        public function set delai_reactivation(value:int):void
        {
            _delai_reactivation = value;
        }

        /**
         *
         * @return
         */
        public function get delai_hour_livraison():String
        {
            return _delai_hour_livraison;
        }

        /**
         *
         * @param value
         */
        public function set delai_hour_livraison(value:String):void
        {
            _delai_hour_livraison = value;
        }

        /**
         *
         * @return
         */
        public function get delai_hour_option():String
        {
            return _delai_hour_option;
        }

        /**
         *
         * @param value
         */
        public function set delai_hour_option(value:String):void
        {
            _delai_hour_option = value;
        }

        /**
         *
         * @return
         */
        public function get delai_hour_resiliation():String
        {
            return _delai_hour_resiliation;
        }

        /**
         *
         * @param value
         */
        public function set delai_hour_resiliation(value:String):void
        {
            _delai_hour_resiliation = value;
        }

        /**
         *
         * @return
         */
        public function get delai_hour_suppression():String
        {
            return _delai_hour_suppression;
        }

        /**
         *
         * @param value
         */
        public function set delai_hour_suppression(value:String):void
        {
            _delai_hour_suppression = value;
        }

        /**
         *
         * @return
         */
        public function get delai_hour_reactivation():String
        {
            return _delai_hour_reactivation;
        }

        /**
         *
         * @param value
         */
        public function set delai_hour_reactivation(value:String):void
        {
            _delai_hour_reactivation = value;
        }
    }
}