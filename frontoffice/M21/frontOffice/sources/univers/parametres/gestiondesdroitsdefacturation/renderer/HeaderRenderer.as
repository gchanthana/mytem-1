package  univers.parametres.gestiondesdroitsdefacturation.renderer
{
	import mx.controls.Label;

	public class HeaderRenderer extends Label
	{
		public function HeaderRenderer()
		{
			super();
			setStyle("horizontalAlign","center");
		}
		
	}
}