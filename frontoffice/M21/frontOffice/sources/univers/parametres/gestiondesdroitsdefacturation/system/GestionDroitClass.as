package univers.parametres.gestiondesdroitsdefacturation.system
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	public class GestionDroitClass
	{
		public function GestionDroitClass()
		{
		}

		public function getListeRacine(resulthandler:Function):void{
			var tmpListeRacine:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","getListeRacine",
						resulthandler);
			RemoteObjectUtil.callService(tmpListeRacine);			
		}

		public function getListeLogin(resulthandler:Function,idracine:int):void{
			if(idracine == -1){
				var tmpUser:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","getListeLogin",
						resulthandler);
				RemoteObjectUtil.callService(tmpUser);
			}else{
				var tmpUser2:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","getListeLogin",
						resulthandler);
				RemoteObjectUtil.callService(tmpUser2,idracine);
			}
		}
		
		public function getListeProfil(resulthandler:Function,idracine:int):void{
			if(idracine==-1){
				var tmpListeProfile:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","getListeProfil",
						resulthandler);
				RemoteObjectUtil.callService(tmpListeProfile);
			}else{
				var tmpListeProfile2:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","getListeProfil",
						resulthandler);
				RemoteObjectUtil.callService(tmpListeProfile2,idracine);
			}
		}

		public function getListeProfils(resulthandler:Function,idracine:int):void{
			if(idracine==-1){
				var tmpListeProfile:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","getListeProfils",
						resulthandler);
				RemoteObjectUtil.callService(tmpListeProfile);
			}else{
				var tmpListeProfile2:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","getListeProfils",
						resulthandler);
				RemoteObjectUtil.callService(tmpListeProfile2,idracine);
			}
		}

		public function addProfil(resulthandler:Function,iduser:int,idprofil:int,idracine:int):void{
			if(idracine == -1){
				var affecteOpe:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","addprofil",
							resulthandler);
				RemoteObjectUtil.callService(affecteOpe,iduser,idprofil);
			}else{
				var affecteOpe2:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","addprofil",
						resulthandler);
				RemoteObjectUtil.callService(affecteOpe2,iduser,idprofil,idracine);
			}
		}

		public function creerProfil(resulthandler:Function,profil:String,tab:String,idracine:int):void{
			if(idracine == -1){
				var creerOpe:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","creerProfil",
							resulthandler);
				RemoteObjectUtil.callService(creerOpe,profil,tab);
			}else{
				var creerOpe2:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","creerProfil",
						resulthandler);
				RemoteObjectUtil.callService(creerOpe2,profil,tab,idracine);
			}
		}

		public function supprimerProfil(resulthandler:Function,idprofil:int):void{
			var supprimerOpe:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","supprimerProfil",
						resulthandler);
			RemoteObjectUtil.callService(supprimerOpe,idprofil);
		}
		
		public function majProfil(resulthandler:Function,idprofil:int,profil:String,tab:String,idracine:int):void{
			if(idracine == -1){
				var majOpe:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","majProfil",
							resulthandler);
				RemoteObjectUtil.callService(majOpe,idprofil,profil,tab);
			}else{
				var majOpe2:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","majProfil",
						resulthandler);
				RemoteObjectUtil.callService(majOpe2,idprofil,profil,tab,idracine);
			}			
		}
		
		public function okProfil(resulthandler:Function,iduser:int,idracine:int):void{
			if(idracine == -1){
				var tmpProfil:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","okProfil",
							resulthandler);
				RemoteObjectUtil.callService(tmpProfil,iduser);
			}else{
				var tmpProfil2:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","okProfil",
						resulthandler);
				RemoteObjectUtil.callService(tmpProfil2,iduser,idracine);
			}
		}

		public function getProfil(resulthandler:Function,idprofil:int):void{
			var tmpProfil:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","getProfil",
						resulthandler);
			RemoteObjectUtil.callService(tmpProfil,idprofil);
		}
	
		public function removeProfil(resulthandler:Function,iduser:int,idprofil:int):void{
			var tmpRemoveProfil:AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(
					RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.parametres.gestiondesdroits.gestiondesdroits","removeProfil",
					resulthandler);
			RemoteObjectUtil.callService(tmpRemoveProfil,iduser,idprofil);
		}
	
	}
}