package univers.parametres.contrat.ihm
{
	
	
	
	
	import mx.resources.ResourceManager;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DateField;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.events.ItemClickEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.contrat.entity.InfosClientOperateurVo;
	import univers.parametres.contrat.system.GestionInfosClientOperateur;

	[Event(name="validerClicked", type="univers.inventaire.contrat.event.InfosClientOperateurFormEvent")]
	[Event(name="annulerClicked", type="univers.inventaire.contrat.event.InfosClientOperateurFormEvent")]
	
	[Bindable]
	public class FormInfosClientOperateurImpl extends TitleWindow
	{	
		
		private var _popUpConfirmation:PopUpMiseAJourParc;
		
		private var _item:InfosClientOperateurVo;
		private var _listeOperateurs:ICollectionView;
		public var listeOpe:ICollectionView;
		private var gestion:GestionInfosClientOperateur  = new GestionInfosClientOperateur();
//================ CONTROLS ===================================================================
		public var btValider	:Button;
		public var btAnnuler	:Button;
		public var cboOperateur	:ComboBox;
		public var rbg			:RadioButtonGroup;
		public var dcDateFpc	:DateField;
		public var txtMtMens	:TextInput;	
		public var txtFraisFixe	:TextInput;
		public var txtSeuil		:TextInput;
		public var rbgPec		:RadioButtonGroup;
		public var ckb12		:CheckBox;
		public var ckb24		:CheckBox;
		public var ckb36		:CheckBox;
		public var ckb48		:CheckBox;
		
		public var cbxEligibiliteDefault 		:ComboBox;
		public var cbxEligibiliteOuverture 		:ComboBox;
		public var cbxEligibiliteRenouvellement :ComboBox;
		
		public var eligibiliteDefault 			:ArrayCollection;
		public var eligibiliteOuverture 		:ArrayCollection;
		public var eligibiliteRenouvellement 	:ArrayCollection;
		
		public var activeEngagement:Boolean = false;
		
//================ FIN CONTROLS ===============================================================		
		public function FormInfosClientOperateurImpl()
		{
			super();
			
			gestion.getListOperateurs();
			listeOperateurs = gestion.listeOperateur;
			addEventListener("closeThis",_closeHandler);
			gestion.addEventListener(GestionInfosClientOperateur.ADDINFOSELIGIBILITE, addEligibiliteHandler);
			gestion.addEventListener(GestionInfosClientOperateur.GETINFOSELIGIBILITE, getEligibiliteHandler);
		}

		override protected function commitProperties():void
		{	
			if(item != null && item.OPERATEUR_CLIENT_ID > 0)
			{
				currentState = '';
				if(item.FPC_UNIQUE !=null)
				{
					rdbtnClickHandler(isChecked());
				}
				else
				{
					rdbtnClickHandler(false);
				}
			}
			else
			{
				currentState = 'addState';
				item = new InfosClientOperateurVo();
			}
			
			if(listeOpe != null)
			{
				rdbtnClickHandler(false);
			}
			super.commitProperties();
		}
		
		protected function creationCompleteHandler():void
		{
			rbg.selectedValue = (item.FPC_UNIQUE != null)?true:false;
			rbgPec.selectedValue = (item.BOOL_ACTUEL > 0)?true:false;
			
			createEligibilite();
			createOuverture_Renouvellement();
		}
		
		private function addEligibiliteHandler(e:Event):void
		{
			dispatchEvent(new Event("closePopUpInfoModifClient", true));
		}
		
		private function getEligibiliteHandler():void
		{
			var eligibilite		:Object = gestion.infosEligibilite;
			var lenEligibilite	:int 	= eligibiliteDefault.length;
			var lenOuvRenouvel	:int	= eligibiliteOuverture.length;
			var i				:int 	= -1;
			
			for(i = 0;i < lenEligibilite;i++)
			{
				if(eligibilite.ELIGIBILITE == eligibiliteDefault[i].ID)
				{
					cbxEligibiliteDefault.selectedIndex = i;
				}
			}
			
			for(i = 0;i < lenOuvRenouvel;i++)
			{
				if(eligibilite.OUVERTURE == eligibiliteOuverture[i].ID)
				{
					cbxEligibiliteOuverture.selectedIndex = i;
				}
				
				if(eligibilite.RENOUVELLEMENT == eligibiliteRenouvellement[i].ID)
				{
					cbxEligibiliteRenouvellement.selectedIndex = i;
				}
			}
		}	
		
		private function createEligibilite():void
		{
			var obj:Object;
			
			eligibiliteDefault = new ArrayCollection();
			
			obj = new Object();
			obj.LIBELLE = '-';
			obj.ID		= 0;
			eligibiliteDefault.addItem(obj);
					
			obj = new Object();
			obj.LIBELLE = ResourceManager.getInstance().getString('M21', '12_mois');
			obj.ID		= 12;
			eligibiliteDefault.addItem(obj);
			
			obj = new Object();
			obj.LIBELLE = ResourceManager.getInstance().getString('M21', '18_mois');
			obj.ID		= 18;
			eligibiliteDefault.addItem(obj);
			
			obj = new Object();
			obj.LIBELLE = ResourceManager.getInstance().getString('M21', '24_mois');
			obj.ID		= 24;
			eligibiliteDefault.addItem(obj);
			
			obj = new Object();
			obj.LIBELLE = ResourceManager.getInstance().getString('M21', '36_mois');
			obj.ID		= 36;
			eligibiliteDefault.addItem(obj);
			

			var ID:int = 1;
			switch(item.DUREE_ELIGIBILITE)
			{
				case 0 :ID = 0;break;
				case 12 :ID = 1;break;
				case 18 :ID = 2;break;
				case 24 :ID = 3;break;
				case 36 :ID = 4;break;
				
				default:ID = 1;break;
			}
			cbxEligibiliteDefault.selectedIndex = ID;
		}
		
		private function createOuverture_Renouvellement():void
		{
			var obj:Object;
			
			eligibiliteOuverture		= new ArrayCollection();
			eligibiliteRenouvellement 	= new ArrayCollection();
			
			for(var i:int = 0; i < 6;i++)
			{
				obj 		= new Object();
				obj.LIBELLE = i.toString() + ResourceManager.getInstance().getString('M21', '_jours');
				obj.ID		= i;
				eligibiliteOuverture.addItem(obj);
				eligibiliteRenouvellement.addItem(obj);
			}
			
			cbxEligibiliteOuverture.selectedIndex 		= item.DUREE_OUVERTURE;
			cbxEligibiliteRenouvellement.selectedIndex 	= item.DUREE_RENOUVEL;

		}
		
		private function isChecked():Boolean
		{
			var bool:Boolean = false;
			if(item.ENGAG_12 == 0 || item.ENGAG_24 == 0 || item.ENGAG_36 == 0 || item.ENGAG_48 == 0 )
			{
				bool = true;
			}
			return bool;
		}
		
		protected function valider():void
		{
				
		}
		
		protected function _closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
//================ ACCESSORS ===================================================================
		public function set item(value:InfosClientOperateurVo):void
		{
			_item = value;
			invalidateProperties()
		}

		public function get item():InfosClientOperateurVo
		{
			return _item;
		}
		
		public function set listeOperateurs(value:ICollectionView):void
		{
			_listeOperateurs = value;
		}

		public function get listeOperateurs():ICollectionView
		{
			return _listeOperateurs;
		}
//==== FIN ======= ACCESSORS ===================================================================

//==== HANDLERS ================================================================================
		protected function _btValiderClickHandler(me:MouseEvent):void
		{
			if (item)
			{
				if(item.OPERATEUR_CLIENT_ID == 0)
				{
					item.OPERATEURID 			= cboOperateur.selectedItem.OPERATEURID;
					item.NOM_OP 				= cboOperateur.selectedItem.NOM_OP;
				} 				
				item.ENGAG_12 					= covertBoolToInt(ckb12.selected);
				item.ENGAG_24 					= covertBoolToInt(ckb24.selected);
				item.ENGAG_36					= covertBoolToInt(ckb36.selected);
				item.ENGAG_48 					= covertBoolToInt(ckb48.selected);
				item.BOOL_ACTUEL 				= (rbgPec.selectedValue)?1:0;
				item.FRAIS_FIXE_RESILIATION 	= Number(txtFraisFixe.text);
				item.MONTANT_MENSUEL_PENALITE 	= Number(txtMtMens.text);
				item.SEUIL_TOLERANCE 			= Number(txtSeuil.text);
				item.DUREE_ELIGIBILITE 			= cbxEligibiliteDefault.selectedItem.ID;
				item.DUREE_OUVERTURE		 	= cbxEligibiliteOuverture.selectedItem.ID;
				item.DUREE_RENOUVEL			 	= cbxEligibiliteRenouvellement.selectedItem.ID;

				if(rbg.selectedValue)
				{
					item.FPC_UNIQUE = dcDateFpc.selectedDate;
					majParc();
				}
				else
				{
					item.FPC_UNIQUE = null;
					closedPopUpMajParc(me);
				}
			}
		}

		private function majParc():void
		{
			_popUpConfirmation = new PopUpMiseAJourParc();
			PopUpManager.addPopUp(_popUpConfirmation,this,true);
			PopUpManager.centerPopUp(_popUpConfirmation);
			_popUpConfirmation.addEventListener(PopUpMiseAJourParc.MAJ_PARC_OUI, closedPopUpMajParc);
			_popUpConfirmation.addEventListener(PopUpMiseAJourParc.MAJ_PARC_NON, closedPopUpMajParc);
		}
		
		private function closedPopUpMajParc(e:Event):void
		{
			if(e.type == 'MAJ_PARC_OUI')
			{
				item.MAJ_PARC	= 1;
			}
			else
			{
				item.MAJ_PARC	= 0;
			}
						
			dispatchEvent(new Event("closePopUpInfoModifClient", true));
		}
		
		private function covertBoolToInt(bool:Boolean):int
		{
			var rslt:int = 0;
			if(bool)
			{
				rslt = 1;
			}
			return rslt;
		}

		protected function _btAnnulerClickHandler(event:MouseEvent):void
		{
			dispatchEvent(new Event("closePopUpInfoClient",true));
		}

		protected function _cboOperateurChangeHandler(event:ListEvent):void
		{
			
		}
		
		protected function _rbgItemClickHandler(event:ItemClickEvent):void
		{
			if (item == null) return;
		}
		
		protected function rdbtnClickHandler(rdbtnSelected:Boolean):void
		{
			activeEngagement = !rdbtnSelected;
			if(rdbtnSelected)
			{
				ckb12.selected = ckb24.selected = ckb36.selected = ckb48.selected = false;
			}
			else
			{
				ckb12.selected = convertIntToBool(item.ENGAG_12);
				ckb24.selected = convertIntToBool(item.ENGAG_24);
				ckb36.selected = convertIntToBool(item.ENGAG_36);
				ckb48.selected = convertIntToBool(item.ENGAG_48);
			}
		}
		
		private function convertIntToBool(eng:int):Boolean
		{
			var bool:Boolean = false;
			if(eng == 1)
			{
				bool = true;
			}
			return bool;
		}
//==== FIN ======= HANDLERS ====================================================================

		
	}
}