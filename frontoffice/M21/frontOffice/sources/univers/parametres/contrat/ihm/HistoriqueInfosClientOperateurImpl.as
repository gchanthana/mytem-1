package univers.parametres.contrat.ihm
{
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.ListEvent;
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import univers.parametres.contrat.entity.InfosClientOperateurVo;
	import univers.parametres.contrat.event.InfosClientOperateurFormEvent;
	import univers.parametres.contrat.system.GestionInfosClientOperateur;
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	
	[Bindable]
	public class HistoriqueInfosClientOperateurImpl extends Box
	{	
		
		//=== POPERTIES ================================================================//
		private var _dataProvider:ArrayCollection;
		private var _selectedItem:InfosClientOperateurVo;
		private var _boolActiver:Boolean = true;
		private var _accessUser:Boolean = false;
		private var gestionInfosClientOperateur:GestionInfosClientOperateur = new GestionInfosClientOperateur();
		//=== FIN == POPERTIES =========================================================//
		 	
		//=== CONTROLS ================================================================//
		public var dgListe:DataGrid;
		public var btAdd:Button;
		public var btEdit:Button;
		public var btSupp:Button;
		public var cbFiltre:CheckBox;
		public var txtFiltre:TextInputLabeled;
		//public var btSupp:Button;
		private var popUpInfoClient:FormInfosClientOperateurIHM;	
		//=== FIN == CONTROLS =========================================================//
		
		
		public function HistoriqueInfosClientOperateurImpl()
		{
			
			//Pour les test access User = true;
			
			//super();
			
			
		}
		public function init(evt : Event=null):void
		{
			accessUser = true;
			addEventListener("eraseThis",	eraseThisHandler);
			addEventListener("editeThis",	editeThisHandler);
			addEventListener(InfosClientOperateurFormEvent.ANNULER_CLICKER, _localeAnnulerClickerHandler);
			addEventListener(InfosClientOperateurFormEvent.VALIDER_CLICKED, _localeValiderClickedHandler);
			gestionInfosClientOperateur.addEventListener("getHistoriqueInfosClientOperateur",getHistoriqueInfosClientOperateurHandler);
			
			dataProvider = gestionInfosClientOperateur.listeInfosClientOperateur;
			
			gestionInfosClientOperateur.getHistoriqueInfosClientOperateur();
		}
		
		private function getHistoriqueInfosClientOperateurHandler(e:Event):void
		{
			dataProvider = gestionInfosClientOperateur.listeInfosClientOperateur;
		}
		
		private function eraseThisHandler(e:Event):void
		{
			if(dgListe.selectedItem != null)
			{
				var handler:Function = function (event:CloseEvent):void
				{
					if(event.detail == Alert.YES)
					{
						dispatchEvent(new GestionPoolEvent(GestionPoolEvent.UPDATE_LISTE_OPERATEUR,true));
						gestionInfosClientOperateur.deleteInfosClientOperateur(dgListe.selectedItem as InfosClientOperateurVo);	
					}
				}
				if(dgListe.selectedIndex != -1)
				{
					Alert.buttonWidth = 100;
					Alert.yesLabel = ResourceManager.getInstance().getString('M21', 'Oui');
					Alert.noLabel = ResourceManager.getInstance().getString('M21', 'Non');
					Alert.show(ResourceManager.getInstance().getString('M21', 'Voulez_vous_vraiment_supprimer_cette_lig'),ResourceManager.getInstance().getString('M21', 'Confirmez'),Alert.YES|Alert.NO,this,handler);
				}	
			}
		}
		
		private function editeThisHandler(e:Event):void
		{
			if(dgListe.selectedItem != null)
			{			
				showPopUpInfoClient();	
			}
			else
			{
				if(dgListe.selectedIndex == -1)
				{
					showPopUpInfoClient();
				}
			}
		}

		private function showPopUpInfoClient():void
		{ 
			popUpInfoClient = new FormInfosClientOperateurIHM();
			if(dgListe.selectedIndex > -1)
			{
				popUpInfoClient.item 				= dgListe.selectedItem as InfosClientOperateurVo;
				popUpInfoClient.listeOperateurs 	= gestionInfosClientOperateur.listeInfosClientOperateur;
			}
			else
			{
				popUpInfoClient.item 				= new InfosClientOperateurVo();
				popUpInfoClient.listeOpe			= gestionInfosClientOperateur.listeInfosClientOperateur;
			}
			
			PopUpManager.addPopUp(popUpInfoClient,this,true);
			PopUpManager.centerPopUp(popUpInfoClient);
			popUpInfoClient.addEventListener("closePopUpInfoClient",closedpopUpInfoClientNoModifHandler);
			popUpInfoClient.addEventListener("closePopUpInfoModifClient",closedpopUpInfoClientModifHandler);
		}
		
		private function closedpopUpInfoClientNoModifHandler(e:Event):void
		{
			var item:InfosClientOperateurVo = popUpInfoClient.item;
			dispatchEvent(new InfosClientOperateurFormEvent(item,InfosClientOperateurFormEvent.ANNULER_CLICKER,true));
			popUpInfoClient.dispatchEvent(new Event("closeThis"));
		}
		
		private function closedpopUpInfoClientModifHandler(e:Event):void
		{
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.UPDATE_LISTE_OPERATEUR,true));
			var item:InfosClientOperateurVo = popUpInfoClient.item;
			dispatchEvent(new InfosClientOperateurFormEvent(item,InfosClientOperateurFormEvent.VALIDER_CLICKED,true));
			popUpInfoClient.dispatchEvent(new Event("closeThis"));
		}
		
		///============== OPERATIONS ==============================================//
		///============== FORMATS =================================================//
		protected function formatColonneDateFunction(item:Object,column:DataGridColumn):String
		{
			
			return DateFunction.formatDateAsString(item[column.dataField] as Date);
		}
		
		protected function formatColonneMonnaie(item:Object,column:DataGridColumn):String
		{			
			return ConsoviewFormatter.formatEuroCurrency(Number(item[column.dataField]),2);
		}
		
		override protected function commitProperties():void
		{
//			activer(boolActiver && accessUser);
			super.commitProperties();
		}
		
		
//		protected function activer(bool:Boolean):void
//		{
//			if(initialized)
//			{
//				btAdd.enabled = bool;
//				btEdit.enabled = bool;
//				btSupp.enabled = bool;	
//				dgListe.selectable = bool;
//			}			
//		}
		
		protected function filterFunction(item:Object):Boolean
		{
			if(item.BOOL_ACTUEL) return true
			else return false;	
		}
		///============== HANDLERS ================================================//
		protected function _btAddClickHandler(event:MouseEvent):void
		{
//			currentState = 'editState';
//			selectedItem = new InfosClientOperateurVo();
//			boolActiver = false;
			dgListe.selectedIndex = -1;	
			editeThisHandler(event);
		}

		protected function _btEditClickHandler(event:MouseEvent):void
		{
			if(dgListe.selectedIndex > -1)
			{
				currentState = 'editState';
				selectedItem = dgListe.selectedItem as InfosClientOperateurVo;
				boolActiver = false;	
			}
		}
		
		protected function _btSuppClickHandler(event:MouseEvent):void
		{
			var handler:Function = function (event:CloseEvent):void
			{
				if(event.detail == Alert.YES)
				{
					gestionInfosClientOperateur.deleteInfosClientOperateur(dgListe.selectedItem as InfosClientOperateurVo);	
				}
			}
			if(dgListe.selectedIndex != -1)
			{
				Alert.buttonWidth = 100;
				Alert.yesLabel = ResourceManager.getInstance().getString('M21', 'Oui');
				Alert.noLabel = ResourceManager.getInstance().getString('M21', 'Non');
				Alert.show(ResourceManager.getInstance().getString('M21', 'Voulez_vous_vraiment_supprimer_cette_lig'),ResourceManager.getInstance().getString('M21', 'Confirmez'),Alert.YES|Alert.NO,this,handler);
			}	
		}
		
		protected function _dgListeInfosChangeHandler(event:ListEvent):void
		{
			selectedItem = dgListe.selectedItem as InfosClientOperateurVo
		}
		
		protected function _localeAnnulerClickerHandler(event:InfosClientOperateurFormEvent):void
		{
			currentState = '';
			boolActiver = true;
		}

		protected function _localeValiderClickedHandler(event:InfosClientOperateurFormEvent):void
		{
			currentState = '';
			boolActiver = true;
			if(event.infos.OPERATEUR_CLIENT_ID > 0)
			{
				gestionInfosClientOperateur.updateInfosClientOperateur2(event.infos);
			}
			else
			{
				gestionInfosClientOperateur.saveInfosClientOperateur(event.infos);
			}
			gestionInfosClientOperateur.listeInfosClientOperateur = new ArrayCollection();
			dataProvider = new ArrayCollection();
			gestionInfosClientOperateur.getHistoriqueInfosClientOperateur();
		}
		
		protected function _cbFiltreClickHandler(event:MouseEvent):void
		{
			if(!cbFiltre.selected && dataProvider != null)
			{
				dataProvider.filterFunction = filterFunction;
				
			}
			else
			{
				dataProvider.filterFunction = null
			}	
			
			dataProvider.refresh()
		}
		
		protected function _btbtFermerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function reinitialiserClickHandler():void
		{
			txtFiltre.text = "";
			cbFiltre.selected = false;
			gestionInfosClientOperateur.getHistoriqueInfosClientOperateur()
		}

		protected function txtFiltreChangeHandler(ev:Event): void
	    {
	    	if ( dataProvider != null)
	    	{
	    		dataProvider.filterFunction = filtrer2;
	    		dataProvider.refresh();
	    	}
	    }
	    
	    private function filtrer2(item : Object):Boolean
		{
			cbFiltre.selected = true;
			if (
				((item.NOM_OP?item.NOM_OP:"").toString().toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				((item.NOM_USER_MODIFIED?item.NOM_USER_MODIFIED:"").toString().toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				|| 
				((item.FPC_UNIQUE?item.FPC_UNIQUE:"").toString().toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				((item.MONTANT_MENSUEL_PENALITE?item.MONTANT_MENSUEL_PENALITE:"").toString().toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				((item.FRAIS_FIXE_RESILIATION?item.FRAIS_FIXE_RESILIATION:"").toString().toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				((item.DATE_MODIF?item.DATE_MODIF:"").toString().toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
			   )
			{
				return true;
			}
			else
			{
				return false;
			}
	    }
		
		protected function _closeHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		//====== FIN == HANDLERS ==============================================//
		
		
		//============== ACCESSORS ================================================//
		public function set dataProvider(value:ArrayCollection):void
		{
			if(_dataProvider != value)
			{
				_dataProvider = value;
				
				if(_dataProvider) _dataProvider.refresh(); 
			};
			
			invalidateProperties();
		}

		public function get dataProvider():ArrayCollection
		{
			return _dataProvider;
		}

		public function set selectedItem(value:InfosClientOperateurVo):void
		{
			_selectedItem = value;
			invalidateProperties();
			invalidateSize();
			invalidateDisplayList();
		}

		public function get selectedItem():InfosClientOperateurVo
		{
			return _selectedItem;
		}

		public function set boolActiver(value:Boolean):void
		{
			_boolActiver = value;
			invalidateProperties();
		}

		public function get boolActiver():Boolean
		{
			return _boolActiver;
		}

		public function set accessUser(value:Boolean):void
		{
			_accessUser = value;
		}

		public function get accessUser():Boolean
		{
			return _accessUser;
		}

		
	}
}