package univers.parametres.poolgestionnaire.ihm
{
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	
	import univers.parametres.poolgestionnaire.event.GestionPoolEvent;
	import univers.parametres.poolgestionnaire.event.GestionProfilEvent;
	import univers.parametres.poolgestionnaire.service.GestionPoolService;
	import univers.parametres.poolgestionnaire.service.ProfilService;
	import univers.parametres.poolgestionnaire.vo.PoolGestionnaire;
	
	[Bindable]
	public class ListePoolLogin extends VBox
	{
		public var gestionPoolService : GestionPoolService = new GestionPoolService();
		public var profilService : ProfilService= new ProfilService();
		private var _idLogin : int = -1;
		public var groupeIndex : int;
		
		//COMPONANT
		public var dg_pool_gestionnaire: DataGrid;
		public var cbAll : CheckBox;		
		
		public function ListePoolLogin()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			
		}
		public function init(evt : FlexEvent):void
		{
			
		}
		public function initData(id : int):void
		{
			idLogin = id;
			gestionPoolService.addEventListener(GestionPoolEvent.UPDATE_PROFIL_ON_POOL,profilUpdateHandler);
			//La liste des profil doit être définit avant d'appeller la liste des pool
			profilService.addEventListener(GestionProfilEvent.LISTE_PROFIL_COMPLETE,getlistePool_of_login);
			profilService.listeProfil(groupeIndex);
		}
		private function getlistePool_of_login(evt : GestionProfilEvent):void
		{
			gestionPoolService.col_all_profil = profilService.col_profil;
			gestionPoolService.listePool_of_login(groupeIndex,idLogin);
		}
		public function set idLogin(id : int):void
		{
				this._idLogin = id;
		}
		public function get idLogin():int
		{
			return _idLogin;
		}
		
		public function onItemChanged(item : Object):void
		{
			var boolExiste : Boolean = false;
			for(var i:int=0;i<gestionPoolService.tab_pool_gestionnaire_of_login_selected.length;i++)
			{
				if(gestionPoolService.tab_pool_gestionnaire_of_login_selected[i]==item.IDPool)
				{
					gestionPoolService.tab_pool_gestionnaire_of_login_selected.removeItemAt(i);
					
					gestionPoolService.updatePoolOfLogin(groupeIndex,idLogin,[item.IDPool],0);
					
					boolExiste = true;
					
					item.indexOfProfil = -1 // permet de deselectionner le profil dans l'admin
				}
			}
			if(!boolExiste) //Si l'item n'est pas déja dans la liste
			{
				gestionPoolService.tab_pool_gestionnaire_of_login_selected.addItem(item.IDPool);
				gestionPoolService.updatePoolOfLogin(groupeIndex,idLogin,[item.IDPool],1);
				
				item.indexOfProfil = -1 // permet de deselectionner le profil dans l'admin
			}
			gestionPoolService.tab_pool_gestionnaire_of_login_selected.refresh();
			profilService.col_profil.refresh();
		}
		public function comboChangeHandler(idProfil : int, data : PoolGestionnaire,indexInCombo:int):void
		{
			data.indexOfProfil = indexInCombo;
			gestionPoolService.updatePoolOfLogin(groupeIndex,idLogin,[data.IDPool],1,idProfil);
		}
		private function profilUpdateHandler(evt : GestionPoolEvent):void
		{
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.UPDATE_PROFIL_LOGIN));
		}
	}
}