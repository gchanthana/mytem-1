package univers.parametres.poolgestionnaire.ihm
{
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrecompte.vo.PoolGestionnaire;
	import univers.parametres.poolgestionnaire.service.GestionPoolService;
	
	public class PopGestionnairePool extends TitleWindow
	{
		private var _gestionnaireService:GestionPoolService = new GestionPoolService();
		
		[Bindable]
		public function get gestionnaireService():GestionPoolService
		{
			return _gestionnaireService;
		}

		private function set gestionnaireService(value:GestionPoolService):void
		{
			_gestionnaireService = value;
		}

		[Bindable]
		protected var _pool:PoolGestionnaire;
		public function set pool(value:PoolGestionnaire):void
		{
			_pool = value;
			if(_pool != null)
			{	
				_gestionnaireService.getListeGestByPool(_pool.IDPool);	
				
			}
		}
		
		public function PopGestionnairePool()
		{
			super();
		}
		
	 
		
		
		
		protected function btFermerClickHandler(event : MouseEvent):void
		{
			fermer();
		}
		
		protected function popCloseHandler(event:CloseEvent):void
		{
			fermer();
		}
		
		//========================================================
		private function fermer():void
		{
			PopUpManager.removePopUp(this);
		}
	}
}