package univers.parametres.poolgestionnaire.vo
{
	[Bindable]
	public class CompteOperateur
	{
		public var idCompteOP : int;
		public var idSousCompteOP : int;
		public var libelle_compte : String;
		public var libelle_sous_compte : String;
		public var libelle : String;
		public var boolSelected : Boolean = false
		public var isCompteMaitre : Boolean = false;
		
		public function CompteOperateur()
		{
		}

	}
}