package univers.parametres.gestionnaire
{
	import mx.resources.ResourceManager;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.gestionnaire.classemetier.PoolGestionnaire;
	import univers.parametres.gestionnaire.classemetier.Site;
	
	public class GestionSites extends GestionSitesIHM
	{
		private var _col_site : ArrayCollection = new ArrayCollection;
		private var _col_site_ofPool : ArrayCollection = new ArrayCollection;
		private var pool_gestionnaire : PoolGestionnaire;
		public function GestionSites(pool_gestionnaire : PoolGestionnaire)
		{
			super();
			this.pool_gestionnaire = pool_gestionnaire;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			addEventListener(CloseEvent.CLOSE,fermer);
		}
		
		private function initIHM(evt : FlexEvent):void
		{
			bt_affecter_site_to_gestionnaire.addEventListener(MouseEvent.CLICK,affecter_site_to_gestionnaire_handler);
			bt_creer_site.addEventListener(MouseEvent.CLICK,creer_site_handler);
			bt_fiche.addEventListener(MouseEvent.CLICK,fiche_handler);
			bt_supprimer_site.addEventListener(MouseEvent.CLICK,supprimer_site_handler);
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer);
			lab_pool.text = pool_gestionnaire.libelle_pool;
			bt_desaffecter_site.addEventListener(MouseEvent.CLICK,desaffecter);
			//Filtre 
			text_input_filtre.addEventListener(Event.CHANGE,filtreSite);
			initData();
			initData_pool();
			
			//Ecouteur sur le Enter 
			text_input_rechercher.addEventListener(FlexEvent.ENTER,initData);
			imgRechercher.addEventListener(MouseEvent.CLICK,initData);
			if(CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)
			{
				gestion_droit(true);
			}
			else
			{
				gestion_droit();
			}
			mode_lecture.addEventListener(Event.CHANGE,changeMode);
		}
		private function changeMode(evt : Event):void
		{
			if(mode_lecture.selected == true)
			{
				gestion_droit();
			}
			else
			{
				gestion_droit(true);
			}
		}
		private function gestion_droit(modeEcriture : Boolean = false):void
		{
			bt_affecter_site_to_gestionnaire.enabled  = modeEcriture;
			bt_creer_site..enabled  = modeEcriture;
			bt_fiche.enabled = modeEcriture;
			bt_supprimer_site.enabled  = modeEcriture;
			bt_desaffecter_site.enabled  = modeEcriture;
		}
		private function filtreSite(e:Event):void{
			_col_site.filterFunction=filtreGridSite;
			_col_site.refresh();
		}
		public function filtreGridSite(item:Object):Boolean 
		{
	
			if (( item.libelle_site && (item.libelle_site as String).toLowerCase().search(text_input_filtre.text.toLowerCase())!=-1) 
				||(item.ref_site && (item.ref_site as String).toLowerCase().search(text_input_filtre.text.toLowerCase())!=-1) 
				||(item.code_interne_site && (item.code_interne_site as String).toLowerCase().search(text_input_filtre.text.toLowerCase())!=-1 )) {
				return (true);
			}
			else
			{
				return (false);
			}
		}
		private function desaffecter(evt : MouseEvent):void
		{
			
			if(dg_site_of_pool.selectedIndex!=-1)
			{
				//trace("------>"+(dg_site_of_pool.selectedItem as Site).id);
				var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																			"desaffecte_site_pool",
																			process_desaffecte_site_pool);
				RemoteObjectUtil.callService(op,(dg_site_of_pool.selectedItem as Site).id,pool_gestionnaire.IDPool);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'S_lectionnez_un_site_affect__au_pool'));
			}
		}
		private function process_desaffecte_site_pool(re : ResultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M21', 'Site_d_saffect_'));
			initData_pool();
		}
		private function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		public function initData(evt : Event = null):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getSite",
																		process_init_dg_site);
			RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,text_input_rechercher.text);
		}
		private function initData_pool():void
		{
				var op2 :AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getSite_pool",
																		process_init_dg_site_ofPool,erreur);
			RemoteObjectUtil.callService(op2 ,pool_gestionnaire.IDPool);
		}
		private function erreur (evt : FaultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M21', 'Erreur')+evt.toString());
		}
		private function process_init_dg_site_ofPool(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			_col_site_ofPool.removeAll();//vider la collection
			for(i=0; i < tmpArr.length;i++){	
								
				var  item : Site = new Site();
				item.id_site=tmpArr[i].IDSITE_PHYSIQUE;
				item.adr_site=tmpArr[i].ADRESSE;
				item.code_interne_site=tmpArr[i].SP_CODE_INTERNE;
				item.commentaire_site=tmpArr[i].SP_COMMENTAIRE;
				item.commune_site=tmpArr[i].SP_COMMUNE;
				item.cp_site=tmpArr[i].SP_CODE_POSTAL;
				item.libelle_site=tmpArr[i].NOM_SITE;
				item.ref_site = tmpArr[i].SP_REFERENCE;
								
				_col_site_ofPool.addItem(item);
			}
			dg_site_of_pool.dataProvider = _col_site_ofPool;
			
		}
		private function process_init_dg_site(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			_col_site.removeAll();//vider la collection
			for(i=0; i < tmpArr.length;i++){	
								
				var  item : Site = new Site();
				item.id_site=tmpArr[i].IDSITE_PHYSIQUE;
				item.adr_site=tmpArr[i].ADRESSE;
				item.code_interne_site=tmpArr[i].SP_CODE_INTERNE;
				item.commentaire_site=tmpArr[i].SP_COMMENTAIRE;
				item.commune_site=tmpArr[i].SP_COMMUNE;
				item.cp_site=tmpArr[i].SP_CODE_POSTAL;
				item.libelle_site=tmpArr[i].NOM_SITE;
				item.ref_site = tmpArr[i].SP_REFERENCE;
				item.IS_FACTURATION = convertIntToBool(tmpArr[i].ISLIVRAISON);
				item.IS_LIVRAISON 	= convertIntToBool(tmpArr[i].ISFACTURAITON);
				_col_site.addItem(item);
			}
			dg_site.dataProvider = _col_site;
		}
		
		private function convertIntToBool(value:int):Boolean
		{
			var bool:Boolean = false;
			
			if(value == 0)
				bool = false;
			if(value == 1)
				bool = true;
				
			return bool;
		}
		
		private function affecter_site_to_gestionnaire_handler(evt : MouseEvent):void
		{
			if(dg_site.selectedIndex != -1)
			{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"affecte_site_pool",
																		process_affecte_site_pool);
			RemoteObjectUtil.callService(op,(dg_site.selectedItem as Site).id,pool_gestionnaire.IDPool);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'S_lectionnez_un_site'));
			}
		}
		private function process_affecte_site_pool(re : ResultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M21', 'Site_affect__au_pool'));
			initData();
			initData_pool();
		}
		private function creer_site_handler(evt : MouseEvent):void
		{
				var panel_fiche : FicheSite = new FicheSite(null,true,this);
				PopUpManager.addPopUp(panel_fiche,this,true);
				PopUpManager.centerPopUp(panel_fiche);
				//panel_fiche.addEventListener(Event.REMOVED,ficheSite_close);
				panel_fiche.addEventListener(CloseEvent.CLOSE,fermer);
		}
		private function fiche_handler(evt : MouseEvent):void
		{
			if(dg_site.selectedIndex != -1)
			{
				var panel_fiche : FicheSite = new FicheSite(dg_site.selectedItem as Site,false,this);
				PopUpManager.addPopUp(panel_fiche,this,true);//Modification
				PopUpManager.centerPopUp(panel_fiche);
				//panel_fiche.addEventListener(Event.REMOVED,ficheSite_close);
				panel_fiche.addEventListener(CloseEvent.CLOSE,fermer);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'S_lectionnez_un_site'));
			}
		}
		private function ficheSite_close(evt : CloseEvent):void
		{
			initData();
		}
		private function supprimer_site_handler(evt : MouseEvent):void
		{
			if(dg_site.selectedIndex!=-1)
			{
				var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"removeSite",
																		process_removeSite);
				RemoteObjectUtil.callService(op,(dg_site.selectedItem as Site).id);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'S_lectionnez_un_site'));
			}
		}
		private function process_removeSite(re : ResultEvent):void
		{
			if(re.result == -3)
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'Impossible_de_supprimer_un_site__tant_af'));
			}
			else
			{
			Alert.show(ResourceManager.getInstance().getString('M21', 'Site_supprim_'));
			initData();
			initData_pool();
			}
		}
		
	}
}