package univers.parametres.login
{
    // itemRenderers/tree/myComponents/MyTreeItemRenderer.as
   import mx.resources.ResourceManager;
	import flash.events.MouseEvent;
   
   import mx.collections.*;
   import mx.controls.treeClasses.*;
   import mx.controls.Image;
   import mx.core.IFlexDisplayObject;
   import flash.display.DisplayObject;
   import mx.controls.Alert;
   import flash.events.Event;
   
   public class MyTreeItemRenderer extends TreeItemRenderer {
   		[Embed(source="../../../assets/images/warning.png")]    
		private var imgInfo : Class;
		[Embed(source="../../../assets/images/nav_right_green.png")]    
		private var imgIconAcces : Class;
		
		private var affiche : int = 0;
     
     	private var img:Image = new Image();
     	 
      
      public function MyTreeItemRenderer() {
         super();
         //Add the image to the renderer
         this.img.setVisible(false);
         this.img.buttonMode = true;
    	 this.addChild(this.img as DisplayObject);
         
      }
      private function afficheAide(event : MouseEvent):void{
      	//Alert.show("Pour ajouter un accès à ce noeud, supprimez les accès éxistants");
      }
      private function supprimerAcces(event : MouseEvent):void{
      		//Alert.show(ResourceManager.getInstance().getString('M21', 'Supprimer'));
      }
       override public function set data(data:Object):void {
		if(data != null){
			super.data = data;  		
				this.label.text = data.@LBL;
	           	if(data.hasOwnProperty("@isInterdit")){
	           		if(data.@isInterdit==true){
	           			this.img.source = imgInfo;
	           			this.img.toolTip = ResourceManager.getInstance().getString('M21', 'Le_client_s_lectionn__poss_de_un_acc_s_d');
	           			img.addEventListener(MouseEvent.CLICK,afficheAide);
		       			affiche = 1;
		       	       	invalidateDisplayList();
		         	}
		          	else
		          	{
		         		affiche = 0;
		         		if(data.hasOwnProperty("@isAcces")){
		        		if(data.@isAcces==true){
		        			this.img.source = imgIconAcces;
		        			this.img.toolTip = ResourceManager.getInstance().getString('M21', 'Le_client_s_lectionn__poss_de_un_acc_s__');
		        			img.addEventListener(MouseEvent.CLICK,supprimerAcces);
		        			affiche = 1;
		       	       		invalidateDisplayList();
		       	       	}
		        	}
		          }
		        } 
		        else
		       	{
		        	affiche = 0;
		        }
		    }
       }                   
      
      
      override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
         super.updateDisplayList(unscaledWidth, unscaledHeight);
         invalidateDisplayList();
           if(data!=null) {
               if(affiche==1){
           			this.img.x = this.getChildAt(3).x + 19  ;
            		this.img.y = this.getChildAt(2).y+1 ;
           			this.img.width = 16;
            		this.img.height = 16;
	           		this.label.x += 20;
	            	this.img.visible=true;
	             		
	            }
	            else
	            {
	            	this.img.visible=false;
	            	affiche = 0;
	            	this.img.visible=false;
	            	this.label.x += 0;
	            	this.img.width = 0;
           			this.img.height = 0;
           		}            	
            
           //Hide the original icon
           // this.getChildAt(3).visible = false;
            
            
         }
      }
      
   }
}

