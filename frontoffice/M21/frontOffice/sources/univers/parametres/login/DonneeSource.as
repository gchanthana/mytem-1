package univers.parametres.login
{
	import mx.resources.ResourceManager;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class DonneeSource
	{
		private var _droitLogin : int;
		private var _idGroupeClient : int;
		private var _nbMaxLogin : int;
		private var _nbLogin : int;
		private var _colLogin : ArrayCollection;
		private var _objGroupeRacine : Object;
		private var _interface : GestionLogin;
		private var _dataArbre : XML;
		private var _idPerimetre : int;
		private var _idappLogin : int;
		
		public function DonneeSource(objPanel : GestionLogin)
		{
			var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
			var PERIMETRE_INDEX : int = perimetreObj.PERIMETRE_INDEX; // idPerimetre ( groupe client) 
			
			_idappLogin = sessionObj.USER.CLIENTACCESSID;
			
			this._idPerimetre = GROUPE_INDEX;
			this._idGroupeClient = GROUPE_INDEX;
			/*this._droitLogin = 2;
			this._nbLogin = 10;
			this._nbMaxLogin = 20;*/
			this._interface = objPanel;
			initInfosClient();
		}
		public function get droitLogin():int
		{
			return _droitLogin;
		}
		public function get IDLogin():int
		{
			return _idappLogin;
		}
		public function get perimetre():int
		{
			return _idPerimetre;
		}
		public function set droitLogin(newDroit : int):void
		{
			this._droitLogin = newDroit;
		}
		public function get idGroupeClient() : int
		{
			return _idGroupeClient;
		}
		public function get nbMaxLogin() : int
		{
			return _nbMaxLogin;
		}
		public function get nbLogin() : int
		{
			return _nbLogin;
		}
		public function get dataArbre() : XML
		{
			return _dataArbre;
		}
		public function get objGroupeRacine() : Object
		{
			return _objGroupeRacine;
		}
		public function get colLogin() : ArrayCollection
		{
			return _colLogin;
		}
		private function initInfosClient():void{
			//var sessionObj:IConsoViewSessionObject = UniversManager.getSession();
			_droitLogin = 2;
			getLoginOfGroupe();
		}
		public function getLoginOfGroupe():void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.parametres.login.GestionClient",
							"getListeLogin",
							resultGetLoginOfGroupe,gestionErreur);
			
			RemoteObjectUtil.callService(opData,_idGroupeClient);
		}
		private function resultGetLoginOfGroupe(evt : ResultEvent):void{
			_colLogin = evt.result as ArrayCollection;
			//
			
			getRacineInfos();
			
		}
		private function getRacineInfos():void {
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.parametres.login.GestionClient",
							"getGroupesMaitres",
							resultGetRacineInfos,gestionErreur);
			
			RemoteObjectUtil.callService(opData,idGroupeClient);
		}
		private function resultGetRacineInfos(evt : ResultEvent):void
		{
			
			var tempCol : ArrayCollection = evt.result as ArrayCollection ;
			 _objGroupeRacine = tempCol.getItemAt(0);
			initArbre();
		}
		private function gestionErreur(evt : FaultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M21', 'Erreur_gestion_des_login___')+ evt.toString());	
		}
		public function initArbre(modeInit : Boolean = false):void{
			var functionResult : Function=resultInitArbre;
			if( modeInit == true)
			{
				functionResult = initArbreInInterface;
			}
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.parametres.login.GestionClient",
								"getArbre",
								functionResult,gestionErreur);
					
			RemoteObjectUtil.callService(opData,_idGroupeClient);
		}
		private function resultInitArbre(evt :ResultEvent):void{
			_dataArbre = evt.result as XML;
			trace(_dataArbre.toXMLString());
			_interface.afficheData();
		}
		private function initArbreInInterface(evt :ResultEvent):void		
		{
			_dataArbre = evt.result as XML;
			trace(_dataArbre.toXMLString());
			_interface.setDataArbre(_dataArbre);
		}
		

	}
}