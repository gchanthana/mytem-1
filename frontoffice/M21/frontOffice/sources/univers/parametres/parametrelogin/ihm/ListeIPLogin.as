package univers.parametres.parametrelogin.ihm
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.service.LoginService;
	
	[Bindable]
	public class ListeIPLogin extends VBox
	{
		public var loginService : LoginService= new LoginService();
		
		
		public var idLogin : int;
		public var groupeIndex : int;
		
		//COMPONANT
		public var dg_IP: DataGrid;
		public var popIP : PopUpAdresseIP;
				
		
		public function ListeIPLogin()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		public function init(evt : FlexEvent=null):void
		{
			loginService.listeIPOfLogin(idLogin);
		}
		public function modifier(item : Object):void
		{
			var adresseIp : String = item.ADRESSE_IP;
			var tabIp : Array= adresseIp.split(".");
			popIP = new PopUpAdresseIP(idLogin,tabIp[0],tabIp[1],tabIp[2],tabIp[3],item.MASQUE1,item.COMMENTAIRE);
			addPop();
		}
		public function ajouter():void
		{
			popIP = new PopUpAdresseIP(idLogin);
			addPop();
		}
		
		private function addPop():void
		{
			popIP.addEventListener(GestionLoginEvent.PARAMETRE_IP_COMPLETE,iniData);
			PopUpManager.addPopUp(popIP,this.parentApplication as DisplayObject,true);
			PopUpManager.centerPopUp(popIP);
		}
		private function iniData(evt : GestionLoginEvent):void
		{
			init();
		}
		public function supprimer(item : Object):void
		{
			var msg : String = ResourceManager.getInstance().getString('M21', '_tes_vous_s_r_de_vouloir_supprimer_cette');	
			ConsoviewAlert.afficherAlertConfirmation(msg,ResourceManager.getInstance().getString('M21', 'Confirmation'),supprimerBtnValiderCloseEvent);
		}
		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				loginService.addEventListener(GestionLoginEvent.DELETE_IP_COMPLETE,removeIP_handler);
				loginService.removeAdresseIP(idLogin,dg_IP.selectedItem.ADRESSE_IP,dg_IP.selectedItem.MASQUE1);
			}
		}
		private function removeIP_handler(evt : GestionLoginEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'L_adresse_IP_a_bien__t__supprim_e_'));
			init();
		}
		
		
		
	}
}