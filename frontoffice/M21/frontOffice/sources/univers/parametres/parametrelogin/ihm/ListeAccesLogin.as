package univers.parametres.parametrelogin.ihm
{
    import flash.display.DisplayObject;
    import flash.events.MouseEvent;
    
    import mx.collections.ArrayCollection;
    import mx.containers.VBox;
    import mx.controls.Alert;
    import mx.controls.DataGrid;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.resources.ResourceManager;
    
    import composants.util.ConsoviewAlert;
    
    import univers.parametres.parametrecompte.vo.Gestionnaire;
    import univers.parametres.parametrelogin.event.GestionLoginEvent;
    import univers.parametres.parametrelogin.service.LoginService;
    import univers.parametres.parametrelogin.util.DuplicateUserFeatureIHM;
    import univers.parametres.parametrelogin.vo.AccesCompte;

    [Bindable]
    public class ListeAccesLogin extends VBox
    {
        public var loginService:LoginService = new LoginService();
        public var idLogin:int;
        public var groupeIndex:int;
		public var login:Gestionnaire=new Gestionnaire();
		private var _allUsersCol:ArrayCollection = new ArrayCollection();
        //COMPONANT
        public var dg_acces:DataGrid;
        public var popAbonnement:ParametreAbonnementIHM;
		private var _typeLogin:Number = 0;
		
        public function ListeAccesLogin()
        {
            groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
        }

        public function init(evt:FlexEvent = null):void
        {
            initializeAccess();
        }

        public function initializeAccess():void
        {
            if(idLogin > 0)
            {
                loginService.getAccesOfLogin(idLogin, groupeIndex);
            }
        }

        public function onParamLoginCompleteHandler(event:Event):void
        {
            loginService.removeEventListener(GestionLoginEvent.PARAMETRE_LOGIN_COMPLETE, onParamLoginCompleteHandler);
            loginService.getAccesOfLogin(idLogin, groupeIndex);
        }

        public function modifier(item:Object):void
        {
            popAbonnement = new ParametreAbonnementIHM();
            popAbonnement.col_all_acces_of_client = loginService.col_acces_not_formated;
            popAbonnement.idLogin = idLogin;
            popAbonnement.accesCompte = dg_acces.selectedItem as AccesCompte;
            addPop();
        }

        public function ajouter():void
        {
            popAbonnement = new ParametreAbonnementIHM();
            popAbonnement.col_all_acces_of_client = loginService.col_acces_not_formated;
            popAbonnement.idLogin = idLogin;
            addPop();
        }

        private function addPop():void
        {
            popAbonnement.addEventListener(GestionLoginEvent.PARAMETRE_ABONNEMENT_COMPLETE, iniData);
            PopUpManager.addPopUp(popAbonnement, this.parentApplication as DisplayObject, true);
            PopUpManager.centerPopUp(popAbonnement);
        }

        private function iniData(evt:GestionLoginEvent):void
        {
            init();
        }

        public function supprimer(item:AccesCompte):void
        {
            var msg:String = ResourceManager.getInstance().getString('M21', '_tes_vous_s_r_de_vouloir_supprimer_l_abo') + item.libelle + ResourceManager.getInstance().getString('M21', '__');
            ConsoviewAlert.afficherAlertConfirmation(msg, ResourceManager.getInstance().getString('M21', 'Confirmation'), supprimerBtnValiderCloseEvent);
        }

        private function supprimerBtnValiderCloseEvent(e:CloseEvent):void
        {
            if(e.detail == Alert.OK)
            {
                loginService.addEventListener(GestionLoginEvent.DELETE_ACCES_COMPLETE, remove_acces_handler);
                loginService.deleteAcces(idLogin, (dg_acces.selectedItem as AccesCompte).idAcces);
            }
        }

        private function remove_acces_handler(evt:GestionLoginEvent):void
        {
            ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'L_abonnement_a_bien__t__supprim__'));
            init();
        }
		
		protected function onClickDuplicateAllAccesHandler(event:MouseEvent):void
		{
			// creation de la popup d'affectation
			var duplicatePopup:DuplicateUserFeatureIHM = new DuplicateUserFeatureIHM();
			PopUpManager.addPopUp(duplicatePopup, this.parent.parent, true);
			PopUpManager.centerPopUp(duplicatePopup);
			duplicatePopup.duplicateAll = true;
			duplicatePopup.refUser = login;
			
			duplicatePopup.usersCollec = allUsersCol;
			duplicatePopup.removeRefUser(duplicatePopup.usersCollec, String(login.id));
			
		}
		
		public function get allUsersCol():ArrayCollection { return _allUsersCol; }
		
		public function set allUsersCol(value:ArrayCollection):void
		{
			if (_allUsersCol == value)
				return;
			_allUsersCol = value;
		}
		
		public function get typeLogin():Number { return _typeLogin; }
		
		public function set typeLogin(value:Number):void
		{
			if (_typeLogin == value)
				return;
			_typeLogin = value;
		}
    }
}