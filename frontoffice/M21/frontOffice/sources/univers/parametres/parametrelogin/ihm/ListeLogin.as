package univers.parametres.parametrelogin.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import composants.util.ConsoviewAlert;
	
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.event.GestionTypeCommandeEvent;
	import univers.parametres.parametrecompte.vo.Gestionnaire;
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.event.UserAccountEvent;
	import univers.parametres.parametrelogin.service.LoginService;
	import univers.parametres.parametrelogin.service.UserAccountService;
	import univers.parametres.parametrelogin.util.DuplicateUserFeatureIHM;
	import univers.parametres.parametrelogin.vo.UserAccountStateVo;
	
	[Bindable]
	public class ListeLogin extends VBox
	{
		private const ID_CONSOTEL : int = 310812;
		public var loginService: LoginService = new LoginService();
		public var userAccountService:UserAccountService = new UserAccountService();
		//UI COMPONANT
		private var popupParametreLogin : ParametreLoginIHM;
		public var dgLogin : DataGrid;
		public var txtFiltre : TextInput;
		public var boolCreaLoginAutorised : Boolean = true;
		
		
		public function ListeLogin()
		{
			
		}
		public function init(evt : FlexEvent=null):void
		{
			//définir le nb max login autorisé sur le groupe :
			loginService.addEventListener(GestionLoginEvent.INFO_ACCES_COMPLETE,info_groupe_complete);
			loginService.getRacineInfos(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
			txtFiltre.setFocus();
			loginService.getIdProfilUser();
			trace("####### loginService.idProfil  ::  " + loginService.idProfil);
		}
				
		public function getListUserAccount():void
		{
			loginService.addEventListener(GestionLoginEvent.LISTE_LOGIN_COMPLETE,listeLoginComplete);
			loginService.listeLogin(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);	
		}
		
		public function imgActualiserHandler():void
		{
			getListUserAccount();
		}
		
		public function lockUserAcccount(userAccount:Gestionnaire):void
		{	
			userAccountService.model.addEventListener(UserAccountEvent.LOCK_COMPLETE,userAccountEventLockHandler)
			userAccountService.updateUserAccountState
			(
				new UserAccountStateVo
				(
					userAccount.id,
					0,
					UserAccountStateVo.LOKED
				)
			)
		}
		
		public function unlockUserAcccount(userAccount:Gestionnaire):void
		{
			userAccountService.model.addEventListener(UserAccountEvent.UNLOCK_COMPLETE,userAccountEventUnlockHandler)
			userAccountService.updateUserAccountState
			(
				new UserAccountStateVo
				(
					userAccount.id,
					0,
					UserAccountStateVo.UNLOKED
				)
			)
		}
		
		public function deleteUserAcccount(userAccount:Gestionnaire):void
		{
			userAccountService.model.addEventListener(UserAccountEvent.DELETE_COMPLETE,userAccountEventDeleteHandler)
			userAccountService.updateUserAccountState
			(
				new UserAccountStateVo
				(
					userAccount.id,
					0,
					UserAccountStateVo.EXPIRED
				)
			)
		}
		
		private function userAccountEventDeleteHandler(event:UserAccountEvent):void
		{
			var msg : String = ResourceManager.getInstance().getString('M21', 'L_utilisateur___bien__t__supprim_');
			ConsoviewAlert.afficherOKImage(msg);
			
			userAccountService.model.removeEventListener(UserAccountEvent.DELETE_COMPLETE,userAccountEventDeleteHandler);
			getListUserAccount()
		}
		
		private function userAccountEventLockHandler(event:UserAccountEvent):void
		{
			userAccountService.model.removeEventListener(UserAccountEvent.LOCK_COMPLETE,userAccountEventLockHandler);
			getListUserAccount()
		}
		
		private function userAccountEventUnlockHandler(event:UserAccountEvent):void
		{
			userAccountService.model.removeEventListener(UserAccountEvent.UNLOCK_COMPLETE,userAccountEventLockHandler);
			getListUserAccount()	
		}
		
		
		private function info_groupe_complete(evt : GestionLoginEvent):void
		{	
			getListUserAccount();
		}
		
		
		private function listeLoginComplete(evt : GestionLoginEvent):void
		{
			loginService.removeEventListener(GestionLoginEvent.LISTE_LOGIN_COMPLETE,listeLoginComplete);
			
			loginService.col_login;
			var marge : int = loginService.objGroupeClient.MARGE_CONTRACTUELLE;
			var nb_login : int =  loginService.objGroupeClient.NB_LOGIN;
			
			var nb_max_login : int =  ((marge/100)*nb_login)+nb_login;
			
			if(loginService.col_login.length>=nb_max_login)
			{
				boolCreaLoginAutorised = false;
			}
			else
			{
				boolCreaLoginAutorised = true;
			}
			
			filtreHandler();
			
		}
		public function modifier(item : Gestionnaire):void
		{
			popupParametreLogin = new ParametreLoginIHM();
			popupParametreLogin.login = item;
			popupParametreLogin.allUsersCol = ObjectUtil.copy(loginService.col_login) as ArrayCollection
			addPop();
		}
		private function listeContrainteOfPoolHandler():void
		{
			
		}
		
		private function remove_login_complete_handler(evt  :GestionLoginEvent):void
		{
			var msg : String = ResourceManager.getInstance().getString('M21', 'L_utilisateur___bien__t__supprim_');
			ConsoviewAlert.afficherOKImage(msg);
			init();
		}
		public function addUser(event:MouseEvent):void
		{
			popupParametreLogin = new ParametreLoginIHM();
			addPop();
		}
		
		public function supprimer(data : Gestionnaire):void
		{
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M21', 'p_interog_inv')
			+' '+ResourceManager.getInstance().getString('M21', '_tes_vous_s_r_de_vouloir_supprimer_l_uti')+' '+data.nom_prenom+' '+ResourceManager.getInstance().getString('M21', 'p_interog'),ResourceManager.getInstance().getString('M21', 'Confirmation'),supprimerBtnValiderCloseEvent);
		}
		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK)
			{
				deleteUserAcccount(dgLogin.selectedItem as Gestionnaire);
			}
		}
		private function addPop():void
		{
			popupParametreLogin.addEventListener(GestionTypeCommandeEvent.TYPE_COMMANDE_CHANGE,info_type_commande_change_handler);
			popupParametreLogin.addEventListener(GestionPoolEvent.UPDATE_PROFIL_LOGIN,info_pool_change_handler);
			popupParametreLogin.addEventListener(GestionLoginEvent.PARAMETRE_LOGIN_COMPLETE,param_login_complete_handler);
			PopUpManager.addPopUp(popupParametreLogin,this,true);
			PopUpManager.centerPopUp(popupParametreLogin);
		}
		private function info_pool_change_handler(evt : GestionPoolEvent):void
		{
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.UPDATE_PROFIL_LOGIN,true));
		}
		private function info_type_commande_change_handler(evt : GestionTypeCommandeEvent):void
		{
			dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.TYPE_COMMANDE_CHANGE,true));	
		}
		
		private function param_login_complete_handler(evt : GestionLoginEvent):void
		{
			init();
		}
		protected function filtreHandler(e:Event = null):void{
			loginService.col_login.filterFunction= filtreGrid;
			loginService.col_login.refresh();
		}
		private function filtreGrid(item:Object):Boolean{
			if(item != null)
			{
				if (((item.nom_prenom as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1 ||
					((item.mail as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1)
				{
					return true;
				}
				else
				{
					return false;	
				}
			}
			else
			{
				return false;
			}
		}
		
		/**
		 *  Ouvre la fenêtre de Duplication de tous les accès ( pour toutes les racines )
		 *  pour l'utilisateur séléctionné dans le DATAGIRD
		 * */
		public function duplicateAllAccess(userObj:Gestionnaire):void
		{
				// creation de la popup d'affectation
				var duplicatePopup:DuplicateUserFeatureIHM = new DuplicateUserFeatureIHM();
				PopUpManager.addPopUp(duplicatePopup, this, true);
				PopUpManager.centerPopUp(duplicatePopup);
				duplicatePopup.duplicateAll = true;
				duplicatePopup.refUser = userObj;
				
				duplicatePopup.usersCollec = ObjectUtil.copy(loginService.col_login) as ArrayCollection
				duplicatePopup.removeRefUser(duplicatePopup.usersCollec, String(userObj.id));				
		}
	}
}