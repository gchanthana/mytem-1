package univers.parametres.parametrelogin.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import univers.parametres.parametrelogin.event.AccessEvent;
	import univers.parametres.parametrelogin.service.AccessService;
	import univers.parametres.parametrelogin.vo.AccessVO;

	public class PopUpAccess extends TitleWindow
	{
		
		[Bindable]
		public var accessService:AccessService;
		[Bindable]
		public var dgAccessDetail:DataGrid;
		
		public function PopUpAccess()
		{
		}
		
		public function init():void{
			accessService.model.addEventListener(AccessEvent.ACTIVEPARAM_COMPLETE, accessEventCompleteHandler);
			accessService.model.addEventListener(AccessEvent.ACTIVEPARAM_ERROR, accessEventErrorHandler);
		}
		
		protected function accessEventErrorHandler(event:Event):void
		{
			// TODO Auto-generated method stub			 
			ConsoviewAlert.afficherAlertInfo(accessService.model.error_message,
				ResourceManager.getInstance().getString('M21', 'Erreur_lors_de_la_sauvegarde'),
				null);
		}
		
		public function datagridDataTipFunction(item:Object):String
		{
			return item.commentaire;
		}
		
		public function accessEventCompleteHandler(event:AccessEvent):void{			
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Les_modifications_ont_bien__t__enregistr'));
			
		}
		
		public function fermer(evt:Event):void
		{
			accessService.model.removeEventListener(AccessEvent.ACTIVEPARAM_COMPLETE,accessEventCompleteHandler);
			accessService.model.removeEventListener(AccessEvent.ACTIVEPARAM_ERROR, accessEventErrorHandler);
			PopUpManager.removePopUp(this);
		}
		
		public function activeParam():void{
			accessService.activeParam();
		}
		
		
		protected function dgAccessDetail_itemClickHandler(evt:ListEvent):void
		{
			var _ref:AccessVO = dgAccessDetail.selectedItem as AccessVO;
			if(_ref != null)
			{
				_ref.check(!_ref.selected)		
			}
		}
		
		
	}
}