package univers.parametres.parametrelogin.vo
{
	[Bindable]
	public class ChampsPersosVO
	{
		private var _CP_ID:int;
		private var _CP_Libelle_Defaut:String;
		private var _CP_Libelle_Client:String;
		private var _CP_Type_Commande:String; 	// M (mobile) ou FR (fixe et reseau)	 
		private var _CP_Code_Champ:String;
		private var _CP_Libelle_Client_Reference:String;
		private var _boolEdited:Boolean;
		private var _Selected:Boolean = false;
		private var _ordreAffichage:int;
		private var _boolFocus:Boolean = false
		
		public function ChampsPersosVO()
		{
		}


		public function set CP_Code_Champ(value:String):void
		{
			_CP_Code_Champ = value;
		}

		public function get CP_Code_Champ():String
		{
			return _CP_Code_Champ;
		}

		public function set CP_Type_Commande(value:String):void
		{
			_CP_Type_Commande = value;
		}

		public function get CP_Type_Commande():String
		{
			return _CP_Type_Commande;
		}

		public function set CP_Libelle_Client(value:String):void
		{
			_CP_Libelle_Client = value;
			if(_CP_Libelle_Client != null && _CP_Libelle_Client != "")
				boolEdited = true
			else
				boolEdited = false
		}

		public function get CP_Libelle_Client():String
		{
			return _CP_Libelle_Client;
		}

		public function set CP_Libelle_Defaut(value:String):void
		{
			_CP_Libelle_Defaut = value;
		}

		public function get CP_Libelle_Defaut():String
		{
			return _CP_Libelle_Defaut;
		}

		public function set CP_ID(value:int):void
		{
			_CP_ID = value;
		}

		public function get CP_ID():int
		{
			return _CP_ID;
		}

		public function set CP_Libelle_Client_Reference(value:String):void
		{
			_CP_Libelle_Client_Reference = value;
		}

		public function get CP_Libelle_Client_Reference():String
		{
			return _CP_Libelle_Client_Reference;
		}

		public function set boolEdited(value:Boolean):void
		{
			_boolEdited = value;
		}

		public function get boolEdited():Boolean
		{
			return _boolEdited;
		}

		public function set Selected(value:Boolean):void
		{
			_Selected = value;
		}

		public function get Selected():Boolean
		{
			return _Selected;
		}

		public function set ordreAffichage(value:int):void
		{
			_ordreAffichage = value;
		}

		public function get ordreAffichage():int
		{
			return _ordreAffichage;
		}

		public function set boolFocus(value:Boolean):void
		{
			_boolFocus = value;
		}

		public function get boolFocus():Boolean
		{
			return _boolFocus;
		}
	}
}