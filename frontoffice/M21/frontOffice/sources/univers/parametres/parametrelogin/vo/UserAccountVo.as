package univers.parametres.parametrelogin.vo
{
	[Bindable]
	public class UserAccountVo
	{
		public function UserAccountVo(id:Number,
									  idClient:Number,
									  code_application:Number,
									  nom:String,
									  prenom:String,
									  email:String,
									  password:String)
		{
			_APP_LOGINID = id;			
			_APPLICATIONID = code_application;
			_LOGIN_NOM = nom;
			_LOGIN_PRENOM  = prenom;
			_LOGIN_EMAIL = email;
			_LOGIN_PWD = password;
		}
		
		private var	_APPLICATIONID			: Number =0;
		private var	_APP_LOGINID			: Number =0;		
		private var	_IDGROUPE_CLIENT		: Number =0;
		private var	_LOGIN_EMAIL			: String ="";
		private var	_LOGIN_NOM				: String ="";
		private var	_LOGIN_PRENOM			: String ="";
		private var	_LOGIN_PWD				: String ="";		
		
		private var	_FAILED_CONNECT			: Number = 0;
		private var	_LOGIN_STATUS			: Number = 0;
		
		private var	_ADRESSE_POSTAL			: String ="";		
		private var	_CODE_POSTAL			: String ="";
		private var	_DIRECTION				: String ="";
		private var	_VILLE_POSTAL			: String ="";
		private var	_TELEPHONE				: String ="";
		
		private var	_GLOBALIZATION			: String ="";		
		
		private var	_IDPROFIL_THEME			: Number =0;
		private var	_IDREVENDEUR			: Number =0;
		private var	_LIBELLE_TYPE_PROFIL	: String ="";		
		private var	_BOOL_ACCES_PERIMETRE	: Number =0;
		private var	_CODE_LANGUE			: String ="";
		private var	_RACINE_APPARTENANCE	: String ="";
		private var	_RESTRICTION_IP			: Number =0;
		private var	_TYPE_PROFIL			: Number =0;
		
		
		
		
		public function get APP_LOGINID():Number
		{
			return _APP_LOGINID;
		}
		

		public function get APPLICATIONID():Number
		{
			return _APPLICATIONID;
		}

		public function get ADRESSE_POSTAL():String
		{
			return _ADRESSE_POSTAL;
		}

		public function set ADRESSE_POSTAL(value:String):void
		{
			_ADRESSE_POSTAL = value;
		}

		public function get BOOL_ACCES_PERIMETRE():Number
		{
			return _BOOL_ACCES_PERIMETRE;
		}

		public function set BOOL_ACCES_PERIMETRE(value:Number):void
		{
			_BOOL_ACCES_PERIMETRE = value;
		}

		public function get CODE_LANGUE():String
		{
			return _CODE_LANGUE;
		}

		public function set CODE_LANGUE(value:String):void
		{
			_CODE_LANGUE = value;
		}

		public function get CODE_POSTAL():String
		{
			return _CODE_POSTAL;
		}

		public function set CODE_POSTAL(value:String):void
		{
			_CODE_POSTAL = value;
		}

		public function get DIRECTION():String
		{
			return _DIRECTION;
		}

		public function set DIRECTION(value:String):void
		{
			_DIRECTION = value;
		}

		

		public function get GLOBALIZATION():String
		{
			return _GLOBALIZATION;
		}

		public function set GLOBALIZATION(value:String):void
		{
			_GLOBALIZATION = value;
		}

		public function get IDGROUPE_CLIENT():Number
		{
			return _IDGROUPE_CLIENT;
		}

		public function set IDGROUPE_CLIENT(value:Number):void
		{
			_IDGROUPE_CLIENT = value;
		}

		public function get IDPROFIL_THEME():Number
		{
			return _IDPROFIL_THEME;
		}

		public function set IDPROFIL_THEME(value:Number):void
		{
			_IDPROFIL_THEME = value;
		}

		public function get IDREVENDEUR():Number
		{
			return _IDREVENDEUR;
		}

		public function set IDREVENDEUR(value:Number):void
		{
			_IDREVENDEUR = value;
		}

		public function get LIBELLE_TYPE_PROFIL():String
		{
			return _LIBELLE_TYPE_PROFIL;
		}

		public function set LIBELLE_TYPE_PROFIL(value:String):void
		{
			_LIBELLE_TYPE_PROFIL = value;
		}

		public function get LOGIN_EMAIL():String
		{
			return _LOGIN_EMAIL;
		}

		public function set LOGIN_EMAIL(value:String):void
		{
			_LOGIN_EMAIL = value;
		}

		public function get LOGIN_NOM():String
		{
			return _LOGIN_NOM;
		}

		public function set LOGIN_NOM(value:String):void
		{
			_LOGIN_NOM = value;
		}

		public function get LOGIN_PRENOM():String
		{
			return _LOGIN_PRENOM;
		}

		public function set LOGIN_PRENOM(value:String):void
		{
			_LOGIN_PRENOM = value;
		}

		public function get LOGIN_PWD():String
		{
			return _LOGIN_PWD;
		}

		public function set LOGIN_PWD(value:String):void
		{
			_LOGIN_PWD = value;
		}

		

		public function get RACINE_APPARTENANCE():String
		{
			return _RACINE_APPARTENANCE;
		}

		public function set RACINE_APPARTENANCE(value:String):void
		{
			_RACINE_APPARTENANCE = value;
		}

		public function get RESTRICTION_IP():Number
		{
			return _RESTRICTION_IP;
		}

		public function set RESTRICTION_IP(value:Number):void
		{
			_RESTRICTION_IP = value;
		}

		public function get TELEPHONE():String
		{
			return _TELEPHONE;
		}

		public function set TELEPHONE(value:String):void
		{
			_TELEPHONE = value;
		}

		public function get TYPE_PROFIL():Number
		{
			return _TYPE_PROFIL;
		}

		public function set TYPE_PROFIL(value:Number):void
		{
			_TYPE_PROFIL = value;
		}

		public function get VILLE_POSTAL():String
		{
			return _VILLE_POSTAL;
		}

		public function set VILLE_POSTAL(value:String):void
		{
			_VILLE_POSTAL = value;
		}
		
		public function get FAILED_CONNECT():Number
		{
			return _FAILED_CONNECT;
		}
		
		public function set FAILED_CONNECT(value:Number):void
		{
			_FAILED_CONNECT = value;
		}
		
		public function get LOGIN_STATUS():Number
		{
			return _LOGIN_STATUS;
		}		

		public function set LOGIN_STATUS(value:Number):void
		{
			_LOGIN_STATUS = value;
		}


	}
}