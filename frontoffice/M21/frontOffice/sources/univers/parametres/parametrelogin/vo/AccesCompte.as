package univers.parametres.parametrelogin.vo
{
	[Bindable]
	public class AccesCompte
	{
		public var libelle : String;
		public var idGroupeMaitre : int;
		public var idLogin : int;
		public var idAcces : int;
		public var libellegm : String;
		public var TYPE:String;
		
		//
		public var DATE_CREATE:Date = null;
		public var module_usage : int;
		public var module_fixe_data : int;
		public var module_gestion_login : int;
		public var module_mobile : int;
		public var module_fournisseur : int;
		public var module_facturation : int;
		public var module_gestion_orga : int;
		public var module_workflow : int;
		
					
		public function AccesCompte()
		{
		} 

	}
}