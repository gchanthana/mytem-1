package  univers.parametres.parametrelogin.vo
{
	import flash.events.EventDispatcher;
	import flash.utils.describeType;
	
	import mx.events.PropertyChangeEvent;

	
	
	
	public class ModuleVo extends EventDispatcher
	{
		private var _CODE_MODULE : String  = "";
		private var _ACCESS_USER : Number = 0;
		private var _NOM_MODULE : String ="";
		private var _ACCESS_RACINE : Number = 0;
		private var _MODULE_CONSOTELID : Number = -1;
		private var _IDGROUPE_CLIENT : Number = -1;
		private var _HAS_PARAMS : Number = 0;
		
		public function ModuleVo()
		{	
		}
	
		/**
		 * data/source property getters
		 */
		
		[Bindable(event="propertyChange")]
		public function get CODE_MODULE() : String
		{
			return _CODE_MODULE;
		}
		
		[Bindable(event="propertyChange")]
		public function get HAS_PARAMS() : Number
		{
			return _HAS_PARAMS;
		}
		
		[Bindable(event="propertyChange")]
		public function get ACCESS_USER() : Number
		{
			return _ACCESS_USER;
		}
		
		[Bindable(event="propertyChange")]
		public function get NOM_MODULE() : String
		{
			return _NOM_MODULE;
		}
		
		[Bindable(event="propertyChange")]
		public function get ACCESS_RACINE() : Number
		{
			return _ACCESS_RACINE;
		}
		
		[Bindable(event="propertyChange")]
		public function get MODULE_CONSOTELID() : Number
		{
			return _MODULE_CONSOTELID;
		}
		
		[Bindable(event="propertyChange")]
		public function get IDGROUPE_CLIENT() : Number
		{
			return _IDGROUPE_CLIENT;
		}
		
		/**
		 * data/source property setters
		 */
		
		public function set CODE_MODULE(value:String) : void
		{
			var oldValue:String = _CODE_MODULE;
			if (oldValue !== value)
			{
				_CODE_MODULE = value;
				this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CODE_MODULE", oldValue, _CODE_MODULE));
			}
		}
		
		public function set HAS_PARAMS(value:Number) : void
		{
			var oldValue:Number = _HAS_PARAMS;
			if (oldValue !== value)
			{
				_HAS_PARAMS = value;
				this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HAS_PARAMS", oldValue, _HAS_PARAMS));
			}
		}
		
		public function set ACCESS_USER(value:Number) : void
		{
			var oldValue:Number = _ACCESS_USER;
			if (oldValue !== value)
			{
				_ACCESS_USER = value;
				this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCESS_USER", oldValue, _ACCESS_USER));
			}
		}
		
		public function set NOM_MODULE(value:String) : void
		{
			var oldValue:String = _NOM_MODULE;
			if (oldValue !== value)
			{
				_NOM_MODULE = value;
				this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NOM_MODULE", oldValue, _NOM_MODULE));
			}
		}
		
		public function set ACCESS_RACINE(value:Number) : void
		{
			var oldValue:Number = _ACCESS_RACINE;
			if (oldValue !== value)
			{
				_ACCESS_RACINE = value;
				this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCESS_RACINE", oldValue, _ACCESS_RACINE));
			}
		}
		
		public function set MODULE_CONSOTELID(value:Number) : void
		{
			var oldValue:Number = _MODULE_CONSOTELID;
			if (oldValue !== value)
			{
				_MODULE_CONSOTELID = value;
				this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MODULE_CONSOTELID", oldValue, _MODULE_CONSOTELID));
			}
		}
		
		public function set IDGROUPE_CLIENT(value:Number) : void
		{
			var oldValue:Number = _IDGROUPE_CLIENT;
			if (oldValue !== value)
			{
				_IDGROUPE_CLIENT = value;
				this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "IDGROUPE_CLIENT", oldValue, _IDGROUPE_CLIENT));
			}
		}
		
		public function cast(value:Object):Boolean
		{
			var _cast_status : Boolean = true; //true si le caste à fonctionné pour ttes les propriété 
			
			
			var classInfo 	:XML = describeType(this);
			
			for each (var v:XML in classInfo..accessor)
			{
				if (this.hasOwnProperty(v.@name))
				{
					this[v.@name] = value[v.@name];
				}
				else
				{
					_cast_status = false
				} 
			}
			
			return 	_cast_status
		}
	
	}

}