package univers.parametres.parametrelogin.event
{
	import flash.events.Event;

	public class AccessEvent extends Event
	{
			
		public static var ACCESS_COMPLETE:String = "AccesComplete"; 
		public static var ACTIVEPARAM_COMPLETE:String = "ActiveParamComplete"; 
		public static var ACTIVEPARAM_ERROR:String = "ActiveParamError";
		
		public function AccessEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new AccessEvent(type,bubbles,cancelable);
		}
		
	}
}