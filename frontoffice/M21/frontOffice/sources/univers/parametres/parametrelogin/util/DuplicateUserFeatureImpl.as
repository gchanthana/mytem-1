package univers.parametres.parametrelogin.util
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import univers.parametres.parametrecompte.vo.Gestionnaire;
	import univers.parametres.parametrelogin.event.UserEvent;
	import univers.parametres.parametrelogin.service.user.UserServices;
	
	/**
	 * Classe d'implémentation de l'interface de gestion de duplication
	 * des styles et accès d'un ensemble d'utilisateur à partir d'un
	 * utilisateur référent
	 */
	[Bindable]	
	public class DuplicateUserFeatureImpl extends TitleWindowBounds
	{
		// Utilisateur référents pour la duplication
		private var _refUser			:Gestionnaire;
		
		// liste de tous les utilisateurs appartenant aux meme groupe que l'utilisateur référents
		public var usersCollec:ArrayCollection = new ArrayCollection();
		// Déclarations des composants mxml  
		public var btn_ajouter		:Button;
		public var btn_retirer			:Button;
		public var btAnnuler			:Button;
		public var btValider			:Button;
		
		public var users_dg				:DataGrid;
		public var usersToAffect_dg		:DataGrid;
		public var txtFiltre			:TextInput;
		// Fiche utilisateur
		public var userName_lb			:Label;
		public var userSurname_lb		:Label;
		public var userMail_lb			:Label;
		
		// les services distants
		private var userServices		:UserServices;
		// la liste des utilisateur sélectionnés
		private var _selectedUsers:ArrayCollection;
		// controleur de la pagination
		// variable qui indique si on duplique tous les accès ou que celui du groupe séléctionné
		private var _duplicateAll:Boolean = false;
		private var _nbToAdd:Number = 0;
		private var _nbToRemove:Number = 0;
		
		public function DuplicateUserFeatureImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		/**********************************************************************************************
		 *		                         INITIALISATION
		 *********************************************************************************************/ /**
		* Initialisation du composant
		* @param evt
		*/
		private function init(evt:FlexEvent):void
		{
			try
			{
				selectedUsers = new ArrayCollection();
				initListeners();
			}
			catch (e:Error)
			{
				trace("DuplicateUserFeaturesImpl-init() Problème d'initialisation du composant : " + e);
			}
		}
		
		/**
		 * Initialisation des écouteurs
		 */
		private function initListeners():void
		{
			btValider.addEventListener(MouseEvent.CLICK, onValidateHandler);
			btAnnuler.addEventListener(MouseEvent.CLICK, onCancelHandler);
			
			btn_ajouter.addEventListener(MouseEvent.CLICK, onAjouterToAffectClickHandler);
			btn_retirer.addEventListener(MouseEvent.CLICK, onRetirerFromToAffectClickHandler);
			
			this.addEventListener(CloseEvent.CLOSE, onCancelHandler);
		}
		
		/**
		 * Initialisation des composants d'affichage
		 */
		private function initDisplay():void
		{
			// remplissage des champs utilisateur référent
			if (refUser)
			{
				userName_lb.text = refUser.nom;
				userSurname_lb.text = refUser.prenom;
				userMail_lb.text = refUser.mail;
			}
			else
			{
				userName_lb.text = "";
				userSurname_lb.text = "";
				userMail_lb.text = "";
			}
			
		}
		
		
		/**********************************************************************************************
		 *		                          USER ACTION HANDLERS
		 *********************************************************************************************/ /**
		* Déclenche la dupliction des caractéristiques
		* des utilisateurs sélectionnés dans le datagrid
		* ( envoi vers la couche metier pour traitement via web services )
		* @param evt
		* @throws (e)
		*/
		private function onValidateHandler(evt:MouseEvent):void
		{
			try
			{
				if (selectedUsers.length > 0)
				{
					userServices = new UserServices();
					userServices.myHandlers.addEventListener(UserEvent.DUPLICATE_PROFILE_SUCCESS, onServiceResultHandler);
					userServices.myHandlers.addEventListener(UserEvent.DUPLICATE_PROFILE_ERROR, onServiceResultHandler);
					var userRefId:Number = Number(refUser.id);
					var usersIdToAffect:Array = new Array();

					
					for (var i:int = 0; i < selectedUsers.length; i++)
					{
						usersIdToAffect.push(Number(selectedUsers[i].id));
					}
					
					userServices.duplicateAllUserProfile(Number(userRefId), usersIdToAffect);
				}
			}
			catch (e:Error)
			{
				throw new Error("DuplicateUserFeaturesImpl-onValidateHandler() # une erreur est survenue : impossible de dupliquer les styles et accès : " + e);
			}
		}
		
		/**
		 * Click sur le BOuton Annuler
		 * @param evt
		 */
		private function onCancelHandler(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		/**********************************************************************************************
		 *		                         WEBSERVICES RESULT HANDLERS
		 *********************************************************************************************/ /**
		*  Handler gérant le retour des appels distants concernant
		*  l'objet Utilisateur ( récupération accès et style , duplication...)
		*  @param evt
		*/
		private function onServiceResultHandler(evt:UserEvent):void
		{
			try
			{
				switch (evt.type)
				{
					// Cas : Erreur dans la duplication des profils
					case UserEvent.DUPLICATE_PROFILE_ERROR:
						ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M21', 'Une_erreur_s_est_produite_lors_de_la_mis'), ResourceManager.getInstance().getString('M21', 'Erreur__'));
						break;
					// Cas : Duplication du profil réussie
					case UserEvent.DUPLICATE_PROFILE_SUCCESS:
						PopUpManager.removePopUp(this);
						ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Les_changements_ont__t__sauvegard__avec_'));
						break;
					default:
						break;
				}
				removeServicesListeners();
			}
			catch (e:Error)
			{
				throw new Error(" ### DuplicateUserFeaturesImpl - onServiceResultHandler() evt: " + evt.type + " Erreur : " + e);
			}
		}
		
		public function initServicesListeners():void
		{
			
		}
		
		/**
		 * Destruction d'écouteurs
		 */
		public function removeServicesListeners():void
		{
			userServices.myHandlers.removeEventListener(UserEvent.DUPLICATE_PROFILE_SUCCESS, onServiceResultHandler);
			userServices.myHandlers.removeEventListener(UserEvent.DUPLICATE_PROFILE_ERROR, onServiceResultHandler);
		}		
		/**
		 * Suppime l'utilisateur référent de la liste
		 * @param datagrid
		 * @param app_loginid de l'user
		 * @return
		 */
		public function removeRefUser(collec:ArrayCollection, userId:String):void
		{
			if (collec && userId)
			{
				for (var i:int = 0; i < usersCollec.length; i++)
				{
					if (userId == usersCollec.getItemAt(i).id)
					{
						usersCollec.removeItemAt(i);
					}
				}
			}
		}
		
		public function filtreHandler(e:Event = null):void{
			usersCollec.filterFunction= filtreGrid;
			usersCollec.refresh();
		}
		public function filtreGrid(item:Object):Boolean{
			if(item != null)
			{
				if (
					((item.nom as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1 ||
					((item.prenom as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1 ||
					((item.mail as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1
					)
				{
					return true;
				}
				else
				{
					return false;	
				}
			}
			else
			{
				return false;
			}
		}
		
		protected function onAjouterToAffectClickHandler(event:MouseEvent):void
		{
			var suppIndices:Array =  new Array();
			
			for each (var user:Object in usersCollec) 
			{
				if(user.selected)
				{
					user.selected = false;
					selectedUsers.addItem(user);
					suppIndices.push(usersCollec.getItemIndex(user));
				}
			}
			
			suppIndices.sort(Array.DESCENDING);
			
			for(var i:int=0 ; i < suppIndices.length; i++) 
			{
				usersCollec.removeItemAt(suppIndices[i]);
			}
			
			nbToAdd = getSelectedItems(usersCollec);
		}
		
		protected function onRetirerFromToAffectClickHandler(event:MouseEvent):void
		{
			var suppIndices:Array =  new Array();
			
			for each (var user:Object in selectedUsers) 
			{
				if(user.selected)
				{
					user.selected = false;
					usersCollec.addItem(user);
					suppIndices.push(selectedUsers.getItemIndex(user));
				}
			}
			
			suppIndices.sort(Array.DESCENDING);
			
			for(var i:int=0 ; i < suppIndices.length; i++) 
			{
				selectedUsers.removeItemAt(suppIndices[i]);
			}
			
			nbToRemove = getSelectedItems(selectedUsers);
			
		}
		
		/**
		 * cocher / décocher un item
		 * @param evt
		 */
		
		public function onAllUsersItemClickHandler(evt:ListEvent):void
		{
			if(evt.currentTarget.selectedItem != null)
			{
				(evt.currentTarget.selectedItem).selected =! (evt.currentTarget.selectedItem).selected;
				usersCollec.itemUpdated(evt.currentTarget.selectedItem);
				
				nbToAdd = getSelectedItems(usersCollec)
			}
		}
		
		public function onUsersToAffectItemClickHandler(evt:ListEvent):void
		{
			if(evt.currentTarget.selectedItem != null)
			{
				(evt.currentTarget.selectedItem).selected =! (evt.currentTarget.selectedItem).selected;
				selectedUsers.itemUpdated(evt.currentTarget.selectedItem);
				
				nbToRemove = getSelectedItems(selectedUsers)
			}
		}
		
		private function getSelectedItems(items:ArrayCollection):int
		{
			var nbrSelected:int = 0;
			var long:int = items.length;
			for(var i:int = 0; i< long; i++)
			{
				if(items[i].selected)
					nbrSelected++;
			}
			return nbrSelected;
		}
		
		/**********************************************************************************************
		 *		                        GETTER / SETTER
		 *********************************************************************************************/
		public function get refUser():Gestionnaire
		{
			return _refUser;
		}
		
		/**
		 * A chaque mise à jour de l'utilisateur de Réf ,
		 * récupération de ses  caractéristiques via
		 * appels distants
		 * @param value
		 */
		public function set refUser(value:Gestionnaire):void
		{
			_refUser = value;
			initDisplay();
		}
				
		public function get selectedUsers():ArrayCollection
		{
			return _selectedUsers;
		}
		
		public function set selectedUsers(value:ArrayCollection):void
		{
			_selectedUsers = value;
		}
	
		public function get duplicateAll():Boolean
		{
			return _duplicateAll;
		}
		
		public function set duplicateAll(value:Boolean):void
		{
			_duplicateAll = value;
		}
		
		public function get nbToAdd():Number { return _nbToAdd; }
		
		public function set nbToAdd(value:Number):void
		{
			if (_nbToAdd == value)
				return;
			_nbToAdd = value;
		}
		
		
		public function get nbToRemove():Number { return _nbToRemove; }
		
		public function set nbToRemove(value:Number):void
		{
			if (_nbToRemove == value)
				return;
			_nbToRemove = value;
		}
	}
}