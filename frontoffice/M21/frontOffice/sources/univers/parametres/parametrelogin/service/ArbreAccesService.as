package univers.parametres.parametrelogin.service
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.util.UtilFunction;
	[Bindable]
	public class ArbreAccesService extends EventDispatcher
	{
		public var  dataArbre : XML;
		public var objUtilFunction : UtilFunction = new UtilFunction();
		public var arrayAccesInterdit : ArrayCollection= new ArrayCollection();
		
		
		public function ArbreAccesService()
		{
			
		}
		public function creerAcces(idBranche : int,idLogin : int):void{
			
			var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
															"fr.consotel.consoview.parametres.login.GestionClient",
																	"updateAffectationV2",
																	creerAcces_handler,creerAcces_fault_handler);
		
			RemoteObjectUtil.callService(op1,idLogin,idBranche);
		}
		private function creerAcces_handler(re:ResultEvent):void
		{
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.CREATE_ABONNEMENT_COMPLETE));
		}
		public function initArbre(idGroupeClient:int):void{
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.parametres.login.GestionClient",
								"getArbre",
								initArbre_handler,initArbre_fault_handler);
			RemoteObjectUtil.callService(opData,idGroupeClient);
		}
		private function initArbre_handler(re : ResultEvent):void
		{
			dataArbre = null;
			dataArbre = re.result as XML;
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.INIT_ARBRE_COMPLETE));
		}
		private function initArbre_fault_handler(evt : ResultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.initArbre_fault_handler'+evt.toString());
		}
		private function creerAcces_fault_handler(evt : ResultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.creerAcces_fault_handler'+evt.toString());
		}
		//Cette méthode implémente un tableau d'accès interdits pour un client et ses accès 
		/*
		Appel de la procédure « getAccesInterdit » avec « tabAcces » en paramètre.
		La procédure "getAccesInterdit" appelle, pour chaque accès de « tabAcces » passé en paramètre, la procédure « getGroupNodes ».
		« getGroupNodes » prend en paramètre un id_Groupe_maitre et un id_groupe. Cette procédure retourne  une liste XML contenant tous les nœuds entre le groupe maitre et l’organisation passés en paramètre.
		Grâce à cette procédure, on dispose d’une liste de tous les nœuds situés au dessus des accès du client sélectionné. 
		Appel de la méthode processCoherence () : traitement du résultat de la procédure "getAccesInterdit".
		*/
		public function traitementCoherence(lesAccesNoFormat : ArrayCollection):void{
			
			//var tab : Array = new Array(lesAcces.length); 
			
			var op1:AbstractOperation =	RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.login.GestionClient",
																			"getAccesInterdit",
																			processCoherence,null);
									
			//RemoteObjectUtil.callService(op1,(dgAccesParPersonne.dataProvider as ArrayCollection).source);
			RemoteObjectUtil.callService(op1,lesAccesNoFormat.source);	
		}
	//Appelle de la méthode donneListeAccesInterdit sur l'objet "objUtilFunction" qui 
	//renvoie un tableau d'accès interdit sans doublon pour un client
	/*
	Appel de la méthode « donneListeAccesInterdit » sur l’objet objUtilFunction de la classe UtilFunction.
	« donneListeAccesInterdit » prend en paramètre le résultat de la requête  "getAccesInterdit" en XML. 
	*/
	private function processCoherence(event:ResultEvent):void{
	 	objUtilFunction = new UtilFunction();
	 	objUtilFunction.addEventListener(GestionLoginEvent.TRAITEMENT_ARBRE_COMPLETE,resultTraitementCoherence);
		objUtilFunction.donneListeAccesInterdit(event.result as Array);
		//callLater(resultTraitementCoherence);
		
	}
	private function resultTraitementCoherence(evt : GestionLoginEvent):void{
		arrayAccesInterdit =  objUtilFunction.result();
		dispatchEvent(new GestionLoginEvent(GestionLoginEvent.TRAITEMENT_COHERENCE_ARBRE_COMPLETE));
	}

	}
}