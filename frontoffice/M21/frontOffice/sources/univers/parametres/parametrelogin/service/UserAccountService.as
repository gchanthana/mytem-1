package univers.parametres.parametrelogin.service
{
	import mx.rpc.AbstractOperation;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import univers.parametres.parametrelogin.vo.UserAccountStateVo;

	[Bindable]
	public class UserAccountService
	{
		public var model:UserAccountModel;
		public var handler:UserAccountHandler;
		
		public function UserAccountService()
		{
			model = new UserAccountModel();
			handler = new UserAccountHandler(model);
		}
		
		public function updateUserAccountList():void
		{
			
			var opUpdateUserAccountList:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M21.UserAccountService",
				"getUserAccountList",
				handler.updateUserAccountList);
			
			RemoteObjectUtil.callService(opUpdateUserAccountList);
		}
		
		public function updateUserAccountState(newUserAccountState:UserAccountStateVo):void
		{
			var opUpdateUserAccountState:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M21.UserAccountService",
				"updateUserAccountState",
				handler.updateUserAccountState);
			
			RemoteObjectUtil.callService(opUpdateUserAccountState,newUserAccountState.APP_LOGINID,newUserAccountState.LOGIN_STATUS);
		}
		
	}
}