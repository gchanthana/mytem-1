package univers.parametres.parametrelogin.service
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.modules.Module;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import univers.parametres.parametrecompte.vo.Gestionnaire;
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.vo.AccesCompte;
	import univers.parametres.parametrelogin.vo.ModuleVo;

	[Bindable]
	public class LoginService extends EventDispatcher
	{
		public var col_login : ArrayCollection = new ArrayCollection();
		public var col_acces : ArrayCollection = new ArrayCollection();
		public var col_adresseIP : ArrayCollection = new ArrayCollection();
		public var col_acces_not_formated : ArrayCollection = new ArrayCollection();
		public var objGroupeClient : Object = new Object();
		
		public var arrProfilRecup : ArrayCollection = new ArrayCollection();
		public var arrListeModule:ArrayCollection;
		public var arrListeModuleCompte:ArrayCollection;
		public var arrListeProfil:ArrayCollection;
		public var arrProfilRight:ArrayCollection;
		
		public var col_racine_rattachement :ArrayCollection = new ArrayCollection();
		
		private var _idProfil:int;
		
		public function LoginService() 
		{
		}
		
		public function get idProfil():int
		{
			return _idProfil;
		}
		
		public function getIdProfilUser():void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M21.GetIdProfilUser",
				"getIdProfil",
				getIdProfilUser_handler,getIdProfilUser_fault_handler);
			
			RemoteObjectUtil.callService(opData);
			
		}
		
		
		public function listeLogin(idGroupe : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.parametres.login.GestionClient",
							"getListeLogin",
							listeLogin_handler,listeLogin_fault_handler);
			
			RemoteObjectUtil.callService(opData,idGroupe);
		}
		public function getAllLogin(idGroupe : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.parametres.login.GestionClient",
							"getLogin",
							listeLogin_handler,listeLogin_fault_handler);
			
			RemoteObjectUtil.callService(opData,idGroupe);
		}
		public function createUser(valNom:String,valPrenom:String,valAdresse:String,valCodePostal:String,valVille:String,valTelephone:String,valDirection:String,valEmail:String,valPwd:String,valRestriction:int, idTypeLogin:int, idTRevendeur:int, idracinerattachement:int, valAttribut:String):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.M21.LoginService",
																		"createUtilisateur",
																		createUser_handler,createUser_fault_handler);
					RemoteObjectUtil.callService(op,valNom,valPrenom,valAdresse,valCodePostal,valVille,valTelephone,valDirection,valEmail,valPwd,valRestriction,idTypeLogin,idTRevendeur,idracinerattachement, valAttribut);
					
		}
	
		public function updateUser(idLogin: int,valNom:String,valPrenom:String,valAdresse:String,valCodePostal:String,valVille:String,valTelephone:String,valDirection:String,valEmail:String,valPwd:String,valRestriction : int,idTypeLogin : int,idTRevendeur:int, idracinerattachement:int, valAttribut:String):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.M21.LoginService",
																		"updateUtilisateur",
																		updateUser_handler,updateUser_fault_handler);
					RemoteObjectUtil.callService(op,idLogin,valNom,valPrenom,valAdresse,valCodePostal,valVille,valTelephone,valDirection,valEmail,valPwd,valRestriction,idTypeLogin,idTRevendeur,idracinerattachement, valAttribut);
					
		}
		public function listeIPOfLogin(idLogin:int):void
		{
			var opGetAdresseIP:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.login.GestionClient",
																	"getAdresseIp",
																	listeIPOfLogin_handler,listeIPOfLogin_fault_handler);
			RemoteObjectUtil.callService(opGetAdresseIP,idLogin);
		}
		public function removeAdresseIP(idLogin:int,adrIP : String, masque : String):void{
		
			var oPsupIp:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.login.GestionClient",
																	"deleteAdresseIp",
																	removeAdresseIPn_handler,removeAdresseIP_fault_handler);
																	
				
			RemoteObjectUtil.callService(oPsupIp,idLogin,adrIP,masque);
		}
		public function envoyerLoginParMail(nom:String,prenom : String,email : String, loginid:int):void{
			
			var op1:AbstractOperation =
				RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.M21.GestionMail",
					"send_Login",
					envoyerLoginParMail_handler,envoyerLoginParMail_fault_handler);
			
			RemoteObjectUtil.callService(op1, [nom, prenom, email, loginid]);
		}
		public function envoyerMDPParMail(nom:String, prenom:String, email:String, loginid:int, passe:String):void
		{
			
			var op1:AbstractOperation =
				RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.M21.GestionMail",
					"send_Pass",
					envoyerMDPParMail_handler,envoyerMDPParMail_fault_handler);
			RemoteObjectUtil.callService(op1,[nom, prenom, email, loginid, passe]);
			
		}
		public function deleteLogin(idLogin : int):void{
					
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.login.GestionClient",
																		"deleteLogin",
																		deleteLogin_handler,deleteLogin_fault_handler);
			RemoteObjectUtil.callService(op,[idLogin]);
			
		}
		public function getAccesOfLogin(idLogin : int, idGroupe : int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.login.GestionClient",
																		"getAccess_v2",
																		getAccesOfLogin_handler,getAccesOfLogin_fault_handler);
			RemoteObjectUtil.callService(op1,idLogin,idGroupe);	
		}
		public function updateParametreAbonnement(module:String,value : int,idLogin : int,idAcces:int):void
		{
						var op1:AbstractOperation =
							RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.login.GestionClient",
																	"changeDroit",
																	updateParametreAbonnement_handler,updateParametreAbonnement_fault_handler);
																
			RemoteObjectUtil.callService(op1,[module,value,idLogin,idAcces]);
		}
		
		public function deleteAcces(idLogin : int, idAcces : int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.login.GestionClient",
																	"deleteAcces",
																	deleteAcces_handler,deleteAcces_fault_handler);
			RemoteObjectUtil.callService(op1,[idLogin,idAcces]);
		}
		public function getRacineInfos(idGroupe : int):void {
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.login.GestionClient",
																		"getGroupesMaitres",
																		getRacineInfos_handler,getRacineInfos_fault_handler);
			RemoteObjectUtil.callService(opData,idGroupe);
		}
		
		public function getAttachementRacineInfos():void {
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M21.LoginService",
				"getRacineRattachement",
				getAttachementRacineInfos_handler,getAttachementRacineInfos_fault_handler);
			RemoteObjectUtil.callService(opData);
		}
		
		/**
		 * liste des racines partenaaire 
		 * 
		 */		
		public function getRacineRattachement():void {
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M21.GetRacineRattachement",
				"getRacineRattachement",
				getRacineRattachement_handler,getRacineRattachement_fault_handler);
			
			RemoteObjectUtil.callService(opData);
		}
		

/* ----------------------------------------------------------------------------------------------------------------------------------
								AFFINAGE DES DROITS DES LOGINS SUR LES MODULES
---------------------------------------------------------------------------------------------------------------------------------  */		
		public function ListeModule(app_loginid:int, id_racine:int,code_app:int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperation(
																	"fr.consotel.consoview.M21.gestionDroitService",
																	"ListeModule3",
																	ListeModuleHandler);
			RemoteObjectUtil.invokeService(op1,app_loginid,id_racine,code_app);
		}
		
		public function ListeModuleCompte(app_loginid:int, id_racine:int,code_app:int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperation(
				"fr.consotel.consoview.M21.gestionDroitService",
				"ListeModule3",
				ListeModuleCompteHandler);
			RemoteObjectUtil.invokeService(op1,app_loginid,id_racine,code_app);
		}
		
		
		public function SauvegarderDroit(app_loginid:int, id_racine:int, liste_Module_Ecriture:String, liste_Module_Lecture:String ):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperation(
																	"fr.consotel.consoview.M21.gestionDroitService",
																	"SauvegarderDroit",
																	SauvegarderDroitHandler);
			RemoteObjectUtil.invokeService(op1,app_loginid,id_racine,liste_Module_Ecriture,liste_Module_Lecture);
		}
		public function listeProfil(id_racine:int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperation(
																	"fr.consotel.consoview.M21.gestionDroitService",
																	"listeProfil",
																	ListeProfilHandler);
			RemoteObjectUtil.invokeService(op1,id_racine);
		}
		public function SupprimerProfil(id_profil:int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperation(
																	"fr.consotel.consoview.M21.gestionDroitService",
																	"SupprimerProfil",
																	SupprimerProfilHandler);
			RemoteObjectUtil.invokeService(op1,id_profil);
		}
		public function EnregistrerProfil(libelle_profil:String, id_racine:int, liste_Module_Lecture:String, liste_Module_ecriture:String, description_profil:String):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperation(
																	"fr.consotel.consoview.M21.gestionDroitService",
																	"EnregistrerProfil",
																	EnregistrerProfilHandler);
			RemoteObjectUtil.invokeService(op1,libelle_profil,id_racine,liste_Module_ecriture,liste_Module_Lecture, description_profil);
		}
		public function RecupProfil(id_profil:int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperation(
																	"fr.consotel.consoview.M21.gestionDroitService",
																	"RecupProfil",
																	RecupProfilHandler);
			RemoteObjectUtil.invokeService(op1,id_profil);
		}
		
		/**
		 *  init l'id profile de l'utilisateur
		 * */
		private function getIdProfilUser_handler(e:ResultEvent):void
		{
			if(e.result != null)
			{
				this._idProfil=int(e.result);
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.ID_PROFIL_USER));
			}	
		}
		
		private function getRacineRattachement_handler(evt:ResultEvent):void
		{
			col_racine_rattachement.removeAll();
			var cursor:IViewCursor = (evt.result as ArrayCollection).createCursor();
			
			while(!cursor.afterLast)
			{
				if(cursor.current.LIBELLE ==null)
				{
					cursor.current.LIBELLE  = 'non renseigné';
				}
				col_racine_rattachement.addItem(cursor.current);	
				cursor.moveNext();
			}			
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.LISTE_RACINE_RATTACHEMENT));
		}
		private function ListeModuleHandler(e:ResultEvent):void
		{
			arrListeModule = new ArrayCollection()
			var moduleVo:ModuleVo;	
			if(e.result != null && e.result.length > 0)
			{
				for each(var obj:Object in e.result as ArrayCollection)
				{
					if(obj.ACCESS_RACINE > 0)
					{
						moduleVo = new ModuleVo();
						moduleVo.cast(obj);
						arrListeModule.addItem(moduleVo);						
					}
						
				}
			}
			arrListeModule.refresh();
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.LISTE_MODULE_COMPLETE));
		}
		
		private function ListeModuleCompteHandler(e:ResultEvent):void
		{
			arrListeModuleCompte = new ArrayCollection()
			if(e.result != null && e.result.length > 0)
			{
				for each(var obj:Object in e.result as ArrayCollection)
				{
					if(obj.ACCESS_RACINE == 2)
						arrListeModuleCompte.addItem(obj);
				}
			}
			arrListeModuleCompte.refresh();
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.LISTE_MODULE_COMPLETE));
		}
		
		private function SauvegarderDroitHandler(e:ResultEvent):void
		{
			if(e.result == -1)
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SAUVEGARDER_DROIT_ERROR))
			else
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SAUVEGARDER_DROIT_COMPLETE))
		}
		private function ListeProfilHandler(e:ResultEvent):void
		{
			if(e.result != null && e.result.length > 0)
				arrListeProfil = e.result as ArrayCollection
			else
				arrListeProfil = new ArrayCollection()
		}
		private function SupprimerProfilHandler(e:ResultEvent):void
		{
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SUPPRIMER_PROFIL_COMPLETE));
		}
		private function EnregistrerProfilHandler(e:ResultEvent):void
		{
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.ENREGISTRER_PROFIL_COMPLETE));
		}
		private function RecupProfilHandler(e:ResultEvent):void
		{
			if(e.result != null)
			{
				arrProfilRecup = e.result as ArrayCollection
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.RECUP_PROFIL_COMPLETE));
			}
		}
		
		public function getRacineInfos_handler(evt : ResultEvent):void
		{
			var tempCol : ArrayCollection = evt.result as ArrayCollection ;
			if(tempCol.length > 0)
			{
				objGroupeClient = tempCol.getItemAt(0);	
			}
			
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.INFO_ACCES_COMPLETE));
		}
		public function deleteAcces_handler(re : ResultEvent):void
		{
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.DELETE_ACCES_COMPLETE));
		}
		public function updateParametreAbonnement_handler(re : ResultEvent):void
		{
			
		}
		public function getAccesOfLogin_handler(re : ResultEvent):void
		{
			// pour l'arbre
			col_acces_not_formated = new ArrayCollection();
			col_acces = new ArrayCollection();
			col_acces_not_formated = re.result as ArrayCollection;
			
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
		
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : AccesCompte = new AccesCompte();
								
				item.libelle=tmpArr[i].LIBELLE ;
			
				item.idLogin=tmpArr[i].APP_LOGINID;
				item.idAcces=tmpArr[i].ID;
				item.idGroupeMaitre =tmpArr[i].idGroupeMaitre;
				item.module_usage=tmpArr[i].MODULE_USAGE;
				item.module_fixe_data =tmpArr[i].MODULE_FIXE_DATA ;
				item.module_gestion_login=tmpArr[i].MODULE_GESTION_LOGIN;
				item.module_mobile=tmpArr[i].MODULE_MOBILE;
				item.module_fournisseur=tmpArr[i].DROIT_GESTION_FOURNIS ;
				item.module_facturation=tmpArr[i].MODULE_FACTURATION;
				item.module_gestion_orga=tmpArr[i].MODULE_GESTION_ORG;
				item.module_workflow=tmpArr[i].MODULE_WORKFLOW ;
				item.DATE_CREATE = tmpArr[i].DATE_CREATE;	
				item.libellegm = tmpArr[i].libellegm;
				item.TYPE = (item.idGroupeMaitre == item.idAcces)?ResourceManager.getInstance().getString('M21', 'Racine'):ResourceManager.getInstance().getString('M21', 'Groupe_de_lignes');
				col_acces.addItem(item);
			}
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.LISTE_ACCES_COMPLETE));
		}
		private function deleteLogin_handler(re : ResultEvent):void
		{
			//if(re.result){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.DELETE_LOGIN));
			//}
		}

		private function envoyerLoginParMail_handler(re: ResultEvent):void
		{
			//if(re.result>1){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SEND_MAIL_COMPLETE));
			//}
		}
		private function envoyerMDPParMail_handler(re: ResultEvent):void
		{
			//if(re.result>1){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SEND_PASS_COMPLETE));
			//}
		}
		private function removeAdresseIPn_handler (re : ResultEvent):void
		{
			if(re.result){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.DELETE_IP_COMPLETE));
			}
		}
		private function listeIPOfLogin_handler(re : ResultEvent):void
		{
			if(re.result)
			{
				col_adresseIP = re.result as ArrayCollection;
			}
		}
		private function createUser_handler(evt :ResultEvent):void
		{
			if(evt.result>1){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.LOGIN_INFO_CHANGE_COMPLETE,false,false,evt.result as int));
			}
			else if(evt.result == 0){
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Cet_compte_existe_d_j__'),ResourceManager.getInstance().getString('M21', 'Information'), null);
			}
				
		}
		private function updateUser_handler(evt :ResultEvent):void
		{
			if(evt.result == 1){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.LOGIN_INFO_CHANGE_COMPLETE,false,false,evt.result as int));
			}
			else if(evt.result == 0){
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Cet_compte_existe_d_j__'),ResourceManager.getInstance().getString('M21', 'Information'), null);
			}
		}
	
		private function listeLogin_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_login = new ArrayCollection();
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : Gestionnaire = new Gestionnaire();
								
				item.nom=tmpArr[i].LOGIN_NOM;
				item.prenom=tmpArr[i].LOGIN_PRENOM;
				item.adresse=tmpArr[i].ADRESSE_POSTAL;
				item.cp=tmpArr[i].CODE_POSTAL;
				item.ville=tmpArr[i].VILLE_POSTAL;
				item.tel=tmpArr[i].TELEPHONE;
				item.direction=tmpArr[i].DIRECTION;
				item.mail=tmpArr[i].LOGIN_EMAIL;
				item.password=tmpArr[i].LOGIN_PWD;
				item.id	= tmpArr[i].APP_LOGINID;
				item.idTypeLogin = tmpArr[i].TYPE_PROFIL;
				item.bool_acces_perimetre = tmpArr[i].BOOL_ACCES_PERIMETRE ;
				item.nb_failed_connection = tmpArr[i].FAILED_CONNECT;
				item.idstatut =tmpArr[i].LOGIN_STATUS;
				item.idRacine=tmpArr[i].IDGROUPE_CLIENT;
				item.idRacineRat=tmpArr[i].IDGROUPE_CLIENT;
				item.idRevendeur=tmpArr[i].IDREVENDEUR;
				item.attribut = (tmpArr[i].ATTRIBUT_SSO)?tmpArr[i].ATTRIBUT_SSO:'';
				
				if(tmpArr[i].RESTRICTION_IP==1)
				{
					item.restrictIP=true;
				}
				else
				{
					item.restrictIP=false;
				}
						
				col_login.addItem(item);
			}
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.LISTE_LOGIN_COMPLETE));
		}
		
		/* GESTION DU RETOUR DE PROCEDURE POUR AFFICHAGE DES LIBELLES DU TYPE DE LA RACINE */
		public var attachementRacineInfos:ArrayCollection = new ArrayCollection;
		private function getAttachementRacineInfos_handler(re:ResultEvent):void
		{
			if(re.result){
				attachementRacineInfos = new ArrayCollection;
				attachementRacineInfos = re.result as ArrayCollection;
				
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.ATTCHEMENTRACINEINFOS));
			}
			else{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Pas_de_racine_disponible'), 'Consoview', null);
			}
		}
		
		
		/* GESTION DES ERREURS DATA(AUCUN RETOUR) */
		private function getRacineInfos_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateParametreAbonnement_fault_handler'+evt.toString());
		}
		private function updateParametreAbonnement_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateParametreAbonnement_fault_handler'+evt.toString());
		}
		private function getAccesOfLogin_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getAccesOfLogin_fault_handler'+evt.toString());
		}
		private function envoyerLoginParMail_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.envoyerLoginParMail_fault_handler'+evt.toString());
		}
		private function envoyerMDPParMail_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.envoyerMDPParMail_fault_handler'+evt.toString());
		}
		private function removeAdresseIP_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeLogin_fault_handler'+evt.toString());
		}
		private function listeIPOfLogin_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeLogin_fault_handler'+evt.toString());
		}
		private function listeLogin_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeLogin_fault_handler'+evt.toString());
		}
		private function createUser_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.createUser_fault_handler'+evt.toString());
		}
		private function deleteAcces_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.deleteAcces_fault_handler'+evt.toString());
		}
		private function updateUser_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.createUser_fault_handler'+evt.toString());
		}
		private function deleteLogin_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.deleteLogin_fault_handler'+evt.toString());
		}
		private function getAttachementRacineInfos_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getAttachementRacineInfos_fault_handler'+evt.toString());
		}
		
		private function getIdProfilUser_fault_handler(evt:FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getIdProfilUser_fault_handler'+evt.toString());
		}
		
		private function getRacineRattachement_fault_handler(evt:FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getRacineRattachement_fault_handler'+evt.toString());
		}
		
	}
}