package univers.parametres.parametrelogin.service
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import univers.parametres.parametrelogin.event.AccessEvent;
	import univers.parametres.parametrelogin.vo.AccessVO;

	[Bindable]
	public class AccessModel extends EventDispatcher
	{
		
		public var module:String;
		
		public var _access:ArrayCollection  = new ArrayCollection();
		public function get access():ArrayCollection
		{
			return _access;
		}
		
		public var _accessTotal:ArrayCollection = new ArrayCollection();;
		public function get accessTotal():ArrayCollection
		{
			return _accessTotal;
		}
		
		
		private var  _error_message:String = "";
		
		public function get error_message():String
		{
			return _error_message;
		}
		
		public function AccessModel()
		{
		}
		
		internal function resetAccess():void
		{
			access.removeAll();		
		}
		
		internal function resetAccessTotal():void
		{
			accessTotal.removeAll();
		}
		
		
		public function updateAccess(values:ArrayCollection):void
		{	
			var _accessVO:AccessVO;
			for(var i:int=0;i<values.length;i++){
				_accessVO = new AccessVO();
				_accessVO.addAccess(values[i]);
				if(module==''){
					accessTotal.addItem(_accessVO);
				}
				else{
					access.addItem(_accessVO);
				}
			}
			
			dispatchEvent(new AccessEvent(AccessEvent.ACCESS_COMPLETE));
			
		}
		
		
		public function activeParamHandler(values:ArrayCollection):void
		{
			var _accesObj:AccessVO;
			var _err_count:Number = 0; 
			
			if(module != '')
			{
				resetAccess();	
			}
			else
			{
				resetAccessTotal();
			}
			
			_error_message = "";	
			
			
			for(var i:int=0;i<values.length;i++){
				_accesObj = new AccessVO();
				_accesObj.cast(values[i]);
				
				if(!_accesObj.cast(values[i])) 
				{
					throw new Error("Erreur de cast : type attendu AccessVO")
					break;
				}
				
				if(module=='')
				{					
					accessTotal.addItem(_accesObj);
				}
				else
				{
					
					access.addItem(_accesObj);
				}
				
				if(_accesObj.isUpdated != 1)
				{					
					_error_message = _error_message + ' - ' + _accesObj.libelle + '\n';
					_err_count ++;
				}
			}
			
			if (_err_count == 0)
			{
				dispatchEvent(new AccessEvent(AccessEvent.ACTIVEPARAM_COMPLETE));
			}
			else
			{
				dispatchEvent(new AccessEvent(AccessEvent.ACTIVEPARAM_ERROR));		
			}
			
		}
	}
}