package univers.parametres.parametrelogin.service
{
	import mx.resources.ResourceManager;
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.ResultEvent;

	public class UserAccountHandler
	{
		private var _model:UserAccountModel;
		
		public function UserAccountHandler(model:UserAccountModel)
		{
			_model = model;
		}
				
		internal function updateUserAccountList(evt:ResultEvent):void
		{	
			_model.updateUserAccountList(evt.result as ArrayCollection);
		}
		
		internal function updateUserAccountState(evt:ResultEvent):void
		{
			if(evt.result > -1)
			{
				_model.updateUserAccountState(evt.token.message.body);
			}	
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'hum____c_est_embarassant'));					
			}
		}
	}
}