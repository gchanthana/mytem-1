package univers.parametres.parametrelogin.service.user
{
    import flash.events.EventDispatcher;
    
    import mx.rpc.events.ResultEvent;
    
    import univers.parametres.parametrelogin.event.UserEvent;

    public class UserHandlers extends EventDispatcher
    {
        private var myDatas:UserDatas;

        public function UserHandlers(instanceOfDatas:UserDatas)
        {
            myDatas = instanceOfDatas;
        }

        internal function duplicateUserProfileResultHandler(re:ResultEvent):void
        {
            if (re.result)
            {
                var o:Object = re.result;
                dispatchEvent(new UserEvent(UserEvent.DUPLICATE_PROFILE_SUCCESS));
            }
            else
            {
                dispatchEvent(new UserEvent(UserEvent.DUPLICATE_PROFILE_ERROR));
            }
        }
    }
}