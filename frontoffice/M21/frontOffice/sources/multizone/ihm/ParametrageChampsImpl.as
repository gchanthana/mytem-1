package multizone.ihm
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import multizone.entity.Zone;
	import multizone.ihm.tabnavigator.Champs1IHM;
	import multizone.ihm.tabnavigator.Champs2IHM;
	import multizone.utils.Champs;
	import multizone.utils.Formator;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.TabNavigator;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class ParametrageChampsImpl extends Box
	{
		
//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					IHM 
		//--------------------------------------------------------------------------------------------//

		public var tabNavigator 	:TabNavigator;
		
		public var champs1			:Champs1IHM;
		public var champs2			:Champs2IHM;
		
		//--------------------------------------------------------------------------------------------//
		//					GLOBALE IHM 
		//--------------------------------------------------------------------------------------------//
		
		private var _champsXML		:Champs;
		
		private var _zoneChamps1	:Object;
		private var _zoneChamps2	:Object;
		
		private var _listeChamps	:XML;
		
		private var _champs1XML		:XMLList;
		private var _champs2XML		:XMLList;
		
		private var attributAll		:int = -1;
		
		private var _idchamps		:int = 0;
		
//--------------------------------------------------------------------------------------------//
//					CONSTRUCTOR
//--------------------------------------------------------------------------------------------//	

		public function ParametrageChampsImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PROTECTED
//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					LISTENER IHM 
		//--------------------------------------------------------------------------------------------//

		protected function btnValiderClickHandler(e:Event):void
		{
			_champsXML = new Champs();
			
			var boolChamps1:Boolean = checkChamps1();
			var boolChamps2:Boolean = checkChamps2();
			
			if(boolChamps1 && boolChamps2)
			{
				createZone();
				createXML(champs1.listeZone, _zoneChamps1, true);
				createXML(champs2.listeZone, _zoneChamps2, false);
				sendXML();
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M21', 'Veuillez_saisir_les_champs_obligatoire__'), ResourceManager.getInstance().getString('M21', 'Consoview'), null);
		}

		protected function btnAnnulerClickHandler(e:Event):void
		{
		}
		
//--------------------------------------------------------------------------------------------//
//					METHODES PRIVATE
//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					LISTENER IHM 
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fl:FlexEvent):void
		{
			addEventListener("REFRESH_DATAGRID1", refreshHandler);
			addEventListener("REFRESH_DATAGRID2", refreshHandler);
			
			_champsXML = new Champs();
			
			addXML(-1);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER IHM 
		//--------------------------------------------------------------------------------------------//

		private function refreshHandler(e:Event):void
		{
			if(e.type == "REFRESH_DATAGRID1")
				attributAll = 0;
			else
				attributAll = 1;

			_champsXML = new Champs();
			
			addXML(attributAll);
		}

		//--------------------------------------------------------------------------------------------//
		//					METHODES 
		//--------------------------------------------------------------------------------------------//
		
		private function createZone():void
		{
			_zoneChamps1 = new Object();
			_zoneChamps2 = new Object();
			
			_zoneChamps1.LIBELLE	= champs1.txtbxLibelle.text;
			_zoneChamps1.EXEMPLE	= champs1.txtbxExemple.text;
			_zoneChamps1.ACTIF		= champs1.chbxActive.selected;
			_zoneChamps1.AUTO		= champs1.chbxAuto.selected;

			
			_zoneChamps2.LIBELLE	= champs2.txtbxLibelle.text;
			_zoneChamps2.EXEMPLE	= champs2.txtbxExemple.text;
			_zoneChamps2.ACTIF		= champs2.chbxActive.selected;
			_zoneChamps2.AUTO		= champs2.chbxAuto.selected;
		}
		
		private function readXML(value:int):void
		{
			if(value < 0)
			{
				champs1.txtbxLibelle.text 	= _listeChamps.champ1_libelle;
				champs1.txtbxExemple.text 	= _listeChamps.champ1_exemple;
				champs1.chbxActive.selected	= Formator.convertIntToBool(_listeChamps.champ1_actif);
				champs1.chbxAuto.selected	= Formator.convertIntToBool(_listeChamps.champ1_auto);
				
				champs2.txtbxLibelle.text 	= _listeChamps.champ2_libelle;
				champs2.txtbxExemple.text 	= _listeChamps.champ2_exemple;
				champs2.chbxActive.selected	= Formator.convertIntToBool(_listeChamps.champ2_actif);
				champs2.chbxAuto.selected	= Formator.convertIntToBool(_listeChamps.champ2_auto);
			}
			
			attributXMLToDatagrid(value);
		}
		
		private function attributXMLToDatagrid(value:int):void
		{
			switch(value)
			{
				case -1:	_champs1XML = (_listeChamps.champs1[0] as XML).children();
							_champs2XML = (_listeChamps.champs2[0] as XML).children();
							attributDataChamps1();
							attributDataChamps2();
							break;
				
				case 0:		_champs1XML = (_listeChamps.champs1[0] as XML).children();
							attributDataChamps1();
							break;
				
				case 1:		_champs2XML = (_listeChamps.champs2[0] as XML).children();
							attributDataChamps2();
							break;
			}
		}
		
		private function attributDataChamps1():void
		{
			var zone:Zone;		
			champs1.listeZone = new ArrayCollection();

			for(var i:int = 0;i < _champs1XML.length();i++)
			{
				var type_zone:String = "";
				
				if(_champs1XML[i].caracteres == Champs.SHADOW_CHAR)
					type_zone = "";
				else
					type_zone = _champs1XML[i].caracteres;
				
				zone = new Zone();		
				zone.COMPTEUR_ZONE					= _champs1XML[i].zone;
				zone.STEPPEUR_ZONE					= _champs1XML[i].number_caracteres;
				zone.EXACT_NBR						= Formator.convertIntToBool(_champs1XML[i].exact_number);
				zone.COMPTEUR_OBLIG					= _champs1XML[i].number_caracteres_oblig;
				zone.TYPE_ZONE						= type_zone;
				zone.IDTYPE_ZONE					= 1;
				zone.CONTENU_ZONE.ID_CARACTERE		= _champs1XML[i].idtype_caracteres;
				zone.CONTENU_ZONE.LIBELLE_CARATERE	= _champs1XML[i].type_caracteres;
				zone.SAISIE_ZONE					= Formator.convertIntToBool(_champs1XML[i].obligatoire);
				champs1.listeZone.addItem(zone);
			}
		}
		
		private function attributDataChamps2():void
		{
			var zone:Zone;		
			champs2.listeZone = new ArrayCollection();
			
			for(var i:int = 0;i < _champs2XML.length();i++)
			{
				var type_zone:String = "";
				
				if(_champs2XML[i].caracteres == Champs.SHADOW_CHAR)
					type_zone = "";
				else
					type_zone = _champs2XML[i].caracteres;
				
				zone = new Zone();		
				zone.COMPTEUR_ZONE					= _champs2XML[i].zone;
				zone.STEPPEUR_ZONE					= _champs2XML[i].number_caracteres;
				zone.EXACT_NBR						= Formator.convertIntToBool(_champs2XML[i].exact_number);
				zone.COMPTEUR_OBLIG					= _champs2XML[i].number_caracteres_oblig;
				zone.TYPE_ZONE						= type_zone;
				zone.IDTYPE_ZONE					= 1;
				zone.CONTENU_ZONE.ID_CARACTERE		= _champs2XML[i].idtype_caracteres;
				zone.CONTENU_ZONE.LIBELLE_CARATERE	= _champs2XML[i].type_caracteres;
				zone.SAISIE_ZONE					= Formator.convertIntToBool(_champs2XML[i].obligatoire);
				champs2.listeZone.addItem(zone);
			}
		}
		
		private function checkChamps1():Boolean
		{
			var OK:Boolean = false;
			
			if(champs1.txtbxLibelle.text == "" )
			{
				OK = false;
				
				if(champs1.txtbxLibelle.text == "")
					champs1.txtbxLibelle.errorString = ResourceManager.getInstance().getString('M21', 'Veuillez_saisir_un_libell___');
				else
					champs1.txtbxLibelle.errorString = "";
			}
			else
			{
				OK = true;
				
				champs1.txtbxLibelle.errorString = "";
				champs1.txtbxExemple.errorString = "";
			}
			
			return OK;
		}
		
		private function checkChamps2():Boolean
		{
			var OK:Boolean = false;
			
			if(champs2.chbxActive.selected)
			{
				if(champs2.txtbxLibelle.text == "" )
				{
					OK = false;
					
					if(champs2.txtbxLibelle.text == "")
						champs2.txtbxLibelle.errorString = ResourceManager.getInstance().getString('M21', 'Veuillez_saisir_un_libell___');
					else
						champs2.txtbxLibelle.errorString = "";
				}
				else
				{
					OK = true;
					
					champs2.txtbxLibelle.errorString = "";
					champs2.txtbxExemple.errorString = "";
				}
			}
			else
			{
				OK = true;
				
				champs2.txtbxLibelle.errorString = "";
				champs2.txtbxExemple.errorString = "";
			}
			
			return OK;
		}
		
		private function createXML(value:ArrayCollection, zone:Object, isChamp1:Boolean):void
		{
			var i:int  = 0;

			for(i = 0;i < value.length;i++)
			{
				if(isChamp1)
					_champsXML.addChamps1(value[i]);
				else
					_champsXML.addChamps2(value[i]);
			}
			
			_champsXML.addZone(zone, isChamp1);
		}

		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURE 
		//--------------------------------------------------------------------------------------------//		
		
		private function addXML(value:int):void
	    {
	    	attributAll = value;
			
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M21.Champs",
	    																		"getChamps",
	    																		addXMLResultHandler);
			RemoteObjectUtil.callService(op, CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
	    }
		
		private function sendXML():void
	    {
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M21.Champs",
	    																		"enregistrerChamps",
	    																		sendXMLResultHandler);
			RemoteObjectUtil.callService(op, CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,
											 _champsXML.champs);
	    }
	    
	    private function updateXML():void
	    {
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M21.Champs",
	    																		"updateChamps",
	    																		updateXMLResultHandler);
			RemoteObjectUtil.callService(op, CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,
											 _idchamps,
											 _champsXML.champs);
	    }

		//--------------------------------------------------------------------------------------------//
		//					RETOUR PROCEDURE 
		//--------------------------------------------------------------------------------------------//	
	    
	    private function addXMLResultHandler(re:ResultEvent):void
	    {
	    	if(re.result && (re.result as ArrayCollection).length > 0 )
	    	{
	    		_listeChamps = new XML(re.result[0].XML);
				
				readXML(attributAll);
	    	}
	    	else
	    		ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Param_trage_du_champ_de_r_f_rence_client'), ResourceManager.getInstance().getString('M21', 'Consoview'), null);
	    }
	    
	    private function sendXMLResultHandler(re:ResultEvent):void
	    {
	    	if(re.result > 0)
	    		ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Param_trage_du_champ_de_r_f_rence_client'), this);
	    	else
	    		ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Param_trage_du_champ_de_r_f_rence_client'), ResourceManager.getInstance().getString('M21', 'Consoview'), null);
	    }
	    
	    private function updateXMLResultHandler(re:ResultEvent):void
	    {
	    	if(re.result > 0)
	    		ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Param_trage_du_champ_de_r_f_rence_client'), this);
	    	else
	    		ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Mise___jour_du_param_trage_du_champ_de_r'), ResourceManager.getInstance().getString('M21', 'Consoview'), null);
	    }
		

	}
}