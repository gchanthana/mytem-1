package multizone.utils
{
	import multizone.entity.Zone;
	
	public class Champs
	{
		
		public static var SHADOW_CHAR:String = "/*|*/";
		
		public var CHAMPSXML :XML = <champs>
									  <champs1>
									    <champ1>
									      <zone>1</zone>
									      <number_caracteres>5</number_caracteres>
									      <type_caracteres>Libre</type_caracteres>
									      <caracteres/>
									      <idtype_caracteres>0</idtype_caracteres>
									      <obligatoire>0</obligatoire>
									    </champ1>
									    <champ1>
									      <zone>2</zone>
									      <number_caracteres>5</number_caracteres>
									      <type_caracteres>Numérique</type_caracteres>
									      <caracteres>-</caracteres>
									      <idtype_caracteres>1</idtype_caracteres>
									      <obligatoire>1</obligatoire>
									    </champ1>
									  </champs1>
									  <champs2>
									    <champ2>
									      <zone>1</zone>
									      <number_caracteres>3</number_caracteres>
									      <type_caracteres>Libre</type_caracteres>
									      <caracteres>_</caracteres>
									      <idtype_caracteres>0</idtype_caracteres>
									      <obligatoire>1</obligatoire>
									    </champ2>
									    <champ2>
									      <zone>2</zone>
									      <number_caracteres>4</number_caracteres>
									      <type_caracteres>Numérique</type_caracteres>
									      <caracteres>_</caracteres>
									      <idtype_caracteres>1</idtype_caracteres>
									      <obligatoire>0</obligatoire>
									    </champ2>
									    <champ2>
									      <zone>3</zone>
									      <number_caracteres>5</number_caracteres>
									      <type_caracteres>Alphanumérique</type_caracteres>
									      <caracteres>_</caracteres>
									      <idtype_caracteres>2</idtype_caracteres>
									      <obligatoire>0</obligatoire>
									    </champ2>
									  </champs2>
									  <champ1_libelle>Libelle1</champ1_libelle>
									  <champ1_exemple>xx-xx</champ1_exemple>
									  <champ1_actif>1</champ1_actif>
 									  <champ1_auto>0</champ1_auto>
									  <champ2_libelle>Libelle2</champ2_libelle>
									  <champ2_exemple>xxx-xxxx-xxxxx</champ2_exemple>
									  <champ2_actif>1</champ2_actif>
 									  <champ2_auto>0</champ2_auto>
									</champs>;
		
		public var champs 	: XML =	<champs>
										<champs1></champs1>
										<champs2></champs2>
									</champs>;
		
		public var champs1	:XML 	= champs.champs1[0];
		public var champs2	:XML 	= champs.champs2[0];


		public function addChamps1(value:Zone):void
		{
			var champ1		:XML 	= <champ1></champ1>;
			var type_zone	:String = "";
			
			if(value.TYPE_ZONE == "")
				type_zone = SHADOW_CHAR;
			else
				type_zone = value.TYPE_ZONE;
			
			champ1.appendChild(<zone>{value.COMPTEUR_ZONE}</zone>);
			champ1.appendChild(<number_caracteres>{value.STEPPEUR_ZONE}</number_caracteres>);
			champ1.appendChild(<number_caracteres_oblig>{value.COMPTEUR_OBLIG}</number_caracteres_oblig>);
			champ1.appendChild(<exact_number>{Formator.convertBoolToInt(value.EXACT_NBR)}</exact_number>);
			champ1.appendChild(<type_caracteres>{value.CONTENU_ZONE.LIBELLE_CARATERE}</type_caracteres>);
			champ1.appendChild(<caracteres>{type_zone}</caracteres>);
			champ1.appendChild(<idtype_caracteres>{value.CONTENU_ZONE.ID_CARACTERE}</idtype_caracteres>);	//0 -> Libre, 1 -> Numérique, 2 -> Alpha
			champ1.appendChild(<obligatoire>{Formator.convertBoolToInt(value.SAISIE_ZONE)}</obligatoire>);	//0 -> Non , 1 -> Oui
			champs1.appendChild(champ1);
		}
		
		public function addChamps2(value:Zone):void
		{
			var champ2		:XML 	= <champ2></champ2>;
			var type_zone	:String = "";
			
			if(value.TYPE_ZONE == "")
				type_zone = SHADOW_CHAR;
			else
				type_zone = value.TYPE_ZONE;
			
			champ2.appendChild(<zone>{value.COMPTEUR_ZONE}</zone>);
			champ2.appendChild(<number_caracteres>{value.STEPPEUR_ZONE}</number_caracteres>);
			champ2.appendChild(<number_caracteres_oblig>{value.COMPTEUR_OBLIG}</number_caracteres_oblig>);
			champ2.appendChild(<exact_number>{Formator.convertBoolToInt(value.EXACT_NBR)}</exact_number>);
			champ2.appendChild(<type_caracteres>{value.CONTENU_ZONE.LIBELLE_CARATERE}</type_caracteres>);
			champ2.appendChild(<caracteres>{type_zone}</caracteres>);
			champ2.appendChild(<idtype_caracteres>{value.CONTENU_ZONE.ID_CARACTERE}</idtype_caracteres>);	//0 -> Libre, 1 -> Numérique, 2 -> Alpha
			champ2.appendChild(<obligatoire>{Formator.convertBoolToInt(value.SAISIE_ZONE)}</obligatoire>);	//0 -> Non , 1 -> Oui
			champs2.appendChild(champ2);
		}
		
		public function addZone(value:Object, isChamp1:Boolean):void
		{					
			if(isChamp1)
			{
				champs.appendChild(<champ1_libelle>{value.LIBELLE}</champ1_libelle>);
				champs.appendChild(<champ1_exemple>{value.EXEMPLE}</champ1_exemple>);
				champs.appendChild(<champ1_actif>{Formator.convertBoolToInt(value.ACTIF)}</champ1_actif>);
				champs.appendChild(<champ1_auto>{Formator.convertBoolToInt(value.AUTO)}</champ1_auto>);
			}
			else
			{
				champs.appendChild(<champ2_libelle>{value.LIBELLE}</champ2_libelle>);
				champs.appendChild(<champ2_exemple>{value.EXEMPLE}</champ2_exemple>);
				champs.appendChild(<champ2_actif>{Formator.convertBoolToInt(value.ACTIF)}</champ2_actif>);
				champs.appendChild(<champ2_auto>{Formator.convertBoolToInt(value.AUTO)}</champ2_auto>);
			}
		}
		
		public function Champs()
		{
		}

	}
}