package multizone.composants
{
	import mx.resources.ResourceManager;
	import flash.events.Event;
	
	import multizone.entity.ContenuZone;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	
	public class ComboboxCaracteresImpl extends Box
	{
		
//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					IHM 
		//--------------------------------------------------------------------------------------------//
		
		public var cbxCaractere		:ComboBox;
		
		//--------------------------------------------------------------------------------------------//
		//					GLOBALE IHM 
		//--------------------------------------------------------------------------------------------//
		
		[Bindable]
		public var dataGeneric		:ArrayCollection;
		
//--------------------------------------------------------------------------------------------//
//					CONSTRUCTOR
//--------------------------------------------------------------------------------------------//	
		
		public function ComboboxCaracteresImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PUBLIC
//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					OVERRIDE - SETDATA
		//--------------------------------------------------------------------------------------------//

		override public function set data(value:Object):void
		{
			if(value != null)
			{
				super.data = value;
				createGenericObject();
				cbxCaractere.selectedIndex = data.CONTENU_ZONE.ID_CARACTERE;
			}
		}
		
//--------------------------------------------------------------------------------------------//
//					METHODES PROTECTED
//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					LISTENER IHM 
		//--------------------------------------------------------------------------------------------//
		
		protected function cbxCaractereChangeHandler(e:Event):void
		{
			if(cbxCaractere.selectedItem != null)
			{
				data.CONTENU_ZONE = cbxCaractere.selectedItem;
			}
			else
			{
				cbxCaractere.selectedIndex = 0;
				data.CONTENU_ZONE = cbxCaractere.selectedItem;
			}
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PRIVATE
//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					LISTENER IHM 
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fl:FlexEvent):void
		{
			createGenericObject();
		}

		//--------------------------------------------------------------------------------------------//
		//					METHODES 
		//--------------------------------------------------------------------------------------------//
				
		private function createGenericObject():void
		{
			dataGeneric	= new ArrayCollection();
			var objectGeneric	:ContenuZone;
			
			objectGeneric = new ContenuZone();
			objectGeneric.ID_CARACTERE 		= 0;
			objectGeneric.LIBELLE_CARATERE	= ResourceManager.getInstance().getString('M21', 'Libre');
			dataGeneric.addItem(objectGeneric);
			
			objectGeneric = new ContenuZone();
			objectGeneric.ID_CARACTERE 		= 1;
			objectGeneric.LIBELLE_CARATERE	= ResourceManager.getInstance().getString('M21', 'Num_rique');
			dataGeneric.addItem(objectGeneric);
			
			objectGeneric = new ContenuZone();
			objectGeneric.ID_CARACTERE 		= 2;
			objectGeneric.LIBELLE_CARATERE	= ResourceManager.getInstance().getString('M21', 'Alpha');
			dataGeneric.addItem(objectGeneric);
		}

	}
}