package
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	
	import composants.HintTextInput;
	
	import entity.CodeLangue;
	
	import interfaceClass.IModuleLogin;
	
	import modulelogin.ihm.retrievepassword.RetrievePasswordIHM;
	
	public class LoginImpl extends Canvas
	{
		private static const default_code_langue 	:String = "fr_FR";
		private var appVersion						:String;
		private var authOp							:AbstractOperation;
		private var sendMailOp						:AbstractOperation;
		private var authInfos						:Object;
		private var infosMessage					:String;
		private var currentlogin					:String;
		private var passwordPopup					:RetrievePasswordIHM;
		private var idLang							:int = 3;
		private var code_langue						:String = default_code_langue;
		private var messColor						:int;
		private var code_application				:Number =  1;		
		private var save_local						:AbstractOperation;
		public var _appliVersion					:String;
		public var _infosMessage					:String;
		
		public var loginText		:HintTextInput;
		public var pwdText			:HintTextInput;
		public var btnAcrobat		:Image;
		public var btnPwd			:Label;
		public var stdConnectButton	:Button;
		public var btnFR 			:Image;
		public var btnES 			:Image;
		public var btnGB 			:Image;
		public var btnUS 			:Image;
		public var btnEU 			:Image;
		public var btnNL 			:Image;
		public var selectedflag		:Image;
		[Bindable]
		public var ckxSeSouvenir	:CheckBox;
		
		public function setInformations(appliVersion:String, infosMessage:String):void{
			this._appliVersion = appliVersion;
			this._infosMessage = infosMessage;
		}
		
		public function setcurrentlogin(curLogin:String = ""):void
		{
			this.currentlogin = curLogin;
		}
		
		public function LoginImpl()
		{
			trace("(Login) Instance Creation **");
			appVersion = this._appliVersion;
			
			this.addEventListener(FlexEvent.CREATION_COMPLETE, afterCreationComplete);
		}
		
		public function getAuthInfos():Object
		{
			return authInfos;
		}
		
		private function afterCreationComplete(event:FlexEvent):void
		{
			trace("(Login) Performing IHM Initialization ****");
			loginText.text = currentlogin;
			pwdText.text = "";
			
			// cursor focus
			if((loginText.text).length == 0){
				loginText.setFocus();
			}else{
				pwdText.setFocus();
			}
			
			//listeners for actions
			btnAcrobat.addEventListener(MouseEvent.CLICK, getAcrobat);
			btnPwd.addEventListener(MouseEvent.CLICK, askPwd);
			
			stdConnectButton.addEventListener(MouseEvent.CLICK, connectItemHandler);
			this.addEventListener(KeyboardEvent.KEY_DOWN, connectItemHandler);
			
			activeControls(true);
			if(moduleLoginIHM.AUTO_LOGIN)
				letsCOnnect();
		}
		
		private function activeControls(state:Boolean):Boolean
		{
			loginText.enabled = pwdText.enabled = stdConnectButton.enabled = btnPwd.enabled = state;
			return state;
		}
		
		private function connectItemHandler(event:Event):void
		{
			if (event is MouseEvent)
			{
				if ((loginText.text.length == 0) || pwdText.text.length == 0)
				{
					var myAlert:Alert = Alert.show(ResourceManager.getInstance().getString('M01', 'Acc_s_Refus____Login_ou_Mot_de_passe_invalide_s_'), ResourceManager.getInstance().getString('M04', 'Connexion'));
					PopUpManager.centerPopUp(myAlert);
				}
				else
					letsCOnnect();
			}
			else if (event is KeyboardEvent)
			{
				if ((event as KeyboardEvent).keyCode == 13)
				{
					letsCOnnect();
				}
			}
		}
		private function letsCOnnect():void
		{
			var cdeLg:CodeLangue = new CodeLangue(code_langue,"","",idLang,"");
			
			var objLogin:Object = new Object();
			objLogin.login = loginText.text;
			objLogin.mdp = pwdText.text;
			objLogin.codeLangue = cdeLg;
			if(ckxSeSouvenir != null)
				objLogin.boolSeSouvenir = ckxSeSouvenir.selected;
			else
				objLogin.boolSeSouvenir = false;
			
			(parentDocument as IModuleLogin).validateLogin(objLogin);
		}
		
		private function getAcrobat(event:Event):void
		{
			var url:String = "http://www.adobe.com/fr/products/acrobat/readstep2.html";
			var variables:URLVariables = new URLVariables();
			var request:URLRequest = new URLRequest(url);
			request.method = "GET";
			navigateToURL(request, "_blank");
		}
		
		
		
		/**
		 * Ouverture de la popUp de demande d'envoi
		 * du mot de passe
		 * @param event
		 *
		 */
		private function askPwd(event:Event):void
		{
			passwordPopup = new RetrievePasswordIHM();
			PopUpManager.addPopUp(passwordPopup, this, true);
			PopUpManager.centerPopUp(passwordPopup);
			trace("#### askPwd ");
			if (loginText.text != "")
			{
				passwordPopup.tiEmail.text = loginText.text;
			}
		}
		
	}
}