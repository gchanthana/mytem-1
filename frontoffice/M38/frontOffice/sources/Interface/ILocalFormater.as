package Interface
{
	public interface ILocalFormater
	{
		
		function formatCurrency(valueCurrency:Number)				 :String;
		
		function formatDate(valueDate:Date)			 				 :String;

		function formatDateWithDot(valueDate:Date)			 		 :String;

		function formatNumber(valueNumber:Number, integer:Boolean)	 :String;
		
	}
}