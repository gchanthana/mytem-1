package Interface
{
	import carol.VO.AnalyseVO;
	
	public interface ISessionUserObject
	{
		
		function addAnalyse(viewInterface:AnalyseVO):void

		function getAnalyse():AnalyseVO

		function getListeAnalyse():Array
		
		function destroySession():void

	}
}