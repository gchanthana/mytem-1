package carol.event
{
	import flash.events.Event;
	
	public class CarolEvent extends Event
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - STATIC - EVENT POPUP
		//--------------------------------------------------------------------------------------------//	
			
		public static const POPUP__VALIDATE 		:String = "POPUP__VALIDATE";
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES STATIC - EVENT NAME
		//--------------------------------------------------------------------------------------------//
		
		public static const CHANGE_VS							:String = "VIEWSTACK_CHANGE";
		public static const CHANGE_CBX							:String = "CHANGE_CBX";
		public static const CHANGE_CBX_PERIMETRE				:String = "CHANGE_CBX_PERIMETRE";
		public static const CHANGE_DATA_PERIMETRE				:String = "CHANGE_DATA_PERIMETRE";
		public static const CHANGE_PERIMETRE					:String = "CHANGE_PERIMETRE";
		
		public static const RESET_DATA							:String = "RESET_DATA";
		public static const OPEN__DATA							:String = "OPEN__DATA";
		public static const SAVE__DATA							:String = "SAVE__DATA";
		public static const EXPOR_DATA							:String = "EXPOR_DATA";
				
		public static const VIEW_DATA							:String = "VIEW_DATA";
		public static const CREATE_DATA							:String = "CREATE_DATA";
		public static const SEARCH_DATA							:String = "SEARCH_DATA";
		
		public static const LOGIN_VALIDED						:String = "LOGIN_VALIDED";
		public static const LOGIN_NOVALID						:String = "LOGIN_NOVALID";
		
		public static const GOTO_DICONNECT						:String = "GOTO_DICONNECT";
		
		public static const RESET_PIVOT							:String = "RESET_PIVOT";
		public static const PAGE_NUMBER_CHANGED					:String = "PAGE_NUMBER_CHANGED";
		
		public static const OPEN_ANALYSE						:String = "OPEN_ANALYSE";
		public static const OPEN_ANALYSE_DATECHANGED			:String = "OPEN_ANALYSE_DATECHANGED";
		public static const OPEN_ANALYSE_DATENOTCHANGED			:String = "OPEN_ANALYSE_DATENOTCHANGED";
		public static const EXPORTPDF_TERMINATED				:String = "EXPORTPDF_TERMINATED";
		
		public static const REFRESH_DATASET_INTERNATIONAL		:String = "REFRESH_DATASET_INTERNATIONAL";
		public static const FAULT_DATASET_INTERNATIONAL			:String = "FAULT_DATASET_INTERNATIONAL";
		public static const LISTE_NOEUD_RESULTEVENT				:String = "LISTE_NOEUD_RESULTEVENT";
		public static const EXPORT_PDF_EVENT					:String	= "EXPORT_PDF_EVENT";
		public static const EXPORT_PDF_AND_SAVE_ANALYSE_EVENT	:String	= "EXPORT_PDF_AND_SAVE_ANALYSE_EVENT";
		
		public static const CHARGE_PERIODESELECTOR				:String	= "CHARGE_PERIODESELECTOR";
		public static const	SAVE_ANALYSE_OK						:String = "SAVE_ANALYSE_OK";
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES PASSEES EN ARGUMENTS
		//--------------------------------------------------------------------------------------------//		
		
		public var obj								:Object = null;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//		
		
		public function CarolEvent(	type		:String,
									bubbles		:Boolean = false,
									objet		:Object  = null,
									cancelable	:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.obj	= objet;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLIC OVERRIDE - COPY OF EVENT
		//--------------------------------------------------------------------------------------------//
				
		override public function clone():Event
		{
			return new CarolEvent(type, bubbles, obj, cancelable);
		}

	}
}
