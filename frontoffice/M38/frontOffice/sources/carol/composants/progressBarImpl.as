package carol.composants
{
	import mx.resources.ResourceManager;
	import carol.event.CarolEvent;
	
	import mx.containers.Box;
	import mx.controls.ProgressBar;
	import mx.events.CubeEvent;
	import mx.managers.PopUpManager;
	
	public class progressBarImpl extends Box
	{

//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//		
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - STATIC - LIBELLE EVENT
		//--------------------------------------------------------------------------------------------//	
		
		private static const _DATALOADED	:String = "DATALOADED";
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - COMPOSANTS
		//--------------------------------------------------------------------------------------------//
		
		public var pgBar					:ProgressBar;
		[Bindable]
		public var indeterminate			:Boolean = true
//--------------------------------------------------------------------------------------------//
//					CONSTRUCTEUR
//--------------------------------------------------------------------------------------------//
		
		public function progressBarImpl()
		{
			//addEventListener(_DATALOADED , closeThis);
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PRIVATE - EVENT
//--------------------------------------------------------------------------------------------//
		
		/* public function progressBarAdvanced(ce:CarolEvent):void
		{
			var progress:int = 0;
			if(!ce.obj.hasOwnProperty("FINISH"))
			{
				progress = (ce.obj.CPTR/ce.obj.TOTAL)*100;
				pgBar.setProgress(progress,100);
	           	pgBar.label= "CurrentProgress" + " " + progress + "%";
			}
			else
			{
				closeThis();
			}
		} */
		public function setProgressBar(e:CubeEvent):void
		{
			if(pgBar.maximum != e.total)
				pgBar.maximum = e.total
			var progress:int = (e.progress * 100) / e.total;
			pgBar.setProgress(e.progress,e.total);
	        pgBar.label= ResourceManager.getInstance().getString('MainCarol', 'Progression___') + " " + progress + "%";
		}
		/* private function closeThis():void
		{
			PopUpManager.removePopUp(this);
		} */

	}
}
