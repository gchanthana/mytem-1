package containerLogin
{
	import classe.Doc;
	import classe.PopupWindows;
	
	import composants.ConsoProgressBar.ConsoProgressBar;
	import composants.util.ConsoviewAlert;
	
	import eventPerso.ContainerEvent;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DateField;
	import mx.controls.ProgressBarMode;
	import mx.controls.RadioButton;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.IFlexDisplayObject;
	import mx.core.ScrollPolicy;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.events.ItemClickEvent;
	import mx.formatters.NumberBaseRoundType;
	import mx.formatters.NumberFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import paginatedatagrid.PaginateDatagrid;
	
	import popup.ContainerprogressBar;
	import popup.PopupCommentaire;
	import popup.PopupEditerDocumentIHM;
	import popup.PopupPartageDocumentIHM;
	import popup.PopupSpecification;
	
	import service.ContainerService;

	public class ContainerLoginImpl extends VBox
	{
/*------------------------------------------------------------------- */		
/*------------------------------ PROPRIETES ------------------------- */
/*------------------------------------------------------------------- */
	// Référence aux composants graphiques	
		[Bindable] public var dtg:PaginateDatagrid;
		public var ckxSelectAll:CheckBox
		public var cboAction:ComboBox
		public var cboLogin:ComboBox
		public var cboEntreprise:ComboBox
		public var cboClient:ComboBox
		public var rbg:RadioButtonGroup;
		public var tiRecherche:TextInput
		public var progress:ConsoProgressBar
		public var rball:RadioButton
		public var rbshare:RadioButton
		public var rbdoc:RadioButton
	// variable
		private var popup:PopupWindows;
		private var nativePath:String;
		private var nbFileTotal:int = 0;			// nombre de fichier drag'n'drop total
		private var nbFileCopy:int  = 1;			// nombre de fichier drag'n'drop copié
		private var dropfiles2:Array				// tableau des fichiers drag'n'drop
		private var rp:Doc							// doc en cours de traitement dans le tableau des fichiers drag'n'drop
		
		private static var TAILLE_LIMITE:Number = 50;
		
		public var zipFile				:ByteArray 
		public var outfile				:FileReference
	// variable bindable 	
		[Bindable]  public var cs					:ContainerService;
		[Bindable]  public var listeDocSelected		:ArrayCollection;
		[Bindable]  public var DocSelected			:Object;
		[Bindable]  public var nbDocSelected		:int = 0;
		[Bindable]  public var poidsTotal			:Number = 0;
		[Bindable] 	public var uploadTotal			:int = 0;
		private var accessGroupSelected				:int = -1; 
/*------------------------------------------------------------------- */		
/*--------------------------------	 MAIN --------------------------- */
/*------------------------------------------------------------------- */	
		public function ContainerLoginImpl()
		{
			super();
		}
/*------------------------------------------------------------------- */		
/*----------------------------- HANDLER ----------------------------- */
/*------------------------------------------------------------------- */
	// Quand on sélectionne une entreprise dans la combobox
		public function cboEntrepriseClickHandler():void
		{
			if(cboEntreprise.selectedIndex > -1)
			{
				accessGroupSelected = cboEntreprise.selectedItem.IDGROUPE_CLIENT
			}
		}
	
	// quand le composant est créationCompleted
		public function creationCompleteHandler():void
		{
			if(dtg != null)
			{
				dtg.dgPaginate.rowCount = 10;
				dtg.dgPaginate.verticalScrollPolicy = ScrollPolicy.OFF;
				dtg.getChildAt(0).visible = false;
				(dtg.getChildAt(0) as UIComponent).includeInLayout = false;
				dtg.getChildAt(1).visible = false;
				(dtg.getChildAt(1) as UIComponent).includeInLayout = false;
				cs = new ContainerService()
			// Les listeners	
				cs.addEventListener(ContainerEvent.VALIDATE_EVENT,refresh);
				cs.addEventListener(ContainerEvent.ZIP_READY,zipReadyHandler);
				cs.addEventListener(ContainerEvent.ZIP_ERROR,zipErrorHandler);
				cs.addEventListener(ContainerEvent.ZIP_SELECT,dirSelectedForZip)
				cs.addEventListener(ContainerEvent.LISTEDOC_RESULTEVENT,listeDocHandler)
				cs.getListeModule(CvAccessManager.getUserObject().CLIENTACCESSID,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
				cs.getGroupeList(CvAccessManager.getUserObject().CLIENTACCESSID);
				rechercheDoc(true)
			}
		}
	// Quand le composant est créé
		public function init():void
		{
		// l'initialisation des variables	
			listeDocSelected= new ArrayCollection();
		}
	// Quand on clique sur une loupe dans le datagrid	
		public function imgLoupeClickHandler(rp:Doc):void
		{
			popup = new PopupSpecification();
			(popup as PopupSpecification).selectedDoc = rp;
			showPopup();
		}
	// quand on clique sur une croix rouge dans le datagrid
		public function imgSuppClickHandler(rp:Doc):void
		{
			var str:String = resourceManager.getString('M62', 'Vous__tes_sur_le_point_de_supprimer_un_D');
			DocSelected = ObjectUtil.copy(rp);

			Alert.okLabel = resourceManager.getString('M62', 'Valider');
			Alert.cancelLabel = resourceManager.getString('M62', 'Annuler');
			Alert.buttonWidth = 75
			Alert.show("",str,Alert.OK | Alert.CANCEL,this,AlertSuppCloseHandler);
		}
	// gestion du choix dans l'alert de suppression d'un Doc
		private function AlertSuppCloseHandler(e:CloseEvent):void
		{
			if(e.detail == Alert.OK)	
			{ 
				var tmptab:ArrayCollection = new ArrayCollection();
				tmptab.addItem(DocSelected);
				for each(var obj:Doc in dtg.dataprovider)
				{
					if(obj.FILEID == DocSelected.FILEID)
					{
						dtg.dataprovider.removeItemAt(dtg.dataprovider.getItemIndex(obj))
						break;
					}
				}
				cs.suppListeDoc(tmptab,CvAccessManager.getUserObject().CLIENTACCESSID);
			}
		}
	// gestion du choix dans l'alert de suppression de plusieurs Docs
		private function AlertSuppXDocCloseHandler(e:CloseEvent):void
		{
			if(e.detail == Alert.OK)	
			{
				for each(var ob:Object in listeDocSelected)
				{
					for each(var obj:Doc in dtg.dataprovider)
					{
						if(obj.FILEID == ob.FILEID)
						{
							dtg.dataprovider.removeItemAt(dtg.dataprovider.getItemIndex(obj))
							break;
						}
					}
				}
				cs.suppListeDoc(listeDocSelected,CvAccessManager.getUserObject().CLIENTACCESSID);
			}
		}
	// quand on clique sur l'icone dans le datagrid pour voir les commentaires
		public function imgCommentaireClickHandler(rp:Doc):void
		{
		 	 popup = new PopupCommentaire();
			(popup as PopupCommentaire).selectedDoc = rp;
			(popup as PopupCommentaire).app_loginid = CvAccessManager.getUserObject().CLIENTACCESSID
			showPopup()
		}
	// quand on clique sur l'icone dans le datagrid pour editer le Doc
		public function imgEditerClickHandler(rp:Doc):void
		{
		 	 popup = new PopupEditerDocumentIHM();
			(popup as PopupEditerDocumentIHM).selectedDoc = rp
			showPopup()
		}	
	// quand on coche sur la combobox pour tout sélectionner
		public function ckxSelectAllClickHandler():void
		{
			for each(var rp:Doc in cs.listeDoc)
			{
				if(rp.STATUS_ID == 2)
				{
					rp.SELECTED = ckxSelectAll.selected;
					if(rp.SELECTED)
					{
						addItemToListeDocSelected(rp)
					}
					else
					{
						for(var i:int=0;i<listeDocSelected.length;i++)
						{
							if(listeDocSelected[i].UUID == rp.UUID)
							{
								listeDocSelected.removeItemAt(i);
								break;
							}
						}
					}
				}
				else
					rp.SELECTED = false
			}
			calculNbSelected()
		}
		private function addItemToListeDocSelected(item:Doc):void
		{
			var boolExist:Boolean = false 
			for each(var obj:Doc in listeDocSelected)
			{
				if(obj.UUID == item.UUID)
				{
					boolExist = true
				}
			}
			if(!boolExist)
				listeDocSelected.addItem(item)
		}
	// quand la suppression est réussie
		private function suppresion_handler(e:ContainerEvent):void
		{
			ConsoviewAlert.afficherOKImage(resourceManager.getString('M62', 'Demande_de_suppression_prise_en_compte'),this);
		}
	// Aprés choix dans la combobox puis clic sur le bouton valider. En fonction de l'option sélectionné, l'action est différente
		protected function btnValiderClickHandler():void
		{
			if(listeDocSelected.length > 0)
			{
				switch(cboAction.selectedItem.code.toString().toUpperCase())
				{
					case "DL" 	: 
						downloadXDocClickHandler();
						break;
					case "SUP" 	:
						suppXDocClickHandler();
						break;
					case "SH" 	:
						if(rbg.selectedValue == rball.value) 
							partagerClickHandler();
						else
							Alert.show(resourceManager.getString('M62', 'Impossible_de_partager_ces_rapports'),resourceManager.getString('M62', 'erreur'));
						break;
				}
			}
			else
			{
				Alert.okLabel = resourceManager.getString('M62', 'Ok');
				Alert.show(resourceManager.getString('M62', 'Veuillez_s_lectionner_un_ou_plusieurs_do'),resourceManager.getString('M62', 'Attention'),Alert.OK);
			}
		}
	// gére la sélection d'un Doc via la checkbox dans le datagrid
		public function updateAddSelectedItems(item:Object):void
		{
			if((item as Doc).SELECTED)
			{
				addItemToListeDocSelected(item as Doc);
			}
			else
			{
				for(var i:int=0;i<listeDocSelected.length;i++)
				{
					if(listeDocSelected[i].UUID == (item as Doc).UUID)
					{
						listeDocSelected.removeItemAt(i);
						break;
					}
				}
			}
			calculNbSelected()
		}
	// gére le changement de filtre 
		public function filtreHandler(evt:ItemClickEvent = null):void
		{
			listeDocSelected = new ArrayCollection()
			rechercheDoc()
		}
		protected function btnRechercherHandler():void
		{
			rechercheDoc(true)
		}
		private function partagerClickHandler():void
		{
			if(checkActionEnable())
			{
				popup = new PopupPartageDocumentIHM();
				(popup as PopupPartageDocumentIHM).listeDocSelected = listeDocSelected;
				(popup as PopupPartageDocumentIHM).cs = this.cs;
				(popup as PopupPartageDocumentIHM).app_loginid = CvAccessManager.getUserObject().CLIENTACCESSID
				showPopup()
			}
			else
			{
				Alert.show(resourceManager.getString('M62', 'Certains_documents_ne_peuvent_pas__tre_t'),resourceManager.getString('M62', 'avertissement'),Alert.OK);
			}
		}
		public function cboClientChangeHandler():void
		{
			cs.getlisteLogin(cboClient.selectedItem.IDRACINE);
		}
		public function cboLoginChangeHandler():void
		{
			cs.getListeModule(cboLogin.selectedItem.APP_LOGINID,cboClient.selectedItem.IDRACINE);
			rechercheDoc()
		}
		protected function intervalleChangeHandler():void
		{
			rechercheDoc()
		}
/* -----------------------------------------------------------------------------------
							GESTION DU TELECHARGEMENT			
   -----------------------------------------------------------------------------------  */
/* ------------------------- TELECHARGEMENT D'UN FICHIER -----------------------------  */	
	// quand on clique sur l'icone dans le datagrid pour télécharger le document
		public function imgTelechargerClickHandler(rp:Doc):void
		{
			if(poidsTotal < TAILLE_LIMITE)
			{
				var str:String = resourceManager.getString('M62', 'Vous__tes_sur_le_point_de_t_l_charger_un');
				DocSelected = ObjectUtil.copy(rp)
	
				Alert.okLabel = resourceManager.getString('M62', 'Valider');
				Alert.cancelLabel = resourceManager.getString('M62', 'Annuler');
				Alert.buttonWidth = 75
				Alert.show("",str,Alert.OK | Alert.CANCEL,this,AlertDownloadCloseHandler);
			}
			else
			{
				Alert.show(resourceManager.getString('M62', 'Taille_maximum_de_t_l_chargement_d_pass_'),resourceManager.getString('M62', 'Erreur'));
			}
		}
	// gestion du choix dans l'alert de téléchargement d'un Doc
		private function AlertDownloadCloseHandler(e:CloseEvent):void
		{
			if(e.detail == Alert.OK)	
			{
				var tmptab:ArrayCollection = new ArrayCollection();
				var obj:Object = new Object()
				obj = ObjectUtil.copy(DocSelected);
				tmptab.addItem(obj);
				var spacePattern:RegExp = new RegExp("[\/\\ ]","g");
				var str:String = DocSelected.LIBELLE.replace(spacePattern,"_")
				var zipNAme:String = str+"_"+DateField.dateToString(new Date(),resourceManager.getString('M62', 'DD-MM-YYYY'));
				cs.downloadListeDocSAAS(tmptab,zipNAme);
			}
		}
/* ------------------------- TELECHARGEMENT DE FICHIERS ------------------------------  */
	// gestion du choix dans l'alert de téléchargement de plusieurs Docs
		private function AlertDownloadXDocCloseHandler(e:CloseEvent):void
		{
			if(e.detail == Alert.OK)	
			{
				var dt:Date = new Date()
				var zipNAme:String = resourceManager.getString('M62', 'Rapports_Telecom_')+DateField.dateToString(dt,resourceManager.getString('M62', 'DD-MM-YYYY'))+"_"+dt.getHours()+dt.getMinutes();
				cs.downloadListeDocSAAS(listeDocSelected,zipNAme);
			}
		}	
		// Quand le fichier zip est prêt à être téléchargé par le client	
		private function zipReadyHandler(e:ContainerEvent):void
		{
			progress.hide();
			ConsoviewAlert.afficherOKImage(resourceManager.getString('M62', 'Op_ration_effectu_e_avec_succ_s'),this);
		}
		// en cas d'erreur IO
		private function zipErrorHandler(e:ContainerEvent):void
		{
			progress.hide();
			Alert.show(resourceManager.getString('M62', 'Impossible_de_copier_ce_fichier'),resourceManager.getString('M62', 'Erreur'));	
		}
		private function dirSelectedForZip(e:Event):void
		{
			progress= new ConsoProgressBar(	ResourceManager.getInstance().getString('M62','Traitement_en_cours___'),
				"",null,true,0,0,false,ProgressBarMode.EVENT,false);
			progress.show(this);
		}
		private function zipOutputCompleteHandler(e:Event):void
		{
			progress.hide();
			ConsoviewAlert.afficherOKImage(resourceManager.getString('M62', 'Op_ration_effectu_e_avec_succ_s'),this);
		}
		private function ioErrorHandler(e:IOErrorEvent):void
		{
		}
		private function uploadProgress(e:ProgressEvent):void
		{
			progress.setProgressBar(e.bytesLoaded,e.bytesTotal);
		}
		private function uploadComplete(e:ContainerEvent):void
		{
			rp.addToTableContainer()
		}
		private function uploadError(e:ContainerEvent):void
		{
			ConsoviewAlert.afficherSimpleAlert(resourceManager.getString('M62', 'Impossible_de_copier_ce_fichier___erreur'),resourceManager.getString('M62', 'Upload_erreur'));
			progress.hide();
		}
		private function documentAddedHanler(e:ContainerEvent):void
		{
			rechercheDoc()
			nbFileCopy ++
			if(nbFileCopy >= nbFileTotal)
			{
				progress.hide();
				NotifyClient()
				rechercheDoc()
			}
			else
			{
				gestFile()
			}
		}
/*------------------------------------------------------------------- */		
/*---------------------------- METHODES ----------------------------- */		
/*------------------------------------------------------------------- */
	// Gestion des fichiers aprés drop de fichiers
		private function gestFile():void
		{
			rp 					= new Doc();
			rp.ENCOURS 			= false;
			rp.BOOL_ERROR 		= false;
			rp.SELECTED 		= false;
			rp.SHARED 			= false;
			
			rp.DATE_DEMANDE 	= new Date();
			rp.DEMANDEUR_NOM 	= resourceManager.getString('M62','Service_Client_Consotel');
			rp.FORMAT 			= dropfiles2[nbFileCopy].extension
			rp.LIBELLE 			= dropfiles2[nbFileCopy].name.substr(0,dropfiles2[nbFileCopy].name.length - 4);
			rp.LIBELLE_SHORT 	= rp.LIBELLE.replace(" ","_");
			rp.IDRACINE 		= cboClient.selectedItem.IDRACINE
			rp.APP_LOGINID 		= cboLogin.selectedItem.APP_LOGINID
			rp.APPLI 			= "ConsoView"
			rp.STATUS 			= resourceManager.getString('M62','disponible')
			rp.TAILLE 			= dropfiles2[nbFileCopy].size
			rp.MODULE 			= "Intranet"
			
			// on uploade le fichier vers le FTP container via le backoffice	
			rp.addEventListener(ProgressEvent.PROGRESS,uploadProgress);
			rp.addEventListener(ContainerEvent.UPLOAD_COMPLETE,uploadComplete);
			rp.addEventListener(ContainerEvent.UPLOAD_ERROR, uploadError);
			rp.addEventListener(ContainerEvent.DOCUMENT_ADDED,documentAddedHanler)
			rp.uploadToFTP();
		}
	// ajouter un rapport, aprés drag and drop, à la liste des rapports d'un container d'un client
		private function addRapport(rp:Doc):void
		{
			if(cs.listeDoc == null)
				cs.listeDoc = new ArrayCollection();
			cs.listeDoc.addItem(rp); 
		}	
	// avertit le client que plusieurs documents ont été déposé sur son container
		private function NotifyClient():void
		{
			var obj:Object = new Object()
			obj.MAILTO 					=	cboLogin.selectedItem.LOGIN
			obj.DATE_DEMANDE 			=	DateField.dateToString(new Date(),"DD/MM/YYYY");			 
			obj.PERIMETRE				=	""
			obj.SURNAME					=	cboLogin.selectedItem.NOM
			obj.FIRSTNAME				=	cboLogin.selectedItem.PRENOM
			obj.NOM_DETAILLE_RAPPORT	=	""
			obj.APP_LOGINID				=	cboLogin.selectedItem.APP_LOGINID
			obj.IDRACINE				=	cboClient.selectedItem.IDRACINE
			cs.SENDMailPublicationParProductionClient(obj);
		}
	// envoyer le Doc à l'API
		private function sendDocToFTP(f:FileReference):void
		{
			/* var url:URLRequest = new URLRequest(moduleContainerIHM.NonSecureUrlBackoffice+"/fr/consotel/consoview/M62/processUpload.cfm");
			f.upload(url); */
		}
	// Configure et affiche le popup	
		private function showPopup():void
		{
			(popup as PopupWindows).addEventListener(ContainerEvent.CLOSE_EVENT,closePopup);
			(popup as PopupWindows).addEventListener(ContainerEvent.VALIDATE_EVENT,refresh);
			PopUpManager.addPopUp(popup as IFlexDisplayObject,this,true);
			PopUpManager.centerPopUp(popup as IFlexDisplayObject);
		}
	// désaffiche le popup
		private function closePopup(e:ContainerEvent = null):void
		{
			PopUpManager.removePopUp(popup as IFlexDisplayObject);
		}
	// désaffiche le popup et met à jour le datagrid en rappellant la procédure
		protected function refresh(e:ContainerEvent = null):void
		{
			if(listeDocSelected != null)
				listeDocSelected = new ArrayCollection()
			if(popup != null && (popup as PopupWindows).isPopUp)
			{	
				PopUpManager.removePopUp(popup);
				ConsoviewAlert.afficherOKImage(resourceManager.getString('M62', 'Op_ration_effectu_e_avec_succ_s'),this);	
			}
			if(dtg.dataprovider != null && dtg.dataprovider.length > 0) 
				rechercheDoc()
			else	
				rechercheDoc(true)
			ckxSelectAll.selected = false;
		}
	// gére la suppression de plusieurs Docs
		private function suppXDocClickHandler():void
		{
			if(poidsTotal < TAILLE_LIMITE)
			{
				var str:String = resourceManager.getString('M62', 'Vous__tes_sur_le_point_de_supprimer_')+listeDocSelected.length+resourceManager.getString('M62','_document_s_');
	
				Alert.okLabel = resourceManager.getString('M62', 'Valider');
				Alert.cancelLabel = resourceManager.getString('M62', 'Annuler');
				Alert.buttonWidth = 75
				Alert.show("",str,Alert.OK | Alert.CANCEL,this,AlertSuppXDocCloseHandler);
			}
			else
			{
				Alert.show(resourceManager.getString('M62', 'Taille_maximum_de_t_l_chargement_d_pass_'),resourceManager.getString('M62','erreur'));
			}
		}
	// gére le téléchargement de plusieurs Docs	
		private function downloadXDocClickHandler():void
		{
			if(poidsTotal < TAILLE_LIMITE)
			{
				if(checkActionEnable())
				{
					var str:String = resourceManager.getString('M62', 'Vous__tes_sur_le_point_de_t_l_charger_')+listeDocSelected.length+' '+resourceManager.getString('M62','_document_s_');
		
					Alert.okLabel = resourceManager.getString('M62', 'Valider');
					Alert.cancelLabel = resourceManager.getString('M62', 'Annuler');
					Alert.buttonWidth = 75
					Alert.show("",str,Alert.OK | Alert.CANCEL,this,AlertDownloadXDocCloseHandler);
				}
				else
				{
					Alert.show(resourceManager.getString('M62', 'Certains_documents_ne_peuvent_pas__tre_t'),resourceManager.getString('M62','avertissement'),Alert.OK);
				}
			}
			else
			{
				Alert.show(resourceManager.getString('M62', 'Taille_maximum_de_t_l_chargement_d_pass_'),resourceManager.getString('M62',"erreur"));
			}
		}
	// vérifie que les fichiers sélectionnés soient téléchargeables
		private function checkActionEnable():Boolean
		{
			for each(var rp:Object in listeDocSelected)
			{
				if(rp.STATUS_ID != 2)
				{
					return false
				}
			}
			return true
		}
	// Calcule le nombre de document sélectionné
		private function calculNbSelected():void
		{
			var nbDocDisabled:int = 0;
			var nbformat:NumberFormatter = new NumberFormatter()
			nbformat.precision = 1
			nbformat.rounding = NumberBaseRoundType.UP
			poidsTotal = 0;
			nbDocSelected = 0;
			for each(var obj:Object in listeDocSelected)
			{
				if(obj.SELECTED)
				{
					nbDocSelected ++;
					poidsTotal = parseFloat(nbformat.format(poidsTotal + obj.TAILLE))
				}
				else
				{
					listeDocSelected.removeItemAt(listeDocSelected.getItemIndex(obj))
				}
			}
			poidsTotal = parseFloat(nbformat.format(poidsTotal));
			
			var boolIsOk:Boolean = true
			for each(var rp:Doc in cs.listeDoc)
			{
				if(rp.STATUS_ID == 2 && !rp.SELECTED)
				{
					boolIsOk= false
					break;	
				}
			}
			ckxSelectAll.selected = boolIsOk
		}
	// lance une recherche de rapport
		private function rechercheDoc(newSeach:Boolean = false):void
		{
			var strmodule:String = "";
			var strfiltre:int;
			var strrecherche:String = ""
			var nbstart:int = 0;

			if(newSeach)
				listeDocSelected = new ArrayCollection()
			nbDocSelected = 0

			if(rbg.selectedValue == 'all')
			{
				strfiltre = 1;
			}
			else if(rbg.selectedValue == 'share')
			{
				strfiltre = 2;	
			}
			else if(rbg.selectedValue == 'doc')
			{
				strfiltre = 3;	
			}
			if(tiRecherche.text != null && tiRecherche.text.length > 0)
			{
				strrecherche = tiRecherche.text
				tiRecherche.text = strrecherche
			}
			if(dtg != null && dtg.currentIntervalle != null)
			{
				if(newSeach)
					nbstart = 0
				else
					nbstart = dtg.currentIntervalle.indexDepart
			}
			if(newSeach)
			{
				dtg.refreshPaginateDatagrid();
 				var args:Array = [strfiltre,strrecherche,strmodule,nbstart]
 				callLater(rechercheDocProc,args)
 			}
 			else
 			{
 				rechercheDocProc(strfiltre,strrecherche,strmodule,nbstart)
 			}
		}
		private function rechercheDocProc(strfiltre:int,strrecherche:String,strmodule:String,nbstart:int):void
		{
			cs.getListeDoc(CvAccessManager.getUserObject().CLIENTACCESSID,strfiltre,strrecherche,strmodule,nbstart,10,accessGroupSelected);
		}
	// HANDLER 
		private function listeDocHandler(e:ContainerEvent):void
		{
			for each(var rp:Doc in cs.listeDoc)
			{
				for each(var obj:Object in listeDocSelected)
				{
					if(rp.UUID == obj.UUID)
					{
						rp.SELECTED = true
						break;
					}
				}
			}
			dtg.dgPaginate.dataProvider = cs.listeDoc
			ckxSelectAll.selected = false;
			calculNbSelected()
		}	
/*------------------------------------------------------------------- */		
/*---------------------------- LABEL FUNCTION ----------------------- */		
/*------------------------------------------------------------------- */
		public function dateFormat(item:Object, column:DataGridColumn):String
		{
			var str:String = resourceManager.getString('M62', 'DD_MM_YYYY3656')
			return DateField.dateToString((item as Doc).DATE_DEMANDE,str);
		}
		public function tailleFormat(item:Object, column:DataGridColumn):String
		{
			return (item as Doc).TAILLE+' '+resourceManager.getString('M62', 'Mo___')
		}
	}
}
