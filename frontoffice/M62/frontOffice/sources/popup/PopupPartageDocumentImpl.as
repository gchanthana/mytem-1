package popup
{
	import classe.Login;
	import classe.PopupWindows;
	
	import eventPerso.ContainerEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.RadioButton;
	import mx.controls.RadioButtonGroup;
	
	import service.ContainerService;

	public class PopupPartageDocumentImpl extends PopupWindows
	{
		public var rbg:RadioButtonGroup;
		public var rbnotall:RadioButton
		public var rball:RadioButton
		public var vbUser:VBox;
		public var dtgUtilisateur:DataGrid
		public var ckxSelectAll:CheckBox
		
		[Bindable] public var cs:ContainerService;
	// contient la liste des logins sélectionnés dans cet écran
		[Bindable] protected var listeLoginSelected:ArrayCollection;
	// Contient la liste des documents sélectionnés sur l'écran principal	
		public var listeDocSelected:ArrayCollection;
	// app_loginid de la personne connectée
		public var app_loginid:int
	// nombre de login sélectionné dans cet écran
		[Bindable] protected var nbLoginSelected:int = 0;
		
		public function PopupPartageDocumentImpl()
		{
			super();
			listeLoginSelected = new ArrayCollection();
		}
		protected function creationCompleteHandler():void
		{
			cs.addEventListener(ContainerEvent.VALIDATE_EVENT,validate);
			var idracine:int = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX
			cs.getlisteLoginShare(idracine)
		}
		protected function ckxSelectAllHandler():void
		{
			for each(var log:Login in cs.listeLogin)
			{
				log.SELECTED = ckxSelectAll.selected
			}
			calculNbLogin()
		}
		protected function rbgChangeHandler():void
		{
			if(rbg.selectedValue == "notall")
			{
				vbUser.visible = true;
				vbUser.includeInLayout = true;
			}
			else
			{
				vbUser.visible = false;
				vbUser.includeInLayout = false;
			}
		}
		public function SelectionLoginHandler():void
		{
			calculNbLogin()
		}
		private function calculNbLogin():void
		{
			nbLoginSelected = 0;
			listeLoginSelected = new ArrayCollection();
			for each(var log:Login in cs.listeLogin)
			{
				if(log.SELECTED)
				{
					nbLoginSelected++;
					listeLoginSelected.addItem(log);
				}
			}
			if(nbLoginSelected == 0)
				ckxSelectAll.selected = false;
			if(nbLoginSelected == cs.listeLogin.length)
				ckxSelectAll.selected = true; 
			else
				ckxSelectAll.selected = false;
		}
		protected function btnValiderHandler():void
		{
			if(rbg.selectedValue == rball.value)
			{
				cs.shareDoc(cs.listeLogin,listeDocSelected,app_loginid);
			}
			else
			{
				cs.shareDoc(listeLoginSelected,listeDocSelected,app_loginid);
			}
		}
		private function validate(e:ContainerEvent):void
		{
			validateWindow()
		}
	}
}