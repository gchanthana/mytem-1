package popup
{
	import mx.resources.ResourceManager;
	import classe.PopupWindows;
	
	import eventPerso.ContainerEvent;
	
	import mx.controls.Alert;
	import mx.controls.TextInput;

	public class PopupEditerDocumentImpl extends PopupWindows
	{
		public var tiLibelle:TextInput;
		
		public function PopupEditerDocumentImpl()
		{
			super();
		}
		protected function init():void
		{
			selectedDoc.addEventListener(ContainerEvent.DOCUMENT_RENAMED,documentRenamedHandler)
		}
		private function documentRenamedHandler(e:ContainerEvent):void
		{
			validateWindow()
		}
		protected function btnValiderClickHandler():void
		{
			if(tiLibelle != null && tiLibelle.text.length > 0)
			{
				selectedDoc.LIBELLE = tiLibelle.text
				selectedDoc.renameDoc() 
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M62', 'Veuillez_saisir_un_nom_pour_ce_document'),ResourceManager.getInstance().getString('M62', 'Erreur'));
			}
		}
		protected function btnAnnulerClickHandler():void
		{
			removeWindow()
		}
	}
}