package composants.util
{
	import mx.formatters.DateFormatter;
	import mx.utils.ObjectUtil;
	
	
	public class DateFunction {
		/**
		 * Tableau contenant les libéllés (symboles de 1 lettre) des jours de la semaine
		 * */
		public static const DAY_NAMES:Array =
			["D","L","M","M","J","V","S"];
		/**
		 * Tableau contenant les noms des mois de l'année
		 * */
		public static const MONTH_NAMES:Array =
			["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Décembre"];
		/**
		 * Date du jour (De type Date).
		 * */
		public static const NOW_DATE:Date = new Date();
	
   /**
	* Retourne le tableau  des 12 dernier mois 
	* 
	**/ 
	public static function getPeriode():Array{
		var df : DateFormatter = new DateFormatter();    	
		var tabPeriode : Array = new Array();
		var invoiceDate:Date = new Date();					
		var endDate:Date = new Date(invoiceDate.getTime() - (30 * millisecondsPerDay));		
	  	var startDate:Date = new Date(invoiceDate.getTime() - (365 * millisecondsPerDay));
	  	
	  	var j :int = 0;
	  	
	  	df.formatString = "DD/MM/YYYY";
	  	for (var i:int = 30 ; i < 365; i=i+30){		
	  		var newDate:Date = new Date(invoiceDate.getTime() - (i * millisecondsPerDay));	

			newDate.setDate(1);				
			tabPeriode.unshift(df.format(newDate.toDateString()));
	  	}
		
		return tabPeriode;  		             
	}
	
	 /**
	 * 
	 * Retourne le mois en lettre ex 0 -> Janvier
	 * @param mois : Number Le nombre représentant le mois 
	 * 
	 **/
	 public static function moisEnLettre(a:Number):String {
    	 var v:Array=new Array("Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");
 	    return v[a];
    } 
	 
	 /**
	 dateDiff(datePart:String, date1:Date, date2:Date):Number<BR>
	 returns the difference between 2 dates<BR>
	 valid dateParts:<BR>
	 s: Seconds<BR>
	 n: Minutes<BR>
	 h: Hours<BR>
	 d: Days<BR>
	 m: Months<BR>
	 y: Years<BR>
	 */
	 public static function dateDiff(datePart:String, date1:Date, date2:Date):Number{
	 	return getDatePartHashMap()[datePart.toLowerCase()](date1,date2);
	 }
	 
	 
	/** dateAdd(datePart:String,date:Date,num:Number):Date<BR>
	 returns a new date object with the appropriate date/time settings<BR>
	*/
	public static function dateAdd(datePart:String,date:Date,num:Number):Date{
		// get date part object;
		var dpo : Object = getDateAddPartHashMap()[datePart.toLowerCase()];
		// create new date as a copy of date passed in
		var newDate : Date = new Date(date.getFullYear(),date.getMonth(),date.getDate(),date.getHours(),date.getMinutes(),date.getSeconds(),date.getMilliseconds());
		// set the appropriate date part of the new date
		newDate[dpo.set](date[dpo.get]()+num);
		// return the new date
		return newDate;
	}

	public static function formatDateAsString(d:Date):String{
	 	if(d != null)
		{
			var jour:String=d.getDate().toString();
			if (jour.length==1)jour="0"+jour;
			var mois:String=(d.getMonth()+1).toString();
			if (mois.length==1) mois="0"+mois;
			var annee:String=d.getFullYear().toString();
			var v:String=jour+"/"+mois+"/"+annee;
			return v;
		}
		else
		{
			return "";
		}
	}
	
	public static function formatDateAsShortString(d:Date):String{
		if(d != null)
		{
			var jour:String=d.getDate().toString();
			if (jour.length==1)jour="0"+jour;
			var mois:String=(d.getMonth()+1).toString();
			if (mois.length==1) mois="0"+mois;
			var annee:String=d.getFullYear().toString().substr(2,2);
			var v:String=jour+"/"+mois+"/"+annee;
			return v;
		}
		else
		{
			return "";
		}
	}
		
	public static function formatDateAsInverseString(d:Date):String{
		if(d != null)
		{
			var jour:String=d.getDate().toString();
			if (jour.length==1)jour="0"+jour;
			var mois:String=(d.getMonth()+1).toString();
			if (mois.length==1) mois="0"+mois;
			var annee:String=d.getFullYear().toString();
			var v:String=annee+"/"+mois+"/"+jour;
			return v;	
		}
		else
		{
			return "";
		}
	}
	
	
	public static function getDateFromInverseDateString(value:String,delimiter:String):Date
	{
		if(value){
			var stringDate:String = value;
			var tabDate:Array = stringDate.split(delimiter);
			switch(tabDate.length)
			{
								
				case 3 :
				{
					var annee:Number = Number(tabDate[0]);
					var mois:Number = Number(tabDate[1])-1;
					var jour:Number = Number(tabDate[2]);
					return new Date(annee,mois,jour);
				}
				case 2 : 
				{
					var annee2:Number = Number(tabDate[0]);
					var mois2:Number = Number(tabDate[1])-1;
					return new Date(annee2,mois2);	
				}
				case 1 :
				{
					var annee3:Number = Number(tabDate[0]);
					return new Date(annee3);
				}
				
				default: return null				
					
			}	
		}
		else
		{
			return null
		}
		
	}
	
	public static function formatODBCDATEToFrenchLiteralDate(d:Date):String{
		var jour:String=d.getDate().toString();		
		var mois:String=(d.getMonth()).toString();
		var annee:String=d.getFullYear().toString();
		var v:String=jour+" "+
			DateFunction.moisEnLettre(parseInt(mois))+" "+annee;
		
		return  v;
	}
	 
	/*------------------------------------------------ PRIVATE --------------------------------------------------*/ 
	 
	public static const millisecondsPerMinute:int = 1000 * 60;
	public static const millisecondsPerHour:int = 1000 * 60 * 60;
	public static const millisecondsPerDay:int = 1000 * 60 * 60 * 24;
	 
	 
	private static function getDateAddPartHashMap():Object{
		var dpHashMap : Object = new Object();
		dpHashMap["s"] = new Object();
		dpHashMap["s"].get = "getSeconds";
		dpHashMap["s"].set = "setSeconds";
		dpHashMap["n"] = new Object();
		dpHashMap["n"].get = "getMinutes";
		dpHashMap["n"].set = "setMinutes";
		dpHashMap["h"] = new Object();
		dpHashMap["h"].get = "getHours";
		dpHashMap["h"].set = "setHours";
		dpHashMap["d"] = new Object();
		dpHashMap["d"].get = "getDate";
		dpHashMap["d"].set = "setDate";
		dpHashMap["m"] = new Object();
		dpHashMap["m"].get = "getMonth";
		dpHashMap["m"].set = "setMonth";
		dpHashMap["y"] = new Object();
	  	dpHashMap["y"].get = "getFullYear";
	  	dpHashMap["y"].set = "setFullYear";
		return dpHashMap;
 	}

	 private static function getDatePartHashMap():Object{
	  var dpHashMap:Object = new Object();
	  dpHashMap["s"] = getSeconds;
	  dpHashMap["n"] = getMinutes;
	  dpHashMap["h"] = getHours;
	  dpHashMap["d"] = getDays;
	  dpHashMap["m"] = getMonths;
	  dpHashMap["y"] = getYears;
	  return dpHashMap;
	 }
	 
	 private static function compareDates(date1:Date,date2:Date):Number{
	  return date1.getTime() - date2.getTime();
	 }
	 
	 private static function getSeconds(date1:Date,date2:Date):Number{
	  return Math.floor(compareDates(date1,date2)/1000);
	 }
	 
	 private static function getMinutes(date1:Date,date2:Date):Number{
	  return Math.floor(getSeconds(date1,date2)/60);
	 }
	 
	 private static function getHours(date1:Date,date2:Date):Number{
	  return Math.floor(getMinutes(date1,date2)/60);
	 }
	 
	 private static function getDays(date1:Date,date2:Date):Number{
	  return Math.floor(getHours(date1,date2)/24);   
	 }   
	 
	 private static function getMonths(date1:Date,date2:Date):Number{
	  var yearDiff : int = getYears(date1,date2);
	  var monthDiff : int = date1.getMonth() - date2.getMonth();
	  if(monthDiff < 0){
	   monthDiff += 12;
	  }
	  if(date1.getDate()< date2.getDate()){
	   monthDiff -=1;
	  }
	  return 12 *yearDiff + monthDiff;
	 }
	 
	 private static function getYears(date1:Date,date2:Date):Number{
	  return Math.floor(getDays(date1,date2)/365);
	 }
	
	/**
	 * getNbJoursOuvres(date1:Date,date2:Date)
	 * Retour le nombre de jours ouvrés(Jour de la semaine travaillé)
	 * sont aussi gérées les périodes suivantes : Pâques  Ascension Pentecote 
	 * Sont gérés les jours :
	 * 	// 1er janvier 
		// 1er mai 
		// 5 mai 
		// 14 juillet 
		// 15 aout 
		// 1er novembre 
		// 11 novembre 
		// 25 décembre 
	 * */
	public static function getDiffJoursOuvres(dateOne:Date,date2:Date):int {
	
		var tmpDate:Date = new Date();
		
		trace("jour de paque cette année :"+DateFunction.formatDateAsShortString(getDatePaquesOfThisYear(tmpDate.getUTCFullYear()+1)));
		var date1 : Date= ObjectUtil.copy(dateOne) as Date;
		var millisecondsPerDay:int = 1000 * 60 * 60 * 24;
		var numberOfDays:int = 0;
			
		while (date1.getTime() <= date2.getTime()) 
		{
			//Si le jour est entre lundi et vendredi on l'ajoute
			if (date1.day >= 1 && date1.day <=5) 
			{
				if(isFerie(date1) == false)
				{
					numberOfDays ++
				}
				
			}
			//On avance d'un jour
			date1.setTime(date1.getTime() + millisecondsPerDay);
		}
		
		return numberOfDays; 
		
		
	}
	private static function alpha(year:int) : int
    {
        if (year <= 1582)
            return 0;
        else if (year <= 1699)
            return 7;
        else if (year <= 1899)
            return 8;
        else if (year <= 2199)
            return 9;
        else
            return 10;
    }

    private static function beta(year:int):int
    {
        if (year <= 1582)
            return 0;
        else if (year <= 1699)
            return 4;
        else if (year <= 1799)
            return 3;
        else if (year <= 1899)
            return 2;
        else if (year <= 2099)
            return 1;
        else if (year <= 2199)
            return 0;
        else
            return 6;
    }

    private static function r1(year:int):int
    {
        return (19 * (year % 19) + 15 + alpha(year)) % 30;
    }

    /** 
     * Nombre de jours entre Pâques et le 21 mars.
     *
     * @param year l'année dont on souhaite connaître le jour de Pâques.
     * @return le nombre de jours.
     */
    private static function r(year:int):int
    {
        var r1m:int = r1(year);
        return r1m < 28 ? r1m : (r1m == 28 ? (year % 19 > 10 ? 27 : 28) : 28);
    }

    private static function t(year:int):int
    {
        return (2 * (year % 4) + 4 * (year % 7) + 6 * r(year) + 6 - beta(year)) % 7;
    }

    /**
     * Calcule le jour de Pâques pour une année donnée.
     *
     * @param year l'année dont on souhaite connaître le jour de Pâques.
     * @return le jour de Pâques.
     */
     public static function getDatePaquesOfThisYear(annee:int):Date
     {
         var year:int = annee;
         var rm:int = r(year);
         var tm:int = t(year);

         var day:int = rm + tm <= 9 ? rm + tm + 22 : rm + tm - 9;
         var month:int = rm + tm <= 9 ? 2 : 3;
              	 
         return new Date(year,month,day);
     }
     public static function isFerie(date1:Date):Boolean
     {
     	var datePaque : Date = getDatePaquesOfThisYear(date1.fullYearUTC);
     	
     	/**
     	 * DEBUG GESTION DES JOURS FERIES
     	 * 
     	trace("date1 :"+DateFunction.formatDateAsShortString(date1));
     	trace("paque :"+DateFunction.formatDateAsShortString(datePaque));
     	trace("Lundi paque :"+DateFunction.formatDateAsShortString(addDays(datePaque,1)));
     	trace("Ascension :"+DateFunction.formatDateAsShortString(addDays(datePaque,39)));
     	trace("Pentecote :"+DateFunction.formatDateAsShortString( addDays(datePaque,49)));
     	trace("Lundi Pentecote :"+DateFunction.formatDateAsShortString( addDays(datePaque,50)));
     	* */
     	
     	if(
     	(date1.date == 1 && date1.month ==0)  ||// 1er janvier 
		(date1.date == 1 && date1.month ==4)  ||// 1er mai 
		(date1.date == 8 && date1.month ==4)  ||// 8 mai 
		(date1.date == 14 && date1.month ==6) ||// 14 juillet 
		(date1.date == 15 && date1.month ==7) ||// 15 aout 
		(date1.date == 1 && date1.month ==10) ||// 1er novembre 
		(date1.date == 11 && date1.month ==10)||// 11 novembre 
		(date1.date == 25 && date1.month ==11)||// 25 décembre
		
		(date1.getTime() == (datePaque as Date).getTime())||//Dimanche de paque
		(date1.getTime() == (addDays(datePaque,1)as Date).getTime()) ||// Lundi de pâque
		(date1.getTime() == (addDays(datePaque,39)as Date).getTime())||// Ascension
		(date1.getTime() == (addDays(datePaque,49)as Date).getTime())||// dimache Pentecote
		(date1.getTime() == (addDays(datePaque,50)as Date).getTime())//lundi Pentecote
		
		)
		{
			return true
		}
		else
		{
			return false
		}
		
     }
     public static function addWeeks(date:Date, weeks:Number):Date {
        return addDays(date, weeks*7);
    }

    public static function addDays(date:Date, days:Number):Date {
        return addHours(date, days*24);
    }

    public static function addHours(date:Date, hrs:Number):Date {
        return addMinutes(date, hrs*60);
    }

    public static function addMinutes(date:Date, mins:Number):Date {
        return addSeconds(date, mins*60);
    }

    public static function addSeconds(date:Date, secs:Number):Date {
        var mSecs:Number = secs * 1000;
        var sum:Number = mSecs + date.getTime();
        return new Date(sum);
    }


	
	 
	}
}

