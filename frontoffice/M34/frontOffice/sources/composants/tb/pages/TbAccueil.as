package composants.tb.pages
{
	import composants.tb.graph.GraphFactory;
	import composants.tb.graph.GraphParameters;
	import composants.tb.graph.IGraphModel;
	import composants.tb.tableaux.ITableauModel;
	import composants.tb.tableaux.TableauAboFactoryStratey;
	import composants.tb.tableaux.TableauChangeEvent;
	import composants.tb.tableaux.TableauConsoFactoryStrategy;
	import composants.tb.tableaux.TableauFactory;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.containers.TitleWindow;
	import mx.core.Container;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	 
	
	public class TbAccueil extends TbAccueil_IHM implements IPage 
	{
		//le segment courrant;
		protected var _segment : String;		
		
		private var lastMoisFin : String = "x";
		
		private var lastMoisDebut : String = "x";				
		
		private var moisDeb : String; 
		 
		private var moisFin : String;	
		
		private var perimetre : String;
		
		private var modeSelection : String;
		
		private var lastIdentifiant : String = "z";
		
		private var identifiant : String;		
		
		private const _pageType : String = "ACCUEIL";
		
		/*----------------------- TABLEAUX ----------------------------*/

		//createur de tableau
		protected var _tf : TableauFactory = new TableauFactory();

		public var _tbAbo : ITableauModel;
		
		public var _tbConso : ITableauModel;
		
		private var _total : Number;
		 
		/*------------------------ GRAPHS -------------------------------*/
		//createur de graph
		protected var _gf : GraphFactory = new GraphFactory();	
		
		protected var _gRepartition : IGraphModel;
		
		protected var _gRepConso : IGraphModel;
		
		protected var _gRepDureeConso : IGraphModel;
		
		//protected var _gEvolution : IGraphModel;
		
				 
		protected var _nextPageType : String; 
		
		protected var _pageLibelle : String ;
		
		protected var _type : TableauChangeEvent;
		
		protected var _goToNextPage : Function ;
				
		/**
		* Constructeur 
		**/
		public function TbAccueil(type :  TableauChangeEvent = null)
		{
			super();	
			_pageLibelle = "Accueil";
			_type = type;
			
			addEventListener(FlexEvent.CREATION_COMPLETE,init);					
		}
		
		public function get pageType():String{
			return _pageType;
		}
		
		/**
		 * Retourne le type de la page la suit
		 * @return type le type de la page suivante
		 * */		
		public function get nextPageType():String{
			//TODO: implement function
			return _nextPageType;
		}
		
		/**
		 * Retourne le type de la page la suit
		 * @return type le type de la page suivante
		 * */		
		public function set nextPageType(type : String):void{
			_nextPageType = type;
		}
						
		/**
		* Reset le compossant, remet toutes les variables a 0, interromp les remotings en cours
		**/
		public function clean():void
		{
			//TODO: implement function
		}
		
		public function getLibelle():String
		{
			//TODO: implement function
			return _pageLibelle;
		}
		
		public function getTotal():Number
		{
			//TODO: implement function
			return _total;
		}		
		
		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...) 			
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		public function updatePerimetre(perimetre : String = null, modeSelection : String = null, idt : String= null):void
		{			
			this.perimetre = perimetre;
			this.modeSelection = modeSelection;			
			this.identifiant = idt;
		}
		
		
		/**
		 * Permet d'exporter un rapport flash,pdf ou excel 
		 * @param format Le format (flashpaper, excel ou pdf)
		 * */
		public function exporterRapport( format: String, rSociale : String = ""):void{			
			 			
		 	var urlback : String = ConsoViewModuleObject.NonSecureUrlBackoffice+ "/fr/consotel/consoview/cfm/tb/pdf/print_document.cfm";	
            var variables : URLVariables = new URLVariables();
            
            variables.format = format;
            variables.tb = _type.SEGMENT;
            variables.datedeb = moisDeb; 
            variables.datefin = moisFin;
            variables.raisonsociale = rSociale;
            
            
            var request:URLRequest = new URLRequest(urlback);
            request.data = variables;
            request.method=URLRequestMethod.POST;
            navigateToURL(request,"_blank");  
		}
		
		/**
		 * Met à jour la periode
		 * */		 
		public function updatePeriode(moisDeb : String = null, moisFin : String = null):void{
			//TOTO: implement function
			this.moisDeb = moisDeb;
			this.moisFin = moisFin;
		}			
		
		public function updateSegment(segment : String = ""):void
		{
			//_gEvolution.updateSegment(segment);
			_type.SEGMENT = segment;
			_segment = segment;
			if (segment.toUpperCase() == "COMPLET"){
				_type.TYPE = "ACCUEIL";
			} else {
				_type.TYPE =  "SEGMENT";
			}			 
		}
		
		
		public function update():void{
			if (lastMoisDebut.concat(lastMoisFin).concat(lastIdentifiant) != moisDeb.concat(moisFin).concat(identifiant)){
				callLater(updateComposants);							
				lastMoisDebut = moisDeb;
				lastMoisFin = moisFin;
				lastIdentifiant = identifiant;
			} 		 	
		}
		 
		//initialisation de l'IHM
		protected function init(fe:FlexEvent):void{
			//creation des elements graphiques
			//tableau	
			
			createTableauAccueil();
			
							
			//graph
			var graphP : GraphParameters = new GraphParameters();
			graphP.SEGMENT = _type.SEGMENT;
			
			graphP.TYPE_GRAPH = "COUTPAROPERATEUR";			
			_gRepartition = _gf.createGraph(graphP);
			
			graphP.TYPE_GRAPH = "COUTCONSOPAROPERATEUR";	
			_gRepConso = _gf.createGraph(graphP);
			
			graphP.TYPE_GRAPH = "DUREEPAROPERATEUR";		
			_gRepDureeConso = _gf.createGraph(graphP);
			
			graphP.TYPE_GRAPH = "EVOLUTION";
			graphP.SEGMENT = _type.SEGMENT;
			
			
			//_gEvolution = _gf.createGraph(graphP);		
											
			//Affichage des elements de droite		
			myGraphSwitcher.x = 0;
			myGraphSwitcher.y = 300;							
			myGraphSwitcher.addChild(DisplayObject(_gRepConso));	
			myGraphSwitcher.addChild(DisplayObject(_gRepDureeConso));		
						
			TitleWindow(_gRepConso).addEventListener("switchGraphEvent",switchGraph);
			TitleWindow(_gRepDureeConso).addEventListener("switchGraphEvent",switchGraph);
			
			//Affichage des elements de gauche			
			DisplayObject(_gRepartition).x = 0;
			DisplayObject(_gRepartition).y = 300;
			myLeftContener.addChild(DisplayObject(_gRepartition));	
			//myLeftContener.addChild(DisplayObject(_gEvolution));
			
			//update(); 
		}
		
		//switch entre les deux graphs conso
		private function switchGraph(e : Event):void{
			 if (myGraphSwitcher.selectedChild == _gRepConso){
			 	myGraphSwitcher.selectedChild = mx.core.Container(_gRepDureeConso);
			 }else{
			 	myGraphSwitcher.selectedChild =mx.core.Container(_gRepConso);
			 };
		}
		
		
		//creation des éléments graphiques
		private function createTableauAccueil():void{
			try{
				myRightContener.removeChild(DisplayObject(_tbConso));
				myLeftContener.removeChild(DisplayObject(_tbAbo));	
			}catch(e : Error){
				trace("les conteneurs ne sont pas encore créés");
			}  
						
			_tf.setStrategy(new TableauAboFactoryStratey());
			_tbAbo = _tf.createTableau(_type);			
			UIComponent(_tbAbo).x = 0;
			UIComponent(_tbAbo).y = 0;
			
			_tf.setStrategy(new TableauConsoFactoryStrategy());
			_tbConso = _tf.createTableau(_type);			
			UIComponent(_tbConso).x = 0;
			UIComponent(_tbConso).y = 0;
			
			myLeftContener.addChildAt(DisplayObject(_tbAbo),0);				
			myRightContener.addChildAt(DisplayObject(_tbConso),0);					
			
			//affectation des Handlers et des listeners			
			UIComponent(_tbAbo).addEventListener("tableauChange",_goToNextPage);			
			UIComponent(_tbConso).addEventListener("tableauChange",_goToNextPage);
			
			UIComponent(_tbConso).addEventListener("totalSet",setTotalHandler);
		}
				
		public function goToNextPageHandler(f : Function):void{			
			_goToNextPage = f;				
		}	
		
		private function updateComposants():void{			
			preUpdateComposants();
			_tbAbo.update();			
		 	_tbConso.update();
		 	_gRepartition.update();
		 	_gRepConso.update();
		 	_gRepDureeConso.update();		 	
		 	//_gEvolution.update();
		}
		
		private function setTotalHandler(e:Event):void
		{
			defineTotal();
		}
		
		private function defineTotal():void
		{
			_total = parseFloat(_tbAbo.total.toString()) + parseFloat(_tbConso.total.toString());
			
			//trace("Abo ["+_tbAbo.total+"] + Conso["+_tbConso.total+"] = ["+ _total +"]");
			
			//Dispatcher SetTotal de Tableau de bord ici ...	
			var MyEv :  Event = new Event("tbTotal");
			dispatchEvent(MyEv);
		}
		
		private function preUpdateComposants():void{
			_tbAbo.updatePerimetre(perimetre,modeSelection);		  
		 	_tbConso.updatePerimetre(perimetre,modeSelection);	
		 	_gRepartition.updatePerimetre(perimetre,modeSelection);	
		 	_gRepConso.updatePerimetre(perimetre,modeSelection);	
		 	_gRepDureeConso.updatePerimetre(perimetre,modeSelection);			 	
		 	//_gEvolution.updatePerimetre(perimetre,modeSelection);	
		 	
		 	_tbAbo.updatePeriode(moisDeb,moisFin);	  
		 	_tbConso.updatePeriode(moisDeb,moisFin);	  
		 	_gRepartition.updatePeriode(moisDeb,moisFin);	  
		 	_gRepConso.updatePeriode(moisDeb,moisFin);	  
		 	_gRepDureeConso.updatePeriode(moisDeb,moisFin);	  		 	
		 	//_gEvolution.updatePeriode(moisDeb,moisFin);	
		}	
	}
}