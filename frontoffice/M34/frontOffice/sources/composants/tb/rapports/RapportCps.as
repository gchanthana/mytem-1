package composants.tb.rapports
{
	import mx.events.FlexEvent;
	import flash.events.MouseEvent;
	
	public class RapportCps extends RapportCps_IHM
	{
		public function RapportCps() 
		{
			//TODO: implement function
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		//initialisation
		private function init(fe : FlexEvent):void{
			myPdfLink.addEventListener(MouseEvent.CLICK,dispatchInfo);
			myExcelLink.addEventListener(MouseEvent.CLICK,dispatchInfo);
			myCsvLink.addEventListener(MouseEvent.CLICK,dispatchInfo);
			/*myFlashpaperLink.addEventListener(MouseEvent.CLICK,dispatchInfo);*/					
			enableExport();
		}
		
		//diffusion de l'evenement
		private function dispatchInfo(me : MouseEvent):void{
			var rapportEvent : RapportEvent = new RapportEvent("export");
			rapportEvent.format = me.currentTarget.toolTip;
			dispatchEvent(rapportEvent);
		}
		
		/**
		 *  Pour rendre inactif les boutons d'export
		 * */
		public function disableExport():void{
			myExcelLink.visible = false;
			myExcelLink.visible = false;
		/*myFlashpaperLink.enabled = false;*/
			myPdfLink.visible = false;
			myCsvLink.visible = false;
		}
		
		/**
		 *  Pour rendre actif les boutons d'export
		 * */
		public function enableExport():void{
			myExcelLink.visible = true;
			/*myFlashpaperLink.enabled = true;*/
			myPdfLink.visible = true;
			myCsvLink.visible = true;
		}
	}
}