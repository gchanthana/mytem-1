package composants.tb.tableaux
{
	import mx.utils.ObjectUtil;
	import mx.controls.Alert;
	
	/**
	 * A pour vocation de creer des objets de type Tableau
	 * @creator Samuel DIVIOKA pour la scociété CONSOTEL 
	 * 
	 **/
	public class TableauFactory
	{
		private var _myTableauFactoryStratgey : ITableauFactoryStrategy;
		/**
		 * Constructeur
		 **/
		public function TableauFactory(){
			//init
		}
				
		/**
		 * Affecte la strategy a la factory
		 **/
		public function setStrategy( myTableauFactoryStratgey : ITableauFactoryStrategy ):void{			
			_myTableauFactoryStratgey = myTableauFactoryStratgey;
		}		
		
		/**
		 * Retourne un Objet Tableu suivant le type passé en paramètre
		 * @param type Le type de page à créer		  
		 * @throw TableauError la page n'existe pas ou n'a pu êtres créée.
		 **/
		public function createTableau(ev : TableauChangeEvent):ITableauModel{
			try{			
				 	
				return _myTableauFactoryStratgey.createTableau(ev);	
						
			}catch(e:Error){						
				
				throw new TableauError();
			}		
			return null;					
		}		
	}
}