package composants.tb.tableaux.renderer
{
	import mx.controls.*;	
    import mx.core.*;
    import mx.containers.Canvas;
	
	public class CellField extends Canvas
    {
        // Define the constructor and set properties.
       private var libelle : Label;
       public function CellField() {
       		libelle = new Label(); 
       		libelle.percentWidth = 100;          
            setStyle("borderStyle", "none");           
        }

        // Override the set method for the data property.
        override public function set data(value:Object):void {
            super.data = value;
       		
       		if (value) {				
                super.data = value;			
				if (value.TYPE.toUpperCase() == "SEGMENT" ){
					
					setStyle("color", "#336699");
					setStyle("fontFamily", "verdana");
					setStyle("fontWeight", "bold");
					 
					libelle.text  =  "Segment " + value.SEGMENT;						
				}else{
					libelle.text  = value.LIBELLE;
				}
			}
            addChild(libelle);
           	super.invalidateDisplayList();
        }
    }
}
