package composants.tb.tableaux
{
	import mx.collections.ArrayCollection;
	
	public interface ITableauModel
	{
						
		/**
		 * Retourne le montant total
		 * */
		 function get total():Number;
				
		/**
		* Permet de passer les valeurs aux tableaux du composant.
		* <p>Pour chaque tableau de la collection, un DataGrid sera affiché à la suite du précédant</p>
		* @param d Une collection de tableau. 
		**/				
		function set dataProviders(d : ArrayCollection):void;
		
		/**
		*  Retourne le dataprovider
		**/				
		function get dataProviders():ArrayCollection;
				
		/**
		* Permet d'affecter une fonction pour traiter les Clicks sur Les DataGrids du composant
		* @param changeFunction La fonction qui traite les clicks.<p> Elle prend un paramètre de type Object qui représente une ligne du DataGrid</p>
		**/		
		function set changeHandlerFunction ( changeFunction : Function ):void;
		
		
		/**
		*  Retourne une reference vers la fonction qui traite le data provider
		**/			
		function get changeHandlerFunction():Function;
		
		/**
		 * Efface les données et interompt le remoting en cours
		 **/
		function clean():void;
		
		/**
		 * Met à jour la periode
		 * */		 
		function updatePeriode(moisDeb : String = null, moisFin : String = null):void;
		
		/**
		 * Met à jour le perimetre
		 * */
		function updatePerimetre(perimetre : String = null, mode : String = null):void;
		
		
		//----------------------------------- CHARGEMENT DES DONNEES --------------------------------------------------------//				
		/**
		 * Met à jour les donées du tableau
		 **/
		function update():void;
		
	}
}