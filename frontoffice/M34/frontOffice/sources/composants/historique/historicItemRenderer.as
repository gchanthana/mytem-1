package composants.historique
{
	import mx.controls.dataGridClasses.DataGridListData;
	import mx.controls.dataGridClasses.DataGridItemRenderer;
	import mx.controls.Label;
	import mx.containers.Canvas;
	import mx.controls.TextInput;
	import flash.display.Graphics;
	import mx.events.ToolTipEvent;
	import composants.util.DateFunction; 
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	import mx.controls.List;
	
	
	public class historicItemRenderer extends DataGridItemRenderer
	{
		public function historicItemRenderer():void
		{
			 setStyle("borderStyle", "none");   
			 super.background = true;
			this.addEventListener(ToolTipEvent.TOOL_TIP_SHOW, toolTipShown);
			this.addEventListener(ToolTipEvent.TOOL_TIP_SHOWN, toolTiphide);
		}
		
		public function toolTiphide(event:ToolTipEvent):void
		{
			var tmp:DataGridListData = super.listData as DataGridListData;
			if (tmp.columnIndex <= 2 || tmp.dataField == "N")
				event.toolTip.visible = false;	
		}
		
		public function toolTipShown(event:ToolTipEvent):void
		{
			var tmp:DataGridListData = super.listData as DataGridListData;
			if (tmp.columnIndex > 2)
			{
				var d:String = tmp.dataField.substr(2, tmp.dataField.length);
				var newDate:Date = new Date(d.substr(0, 4),d.substr(5, 2));

				var monthIndex:int = -1;
				if(newDate.month == 0)
					monthIndex = 11;
				else if(newDate.month > 0)
					monthIndex = newDate.month - 1;
				var str:String = "Mois de " + DateFunction.moisEnLettre(monthIndex) + " " + newDate.fullYear + "\n";				
				var provider:Object = (tmp.owner as HistoriqueDataGrid).dataProvider[tmp.rowIndex -1];
				var value:Number = provider[tmp.dataField];
				if (this.data.TYPEDATA == "A") {
					switch (value) {
						case 0:
							str += "Pas abonné";
							break;
						case 1:
							str += "Abonné mais ticket absent";
							break;
						case 2:
							str += "Tickets importés et calculés";
							break;
					}
				}
				else
					str += "Nombre de factures : " +  value;
				event.toolTip.text = str;
			}
		}
		
		override public function set data(value:Object):void {
            super.data = value;
        
	           if (value.dataField == undefined) {
		         	var tmp:DataGridListData = super.listData as DataGridListData;
				
					var color:uint = 0xFFFFFF;
		           	if (super.data.TYPEDATA == "A")
		           	{
		           		if (tmp.label == "2")
			           		color = 0x0099FF;
						else if (tmp.label == "1")
							color = 0xFF0000;
		           	}
		           	else
		           	{
		           		var max:Number = super.data.MAX_VALUE;
		           		var nb:Number = parseInt(tmp.label);
		           		if (!isNaN(nb))
		           		{
		           			if (nb == 0)
			           			color = 0xffffff;
		           			else if (nb > 0 && (max / 4) > nb)
			           			color = 0xdae41f;
			           		else if (nb >= (max / 4) && nb < (max / 2))
			           			color = 0x9abe19;
			           		else if (nb >= (max /2) && nb < (max  * 3 / 4))
			           			color = 0x539311;
			           		else
			           			color = 0x146d0b;
		           		}
		           	}  
		           	super.backgroundColor = color; 
		           if (tmp.columnIndex > 2) {
			           tmp.label = "";
  		           }
	           }
	          super.validateProperties();
	      
		 }
		 
	 	/* public function updateDisplayList(unscaledWidth:Number,unscaledHeight:Number):void {       
            super.updateDisplayList(unscaledWidth,unscaledHeight);
			//super.label.text =  "Le libellé";
     	} */
  	}
}
