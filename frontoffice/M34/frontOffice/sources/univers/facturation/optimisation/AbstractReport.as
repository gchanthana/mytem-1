package univers.facturation.optimisation {
	import composants.tb.periode.AMonth;
	import composants.tb.periode.PeriodeEvent;
	import composants.util.DateFunction;
	
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.containers.Box;
	import mx.controls.Menu;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;

	/**
	 * Cette classe est le type le plus commun de tous les rapports. Un rapport est
	 * est un composant visuel (Box) qui comprend :
	 * Un type : reportType
	 * Un lib�ll� : getReportName()
	 * Un s�l�cteur de p�riode qui a 1 ou 2 bornes (Mois) : periodSelectorItem et selectorThumbCount
	 * Un lib�ll� de la p�riode choisie : periodLabel
	 * Un ensemble de formats et d'actions effectuables : formatList
	 * Un menu qui affiche la liste des formats et actions : actionMenu
	 * Une r�f�rence sur l'action actuelle : currentAction
	 * Un objet permettant de lancer l'action actuelle : actionItem
	 * Une des variables de requ�te POST et une URL d'export du rapport : urlVariables, urlRequestPath
	 * La version actuelle de l'impl�mentation ne permet de faire que des export de formats
	 * */
	public class AbstractReport extends Box implements IRapport {
		public static const XLS_FORMAT_LABEL:String = "Exporter en XLS";
		public static const XLS_FORMAT_VALUE:String = "excel";
		public static const PDF_FORMAT_LABEL:String = "Exporter en PDF";
		public static const PDF_FORMAT_VALUE:String = "pdf";
		public static const CSV_FORMAT_LABEL:String = "Exporter en CSV";
		public static const CSV_FORMAT_VALUE:String = "csv";
				
		public var periodSelectorItem:Object; // S�l�cteur de p�riode - IHM
		public var selectorThumbCount:int = 1; // Pour acc�s au period selector : 1 mois ou 2 mois � choisir
		public var periodLabel:Object; // Label de la p�riode choisie (IHM)
		
		public var actionItem:Object; // Actions du rapports (Afficher) - IHM
		protected var formatList:Array; // Tableau contenant les formats/actions support�s (Excel,PDF,...)
		protected var actionMenu:Menu; // Menu des actions (Exporter en Excel, etc...)
		protected var currentAction:Object; // Action actuelle
		
		protected var reportType:String = "AbstractReport"; // TYPE (Strategy Pattern) du rapport (Ex: detaildata,etc...)
		protected var urlRequestPath:URLRequest; // URL du rapport
		protected var urlVariables:URLVariables; // Variables pass�es � l'URL du rapport
		
		/**
		 * L'objet requ�te HTTP (URLRequest) ainsi que les variables POST (URLVariables) sont cr��es
		 * dans le constructeur.
		 * Chaque rapport doit sp�cifier son type dans le constructeur car le type par d�faut
		 * est AbstractReport.
		 * Chaque rapport doit initialiser la valeur de selectorThumbCount. Par d�faut il vaut 1
		 * Chaque rapport met � jour ses param�trages suppl�mentaires
		 * La liste des formats et des menus doivent �tre cr��s par le rapport en question
		 * car il varie selon le type de la classe du rapport.
		 * */
		public function AbstractReport() {
			super();
			urlVariables = new URLVariables();
			urlRequestPath = new URLRequest();
			actionMenu = new Menu();
		}
		
		/**
		 * Lib�ll� du rapport
		 * */
		public function getReportName():String {
			return "AbstractReport - Mod�le Abstrait de rapport";
		}
		
		/**
		 * M�thode appel�e lorsque le p�rim�tre a chang�
		 * L'impl�mentation par d�faut n'effectue que les actions de mises � jour suivantes :
		 * Mise � jour des param�tres de p�riode par la m�thode processPeriodInit()
		 * */
		public function onPerimetreChange():void {
			trace("AbstractReport onPerimetreChange() : Impl�mentation par d�faut - " + reportType);
			if(this.initialized) {
				periodSelectorItem.onPerimetreChange();
				processPeriodInit();
			}
		}
		
		/**
		 * Met � jour l'�tat de l'objet qui permet de lancer l'action actuelle
		 * Il retourne cet �tat apr�s mis � jour
		 * L'impl�mentation par d�faut (Cette classe) renvoie toujours true
		 * Il appartient � chaque rapport d'impl�menter son comportement ainsi
		 * que de choisir les points d'ex�cution o� cette m�thode sera appel�e
		 * Dans l'impl�mentation par d�faut, il est appel� � la fin de initIHM()
		 * */
		protected function checkAndUpdateActionItemStatus():Boolean {
			return true;
		}
		
		/**
		 * Remplit la liste des formats et des menus
		 * Chaque rapport initialise sa liste de formats et d'action dans son constructeur
		 * */
		protected function populateActions():void {
			actionMenu.dataProvider = null;
			actionMenu.dataProvider = formatList;
			actionItem.popUp = actionMenu;
			actionMenu.selectedItem = formatList[0];
			actionItem.label = actionMenu.selectedItem.label;
			currentAction = actionMenu.selectedItem;
		}
		
		/**
		 * Handler appel� lorsqu'on choisi un format ou une action
		 * parmi la liste disponible
		 * */
		protected function onSelectAction(event:MenuEvent):void {
			actionItem.label = actionMenu.selectedItem.label;
			currentAction = event.item;
		}

		/**
		 * Initialise le s�l�cteur de p�riode et le lib�ll� de la p�riode choisie.
		 * Les valeurs correspondantes dans les variables d'export sont alors mises � jour
		 * */
		protected function processPeriodInit():void {
			var periodeArray:Array = periodSelectorItem.getTabPeriode();
			var month:AMonth = periodeArray[periodeArray.length - 2]; // Avant dernier dans le tableau
			var monthString:String = month.getDateDebut().substring(3,5);
			var monthIndex:int = parseInt(monthString,10);
			var yearString:String = month.getDateDebut().substring(6,month.getDateDebut().length);
			urlVariables.dateDeb = yearString + "/" + monthString; // Format YYYY/DD
			if(selectorThumbCount == 1) {
				periodLabel.text = "Période : " + DateFunction.MONTH_NAMES[monthIndex - 1]  + " " + yearString;
			} else if(selectorThumbCount == 2) {
				var monthStringFin:String = month.getDateFin().substring(3,5);
				var monthIndexFin:int = parseInt(monthStringFin,10);
				var yearStringFin:String = month.getDateFin().substring(6,month.getDateFin().length);
				urlVariables.dateFin = yearStringFin + "/" + monthStringFin; // Format YYYY/DD
				periodLabel.text = "Période : " + DateFunction.MONTH_NAMES[monthIndex - 1]  + " " + yearString +
				" au " + DateFunction.MONTH_NAMES[monthIndexFin - 1]  + " " + yearStringFin;
			} else
				throw new IllegalOperationError("Nombre de s�l�cteurs de p�riode invalide");
		}

		/**
		 * Cette m�thode est appel�e (Handler) lorsque la ou les bornes du s�l�cteur
		 * de p�riode ont chang�s. Dans ce cas elle met � jour le lib�ll� de la p�riode actuelle
		 * ainsi que les valeurs correspondantes dans les variables d'export
		 * */
		protected function onPeriodSelectorChange(event:PeriodeEvent):void {
			var tmpMoisDeb:String = event.moisDeb;
			var monthString:String = tmpMoisDeb.substring(3,5);
			var monthIndex:int = parseInt(monthString,10);
			var yearString:String = tmpMoisDeb.substring(6,tmpMoisDeb.length);
			urlVariables.dateDeb = yearString + "/" + monthString; // Format YYYY/DD
			if(selectorThumbCount == 1) {
				periodLabel.text = "Période : " + DateFunction.MONTH_NAMES[monthIndex - 1]  + " " + yearString;
			} else if(selectorThumbCount == 2) {
				var tmpMoisFin:String = event.moisFin;
				var monthStringFin:String = tmpMoisFin.substring(3,5);
				var monthIndexFin:int = parseInt(monthStringFin,10);
				var yearStringFin:String = tmpMoisFin.substring(6,tmpMoisFin.length);
				urlVariables.dateFin = yearStringFin + "/" + monthStringFin; // Format YYYY/DD
				periodLabel.text = "Période : " + DateFunction.MONTH_NAMES[monthIndex - 1]  + " " + yearString +
						" au " + DateFunction.MONTH_NAMES[monthIndexFin - 1]  + " " + yearStringFin;
			} else
				throw new IllegalOperationError("Nombre de s�l�cteurs de p�riode invalide");
		}

		/**
		 * Retourne la cha�ne URL qui sera utilis�e pour l'export du rapport
		 * L'impl�mentation par d�faut utilise l'URL commune :
		 * /fr/consotel/consoview/cfm/report/optimisation/optimisation.cfm
		 * pr�fix�e par une constante qui se trouve dans la classe ConsoViewModuleObject
		 * Ce pr�fixe est d�termin� par la valeur du param�tre secureBool
		 * Si secureBool vaut false alors le pr�fixe est : ConsoViewModuleObject.NonSecureUrlBackoffice
		 * sinon c'est : ConsoViewModuleObject.urlBackoffice
		 * Cette m�thode est appel�e par getReportUrlRequest()
		 * */
		protected function getReportUrlString(secureBool:Boolean = true):String {
			trace("AbstractReport getReportUrlString() : URL par d�faut");
			if(secureBool == false)
				return moduleOptimisationIHM.NonSecureUrlBackoffice +
							"/fr/consotel/consoview/cfm/report/optimisation/optimisation.cfm";
			else
				return moduleOptimisationIHM.urlBackoffice +
							"/fr/consotel/consoview/cfm/report/optimisation/optimisation.cfm";
		}

		/**
		 * Retourne la requ�te HTTP (POST) qui sera utilis�e pour l'export du rapport.
		 * L'impl�mentation par d�faut met � jour les param�tres commun � l'export, ensuite chaque
		 * rapport sp�cifique doit effectuer ces mis � jour suppl�mentaires si besoin
		 * Cette m�thode fait appel � getReportUrlString() pour obtenir l'URL (Cha�ne)
		 * Les param�tres communs tel que l'identifiant du p�rim�tre, le type de rapport, le format, etc...
		 * sont mis � jour dans cette m�thode.
		 * Ces param�tres sont :
		 * LIBELLE_CLIENT : Libell� du groupe racine
		 * PERIMETRE_INDEX : Identifiant du p�rim�tre sur lequel on est connect�
		 * RAISON_SOCIALE : Lib�ll� du p�rim�tre sur lequel on est connect�
		 * TYPE_PERIMETRE : Type p�rim�tre du p�rim�tre sur lequel on est connect�
		 * FORMAT : Le format d'export
		 * REPORT_TYPE : Le type du rapport
		 * 
		 * L'impl�mentation par d�faut affecte comme valeur du param�tre TYPE_PERIMETRE celui du
		 * p�rim�tre sur lequel on est connect�.
		 * */
		protected function getReportUrlRequest(secureBool:Boolean = true):URLRequest {
			trace("Impl�mentation par d�faut dans AbstractReport : Cette m�thode doit �tre ref�finie par la sous classe");
			urlRequestPath.url = getReportUrlString(secureBool);
			urlVariables.LIBELLE_CLIENT = CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE;
			urlVariables.PERIMETRE_INDEX = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			urlVariables.RAISON_SOCIALE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
			urlVariables.TYPE_PERIMETRE = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			urlVariables.FORMAT = currentAction.value;
			urlVariables.REPORT_TYPE = reportType;
			urlRequestPath.data = urlVariables;
			return urlRequestPath;
		}

		/**
		 * Effectue l'action actuelle. Si l'action actuelle est un format alors
		 * il s'agira d'un export. Cette m�thode fait appel � getReportUrlRequest pour
		 * obtenir l'objet requ�te HTTP utilis� en cas d'export
		 * La m�thode utilis�e en cas d'export est toujours POST
		 * La version actuelle de l'impl�mentation ne permet de faire que des export de formats
		 * */
		protected function performAction(event:Event):void {
			try {
				var request:URLRequest;
				if(currentAction.value == AbstractReport.PDF_FORMAT_VALUE)
					request = getReportUrlRequest(false);
				else
					request = getReportUrlRequest(true);
				urlRequestPath.method = URLRequestMethod.POST;
				navigateToURL(request,"_blank");
			} catch (error:ArgumentError) {
				throw new Error(error.toString());
			} catch (error:SecurityError) {
				throw new Error(error.toString());
			}
		}
		
		/**
		 * Cette m�thode est appel�e (Handler) lorsque le composant rapport
		 * est affich� et compl�tement cr�� (FlexEvent.CREATION_COMPLETE). Il faut que le constructeur du
		 * rapport enregistre lui m�me ce listener
		 * Le s�l�cteur de p�riode et les valeurs des param�tres correspondants sont alors mis
		 * � jour par appel de processPeriodInit()
		 * L'impl�mentation par d�faut affecte alors les listener aux :
		 * Changement de p�riode
		 * Changement d'action (Export d'un format ou action diff�rente)
		 * D�clenchement d'une action (Export d'un format ou action diff�rente)
		 * */
		protected function initIHM(event:FlexEvent):void {
			processPeriodInit();
			populateActions();
			periodSelectorItem.addEventListener("periodeChange",onPeriodSelectorChange);
			actionMenu.addEventListener(MenuEvent.ITEM_CLICK,onSelectAction);
			actionItem.addEventListener(MouseEvent.CLICK,performAction);
			checkAndUpdateActionItemStatus();
		}
	}
}
