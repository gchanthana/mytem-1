package univers.facturation.optimisation.deploiement {
	import mx.events.FlexEvent;
	import flash.events.Event;
	import univers.facturation.optimisation.IRapport;
	import univers.facturation.optimisation.Main;
	import mx.controls.Alert;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	import composants.tb.periode.PeriodeEvent;
	import composants.tb.periode.AMonth;
	import composants.util.DateFunction;
	import univers.facturation.optimisation.AbstractReport;
	import mx.controls.Menu;
	import mx.events.MenuEvent;
	import flash.events.MouseEvent;
	import flash.utils.getQualifiedClassName;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	
	public class Deploiement extends DeploiementIHM {
		public static const PERIODICITE_MENSUEL:int = 0;
		public static const PERIODICITE_MENSUEL_LABEL:String = "Mensuel";
		public static const PERIODICITE_BIMESTRIEL:int = 1;
		public static const PERIODICITE_BIMESTRIEL_LABEL:String = "Bimestriel";
		private var periodiciteArray:ArrayCollection =
				new ArrayCollection([{label: Deploiement.PERIODICITE_MENSUEL_LABEL, value: Deploiement.PERIODICITE_MENSUEL},
							{label: Deploiement.PERIODICITE_BIMESTRIEL_LABEL, value: Deploiement.PERIODICITE_BIMESTRIEL}]);
		
		public function Deploiement() {
			super();
			reportType = "Deploiement";
			selectorThumbCount = 1; // Un seul mois à choisir
			formatList = new Array(3);
			var xlsObj:Object = new Object();
			var pdfObj:Object = new Object();
			var csvObj:Object = new Object();
			xlsObj.label = AbstractReport.XLS_FORMAT_LABEL;
			xlsObj.value = AbstractReport.XLS_FORMAT_VALUE;
			pdfObj.label = AbstractReport.PDF_FORMAT_LABEL;
			pdfObj.value = AbstractReport.PDF_FORMAT_VALUE;
			csvObj.label = AbstractReport.CSV_FORMAT_LABEL;
			csvObj.value = AbstractReport.CSV_FORMAT_VALUE;
			xlsObj.enabled = pdfObj.enabled = csvObj.enabled = true;
			formatList[0] = xlsObj;
			formatList[1] = pdfObj;
			formatList[2] = csvObj;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}

		public override function getReportName():String {
			return "Rapport de Déploiement";
		}
		
		protected override function initIHM(event:FlexEvent):void {
			trace("Deploiement initIHM");
			super.initIHM(event);
			cmbPeriodicite.dataProvider = periodiciteArray;
		}

		protected override function getReportUrlRequest(secureBool:Boolean = true):URLRequest {
			super.getReportUrlRequest(secureBool);
			urlRequestPath.data.PERIODICITE = cmbPeriodicite.selectedItem.value;
			urlRequestPath.data.CONSO_FT_MIN = parseInt(txtConsoFtMin.text);
			if(isNaN(urlRequestPath.data.CONSO_FT_MIN)) {
				urlRequestPath.data.CONSO_FT_MIN = 0;
				txtConsoFtMin.text = "0";
			}
			return urlRequestPath;
		}
	}
}
