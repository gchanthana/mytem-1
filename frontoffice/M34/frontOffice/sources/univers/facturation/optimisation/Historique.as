package univers.facturation.optimisation {
	import mx.events.FlexEvent;
	import flash.events.Event;
	import composants.historique.HistoriqueDataGrid;
	import composants.util.DateFunction;
	import flash.events.MouseEvent;
	
	public class Historique extends HistoriqueIHM {
		
		public function Historique() {
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		protected function initIHM(event:Event):void {
			histoDataGrid.addEventListener(HistoriqueDataGrid.HISTO_DATA_GRID_READY,onHistoDataGridReady);
			// Gestion du bouton de rafraichissement des données
			btnRefresh.addEventListener(MouseEvent.CLICK,chargerDonnees);
			
		}
		
		/**
		 * Méthode appelée lorsque on clique sur le bouton de refraichissement des données
		 * */
		private function chargerDonnees(event:MouseEvent):void {
			var dateDeb:Date = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateDeb;
			var dateFin:Date = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateFin;
			histoDataGrid.loadHistoData(dateDeb,dateFin);
			btnRefresh.enabled=false;
		}
		
		/**
		 * Méthode appelée lorsque le périmètre a changée et que CvAccessManager a été mis à jour.
		 * */
		public function onPerimetreChange():void {
			var dateDeb:Date = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateDeb;
			var dateFin:Date = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateFin;
			histoDataGrid.itemRenderer = histoDataGrid.defaultRenderer;
			histoDataGrid.dataProvider=null;
			histoDataGrid.visible=false;
			btnRefresh.enabled=false;
		}
		
		private function onHistoDataGridReady(event:Event):void {
			var dateDebMonthIndex:int = histoDataGrid.DATE_DEB.month;
			var dateFinMonthIndex:int = histoDataGrid.DATE_FIN.month;
			
			histoPanel.title = "Historique de : " + DateFunction.MONTH_NAMES[dateDebMonthIndex]  + " " +
			histoDataGrid.DATE_DEB.fullYear + " à " +
			DateFunction.MONTH_NAMES[dateFinMonthIndex]  + " " + histoDataGrid.DATE_FIN.fullYear;
		}
	}
}
