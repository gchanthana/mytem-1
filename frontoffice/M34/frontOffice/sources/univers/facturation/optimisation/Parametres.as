package univers.facturation.optimisation {
	import mx.events.FlexEvent;
	import mx.core.Container;
	import mx.utils.ObjectUtil;
	import flash.utils.getQualifiedClassName;
	import flash.errors.IllegalOperationError;

	public class Parametres extends ParametresIHM {
		public var reportList:Array = null; // Liste des Rapports (label,reportId)
		
		public function Parametres() {
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:FlexEvent):void {
			var tmpChildren:Array = reportContainer.getChildren();
			reportList = new Array(tmpChildren.length);
			var i:int;
			for(i = 0; i < reportList.length; i++) {
				reportList[i] = new Object();
				reportList[i].id = i;
				if(tmpChildren[i] is IRapport)
					reportList[i].label = (tmpChildren[i] as IRapport).getReportName();
				else
					throw new IllegalOperationError((tmpChildren[i] as Container).label +
									" n'hérite pas de AbstractReport ou n'implémente pas IRapport");
			}
			reportContainer.selectedIndex = 0;
		}
		
		public function getReportList():Array {
			return reportList;
		}
		
		public function selectReport(reportId:int):void {
			reportContainer.selectedIndex = reportId;
		}
		
		public function onPerimetreChange():void {
			var tmpChildren:Array = reportContainer.getChildren();
			var i:int;
			for(i = 0; i < tmpChildren.length; i++)
				(tmpChildren[i] as IRapport).onPerimetreChange();
		}
	}
}
