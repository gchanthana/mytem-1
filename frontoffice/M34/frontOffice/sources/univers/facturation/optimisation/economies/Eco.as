package univers.facturation.optimisation.economies {
	import univers.facturation.optimisation.IRapport;
	import mx.events.FlexEvent;
	import univers.facturation.optimisation.Main;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	
	public class Eco extends EcoIHM implements IRapport {
		private static var formatList:Array = new Array(2);
		private static const CFM_REPORT_URL_REQUEST:URLRequest = new URLRequest(ConsoViewModuleObject.urlBackoffice +
					"/fr/consotel/consoview/cfm/report/optimisation/eco.cfm");
		private static const CFM_REPORT_URL_VARIABLES:URLVariables = new URLVariables();
		
		public function Eco() {
			var xlsObj:Object = new Object();
			var pdfObj:Object = new Object();
			xlsObj.label = Main.XLS_FORMAT_LABEL;
			xlsObj.value = Main.XLS_FORMAT_VALUE;
			pdfObj.label = Main.PDF_FORMAT_LABEL;
			pdfObj.value = Main.PDF_FORMAT_VALUE;
			xlsObj.enabled = pdfObj.enabled = true;
			formatList[0] = xlsObj;
			formatList[1] = pdfObj;
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		public function getReportName():String {
			return "Economies réalisées";
		}
		
		protected function initIHM(event:Event):void {
			/*
			reportNote.text = "Afin de déterminer l'économie réalisée sur les consommations filaires, " +
					"ConsoTel calcule ce qu’aurait payé le CLIENT en utilisant les offres " +
					"initiales en tenant compte des évolutions de tarifs." +
					"\n\nPour une période donnée, vous aurez autant de tableau que d'offres " +
					"ou de version de tarifs d'une offre auxquelles votre organisation " +
					"a été abonnée au cours de cette période." +
					"\n\nSi le client était abonné auprès de plusieurs opérateurs, " +
					"et que ConsoTel a récupéré un historique des consommations, " +
					"alors ConsoTel reprend la part relative dans chaque destination de chaque offre initiale.";
			*/
		}
		
		public function getFormatList():Object {
			return Eco.formatList;
		}
		
		public function getReportUrlRequest():URLRequest {
			Eco.CFM_REPORT_URL_REQUEST.data = null;
			Eco.CFM_REPORT_URL_REQUEST.data = Eco.CFM_REPORT_URL_VARIABLES;
			return Eco.CFM_REPORT_URL_REQUEST;
		}
	}
}
