package univers.facturation.optimisation.inventaireLignesGlobal {
	import univers.facturation.optimisation.AbstractReport;
	import mx.events.FlexEvent;
	import flash.net.URLRequest;
	import mx.collections.ArrayCollection;
	import univers.facturation.optimisation.deploiement.Deploiement;
	
	public class InventaireLignesGlobal extends InventaireLignesGlobalIHM {
		private var periodiciteArray:ArrayCollection =
				new ArrayCollection([{label: Deploiement.PERIODICITE_MENSUEL_LABEL, value: Deploiement.PERIODICITE_MENSUEL},
							{label: Deploiement.PERIODICITE_BIMESTRIEL_LABEL, value: Deploiement.PERIODICITE_BIMESTRIEL}]);
		
		public function InventaireLignesGlobal() {
			super();
			reportType = "InventaireLignesGlobal";
			selectorThumbCount = 1; // 1 Mois à choisir
			formatList = new Array(1);
			var xlsObj:Object = new Object();
			xlsObj.label = AbstractReport.XLS_FORMAT_LABEL;
			xlsObj.value = AbstractReport.XLS_FORMAT_VALUE;
			xlsObj.enabled = true;
			formatList[0] = xlsObj;
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		protected override function initIHM(event:FlexEvent):void {
			super.initIHM(event);
			cmbPeriodicite.dataProvider = periodiciteArray;
		}
		
		public override function getReportName():String {
			return "Inventaire des lignes";
		}
		
		protected override function getReportUrlRequest(secureBool:Boolean = true):URLRequest {
			super.getReportUrlRequest(secureBool);
			urlRequestPath.data.PERIODICITE = cmbPeriodicite.selectedItem.value;
			return urlRequestPath;
		}
	}
}
