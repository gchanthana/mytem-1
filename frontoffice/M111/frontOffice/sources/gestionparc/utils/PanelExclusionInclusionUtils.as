package gestionparc.utils
{
	import flash.events.Event;

	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.pool.GestionPool;
	import gestionparc.ihm.fiches.pool.PanelConfirmationInclusionPoolImpl;
	import gestionparc.ihm.fiches.pool.PanelExclusionPoolImpl;

	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;

	public class PanelExclusionInclusionUtils
	{

		/**************************************
		 *			  CONSTANTES
		 **************************************/

		public static const OPERATION_POOL_DONE:String="operationPoolDone";
		public static const OPERATION_POOL_CANCELLED:String="operationPoolCancelled";

		/**************************************
		 *			VARIABLES PRIVEES
		 **************************************/

		private var _myPanelExclusion:PanelExclusionPoolImpl;
		private var _myPanelInclusion:PanelConfirmationInclusionPoolImpl;
		private var myXML:String="";
		public var gestionPoolData:GestionPool=new GestionPool();
		private var whichPanel:String="";

		public function PanelExclusionInclusionUtils(instanceMyPanelExclusion:PanelExclusionPoolImpl=null, instanceMyPanelInclusion:PanelConfirmationInclusionPoolImpl=null)
		{
			myPanelExclusion=instanceMyPanelExclusion;
			myPanelInclusion=instanceMyPanelInclusion;

			if (myPanelExclusion != null)
			{
				whichPanel="exclusion";
			}
			else
			{
				whichPanel="inclusion";
			}
		}

		/**************************************
		 *			 GETTER / SETTER
		 **************************************/

		public function set myPanelExclusion(value:PanelExclusionPoolImpl):void
		{
			_myPanelExclusion=value;
		}

		public function get myPanelExclusion():PanelExclusionPoolImpl
		{
			return _myPanelExclusion;
		}

		public function set myPanelInclusion(value:PanelConfirmationInclusionPoolImpl):void
		{
			_myPanelInclusion=value;
		}

		public function get myPanelInclusion():PanelConfirmationInclusionPoolImpl
		{
			return _myPanelInclusion;
		}

		/**
		 *	Remplissage du datagrid des éléments concernés par l'exclusion et/ou transfert/inclusion
		 **/
		public function set dataDatagrid(value:ArrayCollection):void
		{
			if (whichPanel == "inclusion")
			{
				myPanelInclusion.dg_inclusion_elements.dataProvider=value;
			}
			else
			{
				myPanelExclusion.dg_exclusion_elements.dataProvider=value;
			}
		}

		public function get dataDatagrid():ArrayCollection
		{
			if (whichPanel == "inclusion")
			{
				return myPanelInclusion.dg_inclusion_elements.dataProvider as ArrayCollection;
			}
			else
			{
				return myPanelExclusion.dg_exclusion_elements.dataProvider as ArrayCollection;
			}
		}

		/**************************************
		 ***********	 EXCLUSION   **********
		 **************************************/

		/**
		 *	Affichage de la liste des éléments à exclure du pool V2
		 **/
		public function getElementForExclusion(myArrayCollection:ArrayCollection):void
		{
			myXML="<elements>";

			for each (var tmp:Object in myArrayCollection)
			{
				myXML+="<element><id>" + tmp.id + "</id>" + "<code>" + tmp.code + "</code></element>";
			}

			myXML+="</elements>";

			gestionPoolData.getListWithElements(myXML, SpecificationVO.getInstance().idPool, 3);
			gestionPoolData.addEventListener(GestionPool.LISTE_LOADED, listeElementHandler);
		}

		/**
		 *	Exclusion des éléments du pool V2
		 **/
		public function removeElementsFromPool():void
		{
			gestionPoolData.removeListeFromPool(SpecificationVO.getInstance().idPool, myXML);
			gestionPoolData.addEventListener(GestionPool.LISTE_EXCLUDED, gestionPoolHandler);
		}

		/**
		 *	Exclusion des éléments du pool V2
		 **/
		public function removeElementsFromPoolV2(xml:String):void
		{
			gestionPoolData.removeListeFromPool(SpecificationVO.getInstance().idPool, xml);
			gestionPoolData.addEventListener(GestionPool.LISTE_EXCLUDED, gestionPoolHandler);
		}

		/**
		 *	Transfert des éléments entre deux pool V2
		 **/
		public function moveElementsFromPool(idPoolDest:Number):void
		{
			gestionPoolData.moveListeFromPool(SpecificationVO.getInstance().idPool, idPoolDest, myXML);
			gestionPoolData.addEventListener(GestionPool.LISTE_EXCLUDED, gestionPoolHandler);
		}

		/**
		 *	Transfert des éléments entre deux pool V2
		 **/
		public function moveElementsFromPoolV2(idPoolDest:Number, xml:String):void
		{
			gestionPoolData.moveListeFromPool(SpecificationVO.getInstance().idPool, idPoolDest, xml);
			gestionPoolData.addEventListener(GestionPool.LISTE_EXCLUDED, gestionPoolHandler);
		}

		/**************************************
		 ***********	 INCLUSION   **********
		 **************************************/

		public function getElementForInclusion(tab:Array, type:String, etat:int):void
		{
			myXML="<elements>";
			var longueur:int=tab.length;
			for (var i:int=0; i < longueur; i++)
			{
				myXML+="<element><id>" + tab[i] + "</id>" + "<code>" + type + "</code></element>";
			}

			myXML+="</elements>";

			gestionPoolData.getListWithElements(myXML, SpecificationVO.getInstance().idPool, etat);
			gestionPoolData.addEventListener(GestionPool.LISTE_LOADED, listeElementHandler);
		}

		/**************************************
		 ***********	 COMMUN   ************
		 **************************************/

		/**
		 *	Chargement OK de la liste des éléments à exclure/inclure du pool
		 **/
		private function listeElementHandler(evt:Event):void
		{
			dataDatagrid=new ArrayCollection();
			dataDatagrid=gestionPoolData.liste;

			//nb total d'éléments dans le datagrid
			if (whichPanel == "inclusion")
			{
				if (myPanelInclusion.nbElementPreSelec == dataDatagrid.length)
				{
					myPanelInclusion.lb_nb_element.text=dataDatagrid.length + ResourceManager.getInstance().getString('M111', '__l_ment_s_');
					myPanelInclusion.infoEltSup.visible=false;
				}
				else
				{
					myPanelInclusion.lb_nb_element.text=myPanelInclusion.nbElementPreSelec + " + " + (dataDatagrid.length - myPanelInclusion.nbElementPreSelec) + ResourceManager.getInstance().getString('M111', '___l_ment_s_');
					myPanelInclusion.infoEltSup.visible=true;
				}
			}
			else
			{
				myPanelExclusion.lb_nb_element.text=dataDatagrid.length + ResourceManager.getInstance().getString('M111', '__l_ment_s_');
			}
		}

		/**
		 *	Inclusion des éléments dans un pool V2
		 **/
		public function addElementsToPool(idpool:Number):void
		{
			gestionPoolData.addListeToPool(idpool, myXML);
//			gestionPoolData.addEventListener(GestionPool.LISTE_INCLUDED, gestionPoolHandler);
		}

		/**
		 *	Inclusion des éléments dans un pool V3
		 **/
		public function addElementsToPoolV2(idpool:Number, myxml:String):void
		{
			gestionPoolData.addListeToPool(idpool, myxml);
		}

		/**
		 *	Gestion des événements inclusion / exclusion du pool
		 **/
		private function gestionPoolHandler(evt:Event):void
		{
			if (whichPanel == "exclusion")
			{
				myPanelExclusion.closePanel(null);
				myPanelExclusion.dispatchEvent(new Event(OPERATION_POOL_DONE));
			}
			else
			{
				myPanelInclusion.closePanel(null);
				myPanelInclusion.dispatchEvent(new Event(OPERATION_POOL_DONE, true));
			}
		}

	}
}
