package gestionparc.utils
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.popup.ConfirmWithPassWordIHM;
	
	import mx.managers.PopUpManager;

	public class ConfirmWithPassBoxManager
	{
		private var _popupConfirmWithPassWord:ConfirmWithPassWordIHM;

		public function ConfirmWithPassBoxManager()
		{
		}

		public function displayConfirmBox(titre:String, handler:Function, parentDisplayObject:DisplayObject):void
		{
			if (_popupConfirmWithPassWord == null)
			{
				_popupConfirmWithPassWord=new ConfirmWithPassWordIHM();

			}
			if (!_popupConfirmWithPassWord.isPopUp)
			{
				_popupConfirmWithPassWord.init(titre, handler);
				PopUpManager.addPopUp(_popupConfirmWithPassWord, parentDisplayObject, true);
				PopUpManager.centerPopUp(_popupConfirmWithPassWord);
			}
		}

		public function hideConfirmBox():void
		{
			if (_popupConfirmWithPassWord != null && _popupConfirmWithPassWord.isPopUp)
			{
				PopUpManager.removePopUp(_popupConfirmWithPassWord);
			}
		}
	}
}
