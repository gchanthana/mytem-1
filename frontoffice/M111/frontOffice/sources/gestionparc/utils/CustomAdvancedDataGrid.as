package gestionparc.utils
{
	import flash.display.Sprite;
	
	import mx.controls.AdvancedDataGrid;


	[Bindable]
	public class CustomAdvancedDataGrid extends AdvancedDataGrid
	{

		public var categorieBackGroundColor:uint;

		protected override function drawRowBackground(s:Sprite, rowIndex:int, y:Number, height:Number, color:uint, dataIndex:int):void
		{
			var itemData:Object=rowNumberToData(dataIndex);

			if (itemData != null)
			{
				if (itemData.children)
				{
					color=categorieBackGroundColor;
				}
			}

			super.drawRowBackground(s, rowIndex, y, height, color, dataIndex);
		}
	}

}
