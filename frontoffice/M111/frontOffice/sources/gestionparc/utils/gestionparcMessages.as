package gestionparc.utils
{
	import mx.resources.ResourceManager;
	
	public class gestionparcMessages
	{
		public static const CONSOVIEW:String=ResourceManager.getInstance().getString('M111', 'Consoview');
		public static const ERREUR:String=ResourceManager.getInstance().getString('M111', 'Erreur');
		public static const RENSEIGNER_CHAMPS_OBLI:String=ResourceManager.getInstance().getString('M111', 'Veuillez_renseigner_les_champs_obligatoi');
		public static const SYNTAXE_INVALIDE:String=ResourceManager.getInstance().getString('M111', 'syntaxe_invalide');
		
		public static const NB_CHAR_POUR_LIGNE:String=ResourceManager.getInstance().getString('M111', 'Le_n__de_ligne_doit_contenir_10_chiffres');
		
		public static const RECUP_PROFILS_EQPT_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_des_profils_des__quipeme');
		public static const RECUP_ABO_OPT_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_des_abonnements_et_des_o');
		
		public static const RECUP_FICHE_HISTO_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_fiche_histo_a__chou___');
		
		public static const RECUP_FICHE_COLLAB_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_fiche_collab_impossible');
		public static const MATRICULE_EXISTE:String=ResourceManager.getInstance().getString('M111', '_Ce_matricule_est_d_ja_utilis__sur_la_fi');
		public static const UPDATE_FICHE_COLLAB_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_mise___jour_de_la_fiche_collab_impossible');
		public static const ADD_NEW_COLLAB_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'L_ajout_du_nouveau_collaborateur_a__chou___');
		public static const ADD_NEW_COLLAB_IMPOSSIBLE_CAUSE_5:String=ResourceManager.getInstance().getString('M111', 'L_ajout_du_nouveau_collaborateur_a__chou___') + '\n' + ResourceManager.getInstance().getString('M111', 'ADD_NEW_COLLAB_IMPOSSIBLE_CAUSE_5');
		public static const RECUP_INFOS_LIGNE_SIM_TERM_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_des_liaisons_Ligne_Sim_Terminal_pour_ce_collaborateur_a__chou___');
		public static const RECUP_INFOS_EQPT_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_des__quipements_associ_s___ce_collaborateur_a__chou___');
		public static const RECUP_MATRICULE_AUTO:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_du_matricule_automatique_impossible');
		
		public static const RECUP_FICHE_TERM_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_fiche_terminal_a__chou__');
		public static const UPDATE_FICHE_TERM_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_mise___jour_de_la_fiche_terminal_a__chou___');
		public static const NUMERO_IMEI_EXISTANT:String=ResourceManager.getInstance().getString('M111', 'Le_num_ro_IMEI_que_vous_avez_choisi_est_d_j__utilis__');
		public static const RECUP_INFOS_LIGNE_SIM_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_des_liaisons_Ligne_Sim_pour_ce_terminal_a__chou___');
		public static const RECUP_SS_EQPT_DISPO_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_sous__quipement_dispo_a__chou__');
		
		public static const SET_INFOS_CMD_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'L_enregistrement_des_informations_concernant_la_commande_a__chou___');
		public static const GENERATION_NUM_CMD_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_g_n_ration_du_num_ro_de_commande_a__chou___');
		
		public static const RECUP_CONSTRU_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_constructeurs_a__chou__');
		public static const RECUP_MODELES_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_mod_les_a__chou__');
		public static const MODELES_EXISTANT:String=ResourceManager.getInstance().getString('M111', 'Ce_mod_le_existe_d_j_');
		
		public static const SAVE_INFOS_CONTRAT_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'L_enregistrement_des_informations_concernant_le_contrat_a__chou___');
		public static const DELETE_CONTRAT_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_suppression_du_contrat_a__chou___');
		
		public static const SAVE_INFOS_INCIDENT_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'L_enregistrement_des_informations_concernant_l_incident_a__chou__');
		public static const DELETE_INCIDENT_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_suppression_de_l_incident_a__chou__');
		public static const SAVE_INFOS_INTERVENTION_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'L_enregistrement_des_informations_concernant_l_intervention_a__chou__');
		public static const DELETE_INTERVENTION_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_suppression_de_l_intervention_a__chou__');
		public static const RECUP_LISTE_INTER_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_types_d_interventions_a__chou__');
		
		public static const RECUP_FICHE_SIM_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_fiche_sim_a__chou__');
		public static const UPDATE_FICHE_SIM_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_mise___jour_de_la_fiche_sim_a__chou___');
		public static const NUMERO_SIM_EXISTANT:String=ResourceManager.getInstance().getString('M111', 'Le_num_ro_de_sim_que_vous_avez_choisi_est_d_j__utilis__');
		
		public static const RECUP_FICHE_LIGNE_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_fiche_ligne_a__chou__');
		public static const UPDATE_FICHE_LIGNE_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_mise___jour_de_la_fiche_ligne_a__chou___');
		public static const NUMERO_LIGNE_EXISTANT:String=ResourceManager.getInstance().getString('M111', 'Le_num_ro_de_ligne_que_vous_avez_choisi_est_d_j__utilis__');
		public static const RECUP_INFO_COL_FACURE_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_des_informations_de_la_colonne_factur__a__chou__');
		public static const RECUP_INFO_HISTO_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_des_informations_de_l_historique__a__chou__');
		public static const UPDATE_RACCOR_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_mise___jour_du_raccordement_a__chou__');
		public static const RENUM_LIGNE_IMPOSSIBLE_CAUSE_LIGNE_EXISTE:String=ResourceManager.getInstance().getString('M111', 'renum_impossible_avec_ligne_factur_');
		public static const RENUM_LIGNE_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'renum_impossible_');
		public static const GET_ORGA_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_organisations_a__chou__');
		
		public static const RECUP_FICHE_SITE_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_fiche_site_a__chou__');
		public static const RECUP_EMPLA_SITE_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_des_emplacements_du_site_a__chou__');
		public static const UPDATE_FICHE_SITE_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_mise___jour_de_la_fiche_site_a__chou___');
		
		public static const ASSOC_COLLAB_LIGNE_SIM_IMP:String=ResourceManager.getInstance().getString('M111', 'L_association_du_collab_a_la_ligne_sim_a__chou__');
		
		public static const RECUP_LISTE_TYPE_CONTRAT_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_contrats_a__chou__');
		public static const RECUP_LISTE_TYPE_INTER_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_types_d_interventions_a__chou__');
		public static const RECUP_LISTE_REVENDEUR_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_revendeurs_a__chou__');
		public static const RECUP_SITES_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_sites_a__chou__');
		public static const RECUP_LIBELLE_PERSO_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_libell_s_personnalis_s_a__chou__');
		public static const RECUP_CIVILITES_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_civilit_s_a__chou__');
		public static const RECUP_LISTE_USAGES_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_usages_a__chou__');
		public static const RECUP_LISTE_PROF_EQPT_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_prof_eqpt_a__chou__');
		public static const RECUP_LISTE_TYPE_LIGNE_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_types_ligne_a__chou__');
		public static const RECUP_LISTE_OPERATEUR_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_operateurs_a__chou__');
		public static const RECUP_INFOS_OP_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_des_infos_op_a__chou__');
		public static const HISTO_LOG_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'L_historique_n_a_pas_pu__tre__crit__');
		public static const RECUP_LISTE_PAYS_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_pays_a__chou__');
		public static const RECUP_LISTE_CATEGORIES_EQPT_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_cat_gories__quipement_a__chou__');
		public static const RECUP_LISTE_TYPES_EQPT_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_types__quipement_a__chou__');
		public static const RECUP_LISTE_TYPES_EQPT_WITH_CAT_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_types__quipement_avec_leurs_cat_gories_a__chou__');
		public static const RECUP_LISTE_MODE_RACCOR_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_modes_de_raccordements_a__chou__');
		public static const RECUP_LISTE_DE_TS_LES_OP_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_de_tous_les_op_rateurs_a__chou__');
		public static const RECUP_LISTE_ACT_RACC_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_actions_de_raccordement_a__chou__');
		public static const RECUP_LINE_DISPO_GPEMENT_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_lignes_disponibles_pour_un_groupement_a__chou__');
		public static const RECUP_LINE_GPEMENT_IMP:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_lignes_du_groupement_a__chou__');
		
		public static const PB_GESTION_FAVORIS:String=ResourceManager.getInstance().getString('M111', 'Probl_me_avec_la_gestion_des_favoris__');
		public static const RECUP_CPT_SSCPT_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_des_comptes_et_des_sous_');
		public static const RECHERCHE_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'Recherche_impossible__');
		public static const RECUPERER_DROITUSER_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'Une_erreur_est_survenue_lors_de_la_r_cup');
		public static const LOAD_NODE_ORGA_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'Le_chargement_des_noeuds_de_l_organisation_a__chou__');
		public static const RECUP_LIGNE_SIM_SS_TERM_IMPOSSIBLE:String=ResourceManager.getInstance().getString('M111', 'La_r_cup_ration_de_la_liste_des_lignes_sim_sans_terminaux_a__chou__');
		
		public static const RECUP_TYPE_USER_PROFILE_ERREUR:String=ResourceManager.getInstance().getString('M111', 'R_cup_ration_du_profil_impossible');
		public static const RECUP_MAIL_REVENDEUR_ERREUR:String=ResourceManager.getInstance().getString('M111', 'R_cup_ration_des_emails_revendeur_impossible');
		
		public static const ASSOC_LIGNE_EXT_IMP:String=ResourceManager.getInstance().getString('M111', 'L_association_de_la_ligne_a_l_extension_a__chou__');
		public static const ASSOC_LIGNE_EXT_EQP_IMP:String=ResourceManager.getInstance().getString('M111', 'L_association_du_collaborateur_a_la_ligne_sim_a__chou__');
		public static const ASSOC_COLLAB_LIGNE_EXT_IMP:String=ResourceManager.getInstance().getString('M111', 'L_association_du_collaborateur_a_la_ligne_extension_a__chou__');
		public static const DIASSOC_COLLAB_LIGNE_SIM_IMP:String=ResourceManager.getInstance().getString('M111', 'La_dissociation_du_collaborateur_de_l_extension_a__chou__');
		public static const DIASSOC_EQP_LIGNE_EXT_IMP:String=ResourceManager.getInstance().getString('M111', 'La_dissociation_de_l_extension_de_l__quipement_a__chou__');
		public static const DIASSOC_LIGNE_EXT_IMP:String=ResourceManager.getInstance().getString('M111', 'La_dissociation_de_l_extension_et_la_ligne_a__chou__');
		public static const ASSOC_COLLAB_EXT_IMP:String=ResourceManager.getInstance().getString('M111', 'L_association_du_collaborateur_a_l_extension_a__chou__');
		public static const ASSOC_EQP_EXT_IMP:String=ResourceManager.getInstance().getString('M111', 'L_association_de_l_extension_et_l__equipement_a__chou__');
		public static const ECHANGE_EXT_DISPO_IMP:String=ResourceManager.getInstance().getString('M111', 'L_echange_de_l_extension_a__chou__');
		

		public static const ASSOC_ERROR_EQPT_CALLAB_POOL:String = ResourceManager.getInstance().getString('M111', 'ASSOC_ERROR_EQPT_CALLAB_POOL');
		public static const ASSOC_ERROR_EQPT_EQPT_POOL:String = ResourceManager.getInstance().getString('M111', 'ASSOC_ERROR_EQPT_EQPT_POOL');
		public static const ASSOC_ERROR_LIGNE_SIM_POOL:String = ResourceManager.getInstance().getString('M111', 'ASSOC_ERROR_LIGNE_SIM_POOL');
		public static const ASSOC_ERROR_LIGNE_COLLAB_POOL:String = ResourceManager.getInstance().getString('M111', 'ASSOC_ERROR_LIGNE_COLLAB_POOL');
		
	}
}
