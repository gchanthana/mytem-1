package gestionparc.utils
{
	import mx.controls.Alert;
	import mx.resources.ResourceManager;

	public class ConfirmDialogBox
	{
		public function ConfirmDialogBox()
		{
		}
		
		public static function display(text : String , titre : String,traitement :  Function):void{
			
			[Embed(source='/assets/images/warning.png')]
			var myAlertImg0 : Class;
			var myAlertBox0 : Alert;								
			Alert.okLabel = ResourceManager.getInstance().getString('M111','Valider');
			Alert.cancelLabel = ResourceManager.getInstance().getString('M111','Annuler');			
			Alert.show(text,titre,Alert.OK | Alert.CANCEL,
				null,traitement,myAlertImg0);			
		}
	}
}