package gestionparc.gestion
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.managers.PopUpManager;
	import mx.managers.PopUpManagerChildList;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	
	import composants.util.ConsoviewAlert;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.TypeCommandeEvent;
	import gestionparc.ihm.fiches.popup.SelectTypeCommandeIHM;


	[Bindable]
	public class GestionCommandeMobile extends EventDispatcher
	{
		private var _context:Object;
		private var _popupSelectTypeCommande:SelectTypeCommandeIHM;

		private const SPIE_1					:Number = 5821693;
		private const SPIE_2					:Number = 6149989;
		
		public function GestionCommandeMobile(pool:Object)
		{
			if (pool)
			{
				_context=pool;
			}
		}

		public function goCommande():Boolean
		{
			var groupIdx:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			if (_context != null)
			{
				if((groupIdx == SPIE_1) || (groupIdx == SPIE_2))
					redirectNouvelleCmdSpie();
				else
					showSelectTypeCommandeDialogBox();
				return true;
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'Veuillez_s_lectionner_un_pool__'), ResourceManager.getInstance().getString('M111', 'Consoview'));

			return false
		}
		
		private function redirectNouvelleCmdSpie():void
		{
			var idsegment:int=3;
			var renouvellement:Boolean=false;
			
			_context.IDSEGMENT=idsegment;
			_context.IDTYPECOMMANDE=1;
			
			switchToCommande(renouvellement);
			
		}
		
		public function goResiliation():Boolean
		{
			if (_context != null)
			{
				switchToCommande(false);
				return true
			}

			return false
		}
		
		public function goSAV():Boolean
		{
			if (_context != null)
			{
				switchToCommande(false);
				return true
			}
			
			return false
		}

		public function goRenouvellement():Boolean
		{
			if (_context != null)
			{
				switchToCommande(true);
				return true
			}

			return false
		}

		public function goModification():Boolean
		{
			if (_context != null)
			{
				switchToCommande(false);
				return true
			}

			return false
		}

		public function goSuspension():Boolean
		{
			if (_context != null)
			{
				switchToCommande(false);
				return true
			}

			return false
		}
		
		public function goActivation():Boolean
		{
			if (_context != null)
			{
				switchToCommande(false);
				return true
			}
			
			return false
		}
		
		public function goPortabilite():void
		{
			var idsegment:int=1;
			var renouvellement:Boolean=false;
			
			_context.IDSEGMENT=idsegment;
			_context.IDTYPECOMMANDE=12;
			
			switchToCommande(true);
		}

		private function showSelectTypeCommandeDialogBox():void
		{
			var fixIsAutorised:Boolean=SpecificationVO.getInstance().hasCommandeFixe;
			var mobIsAutorised:Boolean=SpecificationVO.getInstance().hasCommandeMobile;


			//DROIT UNIQUEMENT POUR LE MOBILE ET LE FIXE OU SI ON A LE DROIT UNIQUEMENT POUR LE FIXE (IL FAUDRA CHOISIR ENTRE LE FIXE ET LA DATA)
			if ((fixIsAutorised == true && mobIsAutorised == true) || (fixIsAutorised == true && mobIsAutorised == false))
			{
				_popupSelectTypeCommande=new SelectTypeCommandeIHM();
				_popupSelectTypeCommande.idProfil=_context.IDPROFIL;
				_popupSelectTypeCommande._fixIsAutorised=fixIsAutorised;
				_popupSelectTypeCommande._mobIsAutorised=mobIsAutorised;
				_popupSelectTypeCommande.addEventListener(TypeCommandeEvent.OPE_TYPE_CMD, segmentSelected);

				PopUpManager.addPopUp(_popupSelectTypeCommande, Application.application as DisplayObject, true, PopUpManagerChildList.PARENT);
				PopUpManager.centerPopUp(_popupSelectTypeCommande);
			}


			if (fixIsAutorised == false && mobIsAutorised == true) //DROIT UNIQUEMENT POUR LE MOBILE
			{
				var te:TypeCommandeEvent=new TypeCommandeEvent(TypeCommandeEvent.OPE_TYPE_CMD, TypeCommandeEvent.OPE_NEWCMD, TypeCommandeEvent.TYPE_MOBILE, true);
				segmentSelected(te);
			}

			if (fixIsAutorised == false && mobIsAutorised == false) //AUCUN DROIT
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M111', 'Vous_n_avez_pas_les_droits_n_cessaire_pour_'), ResourceManager.getInstance().getString('M111', 'Consoview'), null);
			}
		}

		private function segmentSelected(te:TypeCommandeEvent):void
		{
			var typeCommande:String=te.typeCommande;
			var typeOperation:String=te.typeOperation;
			var idsegment:int=0;
			var renouvellement:Boolean=false;


			if (typeOperation == TypeCommandeEvent.OPE_NEWCMD)
			{
				switch (typeCommande)
				{
					case TypeCommandeEvent.TYPE_MOBILE:
						idsegment=1;
						break;
					case TypeCommandeEvent.TYPE_FIXE:
						idsegment=2;
						break;
					case TypeCommandeEvent.TYPE_RESEAU:
						idsegment=3;
						break;
				}
			}
			else
			{
				renouvellement=true;

				switch (typeCommande)
				{
					case TypeCommandeEvent.TYPE_MOBILE:
						idsegment=1;
						break;
					case TypeCommandeEvent.TYPE_FIXE:
						idsegment=2;
						break;
					case TypeCommandeEvent.TYPE_RESEAU:
						idsegment=3;
						break;
				}
			}

			_context.IDSEGMENT=idsegment;
			_context.IDTYPECOMMANDE=1;

			if (_popupSelectTypeCommande && _popupSelectTypeCommande.isPopUp)
			{
				PopUpManager.removePopUp(_popupSelectTypeCommande);
			}

			switchToCommande(renouvellement);
		}

		private function switchToCommande(renouvellement:Boolean):void
		{
			updateSession(CvAccessManager.getSession().INFOS_DIVERS, renouvellement);
			changeUniversFunction();
		}


		private function updateSession(sessionInfos:Object, renouvellement:Boolean):void
		{
			if (!sessionInfos)
			{
				sessionInfos={};
			}

			sessionInfos.BOOLNEWCOMMANDE=true;
			sessionInfos.POOL=formatToM16Pool(_context);
			sessionInfos.IDSEGMENT=(_context.IDSEGMENT > 0) ? _context.IDSEGMENT : 1;
			sessionInfos.RENVL=renouvellement;
			sessionInfos.IDTYPECOMMANDE=(_context.IDTYPECOMMANDE > 0) ? _context.IDTYPECOMMANDE : 1; //--- 1-> NOUVELLE LIGNE OU EQ (mobile, fixe, data), 2-> REENGAGEMENT, 3-> MODIFICATION OPTION, 4-> SUSPENSION, 5-> REACTIVATION, 6-> RESILIATION

			if (_context.hasOwnProperty('ligne') && _context.ligne)
			{
				var matricule:String =  (_context.ligne.E_MATRICULE)?(_context.ligne.E_MATRICULE):' ';
				var collaborateur:String =(_context.ligne.COLLABORATEUR)? (_context.ligne.COLLABORATEUR + '    ' + matricule) : ' ';
				sessionInfos.LIBELLECOLLABORATEUR=StringUtil.trim(collaborateur);
				
				sessionInfos.IDCOLLABORATEUR=_context.ligne.IDEMPLOYE;
				sessionInfos.IDOPERATEUR=_context.ligne.OPERATEURID;
				sessionInfos.LIBELLEOPERATEUR=_context.ligne.OPERATEUR;
				sessionInfos.IDSOUSTETE=_context.ligne.IDSOUS_TETE;
				if (_context.IDTYPECOMMANDE == 13)
				{
					sessionInfos.IMEI=_context.ligne.T_IMEI;
					sessionInfos.IDEQUIPEMENT=_context.IDEQUIPEMENT;
				}
			}
			sessionInfos.IS_IN_COMMANDE_PROCESS=true;
		}

		private function changeUniversFunction():void
		{
			CvAccessManager.changeModule('GEST_CMDMOBILE');
		}

		private function formatToM16Pool(value:Object):Object
		{
			var poolObj:Object=new Object();
			poolObj.LIBELLE_POOL=value.LIBELLE_POOL;
			poolObj.LIBELLE_PROFIL=value.LIBELLE_PROFIL;
			poolObj.LIBELLE_CONCAT=value.LIBELLE_POOL + ' - ' + value.LIBELLE_PROFIL;
			poolObj.IDPOOL=value.IDPOOL;
			poolObj.IDPROFIL=value.IDPROFIL;
			poolObj.IDREVENDEUR=value.IDREVENDEUR;
			poolObj.IDOPERATEUR

			return poolObj;
		}
	}
}
