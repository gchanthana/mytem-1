package gestionparc.services.organisation
{
	import flash.events.EventDispatcher;
	
	import gestionparc.entity.OrgaVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;
	
	import mx.collections.ArrayCollection;
	
	public class OrganisationDatas extends EventDispatcher
	{
		
		private var _listeFeuilles			: ArrayCollection = null;
		private var _listeOrga				: ArrayCollection;
		private var _structureOrga			: String = "";
		
		private var _cheminSelected			: Object = null;
		

		public function treatDatasGetNodesOrga(obj:Object):void
		{
			treatListeFeuille(obj[0]);
			treatStructureOrga(obj[1]);
			
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTED_NODES_ORGA_LOADED));
		}
		
		/**
		 * Créé la liste des organisations.
		 */	
		public function treatGetListOrgaClienteResultHandler(obj:Object):void
		{
			listeOrga = new ArrayCollection();
			var myOrga : OrgaVO = new OrgaVO();
			listeOrga = myOrga.fillListOrgaVO(obj);
			
			CacheDatas.listeOrga = new ArrayCollection();
			CacheDatas.listeOrga = listeOrga;
				
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_ORGA_LOADED));
		}
	
		/**
		 * Affecte à la data "listeFeuilles" toutes les feuilles de l'organisation et leurs valeurs.
		 */	
		private function treatListeFeuille(myArray:ArrayCollection):void
		{
			var objFeuille : Object;
			var longeur : int = myArray.length;
			
			listeFeuilles = new ArrayCollection();
			
			for(var i :int = 0 ; i < longeur ; i++)
			{
				objFeuille = new Object();
				objFeuille.chemin = myArray[i].CHEMIN;
				objFeuille.idFeuille = myArray[i].IDFEUILLE;
				
				listeFeuilles.addItem(objFeuille);
			}
		}
		
		/**
		 * Créé la structure de l'organisation et l'affecte à la data "structureOrga"
		 */	
		private function treatStructureOrga(myArray:ArrayCollection):void
		{
			var longeur : int = myArray.length;
			
			for(var i :int = 0 ; i < longeur ; i++)
			{
				structureOrga += myArray[i].TYPE_NIVEAU + "/";
			}
		}
		
		public function set listeFeuilles(value:ArrayCollection):void
		{
			_listeFeuilles = value;
		}

		public function get listeFeuilles():ArrayCollection
		{
			return _listeFeuilles;
		}

		public function set structureOrga(value:String):void
		{
			_structureOrga = value;
		}

		public function get structureOrga():String
		{
			return _structureOrga;
		}

		public function set cheminSelected(value:Object):void
		{
			_cheminSelected = value;
		}

		public function get cheminSelected():Object
		{
			return _cheminSelected;
		}

		public function set listeOrga(value:ArrayCollection):void
		{
			_listeOrga = value;
		}

		public function get listeOrga():ArrayCollection
		{
			return _listeOrga;
		}
	}
}