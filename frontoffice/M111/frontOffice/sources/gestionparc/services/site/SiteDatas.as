package gestionparc.services.site
{
	import flash.events.EventDispatcher;
	
	import gestionparc.entity.SiteVO;
	import gestionparc.event.gestionparcEvent;
	
	import mx.collections.ArrayCollection;


	public class SiteDatas extends EventDispatcher
	{
		private var _site:SiteVO=new SiteVO();
		private var _listEmplacement:ArrayCollection=null;

		/**
		 * Traite les données reçues au retour de la procedure <code>getSiteEmplacement</code>.
		 * Appel la fonction <code>fillSiteVO()</code> pour affecter a la data "site" toutes les données concernant un site.
		 */
		public function treatDataGetSiteEmplacement(obj:Object):void
		{
			site=new SiteVO();
			site=site.fillSiteVO(obj);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_SITE_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getEmplacement</code>.
		 * Appel la fonction <code>fillListEmplacementVO()</code> pour affecter a la data "listEmplacement" toutes les données concernant les emplacements d'un site.
		 */
		public function treatDataGetEmplacement(obj:Object):void
		{
			var mySite:SiteVO=new SiteVO();
			listEmplacement=new ArrayCollection();
			listEmplacement=mySite.fillListEmplacementVO(obj);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_EMPLA_SITE_LOADED));
		}

		/**
		 * Dispatch un événements pour signaler que la mise à jour des informations
		 * concernant la fiche Site s'est bien déroulée.
		 */
		public function treatDataSetSiteEmplacement():void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED));
		}

		public function set site(value:SiteVO):void
		{
			_site=value;
		}

		public function get site():SiteVO
		{
			return _site;
		}

		public function set listEmplacement(value:ArrayCollection):void
		{
			_listEmplacement=value;
		}

		public function get listEmplacement():ArrayCollection
		{
			return _listEmplacement;
		}
	}
}
