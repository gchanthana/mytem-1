package gestionparc.services.liste
{
	import mx.rpc.AbstractOperation;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.SpecificationVO;

	public class ListeService
	{
		public var myDatas:ListeDatas;
		public var myHandlers:ListeHandlers;

		private var urlBackoffice:String="fr.consotel.consoview.M111.actions.Liste"
		private var GROUPE_INDEX:int=CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;

		public function ListeService()
		{
			myDatas=new ListeDatas();
			myHandlers=new ListeHandlers(myDatas);
			
		}

		public function listeCollaborateur(term:Number, ligne:Number ,isTypeSim :Boolean):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"ListeCollab", myHandlers.listeCollaborateurHandler);

			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, ligne, term,isTypeSim);
		}

		public function listeAllCollaborateur():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"listeAllCollaborateur", myHandlers.listeCollaborateurHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool);
		}

		public function ListeLigne():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"ListeLigne", myHandlers.listeLigneHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, "");
		}

		public function ListeSimAvecLigne(boolhasterm:int, boolhascollab:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"ListeSimAvecLigneV2", myHandlers.ListeSimSsTermHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, boolhasterm, boolhascollab);
		}

		public function ListeExtAvecLigne(boolhasterm:int, boolhascollab:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"ListeExtAvecLigne", myHandlers.ListeExtAvecLigneHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, boolhasterm, boolhascollab);
		}
		
		public function ListeSimSsLigne(boolhasterm:int, boolhascollab:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"ListeSimSsLigneV2", myHandlers.ListeSimSsLigneHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, boolhasterm, boolhascollab);
		}

		public function ListeExtSansLigne(boolhasterm:int, boolhascollab:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"ListeExtSansLigne", myHandlers.ListeExtSansLigneHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, boolhasterm, boolhascollab);
		}
		
		
		public function ListeSimFixeSsTerm():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"listeSimFixeAvecLigne", myHandlers.ListeSimFixeSsTermHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, "");
		}

		public function ListeSimFixeSsLigne():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"listeSimFixeSansLigne", myHandlers.ListeSimFixeSsLigneHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, "");
		}

		public function listeTermDispo():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeTermDispo", myHandlers.listeTermDispoHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, "");
		}

		public function listeTermAvecSimDispo():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeTermAvecSimDispo", myHandlers.listeTermAvecSimDispoHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, "");
		}

		public function listeTermFixeDispo():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeTermFixeWithoutEqpFixe", myHandlers.listeTermFixeDispoHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, "");
		}

		public function listeTermFixeAvecEqpFixeDispo():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeTermFixeWithEqpFixe", myHandlers.listeTermFixeAvecEqpFixeDispoHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, "");
		}

		public function listeEqpOfCollab():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeEqpOfCollab", myHandlers.listeEqpOfCollabHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE, "");
		}

		public function listeSimWithLigneOfCollab():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeSimWithLigneOfCollab", myHandlers.listeSimOfCollabHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE, "");
		}

		public function listeSimWithoutLigneOfCollab():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeSimWithoutLigneOfCollab", myHandlers.listeSimWithLigneOfCollabHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE, "");
		}

		public function listeEqFixeWithLigneOfCollab():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeEqFixeOfCollab", myHandlers.listeEqFixeWithLigneOfCollabHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE, "");
		}

		public function listeEqFixeWithoutLigneOfCollab():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeEqFixeWithoutLigneOfCollab", myHandlers.listeEqFixeWithoutLigneOfCollabCollabHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE, "");
		}

		public function listeTermMobileOfCollab():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeTermMobileOfCollab", myHandlers.listeTermMobileOfCollabHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE, "");
		}

		public function ListeSsEqp(segmob:Number=0, segFixe:Number=0, segDat:Number=0):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"ListeSsEqp", myHandlers.ListeSsEqpHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL, SpecificationVO.getInstance().idPool, segmob, segFixe, segDat);
		}

		public function ListeSsEqpOfEqp():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"ListeSsEqpOfEqp", myHandlers.ListeSsEqpOfEqpHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL, SpecificationVO.getInstance().idPool);
		}

		public function listeTermFixeOfCollab():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeTermFixeOfCollab", myHandlers.listeTermFixeOfCollabHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE, "");
		}

		public function listeLigneOfCollab():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeLigneOfCollab", myHandlers.listeLigneOfCollabHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE, "");
		}

		public function listeEqptFixeDisponible():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"listeEqptFixeDispo", myHandlers.listeEqptFixeDispoHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool);
		}

		public function ListeLigneFixeDispo():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"ListeLigneFixeDispo", myHandlers.listeLigneFixeDispoHandler);
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool);
		}
	}
}
