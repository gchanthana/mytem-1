package gestionparc.services.liste
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;

	import gestionparc.event.gestionparcEvent;

	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;

	public class ListeDatas extends EventDispatcher
	{
		private var _dataListeTermMobileOfCollab:ArrayCollection=new ArrayCollection();
		private var _dataListeTermFixeOfCollab:ArrayCollection=new ArrayCollection();
		private var _dataListeTermSansSim:ArrayCollection=new ArrayCollection();
		private var _dataListeTermAvecSim:ArrayCollection=new ArrayCollection();
		private var _dataListeTermFixeSansEqpFixe:ArrayCollection=new ArrayCollection();
		private var _dataListeTermFixeAvecEqpFixe:ArrayCollection=new ArrayCollection();
		private var _dataListeSIM:ArrayCollection=new ArrayCollection();
		private var _dataListeSIMssLigne:ArrayCollection=new ArrayCollection();
		private var _dataListeSIMssTerm:ArrayCollection=new ArrayCollection();
		private var _dataListeEqpOfCollab:ArrayCollection=new ArrayCollection();
		private var _dataListeCollab:ArrayCollection=new ArrayCollection();
		private var _dataListeligne:ArrayCollection=new ArrayCollection();
		private var _dataListeligneOfCollab:ArrayCollection=new ArrayCollection();
		private var _dataListeSIMOfCollab:ArrayCollection=new ArrayCollection();
		private var _dataListeSimFixessLigne:ArrayCollection=new ArrayCollection();
		private var _dataListeSimFixessTerm:ArrayCollection=new ArrayCollection();
		private var _dataListeSousEqpOfEqp:ArrayCollection=new ArrayCollection();
		private var _dataListeSimFixeWithoutLigneOfCollab:ArrayCollection=new ArrayCollection();
		private var _dataListeSimFixeWithLigneOfCollab:ArrayCollection=new ArrayCollection();
		private var _listeEqptDispo:ArrayCollection=new ArrayCollection();
		private var _listeEqptFixeDispo:ArrayCollection=new ArrayCollection();
		private var _listeLigneFixeDispo:ArrayCollection=new ArrayCollection();
		private var _dataListeExtavecLigne:ArrayCollection=new ArrayCollection();
		private var _dataListeExtSansLigne:ArrayCollection=new ArrayCollection();
		
		public function ListeDatas(target:IEventDispatcher=null)
		{
			super(target);
		}

		public function treatListeCollaborateur(resultObject:ArrayCollection):void
		{
			dataListeCollab=resultObject as ArrayCollection;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_COLLAB));
		}

		public function treatListeLigne(resultObject:ArrayCollection):void
		{
			dataListeligne=resultObject;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_LIGNE));
		}

		public function treatListeSimSsTerm(resultObject:ArrayCollection):void
		{
			dataListeSIMssTerm=resultObject;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_SIMSSTERM));
		}

		public function treatListeExtLigne(resultObject:ArrayCollection):void
		{
			dataListeExtavecLigne=resultObject;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_EXTAVECLIGNE));
		}

		public function treatListeSimSsLigne(resultObject:ArrayCollection):void
		{
			dataListeSIMssLigne=resultObject;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_SIMSSLIGNE));
		}

		public function treatListeExtSansLigne(resultObject:ArrayCollection):void
		{
			dataListeExtSansLigne=resultObject;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_EXTSANSLIGNE));
		}
		
		public function treatListeSimFixeSsTerm(resultObject:ArrayCollection):void
		{
			dataListeSimFixessTerm=resultObject;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_SIMFIXESSTERM));
		}

		public function treatListeSimFixeSsLigne(resultObject:ArrayCollection):void
		{
			dataListeSimFixessLigne=resultObject;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_SIMFIXESSLIGNE));
		}

		public function treatlisteTermDispo(resultObject:ArrayCollection):void
		{
			dataListeTermSansSim=listeSansDoublon(resultObject);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_TERMSANSSIM));
		}

		public function treatlisteTermAvecSimDispo(resultObject:ArrayCollection):void
		{
			var newCol:ArrayCollection=new ArrayCollection();
			var i:int;
			for (i=0; i < resultObject.length; i++)
			{
				if (resultObject.getItemAt(i).IDSIM != null)
				{
					newCol.addItem(resultObject.getItemAt(i));
				}
			}
			dataListeTermAvecSim=newCol;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_TERMAVECSIM));
		}

		public function treatlisteTermFixeSansEqpFixe(resultObject:ArrayCollection):void
		{
			dataListeTermFixeSansEqpFixe=listeSansDoublon(resultObject);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_TERMFIXESANSEQPFIXE));
		}

		public function treatlisteTermFixeAvecEqpFixe(resultObject:ArrayCollection):void
		{
			dataListeTermFixeAvecEqpFixe=listeSansDoublon(resultObject);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_TERMFIXEAVECEQPFIXE));
		}

		public function treatlisteEqpOfCollab(resultObject:ArrayCollection):void
		{
			dataListeEqpOfCollab=resultObject;

			for each (var tmp:Object in dataListeEqpOfCollab)
			{
				if (!tmp.IMEI && tmp.IDTERMINAL)
				{
					tmp.IMEI=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_EQPOFCOLLAB));
		}

		public function treatlisteTermMobileOfCollab(resultObject:ArrayCollection):void
		{
			dataListeTermMobileOfCollab=resultObject;

			for each (var tmp:Object in dataListeTermMobileOfCollab)
			{
				if (!tmp.IMEI && tmp.IDTERMINAL)
				{
					tmp.IMEI=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_TERMMOBILEOFCOLLAB));
		}

		public function treatListeSsEqp(resultObject:ArrayCollection):void
		{
			dataListeSousEqpOfEqp=resultObject;

			for each (var tmp:Object in dataListeSousEqpOfEqp)
			{
				if (!tmp.IMEI && tmp.IDTERMINAL)
				{
					tmp.IMEI=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_SOUSEQPOFEQP));
		}

		public function treatListeSsEqpOfEqp(resultObject:ArrayCollection):void
		{
			dataListeSousEqpOfEqp=resultObject;

			for each (var tmp:Object in dataListeSousEqpOfEqp)
			{
				if (!tmp.IMEI && tmp.IDTERMINAL)
				{
					tmp.IMEI=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_SOUSEQPOFEQP));
		}

		public function treatlisteTermFixeOfCollab(resultObject:ArrayCollection):void
		{
			dataListeTermFixeOfCollab=resultObject;

			for each (var tmp:Object in dataListeTermFixeOfCollab)
			{
				if (!tmp.IMEI && tmp.IDTERMINAL)
				{
					tmp.IMEI=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_TERMFIXEOFCOLLAB));
		}

		public function treatlisteSimOfCollab(resultObject:ArrayCollection):void
		{
			dataListeSIMOfCollab=resultObject;

			for each (var tmp:Object in dataListeSIMOfCollab)
			{
				if (!tmp.NUM_SIM && tmp.IDSIM)
				{
					tmp.NUM_SIM=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
				if (!tmp.LIGNE && tmp.IDSOUS_TETE)
				{
					tmp.LIGNE=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_SIMOFCOLLAB));
		}

		public function treatlisteSimWithLigneOfCollab(resultObject:ArrayCollection):void
		{
			dataListeSIMOfCollab=resultObject;

			for each (var tmp:Object in dataListeSIMOfCollab)
			{
				if (!tmp.NUM_SIM && tmp.IDSIM)
				{
					tmp.NUM_SIM=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
				if (!tmp.LIGNE && tmp.IDSOUS_TETE)
				{
					tmp.LIGNE=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_SIMOFCOLLAB));
		}

		public function treatlisteEqFixeWithLigneOfCollab(resultObject:ArrayCollection):void
		{
			dataListeSimFixeWithLigneOfCollab=resultObject;

			for each (var tmp:Object in dataListeSimFixeWithLigneOfCollab)
			{
				if (!tmp.NUM_SIM && tmp.IDSIM)
				{
					tmp.NUM_SIM=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
				if (!tmp.LIGNE && tmp.IDSOUS_TETE)
				{
					tmp.LIGNE=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_SIMFIXEWITHLIGNE_OFCOLLAB));
		}

		public function treatlisteEqFixeWithoutLigneOfCollab(resultObject:ArrayCollection):void
		{
			dataListeSimFixeWithoutLigneOfCollab=resultObject;

			for each (var tmp:Object in dataListeSimFixeWithoutLigneOfCollab)
			{
				if (!tmp.NUM_SIM && tmp.IDSIM)
				{
					tmp.NUM_SIM=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
				if (!tmp.LIGNE && tmp.IDSOUS_TETE)
				{
					tmp.LIGNE=ResourceManager.getInstance().getString('M111', 'n_c_');
				}
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_SIMFIXEWITHOUTLIGNE_COLLAB));
		}

		public function treatlisteLigneOfCollab(resultObject:ArrayCollection):void
		{
			dataListeligneOfCollab.source=resultObject.source;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_DATALISTE_LIGNEOFCOLLAB));
		}


		internal function updateListeEqptFixeDispo(values:ArrayCollection):void
		{
			_listeEqptFixeDispo.source=new Array();

			if (values != null)
			{
				_listeEqptFixeDispo.source=values.source;
			}
			dispatchEvent(new Event("listeEqptFixeDispoUpdated"));

		}

		internal function updateListeLigneFixeDispo(values:ArrayCollection):void
		{
			_listeLigneFixeDispo.source=new Array();

			if (values != null)
			{
				_listeLigneFixeDispo.source=values.source;
			}
			dispatchEvent(new Event("listeLigneFixeDispoUpdated"));
		}

		private function listeSansDoublon(myArray:ArrayCollection):ArrayCollection
		{
			var myResult:ArrayCollection=new ArrayCollection();

			for each (var tmp:Object in myArray)
			{
				if (myResult.contains(tmp) == false)
				{
					myResult.addItem(tmp);
				}
			}
			return myResult;
		}

		/* **************** */
		/* GETTER / SETTER  */
		/* **************** */
		public function get dataListeTermMobileOfCollab():ArrayCollection
		{
			return _dataListeTermMobileOfCollab;
		}

		public function set dataListeTermMobileOfCollab(value:ArrayCollection):void
		{
			_dataListeTermMobileOfCollab=value;
		}

		public function get dataListeTermFixeOfCollab():ArrayCollection
		{
			return _dataListeTermFixeOfCollab;
		}

		public function set dataListeTermFixeOfCollab(value:ArrayCollection):void
		{
			_dataListeTermFixeOfCollab=value;
		}

		public function get dataListeSIM():ArrayCollection
		{
			return _dataListeSIM;
		}

		public function set dataListeSIM(value:ArrayCollection):void
		{
			_dataListeSIM=value;
		}

		public function get dataListeEqpOfCollab():ArrayCollection
		{
			return _dataListeEqpOfCollab;
		}

		public function set dataListeEqpOfCollab(value:ArrayCollection):void
		{
			_dataListeEqpOfCollab=value;
		}

		public function get dataListeTermSansSim():ArrayCollection
		{
			return _dataListeTermSansSim;
		}

		public function set dataListeTermSansSim(value:ArrayCollection):void
		{
			_dataListeTermSansSim=value;
		}

		public function get dataListeTermAvecSim():ArrayCollection
		{
			return _dataListeTermAvecSim;
		}

		public function set dataListeTermAvecSim(value:ArrayCollection):void
		{
			_dataListeTermAvecSim=value;
		}

		public function get dataListeCollab():ArrayCollection
		{
			return _dataListeCollab;
		}

		public function set dataListeCollab(value:ArrayCollection):void
		{
			_dataListeCollab=value;
		}

		public function get dataListeligne():ArrayCollection
		{
			return _dataListeligne;
		}

		public function set dataListeligne(value:ArrayCollection):void
		{
			_dataListeligne=value;
		}

		public function get dataListeligneOfCollab():ArrayCollection
		{
			return _dataListeligneOfCollab;
		}

		public function set dataListeligneOfCollab(value:ArrayCollection):void
		{
			_dataListeligneOfCollab=value;
		}

		public function get dataListeSIMOfCollab():ArrayCollection
		{
			return _dataListeSIMOfCollab;
		}

		public function set dataListeSIMOfCollab(value:ArrayCollection):void
		{
			_dataListeSIMOfCollab=value;
		}

		public function get dataListeSIMssTerm():ArrayCollection
		{
			return _dataListeSIMssTerm;
		}

		public function set dataListeSIMssTerm(value:ArrayCollection):void
		{
			_dataListeSIMssTerm=value;
		}

		public function get dataListeSIMssLigne():ArrayCollection
		{
			return _dataListeSIMssLigne;
		}

		public function set dataListeSIMssLigne(value:ArrayCollection):void
		{
			_dataListeSIMssLigne=value;
		}

		public function get dataListeTermFixeAvecEqpFixe():ArrayCollection
		{
			return _dataListeTermFixeAvecEqpFixe;
		}

		public function set dataListeTermFixeAvecEqpFixe(value:ArrayCollection):void
		{
			_dataListeTermFixeAvecEqpFixe=value;
		}

		public function get dataListeTermFixeSansEqpFixe():ArrayCollection
		{
			return _dataListeTermFixeSansEqpFixe;
		}

		public function set dataListeTermFixeSansEqpFixe(value:ArrayCollection):void
		{
			_dataListeTermFixeSansEqpFixe=value;
		}

		public function get dataListeSimFixessLigne():ArrayCollection
		{
			return _dataListeSimFixessLigne;
		}

		public function set dataListeSimFixessLigne(value:ArrayCollection):void
		{
			_dataListeSimFixessLigne=value;
		}

		public function get dataListeSimFixessTerm():ArrayCollection
		{
			return _dataListeSimFixessTerm;
		}

		public function set dataListeSimFixessTerm(value:ArrayCollection):void
		{
			_dataListeSimFixessTerm=value;
		}

		public function get dataListeSousEqpOfEqp():ArrayCollection
		{
			return _dataListeSousEqpOfEqp;
		}

		public function set dataListeSousEqpOfEqp(value:ArrayCollection):void
		{
			_dataListeSousEqpOfEqp=value;
		}

		public function get dataListeSimFixeWithoutLigneOfCollab():ArrayCollection
		{
			return _dataListeSimFixeWithoutLigneOfCollab;
		}

		public function set dataListeSimFixeWithoutLigneOfCollab(value:ArrayCollection):void
		{
			_dataListeSimFixeWithoutLigneOfCollab=value;
		}

		public function get dataListeSimFixeWithLigneOfCollab():ArrayCollection
		{
			return _dataListeSimFixeWithLigneOfCollab;
		}

		public function set dataListeSimFixeWithLigneOfCollab(value:ArrayCollection):void
		{
			_dataListeSimFixeWithLigneOfCollab=value;
		}

		public function get dataListeExtavecLigne():ArrayCollection
		{
			return _dataListeExtavecLigne;
		}

		public function set dataListeExtavecLigne(value:ArrayCollection):void
		{
			_dataListeExtavecLigne=value;
		}

		
		public function get dataListeExtSansLigne():ArrayCollection
		{
			return _dataListeExtSansLigne;
		}
		
		public function set dataListeExtSansLigne(value:ArrayCollection):void
		{
			_dataListeExtSansLigne = value;
		}

		[Bindable(event="listeEqptFixeDispoUpdated")]
		public function get listeEqptFixeDispo():ArrayCollection
		{
			return _listeEqptFixeDispo;
		}


		[Bindable(event="listeLigneFixeDispoUpdated")]
		public function get listeLigneFixeDispo():ArrayCollection
		{
			return _listeLigneFixeDispo;
		}

	}
}
