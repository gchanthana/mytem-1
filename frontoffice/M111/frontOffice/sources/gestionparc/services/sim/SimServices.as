package gestionparc.services.sim
{
	import flash.events.EventDispatcher;
	
	import mx.rpc.AbstractOperation;
	import mx.utils.ObjectUtil;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	public class SimServices extends EventDispatcher
	{
		
		public var myDatas 		: SimDatas;
		public var myHandlers 	: SimHandlers;
		
		public function SimServices()
		{
			myDatas 	= new SimDatas();
			myHandlers 	= new SimHandlers(myDatas);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer :
		 * - Tous les informations concernant la SIM.
		 * - Tous les informations concernant les abonnements et options de la SIM.
		 * - Tous les informations concernant les incidents de la SIM.
		 * - Toutes les informations liées aux organisations de la SIM.
		 */	
		public function getInfosFicheSim(createObjToServiceGet:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheSim",
				"getInfosFicheSim",
				myHandlers.getInfosFicheSimResultHandler);
			
			RemoteObjectUtil.callService(op, 
				createObjToServiceGet.idSim, 
				createObjToServiceGet.idSousTete, 
				createObjToServiceGet.idGpeClient,
				createObjToServiceGet.ID
				);
		}
		
		/**
		 * Appelle la procédure permettant de mettre à jour :
		 * - Tous les informations concernant la Sim.
		 */	
		public function setInfosFicheSim(createObjToServiceSet : Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheSim",
				"setInfosFicheSim",
				myHandlers.setInfosFicheSimResultHandler);
			
			RemoteObjectUtil.callService(op, 
				createObjToServiceSet.mySim.idEquipement,
				createObjToServiceSet.mySim.numSerieEquipement,
				createObjToServiceSet.mySim.PIN1,
				createObjToServiceSet.mySim.PIN2,
				createObjToServiceSet.mySim.PUK1,
				createObjToServiceSet.mySim.PUK2,
				createObjToServiceSet.mySim.idSite,
				createObjToServiceSet.mySim.idEmplacement,
				createObjToServiceSet.mySim.byod);
		}
		public function mettreAuRebut(obj:Object):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheSim",
				"mettreAuRebut",
				myHandlers.rebusSimResultHandler);
			RemoteObjectUtil.callService(op,obj);
		}
		
		public function echangerSim1(obj:Object):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheSim",
				"echangerSim1",
				myHandlers.echangerSimResultHandler);
			trace("##### echangeSIM1111 obj  :  " + ObjectUtil.toString(obj));
			RemoteObjectUtil.callService(op,obj);
			RemoteObjectUtil.callService(op,obj);
		}
		
		public function echangerExt1(obj:Object):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheSim",
				"echangerExt1",
				myHandlers.echangerExtResultHandler);
			RemoteObjectUtil.callService(op,obj);
		}
		
		public function echangerSim2(obj:Object ):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheSim",
				"echangerSim2",
				myHandlers.echangerSimResultHandler);
			RemoteObjectUtil.callService(op,obj);
		}
		
		public function echangerSim2Manuellement(objectEchangeSim:Object, objectCreateSIM:Object = null ):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheSim",
				"echangerSim2Manually",
				myHandlers.echangerSimResultHandler);
			RemoteObjectUtil.callService(op, objectEchangeSim, objectCreateSIM);
		}
		
		public function echangerExt2(obj:Object):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheSim",
				"echangerExt2",
				myHandlers.echangerExtResultHandler);
			RemoteObjectUtil.callService(op,obj);
		}
	}
}