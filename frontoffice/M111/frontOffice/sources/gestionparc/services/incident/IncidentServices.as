package gestionparc.services.incident
{
	import composants.util.DateFunction;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.IncidentVO;
	import gestionparc.entity.InterventionVO;
	
	import mx.rpc.AbstractOperation;
	
	/**
	 * Appelle les procédures liées aux incidents.
	 */
	public class IncidentServices
	{
		
		public var myDatas 		: IncidentDatas;
		public var myHandlers 	: IncidentHandlers;
			
		public function IncidentServices()
		{
			myDatas 	= new IncidentDatas();
			myHandlers 	= new IncidentHandlers(myDatas);
		}
		
		/**
		 * Appelle la procédure permettant de mettre à jour :
		 * - Tous les informations concernant un incident d'équipement.
		 * 
		 * N.B. : Si l'idIncident = -1 alors l'action correspond à un ajout d'incident,
		 * 		  sinon cela correspond à une mise à jour des infos de l'incident.
		 */	
		public function setInfosIncident(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"setInfosIncident",
				myHandlers.setInfosIncidentResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}
		
		/**
		 * Appelle la procédure permettant de supprimer l'incident.
		 * Informe si l'equipement doit être mis en actif ou non.
		 */	
		public function deleteIncident(idEqpt:int,putActif:int,idIncident:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"deleteIncident",
				myHandlers.deleteIncidentResultHandler);
			
			RemoteObjectUtil.callService(op,idIncident,putActif,idEqpt);
		}
		public function getTypeUserTypeProfile():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getUserProfil",
				myHandlers.getTypeUserTypeProfileResultEvent);
			
			RemoteObjectUtil.callService(op);
		}
		
		public function getEmailsRevendeur(idRevendeur:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getEmailsRevendeur",
				myHandlers.getEmailsRevendeurResultEvent);
			
			RemoteObjectUtil.callService(op,idRevendeur);
		}
		/**
		 * Appelle la procédure permettant de récupérer :
		 * - La liste des interventions pour cet incident;
		 */	
		public function getListeIntervention(idIncident:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getListeIntervention",
				myHandlers.getListeInterventionResultHandler);
			
			RemoteObjectUtil.callService(op, idIncident);
		}
		
		/**
		 * Appelle la procédure permettant de mettre à jour :
		 * - Tous les informations concernant une intervention liée à un incident d'un équipement.
		 * 
		 * N.B. : Si l'idIntervention = -1 alors l'action correspond à un ajout d'intervention,
		 * 		  sinon cela correspond à une mise à jour des infos de l'intervention.
		 */	
		public function setInfosIntervention(myIntervention:InterventionVO, myIncident:IncidentVO):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"setInfosIntervention",
				myHandlers.setInfosInterventionResultHandler);
			
			RemoteObjectUtil.callService(op, 
				myIncident.idIncident,
				myIntervention.idIntervention,
				myIntervention.libelle,
				DateFunction.formatDateAsInverseString(myIntervention.dateIntervention),
				myIntervention.horaire,
				myIntervention.idTypeIntervention,
				myIntervention.prix,
				myIntervention.description,
				myIntervention.isClose);
		}
		
		/**
		 * Appelle la procédure permettant de supprimer l'Intervention.
		 */	
		public function deleteIntervention(myIntervention:InterventionVO):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"deleteIntervention",
				myHandlers.deleteInterventionResultHandler);
			
			RemoteObjectUtil.callService(op,myIntervention.idIntervention);
		}
	}
}