package gestionparc.services.vue.parc
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	/**
	 * Envoie les données reçues à la classe
	 * au retour des procédures appelées dans <code>RechercheServices</code>.
	 */
	public class VueParcHandler
	{
		private var model:VueParcData;

		public function VueParcHandler(instanceOfDatas:VueParcData)
		{
			model=instanceOfDatas;
		}

		internal function ViewResultHandler(re:ResultEvent):void
		{
			if (re.result)
			{
				model.setViewParc(re.result as ArrayCollection);
			}
			else
			{
				ConsoviewAlert.afficherError(gestionparcMessages.RECHERCHE_IMPOSSIBLE, gestionparcMessages.ERREUR);
			}
		}
	}
}