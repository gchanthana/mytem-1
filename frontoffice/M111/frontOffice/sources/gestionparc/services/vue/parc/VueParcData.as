package gestionparc.services.vue.parc
{
	import flash.events.EventDispatcher;
	
	import gestionparc.entity.vue.MatriceParcVO;
	import gestionparc.event.gestionparcEvent;
	
	import mx.collections.ArrayCollection;

	/**
	 * Traiter les données reçues aux retours des procedures appelées dans RechercheServices.
	 */
	public class VueParcData extends EventDispatcher
	{
		private var _listeResultat:ArrayCollection = null;
		private var _NB_ROWS:Number;
		
		
		public function VueParcData():void
		{
		}
		
		public function setViewParc(resultObject:ArrayCollection):void
		{
			listeResultat = new ArrayCollection();
			
			for each(var obj:Object in resultObject)
			{
				listeResultat.addItem(createViewParcItemVo(obj));
			}
			
			if(listeResultat.length > 0)
				NB_ROWS = (listeResultat.getItemAt(0) as MatriceParcVO).NB_ROWN;
			else
				NB_ROWS = 0;
			
			listeResultat.refresh();
			
			dispatchEvent(new gestionparcEvent(gestionparcEvent.RECHERCHER_FINISHED,NB_ROWS));
		}
		
		private function createViewParcItemVo(obj:Object):MatriceParcVO
		{
			var item : MatriceParcVO = new MatriceParcVO();
			item.createObject(obj);
			return item;
		}
		
		public function set listeResultat(value:ArrayCollection):void
		{
			_listeResultat = value;
		}
		
		[Bindable] 
		public function get listeResultat():ArrayCollection
		{
			return _listeResultat;
		}

		public function get NB_ROWS():Number
		{
			return _NB_ROWS;
		}

		public function set NB_ROWS(value:Number):void
		{
			_NB_ROWS = value;
		}
	}
}