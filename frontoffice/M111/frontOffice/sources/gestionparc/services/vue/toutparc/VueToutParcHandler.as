package gestionparc.services.vue.toutparc
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;

	public class VueToutParcHandler
	{
		private var model:VueToutParcData
		
		public function VueToutParcHandler(instanceOfDatas:VueToutParcData)
		{
			model=instanceOfDatas;
		}
		
		internal function ViewResultHandler(re:ResultEvent):void
		{
			if (re.result)
			{
				model.setViewParcGlobal(re.result as ArrayCollection);
			}
			else
			{
				ConsoviewAlert.afficherError(gestionparcMessages.RECHERCHE_IMPOSSIBLE, gestionparcMessages.ERREUR);
			}
		}
	}
}