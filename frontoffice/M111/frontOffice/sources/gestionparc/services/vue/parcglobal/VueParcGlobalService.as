package gestionparc.services.vue.parcglobal
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.services.servicelocator.IServiceView;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;

	public class VueParcGlobalService implements IServiceView 
	{
		[Bindable]
		public var model:VueParcGlobalData;
		public var myHandler:VueParcGlobalHandler;
		private var myData: ArrayCollection;

		public function VueParcGlobalService() 
		{
			model=new VueParcGlobalData();
			myHandler=new VueParcGlobalHandler(model);
			myData=new ArrayCollection();
		}

		public function getData(indexDebut:int, nbItem:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.vue.VueParcGlobal", "getVueParcGlobal", myHandler.ViewResultHandler);
			
			RemoteObjectUtil.callService(op, 
				SpecificationVO.POOL_PARC_GLOBAL_ID, 
				SpecificationVO.getInstance().keyWord, 
				SpecificationVO.getInstance().idKeyWordScope,
				SpecificationVO.getInstance().keyWord2, 
				SpecificationVO.getInstance().idKeyWordScope2,
				SpecificationVO.getInstance().labelSort, 
				indexDebut, 
				nbItem,  
				SpecificationVO.getInstance().idRestriction,
				SpecificationVO.getInstance().orderByReq);
		}
		
		public function getModel():EventDispatcher
		{
			return model;
		}
		
		public function restoreData():ArrayCollection
		{
			myData=model.listeResultat;
			return myData;
		}
		public  function getAllnbRow(): Number
		{
			return model.NB_ROWS; 
		}
	}
}