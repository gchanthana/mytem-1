package gestionparc.services.vue.parcglobal
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	/**
	 * Envoie les données reçues à la classe
	 * au retour des procédures appelées dans <code>RechercheServices</code>.
	 */
	public class VueParcGlobalHandler
	{
		private var model:VueParcGlobalData;

		public function VueParcGlobalHandler(instanceOfDatas:VueParcGlobalData)
		{
			model=instanceOfDatas;
		}

		internal function ViewResultHandler(re:ResultEvent):void
		{
			if (re.result)
			{
				model.setViewParcGlobal(re.result as ArrayCollection);
			}
			else
			{
				ConsoviewAlert.afficherError(gestionparcMessages.RECHERCHE_IMPOSSIBLE, gestionparcMessages.ERREUR);
			}
		}
	}
}