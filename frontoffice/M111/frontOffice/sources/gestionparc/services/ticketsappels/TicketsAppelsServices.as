package gestionparc.services.ticketsappels
{
	import composants.util.searchPaginateDatagrid.CriteresRechercheVO;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class TicketsAppelsServices
	{
		public function TicketsAppelsServices()
		{
		}
		
		public var datas:TicketsAppelsDatas = new TicketsAppelsDatas();
		public var handler:TicketsAppelsHandlers = new TicketsAppelsHandlers(datas);
		
		
		public function callSymbol():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M00.connectApp.DeviseManager",
				"getDevise",
				handler.setCurrencySymbol);
			
			RemoteObjectUtil.callService(op);
		}
		
		public function getFicheTicket(idSous_tete:Number,idinventaire_periode:Number,params:composants.util.searchPaginateDatagrid.CriteresRechercheVO):void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheTicket",
				"getTicket",
				handler.getFicheTicketResultHandler);
			
			RemoteObjectUtil.callService(op,idSous_tete,idinventaire_periode,params.orderColonne,params.orderBy,params.searchColonne,params.searchText,params.offset,params.limit);		
		}

		public function getListeFactures(idSous_tete:Number):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.Factures",
				"getListeFactures",
				handler.getListeFacturesHandler);
			
			RemoteObjectUtil.callService(op, idSous_tete);
		}
	}
}