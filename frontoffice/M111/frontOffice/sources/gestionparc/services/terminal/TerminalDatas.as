package gestionparc.services.terminal
{
	import flash.events.EventDispatcher;
	
	import gestionparc.entity.TerminalVO;
	import gestionparc.event.gestionparcEvent;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * Traite les données reçues aux retours des procedures appelées dans <code>TerminalServices</code>.
	 */
	public class TerminalDatas extends EventDispatcher
	{
		
		private var _terminal			: TerminalVO = new TerminalVO();
		private var _listeConstructeur	: ArrayCollection = new ArrayCollection();
		private var _listeModele		: ArrayCollection = new ArrayCollection();
		
		/**
		 * Traite les données reçues au retour de la procedure <code>getInfosFicheTerminal()</code>.
		 * Appel la fonction <code>fillTerminalVO()</code> pour affecter a la data "terminal" toutes les données concernant :
		 * - Ses informations personnelles.
		 * - Ses lignes et SIM.
		 * - Ses incidents.
		 * - Ses contrats.
		 */
		public function treatDataGetInfosFicheTerminal(obj:Object):void
		{
			terminal = new TerminalVO();
			terminal = terminal.fillTerminalVO(obj);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_TERM_LOADED,null,true));
		}
		
		/**
		 * Dispatch un événements pour signaler que la mise à jour des informations
		 * concernant la fiche terminal s'est bien déroulée.
		 */
		public function treatSetInfosFicheTerminal():void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_TERM_UPLOADED));
		}
		
		/**
		 * Dispatch un événements pour signaler que l'enregistrements des informations
		 * concernant la fiche contrat du terminal s'est bien déroulée.
		 * Cette événement transporte l'idContrat (pour les suppressions de contrats)
		 */
		public function treatSetInfosContrat(obj:Object):void
		{
			var idContrat 	: int 	 = (obj[1] as int);
			var myObj 		: Object = new Object();
			myObj.idContrat = idContrat;
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_CONTRAT_SAVED,myObj));
		}
		
		/**
 		 * Traite les données reçues au retour de la procedure <code>getConstructeur()</code>.
		 * Affecte à la data "listeConstructeur" la liste des constructeurs récupérées (seulement ceux dont le TYPE_FOURNISSEUR = 0).
		 */
		public function treatGetConstructeur(obj:Object):void
		{
			var lenListeConstructeur : int = (obj as ArrayCollection).length;
			
			for(var i : int = 0 ; i < lenListeConstructeur ; i++)
			{
				if(obj[i].TYPE_FOURNISSEUR == 0)
				{
					listeConstructeur.addItem(obj[i]);
				}
			}

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_CONSTRU_LOADED));
		}
		
		/**
		 * Traite les données reçues au retour de la procedure <code>getListeModele()</code>.
		 * Affecte à la data "listeModele" la liste des constructeurs récupérées.
		 */
		public function treatGetListeModele(obj:Object):void
		{
			listeModele = new ArrayCollection();
			var lenListeModele : int = listeModele.length;
			var lenResult : int = (obj as ArrayCollection).length;
			var add : Boolean = false;
			
			for(var i : int = 0; i < lenResult; i++)
			{
				if((obj as ArrayCollection)[i].IDTYPE_EQUIPEMENT != 70) // ce n'est pas un terminal
				{
					break;
				}
				
				add = true;
				for(var index : int = 0; index < lenListeModele; index++)
				{
					if(listeModele[index].LIBELLE_EQ == (obj as ArrayCollection)[i].LIBELLE_EQ)
					{
						add = false;
						break;
					}
				}
				
				if(add)
				{
					listeModele.addItem((obj as ArrayCollection)[i]);
					lenListeModele = listeModele.length;
				}
			}
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_MODELES_LOADED));
		}
		public function treatAddNewModele(obj:int):void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.ADDNEW_MODELE_LOADED,obj));
		}
		
		public function set terminal(value:TerminalVO):void
		{
			_terminal = value;
		}
		
		public function get terminal():TerminalVO
		{
			return _terminal;
		}
		
		public function set listeConstructeur(value:ArrayCollection):void
		{
			_listeConstructeur = value;
		}

		public function get listeConstructeur():ArrayCollection
		{
			return _listeConstructeur;
		}

		public function set listeModele(value:ArrayCollection):void
		{
			_listeModele = value;
		}

		public function get listeModele():ArrayCollection
		{
			return _listeModele;
		}
	}
}