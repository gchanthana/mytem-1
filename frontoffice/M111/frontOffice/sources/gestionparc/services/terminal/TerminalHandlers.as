package gestionparc.services.terminal
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	public class TerminalHandlers
	{
		
		private var myDatas : TerminalDatas;
		
		public function TerminalHandlers(instanceOfDatas:TerminalDatas)
		{
			myDatas = instanceOfDatas;
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>getInfosFicheTerminal()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetInfosFicheTerminal()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getInfosFicheTerminalResultHandler(re:ResultEvent):void
		{
			if((re.result is Array) && (re.result as Array)[0] == 1)
			{
				myDatas.treatDataGetInfosFicheTerminal(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_FICHE_TERM_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}	
		
		/**
		 * Fonction appellée au retour de la fonction <code>setInfosFicheTerminal()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatSetInfosFicheTerminal()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function setInfosFicheTerminalResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatSetInfosFicheTerminal();
			}
			else if(re.result == -2)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.NUMERO_IMEI_EXISTANT,
					gestionparcMessages.ERREUR);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.UPDATE_FICHE_TERM_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>setInfosContrat()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatSetInfosContrat()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function setInfosContratResultHandler(re:ResultEvent):void
		{
			if((re.result as Array).length > 0 && (re.result as Array)[0] > 0)
			{
				myDatas.treatSetInfosContrat(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.SAVE_INFOS_CONTRAT_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>deleteContrat()</code> (appel de procédure).
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function deleteContratResultHandler(re:ResultEvent):void
		{
			if(re.result < 0)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.DELETE_CONTRAT_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>getConstructeur()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatGetConstructeur()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getConstructeurResultHandler(re:ResultEvent):void
		{
			if((re.result as ArrayCollection).length > 0)
			{
				myDatas.treatGetConstructeur(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_CONSTRU_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>getListeModele()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatGetListeModele()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getListeModeleResultHandler(re:ResultEvent):void
		{
			if((re.result as ArrayCollection).length  > 0)
			{
				myDatas.treatGetListeModele(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_MODELES_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		internal function AddNewModeleResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAddNewModele(parseInt(re.result.toString()));
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_MODELES_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
	}
}