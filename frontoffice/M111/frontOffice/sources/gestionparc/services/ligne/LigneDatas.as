package gestionparc.services.ligne
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.core.Application;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import gestionparc.entity.LigneVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.gestionparcEvent;
	
	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Traite les données reçues aux retours des procedures appelées dans <code>LigneServices</code>.
	 * 
	 * Auteur	: Guiou Nicolas
	 * Version 	: 1.0
	 * Date 	: 31.08.2010
	 * </pre></p>
	 * 
	 * @see gestionparc.services.ligne.LigneServices
	 * 
	 */
	public class LigneDatas extends EventDispatcher
	{
		// VARIABLES------------------------------------------------------------------------------
		
		private var _ligne						: LigneVO = new LigneVO();
		private var _listeAbo					: ArrayCollection = null;
		private var _listeOpt					: ArrayCollection = null;
		private var _listeAboFavoris			: ArrayCollection = null;
		private var _listeOptFavoris			: ArrayCollection = null;
		private var _listeTypeAbo				: ArrayCollection = null;
		private var _listeTypeOpt				: ArrayCollection = null;
		
		private var _listeLignesSimSsTerm		: ArrayCollection = null;
		private var _listeLignesFixeSsEqp		: ArrayCollection = null;
		private var _listActionRaccordement		: ArrayCollection = null;
		private var _listeLignesDispoPourGrpment: ArrayCollection = null;
		private var _listeLignesSsTerm			: ArrayCollection = null;
		private  var _row						: AbstractMatriceParcVO = null;
		//private var _listeDestinataires			:ArrayCollection = new ArrayCollection();
		public var  listeEmails					:String = '';
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Traite les données reçues au retour de la procedure <code>getInfosFicheLigne</code>.
		 * Appel la fonction <code>fillLigneVO()</code> pour affecter a la data "ligne" toutes les données concernant :
		 * - Les informations de la ligne.
		 * - Ses abo et options.
		 * - Sa SIM.
		 * - Ses organisations.
		 * </pre></p>
		 * 
		 * @param obj
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#getInfosFicheLigne()
		 * @see gestionparc.entity.LigneVO()
		 * 
		 */	
		public function treatDataGetInfosFicheLigne(obj:Object):void
		{
			ligne = new LigneVO();
			ligne = ligne.fillLigneVO(obj);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_LIGNE_LOADED));
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Dispatch un événements pour signaler que la mise à jour des informations
		 * concernant la fiche ligne s'est bien déroulée.
		 * </pre></p>
		 * 
		 * @return void
		 * 
		 */
		public function treatSetInfosFicheLigne():void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_LIGNE_UPLOADED));
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Dispatch un événements pour signaler que la mise à jour des informations
		 * concernant les listes des abonnements et options s'est bien déroulée.
		 * </pre></p>
		 * 
		 * @param obj
		 * @return void
		 * 
		 */
		public function treatDataFournirListeAbonnementsOptions(obj:Object):void
		{
			listeAbo = new ArrayCollection();
			listeAboFavoris = new ArrayCollection();
			listeOpt = new ArrayCollection();
			listeOptFavoris = new ArrayCollection();
			listeTypeOpt = new ArrayCollection();
			listeTypeAbo = new ArrayCollection();
			
			var abo_opt:ArrayCollection = obj as ArrayCollection;
			var longueur : int = abo_opt.length;
			
			for(var i : int = 0 ; i < longueur ; i++)
			{
				// ajout de l'attribut SELECTED
				abo_opt[i].SELECTED = false;
				
				if(abo_opt[i].BOOL_ACCES == 1 )
				{
					listeAbo.addItem(abo_opt[i]);
					if(abo_opt[i].FAVORI == 1)
					{
						listeAboFavoris.addItem(abo_opt[i]);
					}
					
					var exist:Boolean = false;
					for each(var o:Object in listeTypeAbo)
					{
						if(o.ID == abo_opt[i].IDTHEME_PRODUIT)
						{
							exist = true;
							break;
						}
					}
					if(!exist)
					{
						var obj:Object = new Object();
						obj.ID = abo_opt[i].IDTHEME_PRODUIT;
						obj.LIBELLE = abo_opt[i].THEME_LIBELLE;
						obj.SELECTED = true;
						listeTypeAbo.addItem(obj);
					}
				}
				else if(abo_opt[i].BOOL_ACCES == 0) 
				{
					listeOpt.addItem(abo_opt[i]);
					if(abo_opt[i].FAVORI == 1)
					{
						listeOptFavoris.addItem(abo_opt[i]);
					}
					for each(var o2:Object in listeTypeOpt)
					{
						if(o2.ID == abo_opt[i].IDTHEME_PRODUIT)
						{
							exist = true;
							break;
						}
					}
					if(!exist)
					{
						var obj1:Object = new Object();
						obj1.ID = abo_opt[i].IDTHEME_PRODUIT;
						obj1.LIBELLE = abo_opt[i].THEME_LIBELLE;
						obj1.SELECTED = true;
						listeTypeOpt.addItem(obj1);
					}
				}
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTED_ABO_OPT_LOADED));
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Dispatch un événements pour signaler que la mise à jour de la listes
		 * des lignes/sim sans terminaux s'est bien déroulée.
		 * </pre></p>
		 * 
		 * @param obj
		 * @return void
		 */
		public function treatDataListeLigneSimSansTerm(obj:Object):void
		{
			listeLignesSimSsTerm = new ArrayCollection();
			listeLignesSimSsTerm = (obj as ArrayCollection);

			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_LIGNE_SIM_SS_TERM_LOADED));
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Dispatch un événements pour signaler que la mise à jour de la listes
		 * des lignes/sim sans terminaux s'est bien déroulée.
		 * </pre></p>
		 * 
		 * @param obj
		 * @return void
		 */
		public function treatDataListeLigneFixeSansEqp(obj:Object):void
		{
			listeLignesFixeSsEqp = new ArrayCollection();
			listeLignesFixeSsEqp = (obj as ArrayCollection);
			
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_LIGNE_FIXE_SS_EQP_LOADED));
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Dispatch un événements pour signaler que la mise à jour de la listes
		 * des lignes sans équipements s'est bien déroulée.
		 * </pre></p>
		 * 
		 * @param obj
		 * @return void
		 */
		public function treatDataListeLigneSansTerm(obj:Object):void
		{
			listeLignesSsEqp = new ArrayCollection();
			listeLignesSsEqp = (obj as ArrayCollection);
			
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_LIGNE_SS_TERM_LOADED));
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Traite les données reçues au retour de la procedure <code>majColFacture</code>.
		 * Appel la fonction <code>fillColFacureLigneVO()</code> pour mettre a jour les donnees des abo et opt avec les valeurs de colonne facture
		 * </pre></p>
		 * 
		 * @param obj
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#majColFacture()
		 */	
		public function treatDataMajColFacture(obj:Object):void
		{
			ligne = ligne.fillColFacureLigneVO(ligne,obj);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_COL_FACTURE_LOADED));
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Traite les données reçues au retour de la procedure <code>getActionsPossiblesByEtat()</code>.
		 * Affecte à la data "listActionRaccordement" la liste des actions de raccordement récupérées.
		 * </pre></p>
		 * 
		 * @param obj
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#getActionsPossiblesByEtat()
		 */
		public function treatDataGetActionsPossiblesByEtat(obj:Object):void
		{
			listActionRaccordement = new ArrayCollection();
			var lenListActionRaccordement : int = (obj as ArrayCollection).length;
			
			for(var i : int = 0 ; i  < lenListActionRaccordement ; i++)
			{
				// 3 = suspendue, 4 = Resilier on ne traite pas ces actions ici
				if(obj[i].IDRACCO_ACTION != 3 && obj[i].IDRACCO_ACTION != 4)
				{
					listActionRaccordement.addItem(obj[i]);
				}
			}
			
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_ACT_RACCOR_LOADED));
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Dispatch un événements pour signaler que la mise à jour du raccordement s'est bien déroulée.
		 * </pre></p>
		 * 
		 * @return void
		 */
		public function treatDataDoActionRaccordement():void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.RACCOR_LIGNE_UPLOADED));
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Dispatch un événements pour signaler que la mise à jour de 
		 * la liste des lignes disponibles pour un groupement s'est bien déroulée.
		 * </pre></p>
		 * 
		 * @param obj
		 * @return void
		 */
		public function treatDataGetTeteLigne(obj:Object):void
		{
			listeLignesDispoPourGrpment = new ArrayCollection();
			listeLignesDispoPourGrpment = (obj as ArrayCollection);
			
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_LINE_DISPO_GRPMENT));
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Dispatch un événements pour signaler que la mise à jour de la listes
		 * des lignes/sim sans terminaux s'est bien déroulée.
		 * </pre></p>
		 * 
		 * @param obj
		 * @return void
		 */
		public function treatDataGetLignesGroupement(obj:Object):void
		{
		}
		public function treatDataresilierLigne(obj:Object):void
		{
			dispatchEvent(new gestionparcEvent(gestionparcEvent.ASSOCIATION_REALIZED));
		}
		public function treatDatarenumeroterLigne(obj:Object):void
		{
			SpecificationVO.getInstance().boolHasChanged = true;
			SpecificationVO.getInstance().refreshData();
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString("M111","Votre_ancienne_Ligne_est_maintenant_au_rebut")
											,Application.application as DisplayObject)
			dispatchEvent(new gestionparcEvent(gestionparcEvent.ASSOCIATION_REALIZED));
		}
		
		public function updateEtatLigne(response:Object):void
		{
			row.ST_IDETAT = response.result;
			
			
			var message:String = ".";
			
			switch(row.ST_IDETAT)
			{
				case 1: {
					message = ResourceManager.getInstance().getString('M111', 'Activ_e');
					break;
				}
				case 2: {
					message = ResourceManager.getInstance().getString('M111', 'Suspendue');
					break;
				}
				case 3: {
					message = ResourceManager.getInstance().getString('M111', 'R_sili_');
					break;
				}
			}
			
			ConsoviewAlert.afficherOKImage(message,
				Application.application as DisplayObject)
			dispatchEvent(new gestionparcEvent(gestionparcEvent.ASSOCIATION_REALIZED));
		}
		
		public function treatGetListeDestinatairesEchangeSIM(response:ArrayCollection):void
		{
			//listeDestinataires.source = [];
			var long:int = response.length;
			
			listeEmails= '';
			for(var i:int = 0; i < long; i++)
			{
				listeEmails = listeEmails + response[i].EMAIL;
				if(i < long-1)
					listeEmails = listeEmails + ',';
						
			}
			
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_DESTINA_SIM_STOCK));
		}
		
		public function treatMessageEchangeSim():void
		{
			dispatchEvent(new gestionparcEvent(gestionparcEvent.MESSAGE_ECHANGE_SIM_SENDED));
		}
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		// PUBLICS PROPERTIES (GETTER/SETTER)-----------------------------------------------------
		
		public function set ligne(value:LigneVO):void
		{
			_ligne = value;
		}
		
		public function get ligne():LigneVO
		{
			return _ligne;
		}

		public function set listeAbo(value:ArrayCollection):void
		{
			_listeAbo = value;
		}
		
		public function get listeAbo():ArrayCollection
		{
			return _listeAbo;
		}
		
		public function set listeOpt(value:ArrayCollection):void
		{
			_listeOpt = value;
		}
		
		public function get listeOpt():ArrayCollection
		{
			return _listeOpt;
		}
		
		public function set listeAboFavoris(value:ArrayCollection):void
		{
			_listeAboFavoris = value;
		}
		
		public function get listeAboFavoris():ArrayCollection
		{
			return _listeAboFavoris;
		}
		
		public function set listeOptFavoris(value:ArrayCollection):void
		{
			_listeOptFavoris = value;
		}
		
		public function get listeOptFavoris():ArrayCollection
		{
			return _listeOptFavoris;
		}
		
		public function set listeLignesSimSsTerm(value:ArrayCollection):void
		{
			_listeLignesSimSsTerm = value;
		}
		
		public function get listeLignesSimSsTerm():ArrayCollection
		{
			return _listeLignesSimSsTerm;
		}
				
		public function set listActionRaccordement(value:ArrayCollection):void
		{
			_listActionRaccordement = value;
		}
		
		public function get listActionRaccordement():ArrayCollection
		{
			return _listActionRaccordement;
		}
		
		public function set listeLignesDispoPourGrpment(value:ArrayCollection):void
		{
			_listeLignesDispoPourGrpment = value;
		}
		
		public function get listeLignesDispoPourGrpment():ArrayCollection
		{
			return _listeLignesDispoPourGrpment;
		}

		public function get listeLignesSsEqp():ArrayCollection
		{
			return _listeLignesSsTerm;
		}

		public function set listeLignesSsEqp(value:ArrayCollection):void
		{
			_listeLignesSsTerm = value;
		}

		public function get listeLignesFixeSsEqp():ArrayCollection
		{
			return _listeLignesFixeSsEqp;
		}

		public function set listeLignesFixeSsEqp(value:ArrayCollection):void
		{
			_listeLignesFixeSsEqp = value;
		}

		public function set listeTypeAbo(value:ArrayCollection):void
		{
			_listeTypeAbo = value;
		}
		
		public function get listeTypeAbo():ArrayCollection
		{
			return _listeTypeAbo;
		}
		public function set listeTypeOpt(value:ArrayCollection):void
		{
			_listeTypeOpt = value;
		}
		
		public function get listeTypeOpt():ArrayCollection
		{
			return _listeTypeOpt;
		}

		public function get row():AbstractMatriceParcVO
		{
			return _row;
		}

		public function set row(value:AbstractMatriceParcVO):void
		{
			_row = value;
		}
		
		/*public function get listeDestinataires():ArrayCollection { return _listeDestinataires; }
		
		public function set listeDestinataires(value:ArrayCollection):void
		{
			if (_listeDestinataires == value)
				return;
			_listeDestinataires = value;
		}*/

		
		// PUBLICS PROPERTIES (GETTER/SETTER)-----------------------------------------------------
		
	}
}