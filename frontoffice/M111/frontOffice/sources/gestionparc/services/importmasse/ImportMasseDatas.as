package gestionparc.services.importmasse
{
	import flash.events.EventDispatcher;
	
	import gestionparc.event.ImportDeMasseEvent;
	
	import mx.collections.ArrayCollection;
	
	public class ImportMasseDatas extends EventDispatcher
	{
		
		//--------------- VARIABLES ----------------//
			
		private var _listeOperateur		: ArrayCollection = new ArrayCollection();
		private var _listeRevendeur		: ArrayCollection = new ArrayCollection();
		private var _listeModele		: ArrayCollection = new ArrayCollection();
		
		
		//--------------- METHODES ----------------//
		
		/* */
		public function ImportMasseDatas()
		{}
		
		/* */
		public function treatCheckCollab(ret:Object):void
		{
			this.dispatchEvent(new ImportDeMasseEvent(ImportDeMasseEvent.CHECKED_COLLAB,ret));
		}
		
		/* */
		public function treatCheckIMEI(ret:Object):void
		{
			this.dispatchEvent(new ImportDeMasseEvent(ImportDeMasseEvent.CHECKED_IMEI,ret));
		}
		
		/* */
		public function treatCheckNSERIE(ret:Object):void
		{
			this.dispatchEvent(new ImportDeMasseEvent(ImportDeMasseEvent.CHECKED_NUMSERIE,ret));
		}
		
		/* */
		public function treatCheckNEXTENSION(ret:Object):void
		{
			this.dispatchEvent(new ImportDeMasseEvent(ImportDeMasseEvent.CHECKED_NUMEXTENSION,ret));
		}
		
		/* */
		public function treatCheckNSIM(ret:Object):void
		{
			this.dispatchEvent(new ImportDeMasseEvent(ImportDeMasseEvent.CHECKED_NUMSIM,ret));
		}
		
		/* */
		public function treatCheckNLIGNE(ret:Object):void
		{
			this.dispatchEvent(new ImportDeMasseEvent(ImportDeMasseEvent.CHECKED_NUMLIGNE,ret));
		}
		
		/* */
		public function treatImporterFichier(ret:Object):void
		{
			this.dispatchEvent(new ImportDeMasseEvent(ImportDeMasseEvent.IMPORT_DONE,ret));
		}
		
		
		//--------------- GETTERS - SETTERS ----------------//

		public function set listeOperateur(value:ArrayCollection):void
		{
			_listeOperateur = value;
		}

		public function get listeOperateur():ArrayCollection
		{
			return _listeOperateur;
		}
		
		public function set listeRevendeur(value:ArrayCollection):void
		{
			_listeRevendeur = value;
		}

		public function get listeRevendeur():ArrayCollection
		{
			return _listeRevendeur;
		}

		public function set listeModele(value:ArrayCollection):void
		{
			_listeModele = value;
		}

		public function get listeModele():ArrayCollection
		{
			return _listeModele;
		}
	}
}