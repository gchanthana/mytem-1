package gestionparc.services.associer
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	
	import mx.core.Application;
	import mx.resources.ResourceManager;
	
	public class ActionAssocierDatas extends EventDispatcher
	{
		public function treatAssociationRealized():void
		{
			SpecificationVO.getInstance().boolHasChanged = true;
			SpecificationVO.getInstance().refreshData();
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString("M111","ACTION_REALIZED"),Application.application as DisplayObject);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.ASSOCIATION_REALIZED));
		}
	}
}