package gestionparc.services.associer
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.rpc.events.ResultEvent;

	public class ActionAssocierHandlers
	{
		
		private var myDatas : ActionAssocierDatas;
		
		public function ActionAssocierHandlers(instanceOfDatas:ActionAssocierDatas)
		{
			myDatas = instanceOfDatas;
		}
					
		 /* EQUIPEMENT */
		internal function associerFournirEqp1ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28
				)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerFournirEqp2ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerFournirEqp3ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerFournirEqp4ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		/* LIGNE SIM */
		internal function associerFournirLigneSim1ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function associerFournirLigneExtResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
						gestionparcMessages.ASSOC_COLLAB_LIGNE_EXT_IMP,
						gestionparcMessages.ERREUR);
			}
		}
		
		internal function associerFournirLigneSim2ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		
		internal function associerFournirLigneSim3ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		/* LIGNE */
		internal function associerFournirLigne1ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerFournirLigne2ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerFournirLigne3ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
	/*
		*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
							ASSOCIER A UN EQP
		*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
	*/
		internal function associerCollabToEqpResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerLigneSimToEqpResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function associerLigneExtToEqpResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
						gestionparcMessages.ASSOC_LIGNE_EXT_EQP_IMP,
						gestionparcMessages.ERREUR);
			}
		}
		
		internal function associerLigneToEqpResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerSousEqpToEqpResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}

		internal function associerCollabToLigne1ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0 )
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerCollabToLigne2ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerCollabToLigne3ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerCollabToLigne4ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerEqpToLigneResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function associerExtToLigneResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_LIGNE_EXT_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function associerSimToLigneResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
						gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
						gestionparcMessages.ERREUR);
			}
		}
			
			
		
		internal function associerCollabToSim1ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function associerCollabToExtResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
						gestionparcMessages.ASSOC_COLLAB_EXT_IMP,
						gestionparcMessages.ERREUR);
			}
		}
		
		
		internal function associerCollabToSim2ResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function associerLigneToSimResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		
		internal function associerLigneToExtResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
						gestionparcMessages.ASSOC_LIGNE_EXT_IMP,
						gestionparcMessages.ERREUR);
			}
		}
		
		
		internal function associerEqpToSimResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
						gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
						gestionparcMessages.ERREUR);
			}
		}
		
		internal function associerEqpToExtResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatAssociationRealized();
			}
			else
			{
				if(re.result == -25 
					|| re.result == -26
					|| re.result == -27 || re.result == -28)
					afficherErreurPool(parseInt(re.result.toString()));
				else				
					ConsoviewAlert.afficherError(
						gestionparcMessages.ASSOC_EQP_EXT_IMP,
						gestionparcMessages.ERREUR);
			}
		}
		
		private function afficherErreurPool(value:int):void
		{
			switch(value)
			{
				case -25:
					ConsoviewAlert.afficherError(
						gestionparcMessages.ASSOC_ERROR_EQPT_CALLAB_POOL,
						gestionparcMessages.ERREUR);
					break;
				case -26:
					ConsoviewAlert.afficherError(
						gestionparcMessages.ASSOC_ERROR_EQPT_EQPT_POOL,
						gestionparcMessages.ERREUR);
					break;
				case -27:
					ConsoviewAlert.afficherError(
						gestionparcMessages.ASSOC_ERROR_LIGNE_SIM_POOL,
						gestionparcMessages.ERREUR);
					break;
				case -28:
					ConsoviewAlert.afficherError(
						gestionparcMessages.ASSOC_ERROR_LIGNE_COLLAB_POOL,
						gestionparcMessages.ERREUR);
					break;
			}
		}
	}
}