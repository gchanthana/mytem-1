package gestionparc.services.collaborateur
{
	import flash.events.EventDispatcher;
	
	import mx.rpc.AbstractOperation;
	
	import composants.util.DateFunction;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.SpecificationVO;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Appelle les procédures liées aux collaborateurs.
	 * 
	 * Auteur	: Guiou Nicolas
	 * Version 	: 1.0
	 * Date 	: 15.07.2010
	 * </pre></p>
	 * 
	 */
	public class CollaborateurServices extends EventDispatcher
	{
		
		[Bindable] public var myDatas 		: CollaborateurDatas;
		public var myHandlers 	: CollaborateurHandlers;
		

		public function CollaborateurServices()
		{
			myDatas 	= new CollaborateurDatas();
			myHandlers 	= new CollaborateurHandlers(myDatas);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer :
		 * - Tous les informations personnelles du collaborateurs.
		 * - Tous les terminaux, lignes et SIM du collaborateurs.
		 * - Tous les libellés des champs personnels du collaborateurs.
		 * - Toutes les informations liées aux organisations du collaborateurs.
		 */	
		public function getInfosFicheCollaborateur(createObjToServiceGet : Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCollaborateur",
				"getInfosFicheCollaborateur",
				myHandlers.getInfosFicheCollaborateurResultHandler);
			RemoteObjectUtil.callService(op, createObjToServiceGet.idEmploye, createObjToServiceGet.idPool,createObjToServiceGet.id);	
		}
		
		/**
		 * Appelle la procédure permettant de mettre à jour :
		 * - Tous les informations personnelles du collaborateurs.
		 * - Toutes les informations liées aux organisations du collaborateurs.
		 * - Les informations du Manager
		 */	
		public function setInfosFicheCollaborateur(createObjToServiceSet : Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCollaborateur",
				"setInfosFicheCollaborateur",
				myHandlers.setInfosFicheCollaborateurResultHandler);
			
			RemoteObjectUtil.callService(op, 
				createObjToServiceSet.myCollaborateur.idGpeClient,
				createObjToServiceSet.myCollaborateur.idEmploye,
				createObjToServiceSet.myCollaborateur.idSite,
				createObjToServiceSet.myCollaborateur.cleIdentifiant,
				createObjToServiceSet.myCollaborateur.idCivilite,
				createObjToServiceSet.myCollaborateur.nom,
				createObjToServiceSet.myCollaborateur.prenom,
				createObjToServiceSet.myCollaborateur.email,
				createObjToServiceSet.myCollaborateur.fonction,
				createObjToServiceSet.myCollaborateur.statut,
				createObjToServiceSet.myCollaborateur.commentaires,
				createObjToServiceSet.myCollaborateur.codeInterne,
				createObjToServiceSet.myCollaborateur.reference,
				createObjToServiceSet.myCollaborateur.matricule,
				createObjToServiceSet.myCollaborateur.dansSociete,
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myCollaborateur.dateEntree),
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myCollaborateur.dateSortie),
				createObjToServiceSet.myCollaborateur.texteChampPerso1,
				createObjToServiceSet.myCollaborateur.texteChampPerso2,
				createObjToServiceSet.myCollaborateur.texteChampPerso3,
				createObjToServiceSet.myCollaborateur.texteChampPerso4,
				createObjToServiceSet.myCollaborateur.niveau,
				createObjToServiceSet.xmlListeOrga,
				SpecificationVO.getInstance().elementDataGridSelected.ID,
				createObjToServiceSet.myCollaborateur.idManager,		
				createObjToServiceSet.insertPoolDistrib,
				createObjToServiceSet.insertPoolSociete);
			
			
		}
		
		/**
		 * Appelle la procédure permettant de mettre à jour : cette fonction est utilisée seulement qu'on attribue un nouveau Manager ou on change un Manager d'un collaborateur
		 * - Tous les informations personnelles du collaborateurs.
		 * - Toutes les informations liées aux organisations du collaborateurs.
		 * - Les informations du Manager
		 */	
		public function setInfosManagerFicheCollaborateur(createObjToServiceSet : Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCollaborateur",
				"setInfosFicheCollaborateur",
				myHandlers.setInfosManagerFicheCollaborateurResultHandler);
			
			RemoteObjectUtil.callService(op, 
				createObjToServiceSet.myCollaborateur.idGpeClient,
				createObjToServiceSet.myCollaborateur.idEmploye,
				createObjToServiceSet.myCollaborateur.idSite,
				createObjToServiceSet.myCollaborateur.cleIdentifiant,
				createObjToServiceSet.myCollaborateur.idCivilite,
				createObjToServiceSet.myCollaborateur.nom,
				createObjToServiceSet.myCollaborateur.prenom,
				createObjToServiceSet.myCollaborateur.email,
				createObjToServiceSet.myCollaborateur.fonction,
				createObjToServiceSet.myCollaborateur.statut,
				createObjToServiceSet.myCollaborateur.commentaires,
				createObjToServiceSet.myCollaborateur.codeInterne,
				createObjToServiceSet.myCollaborateur.reference,
				createObjToServiceSet.myCollaborateur.matricule,
				createObjToServiceSet.myCollaborateur.dansSociete,
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myCollaborateur.dateEntree),
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myCollaborateur.dateSortie),
				createObjToServiceSet.myCollaborateur.texteChampPerso1,
				createObjToServiceSet.myCollaborateur.texteChampPerso2,
				createObjToServiceSet.myCollaborateur.texteChampPerso3,
				createObjToServiceSet.myCollaborateur.texteChampPerso4,
				createObjToServiceSet.myCollaborateur.niveau,
				createObjToServiceSet.xmlListeOrga,
				SpecificationVO.getInstance().elementDataGridSelected.ID,
				createObjToServiceSet.myCollaborateur.idManager,		
				createObjToServiceSet.insertPoolDistrib,
				createObjToServiceSet.insertPoolSociete);
			
			
		}
		
		/**
		 * Appelle la procédure permettant d'ajouter un collaborateur.
		 */	
		public function addNewCollaborateur(createObjToServiceSet : Object):void
		{
			var idPool : Number = 0;
			
			if(SpecificationVO.getInstance().idPool > 0)
			{
				idPool = SpecificationVO.getInstance().idPool;	
			}
			else if(SpecificationVO.getInstance().idPoolNotValid > 0)
			{
				idPool = SpecificationVO.getInstance().idPoolNotValid;	
			}
			else
			{
				idPool = 0;
			}
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCollaborateur",
				"addNewCollaborateur",
				myHandlers.addNewCollaborateurResultHandler);
			
			RemoteObjectUtil.callService(op, 
				idPool,
				createObjToServiceSet.myCollaborateur.idSite,
				createObjToServiceSet.myCollaborateur.cleIdentifiant,
				createObjToServiceSet.myCollaborateur.idCivilite,
				createObjToServiceSet.myCollaborateur.nom,
				createObjToServiceSet.myCollaborateur.prenom,
				createObjToServiceSet.myCollaborateur.email,
				createObjToServiceSet.myCollaborateur.fonction,
				createObjToServiceSet.myCollaborateur.statut,
				createObjToServiceSet.myCollaborateur.commentaires,
				createObjToServiceSet.myCollaborateur.codeInterne,
				createObjToServiceSet.myCollaborateur.reference,
				createObjToServiceSet.myCollaborateur.matricule,
				createObjToServiceSet.myCollaborateur.dansSociete,
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myCollaborateur.dateEntree),
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myCollaborateur.dateSortie),
				createObjToServiceSet.myCollaborateur.texteChampPerso1,
				createObjToServiceSet.myCollaborateur.texteChampPerso2,
				createObjToServiceSet.myCollaborateur.texteChampPerso3,
				createObjToServiceSet.myCollaborateur.texteChampPerso4,
				createObjToServiceSet.myCollaborateur.niveau,
				createObjToServiceSet.xmlListeOrga,
				createObjToServiceSet.insertPoolDistrib,
				createObjToServiceSet.insertPoolSociete);
		}
		
		public function initDepartEntreprise(idcollab:Number, idpool:Number):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCollaborateur",
				"initdepartEntreprise",
				myHandlers.initDepartEntrepriseResultHandler);
			RemoteObjectUtil.callService(op,idcollab,idpool);
		}
		public function validerDepartEntreprise(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCollaborateur",
				"validerdepartEntreprise",
				myHandlers.validerDepartEntrepriseResultHandler);
			RemoteObjectUtil.callService(op,obj);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer liste des infos 
		 * sur les liaisons Ligne-Sim-Terminal associé a un collaborateur.
		 */	
		public function getListLigneSimEqptFromCollab(idEmploye : int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCollaborateur",
				"getListLigneSimEqptFromCollab",
				myHandlers.getListLigneSimEqptFromCollabResultHandler);
			
			RemoteObjectUtil.callService(op, idEmploye);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer liste des équipements associés a un collaborateur.
		 */	
		public function getListEqptFromCollab(idEmploye : int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCollaborateur",
				"getListEqptFromCollab",
				myHandlers.getListEqptFromCollabResultHandler);
			
			RemoteObjectUtil.callService(op, idEmploye);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer le matricule automatique du client (racine).
		 */	
		public function getMatriculeAuto():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCollaborateur",
				"getMatriculeAuto",
				myHandlers.getMatriculeAutoResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer la liste des managers potentiels pour un collaborateur sur une racine donnée.
		 */	
		public function getListeManagers(indexDebut:Number, nbItems:Number, searchText:String = ''):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCollaborateur",
				"getListManagers",
				myHandlers.getManagersResultHandler);
			
			RemoteObjectUtil.callService(op, 
											indexDebut,
											nbItems,						 
											searchText
										 );
		}
		
		/**
		 * Récupère la liste des valeurs des champs personnalisées
		 */
		public function getValeursChampPersos(idPool:Number, champs:String):void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCollaborateur",
				"getValeursChampPersos",
				myHandlers.getValeursResultHandlers);
			RemoteObjectUtil.callService(op, idPool, champs);
		}
		
	}
}