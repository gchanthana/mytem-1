package gestionparc.services.historique
{
	import flash.events.EventDispatcher;
	
	import gestionparc.entity.HistoriqueVO;
	import gestionparc.event.gestionparcEvent;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Traite les données reçues aux retours des procedures appelées dans <code>HistoriqueServices</code>.
	 * 
	 * Auteur	: Guiou Nicolas
	 * Version 	: 1.0
	 * Date 	: 20.08.2010
	 * </pre></p>
	 * 
	 * @see gestionparc.services.historique.HistoriqueServices
	 * 
	 */
	public class HistoriqueDatas extends EventDispatcher
	{
		// VARIABLES------------------------------------------------------------------------------
		
		private var _listHistorique		: ArrayCollection = new ArrayCollection();
		
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Traite les données reçues au retour de la procedure <code>getHistorique</code>.
		 * Affecte à la data "listHistorique" un <code>HistoriqueVO</code> avec toutes les données concernant l'historique d'une entitée.
		 * </pre></p>
		 * 
		 * @param obj:<code>Object</code>.
		 * @return void.
		 * 
		 * @see gestionparc.services.historique.HistoriqueServices#getHistorique()
		 * @see gestionparc.entity.HistoriqueVO()
		 * 
		 */	
		public function treatDataGetHistorique(obj:Object):void
		{
			var lenListHisot : int = (obj as ArrayCollection).length;
			
			for(var i : int = 0 ; i < lenListHisot ; i++)
			{
				var myNewHisto : HistoriqueVO = new HistoriqueVO();
				listHistorique.addItem(myNewHisto.fillHistoriqueFiche(obj[i]));
			}
			
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_HISTO_LOADED));
		}
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Traite les données reçues au retour de la procedure <code>getHistoriqueActionRessource</code>.
		 * Affecte à la data "listHistorique" un <code>HistoriqueVO</code> avec toutes les données concernant l'historique d'une entitée.
		 * </pre></p>
		 * 
		 * @param obj:<code>Object</code>.
		 * @return void.
		 * 
		 * @see gestionparc.services.historique.HistoriqueServices#getHistoriqueActionRessource()
		 * @see gestionparc.entity.HistoriqueVO()
		 * 
		 */	
		public function treatDataGetHistoriqueActionRessource(obj:Object):void
		{
			var lenListHisot : int = (obj as ArrayCollection).length;
			
			for(var i : int = 0 ; i < lenListHisot ; i++)
			{
				var myNewHisto : HistoriqueVO = new HistoriqueVO();
				listHistorique.addItem(myNewHisto.fillHistoriqueVO(obj[i]));
			}
			
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_HISTO_LOADED));
		}
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		// PUBLICS PROPERTIES (GETTER/SETTER)-----------------------------------------------------
		
		public function set listHistorique(value:ArrayCollection):void
		{
			_listHistorique = value;
		}
		
		public function get listHistorique():ArrayCollection
		{
			return _listHistorique;
		}
		
		// PUBLICS PROPERTIES (GETTER/SETTER)-----------------------------------------------------
		
	}
}