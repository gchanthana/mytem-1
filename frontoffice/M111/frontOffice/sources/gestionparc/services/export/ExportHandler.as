package gestionparc.services.export
{
	import composants.util.ConsoviewAlert;
	
	import mx.controls.Alert;
	import mx.rpc.events.ResultEvent;

	public class ExportHandler
	{		
		public function ExportHandler()
		{
		}
		
		public function getExportInventaireHandler(event:ResultEvent):void
		{
			if(event.result!=-1)
			{
				ConsoviewAlert.afficherOKImage("Votre demande de rapport est traitée");
			}
			else
			{
				Alert.show("Votre demande de rapport n'a pu être traitée");
			}
		}
		
		public function getExportDetailAppelHandler(event:ResultEvent):void
		{
			if(event.result!=-1)
			{
				ConsoviewAlert.afficherOKImage("Votre demande de rapport est traitée");
			}
			else
			{
				Alert.show("Votre demande de rapport n'a pu être traitée");
			}
		}
		
	}
}