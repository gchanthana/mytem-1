package gestionparc.services.getcolonnes
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	
	import gestionparc.entity.Colonne;
	import gestionparc.entity.vue.MatriceParcVO;
	import gestionparc.event.GetColonneEvent;
	import gestionparc.event.gestionparcEvent;

	/**
	 * Traiter les données reçues aux retours des procedures appelées dans RechercheServices.
	 */
	public class GetColonneData extends EventDispatcher
	{
		private var _listeResultat:ArrayCollection=null;

		public function GetColonneData():void
		{
			_listeResultat=new ArrayCollection();
		}

		public function setColonne(resultObject:ArrayCollection):void
		{
			
			_listeResultat.removeAll();
			for each(var obj:Object in resultObject)
			{
				_listeResultat.addItem(createColonneItemVo(obj));
			}
			dispatchEvent(new GetColonneEvent(GetColonneEvent.GET_ALL_COLONNES));
		}
		
		private function createColonneItemVo(obj:Object):Colonne
		{
			var item : Colonne = new Colonne();
			item.createObject(obj);
			return item;
		}
		
		public function set listeResultat(value:ArrayCollection):void
		{
			_listeResultat=value;
		}

		[Bindable]
		public function get listeResultat():ArrayCollection
		{
			return _listeResultat;
		}
	}
}
