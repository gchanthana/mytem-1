package gestionparc.services.getcolonnes
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.SpecificationVO;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;

	public class GetColonneService 
	{
		[Bindable]
		public var model:GetColonneData;
		public var myHandler :GetColonneHandler;
		private var myData: ArrayCollection;

		public function GetColonneService() 
		{
			model=new GetColonneData();
			myHandler=new GetColonneHandler(model);
			myData=new ArrayCollection();
		}

		public function getColonneVueParc():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getSilentOperation("fr.consotel.consoview.M111.colonnes.GetColonnesVue", "getAllColonnes", myHandler.ViewResultHandler);
			
			RemoteObjectUtil.callSilentService(op);
		}
	}
}