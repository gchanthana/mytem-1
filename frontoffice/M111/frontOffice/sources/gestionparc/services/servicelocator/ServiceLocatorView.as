package gestionparc.services.servicelocator
{
	import gestionparc.services.vue.parc.VueParcService;
	import gestionparc.services.vue.parcglobal.VueParcGlobalService;
	import gestionparc.services.vue.toutparc.VueToutParcService;

	public class ServiceLocatorView
	{
		private var _service:IServiceView;
		private var _serviceId:Number;

		public function ServiceLocatorView(serviceId:Number)
		{
			_serviceId=serviceId;
			initService(_serviceId);
		}

		/**
		 * créer une service par son ID  
		 * @param serviceId
		 * 
		 */		
		private function initService(serviceId :Number):void
		{
			switch (serviceId)
			{
				case 0 : 
					_service=new VueParcGlobalService();
					break;
				
				case 1 : 
					_service=new VueParcService();
					break;
				
				case 2 : 
					_service=new VueToutParcService();
					break;
				
				default:
					_service=new VueParcService();
					break;
			}
		}

		public function get service():IServiceView
		{
			return _service;
		}
	}
}