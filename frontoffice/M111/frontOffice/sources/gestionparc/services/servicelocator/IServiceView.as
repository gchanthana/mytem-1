package gestionparc.services.servicelocator
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;

	public interface IServiceView
	{
		 function getModel(): EventDispatcher;
		 function getData(indexDebut:int, nbItem:int):void;
		 function restoreData():ArrayCollection;
		 function getAllnbRow(): Number;
	}
}