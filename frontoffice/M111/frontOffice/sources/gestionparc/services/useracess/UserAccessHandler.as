package gestionparc.services.useracess
{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;

	internal class UserAccessHandler
	{
		private var _model:UserAccessModel;
		
		public function UserAccessHandler(model:UserAccessModel)
		{
			_model = model;
		}
		
		internal function getUserAccessResultHandler(e:ResultEvent):void
		{
			if(e.result)
			{
				_model.updateUserAccess(e.result as Array);
			}
		}
		
		internal function checkPassWordResultHandler(e:ResultEvent):void
		{
			if(e.result)
			{
				_model.updateCheckPassWordResult(Number(e.result));
			}
		}
		
		internal function updateUserSessionResultHandler(re:ResultEvent):void
		{
			if(re.result == -1 )
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Erreur_lors_de_la_mise___jour_'), gestionparcMessages.ERREUR);
			}
		}
		
		
	}
}