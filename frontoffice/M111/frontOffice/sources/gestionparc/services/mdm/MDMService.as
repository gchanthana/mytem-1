package gestionparc.services.mdm
{
	import flash.events.EventDispatcher;

	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.MDMDataEvent;

	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	import mx.utils.ObjectUtil;

	public class MDMService
	{

		[Bindable]
		public var myDatas:MDMDatas;
		public var myHandler:MDMHandler;

		private var _pathCFC:String='fr.consotel.consoview.M111.MDMService';

		public function MDMService()
		{
			myDatas=new MDMDatas();
			myHandler=new MDMHandler(myDatas);
		}

		private function faultHandler(eventObject:FaultEvent):void
		{
			var eventTarget:EventDispatcher=eventObject.target as EventDispatcher;
			if (eventTarget != null)
				if (eventTarget.hasEventListener(FaultEvent.FAULT))
					eventTarget.removeEventListener(FaultEvent.FAULT, faultHandler);
			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=true;

			if (myDatas != null)
			{
				myDatas.dispatchEvent(new MDMDataEvent(MDMDataEvent.IS_MANAGED_RESULT));
				myDatas.dispatchEvent(new MDMDataEvent(MDMDataEvent.MDM_DATA_COMPLETE));
			}

			var op:AbstractOperation=eventObject.target is AbstractOperation ? (eventObject.target as AbstractOperation) : null;
			if (op != null)
			{
				var opName:String=op.name;
				var opProps:String="\nAbstractOperation properties :\n" + ((op.properties != null) ? ObjectUtil.toString(op.properties) : "");
				var remoteObject:RemoteObject=op.service is RemoteObject ? (op.service as RemoteObject) : null;
				var cfc:String=remoteObject != null ? remoteObject.source + "." : "";
				opName=cfc + opName + "()";
			}
		}

		/**
		 * la methode isManaged est appellé par la methode getDeviceInfo
		 */
		public function isManaged(serialNumber:String, imei:String):void
		{
			// MAJ du FLAG d'erreur avant chaque opération
			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				if (SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=false;
			// 1-/ Récupérer le statut du device
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, _pathCFC, 'isManaged', myHandler.deviceStatusHandler); // Mise à jour du statut du device
			op.addEventListener(FaultEvent.FAULT, faultHandler);
			RemoteObjectUtil.callService(op, serialNumber, imei);

		}

		/**
		 * la methdode getDeviceInfo est appelé principalement dans AbstractItemMenuContextual par la methode initItemMenu lors qu'on fait un clique droit sur IMEI
		 */
		public function getDeviceInfo(serialNumber:String, imei:String):void
		{
			// MAJ du FLAG d'erreur avant chaque opération
			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				if (SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=false;
			
			/**  1- Récupérer le statut du device */
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, _pathCFC, 'isManaged', isManagedResult);

			/** Sauvegarde des paramètres pour les utiliser dans isManagedResult()*/

			op.properties={serialNumber: serialNumber, imei: imei};
			op.addEventListener(FaultEvent.FAULT, faultHandler);
			RemoteObjectUtil.callService(op, serialNumber, imei);
		}

		/**
		 * isManagedResult  permet de Récupèrer les infos du device si le device est enrollé dans le mdm
		 */	
		private function isManagedResult(eventObject:ResultEvent):void
		{
			// Référence vers l'appel à isManaged : Récupération des paramètres
			var deviceIsManaged:Boolean=eventObject.result as Boolean;
			var isManagedOp:AbstractOperation=eventObject.target as AbstractOperation;
			var serialNumber:String=isManagedOp.properties.serialNumber;
			var imei:String=isManagedOp.properties.imei;
			
			// 2- Mise à jour du statut du device 
			myHandler.deviceStatusHandler(eventObject);

			/** Réinitialisation du FLAG d'erreur après chaque remoting effectué sans erreur*/
			
			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				if (SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=false;
			
			/** 3- Si le device est enrollé : Récupèrer les infos du device*/
			if (deviceIsManaged)
			{
				var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, _pathCFC, 
				    'getDeviceInfo', myHandler.deviceInfoHandler);
				// Sauvegarde des paramètres pour les retrouver dans faultHandler()
				op.properties={serialNumber: serialNumber, imei: imei};
				op.addEventListener(FaultEvent.FAULT, faultHandler);
				RemoteObjectUtil.callService(op, serialNumber, imei);
			}
			else
				myDatas.dispatchEvent(new MDMDataEvent(MDMDataEvent.MDM_DATA_COMPLETE));
		}

		public function getLocations(imei:String, mdmimei:String, serial:String, deviceid:Number, idterminal:int, soustete:String, idsoustete:int):void
		{
		/* N'est plus utilisé
		var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			_pathCFC,
																			'getLocations',
																			myHandler.locationsHandler);
		myHandler.currentIMEI = imei;

		RemoteObjectUtil.callService(op,imei,
										serial,
										mdmimei,
										idterminal,
										soustete,
										idsoustete,
										deviceid);
		*/
		}

		public function enrollDevice(imei:String, mdmimei:String, serial:String, idterminal:int, soustete:String, idsoustete:int, idpool:int):void
		{
			// MAJ du FLAG d'erreur avant chaque opération
			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				if (SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=false;
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, _pathCFC, 'simpleEnrollement', myHandler.enrollDeviceHandler);
			op.addEventListener(FaultEvent.FAULT, faultHandler);
			RemoteObjectUtil.callService(op, imei, serial, mdmimei, idterminal, soustete, idsoustete, idpool);
		}

		public function lock(imei:String, mdmimei:String, serial:String, idterminal:int, pincode:String, wait:int=30000):void
		{
			// MAJ du FLAG d'erreur avant chaque opération
			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				if (SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=false;
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, _pathCFC, 'lock', myHandler.lockHandler);
			op.addEventListener(FaultEvent.FAULT, faultHandler);
			RemoteObjectUtil.callService(op, imei, serial, mdmimei, idterminal, pincode, wait);
		}

		public function unlock(imei:String, mdmimei:String, serial:String, idterminal:int, pincode:String, wait:int=30000):void
		{
			// MAJ du FLAG d'erreur avant chaque opération
			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				if (SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=false;
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, _pathCFC, 'unlock', myHandler.unlockHandler);
			op.addEventListener(FaultEvent.FAULT, faultHandler);
			RemoteObjectUtil.callService(op, imei, serial, mdmimei, idterminal, pincode, wait);
		}

		public function wipe(imei:String, mdmimei:String, serial:String, idterminal:int, isWipeComplete:Boolean, isEraseSDCard:Boolean=false, pincode:String='0000', wait:int=30000):void
		{
			// MAJ du FLAG d'erreur avant chaque opération
			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				if (SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=false;
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, _pathCFC, 'wipe', myHandler.wipeHandler);
			op.addEventListener(FaultEvent.FAULT, faultHandler);
			RemoteObjectUtil.callService(op, imei, serial, mdmimei, idterminal, isEraseSDCard, isWipeComplete, pincode, wait);
		}

		public function revoke(imei:String, mdmimei:String, serial:String, idterminal:int, isAuthorize:int):void
		{
			// MAJ du FLAG d'erreur avant chaque opération
			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				if (SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=false;
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, _pathCFC, 'revoke', myHandler.revokeHandler);
			op.addEventListener(FaultEvent.FAULT, faultHandler);
			RemoteObjectUtil.callService(op, imei, serial, mdmimei, idterminal, isAuthorize);
		}

		public function locate(serial:String, imei:String, wait:int=30000):void
		{
			// MAJ du FLAG d'erreur avant chaque opération
			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				if (SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=false;
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, _pathCFC, 'locate', myHandler.locateHandler);
			op.addEventListener(FaultEvent.FAULT, faultHandler);
			RemoteObjectUtil.callService(op, serial, imei, wait);
		}

		public function sendEnrollmentInfos(sup_bug:Number, params:Object):void
		{
			// MAJ du FLAG d'erreur avant chaque opération
			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				if (SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=false;
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, _pathCFC, 'sendEnrollmentInfos', myHandler.sendEmailHandler);
			RemoteObjectUtil.callService(op, sup_bug, params);
		}
	}
}
