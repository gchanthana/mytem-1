package gestionparc.services.mdm
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.MDMDataEvent;
	
	import mx.core.Application;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;

	public class MDMHandler
	{
		public var currentIMEI:String = '';
		
		private var _myDatas:MDMDatas;
		
		public function MDMHandler(instanceOfData:MDMDatas)
		{
			_myDatas = instanceOfData;
		}
		
		public function deviceStatusHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				_myDatas.deviceStatusHandler(re.result);
			}
			else
			{
				if(SpecificationVO.getInstance().elementDataGridSelected != null)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=true;
				_myDatas.deviceStatusHandler(re.result);
			}
		}
		
		public function deviceInfoHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				_myDatas.deviceInfoHandler(re.result);
			}
			else
			{
				if(SpecificationVO.getInstance().elementDataGridSelected != null)
					SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT=true;
				_myDatas.deviceInfoHandler(re.result);
			}
		}
		
		public function locationsHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				_myDatas.locationsHandler(re.result, currentIMEI);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Une_erreur_est_survenue___'), ResourceManager.getInstance().getString('M111', 'erreur'));
			}
		}
		
		public function enrollDeviceHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				ConsoviewAlert.afficherOKImage("Votre campagne a bien été envoyée.", Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Une_erreur_est_survenue___') + re.result.toString(),
												ResourceManager.getInstance().getString('M111', 'erreur'));
			}
		}
		
		public function lockHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Votre_demande_a__t__prise_en_compte'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Une_erreur_est_survenue___') + re.result.toString(),
												ResourceManager.getInstance().getString('M111', 'erreur'));
			}
		}
		
		public function unlockHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Votre_demande_a__t__prise_en_compte'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Une_erreur_est_survenue___') + re.result.toString(),
												ResourceManager.getInstance().getString('M111', 'erreur'));
			}
		}
		
		public function wipeHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Votre_demande_a__t__prise_en_compte'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Une_erreur_est_survenue___') + re.result.toString(),
												ResourceManager.getInstance().getString('M111', 'erreur'));
			}
		}
		
		public function revokeHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Votre_demande_a__t__prise_en_compte'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Une_erreur_est_survenue___') + re.result.toString(),
												ResourceManager.getInstance().getString('M111', 'erreur'));
			}
		}
		
		public function locateHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				if(re.result.LOCATE_STATUS == 1) {
					_myDatas.locateHandler(re.result);
					ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Votre_demande_a__t__prise_en_compte'), Application.application as DisplayObject);
				} else {
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M111','Recherche_impossible__'), ResourceManager.getInstance().getString('M111','Info'),null);
					/*
					ConsoviewAlert.afficherError(
						"Unable to locate device (e.g : A locate operation is already running).\n"+
						"Status : "+re.result.LOCATE_STATUS+". Message : "+re.result.LOCATE_MESSAGE,
						ResourceManager.getInstance().getString('M111', 'erreur')
					);
					*/
				}
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Une_erreur_est_survenue___') + re.result.toString(),
					ResourceManager.getInstance().getString('M111', 'erreur'));
			}
		}
		
		public function sendEmailHandler(re:ResultEvent):void {
			_myDatas.dispatchEvent(new MDMDataEvent(MDMDataEvent.ENROLLMENT_INFOS_SENT));
		}
	}
}