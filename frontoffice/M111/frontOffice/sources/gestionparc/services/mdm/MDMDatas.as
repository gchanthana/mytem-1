package gestionparc.services.mdm
{
	import flash.events.EventDispatcher;

	import gestionparc.entity.Location;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.MDMDataEvent;

	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;

	[Bindable]
	public class MDMDatas extends EventDispatcher
	{
		public var arrListeDevice:ArrayCollection;
		public var arrListeSoftware:ArrayCollection;
		public var arrListeLocation:ArrayCollection;
		public var sortedArrListeDevice:ArrayCollection;

		public var DeviceSelected:Object;

		public function MDMDatas()
		{
			arrListeDevice=new ArrayCollection();
			arrListeSoftware=new ArrayCollection();
			arrListeLocation=new ArrayCollection();
			sortedArrListeDevice=new ArrayCollection();
			DeviceSelected=new Object();

		}

		internal function deviceStatusHandler(resultObject:Object):void
		{
			var isManaged:Boolean=(resultObject != null) ? (resultObject as Boolean) : false;

			if (SpecificationVO.getInstance().elementDataGridSelected != null)
				if (isManaged == true)
					SpecificationVO.getInstance().elementDataGridSelected.BOOL_ISMANAGED=true;
				else
					SpecificationVO.getInstance().elementDataGridSelected.BOOL_ISMANAGED=false;
			// Le résultat de isManaged() est retourné dans le FrontOffice
			dispatchEvent(new MDMDataEvent(MDMDataEvent.IS_MANAGED_RESULT));
		}

		internal function deviceInfoHandler(resultObject:Object):void
		{
			arrListeDevice.removeAll();
			arrListeSoftware.removeAll();
			sortedArrListeDevice.removeAll();
			DeviceSelected=new Object();

			if(resultObject.hasOwnProperty("DEVICEPROPERTIES"))
			{
				arrListeDevice=new ArrayCollection(resultObject == null ? null : resultObject.DEVICEPROPERTIES);
				sortedArrListeDevice=sortCollection(arrListeDevice, "CATEGORIE", "DESC", false);
			}

			if(resultObject.hasOwnProperty("SOFTWAREINVENTORY"))
			{
				arrListeSoftware=new ArrayCollection(resultObject == null ? null : resultObject.SOFTWAREINVENTORY);
			}

			if ((resultObject != null) && resultObject.hasOwnProperty("GENERAL") && (resultObject.GENERAL as Array).length > 0)
			{
				DeviceSelected=resultObject.GENERAL[0];
				// Mise à jour des valeurs des propriétés : SERIAL_NUMBER et IMEI
				if (SpecificationVO.getInstance().elementDataGridSelected != null)
				{
					if (DeviceSelected.hasOwnProperty("T_NO_SERIE"))
						SpecificationVO.getInstance().elementDataGridSelected.T_NO_SERIE=DeviceSelected["T_NO_SERIE"];
					if (DeviceSelected.hasOwnProperty("T_IMEI"))
						SpecificationVO.getInstance().elementDataGridSelected.T_IMEI=DeviceSelected["T_IMEI"];
				}
			}

			dispatchEvent(new MDMDataEvent(MDMDataEvent.MDM_DATA_COMPLETE));
		}

		internal function locationsHandler(resultObject:Object, imei:String):void
		{
			arrListeLocation.removeAll();

			arrListeLocation=Location.formatLocation(resultObject as ArrayCollection, imei);
			dispatchEvent(new MDMDataEvent(MDMDataEvent.MDM_DATA_COMPLETE));
		}

		internal function locateHandler(resultObject:Object):void
		{
			dispatchEvent(new MDMDataEvent(MDMDataEvent.MDM_DEVICE_LOCATE));
		}

		private function sortCollection(arrayCollection:ArrayCollection, sortBy:String, order:String, numericSort:Boolean):ArrayCollection
		{
			//Create the sort field
			var dataSortField:SortField=new SortField();

			//name of the field of the object on which you wish to sort the Collection
			dataSortField.name=sortBy;
			dataSortField.caseInsensitive=true;

			//If you wish to perform numeric sort then set:
			dataSortField.numeric=numericSort;

			/* Set the order, by default it's ascending */
			if (order.toUpperCase() == "DESC")
			{
				dataSortField.descending=true;
			}

			//create the sort object
			var dataSort:Sort=new Sort();
			dataSort.fields=[dataSortField];

			arrayCollection.sort=dataSort;
			//refresh the collection to sort
			arrayCollection.refresh();

			return arrayCollection;
		}
	}
}
