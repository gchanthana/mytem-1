package gestionparc.services.comptefacturation
{
	import flash.events.EventDispatcher;
	
	import gestionparc.event.gestionparcEvent;
	
	import mx.collections.ArrayCollection;

	public class CompteFacturationData extends EventDispatcher
	{
		private var _listeResultat:ArrayCollection = null;
		
		public function CompteFacturationData():void
		{
			_listeResultat = new ArrayCollection();
		}
		
		public function setCompteFacturation(resultObject:ArrayCollection):void
		{
			_listeResultat=resultObject;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.GET_COMPTE_FACTURATION));
		}
				
		public function set listeResultat(value:ArrayCollection):void
		{
			_listeResultat = value;
		}
		
		[Bindable] 
		public function get listeResultat():ArrayCollection
		{
			return _listeResultat;
		}
	}
}