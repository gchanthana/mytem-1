package gestionparc.services.comptefacturation
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	public class CompteFacturationHandler
	{
		private var model:CompteFacturationData;

		public function CompteFacturationHandler(instanceOfDatas:CompteFacturationData)
		{
			model=instanceOfDatas;
		}

		internal function ViewResultHandler(re:ResultEvent):void
		{
			if (re.result)
			{
				model.setCompteFacturation(re.result as ArrayCollection);
			}
			else
			{
				ConsoviewAlert.afficherError(gestionparcMessages.RECHERCHE_IMPOSSIBLE, gestionparcMessages.ERREUR);
			}
		}
	}
}