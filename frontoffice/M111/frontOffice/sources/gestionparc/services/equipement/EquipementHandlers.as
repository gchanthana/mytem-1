package gestionparc.services.equipement
{
	import flash.display.DisplayObject;
	
	import mx.collections.ArrayCollection;
	import mx.core.Application;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	
	import composants.util.ConsoviewAlert;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.utils.gestionparcMessages;
	
	public class EquipementHandlers
	{
		private var myDatas : EquipementDatas;
		
		public function EquipementHandlers(instanceOfDatas:EquipementDatas)
		{
			myDatas = instanceOfDatas;
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>getInfosFicheTerminal()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetInfosFicheTerminal()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getInfosFicheTerminalResultHandler(re:ResultEvent):void
		{
			if((re.result is Array) && (re.result as Array)[0] == 1)
			{
				myDatas.treatDataGetInfosFicheTerminal(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_FICHE_TERM_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}	
		
		/**
		 * Fonction appellée au retour de la fonction <code>setInfosFicheTerminal()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatSetInfosFicheTerminal()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function setInfosFicheTerminalResultHandler(re:ResultEvent):void
		{
			if(re.result > -1)
			{
				myDatas.treatSetInfosFicheTerminal(parseInt(re.result.toString()));
			}
			else if(re.result == -2)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.NUMERO_IMEI_EXISTANT,
					gestionparcMessages.ERREUR);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.UPDATE_FICHE_TERM_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>setInfosContrat()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatSetInfosContrat()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function setInfosContratResultHandler(re:ResultEvent):void
		{
			if((re.result as Array).length > 0 && (re.result as Array)[0] > 0)
			{
				myDatas.treatSetInfosContrat(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.SAVE_INFOS_CONTRAT_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		internal function setsetContratsToEqpResultHandler(re:ResultEvent):void
		{
			if((re.result as Array).length > 0)
			{
				myDatas.treatSetInfosContrat(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.SAVE_INFOS_CONTRAT_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		/**
		 * Fonction appellée au retour de la fonction <code>deleteContrat()</code> (appel de procédure).
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function deleteContratResultHandler(re:ResultEvent):void
		{
			if(re.result < 0)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.DELETE_CONTRAT_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>getListeModele()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatGetListeModele()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getListeModeleResultHandler(re:ResultEvent):void
		{
			if((re.result as ArrayCollection).length  > -1)
			{
				myDatas.treatGetListeModele(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_MODELES_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		internal function getListeTypeResultHandler(re:ResultEvent):void
		{
			if((re.result as ArrayCollection).length  > -1)
			{
				myDatas.treatGetListeType(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_MODELES_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		internal function getListeMarquesResultHandler(re:ResultEvent):void
		{
			if((re.result as ArrayCollection).length  > -1)
			{
				myDatas.treatGetListeMarque(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_MODELES_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		/**
		 * Fonction appellée au retour de la fonction <code>getListLigneSimFromEqpt()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatGetListLigneSimFromEqpt()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getListLigneSimFromEqptResultHandler(re:ResultEvent):void
		{
			if(re.result[0] > 0)
			{
				myDatas.treatGetListLigneSimFromEqpt(re.result[1]);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_INFOS_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>getSousEqptDispo()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatGetSousEqptDispo()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getSousEqptDispoResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatGetSousEqptDispo(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_SS_EQPT_DISPO_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function rebusTerminalResultHandler(re:ResultEvent):void
		{
			if(re.result && re.result != -1)
			{
				myDatas.treatrebusTerminal();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_SS_EQPT_DISPO_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function echangerEqpResultHandler(re:ResultEvent):void
		{
			if(re.result && re.result != -1)
			{
				myDatas.treatechangerEqp();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_SS_EQPT_DISPO_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function equipActiveResultHandler(re:ResultEvent):void
		{
			if(re.result && re.result > 0)
			{
				SpecificationVO.getInstance().boolHasChanged = true;
				SpecificationVO.getInstance().refreshData();
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString("M111","ACTION_REALIZED"),Application.application as DisplayObject);
				myDatas.dispatchEvent(new gestionparcEvent(gestionparcEvent.DISSOCIATION_REALIZED));
			}
		}
	}
}