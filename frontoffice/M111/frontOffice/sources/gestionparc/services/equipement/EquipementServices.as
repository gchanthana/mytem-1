package gestionparc.services.equipement
{
	import flash.events.EventDispatcher;
	
	import mx.rpc.AbstractOperation;
	
	import composants.util.DateFunction;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.ContratVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;

	/**
	 * Appelle les procédures liées aux équipements.
	 */
	public class EquipementServices extends EventDispatcher
	{

		[Bindable]
		public var myDatas:EquipementDatas;
		public var myHandlers:EquipementHandlers;

		/**
		 * Instancie les datas.
		 * Instancie les handlers.
		 */
		public function EquipementServices()
		{
			myDatas=new EquipementDatas();
			myHandlers=new EquipementHandlers(myDatas);
		}

		/**
		 * Appelle la procédure permettant de récupérer :
		 * - Tous les informations concernant l'équipement.
		 * - Tous les Ligne/SIM liées à l'équipement.
		 * - Tous les incidents liés à l'équipement.
		 * - Tous les contrats liés à l'équipement.
		 */
		public function getInfosFicheTerminal(createObjToServiceGet:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.fiches.FicheTerminal", "getInfosFicheTerminal", myHandlers.getInfosFicheTerminalResultHandler);

			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;
			var idTerminale:Number=createObjToServiceGet.idTerminal > 0 ? createObjToServiceGet.idTerminal : ligne.IDTERMINAL;
			var segment:Number=createObjToServiceGet.segment;
			var idPool:Number=SpecificationVO.getInstance().idPool;
			var idRow:Number=createObjToServiceGet.idRow > 0 ? createObjToServiceGet.idRow : ligne.ID;
			
			RemoteObjectUtil.callService(op, idTerminale, segment, idPool, idRow);
		}

		/**
		 * Appelle la procédure permettant de mettre à jour :
		 * - Tous les informations concernant l'équipement.
		 */
		public function setInfosFicheTerminal(createObjToServiceSet:Object):void
		{
			var idPool:Number=0;

			if (SpecificationVO.getInstance().idPool > 0)
			{
				idPool=SpecificationVO.getInstance().idPool;
			}
			else if (SpecificationVO.getInstance().idPoolNotValid > 0)
			{
				idPool=SpecificationVO.getInstance().idPoolNotValid;
			}
			else
			{
				idPool=0;
			}

			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.fiches.FicheTerminal", "setInfosFicheTerminal", myHandlers.setInfosFicheTerminalResultHandler);

			RemoteObjectUtil.callService(op, createObjToServiceSet.myTerminal.idEquipement,
											 createObjToServiceSet.myTerminal.idEqptFournis,
											 createObjToServiceSet.myTerminal.idRevendeur, 
											 createObjToServiceSet.myTerminal.idFabricant,
											 createObjToServiceSet.myTerminal.idTypeEquipement, 
											 createObjToServiceSet.myTerminal.commande.idCommande, 
											 createObjToServiceSet.isNotSameImei,
											 createObjToServiceSet.myTerminal.numIemiEquipement,
											 createObjToServiceSet.isNotSameNumSerie,
											 createObjToServiceSet.myTerminal.numSerieEquipement,
											 DateFunction.formatDateAsInverseString(createObjToServiceSet.myTerminal.commande.dateAchat), 
											 createObjToServiceSet.myTerminal.commande.prix,
											 createObjToServiceSet.myTerminal.commande.numCommande,
											 DateFunction.formatDateAsInverseString(createObjToServiceSet.myTerminal.commande.dateLivraison),
											 createObjToServiceSet.myTerminal.commande.refLivraison, 
											 createObjToServiceSet.myTerminal.commentaires,
											 createObjToServiceSet.myTerminal.idSite, 
											 createObjToServiceSet.myTerminal.idEmplacement,
											 createObjToServiceSet.myTerminal.libEqptFournis, 
											 createObjToServiceSet.myTerminal.refConst,
											 createObjToServiceSet.myTerminal.refRevendeur, 
											 createObjToServiceSet.ID, 
											 idPool, 
											 createObjToServiceSet.myTerminal.niveau, 
											 (createObjToServiceSet.insertPoolDistrib > 0) ? 1 : 0,
											 (createObjToServiceSet.insertPoolSociete > 0) ? 1 : 0,
											 createObjToServiceSet.myTerminal.byod);
		}
		
		/**
		 * La procédure est la meme que setInfosFicheTerminal avec les parametres d'un SIM
		 * Appelle la procédure permettant de crée un équipement SIM 
		 * - Tous les informations concernant la SIM.
		 */
		public function createInfosFicheSIM(createObjToServiceSet:Object):void
		{
			var idPool:Number=0;
			
			if (SpecificationVO.getInstance().idPool > 0)
			{
				idPool=SpecificationVO.getInstance().idPool;
			}
			else if (SpecificationVO.getInstance().idPoolNotValid > 0)
			{
				idPool=SpecificationVO.getInstance().idPoolNotValid;
			}
			else
			{
				idPool=0;
			}
			
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.fiches.FicheTerminal", "setInfosFicheTerminal", myHandlers.setInfosFicheTerminalResultHandler);
			
			RemoteObjectUtil.callService(op, 
				-1,		//idEquipement
				createObjToServiceSet.myTerminal.idEqptFournis,	//cbModele.selectedItem.IDEQUIPEMENT_FOURNISSEUR
				createObjToServiceSet.myTerminal.idRevendeur, 	//cbModele.selectedItem.IDREVENDEUR
				createObjToServiceSet.myTerminal.idFabricant,	//cbMarque.selectedItem.IDFOURNISSEUR
				71, 	// idTypeEquipement
				0,		//idCommande 
				0,		//isNotSameImei
				createObjToServiceSet.myTerminal.numIemiEquipement,// le Numéro de SIM SAISIE
				0,		//isNotSameNumSerie
				'',		//numSerieEquipement
				'',		//dateAchat 
				0,		//Prix
				'',		//numCommande
				'',		//dateLivraison
				'', 	//refLivraison 
				'creation manuel gestion parc',//commentaire
				-1, 	//idSite
				-1,		//idEmplacement
				createObjToServiceSet.myTerminal.libEqptFournis, //cbModele.selectedItem.LIBELLE_EQ
				'',			//refConst
				'', 		//refRevendeur
				0,			//ID 
				idPool, 	//IdPool
				1,			//niveau 
				0,			//insertPoolDistrib
				0,			//insertPoolSociete
				0);		//BYOD
		}

		/**
		 * Appelle la procédure permettant de mettre à jour :
		 * - Tous les informations concernant un contrat d'équipement.
		 *
		 * N.B. : Si l'idContrat = -1 alors l'action correspond à un ajout de contrat,
		 * 		  sinon cela correspond à une mise à jour des infos du contrat.
		 */
		public function setInfosContrat(myContrat:ContratVO, idTerminal:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheTerminal", "setInfosContrat", myHandlers.setInfosContratResultHandler);

			RemoteObjectUtil.callService(op, myContrat.idContrat, idTerminal, myContrat.idTypeContrat, myContrat.idRevendeur, myContrat.refContrat, DateFunction.formatDateAsInverseString(myContrat.dateSignature), DateFunction.formatDateAsInverseString(myContrat.dateEcheance), myContrat.commentaires, myContrat.tarif, myContrat.dureeContrat, myContrat.taciteReconduction, DateFunction.formatDateAsInverseString(myContrat.dateDebut), myContrat.preavis, myContrat.tarifMensuel, DateFunction.formatDateAsInverseString(myContrat.dateResiliation), DateFunction.formatDateAsInverseString(myContrat.dateRenouvellement), DateFunction.formatDateAsInverseString(myContrat.dateEligibilite), myContrat.dureeEligibilite);
		}

		public function setContratsToEqp(idTerminal:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheTerminal", "setContratsToEqp", myHandlers.setsetContratsToEqpResultHandler);

			RemoteObjectUtil.callService(op, myDatas.terminal.listeContrat, idTerminal);
		}

		/**
		 * Appelle la procédure permettant de supprimer le contrat.
		 */
		public function deleteContrat(myContrat:ContratVO):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.inventaire.contrats.GestionContratsService", "deleteContrat", myHandlers.deleteContratResultHandler);

			RemoteObjectUtil.callService(op, myContrat.idContrat);
		}

		/**
		 * Appelle la procédure permettant de récupérer tous les modèles pour une marque.
		 */
		public function getListeModele(idtype:int, idmarque:int):void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview..M111.fiches.FicheTerminal", "getListeModele", myHandlers.getListeModeleResultHandler);
			RemoteObjectUtil.callService(opData, idtype, idmarque)
		}

		public function getListeType(idCategorie:int):void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview..M111.fiches.FicheTerminal", "getListeType", myHandlers.getListeTypeResultHandler);
			RemoteObjectUtil.callService(opData, idCategorie)
		}

		/**
		 * Appelle la procédure permettant de récupérer la liste des infos
		 * sur les liaisons Ligne-Sim associé a ce terminal.
		 */
		public function getListLigneSimFromEqpt(idTerminal:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheTerminal", "getListLigneSimFromEqpt", myHandlers.getListLigneSimFromEqptResultHandler);

			RemoteObjectUtil.callService(op, idTerminal);
		}

		/**

		 * Appelle la procédure permettant de récupérer la liste
		 * des sous equipements disponibles.
		 */
		public function getSousEqptDispo(objToSearch:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.fiches.FicheTerminal", "getSousEqptDispo", myHandlers.getSousEqptDispoResultHandler);

			var idPool:Number = SpecificationVO.getInstance().idPool;
			
			RemoteObjectUtil.callService(op, objToSearch.idType, objToSearch.idCategorie, objToSearch.numSerie, objToSearch.idConstructeur, 
												objToSearch.idRevendeur, objToSearch.idSite, objToSearch.critereTrie, objToSearch.nbItem, 
												objToSearch.startItem, idPool, 1); // segment de l'eqpt : 1=mobile, 2=Fixe, 3=data
		}

		public function mettreAuRebut(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.fiches.FicheTerminal", "mettreAuRebut", myHandlers.rebusTerminalResultHandler);
			RemoteObjectUtil.callService(op, obj);
		}
		
		public function mettreEquipactive(idEquip:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.fiches.FicheTerminal", "mettreEquipActive", myHandlers.equipActiveResultHandler);
			RemoteObjectUtil.callService(op, idEquip);
		}

		public function echangerEqp(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.fiches.FicheTerminal", "echangerEqp", myHandlers.echangerEqpResultHandler);
			RemoteObjectUtil.callService(op, obj);
		}

		public function getListeMarqueWithType(idType:int):void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview..M111.fiches.FicheTerminal", "getListeMarque", myHandlers.getListeMarquesResultHandler);
			RemoteObjectUtil.callService(opData, idType)
		}
	}
}
