package gestionparc.services.equipement
{
	import flash.events.EventDispatcher;
	import flash.sampler.NewObjectSample;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.TerminalVO;
	import gestionparc.event.gestionparcEvent;
	
	/**
	 * Traite les données reçues aux retours des procedures appelées dans <code>EquipementServices</code>.
	 */
	public class EquipementDatas extends EventDispatcher
	{
		
		private const TYPE_ACCESSOIRE:int = 2191;
		
		
		private var _terminal					: TerminalVO = new TerminalVO();
		private var _listeModele				: ArrayCollection = new ArrayCollection();
		private var _listeMarque				: ArrayCollection = new ArrayCollection();
		private var _listeSousEqptDispo			: ArrayCollection = new ArrayCollection();
		private var _listeType					: ArrayCollection = new ArrayCollection();
		
		
		/**
		 * Traite les données reçues au retour de la procedure <code>getInfosFicheTerminal()</code>.
		 * Appel la fonction <code>fillTerminalVO()</code> pour affecter a la data "terminal" toutes les données concernant :
		 * - Ses informations personnelles.
		 * - Ses lignes et SIM.
		 * - Ses incidents.
		 * - Ses contrats.
		 */
		public function treatDataGetInfosFicheTerminal(obj:Object):void
		{
			terminal = new TerminalVO();
			terminal = terminal.fillTerminalVO(obj);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_TERM_LOADED,null,true));
		}
		
		/**
		 * Dispatch un événements pour signaler que la mise à jour des informations
		 * concernant la fiche terminal s'est bien déroulée.
		 */
		public function treatSetInfosFicheTerminal(idEqp:int):void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_TERM_UPLOADED,idEqp));
		}
		
		/**
		 * Dispatch un événements pour signaler que l'enregistrements des informations
		 * concernant la fiche contrat du terminal s'est bien déroulée.
		 * Cette événement transporte l'idContrat (pour les suppressions de contrats)
		 */
		public function treatSetInfosContrat(obj:Object):void
		{
			var idContrat 	: int 	 = (obj[1] as int);
			var myObj 		: Object = new Object();
			myObj.idContrat = idContrat;
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_CONTRAT_SAVED,myObj));
		}
		
		/**
		 * Traite les données reçues au retour de la procedure <code>getListeModele()</code>.
		 * Affecte à la data "listeModele" la liste des constructeurs récupérées.
		 */
		public function treatGetListeModele(obj:Object):void
		{
			listeModele = new ArrayCollection();
			var lenListeModele : int = listeModele.length;
			var lenResult : int = (obj as ArrayCollection).length;
			var add : Boolean = false;
			
			var modele:Object;
			
			var idRevendeur:Number = 0;
			if(SpecificationVO.getInstance().idRevendeur > -1)
				idRevendeur = SpecificationVO.getInstance().idRevendeur;
			else if(SpecificationVO.getInstance().idRevendeurNotValid > -1)
				idRevendeur = SpecificationVO.getInstance().idRevendeurNotValid;
			
			for(var i : int = 0; i < lenResult; i++)
			{
				
				modele = (obj as ArrayCollection)[i];
				if(	idRevendeur == modele.IDREVENDEUR || idRevendeur == 0 )
				{	
					modele = (obj as ArrayCollection)[i];
					modele.REVENDEUR_EQUIPEMENT = modele.LIBELLE_EQ + ' - ' + modele.REVENDEUR;
					listeModele.addItem(modele);
					lenListeModele = listeModele.length;	
				}
			}
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_MODELES_LOADED,null,true));
		}
		public function treatGetListeType(obj:Object):void
		{
			var values:ArrayCollection = obj as ArrayCollection;
			var cursor : IViewCursor = values.createCursor();
			
			if(!_listeType)
			{
				_listeMarque = new ArrayCollection(); 
			}
			
			listeType.removeAll();			
			
			while(!cursor.afterLast)
			{
				if(cursor.current.IDTYPE_EQUIPEMENT != TYPE_ACCESSOIRE)
				{
					listeType.addItem(cursor.current)		
				}
				cursor.moveNext();
			}
			 
		
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_TYPE_LOADED,null,true));
		}
		public function treatGetListeMarque(obj:Object):void
		{
			listeMarque = obj as ArrayCollection;
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_MARQUES_LOADED,null,true));
		}
		/**
		 * Traite les données reçues au retour de la procedure <code>getListLigneSimFromEqpt()</code> 
		 * en appellant la fonction <code>fillListLigneSim()</code> pour affecter a la data 
		 * "terminal.listeLigneSim" la liste des nouvelles informations sur les Ligne-Sim- de ce terminal
		 * Dispatch un événements pour signaler que la liste des liaisons Ligne-Sim de ce terminal a été mise à jour.
		 */
		public function treatGetListLigneSimFromEqpt(objResult:Object):void
		{
			terminal.fillListLigneSim(objResult);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LIST_LINK_LINE_SIM_LOADED));
		}
		
		/**
		 * Traite les données reçues au retour de la procedure <code>getSousEqptDispo()</code> 
		 * Affecte à la data "listeSousEqptDispo" la liste des sous equipements récupérés.
		 */
		public function treatGetSousEqptDispo(objResult:Object):void
		{
			listeSousEqptDispo = new ArrayCollection();
			listeSousEqptDispo = (objResult as ArrayCollection);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LIST_SS_EQPT_DISPO_LOADED));
		}
		public function treatrebusTerminal():void
		{
			refreshData();
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.MISE_AU_REBUT_VALIDATE));
		}
		public function treatechangerEqp():void
		{
			refreshData();
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.ASSOCIATION_REALIZED));
		}
		private function refreshData():void
		{
			SpecificationVO.getInstance().boolHasChanged = true;
			SpecificationVO.getInstance().refreshData();
		}

		[Bindable]
		public function set terminal(value:TerminalVO):void
		{
			_terminal = value;
		}
		public function get terminal():TerminalVO
		{
			return _terminal;
		}
		[Bindable]
		public function set listeModele(value:ArrayCollection):void
		{
			_listeModele = value;
		}

		public function get listeModele():ArrayCollection
		{
			return _listeModele;
		}
		[Bindable]
		public function set listeSousEqptDispo(value:ArrayCollection):void
		{
			_listeSousEqptDispo = value;
		}
		
		public function get listeSousEqptDispo():ArrayCollection
		{
			return _listeSousEqptDispo;
		}

		public function get listeType():ArrayCollection
		{
			return _listeType;
		}
		[Bindable]
		public function set listeType(value:ArrayCollection):void
		{
			_listeType = value;
		}
		public function get listeMarque():ArrayCollection
		{
			return _listeMarque;
		}
		[Bindable]
		public function set listeMarque(value:ArrayCollection):void
		{
			_listeMarque = value;
		}	
	}
}