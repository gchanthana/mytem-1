package gestionparc.services.commande
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.rpc.events.ResultEvent;

	public class CommandeHandlers
	{
		
		private var myDatas : CommandeDatas;
		
		public function CommandeHandlers(instanceOfDatas:CommandeDatas)
		{
			myDatas = instanceOfDatas;
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>setInfosCommande()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataSetInfosCommande()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function setInfosCommandeResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatDataSetInfosCommande();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.SET_INFOS_CMD_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>genererNumeroDeCommande()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGenererNumeroDeCommande()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function genererNumeroDeCommandeResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataGenererNumeroDeCommande(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.GENERATION_NUM_CMD_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
	}
}