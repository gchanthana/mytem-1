package gestionparc.services.cache
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.resources.ResourceManager;
	
	import gestionparc.entity.EquipementVO;
	import gestionparc.entity.GpDroitUserVO;
	import gestionparc.entity.OrgaVO;
	import gestionparc.entity.PoolVO;
	import gestionparc.entity.SiteVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;

	/**
	 * Traite les données reçues aux retours des procedures appelées dans <code>CacheServices</code>.
	 * Tous les datas de cette classe sont "public static" car elles doivent être accessible partout
	 * et sans instanciation de la classe <code>CacheServices</code>. Seuls les classes modifiant ces datas
	 * doivent instancier la classe <code>CacheServices</code>.
	 * @see gestionparc.services.cache.CacheServices
	 */
	[Bindable]
	public class CacheDatas extends EventDispatcher
	{

		public static var listTousOperateur:ArrayCollection=null;
		public static var matricePoolProfil:ArrayCollection=null;
		public static var listModeRaccordement:ArrayCollection=null;
		public static var listCategoriesEquipement:ArrayCollection=null;
		public static var listTypesEquipement:ArrayCollection=null;
		public static var listTypesEquipementWithCategorie:ArrayCollection=null;
		public static var listePays:ArrayCollection=null;
		public static var listeInfosOperateur:ArrayCollection=null;
		public static var listeTypesLigne:ArrayCollection=null;
		public static var listeCompteSousCompte:ArrayCollection=null;
		public static var listeOperateur:ArrayCollection=null;
		public static var listeUsages:ArrayCollection=null;
		public static var listeTypeContrat:ArrayCollection=null;
		public static var listeTypeIntervention:ArrayCollection=null;
		public static var listeRevendeur:ArrayCollection=null;
		public static var listeProfilEqpt:ArrayCollection=null;
		public static var listeRevendeurForProfEqpt:ArrayCollection=null;

		public static var listeConstructeur:ArrayCollection=null;
		public static var listeCategorie:ArrayCollection=new ArrayCollection();
		public static var siteLivraison:ArrayCollection=null;
		public static var libellesPerso1:String=ResourceManager.getInstance().getString('M111', 'Libell__1__');
		public static var libellesPerso2:String=ResourceManager.getInstance().getString('M111', 'Libell__2__');
		public static var libellesPerso3:String=ResourceManager.getInstance().getString('M111', 'Libell__3__');
		public static var libellesPerso4:String=ResourceManager.getInstance().getString('M111', 'Libell__4__');
		public static var libellesPersoLigne1:String=ResourceManager.getInstance().getString('M111', 'Libell__1__');
		public static var libellesPersoLigne2:String=ResourceManager.getInstance().getString('M111', 'Libell__2__');
		public static var libellesPersoLigne3:String=ResourceManager.getInstance().getString('M111', 'Libell__3__');
		public static var libellesPersoLigne4:String=ResourceManager.getInstance().getString('M111', 'Libell__4__');
		public static var civilites:ArrayCollection=null;
		public static var idPool:int=0;
		public static var libellePool:String="";
		public static var listePool:ArrayCollection;
		public static var listeOrga:ArrayCollection;
		public static var listeMois:ArrayCollection;
		public static var listeDroitCommande:ArrayCollection;
		public static var listeChampPerso:ArrayCollection = new ArrayCollection();

		public static function resetCacheData():void
		{
			idPool=0;
			libellePool="";
			libellesPerso1=ResourceManager.getInstance().getString('M111', 'Libell__1__');
			libellesPerso2=ResourceManager.getInstance().getString('M111', 'Libell__2__');
			libellesPerso3=ResourceManager.getInstance().getString('M111', 'Libell__3__');
			libellesPerso4=ResourceManager.getInstance().getString('M111', 'Libell__4__');
			libellesPersoLigne1=ResourceManager.getInstance().getString('M111', 'Libell__1__');
			libellesPersoLigne2=ResourceManager.getInstance().getString('M111', 'Libell__2__');
			libellesPersoLigne3=ResourceManager.getInstance().getString('M111', 'Libell__3__');
			libellesPersoLigne4=ResourceManager.getInstance().getString('M111', 'Libell__4__');
			if (civilites)
				civilites.removeAll();
			if (listCategoriesEquipement)
				listCategoriesEquipement.removeAll();
			if (listeCategorie)
				listeCategorie.removeAll();
			if (listeCompteSousCompte)
				listeCompteSousCompte.removeAll();
			if (listeConstructeur)
				listeConstructeur.removeAll();
			if (listeDroitCommande)
				listeDroitCommande.removeAll();
			if (listeInfosOperateur)
				listeInfosOperateur.removeAll();
			if (listeMois)
				listeMois.removeAll();
			if (listeOperateur)
				listeOperateur.removeAll();
			if (listeOrga)
				listeOrga.removeAll();
			if (listePays)
				listePays.removeAll();
			if (listePool)
				listePool.removeAll();
			if (listeProfilEqpt)
				listeProfilEqpt.removeAll();
			if (listeRevendeur)
				listeRevendeur.removeAll();
			if (listeRevendeurForProfEqpt)
				listeRevendeurForProfEqpt.removeAll();
			if (listeTypeContrat)
				listeTypeContrat.removeAll();
			if (listeTypeIntervention)
				listeTypeIntervention.removeAll();
			if (listeTypesLigne)
				listeTypesLigne.removeAll();
			if (listeUsages)
				listeUsages.removeAll();
			if (listModeRaccordement)
				listModeRaccordement.removeAll();
			if (listTousOperateur)
				listTousOperateur.removeAll();
			if (listTypesEquipement)
				listTypesEquipement.removeAll();
			if (listTypesEquipementWithCategorie)
				listTypesEquipementWithCategorie.removeAll();
			if (matricePoolProfil)
				matricePoolProfil.removeAll();
			
		}

		public function treatDatagetCacheData(obj:Object):void
		{
			treatDataGetCivilite(obj.CIVILITE);
			treatDataGetLibellePersoResultHandler(obj.LIBELLE);
			treatDataGetLibellePersoLigneResultHandler(obj.LIBELLELIGNE);
			treatDataGetListeUsages(obj.USAGE);
			treatDataGetListeTypeContrat(obj.CONTRAT);
			treatDataGetListPays(obj.PAYS);
			treatGetConstructeur(obj.CONSTRUCTEUR);
			treatListeCategorie(obj.CATEGORIE);
			treatDataGetListProfilEquipements(obj.PROFILEQP);
			treatDataGetTypeLigne(obj.TYPELIGNE);
			processMatricePoolProfil(obj.PROFIL_ACTION);

		}

		public function treatinitData(objResult:Object):void
		{
			SpecificationVO.getInstance().gpDroitUser=new GpDroitUserVO();
			SpecificationVO.getInstance().gpDroitUser.cloneParProcedure(objResult.DROITGP);
			treatListeOrga(objResult.LISTEORGA);
			listeMois=objResult.LISTEMOIS;
			listeDroitCommande=objResult.LISTEDROITCOMMANDE;
			siteLivraison=new ArrayCollection();
			var siteVO:SiteVO=new SiteVO();
			siteLivraison=siteVO.fillListSiteVO(objResult.LISTESITE as ArrayCollection);
			treatGetListeTousOperateurs(objResult.OPERATEUR);
			treatDataGetListeRevendeur(objResult.REVENDEUR);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.RECUPERER_DROITUSER_FINISHED));
		}

		public function treatListeOrga(objResult:ArrayCollection):void
		{
			var org:OrgaVO=new OrgaVO();
			listeOrga=org.fillListOrgaVO(objResult);
		}

		public function treatListeCategorie(objResult:ArrayCollection):void
		{
			var cursor:IViewCursor = objResult.createCursor();
			var defautItem:Object;
			
			while(!cursor.afterLast)
			{
				if(cursor.current.IDCATEGORIE_EQUIPEMENT == EquipementVO.CATEGORIE_TERMINAUX)
				{
					defautItem = cursor.current;
				}
				else
				{
					listeCategorie.addItem(cursor.current);
				}
				cursor.moveNext();
			}
			
			listeCategorie.addItemAt(defautItem,0);
		}

		public function treatgetListePool(objResult:ArrayCollection):void
		{
			var isParametreParkGlobal:Number=0;
			var isParametreToutPark:Number=0;
			
			if(moduleGestionParcIHM.globalParameter != null)
			{
				isParametreParkGlobal=moduleGestionParcIHM.globalParameter.userAccess.D_PARC_GL; // si =1 on affiche l'option parc gloabl dans le combobox de pool
				isParametreToutPark=moduleGestionParcIHM.globalParameter.userAccess.D_ALL_POOL; 
			}
			
			listePool=new ArrayCollection();
			
			if (isParametreParkGlobal > 0)
			{
				var FirstObj:Object=new Object();

				FirstObj.IDPOOL=SpecificationVO.POOL_PARC_GLOBAL_ID;
				FirstObj.LIBELLE_POOL=ResourceManager.getInstance().getString('M111', 'Parc_Global');
				FirstObj.LIBELLE_PROFIL = '';
				FirstObj.IDPROFIL=0;
				FirstObj.IDREVENDEUR=0;

				listePool.addItemAt(FirstObj, 0);
			}
			
			if (isParametreToutPark > 0)
			{
				var objTP:Object=new Object();
				objTP.IDPOOL = SpecificationVO.POOL_TOUT_PARC_ID;
				objTP.LIBELLE_POOL = ResourceManager.getInstance().getString('M111', 'tout_le_parc');
				objTP.LIBELLE_PROFIL = '';
				objTP.IDPROFIL = 0;
				objTP.IDREVENDEUR = 0;
				
				listePool.addItemAt(objTP, 0);
			}

			for each (var obj:Object in objResult)
			{
				listePool.addItem(createViewParcItemVo(obj));
			}

			listePool.refresh();

			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LISTE_POOL));
		}

		private function createViewParcItemVo(obj:Object):PoolVO
		{
			var item:PoolVO=new PoolVO();
			item.createObject(obj);
			return item;
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getSiteLivraison()</code>.
		 * @see gestionparc.services.cache.CacheServices#getSiteLivraison()
		 */
		public function treatDataGetSiteLivraison(obj:Object):void
		{
			siteLivraison=new ArrayCollection();
			var siteVO:SiteVO=new SiteVO();
			siteLivraison=siteVO.fillListSiteVO(obj as ArrayCollection);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_SITE_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getCivilite()</code>.
		 * @see gestionparc.services.cache.CacheServices#getCivilite()
		 */
		public function treatDataGetCivilite(obj:Object):void
		{
			var lenCivilite:int=(obj as ArrayCollection).length;
			var objCivilite:Object=null;

			civilites=new ArrayCollection();

			for (var i:int=0; i < lenCivilite; i++)
			{
				objCivilite=new Object();
				objCivilite.idCivilite=obj[i].IDCIVILITE;
				if (obj[i].CIVILITE != null) // pour l'id 99
				{
					objCivilite.civilite=obj[i].CIVILITE;
				}
				else
				{
					objCivilite.civilite="";
				}

				civilites.addItem(objCivilite);
			}
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getLibellePerso()</code>.
		 * @see gestionparc.services.cache.CacheServices#getLibellePerso()
		 */
		public function treatDataGetLibellePersoResultHandler(obj:Object):void
		{
			if ((obj as ArrayCollection).length > 0)
			{
				libellesPerso1=(obj[0].C1 as String).slice(0, (obj[0].C1 as String).length-2);
				libellesPerso2=(obj[0].C2 as String).slice(0, (obj[0].C2 as String).length-2);
				libellesPerso3=(obj[0].C3 as String).slice(0, (obj[0].C3 as String).length-2);
				libellesPerso4=(obj[0].C4 as String).slice(0, (obj[0].C4 as String).length-2);
				
				listeChampPerso.source = [];
				(libellesPerso1 != '') ? listeChampPerso.addItem({LIBELLE:libellesPerso1, CODE:'E_C1'}) : null;// ajout dans la liste ou null pour ne rien faire
				(libellesPerso2 != '') ? listeChampPerso.addItem({LIBELLE:libellesPerso2, CODE:'E_C2'}) : null;
				(libellesPerso3 != '') ? listeChampPerso.addItem({LIBELLE:libellesPerso3, CODE:'E_C3'}) : null;
				(libellesPerso4 != '') ? listeChampPerso.addItem({LIBELLE:libellesPerso4, CODE:'E_C4'}) : null;
				dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_COLONNE_PERSO, true) );
			}
		}

		public function treatDataGetLibellePersoLigneResultHandler(obj:Object):void
		{
			if ((obj as ArrayCollection).length > 0)
			{
				libellesPersoLigne1=obj[0].C1;
				libellesPersoLigne2=obj[0].C2;
				libellesPersoLigne3=obj[0].C3;
				libellesPersoLigne4=obj[0].C4;
			}
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getListeRevendeur()</code>.
		 * @see gestionparc.services.cache.CacheServices#getListeRevendeur()
		 */
		public function treatDataGetListeRevendeur(obj:Object):void
		{
			listeRevendeur=new ArrayCollection();
			listeRevendeur=(obj as ArrayCollection);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_REVENDEUR_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getListeTypeContrat()</code>.
		 */
		public function treatDataGetListeTypeContrat(obj:Object):void
		{
			listeTypeContrat=new ArrayCollection();

			for each (var o:Object in obj as ArrayCollection)
			{
				var oTmp:Object=new Object();
				oTmp.ID=o.IDTYPE_CONTRAT;
				oTmp.LIBELLE=o.TYPE_CONTRAT;

				listeTypeContrat.addItem(oTmp);
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_CONTRAT_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getListeTypeIntervention()</code>.
		 * @see gestionparc.services.cache.CacheServices#getListeTypeIntervention()
		 */
		public function treatDataGetListeTypeIntervention(obj:Object):void
		{
			listeTypeIntervention=new ArrayCollection();
			listeTypeIntervention=(obj as ArrayCollection);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_INTERVENTION_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getListeUsages()</code>.
		 * @see gestionparc.services.cache.CacheServices#getListeUsages()
		 */
		public function treatDataGetListeUsages(obj:Object):void
		{
			listeUsages=new ArrayCollection();
			for each (var o:Object in obj as ArrayCollection)
			{
				if (o.USAGE != null)
					listeUsages.addItem(o);
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_USAGES_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getListProfilEquipements()</code>.	 *
		 * @see gestionparc.services.cache.CacheServices#getListProfilEquipements()
		 */
		public function treatDataGetListProfilEquipements(obj:Object):void
		{
			listeProfilEqpt=new ArrayCollection();
			listeProfilEqpt=(obj as ArrayCollection);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_PROFIL_EQPT_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getListRevendeurForProfEqpt()</code>.
		 * @see gestionparc.services.cache.CacheServices#getListRevendeurForProfEqpt()
		 */
		public function treatDataGetListRevendeurForProfEqpt(obj:Object):void
		{
			listeRevendeurForProfEqpt=new ArrayCollection();
			listeRevendeurForProfEqpt=(obj as ArrayCollection);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_REVENDEUR_PROF_EQPT_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getTypeLigne()</code>.
		 * @see gestionparc.services.cache.CacheServices#getTypeLigne()
		 */
		public function treatDataGetTypeLigne(obj:Object):void
		{
			listeTypesLigne=new ArrayCollection();
			listeTypesLigne=(obj as ArrayCollection);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_TYPES_LIGNE_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getListeComptesOperateurByLoginAndPool()</code>.
		 * @see gestionparc.services.cache.CacheServices#getListeComptesOperateurByLoginAndPool()
		 */
		public function treatDataGetListeComptesOperateurByLoginAndPool(obj:Object):void
		{
			listeCompteSousCompte=new ArrayCollection();

			var objComtpe:Object;
			var objSousComtpe:Object;

			for each (var myObj:* in obj)
			{
				var len:int=listeCompteSousCompte.length;
				var j:int=0;

				for (; j < len; j++)
				{
					if (listeCompteSousCompte[j].IDCOMPTE == myObj.IDCOMPTE_FACTURATION)
					{
						break;
					}
				}

				if (j >= len) // si le compte n'existe pas
				{
					// creation d'un objet compte
					objComtpe=new Object();
					objComtpe.COMPTE=myObj.COMPTE_FACTURATION;
					objComtpe.IDCOMPTE=myObj.IDCOMPTE_FACTURATION;
					objComtpe.TAB_SOUS_COMPTE=new ArrayCollection();

					// creation d'un objet sous compte
					objSousComtpe=new Object();
					objSousComtpe.SOUS_COMPTE=myObj.SOUS_COMPTE;
					objSousComtpe.IDSOUS_COMPTE=myObj.IDSOUS_COMPTE;

					objComtpe.TAB_SOUS_COMPTE.addItem(objSousComtpe); // ajout de l'objet sous compte au tableau sous compte
					listeCompteSousCompte.addItem(objComtpe); // ajout de l'objet compte au tableau des comptes
				}
				else //  le compte existe
				{
					var longueur:int=(listeCompteSousCompte[j].TAB_SOUS_COMPTE as ArrayCollection).length;
					var i:int=0;
					for (; i < longueur; i++)
					{
						if (listeCompteSousCompte[j].TAB_SOUS_COMPTE[i].IDSOUS_COMPTE == myObj.IDSOUS_COMPTE)
						{
							break;
						}
					}

					if (i >= longueur) // le sous compte n'existe pas
					{
						// creation d'un objet sous compte
						objSousComtpe=new Object();
						objSousComtpe.SOUS_COMPTE=myObj.SOUS_COMPTE;
						objSousComtpe.IDSOUS_COMPTE=myObj.IDSOUS_COMPTE;

						(listeCompteSousCompte[j].TAB_SOUS_COMPTE as ArrayCollection).addItem(objSousComtpe); // ajout de l'objet sous compte au tableau sous compte
					}
				}
			}

			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_COMPTES_SOUSCOMPTES_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getListeOperateurs()</code>.
		 * @see gestionparc.services.cache.CacheServices#getListeOperateurs()
		 */
		public function treatDataGetListeOperateurs(obj:Object):void
		{
			listeOperateur=new ArrayCollection();
			listeOperateur=(obj as ArrayCollection);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_OPERATEUR_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getInfoClientOp()</code>.
		 * @see gestionparc.services.cache.CacheServices#getInfoClientOp()
		 */
		public function treatDataGetInfoClientOp(obj:Object):void
		{
			if ((obj as ArrayCollection).length > 0)
			{
				listeInfosOperateur=new ArrayCollection();
				listeInfosOperateur=(obj as ArrayCollection);
			}
			dispatchEvent(new gestionparcEvent(gestionparcEvent.INFO_OP_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getListPays()</code>.
		 * @see gestionparc.services.cache.CacheServices#getListPays()
		 */
		public function treatDataGetListPays(obj:Object):void
		{
			listePays=new ArrayCollection();
			listePays=(obj as ArrayCollection);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_PAYS_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getConstructeur()</code>.
		 * Affecte à la data "listeConstructeur" la liste des constructeurs récupérées (seulement ceux dont le TYPE_FOURNISSEUR = 0).
		 * @see gestionparc.services.cache.CacheServices#getConstructeur()
		 */
		public function treatGetConstructeur(obj:Object):void
		{
			listeConstructeur=obj as ArrayCollection;

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_CONSTRU_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getCategoriesEquipement()</code>.
		 * Affecte à la data "listCategoriesEquipement" la liste des catégories d'équipement récupérées.
		 * @see gestionparc.services.cache.CacheServices#getCategoriesEquipement()
		 */
		public function treatGetCategoriesEquipement(obj:Object):void
		{
			listCategoriesEquipement=new ArrayCollection();
			listCategoriesEquipement=obj as ArrayCollection;

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LIST_CATEGORIES_EQPT_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getTypesEquipement()</code>.
		 * Affecte à la data "listTypesEquipement" la liste des types d'équipement récupérées.
		 * @see gestionparc.services.cache.CacheServices#getTypesEquipement()
		 */
		public function treatGetTypesEquipement(obj:Object):void
		{
			listTypesEquipement=new ArrayCollection();
			listTypesEquipement=obj as ArrayCollection;

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LIST_TYPES_EQPT_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getTypesEquipementWithCategorie()</code>.
		 * Affecte à la data "listTypesEquipementWithCategorie" la liste des types d'équipement récupérées.
		 * @see gestionparc.services.cache.CacheServices#getTypesEquipementWithCategorie()
		 */
		public function treatGetTypesEquipementWithCategorie(obj:Object):void
		{
			listTypesEquipementWithCategorie=new ArrayCollection();

			var lenObj:int=(obj as ArrayCollection).length;
			var objCat:Object=null;
			var objType:Object=null;
			var isPresent:Boolean=false;

			// on va ajouter toutes les catgories a notre liste listTypesEquipementWithCategorie
			var i:int=0;
			for (; i < lenObj; i++)
			{
				isPresent=false;

				// on cree l'objet "categorie"
				objCat=new Object();
				objCat.idCategorie=obj[i].IDCATEGORIE_EQUIPEMENT;
				objCat.categorie=obj[i].CATEGORIE_EQUIPEMENT;

				// on cherche si cette categorie est deja dans notre liste
				var lenListTypesEquipementWithCategorie:int=listTypesEquipementWithCategorie.length;
				var j:int=0;
				for (; j < lenListTypesEquipementWithCategorie; j++)
				{
					if (listTypesEquipementWithCategorie[j].idCategorie == objCat.idCategorie)
					{
						isPresent=true;
						break;
					}
				}

				objType=new Object(); /**  on cree l'objet "type" */
				objType.idType=obj[i].IDTYPE_EQUIPEMENT;
				objType.type=obj[i].TYPE_EQUIPEMENT;

				// la categorie n'etait pas presente.
				if (!isPresent)
				{
					objCat.tabType=new ArrayCollection();
					(objCat.tabType as ArrayCollection).addItem(objType);

					// apres ajout du type dans la catgorie, on ajoute la categorie a notre liste listTypesEquipementWithCategorie
					listTypesEquipementWithCategorie.addItem(objCat);
				}
				else /** la categorie etait presente */
				{
					// on cherche si le type n'est pas present, si c'est le cas on l'ajoute
					if (!(listTypesEquipementWithCategorie[j].tabType as ArrayCollection).contains(objType))
					{
						(listTypesEquipementWithCategorie[j].tabType as ArrayCollection).addItem(objType);
					}
				}
			}
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LIST_TYPES_EQPT_WITH_CAT_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getListeModeRaccordement()</code>.
		 * Affecte à la data "listModeRaccordement" la liste des modes de raccordement récupérées.
		 * @see gestionparc.services.cache.CacheServices#getListeModeRaccordement()
		 */
		public function treatGetListeModeRaccordement(obj:Object):void
		{
			listModeRaccordement=new ArrayCollection();
			listModeRaccordement=obj as ArrayCollection;

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_MODE_RACCOR_LOADED));
		}

		/**
		 * Traite les données reçues au retour de la procedure <code>getListeTousOperateurs()</code>.
		 * Affecte à la data "listTousOperateur" la liste de tous les opérateurs récupérées.
		 * @see gestionparc.services.cache.CacheServices#getListeTousOperateurs()
		 */
		public function treatGetListeTousOperateurs(obj:Object):void
		{
			listTousOperateur=new ArrayCollection();
			listTousOperateur=obj as ArrayCollection;

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_TOUS_OP_LOADED));
		}
		
		
		/**
		 * Traite les données reçues au retour de la procedure <code>getListeTousOperateurs()</code>.
		 * Affecte à la data "listTousOperateur" la liste de tous les opérateurs récupérées.
		 * @see gestionparc.services.cache.CacheServices#getListeTousOperateurs()
		 */
		public function processMatricePoolProfil(obj:Object):void
		{
			matricePoolProfil=new ArrayCollection();
			matricePoolProfil=obj as ArrayCollection;
			
			//this.dispatchEvent(new gestionparcEvent(gestionparcEvent.MATRICE_POOL_PROFIL_LOADED));
		}
	}
}
