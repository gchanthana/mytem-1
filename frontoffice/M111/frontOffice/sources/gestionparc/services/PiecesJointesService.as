package gestionparc.services
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.FileUpload;
	import gestionparc.event.PiecesJointesEvent;

	public class PiecesJointesService extends EventDispatcher
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		private static var _path				:String = "fr.consotel.consoview.M111.AttachementService";
		
		//--------------------------------------------------------------------------------------------//
		//					GLOBALES
		//--------------------------------------------------------------------------------------------//
		
		public var UUID							:String = "";
		
		//--------------------------------------------------------------------------------------------//
		//					LIBELLE A TRADUIRE - LUPO
		//--------------------------------------------------------------------------------------------//
		
		private var text_Error_createUUID		:String = ResourceManager.getInstance().getString('M111', 'Impossible_de_cr_er_un_dossier');
		private var text_Error_editFile			:String = ResourceManager.getInstance().getString('M111', 'Impossible_de_remomer_le_fichier');

		//--------------------------------------------------------------------------------------------//
		//					CONSRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function PiecesJointesService()
		{
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLICS
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		public function createUUID(): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"initUUID",
																				createUUIDResultHandler);		
			RemoteObjectUtil.callService(op);	
		}
		
		public function renameFile(newNameFile:String, doc:FileUpload):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_path,
				"editAttachement",
				renameFileResultHandler);		
			RemoteObjectUtil.callService(op,doc.fileName,
				doc.fileName,
				newNameFile,
				doc.fileId);
		}
		
	
		private function createUUIDResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				UUID = re.result as String;
				dispatchEvent(new PiecesJointesEvent(PiecesJointesEvent.FILE_UUID_CREATED));
			}
			else
				popup(text_Error_createUUID);
		}
		
		private function renameFileResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new PiecesJointesEvent(PiecesJointesEvent.FILE_RENAMED));
			else
				popup(text_Error_editFile);
		}
		
		private function popup(txt:String):void
		{
			ConsoviewAlert.afficherError(txt, 'Consoview');
		}
		
		public function uploadFichier(currentFile:FileUpload=null):void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.upload.UploaderFile",
				"uploadFile",uploadFileResultHandler);
			RemoteObjectUtil.callService(op,currentFile, -1);
		}
		
		private function uploadFileResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new Event('FILE_UPLOADED_OK'));
			else
				ConsoviewAlert.afficherAlertInfo("Une erreur est survenue lors de l'upload du fichier", 'Consoview', null);
		}
		
	}
}