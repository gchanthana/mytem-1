package gestionparc.event
{
	import flash.events.Event;

	public class ImportDeMasseEvent extends Event
	{
		//--------------- VARIABLES ----------------//
		
		public static const CHECKED_COLLAB		:String = "CHECKED_COLLAB";
		public static const CHECKED_IMEI		:String = "CHECKED_IMEI";
		public static const CHECKED_NUMSERIE	:String = "CHECKED_NUMSERIE";
		public static const CHECKED_NUMLIGNE	:String = "CHECKED_NUMLIGNE";
		public static const CHECKED_NUMSIM		:String = "CHECKED_NUMSIM";
		public static const CHECKED_NUMEXTENSION:String = "CHECKED_NUMEXTENSION";
		public static const IMPORT_DONE			:String = "IMPORT_DONE";
		
		private var _objRet:Object = 0;
		
		
		//--------------- METHODES ----------------//
		
		/* */
		public function ImportDeMasseEvent(type:String,objRet:Object,bubbles:Boolean=false,cancealable:Boolean=false)
		{
			super(type,bubbles,cancealable);
			this._objRet = objRet;
		}
		
		/* */
		public function get objRet():Object
		{
			return _objRet;
		}
	}
}