package gestionparc.event
{
	import flash.events.Event;

	public class UserAccessModelEvent extends Event
	{
		public static const UAME_USER_ACCESS_UPDATED:String="uameUserAccessUpdated";
		public static const UAME_CHECK_PASSWORD_UPDATED:String="uameCheckPasswordUpdated";


		public function UserAccessModelEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new UserAccessModelEvent(type, bubbles, cancelable);
		}
	}
}
