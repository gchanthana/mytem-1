package gestionparc.event
{
	import flash.events.Event;

	public class MDMDataEvent extends Event
	{
		public static const MDM_DATA_COMPLETE:String="MDMDataComplete";
		public static const ENROLLMENT_INFOS_SENT:String="ENROLLMENT_INFOS_SENT";
		public static const MDM_ENROLL_COMPLETE:String="MDMEnrollComplete";
		public static const MDM_DEVICE_LOCATE:String="MDMDeviceLocate";
		public static const IS_MANAGED_RESULT:String="IS_MANAGED_RESULT"; // Lorsque le résultat de isManaged() est retourné

		public function MDMDataEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new MDMDataEvent(type, bubbles, cancelable);
		}

	}
}
