package gestionparc.event
{
	import flash.events.Event;

	public class CheckBoxChangeEvent extends Event
	{
		public static const CHBX_SELECTED:String="chbxSelected";
		public static const CHBX_NO_SELECTED:String="chbxNoSelected";
		public static const STATUT_SELECTED_CHECKBOX:String="statutSelectedCheckbox";
		public static const CHBX_SELECTION_CHANGE:String="chbxSelectionChange";

		private var _statutChBx:int;

		public function CheckBoxChangeEvent(type:String, statutChBx:int=0)
		{
			super(type);
			this._statutChBx=statutChBx;
		}

		public function get statutChBx():int
		{
			return _statutChBx;
		}

		override public function clone():Event
		{
			return new CheckBoxChangeEvent(type, statutChBx);
		}

	}
}
