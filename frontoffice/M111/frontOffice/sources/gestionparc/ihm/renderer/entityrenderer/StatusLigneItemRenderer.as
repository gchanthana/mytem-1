package gestionparc.ihm.renderer.entityrenderer
{
	import flash.events.MouseEvent;
	
	import mx.resources.ResourceManager;

	public class StatusLigneItemRenderer extends EntityItemRenderer
	{
		public function StatusLigneItemRenderer()
		{
		}
		
		override public function set data(value:Object):void
		{
			img.visible=false;
			lbLibelle.text="";
			lbLibelle.toolTip="";
			lbLibelle.useHandCursor=false;
			
			lbLibelle.useHandCursor=false;
			lbLibelle.buttonMode=false;
			lbLibelle.setStyle("textDecoration", "none");
			
			if (value != null)
			{
				super.data=value;
				
				if (value.hasOwnProperty("ST_IDETAT") && value.ST_IDETAT)
				{
					switch (value.ST_IDETAT)
					{
						case 1:
							lbLibelle.text = ResourceManager.getInstance().getString('M111','active_');
							lbLibelle.toolTip = ResourceManager.getInstance().getString('M111','active_');
							break;
						
						case 2:
							lbLibelle.text = ResourceManager.getInstance().getString('M111','Suspendue');
							lbLibelle.toolTip = ResourceManager.getInstance().getString('M111','Suspendue');
							break;
						
						case 3:
							lbLibelle.text = ResourceManager.getInstance().getString('M111','R_sili_');
							lbLibelle.toolTip = ResourceManager.getInstance().getString('M111','R_sili_');
							break;
					}
				}
			}
		}
	}
}