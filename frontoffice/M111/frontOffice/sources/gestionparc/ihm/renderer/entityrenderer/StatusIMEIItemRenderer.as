package gestionparc.ihm.renderer.entityrenderer
{
	import mx.resources.ResourceManager;

	public class StatusIMEIItemRenderer extends EntityItemRenderer
	{
		public function StatusIMEIItemRenderer()
		{
			super();
		}
		
		override public function set data(value:Object):void
		{
			img.visible=false;
			lbLibelle.text="";
			lbLibelle.toolTip="";
			lbLibelle.useHandCursor=false;
			lbLibelle.buttonMode=false;
			lbLibelle.setStyle("textDecoration", "none");
			
			if (value != null)
			{
				super.data=value;
				
				if (value.hasOwnProperty("T_ID_STATUT") && value.T_ID_STATUT)
				{
					switch (value.T_ID_STATUT)
					{
						case 1:
							lbLibelle.text = ResourceManager.getInstance().getString('M111','active_');
							lbLibelle.toolTip = ResourceManager.getInstance().getString('M111','active_');
							break;
						
						case 2:
							lbLibelle.text = ResourceManager.getInstance().getString('M111','pr_te__');
							lbLibelle.toolTip = ResourceManager.getInstance().getString('M111','pr_te__');
							break;
						
						case 3:
							lbLibelle.text = ResourceManager.getInstance().getString('M111','_au_rebut');
							lbLibelle.toolTip = ResourceManager.getInstance().getString('M111','_au_rebut');
							break;
						case 4:
							lbLibelle.text = ResourceManager.getInstance().getString('M111','en_SAV___');
							lbLibelle.toolTip = ResourceManager.getInstance().getString('M111','en_SAV___');
							break;
					}
				}
			}
		}
	}
}