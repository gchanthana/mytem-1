package gestionparc.ihm.renderer.entityrenderer
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.fiches.ficheEntity.FicheLigneIHM;
	import gestionparc.ihm.main.menu.MenuContextual;
	import gestionparc.ihm.main.menu.itemmenurenderer.ItemMenuContextualOfLigne;

	public class LigneItemRenderer extends EntityItemRenderer
	{
		public function LigneItemRenderer():void
		{
			super();
		}

		/**
		 * Cette fonction surcharge le set data et met ainsi a jour le texte affiché, le style
		 * et appelle la fonction <code>fillArrayItemMenuEquipement</code> pour créer la liste des items du menu
		 * et la fonction <code>MenuContextualEntity</code> pour créer le menu.
		 */
		override public function set data(value:Object):void
		{
			this.validateNow();
			lbLibelle.text="";
			lbLibelle.toolTip="";
			lbLibelle.useHandCursor=false;
			this.img.visible=false;

			setStyle("backgroundColor", 0xFFFFFF);
			setStyle("backgroundAlpha", 0);
			
			if (value != null)
			{
				super.data=value;

				if (value.hasOwnProperty("SOUS_TETE") && value.SOUS_TETE)
				{
					if (SpecificationVO.getInstance().idPool == SpecificationVO.POOL_PARC_GLOBAL_ID) // si vue parc global on n'affiche pas l'image action
					{
						this.img.visible=false;
					}
					else
					{
						this.img.visible=true;
						this.img.addEventListener(MouseEvent.CLICK, lineSelected);
					}
					lbLibelle.addEventListener(MouseEvent.CLICK, ClickFicheHandler);
					lbLibelle.text=value.SOUS_TETE;
					lbLibelle.toolTip=lbLibelle.text;
					lbLibelle.useHandCursor=true;
					
					switch (value.ST_IDETAT)
					{
						case 3 :
							setStyle("backgroundColor", 0xCCCCCC); // gris résiliée
							setStyle("backgroundAlpha", 1);
							break;
						
						case 2 :
							setStyle("backgroundColor", 0xFEC46D); // orange 
							setStyle("backgroundAlpha", 1);
							break;
						
						default :
							setStyle("backgroundColor", 0xFFFFFF);
							setStyle("backgroundAlpha", 0);
							break;
					}					
				}
			}
		}
		
		

		/**
		 * Cette fonction est appelée lors d'un clique sur l'image de l'item renderer,
		 * elle place les informations concernant le menu de cet item et
		 * appelle la fonction qui affiche se dernier.
		 */
		private function lineSelected(evt:Event):void
		{
			/** on met a jour les information de la ligne selectionnée */
			SpecificationVO.getInstance().elementDataGridSelected=evt.currentTarget.owner.data;
			var ligne:Object=evt.currentTarget.owner.data;
			var Context:SpecificationVO = SpecificationVO.getInstance();
			
			
			if((Context.validSelectedIdPool == SpecificationVO.POOL_TOUT_PARC_ID)) //pas de profil == 'Mes pools'
			{
				Context.updateProfilCommande(ligne);//MYT-1606
				Context.idPool = ligne.IDPOOL;
				Context.labelPool = ligne.POOL;
				var userPool:Object = Context.getUserPoolInfo(ligne.IDPOOL);
				if(userPool != null)
				{
					Context.idProfil = userPool.IDPROFIL;
					Context.labelProfil =  userPool.PROFIL;
				}
			}

			this.arrayItemMenu=new Array();
			var myItemMenuContextualOfLigne:ItemMenuContextualOfLigne=new ItemMenuContextualOfLigne((data as AbstractMatriceParcVO));
			this.arrayItemMenu=myItemMenuContextualOfLigne.returnArrayItemMenuContextual();
			this.menu=new MenuContextual(this.arrayItemMenu);

			clickIconActionHandler();
		}

		override protected function setFiche():void
		{
			this.fiche=new FicheLigneIHM();
			(this.fiche as FicheLigneIHM).idsoustete=(data as AbstractMatriceParcVO).IDSOUS_TETE;
		}
	}
}
