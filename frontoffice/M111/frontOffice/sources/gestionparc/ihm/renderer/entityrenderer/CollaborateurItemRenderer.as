package gestionparc.ihm.renderer.entityrenderer
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.fiches.ficheEntity.FicheCollaborateurIHM;
	import gestionparc.ihm.main.menu.MenuContextual;
	import gestionparc.ihm.main.menu.itemmenurenderer.ItemMenuContextualOfCollaborateur;

	/**
	 * Classe gérant l'item renderer d'un collaborateur.
	 * Elle hérite de la classe EntityItemRenderer
	 * @see gestionparc.ihm.renderer.EntityItemRenderer
	 */
	public class CollaborateurItemRenderer extends EntityItemRenderer
	{
		public function CollaborateurItemRenderer():void
		{
			super();
		}

		/**
		 * Cette fonction surcharge le set data et met ainsi a jour le texte affiché, le style
		 * et appelle la fonction <fillArrayItemMenuCollaborateur pour créer la liste des items du menu
		 * et la fonction MenuContextualEntity  pour créer le menu.
		 */
		override public function set data(value:Object):void
		{
			lbLibelle.text="";
			lbLibelle.toolTip="";
			
			this.img.visible=false;
			this.lbLibelle.useHandCursor=false;
			
			setStyle("backgroundColor", 0xFFFFFF);
			setStyle("backgroundAlpha", 0);
						
			if (value != null)
			{
				super.data=value;

				if (value.hasOwnProperty("IDEMPLOYE") && value.IDEMPLOYE > 0)
				{	
					if (SpecificationVO.getInstance().idPool == SpecificationVO.POOL_PARC_GLOBAL_ID) // si vue parc global on n'affiche pas l'image action
					{
						this.img.visible=false;
					}
					else
					{
						this.img.visible=true;
						this.img.addEventListener(MouseEvent.CLICK, lineSelected);
					}					
					lbLibelle.addEventListener(MouseEvent.CLICK, ClickFicheHandler);
					lbLibelle.text=value.COLLABORATEUR;
					lbLibelle.toolTip=lbLibelle.text;
					this.lbLibelle.useHandCursor=true;
					
					if(value.INOUT == 0) 
					{
						setStyle("backgroundColor", 0xCCCCCC); // gris
						setStyle("backgroundAlpha", 1);
					}
				}
			}
		}

		/**
		 * Cette fonction est appelée lors d'un clique sur l'image de l'item renderer,
		 * elle place les informations concernant le menu de cet item et
		 * appelle la fonction qui affiche se dernier.
		 */
		private function lineSelected(evt:Event):void
		{
			/** on met a jour les information de la ligne selectionnée */
			SpecificationVO.getInstance().elementDataGridSelected=evt.currentTarget.owner.data;
			var ligne:Object=evt.currentTarget.owner.data;
			var Context:SpecificationVO = SpecificationVO.getInstance();
			
			if((Context.validSelectedIdPool == SpecificationVO.POOL_TOUT_PARC_ID)) //pas de profil == 'Mes pools'
			{
				Context.updateProfilCommande(ligne);//MYT-1606
				Context.idPool = ligne.IDPOOL;
				Context.labelPool = ligne.POOL;
			}

			this.arrayItemMenu=new Array();
			var myItemMenuContextualOfCollaborateur:ItemMenuContextualOfCollaborateur=new ItemMenuContextualOfCollaborateur();
			this.arrayItemMenu=myItemMenuContextualOfCollaborateur.returnArrayItemMenuContextual();
			this.menu=new MenuContextual(this.arrayItemMenu);

			clickIconActionHandler();
		}

		override protected function setFiche():void
		{
			this.fiche=new FicheCollaborateurIHM();
			(this.fiche as FicheCollaborateurIHM).isNewCollabFiche=false;
			(this.fiche as FicheCollaborateurIHM).idEmploye=(data as AbstractMatriceParcVO).IDEMPLOYE;
			(this.fiche as FicheCollaborateurIHM).idRow=(data as AbstractMatriceParcVO).ID;
		}
	}
}
