package gestionparc.ihm.renderer.entityrenderer
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.containers.TitleWindow;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.Spacer;
	import mx.core.Application;
	import mx.core.ScrollPolicy;
	import mx.managers.PopUpManager;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.MenuContextual;
	import gestionparc.services.cache.CacheDatas;

	/**
	 * Classe gérant un item renderer générique.
	 */
	public class EntityItemRenderer extends Canvas
	{

		protected var lbLibelle:Label;
		protected var img:Image;
		protected var arrayItemMenu:Array=null;
		protected var menu:MenuContextual=null;

		protected var fiche:TitleWindow;
		private var spacer:Spacer;

		[Embed(source="/assets/images/icon_show_actionV1.png")]
		private var iconClass:Class;

		/**
		 * Constructeur de la classe, permet de renseigner tous les variables pour créer un item renderer générique.
		 */
		public function EntityItemRenderer()
		{
			super();
			this.percentWidth=100;
			this.horizontalScrollPolicy=ScrollPolicy.OFF
			this.verticalScrollPolicy=ScrollPolicy.OFF

			lbLibelle=new Label();
			lbLibelle.setStyle("left", "2");
			lbLibelle.setStyle("textDecoration", "underline");
			lbLibelle.mouseChildren=false;
			lbLibelle.buttonMode=true;
		
			img=new Image();
			img.source=iconClass;
			img.width=16;
			img.height=13;
			img.setStyle("right","3");
			img.setStyle("top", "3");
			img.setStyle("bottom", "2");

			this.addChild(lbLibelle);
			this.addChild(img);
		}

		/**
		 * Cette fonction permet d'afficher le menu.
		 */
		protected function clickIconActionHandler():void
		{
			var maxMenuWidth:Number=100;
			var maxMenuHeight:Number=300;
			var gap:Number=100;
			var boolUp:Boolean=false;
			var boolLeft:Boolean=false;
			var pt:Point=new Point(mouseX, mouseY);
			pt=contentToGlobal(pt);

			if (pt.y + maxMenuHeight < owner.parent.parent.height)
				boolUp=false;
			else
				boolUp=true;

			if (pt.x + maxMenuWidth < owner.parent.width)
				boolLeft=false;
			else
				boolLeft=true;

			if (pt.y < owner.parent.width + owner.parent.height / 2 + gap)
			{
				menu.showMenu(pt.x, pt.y + 5, boolUp, boolLeft);
			}
			else
			{
				menu.showMenu(pt.x, pt.y, boolUp, boolLeft);
			}
		}

		protected function ClickFicheHandler(ev:MouseEvent):void
		{
			SpecificationVO.getInstance().elementDataGridSelected=this.data as AbstractMatriceParcVO;
			var ligne:Object=ev.currentTarget.owner.data;
			var Context:SpecificationVO = SpecificationVO.getInstance();
			
			if((Context.validSelectedIdPool == SpecificationVO.POOL_TOUT_PARC_ID)) //pas de profil == 'Mes pools'
			{
				Context.updateProfilCommande(ligne);//MYT-1606
				Context.idPool = ligne.IDPOOL;
				Context.labelPool = ligne.POOL;
			}
			
			//if((SpecificationVO.getInstance().idPool == SpecificationVO.POOL_TOUT_PARC_ID))
			//{
			//	SpecificationVO.getInstance().idPool = ligne.IDPOOL;
			//}
			
			if( lbLibelle !=null && lbLibelle.text!="")
			{
				setFiche();
				PopUpManager.addPopUp(fiche, Application.application as DisplayObject,true);
				PopUpManager.centerPopUp(fiche);
			}
		}

		protected function setFiche():void
		{
		}

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			lbLibelle.width=this.width - img.width - 3;
		}
	}
}
