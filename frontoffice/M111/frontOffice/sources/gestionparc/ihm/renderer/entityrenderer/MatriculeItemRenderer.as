package gestionparc.ihm.renderer.entityrenderer
{

	public class MatriculeItemRenderer extends EntityItemRenderer
	{
		public function MatriculeItemRenderer()
		{
		}

		override public function set data(value:Object):void
		{
			lbLibelle.text="";

			if (value != null)
			{
				super.data=value;

				lbLibelle.text=value.E_MATRICULE;

				this.img.visible=false;
				lbLibelle.useHandCursor=false;
				lbLibelle.buttonMode=false;
				lbLibelle.setStyle("textDecoration", "none");
			}
		}
	}
}
