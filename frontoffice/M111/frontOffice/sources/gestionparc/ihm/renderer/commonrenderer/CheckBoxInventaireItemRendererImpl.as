package gestionparc.ihm.renderer.commonrenderer
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.popup.PopupActionDate;
	
	import mx.controls.CheckBox;
	import mx.controls.dataGridClasses.DataGridListData;
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class CheckBoxInventaireItemRendererImpl extends CheckBox
	{
		public function CheckBoxInventaireItemRendererImpl()
		{
			super();
			addEventListener(Event.CHANGE, selectOneChbx);
		}

		override public function set data(value:Object):void
		{
			if (value)
			{
				super.data=value;
				if ((listData as DataGridListData).dataField == "dansInventaire")
				{
					enabled=false;
					if (value.dansInventaire == 1)
					{
						selected=true;
					}
					else
					{
						selected=false;
					}
				}
				else if ((listData as DataGridListData).dataField == "aEntrer")
				{
					if (value.aEntrer == true)
						selected=true;
					else
						selected=false;

					if (value.dansInventaire == 1)
					{
						enabled=false;
					}
					else
					{
						enabled=true;
					}
				}
				else if ((listData as DataGridListData).dataField == "aSortir")
				{
					if (value.aSortir == true)
						selected=true;
					else
						selected=false;

					if (value.dansInventaire == 0)
					{
						enabled=false;
					}
					else
					{
						enabled=true;
					}
				}

				if ((listData as DataGridListData).dataField == "facture")
				{
					enabled=false;
				}
			}
		}

		public function selectOneChbx(evt:Event):void
		{
			if (evt == null)
			{
				return;
			}

			if ((listData as DataGridListData).dataField == "aEntrer")
			{
				if (selected)
				{
					data.aEntrer=1;
					showPopupDate(evt);
				}
				else
				{
					data.aEntrer=0;
				}
			}

			else if ((listData as DataGridListData).dataField == "aSortir")
			{
				if (selected)
				{
					data.aSortir=1;
					showPopupDate(evt);
				}
				else
				{
					data.aSortir=0;
				}
			}
		}

		private function showPopupDate(evt:Event=null):void
		{
			var popup:PopupActionDate=new PopupActionDate();
			popup.addEventListener(gestionparcEvent.POPUP_RESILIATION_VALIDATED, popupDataHandler);
			PopUpManager.addPopUp(popup, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(popup);
			if ((evt.target.data as Object).aSortir == 1 && (evt.target.data as Object).boolAcces == "1")
			{
				popup.txtSortirProduitAcces.visible=true;
				popup.txtSortirProduitAcces.text=resourceManager.getString('M111', 'si_vous_sortez_produit_de_l_inventaire_pensez_eventuellement___modifier_etat_de_la_ligne');
				popup.txtSortirProduitAcces.setStyle("color", 0xFF0000);
				popup.txtSortirProduitAcces.includeInLayout=true;
			}
		}

		private function popupDataHandler(evt:gestionparcEvent):void
		{
			data.date=evt.obj as Date;

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.CHBX_SELECTION_CHANGE, true));
		}

	}
}
