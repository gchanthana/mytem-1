package gestionparc.ihm.renderer.headerrenderer
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.main.MainGridImpl;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.containers.Canvas;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;

	public class ToggleSortHeaderRendererImpl extends Canvas
	{
		private var ignoreSortArrowUpdate:Boolean;
		private static var renderers:EventDispatcher=new EventDispatcher();
		
		public var sortArrow:Image;
		public var labelHeader : Label;
		
		[Embed(source="/assets/images/sort_arrow_up.png")]
		private static var UpSortArrow:Class;

		[Embed(source="/assets/images/sort_arrow_down.png")]
		private static var DownSortArrow:Class;
		
		[Embed(source="/assets/images/noSort.png")]
		private static var noSortArrow:Class;

		public function ToggleSortHeaderRendererImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			addEventListener(MouseEvent.CLICK, onClick);
			renderers.addEventListener("sortArrowUpdated", onSortArrowUpdated);

			if (data)
			{
				sortArrow.source=noSortArrow=((data as DataGridColumn).dataField == SpecificationVO.getInstance().labelSort)?UpSortArrow:null;
				//sortArrow.source=noSortArrow;/** permet d'ajouter cette images dans l'IHM */
			}
		}
		
		private function onClick(evt:MouseEvent):void
		{
			updateSortArrow(true);
			ignoreSortArrowUpdate=true;
			
			renderers.dispatchEvent(new Event("sortArrowUpdated"));
			sortColumn();
		}

		private function onSortArrowUpdated(evt:Event):void
		{
			if (!ignoreSortArrowUpdate)
			{
				updateSortArrow(false);
			}
			ignoreSortArrowUpdate=false;
		}

		private function updateSortArrow(selected:Boolean):void
		{
			sortArrow.source=(selected ? (sortArrow.source == UpSortArrow ? DownSortArrow : UpSortArrow) : null);
		}

		private function sortColumn():void
		{
			(this.parentDocument as MainGridImpl).headerClickHandler((data as DataGridColumn).dataField);

		}

		private function get sortDescending():Boolean
		{
			return (sortArrow.source == UpSortArrow);
		}
	}
}
