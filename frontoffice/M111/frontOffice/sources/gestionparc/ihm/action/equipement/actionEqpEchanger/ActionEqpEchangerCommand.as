package gestionparc.ihm.action.equipement.actionEqpEchanger
{
	import gestionparc.ihm.action.ICommand;

	public class ActionEqpEchangerCommand implements ICommand
	{
		private var receiver:ActionEqpEchangerReceiver=null;

		public function ActionEqpEchangerCommand(rec:ActionEqpEchangerReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
