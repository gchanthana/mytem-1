package gestionparc.ihm.action.equipement.actionEqpVoirFiche
{
	import gestionparc.ihm.action.ICommand;

	public class ActionEqpVoirFicheCommand implements ICommand
	{
		private var receiver:ActionEqpVoirFicheReceiver=null;

		public function ActionEqpVoirFicheCommand(rec:ActionEqpVoirFicheReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
