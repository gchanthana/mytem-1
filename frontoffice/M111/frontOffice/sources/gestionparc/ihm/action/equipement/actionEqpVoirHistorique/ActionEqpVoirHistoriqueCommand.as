package gestionparc.ihm.action.equipement.actionEqpVoirHistorique
{
	import gestionparc.ihm.action.ICommand;

	public class ActionEqpVoirHistoriqueCommand implements ICommand
	{
		private var receiver:ActionEqpVoirHistoriqueReceiver=null;

		public function ActionEqpVoirHistoriqueCommand(rec:ActionEqpVoirHistoriqueReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
