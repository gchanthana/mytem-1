package gestionparc.ihm.action.equipement.actionEqpVoirFiche
{
	import flash.display.DisplayObject;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.ficheEntity.FicheEquipementIHM;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionEqpVoirFicheReceiver
	{
		private var ficheTerminal:FicheEquipementIHM;

		public function ActionEqpVoirFicheReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			ficheTerminal=new FicheEquipementIHM();
			ficheTerminal.nomPrenomCollab=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			ficheTerminal.matriculeCollab=SpecificationVO.getInstance().elementDataGridSelected.E_MATRICULE;
			ficheTerminal.idTerminal=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			PopUpManager.addPopUp(ficheTerminal, (Application.application as DisplayObject), true);
			PopUpManager.centerPopUp(ficheTerminal);
		}
	}
}
