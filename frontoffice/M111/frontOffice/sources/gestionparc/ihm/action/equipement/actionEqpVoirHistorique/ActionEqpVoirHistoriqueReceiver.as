package gestionparc.ihm.action.equipement.actionEqpVoirHistorique
{
	import flash.display.DisplayObject;

	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.historique.FicheHistoriqueIHM;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionEqpVoirHistoriqueReceiver
	{
		private var srcObject:Object;
		private var fiche:FicheHistoriqueIHM;

		public function ActionEqpVoirHistoriqueReceiver()
		{
		}

		// PUBLICS FUNCTIONS-------------------------------------------------------------------------	
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Cette fonction appelle l'affichage de l'IHM et renseigne au préalable les sources de l'action.
		 * </pre></p>
		 *
		 * @return void
		 */
		public function action():void
		{
			setSources();
			displayIHM(srcObject);
		}

		// PRIVATES FUNCTIONS-------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Cette fonction permet d'afficher l'IHM permettant de faire l'action.
		 * </pre></p>
		 *
		 * @param objectToAction
		 * @return void
		 */
		private function displayIHM(srcObject:Object):void
		{
			fiche=new FicheHistoriqueIHM();
			fiche.idEmploye=srcObject.idemp;
			fiche.idTerm=srcObject.idterm;
			fiche.idSim=srcObject.idsim;
			fiche.idSoustete=srcObject.idsstete;
			PopUpManager.addPopUp(fiche, (Application.application as DisplayObject), true);
			PopUpManager.centerPopUp(fiche);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Cette fonction renseigne les sources pour réaliser l'action.
		 * </pre></p>
		 *
		 * @return void
		 */
		private function setSources():void
		{
			srcObject=new Object();
			srcObject.idemp=0;
			srcObject.idterm=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			srcObject.idsim=0;
			srcObject.idsstete=0;
		}
	}
}
