package gestionparc.ihm.action.equipement.actionEqpEchanger
{
	import flash.display.DisplayObject;

	import gestionparc.ihm.fiches.equipement.FicheEqpEchangerIHM;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionEqpEchangerReceiver
	{
		private var fiche:FicheEqpEchangerIHM;

		public function ActionEqpEchangerReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheEqpEchangerIHM();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject ,true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
