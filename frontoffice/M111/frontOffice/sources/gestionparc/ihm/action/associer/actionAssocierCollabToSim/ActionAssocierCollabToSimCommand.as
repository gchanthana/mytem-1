package gestionparc.ihm.action.associer.actionAssocierCollabToSim
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierCollabToSimCommand implements ICommand
	{
		private var receiver:ActionAssocierCollabToSimReceiver=null;

		public function ActionAssocierCollabToSimCommand(rec:ActionAssocierCollabToSimReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
