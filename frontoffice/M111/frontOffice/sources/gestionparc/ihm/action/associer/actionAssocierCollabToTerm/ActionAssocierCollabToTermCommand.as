package gestionparc.ihm.action.associer.actionAssocierCollabToTerm
{
	import gestionparc.ihm.action.ICommand;
	import gestionparc.ihm.action.associer.actionAssocierCollabToTerm.ActionAssocierCollabToTermReceiver;

	public class ActionAssocierCollabToTermCommand implements ICommand
	{
		private var receiver:ActionAssocierCollabToTermReceiver=null;

		public function ActionAssocierCollabToTermCommand(rec:ActionAssocierCollabToTermReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
