package gestionparc.ihm.action.associer.actionAssocierCollabToExt
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.extension.FicheAssocierCollabToExt;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierCollabToExtReceiver
	{
		private var fiche:FicheAssocierCollabToExt;
		
		public function ActionAssocierCollabToExtReceiver()
		{
		}
		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			fiche = new FicheAssocierCollabToExt();	
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}