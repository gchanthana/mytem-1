package gestionparc.ihm.action.associer.actionAssocierSousEqpToEqp
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import gestionparc.ihm.fiches.equipement.FicheAssocierSsEqpToEqp;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierSousEqpToEqpReceiver extends EventDispatcher
	{
		private var fiche:FicheAssocierSsEqpToEqp;

		public function ActionAssocierSousEqpToEqpReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheAssocierSsEqpToEqp();
			PopUpManager.addPopUp(fiche, (Application.application as DisplayObject), true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
