package gestionparc.ihm.action.associer.actionAssocierLigneExtToExt
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierLigneExtToExtCommand implements ICommand
	{
		private var receiver:ActionAssocierLigneExtToExtReceiver=null;

		public function ActionAssocierLigneExtToExtCommand(rec:ActionAssocierLigneExtToExtReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
