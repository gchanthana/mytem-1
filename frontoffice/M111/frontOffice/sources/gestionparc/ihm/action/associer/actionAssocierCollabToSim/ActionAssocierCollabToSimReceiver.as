package gestionparc.ihm.action.associer.actionAssocierCollabToSim
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.sim.FicheAssocierCollabToSim;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierCollabToSimReceiver
	{
		private var fiche:FicheAssocierCollabToSim;
		
		public function ActionAssocierCollabToSimReceiver()
		{
		}
		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			fiche = new FicheAssocierCollabToSim();	
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}