package gestionparc.ihm.action.associer.actionAssocierEqpToLigne
{
	import flash.display.DisplayObject;

	import gestionparc.ihm.fiches.ligne.FicheAssocierEqpToLigne;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierEqpToLigneReceiver
	{
		private var fiche:FicheAssocierEqpToLigne;

		public function ActionAssocierEqpToLigneReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheAssocierEqpToLigne();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
