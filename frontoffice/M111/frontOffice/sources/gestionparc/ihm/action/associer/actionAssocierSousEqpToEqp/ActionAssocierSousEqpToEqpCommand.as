package gestionparc.ihm.action.associer.actionAssocierSousEqpToEqp
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierSousEqpToEqpCommand implements ICommand
	{
		private var receiver:ActionAssocierSousEqpToEqpReceiver=null;

		public function ActionAssocierSousEqpToEqpCommand(rec:ActionAssocierSousEqpToEqpReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
