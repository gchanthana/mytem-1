package gestionparc.ihm.action.associer.actionAssocierEqpToLigne
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierEqpToLigneCommand implements ICommand
	{
		private var receiver:ActionAssocierEqpToLigneReceiver=null;

		public function ActionAssocierEqpToLigneCommand(rec:ActionAssocierEqpToLigneReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
