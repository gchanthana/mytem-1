package gestionparc.ihm.action.associer.actionAssocierCollabToTerm
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.equipement.FicheAssocierCollabToEqp;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierCollabToTermReceiver
	{
		private var fiche:FicheAssocierCollabToEqp;
		
		public function ActionAssocierCollabToTermReceiver()
		{
		}
		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			fiche = new FicheAssocierCollabToEqp();
			PopUpManager.addPopUp(fiche,Application.application as DisplayObject);
			PopUpManager.centerPopUp(fiche);
		}
	}
}