package gestionparc.ihm.action.associer.ActionAssocierEqpToExt
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierEqpToExtCommand implements ICommand
	{
		private var receiver:ActionAssocierEqpToExtReceiver=null;

		public function ActionAssocierEqpToExtCommand(rec:ActionAssocierEqpToExtReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
