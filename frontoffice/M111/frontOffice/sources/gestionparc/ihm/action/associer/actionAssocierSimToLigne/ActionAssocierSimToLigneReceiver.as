package gestionparc.ihm.action.associer.actionAssocierSimToLigne
{
	import flash.display.DisplayObject;

	import gestionparc.ihm.fiches.ligne.FicheAssocierSimToLigne;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierSimToLigneReceiver
	{
		private var fiche:FicheAssocierSimToLigne;

		public function ActionAssocierSimToLigneReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheAssocierSimToLigne();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
