package gestionparc.ihm.action.associer.actionAssocierCollabToLigne
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierCollabToLigneCommand implements ICommand
	{
		private var receiver:ActionAssocierCollabToLigneReceiver=null;

		public function ActionAssocierCollabToLigneCommand(rec:ActionAssocierCollabToLigneReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
