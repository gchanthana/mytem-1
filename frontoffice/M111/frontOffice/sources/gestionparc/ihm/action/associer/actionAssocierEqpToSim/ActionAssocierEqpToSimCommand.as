package gestionparc.ihm.action.associer.actionAssocierEqpToSim
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierEqpToSimCommand implements ICommand
	{
		private var receiver:ActionAssocierEqpToSimReceiver=null;

		public function ActionAssocierEqpToSimCommand(rec:ActionAssocierEqpToSimReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
