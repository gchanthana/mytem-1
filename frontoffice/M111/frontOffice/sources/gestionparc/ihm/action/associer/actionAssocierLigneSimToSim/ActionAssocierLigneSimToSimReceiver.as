package gestionparc.ihm.action.associer.actionAssocierLigneSimToSim
{
	import flash.display.DisplayObject;

	import gestionparc.ihm.fiches.sim.FicheAssocierLigneToSim;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierLigneSimToSimReceiver
	{
		private var fiche:FicheAssocierLigneToSim;

		public function ActionAssocierLigneSimToSimReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheAssocierLigneToSim();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
