package gestionparc.ihm.action.associer.actionAssocierLigneSimToEqp
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierLigneSimtoEqpCommand implements ICommand
	{
		private var receiver:ActionAssocierLigneSimToEqpReceiver=null;

		public function ActionAssocierLigneSimtoEqpCommand(rec:ActionAssocierLigneSimToEqpReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
