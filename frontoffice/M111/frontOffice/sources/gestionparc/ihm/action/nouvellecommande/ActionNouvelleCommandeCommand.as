package gestionparc.ihm.action.nouvellecommande
{
	import gestionparc.ihm.action.ICommand;

	public class ActionNouvelleCommandeCommand implements ICommand
	{
		private var receiver:ActionNouvelleCommandeReceiver=null;

		public function ActionNouvelleCommandeCommand(rec:ActionNouvelleCommandeReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
