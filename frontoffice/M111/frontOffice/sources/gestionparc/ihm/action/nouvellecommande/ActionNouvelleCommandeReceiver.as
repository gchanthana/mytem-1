package gestionparc.ihm.action.nouvellecommande
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.gestion.GestionCommandeMobile;

	public class ActionNouvelleCommandeReceiver
	{

		private var srcObject:Object;
		private var gestionCommmande:GestionCommandeMobile;

		public function ActionNouvelleCommandeReceiver()
		{
		}

		/**
		 * Cette fonction appelle l'affichage de l'IHM et renseigne au préalable les sources de l'action.
		 */
		public function action():void
		{
			setSources();
			displayIHM(srcObject);
		}


		/**
		 * Cette fonction permet d'afficher l'IHM permettant de faire l'action.
		 */
		private function displayIHM(srcObject:Object):void
		{
			gestionCommmande=new GestionCommandeMobile(srcObject);
			gestionCommmande.goCommande();
		}

		/**
		 * Cette fonction renseigne les sources pour réaliser l'action.
		 */
		private function setSources():void
		{
			srcObject=new Object();
			srcObject.action=-1;
			srcObject.IDPOOL=SpecificationVO.getInstance().idPoolNotValid;
			srcObject.IDPROFIL=SpecificationVO.getInstance().idProfilNotValid;
			srcObject.IDREVENDEUR=SpecificationVO.getInstance().idRevendeurNotValid;
			srcObject.LIBELLE_CONCAT=SpecificationVO.getInstance().labelPoolNotValid + " - " + SpecificationVO.getInstance().labelProfilNotValid;
			srcObject.LIBELLE_POOL=SpecificationVO.getInstance().labelPoolNotValid;
			srcObject.LIBELLE_PROFIL=SpecificationVO.getInstance().labelProfilNotValid;
			srcObject.BOOLNEWCOMMANDE=true;

		}
	}
}