package gestionparc.ihm.action.collaborateur.actionCollabDepartEntreprise
{
	import gestionparc.ihm.action.ICommand;

	public class ActionCollabDepartEntrepriseCommand implements ICommand
	{
		private var receiver:ActionCollabDepartEntrepriseReceiver=null;

		public function ActionCollabDepartEntrepriseCommand(rec:ActionCollabDepartEntrepriseReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
