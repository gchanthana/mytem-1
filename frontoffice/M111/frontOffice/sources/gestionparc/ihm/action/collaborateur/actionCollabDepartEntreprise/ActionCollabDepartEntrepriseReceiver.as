package gestionparc.ihm.action.collaborateur.actionCollabDepartEntreprise
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.collaborateur.FicheDepartCollabIHM;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionCollabDepartEntrepriseReceiver extends EventDispatcher
	{
		private var ficheDepart:FicheDepartCollabIHM;

		public function ActionCollabDepartEntrepriseReceiver()
		{
			super();
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			ficheDepart=new FicheDepartCollabIHM();
			ficheDepart.ligne=SpecificationVO.getInstance().elementDataGridSelected;
			PopUpManager.addPopUp(ficheDepart, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(ficheDepart);
		}
	}
}
