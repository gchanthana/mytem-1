package gestionparc.ihm.action.collaborateur.actionCollabVoirHistorique
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.historique.FicheHistoriqueIHM;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionCollabVoirHistoriqueReceiver extends EventDispatcher
	{
		private var srcObject:Object;
		private var fiche:FicheHistoriqueIHM;

		public function ActionCollabVoirHistoriqueReceiver()
		{
			super();
		}

		/**
		 * Cette fonction appelle l'affichage de l'IHM et renseigne au préalable les sources de l'action.
		 */
		public function action():void
		{
			setSources();
			displayIHM(srcObject);
		}

		/**
		 * Cette fonction permet d'afficher l'IHM permettant de faire l'action.
		 */
		private function displayIHM(srcObject:Object):void
		{
			fiche=new FicheHistoriqueIHM();
			fiche.idEmploye=srcObject.idemp;
			fiche.idTerm=srcObject.idterm;
			fiche.idSim=srcObject.idsim;
			fiche.idSoustete=srcObject.idsstete;
			PopUpManager.addPopUp(fiche, (Application.application as DisplayObject), true);
			PopUpManager.centerPopUp(fiche);
		}

		/**
		 * Cette fonction renseigne les sources pour réaliser l'action.
		 */
		private function setSources():void
		{
			srcObject=new Object();
			srcObject.idemp=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			srcObject.idterm=0;
			srcObject.idsim=0;
			srcObject.idsstete=0;
		}
	}
}