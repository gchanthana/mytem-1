package gestionparc.ihm.action.collaborateur.actionCollabVoirFiche
{
	import gestionparc.ihm.action.ICommand;

	public class ActionCollabVoirFicheCommand implements ICommand
	{
		private var receiver:ActionCollabVoirFicheReceiver=null;

		public function ActionCollabVoirFicheCommand(rec:ActionCollabVoirFicheReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
