package gestionparc.ihm.action.collaborateur.actionCollabVoirHistorique
{
	import gestionparc.ihm.action.ICommand;

	public class ActionCollabVoirHistoriqueCommand implements ICommand
	{
		private var receiver:ActionCollabVoirHistoriqueReceiver=null;

		public function ActionCollabVoirHistoriqueCommand(rec:ActionCollabVoirHistoriqueReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
