package gestionparc.ihm.action.sim.actionsimechanger
{
	import gestionparc.ihm.action.ICommand;

	public class ActionSimEchangerCommand implements ICommand
	{
		private var receiver:ActionSimEchangerReceiver=null;

		public function ActionSimEchangerCommand(rec:ActionSimEchangerReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
