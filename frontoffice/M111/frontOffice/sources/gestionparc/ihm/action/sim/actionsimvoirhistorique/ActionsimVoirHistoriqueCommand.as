package gestionparc.ihm.action.sim.actionsimvoirhistorique
{
	import gestionparc.ihm.action.ICommand;

	public class ActionsimVoirHistoriqueCommand implements ICommand
	{
		private var receiver:ActionsimVoirHistoriqueReceiver=null;

		public function ActionsimVoirHistoriqueCommand(rec:ActionsimVoirHistoriqueReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
