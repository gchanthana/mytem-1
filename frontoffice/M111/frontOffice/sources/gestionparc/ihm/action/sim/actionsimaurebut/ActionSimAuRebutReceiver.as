package gestionparc.ihm.action.sim.actionsimaurebut
{
	import flash.display.DisplayObject;

	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.sim.FicheSimMettreAuRebusIHM;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionSimAuRebutReceiver
	{
		private var fiche:FicheSimMettreAuRebusIHM;

		public function ActionSimAuRebutReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheSimMettreAuRebusIHM();
			fiche.ligneSelected=SpecificationVO.getInstance().elementDataGridSelected;
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
