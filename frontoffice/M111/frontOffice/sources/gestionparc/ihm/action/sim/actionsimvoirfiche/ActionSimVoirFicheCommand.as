package gestionparc.ihm.action.sim.actionsimvoirfiche
{
	import gestionparc.ihm.action.ICommand;

	public class ActionSimVoirFicheCommand implements ICommand
	{
		private var receiver:ActionSimVoirFicheReceiver=null;

		public function ActionSimVoirFicheCommand(rec:ActionSimVoirFicheReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
