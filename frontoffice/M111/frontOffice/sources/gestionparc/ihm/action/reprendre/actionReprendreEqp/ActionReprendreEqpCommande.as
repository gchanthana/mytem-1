package gestionparc.ihm.action.reprendre.actionReprendreEqp
{
	import gestionparc.ihm.action.ICommand;

	public class ActionReprendreEqpCommande implements ICommand
	{
		private var receiver:ActionReprendreEqpReceiver=null;

		public function ActionReprendreEqpCommande(rec:ActionReprendreEqpReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
