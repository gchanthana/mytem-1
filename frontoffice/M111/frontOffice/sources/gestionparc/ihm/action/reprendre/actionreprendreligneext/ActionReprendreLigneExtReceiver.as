package gestionparc.ihm.action.reprendre.actionreprendreligneext
{
	import gestionparc.ihm.question.collaborateur.QuestionFicheReprendreLigneExt;

	public class ActionReprendreLigneExtReceiver
	{
		private var qn:QuestionFicheReprendreLigneExt;

		public function ActionReprendreLigneExtReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			qn=new QuestionFicheReprendreLigneExt();
		}
	}
}
