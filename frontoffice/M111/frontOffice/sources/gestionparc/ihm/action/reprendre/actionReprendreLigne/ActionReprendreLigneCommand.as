package gestionparc.ihm.action.reprendre.actionReprendreLigne
{
	import gestionparc.ihm.action.ICommand;

	public class ActionReprendreLigneCommand implements ICommand
	{
		private var receiver:ActionReprendreLigneReceiver=null;

		public function ActionReprendreLigneCommand(rec:ActionReprendreLigneReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
