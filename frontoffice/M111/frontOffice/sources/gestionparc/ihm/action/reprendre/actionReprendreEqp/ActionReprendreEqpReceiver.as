package gestionparc.ihm.action.reprendre.actionReprendreEqp
{
	import flash.display.DisplayObject;

	import gestionparc.ihm.fiches.collaborateur.FicheReprendreEqpIHM;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionReprendreEqpReceiver
	{
		private var popup:FicheReprendreEqpIHM=null;

		public function ActionReprendreEqpReceiver()
		{
		}

		/**
		 * Cette fonction appelle l'affichage de l'IHM et renseigne au préalable les sources de l'action.
		 */
		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			popup=new FicheReprendreEqpIHM();
			PopUpManager.addPopUp(popup, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(popup);
		}
	}
}
