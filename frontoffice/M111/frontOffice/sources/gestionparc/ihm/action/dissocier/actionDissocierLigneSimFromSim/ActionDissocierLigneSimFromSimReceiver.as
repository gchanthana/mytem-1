package gestionparc.ihm.action.dissocier.actionDissocierLigneSimFromSim
{
	import gestionparc.ihm.question.sim.QuestionFicheDissocierLigneFromSim;

	public class ActionDissocierLigneSimFromSimReceiver
	{
		private var qn:QuestionFicheDissocierLigneFromSim;

		public function ActionDissocierLigneSimFromSimReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			qn=new QuestionFicheDissocierLigneFromSim();
		}
	}
}
