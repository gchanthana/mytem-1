package gestionparc.ihm.action.dissocier.actionDissocierSimFromLigne
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDissocierSimFromLigneCommand implements ICommand
	{
		private var receiver:ActionDissocierSimFromLigneReceiver=null;

		public function ActionDissocierSimFromLigneCommand(rec:ActionDissocierSimFromLigneReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			this.receiver.action()
		}
	}
}
