package gestionparc.ihm.action.dissocier.actionDissocierLigneSimFromEqp
{
	import gestionparc.ihm.action.ICommand;
	
	public class ActionDissocierLigneSimFromEqpCommand implements ICommand
	{
		private var receiver : ActionDissocierLigneSimFromEqpReceiver = null;
		
		public function ActionDissocierLigneSimFromEqpCommand(rec:ActionDissocierLigneSimFromEqpReceiver)
		{
			this.receiver = rec;
		}
		
		public function execute():void
		{
			receiver.action();
		}
	}
}