package gestionparc.ihm.action.dissocier.actionDissocierCollabfromSim
{
	import gestionparc.ihm.question.sim.QuestionFicheDissocierCollabFromSim;

	public class ActionDissocierCollabFromSimReceiver
	{
		private var qn:QuestionFicheDissocierCollabFromSim;

		public function ActionDissocierCollabFromSimReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			qn=new QuestionFicheDissocierCollabFromSim();
		}
	}
}
