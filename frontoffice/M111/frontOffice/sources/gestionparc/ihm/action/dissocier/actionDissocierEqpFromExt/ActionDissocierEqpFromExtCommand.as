package gestionparc.ihm.action.dissocier.actionDissocierEqpFromExt
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDissocierEqpFromExtCommand implements ICommand
	{
		private var receiver:ActionDissocierEqpFromExtReceiver=null;

		public function ActionDissocierEqpFromExtCommand(rec:ActionDissocierEqpFromExtReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
