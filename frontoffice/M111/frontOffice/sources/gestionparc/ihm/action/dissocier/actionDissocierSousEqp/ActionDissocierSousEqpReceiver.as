package gestionparc.ihm.action.dissocier.actionDissocierSousEqp
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.equipement.FicheDissocierSsEqpFromEqpIHM;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionDissocierSousEqpReceiver
	{
		private var fiche:FicheDissocierSsEqpFromEqpIHM;
		
		public function ActionDissocierSousEqpReceiver()
		{
		}
		public function action():void
		{
			fiche = new FicheDissocierSsEqpFromEqpIHM();
			PopUpManager.addPopUp(fiche,Application.application as DisplayObject);
			PopUpManager.centerPopUp(fiche);
		}
	}
}