package gestionparc.ihm.action.dissocier.actionDissocierEqpFromSim
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDissocierEqpFromSimCommand implements ICommand
	{
		private var receiver:ActionDissocierEqpFromSimReceiver=null;

		public function ActionDissocierEqpFromSimCommand(rec:ActionDissocierEqpFromSimReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
