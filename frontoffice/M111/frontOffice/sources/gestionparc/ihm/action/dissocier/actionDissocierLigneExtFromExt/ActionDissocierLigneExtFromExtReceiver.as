package gestionparc.ihm.action.dissocier.actionDissocierLigneExtFromExt
{
	import gestionparc.ihm.question.extention.QuestionFicheDissocierLigneFromExt;

	public class ActionDissocierLigneExtFromExtReceiver
	{
		private var qn:QuestionFicheDissocierLigneFromExt;

		public function ActionDissocierLigneExtFromExtReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			qn=new QuestionFicheDissocierLigneFromExt();
		}
	}
}
