package gestionparc.ihm.action.dissocier.actionDissocierExtFromLigne
{
	import gestionparc.ihm.action.ICommand;
	
	public class ActionDissocierExtFromLigneCommand implements ICommand
	{
		private var receiver : ActionDissocierExtFromLigneReceiver = null;
		
		public function ActionDissocierExtFromLigneCommand(rec:ActionDissocierExtFromLigneReceiver)
		{
			this.receiver = rec;
		}
		
		public function execute():void
		{
			this.receiver.action()
		}
	}
}