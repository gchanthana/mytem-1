package gestionparc.ihm.action.dissocier.actionDissocierEqpFromSim
{
	import gestionparc.ihm.question.sim.QuestionFicheDissocierEqpFromSim;

	public class ActionDissocierEqpFromSimReceiver
	{
		private var qn:QuestionFicheDissocierEqpFromSim;
		
		public function ActionDissocierEqpFromSimReceiver()
		{
		}
		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			qn = new QuestionFicheDissocierEqpFromSim();
		}
	}
}