package gestionparc.ihm.action.dissocier.actionDissocierSousEqp
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDissocierSousEqpCommand implements ICommand
	{
		private var receiver:ActionDissocierSousEqpReceiver=null;

		public function ActionDissocierSousEqpCommand(rec:ActionDissocierSousEqpReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			this.receiver.action();
		}
	}
}
