package gestionparc.ihm.action.dissocier.actionDissocierEqpFromLigne
{
	import gestionparc.ihm.question.ligne.QuestionDissocierEqpFromLigne;

	public class ActionDissocierEqpFromLigneReceiver
	{
		private var qn:QuestionDissocierEqpFromLigne;

		public function ActionDissocierEqpFromLigneReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			qn=new QuestionDissocierEqpFromLigne();
		}
	}
}
