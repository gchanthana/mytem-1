package gestionparc.ihm.action.dissocier.actionDissocierLigneFromEqp
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDissocierLigneFromEqpCommand implements ICommand
	{
		private var receiver:ActionDissocierLigneFromEqpReceiver=null;

		public function ActionDissocierLigneFromEqpCommand(rec:ActionDissocierLigneFromEqpReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
