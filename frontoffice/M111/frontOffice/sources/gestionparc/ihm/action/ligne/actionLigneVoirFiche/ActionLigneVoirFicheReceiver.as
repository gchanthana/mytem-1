package gestionparc.ihm.action.ligne.actionLigneVoirFiche
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.ficheEntity.FicheLigneIHM;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;
	
	public class ActionLigneVoirFicheReceiver extends EventDispatcher
	{
		private var fiche:FicheLigneIHM;
		
		public function ActionLigneVoirFicheReceiver()
		{
			super();
		}
		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			fiche  = new FicheLigneIHM();
			fiche.idsoustete = SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE;
			PopUpManager.addPopUp(fiche, (Application.application as DisplayObject), true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}