package gestionparc.ihm.action.ligne.actionLigneCommanderOption
{
	import gestionparc.ihm.action.ICommand;

	public class ActionLigneCommanderOptionCommand implements ICommand
	{
		private var receiver:ActionLigneCommanderOptionReceiver=null;

		public function ActionLigneCommanderOptionCommand(rec:ActionLigneCommanderOptionReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
