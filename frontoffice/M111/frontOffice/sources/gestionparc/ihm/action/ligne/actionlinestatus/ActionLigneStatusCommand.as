package gestionparc.ihm.action.ligne.actionlinestatus
{
	import gestionparc.ihm.action.ICommand;
	import gestionparc.ihm.action.IReceiver;
	
	public class ActionLigneStatusCommand implements ICommand
	{
		private var _receiver:IReceiver=null;
		
		public function ActionLigneStatusCommand(receiver:IReceiver)
		{
			_receiver = receiver;
		}
		
		public function execute():void
		{
			if(_receiver)
			{
			 _receiver.action();
			}
			else
			{
				throw Error("this action has no receiver");
			}
		}
	}
}