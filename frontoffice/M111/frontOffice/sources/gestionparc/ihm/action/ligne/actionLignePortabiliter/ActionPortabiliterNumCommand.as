package gestionparc.ihm.action.ligne.actionLignePortabiliter
{
	import gestionparc.ihm.action.ICommand;
	
	public class ActionPortabiliterNumCommand implements ICommand
	{
		private var receiver:ActionPortabilterNumReceiver=null;
		
		public function ActionPortabiliterNumCommand(rec:ActionPortabilterNumReceiver)
		{
			receiver = rec;
		}
		
		public function execute():void
		{
			receiver.action();
		}
	}
}