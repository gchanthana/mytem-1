package gestionparc.ihm.action.ligne.actionLigneRenumeroter
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.ligne.FicheRenumeroterLigneIHM;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionLigneRenumeroterReceiver
	{
		private var fiche:FicheRenumeroterLigneIHM;
		
		public function ActionLigneRenumeroterReceiver()
		{
		}
		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			fiche = new FicheRenumeroterLigneIHM();
			PopUpManager.addPopUp(fiche,Application.application as DisplayObject,true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}