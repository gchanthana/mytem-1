package gestionparc.ihm.action.ligne.actionLignePortabiliter
{
	import mx.utils.ObjectUtil;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.gestion.GestionCommandeMobile;
	
	public class ActionPortabilterNumReceiver
	{
		private var srcObject:Object;
		private var gestionCommmande:GestionCommandeMobile;
		
		public function ActionPortabilterNumReceiver()
		{
		}
		
		public function action():void
		{
			setSources();
			displayIHM(srcObject);
		}
		
		private function displayIHM(srcObject:Object):void
		{
			gestionCommmande=new GestionCommandeMobile(srcObject);
			gestionCommmande.goModification();
		}
		
		/**
		 * Cette fonction renseigne les sources pour réaliser l'action.
		 */
		private function setSources():void
		{
			srcObject=new Object();
			srcObject.ligne=SpecificationVO.getInstance().elementDataGridSelected;
			srcObject.action=-1;
			srcObject.IDTYPECOMMANDE=12;
			//srcObject.IDTYPEOPERATION=12;
			srcObject.IDPOOL=SpecificationVO.getInstance().idPool;
			srcObject.IDPROFIL=SpecificationVO.getInstance().idProfil;
			srcObject.IDREVENDEUR=SpecificationVO.getInstance().idRevendeur;
			srcObject.LIBELLE_CONCAT=SpecificationVO.getInstance().labelPool + " - " + SpecificationVO.getInstance().labelProfil;
			srcObject.LIBELLE_POOL=SpecificationVO.getInstance().labelPool;
			srcObject.LIBELLE_PROFIL=SpecificationVO.getInstance().labelProfil;
			srcObject.IDSEGMENT=1;
			srcObject.BOOLNEWCOMMANDE=true;
			
			trace("############## M111 ligne : " + ObjectUtil.toString(srcObject));
		}
	}
}