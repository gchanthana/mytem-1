package gestionparc.ihm.action.ligne.actionLigneVoirHistorique
{
	import gestionparc.ihm.action.ICommand;

	public class ActionLigneVoirHistoriqueCommand implements ICommand
	{
		private var receiver:ActionLigneVoirHistoriqueReceiver=null;

		public function ActionLigneVoirHistoriqueCommand(rec:ActionLigneVoirHistoriqueReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
