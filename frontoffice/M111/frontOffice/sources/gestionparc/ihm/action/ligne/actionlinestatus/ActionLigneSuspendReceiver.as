package gestionparc.ihm.action.ligne.actionlinestatus
{
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.gestion.GestionCommandeMobile;
	import gestionparc.ihm.action.IReceiver;
	import gestionparc.services.ligne.LigneServices;
	import gestionparc.utils.ConfirmDialogBox;

	public class ActionLigneSuspendReceiver  implements IReceiver
	{
		private var _ligne:AbstractMatriceParcVO;
		private var _service:LigneServices=new LigneServices();
		
		private var srcObject:Object;
		private var gestionCommmande:GestionCommandeMobile;
		
		public function ActionLigneSuspendReceiver(ligne:AbstractMatriceParcVO)
		{
			_ligne = ligne;
		}
		
		public function action():void
		{
			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("MASTER_PARC_GFC")
				&& moduleGestionParcIHM.globalParameter.userAccess.MASTER_PARC_GFC == 1)
			{
				setSources();
				displayIHM(srcObject);
			}
			else
			{
				suspend();
			}
		}
		
		private function suspend():void
		{
			var message:String = ResourceManager.getInstance().getString("M111","confirmation_msg");
			var titre:String = ResourceManager.getInstance().getString("M111","Suspendre") + ' ' + _ligne.SOUS_TETE;
			
			
			ConfirmDialogBox.display(message,titre,execute);
			
		}
		
		private function execute(ce:CloseEvent):void
		{
			if(ce.detail == Alert.OK)
			{
				_service.updateEtatLigne(_ligne,2);	
			}
			
		}
		
		private function displayIHM(srcObject:Object):void
		{
			gestionCommmande=new GestionCommandeMobile(srcObject);
			gestionCommmande.goSuspension();
		}
		
		/**
		 * Cette fonction renseigne les sources pour réaliser l'action.
		 */
		private function setSources():void
		{
			srcObject=new Object();
			srcObject.ligne=SpecificationVO.getInstance().elementDataGridSelected;
			//--- 1-> NOUVELLE LIGNE OU EQ (mobile, fixe, data), 2-> REENGAGEMENT, 3-> MODIFICATION OPTION, 4-> SUSPENSION, 	5-> REACTIVATION, 6-> RESILIATION
			srcObject.IDTYPECOMMANDE = 4;
			srcObject.IDPOOL=SpecificationVO.getInstance().idPool;
			srcObject.IDPROFIL=SpecificationVO.getInstance().idProfil;
			srcObject.IDREVENDEUR=SpecificationVO.getInstance().idRevendeur;
			srcObject.LIBELLE_CONCAT=SpecificationVO.getInstance().labelPool + " - " + SpecificationVO.getInstance().labelProfil;
			srcObject.LIBELLE_POOL=SpecificationVO.getInstance().labelPool;
			srcObject.LIBELLE_PROFIL=SpecificationVO.getInstance().labelProfil;
			srcObject.BOOLNEWCOMMANDE=true;
			srcObject.IDSEGMENT=(SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEMOBILE) ? 1 : 2;
		}
		 
	}
}

