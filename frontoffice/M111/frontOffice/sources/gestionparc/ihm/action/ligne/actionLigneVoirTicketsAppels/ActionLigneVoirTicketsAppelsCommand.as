package gestionparc.ihm.action.ligne.actionLigneVoirTicketsAppels
{
	import gestionparc.ihm.action.ICommand;

	public class ActionLigneVoirTicketsAppelsCommand implements ICommand
	{
		private var receiver:ActionLigneVoirTicketsAppelsReceiver=null;
		
		public function ActionLigneVoirTicketsAppelsCommand(rec:ActionLigneVoirTicketsAppelsReceiver)
		{
			this.receiver = rec;
		}
		
		public function execute():void
		{
			receiver.action();
		}
	}
}