package gestionparc.ihm.action.ligne.actionLigneVoirFiche
{
	import gestionparc.ihm.action.ICommand;

	public class ActionLigneVoirFicheCommand implements ICommand
	{
		private var receiver:ActionLigneVoirFicheReceiver=null;

		public function ActionLigneVoirFicheCommand(rec:ActionLigneVoirFicheReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
