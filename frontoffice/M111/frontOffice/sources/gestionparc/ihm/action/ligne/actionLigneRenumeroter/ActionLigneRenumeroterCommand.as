package gestionparc.ihm.action.ligne.actionLigneRenumeroter
{
	import gestionparc.ihm.action.ICommand;

	public class ActionLigneRenumeroterCommand implements ICommand
	{
		private var receiver:ActionLigneRenumeroterReceiver=null;

		public function ActionLigneRenumeroterCommand(rec:ActionLigneRenumeroterReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
