package gestionparc.ihm.action.ligne.actionFournirSimStock
{
	import gestionparc.ihm.action.ICommand;
	
	public class ActionFournirSimStockCommand implements ICommand
	{
		private var receiver:ActionFournirSIMStokReceiver = null;
		public function ActionFournirSimStockCommand(rec:ActionFournirSIMStokReceiver)
		{
			receiver = rec;
		}
		
		public function execute():void
		{
			receiver.action();
		}
	}
}