package gestionparc.ihm.action.ligne.actionLigneResiliation
{
	import gestionparc.ihm.action.ICommand;

	public class ActionLigneResiliationCommand implements ICommand
	{
		private var receiver:ActionLigneResiliationReceiver=null;

		public function ActionLigneResiliationCommand(rec:ActionLigneResiliationReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
