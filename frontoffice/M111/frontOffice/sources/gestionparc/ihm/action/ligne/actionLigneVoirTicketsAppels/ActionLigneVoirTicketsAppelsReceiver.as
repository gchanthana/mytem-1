package gestionparc.ihm.action.ligne.actionLigneVoirTicketsAppels
{
	import flash.display.DisplayObject;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.ficheEntity.FicheTicketsAppelsIHM;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionLigneVoirTicketsAppelsReceiver
	{
		private var srcObject:Object;
		private var fiche:FicheTicketsAppelsIHM;
		
		public function ActionLigneVoirTicketsAppelsReceiver()
		{
		}
		
		/**
		 * Cette fonction appelle l'affichage de l'IHM et renseigne au préalable les sources de l'action.
		 */
		public function action():void
		{
			setSources();
			displayIHM(srcObject);
		}
		
		/**
		 * Cette fonction permet d'afficher l'IHM permettant de faire l'action.
		 */
		private function displayIHM(srcObject:Object):void
		{
			fiche = new FicheTicketsAppelsIHM();
			/*fiche.idEmploye = srcObject.idemp;
			fiche.idTerm=srcObject.idterm;
			fiche.idSim = srcObject.idsim;*/
			fiche.idSous_tete = SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE;
			fiche.ligne = SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE;
			PopUpManager.addPopUp(fiche, (Application.application as DisplayObject), true);
			PopUpManager.centerPopUp(fiche);
		}
		/**
		 * Cette fonction renseigne les sources pour réaliser l'action.
		 */
		private function setSources():void
		{
		
		}
		
	}
}