package gestionparc.ihm.action.ligne.actionCommanderNvlSim
{
	import mx.utils.ObjectUtil;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.gestion.GestionCommandeMobile;

	public class ActionCommanderNvlSimReceiver
	{
		private var srcObject:Object;
		private var gestionCommmande:GestionCommandeMobile;
		
		public function ActionCommanderNvlSimReceiver()
		{
		}
		
		public function action():void
		{
			setSources();
			displayIHM(srcObject);
		}
		
		private function displayIHM(srcObject:Object):void
		{
			gestionCommmande=new GestionCommandeMobile(srcObject);
			gestionCommmande.goModification();
		}
		
		/**
		 * Cette fonction renseigne les sources pour réaliser l'action.
		 */
		private function setSources():void
		{
			srcObject=new Object();
			srcObject.ligne=SpecificationVO.getInstance().elementDataGridSelected;
			// Dans la gestionParc M111 : //--- 1-> NOUVELLE LIGNE OU EQ (mobile, fixe, data), 2-> REENGAGEMENT, 3-> MODIFICATION OPTION, 4-> SUSPENSION, 	5-> REACTIVATION, 6-> RESILIATION
			// Dans la commande 	M16 : //--- 1-> LIGNE + EQ, 								 2-> LIGNE, 	   3-> EQUIPEMENT, 			4-> CHARGER MODÈLE, 5-> REENGAGEMENT, 6-> MODIFICATION OPTION, 7-> SUSPENSION, 8-> REACTIVATION, 9-> RESILIATION, 10-> EQUIPEMENT FIXE
			srcObject.IDTYPECOMMANDE = 11;
			srcObject.IDPOOL=SpecificationVO.getInstance().idPool;
			srcObject.IDPROFIL=SpecificationVO.getInstance().idProfil;
			srcObject.IDREVENDEUR=SpecificationVO.getInstance().idRevendeur;
			srcObject.LIBELLE_CONCAT=SpecificationVO.getInstance().labelPool + " - " + SpecificationVO.getInstance().labelProfil;
			srcObject.LIBELLE_POOL=SpecificationVO.getInstance().labelPool;
			srcObject.LIBELLE_PROFIL=SpecificationVO.getInstance().labelProfil;
			srcObject.BOOLNEWCOMMANDE=true;
			srcObject.IDSEGMENT=(SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEMOBILE) ? 1 : 2;
		}
	}
}