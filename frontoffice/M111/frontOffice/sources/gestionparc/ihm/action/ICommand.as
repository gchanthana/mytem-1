package gestionparc.ihm.action
{
	/**
	 * Interface permettant de réaliser la pattern Command
	 */
	public interface ICommand
	{
		function execute():void;
	}
}
