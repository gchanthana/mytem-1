package gestionparc.ihm.action.mdm.actionGeolocalisation
{
	import composants.util.ConsoviewAlert;

	import flash.display.DisplayObject;
	import flash.events.Event;

	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.MDMDataEvent;
	import gestionparc.ihm.action.IReceiver;
	import gestionparc.ihm.fiches.popup.PopUpLocationIHM;
	import gestionparc.ihm.fiches.popup.PopUpLocationImpl;
	import gestionparc.services.mdm.MDMService;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionGeolocalisationReceiver implements IReceiver
	{

		private var _popUpLocation:PopUpLocationIHM;

		private var _mdmSrv:MDMService;

		private var _isDroid:Boolean=false;

		public function ActionGeolocalisationReceiver()
		{
			_mdmSrv=new MDMService();
		}

		public function action():void
		{
			popUpLocation();
		}

		private function deviceInfoHandler(mde:MDMDataEvent):void
		{
			if (_mdmSrv.myDatas.arrListeDevice != null && _mdmSrv.myDatas.arrListeDevice.length > 0)
			{
				for each (var item:Object in _mdmSrv.myDatas.arrListeDevice)
				{
					if (item.NAME.toString().toLowerCase() == "plateforme")
					{
						if (item.VALUE.toString().toLowerCase() == 'android')
						{
							_isDroid=true;
							break;
						}
					}
				}
			}

			if (_isDroid)
			{ // Pour ANDROID la localisation est supportée
				// Afficher la fenetre de localisation
				popUpLocation();
			}
			else // Pour iOS la localisation n'est pas supportée
				ConsoviewAlert.afficherAlertInfo("Votre terminal n'est pas compatible", "MDM", null);
		}

		private function updateLocationPopup(eventObject:Event):void
		{
			_popUpLocation=eventObject.target as PopUpLocationIHM;
			_popUpLocation.setInfos(SpecificationVO.getInstance().elementDataGridSelected);
		}

		private function popUpLocation():void
		{
			_popUpLocation=new PopUpLocationIHM();
			_popUpLocation.addEventListener(PopUpLocationImpl.POPUP_UPDATED_EVENT, updateLocationPopup);

			PopUpManager.addPopUp(_popUpLocation, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpLocation);
		}

	}
}
