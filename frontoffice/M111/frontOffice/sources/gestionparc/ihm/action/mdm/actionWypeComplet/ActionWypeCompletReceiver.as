package gestionparc.ihm.action.mdm.actionWypeComplet
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.ConfirmWithPassWordIHMEvent;
	import gestionparc.ihm.action.IReceiver;
	import gestionparc.services.mdm.MDMService;
	import gestionparc.utils.ConfirmWithPassBoxManager;

	import mx.resources.ResourceManager;

	public class ActionWypeCompletReceiver implements IReceiver
	{

		private var _confirmBoxManager:ConfirmWithPassBoxManager;

		private var _mdmSrv:MDMService;

		public function ActionWypeCompletReceiver()
		{
			_mdmSrv=new MDMService();
		}

		public function action():void
		{
			var msg:String=ResourceManager.getInstance().getString('M111', 'Effacement_complet_du_terminal___') + SpecificationVO.getInstance().elementDataGridSelected.T_IMEI;

			_confirmBoxManager=new ConfirmWithPassBoxManager();
			_confirmBoxManager.displayConfirmBox(msg, WYPE_COMP_TERM_CloseHandler, SpecificationVO.getInstance().displayObjectRef);
		}

		private function WYPE_COMP_TERM_CloseHandler(e:ConfirmWithPassWordIHMEvent):void
		{
			if (e.detail == ConfirmWithPassWordIHMEvent.PASS_KO)
			{
				_mdmSrv.wipe(SpecificationVO.getInstance().elementDataGridSelected.T_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_SERIAL_NUMBER, Number(SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL), true);

				_confirmBoxManager.hideConfirmBox();

					//SpecificationVO.getInstance().dispatchEventRefresh();
			}
		}
	}
}
