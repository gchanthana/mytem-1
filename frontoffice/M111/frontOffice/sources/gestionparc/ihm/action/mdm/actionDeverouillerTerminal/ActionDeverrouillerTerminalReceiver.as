package gestionparc.ihm.action.mdm.actionDeverouillerTerminal
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.MDMDataEvent;
	import gestionparc.ihm.action.IReceiver;
	import gestionparc.ihm.fiches.popup.PopUpPinCodeDeviceIHM;
	import gestionparc.services.mdm.MDMService;
	
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class ActionDeverrouillerTerminalReceiver implements IReceiver
	{

		private var _popUpPinCode:PopUpPinCodeDeviceIHM;

		private var _mdmSrv:MDMService;

		private var _isDroid:Boolean=false;

		public function ActionDeverrouillerTerminalReceiver()
		{
			_mdmSrv=new MDMService();
		}

		public function action():void
		{
			_mdmSrv.unlock(SpecificationVO.getInstance().elementDataGridSelected.T_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_SERIAL_NUMBER, SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL, "0000");
		}

		private function deviceInfoHandler(mde:MDMDataEvent):void
		{
			if (_mdmSrv.myDatas.arrListeDevice != null && _mdmSrv.myDatas.arrListeDevice.length > 0)
			{
				for each (var item:Object in _mdmSrv.myDatas.arrListeDevice)
				{
					if (item.NAME.toString().toLowerCase() == "plateforme")
					{
						if (item.VALUE.toString().toLowerCase() == 'android')
							_isDroid=true;
					}
				}
			}

			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'Etes_vous_surs_de_vouloir_verrouiller_ce'), "MDM", resultAlertLock);
		}

		private function resultAlertLock(ce:CloseEvent):void
		{
			if (ce.detail == Alert.OK)
				popUpPinCode();
		}

		private function popUpPinCode():void
		{
			_popUpPinCode=new PopUpPinCodeDeviceIHM();
			_popUpPinCode.addEventListener('PIN_CODE_DEVICE', resultPopUpPinCodeHandler);

			if (_isDroid)
				_popUpPinCode.setInfos(SpecificationVO.getInstance().elementDataGridSelected, 
					ResourceManager.getInstance().getString('M111','Changer_le_code_de_verrouillage'), 
					ResourceManager.getInstance().getString('M111','Nouveau_code_de_verrouillage__'),
							 '', true, _isDroid, false);

			else
				_popUpPinCode.setInfos(SpecificationVO.getInstance().elementDataGridSelected, 
					ResourceManager.getInstance().getString('M111','Changer_le_code_de_verrouillage'), 
					ResourceManager.getInstance().getString('M111','Nouveau_code_de_verrouillage__'), 
					ResourceManager.getInstance().getString('M111','Cette_action_va_initier_la_s_quence_de_'), true, _isDroid);

			PopUpManager.addPopUp(_popUpPinCode, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpPinCode);
		}

		private function resultPopUpPinCodeHandler(e:Event):void
		{
			if (e.type == 'PIN_CODE_DEVICE')
			{
				var statut:Object=_popUpPinCode.getInfos();

				PopUpManager.removePopUp(_popUpPinCode);

				_mdmSrv.unlock(SpecificationVO.getInstance().elementDataGridSelected.T_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_SERIAL_NUMBER, SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL, statut.PINCODE);

				//SpecificationVO.getInstance().dispatchEventRefresh();
			}
		}
	}
}
