package gestionparc.ihm.action.mdm.actionVerrouillerTerminal
{
	import gestionparc.ihm.action.ICommand;

	public class ActionVerrouillerTerminalCommand implements ICommand
	{
		private var receiver:ActionVerrouillerTerminalReceiver=null;

		public function ActionVerrouillerTerminalCommand(rec:ActionVerrouillerTerminalReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
