package gestionparc.ihm.action.mdm.actionRevocation
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.action.IReceiver;
	import gestionparc.ihm.fiches.popup.PopUpUnmanageDeviceIHM;
	import gestionparc.services.mdm.MDMService;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionRevocationReceiver implements IReceiver
	{
		private var _popUpUnmanageDevice:PopUpUnmanageDeviceIHM;

		private var _mdmSrv:MDMService;

		public function ActionRevocationReceiver()
		{
			_mdmSrv=new MDMService();
		}

		public function action():void
		{
			_popUpUnmanageDevice=new PopUpUnmanageDeviceIHM();
			_popUpUnmanageDevice.setInfos(SpecificationVO.getInstance().elementDataGridSelected);
			_popUpUnmanageDevice.addEventListener('UNMANAGE_DEVICE', resultPopUpRevocationHandler);

			PopUpManager.addPopUp(_popUpUnmanageDevice, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpUnmanageDevice);
		}

		private function resultPopUpRevocationHandler(e:Event):void
		{
			if (e.type == 'UNMANAGE_DEVICE')
			{
				var statut:Object=_popUpUnmanageDevice.getInfos();

				PopUpManager.removePopUp(_popUpUnmanageDevice);

				_mdmSrv.revoke(SpecificationVO.getInstance().elementDataGridSelected.T_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_SERIAL_NUMBER, SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL, statut.REMOVEPERMANENTLY);

				//SpecificationVO.getInstance().dispatchEventRefresh();
			}
		}
	}
}
