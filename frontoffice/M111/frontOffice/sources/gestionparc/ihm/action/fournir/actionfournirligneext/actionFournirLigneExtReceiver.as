package gestionparc.ihm.action.fournir.actionfournirligneext
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import gestionparc.ihm.fiches.collaborateur.FicheFournirLigneExt;
	import gestionparc.ihm.fiches.collaborateur.FicheFournirLigneSim;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	/**
	 * Classe gérant l'IHM permettant de réaliser l'action ainsi que le service correspondant.
	 * Elle définit également les sources et les cibles de l'action.
	 */
	public class actionFournirLigneExtReceiver extends EventDispatcher
	{
		private var fiche:FicheFournirLigneExt;
		
		public function actionFournirLigneExtReceiver()
		{
		}
		public function action():void
		{
			fiche = new FicheFournirLigneExt();
			PopUpManager.addPopUp(fiche,Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}