package gestionparc.ihm.action.fournir.actionfournirligne
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.collaborateur.FicheFournirLigne;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionFournirLigneReceiver
	{
		private var fiche:FicheFournirLigne=null;

		public function ActionFournirLigneReceiver()
		{
		}

		public function action():void
		{
			fiche=new FicheFournirLigne()
			PopUpManager.addPopUp(fiche, (Application.application as DisplayObject), true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
