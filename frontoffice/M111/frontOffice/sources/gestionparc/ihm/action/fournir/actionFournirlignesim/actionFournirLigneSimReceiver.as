package gestionparc.ihm.action.fournir.actionFournirlignesim
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import gestionparc.ihm.fiches.collaborateur.FicheFournirLigneSim;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	/**
	 * Classe gérant l'IHM permettant de réaliser l'action ainsi que le service correspondant.
	 * Elle définit également les sources et les cibles de l'action.
	 */
	public class actionFournirLigneSimReceiver extends EventDispatcher
	{
		private var fiche:FicheFournirLigneSim;
		
		public function actionFournirLigneSimReceiver()
		{
		}
		public function action():void
		{
			fiche = new FicheFournirLigneSim();
			PopUpManager.addPopUp(fiche,Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}