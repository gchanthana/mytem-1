package gestionparc.ihm.action.fournir.actionFournirlignesim
{
	import gestionparc.ihm.action.ICommand;

	/**
	 * Classe gérant la pattern command pour l'association d'un collab à une ligne sim.
	 * Cette classe implemente l'interface ICommand
	 * @see gestionparc.ihm.action.ICommand
	 */
	public class actionFournirLigneSimCommand implements ICommand
	{

		private var receiver:actionFournirLigneSimReceiver=null;

		/**
		 * Constructeur de la classe AssocierCollabLigneSimCommand, il renseigne le receiver.
		 */
		public function actionFournirLigneSimCommand(rec:actionFournirLigneSimReceiver)
		{
			this.receiver=rec;
		}

		/**
		 * Cette fonction représente le "execute()" de la pattern command.
		 * Elle delegue les actions a realiser au receiver.
		 */
		public function execute():void
		{
			receiver.action();
		}
	}

}
