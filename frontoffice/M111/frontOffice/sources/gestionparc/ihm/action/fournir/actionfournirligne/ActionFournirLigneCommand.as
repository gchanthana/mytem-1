package gestionparc.ihm.action.fournir.actionfournirligne
{
	import gestionparc.ihm.action.ICommand;
	
	public class ActionFournirLigneCommand implements ICommand
	{
		private var receiver : ActionFournirLigneReceiver = null;
		
		public function ActionFournirLigneCommand(rec:ActionFournirLigneReceiver)
		{
			this.receiver = rec;
		}
		
		public function execute():void
		{
			receiver.action();
		}
	}
}