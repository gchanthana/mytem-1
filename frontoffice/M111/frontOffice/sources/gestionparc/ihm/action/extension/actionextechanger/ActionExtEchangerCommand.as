package gestionparc.ihm.action.extension.actionextechanger
{
	import gestionparc.ihm.action.ICommand;

	public class ActionExtEchangerCommand implements ICommand
	{
		private var receiver:ActionExtEchangerReceiver=null;

		public function ActionExtEchangerCommand(rec:ActionExtEchangerReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
