package gestionparc.ihm.action
{
	/**
	 * Classe permettant d'invoquer la pattern command.
	 */
	public class Invoker
	{
		public static var ClassFiche:Object;
		public static var FICHE_SELECTED:String="";
		private var currentCommand:ICommand;

		/**
		 * Cette fonction renseigne la commande courrante.
		 */
		public function setCommand(curCommand:ICommand):void
		{
			this.currentCommand=curCommand;
		}

		/**
		 * Cette fonction lance la fonction execute de la commande courrante.
		 */
		public function executeCommand():void
		{
			if (currentCommand)
				currentCommand.execute();
		}
	}
}
