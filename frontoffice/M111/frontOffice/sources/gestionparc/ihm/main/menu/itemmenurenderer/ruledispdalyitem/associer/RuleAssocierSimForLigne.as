package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsTypeLigneMobile;

	public class RuleAssocierSimForLigne implements IRuleDisplayItem
	{
		public function RuleAssocierSimForLigne()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new IsTypeLigneMobile();
			var r2:IRuleDisplayItem=new HasSimAssociated();

			if (r1.isDisplay() && !r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}
