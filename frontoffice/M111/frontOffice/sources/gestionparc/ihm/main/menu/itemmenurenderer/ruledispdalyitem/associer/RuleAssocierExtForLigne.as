package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasExtAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsTypeLigneMobile;

	public class RuleAssocierExtForLigne implements IRuleDisplayItem
	{
		public function RuleAssocierExtForLigne()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem= new IsTypeLigneMobile();
			var r2:IRuleDisplayItem=new HasExtAssociated();

			if (!r1.isDisplay() && !r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}
