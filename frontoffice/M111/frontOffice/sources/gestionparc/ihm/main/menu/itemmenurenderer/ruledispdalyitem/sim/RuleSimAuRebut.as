package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.sim
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;

	public class RuleSimAuRebut implements IRuleDisplayItem
	{
		public function RuleSimAuRebut()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new IsLigneMobile();
			return r1.isDisplay();
		}
	}
}
