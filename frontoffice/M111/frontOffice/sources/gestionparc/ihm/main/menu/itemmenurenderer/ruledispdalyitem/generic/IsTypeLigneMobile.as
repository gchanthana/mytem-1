package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;

	public class IsTypeLigneMobile implements IRuleDisplayItem
	{
	
		public function IsTypeLigneMobile()
		{
		}
				
		public function isDisplay():Boolean
		{
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			return (ligne.IDTYPE_LIGNE==moduleGestionParcIHM.TYPE_LIGNE_MOBILE) ? true : false;
		}		
	}
}