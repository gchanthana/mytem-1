package gestionparc.ihm.main.menu
{
	import flash.events.EventDispatcher;
	
	import gestionparc.ihm.action.ICommand;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	
	import mx.controls.Image;

	/**
	 * Classe gérant les items du tous les menus contextuels.
	 * Elle définit un model d'item qu'on appliquera specifiquement au différents itemRenderer des entités du tableau principal.
	 */
	public class ItemMenuContextual extends EventDispatcher
	{
		public var command:ICommand;
		public var ruleDisplayItem:IRuleDisplayItem;
		public var enabled:Boolean;
		public var label:String;
		public var icon:Class;
		public var img:Image;

		/**
		 * Constructeur de la classe, permet de renseigner tous les variables.
		 */
		public function ItemMenuContextual(myLabel:String, myEnabled:Boolean, myIcon:Class, myCommand:ICommand, myRuleDisplayItem:IRuleDisplayItem)
		{
			this.label=myLabel;
			this.enabled=myEnabled; /** cliquable ou pas */
			this.icon=myIcon;
			this.command=myCommand;
			this.ruleDisplayItem=myRuleDisplayItem;
		}
	}
}