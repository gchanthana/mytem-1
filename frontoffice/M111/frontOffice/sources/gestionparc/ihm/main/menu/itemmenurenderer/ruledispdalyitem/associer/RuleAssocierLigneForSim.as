package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasLigneAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;

	public class RuleAssocierLigneForSim implements IRuleDisplayItem
	{
		public function RuleAssocierLigneForSim()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new IsLigneMobile();
			var r2:IRuleDisplayItem = new HasSimAssociated();
			var r3:IRuleDisplayItem = new HasLigneAssociated()
			if(r1.isDisplay()&&r2.isDisplay()&&!r3.isDisplay())
				return true;
			else
				return false;
		}
	}
}