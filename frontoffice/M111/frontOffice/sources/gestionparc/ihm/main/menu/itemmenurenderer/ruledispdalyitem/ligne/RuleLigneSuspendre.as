package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasCarrier;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneActive;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsTypeLigneMobile;
	
	public class RuleLigneSuspendre implements IRuleDisplayItem
	{
		public function RuleLigneSuspendre()
		{
		}
		
		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new IsTypeLigneMobile();
			var r2:IRuleDisplayItem = new IsLigneActive();
			var r3:IRuleDisplayItem = new HasCarrier();
			
			return r1.isDisplay() && r2.isDisplay() && r3.isDisplay();
		}
	}
}