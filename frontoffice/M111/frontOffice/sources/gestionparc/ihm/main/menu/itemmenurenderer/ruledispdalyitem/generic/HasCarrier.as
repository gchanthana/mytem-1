package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	
	public class HasCarrier implements IRuleDisplayItem
	{
		public function HasCarrier()
		{
		}
		
		public function isDisplay():Boolean
		{
			var ligne:AbstractMatriceParcVO = SpecificationVO.getInstance().elementDataGridSelected;
			
			if(ligne.OPERATEURID > 0)
				return true
			else
				return false
		}
	}
}