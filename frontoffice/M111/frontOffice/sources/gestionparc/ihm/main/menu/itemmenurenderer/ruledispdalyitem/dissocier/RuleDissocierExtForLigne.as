package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasExtAssociated;

	public class RuleDissocierExtForLigne implements IRuleDisplayItem
	{
		public function RuleDissocierExtForLigne()
		{
		}
		public function isDisplay():Boolean
		{
			var r2:IRuleDisplayItem = new HasExtAssociated();
			
			if(r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}