package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.mdm
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasEqpAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.MDM_BOOL_ISMANAGED;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.MDM_ENABLE;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.MDM_USER_RIGHT;

	public class RuleWypeComplet implements IRuleDisplayItem
	{
		public function RuleWypeComplet()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new MDM_ENABLE();
			var r2:IRuleDisplayItem=new MDM_USER_RIGHT();
			var r3:IRuleDisplayItem=new MDM_BOOL_ISMANAGED();
			var r4:IRuleDisplayItem=new HasEqpAssociated();

			if (r1.isDisplay() && r2.isDisplay() && r3.isDisplay() && r4.isDisplay())
				return true;
			else
				return false;
		}
	}
}
