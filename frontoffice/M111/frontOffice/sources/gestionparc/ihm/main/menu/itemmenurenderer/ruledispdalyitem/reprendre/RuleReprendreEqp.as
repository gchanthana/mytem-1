package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.reprendre
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasEqpAssociated;

	public class RuleReprendreEqp implements IRuleDisplayItem
	{
		public function RuleReprendreEqp()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new HasEqpAssociated();
			return r1.isDisplay()
		}
	}
}
