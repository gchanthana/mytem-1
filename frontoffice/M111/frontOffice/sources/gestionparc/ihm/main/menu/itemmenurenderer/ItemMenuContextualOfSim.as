package gestionparc.ihm.main.menu.itemmenurenderer
{
	import gestionparc.ihm.action.associer.actionAssocierCollabToSim.ActionAssocierCollabToSimCommand;
	import gestionparc.ihm.action.associer.actionAssocierCollabToSim.ActionAssocierCollabToSimReceiver;
	import gestionparc.ihm.action.associer.actionAssocierEqpToSim.ActionAssocierEqpToSimCommand;
	import gestionparc.ihm.action.associer.actionAssocierEqpToSim.ActionAssocierEqpToSimReceiver;
	import gestionparc.ihm.action.associer.actionAssocierLigneSimToSim.ActionAssocierLigneSimToSimCommand;
	import gestionparc.ihm.action.associer.actionAssocierLigneSimToSim.ActionAssocierLigneSimToSimReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierCollabfromSim.ActionDissocierCollabFromSimCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierCollabfromSim.ActionDissocierCollabFromSimReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierEqpFromSim.ActionDissocierEqpFromSimCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierEqpFromSim.ActionDissocierEqpFromSimReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierLigneSimFromSim.ActionDissocierLigneSimFromSimCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierLigneSimFromSim.ActionDissocierLigneSimFromSimReceiver;
	import gestionparc.ihm.action.sim.actionsimaurebut.ActionSimAuRebutCommand;
	import gestionparc.ihm.action.sim.actionsimaurebut.ActionSimAuRebutReceiver;
	import gestionparc.ihm.action.sim.actionsimechanger.ActionSimEchangerCommand;
	import gestionparc.ihm.action.sim.actionsimechanger.ActionSimEchangerReceiver;
	import gestionparc.ihm.action.sim.actionsimvoirfiche.ActionSimVoirFicheCommand;
	import gestionparc.ihm.action.sim.actionsimvoirfiche.ActionSimVoirFicheReceiver;
	import gestionparc.ihm.action.sim.actionsimvoirhistorique.ActionsimVoirHistoriqueCommand;
	import gestionparc.ihm.action.sim.actionsimvoirhistorique.ActionsimVoirHistoriqueReceiver;
	import gestionparc.ihm.main.menu.ItemMenuContextual;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierCollabForSim;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierEqpForSim;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierLigneForSim;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierCollabForSim;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierEqpForLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierLigneForSim;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuSim;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuSimAssocier;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuSimDissocier;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.sim.RuleSimAuRebut;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.sim.RuleSimEchanger;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.sim.RuleSimVoirFiche;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.sim.RuleSimVoirHistorique;
	
	import mx.resources.ResourceManager;

	public class ItemMenuContextualOfSim extends AbstractItemMenuContextual
	{

		[Embed(source='/assets/images/coll+.png')]
		private var myIconGiveColl:Class;

		[Embed(source='/assets/images/coll-.png')]
		private var myIconGetColl:Class;

		[Embed(source='/assets/images/give_terminal.png')]
		private var myIconGiveTerm:Class;

		[Embed(source='/assets/images/get_terminal.png')]
		private var myIconGetTerm:Class;

		[Embed(source='/assets/images/Newfixe+.png')]
		private var myIconNewFixeP:Class;

		[Embed(source='/assets/images/Newfixe-.png')]
		private var myIconNewFixeM:Class;

		[Embed(source='/assets/images/echange.png')]
		private var myIconActionEchanger:Class;

		[Embed(source='/assets/images/terminal_sav.png')]
		private var myIconActionSAV:Class;

		[Embed(source='/assets/images/action_supprimer.png')]
		private var myIconActionAuRebut:Class;

		[Embed(source='/assets/images/voir.png')]
		private var myIconVoir:Class;

		[Embed(source='/assets/images/historique.png')]
		private var myIconHistorique:Class;

		/**
		 * Cette fonction créer la liste exaustive des items du menu contextuel de la SIM.
		 */
		override protected function fillArrayItemMenuContextual():Array
		{
			var res:Array=new Array();

			// MENU - Associer
			var rule1:RuleMenuSimAssocier=new RuleMenuSimAssocier();
			var obj1:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'ASSOCIER'), false, null, null, rule1);
			res.push(obj1);

			// Associer à un collaborateur
			var recAssocCollab:ActionAssocierCollabToSimReceiver=new ActionAssocierCollabToSimReceiver();
			var cmdAssocCollab:ActionAssocierCollabToSimCommand=new ActionAssocierCollabToSimCommand(recAssocCollab);
			var rule2:RuleAssocierCollabForSim=new RuleAssocierCollabForSim();
			var obj2:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'a_un_collaborateur'), true, myIconGiveColl, cmdAssocCollab, rule2);
			res.push(obj2);

			// Associer avec un équipement
			var recAssoceqp:ActionAssocierEqpToSimReceiver=new ActionAssocierEqpToSimReceiver();
			var cmdAssoceqp:ActionAssocierEqpToSimCommand=new ActionAssocierEqpToSimCommand(recAssoceqp);
			var rule3:RuleAssocierEqpForSim=new RuleAssocierEqpForSim();
			var obj3:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'avec_un__quipement'), true, myIconGiveTerm, cmdAssoceqp, rule3);
			res.push(obj3);

			// Associer à une ligne
			var recAssocligSim:ActionAssocierLigneSimToSimReceiver=new ActionAssocierLigneSimToSimReceiver();
			var cmdAssocligSim:ActionAssocierLigneSimToSimCommand=new ActionAssocierLigneSimToSimCommand(recAssocligSim);
			var rule13:RuleAssocierLigneForSim=new RuleAssocierLigneForSim();
			var obj13:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', '__une_ligne'), true, myIconGiveTerm, cmdAssocligSim, rule13);
			res.push(obj13);

			// MENU - dissocier
			var rule4:RuleMenuSimDissocier=new RuleMenuSimDissocier();
			var obj4:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'DISSOCIER'), false, null, null, rule4);
			res.push(obj4);

			// dissocier du collaborateur
			var recdisoccollabSim:ActionDissocierCollabFromSimReceiver=new ActionDissocierCollabFromSimReceiver();
			var cmddisocCollabSim:ActionDissocierCollabFromSimCommand=new ActionDissocierCollabFromSimCommand(recdisoccollabSim);
			var rule5:RuleDissocierCollabForSim=new RuleDissocierCollabForSim();
			var obj5:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'du_collaborateur'), true, myIconGetColl, cmddisocCollabSim, rule5);
			res.push(obj5);

			// Dissocier de l'équipement
			var recdisocEqpSim:ActionDissocierEqpFromSimReceiver=new ActionDissocierEqpFromSimReceiver();
			var cmddisocEqpSim:ActionDissocierEqpFromSimCommand=new ActionDissocierEqpFromSimCommand(recdisocEqpSim);
			var rule6:RuleDissocierEqpForLigne=new RuleDissocierEqpForLigne();
			var obj6:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'de_l__quipement'), true, myIconGetTerm, cmddisocEqpSim, rule6);
			res.push(obj6);

			// dissocier de la ligne
			var recdisocLigSim:ActionDissocierLigneSimFromSimReceiver=new ActionDissocierLigneSimFromSimReceiver();
			var cmddisocLigSim:ActionDissocierLigneSimFromSimCommand=new ActionDissocierLigneSimFromSimCommand(recdisocLigSim);
			var rule7:RuleDissocierLigneForSim=new RuleDissocierLigneForSim();
			var obj7:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'de_la_ligne'), true, myIconGetTerm, cmddisocLigSim, rule7);
			res.push(obj7);

			// MENU - Sim
			var rule8:RuleMenuSim=new RuleMenuSim();
			var obj8:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', "SIM"), false, null, null, rule8);
			res.push(obj8);

			// Echanger
			var recEchanger:ActionSimEchangerReceiver=new ActionSimEchangerReceiver();
			var cmdEchanger:ActionSimEchangerCommand=new ActionSimEchangerCommand(recEchanger);
			var rule9:RuleSimEchanger=new RuleSimEchanger();
			var obj9:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Echange'), true, myIconActionEchanger, cmdEchanger, rule9);
			res.push(obj9);

			// Mettre au rebut
			var recRebut:ActionSimAuRebutReceiver=new ActionSimAuRebutReceiver();
			var cmdRebut:ActionSimAuRebutCommand=new ActionSimAuRebutCommand(recRebut);
			var rule10:RuleSimAuRebut=new RuleSimAuRebut();
			var obj10:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Mettre_au_r_but'), true, myIconActionAuRebut, cmdRebut, rule10);
			res.push(obj10);

			// Voir fiche
			var recVoirFiche:ActionSimVoirFicheReceiver=new ActionSimVoirFicheReceiver();
			var cmdVoirFiche:ActionSimVoirFicheCommand=new ActionSimVoirFicheCommand(recVoirFiche);
			var rule11:RuleSimVoirFiche=new RuleSimVoirFiche();
			var obj11:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Voir_fiche'), true, myIconVoir, cmdVoirFiche, rule11);
			res.push(obj11);

			// voir historique
			var recVoirHisto:ActionsimVoirHistoriqueReceiver=new ActionsimVoirHistoriqueReceiver();
			var cmdVoirHisto:ActionsimVoirHistoriqueCommand=new ActionsimVoirHistoriqueCommand(recVoirHisto);
			var rule12:RuleSimVoirHistorique=new RuleSimVoirHistorique();
			var obj12:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Voir_historique'), true, myIconHistorique, cmdVoirHisto, rule12);
			res.push(obj12);

			return res;
		}
	}
}