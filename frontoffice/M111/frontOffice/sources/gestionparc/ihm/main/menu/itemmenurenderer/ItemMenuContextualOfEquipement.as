package gestionparc.ihm.main.menu.itemmenurenderer
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.action.associer.actionAssocierCollabToTerm.ActionAssocierCollabToTermCommand;
	import gestionparc.ihm.action.associer.actionAssocierCollabToTerm.ActionAssocierCollabToTermReceiver;
	import gestionparc.ihm.action.associer.actionAssocierLigneExtToEqp.ActionAssocierLigneExtToEqpCommand;
	import gestionparc.ihm.action.associer.actionAssocierLigneExtToEqp.ActionAssocierLigneExtToEqpReceiver;
	import gestionparc.ihm.action.associer.actionAssocierLigneSimToEqp.ActionAssocierLigneSimToEqpReceiver;
	import gestionparc.ihm.action.associer.actionAssocierLigneSimToEqp.ActionAssocierLigneSimtoEqpCommand;
	import gestionparc.ihm.action.associer.actionAssocierLigneToEqp.ActionAssocierLigneToEqpCommand;
	import gestionparc.ihm.action.associer.actionAssocierLigneToEqp.ActionAssocierLigneToEqpReceiver;
	import gestionparc.ihm.action.associer.actionAssocierSousEqpToEqp.ActionAssocierSousEqpToEqpCommand;
	import gestionparc.ihm.action.associer.actionAssocierSousEqpToEqp.ActionAssocierSousEqpToEqpReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierCollabFromEqp.ActionDissocierCollabFromEqpCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierCollabFromEqp.ActionDissocierCollabFromEqpReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierLigneExtFromEqp.ActionDissocierLigneExtFromEqpCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierLigneExtFromEqp.ActionDissocierLigneExtFromEqpReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierLigneFromEqp.ActionDissocierLigneFromEqpCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierLigneFromEqp.ActionDissocierLigneFromEqpReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierLigneSimFromEqp.ActionDissocierLigneSimFromEqpCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierLigneSimFromEqp.ActionDissocierLigneSimFromEqpReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierSousEqp.ActionDissocierSousEqpCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierSousEqp.ActionDissocierSousEqpReceiver;
	import gestionparc.ihm.action.equipement.actionEqpAuRebut.ActionEqpAuRebutCommand;
	import gestionparc.ihm.action.equipement.actionEqpAuRebut.ActionEqpAuRebutReceiver;
	import gestionparc.ihm.action.equipement.actionEqpEchanger.ActionEqpEchangerCommand;
	import gestionparc.ihm.action.equipement.actionEqpEchanger.ActionEqpEchangerReceiver;
	import gestionparc.ihm.action.equipement.actionEqpSav.ActionEqpSavCommand;
	import gestionparc.ihm.action.equipement.actionEqpSav.ActionEqpSavReceiver;
	import gestionparc.ihm.action.equipement.actionEqpVoirFiche.ActionEqpVoirFicheCommand;
	import gestionparc.ihm.action.equipement.actionEqpVoirFiche.ActionEqpVoirFicheReceiver;
	import gestionparc.ihm.action.equipement.actionEqpVoirHistorique.ActionEqpVoirHistoriqueCommand;
	import gestionparc.ihm.action.equipement.actionEqpVoirHistorique.ActionEqpVoirHistoriqueReceiver;
	import gestionparc.ihm.action.mdm.actionDeverouillerTerminal.ActionDeverrouillerTerminalCommand;
	import gestionparc.ihm.action.mdm.actionDeverouillerTerminal.ActionDeverrouillerTerminalReceiver;
	import gestionparc.ihm.action.mdm.actionGeolocalisation.ActionGeolocalisationCommand;
	import gestionparc.ihm.action.mdm.actionGeolocalisation.ActionGeolocalisationReceiver;
	import gestionparc.ihm.action.mdm.actionGererTerminal.ActionGererTerminalCommand;
	import gestionparc.ihm.action.mdm.actionGererTerminal.ActionGererTerminalReceiver;
	import gestionparc.ihm.action.mdm.actionRevocation.ActionRevocationCommand;
	import gestionparc.ihm.action.mdm.actionRevocation.ActionRevocationReceiver;
	import gestionparc.ihm.action.mdm.actionVerrouillerTerminal.ActionVerrouillerTerminalCommand;
	import gestionparc.ihm.action.mdm.actionVerrouillerTerminal.ActionVerrouillerTerminalReceiver;
	import gestionparc.ihm.action.mdm.actionWypeComplet.ActionWypeCompletCommand;
	import gestionparc.ihm.action.mdm.actionWypeComplet.ActionWypeCompletReceiver;
	import gestionparc.ihm.action.mdm.actionWypeTerm.ActionWypeTermCommand;
	import gestionparc.ihm.action.mdm.actionWypeTerm.ActionWypeTermReceiver;
	import gestionparc.ihm.main.menu.ItemMenuContextual;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierCollabForEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierExtForEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierLigneForEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierLigneSimForEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierSousEqpForEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierCollabForEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierLigneExtForEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierLigneForEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierLigneSimForEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierSousEqpForEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.equipement.RuleEqpAuRebut;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.equipement.RuleEqpEchanger;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.equipement.RuleEqpSav;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.equipement.RuleEqpVoirFiche;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.equipement.RuleEqpVoirHistorique;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.mdm.RuleDeverrouillerTerminal;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.mdm.RuleGeolocalisation;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.mdm.RuleGererTerminal;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.mdm.RuleMenuAction;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.mdm.RuleRevocation;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.mdm.RuleVerrouillerTerminal;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.mdm.RuleWypeComplet;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.mdm.RuleWypeTerm;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuEqpAssocier;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuEqpDissocier;
	
	import mx.resources.ResourceManager;

	public class ItemMenuContextualOfEquipement extends AbstractItemMenuContextual
	{

		[Embed(source='/assets/images/coll+.png')]
		private var myIconGiveColl:Class;

		[Embed(source='/assets/images/coll-.png')]
		private var myIconGetColl:Class;

		[Embed(source='/assets/images/give_terminal.png')]
		private var myIconGiveTerm:Class;

		[Embed(source='/assets/images/get_terminal.png')]
		private var myIconGetTerm:Class;

		[Embed(source='/assets/images/Newfixe+.png')]
		private var myIconNewFixeP:Class;

		[Embed(source='/assets/images/Newfixe-.png')]
		private var myIconNewFixeM:Class;

		[Embed(source='/assets/images/echange.png')]
		private var myIconActionEchanger:Class;

		[Embed(source='/assets/images/terminal_sav.png')]
		private var myIconActionSAV:Class;

		[Embed(source='/assets/images/action_supprimer.png')]
		private var myIconActionAuRebut:Class;

		[Embed(source='/assets/images/voir.png')]
		private var myIconVoir:Class;

		[Embed(source='/assets/images/historique.png')]
		private var myIconHistorique:Class;

		[Embed(source='/assets/images/lock_tel.png')]
		private var myIconVerrouillerTel:Class;
		
		[Embed(source='/assets/images/map.png')]
		private var myIconMap:Class;
		
		[Embed(source='/assets/images/data_delete.png')]
		private var myIconDateDelete:Class;
		
		[Embed(source='/assets/images/gerer_terminal.png')]
		private var myIconGerer:Class;
		
		[Embed(source='/assets/images/neplusgerer_terminal.png')]
		private var myIconPlusGerer:Class;
		
		/**
		 * Cette fonction créer la liste exaustive des items du menu contextuel d'un equipement.
		 */
		override protected function fillArrayItemMenuContextual():Array
		{
			var res:Array=new Array();

			// Associer
			var rule1:RuleMenuEqpAssocier=new RuleMenuEqpAssocier();
			var obj1:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'ASSOCIER'), false, null, null, rule1);
			res.push(obj1);

			// Associer à un collaborateur
			var recAssocCollab:ActionAssocierCollabToTermReceiver=new ActionAssocierCollabToTermReceiver();
			var cmdAssocCollab:ActionAssocierCollabToTermCommand=new ActionAssocierCollabToTermCommand(recAssocCollab);
			var rule2:RuleAssocierCollabForEqp=new RuleAssocierCollabForEqp();
			var obj2:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'un_collaborateur'), true, myIconGiveColl, cmdAssocCollab, rule2);
			res.push(obj2);

			// Associer à une ligne/sim
			var recAssocLigneSim:ActionAssocierLigneSimToEqpReceiver=new ActionAssocierLigneSimToEqpReceiver();
			var cmdAssocLigneSim:ActionAssocierLigneSimtoEqpCommand=new ActionAssocierLigneSimtoEqpCommand(recAssocLigneSim);
			var rule3:RuleAssocierLigneSimForEqp=new RuleAssocierLigneSimForEqp();
			var obj3:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne_SIM'), true, myIconNewFixeP, cmdAssocLigneSim, rule3);
			res.push(obj3);

			// Associer à une ligne
			var recAssocLigne:ActionAssocierLigneToEqpReceiver=new ActionAssocierLigneToEqpReceiver();
			var cmdAssocLigne:ActionAssocierLigneToEqpCommand=new ActionAssocierLigneToEqpCommand(recAssocLigne);
			var rule31:RuleAssocierLigneForEqp=new RuleAssocierLigneForEqp();
			var obj31:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne'), true, myIconNewFixeP, cmdAssocLigne, rule31);
			res.push(obj31);

			// Associer à un sous equipement
			var receiverAssocierSousEquipement:ActionAssocierSousEqpToEqpReceiver=new ActionAssocierSousEqpToEqpReceiver();
			var actionAssocierEqptSousEqpt:ActionAssocierSousEqpToEqpCommand=new ActionAssocierSousEqpToEqpCommand(receiverAssocierSousEquipement);
			var rule4:RuleAssocierSousEqpForEqp=new RuleAssocierSousEqpForEqp();
			var obj4:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'un_sous__quipement'), true, myIconGiveTerm, actionAssocierEqptSousEqpt, rule4);
			res.push(obj4);

			/** associer à une extension */
			var recAssocExt:ActionAssocierLigneExtToEqpReceiver=new ActionAssocierLigneExtToEqpReceiver();
			var cmdAssocExt:ActionAssocierLigneExtToEqpCommand=new ActionAssocierLigneExtToEqpCommand(recAssocExt);
			var rule32:RuleAssocierExtForEqp=new RuleAssocierExtForEqp();
			var obj32:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne_Extension'), true, myIconNewFixeP, cmdAssocExt, rule32);
			res.push(obj32);
			
			
			// Dissocier
			var rule5:RuleMenuEqpDissocier=new RuleMenuEqpDissocier();
			var obj5:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'DISSOCIER'), false, null, null, rule5);
			res.push(obj5);

			// Dissocier du collaborateur
			var recDissocCollabFromEqp:ActionDissocierCollabFromEqpReceiver=new ActionDissocierCollabFromEqpReceiver();
			var cmdDissocCollabFromEqp:ActionDissocierCollabFromEqpCommand=new ActionDissocierCollabFromEqpCommand(recDissocCollabFromEqp);
			var rule6:RuleDissocierCollabForEqp=new RuleDissocierCollabForEqp();
			var obj6:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'du_collaborateur'), true, myIconGetColl, cmdDissocCollabFromEqp, rule6);
			res.push(obj6);

			// Dissocier de la ligne/sim
			var recDissocLigneSim:ActionDissocierLigneSimFromEqpReceiver=new ActionDissocierLigneSimFromEqpReceiver();
			var cmdDissocLigneSim:ActionDissocierLigneSimFromEqpCommand=new ActionDissocierLigneSimFromEqpCommand(recDissocLigneSim);
			var rule7:RuleDissocierLigneSimForEqp=new RuleDissocierLigneSimForEqp();
			var obj7:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne_SIM'), true, myIconNewFixeM, cmdDissocLigneSim, rule7);
			res.push(obj7);

			// Dissocier de la ligne
			var recDissocLigne:ActionDissocierLigneFromEqpReceiver=new ActionDissocierLigneFromEqpReceiver();
			var cmdDissocLigne:ActionDissocierLigneFromEqpCommand=new ActionDissocierLigneFromEqpCommand(recDissocLigne);
			var rule71:RuleDissocierLigneForEqp=new RuleDissocierLigneForEqp();
			var obj71:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne'), true, myIconNewFixeM, cmdDissocLigne, rule71);
			res.push(obj71);

			/** Dissocier de l'extension */
			var recDissocLigneExt:ActionDissocierLigneExtFromEqpReceiver=new ActionDissocierLigneExtFromEqpReceiver();
			var cmdDissocLigneExt:ActionDissocierLigneExtFromEqpCommand=new ActionDissocierLigneExtFromEqpCommand(recDissocLigneExt);
			var rule72:RuleDissocierLigneExtForEqp=new RuleDissocierLigneExtForEqp();
			var obj72:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne_Extension'), true, myIconNewFixeM, cmdDissocLigneExt, rule72);
			res.push(obj72);
			
			// Dissocier d'un sous équipement
			var recSousEquip:ActionDissocierSousEqpReceiver=new ActionDissocierSousEqpReceiver();
			var cmdSousEquip:ActionDissocierSousEqpCommand=new ActionDissocierSousEqpCommand(recSousEquip);
			var rule8:RuleDissocierSousEqpForEqp=new RuleDissocierSousEqpForEqp();
			var obj8:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'un_sous__quipement'), true, myIconGetTerm, cmdSousEquip, rule8);
			res.push(obj8);

			// Equipement
			var rule9:RuleMenuEqp=new RuleMenuEqp();
			var obj9:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'EQUIPEMENT'), false, null, null, rule9);
			res.push(obj9);

			// Echange
			var recEchange:ActionEqpEchangerReceiver=new ActionEqpEchangerReceiver();
			var cmdEchange:ActionEqpEchangerCommand=new ActionEqpEchangerCommand(recEchange);
			var rule10:RuleEqpEchanger=new RuleEqpEchanger();
			var obj10:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Echange'), true, myIconActionEchanger, cmdEchange, rule10);
			res.push(obj10);

			// SAV
			var recSAV:ActionEqpSavReceiver=new ActionEqpSavReceiver();
			var cmdSAV:ActionEqpSavCommand=new ActionEqpSavCommand(recSAV);
			var rule11:RuleEqpSav=new RuleEqpSav();
			if(moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("MASTER_PARC_GFC") && moduleGestionParcIHM.globalParameter.userAccess.MASTER_PARC_GFC == 1 
				&& SpecificationVO.getInstance().elementDataGridSelected.T_ID_STATUT==4)
				var obj11:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Fermer_l_incident'), true, myIconActionSAV, cmdSAV, rule11);
			else
				var obj11:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'SAV'), true, myIconActionSAV, cmdSAV, rule11);
			
			res.push(obj11);

			// Mettre au rebut
			var recRebut:ActionEqpAuRebutReceiver=new ActionEqpAuRebutReceiver();
			var cmdRebut:ActionEqpAuRebutCommand=new ActionEqpAuRebutCommand(recRebut);
			var rule12:RuleEqpAuRebut=new RuleEqpAuRebut();
			var obj12:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Mettre_au_r_but'), true, myIconActionAuRebut, cmdRebut, rule12);
			res.push(obj12);

			// Voir fiche
			var recVoirFiche:ActionEqpVoirFicheReceiver=new ActionEqpVoirFicheReceiver();
			var cmdVoirFichr:ActionEqpVoirFicheCommand=new ActionEqpVoirFicheCommand(recVoirFiche);
			var rule13:RuleEqpVoirFiche=new RuleEqpVoirFiche();
			var obj13:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Voir_fiche'), true, myIconVoir, cmdVoirFichr, rule13);
			res.push(obj13);

			// Voir historique
			var recVoirHisto:ActionEqpVoirHistoriqueReceiver=new ActionEqpVoirHistoriqueReceiver();
			var cmdVoirHisto:ActionEqpVoirHistoriqueCommand=new ActionEqpVoirHistoriqueCommand(recVoirHisto);
			var rule14:RuleEqpVoirHistorique=new RuleEqpVoirHistorique();
			var obj14:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Voir_historique'), true, myIconHistorique, cmdVoirHisto, rule14);
			res.push(obj14);

			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;
			
			if ((ligne != null) && (!isNaN(ligne.T_IDTYPE_EQUIP )) && ((ligne.T_IDTYPE_EQUIP  == 70) || (ligne.T_IDTYPE_EQUIP  == 2192)))
			{ 
				// Ces menus ne sont disponibles que pour les terminaux
				var ruleAction:RuleMenuAction=new RuleMenuAction();
				var menuActionLabel:String=ResourceManager.getInstance().getString('M111', 'ACTIONS');
				
				if (SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
				{
					menuActionLabel=ResourceManager.getInstance().getString('M111', 'mdm_menu_is_last_op_fault');
				}

				var objAction:ItemMenuContextual=new ItemMenuContextual(menuActionLabel, false, null, null, ruleAction);
				res.push(objAction);

				if (!SpecificationVO.getInstance().elementDataGridSelected.IS_LAST_OP_FAULT)
				{
					// Verrouiller Term
					var rec16:ActionVerrouillerTerminalReceiver=new ActionVerrouillerTerminalReceiver();
					var cmd16:ActionVerrouillerTerminalCommand=new ActionVerrouillerTerminalCommand(rec16);
					var rule16:RuleVerrouillerTerminal=new RuleVerrouillerTerminal();
					var obj16:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Verrouiller'), true, myIconVerrouillerTel, cmd16, rule16);
					res.push(obj16);

					/** Déverrouiller Term : Pour tous chez MobileIron et uniquement pour iOS chez Zenprise
						MDM_PROVIDER Ajouté  dynamiquement dans la ligne seléctionnée du tableau dans la class AbstractItemMenuContextual**/
					
					if ((String(SpecificationVO.getInstance().elementDataGridSelected.MDM_PROVIDER).toLowerCase() == "mobileiron") || (!isDroid))
					{
						var rec161:ActionDeverrouillerTerminalReceiver=new ActionDeverrouillerTerminalReceiver();
						var cmd161:ActionDeverrouillerTerminalCommand=new ActionDeverrouillerTerminalCommand(rec161);
						var rule161:RuleDeverrouillerTerminal=new RuleDeverrouillerTerminal();
						var obj161:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Changer_le_code_de_verrouillage'), true, myIconVerrouillerTel, cmd161, rule161);
						res.push(obj161);
					}

					/** Uniquement chez Zenprise
						MDM_PROVIDER Ajouté  dynamiquement dans la ligne seléctionnée du tableau dans la class AbstractItemMenuContextual */
					
					if (String(SpecificationVO.getInstance().elementDataGridSelected.MDM_PROVIDER).toLowerCase() == "zenprise")
					{
						// WypeTerminal
						var rec17:ActionWypeTermReceiver=new ActionWypeTermReceiver();
						var cmd17:ActionWypeTermCommand=new ActionWypeTermCommand(rec17);
						var rule17:RuleWypeTerm=new RuleWypeTerm();
						var obj17:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Effacement_des_donn_es_de_l_entreprise'), true, myIconDateDelete, cmd17, rule17);
						res.push(obj17);
					}

					// WypeTerminal
					var rec18:ActionWypeCompletReceiver=new ActionWypeCompletReceiver();
					var cmd18:ActionWypeCompletCommand=new ActionWypeCompletCommand(rec18);
					var rule18:RuleWypeComplet=new RuleWypeComplet();
					var obj18:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Effacement_complet'), true, myIconDateDelete, cmd18, rule18);
					res.push(obj18);

					if (isDroid)
					{
						var rec181:ActionGeolocalisationReceiver=new ActionGeolocalisationReceiver();
						var cmd181:ActionGeolocalisationCommand=new ActionGeolocalisationCommand(rec181);
						var rule181:RuleGeolocalisation=new RuleGeolocalisation();
						var obj181:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'G_olocalisation'), true, myIconMap, cmd181, rule181);
						res.push(obj181);
					}

					//Révocation
					var rec182:ActionRevocationReceiver=new ActionRevocationReceiver();
					var cmd182:ActionRevocationCommand=new ActionRevocationCommand(rec182);
					var rule182:RuleRevocation=new RuleRevocation();
					var obj182:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Ne_plus_g_rer_l__quipement'), true, myIconPlusGerer, cmd182, rule182);
					res.push(obj182);

					// Gerer le terminal
					var rec19:ActionGererTerminalReceiver=new ActionGererTerminalReceiver();
					var cmd19:ActionGererTerminalCommand=new ActionGererTerminalCommand(rec19);
					var rule19:RuleGererTerminal=new RuleGererTerminal();
					var obj19:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'G_rer_le_terminal'), true, myIconGerer, cmd19, rule19);
					res.push(obj19);
				}
			}
			return res;
		}
	}
}
