package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.collab
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;

	public class RuleCollabDepartEntreprise implements IRuleDisplayItem
	{
		public function RuleCollabDepartEntreprise()
		{
		}

		public function isDisplay():Boolean
		{
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;
			
			/*if (ligne.INOUT == 0)
				return false;
			else*/
				return true;
		}
	}
}
