package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasCollaborateurAssociated;

	public class RuleDissocierCollabForEqp implements IRuleDisplayItem
	{
		public function RuleDissocierCollabForEqp()
		{
		}
		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new HasCollaborateurAssociated();
			return r1.isDisplay();
		}
	}
}