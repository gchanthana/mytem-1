package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.fournir
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;

	public class RuleFournirLigneSim implements IRuleDisplayItem
	{
		public function RuleFournirLigneSim()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new IsLigneMobile();
			var r2:IRuleDisplayItem=new IsLigneFixe();
			if (r1.isDisplay())
				return true;
			else if (!r1.isDisplay() && !r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}
