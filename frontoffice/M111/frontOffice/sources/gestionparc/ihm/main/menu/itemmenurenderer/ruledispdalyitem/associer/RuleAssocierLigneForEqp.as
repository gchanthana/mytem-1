package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasLigneAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;

	public class RuleAssocierLigneForEqp implements IRuleDisplayItem
	{
		public function RuleAssocierLigneForEqp()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new HasLigneAssociated();
			var r2:IRuleDisplayItem=new IsLigneFixe();
			
			if (!r1.isDisplay() && r2.isDisplay())
				return true
			else
				return false
		}
	}
}
