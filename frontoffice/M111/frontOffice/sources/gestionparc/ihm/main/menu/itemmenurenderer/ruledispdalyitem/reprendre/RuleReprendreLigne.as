package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.reprendre
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasLigneAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;

	public class RuleReprendreLigne implements IRuleDisplayItem
	{
		public function RuleReprendreLigne()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new IsLigneFixe();
			var r2:IRuleDisplayItem=new HasLigneAssociated();
			if (r1.isDisplay() && r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}
