package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasLigneAssociated;

	public class RuleAssocierLigneForExt implements IRuleDisplayItem
	{
		public function RuleAssocierLigneForExt()
		{
		}

		public function isDisplay():Boolean
		{
			var r2:IRuleDisplayItem=new HasLigneAssociated();
			
			if ( !r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}
