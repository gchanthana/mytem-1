package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;

	public class RuleDissocierLigneSimForEqp implements IRuleDisplayItem
	{
		public function RuleDissocierLigneSimForEqp()
		{
		}
		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new HasSimAssociated();
			return r1.isDisplay();
		}
	}
}