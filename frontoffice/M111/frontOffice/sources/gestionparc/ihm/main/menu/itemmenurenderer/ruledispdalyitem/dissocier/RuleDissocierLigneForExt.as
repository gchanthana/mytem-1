package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasExtAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasLigneAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;

	public class RuleDissocierLigneForExt implements IRuleDisplayItem
	{
		public function RuleDissocierLigneForExt()
		{
		}

		public function isDisplay():Boolean
		{
			var r3:IRuleDisplayItem=new HasLigneAssociated();
			
			if (r3.isDisplay())
				return true;
			else
				return false;
		}
	}
}
