package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;

	public class RuleAssocierLigneSimForEqp implements IRuleDisplayItem
	{
		public function RuleAssocierLigneSimForEqp()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new HasSimAssociated();
			var r2:IRuleDisplayItem=new IsLigneMobile();

			if (!r1.isDisplay() && r2.isDisplay())
				return true
			else
				return false
		}
	}
}
