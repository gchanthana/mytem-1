package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasCollaborateurAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasExtAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;

	public class RuleAssocierCollabForExt implements IRuleDisplayItem
	{
		public function RuleAssocierCollabForExt()
		{
		}
		
		public function isDisplay():Boolean
		{	
			var r2:IRuleDisplayItem = new HasCollaborateurAssociated();
			
			if(!r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}