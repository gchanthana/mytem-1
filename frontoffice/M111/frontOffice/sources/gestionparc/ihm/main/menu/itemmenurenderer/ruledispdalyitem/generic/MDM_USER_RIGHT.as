package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;

	public class MDM_USER_RIGHT implements IRuleDisplayItem
	{
		public function MDM_USER_RIGHT()
		{
		}

		public function isDisplay():Boolean
		{
			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("MDM_N1") && moduleGestionParcIHM.globalParameter.userAccess.MDM_N1 == 1)
			{
				return true;
			}
			else
				return false;
		}
	}
}
