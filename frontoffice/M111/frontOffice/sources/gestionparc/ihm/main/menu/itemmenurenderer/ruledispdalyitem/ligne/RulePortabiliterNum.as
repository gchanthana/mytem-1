package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsTypeLigneMobile;
	
	public class RulePortabiliterNum implements IRuleDisplayItem
	{
		public function RulePortabiliterNum()
		{
		}
		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new IsTypeLigneMobile();
			var r2:IRuleDisplayItem = new HasSimAssociated();
			
			if(r1.isDisplay() && r2.isDisplay() )	
				return true;
			else
				return false;
		}
		
	}
}