package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;

	public class RuleLigneCommanderOption implements IRuleDisplayItem
	{
		public function RuleLigneCommanderOption()
		{
		}

		public function isDisplay():Boolean
		{
			return (SpecificationVO.getInstance().hasCommandeFixe || SpecificationVO.getInstance().hasCommandeMobile);
		}
	}
}
