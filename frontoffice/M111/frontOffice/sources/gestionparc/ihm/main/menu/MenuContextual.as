package gestionparc.ihm.main.menu
{
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.action.Invoker;
	
	import mx.controls.Menu;
	import mx.core.Application;
	import mx.events.MenuEvent;

	/**
	 * Classe gérant le menu contextuel du tableau principal.
	 */
	public class MenuContextual extends Menu
	{
		
		/**
		 * Contructeur de la classe, elle initialise le menu
		 */
		public function MenuContextual(arrayItemMenu:Array)
		{
			super();
			this.tabEnabled=false;
			this.owner=DisplayObjectContainer(Application.application);
			this.showRoot=showRoot;
			this.styleName="menuDisabledColor";

			popUpMenu(this, this.parent, arrayItemMenu);
		}

		/**
		 * Fonction permettant d'afficher le menu.
		 * Elle permet egalement l'ecoute de certain evenement du menu.
		 */
		public function showMenu(posX:Number=0, posY:Number=0, upMenu:Boolean=false, leftMenu:Boolean=false):void
		{
			/** écouteurs sur le menu */ 
			addEventListener(MenuEvent.ITEM_CLICK, onItemClickHandler);
			addEventListener(MouseEvent.ROLL_OVER, menuRollEvent);
			addEventListener(MouseEvent.ROLL_OUT, menuRollEvent);

			show();//affichage du menu

			if (leftMenu) //affichage du menu
				x=posX - width;
			else
				x=posX;
			if (upMenu)
				y=posY - height;
			else
				y=posY;
		}

		/**
		 * Fonction permettant de cacher le menu et de supprimer les ecouteurs du menus.
		 */
		private function maskMenu():void
		{
			hide();

			removeEventListener(MenuEvent.CHANGE, onItemClickHandler);
			removeEventListener(MouseEvent.ROLL_OVER, menuRollEvent);
			removeEventListener(MouseEvent.ROLL_OUT, menuRollEvent);
		}

		/**
		 * Fonction permettant d'invoquer la commande correspondante à l'item qui vient d'etre selectionné.
		 */
		private function onItemClickHandler(e:MenuEvent):void
		{
			var invoker:Invoker=new Invoker();
			invoker.setCommand(e.item.command);
			invoker.executeCommand();
		}

		/**
		 * Fonction permettant de reagir au evenement ROLL OVER et ROLL OUT sur le menu.
		 */
		private function menuRollEvent(e:*):void
		{
			var type:String=e.type;
			if (type == MouseEvent.ROLL_OVER)
			{
				dispatchEvent(new gestionparcEvent(gestionparcEvent.MENU_ROLL_OVER, true));
			}
			else if (type == MouseEvent.ROLL_OUT)
			{
				dispatchEvent(new gestionparcEvent(gestionparcEvent.MENU_ROLL_OUT, true));
				maskMenu();
			}
		}
	}
}
