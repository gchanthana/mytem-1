package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasEqpAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsTypeLigneMobile;

	public class RuleDissocierEqpForLigne implements IRuleDisplayItem
	{
		public function RuleDissocierEqpForLigne()
		{
		}

		public function isDisplay():Boolean
		{
			var r2:IRuleDisplayItem=new IsTypeLigneMobile();
			var r3:IRuleDisplayItem=new HasSimAssociated();
			var r4:IRuleDisplayItem=new HasEqpAssociated();
			
			if (r2.isDisplay() && r3.isDisplay() && r4.isDisplay())
				return true;
			else if (!r2.isDisplay() && r4.isDisplay())
				return true;
			else
				return false;
		}
	}
}
