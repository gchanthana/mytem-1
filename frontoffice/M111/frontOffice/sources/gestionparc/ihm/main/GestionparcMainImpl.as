package gestionparc.ihm.main
{
	import gestionparc.event.gestionparcEvent;

	import mx.containers.VBox;
	import mx.events.FlexEvent;

	public class GestionparcMainImpl extends VBox
	{
		public var specDispIHM:SpecificationDisplayIHM;
		public var mainGridIhm:MainGridIHM;

		public function GestionparcMainImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}

		private function onCreationComplete(ev:FlexEvent):void
		{
			this.addEventListener(gestionparcEvent.REFRESH_MAIN_GRID, onRefreshMainGridHandler);
		}

		public function resetDG():void
		{
			mainGridIhm.resetDG();
		}

		private function onRefreshMainGridHandler(event:gestionparcEvent):void
		{
			mainGridIhm.resetDG();
			mainGridIhm.getData();
		}
	}
}