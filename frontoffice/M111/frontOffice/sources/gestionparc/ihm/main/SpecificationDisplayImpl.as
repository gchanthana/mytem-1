package gestionparc.ihm.main
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.core.Container;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	
	import gestionparc.entity.AbstractVue;
	import gestionparc.entity.RestrictionVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.UserSessionVO;
	import gestionparc.event.UserAccessModelEvent;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.popup.PopUpChoixFiltreIHM;
	import gestionparc.ihm.fiches.popup.PopupChoosePoolIHM;
	import gestionparc.ihm.main.restriction.BtnListeRestrictionIHM;
	import gestionparc.ihm.main.vue.GestionnaireVue;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.collaborateur.CollaborateurServices;
	import gestionparc.services.useracess.UserAccessService;

	[Bindable]
	public class SpecificationDisplayImpl extends HBox
	{
		public var fiRestrictionValeur:HBox;
		public var fiMotclef:HBox;
		public var fiRestriction:HBox;
		public var fiPool:HBox;
		
		public var lblRestriction:FormItem;
		public var tiValue:TextInput;
		public var cbRestriction:BtnListeRestrictionIHM;
		public var tiMotClef:TextInput;
		public var btDisplay:Button;
		public var lbl_MotClef2:Label;

		public var cbDans:ComboBox;
		public var cbPool:ComboBox;
		public var cbChampPerso:ComboBox;
		public var img_chooseChampPerso:Image;
		public var img_delete:Image;
		
		public var sessionSpecificationVO:SpecificationVO;
		public var myServicesCache:CacheServices;
		public var myCollabService:CollaborateurServices;
		private var myUserAccessSrv:UserAccessService;
		
		public var cpt:Number=199999;

		private var listeRestriction:ArrayCollection;
		private var listeVue:ArrayCollection;
		private var listeContainer:Array=[];
		private var gestVue:GestionnaireVue;

		public var poolIndexToSelect:Number = -1;
		public var codeIndexToSelect:Number = -1;
		
		private var _popUpChoosePool		:PopupChoosePoolIHM;
		private var _poolSelected			:Boolean = false;
		private var _popUpChoixFiltre		:PopUpChoixFiltreIHM;
		private var _selectionSession		:UserSessionVO;
		
		public function SpecificationDisplayImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(event:FlexEvent):void
		{
			myServicesCache = new CacheServices();
			myCollabService = new CollaborateurServices();
			myUserAccessSrv = new UserAccessService;
			initSpecificationVo();
			initListener();
		}

		private function initSpecificationVo():void
		{
			if (SpecificationVO.getInstance() == null)
				sessionSpecificationVO=new SpecificationVO();
			else
				sessionSpecificationVO=SpecificationVO.getInstance();
			sessionSpecificationVO.displayObjectRef=this;
		}

		private function initData():void
		{
			if (CvAccessManager.getSession().INFOS_DIVERS && CvAccessManager.getSession().INFOS_DIVERS.IS_IN_COMMANDE_PROCESS)
			{
				poolIndexToSelect=ConsoviewUtil.getIndexById(CacheDatas.listePool, "IDPOOL", CvAccessManager.getSession().INFOS_DIVERS.POOL)
				delete CvAccessManager.getSession().INFOS_DIVERS.M111_CACHE;
			}
			else
			{
				myServicesCache.getListePool();
				myServicesCache.getCacheData();
				
				if (!CvAccessManager.getSession().INFOS_DIVERS)
				{
					CvAccessManager.getSession().INFOS_DIVERS={};
				}
			}
			myCollabService.getMatriculeAuto();
			CvAccessManager.getSession().INFOS_DIVERS.IS_IN_COMMANDE_PROCESS=false;
			
			poolIndexToSelect=-1;
			codeIndexToSelect = -1;
		}

		public function initListener(bool:Boolean=true):void
		{
			if (bool)
			{
				btDisplay.addEventListener(MouseEvent.CLICK, onBtDisplayClickHandler);
				cbRestriction.addEventListener(gestionparcEvent.RESTRICTION_SELECTED, restrictionSelectedHandler);
				this.myServicesCache.myDatas.addEventListener(gestionparcEvent.REFRESH_LISTE_POOL, listePoolLoaded);
				this.myServicesCache.myDatas.addEventListener(gestionparcEvent.RECUPERER_DROITUSER_FINISHED, afterRecupDroitUser);
				this.myServicesCache.myDatas.addEventListener(gestionparcEvent.LISTE_COLONNE_PERSO, afterRecupChampsPersos);
				
				moduleGestionParcIHM.userAccessSrv.model.addEventListener(UserAccessModelEvent.UAME_USER_ACCESS_UPDATED, initPoolsdHandler);
			}
			else
			{
				if (btDisplay && btDisplay.hasEventListener(MouseEvent.CLICK))
					btDisplay.removeEventListener(MouseEvent.CLICK, onBtDisplayClickHandler);
				if (cbRestriction && cbRestriction.hasEventListener(gestionparcEvent.RESTRICTION_SELECTED))
					cbRestriction.removeEventListener(gestionparcEvent.RESTRICTION_SELECTED, restrictionSelectedHandler) ;
				if (myServicesCache && myServicesCache.myDatas && this.myServicesCache.myDatas.hasEventListener(gestionparcEvent.REFRESH_LISTE_POOL))
					this.myServicesCache.myDatas.removeEventListener(gestionparcEvent.REFRESH_LISTE_POOL, listePoolLoaded);
				if (myServicesCache && myServicesCache.myDatas && this.myServicesCache.myDatas.hasEventListener(gestionparcEvent.RECUPERER_DROITUSER_FINISHED))
					this.myServicesCache.myDatas.removeEventListener(gestionparcEvent.RECUPERER_DROITUSER_FINISHED, afterRecupDroitUser);
				if (myServicesCache && myServicesCache.myDatas && this.myServicesCache.myDatas.hasEventListener(gestionparcEvent.LISTE_COLONNE_PERSO))
					this.myServicesCache.myDatas.removeEventListener(gestionparcEvent.LISTE_COLONNE_PERSO, afterRecupChampsPersos);
			}
		}
		
		private function initPoolsdHandler(ev:UserAccessModelEvent):void
		{
			if(moduleGestionParcIHM.userAccessSrv.model.hasEventListener(UserAccessModelEvent.UAME_USER_ACCESS_UPDATED))
				moduleGestionParcIHM.userAccessSrv.model.removeEventListener(UserAccessModelEvent.UAME_USER_ACCESS_UPDATED, initPoolsdHandler);
			
			this.initData();
			
		}
		
		private function listePoolLoaded(evt:gestionparcEvent):void
		{
			if (CacheDatas.listePool.length == 1)
			{
				poolIndexToSelect = 1;
				cbPool_changeHandler(null);
			}
		}
		
		private function listeValeursChampsPersoHandler(evt:gestionparcEvent):void
		{
			onChooseFiltre_clickHandler();
			
			if (myCollabService && myCollabService.myDatas && this.myCollabService.myDatas.hasEventListener(gestionparcEvent.LISTE_VALEURS_CHAMPS_PERSO))
				this.myCollabService.myDatas.removeEventListener(gestionparcEvent.LISTE_VALEURS_CHAMPS_PERSO, listeValeursChampsPersoHandler);
		}

		/** Description : Colore un container et l'ajoute à la liste des containers orangés */

		private function addContainerToListe(obj:Container):void
		{
			if (obj != null && !SpecificationVO.getInstance().boolFirstUse)
			{
				FillColorToContainer(obj);
				listeContainer.push(obj);
			}
		}

		/**  Description : décolore les containers de la liste et réinitialise le tableau des containers */

		private function resetListeContainer():void
		{
			for each (var obj:Container in listeContainer)
			{
				FillColorToContainer(obj, false);
			}
			listeContainer=[];
		}

		/** Description : si toDO est TRUE, on applique le style CONTAINER_CHANGED au container. Sinon on applique le style CONTAINER_DEFAULT au container. */

		private function FillColorToContainer(ct:Container, toDo:Boolean=true):void
		{
			if (toDo)
			{
				ct.setStyle("styleName", "ContainerChanged");
			}
			else
			{
				ct.setStyle("styleName", "ContainerDefault");
			}
		}

		/**
		 * recupérer la liste des vues disponibles 
		 * 
		 */		
		private function buildListVue(ev:Event=null):void
		{
			var vueSelected:AbstractVue;

			gestVue=new GestionnaireVue();
			listeVue=gestVue.buildListVue();

			/** initialiser idView ,view ,ViewObject pour initialiser le  combox de restriction */
			
			sessionSpecificationVO.idView=listeVue[1].IDVUE; /** listeVue[1] parce qu'il y a une seule vue sinon on va utiliser selectedItem d'un combox*/
			sessionSpecificationVO.view=listeVue[1].LIBELLE;
			sessionSpecificationVO.ViewObject=listeVue[1];
			cbVue_changeHandler(null);
		}

		private function afterRecupDroitUser(e:gestionparcEvent):void
		{
			CvAccessManager.getSession().INFOS_DIVERS.M111_CACHE = CacheDatas;
			SpecificationVO.getInstance().initDroitCommandes(CacheDatas.listeDroitCommande);
			buildListVue();
		}
		
		private function afterRecupChampsPersos(e:gestionparcEvent):void
		{
			getInfosUserSession();
		}
		
		private function getInfosUserSession():void
		{
			if(moduleGestionParcIHM.globalParameter.userSession)
			{	
				poolIndexToSelect = ConsoviewUtil.getIndexById(CacheDatas.listePool, 'IDPOOL', moduleGestionParcIHM.globalParameter.userSession.IDPOOL);
				cbPool.selectedIndex = poolIndexToSelect;
				if(poolIndexToSelect > -1)
				{
					codeIndexToSelect = ConsoviewUtil.getIndexByLabel(CacheDatas.listeChampPerso, 'CODE', moduleGestionParcIHM.globalParameter.userSession.COLONNE_CODE);
					cbChampPerso.selectedIndex = codeIndexToSelect;
					lbl_MotClef2.text = moduleGestionParcIHM.globalParameter.userSession.COLONNE_VALUE;
				}
				else
				{
					cbChampPerso.selectedIndex = -1;
					cbChampPerso.prompt = ResourceManager.getInstance().getString('M111','S_lectionner')
					lbl_MotClef2.text = '';
				}
				
				treatOnPoolChanged();
			}	
		}

		/**
		 * permet de dispatcher un event ,cet event est capturé dans GestionparcMainImpl.as
		 * @param event
		 */
		private function onBtDisplayClickHandler(event:MouseEvent=null):void
		{
			majSpecificationVoInstance();

			if (cbPool.selectedIndex > -1 && cbDans.selectedIndex > -1)
			{
				if(cbPool.selectedItem.IDPOOL == SpecificationVO.POOL_TOUT_PARC_ID)
					SpecificationVO.pool_is_tout_parc = true;
				else
					SpecificationVO.pool_is_tout_parc  = false;
				
				if (sessionSpecificationVO.boolFirstUse)
				{
					dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_MAIN_GRID, null, true));
					resetListeContainer();
					sessionSpecificationVO.boolFirstUse=false;
				}
				else
				{
					if (sessionSpecificationVO.boolHasChanged)
					{
						dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_MAIN_GRID, null, true));
						resetListeContainer();
					}
				}
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'Avant_d_effectuer_une_recherche__vous_de'), ResourceManager.getInstance().getString('M111', 'Erreur'), Alert.OK, Application.application as Sprite);
			}
		}

		private function majSpecificationVoInstance():void
		{
			if (cbPool.selectedIndex > -1)
			{
				sessionSpecificationVO.idPool=cbPool.selectedItem.IDPOOL; // id pool après validation de la recherche
				sessionSpecificationVO.validSelectedIdPool = cbPool.selectedItem.IDPOOL;//MYT-1606
				sessionSpecificationVO.labelPool=cbPool.selectedItem.LIBELLE_POOL;
				sessionSpecificationVO.idProfil=cbPool.selectedItem.IDPROFIL;
				sessionSpecificationVO.labelProfil=cbPool.selectedItem.LIBELLE_PROFIL;
				sessionSpecificationVO.idRevendeur=cbPool.selectedItem.IDREVENDEUR;
			}
			
			if (tiMotClef != null)
			{
				sessionSpecificationVO.keyWord=tiMotClef.text;
			}
			
			if (cbDans.selectedIndex > -1)
			{
				sessionSpecificationVO.idKeyWordScope=cbDans.selectedItem.CODE;
				sessionSpecificationVO.labelSort=cbDans.selectedItem.SORT_FIELD;
				sessionSpecificationVO.orderByReq=cbDans.selectedItem.SORT_ORDER;
			}
			
			if (cbChampPerso.selectedIndex > -1)
			{
				sessionSpecificationVO.idKeyWordScope2 = cbChampPerso.selectedItem.CODE;
				
				if (lbl_MotClef2 != null)
				{
					sessionSpecificationVO.keyWord2=lbl_MotClef2.text;
				}
			}
			else
			{
				sessionSpecificationVO.idKeyWordScope2 = null;
				sessionSpecificationVO.keyWord2 = null;
			}

			sessionSpecificationVO.idRestriction=0;
			sessionSpecificationVO.restriction="";
			sessionSpecificationVO.valueRestriction="";
			if (cbRestriction.SelectedItem != null)
			{
				sessionSpecificationVO.idRestriction=cbRestriction.SelectedItem.DATA;
				sessionSpecificationVO.restriction=cbRestriction.SelectedItem.LIBELLE;

				if (cbRestriction.SelectedItem.SAISIE)
				{
					if (tiValue != null && tiValue.text != '')
						sessionSpecificationVO.valueRestriction=tiValue.text;
					else
						sessionSpecificationVO.valueRestriction="";
				}
			}
		}

		protected function cbPool_changeHandler(event:ListEvent):void
		{
			treatOnPoolChanged();
			codeIndexToSelect = -1;
			lbl_MotClef2.text = '';
			myUserAccessSrv.setInfosUserSession(cbPool.selectedItem.IDPOOL);
		}
		
		protected function cbChangeChampsPersoHandler(event:ListEvent):void
		{
			if(cbPool.selectedIndex > -1)
			{ 
				this.myCollabService.myDatas.addEventListener(gestionparcEvent.LISTE_VALEURS_CHAMPS_PERSO, listeValeursChampsPersoHandler);
				var idpool_:Number = cbPool.selectedItem.IDPOOL;
				var code_:String = cbChampPerso.selectedItem.CODE;
				lbl_MotClef2.text = '';
				if(idpool_ == -10)// si le pool tout le parc on on envoie id = 0 comme si c'est parc global
					idpool_ = 0;
				myCollabService.getValeursChampPersos(idpool_, code_);
				myUserAccessSrv.setInfosUserSession(idpool_, code_);

			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Veuillez_s_lectionner_un_pool__'),ResourceManager.getInstance().getString('M111', 'Erreur'));
			}
		}
		

		private function treatOnPoolChanged(infos:int = 1):void
		{
			var selectedObjPool:Object = cbPool.selectedItem;
			if (cbPool.selectedIndex > -1)
			{
				myServicesCache.initDataInCache(selectedObjPool.IDPOOL, selectedObjPool.IDPROFIL);
				SpecificationVO.getInstance().idPoolNotValid=selectedObjPool.IDPOOL; // id de pool avant cliquer sur le button recherche
				SpecificationVO.getInstance().labelPoolNotValid=selectedObjPool.LIBELLE_POOL;
				SpecificationVO.getInstance().idProfilNotValid=selectedObjPool.IDPROFIL;
				SpecificationVO.getInstance().labelProfilNotValid=selectedObjPool.LIBELLE_PROFIL;
				SpecificationVO.getInstance().idRevendeurNotValid=selectedObjPool.IDREVENDEUR;
			}
			
			addContainerToListe(fiPool);
		}
		
		protected function tiMotClef_changeHandler(event:Event):void
		{
			addContainerToListe(fiMotclef);
		}

		protected function tiMotClefKeyDownHandler(evt:KeyboardEvent):void
		{
			if (evt.keyCode == Keyboard.ENTER)
				onBtDisplayClickHandler();
		}

		protected function cbDans_changeHandler(event:ListEvent):void
		{
			addContainerToListe(fiMotclef);
		}

		protected function restrictionSelectedHandler(event:gestionparcEvent):void
		{
			if (cbRestriction.SelectedItem.SAISIE)
			{
				tiValue.text='';
				tiValue.visible=true;
				fiRestrictionValeur.visible=true;
			}
			else if (cbRestriction.SelectedItem.VALUE)
			{
				tiValue.text='';
				tiValue.visible=false;
				fiRestrictionValeur.visible=true;
			}
			else
			{
				tiValue.text='';
				tiValue.visible=false;
				fiRestrictionValeur.visible=false;
			}
			addContainerToListe(fiRestriction);
		}

		/**
		 * changer la liste de retriction lors qu'on change la vue 
		 * en effet, il y a qu'une vue en dehore de la vue parc global qui est considérer comme un pool c'est pour cela on utilise listeVue[1]
		 * @param event
		 * 
		 */		
		protected function cbVue_changeHandler(event:ListEvent):void
		{
			cbRestriction.DATAPROVIDER=listeVue[1].LISTE_RESTRICTION; /**  vu parc est à la position 1 */
			cbRestriction.SelectedItem=cbRestriction.DATAPROVIDER.getItemAt(0) as RestrictionVO;
			tiValue.visible=false;
			tiValue.text="";
		}

		protected function tiValue_changeHandler(event:Event):void
		{
			addContainerToListe(fiRestrictionValeur);
		}

		protected function cbValue_changeHandler(event:ListEvent):void
		{
			addContainerToListe(fiRestrictionValeur);
		}
		
		public function btImgChoosePool_clickHandler(e:Event):void
		{
			var pools:ArrayCollection = cbPool.dataProvider as ArrayCollection;
			if(cbPool.selectedIndex > -1)
				this.majSelectPool(pools);
			_popUpChoosePool = new PopupChoosePoolIHM;
			var point:Point = new Point(stage.mouseX,stage.mouseY);
			_popUpChoosePool.x = point.x - cbPool.width; 
			_popUpChoosePool.y = point.y;
			_popUpChoosePool.listePool = pools;
			_popUpChoosePool.addEventListener(gestionparcEvent.VALID_CHOIX_POOL, getItemPoolHandler);
			PopUpManager.addPopUp(_popUpChoosePool, this, true);
		}
		
		public function onChooseFiltre_clickHandler():void
		{
			if(cbChampPerso.selectedIndex > -1)
			{
				_popUpChoixFiltre = new PopUpChoixFiltreIHM();
				var point:Point = new Point(stage.mouseX,stage.mouseY);
				_popUpChoixFiltre.x = 275 ; 
				_popUpChoixFiltre.y = 230;
				_popUpChoixFiltre.listeFiltre = myCollabService.myDatas.listeValeursPerso;
				_popUpChoixFiltre.addEventListener(gestionparcEvent.VALID_CHOIX_FILTRE, getSelectedValeurFiltreHandler);
				PopUpManager.addPopUp(_popUpChoixFiltre, this, true);
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111','Veuillez_s_lectionner_un__l_ment_dans_la'), ResourceManager.getInstance().getString('M111', 'Erreur'));
		}
		
		private function majSelectPool(_pools:ArrayCollection):void
		{
			var longPool:int = _pools.length;
			var i:int = 0;
			while(i < longPool)
			{
				_pools[i].SELECTED = false;
				if(_pools[i].IDPOOL == cbPool.selectedItem.IDPOOL)
				{
					_pools[i].SELECTED = true;
					_poolSelected = true;
				}
				i++;
			}
		}
		
		protected function getItemPoolHandler(e:Event):void
		{
			_popUpChoosePool.removeEventListener(gestionparcEvent.VALID_CHOIX_POOL, getItemPoolHandler);
			cbPool.selectedItem = _popUpChoosePool.itemCurrent;
			treatOnPoolChanged();
		}
		
		protected function getSelectedValeurFiltreHandler(gpevt:gestionparcEvent):void
		{
			lbl_MotClef2.text = gpevt.obj as String;
			myUserAccessSrv.setInfosUserSession(cbPool.selectedItem.IDPOOL, cbChampPerso.selectedItem.CODE, lbl_MotClef2.text)
		}
		
		protected function imgEraseFilterClickHandler(mevt:MouseEvent):void
		{
			lbl_MotClef2.text = '';
			codeIndexToSelect = -1;cbChampPerso.selectedIndex = -1
			sessionSpecificationVO.idKeyWordScope2 = null;
			sessionSpecificationVO.keyWord2 = null;
			myUserAccessSrv.setInfosUserSession(cbPool.selectedItem.IDPOOL);
		}
		
		public function get selectionSession():UserSessionVO { return _selectionSession; }
		
		public function set selectionSession(value:UserSessionVO):void
		{
			if (_selectionSession == value)
				return;
			_selectionSession = value;
		}
	}
}
