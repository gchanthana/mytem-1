package gestionparc.ihm.main.restriction
{
	import mx.collections.ArrayCollection;
	import mx.containers.Form;
	import mx.containers.HBox;
	import mx.controls.PopUpButton;
	import mx.events.DropdownEvent;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import gestionparc.entity.RestrictionVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.main.restriction.ItemRenderer.ItemRendererRestrictionIHM;

	[Bindable]
	public class BtnListeRestrictionImpl extends HBox
	{
		private var _DATAPROVIDER:ArrayCollection;

		public var SelectedItem:RestrictionVO;
		public var btn:PopUpButton;
		public var frm:Form;

		public function BtnListeRestrictionImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, buildMenu);
		}

		public function restriction_clickHandler(e:gestionparcEvent):void
		{
			SelectedItem=e.obj as RestrictionVO;
			buildMenu();
			var ev:gestionparcEvent=new gestionparcEvent(gestionparcEvent.RESTRICTION_SELECTED);
			dispatchEvent(ev);
			btn.close();
		}

		public function get DATAPROVIDER():ArrayCollection
		{
			return _DATAPROVIDER
		}

		public function set DATAPROVIDER(value:ArrayCollection):void
		{
			_DATAPROVIDER=value;
			buildMenu();
		}

		private function buildMenu(ev:FlexEvent=null):void
		{
			frm.removeAllChildren();
			for each (var rest:RestrictionVO in _DATAPROVIDER)
			{
				var itrend:ItemRendererRestrictionIHM = new ItemRendererRestrictionIHM();
				if (rest == SelectedItem)
					itrend.SELECTED = true;
				else
					itrend.SELECTED = false;
				itrend.addEventListener(gestionparcEvent.RESTRICTION_SELECTED, restriction_clickHandler);
				itrend.DATA = rest;
				frm.addChild(itrend);
			}
		}
		
		protected function btn_closeHandler(event:DropdownEvent):void
		{
			if(SelectedItem)
			{
				if(SelectedItem.ISRESTRICTIONCOLLABORATEUR && (SelectedItem.DATA != 0))
				{
					btn.label = ResourceManager.getInstance().getString('M111', 'COLLABORATEUR') + ' / '+ SelectedItem.LIBELLE;
				}
				else if(SelectedItem.ISRESTRICTIONLIGNE && (SelectedItem.DATA != 0))
				{
					btn.label = ResourceManager.getInstance().getString('M111', 'LIGNE') + ' / ' + SelectedItem.LIBELLE;
				}
				else if((SelectedItem.ISRESTRICTIONSIM) && (SelectedItem.DATA != 30) && (SelectedItem.DATA != 0))
				{
					btn.label =  ResourceManager.getInstance().getString('M111', 'SIM') + ' / '+  SelectedItem.LIBELLE;
				}
				else if(SelectedItem.ISRESTRICTIONEQUIPEMENT && (SelectedItem.DATA != 0))
				{
					btn.label = ResourceManager.getInstance().getString('M111', 'EQUIPEMENT') + ' / '+ SelectedItem.LIBELLE;
				}
				else if(SelectedItem.DATA == 30)
				{
					btn.label = ResourceManager.getInstance().getString('M111', 'EXTENSION_') + ' / '+ SelectedItem.LIBELLE;
				}
				else
					btn.label = SelectedItem.LIBELLE;
			}
			
		}
	}
}