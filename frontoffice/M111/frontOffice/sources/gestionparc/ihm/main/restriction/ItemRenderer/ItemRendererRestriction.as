package gestionparc.ihm.main.restriction.ItemRenderer
{
	import flash.events.MouseEvent;

	import gestionparc.entity.RestrictionVO;
	import gestionparc.event.gestionparcEvent;

	import mx.containers.FormItem;
	import mx.controls.LinkButton;
	import mx.skins.ProgrammaticSkin;

	public class ItemRendererRestriction extends FormItem
	{
		[Bindable]
		public var Clickable:Boolean=false;

		[Bindable]public var lbl:LinkButton;

		private var _DATA:RestrictionVO;

		private var _SELECTED:Boolean=false;

		private const BACKGROUND_COLOR_SELECTED:Object="#558ed5";
		private const BACKGROUND_COLOR:Object=new ProgrammaticSkin();


		public function ItemRendererRestriction()
		{
			super();
		}

		protected function lbl_clickHandler(event:MouseEvent):void
		{
			var ev:gestionparcEvent=new gestionparcEvent(gestionparcEvent.RESTRICTION_SELECTED, DATA);
			dispatchEvent(ev);
		}

		private function changeBackground():void
		{
			if (_SELECTED)
			{
				this.setStyle("backgroundColor", BACKGROUND_COLOR_SELECTED);
				this.setStyle("backgroundAlpha", 0.6);
			}
			else
			{
				this.setStyle("backgroundColor", BACKGROUND_COLOR);
				this.setStyle("backgroundAlpha", 1);
			}
		}

		public function set SELECTED(value:Boolean):void
		{
			_SELECTED=value;
			changeBackground();
		}

		[Bindable]
		public function get SELECTED():Boolean
		{
			return _SELECTED
		}

		[Bindable]
		public function get DATA():RestrictionVO
		{
			return _DATA;
		}

		public function set DATA(value:RestrictionVO):void
		{
			_DATA=value;
			if (_DATA.ISTITLE)
				this.label=_DATA.LIBELLE;
			else
				this.label="";
		}
	}
}

