package gestionparc.ihm.main.vue
{
	import gestionparc.ihm.main.restriction.GestionnaireRestriction;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import gestionparc.event.GetColonneEvent;
	import gestionparc.services.getcolonnes.GetColonneService;

	public class GestionnaireVue
	{
		private var listeVue:ArrayCollection;

		public static const nomVueParc:String=ResourceManager.getInstance().getString('M111', 'Parc');
		public static const nomVueParcGlobal:String=ResourceManager.getInstance().getString('M111', 'Parc_Global');
		public static const nomVueToutParc:String=ResourceManager.getInstance().getString('M111', 'Tout_le_parc');
				
		private var gestRestriction:GestionnaireRestriction=new GestionnaireRestriction();
		private var _getColonneService=new GetColonneService();

		
		
		public function GestionnaireVue()
		{
			//generateTableauVue();
			if(!_getColonneService.model.hasEventListener(GetColonneEvent.GET_ALL_COLONNES)){
				_getColonneService.model.addEventListener(GetColonneEvent.GET_ALL_COLONNES, getColonneHandler);	
			}
			
			_getColonneService.getColonneVueParc();

			listeVue=new ArrayCollection();
			listeVue.addItem(new VueParcGlobal(new ArrayCollection()));
			listeVue.addItem(new VueParc(new ArrayCollection()));
			listeVue.addItem(new VueToutParc(new ArrayCollection()));		
		}

		private function getColonneHandler(evt:GetColonneEvent):void{
				for(var i:int=0; i< listeVue.length; i++ ){
				 listeVue[i].setColonne(_getColonneService.model.listeResultat);
			}	
		}

		public function buildListVue():ArrayCollection
		{
			return listeVue;
		}

	}
}
