package gestionparc.ihm.question.ligne
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.services.associer.ActionAssocierServices;
	
	import mx.controls.DateField;

	public class QuestionAssocierEqpToLigne
	{
		import flash.display.Sprite;

		import gestionparc.ihm.fiches.listedispo.IListeDispo;

		import mx.controls.Alert;
		import mx.core.Application;
		import mx.resources.ResourceManager;

		private var liste:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionAssocierServices=new ActionAssocierServices();

		public function QuestionAssocierEqpToLigne(obj_dispo:IListeDispo, check:Boolean, date:Date)
		{
			this.liste=obj_dispo;
			this.check=check;
			this.date=date;
			Alert.yesLabel=ResourceManager.getInstance().getString('M111', 'oui');
			Alert.noLabel=ResourceManager.getInstance().getString('M111', 'non');
			Alert.cancelLabel=ResourceManager.getInstance().getString('M111', 'Annuler')
			Alert.okLabel=ResourceManager.getInstance().getString('M111', 'Valider')

			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			/** lors que je part de la ligne pour ajouter un eqp */
			if (ligne.IDTYPE_LIGNE ==moduleGestionParcIHM.TYPE_LIGNE_MOBILE)
				associerEqpToLigne2();
			else 
				associerEqpToLigne();
		}

		private function associerEqpToLigne():void
		{
			serv.associerEqpToLigne1(setObj());
		}

		// ASSOCIER UNE LIGNE A UN EQUIPEMENT MOBILE ( LA SIM A L'EQUIPEMENT )
		private function associerEqpToLigne2():void
		{
			serv.associerEqpToLigne2(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=liste.getSelection().ID;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE;
			obj.ITEM2=liste.getSelection().IMEI;
			obj.ID_TERMINAL=liste.getSelection().IDTERM;
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}
