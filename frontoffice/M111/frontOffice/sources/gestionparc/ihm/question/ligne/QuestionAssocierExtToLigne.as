package gestionparc.ihm.question.ligne
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.services.associer.ActionAssocierServices;
	
	import mx.controls.DateField;

	public class QuestionAssocierExtToLigne
	{
		import flash.display.Sprite;

		import gestionparc.ihm.fiches.listedispo.IListeDispo;

		import mx.controls.Alert;
		import mx.core.Application;
		import mx.resources.ResourceManager;

		private var liste:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionAssocierServices=new ActionAssocierServices();

		public function QuestionAssocierExtToLigne(obj_dispo:IListeDispo, check:Boolean, date:Date)
		{
			this.liste=obj_dispo;
			this.check=check;
			this.date=date;
			Alert.yesLabel=ResourceManager.getInstance().getString('M111', 'oui');
			Alert.noLabel=ResourceManager.getInstance().getString('M111', 'non');
			Alert.cancelLabel=ResourceManager.getInstance().getString('M111', 'Annuler')
			Alert.okLabel=ResourceManager.getInstance().getString('M111', 'Valider')

			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			associerExtToLigne()
		}

		private function associerExtToLigne():void
		{
			serv.associerExtToLigne(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=liste.getSelection().ID
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE;
			obj.ITEM2=liste.getSelection().EX_NUMERO;
			obj.ID_TERMINAL=0;
			obj.ID_EXT=liste.getSelection().IDEXTENSION;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}
