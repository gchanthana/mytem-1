package gestionparc.ihm.question.extention
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.services.associer.ActionAssocierServices;
	
	import mx.controls.DateField;

	public class QuestionAssocierLigneToExt
	{
		import flash.display.Sprite;

		import gestionparc.ihm.fiches.listedispo.IListeDispo;

		import mx.controls.Alert;
		import mx.core.Application;
		import mx.resources.ResourceManager;

		private var liste:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionAssocierServices=new ActionAssocierServices();

		public function QuestionAssocierLigneToExt(obj_dispo:IListeDispo, check:Boolean, date:Date)
		{
			this.liste=obj_dispo;
			this.check=check;
			this.date=date;
			associerLigneToExt();
		}

		private function associerLigneToExt():void
		{
			serv.associerLigneToExt(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=liste.getSelection().ID
			obj.ITEM1=liste.getSelection().LIGNE;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.EX_NUMERO;
			obj.ID_TERMINAL=0;
			obj.ID_EXT=SpecificationVO.getInstance().elementDataGridSelected.IDEXTENSION;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=liste.getSelection().IDSOUS_TETE;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}
