package gestionparc.ihm.question.extention
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.services.dissocier.ActionDissocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheDissocierCollabFromExt
	{
		private var serv:ActionDissocierServices=new ActionDissocierServices();

		public function QuestionFicheDissocierCollabFromExt()
		{
			Alert.yesLabel=ResourceManager.getInstance().getString('M111', 'oui');
			Alert.noLabel=ResourceManager.getInstance().getString('M111', 'non');
			Alert.cancelLabel=ResourceManager.getInstance().getString('M111', 'Annuler')
			Alert.okLabel=ResourceManager.getInstance().getString('M111', 'Valider')

			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			if (ligne.T_IMEI != null && ligne.T_IMEI != "")
			{
				
				Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEDISSOCIERCOLLABFROMEXT"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnCloseHandler)
			}
			else
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString("M111", "Etre_vous_s_r_de_vouloir_dissocier_l_extension_du_collaborateur"), ResourceManager.getInstance().getString('M111', 'Dissocier_extension_du_collaborateur'), alertClickHandler);	
			}
		}
		
		/**
		 * permet de capter la réponse de l'utilisateur
		 */
		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				dissocierCollabFromExt();
			}
		}

		private function qnCloseHandler(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
			{
				dissocierCollabFromSimIfOK();
			}
			else if (evt.detail == Alert.NO)
			{
				dissocierCollabFromSimIfNotOk()
			}
			else if (evt.detail == Alert.CANCEL)
			{

			}
		}

		private function dissocierCollabFromSimIfOK():void
		{
			serv.dissocierCollabFromExt1(setObj());
		}

		private function dissocierCollabFromSimIfNotOk():void
		{
			serv.dissocierCollabFromExt2(setObj());
		}

		private function dissocierCollabFromExt():void
		{
			serv.dissocierCollabFromExt3(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.iddestination=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.EX_NUMERO;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			obj.ID_TERMINAL=0;
			obj.ID_EXT=SpecificationVO.getInstance().elementDataGridSelected.IDEXTENSION;
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(new Date(), 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}
	}
}
