package gestionparc.ihm.question.extention
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.services.associer.ActionAssocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.events.CloseEvent;

	public class QuestionAssocierCollabToExt
	{
		import flash.display.Sprite;

		import gestionparc.ihm.fiches.listedispo.IListeDispo;

		import mx.controls.Alert;
		import mx.core.Application;
		import mx.resources.ResourceManager;

		private var liste:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionAssocierServices=new ActionAssocierServices();

		public function QuestionAssocierCollabToExt(obj_dispo:IListeDispo, check:Boolean, date:Date)
		{
			this.liste=obj_dispo;
			this.check=check;
			this.date=date;
			Alert.cancelLabel=ResourceManager.getInstance().getString('M111', 'Annuler')
			Alert.okLabel=ResourceManager.getInstance().getString('M111', 'Valider')

			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			if (liste.getSelection().FALSE_COLLAB == 2 && liste.getSelection().IDEQP != null && liste.getSelection().IDEQP > -1)
			{
				var str:String=ResourceManager.getInstance().getString("M111", "QUESTION_FICHEASSOCIERCOLLABTOEXT") + " " + ligne.EX_NUMERO + " " + ResourceManager.getInstance().getString("M111", "QUESTION_ASSOCIE_A_") + " " + liste.getSelection().NOM;
				Alert.show(str, ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.OK | Alert.CANCEL, Application.application as Sprite, qnCloseValidation)
			}
			else
			{
				associerCollabToExt2()
			}
		}

		private function qnCloseValidation(evt:CloseEvent):void
		{
			if (evt.detail == Alert.OK)
			{
				associerCollabToExt1();
			}
		}

		private function associerCollabToExt1():void
		{
			serv.associerCollabToExt1(setObj());
		}

		private function associerCollabToExt2():void
		{
			serv.associerCollabToExt2(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=this.liste.getSelection().ID
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.EX_NUMERO;
			obj.ITEM2=liste.getSelection().NOM;
			obj.ID_TERMINAL=0;
			obj.ID_EXT=SpecificationVO.getInstance().elementDataGridSelected.IDEXTENSION;
			obj.ID_EMPLOYE=liste.getSelection().IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}