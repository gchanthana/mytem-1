package gestionparc.ihm.question.equipement
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.services.associer.ActionAssocierServices;
	
	import mx.controls.DateField;
	import mx.events.CloseEvent;

	public class QuestionFicheAssocierCollabToEqp
	{
		import flash.display.Sprite;

		import gestionparc.ihm.fiches.listedispo.IListeDispo;

		import mx.controls.Alert;
		import mx.core.Application;
		import mx.resources.ResourceManager;

		private var calendar:Date;
		private var isPret:Boolean=false;
		private var liste:IListeDispo;

		public var serv:ActionAssocierServices=new ActionAssocierServices();

		public function QuestionFicheAssocierCollabToEqp(obj_dispo:IListeDispo, calendar:Date, isPret:Boolean)
		{
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			this.calendar=calendar;
			this.isPret=isPret;
			this.liste=obj_dispo;

			Alert.okLabel=ResourceManager.getInstance().getString("M111", "Valider");
			Alert.cancelLabel=ResourceManager.getInstance().getString("M111", "Annuler");

			if (liste.getSelection().FALSE_COLLAB == 2 && liste.getSelection().LIGNE != null && liste.getSelection().LIGNE != "" && liste.getSelection().LIGNE.length > 0)
			{
				var str:String=ResourceManager.getInstance().getString("M111", "QUESTION_FICHEASSOCIERCOLLABTOEQP1") + " " + ligne.T_MODELE + " - " + ligne.T_IMEI + " " + ResourceManager.getInstance().getString("M111", "QUESTION_ASSOCIE_A_") + " " + obj_dispo.getSelection().NOM;
				Alert.show(str, ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.OK | Alert.CANCEL, Application.application as Sprite, qnHandler)
			}
			else
			{
				associerCollabToEqp()
			}
		}

		private function qnHandler(evt:CloseEvent):void
		{
			if (evt.detail == Alert.OK)
			{
				associerCollabToEqp();
			}
		}

		private function associerCollabToEqp():void
		{
			serv.associerCollabToEqp1(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=liste.getSelection().ID
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.T_IMEI;
			obj.ITEM2=liste.getSelection().NOM;
			obj.ID_TERMINAL=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=liste.getSelection().IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString((calendar != null) ? calendar : new Date(), 'YYYY/MM/DD');
			obj.IS_PRETER=isPret;
			return obj;
		}
	}
}