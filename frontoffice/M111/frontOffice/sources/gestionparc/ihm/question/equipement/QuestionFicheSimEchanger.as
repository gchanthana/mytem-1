package gestionparc.ihm.question.equipement
{
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.services.sim.SimServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheSimEchanger
	{
		private var calendar:CvDateChooser;
		private var liste:IListeDispo;
		public var serv:SimServices=new SimServices();

		public function QuestionFicheSimEchanger(dg_liste:IListeDispo, cal:CvDateChooser)
		{
			this.liste=dg_liste;
			this.calendar=cal;

			if (liste.getSelection().IDSOUS_TETE == null || liste.getSelection().IDSOUS_TETE < -1)
			{
				Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHESIMECHANGER"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnClose)
			}
			else
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'Etre_vous_s_r_de_vouloir__changer_cette_sim'), ResourceManager.getInstance().getString('M111', 'Echanger_SIM'), alertClickHandler);		
			}
		}

		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				echangerSim1();
			}
		}
		/* LIGNE MOBILE */
		private function qnClose(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
				echangerSim2();
			else if (evt.detail == Alert.NO)
				echangerSim1()
		}

		private function echangerSim1():void
		{
			serv.echangerSim1(setObj())
		}

		private function echangerSim2():void
		{
			serv.echangerSim2(setObj())
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idOldeqp=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.idNeweqp=liste.getSelection().ID;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.S_IMEI;
			obj.ITEM2=liste.getSelection().NUM_SIM;
			obj.ID_TERMINAL=0;
			obj.ID_SIM1=SpecificationVO.getInstance().elementDataGridSelected.IDSIM;
			obj.ID_SIM2=parseInt(liste.getSelection().IDEQUIPEMENT);
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(calendar.selectedDate, 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}
	}
}
