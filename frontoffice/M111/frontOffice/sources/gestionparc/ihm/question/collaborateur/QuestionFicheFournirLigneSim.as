package gestionparc.ihm.question.collaborateur
{
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.services.associer.ActionAssocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheFournirLigneSim
	{
		private var liste:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionAssocierServices=new ActionAssocierServices();

		public function QuestionFicheFournirLigneSim(obj_dispo:IListeDispo, check:Boolean, date:Date)
		{
			this.liste=obj_dispo;
			this.check=check;
			this.date=date;
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			if (ligne && ligne.IDTERMINAL > 0)
			{
				Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEFOURNIRLIGNESIM"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnCloseHandler)
			}
			else
			{
				associerFournirLigneSim3();
			}
		}

		private function qnCloseHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.YES)
			{
				associerFournirLigneSim1();
			}
			else if (e.detail == Alert.CANCEL)
			{

			}
			else
			{
				associerFournirLigneSim2();
			}
		}

		private function associerFournirLigneSim1():void
		{
			serv.associerFournirLigneSim1(setObj());
		}

		private function associerFournirLigneSim2():void
		{
			serv.associerFournirLigneSim2(setObj());
		}

		private function associerFournirLigneSim3():void
		{
			serv.associerFournirLigneSim3(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=liste.getSelection().ID
			obj.ITEM1=liste.getSelection().NUM_SIM;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			obj.ID_TERMINAL=0;
			obj.ID_SIM=liste.getSelection().IDEQUIPEMENT;
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}