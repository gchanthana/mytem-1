package gestionparc.ihm.question.collaborateur
{
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.services.associer.ActionAssocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheFournirEqp
	{
		private var obj_listeDispo:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionAssocierServices=new ActionAssocierServices();

		public function QuestionFicheFournirEqp(obj_dispo:IListeDispo, check:Boolean, date:Date,typeEqp : Number)
		{
			this.obj_listeDispo=obj_dispo
			this.check=check;
			this.date=date;
			
			// DANS LE CAS MOBILE
			if (typeEqp==1)
			{
				// SI LE TERMINAL A UNE SIM
				if (obj_dispo.getSelection() && obj_dispo.getSelection().hasOwnProperty("IDSIM") && obj_dispo.getSelection().IDSIM != null && obj_dispo.getSelection().IDSIM != "")
				{
					Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEFOURNIREQP"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL | Alert.CANCEL, Application.application as Sprite, qnCloseHandler)
				}
				// SI LE TERMINAL N'A PAS DE SIM
				else
				{
					if (SpecificationVO.getInstance().elementDataGridSelected.IDSIM > 0) // SI LE COLLAB A UNE SIM
					{
						associerFournirEqp1();
					}
					else
						associerFournirEqp3();// SI LE COLLAB N'A PAS DE SIM
				}
			}
			else if (typeEqp==2)
			{
				if (obj_dispo.getSelection() && obj_dispo.getSelection().hasOwnProperty("LIGNE") && obj_dispo.getSelection().LIGNE != null && obj_dispo.getSelection().LIGNE != "")
				{
					Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEFOURNIREQP"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnCloseHandler)
				}
				else
				{
					associerFournirEqp3();
				}
			}
		}

		private function qnCloseHandler(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
			{
				associerFournirEqp1();
			}
			else if (evt.detail == Alert.CANCEL)
			{

			}
			else
			{
				associerFournirEqp2();
			}
		}

		private function associerFournirEqp1():void
		{
			serv.associerFournirEqp1(setObj());
		}

		private function associerFournirEqp2():void
		{
			serv.associerFournirEqp2(setObj());
		}

		private function associerFournirEqp3():void
		{
			serv.associerFournirEqp3(setObj());
		}

		private function associerFournirEqp4():void
		{
			serv.associerFournirEqp4(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=obj_listeDispo.getSelection().ID
			obj.ITEM1=obj_listeDispo.getSelection().IMEI;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			obj.ID_TERMINAL=obj_listeDispo.getSelection().IDTERM;
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}
