package gestionparc.ihm.question.collaborateur
{
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.services.associer.ActionAssocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheFournirLigne
	{
		private var obj_listeDispo:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionAssocierServices=new ActionAssocierServices();

		public function QuestionFicheFournirLigne(obj_dispo:IListeDispo, check:Boolean, date:Date)
		{
			this.obj_listeDispo=obj_dispo;
			this.check=check;
			this.date=date;

			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			// le collab à un terminal
			// on veut lui associer une ligne sans terminal
			if (ligne && ligne.IDTERMINAL > 0 && obj_listeDispo.getSelection() != null && obj_listeDispo.getSelection().hasOwnProperty("IDEQ") && !(obj_listeDispo.getSelection().IDEQ > 0))
			{
				Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEFOURNIRLIGNE"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnCloseHandler)
			}
			else
				// le collab à un terminal
				// on veut lui associer une ligne + terminal
				if (ligne && ligne.IDTERMINAL > 0 && obj_listeDispo.getSelection() != null && obj_listeDispo.getSelection().hasOwnProperty("IDEQ") && (obj_listeDispo.getSelection().IDEQ > 0))
				{
					associerCollabToLigne2();
				}
				else
					//le collab n'a pas de terminal,
					// on veut lui associer une ligne + terminal
					if (ligne && !(ligne.IDTERMINAL > 0) && obj_listeDispo.getSelection() != null && obj_listeDispo.getSelection().hasOwnProperty("IDEQ") && (obj_listeDispo.getSelection().IDEQ > 0))
					{
						associerFournirLigne2();
					}
					else
						//le collab n'a pas de terminal,
						// on veut lui associer une ligne sans terminal
					{
						associerFournirLigne3();
					}
		}

		private function qnCloseHandler(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
			{
				associerFournirLigne1();
			}
			else if (evt.detail == Alert.CANCEL)
			{

			}
			else
			{
				associerFournirLigne2();
			}
		}

		private function associerFournirLigne1():void
		{
			serv.associerFournirLigne1(setObj());
		}

		private function associerFournirLigne2():void
		{
			serv.associerFournirLigne2(setObj());
		}

		private function associerFournirLigne3():void
		{
			serv.associerFournirLigne3(setObj());
		}

		private function associerFournirEqp1():void
		{
			serv.associerFournirEqp1(setObj());
		}

		//Exception on associe le collab venant de la vue à la ligne + equipement venant de la liste
		//la source devient la cible  cad on prend ID venant de la liste
		//Historique Ko		
		private function associerCollabToLigne2():void
		{
			var obj:Object=new Object();
			obj.idorigine=obj_listeDispo.getSelection().ID;
			obj.iddestination=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.ITEM1=obj_listeDispo.getSelection().LIGNE;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			obj.ID_TERMINAL=0;
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=obj_listeDispo.getSelection().IDSOUS_TETE;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;

			serv.associerCollabToLigne2(obj);

		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=obj_listeDispo.getSelection().ID
			obj.ITEM1=obj_listeDispo.getSelection().LIGNE;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			obj.ID_TERMINAL=0;
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=obj_listeDispo.getSelection().IDSOUS_TETE;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}
