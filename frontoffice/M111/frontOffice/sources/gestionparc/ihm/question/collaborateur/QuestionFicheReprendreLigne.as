package gestionparc.ihm.question.collaborateur
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.services.dissocier.ActionDissocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheReprendreLigne
	{
		public var serv:ActionDissocierServices=new ActionDissocierServices();
		private var liste:IListeDispo;
		private var check:Boolean;
		private var date:Date;

		public function QuestionFicheReprendreLigne(obj_dispo:IListeDispo, check:Boolean, date:Date)
		{
			liste=obj_dispo;
			this.check=check;
			this.date=date;

			if (obj_dispo.getSelection().hasOwnProperty("IDEQUIP") && obj_dispo.getSelection().IDEQUIP != null && obj_dispo.getSelection().IDEQUIP > -1)
			{
				Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEREPRENDRELIGNE"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, closeHandler)
			}
			else
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'dissocier_collaborateur_ligne_'), ResourceManager.getInstance().getString('M111', 'Dissocier_collaborateur_de_la_ligne'), alertClickHandler);
			}
		}

		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				reprendreLigneFromCollab3();
			}
		}
		
		private function closeHandler(ce:CloseEvent):void
		{
			if (ce.detail == Alert.YES)
			{
				reprendreLigneFromCollab1()
			}
			else if (ce.detail == Alert.CANCEL)
			{

			}
			else
			{
				reprendreLigneFromCollab2()
			}
		}

		private function reprendreLigneFromCollab1():void
		{
			serv.reprendreLigneFromCollab1(setObj());
		}

		private function reprendreLigneFromCollab2():void
		{
			serv.reprendreLigneFromCollab2(setObj());
		}

		private function reprendreLigneFromCollab3():void
		{
			serv.reprendreLigneFromCollab3(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.iddestination=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.ITEM1=liste.getSelection().LIGNE;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			
			(liste.getSelection().IDEQUIP != null) ? obj.ID_TERMINAL=liste.getSelection().IDEQUIP : obj.ID_TERMINAL=0;
			
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=liste.getSelection().IDSOUS_TETE;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}
