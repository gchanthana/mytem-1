package gestionparc.ihm.fiches.equipement
{
	import flash.events.MouseEvent;

	import gestionparc.ihm.fiches.listedispo.DispoSimMobile;
	import gestionparc.services.associer.ActionAssocierServices;

	import mx.containers.TitleWindow;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;

	public class FicheAssocierLigneSimToEqpImpl extends TitleWindow
	{
		[Bindable]
		public var liste:DispoSimMobile;
		public var labDgSelection:Label;

		private var serv:ActionAssocierServices=new ActionAssocierServices();

		public function FicheAssocierLigneSimToEqpImpl()
		{
			super();
			liste.addEventListener(MouseEvent.CLICK, listeClickHandler);
		}

		protected function btnValiderClickHandler():void
		{
			associerLigneSimToEqp()
		}

		protected function listeClickHandler(e:MouseEvent):void
		{
			if (liste.getIDSelection() > -1)
			{
				labDgSelection.text=liste.getLibelleSelection();
			}
		}

		protected function close(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		/* APPEL BACKOFFICE */
		private function associerLigneSimToEqp():void
		{
			var SendObj:Object=new Object();
//			serv.associerLigneSimToEqp(SendObj);
		}
	}
}
