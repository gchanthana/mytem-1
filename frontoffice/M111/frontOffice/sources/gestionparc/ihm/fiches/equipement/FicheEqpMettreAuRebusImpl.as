package gestionparc.ihm.fiches.equipement
{
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.equipement.EquipementServices;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DateField;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.TextArea;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class FicheEqpMettreAuRebusImpl extends TitleWindow
	{
		public var imgTitre:Image;
		public var libelle_terminal_rebus:Label;
		public var calendar:CvDateChooser;
		public var bt_annuler:Button;
		public var bt_valider:Button;
		public var inputComm:mx.controls.TextArea;
		public var radio_perte:RadioButton;
		public var radio_casse:RadioButton;
		public var radio_vol:RadioButton;
		public var radio_autre:RadioButton;

		public var ligneSelected:AbstractMatriceParcVO;

		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")]
		private var adrImg:Class;
		private var serv:EquipementServices;

		public function FicheEqpMettreAuRebusImpl()
		{
			super();
			this.ligneSelected=ligneSelected;
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		private function initIHM(evt:FlexEvent):void
		{
			imgTitre.source=adrImg;
			libelle_terminal_rebus.text=ligneSelected.T_IMEI;
			addEventListener(CloseEvent.CLOSE, fermer);
			calendar.selectedDate=new Date();
			bt_annuler.addEventListener(MouseEvent.CLICK, fermer);
			bt_valider.addEventListener(MouseEvent.CLICK, rebusTerminal);
		}

		private function fermer(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function rebusTerminal(evt:MouseEvent):void
		{
			serv=new EquipementServices();
			serv.myDatas.addEventListener(gestionparcEvent.MISE_AU_REBUT_VALIDATE, rebusTerminal_handler);
			serv.mettreAuRebut(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idTerm=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.commentaire=inputComm.text;
			obj.id_cause=setCause();
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.T_IMEI;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			obj.ID_TERMINAL=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(calendar.selectedDate, 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}

		private function setCause():Number
		{
			if (radio_perte.selected == true)
				return 1;
			else if (radio_casse.selected == true)
				return 2;
			else if (radio_vol.selected == true)
				return 3;
			else if (radio_autre.selected == true)
				return 0;
			else
				return 0;
		}

		private function rebusTerminal_handler(evt:gestionparcEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Mise_au_rebus_du_terminal_effectu_e'), Application.application as DisplayObject);
			fermer(null);
		}
	}
}
