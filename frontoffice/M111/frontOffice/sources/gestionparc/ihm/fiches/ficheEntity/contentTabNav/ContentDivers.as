package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import mx.containers.FormItem;
	import mx.containers.VBox;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;

	public class ContentDivers extends VBox
	{
		public var ti_chp1:TextInput;
		public var ti_chp2:TextInput;
		public var ti_chp3:TextInput;
		public var ti_chp4:TextInput;
		public var fi1:FormItem;
		public var fi2:FormItem;
		public var fi3:FormItem;
		public var fi4:FormItem;

		public function ContentDivers()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
		}

		public function set champ1(value:String):void
		{
			ti_chp1.text=value;
		}

		public function set champ2(value:String):void
		{
			ti_chp2.text=value;
		}

		public function set champ3(value:String):void
		{
			ti_chp3.text=value;
		}

		public function set champ4(value:String):void
		{
			ti_chp4.text=value;
		}

		public function get champ1():String
		{
			return ti_chp1.text;
		}

		public function get champ2():String
		{
			return ti_chp2.text;
		}

		public function get champ3():String
		{
			return ti_chp3.text;
		}

		public function get champ4():String
		{
			return ti_chp4.text;
		}
	}
}
