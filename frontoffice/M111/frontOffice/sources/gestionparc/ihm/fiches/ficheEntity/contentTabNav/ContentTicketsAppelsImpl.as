package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	import composants.util.paginateDatagridExtend.PaginateDatagridExtend;
	import composants.util.searchPaginateDatagrid.CriteresRechercheVO;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.FicheEvent;
	import gestionparc.services.export.ExportFicheTicket;
	import gestionparc.services.ticketsappels.TicketsAppelsServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.NumberBaseRoundType;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import paginatedatagrid.event.PaginateDatagridEvent;
	
	import searchpaginatedatagrid.SearchPaginateDatagrid;
	import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;
	
	[Bindable]
	public class ContentTicketsAppelsImpl extends VBox
	{
		
		
		private var _idSous_tete:Number;
		private var _ligne:String;
		private var _facture:Number;
		private var _numero:String;
		private var boolLoaded:Boolean = false;
		
		public var arrayUnite:ArrayCollection = new ArrayCollection();
		public var params:CriteresRechercheVO = new CriteresRechercheVO();
		public var myPaginateDatagrid:PaginateDatagridExtend;
		public var mySearchPaginateDatagrid:SearchPaginateDatagrid;
		public var cbChooseFacture:ComboBox;
		public var nbTotalItem:int;
		public var ficheTicket:ArrayCollection = new ArrayCollection();
		public var listeFacture:ArrayCollection = new ArrayCollection();
		public var listeNumFacture:ArrayCollection = new ArrayCollection();
		public var currencySymbol:String;
		public var _service:TicketsAppelsServices = new TicketsAppelsServices();
		private var serviceExport:ExportFicheTicket = new ExportFicheTicket();
		public var checkFiltre:HBox;
		public var myFooterGrid:DataGrid;
		public var imgHelpNoData:Image;
		
		
		public function ContentTicketsAppelsImpl()
		{
			_service.callSymbol();
		}
		
		public function get numero():String
		{
			return _numero;
		}
		
		public function set numero(value:String):void
		{
			_numero = value;
		}
		
		public function get idSous_tete():Number
		{
			return _idSous_tete;
		}
		
		public function set idSous_tete(value:Number):void
		{
			_idSous_tete = value;
		}
		
		public function get facture():Number
		{
			return _facture;
		}
		
		public function set facture(value:Number):void
		{
			_facture = value;
		}
		
		public function get ligne():String
		{
			return _ligne;
		}
		
		public function set ligne(value:String):void
		{
			_ligne = value;
		}
		
		public function close(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		public function onCreationComplete(event:FlexEvent):void
		{
			if(boolLoaded == false)
			{
				params.limit = 12;
				idSous_tete = SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE;
				ligne = SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE;
				_service.getListeFactures(idSous_tete);
				_service.datas.addEventListener(FicheEvent.FICHETICKET,ficheTicketHandler);
				_service.datas.addEventListener(FicheEvent.LISTEFACTURES, listeFacturesHandler);
				boolLoaded = true;
			}
		}
		
		private function ficheTicketHandler(ev:FicheEvent):void
		{
			ficheTicket = _service.datas.ficheTicket;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
			nbTotalItem = Number(_service.datas.nbTotalItem);
			initPaginateDatagrid();
			getTotal();
			getUnite();
			myPaginateDatagrid.dataprovider.filterFunction = filtreGrid;
			myPaginateDatagrid.dataprovider.refresh();
		}
		
		private function listeFacturesHandler(ev:FicheEvent):void
		{
			listeFacture = _service.datas.listeFacture;
			var i:int = 0;
			if (listeFacture.length > 0)
			{
				while(i < listeFacture.length)
				{
					var o:Object = new Object();
					o.data = i;
					o.label = listeFacture[i].NUMERO_FACTURE + " - " + DateFunction.formatDateAsString(listeFacture[i].DATE_EMISSION) + " - " + listeFacture[i].NOM_OPE;
					listeNumFacture.addItem(o);
					i++;
				}
				facture = listeFacture[0].IDINVENTAIRE_PERIODE;
				_service.getFicheTicket(idSous_tete,facture,params);
			}
			else
			{
				var ob:Object = new Object();
				ob.data = 0;
				ob.label = resourceManager.getString('M111', 'Pas_de_donn_es');
				listeNumFacture.addItem(ob);
				imgHelpNoData.includeInLayout = true;
				imgHelpNoData.visible = true;
			}
		}
		
		public function callServiceTickets(e:Event):void
		{
			facture = listeFacture[cbChooseFacture.selectedItem.data].IDINVENTAIRE_PERIODE;
			_service.getFicheTicket(idSous_tete,facture,params);
		}
		
		protected function initPaginateDatagrid():void
		{
			if(myPaginateDatagrid!=null){
				myPaginateDatagrid.paginationBox.initPagination(nbTotalItem,params.limit);
				myPaginateDatagrid.nbTotalItem = nbTotalItem;
				myPaginateDatagrid.dgPaginate.headerHeight=40;
				myPaginateDatagrid.dgPaginate.wordWrap=true;
				myPaginateDatagrid.filtre = filtreGrid;
			}
		}
		
		
		private function getTotal():void
		{
			if(ficheTicket.length>0){
				myFooterGrid.columns[1].headerText = ConsoviewFormatter.formatNumber(ficheTicket.getItemAt(0).DUREE_FACTURE_TOTAL, 0);
				myFooterGrid.columns[3].headerText = formatNumberCurrency(ficheTicket.getItemAt(0).COUT_OP_TOTAL, 2);
				myFooterGrid.columns[4].headerText = formatNumberCurrency(ficheTicket.getItemAt(0).MONTANT_REMISE_TOTAL, 2);
				myFooterGrid.columns[5].headerText = formatNumberCurrency(ficheTicket.getItemAt(0).COUT_OP_REMISE_TOTAL, 2);
			}
		}
		
		private function getUnite():void
		{
			
			var obj:Object;
			var chek:CheckBox;
			
			if(ficheTicket.length>0){
				for(var i:int=0;i<ficheTicket.length;i++)
				{
					if(!isInArray(ficheTicket.getItemAt(i).UNITE_VOLUME,arrayUnite))
					{
						obj = new Object();
						obj.unite = ficheTicket.getItemAt(i).UNITE_VOLUME;
						obj.selected = true;
						arrayUnite.addItem(obj);
						
						chek = new CheckBox();
						chek.label = arrayUnite.getItemAt(arrayUnite.length-1).unite;
						chek.selected = true;
						chek.addEventListener(MouseEvent.CLICK,filtreChekHandler);
						checkFiltre.addChild(chek);
					}
				}
			}
			
		}
		
		private function filtreChekHandler(e:Event):void
		{
			actualiseUnite(e);
			myPaginateDatagrid.dataprovider.filterFunction = filtreGrid;
			myPaginateDatagrid.dataprovider.refresh();
		}
		
		protected function filtreGrid(ticket:Object):Boolean
		{
			var result:Boolean = filtreUnite(ticket) && filtreText(ticket);
			
			return result;
		}
		
		private function filtreUnite(ticket:Object):Boolean
		{
			
			var verif:Boolean=false;
			
			for(var i:int=0;i<arrayUnite.length;i++)
			{
				if((arrayUnite.getItemAt(i).selected)&&(ticket.UNITE_VOLUME==arrayUnite.getItemAt(i).unite))
				{
					verif=true;
				}
			}
			
			return verif;
			
		}
		
		/**
		 * appelée par filterFunction lorsque txtFiltre change
		 * Cette fonction recherche dans toutes les colonnes d'un dg qui ont un dataField != null le texte entré dans txtFiltre
		 * @param : item est un objet du dataprovider.
		 *
		 * */
		private function filtreText(item:Object):Boolean
		{
			for (var i:int = 0; i < myPaginateDatagrid.columns.length; i++)
			{
				if (myPaginateDatagrid.columns[i].dataField != null)
				{
					var propriete:String = (myPaginateDatagrid.columns[i] as DataGridColumn).dataField;
					if (item[propriete])
					{
						if (((item[propriete].toString()).toLowerCase()).search(myPaginateDatagrid.txtFiltre.text.toLowerCase()) != -1)
						{
							return true;
						}
					}
				}
			}
			return false;
		}
		private function actualiseUnite(event:Event):void
		{
			for(var i:int=0;i<arrayUnite.length;i++)
			{
				if(arrayUnite.getItemAt(i).unite==(event.currentTarget as CheckBox).label)
				{
					arrayUnite.getItemAt(i).selected = (event.currentTarget as CheckBox).selected;
				}
			}
		}
		
		private function isInArray(value:String,array:ArrayCollection):Boolean
		{
			
			var verif:Boolean=false;
			
			if(array.length!=0){
				for(var i:int=0;i<array.length;i++)
				{
					if(array.getItemAt(i).unite==value)
					{
						verif=true;
					}
				}
			}
			
			return verif;
			
		}
		
		protected function search(event:SearchPaginateDatagridEvent):void
		{
			
			var searchable:String = mySearchPaginateDatagrid.parametresRechercheVO.SEARCH_TEXT;
			var orderable:String = mySearchPaginateDatagrid.parametresRechercheVO.ORDER_BY;
			
			if (!searchable)
			{
				searchable=",";
			}
			
			if (!orderable)
			{
				orderable="cout op,DESC";
			}
			
			var ar1:Array = searchable.split(",");
			var ar2:Array = orderable.split(",");
			
			params.searchColonne = ar1[0];
			params.searchText = ar1[1];
			
			params.orderColonne = ar2[0];
			params.orderBy = ar2[1];
			
			params.offset=1;
			
			_service.getFicheTicket(idSous_tete,facture,params);
			
		}
		
		protected function exportDetailAppel():void
		{
			if(nbTotalItem!=0){
				serviceExport.getExportDetailAppel(idSous_tete,numero,facture,params,nbTotalItem);
			}
		}
		
		protected function onIntervalleChangeHandler(evt:PaginateDatagridEvent):void
		{
			
			if (myPaginateDatagrid.currentIntervalle.indexDepart == 0)
			{
				params.offset=1;
			}
			else
			{
				params.offset = myPaginateDatagrid.currentIntervalle.indexDepart;
			}
			
			_service.getFicheTicket(idSous_tete,facture,params);
			
		}
		
		/**
		 * Fonctions de formattage
		 * */
		protected function formateDate(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return DateFunction.formatDateAsString(item[column.dataField]);
			else
				return "-";
		}
		
		protected function formateNumber(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return ConsoviewFormatter.formatNumber(Number(item[column.dataField]), 2);
			else
				return "-";
		}
		
		protected function formateEuros(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return formatNumberCurrency(item[column.dataField], 2);
			else
				return "-";
		}
		
		private function formatNumberCurrency(num:Number, precision:int):String
		{
			var cf:CurrencyFormatter = new CurrencyFormatter();
			cf.precision = ResourceManager.getInstance().getString('ConsoModuleLib', 'CURRENCY_PRECISION');
			cf.rounding = NumberBaseRoundType.NONE;
			cf.decimalSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'DECIMAL_SEPARATOR');
			cf.thousandsSeparatorTo = ResourceManager.getInstance().getString('ConsoModuleLib', 'THOUSANDS_SEPARATOR');
			cf.useThousandsSeparator = true;
			cf.useNegativeSign = true;
			cf.alignSymbol = ResourceManager.getInstance().getString('ConsoModuleLib', 'ALIGN_SYMBOL');
			cf.currencySymbol = ' ' + _service.datas.currencySymbol;
			return cf.format(num);
		}
		
	}
}