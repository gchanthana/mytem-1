package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.ficheEntity.FicheAjoutLigneIHM;
	import gestionparc.ihm.fiches.popup.PopUpAddLineToGrpmentIHM;
	import gestionparc.services.ligne.LigneServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.core.IFlexDisplayObject;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de l'onglet Canaux/Groupement
	 * pour les lignes fixe de type T2, T0, ANA
	 * 
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 25.11.2010
	 * 
	 * </pre></p>
	 */
	[Bindable]	
	public class ContentCanauxGroupmentAjoutLigneImpl extends HBox
	{
		// VARIABLES------------------------------------------------------------------------------
		
		// IHM components
		public var tiCanauxMixteDebut 		: TextInput;
		public var tiCanauxMixteFin 		: TextInput;
		public var tiCanauxEntrantsDebut	: TextInput;
		public var tiCanauxEntrantsFin		: TextInput;
		public var tiCanauxSortantsDebut	: TextInput;
		public var tiCanauxSortantsFin		: TextInput;
		public var tiNbCanaux				: TextInput;

		public var lbNbLigneDsGrpment		: Label;
		
		public var rbLigneGrpmentLigneOK	: RadioButton;
		public var rbLigneGrpmentLigneKO	: RadioButton;
		
		public var cbxTeteLigne				: CheckBox;
		
		public var btAddLineToGrpment		: Button;
		
		public var dgLigneGrpement			: DataGrid;

		public var vbGrid					: VBox;
		
		// publics variables
		public var myServiceLigne			: LigneServices = null;
		
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe ContentCanauxGroupmentImpl.
		 * </pre></p>
		 */
		public function ContentCanauxGroupmentAjoutLigneImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		// PRIVATE FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation générale de cette classe  :
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see #initListener()
		 * @see #initDisplay()
		 */
		private function init(event:FlexEvent):void
		{
			initListener();
			initDisplay();
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise les écouteurs.
		 * </pre></p>
		 * 
		 * @return void
		 */	
		private function initListener():void
		{
			btAddLineToGrpment.addEventListener(MouseEvent.CLICK,onBtAddLineToGrpmentClickHandler);
			cbxTeteLigne.addEventListener(MouseEvent.CLICK,onCbxTeteLigneClickHandler);
			rbLigneGrpmentLigneOK.addEventListener(MouseEvent.CLICK,onRbLigneGrpmentLigneOKClickHandler);
			rbLigneGrpmentLigneKO.addEventListener(MouseEvent.CLICK,onRbLigneGrpmentLigneKOClickHandler);
			
			this.addEventListener(gestionparcEvent.SUPPRIMER_ELT_NAV,ligneDeleteHandler);
			this.addEventListener(gestionparcEvent.OPEN_POPUP_ACTION,onBtAddLineToGrpmentClickHandler);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise l'affichage de cette IHM.
		 * </pre></p>
		 * 
		 * @return void
		 */	
		private function initDisplay():void
		{
			rbLigneGrpmentLigneKO.selected = true;
			vbGrid.visible = false;
			vbGrid.includeInLayout = false;
			cbxTeteLigne.enabled = false;
			
			dgLigneGrpement.dataProvider = new ArrayCollection();
			
			// ajout de la tete de ligne dans le grid
			var obj : Object = new Object();
			obj.idTeteLigne = -1; 
			obj.numLigne = ""; 
			obj.isTete = 1;
			obj.typeLigne = ResourceManager.getInstance().getString('M111','T_te_de_ligne'); 
			
			(dgLigneGrpement.dataProvider as ArrayCollection).addItem(obj);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ecoute l'evement CLICK sur le bouton "btAddLineToGrpment" et affiche une popup permettant d'ajouter des lignes au groupement.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 */	
		private function onBtAddLineToGrpmentClickHandler(event:Event):void
		{
			if((dgLigneGrpement.dataProvider as ArrayCollection).getItemAt(0).numLigne.length > 0)
			{
				var popUpAddLineToGrpment : PopUpAddLineToGrpmentIHM = new PopUpAddLineToGrpmentIHM();
				popUpAddLineToGrpment.addEventListener(gestionparcEvent.LINE_GRPMENT_ADDED, onLineGrpmentAddedHandler);
				popUpAddLineToGrpment.myServiceLigne = myServiceLigne;
				// si on appelle depuis le bouton pour ajouter des lignes au groupement
				if(event.target.id == "btAddLineToGrpment")
				{
					popUpAddLineToGrpment.isTete = false;
					popUpAddLineToGrpment.lignesDuGrid = (dgLigneGrpement.dataProvider as ArrayCollection);
					showPopup(popUpAddLineToGrpment);
				}
				// si on appelle depuis le bouton éditer dans le grid
				//     	Si la case "tete de ligne"est coché, on ne fait rien. 
				//		Sinon on affiche la popup avec lal iste des lignes qui sont des têtes de ligne.
				else
				{
					if(!cbxTeteLigne.selected)
					{
						popUpAddLineToGrpment.isTete = true;
						popUpAddLineToGrpment.lignesDuGrid = (dgLigneGrpement.dataProvider as ArrayCollection);
						showPopup(popUpAddLineToGrpment);
					}
				}
				
			}
		}
		private function showPopup(ref:IFlexDisplayObject):void
		{
			PopUpManager.addPopUp(ref, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(ref);
		}
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ajoute une ligne au groupement.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 */
		private function onLineGrpmentAddedHandler(event:gestionparcEvent):void
		{
			if(event.obj.isTete)
			{
				var lenDataProviderDg : int = (dgLigneGrpement.dataProvider as ArrayCollection).length;

				for(var j : int = 0; j < lenDataProviderDg ; j++)
				{
					if((dgLigneGrpement.dataProvider as ArrayCollection)[j].isTete == 1)
					{
						(dgLigneGrpement.dataProvider as ArrayCollection)[j].idTeteLigne = event.obj.idTeteLigne; 
						(dgLigneGrpement.dataProvider as ArrayCollection)[j].numLigne = event.obj.numLigne;
						(dgLigneGrpement.dataProvider as ArrayCollection).refresh();
						break;
					}
				}
			}
			else
			{
				(dgLigneGrpement.dataProvider as ArrayCollection).addItem(event.obj);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Applique la suppression de la ligne sélectionnée.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 */	
		private function ligneDeleteHandler(event:gestionparcEvent):void
		{
			var lenDataProviderDg : int = (dgLigneGrpement.dataProvider as ArrayCollection).length;
			for(var j : int = 0; j < lenDataProviderDg ; j++)
			{
				if((dgLigneGrpement.dataProvider as ArrayCollection)[j].idTeteLigne == event.obj.idTeteLigne)
				{
					(dgLigneGrpement.dataProvider as ArrayCollection).removeItemAt(j);
					break;
				}
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Précise si la ligne sélectionnée ou la ligne en cours de création est la tête de ligne du groupement.
		 * - Si cette case est cochée, on considère que l'on créé un nouveau groupement de ligne. => 
		 * 		- désactivation du bouton définir, 
		 * 		- activation du bouton "ajouter une ligne au groupement"
		 * - Si cette case est décochée, on considère que l'on ajoute la ligne à un groupement existant => 
		 * 		- desactivation du bouton "ajouter une ligne au groupement", 
		 * 		- activation du bouton définir qui précise quelle ligne est la teête de ligne du groupement. 
		 * 		- On affiche l'ensemble des lignes enfants rattachées à cette tête de ligne. 
		 * 		- Une fois une tête de ligne sélectionnée, le bouton "ajouter une ligne augroupement" est enabled = true. 
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 */	
		private function onCbxTeteLigneClickHandler(event:MouseEvent):void
		{
			if(cbxTeteLigne.selected)
			{
				caseTeteLigneActivate()
			}
			else
			{
				caseTeteLigneDeActivate();
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ecoute l'evement CLICK sur le radioButton "rbLigneGrpmentLigneKO" et affiche ou non le datagrid lié au groupement.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 */	
		private function onRbLigneGrpmentLigneKOClickHandler(event:MouseEvent):void
		{
			if(vbGrid.visible)
			{
				vbGrid.visible = false;
				vbGrid.includeInLayout = false;
				cbxTeteLigne.enabled = false;
			}
		}
		private function majInfosLigneVo():void
		{
			(this.parent.parent as FicheAjoutLigneIHM).refreshInfoLigne(null);
		}
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ecoute l'evement CLICK sur le radioButton "rbLigneGrpmentLigneOK" et affiche ou non le datagrid lié au groupement.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 */	
		private function onRbLigneGrpmentLigneOKClickHandler(event:MouseEvent):void
		{
			majInfosLigneVo()
			if(myServiceLigne.myDatas.ligne.sousTete != null && myServiceLigne.myDatas.ligne.sousTete != "")
			{
				if(!vbGrid.visible)
				{
					vbGrid.visible = true;
					vbGrid.includeInLayout = true;
				}
				cbxTeteLigne.enabled = true;
				caseTeteLigneDeActivate()
			}
			else
			{
				if(vbGrid.visible)
				{
					vbGrid.visible = false;
					vbGrid.includeInLayout = false;
				}
				cbxTeteLigne.enabled = false;
				ConsoviewAlert.afficherError(resourceManager.getString('M111','Ce_champ_est_obligatoire')+' : '+resourceManager.getString('M111','N__de_ligne')
												,ResourceManager.getInstance().getString('M111', 'Attention__'));
				rbLigneGrpmentLigneKO.selected = true;
				rbLigneGrpmentLigneOK.selected = false;
			}
		}
		
		private function caseTeteLigneActivate():void
		{
			var lenDataProviderDg : int = (dgLigneGrpement.dataProvider as ArrayCollection).length;
			for(var j : int = 0; j < lenDataProviderDg ; j++)
			{
				if((dgLigneGrpement.dataProvider as ArrayCollection)[j].isTete == 1)
				{
					(dgLigneGrpement.dataProvider as ArrayCollection)[j].idTeteLigne = myServiceLigne.myDatas.ligne.idSousTete; 
					(dgLigneGrpement.dataProvider as ArrayCollection)[j].numLigne = myServiceLigne.myDatas.ligne.sousTete;
					(dgLigneGrpement.dataProvider as ArrayCollection).refresh();
					break;
				}
			}
			btAddLineToGrpment.enabled = true;
		}
		private function caseTeteLigneDeActivate():void
		{
			var lenDataProviderDg : int = (dgLigneGrpement.dataProvider as ArrayCollection).length;
			for(var i : int = 0 ; i < lenDataProviderDg ; i++)
			{
				if((dgLigneGrpement.dataProvider as ArrayCollection)[i].idTeteLigne == myServiceLigne.myDatas.ligne.idSousTete)
				{
					(dgLigneGrpement.dataProvider as ArrayCollection)[i].idTeteLigne = -1; 
					(dgLigneGrpement.dataProvider as ArrayCollection)[i].numLigne = "";
					(dgLigneGrpement.dataProvider as ArrayCollection).refresh();
					break;
				}
			}
		}
		public function changeSousTete():void
		{
			// - on mets à jour les infos de la ligne
			// - si la ligne fait partie d'un groupement
			//		- on vérifie si elle est tete de ligne ou juste une ligne du groupement
			//		- Si elle est une tete de ligne
			//			- caseTeteLigneActivate()
			//		- Sinon
			//			- caseTeteLigneDeActivate() 
			// - sinon rien
			
//			majInfosLigneVo();
			if(rbLigneGrpmentLigneOK.selected)
			{
				if(cbxTeteLigne.selected)
					caseTeteLigneActivate();
				else
					caseTeteLigneDeActivate();
			}
		}
		// PRIVATE FUNCTIONS-------------------------------------------------------------------------

	}
}