package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import composants.util.ConsoviewFormatter;
	
	import gestionparc.event.ZenPriseDataEvent;
	import gestionparc.services.mdm.MDMService;
	
	import mx.collections.GroupingCollection;
	import mx.containers.VBox;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;

	public class ContentShowDeviceImpl extends VBox
	{
		/* ------------------------- */
		/* -------- VARIABLE ------- */
		/* ------------------------- */
		private var _zenService:MDMService;

		[Bindable]
		public var gc:GroupingCollection;

		/* ---------------------- */
		/* -------- PUBLIC------- */
		/* ---------------------- */
		public function ContentShowDeviceImpl()
		{
			super();
			initListener(true);
		}


		[Bindable]
		public function get zenService():MDMService
		{
			return _zenService;
		}

		public function set zenService(value:MDMService):void
		{
			if (value && _zenService != value)
			{
				_zenService=value;
				_zenService.myDatas.addEventListener(ZenPriseDataEvent.ZPDE_DATA_COMPLETE, zpdeDataCompleteHandler);
			}


		}

		public function init():void
		{
		}

		/* -------------------------- */
		/* -------- PROTECTED ------- */
		/* -------------------------- */
		protected function dataGridStyleFunction(data:Object, column:AdvancedDataGridColumn):Object
		{
			var output:Object;

			if (data.children != null)
			{
				output={color: 0x0000, fontWeight: "bold", fontSize: 12}
			}

			return output;
		}

		protected function numberLabelFunction(item:Object, column:AdvancedDataGridColumn):String
		{
			var result:String="";

			if (item.hasOwnProperty(column.dataField) && item[column.dataField])
			{
				result=ConsoviewFormatter.formatNumber(Number(item[column.dataField]), 2);
			}


			return result;
		}

		/* ------------------------ */
		/* -------- PRIVATE ------- */
		/* ------------------------ */
		private function initListener(toInit:Boolean):void
		{

			if (toInit)
			{

			}
			else
			{

			}
		}

		private function zpdeDataCompleteHandler(event:ZenPriseDataEvent):void
		{
			//gc.refresh(true)
		}
	}
}
