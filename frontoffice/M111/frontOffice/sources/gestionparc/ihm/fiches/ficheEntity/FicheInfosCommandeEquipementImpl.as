package gestionparc.ihm.fiches.ficheEntity
{
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.CommandeVO;
	import gestionparc.entity.TerminalVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.commande.CommandeServices;
	import gestionparc.utils.gestionparcMessages;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.NumberValidator;
	import mx.validators.Validator;

	/**
	 * Classe concernant toutes les informations pour la saisi des informations
	 * d'une commande d'équipement.
	 */
	[Bindable]
	public class FicheInfosCommandeEquipementImpl extends TitleWindow
	{
		public var lbImei:Label;
		public var lbMarque:Label;
		public var lbModele:Label;
		public var lbCategorie:Label;
		public var lbType:Label;
		public var lbNiveau:Label;
		public var lbPool:Label;
		public var lbNumCommande:Label;
		public var lbDistributeur:Label;
		public var lbLibCopie:Label;
		public var lbRefCopie:Label;


		public var cbSiteLivraison:ComboBox;
		public var cbDureeGarantie:ComboBox;

		public var dcDateAchat:CvDateChooser;
		public var dcDateLivraison:CvDateChooser;
		public var dcDateDebut:CvDateChooser;

		public var tiRefOperateur:TextInput;
		public var tiLibelle:TextInput;
		public var tiRefClient:TextInput;
		public var tiRefLivraison:TextInput;

		public var tiPrix:TextInput;

		public var taCommentaires:TextArea;

		public var btValid:Button;
		public var btCancel:Button;

		public var valLibelle:Validator;
		public var valRefClient:Validator;
		public var valRefOperateur:Validator;
		public var valDateLivraison:Validator;
		public var valSiteLivraison:Validator;
		public var valDateDebut:Validator;
		public var valDateCommande:Validator;
		public var valDureeGarantie:Validator;

		public var prixValidator:NumberValidator;

		public var myEqpt:TerminalVO=null;
		public var myServiceCommande:CommandeServices=null;

		private var numCmdUnique:String="";

		/**
		 * Constructeur de la classe FicheDetailleeCollaborateurImpl_V2.
		 */

		public function FicheInfosCommandeEquipementImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Initialisation générale de cette classe  :
		 * - Instanciation du service lié aux collaborateurs.
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 */
		private function init(event:FlexEvent):void
		{
			myServiceCommande=new CommandeServices();
			myServiceCommande.genererNumeroDeCommande();

			initListener();
			initDisplay();
		}

		/**
		 * Initialise les écouteurs.
		 */
		private function initListener():void
		{
			// listeners lié aux boutons de FicheDetailleeNouvelleLigneIHM
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValid.addEventListener(MouseEvent.CLICK, validClickHandler);

			lbLibCopie.addEventListener(MouseEvent.CLICK, copieNumCommandeHandler);
			lbRefCopie.addEventListener(MouseEvent.CLICK, copieNumCommandeHandler);

			tiRefOperateur.addEventListener(Event.CHANGE, activeButtonValidHandler);
			tiRefClient.addEventListener(Event.CHANGE, activeButtonValidHandler);
			tiLibelle.addEventListener(Event.CHANGE, activeButtonValidHandler);
			cbSiteLivraison.addEventListener(ListEvent.CHANGE, activeButtonValidHandler);

			// listeners liés aux services
			myServiceCommande.myDatas.addEventListener(gestionparcEvent.NUM_CMD_GENERATED, numCmdGeneratedHandler);
		}

		/**
		 * Initialise l'affichage de cette IHM.
		 */
		private function initDisplay():void
		{
			// tant que tous les champs requit ne sont pas renseignés ou qu'aucune modif n'a été réalisée
			btValid.enabled=false;

			cbSiteLivraison.dataProvider=CacheDatas.siteLivraison;

			lbImei.text=myEqpt.numSerieEquipement;
			lbMarque.text=myEqpt.nomFabricant;
			lbModele.text=myEqpt.libelleEquipement;
			lbCategorie.text=myEqpt.categorie;
			lbType.text=myEqpt.typeEquipement;
			lbNiveau.text=(myEqpt.niveau != -1) ? myEqpt.niveau.toString() : "";

			lbPool.text=CacheDatas.libellePool;
			lbDistributeur.text=myEqpt.nomRevendeur;

			dcDateAchat.selectedDate=new Date();
			dcDateDebut.selectedDate=new Date();
			dcDateLivraison.selectedDate=new Date();
		}

		/**
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * Ferme la popup courante.
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * Active le bouton "Valider".
		 */
		private function activeButtonValidHandler(event:Event):void
		{
			if (tiRefOperateur.text && tiRefClient.text && tiLibelle.text && cbSiteLivraison.selectedItem && cbDureeGarantie.selectedItem && dcDateAchat.selectedDate && dcDateDebut.selectedDate && dcDateLivraison.selectedDate)
			{
				btValid.enabled=true;
			}
		}

		/**
		 * Valide la fiche collaborateur.
		 * Si c'est un nouveau collaborateur alors il en résulte sa création.
		 * Si le collaborateur existait auparavant, alors il en résulte de sa mise à jour.
		 */
		private function validClickHandler(event:MouseEvent):void
		{
			// test les validators
			if (runValidators())
			{
				setInfoCommandeVO();
				myServiceCommande.setInfosCommande(myServiceCommande.myDatas.commande, myEqpt);
				myServiceCommande.myDatas.addEventListener(gestionparcEvent.INFOS_CMD_SAVED, infosCmdSaved);
			}
		}

		private function setInfoCommandeVO():void
		{
			myServiceCommande.myDatas.commande=new CommandeVO();

			myServiceCommande.myDatas.commande.numCommande=lbNumCommande.text;
			myServiceCommande.myDatas.commande.refOperateur=tiRefOperateur.text;
			myServiceCommande.myDatas.commande.libelle=tiLibelle.text;
			myServiceCommande.myDatas.commande.refClient=tiRefClient.text;
			myServiceCommande.myDatas.commande.distributeur=lbDistributeur.text;
			myServiceCommande.myDatas.commande.prix=parseFloat(tiPrix.text); //tiPrix.prix
			myServiceCommande.myDatas.commande.dateAchat=(dcDateAchat.selectedDate) ? dcDateAchat.selectedDate : null;
			myServiceCommande.myDatas.commande.dateLivraison=(dcDateLivraison.selectedDate) ? dcDateLivraison.selectedDate : null;
			myServiceCommande.myDatas.commande.idSiteLivraison=(cbSiteLivraison.selectedItem) ? cbSiteLivraison.selectedItem.idSite : 0;
			myServiceCommande.myDatas.commande.refLivraison=tiRefLivraison.text;
			myServiceCommande.myDatas.commande.commentaires=taCommentaires.text;
			myServiceCommande.myDatas.commande.dateDebut=(dcDateDebut.selectedDate) ? dcDateDebut.selectedDate : null;
			myServiceCommande.myDatas.commande.duree=(cbDureeGarantie.selectedItem) ? cbDureeGarantie.selectedItem.DUREE : "";
		}

		/**
		 * Dispatch un évenement pour dire que les informations concernant la commande ont été sauvegardées.
		 */
		private function infosCmdSaved(event:gestionparcEvent):void
		{
			closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
			myServiceCommande.myDatas.removeEventListener(gestionparcEvent.INFOS_CMD_SAVED, infosCmdSaved);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.UPLOAD_INFOS_CMD, myServiceCommande.myDatas.commande));
		}

		/**
		 * Active le bouton "Valider".
		 */
		private function activeValidButton(event:Event):void
		{
			btValid.enabled=true;
		}

		/**
		 * Copie le numéro de commande sur le champ de l'IHM au dessus (=libellé ou ref client).
		 */
		private function copieNumCommandeHandler(evt:MouseEvent):void
		{
			switch (evt.target.id)
			{
				case "lbLibCopie":
				{
					tiLibelle.text=lbNumCommande.text;
					break;
				}
				case "lbRefCopie":
				{
					tiRefClient.text=lbNumCommande.text;
					break;
				}
			}
		}

		/**
		 * Recupère l'événement signifiant que le numéro de commande unique vient bien d'être généré.
		 */
		private function numCmdGeneratedHandler(evt:gestionparcEvent):void
		{
			numCmdUnique=(evt.obj as String);
			lbNumCommande.text=numCmdUnique;
		}

		/**
		 * Teste les validateurs et permet ainsi la cohérence des valeurs attendues.
		 */
		private function runValidators():Boolean
		{
			var validators:Array=[valDateCommande, valDateDebut, valDateLivraison, valDureeGarantie, valLibelle, valRefClient, valRefOperateur, valSiteLivraison];
			var results:Array=Validator.validateAll(validators);

			var validatorPrix:Array=[prixValidator];
			var resultsPrix:Array=Validator.validateAll(validatorPrix);

			if (results.length > 0)
			{
				ConsoviewAlert.afficherAlertInfo(gestionparcMessages.RENSEIGNER_CHAMPS_OBLI, ResourceManager.getInstance().getString('M111', 'Attention__'),null);
				return false;
			}
			else
			{
				if (tiPrix.text != "")
				{
					if (resultsPrix.length > 0)
					{
						ConsoviewAlert.afficherAlertInfo(gestionparcMessages.SYNTAXE_INVALIDE, ResourceManager.getInstance().getString('M111', 'Attention__'),null);
						return false;
					}
				}
				return true;
			}
		}
	}
}
