package gestionparc.ihm.fiches.ficheEntity
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.organisation.OrganisationServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	/**
	 * Classe concernant toutes les informations de la fiche organisation.
	 */
	[Bindable]
	public class FicheOrgaImpl extends TitleWindow
	{

		public var tiFiltre:TextInput;

		public var dgListTarget:DataGrid;

		public var dgcHeaderToChange:DataGridColumn;

		public var lbNodeSelected:Label;
		public var orgaSelectedNbNode:Label;

		public var btValid:Button;
		public var btCancel:Button;

		public var libelleOrga:String="";
		public var idOrga:int=0;
		public var myService:OrganisationServices=null;

		private var nbNode:int=0;

		public function FicheOrgaImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Initialisation générale de cette classe :
		 * - Instanciation du service lié aux organisations.
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 */
		private function init(event:FlexEvent):void
		{
			myService=new OrganisationServices();
			myService.getNodesOrga(idOrga);

			initListener();
			initDisplay();
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise les écouteurs.
		 * </pre></p>
		 *
		 * @return void.
		 */
		private function initListener():void
		{
			// listeners liés à FicheDetailleeNouvelleLigneIHM
			this.addEventListener(CloseEvent.CLOSE, closePopup);
			btCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValid.addEventListener(MouseEvent.CLICK, validClickHandler);
			tiFiltre.addEventListener(Event.CHANGE, filtrerGird);
			dgListTarget.addEventListener(ListEvent.ITEM_CLICK, selectItemHandler);

			// listeners liés aux services
			myService.myDatas.addEventListener(gestionparcEvent.LISTED_NODES_ORGA_LOADED, updateGrid);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise l'affichage de cette IHM.
		 * </pre></p>
		 *
		 * @return void.
		 */
		private function initDisplay():void
		{
			orgaSelectedNbNode.text=ResourceManager.getInstance().getString('M111', 'L_organisation');
			orgaSelectedNbNode.text+=" " + libelleOrga + " ";
			orgaSelectedNbNode.text+=ResourceManager.getInstance().getString('M111', 'contient');
			orgaSelectedNbNode.text+=" " + nbNode + " ";
			orgaSelectedNbNode.text+=ResourceManager.getInstance().getString('M111', 'noeud_s_');
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup()</code> pour femrer la popup courante.
		 * </pre></p>
		 *
		 * @param event:<code>MouseEvent</code>.
		 * @return void.
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ferme la popup courante.
		 * </pre></p>
		 *
		 * @param event:<code>Event</code>.
		 * @return void.
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Affecte à la varible <code>cheminSelected</code> de <code>OrganisationDatas</code>
		 * la valeur et le nom du chemin sélectionné dans le DataGrid pour l'organisation
		 * précédemment sélectionnée dans la fiche Collaborateur.
		 * Dispatch de l'événement VALIDE_FICHE_ORGA, pour prévenir la fiche collaborateur qu'un chemin
		 * a été sélectionné pour cette organisation.
		 * </pre></p>
		 *
		 * @param event:<code>MouseEvent</code>.
		 * @return void.
		 */
		private function validClickHandler(event:MouseEvent):void
		{
			myService.myDatas.cheminSelected=new Object();
			myService.myDatas.cheminSelected.chemin=dgListTarget.selectedItem.chemin;
			myService.myDatas.cheminSelected.idFeuille=dgListTarget.selectedItem.idFeuille;
			myService.myDatas.cheminSelected.idOrga=idOrga;

			dispatchEvent(new gestionparcEvent(gestionparcEvent.VALIDE_FICHE_ORGA));
			closePopup(null);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Teste les validateurs et permet ainsi la cohérence des valeurs attendues.
		 * </pre></p>
		 *
		 * @param event:<code>Event</code>.
		 * @return void.
		 */
		private function activeValidButton(event:Event):void
		{
			btValid.enabled=true;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la fonction <code>filtrer()</code> qui filtra le DataGrid selon la valeur du champ tiFiltre et met à jour le DataGrid.
		 * </pre></p>
		 *
		 * @param event:<code>Event</code>.
		 * @return void.
		 */
		private function filtrerGird(event:Event):void
		{
			if (dgListTarget.dataProvider != null)
			{
				(dgListTarget.dataProvider as ArrayCollection).filterFunction=filtrer;
				(dgListTarget.dataProvider as ArrayCollection).refresh();
			}
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Filtre le DataGrid selon la valeur du champ tiFiltre.
		 * </pre></p>
		 *
		 * @param item:<code>Object</code>.
		 * @return Boolean.
		 */
		private function filtrer(item:Object):Boolean
		{
			if (String(item.chemin).toLowerCase().search(tiFiltre.text) != -1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Mise à jour du tableau comprennant les feuilles de l'organisation sélectionnée.
		 * </pre></p>
		 *
		 * @param event:<code>gestionparcEvent</code>.
		 * @return void.
		 */
		private function updateGrid(event:gestionparcEvent):void
		{
			dgListTarget.dataProvider=myService.myDatas.listeFeuilles;
			nbNode=myService.myDatas.listeFeuilles.length;
			dgcHeaderToChange.headerText=myService.myDatas.structureOrga;
			initDisplay();
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * La sélection d'un item met à jour le champ "noeud sélectionné" et active le bouton "Valider".
		 * </pre></p>
		 *
		 * @param event:<code>ListEvent</code>.
		 * @return void.
		 */
		private function selectItemHandler(event:ListEvent):void
		{
			lbNodeSelected.text=ResourceManager.getInstance().getString('M111', 'Noeud_s_lectionn___');
			lbNodeSelected.text+=dgListTarget.selectedItem.chemin;

			activeValidButton(null);
		}
	}
}
