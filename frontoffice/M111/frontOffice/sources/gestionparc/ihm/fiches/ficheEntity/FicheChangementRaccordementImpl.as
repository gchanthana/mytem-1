package gestionparc.ihm.fiches.ficheEntity
{
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.ligne.LigneServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de la fiche raccordement.
	 *
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 18.11.2010
	 *
	 * </pre></p>
	 */
	[Bindable]
	public class FicheChangementRaccordementImpl extends TitleWindow
	{
		// VARIABLES------------------------------------------------------------------------------

		// IHM components
		public var lbNbAboOpt:Label;

		public var cbAction:ComboBox;
		public var cbOperateur:ComboBox;
		public var cbTypeRaccordement:ComboBox;

		public var dgAboOpt:DataGrid;

		public var cvDateApplication:CvDateChooser;

		public var btColFacture:Button;
		public var btValid:Button;
		public var btCancel:Button;

		// publics variables 
		public var myServiceCache:CacheServices=CacheServices.getInstance();//=new CacheServices();
		public var myServiceLigne:LigneServices=null;

		// VARIABLES------------------------------------------------------------------------------

		// PUBLIC FUNCTIONS-------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe FicheChangementRaccordementImpl.
		 * </pre></p>
		 */
		public function FicheChangementRaccordementImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		// PUBLIC FUNCTIONS-------------------------------------------------------------------------

		// PRIVATE FUNCTIONS-------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation générale de cette classe  :
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 *
		 * @see #initListener()
		 * @see #initDisplay()
		 */
		private function init(event:FlexEvent):void
		{
			myServiceLigne.getInfosFicheLigne();
			myServiceLigne.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_LIGNE_LOADED, initData);
		}

		private function initData(evt:Event):void
		{
			myServiceLigne.myDatas.removeEventListener(gestionparcEvent.INFOS_FICHE_LIGNE_LOADED, initData);

			var objToGetService:Object=new Object();
			objToGetService.idTypeRaccordement=myServiceLigne.myDatas.ligne.idTypeRaccordement;

			myServiceLigne.getActionsPossiblesByEtat(objToGetService);

			// charge la liste des modes de raccordement
			if (CacheDatas.listModeRaccordement)
				onListeModeRaccorLoadedHandler(null);
			else
				myServiceCache.getListeModeRaccordement();

			// charge la liste des tous les operateurs
			if (CacheDatas.listTousOperateur)
				onListeTousOperateurLoadedHandler(null);
			else
				myServiceCache.getListeTousOperateurs();

			initListener();
			initDisplay();
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise les écouteurs.
		 * </pre></p>
		 *
		 * @return void
		 */
		private function initListener():void
		{
			// listeners lié aux boutons de FicheChangementRaccordementImpl
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btColFacture.addEventListener(MouseEvent.CLICK, onBtColFactureClickHandler);
			btCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValid.addEventListener(MouseEvent.CLICK, validClickHandler);
			this.addEventListener(gestionparcEvent.AFFICHER_HISTORIQUE, displayHistoriqueHandler);

//			cbAction.addEventListener(ListEvent.CHANGE,activeValidButton);
//			cbOperateur.addEventListener(ListEvent.CHANGE,activeValidButton);
//			cbTypeRaccordement.addEventListener(ListEvent.CHANGE,activeValidButton);
//			cvDateApplication.addEventListener(CalendarLayoutChangeEvent.CHANGE,activeValidButton);
//			dgAboOpt.addEventListener(CollectionEvent.COLLECTION_CHANGE,activeValidButton);

			// listeners lié aux services
			myServiceLigne.myDatas.addEventListener(gestionparcEvent.LISTE_ACT_RACCOR_LOADED, onListeActRaccorLoadedHandler);
			myServiceCache.myDatas.addEventListener(gestionparcEvent.LISTE_MODE_RACCOR_LOADED, onListeModeRaccorLoadedHandler);
			myServiceCache.myDatas.addEventListener(gestionparcEvent.LISTE_TOUS_OP_LOADED, onListeTousOperateurLoadedHandler);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise l'affichage de cette IHM.
		 * </pre></p>
		 *
		 * @return void
		 */
		private function initDisplay():void
		{
			// tant que tous les champs requit ne sont pas renseignés ou qu'aucune modif n'a été réalisée
			//btValid.enabled = false;

			cvDateApplication.selectedDate=new Date();

			dgAboOpt.dataProvider=new ArrayCollection();
			dgAboOpt.dataProvider=myServiceLigne.myDatas.ligne.listeAboOption;

			lbNbAboOpt.text=(dgAboOpt.dataProvider as ArrayCollection).length.toString();
			lbNbAboOpt.text+=ResourceManager.getInstance().getString('M111', '_abonnements_et_options');
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Active le bouton "Valider".
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function activeValidButton(event:Event):void
		{
			btValid.enabled=true;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 *
		 * @see #closePopup()
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ferme la popup courante.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Valide le raccordement de la ligne en appelant le service lié à cet effet.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function validClickHandler(event:MouseEvent):void
		{
			if (cbAction.selectedIndex < 0 || cbOperateur.selectedIndex < 0 || cbTypeRaccordement.selectedIndex < 0 || cvDateApplication.selectedDate == null)
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString("M111", "Vous_ne_pouvez_pas_valider_cette_fiche_tant_que_des_donn_es_sont_manquantes"), ResourceManager.getInstance().getString("M111", "Consoview"), null);
			}
			else
			{
				valideFicheRaccordement();
			}
		}

		private function valideFicheRaccordement():void
		{
			var objToGetService:Object=new Object();
			objToGetService.idSousTete=myServiceLigne.myDatas.ligne.idSousTete;
			objToGetService.idActionRaccordement=cbAction.selectedItem.IDRACCO_ACTION;
			objToGetService.historiqueActions=(myServiceLigne.myDatas.ligne.listeHistoRaccor != null && myServiceLigne.myDatas.ligne.listeHistoRaccor.length > 0 && myServiceLigne.myDatas.ligne.listeHistoRaccor[0].IDRACCO_HISTO_PREC != null) ? myServiceLigne.myDatas.ligne.listeHistoRaccor[0].IDRACCO_HISTO_PREC : -1;
			objToGetService.idTypeRaccordement=cbTypeRaccordement.selectedItem.IDTYPE_RACCORDEMENT;
			objToGetService.dateRaccordement=DateFunction.formatDateAsInverseString(cvDateApplication.selectedDate);
			objToGetService.idOperateur=cbOperateur.selectedItem.OPERATEURID;

			var arrayAEntrer:Array=new Array();
			var arrayASortir:Array=new Array();
			var lenDataProvider:int=(dgAboOpt.dataProvider as ArrayCollection).length;

			for (var i:int=0; i < lenDataProvider; i++)
			{
				if ((dgAboOpt.dataProvider as ArrayCollection)[i].aEntrer == 1)
				{
					arrayAEntrer.push((dgAboOpt.dataProvider as ArrayCollection)[i].idInventaireProduit);
				}
				else if ((dgAboOpt.dataProvider as ArrayCollection)[i].aSortir == 1)
				{
					arrayASortir.push((dgAboOpt.dataProvider as ArrayCollection)[i].idInventaireProduit);
				}
			}
			objToGetService.arrayASortir=arrayASortir;
			objToGetService.arrayAEntrer=arrayAEntrer;

			myServiceLigne.doActionRaccordement(objToGetService);
			myServiceLigne.myDatas.addEventListener(gestionparcEvent.RACCOR_LIGNE_UPLOADED, onRaccorLigneUploadedHandler);
		}


		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Dispatch un évenement pour dire que le raccordement a bien été effectué.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onRaccorLigneUploadedHandler(event:gestionparcEvent):void
		{
			closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);

			var objRacco:Object=new Object();
			objRacco.OPNOM=cbOperateur.selectedItem.NOM;
			objRacco.LIBELLE_ACTION=cbAction.selectedItem.LIBELLE_ACTION
			objRacco.DATE_EFFET=cvDateApplication.selectedDate;
			objRacco.LIBELLE_RACCORDEMENT=cbTypeRaccordement.selectedItem.LIBELLE_RACCORDEMENT
			objRacco.NOM_COMPLET=CvAccessManager.getUserObject().NOM + " " + CvAccessManager.getUserObject().PRENOM;

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.RACCOR_LIGNE_DID, objRacco));
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Met a jour la comboBox "cbTypeRaccordement" par rapport aux données récupérées dans le cache
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onListeModeRaccorLoadedHandler(event:gestionparcEvent):void
		{
			cbTypeRaccordement.dataProvider=new ArrayCollection();
			cbTypeRaccordement.dataProvider=CacheDatas.listModeRaccordement;
			cbTypeRaccordement.dropdown.dataProvider=CacheDatas.listModeRaccordement;

			for each (var obj:Object in cbTypeRaccordement.dataProvider)
			{
				if (myServiceLigne.myDatas.ligne.idTypeRaccordement <= 0 && obj.IDTYPE_RACCORDEMENT == 4)
				{
					cbTypeRaccordement.selectedItem=obj;
					break;
				}
				else if (myServiceLigne.myDatas.ligne.idTypeRaccordement > 0 && obj.IDTYPE_RACCORDEMENT == myServiceLigne.myDatas.ligne.idTypeRaccordement)
				{
					cbTypeRaccordement.selectedItem=obj;
					break;
				}
			}
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Met a jour la comboBox "cbOperateur" par rapport aux données récupérées dans le cache
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onListeTousOperateurLoadedHandler(event:gestionparcEvent):void
		{
			cbOperateur.dataProvider=new ArrayCollection();
			cbOperateur.dataProvider=CacheDatas.listTousOperateur;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appel le service permettant de recuperer les donnees de la colonne "dgcFacture"
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onBtColFactureClickHandler(event:MouseEvent):void
		{
			var objToGetService:Object=new Object();
			objToGetService.idSousTete=myServiceLigne.myDatas.ligne.idSousTete;
			myServiceLigne.majColFacture(objToGetService);
			myServiceLigne.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_LIGNE_LOADED, onInfosFicheLigneLoadedHandler)
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Met a jour le grid "dgAboOpt" en ajoutant les infos de la colonne "dgcFacture" par rapport aux données récupérées de la base
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onInfosFicheLigneLoadedHandler(event:gestionparcEvent):void
		{
			dgAboOpt.dataProvider=myServiceLigne.myDatas.ligne.listeAboOption;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Met a jour la comboBox "cbAction" par rapport aux données récupérées
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onListeActRaccorLoadedHandler(event:gestionparcEvent):void
		{
			cbAction.dataProvider=new ArrayCollection();
			cbAction.dataProvider=myServiceLigne.myDatas.listActionRaccordement;
			cbAction.dropdown.dataProvider=myServiceLigne.myDatas.listActionRaccordement;
			if ((cbAction.dataProvider as ArrayCollection).length == 0)
				btValid.enabled=false;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Affiche la pop up de l'historique du produit selectionné
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function displayHistoriqueHandler(event:gestionparcEvent):void
		{
			//var ficheHistorique : FicheHistoriqueIHM = new FicheHistoriqueIHM();

			//ficheHistorique.idRessource	= event.obj.idInventaireProduit;

//			PopUpManager.addPopUp(ficheHistorique,this,true);
//			PopUpManager.centerPopUp(ficheHistorique);
		}

		// PRIVATE FUNCTIONS-------------------------------------------------------------------------

	}
}
