package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import gestionparc.services.ligne.LigneServices;

	import mx.containers.HBox;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de l'onglet cablage/SDA
	 * pour les lignes fixe de type Point à Point
	 *
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 23.11.2010
	 *
	 * </pre></p>
	 */
	[Bindable]
	public class ContentCablageSdaTypePointaPointImpl extends HBox
	{
		public var tiElementActifEmissionA:TextInput;
		public var tiElementActifEmissionB:TextInput;
		public var tiElementActifReceptionA:TextInput;
		public var tiElementActifReceptionB:TextInput;
		public var tiAmorceTeteEmissionA:TextInput;
		public var tiAmorceTeteEmissionB:TextInput;
		public var tiAmorceTeteReceptionA:TextInput;
		public var tiAmorceTeteReceptionB:TextInput;
		public var tiAdresseA:TextInput;
		public var tiAdresseB:TextInput;
		public var tiCodePostalA:TextInput;
		public var tiCodePostalB:TextInput;
		public var tiCommuneA:TextInput;
		public var tiCommuneB:TextInput;
		public var tiIdentifiantLLA:TextInput;
		public var tiIdentifiantLLB:TextInput;
		public var tiNature:TextInput;
		public var tiDebit:TextInput;
		public var tiDistance:TextInput;
		public var tiElementActifA:TextInput;
		public var tiElementActifB:TextInput;

		public var myServiceLigne:LigneServices;

		public function ContentCablageSdaTypePointaPointImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(event:FlexEvent):void
		{
		}

		public function setInfos():void
		{
			tiElementActifEmissionA.text=(myServiceLigne.myDatas.ligne.PtAElemActifAmorceEmission) ? myServiceLigne.myDatas.ligne.PtAElemActifAmorceEmission.toString() : "";
			tiElementActifReceptionA.text=(myServiceLigne.myDatas.ligne.PtAElemActifAmorceReception) ? myServiceLigne.myDatas.ligne.PtAElemActifAmorceReception.toString() : "";
			tiAmorceTeteEmissionA.text=(myServiceLigne.myDatas.ligne.PtAAmorceTeteEmission) ? myServiceLigne.myDatas.ligne.PtAAmorceTeteEmission.toString() : "";
			tiAmorceTeteReceptionA.text=(myServiceLigne.myDatas.ligne.PtAAmorceTeteReception) ? myServiceLigne.myDatas.ligne.PtAAmorceTeteReception.toString() : "";
			tiAdresseA.text=(myServiceLigne.myDatas.ligne.PtAAdresse) ? myServiceLigne.myDatas.ligne.PtAAdresse.toString() : "";
			tiCodePostalA.text=(myServiceLigne.myDatas.ligne.PtACodePostal) ? myServiceLigne.myDatas.ligne.PtACodePostal.toString() : "";
			tiCommuneA.text=(myServiceLigne.myDatas.ligne.PtACommune) ? myServiceLigne.myDatas.ligne.PtACommune.toString() : "";
			tiElementActifA.text=(myServiceLigne.myDatas.ligne.PtAElemActif) ? myServiceLigne.myDatas.ligne.PtAElemActif.toString() : "";

			tiElementActifEmissionB.text=(myServiceLigne.myDatas.ligne.PtBElemActifAmorceEmission) ? myServiceLigne.myDatas.ligne.PtBElemActifAmorceEmission.toString() : "";
			tiElementActifReceptionB.text=(myServiceLigne.myDatas.ligne.PtBElemActifAmorceEmission) ? myServiceLigne.myDatas.ligne.PtBElemActifAmorceEmission.toString() : "";
			tiAmorceTeteEmissionB.text=(myServiceLigne.myDatas.ligne.PtBAmorceTeteEmission) ? myServiceLigne.myDatas.ligne.PtBAmorceTeteEmission.toString() : "";
			tiAmorceTeteReceptionB.text=(myServiceLigne.myDatas.ligne.PtBAmorceTeteReception) ? myServiceLigne.myDatas.ligne.PtBAmorceTeteReception.toString() : "";
			tiAdresseB.text=(myServiceLigne.myDatas.ligne.PtBAdresse) ? myServiceLigne.myDatas.ligne.PtBAdresse.toString() : "";
			tiCodePostalB.text=(myServiceLigne.myDatas.ligne.PtBCodePostal) ? myServiceLigne.myDatas.ligne.PtBCodePostal.toString() : "";
			tiCommuneB.text=(myServiceLigne.myDatas.ligne.PtBCommune) ? myServiceLigne.myDatas.ligne.PtBCommune.toString() : "";
			tiElementActifB.text=(myServiceLigne.myDatas.ligne.PtBElemActif) ? myServiceLigne.myDatas.ligne.PtBElemActif.toString() : "";

			tiIdentifiantLLA.text=(myServiceLigne.myDatas.ligne.PtAIdentifiantLL) ? myServiceLigne.myDatas.ligne.PtAIdentifiantLL.toString() : "";
			tiIdentifiantLLB.text=(myServiceLigne.myDatas.ligne.PtBIdentifiantLL) ? myServiceLigne.myDatas.ligne.PtBIdentifiantLL.toString() : "";
			tiNature.text=(myServiceLigne.myDatas.ligne.Nature) ? myServiceLigne.myDatas.ligne.Nature.toString() : "";
			tiDebit.text=(myServiceLigne.myDatas.ligne.Debit) ? myServiceLigne.myDatas.ligne.Debit.toString() : "";
			tiDistance.text=(myServiceLigne.myDatas.ligne.Distance) ? myServiceLigne.myDatas.ligne.Distance.toString() : "";
		}
	}
}
