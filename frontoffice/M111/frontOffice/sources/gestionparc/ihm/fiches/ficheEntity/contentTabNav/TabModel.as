package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import composants.util.datagrid.RowColorDataGrid;
	
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.renderer.commonrenderer.EditerItemRenderer;
	import gestionparc.ihm.renderer.commonrenderer.SupprimerItemRenderer;
	import gestionparc.ihm.renderer.commonrenderer.derniereFactureItemRenderer;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.Spacer;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.ClassFactory;
	import mx.events.ListEvent;
	import mx.resources.ResourceManager;

	[Bindable]
	public class TabModel extends VBox
	{
		private var hb_bottom:HBox=new HBox();
		private var bt_action:Button=new Button();
		private var lb_libelle:Label=new Label();
		private var dg_datagrid:RowColorDataGrid=new RowColorDataGrid();
		private var sp_top:Spacer=new Spacer();
		private var sp_bottom:Spacer=new Spacer();

		private var _dataDg:ArrayCollection=new ArrayCollection();
		private var _colonnes:ArrayCollection=new ArrayCollection();
		private var _colSupVisible:Boolean=false;

		private var nbElt:int;
		private var colEditer:DataGridColumn=new DataGridColumn();
		private var colDesaffecter:DataGridColumn=new DataGridColumn();

		public var hb_top:HBox=new HBox(); // public pour la gestion des abo et opt dans la fiche ligne
		public var hb_top1:HBox=new HBox();
		public var colSortirInventaire:DataGridColumn=new DataGridColumn(); // public pour la gestion des abo et opt dans la fiche ligne
		public var colEntrerInventaire:DataGridColumn=new DataGridColumn(); // public pour la gestion des abo et opt dans la fiche ligne
		private var colInventaire:DataGridColumn=new DataGridColumn();
		private var colDerniereFacture:DataGridColumn=new DataGridColumn();
		private var colHistorique:DataGridColumn=new DataGridColumn();

		private var _tabLength:int;
		private var fun:Function;
		private var _typeAjoutColonne:String="none";
		private var _selectedItem:Object;


		public function TabModel()
		{
			super();
			addTopContainer();
			addMiddleContainer();
			addBottomContainer();

			fun=function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint
			{
				var rColor:uint;
				rColor=color;
				var item:Object=datagrid.dataProvider.getItemAt(rowIndex);
				if (item != null && item.dansInventaire && item.boolInOrder)
				{
					if (item.dansInventaire == 0 && item.boolInOrder == 1)
						rColor=0xFEC46D;
					else
						rColor=color;

				}
				return rColor;
			}


		}

		private function addTopContainer():void
		{
			sp_top.percentWidth=100;
			hb_top.addChild(lb_libelle);
			hb_top.percentWidth=100;
			hb_top.addChild(sp_top);
			hb_top.addChild(bt_action);
			hb_top1.setStyle("horizontalAlign", "right");
			hb_top.setStyle("paddingTop","5");
			//hb_top1.addChild(sp_top);
			/*hb_top1.setStyle("borderStyle","solid");
			hb_top1.setStyle("borderColor","0xFEC46D");
			hb_top1.setStyle("paddingTop","5");
			hb_top1.setStyle("paddingBottom","5");
			hb_top1.setStyle("paddingLeft","5");
			hb_top1.setStyle("paddingRight","5");*/
			//hb_top1.styleName="gridBox";
			bt_action.addEventListener(MouseEvent.MOUSE_DOWN, clickBtAction);
			this.addChild(hb_top);
			this.addChild(hb_top1);
		}

		private function addMiddleContainer():void
		{
			dg_datagrid.percentWidth=100;
			dg_datagrid.addEventListener(ListEvent.CHANGE, _dg_datagridChangeHandler);
			this.addChild(dg_datagrid);
		}

		private function addBottomContainer():void
		{
			sp_bottom.percentWidth=100;
			hb_bottom.addChild(sp_bottom);
			hb_bottom.percentWidth=100;
			this.addChild(hb_bottom);
		}

		/**
		 *	(String) Fonction d'ajout des colonnes du datagrid avec personnalisation des dernières colonnes
		 **/
		public function addColonnes(value:String):void
		{
			var listColonnes:Array=new Array();

			for (var i:int=0; i < _colonnes.length; i++)
			{
				if (_colonnes[i] is DataGridColumn)
				{
					listColonnes.push(_colonnes[i]);
				}

			}

			switch (value)
			{
				case "collab":
					//ajouter 2 colonnes
					colEditer.itemRenderer=new ClassFactory(gestionparc.ihm.renderer.commonrenderer.EditerItemRenderer);
					colEditer.width=50;
					colEditer.sortable=false;
					listColonnes.push(colEditer);

					colDesaffecter.itemRenderer=new ClassFactory(gestionparc.ihm.renderer.commonrenderer.SupprimerItemRenderer);
					colDesaffecter.width=75;
					colDesaffecter.sortable=false;

					if (colSupVisible)
					{
						listColonnes.push(colDesaffecter);
					}
					break;
				case "ligne":
					//ajouter 5 colonnes
					colInventaire.itemRenderer=new ClassFactory(gestionparc.ihm.renderer.commonrenderer.CheckBoxInventaireItemRendererIHM);
					colInventaire.width=110;
					colInventaire.dataField="dansInventaire";
					colInventaire.sortable=false;

					colEntrerInventaire.itemRenderer=new ClassFactory(gestionparc.ihm.renderer.commonrenderer.CheckBoxInventaireItemRendererIHM);
					colEntrerInventaire.width=55;
					colEntrerInventaire.headerText=ResourceManager.getInstance().getString('M111', '__entrer');
					colEntrerInventaire.dataField="aEntrer";
					colEntrerInventaire.sortable=false;

					colSortirInventaire.itemRenderer=new ClassFactory(gestionparc.ihm.renderer.commonrenderer.CheckBoxInventaireItemRendererIHM);
					colSortirInventaire.width=55;
					colSortirInventaire.headerText=ResourceManager.getInstance().getString('M111', '__sortir');
					colSortirInventaire.dataField="aSortir";
					colSortirInventaire.sortable=false;

					colDerniereFacture.itemRenderer=new ClassFactory(gestionparc.ihm.renderer.commonrenderer.derniereFactureItemRenderer);
					colDerniereFacture.width=130;
					colDerniereFacture.headerText=ResourceManager.getInstance().getString('M111', '__derniere_fact');
					colDerniereFacture.dataField="boolFacture";
					colDerniereFacture.sortable=false;
					colHistorique.itemRenderer=new ClassFactory(gestionparc.ihm.renderer.commonrenderer.HistoriqueItemRenderer);
					colHistorique.width=70;
					colHistorique.sortable=false;

					listColonnes.push(colInventaire);
					listColonnes.push(colEntrerInventaire);
					listColonnes.push(colSortirInventaire);
					listColonnes.push(colHistorique);
					listColonnes.push(colDerniereFacture);
					dg_datagrid.rowColorFunction=fun;
					break;
				case "sim":
					//ajouter 1 colonnes
					colInventaire.itemRenderer=new ClassFactory(gestionparc.ihm.renderer.commonrenderer.CheckBoxInventaireItemRendererIHM);
					colHistorique.itemRenderer=new ClassFactory(gestionparc.ihm.renderer.commonrenderer.HistoriqueItemRenderer);

					colInventaire.dataField="dansInventaire";

					listColonnes.push(colInventaire);
					listColonnes.push(colHistorique);
					break;
				case "none":
					//ne pas ajouter de colonnes
					break;
			}

			dg_datagrid.columns=listColonnes;
		}

		private function clickBtAction(evt:MouseEvent):void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.AJOUTER_ELT_NAV, true));
		}

		protected function _dg_datagridChangeHandler(event:ListEvent):void
		{
			selectedItem=dg_datagrid.selectedItem;
		}

		public function actualizeDatagrid():void
		{
			_dataDg.refresh();
		}

		public function get tabLength():int
		{
			if (dg_datagrid.dataProvider != null)
				_tabLength=(dg_datagrid.dataProvider as ArrayCollection).length;
			return _tabLength;
		}

		public function get dataDatagrid():ArrayCollection
		{
			return _dataDg;
		}

		public function get colonnes():ArrayCollection
		{
			return _colonnes;
		}

		public function set headerLabel(value:String):void
		{
			lb_libelle.text=value;
		}

		public function set labelTypeOfEdit(value:String):void
		{
			colEditer.headerText=value;
		}

		public function set labelTypeOfDelete(value:String):void
		{
			colDesaffecter.headerText=value;
		}


		public function set labelInventaire(value:String):void
		{
			colInventaire.headerText=value;
		}

		public function set labelHistorique(value:String):void
		{
			colHistorique.headerText=value;
		}

		public function set labelDerniereFact(value:String):void
		{
			colDerniereFacture.headerText=value;
		}

		public function set buttonActionLabel(value:String):void
		{
			bt_action.label=value;
		}

		[Inspectable(defaultValue="true", enumeration="true,false", type="Boolean")]
		public function set buttonActionVisible(value:Boolean):void
		{
			bt_action.visible=value;
		}

		public function get buttonAction():Button
		{
			return bt_action;
		}

		public function set datagridHeight(value:Number):void
		{
			dg_datagrid.height=value;
		}

		public function set dataDatagrid(dg:ArrayCollection):void
		{
			dg_datagrid.dataProvider=dg;
			_dataDg=dg;
		}

		public function set colonnes(col:ArrayCollection):void
		{
			this._colonnes=col;
			addColonnes(_typeAjoutColonne);
		}

		[Inspectable(defaultValue="none", enumeration="none,collab,ligne,sim", type="String")]
		public function set typeAjoutColonnes(value:String):void
		{
			_typeAjoutColonne=value;
		}

		public function set visibleTab(value:Boolean):void
		{
			this.visible=value;
		}

		public function set selectedItem(value:Object):void
		{
			_selectedItem=value;
		}

		public function get selectedItem():Object
		{
			return _selectedItem;
		}

		public function get ligneDgSelected():Object
		{
			return dg_datagrid.selectedItem as Object;
		}

		public function set colSupVisible(value:Boolean):void
		{
			_colSupVisible=value;
		}

		public function get colSupVisible():Boolean
		{
			return _colSupVisible;
		}
	}
}