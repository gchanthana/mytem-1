package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import gestionparc.services.mdm.MDMService;
	
	import mx.containers.VBox;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.resources.ResourceManager;

	public class ContentShowSoftwareImpl extends VBox
	{
		/* ------------------------- */
		/* -------- VARIABLE ------- */
		/* ------------------------- */
		[Bindable]
		public var zenService:MDMService;

		/* ---------------------- */
		/* -------- PUBLIC------- */
		/* ---------------------- */
		public function ContentShowSoftwareImpl()
		{
			super();
			initListener(true);
		}

		public function init():void
		{
		}

		/* -------------------------- */
		/* -------- PROTECTED ------- */
		/* -------------------------- */
		protected function TailleLabelFunction(item:Object, column:DataGridColumn):String
		{
			var str:String="";
			var strSize:String=Math.round(item.SIZE / 1000000).toString() + ResourceManager.getInstance().getString('M111', '_Mo');
			return strSize;
		}

		/* ------------------------ */
		/* -------- PRIVATE ------- */
		/* ------------------------ */
		private function initListener(toInit:Boolean):void
		{
			if (toInit)
			{

			}
			else
			{

			}
		}
	}
}
