package gestionparc.ihm.fiches.ficheEntity
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.TabNavigator;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	import mx.validators.Validator;
	
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	
	import gestionparc.entity.CollaborateurVO;
	import gestionparc.entity.EquipementVO;
	import gestionparc.entity.Matricule;
	import gestionparc.entity.OrgaVO;
	import gestionparc.entity.SIMVO;
	import gestionparc.entity.SiteVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentDiversIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentSiteFicheEquipementIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.TabModelIHM;
	import gestionparc.ihm.fiches.popup.PopUpChoixManagerIHM;
	import gestionparc.ihm.fiches.rago.CreationRegles.ihm.PopUpNewOrigineOrCibleIHM;
	import gestionparc.ihm.fiches.rago.entity.Regle;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.collaborateur.CollaborateurServices;
	import gestionparc.services.organisation.OrganisationServices;
	import gestionparc.utils.TitledBorderBox;
	import gestionparc.utils.gestionparcMessages;

	/**
	 * Classe concernant toutes les informations de la fiche collaborateur.
	 */
	[Bindable]
	public class FicheCollaborateurImpl extends TitleWindowBounds
	{

		public var tiPrenom:TextInput;
		public var tiNom:TextInput;
		public var tiFonction:TextInput;
		public var tiEmail:TextInput;
		public var tiIdentifiant:TextInput;
		public var tiCodeInterne:TextInput;
		public var tiMatricule:TextInput;
		public var tiStatut:TextInput;
		public var tiReference:TextInput;
		public var lbl_manager:Label;
		public var lbl_MatriculeMngr:Label;

		public var ckxPoolDistributeur:CheckBox;
		public var ckxPoolSociete:CheckBox;

		public var lbPosition:Label;
		public var lbPositionOPE:Label;
		public var lbOrgaOPE:Label;

		public var taCommentaires:TextArea;

		public var vbInsertPool:VBox;

		public var valMatricule:Validator;
		public var valNom:Validator;

		public var dfInSociety:CvDateChooser;
		public var dfOutSociety:CvDateChooser;

		public var cbCivilite:ComboBox;
		public var cbDsSociete:ComboBox;
		public var cbOrganisation:ComboBox;
		public var cbNivCollab:ComboBox;
		
		public var boxPositionOrgaOPE:TitledBorderBox;
		public var boxPositionOrgaCUS:TitledBorderBox;

		public var tncEquipement:TabModelIHM;
		public var tncLigneSIM:TabModelIHM;

		public var tncSite:ContentSiteFicheEquipementIHM;
		public var tncDivers:ContentDiversIHM;

		public var tncSiteNew:ContentSiteFicheEquipementIHM;
		public var tncDiversNew:ContentDiversIHM;

		public var btnValid:Button;
		public var btnCancel:Button;
		public var btModifierOrga:Button;

		public var tnOldCollaborateur:TabNavigator;

		public var bxNewCollaborateur:Box;

		public var idEmploye:int=0;
		public var idRow:int=0;
		public var idGpeClient:int=0;
		public var isNewCollabFiche:Boolean=false;

		public var myServiceCollab:CollaborateurServices=null;
		public var myServiceOrga:OrganisationServices=null;

		private var ficheOrga:FicheOrgaIHM=null;
		private var pop:PopUpNewOrigineOrCibleIHM=null;

		public var listeSites:ArrayCollection=new ArrayCollection();

		public var _selectedRegle:Regle=new Regle();

		private var _selectedManager:CollaborateurVO;

		/**
		 * Constructeur de la classe FicheCollaborateurImpl.
		 */
		public function FicheCollaborateurImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation générale de cette classe  :
		 * - Instanciation du service lié aux collaborateurs.
		 * - Instanciation du service lié aux organisations si ce n'est pas un ajout de collaborateur.
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 *
		 * @see #initListener()
		 * @see #initDisplay()
		 */
		private function init(event:FlexEvent):void
		{
			myServiceCollab=new CollaborateurServices();

			if (!isNewCollabFiche)
			{
				initData();
			}
			else
			{
				myServiceOrga=new OrganisationServices();
				listeOragLoadedHandler(null);

//				tncSiteNew.cbSite.dataProvider = CacheDatas.siteLivraison;

				setSites(true);
			}
			

			matriculeAuto();
			initListener();
			initDisplay();
		}

		//ON CASSE LA REFERENCE!!
		private function setSites(isNew:Boolean):void
		{
			var sites:ArrayCollection=CacheDatas.siteLivraison;
			var len:int=sites.length;

			listeSites=new ArrayCollection();

			for (var i:int=0; i < len; i++)
			{
				listeSites.addItem(sites[i]);
			}

			if (isNew)
			{
				tncSiteNew.cbSite.dataProvider=listeSites;
				tncSiteNew.cbSite.selectedIndex=-1;
			}
			else
			{
				tncSite.cbSite.dataProvider=listeSites;
				tncSite.cbSite.selectedIndex=-1;
			}
		}


		private function initData():void
		{
			var objToServiceGet:Object=new Object();
			objToServiceGet.idEmploye=idEmploye;
			objToServiceGet.id=idRow;
			objToServiceGet.idPool=SpecificationVO.getInstance().idPool;
			
			myServiceCollab.getInfosFicheCollaborateur(objToServiceGet);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise les écouteurs.
		 * </pre></p>
		 *
		 * @return void
		 */
		private function initListener():void
		{
			// listeners lié aux boutons de l'IHM
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btnCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btnValid.addEventListener(MouseEvent.CLICK, validClickHandler);

			// listeners permettant l'activation du bouton "valider"
			tiNom.addEventListener(Event.CHANGE, activeValidButton);
			tiPrenom.addEventListener(Event.CHANGE, activeValidButton);
			tiFonction.addEventListener(Event.CHANGE, activeValidButton);
			tiEmail.addEventListener(Event.CHANGE, activeValidButton);
			tiIdentifiant.addEventListener(Event.CHANGE, activeValidButton);
			tiCodeInterne.addEventListener(Event.CHANGE, activeValidButton);
			tiMatricule.addEventListener(Event.CHANGE, activeValidButton);
			tiStatut.addEventListener(Event.CHANGE, activeValidButton);
			tiReference.addEventListener(Event.CHANGE, activeValidButton);
			
			taCommentaires.addEventListener(Event.CHANGE, activeValidButton);

			cbCivilite.addEventListener(ListEvent.CHANGE, activeValidButton);
			cbNivCollab.addEventListener(ListEvent.CHANGE, activeValidButton);
			cbDsSociete.addEventListener(ListEvent.CHANGE, activeValidButton);

			dfInSociety.addEventListener(CalendarLayoutChangeEvent.CHANGE, activeValidButton);
			dfOutSociety.addEventListener(CalendarLayoutChangeEvent.CHANGE, activeValidButton);

			// listeners liés au tabNavigator
			tncSite.cbSite.addEventListener(ListEvent.CHANGE, activeValidButton);
			tncDivers.ti_chp1.addEventListener(Event.CHANGE, activeValidButton);
			tncDivers.ti_chp2.addEventListener(Event.CHANGE, activeValidButton);
			tncDivers.ti_chp3.addEventListener(Event.CHANGE, activeValidButton);
			tncDivers.ti_chp4.addEventListener(Event.CHANGE, activeValidButton);
			this.addEventListener(gestionparcEvent.COLLAB_UPDATE_LIST_LINE_SIM_TERM, onCollabUpdateListLineSimTermHandler);
			this.addEventListener(gestionparcEvent.COLLAB_UPDATE_LIST_TERM, onCollabUpdateListTermHandler);
			tncSite.addEventListener(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED, onInfosFicheSiteUploadedHandler);
			tncSiteNew.addEventListener(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED, onInfosFicheSiteUploadedHandler);
			tncLigneSIM.addEventListener(gestionparcEvent.EDITER_ELT_NAV, onEditerLigneSimHandler);
			tncEquipement.addEventListener(gestionparcEvent.EDITER_ELT_NAV, onEditerEquipementHandler);

			// listeners liés à l'organisation
			cbOrganisation.addEventListener(ListEvent.CHANGE, changeOrgaHandler);
			btModifierOrga.addEventListener(MouseEvent.CLICK, clickModifierOrgaHandler);

			// listeners liés aux services
			myServiceCollab.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_COLLAB_LOADED, infoFicheCollabLoadedHandler);
			myServiceCollab.myDatas.addEventListener(gestionparcEvent.MATRICULE_AUTO, matriculeAutoHandler);

			if (myServiceOrga)
			{
				myServiceOrga.myDatas.addEventListener(gestionparcEvent.LISTE_ORGA_LOADED, listeOragLoadedHandler);
			}
		}

		/**
		 * Initialise l'affichage de cette IHM.
		 */
		private function initDisplay():void
		{
			// tant que tous les champs requit ne sont pas renseignés ou qu'aucune modif n'a été réalisée
			btnValid.enabled=false;

			cbCivilite.dataProvider=CacheDatas.civilites;

			tncSite.myServicesCache=CacheServices.getInstance();
			tncSiteNew.myServicesCache=CacheServices.getInstance();
			tncSite.vbEmplacement.visible=false;
			tncSiteNew.vbEmplacement.visible=false;
			tncSiteNew.vbEmplacement.includeInLayout=false;

			if (isNewCollabFiche)
			{
				bxNewCollaborateur.visible=true;
				bxNewCollaborateur.includeInLayout=true;
				tnOldCollaborateur.visible=false;
				tnOldCollaborateur.includeInLayout=false;
				cbDsSociete.selectedIndex=1; // le collab est dans la societe par defaut
				vbInsertPool.visible=true;
			}
			else
			{
				bxNewCollaborateur.visible=false;
				bxNewCollaborateur.includeInLayout=false;
				tnOldCollaborateur.visible=true;
				tnOldCollaborateur.includeInLayout=true;
				vbInsertPool.visible=false;
			}
			
			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("MASTER_PARC_GFC")
					&& moduleGestionParcIHM.globalParameter.userAccess.MASTER_PARC_GFC == 1){
				boxPositionOrgaOPE.visible = boxPositionOrgaOPE.includeInLayout = true;
				boxPositionOrgaCUS.visible = boxPositionOrgaCUS.includeInLayout = false;
			}else{
				boxPositionOrgaOPE.visible = boxPositionOrgaOPE.includeInLayout = false;
				boxPositionOrgaCUS.visible = boxPositionOrgaCUS.includeInLayout = true;
			}
				
		}

		/**
		 * Charge les données qui viennent d'être collectées de la base dans l'IHM.
		 */
		private function listeOragLoadedHandler(event:gestionparcEvent):void
		{
			myServiceCollab.myDatas.collaborateur=new CollaborateurVO();
			myServiceCollab.myDatas.collaborateur.listeOrga=CacheDatas.listeOrga;
			myServiceOrga.myDatas.listeOrga=CacheDatas.listeOrga;

			cbOrganisation.dataProvider=myServiceCollab.myDatas.collaborateur.listeOrga;
		}

		/**
		 * Charge les données qui viennent d'être collectées de la base dans l'IHM.
		 */
		private function matriculeAuto():void
		{
			myServiceCollab.myDatas.matricule=new Matricule();

			myServiceCollab.getMatriculeAuto();
		}

		/**
		 * Charge les données qui viennent d'être collectées de la base dans l'IHM.
		 */
		private function infoFicheCollabLoadedHandler(event:gestionparcEvent):void
		{

			myServiceCollab.myDatas.collaborateur.idEmploye=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			idGpeClient=myServiceCollab.myDatas.collaborateur.idGpeClient;

			tiNom.text=myServiceCollab.myDatas.collaborateur.nom;
			tiPrenom.text=myServiceCollab.myDatas.collaborateur.prenom;
			tiFonction.text=myServiceCollab.myDatas.collaborateur.fonction;
			cbNivCollab.selectedIndex=myServiceCollab.myDatas.collaborateur.niveau - 1;
			tiEmail.text=myServiceCollab.myDatas.collaborateur.email;
			dfInSociety.selectedDate=myServiceCollab.myDatas.collaborateur.dateEntree;
			dfOutSociety.selectedDate=myServiceCollab.myDatas.collaborateur.dateSortie;
			tiIdentifiant.text=myServiceCollab.myDatas.collaborateur.cleIdentifiant;
			tiCodeInterne.text=myServiceCollab.myDatas.collaborateur.codeInterne;
			tiMatricule.text=myServiceCollab.myDatas.collaborateur.matricule;
			cbDsSociete.selectedIndex=myServiceCollab.myDatas.collaborateur.dansSociete;
			tiStatut.text=myServiceCollab.myDatas.collaborateur.statut;
			tiReference.text=myServiceCollab.myDatas.collaborateur.reference;
			taCommentaires.text=myServiceCollab.myDatas.collaborateur.commentaires;
			lbl_manager.text = lbl_manager.toolTip= myServiceCollab.myDatas.collaborateur.prenomManager + ' ' + myServiceCollab.myDatas.collaborateur.nomManager;
			lbl_MatriculeMngr.text = lbl_MatriculeMngr.toolTip = myServiceCollab.myDatas.collaborateur.matriculeManager;
			
			if(CacheDatas.civilites !=null)
			{
				var lenCivilite:int=CacheDatas.civilites.length;
				for (var j:int=0; j < lenCivilite; j++)
				{
					if (myServiceCollab.myDatas.collaborateur.idCivilite == CacheDatas.civilites[j].idCivilite)
					{
						cbCivilite.selectedItem=CacheDatas.civilites[j];
						break;
					}
				}
			}

			cbOrganisation.dataProvider=myServiceCollab.myDatas.collaborateur.listeOrga;

			// selectionne le dernier chemin modifié
			var dateLastModifPosition:Date=new Date(0);
			var lenListeOrga:int=myServiceCollab.myDatas.collaborateur.listeOrga.length;

			for (var i:int=0; i < lenListeOrga; i++)
			{
				if ((myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO).position == 1 && (myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO).dateModifPosition != null)
				{
					if (ObjectUtil.dateCompare((myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO).dateModifPosition, dateLastModifPosition) == 1)
					{
						lbPosition.text=(myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO).chemin;
						cbOrganisation.selectedIndex=i;
						dateLastModifPosition=(myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO).dateModifPosition;
					}
				}
			}
			
			// selectionne le dernier chemin opérateur modifié
			dateLastModifPosition=new Date(0);
			lenListeOrga=myServiceCollab.myDatas.collaborateur.listeOrgaOPE.length;
			
			for (i=0; i < lenListeOrga; i++)
			{
				if ((myServiceCollab.myDatas.collaborateur.listeOrgaOPE[i] as OrgaVO).position == 1 && (myServiceCollab.myDatas.collaborateur.listeOrgaOPE[i] as OrgaVO).dateModifPosition != null)
				{
					if (ObjectUtil.dateCompare((myServiceCollab.myDatas.collaborateur.listeOrgaOPE[i] as OrgaVO).dateModifPosition, dateLastModifPosition) == 1)
					{
						lbPositionOPE.text=(myServiceCollab.myDatas.collaborateur.listeOrgaOPE[i] as OrgaVO).chemin.split(";").join(" / ");
						lbOrgaOPE.text = (myServiceCollab.myDatas.collaborateur.listeOrgaOPE[i] as OrgaVO).libelleOrga;
						dateLastModifPosition=(myServiceCollab.myDatas.collaborateur.listeOrgaOPE[i] as OrgaVO).dateModifPosition;
					}
				}
			}

			// maj onglet SITE 			
			setSites(false);

			if (myServiceCollab.myDatas.collaborateur.idSite > 0)
			{
				for each (var item:Object in CacheDatas.siteLivraison)
				{
					if (item.idSite == myServiceCollab.myDatas.collaborateur.idSite)
					{
						tncSite.cbSite.selectedItem=item;
						tncSite.txtAdresse.text=item.adresse1;
						tncSite.lbCodePostal.text=item.codePostale;
						tncSite.lbVille.text=item.ville;
						tncSite.lbPays.text=item.pays;
						break;
					}
				}
			}
			else
			{
				tncSite.cbSite.selectedIndex=-1;
			}

			// maj onglet EQUIPEMENTS
			tncEquipement.dataDatagrid=myServiceCollab.myDatas.collaborateur.listeTerm;

			// maj onglet LIGNE/SIM
			tncLigneSIM.dataDatagrid=myServiceCollab.myDatas.collaborateur.listeLigneSim;

			// maj onglet DIVERS
			tncDivers.fi1.label=myServiceCollab.myDatas.collaborateur.libelleChampPerso1;
			tncDivers.fi2.label=myServiceCollab.myDatas.collaborateur.libelleChampPerso2;
			tncDivers.fi3.label=myServiceCollab.myDatas.collaborateur.libelleChampPerso3;
			tncDivers.fi4.label=myServiceCollab.myDatas.collaborateur.libelleChampPerso4;
			tncDivers.champ1=myServiceCollab.myDatas.collaborateur.texteChampPerso1;
			tncDivers.champ2=myServiceCollab.myDatas.collaborateur.texteChampPerso2;
			tncDivers.champ3=myServiceCollab.myDatas.collaborateur.texteChampPerso3;
			tncDivers.champ4=myServiceCollab.myDatas.collaborateur.texteChampPerso4;
		}

		/**
		 * Charge les données qui viennent d'être collectées de la base dans l'IHM.
		 */
		private function matriculeAutoHandler(gpe:gestionparcEvent):void
		{
			if (myServiceCollab.myDatas.matricule.IS_ACTIF > 0)
			{
				if (isNewCollabFiche)
					tiMatricule.text=myServiceCollab.myDatas.collaborateur.matricule=myServiceCollab.myDatas.matricule.CURRENT_VALUE;

				tiMatricule.enabled=false;
			}
			else
				tiMatricule.enabled=true;
		}

		/**
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * Ferme la popup courante.
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * Valide la fiche collaborateur.
		 * Si c'est un nouveau collaborateur alors il en résulte sa création.
		 * Si le collaborateur existait auparavant, alors il en résulte de sa mise à jour.
		 */
		private function validClickHandler(event:MouseEvent):void
		{
			// test les validators
			if (runValidators())
			{
				if (isNewCollabFiche)
				{
					if(ckxPoolSociete.selected)
					{
						ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'L_operation_s_effectuera_sur_tous_les_pools__'),
																	ResourceManager.getInstance().getString('M111', 'Confirmation'),
																	confirmationAddNewCollabHandler);
					}
					else
					{
						myServiceCollab.addNewCollaborateur(createObjToServiceSet());
						myServiceCollab.myDatas.addEventListener(gestionparcEvent.NEW_COLLAB_ADDED, endAddCollab);
					}
				}
				else
				{
					if(ckxPoolSociete.selected)
					{
						ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'L_operation_s_effectuera_sur_tous_les_pools__'),
																	ResourceManager.getInstance().getString('M111', 'Confirmation'),
																	confirmationUpdateHandler);
					}
					else
					{
						myServiceCollab.setInfosFicheCollaborateur(createObjToServiceSet());
						myServiceCollab.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_COLLAB_UPLOADED, endUpdateCollab);
					}
				}
			}
		}
		
		protected function confirmationAddNewCollabHandler(ce : CloseEvent):void{
			if(ce.detail == Alert.OK){
				myServiceCollab.addNewCollaborateur(createObjToServiceSet());
				myServiceCollab.myDatas.addEventListener(gestionparcEvent.NEW_COLLAB_ADDED, endAddCollab);
			}
		}
		
		protected function confirmationUpdateHandler(ce : CloseEvent):void{
			if(ce.detail == Alert.OK){
				myServiceCollab.setInfosFicheCollaborateur(createObjToServiceSet());
				myServiceCollab.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_COLLAB_UPLOADED, endUpdateCollab);
			}
		}

		/**
		 * Créé l'objet non typé que l'on va passer au service
		 */
		private function createObjToServiceSet():Object
		{
			var obj:Object=new Object();

			setInfoCollaborateurVO();
			obj.myCollaborateur=myServiceCollab.myDatas.collaborateur;
			obj.idPool=SpecificationVO.getInstance().idPool;
			obj.xmlListeOrga=createXmlListeOrga();
			obj.insertPoolDistrib=ckxPoolDistributeur.selected ? 1 : 0;
			obj.insertPoolSociete=ckxPoolSociete.selected ? 1 : 0;

			return obj;
		}

		/**
		 * Dispatch un évenement pour dire que le nouveau collaborateur a été ajoutée.
		 */
		private function endAddCollab(event:gestionparcEvent):void
		{
			SpecificationVO.getInstance().refreshData();
			closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Collaborateur_ajout___'), Application.application as DisplayObject);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_COLLAB_UPLOADED));
		}

		/**
		 * Dispatch un évenement pour dire que les informations concernant le collaborateur ont été mis à jour.
		 */
		private function endUpdateCollab(event:gestionparcEvent):void
		{
			SpecificationVO.getInstance().refreshData();
			closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_COLLAB_UPLOADED));
		}

		/**
		 * Met à jour les données du collaborateurs ou en créé un nouveau si c'est une demande d'ajout.
		 */
		private function setInfoCollaborateurVO():void
		{
			myServiceCollab.myDatas.collaborateur.idCivilite=(cbCivilite.selectedItem) ? cbCivilite.selectedItem.idCivilite : 0;
			myServiceCollab.myDatas.collaborateur.nom=tiNom.text;
			myServiceCollab.myDatas.collaborateur.prenom=tiPrenom.text;
			myServiceCollab.myDatas.collaborateur.fonction=tiFonction.text;
			myServiceCollab.myDatas.collaborateur.niveau=(cbNivCollab.selectedItem) ? cbNivCollab.selectedItem.data : 0;
			myServiceCollab.myDatas.collaborateur.email=tiEmail.text;
			myServiceCollab.myDatas.collaborateur.dateEntree=dfInSociety.selectedDate;
			myServiceCollab.myDatas.collaborateur.dateSortie=dfOutSociety.selectedDate;
			myServiceCollab.myDatas.collaborateur.cleIdentifiant=tiIdentifiant.text;
			myServiceCollab.myDatas.collaborateur.codeInterne=tiCodeInterne.text;
			myServiceCollab.myDatas.collaborateur.matricule=tiMatricule.text;
			myServiceCollab.myDatas.collaborateur.dansSociete=(cbDsSociete.selectedItem) ? cbDsSociete.selectedItem.data : 1;
			myServiceCollab.myDatas.collaborateur.statut=tiStatut.text;
			myServiceCollab.myDatas.collaborateur.reference=tiReference.text;
			myServiceCollab.myDatas.collaborateur.commentaires=taCommentaires.text;

			if (isNewCollabFiche)
			{
//				myServiceCollab.myDatas.collaborateur.idSite			 = (tncSiteNew.cbSite.selectedItem)? (tncSiteNew.cbSite.selectedItem as SiteVO).idSite : 0;

				if (tncSiteNew.cbSite.selectedItem != null && (tncSiteNew.cbSite.selectedItem as SiteVO).idSite > 0)
					myServiceCollab.myDatas.collaborateur.idSite=(tncSiteNew.cbSite.selectedItem as SiteVO).idSite;
				else
					myServiceCollab.myDatas.collaborateur.idSite=0;


				myServiceCollab.myDatas.collaborateur.libelleChampPerso1=CacheDatas.libellesPerso1;
				myServiceCollab.myDatas.collaborateur.libelleChampPerso2=CacheDatas.libellesPerso2;
				myServiceCollab.myDatas.collaborateur.libelleChampPerso3=CacheDatas.libellesPerso3;
				myServiceCollab.myDatas.collaborateur.libelleChampPerso4=CacheDatas.libellesPerso4;
				myServiceCollab.myDatas.collaborateur.texteChampPerso1=tncDiversNew.ti_chp1.text;
				myServiceCollab.myDatas.collaborateur.texteChampPerso2=tncDiversNew.ti_chp2.text;
				myServiceCollab.myDatas.collaborateur.texteChampPerso3=tncDiversNew.ti_chp3.text;
				myServiceCollab.myDatas.collaborateur.texteChampPerso4=tncDiversNew.ti_chp4.text;
			}
			else
			{
//				myServiceCollab.myDatas.collaborateur.idSite			 = (tncSite.cbSite.selectedItem)? (tncSite.cbSite.selectedItem as SiteVO).idSite : 0;

				if (tncSite.cbSite.selectedItem != null && (tncSite.cbSite.selectedItem as SiteVO).idSite > 0)
					myServiceCollab.myDatas.collaborateur.idSite=(tncSite.cbSite.selectedItem as SiteVO).idSite;
				else
					myServiceCollab.myDatas.collaborateur.idSite=0;

				myServiceCollab.myDatas.collaborateur.texteChampPerso1=tncDivers.ti_chp1.text;
				myServiceCollab.myDatas.collaborateur.texteChampPerso2=tncDivers.ti_chp2.text;
				myServiceCollab.myDatas.collaborateur.texteChampPerso3=tncDivers.ti_chp3.text;
				myServiceCollab.myDatas.collaborateur.texteChampPerso4=tncDivers.ti_chp4.text;
			}
			
			if(selectedManager)
				myServiceCollab.myDatas.collaborateur.idManager=selectedManager.idManager;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Créé le XML correspondant à la liste des organisations avec les chemins sélectionnés par l'utilisateur.
		 * </pre></p>
		 *
		 * @return String
		 */
		private function createXmlListeOrga():String
		{
			var tmpListeOrga:ArrayCollection=new ArrayCollection();
			var lenListeOrga:int=myServiceCollab.myDatas.collaborateur.listeOrga.length;
			var myXML:String="";

			// trier la liste des orga par date de modification (pour pas changer procedure dba)
			for (var j:int=0; j < lenListeOrga; j++)
			{
				if (tmpListeOrga.length == 0)
				{
					tmpListeOrga.addItem((myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO));
				}
				else
				{
					var index:int=0;
					var longueur:int=tmpListeOrga.length;

					if ((myServiceCollab.myDatas.collaborateur.listeOrga[j] as OrgaVO).dateModifPosition)
					{
						// tant la date de l'item de tmpListeOrga est plus recente que la date de l'item de listeOrga
						while ((index < longueur) && ((tmpListeOrga[index] as OrgaVO).dateModifPosition) && ((tmpListeOrga[index] as OrgaVO).dateModifPosition.getTime() > (myServiceCollab.myDatas.collaborateur.listeOrga[j] as OrgaVO).dateModifPosition.getTime()))
						{
							index++;
						}
						tmpListeOrga.addItemAt((myServiceCollab.myDatas.collaborateur.listeOrga[j] as OrgaVO), index);
					}
					else
					{
						index++;
						tmpListeOrga.addItemAt((myServiceCollab.myDatas.collaborateur.listeOrga[j] as OrgaVO), index);
					}
				}
			}

			// création de l'XML
			myXML+="<orgas>";
			for (var i:int=0; i < lenListeOrga; i++)
			{
				if ((tmpListeOrga[i] as OrgaVO).chemin != null)
				{
					myXML+="<orga>";
					myXML+="<idregleorga><![CDATA[" + (tmpListeOrga[i] as OrgaVO).idRegleOrga + "]]></idregleorga>"
					myXML+="<chemin><![CDATA[" + (tmpListeOrga[i] as OrgaVO).chemin + "]]></chemin>";
					myXML+="<idcible><![CDATA[" + (tmpListeOrga[i] as OrgaVO).idCible + "]]></idcible>";
					myXML+="</orga>";
				}
			}
			myXML+="</orgas>";

			return myXML;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Active le bouton "Valider".
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function activeValidButton(event:Event):void
		{
			btnValid.enabled=true;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Affiche la popup des organisations
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		public function clickModifierOrgaHandler(event:MouseEvent):void
		{
			pop=new PopUpNewOrigineOrCibleIHM();
			pop.typeModif="Noeuds cible";
			pop._selectedRegle=SelectedRegle;
			pop.libelleOrga=(cbOrganisation.selectedItem as OrgaVO).libelleOrga;
			pop.chemin=(cbOrganisation.selectedItem as OrgaVO).chemin;
			pop.idOrga=(cbOrganisation.selectedItem as OrgaVO).idOrga;
			pop.addEventListener(gestionparcEvent.VALIDE_POPUP, updateOrga);
			PopUpManager.addPopUp(pop, this, true);
			PopUpManager.centerPopUp(pop);
		}

		private function refreshDatagridNoeuds(e:Event):void
		{
			dispatchEvent(new Event("regleIsModified", true));
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Sélectionne le "chemin cible" (=position) s'il existe.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function changeOrgaHandler(event:ListEvent):void
		{
			if (cbOrganisation.selectedItem && (cbOrganisation.selectedItem as OrgaVO).position == 1)
			{
				lbPosition.text=(cbOrganisation.selectedItem as OrgaVO).chemin;
			}
			else
			{
				lbPosition.text="";
			}
			activeValidButton(null);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Mise à jour des données pour l'organisation sélectionnée,
		 * après avoir choisi un chemin dans la fiche Organisation.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function updateOrga(event:gestionparcEvent):void
		{

			var lenListeOrga:int=myServiceCollab.myDatas.collaborateur.listeOrga.length;

			for (var i:int=0; i < lenListeOrga; i++)
			{
				if ((myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO).idOrga == pop.myService.myDatas.cheminSelected.idOrga)
				{
					(myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO).idCible=pop.myService.myDatas.cheminSelected.idFeuille;
					(myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO).chemin=pop.myService.myDatas.cheminSelected.chemin;
					(myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO).position=1;
					(myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO).idSource=myServiceCollab.myDatas.collaborateur.idGpeClient;
					(myServiceCollab.myDatas.collaborateur.listeOrga[i] as OrgaVO).dateModifPosition=new Date();
				}
			}
			lbPosition.text=pop.myService.myDatas.cheminSelected.chemin;
			activeValidButton(null);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Apres une modification des infos de la fiche Ligne, appelée depuis l'onglet Ligne/Sim de cette fiche,
		 * cette fonction appelle un service permettant de recupérer les nouvelles informations.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onCollabUpdateListLineSimTermHandler(event:gestionparcEvent):void
		{
//			myServiceCollab.getListLigneSimEqptFromCollab(idEmploye);
//			myServiceCollab.myDatas.addEventListener(gestionparcEvent.LIST_LINK_LINE_SIM_TERM_LOADED,onListLinkLineSimTermLoadedHandler);
			initData();
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Mis a jour du grid apres avoir recupérer les nouvelles informations de la ligne modifiée.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onListLinkLineSimTermLoadedHandler(event:gestionparcEvent):void
		{
			tncLigneSIM.dataDatagrid=myServiceCollab.myDatas.collaborateur.listeLigneSim;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Apres une modification des infos de la fiche Terminal, appelée depuis l'onglet Equipements de cette fiche,
		 * cette fonction appelle un service permettant de recupérer les nouvelles informations.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onCollabUpdateListTermHandler(event:gestionparcEvent):void
		{
//			myServiceCollab.getListEqptFromCollab(idEmploye);
//			myServiceCollab.myDatas.addEventListener(gestionparcEvent.LIST_TERM_COLLAB_LOADED,onListTermCollabLoadedHandler);
			initData();
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Mis a jour du grid apres avoir recupérer les nouvelles informations du terminal modifié.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onListTermCollabLoadedHandler(event:gestionparcEvent):void
		{
			tncEquipement.dataDatagrid=myServiceCollab.myDatas.collaborateur.listeTerm;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appellée après la mise a jour des infos d'un site depuis sa fiche,
		 * pour mettre appeler le service cache permettant de recupere la nouvelle liste des sites.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onInfosFicheSiteUploadedHandler(event:gestionparcEvent):void
		{
			CacheServices.getInstance().getSiteLivraison();
			CacheServices.getInstance().myDatas.addEventListener(gestionparcEvent.LISTE_SITE_LOADED, onListeSiteLoadedhandler);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appellée après la mise a jour des sites de livraison dans le cache,
		 * pour mettre a jour le data provider et les infos affichées.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onListeSiteLoadedhandler(event:gestionparcEvent):void
		{
			CacheServices.getInstance().myDatas.removeEventListener(gestionparcEvent.LISTE_SITE_LOADED, onListeSiteLoadedhandler);
			if (isNewCollabFiche)
			{
				setSites(true);

				tncSiteNew.txtAdresse.text="";
				tncSiteNew.lbCodePostal.text="";
				tncSiteNew.lbVille.text="";
				tncSiteNew.lbPays.text="";
			}
			else
			{
				setSites(false);

				if (myServiceCollab.myDatas.collaborateur.idSite > 0)
				{
					for each (var item:Object in CacheDatas.siteLivraison)
					{
						if (item.idSite == myServiceCollab.myDatas.collaborateur.idSite)
						{
							tncSite.cbSite.selectedItem=item;
							tncSite.txtAdresse.text=item.adresse1;
							tncSite.lbCodePostal.text=item.codePostale;
							tncSite.lbVille.text=item.ville;
							tncSite.lbPays.text=item.pays;
							break;
						}
					}
				}
				else
				{
					tncSite.cbSite.selectedIndex=-1;
				}
			}
		}

		/**
		 * Teste les validateurs et permet ainsi la cohérence des valeurs attendues.
		 */
		private function runValidators():Boolean
		{
			var validators:Array=[valMatricule, valNom];
			var results:Array=Validator.validateAll(validators);

			if (results.length > 0)
			{
				ConsoviewAlert.afficherAlertInfo(gestionparcMessages.RENSEIGNER_CHAMPS_OBLI, ResourceManager.getInstance().getString('M111', 'Attention__'),null);
				return false;
			}
			else
			{
				return true;
			}
		}

		/**
		 * Affiche la popup permettant d'éditer une ligne.
		 */
		private function onEditerLigneSimHandler(event:gestionparcEvent):void
		{
			var ficheLigne:FicheLigneIHM=new FicheLigneIHM();
			ficheLigne.addEventListener(gestionparcEvent.INFOS_FICHE_LIGNE_UPLOADED, onCollabUpdateListLineSimTermHandler, false, 0, true);
			ficheLigne.idsoustete=(tncLigneSIM.selectedItem as SIMVO).ligneRattache.idSousTete;
			ficheLigne.idRow=(tncLigneSIM.selectedItem as SIMVO).idRow > 0 ? (tncLigneSIM.selectedItem as SIMVO).idRow : -1;
			PopUpManager.addPopUp(ficheLigne, parent, true);
			PopUpManager.centerPopUp(ficheLigne);
		}


		/**
		 * Affiche la popup permettant d'éditer un équipement.
		 */
		private function onEditerEquipementHandler(event:gestionparcEvent):void
		{
			var ficheEquipement:FicheEquipementIHM=new FicheEquipementIHM();
			ficheEquipement.idTerminal=(tncEquipement.selectedItem as EquipementVO).idEquipement;
			ficheEquipement.idRow=((tncEquipement.selectedItem as EquipementVO).idRow > 0) ? (tncEquipement.selectedItem as EquipementVO).idRow : -1;
			ficheEquipement.addEventListener(gestionparcEvent.INFOS_FICHE_TERM_UPLOADED, onCollabUpdateListTermHandler, false, 0, true);
			PopUpManager.addPopUp(ficheEquipement, parent, true);
			PopUpManager.centerPopUp(ficheEquipement);
		}
		
		protected function chooseManagerClickHandler(event:MouseEvent):void
		{
			var ficheChoixManager:PopUpChoixManagerIHM = new PopUpChoixManagerIHM();
			ficheChoixManager.addEventListener(gestionparcEvent.MANAGER_SELECTED, getSelectedManager);
			PopUpManager.addPopUp(ficheChoixManager, parent, true);
		}
		
		protected function getSelectedManager(gestParcEvt:gestionparcEvent):void
		{
			lbl_manager.text = '';
			if(gestParcEvt.obj)
			{
				selectedManager = gestParcEvt.obj as CollaborateurVO;
				lbl_manager.text = lbl_manager.toolTip = selectedManager.prenomManager + ' ' + selectedManager.nomManager;
				lbl_MatriculeMngr.text = lbl_MatriculeMngr.toolTip = selectedManager.matriculeManager; 
				
				updateInfosManagerFicheCollaborateur();
			}
		}
		
		private function updateInfosManagerFicheCollaborateur():void
		{
			myServiceCollab.setInfosManagerFicheCollaborateur(createObjToServiceSet());
			myServiceCollab.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_COLLAB_MANAGER_UPLOADED, endUpdateManagerCollab);			
		}
		
		/**
		 * Dispatch un évenement pour dire que les informations concernant le collaborateur ont été mis à jour, ici pricipalement l'info Manager
		 */
		private function endUpdateManagerCollab(event:gestionparcEvent):void
		{
			SpecificationVO.getInstance().refreshData();
			//closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject,2000);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_COLLAB_UPLOADED));
		}
		
		public function get SelectedRegle():Regle
		{
			return _selectedRegle;
		}
		
		public function set SelectedRegle(value:Regle):void
		{
			_selectedRegle=value;
		}
		public function get selectedManager():CollaborateurVO { return _selectedManager; }
		
		public function set selectedManager(value:CollaborateurVO):void
		{
			if (_selectedManager == value)
				return;
			_selectedManager = value;
		}
	}
}