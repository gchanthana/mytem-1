package gestionparc.ihm.fiches.ficheEntity
{
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.ContratVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.equipement.EquipementServices;
	import gestionparc.utils.gestionparcMessages;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.NumberValidator;
	import mx.validators.Validator;

	[Bindable]
	public class FicheContratServiceImpl extends TitleWindow
	{
		public var tiReference:TextInput;
		public var tiTarifMensuel:TextInput;
		public var tiTarifActivation:TextInput;

		public var lbDateEligibilite:Label;
		private var _dateEligibilite:Date;

		public var lbDateButtoirResiliation:Label;
		private var _dateButtoirResiliation:Date;

		public var lbDateFinEngagement:Label;
		private var _dateFinEngagement:Date;

		public var lbOuvertPar:Label;
		public var lbResiliePar:Label;

		public var taCommentaires:TextArea;

		public var valTypeContrat:Validator;
		public var valDureeContrat:Validator;
		public var valDateDebut:Validator;
		public var valRevendeur:Validator;
		public var valTarifMensuel:NumberValidator;
		public var valTarifActivation:NumberValidator;

		public var dcSignature:CvDateChooser;
		public var dcDebut:CvDateChooser;
		public var dcRenouvellement:CvDateChooser;
		public var dcResiliation:CvDateChooser;

		public var cbTypeContrat:ComboBox;
		public var cbRevendeur:ComboBox;
		public var cbDureeContrat:ComboBox;
		public var cbDureeEligibilite:ComboBox;
		public var cbPreavisResiliation:ComboBox;
		public var cbTaciteReconduction:ComboBox;

		public var btValider:Button;
		public var btAnnuler:Button;

		public var myContrat:ContratVO=null;
		public var myServiceTerminal:EquipementServices=null;
		public var myServiceCache:CacheServices=null;

		public var boolSaveDirectly:Boolean=false;
		public var isAddContrat:Boolean=false;

		public function FicheContratServiceImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Initialisation gÃ©nÃ©rale de cette classe  :
		 * - Instanciation du service liÃ© aux caches si c'est le premier appel, sinon pas d'appel de procÃ©dure.
		 */
		private function init(event:FlexEvent):void
		{
			if (!CacheDatas.listeTypeContrat)
			{
				myServiceCache.getListeTypeContrat();
			}
			if (!CacheDatas.listeRevendeur)
			{
				myServiceCache.getListeRevendeur();
			}
			else
			{
				setRevendeurSelected()
			}

			initListener();
			initDisplay();
		}

		private function initListener():void
		{
			// listeners liÃ© aux boutons de FicheDetailleeNouvelleLigneIHM
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btAnnuler.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValider.addEventListener(MouseEvent.CLICK, validClickHandler);

			// listeners permettant l'activation du bouton "valider"
			tiReference.addEventListener(Event.CHANGE, activeValidButton);
			tiTarifActivation.addEventListener(Event.CHANGE, activeValidButton);
			tiTarifMensuel.addEventListener(Event.CHANGE, activeValidButton);

			taCommentaires.addEventListener(Event.CHANGE, activeValidButton);

			cbDureeContrat.addEventListener(ListEvent.CHANGE, activeValidButton);
			cbDureeEligibilite.addEventListener(ListEvent.CHANGE, activeValidButton);
			cbPreavisResiliation.addEventListener(ListEvent.CHANGE, activeValidButton);
			cbTaciteReconduction.addEventListener(ListEvent.CHANGE, activeValidButton);
			cbTypeContrat.addEventListener(ListEvent.CHANGE, activeValidButton);
			cbRevendeur.addEventListener(ListEvent.CHANGE, activeValidButton);

			dcDebut.addEventListener(CalendarLayoutChangeEvent.CHANGE, activeValidButton);
			dcSignature.addEventListener(CalendarLayoutChangeEvent.CHANGE, activeValidButton);
			dcRenouvellement.addEventListener(MouseEvent.CLICK, dcRenouvellementClickHandler);
			dcResiliation.addEventListener(MouseEvent.CLICK, dcResiliationChangeHandler);

			// listeners liÃ©s aux services
			if (myServiceCache)
			{
				myServiceCache.myDatas.addEventListener(gestionparcEvent.LISTE_CONTRAT_LOADED, contratLoadedHandler);
				myServiceCache.myDatas.addEventListener(gestionparcEvent.LISTE_REVENDEUR_LOADED, revendeurLoadedHandler);
			}
		}

		/**
		 * Initialise l'affichage de cette IHM.
		 */
		private function initDisplay():void
		{
			// tant que tous les champs requit ne sont pas renseignÃ©s ou qu'aucune modif n'a Ã©tÃ© rÃ©alisÃ©e
			btValider.enabled=false;

			cbTypeContrat.dataProvider=CacheDatas.listeTypeContrat;
			cbRevendeur.dataProvider=CacheDatas.listeRevendeur;

			if (isAddContrat) // Ajout de contrat
			{
				this.title=ResourceManager.getInstance().getString('M111', 'Ajouter_un_contrat_de_service');
			}
			else // Edition de contrat
			{
				if (myContrat.idContrat > -1)
					this.title=ResourceManager.getInstance().getString('M111', 'Ajouter_un_contrat_de_service');
				else
					this.title=ResourceManager.getInstance().getString('M111', 'Contrat_n__') + " " + myContrat.idContrat;

				// type contrat
				if (CacheDatas.listeTypeContrat)
				{
					var lenListeContrat:int=CacheDatas.listeTypeContrat.length;
					for (var i:int=0; i < lenListeContrat; i++)
					{
						if (CacheDatas.listeTypeContrat[i].ID == myContrat.idTypeContrat)
						{
							cbTypeContrat.selectedIndex=i;
							break;
						}
					}
				}

				// revendeur
				if (CacheDatas.listeRevendeur && myServiceTerminal.myDatas.terminal.revendeur != null)
				{
					var lenListeRevendeur:int=CacheDatas.listeRevendeur.length;
					for (var j:int=0; j < lenListeRevendeur; j++)
					{
						if (CacheDatas.listeRevendeur[j].IDREVENDEUR == myServiceTerminal.myDatas.terminal.revendeur.idRevendeur)
						{
							cbRevendeur.selectedIndex=j;
							break;
						}
					}
				}

				tiReference.text=myContrat.refContrat;
				lbOuvertPar.text=myContrat.nomUserCreate;
				lbResiliePar.text=myContrat.nomUserResi;
				cbDureeEligibilite.selectedIndex=myContrat.dureeEligibilite - 1;
				cbPreavisResiliation.selectedIndex=myContrat.preavis - 1;
				cbTaciteReconduction.selectedIndex=myContrat.taciteReconduction;
				tiTarifMensuel.text=myContrat.tarifMensuel.toString();
				tiTarifActivation.text=myContrat.tarif.toString();
				taCommentaires.text=myContrat.commentaires;

				switch (myContrat.dureeContrat)
				{
					case 12:
					{
						cbDureeContrat.selectedIndex=0;
						break;
					}
					case 18:
					{
						cbDureeContrat.selectedIndex=1;
						break;
					}
					case 24:
					{
						cbDureeContrat.selectedIndex=2;
						break;
					}
					case 36:
					{
						cbDureeContrat.selectedIndex=3;
						break;
					}
					case 48:
					{
						cbDureeContrat.selectedIndex=4;
						break;
					}
				
					case 60:
					{
						cbDureeContrat.selectedIndex=5;
						break;
					}
				}

				if (myContrat.dateSignature != null)
					dcSignature.selectedDate=myContrat.dateSignature;

				if (myContrat.dateDebut != null)
					dcDebut.selectedDate=myContrat.dateDebut;

				if (myContrat.dateRenouvellement != null)
					dcRenouvellement.selectedDate=myContrat.dateRenouvellement;

				if (myContrat.dateEligibilite != null)
				{
					_dateEligibilite=myContrat.dateEligibilite;
					lbDateEligibilite.text=DateFunction.formatDateAsString(myContrat.dateEligibilite);
				}
				else if (myContrat.dureeEligibilite > 0)
				{
					_dateEligibilite=estimateDateEligibilite(myContrat.dateDebut, myContrat.dureeEligibilite);
					lbDateEligibilite.text=DateFunction.formatDateAsString(_dateEligibilite);
				}

				if (myContrat.dateResi != null)
				{
					_dateButtoirResiliation=myContrat.dateResi;
					lbDateButtoirResiliation.text=DateFunction.formatDateAsString(myContrat.dateResi);
				}
				else if (myContrat.preavis > 0 && myContrat.dureeContrat > 0)
				{
					_dateButtoirResiliation=estimateDateButtoireResiliation(myContrat.dateDebut, myContrat.dureeContrat, myContrat.preavis);
					lbDateButtoirResiliation.text=DateFunction.formatDateAsString(_dateButtoirResiliation);
				}

				if (myContrat.dateEcheance != null)
				{
					_dateFinEngagement=myContrat.dateEcheance;
					lbDateFinEngagement.text=DateFunction.formatDateAsString(myContrat.dateEcheance);
				}
				else if (myContrat.dureeContrat > 0)
				{
					_dateFinEngagement=estimateDateFinEngagement(myContrat.dateDebut, myContrat.dureeContrat);
					lbDateFinEngagement.text=DateFunction.formatDateAsString(_dateFinEngagement);
				}

				if (myContrat.dateResiliation != null)
					dcResiliation.selectedDate=myContrat.dateResiliation;
			}
		}

		/**
		 * Active le bouton "Valider".
		 */
		private function activeValidButton(event:Event):void
		{
			btValider.enabled=true;
		}

		/**
		 * Met Ã  jour les donnÃ©es du contrat de service.
		 * Que ca soit un ajout ou une mise Ã  jour, mÃªme traitement.
		 */
		private function setInfoContratVO():void
		{
			myContrat.idTypeContrat=cbTypeContrat.selectedItem.ID;
			myContrat.typeContrat=cbTypeContrat.selectedItem.LIBELLE;
			myContrat.dateSignature=(dcSignature.selectedDate) ? dcSignature.selectedDate : null;
			myContrat.dateDebut=(dcDebut.selectedDate) ? dcDebut.selectedDate : null;
			myContrat.dateRenouvellement=(dcRenouvellement.selectedDate) ? dcRenouvellement.selectedDate : null;

			myContrat.dateEligibilite=(lbDateEligibilite.text) ? _dateEligibilite : null;

			myContrat.dateResi=(lbDateButtoirResiliation.text) ? _dateButtoirResiliation : null;

			myContrat.dateEcheance=(lbDateFinEngagement.text) ? _dateFinEngagement : null;


			myContrat.dateResiliation=(dcResiliation.selectedDate) ? dcResiliation.selectedDate : null;
			myContrat.refContrat=tiReference.text;
			myContrat.idRevendeur=cbRevendeur.selectedItem.IDREVENDEUR;
			myContrat.revendeur=cbRevendeur.selectedItem.LIBELLE;
			myContrat.nomUserCreate=lbOuvertPar.text;
			myContrat.nomUserResi=lbResiliePar.text;
			myContrat.dureeContrat=(cbDureeContrat.selectedItem) ? cbDureeContrat.selectedItem.DUREE_CONTRAT : 0;
			myContrat.dureeEligibilite=(cbDureeEligibilite.selectedItem) ? cbDureeEligibilite.selectedItem.DUREE : 0;
			myContrat.preavis=(cbPreavisResiliation.selectedItem) ? cbPreavisResiliation.selectedItem.DUREE : 0;
			myContrat.taciteReconduction=(cbTaciteReconduction.selectedItem) ? cbTaciteReconduction.selectedItem.value : 0;
			myContrat.tarifMensuel=Number(tiTarifMensuel.text);
			myContrat.tarif=Number(tiTarifActivation.text);
			myContrat.commentaires=taCommentaires.text;
		}

		/**
		 * Valide la fiche contrat de serivce en mettant Ã  jour les donnÃ©es du contrat ou en crÃ©eant le contrat.
		 */
		private function validClickHandler(event:MouseEvent):void
		{
			if (runValidators())
			{
				setInfoContratVO();

				if (boolSaveDirectly)
				{
					myServiceTerminal.setInfosContrat(myContrat, myServiceTerminal.myDatas.terminal.idEquipement);
					myServiceTerminal.myDatas.addEventListener(gestionparcEvent.INFOS_CONTRAT_SAVED, infosContratSavedHandler);
				}
				else
				{
					closePopup(null);
					dispatchEvent(new gestionparcEvent(gestionparcEvent.UPDATE_LISTE_CONTRAT, myContrat));
				}
			}
		}

		/**
		 * Dispatch un évenement pour dire que les informations concernant le contrat du terminal ont été mis à jour.
		 */
		private function infosContratSavedHandler(evt:gestionparcEvent):void
		{
			myContrat.idContrat=(evt.obj.idContrat as int);
			closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
			myServiceTerminal.myDatas.removeEventListener(gestionparcEvent.INFOS_CONTRAT_SAVED, infosContratSavedHandler);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.UPDATE_LISTE_CONTRAT, myContrat));
		}

		/**
		 * Applique les restrictions selon les dates choisies.
		 */
		private function dcResiliationChangeHandler(event:MouseEvent):void
		{
			if (dcRenouvellement.selectedDate)
			{
				dcResiliation.selectableRange={rangeStart: dcRenouvellement.selectedDate};
			}
			else if (dcDebut.selectedDate)
			{
				dcResiliation.selectableRange={rangeStart: dcDebut.selectedDate};
			}
			else
			{
				dcResiliation.selectableRange={rangeStart: new Date()};
			}

			activeValidButton(null);
		}

		/**
		 * Applique les restrictions selon les dates choisies.
		 * On ne peut pas renouveller, résilier avant la date de début
		 */
		protected function dcDebutChangeHandler():void
		{
			if (dcDebut.selectedDate && dcRenouvellement.selectedDate == null)
			{
				dcRenouvellement.selectableRange={rangeStart: dcDebut.selectedDate};
				dcResiliation.selectableRange={rangeStart: dcDebut.selectedDate};


				if (cbDureeEligibilite.selectedIndex != -1)
				{
					_dateEligibilite=estimateDateEligibilite(dcDebut.selectedDate, Number(cbDureeEligibilite.selectedItem.value));
					lbDateEligibilite.text=DateFunction.formatDateAsString(_dateEligibilite);
				}
				else
				{
					lbDateEligibilite.text="";
				}

				if (cbPreavisResiliation.selectedIndex != -1)
				{
					_dateButtoirResiliation=estimateDateButtoireResiliation(dcDebut.selectedDate, Number(cbDureeContrat.selectedItem.value), Number(cbPreavisResiliation.selectedItem.value));
					lbDateButtoirResiliation.text=DateFunction.formatDateAsString(_dateButtoirResiliation);
				}
				else
				{
					lbDateButtoirResiliation.text="";
				}


				if (cbDureeContrat.selectedIndex != -1)
				{
					_dateFinEngagement=estimateDateFinEngagement(dcDebut.selectedDate, Number(cbDureeContrat.selectedItem.value))
					lbDateFinEngagement.text=DateFunction.formatDateAsString(_dateFinEngagement);
				}
				else
				{
					lbDateFinEngagement.text="";
				}
			}

			activeValidButton(null);
		}


		/**
		 * Applique les restrictions selon les dates choisies.
		 * On ne peut pas renouveller, résilier avant la date de renouvellement
		 */
		protected function dcRenouvellementChangeHandler():void
		{
			if (dcRenouvellement.selectedDate != null)
			{
				lbDateEligibilite.text="";

				dcResiliation.selectableRange={rangeStart: dcDebut.selectedDate};
				if (cbDureeEligibilite.selectedIndex != -1)
				{
					_dateEligibilite=estimateDateEligibilite(dcDebut.selectedDate, Number(cbDureeEligibilite.selectedItem.value));
					lbDateEligibilite.text=DateFunction.formatDateAsString(_dateEligibilite);
				}

				lbDateButtoirResiliation.text="";
				if (cbDureeContrat.selectedIndex != -1 && cbPreavisResiliation.selectedIndex != -1)
				{
					_dateButtoirResiliation=estimateDateButtoireResiliation(dcDebut.selectedDate, Number(cbDureeContrat.selectedItem.value), Number(cbPreavisResiliation.selectedItem.value));
					lbDateButtoirResiliation.text=DateFunction.formatDateAsString(_dateButtoirResiliation);
				}

				lbDateFinEngagement.text="";
				if (cbDureeContrat.selectedIndex != -1)
				{
					_dateFinEngagement=estimateDateFinEngagement(dcDebut.selectedDate, Number(cbDureeContrat.selectedItem.value))
					lbDateFinEngagement.text=DateFunction.formatDateAsString(_dateFinEngagement);
				}

			}
			activeValidButton(null);
		}


		/**
		 * Applique les restrictions selon les dates choisies.
		 */
		private function dcRenouvellementClickHandler(event:MouseEvent):void
		{
			if (dcDebut.selectedDate)
			{
				dcRenouvellement.selectableRange={rangeStart: dcDebut.selectedDate};
			}
			else
			{
				dcRenouvellement.selectableRange=null;
			}
			activeValidButton(null);
		}

		/**
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * Ferme la popup courante.
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * Charge les donnÃ©es revenues de la base par rapport Ã  la liste des types de contrat.
		 *
		 */
		private function contratLoadedHandler(event:gestionparcEvent):void
		{
			cbTypeContrat.dataProvider=CacheDatas.listeTypeContrat;

			var lenListeContrat:int=CacheDatas.listeTypeContrat.length;
			for (var i:int=0; i < lenListeContrat; i++)
			{
				if (myContrat && CacheDatas.listeTypeContrat[i].ID == myContrat.idTypeContrat)
				{
					cbTypeContrat.selectedIndex=i;
					break;
				}
			}
		}

		/**
		 * Charge les donnÃ©es revenues de la base par rapport Ã  la liste des revendeurs.
		 */
		private function revendeurLoadedHandler(event:gestionparcEvent):void
		{
			cbRevendeur.dataProvider=CacheDatas.listeRevendeur;
			setRevendeurSelected()
		}

		private function setRevendeurSelected():void
		{
			if (myContrat && myServiceTerminal.myDatas.terminal.revendeur)
			{
				var lenListeRevendeur:int=CacheDatas.listeRevendeur.length;
				for (var j:int=0; j < lenListeRevendeur; j++)
				{
					if (CacheDatas.listeRevendeur[j].IDREVENDEUR == myServiceTerminal.myDatas.terminal.revendeur.idRevendeur)
					{
						cbRevendeur.selectedIndex=j;
						break;
					}
				}
			}
		}

		/**
		 * Teste les validateurs et permet ainsi la cohÃ©rence des valeurs attendues.
		 */
		private function runValidators():Boolean
		{
			var validators:Array=null;
			validators=[valTypeContrat, valDureeContrat, valDateDebut, valRevendeur];
			var results:Array=Validator.validateAll(validators);

			if (results.length > 0)
			{
				ConsoviewAlert.afficherAlertInfo(gestionparcMessages.RENSEIGNER_CHAMPS_OBLI, ResourceManager.getInstance().getString('M111', 'Attention__'),null);
				return false;
			}
			else
			{
				return true;
			}
		}

		private function estimateDateEligibilite(dateValue:Date, dureeEligibilite:Number):Date
		{
			var d1:Date=DateFunction.dateAdd("m", dateValue, dureeEligibilite);
			var d2:Date=DateFunction.dateAdd("d", d1, -1);
			return d2
		}

		private function estimateDateButtoireResiliation(dateValue:Date, dureeContrat:Number, preavis:Number):Date
		{
			var d1:Date=DateFunction.dateAdd("m", dateValue, dureeContrat - preavis);
			var d2:Date=DateFunction.dateAdd("d", d1, -1);
			return d2
		}

		private function estimateDateFinEngagement(dateValue:Date, dureeContrat:Number):Date
		{
			var d1:Date=DateFunction.dateAdd("m", dateValue, dureeContrat);
			var d2:Date=DateFunction.dateAdd("d", d1, -1);
			return d2
		}
	}
}
