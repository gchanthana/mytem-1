package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import gestionparc.services.ligne.LigneServices;

	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;

	public class ContentCaracteristiqueFixeImpl extends VBox
	{
		public var tiNomReseau:TextInput;
		public var tiGTRContractuel:TextInput;
		public var tiPresentation:TextInput;
		public var tiMontantContrat:TextInput;
		public var tiMontantDebitMax:TextInput;
		public var tiProtocolePhysique:TextInput;
		public var tiProtocoleReseau:TextInput;
		public var tiProtocoleLiaison:TextInput;
		public var tiDescendantContrat:TextInput;
		public var tiDescendantDebitMax:TextInput;

		public var cbMontantUnite:ComboBox;
		public var cbDescendantUnite:ComboBox;

		public var ckxUsageData:CheckBox;
		public var ckxUsageVoix:CheckBox;

		public var myLigneService:LigneServices;


		public function ContentCaracteristiqueFixeImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
		}

		public function getUsageValue():int
		{
			/*
				Retourne la valeur en fonction du choix de l'usage :
				VOIX = 1
				DATA = 2
				VOIX et DATA = 3
			*/
			if (ckxUsageData.selected && ckxUsageVoix.selected)
				return 3;
			else if (ckxUsageData.selected && !ckxUsageVoix.selected)
				return 2;
			else if (!ckxUsageData.selected && ckxUsageVoix.selected)
				return 1;
			else
				return 0;
		}

		public function setInfos():void
		{
			tiNomReseau.text=myLigneService.myDatas.ligne.nomReseau;
			tiGTRContractuel.text=myLigneService.myDatas.ligne.GTRContractuel;
			tiPresentation.text=myLigneService.myDatas.ligne.Presentation;
			tiMontantContrat.text=(myLigneService.myDatas.ligne.debitMontantContractuel > -1) ? myLigneService.myDatas.ligne.debitMontantContractuel.toString() : "";
			tiMontantDebitMax.text=(myLigneService.myDatas.ligne.debitMontantMax > -1) ? myLigneService.myDatas.ligne.debitMontantMax.toString() : "";
			tiProtocolePhysique.text=myLigneService.myDatas.ligne.protocolePhysique;
			tiProtocoleReseau.text=myLigneService.myDatas.ligne.protocoleReseau;
			tiProtocoleLiaison.text=myLigneService.myDatas.ligne.protocoleLiaison;
			tiDescendantContrat.text=(myLigneService.myDatas.ligne.debitDescendantContractuel > -1) ? myLigneService.myDatas.ligne.debitDescendantContractuel.toString() : "";
			tiDescendantDebitMax.text=(myLigneService.myDatas.ligne.debitDescendantMax > -1) ? myLigneService.myDatas.ligne.debitDescendantMax.toString() : "";

			switch (myLigneService.myDatas.ligne.usage)
			{
				case 1:
					ckxUsageData.selected=false;
					ckxUsageVoix.selected=true;
					break;
				case 2:
					ckxUsageData.selected=true;
					ckxUsageVoix.selected=false;
					break;
				case 3:
					ckxUsageData.selected=true;
					ckxUsageVoix.selected=true;
					break;
				default:
					ckxUsageData.selected=false;
					ckxUsageVoix.selected=false;
					break;
			}

			if (myLigneService.myDatas.ligne.debitMontantUnite != null)
			{
				for each (var obj:Object in cbMontantUnite.dataProvider)
				{
					if (obj.VALUE == myLigneService.myDatas.ligne.debitMontantUnite.toString())
					{
						cbMontantUnite.selectedItem=obj;
						break;
					}
				}
			}
			if (myLigneService.myDatas.ligne.debitDescendantUnite != null)
			{
				for each (var obj1:Object in cbDescendantUnite.dataProvider)
				{
					if (obj1.VALUE == myLigneService.myDatas.ligne.debitDescendantUnite.toString())
					{
						cbDescendantUnite.selectedItem=obj1;
						break;
					}
				}
			}
		}
	}
}