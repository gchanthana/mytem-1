package gestionparc.ihm.fiches.ficheEntity
{
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.IncidentVO;
	import gestionparc.entity.InterventionVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.popup.PopUpEmailRevendeurIHM;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.incident.IncidentServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.DateField;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	import mx.utils.UIDUtil;

	/**
	 * Classe concernant toutes les informations de la fiche incident.
	 */
	[Bindable]
	public class FicheIncidentImpl extends TitleWindow
	{
		public var lbNumIncident:Label;
		public var lbDateDebut:Label;
		public var lbDateFin:Label;
		public var lbRevendeur:Label;
		public var lbNbIntervention:Label;
		public var lbGarantie:Label;
		public var lbOuvertPar:Label;
		public var lbFermePar:Label;
		public var lbEtat:Label;
		public var lbDuree:Label;
		public var lbCout:Label;

		public var tiReference:TextInput;

		public var taProbleme:TextArea;
		public var taSolution:TextArea;

		public var btAjouterIntervention:Button;
		public var btFermer:Button;
		public var btValider:Button;
		public var btAnnuler:Button;

		public var checkSAV:CheckBox;

		public var dgListe:DataGrid;

		public var myIncident:IncidentVO=null;
		public var isAddIncident:Boolean=false;
		public var isSIM:Boolean=false;

		public var idEqpt:int=-1;
		public var libelleEqpt:String="";
		public var idRevendeurEqpt:int=-1;
		public var revendeurEqpt:String="";
		public var dateFinGarantieEqpt:Date=null;
		public var listIncidentEqpt:ArrayCollection=null;
		public var myServiceCache:CacheServices=null;

		private var myPriceFormatter:CurrencyFormatter=null;
		private var myServiceIncident:IncidentServices=null;

		private var _popUpEmailRevendeur:PopUpEmailRevendeurIHM;
		private var _listeEmails:ArrayCollection=new ArrayCollection();
		private var _isModified:Boolean=false;
		private var _idTypeProfil:int=0;


		/**
		 * Constructeur de la classe FicheIncidentImpl_V2.
		 */
		public function FicheIncidentImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Formate les données de la colonne avec le bon affichage des dates au format String.
		 */
		protected function formaterColonneDates(item:Object, column:DataGridColumn):String
		{
			if (item != null && item[column.dataField] != null)
			{
				return DateFunction.formatDateAsString(item[column.dataField]);
			}
			else
			{
				return "";
			}
		}

		/**
		 * Formate les données de la colonne avec le bon affichage du prix selon le pays.
		 */
		protected function formateColonnneEuros(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{
				return ConsoviewFormatter.formatEuroCurrency(item[column.dataField], 2);
			}
			else
			{
				return "";
			}
		}

		/**
		 * Edite l'intervention sélectionnée.
		 */
		internal function _imgEditerInterventionClickHandler(event:MouseEvent):void
		{
			var ficheEditIntervention:FicheInterventionIncidentIHM=new FicheInterventionIncidentIHM();

			ficheEditIntervention.isAddIntervention=false;
			ficheEditIntervention.myIncident=myIncident;
			ficheEditIntervention.myServiceIncident=myServiceIncident;
			ficheEditIntervention.myServiceCache=myServiceCache;
			ficheEditIntervention.myIntervention=(dgListe.selectedItem as InterventionVO);

			ficheEditIntervention.addEventListener(gestionparcEvent.UPDATE_LISTE_INTERVENTION, updateModifListeInterventionHandler);

			PopUpManager.addPopUp(ficheEditIntervention, this, true);
			PopUpManager.centerPopUp(ficheEditIntervention);
		}

		/**
		 * Supprime l'intervention sélectionnée.
		 */
		internal function _imgSupprimerClickHandler(event:MouseEvent):void
		{
			// seléction de la bonne intervention parmis la liste des interventions de cet incident et suppression
			var lenListeIntervention:int=myIncident.listeIntervention.length;
			for (var j:int=0; j < lenListeIntervention; j++)
			{
				if ((myIncident.listeIntervention[j] as InterventionVO).idIntervention == (dgListe.selectedItem as InterventionVO).idIntervention)
				{
					myIncident.nbIntervention--;
					myIncident.totalCoutIntervention-=(dgListe.selectedItem as InterventionVO).prix;
					lbCout.text=myPriceFormatter.format(myIncident.totalCoutIntervention);
					lbNbIntervention.text=myIncident.nbIntervention.toString();
					myServiceIncident.deleteIntervention(dgListe.selectedItem as InterventionVO);
					myIncident.listeIntervention.removeItemAt(j);
					break;
				}
			}
		}
		
		/**
		 * Initialisation générale de cette classe  :
		 * - Appel la procédure permettant de récupéer la liste des interventions pour cet incident.
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 */
		private function init(event:FlexEvent):void
		{
			myServiceIncident=new IncidentServices();

			initListener();
			initDisplay();

			if (!isAddIncident)
			{
				myServiceIncident.getListeIntervention(myIncident.idIncident);
			}
			getTypeUserTypeProfile();
		}

		/**
		 * Initialise les écouteurs.
		 */
		private function initListener():void
		{
			// listeners lié aux boutons de FicheDetailleeNouvelleLigneIHM
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btAnnuler.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValider.addEventListener(MouseEvent.CLICK, validClickHandler);
			btFermer.addEventListener(MouseEvent.CLICK, closeClickHandler);
			btAjouterIntervention.addEventListener(MouseEvent.CLICK, ajouterInterventionHandler);

			// listeners permettant l'activation du bouton "valider"
			tiReference.addEventListener(Event.CHANGE, activeValidButton);
			taProbleme.addEventListener(Event.CHANGE, activeValidButton);
			taSolution.addEventListener(Event.CHANGE, activeValidButton);

			// listeners liés aux services
			myServiceIncident.myDatas.addEventListener(gestionparcEvent.INFOS_INCIDENT_SAVED, infosIncidentSavedHandler);
			if (!isAddIncident)
			{
				myServiceIncident.myDatas.addEventListener(gestionparcEvent.LISTE_INTER_LOADED, listeInterventionLoadedHandler);
			}
		}

		/**
		 * Initialise l'affichage de cette IHM.
		 */
		private function initDisplay():void
		{
			var currentDate:Date=new Date();

			var myDateFormatter:DateFormatter=new DateFormatter();
			myDateFormatter.formatString=ResourceManager.getInstance().getString('M111', 'DD_MM_YYYY') + " J:NN:SS";

			myPriceFormatter=new CurrencyFormatter();
			myPriceFormatter.currencySymbol=ResourceManager.getInstance().getString('M111', '_signe_de_la_devise__');
			myPriceFormatter.precision=2;
			myPriceFormatter.thousandsSeparatorTo=ResourceManager.getInstance().getString('M111', 'formateur_thousandsSeparatorTo');
			myPriceFormatter.decimalSeparatorTo=ResourceManager.getInstance().getString('M111', 'formateur_decimalSeparatorTo');
			myPriceFormatter.alignSymbol=ResourceManager.getInstance().getString('M111', 'formateur_align_symbol');

			if (isAddIncident) // Creation
			{
				lbDateDebut.text=myDateFormatter.format(currentDate);
				myIncident.dateDebut=currentDate;
				myIncident.etat=ResourceManager.getInstance().getString('M111', 'Ouvert');
				lbNbIntervention.text="0";
				tiReference.text=lbDateDebut.text.substring(0, 2) + lbDateDebut.text.substring(3, 5) + lbDateDebut.text.substring(8, 10) + "-" + UIDUtil.createUID();
				tiReference.text=tiReference.text.substring(0, 12); //Référence automatique : jjmmaa-0001
				lbOuvertPar.text=CvAccessManager.getSession().USER.NOM + ' ' + CvAccessManager.getSession().USER.PRENOM;
				lbCout.text=myPriceFormatter.format(0);

				btValider.enabled=true;
				btAjouterIntervention.enabled=false;
				btFermer.enabled=false;
				dgListe.enabled=false;
			}
			else // Edition
			{
				lbNumIncident.text=myIncident.numIncident;
				lbDateDebut.text=myDateFormatter.format(myIncident.dateDebut);
				lbNbIntervention.text=myIncident.nbIntervention.toString();
				tiReference.text=myIncident.refIncident;
				lbOuvertPar.text=myIncident.userCreate;
				lbFermePar.text=myIncident.userClose;
				lbEtat.text=myIncident.etat;
				lbCout.text=myPriceFormatter.format(myIncident.totalCoutIntervention);
				taProbleme.text=myIncident.description;
				taSolution.text=myIncident.solution;

				btValider.enabled=false;
			}

			// Creation et édition
			if (dateFinGarantieEqpt == null)
			{
				lbGarantie.text=ResourceManager.getInstance().getString('M111', 'Non__pas_de_garantie');
			}
			else
			{
				if (ObjectUtil.dateCompare(dateFinGarantieEqpt, myIncident.dateDebut) == 1)
				{
					lbGarantie.text=ResourceManager.getInstance().getString('M111', 'Oui__fin_de_garantie___') + DateFunction.formatDateAsString(dateFinGarantieEqpt);
				}
				else
				{
					lbGarantie.text=ResourceManager.getInstance().getString('M111', 'Non__fin_de_garantie___') + DateFunction.formatDateAsString(dateFinGarantieEqpt);
				}
			}

			if (myIncident.isClose)
			{
				btAjouterIntervention.enabled=false;
				btFermer.enabled=false;
				lbDateFin.text=myDateFormatter.format(myIncident.dateFin);
				lbDuree.text=new int(((myIncident.dateFin.getTime() - myIncident.dateDebut.getTime() as Number) / 1000 / 60 / 60 as Number)).toString() + "(h)";
			}
			else
			{
				lbFermePar.text="-";
				lbDateFin.text="-";
				
				if(myIncident.dateDebut != null)
				{
					lbDuree.text=new int(((currentDate.getTime() - myIncident.dateDebut.getTime() as Number) / 1000 / 60 / 60 as Number)).toString() + "(h)";	
				}
				
			}

			lbRevendeur.text=revendeurEqpt;
			lbEtat.text=myIncident.etat;
			dgListe.dataProvider=myIncident.listeIntervention;
		}

		/**
		 * Active le bouton "Valider".
		 */
		private function activeValidButton(event:Event):void
		{
			btValider.enabled=true;
		}

		/**
		 * Met à jour les données d'un incident sur un équipement.
		 * Que ca soit un ajout ou une mise à jour, même traitement.
		 */
		private function setInfoIncidentVO(isClose:Boolean):void
		{
			myIncident.description=taProbleme.text;
			myIncident.solution=taSolution.text;
			myIncident.refIncident=tiReference.text;
			myIncident.revendeur=revendeurEqpt;

			if (isClose)
			{
				myIncident.isClose=1;
			}
			else
			{
				myIncident.isClose=0;
			}
		}

		/**
		 * Valide la fiche contrat de serivce en mettant à jour les données du contrat ou en créeant le contrat.
		 */
		private function validClickHandler(event:MouseEvent):void
		{
			var isClose:Boolean=false;
			setInfoIncidentVO(isClose);
			if (_idTypeProfil == 3 || _idTypeProfil == 1)
				popUpEmailsRevendeur()
			else
			{
				myServiceIncident.setInfosIncident(setObj());
			}
		}

		private function setObj(email:String=""):Object
		{
			var obj:Object=new Object();
			obj.idEquipement=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.idrevendeur=idRevendeurEqpt;
			obj.putActif=putSAV();
			obj.refIncident=myIncident.refIncident, obj.description=myIncident.description, obj.isClose=myIncident.isClose, obj.solution=myIncident.solution, obj.idIncident=myIncident.idIncident;
			obj.libelleEqpt=SpecificationVO.getInstance().elementDataGridSelected.T_MODELE;
			obj.idcause=-1;
			obj.revendeuremail=email;
			obj.subject=ResourceManager.getInstance().getString('M111', 'Rapport_de_SAV___');
			obj.lang=ResourceManager.getInstance().localeChain[1];

			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.T_IMEI;
			obj.ID_TERMINAL=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(new Date(), 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}

		/**
		 * Valide la fiche contrat de serivce en mettant à jour les données du contrat ou en créeant le contrat.
		 */
		private function closeClickHandler(event:MouseEvent):void
		{
			var isClose:Boolean=true;
			setInfoIncidentVO(isClose);
			myServiceIncident.setInfosIncident(setObj());
			myServiceIncident.myDatas.addEventListener(gestionparcEvent.INFOS_INCIDENT_SAVED, infosIncidentSavedHandler);
		}

		/**
		 * Regarde toute la liste des incidents, si un des incidents n'est pas cloturé alors
		 */
		private function putSAV():int
		{
			var lenListeIncident:int=listIncidentEqpt.length;

			if (isSIM) // une sim ne peut pas être mis en SAV
			{
				return 0;
			}

			if (isAddIncident)
			{
				return 1;
			}
			else
			{
				for (var i:int=0; i < lenListeIncident; i++)
				{
					if ((listIncidentEqpt[i] as IncidentVO).isClose == 0)
					{
						return 1;
					}
				}

				return 0;
			}
		}

		/**
		 * Dispatch un évenement pour dire que les informations concernant le contrat de l'équipement ont été mis à jour.
		 */
		private function infosIncidentSavedHandler(e:gestionparcEvent):void
		{
			myIncident.idIncident=(e.obj.idIncident as int);
			myIncident.numIncident=(e.obj.numIncident as String);

			if (isAddIncident)
			{
				myIncident.userCreate=CvAccessManager.getSession().USER.NOM + ' ' + CvAccessManager.getSession().USER.PRENOM;
			}

			if (myIncident.isClose)
			{
				myIncident.dateFin=new Date();
				myIncident.userClose=CvAccessManager.getSession().USER.NOM + ' ' + CvAccessManager.getSession().USER.PRENOM;
				myIncident.etat=ResourceManager.getInstance().getString('M111', 'Ferm_');
			}

			myServiceIncident.myDatas.removeEventListener(gestionparcEvent.INFOS_INCIDENT_SAVED, infosIncidentSavedHandler);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.UPDATE_LISTE_INCIDENT, myIncident));

			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
			closePopup(null);
		}

		/**
		 * Charge la liste des interventions recues pour cet incident.
		 *
		 */
		private function listeInterventionLoadedHandler(e:gestionparcEvent):void
		{
			myIncident.listeIntervention=(e.obj as ArrayCollection);
			dgListe.dataProvider=myIncident.listeIntervention;
		}

		/**
		 * Dispatch un évenement pour dire que les informations concernant le contrat de l'équipement ont été mis à jour.
		 */
		private function ajouterInterventionHandler(event:MouseEvent):void
		{
			var ficheAddIntervention:FicheInterventionIncidentIHM=new FicheInterventionIncidentIHM();

			ficheAddIntervention.isAddIntervention=true;
			ficheAddIntervention.myIntervention=new InterventionVO();
			ficheAddIntervention.myIncident=myIncident;
			ficheAddIntervention.myServiceIncident=myServiceIncident;
			ficheAddIntervention.myServiceCache=myServiceCache;

			ficheAddIntervention.addEventListener(gestionparcEvent.UPDATE_LISTE_INTERVENTION, updateAddListeInterventionHandler);

			PopUpManager.addPopUp(ficheAddIntervention, parent, true);
			PopUpManager.centerPopUp(ficheAddIntervention);
		}

		/**
		 * Applique l'ajout de l'Intervention.
		 */
		private function updateAddListeInterventionHandler(e:gestionparcEvent):void
		{
			myIncident.listeIntervention.addItem((e.obj as InterventionVO));
			myIncident.nbIntervention++;
			myIncident.totalCoutIntervention+=(e.obj as InterventionVO).prix;
			lbCout.text=myPriceFormatter.format(myIncident.totalCoutIntervention);
			lbNbIntervention.text=myIncident.nbIntervention.toString();
			interventionCloseIncident((e.obj as InterventionVO));
		}

		/**
		 * Applique les modifications de l'intervention sélectionnée.
		 */
		private function updateModifListeInterventionHandler(e:gestionparcEvent):void
		{
			// seléction du bon incident parmis la liste des incidents de cet equipement et suppression
			var lenListeIntervention:int=myIncident.listeIntervention.length;
			var indexDel:int=-1;
			myIncident.totalCoutIntervention=0;

			for (var j:int=0; j < lenListeIntervention; j++)
			{
				myIncident.totalCoutIntervention+=(myIncident.listeIntervention[j] as InterventionVO).prix;
				if ((myIncident.listeIntervention[j] as InterventionVO).idIntervention == (e.obj as InterventionVO).idIntervention)
				{
					indexDel=j;
				}
			}
			myIncident.listeIntervention.removeItemAt(indexDel);

			// ajout de la nouvelle intervention (avec les mise a jour)
			myIncident.listeIntervention.addItem((e.obj as InterventionVO));
			lbCout.text=myPriceFormatter.format(myIncident.totalCoutIntervention);
			lbNbIntervention.text=myIncident.nbIntervention.toString();
			interventionCloseIncident((e.obj as InterventionVO));
		}

		/**
		 * Vérification si l'incident doit êre fermé et si l'équipement doit sortir de l'état SAV
		 */
		private function interventionCloseIncident(myIntervention:InterventionVO):void
		{
			if (myIntervention.isClose)
			{
				myIncident.isClose=1;
				myIncident.etat=ResourceManager.getInstance().getString('M111', 'Ferm_');
				btAjouterIntervention.enabled=false;
				btAnnuler.enabled=false;
				btValider.enabled=false;
				btFermer.enabled=true;
			}
			else
			{
				myIncident.isClose=0;
				myIncident.etat=ResourceManager.getInstance().getString('M111', 'Ouvert');
				btAjouterIntervention.enabled=true;
				btAnnuler.enabled=true;
				btValider.enabled=true;
				btFermer.enabled=true;
			}
		}

		/**
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * Ferme la popup courante.
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function getTypeUserTypeProfile():void
		{
			myServiceIncident.getTypeUserTypeProfile();
			myServiceIncident.myDatas.addEventListener(gestionparcEvent.INFOS_TYPE_PROFILE, getTypeUserTypeProfileHandler);
		}

		private function getEmailsRevendeur():void
		{
			myServiceIncident.getEmailsRevendeur(idRevendeurEqpt);
			myServiceIncident.myDatas.addEventListener(gestionparcEvent.LISTE_MAIL_REVENDEUR, getEmailsRevendeurHandler);
		}

		private function setInfosIncident(email:String):void
		{
			myServiceIncident.setInfosIncident(setObj(email));
			myServiceIncident.myDatas.addEventListener(gestionparcEvent.INFOS_INCIDENT_SAVED, infosIncidentSavedHandler);
		}

		private function getTypeUserTypeProfileHandler(e:gestionparcEvent):void
		{
			_idTypeProfil=e.obj.idprofile;

			if (_idTypeProfil == 3 || _idTypeProfil == 1)
				getEmailsRevendeur();
		}

		private function getEmailsRevendeurHandler(e:gestionparcEvent):void
		{
			_listeEmails=e.obj.listeEmail;
		}

		private function popUpEmailsRevendeur():void
		{
			_popUpEmailRevendeur=new PopUpEmailRevendeurIHM();
			_popUpEmailRevendeur.listeEmails=_listeEmails;
			_popUpEmailRevendeur.addEventListener('EMAIL_VALIDATE', popUpMailHandler);

			PopUpManager.addPopUp(_popUpEmailRevendeur, this, true);
			PopUpManager.centerPopUp(_popUpEmailRevendeur);
		}

		private function popUpMailHandler(e:Event):void
		{
			var emailRevendeur:String=_popUpEmailRevendeur.emailSelected.EMAIL;

			PopUpManager.removePopUp(_popUpEmailRevendeur);

			_isModified=false;

			setInfosIncident(emailRevendeur);
		}
	}
}
