package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;

	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.ficheEntity.FicheLigneIHM;
	import gestionparc.ihm.fiches.popup.PopUpAddLineToGrpmentIHM;
	import gestionparc.services.ligne.LigneServices;

	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.core.IFlexDisplayObject;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de l'onglet Canaux/Groupement
	 * pour les lignes fixe de type T2, T0, ANA
	 *
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 25.11.2010
	 *
	 * </pre></p>
	 */
	[Bindable]
	public class ContentCanauxGroupmentImpl extends HBox
	{
		// VARIABLES------------------------------------------------------------------------------

		// IHM components
		public var tiCanauxMixteDebut:TextInput;
		public var tiCanauxMixteFin:TextInput;
		public var tiCanauxEntrantsDebut:TextInput;
		public var tiCanauxEntrantsFin:TextInput;
		public var tiCanauxSortantsDebut:TextInput;
		public var tiCanauxSortantsFin:TextInput;

		public var lbNbCanaux:Label;
		public var lbNbLigneDsGrpment:Label;

		public var rbLigneGrpmentLigneOK:RadioButton;
		public var rbLigneGrpmentLigneKO:RadioButton;

		public var cbxTeteLigne:CheckBox;

		public var btAddLineToGrpment:Button;

		public var dgLigneGrpement:DataGrid;

		public var vbGrid:VBox;

		public var myServiceLigne:LigneServices=null;


		public function ContentCanauxGroupmentImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(event:FlexEvent):void
		{
			//initDisplay();
			initListener();
		}

		private function initListener():void
		{
			btAddLineToGrpment.addEventListener(MouseEvent.CLICK, onBtAddLineToGrpmentClickHandler);
			cbxTeteLigne.addEventListener(MouseEvent.CLICK, onCbxTeteLigneClickHandler);
			rbLigneGrpmentLigneOK.addEventListener(MouseEvent.CLICK, onRbLigneGrpmentLigneOKClickHandler);
			rbLigneGrpmentLigneKO.addEventListener(MouseEvent.CLICK, onRbLigneGrpmentLigneKOClickHandler);

			this.addEventListener(gestionparcEvent.SUPPRIMER_ELT_NAV, ligneDeleteHandler);
			this.addEventListener(gestionparcEvent.OPEN_POPUP_ACTION, onBtAddLineToGrpmentClickHandler);
		}

		private function initDisplay():void
		{
			rbLigneGrpmentLigneOK.selected=false;
			rbLigneGrpmentLigneKO.selected=true;
			vbGrid.visible=false;
			vbGrid.includeInLayout=false;
			cbxTeteLigne.enabled=false;
			dgLigneGrpement.dataProvider=new ArrayCollection();
		}

		private function showPopup(ref:IFlexDisplayObject):void
		{
			PopUpManager.addPopUp(ref, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(ref);
		}

		private function onBtAddLineToGrpmentClickHandler(evt:Event):void
		{
			var popUpAddLineToGrpment:PopUpAddLineToGrpmentIHM=new PopUpAddLineToGrpmentIHM();
			popUpAddLineToGrpment.addEventListener(gestionparcEvent.LINE_GRPMENT_ADDED, onLineGrpmentAddedHandler);
			popUpAddLineToGrpment.myServiceLigne=myServiceLigne;
			if (evt.target.id == "btAddLineToGrpment")
			{
				popUpAddLineToGrpment.isTete=false;
			}
			else
			{
				popUpAddLineToGrpment.isTete=true;
			}
			popUpAddLineToGrpment.lignesDuGrid=(dgLigneGrpement.dataProvider as ArrayCollection);
			PopUpManager.addPopUp(popUpAddLineToGrpment, this.parent.parent, true);
			PopUpManager.centerPopUp(popUpAddLineToGrpment);
		}

		private function onLineGrpmentAddedHandler(evt:gestionparcEvent):void
		{
			if (evt.obj.isTete)
			{
				var lenDataProviderDg:int=(dgLigneGrpement.dataProvider as ArrayCollection).length;

				for (var j:int=0; j < lenDataProviderDg; j++)
				{
					if ((dgLigneGrpement.dataProvider as ArrayCollection)[j].isTete == 1)
					{
						(dgLigneGrpement.dataProvider as ArrayCollection)[j].idTeteLigne=evt.obj.idTeteLigne;
						(dgLigneGrpement.dataProvider as ArrayCollection)[j].numLigne=evt.obj.numLigne;
						(dgLigneGrpement.dataProvider as ArrayCollection).refresh();
						break;
					}
				}
			}
			else
			{
				(dgLigneGrpement.dataProvider as ArrayCollection).addItem(evt.obj);
			}
		}

		private function ligneDeleteHandler(evt:gestionparcEvent):void
		{
			var lenDataProviderDg:int=(dgLigneGrpement.dataProvider as ArrayCollection).length;
			for (var j:int=0; j < lenDataProviderDg; j++)
			{
				if ((dgLigneGrpement.dataProvider as ArrayCollection)[j].idTeteLigne == evt.obj.idTeteLigne)
				{
					(dgLigneGrpement.dataProvider as ArrayCollection).removeItemAt(j);
					break;
				}
			}
		}

		private function onCbxTeteLigneClickHandler(event:MouseEvent):void
		{
			if (cbxTeteLigne.selected)
			{
				caseTeteLigneActivate()
			}
			else
			{
				caseTeteLigneDeActivate();
			}
		}

		private function onRbLigneGrpmentLigneKOClickHandler(event:MouseEvent):void
		{
			if (vbGrid.visible)
			{
				vbGrid.visible=false;
				vbGrid.includeInLayout=false;
				cbxTeteLigne.enabled=false;
			}
		}

		public function setInfos():void
		{
			// affichage des informations
			tiCanauxEntrantsDebut.text=myServiceLigne.myDatas.ligne.canaux_entree_deb.toString();
			tiCanauxEntrantsFin.text=myServiceLigne.myDatas.ligne.canaux_entree_fin.toString();

			tiCanauxMixteDebut.text=myServiceLigne.myDatas.ligne.canaux_mixte_deb.toString();
			tiCanauxMixteFin.text=myServiceLigne.myDatas.ligne.canaux_mixte_fin.toString();

			tiCanauxSortantsDebut.text=myServiceLigne.myDatas.ligne.canaux_sortie_deb.toString();
			tiCanauxSortantsFin.text=myServiceLigne.myDatas.ligne.canaux_sortie_fin.toString();

			lbNbCanaux.text=myServiceLigne.myDatas.ligne.nb_canaux.toString();

			// affichage du groupement
			var tmpDataprovider:ArrayCollection=new ArrayCollection();
			var boolFindTeteLigne:Boolean=false;
			if (myServiceLigne.myDatas.ligne.boolGroupement)
			{
				rbLigneGrpmentLigneOK.selected=true;
				for each (var o:Object in myServiceLigne.myDatas.ligne.listeGroupement)
				{
					if (o.IS_TETE_LIGNE == 1)
					{
						boolFindTeteLigne=true;
						var objTete:Object=new Object();
						objTete.idTeteLigne=o.IDTETE_LIGNE;
						objTete.numLigne=o.SOUS_TETE;
						objTete.isTete=1;
						objTete.typeLigne=ResourceManager.getInstance().getString('M111', 'T_te_de_ligne');
						tmpDataprovider.addItemAt(objTete, 0);
					}
					else
					{
						var objLigneGrp:Object=new Object();
						objLigneGrp.idTeteLigne=o.IDTETE_LIGNE;
						objLigneGrp.numLigne=o.SOUS_TETE;
						objLigneGrp.isTete=0;
						objLigneGrp.typeLigne=o.LIBELLE_TYPE_LIGNE;
						tmpDataprovider.addItem(objLigneGrp);
					}
				}

				if (!boolFindTeteLigne) // on verifie, car normalement la tete_ligne n'est pas dans le dataset listeGroupement
				{
					var objTete2:Object=new Object();
					objTete2.idTeteLigne=myServiceLigne.myDatas.ligne.idtete_ligne;
					objTete2.numLigne=myServiceLigne.myDatas.ligne.tete_ligne;
					objTete2.isTete=1;
					objTete2.typeLigne=ResourceManager.getInstance().getString('M111', 'T_te_de_ligne');
					tmpDataprovider.addItemAt(objTete2, 0);
				}

				dgLigneGrpement.dataProvider=tmpDataprovider;

				if (myServiceLigne.myDatas.ligne.boolTeteLigne)
				{
					cbxTeteLigne.selected=true;
					onCbxTeteLigneClickHandler(null);
				}

				onRbLigneGrpmentLigneOKClickHandler(null);
			}
			else
			{
				initDisplay()
			}
		}

		private function majInfosLigneVo():void
		{
			(this.parent.parent as FicheLigneIHM).refreshInfoLigne(null);
		}

		private function onRbLigneGrpmentLigneOKClickHandler(event:MouseEvent):void
		{
			if (!vbGrid.visible)
			{
				vbGrid.visible=true;
				vbGrid.includeInLayout=true;
			}
			cbxTeteLigne.enabled=true;
			caseTeteLigneDeActivate()
		}

		private function caseTeteLigneActivate():void
		{
			var lenDataProviderDg:int=(dgLigneGrpement.dataProvider as ArrayCollection).length;
			var boolFindTeteLigne:Boolean=false;
			for (var j:int=0; j < lenDataProviderDg; j++)
			{
				if ((dgLigneGrpement.dataProvider as ArrayCollection)[j].isTete == 1)
				{
					boolFindTeteLigne=true;
					(dgLigneGrpement.dataProvider as ArrayCollection)[j].idTeteLigne=myServiceLigne.myDatas.ligne.idSousTete;
					(dgLigneGrpement.dataProvider as ArrayCollection)[j].numLigne=myServiceLigne.myDatas.ligne.sousTete;
					(dgLigneGrpement.dataProvider as ArrayCollection).refresh();
					break;
				}
			}
			if (!boolFindTeteLigne)
			{
				var objTete:Object=new Object();
				objTete.idTeteLigne=myServiceLigne.myDatas.ligne.idSousTete;
				objTete.numLigne=myServiceLigne.myDatas.ligne.sousTete;
				objTete.isTete=1;
				objTete.typeLigne=ResourceManager.getInstance().getString('M111', 'T_te_de_ligne');
				(dgLigneGrpement.dataProvider as ArrayCollection).addItemAt(objTete, 0);
			}
		}

		private function caseTeteLigneDeActivate():void
		{
			if (dgLigneGrpement.dataProvider != null)
			{
				var lenDataProviderDg:int=(dgLigneGrpement.dataProvider as ArrayCollection).length;
				for (var i:int=0; i < lenDataProviderDg; i++)
				{
					if ((dgLigneGrpement.dataProvider as ArrayCollection)[i].idTeteLigne == myServiceLigne.myDatas.ligne.idSousTete)
					{
						(dgLigneGrpement.dataProvider as ArrayCollection)[i].idTeteLigne=myServiceLigne.myDatas.ligne.idtete_ligne;
						(dgLigneGrpement.dataProvider as ArrayCollection)[i].numLigne=myServiceLigne.myDatas.ligne.tete_ligne;
						(dgLigneGrpement.dataProvider as ArrayCollection).refresh();
						break;
					}
				}
			}

		}
	}
}
