package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import gestionparc.services.ligne.LigneServices;

	import mx.containers.HBox;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de l'onglet cablage/SDA
	 * pour les lignes fixe de type Xdsl
	 *
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 23.11.2010
	 *
	 * </pre></p>
	 */
	[Bindable]
	public class ContentCablageSdaTypeXdslImpl extends HBox
	{
		public var tiElementActifEmissionA:TextInput;
		public var tiElementActifReceptionA:TextInput;
		public var tiAmorceTeteEmissionA:TextInput;
		public var tiAmorceTeteReceptionA:TextInput;
		public var tiAdresseA:TextInput;
		public var tiCodePostalA:TextInput;
		public var tiCommuneA:TextInput;
		public var tiIdentifiantLLA:TextInput;
		public var tiNature:TextInput;
		public var tiDebit:TextInput;
		public var tiDistance:TextInput;
		public var tiElementActifA:TextInput;

		public var myServiceLigne:LigneServices;

		public function ContentCablageSdaTypeXdslImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(event:FlexEvent):void
		{
		}

		public function setInfos():void
		{
			tiElementActifEmissionA.text=(myServiceLigne.myDatas.ligne.PtAElemActifAmorceEmission) ? myServiceLigne.myDatas.ligne.PtAElemActifAmorceEmission.toString() : "";
			tiElementActifReceptionA.text=(myServiceLigne.myDatas.ligne.PtAElemActifAmorceReception) ? myServiceLigne.myDatas.ligne.PtAElemActifAmorceReception.toString() : "";
			tiAmorceTeteEmissionA.text=(myServiceLigne.myDatas.ligne.PtAAmorceTeteEmission) ? myServiceLigne.myDatas.ligne.PtAAmorceTeteEmission.toString() : "";
			tiAmorceTeteReceptionA.text=(myServiceLigne.myDatas.ligne.PtAAmorceTeteReception) ? myServiceLigne.myDatas.ligne.PtAAmorceTeteReception.toString() : "";
			tiAdresseA.text=(myServiceLigne.myDatas.ligne.PtAAdresse) ? myServiceLigne.myDatas.ligne.PtAAdresse.toString() : "";
			tiCodePostalA.text=(myServiceLigne.myDatas.ligne.PtACodePostal) ? myServiceLigne.myDatas.ligne.PtACodePostal.toString() : "";
			tiCommuneA.text=(myServiceLigne.myDatas.ligne.PtACommune) ? myServiceLigne.myDatas.ligne.PtACommune.toString() : "";
			tiElementActifA.text=(myServiceLigne.myDatas.ligne.PtAElemActif) ? myServiceLigne.myDatas.ligne.PtAElemActif.toString() : "";

			tiIdentifiantLLA.text=(myServiceLigne.myDatas.ligne.PtAIdentifiantLL) ? myServiceLigne.myDatas.ligne.PtAIdentifiantLL.toString() : "";

			tiNature.text=(myServiceLigne.myDatas.ligne.Nature) ? myServiceLigne.myDatas.ligne.Nature.toString() : "";
			tiDebit.text=(myServiceLigne.myDatas.ligne.Debit) ? myServiceLigne.myDatas.ligne.Debit.toString() : "";
			tiDistance.text=(myServiceLigne.myDatas.ligne.Distance) ? myServiceLigne.myDatas.ligne.Distance.toString() : "";
		}
	}
}
