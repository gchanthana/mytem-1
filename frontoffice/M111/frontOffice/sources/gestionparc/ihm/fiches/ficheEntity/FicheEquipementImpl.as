package gestionparc.ihm.fiches.ficheEntity
{
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.CommandeVO;
	import gestionparc.entity.ContratVO;
	import gestionparc.entity.EmplacementVO;
	import gestionparc.entity.IncidentVO;
	import gestionparc.entity.SIMVO;
	import gestionparc.entity.SiteVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.MDMDataEvent;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentGeneralDeviceIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentShowDeviceIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentShowSoftwareIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentSiteFicheEquipementIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.TabModelIHM;
	import gestionparc.ihm.fiches.importmasse.ChoixModeleHorsCatalogueIHM;
	import gestionparc.ihm.fiches.popup.PopUpNewModeleIHM;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.MDM_BOOL_ISMANAGED;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.MDM_ENABLE;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.MDM_USER_RIGHT;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.equipement.EquipementServices;
	import gestionparc.services.incident.IncidentServices;
	import gestionparc.services.mdm.MDMService;
	import gestionparc.services.site.SiteServices;
	import gestionparc.utils.gestionparcMessages;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Form;
	import mx.containers.TabNavigator;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.formatters.CurrencyFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.NumberValidator;
	import mx.validators.StringValidator;
	import mx.validators.Validator;

	/**
	 * Classe concernant toutes les informations de la fiche terminal.
	 */
	[Bindable]
	public class FicheEquipementImpl extends TitleWindowBounds
	{
		public var tiImei:TextInput;
		public var tiNumSerie :TextInput;
		
		public var tiNewImei:TextInput;
		public var tiNumCommande:TextInput;
		public var tiRefLivraison:TextInput;
		public var tiPrix:TextInput;

		public var lbMarque:Label;
		public var lbModele:Label;
		public var lbType:Label;
		public var lbCategorie:Label;
		public var lbNiveau:Label;
		public var lbCurrency:Label;
		public var lbDistributeur:Label;
		public var lbNomPrenomCollab:Label;
		public var lbMatriculeCollab:Label;

		public var cbMarque:ComboBox;
		public var cbModele:ComboBox;
		public var cbCategorie:ComboBox;
		public var cbType:ComboBox;
		
		public var cbxByod:CheckBox;

		public var taCommentaires:TextArea;

		public var valImei:StringValidator;
		public var valNumSerie: StringValidator;
		public var valNewImei:StringValidator;
		public var valPrix:NumberValidator;
		public var valDateAchat:Validator;
		public var valDateLivraison:Validator;

		public var dcDateAchat:CvDateChooser;
		public var dcDateLivraison:CvDateChooser;

		public var tncLigneSIM:TabModelIHM;
		public var tncIncident:TabModelIHM;
		public var tncContrat:TabModelIHM;
		public var tncSousEquipement:TabModelIHM;
		public var tncSite:ContentSiteFicheEquipementIHM;
		public var tncGeneral:ContentGeneralDeviceIHM;
		public var tncSoft:ContentShowSoftwareIHM;
		public var tncDevice:ContentShowDeviceIHM;

		public var tnTerminal:TabNavigator;

		public var btnValid:Button;
		public var btnCancel:Button;
		public var btModifModele:Button;
		public var btNewModele:Button;
		public var btNewCommande:Button;

		public var vbCommande:VBox;
		public var vbNewCommande:VBox;

		public var frCollab:Form;
		public var frShowEqp:Form;
		public var frNewEqp:Form;

		// publics variables 
		public var idTerminal:int=0;
		public var idRow:Number=-1;
		public var matriculeCollab:String="";
		public var nomPrenomCollab:String="";
		public var hasCommande:Boolean=false;
		public var myServiceTerminal:EquipementServices=new EquipementServices();
		public var myServicesCache:CacheServices=CacheServices.getInstance();//=new CacheServices();
		public var zenPrise:MDMService=new MDMService();

		public var colonnesSousEquipement:ArrayCollection;
		public var colonnesIncidents:ArrayCollection;
		public var colonnesLignes:ArrayCollection;
		public var colonnesContrat:ArrayCollection;
		// privates variables
		private var modelAdded:Object=null;
		private var myServicesSite:SiteServices=new SiteServices();

		private var boolSite:Boolean=false;
		private var boolLigneSim:Boolean=false;
		private var boolSousEqp:Boolean=false;
		private var boolIncident:Boolean=false;
		private var boolContrat:Boolean=false;
		private var boolDevice:Boolean=false;
		private var boolSoft:Boolean=false;
		private var boolGeneral:Boolean=false;

		public function FicheEquipementImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		protected function formaterColonneDates(item:Object, column:DataGridColumn):String
		{
			if (item != null && item[column.dataField] != null && item[column.dataField] != "")
			{
				return DateFunction.formatDateAsString(item[column.dataField]);
			}
			else
			{
				return "";
			}
		}

		protected function formateColonnneEuros(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{
				return ConsoviewFormatter.formatEuroCurrency(item[column.dataField], 2);
			}
			else
			{
				return "";
			}
		}

		protected function formaterColonneBoolean(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{
				var value:Number=Number(item[column.dataField]);

				if (value == 1 || value == true)
				{
					return ResourceManager.getInstance().getString('M111', 'Oui')
				}
				else
				{
					return ResourceManager.getInstance().getString('M111', 'Non')
				}

			}
			else
			{
				return "";
			}
		}

		private function init(event:FlexEvent):void
		{
			initListener();
			createOnglet();
			
			/** initData();
			initMDM();*/
		}

		private function initData():void
		{
			/** Récupération des données Fiche Equipement */ 
			var objToServiceGet:Object=new Object();
			objToServiceGet.idTerminal=idTerminal;
			objToServiceGet.idRow=idRow;
			objToServiceGet.segment=(SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEMOBILE) ? 1 : 2;
			myServiceTerminal.getInfosFicheTerminal(objToServiceGet);
		}

		private function initListener():void
		{
			// listeners lié aux boutons de FicheDetailleeNouvelleLigneIHM
			this.addEventListener(CloseEvent.CLOSE, closePopup);
			this.addEventListener(gestionparcEvent.TERM_UPDATE_LIST_LINE_SIM, onTermUpdateListLineSimHandler);

			btnCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btnValid.addEventListener(MouseEvent.CLICK, validClickHandler);

			// listeners liés à une nouvelle commande
			btNewCommande.addEventListener(MouseEvent.CLICK, clickSetInfosCommandeHandler);

			// listeners liés au modèle
			btModifModele.addEventListener(MouseEvent.CLICK, clickModifierModeleHandler);
			btNewModele.addEventListener(MouseEvent.CLICK, clickNewModeleHandler);
			cbModele.addEventListener(ListEvent.CHANGE, onChangeModeleHandler);

			// listeners liés aux services
			myServiceTerminal.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_TERM_LOADED, infoFicheEqptLoadedHandler);
			myServiceTerminal.myDatas.addEventListener(gestionparcEvent.LISTE_MODELES_LOADED, listeModelesLoadedHandler);
		}

		private function initDisplay():void
		{
			// tant que tous les champs requit ne sont pas renseignés ou qu'aucune modif n'a été réalisée
			btnValid.enabled=true;

			lbNomPrenomCollab.text=nomPrenomCollab;
			lbMatriculeCollab.text=matriculeCollab;

			// la commande
			if (hasCommande)
			{
				vbCommande.visible=true;
				vbCommande.includeInLayout=true;
				vbNewCommande.visible=false;
				vbNewCommande.includeInLayout=false;
			}
			else
			{
				vbCommande.visible=false;
				vbCommande.includeInLayout=false;
				vbNewCommande.visible=true;
				vbNewCommande.includeInLayout=true;
			}

			frShowEqp.visible=true;
			frNewEqp.visible=false;
			lbMarque.visible=true;
			lbModele.visible=true;
			btModifModele.visible=true;
			cbMarque.visible=false;
			cbModele.visible=false;
			btNewModele.visible=false;
			frCollab.visible=true;
		}

		private function initDisplayOnglet():void
		{
			tncSite.myServicesCache=myServicesCache;
		}

		private function tncSiteComplete(evt:FlexEvent):void
		{
			boolSite=true;
			initIHM()
		}

		private function tncLigneSimComplete(evt:FlexEvent):void
		{
			boolLigneSim=true;
			initIHM()
		}

		private function tncContratComplete(evt:FlexEvent):void
		{
			boolContrat=true;
			initIHM()
		}

		private function tncSousEqpComplete(evt:FlexEvent):void
		{
			boolSousEqp=true;
			initIHM()
		}

		private function tncIncidentComplete(evt:FlexEvent):void
		{
			boolIncident=true;
			initIHM()
		}

		private function tncDeviceComplete(evt:FlexEvent):void
		{
			boolDevice=true;
			initIHM()
		}

		private function tncSoftComplete(evt:FlexEvent):void
		{
			boolSoft=true;
			initIHM()
		}

		private function tncGeneralComplete(evt:FlexEvent):void
		{
			boolGeneral=true;
			initIHM()
		}

		private function initIHM():void
		{
			if (boolSite && boolLigneSim && boolContrat && boolSousEqp && boolIncident && boolDevice && boolSoft && boolGeneral)
			{
				boolSite=false;
				boolLigneSim=false;
				boolContrat=false;
				boolSousEqp=false;
				boolIncident=false;
				boolDevice=false;
				boolSoft=false;
				boolGeneral=false;

				initDisplayOnglet();
				initData();
			}
		}

		private function createOngletSite():ContentSiteFicheEquipementIHM
		{
			tncSite=new ContentSiteFicheEquipementIHM();
			tncSite.label=resourceManager.getString('M111', 'Site');
			tncSite.percentHeight=100;
			tncSite.percentWidth=100;

			tncSite.addEventListener(FlexEvent.CREATION_COMPLETE, tncSiteComplete);

			return tncSite;
		}

		private function createOngletLigneSim():TabModelIHM
		{
			tncLigneSIM=new TabModelIHM();

			if (SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEMOBILE)
			{
				tncLigneSIM.typeAjoutColonnes="collab";
				tncLigneSIM.labelTypeOfEdit=resourceManager.getString('M111', 'Editer');
				if (SpecificationVO.getInstance().elementDataGridSelected.S_IMEI !=null)
				{
					tncLigneSIM.label=resourceManager.getString('M111', 'Lignes_SIM');
				}
				else if (SpecificationVO.getInstance().elementDataGridSelected.EX_NUMERO !=null)
				{
					tncLigneSIM.label=resourceManager.getString('M111', 'Lignes_EXT');
				}
				else
				{
					tncLigneSIM.label=resourceManager.getString('M111', 'Lignes_SIM'); // par defaut
				}
				tncLigneSIM.visible=true;
			}
			else if (SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEFIXE)
			{
				tncLigneSIM.typeAjoutColonnes="";
				tncLigneSIM.labelTypeOfEdit="";
				tncLigneSIM.label=resourceManager.getString('M111', 'Lignes');
				tncLigneSIM.visible=true;
				tncLigneSIM.includeInLayout=tncLigneSIM.visible;
			}

			tncLigneSIM.labelTypeOfDelete="";
			tncLigneSIM.colonnes=colonnesLignes;
			tncLigneSIM.datagridHeight=180;
			tncLigneSIM.percentWidth=100;
			tncLigneSIM.percentHeight=100;
			tncLigneSIM.buttonActionVisible=false;
			tncLigneSIM.includeInLayout=tncLigneSIM.visible;


			tncLigneSIM.addEventListener(FlexEvent.CREATION_COMPLETE, tncLigneSimComplete);
			tncLigneSIM.addEventListener(gestionparcEvent.EDITER_ELT_NAV, onEditerLigneSimHandler);

			return tncLigneSIM;
		}

		private function createOngletContrat():TabModelIHM
		{
			tncContrat=new TabModelIHM();
			tncContrat.label=resourceManager.getString('M111', 'Contrats_de_service');
			tncContrat.labelTypeOfEdit=resourceManager.getString('M111', 'Editer');
			tncContrat.labelTypeOfDelete=resourceManager.getString('M111', 'Supprimer');
			tncContrat.colSupVisible=true;
			tncContrat.buttonActionLabel=resourceManager.getString('M111', 'Ajouter_contrat');
			tncContrat.typeAjoutColonnes="collab";
			tncContrat.colonnes=colonnesContrat;
			tncContrat.datagridHeight=180;
			tncContrat.percentHeight=100;
			tncContrat.percentWidth=100;
			tncContrat.buttonActionVisible=true;

			tncContrat.addEventListener(FlexEvent.CREATION_COMPLETE, tncContratComplete);
			tncContrat.addEventListener(gestionparcEvent.AJOUTER_ELT_NAV, onAjouterContratHandler);
			tncContrat.addEventListener(gestionparcEvent.EDITER_ELT_NAV, onEditerContratHandler);
			tncContrat.addEventListener(gestionparcEvent.SUPPRIMER_ELT_NAV, contratDeleteHandler);

			return tncContrat;
		}

		private function createOngletSousEqp():TabModelIHM
		{
			tncSousEquipement=new TabModelIHM();
			tncSousEquipement.label=resourceManager.getString('M111', 'Sous_Equipement');
			tncSousEquipement.labelTypeOfEdit="";
			tncSousEquipement.labelTypeOfDelete="";
			tncSousEquipement.typeAjoutColonnes="none";
			tncSousEquipement.colonnes=colonnesSousEquipement;
			tncSousEquipement.datagridHeight=180;
			tncSousEquipement.percentHeight=100;
			tncSousEquipement.percentWidth=100;
			tncSousEquipement.buttonActionVisible=false;

			tncSousEquipement.addEventListener(FlexEvent.CREATION_COMPLETE, tncSousEqpComplete);

			return tncSousEquipement;
		}

		private function createOngletIncident():TabModelIHM
		{
			tncIncident=new TabModelIHM();
			tncIncident.label=resourceManager.getString('M111', 'Incidents');
			tncIncident.labelTypeOfEdit=resourceManager.getString('M111', 'Editer');
			tncIncident.labelTypeOfDelete=resourceManager.getString('M111', 'Supprimer');
			tncIncident.colSupVisible=true;
			tncIncident.buttonActionLabel=resourceManager.getString('M111', 'Nouvel_incident');
			tncIncident.typeAjoutColonnes="collab";
			tncIncident.colonnes=colonnesIncidents;
			tncIncident.datagridHeight=180;
			tncIncident.percentHeight=100;
			tncIncident.percentWidth=100;
			tncIncident.buttonActionVisible=true;

			tncIncident.addEventListener(FlexEvent.CREATION_COMPLETE, tncIncidentComplete);
			tncIncident.addEventListener(gestionparcEvent.AJOUTER_ELT_NAV, onAjouterIncidentHandler);
			tncIncident.addEventListener(gestionparcEvent.EDITER_ELT_NAV, onEditerIncidentHandler);
			tncIncident.addEventListener(gestionparcEvent.SUPPRIMER_ELT_NAV, incidentDeleteHandler);

			return tncIncident;
		}

		private function createOngletDevice():ContentShowDeviceIHM
		{
			tncDevice = new ContentShowDeviceIHM();
			tncDevice.label = resourceManager.getString('M111', 'Equipement');
			tncDevice.zenService = this.zenPrise; //this.zenprise is a MDMService()
			tncDevice.percentWidth = 100;
			tncDevice.percentHeight = 100;

			tncDevice.addEventListener(FlexEvent.CREATION_COMPLETE, tncDeviceComplete);

			return tncDevice;
		}

		private function createOngletSoft():ContentShowSoftwareIHM
		{
			tncSoft = new ContentShowSoftwareIHM();
			tncSoft.label = resourceManager.getString('M111', 'Logiciel');
			tncSoft.zenService = this.zenPrise; //this.zenprise is a MDMService()
			tncSoft.percentWidth = 100;
			tncSoft.percentHeight = 100;

			tncSoft.addEventListener(FlexEvent.CREATION_COMPLETE, tncSoftComplete);

			return tncSoft;
		}

		private function createOngletGeneral():ContentGeneralDeviceIHM
		{
			tncGeneral = new ContentGeneralDeviceIHM();
			tncGeneral.label = resourceManager.getString('M111', 'General');
			tncGeneral.zenService = this.zenPrise; //this.zenprise is a MDMService()
			tncGeneral.percentWidth = 100;
			tncGeneral.percentHeight = 100;

			tncGeneral.addEventListener(FlexEvent.CREATION_COMPLETE, tncGeneralComplete);

			return tncGeneral;
		}

		private function createOnglet():void
		{
			if (SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEMOBILE)
			{
				addOngletMobile();
			}
			else
			{
				addOngletFixe();
			}
		}

		// Ajout des onglets de la fiche mobile
		private function addOngletMobile():void
		{
			var r1:IRuleDisplayItem=new MDM_ENABLE();
			var r2:IRuleDisplayItem=new MDM_USER_RIGHT();
			var r3:IRuleDisplayItem=new MDM_BOOL_ISMANAGED();

			tnTerminal.addChild(createOngletSite());
			tnTerminal.addChild(createOngletLigneSim());
			tnTerminal.addChild(createOngletContrat());
			tnTerminal.addChild(createOngletSousEqp());
			tnTerminal.addChild(createOngletIncident());

			boolDevice=true;
			boolGeneral=true;
			boolSoft=true;

			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			if (r1.isDisplay() && r2.isDisplay() && (ligne != null) && (!isNaN(ligne.T_IDTYPE_EQUIP)) && ((ligne.T_IDTYPE_EQUIP == 70) || (ligne.T_IDTYPE_EQUIP == 2192)))
			{
				// Quand MDMDataEvent.IS_MANAGED_RESULT <=> Statut du device retourné dans le FrontOffice
				zenPrise.myDatas.addEventListener(MDMDataEvent.IS_MANAGED_RESULT, onDeviceStatusResult);
				/* Récupération du statut du device pour savoir si on affiche les onglets MDM ou non
				Dispatche MDMDataEvent.IS_MANAGED_RESULT quand le résultat sera retourné dans le FrontOffice
				*/
				zenPrise.isManaged(ligne.ZDM_SERIAL_NUMBER, ligne.T_IMEI);
			}
			else
				initIHM();
		}

		// Quand MDMDataEvent.IS_MANAGED_RESULT est dispatché : Ajout des onglets MDM (Si le statut du device est géré)
		private function onDeviceStatusResult(event:MDMDataEvent):void
		{
			// Supprime le listener sur MDMDataEvent.IS_MANAGED_RESULT car initMDM() appelle indirectement isManaged()
			zenPrise.myDatas.removeEventListener(MDMDataEvent.IS_MANAGED_RESULT, onDeviceStatusResult);
			initMDM(); // Appelle getDeviceInfo() qui appelle isManaged() => Dispatch MDMDataEvent.IS_MANAGED_RESULT
		}

		private function initMDM():void
		{
			var r1:IRuleDisplayItem = new MDM_ENABLE();
			var r2:IRuleDisplayItem = new MDM_USER_RIGHT();
			var r3:IRuleDisplayItem = new MDM_BOOL_ISMANAGED();

			var ligne:AbstractMatriceParcVO = SpecificationVO.getInstance().elementDataGridSelected;
			try
			{
				if (r1.isDisplay() && r2.isDisplay() && (ligne != null) && (!isNaN(ligne.T_IDTYPE_EQUIP)) && ((ligne.T_IDTYPE_EQUIP == 70) || (ligne.T_IDTYPE_EQUIP == 2192)))
				{
					if (ligne.IS_LAST_OP_FAULT)
					{ // Erreur MDM
						title = ResourceManager.getInstance().getString('M111', 'Fiche_d_taill_e__quipement') + " - " + ResourceManager.getInstance().getString('M111', 'mdm_fiche_is_last_op_fault');
					}
					else
					{ // Pas d'erreur MDM
						title = ResourceManager.getInstance().getString('M111', 'Fiche_d_taill_e__quipement');
						if (r3.isDisplay())
						{ // Enrollé ou Revoké
							tnTerminal.addChild(createOngletDevice());
							tnTerminal.addChild(createOngletSoft());
							tnTerminal.addChild(createOngletGeneral());
							zenPrise.getDeviceInfo(ligne.ZDM_SERIAL_NUMBER, ligne.T_IMEI);
						}
					}
				}
				else
				{
					boolDevice = true;
					boolSoft = true;
					boolGeneral = true;
					initIHM();
				}
			}
			catch (evt:ErrorEvent)
			{
			}
		}

		private function addOngletFixe():void
		{
			tnTerminal.addChild(createOngletSite());
			tnTerminal.addChild(createOngletContrat());
			tnTerminal.addChild(createOngletLigneSim());
			tnTerminal.addChild(createOngletSousEqp());
			tnTerminal.addChild(createOngletIncident());

			boolDevice = true;
			boolGeneral = true;
			boolLigneSim = true;
			boolSoft = true;
		}

		private function infoFicheEqptLoadedHandler(event:gestionparcEvent):void
		{
			myServiceTerminal.myDatas.terminal.idEquipement = SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;

			tiImei.text = myServiceTerminal.myDatas.terminal.numIemiEquipement;
			tiNumSerie.text = myServiceTerminal.myDatas.terminal.numSerieEquipement;
				
			tiImei.maxChars = (myServiceTerminal.myDatas.terminal.idTypeEquipement == 70) ? 15 : 50;
			lbMarque.text = myServiceTerminal.myDatas.terminal.nomFabricant;
			lbModele.text = myServiceTerminal.myDatas.terminal.libelleEquipement;
			lbCategorie.text = myServiceTerminal.myDatas.terminal.categorie;
			lbType.text = myServiceTerminal.myDatas.terminal.typeEquipement;
			lbNiveau.text = (myServiceTerminal.myDatas.terminal.niveau != -1) ? myServiceTerminal.myDatas.terminal.niveau.toString() : "";
			taCommentaires.text = myServiceTerminal.myDatas.terminal.commentaires;
			cbxByod.selected = (myServiceTerminal.myDatas.terminal.byod == 1);
			if (myServiceTerminal.myDatas.terminal.commande)
			{
				hasCommande = true;
				dcDateAchat.selectedDate = myServiceTerminal.myDatas.terminal.commande.dateAchat;
				dcDateLivraison.selectedDate = myServiceTerminal.myDatas.terminal.commande.dateLivraison;
				tiNumCommande.text = myServiceTerminal.myDatas.terminal.commande.numCommande;
				tiRefLivraison.text = myServiceTerminal.myDatas.terminal.commande.refLivraison;
				lbDistributeur.text = myServiceTerminal.myDatas.terminal.nomRevendeur;
				
				cbxByod.selected = (myServiceTerminal.myDatas.terminal.byod == 1);
				
				// pour mettre le prix avec le bon format
				var myFormatterPrix:CurrencyFormatter = new CurrencyFormatter();
				myFormatterPrix.precision = 2;
				myFormatterPrix.currencySymbol = "";
				myFormatterPrix.thousandsSeparatorTo = ResourceManager.getInstance().getString('M111', 'formateur_thousandsSeparatorTo');
				myFormatterPrix.decimalSeparatorTo = ResourceManager.getInstance().getString('M111', 'formateur_decimalSeparatorTo')

				tiPrix.text = myFormatterPrix.format(myServiceTerminal.myDatas.terminal.commande.prix);
			}

			// maj onglet SITE 
			if (tncSite != null && tncSite.cbSite != null)
			{
				if (myServiceTerminal.myDatas.terminal.idSite > 0)
				{
					var objToGetEmplacement:Object = new Object();
					objToGetEmplacement.idSite = myServiceTerminal.myDatas.terminal.idSite;
					myServicesSite.getEmplacement(objToGetEmplacement);
					myServicesSite.myDatas.addEventListener(gestionparcEvent.INFOS_EMPLA_SITE_LOADED, onInfosEmplaSiteLoadedHandler);
					var longueurTabSite:int = tncSite.cbSite.dataProvider.length;
					for each (var item:Object in tncSite.cbSite.dataProvider)
					{
						if (item.idSite == myServiceTerminal.myDatas.terminal.idSite)
						{
							tncSite.cbSite.selectedItem = item;
							tncSite.txtAdresse.text = item.adresse1;
							tncSite.lbCodePostal.text = item.codePostale;
							tncSite.lbVille.text = item.ville;
							tncSite.lbPays.text = item.pays;
							break;
						}
					}
				}
				else
				{
					tncSite.cbSite.selectedIndex = -1;
				}
			}

			// maj onglet LIGNE/SIM
			if (tncLigneSIM != null && tncLigneSIM.dataDatagrid != null)
			{
				tncLigneSIM.dataDatagrid = myServiceTerminal.myDatas.terminal.listeLigneSim;
			}

			// maj onglet INCIDENT
			if (tncIncident != null && tncIncident.dataDatagrid != null)
			{
				tncIncident.dataDatagrid = myServiceTerminal.myDatas.terminal.listeIncident;
				activeButtonNewIncident();
			}

			// maj onglet CONTRAT
			if (tncContrat != null && tncContrat.dataDatagrid != null)
			{
				tncContrat.dataDatagrid = myServiceTerminal.myDatas.terminal.listeContrat;
				initDisplay();
			}
			// maj onglet SOUS EQUIPEMENT
			if (tncSousEquipement != null && tncSousEquipement.dataDatagrid != null)
			{
				tncSousEquipement.dataDatagrid = myServiceTerminal.myDatas.terminal.listeSousEquipement;
			}
		}

		private function activeButtonNewIncident():void
		{
			var lenListeIncident:int = myServiceTerminal.myDatas.terminal.listeIncident.length;
			var isActive:Boolean = true;

			for (var i:int=0; i < lenListeIncident; i++)
			{
				if ((myServiceTerminal.myDatas.terminal.listeIncident[i] as IncidentVO).isClose == 0)
				{
					isActive=false;
					break;
				}
			}

			if (isActive)
			{
				tncIncident.buttonAction.enabled=true;
			}
			else
			{
				tncIncident.buttonAction.enabled=false;
			}
		}

		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function validClickHandler(event:MouseEvent):void
		{
			// test les validators
			if (runValidators())
			{
				myServiceTerminal.setInfosFicheTerminal(createObjToServiceSet());
				myServiceTerminal.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_TERM_UPLOADED, endUpdateEqpt);
			}
		}

		private function createObjToServiceSet():Object
		{
			var obj:Object=new Object();
			obj.isNotSameImei=0;
			obj.isNotSameNumSerie=0;
			
			obj.ID=SpecificationVO.getInstance().elementDataGridSelected.ID;
			
			if (myServiceTerminal.myDatas.terminal.numIemiEquipement != tiImei.text)
			{
				obj.isNotSameImei=1;
			}
			if (myServiceTerminal.myDatas.terminal.numSerieEquipement != tiNumSerie.text)
			{
				obj.isNotSameNumSerie=1;
			}
			setInfoEquipementVO();
			obj.myTerminal=myServiceTerminal.myDatas.terminal;
			return obj;
		}

		private function endUpdateEqpt(event:gestionparcEvent):void
		{
			SpecificationVO.getInstance().refreshData();
			closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_TERM_UPLOADED));
		}

		private function setInfoEquipementVO():void
		{
			var prix:Number=parseFloat(tiPrix.text);
			myServiceTerminal.myDatas.terminal.numIemiEquipement=tiImei.text;
			myServiceTerminal.myDatas.terminal.numSerieEquipement=tiNumSerie.text;
			
			myServiceTerminal.myDatas.terminal.commentaires=taCommentaires.text;
			myServiceTerminal.myDatas.terminal.byod = (cbxByod.selected) ? 1 : 0;

			if (hasCommande)
			{
				if (myServiceTerminal.myDatas.terminal.commande.dateAchat && dcDateAchat.selectedDate && myServiceTerminal.myDatas.terminal.commande.dateAchat.getTime() == dcDateAchat.selectedDate.getTime() && myServiceTerminal.myDatas.terminal.commande.prix == prix && myServiceTerminal.myDatas.terminal.commande.numCommande == tiNumCommande.text && myServiceTerminal.myDatas.terminal.commande.dateLivraison && dcDateLivraison.selectedDate && myServiceTerminal.myDatas.terminal.commande.dateLivraison.getTime() == dcDateLivraison.selectedDate.getTime() && myServiceTerminal.myDatas.terminal.commande.refLivraison == tiRefLivraison.text)
				{
					myServiceTerminal.myDatas.terminal.commande.idCommande=0; // les info de la commande sont inchangées
				}
				else
				{
					myServiceTerminal.myDatas.terminal.commande.dateAchat=(dcDateAchat.selectedDate) ? dcDateAchat.selectedDate : null;
					myServiceTerminal.myDatas.terminal.commande.prix=prix;
					myServiceTerminal.myDatas.terminal.commande.numCommande=tiNumCommande.text;
					myServiceTerminal.myDatas.terminal.commande.dateLivraison=(dcDateLivraison.selectedDate) ? dcDateLivraison.selectedDate : null;
					myServiceTerminal.myDatas.terminal.commande.refLivraison=tiRefLivraison.text;
				}
			}
			else
			{
				myServiceTerminal.myDatas.terminal.commande=new CommandeVO();
			}

			if (tncSite.cbSite.selectedItem)
			{
				myServiceTerminal.myDatas.terminal.idSite=(tncSite.cbSite.selectedItem as SiteVO).idSite;
			}
			else
			{
				myServiceTerminal.myDatas.terminal.idSite=-1; // pas de site
			}

			if (tncSite.cbEmplacement.selectedItem != null )
			{
				myServiceTerminal.myDatas.terminal.idEmplacement=(tncSite.cbEmplacement.selectedItem as EmplacementVO).idEmplcament;
			}
			else
			{
				myServiceTerminal.myDatas.terminal.idEmplacement=-1; // pas d'emplacement
			}
		}

		private function setInfoNewEquipementVO():void
		{
			var prix:Number=parseFloat(tiPrix.text);
			myServiceTerminal.myDatas.terminal.numIemiEquipement=tiNewImei.text;
			myServiceTerminal.myDatas.terminal.numSerieEquipement=tiNumSerie.text;
			myServiceTerminal.myDatas.terminal.commentaires=taCommentaires.text;

			if (hasCommande)
			{
				if (myServiceTerminal.myDatas.terminal.commande.dateAchat && dcDateAchat.selectedDate && myServiceTerminal.myDatas.terminal.commande.dateAchat.getTime() == dcDateAchat.selectedDate.getTime() && myServiceTerminal.myDatas.terminal.commande.prix == prix && myServiceTerminal.myDatas.terminal.commande.numCommande == tiNumCommande.text && myServiceTerminal.myDatas.terminal.commande.dateLivraison && dcDateLivraison.selectedDate && myServiceTerminal.myDatas.terminal.commande.dateLivraison.getTime() == dcDateLivraison.selectedDate.getTime() && myServiceTerminal.myDatas.terminal.commande.refLivraison == tiRefLivraison.text)
				{
					myServiceTerminal.myDatas.terminal.commande.idCommande=0; // les info de la commande sont inchangées
				}
				else
				{
					myServiceTerminal.myDatas.terminal.commande.dateAchat=(dcDateAchat.selectedDate) ? dcDateAchat.selectedDate : null;
					myServiceTerminal.myDatas.terminal.commande.prix=prix;
					myServiceTerminal.myDatas.terminal.commande.numCommande=tiNumCommande.text;
					myServiceTerminal.myDatas.terminal.commande.dateLivraison=(dcDateLivraison.selectedDate) ? dcDateLivraison.selectedDate : null;
					myServiceTerminal.myDatas.terminal.commande.refLivraison=tiRefLivraison.text;
				}
			}
			else
			{
				myServiceTerminal.myDatas.terminal.commande=new CommandeVO();
			}

			if (tncSite.cbSite.selectedItem)
			{
				myServiceTerminal.myDatas.terminal.idSite=(tncSite.cbSite.selectedItem as SiteVO).idSite;
			}
			else
			{
				myServiceTerminal.myDatas.terminal.idSite=-1; // pas de site
			}

			if (tncSite.cbEmplacement.selectedItem)
			{
				myServiceTerminal.myDatas.terminal.idEmplacement=(tncSite.cbEmplacement.selectedItem as EmplacementVO).idEmplcament;
			}
			else
			{
				myServiceTerminal.myDatas.terminal.idEmplacement=-1; // pas d'emplacement
			}
		}

		private function clickModifierModeleHandler(event:MouseEvent):void
		{
			var popUpModele:ChoixModeleHorsCatalogueIHM=new ChoixModeleHorsCatalogueIHM();
			popUpModele.oldMarque=lbMarque.text;
			popUpModele.oldModele=lbModele.text;
			popUpModele.idRevendeur=myServiceTerminal.myDatas.terminal.idRevendeur;
			popUpModele.idType=myServiceTerminal.myDatas.terminal.idTypeEquipement;
			popUpModele.addEventListener(gestionparcEvent.NEW_MODELE_ADDED, choiceModeleDidHandler);
			PopUpManager.addPopUp(popUpModele, this, true);
			PopUpManager.centerPopUp(popUpModele);
		}

		private function clickNewModeleHandler(event:MouseEvent):void
		{
			var popUpAddModele:PopUpNewModeleIHM=new PopUpNewModeleIHM();
			popUpAddModele.myServicesCache=myServicesCache;
			popUpAddModele.addEventListener(gestionparcEvent.NEW_MODELE_ADDED, newModeleAddedHandler);
			PopUpManager.addPopUp(popUpAddModele, this, true);
			PopUpManager.centerPopUp(popUpAddModele);
		}

		private function newModeleAddedHandler(evt:gestionparcEvent):void
		{
			cbModele.visible=false;
			cbModele.includeInLayout=false;

			lbModele.visible=true;
			lbModele.includeInLayout=true;
			lbModele.text=evt.obj.modele;
			lbCategorie.text=evt.obj.categorie;
			lbType.text=evt.obj.type;

			// on instancie et met a jour le modele ajouté pour la validation
			modelAdded=new Object();
			modelAdded=evt.obj;
		}

		private function choiceModeleDidHandler(evt:gestionparcEvent):void
		{
			// MARQUE
			myServiceTerminal.myDatas.terminal.nomFabricant=evt.obj.marque;
			myServiceTerminal.myDatas.terminal.idFabricant=evt.obj.idMarque;
			// MODELE
			myServiceTerminal.myDatas.terminal.idEqptFournis=evt.obj.idModele;
			myServiceTerminal.myDatas.terminal.libEqptFournis=evt.obj.modele;
			//Distributeur
			myServiceTerminal.myDatas.terminal.idRevendeur=evt.obj.idRevendeur;
			// TYPE
			myServiceTerminal.myDatas.terminal.idTypeEquipement=evt.obj.idType;

			lbMarque.text=evt.obj.marque;
			lbModele.text=evt.obj.modele;
		}

		private function clickSetInfosCommandeHandler(evt:MouseEvent):void
		{
			var ficheInfosCommande:FicheInfosCommandeEquipementIHM=new FicheInfosCommandeEquipementIHM();
			ficheInfosCommande.addEventListener(gestionparcEvent.UPLOAD_INFOS_CMD, uploadInfosCmdHandler);
			ficheInfosCommande.myEqpt=myServiceTerminal.myDatas.terminal;
			PopUpManager.addPopUp(ficheInfosCommande, this, true);
			PopUpManager.centerPopUp(ficheInfosCommande);
		}

		private function uploadInfosCmdHandler(evt:gestionparcEvent):void
		{
			if (evt.obj)
			{
				vbCommande.visible=true;
				vbCommande.includeInLayout=true;
				vbNewCommande.visible=false;
				vbNewCommande.includeInLayout=false;

				dcDateAchat.selectedDate=(evt.obj as CommandeVO).dateAchat;
				tiPrix.text=(evt.obj as CommandeVO).prix.toString();
				tiNumCommande.text=(evt.obj as CommandeVO).numCommande;
				dcDateLivraison.selectedDate=(evt.obj as CommandeVO).dateLivraison;
				tiRefLivraison.text=(evt.obj as CommandeVO).refLivraison;
				lbDistributeur.text=(evt.obj as CommandeVO).distributeur;
			}
		}

		private function onAjouterContratHandler(event:gestionparcEvent):void
		{
			var ficheAddContratService:FicheContratServiceIHM=new FicheContratServiceIHM();

			ficheAddContratService.isAddContrat=true;
			ficheAddContratService.boolSaveDirectly=true;
			ficheAddContratService.myServiceTerminal=myServiceTerminal;
			ficheAddContratService.myServiceCache=myServicesCache;
			ficheAddContratService.myContrat=new ContratVO();

			ficheAddContratService.addEventListener(gestionparcEvent.UPDATE_LISTE_CONTRAT, updateAddListeContratHandler);

			PopUpManager.addPopUp(ficheAddContratService, this, true);
			PopUpManager.centerPopUp(ficheAddContratService);
		}

		private function onEditerContratHandler(event:gestionparcEvent):void
		{
			var ficheEditContratService:FicheContratServiceIHM=new FicheContratServiceIHM();

			ficheEditContratService.isAddContrat=false;
			ficheEditContratService.boolSaveDirectly=true;
			ficheEditContratService.myServiceTerminal=myServiceTerminal;
			ficheEditContratService.myServiceCache=myServicesCache;

			// seléction du bon contrat parmis la liste des contrats de cet equipement
			var lenListeContrat:int=myServiceTerminal.myDatas.terminal.listeContrat.length;
			for (var i:int=0; i < lenListeContrat; i++)
			{
				if ((myServiceTerminal.myDatas.terminal.listeContrat[i] as ContratVO).idContrat == (tncContrat.ligneDgSelected as ContratVO).idContrat)
				{
					ficheEditContratService.myContrat=(myServiceTerminal.myDatas.terminal.listeContrat[i] as ContratVO);
					break;
				}
			}

			ficheEditContratService.addEventListener(gestionparcEvent.UPDATE_LISTE_CONTRAT, updateModifListeContratHandler);

			PopUpManager.addPopUp(ficheEditContratService, this, true);
			PopUpManager.centerPopUp(ficheEditContratService);
		}

		private function onAjouterIncidentHandler(event:gestionparcEvent):void
		{
			if (tncIncident.buttonAction.enabled)
			{
				var ficheAddIncident:FicheIncidentIHM=new FicheIncidentIHM();

				ficheAddIncident.isAddIncident=true;
				ficheAddIncident.myIncident=new IncidentVO();
				ficheAddIncident.idEqpt=idTerminal;
				ficheAddIncident.libelleEqpt=myServiceTerminal.myDatas.terminal.numSerieEquipement;
				ficheAddIncident.idRevendeurEqpt=myServiceTerminal.myDatas.terminal.revendeur.idRevendeur;
				ficheAddIncident.revendeurEqpt=myServiceTerminal.myDatas.terminal.revendeur.nom;
				ficheAddIncident.dateFinGarantieEqpt=myServiceTerminal.myDatas.terminal.dateFinGarantie;
				ficheAddIncident.listIncidentEqpt=myServiceTerminal.myDatas.terminal.listeIncident;
				ficheAddIncident.myServiceCache=myServicesCache;

				ficheAddIncident.addEventListener(gestionparcEvent.UPDATE_LISTE_INCIDENT, updateAddListeIncidentHandler);

				PopUpManager.addPopUp(ficheAddIncident, this, true);
				PopUpManager.centerPopUp(ficheAddIncident);
			}
		}

		private function onEditerIncidentHandler(event:gestionparcEvent):void
		{
			var ficheEditIncident:FicheIncidentIHM=new FicheIncidentIHM();

			ficheEditIncident.isAddIncident=false;
			ficheEditIncident.idEqpt=idTerminal;
			ficheEditIncident.libelleEqpt=myServiceTerminal.myDatas.terminal.numSerieEquipement;
			ficheEditIncident.idRevendeurEqpt=myServiceTerminal.myDatas.terminal.revendeur.idRevendeur;
			ficheEditIncident.revendeurEqpt=myServiceTerminal.myDatas.terminal.revendeur.nom;
			ficheEditIncident.dateFinGarantieEqpt=myServiceTerminal.myDatas.terminal.dateFinGarantie;
			ficheEditIncident.listIncidentEqpt=myServiceTerminal.myDatas.terminal.listeIncident;
			ficheEditIncident.myServiceCache=myServicesCache;

			// seléction du bon incident parmis la liste des incidents de cet equipement
			var lenListeIncident:int=myServiceTerminal.myDatas.terminal.listeIncident.length;
			for (var j:int=0; j < lenListeIncident; j++)
			{
				if ((myServiceTerminal.myDatas.terminal.listeIncident[j] as IncidentVO).idIncident == (tncIncident.ligneDgSelected as IncidentVO).idIncident)
				{
					ficheEditIncident.myIncident=(myServiceTerminal.myDatas.terminal.listeIncident[j] as IncidentVO);
					break;
				}
			}

			ficheEditIncident.addEventListener(gestionparcEvent.UPDATE_LISTE_INCIDENT, updateModifListeIncidentHandler);

			PopUpManager.addPopUp(ficheEditIncident, this, true);
			PopUpManager.centerPopUp(ficheEditIncident);
		}

		private function onEditerLigneSimHandler(event:gestionparcEvent):void
		{
			var ficheLigne:FicheLigneIHM=new FicheLigneIHM();
			ficheLigne.idsoustete=(tncLigneSIM.selectedItem as SIMVO).ligneRattache.idSousTete;
			ficheLigne.idRow=(tncLigneSIM.selectedItem as SIMVO).idRow > 0 ? (tncLigneSIM.selectedItem as SIMVO).idRow : -1;
			ficheLigne.addEventListener(gestionparcEvent.INFOS_FICHE_LIGNE_UPLOADED, onTermUpdateListLineSimHandler, false, 0, true);
			PopUpManager.addPopUp(ficheLigne, parent, true);
			PopUpManager.centerPopUp(ficheLigne);
		}

		private function updateAddListeContratHandler(evt:gestionparcEvent):void
		{
			myServiceTerminal.myDatas.terminal.listeContrat.addItem((evt.obj as ContratVO));
		}

		private function updateModifListeContratHandler(evt:gestionparcEvent):void
		{
			// seléction du bon contrat parmis la liste des contrats de cet equipement et suppression
			var lenListeContrat:int=myServiceTerminal.myDatas.terminal.listeContrat.length;
			for (var j:int=0; j < lenListeContrat; j++)
			{
				if ((myServiceTerminal.myDatas.terminal.listeContrat[j] as ContratVO).idContrat == (evt.obj as ContratVO).idContrat)
				{
					myServiceTerminal.myDatas.terminal.listeContrat.removeItemAt(j);
					break;
				}
			}
			// ajout du nouveau contrat (avec les mise a jour)
			myServiceTerminal.myDatas.terminal.listeContrat.addItem((evt.obj as ContratVO));
		}

		private function contratDeleteHandler(evt:gestionparcEvent):void
		{
			// seléction du bon incident parmis la liste des incidents de cet equipement et suppression
			var lenListeContrat:int=myServiceTerminal.myDatas.terminal.listeContrat.length;
			for (var j:int=0; j < lenListeContrat; j++)
			{
				if ((myServiceTerminal.myDatas.terminal.listeContrat[j] as ContratVO).idContrat == (evt.obj as ContratVO).idContrat)
				{
					myServiceTerminal.deleteContrat((myServiceTerminal.myDatas.terminal.listeContrat[j] as ContratVO));
					myServiceTerminal.myDatas.terminal.listeContrat.removeItemAt(j);
					break;
				}
			}
		}

		private function updateAddListeIncidentHandler(evt:gestionparcEvent):void
		{
			myServiceTerminal.myDatas.terminal.listeIncident.addItem((evt.obj as IncidentVO));
			activeButtonNewIncident();
		}

		private function updateModifListeIncidentHandler(evt:gestionparcEvent):void
		{
			// seléction du bon incident parmis la liste des incidents de cet equipement et suppression
			var lenListeIncident:int=myServiceTerminal.myDatas.terminal.listeIncident.length;
			for (var j:int=0; j < lenListeIncident; j++)
			{
				if ((myServiceTerminal.myDatas.terminal.listeIncident[j] as IncidentVO).idIncident == (evt.obj as IncidentVO).idIncident)
				{
					myServiceTerminal.myDatas.terminal.listeIncident.removeItemAt(j);
					break;
				}
			}
			// ajout du nouvel incident (avec les mise a jour)
			myServiceTerminal.myDatas.terminal.listeIncident.addItem((evt.obj as IncidentVO));
			activeButtonNewIncident();
		}

		private function incidentDeleteHandler(evt:gestionparcEvent):void
		{
			var lenListeIncident:int=myServiceTerminal.myDatas.terminal.listeIncident.length;
			var myServiceIncident:IncidentServices=new IncidentServices();
			var putActif:Boolean=true;

			for (var j:int=0; j < lenListeIncident; j++)
			{
				if ((myServiceTerminal.myDatas.terminal.listeIncident[j] as IncidentVO).idIncident != (evt.obj as IncidentVO).idIncident && (myServiceTerminal.myDatas.terminal.listeIncident[j] as IncidentVO).isClose == 0)
				{
					putActif=false;
					break;
				}
			}

			for (var i:int=0; i < lenListeIncident; i++)
			{
				if ((myServiceTerminal.myDatas.terminal.listeIncident[i] as IncidentVO).idIncident == (evt.obj as IncidentVO).idIncident)
				{
					if (putActif)
					{
						myServiceIncident.deleteIncident(idTerminal, 1, (myServiceTerminal.myDatas.terminal.listeIncident[i] as IncidentVO).idIncident);
					}
					else
					{
						myServiceIncident.deleteIncident(idTerminal, 0, (myServiceTerminal.myDatas.terminal.listeIncident[i] as IncidentVO).idIncident);
					}
					myServiceTerminal.myDatas.terminal.listeIncident.removeItemAt(i);
					break;
				}
			}
			activeButtonNewIncident();
		}

		private function onTermUpdateListLineSimHandler(evt:gestionparcEvent):void
		{
			initData();
		}

		private function onListLinkLineSimLoadedHandler(event:gestionparcEvent):void
		{
			tncLigneSIM.dataDatagrid=myServiceTerminal.myDatas.terminal.listeLigneSim;
		}

		private function listeModelesLoadedHandler(event:gestionparcEvent):void
		{
			cbModele.dataProvider=myServiceTerminal.myDatas.listeModele;
			if (cbModele.selectedItem)
			{
				putCategoryAndType(cbModele.selectedItem);
			}
		}

		private function putCategoryAndType(obj:Object):void
		{
			if (obj)
			{
				lbCategorie.text=obj.CATEGORIE_EQUIPEMENT;
				lbType.text=obj.TYPE_EQUIPEMENT;
			}
		}

		private function onChangeModeleHandler(event:ListEvent):void
		{
			if (cbModele.selectedItem)
			{
				putCategoryAndType(cbModele.selectedItem);
			}
		}

		private function onInfosEmplaSiteLoadedHandler(event:gestionparcEvent):void
		{
			myServicesSite.myDatas.removeEventListener(gestionparcEvent.INFOS_EMPLA_SITE_LOADED, onInfosEmplaSiteLoadedHandler);
			tncSite.cbEmplacement.dataProvider=new ArrayCollection();
			tncSite.cbEmplacement.dataProvider=myServicesSite.myDatas.listEmplacement;

			if (myServiceTerminal.myDatas.terminal.idEmplacement > 0)
			{
				var longueurTabEmplacement:int=myServicesSite.myDatas.listEmplacement.length;
				for (var k:int=0; k < longueurTabEmplacement; k++)
				{
					if ((myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).idEmplcament == myServiceTerminal.myDatas.terminal.idEmplacement)
					{
						tncSite.cbEmplacement.selectedIndex=k;
						tncSite.txtBatiment.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).batiment;
						tncSite.txtEtage.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).etage;
						tncSite.txtSalle.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).salle;
						tncSite.txtCouloir.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).couloir;
						tncSite.txtArmoire.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).armoire;
						break;
					}
				}
			}
			else
			{
				tncSite.cbEmplacement.selectedIndex=-1;
			}
		}

		private function runValidators():Boolean
		{
			var validators:Array=new Array();
			if (hasCommande)
			{
				validators.push(valDateAchat);
				validators.push(valDateLivraison);
				//validators.push(valPrix);
			}
		
			var prixValidator:Array=[valPrix];
			var resultPrix:Array=Validator.validateAll(prixValidator);

			if(tiImei.text == "" && tiNumSerie.text=="")
			{
				ConsoviewAlert.afficherAlertInfo(gestionparcMessages.RENSEIGNER_CHAMPS_OBLI, ResourceManager.getInstance().getString('M111', 'Attention__'),null);
				tiImei.setStyle('borderColor','red');
				tiNumSerie.setStyle('borderColor','red');
				return false;
			}
			else
			{
				if (tiPrix.text != "")
				{
					if (resultPrix.length > 0)
					{
						ConsoviewAlert.afficherAlertInfo(gestionparcMessages.SYNTAXE_INVALIDE, ResourceManager.getInstance().getString('M111', 'Attention__'),null);
						return false;
					}
				}
				return true;
			}
		}

		protected function cbCategorie_changeHandler(event:ListEvent):void
		{
			if (cbCategorie.selectedIndex > -1)
			{
				myServiceTerminal.getListeType(cbCategorie.selectedItem.IDCATEGORIE_EQUIPEMENT);
			}
			refreshListeModele();
		}

		protected function cbType_changeHandler(event:ListEvent):void
		{
			refreshListeModele();
		}

		protected function cbMarque_changeHandler(event:ListEvent):void
		{
			refreshListeModele();
		}

		private function refreshListeModele():void
		{
			var idtype:int=-1;
			var idmarque:int=-1;

			if (cbType.selectedIndex > -1 && cbMarque.selectedIndex > -1)
			{
				idtype=cbType.selectedItem.IDTYPE_EQUIPEMENT;
				idmarque=cbMarque.selectedItem.IDFOURNISSEUR;
				myServiceTerminal.myDatas.addEventListener(gestionparcEvent.LISTE_MODELES_LOADED, modeleLoaded);
				myServiceTerminal.getListeModele(idtype, idmarque);
			}
		}

		private function modeleLoaded(evt:gestionparcEvent):void
		{
			cbModele.dataProvider=myServiceTerminal.myDatas.listeModele;
			cbModele.dropdown.dataProvider=myServiceTerminal.myDatas.listeModele;
		}
	}
}