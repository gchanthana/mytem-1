package gestionparc.ihm.fiches.sim
{
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.sim.SimServices;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DateField;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.TextArea;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class FicheSimMettreAuRebusImpl extends TitleWindow
	{
		public var imgTitre:Image;
		public var libelle_terminal_rebus:Label;
		public var calendar:CvDateChooser;
		public var bt_annuler:Button;
		public var bt_valider:Button;
		public var inputComm:mx.controls.TextArea;
		public var radio_perte:RadioButton;
		public var radio_casse:RadioButton;
		public var radio_vol:RadioButton;
		public var radio_autre:RadioButton;

		public var ligneSelected:AbstractMatriceParcVO;

		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")]
		private var adrImg:Class;
		private var serv:SimServices;

		public function FicheSimMettreAuRebusImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		private function initIHM(evt:FlexEvent):void
		{
			imgTitre.source=adrImg;
			libelle_terminal_rebus.text=ligneSelected.S_IMEI;
			addEventListener(CloseEvent.CLOSE, fermer);
			calendar.selectedDate=new Date();
			bt_annuler.addEventListener(MouseEvent.CLICK, fermer);
			bt_valider.addEventListener(MouseEvent.CLICK, rebusTerminal);
		}

		private function fermer(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function rebusTerminal(evt:MouseEvent):void
		{
			serv=new SimServices();
			serv.myDatas.addEventListener(gestionparcEvent.MISE_AU_REBUT_VALIDATE, rebusTerminal_handler);
			serv.mettreAuRebut(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			if (radio_perte.selected == true)
			{
				obj.ID_CAUSE=1;
			}
			else
			{
				if (radio_casse.selected == true)
				{
					obj.ID_CAUSE=2;
				}
				else
				{
					if (radio_vol.selected == true)
					{
						obj.ID_CAUSE=3;
					}
					else
					{
						if (radio_autre.selected == true)
						{
							obj.ID_CAUSE=0;
						}

					}
				}
			}
			obj.idSim=SpecificationVO.getInstance().elementDataGridSelected.IDSIM;
			obj.commentaire=inputComm.text;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.S_IMEI;
			obj.ITEM2="";
			obj.ID_TERMINAL=0;
			obj.ID_SIM=SpecificationVO.getInstance().elementDataGridSelected.IDSIM;
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(calendar.selectedDate, 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}

		private function rebusTerminal_handler(evt:gestionparcEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Mise_au_rebut_de_la_carte_SIM_effectu_e'), Application.application as DisplayObject);
			fermer(null);
		}
	}
}
