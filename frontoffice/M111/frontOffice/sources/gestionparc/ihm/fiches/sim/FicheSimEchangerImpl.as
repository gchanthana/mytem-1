package gestionparc.ihm.fiches.sim
{
	
	// 176572 250244
	// 9818831178760 654585452145845
	// 
	
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.listedispo.DispoSimMobile;
	import gestionparc.ihm.question.equipement.QuestionFicheSimEchanger;
	import gestionparc.services.sim.SimServices;
	import gestionparc.utils.gestionparcConstantes;
	
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	public class FicheSimEchangerImpl extends TitleWindow
	{
		public var img_cellule:Image;
		public var img_liste:Image;
		public var img_liste_big:Image;
		public var lab_cellule_name:Label;
		public var lab_titre:Label;
		public var labDgSelection:Label;
		public var box_listeDispo:VBox;
		public var calendar:CvDateChooser;
		public var btAnnuler:Button;
		public var btValider:Button;
		
		private var obj_liste:DispoSimMobile;
		private var serv:SimServices;
		private var qn:QuestionFicheSimEchanger;
		
		public function FicheSimEchangerImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM)
		}
		private function initIHM(evt : FlexEvent):void
		{
			serv= new SimServices();
			obj_liste = new DispoSimMobile();
			
			img_cellule.source = gestionparcConstantes.adrImgTerm; //Afficher l'icone de la cellule séléctionné
			img_liste.source=obj_liste.getSourceIcone();//Afficher l'icone de la liste
			img_liste_big.source=obj_liste.getSourceIcone();//Afficher l'icone de la liste
			lab_cellule_name.text = SpecificationVO.getInstance().elementDataGridSelected.S_IMEI;//Affichage du nom de la cellule séléctionné
			lab_titre.text=""+obj_liste.getLibelleType()+"";//Affichage du type d'objet de la liste
			box_listeDispo.addChild(obj_liste as VBox);//Ajout du composant contenant la dg + le filtre
			calendar.selectedDate = new Date(); // Selection de la date actuel dans la date du jour
			labDgSelection.text = obj_liste.getLibelleSelection();
			
			btAnnuler.addEventListener(MouseEvent.CLICK,fermer);
			btValider.addEventListener(MouseEvent.CLICK,valider);
			addEventListener(CloseEvent.CLOSE,fermer);
			
			obj_liste.addEventListener(MouseEvent.CLICK,dgListeDispo_handler);
			addEventListener(MouseEvent.CLICK,dgListeDispo_handler);
		}
		private function dgListeDispo_handler(evt : Event):void
		{
			labDgSelection.text = obj_liste.getLibelleSelection();
		}
		private function valider(evt : MouseEvent):void
		{
			if(obj_liste.getIDSelection() !=-1)
			{
				echangerEqp()
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un__l_ment_dans_la_liste'));
		}
		private function echangerEqp():void
		{
			qn = new QuestionFicheSimEchanger(obj_liste,calendar);
			qn.serv.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED,associationHandler);
		}
		private function associationHandler(evt:gestionparcEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Echange_du_terminal_effectu_'),Application.application as DisplayObject);
			fermer(null);
		}
		private function fermer(evt : Event):void 
		{
			PopUpManager.removePopUp(this);
		}
	}
}