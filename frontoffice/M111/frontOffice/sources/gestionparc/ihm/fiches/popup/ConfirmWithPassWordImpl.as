package gestionparc.ihm.fiches.popup
{
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;

	import gestionparc.event.ConfirmWithPassWordIHMEvent;
	import gestionparc.event.UserAccessModelEvent;
	import gestionparc.services.useracess.UserAccessService;

	import mx.containers.TitleWindow;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;


	public class ConfirmWithPassWordImpl extends TitleWindow
	{
		//comp
		[Bindable]
		public var txtPwd:TextInput;


		protected const msgString:String=ResourceManager.getInstance().getString('M111', 'le_mot_de_passe_saisi_est_incorrect');
		protected const requiredFieldError:String=ResourceManager.getInstance().getString('M111', 'veuillez_saisir_votre_mot_dpasse_d_acc_s');
		protected const msgTitre:String=ResourceManager.getInstance().getString('M111', 'Pour_effectuer_cette_action__veuillez_sa');
		protected const lblBtValider:String=ResourceManager.getInstance().getString('M111', 'confirmer');
		protected const lblBtAnnuler:String=ResourceManager.getInstance().getString('M111', 'Annuler');

		[Bindable]public var titre:String=ResourceManager.getInstance().getString('M111', 'G_rer_le_terminal');

		[Bindable]
		public var msgVisble:Boolean=false;

		private var userAccessService:UserAccessService;
		private var _init:Boolean=false;

		[Bindable]protected var password:String='';

		protected var _listnerFunction:Function;



		public function ConfirmWithPassWordImpl()
		{
			super();
			userAccessService=new UserAccessService();
		}

		//// public function ////////////////////////////////////////////////////////////////////////////////////////////////
		public function init(titre:String, listnerFunction:Function):void
		{
			msgVisble=false;
			password="";
			_listnerFunction=listnerFunction;
			titre=titre;
			_init=true;
		}

		///// protectd function /////////////////////////////////////////////////////////////////////////////////////////////
		protected function creationCompleteHandler(event:FlexEvent):void
		{
			if (!_init)
			{
				throw new Error('la methode init doit être appelée');
			}
		}

		protected function closeHandler(event:CloseEvent):void
		{
			closeWindow();
		}


		protected function btValiderClickHandler(event:MouseEvent):void
		{
			password=txtPwd.text;
			userAccessService.checkPassWord(password);
			userAccessService.model.addEventListener(UserAccessModelEvent.UAME_CHECK_PASSWORD_UPDATED, checkPassWordHandler);
		}

		protected function txtPwdKeyboardHandler(ke:KeyboardEvent):void
		{
			if ((ke as KeyboardEvent).keyCode == 13)
				btValiderClickHandler(null);
		}

		private function checkPassWordHandler(event:UserAccessModelEvent):void
		{
			if (userAccessService.model.checkPassWordLastResult > 0)
			{
				_listnerFunction(new ConfirmWithPassWordIHMEvent(ConfirmWithPassWordIHMEvent.CWPWIE_CONFIRM_EVENT, false, false, ConfirmWithPassWordIHMEvent.PASS_KO));
			}
			else
			{
				msgVisble=true;
			}
		}


		protected function btAnnulerClickHandler(event:MouseEvent):void
		{
			closeWindow();
		}


		/////  private fiunctions //////////////////////////////////////////////////////////////////////////////////////////
		private function closeWindow():void
		{
			PopUpManager.removePopUp(this);
		}

	}
}
