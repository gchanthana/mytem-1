package gestionparc.ihm.fiches.popup
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.Validator;

	[Bindable]
	public class PopUpSimImpl extends TitleWindow
	{

		public var tiNumSim:TextInput;
		public var tiPin1:TextInput;
		public var tiPin2:TextInput;
		public var tiPuk1:TextInput;
		public var tiPuk2:TextInput;

		public var btValider:Button;
		public var btAnnuler:Button;

		public var numeroValidator:Validator;

		public var carteSim:Object;


		public function PopUpSimImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			if (carteSim)
			{
				tiNumSim.text=carteSim.numSerieEquipement;
				tiPin1.text=carteSim.PIN1;
				tiPin2.text=carteSim.PIN2;
				tiPuk1.text=carteSim.PUK1;
				tiPuk2.text=carteSim.PUK2;
			}
			initListener();
		}

		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btAnnuler.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValider.addEventListener(MouseEvent.CLICK, validerHandler);
		}

		private function cancelPopup(evt:MouseEvent):void
		{
			closePopup(null);
		}

		private function closePopup(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function validerHandler(evt:MouseEvent):void
		{
			// test les validators
			if (runValidators())
			{
				var obj:Object=new Object();
				obj.numSim=tiNumSim.text;
				obj.PIN1=tiPin1.text;
				obj.PIN2=tiPin2.text;
				obj.PUK1=tiPuk1.text;
				obj.PUK2=tiPuk2.text;
				dispatchEvent(new gestionparcEvent(gestionparcEvent.POPUP_SIM_VALIDATED, obj));
				closePopup(null);
			}
		}

		private function runValidators():Boolean
		{
			var _validators:Array=[numeroValidator];
			var results:Array=Validator.validateAll(_validators);

			if (results.length > 0)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M111', 'Veuillez_renseigner_les_champs_obligatoi'), ResourceManager.getInstance().getString('M111', 'Attention__'),null);
				return false;
			}
			else
			{
				return true;
			}
		}
	}
}
