package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	import gestionparc.event.gestionparcEvent;
	
	public class PopupChoosePoolImpl extends TitleWindow
	{
		//--------------- VARIABLES ----------------//
		[Bindable] 
		public var rbgPool				:RadioButtonGroup;
		[Bindable] 
		public var itemCurrentLabel		:Label;
		public var dgChoosePool			:DataGrid;
		public var tiFiltre				:TextInput;
		public var btnValider			:Button;
		public var btnAnnuler			:Button;
		[Bindable]
		public var btnValidEnabled		:Boolean = false;
		
		private var _itemCurrent		:Object = null;
		private var _selectedIndex		:Number;
		private var _listePool 			:ArrayCollection;
		private var _itemSelectedFirst 	: Object = null;
		
		//--------------- METHODES ----------------//
		
		public function PopupChoosePoolImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		/* initialisation */
		private function init(fe:FlexEvent):void
		{
			initData();
			initListeners();
		}
		
		private function initData():void
		{
			listePool.filterFunction = filtrerPool;
			listePool.refresh();
			
			
			// initialisation de l'affichage du label du pool
			afficherInitialPoolLabel();
			initCurseur();
		}
		
		private function initCurseur():void
		{
			callLater(tiFiltre.setFocus);
		}
		
		// initialisation de l'affichage du label du pool
		private function afficherInitialPoolLabel():void
		{
			if(listePool != null)
			{
				var longPool:int = listePool.length;
				for (var i:int = 0; i < longPool; i++) 
				{
					if(listePool[i].SELECTED)
					{
						itemCurrent = listePool[i];
						_itemSelectedFirst = listePool[i]; 
						break;
					}
				}
			}
			itemCurrentLabel.text = getItemCurrentLabel();
		}
		
		private function initListeners():void
		{
			tiFiltre.addEventListener(Event.CHANGE,saisieFiltreHandler);
			btnValider.addEventListener(MouseEvent.CLICK,validerChooseRacineHandler);
			addEventListener(KeyboardEvent.KEY_DOWN,validerChooseRacineHandler2);
			btnAnnuler.addEventListener(MouseEvent.CLICK,onCloseHandler);
			dgChoosePool.addEventListener(ListEvent.CHANGE,currentItemHandler);
			
		}
		
		/* */
		protected function currentItemHandler(le:ListEvent):void
		{
			itemCurrent = le.currentTarget.selectedItem;
			var i:int = 0;
			while (i < listePool.length)
			{
				if(itemCurrent!=listePool[i])
					listePool[i].SELECTED = false;
				else
				{
					listePool[i].SELECTED = true;
//					_selectedIndex = i;
				}
				listePool.itemUpdated(listePool[i]);
				i++;
			}
			
			itemCurrentLabel.text = getItemCurrentLabel();
			btnValidEnabled = true;
		}
		
		/* lors de la saisie du filtre */
		private function saisieFiltreHandler(e:Event):void
		{
			listePool.filterFunction = filtrerPool;
			listePool.refresh();
		}
		
		/* systeme de filtre */
		private function filtrerPool(item:Object):Boolean
		{
			if(dgChoosePool.selectedIndex)
				rbgPool.selection = rbgPool.getRadioButtonAt(dgChoosePool.selectedIndex + 1);
			
			if (item.LIBELLE_POOL != null && (item.LIBELLE_POOL as String).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				return true;
			else
				return false;
		}
		
		/**
		 * Retourne le libelle racine approprié peuplant la combobox :
		 */
		public function labelPoolFunction(item:Object, column:DataGridColumn):String
		{
			return item.LIBELLE_POOL;
		}
		
		protected function onCloseHandler(event:Event):void
		{
			tiFiltre.text = '';
			tiFiltre.dispatchEvent(new Event(Event.CHANGE));
			this.initPopUp();
			PopUpManager.removePopUp(this);
		}
		
		// initialise la popup vers le choix du pool initial
		private function initPopUp():void
		{
			for (var i:int = 0; i < listePool.length; i++) 
			{
				listePool[i].SELECTED = false;
				if((_itemSelectedFirst != null)&& (listePool[i].IDPOOL == _itemSelectedFirst.IDPOOL))
				{
					listePool[i].SELECTED = true;
					listePool.itemUpdated(listePool[i]);
					break;
				}
				
			}
				
			itemCurrentLabel.text = getItemCurrentLabel();
		}
		
		/* au click sur valider */
		private function validerChooseRacineHandler(evt:MouseEvent):void
		{
			validerChoix();
		}
		
		private function validerChoix():void
		{
			dispatchEvent(new gestionparcEvent(gestionparcEvent.VALID_CHOIX_POOL,true));
			onCloseHandler(null);
			
		}
		
		private function validerChooseRacineHandler2(evt:KeyboardEvent):void
		{
			if(evt.keyCode == Keyboard.ENTER)
			{
				validerChoix();
			}
		}
		
		//--------------- GETETRS - SETTERS ----------------//
		[Bindable]
		public function get listePool():ArrayCollection
		{
			return _listePool;
		}
		public function set listePool(value:ArrayCollection):void
		{
			_listePool = value;
		}
		
		public function get itemCurrent():Object
		{
			return _itemCurrent;
		}
		
		public function set itemCurrent(value:Object):void
		{
			_itemCurrent = value;
		}
		
		public function getItemCurrentLabel():String
		{
			if (_itemCurrent != null)
				return _itemCurrent.LIBELLE_POOL;
			else
				return '';
		}
	}
}