package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.ligne.LigneServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TabNavigator;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;

	[Bindable]
	public class PopUpAboOptImpl extends TitleWindow
	{

		public var btValider:Button;
		public var btAnnuler:Button;

		public var idOperateur:Number;
		public var idProfilEquipement:Number;
		public var isAbonnement:Boolean;
		public var isFixe:Number=0;
		public var isMobile:Number=1;

		public var myServices:LigneServices;
		public var popupOpt:PopUpChoiceSubscriptionIHM;
		public var popupAbo:PopUpChoiceSubscriptionIHM;
		public var tnChoice:TabNavigator;

		public function PopUpAboOptImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		public function getSelectedAboOpt():ArrayCollection
		{
			var tmpArray:ArrayCollection=new ArrayCollection();
			tmpArray.addAll(popupAbo.getSelectedAboOpt());
			tmpArray.addAll(popupOpt.getSelectedAboOpt());
			return tmpArray;
		}

		private function init(evt:FlexEvent):void
		{
			myServices.fournirListeAbonnementsOptions(idOperateur, idProfilEquipement, isMobile, isFixe);
			initListener();
		}

		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btAnnuler.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValider.addEventListener(MouseEvent.CLICK, validerHandler);
		}

		private function cancelPopup(evt:MouseEvent):void
		{
			closePopup(null);
		}

		private function closePopup(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function validerHandler(evt:MouseEvent):void
		{
			dispatchEvent(new gestionparcEvent(gestionparcEvent.POPUP_ABO_OPT_VALIDATED));
			closePopup(null);
		}
	}
}
