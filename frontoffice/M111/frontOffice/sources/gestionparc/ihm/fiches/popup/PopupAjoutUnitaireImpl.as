package gestionparc.ihm.fiches.popup
{
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.ficheEntity.FicheAjoutEquipementIHM;
	import gestionparc.ihm.fiches.ficheEntity.FicheAjoutLigneIHM;
	import gestionparc.ihm.fiches.ficheEntity.FicheCollaborateurIHM;
	
	import mx.containers.Canvas;
	import mx.containers.TitleWindow;
	import mx.controls.Image;
	import mx.core.Application;
	import mx.core.IFlexDisplayObject;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class PopupAjoutUnitaireImpl extends TitleWindowBounds
	{

		public var imgEqp:Image;
		public var imgCollab:Image;
		public var imgLigne:Image;

		public var cvs0:Canvas;
		public var cvs1:Canvas;
		public var cvs2:Canvas;

		[Embed(source='/assets/commande/blue_background_90x90.png', mimeType='image/png')]
		[Bindable]
		public var img_Default:Class;

		[Embed(source='/assets/commande/green_background_90x90.png', mimeType='image/png')]
		[Bindable]
		public var imgSelected_Default:Class; //---LORS DU PASSAGE SUR L'IMAGE


		[Embed(source='/assets/commande/blue_background_90x90.png', mimeType='image/png')]
		[Bindable]
		public var imgDisable:Class; //---DISABLE

		public function PopupAjoutUnitaireImpl()
		{
			super();
			this.addEventListener(CloseEvent.CLOSE, close);
		}

		protected function close(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(evt.target as IFlexDisplayObject);
		}

		protected function canevasClickHandler(btnNumber:int):void
		{
			switch (btnNumber)
			{
				case 0:
					showNewCollab()
					break;
				case 1:
					showNewEqp()
					break;
				case 2:
					showNewLigne()
					break;
			}
		}

		public function showNewCollab():void
		{
			if ((SpecificationVO.getInstance().idPoolNotValid > 0 && !SpecificationVO.getInstance().idPool) || SpecificationVO.getInstance().idPool > 0)
			{
				this.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
				var fiche:FicheCollaborateurIHM=new FicheCollaborateurIHM();
				fiche.isNewCollabFiche=true;
				PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
				PopUpManager.centerPopUp(fiche);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_un_pool_de_gesti'), ResourceManager.getInstance().getString('M111', 'Attention__'));

			}
		}

		public function showNewEqp():void
		{
			if ((SpecificationVO.getInstance().idPoolNotValid > 0 && !SpecificationVO.getInstance().idPool) || SpecificationVO.getInstance().idPool > 0)
			{

				this.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
				var fiche:FicheAjoutEquipementIHM=new FicheAjoutEquipementIHM();
				PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
				PopUpManager.centerPopUp(fiche);

			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_un_pool_de_gesti'), ResourceManager.getInstance().getString('M111', 'Attention__'));
			}
		}

		public function showNewLigne():void
		{
			if ((SpecificationVO.getInstance().idPoolNotValid > 0 && !SpecificationVO.getInstance().idPool) || SpecificationVO.getInstance().idPool > 0)
			{
				this.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
				var fiche:FicheAjoutLigneIHM=new FicheAjoutLigneIHM();
				PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
				PopUpManager.centerPopUp(fiche);

			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_un_pool_de_gesti'), ResourceManager.getInstance().getString('M111', 'Attention__'));
			}
		}

		protected function rollOverHandler(btnNumber:int):void
		{
			switch (btnNumber)
			{
				case 0:
					cvs0.setStyle("backgroundImage", imgSelected_Default);
					break;
				case 1:
					cvs1.setStyle("backgroundImage", imgSelected_Default);
					break;
				case 2:
					cvs2.setStyle("backgroundImage", imgSelected_Default);
					break;
			}
		}

		protected function rollOutHandler(btnNumber:int):void
		{
			switch (btnNumber)
			{
				case 0:
					cvs0.setStyle("backgroundImage", img_Default);
					break;
				case 1:
					cvs1.setStyle("backgroundImage", img_Default);
					break;
				case 2:
					cvs2.setStyle("backgroundImage", img_Default);
					break;
			}
		}

		private function addPopup(fiche:TitleWindow):void
		{
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
