package gestionparc.ihm.fiches.popup
{
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.EquipementVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant l'ajout de nouveaux modeles
	 * 
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 06.10.2010
	 * 
	 * </pre></p>
	 * 
	 */
	[Bindable]
	public class PopUpNewModeleImpl extends TitleWindow
	{
		public var tiModele					:TextInput;
		
		public var cbCategorie				:ComboBox;
		public var cbType					:ComboBox;
		public var cbRevendeur				:ComboBox;
		
		public var btnValid					:Button;
		public var btnCancel				:Button;
		
		public var myServicesCache			:CacheServices;
				
		public function PopUpNewModeleImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/**
		 * Initialisation générale de cette classe  :
		 * - Appel les services <code>getCategoriesEquipement</code> et <code>getTypesEquipement</code>
		 * si les data listCategoriesEquipement et listTypesEquipement n'ont pas deja ete remplis.
		 * - Appel des fonctions <code>initListener()</code>
		 */
		private function init(event:FlexEvent):void
		{
			if(CacheDatas.listCategoriesEquipement)
			{
				onListCategoriesEqptLoadedHandler(null);
			}
			else
			{
				myServicesCache.getCategoriesEquipement();
			}
			
			if(CacheDatas.listTypesEquipement)
			{
				onListTypesEqptLoadedHandler(null);
			}
			else
			{
				myServicesCache.getTypesEquipement();
			}
			
			if(CacheDatas.listeRevendeur)
			{
				onListeRevendeurLoadedHandler(null);
			}
			else
			{
				myServicesCache.getListeRevendeur();
			}
			
			initListener();
		}
		
		/**
		 * Initialise les écouteurs.
		 */	
		private function initListener():void
		{
			// listeners lié aux boutons de FicheDetailleeNouvelleLigneIHM
			this.addEventListener(CloseEvent.CLOSE,closePopup);
			
			btnCancel.addEventListener(MouseEvent.CLICK,cancelPopup);
			btnValid.addEventListener(MouseEvent.CLICK,validClickHandler);
			
			// listeners liés aux services
			myServicesCache.myDatas.addEventListener(gestionparcEvent.LIST_CATEGORIES_EQPT_LOADED,onListCategoriesEqptLoadedHandler);
			myServicesCache.myDatas.addEventListener(gestionparcEvent.LIST_TYPES_EQPT_LOADED,onListTypesEqptLoadedHandler);
			myServicesCache.myDatas.addEventListener(gestionparcEvent.LISTE_REVENDEUR_LOADED,onListeRevendeurLoadedHandler);
		}

		
		/**
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}
		
		/**
		 * Ferme la popup courante.
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		/**
		 * Valide la popup permettant l'ajout d'un equipement
		 */			
		private function validClickHandler(event:MouseEvent):void
		{
			var objResult : Object = new Object();
			objResult.modele = tiModele.text;
			objResult.categorie = cbCategorie.selectedItem.VALUE;
			objResult.idCategorie = cbCategorie.selectedItem.IDCATEGORIE_EQUIPEMENT;
			objResult.type = cbType.selectedItem.VALUE;
			objResult.idType = cbType.selectedItem.IDTYPE_EQUIPEMENT;
			objResult.idRevendeur = cbRevendeur.selectedItem.IDREVENDEUR;
			objResult.libelleRevendeur = cbRevendeur.selectedItem.LIBELLE;
			
			dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_MODELE_ADDED,objResult));
			closePopup(null);
		}
		
		/**
		 * Met a jour la comboBox "cbCategorie"
		 */			
		private function onListCategoriesEqptLoadedHandler(event:gestionparcEvent):void
		{
			cbCategorie.dataProvider = CacheDatas.listCategoriesEquipement;
			cbCategorie.selectedIndex = ConsoviewUtil.getIndexById(CacheDatas.listCategoriesEquipement,"IDCATEGORIE_EQUIPEMENT",EquipementVO.CATEGORIE_TERMINAUX);
			cbCategorie.dispatchEvent(new Event(Event.CHANGE));
		}
		
		/**
		 * Met a jour la comboBox "cbType"
		 */			
		private function onListTypesEqptLoadedHandler(event:gestionparcEvent):void
		{
			cbType.dataProvider = CacheDatas.listTypesEquipement;
			cbType.selectedIndex = ConsoviewUtil.getIndexById(CacheDatas.listTypesEquipement,"IDTYPE_EQUIPEMENT",EquipementVO.TYPE_MOBILE);
			cbType.dispatchEvent(new Event(Event.CHANGE));
		}
		
		/**
		 * Met a jour la comboBox "cbRevendeur"
		 */			
		private function onListeRevendeurLoadedHandler(event:gestionparcEvent):void
		{
			cbRevendeur.dataProvider = CacheDatas.listeRevendeur;
		}
	}
}