package gestionparc.ihm.fiches.popup
{
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpUsageImpl extends TitleWindow
	{
		
		public var btValider 	:Button;
		public var btAnnuler 	:Button;
		
		public var tiNewUsage	:TextInput;
		
		public var oldUsage		:String = "";
		
		public function PopUpUsageImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
				
		/**
		 * Initialisation générale de cette classe  :
		 * - Appel des fonctions <code>initListener()</code>
		 */
		private function init(event:FlexEvent):void
		{
			initListener();
			initDisplay();
		}
		
		/**
		 * Initialise les écouteurs.
		 */	
		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE,closePopup);
			btValider.addEventListener(MouseEvent.CLICK,btValiderHandler);
			btAnnuler.addEventListener(MouseEvent.CLICK,cancelPopup);
		}
		
		/**
		 * Initialise l'affichage de cette IHM.
		 */	
		private function initDisplay():void
		{
			tiNewUsage.text = oldUsage;
		}
		
		/**
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 * 
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * Ferme la popup courante.
		 * 
		 */
		private function closePopup(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		/**
		 * Valide la pop up usage. 
		 * Dispatch un evenement avec la nouveau usage a ajouter.
		 */	
		private function btValiderHandler(event:MouseEvent):void
		{
			dispatchEvent(new gestionparcEvent(gestionparcEvent.POPUP_USAGE_VALIDED,tiNewUsage.text));
			PopUpManager.removePopUp(this);
		}
	}
}