package gestionparc.ihm.fiches.popup.searchbox
{
	import flash.display.DisplayObject;
	
	import mx.controls.Image;
	
	import composants.controls.TextInputLabeled;
	
	public class SearchInput extends TextInputLabeled
	{
		public var image:Image;
		
		[Embed(source="/assets/images/find.png", mimeType="image/png")]
		[Bindable]
		private var searchIcon:Class;
		
		public function SearchInput()
		{
			super();
		}
		
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			image = new Image();
			image.source = searchIcon;
			
			addChild(DisplayObject(image));
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			//taille de l'image search (loupe)
			this.image.width = 14;
			this.image.height = 14;
			this.image.scaleContent =true;
			// positionnement de l'image
			this.image.x = this.width - this.image.width -10;
			this.image.y = this.height - this.image.height-5;
		}
	}
}