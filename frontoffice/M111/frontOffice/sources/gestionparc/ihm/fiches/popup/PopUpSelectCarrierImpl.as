package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.ComboBox;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import gestionparc.entity.OperateurVO;
	import gestionparc.ihm.fiches.popup.searchbox.SearchBoxIHM;

	public class PopUpSelectCarrierImpl extends TitleWindow
	{
		//Event
		public static const POPUPCARRIER_SUBMIT_EVENT:String = "PopUpCarrierSelectEvent";
		
		//WordsResourceManager.getInstance().getString('M111', 'Veuillez_renseigner_les_champs_obligatoi')
		private var CONF_ERROR:String = ResourceManager.getInstance().getString("M111","sav_parametre_insuffisants");
		private var CARRIER_ERROR:String = ResourceManager.getInstance().getString("M111","sav_select_carrier_error"); 
		
		public var cbCarrier:ComboBox;
		[Bindable]
		public var subtitle:String;
		
		private var _searchbox:SearchBoxIHM;		
		private var _carrierList:ArrayCollection;		
		private var _selectedItem:OperateurVO;
		
		private var _compteList:ArrayCollection;
		
		private var _sousCompteList:ArrayCollection;
		
		private var _infos_message:String = "";
		
		private var _context:Object;
		
		private var _boolEnable:Boolean;
		
		
		public function PopUpSelectCarrierImpl()
		{

		}
		

		
	
		 
		public function get context():Object
		{
			return _context;
		}

		public function set context(value:Object):void
		{
			if(value != null){
				_context = value;
			}
			
		}

		public function creationCompleteHandler():void{
			cbCarrier.setFocus();
		}
		
		public function btCloseHandler():void{
			PopUpManager.removePopUp(this);
		}
		
		
		public function popupselectcarrierimpl1_closeHandler():void{
			PopUpManager.removePopUp(this);
		}
		
		public function btSubmitHandler():void{
			
			if(cbCarrier.selectedItem != null)
			{
				_selectedItem = new OperateurVO();
				selectedItem.nom = cbCarrier.selectedItem.NOM;
				selectedItem.idOperateur = cbCarrier.selectedItem.OPERATEURID;
				cbCarrier.errorString = "";
				dispatchEvent(new Event(PopUpSelectCarrierImpl.POPUPCARRIER_SUBMIT_EVENT));
			}else{
				cbCarrier.errorString = CARRIER_ERROR;
			}
			
		}
		
		public function imgSelectItemClickHandler(source:ComboBox):void{
			if(_searchbox != null) _searchbox = null;
			_searchbox = new SearchBoxIHM();
			_searchbox.comboSource = source;
			PopUpManager.addPopUp(_searchbox, this,true);
		}
		
		
		public function cbCarrierChangeHandler():void
		{
			//TO-DO: getCompte // restriction sur les sous cmpte autorisées?
		}
		
		public function cbCompteChangeHandler():void
		{
			//TO-DO: getSousCompte // restriction sur les sous cmpte autorisées? 
		}
				
		
		
		
		
		////////////////// ACCESSORS ------------------------------------------------
		
		public function get selectedItem():OperateurVO
		{
			return _selectedItem;
		}
		
		[Bindable]
		public function get carrierList():ArrayCollection
		{
			return _carrierList;
		}
		
		public function set carrierList(value:ArrayCollection):void
		{
			if(value){
				_carrierList = value;
				if(value.length == 0){
					
					infos_message = CONF_ERROR.replace("*LIBELLE_POOL*",context.LIBELLE_POOL);
					boolEnable = false;
				}else
				{
					infos_message = "";
					boolEnable = true;
				}
			}
		}
		
		[Bindable]
		public function get infos_message():String
		{
			return _infos_message;
		}
		
		public function set infos_message(value:String):void
		{
			_infos_message = value;
		}
		
		[Bindable]
		public function get sousCompteList():ArrayCollection
		{
			return _sousCompteList;
		}
		
		public function set sousCompteList(value:ArrayCollection):void
		{
			_sousCompteList = value;
		}
		
		[Bindable]
		public function get compteList():ArrayCollection
		{
			return _compteList;
		}
		
		public function set compteList(value:ArrayCollection):void
		{
			_compteList = value;
		}

		[Bindable]
		public function get boolEnable():Boolean
		{
			return _boolEnable;
		}

		public function set boolEnable(value:Boolean):void
		{
			_boolEnable = value;
		}


	}
}