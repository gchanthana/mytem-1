package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.CommandeEvent;
	import gestionparc.event.TypeCommandeEvent;
	import gestionparc.services.PoolService;
	import gestionparc.services.cache.CacheDatas;
	
	import mx.containers.Box;
	import mx.containers.Canvas;
	import mx.controls.Image;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	[Bindable]
	public class SelectTypeCommandeImpl extends Box
	{
		public var cvs0:Canvas;
		public var cvs1:Canvas;
		public var cvs2:Canvas;
		public var cvs3:Canvas;
		public var cvs4:Canvas;
		public var cvs5:Canvas;

		public static var text_toolTip_NEWCMD:String=ResourceManager.getInstance().getString('M111', 'Cr_ation_d_une_nouvelle_commande');
		public static var text_toolTip_MODIFCMD:String=ResourceManager.getInstance().getString('M111', 'Cr_ation_d_une_nouvelle_commande');

		public static var text_toolTip_NCMD_Mobile:String=ResourceManager.getInstance().getString('M111', 'Cr_ation_d_une_nouvelle_commande');
		public static var text_toolTip_NCMD_Fixe:String=ResourceManager.getInstance().getString('M111', 'Op_ration_sur_ligne_existante');
		public static var text_toolTip_NCMD_Reseau:String=ResourceManager.getInstance().getString('M111', 'Op_ration_sur_ligne_existante');
		public static var text_toolTip_MCMD_Mobile:String=ResourceManager.getInstance().getString('M111', 'Op_ration_sur_ligne_existante');
		public static var text_toolTip_MCMD_Fixe:String=ResourceManager.getInstance().getString('M111', 'Op_ration_sur_ligne_existante');
		public static var text_toolTip_MCMD_Reseau:String=ResourceManager.getInstance().getString('M111', 'Op_ration_sur_ligne_existante');

		public static var text_toolTip_NoAutorised:String=ResourceManager.getInstance().getString('M111', 'Vous_n_avaez_pas_les_droits_n_cessaire__');


		private var _poolsSrvc:PoolService=new PoolService();


		public var _fixIsAutorised:Boolean=false;
		public var _mobIsAutorised:Boolean=false;

		public var idProfil:int=0;

		public var imgMobileLeft:Image;
		public var imgFixeLeft:Image;
		public var imgReseauLeft:Image;

		public var imgMobileRight:Image;
		public var imgFixeRight:Image;
		public var imgReseauRight:Image;

		[Embed(source="/assets/commande/mobile_vert.png", mimeType='image/png')]
		public var imgMobileLeft_Default:Class;
		[Embed(source="/assets/commande/fixe_vert.png", mimeType='image/png')]
		public var imgFixeLeft_Default:Class;
		[Embed(source="/assets/commande/reseau_vert.png", mimeType='image/png')]
		public var imgReseauLeft_Default:Class;

		[Embed(source="/assets/commande/mobile_bleu.png", mimeType='image/png')]
		public var imgMobileRight_Default:Class;
		[Embed(source="/assets/commande/fixe_bleu.png", mimeType='image/png')]
		public var imgFixeRight_Default:Class;
		[Embed(source="/assets/commande/reseau_bleu.png", mimeType='image/png')]
		public var imgReseauRight_Default:Class;

		[Embed(source="/assets/commande/mobile_brun.png", mimeType='image/png')]
		public var imgMobileSelected_Default:Class;
		[Embed(source="/assets/commande/fixe_brun.png", mimeType='image/png')]
		public var imgFixeSelected_Default:Class;
		[Embed(source="/assets/commande/reseau_brun.png", mimeType='image/png')]
		public var imgReseauSelected_Default:Class;

		[Embed(source="/assets/commande/mobile_gris.png", mimeType='image/png')]
		public var imgMobileDisable:Class;
		[Embed(source="/assets/commande/fixe_gris.png", mimeType='image/png')]
		public var imgFixeDisable:Class;
		[Embed(source="/assets/commande/reseau_gris.png", mimeType='image/png')]
		public var imgReseauDisable:Class;


		public function SelectTypeCommandeImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}

		private function creationCompleteHandler(fe:FlexEvent):void
		{
			isAutorised();
		}

		private function isAutorised():void
		{
//			_poolsSrvc.fournirListeActionProfilCommande(idProfil);
//			_poolsSrvc.addEventListener(CommandeEvent.LISTED_PRF_ACTIONS, actionsProfilesListedHandler);
			actionsProfilesListedHandler(null);
		}

		private function actionsProfilesListedHandler(cmde:CommandeEvent):void
		{
			var len:int=CacheDatas.listeDroitCommande.length;

			for (var i:int=0; i < len; i++)
			{
				if (CacheDatas.listeDroitCommande[i].hasOwnProperty("FIXE"))
					_fixIsAutorised=CacheDatas.listeDroitCommande[i].ACTIF;

				if (CacheDatas.listeDroitCommande[i].hasOwnProperty("MOBILE"))
					_mobIsAutorised=CacheDatas.listeDroitCommande[i].ACTIF;
			}

			majIhm();
		}

		private function majIhm():void
		{
			if (!_mobIsAutorised)
			{
				imgMobileLeft.source=imgMobileDisable;
				cvs0.toolTip=text_toolTip_NoAutorised;
				imgMobileLeft.toolTip=text_toolTip_NoAutorised;
				imgMobileRight.source=imgMobileDisable;
				cvs3.toolTip=text_toolTip_NoAutorised;
				imgMobileRight.toolTip=text_toolTip_NoAutorised;
			}

			if (!_fixIsAutorised)
			{
				imgFixeLeft.source=imgFixeDisable;
				imgFixeLeft.toolTip=text_toolTip_NoAutorised;
				cvs1.toolTip=text_toolTip_NoAutorised;
				imgReseauLeft.source=imgReseauDisable;
				imgReseauLeft.toolTip=text_toolTip_NoAutorised;
				cvs4.toolTip=text_toolTip_NoAutorised;
				imgFixeRight.source=imgFixeDisable;
				imgFixeRight.toolTip=text_toolTip_NoAutorised;
				cvs2.toolTip=text_toolTip_NoAutorised;
				imgReseauRight.source=imgReseauDisable;
				imgReseauRight.toolTip=text_toolTip_NoAutorised;
				cvs5.toolTip=text_toolTip_NoAutorised;
			}
		}

		protected function btnAnnulerClickHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		protected function canevasClickHandler(me:MouseEvent):void
		{
			var cvsName:String=(me.currentTarget as Canvas).id;

			switch (cvsName)
			{
				case "cvs0":
					if (_mobIsAutorised)
						dispatchEvent(new TypeCommandeEvent(TypeCommandeEvent.OPE_TYPE_CMD, TypeCommandeEvent.OPE_NEWCMD, TypeCommandeEvent.TYPE_MOBILE, true));
					break;
				case "cvs1":
					if (_fixIsAutorised)
						dispatchEvent(new TypeCommandeEvent(TypeCommandeEvent.OPE_TYPE_CMD, TypeCommandeEvent.OPE_NEWCMD, TypeCommandeEvent.TYPE_FIXE, true));
					break;
				case "cvs2":
					if (_fixIsAutorised)
						dispatchEvent(new TypeCommandeEvent(TypeCommandeEvent.OPE_TYPE_CMD, TypeCommandeEvent.OPE_NEWCMD, TypeCommandeEvent.TYPE_RESEAU, true));
					break;
				case "cvs3":
					if (_mobIsAutorised)
						dispatchEvent(new TypeCommandeEvent(TypeCommandeEvent.OPE_TYPE_CMD, TypeCommandeEvent.OPE_MODIFCMD, TypeCommandeEvent.TYPE_MOBILE, true));
					break;
				case "cvs4":
					if (_fixIsAutorised)
						dispatchEvent(new TypeCommandeEvent(TypeCommandeEvent.OPE_TYPE_CMD, TypeCommandeEvent.OPE_MODIFCMD, TypeCommandeEvent.TYPE_FIXE, true));
					break;
				case "cvs5":
					if (_fixIsAutorised)
						dispatchEvent(new TypeCommandeEvent(TypeCommandeEvent.OPE_TYPE_CMD, TypeCommandeEvent.OPE_MODIFCMD, TypeCommandeEvent.TYPE_RESEAU, true));
					break;
			}


		}

		protected function rollOverHandler(btnNumber:int):void
		{
			switch (btnNumber)
			{
				case 0:
					if (_mobIsAutorised)
						imgMobileLeft.source=imgMobileSelected_Default;
					break;
				case 1:
					if (_fixIsAutorised)
						imgFixeLeft.source=imgFixeSelected_Default;
					break;
				case 2:
					if (_fixIsAutorised)
						imgReseauLeft.source=imgReseauSelected_Default;
					break;
				case 3:
					if (_mobIsAutorised)
						imgMobileRight.source=imgMobileSelected_Default;
					break;
				case 4:
					if (_fixIsAutorised)
						imgFixeRight.source=imgFixeSelected_Default;
					break;
				case 5:
					if (_fixIsAutorised)
						imgReseauRight.source=imgReseauSelected_Default;
					break;
			}
		}

		protected function rollOutHandler(btnNumber:int):void
		{
			switch (btnNumber)
			{
				case 0:
					if (_mobIsAutorised)
						imgMobileLeft.source=imgMobileLeft_Default;
					break;
				case 1:
					if (_fixIsAutorised)
						imgFixeLeft.source=imgFixeLeft_Default;
					break;
				case 2:
					if (_fixIsAutorised)
						imgReseauLeft.source=imgReseauLeft_Default;
					break;
				case 3:
					if (_mobIsAutorised)
						imgMobileRight.source=imgMobileRight_Default;
					break;
				case 4:
					if (_fixIsAutorised)
						imgFixeRight.source=imgFixeRight_Default;
					break;
				case 5:
					if (_fixIsAutorised)
						imgReseauRight.source=imgReseauRight_Default;
					break;
			}
		}
	}
}
