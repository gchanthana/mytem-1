package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import gestionparc.entity.CollaborateurVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.collaborateur.CollaborateurServices;
	
	import paginatedatagrid.PaginateDatagrid;
	import paginatedatagrid.event.PaginateDatagridEvent;
	
	public class PopUpChoixManagerImpl extends TitleWindowBounds
	{
		[Bindable]
		public static var NB_PAR_PAGE	:Number = 10;
		public var pgdg_manager			:PaginateDatagrid;
		public var tiMotClef			:TextInput;
		public var btn_recherche		:Button;
		
		private var _srvCollaborateur	:CollaborateurServices;
		private var _listeManagers		:ArrayCollection;
		private var itemCurrent			:Object
		private var _nbreTotalResult	:Number;
		private var _startindex 		:int=1;
		private var _itemCurrent		:CollaborateurVO = new CollaborateurVO();
		
		public function PopUpChoixManagerImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initcc);
		}
		
		protected function initcc(event:FlexEvent):void
		{
			srvCollaborateur=new CollaborateurServices();
			initListeners();
			initData();
			initDisplay();
		}
		
		private function initDisplay():void
		{
			callLater(tiMotClef.setFocus)
		}
		
		private function initData():void
		{
			// default search: all managers
			srvCollaborateur.getListeManagers(1, NB_PAR_PAGE);
			
		}
		
		private function initListeners():void
		{
			pgdg_manager.addEventListener(ListEvent.ITEM_CLICK, currentItemHandler);
			btn_recherche.addEventListener(MouseEvent.CLICK, onClickRecherche);
			addEventListener(KeyboardEvent.KEY_DOWN, onClickKeyBoardRecherche);
			srvCollaborateur.myDatas.addEventListener(gestionparcEvent.LISTE_MANAGERS, getListeManagersHandler);
			
		}
		
		protected function currentItemHandler(l_evt:ListEvent):void
		{
			var itemCurrent:Object = l_evt.currentTarget.selectedItem;
			var i:int = 0;
			while (i < listeManagers.length)
			{
				if(itemCurrent!=listeManagers[i])
					listeManagers[i].SELECTED = false;
				else
				{
					listeManagers[i].SELECTED = true;
				}
				listeManagers.itemUpdated(listeManagers[i]);
				i++;
			}
		}
		
		protected function getListeManagersHandler(event:Event):void
		{
			this.listeManagers = srvCollaborateur.myDatas.listeManagers;
			pgdg_manager.nbItemParPage = NB_PAR_PAGE;
			pgdg_manager.nbTotalItem = srvCollaborateur.myDatas.nbreTotalManagers;
			if(listeManagers.length > 1){
				nbreTotalResult = (listeManagers[1] as CollaborateurVO).NBRECORD;
				pgdg_manager.paginationBox.initPagination(nbreTotalResult, NB_PAR_PAGE);
			}
		}
		
		protected function closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
				if((listeManagers.length > 0) && (pgdg_manager.selectedItem))
				{
					if(pgdg_manager.selectedItem.SELECTED)
					{
						dispatchEvent(new gestionparcEvent(gestionparcEvent.MANAGER_SELECTED, pgdg_manager.selectedItem, true));
						PopUpManager.removePopUp(this);
					}
				}
				else
				{
					ConsoviewAlert.afficherAlertInfo( ResourceManager.getInstance().getString('M111', 'Veuillez_s_lectionner_un_gestionnaire__') , ResourceManager.getInstance().getString('M111', 'Alerte'), null);
				}
		}
		
		protected function onClickRecherche(evt:MouseEvent):void
		{
			this.getListeManagers();
		}
		
		protected function onClickKeyBoardRecherche(evt:KeyboardEvent):void
		{
			if (evt.keyCode == Keyboard.ENTER)
				this.getListeManagers();
		}
		
		
		protected function onIntervalleChangeHandler(evt:PaginateDatagridEvent):void
		{
			if(pgdg_manager.currentIntervalle!=null)
			{
				if (pgdg_manager.currentIntervalle.indexDepart == 0)
				{
					_startindex = 1;
				}
				else
				{		
					_startindex = pgdg_manager.currentIntervalle.indexDepart + 1;
				}
				
				this.getListeManagers();
			}
		}
		
		private function getListeManagers():void
		{
			srvCollaborateur.getListeManagers(_startindex, NB_PAR_PAGE, StringUtil.trim(tiMotClef.text));
		}
		
		protected function pgdg_manager_onItemSelectedHandler(evt:PaginateDatagridEvent):void
		{
			_itemCurrent = evt.currentTarget.selectedItem;
			var i:int = 0;
			while (i < listeManagers.length)
			{
				if(_itemCurrent.idManager!=listeManagers[i].idManager)
					listeManagers[i].SELECTED = false;
				else
				{
					listeManagers[i].SELECTED = true;
				}
				i++;
			}
			
			listeManagers.refresh();
		}
		
		public function get srvCollaborateur():CollaborateurServices { return _srvCollaborateur; }
		
		public function set srvCollaborateur(value:CollaborateurServices):void
		{
			if (_srvCollaborateur == value)
				return;
			_srvCollaborateur = value;
		}
		
		[Bindable]
		public function get listeManagers():ArrayCollection { return _listeManagers; }
		
		public function set listeManagers(value:ArrayCollection):void
		{
			if (_listeManagers == value)
				return;
			_listeManagers = value;
		}
		
		[Bindable]
		public function get nbreTotalResult():Number { return _nbreTotalResult; }
		
		public function set nbreTotalResult(value:Number):void
		{
			if (_nbreTotalResult == value)
				return;
			_nbreTotalResult = value;
		}
	}
}