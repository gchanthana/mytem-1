package gestionparc.ihm.fiches.popup.searchbox
{
	import flash.geom.Rectangle;
	
	import mx.collections.ICollectionView;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.controls.List;
	import mx.managers.PopUpManager;
	
	
	public class SearchBoxImpl extends TitleWindow
	{
		private var _comboSource:ComboBox;
		
		public var list:List;
		public var btValidate:Button;
		public var btCancel:Button;
		public var txtFilter:SearchInput;
		public var imgFunel:Image;
		[Bindable]
		public var currentSelectedLabel:String; 
		
		
		public function SearchBoxImpl()
		{
			super();
		}
		
		//-------- Handlers ------------------
		
		protected function onCloseHandler():void
		{
			reset();
			closeMe();
		}
		
		protected function btOkClickHandler():void
		{
			comboSource.selectedItem = list.selectedItem;
			reset();
			closeMe();
		}
		
		protected function btCancelClickHandler():void
		{
			reset();
			closeMe();
		}
		
		protected function txtFilterHandler():void
		{
			(list.dataProvider as ICollectionView).filterFunction = filterList;
			(list.dataProvider as ICollectionView).refresh();
			
		}
		
		protected function onCreationCompleteHandler():void
		{
			if(comboSource == null || comboSource.dataProvider == null) closeMe();
			if((comboSource.dataProvider as ICollectionView).length == 0) closeMe();
			init()
		}
		
		protected function listChangeHabdler():void
		{
			currentSelectedLabel = list.selectedItem[list.labelField];
		}
		
		protected function listItemDoubleClick():void
		{
			comboSource.selectedItem = list.selectedItem;
			reset();
			closeMe();
		}
		
		//------- Private ------------------
		
		private function closeMe():void
		{
			PopUpManager.removePopUp(this);
		}
		
		
		
		private function init():void
		{
			list.labelField=_comboSource.labelField;
			list.dataProvider=_comboSource.dataProvider;
			list.selectedIndex=_comboSource.selectedIndex;
			list.validateNow();
			list.scrollToIndex(list.selectedIndex);
			txtFilter.setFocus();
		}
		
		private function reset():void
		{
			txtFilter.text = "";
			(list.dataProvider as ICollectionView).filterFunction = null;
			(list.dataProvider as ICollectionView).refresh();
		}
		
		private function filterList(item:Object):Boolean
		{
			var boolean:Boolean = false;
			
			if(
				(item && item[list.labelField] != null)
				&&
				((item[list.labelField] as String).toUpperCase().search(txtFilter.text.toLocaleUpperCase()) != -1)
			)
				boolean = true;
			
			return boolean;
		}
		
		
		//----- Accessors -----------------
		
		
		[Bindable]
		public function get comboSource():ComboBox
		{
			return _comboSource;
		}
		
		
		public function set comboSource(value:ComboBox):void
		{
			if(value != null)
			{
				_comboSource = value;
				width = _comboSource.width + 100;
				var rectangle:Rectangle = _comboSource.getVisibleRect(this.parent);
				x = rectangle.x;
				y = rectangle.y;
			}
			
		}
		
	}
}