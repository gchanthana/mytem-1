package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import mx.resources.ResourceManager;

	public class PopUpUnmanageDeviceImpl extends TitleWindow
	{
		private var _currentEquipementInfos:AbstractMatriceParcVO;

		[Bindable]
		public var currentTitle:String=ResourceManager.getInstance().getString('M111','Ne_plus_g_rer_l__quipement');
		[Bindable]
		public var currentMessage:String=ResourceManager.getInstance().getString('M111','Suppression_d_finitive');

		[Bindable]
		public var lblChbxRemovePermanently:String=ResourceManager.getInstance().getString('M111','Suppression_d_finitive');
		[Bindable]
		public var lblBtnValider:String=ResourceManager.getInstance().getString('M111','Valider');
		[Bindable]
		public var lblBtnAnnuler:String=ResourceManager.getInstance().getString('M111','Annuler');

		
		public function PopUpUnmanageDeviceImpl()
		{
		}


		public function setInfos(currentEquipementInfos:AbstractMatriceParcVO):void
		{
			_currentEquipementInfos=currentEquipementInfos;

			currentMessage=ResourceManager.getInstance().getString('M111','_tes_vous_sur_de_ne_plus_vouloir_g_rer_l__quipement')
				 + _currentEquipementInfos.T_IMEI + " ?";
		}

		public function getInfos():Object
		{
			var infos:Object=new Object();
			infos.CURRENTINFOS=_currentEquipementInfos;
			infos.REMOVEPERMANENTLY=true;

			return infos;
		}


		protected function closeHandler(ce:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('UNMANAGE_DEVICE'));
		}

		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		private function formatBooleanToInteger(value:Boolean):int
		{
			var intValue:int=0;

			if (value)
				intValue=1;

			return intValue;
		}
	}
}
