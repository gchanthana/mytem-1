package gestionparc.ihm.fiches.popup
{
	import composants.util.CvDateChooser;

	import flash.events.MouseEvent;

	import gestionparc.event.gestionparcEvent;

	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class popupResiliationLigne extends TitleWindow
	{
		private var _dgProduit:ArrayCollection;
		private var _selectedDate:Date;
		private var cvdate:CvDateChooser;

		public function popupResiliationLigne(dgProvider:ArrayCollection)
		{
			super();
			this._dgProduit=dgProvider;

			this.title=ResourceManager.getInstance().getString("M111", "resiliation__Ligne____");
			this.setStyle("BorderStyle", "solid");
			this.layout="vertical";
			this.styleName="M111popUpBox";
			this.width=400;

			var hb1:HBox=new HBox();
			hb1.percentWidth=100;
			hb1.setStyle("horizontalAlign", "right");

			var hb2:HBox=new HBox();
			hb2.percentWidth=100;
			hb2.setStyle("horizontalAlign", "right");

			var dg:DataGrid=new DataGrid();
			dg.dataProvider=this._dgProduit;
			dg.percentWidth=100;
			var col:DataGridColumn=new DataGridColumn(ResourceManager.getInstance().getString('M111', 'Produit'));
			col.dataField="libelleProduit";
			dg.columns=[col];

			cvdate=new CvDateChooser();
			cvdate.selectedDate=new Date

			var btnValider:Button=new Button;
			btnValider.label=ResourceManager.getInstance().getString('M111', 'Valider');
			btnValider.addEventListener(MouseEvent.CLICK, btnValiderClickHandler);

			var btnCancel:Button=new Button;
			btnCancel.label=ResourceManager.getInstance().getString('M111', 'Annuler');
			btnCancel.addEventListener(MouseEvent.CLICK, btnCancelClickHandler);

			hb1.addChild(cvdate);
			hb2.addChild(btnValider);
			hb2.addChild(btnCancel);

			this.addChild(dg);
			this.addChild(hb1);
			this.addChild(hb2);
		}

		private function btnValiderClickHandler(evt:MouseEvent):void
		{
			if (cvdate.selectedDate != null)
				_selectedDate=cvdate.selectedDate;
			else
				_selectedDate=new Date();
			dispatchEvent(new gestionparcEvent(gestionparcEvent.POPUP_RESILIATION_VALIDATED));
			btnCancelClickHandler(null);
		}

		private function btnCancelClickHandler(evt:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		public function get selectedDate():Date
		{
			return _selectedDate;
		}

		public function set selectedDate(value:Date):void
		{
			_selectedDate=value;
		}

	}
}
