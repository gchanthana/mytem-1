package gestionparc.ihm.fiches.popup
{
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.equipement.EquipementServices;
	
	import mx.containers.FormItem;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de la popUp "choix d'un modèle hors catalogue".
	 * 
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 26.07.2010
	 * 
	 * </pre></p>
	 * 
	 */
	[Bindable]
	public class ChoixModeleHorsCatalogueImpl extends TitleWindow
	{
		// VARIABLES------------------------------------------------------------------------------
		 		
		// IHM components
		public var cbConstructeur				:ComboBox;
		public var cbModele						:ComboBox;
		
		public var tiAutreModele				:TextInput;
		
		public var rbModele						:RadioButton;
		public var rbAutreModele				:RadioButton;
		
		public var btAnnuler					:Button;
		public var btValider					:Button;
		
		public var formItemModele				:FormItem;
		public var formItemAutreModele			:FormItem;
		public var formItemConstructeur			:FormItem;
		
		// publics variables 
		public var oldModele 					:String;
		public var oldMarque 					:String;	
		
		public var myServiceTerminal			:EquipementServices;
		public var myServicesCache				:CacheServices;
		
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe ChoixModeleHorsCatalogueImpl.
		 * </pre></p>
		 */	
		public function ChoixModeleHorsCatalogueImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		// PRIVATE FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation générale de cette classe  :
		 * - Appel le service permettant de récupérer la liste des constructeurs.
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 * </pre></p>
		 * 
		 * @param event:<code>FlexEvent</code>.
		 * @return void.
		 * 
		 * @see #initListener()
		 * @see #initDisplay()
		 * 
		 * @see gestionparc.services.terminal.TerminalServices#getConstructeur()
		 * 
		 */
		private function init(evt:FlexEvent):void
		{
			if(CacheDatas.listeConstructeur)
			{
				listeConstructeurLoadedHandler(null)
			}
			else
			{
				myServicesCache.getConstructeur();
			}
			
			initListener();
			initDisplay();
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise les écouteurs.
		 * </pre></p>
		 * 
		 * @return void.
		 */	
		private function initListener():void
		{
			// listeners lié aux boutons de FicheDetailleeNouvelleLigneIHM
			this.addEventListener(CloseEvent.CLOSE, closePopup); 
			
			rbModele.addEventListener(MouseEvent.CLICK , clickRadioButton); 
			rbAutreModele.addEventListener(MouseEvent.CLICK , clickRadioButton); 
			btAnnuler.addEventListener(MouseEvent.CLICK , cancelPopup);
			btValider.addEventListener(MouseEvent.CLICK , validerHandler);
			
			// listeners lié a la maj des modeles lorsque le revendeur change
			cbConstructeur.addEventListener(ListEvent.CHANGE , changeConstructeurHandler);
			
			// listeners liés aux services
			myServicesCache.myDatas.addEventListener(gestionparcEvent.LISTE_CONSTRU_LOADED,listeConstructeurLoadedHandler);
			myServiceTerminal.myDatas.addEventListener(gestionparcEvent.LISTE_MODELES_LOADED,listeModelesLoadedHandler);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise l'affichage de cette IHM.
		 * </pre></p>
		 * 
		 * @return void.
		 */	
		private function initDisplay():void
		{
			formItemAutreModele.enabled=false;
			rbModele.selected=true;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Charge les données qui viennent d'être collectées de la base dans la comboBox "Marque" dans l'IHM.
		 * </pre></p>
		 * 
		 * @param event:<code>gestionparcEvent</code>.
		 * @return void.
		 */
		private function listeConstructeurLoadedHandler(event:gestionparcEvent):void
		{
			var lenListeConstructeur : int = CacheDatas.listeConstructeur.length;
			cbConstructeur.dataProvider = CacheDatas.listeConstructeur;
			
			for(var i : int = 0 ; i < lenListeConstructeur ; i++)
			{
				if(CacheDatas.listeConstructeur[i].NOM_FOURNISSEUR == oldMarque)
				{
					cbConstructeur.selectedIndex = i;
					break;
				}
			}
			myServiceTerminal.getListeModele(cbConstructeur.selectedItem.IDFOURNISSEUR);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Charge les données qui viennent d'être collectées de la base dans la comboBox "Modèle" dans l'IHM.
		 * </pre></p>
		 * 
		 * @param event:<code>gestionparcEvent</code>.
		 * @return void.
		 */
		private function listeModelesLoadedHandler(event:gestionparcEvent):void
		{
			var lenListeModele : int = myServiceTerminal.myDatas.listeModele.length;
			
			cbModele.dataProvider = myServiceTerminal.myDatas.listeModele;
			
			for(var i : int = 0 ; i < lenListeModele ; i++)
			{
				if(myServiceTerminal.myDatas.listeModele[i].LIBELLE_EQ == oldModele)
				{
					cbModele.selectedIndex = i;
					break;
				}
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appel le service permettant de récupérer la liste des modèles pour ce constructeur.
		 * </pre></p>
		 * 
		 * @param event:<code>ListEvent</code>.
		 * @return void.
		 */
		private function changeConstructeurHandler(event:ListEvent):void
		{
			if(cbConstructeur.selectedItem)
			{
				myServiceTerminal.getListeModele(cbConstructeur.selectedItem.IDFOURNISSEUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Active ou désactive le bon formItem selon le clique de l'utilisateur.
		 * </pre></p>
		 * 
		 * @param event:<code>MouseEvent</code>.
		 * @return void.
		 */
		private function clickRadioButton(event:MouseEvent):void
		{
			if(rbModele.selected)
			{
				formItemModele.enabled=true;
				formItemAutreModele.enabled=false;
			}
			else
			{
				formItemModele.enabled=false;
				formItemAutreModele.enabled=true;
			}
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 * </pre></p>
		 * 
		 * @param event:<code>MouseEvent</code>.
		 * @return void.
		 * 
		 * @see #closePopup()
		 * 
		 */
		private function cancelPopup(event:MouseEvent):void 
		{
			closePopup(null);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ferme la popup courante.
		 * </pre></p>
		 * 
		 * @param event:<code>Event</code>.
		 * @return void.
		 */
		private function closePopup(event:CloseEvent):void 
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Valide la popUp courante et dispatch un evenement contenant un objet 
		 * avec la nouvelle marque, le nouveau modele et leurs id. 
		 * Aucune procédure est appelée et l'enregistrement se fait au moment de la validation de la fiche terminal
		 * </pre></p>
		 * 
		 * @param event:<code>MouseEvent</code>.
		 * @return void.
		 */
		private function validerHandler(event:MouseEvent):void 
		{
			var objMarqueModele : Object = new Object();
			objMarqueModele.libelleMarque 	= cbConstructeur.selectedItem.NOM_FOURNISSEUR;
			objMarqueModele.idFabricant		= cbConstructeur.selectedItem.IDFOURNISSEUR; 
			
			if(formItemModele.enabled)
			{
				objMarqueModele.idEqptFournis 		= cbModele.selectedItem.IDEQUIPEMENT_FOURNIS;
				objMarqueModele.idTypeEqpt	 		= cbModele.selectedItem.IDTYPE_EQUIPEMENT;
				objMarqueModele.libEqptFournis		= cbModele.selectedItem.LIBELLE_EQ;
				objMarqueModele.refConst 			= (cbModele.selectedItem.REF_CONSTRUCTEUR)?cbModele.selectedItem.REF_CONSTRUCTEUR:"";
				objMarqueModele.refRevendeur 		= (cbModele.selectedItem.REF_REVENDEUR)?cbModele.selectedItem.REF_REVENDEUR:"";
			}
			else // le modèle n'existe pas, il sera crée à la validation de la fiche eqpt (id=0)
			{
				objMarqueModele.idEqptFournis 		= 0; 
				objMarqueModele.idTypeEqpt	 		= 70; // eqpt mobile 
				objMarqueModele.libEqptFournis		= tiAutreModele.text;
				objMarqueModele.refConst 			= ""; 
				objMarqueModele.refRevendeur		= ""; 
			}
			
			dispatchEvent(new gestionparcEvent(gestionparcEvent.CHOICE_MODELE_DID,objMarqueModele));
			
			closePopup(null);
		}
		
		// PRIVATE FUNCTIONS-------------------------------------------------------------------------

	}
}