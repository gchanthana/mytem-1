package gestionparc.ihm.fiches.popup
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.MDMDataEvent;
	import gestionparc.services.mdm.MDMService;
	import gestionparc.services.useracess.UserAccessService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;

	public class PopUpLocationImpl extends TitleWindow
	{

		public static const POPUP_UPDATED_EVENT:String="POPUP UPDATED";

		private var _currentEquipementInfos:AbstractMatriceParcVO;

		private var _userAccessSrv:UserAccessService;

		private var _mdmSrv:MDMService;

		[Bindable]
		public var locations:ArrayCollection=new ArrayCollection();


		[Bindable]
		public var currentTitle:String="Géolocalisation";

		[Bindable]
		public var lblBtnLocate:String="Localiser";
		[Bindable]
		public var lblBtnRefresh:String="Rafraichir";
		[Bindable]
		public var lblBtnFermer:String="Fermer";


		public function PopUpLocationImpl()
		{
			_mdmSrv=new MDMService();
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}

		private function onCreationComplete(flexEvent:FlexEvent):void
		{
			dispatchEvent(new Event(PopUpLocationImpl.POPUP_UPDATED_EVENT)); // Popup mis à jour
		}


		public function setInfos(currentEquipementInfos:AbstractMatriceParcVO):void
		{
			_currentEquipementInfos=currentEquipementInfos;
			getLocations();
		}

		public function getInfos():Object
		{
			var infos:Object=new Object();

			return infos;
		}


		protected function closeHandler(ce:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		protected function btnLocateClickHandler(me:MouseEvent):void
		{
			locate();
		}

		protected function btnRefreshClickHandler(me:MouseEvent):void
		{
			getLocations();
		}

		protected function btnCloseClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		protected function dgListeLocateItemClickHandler(le:ListEvent):void
		{
			var itemRenderer:Object=le.itemRenderer;
			var currentItem:Object=itemRenderer.data;

			if (itemRenderer)
			{
				if (itemRenderer.listData.dataField == 'LOCATION')
				{
					if (itemRenderer.listData.label != '')
						navigateToURL(new URLRequest(itemRenderer.listData.label), '_blank');
					else
						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M111', 'Recherche_impossible__'), ResourceManager.getInstance().getString('M111', 'Info'), null);
				}
			}
		}


		private function getLocations():void
		{
			// Récupération des infos du device à partir de l'API MDM
			_mdmSrv.getDeviceInfo(_currentEquipementInfos.ZDM_SERIAL_NUMBER, _currentEquipementInfos.ZDM_IMEI);
			_mdmSrv.myDatas.addEventListener(MDMDataEvent.MDM_DATA_COMPLETE, deviceInfoHandler);
		}

		// Récupération des coordonnées de localisations à partir des infos du device
		private function deviceInfoHandler(mde:MDMDataEvent):void
		{
			var dateLocation:Date=null;
			var latitude:Number=NaN;
			var longitude:Number=NaN;
			if (_mdmSrv.myDatas.arrListeDevice != null && _mdmSrv.myDatas.arrListeDevice.length > 0)
			{
				for each (var item:Object in _mdmSrv.myDatas.arrListeDevice)
				{
					// Si la propriété est GPS_TIMESTAMP_FROM_CELLULAR
					var propName:String=String(item.NAME).toUpperCase();
					if (propName == "GPS_TIMESTAMP_FROM_CELLULAR".toLowerCase() && (item["VALUE"] != null) && StringUtil.trim(item["VALUE"]) != "")
					{
						dateLocation=new Date();
						dateLocation.setTime(item["VALUE"]);
					}

					// Si la propriété est GPS_LATITUDE_FROM_CELLULAR
					if (propName == "GPS_LATITUDE_FROM_CELLULAR".toLowerCase() && (item["VALUE"] != null) && StringUtil.trim(item["VALUE"]) != "")
						latitude=item["VALUE"];
					// Si la propriété est GPS_LONGITUDE_FROM_CELLULAR
					if (propName == "GPS_LONGITUDE_FROM_CELLULAR".toLowerCase() && (item["VALUE"] != null) && StringUtil.trim(item["VALUE"]) != "")
						longitude=item["VALUE"];

					// Si la propriété est GPS_TIMESTAMP_FROM_GPS
					if (propName == "GPS_TIMESTAMP_FROM_GPS".toLowerCase() && (item["VALUE"] != null) && StringUtil.trim(item["VALUE"]) != "")
					{
						dateLocation=new Date();
						dateLocation.setTime(item["VALUE"]);
					}
					// Si la propriété est GPS_LATITUDE_FROM_GPS
					if (propName == "GPS_LATITUDE_FROM_GPS".toLowerCase() && (item["VALUE"] != null) && StringUtil.trim(item["VALUE"]) != "")
						latitude=item["VALUE"];
					// Si la propriété est GPS_LONGITUDE_FROM_GPS
					if (propName == "GPS_LONGITUDE_FROM_GPS".toLowerCase() && (item["VALUE"] != null) && StringUtil.trim(item["VALUE"]) != "")
						longitude=item["VALUE"];
				}
				if (dateLocation != null && (!isNaN(latitude)) && (!isNaN(longitude)))
				{
					_currentEquipementInfos["REQUEST_DATE_STR"]=dateLocation;
					_currentEquipementInfos["LOCATION"]="https://maps.google.com/maps?q=@" + latitude + "," + longitude;
					_currentEquipementInfos["IDSTATUT"]=1;
					locations=new ArrayCollection([_currentEquipementInfos]);
					this.validateNow();
				}
			}
		}

		private function locate():void
		{
			_mdmSrv.locate(_currentEquipementInfos.ZDM_SERIAL_NUMBER, _currentEquipementInfos.ZDM_IMEI);
			_mdmSrv.myDatas.addEventListener(MDMDataEvent.MDM_DEVICE_LOCATE, locateHandler);
		}

		private function locateHandler(mde:MDMDataEvent):void
		{
			getLocations();
		}
	}
}
