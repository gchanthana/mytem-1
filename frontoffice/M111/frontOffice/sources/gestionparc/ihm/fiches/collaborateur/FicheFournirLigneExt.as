package gestionparc.ihm.fiches.collaborateur
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.FicheAffectationIHM;
	import gestionparc.ihm.fiches.listedispo.DispoExt;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasLigneAssociated;
	import gestionparc.ihm.question.collaborateur.QuestionFicheFournirLigneExt;
	import gestionparc.utils.gestionparcConstantes;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class FicheFournirLigneExt extends FicheAffectationIHM
	{
		private var liste:DispoExt;
		private var qn:QuestionFicheFournirLigneExt;

		
		public function FicheFournirLigneExt()
		{
			super();
			
			liste=new DispoExt(1, -1);
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}		
		private function init(event:FlexEvent):void
		{
			initListeners();
			initIHM();
		}

		private function initListeners():void
		{
			btAnnuler.addEventListener(MouseEvent.CLICK, fermer);
			btValider.addEventListener(MouseEvent.CLICK, valider);
			this.addEventListener(CloseEvent.CLOSE, fermer);
			liste.addEventListener(gestionparcEvent.REFRESH_LIBELLESELECTION, dgListeDispo_handler);
		}

		private function initIHM():void
		{
			this.title=ResourceManager.getInstance().getString("M111", "une_ligne_Extension");

			this.img_cellule.source=gestionparcConstantes.adrImgEmp;
			this.lab_cellule_name.text=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR

			this.img_liste.source=gestionparcConstantes.adrImgTerm;
			this.img_liste_big.source=gestionparcConstantes.adrImgTerm;
			this.lab_titre.text=liste.getLibelleType()

			this.box_listeDispo.addChild(liste as VBox);
		}

		private function dgListeDispo_handler(evt:Event):void
		{
			labDgSelection.text=liste.getLibelleSelection();
		}

		private function fermer(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function valider(e:Event):void
		{
			if (liste.getIDSelection() > -1)
			{
				qn=new QuestionFicheFournirLigneExt(liste, check_pret.selected, (calendar.selectedDate != null) ? calendar.selectedDate : new Date());
				qn.serv.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED, fermer);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un__l_ment_dans_la_liste'));
			}
		}
	}
}
