package gestionparc.ihm.fiches.collaborateur
{
	import composants.util.CvDateChooser;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.listedispo.LigneFixeDuCollaborateurIHM;
	import gestionparc.ihm.question.collaborateur.QuestionFicheReprendreLigne;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class FicheReprendreLigneImpl extends TitleWindow
	{
		public var liste:LigneFixeDuCollaborateurIHM;
		public var btAnnuler:Button;
		public var btValider:Button;
		public var labDgSelection:Label;
		public var calendar:CvDateChooser;


		private var qn:QuestionFicheReprendreLigne;

		public function FicheReprendreLigneImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			initListeners();
		}

		private function initListeners():void
		{
			btAnnuler.addEventListener(MouseEvent.CLICK, fermer);
			btValider.addEventListener(MouseEvent.CLICK, valider);
			addEventListener(CloseEvent.CLOSE, fermer);
			addEventListener(MouseEvent.CLICK, dgListeDispo_handler);
			addEventListener(gestionparcEvent.REFRESH_LIBELLESELECTION, dgListeDispo_handler);
		}

		private function dgListeDispo_handler(evt:Event):void
		{
			labDgSelection.text=liste.getLibelleSelection();
		}

		private function fermer(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function valider(evt:MouseEvent):void
		{
			if (liste.getIDSelection() > -1)
			{
				qn=new QuestionFicheReprendreLigne(liste, false, (calendar.selectedDate != null) ? calendar.selectedDate : new Date())
				qn.serv.myDatas.addEventListener(gestionparcEvent.DISSOCIATION_REALIZED, fermer);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un__l_ment_dans_la_liste'));
			}
		}
	}
}
