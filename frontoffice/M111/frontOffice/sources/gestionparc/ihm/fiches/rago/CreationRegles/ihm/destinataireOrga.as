package gestionparc.ihm.fiches.rago.CreationRegles.ihm
{
	import composants.parametres.perimetres.OrgaStructure;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.collections.XMLListCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class destinataireOrga extends OrgaStructure
	{
		public var _selectedOrga:Number;
		public var _arrayC:XMLListCollection=new XMLListCollection();
		public var _arrayO:ArrayCollection=new ArrayCollection();

		[Bindable]
		public function set treeData(values:XMLList):void
		{
			setTreeData(values, '@LABEL');
		}

		public function get treeData():XMLList
		{
			return treePerimetre.dataProvider as XMLList;
		}

		[Bindable]
		public function set treeDataArray(values:XMLListCollection):void
		{
			_arrayC=values;
		}

		public function get treeDataArray():XMLListCollection
		{
			return _arrayC;
		}

		[Bindable]
		public function set treeDataObject(values:ArrayCollection):void
		{
			_arrayO=values;
		}

		public function get treeDataObject():ArrayCollection
		{
			return _arrayO;
		}

		public function destinataireOrga()
		{
		}

		public function clickHandler():void
		{
			var event:MouseEvent;
			onbtValiderRechercheClicked(event);
		}

		override public function initIHM(event:Event):void
		{
			super.initIHM(event);
			cmbOrganisation.visible=false;
			btnFilterCmb.visible=false;
		}

		/** Set the tree xml data in the dataprovider of the tree */
		override public function refreshTree():void
		{
			if (isNaN(_selectedOrga) == true)
				return;
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "getNode", fillTreeResult);
			RemoteObjectUtil.callService(op, _selectedOrga.toString(), 0, null);
		}

		override public function searchNode(nodeName:String):void
		{
			_searchTreeResult=_treeNodes.descendants().(@LABEL.toString().toLowerCase().search(nodeName.toLowerCase()) > -1);
			_searchItemIndex=0;
			selectNextNode();
			_searchExecQuery=false;
		}

		override public function searchNodeValue(value:int):void
		{
			_searchTreeResult=_treeNodes.descendants().(@VALUE.toString().toLowerCase().search(value.toString()) > -1);
			_searchItemIndex=0;
			searchNodeByValue(value);
			_searchExecQuery=false;
		}

		override public function selectNextNode():void
		{
			if (_searchTreeResult != null)
			{
				if (_searchTreeResult[_searchItemIndex] == null)
					_searchItemIndex=0;
				if (_searchTreeResult[_searchItemIndex] == null)
					return;
				selectNodeByAttribute("VALUE", _searchTreeResult[_searchItemIndex].@VALUE);
				treeDataObject.addItem(_searchTreeResult[_searchItemIndex]);
				_searchItemIndex++;
			}
		}

		public function searchNodeByValue(value:int):void
		{
			if (_searchTreeResult != null)
			{
				if (_searchTreeResult[_searchItemIndex] == null)
					_searchItemIndex=0;
				if (_searchTreeResult[_searchItemIndex] == null)
					return;
				selectNodeByAttribute("VALUE", value.toString());
				treeDataObject.addItem(_searchTreeResult[_searchItemIndex]);
				_searchItemIndex++;
			}
		}

		private function fillTreeResult(evt:ResultEvent):void
		{
			_treeNodes=XML(evt.result).children();
			var mylist:XMLListCollection=new XMLListCollection();
			mylist.source=XML(evt.result).children();

			var tri:Sort=new Sort();
			tri.fields=[new SortField("@LABEL", true)];
			mylist.sort=tri;
			mylist.refresh();
			setTreeData(mylist.children(), "@LABEL");

			treeDataArray=mylist;
			dispatchEvent(new Event("finishToRefresh", true));
		}

		public function fillTree(objectSelected:Object):void
		{
			var treeNodes:XMLList=XML(objectSelected).children();
			var mylist:XMLListCollection=new XMLListCollection();
			mylist.source=XML(objectSelected).children();

			var tri:Sort=new Sort();
			tri.fields=[new SortField("@LABEL", true)];
			mylist.sort=tri;
			mylist.refresh();

			treeDataArray=mylist;
			dispatchEvent(new Event("finishToRefresh", true));
		}
	}
}
