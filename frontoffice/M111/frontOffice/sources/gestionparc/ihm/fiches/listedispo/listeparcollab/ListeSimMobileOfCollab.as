package gestionparc.ihm.fiches.listedispo.listeparcollab
{
	import flash.events.Event;

	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.services.liste.ListeService;
	import gestionparc.utils.gestionparcConstantes;

	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;

	public class ListeSimMobileOfCollab extends ListeSimMobileOfCollabIHM implements IListeDispo
	{
		public var avecLigne:Boolean=true;
		private var serv:ListeService=new ListeService();

		public function ListeSimMobileOfCollab()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		private function radioOtherSIMhandler(ev:Event=null):void
		{
			avecLigne=false;
			initData();
		}

		private function radioSIMhandler(ev:Event=null):void
		{
			avecLigne=true;
			initData();
		}

		private function initIHM(evt:FlexEvent=null):void
		{
			rdWithSIM.addEventListener(Event.CHANGE, radioSIMhandler);
			rdNoneSIM.addEventListener(Event.CHANGE, radioOtherSIMhandler);
			txtFiltreMySim.setFocus();
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_SIMOFCOLLAB, updateDg);
			initData();
		}

		public function getSelection():Object
		{
			if (dgListeMySIM.selectedIndex != -1)
			{
				return dgListeMySIM.selectedItem;
			}
			else
			{
				return null;
			}
		}

		public function getIDSelection():int
		{
			if(dgListeMySIM.selectedItem != null)
			{
				return dgListeMySIM.selectedItem.IDSIM;
			}
			else
			{
				return -1;
			}
			
		}

		public function getDataGrid():DataGrid
		{
			return dgListeMySIM;
		}

		public function getLibelleType():String
		{
			return ResourceManager.getInstance().getString('M111', 'Liste_des_cartes_SIM');
		}

		public function getSourceIcone():Class
		{
			return gestionparcConstantes.adrImgSIM;
		}

		public function getLibelleSelection():String
		{
			if (dgListeMySIM.selectedIndex != -1)
			{
				return dgListeMySIM.selectedItem.NUM_SIM;
			}
			else
			{
				return ResourceManager.getInstance().getString('M111', 'Aucune_s_lection');
			}
		}

		private function initData():void
		{
			if (!avecLigne)
				serv.listeSimWithoutLigneOfCollab();
			else
				serv.listeSimWithLigneOfCollab();
		}

		private function updateDg(re:gestionparcEvent=null):void
		{
			dgListeMySIM.dataProvider=serv.myDatas.dataListeSIMOfCollab;
			serv.myDatas.dataListeSIMOfCollab.refresh();
			dgListeMySIM.selectedIndex=-1;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION, false));
		}
	}
}
