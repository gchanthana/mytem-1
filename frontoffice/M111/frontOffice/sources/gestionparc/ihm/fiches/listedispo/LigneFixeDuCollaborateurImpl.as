package gestionparc.ihm.fiches.listedispo
{
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;

	public class LigneFixeDuCollaborateurImpl extends LigneFixeBase
	{
		public function LigneFixeDuCollaborateurImpl()
		{
			excludeField="IDEQUIP";
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteIHM);
			serv.myDatas.dataListeligneOfCollab.addEventListener(CollectionEvent.COLLECTION_CHANGE, listeChangeHandler);
			getData();
		}

		override protected function listeChangeHandler(evt:CollectionEvent):void
		{
			if (evt.kind == CollectionEventKind.REFRESH)
			{
				_dataProvider.enableAutoUpdate();
			}

			dataProvider=serv.myDatas.dataListeligneOfCollab;
			s_compteur=_dataProvider.length.toString() + " / " + _real_len.toString() + "  " + ResourceManager.getInstance().getString('M111', 'ligne_s_');
		}

		override protected function getData():void
		{
			serv.listeLigneOfCollab();
		}

	}
}
