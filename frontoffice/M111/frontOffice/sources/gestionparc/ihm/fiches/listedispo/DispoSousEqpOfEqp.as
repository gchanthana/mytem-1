package gestionparc.ihm.fiches.listedispo
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.liste.ListeService;
	
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;

	public class DispoSousEqpOfEqp extends DispoSousEqpOfEqpIhm implements IListeDispo
	{
		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")]
		private var adrImg:Class;
		public var serv:ListeService=new ListeService();


		public function DispoSousEqpOfEqp()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		private function initIHM(evt:FlexEvent):void
		{
			txtFiltreFixe.setFocus();
			dgListeSousEqp.addEventListener(MouseEvent.CLICK, dgVisibleChanged);
			txtFiltreFixe.addEventListener(Event.CHANGE, txtFiltreFixeChangeHandler);
			initData();
		}

		private function txtFiltreFixeChangeHandler(evt:Event):void
		{
			serv.myDatas.dataListeSousEqpOfEqp.filterFunction=filtreGridSsEqp;
			serv.myDatas.dataListeSousEqpOfEqp.refresh();

		}

		private function filtreGridSsEqp(item:Object):Boolean
		{
			if ((String(item.T_NO_SERIE).toLowerCase().search(txtFiltreFixe.text.toLowerCase()) != -1) || (String(item.MARQUE).toLowerCase().search(txtFiltreFixe.text.toLowerCase()) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltreFixe.text.toLowerCase()) != -1))
				return true
			else
				return false
		}

		private function dgVisibleChanged(ev:Event):void
		{
			if (dgListeSousEqp && dgListeSousEqp.selectedIndex > -1)
				dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION));
		}

		public function getSelection():Object
		{
			if (dgListeSousEqp.selectedIndex != -1)
			{
				return dgListeSousEqp.selectedItem;
			}
			else
			{
				return null;
			}
		}

		public function getIDSelection():int
		{
			return dgListeSousEqp.selectedItem.IDTERMINAL
		}

		public function getDataGrid():DataGrid
		{
			return dgListeSousEqp;
		}

		public function getLibelleType():String
		{
			return ResourceManager.getInstance().getString('M111', 'Liste_des_sseqp');
		}

		public function getSourceIcone():Class
		{
			return adrImg;
		}

		public function getLibelleSelection():String
		{
			if (dgListeSousEqp.selectedIndex != -1)
			{
				return dgListeSousEqp.selectedItem.T_NO_SERIE;
			}
			else
			{
				return ResourceManager.getInstance().getString('M111', 'Aucune_s_lection');
			}
		}

		private function initData():void
		{
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_SOUSEQPOFEQP, updateDG);
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;
			if (ligne.ISLIGNEFIXE && !ligne.ISLIGNEMOBILE)
				serv.ListeSsEqp(0, 1, 1);
			else if (ligne.ISLIGNEMOBILE && !ligne.ISLIGNEFIXE)
				serv.ListeSsEqp(1, 0, 0);
			else if (ligne.ISLIGNEFIXE && ligne.ISLIGNEMOBILE)
				serv.ListeSsEqp(1, 1, 1);
			else
				serv.ListeSsEqp(0, 0, 0);
		}

		private function updateDG(re:gestionparcEvent):void
		{
			dgListeSousEqp.dataProvider=serv.myDatas.dataListeSousEqpOfEqp;
		}
	}
}
