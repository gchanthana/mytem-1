package gestionparc.ihm.fiches.listedispo
{
	import flash.events.Event;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasEqpAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasLigneAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;
	import gestionparc.services.liste.ListeService;
	
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;

	public class DispoCollab extends DispoCollabIHM implements IListeDispo
	{
		[Embed(source="/assets/equipement/User_3.gif")]
		private var adrImg:Class;
		private var serv:ListeService=new ListeService();
				
		[Bindable]
		public var boolTerm:Number; /** dire si le collaborateur a un terminal ou non*/
		
		[Bindable]
		public var boolLigne:Number; /** dire si le collaborateur a une ligne */
		
		[Bindable]
		public var boolAllCollab:Boolean;
		
		[Bindable]
		public var isTypeSim:Boolean;

		public function DispoCollab()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);

			var ruleLigne:IRuleDisplayItem=new HasLigneAssociated();
			var ruleEqp:IRuleDisplayItem=new HasEqpAssociated();
			var ruleSim:IRuleDisplayItem=new HasSimAssociated();
			
			if (SpecificationVO.getInstance().elementDataGridSelected.S_IDTYPE_EQUIP == moduleGestionParcIHM.IDTYPESIM) // pour sim 
			{
				boolAllCollab=false;
				if (ruleLigne.isDisplay() && ruleSim.isDisplay())
					boolLigne=0;
				else
					boolLigne=-1;
				if (ruleEqp.isDisplay())
					boolTerm=0;
				else
					boolTerm=-1;
				isTypeSim=true;
			}
			else if (SpecificationVO.getInstance().elementDataGridSelected.EX_IDTYPE_EQUIP == moduleGestionParcIHM.IDTYPEEXT) // pour extension
			{
				boolAllCollab=false;
				if (ruleLigne.isDisplay())
					boolLigne=0;
				else
					boolLigne=-1;
				if (ruleEqp.isDisplay())
					boolTerm=0;
				else
					boolTerm=-1;
				isTypeSim=false;
			}
		}

		private function initIHM(evt:FlexEvent):void
		{
			initData();
			txtFiltre.setFocus();
			txtFiltre.addEventListener(Event.CHANGE, txtFiltreChangeHandler);

			if (boolTerm == 0)
				colEqp.visible=false;
			else
				colEqp.visible=true;

			if (boolLigne == 0)
				colLigne.visible=false
			else
				colLigne.visible=true;
		}

		public function getSelection():Object
		{
			if (dgListeCollab.selectedIndex != -1)
			{
				return dgListeCollab.selectedItem;
			}
			else
				return null;
		}

		public function getIDSelection():int
		{
			if (dgListeCollab.selectedIndex != -1)
			{
				return dgListeCollab.selectedItem.IDGROUPE_CLIENT;
			}
			else
			{
				return -1;
			}
		}

		public function getDataGrid():DataGrid
		{
			return dgListeCollab;
		}

		public function getLibelleSelection():String
		{
			if (dgListeCollab.selectedIndex != -1)
			{
				return dgListeCollab.selectedItem.NOM;
			}
			else
			{
				return ResourceManager.getInstance().getString('M111', 'Aucune_s_lection');
			}
		}

		public function getLibelleType():String
		{
			return ResourceManager.getInstance().getString('M111', 'Liste_des_collaborateurs');
		}

		public function getSourceIcone():Class
		{
			return adrImg;
		}

		public function libelleSelection():String
		{
			if (dgListeCollab.selectedIndex != -1)
			{
				return dgListeCollab.selectedItem.NOMPRENOM;
			}
			else
			{
				return ResourceManager.getInstance().getString('M111', 'Aucune_s_lection');
			}
		}

		private function initData():void
		{
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_COLLAB, updateDG);
			if (boolAllCollab)
			{
				serv.listeAllCollaborateur();
			}
			else
			{
				serv.listeCollaborateur(boolTerm, boolLigne ,isTypeSim);
			}
		}

		private function updateDG(ev:gestionparcEvent):void
		{
			dgListeCollab.dataProvider=serv.myDatas.dataListeCollab;
			serv.myDatas.dataListeCollab.refresh();
		}

		private function txtFiltreChangeHandler(ev:Event):void
		{
			if (serv.myDatas.dataListeCollab)
			{
				serv.myDatas.dataListeCollab.filterFunction=filtrerLeGrid;
				serv.myDatas.dataListeCollab.refresh();
			}
		}

		private function filtrerLeGrid(item:Object):Boolean
		{
			if ((String(item.NOM).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MATRICULE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (boolTerm && (String(item.LIBELLE_EQ).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)) || (boolLigne && (String(item.LIGNE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)))
				return true
			else
				return false
		}
	}
}
