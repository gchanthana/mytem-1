package gestionparc.ihm.fiches.listedispo
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.liste.ListeService;
	
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;

	[Bindable]
	public class DispoExt extends DispoExtIHM implements IListeDispo
	{
		[Embed(source="/assets/equipement/ext.jpg")]
		private var adrImg:Class;
		
		public var avecLigne:int=1;
		public var avecTerm:int=0;
		public var avecCollab:int=0;
		private var serv:ListeService=new ListeService();

		public function DispoExt(boolVisibleAvecLigne:int=1, boolVisibleAvecTerm:int=0, boolVisibleAvecCollab:int=0)
		{
			this.avecLigne=boolVisibleAvecLigne;
			this.avecTerm=boolVisibleAvecTerm;
			this.avecCollab=boolVisibleAvecCollab;
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		private function initIHM(evt:FlexEvent):void
		{
			initData();
			txtFiltreMyExt.addEventListener(Event.CHANGE, txtFiltreChangeHandler);

			dgListeMyExt.addEventListener(MouseEvent.CLICK, dgVisibleChanged);

			rdWithExt.addEventListener(Event.CHANGE, rdWithExtChangeHandler);
			rdNoneExt.addEventListener(Event.CHANGE, rdNoneExtChangeHandler);

			txtFiltreMyExt.setFocus();

			rdWithExt.visible=(avecLigne == 0)  ? false : true;
			rdWithExt.selected=(avecLigne == 0) ? false : true;
			rdNoneExt.selected=(avecLigne == 0) ? true : false;
		}

		private function dgVisibleChanged(ev:Event):void
		{
			if (dgListeMyExt.selectedIndex > -1)
				dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION));
		}

		private function dgListeOtherSIMHandler(evt:Event):void
		{
			dgListeMyExt.dataProvider=serv.myDatas.dataListeligne; // Declenche un evt donc actualise le champs libelleSelection Dan PanelAffectation.as
		}

		public function getSelection():Object
		{
			var obj:Object=new Object();
			if (dgListeMyExt.selectedItem != null)
			{
				obj=dgListeMyExt.selectedItem;
			}
			return obj;
		}

		public function getLibelleSelection():String
		{
			if (dgListeMyExt.selectedIndex > -1)
			{
				if (dgListeMyExt.selectedItem.EX_NUMERO != null)
				{
					return dgListeMyExt.selectedItem.EX_NUMERO;
				}
				else
				{
					return "";
				}
			}
			else
			{
				return ResourceManager.getInstance().getString('M111', 'Aucune_s_lection');
			}
		}

		public function getDataGrid():DataGrid
		{
			return dgListeMyExt;
		}

		public function getIDSelection():int
		{
			if (dgListeMyExt.selectedIndex != -1)
				return dgListeMyExt.selectedItem.IDEXTENSION;
			else
				return -1;
		}

		public function getLibelleType():String
		{
			return ResourceManager.getInstance().getString('M111', 'Liste_des_extensions_disponibles_');
		}

		public function getSourceIcone():Class
		{
			return adrImg;
		}

		private function initData():void
		{
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_EXTAVECLIGNE, updateDGAvecLigne)
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_EXTSANSLIGNE, updateDGSansLigne)

			if (!avecLigne)
				serv.ListeExtSansLigne(avecTerm, avecCollab);
			else
				serv.ListeExtAvecLigne(avecTerm, avecCollab);
		}

		private function updateDGAvecLigne(ev:gestionparcEvent):void
		{
			dgListeMyExt.dataProvider=serv.myDatas.dataListeExtavecLigne;
			serv.myDatas.dataListeSIMssTerm.refresh();
		}

		private function updateDGSansLigne(ev:gestionparcEvent):void
		{
			dgListeMyExt.dataProvider=serv.myDatas.dataListeExtSansLigne
				;
			serv.myDatas.dataListeSIMssLigne.refresh();
		}

		protected function rdWithExtChangeHandler(e:Event):void
		{
			avecLigne=1;
			txtFiltreMyExt.setFocus();
			initData();
			resetSelection()
		}

		protected function rdNoneExtChangeHandler(e:Event):void
		{
			avecLigne=0;
			txtFiltreMyExt.setFocus();
			initData();
			resetSelection()
		}

		private function resetSelection():void
		{
			dgListeMyExt.selectedIndex=-1;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION));
		}

		private function txtFiltreChangeHandler(ev:Event):void
		{
			try
			{
				dgListeMyExt.dataProvider.filterFunction=filtrerLeGrid;
				dgListeMyExt.dataProvider.refresh();
			}
			catch (e:Error)
			{
			}
		}

		private function filtrerLeGrid(item:Object):Boolean
		{
			if ((String(item.EX_NUMERO).toLowerCase().search(txtFiltreMyExt.text.toLowerCase()) != -1) 
				|| (String(item.LIGNE).toLowerCase().search(txtFiltreMyExt.text.toLowerCase()) != -1) 
				|| (String(item.REVENDEUR).toLowerCase().search(txtFiltreMyExt.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
	}
}
