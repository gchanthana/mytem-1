package gestionparc.ihm.fiches.listedispo
{
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;

	public class LigneFixeDisponibleImpl extends LigneFixeBase
	{
		public function LigneFixeDisponibleImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteIHM);
			serv.myDatas.listeLigneFixeDispo.addEventListener(CollectionEvent.COLLECTION_CHANGE, listeChangeHandler);
			getData();
		}

		override protected function listeChangeHandler(evt:CollectionEvent):void
		{
			if (evt.kind == CollectionEventKind.REFRESH)
			{
				_dataProvider.enableAutoUpdate();
			}
			dataProvider=serv.myDatas.listeLigneFixeDispo;
			s_compteur=_dataProvider.length.toString() + " / " + _real_len.toString() + "  " + ResourceManager.getInstance().getString('M111', 'ligne_s_');
		}

		override protected function getData():void
		{
			serv.ListeLigneFixeDispo();
		}
	}
}
