package gestionparc.ihm.fiches.pool
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.utils.AbstractBaseViewImpl;
	import gestionparc.utils.PanelExclusionInclusionUtils;
	
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class PanelConfirmationInclusionPoolImpl extends AbstractBaseViewImpl
	{
		public var lb_txt_confirmation:Label;
		public var lb_nb_element:Label;
		public var dg_inclusion_elements:DataGrid;
		public var myPanelExclusionInclusionUtils:PanelExclusionInclusionUtils;
		public var infoEltSup:Label;

		public var nbElementPreSelec:int;

		public function PanelConfirmationInclusionPoolImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			myPanelExclusionInclusionUtils=new PanelExclusionInclusionUtils(null, this);

			//textes 
			lb_txt_confirmation.text=ResourceManager.getInstance().getString('M111', 'Attention__vous__tes_sur_le_point_d_incl') + SpecificationVO.getInstance().labelPool + ResourceManager.getInstance().getString('M111', '_les__l_ments_ci_dessous__Confirmez_vous');
		}


		public function validPopup(evt:MouseEvent):void
		{
			myPanelExclusionInclusionUtils.addElementsToPool(SpecificationVO.getInstance().idPool);
		}

		public function cancelPopup(evt:MouseEvent):void
		{
			closePanel(null);
		}

		public function closePanel(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
			this.dispatchEvent(new Event(PanelExclusionInclusionUtils.OPERATION_POOL_CANCELLED));
		}
	}
}
