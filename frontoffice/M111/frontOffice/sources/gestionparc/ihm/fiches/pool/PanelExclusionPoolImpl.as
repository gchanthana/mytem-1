package gestionparc.ihm.fiches.pool
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.utils.AbstractBaseViewImpl;
	import gestionparc.utils.PanelExclusionInclusionUtils;
	
	import mx.collections.ArrayCollection;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;


	[Bindable]
	public class PanelExclusionPoolImpl extends AbstractBaseViewImpl
	{

		public var lb_txt_confirmation:Label;
		public var lb_nb_element:Label;
		public var dg_exclusion_elements:DataGrid;
		public var rb_exclusion:RadioButton;
		public var rb_exclusion_transfert:RadioButton;
		public var cb_pool_gestion:ComboBox;
		public var idPoolSelected:Number;
		public var myPanelExclusionInclusionUtils:PanelExclusionInclusionUtils;

		public var liste_element:ArrayCollection=new ArrayCollection();

		public function PanelExclusionPoolImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			myPanelExclusionInclusionUtils=new PanelExclusionInclusionUtils(this, null);

			//textes 
			lb_txt_confirmation.text=ResourceManager.getInstance().getString('M111', 'Attention__vous__tes_sur_le_point_d_excl') + SpecificationVO.getInstance().labelPool + ResourceManager.getInstance().getString('M111', '_les__l_ments_ci_dessous__Confirmez_vous');

			rb_exclusion.label=ResourceManager.getInstance().getString('M111', 'Simplement_exclure_les__l_ments_du_pool_') + SpecificationVO.getInstance().labelPool;
			rb_exclusion_transfert.label=ResourceManager.getInstance().getString('M111', 'Exclure_du_pool_') + SpecificationVO.getInstance().labelPool + ResourceManager.getInstance().getString('M111', '_et_transf_rer_les__l_ments_dans_le_pool');

			//combobox pool de gestion
			var listePool:ArrayCollection=new ArrayCollection();

			for each (var tmp:Object in CacheDatas.listePool)
			{
				listePool.addItem(tmp);			// copie du ArrayCollection
			}
			
			cb_pool_gestion.dataProvider=listePool;//listePool = GestionParcMobileImpl.poolGestionArrayCol;

			//suppression du pool courant
			var position:int=0;
			var present:Boolean=false;
			for each (var aPool:Object in listePool)
			{
				if (aPool.LIBELLE_POOL == SpecificationVO.getInstance().labelPool)
				{
					present=true;
					break;
				}
				position++;
			}

			if (present)
				listePool.removeItemAt(position);

			if (listePool.length == 1)
				cb_pool_gestion.selectedIndex=0;

			if (listePool.length == 0)
				rb_exclusion_transfert.enabled=false;
		}

		/**
		 *	Sélection du pool de transfert
		 **/
		public function cbPoolGestionHandler(evt:ListEvent):void
		{
			idPoolSelected=evt.currentTarget.selectedItem.IDPOOL;
			if (idPoolSelected == SpecificationVO.getInstance().idPool)
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Le_pool_s_lectionn__pour_le_transfert_de'), ResourceManager.getInstance().getString('M111', 'Attention__'));
			}
		}

		public function validPopup(evt:MouseEvent):void
		{
			if (rb_exclusion.selected)
			{
				myPanelExclusionInclusionUtils.removeElementsFromPoolV2(buildXml());
			}
			else if (rb_exclusion_transfert.selected)
			{
				if (cb_pool_gestion.selectedIndex != -1)
				{
					myPanelExclusionInclusionUtils.moveElementsFromPoolV2(cb_pool_gestion.selectedItem.IDPOOL, buildXml());
				}
				else
				{
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_un_pool_pour_eff'), ResourceManager.getInstance().getString('M111', 'Attention__'));
				}
			}
		}

		private function buildXml():String
		{
			var xml:String="<elements>";
			for each (var obj:AbstractMatriceParcVO in dg_exclusion_elements.dataProvider)
			{
				xml+="<element><id>" + obj.ID + "</id></element>";
			}
			xml+="</elements>";
			return xml;
		}

		public function cancelPopup(evt:MouseEvent):void
		{
			closePanel(null);
		}

		public function closePanel(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
	}
}