package gestionparc.ihm.fiches.ligne
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.FicheAffectationIHM;
	import gestionparc.ihm.fiches.listedispo.DispoEqp;
	import gestionparc.ihm.question.ligne.QuestionAssocierEqpToLigne;
	import gestionparc.utils.gestionparcConstantes;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class FicheAssocierEqpToLigne extends FicheAffectationIHM
	{
		public var liste:DispoEqp;
		private var qn:QuestionAssocierEqpToLigne;

		public function FicheAssocierEqpToLigne()
		{
			super();
			liste=new DispoEqp(false);
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(ev:FlexEvent):void
		{
			initListeners();
			initIHM();
		}

		private function initListeners():void
		{
			liste.addEventListener(MouseEvent.CLICK, dgListeDispo_handler);
			liste.addEventListener(gestionparcEvent.REFRESH_LIBELLESELECTION, dgListeDispo_handler);
			btAnnuler.addEventListener(MouseEvent.CLICK, fermer);
			btValider.addEventListener(MouseEvent.CLICK, valider);
			this.addEventListener(CloseEvent.CLOSE, fermer);
		}

		private function initIHM():void
		{
			this.title=ResourceManager.getInstance().getString("M111", "ASSOCIER_EQPTOLIGNE");

			this.img_cellule.source=gestionparcConstantes.adrImgLigne;
			this.lab_cellule_name.text=SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE

			this.img_liste.source=gestionparcConstantes.adrImgTerm;
			this.img_liste_big.source=gestionparcConstantes.adrImgTerm;
			this.lab_titre.text=liste.getLibelleType()

			this.box_listeDispo.addChild(liste as VBox);
		}

		private function dgListeDispo_handler(evt:Event):void
		{
			labDgSelection.text=liste.getLibelleSelection();
		}

		protected function fermer(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function valider(e:Event):void
		{
			if (liste.getIDSelection() > -1)
			{
				qn=new QuestionAssocierEqpToLigne(liste, check_pret.selected, (calendar.selectedDate != null) ? calendar.selectedDate : new Date());
				qn.serv.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED, fermer);
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un__l_ment_dans_la_liste'));
		}
	}
}
