package gestionparc.ihm.fiches.ligne
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.ligne.LigneServices;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class FicheRenumeroterLigneImpl extends TitleWindow
	{
		public var tiLigne:TextInput;
		public var btnValider:Button;
		public var btnAnnuler:Button;

		private var _service:LigneServices=new LigneServices();

		public function FicheRenumeroterLigneImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			initListener(true);
		}

		private function initListener(bool:Boolean):void
		{
			if (bool)
			{
				_service.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED, annuler);
				btnAnnuler.addEventListener(MouseEvent.CLICK, annuler);
				btnValider.addEventListener(MouseEvent.CLICK, valider);
			}
			else
			{
				btnAnnuler.removeEventListener(MouseEvent.CLICK, annuler);
				btnValider.removeEventListener(MouseEvent.CLICK, valider);
			}
		}

		private function annuler(evt:Event=null):void
		{
			PopUpManager.removePopUp(this);
		}

		private function valider(evt:MouseEvent=null):void
		{
			var boolValid:Boolean=true;
			if (tiLigne.text != null && tiLigne.text != "")
			{
				if (tiLigne.text != SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE)
				{
					_service.renumeroterLigne(setObj());
				}
				else
					boolValid=false;
			}
			else
				boolValid=false;

			if (!boolValid)
				Alert.show(ResourceManager.getInstance().getString("M111", "Vous_devez_sp_cifier_un_numero_de_ligne_diff_rent_de_l_initial__"), ResourceManager.getInstance().getString("M111", "Erreur"));
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.ID=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.SOUSTETE=tiLigne.text;
			obj.oldNumero=SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE;
			return obj;
		}
	}
}