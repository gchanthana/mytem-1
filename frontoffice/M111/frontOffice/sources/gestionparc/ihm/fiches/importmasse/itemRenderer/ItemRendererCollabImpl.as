package gestionparc.ihm.fiches.importmasse.itemRenderer
{	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataCollab;
	import gestionparc.ihm.fiches.importmasse.renderer.CellRendererHelper;
	import gestionparc.services.collaborateur.CollaborateurDatas;
	import gestionparc.utils.CustomDatagrid;
	import gestionparc.utils.preloader.Spinner;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Image;
	import mx.controls.TextInput;
	import mx.controls.ToolTip;
	import mx.controls.listClasses.BaseListData;
	import mx.controls.listClasses.IDropInListItemRenderer;
	import mx.events.FlexEvent;
	import mx.managers.IFocusManagerComponent;
	import mx.managers.ToolTipManager;
	import mx.resources.ResourceManager;
	
	
	public class ItemRendererCollabImpl extends HBox implements IDropInListItemRenderer, IFocusManagerComponent
	{		
		
		//--------------- VARIABLES ----------------//
		
		public var maxCharTimer			:Timer 		= new Timer(_maxCharsDelayTimer, 0);
		public var spin					:Spinner 	= new Spinner();
		public var img					:Image		= new Image();
		public var tooltip				:ToolTip;
		public var tooltipMaxChar		:ToolTip;
		public var tiMatricule			:TextInput;
		public var tiNom				:TextInput;
		public var tiPrenom				:TextInput;
		public var msgTooltip			:String 	= "";
		public var msgTooltipMaxChar	:String		= "";
		public var colorTlp				:String 	= "";
		public var redBackTltp			:String		= "0xC00000";
		public var greenBackTltp		:String		= "0x68AC3A";
		public var strMaxChar			:String		= ResourceManager.getInstance().getString('M111', 'Nombre_de_caract_res_maximum_atteint_max___');
		public var strMaxCharEnd		:String		= ResourceManager.getInstance().getString('M111', '___ou_caract_res_non_autoris_s');
		public var strRefExist			:String 	= ResourceManager.getInstance().getString('M111', 'Attention___Cette_cl__est_d_j__pr_sente_dans_le_parc');
		public var strRefAuto			:String		= ResourceManager.getInstance().getString('M111', 'Matricule_saisit_automatiquement__');
		public var strInputEmpty		:String 	= ResourceManager.getInstance().getString('M111', 'Champ_s__obligatoire_s_');
		
		public var thisItemRenderer		:DisplayObjectContainer;
		
		[Bindable]
		public var maxcharsMatricule	:int = 200;
		[Bindable]
		public var maxcharsNom			:int = 200;
		[Bindable]
		public var maxcharsPrenom		:int = 200;
		[Bindable]
		public var matriculeCollab		:String = "";
		[Bindable]
		public var nomCollab			:String = "";
		[Bindable]
		public var prenomCollab			:String = "";
		
		[Embed(source="/assets/images/warningSaisie.png")]
		public var imgSaisieWarning:Class;
		[Embed(source="/assets/images/okSaisie.png")]
		public var imgSaisieOk:Class;
		
		private var _maxCharsDelayTimer	:Number	= 1500;
		
		private var _checkData			:CheckingEtatDataCollab;
		private var _listData			:BaseListData;
		private var _arrayData			:ArrayCollection = new ArrayCollection();
		
		public var dg:CustomDatagrid;
		
		
		//---------------- METHODES ----------------//
		
		/* constructeur */
		public function ItemRendererCollabImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		/* initialisation */
		private function init(fle:FlexEvent):void
		{
			// saisie
			tiMatricule.addEventListener(Event.CHANGE, tiCollabHandler);
			tiNom.addEventListener(Event.CHANGE, tiCollabHandler);
			tiPrenom.addEventListener(Event.CHANGE, tiCollabHandler);
			// delai timer saisie
			maxCharTimer.addEventListener(TimerEvent.TIMER, maxCharTimerHandler);
			// gestion tabulations
			tiMatricule.addEventListener(KeyboardEvent.KEY_DOWN, focusHandler);
			tiPrenom.addEventListener(KeyboardEvent.KEY_DOWN, focusHandler);
			tiNom.addEventListener(KeyboardEvent.KEY_DOWN, focusHandler);
			tiNom.addEventListener("GOTO_NEXT_ITEMRENDERER_AVAILABLE", focusNextHandler);
			tiNom.addEventListener("GOTO_PREVIOUS_ITEMRENDERER_AVAILABLE", focusPreviousHandler);
			
			tiMatricule.addEventListener(MouseEvent.CLICK, clickHandler);
			tiPrenom.addEventListener(MouseEvent.CLICK, clickHandler);
			tiNom.addEventListener(MouseEvent.CLICK, clickHandler);
//			tiMatricule.addEventListener(FocusEvent.FOCUS_IN,focusInInputA);
//			tiPrenom.addEventListener(FocusEvent.FOCUS_IN,focusInInputA);
//			tiNom.addEventListener(FocusEvent.FOCUS_IN,focusInInput);
			
			_arrayData = (listData.owner as CustomDatagrid).dataProvider as ArrayCollection;
			_checkData = new CheckingEtatDataCollab(this);
			
			//attacher object à l'item renderer
			thisItemRenderer = this as DisplayObjectContainer;
			spin.size = 15;
			thisItemRenderer.addChild(spin);
			thisItemRenderer.addChild(img);
			
			dg = listData.owner as CustomDatagrid;
		}
		
		/* */
		private function clickHandler(me:MouseEvent):void
		{
//			dg.editable = true;
			// detecter les colonnes editables/non editables sur le row cliqué
//			CellRendererHelper.setEditableColumn(listData);
		}
		
		/* */
		private function focusHandler(ke:KeyboardEvent):void
		{
			CellRendererHelper.setEditableColumn(listData);
			
			// tabulation en avant
			if(!ke.shiftKey && ke.keyCode == Keyboard.TAB)
			{
				dg.editable = false;
				if(ke.currentTarget.id == "tiMatricule")
				{
					focusManager.setFocus(tiPrenom);
				}
				else if(ke.currentTarget.id == "tiPrenom")
				{
					focusManager.setFocus(tiNom);
				}
				if(ke.currentTarget.id == "tiNom")
				{
					dg.editable = true;
					CellRendererHelper.gotToNextEditableCell(listData);
				}
			}
			// tabulation en arriere
			if(ke.shiftKey && ke.keyCode==Keyboard.TAB)
			{
				dg.editable = true;
				if(ke.currentTarget.id == "tiNom")
				{
					focusManager.setFocus(tiPrenom);
				}
				else if(ke.currentTarget.id == "tiPrenom")
				{
					dg.editable = false;
					focusManager.setFocus(tiMatricule);
				}
				if(ke.currentTarget.id == "tiMatricule")
				{
					dg.editable = false;
					CellRendererHelper.gotToPreviousEditableCell(listData);
				}
			}
		}
		
		/* */
		private function focusNextHandler(evt:Event):void
		{
			focusManager.setFocus(tiMatricule);
		}
		
		/* */
		private function focusPreviousHandler(evt:Event):void
		{
			focusManager.setFocus(tiNom);
		}
		
		/* appelé lors d'import ou scroll */
		override public function set data(value:Object):void
		{
			tiMatricule.text = "";
			tiNom.text = "";
			tiPrenom.text = "";
			matriculeCollab = "";
			nomCollab = "";
			prenomCollab = "";
			
			if(value == null) return;
			super.data = value;
						
			if(data.COLLAB_TO_IMPORT)
			{
				tiMatricule.visible = true;
				tiNom.visible = true;
				tiPrenom.visible = true;
			}
			else
			{
				tiMatricule.visible = false;
				tiNom.visible = false;
				tiPrenom.visible = false;
				undisplayIMG();
			}
			
			matriculeCollab = data.MATRICULE;
			nomCollab = data.NOM;
			prenomCollab = data.PRENOM;
			
			colorEmptyInput(); //style input
			
			if(!data.COLLAB_EN_SAISIE)
				handleMaxCharListeners();// maxchar handler
			
			// gerer affichage des images
			if(data.ETAT_COLLAB && data.COLLAB_TO_IMPORT)
				displayImgEtat(data.ETAT_COLLAB);
		}
		
		/* */
		private function tiCollabHandler(evt:Event):void
		{
			stopTimer();
			removeListeners();
			undisplayIMG();
			undisplayTooltip();
			
			data.MATRICULE = tiMatricule.text;
			data.NOM = tiNom.text;
			data.PRENOM = tiPrenom.text;
//			data.ISVERIF_AVAILABLE = true;
			data.COLLAB_EN_SAISIE = true;
			
			handleMaxCharListeners();
			
			_checkData.processCheckingEtatData();
		}
		
		/* */
		private function displayImgEtat(etat:int):void
		{
			undisplayIMG();
			undisplayTooltip();
			
			var boolOkSaisie:Boolean = false;
			
			switch(etat)
			{
				case 2:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strMaxChar + maxcharsNom + strMaxCharEnd;
					colorTlp = redBackTltp;
					data.COLLAB_OK = false;
					break;
				case 4:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strMaxChar + maxcharsNom + strMaxCharEnd;
					colorTlp = redBackTltp;
					data.COLLAB_OK = false;
					break;
				case 5:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strMaxChar + maxcharsNom + strMaxCharEnd;
					colorTlp = redBackTltp;
					data.COLLAB_OK = false;
					break;
				case 6:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strRefExist;
					colorTlp = redBackTltp;
					data.COLLAB_OK = true;
					break;
				case 7:
					img.source = imgSaisieOk;
					img.buttonMode = false;
					img.visible = true;
					img.includeInLayout = true;
					boolOkSaisie = true;
					colorTlp = greenBackTltp;
					data.COLLAB_OK = true;
					break;
				case 8:
					img.source = imgSaisieOk;
					img.buttonMode = false;
					img.visible = true;
					img.includeInLayout = true;
					boolOkSaisie = true;
					colorTlp = greenBackTltp;
					data.COLLAB_OK = true;
					break;
				case 9:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strInputEmpty;
					colorTlp = redBackTltp;
					data.COLLAB_OK = false;
					break;
				case 11:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strMaxChar + maxcharsNom + strMaxCharEnd;
					colorTlp = redBackTltp;
					data.COLLAB_OK = false;
					break;
				case -1:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = ResourceManager.getInstance().getString('M111', 'Erreur_lors_de_la_v_rification');
					colorTlp = redBackTltp;
					data.COLLAB_OK = false;
					break;
				case -10:
					spin.play();
					spin.visible = true;
					spin.includeInLayout = true;
					data.COLLAB_OK = false;
					break;
				default:break;
			}
			
			dispatchEvent(new Event("CHECK_BTN_VALID", true));
			
			//remove eventuels listeners existants
			img.removeEventListener(MouseEvent.ROLL_OVER, afficheTooltip);
			img.removeEventListener(MouseEvent.ROLL_OUT, supTooltip);
			if(!boolOkSaisie)
			{
				//ajout de listeners
				img.addEventListener(MouseEvent.ROLL_OVER, afficheTooltip);
				img.addEventListener(MouseEvent.ROLL_OUT, supTooltip);
			}
		}
		
		/* */
		private function handleMaxCharListeners():void
		{
			tiMatricule.removeEventListener(TextEvent.TEXT_INPUT, maxCharsHandler);
			tiNom.removeEventListener(TextEvent.TEXT_INPUT, maxCharsHandler);
			tiPrenom.removeEventListener(TextEvent.TEXT_INPUT, maxCharsHandler);
			
			// ajoute un listener si le maxChar est atteint
			if(tiMatricule.text.length == maxcharsMatricule)
				tiMatricule.addEventListener(TextEvent.TEXT_INPUT, maxCharsHandler);
			
			// ajoute un listener si le maxChar est atteint
			if(tiNom.text.length == maxcharsNom)
				tiNom.addEventListener(TextEvent.TEXT_INPUT, maxCharsHandler);
			
			// ajoute un listener si le maxChar est atteint
			if(tiPrenom.text.length == maxcharsPrenom)
				tiPrenom.addEventListener(TextEvent.TEXT_INPUT, maxCharsHandler);
		}
		
		/* affiche le tooltip maxChars dès la saisie du maxChar + 1 */
		private function maxCharsHandler(te:TextEvent):void
		{
			// stopper le timer pour tooltip maxChars si il est lancé
			maxCharTimer.stop();
			
			msgTooltipMaxChar = strMaxChar + maxcharsNom + strMaxCharEnd;
			afficheTooltipMaxChar();
			
			// débuter le timer pour tooltip maxChars
			maxCharTimer.start();
		}
		
		/* à la fin du temps imparti par le Timer - Handler */
		private function maxCharTimerHandler(te:TimerEvent):void
		{
			maxCharTimer.stop();
			
			if(tooltipMaxChar != null) 
				supTooltipMaxChar();
		}
		
		/* verif input empty */
		private function colorEmptyInput():void
		{
//			if(data.ISVERIF_AVAILABLE)
//			{
				if((data.MATRICULE == "" && CollaborateurDatas.matriculeIsActif == 0) || (data.MATRICULE as String).length > maxcharsMatricule)
					tiMatricule.setStyle("borderColor", "0xC00000");
				else
					tiMatricule.setStyle("borderColor", "0xD3D5D6");
				
				if(data.NOM == "" || (data.NOM as String).length > maxcharsNom)
					tiNom.setStyle("borderColor", "0xC00000");
				else
					tiNom.setStyle("borderColor", "0xD3D5D6");
				
				if(data.PRENOM == "" || (data.PRENOM as String).length > maxcharsPrenom)
					tiPrenom.setStyle("borderColor", "0xC00000");
				else
					tiPrenom.setStyle("borderColor", "0xD3D5D6");
//			}
//			else
//			{
//				tiMatricule.setStyle("borderColor","0xD3D5D6");
//				tiNom.setStyle("borderColor","0xD3D5D6");
//				tiPrenom.setStyle("borderColor","0xD3D5D6");
//			}
		}
		
		/* creation tooltip - Handler */
		protected function afficheTooltip(evt:MouseEvent):void
		{
			var img:Image = evt.currentTarget as Image;
			var p:Point = new Point(img.x, img.y);
			p = img.contentToGlobal(p);
			
			// créer un tooltip
			supTooltip(null);
			tooltip = ToolTipManager.createToolTip(msgTooltip, (p.x - img.x + 15), p.y) as ToolTip;
			tooltip.setStyle("backgroundColor", "0xC00000");
			tooltip.setStyle("cornerRadius", 5);
			tooltip.setStyle("fontWeight", "bold");
			tooltip.setStyle("color", "0xFFFFFF");
			tooltip.setStyle("paddingTop", 5);
			tooltip.setStyle("paddingLeft", 5);
			tooltip.setStyle("paddingRight", 5);
			tooltip.setStyle("paddingBottom", 5);			
		}
		
		/* */
		protected function supTooltip(event:MouseEvent):void
		{
			if(tooltip != null)
			{
				ToolTipManager.destroyToolTip(tooltip);
				tooltip = null;
			}
		}
		
		/* creation tooltip - Handler */
		protected function afficheTooltipMaxChar():void
		{		
			var obj:Object;
			obj = (img.includeInLayout) ? img : spin;
			var p:Point = new Point(obj.x, obj.y);
			p = img.contentToGlobal(p);
			
			// créer un tooltip
			supTooltipMaxChar();
			tooltipMaxChar = ToolTipManager.createToolTip(msgTooltipMaxChar,(p.x - img.x + 15), p.y) as ToolTip;
			tooltipMaxChar.setStyle("backgroundColor", "0xC00000");
			tooltipMaxChar.setStyle("cornerRadius", 5);
			tooltipMaxChar.setStyle("fontWeight", "bold");
			tooltipMaxChar.setStyle("color", "0xFFFFFF");
			tooltipMaxChar.setStyle("paddingTop", 5);
			tooltipMaxChar.setStyle("paddingLeft", 5);
			tooltipMaxChar.setStyle("paddingRight", 5);
			tooltipMaxChar.setStyle("paddingBottom", 5);			
		}
		
		/* */
		protected function supTooltipMaxChar():void
		{
			if(tooltipMaxChar != null)
			{
				ToolTipManager.destroyToolTip(tooltipMaxChar);
				tooltipMaxChar = null;
			}
		}
		
		/* */
		private function stopTimer():void
		{
			maxCharTimer.stop();
		}
		
		/* */
		private function undisplayIMG():void
		{
			img.includeInLayout = false;
			img.visible = false;
			spin.includeInLayout = false;
			spin.visible = false;
		}
		
		/* */
		private function undisplayTooltip():void
		{
			if(tooltip != null){
				ToolTipManager.destroyToolTip(tooltip); 
				tooltip = null;
			}
			if(tooltipMaxChar != null){
				ToolTipManager.destroyToolTip(tooltipMaxChar); 
				tooltipMaxChar = null;
			}
		}
		
		/* */
		private function removeListeners():void
		{
			img.removeEventListener(MouseEvent.ROLL_OVER, afficheTooltip);
			img.removeEventListener(MouseEvent.ROLL_OUT, supTooltip);
		}
		
		
		
		//------------------ GETETRS - SETTERS ------------------//
		
		public function get listData():BaseListData
		{
			return _listData;
		}
		
		public function set listData(value:BaseListData):void
		{
			_listData = value;
		}

		public function get arrayData():ArrayCollection
		{
			return _arrayData;
		}

		public function set arrayData(value:ArrayCollection):void
		{
			_arrayData = value;
		}
	}
}