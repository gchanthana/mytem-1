package gestionparc.ihm.fiches.importmasse.checkingEtatDataClass
{
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import gestionparc.entity.ImportDeMasseVO;
	import gestionparc.event.ImportDeMasseEvent;
	import gestionparc.ihm.fiches.importmasse.itemRenderer.ItemRendererNumLigneImpl;
	import gestionparc.services.importmasse.ImportMasseServices;


	public class CheckingEtatDataNumLigne
	{
		
		//--------------- VARIABLES ----------------//
		
		private var _afterDoublonTimer		:Timer 	= new Timer(_afterDoublonDelayTimer,0);
		private var _afterDoublonDelayTimer	:Number = 1500;
		private var _maxcharsNumLigne		:int 	= 10;
		
		private var _itemData1				:ImportDeMasseVO;
		private var _itemData2				:ImportDeMasseVO;
		private var _itemImp				:ImportDeMasseVO;
		private var _objImp					:ImportMasseServices;
		
		private var _itemRend				:ItemRendererNumLigneImpl;
		
		
		//--------------- METHODES ----------------//
		
		/* */
		public function CheckingEtatDataNumLigne(itemRend:ItemRendererNumLigneImpl)
		{
			_objImp = new ImportMasseServices();
			this._itemRend = itemRend;
		}
		
		/* */
		public function checkEtatData(obj:ImportDeMasseVO):void
		{
			if(obj.NUMLIGNE.length > 100)
			{
				obj.ETAT_NUMLIGNE = 11; //etat trop long (à l'import)
			}
			else if(obj.NUMLIGNE.length == 0)
			{
				obj.ETAT_NUMLIGNE = 9; //etat champs vide (à l'import)
			}
			else
			{
				obj.ETAT_NUMLIGNE = -10 //etat spinner
				callServiceCheckEtat(obj);
			}
			_itemImp = obj;
		}
		
		/* */
		public function callServiceCheckEtat(obj:ImportDeMasseVO):void
		{
//			removeListener();
			
			_objImp.checkNLIGNE(obj.NUMLIGNE);
			_objImp.myDatas.addEventListener(ImportDeMasseEvent.CHECKED_NUMLIGNE,checkNLIGNEHandler);
		}
		
		/* */
		public function processCheckingEtatData():void
		{
			stopTimer();
			removeListener();
			
			//si maxChar de l'input est dépassé à l'import
			if((_itemRend.data.NUMLIGNE as String).length > _maxcharsNumLigne)
			{
				_itemRend.data.ETAT_NUMLIGNE = -100;
				_itemRend.data.ETAT_NUMLIGNE = 11;
			}
			else
			{
				//si input vide
				if(_itemRend.data.NUMLIGNE=="")
					_itemRend.data.ETAT_NUMLIGNE = 9;
				
				//si input contient au moins un caractere
				if(_itemRend.data.NUMLIGNE!="")
					findDoublonInListe();
			}
		}
		
		/* */
		private function findDoublonInListe():void
		{			
			var hasDoublon:Boolean = false;
			
			// reperer tous les doublons sur la saisie courante
			for each(var item:ImportDeMasseVO in _itemRend.arrayData)
			{
				if(_itemRend.data!=item && _itemRend.data.NUMLIGNE!="" && _itemRend.data.NUMLIGNE==item.NUMLIGNE)
				{
					_itemRend.data.ETAT_NUMLIGNE = 4; //etat doublon
					item.ETAT_NUMLIGNE = 4;
					_itemRend.data.DOUBLON_NUMLIGNE = true;
					item.DOUBLON_NUMLIGNE = true;
					hasDoublon = true;
				}
			}
			// si le input current n'a pas de doublon
			if (!hasDoublon)
				_itemRend.data.DOUBLON_NUMLIGNE = false;
			
			//on verifie si l'ancienne valeur du TI courant a encore des doublons
			findDoublonOnPreviousValue();
			
			//si aucun doublon sur la saisie -> appel au service de verif
			if (!_itemRend.data.DOUBLON_NUMLIGNE)
			{
				_afterDoublonTimer.addEventListener(TimerEvent.TIMER, checkEtatHandler);
				_afterDoublonTimer.start();
			}
		}
		
		/* */
		private function findDoublonOnPreviousValue():void
		{
			var cpt:int = 0;
			_itemData1 = new ImportDeMasseVO();
			
			// reperer tous les doublons sur l'ancien dernier doublon de la saisie courante
			for each(var item:ImportDeMasseVO in _itemRend.arrayData)
			{
				if ((_itemRend.data as ImportDeMasseVO).OLD_NUMLIGNE!="" && (_itemRend.data as ImportDeMasseVO).OLD_NUMLIGNE == item.NUMLIGNE)
				{
					item.ETAT_NUMLIGNE=4;
					_itemData1 = item;
					cpt++;
				}
			}
			// si il n'y a pas de doublon sur OLD value du TI
			//on appelle le service pour verifier son etat
			if (cpt==1)
			{
				_itemData1.DOUBLON_NUMLIGNE = false;
//				_itemData1.checkCurrentData(this);
				checkEtatData(_itemData1);
			}
		}
		
		/* */
		private function checkEtatHandler(evt:Event):void
		{
			_afterDoublonTimer.removeEventListener(TimerEvent.TIMER, checkEtatHandler);
			processVerifEtat();
		}
		
		/* appel du service pour verif */
		protected function processVerifEtat():void
		{
			_itemData2 = new ImportDeMasseVO();
			_itemData2 = _itemRend.data as ImportDeMasseVO;
//			_itemData2.checkCurrentData(this);
			checkEtatData(_itemData2);
		}
		
		/* retour des verifs - Handler */
		public function checkNLIGNEHandler(impe:ImportDeMasseEvent):void
		{
			if (!_itemImp.DOUBLON_NUMLIGNE)
			{
				_itemImp.ETAT_NUMLIGNE = int(impe.objRet);
				
				if(int(impe.objRet)==1 || int(impe.objRet)==3)
					_itemImp.NUMLIGNE_OK=true;
				else
					_itemImp.NUMLIGNE_OK=false;
			}
		}
		
		/* */
		private function removeListener():void
		{
			_objImp.myDatas.removeEventListener(ImportDeMasseEvent.CHECKED_NUMLIGNE,checkNLIGNEHandler);
		}
		
		/* */
		private function stopTimer():void
		{
			_afterDoublonTimer.stop();
		}
	}
}