package gestionparc.ihm.fiches.importmasse.itemRenderer
{
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataNumLigne;
	import gestionparc.ihm.fiches.importmasse.renderer.CellRendererHelper;
	import gestionparc.utils.CustomDatagrid;
	import gestionparc.utils.preloader.Spinner;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Image;
	import mx.controls.TextInput;
	import mx.controls.ToolTip;
	import mx.controls.listClasses.BaseListData;
	import mx.controls.listClasses.IDropInListItemRenderer;
	import mx.events.FlexEvent;
	import mx.managers.IFocusManagerComponent;
	import mx.managers.ToolTipManager;
	import mx.resources.ResourceManager;
	
	
	public class ItemRendererNumLigneImpl extends HBox implements IDropInListItemRenderer,IFocusManagerComponent
	{
		//--------------- VARIABLES ----------------//
		
		public var maxCharTimer			:Timer 		= new Timer(_maxCharsDelayTimer,0);
		public var spin					:Spinner 	= new Spinner();
		public var img					:Image		= new Image();
		public var tooltip				:ToolTip;
		public var tooltipMaxChar		:ToolTip;
		public var tiNumLigne			:TextInput;
		public var msgTooltip			:String 	= "";
		public var msgTooltipMaxChar	:String		= "";
		public var colorTlp				:String 	= "";
		public var redBackTltp			:String		= "0xC00000";
		public var greenBackTltp		:String		= "0x68AC3A";
		public var strMaxChar			:String		= ResourceManager.getInstance().getString('M111', 'Nombre_de_caract_res_maximum_atteint_max___');
		public var strMaxCharEnd		:String		= ResourceManager.getInstance().getString('M111', '___ou_caract_res_non_autoris_s');
		public var strRefExist			:String 	= ResourceManager.getInstance().getString('M111', 'Attention___Cette_cl__est_d_j__pr_sente_dans_le_parc');
		public var strRefExistOtherRac	:String		= ResourceManager.getInstance().getString('M111', 'Num_ro_d_j__pr_sent_dans_la_base_pour_une_autre_racine');
		public var strInputEmpty		:String 	= ResourceManager.getInstance().getString('M111', 'Champ_obligatoire');
		public var strDoublon			:String 	= ResourceManager.getInstance().getString('M111', 'Cette_r_f_rence_est_en_doublon');
		
		public var thisItemRenderer		:DisplayObjectContainer;
		
		[Bindable]
		public var maxcharsNumLigne	:int = 100;
		[Bindable]
		public var numLigne:String = "";
		
		[Embed(source="/assets/images/warningSaisie.png")]
		public var imgSaisieWarning:Class;
		[Embed(source="/assets/images/okSaisie.png")]
		public var imgSaisieOk:Class;
		
		private var _maxCharsDelayTimer		:Number	= 1500;
		
		private var _checkData			:CheckingEtatDataNumLigne;
		private var _listData			:BaseListData;
		private var _arrayData			:ArrayCollection = new ArrayCollection();
		
		public var dg:CustomDatagrid;
		
		
		//--------------- METHODES ----------------//
		
		/* */
		public function ItemRendererNumLigneImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/* */
		private function init(fe:FlexEvent):void
		{
			// saisie
			tiNumLigne.addEventListener(Event.CHANGE,tiNumLigneHandler);
			// delai timer saisie
			maxCharTimer.addEventListener(TimerEvent.TIMER,maxCharTimerHandler);
			//tabulation
			tiNumLigne.addEventListener("GOTO_NEXT_ITEMRENDERER_AVAILABLE",focusHandler);
			tiNumLigne.addEventListener("GOTO_PREVIOUS_ITEMRENDERER_AVAILABLE",focusHandler);
			
			tiNumLigne.addEventListener(KeyboardEvent.KEY_DOWN,keyDownTabHandler);
			tiNumLigne.addEventListener(MouseEvent.CLICK,clickHandler);
			
			_arrayData = (listData.owner as CustomDatagrid).dataProvider as ArrayCollection;
			_checkData = new CheckingEtatDataNumLigne(this);
			
			//attacher object à l'item renderer
			thisItemRenderer = this as DisplayObjectContainer;
			spin.size = 15;
			thisItemRenderer.addChild(spin);
			thisItemRenderer.addChild(img);
			
			dg = listData.owner as CustomDatagrid;
		}
		
		/* */
		private function clickHandler(me:MouseEvent):void
		{
			dg.editable = true;
			// detecter les colonnes editables/non editables sur le row cliqué
//			CellRendererHelper.setEditableColumn(listData);
		}
		
		/* */
		private function keyDownTabHandler(ke:KeyboardEvent):void
		{
			CellRendererHelper.setEditableColumn(listData);
			
			// tabulation en avant
			if(!ke.shiftKey && ke.keyCode==Keyboard.TAB)
			{
				CellRendererHelper.gotToNextEditableCell(listData);
			}
			// tabulation en arriere
			if(ke.shiftKey && ke.keyCode==Keyboard.TAB)
			{
				CellRendererHelper.gotToPreviousEditableCell(listData);
			}
		}

		/* */
		private function focusHandler(evt:Event):void
		{
			focusManager.setFocus(tiNumLigne);
		}
		
		/* */
		override public function set data(value:Object):void
		{
			tiNumLigne.text = "";
			numLigne = "";
			
			if(value==null) return;
			super.data = value;
			
			if(data.NUMLIGNE_TO_IMPORT)
			{
				tiNumLigne.visible = true;
			}
			else
			{
				tiNumLigne.visible = false;
				undisplayIMG();
			}
			
			numLigne = data.NUMLIGNE;
			
			colorEmptyInput(); //style input
			
			if(!data.NUMLIGNE_EN_SAISIE)
				handleMaxCharListeners();// maxchar handler
			
			// gerer affichage des images
			if(/*data.ETAT_NUMLIGNE && */data.NUMLIGNE_TO_IMPORT)
				displayImgEtat(data.ETAT_NUMLIGNE);
		}
		
		/* */
		private function tiNumLigneHandler(te:Event):void
		{
			stopTimer();
			removeListeners();
			undisplayIMG();
			undisplayTooltip();
			
			data.OLD_NUMLIGNE = data.NUMLIGNE;
			data.NUMLIGNE = tiNumLigne.text;
//			data.ISVERIF_AVAILABLE = false;
			data.NUMLIGNE_EN_SAISIE = true;
			
			handleMaxCharListeners();
			
			_checkData.processCheckingEtatData();
		}
		
		/* */
		private function displayImgEtat(etat:int):void
		{
			undisplayIMG();
			undisplayTooltip();
			
			var boolOkSaisie:Boolean = false;
			
			switch(etat)
			{
				case 0:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strRefExistOtherRac;
					colorTlp = redBackTltp;
					data.NUMLIGNE_OK = false;
					break;
				case 1:
					img.source = imgSaisieOk;
					img.buttonMode = false;
					img.visible = true;
					img.includeInLayout = true;
					boolOkSaisie = true;
					colorTlp = greenBackTltp;
					data.NUMLIGNE_OK = true;
					break;
				case 2:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strMaxChar + maxcharsNumLigne + strMaxCharEnd;
					colorTlp = redBackTltp;
					data.NUMLIGNE_OK = false;
					break;
				case 3:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strRefExist;
					colorTlp = redBackTltp;
					data.NUMLIGNE_OK = true;
					break;
				case 4:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strDoublon;
					colorTlp = redBackTltp;
					data.NUMLIGNE_OK = false;
					break;
				case 9:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strInputEmpty;
					colorTlp = redBackTltp;
					data.NUMLIGNE_OK = false;
					break;
				case 11:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = strMaxChar + maxcharsNumLigne + strMaxCharEnd;
					colorTlp = redBackTltp;
					data.NUMLIGNE_OK = false;
					break;
				case -1:
					img.source = imgSaisieWarning;
					img.buttonMode = false;
					img.visible = true;
					img.includeInLayout = true;
					msgTooltip = ResourceManager.getInstance().getString('M111', 'Erreur_lors_de_la_v_rification');
					colorTlp = redBackTltp;
					data.NUMLIGNE_OK = false;
					break;
				case -10:
					spin.play();
					spin.visible = true;
					spin.includeInLayout = true;
					data.NUMLIGNE_OK = false;
					break;
				default:break;
			}
			
			dispatchEvent(new Event("CHECK_BTN_VALID",true));
			
			//remove eventuels listeners existants
			img.removeEventListener(MouseEvent.ROLL_OVER,afficheTooltip);
			img.removeEventListener(MouseEvent.ROLL_OUT,supTooltip);
			if(!boolOkSaisie)
			{
				//ajout de listeners
				img.addEventListener(MouseEvent.ROLL_OVER,afficheTooltip);
				img.addEventListener(MouseEvent.ROLL_OUT,supTooltip);
			}
		}
		
		/* */
		private function handleMaxCharListeners():void
		{
			tiNumLigne.removeEventListener(TextEvent.TEXT_INPUT,maxCharsHandler);
			// ajoute un listener si le maxChar est atteint
			if(tiNumLigne.text.length == maxcharsNumLigne)
				tiNumLigne.addEventListener(TextEvent.TEXT_INPUT,maxCharsHandler);
		}
		
		/* affiche le tooltip maxChars dès la saisie du maxChar + 1 */
		private function maxCharsHandler(te:TextEvent):void
		{
			// stopper le timer pour tooltip maxChars si il est lancé
			maxCharTimer.stop();
			
			msgTooltipMaxChar = strMaxChar + maxcharsNumLigne + strMaxCharEnd;
			afficheTooltipMaxChar();
			
			// débuter le timer pour tooltip maxChars
			maxCharTimer.start();
		}
		
		/* à la fin du temps imparti par le Timer - Handler */
		private function maxCharTimerHandler(te:TimerEvent):void
		{
			maxCharTimer.stop();
			
			if(tooltipMaxChar!=null) 
				supTooltipMaxChar();
		}
		
		/* verif input empty */
		private function colorEmptyInput():void
		{
//			if(data.ISVERIF_AVAILABLE)
//			{
				if(data.NUMLIGNE=="" || (data.NUMLIGNE as String).length>maxcharsNumLigne)
					tiNumLigne.setStyle("borderColor","0xC00000");
				else
					tiNumLigne.setStyle("borderColor","0xD3D5D6");
//			}
//			else
//				tiNumLigne.setStyle("borderColor","0xD3D5D6");
		}
		
		/* creation tooltip - Handler */
		protected function afficheTooltip(evt:MouseEvent):void
		{
			var img:Image = evt.currentTarget as Image;
			var p:Point = new Point(img.x,img.y);
			p = img.contentToGlobal(p);
			
			// créer un tooltip
			supTooltip(null);
			tooltip = ToolTipManager.createToolTip(msgTooltip,(p.x-img.x+15),p.y) as ToolTip;
			tooltip.setStyle("backgroundColor","0xC00000");
			tooltip.setStyle("cornerRadius",5);
			tooltip.setStyle("fontWeight","bold");
			tooltip.setStyle("color","0xFFFFFF");
			tooltip.setStyle("paddingTop",5);
			tooltip.setStyle("paddingLeft",5);
			tooltip.setStyle("paddingRight",5);
			tooltip.setStyle("paddingBottom",5);			
		}
		
		/* */
		protected function supTooltip(event:MouseEvent):void
		{
			if(tooltip!=null)
			{
				ToolTipManager.destroyToolTip(tooltip);
				tooltip = null;
			}
		}
		
		/* creation tooltip - Handler */
		protected function afficheTooltipMaxChar():void
		{
			var p:Point = new Point(img.x,img.y);
			p = img.contentToGlobal(p);
			
			// créer un tooltip
			supTooltipMaxChar();
			tooltipMaxChar = ToolTipManager.createToolTip(msgTooltipMaxChar,(p.x-img.x+15),p.y) as ToolTip;
			tooltipMaxChar.setStyle("backgroundColor","0xC00000");
			tooltipMaxChar.setStyle("cornerRadius",5);
			tooltipMaxChar.setStyle("fontWeight","bold");
			tooltipMaxChar.setStyle("color","0xFFFFFF");
			tooltipMaxChar.setStyle("paddingTop",5);
			tooltipMaxChar.setStyle("paddingLeft",5);
			tooltipMaxChar.setStyle("paddingRight",5);
			tooltipMaxChar.setStyle("paddingBottom",5);			
		}
		
		/* */
		protected function supTooltipMaxChar():void
		{
			if(tooltipMaxChar!=null)
			{
				ToolTipManager.destroyToolTip(tooltipMaxChar);
				tooltipMaxChar = null;
			}
		}
		
		/* */
		private function stopTimer():void
		{
			maxCharTimer.stop();
//			afterDoublonTimer.stop();
		}
		
		/* */
		private function undisplayIMG():void
		{
			img.includeInLayout = false;
			img.visible = false;
			spin.includeInLayout = false;
			spin.visible = false;
		}
		
		/* */
		private function displayIMG():void
		{
			img.includeInLayout = true;
			img.visible = true;
			spin.includeInLayout = true;
			spin.visible = true;
		}
		
		/* */
		private function undisplayTooltip():void
		{
			if(tooltip!=null){
				ToolTipManager.destroyToolTip(tooltip); 
				tooltip=null;
			}
			if(tooltipMaxChar!=null){
				ToolTipManager.destroyToolTip(tooltipMaxChar); 
				tooltipMaxChar=null;
			}
		}
		
		/* */
		private function removeListeners():void
		{
			img.removeEventListener(MouseEvent.ROLL_OVER,afficheTooltip);
			img.removeEventListener(MouseEvent.ROLL_OUT,supTooltip);
//			_objIMP.myDatas.removeEventListener(ImportDeMasseEvent.CHECKED_NUMLIGNE,checkNLIGNEHandler);
//			itemCurrent.objIMP.myDatas.removeEventListener(ImportDeMasseEvent.CHECKED_NUMLIGNE,checkNLIGNEHandler);
		}
		
		
		
		//------------------ GETETRS - SETTERS ------------------//
		
		public function get listData():BaseListData
		{
			return _listData;
		}

		public function set listData(value:BaseListData):void
		{
			_listData = value;
		}

		public function get arrayData():ArrayCollection
		{
			return _arrayData;
		}

		public function set arrayData(value:ArrayCollection):void
		{
			_arrayData = value;
		}

	}
}