package gestionparc.ihm.fiches.importmasse.renderer
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import gestionparc.utils.CustomDatagrid;
	
	import mx.containers.HBox;
	import mx.controls.listClasses.BaseListData;

	public class CellRendererHelper extends EventDispatcher
	{
		public static var rowtest:int=-1;
		
		/* */
		public function CellRendererHelper()
		{
		}
		
		/* */
		public static function gotToPreviousEditableCell(listData:BaseListData, minRow:Number=-1):void
		{
			var disObj	:DisplayObject;
			var dtg		:CustomDatagrid = (listData.owner as CustomDatagrid);
			if (dtg == null) return;
			
			var previous_columnIndex:int	= listData.columnIndex;
			var previous_rowIndex	:int	= listData.rowIndex;
			var listItemArr			:Array	= dtg.listItemsArray;
			
			var arrColumnToImport:Array = new Array("COLLAB_TO_IMPORT","IMEI_TO_IMPORT","IMEI_TO_IMPORT","NUMSIM_TO_IMPORT","NUMEXT_TO_IMPORT","NUMLIGNE_TO_IMPORT");
			
			while (-1 < previous_columnIndex)
			{
				previous_columnIndex--;
				
				if (arrColumnToImport[previous_columnIndex]!=null 
					&& dtg.dataProvider[previous_rowIndex][arrColumnToImport[previous_columnIndex]]==true 
					&& previous_columnIndex>-1 && previous_columnIndex<6)
				{
					if(previous_columnIndex==0)
						disObj = (listItemArr[previous_rowIndex][previous_columnIndex] as HBox).getChildAt(2);
					else
						disObj = (listItemArr[previous_rowIndex][previous_columnIndex] as HBox).getChildAt(0);
//					dtg.columns[previous_columnIndex].editable = true;
					
//					dtg.selectedIndex = previous_columnIndex;
//					ind = next_columnIndex;
//					dtg.dispatchEvent(new CustomListEvent(CustomListEvent.CUSTOM_ITEM_CLICK,true,previous_columnIndex));
//					dtg.dispatchEvent(new ListEvent(ListEvent.CHANGE, false, false,previous_columnIndex, previous_rowIndex));
					disObj.dispatchEvent(new Event("GOTO_PREVIOUS_ITEMRENDERER_AVAILABLE",true));
					break;
				}
				// si on arrive à la premiere colonne du row
				if (previous_columnIndex==-1 && minRow<previous_rowIndex)
				{
					previous_rowIndex--;
					previous_columnIndex=dtg.columnCount-1;
					
					// si on arrive à la premiere colonne du premier row
					if(previous_rowIndex==-1) break;
				}
			}
		}
		
		/* detecter les colonnes editables/non editables sur le row cliqué */
		public static function setEditableColumn(listData:BaseListData):void
		{
			var arrColumnToImport:Array = new Array("COLLAB_TO_IMPORT","IMEI_TO_IMPORT","IMEI_TO_IMPORT","NUMSIM_TO_IMPORT","NUMEXT_TO_IMPORT","NUMLIGNE_TO_IMPORT");
			
			var dtg:CustomDatagrid = listData.owner as CustomDatagrid;
			if (dtg == null) return; //nb maxi colonnes du DG=7, indice [6]
			
			var columnIndex:int=listData.columnIndex;
			var rowIndex:int=listData.rowIndex;
			var ind:int=0;
			
//			trace("**DEBUT**");
//			trace("**ROW= "+rowIndex+" / COLONNE= "+columnIndex+" **");
			for(var i:int; i<arrColumnToImport.length; i++)
			{
				dtg.columns[i].rendererIsEditor = false;
				dtg.columns[i].editable = false;
				if(dtg.dataProvider[rowIndex][arrColumnToImport[i]]==true)
				{
					dtg.columns[i].rendererIsEditor = true;
					dtg.columns[i].editable = true;
//					ind = i;
//					trace("**col[" + i + "].edit= true**");
				}
				else{
//					trace("**col[" + i + "].edit= false**");
				}
			}
//			dtg.dispatchEvent(new ListEvent(ListEvent.ITEM_CLICK,true,false,i,rowIndex));
//			trace("**FIN**");
		}
		
		/* */
		public static function gotToNextEditableCell(listData:BaseListData, maxRow:Number=100):void
		{
			var dtg:CustomDatagrid = (listData.owner as CustomDatagrid);
			if (dtg == null) return; //nb maxi colonnes du DG=7, indice [6]

			var disObj:DisplayObject;
			var listItemArr:Array = dtg.listItemsArray;
			var next_columnIndex:int=listData.columnIndex;
			var next_rowIndex:int=listData.rowIndex;
			
			var arrColumnToImport:Array = new Array("COLLAB_TO_IMPORT","IMEI_TO_IMPORT","IMEI_TO_IMPORT","NUMSIM_TO_IMPORT","NUMEXT_TO_IMPORT","NUMLIGNE_TO_IMPORT");
			
			while (next_columnIndex < dtg.columnCount-1) //dtg.columnCount-1 = 6
			{
				next_columnIndex++;
				
				// setter les colonnes des itemrenderers qui ne seront pas importer en editable=false; pour gestion du focus
//				dtg.columns[next_columnIndex].editable = false;
//				dtg.columns[next_columnIndex].rendererIsEditor = false
				
				// parcourir les colonnes qui sont "importables"
				if (arrColumnToImport[next_columnIndex]!=null 
					&& dtg.dataProvider[next_rowIndex][arrColumnToImport[next_columnIndex]]==true 
					&& next_columnIndex>-1 && next_columnIndex<6)
				{
					// setter editable=false les colonnes des itemrenderers qui ne seront pas importer; pour gestion du focus
//					dtg.columns[next_columnIndex].editable = true;
//					dtg.columns[next_columnIndex].rendererIsEditor = true
					
					if(next_columnIndex==0)
						disObj = (listItemArr[next_rowIndex][next_columnIndex] as HBox).getChildAt(2);
					else
						disObj = (listItemArr[next_rowIndex][next_columnIndex] as HBox).getChildAt(0);
					
					disObj.dispatchEvent(new Event("GOTO_NEXT_ITEMRENDERER_AVAILABLE",true));
					break;
				}
				// si on arrive à la dernière colonne du row
				if (next_columnIndex==(dtg.columnCount-1) && next_rowIndex<maxRow)
				{
					next_rowIndex++;
					next_columnIndex=-1;
					
					CellRendererHelper.setEditableColumn(listData);
//					CellRendererHelper.setEditableColumnForRow(listData,next_rowIndex);
					
//					dtg.dispatchEvent(new ListEvent(ListEvent.CHANGE, true, false,next_columnIndex, next_rowIndex));
					
					// on arrive à la derniere colonne du dernier row
					if(!(listItemArr[next_rowIndex] as Array) || (listItemArr[next_rowIndex] as Array).length==0) 
						return;
				}
			}
		}
		
		/* detecter les colonnes editables/non editables sur le row cliqué */
		public static function setEditableColumnForRow(listData:BaseListData,rowIndex:int):void
		{
			var arrColumnToImport:Array = new Array("COLLAB_TO_IMPORT","IMEI_TO_IMPORT","IMEI_TO_IMPORT","NUMSIM_TO_IMPORT","NUMEXT_TO_IMPORT","NUMLIGNE_TO_IMPORT");
			
			var dtg:CustomDatagrid = (listData.owner as CustomDatagrid);
			if (dtg == null) return; //nb maxi colonnes du DG=7, indice [6]
						
//			trace("*********** ROW= "+rowIndex+" / COLONNE= "+columnIndex+" *************");
			trace("**DEBUT*A**");
			for(var i:int; i<arrColumnToImport.length; i++)
			{
				dtg.columns[i].editable = false;
				dtg.columns[i].rendererIsEditor = false;
				if(dtg.dataProvider[rowIndex][arrColumnToImport[i]]==true)
				{
					dtg.columns[i].editable = true;
					dtg.columns[i].rendererIsEditor = true;
					trace("**col[" + i + "].edit= true**");
				}
				else
					trace("**col[" + i + "].edit= false**");
			}
			trace("**FIN*A**");
		}
		
		/* */
//		public static function gotToNextEditableCellNonUse(listData:BaseListData, maxRow:Number=100):void
//		{
//			var dtg:DataGrid=(listData.owner as DataGrid);
//			if (dtg == null)
//				return;
//
//			var next_columnIndex:int=listData.columnIndex;
//			var next_rowIndex:int=listData.rowIndex;
//
//
//			while (next_columnIndex < dtg.columnCount - 1)
//			{
//				next_columnIndex++;
//				if ((dtg.columns[next_columnIndex] as DataGridColumn).editable == true)
//				{
//					dtg.editedItemPosition={rowIndex: next_rowIndex, columnIndex: next_columnIndex};
//					break;
//				}
//				if (next_columnIndex == (dtg.columnCount - 1) && next_rowIndex < maxRow)
//				{
//					next_rowIndex++;
//					next_columnIndex=-1;
//				}
//			}
//		}
	}
}