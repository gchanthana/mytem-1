package gestionparc.ihm.fiches.importmasse.checkingEtatDataClass
{
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import gestionparc.entity.ImportDeMasseVO;
	import gestionparc.event.ImportDeMasseEvent;
	import gestionparc.ihm.fiches.importmasse.itemRenderer.ItemRendererImeiImpl;
	import gestionparc.services.importmasse.ImportMasseServices;
	
	
	public class CheckingEtatDataIMEI
	{
		
		//--------------- VARIABLES ----------------//
		
		private var _afterDoublonTimer		:Timer 	= new Timer(_afterDoublonDelayTimer,0);
		private var _afterDoublonDelayTimer	:Number = 500;
		private var _maxcharsIMEI			:int 	= 20;
		
		private var _itemData1				:ImportDeMasseVO;
		private var _itemData2				:ImportDeMasseVO;
		private var _itemImp				:ImportDeMasseVO;
		private var _objImp					:ImportMasseServices;
		
		private var _itemRend				:ItemRendererImeiImpl;
		
		
		//--------------- METHODES ----------------//
		
		/* */
		public function CheckingEtatDataIMEI(itemRend:ItemRendererImeiImpl)
		{
			_objImp = new ImportMasseServices();
			this._itemRend = itemRend;
		}
		
		/* */
		public function checkEtatData(obj:ImportDeMasseVO):void
		{
			if(obj.IMEI.length > 20)
			{
				obj.ETAT_IMEI = 11; //etat trop long (à l'import)
			}
			else if(obj.IMEI.length == 0)
			{
				obj.ETAT_IMEI = 9; //etat champs vide (à l'import)
				
				if(obj.NUMSERIE.length > 0)
				{
					obj.ETAT_IMEI = 1; //etat champs non vide (à l'import)
					
				}
			}
			else
			{
				obj.ETAT_IMEI = -10 //etat spinner
				callServiceCheckEtat(obj);
			}
			_itemImp = obj;
		}
		
		/* */
		public function callServiceCheckEtat(obj:ImportDeMasseVO):void
		{
//			removeListener();
			
			_objImp.checkIMEI(obj.IMEI);
			_objImp.myDatas.addEventListener(ImportDeMasseEvent.CHECKED_IMEI,checkIMEIHandler);
		}
		
		/* */
		public function processCheckingEtatData():void
		{
			stopTimer();
			removeListener();
			
			//si maxChar de l'input est dépassé à l'import
			if ((_itemRend.data.IMEI as String).length > _maxcharsIMEI)
			{
				_itemRend.data.ETAT_IMEI = 11;
			}
			else
			{
				if (_itemRend.data.IMEI=="")
				{
					_itemRend.data.ETAT_IMEI = 9;
					
					if (_itemRend.data.NUMSERIE !="")
					{
						_itemRend.data.ETAT_IMEI = -100;
					}
					else
					{
						_itemRend.data.ETAT_NUMSERIE = 9;
					}
				}
				//si input contient au moins un caractere
				if (_itemRend.data.IMEI!="")
				{
					findDoublonInListe();
					
					_itemRend.data.ETAT_NUMSERIE = -100;
					if(_itemRend.data.NUMSERIE =="")
					{
						_itemRend.data.NUMSERIE_OK = true;
					}
				}
			}
		}
		
		/* */
		private function findDoublonInListe():void
		{			
			var hasDoublon:Boolean = false;
			
			// reperer tous les doublons sur la saisie courante
			for each(var item:ImportDeMasseVO in _itemRend.arrayData)
			{
				if(_itemRend.data!=item && _itemRend.data.IMEI!="" && _itemRend.data.IMEI==item.IMEI)
				{
					_itemRend.data.ETAT_IMEI = 4; //etat doublon
					item.ETAT_IMEI = 4;
					_itemRend.data.DOUBLON_IMEI = true;
					item.DOUBLON_IMEI = true;
					hasDoublon = true;
				}
			}
			// si le input current n'a pas de doublon
			if(!hasDoublon)
				_itemRend.data.DOUBLON_IMEI = false;
			
			//on verifie si l'ancienne valeur du TI courant a encore des doublons
			findDoublonOnPreviousValue();
			
			//si aucun doublon sur la saisie -> appel au service de verif
			if(!_itemRend.data.DOUBLON_IMEI)
			{
				_afterDoublonTimer.addEventListener(TimerEvent.TIMER, checkEtatHandler);
				_afterDoublonTimer.start();
			}
		}
		
		/* */
		private function findDoublonOnPreviousValue():void
		{
			var cpt:int = 0;
			_itemData1 = new ImportDeMasseVO();
			
			// reperer tous les doublons sur l'ancien dernier doublon de la saisie courante
			for each(var item:ImportDeMasseVO in _itemRend.arrayData)
			{
				if((_itemRend.data as ImportDeMasseVO).OLD_IMEI!="" && (_itemRend.data as ImportDeMasseVO).OLD_IMEI == item.IMEI)
				{
					item.ETAT_IMEI=4;
					_itemData1 = item;
					cpt++;
				}
			}
			// si il n'y a pas de doublon sur OLD value du TI
			//on appelle le service pour verifier son etat
			if(cpt==1)
			{
				_itemData1.DOUBLON_IMEI = false;
//				_itemData1.checkCurrentData(this);
				checkEtatData(_itemData1);
			}
		}
		
		/* appel du service pour verif etat */
		protected function processVerifEtat():void
		{
			_itemData2 = new ImportDeMasseVO();
			_itemData2 = _itemRend.data as ImportDeMasseVO;
//			_itemData2.checkCurrentData(this);
			checkEtatData(_itemData2);
		}
		
		/* */
		private function checkEtatHandler(evt:Event):void
		{
			_afterDoublonTimer.removeEventListener(TimerEvent.TIMER, checkEtatHandler);
			processVerifEtat();
		}
		
		/* */
		private function checkIMEIHandler(impe:ImportDeMasseEvent):void
		{
			if(!_itemImp.DOUBLON_IMEI)
			{
				_itemImp.ETAT_IMEI = int(impe.objRet);
				
				if(int(impe.objRet)==1 || int(impe.objRet)==3)
					_itemImp.IMEI_OK=true;
				else
					_itemImp.IMEI_OK=false;
			}
		}
		
		/* */
		private function removeListener():void
		{
			_objImp.myDatas.removeEventListener(ImportDeMasseEvent.CHECKED_IMEI,checkIMEIHandler);
		}
		
		/* */
		private function stopTimer():void
		{
			_afterDoublonTimer.stop();
		}
	}
}