package gestionparc.ihm.fiches.importmasse.alimentaionpool
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.services.PoolService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class AlimentationPoolsImpl extends TitleWindow
	{

		public static var POOL_NUMBER_BY_BOX:int=8;

		public var boxElements:HBox;

		private var _cstmBox:CustomHboxCheckbox;

		private var _poolSrv:PoolService=new PoolService();


		public var pools:ArrayCollection=new ArrayCollection();

		public var objPoolRev:Object; //OBJECT POOL REVENDEUR DE L'IHM 'Gestion de parc'

		public var isDefault:Boolean=false;
		public var isPoolRev:Boolean=true;

		private var _poolsValue:ArrayCollection=new ArrayCollection();

		private var _objAllPool:Object;
		private var _objPoolRev:Object; //OBJECT POOL REVENDEUR FORMATÉ

		private var _idPoolRev:int=-1;
		private var _poolsNbr:int=-1;

		public function AlimentationPoolsImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}

		protected function closeThisHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			validation();
		}

		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}


		private function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener('CHECKBOX_CHANGE', checkboxChangeHandler);

			initialization();
		}

		private function checkboxChangeHandler(e:Event):void
		{
			if (e.target as CustomHboxCheckbox)
			{
				var cstmBx:CustomHboxCheckbox=e.target as CustomHboxCheckbox;
				var idCstmBx:int=cstmBx.ID;
				var selected:Boolean=cstmBx.SELECTED;

				if (idCstmBx == -1)
					enabledDisabledCustomHboxCheckbox(selected);
				else
				{
					var children:Array=boxElements.getChildren();
					var lenChild:int=children.length;
					var idx:int=-1;
					var cptr:int=0;
					var bool:Boolean=false;

					for (var i:int=0; i < lenChild; i++)
					{
						if (children[i] as VBox)
						{
							var vbxChild:Array=(children[i] as VBox).getChildren();
							var lenvbx:int=vbxChild.length;

							for (var j:int=0; j < lenvbx; j++)
							{
								if (vbxChild[j] as CustomHboxCheckbox)
								{
									if (vbxChild[j].ID == -1 || vbxChild[j].ID == _idPoolRev)
									{
										if (vbxChild[j].ID == -1)
											idx=j;
									}
									else
									{
										if (vbxChild[j].SELECTED)
											cptr++;
									}
								}
							}
						}
					}

					if (cptr == _poolsNbr)
						((children[0] as VBox).getChildAt(idx) as CustomHboxCheckbox).SELECTED=true;
					else
						((children[0] as VBox).getChildAt(idx) as CustomHboxCheckbox).SELECTED=false;
				}
			}
		}

		private function initialization():void
		{
			createPoolRevendeur();
			createAllPoolObject();
			getPools();
		}

		private function createPoolRevendeur():void
		{
			_objPoolRev=new Object();
			_objPoolRev.LIBELLE=objPoolRev.LIBELLE_POOL + ' - ' + objPoolRev.LIBELLE_PROFIL;
			_objPoolRev.SELECTED=true;
			_objPoolRev.ENABLED=false;
			_objPoolRev.ID=objPoolRev.IDPOOL;

			_poolsValue.addItem(_objPoolRev);

			_idPoolRev=_objPoolRev.ID;
		}

		private function createAllPoolObject():void
		{
			_objAllPool=new Object();
			_objAllPool.LIBELLE='Tous les pools client';
			_objAllPool.SELECTED=false;
			_objAllPool.ENABLED=true;
			_objAllPool.ID=-1;
			_poolsValue.addItem(_objAllPool);
		}

		private function buildCustomBox():void
		{
			var cptr:int=0;
			var len:int=_poolsValue.length;

			for (var i:int=0; i < len; i++)
			{
				var cstmbx:CustomHboxCheckbox=new CustomHboxCheckbox();
				var obj:Object=new Object();
				var myvbx:VBox;

				if (i == (cptr * POOL_NUMBER_BY_BOX))
				{
					myvbx=new VBox();
					boxElements.addChild(myvbx);
					cptr++;
				}

				if (!_poolsValue[i].hasOwnProperty('IDPOOL'))
				{
					obj.LIBELLE=_poolsValue[i].LIBELLE;
					obj.SELECTED=_poolsValue[i].SELECTED;
					obj.ENABLED=_poolsValue[i].ENABLED;
					obj.ID=_poolsValue[i].ID;
				}
				else
				{
					obj.LIBELLE=_poolsValue[i].LIBELLE;
					obj.SELECTED=false;
					obj.ENABLED=true;
					obj.ID=_poolsValue[i].IDPOOL;
				}

				cstmbx.setDataToObject(obj.LIBELLE, obj.ID, obj.SELECTED, obj.ENABLED);

				myvbx.addChild(cstmbx);
			}

			_poolsNbr=_poolSrv.listePoolsValues.length;
		}

		private function enabledDisabledCustomHboxCheckbox(bool:Boolean):void
		{
			var children:Array=boxElements.getChildren();
			var lenChild:int=children.length;

			for (var i:int=0; i < lenChild; i++)
			{
				if (children[i] as VBox)
				{
					var vbxChild:Array=(children[i] as VBox).getChildren();
					var lenvbx:int=vbxChild.length;

					for (var j:int=0; j < lenvbx; j++)
					{
						if (vbxChild[j] as CustomHboxCheckbox)
						{
							if (vbxChild[j].ID != -1 && vbxChild[j].ID != _idPoolRev)
								vbxChild[j].SELECTED=bool;

							(vbxChild[j] as CustomHboxCheckbox).refreshData();
						}
					}
				}
			}
		}

		private function validation():void
		{
			if (checkdata())
				sendData();
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M111', 'Erreur___d_finir'), "Consoview", null);
		}

		private function checkdata():Boolean
		{
			var children:Array=boxElements.getChildren();
			var lenChild:int=children.length;
			var bool:Boolean=true;

			for (var i:int=0; i < lenChild; i++)
			{
				if (children[i] as VBox)
				{
					var vbxChild:Array=(children[i] as VBox).getChildren();
					var lenvbx:int=vbxChild.length;

					for (var j:int=0; j < lenvbx; j++)
					{
						if (vbxChild[j] as CustomHboxCheckbox)
						{
							if (vbxChild[j].ID != -1 && vbxChild[j].ID != _idPoolRev && vbxChild[j].SELECTED == true)

								pools.addItem(vbxChild[j].ID);
						}
					}
				}
			}

			if (pools.length == 0)
				isDefault=true;

			return bool;
		}

		// 0-> POOLS CLIENT, 1-> POOLS DISTRIBUTEUR
		private function getPools():void
		{
			var isRevValue:int=1;

			if (isPoolRev)
				isRevValue=0;

			_poolSrv.fournirPools(isRevValue);
			_poolSrv.addEventListener("POOLS_LISTED", getPoolsHandler);
		}

		private function sendData():void
		{
			dispatchEvent(new Event('POOL_CHANGED'));
		}

		private function getPoolsHandler(e:Event):void
		{
			var lenPools:int=_poolSrv.listePoolsValues.length;
			var cptr:int=0;

			for (var i:int=0; i < lenPools; i++)
			{
				_poolsValue.addItem(_poolSrv.listePoolsValues[i]);
			}

			buildCustomBox();
		}

		private function sendDataHandler(e:Event):void
		{
			dispatchEvent(new Event('POOL_CHANGED'));
		}
	}
}