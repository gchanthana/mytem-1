package gestionparc.entity
{
	import flash.events.EventDispatcher;

	public class ModeleVO extends EventDispatcher
	{
		//--------------- VARIABLES ----------------//
		
		private var _ISFIXE		:Boolean;
		private var _ISMOBILE	:Boolean;
		
		private var _IDTYPE_EQUIPEMENT			:int;
		private var _IDEQUIPEMENT_FOURNISSEUR	:int;
		private var _IDEQUIPEMENT_FOURNIS		:int;
		private var _LINE_ID					:int;
		
		private var _LIBELLE_EQ				:String = "";
		private var _CATEGORIE_EQUIPEMENT	:String = "";
		private var _NOM_FABRIQUANT			:String = "";
		private var _TYPE_EQUIPEMENT		:String = "";
		private var _NOM_FOURNISSEUR		:String = "";
		
		private var _SELECTED				:Boolean;
		
		
		//--------------- METHODES ----------------//
		
		/* */
		public function ModeleVO()
		{
		}
		
		/* */
		public function fillVO(obj:Object):void
		{
			if(obj.hasOwnProperty("LIBELLE_EQ"))
				this._LIBELLE_EQ = obj.LIBELLE_EQ;
				
			if(obj.hasOwnProperty("CATEGORIE_EQUIPEMENT"))
				this._CATEGORIE_EQUIPEMENT = obj.CATEGORIE_EQUIPEMENT;
				
			if(obj.hasOwnProperty("TYPE_EQUIPEMENT"))
				this._TYPE_EQUIPEMENT = obj.TYPE_EQUIPEMENT;
				
			if(obj.hasOwnProperty("NOM_FOURNISSEUR"))
				this._NOM_FOURNISSEUR = obj.NOM_FOURNISSEUR;
				
			if(obj.hasOwnProperty("NOM_FABRIQUANT"))
				this._NOM_FABRIQUANT = obj.NOM_FABRIQUANT;
			
			if(obj.hasOwnProperty("ISFIXE"))
				this._ISFIXE = obj.ISFIXE;
			
			if(obj.hasOwnProperty("ISMOBILE"))
				this._ISMOBILE = obj.ISMOBILE;
			
			if(obj.hasOwnProperty("IDTYPE_EQUIPEMENT"))
				this._IDTYPE_EQUIPEMENT = obj.IDTYPE_EQUIPEMENT;
			
			if(obj.hasOwnProperty("IDEQUIPEMENT_FOURNISSEUR"))
				this._IDEQUIPEMENT_FOURNISSEUR = obj.IDEQUIPEMENT_FOURNISSEUR;
			
			if(obj.hasOwnProperty("IDEQUIPEMENT_FOURNIS"))
				this._IDEQUIPEMENT_FOURNIS = obj.IDEQUIPEMENT_FOURNIS;
			
			if(obj.hasOwnProperty("LINE_ID"))
				this._LINE_ID = obj.LINE_ID;
		}
		
		
		//--------------- GETTERS - SETTERS ----------------//
		
		public function get LIBELLE_EQ():String
		{
			return _LIBELLE_EQ;
		}
		public function set LIBELLE_EQ(value:String):void
		{
			_LIBELLE_EQ = value;
		}
		
		public function get CATEGORIE_EQUIPEMENT():String
		{
			return _CATEGORIE_EQUIPEMENT;
		}
		public function set CATEGORIE_EQUIPEMENT(value:String):void
		{
			_CATEGORIE_EQUIPEMENT = value;
		}
		
		public function get NOM_FABRIQUANT():String
		{
			return _NOM_FABRIQUANT;
		}
		public function set NOM_FABRIQUANT(value:String):void
		{
			_NOM_FABRIQUANT = value;
		}
		
		public function get TYPE_EQUIPEMENT():String
		{
			return _TYPE_EQUIPEMENT;
		}
		public function set TYPE_EQUIPEMENT(value:String):void
		{
			_TYPE_EQUIPEMENT = value;
		}
		
		public function get NOM_FOURNISSEUR():String
		{
			return _NOM_FOURNISSEUR;
		}
		public function set NOM_FOURNISSEUR(value:String):void
		{
			_NOM_FOURNISSEUR = value;
		}
		
		[Bindable]
		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}
		public function set SELECTED(value:Boolean):void
		{
			_SELECTED = value;
		}

		public function get ISFIXE():Boolean
		{
			return _ISFIXE;
		}
		public function set ISFIXE(value:Boolean):void
		{
			_ISFIXE = value;
		}
		
		public function get ISMOBILE():Boolean
		{
			return _ISMOBILE;
		}
		public function set ISMOBILE(value:Boolean):void
		{
			_ISMOBILE = value;
		}
		
		public function get IDTYPE_EQUIPEMENT():int
		{
			return _IDTYPE_EQUIPEMENT;
		}
		public function set IDTYPE_EQUIPEMENT(value:int):void
		{
			_IDTYPE_EQUIPEMENT = value;
		}
		
		public function get IDEQUIPEMENT_FOURNISSEUR():int
		{
			return _IDEQUIPEMENT_FOURNISSEUR;
		}
		public function set IDEQUIPEMENT_FOURNISSEUR(value:int):void
		{
			_IDEQUIPEMENT_FOURNISSEUR = value;
		}
		
		public function get IDEQUIPEMENT_FOURNIS():int
		{
			return _IDEQUIPEMENT_FOURNIS;
		}
		public function set IDEQUIPEMENT_FOURNIS(value:int):void
		{
			_IDEQUIPEMENT_FOURNIS = value;
		}
		
		public function get LINE_ID():int
		{
			return _LINE_ID;
		}
		public function set LINE_ID(value:int):void
		{
			_LINE_ID = value;
		}
	}
}