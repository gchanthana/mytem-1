package gestionparc.entity
{
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataCollab;
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataIMEI;
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataNumExtension;
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataNumLigne;
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataNumSIM;
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataNumSerie;
	
	public class ImportDeMasseVO
	{
		
		//---------------- VARIABLES ----------------//
		public var LIGNE_IS_MOBILE		:Boolean;
		private var _MATRICULE			:String = "";
		private var _PRENOM				:String = "";
		private var _NOM				:String = "";
		private var _IMEI				:String = "";
		private var _NUMEXT				:String = "";
		private var _NUMSERIE			:String = "";
		private var _NUMSIM				:String = "";
		private var _NUMLIGNE			:String = "";
		
		private var _ISFIXE				:int	= 0;
		private var _ISMOBILE			:int	= 0;
		
		private var _OLD_IMEI			:String = "";
		private var _OLD_NUMSERIE		:String = "";
		private var _OLD_NUMEXT			:String = "";
		private var _OLD_NUMSIM			:String = "";
		private var _OLD_NUMLIGNE		:String = "";
		
		private var _DOUBLON_IMEI		:Boolean = false;
		private var _DOUBLON_NUMSERIE	:Boolean = false;
		private var _DOUBLON_NUMEXT		:Boolean = false;
		private var _DOUBLON_NUMSIM		:Boolean = false;
		private var _DOUBLON_NUMLIGNE	:Boolean = false;
		
		private var _ISVERIF_AVAILABLE	:Boolean = false;
		
		// statut de la colonne du row pour l'import
		[Bindable]
		private var _COLLAB_TO_IMPORT	:Boolean;
		[Bindable]
		private var _IMEI_TO_IMPORT		:Boolean;
		[Bindable]
		private var _NUMEXT_TO_IMPORT	:Boolean;
		[Bindable]
		private var _NUMSERIE_TO_IMPORT	:Boolean;
		[Bindable]
		private var _NUMSIM_TO_IMPORT	:Boolean;
		[Bindable]
		private var _NUMLIGNE_TO_IMPORT	:Boolean;
		[Bindable]
		private var _GARANTIE_AVAILABLE	:Boolean;
		
		// statut des colonnes (à importer) du row pour la verif
		[Bindable]
		private var _COLLAB_OK			:Boolean;
		[Bindable]
		private var _IMEI_OK			:Boolean;
		[Bindable]
		private var _NUMEXT_OK			:Boolean;
		[Bindable]
		private var _NUMSERIE_OK		:Boolean;
		[Bindable]
		private var _NUMSIM_OK			:Boolean;
		[Bindable]
		private var _NUMLIGNE_OK		:Boolean;
		
		// resultat de la verif pour l'image à afficher
		[Bindable]
		private var _ETAT_COLLAB	:int 	= -100;
		[Bindable]
		private var _ETAT_IMEI		:int 	= -100;
		[Bindable]
		private var _ETAT_NUMSERIE	:int 	= -100;
		[Bindable]
		private var _ETAT_NUMEXT	:int 	= -100;
		[Bindable]
		private var _ETAT_NUMSIM	:int 	= -100;
		[Bindable]
		private var _ETAT_NUMLIGNE	:int 	= -100;
		
		// pour le cas ou le IMEI est mettable mais facultatif, ou l'inverse, idem avec NUMSERIE
		
		[Bindable] 
		private var _IMEI_IS_REQUIRED	:Boolean;
		
		// reperer lorsque l'input est saisi manuellement
		private var _COLLAB_EN_SAISIE	:Boolean;
		private var _IMEI_EN_SAISIE		:Boolean;
		private var _NUMSERIE_EN_SAISIE	:Boolean;
		private var _NUMEXT_EN_SAISIE	:Boolean;
		private var _NUMSIM_EN_SAISIE	:Boolean;
		private var _NUMLIGNE_EN_SAISIE	:Boolean;
		
		private var boolCollab:Boolean = false;
		
		
		//---------------- METHODES ----------------//
		
		/* */
		public function ImportDeMasseVO()
		{
		}
		
		/* */
		public function fillVO(obj:Object):void
		{
			if(obj.hasOwnProperty("PRENOM"))
			{
				this._PRENOM = obj.PRENOM;
				boolCollab = true;
				this._COLLAB_TO_IMPORT = true;
			}
			
			if(obj.hasOwnProperty("NOM"))
			{
				this._NOM = obj.NOM;
				boolCollab = true;
				this._COLLAB_TO_IMPORT = true;
			}
			
			if(obj.hasOwnProperty("MATRICULE"))
			{
				this._MATRICULE = obj.MATRICULE;
				boolCollab = true;
				this._COLLAB_TO_IMPORT = true;
			}
			
			if(boolCollab)
			{
				var _objCheckCollab:CheckingEtatDataCollab = new CheckingEtatDataCollab(null);
				_objCheckCollab.checkEtatData(this);
			}
			
			if(obj.hasOwnProperty("IMEI"))
			{
				this._IMEI = obj.IMEI;
				this._IMEI_TO_IMPORT = true;
			}
			
			if(obj.hasOwnProperty("NUMEXT"))
			{
				this._NUMEXT = obj.NUMEXT;
				this._NUMEXT_TO_IMPORT=true;
			}
			
			if(obj.hasOwnProperty("NUMSERIE"))
			{
				this._NUMSERIE = obj.NUMSERIE;
				this._NUMSERIE_TO_IMPORT=true;
			}
			
			if(obj.hasOwnProperty("NUMSIM"))
			{
				this._NUMSIM = obj.NUMSIM;
				var _objCheckSIM:CheckingEtatDataNumSIM = new CheckingEtatDataNumSIM(null);
				_objCheckSIM.checkEtatData(this);
				this._NUMSIM_TO_IMPORT = true;
			}
			
			if(obj.hasOwnProperty("NUMLIGNE"))
			{
				this._NUMLIGNE = obj.NUMLIGNE;
				var _objCheckNumLigne:CheckingEtatDataNumLigne = new CheckingEtatDataNumLigne(null);
				_objCheckNumLigne.checkEtatData(this);
				this._NUMLIGNE_TO_IMPORT = true;
			}
			
			// vérification des ETATs de IMEI et NUMSERIE: si un ou l'autre alors import possible
			if(obj.hasOwnProperty("IMEI"))	{
				var _objCheckIMEI:CheckingEtatDataIMEI = new CheckingEtatDataIMEI(null);
				_objCheckIMEI.checkEtatData(this);
			}
			
			if(obj.hasOwnProperty("NUMSERIE"))	{
				var _objCheckNumSerie:CheckingEtatDataNumSerie = new CheckingEtatDataNumSerie(null);
				_objCheckNumSerie.checkEtatData(this);
			}
			
		}
		
		
		
		//---------------- SETTERS - GETTERS ----------------//
		
		public function get MATRICULE():String
		{
			return _MATRICULE;
		}

		public function set MATRICULE(value:String):void
		{
			_MATRICULE = value;
		}

		public function get PRENOM():String
		{
			return _PRENOM;
		}

		public function set PRENOM(value:String):void
		{
			_PRENOM = value;
		}

		public function get NOM():String
		{
			return _NOM;
		}

		public function set NOM(value:String):void
		{
			_NOM = value;
		}

		public function get IMEI():String
		{
			return _IMEI;
		}

		public function set IMEI(value:String):void
		{
			_IMEI = value;
		}

		public function get NUMEXT():String
		{
			return _NUMEXT;
		}

		public function set NUMEXT(value:String):void
		{
			_NUMEXT = value;
		}

		public function get NUMSIM():String
		{
			return _NUMSIM;
		}

		public function set NUMSIM(value:String):void
		{
			_NUMSIM = value;
		}

		public function get NUMLIGNE():String
		{
			return _NUMLIGNE;
		}

		public function set NUMLIGNE(value:String):void
		{
			_NUMLIGNE = value;
		}

		public function get NUMSERIE():String
		{
			return _NUMSERIE;
		}

		public function set NUMSERIE(value:String):void
		{
			_NUMSERIE = value;
		}

		public function get ETAT_COLLAB():int
		{
			return _ETAT_COLLAB;
		}

		public function set ETAT_COLLAB(value:int):void
		{
			_ETAT_COLLAB = value;
		}

		public function get ETAT_IMEI():int
		{
			return _ETAT_IMEI;
		}

		public function set ETAT_IMEI(value:int):void
		{
			_ETAT_IMEI = value;
		}

		public function get ETAT_NUMSERIE():int
		{
			return _ETAT_NUMSERIE;
		}

		public function set ETAT_NUMSERIE(value:int):void
		{
			_ETAT_NUMSERIE = value;
		}

		public function get ETAT_NUMEXT():int
		{
			return _ETAT_NUMEXT;
		}

		public function set ETAT_NUMEXT(value:int):void
		{
			_ETAT_NUMEXT = value;
		}

		public function get ETAT_NUMSIM():int
		{
			return _ETAT_NUMSIM;
		}

		public function set ETAT_NUMSIM(value:int):void
		{
			_ETAT_NUMSIM = value;
		}

		public function get ETAT_NUMLIGNE():int
		{
			return _ETAT_NUMLIGNE;
		}

		public function set ETAT_NUMLIGNE(value:int):void
		{
			_ETAT_NUMLIGNE = value;
		}

		public function get DOUBLON_NUMEXT():Boolean
		{
			return _DOUBLON_NUMEXT;
		}

		public function set DOUBLON_NUMEXT(value:Boolean):void
		{
			_DOUBLON_NUMEXT = value;
		}

		public function get DOUBLON_IMEI():Boolean
		{
			return _DOUBLON_IMEI;
		}

		public function set DOUBLON_IMEI(value:Boolean):void
		{
			_DOUBLON_IMEI = value;
		}
		
		public function get DOUBLON_NUMSERIE():Boolean
		{
			return _DOUBLON_NUMSERIE;
		}
		
		public function set DOUBLON_NUMSERIE(value:Boolean):void
		{
			_DOUBLON_NUMSERIE = value;
		}
		
		public function get DOUBLON_NUMSIM():Boolean
		{
			return _DOUBLON_NUMSIM;
		}

		public function set DOUBLON_NUMSIM(value:Boolean):void
		{
			_DOUBLON_NUMSIM = value;
		}

		public function get DOUBLON_NUMLIGNE():Boolean
		{
			return _DOUBLON_NUMLIGNE;
		}
		public function set DOUBLON_NUMLIGNE(value:Boolean):void
		{
			_DOUBLON_NUMLIGNE = value;
		}
		
		public function get OLD_NUMEXT():String
		{
			return _OLD_NUMEXT;
		}
		public function set OLD_NUMEXT(value:String):void
		{
			_OLD_NUMEXT = value;
		}
		
		public function get OLD_IMEI():String
		{
			return _OLD_IMEI;
		}
		public function set OLD_IMEI(value:String):void
		{
			_OLD_IMEI = value;
		}
		
		public function get OLD_NUMSERIE():String
		{
			return _OLD_NUMSERIE;
		}
		public function set OLD_NUMSERIE(value:String):void
		{
			_OLD_NUMSERIE = value;
		}
		
		public function get OLD_NUMSIM():String
		{
			return _OLD_NUMSIM;
		}
		public function set OLD_NUMSIM(value:String):void
		{
			_OLD_NUMSIM = value;
		}
		
		public function get OLD_NUMLIGNE():String
		{
			return _OLD_NUMLIGNE;
		}
		public function set OLD_NUMLIGNE(value:String):void
		{
			_OLD_NUMLIGNE = value;
		}

		public function get ISFIXE():int
		{
			return _ISFIXE;
		}
		public function set ISFIXE(value:int):void
		{
			_ISFIXE = value;
		}

		public function get ISMOBILE():int
		{
			return _ISMOBILE;
		}
		public function set ISMOBILE(value:int):void
		{
			_ISMOBILE = value;
		}

		public function get ISVERIF_AVAILABLE():Boolean
		{
			return _ISVERIF_AVAILABLE;
		}
		public function set ISVERIF_AVAILABLE(value:Boolean):void
		{
			_ISVERIF_AVAILABLE = value;
		}
		
		public function get COLLAB_TO_IMPORT():Boolean
		{
			return _COLLAB_TO_IMPORT;
		}
		public function set COLLAB_TO_IMPORT(value:Boolean):void
		{
			_COLLAB_TO_IMPORT = value;
		}

		public function get IMEI_TO_IMPORT():Boolean
		{
			return _IMEI_TO_IMPORT;
		}
		public function set IMEI_TO_IMPORT(value:Boolean):void
		{
			_IMEI_TO_IMPORT = value;
		}

		public function get NUMEXT_TO_IMPORT():Boolean
		{
			return _NUMEXT_TO_IMPORT;
		}
		public function set NUMEXT_TO_IMPORT(value:Boolean):void
		{
			_NUMEXT_TO_IMPORT = value;
		}

		public function get NUMSERIE_TO_IMPORT():Boolean
		{
			return _NUMSERIE_TO_IMPORT;
		}
		public function set NUMSERIE_TO_IMPORT(value:Boolean):void
		{
			_NUMSERIE_TO_IMPORT = value;
		}

		public function get NUMSIM_TO_IMPORT():Boolean
		{
			return _NUMSIM_TO_IMPORT;
		}
		public function set NUMSIM_TO_IMPORT(value:Boolean):void
		{
			_NUMSIM_TO_IMPORT = value;
		}

		public function get NUMLIGNE_TO_IMPORT():Boolean
		{
			return _NUMLIGNE_TO_IMPORT;
		}
		public function set NUMLIGNE_TO_IMPORT(value:Boolean):void
		{
			_NUMLIGNE_TO_IMPORT = value;
		}
		
		public function get GARANTIE_AVAILABLE():Boolean
		{
			return _GARANTIE_AVAILABLE;
		}
		public function set GARANTIE_AVAILABLE(value:Boolean):void
		{
			_GARANTIE_AVAILABLE = value;
		}
		
		public function get COLLAB_OK():Boolean
		{
			return _COLLAB_OK;
		}
		public function set COLLAB_OK(value:Boolean):void
		{
			_COLLAB_OK = value;
		}
		
		public function get IMEI_OK():Boolean
		{
			return _IMEI_OK;
		}
		public function set IMEI_OK(value:Boolean):void
		{
			_IMEI_OK = value;
		}
		
		public function get NUMEXT_OK():Boolean
		{
			return _NUMEXT_OK;
		}
		public function set NUMEXT_OK(value:Boolean):void
		{
			_NUMEXT_OK = value;
		}
		
		public function get NUMSERIE_OK():Boolean
		{
			return _NUMSERIE_OK;
		}
		public function set NUMSERIE_OK(value:Boolean):void
		{
			_NUMSERIE_OK = value;
		}
		
		public function get NUMSIM_OK():Boolean
		{
			return _NUMSIM_OK;
		}
		public function set NUMSIM_OK(value:Boolean):void
		{
			_NUMSIM_OK = value;
		}
		
		public function get NUMLIGNE_OK():Boolean
		{
			return _NUMLIGNE_OK;
		}
		public function set NUMLIGNE_OK(value:Boolean):void
		{
			_NUMLIGNE_OK = value;
		}

		public function get IMEI_IS_REQUIRED():Boolean
		{
			return _IMEI_IS_REQUIRED;
		}
		public function set IMEI_IS_REQUIRED(value:Boolean):void
		{
			_IMEI_IS_REQUIRED = value;
		}
		
		public function get COLLAB_EN_SAISIE():Boolean
		{
			return _COLLAB_EN_SAISIE;
		}
		public function set COLLAB_EN_SAISIE(value:Boolean):void
		{
			_COLLAB_EN_SAISIE = value;
		}

		public function get IMEI_EN_SAISIE():Boolean
		{
			return _IMEI_EN_SAISIE;
		}
		public function set IMEI_EN_SAISIE(value:Boolean):void
		{
			_IMEI_EN_SAISIE = value;
		}

		public function get NUMSERIE_EN_SAISIE():Boolean
		{
			return _NUMSERIE_EN_SAISIE;
		}
		public function set NUMSERIE_EN_SAISIE(value:Boolean):void
		{
			_NUMSERIE_EN_SAISIE = value;
		}
		
		public function get NUMEXT_EN_SAISIE():Boolean
		{
			return _NUMEXT_EN_SAISIE;
		}
		public function set NUMEXT_EN_SAISIE(value:Boolean):void
		{
			_NUMEXT_EN_SAISIE = value;
		}
		
		public function get NUMSIM_EN_SAISIE():Boolean
		{
			return _NUMSIM_EN_SAISIE;
		}
		public function set NUMSIM_EN_SAISIE(value:Boolean):void
		{
			_NUMSIM_EN_SAISIE = value;
		}

		public function get NUMLIGNE_EN_SAISIE():Boolean
		{
			return _NUMLIGNE_EN_SAISIE;
		}
		public function set NUMLIGNE_EN_SAISIE(value:Boolean):void
		{
			_NUMLIGNE_EN_SAISIE = value;
		}
	}
}