package gestionparc.entity
{
	import flash.events.EventDispatcher;
	
	import gestionparc.ihm.fiches.rago.entity.TypeRegle;
	
	public class TypeLigneVO extends EventDispatcher
	{
		//--------------- VARIABLES ----------------//
		
		private var _ISFIXE				:Boolean;
		private var _ISMOBILE			:Boolean;
		
		private var _CODE_FT			:String;
		
		private var _IDTYPE_LIGNE		:int;
		private var _LIBELLE_TYPE_LIGNE	:String;
		private var _TYPE_FICHELIGNE	:String;
		
		private var _SELECTED			:Boolean;
		
		
		//--------------- METHODES ----------------//
		
		/* */
		public function TypeLigneVO()
		{
			super();
		}
		
		/* */
		public function fillVO(obj:Object):void
		{
			if(obj.hasOwnProperty("CODE_FT"))
				this._CODE_FT = obj.CODE_FT;
			
			if(obj.hasOwnProperty("IDTYPE_LIGNE"))
				this._IDTYPE_LIGNE = obj.IDTYPE_LIGNE;
			
			if(obj.hasOwnProperty("LIBELLE_TYPE_LIGNE"))
				this._LIBELLE_TYPE_LIGNE = obj.LIBELLE_TYPE_LIGNE;
			
			if(obj.hasOwnProperty("TYPE_FICHELIGNE"))
				this._TYPE_FICHELIGNE = obj.TYPE_FICHELIGNE;
			
			if(obj.hasOwnProperty("ISFIXE"))
				this._ISFIXE = obj.ISFIXE;
			
			if(obj.hasOwnProperty("ISMOBILE"))
				this._ISMOBILE = obj.ISMOBILE;
		}
		
		
		//--------------- GETTERS - SETTERS ----------------//
		
		public function get IDTYPE_LIGNE():int
		{
			return _IDTYPE_LIGNE;
		}
		public function set IDTYPE_LIGNE(value:int):void
		{
			_IDTYPE_LIGNE = value;
		}
		
		public function get LIBELLE_TYPE_LIGNE():String
		{
			return _LIBELLE_TYPE_LIGNE;
		}
		public function set LIBELLE_TYPE_LIGNE(value:String):void
		{
			_LIBELLE_TYPE_LIGNE = value;
		}
		
		public function get TYPE_FICHELIGNE():String
		{
			return _TYPE_FICHELIGNE;
		}
		public function set TYPE_FICHELIGNE(value:String):void
		{
			_TYPE_FICHELIGNE = value;
		}
		
		public function get CODE_FT():String
		{
			return _CODE_FT;
		}
		public function set CODE_FT(value:String):void
		{
			_CODE_FT = value;
		}
		
		[Bindable]
		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}
		public function set SELECTED(value:Boolean):void
		{
			_SELECTED = value;
		}
	}
}