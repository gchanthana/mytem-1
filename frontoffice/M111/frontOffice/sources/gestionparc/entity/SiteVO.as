package gestionparc.entity
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class SiteVO
	{
		public var idSite:int=-1;
		public var libelleSite:String="";

		public var refSite:String="";
		public var codeInterneSite:String="";

		public var adresse1:String="";
		public var adresse2:String="";
		public var ville:String="";
		public var codePostale:String="";
		public var pays:String="";
		public var idPays:int=-1;

		public var commentaires:String="";

		public var SELECTED:Boolean=false;

		public var listeEmplacement:ArrayCollection=null;

		/**
		 * La fonction remplit une liste d'objets SiteVO avec les donnees passees en parametre et retourne cette liste
		 */
		public function fillListSiteVO(obj:Object):ArrayCollection
		{
			var mySiteVO:SiteVO=null;
			var longueur:int=(obj as ArrayCollection).length;
			var listSiteVO:ArrayCollection=new ArrayCollection();

			for (var i:int=0; i < longueur; i++)
			{
				mySiteVO=new SiteVO();

				mySiteVO.idSite=obj[i].IDSITE_PHYSIQUE;
				mySiteVO.libelleSite=obj[i].NOM_SITE;
				mySiteVO.refSite=obj[i].SP_REFERENCE;
				mySiteVO.codeInterneSite=obj[i].CODE_INTERNE;
				mySiteVO.adresse1=obj[i].ADRESSE;
				mySiteVO.ville=obj[i].SP_COMMUNE;
				mySiteVO.codePostale=obj[i].SP_CODE_POSTAL;
				mySiteVO.idPays=obj[i].PAYSCONSOTELID;
				mySiteVO.pays=obj[i].PAYS;
				mySiteVO.commentaires=obj[i].SP_COMMENTAIRE;
				mySiteVO.SELECTED=false;

				listSiteVO.addItem(mySiteVO);
			}

			return listSiteVO;
		}

		/**
		 * La fonction remplit un objet SiteVO avec les donnees passees en parametre et retourne cette SiteVO
		 */
		public function fillSiteVO(obj:Object):SiteVO
		{
			var mySiteVO:SiteVO=new SiteVO();

			mySiteVO.idSite=obj[0][0].IDSITE_PHYSIQUE;
			mySiteVO.libelleSite=obj[0][0].LIBELLE_SITE;
			mySiteVO.refSite=obj[0][0].REFERENCE;
			mySiteVO.codeInterneSite=obj[0][0].CODE_INTERNE;
			mySiteVO.adresse1=obj[0][0].ADRESSE;
			mySiteVO.adresse2="";
			mySiteVO.ville=obj[0][0].VILLE;
			mySiteVO.codePostale=obj[0][0].CODE_POSTALE;
			mySiteVO.idPays=obj[0][0].IDPAYS;
			mySiteVO.commentaires=obj[0][0].COMMENTAIRE;

			// les emplacements
			mySiteVO.listeEmplacement=new ArrayCollection();

			var emplacementLen:int=(obj[1] as ArrayCollection).length;
			var myEmplacementVO:EmplacementVO=null;

			for (var i:int=0; i < emplacementLen; i++)
			{
				myEmplacementVO=new EmplacementVO();
				myEmplacementVO.idEmplcament=(obj[1][i].IDEMPLACEMENT) ? obj[1][i].IDEMPLACEMENT : -1;
				myEmplacementVO.libelleEmplacement=(obj[1][i].LIBELLE_EMPLACEMENT) ? obj[1][i].LIBELLE_EMPLACEMENT : "";
				myEmplacementVO.batiment=(obj[1][i].BATIMENT) ? obj[1][i].BATIMENT : "";
				myEmplacementVO.etage=(obj[1][i].ETAGE) ? obj[1][i].ETAGE : "";
				myEmplacementVO.salle=(obj[1][i].SALLE) ? obj[1][i].SALLE : "";
				myEmplacementVO.couloir=(obj[1][i].COULOIR) ? obj[1][i].COULOIR : "";
				myEmplacementVO.armoire=(obj[1][i].ARMOIRE) ? obj[1][i].ARMOIRE : "";

				mySiteVO.listeEmplacement.addItem(myEmplacementVO);
			}

			return mySiteVO;
		}

		/**
		 * La fonction remplit une liste d'objet EmplacementVO avec les donnees passees en parametre et retourne cette liste.
		 */
		public function fillListEmplacementVO(obj:Object):ArrayCollection
		{
			var listeEmplacement:ArrayCollection=new ArrayCollection();
			var emplacementLen:int=(obj[0] as ArrayCollection).length;
			var myEmplacementVO:EmplacementVO=null;

			for (var i:int=0; i < emplacementLen; i++)
			{
				myEmplacementVO=new EmplacementVO();
				myEmplacementVO.idEmplcament=(obj[0][i].IDEMPLACEMENT) ? obj[0][i].IDEMPLACEMENT : -1;
				myEmplacementVO.libelleEmplacement=(obj[0][i].LIBELLE_EMPLACEMENT) ? obj[0][i].LIBELLE_EMPLACEMENT : "";
				myEmplacementVO.batiment=(obj[0][i].BATIMENT) ? obj[0][i].BATIMENT : "";
				myEmplacementVO.etage=(obj[0][i].ETAGE) ? obj[0][i].ETAGE : "";
				myEmplacementVO.salle=(obj[0][i].SALLE) ? obj[0][i].SALLE : "";
				myEmplacementVO.couloir=(obj[0][i].COULOIR) ? obj[0][i].COULOIR : "";
				myEmplacementVO.armoire=(obj[0][i].ARMOIRE) ? obj[0][i].ARMOIRE : "";

				listeEmplacement.addItem(myEmplacementVO);
			}

			return listeEmplacement;
		}
	}
}