package gestionparc.entity
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class IncidentVO
	{
		public var idIncident:int=-1;
		public var refIncident:String="";
		public var numIncident:String="";

		public var revendeur:String=""; //pour le dataField

		public var userCreate:String="";
		public var userModify:String="";
		public var userClose:String="";

		public var totalCoutIntervention:Number=0;
		public var nbIntervention:int=0;

		public var description:String="";
		public var solution:String="";

		public var dateDebut:Date=null;
		public var dateFin:Date=null;

		public var etat:String="";
		public var isClose:int=0;

		public var listeIntervention:ArrayCollection=null;

	}
}