package gestionparc.entity
{
	import composants.util.DateFunction;

	import mx.collections.ArrayCollection;

	public class Location
	{

		public var IDDEVICE:int=0;
		public var IDUSER:int=0;
		public var IDSTATUT:int=0; //---> -1 -> ECHEC, 1 -> SUCCES, 0 -> EN COURS 

		public var USER:String='';
		public var LONGITUDE:String='';
		public var LATITUDE:String='';
		public var LOCATION:String='';
		public var IMEI:String='';
		public var FAILURE_DATE_STR:String='';
		public var SUCCESS_DATE_STR:String='';
		public var REQUEST_DATE_STR:String='';

		public var FAILURE_DATE:Date=null;
		public var SUCCESS_DATE:Date=null;
		public var REQUEST_DATE:Date=null;


		public function Location()
		{
		}

		public static function copyLocation(value:Location):Location
		{
			var location:Location=new Location();
			location.IDDEVICE=value.IDDEVICE;
			location.IDUSER=value.IDUSER;
			location.IDSTATUT=value.IDSTATUT;
			location.USER=value.USER;
			location.LONGITUDE=value.LONGITUDE;
			location.LATITUDE=value.LATITUDE;
			location.LOCATION=value.LOCATION;
			location.IMEI=value.IMEI;
			location.FAILURE_DATE_STR=value.FAILURE_DATE_STR;
			location.SUCCESS_DATE_STR=value.SUCCESS_DATE_STR;
			location.REQUEST_DATE_STR=value.REQUEST_DATE_STR;
			location.FAILURE_DATE=value.FAILURE_DATE;
			location.SUCCESS_DATE=value.SUCCESS_DATE;
			location.REQUEST_DATE=value.REQUEST_DATE;

			return location;
		}


		public static function formatLocation(values:ArrayCollection, imei:String):ArrayCollection
		{
			var locations:ArrayCollection=new ArrayCollection();
			var lenOri:int=values.length;

			for (var i:int=0; i < lenOri; i++)
			{
				locations.addItem(mappingLocation(values[i], imei));
			}

			return locations;
		}

		public static function mappingLocation(value:Object, imei:String):Location
		{
			var location:Location=new Location();
			location.IDDEVICE=value.DEVICE_ID;
			location.LONGITUDE=value.LONGITUDE;
			location.LATITUDE=value.LATITUDE;
			location.IMEI=imei;
			location.FAILURE_DATE=value.FAILURE_DATE;
			location.SUCCESS_DATE=value.SUCCESS_DATE;
			location.REQUEST_DATE=value.REQUEST_DATE;

			if (location.FAILURE_DATE != null)
				location.FAILURE_DATE_STR=DateFunction.formatDateAsString(location.FAILURE_DATE);

			if (location.SUCCESS_DATE != null)
				location.SUCCESS_DATE_STR=DateFunction.formatDateAsString(location.SUCCESS_DATE);

			if (location.REQUEST_DATE != null)
				location.REQUEST_DATE_STR=DateFunction.formatDateAsString(location.REQUEST_DATE);

			if (location.FAILURE_DATE != null)
			{
				location.IDSTATUT=-1;
			}
			else if (location.SUCCESS_DATE != null)
			{
				location.IDSTATUT=1;
			}

			if (((location.LONGITUDE != null && location.LONGITUDE != '') && (location.LATITUDE != null && location.LATITUDE != '')) && location.IDSTATUT > 0)
			{
				location.LOCATION="https://maps.google.com/maps?q=localisation+terminal+" + location.IMEI + "%40" + location.LATITUDE + "," + location.LONGITUDE + "&t=m&z=16";
			}


			return location;
		}
	}
}
