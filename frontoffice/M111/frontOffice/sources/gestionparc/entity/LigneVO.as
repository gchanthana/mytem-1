package gestionparc.entity
{
	import mx.collections.ArrayCollection;
	
	import gestionparc.services.cache.CacheDatas;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Value Object représentant les propriétés d'une ligne.
	 */
	[Bindable]
	public class LigneVO
	{
		public var idSousTete:int=-1;

		public var sousTete:String="";

		public var idTypeLigne:int=-1;
		public var typeLigne:String="";

		public var idContrat:int=-1;
		public var numContrat:String="";
		public var aboPrincipale:String="";

		public var etat:int=-1;

		public var isLignePrincipale:int=-1;
		public var isLigneBackUp:int=-1;

		public var idTypeRaccordement:int=-1;
		public var libTypeRaccordement:String="";

		public var listeHistoRaccor:ArrayCollection=null;

		public var titulaire:int=-1;
		public var payeur:int=-1;
		public var fonction:String="";

		public var compte:String="";
		public var sousCompte:String="";
		public var idCompte:int=-1;
		public var idSousCompte:int=-1;
		public var codeRio="";

		public var operateur:OperateurVO=null;
		public var operateurPreselect:OperateurVO=null;

		public var fournisseur:RevendeurVO=null;

		public var collabRattache:CollaborateurVO=null;

		public var simRattache:SIMVO=null;
		public var eqpRattache:SIMVO=null;

		public var idSite:int=-1;
		public var idEmplacement:int=-1;
		public var commentaires:String="";

		public var listeOrganisation:ArrayCollection=null;
		public var positionOrga:String="";

		public var listeAboOption:ArrayCollection=null;

		public var dureeContrat:int=0;
		public var dateOuverture:Date=null;
		public var dateRenouvellement:Date=null;
		public var dateResiliation:Date=null;
		public var dureeEligibilite:int=0;
		public var dateEligibilite:Date=null;
		public var dateFPC:Date=null;
		public var dateDerniereFacture:Date=null;

		public var libelleChampPerso1:String="";
		public var libelleChampPerso2:String="";
		public var libelleChampPerso3:String="";
		public var libelleChampPerso4:String="";

		public var texteChampPerso1:String="";
		public var texteChampPerso2:String="";
		public var texteChampPerso3:String="";
		public var texteChampPerso4:String="";

		public var TYPE_FICHELIGNE:String="";

		public var canaux_sortie_deb:int=0;
		public var canaux_sortie_fin:int=0;
		public var canaux_entree_deb:int=0;
		public var canaux_entree_fin:int=0;
		public var canaux_mixte_deb:int=0;
		public var canaux_mixte_fin:int=0;
		public var nb_canaux:int=0;
		public var tete_ligne:String="";
		public var idtete_ligne:int=0;
		public var boolTeteLigne:Boolean=false;
		public var boolGroupement:Boolean=false;

		public var amorce:String="";
		public var reglette1:String="";
		public var reglette2:String="";
		public var paire1:String="";
		public var paire2:String="";

		public var PtAElemActifAmorceEmission:String="";
		public var PtAElemActifAmorceReception:String="";
		public var PtAAmorceTeteEmission:String="";
		public var PtAAmorceTeteReception:String="";
		public var PtAElemActif:String="";
		public var PtAAdresse:String="";
		public var PtACodePostal:String="";
		public var PtACommune:String="";
		public var PtBElemActifAmorceEmission:String="";
		public var PtBElemActifAmorceReception:String="";
		public var PtBAmorceTeteEmission:String="";
		public var PtBAmorceTeteReception:String="";
		public var PtBElemActif:String="";
		public var PtBAdresse:String="";
		public var PtBCodePostal:String="";
		public var PtBCommune:String="";
		public var PtAIdentifiantLL:String="";
		public var PtBIdentifiantLL:String="";
		public var Nature:String="";
		public var Debit:String="";
		public var Distance:String="";

		public var nomReseau:String="";
		public var usage:int=-1;
		public var debitMontantUnite:String="";
		public var debitMontantContractuel:int=-1;
		public var debitMontantMax:int=-1;
		public var debitDescendantUnite:String="";
		public var debitDescendantContractuel:int=-1;
		public var debitDescendantMax:int=-1;
		public var GTRContractuel:String="";
		public var Presentation:String="";
		public var protocolePhysique:String="";
		public var protocoleLiaison:String="";
		public var protocoleReseau:String="";

		public var inst_adresse1:String="";
		public var inst_adresse2:String="";
		public var inst_codesite:String="";
		public var inst_commune:String="";
		public var inst_nomsite:String="";
		public var inst_zipcode:String="";

		public var listeGroupement:ArrayCollection=new ArrayCollection();
		public var listeTranche:ArrayCollection=new ArrayCollection();
		
		public var operateurTelecom				:String = '';
		public var compteFacturationTelecom 	:String = '';

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * La fonction remplit un objet LigneVO avec les donnees passees en parametre et retourne cette LigneVO
		 * </pre></p>
		 *
		 * @param obj
		 * @return LigneVO
		 *
		 */
		public function fillLigneVO(obj:Object):LigneVO
		{
			var myLigneVO:LigneVO=new LigneVO();
			myLigneVO.simRattache=new SIMVO();
			myLigneVO.eqpRattache=new SIMVO();
			myLigneVO.operateur=new OperateurVO();
			myLigneVO.operateurPreselect=new OperateurVO();
			myLigneVO.fournisseur=new RevendeurVO();
			// Ajout des infos concernant la ligne
			if (obj[1] && obj[1].length > 0)
			{
				myLigneVO.idSousTete=obj[1][0].IDSOUS_TETE;
				myLigneVO.sousTete=obj[1][0].SOUS_TETE;
				myLigneVO.idContrat=(obj[1][0].IDCONTRAT_ABO) ? obj[1][0].IDCONTRAT_ABO : -1;
				myLigneVO.numContrat=obj[1][0].NUM_CONTRAT_ABO;
				myLigneVO.aboPrincipale=obj[1][0].ABO_PRINCIPAL;
				myLigneVO.etat=obj[1][0].ETAT_LIGNE;
				myLigneVO.TYPE_FICHELIGNE=obj[1][0].TYPE_FICHELIGNE;
				myLigneVO.typeLigne=obj[1][0].LIBELLE_TYPE_LIGNE;
				myLigneVO.idTypeLigne=obj[1][0].IDTYPE_LIGNE;
				myLigneVO.titulaire=obj[1][0].BOOL_TITULAIRE;
				myLigneVO.payeur=obj[1][0].BOOL_PAYEUR;
				myLigneVO.fonction=obj[1][0].FONCTION;
				myLigneVO.compte=obj[1][0].COMPTE_FACTURATION;
				myLigneVO.sousCompte=obj[1][0].SOUS_COMPTE;
				myLigneVO.idCompte=(obj[1][0].IDCOMPTE_FACTURATION) ? obj[1][0].IDCOMPTE_FACTURATION : -1;
				myLigneVO.idSousCompte=(obj[1][0].IDSOUS_COMPTE) ? obj[1][0].IDSOUS_COMPTE : -1;
				myLigneVO.idSite=(obj[1][0].IDSITE_PHYSIQUE) ? obj[1][0].IDSITE_PHYSIQUE : -1;
				myLigneVO.idEmplacement=(obj[1][0].IDEMPLACEMENT) ? obj[1][0].IDEMPLACEMENT : -1;
				myLigneVO.commentaires=obj[1][0].COMMENTAIRES;
				myLigneVO.codeRio=obj[1][0].CODE_RIO;

				myLigneVO.inst_adresse1=obj[1][0].INS_ADRESSE1;
				myLigneVO.inst_adresse2=obj[1][0].INS_ADRESSE2;
				myLigneVO.inst_codesite=obj[1][0].INS_CODESITE;
				myLigneVO.inst_commune=obj[1][0].INS_COMMUNE;
				myLigneVO.inst_nomsite=obj[1][0].INS_NOMSITE;
				myLigneVO.inst_zipcode=obj[1][0].INS_ZIPCODE;

				myLigneVO.isLignePrincipale=obj[1][0].BOOL_PRINCIPAL;
				myLigneVO.isLigneBackUp=obj[1][0].BOOL_BACKUP;

				myLigneVO.idTypeRaccordement=obj[1][0].IDTYPE_RACCORDEMENT;
				myLigneVO.libTypeRaccordement=obj[1][0].LIBELLE_RACCORDEMENT;

				// Ajout des infos concernant la SIM
				myLigneVO.simRattache.idEquipement=(obj[1][0].IDSIM) ? obj[1][0].IDSIM : -1;
				myLigneVO.simRattache.numSerieEquipement=obj[1][0].NUM_SIM;
				myLigneVO.simRattache.PIN1=obj[1][0].PIN1;
				myLigneVO.simRattache.PIN2=obj[1][0].PIN2;
				myLigneVO.simRattache.PUK1=obj[1][0].PUK1;
				myLigneVO.simRattache.PUK2=obj[1][0].PUK2;

				// Ajout des infos concernant l'equipement fixe
				myLigneVO.eqpRattache.idEquipement=(obj[1][0].IDEQP) ? obj[1][0].IDEQP : -1;
				myLigneVO.eqpRattache.numSerieEquipement=obj[1][0].NUM_SERIE;

				// Ajout des infos concernant le contrat de la ligne
				myLigneVO.dureeContrat=obj[1][0].DUREE_CONTRAT;
				myLigneVO.dateOuverture=obj[1][0].DATE_OUVERTURE;
				myLigneVO.dateRenouvellement=obj[1][0].DATE_RENOUVELLEMENT;
				myLigneVO.dateResiliation=obj[1][0].DATE_RESILIATION;
				myLigneVO.dureeEligibilite=obj[1][0].DUREE_ELLIGIBILITE;
				myLigneVO.dateEligibilite=obj[1][0].DATE_ELLIGIBILITE;
				myLigneVO.dateFPC=obj[1][0].DATE_FPC;
				myLigneVO.dateDerniereFacture=obj[1][0].ACCES_LAST_FACTURE;

				myLigneVO.operateur.nom=obj[1][0].OPERATEUR;
				myLigneVO.operateur.idOperateur=obj[1][0].OPERATEURID;

				myLigneVO.operateurPreselect.nom=obj[1][0].OPERATEUR_PRESELECTION;
				myLigneVO.operateurPreselect.idOperateur=obj[1][0].OPERATEURID_PRESELECTION;

				myLigneVO.fournisseur.idRevendeur=obj[1][0].IDREVENDEUR;

				myLigneVO.texteChampPerso1=obj[1][0].V1;
				myLigneVO.texteChampPerso2=obj[1][0].V2;
				myLigneVO.texteChampPerso3=obj[1][0].V3;
				myLigneVO.texteChampPerso4=obj[1][0].V4;
				myLigneVO.operateurTelecom = obj[1][0].OPERATEUR_TELECOM;
				myLigneVO.compteFacturationTelecom = obj[1][0].COMPTE_FACTURATION_TELECOM;
				
			}

			// Ajout des libellés
			myLigneVO.libelleChampPerso1=CacheDatas.libellesPersoLigne1;
			myLigneVO.libelleChampPerso2=CacheDatas.libellesPersoLigne2;
			myLigneVO.libelleChampPerso3=CacheDatas.libellesPersoLigne3;
			myLigneVO.libelleChampPerso4=CacheDatas.libellesPersoLigne4;

			// Ajout des infos concernant la liste des abonnements et options de la ligne
			myLigneVO.listeAboOption=new ArrayCollection();
			var lenListeAboOption:int=(obj[2] as ArrayCollection).length;
			var aboOpt:Object;

			for (var index:int=0; index < lenListeAboOption; index++)
			{
				if (obj[2][index].BOOL_FACTURE != 0 || obj[2][index].BOOL_IN_INVENTAIRE != 0)
				{
					aboOpt=new Object();
					aboOpt.operateur=obj[2][index].OPERATEUR_NOM;
					aboOpt.libelleProduit=obj[2][index].LIBELLE_PRODUIT;
					aboOpt.idInventaireProduit=obj[2][index].IDINVENTAIRE_PRODUIT;
					aboOpt.dansInventaire=obj[2][index].BOOL_IN_INVENTAIRE;
					aboOpt.boolAcces=obj[2][index].BOOL_ACCES;
					aboOpt.boolFacture=obj[2][index].BOOL_FACTURE;
					aboOpt.boolInOrder=obj[2][index].BOOL_IN_ORDER;
					aboOpt.aEntrer=0;
					aboOpt.aSortir=0;
					aboOpt.date=null;

					myLigneVO.listeAboOption.addItem(aboOpt);
				}
			}

			// Ajout des infos concernant la liste des historiques des raccordement
			myLigneVO.listeHistoRaccor=new ArrayCollection();
			myLigneVO.listeHistoRaccor=obj[3];

			// Ajout des infos concernant les organisations et position
			myLigneVO.listeOrganisation=new ArrayCollection();

			var lenListeOrga:int=(obj[4] as ArrayCollection).length;
			var newOrga:OrgaVO;

			for (var i:int=0; i < lenListeOrga; i++)
			{
				newOrga=new OrgaVO();
				newOrga.idOrga=obj[4][i].IDORGA;
				newOrga.libelleOrga=obj[4][i].LIBELLE_ORGA;
				newOrga.idCible=obj[4][i].IDCIBLE;
				newOrga.position=obj[4][i].POSITION;
				newOrga.chemin=obj[4][i].CHEMIN_CIBLE;
				newOrga.idRegleOrga=obj[4][i].IDREGLE;
				newOrga.dateModifPosition=obj[4][i].DATE_MODIF_POSITION;

				myLigneVO.listeOrganisation.addItem(newOrga);
			}

			// ONGLET GROUPEMENT
			if (obj[1] && obj[1].length > 0)
			{
				myLigneVO.canaux_entree_deb=obj[1][0].CANAUX_ENTREE_DEBUT;
				myLigneVO.canaux_entree_fin=obj[1][0].CANAUX_ENTREE_FIN;
				myLigneVO.canaux_mixte_deb=obj[1][0].CANAUX_MIXTE_DEBUT;
				myLigneVO.canaux_mixte_fin=obj[1][0].CANAUX_MIXTE_FIN;
				myLigneVO.canaux_sortie_deb=obj[1][0].CANAUX_SORTIE_DEBUT;
				myLigneVO.canaux_sortie_fin=obj[1][0].CANAUX_SORTIE_FIN;
				myLigneVO.nb_canaux=obj[1][0].NBRE_CANAUX;
				if (obj[1][0].TETE_LIGNE == null || obj[1][0].TETE_LIGNE < 0)
				{
					myLigneVO.tete_ligne=myLigneVO.sousTete;
					myLigneVO.idtete_ligne=myLigneVO.idSousTete;
				}
				else
				{
					myLigneVO.tete_ligne=obj[1][0].TETE_LIGNE;
					myLigneVO.idtete_ligne=obj[1][0].IDTETE_LIGNE;
				}
			}

			if ((obj[5] as ArrayCollection).length > 0)
			{
				myLigneVO.boolGroupement=true;
				// Si la ligne fait partie d'un groupement et que la tete_ligne est null, alors la ligne est la tête de ligne
				if (obj[1] && obj[1].length > 0)
				{
					if (obj[1][0].TETE_LIGNE == null || obj[1][0].TETE_LIGNE < 0 || obj[1][0].TETE_LIGNE == myLigneVO.sousTete)
					{
						myLigneVO.boolTeteLigne=true;
					}
					else
					{
						myLigneVO.boolTeteLigne=false;
					}
				}
			}
			else
			{
				myLigneVO.boolGroupement=false;
				myLigneVO.boolTeteLigne=false;
			}

			myLigneVO.listeGroupement=obj[5] as ArrayCollection;
			// ONGLET SDA - T2/T0
			if (obj[1] && obj[1].length > 0)
			{
				myLigneVO.amorce=(obj[1][0].AMORCE) ? obj[1][0].AMORCE : "";
				myLigneVO.reglette1=(obj[1][0].REGLETTE1) ? obj[1][0].REGLETTE1 : "";
				myLigneVO.reglette2=(obj[1][0].REGLETTE2) ? obj[1][0].REGLETTE2 : "";
				myLigneVO.paire1=(obj[1][0].PAIRE1) ? obj[1][0].PAIRE1 : "";
				myLigneVO.paire2=(obj[1][0].PAIRE2) ? obj[1][0].PAIRE2 : "";
			}
			for each (var o:Object in obj[6])
			{
				myLigneVO.listeTranche.addItem(TrancheVO.returnTrancheVO(o));
			}

			// ONGLET SDA - POINT A POINT
			if (obj[1] && obj[1].length > 0)
			{
				myLigneVO.PtAElemActifAmorceEmission=obj[1][0].LS_LEGA_ELEM_ACTIF_AMORCE_E;
				myLigneVO.PtAElemActifAmorceReception=obj[1][0].LS_LEGA_ELEM_ACTIF_AMORCE_R;
				myLigneVO.PtAAmorceTeteEmission=obj[1][0].LS_LEGA_TETE_AMORCE_E;
				myLigneVO.PtAAmorceTeteReception=obj[1][0].LS_LEGA_TETE_AMORCE_R;
				myLigneVO.PtAElemActif=obj[1][0].LS_LEGA_ELEM_ACTIF;
				myLigneVO.PtAAdresse=obj[1][0].LS_LEGA_ADRESSE1;
				myLigneVO.PtACodePostal=obj[1][0].LS_LEGA_ZIPCODE;
				myLigneVO.PtACommune=obj[1][0].LS_LEGA_COMMUNE;
				myLigneVO.PtBElemActifAmorceEmission=obj[1][0].LS_LEGB_ELEM_ACTIF_AMORCE_E;
				myLigneVO.PtBElemActifAmorceReception=obj[1][0].LS_LEGB_ELEM_ACTIF_AMORCE_R;
				myLigneVO.PtBAmorceTeteEmission=obj[1][0].LS_LEGB_TETE_AMORCE_E;
				myLigneVO.PtBAmorceTeteReception=obj[1][0].LS_LEGB_TETE_AMORCE_R;
				myLigneVO.PtBElemActif=obj[1][0].LS_LEGB_ELEM_ACTIF;
				myLigneVO.PtBAdresse=obj[1][0].LS_LEGB_ADRESSE1;
				myLigneVO.PtBCodePostal=obj[1][0].LS_LEGB_ZIPCODE;
				myLigneVO.PtBCommune=obj[1][0].LS_LEGB_COMMUNE;
				myLigneVO.PtAIdentifiantLL=obj[1][0].IDLLA;
				myLigneVO.PtBIdentifiantLL=obj[1][0].IDLLB;
				myLigneVO.Nature=obj[1][0].LS_NATURE;
				myLigneVO.Debit=obj[1][0].LS_DEBIT2;
				myLigneVO.Distance=obj[1][0].LS_DISTANCE2;

				// ONGLET CARACTERISTIQUE FIXE
				myLigneVO.nomReseau=obj[1][0].NOMRESEAU;
				myLigneVO.usage=obj[1][0].USAGE_SEGMENT;

				myLigneVO.debitMontantUnite=obj[1][0].DEBITMONTANTUNITE;
				myLigneVO.debitMontantContractuel=(obj[1][0].DEBITMONTANTCONTRAT) ? obj[1][0].DEBITMONTANTCONTRAT : -1;
				myLigneVO.debitMontantMax=(obj[1][0].DEBITMONTANTMAX) ? obj[1][0].DEBITMONTANTMAX : -1;

				myLigneVO.debitDescendantUnite=obj[1][0].DEBITDESCENDANTUNITE;
				myLigneVO.debitDescendantContractuel=(obj[1][0].DEBITDESCENDANTCONTRAT) ? obj[1][0].DEBITDESCENDANTCONTRAT : -1;
				myLigneVO.debitDescendantMax=(obj[1][0].DEBITDESCENDANTMAX) ? obj[1][0].DEBITDESCENDANTMAX : -1;

				myLigneVO.GTRContractuel=obj[1][0].GTRCONTRAT;
				myLigneVO.Presentation=obj[1][0].PRESENTATION;
				myLigneVO.protocolePhysique=obj[1][0].PROTOCOLEPHYSIQUE;
				myLigneVO.protocoleLiaison=obj[1][0].PROTOCOLELIAISON;
				myLigneVO.protocoleReseau=obj[1][0].PROTOCOLERESEAU;
			}

			return myLigneVO;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * La fonction met a jour les donnees des abo et opt avec les valeurs de colonne facture
		 * </pre></p>
		 *
		 * @param obj
		 * @return LigneVO
		 */
		public function fillColFacureLigneVO(ligne:LigneVO, obj:Object):LigneVO
		{
			var myLigneVO:LigneVO=new LigneVO();
			myLigneVO=ligne;

			// Ajout des infos concernant la liste des abonnements et options de la ligne
			myLigneVO.listeAboOption=new ArrayCollection();
			var lenListeAboOption:int=(obj[1] as ArrayCollection).length;
			var aboOpt:Object;

			for (var index:int=0; index < lenListeAboOption; index++)
			{
				aboOpt=new Object();
				aboOpt.operateur=obj[1][index].OPERATEUR_NOM;
				aboOpt.libelleProduit=obj[1][index].LIBELLE_PRODUIT;
				aboOpt.idInventaireProduit=obj[1][index].IDINVENTAIRE_PRODUIT;
				aboOpt.dansInventaire=obj[1][index].BOOL_IN_INVENTAIRE;
				aboOpt.facture=obj[1][index].BOOL_FACT;
				aboOpt.aEntrer=0;
				aboOpt.aSortir=0;

				myLigneVO.listeAboOption.addItem(aboOpt);
			}

			return myLigneVO;
		}
	}

}
