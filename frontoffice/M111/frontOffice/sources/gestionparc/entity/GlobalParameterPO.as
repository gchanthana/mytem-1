package gestionparc.entity
{
	import mx.collections.ArrayCollection;

	public dynamic class GlobalParameterPO
	{
		public var userAccess	:UserAccessPO;
		public var racineInfos	:ArrayCollection;
		public var listOS		:ArrayCollection;
		public var userSession	:UserSessionVO;
		
		public function GlobalParameterPO()
		{
			
		}

	}
}
