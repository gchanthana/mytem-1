package gestionparc.entity
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import gestionparc.ihm.main.restriction.GestionnaireRestriction;
	import gestionparc.ihm.main.vue.IVue;
	import gestionparc.ihm.renderer.commonrenderer.ByodRenderer;
	import gestionparc.ihm.renderer.commonrenderer.OperateurRenderer;
	import gestionparc.ihm.renderer.commonrenderer.OperateurTelecomRenderer;
	import gestionparc.ihm.renderer.commonrenderer.dateEntreeItemRenderer;
	import gestionparc.ihm.renderer.commonrenderer.dateSortieItemRenderer;
	import gestionparc.ihm.renderer.entityrenderer.CollaborateurItemRenderer;
	import gestionparc.ihm.renderer.entityrenderer.EquipementItemRenderer;
	import gestionparc.ihm.renderer.entityrenderer.LigneItemRenderer;
	import gestionparc.ihm.renderer.entityrenderer.MatriculeItemRenderer;
	import gestionparc.ihm.renderer.entityrenderer.ModelelItemRenderer;
	import gestionparc.ihm.renderer.entityrenderer.SerieItemRenderer;
	import gestionparc.ihm.renderer.entityrenderer.SimItemRenderer;
	import gestionparc.ihm.renderer.entityrenderer.StatusIMEIItemRenderer;
	import gestionparc.ihm.renderer.entityrenderer.StatusLigneItemRenderer;
	import gestionparc.ihm.renderer.entityrenderer.StatusSIMItemRenderer;
	import gestionparc.services.export.ExportAllService;

	[Bindable]
	public class AbstractVue extends EventDispatcher implements IVue
	{
		public static const MIXTE:String="Mixte";

		private var _ISRESTRICTIONLIGNE:Boolean;
		private var _ISRESTRICTIONSIM:Boolean;
		private var _ISRESTRICTIONEQUIPEMENT:Boolean;
		private var _ISRESTRICTIONCOLLABORATEUR:Boolean;
		private var _ISRESTRICTIONCONTRAT:Boolean;

		private var _LIBELLE:String;
		private var _TYPE:String;
		private var _ISVUEALERTE:Boolean=false;
		private var _ISVUECONSOS:Boolean=false;
		private var _IDVUE:int;
		private var _LISTE_COLONNE:ArrayCollection;
		private var _LISTE_RESTRICTION:ArrayCollection;
		private var _active:Boolean=false;
		private var _ISMIXTE:Boolean;

		public var exportService:ExportAllService=new ExportAllService();

		private var gestRestriction:GestionnaireRestriction=new GestionnaireRestriction();

		public function AbstractVue(colonneList:ArrayCollection)
		{
		}

		/**
		 * créer une vue
		 * @param id : l'id de la vue créé
		 */
		public function createVue(lib:String, typ:String, id:int, col:ArrayCollection, activ:Boolean=false, vueAlerte:Boolean=false, vueConso:Boolean=false, isligne:Boolean=false, isSIM:Boolean=false, isEquipmeent:Boolean=false, isCollab:Boolean=false, isContrat:Boolean=false, needOrga:Boolean=false, needIdMois:Boolean=false):void
		{
			this.LIBELLE=lib;
			this.TYPE=typ;
			this.IDVUE=id;
			this.ISVUEALERTE=vueAlerte;
			this.ISVUECONSOS=vueConso;
			this.LISTE_COLONNE=col;
			this.active=activ;
			this.ISRESTRICTIONCOLLABORATEUR=isCollab;
			this.ISRESTRICTIONCONTRAT=isContrat;
			this.ISRESTRICTIONEQUIPEMENT=isEquipmeent;
			this.ISRESTRICTIONLIGNE=isligne;
			this.ISRESTRICTIONSIM=isSIM;
			this.LISTE_RESTRICTION=gestRestriction.buildListRestriction(this);
		}

		public function exportAll():void
		{
		}

		public function createAllColonnes(listeColonnes:ArrayCollection):ArrayCollection
		{
			var arrAllColonne:ArrayCollection=new ArrayCollection();
			var newColonne:Colonne;
			listeColonnes = moveByodColumn(listeColonnes);
			this.setOrderStatusIMEI(listeColonnes);
			this.setOrderStatusSIM(listeColonnes);
			this.setOrderStatusLigne(listeColonnes);
			this.setOrderPoolCollaborateur(listeColonnes);
			for (var i:int=0; i < listeColonnes.length; i++)
			{
				switch ((listeColonnes[i] as Colonne).CODE)
				{
					case "col_1":
						newColonne=createColonne(listeColonnes[i] as Colonne,CollaborateurItemRenderer, 160);
						arrAllColonne.addItem(newColonne);
						break;
					
					case "col_2":
						newColonne=createColonne(listeColonnes[i] as Colonne, MatriculeItemRenderer, 80);
						arrAllColonne.addItem(newColonne);
						break;

					case "col_3":
						newColonne=createColonne(listeColonnes[i] as Colonne, ModelelItemRenderer, 155); /**  ModelelItemRenderer permet d'afficher le label avec  trois ... point */
						arrAllColonne.addItem(newColonne);
						break;

					case "col_4"://IMEI
						newColonne=createColonne(listeColonnes[i] as Colonne,EquipementItemRenderer, 135);
						arrAllColonne.addItem(newColonne);
						break;

					case "col_5":
						newColonne=createColonne(listeColonnes[i] as Colonne,SerieItemRenderer, 140);
						arrAllColonne.addItem(newColonne);
						break;

					case "col_6"://SIM
						newColonne=createColonne(listeColonnes[i] as Colonne, SimItemRenderer, 150);
						arrAllColonne.addItem(newColonne);
						break;

					case "col_7"://Ligne
						newColonne=createColonne(listeColonnes[i] as Colonne, LigneItemRenderer, 130);
						arrAllColonne.addItem(newColonne);
						break;

					case "col_8":
						newColonne=createColonne(listeColonnes[i] as Colonne, OperateurRenderer, 90);
						arrAllColonne.addItem(newColonne);
						break;
					
					case "col_9":
						newColonne=createColonne(listeColonnes[i] as Colonne, dateEntreeItemRenderer, 115);
						arrAllColonne.addItem(newColonne);
						break;
					
					case "col_10":
						newColonne=createColonne(listeColonnes[i] as Colonne, dateSortieItemRenderer, 115);
						arrAllColonne.addItem(newColonne);
						break;

					case "col_22"://BYOD
						newColonne = createColonne(listeColonnes[i] as Colonne, ByodRenderer, 65);
						arrAllColonne.addItem(newColonne);
						break;
					
					case "col_23"://Gestionnaire
						newColonne = createColonne(listeColonnes[i] as Colonne, null, 160);
						arrAllColonne.addItem(newColonne);
						break;
					
					case "col_24"://Status IMEI
						newColonne = createColonne(listeColonnes[i] as Colonne, StatusIMEIItemRenderer, 90, getLabelStatusFunc);
						arrAllColonne.addItem(newColonne);
						break;
					
					case "col_25"://Status SIM
						newColonne = createColonne(listeColonnes[i] as Colonne, StatusSIMItemRenderer, 90, getLabelStatusFunc);
						arrAllColonne.addItem(newColonne);
						break;
					
					case "col_26"://Status Ligne
						newColonne = createColonne(listeColonnes[i] as Colonne, StatusLigneItemRenderer, 90, getLabelStatusFunc);
						arrAllColonne.addItem(newColonne);
						break;
					
					case "col_32":// Opérateur Télécom
						newColonne=createColonne(listeColonnes[i] as Colonne, OperateurTelecomRenderer, 120);
						arrAllColonne.addItem(newColonne);
						break;
					
					default:
						newColonne=createColonne(listeColonnes[i] as Colonne, null, 120);
						arrAllColonne.addItem(newColonne);
						break;
				}
			}
			return arrAllColonne;
		}
		
		public function getLabelStatusFunc(rowItem:Object,column:DataGridColumn):String
		{
			var res:String = '';
			if(column.dataField == 'T_ID_STATUT')
				res = getStatus(rowItem.T_ID_STATUT);
			
			else if(column.dataField == 'S_ID_STATUT')
				res = getStatus(rowItem.S_ID_STATUT);
			
			else if (column.dataField == 'ST_IDETAT')
				res = getStatusLigne(rowItem.ST_IDETAT);
			
			return res;
		}
		
		private function getStatus(st:int):String{
			
			var res:String = '';
			switch (st)
			{
				case 1:
					 res= ResourceManager.getInstance().getString('M111','active_');
					break;
				
				case 2:
					res = ResourceManager.getInstance().getString('M111','pr_te__');
					break;
				
				case 3:
					res = ResourceManager.getInstance().getString('M111','_au_rebut');
					break;
				case 4:
					res = ResourceManager.getInstance().getString('M111','en_SAV___');
					break;
			}
			
			return res;
		}
		
		private function getStatusLigne(st:int):String{
			var res:String = '';
			switch (st)
			{
				case 1:
					res = ResourceManager.getInstance().getString('M111','active_');
					break;
				
				case 2:
					res = ResourceManager.getInstance().getString('M111','Suspendue');
					break;
				
				case 3:
					res = ResourceManager.getInstance().getString('M111','R_sili_');
					break;
			}			
			return res;
		}
		/**
		 * replace la colonne BYOD entre l'équipement et l'imei
		 */
		private function moveByodColumn(listeColonnes:ArrayCollection):ArrayCollection
		{
			var i:int = 0;
			while (i < listeColonnes.length)
			{
				if ((listeColonnes[i] as Colonne).CODE == 'col_22')//Si c'est la colonne du BYOD
				{
					var obj:Object = listeColonnes[i];//On sauvegarde la colonne;
					listeColonnes.removeItemAt(i);//On la supprime
					listeColonnes.addItemAt(obj, 3);//On la replace entre l'équipement et l'imei
					return listeColonnes;
				}
				i++;
			}
			return listeColonnes;
		}
		
		/**
		 * Mettre la colonne status après la colonne qu'elle qualifie: IMEI, Status IMEI
		 */
		private function setOrderStatusIMEI(listCol:ArrayCollection):void
		{
			var idxIMEI:int = -1;
			var idxStatusIMEI:int = -1;
			for (var i:int = 0; i < listCol.length; i++) 
			{
				if(listCol[i].CODE == 'col_4')
					idxIMEI = i;
				if(listCol[i].CODE == 'col_24')
					idxStatusIMEI = i;
			}
			//ordre: IMEI, Status IMEI
			var objIMEI:Object = listCol[idxStatusIMEI];
			listCol.removeItemAt(idxStatusIMEI);
			listCol.addItemAt(objIMEI,idxIMEI + 1);
		}
		
		/**
		 * Mettre la colonne status après la colonne qu'elle qualifie: SIM, Status SIM
		 */
		private function setOrderStatusSIM(listCol:ArrayCollection):void
		{
			var idxSIM:int = -1;
			var idxStatusSIM:int = -1;
			for (var i:int = 0; i < listCol.length; i++) 
			{
				if(listCol[i].CODE == 'col_6')
					idxSIM = i;
				if(listCol[i].CODE == 'col_25')
					idxStatusSIM = i;
				if(idxSIM != -1 && idxStatusSIM != -1)
					break;
			}
			var objStatus:Object = listCol[idxStatusSIM];
			listCol.removeItemAt(idxStatusSIM);
			listCol.addItemAt(objStatus,idxSIM + 1);
		}
		
		/**
		 * Mettre la colonne status après la colonne qu'elle qualifie: LIGNE, Status LIGNE
		 */
		private function setOrderStatusLigne(listCol:ArrayCollection):void
		{
			var idxLigne:int = -1;
			var idxStatusLigne:int = -1;
			for (var i:int = 0; i < listCol.length; i++) 
			{
				if(listCol[i].CODE == 'col_7')
					idxLigne = i;
				if(listCol[i].CODE == 'col_26')
					idxStatusLigne = i;
				if(idxLigne != -1 && idxStatusLigne != -1)
					break;
			}
			var objStatus:Object = listCol[idxStatusLigne];
			listCol.removeItemAt(idxStatusLigne);
			listCol.addItemAt(objStatus,idxLigne + 1);	
		}
		
		/**
		 * Mettre la colonne Pool à gauche de la colonne collaborateur
		 */
		private function setOrderPoolCollaborateur(listCol:ArrayCollection):void
		{
			var indx2:int = -1;
			for (var i:int = 0; i < listCol.length; i++) 
			{
				if(listCol[i].CODE == 'col_31')
					indx2 = i;
				if(indx2 != -1)// une fois trouver une valeur, ne continue pas à chercher
					break;
			}
			var objPool:Object = listCol[indx2];
			listCol.removeItemAt(indx2);
			
			listCol.addItemAt(objPool, 0);
		}

		/**
		 * 
		 */
		public function setColonne(value:ArrayCollection):void{
			
		}
			
		/**
		 * permet de mettre à jour un colonne en ajoutant LIBELLE,DATAFILED 
		 */		
		public function createColonne(colonne:Colonne,renderer:Class, w:int=-1, lblfunction:Function=null):Colonne
		{
			colonne.ITEMRENDERER=renderer;
			colonne.WIDTH=w;
			colonne.LABEL_FUNCTION=lblfunction;
			return colonne;
		}

		public function rechercher(indexDebut:int, nbItem:int):void
		{
		}

		public function get ISMIXTE():Boolean
		{
			return _ISMIXTE;
		}

		public function set ISMIXTE(value:Boolean):void
		{
			_ISMIXTE=value;
		}

		public function get LIBELLE():String
		{
			return _LIBELLE;
		}

		public function set LIBELLE(value:String):void
		{
			_LIBELLE=value;
		}

		public function get TYPE():String
		{
			return _TYPE;
		}

		public function set TYPE(value:String):void
		{
			_TYPE=value;
		}

		public function get IDVUE():int
		{
			return _IDVUE;
		}

		public function set IDVUE(value:int):void
		{
			_IDVUE=value;
		}

		public function get LISTE_COLONNE():ArrayCollection
		{
			return _LISTE_COLONNE;
		}

		public function set LISTE_COLONNE(value:ArrayCollection):void
		{
			_LISTE_COLONNE=value;
		}

		public function get ISVUEALERTE():Boolean
		{
			return _ISVUEALERTE;
		}

		public function set ISVUEALERTE(value:Boolean):void
		{
			_ISVUEALERTE=value;
		}

		public function get ISVUECONSOS():Boolean
		{
			return _ISVUECONSOS;
		}

		public function set ISVUECONSOS(value:Boolean):void
		{
			_ISVUECONSOS=value;
		}

		public function get active():Boolean
		{
			return _active;
		}

		public function set active(value:Boolean):void
		{
			_active=value;
		}

		public function get ISRESTRICTIONCONTRAT():Boolean
		{
			return _ISRESTRICTIONCONTRAT;
		}

		public function set ISRESTRICTIONCONTRAT(value:Boolean):void
		{
			_ISRESTRICTIONCONTRAT=value;
		}

		public function get ISRESTRICTIONCOLLABORATEUR():Boolean
		{
			return _ISRESTRICTIONCOLLABORATEUR;
		}

		public function set ISRESTRICTIONCOLLABORATEUR(value:Boolean):void
		{
			_ISRESTRICTIONCOLLABORATEUR=value;
		}

		public function get ISRESTRICTIONEQUIPEMENT():Boolean
		{
			return _ISRESTRICTIONEQUIPEMENT;
		}

		public function set ISRESTRICTIONEQUIPEMENT(value:Boolean):void
		{
			_ISRESTRICTIONEQUIPEMENT=value;
		}

		public function get ISRESTRICTIONSIM():Boolean
		{
			return _ISRESTRICTIONSIM;
		}

		public function set ISRESTRICTIONSIM(value:Boolean):void
		{
			_ISRESTRICTIONSIM=value;
		}

		public function get ISRESTRICTIONLIGNE():Boolean
		{
			return _ISRESTRICTIONLIGNE;
		}

		public function set ISRESTRICTIONLIGNE(value:Boolean):void
		{
			_ISRESTRICTIONLIGNE=value;
		}

		public function get LISTE_RESTRICTION():ArrayCollection
		{
			return _LISTE_RESTRICTION;
		}

		public function set LISTE_RESTRICTION(value:ArrayCollection):void
		{
			_LISTE_RESTRICTION=value;
		}
	}
}
