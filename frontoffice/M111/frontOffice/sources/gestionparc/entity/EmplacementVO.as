package gestionparc.entity
{
	/**
	 * Value Object représentant les propriétés d'un emplacement.
	 */
	[Bindable]
	public class EmplacementVO
	{
		public var idEmplcament:int=-1;
		public var libelleEmplacement:String="";

		public var batiment:String="";
		public var etage:String="";
		public var salle:String=""; // correspondant a la piece
		public var couloir:String="";
		public var armoire:String="";

		/**
		 * La fonction remplit un objet EmplacementVO avec les donnees passees en parametre et retourne cet objet
		 */
		public function fillEmplacementVO(obj:Object):EmplacementVO
		{
			var myEmplacementVO:EmplacementVO=new EmplacementVO();

			myEmplacementVO.libelleEmplacement=obj.libelle;
			myEmplacementVO.batiment=obj.batiment;
			myEmplacementVO.etage=obj.etage;
			myEmplacementVO.salle=obj.salle;
			myEmplacementVO.couloir=obj.couloir;
			myEmplacementVO.armoire=obj.armoire;

			return myEmplacementVO;
		}
	}
}
