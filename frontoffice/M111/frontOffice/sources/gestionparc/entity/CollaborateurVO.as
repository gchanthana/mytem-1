package gestionparc.entity
{
	import mx.collections.ArrayCollection;
	
	import gestionparc.services.cache.CacheDatas;

	[Bindable]
	public class CollaborateurVO
	{
		public var idGpeClient:int=0;
		public var idEmploye:int=0;
		public var nom:String="";
		public var prenom:String="";
		public var idCivilite:int=-1; // 0=Mr | 1=Mme | 2=Mlle | 99=""
		public var email:String="";
		public var matricule:String="";

		public var fonction:String="";
		public var niveau:int=-1;

		public var dateEntree:Date=null;
		public var dateSortie:Date=null;

		public var cleIdentifiant:String="";

		public var codeInterne:String="";

		public var dansSociete:int=-1; //0=

		public var statut:String="";

		public var reference:String="";

		public var listeOrga:ArrayCollection=null;
		public var positionOrga:String="";
		
		public var listeOrgaOPE:ArrayCollection=null;
		public var positionOrgaOPE:String="";

		public var commentaires:String="";

		public var idSite:int=-1;

		public var listeLiaison:ArrayCollection=null;
		public var listeTerm:ArrayCollection=null;
		public var listeLigneSim:ArrayCollection=null;

		public var libelleChampPerso1:String="";
		public var libelleChampPerso2:String="";
		public var libelleChampPerso3:String="";
		public var libelleChampPerso4:String="";

		public var texteChampPerso1:String="";
		public var texteChampPerso2:String="";
		public var texteChampPerso3:String="";
		public var texteChampPerso4:String="";

		public var dateCreation:Date=null;
		public var dateModification:Date=null;
		private var _SELECTED:Boolean = false;
		private var _idManager:int = 0;
		private var _nomManager:String = '';
		private var _prenomManager:String = '';
		private var _matriculeManager:String = '';
		private var _NBRECORD:Number;
		
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * La fonction remplit un objet CollaborateurVO avec les donnees passees en parametre et retourne cette CollaborateurVO
		 * </pre></p>
		 *
		 * @param obj
		 * @return CollaborateurVO
		 *
		 */
		public function fillCollaborateurVO(obj:Object):CollaborateurVO
		{
			var myCollabVO:CollaborateurVO=new CollaborateurVO();

			// Ajout des infos perso
			myCollabVO.idGpeClient=obj[1][0].IDGC_EMPLOYE;
			myCollabVO.idCivilite=obj[1][0].IDCIVILITE;
			myCollabVO.nom=obj[1][0].NOM;
			myCollabVO.prenom=obj[1][0].PRENOM;
			myCollabVO.fonction=obj[1][0].FONCTION_EMPLOYE;
			myCollabVO.niveau=obj[1][0].NIVEAU;
			myCollabVO.email=obj[1][0].EMAIL;
			myCollabVO.dateEntree=obj[1][0].DATE_ENTREE;
			myCollabVO.dateSortie=obj[1][0].DATE_SORTIE;
			myCollabVO.cleIdentifiant=obj[1][0].CLE_IDENTIFIANT;
			myCollabVO.codeInterne=obj[1][0].CODE_INTERNE;
			myCollabVO.matricule=obj[1][0].MATRICULE;
			myCollabVO.dansSociete=obj[1][0].INOUT;
			myCollabVO.statut=obj[1][0].STATUS_EMPLOYE;
			myCollabVO.reference=obj[1][0].REFERENCE_EMPLOYE;
			myCollabVO.commentaires=obj[1][0].COMMENTAIRE;
			
			myCollabVO.idManager=(obj[1][0].IDMANAGER != null)?obj[1][0].IDMANAGER : 0;
			myCollabVO.nomManager=(obj[1][0].NOM_MANAGER != null)?obj[1][0].NOM_MANAGER : '';
			myCollabVO.prenomManager= (obj[1][0].PRENOM_MANAGER != null)?obj[1][0].PRENOM_MANAGER : '';
			myCollabVO.matriculeManager=(obj[1][0].MATRICULE_MANAGER != null)?obj[1][0].MATRICULE_MANAGER : '';

			// Liste des organisations et position
			myCollabVO.listeOrga=new ArrayCollection();
			myCollabVO.listeOrgaOPE=new ArrayCollection();
			
			var longueurListeOrga:int=(obj[3] as ArrayCollection).length;
			var newOrga:OrgaVO;

			for (var index:int=0; index < longueurListeOrga; index++)
			{
				newOrga=new OrgaVO();
				newOrga.idOrga=obj[3][index].IDORGA;
				newOrga.libelleOrga=obj[3][index].LIBELLE_ORGA;
				newOrga.idCible=obj[3][index].IDCIBLE;
				newOrga.position=obj[3][index].POSITION;
				newOrga.chemin=obj[3][index].CHEMIN_CIBLE;
				newOrga.idRegleOrga=obj[3][index].IDREGLE;
				newOrga.dateModifPosition=obj[3][index].DATE_MODIF_POSITION;

				myCollabVO.listeOrga.addItem(newOrga);
			}
			
			longueurListeOrga=(obj[4] as ArrayCollection).length;
			for (var index1:int=0; index1 < longueurListeOrga; index1++)
			{
				newOrga=new OrgaVO();
				newOrga.idOrga=obj[4][index1].IDORGA;
				newOrga.libelleOrga=obj[4][index1].LIBELLE_ORGA;
				newOrga.idCible=obj[4][index1].IDCIBLE;
				newOrga.position=obj[4][index1].POSITION;
				newOrga.chemin=obj[4][index1].CHEMIN_CIBLE;
				newOrga.idRegleOrga=obj[4][index1].IDREGLE;
				newOrga.dateModifPosition=obj[4][index1].DATE_MODIF_POSITION;
				
				myCollabVO.listeOrgaOPE.addItem(newOrga);
			}

			// Ajout des sites
			myCollabVO.idSite=obj[1][0].IDSITE_PHYSIQUE;

			// Ajout des liaisons
			myCollabVO.listeLiaison=new ArrayCollection();
			var longueurTabLiaison:int=(obj[2] as ArrayCollection).length;
			var newLiaison:LiaisonVO;

			for (var i:int=0; i < longueurTabLiaison; i++)
			{
				newLiaison=new LiaisonVO();

				newLiaison.idRow=obj[2][i].ID;

				newLiaison.idTypeEquipement=obj[2][i].IDTYPE_EQUIPEMENT;
				newLiaison.typeEquipement=obj[2][i].TYPE_EQUIPEMENT;

				// SIM
				newLiaison.idSim=obj[2][i].IDSIM;
				newLiaison.idFabricantSim=obj[2][i].IDFABRICANT_SIM;
				newLiaison.idRevendeurSim=obj[2][i].IDREVENDEUR_SIM;
				newLiaison.revendeurSim=obj[2][i].REVENDEUR_SIM;
				newLiaison.numSim=obj[2][i].NUM_SIM;
				newLiaison.operateurId=obj[2][i].OPERATEURID;
				newLiaison.operateur=obj[2][i].OPERATEUR;
				newLiaison.modele=obj[2][i].MODELE;


				// TERMINAL
				newLiaison.idTerm=obj[2][i].IDTERM;
				newLiaison.idFabricantTerm=obj[2][i].IDFABRICANT_TERM;
				newLiaison.idRevendeurTerm=obj[2][i].IDREVENDEUR_TERM;
				newLiaison.revendeurTerm=obj[2][i].REVENDEUR_TERM;
				newLiaison.imei=obj[2][i].T_IMEI;
				newLiaison.marqueTerm=obj[2][i].MARQUE_TERM;
				newLiaison.modele=obj[2][i].MODELE;

				// LIGNE
				newLiaison.idLigne=obj[2][i].IDSOUS_TETE;
				newLiaison.numLigne=obj[2][i].SOUS_TETE;
				newLiaison.libelleTypeLigne=obj[2][i].IBELLE_TYPE_LIGNE;

				myCollabVO.listeLiaison.addItem(newLiaison);
			}

			// Ajout des Terminal et des LigneSim
			myCollabVO.listeTerm=new ArrayCollection();
			myCollabVO.listeLigneSim=new ArrayCollection();
			var newTerm:TerminalVO;
			var newSIM:SIMVO;

			for (var j:int=0; j < longueurTabLiaison; j++)
			{
				// Terminal
				newTerm=new TerminalVO();
				if ((myCollabVO.listeLiaison[j] as LiaisonVO).idTerm)
				{
					newTerm.idRow=(myCollabVO.listeLiaison[j] as LiaisonVO).idRow;
					newTerm.idEquipement=(myCollabVO.listeLiaison[j] as LiaisonVO).idTerm;
					newTerm.numIemiEquipement=(myCollabVO.listeLiaison[j] as LiaisonVO).imei;
					newTerm.idTypeEquipement=(myCollabVO.listeLiaison[j] as LiaisonVO).idTypeEquipement;
					newTerm.typeEquipement=(myCollabVO.listeLiaison[j] as LiaisonVO).typeEquipement;
					newTerm.libelleEquipement=(myCollabVO.listeLiaison[j] as LiaisonVO).modele;
					newTerm.nomFabricant=(myCollabVO.listeLiaison[j] as LiaisonVO).marqueTerm;
					newTerm.revendeur=new RevendeurVO();
					newTerm.revendeur.nom=(myCollabVO.listeLiaison[j] as LiaisonVO).revendeurTerm;
					newTerm.revendeur.idRevendeur=(myCollabVO.listeLiaison[j] as LiaisonVO).idRevendeurTerm;
					newTerm.nomRevendeur=(myCollabVO.listeLiaison[j] as LiaisonVO).revendeurTerm;

				}

				// LigneSim
				newSIM=new SIMVO();
				if ((myCollabVO.listeLiaison[j] as LiaisonVO).idSim)
				{
					newSIM.idRow=(myCollabVO.listeLiaison[j] as LiaisonVO).idRow;
					newSIM.idEquipement=(myCollabVO.listeLiaison[j] as LiaisonVO).idSim;
					newSIM.numSerieEquipement=(myCollabVO.listeLiaison[j] as LiaisonVO).numSim
					newSIM.ligneRattache=new LigneVO();
					newSIM.ligneRattache.idSousTete=(myCollabVO.listeLiaison[j] as LiaisonVO).idLigne;
					newSIM.ligneRattache.sousTete=(myCollabVO.listeLiaison[j] as LiaisonVO).numLigne;
					newSIM.ligneRattache.typeLigne=(myCollabVO.listeLiaison[j] as LiaisonVO).libelleTypeLigne;
					newSIM.numLigne=(myCollabVO.listeLiaison[j] as LiaisonVO).numLigne;
					newSIM.typeLigne=(myCollabVO.listeLiaison[j] as LiaisonVO).libelleTypeLigne;
					newSIM.revendeur=new RevendeurVO();
					newSIM.revendeur.nom=(myCollabVO.listeLiaison[j] as LiaisonVO).revendeurSim;
					newSIM.revendeur.idRevendeur=(myCollabVO.listeLiaison[j] as LiaisonVO).idRevendeurSim;
					newSIM.nomRevendeur=(myCollabVO.listeLiaison[j] as LiaisonVO).revendeurSim;
					newSIM.operateur=new OperateurVO();
					newSIM.operateur.idOperateur=(myCollabVO.listeLiaison[j] as LiaisonVO).operateurId;
					newSIM.operateur.nom=(myCollabVO.listeLiaison[j] as LiaisonVO).operateur;
					newSIM.nomOperateur=(myCollabVO.listeLiaison[j] as LiaisonVO).operateur;
				}

				// liaison entre Terminal et LigneSim
				if ((myCollabVO.listeLiaison[j] as LiaisonVO).idTerm && (myCollabVO.listeLiaison[j] as LiaisonVO).idSim)
				{
					newTerm.simRattache=newSIM;
					newTerm.numSim=newSIM.numSerieEquipement;
					newTerm.numSerieEquipement = newSIM.numSerieEquipement;
					
					newSIM.termRattache=newTerm;
					newSIM.imei=newTerm.numIemiEquipement;
					newSIM.numIemiEquipement = newTerm.numIemiEquipement;
				}

				if ((myCollabVO.listeLiaison[j] as LiaisonVO).idTerm)
				{
					myCollabVO.listeTerm.addItem(newTerm);
				}

				if ((myCollabVO.listeLiaison[j] as LiaisonVO).idSim)
				{
					myCollabVO.listeLigneSim.addItem(newSIM);
				}
			}

			// Ajout des libellés
			myCollabVO.libelleChampPerso1=CacheDatas.libellesPerso1;
			myCollabVO.libelleChampPerso2=CacheDatas.libellesPerso2;
			myCollabVO.libelleChampPerso3=CacheDatas.libellesPerso3;
			myCollabVO.libelleChampPerso4=CacheDatas.libellesPerso4;
			myCollabVO.texteChampPerso1=obj[1][0].C1;
			myCollabVO.texteChampPerso2=obj[1][0].C2;
			myCollabVO.texteChampPerso3=obj[1][0].C3;
			myCollabVO.texteChampPerso4=obj[1][0].C4;

			return myCollabVO;
		}

		/**
		 * La fonction met à jour la liste listeLigneSim avec les donnees passees en parametre.
		 */
		public function fillListLigneSimEqpt(objResult:Object):void
		{
			var objResultLen:int=(objResult as ArrayCollection).length;
			var sim:SIMVO=null;

			listeLigneSim=new ArrayCollection();

			for (var i:int; i < objResultLen; i++)
			{
				sim=new SIMVO();
				sim.ligneRattache=new LigneVO();
				sim.ligneRattache.idSousTete=(objResult as ArrayCollection)[i].IDSOUS_TETE;
				sim.typeLigne=(objResult as ArrayCollection)[i].LIBELLE_TYPE_LIGNE;
				sim.numLigne=(objResult as ArrayCollection)[i].SOUS_TETE;
				sim.numSerieEquipement=(objResult as ArrayCollection)[i].NUM_SIM;
				sim.imei=(objResult as ArrayCollection)[i].IMEI;
				sim.nomOperateur=(objResult as ArrayCollection)[i].OPERATEUR;
				sim.nomRevendeur=(objResult as ArrayCollection)[i].DISTRIBUTEUR;
				sim.idEquipement=(objResult as ArrayCollection)[i].IDSIM;

				listeLigneSim.addItem(sim);
			}
		}

		/**
		 * La fonction met à jour la liste listeTerm avec les donnees passees en parametre.
		 */
		public function fillListTerm(objResult:Object):void
		{
			var objResultLen:int=(objResult as ArrayCollection).length;
			var terminal:TerminalVO=null;

			listeTerm=new ArrayCollection();

			for (var i:int; i < objResultLen; i++)
			{
				terminal=new TerminalVO();
				terminal.typeEquipement=(objResult as ArrayCollection)[i].TYPE_EQUIPEMENT;
				terminal.nomFabricant=(objResult as ArrayCollection)[i].MARQUE;
				terminal.libelleEquipement=(objResult as ArrayCollection)[i].LIBELLE_MODELE;
				terminal.numSerieEquipement=(objResult as ArrayCollection)[i].IMEI;
				terminal.nomRevendeur=(objResult as ArrayCollection)[i].DISTRIBUTEUR;
				terminal.idEquipement=(objResult as ArrayCollection)[i].IDEQUIPEMENT;

				listeTerm.addItem(terminal);
			}
		}
		
		/**
		 * La fonction pour mapper les infos de manager.
		 */
		public function fillManager(manager:Object):void
		{
			this.idManager = manager.IDEMPLOYE;
			this.nomManager = (manager.NOM != null)?manager.NOM : '';
			this.prenomManager = (manager.PRENOM != null)?manager.PRENOM : '';
			this.matriculeManager = manager.MATRICULE;
			this.NBRECORD = manager.NB_ROWN
		}
		
		public function get SELECTED():Boolean { return _SELECTED; }
		
		public function set SELECTED(value:Boolean):void
		{
			if (_SELECTED == value)
				return;
			_SELECTED = value;
		}
		
		public function get matriculeManager():String { return _matriculeManager; }
		
		public function set matriculeManager(value:String):void
		{
			if (_matriculeManager == value)
				return;
			_matriculeManager = value;
		}
		
		public function get prenomManager():String { return _prenomManager; }
		
		public function set prenomManager(value:String):void
		{
			if (_prenomManager == value)
				return;
			_prenomManager = value;
		}
		
		public function get nomManager():String { return _nomManager; }
		
		public function set nomManager(value:String):void
		{
			if (_nomManager == value)
				return;
			_nomManager = value;
		}
		
		public function get idManager():int { return _idManager; }
		
		public function set idManager(value:int):void
		{
			if (_idManager == value)
				return;
			_idManager = value;
		}
		
		public function get NBRECORD():Number { return _NBRECORD; }
		
		public function set NBRECORD(value:Number):void
		{
			if (_NBRECORD == value)
				return;
			_NBRECORD = value;
		}
	}
}
