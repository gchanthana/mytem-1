package gestionparc.entity
{
	

	public class PoolVO
	{
		private var _LIBELLE_PROFIL :String ;
		private var _IDPROFIL : Number;
		private var _CODE_INTERNE_POOL :String;
		private var _IDREVENDEUR :Number;
		private var _COMMENTAIRE_POOL :String;
		private var _LIBELLE_POOL :String;
		private var _ACCES_COLLABORATEUR :String;
		private var _IDPOOL :Number;
		private var _SELECTED:Boolean;
		
		public function PoolVO()
		{
		}
		
		public function createObject(obj:Object):void
		{
			this._IDPOOL=obj.IDPOOL;
			this._LIBELLE_POOL=obj.LIBELLE_POOL;
			this._LIBELLE_PROFIL=obj.LIBELLE_PROFIL;
			this._IDPROFIL=obj.IDPROFIL;
			this._IDREVENDEUR=obj.IDREVENDEUR;
		}

		public function get IDPROFIL():Number
		{
			return _IDPROFIL;
		}

		public function set IDPROFIL(value:Number):void
		{
			_IDPROFIL = value;
		}

		public function get CODE_INTERNE_POOL():String
		{
			return _CODE_INTERNE_POOL;
		}

		public function set CODE_INTERNE_POOL(value:String):void
		{
			_CODE_INTERNE_POOL = value;
		}

		public function get IDREVENDEUR():Number
		{
			return _IDREVENDEUR;
		}

		public function set IDREVENDEUR(value:Number):void
		{
			_IDREVENDEUR = value;
		}

		public function get COMMENTAIRE_POOL():String
		{
			return _COMMENTAIRE_POOL;
		}

		public function set COMMENTAIRE_POOL(value:String):void
		{
			_COMMENTAIRE_POOL = value;
		}

		public function get LIBELLE_POOL():String
		{
			return _LIBELLE_POOL;
		}

		public function set LIBELLE_POOL(value:String):void
		{
			_LIBELLE_POOL = value;
		}

		public function get ACCES_COLLABORATEUR():String
		{
			return _ACCES_COLLABORATEUR;
		}

		public function set ACCES_COLLABORATEUR(value:String):void
		{
			_ACCES_COLLABORATEUR = value;
		}

		public function get IDPOOL():Number
		{
			return _IDPOOL;
		}

		public function set IDPOOL(value:Number):void
		{
			_IDPOOL = value;
		}
		
		public function get LIBELLE_PROFIL():String
		{
			return _LIBELLE_PROFIL;
		}
		
		public function set LIBELLE_PROFIL(value:String):void
		{
			_LIBELLE_PROFIL = value;
		}
		
		public function get SELECTED():Boolean { return _SELECTED; }
		
		public function set SELECTED(value:Boolean):void
		{
			if (_SELECTED == value)
				return;
			_SELECTED = value;
		}

	}
}
