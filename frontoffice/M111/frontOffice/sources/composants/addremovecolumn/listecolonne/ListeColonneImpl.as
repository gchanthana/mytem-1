package composants.addremovecolumn.listecolonne
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.entity.Colonne;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.services.setcolonnes.SetColonneService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.List;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;

	public class ListeColonneImpl extends VBox
	{
		[Bindable]
		public var heightComposant:int=550;
		[Bindable]
		public var listeItemSelected:ArrayCollection=new ArrayCollection;
		
		public var lstColumns:List;
		private var _serviceSetColonne:SetColonneService;
		private var _numColonneSelected:Number=0;
		
		public function ListeColonneImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			if (SpecificationVO.getInstance().ViewObject != null && SpecificationVO.getInstance().ViewObject.LISTE_COLONNE.length > 0)
			{
				//heightComposant=(25 * SpecificationVO.getInstance().ViewObject.LISTE_COLONNE.length) + 140; 
			}
			_serviceSetColonne=new SetColonneService();
		}

		/**
		 * masquer / afficher une colonne et sauvegarder les colonnes sélectionnées
		 */
		public function hideShowColumns():void
		{
			var actualColumns:Array=SpecificationVO.getInstance().dataGridView.columns; /** les colonnes de data grid */
			var allColonne:ArrayCollection=SpecificationVO.getInstance().ViewObject.LISTE_COLONNE; /** les VO des colonnes ( avec ces Vo , on crée les DataGridColumns) */
			var idOfCol:String=getSelectedId();
			
			if(_numColonneSelected==0) 
			{
				(lstColumns.selectedItem as Colonne).VISIBLE=true;
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_au_moins_une_colonne'), ResourceManager.getInstance().getString('M111', 'Alerte'), null);
				return;
			}
			
			if (_numColonneSelected > 0) // pour garder au moins une colonne 
			{
				for (var i:int=0; i < actualColumns.length - 1; i++)
				{
					if ((actualColumns[i + 1] as DataGridColumn).headerText == (allColonne[i] as Colonne).LIBELLE)
					{
						(actualColumns[i + 1] as DataGridColumn).visible=(allColonne[i] as Colonne).VISIBLE; // i+1 parce que 1 ème colonne de data gird reset tourjour affiché
					}
				}
			}
			//Patch de replacement de la colonne BYOD entre l'équipement et l'IMEI
			
			refreshDataGridColonne();
			_serviceSetColonne.setColonneVueParc(idOfCol); /** appeler le service pour sauvegarder les colonnes*/
		}

		private function refreshDataGridColonne():void
		{
			if (SpecificationVO.getInstance().dataGridView.dataProvider != null)
			{
				(SpecificationVO.getInstance().dataGridView.dataProvider as ArrayCollection).refresh();
			}
		}
		/**
		 * récupérer les id des colonnes selectionnées par l'utilisateur
		 */
		public function getSelectedId():String
		{
			var listeId:Array=[];
			var k:Number=0;
			var idOfCol:String="";
			var allColonne:ArrayCollection=new ArrayCollection();

			if (SpecificationVO.getInstance().ViewObject != null)
			{
				allColonne=SpecificationVO.getInstance().ViewObject.LISTE_COLONNE;
			}

			if (allColonne.length > 0)
			{
				for (var i:int=0; i < allColonne.length; i++)
				{
					if (allColonne.getItemAt(i).VISIBLE) //VISIBLE pour dire selected 
					{
						var idColonne:Number=(allColonne.getItemAt(i) as Colonne).ID;
						listeId[k]=idColonne;
						k++;
					}
				}

				this._numColonneSelected=k;
				
				if (listeId.length > 0)
				{
					idOfCol=listeId.toString();
				}
			}
			return idOfCol;
		}

		/**
		 * remettre les colonnes par défauts  
		 * 
		 */		
		public function defautColonne_clickHandler():void
		{
			var allColonne:ArrayCollection=new ArrayCollection();
			var k:Number=0;
			
			if (SpecificationVO.getInstance().ViewObject != null)
			{
				allColonne=SpecificationVO.getInstance().ViewObject.LISTE_COLONNE;
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_un_pool_de_gestion_pour'), ResourceManager.getInstance().getString('M111', 'Attention'));
			}
			
			if (allColonne.length > 0)
			{
				for (var i:int=0; i < allColonne.length; i++)
				{
					if(allColonne.getItemAt(i).DEFAUT==1)
					{
						 allColonne.getItemAt(i).VISIBLE=true
						 k++;
						 this._numColonneSelected=k;
					}
					else
					{
						allColonne.getItemAt(i).VISIBLE=false;
					}
				}
				hideShowColumns();
			}
		}
	}
}
