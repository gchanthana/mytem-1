package composants.util.searchPaginateDatagrid
{
	[Bindable]
	public class CriteresRechercheVO
	{	
		
		/**
		 * <b>Constructeur <code>CriteresRechercheVO()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe CriteresRechercheVO.
		 * </pre></p>
		 */
		public function CriteresRechercheVO()
		{	
		}
		
		public var searchColonne:String='Date appel';
		public var searchText:String='';
		public var orderColonne:String='Date appel';
		public var orderBy:String='ASC';
		public var offset:Number = 1;
		public var limit:Number = 100;
		
		/**
		 * <b>Fonction <code>getParameter()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Get de CriteresRechercheVO
		 * </pre></p>
		 *
		 */
		public function getParameter():Object
		{
			return this;
		}
	}
}