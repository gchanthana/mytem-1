package composants.importmasse
{
	import com.as3xls.xls.ExcelFile;
	import com.as3xls.xls.Sheet;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	
	import gestionparc.entity.ImportDeMasseVO;
	import gestionparc.utils.CustomDatagrid;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	/**
	 * <code>ImportMasseImpl</code> est la classe principale pour l'import de masse.
	 * Elle instancie les classes extérieures pour gérer tous ce qui permettra l'import de masse de valeurs dans un DataGrid.
	 * L'import de masse se fait par rapport à un fichier .xls ou un fichier .csv
	 * @author GUIOU Nicolas
	 */
	[Bindable]
	public class ImportMasseImpl extends Box
	{
		
		//---------------- VARIABLES ----------------//
		
		public var dgElement				:CustomDatagrid;
		
		public var btDownloadMatriceImport	:Button;
		public var btUploadMatriceImport	:Button;
		public var btAddRow					:Button;
	
		public var btClear					:Button;
		public var nbElement				:Label;
		public var imgDelete				:Image;
		
		public var myDataProvider			:ArrayCollection = new ArrayCollection();
		public var tabNomColAttendu			:ArrayCollection;
		public var tabDataFieldColAttendu	:ArrayCollection;
		public var arrayColonnesToImport	:Array;
		
		public var _popUpUpload					:ImportDeMasseUpload;
		public var myImportMasseUploadDownload	:ImportMasseUploadDownload;
		public var myImportMasseService			:ImportMasseService;
		public var myImportMasseUtils			:ImportMasseUtils;
		
		private var _collab_isChecked		:Boolean;
		private var _imei_isChecked			:Boolean;
		private var _sim_isChecked			:Boolean;
		private var _extension_isChecked	:Boolean;
		private var _ligne_isChecked		:Boolean;
		private var _garantie_isChecked		:Boolean;
		private var _engagement_isAvailable	:Boolean;
		
		private var _uuid				:String = "";
//		private var _fileName			:String = "";
		private var _textNbElement		:String;
		private var _urlDownload		:String;
		private var _urlUpload			:String;
		private var _basePath			:String;
//		private var _isMobile			:int;
//		private var _isFixe				:int = 0;
		private var _qteToImport		:int = 0;
		private var _isEnabledBtnImport	:Boolean = false;
		private var _isEnabledBtnAddRow :Boolean = false;
		private var _fileRef			:FileReference;
		
		private var _arrayNomColonnes	:Array = [
			ResourceManager.getInstance().getString('M111', 'Matricule'), 
			ResourceManager.getInstance().getString('M111', 'Pr_nom'),
			ResourceManager.getInstance().getString('M111', 'Nom'),
			ResourceManager.getInstance().getString('M111', 'IMEI__'),
			ResourceManager.getInstance().getString('M111', 'S_rie'),
			ResourceManager.getInstance().getString('M111', 'SIM'),
			ResourceManager.getInstance().getString('M111', 'Extension'),
			ResourceManager.getInstance().getString('M111', 'Ligne')
		];
		private var _arrayDataFieldColonnes	:Array = [
			"MATRICULE",
			"PRENOM",
			"NOM",
			"IMEI",
			"NUMSERIE",
			"NUMSIM",
			"NUMEXT",
			"NUMLIGNE"
		];
		
		
		//--------------- METHODES ----------------//

		/**
		 * Appelle <code>init()</code> lorsque l'IHM a été créée.
		 */
		public function ImportMasseImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		/**
		 * Initialise les instances de classe <code>ImportMasseUploadDownload</code>, <code>ImportMasseService</code> et <code>ImportMasseUtils</code>.
		 * Initialise le DataGrid.
		 */
		private function init(e:FlexEvent):void
		{
			myImportMasseUploadDownload	= new ImportMasseUploadDownload(this);
			myImportMasseService		= new ImportMasseService(this);
			myImportMasseUtils			= new ImportMasseUtils(this);
			
			dgElement.initialize();
			initListener();
		}
		
		/**
		 * Initialise tous les listeners.
		 */
		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);
			
			btClear.addEventListener(MouseEvent.CLICK, btClearHandler);
			btDownloadMatriceImport.addEventListener(MouseEvent.CLICK, btDownloadMatriceImportClickHandler);
			btUploadMatriceImport.addEventListener(MouseEvent.CLICK, btUploadMatriceImportClickHandler);
			btAddRow.addEventListener(MouseEvent.CLICK, btAddRowClickHandler);
			
			dgElement.addEventListener(KeyboardEvent.KEY_DOWN, myImportMasseUtils.dataGridKeyDownHandler);
		}

		/**
		 * Ajoutes toutes les DataGridColumn contenu dans le tableau passé en paramètre.
		 * @param myColumns:<code>Array</code> contenant toutes les DataGridColumn du DataGrid.
		 */
		
		public function addColumnInDG(myColumns:Array):void
		{
			var dgColumns:Array = new Array();
			
			for each (var obj:Object in myColumns)
			{
				dgColumns.unshift(obj);
			}
			
			dgElement.columns = dgColumns;
			
			initTabNomColAttendu(myColumns);
		}
		
		/* */
		private function getArrayFileUploded():void
		{
			var fileXLS:ExcelFile = new ExcelFile();
			fileXLS.loadFromByteArray(_popUpUpload.fileUploaded);
			
			var sheet:Sheet = fileXLS.sheets[0]; //1ere feuille
			
			if (myImportMasseUtils.checkHeader(sheet.values[0])) //ligne des intitulés de la matrice importée
				myImportMasseUtils.fillDG(sheet.values);
		}
		
		/**
		 * Initialise le tableau "tabNomColAttendu" avec le nom des colonnes
		 * @param myColumns:<code>Array</code> contenant toutes les DataGridColumn du DataGrid.
		 */
		private function initTabNomColAttendu(myColumns:Array):void
		{
			var myNames		:Array = new Array();
			var myDataField	:Array = new Array();
			var longueur	:int = myColumns.length;
			
			for (var i:int=1; i<longueur; i++)
			{
				myNames.unshift(myColumns[i].headerText);
				myDataField.unshift(myColumns[i].dataField);
			}
			tabNomColAttendu		= new ArrayCollection(myNames);
			tabDataFieldColAttendu	= new ArrayCollection(myDataField);
		}
		
		/**
		 * Met à jour le libellé indiquand le nombre d'élément.
		 * @return Boolean indiquant si tous les champs sont bien valides.
		 */
		public function updateNbElementText():void
		{
			if (myDataProvider == null)
				nbElement.text="0 " + textNbElement;
			else
				nbElement.text=myDataProvider.length + " " + textNbElement;
		}

		/**
		 * Retourne toutes les valeurs du DataGrid sous forme XML.
		 * @return XML contenant tous les valeurs du DataGrid.
		 */
		public function getResultXML():XML
		{
			var resultXML:XML=<elements></elements>;
			var myString:String="";
			var longueur:int=tabNomColAttendu.length;
			var nameNode:String="";
			var myPatternSpace:RegExp=/ /gi;
			var myPatternA:RegExp=/[àâä]/gi;
			var myPatternC:RegExp=/[ç]/gi;
			var myPatternE:RegExp=/[éèêë]/gi;
			var myPatternI:RegExp=/[îï]/gi;
			var myPatternO:RegExp=/[ôö]/gi;
			var myPatternU:RegExp=/[ûüù]/gi;

			for each (var item:Object in myDataProvider)
			{
				myString="<element>";
				for (var index:int=0; index < longueur; index++)
				{
					nameNode=tabNomColAttendu[index];
					nameNode=nameNode.toLowerCase();
					nameNode=nameNode.replace(myPatternSpace, "_");
					nameNode=nameNode.replace(myPatternA, "a");
					nameNode=nameNode.replace(myPatternC, "c");
					nameNode=nameNode.replace(myPatternE, "e");
					nameNode=nameNode.replace(myPatternI, "i");
					nameNode=nameNode.replace(myPatternO, "o");
					nameNode=nameNode.replace(myPatternU, "u");

					myString+="<" + nameNode + ">";
					if (item[tabNomColAttendu[index]])
					{
						myString+=item[tabNomColAttendu[index]];
					}
					myString+="</" + nameNode + ">";
				}
				myString+="</element>";
				var myChild:Object=myString;
				resultXML.appendChild(myChild);
			}
			return resultXML;
		}

		/* */
		protected function btUploadMatriceImportClickHandler(me:MouseEvent):void
		{
			if(isEnabledBtnImport)
			{
				arrayColonnesToImport = new Array(
					{MATRICULE:collab_isChecked},
					{PRENOM:collab_isChecked},
					{NOM:collab_isChecked},
					{IMEI:imei_isChecked},
					{NUMSERIE:imei_isChecked},
					{NUMSIM:sim_isChecked},
					{NUMEXT:extension_isChecked},
					{NUMLIGNE:ligne_isChecked}
				);
				
//				_uuid='';
				_popUpUpload=new ImportDeMasseUpload();
				_popUpUpload.urlMatriceUpload=urlUpload;
				
				_popUpUpload.addEventListener('POPUP_CLOSED', popUpClosedHandler);
				_popUpUpload.addEventListener('POPUP_CLOSED_UPLOADED', popUpClosedHandler);
				
				PopUpManager.addPopUp(_popUpUpload,parent, true);
				PopUpManager.centerPopUp(_popUpUpload);
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_au_moins_type_éléments___importer_'), ResourceManager.getInstance().getString('M111', 'Erreur'));
		}
		
		/* */
		private function popUpClosedHandler(e:Event):void
		{
			if (e.type == 'POPUP_CLOSED_UPLOADED')
			{
				getArrayFileUploded();
//				this.myImportMasseService.renameFile();
			}
		}
		
		/**
		 * Appelle <code>closePopup()</code> pour fermer la popup.
		 * @param evt:<code>MouseEvent</code>.
		 * @return void.
		 */
		private function cancelPopup(me:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * Fermer la popup grâce à la méthode static <code>removePopUp</code> de la classe <code>PopUpManager</code>.
		 */
		private function closePopup(ce:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * Supprime tous les items qui étaient dans le dataProvider du DataGrid.
		 * Appelle la méthode <code>updateNbElementText()</code> pour mettre à jour le label indiquant le nombre de commande identique.
		 */
		private function btClearHandler(e:Event):void
		{
			myDataProvider.removeAll();
			updateNbElementText();
			
			btClear.dispatchEvent(new Event("DISABLE_BTN_VALID",true));
		}
		
		/* */
		private function btDownloadMatriceImportClickHandler(e:Event):void
		{
			_fileRef = new FileReference();
			_fileRef.download(new URLRequest(this.urlDownload));
		}
		
		/**
		 * Supprime la ligne séléctionnée du DataGrid
		 */
		public function imgDeleteClickHandler():void
		{
			var index:int = dgElement.selectedIndex;
			var item:Object = dgElement.selectedItem;
			
			if (index != -1)
			{
				myDataProvider.removeItemAt(index);
				updateNbElementText();
				myImportMasseUtils.checkDoublonsForSingleValue(myDataProvider,item as ImportDeMasseVO);
			}
			
			if(myDataProvider.length < 1)
				dispatchEvent(new Event("IS_ROW_DG_LEFT", true));
		}
		
		/**
		 * Ajoute un nouvel item au dataProvider du DataGrid.
		 * Appelle la méthode <code>updateNbElementText()</code> pour mettre à jour le label indiquant le nombre de commande identique.
		 */
		private function btAddRowClickHandler(e:Event):void
		{
			var test:Boolean = true;
			
			if (_isEnabledBtnAddRow)
			{								
				if ((myDataProvider.length + 1) <= 100)
				{
					var imp:ImportDeMasseVO = new ImportDeMasseVO();
					
					imp.COLLAB_TO_IMPORT = collab_isChecked;
					if (collab_isChecked) imp.ETAT_COLLAB = 9;
					
					imp.IMEI_TO_IMPORT = imei_isChecked;
					if (imei_isChecked)
					{
						if (this.parentDocument.comboModele.dataProvider.length > 0)
						{
							imp.ETAT_IMEI = 9;
							imp.ETAT_NUMSERIE = 9;
						}
						else
						{
							ConsoviewAlert.afficherError("Il n'existe pas de modèle lié à ce fournisseur.", "Erreur");
							return;
						}
					}
					
					imp.GARANTIE_AVAILABLE = garantie_isChecked;
					
					imp.NUMEXT_TO_IMPORT = extension_isChecked;
					if (extension_isChecked) imp.ETAT_NUMEXT = 9;
					
					imp.NUMSIM_TO_IMPORT = sim_isChecked;
					if (sim_isChecked) imp.ETAT_NUMSIM = 9;
					
					imp.NUMLIGNE_TO_IMPORT = ligne_isChecked;
					if (ligne_isChecked) imp.ETAT_NUMLIGNE = 9;
				
					myDataProvider.addItemAt(imp, 0);
					dgElement.selectedIndex=-1;
					updateNbElementText();
						
				}
				else if ((myDataProvider.length + 1) > 100)
				{
					Alert.show(ResourceManager.getInstance().getString('M111', 'Vous_ne_pouvez_importer_que_100__l_ments_maximum___la_fois_'), ResourceManager.getInstance().getString('M111', 'Erreur'));
				}
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_au_moins_type_éléments___importer_'), ResourceManager.getInstance().getString('M111', 'Erreur'));
			}
		}
		
		/**
		 * Attribut une valeur à la variable privée _textNbElement.
		 */
		public function set textNbElement(value:String):void
		{
			_textNbElement=value;
		}
		
		/**
		 * Retourne la valeur de la variable privée _textNbElement.
		 * @return String qui représente la variable privée _textNbElement.
		 */
		public function get textNbElement():String
		{
			return _textNbElement;
		}
		
		/**
		 * Attribut une valeur à la variable privée _urlDownload.
		 */
		public function set urlDownload(value:String):void
		{
			_urlDownload=value;
		}
		
		/**
		 * Retourne la valeur de la variable privée _urlDownload.
		 */
		public function get urlDownload():String
		{
			return _urlDownload;
		}
		
		/**
		 * Attribut une valeur à la variable privée _urlUpload.
		 */
		public function set urlUpload(value:String):void
		{
			_urlUpload=value;
		}
		
		/**
		 * Retourne la valeur de la variable privée _urlUpload.
		 */
		public function get urlUpload():String
		{
			return _urlUpload;
		}
		
		/**
		 * Attribut une valeur à la variable privée _basePath.
		 */
		public function set basePath(value:String):void
		{
			_basePath=value;
		}
		
		/**
		 * Retourne la valeur de la variable privée _basePath.
		 * @return String qui représente la variable privée _basePath.
		 */
		public function get basePath():String
		{
			return _basePath;
		}
		
//		public function get isMobile():int
//		{
//			return _isMobile;
//		}
//		
//		public function set isMobile(value:int):void
//		{
//			_isMobile=value;
//		}
		
		public function get isEnabledBtnImport():Boolean
		{
			return _isEnabledBtnImport;
		}

		public function set isEnabledBtnImport(value:Boolean):void
		{
			_isEnabledBtnImport = value;
			_isEnabledBtnAddRow = value;
		}

		public function get imei_isChecked():Boolean
		{
			return _imei_isChecked;
		}

		public function set imei_isChecked(value:Boolean):void
		{
			_imei_isChecked = value;
		}

		public function get collab_isChecked():Boolean
		{
			return _collab_isChecked;
		}

		public function set collab_isChecked(value:Boolean):void
		{
			_collab_isChecked = value;
		}

		public function get sim_isChecked():Boolean
		{
			return _sim_isChecked;
		}

		public function set sim_isChecked(value:Boolean):void
		{
			_sim_isChecked = value;
		}

		public function get extension_isChecked():Boolean
		{
			return _extension_isChecked;
		}

		public function set extension_isChecked(value:Boolean):void
		{
			_extension_isChecked = value;
		}

		public function get ligne_isChecked():Boolean
		{
			return _ligne_isChecked;
		}

		public function set ligne_isChecked(value:Boolean):void
		{
			_ligne_isChecked = value;
		}
		
		public function get garantie_isChecked():Boolean
		{
			return _garantie_isChecked;
		}
		
		public function set garantie_isChecked(value:Boolean):void
		{
			_garantie_isChecked = value;
		}
		
		public function get engagement_isAvailable():Boolean
		{
			return _engagement_isAvailable;
		}
		
		public function set engagement_isAvailable(value:Boolean):void
		{
			_engagement_isAvailable = value;
		}
		
		public function get qteToImport():int
		{
			return _qteToImport;
		}

		public function set qteToImport(value:int):void
		{
			_qteToImport = value;
		}

		public function get arrayNomColonnes():Array
		{
			return _arrayNomColonnes;
		}

		public function set arrayNomColonnes(value:Array):void
		{
			_arrayNomColonnes = value;
		}

		public function get arrayDataFieldColonnes():Array
		{
			return _arrayDataFieldColonnes;
		}

		public function set arrayDataFieldColonnes(value:Array):void
		{
			_arrayDataFieldColonnes = value;
		}
	}
}
