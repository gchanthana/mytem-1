package composants.importmasse
{
	import mx.resources.ResourceManager;

	public class ImportMasseText
	{

		public static var myTextAide:String="<PRE>" + ResourceManager.getInstance().getString('M111', '_br_Le_principe_des_imports_de_masse_est') +
													  ResourceManager.getInstance().getString('M111', 'Les_caract_ristiques_communes_sont___br_') +
													  ResourceManager.getInstance().getString('M111', '___________Dans_le_cas_de_t_l_phone___br') + 
													  ResourceManager.getInstance().getString('M111', '___________o_____Distributeur__Marque__M') +
													  ResourceManager.getInstance().getString('M111', '___________Dans_le_cas_de_Lignes_mobiles') + 
													  ResourceManager.getInstance().getString('M111', '___________o_____Op_rateur__date_de_d_bu') + "<br>" + 
													  ResourceManager.getInstance().getString('M111', 'L_import_de_masse_suppose_un_soin_partic') + 
													  ResourceManager.getInstance().getString('M111', 'Les_r_gles_sont_les_suivantes__le_non_re') + "<br>" + 
													  ResourceManager.getInstance().getString('M111', '___________La_casse__majuscule__minuscule') + 
													  ResourceManager.getInstance().getString('M111', '___________Un_fichier_d_import_ne_peut_c') + 
													  ResourceManager.getInstance().getString('M111', '___________Une_carte_SIM_seule__sans_Lig') + 
													  ResourceManager.getInstance().getString('M111', '___________ne_peut__tre_associ____un_ter') + 
													  ResourceManager.getInstance().getString('M111', '___________Pas_de_doublons_possible_pour') + 
													  ResourceManager.getInstance().getString('M111', '___________o_____Les__l_ments_d_j__exist') + 
													  ResourceManager.getInstance().getString('M111', '_________________des_nouveaux__l_ments_a') + 
													  ResourceManager.getInstance().getString('M111', '___________IMEI_doit_comporter_au_moins_') + 
													  ResourceManager.getInstance().getString('M111', '___________Ligne_commence_par_06_ou_07__') + 
													  ResourceManager.getInstance().getString('M111', '___________Un_nom_ou_un_pr_nom_doit_touj') + 
													  ResourceManager.getInstance().getString('M111', '___________Si_un_matricule_est_associ___') + 
													  ResourceManager.getInstance().getString('M111', '___________il_doit_toujours__tre_associ_') + 
													  ResourceManager.getInstance().getString('M111', '___________Si_une_SIM_du_fichier_d_impor') + 
													  ResourceManager.getInstance().getString('M111', '___________une_Ligne_nouvelle__ou_exista') + 
													  ResourceManager.getInstance().getString('M111', '___________Si_une_Ligne__06___du_fichier') + 
													  ResourceManager.getInstance().getString('M111', '___________une_SIM_nouvelle__ou_existant') + "<br>" + "<br>" + "</PRE>";

		public static var myTextImporter:String="<PRE>" + ResourceManager.getInstance().getString('M111', '_br_La_fonction_de_chargement_des_donn_e') + "<br>" +
														  ResourceManager.getInstance().getString('M111', '_br_Pour_les_autres_navigateurs_il_faut_') + "<br>" + 
														  ResourceManager.getInstance().getString('M111', '_br_Pour_importer_des__l_ments_depuis_un') + "<br>" + "<br>" + 
														  ResourceManager.getInstance().getString('M111', '___________Dans_Excel__s_lectionnez_tout') + 
														  ResourceManager.getInstance().getString('M111', '____________Clique_droit_puis_copier_ou_') + "<br>" + "<br>" + 
														  ResourceManager.getInstance().getString('M111', '___________Dans_Consoview__cliquez_sur_l') + 
														  ResourceManager.getInstance().getString('M111', '____________Pour_coller_dans_Consoview_u') + "<br>" + "<br>" + 
														  ResourceManager.getInstance().getString('M111', '_B_Attention____B__br_') + 
														  ResourceManager.getInstance().getString('M111', 'Lorsque_vous_s_lectionnez_des__l_ments_d') + "<br>" + "<br>" + "<br>" + "</PRE>";

	}
}
