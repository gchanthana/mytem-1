package composants.importmasse
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.KeyboardEvent;
	import flash.events.TextEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	import gestionparc.entity.ImportDeMasseVO;
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataIMEI;
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataNumExtension;
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataNumLigne;
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataNumSIM;
	import gestionparc.ihm.fiches.importmasse.checkingEtatDataClass.CheckingEtatDataNumSerie;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.resources.ResourceManager;
	

	/**
	 * <code>ImportMasseUtils</code> est la classe qui gère les méthodes utiles et les constantes pour l'import de masse.
	 * Elle instancie une référence unique de la classe <code>ImportMasseImpl</code>.
	 * Grâce à cette instance elle peut appeller toutes les méthodes qui lui seront utiles dans les classes <code>ImportMasseService</code> et <code>ImportMasseUploadDownload</code>.
	 *
	 * @author GUIOU Nicolas
	 * @version 1.0 (02-03-2010)
	 */
	[Bindable]
	public class ImportMasseUtils
	{

		public var LIGNE_IS_MOBILE:Boolean;
		///////////////////////////////////////////////////////////
		/////////////		VARIABLES PRIVEES     /////////////////
		///////////////////////////////////////////////////////////

		private var _myImportMasse:ImportMasseImpl;

		/**
		 * <b>Constructeur <code>ImportMasseUtils(instanceMyImportMasse:ImportMasseImpl)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Instancie la variable privée myImportMasse pour avoir une référence unique de l'objet ImportMasseImpl.
		 * </pre>
		 * </p>
		 *
		 * @param instanceMyImportMasse:<code>ImportMasseImpl</code>.
		 *
		 */
		public function ImportMasseUtils(instanceMyImportMasse:ImportMasseImpl)
		{
			myImportMasse = instanceMyImportMasse;
		}


		///////////////////////////////////////////////////////////
		/////////////	   FONCTIONS PUBLIQUES    /////////////////
		///////////////////////////////////////////////////////////

		/**
		 * <b>Fonction <code>fillDG(myArray:Array)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Remplis le DataGrid avec les valeurs du fichier importé. Ces valeurs sont passées en paramètre dans un Array.
		 * </pre>
		 * </p>
		 *
		 * @param myArray:<code>Array</code>.
		 *
		 * @return void.
		 *
		 */
		public function fillDG(myAC:ArrayCollection):void
		{
			var newItem		:Object;
			var objImp		:ImportDeMasseVO;
			var longueur	:int = myAC.length-1;
			var longueur2	:int = myImportMasse.arrayNomColonnes.length;
			var newArrayToAttach:ArrayCollection = new ArrayCollection();
			
			if (longueur <= 101) //ligne des noms de colonnes + 100 enregistrements max
			{
				if(longueur + myImportMasse.myDataProvider.length <= 101) //ligne des noms de colonnes + 100 enregistrements max
				{
					for (var index:int=1; index<longueur; index++)
					{
						newItem = new Object();
						objImp = new ImportDeMasseVO();
						for (var index2:int=0; index2<longueur2; index2++)
						{
							if(myImportMasse.arrayColonnesToImport[index2][myImportMasse.arrayDataFieldColonnes[index2]])
							{
								newItem[myImportMasse.arrayDataFieldColonnes[index2]] = myAC[index][index2]['value'];
							}
						}
						objImp.LIGNE_IS_MOBILE = this.LIGNE_IS_MOBILE;
						objImp.fillVO(newItem);
						newArrayToAttach.addItem(objImp);
					}
					myImportMasse.myDataProvider.addAllAt(newArrayToAttach,0);
					
					myImportMasse.updateNbElementText();
					
					checkDoublonsForMultipleValue(myImportMasse.myDataProvider);
				}
				else
				{
					Alert.show(ResourceManager.getInstance().getString('M111','Vous_allez_d_passer_le_nombre_maximum_d__l_ments_autoris_'), ResourceManager.getInstance().getString('M111','Erreur'));
				}
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M111','Le_fichier_est_trop_lourd__il_contient_p'), ResourceManager.getInstance().getString('M111','Erreur'));
		}
		
		/* verifier tous les doublons dans le DataGrid */
		public function checkDoublonsForMultipleValue(myAC:ArrayCollection):void
		{
			for(var i:int=0;i<myAC.length;i++)
			{
				for(var j:int=i+1;j<myAC.length;j++)
				{
					if(myAC[i]['IMEI']!="" && myAC[i]['IMEI']==myAC[j]['IMEI'])
					{
						myAC[i]['ETAT_IMEI']=4;
						myAC[j]['ETAT_IMEI']=4;
						myAC[i]['DOUBLON_IMEI']=true;
						myAC[j]['DOUBLON_IMEI']=true;
					}
					if(myAC[i]['NUMEXT']!="" && myAC[i]['NUMEXT']==myAC[j]['NUMEXT'])
					{
						myAC[i]['ETAT_NUMEXT']=4;
						myAC[j]['ETAT_NUMEXT']=4;
						myAC[i]['DOUBLON_NUMEXT']=true;
						myAC[j]['DOUBLON_NUMEXT']=true;
					}
					if(myAC[i]['NUMSERIE']!="" && myAC[i]['NUMSERIE']==myAC[j]['NUMSERIE'])
					{
						myAC[i]['ETAT_NUMSERIE']=4;
						myAC[j]['ETAT_NUMSERIE']=4;
						myAC[i]['DOUBLON_NUMSERIE']=true;
						myAC[j]['DOUBLON_NUMSERIE']=true;
					}
					if(myAC[i]['NUMSIM']!="" && myAC[i]['NUMSIM']==myAC[j]['NUMSIM'])
					{
						myAC[i]['ETAT_NUMSIM']=4;
						myAC[j]['ETAT_NUMSIM']=4;
						myAC[i]['DOUBLON_NUMSIM']=true;
						myAC[j]['DOUBLON_NUMSIM']=true;
					}
					if(myAC[i]['NUMLIGNE']!="" && myAC[i]['NUMLIGNE']==myAC[j]['NUMLIGNE'])
					{
						myAC[i]['ETAT_NUMLIGNE']=4;
						myAC[j]['ETAT_NUMLIGNE']=4;
						myAC[i]['DOUBLON_NUMLIGNE']=true;
						myAC[j]['DOUBLON_NUMLIGNE']=true;
					}
				}
			}
		}
		
		/* verifier les doublons d'une valeur en particulier */
		public function checkDoublonsForSingleValue(myAC:ArrayCollection,item:ImportDeMasseVO):void
		{
			var objCheckIMEI:CheckingEtatDataIMEI;
			var objCheckNumSerie:CheckingEtatDataNumSerie;
			var objCheckNumSIM:CheckingEtatDataNumSIM;
			var objCheckNumExt:CheckingEtatDataNumExtension;
			var objCheckNumLigne:CheckingEtatDataNumLigne;
			
			var cptDoublonIMEI:int=0;
			var cptDoublonNUMSERIE:int=0;
			var cptDoublonNUMEXT:int=0;
			var cptDoublonNUMSIM:int=0;
			var cptDoublonNUMLIGNE:int=0;
			
			var itemDoublonIMEI:ImportDeMasseVO = new ImportDeMasseVO();
			var itemDoublonNUMSERIE:ImportDeMasseVO = new ImportDeMasseVO();
			var itemDoublonNUMEXT:ImportDeMasseVO = new ImportDeMasseVO();
			var itemDoublonNUMSIM:ImportDeMasseVO = new ImportDeMasseVO();
			var itemDoublonNUMLIGNE:ImportDeMasseVO = new ImportDeMasseVO();
			
			for(var i:int=0; i<myAC.length; i++)
			{
				if(item['IMEI']!="" && item['IMEI']==myAC[i]['IMEI'])
				{
					itemDoublonIMEI=myAC[i];
					cptDoublonIMEI++;
				}
				if(item['NUMEXT']!="" && item['NUMEXT']==myAC[i]['NUMEXT'])
				{
					itemDoublonNUMEXT=myAC[i];
					cptDoublonNUMEXT++;
				}
				if(item['NUMSERIE']!="" && item['NUMSERIE']==myAC[i]['NUMSERIE'])
				{
					itemDoublonNUMSERIE=myAC[i];
					cptDoublonNUMSERIE++;
				}
				if(item['NUMSIM']!="" && item['NUMSIM']==myAC[i]['NUMSIM'])
				{
					itemDoublonNUMSIM=myAC[i];
					cptDoublonNUMSIM++;
				}
				if(item['NUMLIGNE']!="" && item['NUMLIGNE']==myAC[i]['NUMLIGNE'])
				{
					itemDoublonNUMLIGNE=myAC[i];
					cptDoublonNUMLIGNE++;
				}
			}
			if(cptDoublonIMEI==1)
			{
				itemDoublonIMEI.DOUBLON_IMEI=false;
				objCheckIMEI = new CheckingEtatDataIMEI(null);
				objCheckIMEI.checkEtatData(itemDoublonIMEI);
			}
			if(cptDoublonNUMSERIE==1)
			{
				itemDoublonNUMSERIE.DOUBLON_NUMSERIE=false;
				objCheckNumSerie = new CheckingEtatDataNumSerie(null);
				objCheckNumSerie.checkEtatData(itemDoublonNUMSERIE);
			}
			if(cptDoublonNUMEXT==1)
			{
				itemDoublonNUMEXT.DOUBLON_NUMEXT=false;
				objCheckNumExt = new CheckingEtatDataNumExtension(null);
				objCheckNumExt.checkEtatData(itemDoublonNUMEXT);
			}
			if(cptDoublonNUMSIM==1)
			{
				itemDoublonNUMSIM.DOUBLON_NUMSIM=false;
				objCheckNumSIM = new CheckingEtatDataNumSIM(null);
				objCheckNumSIM.checkEtatData(itemDoublonNUMSIM);
			}
			if(cptDoublonNUMLIGNE==1)
			{
				itemDoublonNUMLIGNE.DOUBLON_NUMLIGNE=false;
				objCheckNumLigne = new CheckingEtatDataNumLigne(null);
				objCheckNumLigne.checkEtatData(itemDoublonNUMLIGNE);
			}
		}
		
		/* */
//		public function isColumnsFilled(nameCol:String):Boolean
//		{
//			var dgColums:Array = myImportMasse.arrayColonnesToImport; //myImportMasse.dgElement.columns;
//			
//			for(var i:int=0; i<dgColums.length; i++)
//			{
//				if(dgColums[i]==nameCol && dgColums[i][nameCol]==true)
//					return true
//			}
//			return false;
//		}		
		
		/**
		 * <b>Fonction <code>checkHeader(myObj:Object)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Vérifie si le fichier importé contient bien les mêmes en-têtes que la DataGrid.
		 * </pre>
		 * </p>
		 *
		 * @param myObj:<code>Object</code>.
		 *
		 * @return Boolean indiquant si le fichier importé contient bien les mêmes en-têtes que la DataGrid.
		 *
		 */
		public function checkHeader(myObj:Object):Boolean
		{
			var longueur:int = myImportMasse.arrayNomColonnes.length;
			
			if(myObj)
			{
				if ((myObj as Array).length > 0)
				{
					for (var index:int=0; index<longueur; index++)
					{
						if (myObj[index]['value'].toString().toUpperCase() != myImportMasse.arrayNomColonnes[index].toString().toUpperCase())
						{
							Alert.show(ResourceManager.getInstance().getString('M111', 'Le_fichier_n_a_pas_le_format_d_une_matri'), ResourceManager.getInstance().getString('M111', 'Erreur'));
							return false;
						}
					}
					return true;
				}
				else
				{
					Alert.show(ResourceManager.getInstance().getString('M111', 'Le_fichier_n_a_pas_le_format_d_une_matri'), ResourceManager.getInstance().getString('M111', 'Erreur'));
					return false;
				}
				return false;
			}
			return false;
		}
		
		/**
		 * <b>Fonction <code>dataGridKeyDownHandler(evt:KeyboardEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Détecte l'événement clavier "Ctrl + V" et colle dans le DataGrid ce qui y a dans le presse papier.
		 * </pre>
		 * </p>
		 *
		 * @param evt:<code>KeyboardEvent</code>.
		 *
		 * @return void.
		 *
		 */
		public function dataGridKeyDownHandler(evt:KeyboardEvent):void
		{
			if (evt.ctrlKey && !myImportMasse.dgElement.getChildByName("clipboardProxy") && (evt.target is DataGrid))
			{
				// Ajout d'un objet TextField invisible pour le DataGrid
				var textField:TextField=new TextField();
				textField.name="clipboardProxy";
				myImportMasse.dgElement.addChild(textField);
				textField.visible=false;
				textField.type=TextFieldType.INPUT;
				textField.multiline=true;
				// Mettre dans le TextField les données copiées depuis le format TSV 
				textField.text=getTextFromItems(myImportMasse.dgElement.selectedItems);
				textField.setSelection(0, textField.text.length - 1);

				textField.addEventListener(TextEvent.TEXT_INPUT, clipboardProxyPasteHandler);

				// Mettre le focus au TextField
				myImportMasse.systemManager.stage.focus=textField;
			}
		}

		///////////////////////////////////////////////////////////
		/////////////		FONCTIONS PRIVEES     /////////////////
		///////////////////////////////////////////////////////////   

		/**
		 * <b>Fonction <code>clipboardProxyPasteHandler(evt:TextEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Vérifie la cohérence entre ce qui est dans le presse papier et les en-têtes de colonnes attendues.
		 * Appelle la méthode <code>cleanClipBoard()</code> qui effacera le contenu du presse papier.
		 * </pre>
		 * </p>
		 *
		 * @param evt:<code>TextEvent</code>.
		 *
		 * @return void.
		 *
		 */
		private function clipboardProxyPasteHandler(evt:TextEvent):void
		{
			if(myImportMasse.isEnabledBtnImport)
			{
				// recup statut des cases à cocher
				myImportMasse.arrayColonnesToImport = new Array(
					{MATRICULE:myImportMasse.collab_isChecked},
					{PRENOM:myImportMasse.collab_isChecked},
					{NOM:myImportMasse.collab_isChecked},
					{IMEI:myImportMasse.imei_isChecked},
					{NUMSERIE:myImportMasse.imei_isChecked},
					{NUMSIM:myImportMasse.sim_isChecked},
					{NUMEXT:myImportMasse.extension_isChecked},
					{NUMLIGNE:myImportMasse.ligne_isChecked}
				);
				
				// Extraction des valeurs depuis le format TSV
				var items:Array=getItemsFromText(evt.text);
				var longueur:int=items.length;
				var longueur2:int=myImportMasse.arrayNomColonnes.length;
				
				if (longueur > 0 && longueur <= 101)
				{
					if(longueur + myImportMasse.myDataProvider.length <= 101)
					{
						if(checkHeaderClipboard(items[0]))
						{
							// entetes en cohérence donc affectation des valeurs
							for (var index:int=1; index<longueur; index++)
							{
								var newItem:Object = new Object();
								var objImp:ImportDeMasseVO = new ImportDeMasseVO();							
								for (var index2:int=0; index2<longueur2; index2++)
								{
									if(myImportMasse.arrayColonnesToImport[index2][myImportMasse.arrayDataFieldColonnes[index2]])
									{
										newItem[myImportMasse.arrayDataFieldColonnes[index2]] = items[index][myImportMasse.arrayDataFieldColonnes[index2]];
									}
								}
								objImp.fillVO(newItem);/*items[index]*/
								myImportMasse.myDataProvider.addItem(objImp);
							}
							myImportMasse.updateNbElementText();
							
							checkDoublonsForMultipleValue(myImportMasse.myDataProvider);
						}
					}
					else
					{
						Alert.show(ResourceManager.getInstance().getString('M111', 'Vous_allez_d_passer_le_nombre_maximum_d__l_ments_autoris_'), ResourceManager.getInstance().getString('M111', 'Erreur'));
					}
				}
				else
				{
					Alert.show(ResourceManager.getInstance().getString('M111','Le_fichier_est_trop_lourd__il_contient_p'), ResourceManager.getInstance().getString('M111','Erreur'));
				}
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_au_moins_type_éléments___importer_'), ResourceManager.getInstance().getString('M111', 'Erreur'));
			}
			
			cleanClipBoard();
		}
		
		/* verifier les entetes du fichier copié/collé */
		private function checkHeaderClipboard(arrayEntete:Object):Boolean
		{
			var longueur2:int=myImportMasse.arrayNomColonnes.length;
			
			if(arrayEntete)
			{
//				if ((arrayEntete as Array).length > 0)
//				{
						for (var index2:int=0; index2<longueur2; index2++)
						{
							if ((arrayEntete[myImportMasse.arrayDataFieldColonnes[index2]] as String).toUpperCase() != (myImportMasse.arrayNomColonnes[index2] as String).toUpperCase())
							{
								ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Les_en_t_tes_des_colonnes_ne_sont_pas_en'), ResourceManager.getInstance().getString('M111', 'Erreur'));
								cleanClipBoard();
								return false;
							}
						}
					return true;
//				}
//				else
//				{
//					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Les_en_t_tes_des_colonnes_ne_sont_pas_en'), ResourceManager.getInstance().getString('M111', 'Erreur'));
//					cleanClipBoard();
//					return false;
//				}
//				return false;
			}
			return false;
		}
			
		/**
		 * <b>Fonction <code>cleanClipBoard()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Efface le contenu du presse papier.
		 * </pre>
		 * </p>
		 *
		 * @return void.
		 *
		 */
		private function cleanClipBoard():void
		{
			var textField:TextField=TextField(myImportMasse.dgElement.getChildByName("clipboardProxy"));
			if (textField)
			{
				textField.removeEventListener(TextEvent.TEXT_INPUT, clipboardProxyPasteHandler);
				myImportMasse.dgElement.removeChild(textField);
				textField=null;
			}
		}

		/**
		 * <b>Fonction <code>getItemsFromText(text:String)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Créée un Array contenant toutes les cellules à coller dans le DataGrid, d'après le contenu du presse papier.
		 * </pre>
		 * </p>
		 *
		 * @param text:<code>String</code> représentant le contenu du presse papier.
		 *
		 * @return Array contenant toutes les cellules à coller dans le DataGrid.
		 *
		 */
		private function getItemsFromText(text:String):Array
		{
			var rows:Array=text.split("\n");
			var columns:Array=myImportMasse.arrayDataFieldColonnes; //dgElement.columns;
			var itemsFromText:Array=[];

			if (!rows[rows.length - 1])
			{
				rows.pop();
			}

			for each (var rw:String in rows)
			{
				var fields:Array=rw.split("\t");
				var n:int=Math.min(columns.length, fields.length);
				var item:Object={};

				for (var i:int=0; i<n; i++)
				{
					item[columns[i]]=fields[i];
				}
				itemsFromText.push(item);
			}
			return itemsFromText;
		}

		/**
		 * <b>Fonction <code>getTextFromItems(items:Array)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Créée une String contenant toutes les en-têtes du DataGrid.
		 * </pre>
		 * </p>
		 *
		 * @param items:<code>Array</code>.
		 *
		 * @return String contenant toutes les en-têtes du DataGrid.
		 *
		 */
		private function getTextFromItems(items:Array):String
		{
//			var columns:Array=myImportMasse.dgElement.columns;
			var columns:Array=myImportMasse.arrayDataFieldColonnes;
			var textFromItems:String="";

			for each (var it:Object in items)
			{
//				for each (var c:DataGridColumn in columns)
//				{
//					textFromItems+=it[c.dataField] + "\t";
//				}
				for each (var c:Object in columns)
				{
					textFromItems+=it[c] + "\t";
				}
				textFromItems+="\n";
			}
			return textFromItems;
		}
		
		
		///////////////////////////////////////////////////////////
		/////////////	    GETTER / SETTER       /////////////////
		///////////////////////////////////////////////////////////
		
		/**
		 * <b>Setter <code>myImportMasse(value:ImportMasseImpl)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Attribut une valeur à la variable privée _myImportMasse.
		 * </pre>
		 * </p>
		 *
		 * @param value:<code>ImportMasseImpl</code>.
		 *
		 * @return void.
		 *
		 */
		public function set myImportMasse(value:ImportMasseImpl):void
		{
			_myImportMasse = value;
		}
		
		/**
		 * <b>Getter <code>myImportMasse()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Retourne la valeur de la variable privée _myImportMasse.
		 * </pre>
		 * </p>
		 *
		 * @return ImportMasseImpl qui représente la référence unique de l'objet ImportMasseImpl.
		 *
		 */
		public function get myImportMasse():ImportMasseImpl
		{
			return _myImportMasse;
		}
	}
}
