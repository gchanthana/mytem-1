package composants.importmasse
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.UIDUtil;

	/**
	 * <code>ImportMasseService</code> est la classe qui gère l'appel des procédures stockées et leurs retours pour l'import de masse.
	 * Elle instancie une référence unique de la classe <code>ImportMasseImpl</code>.
	 * Grâce à cette instance elle peut appeller toutes les méthodes qui lui seront utiles dans les classes <code>ImportMasseUtils</code> et <code>ImportMasseUploadDownload</code>.
	 *
	 * @author GUIOU Nicolas
	 * @version 1.0 (02-03-2010)
	 */
	[Bindable]
	public class ImportMasseService
	{

		///////////////////////////////////////////////////////////
		/////////////		VARIABLES PRIVEES     /////////////////
		///////////////////////////////////////////////////////////

		private var _myImportMasse:ImportMasseImpl;

		/**
		 * <b>Constructeur <code>ImportMasseUtils(instanceMyImportMasse:ImportMasseImpl)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Instancie la variable privée myImportMasse pour avoir une référence unique de l'objet ImportMasseImpl.
		 * </pre>
		 * </p>
		 *
		 * @param instanceMyImportMasse:<code>ImportMasseImpl</code>.
		 *
		 */
		public function ImportMasseService(instanceMyImportMasse:ImportMasseImpl)
		{
			myImportMasse=instanceMyImportMasse;
		}

		///////////////////////////////////////////////////////////
		/////////////	    GETTER / SETTER       /////////////////
		///////////////////////////////////////////////////////////

		/**
		 * <b>Setter <code>myImportMasse(value:ImportMasseImpl)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Attribut une valeur à la variable privée _myImportMasse.
		 * </pre>
		 * </p>
		 *
		 * @param value:<code>ImportMasseImpl</code>.
		 *
		 * @return void.
		 *
		 */
		public function set myImportMasse(value:ImportMasseImpl):void
		{
			_myImportMasse=value;
		}

		/**
		 * <b>Getter <code>myImportMasse()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Retourne la valeur de la variable privée _myImportMasse.
		 * </pre>
		 * </p>
		 *
		 * @return ImportMasseImpl qui représente la référence unique de l'objet ImportMasseImpl.
		 *
		 */
		public function get myImportMasse():ImportMasseImpl
		{
			return _myImportMasse;
		}

		///////////////////////////////////////////////////////////
		///////////// APPEL DE PROCEDURE STOCKEE  /////////////////
		///////////////////////////////////////////////////////////		

		/**
		 * <b>Fonction <code>readCSVandCSVToArray()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Cette procédure stockée transforme un fichier .csv en un Array.
		 * </pre>
		 * </p>
		 *
		 * @return void.
		 *
		 */
		public function readCSVandCSVToArray():void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.equipement.utils.ParsingCSV",
				"ReadCSVandCSVToArray",
				readCSVandCSVToArrayResultHandler);
			RemoteObjectUtil.callService(opData, myImportMasse._popUpUpload.pathFile);
		}

		/**
		 * <b>Fonction <code>readXLSandXLSToArray()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Cette procédure stockée transforme un fichier .xls en un Array.
		 * </pre>
		 * </p>
		 *
		 * @return void.
		 *
		 */
		public function readXLSandXLSToArray():void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.equipement.utils.ParsingXLS",
				"ReadExcel",
				readXLSandXLSToArrayResultHandler);
			RemoteObjectUtil.callService(opData, myImportMasse._popUpUpload.pathFile, myImportMasse.tabNomColAttendu.length, true, 0);
		}

		/**
		 * <b>Fonction <code>renameFile()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Cette procédure stockée transforme renomme un fichier de manière unique.
		 * </pre>
		 * </p>
		 *
		 * @return void.
		 *
		 */
		public function renameFile():void
		{
			var longueur:int=myImportMasse._popUpUpload.fileName.length;
			var newName:String=myImportMasse._popUpUpload.fileName.substr(0, longueur - 4)
			newName+="_" + UIDUtil.createUID();
			newName+=myImportMasse._popUpUpload.fileName.substr(longueur - 4, longueur);
			myImportMasse._popUpUpload.pathFile=myImportMasse.basePath + newName;

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.inventaire.equipement.utils.RenameFileUploaded", 
				"RenameFile", 
				renameFileResultHandler);
			RemoteObjectUtil.callService(opData, myImportMasse._popUpUpload.fileName, newName, myImportMasse.basePath);
		}

		///////////////////////////////////////////////////////////
		/////////   RESULTAT D'APPEL DE PROCEDURE STOCKEE  ////////
		///////////////////////////////////////////////////////////	

		/**
		 * <b>Fonction <code>readCSVandCSVToArrayResultHandler(evt:ResultEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Traite le retour de la procédure stockée "readCSVandCSVToArray".
		 * Si le retour est celui attendu, un Array contenant les valeurs du fichier .csv,
		 * alors la méthode <code>checkHeader()</code> de la classe ImportMasseUtils est appellée et vérifie la cohérence des en-têtes des colonnes entre le fichier importé et le DataGrid,
		 * puis si les en-têtes sont cohérentes alors la méthode <code>fillDG()</code> de la classe ImportMasseUtils est appellée et remplis le DataGrid.
		 * </pre>
		 * </p>
		 *
		 * @param evt:<code>ResultEvent</code>.
		 *
		 * @return void.
		 *
		 */
		private function readCSVandCSVToArrayResultHandler(re:ResultEvent):void
		{
			if (re.result is Array)
			{
				if (myImportMasse.myImportMasseUtils.checkHeader((re.result as Array)))
				{
					var tmpArray:Array=new Array();
					for (var index:int=myImportMasse.tabNomColAttendu.length; index < (re.result as Array)[0].length - 1; index++)
					{
						tmpArray.push((re.result as Array)[0][index]);
					}
//					myImportMasse.myImportMasseUtils.fillDG(tmpArray);
				}
			}
		}

		/**
		 * <b>Fonction <code>readXLSandXLSToArrayResultHandler(evt:ResultEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Traite le retour de la procédure stockée "readXLSandXLSToArray".
		 * Si le retour est celui attendu, un Array contenant les valeurs du fichier .csv,
		 * alors la méthode <code>checkHeader()</code> de la classe ImportMasseUtils est appellée et vérifie la cohérence des en-têtes des colonnes entre le fichier importé et le DataGrid,
		 * puis si les en-têtes sont cohérentes alors la méthode <code>fillDG()</code> de la classe ImportMasseUtils est appellée et remplis le DataGrid.
		 * </pre>
		 * </p>
		 *
		 * @param evt:<code>ResultEvent</code>.
		 *
		 * @return void.
		 *
		 */
		private function readXLSandXLSToArrayResultHandler(re:ResultEvent):void
		{
			if (re.result is Object)
			{
				if (myImportMasse.myImportMasseUtils.checkHeader((re.result.SHEETDATA as Object)))
				{
//					myImportMasse.myImportMasseUtils.fillDG((evt.result.ARRCELL as Array));
				}
			}
		}

		/**
		 * <b>Fonction <code>renameFileResultHandler(evt:ResultEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Traite le retour de la procédure stockée "renameFile".
		 * Si l'extention du fichier importé est ".csv" alors la méthode <code>readCSVandCSVToArray()</code> est appellée, sinon (fichier .xls) c'est la méthode <code>readXLSandXLSToArray()</code> qui est appellée.
		 * </pre>
		 * </p>
		 *
		 * @param evt:<code>ResultEvent</code>.
		 *
		 * @return void.
		 *
		 */
		private function renameFileResultHandler(re:ResultEvent):void
		{
			var longueur:int=myImportMasse._popUpUpload.fileName.length;
			if (myImportMasse._popUpUpload.fileName.substr(longueur - 3, longueur) == "csv")
			{
				readCSVandCSVToArray();
			}
			else
			{
				readXLSandXLSToArray();
			}
		}

	}
}
