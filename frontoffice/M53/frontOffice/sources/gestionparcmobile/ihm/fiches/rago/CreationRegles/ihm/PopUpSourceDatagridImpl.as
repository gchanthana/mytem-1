package univers.parametres.rago.CreationRegles.ihm
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.rago.CreationRegles.system.Wizzard;
	
	[Bindable]
	public class PopUpSourceDatagridImpl extends HBox
	{
		
//VARIABLES GLOBALES----
	
		public var searchabletreeihm2:destinataireOrga;
		public var dgListSource:DataGrid;
		public var _strgToSearch:TextInput;
		public var lblNbrNode:Label;
		public var btChoisir:Button;
		public var btCancel:Button;
		public var txtFiltre:TextInput;
		
		public var _wizzard:Wizzard;

		public var _nbrOfStage:Boolean;

		private var _resultOpeAnnu:Object;
		private var _numberOfResultFound:int;
		private var _nbNoeudSource:int = 0;
		private var _libelleSource:ArrayCollection;
		private var dataConcat:ArrayCollection = new ArrayCollection();

//FIN VARIABLES GLOBALES----

//PROPRIETES PUBLICS----
		
		//Nombre de noeuds sources
		public function set nbNoeudSource(value:int):void
		{ _nbNoeudSource = value; }

		public function get nbNoeudSource():int
		{ return _nbNoeudSource; }
		
		//Resultat de la sélection dans le viewstack 'SlectTypeIHM'
		public function set resultOpeAnnu(value:Object):void
		{ _resultOpeAnnu = value; }

		public function get resultOpeAnnu():Object
		{ return _resultOpeAnnu; }
		
		//Libelle source
		public function set libelleSource(value:ArrayCollection):void
		{ _libelleSource = value; }

		public function get libelleSource():ArrayCollection
		{ return _libelleSource; }
		
		//Nombre de résultat lors de la recherche
		public function set numberOfResultFound(value:int):void
		{ _numberOfResultFound = value; }

		public function get numberOfResultFound():int
		{ return _numberOfResultFound; }

//FIN PROPRIETES PUBLICS----

//METHODES PUBLICS----
		
		//Constructeur
		public function PopUpSourceDatagridImpl()
		{ addEventListener("finishToRefresh",search); }
		
//FIN METHODES PUBLICS----

//METHODES PROTECTED----
		
		//Appel à la fonction : de récupération de l'XML pour le traitement de conversion
		protected function initSourceFind(event:FlexEvent):void
		{ chargeDataToDatagrid(); }

		//Filtre (recherche dans le grid)
		protected function filtreChangeHandler(event:Event):void
		{
			var arrayCollectionSearch:ArrayCollection = new ArrayCollection();
			if(txtFiltre.text == "")
			{
				dgListSource.dataProvider = dataConcat;
				lblNbrNode.text = dataConcat.length.toString() + "noeuds";
			}
			else
			{
				for(var i:int = 0;i<dataConcat.length;i++)
				{
					if(dataConcat[i].LABEL.toString().toLowerCase().indexOf(txtFiltre.text.toLowerCase()) != -1)
					{ arrayCollectionSearch.addItem(dataConcat[i]); }
				}
			
				if(arrayCollectionSearch.length == 0)
				{ dgListSource.dataProvider = null }
				else
				{ dgListSource.dataProvider = arrayCollectionSearch; }
			
				lblNbrNode.text = arrayCollectionSearch.length.toString() + "noeuds";
			}
		}
		
		//Remet tous a 0
		protected function searchClickHandler(event:Event):void
		{
			if(libelleSource != null)
			{
				txtFiltre.text = "";
				dgListSource.dataProvider = dataConcat;
				lblNbrNode.text = dataConcat.length.toString() + "noeuds";
			}
		}
		
		//Dispatch l'évenement que la sélection de la source a été sélectionnée
		protected function btChoisirClickHandler(event:Event):void
		{ 
			dispatchEvent(new Event("getChoiceSource",true));
			PopUpManager.removePopUp(this); 
		}
		
		//Dispatch l'évenement que la sélection de la source a été annulé
		protected function btCancelClickHandler(event:Event):void
		{ 
			dispatchEvent(new Event("AnnulChoiceSource",true));
			PopUpManager.removePopUp(this); 
		}
		
		//Ne fais rien
		protected function datachangeHandler(event:Event):void
		{  }
		
		//Evenement lors du click sur le grid
		protected function dgClickHandler(event:Event):void
		{
			if(dgListSource.selectedItem != null)
			{
				if(dgListSource.selectedItem.LABEL == "Non répertoriée")
				{
					btChoisir.enabled = false;
				}
				else
				{
					btChoisir.enabled = true;
					//_wizzard.noeudSource = dgListSource.selectedItem;
					_wizzard.noeudSource = searchInXML(dgListSource.selectedItem); 
					//searchInXML(dgListSource.selectedItem);
					_wizzard.myRegle.IDSOURCE = dgListSource.selectedItem.VALUE;
					_wizzard.myRegle.LIBELLE_SOURCE = dgListSource.selectedItem.LABEL;
				}
			}
		}


//FIN METHODES PROTECTED----


//METHODES PRIVATE----
		
		//Récupération de l'XML pour le traitement de conversion
		private function chargeDataToDatagrid():void
		{
			searchabletreeihm2.clearSearch();			
			searchabletreeihm2._selectedOrga = resultOpeAnnu.IDGROUPE_CLIENT;
			searchabletreeihm2.refreshTree();
		}
		
		//Recherche et affiche les données XML que l'on recherche
		private function searchData():void
		{		
			var dataSource:ArrayCollection = new ArrayCollection();
			var dataFound:ArrayCollection = new ArrayCollection();
			var dataFoundConcat:ArrayCollection = new ArrayCollection();
			
			libelleSource = new ArrayCollection();
			
			searchabletreeihm2.txtRechercheOrga.text = _strgToSearch.text;
			libelleSource.addItem(searchabletreeihm2.treeDataArray[0]);
			
			//recupere le dossier maitre
			var parents:XML = libelleSource[0] as XML;
			//Recupere le dossier parents
			var mere:XMLList = parents.children();
			var obj:Object = new Object();
			
			
//				dataFound.addItem(parents);
//				
//				obj.VALUE = parents.@VALUE;
//				obj.NIV = parents.@NIV;
//				obj.LABEL = parents.@LABEL;
//				dataFoundConcat.addItem(obj);
			
			for(var i:int = 0;i < mere.length();i++)
			{	
				obj = new Object();
				dataFound.addItem(mere[i]);
				
				obj.VALUE = mere[i].@VALUE;
				obj.NIV = mere[i].@NIV;
				obj.LABEL = mere[i].@LABEL;
				dataFoundConcat.addItem(obj);

				var enfants0:XMLList = mere[i].children();
				for(var j:int = 0;j < enfants0.length();j++)
				{	
					obj = new Object();
					dataFound.addItem(enfants0[j]);
					
					obj.VALUE = enfants0[j].@VALUE;
					obj.NIV = enfants0[j].@NIV;
					obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL;
					dataFoundConcat.addItem(obj);

					var enfants1:XMLList = enfants0[j].children();
					for(var k:int = 0;k < enfants1.length();k++)
					{
						obj = new Object();
						dataFound.addItem(enfants1[k]);
						
						obj.VALUE = enfants1[k].@VALUE;
						obj.NIV = enfants1[k].@NIV;
						obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL;
						dataFoundConcat.addItem(obj);

						var enfants2:XMLList = enfants1[k].children();
						for(var l:int = 0;l < enfants2.length();l++)
						{
							obj = new Object();
							dataFound.addItem(enfants2[l]);
							
							obj.VALUE = enfants2[l].@VALUE;
							obj.NIV = enfants2[l].@NIV;
							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL;
							dataFoundConcat.addItem(obj);

							var enfants3:XMLList = enfants2.children();
							for(var m:int = 0;m < enfants3.length();m++)
							{
								obj = new Object();
								dataFound.addItem(enfants3[m]);
								
								obj.VALUE = enfants3[m].@VALUE;
								obj.NIV = enfants3[m].@NIV;
								obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL;
								dataFoundConcat.addItem(obj);

								var enfants4:XMLList = enfants3.children();
								for(var n:int = 0;n < enfants4.length();n++)
								{
									obj = new Object();
									dataFound.addItem(enfants4[n]);
									
									obj.VALUE = enfants4[n].@VALUE;
									obj.NIV = enfants4[n].@NIV;
									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL;
									dataFoundConcat.addItem(obj);

									var enfants5:XMLList = enfants4.children();
									for(var o:int = 0;o < enfants5.length();o++)
									{
										obj = new Object();
										dataFound.addItem(enfants5[o]);
										
										obj.VALUE = enfants5[o].@VALUE;
										obj.NIV = enfants5[o].@NIV;
										obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL;
										dataFoundConcat.addItem(obj);

										var enfants6:XMLList = enfants5.children();
										for(var p:int = 0;p < enfants6.length();p++)
										{
											obj = new Object();
											dataFound.addItem(enfants6[o]);
										
											obj.VALUE = enfants6[p].@VALUE;
											obj.NIV = enfants6[p].@NIV;
											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL;
											dataFoundConcat.addItem(obj);	
										}
									}
								}
							}
						}
					}
				}
			}


			var dataCorresponded:ArrayCollection = new ArrayCollection();
			
			for(var q:int = 0;q < dataFoundConcat.length;q++)
			{
				if(dataFoundConcat[q].LABEL.toString().toLowerCase().indexOf(_strgToSearch.text.toLowerCase()) != -1)
				{ dataCorresponded.addItem(dataFoundConcat[q]); }
			}
		
			if(dataCorresponded.length == 0)
			{
				var newObject:Object = new Object();
				newObject.LABEL = "Non répertoriée";
				dgListSource.dataProvider = newObject;
			}
			else
			{
				dataConcat = dataCorresponded;
				dgListSource.dataProvider = dataCorresponded;
			}
			
			nbNoeudSource = dataCorresponded.length;
		}
		
		//Recherche et affiche tout l'XML
		private function searchAll():void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			var dataSource:ArrayCollection = new ArrayCollection();
			var dataFound:ArrayCollection = new ArrayCollection();
			var dataFoundConcat:ArrayCollection = new ArrayCollection();
	
			libelleSource = new ArrayCollection();
			
			searchabletreeihm2.txtRechercheOrga.text = _strgToSearch.text;
			libelleSource.addItem(searchabletreeihm2.treeDataArray[0]);
			
			//recupere le dossier maitre
			var parents:XML = libelleSource[0] as XML;
			//Recupere le dossier parents
			
			var obj:Object = new Object();
			
			
			//dataFound.addItem(parents);
				
			//obj.VALUE = parents.@VALUE;
			//obj.NIV = parents.@NIV;
			//obj.LABEL = parents.@LABEL;
			//dataFoundConcat.addItem(obj);
				
			var mere:XMLList = parents.children();
			for(var i:int = 0;i < mere.length();i++)
			{	
				obj = new Object();
				dataFound.addItem(mere[i]);
				
				obj.VALUE = mere[i].@VALUE;
				obj.NIV = mere[i].@NIV;
				obj.LABEL = mere[i].@LABEL;
				dataFoundConcat.addItem(obj);

				var enfants0:XMLList = mere[i].children();
				for(var j:int = 0;j < enfants0.length();j++)
				{	
					obj = new Object();
					dataFound.addItem(enfants0[j]);
					
					obj.VALUE = enfants0[j].@VALUE;
					obj.NIV = enfants0[j].@NIV;
					obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL;
					dataFoundConcat.addItem(obj);

					var enfants1:XMLList = enfants0[j].children();
					for(var k:int = 0;k < enfants1.length();k++)
					{
						obj = new Object();
						dataFound.addItem(enfants1[k]);
						
						obj.VALUE = enfants1[k].@VALUE;
						obj.NIV = enfants1[k].@NIV;
						obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL;
						dataFoundConcat.addItem(obj);

						var enfants2:XMLList = enfants1[k].children();
						for(var l:int = 0;l < enfants2.length();l++)
						{
							obj = new Object();
							dataFound.addItem(enfants2[l]);
							
							obj.VALUE = enfants2[l].@VALUE;
							obj.NIV = enfants2[l].@NIV;
							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL;
							dataFoundConcat.addItem(obj);

							var enfants3:XMLList = enfants2.children();
							for(var m:int = 0;m < enfants3.length();m++)
							{
								obj = new Object();
								dataFound.addItem(enfants3[m]);
								
								obj.VALUE = enfants3[m].@VALUE;
								obj.NIV = enfants3[m].@NIV;
								obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL;
								dataFoundConcat.addItem(obj);

								var enfants4:XMLList = enfants3.children();
								for(var n:int = 0;n < enfants4.length();n++)
								{
									obj = new Object();
									dataFound.addItem(enfants4[n]);
									
									obj.VALUE = enfants4[n].@VALUE;
									obj.NIV = enfants4[n].@NIV;
									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL;
									dataFoundConcat.addItem(obj);

									var enfants5:XMLList = enfants4.children();
									for(var o:int = 0;o < enfants5.length();o++)
									{
										obj = new Object();
										dataFound.addItem(enfants5[o]);
										
										obj.VALUE = enfants5[o].@VALUE;
										obj.NIV = enfants5[o].@NIV;
										obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL;
										dataFoundConcat.addItem(obj);

										var enfants6:XMLList = enfants5.children();
										for(var p:int = 0;p < enfants6.length();p++)
										{
											obj = new Object();
											dataFound.addItem(enfants6[o]);
										
											obj.VALUE = enfants6[p].@VALUE;
											obj.NIV = enfants6[p].@NIV;
											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL;
											dataFoundConcat.addItem(obj);	
										}
									}
								}
							}
						}
					}
				
				}
			}

			if(dataFound.length != 0)
			{
				dataConcat = dataFoundConcat;
				dgListSource.dataProvider = dataFoundConcat;
			}
			
			nbNoeudSource = dataFoundConcat.length;
		}
		
		//Cherche soit tout ou un cas particulier sélectionnée dans le viewstack 'SelectSourceIHM'
		private function search(event:Event):void
		{
			if( _strgToSearch.text == "")
			{ searchAll(); }
			else
			{ searchData(); }
			lblNbrNode.text = dataConcat.length.toString() + "noeuds";
		}
		
		
		//Recherche les données dans l'XML
		private function searchInXML(objectSelected:Object):Object
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			var dataSource:ArrayCollection = new ArrayCollection();
			var dataFound:ArrayCollection = new ArrayCollection();
			var dataFoundConcat:ArrayCollection = new ArrayCollection();
			
			libelleSource = new ArrayCollection();
			
			searchabletreeihm2.txtRechercheOrga.text = _strgToSearch.text;
			libelleSource.addItem(searchabletreeihm2.treeDataArray[0]);
			
			//recupere le dossier maitre
			var parents:XML = libelleSource[0] as XML;
			//Recupere le dossier parents
			var mere:XMLList = parents.children();
			var obj:Object = new Object();
			
			
			dataFound.addItem(parents);
				
			obj.VALUE = parents.@VALUE;
			obj.NIV = parents.@NIV;
			obj.LABEL = parents.@LABEL;
			dataFoundConcat.addItem(obj);

			for(var i:int = 0;i < mere.length();i++)
			{	
				obj = new Object();
				dataFound.addItem(mere[i]);
				
				obj.VALUE = mere[i].@VALUE;
				obj.NIV = mere[i].@NIV;
				obj.LABEL = mere[i].@LABEL;
				dataFoundConcat.addItem(obj);

				var enfants0:XMLList = mere[i].children();
				for(var j:int = 0;j < enfants0.length();j++)
				{	
					obj = new Object();
					dataFound.addItem(enfants0[j]);
					
					obj.VALUE = enfants0[j].@VALUE;
					obj.NIV = enfants0[j].@NIV;
					obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL;
					dataFoundConcat.addItem(obj);

					var enfants1:XMLList = enfants0[j].children();
					for(var k:int = 0;k < enfants1.length();k++)
					{
						obj = new Object();
						dataFound.addItem(enfants1[k]);
						
						obj.VALUE = enfants1[k].@VALUE;
						obj.NIV = enfants1[k].@NIV;
						obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL;
						dataFoundConcat.addItem(obj);

						var enfants2:XMLList = enfants1[k].children();
						for(var l:int = 0;l < enfants2.length();l++)
						{
							obj = new Object();
							dataFound.addItem(enfants2[l]);
							
							obj.VALUE = enfants2[l].@VALUE;
							obj.NIV = enfants2[l].@NIV;
							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL;
							dataFoundConcat.addItem(obj);

							var enfants3:XMLList = enfants2.children();
							for(var m:int = 0;m < enfants3.length();m++)
							{
								obj = new Object();
								dataFound.addItem(enfants3[m]);
								
								obj.VALUE = enfants3[m].@VALUE;
								obj.NIV = enfants3[m].@NIV;
								obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL;
								dataFoundConcat.addItem(obj);

								var enfants4:XMLList = enfants3.children();
								for(var n:int = 0;n < enfants4.length();n++)
								{
									obj = new Object();
									dataFound.addItem(enfants4[n]);
									
									obj.VALUE = enfants4[n].@VALUE;
									obj.NIV = enfants4[n].@NIV;
									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL;
									dataFoundConcat.addItem(obj);

									var enfants5:XMLList = enfants4.children();
									for(var o:int = 0;o < enfants5.length();o++)
									{
										obj = new Object();
										dataFound.addItem(enfants5[o]);
										
										obj.VALUE = enfants5[o].@VALUE;
										obj.NIV = enfants5[o].@NIV;
										obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL;
										dataFoundConcat.addItem(obj);

										var enfants6:XMLList = enfants5.children();
										for(var p:int = 0;p < enfants6.length();p++)
										{
											obj = new Object();
											dataFound.addItem(enfants6[o]);
										
											obj.VALUE = enfants6[p].@VALUE;
											obj.NIV = enfants6[p].@NIV;
											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL;
											dataFoundConcat.addItem(obj);	
										}
									}
								}
							}
						}
					}
				
				}
			}
			try
			{
				var numberOfResultFound:int = 0;
				var strg:String = objectSelected.LABEL;
				for(var tp:int;tp < dataFoundConcat.length;tp++)
				{
					if(dataFound[tp].@VALUE == objectSelected.VALUE)
					{
						dataFound.addItem(dataFound[tp]);
						numberOfResultFound = tp;
					}
				}
				if(numberOfResultFound != 0) { _wizzard._endStageOrNot = (dataFound[numberOfResultFound] as XML).hasSimpleContent(); }
			}
			catch(error:Error){}
			return dataFound[numberOfResultFound];
		}
		
//FIN METHODES PRIVATE----

	}
}