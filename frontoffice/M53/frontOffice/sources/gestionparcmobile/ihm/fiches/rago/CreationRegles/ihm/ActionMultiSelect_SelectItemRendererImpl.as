package univers.parametres.rago.CreationRegles.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.HBox;
	import mx.controls.CheckBox;
	
	[Bindable]
	public class ActionMultiSelect_SelectItemRendererImpl extends HBox
	{
		public var ckbSelected:CheckBox;
		
		
		public function ActionMultiSelect_SelectItemRendererImpl()
		{
			super();
			addEventListener(MouseEvent.CLICK, _localeClickHandler);
			callLater(initActionMultiSelect_SelectItemRenderer);
		}
		
		protected function _localeClickHandler(event:MouseEvent):void
		{
			if(event == null) return;			
		
			switch(ckbSelected.selected)
			{
				case true: 	{
								dispatchEvent(new Event("thisNodeIsAnExceptionSelect",true)); break;
							}
				case false: {
								dispatchEvent(new Event("thisNodeIsNotAnExceptionSelect",true)); break;
							}
				default:
				{ ckbSelected.selected = false; }
			}
		}
		
		protected function initActionMultiSelect_SelectItemRendererHandler(event:Event):void
		{
			ckbSelected.selected = false;
		}
		
		protected function initActionMultiSelect_SelectItemRenderer():void
		{
			ckbSelected.selected = false;
		}

	}
}