package univers.parametres.rago.CreationRegles.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Box;
	import mx.containers.ViewStack;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.rago.CreationRegles.event.WizardEvent;
	import univers.parametres.rago.CreationRegles.system.Wizzard;
	import univers.parametres.rago.entity.Regle;
	import univers.parametres.rago.entity.Rules;
	
	
	[Event(name="goNextEtape",type="univers.parametres.rago.CreationRegles.event.WizardEvent")]
	[Event(name="goPreviousEtape",type="univers.parametres.rago.CreationRegles.event.WizardEvent")]
	[Event(name="annulerCreationRegle",type="univers.parametres.rago.CreationRegles.event.WizardEvent")]
	
	[Bindable]
	public class MainCreateRegleImpl extends Box
	{

//VARIABLES GLOBAL----
		
		public var etapeSelectType:SelectTypeIHM;
		public var etapeSelectSource:SelectSourceIHM;
		public var etapeSelectTarget:SelectTargetIHM;
		public var etapeSelectExceptions:SelectExceptionsIHM;
		public var etapeEditInformations:EditInformationsIHM;
		
		private var _wizzard:Wizzard = new Wizzard();	
		private var _myRegle:Regle = new Regle();
		private var _myRules:Rules = new Rules();
		
		private var _noeudSource:Object;
		private var _noeudCible:Object;
		private var _libelleNodeSelected:String;

		private var _cboxOpeAnnuResult:Object;
		private var _cboxClienteResult:Object;
		
		private var _cbxCliente:Object;
		private var _cbxOperateurAnnuaire:Object;
						
		public var btPre:Button;
		public var btCancel:Button;
		public var btNext:Button;
		public var btValid:Button;
		
		public var btnParametres:Button;
		public var btnOrigine:Button;
		public var btnCible:Button;
		public var btnException:Button;
		public var btnValidation:Button;
		
		public var vs:ViewStack;
		
		
		private var _selectedIndex:int;
		private var _dataProviderCbx:Object = null;				
		public var _contentWidth:Number=610; 
		public var _contentHeight:Number=460;
		private var _newRegle:Object;
		
		private var _jumpExceptionOrNot:Boolean;
		
		public var _colorBtn0:Number;
		public var _colorBtn1:Number;
		public var _colorBtn2:Number;
		public var _colorBtn3:Number;
		public var _colorBtn4:Number;
		
		public var _colorBtnActif_R:Number = 237;
		public var _colorBtnActif_G:Number = 116;
		public var _colorBtnActif_B:Number = 18;
		
		private var _colorBtnNoActif_R:Number = 183;
		private var _colorBtnNoActif_G:Number = 186;
		private var _colorBtnNoActif_B:Number = 188;
		
		private var _colorBtnActifNotEnabled_R:Number = 13;
		private var _colorBtnActifNotEnabled_G:Number = 13;
		private var _colorBtnActifNotEnabled_B:Number = 13;

//FIN VARIABLES GLOBAL----

//PROPRIETEES PUBLICS----

		
		
		//Wizzard
		private function set wizzard(value:Wizzard):void
		{ _wizzard = value; }

		private function get wizzard():Wizzard
		{ return _wizzard; }
		
		//Regle
		public function set regle(value:Regle):void
		{ _myRegle = value; }

		public function get regle():Regle
		{ return _myRegle; }
		
		//Rules
		public function set rules(value:Rules):void
		{ _myRules = value; }

		public function get rules():Rules
		{ return _myRules; }
		
		//Récupére l'organisation opérateur ou annuaire sélectionnée dans le cbox
		public function get CboxOpeAnnuResult(): Object
		{ return _cboxOpeAnnuResult; } 
		
		public function set CboxOpeAnnuResult(data : Object):void
		{ _cboxOpeAnnuResult = data; }
		
		//Récupére l'organisation cliente sélectionnée dans le cbox
		public function get CboxClienteResult(): Object
		{ return _cboxClienteResult; } 
		
		public function set CboxClienteResult(data : Object):void
		{ _cboxClienteResult = data; }
		
		//Concerve la trace du nom du noeud ou de la feuille selectionnée
		public function set libelleNodeSelected(value:String):void
		{ _libelleNodeSelected = value; }

		public function get libelleNodeSelected():String
		{ return _libelleNodeSelected; }
		
		//Liste des organisations opérateur ou annuaire pour le dataprovider du cbox opérateur/annuaire
		public function set CboxOperateurAnnuaire(value:Object):void
		{ _cbxOperateurAnnuaire = value; }

		public function get CboxOperateurAnnuaire():Object
		{ return _cbxOperateurAnnuaire; }
		
		//Liste des organisations clientes pour le dataprovider du cbox client
		public function set CboxCliente(value:Object):void
		{ _cbxCliente = value; }

		public function get CboxCliente():Object
		{ return _cbxCliente; }
		
		//On saute l'etape exception ou non
		private function set jumpExceptionOrNot(value:Boolean):void
		{ _jumpExceptionOrNot = value; }

		private function get jumpExceptionOrNot():Boolean
		{ return _jumpExceptionOrNot; }

		//Nouvelle Regle
		public function set newRegle(value:Object):void
		{ _newRegle = value; }

		public function get newRegle():Object
		{ return _newRegle; }
		
		//Index du viewstack
		public function set selectedIndex(value:int):void
		{ _selectedIndex = value; }

		public function get selectedIndex():int
		{ return _selectedIndex; }
		
//FIN PROPRIETEES PUBLICS----

//METHODES PUBLICS----
		
		//Constructeur
		public function MainCreateRegleImpl()
		{ 
			addEventListener(WizardEvent.GO_NEXT_ETAPE, _localeGoNextEtapeHandler, true);
			addEventListener(WizardEvent.GO_PREVIOUS_ETAPE, _localeGoPreviousEtapeHandler, true);
			addEventListener(WizardEvent.ANNULER_CREATION_REGLE, _localeAnnulerCreationRegleHandler, true);
			addEventListener(WizardEvent.VALIDER_CREATION_REGLE, _localeValiderCreationRegleHandler, true);
			addEventListener(WizardEvent.VALIDER_CREATION_ET_EXECUTER_REGLE, _localeValiderCreationEtExecuterRegleHandler);
			super();
		}
		
		//Méthodes RGB converti en hexa
		public function rgb2hex(r:int, g:int, b:int):Number 
		{ return(r<<16 | g<<8 | b); }


//FIN METHODES PUBLICS----


//METHODES PROTECTED----
		
		//Ecoute s'il y a un click
		protected function init():void
		{ addEventListener(MouseEvent.CLICK, _localeClickHandler); }
		
		//Initilise l'IHM lorsque la page a fini de ce loader
		protected function initMainCreateRegle(event:Event):void
		{
			selectedIndex = -2;
			zBtnActifOrNot(true,false,false,false,false,true);
			zFindNameUser();
			attributWizzardAtAll();
			_colorBtn0 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
			_colorBtn1 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
			_colorBtn2 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
			_colorBtn3 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
			_colorBtn4 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
		}
	
//----------------------------------- HANDLERS -------------------------------------------------------------/		
			
		protected function _localeCreationCompleteHandler(event:FlexEvent):void
		{ callLater(init); }

		protected function _localeClickHandler(event:MouseEvent):void
		{
			try { this[event.target.id + "ClickHandler"](event); }
			catch(error:Error) { }
		}
		
//-------------------------- HANDLERS ------------------------------------------------------------------------//
		
		//Attribut l'index du viewstack sur lequel on veut aller lors du click sur le bouton 'Suivant'
		protected function _localeGoNextEtapeHandler(event:WizardEvent):void
		{
			if(selectedIndex < 5)
			{
				if(selectedIndex < vs.numChildren -1)
				{ selectedIndex ++; }
			}
		}
		
		//Attribut l'index du viewstack sur lequel on veut aller lors du click sur le bouton 'Précédent'
		protected function _localeGoPreviousEtapeHandler(event:WizardEvent):void
		{
			if(selectedIndex > 0)
			{ selectedIndex --; }
		}

		protected function _localeAnnulerCreationRegleHandler(event:WizardEvent):void
		{ PopUpManager.removePopUp(this); }

		protected function _localeValiderCreationRegleHandler(event:WizardEvent):void
		{ PopUpManager.removePopUp(this); }

		protected function _localeValiderCreationEtExecuterRegleHandler(event:WizardEvent):void
		{ PopUpManager.removePopUp(this); }

		protected function _localeCloseHandler(event:CloseEvent):void
		{ PopUpManager.removePopUp(this); }
		
		//Attribut l'index du viewstack sur lequel on veut aller ainsi que sa couleur
		protected function btnParametresClickHandler(event:Event):void
		{
			if(btnParametres.enabled) {selectedIndex = 0;}
			noHiddenBtnNext();
			btPre.visible = false;
			
			_colorBtn0 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
			_colorBtn1 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn2 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn3 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn4 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
		}
		
		//Attribut l'index du viewstack sur lequel on veut aller ainsi que sa couleur
		protected function btnOrigineClickHandler(event:Event):void
		{
			if(btnOrigine.enabled) {selectedIndex = 1;}
			noHiddenBtnNext();
			
			_colorBtn0 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn1 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
			_colorBtn2 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn3 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn4 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
		}
		
		//Attribut l'index du viewstack sur lequel on veut aller ainsi que sa couleur
		protected function btnCibleClickHandler(event:Event):void
		{
			if(btnCible.enabled) {selectedIndex = 2;}
			noHiddenBtnNext();
			
			_colorBtn0 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn1 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn2 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
			_colorBtn3 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn4 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
		}
		
		//Attribut l'index du viewstack sur lequel on veut aller ainsi que sa couleur
		protected function btnExceptionClickHandler(event:Event):void
		{
			if(btnException.enabled) {selectedIndex = 3;}
			noHiddenBtnNext();
			
			_colorBtn0 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn1 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn2 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn3 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
			_colorBtn4 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
		}
		
		//Attribut l'index du viewstack sur lequel on veut aller ainsi que sa couleur
		protected function btnValidationClickHandler(event:Event):void
		{
			if(btnValidation.enabled) {selectedIndex = 4;}
			hiddenBtnNext();
			
			_colorBtn0 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn1 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn2 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn3 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
			_colorBtn4 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
		}
		
		//Evenement lors du click sur le bouton 'Précédent'
		protected function btPreClickHandler(event:Event):void
		{ precedant(); }
		
		//Evenement lors du click sur le bouton 'Annuler'
		protected function btCancelClickHandler(event:Event):void
		{ annuler(); }
		
		//Evenement lors du click sur le bouton 'Suivant'
		protected function btNextClickHandler(event:Event):void
		{ valider(); }
		
		//Check si tous les champs nécessaire à la création d'une règle ne son pas vide
		protected function btValidClickHandler(event:Event):void
		{ 
			if(wizzard.myRegle.REGLE_NOM == "->" || wizzard.myRegle.IDSOURCE == 0 || wizzard.myRegle.IDCIBLE == 0 || 
			wizzard.myRegle.LIBELLE_CIBLE == "" || wizzard.myRegle.LIBELLE_SOURCE == ""
			|| wizzard.myRegle.LIBELLE_CIBLE == "Non répertoriée" || wizzard.myRegle.LIBELLE_SOURCE == "Non répertoriée")
			{ Alert.show("Veuillez renseigner tous les champs.","Consoview"); }
			else
			{ executeSendRegle(); }
		}
		
//FIN METHODES PROTECTED----		

//METHODES PRIVATE----
		
		//Récupere le nom de la personne connectée dans la SESSION dans coldfusion
		private function zFindNameUser():void
		{ rules.getNameAndSurname(); }
		
		//Attribut à chaque viewstack toutes les propiétées et les méthodes de la classe wizzard 
		//pour ne pas instancier une nouvelle fois le mm classe et pouvoir la modifier
		private function attributWizzardAtAll():void
		{
			etapeSelectType._typeWizzard = wizzard;
			etapeSelectSource._sourceWizzard = wizzard;
			etapeSelectTarget._targetWizzard = wizzard;
			etapeSelectExceptions._exceptionWizzard = wizzard;
			etapeEditInformations._editInfowizzard = wizzard;
		}
		
		//Va chercher la fonction Activer/Désactiver les boutons du wizzard suivant 
		//l'étape sur laquelle on se trouve lorsque l'on appui sur le bouton 'suivant'
		private function valider():void
		{	
			dispatchEvent(new WizardEvent(wizzard.myRegle,WizardEvent.GO_NEXT_ETAPE,true));
			if(selectedIndex < 0) {selectedIndex = 0;}
			if(selectedIndex > 3) {return;}
			zFunction(selectedIndex,true);
			switch (selectedIndex)
			{
				case 0:zBtnActifOrNot(true,true,false,false,false,true);break;
				case 1:zBtnActifOrNot(true,true,true,false,false,true);break;
				case 2:zBtnActifOrNot(true,true,true,true,false,true);break;
				case 3:zBtnActifOrNot(true,true,true,true,true,true);break;		
				case 4:zBtnActifOrNot(true,true,true,true,true,true);break;
			}
			
		}
		
		//Dispactch l'évenement 'Annuler la crétion de la règle en cours'
		private function annuler():void
		{	
			dispatchEvent(new WizardEvent(wizzard.myRegle,WizardEvent.ANNULER_CREATION_REGLE,true));
			PopUpManager.removePopUp(this);
		}
		
		//Va chercher la fonction Activer/Désactiver les boutons du wizzard suivant l'étape sur laquelle on se trouve lorsque l'on appui sur le bouton 'précédent'
		private function precedant():void
		{	
			dispatchEvent(new WizardEvent(wizzard.myRegle,WizardEvent.GO_PREVIOUS_ETAPE,true));
			
			if(selectedIndex > 4) {selectedIndex = 4;}
			switch (selectedIndex)
			{
				case 0:zBtnActifOrNot(true,false,false,false,false,false);break;
				case 1:zBtnActifOrNot(true,false,false,false,false,false);break;
				case 2:zBtnActifOrNot(true,true,false,false,false,false);break;
				case 3:zBtnActifOrNot(true,true,true,false,false,false);break;		
				case 4:zBtnActifOrNot(true,true,true,true,false,false);break;
			}
			
			zFunction(selectedIndex,false);
		}
		
		//Active/Désactive les boutons du wizzard suivant l'étape sur laquelle on se trouve
		private function zBtnActifOrNot(btn0:Boolean,btn1:Boolean,btn2:Boolean,btn3:Boolean,btn4:Boolean,nextOrNot:Boolean):void
		{
			jumpExceptionOrNot = etapeSelectSource._jumpOrNot;
			
			if(nextOrNot) { selectedIndex++; }
			else { selectedIndex--; }
			
			if(jumpExceptionOrNot && selectedIndex == 3)
			{
				if(nextOrNot) { selectedIndex++; }
				else { selectedIndex--; }
				btn3 = false;
				btn4 = true;
				_colorBtn3 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
				_colorBtn4 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
			}
			
			if(selectedIndex == 4) { hiddenBtnNext(); }
			else { noHiddenBtnNext(); }
				
			btnParametres.enabled = btn0;
			btnOrigine.enabled = btn1;
			btnCible.enabled = btn2;
			btnException.enabled = btn3;
			btnValidation.enabled = btn4;
		}
		
		//Va chercher la fonction suivant l'etape sur laquelle on se trouve
		private function zFunction(itemSelected:int,bool:Boolean):void
		{
			switch (itemSelected)
			{
				case 0:functionItemSelected0(bool);break;
				case 1:functionItemSelected1(bool);break;
				case 2:functionItemSelected2(bool);break;
				case 3:functionItemSelected3(bool);break;		
			}
		}
		
		//Attribut les couleurs des boutons du wizzard
		private function functionItemSelected0(bool:Boolean):void
		{
			if(bool)
			{
				etapeSelectSource.operateurResult = etapeSelectType.operateurResult;
				etapeSelectTarget.clientResult = etapeSelectType.clientResult;
				etapeSelectTarget.operateurResult = etapeSelectType.operateurResult;
		
				_colorBtn0 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn1 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
				_colorBtn2 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
				_colorBtn3 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
				_colorBtn4 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
			}
			else
			{
				_colorBtn0 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
				_colorBtn1 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
				_colorBtn2 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
				_colorBtn3 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
				_colorBtn4 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
			}
		}
		
		//Attribut les couleurs des boutons du wizzard
		private function functionItemSelected1(bool:Boolean):void
		{
			if(bool)
			{
				_colorBtn0 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn1 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn2 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
				_colorBtn3 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
				_colorBtn4 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
			}
			else
			{
				_colorBtn0 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn1 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
				_colorBtn2 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
				_colorBtn3 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
				_colorBtn4 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
			}
		}
		
		//Attribut les couleurs des boutons du wizzard
		private function functionItemSelected2(bool:Boolean):void
		{
			if(bool)
			{
				noHiddenBtnNext();
				
				wizzard.myRegle.CREATE_DATE = zformateDate(new Date());
				wizzard.myRegle.NOM_USER_CREATE = rules.concatNameSurname;
				wizzard.myRegle.REGLE_NOM =  wizzard.myRegle.LIBELLE_SOURCE + "->" + wizzard.myRegle.LIBELLE_CIBLE;
				
				etapeSelectExceptions.zListNoeud();

				_colorBtn0 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn1 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn2 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn3 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
				_colorBtn4 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
			}
			else
			{
				_colorBtn0 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn1 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn2 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
				_colorBtn3 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
				_colorBtn4 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
			}
		}
		
		//Attribut les couleurs des boutons du wizzard
		private function functionItemSelected3(bool:Boolean):void
		{
			if(bool)
			{
				hiddenBtnNext();
				etapeEditInformations._cboxClient = etapeSelectType.clientResult;
				etapeEditInformations._cboxOpe = etapeSelectType.operateurResult;
				
				_colorBtn0 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn1 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn2 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn3 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn4 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
			}
			else
			{
				_colorBtn0 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn1 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn2 = rgb2hex(_colorBtnActifNotEnabled_R,_colorBtnActifNotEnabled_G,_colorBtnActifNotEnabled_B);
				_colorBtn3 = rgb2hex(_colorBtnActif_R,_colorBtnActif_G,_colorBtnActif_B);
				_colorBtn4 = rgb2hex(_colorBtnNoActif_R,_colorBtnNoActif_G,_colorBtnNoActif_B);
			}
		}
		
		//Formate la date en JJ/MM/AAAA
		private function zformateDate(dateToFormat:Object):String
		{
			var realDate:Object = new Object();
			
			realDate = {date:dateToFormat.date + "/" , month:zformateMonth(dateToFormat.month) + "/" , fullyear:dateToFormat.fullYear};
			
			var dayInString:String = realDate.date.toString();
			var monthInString:String = realDate.month.toString();
			var yearInString:String = realDate.fullyear.toString();
			
			if(dayInString.length == 1){dayInString = "0" + dayInString;}
			
			var realDateInString:String = dayInString + monthInString + yearInString;
			realDate = {date:realDateInString};
			return realDateInString;
		}

		//Formate le mois 0->01(janvier), 1->02(février), ..., 11->12(décembre)
		private function zformateMonth(monthToFormate:int):String
		{
			var month:String = null;
			
			switch (monthToFormate)
			{
					case 0:month = "01";break;
					case 1:month = "02";break;
					case 2:month = "03";break;
					case 3:month = "04";break;
					case 4:month = "05";break;
					case 5:month = "06";break;
					case 6:month = "07";break;
					case 7:month = "08";break;
					case 8:month = "09";break;
					case 9:month = "10";break;
					case 10:month = "11";break;
					case 11:month = "12";break;
			}
			
			return month;
		}
		
		//Cache le bouton 'next' et affiche le bouton 'valider' lorsque que l'on est sur le viewstack 'Information règle'
		private function hiddenBtnNext():void
		{
			if(selectedIndex == 0)
			{btPre.visible = false;}
			else
			{btPre.visible = true;}
			
			btNext.visible = false;
			btNext.width = 0;
			btValid.visible = true;
			btValid.width = 110;
		}
		
		//Cache le bouton 'valider' et affiche le bouton 'next' tant que l'on est pas sur le viewstack 'Information règle'
		private function noHiddenBtnNext():void
		{
			if(selectedIndex == 0 || selectedIndex == -1)
			{btPre.visible = false;}
			else
			{btPre.visible = true;}
			
			btNext.visible = true;
			btNext.width = 110;
			btValid.visible = false;
			btValid.width = 0;
		}

		//Enregistre les modifications apportées à une regle
		private function executeSendRegle():void
		{
			wizzard.myRegle.IDREGLE_ORGA = 0;
			rules.IURulesOrganisation(		wizzard.myRegle.IDREGLE_ORGA,
											wizzard.myRegle.REGLE_NOM,
											wizzard.myRegle.TYPE_REGLE,
											wizzard.myRegle.REGLE_COMMENT,
											wizzard.myRegle.IDSOURCE,
											wizzard.myRegle.IDCIBLE,
											zFormatBoolean(true),
											zFormatBoolean(wizzard.myRegle.BOOL_SUIVRE_AUTO));
			
			rules.addEventListener("envoiRegle",executeSendException);
		}
		
		//Recupere l'IDREGLE_ORGA et envoie les exception de la règle ou execute directement la regle
		private function executeSendException(event:Event):void
		{
			wizzard.myRegle.IDREGLE_ORGA = rules.idRegleSend;
			
			if(wizzard.myRegle.LIST_EXCEPTION != null)
			{	
				if(wizzard.myRegle.LIST_EXCEPTION.length !=0)
				{
					sendException();
					rules.addEventListener("IUOrgaException",executeSendExecuteNow);
				}
				else
				{ executeSendExecuteNowWhenNoException(); }
			}
			else
			{ executeSendExecuteNowWhenNoException(); }
		}
		
		//Mise en forme d'un tableau contenant l'ID des exception sélectionnée
		private function sendException():void
		{
			if(wizzard.myRegle.LIST_EXCEPTION != null)
			{
				if(wizzard.myRegle.LIST_EXCEPTION.length !=0)
				{
					var ListExcep:Array = new Array();
					var obj:Object = wizzard.myRegle.LIST_EXCEPTION;

					for(var i:int = 0;i<obj.length;i++)
					{ ListExcep[i] = obj[i].VALUE.toString(); }
				
					rules.IURulesOrganisationException(wizzard.myRegle.IDREGLE_ORGA,ListExcep.length,1,ListExcep);
				}
			}
		}
		
		//Exécuter la règle le soir même si aucune exception n'a été selectionnée
		private function executeSendExecuteNowWhenNoException():void
		{
			rules.executeRules(wizzard.myRegle.IDREGLE_ORGA);
			rules.addEventListener("executed",viewMessage);
			//viewNewMessage();
		}
		
		//Exécuter la règle le soir même si une exception a été selectionnée
		private function executeSendExecuteNow(event:Event):void
		{
			rules.executeRules(wizzard.myRegle.IDREGLE_ORGA);
			rules.addEventListener("executed",viewMessage);
			//viewMessage(event);
		}
		
		//Affiche le message de confirmation de validation de la règle
		private function viewMessage(event:Event):void
		{
			ConsoviewAlert.afficherOKImage("Votre demande sera prise en compte et s'exéctuera dans les 24heures.",this);
			//Alert.show("Votre règle a été enregistrée.\nElle sera prise en compte par traitement dans les 24heures.","CONSOVIEW");
			dispatchEvent(new Event("regleCreated",true)); 
			PopUpManager.removePopUp(this);
			
		}
		
		//Affiche le message de confirmation de validation de la règle
		private function viewNewMessage():void
		{
			//Alert.show("Votre règle a été enregistrée.\nElle sera prise en compte par traitement dans les 24heures.","CONSOVIEW");
			ConsoviewAlert.afficherOKImage("Votre demande sera prise en compte et s'exéctuera dans les 24heures.",this);
			dispatchEvent(new Event("regleCreated",true));
			PopUpManager.removePopUp(this); 
		}

		//Format un int en bool 0->false, 1->true
		private function zFormatBoolean(bool:Boolean):int
		{
			if(bool)return 1;
			else return 0;
		}
		
//METHODES PRIVATE----		
		
	}
}