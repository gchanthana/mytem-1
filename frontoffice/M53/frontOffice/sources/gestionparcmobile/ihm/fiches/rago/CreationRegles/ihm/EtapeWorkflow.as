package  univers.parametres.rago.CreationRegles.ihm
{
	import flash.events.MouseEvent;
	
	import mx.containers.Box;
	import mx.controls.Button;
	import mx.events.FlexEvent;
	
	import univers.parametres.rago.CreationRegles.event.WizardEvent;
	import univers.parametres.rago.CreationRegles.system.Wizzard;
	import univers.parametres.rago.entity.Regle;
	
	
	[Event(name="goNextEtape",type="univers.parametres.rago.wizard.event.WizardEvent")]
	[Event(name="goPreviousEtape",type="univers.parametres.rago.wizard.event.WizardEvent")]
	[Event(name="annulerCreationRegle",type="univers.parametres.rago.wizard.event.WizardEvent")]
	
	[Bindable]
	public class EtapeWorkflow extends Box
	{
		
		private var _wizzard:Wizzard = new Wizzard();
		private var _myRegle:Regle;
		private var _noeudSource:Object;
		private var _noeudCible:Object;
						
		public var btPre:Button;
		public var btCancel:Button;
		public var btNext:Button;
	 
			
		public function EtapeWorkflow()
		{	 
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, _localeCreationCompleteHandler);
		}				

		protected function valider():void
		{	
//			dispatchEvent(new WizardEvent(wizzard.myRegle,WizardEvent.GO_NEXT_ETAPE,true));
		}
		
		protected function init():void
		{
			addEventListener(MouseEvent.CLICK, _localeClickHandler);
		}
			
//----------------------------------- HANDLERS -------------------------------------------------------------/		
	
		protected function btCancelClickHandler(event:MouseEvent):void
		{
//			dispatchEvent(new WizardEvent(wizzard.myRegle,WizardEvent.ANNULER_CREATION_REGLE));	
		}

		protected function btPreClickHandler(event:MouseEvent):void
		{
//			dispatchEvent(new WizardEvent(wizzard.myRegle,WizardEvent.GO_PREVIOUS_ETAPE));
		}

		protected function btNextClickHandler(event:MouseEvent):void
		{
			valider();
		}

		protected function _localeCreationCompleteHandler(event:FlexEvent):void
		{
		 	callLater(init);
		}

		protected function _localeClickHandler(event:MouseEvent):void
		{
			try { this[event.target.id + "ClickHandler"](event); }
			catch(error:Error) { }
		}

		public function set wizzard(value:Wizzard):void
		{
			_wizzard = value;
		}

		public function get wizzard():Wizzard
		{
			return _wizzard;
		}
	}
}