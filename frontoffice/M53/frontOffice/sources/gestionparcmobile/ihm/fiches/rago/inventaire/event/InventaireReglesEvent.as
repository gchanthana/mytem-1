package univers.parametres.rago.inventaire.event
{
	import flash.events.Event;
	
	import univers.parametres.rago.entity.Regle;

	public class InventaireReglesEvent extends Event
	{
		public static const ACTIVER_REGLE_CLICKED:String="activerRegleClicked";
		public static const DESACTIVER_REGLE_CLICKED:String="desactiverRegleClicked";
		public static const ANNULER_REGLE_CLICKED:String="annulerRegleClicked";
		public static const VOIR_REGLE_CLICKED:String="voirRegleClicked";
		public static const EXECUTER_REGLE_CLICKED:String="executerRegleClicked";
		public static const CREER_REGLE_CLICKED:String="creerRegleClicked";
		public static const ATTRIBUT_IN_DATAGRID:String="attributInDatagrid";
		public static const REFRESH_DATAGRID:String="refreshNow";
		
		private var _regle:Regle;
		
		public function InventaireReglesEvent(__regle:Regle, type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this._regle = __regle;
		}
		
		override public function clone():Event
		{ 
			return new InventaireReglesEvent(this._regle,type,bubbles,cancelable);
		}
		
		
		public function get regle():Regle
		{
			return _regle;
		}
	}
}