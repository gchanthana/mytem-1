package univers.parametres.rago.entity
{
	import mx.collections.HierarchicalData;
	
	public class Orga
	{
		private var _idorganisation:int;
		private var _libelleOrganisation:String;
		private var _hasRegle:Boolean;
		
		public function Orga()
		{
		}

		public function set idorganisation(value:int):void
		{
			_idorganisation = value;
		}

		public function get idorganisation():int
		{
			return _idorganisation;
		}

		public function set libelleOrganisation(value:String):void
		{
			_libelleOrganisation = value;
		}

		public function get libelleOrganisation():String
		{
			return _libelleOrganisation;
		}

		public function set hasRegle(value:Boolean):void
		{
			_hasRegle = value;
		}

		public function get hasRegle():Boolean
		{
			return _hasRegle;
		}		
	}
}