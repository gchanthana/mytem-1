package contrat.ihm
{
	import commandemobile.entity.Commande;
	import commandemobile.system.ArticleEvent;
	import commandemobile.system.CommandeEvent;
	import commandemobile.system.GestionOptionsMobile;
	import commandemobile.system.GestionPanierMobile;
	
	import composants.tb.effect.EffectProvider;
	import composants.util.ConsoviewUtil;
	import composants.util.article.Article;
	import composants.util.contrats.Contrat;
	import composants.util.lignes.LignesUtils;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.commande.ihm.CreerArticleImpl;
	import univers.inventaire.commande.ihm.SelectionAbosOptionsIHM;
	import univers.inventaire.commande.ihm.SelectionBaseViewImpl;
	import univers.inventaire.commande.ihm.SelectionCompteIHM;
	import univers.inventaire.commande.ihm.ValiderCommandeIHM;


	[Bindable]
	public class GererOptionsAbonnementImpl extends CreerArticleImpl
	{
		
		
		
	    
	    
		private 	var _contrat:Contrat;
		protected 	var _gestionAbosOptions:GestionOptionsMobile;
		private 	var _lignesUtils : LignesUtils = new LignesUtils();
			
		override protected function commitProperties():void
		{	
			super.commitProperties();
					
			if(article != null)
			{	 
				cboEngagementRes.selectedIndex = ConsoviewUtil.getIndexByLabel(cboEngagementRes.dataProvider as ArrayCollection,"value",article.dureeEngagementRessources.toString());
				lignesUtils.listeNumerosValides.push(article.sousTete.toString());	
			}			
		}
		
		public function GererOptionsAbonnementImpl(idContrat:Number = 0)
		{ 	
			super();
			
			articleCommande = new Article();
			addEventListener(FlexEvent.CREATION_COMPLETE, _localeCreationCompleteHandler);
		}
		
		///////////////////////////////////// HANDLERS /////////////////////////////////////////////////////////////////////
		
		protected function txtFiltreRessourcesChangeHandler(event:Event): void
	    {		
	    	if(dgListeRessources.dataProvider)
	    	{	
	    		(dgListeRessources.dataProvider as XMLListCollection).filterFunction = listeRessourcesFilterFunction;
	    		(dgListeRessources.dataProvider as XMLListCollection).refresh();
	    	}
	    }
	  	
	  	override protected function btAjouterRessourcesClickHandler(event:MouseEvent):void
	  	{
	  		removePopUp();
			_popUp = new SelectionAbosOptionsIHM();
			SelectionAbosOptionsIHM(_popUp).article = article;
			SelectionAbosOptionsIHM(_popUp).articleCommande = articleCommande;
			SelectionAbosOptionsIHM(_popUp).commande = commande;
			SelectionAbosOptionsIHM(_popUp).addEventListener(SelectionBaseViewImpl.AJOUTER_CLICKED,ressourceAddedHandler);
			_popUp.x = 50;
			_popUp.y = 100;			 			 
			PopUpManager.addPopUp(_popUp,this,true);  
	  	}
	  	
	  	override protected function btRetirerRessourcesClickHandler(event:MouseEvent):void
		{
			articleCommande.addXMLRessource(ObjectUtil.copy(dgListeRessources.selectedItem) as XML);	
			article.removeRessource(XML(dgListeRessources.selectedItem));						
			
			
			txtFiltreRessources.text = "";
		}
		
		protected function btChoisirCompteClickHandler(event:MouseEvent): void
	    {
	    	 removePopUp();
			_popUp = new SelectionCompteIHM();
			SelectionCompteIHM(_popUp).commande = commande;
			PopUpManager.addPopUp(_popUp,this,true);
			PopUpManager.centerPopUp(_popUp);    	
	    }
		
		override protected function btValiderClickHandler(event:MouseEvent):void
		{
			var boolRessources:Boolean = false;
			var boolEquipement:Boolean = false;
			var message:String="\n";
			/* 
			if(XMLList(article.article.equipements.equipement).length() >0)			
			{
				boolEquipement = true
			}
			else
			{
				boolEquipement = false
			} */
			
			if(XMLList(article.article.ressources.ressource).length() >0)
			{
				boolRessources = true
			}
			else
			{	
				boolRessources = false	
			}
				
			if (/* boolEquipement || */ boolRessources)
			{
				article.sousTete = txtNumLigne.text;
				article.codeInterne = txtCodeInterne.text;
				
				trace(articleCommande.article);				
				if(cboEngagementRes.selectedItem != null)
				{
					article.libelleContratRes = "Contrat d'abonnement";
					article.dureeEngagementRessources = Number(cboEngagementRes.selectedItem.value);	
				}
				else
				{
					article.dureeEngagementRessources = -1;
				}
				
				
				
				if(boolCreate)
				{
					articleCommande.sousTete = article.sousTete;
					articleCommande.employe = article.employe;
					
					var gestionArticles:GestionPanierMobile = new GestionPanierMobile();
					gestionArticles.addItem( articleCommande );
					
					removePopUp();
					_popUp = new ValiderCommandeIHM();
					
					ValiderCommandeIHM(_popUp).commande = commande;
					ValiderCommandeIHM(_popUp).gestionArticles = gestionArticles;
					ValiderCommandeIHM(_popUp).addEventListener(CommandeEvent.COMMANDE_CREATED,commandeMobileSAvedHandler);
					PopUpManager.addPopUp(_popUp,this,true);
					PopUpManager.centerPopUp(_popUp);	
				}
				else
				{
					dispatchEvent(new ArticleEvent(article,ArticleEvent.ARTICLE_UPDATED));
				}
				PopUpManager.removePopUp(this);
			}
			else
			{
				Alert.show("Vous devez sélectionner des produits","Erreur");
			}
			
		}
		
		
		protected function txtNumLIgneChangeHandler(event:Event):void
		{
			if(ConsoviewUtil.isPresent(txtNumLigne.text,lignesUtils.listeNumerosValides))
			{
				lignesUtils.boolOK = true;
				btVerifierNumLigne.visible = false;
				btVerifierNumLigne.width = 0;
				return;
			}
			else
			{
				lignesUtils.boolOK = false;
				btVerifierNumLigne.visible = true;
				btVerifierNumLigne.width = 80;
				EffectProvider.FadeThat(btVerifierNumLigne);	
			}
						
		}
		
		protected function btVerifierNumLigneClickHandler(event:Event):void
		{
			if(txtNumLigne.text!="")
			{
				btValider.enabled = false;
				lignesUtils.verifierUniciteNumeroLigne(txtNumLigne.text);
			}
		}
		
		//////////// ACCESSOR ///////////////////////////////////////////////////////////////////////////
		
		
		private function commandeMobileSAvedHandler(event:CommandeEvent):void
		{
			removePopUp();
			PopUpManager.removePopUp(this);	
		}
		
		public function set gestionAbosOptions(value:GestionOptionsMobile):void
		{
			_gestionAbosOptions = value;
		}

		public function get gestionAbosOptions():GestionOptionsMobile
		{
			if(_gestionAbosOptions == null) _gestionAbosOptions = new GestionOptionsMobile();
			return _gestionAbosOptions;
		}

		protected function _localeCreationCompleteHandler(event:FlexEvent):void
		{
					
			if(boolCreate)
			{
				if(contrat != null && contrat.IDSOUS_TETE > 0)
				{
					//_gestionAbosOptions.genererCommandeFromContratAbo(contrat.IDCONTRAT);	
					_gestionAbosOptions.genererCommandeFromLigne(contrat.IDSOUS_TETE);
				}
			}
			else
			{
				 
				gestionAbosOptions.contrat = new Contrat();
				
				gestionAbosOptions.genererCommandeFromLigne(article.article.idsoustete);
			}
			
		}

		public function set listeResourcesLigne(value:ArrayCollection):void
		{
			_gestionAbosOptions.listeResourcesLigne = value;
		}

		public function get listeResourcesLigne():ArrayCollection
		{
			return _gestionAbosOptions.listeResourcesLigne;
		}

		public function set contrat(value:Contrat):void
		{
			_contrat = value;
			gestionAbosOptions.contrat = value;
		}

		public function get contrat():Contrat
		{
			return _contrat;
		}

		override public function set article(value:Article):void
		{
			_article = value;
			gestionAbosOptions.article = article;
		}
		
		public function set commande(value : Commande):void
	    {
	    	_commande = value;
	    	gestionAbosOptions.commande = _commande;
	    }
	    
	    public function get lignesUtils():LignesUtils
		{
			return _lignesUtils;
		}
	    
	    
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		protected function listeRessourcesFilterFunction(item:Object):Boolean
	    {	
	    	
		    var rfilter:Boolean = true;
		    var node:XML = XML(item);
		    
		    // Filtre sur le libelle des équipements
		    if (node.libelle != undefined)
		    {  
		        rfilter = rfilter && (String(node.libelle ).toLowerCase().search(txtFiltreRessources.text.toLowerCase()) != -1);
		    }
		    
		    // Filtre sur le libelle du type d équipements
		    if (node.theme != undefined)
		    {  
		        rfilter = rfilter && (String(node.theme).toLowerCase().search(txtFiltreRessources.text.toLowerCase()) != -1);
		    }
		    
		    // Filtre sur le prix
		    if (node.prix != undefined)
		    {  
		        rfilter = rfilter && (String(node.prix).toLowerCase().search(txtFiltreRessources.text.toLowerCase()) != -1);
		    }		    
		    return rfilter;
	    }

		 
	}
}