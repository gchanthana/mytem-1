package composants.access {
	import flash.events.Event;

	public class ListePerimetreWindowEvent extends Event {
		public static const GROUP_LIST_AFFECTED:String = "GROUP LIST AFFECTED"; // Liste des groupes racines stockée dans ListePerimetreWindow
		/*
		public static const IHM_READY:String = "ListePerimetreWindow IHM READY"; // IHM prêt pour interaction utilisateur
		*/
		/*
		public static const GROUPE_CHANGED:String = "List Group Changed";
		public static const LIST_PERIMETRE_CHANGED:String = "List Perimetre Changed";
		public static const IHM_UNIVERS_CHANGED:String = "IHM Univers Changed";
		public static const IHM_FAMILLE_CHANGED:String = "IHM Famille Changed";
		public static const CANCEL_PERIMETRE:String = "Annulation Window Périmetre";

		private var dataObject:Object;

		
		public function ListePerimetreWindowEvent(type:String,dataObject:Object) {
			super(type);
			this.dataObject = dataObject;
		}

		public function getDataObject():Object {
			return dataObject;
		}
		*/
		public function ListePerimetreWindowEvent(type:String) {
			super(type);
		}
	}
}
