package composants.util.produits
{
	import composants.util.ConsoviewUtil;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class ProduitsUtils
	{
		public static const THEME_ABOSVOIX:int=41;
		public static const THEME_ABOSDATA:int=54;
		public static const THEME_ABOSPUSHMAIL:int=65;
		public static const THEME_OPTSVOIX:int=56;
		public static const THEME_OPTSDATA:int=49;
		public static const THEME_OPTSAUTRE:int=52;
		public static const THEME_ACCESSOIRES:int=59;
		public static const THEME_LOCATIONSTERMINAUX:int=58;
		
		private static const CFC_ProduitsUtils:String = "fr.consotel.consoview.inventaire.produits.ProduitsUtils";
		
		private var _listeProduitsCatalogue : ICollectionView;
		public function get listeProduitsCatalogue():ICollectionView{
			return _listeProduitsCatalogue;
		}
		
		public function set listeProduitsCatalogue(values:ICollectionView):void{
			_listeProduitsCatalogue = values;	
		}
		
		private var _listeThemes: ICollectionView;
		public function get listeThemes():ICollectionView{
			return _listeThemes;
		}
		
		public function set listeThemes(values:ICollectionView):void{
			_listeThemes = values;	
		}
		
		private var _selectedTheme:Theme;
		public function set selectedTheme(value:Theme):void
		{
			_selectedTheme = value;
		}
		
		public function get selectedTheme():Theme
		{
			return _selectedTheme	
		}
		
		
		
		public function ProduitsUtils()
		{
		}
		
		protected function fillListeThemes( source : ICollectionView):ICollectionView{
			var newCollection : ICollectionView = new ArrayCollection();
			
			if (source != null)
			{	
				var cursor : IViewCursor = source.createCursor();
				 
				var sort:Sort = new Sort();   
    			sort.fields = [new SortField("THEME_LIBELLE", true)];
    			source.sort = sort;
    			source.refresh();
    			var tabIdTheme:Array=[];
				while(!cursor.afterLast)
				{
					if (!ConsoviewUtil.isPresent(cursor.current.IDTHEME_PRODUIT.toString(),tabIdTheme)){
						var obj : Theme = new Theme();				
						obj.THEME_LIBELLE = cursor.current.THEME_LIBELLE;
						obj.IDTHEME_PRODUIT = cursor.current.IDTHEME_PRODUIT;						
						tabIdTheme.push(obj.IDTHEME_PRODUIT);
						(newCollection as ArrayCollection).addItem(obj);
					}
					cursor.moveNext();
				}
			}					
			return newCollection;
		}
		
		/**
		*	Fournit la liste des produit du catalogue opérateur
		*	met à jour listeProduitsCatalogue
		*	met à jour listeThemes
		*	
		*	@param idOperateur
		**/
		public function fournirListeProduitCatalogueOperateur(idOperateur:Number):void{
			var opTheme : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				CFC_ProduitsUtils,
																				"fournirListeProduitsCatalogueOperateur",
																				fournirListeProduitCatalogueOperateurResultHandler,
																				null);				
			RemoteObjectUtil.callService(opTheme,idOperateur);
		}
		
		protected function fournirListeProduitCatalogueOperateurResultHandler(event :ResultEvent):void{
			listeProduitsCatalogue = event.result as ArrayCollection;
			listeProduitsCatalogue.refresh();
			//listeThemes = fillListeThemes(listeProduitsCatalogue);
		}
		
		
		/**
		*	Ajoute un produit catalogue à mes favoris
		*	met à jour listeProduitsCatalogue
		*	@param Object avec propriété public IDPRODUIT_CATALOGUE
		**/
		public function ajouterProduitAMesFavoris(value:Object):void{
			if (!value.hasOwnProperty("IDPRODUIT_CATALOGUE")) return;
			if (value.IDPRODUIT_CATALOGUE < 1) return;
			
			var op : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				CFC_ProduitsUtils,
																				"ajouterProduitAMesFavoris",
																				function ajouterProduitAMesFavorisResultHandler(event:ResultEvent):void
																				{
																					if(event.result > 0)
																					{
																						value.FAVORI = 1;
																						
																					}
																					else
																					{
																						value.FAVORI = 0;
																						Alert.show("Erreur : " + event.result.toString(),"Erreur");	
																					}
																					
																					listeProduitsCatalogue.itemUpdated(value);
																				},
																				null);				
			RemoteObjectUtil.callService(op,value.IDPRODUIT_CATALOGUE);
		}
		
		/**
		*	Ajoute un produit catalogue à mes favoris
		*	met à jour listeProduitsCatalogue
		*	@param Object avec propriété public IDPRODUIT_CATALOGUE
		**/
		public function supprimerProduitDeMesFavoris(value:Object):void{
			if (!value.hasOwnProperty("IDPRODUIT_CATALOGUE")) return;
			if (value.IDPRODUIT_CATALOGUE < 1) return;
			
			var op : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				CFC_ProduitsUtils,
																				"supprimerProduitDeMesFavoris",
																				function supprimerProduitDeMesFavorisResultHandler(event:ResultEvent):void
																				{
																					if(event.result > 0)
																					{
																						value.FAVORI = 0;
																						
																					}
																					else
																					{
																						value.FAVORI = 1;
																						Alert.show("Erreur : " + event.result.toString(),"Erreur");	
																					}
																					
																					listeProduitsCatalogue.itemUpdated(value);
																				},
																				null);				
			RemoteObjectUtil.callService(op,value.IDPRODUIT_CATALOGUE);
		}
		
	}
}


	
