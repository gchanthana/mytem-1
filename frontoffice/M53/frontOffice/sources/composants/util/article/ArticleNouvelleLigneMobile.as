package composants.util.article
{
	import composants.util.ConsoviewFormatter;
	import composants.util.equipements.EquipementsUtils;
	
	import mx.binding.utils.ChangeWatcher;
	import mx.utils.ObjectUtil;
	
	[Bindable]
	public class ArticleNouvelleLigneMobile extends Article
	{
		protected static const MOBILE:Number = 70;
	    protected static const SIM:Number = 71;
		
		private var equipementsUtils:EquipementsUtils = new EquipementsUtils();
		public var changewatcher:ChangeWatcher;
		
		private var _carteSim:Object = null;
		public function creerCarteSim(idrevenduer:Number,idOperateur:Number):void
		{
			if(idrevenduer > 0)
			{	
				changewatcher = ChangeWatcher.watch(this.equipementsUtils, "selectedEquipement", creerCarteSimHandler);
				equipementsUtils.fournirCarteSim(idrevenduer,idOperateur);
			}
		}
		
		protected function creerCarteSimHandler(event:Event):void
		{
			if(changewatcher != null) changewatcher.unwatch();
			
			_carteSim = equipementsUtils.selectedEquipement;
			addEquipement(_carteSim,null);			
		}
		//Au moins un produit et un equipement carte sim
		public function ArticleNouvelleLigneMobile(article:XML=null)
		{
			super(article);			
		}
		
		
		//SI EQ = SIM alors Impossible de supprimer l'element
		override public function removeEquipement(value:Object):void
		{
			var len:Number = XMLList(article.equipements.equipement).length();
			var obj:Object = XMLList(article.equipements.equipement);
			
			if(len > 1 && String(value.type_equipement).toUpperCase() == "MOBILE")
			{
				//on met à jour idequipement parents chez les autres équipements
				updateIdEquipementsParent(0);
			}
			
			super.removeEquipement(value);
					
			/* if(len > 1 && String(obj.type).toUpperCase() != "CARTE SIM")
			{
				super.removeEquipement(value);
			}
			else
			{
				Alert.show("L'article doit contenir au moins une carte sim")
			} */
		}
		
		private function formatTotal(totalNbr:Number):String
		{
			var strg0:String = "0.00";
			if(totalNbr > 0.0000001)
			{
				var strg:String = totalNbr.toString();
				var strg1:String = "";
				var strg2:String = "";
				var strg3:String = "";
				var strg4:String = "";
				var nbr:Number = -1;
				var len:Number = -1;
				var price:Number = 0;
																			
				len = strg.length;	
				nbr = strg.lastIndexOf(".",0);
																			
				if(totalNbr.toString().toLowerCase().indexOf(".") == -1)
				{
					strg0 = strg + ".00";
				}
				else
				{
					nbr = totalNbr.toString().toLowerCase().lastIndexOf(".");
					strg1 = strg.substring(0,nbr);
					strg3 = strg.substring(nbr+1,len);
					if(strg3.length > 2)
					{
						strg4 = strg.substring(nbr+3,nbr+4);
						if(Number(strg4) > 4)
						{
							price = Number(strg3)+1;
							strg2 = price.toString().substr(0,2);
						}
						else
						{
							strg2 = strg.substring(nbr+1,nbr+3);
						}
					}
					else
					{
						strg2 = strg.substring(nbr+1,nbr + 3);
						if(strg2.length < 2)
						{
							strg2 = strg2 + "0";
						}
					}
																				
						strg0 = strg1 + "." + strg2;
					}
				}
				return strg0;
		}
		
		/**
		 * Ajoute un equipement pour l'article
		 * si il y a déjà un mobile dans l'article, alors on le remplace par celui sélectionné
		 * @param value les attributs suivant sont obligatoire 
		 * 			   TYPE_EQUIPEMENT   		MOBILE|CARTE SIM|...
		 * 			   IDEQUIPEMENT_FOURNISSEUR		NUMBER
		 * 			   IDEQUIPEMENT_PARENT		NUMBER
		 * 			   LIBELLE_EQUIPEMENT		STRING
		 * 			   MODELE_EQUIPEMENT		STRING
		 * 			   PRIX						NUMBER
		 * 			   COMMENTAIRES				NUMBER	
		 * 			   [BONUS]					NUMBER
		 * @return code (1 si ok sinon -1) 
		 * */
		override public function addEquipement(value : Object,price:Object):Number
		{	
			//Si il y a déjà un mobile dans l'article on le supprime avant d'ajouter le nouveau mobile
			if(value == null) return -1;
			var prix:String = null;

		    if(value.IDTYPE_EQUIPEMENT == MOBILE)
			{
				//on met à jour idequipement parents chez les autres équipements	
				var equipementsMobile:XML = article.equipements.equipement.(type_equipement.toString().toLowerCase() == "mobile")[0];
				trace(ObjectUtil.toString(equipementsMobile));
				//il y a un équipement de type mobile dans l'article
				if (equipementsMobile)
				{
					//on supprime le mobile
					removeEquipement(equipementsMobile);
				}
				
				updateIdEquipementsParent(value.IDEQUIPEMENT_FOURNISSEUR);
			}
				
			if(value.IDTYPE_EQUIPEMENT != 0)
			{	
				prix = formatTotal(Number(value.PRIX_CATALOGUE));
				
				var prixToString:String = prix.replace(",",".");	
			    						
				var equipement : XML = <equipement></equipement>;
				
				equipement.appendChild(<idarticle_equipement></idarticle_equipement>);
				equipement.appendChild(<idequipementfournisparent></idequipementfournisparent>);
				equipement.appendChild(<idequipementfournis>{value.IDEQUIPEMENT_FOURNISSEUR}</idequipementfournis>);			
				equipement.appendChild(<idequipement></idequipement>);
				equipement.appendChild(<idtype_equipement>{value.IDTYPE_EQUIPEMENT}</idtype_equipement>);
				equipement.appendChild(<code_ihm>{value.TE_CODE_IHM}</code_ihm>);
				equipement.appendChild(<codepin></codepin>);
				equipement.appendChild(<codepuk></codepuk>);
				if(value.IDEQUIPEMENT_FOURNISSEUR)
				{
					equipement.appendChild(<idequipementclient>{value.IDEQUIPEMENT_FOURNISSEUR}</idequipementclient>);	
				}
				else
				{
					equipement.appendChild(<idequipementclient/>);
				}
				equipement.appendChild(<numeroserie></numeroserie>);			
				equipement.appendChild(<libelle>{value.LIBELLE_EQ}</libelle>);
				equipement.appendChild(<type_equipement>{value.TYPE_EQUIPEMENT}</type_equipement>);
				equipement.appendChild(<modele></modele>);
				equipement.appendChild(<prix>{(prix!=null)?prixToString:0}</prix>);
				equipement.appendChild(<prixAffiche>0</prixAffiche>);
				equipement.appendChild(<bonus>{(value.BONUS>0)?value.BONUS:0}</bonus>);	
				equipement.appendChild(<commentaire>{value.COMMENTAIRES}</commentaire>);
				equipement.appendChild(<contrats></contrats>);
				equipements.appendChild(equipement);
				
				calculTotal();
				trace(article.toXMLString());
			}
			return 1;
		}
		
						
		override public function removeRessource(value:Object):void
		{
			var len:Number = XMLList(article.ressources.ressource).length();
			var obj:Object = XMLList(article.ressources.ressource);
			
			/* if(len > 1)
			{ */
				super.removeRessource(value);
			/* }
			else
			{
				Alert.show("L'article doit contenir au moins un produit")
			} */
		}
		
	}
}