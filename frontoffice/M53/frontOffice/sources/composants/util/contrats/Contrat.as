package composants.util.contrats
{
	[RemoteClass(alias="fr.consotel.consoview.inventaire.contrats.Contrat")]

	[Bindable]
	public class Contrat
	{

		public var IDCONTRAT:Number = 0;
		public var IDTYPE_CONTRAT:Number = 0;
		public var IDFOURNISSEUR:Number = 0;
		public var REFERENCE_CONTRAT:String = "";
		public var DATE_SIGNATURE:Date = null;
		public var DATE_ECHEANCE:Date = null;
		public var COMMENTAIRE_CONTRAT:String = "";
		public var MONTANT_CONTRAT:Number = 0;
		public var DUREE_CONTRAT:Number = 0;
		public var TACITE_RECONDUCTION:Number = 0;
		public var DATE_DEBUT:Date = null;
		public var PREAVIS:Number = 0;
		public var DATE_DENONCIATION:Date = null;
		public var IDCDE_CONTACT_SOCIETE:Number = 0;
		public var LOYER:Number = 0;
		public var PERIODICITE:Number = 0;
		public var MONTANT_FRAIS:Number = 0;
		public var NUMERO_CLIENT:String = "";
		public var NUMERO_FOURNISSEUR:String = "";
		public var NUMERO_FACTURE:String = "";
		public var CODE_INTERNE:String = "";
		public var BOOL_CONTRAT_CADRE:Number = 0;
		public var BOOL_AVENANT:Number = 0;
		public var ID_CONTRAT_MAITRE:Number = 0;
		public var FRAIS_FIXE_RESILIATION:Number = 0;
		public var MONTANT_MENSUEL_PEN:Number = 0;
		public var DATE_RESILIATION:Date = null;
		public var PENALITE:Number = 0;
		public var DESIGNATION:String = "";
		public var DATE_RENOUVELLEMENT:Date = null;
		public var OPERATEURID:Number = 0;
		public var IDCOMPTE_FACTURATION:Number = 0;
		public var IDSOUS_COMPTE:Number = 0;
		public var NUMERO_CONTRAT:String = "";
		public var IDRACINE:Number = 0;
		public var DATE_ELLIGIBILITE:Date = null;
		public var DUREE_ELLIGIBILITE:Number = 0;
		public var SOUS_TETE:String = "";
		public var IDSOUS_TETE:Number = 0;


		public function Contrat()
		{
		}

	}
}