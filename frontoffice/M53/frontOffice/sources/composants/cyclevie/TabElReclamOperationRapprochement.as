package composants.cyclevie
{
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	import composants.util.ConsoviewFormatter;
	import mx.controls.dataGridClasses.DataGridColumn;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;
	import mx.controls.Alert;
	import composants.util.DateFunction;
	
	public class TabElReclamOperationRapprochement extends TabElFactOperationRapprochementIHM
	{
		[Bindable]
		private var tabListeProdReclam : ArrayCollection = new ArrayCollection();	
		private var tabSelectedItems : Array;
		
		private var totalOp : Number ;
		private var totalFact : Number;
		private var totalDiff : Number;
		private var _titre : String = "";
		
		public function TabElReclamOperationRapprochement()
		{
			//TODO: implement function
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);			
		}
		/*
		/** 
		 * Ajoute un Item dans le grid de réclamtion
		 * @param itemGridProduitOp Une ligne du grid des produits des operations 
		 * @param itemGridProduitFact Une ligne du grid des éléments de la facturation
		 * 
		public function ajouterItem(itemGridProduitOp : Object, itemGridProduitFact : Object):Boolean{
			if (isInGridProduit(itemGridProduitOp,tabListeProdReclam)){
				Alert.show("Produit déja en réclame");
				return false;				
			}else{
				tabListeProdReclam.addItem(formateData(itemGridProduitOp,itemGridProduitFact));
				tabListeProdReclam.refresh();	
				return true;
			}
							
		}*/
		 /**
		 * Retourne les donnée du grid
		 * @return les données sous forme de tableau
		 * */
		 public function get Donnees():Array{
		 	
		 	return tabListeProdReclam.source;
		 }
		
		/**
		 * Les donnees pour le Grid
		 * @param donnees  Les données por le grid
		 * */		
		public function setData(donnees : ArrayCollection):void{
			try{
				tabListeProdReclam = donnees;
				tabListeProdReclam.refresh();
				totalFact = calculTotal(tabListeProdReclam,"MONTANT");
				myGridFacturation.dataProvider = tabListeProdReclam;
				DataGridColumn(myFooterGrid.columns[1]).headerText = ConsoviewFormatter.formatEuroCurrency(totalFact,2);
			}catch(e : Error){
				trace("tablisprodreclam n'est pas dipo");
			}
			
		} 
		
		/**
		 * Met le titre
		 * */
		 [Bindable]
		 public function set titre(t : String):void{
		 	_titre = t;
		 	if (leTitre != null) leTitre.text = _titre;
		 }
		 
		 public function get titre():String{
		 	return _titre;
		 }
		 
		//initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{
			tabListeProdReclam.addEventListener(CollectionEvent.COLLECTION_CHANGE,calculeTotaux);
			txtFiltre.addEventListener(Event.CHANGE,filtrerGird);
			myGridFacturation.addEventListener(Event.CHANGE,remplirtabSelectedItems);
			
			myGridFacturation.doubleClickEnabled = true;
			myGridFacturation.addEventListener(MouseEvent.DOUBLE_CLICK,supprimerItems);			
			
			myGridFacturation.dataProvider = tabListeProdReclam;
			DataGridColumn(myGridFacturation.columns[0]).labelFunction = formateDates;
			DataGridColumn(myGridFacturation.columns[1]).labelFunction = formateDates;
			DataGridColumn(myGridFacturation.columns[2]).labelFunction = formateDates;
			DataGridColumn(myGridFacturation.columns[4]).labelFunction = formateLigne;
			DataGridColumn(myGridFacturation.columns[7]).labelFunction = formateEuros;
			DataGridColumn(myFooterGrid.columns[1]).headerText = ConsoviewFormatter.formatEuroCurrency(totalFact,2);
		}
		
	
		//formate les données pour le Gird Reclam--------
		private function formateDates(item : Object, column : DataGridColumn):String{
			var ladate:Date = new Date(item[column.dataField]);
			return DateFunction.formatDateAsShortString(ladate);			
		}
		
		private function formateEuros(item : Object, column : DataGridColumn):String{			
			return ConsoviewFormatter.formatEuroCurrency(item.MONTANT,2);	
			return "";		
		}
		
		private function formateLigne(item : Object, column : DataGridColumn):String{			
			return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);			
		}
		
		
		private function formateData(itemGridProduitOp : Object, itemGridProduitFact : Object):Object{
			
			var itemOp : Object =  itemGridProduitOp;
			var itemFact : Object = itemGridProduitFact;
			var newItem : Object = new Object();
			
			newItem.IDSOUS_TETE = itemOp.IDSOUS_TETE;
			newItem.SOUS_TETE = ConsoviewFormatter.formatPhoneNumber(itemOp.SOUS_TETE);
			
			newItem.OPERATEURID = itemOp.OPERATEURID;
			newItem.OPNOM = itemOp.OPNOM;
			
			newItem.IDINVENTAIRE_PRODUIT_OP = itemOp.IDINVENTAIRE_PRODUIT;
			newItem.LIBELLE_PRODUIT_OP = itemOp.LIBELLE_PRODUIT;
			newItem.ESTIMATION_OP = itemOp.ESTIMATION;
			
			newItem.IDINVENTAIRE_PRODUIT_FACT = itemFact.IDINVENTAIRE_PRODUIT;
			newItem.LIBELLE_PRODUIT_FACT = itemFact.LIBELLE_PRODUIT;
			newItem.MONTANT_FACT = itemFact.MONTANT;
			newItem.IDDETAIL_FACTURE_ABO = itemFact.IDDETAIL_FACTURE_ABO;
			newItem.DIFF = Math.abs(Number(newItem.ESTIMATION_OP)) - Math.abs(Number(newItem.MONTANT_FACT));		
			return newItem;
		}
		//Filtres------------------
		
		private function filtrerGird(ev : Event):void{
			if (myGridFacturation.dataProvider != null){
				(myGridFacturation.dataProvider as ArrayCollection).filterFunction = filtrer;
				(myGridFacturation.dataProvider as ArrayCollection).refresh();
			}
		}
		
		private function filtrer(item : Object):Boolean{
			if ((String(item.SOUS_TETE).toLowerCase().search(txtFiltre.text) != -1)
				||
				(String(item.OPNOM).toLowerCase().search(txtFiltre.text) != -1)
				||
				(String(item.LIBELLE_PRODUIT_OP).toLowerCase().search(txtFiltre.text) != -1)		
				||
				(String(item.ESTIMATION_OP).toLowerCase().search(txtFiltre.text) != -1)		
				||
				(String(item.LIBELLE_PRODUIT_FACT).toLowerCase().search(txtFiltre.text) != -1)
				||
				(String(item.MONTANT_FACT).toLowerCase().search(txtFiltre.text) != -1)
				||
				(String(item.LIBELLE_PRODUIT_FACT).toLowerCase().search(txtFiltre.text) != -1))
			{
				return true;
			}else{
				return false;
			}
		}
		
		//verifier si le produit n'est deja pas en reclam
		private function isInGridProduit( item : Object, a : ArrayCollection):Boolean{			
			var ligne : Object;
			if (item.IDINV_ETAT == 6 )
				return true;
			else{
				
				for(ligne in a){
					if (parseInt(item.IDINVENTAIRE_PRODUIT) == parseInt(a[ligne]["IDINVENTAIRE_PRODUIT_OP"]) ){
						trace(item.IDINVENTAIRE_PRODUIT+" fact "+ a[ligne]["IDINVENTAIRE_PRODUIT_OP"])
					} return true
				}
			}
				
			
			return false;
		}
		
		//fait la somme de la colonne du tab passer en param
		private function calculTotal(d : ArrayCollection,colname : String):Number{
			var ligne : Object;
			var letotal : Number = 0;
			
			for (ligne in d){
				letotal = letotal + Number(d[ligne][colname]); 
			}	
									
			return letotal;	
		}		
		
		//calcule les totaux
		private function calculeTotaux(ce : CollectionEvent):void{			
			totalFact = calculTotal(tabListeProdReclam,"MONTANT");	
			DataGridColumn(myFooterGrid.columns[2]).headerText = ConsoviewFormatter.formatEuroCurrency(totalFact,2);			
		}
		
		//mettre a jour le tableau des item selectionne
		private function remplirtabSelectedItems(ev : Event):void{			
			tabSelectedItems = ev.currentTarget.selectedItems;	
		}
		
		
		//enregistre la reclamation
		private function validerReclamation(me : MouseEvent):void{
			
		}
		
		//enleve les items selectionnés du grid
		private function supprimerItems(me : MouseEvent):void{
			try{				
				var evtObj : SelectionProduitEvent = new SelectionProduitEvent("SuppressionProduitLettre");			
				evtObj.tabProduits = myGridFacturation.selectedItems;
				dispatchEvent(evtObj);
				//tabSelectedItems.forEach(supprimerLigne);					
			}catch( e : Error){
				trace("pas d'elements selectionné");
			}	 
		}
		
		private function supprimerLigne(element:*, index:Number, arr:Array):void{
			//var obj : Object = tabSelectedItems[element];
			var i : int = tabListeProdReclam.getItemIndex(element);
			tabListeProdReclam.removeItemAt(i);			 
			tabListeProdReclam.refresh();			
		}	
				
	}
}