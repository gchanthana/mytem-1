package composants.parametres.perimetres.gabarits
{
	import flash.utils.getDefinitionByName;
	
	public class GabaritFactory
	{
		
		public static const TYPE_COMMANDE_FIXEDATE : String = "CommandeFixeData";
		public static const TYPE_COMMANDE_MOBILE : String = "CommandeMobile";
		public static const TYPE_RESILIATION_FIXEDATE : String = "ResiliationFixeData";
		public static const TYPE_MVT_SOUSTETE : String = "MouvementDeSousTete";
		public static const BLANK : String = "Blank";
		
		static public function getGabarit(idGroupe : Number, infos : Object):String{
			
			Gabarit477;
			var gabarit : AbstractGabarit;
			
			try{
				var myClass : Class = getDefinitionByName("composants.parametres.perimetres.gabarits.Gabarit" + idGroupe.toString()) as Class;
				gabarit = new myClass();
				return gabarit.getGabarit(infos);
			}catch(e : Error){				
				gabarit = new GabaritParDefaut();
			}finally{
				return gabarit.getGabarit(infos);
			}
		}	
		
	}
}