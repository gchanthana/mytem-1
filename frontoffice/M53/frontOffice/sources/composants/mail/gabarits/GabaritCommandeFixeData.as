package composants.mail.gabarits
{
	import commandemobile.entity.Commande;
	import commandemobile.system.Contact;
	import commandemobile.system.ElementCommande;
	import commandemobile.system.Societe;
	
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	public class GabaritCommandeFixeData extends AbstractGabarit
	{
		private var _commande : Commande;
		private var _panier : ElementCommande;
		private var _contact : Contact;
		private var _societe : Societe;
		
		public function GabaritCommandeFixeData()
		{
			super();
		}
		
		override public function getGabarit():String{
			
			_commande = infos.commande;
			_panier = infos.panier;
			_contact = infos.contact;
			_societe = infos.societe;
			
			var listeLignes : String = "";
			if (_panier != null){
				listeLignes = printListeSousTete(_panier.liste.source);
			}
			
			
			
			var cnom : String = (_contact!= null  &&_contact.nom != null )? _contact.nom : " ";
			var cprenom : String = (_contact!= null && _contact.prenom != null )? _contact.prenom : " ";
			var cemail: String = (_contact!= null &&  _contact.email!=null)?_contact.email:" ";
			var csociete : String = (_societe != null && _societe.raisonSociale!=null)?_societe.raisonSociale:" ";
			var cadresse1 : String = (_societe != null  && _societe.adresse1!=null)?_societe.adresse1:" ";
			var czipcode : String = (_societe != null &&_societe.codePsotal!=null)?_societe.codePsotal:" ";
			var ccommune : String = (_societe != null && _societe.commune!=null)?_societe.commune:" ";
			
			var refclient : String = (_commande.REF_CLIENT!=null)?_commande.REF_CLIENT:" ";
			var refOperateur : String = (_commande.REF_OPERATEUR!=null)?_commande.REF_OPERATEUR:" ";
			var dateLivPrevue : String = (_commande.LIVRAISON_PREVUE_LE != null) ? DateFunction.formatDateAsString(new Date(_commande.LIVRAISON_PREVUE_LE)) : " " ;
			var commentaires : String = (_commande.COMMENTAIRES!=null)?_commande.COMMENTAIRES:" ";
			var compte : String = infos.compte;
			var sousCompte : String = infos.sousCompte;
			
			return	"<br/>"
					+"<p>Objet : Commande</p>"
					+"<br/>"
					+"<b>"+infos.NOM_EXPEDITEUR+" "+infos.PRENOM_EXPEDITEUR +"</b>"
					+"<br/>"+infos.MAIL_EXPEDITEUR
					+"<br/><u>Date</u> : "+DateFunction.formatDateAsString(new Date())
					+"<br/>Société : <b>"+infos.SOCIETE_EXPEDITEUR+"</b>"
					+"<br/>"	
					+"<br/>"											
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Contact : "+"</b>"	
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>"+cnom +" "+cprenom +"</b>"
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+cemail
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>"+csociete+"</b>"
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+cadresse1
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+czipcode
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+ccommune
					+"<br/>"
					+"<br/>Libellé de la commande <b>" + _commande.LIBELLE_COMMANDE+"</b>"
					+"<br/>Référence client : <b>" + refclient +"</b>"
					+"<br/>Référence opérateur : <b>" + refOperateur +"</b>"
					+"<br/>Remarque : <b>" + commentaires +"</b>"
					+"<br/>Date de la commande : <b>" + DateFunction.formatDateAsString(new Date(_commande.DATE_COMMANDE))+"</b>"
					+"<br/>Date livraison annoncée : <b>" + dateLivPrevue+"</b>"
					+"<br/>"
					+"<br/> Compte de facturation : <b>"+ compte +"</b>           Sous compte : <b>" + sousCompte+"</b>"
					+"<br/>_______________________________________________________________________________________________"
					+"<br/>"
					+ listeLignes
					+"<br/>_______________________________________________________________________________________________"
					+"<br/>"	
					+"<br/>"	
					+"<br/>"	
					+"<br/>"					
					+"Cordialement,"
					+"<br/>"	
					+"<b>"+infos.NOM_EXPEDITEUR+" "+infos.PRENOM_EXPEDITEUR +".</b>"
		}
					
		
		protected function printListeSousTete(liste : Array):String{
			var output : String = "<br/>";
			if (liste != null){
				for (var i:Number=0; i<liste.length;i++){
					var e : ElementCommande = liste[i] as ElementCommande;					
					var compte : String = "";
					var sousCompte : String = "";
					var produit : String = e.libelleProduit;
					var ligne : String = e.libelleLigne;
					var theme : String = e.libelletheme;
					var comms : String = e.commentaire != null ? e.commentaire: " ";					
					output = output + "<li>Ligne :<b>"+ ConsoviewFormatter.formatPhoneNumber(ligne) 
									+ "</b>\t\t\t   Thème : <b>" 
									+ theme 
									+ "</b>\t\t\t   Produits :<b>" 
									+  produit
									+ "</b>\t\t\t   Commentaire :<b>"
									+  comms
									+ "</b></li>";
				}
			}
			return output;
		}
	}
}