package composants.mail.gabarits
{
	import commandemobile.system.Contact;
	import commandemobile.system.OperationVO;
	import commandemobile.system.Societe;
	
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import mx.collections.ArrayCollection;

	public class GabaritResiliationFixeData extends AbstractGabarit
	{
			 
		private var _listeProduits : ArrayCollection;
		private var _contact : Contact;
		private var _societe : Societe;
		private var _operation : OperationVO;
		
		public function GabaritResiliationFixeData()
		{
			super();
		}
		
		override public function getGabarit():String{
			
			_operation = infos.operation;
			_listeProduits = infos.listeProduitsOperation;
			_contact = infos.contact;
			_societe = infos.societe;
			
			var listeLignes : String = "";
			if (_listeProduits != null){
				listeLignes = printListeSousTete(_listeProduits.source);
			}
			var cnom : String = (_contact!= null  &&_contact.nom != null )? _contact.nom : " ";
			var cprenom : String = (_contact!= null  && _contact.prenom != null )? _contact.prenom : " ";
			var cemail: String = (_contact!= null &&_contact.email!=null)?_contact.email:" ";
			var csociete : String = (_societe != null && _societe.raisonSociale!=null)?_societe.raisonSociale:" ";
			var cadresse1 : String = (_societe != null  && _societe.adresse1!=null)?_societe.adresse1:" ";
			var czipcode : String = (_societe != null &&_societe.codePsotal!=null)?_societe.codePsotal:" ";
			var ccommune : String = (_societe != null && _societe.commune!=null)?_societe.commune:" ";
			
			var refclient : String = (_operation.REFERENCE_INTERNE !=null)?_operation.REFERENCE_INTERNE:" ";
			var refOperateur : String = (_operation.REF_OPERATEUR!=null)?_operation.REF_OPERATEUR:" ";
			//var dateLivPrevue : String = (_commande.dateLivraisonPrevue != null) ? DateFunction.formatDateAsString(new Date(_commande.dateLivraisonPrevue)) : " " ;
			var commentaires : String = (_operation.COMMENTAIRE!=null)?_operation.COMMENTAIRE:" ";
			var compte : String = infos.compte;
			var sousCompte : String = infos.sousCompte;
			
			return	"<br/>"
					+"<p>Objet : Demande de réiliation</p>"
					+"<br/>"
					+"<b>"+infos.NOM_EXPEDITEUR+" "+infos.PRENOM_EXPEDITEUR +"</b>"
					+"<br/>"+infos.MAIL_EXPEDITEUR
					+"<br/><u>Date</u> : "+DateFunction.formatDateAsString(new Date())
					+"<br/>Société : <b>"+infos.SOCIETE_EXPEDITEUR+"</b>"
					+"<br/>"	
					+"<br/>"											
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Contact : "	
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+cnom +" "+cprenom +"</b>"
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+cemail
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>"+csociete+"</b>"
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+cadresse1
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+czipcode
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+ccommune
					+"<br/>"
					+"<br/>Libellé de la demande <b>" + _operation.LIBELLE_OPERATIONS+"</b>"
					+"<br/>Référence client : <b>" + refclient +"</b>"
					+"<br/>Référence opérateur : <b>" + refOperateur +"</b>"
					+"<br/>Remarque : <b>" + commentaires +"</b>"
					+"<br/>Date de la demande : <b>" + DateFunction.formatDateAsString(new Date(_operation.DATE_CREATION))+"</b>"
					//+"<br/>Date livraison annoncée : <b>" + dateLivPrevue+"</b>"
					+"<br/>"
					//+"<br/> Compte de facturation : <b>"+ compte +"</b>           Sous compte : <b>" + sousCompte+"</b>"
					+"<br/>_______________________________________________________________________________________________"
					+"<br/>"
					+ listeLignes
					+"<br/>_______________________________________________________________________________________________"
					+"<br/>"	
					+"<br/>"	
					+"<br/>"	
					+"<br/>"					
					+"Cordialement,"
					+"<br/>"	
					+"<b>"+infos.NOM_EXPEDITEUR+" "+infos.PRENOM_EXPEDITEUR +".</b>"
		}
					
		
		protected function printListeSousTete(liste : Array):String{
			var output : String = "<br/>";
			if (liste != null){
				for (var i:Number=0; i<liste.length;i++){
					var ligne : Object = liste[i];
					
					var compte : String = ligne.COMPTE != null ? ligne.COMPTE  : " ";
					var sousCompte : String = ligne.SOUS_COMPTE != null ? ligne.SOUS_COMPTE :" ";
										
					var produit : String = ligne.LIBELLE_PRODUIT;
					var numligne : String = ligne.SOUS_TETE;
					
										
					output = output + "<li>Ligne :<b>"+ ConsoviewFormatter.formatPhoneNumber(numligne) 
									+  "</b>\t\t   Compte: <b>" 
									+ compte 
									+ "</b>\t\t    Sous compte :<b>" 
									+  sousCompte
									+ "</b>\t\t    Produit :<b>"
									+  produit
									+ "</b></li>";
				}
			}
			return output;
		}
	}
}