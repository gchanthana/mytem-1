package composants.mail.gabarits
{
	import commandemobile.entity.Commande;
	
	import flash.utils.getDefinitionByName;
	
	public class GabaritFactory
	{
		public static const TYPE_COMMANDE_FIXEDATE : String = "CommandeFixeData";
		public static const TYPE_COMMANDE_MOBILE : String = "CommandeMobile";
		public static const TYPE_RESILIATION_FIXEDATE : String = "ResiliationFixeData";
		public static const TYPE_MVT_SOUSTETE : String = "MouvementDeSousTete";
		public static const BLANK : String = "Blank";
		
		public static var _cmd:Commande;
						
		static public function getGabarit(infos : InfosObject, type : String = "Blank",idGroupe : Number = 0):String{
			
			GabaritMouvementDeSousTete477;
			GabaritCommandeFixeData;
			GabaritCommandeMobile;
			GabaritResiliationFixeData;
			GabaritBlank;
			
			var gabarit : AbstractGabarit;
			var myClass : Class;
			
			if(idGroupe > 0){
				try{
					myClass = getDefinitionByName("composants.mail.gabarits.Gabarit" + type +idGroupe.toString()) as Class;
					gabarit = new myClass();	
				}catch(e : Error){
					myClass = getDefinitionByName("composants.mail.gabarits.Gabarit" + type) as Class;
					gabarit = new myClass();
				}													
			}else{				
				try{
					myClass = getDefinitionByName("composants.mail.gabarits.Gabarit" + type) as Class;
					gabarit = new myClass();
					
				}catch(e : Error){
					gabarit = new GabaritBlank();
				}
			}
			
			
			gabarit.infos = infos;
			return gabarit.getGabarit();		
			
		}	
		
	}
}