package composants.mail
{

	import commandemobile.system.Contact;
	
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;


	//---- EVENTS  ---------------------------------------------------
	[Event(name='mail_sent')]
	//--- FIN EVENTS --------------------------------------------------
	
	[Bindable]
	public class MailBoxImpl extends Box
	{	
		//composants
		public var btnClose : Button;
		public var btnEnvoyer : Button;
		public var rteMessage : TextArea;
		public var imgContacts : Image;
		public var imgCancel : Image;
		
		public var txtExpediteur : Label;
		public var txtDest : Label;
		public var txtModule : Label;
		public var txtcc : TextInput;
		public var txtcci : TextInput;
		public var txtSujet : TextInput;
		public var cbCopie : CheckBox;
		public var cbCopieOperateur : CheckBox;		
		
//		protected var _pUpcontactWindow : SelectContactView;
		//fin composants
		
		//Les infos pour le mask du mail
		protected var _infosObject : InfosObject;	
		//Le mail
		protected var _mail : MailVO;	
		
		// le mail du destinatire
		protected var _emailDestiantaire : String;
				
		//le contact destinataire du mail
		protected var _contact : Contact;		
		//--- FIN VARS ---------------------------------------------
		
		
	
	
		
		//--- CONSTANTES --------------------------------------------
		public static const MAIL_ENVOYE : String = 'mail_sent';				
		//---- FIN CONSTANTES ----------------------------------------
		
				
		//--- HANDLERS ----------------------------------------------
		protected function creationCompleteHandler(event:FlexEvent):void {
			PopUpManager.centerPopUp(this);
			imgCancel.addEventListener(MouseEvent.CLICK,imgCancelClickHandler);
			imgContacts.addEventListener(MouseEvent.CLICK,imgContactsClickHandler);
		}
		
		protected function closeEventHandler(ce : CloseEvent):void {
			PopUpManager.removePopUp(this);
		}
		
		protected function btnCloseClickHandler(me : MouseEvent):void {
			PopUpManager.removePopUp(this);
		}
		
		protected function btnEnvoyerClickHandler(me : MouseEvent):void{
			sendTheMail();	
		}
		
		//affiche la fenetre de selection d'un contact
		protected function imgContactsClickHandler(me : MouseEvent):void
		{
			var bool:Boolean = false;
		}		
		
		protected function constactSelectedHandler(ev : Event):void
		{
			var bool:Boolean = false;
		}
		
		protected function imgCancelClickHandler(me : MouseEvent):void{
			
		}
		//--- FIN HANDLERS -------------------------------------------
		
		
		
		
		
		//-------- PUBLIC -------------------------------------------		
		public function set contact(c : Contact):void{
			_contact = c;
		}
		public function get contact():Contact{
			if(_contact == null) _contact = new Contact();
			return _contact;
		}
		
		//Les infos pour le corps du mail
		public function set infosObject(infos : InfosObject):void{
			_infosObject = infos;
		}
		
		public function get infosObject():InfosObject{
			return _infosObject;
		}
		
		//Le corps du mail
		protected var _rteMessageText : String;
		public function get rteMessageText():String{
			return _rteMessageText;
		}
		
		protected var _moduleText : String;
		public function get moduleText():String{
			return _moduleText;
		}
				
		protected var _sujetText : String;
		public function get sujetText():String{
			return _sujetText;
		}
		
		protected var _expediteurText : String;
		public function get expediteurText():String{
			return _expediteurText;
		}
		
		protected var _destinataireText : String;
		public function get destinataireText():String{
			return _destinataireText;
		}
		
		
		//initialisation des donnees du mail
		public function initMail(module:String,sujet:String,dest:String):void {
			
			_moduleText = module;
			_sujetText = sujet;
			_destinataireText = dest;	
			_emailDestiantaire = dest;					 
			_expediteurText = CvAccessManager.getSession().USER.PRENOM + " " +
					CvAccessManager.getSession().USER.NOM;
			
			
			_mail = new MailVO();
			_mail.expediteur = CvAccessManager.getSession().USER.EMAIL;
			_mail.destinataire = _emailDestiantaire;
		}
		
		
		//Affichage du corps du message
		public function configRteMessage(infos : InfosObject,type : String = "Blank", idGroupe : Number = 0):void{
			infosObject = infos;
			_rteMessageText = GabaritFactory.getGabarit(infosObject,type,idGroupe);
			//updateDisplayList(unscaledHeight,unscaledWidth);
		}
		//------ FIN PUBLIC -----------------------------------------
		
		
		
		
		
		
		
		//-------- PROTECTED -------------------------------------------
		override protected function commitProperties():void{
			super.commitProperties();
			
			addEventListener(CloseEvent.CLOSE,closeEventHandler);
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			
			btnClose.addEventListener(MouseEvent.CLICK,btnCloseClickHandler);
			btnEnvoyer.addEventListener(MouseEvent.CLICK,btnEnvoyerClickHandler);
						
			rteMessage.styleName="TextInputAccueil";
			
		}
		
		
		//Envoi du message
		protected function sendTheMail():void {
			
			_mail.cc = txtcc.text;
			_mail.bcc = txtcci.text;
			_mail.module = txtModule.text;			
			_mail.copiePourExpediteur = (cbCopie.selected)?"YES":"NO";
			_mail.copiePourOperateur = "NO";			
			_mail.sujet = txtSujet.text;
			_mail.message = rteMessage.htmlText;
			_mail.destinataire = _emailDestiantaire; 
			
			if(_mail.destinataire != null){
				var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.parametres.perimetres.mail.MailSender",
								"envoyer",
								sendTheMailResultHandler);		
								
				RemoteObjectUtil.callService(op,_mail);
				
				dispatchEvent(new Event(MAIL_ENVOYE));	
				
				PopUpManager.removePopUp(this);
			}else{
				Alert.buttonWidth = 100;
				Alert.okLabel = "Fermer";
				Alert.show("Le destinataire est obligatoire");		
			}
			
		}
		
		//------------- FIN PROTECTED -------------------------------------
		
		
		
		
		
		
		
		
		
		//------------- PRIVATE --------------------------------------------
		protected function sendTheMailResultHandler(re : ResultEvent):void{	
			/* if (re.result > 0){
				dispatchEvent(new Event(MAIL_ENVOYE));
			}else{ */
				/* Alert.show("Le mail n'a pas pu être envoyé : " + re.result , "Erreur");
				dispatchEvent(new Event(MAIL_ENVOYE)); */
			/* } */
			
		}//------------ FIN PRIVATE ---------------------------------------
		
		
	}
}