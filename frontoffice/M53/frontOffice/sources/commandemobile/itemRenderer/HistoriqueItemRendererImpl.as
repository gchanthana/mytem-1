package commandemobile.itemRenderer
{
	import commandemobile.entity.Commande;
	import commandemobile.system.Action;
	import commandemobile.system.ActionEvent;
	import commandemobile.system.GestionWorkFlow;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.events.MenuEvent;
	
	[Bindable]
	public class HistoriqueItemRendererImpl extends Box
	{
		//COMPOSANTS
		
		//OBJETS
		public var cmd				:Commande;
		public var gworkFlow		:GestionWorkFlow = new GestionWorkFlow();
		
		
		//VARIABLES LOCALES
		public var listHistorique:ArrayCollection = new ArrayCollection();
		
		
		//CONSTRUCTEUR (>LISTNER)
		public function HistoriqueItemRendererImpl()
		{
			gworkFlow.addEventListener("historiqueFinished",	getHistoriqueResultHandler);
			addEventListener("getProcedure",					getProcedure);
		}

		private function getProcedure(e:Event):void
		{			
			gworkFlow.fournirHistoriqueEtatsCommande(cmd);
		}
		
		private function getHistoriqueResultHandler(e:Event):void
		{
			listHistorique = new ArrayCollection();
			listHistorique = gworkFlow.historiqueWorkFlow;
		}
		
		

	}
}