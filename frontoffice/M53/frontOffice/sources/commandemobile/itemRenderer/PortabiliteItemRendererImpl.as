package commandemobile.itemRenderer
{
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	import composants.util.lignes.LignesUtils;
	
	import flash.events.Event;
	
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.TextInput;
	
	[Bindable]
	public class PortabiliteItemRendererImpl extends VBox
	{
		
//VARIABLES------------------------------------------------------------------------------
		
		//COMPOSANTS
		public var vboxPortabilite				:VBox;
		public var ckbxActivePortabilite		:CheckBox;
		public var txtNumero					:TextInput;
		public var txtRIO						:TextInput;
		public var dcLivraisonPrevue			:CvDateChooser;

		//OBJETS
		private var _numLine					:LignesUtils = new LignesUtils();
		
		//VARIABLES GLOBALES
		
		//VARIABLES LOACALES
		public var numeroLigneUnique			:String = "";
		public var periodeObject				:Object = {rangeStart:new Date(new Date().getTime() + (10 * DateFunction.millisecondsPerDay) )};
		public var dateDuJourPlus2JoursOuvrees	:Date	= DateFunction.addXJoursOuvreesToDate(new Date(),10);
		public var enablePortabilite			:Boolean = true;

		//CONSTANTES
		private const widthMax		:int = 160;
		private const widthMin		:int = 0;

//VARIABLES------------------------------------------------------------------------------	

//METHODE PUBLICS------------------------------------------------------------------------
		
		//CONSTRUCTEUR
		public function PortabiliteItemRendererImpl()
		{
			_numLine.addEventListener("genererNumeroLigneUnique",genererNumeroLigneUniqueHandler);
		}
		
//METHODE PUBLICS------------------------------------------------------------------------

//METHODE PROTECTED----------------------------------------------------------------------
		
		override public function set data(value:Object):void
		{
			ckbxActivePortabilite.selected 			= false;
			vboxPortabilite.enabled					= false;
			txtNumero.text = "";
			txtRIO.text = "";
			txtRIO.errorString 				= "";
			txtNumero.errorString 			= "";
			dcLivraisonPrevue.errorString 	= "";
			
			if(value != null)
			{
				super.data = value;
				enablePortabilite 					= data.ENABLEPORTABILITE;
				if(data.PORTABILITE) 
				{
					ckbxActivePortabilite.selected 	= true;
					vboxPortabilite.enabled 		= true;
					if(data.NUM_LINE != "" || data.NUM_RIO != "") 
					{
						if(data.NUM_LINE.toString().indexOf("06TEMP") == -1)
						{ 
							txtNumero.text 					= data.NUM_LINE;
						}
						txtRIO.text 					= data.NUM_RIO;
					}
				}
				if(data.DATE_PORTABILITE != null)
				{ 
					dcLivraisonPrevue.selectedDate 	= data.DATE_PORTABILITE;
				}
				
				if(data.NUM_LINE == "")
				{
					_numLine.genererNumeroLigneUnique();
					creationCompleteHandler();
				}
				errorTag();
			}
		}
		
		protected function txtRIOFocusOutHandler():void
		{
			if(txtRIO.text.length < 12)
			{
				txtRIO.errorString = "Le numéro RIO doit comporter 12 caractères";
			}
			else
			{
				if(txtRIO.text.length == 0)
				{
					txtRIO.errorString = "Veuillez saisir votre numéro RIO";
				}
				else
				{
					txtRIO.errorString = "";
				}
			}
		}
		
		protected function txtNumeroFocusOutHandler():void
		{
			if(txtNumero.text.length < 10)
			{
				txtNumero.errorString = "Votre numéro de téléphone doit comporter 10 chiffres";
			}
			else
			{
				if(txtNumero.text.length == 0)
				{
					txtNumero.errorString = "Veuillez saisir votre numéro de téléphone";
				}
				else
				{
					txtNumero.errorString = "";
				}
			}
		}
		protected function errorTag():void
		{
			if(data.PORTABILITE)
			{
				if(txtNumero.text == "")
				{
					txtNumero.errorString = "Veuillez saisir votre numéro de portable";
				}
				else
				{
					txtNumeroFocusOutHandler();
				}			
				if(txtRIO.text == "")
				{
					txtRIO.errorString = "Veuillez saisir votre numéro RIO";
				}
				else
				{
					txtRIOFocusOutHandler();
				}
				if(dcLivraisonPrevue.text == "")
				{
					dcLivraisonPrevue.errorString = "Veuillez saisir une date de portabilité";
				}
				else
				{
					dcLivraisonPrevue.errorString = "";
				}
			}
			else
			{
				txtNumero.errorString = "";
				txtRIO.errorString = "";
				dcLivraisonPrevue.errorString = "";
			}
		}
		
		protected function creationCompleteHandler():void
		{
			txtNumero.text = "";
			txtRIO.text = "";
			if(!enablePortabilite)
			{
				if(txtNumero.text == "")
				{
					txtNumero.errorString="Veuillez saisir votre numéro de portable";
				}
				if(txtRIO.text == "")
				{
					txtRIO.errorString="Veuillez saisir votre numéro RIO";
				}
				if(dcLivraisonPrevue.text == "")
				{
					dcLivraisonPrevue.errorString="Veuillez saisir une date de portabilité";
				}
			}
			else
			{
				if(txtNumero.text == "")
				{
					txtNumero.errorString="";
				}
				if(txtRIO.text == "")
				{
					txtRIO.errorString="";
				}
				if(dcLivraisonPrevue.text == "")
				{
					dcLivraisonPrevue.errorString="";
				}
			}
		}
		
		protected function ckbxActivePortabiliteClickHandler():void
		{
			if(ckbxActivePortabilite.selected)
			{
				vboxPortabilite.enabled 	= true;
				data.PORTABILITE 			= true;
				data.NUM_LINE 				= txtNumero.text;
				data.NUM_RIO 				= txtRIO.text;
				data.DATE_PORTABILITE 		= dcLivraisonPrevue.selectedDate;
				if(txtNumero.text == "")
				{
					txtNumero.errorString="Veuillez saisir votre numéro de portable";
				}
				if(txtRIO.text == "")
				{
					txtRIO.errorString="Veuillez saisir votre numéro RIO";
				}
				if(dcLivraisonPrevue.text == "")
				{
					dcLivraisonPrevue.errorString="Veuillez saisir une date de portabilité";
				}
			}
			else
			{
				vboxPortabilite.enabled 	= false;
				data.PORTABILITE 			= false;
				txtNumero.errorString = "";
				txtRIO.errorString = "";
				dcLivraisonPrevue.errorString = "";			
				data.NUM_LINE 				= numeroLigneUnique;
			}
		}
		
		protected function txtInputHandler(bool:Boolean):void
		{
			if(bool)
			{
				data.NUM_LINE 	= txtNumero.text;
				txtNumero.errorString = "";
			}
			else
			{
				data.NUM_RIO 	= txtRIO.text;
				txtRIO.errorString = "";
			}
		}
		
		protected function dataPortabiliteCommitHAndler():void
		{
			data.DATE_PORTABILITE = dcLivraisonPrevue.selectedDate;
			dcLivraisonPrevue.errorString = "";
		}

//METHODE PROTECTED----------------------------------------------------------------------

//METHODE PRIVATE------------------------------------------------------------------------		
		
		private function genererNumeroLigneUniqueHandler(e:Event):void
		{
			if(_numLine.boolOK)
			{
				numeroLigneUnique = _numLine.numeroUnique;
				data.NUM_LINE = numeroLigneUnique;
			}
		}
		
//METHODE PRIVATE------------------------------------------------------------------------

	}
}