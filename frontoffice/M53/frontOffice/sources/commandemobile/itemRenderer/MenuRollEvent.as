package commandemobile.itemRenderer
{
	import flash.events.Event;

	public class MenuRollEvent extends Event
	{
		public static const MENU_ROLL_OVER:String = "menuRollOver";
		public static const MENU_ROLL_OUT:String = "menuRollOut";
		
		public function MenuRollEvent(type:String, bubbles:Boolean=false) {
			super(type, bubbles);
		}
		
		override public function clone():Event {
			return new MenuRollEvent(type, bubbles);
		}
		
	}
}