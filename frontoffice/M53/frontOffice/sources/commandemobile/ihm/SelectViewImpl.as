package commandemobile.ihm
{
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.containers.ViewStack;
	
	[Bindable]
	public class SelectViewImpl extends Box
	{
		
//VARIABLES------------------------------------------------------------------------------

		//COMPOSANTS
		public var etapeListCommande	:ListeCommandeResiliationIHM 	= null;
		public var etapeNewCommande		:MainNewCommandeIHM 			= null;
		public var etapeHistorique		:HistoriqueIHM 					= null;
		public var vs					:ViewStack;
		
		//VARIALBES LOCALES
		public var selectedIndex		:int = 0;

//VARIABLES------------------------------------------------------------------------------

//METHODE PUBLICS------------------------------------------------------------------------
		
		//CONSTRUCTEUR (>LISTENER)
		public function SelectViewImpl()
		{
			if(etapeNewCommande != null)
			{
				etapeNewCommande.addEventListener("etapeListCommande",listCommandeIndexHandler);
			}
			
			if(etapeListCommande != null)
			{
				etapeListCommande.addEventListener("etapeNewCommande",listNewCommandeIndexHandler);
			}
			
			if(etapeHistorique != null)
			{
				etapeListCommande.addEventListener("etapeHistorique",etapeHistoriqueIndexHandler);
			}
			
			addEventListener("etapeListCommande",	listCommandeIndexHandler);
			addEventListener("etapeNewCommande",	listNewCommandeIndexHandler);
			addEventListener("etapeHistorique",		etapeHistoriqueIndexHandler);
		}

//METHODE PUBLICS------------------------------------------------------------------------

//METHODE PROTECTED----------------------------------------------------------------------

		//AFFICHE LA FENETRE DE LA LISTE DES COMMANDES ET APPEL LA PROCEDURE PERMETTANT L'AFFICHAGE DE TOUTES LES COMMANDES
		protected function creationCompleteHandler(e:Event):void
		{
			vs.selectedChild = etapeListCommande;
			etapeListCommande.dispatchEvent(new Event("etapeListCommande"));	
		}

//METHODE PROTECTED----------------------------------------------------------------------

//METHODE PRIVATE------------------------------------------------------------------------
		
		//AFFICHE LA FENETRE DE LA LISTE DES COMMANDES ET APPEL LA PROCEDURE PERMETTANT L'AFFICHAGE DE TOUTES LES COMMANDES
		private function listCommandeIndexHandler(e:Event):void
		{
			vs.removeAllChildren();
			try
			{
				etapeListCommande = new ListeCommandeResiliationIHM();
				vs.addChild(etapeListCommande);
				vs.selectedChild = etapeListCommande;
			}
			catch(e:Error)
			{
				vs.selectedChild = etapeListCommande;
			}
			etapeListCommande.dispatchEvent(new Event("etapeListCommande"));	
		}
		
		//AFFICHE LA FENETRE NOUVELLES COMMANDES
		private function listNewCommandeIndexHandler(e:Event):void
		{	
			vs.removeAllChildren();
			try
			{
				etapeNewCommande = new MainNewCommandeIHM();
				vs.addChild(etapeNewCommande);
				vs.selectedChild = etapeNewCommande;
				etapeNewCommande.dispatchEvent(new Event("fournirPoolsDuGestionnaire"));
			}
			catch(e:Error)
			{
				vs.selectedChild = etapeNewCommande;
				etapeNewCommande.dispatchEvent(new Event("fournirPoolsDuGestionnaire"));
			}
		}
		
		//AFFICHE LA FENETRE HISTORIQUE
		private function etapeHistoriqueIndexHandler(e:Event):void
		{	
			vs.removeAllChildren();
			try
			{
				etapeHistorique = new HistoriqueIHM();
				vs.addChild(etapeHistorique);
				vs.selectedChild = etapeHistorique;
				etapeHistorique.dispatchEvent(new Event("fournirPoolsDuGestionnaire"));
			}
			catch(e:Error)
			{
				vs.selectedChild = etapeHistorique;
				etapeHistorique.dispatchEvent(new Event("fournirPoolsDuGestionnaire"));
			}
		}
		
//METHODE PRIVATE------------------------------------------------------------------------
	
	}
}