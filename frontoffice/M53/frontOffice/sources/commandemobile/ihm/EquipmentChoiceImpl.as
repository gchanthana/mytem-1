package commandemobile.ihm
{
	import commandemobile.composants.PanierIHM;
	import commandemobile.entity.Commande;
	import commandemobile.entity.ItemSelected;
	import commandemobile.entity.Panier;
	import commandemobile.popup.DetailProduitIHM;
	import commandemobile.temp.commandeSNCF;
	
	import composants.util.equipements.EquipementsUtils;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.VBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class EquipmentChoiceImpl extends Box
	{
		
//VARIABLES------------------------------------------------------------------------------
		
		//IHM
		public var composantPanier			:PanierIHM;
		
		//COMPOSANTS
		public var cboxEngagement			:ComboBox;	
		public var txtptSearch				:TextInput;
		public var dgListTerminal			:DataGrid;
		public var vbxEquipementsSelection	:VBox;	
		public var boxVisible				:Box;	
		public var boxEngagementVisible		:Box;
		//OBJETS
		public var _cmd						:Commande;
		private var _cmdSNCF				:commandeSNCF 		= new commandeSNCF();
		public var _itemsSelected			:ItemSelected		= new ItemSelected();
		public var _panier					:Panier;
		
		//VARAIBLES GLOBALES
		public var parameterFill			:Boolean 			= false;
		public var carteSim					:Boolean 			= true;
		public var viewEngagement			:Boolean 			= true;

		//VARAIBLES LOCALES
		private var newView					:Boolean 			= true;
		private var _listProduitVisible		:ArrayCollection 	= new ArrayCollection();
		public var _terminaux				:ArrayCollection 	= new ArrayCollection();
		private var carteSimCreation		:EquipementsUtils 	= new EquipementsUtils();

		private var indexObjectSelected		:int 				= -1;
		private var objectCarteSim			:Object 			= null;
		private var objectMobile			:Object 			= null;
		private var objectCarteSimTemp		:Object 			= null;
		public var totalCard				:Number 			= 0;
		public var totalMobile				:Number 			= 0;
		private var newTerminal				:Boolean			= true;
		public var price					:String = "[PRIX_CATALOGUE]";

//VARIABLES------------------------------------------------------------------------------

//PROPRIETEES PUBLICS--------------------------------------------------------------------
		
		public function get terminaux():ArrayCollection
		{
			return _terminaux;
		}
		public function set terminaux(values:ArrayCollection):void
		{
			_terminaux = values;
		}
		
//PROPRIETEES PUBLICS--------------------------------------------------------------------		
		
//METHODES PUBLICS-----------------------------------------------------------------------
		
		//CONSTRUCTEUR (>LISTNER)
		public function EquipmentChoiceImpl()
		{
			carteSimCreation.addEventListener("fournirCarteSim",listnerTempEventCarteSim);
			_cmdSNCF.addEventListener("listeEquipements", 		listnerTempEvent);
			addEventListener("nextPageClicked",					checkIfAttributToCommandeFill);
			addEventListener("equipementSelect", 				detailsThisTerminalSelected);
			addEventListener("terminalSelected", 				thisIsTerminalSelected);
		}

//METHODES PUBLICS-----------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
	
		private function listnerTempEventCarteSim(e:Event):void
		{
			if(Number(carteSimCreation.selectedEquipement.IDTYPE_EQUIPEMENT) == 71)
			{
				objectCarteSim = carteSimCreation.selectedEquipement;
				return;
			}
		}
	
		protected function dgListTerminalClickHandler():void
		{
			if(dgListTerminal.selectedItem != null)
			{
				var ev:Event = new Event(Event.ACTIVATE);
				thisIsTerminalSelected(ev);
			}
		}
	
		protected function composantPanierCreationCompleteHandler():void
		{
			composantPanier.panier = _panier;
		}
		
		protected function showHandler():void
		{
			objectCarteSim 	= null;
			objectMobile 	= null;
			if(_itemsSelected.TERMINAUX.length > 0)
			{
				newTerminal = false;
			}
			_cmdSNCF.fournirListeEquipements(_cmd.IDGESTIONNAIRE,_cmd.IDPOOL_GESTIONNAIRE,_cmd.IDPROFIL_EQUIPEMENT, _cmd.IDREVENDEUR,"");
			carteSimCreation.fournirCarteSim(Number(_cmd.IDREVENDEUR),Number(_cmd.IDOPERATEUR));
			composantPanierCreationCompleteHandler();
		}

		protected function txtptSearchHandler():void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
//			_listProduitVisible = terminaux;//SE MEFIER
			terminaux = new ArrayCollection();
			if(txtptSearch.text == "")
			{
				terminaux = _listProduitVisible;
			}
			else
			{
				for(var i:int = 0;i < _listProduitVisible.length;i++)
				{
					if(_listProduitVisible[i].LIBELLE_EQ.toString().toLowerCase().indexOf(txtptSearch.text.toLowerCase()) != -1)
					{ 
						arrayC.addItem(_listProduitVisible[i]); 
					}
				}
				terminaux = arrayC;
			}
		}
		
		protected function detailsThisTerminalSelected(e:Event):void
		{
			if(dgListTerminal.selectedItem != null)
			{
				var popUpDetails:DetailProduitIHM = new DetailProduitIHM();
				popUpDetails.equipement = dgListTerminal.selectedItem;
				popUpDetails.detailsProduitReceived = dgListTerminal.selectedItem;
				PopUpManager.addPopUp(popUpDetails,this,true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}
		
//METHODES PROTECTED---------------------------------------------------------------------

//METHODES PRIVATE-----------------------------------------------------------------------

		private function addItemEmpty():void
		{
			var objectEmpty:Object = new Object();
			objectEmpty.TYPE_EQUIPEMENT 		= "Aucun mobile";
			objectEmpty.LIBELLE_EQ				= "-";
			objectEmpty.NIVEAU					= "";
			objectEmpty.PRIX_CATALOGUE			= "";
			objectEmpty.IDEQUIPEMENT_FOURNISSEUR= 0;
			objectEmpty.IDTYPE_EQUIPEMENT 		= 0;	
			
			terminaux.addItemAt(objectEmpty,0);
			_listProduitVisible.addItemAt(objectEmpty,0);
		}

		private function allSelectedAtFalse():void
		{
			for(var i:int = 0;i < terminaux.length;i++)
			{
				terminaux[i].SELECTED = false;
			}
		}

		private function attributToCommande():void
		{
			_itemsSelected.TERMINAUX = new ArrayCollection();
			
			if(!carteSim)
			{
				_itemsSelected.TERMINAUX.addItem(objectMobile);
			}
			else
			{
				if(objectMobile.IDEQUIPEMENT_FOURNISSEUR == 0)
				{
					_itemsSelected.TERMINAUX.addItem(objectCarteSim);
				}
				else
				{
					_itemsSelected.TERMINAUX.addItem(objectMobile);
					_itemsSelected.TERMINAUX.addItem(objectCarteSim);
				}
				
			}
		}
		
//EVENT----------------------------------------------------------------------------------			
		
		private function listnerTempEvent(e:Event):void
		{
			if(carteSim)
			{
				terminaux = new ArrayCollection();
				_listProduitVisible = new ArrayCollection();
				for(var i:int = 0;i < _cmdSNCF.listeEquipements.length;i++)
				{
					if(!newTerminal)
					{
						for(var j:int = 0;j < _itemsSelected.TERMINAUX.length;j++)
						{
							if(_itemsSelected.TERMINAUX[0].TYPE_EQUIPEMENT != "Carte Sim")
							{
								if(_itemsSelected.TERMINAUX[j] != null)
								{
									if(_cmdSNCF.listeEquipements[i].IDEQUIPEMENT_FOURNISSEUR == _itemsSelected.TERMINAUX[j].IDEQUIPEMENT_FOURNISSEUR)
									{
										_cmdSNCF.listeEquipements[i].SELECTED = true;
									}
								}
							}
							else
							{
								if(terminaux.length > 0)
								{
									terminaux[0].SELECTED = true;
								}
							}
						}
					}
					if(_cmdSNCF.listeEquipements[i].TYPE_EQUIPEMENT == "Mobile")
					{
						terminaux.addItem(_cmdSNCF.listeEquipements[i]);
						_listProduitVisible.addItem(_cmdSNCF.listeEquipements[i]);
					}
					if(_cmdSNCF.listeEquipements[i].TYPE_EQUIPEMENT == "Carte Sim")
					{
						objectCarteSim = _cmdSNCF.listeEquipements[i];
					}
				}
			}
			else
			{
				terminaux = new ArrayCollection();
				_listProduitVisible = new ArrayCollection();
				if(!viewEngagement)
				{
					addItemEmpty();///IMPLEMENTATION RAJOUT D'UN OBJET VIDE!
				}
				for(var k:int = 0;k < _cmdSNCF.listeEquipements.length;k++)
				{
					if(!newTerminal)
					{
						for(var l:int = 0;l < _itemsSelected.TERMINAUX.length;l++)
						{
							if(_itemsSelected.TERMINAUX[l] != null)
							{
								if(_cmdSNCF.listeEquipements[k].IDEQUIPEMENT_FOURNISSEUR == _itemsSelected.TERMINAUX[l].IDEQUIPEMENT_FOURNISSEUR)
								{
									_cmdSNCF.listeEquipements[k].SELECTED = true;
								}
							}
						}
					}
					if(_cmdSNCF.listeEquipements[k].TYPE_EQUIPEMENT == "Mobile")
					{
						terminaux.addItem(_cmdSNCF.listeEquipements[k]);
						_listProduitVisible.addItem(_cmdSNCF.listeEquipements[k]);
					}
				}
			}
		}			
		
		private function thisIsTerminalSelected(e:Event):void
		{
			if(dgListTerminal.selectedItem != null)
			{
				if(dgListTerminal.selectedItem.IDTYPE_EQUIPEMENT == 70 || dgListTerminal.selectedItem.IDTYPE_EQUIPEMENT == 0)
				{
					allSelectedAtFalse();
					objectMobile = dgListTerminal.selectedItem;
					dgListTerminal.selectedItem.SELECTED = true;
				}
				(dgListTerminal.dataProvider as ArrayCollection).itemUpdated(dgListTerminal.selectedItem);
			}
		}

		private function checkIfAttributToCommandeFill(e:Event):void
		{	
			parameterFill = false;
			searchMobileSelected();
			if(objectMobile != null)
			{
				parameterFill = true;
				attributToCommande();
			}
		}
		
		private function searchMobileSelected():void
		{
			var arrayC:ArrayCollection = dgListTerminal.dataProvider as ArrayCollection;
			objectMobile = null;
			for(var i:int = 0;i < arrayC.length;i++)
			{
				if(arrayC[i].SELECTED)
				{
					objectMobile = arrayC[i];
					return;
				}
			}
		}
		
//EVENT----------------------------------------------------------------------------------		
		
//METHODES PRIVATE-----------------------------------------------------------------------		

	}
}