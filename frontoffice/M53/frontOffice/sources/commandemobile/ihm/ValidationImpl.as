package commandemobile.ihm
{
	import commandemobile.composants.PanierIHM;
	import commandemobile.entity.Commande;
	import commandemobile.entity.ItemSelected;
	import commandemobile.entity.Panier;
	import commandemobile.system.Action;
	import commandemobile.system.Export;
	import commandemobile.system.GestionWorkFlow;
	import commandemobile.temp.temp;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.List;
	import mx.controls.RadioButton;
	
	[Bindable]
	public class ValidationImpl extends Box
	{

//VARIABLES------------------------------------------------------------------------------

		//COMPOSANTS
		public var listConfig				:List;
		public var rdbtnRecord				:RadioButton;
		public var rdbtnValideIterne		:RadioButton;
		public var rdbtnValideTarifConfig	:RadioButton;
		public var rdbtnValideSend			:RadioButton;
		public var composantPanier			:PanierIHM;
		
		//OBJETS
		private var _gworkFlow				:GestionWorkFlow = new GestionWorkFlow();
		public var _cmd						:Commande;
		public var _itemsSelected			:ItemSelected;
		public var _panier					:Panier;
		public var _listActions0			:Action = new Action();
		public var _listActions1			:Action = new Action();
		public var _listActions2			:Action = new Action();
		public var selectedAction			:Action;
		
		//VARIABLES LOCALES
		private var _listCommande			:ArrayCollection = new ArrayCollection();
		public var listCommande				:ArrayCollection = new ArrayCollection();
		public var _listActions				:ArrayCollection = new ArrayCollection();
		
		
		//VARIBLES GLOBALES
		public var parameterFill			:Boolean = false;

//VARIABLES------------------------------------------------------------------------------

//METODES PUBLICS------------------------------------------------------------------------
	
		//CONTRUCTEUR (>LISTENER)
		public function ValidationImpl()
		{
			addEventListener("eraseThisConfig",				earseConfigInList);
			_gworkFlow.addEventListener("actionsFinished",	getActionsResultHandler);
		}

//METODES PUBLICS------------------------------------------------------------------------

//METODES PROTECTED----------------------------------------------------------------------
		
		protected function btValidAndPurchaseClickHandler(event:Event):void
		{
			dispatchEvent(new Event("ValidAndPurchase",true));
		}
		
		protected function composantPanierCreationCompleteHandler():void
		{
			composantPanier.panier = _panier;
		}
		
		protected function showHandler():void
		{	
			composantPanierCreationCompleteHandler();
			rdbtnRecord.selected 			= false;
			rdbtnValideIterne.enabled 		= false;
			rdbtnValideSend.enabled 		= false;
			rdbtnValideTarifConfig.enabled 	= false;
			
			_listCommande 	= new ArrayCollection();
			listCommande	= new ArrayCollection();
			
			attributConfig_TempToConfig();
			var nbr					:int = 0;
			var numeroConfiguration	:int = 0;////------------------------------------------------
			var quantity			:int = 0;
			for(var i:int = 0; i < _cmd.COMMANDE.length;i++)
			{
				var commande			:ArrayCollection = new ArrayCollection();
				var tempArrayC			:ArrayCollection = new ArrayCollection();
				var tempCollaborateur	:ArrayCollection = new ArrayCollection();

				if(i == 0)
				{
					numeroConfiguration = _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE;
					for(var cp:int = 0; cp < _cmd.COMMANDE.length;cp++)
					{
						if(numeroConfiguration == _cmd.COMMANDE[cp].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE)
						{
							quantity++;
						}
					}
				}
				else
				{
					if(numeroConfiguration != _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE)
					{
						numeroConfiguration = _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE;
						quantity = 0;
						for(var cpt:int = 0; cpt < _cmd.COMMANDE.length;cpt++)
						{
							if(numeroConfiguration == _cmd.COMMANDE[cpt].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE)
							{
								quantity++;
							}
						}
					}
				}

				commande.addItem(convertToItemrendere(_cmd.COMMANDE[i].ABO			,false	,quantity));
				commande.addItem(convertToItemrendere(_cmd.COMMANDE[i].ACCESSOIRES	,true	,quantity));
				commande.addItem(convertToItemrendere(_cmd.COMMANDE[i].OPTIONS		,false	,quantity));
				commande.addItem(convertToItemrendere(_cmd.COMMANDE[i].TERMINAUX	,true	,quantity));
				tempCollaborateur.addItem(_cmd.COMMANDE[i].EMPLOYE_CONFIGURATION);
				for(var j:int = 0; j < commande.length;j++)
				{
					var arrayC:ArrayCollection = commande[j];
					for(var k:int = 0;k < arrayC.length;k++)
					{
						tempArrayC.addItem(arrayC[k]);
					}
				}
				
				_listCommande.addItem(convertToObject(tempArrayC,_cmd.COMMANDE[i].EMPLOYE_CONFIGURATION));
			}
			
			attrinutToListCommande();
			_panier.attributMontant(_itemsSelected);
			fournirListeActionsPossibles();
		}
		
		private function calculNombreConfiguration():int
		{
			var quantity:int = 0;
			for(var i:int = 0; i < _cmd.COMMANDE.length;i++)
			{
				if(_cmd.COMMANDE[0].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE == _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE)
				{
					quantity++;
				}
			}
			return quantity;
		}
		
		
		private function attrinutToListCommande():void
		{
			var cptrNbrCommande:int = 1;
			for(var i:int = 0;i < _listCommande.length;i++)
			{
				if(i == 0)
				{
					_listCommande[i].COLLABORATEUR.CONFIGURATION_NAME = "Configuration " + cptrNbrCommande;
					listCommande.addItem(_listCommande.getItemAt(i));
					cptrNbrCommande++;
				}
				else
				{
					if(_listCommande[i].COLLABORATEUR.NUMEROCONFIG_UNIQUE != _listCommande[i-1].COLLABORATEUR.NUMEROCONFIG_UNIQUE)
					{
						_listCommande[i].COLLABORATEUR.CONFIGURATION_NAME = "Configuration " + cptrNbrCommande;
						listCommande.addItem(_listCommande.getItemAt(i));
						cptrNbrCommande++;
					}
				}
			}
		}
		
		private function fournirListeActionsPossibles():void
		{
			_gworkFlow.fournirListeActionsPossibles(3066,_cmd);
		}
		
		private function getActionsResultHandler(e:Event):void
		{
			if(_gworkFlow.listeActionsPossibles.length > 0)
			{
				for(var i:int = 0;i < _gworkFlow.listeActionsPossibles.length;i++)
				{
					if(_gworkFlow.listeActionsPossibles[i].CODE_ACTION == "EMA2B")
					{
						rdbtnValideIterne.enabled = true;
						_listActions0 = _gworkFlow.listeActionsPossibles[i];
					}
					if(_gworkFlow.listeActionsPossibles[i].CODE_ACTION == "EMA1")
					{
						rdbtnValideTarifConfig.enabled = true;
						_listActions2 = _gworkFlow.listeActionsPossibles[i];
					}
					if(_gworkFlow.listeActionsPossibles[i].CODE_ACTION == "EMA3T")
					{
						rdbtnValideSend.enabled = true;
						_listActions1 = _gworkFlow.listeActionsPossibles[i];
					}
				}
			}
		}

		
		protected function rdbtnValideIterneChangeHandler():void
		{
			selectedAction = _listActions0;
			btnValiderActionClickHandler();
		}
		
		protected function rdbtnValideSendChangeHandler():void
		{
			selectedAction = _listActions1;
			btnValiderActionClickHandler();
		}
		
		protected function rdbtnValideTarifConfigChangeHandler():void
		{
			selectedAction = _listActions2;			
			btnValiderActionClickHandler();
		}
		
		protected function rdbtnRecordChangeHandler():void
		{
			selectedAction = null;
			btnValiderActionClickHandler();
		}
		
		protected function btnValiderActionClickHandler():void
		{
			dispatchEvent(new Event("enableBtnValider",true));
		}

		//AFFICHE LA FATURE EN 'PDF'
		protected function btExporterClickHandler(event:MouseEvent):void
	    {
	    	if(_cmd.IDCOMMANDE > 0)
	    	{
	    		var export:Export = new Export();
	    		export.exporterCommandeEnPDF(_cmd);
	    	}	
	    }

//METODES PROTECTED----------------------------------------------------------------------

//METODES PRIVATE------------------------------------------------------------------------

		//AFFECTE 'COMMANDE_TEMPORAIRE' A LA 'COMMANDE'
		private function attributConfig_TempToConfig():void
		{
			for(var i:int = 0;i < _cmd.COMMANDE_TEMPORAIRE.length;i++)
			{
				_cmd.COMMANDE.addItem(_cmd.COMMANDE_TEMPORAIRE[i]);
			}
		}
		
		//CONVERTIT 'COMMANDE' EN OBJET POUR LE DONNER A L'ITEMRENDERER
		private function convertToObject(arrayCollection:ArrayCollection,object:Object):Object
		{
			var commande:Object 	= new Object();
			commande.COLLABORATEUR	= object;
			commande.ITEMS 			= arrayCollection;
			commande.PRIX			= calculConfigurationPrice(arrayCollection);
			return commande;
		}
		
		//CALCUL LE PRIX TOTAL DE LA COMMANDE PAR CONFIGURATION
		private function calculConfigurationPrice(arrayCollection:ArrayCollection):Object
		{
			var number:Number = 0;
			for(var i:int = 0;i < arrayCollection.length;i++)
			{
				number = number + Number(arrayCollection[i].TOTAL);
			}
			return number;
		}
				
		//CONVERTIT LES DONNEES POUR POUVOIR LES DONNER A L'ITEMRENDERER
		private function convertToItemrendere(toConvert:ArrayCollection,terminaux:Boolean,quantity:int):Object
		{
			var commande:ArrayCollection = new ArrayCollection();
			for(var i:int = 0; i < toConvert.length;i++)
			{
				if(toConvert[i] != null)
				{
					commande.addItem(convertToDatagrid(toConvert[i],terminaux,quantity));
				}
			}
			return commande;
		}
		
		//CONVERTIT 'COMMANDE_TEMPORAIRE' EN OBJET POUR POUVOIR L'AFFICHER DANS LE DATAGRID ET CALCULER LE MONTANT
		private function convertToDatagrid(toConvert:Object,terminaux:Boolean,quantity:int):Object
		{
			var object:Object = new Object();
			if(terminaux)
			{
				if(toConvert != null)
				{
					object.TYPE_EQUIPEMENT 	= toConvert.TYPE_EQUIPEMENT;
					object.LIBELLE_PRODUIT 	= toConvert.LIBELLE_EQ;
					object.PRIX_UNIT 		= checkPriceTerminaux(toConvert);
				}
			}
			else
			{
				object.TYPE_EQUIPEMENT 	= toConvert.SUR_THEME;
				object.LIBELLE_PRODUIT 	= toConvert.LIBELLE_PRODUIT;
				object.PRIX_UNIT 		= " - ";
			}
			object.QUANTITY 			= quantity;
			object.TOTAL 				= checkPrice(object,quantity);
			return object;
		}
		
		private function checkPriceTerminaux(object:Object):Number
		{
			var strg0:String = "";
			strg0 = object.PRIX_CATALOGUE;
			return Number(strg0);
		}
		
		private function checkPrice(object:Object,quantity:int):Object
		{
			if(object.PRIX_UNIT != " - ")
			{
				object.TOTAL = Number(object.PRIX_UNIT) * Number(quantity);
			}
			return object.TOTAL;
		}
		
		//CONVERTI LE TOTAL EN STRING
		private function convertStringToNumber(objectSelected:Object):Number
		{
			var tempNumber:Number = 0;
			tempNumber =  Number(objectSelected.QUANTITY) *  Number(objectSelected.PRIX_UNIT);
			return tempNumber;
		}
		
		//AFFACE LE CONFIGURATION SELECTIONNEE
		private function earseConfigInList(e:Event):void
		{
			if(listConfig.selectedItem != null)
			{
				var i:int = 0
				for(i = 0;i < _cmd.COMMANDE.length;i++)
				{
					if( listConfig.selectedItem.COLLABORATEUR.NUMEROCONFIG_UNIQUE == _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE)
					{
						try
						{
							_cmd.COMMANDE.removeItemAt(i);
							i--;
						}
						catch(e:Error)
						{
						
						}
					}
				}
				for(i = 0;i < listCommande.length;i++)
				{
					if(listConfig.selectedItem != null)
					{
						if( listConfig.selectedItem.COLLABORATEUR.NUMEROCONFIG_UNIQUE == listCommande[i].COLLABORATEUR.NUMEROCONFIG_UNIQUE)
						{
							try
							{
								listCommande.removeItemAt(i);
							}
							catch(e:Error)
							{
							
							}
						}
					}
				}
			}
			_panier.attributMontant(_itemsSelected);
		}
		
//METODES PRIVATE------------------------------------------------------------------------

	}
}