package commandemobile.ihm
{
	import commandemobile.composants.PanierIHM;
	import commandemobile.entity.Commande;
	import commandemobile.entity.ItemSelected;
	import commandemobile.entity.Panier;
	import commandemobile.popup.FicheSiteIHM;
	import commandemobile.popup.PopUpChargerCommandeIHM;
	import commandemobile.system.Revendeur;
	import commandemobile.temp.commandeSNCF;
	import commandemobile.temp.temp;
	
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	import composants.util.lignes.LignesUtils;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class ParametresImpl extends Box
	{

//VARIABLES------------------------------------------------------------------------------

		//IHM
		private var _popUp						:PopUpChargerCommandeIHM;
		private var _popUpFicheSites			:FicheSiteIHM;
		public var composantPanier				:PanierIHM;

		//COMPOSANTS
		public var cbxPools						:ComboBox;
		public var cbxSites						:ComboBox;
		public var cbxCmdType					:ComboBox;
		public var cbxRevendeur					:ComboBox;
		public var cbxAgence					:ComboBox;
		public var cbxActeFor					:ComboBox;
		public var cbxContact					:ComboBox;
		public var cbxOperateur					:ComboBox;
		public var cbxTitulaire					:ComboBox;
		public var cbxPointFacturation			:ComboBox;
		public var cbxCodeList					:ComboBox;
		public var txtbxRIO						:TextInput;		
		public var txtLibelle					:TextInput;
		public var txtERPPrefixe				:TextInput;
		public var txtERPSuffixe				:TextInput;
		public var txtNumeroMarche				:TextInput;
		public var txtRefOp						:Label;
		public var lblNumCmd					:Label;		
		public var dcLivraisonPrevue			:CvDateChooser;
		public var txtAreaCommentaires			:TextArea;
		public var rdbtnNewTerminal				:RadioButton;
		public var rdbtnNoTerminal				:RadioButton;
		public var rdbtnterminalOnly			:RadioButton;
		public var rdbtnChargerConfiguration	:RadioButton;
		public var txtNameConfig				:Label;
		public var txtDateCommande				:Label;
		public var btnNewSite					:Button;
		public var btnParcourir					:Button;
		
		//OBJETS
		public var _cmd							:Commande;
		public var _panier						:Panier;
		public var _agence						:temp 			= new temp();
		private var _cmdSNCF					:commandeSNCF 	= new commandeSNCF();
		private var _numLine					:LignesUtils 	= new LignesUtils();
		private var _revendeur					:Revendeur 		= new Revendeur();
		public var _itemSelected				:ItemSelected 	= new ItemSelected();
		
		//Saut de fenetre suivant la sélection du terminal
		public var noSkip						:Boolean 		= true;
		public var skipAll						:Boolean 		= false;
		public var skipEquipment				:Boolean 		= false;
		public var skipSubscription				:Boolean 		= false;
		
		//VARIABLES LOCALES PUBLICS
		public var select						:Boolean		= true;
		public var numeroLigneUnique			:String 		= "";
		public var dateOfToday					:String;
		public var parameterFill				:Boolean 		= false;
		public var periodeObject				:Object 		= {rangeStart:new Date(new Date().getTime() + (2 * DateFunction.millisecondsPerDay) )};
		public var dateDuJourPlus2JoursOuvrees	:Date			= DateFunction.addXJoursOuvreesToDate(new Date(),5);
		public var createLine					:Boolean		= true;
		public var acte							:Boolean		= false;
		
		//VARIABLES LOCALES PRIVEES
		private var date						:Date 			= new Date();
		private var listArticles				:ArrayCollection = new ArrayCollection();
		private var raz							:Boolean		= true;
		private var newCommande					:Boolean		= true;
		private var configObject				:Object 		= new Object();
		
		//CONSTANTES
		public static const NOUVELLES_LIGNES	:Number = 1083;
		private const ABO_VOIX					:String = "Abo Voix";
		private const ABO_DATA					:String = "Abo Data";

//VARIABLES------------------------------------------------------------------------------

//METHODES PUBLIC------------------------------------------------------------------------
		
		//CONSTRUCTEUR
		public function ParametresImpl()
		{
			addEventListener("fournirPoolsDuGestionnaire",	initialisation);
		}

//METHODES PUBLIC------------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
		
		//INITIALISE 'Paramètres' (APPEL LES PROCEDURES : 'ListPoolsGestionnaire', 'GenererNumDeCommande', 'RechercheMesRevendeurs'
		protected function initialisation(e:Event):void
		{
			_agence.fournirPoolsDuGestionnaire();
			_cmdSNCF.fournirOperateur();

			dateOfToday = formatDate(date);
			addEventListener("nextPageClicked",	checkIfAttributToCommandeFill);
			addEventListener("allEnabledFalse",	allEnabledFalseHandler);
			
			_agence.addEventListener("listPool",			listPoolHandler);
			_agence.addEventListener("listSites",			listSitesHandler);
			_cmdSNCF.addEventListener("operateur",			operateurHandler);
			_cmdSNCF.addEventListener("profilEquipements",	profilEquipementsHandler);
			_cmdSNCF.addEventListener("titulaire",			titulaireHandler);
			_cmdSNCF.addEventListener("pointFacturation",	pointFacturationHandler);
			_cmdSNCF.addEventListener("codeListe", 			codeListeHandler);
			_cmdSNCF.addEventListener("revendeurs",			revendeursHandler);
			_agence.addEventListener("listAgences",			listAgencesHandler);
			_agence.addEventListener("listContacts",		listContactsHandler);
			_agence.addEventListener("listActePour",		listActePourHandler);
			addEventListener("_panier",						composantPanierCreationCompleteHandler);
		}

		protected function cbxActeForChangeHandler():void
		{
			if(cbxActeFor.selectedItem != null)
			{
				cbxActeFor.errorString = "";
			}
		}

		protected function btnNewSiteClickHandler():void
		{
			if(cbxPools.selectedItem !=null)
			{
				_popUpFicheSites = new FicheSiteIHM();
				_popUpFicheSites.idPoolGestionnaire = cbxPools.selectedItem.IDPOOL;
				PopUpManager.addPopUp(_popUpFicheSites,this,true);
				PopUpManager.centerPopUp(_popUpFicheSites);
				_popUpFicheSites.addEventListener("refreshSite",ficheSiteHandler);
			}
			else
			{
				Alert.show("Veuillez sélectionner un pool!","Consoview");
			}
		}
		
		private function ficheSiteHandler(e:Event):void
		{
			_agence.fournirListeSiteLivraisonsPoolGestionnaire(cbxPools.selectedItem.IDPOOL);
		}

		protected function composantPanierCreationCompleteHandler(e:Event):void
		{
			composantPanier.panier = _panier;
		}
		
		private function listPoolHandler(e:Event):void //**************************************************			OK
		{
			cbxPools.prompt				= "";
			cbxPools.dataProvider 		= null;
			cbxSites.dataProvider 		= null;
			cbxRevendeur.prompt 		= "";
			cbxRevendeur.dataProvider 	= null;
			cbxAgence.dataProvider 		= null;
			cbxContact.dataProvider 	= null;
			if(_agence.listePools.length == 1)
			{
				cbxPools.dataProvider 	= _agence.listeConcatPools[0];
				cbxPools.errorString 	= "";
				_cmdSNCF.fournirProfilEquipements();
				_agence.fournirListeSiteLivraisonsPoolGestionnaire(cbxPools.selectedItem.IDPOOL);
			}
			else
			{
				if(_agence.listePools.length == 0)
				{
					cbxPools.prompt = "Aucun pool";
				}
				else
				{
					cbxPools.prompt = "Sélectionnez";
					cbxPools.dataProvider = _agence.listeConcatPools;
				}
			}
		}
		
		private function profilEquipementsHandler(e:Event):void//**************************************************			OK
		{
			cbxCmdType.prompt			= "";
			cbxCmdType.dataProvider 	= null;
			cbxRevendeur.prompt 		= "";
			cbxAgence.prompt 			= "";
			cbxContact.prompt 			= "";
			cbxActeFor.prompt 			= "";
			cbxRevendeur.dataProvider 	= null;
			cbxAgence.dataProvider 		= null;
			cbxContact.dataProvider 	= null;
			cbxActeFor.dataProvider 	= null;
			if(_cmdSNCF.profilEquipements.length == 1)
			{
				cbxCmdType.dataProvider = _cmdSNCF.profilEquipements[0];
				_cmdSNCF.fournirRevendeurs(cbxPools.selectedItem.IDPOOL,_cmdSNCF.profilEquipements[0].IDPROFIL_EQUIPEMENT);
				cbxCmdType.errorString 	= "";
			}
			else
			{
				if(_cmdSNCF.profilEquipements.length == 0)
				{
					cbxCmdType.prompt = "Aucun profil";
				}
				else
				{
					cbxCmdType.prompt = "Sélectionnez";
					cbxCmdType.dataProvider = _cmdSNCF.profilEquipements;
				}
			}
		}
		
		private function listSitesHandler(e:Event):void//**************************************************			OK
		{
			cbxSites.prompt			= "";
			cbxSites.dataProvider 	= null;
			if(_agence.listeSites.length == 1)
			{
				cbxSites.dataProvider 	= _agence.listeSites[0];
				cbxSites.errorString 	= "";
			}
			else
			{
				if( _agence.listeSites.length == 0)
				{
					cbxSites.prompt = "Aucun sites";
				}
				else
				{
					cbxSites.prompt = "Sélectionnez";
					cbxSites.dataProvider = _agence.listeSites;
				}
			}
		}
		
		private function operateurHandler(e:Event):void//**************************************************			OK
		{			
			if(_cmdSNCF.operateur.length > 0)
			{
				cbxOperateur.dataProvider = _cmdSNCF.operateur[0];
				_cmdSNCF.fournirTitulaire(cbxOperateur.selectedItem.OPERATEURID);
			}	
		}

		private function titulaireHandler(e:Event):void//**************************************************			OK
		{
			cbxTitulaire.prompt					= "";
			cbxTitulaire.dataProvider 			= null;
			cbxPointFacturation.dataProvider 	= null;
			cbxCodeList.dataProvider 			= null;
			if(_cmdSNCF.titulaire.length == 1)
			{
				cbxTitulaire.dataProvider 	= _cmdSNCF.titulaire[0];
				cbxTitulaire.errorString	= "";
				_cmdSNCF.fournirPointFacturation(cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION);
			}
			else
			{
				if(_cmdSNCF.titulaire.length == 0)
				{
					cbxTitulaire.prompt = "Aucun titulaire";
				}
				else
				{
					cbxTitulaire.prompt = "Sélectionnez";
					cbxTitulaire.dataProvider = _cmdSNCF.titulaire;
				}
			}			
		}
		
		private function pointFacturationHandler(e:Event):void//**************************************************			OK
		{
			cbxPointFacturation.prompt			= "";
			cbxPointFacturation.dataProvider 	= null;
			cbxCodeList.dataProvider 			= null;
			if(_cmdSNCF.pointFacturation.length == 1)
			{
				cbxPointFacturation.errorString		= "";
				cbxPointFacturation.dataProvider 	= _cmdSNCF.pointFacturation[0];
				_cmdSNCF.fournirCodeListe(cbxPointFacturation.selectedItem.IDSOUS_COMPTE);
			}
			else
			{
				if(_cmdSNCF.pointFacturation.length == 0)
				{
					cbxPointFacturation.prompt = "Aucun point de facturation";
				}
				else
				{
					cbxPointFacturation.prompt = "Sélectionnez";
					cbxPointFacturation.dataProvider = _cmdSNCF.pointFacturation;
				}
			}
		}

		private function codeListeHandler(e:Event):void//**************************************************			OK MAIS POUR LE MOMENT IL N'Y A RIEN
		{
			cbxCodeList.prompt		 = "";
			cbxCodeList.dataProvider = null;
			if(_cmdSNCF.codeListe.length == 1)
			{
				cbxCodeList.errorString		= "";
				cbxCodeList.dataProvider 	= _cmdSNCF.codeListe[0];
			}
			else
			{
				if(_cmdSNCF.codeListe.length == 0)
				{
					cbxCodeList.prompt = "Aucun code liste";
				}
				else
				{
					cbxCodeList.prompt = "Sélectionnez";
					cbxCodeList.dataProvider = _cmdSNCF.codeListe;
				}
			}
		}

		private function listActePourHandler(e:Event):void
		{
			cbxActeFor.prompt 				= "";
			cbxActeFor.dataProvider 		= null;
			if( _agence.listActePour.length == 1)
			{
				cbxActeFor.dataProvider 	=  _agence.listActePour[0];
				cbxActeFor.errorString		= "";
			}
			else
			{
				if(_agence.listActePour.length == 0)
				{
					cbxActeFor.prompt 		= "Aucun revendeur";
				}
				else
				{
					cbxActeFor.prompt 		= "Sélectionnez";
					cbxActeFor.dataProvider =  _agence.listActePour;
				}
			}		
		}

		private function revendeursHandler(e:Event):void//**************************************************			OK
		{
			cbxRevendeur.prompt 		= "";
			cbxRevendeur.dataProvider 	= null;
			cbxAgence.prompt 			= "";
			cbxAgence.dataProvider 		= null;
			cbxContact.prompt 			= "";
			cbxContact.dataProvider 	= null;
			cbxActeFor.prompt 			= "";
			cbxActeFor.dataProvider 	= null;
			acteToDo();
			if( _cmdSNCF.revendeurs.length == 1)
			{
				cbxRevendeur.dataProvider 	= _cmdSNCF.revendeurs[0];
				cbxRevendeur.errorString	= "";
				castingInRevendeur(_cmdSNCF.revendeurs[0]);
				_agence.rechercherAgencesRevendeur("",_revendeur);
				if(acte)
				{
					_agence.functionActePour(cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT,cbxRevendeur.selectedItem.IDREVENDEUR);
				}
			}
			else
			{
				if(_cmdSNCF.revendeurs.length == 0)
				{
					cbxRevendeur.prompt = "Aucun revendeur";
				}
				else
				{
					cbxRevendeur.prompt = "Sélectionnez";
					cbxRevendeur.dataProvider = _cmdSNCF.revendeurs;
				}
			}		
		}
		
		private function acteToDo():void
		{
			if(cbxPools.selectedItem != null)
			{
				if(cbxPools.selectedItem.IDREVENDEUR != null || cbxPools.selectedItem.IDREVENDEUR > 0)
				{
					acte = true;
					for(var i:int = 0;i < _cmdSNCF.revendeurs.length;i++)
					{
						if( _cmdSNCF.revendeurs[i].IDREVENDEUR != cbxPools.selectedItem.IDREVENDEUR)
						{
							_cmdSNCF.revendeurs.removeItemAt(i);
							i--;
						}
					}
					if( _cmdSNCF.revendeurs.length == 0)
					{
						acte = false;
					}
				}
				else
				{
					acte = false;
				}
			}
		}
		
		private function listAgencesHandler(e:Event):void//**************************************************			OK
		{
			cbxAgence.prompt 		= "";
			cbxAgence.dataProvider 	= null;
			cbxContact.dataProvider = null;
			if(_agence.listeAgences.length == 1)
			{
				cbxAgence.prompt 		= "";
				cbxAgence.errorString 	= "";
				cbxAgence.dataProvider 	= _agence.listeAgences[0];
				_agence.rechercherContactsRevendeur("",_revendeur);
			}
			else
			{
				if(_agence.listeAgences.length == 0)
				{
					cbxAgence.prompt = "Aucune agence";
				}
				else
				{
					cbxAgence.prompt = "Sélectionnez";
					cbxAgence.dataProvider = _agence.listeAgences;
				}
			}		
		}
		
		private function listContactsHandler(e:Event):void
		{
			cbxContact.prompt 		= "";
			cbxContact.dataProvider = null;
			if(_agence.listeContacts.length == 1)
			{
				cbxContact.dataProvider = _agence.listeContacts[0];
				cbxContact.prompt = "";
			}
			else
			{
				if(_agence.listeContacts.length == 0)
				{
					cbxContact.prompt = "Aucun contact";
				}
				else
				{
					cbxContact.prompt = "Sélectionnez";
					cbxContact.dataProvider = _agence.listeContacts;
				}
			}			
		}
		
		//SE PRODUIT LORS DE LA FERMETURE DU COMBOBOX 'Sélectionnez un pools'
		protected function cbxPoolsCloseHandler():void//**************************************************			OK
		{
			if(cbxPools.selectedItem != null)
			{
				if(cbxPools.selectedItem.LIBELLE_CONCAT != "Sélectionnez")
				{
					_cmdSNCF.fournirProfilEquipements();
					_agence.fournirListeSiteLivraisonsPoolGestionnaire(cbxPools.selectedItem.IDPOOL);
					cbxPools.errorString = "";
				}
			}
		}
		
		protected function cbxCmdTypeChangeHandler():void//**************************************************			OK
		{
			cbxRevendeur.dataProvider = null;
			if(cbxCmdType.selectedItem != null)
			{
				_cmdSNCF.fournirRevendeurs(cbxPools.selectedItem.IDPOOL,cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT);
				cbxCmdType.errorString = "";
			}
		}
		
		//SE PRODUIT LORS DE LA FERMETURE DU COMBOBOX 'Sélectionnez un site'
		protected function cbxSitesCloseHandler():void//**************************************************			OK
		{
			cbxSites.errorString = "";
		}
		
		//DE PRODUIT LORS DE LA FERMETURE DU COMBOBOX 'Sélectionnez un sous compte'
		protected function cbxTitulaireChangeHandler():void//**************************************************			OK
		{
			if(cbxTitulaire.selectedItem != null)
			{
				_cmdSNCF.fournirPointFacturation(cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION);
				cbxTitulaire.errorString = "";
			}
		}
		
		//DE PRODUIT LORS DE LA FERMETURE DU COMBOBOX 'Sélectionnez un compte' (liste les sous comptes)
		protected function cbxPointFacturationChangeHandler():void//**************************************************			OK
		{
			if(cbxPointFacturation.selectedItem != null)
			{
				_cmdSNCF.fournirCodeListe(cbxPointFacturation.selectedItem.IDSOUS_COMPTE);
				cbxPointFacturation.errorString = "";
			}
		}

		//SE PRODUIT LORS DE LA FERMETURE DU COMBOBOX 'Sélectionnez un revendeur' (liste les agences)
		protected function cbxRevendeurChangeHandler():void//**************************************************			OK
		{
			if(cbxRevendeur.selectedItem != null)
			{
				if(cbxRevendeur.selectedItem.LIBELLE != "Sélectionnez")
				{
					castingInRevendeur(cbxRevendeur.selectedItem);
					_agence.rechercherAgencesRevendeur("",_revendeur);
					if(acte)
					{
						_agence.functionActePour(cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT,cbxRevendeur.selectedItem.IDREVENDEUR);
					}
					dispatchEvent(new Event("revendeurChange",true));
					_cmd.IDREVENDEUR = cbxRevendeur.selectedItem.IDREVENDEUR;
					cbxRevendeur.errorString = "";
				}
			}
		}
		
		//SE PRODUIT LORS DE LA FERMETURE DU COMBOBOX 'Sélectionnez une agence' (liste les contacts)
		protected function cbxAgenceChangeHandler():void
		{
			if(cbxAgence.selectedItem != null)
			{
				if(cbxAgence.selectedItem.LIBELLE != "Sélectionnez")
				{
					_agence.rechercherContactsRevendeur("",_revendeur);
					cbxAgence.errorString = "";
				}
			}
		}

		//AFFECTE LE 'LIBELLE_COMMANDE' à 'Référence client' ou 'Libellé' LORS DU CLIC SUR LE LABEL 'recopier n° de commande'
		protected function lblclickHendler(bool:Boolean):void
		{
			if(bool)
			{
				txtLibelle.text 	= _cmd.NUMERO_COMMANDE;
				txtLibelle.errorString = "";
			}
		}
		
		protected function textInputHandler(txtbxNumber:int):void
		{
			switch(txtbxNumber)
			{
				case 0: txtLibelle.errorString 		= "";break;
				case 2: txtRefOp.errorString 		= "";break;
				case 3: lblNumCmd.errorString 		= "";break;
				case 4: txtERPPrefixe.errorString 	= "";break;
				case 5: txtERPSuffixe.errorString 	= "";break;
				case 6: txtNumeroMarche.errorString = "";break;
			}
		}

		//ENVOIE A 'MainNewCommande' LE PASSAGE D'UNE COMMANDE NORMALE
		protected function rdbtnNewTerminalChangeHandler():void
		{
			rdbtnNewTerminal.selected = true;
			dispatchEvent(new Event("revendeurChange",true));
			createLine = true;
			skipOrNot(true,false,false);
			btnParcourir.enabled = false;
			razLoadConfiguration();
			select = true;
			if(!newCommande)
			{
				cbxTitulaire.enabled 		= false;
				cbxPointFacturation.enabled = false;
				cbxCodeList.enabled 		= false;
			}
		}
		
		//ENVOIE A 'MainNewCommande' LE PASSAGE DIRECTE A L'ETAPE 'Choix des abonnements'
		protected function rdbtnNoTerminalChangeHandler():void
		{
			rdbtnNoTerminal.selected = true;
			dispatchEvent(new Event("revendeurChange",true));
			createLine = true;
			skipOrNot(false,true,false);
			btnParcourir.enabled = false;
			razLoadConfiguration();
			select = true;
			if(!newCommande)
			{
				cbxTitulaire.enabled 		= false;
				cbxPointFacturation.enabled = false;
				cbxCodeList.enabled 		= false;
			}
		}
		
		//ENVOIE A 'MainNewCommande' LE PASSAGE DIRECTE A L'ETAPE 'Choix des équipements - Terminal (1/2)'
		protected function rdbtnterminalOnlyChangeHandler():void
		{
			rdbtnterminalOnly.selected = true;
			dispatchEvent(new Event("revendeurChange",true));
			createLine = false;
			
			skipOrNot(false,false,true);
			btnParcourir.enabled = false;
			razLoadConfiguration();
			select = true;
			if(!newCommande)
			{
				cbxTitulaire.enabled 		= false;
				cbxPointFacturation.enabled = false;
				cbxCodeList.enabled 		= false;
			}
			errorStringZERO();
		}

		private function errorStringZERO():void
		{
			cbxTitulaire.errorString 		= "";
			cbxPointFacturation.errorString = "";
			cbxCodeList.errorString 		= "";
		}
		
		//ENVOIE A 'MainNewCommande' LE PASSAGE DIRECTE A L'ETAPE 'Choix des quantités'
		protected function rdbtnChargerConfigurationChangeHandler():void
		{
			if(cbxPools.selectedItem != null)
			{
				if(cbxCmdType.selectedItem != null)
				{
					rdbtnChargerConfiguration.selected = true;
					dispatchEvent(new Event("revendeurChange",true));
					skipOrNot(false,false,false,true);
					btnParcourir.enabled = true;
					razLoadConfiguration();
					select = false;	
				}
				else
				{
					rdbtnChargerConfiguration.selected 	= false;
					Alert.show("Sélectionnez un type de commande!","Consoview");
					callLater(rdbtnNewTerminalChangeHandler);
					
				}
			}
			else
			{
				rdbtnChargerConfiguration.selected 	= false;
				Alert.show("Sélectionnez un pool!","Consoview");
				callLater(rdbtnNewTerminalChangeHandler);
				
			}
		}
		
		protected function btnParcourirClickHandler():void
		{
			 popUpParcourir();
		}
		
		protected function razLoadConfiguration():void
		{
			txtNameConfig.text = "";
			configObject = new Object();
		}
	    
//METHODES PROTECTED---------------------------------------------------------------------

//METHODES PRIVATE-----------------------------------------------------------------------

		private function popUpParcourir():void
		{
			if(cbxPools.selectedItem != null && cbxPools.selectedItem != 0)
			{
				_popUp = new PopUpChargerCommandeIHM();
				_popUp.idPool 	= cbxPools.selectedItem.IDPOOL;
				if(cbxCmdType.selectedItem != null)
				{
					_popUp.idProfil = cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT;
				}
				PopUpManager.addPopUp(_popUp,this,true);
				PopUpManager.centerPopUp(_popUp);
				_popUp.dispatchEvent(new Event("listerConfiguration"));
				_popUp.addEventListener("formatInArrayCollectionFinished",configurationSelectedHandler);
			}
			else
			{
				Alert.show("Veuillez sélectionner un pool!","Consoview")
			}
		}
		
		private function configurationSelectedHandler(e:Event):void
		{
			listArticles = _popUp.listArticles;
			configObject = _popUp.configObject;
			txtNameConfig.text = configObject.LIBELLE;

			_popUp.dispatchEvent(new Event("closePopUp"));
			formatConfigurationInItemSelected();
		}
		
		private function formatConfigurationInItemSelected():void
		{
			if(listArticles && listArticles.length > 0)
			{
				_itemSelected = new ItemSelected();
				for(var j:int = 0; j < listArticles[0].EQUIPEMENTS.length;j++)
				{
					_itemSelected.TERMINAUX.addItem(listArticles[0].EQUIPEMENTS[j]);
				}	
				for(var i:int = 0; i < listArticles[0].RESSOURCES.length;i++)
				{
					if(listArticles[0].RESSOURCES[i].TYPE_EQUIPEMENT == ABO_DATA || listArticles[0].RESSOURCES[i].TYPE_EQUIPEMENT == ABO_VOIX)
					{
						_itemSelected.ABO.addItem(listArticles[0].RESSOURCES[i]);
					}
					else
					{
						_itemSelected.OPTIONS.addItem(listArticles[0].RESSOURCES[i]);
					}
				}	
			}
		}
		
		private function configurationNotSelectedHandler(e:Event):void
		{
		}
		
		//FORMATE LA DATE EN STRING
		private function formatDate(dateobject:Object):String
		{
			var day:String = dateobject.date;
			if(day.length < 2)
			{day = "0" + day;}
			var month:int = Number(dateobject.month) + 1;
			var temp:String = month.toString();
			if(temp.length < 2)
			{temp = "0" + temp;}
			var year:String = dateobject.fullYear;
			return day + "/" + temp + "/" + year;
		}

		//ATTRIBUT LES VALEURS SAISI PAR L'UTILISATEUR A L'OBJET 'Commande'
		private function attributToCommande():Boolean
		{
			var bool:Boolean = false;
			var check:Boolean = true;
			try{
					if(!acte)
					{
						_cmd.IDGESTIONNAIRE = CvAccessManager.getSession().USER.CLIENTACCESSID;
					}
					if(rdbtnChargerConfiguration.selected)
					{
						if(listArticles == null || listArticles.length == 0)
						{
							check =  false;
						}
					}
					if(cbxPools.selectedItem != null)
					{
						if(!acte)
						{
							_cmd.IDPOOL_GESTIONNAIRE 	= cbxPools.selectedItem.IDPOOL;
							_cmd.IDPROFIL 				= cbxPools.selectedItem.IDPROFIL;
						}
						else
						{
							if(cbxActeFor.selectedItem != null)
							{
								_cmd.IDPOOL_GESTIONNAIRE 	= cbxActeFor.selectedItem.IDPOOL;
								_cmd.IDPROFIL 				= cbxActeFor.selectedItem.IDPROFILE;
							}
							else
							{
								cbxActeFor.errorString = "Veuillez sélectionner un gestionnaire!";
								check =  false;
							}
						}
					}
					else
					{
						cbxPools.errorString = "Veuillez saisir votre pool!";
						check =  false;
					}
					if(txtLibelle.text != "")
					{
						_cmd.LIBELLE_COMMANDE 		= txtLibelle.text;
					}
					else
					{
						txtLibelle.errorString = "Veuillez saisir le libellé de la commande!";
						check =  false;
					}
					if(txtERPPrefixe.text != "" && txtERPSuffixe.text != "")
					{
						_cmd.REF_CLIENT 			= txtERPPrefixe.text + "-" + txtERPSuffixe.text;//A VERIFIER DAN SI LA PROCEDURE SI 
						if(_cmd.REF_CLIENT.length != 16)										//ON AJOUTE PAS UN CHAMP SPECIAL ERP AU LIEU DE REF CLIENT
						{
							check =  false;
							txtERPPrefixe.errorString = "Vérifier la longueur de votre numéro ERP!";
							txtERPSuffixe.errorString = "Vérifier la longueur de votre numéro ERP!";
						}
						else
						{
							txtERPPrefixe.errorString = "";
							txtERPSuffixe.errorString = "";
						}
					}
					else
					{
						if(txtERPPrefixe.text == "" )
						{
							txtERPPrefixe.errorString = "Veuillez saisir votre numéro ERP";
						}
						if(txtERPSuffixe.text == "")
						{
							txtERPSuffixe.errorString = "Veuillez saisir votre numéro ERP";
						}
						check =  false;
					}
					if(txtRefOp.text != "")
					{
						_cmd.REF_OPERATEUR 			= txtRefOp.text;
					}
					else
					{
						txtRefOp.errorString = "Veuillez saisir la référence opérateur!";
						check =  false;
					}
					if(lblNumCmd.text != "")
					{
						_cmd.NUMERO_COMMANDE 		= lblNumCmd.text;
					}
					else
					{
						lblNumCmd.errorString = "Pas de numéro de commande!";
						check =  false;
					}
					if(dcLivraisonPrevue.selectedDate != null)
					{
						_cmd.LIVRAISON_PREVUE_LE 	= dcLivraisonPrevue.selectedDate;
					}
					else
					{
						dcLivraisonPrevue.errorString = "Veuillez saisir une date de livraison!";
						check =  false;
					}
					if(cbxSites.selectedItem != null)
					{
						_cmd.LIBELLE_SITELIVRAISON 	= cbxSites.selectedItem.NOM_SITE;
						_cmd.IDSITELIVRAISON 		= cbxSites.selectedItem.IDSITE_PHYSIQUE;
					}
					else
					{
						cbxSites.errorString = "Veuillez saisir un site de livraison!";
						check =  false;
					}
					if(cbxCmdType.selectedItem != null)
					{
						_cmd.IDPROFIL_EQUIPEMENT 	= cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT;
					}
					else
					{
						cbxCmdType.errorString = "Veuillez saisir un type de commande!";
						check =  false;
					}
					if(cbxOperateur.selectedItem != null)
					{
						_cmd.IDOPERATEUR 			= cbxOperateur.selectedItem.OPERATEURID;
						_cmd.LIBELLE_OPERATEUR 		= cbxOperateur.selectedItem.LIBELLE;
					}
					else
					{
						cbxOperateur.errorString = "Veuillez sélectionner un opérateur!";
						check =  false;
					}
					if(cbxTitulaire.selectedItem != null)
					{
						_cmd.IDCOMPTE_FACTURATION	= cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION;
						_cmd.LIBELLE_COMPTE			= cbxTitulaire.selectedItem.COMPTE_FACTURATION;
					}
					else
					{
						if(createLine)
						{
							cbxTitulaire.errorString = "Veuillez sélectionner un titulaire!";
							check =  false;
						}
						else
						{
							cbxTitulaire.errorString = "";
							
						}
					}
					if(cbxPointFacturation.selectedItem != null)
					{
						_cmd.IDSOUS_COMPTE		= cbxPointFacturation.selectedItem.IDSOUS_COMPTE;
						_cmd.LIBELLE_SOUSCOMPTE	= cbxPointFacturation.selectedItem.SOUS_COMPTE;
					}
					else
					{
						if(createLine)
						{
							cbxPointFacturation.errorString = "Veuillez saisir un point de facturation!";
							check =  false;
						}
						else
						{
							cbxPointFacturation.errorString = "";
						}
					}
					if(cbxCodeList.selectedItem != null)
					{
						_cmd.LIBELLE_CODELISTE	= cbxCodeList.selectedItem.CODE_LISTE;
					}

					if(select)
					{
						if(cbxRevendeur.selectedItem != null)
						{
							_cmd.LIBELLE_REVENDEUR 		= cbxRevendeur.selectedItem.LIBELLE;
							_cmd.IDSOCIETE 				= cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE;
							_cmd.IDREVENDEUR			= cbxRevendeur.selectedItem.IDREVENDEUR;
						}
						else
						{
							cbxRevendeur.errorString = "Veuillez sélectionner un revendeur!";
							check =  false;
						}
						if(cbxAgence.selectedItem != null)
						{

						}
						else
						{
							cbxAgence.errorString = "Veuillez sélectionner une agence!";
							check =  false;
						}
						if(cbxContact.selectedItem != null)
						{
							_cmd.EMAIL_CONTACT 			= cbxContact.selectedItem.EMAIL;
							_cmd.IDCONTACT 				= cbxContact.selectedItem.IDCDE_CONTACT;
						}
					}

					if(acte)
					{
						if(cbxActeFor.selectedItem != null)
						{
							_cmd.IDACTEPOUR		= cbxActeFor.selectedItem.APPLOGINID;
							_cmd.IDGESTIONNAIRE =  cbxActeFor.selectedItem.APPLOGINID;
						}
						else
						{
							cbxActeFor.errorString = "Veuillez sélectionner un gestionnaire!";
							check =  false;
						}
						if(cbxRevendeur.selectedItem != null)
						{
							_cmd.LIBELLE_REVENDEUR 		= cbxRevendeur.selectedItem.LIBELLE;
							_cmd.IDSOCIETE 				= cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE;
							_cmd.IDREVENDEUR			= cbxRevendeur.selectedItem.IDREVENDEUR;
						}
						else
						{
							cbxRevendeur.errorString = "Veuillez sélectionner un revendeur!";
							check =  false;
						}
						if(cbxContact.selectedItem != null)
						{
							_cmd.EMAIL_CONTACT 			= cbxContact.selectedItem.EMAIL;
							_cmd.IDCONTACT 				= cbxContact.selectedItem.IDCDE_CONTACT;
						}
					}

					
					if(check)
					{
						if(select)
						{
							_cmd.COMMENTAIRES 				= txtAreaCommentaires.text;
							_cmd.DATE_COMMANDE 				= date; 
							_cmd.ENCOURS 					= 1;
							_cmd.ACTION			 			= "A";
							_cmd.IDTYPE_COMMANDE 			= NOUVELLES_LIGNES;
						}
						else
						{
							_cmd.IDPOOL_GESTIONNAIRE 		= configObject.IDPOOL;
							_cmd.COMMENTAIRES 				= txtAreaCommentaires.text;
							_cmd.DATE_COMMANDE 				= date; 
							_cmd.ENCOURS 					= 1;
							_cmd.ACTION						= "A";
							_cmd.MONTANT 					= configObject.MONTANT;
							_cmd.IDGROUPE_REPERE 			= configObject.IDGROUPE_REPERE;
							_cmd.IDTYPE_COMMANDE			= configObject.IDTYPE_OPERATION;
							_cmd.IDCONTACT					= configObject.IDCDE_CONTACT
							_cmd.IDREVENDEUR				= configObject.IDREVENDEUR;
							_cmd.REF_OPERATEUR 				= configObject.REF_REVENDEUR;
						}
					}
					bool = check;
			}
			catch(e:Error)
			{
				bool = false;
			}
			return bool;
		}
		
		//FORMATE 'IDCDE_CONTACT_SOCIETE' en 'IDCDE_CONTACT_SOCIETE'
		private function castingInRevendeur(obj:Object):void
		{
			_revendeur.IDCDE_CONTACT_SOCIETE = obj.IDCDE_CONTACT_SOCIETE;
		}
		
		//REGARDE S'IL FAUT SAUTER DES ETAPES OU NON ET LES ENVOIE A 'MainNewCommande'
		private function skipOrNot(bool0:Boolean,bool1:Boolean,bool2:Boolean,bool3:Boolean = false):void
		{
			noSkip = bool0;
			skipAll = bool3;
			skipEquipment = bool1;
			skipSubscription = bool2;
		}

//EVENT----------------------------------------------------------------------------------
		
		//'enable' TOUS LES COMPOSANTS QUI NE DOIVENT PAS ETRE CHANGES
		private function allEnabledFalseHandler(e:Event):void
		{
			cbxPools.enabled 				= false;
			cbxSites.enabled 				= false;
			cbxCmdType.enabled 				= false;
			cbxRevendeur.enabled 			= false;
			cbxAgence.enabled 				= false;
			cbxContact.enabled 				= false;
			cbxOperateur.enabled 			= false;
			cbxTitulaire.enabled 			= false;
			cbxPointFacturation.enabled 	= false;
			cbxCodeList.enabled 			= false;
			txtLibelle.enabled 				= false;
			txtERPPrefixe.enabled 			= false;
			txtERPSuffixe.enabled 			= false;
			txtRefOp.enabled 				= false;
			txtDateCommande.enabled 		= false;
			dcLivraisonPrevue.enabled 		= false;
			txtAreaCommentaires.enabled 	= false;
			btnNewSite.enabled				= false;
			newCommande = false;
		}

		//VERIFIE SI TOUS LES CHAMPS ONT BIEN ETE REMPLIS ET ATTRIBUT LES ACCESSOIRES SELECTIONNES A L'OBJET 'ItemSelected'
		private function checkIfAttributToCommandeFill(e:Event):void
		{		
			parameterFill = attributToCommande();
		}
		
//EVENT----------------------------------------------------------------------------------		

//METHODES PRIVATE-----------------------------------------------------------------------		

	}
}
