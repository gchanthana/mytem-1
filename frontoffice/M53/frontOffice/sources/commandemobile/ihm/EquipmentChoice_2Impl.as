package commandemobile.ihm
{
	import commandemobile.composants.PanierIHM;
	import commandemobile.entity.Commande;
	import commandemobile.entity.ItemSelected;
	import commandemobile.entity.Panier;
	import commandemobile.popup.PopUpFicheProduitAccessoireIHM;
	import commandemobile.temp.temp;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class EquipmentChoice_2Impl extends Box
	{

//VARIABLES------------------------------------------------------------------------------
		
		//COMPOSANTS
		public var dgListAccessoire			:DataGrid;
		public var txtptSearch				:TextInput;
		public var composantPanier			:PanierIHM;
		
		//OBJETS
		public var _cmd						:Commande;
		public var _itemsSelected			:ItemSelected;
		public var _panier					:Panier;
		public var _temp					:temp = new temp();
		
		//VARIABLES GLOBALES
		public var newView					:Boolean = true;
		public var goprevious				:Boolean = true;
		public var parameterFill			:Boolean = false;
		public var selectedNumber			:Number = 0;
		public var totalNumber				:Number = 0;
		public var indexObjectSelected		:int = 0;
		
		//VARIABLES LOCALES
		private var _accessoire				:ArrayCollection = new ArrayCollection();
		private var _listAccessoireVisible	:ArrayCollection = new ArrayCollection();
		
		//CONSTANTES
		private const ACCESSOIRES			:String = "Accessoires";
		private const EVENT_SELECTED		:String = "equipmentSelected";
		private const EVENT_NO_SELECTED		:String = "equipmentNotSelected";
		
//VARIABLES------------------------------------------------------------------------------

//PROPRIETEES PUBLICS--------------------------------------------------------------------
		
		//CONTIENT TOUS LES ACCESSOIRES RECU PAR LA PROCEDURE
		public function get accessoire():ArrayCollection
		{
			return _accessoire;
		}
			
		public function set accessoire(values:ArrayCollection):void
		{
			_accessoire = values;
		}
		
//PROPRIETEES PUBLICS--------------------------------------------------------------------

//METHODES PUBLICS-----------------------------------------------------------------------
		
		//CONSTRUCTEUR
		public function EquipmentChoice_2Impl()
		{
			_temp.addEventListener("listeProductRelatedOk", 			listAccessoires);
			_temp.addEventListener("listeProductRelatedNoAccessory", 	noAccessory);
			addEventListener("nextPageClicked",							checkIfAttributToCommandeFill);
			addEventListener("accessorySelect", 						detailsThisAccessorySelected);
			addEventListener("equipmentSelected", 						thisIsAccessoireSelected);
			addEventListener("equipmentNotSelected", 					thisIsNotAccessoireSelected);
			addEventListener("thisIsFavorite", 							thisIsFavoriteSelected);
			addEventListener("thisIsNotFavorite", 						thisIsNotFavoriteSelected);
		}

//METHODES PUBLICS-----------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
		
		protected function composantPanierCreationCompleteHandler():void
		{
			composantPanier.panier = _panier;
		}
		
		//APPEL LA PROCEDURE PERMETTANT D'AFFICHER LES ACCESSOIRES DE L'APPREIL SELECTIONNE
		protected function showHandler():void
		{	
			var idEquipement:int = _itemsSelected.TERMINAUX[0].IDEQUIPEMENT_FOURNISSEUR;
			_temp.listeProductRelated = new ArrayCollection();
			accessoire = new ArrayCollection();
			totalNumber		= 0;
			selectedNumber 	= 0;
			if(idEquipement != 0)
			{
				_temp.getProductRelated(idEquipement);
			}
			else
			{
				_temp.fournirListeAccessoiresCatalogueClient(_cmd.IDREVENDEUR,"");
			}
			composantPanierCreationCompleteHandler();
		}
		
		//OUVRE UNE POPUP PERMETTANT DE VISUALISER LES DETAILS DE L'ACCESSOIRE SELECTIONNE
		protected function detailsThisAccessorySelected(e:Event):void
		{
			if(dgListAccessoire.selectedItem != null)
			{
				var popUpDetails:PopUpFicheProduitAccessoireIHM = new PopUpFicheProduitAccessoireIHM();
				PopUpManager.addPopUp(popUpDetails,this,true);
				PopUpManager.centerPopUp(popUpDetails);
				popUpDetails.detailsProduitReceived = dgListAccessoire.selectedItem;
				popUpDetails.dispatchEvent(new Event("detaillerProduit"));
			}
		}
		
		//FILTRE TEXTE
		protected function txtptSearchHandler():void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			accessoire = new ArrayCollection();
			if(txtptSearch.text == "")
			{
				accessoire = _listAccessoireVisible;
			}
			else
			{
				for(var i:int = 0;i < _listAccessoireVisible.length;i++)
				{
					if(_listAccessoireVisible[i].LIBELLE_PRODUIT.toString().toLowerCase().indexOf(txtptSearch.text.toLowerCase()) != -1)
					{ 
						arrayC.addItem(_listAccessoireVisible[i]); 
					}
				}
				accessoire = arrayC;
			}
		}

//METHODES PROTECTED---------------------------------------------------------------------

//METHODES PRIVATE-----------------------------------------------------------------------

//EVENT----------------------------------------------------------------------------------		

		//LISTE LES ACCESSOIRES
		private function listAccessoires(e:Event):void
		{
			if(_temp.listeProductRelated.length > 0)
			{
				accessoire 				= new ArrayCollection();
				_listAccessoireVisible 	= new ArrayCollection();
				for(var i:int = 0;i < _temp.listeProductRelated.length;i++)
				{
					accessoire.addItem(_temp.listeProductRelated[i]);
					_listAccessoireVisible.addItem(_temp.listeProductRelated[i]);
				}
			}
			else
			{
				if(!goprevious)
				{
					dispatchEvent(new Event("goToNextPageNoAccessoires",true));//IMPLEMENTATION DE PASSAGE A UNE PAGE
				}
				else
				{
					dispatchEvent(new Event("goToPreviousPageNoAccessoires",true));//IMPLEMENTATION DE PASSAGE A UNE PAGE
				}
			}
		}
		
		private function noAccessory(e:Event):void
		{
			if(_itemsSelected.TERMINAUX[0].IDEQUIPEMENT_FOURNISSEUR !=0)
			{
				if(!goprevious)
				{
					dispatchEvent(new Event("goToNextPageNoAccessoires",true));//IMPLEMENTATION DE PASSAGE A UNE PAGE
				}
				else
				{
					dispatchEvent(new Event("goToPreviousPageNoAccessoires",true));//IMPLEMENTATION DE PASSAGE A UNE PAGE
				}
			}
		}
		
		//SELECTION D'UN ACCESSOIRE POUR METTRE DANS LES FAVORIS
		private function thisIsFavoriteSelected(e:Event):void
		{
			if(dgListAccessoire.selectedItem.FAVORI == 1)
			{
				_temp.ajouterProduitAMesFavoris(dgListAccessoire.selectedItem);
				_temp.fournirListeAccessoiresCatalogueClient(_cmd.IDREVENDEUR,"");
			}
		}
		
		//DESELECTION D'UN ACCESSOIRE POUR L'ENLEVER DES FAVORIS
		private function thisIsNotFavoriteSelected(e:Event):void
		{
			if(dgListAccessoire.selectedItem.FAVORI == 0)
			{
				_temp.supprimerProduitDeMesFavoris(dgListAccessoire.selectedItem);
				_temp.fournirListeAccessoiresCatalogueClient(_cmd.IDREVENDEUR,"");
			}
		}
		
		//AFFECTE L'ACCESSOIRE SELECTIONNER A TRUE
		private function thisIsAccessoireSelected(e:Event):void
		{
			if(dgListAccessoire.selectedItem != null)
			{
				dgListAccessoire.selectedItem.SELECTED = true;
				totalNumber = totalNumber + Number(dgListAccessoire.selectedItem.PRIX_UNIT);
				selectedNumber++;
			}
			accessoire.itemUpdated(dgListAccessoire.selectedItem);
		}
		
		//AFFECTE L'ACCESSOIRE SELECTIONNER A FALSE
		private function thisIsNotAccessoireSelected(e:Event):void
		{
			if(dgListAccessoire.selectedItem != null)
			{
				dgListAccessoire.selectedItem.SELECTED = false;
				totalNumber = totalNumber - Number(dgListAccessoire.selectedItem.PRIX_UNIT);
				selectedNumber--;
			}
			accessoire.itemUpdated(dgListAccessoire.selectedItem);
		}
		
		//COCHE/DECOCHE LA CASE DE LA LIGNE SELECTIONNE
		protected function dgListAccessoireItemClickHandler(event:Event):void
		{
			if(event.currentTarget == null) return
			
			var item:Object = event.currentTarget.selectedItem;
						 
			if(item.SELECTED ==  true)
			{
				item.SELECTED = false	
			}
			else
			{
				item.SELECTED = true;
				
			}
			
			switch(item.SELECTED)
			{
				case true: 	{
								dispatchEvent(new Event(EVENT_SELECTED,true));
								break;
							}
				case false: {
								dispatchEvent(new Event(EVENT_NO_SELECTED,true)); 
								break;
							}
				default:
				{ }
			}
			
			accessoire.itemUpdated(item);
		}
		
		//VERIFIE SI TOUS LES CHAMPS ONT BIEN ETE REMPLIS ET ATTRIBUT LES ACCESSOIRES SELECTIONNES A L'OBJET 'ItemSelected'
		private function checkIfAttributToCommandeFill(e:Event):void
		{	
			goprevious = true;
			indexObjectSelected = 0;
			_itemsSelected.ACCESSOIRES = new ArrayCollection();
			for(var i:int = 0; i < accessoire.length;i++)
			{
				if(accessoire[i].SELECTED)
				{ 
						indexObjectSelected++;
						//accessoire[i].PRIX_UNIT = accessoire[i].PRIX_CATALOGUE;
						_itemsSelected.ACCESSOIRES.addItem(accessoire[i]);
				}
			}
			if(_itemsSelected.TERMINAUX[0].IDEQUIPEMENT_FOURNISSEUR == 0 && accessoire.length == 0)
			{
				parameterFill = false;
			}
			else
			{
				parameterFill = true;
			}
		}
		
//EVENT----------------------------------------------------------------------------------		
		
//METHODES PRIVATE-----------------------------------------------------------------------

	}
}