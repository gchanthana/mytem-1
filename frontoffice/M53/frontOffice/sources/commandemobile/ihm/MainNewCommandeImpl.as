package commandemobile.ihm
{
	import commandemobile.entity.Commande;
	import commandemobile.entity.ItemSelected;
	import commandemobile.entity.Panier;
	import commandemobile.ihm.mail.ActionMailBoxIHM;
	import commandemobile.popup.PopUpConfirmationCommandeIHM;
	import commandemobile.system.AbstractGestionPanier;
	import commandemobile.system.Action;
	import commandemobile.system.ActionEvent;
	import commandemobile.system.CommandeEvent;
	import commandemobile.system.GestionCommandeMobile;
	import commandemobile.system.GestionWorkFlow;
	import commandemobile.temp.commandeSNCF;
	import commandemobile.temp.temp;
	
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.util.ConsoviewAlert;
	import composants.util.URLUtilities;
	import composants.util.article.Article;
	import composants.util.article.ArticleNouvelleLigneMobile;
	import composants.util.equipements.EquipementsUtils;
	import composants.util.operateurs.OperateursUtils;
	import composants.util.poolsgestion.PoolsGestionUtils;
	
	import flash.events.Event;
	import flash.net.URLRequest;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Image;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class MainNewCommandeImpl extends Box
	{

//VARIABLES------------------------------------------------------------------------------

		private var _pUpMailBox					:ActionMailBoxIHM;
		//IHM
		public var etapeParametres				:ParametresIHM;
		public var etapeEquipmentChoice			:EquipmentChoiceIHM;
		public var etapeEquipmentChoice_2		:EquipmentChoice_2IHM;
		public var etapeSubscriptionChoice		:SubscriptionChoiceIHM;
		public var etapeQuantityChoice			:QuantityChoiceIHM;
		public var etapeQuantityChoice_2		:QuantityChoice_2IHM;
		public var etapeValidation				:ValidationIHM;
		
		private var selectedAction				:Action = new Action();
		
		//Bouton WIZZARD
		public var btnParametres				:Button;
		public var btnEquipmentChoice			:Button;
		public var btnSubscriptionChoice		:Button;
		public var btnQuantityChoice			:Button;
		public var btnValidation				:Button;
		
		//Bouton VIEWSTACK
		public var btPre						:Button;
		public var btNext						:Button;
		public var btValid						:Button;
		public var btValidAndPurchase			:Button;
		public var btFinishCommand				:Button;
		public var btCancel						:Button;
		
		//Index VIEWSTACK
		public var selectedIndex				:int = 0;
		
		//Couleur des boutons WIZZARD
		public var _colorBtn0					:int = 0;
		public var _colorBtn1					:int = 0;
		public var _colorBtn2					:int = 0;
		public var _colorBtn3					:int = 0;
		public var _colorBtn4					:int = 0;
		
		public var _colorBtnActif_R:Number = 0;
		public var _colorBtnActif_G:Number = 0;
		public var _colorBtnActif_B:Number = 255;
		
		private var _colorBtnNoActif_R:Number = 0;
		private var _colorBtnNoActif_G:Number = 0;
		private var _colorBtnNoActif_B:Number = 0;
		
		private var _colorBtnActifNotEnabled_R:Number = 13;
		private var _colorBtnActifNotEnabled_G:Number = 13;
		private var _colorBtnActifNotEnabled_B:Number = 13;
		
		//Saut de fenetre suivant la sélection du terminal
		public var noSkip						:Boolean = true;
		public var skipAll						:Boolean = false;
		public var skipEquipment				:Boolean = false;
		public var skipSubscription				:Boolean = false;
		private var quantityIsLastSelectedIndex	:Boolean = false;
		public var actePour						:Boolean = false;
		
		//VARIABLES LOCALES
		private var nextPage					:Boolean = false;
		private var newCommande					:Boolean = true;
		private var boolSubscription			:Boolean = false;
		
		public var spacerWidth:int = 0;
		
		//OBJETS
		private var carteSimCreation			:EquipementsUtils		= new EquipementsUtils();
		private var _cmdSNCF					:commandeSNCF 			= new commandeSNCF();
		private var _cmd						:Commande;
		private var _itemsSelected				:ItemSelected 			= new ItemSelected();
		private var _panier						:Panier;
		private var _operateurUtils				:OperateursUtils 		= new OperateursUtils();
		private var _poolsGestionUtils			:PoolsGestionUtils 		= new PoolsGestionUtils();
		private var _gc							:GestionCommandeMobile 	= new GestionCommandeMobile();
		private var _gestionArticles			:AbstractGestionPanier  = new AbstractGestionPanier();
		private var _article					:Article 				= new Article();
		public var proc							:temp 					= new temp();
		private var _gworkFlow					:GestionWorkFlow 		= new GestionWorkFlow();
		
		public var img							:Image;
		[Embed(source="/assets/images/Help3.png",mimeType='image/png')]
		public var imgTrue :Class;


//VARIABLES------------------------------------------------------------------------------
		
		//LISTE LES ARTICLES
		public function get gestionArticles():AbstractGestionPanier
		{
			if (_gestionArticles == null)
			{
				_gestionArticles = new AbstractGestionPanier();
			} 
			return _gestionArticles;
		}
		
		public function set gestionArticles(value:AbstractGestionPanier):void
		{
			_gestionArticles = value;
		}
		
//METHODES PUBLIC------------------------------------------------------------------------
		
		//CONSTRUCTEUR (>LISTNER)
		public function MainNewCommandeImpl()
		{
			carteSimCreation.addEventListener("fournirCarteSim", 	listnerTempEvent);
			addEventListener("fournirPoolsDuGestionnaire",			initialisationNewCommande);
			addEventListener("revendeurChange",						revendeurChange);
			proc.addEventListener(CommandeEvent.COMMANDE_CREATED,	closedThisPopUpHandler);
			_cmdSNCF.addEventListener("listeEquipements", 			listnerTempEvent);
			addEventListener("thisMailSent", 						closeThis);
			addEventListener("enableBtnValider", 					enableBtnValiderHandler);
			_gworkFlow.addEventListener("actionsWorkFlowFinished",	close);
			addEventListener("goToNextPageNoAccessoires",			btNextClickHandler);
			addEventListener("goToPreviousPageNoAccessoires",		btPreClickHandler);
			addEventListener("ValidAndPurchase",					btValidAndPurchaseClickHandler);
		}

//METHODES PUBLIC------------------------------------------------------------------------
		
		private function initialisationNewCommande(e:Event):void
		{
			etapeParametres.dispatchEvent(new Event("fournirPoolsDuGestionnaire"));
		}
		
		protected function imgCreationCompletehandler():void
		{
			img.visible = true;
			img.source 	= imgTrue;
		}
				
		protected function imgClickHandler():void
		{
			var u:URLRequest = new URLRequest("http://www.consotel.fr/gestiondecommandesncf/gestioncommandesncf.htm");
			URLUtilities.openWindow(u.url);
		}
		
		private function enableBtnValiderHandler(e:Event):void
		{
			btValid.enabled = true;
		}
		
		//Méthodes RGB converti en hexa
		protected function rgb2hex(r:int, g:int, b:int):Number 
		{ 
			return(r<<16 | g<<8 | b); 
		}
		
		//Initilise l'IHM lorsque la page a fini de ce loader
		protected function attributColor(btn:Number):void
		{
			if(btn == 0)
			{
				btnParametres.styleName 			= "btnActif";
				btnEquipmentChoice.styleName 		= "btnInactif";
				btnSubscriptionChoice.styleName 	= "btnInactif";
				btnQuantityChoice.styleName 		= "btnInactif";
				btnValidation.styleName 			= "btnInactif";
			}
			if(btn == 1)
			{
				btnParametres.styleName 			= "btnInactif";
				btnEquipmentChoice.styleName 		= "btnActif";
				btnSubscriptionChoice.styleName 	= "btnInactif";
				btnQuantityChoice.styleName 		= "btnInactif";
				btnValidation.styleName 			= "btnInactif";
			}
			if(btn == 2)
			{
				btnParametres.styleName 			= "btnInactif";
				btnEquipmentChoice.styleName 		= "btnInactif";
				btnSubscriptionChoice.styleName 	= "btnActif";
				btnQuantityChoice.styleName 		= "btnInactif";
				btnValidation.styleName 			= "btnInactif";			}
			if(btn == 3)
			{
				btnParametres.styleName 			= "btnInactif";
				btnEquipmentChoice.styleName 		= "btnInactif";
				btnSubscriptionChoice.styleName 	= "btnInactif";
				btnQuantityChoice.styleName 		= "btnActif";
				btnValidation.styleName 			= "btnInactif";			}
			if(btn == 4)
			{
				btnParametres.styleName 			= "btnInactif";
				btnEquipmentChoice.styleName 		= "btnInactif";
				btnSubscriptionChoice.styleName 	= "btnInactif";
				btnQuantityChoice.styleName 		= "btnInactif";
				btnValidation.styleName 			= "btnActif";			}
		}
		
		//Initilise l'IHM lorsque la page a fini de ce loader
		protected function buttunColor():void
		{
			switch(selectedIndex)
			{
				case 0: attributColor(0);break;
				case 1: attributColor(1);break;
				case 2: attributColor(1);break;
				case 3: attributColor(2);break;
				case 4: attributColor(3);break;
				case 5: attributColor(3);break;
				case 6: attributColor(4);break;
			}
		}
		
		protected function creationCompleteHandler(event:Event):void
		{
			 _cmd = new Commande(true);
			 _panier = new Panier(_cmd);
			thisButtonIsActive(true);///----------------------------------
			initialisation();
			buttunColor();
		}
		
		protected function btnParametresClickHandler(event:Event):void
		{
			checkIfselectIndex6();
			if(doThis(0))
			{
				selectedIndex = 0;
				buttunColor();
				activeViewstack();
			}
		}
		
		protected function btnEquipmentChoiceClickHandler(event:Event):void
		{
			checkIfselectIndex6();
			if(doThis(1))
			{
				selectedIndex = 1;
				buttunColor();
				activeViewstack();
			}		
		}
		
		protected function btnSubscriptionChoiceClickHandler(event:Event):void
		{
			checkIfselectIndex6();
			if(doThis(3))
			{
				selectedIndex = 3;
				buttunColor();
				activeViewstack();
			}		
		}
		
		protected function btnQuantityChoiceClickHandler(event:Event):void
		{
			checkIfselectIndex6();
			if(doThis(4))
			{
				selectedIndex = 4;
				buttunColor();
				activeViewstack();
			}
		}
		
		protected function btnValidationClickHandler(event:Event):void
		{
			checkIfselectIndex6();
			if(doThis(6))
			{
				selectedIndex = 6;
				buttunColor();
				activeViewstack();
			}
		}
		
		private function checkIfselectIndex6():void
		{
			if(selectedIndex == 6)
			{
				if(_cmd.COMMANDE_TEMPORAIRE.length > 0)
				{
					var numeroConfiguration:int = _cmd.COMMANDE_TEMPORAIRE.getItemAt(0).EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE;
				} 
				for(var i:int = 0;i < _cmd.COMMANDE.length;i++)
				{
					try
					{
						if(_cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE == numeroConfiguration)
						{
							while(_cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE == numeroConfiguration)
							{
								_cmd.COMMANDE.removeItemAt(i);
							}
						}
					}
					catch(e:Error)
					{ }
				}

				etapeQuantityChoice_2._cmd 				= _cmd;
				etapeQuantityChoice_2._itemsSelected 	= _itemsSelected;
				etapeQuantityChoice_2._panier 			= _panier;
			}
			_panier.attributMontant(_itemsSelected);
		}
		
		protected function doThis(thisIndex:int):Boolean
		{
			var bool:Boolean = false;
			switch(thisIndex)
			{
				case 0:
				{	
					bool = act0();
					break;
				}
				case 1:
				{	
					bool = act1();
					break;
				}
				case 3:
				{
					bool = act3();
					break;
				}
				case 4:
				{
					bool = act4();
					break;
				}
				case 6:
				{
					bool = act6();
					break;
				}
				default: { }
			}
			return bool;
		}
		
		private function act0():Boolean
		{
			var bool:Boolean = false;
			if(actionetapeParametres())
			{
				bool = true;
			}
			return bool;
		}
		
		private function act1():Boolean
		{
			var bool:Boolean = false;
			if(actionetapeParametres())
			{
				bool = true;
			}
			return bool;
		}
		
		private function act3():Boolean
		{
			var bool:Boolean = false;
			if(!noSkip)
			{
				if(skipAll)
				{
					return true;
				}
				if(skipEquipment)
				{
					if(actionetapeParametres())
					{
						bool = true;
					}
				}
				if(skipSubscription)
				{
					if(actionetapeParametres())
					{
						if(actionetapeEquipmentChoice())
						{
							if(actionetapeEquipmentChoice_2())
							{
								bool = true;
							}
						}
					}
				}
			}
			else
			{
				if(actionetapeParametres())
				{
					if(actionetapeEquipmentChoice())
					{
						if(actionetapeEquipmentChoice_2())
						{
							bool = true;
						}
					}
				}
			}
			return bool;
		}
		private function act4():Boolean
		{
			var bool:Boolean = false;

			if(!noSkip)
			{
				if(skipAll)
				{
					return true;
				}
				if(skipEquipment)
				{
					if(actionetapeParametres())
					{
						if(actionetapeSubscriptionChoice())
						{
							bool = true;
						}
					}
				}
				if(skipSubscription)
				{
					if(actionetapeParametres())
					{
						if(actionetapeEquipmentChoice())
						{
							if(actionetapeEquipmentChoice_2())
							{
								bool = true;
							}
						}
					}
				}
			}
			else
			{
				if(actionetapeParametres())
				{
					if(actionetapeEquipmentChoice())
					{
						if(actionetapeEquipmentChoice_2())
						{
							if(actionetapeSubscriptionChoice())
							{
								bool = true;
							}
						}
					}
				}
			}
			return bool;
		}
		private function act6():Boolean
		{
			var bool:Boolean = false;
			if(!noSkip)
			{
				if(skipAll)
				{
					if(actionetapeParametres())
					{
						if(actionetapeQuantityChoice())
						{
							if(actionetapeQuantityChoice_2())
							{
								bool = true;
							}
						}
					}
				}
				if(skipEquipment)
				{
					if(actionetapeSubscriptionChoice())
					{
						if(actionetapeQuantityChoice())
						{
							if(actionetapeQuantityChoice_2())
							{
								bool = true;
							}
						}
					}
				}
				if(skipSubscription)
				{
					if(actionetapeParametres())
					{
						if(actionetapeEquipmentChoice())
						{
							if(actionetapeEquipmentChoice_2())
							{
								if(actionetapeQuantityChoice())
								{
									if(actionetapeQuantityChoice_2())
									{
										bool = true;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				if(actionetapeParametres())
				{
					if(actionetapeEquipmentChoice())
					{
						if(actionetapeEquipmentChoice_2())
						{
							if(actionetapeSubscriptionChoice())
							{
								if(actionetapeQuantityChoice())
								{
									if(actionetapeQuantityChoice_2())
									{
										bool = true;
									}
								}
							}
						}
					}
				}
			}
			return bool;
		}
		
		protected function btPreClickHandler(event:Event):void
		{
			if(selectedIndex == 6)
			{
				if(_cmd.COMMANDE_TEMPORAIRE.length > 0)
				{
					var numeroConfiguration:int = _cmd.COMMANDE_TEMPORAIRE.getItemAt(0).EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE;
				} 
				for(var i:int = 0;i < _cmd.COMMANDE.length;i++)
				{
					try
					{
						if(_cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE == numeroConfiguration)
						{
							while(_cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE == numeroConfiguration)
							{
								_cmd.COMMANDE.removeItemAt(i);
							}
						}
					}
					catch(e:Error)
					{ }
				}

				etapeQuantityChoice_2._cmd 				= _cmd;
				etapeQuantityChoice_2._itemsSelected 	= _itemsSelected;
				etapeQuantityChoice_2._panier 			= _panier;
			}
			
			decompteViewStack();
			buttunColor();
			_panier.attributMontant(_itemsSelected);
		}
		
		protected function btNextClickHandler(event:Event):void
		{
			if(selectedIndex == 0 && !etapeParametres.skipAll)
			{
				_itemsSelected 								= new ItemSelected();
				_cmd.COMMANDE_TEMPORAIRE 					= new ArrayCollection();
				etapeSubscriptionChoice.listOptions 		= new ArrayCollection();
				etapeSubscriptionChoice.strgAbo 			= "";
			}

			actions();

			if(nextPage)
			{
				compteViewStack();
				initialisation();
				buttunColor();
			}
			else
			{
				if(selectedIndex == 1)
				{
					Alert.show("Veuillez sélectionner le terminal de votre choix!", "Consoview");
				}
				if(selectedIndex == 3)
				{
					Alert.show("Veuillez sélectionner l'abonnement principal de votre choix!", "Consoview");
				}
				else
				{
					if(selectedIndex != 1 && selectedIndex != 2 && selectedIndex != 3)
					{
						Alert.show("Veuillez remplir tous les champs obligatoire!", "Consoview");
					}
				}
				if(selectedIndex == 2)
				{
					Alert.show("Aucun accessoire disponible dans votre catalogue !\nVeuillez faire une autre commande !", "Consoview");
					selectedIndex = 0;
					dispatchEvent(new Event("revendeurChange",true));
					thisButtonIsActive(false);
				}
			}
		}
		
		protected function btValidClickHandler(event:Event):void
		{
//			if(selectedIndex == 4)
//			{
//				actions();
//				compteViewStack();
//				buttunColor();
//			}
			if(selectedIndex == 6)
			{
				if(_cmd.COMMANDE.length > 0)
				{
					if(etapeValidation.selectedAction != null)
					{
						if(etapeValidation.selectedAction.CODE_ACTION == "EMA3T")
						{ 
							var poUpConfir:PopUpConfirmationCommandeIHM = new PopUpConfirmationCommandeIHM();
							PopUpManager.addPopUp(poUpConfir,this,true);
							PopUpManager.centerPopUp(poUpConfir);
							poUpConfir.addEventListener("sendCommande",sendCommandeHandler);
						}
						else
						{
							var ev:Event = new Event(Event.ACTIVATE);
							sendCommandeHandler(ev);
						}
					}
					else
					{
						var eve:Event = new Event(Event.ACTIVATE);
						sendCommandeHandler(eve);
					}
				}
				else
				{
					Alert.show("Veuillez créer ou moins une commande!","Consoview");
				}
			}
		}
		
		protected function btValidAndPurchaseClickHandler(event:Event):void
		{
			nextPage = actionetapeQuantityChoice_2();
			if(nextPage)
			{
				selectedIndex = 0;
				etapeQuantityChoice_2.dispatchEvent(new Event("compteLastMontant"));
				etapeParametres.dispatchEvent(new Event("allEnabledFalse"));
//				addConfiguration();
				_panier.attributMontant(_itemsSelected);
				_panier.MONTANT_COMMANDE_ENCOURS = "0.00";
				newCommande = false;
				_itemsSelected = new ItemSelected();
				_cmd.PRICE_EQUIPEMENT = new Object();
				_cmd.CONFIG_NUMBER = 1;
				thisButtonIsActive(true);
				buttunColor();
			}
			else
			{
				Alert.show("Veuillez remplir tous les champs obligatoire!", "Consoview");
			}
		}


		protected function btFinishCommandClickHandler(event:Event):void
		{
			actions();
			if(nextPage)
			{
				compteViewStack();
				buttunColor();
			}
			else
			{
				if(selectedIndex == 1)
				{
					Alert.show("Veuillez sélectionner le terminal de votre choix!", "Consoview");
				}
				else
				{
					Alert.show("Veuillez remplir tous les champs obligatoire!", "Consoview");
				}
			}
		}
		
		protected function btCancelClickHandler(event:Event):void
		{
			selectedIndex = 0;
			_cmd 				= new Commande(true);
			_itemsSelected 		= new ItemSelected();
			_panier 			= new Panier(_cmd);
			_operateurUtils 	= new OperateursUtils();
			_poolsGestionUtils 	= new PoolsGestionUtils();
			_gc 				= new GestionCommandeMobile();
			_article 			= new Article();
			dispatchEvent(new Event("etapeListCommande",true));	
		}
		
//METHODES PROTECTED---------------------------------------------------------------------

//METHODES PRIVATE-----------------------------------------------------------------------
		
		private function razLastSelection():void
		{
			etapeEquipmentChoice._itemsSelected 					= new ItemSelected();
			etapeEquipmentChoice.terminaux		 					= new ArrayCollection();
			etapeEquipmentChoice_2._itemsSelected 					= new ItemSelected();
			etapeEquipmentChoice_2.accessoire	 					= new ArrayCollection();
			etapeSubscriptionChoice._itemsSelected 					= new ItemSelected();
			etapeSubscriptionChoice.listOptions 					= new ArrayCollection();
			etapeSubscriptionChoice.txtiptAbonnementPrincipal.text 	= "";
		}

		private function listnerTempEvent(e:Event):void
		{
			if(Number(carteSimCreation.selectedEquipement.IDTYPE_EQUIPEMENT) == 71)
			{
				_itemsSelected.TERMINAUX.addItem(carteSimCreation.selectedEquipement);
				return;
			}
		}
		
		private function sendCommandeHandler(e:Event):void
		{
			var articles:XML = new XML();
			_cmd.ARTICLES = <articles></articles>;
			articles = convertToArticles();
			if(articles != null)
			{
				proc.enregistrerCommande(_cmd,articles,actePour);
			}
		}
		
		private function convertToArticles():XML
		{
			for(var i:int = 0;i < _cmd.COMMANDE.length;i++)
			{
				var tmpArticle : Article = new ArticleNouvelleLigneMobile(XML(_article.article).copy());
				_cmd.ARTICLES.appendChild(convertArticlesInXML(_cmd.COMMANDE[i],tmpArticle,tmpArticle.article));
			}
			return _cmd.ARTICLES;
		}
		
		private function convertArticlesInXML(object:Object,article:Article,newArticle:XML):XML
		{
			addRessourcesArticles(article,object);
			addEquipementsArticles(article,object);
			addArticle(newArticle,object.EMPLOYE_CONFIGURATION);
			return newArticle;
		}

		private function addRessourcesArticles(article:Article,object:Object):void
		{
			var allArticles:ArrayCollection = new ArrayCollection();

			//ABONNEMENTS
			for(var j:int = 0;j < object.ABO.length;j++)
			{
				allArticles.addItem(object.ABO[j]);
			}
			//OPTIONS
			for(var k:int = 0;k < object.OPTIONS.length;k++)
			{
				allArticles.addItem(object.OPTIONS[k]);
			}
	
			convertToArticle(article,allArticles,true,object);
		}
		
		private function addEquipementsArticles(article:Article,object:Object):void
		{
			var allArticles:ArrayCollection = new ArrayCollection();

			//TELEPHONE + CARTE SIM
			for(var h:int = 0;h < object.TERMINAUX.length;h++)
			{
				allArticles.addItem(object.TERMINAUX[h]);
			}
			//ACCESSOIRES
			for(var g:int = 0;g < object.ACCESSOIRES.length;g++)
			{
				allArticles.addItem(object.ACCESSOIRES[g]);
			}
			
			convertToArticle(article,allArticles,false,object);
		}
		
		private function addArticle(newArticle:XML,object:Object):void
		{
			if(object.FPC != "")
			{
				newArticle.appendChild(<fpc></fpc>);//A VOIR**********************************************{/*object.FPC*/}
				newArticle.appendChild(<engagement_eq>12</engagement_eq>);//A VOIR**********************************************
				newArticle.appendChild(<engagement_res>12</engagement_res>);//A VOIR**********************************************
			}
			else
			{
				newArticle.appendChild(<fpc></fpc>);
				newArticle.appendChild(<engagement_eq>12</engagement_eq>);//A VOIR**********************************************{/*object.ENGAGEMENT.VALUE*/}
				newArticle.appendChild(<engagement_res>12</engagement_res>);//A VOIR**********************************************{/*object.ENGAGEMENT.VALUE*/}
			}

			if(object.IDCOLLABORATEUR != 0)
			{
				newArticle.appendChild(<nomemploye>{object.COLLABORATEUR}</nomemploye>);
				newArticle.appendChild(<idemploye>{object.IDCOLLABORATEUR}</idemploye>);
			}

			newArticle.appendChild(<code_interne>{object.LIBELLE_CONFIGURATION}</code_interne>);
			
			if(object.PORTABILITE)
			{
				newArticle.appendChild(<soustete>{object.NUM_LINE}</soustete>);
				newArticle.appendChild(<code_rio>{object.NUM_RIO}</code_rio>);
				newArticle.appendChild(<date_portage>{formatDate(object.DATE_PORTABILITE)}</date_portage>);				
			}
			else
			{
				newArticle.appendChild(<soustete>{object.NUM_LINE}</soustete>);
			}
		}
		
		private function formatDate(dateobject:Object):String
		{
			var day:String = dateobject.date;
			if(day.length < 2)
			{day = "0" + day;}
			var month:int = Number(dateobject.month) + 1;
			var temp:String = month.toString();
			if(temp.length < 2)
			{temp = "0" + temp;}
			var year:String = dateobject.fullYear;
			return year + "/" + temp + "/" + day;
		}
		
		private function convertToArticle(article:Article,arrayC:ArrayCollection,ressources:Boolean,obj:Object):void
		{
			var len:int = arrayC.length;
			if(ressources)
			{
				for(var i:int = 0;i < len;i++)
				{
					article.addRessource(arrayC[i]);
				}
			}
			else
			{
				for(var j:int = 0;j < len;j++)
				{
					article.addEquipement(arrayC[j],obj.EMPLOYE_CONFIGURATION.ENGAGEMENT);
				}
			}
		}
		
		private function closedThisPopUpHandler(e:Event):void
		{
			if(etapeValidation.selectedAction != null)
			{
				selectedAction = etapeValidation.selectedAction;
				_cmd.IDCOMMANDE = proc.idCommande;
				btnValiderActionClickHandler();
			}
			else
			{
				var ev:Event = new Event(Event.CLOSE);
				close(ev);
			}
		}
		
		protected function btnValiderActionClickHandler():void
		{
			afficherMailBox();
		}
		
		private var _infosMail:InfosObject = new InfosObject();
		public function set infosMail(value:InfosObject):void
		{
			_infosMail  = value
		}
		public function get infosMail():InfosObject
		{
			return _infosMail;
		}
		
		protected function afficherMailBox():void
		{	
			_pUpMailBox = new ActionMailBoxIHM();
			infosMail.commande = _cmd;
			infosMail.corpMessage = selectedAction.MESSAGE;
			var objet:String = "Réf. : "  + infosMail.commande.REF_CLIENT;
			_pUpMailBox.initMail("Commande Mobile",objet,"");
			_pUpMailBox.configRteMessage(infosMail,GabaritFactory.TYPE_COMMANDE_MOBILE);			
			_pUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE,_pUpMailBoxMailEnvoyeHandler);
			_pUpMailBox.selectedAction = selectedAction;
			PopUpManager.addPopUp(_pUpMailBox,this,true);
			PopUpManager.centerPopUp(_pUpMailBox);
		}
		
		protected function _pUpMailBoxMailEnvoyeHandler(ev : Event):void
		{	
			selectedAction.COMMENTAIRE_ACTION = _pUpMailBox.txtCommentaire.text; //txtCommentaire.text;
	    	selectedAction.DATE_ACTION = new Date();//dcDateAction.selectedDate;
	    		    	
	    	var actionEvent:ActionEvent = new ActionEvent(ActionEvent.ACTION_CLICKED);
	    	actionEvent.action = selectedAction;
	    	dispatchEvent(actionEvent);	    	
	    	
			_pUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE,_pUpMailBoxMailEnvoyeHandler);
			PopUpManager.removePopUp(_pUpMailBox);
			_pUpMailBox = null;
			ConsoviewAlert.afficherOKImage("Votre mail a été envoyé!");
			dispatchEvent(new Event("thisMailSent"));
		}
		
		private function closeThis(e:Event):void
		{
			if(etapeValidation.selectedAction != null)
			{
				_gworkFlow.faireActionWorkFlow(etapeValidation.selectedAction,_cmd,false);
			}
			close(e);
		}
		
		private function close(e:Event):void
		{
			selectedIndex = 0;
			_cmd 				= new Commande(true);
			_itemsSelected 		= new ItemSelected();
			_panier 			= new Panier(_cmd);
			_operateurUtils 	= new OperateursUtils();
			_poolsGestionUtils 	= new PoolsGestionUtils();
			_gc 				= new GestionCommandeMobile();
			_article 			= new Article();
			
			dispatchEvent(new Event("etapeListCommande",true));	
		}
		
		private function addConfiguration():void
		{
			for(var i:int = 0; i < _cmd.COMMANDE_TEMPORAIRE.length;i++)
			{
				_cmd.COMMANDE.addItem(_cmd.COMMANDE_TEMPORAIRE[i]);
			}
		}

		private function actions():void
		{
			if(selectedIndex == 0)
			{
				etapeEquipmentChoice.viewEngagement = !etapeParametres.skipSubscription;
				etapeQuantityChoice_2.newView = true;
				nextPage = actionetapeParametres();
			}
			if(selectedIndex == 1)
			{
				etapeQuantityChoice_2.newView = true;
				nextPage = actionetapeEquipmentChoice();
			}
			if(selectedIndex == 2)
			{
				if(skipSubscription)
				{
					etapeQuantityChoice.newView = true;
					etapeQuantityChoice_2.newView = true;
				}
				etapeQuantityChoice_2.newView = true;
				
				nextPage = actionetapeEquipmentChoice_2();
			}
			if(selectedIndex == 3)
			{
				etapeQuantityChoice.newView = true;
				etapeQuantityChoice_2.newView = true;

				nextPage = actionetapeSubscriptionChoice();
			}
			if(selectedIndex == 4)
			{
				nextPage = actionetapeQuantityChoice();
			}
			if(selectedIndex == 5)
			{
				etapeQuantityChoice.newView 	= false;
	 			etapeQuantityChoice_2.newView 	= false;
				nextPage = actionetapeQuantityChoice_2();
			}
			if(selectedIndex == 6)
			{
				etapeQuantityChoice_2.newView = false;
				nextPage = actionetapeValidation();
			}
		}
		
		private function actionetapeParametres():Boolean
		{
			var bool:Boolean = false;
			etapeParametres.dispatchEvent(new Event("nextPageClicked"));
			bool = etapeParametres.parameterFill;
			_panier.attributMontant(_itemsSelected);
			return bool;
		}
		
		private function actionetapeEquipmentChoice():Boolean
		{
			var bool:Boolean = false;
			etapeEquipmentChoice_2.goprevious = false;
			etapeEquipmentChoice.dispatchEvent(new Event("nextPageClicked"));
			bool = etapeEquipmentChoice.parameterFill;
			_panier.attributMontant(_itemsSelected);
			return bool;
		}
		
		private function actionetapeEquipmentChoice_2():Boolean
		{
			var bool:Boolean = false;
			etapeEquipmentChoice_2.dispatchEvent(new Event("nextPageClicked"));
			bool = etapeEquipmentChoice_2.parameterFill;
			_panier.attributMontant(_itemsSelected);
			return bool;
		}
		
		private function actionetapeSubscriptionChoice():Boolean
		{
			var bool:Boolean = false;
			etapeSubscriptionChoice.dispatchEvent(new Event("nextPageClicked"));
			bool = etapeSubscriptionChoice.parameterFill;
			_panier.attributMontant(_itemsSelected);
			return bool;
		}
		
		private function resultNextPageClickedf(e:Event):void
		{
			boolSubscription = etapeSubscriptionChoice.parameterFill;
		}
		
		private function actionetapeQuantityChoice():Boolean
		{
			var bool:Boolean = false;
			etapeQuantityChoice.dispatchEvent(new Event("nextPageClicked"));
			bool = etapeQuantityChoice.parameterFill;
			_panier.attributMontant(_itemsSelected);
			return bool;
		}
		
		private function actionetapeQuantityChoice_2():Boolean
		{
			var bool:Boolean = false;
			etapeQuantityChoice_2.dispatchEvent(new Event("compteLastMontant"));
			etapeQuantityChoice_2.dispatchEvent(new Event("nextPageClicked"));
			bool = etapeQuantityChoice_2.parameterFill;
			_panier.attributMontant(_itemsSelected);
			return bool;
		}
		
		private function actionetapeValidation():Boolean
		{
			var bool:Boolean = false;
			etapeValidation.dispatchEvent(new Event("nextPageClicked"));
			bool = etapeValidation.parameterFill;
			_panier.attributMontant(_itemsSelected);
			return bool;
		}
		
		private function initialisation():void
		{
			etapeParametres._cmd = etapeEquipmentChoice._cmd = etapeEquipmentChoice_2._cmd = etapeSubscriptionChoice._cmd = etapeQuantityChoice._cmd = etapeQuantityChoice_2._cmd = etapeValidation._cmd = _cmd;
			etapeParametres._panier = etapeEquipmentChoice._panier = etapeEquipmentChoice_2._panier = etapeSubscriptionChoice._panier = etapeQuantityChoice._panier = etapeQuantityChoice_2._panier = etapeValidation._panier = _panier;
			etapeParametres.dispatchEvent(new Event("_panier"));
			if(etapeParametres.skipAll)
			{
				etapeEquipmentChoice._itemsSelected = etapeEquipmentChoice_2._itemsSelected = etapeSubscriptionChoice._itemsSelected = etapeQuantityChoice._itemsSelected = etapeQuantityChoice_2._itemsSelected = etapeValidation._itemsSelected = _itemsSelected = etapeParametres._itemSelected;
			}
			else
			{
				etapeParametres._itemSelected = etapeEquipmentChoice._itemsSelected = etapeEquipmentChoice_2._itemsSelected = etapeSubscriptionChoice._itemsSelected = etapeQuantityChoice._itemsSelected = etapeQuantityChoice_2._itemsSelected = etapeValidation._itemsSelected = _itemsSelected;
			}
		}
		
		private function newInitialisation():void
		{
			selectedIndex = 0;
			_cmd 				= new Commande(true);
			_itemsSelected 		= new ItemSelected();
			_panier 			= new Panier(_cmd);
			_operateurUtils 	= new OperateursUtils();
			_poolsGestionUtils 	= new PoolsGestionUtils();
			_gc 				= new GestionCommandeMobile();
			_article 			= new Article();
			_cmd.COMMANDE_TEMPORAIRE = new ArrayCollection();
			
			etapeParametres.noSkip 				= true;
			etapeParametres.skipAll				= false;
			etapeParametres.skipEquipment 		= false;
			etapeParametres.skipSubscription 	= false;
			etapeParametres.acte 				= false;
		}
		
		private function checkSkip():void
		{
			actePour 			= etapeParametres.acte;
			noSkip 				= etapeParametres.noSkip;
			skipAll				= etapeParametres.skipAll;
			skipEquipment 		= etapeParametres.skipEquipment;
			skipSubscription 	= etapeParametres.skipSubscription;
			etapeQuantityChoice_2.skipSubscription = skipSubscription;
			etapeQuantityChoice_2.skipAll = skipAll;
		}

		private function attribut(bool:Boolean,etat:Boolean = true,skipAllOrNot:Boolean = false):void
		{

		}
		
		private function compteViewStack():void//----------------------------------
		{
			checkSkip();
			
			if(noSkip)
			{
				selectedIndex++;
				etapeEquipmentChoice.carteSim = true;
				thisButtonIsActive(true);
				return;
			}
			if(skipEquipment)
			{
				if(selectedIndex == 0)
				{
					selectedIndex = 3;
					etapeEquipmentChoice.carteSim = true;
//					_cmdSNCF.fournirListeEquipements(_cmd.IDGESTIONNAIRE,_cmd.IDPOOL_GESTIONNAIRE,_cmd.IDPROFIL_EQUIPEMENT,_cmd.IDREVENDEUR,"");
					carteSimCreation.fournirCarteSim(Number(_cmd.IDREVENDEUR),Number(_cmd.IDOPERATEUR));
					thisButtonIsActive(true);
					return;
				}
				if(selectedIndex > 2)
				{
					selectedIndex++;
					thisButtonIsActive(true);
					return;
				}
			}
			if(skipSubscription)
			{
				if(selectedIndex < 2)
				{
					selectedIndex++;
					etapeEquipmentChoice.carteSim = false;
					thisButtonIsActive(true);
					return;
				}
				
				if(selectedIndex == 2)
				{
					selectedIndex = 4;
					thisButtonIsActive(true);
					return;
				}
				
				if(selectedIndex == 4)
				{
					selectedIndex = selectedIndex + 2;
					etapeQuantityChoice_2.showHandler();
					thisButtonIsActive(true);
					return;
				}
				
				if(selectedIndex > 3)
				{
					selectedIndex++;
					thisButtonIsActive(true);
					return;
				}
			}
			if(skipAll)
			{
				if(selectedIndex == 0)
				{
					selectedIndex = 4;
					thisButtonIsActive(true);
					return;
				}
				if(selectedIndex > 3)
				{
					selectedIndex++;
					thisButtonIsActive(true);
					return;
				}
			}
		}
		
		private function decompteViewStack():void
		{
			checkSkip();
			
			if(noSkip)
			{
				selectedIndex--;
				etapeEquipmentChoice.carteSim = true;
				thisButtonIsActive(false);
				return;
			}
			if(skipEquipment)
			{
				if(selectedIndex == 3)
				{
					selectedIndex = 0;
					thisButtonIsActive(true);
					return;
				}
				if(selectedIndex > 2)
				{
					selectedIndex--;
					thisButtonIsActive(true);
					return;
				}
			}
			if(skipSubscription)
			{
				if(selectedIndex < 3)
				{
					selectedIndex--;
					etapeEquipmentChoice.carteSim = false;
					thisButtonIsActive(false);
					return;
				}
				
				if(selectedIndex == 4)
				{
					selectedIndex = 2;
					thisButtonIsActive(false);
					return;
				}
				
				if(selectedIndex == 6)
				{
					selectedIndex = selectedIndex-2;
					thisButtonIsActive(false);
					return;
				}
				
				if(selectedIndex > 3)
				{
					selectedIndex--;
					thisButtonIsActive(false);
					return;
				}
			}
			if(skipAll)
			{
				if(selectedIndex == 4)
				{
					selectedIndex = 0;
					thisButtonIsActive(false);
					return;
				}
				if(selectedIndex > 3)
				{
					selectedIndex--;
					thisButtonIsActive(false);
					return;
				}
			}
		}
		
		
		private function tempActiveButton():void
		{

		}
		
		private function thisButtonIsActive(bool:Boolean):void
		{
			activeWizzard(bool);
			activeViewstack();
		}
		
		private function revendeurChange(e:Event):void
		{
			activeWizzard(false);
		}
		
		private function activeViewstack():void
		{
			switch(selectedIndex)
			{
				case 0:
				{
					activeButtonViewstack(false,true,false,false,false,true);
					break;
				}
				case 1:
				{
					activeButtonViewstack(true,true,false,false,false,true);
					break;
				}
				case 2:
				{
					activeButtonViewstack(true,true,false,false,false,true);
					break;
				}
				case 3:
				{
					activeButtonViewstack(true,true,false,false,false,true);
					break;
				}
				case 4:
				{
					activeButtonViewstack(true,true,false,false,false,true);
					break;
				}
				case 5:
				{
					activeButtonViewstack(true,true,false,false,false,true);
					break;
				}
				case 6:
				{
					activeButtonViewstack(true,false,true,false,false,true);
					break;
				}
				default: { }
			}
		}
		
		
		private function activeWizzard(compte:Boolean):void
		{
			if(compte)
			{
				activeCompteWizzard();
			}
			else
			{
				activeDecompteWizzard();
			}
		}
		
		private function activeCompteWizzard():void
		{
			switch(selectedIndex)
			{
				case 0:
				{
					activeButtonWizzard(true,false,false,false,false);
					break;
				}
				case 1:
				{
					activeButtonWizzard(true,true,false,false,false);
					break;
				}
				case 2:
				{
					activeButtonWizzard(true,true,false,false,false);
					break;
				}
				case 3:
				{
					activeButtonWizzard(true,true,true,false,false);
					break;
				}
				case 4:
				{
					activeButtonWizzard(true,true,true,true,false);
					break;
				}
				case 5:
				{
					activeButtonWizzard(true,true,true,true,false);
					break;
				}
				case 6:
				{
					activeButtonWizzard(true,true,true,true,true);
					break;
				}
				default: { }
			}
		}
		
		private function activeDecompteWizzard():void
		{
			switch(selectedIndex)
			{
				case 0:
				{
					activeButtonWizzard(true,false,false,false,false);
					break;
				}
				case 1:
				{
					activeButtonWizzard(true,true,false,false,false);
					break;
				}
				case 2:
				{
					activeButtonWizzard(true,true,false,false,false);
					break;
				}
				case 3:
				{
					activeButtonWizzard(true,true,true,false,false);
					break;
				}
				case 4:
				{
					activeButtonWizzard(true,true,true,true,false);
					break;
				}
				case 5:
				{
					activeButtonWizzard(true,true,true,true,false);
					break;
				}
				case 6:
				{
					activeButtonWizzard(true,true,true,true,false);
					break;
				}
				default: { }
			}
		}
		
		
		private function activeButtonWizzard(btn0:Boolean,btn1:Boolean,btn2:Boolean,btn3:Boolean,btn4:Boolean):void
		{
			if(noSkip)
			{
				btnParametres.enabled 			= btn0;
				btnEquipmentChoice.enabled 		= btn1;
				btnSubscriptionChoice.enabled 	= btn2;
				btnQuantityChoice.enabled 		= btn3;
				btnValidation.enabled 			= btn4;
			}
			if(skipAll)
			{
				btnParametres.enabled 			= btn0;
				btnEquipmentChoice.enabled 		= false;
				btnSubscriptionChoice.enabled 	= false;
				btnQuantityChoice.enabled 		= true;
				btnValidation.enabled 			= btn4;
			}
			if(skipEquipment)
			{
				btnParametres.enabled 			= btn0;
				btnEquipmentChoice.enabled 		= false;
				btnSubscriptionChoice.enabled 	= btn2;
				btnQuantityChoice.enabled 		= btn3;
				btnValidation.enabled 			= btn4;
			}
			if(skipSubscription)
			{
				btnParametres.enabled 			= btn0;
				btnEquipmentChoice.enabled 		= btn1;
				btnSubscriptionChoice.enabled 	= false;
				btnQuantityChoice.enabled 		= btn3;
				btnValidation.enabled 			= btn4;
			}
		}
		
		private function activeButtonViewstack(btn0:Boolean,btn1:Boolean,btn2:Boolean,btn3:Boolean,btn4:Boolean,btn5:Boolean):void
		{
			hiddenButtonViewstack(btPre,btn0);
			hiddenButtonViewstack(btNext,btn1);
			hiddenButtonViewstack(btValid,btn2,false,false,btn2);
			hiddenButtonViewstack(btValidAndPurchase,btn3,true,true);
			hiddenButtonViewstack(btFinishCommand,btn4,true);
			hiddenButtonViewstack(btCancel,btn5);
		}
		
		private function colorButton():void
		{
		
		}
		
		private function hiddenButtonViewstack(btn:Button,bool:Boolean,otherBool:Boolean = false,btnValideEndPur:Boolean = false,btnValider:Boolean = false):void
		{
			btn.visible = bool;	
			switch(btn.visible)
			{
				case true:
				{
					btn.width = 110;
					spacerWidth = 0;
					if(btnValider)
					{
						btValid.enabled = false;
					}
					if(otherBool)
					{
						if(btnValideEndPur)
						{
							btn.width = 215;
						}
						else
						{
							btn.width = 165;
							spacerWidth = 5;
						}
					}
					break;
				}
				case false:
				{
					btn.width = 0;
					break;
				}
				default: { }
			}
		}
////


////		

	}
}