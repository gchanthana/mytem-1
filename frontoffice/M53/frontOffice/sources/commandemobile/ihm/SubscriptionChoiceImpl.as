package commandemobile.ihm
{
	import commandemobile.composants.PanierIHM;
	import commandemobile.entity.Commande;
	import commandemobile.entity.ItemSelected;
	import commandemobile.entity.Panier;
	import commandemobile.popup.PopUpChoiceOptionsIHM;
	import commandemobile.popup.PopUpChoiceSubscriptionIHM;
	import commandemobile.popup.PopUpConfirmationPasseOptionsIHM;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class SubscriptionChoiceImpl extends Box
	{

//VARIABLES------------------------------------------------------------------------------
		
		//COMPOSANTS
		public var dgListOptions				:DataGrid;
		public var txtiptAbonnementPrincipal	:Label;
		public var boxVisible					:Box;
		public var composantPanier				:PanierIHM;

		//OBJETS
		public var _cmd							:Commande;
		public var _itemsSelected				:ItemSelected;
		public var _panier						:Panier;
		
		//VARIABLES LOCALES
		private var _listOptions				:ArrayCollection = new ArrayCollection();
		public var parameterFill				:Boolean = false;
		public var montantAbonnement			:Number = 0;
		public var montantOptions				:Number = 0;
		public var strgAbo						:String = "";

		//POPUP
		private var popUpSubscrib				:PopUpChoiceSubscriptionIHM;		
		private var popUpOptions				:PopUpChoiceOptionsIHM;

//VARIABLES------------------------------------------------------------------------------

//PROPRIETEES PUBLICS--------------------------------------------------------------------
		
		//CONTIENT TOUS LES ACCESSOIRES RECU PAR LA PROCEDURE
		public function get listOptions():ArrayCollection
		{
			return _listOptions;
		}
			
		public function set listOptions(values:ArrayCollection):void
		{
			_listOptions = values;
		}
		
//PROPRIETEES PUBLICS--------------------------------------------------------------------

//METHODES PUBLICS-----------------------------------------------------------------------
		
		//CONSTRUCTEUR (>LISTENER)
		public function SubscriptionChoiceImpl()
		{
			addEventListener("eraseThisOptions",	earseOptionsInList);
			addEventListener("nextPageClicked",		checkIfAttributToCommandeFill);
		}

//METHODES PUBLICS-----------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
		
		protected function composantPanierCreationCompleteHandler():void
		{
			composantPanier.panier = _panier;
		}
		
		protected function showHandler():void
		{
			if(_itemsSelected.ABO.length > 0)
			{
				boxVisible.visible = true;
			}
			else
			{
				boxVisible.visible = false;
				btnChoisirClickHandler();
			}
			composantPanierCreationCompleteHandler();
		}
		
		//OUVRE LA POPUP PERMETTANT DE SELECTIONNER LES ABONNEMENTS
		protected function btnChoisirClickHandler():void
		{
			popUpSubscrib = new PopUpChoiceSubscriptionIHM();
			popUpSubscrib._cmd 				= _cmd;
			popUpSubscrib._itemsSelected 	= _itemsSelected;
			PopUpManager.addPopUp(popUpSubscrib,this,true);
			PopUpManager.centerPopUp(popUpSubscrib);
			popUpSubscrib.dispatchEvent(new Event("listerProduit"));
			popUpSubscrib.addEventListener("POPUP_SUBSCRIB_CLOSED_AND_VALIDATE",closedPopUpSubscribHandler);
		}
		
		//OUVRE LA POPUP PERMETTANT DE SELECTIONNER LES OPTIONS
		protected function btnOptionsClickHandler():void
		{
			popUpOptions = new PopUpChoiceOptionsIHM();
			PopUpManager.addPopUp(popUpOptions,this,true);
			PopUpManager.centerPopUp(popUpOptions);
			popUpOptions._cmd 				= _cmd;
			popUpOptions._itemsSelected 	= _itemsSelected;
			popUpOptions.dispatchEvent(new Event("listerProduit"));
			popUpOptions.addEventListener("POPUP_OPTIONS_CLOSED_AND_VALIDATE",closedPopUpOptionsHandler);
		}

//METHODES PROTECTED---------------------------------------------------------------------

//METHODES PRIVATE-----------------------------------------------------------------------

		//CACUL LE MONTANT TOTAL DES ABONNEMENTS OU DES OPTIONS SELECTIONNER
		private function calculMontantTotal(abo:Boolean):void
		{
			if(abo)
			{
				montantAbonnement = 0;
				for(var i:int = 0; i < _itemsSelected.ABO.length;i++)
				{
					montantAbonnement = montantAbonnement + Number(_itemsSelected.ABO[i].PRIX_UNIT);
				}
			}
			else
			{
				montantOptions = 0;
				for(var j:int = 0; j < listOptions.length;j++)
				{
					montantOptions = montantOptions + Number(listOptions[j].PRIX_UNIT);
				}
			}
		}

//EVENT----------------------------------------------------------------------------------		
		
		//REGARDE SI L'ABONNEMENTS ET LES OPTIONS NE SONT PAS VIDE
		private function checkIfAttributToCommandeFill(e:Event):void
		{
			if(_itemsSelected.ABO.length != 0)
			{
				parameterFill = true;
			}
			else
			{
				parameterFill = false;
			}
		}
		
		//EFFACE L'OPTION SELECTIONNEE
		private function earseOptionsInList(e:Event):void
		{
			if(dgListOptions.selectedItem != null)
			{
				for(var i:int = 0;i < listOptions.length;i++)
				{
					if(listOptions[i].LIBELLE_PRODUIT.toString().toLowerCase().indexOf(dgListOptions.selectedItem.LIBELLE_PRODUIT.toString().toLowerCase()) != -1)
					{ 
						listOptions.removeItemAt(i);
						_itemsSelected.OPTIONS = new ArrayCollection();
						_itemsSelected.OPTIONS = listOptions;
					}
				}
			}
			calculMontantTotal(false);
			_panier.attributMontant(_itemsSelected);
		}

		//AFFECTE LES OPTIONS SELECTIONNEES A '_itemsSelected.ABO' ET 
		//AFFICHE LE DATAGRID LORS DE LA FERMETURE DE LA POPUP 'Sélectionnez vos abonnements'
		private function closedPopUpSubscribHandler(e:Event):void
		{
			if(_itemsSelected.ABO.length > 0)
			{
				for(var i:int = 0;i <  _itemsSelected.ABO.length;i++)
				{
					strgAbo = _itemsSelected.ABO[i].LIBELLE_PRODUIT;
				}
				boxVisible.visible = true;
			}
			else
			{
				boxVisible.visible = false;
				_itemsSelected.OPTIONS = new ArrayCollection();
			}
			calculMontantTotal(true);
			_panier.attributMontant(_itemsSelected);
		}
		
		//AFFECTE LES OPTIONS SELECTIONNEES A '_itemsSelected.OPTIONS' ET 
		//AFFICHE LE DATAGRID LORS DE LA FERMETURE DE LA POPUP 'Sélectionnez vos options'
		private function closedPopUpOptionsHandler(e:Event):void
		{
			_cmd = popUpOptions._cmd;
			_itemsSelected = popUpOptions._itemsSelected;
			listOptions = new ArrayCollection();
			if(_itemsSelected.OPTIONS.length > 0)
			{
				listOptions = _itemsSelected.OPTIONS;
				boxVisible.visible = true;
			}
			calculMontantTotal(false);
			_panier.attributMontant(_itemsSelected);
		}
		
//EVENT----------------------------------------------------------------------------------		
			
//METHODES PRIVATE-----------------------------------------------------------------------		

	}
}