package commandemobile.popup
{
	import commandemobile.entity.Commande;
	import commandemobile.entity.SaisieTechniqueEquipement;
	import commandemobile.system.AbstractGestionPanier;
	import commandemobile.system.ArticleEvent;
	import commandemobile.temp.temp;
	
	import composants.util.ConsoviewAlert;
	import composants.util.lignes.LignesUtils;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.managers.PopUpManager;
	
	
	[Bindable]
	public class PopUpSaisieTechniqueImpl extends TitleWindow
	{
		
		//COMPOSANTS
		public var dgListCommande			:DataGrid;
		
		//OBJETS
		public var commande					:Commande;
		private var _gPanier				:AbstractGestionPanier = new AbstractGestionPanier();
		private var _ligneUtils				:LignesUtils = new LignesUtils();
		private var _temp					:temp = new temp();
		
		
		//VARIABLES LOCALES
		public var listCommande				:ArrayCollection = new ArrayCollection();
		private var listArticles			:ArrayCollection = new ArrayCollection();
		private var _articlesXML			:XML = new XML();
		private var _articlesToSendXML		:XML = new XML();
		
		
		//CONSTANTES
		private const ABO_VOIX				:String = "Abo Voix";
		private const ABO_DATA				:String = "Abo Data";
		
//////////////////////////////////////////////////////   CONSTRUCTEUR    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


		public function PopUpSaisieTechniqueImpl()
		{
			addEventListener("renommerSousTete",										sousTeteHandler);
			_gPanier.addEventListener(ArticleEvent.ARTICLE_UPDATED,						articleUpdateHandler);
			_gPanier.addEventListener(AbstractGestionPanier.LIVRAISON_MANAGED,			livraisonManagedHandler);
			_temp.addEventListener("articlesArrived",									articlesArrivedHandler);
		}

//////////////////////////////////////////////////////   PROTECTED    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		protected function creationCompleteHandler():void
		{
			_temp.fournirArticlesCommande(commande);
		}
		
		protected function closeHandler():void
		{
			dispatchEvent(new Event("livraisonClosed"));
			PopUpManager.removePopUp(this);
		}
		
		protected function btnAnnulClickHandler():void
		{
			closeHandler();
		}
		
		protected function btnValidClickHandler():void
		{
			if(checkChamps())
			{
				sendSousTeteIfChange();
				dispatchEvent(new Event("livraisonClosed"));
			}
			else
			{
				Alert.show("Veuillez remplir tous les champs obligatoire!","Consoview");
			}
		}

//////////////////////////////////////////////////////   PRIVATE    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
		private function sendSousTeteIfChange():void
		{
			for(var i:int = 0;i < listCommande.length;i++)
			{
				if(listCommande[i].LIGNE != listCommande[i].SOUSTETE)
				{
					if(listCommande[i].BOOL_LIGNE as Boolean)
					{
						_ligneUtils.renommerSousTete(listCommande[i].IDSOUSTETE,listCommande[i].LIGNE);
					}
				}
			}
			dispatchEvent(new Event("renommerSousTete"));
		}
		
		private function sousTeteHandler(e:Event):void
		{
			updateArticle();
		}
		
		private function articleUpdateHandler(e:Event):void
		{
			_articlesToSendXML = <articles></articles>;
			formatArrayCollectionToXML();
			_gPanier.manageLivraisonEquipements(_articlesToSendXML);
		}
		
		
		private function updateArticle():void
		{
			var xmlList:XMLList = _articlesXML.children();
			var nbre:int = xmlList.length();
			if(nbre > 1)
			{
				updateFormatMultiXml(xmlList);
			}
			else
			{
				updateFormatOneXml(xmlList[0],0);
			}
			_gPanier.majArtilcesOperation(commande,_articlesXML);
		}
		
		private function updateFormatMultiXml(xmlList:XMLList):void
		{
			for(var i:int = 0;i < xmlList.length();i++)
			{
				updateFormatOneXml(xmlList[i],i);
			}
		}
		
		private function updateFormatOneXml(xml:XML,value:int):void
		{
			if(xml != null)
			{
				if(listCommande[value].LIGNE != listCommande[value].SOUSTETE)
				{
					if(listCommande[value].BOOL_LIGNE as Boolean)
					{
						xml.soustete = listCommande[value].LIGNE;
					}
				}
				var temp:XMLList = xml.equipements.equipement;
				for(var i:int = 0;i < temp.length();i++)
				{
					if(temp[i].code_ihm == "CarteSim" || temp[i].code_ihm == "Mobile")
					{
						for(var j:int = 0; j < listCommande.length;j++)
						{
							if(temp[i].idequipement == listCommande[j].IDEQUIPEMENT_SIM)
							{
								temp[i].numeroserie = listCommande[j].NUM_CARTESIM;
								temp[i].codepin = listCommande[j].PIN;
								temp[i].codepuk = listCommande[j].PUK;
							}
							if(temp[i].idequipement == listCommande[j].IDEQUIPEMENT_MOB)
							{
								temp[i].numeroserie = listCommande[j].IMEI;
							}
						}
					}
				}
			}
		}

		private function formatArrayCollectionToXML():void
		{
			var article : XML = <article></article>;
			var equipement : XML = <equipement></equipement>;
			var equipements : XML = <equipements></equipements>;

			for(var i:int = 0;i < listCommande.length;i++)
			{
				equipements = <equipements></equipements>;
				if(listCommande[i].BOOL_TERMINAL as Boolean)
				{
					equipement = <equipement></equipement>;
					equipement.appendChild(<idequipement>{listCommande[i].IDEQUIPEMENT_MOB}</idequipement>);
					equipement.appendChild(<numeroserie>{listCommande[i].IMEI}</numeroserie>);
					equipement.appendChild(<codepin></codepin>);
					equipement.appendChild(<codepuk></codepuk>);
					equipements.appendChild(equipement);
				}
				if(listCommande[i].BOOL_CARTESIM as Boolean)
				{
					equipement = <equipement></equipement>;
					equipement.appendChild(<idequipement>{listCommande[i].IDEQUIPEMENT_SIM}</idequipement>);
					equipement.appendChild(<numeroserie>{listCommande[i].NUM_CARTESIM}</numeroserie>);
					equipement.appendChild(<codepin>{listCommande[i].PIN}</codepin>);
					equipement.appendChild(<codepuk>{listCommande[i].PUK}</codepuk>);
					equipements.appendChild(equipement);
				}
				article.appendChild(equipements);
			}
			_articlesToSendXML.appendChild(article);
		}

		private function formatXMLToArrayCollection():void
		{
			listArticles = new ArrayCollection();
			var xmlList:XMLList = _articlesXML.children();
			var nbre:int = xmlList.length();
			if(nbre > 1)
			{
				formatXMLList(xmlList);
			}
			else
			{
				formatXML(xmlList[0]);
			}
			checkTerminauxOnlyOrNot();
		}
		
		private function formatXMLList(xmlList:XMLList):void
		{	
			for(var i:int = 0;i < xmlList.length();i++)
			{
				formatXML(xmlList[i]);
			}
		}
		
		private function formatXML(xml:XML):void
		{
			if(!xml) return;
			var object			:Object = new Object();
			var xmlListEquip	:XMLList = 	xml.equipements.children();
			var xmlListRess		:XMLList =	xml.ressources.children();
			var arrayEquip		:ArrayCollection = new ArrayCollection();
			var arrayRess		:ArrayCollection = new ArrayCollection();
			var items			:ArrayCollection = new ArrayCollection();
			var arrayResult		:ArrayCollection = new ArrayCollection();
			 	
			if(xml.fpc[0] != "")
			{
				object.ENGAGEMENT_RESSOURECES 	= giveMeDate(String(xml.fpc[0]));
			}
			else
			{
				object.ENGAGEMENT_RESSOURECES   = Number(xml.engagement_res[0]);
				object.ENGAGEMENT_EQUIPEMENTS 	= Number(xml.engagement_eq[0]);
			}
			object.NOMEMPLOYE 				= lookForCollaborateur(String(xml.nomemploye[0]));
			object.SOUSTETE 				= String(xml.soustete[0]);
			object.IDSOUSTETE 				= Number(xml.idsoustete[0]);

			arrayEquip 	= addEquipements(xmlListEquip);
			arrayRess 	= addRessources(xmlListRess);
			items = addToArrayCollection(arrayEquip,arrayRess);
			
			listArticles.addItem(addToObject(items,object));
		}
		
		private function giveMeDate(strg:String):String
		{
			var day:String 		= strg.substr(8,2);
			var month:String	= strg.substr(5,2);
			var year:String		= strg.substr(0,4);
			return day + "/" + month + "/" + year;
		}
		
		private function addEquipements(xmlList:XMLList):ArrayCollection
		{
			var object:Object = new Object();
			var arrayCollection:ArrayCollection = new ArrayCollection();
			for(var i:int = 0; i < xmlList.length();i++)
			{
				object = new Object();
				object.TYPE_EQUIPEMENT 	= String(xmlList[i].type_equipement);
				object.LIBELLE_PRODUIT 	= String(xmlList[i].libelle);
				object.ID_PRODUIT 		= String(xmlList[i].idequipement);
				object.PRIX_UNIT 		= Number(xmlList[i].prix);
				object.TOTAL			= Number(xmlList[i].prix);
				if(xmlList[i].code_ihm == "Mobile")
				{
					object.IMEI = String(xmlList[i].numeroserie);
				}
				
				if(xmlList[i].code_ihm == "CarteSim")
				{
					object.NUM_CARTESIM = String(xmlList[i].numeroserie);
					object.PUK = String(xmlList[i].codepuk);
				}
				
				arrayCollection.addItem(object);
			}
			return arrayCollection;
		}
		
		private function addRessources(xmlList:XMLList):ArrayCollection
		{
			var object:Object = new Object();
			var arrayCollection:ArrayCollection = new ArrayCollection();
			for(var i:int = 0; i < xmlList.length();i++)
			{
				if(String(xmlList[i].libelle) != "")
				{
					object = new Object();
					object.TYPE_EQUIPEMENT 	= String(xmlList[i].theme);
					object.LIBELLE_PRODUIT 	= String(xmlList[i].libelle);
					object.ID_PRODUIT 		= String(xmlList[i].idressource);
					object.PRIX_UNIT 		= " - ";
					object.TOTAL 			= " - ";
					arrayCollection.addItem(object);
				}
			}
			return arrayCollection;
		}
		
		private function addToArrayCollection(equipements:ArrayCollection,ressources:ArrayCollection):ArrayCollection
		{
			var arrayCollection:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0; i < equipements.length;i++)
			{
				arrayCollection.addItem(equipements[i]);
			}
			for(var j:int = 0; j < ressources.length;j++)
			{
				arrayCollection.addItem(ressources[j]);
			}
			return arrayCollection;
		}
		
		private function addToObject(allItems:Object,info:Object):Object
		{
			var arrayCollection:ArrayCollection = new ArrayCollection();
			var object:Object = new Object();
			
			object.ITEMS 			= allItems;
			object.COLLABORATEUR 	= info.NOMEMPLOYE;
			object.ENGAGEMENT 		= info.ENGAGEMENT_RESSOURECES;
			object.SOUSTETE 		= info.SOUSTETE;
			object.IDSOUSTETE		= info.IDSOUSTETE;
			object.PRIX 			= calculPrice(allItems as ArrayCollection);
			return object;
		}
		
		private function calculPrice(allItems:ArrayCollection):String
		{
			var total:Number = 0;
			for(var i:int = 0; i < allItems.length;i++)
			{
				if(allItems[i].PRIX_UNIT != " - ")
				{
					total = total + Number(allItems[i].PRIX_UNIT);
				}
			}
			return total.toString();
		}
		
		private function lookForCollaborateur(name:String):String
		{
			var nameCollaborateur:String = "";
			if(name == "")
			{
				nameCollaborateur = " - " 
			}
			else
			{
				nameCollaborateur = name;
			}
			return nameCollaborateur;
		}

		private function checkTerminauxOnlyOrNot():void
		{
			var eq:ArrayCollection = new ArrayCollection();
			if(listArticles != null && listArticles.length > 0)
			{
				var  object:Object = new Object();
				for(var i:int = 0;i < listArticles.length;i++)
				{
					object = new Object();
					object.LIGNE = listArticles[i].SOUSTETE;
					object.IDSOUSTETE = listArticles[i].IDSOUSTETE;
					object.IMEI = listArticles[i].IMEI;
					object.NUM_CARTESIM	= listArticles[i].NUM_CARTESIM;
					object.PUK = listArticles[i].PUK;
					for(var j:int = 0;j < listArticles[i].ITEMS.length;j++)
					{
						if(listArticles[i].ITEMS[j].TYPE_EQUIPEMENT == "Carte Sim")
						{
							object.BOOL_CARTESIM 	= true;
							object.IDEQUIPEMENT_SIM = listArticles[i].ITEMS[j].ID_PRODUIT;
							object.NUM_CARTESIM		= listArticles[i].ITEMS[j].NUM_CARTESIM;
							object.PUK				= listArticles[i].ITEMS[j].PUK;
						}
						if(listArticles[i].ITEMS[j].TYPE_EQUIPEMENT == "Mobile")
						{
							object.BOOL_TERMINAL 	= true;
							object.IDEQUIPEMENT_MOB = listArticles[i].ITEMS[j].ID_PRODUIT;
							object.NAME_EQUIPEMENT 	= listArticles[i].ITEMS[j].LIBELLE_PRODUIT;
							object.IMEI				= listArticles[i].ITEMS[j].IMEI;
						}
					}
					if(!object.BOOL_TERMINAL as Boolean)
					{
						object.NAME_EQUIPEMENT = "CARTE SIM";
					}
					eq.addItem(object);
				}
				mapDataSaisieTechniqueEquipement(eq);
			}
		}

		private function mapDataSaisieTechniqueEquipement(eq:ArrayCollection):void
		{
			var saisie:SaisieTechniqueEquipement;
			for(var i:int = 0;i < eq.length;i++)
			{
				saisie = new SaisieTechniqueEquipement();
				saisie.BOOL_CARTESIM 	= eq[i].BOOL_CARTESIM;
				saisie.BOOL_TERMINAL 	= eq[i].BOOL_TERMINAL;
				saisie.IDEQUIPEMENT_MOB = eq[i].IDEQUIPEMENT_MOB;
				saisie.IDEQUIPEMENT_SIM = eq[i].IDEQUIPEMENT_SIM;
				saisie.NAME_EQUIPEMENT 	= eq[i].NAME_EQUIPEMENT;
				saisie.LIGNE			= eq[i].LIGNE;
				saisie.SOUSTETE			= eq[i].LIGNE;
				saisie.IDSOUSTETE		= eq[i].IDSOUSTETE;
				saisie.IMEI				= eq[i].IMEI;
				saisie.NUM_CARTESIM		= eq[i].NUM_CARTESIM;
				saisie.PUK				= eq[i].PUK;

				listCommande.addItem(saisie);
			}
		}

		private function checkChamps():Boolean
		{
			var bool:Boolean = true;
			for(var i:int = 0;i < listCommande.length;i++)
			{
				if(listCommande[i].BOOL_TERMINAL as Boolean)
				{
					if(listCommande[i].IMEI == "")
					{
						bool = false;
					}
				}
				if(listCommande[i].BOOL_CARTESIM as Boolean)
				{
					if(listCommande[i].LIGNE == "" || listCommande[i].NUM_CARTESIM == "" || listCommande[i].PUK == "")
					{
						bool = false;
					}
				}
			}
			return bool;
		}

//////////////////////////////////////////////////////   EVENEMENTS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
		private function detailsFinishedHandler(e:Event):void
		{
			_temp.fournirArticlesCommande(commande);
		}
		
		private function articlesArrivedHandler(e:Event):void
		{
			_articlesXML = _temp.listeArticles;
			formatXMLToArrayCollection();
		}
		
		private function livraisonManagedHandler(e:Event):void
		{
			ConsoviewAlert.afficherOKImage("Enregistrement effectué!");
			closeHandler();
		}

	}
}