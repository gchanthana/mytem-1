package commandemobile.popup
{
	import commandemobile.temp.temp;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpFicheProduitImpl extends Box
	{
		
//VARIABLES------------------------------------------------------------------------------

		//COMPOSANTS
		
		
		//OBJECTS
		private var _temp					:temp = new temp()
		
		//VARIABLES GLOBALES
		public var detailsProduitReceived	:Object;
		
		//VARIABLES LOCALES
		public var listProduitDetailler		:Object = new Object();
		public var listDetails				:ArrayCollection = new ArrayCollection();

//VARIABLES------------------------------------------------------------------------------

//METHODES PUBLIC------------------------------------------------------------------------
		
		public function PopUpFicheProduitImpl()
		{
			addEventListener("detaillerProduit",		detaillerProduitHandler);
			_temp.addEventListener("listeFeatureOk",	listeFeatureOkHandler);
		}
		
//METHODES PUBLIC------------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
		
		protected function detaillerProduitHandler(e:Event):void
		{
			if(detailsProduitReceived != null)
			{
				listProduitDetailler = detailsProduitReceived;
				_temp.getProductFeature(listProduitDetailler.IDEQUIPEMENT_FOURNISSEUR);
			}
		}
		
		protected function btnCloseClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}
		
//METHODES PROTECTED---------------------------------------------------------------------

		private function listeFeatureOkHandler(e:Event):void
		{
			listDetails = _temp.listeFeature;
		}


	}
}