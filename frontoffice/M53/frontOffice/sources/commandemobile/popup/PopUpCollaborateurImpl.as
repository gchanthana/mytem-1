package commandemobile.popup
{
	import commandemobile.entity.Configuration;
	import commandemobile.temp.temp;
	
	import ficheemploye.InfosPersoCollaborateurIHM;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpCollaborateurImpl extends TitleWindow
	{

//VARIABLES------------------------------------------------------------------------------
		
		//COMPOSANTS
		public var dgListCollaborateurs:DataGrid;
		public var txtptSearch:TextInput;
		
		public var IDPOOL_GESTIONNAIRE:Number = 0;		

		private var _temp:temp = new temp();
		public var _itemsSelected:Object;
		public var newCollabo				:InfosPersoCollaborateurIHM; 
		
		private var indexObjectSelected:int = -1;
		
		//
		public var listCollaborateur:ArrayCollection = new ArrayCollection();
		public var _listCollaborateursVisible:ArrayCollection = new ArrayCollection();
		
		private var collaborateurSelected:Configuration = new Configuration();

//VARIABLES------------------------------------------------------------------------------

//METHODE PUBLIC-------------------------------------------------------------------------
		
		public function PopUpCollaborateurImpl()
		{
			_temp.addEventListener("employeLoaded",collaborateursArrivedHandler);
			
			addEventListener("listerCollaborateurs",listerCollaborateursHandler);
			addEventListener("collaborateurIsSelected",selectedHandler);
			addEventListener("collaborateurIsNotSelected",notSelectedHandler);
		}
		
//METHODE PUBLIC-------------------------------------------------------------------------		

//METHODE PROTECTED----------------------------------------------------------------------
		
		protected function _closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnNewCollabClickHandler():void
		{
			newCollabo = new InfosPersoCollaborateurIHM();
			newCollabo.IDPOOL_GESTIONNAIRE = IDPOOL_GESTIONNAIRE;
			newCollabo.ip_collaborateur = newCollabo;
			newCollabo.addEventListener("infoCollaborateur",listerCollaborateursHandler);
			PopUpManager.addPopUp(newCollabo,this,true);
			PopUpManager.centerPopUp(newCollabo);
		}
		
		
		protected function dgListCollaborateursClickHandler():void
		{
			if(dgListCollaborateurs.selectedItem != null)
			{
				var ev:Event = new Event(Event.ACTIVATE);
				selectedHandler(ev);
			}
		}
		
		protected function btReinitialisationClickHandler():void
		{
			txtptSearch.text = "";
			_temp.fournirListeEmployes(IDPOOL_GESTIONNAIRE,"");
		}
		
		protected function txtptSearchHandler():void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			listCollaborateur = new ArrayCollection();
			if(txtptSearch.text == "")
			{
				listCollaborateur = _listCollaborateursVisible;
			}
			else
			{
				for(var i:int = 0;i < _listCollaborateursVisible.length;i++)
				{
					if(_listCollaborateursVisible[i].NOM.toString().toLowerCase().indexOf(txtptSearch.text.toLowerCase()) != -1)
					{ 
						arrayC.addItem(_listCollaborateursVisible[i]); 
					}
				}
				listCollaborateur = arrayC;
			}
		}
		
		protected function btnValiderClickHandler():void
		{
			if(validerSelectedCollaborateur())
			{
				_itemsSelected = new Configuration();
				_itemsSelected = collaborateurSelected as Configuration;
			}
			else
			{
				_itemsSelected = new Configuration();
				_itemsSelected = collaborateurSelected as Configuration;
			}

			dispatchEvent(new Event("POPUP_COLLABORATEURS_CLOSED_AND_VALIDATE"));
			PopUpManager.removePopUp(this);
		}
		
		protected function btnAnnulerClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}

//METHODE PROTECTED----------------------------------------------------------------------

//METHODE PRIVATE------------------------------------------------------------------------
		
		private function validerSelectedCollaborateur():Boolean
		{
			var ok:Boolean = false;
			for(var i:int = 0; i < listCollaborateur.length;i++)
			{
				if(listCollaborateur[i].SELECTED)
				{
					collaborateurSelected = new Configuration();
					collaborateurSelected = convertToConfiguration(listCollaborateur[i]);
					ok = true;
				}
			}
			return ok;
		}
		
		private function  listerCollaborateursHandler(e:Event):void
		{
			listCollaborateur = new ArrayCollection();
			_listCollaborateursVisible = new ArrayCollection();
			_temp.fournirListeEmployes(IDPOOL_GESTIONNAIRE,"");
		}
		
		private function  collaborateursArrivedHandler(e:Event):void
		{
			listCollaborateur = new ArrayCollection();
			_listCollaborateursVisible = new ArrayCollection();
			for(var i:int = 0; i < _temp.listeEmployes.length;i++)
			{
				if(_temp.listeEmployes[i].NOM == _itemsSelected.NOM)
				{
					_temp.listeEmployes[i].SELECTED = true;
				}
				else
				{
					_temp.listeEmployes[i].SELECTED = false;
				}
				
				listCollaborateur.addItem(_temp.listeEmployes[i]);
				_listCollaborateursVisible.addItem(_temp.listeEmployes[i]);
			}
		}
		
		private function selectedHandler(e:Event):void
		{
			if(dgListCollaborateurs.selectedItem != null)
			{
				indexObjectSelected = -1;
				for(var i:int = 0; i < listCollaborateur.length;i++)
				{
					if(listCollaborateur[i].NOM != dgListCollaborateurs.selectedItem.NOM)
					{
						listCollaborateur[i].SELECTED 			= false;
						_listCollaborateursVisible[i].SELECTED 	= false;
					}
					else
					{
						listCollaborateur[i].SELECTED 			= true;
						_listCollaborateursVisible[i].SELECTED 	= true;
					}
				}
				refreshDatagrid();
			}
		}
		
		private function notSelectedHandler(e:Event):void
		{
		
		}
		
		private function convertToConfiguration(object:Object):Configuration
		{
			var config:Configuration 	= new Configuration();
			config.CLE_IDENTIFIANT 		= object.CLE_IDENTIFIANT;
			config.COLLABORATEUR 		= object.NOM;
			config.IDCOLLABORATEUR 		= object.IDEMPLOYE;
			config.IDEMPLOYE 			= object.IDEMPLOYE;
			config.IDGROUPE_CLIENT 		= object.IDGROUPE_CLIENT;
			config.EMAIL 				= object.EMAIL;
			config.INOUT 				= object.INOUT;
			config.MATRICULE 			= object.MATRICULE;
			config.NOM 					= object.NOM;
			
			return config;
		}
		
		private function refreshDatagrid():void
		{
//			dgListCollaborateurs.dataProvider = listCollaborateur;
			listCollaborateur.itemUpdated(null);
		}

//METHODE PRIVATE------------------------------------------------------------------------

	}
}