package commandemobile.popup
{
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.managers.PopUpManager;
	
	public class PopUpConfirmationPasseOptionsImpl extends Box
	{
		public function PopUpConfirmationPasseOptionsImpl()
		{
		}
		
		protected function btnValiderClickHandler():void
		{
			dispatchEvent(new Event("passOptions",true));
			PopUpManager.removePopUp(this);
		}

		protected function btnAnnulerClickHandler():void
		{
			dispatchEvent(new Event("noPassOptions",true));
			PopUpManager.removePopUp(this);
		}

	}
}