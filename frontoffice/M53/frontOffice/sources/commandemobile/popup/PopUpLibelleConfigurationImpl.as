package commandemobile.popup
{
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	
	public class PopUpLibelleConfigurationImpl extends Box
	{
		//COMPOSANTS
		public var txtConfigurationName:TextInput;
		
		//VARIABLES GLOBALES
		public var libelleConfiguration:String = "";
		
		public function PopUpLibelleConfigurationImpl()
		{
			addEventListener("closeThis",closeThisHandler);
		}
		
		protected function btnValiderClickHandler():void
		{
			libelleConfiguration = txtConfigurationName.text;
			dispatchEvent(new Event("messageSaisie"));
		}
		protected function btnAnnulerClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function closeThisHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}

	}
}