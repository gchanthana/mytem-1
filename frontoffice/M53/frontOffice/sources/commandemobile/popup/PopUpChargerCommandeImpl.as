package commandemobile.popup
{
	import commandemobile.temp.commandeSNCF;
	import commandemobile.temp.temp;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpChargerCommandeImpl extends TitleWindow
	{

//VARIABLES------------------------------------------------------------------------------
	
		//IHM
		private var _popUpDetailConfiguration	:PopUpViewConfigurationElementsIHM;
	
		//COMPOSANTS
		public var dgListConfiguration			:DataGrid;
		
		//OBJETS
		private var _temp						:temp = new temp();
		private var _cmdSNCF					:commandeSNCF 	= new commandeSNCF();
		
		//VARIABLES GLOBALES
		public var configurationSelected		:ArrayCollection = new ArrayCollection();
		public var idPool						:int = 0;
		public var idProfil						:int = 0;
		public var configObject					:Object = new Object();
		public var idOperateur					:int = 0;
		public var operateurName				:String = "";
		public var newCommande					:Boolean = true;
				
		//VARIABLES LOCALES
		public var listConfiguration			:ArrayCollection = new ArrayCollection();
		private var _articlesXML				:XML = new XML();
		public var listArticles					:ArrayCollection = new ArrayCollection();
		
		//CONSTANTES
		private const ABO_VOIX			:String = "Abo Voix";
		private const ABO_DATA			:String = "Abo Data";
		private const ABO_PUSHMAIL		:String = "Abo Push Mail";

//VARIABLES------------------------------------------------------------------------------

//METHODES PUBLICS-----------------------------------------------------------------------
		
		//CONSTRUCTEUR (>LISTENER)
		public function PopUpChargerCommandeImpl()
		{
			addEventListener("listerConfiguration",			listeConfigurationOkHandler);
			addEventListener("configurationViewSelect",		viewConfigurationHandler);
			addEventListener("configSelected",				configSelectedHandler);
			addEventListener("configurationSelect",			configNotSelectedHandler);
			addEventListener("closePopUp",					closePopUp);
			addEventListener("eraseModele",					eraseModeleHandler);
			
			_temp.addEventListener("eraseModeleOk",			eraseModeleOkHandler);
			_temp.addEventListener("listeModeleOk",			listeModeleOkHandler);
			
			_cmdSNCF.addEventListener("listeModele",		listedConfigurationHandler);
		}
		
//METHODES PUBLICS-----------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
		
		protected function _closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnValiderClickHandler():void
		{
			var idModele:int = 0;
			for(var i:int = 0;i < listConfiguration.length;i++)
			{
				if(listConfiguration[i].SELECTED)
				{
					idModele = listConfiguration[i].IDMODELE;
				}
			}

			if(idModele > 0)
			{
				_temp.getModeleCommande(idModele);
			}
			else
			{
				Alert.show("Veuillez sélectionner une configuration!","Consoview");
			}
		}
		
		protected function dgListConfigurationClickHandler():void
		{
			var ev:Event = new Event(Event.ACTIVATE);
			configSelectedHandler(ev);
		}
		
		protected function btnAnnulerClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}
		
//METHODES PROTECTED---------------------------------------------------------------------

//METHODES PRIVATE-----------------------------------------------------------------------
		
		private function eraseModeleHandler(e:Event):void
		{
			_temp.deleteModeleCommande(dgListConfiguration.selectedItem.IDMODELE);
		}
		
		private function eraseModeleOkHandler(e:Event):void
		{
			dispatchEvent(new Event("listerConfiguration"));
			ConsoviewAlert.afficherOKImage("Modele supprimmer !");
			
		}
		
		//APPEL LA PROCEDURE PERMETTANT DE RECUPERER LA LISTE DES LIBELLE DES CONFIGURATIONS ENREGISTRER
		private function listeConfigurationOkHandler(e:Event):void
		{
//			_temp.getListModeleCommande(idPool);
			_cmdSNCF.fournirListeModele(idPool,idProfil);
		}
		
		//AFFICHE LE LIBELLE DES CONFIGURATION
		private function listedConfigurationHandler(e:Event):void
		{
			listConfiguration = new ArrayCollection();
			if(newCommande)
			{
				if(idOperateur != 0)
				{
					for(var j:int = 0;j < _cmdSNCF.listeModele.length;j++)
					{
						if(_cmdSNCF.listeModele[j].OPEURATEURID == idOperateur)
						{
							listConfiguration.addItem(_cmdSNCF.listeModele[j]);
						}
					}
				}
				else
				{
					listConfiguration = _cmdSNCF.listeModele;
				}
			}
			else
			{
				for(var i:int = 0;i < _cmdSNCF.listeModele.length;i++)
				{
					if(_cmdSNCF.listeModele[i].IDOPERATEUR == idOperateur)
					{
						listConfiguration.addItem(_cmdSNCF.listeModele[i]);
					}
				}
			}
			if(listConfiguration.length == 0)
			{
				Alert.show("Aucun modèle n'existe encore pour ce type de commande!","Consoview");
				PopUpManager.removePopUp(this);
			}
		}
		
		private function configSelectedHandler(e:Event):void//TESTS NULL
		{
			if(dgListConfiguration.selectedItem != null)
			{
				for(var i:int = 0;i < listConfiguration.length;i++)
				{
					if(dgListConfiguration.selectedItem.IDMODELE == listConfiguration[i].IDMODELE)
					{
						listConfiguration[i].SELECTED = true;
					}
					else
					{
						listConfiguration[i].SELECTED = false;
					}
				}
				refreshDatagrid();
			}
		}
		
		private function configNotSelectedHandler(e:Event):void
		{
			for(var i:int = 0;i < listConfiguration.length;i++)
			{
				listConfiguration[i].SELECTED = false;
			}
		}
		
		private function refreshDatagrid():void
		{
//			dgListConfiguration.dataProvider = listConfiguration;
			listConfiguration.itemUpdated(null);
		}

		private function viewConfigurationHandler(e:Event):void
		{
			if(dgListConfiguration.selectedItem != null)
			{
				_popUpDetailConfiguration = new PopUpViewConfigurationElementsIHM();
				_popUpDetailConfiguration.idConfiguration = dgListConfiguration.selectedItem.IDMODELE;
				
				PopUpManager.addPopUp(_popUpDetailConfiguration,this,true);
				PopUpManager.centerPopUp(_popUpDetailConfiguration);
					
				_popUpDetailConfiguration.dispatchEvent(new Event("listerConfiguration"));
				_popUpDetailConfiguration.addEventListener("refreshDatagrid",refresh);
			}
		}
		
		private function refresh(e:Event):void
		{
			dispatchEvent(new Event("listerConfiguration"));
		}
		
		private function closePopUp(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function listeModeleOkHandler(e:Event):void
		{
			 configObject = _temp.configObject;
			_articlesXML = new XML();
			_articlesXML = _temp.listeModele;
			formatXMLToArrayCollection();
			dispatchEvent(new Event("formatInArrayCollectionFinished"));
		}
		
		private function formatXMLToArrayCollection():void
		{
			listArticles = new ArrayCollection();
			var xmlList:XMLList = _articlesXML.children();
			var nbre:int = xmlList.length();
			if(nbre > 1)
			{
				formatXMLList(xmlList);
			}
			else
			{
				formatXML(xmlList[0]);
			}
		}
		
		private function formatXMLList(xmlList:XMLList):void
		{	
			for(var i:int = 0;i < xmlList.length();i++)
			{
				formatXML(xmlList[i]);
			}
		}
		
		private function giveMeDate(strg:String):String
		{
			var day:String 		= strg.substr(8,2);
			var month:String	= strg.substr(5,2);
			var year:String		= strg.substr(0,4);
			return day + "/" + month + "/" + year;
		}
		
		private function formatXML(xml:XML):void
		{
			if(xml == null)
			{
				listArticles = null;
				return;
			}
			var object			:Object = new Object();
			var xmlListEquip	:XMLList = 	xml.equipements.children();
			var xmlListRess		:XMLList =	xml.ressources.children();
			var arrayEquip		:ArrayCollection = new ArrayCollection();
			var arrayRess		:ArrayCollection = new ArrayCollection();
			var items			:ArrayCollection = new ArrayCollection();
			var arrayResult		:ArrayCollection = new ArrayCollection();
			 	

			
			object.SOUSTETE 				= String(xml.soustete[0]);
			object.IDSOUSTETE 				= Number(xml.idsoustete[0]);
			
			arrayEquip 	= addEquipements(xmlListEquip);
			arrayRess 	= addRessources(xmlListRess);
//			items = addToArrayCollection(arrayEquip,arrayRess);
			
			listArticles = addToObject(arrayEquip,arrayRess,object);
		}
		
		private function addEquipements(xmlList:XMLList):ArrayCollection
		{
			var object:Object = new Object();
			var arrayCollection:ArrayCollection = new ArrayCollection();
			for(var i:int = 0; i < xmlList.length();i++)
			{
				var strg:String = xmlList[i].idtype_equipement.toString();
				if( strg != "undefined")
				{ 
					var thisPrice:String = xmlList[i].prix.toString();
					thisPrice = thisPrice.replace(",",".");
					object = new Object();
					object.TYPE_EQUIPEMENT 				= String(xmlList[i].type_equipement);
					object.IDTYPE_EQUIPEMENT 			= String(xmlList[i].idtype_equipement);
					object.LIBELLE_EQ	 				= String(xmlList[i].libelle);
					object.IDEQUIPEMENT_FOURNISSEUR		= Number(xmlList[i].idequipementfournis);
					object.IDEQUIPEMENT_CLIENT			= Number(xmlList[i].idequipementclient);
					object.CODE_IHM 					= String(xmlList[i].code_ihm);
					object.PRIX_CATALOGUE				= Number(thisPrice);
					object.TOTAL						= Number(thisPrice);
					object.EQUIPEMENT					= 1;
					arrayCollection.addItem(object);
				}
			}
			return arrayCollection;
		}
		
		private function addRessources(xmlList:XMLList):ArrayCollection
		{
			var object:Object = new Object();
			var arrayCollection:ArrayCollection = new ArrayCollection();
			for(var i:int = 0; i < xmlList.length();i++)
			{
				if(String(xmlList[i].libelle) != "")
				{
					object = new Object();
					
					if( String(xmlList[i].theme) == ABO_VOIX || String(xmlList[i].theme) == ABO_DATA || String(xmlList[i].theme) == ABO_PUSHMAIL)
					{
						object.TYPE_THEME 	= "Abonnement";
						object.SUR_THEME 	= "Abonnement";
					}
					else
					{
						object.SUR_THEME 	= "Options";
					}
					object.THEME_LIBELLE 		= String(xmlList[i].theme)
					object.LIBELLE_PRODUIT 		= String(xmlList[i].libelle);
					object.IDPRODUIT_CATALOGUE 	= Number(xmlList[i].idproduit_catalogue);
					object.IDTHEME_PRODUIT		= Number(xmlList[i].idThemeProduit);
					object.PRIX_UNIT 			= Number(xmlList[i].prix);
					object.TOTAL 				= Number(xmlList[i].prix);
					object.EQUIPEMENT			= 0;
					arrayCollection.addItem(object);
				}
			}
			return arrayCollection;
		}
				
		private function addToArrayCollection(equipements:ArrayCollection,ressources:ArrayCollection):ArrayCollection
		{
			var arrayCollection:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0; i < equipements.length;i++)
			{
				arrayCollection.addItem(equipements[i]);
			}
			for(var j:int = 0; j < ressources.length;j++)
			{
				arrayCollection.addItem(ressources[j]);
			}
			return arrayCollection;
		}
		
		private function addToObject(equiItems:Object,ressItems:Object,info:Object):ArrayCollection
		{
			var arrayCollection:ArrayCollection = new ArrayCollection();
			var object:Object = new Object();
			var arrayCTemp:ArrayCollection = new ArrayCollection();
			arrayCTemp.addItem(equiItems);
			arrayCTemp.addItem(ressItems);
			object.EQUIPEMENTS 		= equiItems;
			object.RESSOURCES 		= ressItems;
			object.COLLABORATEUR 	= info.NOMEMPLOYE;
			object.SOUSTETE 		= info.SOUSTETE;
			object.IDSOUSTETE		= info.IDSOUSTETE;
			object.PRIX 			= calculPrice(arrayCTemp as ArrayCollection);
			arrayCollection.addItem(object);
			return arrayCollection;
		}
		
		private function calculPrice(allItems:ArrayCollection):int
		{
			var total:Number = 0;
			for(var i:int = 0; i < allItems.length;i++)
			{
				var arrayCtem:ArrayCollection = allItems[i];
				for(var j:int = 0; j < arrayCtem.length;j++)
				{
					total = total + Number(arrayCtem[j].PRIX_UNIT);
				}
			}
			return total;
		}

		//RAFRAICHI LE DATAGRID
		private function refreshGrid():void
		{
//			dgListConfiguration.dataProvider = listConfiguration;
			listConfiguration.itemUpdated(null);
		}
		
//METHODES PRIVATE-----------------------------------------------------------------------

	}
}