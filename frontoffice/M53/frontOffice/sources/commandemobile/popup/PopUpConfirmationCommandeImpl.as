package commandemobile.popup
{
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.managers.PopUpManager;
	
	public class PopUpConfirmationCommandeImpl extends Box
	{
		public function PopUpConfirmationCommandeImpl()
		{
		}
		
		protected function btnValiderClickHandler():void
		{
			//APPEL DE PROCEDURE POUR VALIDER ET ENVOYER LA COMMANDE
			dispatchEvent(new Event("sendCommande"));
			PopUpManager.removePopUp(this);
			dispatchEvent(new Event("closePopUpCommande"));
		}
		
		protected function btnAnnulerClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}

	}
}