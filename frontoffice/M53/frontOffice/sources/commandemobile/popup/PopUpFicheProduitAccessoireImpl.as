package commandemobile.popup
{
	import flash.events.Event;
	
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpFicheProduitAccessoireImpl extends TitleWindow
	{

//VARIABLES------------------------------------------------------------------------------
		
		//COMPOSANTS
		
		//
		public var detailsProduitReceived:Object;
		
		public var _listProduitDetailler:Object = new Object;

//VARIABLES------------------------------------------------------------------------------
		
//METHODES PUBLIC------------------------------------------------------------------------
		
		public function PopUpFicheProduitAccessoireImpl()
		{
			addEventListener("detaillerProduit",detaillerProduitHandler);
		}

//METHODES PUBLIC------------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
		
		protected function detaillerProduitHandler(e:Event):void
		{
			if(detailsProduitReceived != null)
			{
				_listProduitDetailler = detailsProduitReceived;
			}
		}
		
		protected function btnCloseClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function closeMeHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

//METHODES PROTECTED---------------------------------------------------------------------

	}
}