package commandemobile.popup
{
	import commandemobile.entity.Commande;
	import commandemobile.entity.ItemSelected;
	import commandemobile.temp.commandeSNCF;
	import commandemobile.temp.temp;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpChoiceOptionsImpl extends TitleWindow
	{

//VARIABLES------------------------------------------------------------------------------
		
		//COMPOSANTS
		public var ckbxSelectAll		:CheckBox;
		public var ckbxVoix				:CheckBox;
		public var ckbxData				:CheckBox;
		public var ckbxPushMail			:CheckBox;
		public var ckbxPush:CheckBox;
		public var dgListProduit		:DataGrid;
		public var txtptSearch			:TextInput;
		public var lblFavoris			:Label;
		public var lblFavorisUnder		:Label;
		public var lblTout				:Label;
		public var lblToutUnder			:Label;
		
		//OBJETS
		private var _cmdSNCF			:commandeSNCF 	= new commandeSNCF();
		public var _temp				:temp = new temp();
		public var _itemsSelected		:ItemSelected;
		public var _cmd					:Commande;
		
		//VARIABLES LOCALES
		private var _listProduit		:ArrayCollection = new ArrayCollection();
		private var _arrayTemp			:ArrayCollection = new ArrayCollection();
		private var _listProduitVisible	:ArrayCollection = new ArrayCollection();
		private var _refListProduit		:ArrayCollection = new ArrayCollection();
		private var _isFavorite			:Boolean = true;
		private var _datagrid			:Boolean = true;

		//CONSTANTES
		private const ABO_VOIX			:String = "Options Voix";
		private const ABO_DATA			:String = "Options Data";
		private const ABO_PUSHMAIL		:String = "Options Diverses";
		private const ABO_PUSH			:String = "Abo Push Mail";
		
//VARIABLES------------------------------------------------------------------------------

//PROPRIETEES PUBLIC---------------------------------------------------------------------

		//RECUPERE LA LISTE DES OPTIONS
		public function get listProduit(): ArrayCollection
	    {
	    	return _listProduit;
	    }
	    
	    public function set listProduit(value:ArrayCollection): void
	    {
	    	_listProduit = value;
	    }

//PROPRIETEES PUBLIC---------------------------------------------------------------------

//METHODES PUBLIC------------------------------------------------------------------------

		//CONSTRUCTEUR (>LISTENER)
		public function PopUpChoiceOptionsImpl()
		{
			_cmdSNCF.addEventListener("listeAbonnementsOptions", listnerTempEvent);
			addEventListener("listerProduit", listerProduitHandler);
			addEventListener("thisIsFavorite", thisIsAFavoriteSelected);
			addEventListener("thisIsNotFavorite", thisIsNotFavoriteSelected);
			addEventListener("optionIsSelected", thisIsOptionSelected);
			addEventListener("optionIsNotSelected", thisIsNotOptionSelected);
		}
		
//METHODES PUBLIC------------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
		
		protected function _closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		//COCHE/DECOCHE LA CASE DE LA LIGNE SELECTIONNE
		protected function dgListProduitItemClickHandler(event:Event):void
		{
			if(event.currentTarget == null) return
			
			var item:Object = event.currentTarget.selectedItem;
						 
			if(item.SELECTED ==  true)
			{
				item.SELECTED = false	
			}
			else
			{
				item.SELECTED = true;
				
			}		 
			
			switch(item.SELECTED)
			{
				case true: 	{
								dispatchEvent(new Event("optionIsSelected",true)); break;
							}
				case false: {
								dispatchEvent(new Event("optionIsNotSelected",true)); break;
							}
				default:
				{ }
			}
			
			listProduit.itemUpdated(item);		
		}
		
		
		protected function dgListProduitClickHandler():void
		{
			if(dgListProduit.selectedItem != null)
			{
				if(_datagrid)
				{
					if(!dgListProduit.selectedItem.SELECTED)
					{
						dgListProduit.selectedItem.SELECTED = true;
					}
					else
					{
						dgListProduit.selectedItem.SELECTED = false;
					}
				}
				listProduit.itemUpdated(dgListProduit.selectedItem);
			}
			_datagrid = true;
		}

		//RECHERCHE ET AFFICHAGE DE LA LISTE DES OPTIONS AU FUR ET A MESURE QUE L'ON SAISIE DU TEXTE
		protected function txtptSearchHandler():void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			listProduit = new ArrayCollection();
			if(txtptSearch.text == "")
			{
				listProduit = _listProduitVisible;
			}
			else
			{
				for(var i:int = 0;i < _listProduitVisible.length;i++)
				{
					if(_listProduitVisible[i].LIBELLE_PRODUIT.toString().toLowerCase().indexOf(txtptSearch.text.toLowerCase()) != -1)
					{ 
						arrayC.addItem(_listProduitVisible[i]); 
					}
				}
			
				listProduit = arrayC;
			}
		}
		
		//REINITIALISE LE CHAMP DE RECHERCHE AINSI QUE LE DATAGRID (>REAFFICHE TOUS)
		protected function btReinitialisationClickHandler():void
		{
			txtptSearch.text = "";
			listProduit = new ArrayCollection();
			_listProduitVisible = new ArrayCollection();
			_cmdSNCF.fournirListeAbonnementsOptions(_cmd.IDGESTIONNAIRE,_cmd.IDOPERATEUR,_cmd.IDPROFIL_EQUIPEMENT ,false);
			whatIsChbxSelected();
		}
		
		//AFFICHE TOUT
		protected function ckbxSelectAllChangeHandler():void
		{
			ckbxData.selected = ckbxVoix.selected = ckbxPushMail.selected = ckbxPush.selected = ckbxSelectAll.selected;
			whatIsChbxSelected();
			refreshDatagrid();
		}
		
		//AFFICHE LES OPTIONS 'VOIX'
		protected function ckbxVoixChangeHandler():void
		{
			checkSelectAll();
			whatIsChbxSelected();
			refreshDatagrid();
		}
		
		//AFFICHE LES OPTIONS 'DATA'
		protected function ckbxDataChangeHandler():void
		{
			checkSelectAll();
			whatIsChbxSelected();
			refreshDatagrid();
		}
		
		//AFFICHE LES OPTIONS 'DIVERSES'
		protected function ckbxPushMailChangeHandler():void
		{
			checkSelectAll();
			whatIsChbxSelected();
			refreshDatagrid();
		}
		
		//AFFICHE LES OPTIONS 'PUSH MAIL'
		protected function ckbxPushChangeHandler():void
		{
			checkSelectAll();
			whatIsChbxSelected();
			refreshDatagrid();
		}
		
		//AFFICHE LES FAVORIS
		protected function lblFavorisClickHandler():void
		{
			_isFavorite = true;
			
			lblFavoris.visible = false;
			lblFavoris.width = 0;
			lblFavorisUnder.visible = true;
			lblFavorisUnder.width = 50;
			
			lblTout.visible = true;
			lblTout.width = 50;
			lblToutUnder.visible = false;
			lblToutUnder.width = 0;
			
			whatIsChbxSelected();
		}

		//AFFICHE LES FAVORIS
		protected function lblToutClickHandler():void
		{
			_isFavorite = false;

			lblTout.visible = false;
			lblTout.width = 0;
			lblToutUnder.visible = true;
			lblToutUnder.width = 50;
			
			lblFavoris.visible = true;
			lblFavoris.width = 50;
			lblFavorisUnder.visible = false;
			lblFavorisUnder.width = 0;

			whatIsChbxSelected();
		}
		
		//VALIDE LA SELECTION DES OPTIONS
		protected function btnValiderClickHandler():void
		{
			var OPTIONS:ArrayCollection = new ArrayCollection();
			for(var i:int = 0;i < listProduit.length;i++)
			{
				if(listProduit[i] != null)
				{
					if(listProduit[i].SELECTED)
					{ 
						OPTIONS.addItem(listProduit[i]);
					}
				}
			}
			if(OPTIONS.length > 0)
			{
				_itemsSelected.OPTIONS = new ArrayCollection();
				_itemsSelected.OPTIONS = OPTIONS;
				dispatchEvent(new Event("POPUP_OPTIONS_CLOSED_AND_VALIDATE"));
				PopUpManager.removePopUp(this);
			}
			else
			{
				Alert.show("Veuillez sélectionner une/des option(s)!","Consoview");
			}
		}
		
		//ANNULE LA SELECTION DES OPTIONS
		protected function btnAnnulerClickHandler():void
		{
			dispatchEvent(new Event("POPUP_OPTIONS_CLOSED_AND_VALIDATE"));
			PopUpManager.removePopUp(this);
		}

//METHODES PROTECTED---------------------------------------------------------------------

//METHODES PRIVATE-----------------------------------------------------------------------
		
		//REGADE QUELLES CHECKBOX SONT SELECTIONNEES ET AFFICHE LES OPTIONS SUIVANT LE FILTRE
		private function whatIsChbxSelected():void
		{
			txtptSearch.text = "";
			listProduit = new ArrayCollection();
			_listProduitVisible = new ArrayCollection();
			checkIsFavorite();
			
			if(ckbxVoix.selected == true && ckbxData.selected == true && ckbxPushMail.selected == true && ckbxPush.selected == true)
			{
				ckbxSelectAll.selected = true;
			}

			if(!ckbxSelectAll.selected)
			{
				if(ckbxVoix.selected == true && ckbxData.selected == true && ckbxPushMail.selected == true)
				{
					addDataToPrint(ABO_VOIX,ABO_DATA,ABO_PUSHMAIL);
					return;
				}
				if(ckbxVoix.selected == true && ckbxPushMail.selected == true && ckbxPush.selected == true)
				{
					addDataToPrint(ABO_VOIX,ABO_PUSHMAIL,ABO_PUSH);
					return;
				}
				if(ckbxVoix.selected == true && ckbxData.selected == true && ckbxPush.selected == true)
				{
					addDataToPrint(ABO_VOIX,ABO_DATA,ABO_PUSH);
					return;
				}
				if(ckbxData.selected == true && ckbxPushMail.selected == true && ckbxPush.selected == true)
				{
					addDataToPrint(ABO_DATA,ABO_PUSHMAIL,ABO_PUSH);
					return;
				}
				
				
				
				if(ckbxVoix.selected == true && ckbxData.selected == true)
				{
					addDataToPrint(ABO_VOIX,ABO_DATA);
					return;
				}
				
				if(ckbxVoix.selected == true && ckbxPushMail.selected == true)
				{
					addDataToPrint(ABO_VOIX,ABO_PUSHMAIL);
					return;
				}
				
				if(ckbxData.selected == true && ckbxPushMail.selected == true)
				{
					addDataToPrint(ABO_DATA,ABO_PUSHMAIL);
					return;
				}
				
				if(ckbxVoix.selected == true && ckbxPush.selected == true)
				{
					addDataToPrint(ABO_VOIX,ABO_PUSH);
					return;
				}
				
				if(ckbxData.selected == true && ckbxPush.selected == true)
				{
					addDataToPrint(ABO_DATA,ABO_PUSH);
					return;
				}
				
				if(ckbxPushMail.selected == true && ckbxPush.selected == true)
				{
					addDataToPrint(ABO_PUSHMAIL,ABO_PUSH);
					return;
				}
				
				if(ckbxVoix.selected == true)
				{
					dataToPrint(ABO_VOIX);
					return;
				}
				
				if(ckbxData.selected == true)
				{
					dataToPrint(ABO_DATA);
					return;
				}
				
				if(ckbxPush.selected == true)
				{
					dataToPrint(ABO_PUSH);
					return;
				}				
				
				if(ckbxPushMail.selected == true)
				{
					dataToPrint(ABO_PUSHMAIL);
					return;
				}
			}
			else
			{
				if(_arrayTemp != null)
				{
					for(var i:int = 0;i < _arrayTemp.length;i++)
					{
						if(_arrayTemp[i].THEME_LIBELLE == ABO_VOIX)
						{
							listProduit.addItem(_arrayTemp[i]);
							_listProduitVisible.addItem(_arrayTemp[i]);
						}
						if(_arrayTemp[i].THEME_LIBELLE == ABO_DATA)
						{
							listProduit.addItem(_arrayTemp[i]);
							_listProduitVisible.addItem(_arrayTemp[i]);
						}
						if(_arrayTemp[i].THEME_LIBELLE == ABO_PUSH)
						{
							listProduit.addItem(_arrayTemp[i]);
							_listProduitVisible.addItem(_arrayTemp[i]);
						}
						if(_arrayTemp[i].THEME_LIBELLE == ABO_PUSHMAIL)
						{
							listProduit.addItem(_arrayTemp[i]);
							_listProduitVisible.addItem(_arrayTemp[i]);
						}
					}
				}
			}
		}
		
		//AFFICHE LES OPTIONS SUIVANT LE FILTRE SELECTIONNE
		private function dataToPrint(aboType:String):void
		{
			for(var i:int = 0;i < _arrayTemp.length;i++)
			{
				if(_arrayTemp[i].THEME_LIBELLE == aboType)
				{
					listProduit.addItem(_arrayTemp[i]);
					_listProduitVisible.addItem(_arrayTemp[i]);
				}
			}
		}
		
		//AFFICHE LES OPTIONS SUIVANT LES 2 FILTRES SELECTIONNES
		private function addDataToPrint(aboType0:String,aboType1:String,aboType2:String = null):void
		{
			for(var i:int = 0;i < _arrayTemp.length;i++)
			{
				if(_arrayTemp[i].THEME_LIBELLE == aboType0)
				{
					listProduit.addItem(_arrayTemp[i]);
					_listProduitVisible.addItem(_arrayTemp[i]);
				}
				if(_arrayTemp[i].THEME_LIBELLE == aboType1)
				{
					listProduit.addItem(_arrayTemp[i]);
					_listProduitVisible.addItem(_arrayTemp[i]);
				}
				if(aboType2 != null)
				{
					if(_arrayTemp[i].THEME_LIBELLE == aboType2)
					{
						listProduit.addItem(_arrayTemp[i]);
						_listProduitVisible.addItem(_arrayTemp[i]);
					}
				}
			}
		}
		
		//SI 'ALL' EST SELECTIONNE ET QUE L'ON COCHE 'VOIX, 'DATA', 'PUSH MAIL' ALORS ON DESELECTIONNE 'ALL'
		private function checkSelectAll():void
		{
			if(ckbxSelectAll.selected)
			{
				ckbxSelectAll.selected = false;
			}
		}
		
		//ATTRIBUT LES FAVORIS OU TOUTES LES OPTIONS SUIVANT LA SELECTION DES LABELS 'Favoris' ou 'Tout'es
		private function checkIsFavorite():void
		{
			_arrayTemp = new ArrayCollection();
			if(_isFavorite)
			{
				_arrayTemp = _cmdSNCF.listeProduitsCatalogueFavoris;
			}
			else
			{
				_arrayTemp = _cmdSNCF.listeProduitsCatalogue;
			}
		}

		//RAFRAICHI LE DATAGRID
		private function refreshDatagrid():void
		{
//			dgListProduit.dataProvider = listProduit;
			listProduit.itemUpdated(null);
		}
		
		//APPEL DE PROCEDURE PERMETTANT D'OBTENIR TOUTES LES OPTIONS
		private function listerProduitHandler(e:Event):void
		{
			if(_cmd != null)
			{
				_cmdSNCF.fournirListeAbonnementsOptions(_cmd.IDGESTIONNAIRE,_cmd.IDOPERATEUR,_cmd.IDPROFIL_EQUIPEMENT,false);
			}
		}
		
		//AFFECTE A L'OPTION SELECTIONNEE 'true'
		private function thisIsOptionSelected(e:Event):void
		{
			if(dgListProduit.selectedItem != null)
			{
				dgListProduit.selectedItem.SELECTED = true;
			}
			listProduit.itemUpdated(dgListProduit.selectedItem);
		}
		
		//AFFECTE A L'OPTION SELECTIONNEE 'false'
		private function thisIsNotOptionSelected(e:Event):void
		{
			if(dgListProduit.selectedItem != null)
			{
				dgListProduit.selectedItem.SELECTED = false;
			}
			listProduit.itemUpdated(dgListProduit.selectedItem);
		}
		
		//APPEL DE PROCEDURE PERMETTANT L'ENVOI D'UNE OPTIONS SELECTIONNE EN TANT QUE 'FAVORI'
		//APPEL DE PROCEDURE PERMETTANT DE LISTER TOUTES LES OPTIONS
		private function thisIsAFavoriteSelected(e:Event):void
		{
			_refListProduit = new ArrayCollection();
			if(dgListProduit.selectedItem.FAVORI == 1)
			{
				_refListProduit = dgListProduit.dataProvider as ArrayCollection;
				_temp.ajouterProduitAMesFavoris(dgListProduit.selectedItem);
				_cmdSNCF.fournirListeAbonnementsOptions(_cmd.IDGESTIONNAIRE,_cmd.IDOPERATEUR,_cmd.IDPROFIL_EQUIPEMENT,false);
			}
		}
		
		//APPEL DE PROCEDURE PERMETTANT L'ENVOI D'UNE OPTIONS SELECTIONNE EN TANT QUE 'NON FAVORI'
		//APPEL DE PROCEDURE PERMETTANT DE LISTER TOUTES LES OPTIONS
		private function thisIsNotFavoriteSelected(e:Event):void
		{
			_refListProduit = new ArrayCollection();
			if(dgListProduit.selectedItem.FAVORI == 0)
			{
				_refListProduit = dgListProduit.dataProvider as ArrayCollection;
				_temp.supprimerProduitDeMesFavoris(dgListProduit.selectedItem);
				_cmdSNCF.fournirListeAbonnementsOptions(_cmd.IDGESTIONNAIRE,_cmd.IDOPERATEUR,_cmd.IDPROFIL_EQUIPEMENT,false);
			}
		}

		private function checkIfThisSelected():void
		{
			if(_itemsSelected.OPTIONS.length > 0)
			{
				for(var i:int = 0;i < _cmdSNCF.listeProduitsCatalogue.length;i++)
				{
					for(var j:int = 0;j < _itemsSelected.OPTIONS.length;j++)
					{
						if( _cmdSNCF.listeProduitsCatalogue[i].IDPRODUIT_CATALOGUE.toString().toLowerCase().indexOf(_itemsSelected.OPTIONS[j].IDPRODUIT_CATALOGUE.toString().toLowerCase()) != -1)
						{ 
							_cmdSNCF.listeProduitsCatalogue[i].SELECTED = true;
						}
					}
				}
				
				for(var k:int = 0;k < _cmdSNCF.listeProduitsCatalogueFavoris.length;k++)
				{
					for(var l:int = 0;l < _itemsSelected.OPTIONS.length;l++)
					{
						if( _cmdSNCF.listeProduitsCatalogueFavoris[k].IDPRODUIT_CATALOGUE.toString().toLowerCase().indexOf(_itemsSelected.OPTIONS[l].IDPRODUIT_CATALOGUE.toString().toLowerCase()) != -1)
						{ 
							_cmdSNCF.listeProduitsCatalogueFavoris[k].SELECTED = true;
						}
					}
				}
			}
		}

		//ECOUTEUR PERMETTANT D'ATTENDRE LE ETOUR DE LA PROCEDURE DE LA RECUPERATION DE TOUTES LES OPTIONS
		private function listnerTempEvent(e:Event):void
		{
			if(_cmdSNCF.listeProduitsCatalogueFavoris.length == 0)
			{
				lblToutClickHandler();
			}
			checkIsFavorite();
			checkIfThisSelected();
			recheckLastSelected();
			whatIsChbxSelected();
		}
		
		private function recheckLastSelected():void
		{
			for(var i:int = 0;i < _refListProduit.length;i++)
			{
				if(_refListProduit[i].SELECTED)
				{
					for(var j:int = 0;j < _cmdSNCF.listeProduitsCatalogue.length;j++)
					{
						if(_refListProduit[i].IDPRODUIT_CATALOGUE.toString().toLowerCase().indexOf(_cmdSNCF.listeProduitsCatalogue[j].IDPRODUIT_CATALOGUE.toString().toLowerCase()) != -1)
						{ 
							_cmdSNCF.listeProduitsCatalogue[j].SELECTED = true;
						}
					}
				}
			}
		}

//METHODES PRIVATE-----------------------------------------------------------------------
		
	}
}