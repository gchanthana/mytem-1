package commandemobile.popup
{
	import commandemobile.entity.Commande;
	import commandemobile.entity.ItemSelected;
	import commandemobile.temp.commandeSNCF;
	import commandemobile.temp.temp;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpChoiceSubscriptionImpl extends TitleWindow
	{

//VARIABLES------------------------------------------------------------------------------
		
		//COMPOSANTS
		public var ckbxSelectAll		:CheckBox;
		public var ckbxVoix				:CheckBox;
		public var ckbxData				:CheckBox;
		public var ckbxPushMail			:CheckBox;	
		public var dgListProduit		:DataGrid;
		public var txtptSearch			:TextInput;
		public var lblFavoris			:Label;
		public var lblFavorisUnder		:Label;
		public var lblTout				:Label;
		public var lblToutUnder			:Label;
		
		//CREATION DES OBJETS UTILS
		private var _cmdSNCF			:commandeSNCF 	= new commandeSNCF();
		public var _temp				:temp = new temp();
		public var _itemsSelected		:ItemSelected;
		public var _cmd					:Commande;
		
		//VARIABLES GLOBALES
		private var _refListProduitTemp	:ArrayCollection = new ArrayCollection();
		private var _refListProduit		:ArrayCollection = new ArrayCollection();
		private var _listProduit		:ArrayCollection = new ArrayCollection();
		private var _listProduitFavoris	:ArrayCollection = new ArrayCollection();
		private var _listProduitVisible	:ArrayCollection = new ArrayCollection();
		private var _arrayTemp			:ArrayCollection = new ArrayCollection();
		private var _isFavorite			:Boolean = true;	

		//TOTAL DE LA COMMANDE
		public var total				:Number = 0;
		public var totalString			:String = "0.00";
		
		//CONSTANTES
		private const ABO_VOIX			:String = "Abo Voix";
		private const ABO_DATA			:String = "Abo Data";
		
//VARIABLES------------------------------------------------------------------------------

//PROPRIETEES PUBLIC---------------------------------------------------------------------		
		
		public function get listProduit(): ArrayCollection
	    {
	    	return _listProduit;
	    }
	    
	    public function set listProduit(value:ArrayCollection): void
	    {
	    	_listProduit = value;
	    }
	    
	    public function get listProduitFavoris(): ArrayCollection
	    {
	    	return _listProduitFavoris;
	    }
	    
	    public function set listProduitFavoris(value:ArrayCollection): void
	    {
	    	_listProduitFavoris = value;
	    }


//PROPRIETEES PUBLIC---------------------------------------------------------------------

//METHODE PUBLIC-------------------------------------------------------------------------

		public function PopUpChoiceSubscriptionImpl()
		{
			_cmdSNCF.addEventListener("listeAbonnementsOptions", listnerTempEvent);
			addEventListener("listerProduit", listerProduitHandler);
			addEventListener("thisIsFavorite", thisIsAFavoriteSelected);
			addEventListener("thisIsNotFavorite", thisIsNotFavoriteSelected);
			addEventListener("aboIsSelected", thisIsAboSelected);
		}

//METHODE PUBLIC-------------------------------------------------------------------------

//METHODE PROTECTED----------------------------------------------------------------------
		
		protected function _closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function dgListProduitClickHandler():void
		{
			if(dgListProduit.selectedItem != null)
			{
				attributSelected();
			}
		}

		protected function txtptSearchHandler():void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			listProduit = new ArrayCollection();
			if(txtptSearch.text == "")
			{
				listProduit = _listProduitVisible;
			}
			else
			{
				for(var i:int = 0;i < _listProduitVisible.length;i++)
				{
					if(_listProduitVisible[i].LIBELLE_PRODUIT.toString().toLowerCase().indexOf(txtptSearch.text.toLowerCase()) != -1)
					{ 
						arrayC.addItem(_listProduitVisible[i]); 
					}
				}
				listProduit = arrayC;
			}
		}

		protected function btReinitialisationClickHandler():void
		{
			txtptSearch.text = "";
			listProduit = new ArrayCollection();
			_listProduitVisible = new ArrayCollection();
			_cmdSNCF.fournirListeAbonnementsOptions(_cmd.IDGESTIONNAIRE,_cmd.IDOPERATEUR,_cmd.IDPROFIL_EQUIPEMENT,true);
			whatIsChbxSelected();
		}
		
		protected function ckbxSelectAllChangeHandler():void
		{
			ckbxData.selected = ckbxVoix.selected = ckbxSelectAll.selected;
			whatIsChbxSelected();
		}
		
		protected function ckbxVoixChangeHandler():void
		{
			checkSelectAll();
			whatIsChbxSelected();
		}
		
		protected function ckbxDataChangeHandler():void
		{
			checkSelectAll();
			whatIsChbxSelected();
		}
		
		protected function ckbxPushMailChangeHandler():void
		{
			checkSelectAll();
			whatIsChbxSelected();
		}
		
		protected function lblFavorisClickHandler():void
		{
			_isFavorite = true;
			
			lblFavoris.visible = false;
			lblFavoris.width = 0;
			lblFavorisUnder.visible = true;
			lblFavorisUnder.width = 50;
			
			lblTout.visible = true;
			lblTout.width = 30;
			lblToutUnder.visible = false;
			lblToutUnder.width = 0;
			
			whatIsChbxSelected();
		}
		
		protected function lblToutClickHandler():void
		{
			_isFavorite = false;
			
			lblTout.visible = false;
			lblTout.width = 0;
			lblToutUnder.visible = true;
			lblToutUnder.width = 30;
			
			lblFavoris.visible = true;
			lblFavoris.width = 50;
			lblFavorisUnder.visible = false;
			lblFavorisUnder.width = 0;
			
			whatIsChbxSelected();
		}

		protected function btnValiderClickHandler():void
		{
			var ABO:ArrayCollection = new ArrayCollection();
			for(var i:int = 0;i < listProduit.length;i++)
			{
				if(listProduit[i].SELECTED)
				{ 
					ABO.addItem(listProduit[i]);
				}
			}
			if(ABO.length > 0)
			{
				_itemsSelected.ABO = new ArrayCollection();
				_itemsSelected.ABO = ABO;
				dispatchEvent(new Event("POPUP_SUBSCRIB_CLOSED_AND_VALIDATE"));
				PopUpManager.removePopUp(this);
			}
			else
			{
				Alert.show("Veuillez sélectionner un abonnement!","Consoview");
			}
		}

		protected function btnAnnulerClickHandler():void
		{
			dispatchEvent(new Event("POPUP_SUBSCRIB_CLOSED_AND_VALIDATE"));
			PopUpManager.removePopUp(this);
		}
		
//METHODE PROTECTED----------------------------------------------------------------------

//METHODE PRIVATE------------------------------------------------------------------------
		
		private function listnerTempEvent(e:Event):void//////////////////--------------------------------------------------------------------------
		{
			if(_cmdSNCF.listeProduitsCatalogueFavoris.length == 0)
			{
				lblToutClickHandler();
			}
			checkIfThisSelected();
			whatIsChbxSelected();
			_refListProduit = _cmdSNCF.listeProduitsCatalogue;
		}
		
		private function listerProduitHandler(e:Event):void
		{
			if(_cmd != null)
			{
				_cmdSNCF.fournirListeAbonnementsOptions(_cmd.IDGESTIONNAIRE,_cmd.IDOPERATEUR,_cmd.IDPROFIL_EQUIPEMENT,true);
			}
		}

		private function thisIsAFavoriteSelected(e:Event):void
		{
			if(dgListProduit.selectedItem != null)
			{
				_refListProduitTemp = new ArrayCollection();
				if(dgListProduit.selectedItem.FAVORI == 1)
				{
					_refListProduitTemp = dgListProduit.dataProvider as ArrayCollection;
					_temp.ajouterProduitAMesFavoris(dgListProduit.selectedItem);
					_cmdSNCF.fournirListeAbonnementsOptions(_cmd.IDGESTIONNAIRE,_cmd.IDOPERATEUR,_cmd.IDPROFIL_EQUIPEMENT,true);//PEUT ETRE A SUPPRIMER SI ON FAIT JSUTE UN ARRAY.REFRESH() AVOIR!!!
				}
				refreshDatagrid();
			}
		}
		
		private function thisIsNotFavoriteSelected(e:Event):void
		{
			if(dgListProduit.selectedItem != null)
			{
				_refListProduitTemp = new ArrayCollection();
				if(dgListProduit.selectedItem.FAVORI == 0)
				{
					_refListProduitTemp = dgListProduit.dataProvider as ArrayCollection;
					_temp.supprimerProduitDeMesFavoris(dgListProduit.selectedItem);
					_cmdSNCF.fournirListeAbonnementsOptions(_cmd.IDGESTIONNAIRE,_cmd.IDOPERATEUR,_cmd.IDPROFIL_EQUIPEMENT,true);//PEUT ETRE A SUPPRIMER SI ON FAIT JSUTE UN ARRAY.REFRESH() AVOIR!!!
				}
				refreshDatagrid();
			}
		}
		
		private function thisIsAboSelected(e:Event):void
		{			
			attributSelected();
		}
		
		private function attributSelected():void
		{
			if(dgListProduit.selectedItem != null)
			{
				for(var k:int = 0;k < _cmdSNCF.listeProduitsCatalogueFavoris.length;k++)
				{
					if( _cmdSNCF.listeProduitsCatalogueFavoris[k].IDPRODUIT_CATALOGUE.toString().toLowerCase().indexOf(dgListProduit.selectedItem.IDPRODUIT_CATALOGUE.toString().toLowerCase()) != -1)
					{ 
						_cmdSNCF.listeProduitsCatalogueFavoris[k].SELECTED = true;
					}
					else
					{
						_cmdSNCF.listeProduitsCatalogueFavoris[k].SELECTED = false;
					}
				}
				for(var i:int = 0;i < _cmdSNCF.listeProduitsCatalogue.length;i++)
				{
					if( _cmdSNCF.listeProduitsCatalogue[i].IDPRODUIT_CATALOGUE.toString().toLowerCase().indexOf(dgListProduit.selectedItem.IDPRODUIT_CATALOGUE.toString().toLowerCase()) != -1)
					{ 
						_cmdSNCF.listeProduitsCatalogue[i].SELECTED = true;
					}
					else
					{
						_cmdSNCF.listeProduitsCatalogue[i].SELECTED = false;
					}
				}
				(dgListProduit.dataProvider as ArrayCollection).itemUpdated(dgListProduit.selectedItem);
//				listProduit.itemUpdated(null);
			}
		}

		private function checkIfThisSelected():void
		{
			for(var i:int = 0;i < _cmdSNCF.listeProduitsCatalogue.length;i++)
			{
				for(var j:int = 0;j < _itemsSelected.ABO.length;j++)
				{
					if( _cmdSNCF.listeProduitsCatalogue[i].IDPRODUIT_CATALOGUE.toString().toLowerCase().indexOf(_itemsSelected.ABO[j].IDPRODUIT_CATALOGUE.toString().toLowerCase()) != -1)
					{ 
						_cmdSNCF.listeProduitsCatalogue[i].SELECTED = true;
					}
					else
					{
						_cmdSNCF.listeProduitsCatalogue[i].SELECTED = false;
					}
				}
			}
			
			for(var k:int = 0;k < _cmdSNCF.listeProduitsCatalogueFavoris.length;k++)
			{
				for(var l:int = 0;l < _itemsSelected.ABO.length;l++)
				{
					if( _cmdSNCF.listeProduitsCatalogueFavoris[k].IDPRODUIT_CATALOGUE.toString().toLowerCase().indexOf(_itemsSelected.ABO[l].IDPRODUIT_CATALOGUE.toString().toLowerCase()) != -1)
					{ 
						_cmdSNCF.listeProduitsCatalogueFavoris[k].SELECTED = true;
					}
					else
					{
						_cmdSNCF.listeProduitsCatalogueFavoris[k].SELECTED = false;
					}
				}
			}
		}

		private function whatIsChbxSelected():void
		{
			txtptSearch.text = "";
			listProduit = new ArrayCollection();
			_listProduitVisible = new ArrayCollection();
			checkIsFavorite();
			
			if(ckbxVoix.selected == true && ckbxData.selected == true)
			{
				ckbxSelectAll.selected = true;
			}

			if(!ckbxSelectAll.selected)
			{
				if(ckbxVoix.selected == true && ckbxData.selected == true)
				{
					addDataToPrint(ABO_VOIX,ABO_DATA);
					return;
				}

				if(ckbxVoix.selected == true)
				{
					dataToPrint(ABO_VOIX);
					return;
				}
				
				if(ckbxData.selected == true)
				{
					dataToPrint(ABO_DATA);
					return;
				}
			}
			else
			{
				if(_arrayTemp != null)
				{
					for(var i:int = 0;i < _arrayTemp.length;i++)
					{
						if(_arrayTemp[i].THEME_LIBELLE == ABO_VOIX)
						{
							listProduit.addItem(_arrayTemp[i]);
							_listProduitVisible.addItem(_arrayTemp[i]);
						}
						if(_arrayTemp[i].THEME_LIBELLE == ABO_DATA)
						{
							listProduit.addItem(_arrayTemp[i]);
							_listProduitVisible.addItem(_arrayTemp[i]);
						}
					}
				}
			}
			
		}
		
		private function dataToPrint(aboType:String):void
		{
			for(var i:int = 0;i < _arrayTemp.length;i++)
			{
				if(_arrayTemp[i].THEME_LIBELLE == aboType)
				{
					listProduit.addItem(_arrayTemp[i]);
					_listProduitVisible.addItem(_arrayTemp[i]);
				}
			}
		}
		
		private function addDataToPrint(aboType0:String,aboType1:String):void
		{
			for(var i:int = 0;i < _arrayTemp.length;i++)
			{
				if(_arrayTemp[i].THEME_LIBELLE == aboType0)
				{
					listProduit.addItem(_arrayTemp[i]);
					_listProduitVisible.addItem(_arrayTemp[i]);
				}
				if(_arrayTemp[i].THEME_LIBELLE == aboType1)
				{
					listProduit.addItem(_arrayTemp[i]);
					_listProduitVisible.addItem(_arrayTemp[i]);
				}
			}
		}
		
		private function checkSelectAll():void
		{
			if(ckbxSelectAll.selected)
			{
				ckbxSelectAll.selected = false;
			}
		}
		
		private function checkIsFavorite():void
		{
			_arrayTemp = new ArrayCollection();
			if(_isFavorite)
			{
				_arrayTemp = _cmdSNCF.listeProduitsCatalogueFavoris;
			}
			else
			{
				_arrayTemp = _cmdSNCF.listeProduitsCatalogue;
			}
		}
		
		private function refreshDatagrid():void
		{
			(dgListProduit.dataProvider as ArrayCollection).itemUpdated(null);
		}

//METHODE PRIVATE------------------------------------------------------------------------
		
	}
}