package commandemobile.popup
{
	import commandemobile.temp.temp;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.DataGrid;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpViewConfigurationElementsImpl extends TitleWindow
	{
		
//VARIABLES------------------------------------------------------------------------------
	
		//COMPOSANTS
		public var dgRecapCommande				:DataGrid;
		
		//OBJETS
		private var _temp						:temp = new temp();
		
		//VARIABLES GLOBALES
		public var idConfiguration				:int = 0;
				
		//VARIABLES LOCALES
		private var _articlesXML				:XML = new XML();
		public var listArticles					:ArrayCollection = new ArrayCollection();

//VARIABLES------------------------------------------------------------------------------

		protected function _closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		public function PopUpViewConfigurationElementsImpl()
		{
			addEventListener("listerConfiguration",listeConfigurationOkHandler);
			
			_temp.addEventListener("listeModeleOk",listeModeleOkHandler);
			_temp.addEventListener("eraseModeleOk",eraseModeleOkHandler);
		}
		
		protected function btnFermerClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnSuppConfigClickHandler():void
		{
			_temp.deleteModeleCommande(idConfiguration);
		}
		
		private function eraseModeleOkHandler(e:Event):void
		{
			dispatchEvent(new Event("refreshDatagrid"));
			ConsoviewAlert.afficherOKImage("Modèle supprimmer !");
			btnFermerClickHandler();
		}
		
		private function listeConfigurationOkHandler(e:Event):void
		{
			_temp.getModeleCommande(idConfiguration);
		}
		
		private function listeModeleOkHandler(e:Event):void
		{
			_articlesXML = new XML();
			_articlesXML = _temp.listeModele;
			formatXMLToArrayCollection();
		}
		
		private function formatXMLToArrayCollection():void
		{
			listArticles = new ArrayCollection();
			var xmlList:XMLList = _articlesXML.children();
			var nbre:int = xmlList.length();
			if(nbre > 1)
			{
				formatXMLList(xmlList);
			}
			else
			{
				formatXML(xmlList[0]);
			}
		}
		
		private function formatXMLList(xmlList:XMLList):void
		{	
			for(var i:int = 0;i < xmlList.length();i++)
			{
				formatXML(xmlList[i]);
			}
		}
		
		private function giveMeDate(strg:String):String
		{
			var day:String 		= strg.substr(8,2);
			var month:String	= strg.substr(5,2);
			var year:String		= strg.substr(0,4);
			return day + "/" + month + "/" + year;
		}
		
		private function formatXML(xml:XML):void
		{
			if(xml == null)
			{
				listArticles = null;
				return;
			}
			var object			:Object = new Object();
			var xmlListEquip	:XMLList = 	xml.equipements.children();
			var xmlListRess		:XMLList =	xml.ressources.children();
			var arrayEquip		:ArrayCollection = new ArrayCollection();
			var arrayRess		:ArrayCollection = new ArrayCollection();
			var items			:ArrayCollection = new ArrayCollection();
			var arrayResult		:ArrayCollection = new ArrayCollection();
			 	
			
			if(xml.fpc[0] != "")
			{
				object.ENGAGEMENT_RESSOURECES 	= giveMeDate(String(xml.fpc[0]));
			}
			else
			{
				object.ENGAGEMENT_RESSOURECES   = Number(xml.engagement_res[0]);
				object.ENGAGEMENT_EQUIPEMENTS 	= Number(xml.engagement_eq[0]);
			}
			
			object.SOUSTETE 				= String(xml.soustete[0]);
			object.IDSOUSTETE 				= Number(xml.idsoustete[0]);
			
			arrayEquip 	= addEquipements(xmlListEquip);
			arrayRess 	= addRessources(xmlListRess);
			items = addToArrayCollection(arrayEquip,arrayRess);
			
			listArticles = addToObject(items,object);
		}
		
		private function addEquipements(xmlList:XMLList):ArrayCollection
		{
			var object:Object = new Object();
			var arrayCollection:ArrayCollection = new ArrayCollection();
			for(var i:int = 0; i < xmlList.length();i++)
			{
				var strg:String = xmlList[i].idtype_equipement.toString();
				if( strg != "undefined")
				{ 
					var thisPrice:String = xmlList[i].prix.toString();
					thisPrice = thisPrice.replace(",",".");
					object = new Object();
					object.TYPE_EQUIPEMENT 	= String(xmlList[i].type_equipement);
					object.IDTYPE_EQUIPEMENT= String(xmlList[i].idtype_equipement);
					object.LIBELLE_PRODUIT 	= String(xmlList[i].libelle);
					object.PRIX_UNIT 		= Number(thisPrice);
					object.TOTAL			= Number(thisPrice);
					arrayCollection.addItem(object);
				}
			}
			return arrayCollection;
		}
		
		private function addRessources(xmlList:XMLList):ArrayCollection
		{
			var object:Object = new Object();
			var arrayCollection:ArrayCollection = new ArrayCollection();
			for(var i:int = 0; i < xmlList.length();i++)
			{
				if(String(xmlList[i].libelle) != "")
				{
					object = new Object();
					object.TYPE_EQUIPEMENT 	= String(xmlList[i].theme);
					object.LIBELLE_PRODUIT 	= String(xmlList[i].libelle);
					object.PRIX_UNIT 		= " - ";
					object.TOTAL 			= " - ";
					arrayCollection.addItem(object);
				}
			}
			return arrayCollection;
		}
				
		private function addToArrayCollection(equipements:ArrayCollection,ressources:ArrayCollection):ArrayCollection
		{
			var arrayCollection:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0; i < equipements.length;i++)
			{
				arrayCollection.addItem(equipements[i]);
			}
			for(var j:int = 0; j < ressources.length;j++)
			{
				arrayCollection.addItem(ressources[j]);
			}
			return arrayCollection;
		}
		
		private function addToObject(allItems:Object,info:Object):ArrayCollection
		{
			var arrayCollection:ArrayCollection = new ArrayCollection();
			var object:Object = new Object();
			
			object.ITEMS 			= allItems;
			object.COLLABORATEUR 	= info.NOMEMPLOYE;
			object.ENGAGEMENT 		= info.ENGAGEMENT_RESSOURECES;
			object.SOUSTETE 		= info.SOUSTETE;
			object.IDSOUSTETE		= info.IDSOUSTETE;
			object.PRIX 			= calculPrice(allItems as ArrayCollection);
			arrayCollection.addItem(object);
			return arrayCollection;
		}
		
		private function calculPrice(allItems:ArrayCollection):String
		{
			var total:Number = 0;
			for(var i:int = 0; i < allItems.length;i++)
			{
				if(allItems[i].PRIX_UNIT != " - ")
				{
					total = total + Number(allItems[i].PRIX_UNIT);
				}
			}
			return total.toString();
		}

	}
}