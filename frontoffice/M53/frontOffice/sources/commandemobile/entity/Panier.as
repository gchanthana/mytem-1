package commandemobile.entity
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class Panier extends EventDispatcher
	{

//VARIABLES------------------------------------------------------------------------------
		
		private var _NOMBRE_CONFIGURATION:		int = 0;
		private var _MONTANT_CONFIGURATION_PREC:String = "0.00";
		private var _MONTANT_COMMANDE_ENCOURS:	String = "0.00";
		private var _MONTANT_TOTAL_COMMANDE:	String = "0.00";

		private var _PRIX_EQUIPEMENTS:			Number = 0.00;
		private var _PRIX_ACCESSOIRES:			Number = 0.00;
		private var _QUANTITE:					Number = 0;

		private var isReturn: 					Boolean = false;
		
//		private var _itemsSelected:ItemSelected;
		private var _cmd:Commande;
	
//VARIABLES------------------------------------------------------------------------------
		
//PROPRIETEES PUBLIC---------------------------------------------------------------------
		
		public function get NOMBRE_CONFIGURATION():int
		{
			return _NOMBRE_CONFIGURATION;
		}
			
		public function set NOMBRE_CONFIGURATION(values:int):void
		{
			_NOMBRE_CONFIGURATION = values;
		}
		
		public function get MONTANT_CONFIGURATION_PREC():String
		{
			return _MONTANT_CONFIGURATION_PREC;
		}
			
		public function set MONTANT_CONFIGURATION_PREC(values:String):void
		{
			 _MONTANT_CONFIGURATION_PREC = values;
		}
		
		public function get MONTANT_COMMANDE_ENCOURS():String
		{
			return _MONTANT_COMMANDE_ENCOURS;
		}
			
		public function set MONTANT_COMMANDE_ENCOURS(values:String):void
		{
			_MONTANT_COMMANDE_ENCOURS = values;
		}
		
		public function get MONTANT_TOTAL_COMMANDE():String
		{
			return _MONTANT_TOTAL_COMMANDE;
		}
			
		public function set MONTANT_TOTAL_COMMANDE(values:String):void
		{
			_MONTANT_TOTAL_COMMANDE = values;
		}

//PROPRIETEES PUBLIC---------------------------------------------------------------------

//METHODE PUBLIC-------------------------------------------------------------------------
		
		public function Panier(/*items:ItemSelected,*/cmd:Commande)
		{
//			_itemsSelected	= items;
			_cmd 			= cmd;
			addEventListener("montantCourantChange",		calulMontantEnCours);
			addEventListener("montantTotalChange",			calculMontantTotal);
			addEventListener("nombreConfigurationChanged",	nombreConfigurationChanged);
			addEventListener("montantLastConfig",			montantLastConfigHandler);
		}

		public function attributMontant(itemSelected:ItemSelected):void
		{
			_PRIX_EQUIPEMENTS 		= calculPrice(itemSelected.TERMINAUX,	true,itemSelected);
			_PRIX_ACCESSOIRES 		= calculPrice(itemSelected.ACCESSOIRES,	false,itemSelected);
			
			dispatchEvent(new Event("montantLastConfig"));
			dispatchEvent(new Event("montantCourantChange"));
		}
			
//METHODE PUBLIC-------------------------------------------------------------------------
		
//METHODE PRIVATE------------------------------------------------------------------------
		
		private function calculPrice(object:ArrayCollection,terminaux:Boolean,itmSelected:ItemSelected):Number
		{
			var number:Number = 0;
			if(terminaux)
			{
				for(var i:int = 0; i < object.length;i++)
				{
					if(object[i] != null)
					{
						var tempMontant:Number = 0;
						switch(itmSelected.EMPLOYE_CONFIGURATION.ENGAGEMENT.CODE)
						{
							case "EDM": 	tempMontant =  Number(object[i].PRIX_2);//12
											break;
							case "EVQM":	tempMontant =  Number(object[i].PRIX_3);//24
											break;
							case "ETSM": 	tempMontant =  Number(object[i].PRIX_4);//36
											break;
							case "EQHM": 	tempMontant =  Number(object[i].PRIX_5);//48
											break;
							case "EDMAR": 	tempMontant =  Number(object[i].PRIX_6);//12 ren
											break;
							case "EVQMAR": 	tempMontant =  Number(object[i].PRIX_7);//24 ren
											break;
							case "ETSMAR": 	tempMontant =  Number(object[i].PRIX_8);//36 ren
											break;
							case "EQHMAR": 	tempMontant =  Number(object[i].PRIX_9);//48 ren
											break;
							case "EVQMFCP":	tempMontant =  Number(object[i].PRIX_3);//fpc
											break;
							default :		tempMontant =  Number(object[i].PRIX_CATALOGUE);//equip nu
											break;
						}
						if(tempMontant.toString() == "NaN")
						{
							tempMontant = 0;
						}
					}
					number = number + tempMontant;
				}
			}
			else
			{
				for(var j:int = 0; j < object.length;j++)
				{
					number = number + Number(object[j].PRIX_UNIT);
				}
			}
			
			return number;
		}
		
		private function calculTotalPrice():Number
		{
			var terminaux	:Number = 0;
			var accessoires	:Number = 0;
			var abonnements	:Number = 0;
			var options		:Number = 0;
			var total		:Number = 0;
			var number		:Number = 0;
			var itm:ItemSelected;
			
			for(var i:int = 0; i < _cmd.COMMANDE.length;i++)
			{
				itm = new ItemSelected();
				itm.EMPLOYE_CONFIGURATION.ENGAGEMENT.CODE = _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.ENGAGEMENT.CODE;
				terminaux 	= calculPrice(_cmd.COMMANDE[i].TERMINAUX	,true	,itm);
				accessoires = calculPrice(_cmd.COMMANDE[i].ACCESSOIRES	,false	,itm);
				
				total = terminaux + accessoires;	
				number = number + total;
			}
			return number;
		}

//----------------------EVENT----------------------------------------------------
		
		private function calulMontantEnCours(e:Event):void
		{
			MONTANT_COMMANDE_ENCOURS = "";
			MONTANT_COMMANDE_ENCOURS = formatTotal(_PRIX_EQUIPEMENTS + _PRIX_ACCESSOIRES);
			if(_cmd.COMMANDE_TEMPORAIRE.length > 0)
			{
				MONTANT_COMMANDE_ENCOURS = formatTotal( Number(MONTANT_COMMANDE_ENCOURS) * Number(_cmd.COMMANDE_TEMPORAIRE.length));
			}
			dispatchEvent(new Event("montantTotalChange"));
		}

		private function nombreConfigurationChanged(e:Event):void
		{	
			var nbrConfiguration:int = 0;
			var numeroConfiguration:int = 0;
			for(var i:int = 0;i < _cmd.COMMANDE.length;i++)
			{
				if(i == 0)
				{
					numeroConfiguration = _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE;
					nbrConfiguration++;
				}
				else
				{
					if(numeroConfiguration != _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE)
					{
						numeroConfiguration = _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE;
						nbrConfiguration++;
					}
				}
			}

			NOMBRE_CONFIGURATION = nbrConfiguration;
			dispatchEvent(new Event("montantTotalChange"));
		}
		
		private function calculNombreConfigurations():int
		{	
			var nbrConfiguration:int = 0;
			var numeroConfiguration:int = 0;
			for(var i:int = 0;i < _cmd.COMMANDE.length;i++)
			{
				if(i == 0)
				{
					numeroConfiguration = _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE;
					nbrConfiguration++;
				}
				else
				{
					if(numeroConfiguration != _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE)
					{
						numeroConfiguration = _cmd.COMMANDE[i].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE;
						nbrConfiguration++;
					}
				}
			}

			return nbrConfiguration;
		}

		private function calculMontantTotal(e:Event):void
		{
			NOMBRE_CONFIGURATION 	= calculNombreConfigurations();
			MONTANT_TOTAL_COMMANDE = formatTotal(calculTotalPrice());		
		}
		
		private function montantLastConfigHandler(e:Event):void
		{
			NOMBRE_CONFIGURATION 	= calculNombreConfigurations();
			MONTANT_CONFIGURATION_PREC = formatTotal(calculTotalPrice());	
		}
		
		private function formatTotal(totalNbr:Number):String
		{
			var strg0:String = "0.00";
			if(totalNbr > 0.0000001)
			{
				var strg:String = totalNbr.toString();
				var strg1:String = "";
				var strg2:String = "";
				var strg3:String = "";
				var strg4:String = "";
				var strg5:String = "";
				var nbr:Number = -1;
				var len:Number = -1;
				var price:Number = 0;
				
				len = strg.length;	
				nbr = strg.lastIndexOf(".",0);
				
				if(totalNbr.toString().toLowerCase().indexOf(".") == -1)
				{
					strg0 = strg + ".00";
				}
				else
				{
					nbr = totalNbr.toString().toLowerCase().lastIndexOf(".");
					strg1 = strg.substring(0,nbr);
					strg3 = strg.substring(nbr+1,len);
					if(strg3.length > 2)
					{
						strg4 = strg.substring(nbr+3,nbr+4);
						if(Number(strg4) > 4)
						{
							strg5 = strg.substring(nbr+1,nbr+3);
							strg2 = (Number(strg5)+1).toString();
						}
						else
						{
							strg2 = strg.substring(nbr+1,nbr+3);
						}
					}
					else
					{
						strg2 = strg.substring(nbr+1,nbr + 3);
						if(strg2.length < 2)
						{
							strg2 = strg2 + "0";
						}
					}
					strg0 = strg1 + "." + strg2;
				}
			}
			return strg0;
		}

//----------------------EVENT----------------------------------------------------

//METHODE PRIVATE------------------------------------------------------------------------

	}
}