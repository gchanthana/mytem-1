package commandemobile.entity
{
	[Bindable]
	public class Configuration
	{
		
		public var TYPE						:int = 1;
		public var LIBELLE_CONFIGURATION	:String = "";
		public var NUMEROCONFIG_UNIQUE		:int = 0;
		public var CONFIGURATION_NAME		:String = "";
		
		public var NOMBRE_CONFIGURATION		:int = 1;
		public var ENGAGEMENT				:Engagement = new Engagement();
		
		
		public var FPC						:String	= "";
		
		public var ENABLEPORTABILITE		:Boolean = false;
		public var COLLABORATEURSELECTED	:Boolean = false;
		public var LIBELLESELECTED			:Boolean = true;
		public var CHECKBOXSELECTED			:Boolean = false;
		
		
		public var COLLABORATEUR			:String = "Non affecté";
		public var EMAIL					:String = "";
		public var NOM						:String = "";
		
		public var IDCOLLABORATEUR			:int = 0;
		public var CONFIG_ID				:int = 0;
		public var IDEMPLOYE				:int = 0;
		public var IDGROUPE_CLIENT			:int = 0;
		public var CLE_IDENTIFIANT			:int = 0;
		public var MATRICULE				:int = 0;

		public var INOUT					:int = 0;
		
		public var SELECTED					:Boolean = false;
		public var PORTABILITE				:Boolean = false;
		public var ERROR					:Boolean = false;
		
		public var IDPOOL_GESTIONNAIRE		:Number = 0;
		public var NUM_LINE					:String = "";
		public var NUM_RIO					:String	= "";
		public var DATE_PORTABILITE			:Date 	= null;		
		
		public function Configuration()
		{
		}

	}
}