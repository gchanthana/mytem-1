package commandemobile.entity
{
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class ItemSelected
	{

//VARIABLES GLOBALES---------------------------------------------------------------------

		public var TERMINAUX				:ArrayCollection 	= new ArrayCollection();
		public var ACCESSOIRES				:ArrayCollection	= new ArrayCollection();
		public var ABO						:ArrayCollection 	= new ArrayCollection();
		public var OPTIONS					:ArrayCollection 	= new ArrayCollection();
		public var COLLABORATEUR			:Object 			= new Object();
		public var EMPLOYE_CONFIGURATION	:Configuration 		= new Configuration();

//VARIABLES GLOBALES---------------------------------------------------------------------		
		
//CONSTRUCTEUR---------------------------------------------------------------------------

		public function ItemSelected()
		{
			
		}
		
//CONSTRUCTEUR---------------------------------------------------------------------------

	}
}