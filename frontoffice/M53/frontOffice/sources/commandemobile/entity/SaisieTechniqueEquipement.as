package commandemobile.entity
{
	public class SaisieTechniqueEquipement
	{
		public var TYPE_EQUIPEMENT			:String = "";
		public var NAME_EQUIPEMENT			:String = "";
		public var IDEQUIPEMENT_MOB			:Number = 0;
		public var IDEQUIPEMENT_SIM			:Number = 0;
		public var IMEI						:String = "";
		public var LIGNE					:String = "";
		public var IDLIGNE					:Number = 0;
		public var NUM_CARTESIM				:String = "";
		public var PIN						:String = "";
		public var PIN2						:String = "";
		public var PUK						:String = "";
		public var PUK2						:String = "";
		public var SOUSTETE					:String = "";
		public var IDSOUSTETE				:Number = 0;
		public var CHEKED					:Boolean = false;
		public var BOOL_LIGNE				:Boolean = false;
		public var BOOL_TERMINAL			:Boolean = false;
		public var BOOL_CARTESIM			:Boolean = false;

		
		public function SaisieTechniqueEquipement()
		{
		}

	}
}