package commandemobile.system
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class OperationVO extends EventDispatcher
	{
		public static const REFRESH_COMPPLETE : String = "refreshComplete";
		public static const UPDATE_COMPLETE : String = "updateComplete";
		
		public static const FORMAT_PDF : String = "PDF";
		
		public var IDGROUPE_CLIENT : Number;
		public var DATE_CREATION : Date; 
		public var IDINV_OPERATIONS : Number;
		public var COMMENTAIRE : String;
		public var DATE_REF_CALCUL : String; 
		public var CONTACT : String = "-";
		public var EMAIL_CONACT : String = "-";
		public var EN_COURS : Number;
		public var IDCDE_CONTACT : Number; 
		public var USERID : Number;
		public var LIBELLE_OPERATIONS : String = "-";
		public var DATE_CLOTURE : Date;
		public var DATE_MODIF : Date;
		public var IDINV_TYPE_OPE : Number;
		public var IDOPERATIONS_MAITRE : Number;
		public var RAISON_SOCIALE : String = "-";
		public var NUMERO_OPERATION : String = "-";
		public var REFERENCE_INTERNE : String = "-";
		public var IDCDE_CONTACT_SOCIETE : Number;
		public var REF_OPERATEUR : String = "-";
		
		public var DATE_ENVOI_MAIL : Date;
		public var FLAG_MAIL : Boolean = false;
		
		public function OperationVO(id : Number){
			IDINV_OPERATIONS = id;
			refresh();
		}
				
		public function refresh():void{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																					"getDetailOperation",
																					refreshResultHandler);				
			RemoteObjectUtil.callService(op,IDINV_OPERATIONS);		
		}
		
		public function updateContact():void{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																					"updateContact",
																					updateResultHandler);
																						
			RemoteObjectUtil.callService(op,IDINV_OPERATIONS,IDCDE_CONTACT,IDCDE_CONTACT_SOCIETE);
		}
		
		private function refreshResultHandler(re : ResultEvent):void{
			if (re.result && (re.result as ArrayCollection).length > 0){
				IDGROUPE_CLIENT = re.result[0].IDGROUPE_CLIENT;
				DATE_CREATION = re.result[0].DATE_CREATION;
				IDINV_OPERATIONS = re.result[0].IDINV_OPERATIONS;
				COMMENTAIRE = (re.result[0].COMMENTAIRE != null)?re.result[0].COMMENTAIRE:"-";
				DATE_REF_CALCUL = re.result[0].DATE_REF_CALCUL;
				CONTACT = re.result[0].CONTACT;
				EN_COURS = re.result[0].EN_COURS;
				IDCDE_CONTACT = re.result[0].IDCDE_CONTACT ;
				USERID = re.result[0].USERID;
				LIBELLE_OPERATIONS = re.result[0].LIBELLE_OPERATIONS;
				DATE_CLOTURE = re.result[0].DATE_CLOTURE;
				DATE_MODIF = re.result[0].DATE_MODIF;
				IDINV_TYPE_OPE = re.result[0].IDINV_TYPE_OPE;
				IDOPERATIONS_MAITRE = re.result[0].IDOPERATIONS_MAITRE;
				RAISON_SOCIALE = (re.result[0].RAISON_SOCIALE != null)?re.result[0].RAISON_SOCIALE:"-";
				NUMERO_OPERATION = (re.result[0].NUMERO_OPERATION != null)? re.result[0].NUMERO_OPERATION :"-";
				REFERENCE_INTERNE = (re.result[0].REFERENCE_INTERNE != null)? re.result[0].REFERENCE_INTERNE :"-";
				REF_OPERATEUR = (re.result[0].REF_OPERATEUR != null)?re.result[0].REF_OPERATEUR:"-";
				IDCDE_CONTACT_SOCIETE = re.result[0].IDCDE_CONTACT_SOCIETE;
				DATE_ENVOI_MAIL = null;
				FLAG_MAIL = false;		
				dispatchEvent(new Event(OperationVO.REFRESH_COMPPLETE));
			}
		}
		
		private function updateResultHandler(event : ResultEvent):void{
			if (event.result > 0){
				dispatchEvent(new Event(OperationVO.UPDATE_COMPLETE));
			}
		}
		
		

	}
}