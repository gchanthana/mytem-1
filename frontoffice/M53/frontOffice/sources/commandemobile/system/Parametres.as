package commandemobile.system
{
	public class Parametres
	{
		
		private var _listePools:ArrayCollection;
		private var _listeConcatPools:ArrayCollection;	
		private var _listeAgences:ArrayCollection;
		private var _listeContacts:ArrayCollection;
		private var _listeMesRevendeurs:ArrayCollection;
		private var _listeOperateurs:ArrayCollection;
		private var _listeComptes:ArrayCollection;
		private var _listeSousComptes:ArrayCollection;
		private var _listeSites:ArrayCollection;

		private var _revendeur: Revendeur;
		private var _selectedPoolId:Number = 0;
		
		private var txtDefaultCbx:String = "Sélectionnez";
		
			
		public function get listePools():ArrayCollection
		{
			return _listePools;
		}
			
		public function set listePools(values:ArrayCollection):void
		{
			_listePools = values;
		}
		
		public function get listeConcatPools():ArrayCollection
		{
			return _listeConcatPools;
		}
			
		public function set listeConcatPools(values:ArrayCollection):void
		{
			_listeConcatPools = values;
		}
		
		public function get listeAgences(): ArrayCollection
	    {
	    	return _listeAgences;
	    }

	    public function set listeAgences(agences:ArrayCollection): void
	    {
	    	_listeAgences = agences;
	    }
		
		public function get listeContacts(): ArrayCollection
	    {
	    	return _listeContacts;
	    }	

	    public function set listeContacts(ar:ArrayCollection): void
	    {
	    	_listeContacts = ar;
	    }

		public function get listeMesRevendeurs(): ArrayCollection
	    {
	    	return _listeMesRevendeurs;
	    }

	    public function set listeMesRevendeurs(liste:ArrayCollection): void
	    {
	    	_listeMesRevendeurs = liste;
	    }

	   	public  function get listeOperateurs():ArrayCollection
	    {
	    	return _listeOperateurs
	    }
	
	    public  function set listeOperateurs(values:ArrayCollection): void
	    {
	    	_listeOperateurs = values
	    }
	    
	    public function get listeComptes(): ArrayCollection
	    {
	    	return _listeComptes;
	    }
	
	    public function set listeComptes(values:ArrayCollection): void
	    {
	    	_listeComptes = values
	    }
	    
	    public function get listeSousComptes(): ArrayCollection
	    {
	    	return _listeSousComptes;
	    }
	
	    public function set listeSousComptes(values:ArrayCollection): void
	    {
	    	_listeSousComptes = values;
	    }
	    
	    public function get listeSites():ArrayCollection
	    {
	    	return _listeSites;
	    }

	    public function set listeSites(values:ArrayCollection):void
	    {
	    	_listeSites = values;    	
	    }
		
		public function Parametres()
		{
		}
		
				public function fournirPoolsDuGestionnaire():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"fournirPoolsDuGestionnaire", 
																		fournirPoolsDuGestionnaireResultHandler);
			RemoteObjectUtil.callService(op);
		} 
		
		protected function fournirPoolsDuGestionnaireResultHandler(event:ResultEvent):void
		{
			listePools = event.result as ArrayCollection;
			listeConcatPools = new ArrayCollection();
			
			for(var i:int = 0;i < listePools.length;i++)
			{
				var strgTemp:String;
				
				strgTemp = listePools[i].LIBELLE_POOL;
				strgTemp = strgTemp + " - " + listePools[i].LIBELLE_PROFIL;
				
				var objectTemp:Object = new Object();
				objectTemp.LIBELLE_POOL = listePools[i].LIBELLE_POOL;
				objectTemp.LIBELLE_PROFIL = listePools[i].LIBELLE_PROFIL;
				objectTemp.LIBELLE_CONCAT = strgTemp;
				objectTemp.IDPOOL = listePools[i].IDPOOL;
				objectTemp.IDPROFIL = listePools[i].IDPROFIL;
				listeConcatPools.addItem(objectTemp);
			}
			
								var objTemp:Object = new Object();
								objTemp.LIBELLE_CONCAT = txtDefaultCbx;
								listeConcatPools.addItemAt(objTemp,0);
	    }
	    
	    public function fournirListeSiteLivraisonsPoolGestionnaire(idPool:Number): void
	    {
	    	_selectedPoolId = idPool;
	    	
	    	if(_selectedPoolId>0)
	    	{
	    		var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  "fr.consotel.consoview.inventaire.sites.SitesUtils",
	    																		  "fournirListeSitesLivraisonsPoolGestionnaire",
	    																		  fournirListeSiteLivraisonsPoolGestionnaireResultHandler);
	    		RemoteObjectUtil.callService(op,idPool);	
	    	}
	    }
	
	    protected function fournirListeSiteLivraisonsPoolGestionnaireResultHandler(event:ResultEvent):void
	    {
	    	listeSites = event.result as ArrayCollection;
	    	
	    					var objTemp:Object = new Object();
	    					objTemp.NOM_SITE = txtDefaultCbx;
	    					listeSites.addItemAt(objTemp,0);
	    }
	    
		
		public function rechecherMesRevendeurs(chaine:String): void
	    {
	    	var Idracine:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
	    	var chaine:String = chaine;
	    	var APP_LOGINID:int = CvAccessManager.getSession().USER.CLIENTACCESSID;
	    	var p_segment:int = 2;
	    	
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur",
																					"get_catalogue_revendeur", 
																					rechecherMesRevendeursResultHandler);
			RemoteObjectUtil.callService(opData, Idracine, chaine, APP_LOGINID, p_segment);
	    }
	    
	    private function rechecherMesRevendeursResultHandler(re:ResultEvent): void
	    {
	    	var source:ArrayCollection = re.result as ArrayCollection;  
           	var cursor:IViewCursor = source.createCursor();	 
           	
           	listeMesRevendeurs = new ArrayCollection();   	
            while (!cursor.afterLast)
            {
            	var currentObj:Object = cursor.current;
            	listeMesRevendeurs.addItem(currentObj);
            	cursor.moveNext();
          	}
          	
          					var objTemp:Object = new Object();
          					objTemp.LIBELLE = txtDefaultCbx;
          					listeMesRevendeurs.addItemAt(objTemp,0);
	    }
	    
	    public function rechercherAgencesRevendeur(chaine:String, revendeur:Revendeur): void
	    {
	    	var IDCDE_CONTACT_SOCIETE:int = revendeur.IDCDE_CONTACT_SOCIETE;
	    	var chaine:String = chaine;
	    	
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.revendeurs.FicheSociete",
																					"get_liste_agence", 
																					rechercherAgencesRevendeurResultHandler);
			RemoteObjectUtil.callService(opData, IDCDE_CONTACT_SOCIETE, chaine);
	    }
	    
	    private function rechercherAgencesRevendeurResultHandler(re:ResultEvent): void
	    {
	    	var source:ArrayCollection = re.result as ArrayCollection;
           	var cursor:IViewCursor = source.createCursor();
           	var i:Number = 0;
           	
           	listeAgences = new ArrayCollection();   
           		
            while (!cursor.afterLast)
            {
            	var currentObj:Object = cursor.current;
            	listeAgences.addItem(currentObj);
            	cursor.moveNext();
          	}
          	
          					var objTemp:Object = new Object();
          					objTemp.RAISON_SOCIALE = txtDefaultCbx;
          					listeAgences.addItemAt(objTemp,0);
	    }
	    
	    public function rechercherContactsRevendeur(chaine:String, revendeur:Revendeur): void
	    {
	    	var IDCDE_CONTACT_SOCIETE:int = revendeur.IDCDE_CONTACT_SOCIETE;
	    	var APP_LOGINID:int = CvAccessManager.getSession().USER.CLIENTACCESSID;
	    	var chaine:String = chaine;
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.revendeurs.Contact",
																					"lister_cde_contact", 
																					rechercherContactsRevendeurResultHandler);
			RemoteObjectUtil.callService(opData, IDCDE_CONTACT_SOCIETE, APP_LOGINID, chaine);
	    }

	    private function rechercherContactsRevendeurResultHandler(re:ResultEvent): void
	    {
	    	var source:ArrayCollection = re.result as ArrayCollection;
           	var cursor:IViewCursor = source.createCursor();
           	
           	listeContacts = new ArrayCollection();  
           	 	
            while (!cursor.afterLast)
            {
            	var currentObj:Object = cursor.current;
            	listeContacts.addItem(currentObj);
            	cursor.moveNext();
          	}
          	
          					var objTemp:Object = new Object();
          					objTemp.EMAIL = txtDefaultCbx;
          					listeContacts.addItemAt(objTemp,0);	
	    }

		public  function fournirListeOperateursSegment(segment:String): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  "fr.consotel.consoview.inventaire.operateurs.OperateursUtils",
	    																		  "fournirListeOperateursSegment",
	    																		  fournirListeOperateursSegmentResultHandler);
	    	RemoteObjectUtil.callService(op,segment);
	    }
	    
	    protected  function fournirListeOperateursSegmentResultHandler(event:ResultEvent): void
	    {	
	    	listeOperateurs = event.result as ArrayCollection;
	    	
	    					var objTemp:Object = new Object();
	    					objTemp.LIBELLE = txtDefaultCbx;
	    					listeOperateurs.addItemAt(objTemp,0);
	    }

		public function fournirListeComptes(idOperateur:Number): void
	    {
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			"fr.consotel.consoview.inventaire.comptes.ComptesUtils",
	    																			"fournirListeComptes",
	    																			fournirListeComptesResultHandler);
			RemoteObjectUtil.callService(op,idOperateur);
	
	    }

 		protected function fournirListeComptesResultHandler(event:ResultEvent): void
	    {
	    	listeComptes = event.result as ArrayCollection;

			var objTemp:Object = new Object();
			objTemp.COMPTE_FACTURATION = txtDefaultCbx;
			listeComptes.addItemAt(objTemp,0);
	    }

		public function fournirListeSousComptes(idCompte:Number): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			"fr.consotel.consoview.inventaire.comptes.ComptesUtils",
	    																			"fournirListeSousComptes",
	    																			fournirListeSousComptesResultHandler);
			RemoteObjectUtil.callService(op,idCompte);	
	    }
	    
	    protected function fournirListeSousComptesResultHandler(event:ResultEvent): void
	    {
	    	listeSousComptes = event.result as ArrayCollection;
	    	
	    					var objTemp:Object = new Object();
	    					objTemp.SOUS_COMPTE = txtDefaultCbx;
	    					listeSousComptes.addItemAt(objTemp,0);
	    }

	}
}