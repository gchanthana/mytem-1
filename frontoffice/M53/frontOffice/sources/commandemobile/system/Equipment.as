package commandemobile.system
{
	public class Equipment
	{
		
		private var _listeEquipementsRevendeur:ArrayCollection;
		private var _listeTypesEquipement:ArrayCollection;

		private var _revendeur: Revendeur;
		private var _selectedPoolId:Number = 0;
		
		private var txtDefaultCbx:String = "Sélectionnez";

		public function get listeTypesEquipement(): ArrayCollection
	    {
	    	return _listeTypesEquipement;
	    } 
	    
	    public function set listeTypesEquipement(value:ArrayCollection): void
	    {
	    	_listeTypesEquipement = value;
	    } 

		
		public function Equipment()
		{
		}
		
		public function fournirListeEquipementsCatalogueClient(idrevendeur:Number, clef:String, segment:String="MOBILE"): void
	    {
			listeEquiementsRevendeur = null;
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.inventaire.equipement.utils.EquipementsUtils",
								"fournirListeEquipementsCatalogueClient", 
								fournirListeEquipementsCatalogueResultHandler);
			RemoteObjectUtil.callService(opData,idrevendeur,clef,segment)
	    		
		}
	}
}