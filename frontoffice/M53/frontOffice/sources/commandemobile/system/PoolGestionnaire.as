package commandemobile.system
{
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class PoolGestionnaire
	{
		private var _IDPool : int;
		private var _libelle_pool : String;
		private var _codeInterne_pool : String;
		private var _commentaire_pool: String;
		private var _IDPoolRevendeur: int;
		private var _id_profil : int;
		private var _libelle_profil : String;
		
		[ArrayElementType("univers.parametres.Gestionnaire.classeMetier.Gestionnaire")]
		private var _col_gestionnaire : ArrayCollection = new ArrayCollection();
		
		[ArrayElementType("univers.parametres.Gestionnaire.classeMetier.Site")]
		private var _col_site : ArrayCollection = new ArrayCollection();

		public function PoolGestionnaire()
		{
		}
		public function add_gestionnaire(idGestionnaire : int):void
		{
			/*var obj_pool : PoolGestionnaire = new PoolGestionnaire();
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"add_gestionnaire_in_pool",
																		process_init_dg_gestionnaire_of_pool,erreur);
			RemoteObjectUtil.callService(op,IDPool);*/
		}
		public function delete_gestionnaire(idGestionnaire : int):void
		{
			
		}
		public function add_site(idSite : int):void
		{
			
		}
		public function delete_site(idGestionnaire : int):void
		{
			
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------GET------------------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
		public function get IDPoolRevendeur():int
		{	
			return _IDPoolRevendeur;
		}
		public function get id_profil():int
		{	
			return _id_profil;
		}
		public function get IDPool():int
		{	
			return _IDPool;
		}
		public function get libelle_pool():String
		{	
			return _libelle_pool;
		}
		public function get libelle_profil():String
		{	
			return _libelle_profil;
		}
		public function get codeInterne_pool():String
		{	
			return _codeInterne_pool;
		}
		public function get commentaire_pool():String
		{
			return _commentaire_pool;
		}
		public function get col_gestionnaire():ArrayCollection
		{
			return _col_gestionnaire;
		}
		public function get col_site():ArrayCollection
		{
			return _col_site;
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------SET------------------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
		public function set IDPool(id:int):void
		{	
			this._IDPool =id ;
		}
		public function set id_profil(id:int):void
		{	
			this._id_profil =id ;
		}
		public function set IDPoolRevendeur(IDPoolRevendeur:int):void
		{	
			this._IDPoolRevendeur =IDPoolRevendeur ;
		}
		public function set libelle_pool(libelle_pool:String):void
		{	
			this._libelle_pool = libelle_pool;
		}
		public function set libelle_profil(libelle_profil:String):void
		{	
			this._libelle_profil = libelle_profil;
		}
		public function set codeInterne_pool(codeInterne_pool:String):void
		{	
			this._codeInterne_pool = codeInterne_pool;
		}
		public function set commentaire_pool(commentaire_pool:String):void
		{
			this._commentaire_pool =commentaire_pool ;
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------méthodes public-------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
		public function init_dg_gestionnaire_of_pool():void
		{
			var obj_pool : PoolGestionnaire = new PoolGestionnaire();
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getGestionnaire_pool",
																		process_init_dg_gestionnaire_of_pool,erreur);
			RemoteObjectUtil.callService(op,IDPool);
		}
		private function process_init_dg_gestionnaire_of_pool(re : ResultEvent):void
		{
			col_gestionnaire.removeAll();
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			for(i=0; i < tmpArr.length;i++){	
							
				var  item : Gestionnaire = new Gestionnaire();
				item.id=tmpArr[i].APP_LOGINID;
				item.nom=tmpArr[i].LOGIN_NOM;
				item.prenom=tmpArr[i].LOGIN_PRENOM;
											
				col_gestionnaire.addItem(item);
			}
			//dg_gestionnaire_pool.dataProvider = (dg_pool_gestionnaire.selectedItem as PoolGestionnaire).col_gestionnaire;
		}
		public function update_pool_gestionnaire():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"updatePoolGestionnaire", 
																		process_update_pool_gestionnaire,erreur);
			
			RemoteObjectUtil.callService(op,_IDPool,_libelle_pool,_codeInterne_pool,_commentaire_pool,CvAccessManager.getSession().USER.CLIENTACCESSID,_IDPoolRevendeur);
		}
		public function add_pool_gestionnaire():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"addPoolGestionnaire", 
																		process_add_pool_gestionnaire,erreur);
			RemoteObjectUtil.callService(op,_libelle_pool,_codeInterne_pool,_commentaire_pool,CvAccessManager.getSession().USER.CLIENTACCESSID,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,_IDPoolRevendeur);
		}
		public function remove_pool_gestionnaire():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"addPoolGestionnaire", 
																		process_remove_pool_gestionnaire,erreur);
			RemoteObjectUtil.callService(op,_IDPool);
		}
		private function process_remove_pool_gestionnaire( re : ResultEvent):void
		{
			if(re.result != -1)
			{
				ConsoviewAlert.afficherOKImage("Supprimé!");
			}
		}
		private function process_add_pool_gestionnaire(re : ResultEvent):void
		{
			if(re.result != -1)
			{
				ConsoviewAlert.afficherOKImage("Ajouté!");
			}
		}
		private function process_update_pool_gestionnaire(re : ResultEvent):void
		{
			if(re.result != -1)
			{
				//Alert.show("Modifié");
			}
		}
		private function erreur(evt : FaultEvent):void
		{
			Alert.show("Erreur traitement pool de gestionnaire");
		}
	}
}