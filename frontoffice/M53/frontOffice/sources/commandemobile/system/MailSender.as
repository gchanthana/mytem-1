package commandemobile.system
{
	import commandemobile.popup.ActionMailBoxIHM;
	import commandemobile.popup.EnregistrerActionImpl;
	
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import mx.managers.PopUpManager;
	
	[Event(name="mailSent",type="flash.events.Event")]
	
	public class MailSender extends EnregistrerActionImpl
	{
		public static const MAIL_SENT:String = "mailSent";	
		
		private var _parentRef:DisplayObject;
		public function set parentRef(value:DisplayObject):void{
			_parentRef = value;
		}	
		
		private var _infosMail:InfosObject = new InfosObject();
		public function set infosMail(value:InfosObject):void
		{
			_infosMail  = value
		}
		public function get infosMail():InfosObject
		{
			return _infosMail	
		}
		
		private var _pUpMailBox : ActionMailBoxIHM;
						
		public function MailSender()
		{
			super();
		}
		
		public function afficherMailBox():void
		{	
			removePopUp();
			
			_pUpMailBox = new ActionMailBoxIHM();
			var objet:String = "Réf. : "  + infosMail.commande.REF_CLIENT;
			_pUpMailBox.initMail("Commande Mobile",objet,"");	
			_pUpMailBox.configRteMessage(infosMail,GabaritFactory.TYPE_COMMANDE_MOBILE);			
			_pUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE,_pUpMailBoxMailEnvoyeHandler);
			_pUpMailBox.selectedAction = selectedAction;
			PopUpManager.addPopUp(_pUpMailBox,_parentRef.parent,true);
			PopUpManager.centerPopUp(_pUpMailBox);
		}
		
		
		
		protected function _pUpMailBoxMailEnvoyeHandler(ev : Event):void
		{	
			selectedAction.COMMENTAIRE_ACTION = _pUpMailBox.txtCommentaire.text; //txtCommentaire.text;
	    	selectedAction.DATE_ACTION = new Date();//dcDateAction.selectedDate;
	    		    	
	    	var actionEvent:ActionEvent = new ActionEvent(ActionEvent.ACTION_CLICKED);
	    	actionEvent.action = selectedAction;
	    	dispatchEvent(actionEvent);	    	
	    	
			_pUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE,_pUpMailBoxMailEnvoyeHandler);
			PopUpManager.removePopUp(_pUpMailBox);
			_pUpMailBox = null;
		 
		}
	   
	}
}