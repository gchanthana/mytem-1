package commandemobile.system
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class Login extends EventDispatcher
	{
		//VARIABLES PRIVEES
		private const CFC_GestionCommande	:String = "fr.consotel.consoview.inventaire.commande.GestionCommande";
		
		//VARIABLES PUBLICS
		public var resultLogin				:int = -1;
		
		public function Login()
		{
		}
		
		public function checkLogin(log:String,pwd:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			CFC_GestionCommande,
	    																			"checkLogin",
	    																			checkLoginResultHandler);
	    																			
	    		RemoteObjectUtil.callService(op,log,pwd)	
		}
		
		private function checkLoginResultHandler(e:ResultEvent):void
		{
			resultLogin = e.result;
			if(resultLogin == 1)
			{
				dispatchEvent(new Event("checkLoginOk"));
			}
			else
			{
				dispatchEvent(new Event("checkLoginKo"));
			}
		}
	}
}