package login
{
	import commandemobile.system.Login;
	
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.TextInput;
	
	public class LoginImpl extends Box
	{

		//COMPOSANTS
		public var txtbxLogin		:TextInput;
		public var txtbxPassWord	:TextInput;
		
		//OBJETS
		private var _log			:Login = new Login();
		
		public function LoginImpl()
		{
			_log.addEventListener("checkLoginOk",checkLoginOkResultHandler);
			_log.addEventListener("checkLoginKo",checkLoginKoResultHandler);
		}
		

		
		protected function btnValidateLoginClickHandler():void
		{
			if(txtbxLogin.text != "" && txtbxPassWord.text != "")
			{
				_log.checkLogin(txtbxLogin.text,txtbxPassWord.text);
			}
			else
			{
				if(txtbxLogin.text == "" && txtbxPassWord.text == "")
				{
					Alert.show("Veuillez saisir votre identifiant et votre mot de passe!","Consoview");
					return;
				}
				if(txtbxLogin.text == "")
				{
					Alert.show("Veuillez saisir votre identifiant!","Consoview");
					return;
				}
				if(txtbxPassWord.text == "")
				{
					Alert.show("Veuillez saisir votre mot de passe!","Consoview");
					return;
				}
			}
		}
		
		private function checkLoginOkResultHandler(e:Event):void
		{
			if(_log.resultLogin == 1)
			{
				//LOAD LA FENETRE 'COMMANDE RESILIATION'
			}
		}
		
		private function checkLoginKoResultHandler(e:Event):void
		{
			if(_log.resultLogin == -1)
			{
				Alert.show("Vérifier votre identifiant et votre mot de passe!","Consoview");
			}
		}
		
		
	}
}