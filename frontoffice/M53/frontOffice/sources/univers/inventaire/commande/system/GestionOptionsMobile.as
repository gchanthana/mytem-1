package univers.inventaire.commande.system
{
	import composants.util.ConsoviewAlert;
	import composants.util.article.Article;
	import composants.util.contrats.Contrat;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.managers.SystemManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class GestionOptionsMobile
	{
		private const CFC_GestionAbosOptions:String = "fr.consotel.consoview.inventaire.commande.GestionAbosOptions";
		
		private var _commande:Commande;
		private var _contrat:Contrat;
		private var _article:Article;
		private var _listeResourcesLigne:ArrayCollection;
		
		
		
		public function GestionOptionsMobile()
		{
		}
		
		public function genererCommandeFromContratAbo(idContrat:Number):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			CFC_GestionAbosOptions,
	    																			"findDataFromContrat",
	    																			function createCommandeFromContratResultHandler(event:ResultEvent):void
																					{																						
																						/* if(article   == null) article = new Article();
																						if(commande  == null) commande = new Commande();
																						if(contrat == null) contrat = new Contrat(); */																							
																						
																						contrat.IDCONTRAT = idContrat;
																						createCommandeFromContrat(contrat,event.result as Array);																						
																					});
			RemoteObjectUtil.callService(op,idContrat)
		}
		
		
		
		protected function createCommandeFromContrat(ctrt:Contrat ,donnee:Array):void
		{
			if(donnee[0] != null && (donnee[0] as ArrayCollection).length > 0)
			{
				ctrt.IDTYPE_CONTRAT = donnee[0][0].IDTYPE_CONTRAT;
				ctrt.IDFOURNISSEUR = donnee[0][0].IDFOURNISSEUR;
				ctrt.REFERENCE_CONTRAT = donnee[0][0].REFERENCE_CONTRAT;
				ctrt.DATE_SIGNATURE = donnee[0][0].DATE_SIGNATURE;
				ctrt.DATE_ECHEANCE = donnee[0][0].DATE_ECHEANCE;
				ctrt.COMMENTAIRE_CONTRAT = donnee[0][0].COMMENTAIRE_CONTRAT;
				ctrt.MONTANT_CONTRAT = donnee[0][0].MONTANT_CONTRAT;
				ctrt.DUREE_CONTRAT = donnee[0][0].DUREE_CONTRAT;
				ctrt.TACITE_RECONDUCTION = donnee[0][0].TACITE_RECONDUCTION;
				ctrt.DATE_DEBUT = donnee[0][0].DATE_DEBUT;
				ctrt.PREAVIS = donnee[0][0].PREAVIS ;
				ctrt.DATE_DENONCIATION = donnee[0][0].DATE_DENONCIATION;
				ctrt.IDCDE_CONTACT_SOCIETE = donnee[0][0].IDCDE_CONTACT_SOCIETE;
				ctrt.LOYER = donnee[0][0].LOYER;
				ctrt.PERIODICITE = donnee[0][0].PERIODICITE;
				ctrt.MONTANT_FRAIS = donnee[0][0].MONTANT_FRAIS;
				ctrt.NUMERO_CLIENT = donnee[0][0].NUMERO_CLIENT;
				ctrt.NUMERO_FOURNISSEUR = donnee[0][0].NUMERO_FOURNISSEUR;
				ctrt.NUMERO_FACTURE = donnee[0][0].NUMERO_FACTURE;
				ctrt.CODE_INTERNE = donnee[0][0].CODE_INTERNE;
				ctrt.BOOL_CONTRAT_CADRE = donnee[0][0].BOOL_CONTRAT_CADRE;
				ctrt.BOOL_AVENANT = donnee[0][0].BOOL_AVENANT;
				ctrt.ID_CONTRAT_MAITRE = donnee[0][0].ID_CONTRAT_MAITRE;
				ctrt.FRAIS_FIXE_RESILIATION = donnee[0][0].FRAIS_FIXE_RESILIATION;
				ctrt.MONTANT_MENSUEL_PEN = donnee[0][0].MONTANT_MENSUEL_PEN;
				ctrt.DATE_RESILIATION = donnee[0][0].DATE_RESILIATION;
				ctrt.PENALITE = donnee[0][0].PENALITE;
				ctrt.DESIGNATION = donnee[0][0].DESIGNATION;
				ctrt.DATE_RENOUVELLEMENT = donnee[0][0].DATE_RENOUVELLEMENT;
				ctrt.OPERATEURID = donnee[0][0].OPERATEURID;
				ctrt.IDCOMPTE_FACTURATION = donnee[0][0].IDCOMPTE_FACTURATION;
				ctrt.IDSOUS_COMPTE = donnee[0][0].IDSOUS_COMPTE;
				ctrt.NUMERO_CONTRAT = donnee[0][0].NUMERO_CONTRAT;
				ctrt.IDRACINE = donnee[0][0].IDRACINE;
				ctrt.DATE_ELLIGIBILITE = donnee[0][0].DATE_ELLIGIBILITE;
				ctrt.DUREE_ELLIGIBILITE = donnee[0][0].DUREE_ELLIGIBILITE;		
				
				
					
				ctrt.IDCDE_CONTACT_SOCIETE = donnee[0][0].IDCONTACT;
				
				//mapping de la commande
				commande.IDOPERATEUR = ctrt.OPERATEURID;
				commande.LIBELLE_OPERATEUR = donnee[0][0].LIBELLE_OPERATEUR;
				
				commande.IDREVENDEUR = ctrt.IDFOURNISSEUR;
				commande.LIBELLE_REVENDEUR = donnee[0][0].LIBELLE_OPERATEUR;
				commande.IDRACINE = ctrt.IDRACINE;
				commande.IDCOMPTE_FACTURATION = ctrt.IDCOMPTE_FACTURATION;
				commande.IDSOUS_COMPTE = ctrt.IDSOUS_COMPTE;
				commande.LIBELLE_SOUSCOMPTE = donnee[0][0].SOUS_COMPTE;
				commande.LIBELLE_COMPTE = donnee[0][0].COMPTE_FACTURATION;
				
				/* commande.IDPOOL_GESTIONNAIRE = 
				commande.IDGESTIONNAIRE = */
				
				article.dureeEngagementRessources = ctrt.DUREE_CONTRAT; 
				//article.sousTete = 
				article.idRevendeur = ctrt.IDFOURNISSEUR;
				//article.
				//mapping de larticle
				
				/* var donneesEquipements : ArrayCollection = donnee[2] as ArrayCollection;
				var lenRessources : Number = donneesEquipements.length;
				 
				for(var i:int = 0; i < lenRessources; i ++)
				{
					article.addRessource(donneesEquipements[i]);
				} */
				
				
				var donneesRessources : ArrayCollection = donnee[2] as ArrayCollection;
				var lenRessources : Number = donneesRessources.length;
				 
				for(var j:int = 0; j < lenRessources; j ++)
				{
					article.addRessource(donneesRessources[j]);
				}
			}
		}
		
		protected function createCommandeFromLigne(donnee:ArrayCollection,ressources:ArrayCollection):void
		{
			if(donnee.length > 0)
			{
				//le contrat
				contrat.IDTYPE_CONTRAT = donnee[0].IDTYPE_CONTRAT;
				contrat.DUREE_CONTRAT = donnee[0].DUREE_CONTRAT;
				contrat.DESIGNATION = donnee[0].DESIGNATION;
				contrat.REFERENCE_CONTRAT = donnee[0].REFERENCE_CONTRAT;
				contrat.IDCONTRAT = donnee[0].IDCONTRAT;
				
				article.libelleContratRes = contrat.DESIGNATION;
				article.dureeEngagementRessources = contrat.DUREE_CONTRAT;
			}
			if(ressources.length > 0)
			{
				if(!(commande.IDCOMPTE_FACTURATION > 0))
				{
					commande.IDCOMPTE_FACTURATION = ressources[0].IDCOMPTE_FACTURATION;
					commande.LIBELLE_COMPTE = ressources[0].COMPTE_FACTURATION;	
				}
								
				if(!(commande.IDSOUS_COMPTE > 0))
				{
					commande.IDSOUS_COMPTE = ressources[0].IDSOUS_COMPTE;
					commande.LIBELLE_SOUSCOMPTE = ressources[0].SOUS_COMPTE;	
				}
				
				if(!(commande.IDOPERATEUR > 0))
				{
					commande.IDOPERATEUR = ressources[0].OPERATEURID;
					commande.LIBELLE_OPERATEUR = ressources[0].NOM;	
				}
				
				
			 
				var lenRessources : Number = ressources.length;
				
				if(commande.IDTYPE_COMMANDE != TypesCommandesMobile.RENOUVELLEMENT)
				{
					for(var j:int = 0; j < lenRessources; j ++)
					{
						article.addRessource(ressources[j]);
					}	
				}
				 
			}
		}
		
		public function genererCommandeFromLigne(idSousTete:Number):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			CFC_GestionAbosOptions,
	    																			"genererCommandeFromSousTete",
	    																			function genererCommandeFromLigneResultHandler(event:ResultEvent):void
																					{																						
																						var donnee:ArrayCollection = event.result[0];
																						var ressources:ArrayCollection = event.result[1]
																						createCommandeFromLigne(donnee,ressources);																						
																					});
			
			
			RemoteObjectUtil.callService(op,idSousTete)	
		}
		
		
		public function renouvlerContratSousTete(idsoustete:Number,duree:Number):void
		{
			if(idsoustete > 0)
			{
				var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			CFC_GestionAbosOptions,
	    																			"renouvellerContratAboSoustete",
	    																			function renouvelerContratSousTete(event:ResultEvent):void
																					{																						
																						if(event.result > 0)
																						{
																							ConsoviewAlert.afficherOKImage("Renouvellement du contrat",SystemManager.getSWFRoot(this));
																						}																				
																					});
				RemoteObjectUtil.callService(op,idsoustete,duree)	
			}	
		}
		
		public function set commande(value:Commande):void
		{
			_commande = value;
		}

		public function get commande():Commande
		{
			return _commande;
		}

		public function set contrat(value:Contrat):void
		{
			_contrat = value;
		}

		public function get contrat():Contrat
		{
			return _contrat;
		}

		public function set article(value:Article):void
		{
			_article = value;
		}

		public function get article():Article
		{
			return _article;
		}

		public function set listeResourcesLigne(value:ArrayCollection):void
		{
			_listeResourcesLigne = value;
		}

		public function get listeResourcesLigne():ArrayCollection
		{
			return _listeResourcesLigne;
		}
	}
}