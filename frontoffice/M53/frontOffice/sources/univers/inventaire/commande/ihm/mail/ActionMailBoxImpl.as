package univers.inventaire.commande.ihm.mail
{
	import composants.mail.MailBoxImpl;
	import composants.util.ConsoviewUtil;
	import composants.util.CvDateChooser;
	import composants.util.PopUpImpl;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.TextArea;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.commande.system.Action;
	import univers.inventaire.commande.system.Commande;
	import univers.inventaire.commande.system.ElementHistorique;
	
	[Bindable]
	public class ActionMailBoxImpl extends MailBoxImpl
	{
		/**
	     * Référence vers une autre popUp
	     */
	    protected var _popUp: PopUpImpl;	    
		
		private var _popUpDestinataire:SelectDestinataireIHM = new SelectDestinataireIHM();
		
		private var _selectedAction:Action;
		
		private var _listeDestinataires:String;
		
		 private var _selectedFunction:Function;
		 
		 
	    
	    /**
	     * La date de l'action
	     */
	    public var dcDateAction: CvDateChooser;
	    
	
	    
	    /**
	     * Un commentaire pour l'action.
	     */
	    public var txtCommentaire: TextArea;
	    	    
	   	public var envoyerMail:Boolean;	   	
		
		public var cbMail:CheckBox;
		
		public function ActionMailBoxImpl()
		{
			super();
		}
		
	   	
	   	
	/*  public var defaultSelectedDate:Date = new Date();		
		
	 	private var _commande:Commande;
		public function get commande():Commande
		{
			return _commande
		}
		public function set commande(value:Commande):void
		{
			_commande = value
		} 
		private var _selectableRange:Object = {rangeStart:new Date()};
		public function get selectableRange():Object
		{			
			return _selectableRange
		}
		
		public function set selectableRange(value:Object):void
		{
			_selectableRange = value;
			
			var diff:int = ObjectUtil.dateCompare(_selectableRange.rangeStart as Date,new Date());
		 
			if(diff >= 0)
			{
				defaultSelectedDate = _selectableRange.rangeStart;
			}
			else
			{
				defaultSelectedDate = new Date();
			}
		}
	*/
		
		/**
	     * Si le mode écriture est à false alors on grise tous les boutons de
	     * modifications (Enregistrer, editer ...)
	     */
	    private var _modeEcriture: Boolean = true;
	    
	    /**
	     * setter pour _modeEcriture
	     * 
	     * @param mode
	     */
	    public function set modeEcriture(mode:Boolean): void
	    {
	    	_modeEcriture = mode;
	    }

	    /**
	     * getter pour _modeEcriture
	     */
	    public function get modeEcriture(): Boolean
	    {
	    	return _modeEcriture;		
	    }
	    
	    public function set popUpDestinataire(value:SelectDestinataireIHM):void
		{
			_popUpDestinataire = value;
		}

		public function get popUpDestinataire():SelectDestinataireIHM
		{
			return _popUpDestinataire;
		}
	    
	    public function set selectedAction(value:Action):void
		{
			_selectedAction = value;
			configurerLesDestinatairesDuMail();
			
		}
		
		public function get selectedAction():Action
		{
			return _selectedAction;
		}
	    
	    protected function removePopUp():void
		{
			if(_popUp != null)
			{
				
				if(_popUp.isPopUp)
				{
					PopUpManager.removePopUp(_popUp)
				}
				
				_popUp = null;
			}
		}
		
		
		protected function cbMailClickHandler(event:MouseEvent):void
		{
			if(cbMail.selected)
			{
				currentState = "mailState";
			}
			else
			{
				currentState = "noMailState";
			}	
		}
		
		
		/**
	     * Pour enregistrer l'action à la date du jour
	     * 
	     * @param event
	     */
	    override protected function btnEnvoyerClickHandler(event:MouseEvent): void
	    {
	    	if(currentState ==  "mailState")
	    	{
	    		
	    	
		    	_mail.destinataire = popUpDestinataire.listeMails;
		    	
		    	if(_mail.destinataire != "" && _mail.destinataire != null)
		    	{	
					_mail.cc = txtcc.text;
					_mail.bcc = txtcci.text;
					_mail.module = txtModule.text;			
					_mail.copiePourExpediteur = (cbCopie.selected)?"YES":"NO";
					_mail.copiePourOperateur = "NO";			
					_mail.sujet = txtSujet.text;
					_mail.message = rteMessage.text;
					_mail.repondreA = txtExpediteur.text;
					_mail.type = "text";
					
					if(_mail.destinataire != null)
					{
						var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
										"fr.consotel.consoview.inventaire.commande.GestionCommande",
										"envoyer",
										sendTheMailResultHandler,null,false);		
										
						RemoteObjectUtil.callService(op,_mail,Commande(infosObject.commande).IDCOMMANDE);		
					 
					
						dispatchEvent(new Event(MAIL_ENVOYE));	
						
						PopUpManager.removePopUp(this);
					}else{
						Alert.buttonWidth = 100;
						Alert.okLabel = "Fermer";
						Alert.show("Le destinataire est obligatoire");		
					}
		    	}
		    	else
		    	{
		    		Alert.buttonWidth = 100;
		    		Alert.show("Vous n'avez pas choisi de destinataire\n");
		    	}
		    }
		    else
		    {
		    	dispatchEvent(new Event(MAIL_ENVOYE));
		    }
	    }
	    
	    
	    
	    
	   
	    
	    override protected function imgContactsClickHandler(event:MouseEvent):void
		{	
			afficherSelectionContact()
	    }
		
		protected function configurerLesDestinatairesDuMail():void
		{
			removePopUp();
			if(selectedAction)
			{
				var localCode:String = _selectedAction.EXP+_selectedAction.DEST;
				
				switch(localCode)
				{
					case "CC":
					{
						//Liste des contacts client pouvant faire l'action 4
						getListeDestinataire();
						break;
					}
					case "CR":
					{
						//Liste des contact revendeurs ayant un role "Commande"
						getListeDestinatairesRevendeur(); 
						break;
					}
					case "RC":
					{
						
						if(_selectedAction.IDACTION == 2068)
						{
							//personne ayant fait l'action 'Demande
							getListeUserAyantFaitAction([2067])					
						}
						else
						{
							//Personne ayant fait l'action 'Préparer une commande' et 'Valider et envoyer'
							getListeUserAyantFaitAction([2066,2070,2116]);
						}
						break;
					}			
				}
	
			}
		}

	
		protected function afficherSelectionContact():void
		{				
			PopUpManager.addPopUp(_popUpDestinataire,this,true);
			PopUpManager.centerPopUp(_popUpDestinataire);
		}
		
		
		
		
		
		public function getListeDestinataire():void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil
	    									.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.inventaire.commande.GestionWorkFlow",
																	"getContactsActSuivante",
																	function(event:ResultEvent):void
															    	{
															    		if(event.result)	    	
															    		{	
															    			_popUpDestinataire.dataProvider = formatConatcts(event.result as ArrayCollection);
															    			
															    			
																    		/* listeDestinataires = ConsoviewUtil.
																    						ICollectionViewToList(event.result as ArrayCollection,"LOGIN_EMAIL",",");
																    		afficher(); */
																    	}
																    	else
																    	{
																    		Alert.show("Pas de destinataire configuré");
																    	}	
															    	});
			RemoteObjectUtil.callService(op,Commande(infosObject.commande).IDPOOL_GESTIONNAIRE,selectedAction.IDACTION);
	    }
	    
	    public function getListeDestinatairesRevendeur():void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil
	    									.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur",
																	"getContactsRevendeur",
																	function(event:ResultEvent):void
															    	{
															    		if(event.result)    	
															    		{	
															    			_popUpDestinataire.dataProvider = event.result as ArrayCollection;
															    			afficherSelectionContact()
															    			
																    		/* listeDestinataires = ConsoviewUtil.
																    						ICollectionViewToList(event.result as ArrayCollection,"EMAIL",";");
																    		afficher(); */
																    	}
																    	else
																    	{
																    		Alert.show("Pas de destinataire configuré pour ce revendeur");
																    	}	
															    	});
			RemoteObjectUtil.callService(op,infosObject.commande.IDREVENDEUR);
	    }
	    
	    public function getListeUserAyantFaitAction(selectedAction:Array):void
	    {
	    	var dataProvider:ArrayCollection = new ArrayCollection();
	    	
	    	if(infosObject.hasOwnProperty("historiqueCommande"))
	    	{
	    		
	    		var cursor:IViewCursor = (infosObject.historiqueCommande as ArrayCollection).createCursor();
	    		while(!cursor.afterLast)
	    		{
	    			if(ConsoviewUtil.isPresent(ElementHistorique(cursor.current).IDACTION.toString(),selectedAction))
	    			{
	    				if(!ConsoviewUtil.isLabelInArray(ElementHistorique(cursor.current).EMAIL,"EMAIL",dataProvider.source))
	    				{
	    					dataProvider.addItem({NOM:ElementHistorique(cursor.current).PATRONYME_USERACTION,
	    									  EMAIL:ElementHistorique(cursor.current).EMAIL})	
	    				}
	    				
	    			}
	    			cursor.moveNext();
	    		}
	    		
	    		_popUpDestinataire.dataProvider = dataProvider;
	    		_popUpDestinataire.contactsSelectionnes = dataProvider.source;
				_popUpDestinataire.listeMails = ConsoviewUtil.ICollectionViewToList(dataProvider,"EMAIL",",");
	    	}
	    }		
		
		
		
		private function formatConatcts(values:ICollectionView):ArrayCollection
	    {
	    	var retour : ArrayCollection = new ArrayCollection();
			if (values != null)
			{	
				var cursor : IViewCursor = values.createCursor();
				
				while(!cursor.afterLast){
					var ctcObj:Object = new Object();					
					ctcObj.NOM = cursor.current.NOM;	
					ctcObj.EMAIL = cursor.current.LOGIN_EMAIL;
					retour.addItem(ctcObj);		
					cursor.moveNext();
				}
				
					
			}
			return retour 
	    }
		
	}
}