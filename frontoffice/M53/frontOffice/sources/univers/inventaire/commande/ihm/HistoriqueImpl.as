package univers.inventaire.commande.ihm
{
	import flash.events.MouseEvent;
	
	import mx.collections.ICollectionView;
	import mx.controls.Menu;
	import mx.controls.PopUpMenuButton;
	import mx.events.MenuEvent;
	
	import univers.inventaire.commande.system.Action;
	import univers.inventaire.commande.system.ActionEvent;
	
	
	
	
	[Event(name="actionClicked",type="univers.inventaire.commande.system.ActionEvent")]
	
	
	[Bindable]
	public class HistoriqueImpl extends AbstractBaseViewImpl
	{
			
		public var btActionsItem:PopUpMenuButton;
		
		private var _dataProviderHistorique:ICollectionView;
		private var _dataProviderActionsPossibles:ICollectionView;		
		private var _boolDataProviderActionsPossiblesChange:Boolean = false;
		public var selectedAction:Action;
		public var actionEnabled:Boolean = true;
		
		 

        
		public function HistoriqueImpl()
		{
				
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();			
			if(_boolDataProviderActionsPossiblesChange)
			{
				btActionsItem.dataProvider = _dataProviderActionsPossibles;
			}
		}
		 
		protected function btActionItemMenuClickHandler(event:MenuEvent):void
		{
			if(event.item != null)
			{	
				selectedAction = event.item as Action;
				
			}
		}
            
		protected function btActionClickHandler(event:MouseEvent):void
		{
			if(selectedAction != null)
			{
				var actionEvent:ActionEvent = new ActionEvent(ActionEvent.ACTION_CLICKED);
				actionEvent.action = selectedAction;
				dispatchEvent(actionEvent);	
			}
		}
		
		

		public function set dataProviderHistorique(value:ICollectionView):void
		{
			_dataProviderHistorique = value;
		}

		public function get dataProviderHistorique():ICollectionView
		{
			return _dataProviderHistorique;
		}
		
		public function set dataProviderActionsPossibles(value:ICollectionView):void
		{
			if(value != _dataProviderActionsPossibles)
			{
				_dataProviderActionsPossibles = value;
				_boolDataProviderActionsPossiblesChange = true;
			}
			
			invalidateProperties();
		}

		public function get dataProviderActionsPossibles():ICollectionView
		{
			return _dataProviderActionsPossibles;
		}
		
		
		/* private function selectFirstAction():void
		{	
			if(!dataProviderActionsPossibles)
			{
				callLater(selectFirstAction)		
			}
			else
			{
				if(dataProviderActionsPossibles.length)
				{					
					Menu(btActionsItem.popUp).selectedIndex = 0;					
					selectedAction = dataProviderActionsPossibles[Menu(btActionsItem.popUp).selectedIndex] as Action;
					btActionsItem.label = dataProviderActionsPossibles[Menu(btActionsItem.popUp).selectedIndex].LIBELLE_ACTION; 
					
				}	
			}
		} */
	}
}