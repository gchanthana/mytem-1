package univers.inventaire.commande.ihm
{
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewFormatter;
	import composants.util.CvDateChooser;
	import composants.util.article.Article;
	import composants.util.article.ArticleFactory;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.commande.system.ArticleEvent;
	
	[Event(name="articleCreated",type="univers.inventaire.commande.system.ArticleEvent")]
	[Event(name="articleRmoved",type="univers.inventaire.commande.system.ArticleEvent")]
	
	[Bindable]
	public class CreerArticleImpl extends AbstractBaseViewImpl
	{
		public var dcDateFPC:CvDateChooser;
		public var dcDatePortage:CvDateChooser;		
		public var txtCodeRIO:TextInputLabeled;
		public var cbxPortage:CheckBox;
		public var cbxContratRessources:CheckBox;
		public var cbxContratEq:CheckBox;
		public var txtNumLigne:TextInputLabeled;
		public var txtCodeInterne:TextInputLabeled;
		public var dgListeRessources:DataGrid;
		public var txtDesignationContratRes:TextInputLabeled;
		public var txtDesignationContratEq:TextInputLabeled;
		public var cbxOuvertureLigne:CheckBox;
						
		public var txtFiltreRessources:TextInputLabeled;
		
		public var btValider:Button;
		public var btFermer:Button;		
		public var btVerifierNumLigne:Button;
		
		public var btAjouterEquipements:Button;
		public var btRetirerEquipements:Button;
		
		public var btAjouterRessources:Button;
		public var btRetirerRessources:Button;
		
		public var cboEngagementRes:ComboBox;
		public var cboEngagementEq:ComboBox;
		public var lblCollaborateur:Label;
		
		public var strCollaborateur:String;
		
		protected var _type:String="DEFAULT";
		public function set type(value:String):void
		{	
			_type = value;
			article = ArticleFactory.createArticle(_type);
		}
		public function get type():String
		{
			return _type
		}
		
		protected var _article : Article = new Article()
		public function get article():Article
		{
			return _article;
		}
		public function set article(value : Article):void
		{
			_article = value;
			strCollaborateur = _article.article.nomemploye[0];
		}
		
		private var _boolCreate:Boolean=true;
		public function set boolCreate(value:Boolean):void{
			_boolCreate = value; 
		}
		public function get boolCreate():Boolean{
			return _boolCreate;
		}
				
		
		public function CreerArticleImpl()
		{
			super();
		}		
		
		protected function formateEuros(value:Number):String{
			return isNaN(value)?"0.00":ConsoviewFormatter.formatEuroCurrency(value,2);
		}
		
		protected function txtNumLigneChangeHandler(event:Event):void
		{
			
		}		
		protected function btAjouterEquipementsClickHandler(event:MouseEvent):void
		{
			removePopUp();
			_popUp = new SelectionEquipementsIHM();
			SelectionEquipementsIHM(_popUp).article = article;
			SelectionEquipementsIHM(_popUp).commande = commande;
			SelectionEquipementsIHM(_popUp).addEventListener(SelectionBaseViewImpl.AJOUTER_CLICKED,equipementAddedHandler);
			_popUp.x = 50;
			_popUp.y = 100;		 
			PopUpManager.addPopUp(_popUp,this,true);
		}		
		protected function btRetirerEquipementsClickHandler(event:MouseEvent):void
		{
			article.removeEquipement(XML(dgListe.selectedItem));
			txtFiltre.text = "";
		}
		
		protected function btAjouterRessourcesClickHandler(event:MouseEvent):void
		{
			removePopUp();
			_popUp = new SelectionAbosOptionsIHM();
			SelectionAbosOptionsIHM(_popUp).article = article;
			SelectionAbosOptionsIHM(_popUp).commande = commande;
			SelectionAbosOptionsIHM(_popUp).addEventListener(SelectionBaseViewImpl.AJOUTER_CLICKED,ressourceAddedHandler);
			_popUp.x = 50;
			_popUp.y = 100;			 			 
			PopUpManager.addPopUp(_popUp,this,true);
		}	
			
		protected function btRetirerRessourcesClickHandler(event:MouseEvent):void
		{
			article.removeRessource(XML(dgListeRessources.selectedItem));
			txtFiltreRessources.text = "";
			
		}
		
		protected function btChoisirDestinatairesClickHandler(event:Event): void
	    {
	    	if(_popUp != null) _popUp = null;
			_popUp = new SelectionEmployeIHM();
			SelectionEmployeIHM(_popUp).commande = commande;
			SelectionEmployeIHM(_popUp).article = article;
			PopUpManager.addPopUp(_popUp,this,true);
			PopUpManager.centerPopUp(_popUp);	
			   	
	    }
		
		protected function btValiderClickHandler(event:MouseEvent):void
		{
			var boolRessources:Boolean = false;
			var boolEquipement:Boolean = false;
			var message:String="\n";
			
			if(XMLList(article.article.equipements.equipement).length() >0)			
			{
				boolEquipement = true
			}
			else
			{
				boolEquipement = false
			}
			
			if(XMLList(article.article.ressources.ressource).length() >0)
			{
				boolRessources = true
			}
			else
			{	
				boolRessources = false	
			}
				
			if (boolEquipement || boolRessources)
			{
				article.sousTete = txtNumLigne.text;
			
				article.codeInterne = txtCodeInterne.text;
				
				if(cbxContratRessources.selected && cboEngagementRes.selectedItem != null)
				{
					article.dureeEngagementRessources = Number(cboEngagementRes.selectedItem.value);	
				}
				else
				{
					article.dureeEngagementRessources = -1;
				}
				
				
				if(cbxContratEq.selected && cboEngagementEq.selectedItem != null)
				{
					article.dureeEngagementEquipement = Number(cboEngagementEq.selectedItem.value);	
				}
				else
				{
					article.dureeEngagementRessources = -1;
				}
				
			
				if (boolCreate)
				{
					dispatchEvent(new ArticleEvent(article,ArticleEvent.ARTICLE_CREATED));
				}
				else
				{
					dispatchEvent(new ArticleEvent(article,ArticleEvent.ARTICLE_UPDATED));
				}
				
				
				PopUpManager.removePopUp(this);	
			}
			else
			{
				Alert.show("Vous devez sélectionner un produits ou un équipement","Erreur");
			}
			
		}
		
		protected function ressourceAddedHandler(event:Event):void
		{
			 
			txtFiltreRessources.text = "";
		}
		
		protected function equipementAddedHandler(event:Event):void
		{
			txtFiltre.text = "";
		}
				
		protected function btFermerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function numericSortFunction(itemA:Object, itemB:Object):int{
			return ObjectUtil.numericCompare(itemA.prix,itemB.prix);
		}
	}
}