package univers.inventaire.commande.ihm
{
	[Event(name="btAjouterClicked",type="flash.events.Event")]
	[Event(name="btFermerClicked",type="flash.events.Event")]
	
	
	[Bindable]
	public class SelectionBaseViewImpl extends AbstractBaseViewImpl
	{
		
		public static const AJOUTER_CLICKED:String="btAjouterClicked";
	    public static const FERMER_CLICKED:String="btFermerClicked";
	    
		public function SelectionBaseViewImpl()
		{
			super();
		}
		
	}
}