package univers.inventaire.contrat.system
{
	import mx.rpc.events.ResultEvent;
	
	public class RechercheContratsEquipementsMobile extends RechercheContrats
	{
		
		public function RechercheContratsEquipementsMobile()
		{
			super();
		}
		
				
		override public function findContrats(idPool:Number):void
		{
			 op = RemoteObjectUtil
					.getHandledOperationFrom(	RemoteObjectUtil.DEFAULT_DESTINATION,
												"fr.consotel.consoview.inventaire.contrats.RechercheContratsEquipementsMobile",
												"findContrat");
			
			op.send(idPool);
		}
		
		private function findContratsResultHandler(event:ResultEvent):void
		{
			resutatRecherche = event.result;		
		}
	}
}