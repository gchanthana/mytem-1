package univers.inventaire.contrat.system
{
	import flash.events.IEventDispatcher;
	
	import mx.collections.ICollectionView;
	
	public interface IResultatRecherche extends IEventDispatcher
	{	 
		function get data():ICollectionView;
	}
}