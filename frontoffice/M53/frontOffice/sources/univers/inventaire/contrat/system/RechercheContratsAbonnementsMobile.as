package univers.inventaire.contrat.system
{
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	 
	[Event(name="resultatChange")]
	public class RechercheContratsAbonnementsMobile extends RechercheContrats
	{
		private const remoteRechercheContratsAbonnements:String = "fr.consotel.consoview.inventaire.contrats.RechercheContratsAbonnementsMobile"; 
		
		public function RechercheContratsAbonnementsMobile()
		{	 
			super()	
		}
		
		override public function findContrats(clef :String = "",idPool:Number=1401):void
		{
			var op:AbstractOperation = RemoteObjectUtil
					.getHandledOperationFrom(	RemoteObjectUtil.DEFAULT_DESTINATION,
												remoteRechercheContratsAbonnements,
												"findContrats",
												findContratsResultHandler);
			
			RemoteObjectUtil.callService(op,idPool,clef);
		}
		
		protected function findContratsResultHandler(event:ResultEvent):void
		{	
			mappData(event.result as ArrayCollection);
				
			dispatchEvent (new Event ("resultatChange"));	
		}
				
		
		public function get resultat ():ICollectionView
		{
			return _resultat;
		}
		
		private function mappData(newValues:ICollectionView):void
		{
			if (!newValues) return;
			
			var cursor:IViewCursor = newValues.createCursor();
			_resultat = new ArrayCollection();
			while(!cursor.afterLast)
			{
				var contrat:ContratAbonnementMobile = new ContratAbonnementMobile();
				contrat.NUMERO_CONTRAT = cursor.current.NUMERO_CONTRAT;
				contrat.SOUS_TETE = cursor.current.SOUS_TETE;
				contrat.IDSOUS_TETE = cursor.current.IDSOUS_TETE;
				contrat.IDCONTRAT =cursor.current.IDCONTRAT;
				contrat.IDTYPE_CONTRAT =cursor.current.IDTYPE_CONTRAT;
				contrat.IDFOURNISSEUR =cursor.current.IDFOURNISSEUR;
				contrat.REFERENCE_CONTRAT =cursor.current.REFERENCE_CONTRAT;
				contrat.DATE_SIGNATURE =cursor.current.DATE_SIGNATURE;
				contrat.DATE_ECHEANCE =cursor.current.DATE_ECHEANCE;
				contrat.COMMENTAIRE_CONTRAT =cursor.current.COMMENTAIRE_CONTRAT;
				contrat.MONTANT_CONTRAT =cursor.current.MONTANT_CONTRAT;
				contrat.DUREE_CONTRAT =cursor.current.DUREE_CONTRAT;
				contrat.TACITE_RECONDUCTION =cursor.current.TACITE_RECONDUCTION;
				contrat.DATE_DEBUT =cursor.current.DATE_DEBUT;
				contrat.PREAVIS =cursor.current.PREAVIS;
				contrat.DATE_DENONCIATION =cursor.current.DATE_DENONCIATION;
				contrat.IDCDE_CONTACT_SOCIETE =cursor.current.IDCDE_CONTACT_SOCIETE;
				contrat.LOYER =cursor.current.LOYER;
				contrat.PERIODICITE =cursor.current.PERIODICITE;
				contrat.MONTANT_FRAIS =cursor.current.MONTANT_FRAIS;
				contrat.NUMERO_CLIENT =cursor.current.NUMERO_CLIENT;
				contrat.NUMERO_FOURNISSEUR =cursor.current.NUMERO_FOURNISSEUR;
				contrat.NUMERO_FACTURE =cursor.current.NUMERO_FACTURE;
				contrat.CODE_INTERNE =cursor.current.CODE_INTERNE;
				contrat.BOOL_CONTRAT_CADRE =cursor.current.BOOL_CONTRAT_CADRE;
				contrat.BOOL_AVENANT =cursor.current.BOOL_AVENANT;
				contrat.ID_CONTRAT_MAITRE =cursor.current.ID_CONTRAT_MAITRE;
				contrat.FRAIS_FIXE_RESILIATION =cursor.current.FRAIS_FIXE_RESILIATION;
				contrat.MONTANT_MENSUEL_PEN =cursor.current.MONTANT_MENSUEL_PEN;
				contrat.DATE_RESILIATION =cursor.current.DATE_RESILIATION;
				contrat.PENALITE =cursor.current.PENALITE;
				contrat.DESIGNATION =cursor.current.DESIGNATION;
				contrat.DATE_RENOUVELLEMENT =cursor.current.DATE_RENOUVELLEMENT;
				(_resultat as ArrayCollection).addItem(contrat);
				cursor.moveNext();
			}
		}

		
		
		
	}
}