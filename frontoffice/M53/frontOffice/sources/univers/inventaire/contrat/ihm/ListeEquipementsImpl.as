package univers.inventaire.contrat.ihm
{
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;

	public class ListeEquipementsImpl extends TitleWindow
	{
		public function ListeEquipementsImpl()
		{
			super();
		}
		

		protected function _localeCloseHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btAnnulerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function _dgListeChangeHandler(event:ListEvent):void
		{
			
		}
	}
}