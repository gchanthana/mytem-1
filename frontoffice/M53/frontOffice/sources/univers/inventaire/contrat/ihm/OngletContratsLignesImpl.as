package univers.inventaire.contrat.ihm
{
	import flash.events.Event;
	
	import mx.collections.ICollectionView;
	import mx.containers.VBox;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.contrat.system.RechercheContratsAbonnementsMobile;

	
	
	public class OngletContratsLignesImpl extends VBox
	{
		[Bindable]
		public var cpDetailContratLigne:DetailContratLigneIHM;
		
		[Bindable]
		public var cpListeContratsLignes:ListeContratsLignesMobilesIHM;
		public var ppCreerContratAbonnement:CreerContratAbonnementIHM;
		
		[Bindable]
		protected var _listeContrats:ICollectionView;
				
		private var _finder:RechercheContratsAbonnementsMobile;
					
		public function OngletContratsLignesImpl()
		{
			super();
			
		
			
			_finder = new RechercheContratsAbonnementsMobile();
			_finder.addEventListener("resultatChange",finderResultatChangeHandler);
			_finder.findContrats();
		}
		
				
		protected function btCreerContratClickedHandler(event:Event):void
		{
			ppCreerContratAbonnement = new CreerContratAbonnementIHM();
			PopUpManager.addPopUp(ppCreerContratAbonnement,this,true);
			PopUpManager.centerPopUp(ppCreerContratAbonnement);
		}
		
		private function finderResultatChangeHandler(event:Event):void
		{
			_listeContrats = _finder.resultat;
		}
		
		
		
	}
}