package univers.inventaire.grid
{
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.containers.VBox;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.ListEvent;
	
	[Bindable]
	public class GridFlotteMobileImpl extends VBox
	{
		public var dgListe:DataGrid;
		
		private var _dataProvider:ICollectionView;
		private var _vue:String="Accueil";
		
		
		public static const VUE_ACCUEIL:String="Accueil";
		public static const VUE_LIGNES:String="Lignes";
		public static const VUE_EQUIPEMENTS:String="Equipements";
		public static const VUE_COLLABORATEURS:String="Collaborateurs";
		
		private var _listeVues:ArrayCollection = new ArrayCollection([	{label:VUE_ACCUEIL},
																		{label:VUE_COLLABORATEURS},
																		{label:VUE_EQUIPEMENTS},
																		{label:VUE_LIGNES}]);
		
		public function GridFlotteMobileImpl()
		{
			super();		
		}	
		
		    
	   //---- des formatteur
		protected function formaterTipsDATE_ENTREE(item : Object):String
		{
			return DateFunction.formatDateAsString(item.DATE_ENTREE);
		} 
		protected function formaterTipsDATE_SORTIE(item : Object):String
		{
			return DateFunction.formatDateAsString(item.DATE_SORTIE);
		} 
		protected function formaterTipsDATE_ELLIGIBILITE(item : Object):String
		{
			return DateFunction.formatDateAsString(item.DATE_ELLIGIBILITE);
		} 
		protected function formaterTipsDATE_FIN_GARANTIE(item : Object):String
		{
			return DateFunction.formatDateAsString(item.DATE_FIN_GARANTIE);
		} 
		protected function formaterTipsDATE_COMMANDE(item : Object):String
		{
			return DateFunction.formatDateAsString(item.DATE_COMMANDE);
		} 
		
					
	 	protected function formaterColonneDates(item : Object, column : DataGridColumn):String
	   	{	
			if (item != null && item[column.dataField] != null)
			{
				return DateFunction.formatDateAsString(item[column.dataField]);				
			}else return "";
			
		}
		
		protected function formateColonnneEuros(item : Object, column : DataGridColumn):String
		{			
			if (item != null) return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2);	
			else return "";
		}
		
		
		protected function formateColonneNumber(item : Object, column : DataGridColumn):String
		{
			if (item != null) return ConsoviewFormatter.formatNumber(Number(item[column.dataField]),2);
			else return "";
		}
		
		protected function formateColonneSousTete(item : Object, column : DataGridColumn = null):String
		{			
			if (item != null && column != null) return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
			else return "";
		}
		
		protected function formateColonneBoolean(item : Object, column : DataGridColumn):String
		{
			if (item != null) return (Number(item[column.dataField]) == 1)?"OUI":"NON";
			else return "";
		}
		
		
		protected function formateNumber(value:Number):String
		{
			return ConsoviewFormatter.formatNumber(Number(value),2)
		}
		
		[Inspectable(defaultValue="Accueil",enumeration="Accueil,Collaborateurs,Lignes,Equipements",type="String")] 
		public function set vue(value:String):void
		{
			if(!value){
				_vue = VUE_ACCUEIL;
			}
			else
			{
				_vue = value;
			}			
			invalidateProperties()
		}

		public function get vue():String
		{
			return _vue;
		}
			
		protected function _localeChangeHandler(event:ListEvent):void
		{
		}
		
		protected function _localeItemClickEventHandler(event:ListEvent):void
		{		
			
		}
		
		//////// ACCESSOR ////////////////////////////////////////
		
		public function set dataProvider(value:ICollectionView):void
		{
			_dataProvider = value;
			invalidateProperties()
		}

		public function get dataProvider():ICollectionView
		{
			return _dataProvider;
		}

		
		public function set listeVues(value:ArrayCollection):void
		{
			_listeVues = value;
		}

		public function get listeVues():ArrayCollection
		{
			return _listeVues;
		}
	}
}