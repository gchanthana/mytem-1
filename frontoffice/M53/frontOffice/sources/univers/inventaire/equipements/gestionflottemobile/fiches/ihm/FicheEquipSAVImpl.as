package univers.inventaire.equipements.gestionflottemobile.fiches.ihm
{
	import composants.util.*;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.commande.ihm.AbstractBaseViewImpl;
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	import univers.inventaire.equipements.gestionflottemobile.fiches.system.sav.Intervention;
	import univers.inventaire.equipements.gestionflottemobile.fiches.system.sav.SavEquipementUtil;
	import univers.inventaire.equipements.gestionflottemobile.fiches.system.sav.Ticket;
	
	
	
	[Bindable]
	public class FicheEquipSAVImpl extends AbstractBaseViewImpl
	{
		
		
		public var libelleEquip : String;
		public var idFournisseur : Number;
		public var idEmploye : Number;
		public var idContrat : Number;
		public var idEquipement : Number;
		
		public var _editabel:Boolean = false;
		public var sav_equip_utils:SavEquipementUtil = new SavEquipementUtil();
		
		public var dgListeTicketOfEquip:DataGrid;
		public var dgListeIntervention:DataGrid;
		public var DCdate_deb :CvDateChooser;
		public var TIdescription:TextInput;
		public var TIcommentaires:TextInput;
	
		public var btValider:Button;
		public var CMBSite:ComboBox;
		public var bt_add_ticket:Button;
		public var cmbTypeInter : ComboBox;
		public var cmbMotifInter : ComboBox;
		
		public var bt_out_sav:Button;
		public var bt_close_sav:Button;
				
		public var garantie_applicable:int;
		public var idfournisseur:int;
		public var idsiteint:int;
		public var iduser:int;
		public var idemploye:int;
		
		private var _modeNewIntervention : Boolean = true;

		//protected var listeMotifIntervention : ArrayCollection = new ArrayCollection();
		//protected var listeTypeIntervention : ArrayCollection = new ArrayCollection();		
		
		public function FicheEquipSAVImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, _localeCreationCompleteHandler);
		}		

		public function set selectedTicket(value:Object):void
		{
			sav_equip_utils.selectedTicket = value;
		}
		public function get selectedTicket():Object
		{
			return sav_equip_utils.selectedTicket;
		}
		
		public function set editabel(value:Boolean):void
		{
			_editabel = value;
		}

		public function get editabel():Boolean
		{
			return _editabel;
		}
		private function getData(evt : Event = null):void
		{
			sav_equip_utils.getMotifIntervention();
			sav_equip_utils.getTypeIntervention();	 
			sav_equip_utils.getSiteOfPool();
			sav_equip_utils.getTicketOfEquip(Number(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL));
			selectedTicket = new Ticket(idFournisseur);
			editabel = false;
			setCurrentState('');
			
		}
		public function bt_add_ticketHandler(e : Event):void
		{
			sav_equip_utils.createNewTicketNumber(idfournisseur);
			//btValider.label = "Ajouter le ticket";
			dgListeTicketOfEquip.selectedIndex = -1;
			setCurrentState('state_creation');
			editabel = true;
		}
		
				
		//------------- HANDLERS --------------------------------------------------------------------------------------------------------------------
		protected function cboSiteChangeHandler(evt : Event):void
		{
			if(CMBSite.selectedIndex !=-1){
				sav_equip_utils.tmpValues.IDSITE = CMBSite.selectedItem.IDSITE_PHYSIQUE;
			}
		}
		protected function _localeCreationCompleteHandler(event:FlexEvent):void
		{
			
			getData();
			CMBSite.addEventListener(ListEvent.CHANGE, _cmbDureeGarantieChangeHandler);
			sav_equip_utils.addEventListener(SavEquipementUtil.MAJINFOSSAV,getData);
			sav_equip_utils.addEventListener(SavEquipementUtil.UPDATEINTERVENTION,refreshListeInterventions);
			sav_equip_utils.addEventListener(PanelGestionFoltteMobile.VALIDER_EVENT,closePopUp);
		}
		private function closePopUp(e : Event):void
		{
			PopUpManager.removePopUp(this);
			dispatchEvent(new Event(PanelGestionFoltteMobile.VALIDER_EVENT));
		}
		private function refreshListeInterventions(evt : Event = null):void
		{
			sav_equip_utils.getInfosTicket(dgListeTicketOfEquip.selectedItem as Ticket);
			setCurrentState('state_update');
		}
		
		
		protected function _localeCloseHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
				
		protected function btAnnulerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
	 		
		protected function btValiderClickHandler(event:MouseEvent):void
		{
			
			sav_equip_utils.tmpValues.IDFOURNISSEUR = idfournisseur;
			sav_equip_utils.tmpValues.IDEMPLOYE = idemploye;
			sav_equip_utils.tmpValues.IDCONTRAT = idContrat;
			sav_equip_utils.tmpValues.IDEQUIPEMENT = idEquipement;
			sav_equip_utils.tmpValues.LIBELLE_EQUIP  = libelleEquip;
			
			
			
			if(dgListeTicketOfEquip.selectedIndex == -1)
			{
				//Alert.show("CREATION /n "+ObjectUtil.toString(sav_equip_utils.tmpValues));
				sav_equip_utils.createTicket(sav_equip_utils.tmpValues as Ticket);
			}
			else
			{
				//Alert.show("UPDATE /n "+ObjectUtil.toString(sav_equip_utils.tmpValues));
				sav_equip_utils.updateTicket(sav_equip_utils.tmpValues);
			}
		}
		private function updateDataHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
			dispatchEvent(new FicheEvent(FicheEvent.FICHE_UPDATED,true));
		}

		protected function _cmbDureeGarantieChangeHandler(event:ListEvent):void
		{
			if(CMBSite.selectedIndex > -1)
			{
				//sav_equip_utils.tmpValues.IDSITE=CMBSite.selectedItem.IDSITE	
			}
			else
			{
				CMBSite.selectedIndex = 0;
			}
		}
		

		protected function getLabelLibelle(item:Object,column:DataGridColumn):String
 		{
 			var value : String = "Ouvert";
 			if (item.OUVERT == 0)
 			{
 				value = "Clôturé";
 			}
  			return value;
 		}
		
		protected function dgListeTicketHandler(evt: Event):void
		{
			if(dgListeTicketOfEquip.selectedIndex !=-1)
			{ 
				sav_equip_utils.getInfosTicket(dgListeTicketOfEquip.selectedItem as Ticket); 
				setCurrentState('state_update');
				editabel = true;
			}
		}
		protected function dgListeInterventionHandler(evt : Event):void
		{
			if(dgListeIntervention.selectedIndex != -1)
			{
				sav_equip_utils.selectedIntervention = dgListeIntervention.selectedItem;
			}			
		}
		protected function cboTypeHandler(e : Event):void
		{
			sav_equip_utils.tmpIntervention.IDTYPE = cmbTypeInter.selectedItem.IDTYPE_INTERVENTION;
		}
		protected function cboMotifHandler(e : Event):void
		{
			sav_equip_utils.tmpIntervention.IDMOTIF = cmbMotifInter.selectedItem.IDMOTIF_INTERVENTION;
		}
		protected function bt_add_intervention_handler(e : MouseEvent):void
		{
			 
			if(_modeNewIntervention == true)
			{
				//Alert.show("NEW" + ObjectUtil.toString(sav_equip_utils.tmpIntervention));
				 sav_equip_utils.addIntervention(sav_equip_utils.tmpIntervention as Intervention);
			}
			else
			{
				//Alert.show("update" + ObjectUtil.toString(sav_equip_utils.tmpIntervention));
				sav_equip_utils.updateIntervention(sav_equip_utils.tmpIntervention);
			}
		}
		protected function editeInterventionHandler(e:Event):void
		{
			if(dgListeIntervention.selectedIndex!=-1){
				_modeNewIntervention = false;
				sav_equip_utils.selectedIntervention = dgListeIntervention.selectedItem;
				sav_equip_utils.tmpIntervention = ObjectUtil.copy(dgListeIntervention.selectedItem);
				setCurrentState('state_add_intervention');
			}
			else
			{
				Alert.show("Une intervention doit être sélectionnée");
			}
		}
		protected function newInterventionHandler(e:MouseEvent):void
		{
			_modeNewIntervention = true;
			sav_equip_utils.selectedIntervention = new Intervention(selectedTicket.IDSAV);
			sav_equip_utils.tmpIntervention = new Intervention(selectedTicket.IDSAV);
			setCurrentState('state_add_intervention');
		}
	
		protected function bt_out_sav_handler(e : MouseEvent):void
		{
			sav_equip_utils.putEquipActive(idEquipement);
		}
		protected function bt_close_sav_handler(e : MouseEvent):void
		{
			if(dgListeTicketOfEquip.selectedIndex!=-1)
			{
				sav_equip_utils.updateTicket(dgListeTicketOfEquip.selectedItem as Ticket);
			}
			else
			{
				Alert.show("Un ticket doit être sélectionné");
			}
		}
		
		
	}
}