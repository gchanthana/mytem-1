package univers.inventaire.equipements.gestionflottemobile.rebus
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.gestionflottemobile.cellule.Cellule;
	
	public class PanelConfirmationRebus extends PanelConfirmationRebusIHM
	{
		public static const VALIDER_EVENT:String="validerEvent";
		private var colonneGauche : VBox;
		private var colonneDroite : VBox;
		private var dateRebus : String;
		private var listeEquip : String;
		private var listeLigne : String;
		private var obj_cellule : Cellule;
		public function PanelConfirmationRebus(colonneGauche : VBox,colonneDroite : VBox,dateRebus : String,listeEquip : String,listeLigne : String,obj_cellule : Cellule)
		{
			this.dateRebus=dateRebus;
			this.listeEquip=listeEquip;
			this.listeLigne =listeLigne;
			this.obj_cellule = obj_cellule;
			this.colonneDroite = colonneDroite;
			this.colonneGauche = colonneGauche;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt : FlexEvent):void
		{	
			//BEBUG : 
			//Alert.show("liste d'id equip : "+listeEquip+"...........Liste d'id Ligne"+listeLigne);
			panel_gauche.addChild(colonneGauche);
			panel_droite.addChild(colonneDroite);
			addEventListener(CloseEvent.CLOSE,fermer);
			btAnnuler.addEventListener(MouseEvent.CLICK,fermer);
			btValider.addEventListener(MouseEvent.CLICK,valider);
		}
		private function fermer(evt : Event):void 
		{
			PopUpManager.removePopUp(this);
		}
		private function valider(evt : MouseEvent):void
		{
			 
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"putOutEmploye",
																		rebus_handler);
			RemoteObjectUtil.callService(op,obj_cellule.id_cellule,obj_cellule.libelle_cellule,dateRebus,listeEquip,listeLigne);
			//ConsoviewAlert.afficherOKImage("En construction");
			//PopUpManager.removePopUp(this);
		}
		private function rebus_handler (evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				ConsoviewAlert.afficherOKImage("Départ enregistré");
				dispatchEvent(new Event(VALIDER_EVENT));
				PopUpManager.removePopUp(this);
			}
			else
			{
				trace("Erreur putOutEmploye"+evt);
			}
		}

	}
}