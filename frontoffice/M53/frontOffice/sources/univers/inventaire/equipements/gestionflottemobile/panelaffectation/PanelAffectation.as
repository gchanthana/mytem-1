package univers.inventaire.equipements.gestionflottemobile.panelaffectation
{
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	import univers.inventaire.equipements.gestionflottemobile.cellule.Cellule;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.IComportementAffectation;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.IListeDispo;
	
	public class PanelAffectation extends PanelAffectationIHM
	{
		private var obj_cellule:Cellule;
		private var obj_comportement : IComportementAffectation;
		private var obj_listeDispo : IListeDispo;
		
		private var pretAutorise : Boolean;
		private var cocheCasePret : Boolean;
		private var defaultParametreOrder : Boolean;
		
		public function PanelAffectation(obj_cellule:Cellule, obj_comportement : IComportementAffectation,obj_listeDispo : IListeDispo, pretAutorise : Boolean = true,cocheCasePret :Boolean = false,defaultParametreOrder : Boolean=true)
		{
			this.cocheCasePret = cocheCasePret;
			this.pretAutorise = pretAutorise;
			this.defaultParametreOrder = defaultParametreOrder;
			this.obj_cellule = obj_cellule;//La cellule séléctionnée dans l'écran principal
			this.obj_comportement = obj_comportement;//Les précédures à appeler
			this.obj_listeDispo = obj_listeDispo;//La liste à afficher (Ou sim, ou collab, ou terminal)
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM)
			
		}
		private function initIHM(evt : FlexEvent):void
		{
			img_cellule.source = obj_cellule.image_cellule; //Afficher l'icone de la cellule séléctionné
			img_liste.source=obj_listeDispo.getSourceIcone();//Afficher l'icone de la liste
			img_liste_big.source=obj_listeDispo.getSourceIcone();//Afficher l'icone de la liste
			lab_cellule_name.text = obj_cellule.libelle_cellule;//Affichage du nom de la cellule séléctionné
			lab_titre.text=""+obj_listeDispo.getLibelleType()+"";//Affichage du type d'objet de la liste
			box_listeDispo.addChild(obj_listeDispo as VBox);//Ajout du composant contenant la dg + le filtre
				
			calendar.selectedDate = new Date(); // Selection de la date actuel dans la date du jour
			
			btAnnuler.addEventListener(MouseEvent.CLICK,fermer);
			btValider.addEventListener(MouseEvent.CLICK,valider);
			addEventListener(CloseEvent.CLOSE,fermer);
			labDgSelection.text = obj_listeDispo.getLibelleSelection();
			
			if(pretAutorise == false)
			{
				check_pret.enabled = false;
				imgPret.enabled = false;
			}
			(obj_listeDispo.getDataGrid() as DataGrid).addEventListener(Event.CHANGE,dgListeDispo_handler);
			addEventListener(MouseEvent.CLICK,dgListeDispo_handler);
			if(cocheCasePret == true)
			{
				imgFiltre.visible=true;
				preterEffect.play();
				check_pret.selected = true;
			}
		}
		private function dgListeDispo_handler(evt : Event):void
		{
			labDgSelection.text = obj_listeDispo.getLibelleSelection();
		}
		private function valider(evt : MouseEvent):void
		{
			if(obj_listeDispo.getIDSelection() !=-1)
			{
				var isPrete : int = 0;
				if(check_pret.selected == true)
				{
					isPrete = 1;
				}
				if(defaultParametreOrder == true)
				{
					obj_comportement.affecter(obj_cellule.id_cellule,obj_listeDispo.getIDSelection(),obj_cellule.libelle_cellule,obj_listeDispo.getLibelleSelection(),DateFunction.formatDateAsInverseString(calendar.selectedDate),isPrete);
				}
				else
				{
					obj_comportement.affecter(obj_listeDispo.getIDSelection(),obj_cellule.id_cellule,obj_listeDispo.getLibelleSelection(),obj_cellule.libelle_cellule,DateFunction.formatDateAsInverseString(calendar.selectedDate),isPrete);
				}
				
				dispatchEvent(new Event(PanelGestionFoltteMobile.VALIDER_EVENT));
				
			}
			else
			{
				Alert.show("Sélectionnez un élément dans la liste");
			}
		}
		private function fermer(evt : Event):void 
		{
			dispatchEvent(new Event(PanelGestionFoltteMobile.ANNULER_EVENT));
		}
	
	}
}