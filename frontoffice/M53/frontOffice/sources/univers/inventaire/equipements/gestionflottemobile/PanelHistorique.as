package univers.inventaire.equipements.gestionflottemobile
{
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelHistoriqueIHM;

	public class PanelHistorique extends PanelHistoriqueIHM
	{
		private var colHistorique : ArrayCollection;
		public function PanelHistorique(colHistorique : ArrayCollection){
			
			this.colHistorique = colHistorique;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt : FlexEvent):void
		{
		//	var colPhraseHisto : ArrayCollection = new ArrayCollection();
			var tmpArr:Array = (colHistorique as ArrayCollection).source;
			var i:int;
			for(i=0; i < tmpArr.length;i++){	
				
				colHistorique.getItemAt(i).PHRASE = tmpArr[i].WORD1 +" "+tmpArr[i].ITEM1+" "+ tmpArr[i].WORD2 +" "+tmpArr[i].ITEM2+".";
				colHistorique.getItemAt(i).DATE_ACTION=DateFunction.formatDateAsString(tmpArr[i].DATE_ACTION);
				colHistorique.getItemAt(i).DATE_EFFET=DateFunction.formatDateAsString(tmpArr[i].DATE_EFFET);
			}
						
			dgHisto.dataProvider = colHistorique;
			btAnnuler.addEventListener(MouseEvent.CLICK,close);
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
		}
		private function close(evt : MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		private function txtFiltreChangeHandler(ev : Event):void
		{
			try{
				colHistorique.filterFunction = filtrerLeGrid;
				colHistorique.refresh();
			}catch(e :Error){
				trace("Erreur Gestion Flotte GSM");
			}
		}
	private function filtrerLeGrid(item : Object):Boolean
	{
			if ((String(item.PHRASE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) ||
			(String(item.AUTEUR).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)||
			(String(item.DATE_ACTION).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)||
			(String(item.DATE_EFFET).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
			)
			{
				return true;
			}
			else
			{
				return false;
			}
		
		return true;
		}
	}
}