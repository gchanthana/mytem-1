package univers.inventaire.equipements.gestionflottemobile.sav
{
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.gestionflottemobile.DgRechercheItemVo;
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	public class SavTerminal extends SavTerminalIHM
	{
		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")] 
		private var adrImg : Class;
		private var ligneSelected : DgRechercheItemVo;
		
		public function SavTerminal(ligneSelected : DgRechercheItemVo)
		{
			super();
			this.ligneSelected = ligneSelected;

			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);			
		}

		private function initIHM(evt : FlexEvent):void
		{
			imgTitre.source = adrImg; 
			libelle_terminal_sav.text = ligneSelected.IMEI;
			addEventListener(CloseEvent.CLOSE,fermer);
			calendar.selectedDate = new Date();	
			bt_annuler.addEventListener(MouseEvent.CLICK,fermer);
			bt_valider.addEventListener(MouseEvent.CLICK,savTerminal);	
		}
		
		private function fermer(evt : Event):void 
		{
			dispatchEvent(new Event(PanelGestionFoltteMobile.ANNULER_EVENT));
		}
		
		private function savTerminal(evt : MouseEvent):void
		{
			var idCause : int;
	
				if (radio_casse.selected == true){
					idCause = 2 ;
				}
				else
				{
					if (radio_vol.selected == true){
						idCause = 3;
					}
					else
					{
						if (radio_autre.selected == true){
						idCause = 0;
					}
					}
				}

			var idTerm : String = ligneSelected.IDTERMINAL;
			var commentaire : String = inputComm.text;
			var date : String = DateFunction.formatDateAsInverseString(calendar.selectedDate);
			//Alert.show("SAV : Terminal :"+ligneSelected.IMEI+" mis au rebus. Cause : "+idCause+" Commentaire : "+commentaire+" date : "+date );
			//fermer(null);
			//requête
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"putSAVEEquip_term",
																		savTerminal_handler);
			RemoteObjectUtil.callService(op,idTerm,ligneSelected.MODELE+"("+ligneSelected.IMEI+")",date,commentaire,idCause);
		}
		
		private function savTerminal_handler (evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				ConsoviewAlert.afficherOKImage("Mise en service après vente du terminal effectuée");
				PopUpManager.removePopUp(this);
				
			}
			else
			{
				trace("Erreur getPoolGestionnaire");
			}
		}
	}
}