package univers.inventaire.equipements.gestionflottemobile.comportementaffectation
{
	public interface IComportementAffectation
	{
		//Pour une affectation il faut 5 paramètres, dont les Ids item 1 et 2, les libelles item 1 et 2, la date et le statut pret (1 si preté et 0 si non) par défaut à 0.
		function affecter(id1 : int,id2:int,lib1 : String, lib2 : String, date : String, pret : int):void;
		function annuler():void;
	}
}