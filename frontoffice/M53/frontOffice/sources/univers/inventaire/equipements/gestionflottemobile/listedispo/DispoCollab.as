package univers.inventaire.equipements.gestionflottemobile.listedispo
{
	import composants.util.employes.Collaborateur;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	//import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	public class DispoCollab extends DispoCollabIHM implements IListeDispo
	{
		[Embed(source="/assets/equipement/User 3.gif")] 
		private var adrImg : Class;
		[Bindable]
		private var dataListeCollab : ArrayCollection = new ArrayCollection();
		public function DispoCollab()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			
		}
		private function initIHM(evt : FlexEvent):void
		{
			initData();
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
		}
		public function getIDSelection():int
		{
			if(dgListeCollab.selectedIndex!=-1){
			return dgListeCollab.selectedItem.IDGROUPE_CLIENT; 
			}
			else
			{
				return -1;
			}
		}
		public function getDataGrid():DataGrid
		{
			return dgListeCollab;
		}
		public function getLibelleSelection():String
		{
			if(dgListeCollab.selectedIndex!=-1){
				return dgListeCollab.selectedItem.NOMPRENOM;
			}
			else
			{
				return "Aucune sélection";
			}
		}
		public function getLibelleType():String
		{
			return "Liste des collaborateurs";
		}
		public function getSourceIcone():Class
		{
			return adrImg;
		}
		public function libelleSelection():String
		{
			if(dgListeCollab.selectedIndex!=-1){
				return dgListeCollab.selectedItem.NOMPRENOM;
			}
			else
			{
				return "Aucune sélection";
			}
		}
		private function initData():void
		{
			
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
			
			 var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.employes.EmployesUtils",
							"fournirListeEmployes",
							initResultHandler);
							
			RemoteObjectUtil.callService(opData,PanelGestionFoltteMobile.idPoolGestionnaire,"");				
		}
		private function initResultHandler(re : ResultEvent):void{
			if (re.result)
			{
				if(dataListeCollab) dataListeCollab = null; 
				dataListeCollab = new ArrayCollection();
	    
	    		var cursor : IViewCursor = (re.result as ArrayCollection).createCursor();
	    		while(!cursor.afterLast)
	    		{	
	    			var employeObj:Collaborateur = new Collaborateur();	    			
	    			employeObj.NOMPRENOM = (cursor.current.NOM != null)?cursor.current.NOM:'';
	    			employeObj.NOM = employeObj.NOMPRENOM;
	    			employeObj.IDEMPLOYE = cursor.current.IDGROUPE_CLIENT;
	    			employeObj.IDGROUPE_CLIENT = cursor.current.IDGROUPE_CLIENT;
	    			employeObj.MATRICULE = cursor.current.MATRICULE;
	    			employeObj.CLE_IDENTIFIANT = cursor.current.CLE_IDENTIFIANT;
	    			employeObj.INOUT = 	cursor.current.INOUT; 
	    			   			
	    			if(employeObj.INOUT > 0)
	    			{
	    				dataListeCollab.addItem(employeObj);	
	    			}
	    			cursor.moveNext();
	    		}	
	    		
	    		dgListeCollab.dataProvider = dataListeCollab;
				dataListeCollab.refresh();
	    	}
		}
		
		private function txtFiltreChangeHandler(ev : Event):void
		{
			if(dataListeCollab)
			{
				dataListeCollab.filterFunction = filtrerLeGrid;
				dataListeCollab.refresh();
			}
		}
		private function filtrerLeGrid(item : Object):Boolean
		{
			if((String(item.NOMPRENOM).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
	}
}