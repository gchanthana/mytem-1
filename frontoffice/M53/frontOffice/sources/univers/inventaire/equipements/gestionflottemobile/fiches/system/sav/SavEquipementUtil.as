package univers.inventaire.equipements.gestionflottemobile.fiches.system.sav
{
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	[Bindable]   
	public class SavEquipementUtil extends EventDispatcher
	{
		
		//Valeur selectionnée + ecriture dans tmp
		private var _ticketSav : Object;
		private var _tmpValues:Object;
		private var _intervention:Object;
		private var _tmpIntervention:Object;
		
				
		//Collection des dg et des cmb
		public var _listeMotifIntervention : ArrayCollection = new ArrayCollection();
		public var _listeTypeIntervention : ArrayCollection = new ArrayCollection();
		public var _listeSite : ArrayCollection = new ArrayCollection();
		public var _listeTicketOfEquip : ArrayCollection = new ArrayCollection();
		
		//Variable utilisées pour selectionné un élément dans les cmb
		public var _indexSiteOfTicketSelected : int = -1;
		public var _indexMotifInterventionSelected : int = -1;
		public var _indexTypeInterventionSelected : int = -1;
		
		
		public function SavEquipementUtil() 
		{
		}
		public function getInfosTicket(obj_ticket : 	Ticket):void
		{
			//selectedTicket = obj_ticket;
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.savEquipement",
							"getTicketInfo",getInfosTicket_handler);
			RemoteObjectUtil.callService(opData,obj_ticket.IDSAV);
		}
		protected function getInfosTicket_handler(event:ResultEvent):void
		{
			if(event.result)
			{
				if((event.result as ArrayCollection).length > 0)
				{
					selectedTicket = new Ticket();	
					selectedTicket.IDSAV = event.result[0].ID_SAV;
					selectedTicket.IDEQUIPEMENT = event.result[0].IDEQUIPEMENT;
					selectedTicket.IDCONTRAT = Number(event.result[0].IDCONTRAT);
					selectedTicket.DATE_DEB = event.result[0].DATE_DEBUT;
					selectedTicket.DATE_FIN = event.result[0].DATE_ECHEANCE;
					selectedTicket.GARANTIE_APPLICABLE = event.result[0].GARANTIE_APPLICABLE;
					selectedTicket.IDFOURNISSEUR =event.result[0].IDFOURNISSEUR;
					selectedTicket.IDSITE = event.result[0].IDSITE;
					selectedTicket.IDUSER = event.result[0].IDUSER;
					selectedTicket.COMMENTAIRES =event.result[0].COMMENTAIRES;
					selectedTicket.NUMERO_SAV = event.result[0].NUMERO_TICKET;
										
					tmpValues = ObjectUtil.copy(selectedTicket);
					setIndexOfSiteSelected();
					selectedIntervention = new Intervention(selectedTicket.IDSAV);
					tmpIntervention = new Intervention(selectedTicket.IDSAV);
				}
			}
			
			var values : ArrayCollection = event.result as ArrayCollection
			if (values != null)
	    	{
	    		var cursor : IViewCursor = values.createCursor();
	    		while(!cursor.afterLast){
		    		if (cursor.current.IDINTERVENTIONS  != null)//La ligne 0 peut contenir une intervention vide :(
		    		{	    			 
						var obj_intervention:Intervention = new Intervention();	    		
						obj_intervention.IDINTERVENTION = cursor.current.IDINTERVENTIONS;
						obj_intervention.IDSAV = cursor.current.ID_SAV;
						obj_intervention.IDMOTIF= cursor.current.IDMOTIF_INTERVENTION;
						obj_intervention.IDTYPE= cursor.current.IDTYPE_INTERVENTION;
						obj_intervention.LIBELLE= cursor.current.LIBELLE_INTERVENTION;
						obj_intervention.REFERENCE= cursor.current.REFERENCE_INTERVENTION;
						obj_intervention.CODE_INTERNE= cursor.current.CODE_INTERNE_INTERVENTION;
						obj_intervention.DATE_DEB= cursor.current.DEBUT_INTERVENTION;
						obj_intervention.DATE_FIN= cursor.current.FIN_INTERVENTION;
						obj_intervention.DUREE= cursor.current.DUREE;
						obj_intervention.COUT_INTERVENTION= cursor.current.COUT_INTERVENTION;
						obj_intervention.COMMENTAIRE= cursor.current.COMMENTAIRE_INTERVENTION;
						obj_intervention.LIBELLE_TYPE = cursor.current.TYPE_INTERVENTION;
						obj_intervention.LIBELLE_MOTIF = cursor.current.LIBELLE_MOTIF;
						
						(selectedTicket as Ticket).INTERVENTIONS.addItem(obj_intervention);
						
		    		}
		    		cursor.moveNext();
				}
				//Alert.show(ObjectUtil.toString((selectedTicket as Ticket).INTERVENTIONS));
			}
			
			else
			{
				Alert.show("Erreur lors de la récupération des interventions","Erreur");									
			}
				
		}
		public function createTicket(obj_ticket : Ticket):void
		{
			var date : String = DateFunction.formatDateAsInverseString(obj_ticket.DATE_DEB);
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.savEquipement",
							"createSAV", createSAVResultHandler	);
			RemoteObjectUtil.callService(opData,obj_ticket.IDEQUIPEMENT,
												obj_ticket.IDCONTRAT,
												date,
												obj_ticket.DESCRIPTION,
												obj_ticket.GARANTIE_APPLICABLE,
												obj_ticket.IDFOURNISSEUR,
												obj_ticket.IDSITE,
												obj_ticket.IDEMPLOYE,
												obj_ticket.COMMENTAIRES,
												obj_ticket.NUMERO_SAV
												);
		}
		private function createSAVResultHandler(event:ResultEvent):void
		{
			if(event.result > 0)
			{
					ConsoviewAlert.afficherOKImage("Informations sauvegardées");
				//getTicketOfEquip(selectedTicket.IDEQUIPEMENT);
				dispatchEvent(new Event(MAJINFOSSAV));
			}
			else
			{
				Alert.show("Erreur lors de la sauvegarde","Erreur");									
			}
		}
		public function updateTicket(obj_ticket : Object,fermer : int = 0):void
		{
			var date : String = DateFunction.formatDateAsInverseString(obj_ticket.DATE_DEB);
			var idContrat : int = obj_ticket.IDCONTRAT;
		
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.savEquipement",
							"updateSAV", updateSAVResultHandler	);
				RemoteObjectUtil.callService(opData,obj_ticket.IDSAV,
												obj_ticket.IDEQUIPEMENT,
												idContrat,
												date,
												obj_ticket.DESCRIPTION,
												obj_ticket.GARANTIE_APPLICABLE,
												obj_ticket.IDFOURNISSEUR,
												obj_ticket.IDSITE,
												obj_ticket.COMMENTAIRES,
												obj_ticket.NUMERO_SAV,
												obj_ticket.IDEMPLOYE,
												fermer											
												);
		
		}
		public static const MAJINFOSSAV:String = "majSAV";
		private function updateSAVResultHandler(event:ResultEvent):void
		{
			if(event.result > 0)
			{
				//ConsoviewAlert.afficherOKImage("Informations modifiées");
				ConsoviewAlert.afficherOKImage("Informations modifiées");
				dispatchEvent(new Event(MAJINFOSSAV));
				
			}
			else
			{
				Alert.show("Erreur lors de la sauvegarde","Erreur");									
			}
		}
		public function getTicketOfEquip(idEquip : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.savEquipement",
							"getTicketOfEquip",getTicketHandler);
			RemoteObjectUtil.callService(opData,idEquip);
		}
		private function getTicketHandler(event:ResultEvent):void
			{
				_listeTicketOfEquip.removeAll();
				
				var values : ArrayCollection = event.result as ArrayCollection
				if (values != null)
		    	{
		    		var cursor : IViewCursor = values.createCursor();
		    		while(!cursor.afterLast){
		    			 
					var obj_ticket:Ticket = new Ticket();	    			
    			
					obj_ticket.IDSAV = cursor.current.ID_SAV;
					obj_ticket.NUMERO_SAV = cursor.current.NUMERO_TICKET;
					obj_ticket.DATE_DEB = cursor.current.DATE_DEBUT;
					obj_ticket.IDFOURNISSEUR = cursor.current.IDREVENDEUR;
					
					if(cursor.current.DATE_FIN !=null)
					{
						obj_ticket.DATE_FIN = cursor.current.DATE_FIN;
						obj_ticket.OUVERT = 0;
					}
					   			
	    			_listeTicketOfEquip.addItem(obj_ticket);	
		    		cursor.moveNext();
		    		}
		    	}
				else
				{
					Alert.show("Erreur lors de la récupération des tickets","Erreur");									
				}
		}
		public static const ADDINTERVENTION:String = "addInterventionsSucced";
		
		public function addIntervention(obj_intervention : Intervention):void
		{
			var dateDeb : String = DateFunction.formatDateAsInverseString(obj_intervention.DATE_DEB);
			var dateFin : String = DateFunction.formatDateAsInverseString(obj_intervention.DATE_FIN);
			var duree :Number = DateFunction.dateDiff("d",obj_intervention.DATE_FIN,obj_intervention.DATE_DEB);
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.savEquipement",
							"addIntervention",addInterventionHandler);
			RemoteObjectUtil.callService(opData,obj_intervention.IDSAV,
												obj_intervention.IDMOTIF,
												obj_intervention.IDTYPE,
												obj_intervention.LIBELLE,
												obj_intervention.REFERENCE,
												obj_intervention.CODE_INTERNE, 
												dateDeb,
												dateFin,
												duree,
												obj_intervention.COUT_INTERVENTION,
												obj_intervention.COMMENTAIRE
												);
		}
		private function addInterventionHandler(event:ResultEvent):void
		{
			if(event.result > 0)
			{
				//dispatchEvent(new Event(ADDINTERVENTION));
				dispatchEvent(new Event(UPDATEINTERVENTION));
			}
			else
			{
				Alert.show("Erreur lors de l'ajout d'une intervention","Erreur");									
			}
		}
		public static const UPDATEINTERVENTION:String = "updateInterventionsSucced";
		public function updateIntervention(obj_intervention : Object):void
		{
			var dateDeb : String = DateFunction.formatDateAsInverseString(obj_intervention.DATE_DEB);
			var dateFin : String = DateFunction.formatDateAsInverseString(obj_intervention.DATE_FIN);
			var duree :Number = DateFunction.dateDiff("d",obj_intervention.DATE_FIN,obj_intervention.DATE_DEB);
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.savEquipement",
							"updateIntervention",updateInterventionHandler);
			RemoteObjectUtil.callService(opData,obj_intervention.IDINTERVENTION,
												obj_intervention.IDSAV,
												obj_intervention.IDMOTIF,
												obj_intervention.IDTYPE,
												obj_intervention.LIBELLE,
												obj_intervention.REFERENCE,
												obj_intervention.CODE_INTERNE, 
												dateDeb,
												dateFin,
												duree,
												obj_intervention.COUT_INTERVENTION,
												obj_intervention.COMMENTAIRE
											
												);

		}
		private function updateInterventionHandler(event:ResultEvent):void
		{
			if(event.result > 0)
			{
				dispatchEvent(new Event(UPDATEINTERVENTION));
				//Alert.show("");
			}
			else
			{
				Alert.show("Erreur lors de la modifiaction d'une intervention","Erreur");									
			}
		}
		
		public function getTypeIntervention():void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.savEquipement",
							"getTypeIntervention", 
							function getTypeInterventionHandler(event:ResultEvent):void
							{
								if(event.result)
								{
									_listeTypeIntervention = event.result as ArrayCollection;
								}
								else
								{
									Alert.show("Erreur lors de la récupération des types d'intervention","Erreur");									
								}
							}
							);
			RemoteObjectUtil.callService(opData);
		}
		
		public function getMotifIntervention():void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.savEquipement",
							"getMotifIntervention", 
							function getMotifInterventionHandler(event:ResultEvent):void
							{
								if(event.result)
								{
									_listeMotifIntervention = event.result as ArrayCollection;
								}
								else
								{
									Alert.show("Erreur lors de la récupération des modifs d'intervention","Erreur");									
								}
							}
							);
			RemoteObjectUtil.callService(opData);
		}

		public function putEquipActive(idEquipement : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.savEquipement",
							"putEquipActive",putEquipActiveHandler);
							
			RemoteObjectUtil.callService(opData,idEquipement);
		}
		private function putEquipActiveHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				ConsoviewAlert.afficherOKImage("Retour de SAV enregistré");
				dispatchEvent(new Event(PanelGestionFoltteMobile.VALIDER_EVENT));
			}
			else
			{
				Alert.show("Erreur lors de la cloture du ticket","Erreur");									
			}
		}
							
		public function getSiteOfPool():void
		{
			var op :AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
					"getSite_pool",
					function getSiteOfPoolHandler(event:ResultEvent):void
					{
						if(event.result)
						{
							_listeSite = event.result as ArrayCollection;
							
						}
						else
						{
							Alert.show("Erreur lors de la récupération des sites","Erreur");									
						}
					}
					);
			RemoteObjectUtil.callService(op ,PanelGestionFoltteMobile.idPoolGestionnaire);
		}

		public function set selectedTicket(value:Object):void
		{
			_ticketSav = value;
		}
		public function get selectedTicket():Object
		{
			return _ticketSav;
		}
		public function set tmpValues(value:Object):void
		{
			_tmpValues = value;
		}
		public function get tmpValues():Object
		{
			return _tmpValues;
		}
		public function set selectedIntervention(value:Object):void
		{
			_intervention = value;
		}
		
		public function get selectedIntervention():Object
		{
			return _intervention;
		}
		public function set tmpIntervention(value:Object):void
		{
			_tmpIntervention = value;
		}
		public function get tmpIntervention():Object
		{
			return _tmpIntervention;
		}
		private function setIndexOfSiteSelected ():void
		{
			for(var i:int = 0; i<_listeSite.length;i++)
			{
				if(_listeSite.getItemAt(i).IDSITE_PHYSIQUE==selectedTicket.IDSITE)
				{
					_indexSiteOfTicketSelected =i;
				}
			}
		}
		public function createNewTicketNumber(idFournisseur:int):void
		{
			_tmpValues = new Ticket(idFournisseur);  
			_tmpValues.addEventListener(Ticket.SAVUNIQUENUMERO,createNewTicketHandler);
		}
		private function createNewTicketHandler(evt : Event):void
		{
			selectedTicket = ObjectUtil.copy(_tmpValues);
		}
	}
}