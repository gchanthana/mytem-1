package univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile
{
	import mx.events.FlexEvent;
	import mx.controls.Image;
	import flash.display.Loader;
	
	public class MenuFlotteMobDefault extends MenuGestionFlotteMobileIHM
	{
		/*[Embed(source="/assets/equipement/Ligne.png")] 
		private var adrImg : Class;*/
		
		public static const BT_HISTORIQUE_CLIKED:String = "btHistoriqueCliked";
		
		public function MenuFlotteMobDefault()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(fe : FlexEvent):void
		{
			/*var imgNew : Image = new Image();
			imgNew.source=adrImg;
			canvas_icone.addChild(imgNew);*/
			
			bt_new_user.initParametre(false);
			bt_new_tel.initParametre(false);
			bt_new_num.initParametre(false);
			bt_new_sim.initParametre(false);
			
			bt_sup_user.initParametre(false);
			bt_sup_tel.initParametre(false);
			bt_sup_num.initParametre(false);
			bt_sup_sim.initParametre(false);
			
			bt_mod_sav.initParametre(false);	
			bt_mod_renouv.initParametre(false);
			bt_mod_preter.initParametre(false);
			bt_mod_rebus.initParametre(false);
			btGetHistorique.initParametre(false);
		}
	}
}