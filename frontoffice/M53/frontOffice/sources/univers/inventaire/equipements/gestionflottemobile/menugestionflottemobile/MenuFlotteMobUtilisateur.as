package univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Image;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	import univers.inventaire.equipements.gestionflottemobile.cellule.Cellule;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.AffecterSimCollab;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.AffecterTermCollab;
	import univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation.DesaffecterSIMofCollab;
	import univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation.DesaffecterTermOfCollab;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoSim;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoTerm;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.listeparcollab.ListeSimOfCollab;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.listeparcollab.ListeTermOfCollab;
	import univers.inventaire.equipements.gestionflottemobile.panelaffectation.PanelAffectation;
	import univers.inventaire.equipements.gestionflottemobile.paneldesaffectationequipofcollab.PanelDesaffectationOfCollab;
	import univers.inventaire.equipements.gestionflottemobile.rebus.RebusCollab;
	
	[Event(name="btHistoriqueCliked",type="flash.events.MouseEvent")]
	
	public class MenuFlotteMobUtilisateur extends MenuGestionFlotteMobileIHM
	{
		public static const BT_HISTORIQUE_CLIKED:String = "btHistoriqueCliked";
		
		[Embed(source="/assets/equipement/User 3.gif")] 
		private var adrImg : Class;
		[Embed(source='/assets/flotteMobile/Color/Collaborateur-rebus.jpg')] 
		private var adrRebusCollab : Class;
		[Embed(source='/assets/equipement/minilogo_mobiles/icCollaborateur-rebus.jpg')] 
		private var small_adrRebusCollab : Class;
		private var panelGestionFlotteMobile  : PanelGestionFoltteMobile;
		private var popup : TitleWindow;
		private var stateToolTip : Boolean = stateToolTip;
			
		public function MenuFlotteMobUtilisateur(panelGestionFlotteMobile  : PanelGestionFoltteMobile,stateToolTip : Boolean = false )
		{
			super();
			this.stateToolTip = stateToolTip;
			this.panelGestionFlotteMobile=panelGestionFlotteMobile;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(fe : FlexEvent):void
		{
			if(stateToolTip==true)
			{
				bt_new_user.visible = false;
				bt_new_user.width=0;
				bt_new_user.height=0;
				
				bt_sup_user.visible = false;
				bt_sup_user.width=0;
				bt_sup_user.height=0;
				
				bt_mod_sav.visible = false;
				bt_mod_sav.width=0;
				bt_mod_sav.height=0;
			
				bt_mod_renouv.visible = false;
				bt_mod_renouv.width=0;
				bt_mod_renouv.height=0;
			
				bt_mod_preter.visible = false;
				bt_mod_preter.width=0;
				bt_mod_preter.height=0;
				
				this.setCurrentState("toolTipState");
				labToolTip = panelGestionFlotteMobile.objMenuFlotteMobUtilisateur_fixe.labToolTip;
			}
			
			var imgNew : Image = new Image();
			imgNew.source=adrImg;
			canvas_icone.addChild(imgNew);
			
			bt_new_user.initParametre(false);	
			bt_new_tel.initParametre(true,"Fournir un terminal à ");	
			bt_new_num.initParametre(true,"Fournir une ligne/SIM à ");
			bt_new_sim.initParametre(true,"Fournir une ligne/SIM à ");
			
			
			bt_sup_user.initParametre(false);	
			bt_sup_tel.initParametre(true,"Reprendre le terminal de ");	
			bt_sup_num.initParametre(true,"Reprendre ligne/SIM de ");
			bt_sup_sim.initParametre(true,"Reprendre ligne/SIM de ");
			
			bt_mod_sav.initParametre(false);	
			bt_mod_renouv.initParametre(false);	
			bt_mod_preter.initParametre(false)/* ,"Gestion des prêts de ") */;
			bt_mod_rebus.initParametre(true,"Départ de l'entreprise de ");
			btGetHistorique.initParametre(true,"Historique de ");
			
			//bt_mod_preter.initParametre(false);
			//bt_mod_rebus.initParametre(false);
			
			bt_new_tel.addEventListener(MouseEvent.CLICK,newPanelListeTerminal);
			bt_new_num.addEventListener(MouseEvent.CLICK,newPanelListeNDI);
			bt_new_sim.addEventListener(MouseEvent.CLICK,newPanelListeSIM);
			
			bt_sup_tel.addEventListener(MouseEvent.CLICK,newPanelListeTerminalParCollab);
			bt_sup_num.addEventListener(MouseEvent.CLICK,newPanelListeNDIParCollab);
			bt_sup_sim.addEventListener(MouseEvent.CLICK,newPanelListeSIMParCollab);
			
			btGetHistorique.addEventListener(MouseEvent.CLICK,_btGetHistoriqueClickHandler);
			
			if(stateToolTip==true)
			{
				bt_mod_rebus.setStyle("icon",small_adrRebusCollab); // Changement exeptionnel (la poubelle n'est pas adaptée pour un collab)
			}
			else
			{
				bt_mod_rebus.setStyle("icon",adrRebusCollab); // Changement exeptionnel (la poubelle n'est pas adaptée pour un collab)
			}
			bt_mod_rebus.addEventListener(MouseEvent.CLICK,newPanelRebusCollab);

		}
		private function newPanelRebusCollab(event : MouseEvent):void
		{
			addPopup(new RebusCollab(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE),PanelGestionFoltteMobile.objLigneSelected.COLLABORATEUR,adrImg,"collaborateur")
									));
		}
		private function newPanelListeTerminal(event : MouseEvent):void
		{
			addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE),PanelGestionFoltteMobile.objLigneSelected.COLLABORATEUR,adrImg,"collaborateur"),
									new AffecterTermCollab(),
									new DispoTerm(true),
									true,//Les prêts sont autorisés
									false,//La case préter n'est pas précochée
									false//L'ordre des paramêtre n'est pas celui par default
									));
		}
		private function newPanelListeTerminalParCollab(event : MouseEvent):void
		{
			addPopup(new PanelDesaffectationOfCollab(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE),PanelGestionFoltteMobile.objLigneSelected.COLLABORATEUR,adrImg,"collaborateur"),
									new DesaffecterTermOfCollab(),
									new ListeTermOfCollab(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE))
									));
		}
		private function newPanelListeNDI(event : MouseEvent):void
		{
			addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE),PanelGestionFoltteMobile.objLigneSelected.COLLABORATEUR,adrImg,"collaborateur"),
									new AffecterSimCollab(),
									new DispoSim(),
									true,//Les prêts sont autorisés
									false,//La case préter n'est pas précochée
									false//L'ordre des paramêtre n'est pas celui par default
									));
		}
		private function newPanelListeNDIParCollab(event : MouseEvent):void
		{
			addPopup(new PanelDesaffectationOfCollab(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE),PanelGestionFoltteMobile.objLigneSelected.COLLABORATEUR,adrImg,"collaborateur"),
									new DesaffecterSIMofCollab(),
									new ListeSimOfCollab(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE))
									));
		}
		private function newPanelListeSIM(event : MouseEvent):void
		{
			addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE),PanelGestionFoltteMobile.objLigneSelected.COLLABORATEUR,adrImg,"collaborateur"),
									new AffecterSimCollab(),
									new DispoSim(),
									true,//Les prêts sont autorisés
									false,//La case préter n'est pas précochée
									false//L'ordre des paramêtre n'est pas celui par default
									));
		}
		private function newPanelListeSIMParCollab(event : MouseEvent):void
		{
			addPopup(new PanelDesaffectationOfCollab(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE),PanelGestionFoltteMobile.objLigneSelected.COLLABORATEUR,adrImg,"collaborateur"),
									new DesaffecterSIMofCollab(),
									new ListeSimOfCollab(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE))
									));
		}
		private function addPopup(popup : TitleWindow):void {
			this.popup=popup;
			
			popup.addEventListener(PanelGestionFoltteMobile.VALIDER_EVENT,closePopUp); // Si il y a une validation
			popup.addEventListener(PanelGestionFoltteMobile.ANNULER_EVENT,closePopUp); // Si il y a une annulation
			PopUpManager.addPopUp(popup,panelGestionFlotteMobile,true);
			PopUpManager.centerPopUp(popup);
		}
		private function closePopUp(evt : Event):void {
			if(evt.type == PanelGestionFoltteMobile.ANNULER_EVENT)
			{
				PopUpManager.removePopUp(popup); 
			}
			else if(evt.type == PanelGestionFoltteMobile.VALIDER_EVENT) 
			{
				panelGestionFlotteMobile.actualise(); 
				PopUpManager.removePopUp(popup);
			}
		}

		protected function _btGetHistoriqueClickHandler(event:MouseEvent):void
		{
			dispatchEvent(new MouseEvent(BT_HISTORIQUE_CLIKED));
		}
	}
}