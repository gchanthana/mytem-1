package univers.inventaire.equipements.gestionflottemobile.comportementaffectation
{
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class AffecterSimTerm implements IComportementAffectation
	{
		public function AffecterSimTerm()
		{
			
		}
		
		public function affecter(idSIM : int, idTerm : int, libelle_SIM : String, libelle_term : String, date : String, pret :int):void
		{
			//Alert.show("AffecterSimTerm : "+idSIM+"--"+idTerm);
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"affectEquipementToSim_V3",
																		affecter_handler);
			RemoteObjectUtil.callService(op,idSIM,idTerm,libelle_SIM,libelle_term,date);
		}
		private function affecter_handler (evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				ConsoviewAlert.afficherOKImage("Affectation de la SIM au terminal effectuée");
			}
			else
			{
				trace("Erreur getPoolGestionnaire");
			}
		}
		public function annuler():void
		{
			Alert.show("fermer");
		}
		

	}
}