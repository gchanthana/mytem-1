package univers.inventaire.equipements.gestionflottemobile.listedispo.listeparcollab
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	import mx.utils.UIDUtil;
	
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoTermIHM;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.IListeDispo;
	
	public class ListeTermOfCollab extends DispoTermIHM implements IListeDispo
	{
		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")] 
		private var adrImg : Class;
		private var dataListeTerm : ArrayCollection = new ArrayCollection();
		private var idCollab : int;
		
		public function ListeTermOfCollab (idCollab : int)
		{
			
			this.idCollab = idCollab;
		
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt : FlexEvent):void
		{
			initData();
		}
		public function getIDSelection():int
		{
			return dgListeTerm.selectedItem.IDTERMINAL 
		}
		public function getDataGrid():DataGrid
		{
			return dgListeTerm;
		}
		
		public function getLibelleType():String
		{
			return "Liste des terminaux ";
		}
		public function getSourceIcone():Class
		{
			return adrImg;
		}
		public function getLibelleSelection():String
		{
			if(dgListeTerm.selectedIndex!=-1){
				return dgListeTerm.selectedItem.IMEI;
			}
			else
			{
				return "Aucune sélection";
			}
		}
		private function initData():void
		{
			
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
   		 
			var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"getAffectedTermToEmp",
							initResultHandler);
						
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,idCollab);		
		}
		
		private function initResultHandler(re : ResultEvent):void{
			if (re.result){
				dataListeTerm = re.result as ArrayCollection;
				dgListeTerm.dataProvider = dataListeTerm;
				dataListeTerm.refresh();
				trace("Terminal disponibles :"+ObjectUtil.toString(re.result));
			}		
		}
	}
}