package univers.inventaire.equipements.gestionflottemobile.paneldesaffectation
{
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	import univers.inventaire.equipements.gestionflottemobile.cellule.Cellule;
	import univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation.IComportementDesaffectation;
	
	public class PanelDesaffectation extends PanelDesaffectationIHM
	{
		private var obj_cellule:Cellule;
		private var obj_cellule_a_desaffecter:Cellule;
		private var obj_comportement : IComportementDesaffectation;
		
		
		public function PanelDesaffectation(obj_cellule:Cellule,obj_cellule_a_desaffecter : Cellule, obj_comportement : IComportementDesaffectation)
		{
			this.obj_cellule = obj_cellule;//La cellule séléctionnée dans l'écran principal
			this.obj_cellule_a_desaffecter = obj_cellule_a_desaffecter;//La cellule à desaffecter
			this.obj_comportement = obj_comportement;//Les précédure à appeler
			
			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM)
		}
		private function initIHM(evt : FlexEvent):void
		{
			img_cellule.source = obj_cellule.image_cellule; //Afficher l'icone de la cellule séléctionné
			img_liste.source=obj_cellule_a_desaffecter.image_cellule;
			lab_cellule_name.text = obj_cellule.libelle_cellule;//Affichage du nom de la cellule séléctionné
			labCelluleAdesaffecter.text = obj_cellule_a_desaffecter.libelle_cellule;
			lab_titre.text="Désaffectation "+obj_cellule_a_desaffecter.type_cellule+": "+obj_cellule_a_desaffecter.libelle_cellule;//Affichage du type d'objet de la liste
						
			calendar.selectedDate = new Date(); // Selection de la date actuel dans la date du jour
			
			btAnnuler.addEventListener(MouseEvent.CLICK,fermer);
			btValider.addEventListener(MouseEvent.CLICK,valider);
			addEventListener(CloseEvent.CLOSE,fermer);
		}
		private function valider(evt : MouseEvent):void
		{
			obj_comportement.desaffecter(obj_cellule.id_cellule,obj_cellule_a_desaffecter.id_cellule,obj_cellule.libelle_cellule,obj_cellule_a_desaffecter.libelle_cellule,DateFunction.formatDateAsInverseString(calendar.selectedDate));
			dispatchEvent(new Event(PanelGestionFoltteMobile.VALIDER_EVENT));
		}
		private function fermer(evt : Event):void 
		{
			dispatchEvent(new Event(PanelGestionFoltteMobile.ANNULER_EVENT));
		}
		
		
		

	}
}