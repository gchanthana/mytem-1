package univers.inventaire.equipements.gestionflottemobile.fiches.system
{
	[Bindable]
	public class FicheTerminale implements IFiche
	{
		private var _tmpData:Object;
		private var _ficheTerminaleVo:Object;
		
		public function FicheTerminale() 
		{
		}
		
		public function getDate():void
		{
			
		}
		
		public function updateDate():void
		{
			
		}

		public function set tmpData(value:Object):void
		{
			_tmpData = value;
		}

		public function get tmpData():Object
		{
			return _tmpData;
		}

		public function set ficheTerminaleVo(value:Object):void
		{
			_ficheTerminaleVo = value;
		}

		public function get ficheTerminaleVo():Object
		{
			return _ficheTerminaleVo;
		}
	}
}