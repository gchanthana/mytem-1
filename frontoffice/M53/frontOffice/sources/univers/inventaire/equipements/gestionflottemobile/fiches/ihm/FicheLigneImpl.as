package univers.inventaire.equipements.gestionflottemobile.fiches.ihm
{
	import composants.controls.TextInputLabeled;
	import composants.tb.effect.EffectProvider;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	import composants.util.lignes.LignesUtils;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class FicheLigneImpl extends TitleWindow
	{
		public var btVerifierNumLigne:Button;
		public var btValider:Button;
		public var txtNumLigne:TextInputLabeled;
		public var cbDureeContrat:ComboBox;		
		public var cbDureeEligibilite:ComboBox;
		public var dcFPC:CvDateChooser;
		public var dcDateOuverture:CvDateChooser;
		public var dcDateRenouv:CvDateChooser;
		public var dcEligibilite:CvDateChooser;
		
		private var boolLigneChange:Boolean = false;
		private var _editabel:Boolean;
		
		public var lignesUtils:LignesUtils = new LignesUtils();
		
		public function FicheLigneImpl()
		{
			super();
		}
		
		public function set selectedLigne(value:Object):void
		{
			lignesUtils.selectedLigne = value;
		}
		
		public function get selectedLigne():Object
		{
			return lignesUtils.selectedLigne;
		}
		
		public function set editabel(value:Boolean):void
		{
			_editabel = value;
		}
		
		public function get editabel():Boolean
		{
			return _editabel;
		}
		
		private function getData():void
		{			 
			lignesUtils.getInfosFicheSousTete();
			lignesUtils.addEventListener(LignesUtils.GETINFOSFICHESOUSTETEOK,lignesUtilsInfosLigneHandler);
		}
		
		private function updateData():void
		{	 
			var boolSim:Boolean = true;
			var boolLigne:Boolean = true;
			var errorMsg:String = "\n";
			if(lignesUtils.selectedLigne.IDSIM > 0 )
			{
				if(lignesUtils.tmpLigne.NUM_SIM && String(lignesUtils.tmpLigne.NUM_SIM).length > 0)
				{
					boolSim = true;
				}
				else
				{
					boolSim = false;
					errorMsg += "- le numéro de carte sim est obligatoire\n";	
				}
			}
			else
			{
				boolSim = true;
			}
			
			if(lignesUtils.selectedLigne.IDSOUS_TETE > 0)
			{
				if(lignesUtils.tmpLigne.SOUS_TETE && String(lignesUtils.tmpLigne.SOUS_TETE).length > 0)
				{
					boolLigne = true;	
				}
				else
				{
					boolLigne = false;	
					errorMsg += "- le numéro de ligne est obligatoire\n";
				}	
			}
			else
			{
				boolLigne = true;
			}
						
			
			if(boolLigne && boolSim)
			{
				lignesUtils.addEventListener(LignesUtils.UPDATEINFOSFICHESOUSTETEOK,updateDataHandler);
				lignesUtils.upDateInfosFicheSousTete();	
			}
			else
			{
				Alert.okLabel = "OK";
				Alert.show(errorMsg,"Erreur");
			}
			
		}
		
		protected function updateDateFPC():void
		{
			try
			{
				//si il y a une date de renouvellement on prend cette date pour calculer date FPC				
				if(dcDateRenouv.selectedDate != null)
				{
					
					dcFPC.selectedDate = DateFunction.dateAdd("M",dcDateRenouv.selectedDate,lignesUtils.tmpLigne.DUREE_CONTRAT);
					dcEligibilite.selectedDate = DateFunction.dateAdd("M",dcDateRenouv.selectedDate,lignesUtils.tmpLigne.DUREE_ELLIGIBILITE);
				}
				else
				{
					dcFPC.selectedDate = DateFunction.dateAdd("M",dcDateOuverture.selectedDate,lignesUtils.tmpLigne.DUREE_CONTRAT);
					dcEligibilite.selectedDate = DateFunction.dateAdd("M",dcDateOuverture.selectedDate,lignesUtils.tmpLigne.DUREE_ELLIGIBILITE);
				}
			}
			catch(e : Error)
			{
				
			}
		}
		
		///////////////////// HANDLERS /////////////////////////////////////////////////////////////////
		protected function _localeCloseHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
			
		}

		protected function _localeCreationCompleteHandler(event:FlexEvent):void
		{
			dcDateOuverture.addEventListener(CalendarLayoutChangeEvent.CHANGE,_dcDateOuvertureChangeHandler);
			dcDateRenouv.addEventListener(CalendarLayoutChangeEvent.CHANGE,_dcDateRenouvChangeHandler);
			
			getData();
		}
		
		protected function txtNumLIgneChangeHandler(event:Event):void
		{
			if(ConsoviewUtil.isPresent(txtNumLigne.text,lignesUtils.listeNumerosValides))
			{
				lignesUtils.boolOK = true;
				btVerifierNumLigne.visible = false;
				btVerifierNumLigne.width = 0;
				return;
			}
			else
			{
				lignesUtils.boolOK = false;
				btVerifierNumLigne.visible = true;
				btVerifierNumLigne.width = 80;
				EffectProvider.FadeThat(btVerifierNumLigne);	
			}
			
			lignesUtils.tmpLigne.SOUS_TETE = txtNumLigne.text;	
			boolLigneChange = true;
						
		}
		
		protected function lignesUtilsInfosLigneHandler(event:Event):void
		{
			var boolLigne:Boolean = (lignesUtils.selectedLigne.IDSOUS_TETE > 0);
			var boolSim:Boolean =  (lignesUtils.selectedLigne.IDSIM > 0);
			
			if(boolLigne && boolSim)
			{
				currentState = ""
			}
			else if (boolLigne && (!boolSim))
			{
				currentState = "LigneState"
			}
			else if (!boolLigne && boolSim)
			{
				currentState = "SimState"	
			}
			else
			{
				currentState = ""
			}
			
			executeBindings(true);
		}
		
		protected function btVerifierNumLigneClickHandler(event:Event):void
		{
			if(txtNumLigne.text!="")
			{
				btValider.enabled = false;
				lignesUtils.verifierUniciteNumeroLigne(txtNumLigne.text);
			}
		}
		
		protected function btValiderClickHandler(event:MouseEvent):void
		{	
			if(boolLigneChange)
			{
				var oldLigne:String = lignesUtils.selectedLigne.SOUS_TETE;
				var newLigne:String = lignesUtils.tmpLigne.SOUS_TETE;
				ConsoviewAlert.afficherAlertConfirmation("Êtes-vous sur de vouloir fair cette action ?"
														,"Changement de numéro de ligne."
														,confirmNumeroDeLigneHandler);
			}
			else
			{
				updateData();	
			}
		}
		
		protected function confirmNumeroDeLigneHandler(event:CloseEvent):void
		{
			if(event.detail == Alert.OK)
			{
				updateData();
			}
		}
		
		protected function btAnnulerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function updateDataHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
			dispatchEvent(new FicheEvent(FicheEvent.FICHE_UPDATED,true));
		}

		protected function _cbDureeContratChangeHandler(event:ListEvent):void
		{
			if(cbDureeContrat.selectedIndex != -1)
			{
				lignesUtils.tmpLigne.DUREE_CONTRAT = Number(cbDureeContrat.selectedItem.DUREE_CONTRAT);				
			}
			else
			{
				cbDureeContrat.selectedIndex = 0;
			}
			updateDateFPC();
		}
		
		protected function _cbDureeEligibiliteChangeHandler(event:ListEvent):void
		{
			if(cbDureeEligibilite.selectedIndex != -1)
			{
				lignesUtils.tmpLigne.DUREE_ELLIGIBILITE = Number(cbDureeEligibilite.selectedItem.DUREE_ELLIGIBILITE);				
			}
			else
			{
				cbDureeEligibilite.selectedIndex = 0;
			}
			updateDateFPC();
		}
		
		/* dcDateOuverture.addEventListener(CalendarLayoutChangeEvent.CHANGE, _dcDateOuvertureChangeHandler);*/
		protected function _dcDateOuvertureChangeHandler(event:CalendarLayoutChangeEvent):void
		{
			updateDateFPC();
		}
		
		protected function _dcDateRenouvChangeHandler(event:CalendarLayoutChangeEvent):void
		{
			updateDateFPC();
		}

		
	}
}