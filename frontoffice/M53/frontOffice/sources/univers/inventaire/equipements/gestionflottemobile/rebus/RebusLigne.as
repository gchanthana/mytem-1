package univers.inventaire.equipements.gestionflottemobile.rebus
{
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.gestionflottemobile.DgRechercheItemVo;
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	// N'EST PAS EN SERVICE, SERA REMPLACE PAR UN WORKFLOW
	
	public class RebusLigne extends RebusLigneIHM
	{
		private var ligneSelected : DgRechercheItemVo;
		[Embed(source="/assets/equipement/06-50x50BIG.png")] 
		private var adrImg : Class;
		public function RebusLigne(ligneSelected : DgRechercheItemVo)
		{
			super();
			this.ligneSelected = ligneSelected;

			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);	
		}
		
		private function initIHM(evt : FlexEvent):void
		{
			libelle_ligne_rebus.text = ligneSelected.LIGNE;
			addEventListener(CloseEvent.CLOSE,fermer);
			calendar.selectedDate = new Date();	
			bt_annuler.addEventListener(MouseEvent.CLICK,fermer);
			bt_valider.addEventListener(MouseEvent.CLICK,rebusLigne);	
			imgTitre.source = adrImg; 
			
		}
		
		private function fermer(evt : Event):void 
		{
			dispatchEvent((new Event(PanelGestionFoltteMobile.ANNULER_EVENT)));
		}
		
		private function rebusLigne(evt : MouseEvent):void
		{
			var idLigne : int = ligneSelected.IDSOUS_TETE;
			var portage : int;
			var envoiMail : int;
			
			if (radio_portage_oui.selected == true){
				portage = 1;
			}
			else
			{
				if (radio_portage_non.selected == true){
					portage = 2 ;
				}
			}
					
			envoiMail = 1;
					
			/*var nbMoisAvFinContrat : String = inputNbMoisAvFinCont.text; // ?
			var date : String = DateFunction.formatDateAsInverseString(calendar.selectedDate);
			Alert.show("Résiliation : Ligne :"+ligneSelected.IDSOUS_TETE+" résilée. Nombre de jours avant fin de contrat : "+nbMoisAvFinContrat+" Portage : "+portage+" Envoie mail : "+envoiMail+" Date : "+date);
		
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"rebusLigne",
																		rebusLigne_handler);
			RemoteObjectUtil.callService(op,idLigne,nbMoisAvFinContrat,portage,envoiMail,date);*/
			rebusLigne_handler(null);
		}
		
		private function rebusLigne_handler (evt : ResultEvent):void
		{
			//if(evt.result!=-1)
			//{
				ConsoviewAlert.afficherOKImage("Résiliation de la ligne effectuée");
				PopUpManager.removePopUp(this);
			//}
			//else
			//{
				trace("Erreur getPoolGestionnaire");
			//}
		}
	}
}