package univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation
{
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class DesaffecterSIMOfLigne implements IComportementDesaffectation
	{
		public function desaffecter( idLigne : int,idSIM : int,libelleLigne : String,libelleSIM : String,date_effet : String):void
		{
			var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"releaseLigneToSim",
							desaffectationResultHandler,null);
			RemoteObjectUtil.callService(opData,idSIM,idLigne,libelleSIM,libelleLigne,date_effet);					
		}
		
		private function desaffectationResultHandler(re : ResultEvent):void
		{
			
			if (re.result)
			{
				//Alert.show("Equipement desaffecté","",4.0,null,closeAll);
				ConsoviewAlert.afficherOKImage("Equipement desaffecté");
			}		
		}
		public function annuler():void
		{
			//Alert.show("AffecterSimCollab");
		}

	}
}