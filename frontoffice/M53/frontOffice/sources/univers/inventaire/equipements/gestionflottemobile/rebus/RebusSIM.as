package univers.inventaire.equipements.gestionflottemobile.rebus
{
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.equipements.gestionflottemobile.DgRechercheItemVo;
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	public class RebusSIM extends RebusSIMIHM
	{
		[Embed(source="/assets/equipement/SIM50x50.png")] 
		private var adrImg : Class;
		[Embed(source="/assets/images/warning.png")] 
		private var adrImgWarning : Class;
		private var ligneSelected : DgRechercheItemVo;
		
		public function RebusSIM(ligneSelected : DgRechercheItemVo)
		{
			super();
			this.ligneSelected = ligneSelected;

			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);			
		}
			
		public function initIHM(evt : FlexEvent):void
		{
			initData();
			imgTitre.source = adrImg; 
			imgAlert.source = adrImgWarning;
			libelle_SIM_rebus.text = ligneSelected.NUM_SIM;
			addEventListener(CloseEvent.CLOSE,fermer);
			calendar.selectedDate = new Date();	
			bt_annuler.addEventListener(MouseEvent.CLICK,fermer);
			bt_valider.addEventListener(MouseEvent.CLICK,rebusSIM);
		}
		
		private function initData():void
		{
			/*var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"getInfosRebusLigne",
																		infos_Handler);
			RemoteObjectUtil.callService(op,ligneSelected.IDSIM);*/
		}
		
		private function infos_Handler(re : ResultEvent):void
		{
			trace (ObjectUtil.toString(re.result));
			if (re.result == -1)
			{
				
			}
			else
			{
				viewstack_contrat.selectedIndex = 2;
				// récupérer les paramètres de la requête et les afficher dans les inputText (inputNbMoisAvFnContrat, inputPenaliteResi)
				/*var nbMoisAvFnContrat : int;
				var penaliteResi : int;
				
				inputNbMoisAvFnContrat.text = nbMoisAvFnContrat;
				inputPenaliteResi.text = penaliteResi;*/
			}
		}
		
		private function fermer(evt : Event):void 
		{
			dispatchEvent((new Event(PanelGestionFoltteMobile.ANNULER_EVENT)));
		}
		
		private function rebusSIM(evt : MouseEvent):void
		{
			var idCause : int;
						
			if (radio_perte.selected == true){
				idCause = 1;
			}
			else
			{
				if (radio_casse.selected == true){
					idCause = 2 ;
				}
				else
				{
					if (radio_vol.selected == true){
						idCause = 3;
					}
					else
					{
						if (radio_autre.selected == true){
						idCause = 0;
					}
					}
			
				}
			}
			
			var idSIM : int = ligneSelected.IDSIM;
			var commentaire : String = inputComm.text;
			var date : String = DateFunction.formatDateAsInverseString(calendar.selectedDate);
			//Alert.show("Rebus : carte SIM :"+ligneSelected.IDSIM+" mise au rebus. Cause : "+idCause+" Commentaire : "+commentaire+" date : "+date);
			//fermer(null);
			//requête
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"putEquipRecycleSIM_v3",
																		rebusSIM_handler);
			RemoteObjectUtil.callService(op,idSIM,ligneSelected.NUM_SIM,date,commentaire,idCause);
		}
	
		private function rebusSIM_handler (evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				ConsoviewAlert.afficherOKImage("Mise au rebut de la carte SIM effectuée");
				PopUpManager.removePopUp(this);
			}
			else
			{
				trace("Erreur getPoolGestionnaire");
			}
		}
	}
}