package univers.inventaire.equipements.gestionflottemobile.listedispo
{
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;

	
	public class DispoSim extends DispoSimIHM implements IListeDispo 
	{
		[Embed(source="/assets/equipement/SIM50x50.png")] 
		private var adrImg : Class;
		private var dataListeSIM : ArrayCollection = new ArrayCollection();
		private var avecLigne : Boolean = true;
		public function DispoSim(avecLigne : Boolean=true)//Par default on affiche les SIM avec et sans Ligne
		{
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt : FlexEvent):void
		{
			initData();
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
		
		}
		public function getIDSelection():int
		{
			return dgListeSIM.selectedItem.IDSIM;
		}
		public function getDataGrid():DataGrid
		{
			return dgListeSIM;
		}
		public function getLibelleSelection():String
		{
			if(dgListeSIM.selectedIndex!=-1){
				return dgListeSIM.selectedItem.NUM_SIM;
			}
			else
			{
				return "Aucune sélection";
			}
		}
		public function getLibelleType():String
		{
			return "Liste des cartes SIM disponibles";
		}
		public function getSourceIcone():Class
		{
			return adrImg;
		}
		
		private function initData():void
		{
			var maProcdedure : String = "listeSIMDispo";
			
			
			var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
		   		 
			var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							maProcdedure,
							initResultHandler);
							
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,"",PanelGestionFoltteMobile.idPoolGestionnaire,0);						
		}
		private function initResultHandler(re : ResultEvent):void{
			if (re.result){
				dataListeSIM= re.result as ArrayCollection;
				
				if(avecLigne==false){
				
				}
				
				
				dgListeSIM.dataProvider = dataListeSIM;
				dataListeSIM.refresh();
				trace("SIM disponible :"+ObjectUtil.toString(re.result));
			}		
		}
		private function txtFiltreChangeHandler(ev : Event):void
		{
			try{
				dataListeSIM.filterFunction = filtrerLeGrid;
				dataListeSIM.refresh();
			}catch(e :Error){
				trace("Erreur Gestion Flotte GSM");
			}
		}
		private function filtrerLeGrid(item : Object):Boolean{
			if ((String(item.NUM_SIM).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				(String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||				
				(String(item.LIGNE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				(String(item.REVENDEUR).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
	}
}