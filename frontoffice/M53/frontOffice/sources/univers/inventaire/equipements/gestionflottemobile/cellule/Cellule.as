package univers.inventaire.equipements.gestionflottemobile.cellule
{
	import mx.controls.Image;
	
	public class Cellule
	{
		private var id : int;
		private var libelle : String;
		private var type : String ;
		private var icone : Class;
		public function Cellule(id : int,libelle : String,icone : Class,type : String)
		{
			this.id=id;
			this.libelle=libelle;
			this.icone = icone;
			this.type = type;
		}
		public function get id_cellule():int
		{
			return id;
		}
		public function get type_cellule():String
		{
			return type;
		}
		public function get libelle_cellule():String
		{
			return libelle;
		}
		public function get image_cellule():Class
		{
			return icone;
		}
		public function set libelle_cellule(lib : String):void
		{
			this.libelle = lib;
		}
		public function set type_cellule(type : String):void
		{
			this.type = type;
		}
		public function set id_cellule(id : int):void
		{
			this.id= id;
		}
		public function set icone_cellule(icone : Class):void
		{
			this.icone=icone;
		}

	}
}