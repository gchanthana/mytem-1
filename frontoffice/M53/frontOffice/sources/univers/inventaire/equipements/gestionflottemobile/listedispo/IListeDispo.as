package univers.inventaire.equipements.gestionflottemobile.listedispo
{
	import mx.controls.DataGrid;
		
	public interface IListeDispo
	{
		function getIDSelection():int;
		function getLibelleSelection():String;
		function getLibelleType():String;
		function getDataGrid():DataGrid;
		function getSourceIcone():Class;
		
		
	}
}