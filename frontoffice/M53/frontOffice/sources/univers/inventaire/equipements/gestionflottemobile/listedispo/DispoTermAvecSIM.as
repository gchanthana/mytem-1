package univers.inventaire.equipements.gestionflottemobile.listedispo
{
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	public class DispoTermAvecSIM extends DispoTermAvecSIMIHM implements IListeDispo 
	{
		
		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")] 
		private var adrImg : Class;
		private var dataListeTerm : ArrayCollection = new ArrayCollection();
		public function DispoTermAvecSIM()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt : FlexEvent):void
		{
			initData();
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
		}
		public function getIDSelection():int
		{
			return dgListeTermAvecSIM.selectedItem.IDTERMINAL;
		}
		public function getDataGrid():DataGrid
		{
			return dgListeTermAvecSIM;
		}
		
		public function getLibelleType():String
		{
			return "Liste des terminaux disponibles avec SIM";
		}
		public function getSourceIcone():Class
		{
			return adrImg;
		}
		public function getLibelleSelection():String
		{
			if(dgListeTermAvecSIM.selectedIndex!=-1){
				return dgListeTermAvecSIM.selectedItem.IMEI;
			}
			else
			{
				return "Aucune sélection";
			}
		}
		private function initData():void
		{
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
	 
			var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"getNonAffectedTerm",
							initResultHandler);
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,PanelGestionFoltteMobile.idPoolGestionnaire);					
		}
		private function initResultHandler(re : ResultEvent):void{
			if (re.result){
				dataListeTerm = re.result as ArrayCollection;
				dgListeTermAvecSIM.dataProvider = dataListeTerm;
				dataListeTerm.refresh();
				trace("Terminal disponibles :"+ObjectUtil.toString(re.result));
			}		
		}
		private function txtFiltreChangeHandler(ev : Event):void
		{
			try{
				dataListeTerm.filterFunction = filtrerLeGrid;
				dataListeTerm.refresh();
			}catch(e :Error){
				trace("Erreur Gestion Flotte GSM");
			}
		}
		private function filtrerLeGrid(item : Object):Boolean{
			if ((String(item.IMEI).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				(String(item.MARQUE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||				
				(String(item.MODELE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||				
				(String(item.NUM_SIM).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||				
				(String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||				
				(String(item.LIGNE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||				
				(String(item.REVENDEUR).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
	}
}