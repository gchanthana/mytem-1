package univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation
{
	public interface IComportementDesaffectation
	{
		//function desaffecter(id1 : int,id2:int,lib1 :String, lib2 : String, date : String):void;
		function desaffecter(id1 : int, id2 : int,libelle1 : String,libelle2 : String,date_effet : String ):void;
		function annuler():void;
	}
}