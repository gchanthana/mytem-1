package univers.inventaire.equipements.gestionflottemobile.listedispo
{
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	public class DispoLigne extends DispoLigneIHM implements IListeDispo
	{
		[Embed(source="/assets/equipement/Ligne.png")] 
		private var adrImg : Class;
		private var dataListeLigne : ArrayCollection = new ArrayCollection();
		public function DispoLigne()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt : FlexEvent):void
		{
			initData();
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
		}
		public function getIDSelection():int
		{
			return dgListeLigne.selectedItem.IDSOUS_TETE; 
		}
		public function getDataGrid():DataGrid
		{
			return dgListeLigne;
		}
		public function getLibelleType():String
		{
			return "Liste des lignes disponibles";
		}
		public function getSourceIcone():Class
		{
			return adrImg;
		}
		public function getLibelleSelection():String
		{
			if(dgListeLigne.selectedIndex!=-1){
				return dgListeLigne.selectedItem.LIGNE;
			}
			else
			{
				return "Aucune sélection";
			}
		}
		private function initData():void
		{
			
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
			 
			 var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"get_Ligne_sans_sim",
							initResultHandler);
			RemoteObjectUtil.callService(opData,PanelGestionFoltteMobile.idPoolGestionnaire);			
		}
		private function initResultHandler(re : ResultEvent):void{
			if (re.result){
				dataListeLigne = re.result as ArrayCollection;
				dgListeLigne.dataProvider = dataListeLigne;
				dataListeLigne.refresh();
				trace("Collaborateurs disponibles :"+ObjectUtil.toString(re.result));
			}		
		}
		private function txtFiltreChangeHandler(ev : Event):void
		{
			try{
				dataListeLigne.filterFunction = filtrerLeGrid;
				dataListeLigne.refresh();
			}catch(e :Error){
				trace("Erreur Gestion Flotte GSM");
			}
		}
		private function filtrerLeGrid(item : Object):Boolean{
			if ((String(item.LIGNE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
	}
}