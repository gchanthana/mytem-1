package univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation
{
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class DesaffecterTermOfCollab implements IComportementDesaffectation
	{
		
		public function desaffecter(idCollab : int,idTerm : int,libelleCollab : String,libelleTerm : String,date_effet : String):void
		{
			var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
	   	
		   var opDesaffecteTermToEmp  : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"releaseTermToEmp",
							desaffectationResultHandler);
		   			
			RemoteObjectUtil.callService(opDesaffecteTermToEmp,idTerm,idCollab,libelleTerm,libelleCollab,date_effet);
		}
		private function desaffectationResultHandler(re : ResultEvent):void
		{
			trace ("desaffectationResultHandler : " +re.result.toString());
			
			if (re.result){
				//Alert.show("Equipement desaffecté","",4.0,null,closeAll);*
				ConsoviewAlert.afficherOKImage("Equipement desaffecté");
			}		
		}
		public function annuler():void
		{
			Alert.show("AffecterSimCollab");
		}
	}
}