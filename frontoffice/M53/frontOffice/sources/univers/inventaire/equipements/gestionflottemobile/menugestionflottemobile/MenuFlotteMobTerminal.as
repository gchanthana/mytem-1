package univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	import univers.inventaire.equipements.gestionflottemobile.cellule.Cellule;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.AffecterSimTerm;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.AffecterTermCollab;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.RemplacerTERM;
	import univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation.DesaffecterSIMOfTerm;
	import univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation.DesaffecterTermOfCollab;
	//import univers.inventaire.equipements.gestionflottemobile.fiches.ihm.FicheEquipSAVIHM;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoCollab;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoSim;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoTerm;
	import univers.inventaire.equipements.gestionflottemobile.panelEnconstruction;
	import univers.inventaire.equipements.gestionflottemobile.panelaffectation.PanelAffectation;
	import univers.inventaire.equipements.gestionflottemobile.paneldesaffectation.PanelDesaffectation;
	import univers.inventaire.equipements.gestionflottemobile.panelechange.PanelEchange;
	import univers.inventaire.equipements.gestionflottemobile.rebus.RebusTerminal;
	
	[Event(name="btHistoriqueCliked",type="flash.events.MouseEvent")]
	
	public class MenuFlotteMobTerminal extends MenuGestionFlotteMobileIHM
	{
		public static const BT_HISTORIQUE_CLIKED:String = "btHistoriqueCliked";
		
		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")] 
		private var adrImg : Class;
		
		private var panelGestionFlotteMobile  : PanelGestionFoltteMobile;
		private var popup : TitleWindow;
		private var stateToolTip : Boolean = stateToolTip;
		public function MenuFlotteMobTerminal(panelGestionFlotteMobile  : PanelGestionFoltteMobile,stateToolTip : Boolean=false)
		{
			this.panelGestionFlotteMobile=panelGestionFlotteMobile;
			super();
			this.stateToolTip = stateToolTip;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(fe : FlexEvent):void
		{
			if(stateToolTip==true)
			{
	
				bt_sup_tel.visible = false;
				bt_sup_tel.width=0;
				bt_sup_tel.height=0;
				
				bt_new_tel.visible = false;
				bt_new_tel.width=0;
				bt_new_tel.height=0;
				
				bt_mod_renouv.visible = false;
				bt_mod_renouv.width=0;
				bt_mod_renouv.height=0;
								
				this.setCurrentState("toolTipState");
				labToolTip = panelGestionFlotteMobile.objMenuFlotteMobTerminal_fixe.labToolTip;
			}
			var imgNew : Image = new Image();
			imgNew.source=adrImg;
			canvas_icone.addChild(imgNew);
			
			bt_new_user.initParametre(true,"Associer un utilisateur au terminal(IMEI): ");
			bt_new_tel.initParametre(false);	
			bt_new_num.initParametre(true,"Associer ligne/SIM au terminal(IMEI): ");
			bt_new_sim.initParametre(true,"Associer ligne/SIM au terminal(IMEI): ");
			
			bt_sup_user.initParametre(true,"Dissocier l'utilisateur du terminal(IMEI): ");	
			bt_sup_tel.initParametre(false);	
			bt_sup_num.initParametre(true,"Dissocier ligne/SIM du terminal(IMEI): ");
			bt_sup_sim.initParametre(true,"Dissocier ligne/SIM du terminal(IMEI): ");
			
			bt_mod_sav.initParametre(true,"Gestion du SAV du terminal(IMEI): ");	
			bt_mod_renouv.initParametre(false);	
			
			bt_mod_preter.initParametre(true,"Remplacer le terminal(IMEI): ");/*,"Remplacer le terminal(IMEI): "  */
			
			bt_mod_rebus.initParametre(true,"Mettre au rebut le terminal(IMEI): ");
			btGetHistorique.initParametre(true,"Historique du terminal(IMEI): ");
			
			//bt_new_tel.addEventListener(MouseEvent.CLICK,newPanelListeTerminal);
			
			bt_sup_user.addEventListener(MouseEvent.CLICK,newPanelTerminalDesaffectationEmp);
			bt_sup_num.addEventListener(MouseEvent.CLICK,newPanelTerminalDesaffectationLigne);
			bt_sup_sim.addEventListener(MouseEvent.CLICK,newPanelTerminalDesaffectationSIM);
			
			bt_new_user.addEventListener(MouseEvent.CLICK,newPanelTerminalAffectationEmp);
			bt_new_num.addEventListener(MouseEvent.CLICK,newPanelTerminalAffectationLigne);
			bt_new_sim.addEventListener(MouseEvent.CLICK,newPanelTerminalAffectationSIM);

			bt_mod_renouv.addEventListener(MouseEvent.CLICK,newPanelEnConstruction);
			bt_mod_preter.addEventListener(MouseEvent.CLICK,newPanelPret);
			bt_mod_rebus.addEventListener(MouseEvent.CLICK,newPanelRebus);
			bt_mod_sav.addEventListener(MouseEvent.CLICK,newPanelSav);
			
			btGetHistorique.addEventListener(MouseEvent.CLICK,_btGetHistoriqueClickHandler);
			
		}
		
		private function newPanelEnConstruction(event : MouseEvent):void
		{
			addPopup(new panelEnconstruction());
		}
		private function newPanelRebus(event : MouseEvent):void
		{
			addPopup(new RebusTerminal(PanelGestionFoltteMobile.objLigneSelected));
		}
		private function newPanelSav(event : MouseEvent):void
		{
			
/* 			var pop : FicheEquipSAVIHM = new FicheEquipSAVIHM();
			
			pop.idfournisseur = PanelGestionFoltteMobile.objLigneSelected.IDFOURNISSEUR;
			pop.idContrat = PanelGestionFoltteMobile.objLigneSelected.IDCONTRAT_GARANTIE;
			pop.idEmploye = Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE);
			pop.idEquipement = Number(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL);			
			pop.libelleEquip = PanelGestionFoltteMobile.objLigneSelected.IMEI;
			
			addPopup(pop); */
		}
		
		private function newPanelListeTerminal(event : MouseEvent):void
		{
			
		}
		//Case desaffectation Emp
		private function newPanelTerminalDesaffectationEmp(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE!=null)
			{
				[Embed(source="/assets/equipement/User.gif")] 
				var adrImgEmp : Class;
				//Pour ce cas, il n'y a pas besoin de liste dispo : Un term ne peut avoir que une SIM.
				addPopup(new PanelDesaffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE),PanelGestionFoltteMobile.objLigneSelected.COLLABORATEUR,adrImgEmp,"collaborateur"),
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL),PanelGestionFoltteMobile.objLigneSelected.IMEI,adrImg,"terminal"),
									new DesaffecterTermOfCollab()
									));
			}
			else
			{
				Alert.show("Le terminal sélectionné n'est pas affecté à un collaborateur","Impossible de rendre le terminal");
			}		
		}
		private function newPanelTerminalDesaffectationSIM(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDSIM > 0)
			{
				[Embed(source="/assets/equipement/SIM50x50.png")] 
				var adrImgSIM : Class;
				//Pour ce cas, il n'y a pas besoin de liste dispo : Un term ne peut avoir que une SIM.
				addPopup(new PanelDesaffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL),PanelGestionFoltteMobile.objLigneSelected.IMEI,adrImg,"terminal"),
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImgSIM,"de la carte SIM"),
									new DesaffecterSIMOfTerm()
									));
			}
			else
			{
				Alert.show("Le terminal sélectionné n'a pas de carte SIM","Impossible de retirer la carte SIM");
			}
		
		}
		private function newPanelTerminalDesaffectationLigne(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDSOUS_TETE > 0){
				[Embed(source="/assets/equipement/SIM50x50.png")] 
				var adrImgSIM : Class;
				//Pour ce cas, il n'y a pas besoin de liste dispo : Un term ne peut avoir que une SIM.
				addPopup(new PanelDesaffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL),PanelGestionFoltteMobile.objLigneSelected.IMEI,adrImg,"terminal"),
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImgSIM,"de la carte SIM"),
									new DesaffecterSIMOfTerm()
									));
			}
			else
			{
				Alert.show("Le terminal sélectionné n'a pas de ligne","Impossible de dissocier la ligne");
			}
			
		}
		private function newPanelTerminalAffectationEmp(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE != null){
				Alert.show("Le terminal sélectionné est déja associé à un collaborateur","Impossible de fournir ce terminal à un collaborateur");
			}
			else
			{
				addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL),PanelGestionFoltteMobile.objLigneSelected.IMEI,adrImg,"terminal"),
									new AffecterTermCollab(),
									new DispoCollab()
									));
			}
		}
		private function newPanelPret(event : MouseEvent):void
		{
			/*
			******************
			*	La gestion des prêts a été abandonnée : Ci dessous le code permettant d'ouvrir un pop-up avec la case Prêt pré-coché"	
			*******************
			*/
			/*if(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE != null){
				Alert.show("Le terminal sélectionné est déja associé à un collaborateur","Impossible de fournir ce terminal à un collaborateur");
			}
			else
			{
				addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL),PanelGestionFoltteMobile.objLigneSelected.IMEI,adrImg,"terminal"),
									new AffecterTermCollab(),
									new DispoCollab(),
									true,
									true
									));
			}*/
			if(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE ==null && PanelGestionFoltteMobile.objLigneSelected.IDSIM <0)
			{
				Alert.show("Le terminal ne peut pas être échangé car il n'est pas dans une relation (pas de collaborateur et pas de carte SIM)");
			}
			else
			{
				addPopup(new PanelEchange(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL),PanelGestionFoltteMobile.objLigneSelected.IMEI,adrImg,"terminal"),
									new RemplacerTERM(),
									new DispoTerm()
									
									));
			}
			
		}
		private function newPanelTerminalAffectationSIM(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDSIM > 0){
				Alert.show("Le terminal sélectionné a déja une carte SIM","Impossible d'affecter une carte SIM au terminal");
			}
			else
			{
				addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL),PanelGestionFoltteMobile.objLigneSelected.IMEI,adrImg,"terminal"),
									new AffecterSimTerm(),
									new DispoSim(),
									true,
									false,
									false
									));
			}
		}
		private function newPanelTerminalAffectationLigne(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDSIM > 0){
				Alert.show("Le terminal sélectionné a déja une carte SIM","Impossible d'affecter une carte SIM au terminal");
			}
			else
			{
				addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL),PanelGestionFoltteMobile.objLigneSelected.IMEI,adrImg,"terminal"),
									new AffecterSimTerm(),
									new DispoSim(),
									false
									));
			}
		}
		private function addPopup(popup : TitleWindow):void {
			this.popup=popup;
			popup.addEventListener(PanelGestionFoltteMobile.VALIDER_EVENT,closePopUp); // Si il y a une validation
			popup.addEventListener(PanelGestionFoltteMobile.ANNULER_EVENT,closePopUp); // Si il y a une annulation
									
			PopUpManager.addPopUp(popup,panelGestionFlotteMobile,true);
			PopUpManager.centerPopUp(popup);
		}
		private function closePopUp(evt : Event):void {
			if(evt.type == PanelGestionFoltteMobile.ANNULER_EVENT)
			{
				PopUpManager.removePopUp(popup); 
			}
			else if(evt.type == PanelGestionFoltteMobile.VALIDER_EVENT) 
			{
				panelGestionFlotteMobile.actualise(); 
				PopUpManager.removePopUp(popup); 
			}
		}
		
		protected function _btGetHistoriqueClickHandler(event:MouseEvent):void
		{
			dispatchEvent(new MouseEvent(BT_HISTORIQUE_CLIKED));
		}
	}
}