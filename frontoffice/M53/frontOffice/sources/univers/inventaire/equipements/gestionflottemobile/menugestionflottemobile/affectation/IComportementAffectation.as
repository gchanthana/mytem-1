package univers.inventaire.equipements.gestionFlotteMobile.comportementAffectation
{
	public interface IComportementAffectation
	{
		//Pour une affectation il faut 2 Ids
		function affecter(id1 : int,id2:int):void;
		function annuler():void;
	}
}