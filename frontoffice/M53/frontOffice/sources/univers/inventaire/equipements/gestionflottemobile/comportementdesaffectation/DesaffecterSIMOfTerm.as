package univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation
{
		import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	
	public class DesaffecterSIMOfTerm implements IComportementDesaffectation
	{
		
		public function desaffecter(idTerm : int, idSIM : int,libelleTerm : String,libelleSIM : String,date_effet : String ):void
		{
			 var opDesaffecteTermToEmp  : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"releaseSimToTerm",
							desaffectationResultHandler);
		   			
			RemoteObjectUtil.callService(opDesaffecteTermToEmp,idTerm,idSIM,libelleTerm,libelleSIM,date_effet);
		}
		private function desaffectationResultHandler(re : ResultEvent):void
		{
			trace ("desaffectationResultHandler : " +re.result.toString());
			
			if (re.result){
				
				ConsoviewAlert.afficherOKImage("Equipement desaffecté");
			}		
		}
		public function annuler():void
		{
			Alert.show("AffecterSimCollab");
		}
	}
}