package univers.inventaire.equipements.gestionFlotteMobile.ListeDispo
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;

	
	public class DispoSim extends DispoSimIHM implements IListeDispo 
	{
		[Embed(source="/assets/equipement/SIM50x50.png")] 
		private var adrImg : Class;
		private var dataListeSIM : ArrayCollection = new ArrayCollection();
		public function DispoSim()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt : FlexEvent):void
		{
			initData();
		
		}
		public function getIDSelection():int
		{
			return 100;
		}
		public function getDataGrid():DataGrid
		{
			return dgListeSIM;
		}
		public function getLibelleType():String
		{
			return "des cartes SIM";
		}
		public function getSourceIcone():Class
		{
			return adrImg;
		}
		public function getLibelleSelection():String
		{
			if(dgListeSIM.selectedIndex!=-1){
				return dgListeSIM.selectedItem.NUM_SIM;
			}
			else
			{
				return "Aucune sélection";
			}
		}
		private function initData():void
		{
			
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
							 
		 	var xmlParam :XML = XmlParamUtil.createXmlParamObject();
		 	XmlParamUtil.addParam(xmlParam,"IDGROUPE_RACINE","id Groupe racine",GROUPE_INDEX);
		   		 
			var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteManager",
							"getAllNonAffectedSimToEmp",
							initResultHandler);
							
			RemoteObjectUtil.callService(opData,xmlParam);						
		}
		private function initResultHandler(re : ResultEvent):void{
			if (re.result){
				dataListeSIM= re.result as ArrayCollection;
				dgListeSIM.dataProvider = dataListeSIM;
				dataListeSIM.refresh();
				trace("SIM disponible :"+ObjectUtil.toString(re.result));
			}		
		}
		

	}
}