package univers.inventaire.equipements.revendeurs.system
{
	///////////////////////////////////////////////////////////
	//  Agence.as
	//  Macromedia ActionScript Implementation of the Class Agence
	//  Generated by Enterprise Architect
	//  Created on:      20-ao�t-2008 16:26:31
	//  Original author: samuel.divioka
	///////////////////////////////////////////////////////////



	/**
	 * [Bindable]
	 * 
	 * Si ID_SOCIETEMERE &gt; 0 , c'est une agence sinon c'est la fiche d'un revendeur
	 * (fiche agence)
	 * @author samuel.divioka
	 * @version 1.0
	 * @created 20-ao�t-2008 16:26:31
	 */
	 
	[Bindable]
	public class Agence
	{
	    public var ADRESSE1: String = "";
	    /**
	     * compl�ment d'adresse
	     */
	    public var ADRESSE2: String = "";
	    public var CODE_INTERNE: String = "";
	    public var COMMUNE: String = "";
	    /**
	     * applogin_id.nom applogin_id.prenom (USER_CREATE)
	     */
	    public var CREE_PAR: String = "";
	    public var DATE_CREATE: Date = null;
	    public var DATE_MODIF: Date = null;
	    public var FAX: String = "";
	    /**
	     * l'agence parente (IDCDE_CONTACT_SOCIETE)
	     */
	    public var IDCDE_CONTACT_SOCIETE: Number = 0;
	    public var IDRACINE: Number = 0;
	    /**
	     * l'agence parente (IDCDE_CONTACT_SOCIETE)
	     */
	    public var IDSOCIETE_MERE: Number = 0;
	    /**
	     * applogin_id.nom applogin_id.prenom (USER_CREATE
	     */
	    public var MODIFIE_PAR: String = "";
	    public var PAYSCONSOTELID: Number = 0;
	    public var RAISON_SOCIALE: String = "";
	    public var SIREN: String = "";
	    public var TELEPHONE: String = "";
	    public var USER_CREATE: Number = 0;
	    public var USER_MODIF: Number = 0;
	    public var ZIPCODE: String = "";

	}//end Agence
}	