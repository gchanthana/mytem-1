package univers.inventaire.inventaire.export
{
	/**
	 * Interface à implémenter pour les Exports
	 * */
	public interface IExportable	
	{
		/**
		 * Export au format passer en parametres
		 * @param format le format sous lequel on exporte les données
		 * */
		function exporter(format : String = "PDF"):void;			
	}
}