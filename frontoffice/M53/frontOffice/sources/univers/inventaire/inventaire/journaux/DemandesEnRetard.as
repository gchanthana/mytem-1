package univers.inventaire.inventaire.journaux
{
	import mx.events.FlexEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.controls.dataGridClasses.DataGridColumn;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	/**
	 * Journal de l'inventaire complet 
	 * Affiche l'inventaire à une date donnée
	 * */
	public class DemandesEnRetard extends DemandesEnRetardIHM implements IJournal
	{
		//ArrayCollection contenant les lignes du journal
		[Bindable]
		private var elementsDuJournal : ArrayCollection;
		
		//Reference vers la méthode distante qui ramène les lignes du journal
		private var opElementDuJournal : AbstractOperation;
		
		//les information du noeud (inutilisé ici)
		private var _nodeInfos : Object;
		
		/**
		 * Constructeur
		 * */	
		public function DemandesEnRetard()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		
		/**
		 * les infos sur le noeud
		 * */
		 public function set nodeInfos(infos : Object):void{
		 	_nodeInfos = infos;
		 }
		 
		/**
		 * Gere le changement de périmetre
		 * */
		public function onPerimetreChange():void
		{
		 
			//init filtre
			txtFiltre.text = "";
			
			
			
			//init le data provider
			elementsDuJournal.source = null
			elementsDuJournal.refresh();
			
			//données
			chargerDonnees();
			 
		}
		
		
		/**
		 * Recharge les données du journal
		 * */
		public function refresh():void
		{
			chargerDonnees();
		}
		
		/**
		* Fonction qui permet d'afficher la période sur laquelle on travaille, au format jour mois annee 
		*  
		* @param moisDebut : String La date de debut de la période
		* @param moisFin : String La date de fin de la période
		*
		* Ne fait rien pour cette classe 
		* */
		public function setSelectedPeriode(moisDebut:String, moisFin:String):void
		{
			//not used
		}
		
		/**
		 * Interrompt les remotings en cours
		 * Méthode non utilisée
		 * */
		public function cancelRemotings():void
		{
			try{
				 
			}catch(e : Error){
				trace("ok");
			}
		}
		
		
		//Initialisation de l'ihm
		//Affecte les écouteurs d'évènements
		//Charge les données 
		private function initIHM(fe : FlexEvent):void{
			//Filtre
			txtFiltre.addEventListener(Event.CHANGE,filtrerGrid);
			dateCom.labelFunction = formateDates;
			dateLivP.labelFunction = formateDates;
			
			myGrid.addEventListener(Event.CHANGE,goToselectedOperation);
						
			chargerDonnees();			
		}
		
//--- FILTRE -----------------------------------------------------------------------------------------------------------------------------------		
		
		
		//formate une colonne avec le symbol Euros	
		//voir l'attribut LabelFunction pour un DataGRid pour les parametres	
		private function formateEuros(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2);						
		}
		
		
		//formate une colonne de numéro de téléphone	
		//voir l'attribut LabelFunction pour un DataGRid pour les parametres
		private function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}
		
		
		//formate une colonne de date dans un DataGrid	
		//voir l'attribut LabelFunction pour un DataGRid pour les parametres
		private function formateDates(item : Object, column : DataGridColumn):String{
			var ladate:Date = new Date(item[column.dataField]);
			return DateFunction.formatDateAsString(ladate);			
		}
		
//--- FIN PERIODE SELECTOR ---------------------------------------------------------------------------------------------------------------------
		
				
//--- FILTRE -----------------------------------------------------------------------------------------------------------------------------------
		
		//Gere le filtre du tableau
		private function filtrerGrid(ev :Event):void{
			elementsDuJournal.filterFunction = filterFunc;
			elementsDuJournal.refresh();		
		}
		
		//filtre sur les attributs LIBELLE_COMMANDE, NOMOP, CONTACT, DATE_COMMANDE, COMMENTAIRES, DATE_LIVRAISON_PREVUE des elements du Tabelau
		private function filterFunc(value : Object):Boolean{				
			if ((String(value.LIBELLE_COMMANDE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.NOMOP).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.CONTACT).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.DATE_COMMANDE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.COMMENTAIRES).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.DATE_LIVRAISON_PREVUE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
				
			{
				return (true);
			} else {
				return (false);
			}	
		}

//--- FIN FILTRE -------------------------------------------------------------------------------------------------------------------------------
//--- GRID -------------------------------------------------------------------------------------------------------------------------------------
		
		//Dispatche un evenement SelectionOperationEvent de type SelectionOperationEvent.OPERATION_SELECTED
		//Permet de passer l'identifiant de la commande
		private function goToselectedOperation(ev : Event):void{
			
			if (myGrid.selectedIndex != -1){
				var evtObj : SelectionOperationEvent = new SelectionOperationEvent(SelectionOperationEvent.OPERATION_SELECTED);			
				evtObj.idOperation = myGrid.selectedItem.IDCDE_COMMANDE;			
				callLater(dispatchEvent,[evtObj]);	
			}
			
						
		}
//--- FIN GRID ---------------------------------------------------------------------------------------------------------------------------------
//--- REMOTINGS --------------------------------------------------------------------------------------------------------------------------------
		
		//Charge les donnees du Journal
		private function chargerDonnees():void
		{
			 
			var idGroupe:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			
			lblError.visible = false;			
			opElementDuJournal = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Journaux",
																				"getDemandeAvecRetard",
																				chargerDonneesResultHandler);						
			RemoteObjectUtil.callService(opElementDuJournal,
										idGroupe);		
			 
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerDonnees' à l'ArrayCollection 'elementsDuJournal' et les affiches
		private function chargerDonneesResultHandler(re :ResultEvent):void{
			elementsDuJournal = re.result as ArrayCollection;	
			if (elementsDuJournal.length > 0){
				txtFiltre.editable = true;
				txtFiltre.enabled =true;
			} 
			else {
				txtFiltre.editable = false;
				txtFiltre.enabled =true
				lblError.visible = true;
			}
			myGrid.dataProvider = elementsDuJournal;
			elementsDuJournal.filterFunction = filterFunc;
			elementsDuJournal.refresh();
		}
//--- FIN REMOTINGS ----------------------------------------------------------------------------------------------------------------------------
	}
}