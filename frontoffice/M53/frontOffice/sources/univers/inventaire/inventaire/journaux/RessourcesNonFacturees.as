package univers.inventaire.inventaire.journaux
{
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.collections.ArrayCollection;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import composants.util.DateFunction;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	
	import mx.controls.dataGridClasses.DataGridColumn;
	import composants.util.ConsoviewFormatter;
	
	
	
	/**
	 * Classe Non utilisée
	 * */
	public class RessourcesNonFacturees extends RessourcesNonFactureesIHM
	{
		private var op : AbstractOperation;
		[Bindable]
		private var dataGridData : ArrayCollection;
		
		public function RessourcesNonFacturees()
		{
			//TODO: implement function
			super();			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		public function refresh():void{
			chargerData();
		}
		
		private function initIHM(fe : FlexEvent):void{
			myGrid.dataProvider = dataGridData;
			DataGridColumn(myGrid.columns[2]).labelFunction = formateLigne;
		}
		
		private function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);			
		}
		
		private function chargerData():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
						
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Journaux",
																				"getListeRessourcesNonFacturees",
																				chargerDataResultHandler);				
			var toDay : Date = new Date();
			var toDayString : String = DateFunction.formatDateAsString(toDay);
			RemoteObjectUtil.callService(op,idGroupeMaitre,toDayString);		
		}
		
		private function chargerDataResultHandler(re :ResultEvent):void{
			
			dataGridData = re.result as ArrayCollection;
			myGrid.dataProvider = dataGridData;
			dataGridData.refresh();			 
		}
		
	}
}