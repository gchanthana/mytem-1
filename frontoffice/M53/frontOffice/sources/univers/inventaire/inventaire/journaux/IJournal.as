package univers.inventaire.inventaire.journaux
{
	
	/**
	 * Interface journal
	 * Les journaux du module doivent implementer cette interface
	 * */
	public interface IJournal
	{
		/**
		 *Recharge les données du journal
		 * */
		function refresh():void;
		
		/**
		* Fonction qui permet d'afficher la période sur laquelle on travaille, au format jour mois annee 
		*  
		* @param moisDebut : String La date de debut de la période
		* @param moisFin : String La date de fin de la période
		* 
		* */
		function setSelectedPeriode(moisDebut : String, moisFin : String):void;
		
		/**
		 * Gere le changement de périmetre
		 * */
		function onPerimetreChange():void;
		
		/**
		 * Interrompt les remotings en cours
		 * Méthode vide -> l'interruption des remotings est gérée par la classe fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil
		 * */
		function cancelRemotings():void;
		
	 
		/**
		 * Affecte les infos sur le noeud au journal
		 * */
		function set nodeInfos(infos : Object):void;
		
	}
}