package univers.inventaire.equipements.lignes.fiche {
	import flash.events.Event;

	public class FicheLigneEvent extends Event {
		public static const FICHELIGNE_RETURN_CLICK:String = "onReturnClick";
		
		public function FicheLigneEvent() {
			super(FICHELIGNE_RETURN_CLICK);
		}
	}
}
