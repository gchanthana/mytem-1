package univers.inventaire.equipements.lignes.fiche {
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.events.ItemClickEvent;
	import mx.core.Container;
	import mx.controls.Alert;
	
	/**
	 * Chaque sous-classe doit être répertoriée dans :
	 * consoview.annuaire.site.fiche.FicheLigneFactory
	 * Design Pattern : Abstract Factory
	 * */
	public class AbstractFicheLigne extends FicheLigneIHM {
		/** Objet encapsulant tous les champs d'une ligne séléctionnée dans le grid des lignes **/
		protected var sousteteObject:Object;
		/** Index du menu de retour (Protected : Modifiable par les sous-classes) **/
		protected var returnIndex:int;
		
		/**
		 * Constructeur des Fiches Lignes.
		 * Il initialise la valeur de l'index du menu de retour et initialise
		 * le handler associé.
		 * */
		public function AbstractFicheLigne(sousteteObject:Object) {
			// Objet contenant les champs de la sous-tete
			this.sousteteObject = sousteteObject;
			// Index Retour par défaut
			returnIndex = 0;
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		protected function initIHM(event:Event):void {
			// Link Bar - Menu
			menuFiche.selectedIndex = 0;
			menuFiche.addEventListener(ItemClickEvent.ITEM_CLICK,dispatchItemClickEvent);
			// Instanciation des items de la fiche (ie les IHM des menus qui composent la fiche).
			createIhmItems();
		}

		/**
		 * C'est dans cette fonction que l'on instancie les objets menu (visuels)
		 * qui composent la fiche.
		 * On commence par instancier les menus puis on les ajoute à la fiche avec
		 * la méthode addMenuList (ci-dessous).
		 * Donc chaque sous-classe de FicheLigne implémente sa propre méthode :
		 * createIhmItems().
		 * A noter que par défaut l'index du menu de retour est 0. C'est à dire que
		 * par défaut c'est le 1er menu que l'on a ajouté.
		 * IMPORTANT : Il ne faut pas créer le menu de retour (et l'ajouter).
		 * Une fois tous les menus (sauf retour) créés et ajoutés. Il suffit d'appeller
		 * la méthode setReturnAt(index:int) pour créer le menu de retour
		 * */
		protected function createIhmItems():void {
			// A Customizer par les sous-classes
		}
		
		/**
		 * Ajoute la liste de menu spécifiée en paramètre. L'ordre des menus est celuis
		 * des paramètres. Le dernier menu spécifié dans la liste est le menu de Retour.
		 * Chaque menu doit avoir un label, car c'est ce dernier qui sera affiché dans la
		 * barre de menu (LinkBar). Les items qui sont des sous-types de DisplayObject seront
		 * visibles (Panel, VBox, etc...) et leur label sera affiché dans le menu.
		 * Le 1er paramètre aura l'index 0 dans le menu, le 2ème l'index 1, ainsi de suite...
		 * */
		protected function addMenuList(... items):void {
			var i:int = 0;
			for(i = 0; i < items.length; i++)
				ficheLigneStack.addChild(items[i]);
		}
		
		protected function setReturnAt(newReturnIndex:int,returnLabel:String):void {
			var returnObject:Container = new Container();
			returnObject.label = returnLabel;
			this.returnIndex = newReturnIndex;
			ficheLigneStack.addChildAt(returnObject,returnIndex);
		}
		
		protected function dispatchItemClickEvent(event:Event):void {
			var itemEvent:ItemClickEvent = event as ItemClickEvent;
			if(itemEvent.index == returnIndex)
				dispatchEvent(new FicheLigneEvent());
		}
	}
}
