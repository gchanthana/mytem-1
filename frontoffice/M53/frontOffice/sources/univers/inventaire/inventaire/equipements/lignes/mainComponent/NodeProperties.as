package univers.inventaire.equipements.lignes.mainComponent
{
	import mx.events.FlexEvent;
	import mx.events.CloseEvent;
	import flash.events.Event;
	import mx.managers.PopUpManager;
	import flash.events.MouseEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.FaultEvent;
	import mx.collections.XMLListCollection;
	import mx.collections.ArrayCollection;
	import univers.parametres.perimetres.main;

	
	public class NodeProperties extends NodePropertiesIHM
	{
		private var _IDnode:Number;
		private var _IDorga:Number;
		private var _TypeOrga:String;
		private var _parentRef:main;
		
		public function NodeProperties()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		public function setParentRef(ref:main):void
		{
			_parentRef = ref;
		}
		
		/*public function setNodeId(id:Number):void
		{
			_IDnode = id;
		}*/
		
		public function setID(orgaID:Number, typeOrga:String, nodeID:Number):void
		{
			_IDnode = nodeID;
			_IDorga = orgaID;
			_TypeOrga = typeOrga;
		}

		
		protected function initIHM(event:Event):void
		{
			PopUpManager.centerPopUp(this);
			this.addEventListener(CloseEvent.CLOSE, closeWin);
			btnCancel.addEventListener(MouseEvent.CLICK, onbtnCancelClicked);
			btnUpdate.addEventListener(MouseEvent.CLICK, onbtnUpdateClicked);
							
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"getNodeProperties",
																				getNodePropertiesResult, throwError);
			RemoteObjectUtil.callService(op, _IDnode);
			refreshWindow();
		}
		
		private function refreshWindow():void
		{
			var hide:Boolean = true;
			switch (_TypeOrga)
			{
				case "GEO":
					cbTypeOrga.selected = true;
					break;
				case "CUS":
					break;
				default:
					hide = false;
			}
			
			if (_IDnode == _IDorga)
			{
				fiTypeOrga.visible = hide;
				fiNbLine.visible = false;
			}
			else
			{
				fiTypeOrga.visible = false;
				fiNbLine.visible = true;
			}
		}
		
		private function getNodePropertiesResult(event:ResultEvent):void
		{
			/*var _propertiesData = new ArrayCollection(
							[{comment:"Hello", date:"10-06-2001", nbLines:35}]
							);*/
							
			var d:ArrayCollection = ArrayCollection(event.result);
			txtComment.text = d[0].COMMENTAIRES;
			var newDate:Date = d[0].DATE_CREATION as Date;
			txtDate.text = d[0].DATE_CREATION_STRING;
			txtNbLine.text = d[0].NB_ELEM;
			
		}
		
		private function closeWin(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
			//winProperties.remo
		}
		
		private function onbtnCancelClicked(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function onbtnUpdateClicked(event:MouseEvent):void
		{
			var idOrga:Number;
			var typeOrga:String = "CUS";
			if (fiTypeOrga.visible)
				idOrga = _IDorga;
			if (cbTypeOrga.selected)
				typeOrga = "GEO";
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"updateNodeProperties",
																				UpdateNodePropertiesResult, throwError);
			_TypeOrga = typeOrga;
			RemoteObjectUtil.callService(op, _IDnode, txtComment.text, idOrga, typeOrga);
		}
		
		private function UpdateNodePropertiesResult(event: ResultEvent):void
		{
			_parentRef.updateSelectedOrgaType(_TypeOrga);
			closeWin(new CloseEvent("close"));
		}
		
		private function throwError(result:FaultEvent):void
		{
			
		}
	}
}