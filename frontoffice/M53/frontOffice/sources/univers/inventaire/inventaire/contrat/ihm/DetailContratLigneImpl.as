package univers.inventaire.contrat.ihm
{
	import composants.util.article.Article;
	import composants.util.contrats.Contrat;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.managers.PopUpManager;
	import mx.managers.SystemManager;
	
	import univers.inventaire.commande.system.Commande;
	import univers.inventaire.commande.system.TypesCommandesMobile;
	import univers.inventaire.contrat.system.ContratAbonnementMobile;
	
	[Bindable]
	public class DetailContratLigneImpl extends VBox implements IFenetreContrat
	{
		public function DetailContratLigneImpl()
		{
		}
		
		public var btValider:Button;
		
		
		private var _selectedContrat:ContratAbonnementMobile;
		private var _ppListeRessourcesLigne:ListeRessourcesIHM;
		private var _ppGererOptions:GererOptionsAbonnementIHM;				
		

		public function getDisplayObject():DisplayObject
		{	 
			return this;
		}
		
		protected function btValiderClickHandler(event:MouseEvent):void
		{
			//TODO: implement function
		}
		
		protected function btMajOptionsClickHandler(event:Event):void
		{
			_ppListeRessourcesLigne = new ListeRessourcesIHM();
			PopUpManager.addPopUp(_ppListeRessourcesLigne,SystemManager.getSWFRoot(this),true);
			PopUpManager.centerPopUp(_ppListeRessourcesLigne);
		}
		
		protected function btGererOptionsClickHandler(event:Event):void
		{
			var contrat:Contrat = new Contrat();
			
			contrat.IDCONTRAT = selectedContrat.IDCONTRAT;
			contrat.IDSOUS_TETE = selectedContrat.IDSOUS_TETE;
			contrat.SOUS_TETE = selectedContrat.SOUS_TETE;
			
			var commande:Commande = new Commande();
			commande.IDTYPE_COMMANDE = TypesCommandesMobile.RENOUVELLEMENT;
			var article:Article = new Article();
			
			var _ppRenouvellement:RenouvelerContratIHM = new RenouvelerContratIHM();
			_ppRenouvellement.boolCreate = true;
			_ppRenouvellement.commande = commande;
			_ppRenouvellement.article = article;
			_ppRenouvellement.contrat = contrat;
			
			//addPopup(_ppRenouvellement);
			/*_ppGererOptions = new GererOptionsAbonnementIHM();
			 _ppGererOptions.commande = commande;
			_ppGererOptions.article = article;
			_ppGererOptions.contrat = contrat; */
			
			
			PopUpManager.addPopUp(_ppRenouvellement,SystemManager.getSWFRoot(this),true);
			PopUpManager.centerPopUp(_ppRenouvellement);
		}
		
		
		
		public function initialiser():void
		{
			//TODO: implement function
		}
		

		public function set selectedContrat(value:ContratAbonnementMobile):void
		{
			_selectedContrat = value;
		}

		public function get selectedContrat():ContratAbonnementMobile
		{
			return _selectedContrat;
		}

	}
}