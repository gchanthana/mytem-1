package univers.inventaire.contrat.ihm
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Button;
	
	import system.Contrat;

	[Bindable]
	public class DetailContratDefautImpl extends VBox implements IFenetreContrat
	{		
		public var btValider:Button;
		
		private var _selectedContrat:Contrat;
						
		public function DetailContratDefautImpl()
		{	
			//TODO: implement function
		}

		public function getDisplayObject():DisplayObject
		{	 
			return this;
		}
		
		public function btValiderClickHandler(event:MouseEvent):void
		{
			//TODO: implement function
		}
		
		public function initialiser():void
		{
			//TODO: implement function
		}
		

		public function set selectedContrat(value:Contrat):void
		{
			_selectedContrat = value;
		}

		public function get selectedContrat():Contrat
		{
			return _selectedContrat;
		}
	}
}