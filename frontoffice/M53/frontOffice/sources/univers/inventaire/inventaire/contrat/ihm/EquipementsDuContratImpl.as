package univers.inventaire.contrat.ihm
{
	import flash.events.Event;
	
	import mx.containers.VBox;
	import mx.controls.DataGrid;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class EquipementsDuContratImpl extends VBox
	{
		
		public var dgListe:DataGrid;		
		private var _selectedContrat:Object;
		private var _selectedEquipement:Object;
		private var _listeEquipement:ListeEquipementsIHM;
		
		public function EquipementsDuContratImpl()
		{
		}
		
		public function set selectedContrat(value:Object):void
		{
			_selectedContrat = value;
		}

		public function get selectedContrat():Object
		{
			return _selectedContrat;
		}
		
		protected function _dgListeChangeHandler(event:ListEvent):void
		{
			if(dgListe.selectedIndex != -1)
			{
				selectedEquipement = dgListe.selectedItem;	
			}
			else
			{
				selectedEquipement = null;
			}
		}

		public function set selectedEquipement(value:Object):void
		{
			_selectedEquipement = value;
		}

		public function get selectedEquipement():Object
		{
			return _selectedEquipement;
		}
		
		///Handlers
		protected function btAjouterClickHandler(event:Event):void
		{
			_listeEquipement = ListeEquipementsIHM( PopUpManager.createPopUp(this,ListeEquipementsIHM,true));
			PopUpManager.centerPopUp(_listeEquipement);	
		}
	}
}