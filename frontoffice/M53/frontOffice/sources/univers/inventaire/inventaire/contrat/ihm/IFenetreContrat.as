package univers.inventaire.contrat.ihm
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	public interface IFenetreContrat
	{
		function getDisplayObject():DisplayObject;
		function initialiser():void;
	}
}