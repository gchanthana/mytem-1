package univers.inventaire.inventaire.menu.filtre
{
	import mx.controls.TextInput;
	
	/**
	 * Classe fournissant des methodes statiques pour filtrer une liste d'opération
	 * */
	public class FiltreOperation
	{
		/**
		 * Compare les valeurs des atribut TYPE_OPERATION, LIBELLE_OPERATIONS et REFERENCE_INTERNE d'un objet 
		 * avec le texte d'un TextInput 
		 * Retourne true si on retrouve la chaine du TextInput dans une des attributs cités sinon retourne false
		 * @param  item l'objet dont on veut comparer les attributs
		 * @param  txtFiltre le TextInput dont on veut comparer le texte
		 * @return true si ok sinon false
		 * */
		public static function lettersMatch(item : Object,txtFiltre : TextInput):Boolean{
			var bool : Boolean = false;			
			if(		(String(item.TYPE_OPERATION).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)  ||
					(String(item.LIBELLE_OPERATIONS).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) ||					 	   
					(String(item.REFERENCE_INTERNE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				)  
			{
				bool = true;
			}	
			return bool;
		}
		
		/**
		 * Determine si l'attribut EN_COURS de l'object passé en parametre est égal à 1 
		 * 1 = 'en cours' 
		 * Retourne true si c'est égal sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isEnCours(item : Object):Boolean{
			var bool : Boolean = false;
			if (parseInt(item.EN_COURS) == 1)
			{
			   	bool = true;
			}
			return bool;
		}
		
		
		
		/**
		 * Determine si l'attribut EN_COURS de l'object passé en parametre est égal à 0 
		 * 0 = 'cloturée' 
		 * Retourne true si c'est égal sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isTermine(item : Object):Boolean{
			var bool : Boolean = false;
			if (parseInt(item.EN_COURS) == 0){
			    	bool = true;
			}			
			return bool;
		}		
		
		
		/**
		 * Determine si l'attribut IDINV_ETAT de l'object passé en parametre est égal à 7 ou 8
		 * 7 = 'annulée avant resiliation',8 = 'annulee apres resiliation' 
		 * Retourne true si c'est égal sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isAnnulee(item : Object):Boolean{
			var bool : Boolean = false;
			if ((parseInt(item.IDINV_ETAT) == 7 )||(parseInt(item.IDINV_ETAT) == 8)){
			    	bool = true;
			}			
			return bool;
		}		
		
		/**
		 * Determine si l'attribut IDINV_ETAT de l'object passé en parametre est égal à 2 
		 * 2 = 'en attente d'envoi' 
		 * Retourne true si c'est égal sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isEnAttenteEnvoi(item : Object):Boolean{
			var bool : Boolean = false;
			if (parseInt(item.IDINV_ETAT) == 2){
			    	bool = true;
			}			
			return bool;
		}
		
		/**
		 * Determine si l'attribut IDINV_ETAT de l'object passé en parametre est égal à 3 
		 * 3 = 'en attente de resiliation' 
		 * Retourne true si c'est égal sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isEnAttenteResi(item : Object):Boolean{
			var bool : Boolean = false;
			if (parseInt(item.IDINV_ETAT) == 3){
			    	bool = true;
			}			
			return bool;			
		}
		
		/**
		 * Determine si l'attribut IDINV_ETAT de l'object passé en parametre est égal à 4 
		 * 4 = 'en attente de décision de vérification de la facturation' 
		 * Retourne true si c'est égal sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isEnAttenteCtrl(item : Object):Boolean{
			var bool : Boolean = false;
			if (parseInt(item.IDINV_ETAT) == 4){
			    	bool = true;
			}			
			return bool;	
		}
	}
}