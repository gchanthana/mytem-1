package univers.inventaire.inventaire.menu
{
	import composants.access.ListePerimetresWindow;
	import composants.util.ConsoviewFormatter;
	import composants.util.SearchTree;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Tree;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.cible.InvSearchTree;
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;
	import univers.inventaire.inventaire.recherche.IPanelRecherche;
	import composants.access.perimetre.tree.SearchPerimetreWindow;
	import composants.access.perimetre.tree.PerimetreTreeWindow;
	
	/**
	 * Classe onglet 'Gestion Directe' 
	 * 
	 * */	
	public class RechercheMenu extends RechercheMenuIHM implements IPanelRecherche
	{
		//Référence vers la methode distante chargeant la liste des opérateurs 
		private var opOperateur : AbstractOperation;				
		
		//le type de périmetre sur lequel on est
		private var  perimetre : String; 	
		
		//Référence vers l'identifiant du noeud sélectionné
		private var nodeId : int;  
		
		//Référence vers le libellé du noeud sélectionné
		private var nodeLibelle : String; 
		
		//Référence vers le type de perimetre du noeud sélectionné
		private var nodePerimetre : String;	
		
//-- PUBLIC ------------------------------------------------------------------------------------------------------------------------------------		
		
		/**
		 * Constructeur 
		 * */		
		public function RechercheMenu()
		{
			perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE		
			super();			
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
				
		/**
		*  Fait les actrions nécessaire au changement de périmetre
		* */
		public function onPerimetreChange():void{			
			remplirArbreOP();		
		}
		
		
//--- FIN PUBLIC -------------------------------------------------------------------------------------------------------------------------------		


//INITIALISATION -------------------------------------------------------------------------------------------------------------------------------
		
		//Initialisation de l'ihm apres l'évènement flexEvent.CREATION_COMPLETE
		//Mise en forme de l'arbre
		protected function initIHM(event:Event):void {
			//--- Arbre ------------
			myTree.percentHeight = 100;
			myTree.percentWidth = 100;
			SearchPerimetreWindow(myTree.searchTree).resultControl.setStyle("horizontalGap",0);
			SearchPerimetreWindow(myTree.searchTree).btnBack.width = 51;
			SearchPerimetreWindow(myTree.searchTree).searchInput.width = 75;
			PerimetreTreeWindow(myTree.perimetreTree).searchInput.width = 75;
			SearchPerimetreWindow(myTree.searchTree).searchInput.styleName = "TextInputInventaire";
			PerimetreTreeWindow(myTree.perimetreTree).searchInput.styleName = "TextInputInventaire";
			SearchPerimetreWindow(myTree.searchTree).btnBack.styleName = "MainButton";
			SearchPerimetreWindow(myTree.searchTree).searchTree.setStyle("borderStyle","solid");
			PerimetreTreeWindow(myTree.perimetreTree).perimetreTree.setStyle("borderStyle","solid");			
			SearchPerimetreWindow(myTree.searchTree).searchTree.liveScrolling = false;
			PerimetreTreeWindow(myTree.perimetreTree).perimetreTree.liveScrolling = false;
			
			SearchPerimetreWindow(myTree.searchTree).lblRecherche.visible = false;
			SearchPerimetreWindow(myTree.searchTree).lblRecherche.width = 0;	
			SearchPerimetreWindow(myTree.searchTree).bxMain.setStyle("paddingRight",0);
			SearchPerimetreWindow(myTree.searchTree).bxMain.setStyle("paddingLeft",0);
			
			myTree.addEventListener(InvSearchTree.NODE_CHANGED,chargerListeRessources);		
		}
		
//FIN INITIALISATION ---------------------------------------------------------------------------------------------------------------------------


//ARBRE OPERATEURS -----------------------------------------------------------------------------------------------------------------------------
		
		
		//Positionne les variables globales de noeud au clique sur un noeud de l'arbre 
		//les cliques sur un noeud de type logique analytique(Historisé) est ignoré
		private function chargerListeRessources(e : Event):void{
			if (myTree.nodeInfo != null && myTree.nodeInfo.hasOwnProperty("TYPE_LOGIQUE")){
				if (String(myTree.nodeInfo.TYPE_LOGIQUE) == "ANA"){					 
					SearchPerimetreWindow(myTree.searchTree).searchTree.selectedIndex = -1;
					PerimetreTreeWindow(myTree.perimetreTree).perimetreTree.selectedIndex = -1;
					myTree.nodeInfo = null;
					unSetSelectedNodeParams();
					e.preventDefault();
				}else if (myTree.nodeInfo.TYPE_PERIMETRE=="GroupeLigne" ){					
					setSelectedNodeParams(myTree.nodeInfo);	 
					myTree.nodeInfo = null;
				}
			}else{
				SearchPerimetreWindow(myTree.searchTree).searchTree.selectedIndex = -1;
				PerimetreTreeWindow(myTree.perimetreTree).perimetreTree.selectedIndex = -1;
				myTree.nodeInfo = null;
				unSetSelectedNodeParams();				
				e.preventDefault();
			}	
		}
		
		//Met à jour les variables globales de noeud. 
		//Dispatche un evenement RechercheMenuEvent de type 'GlSelectionne' contenant les parametre du noeud sélectionné
		private function setSelectedNodeParams(node : Object):void{		
			nodeId = node.NID;
			nodeLibelle =  node.LBL; 
			nodePerimetre =  node.TYPE_PERIMETRE;
			
			var evtObj : RechercheMenuEvent = new RechercheMenuEvent("GlSelectionne");				
			evtObj.nodePerimetre = node.TYPE_PERIMETRE;			
			evtObj.nodeLibelle = node.LBL;		
			evtObj.nodeId = node.NID;
					
			dispatchEvent(evtObj);
		}
		
		
		//Remet à zero les parametre gloable de noeud.		
		//Dispatche un evenement RechercheMenuEvent de type 'ResetGDirect' contenant les parametre de noeud vide
		private function unSetSelectedNodeParams():void{			
			nodeId = -1;
			nodeLibelle = "";
			nodePerimetre = "";		
			
			var evtObj : RechercheMenuEvent = new RechercheMenuEvent("ResetGDirect");				
			dispatchEvent(evtObj);
		}
		
		
		//Reinitilalise l'arbre avec le perimetre courant
		//Params in operateurId optionnel
		private function remplirArbreOP(opid : int = 0):void{		
			boxTree.removeChildAt(1);			
			myTree = null
			myTree = new InvSearchTree();		
			boxTree.addChildAt(myTree,1);
			initIHM(null);
		}
		
//FIN ARBRE OPERATEUR --------------------------------------------------------------------------------------------------------------------------

//NAVIGATION -----------------------------------------------------------------------------------------------------------------------------------
		
		
		//Reinitilalise l'onglet avec le perimetre courant
		private function reset():void{
			try{
				remplirArbreOP();
			}catch( e : Error ){
				trace("reset error ");
			}  
		}
		
//FIN NAVIGATION -------------------------------------------------------------------------------------------------------------------------------
		

//REMOTINGS --------------------------------------------------------------------------------------------------------------------
		
		
		//Va chercher la liste des operateurs disponible pour le perimetre
		private function getListeOperateur():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var typeGroupe : String = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			
			opOperateur = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"getListeOperateur",
																				getListeOperateurResultHandler
																				);				
			RemoteObjectUtil.callService(opOperateur,idGroupeMaitre
										   ,typeGroupe);		
		}
		
		//Ne fait rien
		private function getListeOperateurResultHandler(re :ResultEvent):void{
			//cbxOperateur.dataProvider = re.result as ArrayCollection;
			//(cbxOperateur.dataProvider as ArrayCollection).refresh();
			//cbxOperateur.labelField = "OPNOM"; 
		}
						
	}
}