package univers.inventaire.inventaire.menu.filtre
{
	import mx.controls.TextInput;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.info.StatuTInfo;   
	
	public class FiltreCommande
	{
		/**
		 * Compare les valeurs des atribut TYPE_LIBELLE, LIBELLE_OPERATIONS et REFERENCE_INTERNE d'un objet 
		 * avec le texte d'un TextInput 
		 * Retourne true si on retrouve la chaine du TextInput dans une des attributs cités sinon retourne false
		 * @param  item l'objet dont on veut comparer les attributs
		 * @param  txtFiltre le TextInput dont on veut comparer le texte
		 * @return true si ok sinon false
		 * */
		public static function lettersMatch(item : Object,txtFiltre : TextInput):Boolean{
			var bool : Boolean = false;			
			if  ((String(item.LIBELLE_OPERATIONS).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) ||					 	   				 	   
				 (String(item.REFERENCE_INTERNE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
			{
				bool = true;
			}	
			return bool;
		}
		 
		
		/** 
		 * Si l'attribut TYPE de l'objet passé en param est egal à 1 (pour commande) 
		 * determine si l'attribut EN_COURS_COM  de l'object passé en parametre est égal à 1, 2 ou 3 		 
		 * 1 = 'commande en attentde d envoi'
		 * 2 = 'commande en attentde de livraison'  
		 * 3 = 'commande en attente de decision de verifiaction de la factration'
		 * 
		 * ou si l'attribut TYPE de cet objet est egal à 2 (pour operation), determine si l'attribut IDINV_ETAT  de l'object passé en parametre est différent de 6 		 
		 * 6 = 'operation cloturé'		
		 * 
		 * Retourne true si c'est vrai sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isEnCours(item : Object):Boolean{
			var bool : Boolean = false;
			if (item.EN_COURS == 1){
			    bool = true;
			}
			return bool;
		}
		
		
		/**
		 * Determine si l'attribut IDINV_ETAT de l'object passé en parametre est différent de 6 (Opération cloturée) 
		 * et si l'attribut TYPE de ce meme objet est égale à 2(operation)
		 * Retourne true si c'est vrai sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isTermine(item : Object):Boolean{
			var bool : Boolean = false;
			if (item.CODE_ETAT == "T"){
			    bool = true;
			}
			return bool;
		}		
		
		
		/**
		 * Determine si l'attribut EN_COURS_COM de l'object passé en parametre est égal à 1(en attente d'envoi) 
		 * et si l'attribut TYPE de ce meme objet est égale à 1(commande)
		 * Retourne true si c'est vrai sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isEnAttenteEnvoi(item : Object):Boolean{
			var bool : Boolean = false;
			if (item.CODE_ETAT == "AE"){
			    bool = true;
			}			
			return bool;
		}
		
		
		/**
		 * Determine si l'attribut EN_COURS_COM de l'object passé en parametre est égal à 2(en attente de livraison) 
		 * et si l'attribut TYPE de ce meme objet est égale à 1(commande)
		 * Retourne true si c'est vrai sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isEnAttenteLivraison(item : Object):Boolean{
			var bool : Boolean = false;
			if (item.CODE_ETAT == "AL"){
			    bool = true;
			}			
			return bool;			
		}
		
		/**
		 * Determine si l'attribut EN_COURS_COM de l'object passé en parametre est égal à 3(en attente de décision de verification de la facturation) 
		 * et si l'attribut TYPE de ce meme objet est égale à 1(commande)
		 * Retourne true si c'est vrai sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isEnAttenteCtrl(item : Object):Boolean{
			var bool : Boolean = false;
			if (item.CODE_ETAT == "AC"){
			    bool = true;
			}				
			return bool;	
		}
		
		/**
		 * Determine si l'attribut EN_COURS_COM de l'object passé en parametre est égal à 1022(en attente de rapprochement) 
		 * et si l'attribut TYPE de ce meme objet est égale à 1(Commande)
		 * Retourne true si c'est vrai sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isEnAttenteRapprochement(item : Object):Boolean{
			var bool : Boolean = false;
			if (item.CODE_ETAT == "AR"){
				bool = true;
			}
			return bool;			
		}
		
		/**
		 * Determine si l'attribut EN_COURS_COM de l'object passé en parametre est égal à 22 ou 23(commande annulée avent et apres envoi) 
		 * et si l'attribut TYPE de ce meme objet est égale à 1(commande)
		 * Retourne true si c'est vrai sinon retourne false
		 * @param item 
		 * @return true ou false 
		 * */
		public static function isAnnulee(item : Object):Boolean{
			var bool : Boolean = false;
			if (item.CODE_ETAT == "A"){
			    bool = true;
			}		
			return bool;	
		}
	}
}