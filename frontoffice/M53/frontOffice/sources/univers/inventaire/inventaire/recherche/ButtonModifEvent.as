package univers.inventaire.inventaire.recherche
{
	import flash.events.Event;

	public class ButtonModifEvent extends Event
	{
		private var _rowIndex : int;
		
		public function ButtonModifEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		public function set rowIndex(ri : int):void{
			_rowIndex = ri;
		}
		
		public function get rowIndex():int{
			return _rowIndex;	
		}	
		
	}
}