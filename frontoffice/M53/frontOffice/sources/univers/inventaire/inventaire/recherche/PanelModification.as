package univers.inventaire.inventaire.recherche
{
	import mx.events.FlexEvent;
	import mx.collections.ArrayCollection;
	import flash.events.MouseEvent;
	import mx.managers.PopUpManager;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	public class PanelModification extends PanelModificationIHM 
	{
		private var _historique : ArrayCollection; 
		private var _dateAction : String;
		private var _commentaire : String;
		private var _idAction : int;
		private var _rowIndex : int;
			
		public function PanelModification()
		{
			//TODO: implement function
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		public function set historique(h : ArrayCollection):void{
			_historique = h;
		}
		
		public function set commentaire(c : String):void{
			_commentaire = c;
		}
		
		public function set dateAction(d : String):void{
			_dateAction = d;
		}
		
		public function set idAction(idac : int):void{
			_idAction = idac;
		}
		
		public function get idAction():int{
			return _idAction;
		}
		
		public function set rowIndex(ri : int):void{
			_rowIndex = ri;
		}
		
		public function get rowIndex():int{			
			return _rowIndex;
		}
		
		
		//initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{
			btEnregistrer.addEventListener(MouseEvent.CLICK,validerEnregistrerHandler);
			btAnnuler.addEventListener(MouseEvent.CLICK,annulerEnregistrerHandler);
			try{
				dcDateAction.selectedDate = new Date(_dateAction);
				dcDateAction.selectableRange = setSelectableRange(_idAction);
				txtCommentaire.text = _commentaire;
			}catch(e : Error){
				trace("Mettre la date et le commentaire");
			}		
			PopUpManager.centerPopUp(this);	
		}
		
		//valider la modif		
		private function validerEnregistrerHandler(me : MouseEvent):void{
			var mde : ModifiactionEvent = new ModifiactionEvent(ModifiactionEvent.MODIFICATION);			
			mde.commentaire = txtCommentaire.text;
			mde.dateAction = dcDateAction.text;	
			mde.idAction = _idAction;					
			dispatchEvent(mde);
			PopUpManager.removePopUp(this);
		}
		
		//annuler la modif
		private function annulerEnregistrerHandler(me : MouseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		
		//retrouve l'index de l'etape dans l'historique via l'idAction 
		private function chercherIndexHistoriqueEtat(idopAction : int):int{			 		
			var i : int;
			for (i = 0; i <  _historique.length; i++){
				if (idopAction == _historique[i].IDINV_OP_ACTION ){										 
					return i;
				}
			}			
			return -1;
		}
		
		//config du DateChooser
		private function setSelectableRange(idopAction : int):Object{
			var LAST_POSITION : int = _historique.length - 1;
			var FIRST_POSITION : int = 0;  
			var positionIndex : int = chercherIndexHistoriqueEtat(_idAction);;
						
			var selectableRange : Object = new Object();	 
						
			if (positionIndex == LAST_POSITION ){				
				try{
					selectableRange.rangeStart = new Date(_historique[LAST_POSITION - 1].DATE_ACTION);								
				}catch(re : RangeError){
					trace("[LAST_POSITION] pas d'étape avant celle ci ");
					selectableRange.rangeStart =  new Date(1970,0,1);
				}catch(e : Error){
					trace("[LAST_POSITION] erreur ");
				}finally{ 
					selectableRange.rangeEnd = new Date(2999,0,1); 			
				}
			}else if (positionIndex == FIRST_POSITION ){
					try{
						selectableRange.rangeEnd = new Date(_historique[positionIndex + 1].DATE_ACTION);
					}catch(re : RangeError){
						trace("[FIRST_POSITION] pas d'étape apres celle ci ");
						selectableRange.rangeEnd = new Date(2999,0,1);
					}catch(e : Error){
						trace("[FIRST_POSITION] erreur ");
					}finally{
						selectableRange.rangeStart = new Date(1970,0,1);				
					}
			}else{					
							
					selectableRange.rangeStart = new Date(_historique[positionIndex - 1].DATE_ACTION);
					selectableRange.rangeEnd = new Date(_historique[positionIndex + 1].DATE_ACTION);				
			}
					
			return selectableRange;
		}	
		
	}
}