package univers.inventaire.inventaire.creation.defaultpanel
{
	import mx.events.FlexEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 *Classe gerant un panel vide 
	 **/
	public class DefaultPanel extends DefaultPanelIHM
	{
		/**
		 * Constructeur
		 * */
		public function DefaultPanel()
		{
			//TODO: implement function
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		//Initialisation de 'IHM
		private function initIHM(fe : FlexEvent):void{
			btSortir.addEventListener(MouseEvent.CLICK,sortir);			
		}
		
		
		//Dispatche un évenement de type 'sortir' signifiant que l'on veut sortir du module
		private function sortir(me : MouseEvent):void{
			dispatchEvent(new Event("sortir"));
		}		
		
	}
}