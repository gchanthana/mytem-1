package univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts
{
	public dynamic class ContactCache
	{
		public var operateurIndex : int = 0;
		public var agenceIndex : int = 0;
		public var contactIndex : int = 0;
	}
}