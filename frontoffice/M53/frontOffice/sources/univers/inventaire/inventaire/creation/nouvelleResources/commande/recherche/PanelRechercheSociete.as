package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche
{
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.events.FlexEvent;
	import flash.events.MouseEvent;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.DetailSociete;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
	import flash.events.Event;
	
	import composants.util.ConsoviewUtil;
	import mx.collections.ArrayCollection;
	import flash.display.DisplayObject;
	
	/**
	 * Classe gerant l'ecran de recherche d'un distributeur ou d'une Agence
	 * */
	public class PanelRechercheSociete extends PanelRechercheSocieteIHM
	{
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la validation de sélection d'une société (agence ou distributeur)
		 * */
		public static const VALIDER : String = "societeSelected";
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la suppression d'un société (agence ou distributeur)
		 * */
		public static const EFFACER : String = "societDeleted";
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la Creatiton d'un société (agence ou distributeur)
		 * */
		public static const CREER : String = "societeCreated";
		
		//Reference vers l'ecran détail d'une societe
		private var detailSociete : DetailSociete;		
		
		//Reference vers l'objet gerant les societes
		private var societe : Societe;
		
		//Reference vers l'identifiant du groupe sur lequel on est logué
		private var idGroupe : Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
		
		//Booléen placé a true si le composant est éditable
		private var _editable : Boolean = true;
		
		//Reference locale vers l'id de la societe sélectionnée
		private var _societeSelectionneeID : int;
		
		/**
		 * Retourne l'identifiant de la société sélectionnée
		 * @return _societeSelectionneeID 
		 * */
		public function get societeSelectionneeID():int{
			return _societeSelectionneeID;
		}
		
		/**
		 * Constructeur
		 * @param editable ,true si le composant est éditable
		 * */
		public function PanelRechercheSociete(editable : Boolean = true)
		{
			//TODO: implement function
			super();
			_editable = editable;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			
		}
		
		/**
		 * Ne fait rien
		 * */
		public function clean():void{
			societe.clean();
			if (detailSociete != null) detailSociete.clean();
			detailSociete = null;
			PopUpManager.removePopUp(this);
			
		}
		
		
		//Initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{
			addEventListener(CloseEvent.CLOSE,fermerPanel);	
			
			if (_editable){
				btSuppSociete.visible = false;
				btAjouterSociete.visible = true;
				btSuppSociete.addEventListener(MouseEvent.CLICK,clickEffacerSociete);			
				btAjouterSociete.addEventListener(MouseEvent.CLICK,clickAddSociete);	
				btVoir.addEventListener(MouseEvent.CLICK,clickVoirSociete);
			}else{
				btSuppSociete.visible = false;
				btAjouterSociete.visible = false;
				btVoir.visible = false;
			}

			btAnnuler.addEventListener(MouseEvent.CLICK,annuler);
			btValider.addEventListener(MouseEvent.CLICK,clickValider);
			txtFiltre.addEventListener(Event.CHANGE,filtrerGrid);
			
			societe = new Societe();
			societe.addEventListener(Societe.LISTE_COMPLETE,afficherSocietes);
			societe.addEventListener(Societe.DELETE_COMPLETE,societeEfface);
			societe.prepareList(idGroupe);
		}
		
		
		
		//Ferme le panel apres un appui sur la croix
		private function fermerPanel(ce : CloseEvent):void{
			clean();
			PopUpManager.removePopUp(this);	
		}	
		
		//Ferme le panel apres un appui sur 'Annuler'
		private function annuler(me : MouseEvent):void{
			clean();
			PopUpManager.removePopUp(this);
		}	
		
		//Gere le click sur le bouton valider, dispatche un évenement signifiant que l'on a valider la sélection
		private function clickValider(me : MouseEvent):void{			
			if (myGridSociete.selectedIndex != -1){
				_societeSelectionneeID = Societe(myGridSociete.selectedItem).societeID;		
				dispatchEvent(new Event(PanelRechercheSociete.VALIDER));
				PopUpManager.removePopUp(this);
			}	
		}
		
		
		//Gere le click sur le bouton 'Voir la Société sélectionnée'. Affiche une fenêtre avec le détail de la société sélectionnée
		private function clickVoirSociete(me : MouseEvent):void{
			if ((myGridSociete.selectedIndex != -1)&&(Societe(myGridSociete.selectedItem).societeID != 0)){
				detailSociete = new DetailSociete(Societe(myGridSociete.selectedItem));
				detailSociete.addEventListener(DetailSociete.VALIDER,societeCreee);				
				ConsoviewUtil.scale(DisplayObject(detailSociete),moduleCommandeMobileIHM.W_ratio,moduleCommandeMobileIHM.H_ratio);
				PopUpManager.addPopUp(detailSociete,this,true);
				PopUpManager.centerPopUp(detailSociete);									
			}
		}	
		
		//Gere le click sur le bouton 'Effacer société'. Supprime la société sélectionnée
		private function clickEffacerSociete(me : MouseEvent):void{
			if (myGridSociete.selectedIndex != -1){
				societe.deleteSociete(Societe(myGridSociete.selectedItem).societeID);		
			}								
		}	
		
		//Gere le click sur le bouton 'Ajouter une societe'. Affiche une fenêtre avec un formulaire d'ajout d'une société
		private function clickAddSociete(me : MouseEvent):void{			
				detailSociete = new DetailSociete();
				detailSociete.addEventListener(DetailSociete.VALIDER,societeCreee);
				ConsoviewUtil.scale(DisplayObject(detailSociete),moduleCommandeMobileIHM.W_ratio,moduleCommandeMobileIHM.H_ratio);
				PopUpManager.addPopUp(detailSociete,this,true);
				PopUpManager.centerPopUp(detailSociete);				
		}	
		
		//Gere le filtre sur le DataGrid 	
		private function filtrerGrid(ev : Event):void{
			if (societe.liste != null){
				societe.liste.filterFunction = filtrerFunc;
				societe.liste.refresh();
			}			
		}
		
		//Filtre les elements du DataGrid sur les propriétés raisonSociale, listeOperateurStr, commune 
		private function filtrerFunc(value : Object):Boolean{			
			if (value.commune == null) value.commune = " ";
			if (value.listeOperateurStr == null) value.listeOperateurStr = " ";
			if (value.raisonSociale == null) value.raisonSociale = " ";
			
			if ((String(value.raisonSociale.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
					||
				(String(value.listeOperateurStr.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
					||  
				(String(value.commune.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1))
		    {
				return (true);
			} else {
				return (false);
			}			
		}	
		
		//affiche les sociétés dans le DataGrid 
		private function afficherSocietes(ev : Event):void{
			myGridSociete.dataProvider = societe.liste;
			if (societe.liste.length > 0) txtFiltre.enabled = true
			else txtFiltre.enabled = false;
			callLater(selectSociete);
			
		}
		
		//Met en surbrillance la societé nouvellement créée.
		private function selectSociete():void{
			if (detailSociete != null){
				try{
					var index : int = ConsoviewUtil.getIndexById(myGridSociete.dataProvider as ArrayCollection,
																				"societeID",detailSociete.newSocieteID);
					myGridSociete.selectedIndex = index;
					myGridSociete.scrollToIndex(index);																				
				}catch(e : Error){
					trace(e.getStackTrace());
				}	
			}
		}
		
		
		//Recharge la liste de sociétés
		private function rafraichirListeSocietes():void{			
			societe.addEventListener(Societe.LISTE_COMPLETE,afficherSocietes);
			societe.prepareList(idGroupe);
		}
		
		//Rafraichit la liste des societes et dispatche un évènement signifiant qu'une société vient d'être créée
		private function societeCreee(ev : Event):void{			
			rafraichirListeSocietes();
			dispatchEvent(new Event(PanelRechercheSociete.CREER));
		}		
			
		//Rafraichit la liste des societes et dispatche un évènement signifiant qu'une société vient d'être effacée
		private function societeEfface(ev : Event):void{
			rafraichirListeSocietes();
			dispatchEvent(new Event(PanelRechercheSociete.EFFACER));
		}
	}	
}