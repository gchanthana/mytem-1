package univers.inventaire.inventaire.creation.nouvelleResources.commande.info
{
	import composants.controls.TextInputLabeled;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	
	import mx.containers.Canvas;
	import mx.controls.Label;
	import mx.controls.TextArea;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;

	public class ConsulterInformations extends Canvas
	{
		
		/**
		 * Reference vers le TextInputLabeled affichant le libellé
		 * */
		public var txtLibelle : TextInputLabeled;
				
		/**
		 * Reference vers le TextInputLabeled affichant la référence client
		 * */
		public var txtReferenceCli : TextInputLabeled;
		
		/**
		 * Reference vers le TextInputLabeled affichant la référence opérateur
		 * */
		public var txtReferenceOpe : TextInputLabeled;		
		
		/**
		 * Reference vers le TextInputLabeled affichant la date d'effet au format jj/mm/aaaa
		 * */
		public var txtDateEffet : TextInputLabeled;
		
		/**
		 * Reference vers le TextInputLabeled affichant la date de livraison prévue au format jj/mm/aaaa
		 * */
		public var txtDateLivraisonPrevue : TextInputLabeled;
		
		/**
		 * Reference vers le TextInputLabeled affichant la date d'envoi au format jj/mm/aaaa
		 * */	
		public var txtDateEnvoi : TextInputLabeled;
		
		/**
		 * Reference vers le TextInputLabeled affichant le commentaire
		 * */
		public var txtCommentaire : TextArea;				
		
		/**
		 * Reference vers le Label d'affichage du statut de la commande
		 * */
		public var lblStatut : Label;
		
		
		/**
		 * Reference vers le TextInputLabeled affichant la date de livraison au format jj/mm/aaaa
		 * */	
		public var txtDateLivraison : TextInputLabeled;
				
		//Reference locale vers la commande
		private var _commande : Commande;
		
		/**
		 * Constructeur
		 * */
		public function ConsulterInformations()
		{
			super();
		}
		
		/**
		 * Affiche les informations de la commande passée en parametre
		 * @param cde une commande
		 * */
		public function afficherInfosCommande(cde : Commande):void{
			_commande = cde;	
			_commande.addEventListener(Commande.UPDATESTATUT_COMPLETE,commandeStatutUpdated);			
			
			txtLibelle.text = _commande.libelle;
			txtReferenceCli.text = _commande.refClient
			txtReferenceOpe.text = _commande.refOperateur;
			txtCommentaire.text = _commande.commentaire;
			txtDateEffet.text = (_commande.dateEffective != null)? DateFunction.formatDateAsString(new Date(_commande.dateEffective)) : " ";
			txtDateLivraisonPrevue.text = (_commande.dateLivraisonPrevue != null)? DateFunction.formatDateAsString(new Date(_commande.dateLivraisonPrevue)) : " ";
			txtDateEnvoi.text = (_commande.dateEnvoi != null)? DateFunction.formatDateAsString(new Date(_commande.dateEnvoi)) : " ";
			txtDateLivraison.text = (_commande.dateLivraison != null)? DateFunction.formatDateAsString(new Date(_commande.dateLivraison)) : " ";
			lblStatut.text = _commande.statut;
		}
		
		
		/**
		 * Change le statut de la commande
		 * @param idStatut l'identifiant du nouveau statut
		 * 
		 * */
		public function changerStatutCommande(idStatut : int):void{
			if (_commande.commandeID) {
				_commande.statutID = idStatut;
				_commande.updateStatut();				
			}
		}
		
		//Affiche le nouveau statut de la commande
		private function commandeStatutUpdated(ev : Event):void{
			lblStatut.text = _commande.statut;				
		}
				
	}
}