package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche
{
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import flash.events.MouseEvent;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.DetailContact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Contact;
	import flash.events.Event;
	
	import composants.util.ConsoviewUtil;
	import mx.collections.ArrayCollection;
	import flash.display.DisplayObject;
	
	
	/**
	 * Classe gerant l'ecran de recherche d'un contact
	 * */
	public class PanelRechercheContact extends PanelRechercheContactIHM
	{	
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la validation de sélection d'un contact
		 * */
		public static const VALIDER : String = "contactSelected";
		
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la supression d'un contact
		 * */
		public static const EFFACER : String = "contactDeleted";
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la création d'un contact
		 * */
		public static const CREER : String = "contactCreated";
		
		//Reference vers l'ecran détail d'un contact
		private var detailContact : DetailContact;
		
		//Reference vers l'objet gerant les contacts
		private var contacts : Contact;
		
		//Reference vers l'identifiant du groupe sur lequel on est logué
		private var idGroupe : Number =  CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
		
		//Booléen placé a true si le composant est éditable
		private var _editable : Boolean = true;
		
		//Reference locale vers le contact sélectionné
		private var _contactSelectionne : Contact;
		
		/**
		 * Retourne une refernece vers le contact sélectionné
		 * @return _contactSelectionne 
		 * */
		public function get contactSelectionne():Contact{
			return _contactSelectionne;
		} 
		
		/**
		 * Constructeur
		 * @param editable ,true si le composant est éditable
		 * */		
		public function PanelRechercheContact(editable : Boolean = true){
			//TODO: implement function
			super();
			_editable = editable;			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);								
		}
						
		/**
		 * Ne fait rien
		 * */
		public function clean():void{
			if (detailContact != null) detailContact.clean();
			detailContact = null;
			contacts.clean();
			PopUpManager.removePopUp(this);
		}
		
		//Initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{
			addEventListener(CloseEvent.CLOSE,fermerPanel);	
			
			if(_editable){
				btAjouterContact.visible = true;
				btSuppContact.visible = false;
				btAjouterContact.addEventListener(MouseEvent.CLICK,clickAddContact);
				btSuppContact.addEventListener(MouseEvent.CLICK,clickEffacerContact);	
				btVoir.addEventListener(MouseEvent.CLICK,clickVoirContact);	
			}else{
				btAjouterContact.visible = false;
				btSuppContact.visible = false;
				btVoir.visible = false;
			}
			
			btAnnuler.addEventListener(MouseEvent.CLICK,annuler);
			btValider.addEventListener(MouseEvent.CLICK,clickValider);
			
			txtFiltre.addEventListener(Event.CHANGE,filtrerGrid);
			
									
			contacts	= new Contact();
			contacts.addEventListener(Contact.LISTE_COMPLETE,afficherContacts);
			contacts.addEventListener(Contact.DELETE_COMPLETE,contactEfface);									
						
			
			contacts.prepareList(idGroupe);						
		}
		
		//Ferme le panel apres un appui sur la croix
		private function fermerPanel(ce : CloseEvent):void{
			clean();
			PopUpManager.removePopUp(this);	
		}
		
		//Ferme le panel apres un appui sur 'Annuler'
		private function annuler(me : MouseEvent):void{
			clean();
			PopUpManager.removePopUp(this);
		}
		
		//Gere le click sur le bouton valider, dispatche un évenement signifiant que l'on a valider la sélection
		private function clickValider(me : MouseEvent):void{			
			if (myGridContact.selectedIndex != -1){
				_contactSelectionne = Contact(myGridContact.selectedItem);		
				dispatchEvent(new Event(PanelRechercheContact.VALIDER));
				PopUpManager.removePopUp(this);
			}	
		}
		
		//Gere le click sur le bouton 'Voir le contact sélectionné'. Affiche une fenêtre avec le détail du contact
		private function clickVoirContact(me : MouseEvent):void{
			if (myGridContact.selectedIndex != -1){
				detailContact = new DetailContact(Contact(myGridContact.selectedItem));
				detailContact.addEventListener(DetailContact.VALIDER,contactCree);
				ConsoviewUtil.scale(DisplayObject(detailContact),moduleCommandeMobileIHM.W_ratio,moduleCommandeMobileIHM.H_ratio);
				PopUpManager.addPopUp(detailContact,this,true);
				PopUpManager.centerPopUp(detailContact);					
			}								
		}	
		
		//Gere le click sur le bouton 'Effacer contact'. Supprime le contact sélectionné
		private function clickEffacerContact(me : MouseEvent):void{
			if (myGridContact.selectedIndex != -1){
				contacts.deleteContact(Contact(myGridContact.selectedItem).contactID);		
			}								
		}	
		
		//Gere le click sur le bouton 'Ajouter un contact'. Affiche une fenêtre avec un formulaire d'ajout de contact
		private function clickAddContact(me : MouseEvent):void{			
			detailContact = new DetailContact();
			detailContact.addEventListener(DetailContact.VALIDER,contactCree);
			ConsoviewUtil.scale(DisplayObject(detailContact),moduleCommandeMobileIHM.W_ratio,moduleCommandeMobileIHM.H_ratio);
			
			PopUpManager.addPopUp(detailContact,this,true);
			PopUpManager.centerPopUp(detailContact);				
		}	
		
		//Gere le filtre sur le DataGrid 			
		private function filtrerGrid(ev : Event):void{
			if (contacts.liste != null){
				contacts.liste.filterFunction = filtrerFunc;
				contacts.liste.refresh();
			}			
		}
		
		//Filtre les elements du DataGrid sur les propriétés nom, prenom, societeNom 
		private function filtrerFunc(value : Object):Boolean{
			if (value.prenom == null) value.prenom = " ";
			if ((String(value.nom.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
					||
				(String(value.prenom.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
					||
				(String(value.societeNom.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1))
		    {
				return (true);
			} else {
				return (false);
			}			
		}
		
		//Affiche les contacts dans le DataGrid 
		////Met en surbrillance le contact nouvellement créé s'il y a un.
		private function afficherContacts(ev : Event):void{
			myGridContact.dataProvider = contacts.liste;
			if (contacts.liste.length > 0) txtFiltre.enabled = true
			else txtFiltre.enabled = false;
			
			if (detailContact != null){
				try{
					var index : int = ConsoviewUtil.getIndexById(myGridContact.dataProvider as ArrayCollection,
																				"contactID",detailContact.newContactID);
					myGridContact.selectedIndex = index;
					myGridContact.scrollToIndex(index);																				
				}catch(e : Error){
					trace(e.getStackTrace());
				}	
			}
			
		}
		
		//Recharge la liste de contacts
		private function rafraichirListeContacts():void{			
			contacts.addEventListener(Contact.LISTE_COMPLETE,afficherContacts);
			contacts.prepareList(idGroupe);
		}
		
		//Rafraichit la liste de contact et dispatche un évènement signifiant qu'un contact vient d'être créé
		private function contactCree(ev : Event):void{			
			rafraichirListeContacts();
			dispatchEvent(new Event(PanelRechercheContact.CREER));
		}		
		
		//Rafraichit la liste de contact et dispatche un évènement signifiant qu'un contact vient d'être supprimé
		private function contactEfface(ev : Event):void{
			rafraichirListeContacts();
			dispatchEvent(new Event(PanelRechercheContact.EFFACER));
		}
	}
}