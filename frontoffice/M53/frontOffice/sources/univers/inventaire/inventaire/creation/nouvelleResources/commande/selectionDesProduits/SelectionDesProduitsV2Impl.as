package univers.inventaire.inventaire.creation.nouvelleResources.commande.selectionDesProduits
{
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.DataGridEvent;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;
	
	
	[Event(name="AnnulerChangementOperateur", type="flash.events.Event")]
	
	[Bindable]
	public class SelectionDesProduitsV2Impl extends VBox
	{
		public var txtFiltreCat : TextInputLabeled;
		public var txtFiltreCom : TextInputLabeled;
		public var cmbTheme : ComboBox;
		public var myGridCatalogue : DataGrid;
		public var myGridProduits : DataGrid;
		public var btAllUp : Image;
		public var btDown : Image;
		public var btUp : Image;
		
		
		protected var _panier : ElementCommande;
		
		//La référence vers la commande
		protected var _commande : Commande;
		
		////La référence vers les lignes ajouter à la commande
		protected var _tabLignes : Array;
		
		//La référence vers l'identifiant de l'ancien opérateur selectionné
		public var oldOperateur : int;
		
		//La référence vers l'identifiant du nouvel opérateur selectionné
		protected var newOperateur : int;
		
		//Référence temporaire vers une ligne de la commande
		protected var tmpElement : ElementCommande;
		
		
		
		//La référence vers la méthode distante ramenant la liste des themes
		protected var opTheme : AbstractOperation;
		
		//La référence vers la méthode distante	la liste des opérateurs	
		protected var opOperateur : AbstractOperation;
		
		
		private var _operateurSelectionne : Object = {};
		private var _lastOp : Object = {};
		
		public function set operateurSelectionne(op : Object):void{
			_operateurSelectionne = op;
			getTheme();
		}
		public function get operateurSelectionne():Object{
			return _operateurSelectionne; 
		}
		
		/**
		 * Constante definissant le type d'evenement dispatché lors d'un changement de libelle de ligne
		 * d'un élément de la commande
		 * */
		public static const LIBELLE_LIGNE_CHANGED : String = "libelleLigneCHanged"; 
		
		/**
		 * Ne fait rien pour le moment
		 * */
		public function clean():void{
			 
		}
		
		
		
		public function SelectionDesProduitsV2Impl()
		{
		}
		
		/**
		 * setteur pour le tableau contenant la liste des lignes ajoutées à la commande
		 * @param tab le tableau de lignes
		 * */		
		public function set tabLignes(tab : Array):void{
			if (_tabLignes != null) _tabLignes = null;
			_tabLignes = tab;			
		}
		public function get tabLignes():Array{
			return _tabLignes;			
		}
		
		
				 
		 
		/**
		 * setteur pour la commande et le panier
		 * @param cmde la commande
		 * @param panier le panier
		 * */		
		public function setCommande(cmde : Commande,panier : ElementCommande):void{
			_commande = cmde;
			_panier = panier;
		}
		
		/**
		 * Efface la selection
		 * */
		public function clearSelection():void{
			
			
			
			monterTousLesProduits(null);
			
			myGridProduits.dataProvider = null;		
			myGridCatalogue.dataProvider = null;
			
			_panier.liste = null;
			_panier.liste = new ArrayCollection();
			_panier.liste.refresh();
		
		}	
		
		//Initialisation de lIHM
		protected function initIHM(fe : FlexEvent):void{		
							
			
		 

			cmbTheme.addEventListener(Event.CHANGE,filtrerGridCatalogue);	
		 
		}	
		
		override protected function commitProperties():void{
			
				
			//selction des produits			
			btUp.addEventListener(MouseEvent.CLICK,monterLesProduitsSelectionnes);
			btAllUp.addEventListener(MouseEvent.CLICK,monterTousLesProduits);
			btDown.addEventListener(MouseEvent.CLICK,descendreLesProduisSelectionnes);		
			
			//filtres
			txtFiltreCat.addEventListener(Event.CHANGE,filtrerGridCatalogue);			
			txtFiltreCom.addEventListener(Event.CHANGE,filtrerGridProduit);
			
			//édition des libellés de lignes
			myGridProduits.addEventListener(DataGridEvent.ITEM_EDIT_END,updateLigne);
			
			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		//------------------------------------ FORMATS -----------------------------------------------
		
		//formatage de numero de téléphone pour une colonne de DataGrid
		protected function formatPhoneNumber(item : Object,column : DataGridColumn):String{		
		 
			if (isNaN(item[column.dataField])){
				return item[column.dataField];
			}else{
				return ConsoviewFormatter.formatSimplePhoneNumber(item[column.dataField]);		
			}	
			
		}	
		
		//------------------------------------ FILTRES ----------------------------------------------- 
		
		
		//Gere le filtre du tableau contenant les produits du catalogue opérateur
		protected function filtrerGridCatalogue(ev : Event):void{
			if (myGridCatalogue.dataProvider != null){
				(myGridCatalogue.dataProvider as ArrayCollection).filterFunction = processFiltrerGridCatalogue;
				(myGridCatalogue.dataProvider as ArrayCollection).refresh();	
			}
		}
		
		//Filtre les elements du tableau du catalogue opérateur sur les attributs THEME_LIBELLE,LIBELLE_PRODUIT
		//param in value un element du tableau 'Catalogue opéateur'
		protected function processFiltrerGridCatalogue(value : Object):Boolean{
			var cbThemeMatch : Boolean = (String(value.THEME_LIBELLE).toLowerCase().search(cmbTheme.selectedItem.THEME_LIBELLE.toLowerCase()) != -1);
			
			if (((String(value.THEME_LIBELLE).toLowerCase().search(txtFiltreCat.text.toLowerCase()) != -1)&& cbThemeMatch)
			||
			((String(value.LIBELLE_PRODUIT).toLowerCase().search(txtFiltreCat.text.toLowerCase()) != -1) && cbThemeMatch ))
			{
				return true;
			}else{
				return false;
			}			
		}
		
		//Gere le filtre du tableau contenant les produits sélectionnés
		protected function filtrerGridProduit(ev : Event):void{
			if (myGridProduits.dataProvider != null){
				(myGridProduits.dataProvider as ArrayCollection).filterFunction = processFiltrerGridProduit;
				(myGridProduits.dataProvider as ArrayCollection).refresh();	
			}
		}
		
		
		//Filtre les elements du tableau 'Produits sélectionnés' sur les attributs libelleLigne, libelletheme, libelleProduit
		//param in value un element du tableau 'Produits sélectionnés'
		protected function processFiltrerGridProduit(value : Object):Boolean{
 			if ((String(value.libelleLigne.toLowerCase()).search(txtFiltreCom.text.toLowerCase()) != -1)
				||
			    (String(value.libelletheme.toLowerCase()).search(txtFiltreCom.text.toLowerCase()) != -1)
			    || 
			    (String(value.libelleProduit.toLowerCase()).search(txtFiltreCom.text.toLowerCase()) != -1))
			{
				return (true);
			} else {
				return (false);
			}
		}
		
				
		
		//--------------------------------- DESCENDRE NOUVEAUX PRODUIT ------------------------------------------
		
		//Ajoute les produits sélectionnés du catalogue au tableau des éléments de la commande en y associant les lignes		
		protected function descendreLesProduisSelectionnes(me : MouseEvent):void{
			if (myGridProduits.dataProvider == null) myGridProduits.dataProvider = _panier.liste;				
			try{					
			
				if ((_tabLignes.length > 0)
					&&(myGridCatalogue.selectedIndices.length > 0)){							
					myGridCatalogue.selectedItems.forEach(descendreLeProduitPourChaqueLigneSelectionne);					
					myGridProduits.dataProvider.refresh();
				}
				
			}catch( re : RangeError ){
				trace("l'indice sort des limites")
			}catch( e : Error){
				trace(e.message,e.getStackTrace());
			}		
		}
		
		//Associ une ligne selectionnée du tableau des lignes ajoutées à chaque produits de la sélection
		// et l'ajoute au tableau des élément de la commande
		protected function descendreLeProduitPourChaqueLigneSelectionne(element : * , index : int , arr : Array):void{							
			var len : int = _tabLignes.length;
			for (var i : int = 0; i < len; i++){
				var ligne : ElementCommande = new ElementCommande();	
												
				ligne.libelletheme = element.THEME_LIBELLE;
				ligne.libelleProduit = element.LIBELLE_PRODUIT;				
				ligne.produitCatID = element.IDPRODUIT_CATALOGUE;				
				ligne.libelleLigne = _tabLignes[i].libelleLigne;
				ligne.ligneID = _tabLignes[i].ligneID;
				
				if (myGridProduits.dataProvider != null)			
					if (!ConsoviewUtil.is2InArray(ligne.produitCatID.toString(),
												  ligne.ligneID.toString(),
												  "produitCatID",
												  "ligneID",
												  (myGridProduits.dataProvider as ArrayCollection).source))
					{							
						_panier.liste.addItem(ligne);	  	
						_panier.liste.refresh();											
								
					}	
					
				myGridProduits.dataProvider = _panier.liste;								
			}	
			
		}
				
		//--- MONTER NOUVEAUX PRODUIT-------------------------------------------------------------
		
		//Efface les produits de la commande sélectionnés 
		protected function monterLesProduitsSelectionnes(me : MouseEvent):void{		
			if ( myGridProduits.dataProvider != null)
				if (myGridProduits.dataProvider.length > 0){				
					myGridProduits.selectedItems.forEach(monterLeProduit);
					myGridProduits.dataProvider.refresh();					
				}			
		}
		
		//Effaces tous les produits de la commande 
		protected function monterTousLesProduits(me:MouseEvent):void{
			if (myGridProduits.dataProvider != null){
				var len : int = (myGridProduits.dataProvider as ArrayCollection).length;				
				for(var i : int = 0; i < len; i++){						
					(myGridProduits.dataProvider as ArrayCollection).removeItemAt(0);
				}
				myGridProduits.dataProvider.refresh();
			}
		}
		
		//Efface un produit de la commande
		protected function monterLeProduit(element : * , index : int , arr : Array):void{			
		 	(myGridProduits.dataProvider as ArrayCollection).removeItemAt((myGridProduits.dataProvider as ArrayCollection).getItemIndex(element));
		}
		
		//------- UPDATE LIBELLE DES lIGNES-------------------------------//
		
		//Met à jour le libelle d'une ligne
		protected function updateLigne(de : DataGridEvent):void{			
			if (myGridProduits.dataProvider != null){														
				if (de.dataField == "libelleLigne"){
						var myEditor:TextInput = TextInput(de.currentTarget.itemEditorInstance);                
	                // Get the new value from the editor.
	    	        var newVal:String = myEditor.text;	                
	                // Get the old value.
	                var oldVal:String = de.currentTarget.editedItemRenderer.data[de.dataField];                          					
					
					ElementCommande(myGridProduits.dataProvider[de.rowIndex]).libelleLigne = newVal;																						 													
					tmpElement = null;
					tmpElement = ElementCommande(myGridProduits.dataProvider[de.rowIndex]);
					
					_panier.liste.source.forEach(updatePanier);
					_panier.liste.refresh();
					
					var evtObj : SelectionProduitEvent = new SelectionProduitEvent(SelectionDesProduitsV2Impl.LIBELLE_LIGNE_CHANGED);
					evtObj.ligneSelectionnee = tmpElement;
					dispatchEvent(evtObj);
					
				}
			}else{		
				
				de.preventDefault();					
			}   
		}
		
		
		//Met à jour le libelle d'une ligne
		protected function updatePanier(element : * , index : int , arr : Array):void{			
		
		 	if (ElementCommande(element).ligneID == tmpElement.ligneID){
		 		ElementCommande(element).libelleLigne = tmpElement.libelleLigne;
		 	}
		 		
		}


		

		//charge la liste des themes suivant l'opérateur sélectionné
		//si on change d'opérateur et qu'il y a des éléments dans la commande, une dialog box apparait nous demandant de confirmer l'action.
		//En effet, la commande etant mono opérateur, le chagement de ce dernier efface les produits de cette commande
		
		protected function getTheme():void{			
			
			if(operateurSelectionne != null){
				try{
					_commande.operateurID = operateurSelectionne.id;
				}catch(e : Error){
					trace("[WARNING] pas de commande affecter");
				}				
				newOperateur = operateurSelectionne.id;
				if (operateurSelectionne.id == oldOperateur){
				
				}else{	
					if (myGridProduits.dataProvider && (myGridProduits.dataProvider as ArrayCollection).length > 0  ){
						Alert.okLabel = "Fermer";
						Alert.buttonWidth = 100;
						ConsoviewAlert.afficherAlertInfo("Attention, il ne peut pas y avoir plusieur operateur pour un même commande.\n","Attention!",alertHandler);
					}else{
						getThemeByOperateur(operateurSelectionne.id);
					}	
				}	
			}else{			
				//getOperateurs();
				getThemeByOperateur(0);
			}
			oldOperateur = newOperateur; 
		}
		
		
		//Alerte demandant la confirmation pour le changement d'opérateur
		protected function alertHandler(ce : CloseEvent):void{
			if (ce.detail == Alert.OK){
				clearSelection();			
				getThemeByOperateur(operateurSelectionne.id);	
			}			
		}
		
		
		
		//--------------------------- REMOTINGS----------------------------------------------------
		
		
		//Formate les données d'un ArrayCollection contenant des themes, efface les doublons
		//param in source l'arrayCollection source
		//param out newCollection 
		protected function fillComboTheme( source : ArrayCollection):ArrayCollection{
			var newCollection : ArrayCollection = new ArrayCollection();
			var len : int = source.length;			
			for (var i:int = 0;i < len; i++){						
				if (!ConsoviewUtil.isIdInArray(int(source[i].IDTHEME_PRODUIT),"IDTHEME_PRODUIT",newCollection.source)){
					var obj : Object = new Object();				
					obj.THEME_LIBELLE = source[i].THEME_LIBELLE;
					obj.IDTHEME_PRODUIT = source[i].IDTHEME_PRODUIT;	
					newCollection.addItem(obj);
				}
			}			
			return newCollection;
		}
		
		
		
		//Handler de la fonction getThemeByOperateur 
		//Rempli la combo avec la liste des themes d'un opérateur
		//dispatche un evenement signifiant que l'on vient de changer de theme
		protected function getListeThemeResultHandler(re :ResultEvent):void{
			cmbTheme.dataProvider = fillComboTheme(re.result as ArrayCollection);
			cmbTheme.labelField = "THEME_LIBELLE"; 				
			myGridCatalogue.dataProvider = re.result as ArrayCollection;	
			txtFiltreCat.enabled = Boolean((myGridCatalogue.dataProvider as ArrayCollection).length > 0);			
			cmbTheme.dispatchEvent(new Event(Event.CHANGE));
		}
		
		
		//Charge la liste des themes pour un opérateur
		//param in idop lidentifiant de l'opérateur pour lequel on veut les themes		
		protected function getThemeByOperateur(idope : int):void{
			 
			opTheme  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande",
																				"getProduitsByOperateur",
																				getListeThemeResultHandler,
																				null);				
			RemoteObjectUtil.callService(opTheme,idope);		
		}

	}
}