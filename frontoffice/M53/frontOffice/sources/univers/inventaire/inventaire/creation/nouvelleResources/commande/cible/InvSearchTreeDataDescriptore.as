package univers.inventaire.inventaire.creation.nouvelleResources.commande.cible
{
	import mx.collections.ICollectionView;
	import mx.controls.treeClasses.ITreeDataDescriptor;
	import mx.collections.XMLListCollection;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.ResultEvent;
	import mx.controls.Tree;


	public class InvSearchTreeDataDescriptore implements ITreeDataDescriptor
	{	
		private var _refOwner : Tree
		public function InvSearchTreeDataDescriptore(refOwner : Tree){
			_refOwner = refOwner;			
		}
		
		public function getData(node:Object, model:Object=null):Object
		{
			//TODO: implement function
			return null;
		}
		
		public function hasChildren(node:Object, model:Object=null):Boolean
		{
			var hasChild:int; 
			if (typeof(node) == "xml") hasChild = parseInt(node.@NTY,10);
			else hasChild = int(node.NTY);
			
			return (hasChild > 0);
		}
		
		public function addChildAt(parent:Object, newChild:Object, index:int, model:Object=null):Boolean
		{
			return false;
		}
		
		public function isBranch(node:Object, model:Object=null):Boolean
		{
			
			var hasChild:int;
			if (typeof(node) == "xml") hasChild = parseInt(node.@NTY,10);
			else hasChild = int(node.NTY);
			
			return (hasChild > 0);
		}
		
		public function removeChildAt(parent:Object, child:Object, index:int, model:Object=null):Boolean
		{	
			return false;
		}
		
	
		public function getChildren(node:Object, model:Object=null):ICollectionView
		{	
	 		var childrenList:XMLList = (node as XML).children();
			if (childrenList.length() == 0){				
				getNodeChild(node);	
			}
			return new XMLListCollection(childrenList);
		}
	
		private var _node : Object = null;	
		private function getNodeChild(node : Object):void{		
			
			if ((parseInt(node.@NID,10) > 0) && (node as XML).children().length() == 0){
				_node = node;
				var getChildOp:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													"fr.consotel.consoview.access.AccessManager","getNodeChild",
													addNodeChild);
				RemoteObjectUtil.callService(getChildOp,parseInt(node.@NID,10));
			}
		}
		
		private function addNodeChild(event:ResultEvent):void {
			if (_node != null){
				if ((_node as XML).children().length() == 0){
					_refOwner.expandItem(_node,false);
					(_node as XML).appendChild((event.result as XML).children());					
					_refOwner.expandItem(_node,true);
				}
					
			}
		}	
	}
}