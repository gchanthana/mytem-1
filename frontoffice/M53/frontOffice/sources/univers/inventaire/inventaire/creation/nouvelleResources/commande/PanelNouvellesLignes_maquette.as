package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
		
	import composants.tb.effect.EffectProvider;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.ajoutnouvelleslignes.AjoutDeLignes;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts.ContactChangeEvent;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts.GestionContacts;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.CommandeExportable;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.selectionDesProduits.PanelDemandeEvent;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.selectionDesProduits.SelectionDesProduitsV2Impl;
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;
	import univers.inventaire.inventaire.export.ExportBuilder;	
	
	
	/**
	 * Classe gérant le formulaire de commande de 'Nouvelles Lgnes'
	 * */
	public class PanelNouvellesLignes_maquette extends PanelNouvellesLignesIHM_maquette implements IDemande
	{
		
		//identifiant de la commande que l'on souhaite afficher	
	 	private var _idDemande: int;
		
		//Reference vers le panier de la commande
		private var panier : ElementCommande = new ElementCommande();
		
		//Reference vers la commande
		private var commande : Commande = new Commande();
		
			
		
		private var _modeEcriture : Boolean;
		[Bindable]
		public function get modeEcriture():Boolean{
			return _modeEcriture 
		}
		
		public function set modeEcriture(mode : Boolean):void{
			_modeEcriture = mode;
		}
		
		
		
		/**
		 * Constructeur
		 * */
		public function PanelNouvellesLignes_maquette(){
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);						
		}
		
		
		/**
		 * Appel la methode clean 
		 * Ne fait rien pour le moment
		 * */		
		public function clean():void
		{
			
		 	pnlSelection.clean();
		 	pnlGestionContact.clean();		 	
			if (commande != null) commande.clean();
			if (panier != null) panier.clean();			
		}
		
		/**
		 * Retourne l'identifiant de la commande
		 * @return idDemande l'identifiant de la commande
		 * */
		public function get idDemande():int
		{
			return _idDemande;
		}
		
		/**
		 * Setter pour l'identifiant de la commande que l'on souhaite afficher
		 * @param idd l'identifiant de la commande
		 * */
		public function set idDemande(idd:int):void
		{
			_idDemande = idd;
		}
		
		
		/**
		 * Setter pour la commande que l'on souhaite afficher
		 * @param c la commande que l'on souhaite afficher
		 * */
		public function setCommande(c : Commande):void{
			commande = c;
		} 
		
		//Affiche la liste des operateurs pour la societé sélectionnée 
		//Si aucune société n'a ete selectionnée, affiche tous les opérateurs		
		private function remplirComboOperateur(che : ContactChangeEvent):void{
			var societe : Societe = che.societe;
			getOperateurs(societe.societeID);	
		}
		
		
		//Initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{
			
			pnlGestionContact.addEventListener(GestionContacts.SOCIETE_CHANGED,remplirComboOperateur);		
			pnlAjoutLigne.addEventListener(AjoutDeLignes.SELECTION_CHANGED,setSelectedLignes);						
			pnlSelection.addEventListener(SelectionDesProduitsV2Impl.LIBELLE_LIGNE_CHANGED,changerLibelle);
			
			//bouttons 
			btPDF.addEventListener(MouseEvent.CLICK,afficherPDF);
			btEnregistrer.addEventListener(MouseEvent.CLICK,verifierDemmande);
			btAnnuler.addEventListener(MouseEvent.CLICK,fermerLaFenetre);			
			
			pnlGestionContact.setCommande(commande,panier);
			pnlAjoutLigne.setPanier(panier);			
			pnlSelection.setCommande(commande,panier);					
			
			commande.idGroupeClient = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			commande.typeCommande =  Commande.NOUVELLES_LIGNES;
			commande.addEventListener(Commande.SAVE_COMPLETE,enregistrerCommandeHandler);
			commande.addEventListener(Commande.SAVE_ERROR,enregistrerCommandeErrorHandler);
			commande.addEventListener(Commande.CREATION_COMPLETE,afficherCommande);
			
			panier.addEventListener(ElementCommande.SAVEALL_COMPLETE,enregistrerPanierHandler);
			panier.addEventListener(ElementCommande.SAVE_ERROR,enregistrerPanierErrorHandler);
			
			getOperateurs(0);
		}
		
		//Prend les lignes selectionnées et les ajoute au tableaux du composant 'Selection de produits'		
		private function setSelectedLignes(ev: Event):void{			
			pnlSelection.tabLignes = pnlAjoutLigne.getSelectedItems();
		}
		
		//Met à jour les libellés des lignes du tableau de ligne à ajouter
		//lorsqu'on les modifie dans le tableau des produits de la commande
		private function changerLibelle(spe : SelectionProduitEvent):void{
			pnlAjoutLigne.updateLigneLibelle(spe.ligneSelectionnee);
		}
		
		
		
		//Ferme la fenetre sans enregistrer
		//dispatche un évènement 'PanelDemandeEvent' de type 'SORTIR' signifiant que l'on veut sortir de la commande sans l'enregistrer
		private function fermerLaFenetre(me : MouseEvent):void{
			ConsoviewAlert.afficherAlertConfirmation("Etes vous sur de vouloir Annuler l'Opération","Confirmation",executeFermerLaFenetre);
		}
		
		private function executeFermerLaFenetre(eventObj:CloseEvent):void{	
			if (eventObj.detail == Alert.OK){
				dispatchEvent(new PanelDemandeEvent(PanelDemandeEvent.SORTIR));
			}
		}		
			
		//Verifie la validité de la commande
		//les champs libellé, Date Commande, Date de livraison prévue sont obligatoires
		//Il faut avoir au moins un produit dans la commande.
		//Si la commande est valide, on l'enregistre
		private function verifierDemmande(me : MouseEvent):void{
			pnlGestionContact.affecterContactCommande(commande);
			pnlInformations.affecterCommande(commande);
			pnlCible.affecterCibleCommande(commande);
			
			if(pnlCibleFact.initialized && pnlCibleFact.dgCompteFacturation.selectedItem != null) commande.compteID = pnlCibleFact.dgCompteFacturation.selectedItem.IDCOMPTE_FACTURATION;			
			if(pnlCibleFact.initialized && pnlCibleFact.dgSousCompte.selectedItem != null) commande.sousCompteID = pnlCibleFact.dgSousCompte.selectedItem.IDSOUS_COMPTE;
			commande.idUserCreate = CvAccessManager.getSession().USER.CLIENTACCESSID;
			
			
			var boolPanier : Boolean = false;
			var boolLibelle : Boolean = (commande.libelle.length > 0);
			var boolDateCommande : Boolean = (commande.dateEffective.length > 0);
			var boolDateLivraisonPrevue : Boolean = (commande.dateLivraisonPrevue.length > 0);
			var boolCompteFacturation : Boolean = (commande.compteID > 0);
			var message : String = " ";
					
			if (panier.liste != null){
				if(panier.liste.length > 0) boolPanier = true;
				else boolPanier = false;
			}else{
				boolPanier = false;
			}
			
			if(!boolCompteFacturation){
				message = message +"\n - le Compte de facturation est obligatoire";
				EffectProvider.FadeThat(pnlCibleFact);
			}
			
			if (!boolLibelle){
				message = message +"\n - le Libellé est obligatoire";
				EffectProvider.FadeThat(pnlInformations.txtLibelle);
			}
			
			if (!boolDateCommande){
				message = message +"\n - la date de la commande est obligatoire";
				EffectProvider.FadeThat(pnlInformations.dfDateEffet);
			}	
			
			if (!boolDateLivraisonPrevue){
				message = message +"\n - le date de livraison prevue est obligatoire";
				EffectProvider.FadeThat(pnlInformations.dfdateLivraisonPrevue);
			}
			
			if (!boolPanier){
				message = message +"\n - il n'y a aucun produit dans la commande";
				EffectProvider.FadeThat(pnlInformations);				
			}
			
			
			if (boolCompteFacturation && boolDateCommande && boolDateLivraisonPrevue && boolLibelle && boolPanier) enregistrerDemande();
			else{
				Alert.show(message,"Erreur");
			}
			
		}
		
		//enregistre la Commande
		private function enregistrerDemande():void{			
			//commande.sauvegarder(CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
			commande.idGroupeClient = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			commande.sauverCommandeCreerLignesRessourcesEtWorkflow(panier);			
		}
		
		
		//Enregistre le Pannier de la Commande
		private function enregistrerCommandeHandler(ev : Event):void{
			trace("enregistrerCommandeHandler");	
			//panier.commandeID = commande.commandeID;			
			//panier.sauvegarderAll();				
			//commande.refresh();
 			var  evObj : PanelDemandeEvent = new PanelDemandeEvent(PanelDemandeEvent.FERMER);
			evObj.commande = commande;	
			dispatchEvent(evObj);	
		}
		
		//Affiche une alerte lorsque l'enregistrement de la commande n'a pas abouti
		private function enregistrerCommandeErrorHandler(ev : Event):void{
			Alert.show("Erreur de sauvegarde");
		}
		
		
		//Met à jour les données de la commande
		private function enregistrerPanierHandler(ev : Event):void{
			trace("sauvegarde panelNouvelleLignes");
			commande.refresh();	
			
		}
		
		//Affiche une Alert lorsque l'enregistrement du panier n'a pas abouti
		private function enregistrerPanierErrorHandler(ev : Event):void{
			Alert.show("Erreur de sauvegarde");	
		}	
		
		//Dispatche un évenement 'PanelDemandeEvent' de type 'SAVE_COMPLETE' signifiant que l'enregistrement de la commande s'est bien passé
		private function afficherCommande(ev : Event):void{	
			var  evObj : PanelDemandeEvent = new PanelDemandeEvent(PanelDemandeEvent.SAVE_COMPLETE);
			evObj.commande = commande;	
			dispatchEvent(evObj);			
		}
		
			
		
		//--------------PDF
		
		//Verifie la validité de la commande
		//les champs libellé, Date Commande, Date de livraison prévue sont obligatoires
		//Il faut avoir au moins un produit dans la commande.
		//Si la commande est valide, affiche le PDF
		private function afficherPDF(me : MouseEvent):void{
			pnlGestionContact.affecterContactCommande(commande);
			pnlInformations.affecterCommande(commande);
			pnlCible.affecterCibleCommande(commande);
			
			if(pnlCibleFact.initialized && pnlCibleFact.dgCompteFacturation.selectedItem != null) commande.compteID = pnlCibleFact.dgCompteFacturation.selectedItem.IDCOMPTE_FACTURATION;			
			if(pnlCibleFact.initialized && pnlCibleFact.dgSousCompte.selectedItem != null) commande.sousCompteID = pnlCibleFact.dgSousCompte.selectedItem.IDSOUS_COMPTE;
			commande.idUserCreate = CvAccessManager.getSession().USER.CLIENTACCESSID;
			
			var boolPanier : Boolean = false;
			var boolLibelle : Boolean = (commande.libelle.length > 0);
			var boolDateCommande : Boolean = (commande.dateEffective.length > 0);
			var boolDateLivraisonPrevue : Boolean = (commande.dateLivraisonPrevue.length > 0);
			var boolCompteFacturation : Boolean = (commande.compteID > 0);
			
			var message : String = " ";
					
			if (panier.liste != null){
				if(panier.liste.length > 0) boolPanier = true;
				else boolPanier = false;
			}else{
				boolPanier = false;
			}
			
			if (!boolLibelle){
				message = message +"\n - le Libellé est obligatoire";
				EffectProvider.FadeThat(pnlInformations.txtLibelle);
			}
			
			if (!boolDateCommande){
				message = message +"\n - la date de la commande est obligatoire";
				EffectProvider.FadeThat(pnlInformations.dfDateEffet);
			}	
			
			if (!boolDateLivraisonPrevue){
				message = message +"\n - le date de livraison prevue est obligatoire";
				EffectProvider.FadeThat(pnlInformations.dfdateLivraisonPrevue);
			}
			
			if(!boolCompteFacturation){
				message = message +"\n - le Compte de facturation est obligatoire";
				EffectProvider.FadeThat(pnlCibleFact);
			}
			
			
			if (!boolPanier){
				message = message +"\n - il n'y a aucun produit dans la commande";
				EffectProvider.FadeThat(pnlInformations);				
			}
			
			
			if (boolCompteFacturation && boolDateCommande && boolDateLivraisonPrevue && boolLibelle && boolPanier) exporterLePdf();
			else{
				Alert.show(message,"Erreur");
			}
			
		}
		
		 
		
		//Affiche la commande en PDF
		private function exporterLePdf():void{			
			displayExport(Commande.FORMAT_PDF);
		}
		
		
		
		//Affiche la commande dans une nouvelle fenetre au format spécifié
		//param in format , le format sous lequel on souhaite afficher la commande
		private function displayExport(format : String):void {		
			pnlGestionContact.affecterContactCommande(commande);
			pnlInformations.affecterCommande(commande);
			pnlCible.affecterCibleCommande(commande);
				 
			var commandeToExp : CommandeExportable = new CommandeExportable(commande,panier);				 
			var expB : ExportBuilder = new ExportBuilder(commandeToExp);
			expB.exporter(format);
			
			/* switch (format.toUpperCase()){
				case "PDF" : {
					var pdf : PDFCreator = new PDFCreator(commande,panier);
					pdf.displayExport();					
					break
				}
			} */
					 
		} 
		
		
		//------------- choix opérateur ---------------//
		//Rempli la combo avec la liste des opérateur provenant d'un ArrayCollection
		//param in source une ArrayCollection contenant la liste des opérateurs
		protected function fillComboOperateur( source : ArrayCollection):ArrayCollection{
			var newCollection : ArrayCollection = new ArrayCollection();
			var len : int = source.length;			
			for (var i:int = 0;i < len; i++){						
				
				var operateur : OperateurVO = new OperateurVO();				
				operateur.nom = source[i].NOM;
				operateur.id = source[i].OPERATEURID;	
				newCollection.addItem(operateur);
				
			}			
			return newCollection;
		}
		
		
		
		//Charge la liste des opérateurs pour un distributeur ou une agence
		//charge tous les opérateur si l'identifiant est negatif
		//param in idSociete l'identifiant du disributeur ou de l'agence pour qui on charge les opérateurs
		public function getOperateurs(idSociete : int = -1 ):void{
			
			var methode : String = "getOperateurList";
			if (idSociete > 0){
				methode = "getOperateurlistSociete";
			}
			 
			var opOperateur : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Societe",
																				methode,
																				getOperateurResultHandler,
																				null);				
			RemoteObjectUtil.callService(opOperateur,idSociete);	
		}
		
		
		
		//Handler de la fonction getOperateur 
		//Rempli la combo avec la liste des opérateurs
		protected function getOperateurResultHandler(re : ResultEvent):void{
			cmbOperateur.dataProvider = fillComboOperateur(re.result as ArrayCollection);
			cmbOperateur.labelField = "nom";
		}
		
	}
}