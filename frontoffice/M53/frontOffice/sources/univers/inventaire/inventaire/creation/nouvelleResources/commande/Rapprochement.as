package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import mx.collections.ArrayCollection;
	import mx.controls.dataGridClasses.DataGridColumn;
	
	public class Rapprochement extends RapprochementIHM
	{
		private const COLDFUSION_COMPONENT : String = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.GestionRapprochement";
		private const COLDFUSION_GETNVLLELIGNE : String = "getListNvlleLignesFacturees";
		private const COLDFUSION_RAPPROCHER : String = "rapprocher";
		
		public static const RAPPROCHER : String = "ligneRapprochee";
		public static const SORTIR : String = "sortirDuRapprochement";
		
		private var opNvlleLigne : AbstractOperation;
		private var opRapprochement : AbstractOperation;
		
		private var _commande : Commande;
		private var _element : ElementCommande;
		private var dateRef : String;//date de ref pour la liste des lignes facturées
		
		public function clean():void{
			if (opNvlleLigne != null) opNvlleLigne.cancel();
			if (opRapprochement != null) opRapprochement.cancel();
		}
		
		public function Rapprochement(commande : Commande, elementCommande : ElementCommande){
			//TODO: implement function
			super();
			_commande = commande;			
			_element = elementCommande;
			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);			
		}
		
		private function initIHM(fe : FlexEvent):void{
			addEventListener(CloseEvent.CLOSE,fermerPanel);
			btSortir.addEventListener(MouseEvent.CLICK,fermerPanel);
			btRapprocher.addEventListener(MouseEvent.CLICK,rapprocher);
			lblGroupe.label = "Etendre la liste";
			
			
			txtIdentifiant.text = _element.libelleLigne;
			txtOperateur.text = _commande.operateurNom;
			txtCible.text = "";
			
			txtFiltreLigne.addEventListener(Event.CHANGE,filtrerLeGrid);
			
			var idGroupe : int;
			if (_commande.idGroupeCible > 0){
				idGroupe = _commande.idGroupeCible;
				btEtendre.visible = true;
				btEtendre.addEventListener(MouseEvent.CLICK,etendreLaRecherche);
				
			}else{
				idGroupe = _commande.idGroupeClient;
				btEtendre.visible = false;
			}			
			getNouvelleLignesFacturation(idGroupe);			
		}
		
		private function fermerPanel(ev : Event):void{
			clean();
			dispatchEvent(new Event(Rapprochement.SORTIR));
			PopUpManager.removePopUp(this);	
			
		}		
		
		//etendre la recherche au Groupe
		private function etendreLaRecherche(me : MouseEvent):void{
			var idGroupe : int = _commande.idGroupeClient;
			getNouvelleLignesFacturation(idGroupe);
		}
		
		
		private function rapprocher(me :MouseEvent):void{
			if (myGridLigne.selectedIndex != -1){
				rapprocherLaLigne(_element.elementID,myGridLigne.selectedItem.IDSOUS_TETE);		
			}
		}
		
	//---------- FILTRE -------------------------------
		private function filtrerLeGrid(ev : Event):void{
			if (myGridLigne.dataProvider != null){
				if((myGridLigne.dataProvider as ArrayCollection).length > 0){
					(myGridLigne.dataProvider as ArrayCollection).filterFunction = filtrerLeGridFunc;	
					(myGridLigne.dataProvider as ArrayCollection).refresh();
				}
			}  
		}
		
		private function filtrerLeGridFunc(value : Object):Boolean{
			if ((String(value.SOUS_COMPTE).toLowerCase().search(txtFiltreLigne.text) != -1)
				||
				(String(value.SOUS_TETE).toLowerCase().search(txtFiltreLigne.text) != -1)
				||
				(String(value.DATE_EMISSION_FACTURE).toLowerCase().search(txtFiltreLigne.text) != -1))
			{
				return true;		
			}else{
				return false;
			}			
		}
		
	//---------- REMOTING -----------------------------	
	
		//recupere les nouvelles lignes de la facturatioin 
		private function getNouvelleLignesFacturation(idGroupe : int):void{
			opNvlleLigne = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				COLDFUSION_COMPONENT,
																				COLDFUSION_GETNVLLELIGNE,
																				getNouvelleLigneResultHandler,
																				null);				
			RemoteObjectUtil.callService(opNvlleLigne,idGroupe,_commande.dateCreation);		
		}
		
		private function getNouvelleLigneResultHandler(re : ResultEvent):void{
			myGridLigne.dataProvider = re.result as ArrayCollection;
			txtFiltreLigne.enabled = true;	

		}
		
		
		//rapprocher la ligne
		private function rapprocherLaLigne(idDetaile : int, idSousTete : int):void{
			opNvlleLigne = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				COLDFUSION_COMPONENT,
																				COLDFUSION_RAPPROCHER,
																				rapprocherResultHandler,
																				null);				
			RemoteObjectUtil.callService(opNvlleLigne,idDetaile,idSousTete);		
		}
		
		private function rapprocherResultHandler(re : ResultEvent):void{
			dispatchEvent(new Event(Rapprochement.RAPPROCHER));			
		}
		
	}
}