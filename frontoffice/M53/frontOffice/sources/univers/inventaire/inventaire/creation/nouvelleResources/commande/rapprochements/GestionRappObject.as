package univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements
{
	public class GestionRappObject
	{
		public static const BYLIBELLE : int = 1;
		public static const BYCIBLE : int = 2;		
		public static const BYOP: int = 3;
		
		public static const RemoteName : String = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.GestionRapprochement"
		
		public static const getNouveauxProduitsByCible : String = "getNouveauxProduitsByCible";
		
		public static const getNouveauxProduitsByOP : String = "getNouveauxProduitsByOP";
		
		public static const getNouveauxProduitsByLibelle : String = "getNouveauxProduitsByLibelle";
		
		public static const getCibleLibelle : String = "getCibleLibelle";
		
	}
}