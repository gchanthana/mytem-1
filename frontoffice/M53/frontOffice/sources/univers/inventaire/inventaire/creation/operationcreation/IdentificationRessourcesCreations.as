package univers.inventaire.inventaire.creation.operationcreation
{     
	import composants.access.ListePerimetresWindow;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.OpeSearchTree;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	
	i
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Tree;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ItemClickEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;
	import univers.parametres.perimetres.rightSide.assignmentComponent.itemRender;
	import mx.controls.TextInput;
	
	/**
	 * Classe non utilisée
	 * */
	public class IdentificationRessourcesCreations extends IdentificationRessourcesCreationsIHM
	{   
		private var ctNvLigne : int = -1;
		//params pour le remoting	
		private var op : AbstractOperation;			
		protected var  perimetre : String; // le périmetre de la recherche	
		
		private var filtre : int = 0; //filtre nvlle ligne / ligne existante pour la requete listeproduitsclient		
		//liste des produits client selectionnés
		private var tabElementSelectionnee : ArrayCollection;
		//le noeuds sur lequel on click 
		private var nodeId : int; //l'identifiant du noeud 
		private var nodeLibelle : String; //le libellé du noeud
		private var nodePerimetre : String;	//le type de perimetre pour le noeud
		private var myTree : OpeSearchTree;
		
		//la ligne sur laquelle on click (dans le datagrid)
		private var sousTete : String; //le numero
		private var sousTeteId : int; // l'identifiant
		
		private var listeOperateur : Array; 
		
		//Selection des lignes --------------------------------------------------------------------------------------------
		[Bindable]
		private var listeSousTete : ArrayCollection;
		
		[Bindable]
		private var selectedSousTete : Array;
		
		
		
		//Selection des produits --------------------------------------------------------------------------------------------
		[Bindable]
		private var tmpSelectedProduitsClient : Array ;
			
		
		[Bindable]
		private var listeProduitsClient : ArrayCollection = new ArrayCollection();
		
		[Bindable]
		private var listeProduitsClientSelectionnes : ArrayCollection = new ArrayCollection();
		
		/**
		 * Supprime les fenêtres surgissantes
		 * (Fait le ménage)
		 * */ 
		public function clean():void{
			
		} 
		 
		/**
		 * Constructeur
		 **/ 
		public function IdentificationRessourcesCreations()
		{
			//TODO: implement function
			super();
			perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;				
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);						
			myTree = new OpeSearchTree();						
			myTree.addEventListener(FlexEvent.CREATION_COMPLETE,initTree);			
		}
		
		
		//initialisation de l'arbre		
		private function initTree(fe : FlexEvent):void{
			
		}
		//initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{
			//Ressources ------------------------------------				
		 						
			compte.addChildAt(myTree,1);			
			myTree.myTree.searchTree.liveScrolling = false;			
			myTree.myTree.addEventListener(Event.CHANGE,chargerListeSousTete);		
 			myTree.height = 162;
 			
 			 
			txtFiltreLigne.addEventListener(Event.CHANGE,filtrerLeDataGrid)	
			txtFiltreLigne.addEventListener(FocusEvent.FOCUS_IN,selectAllText);		
			
			 
			myGridLigne.addEventListener(Event.CHANGE,gridLigneChangeHandler);				
			myGridLigne.doubleClickEnabled = true;
			myGridLigne.addEventListener(MouseEvent.DOUBLE_CLICK,gridLineDClickHandler);
			DataGridColumn(myGridLigne.columns[0]).labelFunction = formatDataTip;		
				
			
			DataGridColumn(myGridListeProduitsClientSelectionnes.columns[0]).labelFunction = formatDataTip;
		
			//Produits Client ------------------------------------------
			cbxOperateur.addEventListener(Event.CHANGE,cbxChangeHandler);
			
			txtFiltre.addEventListener(Event.CHANGE,filtreChangeHandler);
			
			txtFiltre.addEventListener(FocusEvent.FOCUS_IN,selectAllText);
						 
			//Action 
			btEnregistrer.enabled = true;
			btEnregistrer.addEventListener(MouseEvent.CLICK,enregistrerHandler);		 			
			btAnnuler.addEventListener(MouseEvent.CLICK,annulerHandler);				
			
			myGridListeProduitsClient.dataProvider = listeProduitsClient;
			//myGridListeProduitsClient.addEventListener(Event.CHANGE,selectionnerProduitClient);
			//myGridListeProduitsClientSelectionnes.doubleClickEnabled = true;
			//myGridListeProduitsClientSelectionnes.addEventListener(MouseEvent.DOUBLE_CLICK,deselectProduitClient)
			myGridListeProduitsClientSelectionnes.allowMultipleSelection = true;
			myGridListeProduitsClientSelectionnes.dataProvider = listeProduitsClientSelectionnes;

			//bouttons
			btUp.addEventListener(MouseEvent.CLICK,monterLesProduitsSelectionnes);
			btAllUp.addEventListener(MouseEvent.CLICK,monterTousLesProduitsSelectionnes);
			btDown.addEventListener(MouseEvent.CLICK,descendreLesProduisSelectionnes);
			btAllDown.addEventListener(MouseEvent.CLICK,descendreTousLesProduitsSelectionnes);
			
			getListeOperateur(); 			 
		}
		
		
	//navigation ---------------------------------------------------------------------------------------------------------
		//suivant
		
		
		
//arbre ---------------------------------------------------------------------------------------
		//gere les dclick sur l'arbre
		private function chargerListeSousTete(e : Event):void{
			 
			var selectedNode:Tree = (e.currentTarget as Tree);
			var nodeType:int = parseInt(selectedNode.selectedItem.@BOOL_ORGA,10);	
			var nodeTypeOrga : String = selectedNode.selectedItem.@TYPE_ORGA;	
						
			/* if ((nodeType != BOOL_ORGA) && (nodeTypeOrga != "" )) {				
				setLocalNodeParams(selectedNode.selectedItem);										
				txtFiltre.enabled = false;
				getLinesFromNode();
				
				listeProduitsClient.source = null;
				listeProduitsClient.refresh();
				
				txtFiltre.text = "";
				txtFiltreLigne.text = "";
				
				listeProduitsClientSelectionnes.source = null;
				listeProduitsClientSelectionnes.refresh();
				
			}else{					
				myGridLigne.dataProvider = null; 
				unSetLocalNodeParams();
			}			 */
			 
			 
		}
		
		
		
		//change l'op de l'arbre	
		public function cbxChangeHandler(evt :Event):void{
			remplirComboOP(cbxOperateur.selectedItem.OPERATEURID);
			
			listeProduitsClient.source = null;
			listeProduitsClient.refresh();
			
			listeProduitsClientSelectionnes.source = null;
			listeProduitsClientSelectionnes.refresh();
			
			try{
				listeSousTete.source = null;
				listeSousTete.refresh();	
			}catch(e : Error){
				trace("pas de sous tete");
			}
			
			
			txtFiltre.text = "";
			txtFiltreLigne.text = "";
		}
		
		//met à jour les params pour mis a jour du tb
		private function setLocalNodeParams(node : Object):void{			
			nodePerimetre = node.@TYPE_PERIMETRE;
			nodeId = node.@NODE_ID;
			nodeLibelle = node.@LABEL;
						
		}
		//reset les parametre locale		
		private function unSetLocalNodeParams():void{
			
			nodeId = -1;
			nodeLibelle = "";
			nodePerimetre = "";		
		}
//fin arbre---------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------
//Grid des lignes -------------------------------------------------		
		private function gridLineDClickHandler(me : MouseEvent):void{
			gridLigneChangeHandler(null);			
		}
		//gere le click sur une ligne du grid
		private function gridLigneChangeHandler(ev : Event):void{
			selectedSousTete = new Array();			
			selectedSousTete = myGridLigne.selectedItems;
								
			if (selectedSousTete.length > 0){				
			
				chargerListeProduitsHandler(null);
			}
		}
		//formate les donnees du selectedItems du grid
		private function formateData( data : Array , colname : String):Array{
			var s : String;
			var a : Array = new Array();
			for (var i : uint = 0 ; i < data.length ; i++ ){
				a.push(data[i][colname]);
			}
			return a;
		}	
			
		//filtre le grid
		private function filtrerLeDataGrid(e : Event):void{
			listeSousTete.filterFunction = processfitrerLeDataGrid;
			listeSousTete.refresh();
		}
	
		//filtre pour le grid des sous-tete
		private function processfitrerLeDataGrid(value : Object):Boolean{
			if (String(value.SOUS_TETE.toLowerCase()).search(txtFiltreLigne.text.toLowerCase()) != -1) {
				return (true);
			} else {
				return (false);
			}
	
		}		
		
		//selectionne le texte au focus in
		private function selectAllText(fe : FocusEvent):void{
			var len : int = TextInput(fe.currentTarget).text.length;
			if (len != 0){									
				TextInput(fe.currentTarget).setSelection(0,len);					
			}
		}
		
		//-----------------------------------------------------------------------------------------------		
		//formatage du telepone dans le grid
		private function formatDataTip(item : Object, column : DataGridColumn):String{
			
			if ((String(item.SOUS_TETE).length == 10)&& parseInt(item.SOUS_TETE).toString().length == 9)
					return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);
				else
					return item.SOUS_TETE;
			
		}
		
		
		//---------filtres------------------------------------------------------------
		
		//gere le fitre
		private function filtreChangeHandler(ev : Event):void{
			try{
				listeProduitsClient.filterFunction = filterFunc;
				listeProduitsClient.refresh();
			}catch(e : Error){
				trace("Erreur filtre liste Elements de facturation");
			}
			
		}
		
		//filtre
		private function filterFunc(item:Object):Boolean {	
			try {			
				if 	((String(item.THEME_LIBELLE).toLowerCase().search(String(txtFiltre.text).toLowerCase()) != -1)  
					   				||
					 (String(item.LIBELLE_PRODUIT).toLowerCase().search(String(txtFiltre.text).toLowerCase()) != -1) 
					   				||					 	   
					 (String(item.CODE_ARTICLE).toLowerCase().search(String(txtFiltre.text).toLowerCase()) != -1))
					   		  
					{
						return (true);
					}else {
						return (false);
					} 
			} catch(e:Error){
				trace(e.message,e.getStackTrace());		
			}
			return true; 
		} 
		
		private function remplirComboOP(opid : int):void{		
			myTree.initDp(opid);
		}
		
		//regarde si la valeur passer en param est presente dans le tableau passer en param
		private function isPresent(s:String, a : Array):Boolean{			
			var OK:Boolean = false;			
			for (var i:int = 0; i < a.length; i++){				 
					 if ( s == a[i].toString() ) OK = true;				 
			}
			return OK;
		}
		
		private function isInArray(param:String, colname:String, acol : Array):Boolean{			
			var OK:Boolean = false;
			var ligne : Object;				
			var len : int = acol.length;
			for (ligne in acol){				
 				if (param == acol[ligne][colname]) OK = true; 
			}		
			return OK;
		}		
		
		private function is2InArray(param1:String,param2:String, colname1:String, colname2:String, acol : Array):Boolean{			
			var OK:Boolean = false;
			var ligne : Object;				
			var len : int = acol.length;
			for (ligne in acol){
				

 				if ((param1 == acol[ligne][colname1])&&(param2 == acol[ligne][colname2]))OK = true; 
			}
			trace ("OK = "+ OK)
			return OK;
		}		
		//--------------- Grid Produits Client -------------------------------------------------------------
		//la selection d'un produits dans le grid des produits client
		private function selectionnerProduitClient(ev : Event):void{			
			
			tmpSelectedProduitsClient = null;			
			tmpSelectedProduitsClient = new Array();			
			tmpSelectedProduitsClient = myGridListeProduitsClient.selectedItems;			
			//tmpSelectedProduitsClient.forEach(ajouterListedeProduits);
						
			selectedSousTete.forEach(ajouterListedeProduits);					
			//listeProduitsClientSelectionnes.source = selectedSousTete;
			
			myGridListeProduitsClientSelectionnes.dataProvider = listeProduitsClientSelectionnes;
			//listeProduitsClientSelectionnes.refresh();
		}
		
		
		
		private function ajouterListedeProduits(element:*, index:Number, arr:Array):void{
			var len : int = tmpSelectedProduitsClient.length;
			if (element.IDSOUS_TETES < 0){
				element.IDSOUS_TETES = ctNvLigne;
				element.SOUS_TETE = element.SOUS_TETE + Math.abs(ctNvLigne).toString();
				ctNvLigne --;
			}
			
			for (var i:int = 0; i< len; i++){
				var obj : Object  = new Object();								
				element.CODE_ARTICLE = tmpSelectedProduitsClient[i].CODE_ARTICLE;
				element.LIBELLE_PRODUIT = tmpSelectedProduitsClient[i].LIBELLE_PRODUIT;
				element.OPERATEURID = tmpSelectedProduitsClient[i].OPERATEURID;
				element.THEME_LIBELLE = tmpSelectedProduitsClient[i].THEME_LIBELLE;
				element.IDPRODUIT_CLIENT = tmpSelectedProduitsClient[i].IDPRODUIT_CLIENT;
				element.OPNOM = tmpSelectedProduitsClient[i].OPNOM;
				element.SEGMENT_THEME = tmpSelectedProduitsClient[i].SEGMENT_THEME;		
				element.QUANTITE = 1;
				
				obj = ObjectUtil.copy(element);
				if ((element != null) && (!is2InArray(element.IDPRODUIT_CLIENT,
												  element.IDSOUS_TETES,
												  "IDPRODUIT_CLIENT",
												  "IDSOUS_TETES",
												  listeProduitsClientSelectionnes.source))){	
														listeProduitsClientSelectionnes.addItem(obj);													  	
												  }				
			}				
		}	
					
		private function deselectProduitClient(me : MouseEvent):void{
			try{
				listeProduitsClientSelectionnes.removeItemAt(myGridListeProduitsClientSelectionnes.selectedIndex);	
				listeProduitsClientSelectionnes.refresh();
			}catch( re : RangeError ){
				trace("l'indeice sort des limites")
			}catch( e : Error){
				trace(e.message,e.getStackTrace());
			}			
		}
				
		//bt------------------------------------------------------------------------------------------------
		//annuler
		private function annulerHandler(me : MouseEvent):void{
			reInitPanel();			
			dispatchEvent(new Event("AnnulerCreation"));			
		}
		
		//gere le click sur le bouton valider
		private function chargerListeProduitsHandler(me : MouseEvent):void{
			chargerListeProduitsClient();
		}
		
		//enregistrer l'operation
		private function enregistrerHandler(me : MouseEvent):void{	
			
			//afficher alert de confirmation	
			ConsoviewAlert.afficherAlertConfirmation("Etes vous sur de vouloir Enregistrer l'Opération","Confirmation",executerEnregistrer);
		}
		
		//executer l'enregistrement
		private function executerEnregistrer(eventObj:CloseEvent):void{
			
			if (eventObj.detail == Alert.OK){				
				var tmpArray : Array = (myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).source ;//listeProduitsClientSelectionnes.source;
									
				var eventObjbis : SelectionProduitEvent = new SelectionProduitEvent("EnregistrerSelectionProduit");
				eventObjbis.tabProduits = tmpArray;	
				dispatchEvent(eventObjbis);					
			
			}
			
		}
//----------- SELECTION DES PRODUITS --------------------------------------------------------------------------------
		private function descendreLesProduisSelectionnes(me:MouseEvent):void{
				try{	
					var len : int = myGridListeProduitsClient.selectedIndices.length;
					
					if (len > 0){
							
							myGridListeProduitsClient.selectedItems.forEach(descendreLeProduitPourChaqueLigneSelectionne);
							myGridListeProduitsClientSelectionnes.dataProvider.refresh();
							myGridListeProduitsClient.dataProvider.refresh();
					}else if (len == 1){
						var index : int = myGridListeProduitsClient.selectedIndex;
						
						
						myGridListeProduitsClientSelectionnes.dataProvider.
							addItem(myGridListeProduitsClient.
							dataProvider.removeItemAt(index));	
							
						myGridListeProduitsClientSelectionnes.dataProvider.refresh();
						myGridListeProduitsClient.dataProvider.refresh();				
					}
					
				}catch( re : RangeError ){
					trace("l'indice sort des limites")
				}catch( e : Error){
					trace(e.message,e.getStackTrace());
				}	
		}
		
		private function descendreTousLesProduitsSelectionnes(me : MouseEvent):void{
			
			if ((myGridListeProduitsClient.dataProvider as ArrayCollection).length > 0){
				
				(myGridListeProduitsClient.dataProvider as ArrayCollection).source.forEach(descendreLeProduitPourChaqueLigneSelectionne);
				
				myGridListeProduitsClient.dataProvider.refresh();		
				myGridListeProduitsClientSelectionnes.dataProvider.refresh();	
			}
			
		}
		
		private function descendreLeProduitPourChaqueLigneSelectionne(element : * , index : int , arr : Array):void{				
			
			var len : int = selectedSousTete.length;
			for (var i : int = 0; i < len; i++){
				
				element.SOUS_TETE = selectedSousTete[i].SOUS_TETE;
				element.IDSOUS_TETES = selectedSousTete[i].IDSOUS_TETES;
				element.QUANTITE = 1;
				
				var obj : Object  = new Object();								
				obj = ObjectUtil.copy(element);
				
				if ((element != null) && (!is2InArray(element.IDPRODUIT_CLIENT,
												  element.IDSOUS_TETES,
												  "IDPRODUIT_CLIENT",
												  "IDSOUS_TETES",
												  (myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).source)))
				{								  	
					(myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).addItem(obj);													  	
					
				}								  
													
			}	
			
		}
		
		
		private function monterLesProduitsSelectionnes(me : MouseEvent):void{		
			try{	
				var len : int = myGridListeProduitsClientSelectionnes.selectedIndices.length;
				
				if (len > 0){
						myGridListeProduitsClientSelectionnes.selectedItems.forEach(monterLeProduit);
						myGridListeProduitsClientSelectionnes.dataProvider.refresh();
						myGridListeProduitsClient.dataProvider.refresh();
				}
				
			}catch( re : RangeError ){
				trace("l'indeice sort des limites")
			}catch( e : Error){
				trace(e.message,e.getStackTrace());
			}			
		}
		
		private function monterTousLesProduitsSelectionnes(me:MouseEvent):void{
			if ((myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).length > 0){
				(myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).source.forEach(monterLeProduit);
				myGridListeProduitsClientSelectionnes.dataProvider.refresh();
				myGridListeProduitsClient.dataProvider.refresh();			
			}
				
		}
		
		private function monterLeProduit(element : * , index : int , arr : Array):void{
			
				(myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection)
					.removeItemAt(0);								
		}
			
		


			
//----------- REMOTINGS ------------------------------------------------------------------------------------//
		//------------------------------------
		//récupere les lignes du noeud selectioné							
		private function getLinesFromNode():void{
			
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.inventaire.cycledevie.recherche.facade",
												  "rechercherListeLigne",
												  getLinesFromNodeResultHandler,
												  defaultFaultHandler);
												  
			RemoteObjectUtil.callService(op,
										nodePerimetre,										
										nodeId); 
		}
		
		private function getLinesFromNodeResultHandler(re : ResultEvent):void{
			var nvelleLigneObject : Object = new Object();
			nvelleLigneObject.SOUS_TETE = "Nouvelle ligne";
			nvelleLigneObject.IDSOUS_TETES = 0; 
			
			listeSousTete = re.result as ArrayCollection;	
						
			if (listeSousTete.length > 0) {				
				txtFiltreLigne.enabled = true;								
				//listeSousTete.addItemAt(nvelleLigneObject,0);	
			}
			else {				
				listeSousTete.addItem(nvelleLigneObject);	
				//txtFiltreLigne.enabled = false;				
			}			
			myGridLigne.dataProvider = listeSousTete;
		}
		
		//récupere les Produits Client pour une ligne							
		private function chargerListeProduitsClient():void{
				/* if (selectedSousTete[0].IDSOUS_TETES == 0 ){
					filtre = 1; // pour les nvelle ligne 					
				
				} else 	if (selectedSousTete[0].IDSOUS_TETES > 0 ){
					 filtre = 0;
				} */
				op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.inventaire.cycledevie.ElementDInventaire",
												  "getListeProduitsClient",
												  chargerListeProduitsClientResultHandler,
												  defaultFaultHandler);
												  
				RemoteObjectUtil.callService(op,selectedSousTete,filtre); 						
		}			
		
		private function chargerListeProduitsClientResultHandler(re : ResultEvent):void{
			/// charger liste elements de facturation 		
				listeProduitsClient = re.result as ArrayCollection;	 
				myGridListeProduitsClient.dataProvider = listeProduitsClient;
				txtFiltre.enabled = true;			 
		}
		
		//-organisation-operateur du client
		private function getListeOperateur():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var typeGroupe : String = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"getListeOperateur",
																				getListeOperateurResultHandler,
																				defaultFaultHandler);				
			RemoteObjectUtil.callService(op,idGroupeMaitre
										   ,typeGroupe);		
		}
		
		private function getListeOperateurResultHandler(re :ResultEvent):void{
			
			cbxOperateur.dataProvider = re.result;
			cbxOperateur.labelField = "OPNOM"; 
			 
		}
						
		///Fault--------------------------------------------------		
		private function defaultFaultHandler(fe : FaultEvent):void{
			trace("error remoting");
		}	
				
///- FIN --- REMOTINGS ------------------------------------------------------------------------------------------------------------------		
					
		//reinitialiser le composants
		private function reInitPanel():void{
			/* myGridLigne.dataProvider = null;			
			myTree.dataProvider = null;
			lblNbNoeud.text = "";
			cbFiltre.selected = false;	 */
						
		}	
		
	}
}