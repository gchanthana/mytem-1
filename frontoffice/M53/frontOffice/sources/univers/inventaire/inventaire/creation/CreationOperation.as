package univers.inventaire.inventaire.creation
{
	import composants.tb.effect.EffectProvider;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	
	import univers.inventaire.inventaire.creation.defaultpanel.DefaultPanel;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts.ContactChangeEvent;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts.GestionContacts;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Contact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
	import univers.inventaire.inventaire.creation.operationresiliation.EnregistrerOperationEvent;
	import univers.inventaire.inventaire.creation.operationresiliation.IdentificationRessourcesResiliation;
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;
	import univers.inventaire.inventaire.creation.operationresiliation.ExportDemandeResiliation;
	import univers.inventaire.inventaire.export.ExportBuilder;
	
	
	/**
	 * Classe gerant l'ihm de creation d'opération
	 * */
	public class CreationOperation extends CreationOperationIHM
	{
		//---- remoting---------------------------------
		
		//Reference vers la methode distante qui ramene la liste des types d'operations
		private var opListeTypeOpe : AbstractOperation;
		
		//ArrayCollection contenant la liste des type d'opérations
		[Bindable]
		private var listeTypeOperations : ArrayCollection;
		
		//Reference vers la methode distante qui enregistrer l'operation
		private var opSaveOpe : AbstractOperation;
		
		//-------------------------------------------
		
		
		
		//Constante definissant les operation de type résiliation
		private const RESILIATION : int = 1;	
		
		//Constant definissant les opération de type Création
		private const COMMANDE : int = 2;

		//L'identifiant de 'operation (= 0 pour une creation d'opération)
		private var idOperation : int = 0;
	
		//Qualificatif de l'operation 'en cours ou pas' (= 1 pour une creation d'opération) 	
		private var flagEnCours : uint = 1;
		
		//L'identifiant du groupe client sur lequelle on est connecté
		private var idGroupe_client : int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
		
		//l'identifiant de l'opération 	maitre (= 0 pour une creation d'opération)		
		private var idOperation_maitre : int;
	
		//la date de cloture pour l'operation (=null pour une creation)
		private var date_cloture : String;
		
		//Reference vers l'objet gerant l'identification des ressource pour résiliation 
		private var myIdentificationRessourcesResiliationPanel : IdentificationRessourcesResiliation;	
	
		//private var myIdentificationRessourcesCreationPanel : IdentificationRessourcesCreations;  
 		
 		//reference vers un panel vide (plus utilisé)
 		private var myDefaultPanel : DefaultPanel;
 		
 		//Agence ou distributeur pour l'operation
 		private var societe : Societe = new Societe();
		
		//liste des produits selectionnés		
		private var tabProduitsSelectionnes : Array;
		
		//La date de la demande
		private var dateDemande : String ="";
		
		/**
		 * Constructeur
		 **/
		public function CreationOperation()
		{
			super();
			myDefaultPanel = new DefaultPanel();
			myDefaultPanel.addEventListener("sortir",AnnulerCreationHandler);			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);			
		}
		
		/**
		 *Remet tout à zero
		 * */ 
		public function clean():void{
			try{
				pnlContact.clean();
				myIdentificationRessourcesResiliationPanel.clean();
				lblOperation.text = "";
				txtCommentaire.text = "";	
				txtReferenceOpe.text = "";
				txtReferenceCli.text = "";			
				//cbxTypeOperation.selectedIndex = -1;
				 				
			}catch(e : Error){
				trace("ERROR IDETIFICATION RESSOURCES CLEAN");
			}
		} 
		
		/*---------------------------------- PRIVATE -------------------------------------------------*/
 		
		//Initialisation de l'ihm
		private function initIHM(fe : FlexEvent):void{						
			//chargerListeTypeOperation();	
			//cbxTypeOperation.addEventListener(Event.CHANGE,switchTypeCreation);	
			switchTypeCreation(null);	
			//conteneur.addChild(myDefaultPanel);					
			pnlContact.addEventListener(GestionContacts.SOCIETE_CHANGED,setSociete);
		}		
		
		//Affecte la societe à l'opération
		private function setSociete(cce : ContactChangeEvent):void{
			societe = cce.societe;
			societe.addEventListener(Societe.LISTE_OPERATEUR_COMPLETE,setListOperateur)
			societe.preparelistOperateurSociete(societe.societeID);
		}
		
		//Affecte la liste des operatueus de la societe au formulaire de creation d'opération
		//Quand la societe change
		private function setListOperateur (ev : Event):void{
			myIdentificationRessourcesResiliationPanel.setListOperateur(societe.listeOperateurs);
		}
		
		//Affiche le formulaire de creation d'operation
		private function switchTypeCreation(ev : Event):void{
		//	switch(parseInt(cbxTypeOperation.selectedItem.IDINV_TYPE_OPE)){
		//		case RESILIATION :
					try{
						conteneur.removeChildAt(0);
					}catch(e : Error){
						trace("pas de panel creation a supprimer");
					}finally{
						myIdentificationRessourcesResiliationPanel = new IdentificationRessourcesResiliation();
						myIdentificationRessourcesResiliationPanel.addEventListener("AnnulerCreation",AnnulerCreationHandler);
						myIdentificationRessourcesResiliationPanel.addEventListener("EnregistrerSelectionProduit",enregistrerSelectionProduitHandler);
						myIdentificationRessourcesResiliationPanel.addEventListener(IdentificationRessourcesResiliation.AFFICHER_PDF,afficherPDF);
						conteneur.addChild(myIdentificationRessourcesResiliationPanel);						
					}								
	//			break;
				
	//			case COMMANDE :
	//				try{
	//					conteneur.removeChildAt(0);
	//				}catch(e : Error){
	//					trace("pas de panel creation a supprimer");
	//				}finally{
	//					myIdentificationRessourcesCreationPanel = new IdentificationRessourcesCreations();
	//					myIdentificationRessourcesCreationPanel.addEventListener("AnnulerCreation",AnnulerCreationHandler);
	//					myIdentificationRessourcesCreationPanel.addEventListener("EnregistrerSelectionProduit",enregistrerSelectionProduitHandler);
	//					conteneur.addChild(myIdentificationRessourcesCreationPanel);						
	//				}								
	//			break;	
	//			
	//			case 999 :
	//				try{
	//					conteneur.removeChildAt(0);
	//				}catch(e : Error){
	//					trace("pas de panel creation a supprimer");
	//				}finally{
	//					//conteneur.addChild(myIdentificationRessourcesResiliationPanel);
	//				}
	//			break;
	//			
	//			
	//		}
			
		}
		
		
		//Annuler la creation de l'opération
		//dispatch un evenement signifiant que l'on à annuler la saisie du formulaire de creation d'operation		
		private function AnnulerCreationHandler(ev : Event):void{						
			dispatchEvent(new Event("AnnulerCreation"));	
		}
		
		//Enregistre l'operation avec la liste de produits selectionnées
		//Verifie la validiter des éléments du formulaire		
		private function enregistrerSelectionProduitHandler(ev : SelectionProduitEvent):void{			
			var message : String = "";
			var valide  : Boolean = true;
			
			/* if ( cbxTypeOperation.selectedItem == null){
				message == message +"Vous devez choisir un type d'operation\n";
				EffectProvider.FadeThat(cbxTypeOperation);
				cbxTypeOperation.setFocus();
				 cbxTypeOperation.setStyle( "fillColors",[0xff0000, 0xff6600, 0xff6600, 0xff0000]);
				valide = false;				
			} */
						
			if ( ev.tabProduits.length == 0){
				
				message = message + " - Vous devez selectionner des produits\n";			
						
				valide = false;					
			}
			
			if ( lblOperation.text.length == 0){
				message = message + " - Vous devez saisir un libellé pour cette operation\n";
				EffectProvider.FadeThat(lblOperation);
				
				lblOperation.setStyle("backgroundColor","red");
				lblOperation.setStyle("backgroundColorAlpha",.1);
				
				lblOperation.setFocus();				
				valide = false;					
			}		
			
			if (( cvDateDemande.text == null)||(cvDateDemande.text.length < 10)){
				message = message + " - Vous devez saisir une date pour cette operation\n";
				EffectProvider.FadeThat(cvDateDemande);
				
				cvDateDemande.setStyle("backgroundColor","red");
				cvDateDemande.setStyle("backgroundColorAlpha",.1);
				
				cvDateDemande.setFocus();				
				valide = false;					
			}
					
			
			if (valide){
				
				tabProduitsSelectionnes = ev.tabProduits;	
				dateDemande = cvDateDemande.text;							
				enregistrerOperation();						
				
			}else{
				Alert.show(message,"Erreur");
			}
			
		}
		
		
		/*
			//formate les donnees du commbo type operation
			private function formaterDonneeCombo(d : ArrayCollection):ArrayCollection{
			var newData : ArrayCollection = new ArrayCollection();
			var ligne : Object;
			for (ligne in d){
				if ((d[ligne].IDINV_TYPE_OPE != 41 ) 
					&& (d[ligne].IDINV_TYPE_OPE != 61 )
					&& (d[ligne].IDINV_TYPE_OPE != 62 )){	
					newData.addItem(d[ligne]);
				}								
			}
			return newData;
		} */
		
		
		//----------------- REMOTING -------------------------------------------------------------------
		
		
		/* private function chargerListeTypeOperation():void{			
		
 				opListeTypeOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"getListeTypeOperation",
																				listeTypeOperationResultHandler,
																				defaultFaultHandler);				
				RemoteObjectUtil.callService(opListeTypeOpe);
		}
		 */
		/* private function listeTypeOperationResultHandler(re : ResultEvent):void{
			listeTypeOperations = re.result as ArrayCollection;		
		//	cbxTypeOperation.dataProvider = formaterDonneeCombo(listeTypeOperations);
		//	cbxTypeOperation.labelField = "TYPE_OPERATION";	
		} */
			
		
		
		//Enregistre l'opération
		private function enregistrerOperation():void{					
 				opSaveOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"enregistrerOperation",
																				enregistrerOperationResultHandler);				

				
				var societeId : int = (societe.societeID > 0)?societe.societeID:0;
				var contactId : int = 0;												
				if (pnlContact.cmbContact.selectedItem){
					contactId = (Contact(pnlContact.cmbContact.selectedItem).contactID > 0)? Contact(pnlContact.cmbContact.selectedItem).contactID : 0;
				}
				
				RemoteObjectUtil.callService(opSaveOpe ,idOperation
													   ,RESILIATION
													   ,lblOperation.text													  
													   ,flagEnCours
													   ,txtCommentaire.text
													   ,date_cloture
													   ,idOperation_maitre
													   ,idGroupe_client
													   ,tabProduitsSelectionnes
													   ,societeId
													   ,contactId
													   ,dateDemande
													   ,txtReferenceCli.text													   
													   ,txtReferenceOpe.text
													   
													   )		 
				
		}
		
		
		//Handler de la méthode 'enregistrerOperation'
		//En cas de succes, dispatch un evenement EnregistrerOperationEvent de type 'EnregistrerOperation'
		//signifiant qu'une opération vient d'être céée
		private function enregistrerOperationResultHandler(re : ResultEvent):void{
			
			 if (parseInt(re.result.toString()) > 0){	
			 	var evtObj : EnregistrerOperationEvent = new EnregistrerOperationEvent("EnregistrerOperation");
			 	evtObj.idOperation = Number(re.result);
			 
				dispatchEvent(evtObj);
			}else{
				Alert.show("Une erreur c'est produite durand l'enregistrement");
			}
		}
		
		///----------------- PDF ------------------------------------///
		private function afficherPDF(spe : SelectionProduitEvent):void{
			 var resiExp : ExportDemandeResiliation = new ExportDemandeResiliation();
			 
			 resiExp.libelle = lblOperation.text;
			 resiExp.commentaire = txtCommentaire.text;
			 resiExp.ref_interne = txtReferenceCli.text;
			 resiExp.ref_operateur = txtReferenceOpe.text;
			 
			 resiExp.contactId = pnlContact.getContact();
			 resiExp.societeId = pnlContact.getSociete();			
			 resiExp.operateurNom =  spe.tabProduits[0].OPNOM;
			 resiExp.panier = spe.tabProduits;
			 
			 var expB : ExportBuilder = new ExportBuilder(resiExp);
			 expB.exporter(ExportDemandeResiliation.FORMAT_PDF);
		}
	}
}
