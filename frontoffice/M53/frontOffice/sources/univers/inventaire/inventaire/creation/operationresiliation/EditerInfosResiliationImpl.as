package univers.inventaire.inventaire.creation.operationresiliation
{
	import flash.events.MouseEvent;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.EditerInfosCommandeImpl;
	import univers.inventaire.inventaire.etapes.OperationVO;
	
	[Bindable]
	public class EditerInfosResiliationImpl extends EditerInfosCommandeImpl
	{
		public function EditerInfosResiliationImpl()
		{
			super();
		}
		
		private var _operation : OperationVO;
		public function set operation(value : OperationVO):void{
			_operation = value;
		}
		public function get operation():OperationVO{
			return _operation;
		}
		
		
		override protected function btUpdateClickHandler(me:MouseEvent):void{
			
		}
	}
}