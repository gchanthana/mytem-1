package univers.inventaire.equipements.fournisseurs.vo
{
	public class ContactFournisseurVo
	{
		public var IDFOURNISSEUR:Number=0;
		public var NOM:String="";		//	VARCHAR2(50)
		public var PRENOM:String="";	//	VARCHAR2(50)
		public var EMAIL:String="";		//	VARCHAR2(50)
		public var ADRESSE1:String="";	//	VARCHAR2(200)
		public var ADRESSE2:String=""; 	//	VARCHAR2(200)
		public var CP:String="";		//	VARCHAR2(10)
		public var VILLE:String=""; 	//	VARCHAR2(100)
		public var PAYS:String=""; 		//france par défaut
			
		public function ContactFournisseurVo()
		{	
		}
	}
}