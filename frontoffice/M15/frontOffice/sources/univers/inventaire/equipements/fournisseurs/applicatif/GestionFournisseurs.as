package univers.inventaire.equipements.fournisseurs.applicatif
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.fournisseurs.vo.ContactFournisseurVo;
	import univers.inventaire.equipements.fournisseurs.vo.FournisseurVo;
	
	[Bindable]
	public class GestionFournisseurs
	{
		
		// === Forunisseurs
		private var _listeFournisseurs : ArrayCollection;
		public function get listeFournisseurs():ArrayCollection{
			return _listeFournisseurs;
		}
		public function set listeFournisseurs(liste : ArrayCollection):void{
			_listeFournisseurs = liste;
		}
		
		private var _leFournisseurSelectionne : FournisseurVo
		public function get leFournisseurSelectionne():FournisseurVo{
			return _leFournisseurSelectionne;
		}
		public function set leFournisseurSelectionne(unFournisseur : FournisseurVo):void{
			_leFournisseurSelectionne = unFournisseur;
		}
		
		
		// === Contacts
		
		private var _listeContacts : ArrayCollection;
		public function get listeContacts():ArrayCollection{
			return _listeContacts;
		}
		public function set listeContacts(liste : ArrayCollection):void{
			_listeContacts = liste;
		}
		
		private var _leContactSelectionne : ContactFournisseurVo
		public function get leContactSelectionne():ContactFournisseurVo{
			return _leContactSelectionne;
		}
		public function set leContactSelectionne(unContact : ContactFournisseurVo):void{
			_leContactSelectionne = unContact;
		}
		
		
		public function GestionFournisseurs()
		{
		}
		
		
		
		//Fournit la liste des fournisseurs
		public function getListeFournisseurs(chaine : String = ""):void{
			
			if (listeFournisseurs != null) listeFournisseurs = null;
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.equipement",
																					"getListeFournisseurs",
																					getListeFournisseursResultHandler);
																																										
				
				RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX.toString(),chaine);
		}
		private function getListeFournisseursResultHandler(re : ResultEvent):void{			
			if (re.result){
				listeFournisseurs = re.result as ArrayCollection;
			}			
		}
		
		//Met à jour les infos d'un fournisseur
		private var tmpFournisseur : FournisseurVo;
		public function majFournisseur(fournisseur : FournisseurVo):void{
			tmpFournisseur = fournisseur;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.equipement",
																					"updateFournisseur",
																					majFournisseurResultHandler);
																																										
				 
				RemoteObjectUtil.callService(op, leFournisseurSelectionne.IDFOURNISSEUR,
												 leFournisseurSelectionne.IDGROUPE_CLIENT,
												 leFournisseurSelectionne.NOM_FOURNISSEUR,
												 leFournisseurSelectionne.ADRESSE1_HQ,
												 leFournisseurSelectionne.ADRESSE2_HQ,
												 leFournisseurSelectionne.CODE_POSTAL_HQ, 
												 leFournisseurSelectionne.COMMUNE_HQ,
												 leFournisseurSelectionne.PAYSID_HQ,
												 leFournisseurSelectionne.PAYSORIGINEID_HQ,
												 leFournisseurSelectionne.TYPE_FOURNISSEUR);
			
		}
		
		private function majFournisseurResultHandler(re : ResultEvent):void{
			if(re.result > 0){
				
				leFournisseurSelectionne.ADRESSE1_HQ = tmpFournisseur.ADRESSE1_HQ;
				leFournisseurSelectionne.ADRESSE2_HQ = tmpFournisseur.ADRESSE2_HQ
				leFournisseurSelectionne.CODE_POSTAL_HQ = tmpFournisseur.CODE_POSTAL_HQ;
				leFournisseurSelectionne.COMMUNE_HQ = tmpFournisseur.COMMUNE_HQ;
				leFournisseurSelectionne.NOM_FOURNISSEUR = tmpFournisseur.NOM_FOURNISSEUR;
				leFournisseurSelectionne.PAYS_HQ = tmpFournisseur.PAYS_HQ;
				leFournisseurSelectionne.PAYSID_HQ = tmpFournisseur.PAYSID_HQ;
				leFournisseurSelectionne.PAYSORIGINEID_HQ = tmpFournisseur.PAYSORIGINEID_HQ;
				leFournisseurSelectionne.PAYSORIGINE_HQ = tmpFournisseur.PAYSORIGINE_HQ;
				leFournisseurSelectionne.TYPE_FOURNISSEUR = tmpFournisseur.TYPE_FOURNISSEUR;
				leFournisseurSelectionne.TYPE_FOURNISSEUR_LIBELLE = tmpFournisseur.TYPE_FOURNISSEUR_LIBELLE;
								
				listeFournisseurs.itemUpdated(leFournisseurSelectionne);
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Une erreur s'est produite : " + re.result.toString());
			}
		}
		
		
		public function creerFournisseur(fournisseur : FournisseurVo):void{
			leFournisseurSelectionne = fournisseur;					
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.equipement",
																					"addFournisseur",
																					creerFournisseurResultHandler);
			
			RemoteObjectUtil.callService(op, leFournisseurSelectionne.IDGROUPE_CLIENT,
											 leFournisseurSelectionne.NOM_FOURNISSEUR,
											 leFournisseurSelectionne.ADRESSE1_HQ,
											 leFournisseurSelectionne.ADRESSE2_HQ,
											 leFournisseurSelectionne.CODE_POSTAL_HQ, 
											 leFournisseurSelectionne.COMMUNE_HQ, 
											 leFournisseurSelectionne.PAYSID_HQ.toString(),
											 leFournisseurSelectionne.PAYSORIGINEID_HQ.toString(),
											 leFournisseurSelectionne.TYPE_FOURNISSEUR);
		}
		
		private function creerFournisseurResultHandler(re : ResultEvent):void{
			if(re.result > 0){
				leFournisseurSelectionne.IDFOURNISSEUR = (re.result as Number)
				listeFournisseurs.addItem(leFournisseurSelectionne);
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Une erreur s'est produite : " + re.result.toString());
			}
		}
		
		
		//Supprime un fournisseur
		//si aucun équipement n'est lié
		public function supprimerFournisseur(fournisseur : FournisseurVo):void{
			leFournisseurSelectionne = fournisseur;
			
			if (leFournisseurSelectionne.TYPE_FOURNISSEUR != 0 && leFournisseurSelectionne.IDGROUPE_CLIENT > 0){
				var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.equipement",
																					"deleteFournisseur",
																					supprimerFournisseurResultHandler);
				
				RemoteObjectUtil.callService(op, leFournisseurSelectionne.IDFOURNISSEUR.toString());	
			}else{
				Alert.show("Impossible de supprimer ce revendeur.");
			}
		}
		
		private function supprimerFournisseurResultHandler(re : ResultEvent):void{
			var message : String = "";
			
			switch(re.result){
				case -2 :{
					message = "Des équipements sont affectés à ce Revendeur. Veuillez supprimer ces équipements avant de supprimer le revendeur : " + re.result.toString();
					break;
				} 
				case -1 :{
					message = "Une erreur s'est produite : " + re.result.toString();
					break;
				} 
				default :{
					message = "Revendeur effacé";
					listeFournisseurs.removeItemAt(listeFournisseurs.getItemIndex(leFournisseurSelectionne));
				};
				
				Alert.okLabel = "Fermer";
				Alert.show(message);
			}
		}	
	
	}
}