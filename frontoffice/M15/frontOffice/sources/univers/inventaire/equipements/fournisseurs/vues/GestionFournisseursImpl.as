package univers.inventaire.equipements.fournisseurs.vues
{
	import composants.util.TextInputLabeled;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.equipements.catalogue.ChoixGammeAjoutItem;
	import univers.inventaire.equipements.fournisseurs.applicatif.GestionFournisseurs;
	import univers.inventaire.equipements.fournisseurs.vo.FournisseurVo;
	
	[Bindable]
	public class GestionFournisseursImpl extends BaseViewImpl
	{	
		public var btAjouterFournis:Button;
		public var btEditerFournis:Button;
		public var btSupprimerFournis:Button;
		public var dgFournisseurs:DataGrid;
		public var txtRecherche:TextInputLabeled;
		public var txtFiltre:TextInputLabeled;
		public var imgRechercher : Image;
		
		private var _modeEcriture : Boolean = false;				
		public function get modeEcriture():Boolean{
		 	return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)
		}
		public function set modeEcriture(mode : Boolean):void{
			//ne fait rien	
		}
		
		
		protected var _gestionFournisseur : GestionFournisseurs;
		public function get gestionFournisseur():GestionFournisseurs{
			return _gestionFournisseur;
		}
		public function set gestionFournisseur(gest : GestionFournisseurs):void{
			_gestionFournisseur = gest;
		}
		
		//La fenetre d'ajout de fournisseur
		protected var _ajouterFournisseurWindow : ChoixGammeAjoutItem;
		
		override protected function commitProperties():void{
			
			imgRechercher.addEventListener(MouseEvent.CLICK,imgRechercherClickHandler);
			txtRecherche.addEventListener(FlexEvent.ENTER,txtRechercheEnterHandler);
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			
			
			btAjouterFournis.addEventListener(MouseEvent.CLICK,btAjouterFournisClickHandler);
			btSupprimerFournis.addEventListener(MouseEvent.CLICK,btSupprimerFournisClickHandler);
			btEditerFournis.addEventListener(MouseEvent.CLICK,btEditerFournisClickHandler);
			dgFournisseurs.addEventListener(ListEvent.CHANGE,dgFournisseursChangeHandler);
			dgFournisseurs.addEventListener(FlexEvent.UPDATE_COMPLETE,dgFournisseursUpdateCompleteHandler);
			
			
			btAjouterFournis.enabled = modeEcriture;
			btSupprimerFournis.enabled = modeEcriture;
			btEditerFournis.enabled = modeEcriture;
		}
		
		public function GestionFournisseursImpl()
		{
			super();			
			gestionFournisseur = new GestionFournisseurs();
		}
		
		
		public function init():void{
			
		}
		
		//============HANDLERS ================================================================================
		protected function btAjouterFournisClickHandler(me : MouseEvent):void{
			if (_ajouterFournisseurWindow != null ) _ajouterFournisseurWindow = null;
			_ajouterFournisseurWindow = PopUpManager.createPopUp(this, ChoixGammeAjoutItem, true) as ChoixGammeAjoutItem;
			_ajouterFournisseurWindow.setParentRef(this);
			_ajouterFournisseurWindow.setType(1);
		}
		
		
		protected function btSupprimerFournisClickHandler(me : MouseEvent):void{
			if(dgFournisseurs.selectedIndex!= -1){
				
				gestionFournisseur.leFournisseurSelectionne = (dgFournisseurs.selectedItem as FournisseurVo);
				gestionFournisseur.supprimerFournisseur(dgFournisseurs.selectedItem as FournisseurVo);
				
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Vous devez sélectionner un fournisseur")
			}
		}
		protected function btEditerFournisClickHandler(me : MouseEvent):void{
			if(dgFournisseurs.selectedIndex!= -1){
				gestionFournisseur.leFournisseurSelectionne = (dgFournisseurs.selectedItem as FournisseurVo)
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Vous devez sélectionner un fournisseur")
			}	
		}
		
		protected function dgFournisseursChangeHandler(me : MouseEvent):void{
			
		}
		
		protected function dgFournisseursUpdateCompleteHandler(fe : FlexEvent):void{
			if(dgFournisseurs.initialized){
				
			}
		}
		
		protected function imgRechercherClickHandler(me : MouseEvent):void{
			gestionFournisseur.getListeFournisseurs(txtRecherche.text);	
		}
		
		protected function txtRechercheEnterHandler(fe : FlexEvent):void{
			gestionFournisseur.getListeFournisseurs(txtRecherche.text);
		}
		
		protected function txtFiltreChangeHandler(ev : Event):void{
			if(gestionFournisseur.listeFournisseurs != null){
				
				gestionFournisseur.listeFournisseurs.filterFunction = filtrerGdFournisseurs; 
				gestionFournisseur.listeFournisseurs.refresh();
				
			}
		}		
		//============FIN HANDLERS ============================================================================
		
		private function filtrerGdFournisseurs(item : Object):Boolean{
			return true
		}
		
	}
}