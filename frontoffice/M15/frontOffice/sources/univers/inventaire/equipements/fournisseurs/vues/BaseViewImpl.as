package univers.inventaire.equipements.fournisseurs.vues
{
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import mx.containers.VBox;
	import mx.controls.dataGridClasses.DataGridColumn;
	
	public class BaseViewImpl extends VBox
	{
		public function BaseViewImpl()
		{
			super();
		}
		
		
		//==== FORMATEURS =============================================================
		protected function formateDates(item : Object, column : DataGridColumn):String{
			if (item != null && item[column.dataField] != null){
				return DateFunction.formatDateAsString(item[column.dataField]);				
			}else return "-";
		}
		
		protected function formateEuros(item : Object, column : DataGridColumn):String{			
			if (item != null) return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2);	
			else return "-";
		}
		
		protected function formateNumber(item : Object, column : DataGridColumn):String{
			if (item != null) return ConsoviewFormatter.formatNumber(Number(item[column.dataField]),2);
			else return "-";
		}
		
		protected function formateLigne(item : Object, column : DataGridColumn = null):String{			
			if (item != null && column != null) return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);		
			else return "-";
		}
		
		protected function formateBoolean(item : Object, column : DataGridColumn):String{
			if (item != null) return (Number(item[column.dataField]) == 1)?"OUI":"NON";
			else return "-";
		}
		
		//====FIN  FORMATEURS =============================================================
		
		protected function onPerimetreChange():void{}
		
		protected function logoff():void{}

	}
}