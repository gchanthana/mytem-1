package univers.inventaire.equipements.lignes.fiche.ll {
	import univers.inventaire.equipements.lignes.fiche.AbstractFicheLigne;
	import univers.inventaire.equipements.lignes.fiche.ll.menu.Infos;
	import univers.inventaire.equipements.lignes.fiche.ll.menu.Localisation;

	public class FicheLigneLL extends AbstractFicheLigne {
		/**
		 * On utilise toujours le constructeur de AbstractFicheLigne
		 * Ce constructeur parent initialise le champs sousteteObject.
		 * */
		public function FicheLigneLL(sousteteObject:Object) {
			super(sousteteObject);
		}
		
		/**
		 * Dans cette méthode on crée les menus qui vont constituer la fiche
		 * puis on les ajoute avec la méthode addMenuList(...menus) :
		 * Le paramètre menus est la liste des menu à ajouter
		 * Enfin on définit le menu de retour avec la méthode
		 * setReturnAt(indexDuRetour,libelléDuRetour) :
		 * indexDuRetour est l'index du menu de retour. Sa valeur doit être comprise
		 * entre 0 et nbre total des menus (sans compter le menu de retour).
		 * et libelléDuRetour le libellé du menu de retour.
		 * Consulter la doc dans :
		 * consoview.annuaire.site.fiche.FicheLigne
		 * */
		protected override function createIhmItems():void {
			var infosIHM:Infos = new Infos(this.sousteteObject);
			var localisationIHM:Localisation = new Localisation(this.sousteteObject);
			addMenuList(infosIHM,localisationIHM);
		
		}
	}
}
