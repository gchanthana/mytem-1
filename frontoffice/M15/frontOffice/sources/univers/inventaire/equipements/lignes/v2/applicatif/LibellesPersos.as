package univers.inventaire.equipements.lignes.v2.applicatif
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.messaging.MessageAgent;
	import mx.messaging.MessageResponder;
	import mx.messaging.events.MessageAckEvent;
	import mx.messaging.messages.IMessage;
	import mx.rpc.AbstractOperation;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	import univers.inventaire.equipements.lignes.v2.vo.LibellePerso;
	import mx.collections.ArrayCollection;
	
	 
	
	
	[Event(name="libelleChanged")]
	
	
	[Bindable]
	public class LibellesPersos extends EventDispatcher
	{
		public static const NB_LIBELLE : Number = 10;
		public var libelle1 : String = "Sans titre";		
		public var libelle2 : String = "Sans titre";
		public var libelle3 : String = "Sans titre";
		public var libelle4 : String = "Sans titre";
		public var libelle5 : String = "Sans titre";
		public var libelle6 : String = "Sans titre";
		public var libelle7 : String = "Sans titre";
		public var libelle8 : String = "Sans titre";
		public var libelle9 : String = "Sans titre";
		public var libelle10 : String = "Sans titre";
		public var libelle11 : String = "Sans titre";
		public var libelle12 : String = "Sans titre";
		public var libelle13 : String = "Sans titre";
		public var libelle14 : String = "Sans titre";
		public var libelle15 : String = "Sans titre";
		public var libelle16 : String = "Sans titre";
		public var libelle17 : String = "Sans titre";
		public var libelle18 : String = "Sans titre";
		public var libelle19 : String = "Sans titre";
		public var libelle20 : String = "Sans titre";
		public var libelle21 : String = "Sans titre";
		public var libelle22 : String = "Sans titre";
		public var libelle23 : String = "Sans titre";
		public var libelle24 : String = "Sans titre";
		public var libelle25 : String = "Sans titre";
		public var libelle26 : String = "Sans titre";		
		
		public function LibellesPersos(){
			
		}
		
		public function updateAllLibelles(c1:String,
										  c2:String,
										  c3:String,
										  c4:String,
										  c5:String,
										  c6:String,
										  c7:String,
										  c8:String,
										  c9:String,
										  c10:String):void{
										  	
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"updateCustomFieldsLabel",
																				updateAllLibellesResultHandler);
				RemoteObjectUtil.callService(op,c1,
												c2,
												c3,
												c4,
												c5,
												c6,
												c7,
												c8,
												c9,
												c10)
				
			 
		}
		
		private function updateAllLibellesResultHandler(re : ResultEvent):void{			
			if(re.result > 0){
				Alert.okLabel = "Fermer";
				Alert.show("Champs mis à jour","INFO",Alert.OK);				
				for (var i : Number = 1; i< NB_LIBELLE+1; i++){					
					if (re.token.message.body[i-1] != "Sans titre"){
						this["libelle"+i] = String(re.token.message.body[i-1]);
					}
				}
				
				
			}else{
				Alert.show("Une erreur s'est produite lors de la mise à jour","Erreur");
			}
		}		
		
		
		public function updateLibelle(libelleVo : LibellePerso):void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"updateCustomFieldLabel",
																				updateLibelleResultHandler);
			RemoteObjectUtil.callService(op,Number(libelleVo.idLibelle),libelleVo.libelle)
		}
		private function updateLibelleResultHandler(re : ResultEvent):void{			
			if(re.result){
				this["libelle"+re.token.message.body[0]] = String(re.token.message.body[1]);
				dispatchEvent(new Event("libelleChanged"));
			}else{
				Alert.show("Une erreur s'est produite lors de la mise à jour","Erreur");
			}
		}		
		
		
		public function getLibellesPersos():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getCustomFieldsLabel",
																				getLibellesPersosResultHandler);
			RemoteObjectUtil.callService(op)
		}
		private function getLibellesPersosResultHandler(re : ResultEvent):void{			
			if(re.result && (re.result as ArrayCollection).length > 0){
				var tabResult : ArrayCollection = re.result as ArrayCollection;
				for (var i : Number = 1; i< NB_LIBELLE+1; i++){					
					if (tabResult[0]["C"+i] != null){
						this["libelle"+i] = tabResult[0]["C"+i]
					} 
				}
			}
		}
	}
}