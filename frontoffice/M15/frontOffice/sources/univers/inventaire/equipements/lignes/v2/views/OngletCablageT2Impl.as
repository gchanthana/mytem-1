package univers.inventaire.equipements.lignes.v2.views
{
	import mx.containers.VBox;
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import flash.display.DisplayObject;
	import composants.util.TextInputLabeled;
	import mx.controls.CheckBox;
	import mx.controls.TextArea;
	import mx.containers.HBox;
	import flash.events.MouseEvent;
	import mx.controls.Button;
	import mx.containers.FormItem;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import univers.inventaire.equipements.lignes.v2.vo.TrancheSDAVo;
	import mx.managers.PopUpManager;
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class OngletCablageT2Impl extends BaseView implements IOngletCablage
	{
		
		
		//==================================================================================================
		public var NOM_SITE_INSTALL:TextInputLabeled;
		public var ADRESSE1_INSTALL:TextInputLabeled;
		public var ADRESSE2_INSTALL:TextInputLabeled;
		public var ZIPCODE_INSTALL:TextInputLabeled;
		public var VILLE_INSTALL:TextInputLabeled;
		
		public var REGLETTE1:TextInputLabeled;
		public var PAIRE1:TextInputLabeled;
		public var REGLETTE2:TextInputLabeled;
		public var PAIRE2:TextInputLabeled;
		public var BAT:TextInputLabeled;
		public var ETAGE:TextInputLabeled;
		public var SALLE:TextInputLabeled;
		public var NOEUD:TextInputLabeled;
		public var AMORCE:TextInputLabeled;
		public var CARTE_PABX:TextInputLabeled;
		public var COMMENTAIRE_CABLAGE:TextArea;
		
		public var NOMBRE_POSTES:TextInputLabeled;
		public var SECRET_IDENTIFIANT:CheckBox;
		public var PUBLICATION_ANNUAIRE:CheckBox;
		public var DISCRIMINATION:TextInputLabeled;
				
		public var libelle7 : FormItem;
		public var libelle8 : FormItem;		
		public var V7 : TextInputLabeled;
		public var V8 : TextInputLabeled;
		
		public var TRANCHES_SDA : DataGrid;	
					
		public var LIBELLE_TRANCHE : TextInputLabeled;
		public var NBRE_SDA : TextInputLabeled;
		public var NUM_DEPART : TextInputLabeled;
		public var NUM_FIN : TextInputLabeled;
		public var TYPE_TRANCHE : TextInputLabeled;		
				
		public var NBRE_CANAUX : TextInputLabeled;
		public var CANAUX_MIXTE_CAN_DEB:TextInputLabeled;
		public var CANAUX_MIXTE_CAN_FIN:TextInputLabeled;
		public var CANAUX_ENTREE_CAN_DEB:TextInputLabeled;
		public var CANAUX_ENTREE_CAN_FIN:TextInputLabeled;
		public var CANAUX_SORTIE_CAN_DEB:TextInputLabeled;
		public var CANAUX_SORTIE_CAN_FIN:TextInputLabeled;
								
		public var btUpdate : Button;
		public var btUpdateTranche : Button;
		public var btAjouterSDA : Button;
		public var btSupprimerSDA : Button;
		
		private var addTrancheWindow : AddTrancheView;
		//================================================================================
				
		
		public function OngletCablageT2Impl()
		{
			super();
		}
		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
			btAjouterSDA.addEventListener(MouseEvent.CLICK,btAjouterSDAClickHandler);
			btSupprimerSDA.addEventListener(MouseEvent.CLICK,btSupprimerSDAClickHandler);
			// test ------------ > 
			TRANCHES_SDA.dataProvider = new ArrayCollection();
			// fin test
			//btUpdateTranche.addEventListener(MouseEvent.CLICK,btUpdateTrancheClickHandler);
		}
		
		
				
		//======================== HANDLERS ==========================================
		protected function btUpdateClickHandler(me : MouseEvent):void{
			
		}
		protected function btAjouterSDAClickHandler(me : MouseEvent):void{
			if (TRANCHES_SDA.selectedItem != null){ 
				afficherLaFenetreDEditionDeLaTranche(TRANCHES_SDA.selectedItem as TrancheSDAVo);
			}else{
				afficherLaFenetreDEditionDeLaTranche(new TrancheSDAVo());
			}
		}
		protected function btSupprimerSDAClickHandler(me : MouseEvent):void{
			
		}
		protected function btUpdateTrancheClickHandler(me : MouseEvent):void{
			
		}
		protected function addTrancheWindowValiderClickedHandler(ev : Event):void{
			if (addTrancheWindow != null && addTrancheWindow.trancheSDA != null){
				// lancer la procédure
				//suver la tranche dans la base	
				//
				//test
				// Ajoutons la tranche o tableau
				if (addTrancheWindow.trancheSDA.IDTRANCHE_SDA > 0){
					(TRANCHES_SDA.dataProvider as ArrayCollection).addItem(addTrancheWindow.trancheSDA);		
				}else{
					(TRANCHES_SDA.dataProvider as ArrayCollection).itemUpdated(addTrancheWindow.trancheSDA);
				}
				
				PopUpManager.removePopUp(addTrancheWindow);
			}
		}
		protected function addTrancheWindowAnnulerClickedHandler(ev : Event):void{
			
		}
		//======================== FIN HANDLERS ======================================
		
		//==================== PRIVATE ===============================================
		private function afficherLaFenetreDEditionDeLaTranche(tranche : TrancheSDAVo):void{
			if (addTrancheWindow == null) addTrancheWindow = new AddTrancheView();
			addTrancheWindow.trancheSDA = tranche; 
			addTrancheWindow.modeEcriture = modeEcriture;
			
			addTrancheWindow.addEventListener(AddTrancheImpl.VALIDER_CLICKED,addTrancheWindowValiderClickedHandler);
			addTrancheWindow.addEventListener(AddTrancheImpl.ANNULER_CLICKED,addTrancheWindowAnnulerClickedHandler);
			PopUpManager.addPopUp(addTrancheWindow,DisplayObject(parentApplication),true);
		}
		//====================FIN PRIVATE ============================================
	}
}