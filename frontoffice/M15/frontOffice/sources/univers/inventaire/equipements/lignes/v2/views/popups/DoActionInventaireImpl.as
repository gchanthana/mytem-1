package univers.inventaire.equipements.lignes.v2.views.popups
{
	import mx.controls.Label;
	
	import univers.inventaire.equipements.lignes.v2.vo.Action;
	
	[Bindable]
	public class DoActionInventaireImpl extends DoActionRaccoImpl
	{	
		 
		 
		public var lblAction : Label;
				
		private var _action : Action;
		public function get action():Action{
			return _action;
		} 
		public function set action(act : Action):void{
			_action = act;
		}
		
		
		
		
						
		public function DoActionInventaireImpl()
		{			
			super();		
		}
		
		override protected function commitProperties():void{			
			super.commitProperties();
		}
		
	}
}