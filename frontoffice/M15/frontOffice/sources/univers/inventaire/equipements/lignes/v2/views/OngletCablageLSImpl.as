package univers.inventaire.equipements.lignes.v2.views
{
	import mx.containers.VBox;
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import flash.display.DisplayObject;
	import composants.util.TextInputLabeled;
	import mx.controls.Button;
	import flash.events.MouseEvent;
	import mx.containers.FormItem;
	
	[Bindable]
	public class OngletCablageLSImpl extends BaseView implements IOngletCablage
	{
		public var IDLL : TextInputLabeled;
		public var LS_LEGA_TETE : TextInputLabeled;
		public var LS_LEGA_ELEM_ACTIF : TextInputLabeled;
		public var LS_LEGA_NOMSITE : TextInputLabeled;
		public var LS_LEGA_ADRESSE1 : TextInputLabeled;
		public var LS_LEGA_ADRESSE2 : TextInputLabeled;
		public var LS_LEGA_ZIPCODE : TextInputLabeled;
		public var LS_LEGA_COMMUNE : TextInputLabeled;
		public var LS_LEGA_ELEM_ACTIF_AMORCE_E : TextInputLabeled;
		public var LS_LEGA_ELEM_ACTIF_AMORCE_R : TextInputLabeled;
		public var LS_LEGA_TETE_AMORCE_E : TextInputLabeled;
		public var LS_LEGA_TETE_AMORCE_R : TextInputLabeled;
		public var LS_NATURE : TextInputLabeled;
		public var LS_DEBIT2 : TextInputLabeled;
		public var LS_DISTANCE2 : TextInputLabeled;
		public var LS_LEGB_TETE : TextInputLabeled;
		public var LS_LEGB_ELEM_ACTIF : TextInputLabeled;
		public var LS_LEGB_NOMSITE : TextInputLabeled;
		public var LS_LEGB_ADRESSE1 : TextInputLabeled;
		public var LS_LEGB_ADRESSE2 : TextInputLabeled;
		public var LS_LEGB_ZIPCODE : TextInputLabeled;
		public var LS_LEGB_COMMUNE : TextInputLabeled;
		public var LS_LEGB_ELEM_ACTIF_AMORCE_E : TextInputLabeled;
		public var LS_LEGB_ELEM_ACTIF_AMORCE_R : TextInputLabeled;
		public var LS_LEGB_TETE_AMORCE_E : TextInputLabeled;
		public var LS_LEGB_TETE_AMORCE_R : TextInputLabeled;
		
		public var c1 : FormItem;		
		public var c2 : FormItem;	
		public var V_CAB1 : TextInputLabeled;
		public var V_CAB2 : TextInputLabeled;
		public var btChampPerso1 : Button;
		public var btChampPerso2 : Button;
		public var btUpdate : Button;
		
		public function OngletCablageLSImpl()
		{
			super();
		}
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
		}
				
		//======================== HANDLERS ==========================================
		protected function btUpdateClickHandler(me : MouseEvent):void{
			
		}
		//======================== FIN HANDLERS ======================================
		
		
		
		
		
	}
}