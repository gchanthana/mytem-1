package univers.inventaire.equipements.lignes.v2.views.popups
{
	import composants.util.TextInputLabeled;
	import composants.util.CvDateChooser;
	
	import mx.controls.TextArea;
	
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionEtatRaccordement;
	
	public class DoActionRaccoImpl extends PopUpImpl
	{
		public var dcDateRacco : CvDateChooser;
		public var txtReference :TextInputLabeled;
		public var txtCommentaires : TextArea;
		
		private var _app:GestionEtatRaccordement;
		public function get app():GestionEtatRaccordement{
			return _app;
		} 
		public function set app(appli :GestionEtatRaccordement):void{
			_app = appli;
		} 
		
		public function DoActionRaccoImpl()
		{
			super();
		}
		
	}
}