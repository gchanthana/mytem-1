package univers.inventaire.equipements.lignes.fiche.ll.menu {
	import flash.events.Event;
	import mx.events.FlexEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.FaultEvent;
	import mx.collections.ArrayCollection;
	import flash.events.MouseEvent;
	
	
	public class Localisation extends LocalisationIHM {
		private var sousteteObject:Object;
		private var _data:Object;
		
		public function Localisation(sousteteObject:Object) {
			this.sousteteObject = sousteteObject
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:Event):void {
			adresse1TextE.addEventListener(Event.CHANGE, txtChanged);
			zipcodeTextE.addEventListener(Event.CHANGE, txtChanged);
			adresse1TextO.addEventListener(Event.CHANGE, txtChanged);
			zipcodeTextO.addEventListener(Event.CHANGE, txtChanged);
			ficheSiteUpdateButton.addEventListener(MouseEvent.CLICK, btUpdateClicked);
			getFicheSite();
		}
		
		protected function txtChanged(event:Event):void
		{
			ficheSiteUpdateButton.enabled = true;
		}
		
		protected function btUpdateClicked(event:MouseEvent):void
		{
			var sitePhysique:String = _data.IDSITE_PHYSIQUE;
			var adresse_o:String = adresse1TextO.text;
			var adresse_e:String = adresse1TextE.text;
			var cp_o:String = zipcodeTextO.text;
			var cp_e:String = zipcodeTextE.text;
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.equipement",
																				"updateDataLL",
																				updateDataLLResult, processError);
			RemoteObjectUtil.callService(op, sitePhysique, adresse_o, cp_o, adresse_e, cp_e);
		}
		
		private function updateDataLLResult(event:ResultEvent):void
		{
			ficheSiteUpdateButton.enabled = false;
		}
		
		private function getFicheSite():void
		{
			var idsoutete:String = sousteteObject.IDSOUS_TETE;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.equipement",
																					"getLigneInfoLL",
																					getLigneInfoLLResult, processError);
			RemoteObjectUtil.callService(op, idsoutete);
		}
		
		private function getLigneInfoLLResult(event:ResultEvent):void
		{
			try
			{
				_data = ArrayCollection(event.result)[0];
				adresse1TextO.text = _data.ADRESSE_O;
				adresse1TextE.text = _data.ADRESSE_E;
				zipcodeTextO.text = _data.CP_O;
				zipcodeTextE.text = _data.CP_E;
			}
			catch (error:Error) {}
			
		}
		
		private function processError(error:FaultEvent):void
		{
			
		}
	}
}
