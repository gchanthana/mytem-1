package univers.inventaire.equipements.lignes.v2.views
{
	import mx.containers.VBox;
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import mx.controls.ComboBox;
	import composants.util.TextInputLabeled;
	import mx.containers.FormItem;
	import mx.controls.Button;
	import flash.events.MouseEvent;
	import mx.events.FlexEvent;

	[Bindable]
	public class OngletAffectationImpl extends BaseView 
	{
		
		public static const ANA : String = "ANA";
		public static const MOBILE : String = "MOB";
		public static const NUM : String = "NUM";
		
		
		
		public function OngletAffectationImpl() 
		{	
			super();
		}
		
		private var onglet : BaseView;
		
		
		public function configurerOnglet():void{
			if (onglet != null){
				removeChild(onglet.displayObject);
				onglet = null;
				
			}
			onglet = createFicheCablage(gestionTechnique.ligne.TYPE_FICHELIGNE);
			onglet.gestionTechnique = gestionTechnique;
			onglet.modeEcriture = modeEcriture;
			addChild(onglet.displayObject);
		}
		
		override protected function commitProperties():void{
			super.commitProperties();
		}	
		
		//================ HANDLERS =====================================
		
		//===============================================================
		
		
			
		
		private function createFicheCablage(type : String):BaseView{
			switch(type){
				case MOBILE : return new OngletAffectationMobileView();break;				
				default : return new OngletNonDisponibleView();break;
			}	
		}
		
	}
}