package univers.inventaire.equipements.lignes.v2.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	import composants.util.TextInputLabeled;
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class OngletLAVo implements ItoXMLParams
	{
		protected var _xmlParams : XML;
		
		
		public var IDSOUS_TETE:Number;
		
		public var NOM_SITE_INSTALL:String;
		public var ADRESSE1_INSTALL:String;
		public var ADRESSE2_INSTALL:String;
		public var ZIPCODE_INSTALL:String;
		public var VILLE_INSTALL:String;
		
		
		public var BAT:String;
		public var ETAGE:String;
		public var SALLE:String;
		public var NOEUD:String;
		
		
		public var REGLETTE1:String;
		public var PAIRE1:String;
		public var REGLETTE2:String;
		public var PAIRE2:String;
	
	
		public var AMORCE:String;
		public var NOMBRE_POSTES:String;
			
			
		public var CARTE_PABX:String;
		public var SECRET_IDENTIFIANT:Number;
		public var PUBLICATION_ANNUAIRE:Number;
		public var DISCRIMINATION:String;
		
		public var TRANCHES_SDA:ArrayCollection;
		
		
		public var COMMENTAIRE_CABLAGE:String;
		
		
		public var V7 : String;		
		public var V8 : String;
		
		
		public function OngletLAVo(){
			
		}
		
		public function toXMLParams(registerInLog : int=0):XML{
			_xmlParams = XmlParamUtil.createXmlParamObject(registerInLog);
			
			XmlParamUtil.addParam(_xmlParam,"IDSOUS_TETE","l'identifiant de la ligne",IDSOUS_TETE);
			XmlParamUtil.addParam(_xmlParam,"NOM_SITE_INSTALL","le nom du site d'installation",NOM_SITE_INSTALL);
			XmlParamUtil.addParam(_xmlParam,"ADRESSE1_INSTALL","l'addresse du site d'installation",ADRESSE1_INSTALL);
			XmlParamUtil.addParam(_xmlParam,"ADRESSE2_INSTALL","complément d'addresse du site d'installation",ADRESSE2_INSTALL);
			XmlParamUtil.addParam(_xmlParam,"ZIPCODE_INSTALL","le code postal",ZIPCODE_INSTALL);
			XmlParamUtil.addParam(_xmlParam,"VILLE_INSTALL","la ville d'installation",VILLE_INSTALL);
			
			XmlParamUtil.addParam(_xmlParam,"BAT","localisation lbâtiment",BAT);
			XmlParamUtil.addParam(_xmlParam,"ETAGE","localisation  étage",ETAGE);
			XmlParamUtil.addParam(_xmlParam,"SALLE","localisation salle",SALLE);
			XmlParamUtil.addParam(_xmlParam,"NOEUD","localisation noeud",NOEUD);
			
			XmlParamUtil.addParam(_xmlParam,"REGLETTE1","reglette1",REGLETTE1);
			XmlParamUtil.addParam(_xmlParam,"PAIRE1","paire1",PAIRE1);
			XmlParamUtil.addParam(_xmlParam,"REGLETTE2","reglette2",REGLETTE2);
			XmlParamUtil.addParam(_xmlParam,"PAIRE2","paire2",PAIRE2);
			
			XmlParamUtil.addParam(_xmlParam,"AMORCE","Amorce",AMORCE);
			XmlParamUtil.addParam(_xmlParam,"NOMBRE_POSTES","Nombre de postes",NOMBRE_POSTES);
						
			XmlParamUtil.addParam(_xmlParam,"CARTE_PABX","la carte PABX",CARTE_PABX);
			XmlParamUtil.addParam(_xmlParam,"SECRET_IDENTIFIANT","secret identifiant ",SECRET_IDENTIFIANT);
			XmlParamUtil.addParam(_xmlParam,"PUBLICATION_ANNUAIRE","publication annuaire",PUBLICATION_ANNUAIRE);
			XmlParamUtil.addParam(_xmlParam,"DISCRIMINATION","Discrimination",DISCRIMINATION);			
			
			XmlParamUtil.addParam(_xmlParam,"COMMENTAIRE_CABLAGE","commentaires",COMMENTAIRE_CABLAGE);
			XmlParamUtil.addParam(_xmlParam,"V7","Valeurs du champ perso 7",V7);
			XmlParamUtil.addParam(_xmlParam,"V8","Valeurs du champ perso 8",V8);
			
			return _xmlParams;
		}
		
		
	}
}