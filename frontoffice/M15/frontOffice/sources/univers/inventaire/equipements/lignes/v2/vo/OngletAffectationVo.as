package univers.inventaire.equipements.lignes.v2.vo
{
	import mx.collections.ArrayCollection;
		
	[Bindable]
	public class OngletAffectationVo
	{
		public var SI_OPERATEUR:Boolean=true;
		
		public var COMPTE:String = "";		
		public var NOM:String="";
		public var PRENOM:String="";
		public var COLLABORATEURID:Number=0;
		public var MAJ_AUTO_COLLABORATEUR:Number=0;
		
		public var EMPLOYEID:Number=0;
		public var EMPLOYE:String="";
		
		public var V5:String="";
		public var V6:String="";
		
		public var listeNoeudsDAffectations : ArrayCollection;
		
		public function OngletAffectationVo()
		{	
		}
	}
}