package univers.inventaire.equipements.lignes.v2.views
{
	import composants.util.TextInputLabeled;
	import mx.controls.Button;
	import flash.events.MouseEvent;
	import mx.managers.PopUpManager;
	import mx.events.FlexEvent;
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import univers.inventaire.equipements.lignes.v2.vo.TrancheSDAVo;
	import mx.controls.Alert;
	import flash.events.Event;
	
	
	
	
	[Event(name='annulerClicked')]
	[Event(name='validerClicked')]
	
	
	
	
	[Bindable]
	public class AddTrancheImpl extends BaseView
	{
		public static const ANNULER_CLICKED:String = "annulerClicked";
		public static const VALIDER_CLICKED:String = "validerClicked";
		
		
		public var LIBELLE_TRANCHE : TextInputLabeled;
		public var NBRE_SDA : TextInputLabeled;
		public var NUM_DEPART : TextInputLabeled;
		public var NUM_FIN : TextInputLabeled;
		public var TYPE_TRANCHE : TextInputLabeled;		
		
		public var btValider : Button;
		public var btAnnuler : Button;
		
		//--- POPERTIES -------------------------------------------------------------------
		private var _trancheSDA : TrancheSDAVo;
		public function get trancheSDA():TrancheSDAVo{
			return _trancheSDA;
		} 
		public function set trancheSDA(tranche : TrancheSDAVo):void{
			_trancheSDA = tranche;
		}
		//--
		
		//--- FIN POPERTIES ---------------------------------------------------------------
		
		
		
		//--- HANDLERS --------------------------------------------------------------------
		protected function creationCompleteHandler(fe : FlexEvent):void{
			PopUpManager.centerPopUp(this);
		}
		
		protected function btAnnulerClickHandler(me : MouseEvent):void{
			dispatchEvent(new Event(ANNULER_CLICKED));
			PopUpManager.removePopUp(this);
		}
		
		protected function btValiderClickHandler(me : MouseEvent):void{
			if (trancheSDA != null){				
				enregistrer();
			}	
		}
		//--- FIN HANDLERS --------------------------------------------------------------------
		
		
		//--- PUBLIC --------------------------------------------------------------------------
		
		//--- FIN PUBLIC ----------------------------------------------------------------------
		
		//--- PROTECTED --------------------------------------------------------------------------
		override protected function commitProperties():void{
			super.commitProperties();
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			btAnnuler.addEventListener(MouseEvent.CLICK,btAnnulerClickHandler);
			btValider.addEventListener(MouseEvent.CLICK,btValiderClickHandler);
		}
		
		override protected function enregistrer():void{
			
			var boolOk : Boolean = true;
			if (LIBELLE_TRANCHE.text.length == 0){
				boolOk = false;
			}
			
			if (boolOk){

				trancheSDA.IDSOUS_TETE = 0;
				trancheSDA.IDTRANCHE_SDA = 22;
				trancheSDA.NBRE_SDA = NBRE_SDA.text;
				trancheSDA.NUM_DEPART = NUM_DEPART.text;
				trancheSDA.NUM_FIN = NUM_FIN.text;
				trancheSDA.TRANCHE_SDA = LIBELLE_TRANCHE.text;
				trancheSDA.TYPE_TRANCHE = TYPE_TRANCHE.text;
				
				dispatchEvent(new Event(VALIDER_CLICKED));
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Vous devez saisir un libellé pour cette tranche","Erreur",Alert.OK);
			}
		}
		//--- FIN PROTECTED ----------------------------------------------------------------------
		
		
		//--- PRIVATE --------------------------------------------------------------------------
		
		//--- FIN PRIVATE ----------------------------------------------------------------------
		
		
	}//--FIN
}