package univers.inventaire.equipements.lignes.fiche {
	import flash.errors.IllegalOperationError;
	import univers.inventaire.equipements.lignes.fiche.aut.FicheLigneAUT;
	import univers.inventaire.equipements.lignes.fiche.ll.FicheLigneLL;
	import univers.inventaire.equipements.lignes.fiche.mob.FicheLigneMOB;
	import univers.inventaire.equipements.lignes.fiche.num.FicheLigneNUM;
	import univers.inventaire.equipements.lignes.fiche.ser.FicheLigneSER;
	import univers.inventaire.equipements.lignes.fiche.ana.FicheLigneANA;
	
	public class FicheLigneFactory {
		public static function getFactory():FicheLigneFactory {
			return new FicheLigneFactory();
		}
		
		public function createFicheLigne(type:String,sousteteObject:Object):AbstractFicheLigne {
			try {
				return this["createFicheLigne" + type](sousteteObject);
			} catch(e:Error) {
				throw new IllegalOperationError("Erreur :\nLa méthode createFicheLigne" +
															type + " n'est pas implémentée pour le type :\n" +
															type + "\ndans la Factory :\n" +
															"consoview.annuaire.site.fiche.FicheLigneFactory");
			}
			return null;
		}

		private function createFicheLigneANA(sousteteObject:Object):AbstractFicheLigne {
			return new FicheLigneANA(sousteteObject);
		}
		
		private function createFicheLigneAUT(sousteteObject:Object):AbstractFicheLigne {
			return new FicheLigneAUT(sousteteObject);
		}
		
		private function createFicheLigneLL(sousteteObject:Object):AbstractFicheLigne {
			return new FicheLigneLL(sousteteObject);
		}
		
		private function createFicheLigneMOB(sousteteObject:Object):AbstractFicheLigne {
			return new FicheLigneMOB(sousteteObject);
		}
		
		private function createFicheLigneNUM(sousteteObject:Object):AbstractFicheLigne {
			return new FicheLigneNUM(sousteteObject);
		}
		
		private function createFicheLigneSER(sousteteObject:Object):AbstractFicheLigne {
			return new FicheLigneSER(sousteteObject);
		}
	}
}
