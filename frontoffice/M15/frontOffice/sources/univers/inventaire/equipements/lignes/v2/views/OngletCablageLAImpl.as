package univers.inventaire.equipements.lignes.v2.views
{
	import mx.containers.VBox;
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import composants.util.TextInputLabeled;
	import mx.controls.TextArea;
	import mx.controls.ComboBox;
	import mx.controls.Button;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import mx.containers.FormItem;
	import mx.controls.CheckBox;
	
	[Bindable]
	public class OngletCablageLAImpl extends BaseView implements IOngletCablage
	{

		public var NOM_SITE_INSTALL:TextInputLabeled;
		public var ADRESSE1_INSTALL:TextInputLabeled;
		public var ADRESSE2_INSTALL:TextInputLabeled;
		public var ZIPCODE_INSTALL:TextInputLabeled;
		public var VILLE_INSTALL:TextInputLabeled;

		
		public var REGLETTE1:TextInputLabeled;
		public var PAIRE1:TextInputLabeled;
		public var REGLETTE2:TextInputLabeled;
		public var PAIRE2:TextInputLabeled;
		public var BAT:TextInputLabeled;
		public var ETAGE:TextInputLabeled;
		public var SALLE:TextInputLabeled;
		public var NOEUD:TextInputLabeled;
		public var AMORCE:TextInputLabeled;
		public var CARTE_PABX:TextInputLabeled;
		public var COMMENTAIRE_CABLAGE:TextArea;	
		
		public var NOMBRE_POSTES:TextInputLabeled;
		public var SECRET_IDENTIFIANT:CheckBox;
		public var PUBLICATION_ANNUAIRE:CheckBox;
		public var DISCRIMINATION:TextInputLabeled;
				
		public var libelle7 : FormItem;
		public var libelle8 : FormItem;		
		public var V7 : TextInputLabeled;
		public var V8 : TextInputLabeled;
		
		public var TRANCHES_SDA : ComboBox;
		public var LIBELLE_TRANCHE : TextInputLabeled;
		public var NBRE_SDA : TextInputLabeled;
		public var NUM_DEPART : TextInputLabeled;
		public var NUM_FIN : TextInputLabeled;
		public var TYPE_TRANCHE : TextInputLabeled;
		
		public var btUpdate : Button;
		public var btUpdateTranche : Button;
		public var btAjouterSDA : Button;
		public var btSupprimerSDA : Button;
		
		public function OngletCablageLAImpl()
		{	
			super();
		}
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
			btUpdateTranche.addEventListener(MouseEvent.CLICK,btUpdateTrancheClickHandler);
			btAjouterSDA.addEventListener(MouseEvent.CLICK,btAjouterSDAClickHandler);
			btSupprimerSDA.addEventListener(MouseEvent.CLICK,btSupprimerSDAClickHandler);
		}
		
				
		//======================== HANDLERS ==========================================
		protected function btUpdateClickHandler(me : MouseEvent):void{
			
		}
		protected function btUpdateTrancheClickHandler(me : MouseEvent):void{
			
		}
		protected function btAjouterSDAClickHandler(me : MouseEvent):void{
			
		}
		protected function btSupprimerSDAClickHandler(me : MouseEvent):void{
			
		}
		//======================== FIN HANDLERS ======================================
		
	}
}