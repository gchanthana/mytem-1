package univers.inventaire.equipements.lignes.recherche {
	import flash.events.Event;

	public class RechercheEvent extends Event {
		public static const GRIDLIGNES_CHANGE_EVENT:String = "onGridLignesChange";
		private var selectedItem:Object;
		
		public function RechercheEvent(selectedItem:Object) {
			super(GRIDLIGNES_CHANGE_EVENT);
			this.selectedItem = selectedItem;
		}
		
		public function getSelectedItem():Object {
			return this.selectedItem;
		}
	}
}
