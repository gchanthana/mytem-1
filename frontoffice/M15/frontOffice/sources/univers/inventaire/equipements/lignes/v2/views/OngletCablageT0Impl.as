package univers.inventaire.equipements.lignes.v2.views
{
	import mx.containers.VBox;
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import flash.display.DisplayObject;
	import mx.controls.CheckBox;
	import composants.util.TextInputLabeled;
	import mx.controls.Button;
	import flash.events.MouseEvent;
	import mx.controls.TextArea;
	import mx.containers.HBox;
	import mx.containers.FormItem;
	import mx.controls.ComboBox;
	
	[Bindable]
	public class OngletCablageT0Impl extends BaseView implements IOngletCablage
	{
		public var NOM_SITE_INSTALL:TextInputLabeled;
		public var ADRESSE1_INSTALL:TextInputLabeled;
		public var ADRESSE2_INSTALL:TextInputLabeled;
		public var ZIPCODE_INSTALL:TextInputLabeled;
		public var VILLE_INSTALL:TextInputLabeled;

		
		public var REGLETTE1:TextInputLabeled;
		public var PAIRE1:TextInputLabeled;
		public var REGLETTE2:TextInputLabeled;
		public var PAIRE2:TextInputLabeled;
		public var BAT:TextInputLabeled;
		public var ETAGE:TextInputLabeled;
		public var SALLE:TextInputLabeled;
		public var NOEUD:TextInputLabeled;
		public var AMORCE:TextInputLabeled;
		public var CARTE_PABX:TextInputLabeled;
		public var COMMENTAIRE_CABLAGE:TextArea;	
		
		public var NOMBRE_POSTES:TextInputLabeled;
		public var SECRET_IDENTIFIANT:CheckBox;
		public var PUBLICATION_ANNUAIRE:CheckBox;
		public var DISCRIMINATION:TextInputLabeled;
				
		public var libelle7 : FormItem;
		public var libelle8 : FormItem;		
		public var V7 : TextInputLabeled;
		public var V8 : TextInputLabeled;
		
		public var TRANCHES_SDA : ComboBox;
		public var LIBELLE_TRANCHE : TextInputLabeled;
		public var NBRE_SDA : TextInputLabeled;
		public var NUM_DEPART : TextInputLabeled;
		public var NUM_FIN : TextInputLabeled;
		public var TYPE_TRANCHE : TextInputLabeled;
		
		public var NBRE_CANAUX : TextInputLabeled;
				
		public var btUpdate : Button;
		public var btUpdateTranche : Button;		
		public var btAjouterSDA : Button;
		public var btSupprimerSDA : Button;
		
		public function OngletCablageT0Impl()
		{
			//TODO: implement function
			super();
		}
		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
		}
		
				
		//======================== HANDLERS ==========================================
		protected function btUpdateClickHandler(me : MouseEvent):void{
			
		}
		//======================== FIN HANDLERS ======================================
		
		
		
		
	}
}