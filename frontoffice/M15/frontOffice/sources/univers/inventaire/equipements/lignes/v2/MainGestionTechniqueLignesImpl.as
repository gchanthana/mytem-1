package univers.inventaire.equipements.lignes.v2
{
	import univers.UniversManager;
	import univers.inventaire.equipements.lignes.v2.util.viewsHelpers;
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import univers.inventaire.equipements.lignes.v2.views.GestionTechniqueView;
	import mx.containers.TabNavigator;
	import univers.inventaire.equipements.lignes.v2.views.OngletChampsPersoView;
	import mx.containers.ViewStack;
	
	public class MainGestionTechniqueLignesImpl extends Canvas
	{
		[Bindable]
		protected var gestionTechnique : GestionTechniqueLignesApp;			
		
		[Bindable]
		protected var mode : Boolean = false;
		
		public var cmpGestionLignes : GestionTechniqueView;
		public var cmpOngletChampsPersoView : OngletChampsPersoView;
		public var tbnMain : ViewStack;
	
		public function init():void{
			gestionTechnique = new GestionTechniqueLignesApp();
			mode = (UniversManager.getCurrentUniversMode() === UniversManager.WRITE_MODE);
			tbnMain.selectedChild = cmpGestionLignes;
			cmpGestionLignes.onPerimetreChange();
		}			
		
		public function onPerimetreChange():void{
			gestionTechnique = new GestionTechniqueLignesApp();				
			mode = (UniversManager.getCurrentUniversMode() === UniversManager.WRITE_MODE);
			tbnMain.selectedChild = cmpGestionLignes;
			cmpGestionLignes.onPerimetreChange();
		}
	}
}