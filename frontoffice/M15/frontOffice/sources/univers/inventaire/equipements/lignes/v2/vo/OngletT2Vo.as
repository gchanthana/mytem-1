package univers.inventaire.equipements.lignes.v2.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	[Bindable]
	public class OngletT2Vo extends OngletT0Vo implements ItoXMLParams
	{
		public function OngletT2Vo()
		{
			super();
		}
		
		public function toXMLParams(registerInLog:int=XmlParamUtil.NOT_REGISTER_PARAM_IN_LOG):XML
		{
			_xmlParams = super.toXMLParams();
			
			return _xmlParams;
		}
		
	}
}