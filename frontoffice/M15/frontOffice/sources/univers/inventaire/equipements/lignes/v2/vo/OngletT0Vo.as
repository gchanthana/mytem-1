package univers.inventaire.equipements.lignes.v2.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	[Bindable]
	public class OngletT0Vo extends OngletLAVo implements ItoXMLParams
	{		
		public var NBRE_CANAUX:String="";
		
		public function OngletT0Vo()
		{
			super();
		}
		
		public function toXMLParams(registerInLog:int=XmlParamUtil.NOT_REGISTER_PARAM_IN_LOG):XML
		{
			_xmlParams = super.toXMLParams();
			XmlParamUtil.addParam(_xmlParams,"NBRE_CANAUX","Le nombre de canaux",NBRE_CANAUX);		
			return _xmlParams;
		}
		
	}
}