package univers.inventaire.equipements.lignes.v2.vo
{
	[RemoteClass(alias="fr.consotel.consoview.inventaire.equipement.lignes.OngletEtatVo")]
	
	[Bindable]
	public class OngletEtatVo
	{
		public var IDSOUS_TETE:Number = 0;
		public var ETAT:String = "active";
		public var DATE_COMMANDE:Date = null;
		public var REF_COMMANDE:String = "";
		public var BOOL_TITULAIRE:Number = 0;
		public var BOOL_PAYEUR:Number = 0;
		public var COMMENTAIRE_ETAT:String = "";
		public var DATE_MISE_EN_SERV:Date = null;
		public var REF_MISE_EN_SERV:String = "";
		public var GEST_MISE_EN_SERV:String = "";
		public var DATE_PRESELECTION:Date = null; 
		public var REF_PRESELECTION:String = "";
		public var GEST_PRESELECTION:String = "";
		public var DATE_SUSPENSION:Date = null; 
		public var REF_SUSPENSION:String = "";
		public var GEST_SUSPENSION:String = "";
		public var DATE_RESILIATION:Date = null;
		public var REF_RESILIATION:String = "";
		public var GEST_RESILIATION:String = "";
		public var V3:String = "";
		public var V4:String = "";
	}
}