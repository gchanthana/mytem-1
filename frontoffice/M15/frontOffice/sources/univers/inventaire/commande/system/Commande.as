package univers.inventaire.commande.system
{
	import mx.formatters.DateFormatter;
	
	[RemoteClass(alias="fr.consotel.consoview.inventaire.commande.Commande")]

	[Bindable]
	public class Commande
	{

		public var IDCOMMANDE:Number = 0;
		public var IDCONTACT:Number = 0;
		public var IDREVENDEUR:Number = 0;
		public var IDOPERATEUR:Number = 0;
		public var IDTYPE_COMMANDE:Number = 0;
		public var IDSOCIETE:Number = 0;
		public var IDCOMPTE_FACTURATION:Number = 0;
		public var IDSOUS_COMPTE:Number = 0;
		public var IDRACINE:Number = 0;
		public var IDLAST_ETAT:Number = 0;
		public var IDLAST_ACTION:Number = 0;
		public var IDSITELIVRAISON:Number = 0;
		public var IDTRANSPORTEUR:Number = 0;
		public var IDPOOL_GESTIONNAIRE:Number = 0;
		public var IDGESTIONNAIRE:Number = 0;
		public var NUMERO_COMMANDE:String = "";
		public var LIBELLE_COMMANDE:String = "";
		public var LIBELLE_POOL:String = "";
		public var REF_CLIENT:String = "";
		public var REF_REVENDEUR:String = "";
		public var LIBELLE_REVENDEUR:String = "";
		public var LIBELLE_OPERATEUR:String = "";
		public var COMMENTAIRES:String = "";
		public var LIBELLE_COMPTE:String = "";
		public var LIBELLE_SOUSCOMPTE:String = "";
		public var LIBELLE_TRANSPORTEUR:String = "";
		public var LIBELLE_SITELIVRAISON:String = "";
		public var LIBELLE_LASTETAT:String = "";
		public var LIBELLE_LASTACTION:String = "";
		public var TYPE_OPERATION:String = "";
		public var PATRONYME_CONTACT:String = "";
		public var CREEE_PAR:String = "";
		public var MODIFIEE_PAR:String = "";
		public var BOOL_ENVOYER_VIAMAIL:Number = 0;
		public var CREEE_LE:Date = null;
		public var MODIFIEE_LE:Date = null;
		public var ENVOYER_LE:Date = null;
		public var LIVREE_LE:Date = null;
		public var LIVRAISON_PREVUE_LE:Date = null;
		public var DATE_COMMANDE:Date = null;
		public var EXPEDIE_LE:Date=null;
		public var BOOL_DEVIS:Number = 0;
		public var NUMERO_TRACKING:String = "";
		public var MONTANT:Number = 0;
		public var SEGMENT_FIXE:Number = 0;
		public var SEGMENT_MOBILE:Number = 0;
		public var IDGESTIONNAIRE_MODIF:Number = 0;
		public var IDGESTIONNAIRE_CREATE:Number = 0;
		public var IDGROUPE_REPERE:Number = 0;
		public var ENCOURS:Number = 1;
		public var EMAIL_CONTACT:String="";


		public function Commande()
		{
			genererNumeroDeCommande()
		}
		
		 protected function genererNumeroDeCommande():void
	    {
	    	var df:DateFormatter = new DateFormatter();
	    	df.formatString = "DDMMYY"
	    	var debut : String = CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE.substr(0,3);
	    	var milieu : String = df.format(new Date());
	    	var fin : String = int(Math.random() * 1000).toString(10);	    	
	    	var defautNumeroCommande:String = debut + milieu + fin;
	    	
	    	LIBELLE_COMMANDE = defautNumeroCommande;
	    	REF_CLIENT = LIBELLE_COMMANDE;
	    	REF_REVENDEUR = LIBELLE_COMMANDE;
	    	//NUMERO_COMMANDE = LIBELLE_COMMANDE;
	    }
	}
}