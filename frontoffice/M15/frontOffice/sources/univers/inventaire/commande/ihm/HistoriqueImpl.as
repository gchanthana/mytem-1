package univers.inventaire.commande.ihm
{
	import flash.events.MouseEvent;
	
	import mx.collections.ICollectionView;
	import mx.controls.PopUpMenuButton;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.ListEvent;
	import mx.events.MenuEvent;
	import mx.formatters.DateFormatter;
	
	import univers.inventaire.commande.system.Action;
	import univers.inventaire.commande.system.ActionEvent;
	
	
	
	
	[Event(name="actionClicked",type="univers.inventaire.commande.system.ActionEvent")]
	
	
	[Bindable]
	public class HistoriqueImpl extends AbstractBaseViewImpl
	{
			
		public var btActionsItem:PopUpMenuButton;
		
		public var dataProviderHistorique:ICollectionView;
		public var dataProviderActionsPossibles:ICollectionView;		
		
		
		public var selectedAction:Action;
		public var actionEnabled:Boolean = true;
		
	
		
		public function HistoriqueImpl()
		{
				
		}
		
		
		 
		
		protected function btActionChageHandler(event:ListEvent):void
		{
			 
		}
		
		protected function btActionItemMenuClickHandler(event:MenuEvent):void
		{
			if(event.item != null)
			{	
				selectedAction = event.item as Action;
				
			}
		}
		
		protected function btActionClickHandler(event:MouseEvent):void
		{
			if(selectedAction != null)
			{
				var actionEvent:ActionEvent = new ActionEvent(ActionEvent.ACTION_CLICKED);
				actionEvent.action = selectedAction;
								
				dispatchEvent(actionEvent);	
			}
		}
		
	}
}