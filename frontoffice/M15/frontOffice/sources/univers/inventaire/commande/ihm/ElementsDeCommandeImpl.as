package univers.inventaire.commande.ihm
{
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	import composants.util.article.Article;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.ListEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.commande.system.AbstractGestionPanier;
	import univers.inventaire.commande.system.ArticleEvent;
	import univers.inventaire.commande.system.Commande;
	
	[Event(name="showArticle",type="univers.inventaire.commande.system.ArticleEvent")]
	
	[Event(name="imgEmployeClicked",type="flash.events.Event")]
	
	[Bindable]
	public class ElementsDeCommandeImpl extends VBox
	{
		
		public static const IMG_EMPLOYE_CLICKED:String = "imgEmployeClicked";
		
		public var dgArticles:DataGrid;
		public var txtTotal:TextInput;
		public var btRetirerArticle:Button;
		public var imgEditer:Image;
				
		private var _removeItemEnabled:Boolean = false;
		private var _collaborateursEditable:Boolean = true;
		
		
		public function get collaborateursEditable():Boolean
		{
			return _collaborateursEditable
		}
		public function set collaborateursEditable(value:Boolean):void
		{
			_collaborateursEditable = value
		} 
		
		public var messageEdition:String="Double clickez sur l'article pour le modifier";
		
		/**
		 * Permet d'afficher le bouton pour retirer un article
		 * @param value
		 * */
		public function set removeItemEnabled(value:Boolean):void
		{
			_removeItemEnabled = value;
		}
		public function get removeItemEnabled():Boolean
		{
			return _removeItemEnabled;
		}
		
		private var _itemEditable : Boolean = false;
		/**
		 * Permet d'afficher le bouton pour editer un article
		 * @param value
		 * */
		public function set itemEditable(value:Boolean):void{
			_itemEditable = value;
		}
		public function get itemEditable():Boolean
		{
			return _itemEditable;
		}
		
		private var _selectedArticle:Article;
		public function set selectedArticle(value:Article):void
		{
			_selectedArticle = value;
		}
		public function get selectedArticle():Article
		{
			return _selectedArticle;
		}
		
		
		protected var _gestionArticles:AbstractGestionPanier= new AbstractGestionPanier();
		public function get gestionArticles():AbstractGestionPanier
		{
			if (_gestionArticles == null)
			{
				_gestionArticles = new AbstractGestionPanier()
			} 
			
			return _gestionArticles
		}
		public function set gestionArticles(value:AbstractGestionPanier):void
		{
			_gestionArticles = value
		}
		
		
		
		protected var _commande : Commande;
		public function set commande (value:Commande) :void
		{
			_commande = value;
		} 
		
		public function ElementsDeCommandeImpl()
		{	
			super();
			commande = new Commande();
						
		}
		
		override protected function commitProperties():void
		{
			btRetirerArticle.enabled = removeItemEnabled;
		}
		
		
		//------ protected----------------------
		//exception pour itemrenderer
		public function formatEuro(value:Number):String
		{
			if (isNaN(value))
			{
				return "0.00"
			}
			else
			{
				return ConsoviewFormatter.formatEuroCurrency(value,2)	
			}
		}
		
		protected function formaterColonneDates(item : Object, column : DataGridColumn):String
		{
			if (item != null && item[column.dataField] != null)
			{
				return DateFunction.formatDateAsString(item[column.dataField]);				
			}
			else
			{
				return "";	
			}
			
		}
		
		protected function formateColonnneEuros(item : Object, column : DataGridColumn):String
		{			
			if (item != null) return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2)	
			else return ""
		}
		
		
		protected function formateColonneNumber(item : Object, column : DataGridColumn):String
		{
			if (item != null) return ConsoviewFormatter.formatNumber(Number(item[column.dataField]),2)
			else return ""
		}
		
		protected function formateColonneSousTete(item : Object, column : DataGridColumn = null):String
		{			
			if (item != null && column != null) return ConsoviewFormatter.formatPhoneNumber(item[column.dataField])			
			else return ""
		}
		
		protected function formateColonneBoolean(item : Object, column : DataGridColumn):String
		{
			if (item != null) return (Number(item[column.dataField]) == 1)?"OUI":"NON"
			else return ""
		}
		
		protected function coparePrixSortFunction(itemA : Object,itemB : Object):Number
		{
			return ObjectUtil.numericCompare(itemA.prix,itemB.prix);
		}
		
		//----- Handlers -------------------------
		protected function dgArticlesChangeHandler(event : ListEvent):void
		{
			if(dgArticles.selectedItem != null)
			{
				gestionArticles.selectedItem = dgArticles.selectedItem;
				selectedArticle = new Article(XML(dgArticles.selectedItem));
			}
			else
			{
				selectedArticle = null;
				gestionArticles.selectedItem = null;
			}
		}
		
		protected function btRetirerArticleClickHandler(event : MouseEvent):void
		{
			if (gestionArticles.selectedItem != null && removeItemEnabled)
			{
				var article : Article = new Article(XML(dgArticles.selectedItem));
				gestionArticles.removeItem(article);
			}
		}
		
		protected function dgArticlesDoubleClickHandler(event:MouseEvent):void
		{	
			
			
			if (dgArticles.selectedItem != null && itemEditable)
			{
				var  article : Article = new Article(dgArticles.selectedItem as XML);
				dispatchEvent(new ArticleEvent(article,ArticleEvent.SHOW_ARTICLE));	
			}
			
		}
		
		protected function dgArticleUpdateCompleteHandler(event:Event):void
		{	
			if(dgArticles.initialized)
			{
				dgArticles.validateNow()
			}
		}
		
		
		/**
		 * protected
		 * */
		public function imgEmployeClickHandler(event:MouseEvent):void{
			if (gestionArticles.selectedItem != null)
			{
			
				dispatchEvent(new Event(IMG_EMPLOYE_CLICKED));
			}
		}
		
	}
}