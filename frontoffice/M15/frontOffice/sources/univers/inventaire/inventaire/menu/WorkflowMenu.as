package univers.inventaire.inventaire.menu
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	
	/**
	 * Classe Onglet Journaux
	 * 
	 * */
	public class WorkflowMenu extends WorkflowMenuIHM
	{
		private var myDp:ArrayCollection=new ArrayCollection([
						{label:"Liste des commandes",template:"global",libelle:"Liste_Des_Commandes",type_commande:"Commande"},
						{label:"Retards de livraison des commandes",template:"RetardLivraison",libelle:"Retards_De_livraison",type_commande:"Commande"},
						{label:"Délais de traitement interne des commandes",template:"TempsTraitementInterne",libelle:"Delais_Traiement_Interne",type_commande:"Commande"},
						{label:"Délais de traitement opérateur des commandes",template:"TempsTraitementOperateur",libelle:"Delais_Traitement_Operateur",type_commande:"Commande"},
						{label:"Liste des résiliations",template:"global",libelle:"Liste_Des_Resiliations",type_commande:"Résiliation"},
						{label:"Délais de traitement interne des résiliations",template:"TempsTraitementInterne",libelle:"Delais_Traiement_Interne",type_commande:"Résiliation"},
						{label:"Délais de traitement opérateur des résiliations",template:"TempsTraitementOperateur",libelle:"Delais_Traitement_Operateur",type_commande:"Résiliation"}
					]);
		
		/**
		 * Constructeur 
		 * */
		public function WorkflowMenu()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		
		//Initialisation de l'IHM 
		//Affectation des ecouteurs d'évènements
		private function initIHM(event:Event):void {
			btWfGenerate.addEventListener(MouseEvent.CLICK,exporter);
			myWfGrid.dataProvider=myDp;
		}
		
        //Export ----------------------------------------------------------------------------------
        
        //Handler du clique sur le bouton exporter
        private function exporter(me : MouseEvent):void{
        	if (myWfGrid.selectedIndex == -1/*  && treeOrga.nodeInfo == null */){
        		lblErrorWf.text = "Sélectionnez un rapport";
        	}else{
        		displayExport();
        	}        	       
        }
        
         //Exporte le journal affiché suivant le format passé en paramètre        
         //param in format le format sous lequel on souhaite exporter
        private function displayExport():void {
			var url:String = cv.NonSecureUrlBackoffice+"/fr/consotel/consoview/inventaire/rapport/workflow/detailCommande.cfm";
            var variables:URLVariables = new URLVariables();
            variables.template = myWfGrid.selectedItem.template;
            variables.libelle = myWfGrid.selectedItem.libelle;
            variables.type_commande = myWfGrid.selectedItem.type_commande ; 
			variables.code_etat = cmbTypeEtat.selectedItem.value;
			variables.en_cours = cmbOpen.selectedItem.value;
			variables.detail_produit = Number(cbDetail.selected).toString();
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method=URLRequestMethod.POST;
            navigateToURL(request,"_blank");
        } 			    
        
        //Fin Export ------------------------------------------------------------------------------
        
		/**
		*  Fait les actrions nécessaire au changement de périmetre
		* */
		public function onPerimetreChange():void{
		
			//dispatchEvent(new Event("PerimetreChange"));
			//synthese.removeChild(treeOrga);
			//treeOrga = null;
			//treeOrga = new InvSearchTree();			
			//synthese.addChildAt(treeOrga,0);
			//callLater(initTree);
			
		}
	}
}