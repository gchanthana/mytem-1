package univers.inventaire.inventaire.menu
{
	import flash.events.Event;
	
	/**
	 * Classe dynamique Evenment dispatcher par l'onglet 'Gestion Direct'
	 * Permet de passer les parametres concernant un noeud
	 **/ 
	public dynamic class RechercheMenuEvent extends Event
	{
		/**
		 * Constructeur 
		 * Voir la classe flash.events.Event pour les parametres
		 * */		
		public function RechercheMenuEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
	}
}