package univers.inventaire.inventaire.creation.nouvelleResources.commande.cible
{
	import composants.util.TextInputLabeled;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class ChoixCibleCompteFacturationImpl extends Canvas
	{
		
		public var dgCompteFacturation : DataGrid;
		public var dgSousCompte : DataGrid;
		public var txtFiltreCompte : TextInputLabeled;
		public var txtFiltreSousCompte : TextInputLabeled;
		
		
		public var listeCompteFacturation : ArrayCollection;		
		public var listeSousCompte : ArrayCollection;		
		
		
		private var _operateurSelectionne : Object;
		public function set operateurSelectionne(op : Object):void{
			_operateurSelectionne = op;
			getCompteFacturation();
		}
		public function get operateurSelectionne():Object{
			return _operateurSelectionne; 
		}
		
		
		public var idGroupeClient : Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
		
		
		
		public function ChoixCibleCompteFacturationImpl()
		{
			super();
		}
		
		override protected function commitProperties():void{  
			super.commitProperties();
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
		}
		
		//---------- HANDLERS -------------------------------------------------------------------------------//
		 protected function creationCompleteHandler(fe : FlexEvent):void{
		 	dgCompteFacturation.addEventListener(ListEvent.CHANGE,dgCompteFacturationChangeHandler);
			dgSousCompte.addEventListener(ListEvent.CHANGE,dgSousCompteChangeHandler);
			txtFiltreCompte.addEventListener(Event.CHANGE,txtFiltreCompteChangeHandler);
			txtFiltreSousCompte.addEventListener(Event.CHANGE,txtFiltreSousCompteChangeHandler);
		 }
		 
		 protected function dgCompteFacturationChangeHandler(le : ListEvent):void{
		 	
		 	if(dgCompteFacturation.selectedItem != null)
		 	{
		 		getSousCompte(dgCompteFacturation.selectedItem.IDCOMPTE_FACTURATION);
		 		compteFacturation = {
		 			IDCOMPTE_FACTURATION : dgCompteFacturation.selectedItem.IDCOMPTE_FACTURATION,
		 			OPERATEURID : operateurSelectionne.id}			 	
		 	}
		 }
		 
		 protected function dgSousCompteChangeHandler(le : ListEvent):void{
		 	if(dgSousCompte.selectedItem != null){
		 		sousCompteFacturation = {
		 				SOUS_COMPTE : dgSousCompte.selectedItem.SOUS_COMPTE,
		 				IDSOUS_COMPTE : dgSousCompte.selectedItem.IDSOUS_COMPTE}
		 	}
		 }
		 
		 protected function txtFiltreCompteChangeHandler(ev : Event):void
		 {
		 	filtrerLaListe(listeCompteFacturation,listeCompteFacturationFilterFunction)        
		 }
		 protected function txtFiltreSousCompteChangeHandler(ev : Event):void
		 {
		 	filtrerLaListe(listeSousCompte,listeSousCompteFilterFunction)	       
		 }
		
		//---------- PRIVATE FUNCTONS -----------------------------------------------------------------------//
		private function filtrerLaListe(liste : ArrayCollection, filterFunction : Function):void{
			if(liste != null){
				liste.filterFunction = filterFunction;
				liste.refresh();
			}
		}
		
		private function listeCompteFacturationFilterFunction(item : Object):Boolean{
			if(String(item.COMPTE_FACTURATION).toLowerCase().search(txtFiltreCompte.text.toLowerCase()) != -1){
				return true;
			}else{
				return false;
			}
		}
		
		private function listeSousCompteFilterFunction(item : Object):Boolean{
			if(String(item.SOUS_COMPTE).toLowerCase().search(txtFiltreSousCompte.text.toLowerCase()) != -1){
				return true;
			}else{
				return false;
			}
		}
		
		
		//----------- REMOTING ------------------------------------------------------------------------------//
		private function getCompteFacturation():void{
			
			listeCompteFacturation = null;
			listeCompteFacturation = new ArrayCollection();
			listeCompteFacturation.refresh();
			
			listeSousCompte = null;
			listeSousCompte = new ArrayCollection();
			listeSousCompte.refresh();
			
			
			if((operateurSelectionne != null) && (idGroupeClient > 0)){
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande",
																				"getListCfRacine",
																				getCompteFacturationResultHandler);
				
			RemoteObjectUtil.callService(op,idGroupeClient,
											operateurSelectionne.id);
			}
		}
		private function getCompteFacturationResultHandler(re : ResultEvent):void{
						
			if(re.result){
				
				listeCompteFacturation = re.result as ArrayCollection;
				compteFacturation = 
				{
				 	IDCOMPTE_FACTURATION : 0,
				 	OPERATEURID : 0
			 	}				
			}
			
		}
		
		
		private function getSousCompte(idCompteFactuartion : Number):void{
			listeSousCompte = null;
			listeSousCompte = new ArrayCollection();
			listeSousCompte.refresh();
			
			if (idCompteFactuartion > 0){
				var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande",
												  "getListSousCompteCf",
												  getSousCompteResultHandler
												  );
												  
				RemoteObjectUtil.callService(op,idCompteFactuartion); 	
			}
				
		}
		
		private function getSousCompteResultHandler(re : ResultEvent):void{
			if(re.result){
				listeSousCompte = re.result as ArrayCollection;
			}	
		}
		
		private var _compteFacturation : Object;
		public function set compteFacturation(value : Object):void
		{
			_compteFacturation = value;
		}
		public function get compteFacturation():Object
		{
			return _compteFacturation;
		}
		
		private var _sousCompteFacturation: Object;
		public function set sousCompteFacturation(value : Object):void
		{
			_sousCompteFacturation = value;
		}
		public function get sousCompteFacturation():Object
		{
			return _sousCompteFacturation;
		}
		
		
		
	}
}