package univers.inventaire.inventaire.creation.operationresiliation
{
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	
	
	/**
	 * Classe evenement signifiant l'on veut enregistrer une Opération
	 * Permet le passage de parametres relatif à  l'enregistrement de l'opération
	 * */
	public class EnregistrerOperationEvent extends Event
	{
		
		//un tableau d'idinventaire produits à enregistrer pour l'opération
		private var _tabproduitsSelectionnee : Array;
		
		//le commentaire de l'operation
		private var _commentaire : String;
		
		//la ref. client
		private var _ReferenceCli :  String;
		
		//la ref. operateur
		private var _ReferenceOpe :  String;		
		
		//le type d'operation
		private var _typeOperation : String;
		
		//le libelle de l'operation
		private var _libelleOperation : String;
		
		//l'identifiant de l'operation
		private var _idOperation : int;		
		
		//l'identifiant du contact de l'operation
		private var _idContact : int = 0;
		
		
		//l'identifiant du distributeur ou de l'agence de l'operation
		private var _idSociete : int = 0;
		
		
		/**
		 * Constructeur
		 * voir la classe lash.events.Event pour les parametres
		 * */
		public function EnregistrerOperationEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		
		//getter
		/**
		 * Retourne un tableau d'identifiants de ressource
		 * @return _tabproduitsSelectionnee un tableau d'identifiants de ressource
		 * */
		public function get tabProduits():Array{
			return _tabproduitsSelectionnee;
		}
		
		
		/**
		 * Retourne le commentaire pour l'operation
		 * @return _commentaire le commentaire pour l'operation
		 * */
		public function get commentaire():String{
			return _commentaire;
		}
		
		/**
		 * Retourne la ref. client pour l'operation
		 * @return _ReferenceCli la ref. client pour l'operation
		 * */
		public function get ReferenceCli():String{
			return _ReferenceCli;
		}
		
		/**
		 * Retourne la ref. operateur pour l'operation
		 * @return _ReferenceOpe la ref. operateur pour l'operation
		 * */
		public function get ReferenceOpe():String{
			return _ReferenceOpe;
		}				
		
		/**
		 * Retourne l'identifiant de l'operation
		 * @return _idOperation l'identifiant de l'operation
		 * */
		public function get idOperation():int{
			return _idOperation;
		}
		
		/**
		 * Retourne le type d'operation (1 = resiliation, 2 = creation)
		 * @return _typeOperation le type d'operation
		 * */
		public function get typeOperation():String{
			return _typeOperation;
		}
		
		/**
		 * Retourne le libelle de l'operation
		 * @return _libelleOperation 
		 * */
		public function get libelleOperation():String{
			return _libelleOperation;
		}
		
		/**
		 * Retourne l'identifiant du contact de l'operation
		 * @return _idContact
		 * */
		public function get idContact():int{
			return _idContact;
		}
		
		/**
		 * Retourne l'identifiant du distributeur ou de l'agence pour l'operation
		 * @return _idSociete
		 * */
		public function get idSociete():int{
			return _idSociete;
		}
		
		/**
		 * Setter pour le tableau d'identifiants de ressources
		 * @param tab le tableau d'identifiant
		 * */
		public function set tabProduits(tab : Array):void{
			_tabproduitsSelectionnee = tab;
		}
		
		/**
		 * Setter pour l'identifiant de l'opération
		 * @param idop l'id de l'operation
		 * */
		public function set idOperation(idop : int):void{
			_idOperation = idop;
		}
		
		
		/**
		 * Setter pour le commentaire de l'operation
		 * @param s le commentaire
		 * */
		public function set commentaire(s : String):void{
			_commentaire = s;
		}
		
		/**
		 * Setter pour la ref. client pour l'operation
		 * @param s la ref. client pour l'operation
		 * */
		public function set ReferenceCli(s : String):void{
			_ReferenceCli = s;
		}
		
		/**
		 * Setter pour la ref. operateur pour l'operation
		 * @param s la ref. operateur pour l'operation
		 * */
		public function set ReferenceOpe(s : String):void{
			_ReferenceOpe = s;
		}	
				
		/**
		 * Setter pour le type d'operation
		 * @param le type d'operation
		 * */
		public function set typeOperation(s : String):void{
			_typeOperation = s;
		}
		
		/**
		 * Setter pour le libelle de l'operation
		 * @param s le libelle
		 * */
		public function set libelleOperation(s : String):void{
			_libelleOperation = s;
		}
		
		/**
		 * Setter pour l identifiant du contact de l'operation
		 * @param idc l'identifiant du contact de l'operation
		 * */
		public function set idContact(idc : int):void{
			_idContact = idc;
		}
		
		/**
		 * Setter pour l'identifiant du distributeur ou de l'agence 
		 * @param ids l'identifiant de la societe
		 * */
		public function set idSociete(ids : int):void{
			_idSociete = ids;
		} 
		
	}
}