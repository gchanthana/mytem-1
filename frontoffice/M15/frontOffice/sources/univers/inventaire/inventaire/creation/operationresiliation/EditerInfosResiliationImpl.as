package univers.inventaire.inventaire.creation.operationresiliation
{
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.EditerInfosCommandeImpl;
	import univers.inventaire.inventaire.etapes.OperationVO;
	
	[Bindable]
	public class EditerInfosResiliationImpl extends EditerInfosCommandeImpl
	{
		public function EditerInfosResiliationImpl()
		{
			super();
		}
		
		private var _operation : OperationVO;
		public function set operation(value : OperationVO):void{
			_operation = value;
		}
		public function get operation():OperationVO{
			return _operation;
		}
		
		
		override protected function btUpdateClickHandler(me:MouseEvent):void
		{
			if(txtLibelle.text.length > 0){
				
				operation.LIBELLE_OPERATIONS = txtLibelle.text;
				operation.COMMENTAIRE = txtCommentaires.text;
				operation.REF_OPERATEUR = txtRefOperateur.text;
				operation.REFERENCE_INTERNE = txtRefClient.text;
								
				operation.updateInfos();	
				
			}else{
				Alert.okLabel = "Fermer";
				Alert.buttonWidth = 100;
				Alert.show("Le libellé de la commande est obligatoire");
			}
			
		}
	}
}