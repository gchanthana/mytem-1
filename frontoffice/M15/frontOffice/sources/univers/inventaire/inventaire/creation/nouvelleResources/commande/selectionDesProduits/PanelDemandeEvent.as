package univers.inventaire.inventaire.creation.nouvelleResources.commande.selectionDesProduits
{
	import flash.events.Event;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	
	
	
	/**
	 * Classe évènement permettant de passer les evenements relatifs aux ecrans de Commandes
	 * */
	public class PanelDemandeEvent extends Event
	{
		/**
		 * Constante definissant le type d'evenement dispatché lors de la sortie d'une commande
		 * */
		public static const SORTIR : String = "sortirDemande";
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la sauvegarde d'une commande
		 * */
		public static const SAVE_COMPLETE : String = "demandeSaved";
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la mise à jour d'une commande
		 * */
		public static const UPDATE_COMPLETE : String = "demandeUpdated"
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la fermeture d'un ecran de commande
		 * */
		public static const FERMER : String = "demandeFermer";
		
		/**
		 * Reference vers la commande
		 * */
		public var commande : Commande;
		
		/**
		 * Constructeur
		 * Voir la classe flash.events.Event pour les parametres
		 * */
		public function PanelDemandeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}