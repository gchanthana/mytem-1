package univers.inventaire.inventaire.creation.operationresiliation
{
	import composants.util.ConsoviewUtil;
	
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	
	import univers.inventaire.inventaire.export.IExportable;
	/**
	 * Classe VObjet de la demande de résiliation
	 * */
	public class ExportDemandeResiliation implements IExportable
	{
		//le type resiliation
		private const RESILIATION : int = 1;
		
		/**
		 * le libelle de l'opération
		 * */
		public var libelle : String;
		
		/**
		 * la liste des produits à résilier
		 * */
		public var panier : Array;
		
		/**
		 * l'identifiant du contact
		 * */
		public var contactId : int;
		
		/**
		 * l'identifiant de la société
		 * */			
		public var societeId : int;
		
		/**
		 * l'identifiant de l'opérateur
		 * */
		public var operateurId : int;
		
		/**
		 * le nom de l'opérateur
		 * */
		public var operateurNom : int;
				
		/**
		 * les commentaires
		 * */
		public var commentaire : String;
				
		/**
		 * la ref client
		 * */
		public var ref_interne : String;
		
		/**
		 * la ref opérateur
		 * */
		public var ref_operateur : String;
		
		//l'url pour l'export
		private const URL_EXPORT : String = "";
		
		
		
		/**
		 * Constructeur
		 * */
		public function ExportDemandeResiliation(){
			
		}
		
		/**
		 * Constant definissant le format PDF
		 * */
		public static const FORMAT_PDF : String = "PDF";
		
				
		/**
		 * Affiche la resiliation au format spécifié dans une nouvelle fenêtre
		 * */
		public function exporter(format:String="PDF"):void
		{
			if (panier != null){
				var url:String = moduleWorkFlowIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/cfm/cycledevie/demandes/Display_Demande.cfm";
	            var variables:URLVariables = new URLVariables();
	            
	            variables.FORMAT = format;	             
	            variables.TYPE = RESILIATION;
				variables.PERIMETRE_LIBELLE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
				//infos commandes
				variables.COMMENTAIRE = commentaire;
				variables.LIBELLE = libelle;
				variables.REF_INTERNE = ref_interne;
				variables.REF_OPERATEUR = ref_operateur;
				
				//operateur
				variables.CONTACTID = contactId;
				variables.OPERATEURNOM = operateurNom;				
				variables.SOCIETEID = societeId;
				
				variables.LI_PRODUITS = ConsoviewUtil.extraireColonne(ArrayCollection(panier),"libelleProduit");			
				variables.LI_LIGNES = ConsoviewUtil.extraireColonne(ArrayCollection(panier),"libelleLigne");
				
				
				var request:URLRequest = new URLRequest(url);
	            request.data = variables;
	            request.method=URLRequestMethod.POST;
	            
	            navigateToURL(request,"_blank");

			}									
			else
				Alert.show("la demande n'a pas de produit");                 
				
		}
		
	}
}