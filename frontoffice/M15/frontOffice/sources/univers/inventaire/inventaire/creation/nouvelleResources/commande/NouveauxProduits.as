package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	import composants.access.ListePerimetresWindow;
	import composants.util.ConsoviewFormatter;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Tree;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.IDemande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.PanelRechercheContact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.PanelRechercheSociete;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Contact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
		
	public class NouveauxProduits extends NouveauxProduitsIHM implements IDemande
	{
		public static const  EN_COURS : int = 1;
		public static const  ANNULER : int = 2;
		public static const  LIVREE : int = 3;
						
		public static const UPDATE_COMPLETE : String = "demandeUpdated";
		public static const SAVE_COMPLETE : String = "demandeSaved";
		
		public static const LISTE_COMPLETE : String = "demandeListOk";
		public static const DELETE_COMPLETE : String = "demandeDeleted";
		public static const SORTIR : String = "sortirDemande";
		
		private var opOperateur : AbstractOperation;
		private var opLigneExistante : AbstractOperation;
		private var opTheme : AbstractOperation;		
		
		private var nodeId : int; //l'identifiant du noeud 
		private var nodeLibelle : String; //le libellé du noeud
		private var nodePerimetre : String;	//le type de perimetre pour le noeud
		
		private const NOUVELLE_LIGNE : int = 1;
		private const NOUVEAU_PRODUIT : int = 2;
		private const MIXTE : int = 3;
		
		private const TYPE_COMMANDE : String = "NouveauxProduits";
		
		private var isConsult : Boolean = false;
	
		//Selection des lignes --------------------------------------------------------------------------------------------
		[Bindable]
		private var listeSousTete : ArrayCollection;
		
		[Bindable]
		private var selectedSousTete : Array;
		
		/// recherche contact / société
		private var rechercheContact : PanelRechercheContact;
		private var rechercheSociete : PanelRechercheSociete;		
		private var listeOperateur : ArrayCollection;		
		private var idGroupe : int =  CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;	
		
		private var commande : Commande;
		private var nouvelleLigneDeLaCommande : ElementCommande;
		private var panier : ElementCommande;
		private var themes : ArrayCollection;
		private var contacts : Contact;
		private var societes : Societe;
	
		private var currentSocieteId : int = 0;
			
		//--- operateur
		private var currentOperateurId : int = -1;	
		private var init : Boolean = true;
		private var indexLigne : int = 0;
			
			
		private var _modeEcriture : Boolean;
		[Bindable]
		public function get modeEcriture():Boolean{
			return _modeEcriture 
		}
		
		public function set modeEcriture(mode : Boolean):void{
			_modeEcriture = mode;
		}	
			
						
		public function NouveauxProduits()
		{
			//TODO: implement function
			super();	
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);			
		}
		
		public function clean():void{
			if (opTheme != null) opTheme.cancel();
			if (opLigneExistante != null) opLigneExistante.cancel();
			if (opOperateur != null) opOperateur.cancel();			
			commande.clean();
			nouvelleLigneDeLaCommande.clean();	
			panier.clean();		
		}
		
		private var _idDemande : int = 0;
		public function set idDemande(idd : int):void{
			_idDemande = idd;
		}
		
		public function get idDemande():int{
			return _idDemande;
		}
		
		public function getDemande(idd : int):void{
			if (idd > 0){
				commande = new Commande(idd);
				nouvelleLigneDeLaCommande = new ElementCommande();				
				isConsult = true;
				panier = new ElementCommande();			
				initCommande();
			}
		}
		
		public function cacherRapprochement():void{
			/*-- NOTHING TO DO ---*/
		}
		
		private function initIHM(fe : FlexEvent):void{
			
			
			
			//rechercher Contact
			imgSearchContact.addEventListener(MouseEvent.CLICK,afficherRechercheContact);
			imgSearchSociete.addEventListener(MouseEvent.CLICK,afficherRechercheSociete);			
				
			//cmbSociete.addEventListener(Event.CHANGE,filtrerContactBySociete);
						
			cmbContact.addEventListener(Event.CHANGE,selectSocieteFromContact);		
			cmbSociete.addEventListener(Event.CHANGE,selectOperateursBySociete);
			cmbOperateur.addEventListener(Event.CHANGE,selectThemesByOperateur);						
			
			cmbTheme.addEventListener(Event.CHANGE,filtrerGridCatalogue);
			
			btEnregistrer.addEventListener(MouseEvent.CLICK,sauvegarderCommande);			
			btAnnuler.addEventListener(MouseEvent.CLICK,annulerCommande);			
			btPDF.addEventListener(MouseEvent.CLICK,exporterPDF);
			btSortir.addEventListener(MouseEvent.CLICK,sortirDeLaCommande);
			btAjouterLignes.addEventListener(MouseEvent.CLICK,ajouterLignes);						
			btSuppLignes.addEventListener(MouseEvent.CLICK,supprimerLignes);
			
			tabNlleLigne.addEventListener(FlexEvent.SHOW,configCatalogue);
			tabligneEx.addEventListener(FlexEvent.SHOW,configCatalogue);
			
			txtFiltreNllesLignes.addEventListener(Event.CHANGE,filtrerGridLignes);

			//treeOperateur
			treeOperateur.myTree.liveScrolling = false;			
			treeOperateur.myTree.addEventListener(Event.CHANGE,chargerListeSousTete);			
			treeOperateur.searchInput.styleName = "TextInputInventaire";			
			txtFiltreLigne.addEventListener(Event.CHANGE,filtrerLeGridLigne);		
			colLigneExistante.labelFunction = formatDataTipLigne;
			
			
			
			//selction des produits
			myGridLignes.addEventListener(Event.CHANGE,selectionnerThemeCorrespondant);
			btUp.addEventListener(MouseEvent.CLICK,monterLesProduitsSelectionnes);
			btAllUp.addEventListener(MouseEvent.CLICK,monterTousLesProduitsSelectionnes);
			btDown.addEventListener(MouseEvent.CLICK,descendreLaSelection);		
			
			
			//gridCatalogue
			txtFiltreCat.addEventListener(Event.CHANGE,filtrerGridCat);
			txtFiltreCat.enabled = true;
			//gridProduit
			myGridProduits.dataProvider = new ArrayCollection();
			txtFiltreCom.addEventListener(Event.CHANGE,filtrerGridProduit);
			txtFiltreCom.enabled = true;
			
			contacts = new Contact();
			contacts.addEventListener(Contact.LISTE_COMPLETE,remplirComboContact);			
			contacts.prepareList(idGroupe);
			
			societes = new Societe();
			societes.addEventListener(Societe.LISTE_COMPLETE,remplirComboSociete);			
			
			
			//colpdIdLignes.labelFunction = formatDataTipLigne;
			if (commande == null){				
				commande = new Commande();	
				nouvelleLigneDeLaCommande = new ElementCommande();				
				panier = new ElementCommande();				
				commande.typeCommande = 2;				
				initCommande();
			}
			//getListeOperateur();
			//getListeTheme();			
		}
		
		//-----FILTRE
		private function configCatalogue(fe : FlexEvent):void{
			switch(fe.currentTarget.id){
				case "tabligneEx" : afficherLeCatalogue();break;
				case "tabNlleLigne" : cmbTheme.dispatchEvent(new Event(Event.CHANGE));break;
			}
			
		}
		
		
		private function filtrerGridLignes(ev : Event):void{
			(myGridLignes.dataProvider as ArrayCollection).filterFunction = filterFunc;
			(myGridLignes.dataProvider as ArrayCollection).refresh();
		}
		
		private function filterFunc(value : Object):Boolean{
			if ((String(value.libelletheme).toLowerCase().search(txtFiltreNllesLignes.text.toLowerCase()) != -1) 
				||
				(String(value.libelleLigne).toLowerCase().search(txtFiltreNllesLignes.text.toLowerCase()) != -1) 
				/* ||
				((String(value.commentaire).toLowerCase().search(txtFiltreLignes.text.toLowerCase() != -1)  */)
			{
				return true;
			}else{
				return false;
			}
		}
		
		private function filtrerLeGridLigne(e : Event):void{
			listeSousTete.filterFunction = processfitrerLeDataGrid;
			listeSousTete.refresh();
		}
	
		//filtre pour le grid des sous-tete
		private function processfitrerLeDataGrid(value : Object):Boolean{
			if (String(value.SOUS_TETE.toLowerCase()).search(txtFiltreLigne.text.toLowerCase()) != -1) {
				return (true);
			} else {
				return (false);
			}
		}	
		
		//filtre le grid des produits
		private function filtrerGridProduit(ev : Event):void{
			if (myGridProduits.dataProvider != null){
				(myGridProduits.dataProvider as ArrayCollection).filterFunction = processFiltrerGridProduit;
				(myGridProduits.dataProvider as ArrayCollection).refresh();	
			}
		}
		
		private function processFiltrerGridProduit(value : Object):Boolean{
 			if ((String(value.libelleLigne.toLowerCase()).search(txtFiltreCom.text.toLowerCase()) != -1)
				||
			    (String(value.libelletheme.toLowerCase()).search(txtFiltreCom.text.toLowerCase()) != -1)
			    || 
			    (String(value.libelleProduitCat.toLowerCase()).search(txtFiltreCom.text.toLowerCase()) != -1))
			{
				return (true);
			} else {
				return (false);
			}
		}
		
		//filtre le grid catalogue
		private function filtrerGridCat(ev : Event):void{
			if (myGridCatalogue.dataProvider != null){
				(myGridCatalogue.dataProvider as ArrayCollection).filterFunction = processFiltrerGridCat;
				(myGridCatalogue.dataProvider as ArrayCollection).refresh();
			}
		}
		
		private function processFiltrerGridCat(value : Object):Boolean{
			if ((String(value.THEME_LIBELLE.toLowerCase()).search(txtFiltreCat.text.toLowerCase()) != -1)
				||
			    (String(value.LIBELLE_PRODUIT.toLowerCase()).search(txtFiltreCat.text.toLowerCase()) != -1))
			{
				return (true);
			} else {
				return (false);
			}
		}
		//-----FORMAT			
		
		//formatage du telepone dans le grid
		private function formatDataTipLigne(item : Object, column : DataGridColumn):String{
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}
		
		//-----INFORMATIONS
		
		
		//----- TREEOPERATEUR
		private function chargerListeSousTete(e : Event):void{
			 
			var selectedNode:Tree = (e.currentTarget as Tree);
			
			var nodeType:int = parseInt(selectedNode.selectedItem.@BOOL_ORGA,10);	
			var nodeTypeOrga : String = selectedNode.selectedItem.@TYPE_ORGA;	
						
			if ((nodeType != ListePerimetresWindow.BOOL_ORGA) && (nodeTypeOrga != "" )) {				
				setLocalNodeParams(selectedNode.selectedItem);										
				txtFiltreLigne.enabled = false;
				getLinesFromNode();					
				txtFiltreLigne.text = "";
			}else{					
				myGridLigne.dataProvider = null; 
				txtFiltreLigne.enabled = false;
				unSetLocalNodeParams();
			}			
			 
		}
		
		//met à jour les params pour mis a jour du tb
		private function setLocalNodeParams(node : Object):void{			
			nodePerimetre = node.@TYPE_PERIMETRE;
			nodeId = node.@NODE_ID;
			nodeLibelle = node.@LABEL;
						
		}
		//reset les parametre locale		
		private function unSetLocalNodeParams():void{
			
			nodeId = -1;
			nodeLibelle = "";
			nodePerimetre = "";		
		}
		
		
		//------ Recherche
		private function afficherRechercheContact(me :MouseEvent):void{
			rechercheContact = new PanelRechercheContact();	
			rechercheContact.addEventListener(PanelRechercheContact.CREER,rafraichirContact);
			rechercheContact.addEventListener(PanelRechercheContact.EFFACER,rafraichirContact);
			rechercheContact.addEventListener(PanelRechercheContact.VALIDER,selectionnerContact);		
			PopUpManager.addPopUp(rechercheContact,this,true);	
			PopUpManager.centerPopUp(rechercheContact);
		}
		
		private function rafraichirContact(ev : Event):void{
			contacts.prepareList(idGroupe);
		}
		
		private function afficherRechercheSociete(me : MouseEvent):void{
			rechercheSociete = new PanelRechercheSociete();
			rechercheSociete.addEventListener(PanelRechercheSociete.CREER,rafraichirSociete);
			rechercheSociete.addEventListener(PanelRechercheSociete.EFFACER,rafraichirSociete);
			rechercheSociete.addEventListener(PanelRechercheSociete.VALIDER,selectionnerSociete);		
			PopUpManager.addPopUp(rechercheSociete,this,true);	
			PopUpManager.centerPopUp(rechercheSociete);		
		}
		
		private function rafraichirSociete(ezv : Event):void{
			societes.prepareList(idGroupe);
		}
		
		//---- COMMANDE
		private function initCommande():void{				
			commande.addEventListener(Commande.CREATION_COMPLETE,afficherCommande);
			commande.addEventListener(Commande.SAVE_COMPLETE,commandeCree);
			commande.addEventListener(Commande.UPDATE_COMPLETE,commandeUpdated);
			commande.addEventListener(Commande.UPDATESTATUT_COMPLETE,commandeStautUpdated);
			initnouvelleLigneDeLaCommande();	
			initPanier();
		}
		
		private function sortirDeLaCommande(me : MouseEvent):void{
			dispatchEvent(new Event(NouveauxProduits.SORTIR));
		}
		
		private function sauvegarderCommande(me : MouseEvent):void{			
			//1.Informations
			
			commande.libelle = txtLibelle.text;
			commande.refClient = txtReferenceCli.text;
			commande.refOperateur = txtReferenceOpe.text;
			commande.dateEffective = dfDateEffet.text;
			commande.dateLivraison = dfDateLivraison.text;
			commande.statutID = EN_COURS;			
			//2.Opérateur		
			if (cmbContact.selectedIndex != -1){
				commande.contactID = cmbContact.selectedItem.contactID;					
			}else{
				commande.contactID = 0;					
			}
			if (cmbOperateur.selectedIndex != -1){
				commande.operateurID = cmbOperateur.selectedItem.OPERATEURID; 			
			}else{
				commande.operateurID = 0;
			}
			if (cmbSociete.selectedIndex != -1){
				commande.societeID = cmbSociete.selectedItem.societeID;				
			}else{
				commande.societeID = 0;
			}
			
			//3.Cible
			try{				
				commande.idGroupeCible = treeCible.myTree.selectedItem.@NODE_ID;			
			}catch(e :Error){
				trace("pas de cible selectionne");
			}
			
			commande.flagCible = int(cbNewCible.selected);
			commande.idGroupeClient = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				
			if (!(commande.commandeID)){
				if (checkCommande(commande)){
					commande.sauvegarder(idGroupe);	
				}				
			}else{
				if (checkCommande(commande)){
					commande.updateCommande();	
				}	
			}
		}
		
		private function checkCommande(c : Commande):Boolean{
			var libelleOk : Boolean = false;
			var dateEffetOk : Boolean = false;
			var contactOk : Boolean = false;
			var produitsOk : Boolean = false;
			
			var message : String = "\n";
			var pronpt : String;
			var count : int = 0;
			
			if (c.libelle.length > 0) libelleOk = true;
			else {
				message = message + "- Libellé.\n";
				count++;
			}
			
			if (dfDateEffet.text.length > 0) dateEffetOk = true;
			else {
				message = message + "- Date effective.\n";
				count++;
			}
			
			if (cmbContact.selectedIndex != -1) contactOk = true;
			else {
				message = message + "- Contact\n";
				count++;
			}
			
			if (myGridProduits.dataProvider != null){
				if ((myGridProduits.dataProvider as ArrayCollection).length >0){
					return true;
				}else{
					message = message + "- La grille des produits\n";
					count++;
				}
			}else{
				message = message + "- La grille des produits\n";
				count++;
			} 
			
			if (libelleOk && dateEffetOk && contactOk && produitsOk){
				return true;
			}else{
				if (count > 1){
					pronpt = "Les champs suivant sont obligatoires.";
				}else{
					pronpt = "Le champ suivant est obligatoire."
				}
				Alert.show(message,pronpt);	
				return false;
			}
		}
		
		private function annulerCommande(me : MouseEvent):void{
			if (commande.commandeID) {
				commande.statutID = ANNULER;
				commande.updateStatut();				
			}
		}
		
		private function reprendreCommande(me : MouseEvent):void{
			if (commande.commandeID) {
				commande.statutID = EN_COURS;
				commande.updateStatut();				
			}
		}
		
		
		
		private function afficherCommande(ev : Event):void{
			//1.Informations
			txtLibelle.text = commande.libelle;
			txtReferenceCli.text = commande.refClient;
			txtReferenceOpe.text = commande.refOperateur;
			
			lblStatut.text= commande.statut;
			configBtAnuller(commande.statutID);		
			
			dfDateEffet.selectedDate = new Date(commande.dateEffective);
			dfDateLivraison.selectedDate = new Date(commande.dateLivraison);
			
			//2 contact
			//cmbContact.selectedIndex = ConsoviewUtil.getIndexById(cmbContact.dataProvider as ArrayCollection,"contactID",commande.contactID);						
			
			currentOperateurId = commande.operateurID;
			currentSocieteId = commande.societeID;
			
			/* if (currentOperateurId > 0) cmbOperateur.selectedIndex = ConsoviewUtil.getIndexById(cmbOperateur.dataProvider as ArrayCollection,"OPERATEURID",currentOperateurId)
			else cmbOperateur.selectedIndex = 0;
			
			cmbOperateur.dispatchEvent(new Event(Event.CHANGE)); */
			
			//3.Cible			
			if (commande.idGroupeCible != 0){
				try{
					selectNode(commande.idGroupeCible);
				}catch(e : Error){
					
				}
			}		
				
			cbNewCible.selected = Boolean(commande.flagCible == 1);	
			
			
			
			
			//4.lignes						
			nouvelleLigneDeLaCommande.prepareList(commande.commandeID,NOUVELLE_LIGNE);				
			panier.prepareList(commande.commandeID,commande.typeCommande);	
			
			callLater(desactiverFormulaireOperateur);
			
			
		}
		
		private function selectNode(nodeId : int):void
		{
			trace(nodeId);
			if (treeCible.myTree.selectedIndex < 0 && nodeId >= 0) {
				var tmp:XMLList = (treeCible.myTree.dataProvider[0] as XML).descendants().(@NODE_ID==nodeId);
				if (tmp[0] == null)
					return ;
				treeCible.expandToNode(tmp,true);
				treeCible.myTree.selectedItem = tmp;
				treeCible.myTree.scrollToIndex(treeCible.myTree.selectedIndex);
			}
		}
		
		private function configBtAnuller(statutID : int):void{
			if (statutID == ANNULER) {
				btAnnuler.label = "Reprendre";
				if (btAnnuler.hasEventListener(MouseEvent.CLICK)){
					try{
						btAnnuler.removeEventListener(MouseEvent.CLICK,annulerCommande);	
					}catch(e : Error){						
					}					
				}
				btAnnuler.addEventListener(MouseEvent.CLICK,reprendreCommande);
				desactiverFormulaire();
			}else if (statutID == EN_COURS){
				btAnnuler.label = "Annuler";
				if (btAnnuler.hasEventListener(MouseEvent.CLICK)){
					try{
						btAnnuler.removeEventListener(MouseEvent.CLICK,reprendreCommande);	
					}catch(e : Error){						
					}finally{
						btAnnuler.addEventListener(MouseEvent.CLICK,annulerCommande);	
						activerFormulaire();	
					}			
				}
			}else if(statutID == LIVREE){
				desactiverFormulaire();
			}
		}
		
		private function desactiverFormulaireOperateur():void{
			cmbContact.enabled = false;
			cmbOperateur.enabled = false;			
			cmbSociete.enabled = false;	
			imgSearchContact.visible = false;
			imgSearchSociete.visible = false;		
		}
		
		private function desactiverFormulaire():void{
			btAjouterLignes.visible = false;
			btEnregistrer.visible = false;
			
			btSuppLignes.visible = false;
			
			imgSearchContact.visible = false;
			imgSearchSociete.visible = false;
			
			txtLibelle.enabled = false;
			txtLibelle.enabled = false;
			txtReferenceCli.enabled = false;
			txtReferenceOpe.enabled = false;
			
			dfDateEffet.enabled = false;
			dfDateLivraison.enabled = false;
			
			treeCible.enabled = false;
			cbNewCible.enabled = false;
			
			cmbContact.enabled = false;
			cmbOperateur.enabled = false;
			cmbSociete.enabled = false;
			cmbTheme.enabled = false;
			
			nsNbLignes.enabled = false;
		}
		
		private function activerFormulaire():void{
			
			btAjouterLignes.visible = true;
			btEnregistrer.visible = true;			
			btSuppLignes.visible = true;
			
			txtLibelle.enabled = true;
			txtLibelle.enabled = true;
			txtReferenceCli.enabled = true;
			txtReferenceOpe.enabled = true;
			
			dfDateEffet.enabled = true;
			dfDateLivraison.enabled = true;
			
			treeCible.enabled = true;
			cbNewCible.enabled = true;
			
						
			cmbTheme.enabled = true;
			
			nsNbLignes.enabled = true;
		}
		
		private function commandeCree(ev : Event):void{
			
			//sauvegarder les nouvelles lignes de la commande
			nouvelleLigneDeLaCommande.commandeID = commande.commandeID;			
			nouvelleLigneDeLaCommande.sauvegarderAll(NOUVELLE_LIGNE);			
			
			//sauvegarder les produits de la commande
			panier.commandeID = commande.commandeID;
			panier.sauvegarderAll(NOUVEAU_PRODUIT);
			
			//mettre à jour la liste des commandes				
			dispatchEvent(new Event(NouvelleLigne.SAVE_COMPLETE));
		}
		
		private function commandeUpdated(ev : Event):void{
			dispatchEvent(new Event(NouvelleLigne.UPDATE_COMPLETE));
		}
		
		private function commandeStatutUpdated(ev : Event):void{
			lblStatut.text = commande.statut;			
		}
		
		private function commandeStautUpdated(ev : Event):void{
			
			lblStatut.text = commande.statut;
			configBtAnuller(commande.statutID);
			
		}
		
		//----- nouvelleLigneDeLaCommande
		private function initnouvelleLigneDeLaCommande():void{
			nouvelleLigneDeLaCommande.addEventListener(ElementCommande.LISTE_COMPLETE,affichernouvelleLigneDeLaCommande);	
			nouvelleLigneDeLaCommande.addEventListener(ElementCommande.DELETE_COMPLETE,nouvelleLigneDeLaCommandeDeleted);
			nouvelleLigneDeLaCommande.addEventListener(ElementCommande.DELETE_ERROR,supprimerLigneError);
			nouvelleLigneDeLaCommande.addEventListener(ElementCommande.UPDATE_COMPLETE,nouvelleLigneDeLaCommandeUpdated);
			nouvelleLigneDeLaCommande.addEventListener(ElementCommande.SAVE_COMPLETE,nouvelleLigneDeLaCommandeSauve);
			nouvelleLigneDeLaCommande.addEventListener(ElementCommande.SAVEALL_COMPLETE,nouvelleLigneDeLaCommandeEntierSauve);
						
		}
		
		private function setIndexLigne():int{
			var index : int = 0;					
			
			try{	
				var tab : Array = ConsoviewUtil.extraireColonne((myGridLignes.dataProvider as ArrayCollection),"libelleLigne");							
				extraireNumeroLigne(tab);
				tab.sort();				
				index = tab[tab.length-1] + 1;												
			}catch(e : Error){
				trace(e.getStackTrace());
			}			
			return index;			
		}
		
		private function extraireNumeroLigne(arr : Array):void{
			for (var i : int = 0; i < arr.length; i++){
				var tmp : String = String(arr[i]);
				var endIndex : int = tmp.length;
				var tmp2 : String = tmp.substring(9,endIndex);				
				arr[i] = parseInt(tmp2);	
			}
		}
		
		private function rafraichirnouvelleLigneDeLaCommande():void{
			nouvelleLigneDeLaCommande.prepareList(commande.commandeID,NOUVELLE_LIGNE);			
		}
		
		private function affichernouvelleLigneDeLaCommande(ev : Event):void{						
			myGridLignes.dataProvider = nouvelleLigneDeLaCommande.liste;	
			indexLigne = setIndexLigne();		
		}
		
		private function nouvelleLigneDeLaCommandeSauve(ev : Event):void{		
			rafraichirnouvelleLigneDeLaCommande();	
			nsNbLignes.value = 1;
		}
		
		private function nouvelleLigneDeLaCommandeEntierSauve(ev : Event):void{				

		}
		
		private function nouvelleLigneDeLaCommandeUpdated(ev : Event):void{
			rafraichirnouvelleLigneDeLaCommande();
		}
		
		private function nouvelleLigneDeLaCommandeDeleted(ev : Event):void{

			rafraichirnouvelleLigneDeLaCommande();
		}
							
		private function ajouterLignes(me : MouseEvent):void{			
			if (cmbTheme.selectedIndex != -1){				
			//--- to DO ----	
				for (var i : int = 0; i < nsNbLignes.value; i++){
					var ligne : ElementCommande = new ElementCommande();											
					ligne.libelleLigne = "ligne n° " + indexLigne.toString();					 
					ligne.themeID = cmbTheme.selectedItem.IDTHEME_PRODUIT;
					ligne.flagRapproche = 0;
					ligne.libelletheme = cmbTheme.selectedItem.THEME_LIBELLE;
					
					if (commande.commandeID > 0)	{		
										
						ligne.commandeID = commande.commandeID;
						ligne.addEventListener(ElementCommande.SAVE_COMPLETE,nouvelleLigneDeLaCommandeSauve);	
						ligne.sauvegarder(NOUVELLE_LIGNE);
						
					}else{						
						nouvelleLigneDeLaCommande.liste.addItem(ligne);
						nouvelleLigneDeLaCommande.liste.refresh();	
						myGridLignes.dataProvider = nouvelleLigneDeLaCommande.liste;
						indexLigne = indexLigne +1;
					}
				}														 						
			}	
		}	
		
		private function ajouterUnThemePourCetteLigne(ligne : ElementCommande):void{
			/*------- TO DO --------------*/			
		}
		
		private function supprimerLignes(me : MouseEvent):void{		
			if (myGridLignes.selectedIndices.length > 0){
				if (commande.commandeID > 0){
					for ( var i : int = 0 ; i < myGridLignes.selectedItems.length ; i++){
						nouvelleLigneDeLaCommande.deleteElementCommande(ElementCommande(myGridLignes.selectedItems[i]).elementID);	
					}
				}else{					
					 myGridLignes.selectedItems.forEach(supprimerCetteLigne);
					(myGridLignes.dataProvider as ArrayCollection).refresh();					
				}
									
			}
		}
		
		private function supprimerLigneError(ev : Event):void{
			Alert.show("Des produits sont associés à cette ligne.\nSupprimez ces produits avant de supprimer la ligne");
		}
		
		private function supprimerCetteLigne(element : * , index : int , arr : Array):void{
			(myGridLignes.dataProvider as ArrayCollection).removeItemAt((myGridLignes.dataProvider as ArrayCollection).getItemIndex(element));

		}
		
		//--- EXPORT PDF --------------------
		private function exporterPDF(me : MouseEvent):void{
			if (commande.commandeID != 0){
				exporterLePdf();
			}
		}
		
		private function exporterLePdf():void{
			displayExport("pdf");
		}
		
		private function displayExport(format : String):void {			 
			var url:String = cv.NonSecureUrlBackoffice + "/fr/consotel/consoview/cfm/cycledevie/demandes/Display_Demande.cfm";
            var variables:URLVariables = new URLVariables();
            variables.FORMAT = format;	              
            variables.TYPE = TYPE_COMMANDE;
            
            var demande : Object = new Object();
            
			variables.TITRE = "Demande de nouveaux produits";	 
			variables.PERIMETRE_LIBELLE = CvAccessManager.getCurrentPerimetre().PERIMETRE_LIBELLE;           			
			//infos commandes
			variables.LIBELLE = commande.libelle;
			variables.REF_CLIENT = commande.refClient;				
			variables.REF_OPERATEUR = commande.refOperateur;				
			variables.DATE_EFFECTIVE = commande.dateEffective;
			variables.DATE_LIVRAISON = commande.dateCreation;
			variables.SATUT = commande.statut;
			
			//operateur
			variables.CONTACT = Contact(cmbContact.selectedItem).nom + " " + Contact(cmbContact.selectedItem).prenom;
			variables.OPERATEUR = commande.operateurNom;				
			variables.SOCIETE = Societe(cmbSociete.selectedItem).raisonSociale;
		
			variables.FLAG_CIBLE = commande.flagCible;
			
			variables.LI_LIGNES = ConsoviewUtil.extraireColonne(panier.liste,"libelleLigne");
			variables.LI_THEME = ConsoviewUtil.extraireColonne(panier.liste,"libelletheme");
			variables.LI_COMMENTAIRE = ConsoviewUtil.extraireColonne(panier.liste,"commentaire");
			
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method=URLRequestMethod.POST;
            
            navigateToURL(request,"_blank");
        } 			
        //------ FIN EXPORT PDF -----------------
		//----------------------
		private function selectionnerContact(ev : Event):void{			
			cmbContact.selectedIndex = ConsoviewUtil.getIndexById(cmbContact.dataProvider as ArrayCollection,"contactID",rechercheContact.contactSelectionneID);
			cmbContact.dispatchEvent(new Event(Event.CHANGE));				
		}
		
		private function selectionnerSociete(ev : Event):void{
			cmbSociete.selectedIndex = ConsoviewUtil.getIndexById(cmbSociete.dataProvider as ArrayCollection,"societeID",rechercheSociete.societeSelectionneeID);			
			if (currentOperateurId > 0) cmbOperateur.selectedIndex = ConsoviewUtil.getIndexById(cmbOperateur.dataProvider as ArrayCollection,"OPERATEURID",currentOperateurId)			else cmbOperateur.selectedIndex = 0;
			
			cmbSociete.dispatchEvent(new Event(Event.CHANGE));
		}
		
		private function selectSocieteFromContact(ev : Event):void{
			if (cmbContact.selectedIndex != -1){
				cmbSociete.selectedIndex = ConsoviewUtil.getIndexById(cmbSociete.dataProvider as ArrayCollection,
											"societeID",Contact(cmbContact.selectedItem).societeID);	
				cmbSociete.dispatchEvent(new Event(Event.CHANGE));
			}
		}		
			
		private function selectOperateursBySociete(ev : Event):void{
			if (cmbSociete.selectedIndex != -1){
				cmbOperateur.labelField = "NOM";
				cmbOperateur.dataProvider = Societe(cmbSociete.selectedItem).listeOperateurs;
				cmbOperateur.selectedIndex = 0;			
				cmbOperateur.dispatchEvent(new Event(Event.CHANGE));
			}			
		}
		
		private function selectThemesByOperateur(ev : Event):void{
			if(cmbOperateur.selectedIndex != -1){
				currentOperateurId = cmbOperateur.selectedItem.OPERATEURID;
				
				getThemeByOperateur(cmbOperateur.selectedItem.OPERATEURID);
				treeOperateur.initDp(cmbOperateur.selectedItem.OPERATEURID);
					
				if (commande.commandeID > 0){
				
				}else{
					resetElementsCommande();
				}
				 
			}
		}
		
		private function resetElementsCommande():void{			
			
			myGridLigne.dataProvider = null;	
				
			myGridLignes.dataProvider = null;					
			nouvelleLigneDeLaCommande.liste = null;				
			nouvelleLigneDeLaCommande.liste =  new ArrayCollection();
			
			myGridProduits.dataProvider = null;		
			myGridProduits.dataProvider = new ArrayCollection();				
			panier.liste = null;
			panier.liste = new ArrayCollection();
		}
		
		private function filtrerContactBySociete(ev : Event):void{
			/*cmbContact.selectedIndex = -1;
			 try{
			(cmbContact.dataProvider as ArrayCollection).filterFunction = filtrerContact;
			(cmbContact.dataProvider as ArrayCollection).refresh();
			}catch(e : Error){
				
			} */
		}
		
		private function filtrerContact(value : Object):Boolean{
								
			if ((value.hasOwnProperty("societeID"))
				&&(cmbSociete.selectedItem.hasOwnProperty("societeID"))){
				if (parseInt(value.societeID) == cmbSociete.selectedItem.societeID){					
					return (true);
				} else {
					return (false);
				}	
			}
			return (false);				
		}
				
		
		private function remplirComboContact(ev : Event):void{		
			cmbContact.labelField = "nom";
			cmbContact.dataProvider = contacts.liste;
			cmbContact.selectedIndex = ConsoviewUtil.getIndexById(cmbContact.dataProvider as ArrayCollection,"contactID",commande.contactID);
			societes.prepareList(idGroupe);
		}
		
		private function remplirComboSociete(ev : Event):void{
			cmbSociete.labelField = "raisonSociale";
			cmbSociete.dataProvider = societes.liste; 	
			(cmbSociete.dataProvider as ArrayCollection).refresh();		
			selectionnerIndexSociete();			
		}
		
		private function selectionnerIndexSociete():void{
			cmbSociete.selectedIndex = ConsoviewUtil.getIndexById(cmbSociete.dataProvider as ArrayCollection,"societeID",commande.societeID);						
			callLater(remplirComboOperateur);
		}
		
		private function remplirComboOperateur():void{
			cmbOperateur.labelField = "NOM";			
			cmbOperateur.dataProvider = Societe(cmbSociete.selectedItem).listeOperateurs;						
			(cmbOperateur.dataProvider as ArrayCollection).refresh();

			trace("remplirComboOperateur");
			
			if (commande.operateurID > 0){
				cmbOperateur.selectedIndex = ConsoviewUtil.getIndexById(cmbOperateur.dataProvider as ArrayCollection,"OPERATEURID",commande.operateurID)
				cmbOperateur.dispatchEvent(new Event(Event.CHANGE));
			}
		}
		
		//---- FILTRER CATALOGUE
		private function afficherLeCatalogue():void{
			try{
				(myGridCatalogue.dataProvider as ArrayCollection).filterFunction = null;
				(myGridCatalogue.dataProvider as ArrayCollection).refresh();				
			}catch(e : Error){
				trace("[WARNING] Grid Catalogue"); 
			}	
		}
		 
		private function selectionnerThemeCorrespondant(ev : Event):void{
			if (myGridLignes.selectedIndex != -1){
				cmbTheme.selectedIndex = ConsoviewUtil.getIndexById(cmbTheme.dataProvider as ArrayCollection,
																	"IDTHEME_PRODUIT",
																	ElementCommande(myGridLignes.selectedItem).themeID);
				cmbTheme.dispatchEvent(new Event(Event.CHANGE));
				
			}
		}
		
		
		private function filtrerGridCatalogue(ev : Event):void{			
			switch(ev.currentTarget.id){
				case "cmbTheme" : if ((myGridCatalogue.dataProvider as ArrayCollection)!= null) (myGridCatalogue.dataProvider as ArrayCollection).filterFunction = filtrerCmbCatFunc;break;
				case "txtFiltre" : if ((myGridCatalogue.dataProvider as ArrayCollection)!= null) (myGridCatalogue.dataProvider as ArrayCollection).filterFunction = filtrerTxtCatFunc;break;
				default : (myGridCatalogue.dataProvider as ArrayCollection).filterFunction = filtrerTxtCatFunc;break;
			}			
			if (myGridCatalogue.dataProvider != null) (myGridCatalogue.dataProvider as ArrayCollection).refresh();
		}
		
		private function filtrerCmbCatFunc(value : Object):Boolean{
			if (cmbTheme.selectedIndex != -1){				
				var txt : String = cmbTheme.selectedItem.THEME_LIBELLE;
				if ((String(value.THEME_LIBELLE).toLowerCase().search(txt.toLowerCase()) != -1))
				{
					return true;
				}else{
					return false;
				}				
			}
			return false;
		}
		
		private function filtrerTxtCatFunc(value : Object):Boolean{
			var isLigneEx : Boolean = (tnSwitchCommande.selectedChild == tabligneEx);
			trace(isLigneEx);
			if(isLigneEx){				
				if ((String(value.THEME_LIBELLE).toLowerCase().search(txtFiltreCat.text.toLowerCase()) != -1) 
				||
				(String(value.LIBELLE_PRODUIT).toLowerCase().search(txtFiltreCat.text.toLowerCase()) != -1))
				{
					return true;
				}else{
					return false;
				}	
			}else{
				if ((String(value.THEME_LIBELLE).toLowerCase().search(txtFiltreCat.text.toLowerCase()) != -1) 
				||
				(String(value.THEME_LIBELLE).toLowerCase().search(txtFiltreCat.text.toLowerCase()) != -1)
				||
				(String(value.LIBELLE_PRODUIT).toLowerCase().search(txtFiltreCat.text.toLowerCase()) != -1))
				{
					return true;
				}else{
					return false;
				}
			}
		}
		
		
		//FILTRER COMMANDE
		private function filtrergGridCommande(ev : Event):void{
			(myGridProduits.dataProvider as ArrayCollection).filterFunction = filtrerComFunc;
			(myGridProduits.dataProvider as ArrayCollection).refresh();
		}
		
		private function filtrerComFunc(value : Object):Boolean{
			if ((String(value.libelletheme).toLowerCase().search(txtFiltreCom.text.toLowerCase()) != -1) 
				||
				(String(value.libelleLigne).toLowerCase().search(txtFiltreCom.text.toLowerCase()) != -1) 
				||
				(String(value.libelleProduitCat).toLowerCase().search(txtFiltreCom.text.toLowerCase()) != -1))
				{
					return true;
				}else{
					return false;
				}
		}
		//----------- SELECTION DES PRODUITS --------------------------------------------------------------------------------
		//----- panier
		private function initPanier():void{
			
			panier.addEventListener(ElementCommande.LISTE_COMPLETE,afficherPanier);	
			panier.addEventListener(ElementCommande.DELETE_COMPLETE,panierDeleted);
			panier.addEventListener(ElementCommande.UPDATE_COMPLETE,panierUpdated);
			panier.addEventListener(ElementCommande.SAVE_COMPLETE,panierSauve);
			panier.addEventListener(ElementCommande.SAVEALL_COMPLETE,panierSauve);  			
			
		}
		
		private function rafraichirPanier():void{
			panier.prepareList(commande.commandeID,commande.typeCommande);			
		}
		
		private function afficherPanier(ev : Event):void{
			//var len : int = panier.liste.length;
			myGridProduits.dataProvider = panier.liste;			
		}
		
		private function panierSauve(ev : Event):void{					
			rafraichirPanier();	
		}
		
		private function panierEntierSauve(ev : Event):void{				

		}
		
		private function panierUpdated(ev : Event):void{
			rafraichirPanier();
		}
		
		private function panierDeleted(ev : Event):void{

			rafraichirPanier();
		}
		
		
		//--- DESCENDRE
		private function descendreLaSelection(me : MouseEvent):void{
			if (tnSwitchCommande.selectedChild == tabligneEx){
				descendreLesProduisSelectionnesPourLigneEx();
			}else{
				descendreLesProduisSelectionnesPourNlleLigne();
			}
		}
		
		
		//--- DESCENDRE NOUVEAUX PRODUIT
		private function descendreLesProduisSelectionnesPourLigneEx():void{				
			try{					
				if ((myGridLigne.selectedIndices.length > 0)&&(myGridCatalogue.selectedIndices.length > 0)){							
					myGridCatalogue.selectedItems.forEach(descendreLeProduitPourChaqueLigneSelectionne);					
					myGridProduits.dataProvider.refresh();
				}
				
			}catch( re : RangeError ){
				trace("l'indice sort des limites")
			}catch( e : Error){
				trace(e.message,e.getStackTrace());
			}			
		}
		
		private function descendreLeProduitPourChaqueLigneSelectionne(element : * , index : int , arr : Array):void{							
			var len : int = myGridLigne.selectedItems.length;
			for (var i : int = 0; i < len; i++){
				var ligne : ElementCommande = new ElementCommande();	
																		
				ligne.themeID = element.IDTHEME_PRODUIT;								
				ligne.libelletheme = element.THEME_LIBELLE;
				ligne.libelleProduitCat = element.LIBELLE_PRODUIT;
				ligne.produitCatID = element.IDPRODUIT_CATALOGUE;
				ligne.libelleLigne = myGridLigne.selectedItems[i].SOUS_TETE;
				ligne.ligneID = myGridLigne.selectedItems[i].IDSOUS_TETES;
				ligne.typeElement = NOUVEAU_PRODUIT;
				
				
				if (commande.commandeID > 0)	{
					
					ligne.commandeID = commande.commandeID;
					ligne.addEventListener(ElementCommande.SAVE_COMPLETE,panierSauve);	
					ligne.sauvegarder(NOUVEAU_PRODUIT);
				}else{
					if (!ConsoviewUtil.is2InArray(ligne.produitCatID.toString(),
													  ligne.ligneID.toString(),
													  "produitCatID",
													  "ligneID",
													  (myGridProduits.dataProvider as ArrayCollection).source))
					{							
						panier.liste.addItem(ligne);	  	
						panier.liste.refresh();							
						myGridProduits.dataProvider = panier.liste;
						
					}
				}									
			}	
		}
		
		//DESCENDRE NOUVEAUX PRODUITS SUR NOUVELLES LIGNES
		private function descendreLesProduisSelectionnesPourNlleLigne():void{
			try{					
				if ((myGridLignes.selectedIndices.length > 0)&&(myGridCatalogue.selectedIndices.length > 0)){					
					myGridCatalogue.selectedItems.forEach(descendreLeProduitPourChaqueNouvelleLigneSelectionne);					
					myGridProduits.dataProvider.refresh();
				}
				
			}catch( re : RangeError ){
				trace("l'indice sort des limites")
			}catch( e : Error){
				trace(e.message,e.getStackTrace());
			}	
		}
		
		
		private function descendreLeProduitPourChaqueNouvelleLigneSelectionne(element : * , index : int , arr : Array):void{							
			var len : int = myGridLignes.selectedItems.length;
			for (var i : int = 0; i < len; i++){
				var ligne : ElementCommande = new ElementCommande();															
				ligne.themeID = element.IDTHEME_PRODUIT;								
				ligne.libelletheme = element.THEME_LIBELLE;
				ligne.libelleProduitCat = element.LIBELLE_PRODUIT;
				ligne.produitCatID = element.IDPRODUIT_CATALOGUE;
				ligne.libelleLigne = myGridLignes.selectedItems[i].libelleLigne;				
				ligne.typeElement = NOUVELLE_LIGNE;
				
				if (commande.commandeID > 0)	{
					
					ligne.commandeID = commande.commandeID;
					ligne.addEventListener(ElementCommande.SAVE_COMPLETE,panierSauve);	
					ligne.sauvegarder(NOUVEAU_PRODUIT);
				}else{
					if (!ConsoviewUtil.is2InArray(ligne.produitCatID.toString(),
													  ligne.libelleLigne.toString(),
													  "produitCatID",
													  "libelleLigne",
													  (myGridProduits.dataProvider as ArrayCollection).source))
					{							
						panier.liste.addItem(ligne);	  	
						panier.liste.refresh();							
						myGridProduits.dataProvider = panier.liste;
						
					}else{
						trace("produit deja dans la litse pour cette ligne");
					}
				}									
			}	
		}
		
		//--- MONTER
		private function monterLesProduitsSelectionnes(me : MouseEvent):void{		
			if (myGridProduits.selectedIndices.length > 0){
				if (commande.commandeID > 0){
					var datas : ArrayCollection = new ArrayCollection()
					datas.source = myGridProduits.selectedItems;
					panier.deleteAllElementCommande(datas);
				}else{
					myGridProduits.selectedItems.forEach(monterLeProduit);
					myGridProduits.dataProvider.refresh();	
				}
			}			
		}
		
		private function monterTousLesProduitsSelectionnes(me:MouseEvent):void{
			var len : int = (myGridProduits.dataProvider as ArrayCollection).length;				
			if (len > 0){
				if (commande.commandeID > 0){				
					panier.deleteAllElementCommande(myGridProduits.dataProvider as ArrayCollection);
				}else{
					(myGridProduits.dataProvider as ArrayCollection).source.forEach(monterLeProduit);
					myGridProduits.dataProvider.refresh();												
				}	
			}				
		}
		
		private function monterLeProduit(element : * , index : int , arr : Array):void{			
		 	(myGridProduits.dataProvider as ArrayCollection).removeItemAt((myGridProduits.dataProvider as ArrayCollection).getItemIndex(element));
		}
		
//-------------- REMOTING ------------------------------
		private function getListeOperateur():void
		{	
			opOperateur  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Societe",
																				"getOperateurlist",
																				getListeOperateurResultHandler,
																				null);				
			RemoteObjectUtil.callService(opOperateur);		
		}
		
		private function getListeOperateurBySociete(idsociete : int):void
		{
			if (opOperateur != null) opOperateur.cancel();
			opOperateur  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Societe",
																				"getOperateurlistSociete",
																				getListeOperateurResultHandler,
																				null);				
			RemoteObjectUtil.callService(opOperateur,idsociete);		
		}
		
		private function getListeOperateurResultHandler(re :ResultEvent):void{
			cmbOperateur.dataProvider = re.result;
			cmbOperateur.labelField = "NOM"; 		
			if (currentOperateurId > 0) cmbOperateur.selectedIndex = ConsoviewUtil.getIndexById(cmbOperateur.dataProvider as ArrayCollection,"operateurID",currentOperateurId);	
		}
		
		private function getListeTheme():void
		{	
			if (opTheme != null) opTheme.cancel();
			opTheme  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande",
																				"getTheme",
																				getListeThemeResultHandler,
																				null);				
			RemoteObjectUtil.callService(opTheme);		
		}
		
		private function fillComboTheme( source : ArrayCollection):ArrayCollection{
			var newCollection : ArrayCollection = new ArrayCollection();
			var len : int = source.length;			
			for (var i:int = 0;i < len; i++){						
				if (!ConsoviewUtil.isIdInArray(int(source[i].IDTHEME_PRODUIT),"IDTHEME_PRODUIT",newCollection.source)){
					var obj : Object = new Object();				
					obj.THEME_LIBELLE = source[i].THEME_LIBELLE;
					obj.IDTHEME_PRODUIT = source[i].IDTHEME_PRODUIT;	
					newCollection.addItem(obj);
				}
			}			
			return newCollection;
		}
		
		private function getListeThemeResultHandler(re :ResultEvent):void{
			cmbTheme.dataProvider = fillComboTheme(re.result as ArrayCollection);
			cmbTheme.labelField = "THEME_LIBELLE"; 	
			myGridCatalogue.dataProvider = re.result as ArrayCollection;	
			txtFiltreNllesLignes.enabled = Boolean((myGridCatalogue.dataProvider as ArrayCollection).length > 0);			
		}
		
		private function getThemeByOperateur(idope : int):void{
			if (opTheme != null) opTheme.cancel();
			opTheme  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande",
																				"getProduitsByOperateur",
																				getListeThemeResultHandler,
																				null);				
			RemoteObjectUtil.callService(opTheme,idope);		
		}
		
		//--- liste des lignes existante
		private function getLinesFromNode():void{
			
			opLigneExistante = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.inventaire.cycledevie.recherche.facade",
												  "rechercherListeLigne",
												  getLinesFromNodeResultHandler,
												  null);
												  
			RemoteObjectUtil.callService(opLigneExistante,
										nodePerimetre,										
										nodeId); 
		}
		
		private function getLinesFromNodeResultHandler(re : ResultEvent):void{
			listeSousTete = re.result as ArrayCollection;	
			myGridLigne.dataProvider = listeSousTete;
			txtFiltreLigne.enabled = Boolean((myGridLigne.dataProvider as ArrayCollection).length > 0)												
		}
			
	}
}
