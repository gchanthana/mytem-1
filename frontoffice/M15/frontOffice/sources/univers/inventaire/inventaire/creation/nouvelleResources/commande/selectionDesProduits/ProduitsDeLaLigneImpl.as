package univers.inventaire.inventaire.creation.nouvelleResources.commande.selectionDesProduits
{
	import composants.util.TextInputLabeled;
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.fournisseurs.vues.BaseViewImpl;
	
	[Bindable]
	public class ProduitsDeLaLigneImpl extends BaseViewImpl
	{
		public var cboInventaire : ComboBox;
		public var txtFiltre : TextInputLabeled;
		public var dgProduits : DataGrid;
		
		
		private var _listeProduits : ArrayCollection;		
		public function get listeProduits():ArrayCollection{
			if (_listeProduits == null) _listeProduits = new ArrayCollection();
			return _listeProduits;
		}
		public function set listeProduits(liste : ArrayCollection):void{
			_listeProduits = liste;
		}
		
		private var _lignesSelectionnees : Array;
		public function set lignesSelectionees(lignes : Array):void{
			_lignesSelectionnees = lignes;
			getListeProduitsLigneSelectionee(_lignesSelectionnees);
		}
		public function get lignesSelectionees():Array{
			return _lignesSelectionnees;	
		}
		
		
		public function ProduitsDeLaLigneImpl()
		{
			super();
		}		
		
		///========= PROTECTED FUNCTION =====================================================
		protected override function commitProperties():void{
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			cboInventaire.addEventListener(ListEvent.CHANGE,cboInventaireChangeHandler);
			addEventListener(FlexEvent.CREATION_COMPLETE,produitsDeLaLigneImplCreationCompleteHandler);
		}
		
		
		///========= HANDLERS ===============================================================		
		protected function txtFiltreChangeHandler(ev : Event):void{
			filtrerListeProduits();
		}
		protected function cboInventaireChangeHandler(le : ListEvent):void{
			filtrerListeProduits();
		}
		protected function produitsDeLaLigneImplCreationCompleteHandler(fe : FlexEvent):void{
			
		}
		///========= FIN HANDLERS ===========================================================
		
		///======== FIN PROTECTED FUNCTION ==================================================
		
		///========== PROTECTED =============================================================
		
		///========== FIN PROTECTED =========================================================
		
		///========== PRIVATE ===============================================================
		//Liste des produits d'une ligne
		private function getListeProduitsLigneSelectionee(lignes : Array):void{		
			listeProduits = null;
			if (lignes.length > 0){
				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.inventaire.cycledevie.ElementDInventaire",
												  "getListeElementIventaire",
												  getListeProduitsLigneSelectioneeResultHandler);			  
				RemoteObjectUtil.callService(op,ConsoviewUtil.extractIDs("ligneID",lignes));	
			}
			 		
		}
		private function getListeProduitsLigneSelectioneeResultHandler(re : ResultEvent):void{
			
			listeProduits = re.result as ArrayCollection;
			if (initialized){
				filtrerListeProduits();
			}
		}
		
		
		private function filtrerListeProduits():void{
			if(listeProduits != null){
				listeProduits.filterFunction = filterFunc;
				listeProduits.refresh();
			}
		}
		
		//filtre sur les attributs SOUS_TETE, LIBELLE_PRODUIT du tableau des ressources des lignes selectionnées
		//voir la methode filterFunction d'un ArrayCollection
		private function filterFunc(item:Object):Boolean {
			var lettreMatch : Boolean = ((String(item.SOUS_TETE).toLowerCase().search(String(txtFiltre.text).toLowerCase()) != -1)  
				   				||
						 (String(item.LIBELLE_PRODUIT).toLowerCase().search(String(txtFiltre.text).toLowerCase()) != -1));
		
		    switch (cboInventaire.selectedIndex) {
		    	case 0: 
					if (lettreMatch && (Number(item.DANS_INVENTAIRE) == 1))return true
					else return false
		        case 1:
					if (lettreMatch && (Number(item.DANS_INVENTAIRE) == 0))return true
					else return false	
		        case 2: 
					if (lettreMatch && ((Number(item.DANS_INVENTAIRE) == 1)	|| (Number(item.DANS_INVENTAIRE) == 0)))return true					
					else return false
		        default: 
					return false;
			}
		}
	}
}