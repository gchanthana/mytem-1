package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	import flash.display.DisplayObject;
	import flash.events.IEventDispatcher;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	
	
	/**
	 * Interface permettant de manipiler les ecrans qui l'implemente (Nouvelle lignes et nouveaux produits)
	 * */
	public interface IDemande extends IEventDispatcher
	{	
		
		function get modeEcriture():Boolean;
		function set modeEcriture(mode : Boolean):void;
		
		/**
		 * Setter pour la commande que l'on souhaite afficher
		 * @param c la commande que l'on souhaite afficher
		 * */
		function setCommande(c : Commande):void;
		
		
		/**
		 * Appel la methode clean 
		 * Ne fait rien pour le moment
		 * */
		function clean():void;		
		
		
		/**
		 * Setter pour l'identifiant de la commande que l'on souhaite afficher
		 * @param idd l'identifiant de la commande
		 * */
		function set idDemande(idd : int):void;		
		
		
		/**
		 * Retourne l'identifiant de la commande
		 * @return idDemande l'identifiant de la commande
		 * */
		function get idDemande():int;			
		 
	}
}