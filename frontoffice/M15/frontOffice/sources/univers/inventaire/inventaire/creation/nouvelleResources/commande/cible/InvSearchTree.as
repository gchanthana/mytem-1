package univers.inventaire.inventaire.creation.nouvelleResources.commande.cible
{
	import composants.access.PerimetreTreeEvent;
	import composants.access.perimetre.PerimetreTreeDataDescriptor;
	import composants.access.perimetre.tree.CvPerimetreWindowIHM;
	import composants.access.perimetre.tree.NodeInfos;
	import composants.access.perimetre.tree.PerimetreTreeWindow;
	import composants.access.perimetre.tree.SearchPerimetreWindow;
	import composants.access.perimetre.tree.SearchPerimetreWindowImpl;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	/**
	 * Classe gerant les arbres du cycle de vie
	 * */
	public class InvSearchTree extends CvPerimetreWindowIHM
	{
		/**
		 * Constante definissant le type de l'évenement dispatché lors de la selection d'un noeud ou d'une feuille
		 * */
		public static const NODE_CHANGED : String = "nodeChanged";
		
		/**
		 * Constante definissant le type de l'évenement dispatché lorsque le noeud est pret pour être déroulé
		 * */
		public static const NODE_READY_EXP : String = "nodeReadyToBeExpanded";
		
		/**
		 * les infos sur le noeud sélectionné
		 * */
		[Bindable]
		public var nodeInfo : NodeInfos; 
		
		/**
		 * information sur le noeud selectionné de l'arbre des perimetre (pour le retour)
		 **/
		public var backedUpNodeInfo : NodeInfos;
		
		//Booléen placé à true si on a selecionné un feuille
		private var _isLeaF : Boolean = false;
		
		/**
		 * Retourne true quand on a selectionné un feuille
		 * @return isLeaf
		 * */
		public function get isLeaf():Boolean{
			return _isLeaF;
		}
		
		/**
		 * Constructeur
		 * */
		public function InvSearchTree()
		{
			 
			super();
			creationPolicy="all";
			loadDataOnSelection = true;
			
			
			addEventListener(FlexEvent.CREATION_COMPLETE,initCPS);
		}
		
		override protected function commitProperties():void{
			super.commitProperties();
			perimetreTree.loadDataOnSelection = loadDataOnSelection;
		}
		
		/**
		 * Initialise l' arbre avec le noeud choisit comme racine
		 * @param nodeId l'identifiant du noeud
		 * */
		public function initDP(nodeId : int):void{			
			if(CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_LOGIQUE == ConsoViewPerimetreObject.TYPE_ROOT) {
				//PerimetreTreeWindow(perimetreTree).perimetreTree.dataProvider = (XMLList(XML(CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList).descendants().(@NID == nodeId)))[0];
				var nodeliste:XMLList = (CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList).descendants().(@NID == nodeId);				
				 
				PerimetreTreeWindow(perimetreTree).perimetreTree.setDataProvider(nodeliste,"LBL",new PerimetreTreeDataDescriptor());
			} else {
				var xmlPerimetreData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;
				var resultList:XMLList =
					xmlPerimetreData.descendants().(@NID == CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
				if(resultList.length() > 0) {
					PerimetreTreeWindow(perimetreTree).perimetreTree.setDataProvider(resultList[0],"LBL",new PerimetreTreeDataDescriptor());
					
					//PerimetreTreeWindow(perimetreTree).perimetreTree.dataProvider = resultList[0];
				}
			}
		}
		
		//reference locale vers l'identifiant du noeud selectionné
		private var _nodeId : int;
		
		/**
		 * pour retrouver la cible  charge le chemin complet depuis la racine passée en paramètre jusqu'au noeud choisi
		 * @param rootId l'identifiant du noeud racine
		 * @parame nodeId l'identifiant du noeud ciblé
		 * 
		 **/
		public function getNodeXmlPathCible(rootId:int,nodeId:int):void {
			_nodeId = nodeId;
			var getPathOp:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													"fr.consotel.consoview.access.AccessManager",
													"getNodeXmlPath",getNodeXmlPathCibleResult);
			RemoteObjectUtil.callService(getPathOp,
					CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,rootId,nodeId);
		}
		
		/***
		* ResultHandler de getNodeXmlPath
		* Affiche le chemin complet et selectionne le noeud qui a ete choisi
		**/
		private function getNodeXmlPathCibleResult(event:ResultEvent):void {
			var currentPath : XML = event.result as XML;
			if(SearchPerimetreWindow(searchTree).initialized) {
				SearchPerimetreWindow(searchTree).searchTree.labelField = "@LBL";
				SearchPerimetreWindow(searchTree).searchTree.dataProvider = currentPath;	
				callLater(selectNodeById,[_nodeId]);
			}
		}
		
		
		/**
		 * pour retrouver la cible  charge le chemin complet depuis la racine passée en paramètre jusqu'au noeud choisi
		 * @param rootId l'identifiant du noeud racine
		 * @parame nodeId l'identifiant du noeud ciblé
		 * @event result  xml
		 **/
		public function getXmlPath(rootId:int,nodeId:int):void {
			_nodeId = nodeId;
			var getPathOp:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													"fr.consotel.consoview.inventaire.equipement.TreeFunction",
													"getXmlPath",getXmlPathResult);
			RemoteObjectUtil.callService(getPathOp,
					CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,rootId,nodeId);
		}
		
		/***
		* ResultHandler de getXmlPath
		* Affiche le chemin complet et selectionne le noeud qui a ete choisi
		**/
		private function getXmlPathResult(event:ResultEvent):void {
			var currentPath : XML;
			
			var result : ArrayCollection = event.result as ArrayCollection;
						
			currentPath = (formateResult(result[0]) as XML);
			
			var node : XML = new XML();
			
			for (var i : int = 1; i < result.length; i++){ 
				
				
				node = node.appendChild(formateResult(result[i] as XML));
				
									 
			}
			currentPath.appendChild(node);	
			
			

			
			if(SearchPerimetreWindow(searchTree).initialized) {
				SearchPerimetreWindow(searchTree).searchTree.dataProvider = currentPath;	
				callLater(selectNodeById,[_nodeId]);
			}
		}
 
		//formate un objet avec les bons attributs
		private function formateResult(obj : Object):Object{
			var newObject : Object = new Object();
			
			newObject.LBL = obj.LIBELLE_GROUPE_CLIENT;
			newObject.NID = obj.IDGROUPE_CLIENT
			newObject.NTY = obj.TYPE_NOEUD
			newObject.STC = obj.STC
			
			return newObject;			
		}
		
		
		//Selectionne un noeud de l'arbre
		//param in nodeId l'identifiant du noeud
		private function selectNodeById(nodeId : int):void{
			SearchPerimetreWindow(searchTree).searchTree.selectNodeById(nodeId);		
			dispatchEvent(new Event(NODE_READY_EXP));			
		}
		 
		//initialisation du composant 		
		private function initCPS(fe : FlexEvent):void{
			 
			SearchPerimetreWindow(searchTree).searchTree.addEventListener(ListEvent.CHANGE,loadNodeInfos);	
			SearchPerimetreWindow(searchTree).addEventListener(SearchPerimetreWindowImpl.NODE_SELECTED,nodeFinded);
			SearchPerimetreWindow(searchTree).addEventListener(ConsoViewDataEvent.BACK_NODE_SEARCH,backToPerimetreTree);
			SearchPerimetreWindow(searchTree).searchTree.dataDescriptor = new InvSearchTreeDataDescriptore(SearchPerimetreWindow(searchTree).searchTree);
			PerimetreTreeWindow(perimetreTree).perimetreTree.addEventListener(PerimetreTreeEvent.NODE_INFOS_RESULT,processNodeInfoBis);
		}
		 
		
		
		//charge les information sur un noeud
		private function loadNodeInfos(event:ListEvent):void {
			var clientID : Number = CvAccessManager.getSession().USER.CLIENTACCESSID;
			var loadNodeInfosOp:AbstractOperation =
				RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
										"fr.consotel.consoview.access.PerimetreManager",
										"getNodeInfos",processNodeInfos);
										
			if(SearchPerimetreWindow(searchTree).searchTree.selectedItem == null)// searchTree.searchTree.selectedIndex = 0;
				SearchPerimetreWindow(searchTree).searchTree.selectNodeById(CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX); 
				
			var perimetreIndex:int = SearchPerimetreWindow(searchTree).searchTree.selectedItem.@NID;
			
			if (perimetreIndex > 0){
				RemoteObjectUtil.callService(loadNodeInfosOp,clientID,perimetreIndex);
			} else{
				nodeInfo = null;
			}
		}
		
		//l'auto selection d' un noeud
		private function nodeFinded(ev : Event):void{
			loadNodeInfos(null);
		}
		
		//Met à jour les infos sur le noeud -- Provient l'arbre des recherches
		private function processNodeInfos(event:ResultEvent):void {
			nodeInfo = null;
			nodeInfo = new NodeInfos(event.result);
			
			_isLeaF = 
				(int(SearchPerimetreWindow(searchTree).searchTree.selectedItem.@NTY) == 0);
			
			dispatchEvent(new Event(InvSearchTree.NODE_CHANGED));
		}
		
		
		//Met à jour les infos sur le noeud -- Provient l'arbre des perimetre
		private function processNodeInfoBis(pte : PerimetreTreeEvent):void{
			nodeInfo = null;				
			nodeInfo = new NodeInfos(perimetreTree.nodeInfos);
			_isLeaF = 
				(int(PerimetreTreeWindow(perimetreTree).perimetreTree.selectedItem.@NTY) == 0);			
			backedUpNodeInfo = new NodeInfos(perimetreTree.nodeInfos);
			dispatchEvent(new Event(InvSearchTree.NODE_CHANGED));
		}
		
		
		private function backToPerimetreTree(ev : Event):void{
			processNodeInfoBis(null);
		}
	}
}