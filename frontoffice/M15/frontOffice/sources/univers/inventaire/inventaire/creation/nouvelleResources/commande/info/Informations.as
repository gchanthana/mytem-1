package univers.inventaire.inventaire.creation.nouvelleResources.commande.info
{
	import mx.events.FlexEvent;
	import composants.util.TextInputLabeled;
	import composants.util.CvDateChooser;
	import mx.containers.Canvas;
	import mx.controls.Label;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import flash.events.MouseEvent;
	import mx.events.CalendarLayoutChangeEvent;
	
	/**
	 * Classe gérant la partie information du formulaire de commande des nouvelles lignes
	 * ou de nouveaux produits
	 * */
	public class Informations extends Canvas
	{
		/**
		 * Reference vers le TextInputLabeled de saisie du libelle
		 * */
		public var txtLibelle : TextInputLabeled;
		
		/**
		 * Reference vers le TextInputLabeled de saisie de la reference client
		 * */
		public var txtReferenceCli : TextInputLabeled;
		
		/**
		 * Reference vers le TextInputLabeled de saisie de reference operateur
		 * */
		public var txtReferenceOpe : TextInputLabeled;		
		
		/**
		 * Reference vers le CvDateChooser de saisie de la date d'effet de la commande
		 * */
		public var dfDateEffet : CvDateChooser;
		
		/**
		 * Reference vers le CvDateChooser de saisie de la date de livraison porevue
		 * */
		public var dfdateLivraisonPrevue : CvDateChooser;

		/**
		 * Reference vers le TextInputLabeled de saisie de la commentaire client
		 * */
		public var txtCommentaire : TextInputLabeled;
		
		/**
		 * Reference vers le Label d'affichage du statut de la commande
		 * */
		public var lblStatut : Label;		
		
		//Reference locale vers la commande
		private var _commande : Commande;
		
		/**
		 * Constructeur
		 * */
		public function Informations()
		{	
			super();
		}
		
		/**
		 * Retourne le libelle saisi
		 * @retuurn text
		 * */
		public function get libelle():String{
			return txtLibelle.text;
		}
		
		
		/**
		 * Retourne la reference client saisie
		 * @retuurn text
		 * */
		public function get refClient():String{
			return txtReferenceCli.text;
		}
		
		
		/**
		 * Retourne la reference opérateur saisie
		 * @retuurn text
		 * */
		public function get refOperateur():String{
			return txtReferenceOpe.text;
		}
		
		
		
		/**
		 * Retourne la date de livraison au format jj/mm/aaaa
		 * @retuurn text
		 * */
		public function get dateLivraisonPrevue():String{
			return dfdateLivraisonPrevue.text;
		}
		
		
		/**
		 * Retourne la date de livraison au format jj/mm/aaaa
		 * @retuurn text
		 * */
		public function get dateEffective():String{
			return dfDateEffet.text;
		}
		
		/**
		 * Retourne le commentaire saisi
		 * @retuurn text
		 * */
		public function get commentaire():String{
			return txtCommentaire.text;
		}
				
		/**
		 * Affecte la commande
		 * @param cmde, la commande
		 * */				
		public function affecterCommande(cmde : Commande):void{
		 	_commande = cmde;		 	
		 	_commande.libelle = libelle;
		 	_commande.refClient = refClient;
		 	_commande.refOperateur = refOperateur;
		 	_commande.statutID = StatuTInfo.EN_ATTENTE_ENVOI;
		 	_commande.dateEffective = dateEffective;
		 	_commande.dateLivraisonPrevue = dateLivraisonPrevue;
		 	_commande.commentaire = commentaire ;	
		}
		
		/**
		 * Definit la plage de selection pour le calendrier  
		 * */
		protected function setDateLivraisonRange(clce : CalendarLayoutChangeEvent):void{
			var selectableRange : Object = new Object();
			selectableRange.rangeStart = dfDateEffet.selectedDate;	
			dfdateLivraisonPrevue.selectableRange = selectableRange;
		}
	}
}