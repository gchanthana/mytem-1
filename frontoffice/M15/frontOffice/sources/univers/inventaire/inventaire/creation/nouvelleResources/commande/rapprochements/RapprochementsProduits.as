package univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements
{
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	
	
	/**
	 * Classe gérant la fenêtre de rapprochement
	 * */
	 [Bindable]
	public class RapprochementsProduits extends TitleWindow
	{
		//Constante référençant le nom de la classe distante gerant les rapprochements
		private const COLDFUSION_COMPONENT : String = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.GestionRapprochement";
		
		//Constante référençant le nom de la méthode ramenant le libelle du noeud cible
		private const COLDFUSION_GETCIBLELIBELLE : String = "getCibleLibelle";		
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la sortie de la fenetre
		 * Click sur Annuler ou sur la croix
		 * */		
		public static const SORTIR : String = "sortirRapprochement";
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la validation du rapprochement
		 * */
		public static const VALIDER : String = "rapprochementValide";
		
		/**
		 * Constante definissant le type d'evenement dispatché lors d'un rapprochement de produit
		 * */
		public static const UPDATE_PRODUITS_RAPPROCHER : String = "rapprochementLigne";		
		
		
		/**
		 * Référence vers le TextInput qui affiche l'operateur de la commande
		 * */
		public var txtOperateur : TextInput;
		
		/**
		 * Référence vers le TextInput qui affiche le libellé du noeud cible 
		 * */
		public var txtCible : TextInput;
		
		/**
		 * Référence vers la section gérant les rapprochements 
		 * */
		public var sectionRapprochement : SectionRapprochementIHM;
		
		/**
		 * Référence vers la section gérant les affectattions
		 * */
		public var sectionAffecterLigne : SectionAffectationIHM;
		
		
		
		//Référence locale vers la commande		
		private var _commande : Commande;
		/* public function get commande(): Commande
		{
			return (_commande != null)?_commande:new Commande();	
		} */
		
		//Référence locale vers le panier de la commande
		private var _panier : ElementCommande;	
		
		//Collection contenant la liste des lignes rapprochées
		private var ligneRapproche : ArrayCollection = new ArrayCollection();
		
		//Reference vers la méthode distante charger de ramener le libelle du noeud cible
		private var opCibleLibelle  : AbstractOperation;
		
		
		public function get dateLivraison():String{
			
			return _commande.dateLivraison != null ? DateFunction.formatDateAsString(new Date( _commande.dateLivraison)) : "------";
		}
		
		
		/**
		 * Constructeur
		 * */
		public function RapprochementsProduits()
		{
			super();
		}
		
		/**
		 * Ne fait rien pour le moment
		 * */
		public function clean():void{
			
		}
		
		/**
		 * Affecte la commande et le panier 
		 * @param commande, la commande
		 * @param panier le panier de la commande
		 * */
		public function setCommande(commande : Commande,panier : ElementCommande):void{
			_commande = commande;
			_panier = panier;
		};
		
		
		
		/**
		 * Sort de la commande
		 * Annule les rapprochements
		 * Dispatche un évenement de type 'RapprochementsProduits.SORTIR' qignifiant que l'on sort du rapprochement
		 * Ferme la fenêtre
		 * */		
		protected function sortir():void{
			annuler();
		}
		
		
		/**
		 * Initialisation de l'IHM
		 **/ 
		protected function init():void{			
			getCibleLibelle(_commande.idGroupeCible);
			txtOperateur.text = _commande.operateurNom;						
		 
		 	sectionRapprochement.setCommande(_commande,_panier);
		 	sectionRapprochement.addEventListener(SectionRapprochement.RAPPROCHER,rapprochementHandler);	
		 	sectionRapprochement.addEventListener(SectionRapprochement.ANNULER_RAPPROCHEMENT,annulerRapprochementHandler);	
		 	
		 	if (_commande.typeCommande == Commande.NOUVEAUX_PRODUITS_SUR_LIGNES_EX){
		 		sectionAffecterLigne.visible = false;
		 		sectionAffecterLigne.height = 0;
		 	}else{
		 		sectionAffecterLigne.setCommande(_commande,_panier);	
		 	}
		 	
		 	executeBindings(true);
		}
		
		//Annule les rapprochements
		private function annuler():void{
			ConsoviewAlert.afficherAlertConfirmation("Êtes vous sur de vouloir annuler ce rapprochement","Demande de confirmation",annulerRapprochementConfirmHandler);
		}
		
		private function annulerRapprochementConfirmHandler(ce : CloseEvent):void{
			if (ce.detail == Alert.OK){
				sectionRapprochement.annuler();	
				clean();
				dispatchEvent(new Event(RapprochementsProduits.SORTIR));
			}
		}
		
			
		
		//Remplir le tableau du bas avec la ligne rapproche		
		private function rapprochementHandler(ev : Event):void{							
			sectionAffecterLigne.fillGrid(ElementCommande(ev.currentTarget.produitRapproche));		
		}
		

		//Supprimer les lignes rapprochées si annulation		
		private function annulerRapprochementHandler(ev : Event):void{	
			if (ev.currentTarget.produitRapprocheAnnule != null)		
			if (sectionAffecterLigne != null) sectionAffecterLigne.deleteLigneInGrid(ev.currentTarget.produitRapprocheAnnule);
		}
				
		///navigation ---------------------------------------------
		
		/**
		 * Gere le click sur le bouton 'Valider le rapprochement'
		 * 
		 * */
		protected function valider(me : MouseEvent):void{
			trace("validerAvecVerfication");
			var message : String = verifierRapprochement();
			if (message.length > 0)Alert.show(message,"Erreur");
			else{
				ConsoviewAlert.afficherAlertConfirmation("Cette action est irreversible.\n Vous ne pourrez plus annuler le rapprochement. Etes vous sur de vouloir continuer?","Confirmation",validerConfirmationHandler);
			}
		}
		
		//confirmation
		private function validerConfirmationHandler(event : CloseEvent):void{
			if (event.detail == Alert.OK){
				validerAffectation();		
			}
		}
		
		//Verifie la validité du formulaire
		private function verifierRapprochement():String{						
			var boolRap : Boolean = false;
			var tabProduistRapp : Array = (sectionRapprochement.grdPdtsDemande.dataProvider as ArrayCollection).source;
			
			for (var i : int = 0; i < tabProduistRapp.length; i++){				
				boolRap = (ElementCommande(tabProduistRapp[i]).flagRapproche == 1);
				if (!boolRap) break;
			}
			 
			var message : String = "\n";			
			if (!boolRap){
				message = message + "- Vous devez rapprocher l'enssemble des produits de la commande";				
			}
			
			
			if (boolRap){
				return "";
			}else{
				return message;				
			}
		}
		
		//Dans la procedure de cloture d'une commande, valide les affectations
		private function validerAffectation():void{
			sectionAffecterLigne.addEventListener(SectionAffectation.LIGNES_AFFECTEES,affectationsOkHandler);
			sectionAffecterLigne.validerAffectations();
		}
		
		//Dans la procedure de cloture d'une commande, créé un opération de Commande
		private function affectationsOkHandler(ev : Event):void{
			_panier.addEventListener(ElementCommande.RAPPROCHER_PRODUITS_COMMANDE_ERROR,rapprocherProduitCommandeErrorHandler);
			_panier.addEventListener(ElementCommande.RAPPROCHER_PRODUITS_COMMANDE_COMPLETE,rapprocherProduitCommandeCompleteHandler);
			_panier.validerRapprochementCommande();
		}
		private function rapprocherProduitCommandeErrorHandler(event : Event):void{
			Alert.show("Erreur lors de la validation");
		}
		private function rapprocherProduitCommandeCompleteHandler(event : Event):void{
			dispatchEvent(new Event(RapprochementsProduits.VALIDER));	
		} 
		//----REMOTING --------------
		
		//Obtenir le libelle de la cible 
		private function getCibleLibelle(idcible : int):void{
			opCibleLibelle = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				COLDFUSION_COMPONENT,
																				COLDFUSION_GETCIBLELIBELLE,
																				getCibleLibelleResultHandler,
																				null);				

			RemoteObjectUtil.callService(opCibleLibelle,idcible);																					
		}
		
		//Handler de la méthode getCibleLibelle, affiche le libelle du noeud cible
		private function getCibleLibelleResultHandler(re : ResultEvent):void{
				if (String(re.result).length > 0){
					txtCible.text = String(re.result);
					
				}else{
					txtCible.text = "---------------";
				}		
		}
		
	}
}