package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import mx.containers.Canvas;
	import mx.events.FlexEvent;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.selectionDesProduits.PanelDemandeEvent;
	
	
	/**
	 * Classe containeur des ecrans de type Commande (IDemande)
	 * */
	public class PanelNouveauxProduits extends PanelNouveauxProduitsIHM
	{
		/**
		 * Constante definissant le type 'demandeCreated' pour un evenement
		 * */
		public static const DEMANDE_CREEE : String  = "demandeCreated";
		
		/**
		 * Constante definissant le type 'demandeUpdated' pour un evenement
		 * */
		public static const DEMANDE_UPDATER : String = "demandeUpdated";
		
		/**
		 * Constante definissant le type 'demandeExit' pour un evenement
		 * */
		public static const SORTIR : String = "demandeExit";
		
		/**
		 * Constante definissant le type 'opDemandeCreated' pour un evenement
		 * */
		public static const OPERATION_CREE : String = "opDemandeCreated";
		
		
		//Refernece vers l'ecran affiché 
		private var demande : IDemande;
		
		//Refernece vers le type d'ecran a afficher
		private var typeop : int = DemandeFactory.CODE_NOUVELLES_LIGNES;
		
		//Refernece vers l'identifiant de l'operation à afficher
		private var idop : int;	
		
		//Refernece vers la commande à afficher
		private var _commande : Commande;
		
		/**
		 * Retourne la commande qui est affichée
		 * @return _commande, la commande affichée
		 * */
		public function get commande():Commande{
			return _commande;
		} 
		
		/**
		 * Constructeur
		 * @param le code_ihm de l'ecran à afficher
		 * @param l'identifiant de la commande 
		 * */		
		public function PanelNouveauxProduits(type : int = 1,ido : int = 0)
		{	
			super();
			typeop = type;
			idop = ido;									
						
			demande = DemandeFactory.createDemande(type,ido);	
			demande.modeEcriture = (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE);	
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);		
		}
		
		public function clean():void{
			if (demande != null) demande.clean();
			demande = null;
		}
		
		//Initialisation de l'ecran affiché
		//Affectation des écouteurs d'évenement
		//affichage du titre 
		private function initDemande():void{
			demande.addEventListener(PanelDemandeEvent.SORTIR,sortirDeLaDemande);		
			demande.addEventListener(PanelDemandeEvent.SAVE_COMPLETE,rafraichirGridDemandes);
			demande.addEventListener(PanelDemandeEvent.UPDATE_COMPLETE,rafraichirGridDemandes);
			demande.addEventListener(PanelDemandeEvent.FERMER,rafraichirGridDemandes);	
			title = Canvas(demande).label;
		}
		
		
		//Initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{			
			demande.idDemande = idop;
			addChild(DisplayObject(demande));							
			initDemande();
		}	
		
		//Dispatche les evenements signifiant que la commande à été :
		//enregistrer, mis à jour ou fermer
		private function rafraichirGridDemandes(ev : PanelDemandeEvent):void{
			switch(ev.type){
				case PanelDemandeEvent.SAVE_COMPLETE : {
					removeAllChildren();
					
					_commande = ev.commande;
					
					demande = null;
				
					demande = DemandeFactory.createDemande(_commande.typeCommande,_commande.commandeID);
					demande.modeEcriture = (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE);
					initDemande();
					demande.setCommande(_commande);
					addChild(DisplayObject(demande));				
					dispatchEvent(new Event(PanelNouveauxProduits.DEMANDE_CREEE));
					break;				
				}
				case PanelDemandeEvent.UPDATE_COMPLETE : {
					dispatchEvent(new Event(PanelNouveauxProduits.DEMANDE_UPDATER));
					break;
				}
				case PanelDemandeEvent.FERMER : {
					_commande = ev.commande;					
					dispatchEvent(new Event(PanelNouveauxProduits.OPERATION_CREE));					
					break;									
				}
			}
		}	
		
		//Dispatch un évenement signifiant que l'on souhaite sortir du module
		private function sortirDeLaDemande(ev : Event):void{
			dispatchEvent(new Event(PanelNouveauxProduits.SORTIR));
		}
	}
}