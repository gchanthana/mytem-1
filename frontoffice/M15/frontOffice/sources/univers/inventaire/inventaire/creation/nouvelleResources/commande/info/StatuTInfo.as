package univers.inventaire.inventaire.creation.nouvelleResources.commande.info
{
	/**
	 * Classe proposant les constantes qui definissent les identifiants des différents états d'une commande
	 * */
	public class StatuTInfo
	{
		/**
		 * Identifiant de l'état en attente d'envoi
		 * */
		public static const  EN_ATTENTE_ENVOI : int = 1;
		
		/**
		 * Identifiant de l'état en attente de livraison
		 * */
		public static const  EN_ATTENTE_LIVRAISON : int = 2;
		
		
		/**
		 * Identifiant de l'état en attente de décision de contrôle de la facturation
		 * */
		public static const  EN_ATTENTE_DECISION_CTRL : int = 3;
		
		
		/**
		 * Identifiant de l'état rapproché
		 * */
		public static const  RAPPROCHEE : int = 4;
		
		/**
		 * Identifiant de l'état annulée apres envoi de la commande
		 * */
		public static const  COMMANDE_ANNULEE_APRES_ENVOI : int = 23;
		
		
		/**
		 * Identifiant de l'état annulée avant envoi de la commande
		 * */
		public static const  COMMANDE_ANNULEE_AVANT_ENVOI : int = 22;
		
		
		
		/**
		 * Identifiant de l'état en attente rapprochement
		 * */
		public static const EN_ATTENTE_RAPPROCHEMENT : int = 1022;
	}
}