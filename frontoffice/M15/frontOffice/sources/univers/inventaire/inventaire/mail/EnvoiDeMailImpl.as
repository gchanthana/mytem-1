package univers.inventaire.inventaire.mail
{
	import mx.containers.TitleWindow;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Contact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts.GestionContactsIHM;
	import mx.controls.Button;
	import flash.events.MouseEvent;
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	import mx.events.CloseEvent;
	import flash.events.Event;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.ResultEvent;
	import mx.events.ListEvent;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts.GestionContacts;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.etapes.OperationVO;

	public class EnvoiDeMailImpl extends TitleWindow
	{
		/**
		 * ref vers le composant gestion de contact
		 * */
		public var gestionContact : GestionContactsIHM;
		
		/**
		 * ref vers le bouton envoyer
		 * */
		public var btEnvoyer : Button;
		
		
		/**
		 * constante definissant l'envenment Mail Envoyer
		 * */
		public static const MAIL_ENVOYER_EVENT : String = "mailEvoyer";
		
		private var _commande : Commande;
		private var _operartion : OperationVO;		
		private var _agence : Societe;
		private var _distributeur : Societe;
		private var _contact : Contact;
		private var _operateur : OperateurVO;
	 
				
		//--- SETTER/GETTER -----
		
		public function set operation(op : OperationVO):void{
			_operartion = op;
		}		
		public function set commande(cmd : Commande):void{
			_commande = cmd;
		}
		
		public function set agence(a : Societe):void{
			_agence = a;	
		}		
		public function set distributeur(d : Societe):void{
			_distributeur = d;
		}
		public function set contact(c : Contact):void{
			_contact = c;
		}
		public function set operateur(op : OperateurVO):void{
			_operateur = op;
		}
		
		public function get agence():Societe{
			return _agence;
		}		
		public function get distributeur():Societe{
			return _distributeur;
		}
		public function get contact():Contact{
			return _contact;
		}
		public function get operateur():OperateurVO{
			return _operateur;
		}
		
		
		
		/**
		 * Constructeur
		 * */
		public function EnvoiDeMailImpl()
		{	
			super();
		}
		
		/**
		 * supprime la fenêtre au logoff
		 * (fait le menage)
		 * */
		 public function logOff():void{
		 	_commande = null;
			_operartion = null;		
			_agence = null;
			_distributeur = null;
			_contact = null;
			_operateur = null;
		 	PopUpManager.removePopUp(this);
		 }
		
		//-- Handlers
		
		//Handler du click sur le bouton envoyer
		//param in
		//MouseEvent
		protected function btEnvoyerClickHandler(me : MouseEvent):void{	
			if (_commande){
				envoyerMail("cmd",_commande.commandeID);	
			}else if (_operartion){
				envoyerMail("ope",_operartion.IDINV_OPERATIONS);	
				
				/* switch(_operartion.IDINV_TYPE_OPE){
					case INVTYPE_OPERATION.COMMANDE:envoyerMail("opeCmde",_operartion.IDINV_OPERATIONS);break;
					case INVTYPE_OPERATION.RESILIATION:envoyerMail("opeResi",_operartion.IDINV_OPERATIONS);break;
				}*/
			}
		}
		
		//Handler du click sur le bouton fermer
		//param in
		//MouseEvent
		protected function btCloseClickHandler(me : MouseEvent):void{			
			closeWindow();
		}
		
		//Handler du click sur la croix de fermeture
		//param in
		//CloseEvent
		protected function iconCloseClickHandler(ce : CloseEvent):void{
			closeWindow();
		}
		
		
		//Handler du controle contact
		//param in
		//ListEvent
		private function contactChangeHandler(le : ListEvent):void{
			
			if (gestionContact.getContact() > 0){
				btEnvoyer.enabled = true;
			}else{
				btEnvoyer.enabled = false;
			}
			
		}
		
		//-- protected
		
		//Initialisation du composant
		protected function init():void{
			gestionContact.lblTitre.text = "Distributeur";	
			gestionContact.cmbAgence.width = 200;
			gestionContact.cmbContact.width = 200;
			gestionContact.cmbDistributeur.width = 200;
			
			
			gestionContact.addEventListener(GestionContacts.COMBO_SOCIETE_READY,autoSelectionnerContact);
			gestionContact.cmbContact.addEventListener(ListEvent.CHANGE,contactChangeHandler);
			title = "Envoyer un mail avec bon de commande";
		}
		
		
		//---privée
		
		//ferme la fentre
		private function closeWindow():void{
			PopUpManager.removePopUp(this);
		}
		
		//selectionne le contact dans la combo
		private function autoSelectionnerContact(evt : Event):void{
			if (distributeur){
				gestionContact.autoSelectContact(_distributeur,_agence,_contact); 
			}else if(agence){
				gestionContact.autoSelectContact(agence,null,_contact); 
			}
		}
		
		
		
		//----- Remotings --------------------------------------------------------------------------------------------------------------------
		
		//Envoi le mail 
		//param in idOperation
		private function envoyerMail(type : String, idop : Number):void{
			var composant : String;
			if (type == "cmd") composant = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande";
			else if (type == "ope") composant = "fr.consotel.consoview.inventaire.cycledevie.OperationAction";
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					composant,
																					"updateContactEnvoyerMail",
																					envoyerMailResult);		
			RemoteObjectUtil.callService(op,idop,gestionContact.getContact(),gestionContact.getSociete());		 			
		}
		
		//Envoyer mail result
		//param in
		//ResultEvent
		private function envoyerMailResult(re : ResultEvent):void{
			if (re.result > 0){
				dispatchEvent(new Event(EnvoiDeMailImpl.MAIL_ENVOYER_EVENT));
				if (_commande)
					_commande.refresh();			
				else if (_operartion){
					_operartion.refresh();
				}	
				closeWindow();
			}else{
				Alert.show("Une erreur est survenue");
			}
		}
	}
}