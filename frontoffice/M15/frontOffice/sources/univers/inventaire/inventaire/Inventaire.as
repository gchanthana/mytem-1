package univers.inventaire.inventaire
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.getQualifiedClassName;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.ChildExistenceChangedEvent;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.UniversFunctionItem;
	
	import univers.inventaire.inventaire.creation.CreationOperation;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.PanelNouveauxProduits;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.operationresiliation.EnregistrerOperationEvent;
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;
	import univers.inventaire.inventaire.etapes.CloturerOperationEvent;
	import univers.inventaire.inventaire.etapes.EtapesOperation;
	import univers.inventaire.inventaire.etapes.EtapesOperationCreation;
	import univers.inventaire.inventaire.etapes.EtapesOperationRapprochements;
	import univers.inventaire.inventaire.etapes.EtapesOperationReclam;
	import univers.inventaire.inventaire.journaux.AfficherJournalEvent;
	import univers.inventaire.inventaire.journaux.JournauxMain;
	import univers.inventaire.inventaire.journaux.SelectionOperationEvent;
	import univers.inventaire.inventaire.menu.OperationMenuEvent;
	import univers.inventaire.inventaire.menu.RechercheMenuEvent;
	import univers.inventaire.inventaire.recherche.PanelResilierProduit;
	
	
	/**
	 * Class permettant de faire la liaison entre les differents écrans du cycle de vie. 
	 * Les ecrans sont estancies, affiches ou detruits ici.
	 * 
	 * */
	public class Inventaire extends InventaireIHM implements UniversFunctionItem
	{	
		
		//Reference vers les ecrans des operations de Resiliation (affichage de l historique des actions pour une operation : WorkFlow)
		protected var myMainOpe:EtapesOperation;
		
		//Reference vers l ecran des operations Verification
		protected var myMainOpeRap:EtapesOperationRapprochements;
		
		//Reference vers l ecran des operations de Reclamation
		protected var myMainOpeReclam:EtapesOperationReclam;
		
		//Reference vers l ecran des operations de Creation 
		protected var myMainOpeCrea : EtapesOperationCreation;
		
		//Reference vers l ecran des Commandes (Nouveux produits et Nouvelles Lignes)
		protected var myPanelNouveauxProduits : PanelNouveauxProduits;
		
		//Reference vers l ecran contenant les Journeaux
		protected var myMainJournaux:JournauxMain = new JournauxMain();
		
		//Reference vers l ecran de creation des opérations (Résiliation)
		protected var myMainCreate:CreationOperation;
		
		//Reference vers l ecran de Gestion direct
		protected var myRechercheResi : PanelResilierProduit;	 
			
		//Booleen permettant de savoir si l ecran de creation d'operation est affiche
		private var myMainCreateDisplayed : Boolean = false;		
		
		//Booleen permettant de savoir si l ecran de Verification est affiche
		private var myMainOpeRapDisplayed: Boolean = false;			
		
		//Booleen permettant de savoir si l ecran de Reclamation est affiche
		private var myMainOpeReclamDisplayed : Boolean = false;

		//Reference vers l Operation affiche (WorkFlow)
		private var displayedOpPanel : DisplayObject;
		
		//Reference vers le journal affiche
		private var displayedJrnxPanel : DisplayObject;
		
		//Reference vers l'ecran de gestion direct affiche
		private var displayedRchePanel : DisplayObject;
		
		//Booleen permettant de savoir si un WorkFlow etait affihce
		private var wasDisplayedOpPanel : Boolean = false;

		//Booleen permettant de savoir si un Journal etait affihce
		private var wasDisplayedJrnxPanel : Boolean = false;
		
		//Booleen permettant de savoir si la gestion direct etait affihce		
		private var wasDisplayedRchePanel : Boolean = false;
		
		//Booleen permettant de savoir si on auto-affiche l operation selectionne 
		private var autoSelectOperation : Boolean = false;
		
		//Une reference vres l'identifiant de l'operation a afficher
		private var idOpCree : int;
		//----------------------------------------------------------
	 	
	 	
	 	/**
	 	 * Constructeur de la Class
	 	 * */	
		public function Inventaire()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);			
		}
		
		/**
		 * Methode a lancer lors d un changement de  perimetre.
		 * On lance la methode onPerimetreChange pour tous les ecrans du cycle de vie.
		 * On détruuit les ecrans sauf celui des journaux. Ce dernier met a jour ses données pour le nouveau perimetre.
		 * */
		public function onPerimetreChange():void{
			
			try{
				trace("[PERIMETRE CHANGE] -- MENU OPERATION");
				myOpMenu.onPerimetreChange();
			}catch(e : Error){
				trace("wasDisplayedOpPanel " + wasDisplayedOpPanel);
			}
			
			try{
				trace("[PERIMETRE CHANGE] -- MENU RECHERCHE");
				myRechercheMenu.onPerimetreChange();
			}catch(e : Error){
				trace("myRechercheMenu pas encore initialisé");
			}
			
			
			try{
				trace("[PERIMETRE CHANGE] -- Journaux Menu");
				myJournauxMenu.onPerimetreChange();			
			}catch(e : Error){
				trace("myJournauxMenu pas encore initialisé");	
			}
			
			
			try{
				trace("[PERIMETRE CHANGE] -- Journaux");
				myMainJournaux.onPerimetreChange();
			}catch(e : Error){					
				trace("myMainJournaux pas encore initialisé");
			}
			
			if (myMainBox.getChildren().length>0){
				myMainBox.removeAllChildren();
				wasDisplayedOpPanel = false;
				wasDisplayedRchePanel = false;
				myMainCreateDisplayed = false;
				wasDisplayedJrnxPanel = false;
			}
			
			
			/* if (wasDisplayedOpPanel || wasDisplayedRchePanel || myMainCreateDisplayed){
				if (myMainBox.getChildren().length>0){
					myMainBox.removeAllChildren();
				}
				wasDisplayedOpPanel = false;
				wasDisplayedRchePanel = false;
				myMainCreateDisplayed = false;
				wasDisplayedJrnxPanel = false;
			}else{
				
			} */			
		}
		
		
		/**
		 * Méthode à lancer lors de la déconnexion
		 * */
		public function logOff():void{
			if (myMainOpe != null) myMainOpe.clean();
			myMainOpe  = null;
			if (myMainOpeCrea != null) myMainOpeCrea.clean();
			myMainOpeCrea = null;
			if (myMainOpeRap != null) myMainOpeRap.clean();
			myMainOpeRap = null;
			if (myMainOpeReclam != null) myMainOpeReclam.clean();
			myMainOpeReclam = null;
		}
		
		//initialisation de l'ecran apres l'evenement 'creationcomplete'
		//On ajoute les 'ecouteurs d evenement' pour chaque onglet du cycle de vie 		
		private function initIHM(event:Event):void {						
			/*------*/
			lbMenu.addEventListener(MenuEvent.ITEM_CLICK,switchMenu);					
			/*-- operation ----*/
			myOpMenu.addEventListener(OperationMenuEvent.SHOW_SUIVIT_OPERATION,displayOpePanel);			
			myOpMenu.addEventListener(OperationMenuEvent.SHOW_NOUVELLES_DEMANDE,displayedDemande);
			myOpMenu.addEventListener(OperationMenuEvent.SHOW_DEMANDE,displayedDemande);
			
			//myOpMenu.addEventListener(OperationMenuEvent.DEMANDE_DELETED,effacerDemande);
			
			myOpMenu.addEventListener("GridsOK",girOpestOk);
			
			
			/*-- Journaux ---*/
			myJournauxMenu.addEventListener(AfficherJournalEvent.AFFICHER_JOURAUX,displayMenuPanel);
			myMainJournaux.addEventListener(SelectionOperationEvent.OPERATION_SELECTED,goToSelectedOpreation);		
			/*--- Recherche ---*/
			myRechercheMenu.addEventListener("GlSelectionne",displayRechercheResi);	
			myRechercheMenu.addEventListener("ResetGDirect",resetRechercheResi);	
					
		}
		
		//Switche entre les differents onglet du cycle de vie.(Attend un evenement)
		//Pour chaque onglet on regarde si un ecran etait affiche avant le changement d'onglet.
		//Si oui on le re-affiche, si non on en affiche un nouveau.
		private function switchMenu(event:Event):void {
			if (myMainBox.getChildren().length>0){
				myMainBox.removeAllChildren();
			}
			
			switch (vsMenu.selectedChild) {
				case myOpMenu:	
					if (myMainCreateDisplayed){
						myMainBox.addChild(myMainCreate);	
						myOpMenu.disablePanel();
					}else if (wasDisplayedOpPanel){
						
						myMainBox.addChild(displayedOpPanel);	
						myOpMenu.disablePanel();
						
					}else{
						myOpMenu.enabledPanel();
					}						
					break;			
				case myJournauxMenu:
					if (wasDisplayedJrnxPanel){
						myMainBox.addChild(displayedJrnxPanel);							
					}	
					break;	
				case myRechercheMenu:
					if (wasDisplayedRchePanel){
						myMainBox.addChild(displayedRchePanel);							
					}	
					break;					
			} 
		}
		
		//Affiche l'ecran des formulaires de commande (nouvelles lignes ou nouveaux produits)
		//Le choix se fait suivant les parametre de l'evenement OperationMenuEvent
		//Voir la class OperationMenuEvent pour les parametres à rensigner
		private function displayedDemande(pme : OperationMenuEvent):void{
			 	
			if (myMainBox.getChildren().length>0){
				myMainBox.removeAllChildren();
				wasDisplayedOpPanel = false;
				myMainCreateDisplayed = false;
				displayedOpPanel = null;
			}
			
			myPanelNouveauxProduits = new PanelNouveauxProduits(pme.type_Operation,pme.id_Operation);		
			myPanelNouveauxProduits.addEventListener(PanelNouveauxProduits.DEMANDE_CREEE,sortirDemandeApresEnregistrement);	
			myPanelNouveauxProduits.addEventListener(PanelNouveauxProduits.DEMANDE_UPDATER,sortirDemandeApresEnregistrement);						
			myPanelNouveauxProduits.addEventListener(PanelNouveauxProduits.SORTIR,sortir);				
			myPanelNouveauxProduits.addEventListener(PanelNouveauxProduits.OPERATION_CREE,sortirDemandeApresCreation);
			myMainBox.addChild(myPanelNouveauxProduits);
			wasDisplayedOpPanel = true;
			displayedOpPanel = myPanelNouveauxProduits;		
			myOpMenu.disablePanel();
		}
		
		
		/* private function effacerDemande(ome : OperationMenuEvent):void{								
							
			if ((myMainBox.getChildren()[0] == myPanelNouveauxProduits)
				&&
				(OperationMenuEvent(ome).isDemande))
			{					
				myMainBox.removeAllChildren();							
				myMainCreateDisplayed = false;
				wasDisplayedOpPanel = false;
				displayedOpPanel = null;	
			}			
			myOpMenu.mettreAJourGridOperation();	
		} */
		
		
		//affiche une operation (Resiliation, creation, verification ou reclamation) suivant sont type
		//le type est renseigne par l'evenement OperationMenuEvent attendu par cette methode
		private function displayOpePanel( ome : OperationMenuEvent):void {
			
			//Si il y avait une operation affichee on la retire 	
			if (myMainBox.getChildren().length>0){
				myMainBox.removeAllChildren();
				wasDisplayedOpPanel = false;
				myMainCreateDisplayed = false;
				displayedOpPanel = null;
			}
			
			//On estancie la bonne operation puis on l'affiche
			switch(ome.type_Operation){
				
				//WorkFlow Resiliation
				case 1 : {
					
					myMainOpe = new EtapesOperation(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);					
					myMainOpe.addEventListener("EnregistrerOperationRapprochement",enregistrerOPRapprochement);
					myMainOpe.addEventListener("SortirSuivitOperationResi",sortir);
					myMainOpe.addEventListener("mettreAjourGrid",refreshGrid);						
					myMainBox.addChild(myMainOpe);
					wasDisplayedOpPanel = true;
					displayedOpPanel = myMainOpe;		
					myOpMenu.disablePanel();
					break;
				}
				//WorkFlow Creation
				case 2 : {
					myMainOpeCrea = new EtapesOperationCreation(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);					
					myMainOpeCrea.addEventListener("EnregistrerOperationRapprochement",enregistrerOPRapprochement);
					myMainOpeCrea.addEventListener("SortirSuivitOperationCrea",sortir);	
					myMainOpeCrea.addEventListener("mettreAjourGrid",refreshGrid);								
					myMainBox.addChild(myMainOpeCrea);
					wasDisplayedOpPanel = true;
					displayedOpPanel = myMainOpeCrea;						
					myOpMenu.disablePanel();
					break	
				}
				
				//Verification de la facturation apres résiliation				
				case 41 : {
					myMainOpeRap = new EtapesOperationRapprochements(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);
					myMainOpeRap.addEventListener("EnregistrerOperationReclam",enregistrerOperationReclam);
					myMainOpeRap.addEventListener("SortirSuivitOperationRapp",sortir);
					myMainOpeRap.addEventListener("CloturerOperationRapp",sortir);
					myMainBox.addChild(myMainOpeRap);
					wasDisplayedOpPanel = true;
					displayedOpPanel = myMainOpeRap;	
					myOpMenu.disablePanel(); 
					break;
				}
				
				//Verification de la facturation apres commande			
				case 83 : {
					myMainOpeRap = new EtapesOperationRapprochements(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);
					myMainOpeRap.addEventListener("EnregistrerOperationReclam",enregistrerOperationReclam);
					myMainOpeRap.addEventListener("SortirSuivitOperationRapp",sortir);
					myMainOpeRap.addEventListener("CloturerOperationRapp",sortir);
					myMainBox.addChild(myMainOpeRap);
					wasDisplayedOpPanel = true;
					displayedOpPanel = myMainOpeRap;	
					myOpMenu.disablePanel(); 
					break;
				}
				
				
				//Reclamation d'une résiliation
				case 61 : {					
					myMainOpeReclam = new EtapesOperationReclam(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);				
				  	myMainOpeReclam.addEventListener("SortirSuivitOperationReclam",sortir);	
			  		myMainOpeReclam.addEventListener("CloturerOperationReclamation",sortirApresClotureReclamation);						  	
					myMainBox.addChild(myMainOpeReclam);
					wasDisplayedOpPanel = true;
					displayedOpPanel = myMainOpeReclam;	
					myOpMenu.disablePanel(); 					
					break	
				}	
				
				//Reclamation d'une commande
				case 83 : {					
					myMainOpeReclam = new EtapesOperationReclam(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);				
				  	myMainOpeReclam.addEventListener("SortirSuivitOperationReclam",sortir);	
			  		myMainOpeReclam.addEventListener("CloturerOperationReclamation",sortirApresClotureReclamation);						  	
					myMainBox.addChild(myMainOpeReclam);
					wasDisplayedOpPanel = true;
					displayedOpPanel = myMainOpeReclam;	
					myOpMenu.disablePanel(); 					
					break	
				}	
			}
		}
		
		//Sort de lecran qui genere l'evenement passe en parametre  
		//S'il sagit d'un des formulaires de commande on met a jour les tableaux qui listent les differntes operations 
		private function sortir(ev : Event):void{					
		
			if (myMainBox.getChildren().length>0){
				myMainBox.removeAllChildren();
			}
			
			myMainCreateDisplayed = false;
			wasDisplayedOpPanel = false;
			displayedOpPanel = null;
			
			
			if (ev.type != PanelNouveauxProduits.SORTIR){
				myOpMenu.mettreAJourGridOperation();				
			}
		}
		
		private function sortirApresClotureReclamation(cloe : CloturerOperationEvent):void{
			
			if (myMainBox.getChildren().length>0){
				myMainBox.removeAllChildren();
			}
			
			myMainCreateDisplayed = false;
			wasDisplayedOpPanel = false;
			displayedOpPanel = null;
			
			//myOpMenu.enabled = true;
			if (cloe.type == "CloturerOperationReclamation"){
				idOpCree = cloe.idOperation;
				autoSelectOperation = true;
				myOpMenu.mettreAJourGridOperation();				
			}
		}
		
		
		//Met à jour les listes d'operations apres avoir enregistrer une commande
		private function sortirDemandeApresEnregistrement(ev : Event):void{		
			autoSelectOperation = false;
			myOpMenu.mettreAJourGridOperation();
			
		}
		
		
		//Sort de la commande apres avoir valider le rapprochement
		//On met à jour les listes d'operation 
		//On auto selectionne l'operation qui vient d etre creee		
		private function sortirDemandeApresCreation(ev : Event):void{
			
			if (myMainBox.getChildren().length>0){
				myMainBox.removeAllChildren();
			}
			
			myMainCreateDisplayed = false;
			wasDisplayedOpPanel = false;
			displayedOpPanel = null;
			
			//myOpMenu.enabled = true;
			if (ev.type != PanelNouveauxProduits.SORTIR){
				idOpCree = Commande(ev.currentTarget.commande).operationID;
				autoSelectOperation = true;
				myOpMenu.mettreAJourGridOperation();				
			}
		}
		
		//annuler la creation d'operation (Resiliation)
		private function annulerCreationHandler(ev : Event):void{
			try{
				
				myMainCreate.clean();				
				myMainBox.removeChild(myMainCreate);
				myMainCreateDisplayed = false;				
				myOpMenu.enabledPanel();
				 
			}catch(e : Error){
				trace("myMainCreate n'existe pas");
			}
		}
		
		//rend le menu courant inactif si le panel lui concernant est affiché et vis versa
		private function disableMenus(fe : FlexEvent):void{
					
		}
		//Enregistrer la creation 
		private function enregistrerCreationHandler(eo : EnregistrerOperationEvent):void{
			try{				
				idOpCree = eo.idOperation;				
				autoSelectOperation = true;						  
				myOpMenu.mettreAJourGridOperation();
				myMainCreate.clean();				
				myMainBox.removeChild(myMainCreate);
				myMainCreateDisplayed = false;
				myOpMenu.enabledPanel();	
					
			}catch(e : Error){
				trace("myMainCreate n'existe pas");
			}
		}		
		
		//met a jour le grid des operations apres l'enregistrement d'une reclamation
		private function enregistrerOperationReclam(eoe : EnregistrerOperationEvent):void{
			myOpMenu.mettreAJourGridOperation();
			 		
		}
		
		//montre le panel de rapprochement de suite apres une resiliation	
		private function enregistrerOPRapprochement(eoe : EnregistrerOperationEvent):void{
			idOpCree = eoe.idOperation;
			autoSelectOperation = true;
			myOpMenu.mettreAJourGridOperation();
			
		}
		
		//enregistre puis montre l'op de rapprochement suite a une cloture avec rapprochement		
		private function displayOpeRappPanel(ome : OperationMenuEvent):void{		
			
			if (myMainBox.getChildren().length>0){
				myMainBox.removeAllChildren();
				myMainOpeRapDisplayed = false;
				displayedOpPanel = null;				
			}
			
			//config rapprochement suite a une resiliation
				myMainOpeRap = new EtapesOperationRapprochements(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);
			/* 	myMainOpeRap.addEventListener("AnnulerCreation",annulerCreationHandler);
				myMainOpeRap.addEventListener("EnregistrerOperation",enregistrerCreationHandler); */
				myMainBox.addChild(myMainOpeRap);
				wasDisplayedOpPanel = true;
				displayedOpPanel = myMainOpeRap;
				myMainOpeRapDisplayed = true;
				myOpMenu.disablePanel(); 
		}
		
		//Une fois les listes d'operations mis a jour,si on a positionne le booleen autoSelectOperation a true 
		// on selectionne l'operation reference par idOpCree et on l' affiche
		private  function girOpestOk(e : Event):void{
			 
			if (autoSelectOperation){
				myOpMenu.selectionnerGridLigne(idOpCree);				
				autoSelectOperation = (!myOpMenu.displayOpePanel(null));				
			}
		}
		
		
		//Affiche le formualire de creation d'operation de resiliation
		public function displayCreatePanel():void {
			if (myMainBox.getChildren().length>0){
				myMainBox.removeAllChildren();
				myMainCreateDisplayed = false;
				displayedOpPanel = null;				
			}
						
			myMainCreate = new CreationOperation();
			myMainCreate.addEventListener("AnnulerCreation",annulerCreationHandler);			
			myMainCreate.addEventListener("EnregistrerOperation",enregistrerCreationHandler);
			myMainBox.addChild(myMainCreate);
		
			myMainCreateDisplayed = true;
			myOpMenu.disablePanel();
		}
		
		
		//Selectionne une operation suivant sont identifaint depuis les journaux
		//Attend un evenement de type SelectionOperationEvent
		private function goToSelectedOpreation(soe : SelectionOperationEvent):void{
			try{
				vsMenu.selectedChild = myOpMenu;			
				myOpMenu.selectionnerGridLigne(soe.idOperation);
				myOpMenu.displayOpePanel(null);	
			}catch(e:Error){
				trace(e.getStackTrace());
			}		
			
		}		
		
		
		//Rafraichit les listes d'operations		
		private function refreshGrid(e : Event):void{			
			myOpMenu.mettreAJourGridOperation();
		}
		
		/*---------------------------------------- JOURNEAUX --------------------------------------------------*/
		
		
		//Affiche le journal suivant le type et eventuellement l'id du noeud passés en parametre
		//via l'evenement AfficherJournalEvent
		private function displayMenuPanel(aje : AfficherJournalEvent):void{			
			if (myMainBox.getChildren().length > 0){
				myMainJournaux.afficherJournal(aje.typeJournal,aje.nodeInfos);				
			}else{
				if(vsMenu.selectedChild == myJournauxMenu){
					myMainBox.addChild(myMainJournaux);
					myMainJournaux.afficherJournal(aje.typeJournal,aje.nodeInfos);
					wasDisplayedJrnxPanel = true;
					displayedJrnxPanel = myMainJournaux;	
				}else{
					
				}
				
			} 
			
		}
						
		/*-------------------------------- RECHERCHE ----------------------------------------------------------------------*/

		//Affiche le panel de Gestion direct
		//les parametres sont passés via l'evenement RechercheMenuEvent
		private function displayRechercheResi(rme : RechercheMenuEvent):void{			
			if (myRechercheResi != null){
				trace("- displayRechercheResi -  myRechercheResi != null");
					
				if (myMainBox.getChildren().length == 0){
					myMainBox.addChild(myRechercheResi);
				}				
				myRechercheResi.setParams(rme);		
				myRechercheResi.mettreAJourGridRessources();
				myRechercheResi.reset();				
			}else{
				trace("- displayRechercheResi - myRechercheResi == null");
				myRechercheResi = new PanelResilierProduit();
				myRechercheResi.setParams(rme);			
				myRechercheResi.addEventListener("sortirRechercheResi",sortirRechercheResi);			
				myMainBox.addChild(myRechercheResi);
			}
			wasDisplayedRchePanel = true;
			displayedRchePanel = myRechercheResi;			
		}
		
		//Efface les données de l'ecran Gestion Direct
		//un evenement de type RechercheMenuEvent est attendu
		private function resetRechercheResi(rme : RechercheMenuEvent):void{			
			if (myRechercheResi != null){
				myRechercheResi.setParams(rme);						
				myRechercheResi.resetAll();				
			}		
		}
		
		//Sort de la Gestion direct
		private function sortirRechercheResi(ev : Event):void{
			if (myMainBox.getChildren().length>0){
				myMainBox.removeAllChildren();
			}
			
			myRechercheResi = null;
			wasDisplayedRchePanel = false;
			displayedRchePanel = null;				
		}
		
	}
}