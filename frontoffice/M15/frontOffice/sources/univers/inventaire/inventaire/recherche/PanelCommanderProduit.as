package univers.inventaire.inventaire.recherche
{
	import mx.events.FlexEvent;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	import composants.util.ConsoviewFormatter;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.utils.ObjectUtil;
	import mx.controls.Alert;
	import univers.inventaire.inventaire.menu.RechercheMenuEvent;
	import composants.util.DateFunction;
	
	public class PanelCommanderProduit extends PanelCommanderProduitIHM
	{

		private const OLD_LINE : int = 0;

		 
		private var nodeId : int;//le noeud choisit
		private var nodePerimetre : String;//le perimetre du noeud
		private var nodeLibelle : String;//le libelle du noeud
		 		
		//--- lignes -----
		[Bindable]
		private var listeSousTete : ArrayCollection;		
		[Bindable]		
		private var selectedSousTete : Array;		 
		
		private var opLisetLignes : AbstractOperation;
		
		//la ligne sur laquelle on click (dans le datagrid)
		private var sousTete : String; //le numero
		private var sousTeteId : int; // l'identifiant
		private var ctNvLigne : int = -1;//compteur nouvelle ligne
		
		//--- produits client ----
		private var tmpSelectedProduitClient : Object;
		
		[Bindable]
		private var listeProduitsClient : ArrayCollection = new ArrayCollection();
		[Bindable]
		private var listeProduitsClientSelectionnes : ArrayCollection = new ArrayCollection();
		
		private var opPclient : AbstractOperation;
		
		
		
		//--- commande ----
		private var opCommander : AbstractOperation;
		
			
		public function PanelCommanderProduit()
		{		
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		public function setParams(params : RechercheMenuEvent):void{
			nodeId = params.nodeId;//l'identifiant du noeud 
			nodeLibelle = params.nodeLibelle;//le libellé du noeud
			nodePerimetre =	params.nodePerimetre;//le type de perimetre pour le noeud
		}	
		
		/**
		 * Gere le changement de perimetre
		 * */
		public function onPerimetreChange():void{
			btSortir.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
		}
		
		
		
//--- INITIALISATION ---------------------------------------------------------------------------------------------------------------------------
		private function initIHM(fe : FlexEvent):void{
			
			//-- sortir ----
			btSortir.addEventListener(MouseEvent.CLICK,sortir);	
			
			//-- grid lignes ----
			myGridLigne.addEventListener(Event.CHANGE,gridLigneChangeHandler);				
			DataGridColumn(myGridLigne.columns[0]).labelFunction = formatDataTip;	
			txtFiltreLigne.addEventListener(Event.CHANGE,filtrerLeDataGrid);
			
			//--- grid produits client ----
			txtFiltre.addEventListener(Event.CHANGE,filtreChangeHandler);
			myGridListeProduitsClient.dataProvider = listeProduitsClient;
			
			//--- produit client selectionne
			
			myGridListeProduitsClientSelectionnes.allowMultipleSelection = true;
			myGridListeProduitsClientSelectionnes.dataProvider = listeProduitsClientSelectionnes;
			DataGridColumn(myGridListeProduitsClientSelectionnes.columns[0]).labelFunction = formatDataTip;			
			
			//bouttons FLECHES
			btUp.addEventListener(MouseEvent.CLICK,monterLesProduitsSelectionnes);
			btAllUp.addEventListener(MouseEvent.CLICK,monterTousLesProduitsSelectionnes);
			btDown.addEventListener(MouseEvent.CLICK,descendreLesProduisSelectionnes);
			btAllDown.addEventListener(MouseEvent.CLICK,descendreTousLesProduitsSelectionnes);
			
			
			//--- commander ----
			btValider.addEventListener(MouseEvent.CLICK,commanderProduit);
			getLinesFromNode();			
		}
		
		//formatage du telepone dans le grid
		private function formatDataTip(item : Object, column : DataGridColumn):String{
			
			if ((String(item.SOUS_TETE).length == 10)&& parseInt(item.SOUS_TETE).toString().length == 9)
					return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);
				else
					return item.SOUS_TETE;
			
		}
//--- FIN INITIALISTAION -----------------------------------------------------------------------------------------------------------------------


		
//--- NAVIGUER ---------------------------------------------------------------------------------------------------------------------------------
		private function sortir(me : MouseEvent):void{
			dispatchEvent(new Event("sortirRechercheComm"));
		}
		
	
//--- FIN NAVIGATION ---------------------------------------------------------------------------------------------------------------------------		
		private function is2InArray(param1:String,param2:String, colname1:String, colname2:String, acol : Array):Boolean{			
			var OK:Boolean = false;
			var ligne : Object;				
			var len : int = acol.length;
			for (ligne in acol){
				

 				if ((param1 == acol[ligne][colname1])&&(param2 == acol[ligne][colname2]))OK = true; 
			}
			trace ("OK = "+ OK)
			return OK;
		}	

//--- GRID LIGNES ------------------------------------------------------------------------------------------------------------------------------
		
		//gere le click sur une ligne du grid
		private function gridLigneChangeHandler(ev : Event):void{
			selectedSousTete = new Array();			
			selectedSousTete = myGridLigne.selectedItems;
			if (selectedSousTete.length > 0)  chargerListeProduitsClient();						
		}
		
		//formate les donnees du selectedItems du grid
		private function formateData( data : Array , colname : String):Array{
			var s : String;
			var a : Array = new Array();
			for (var i : uint = 0 ; i < data.length ; i++ ){
				a.push(data[i][colname]);
			}
			return a;
		}	
		
		//filtre le grid
		private function filtrerLeDataGrid(e : Event):void{
			listeSousTete.filterFunction = processfitrerLeDataGrid;
			listeSousTete.refresh();
		}
	
		//filtre pour le grid des sous-tete
		private function processfitrerLeDataGrid(value : Object):Boolean{
					
			if (String(value.SOUS_TETE.toLowerCase()).search(txtFiltreLigne.text.toLowerCase()) != -1) {
				return (true);
			} else {
				return (false);
			}
	
		}		
//--- FIN GRID LIGNES --------------------------------------------------------------------------------------------------------------------------

//----------- SELECTION DES PRODUITS --------------------------------------------------------------------------------
		private function descendreLesProduisSelectionnes(me:MouseEvent):void{
				try{	
					var len : int = myGridListeProduitsClient.selectedIndices.length;
					
					if (len > 0){
							
							myGridListeProduitsClient.selectedItems.forEach(descendreLeProduitPourChaqueLigneSelectionne);
							myGridListeProduitsClientSelectionnes.dataProvider.refresh();
							myGridListeProduitsClient.dataProvider.refresh();
					}else if (len == 1){
						var index : int = myGridListeProduitsClient.selectedIndex;
						
						
						myGridListeProduitsClientSelectionnes.dataProvider.
							addItem(myGridListeProduitsClient.
							dataProvider.removeItemAt(index));	
							
						myGridListeProduitsClientSelectionnes.dataProvider.refresh();
						myGridListeProduitsClient.dataProvider.refresh();				
					}
					
				}catch( re : RangeError ){
					trace("l'indice sort des limites")
				}catch( e : Error){
					trace(e.message,e.getStackTrace());
				}	
		}
		
		private function descendreTousLesProduitsSelectionnes(me : MouseEvent):void{
			
			if ((myGridListeProduitsClient.dataProvider as ArrayCollection).length > 0){
				
				(myGridListeProduitsClient.dataProvider as ArrayCollection).source.forEach(descendreLeProduitPourChaqueLigneSelectionne);
				
				myGridListeProduitsClient.dataProvider.refresh();		
				myGridListeProduitsClientSelectionnes.dataProvider.refresh();	
			}
			
		}
		
		private function descendreLeProduitPourChaqueLigneSelectionne(element : * , index : int , arr : Array):void{				
			
			var len : int = selectedSousTete.length;
			for (var i : int = 0; i < len; i++){
				
				element.SOUS_TETE = selectedSousTete[i].SOUS_TETE;
				element.IDSOUS_TETES = selectedSousTete[i].IDSOUS_TETES;
				element.QUANTITE = 1;
				
				var obj : Object  = new Object();								
				obj = ObjectUtil.copy(element);
				
				if ((element != null) && (!is2InArray(element.IDPRODUIT_CLIENT,
												  element.IDSOUS_TETES,
												  "IDPRODUIT_CLIENT",
												  "IDSOUS_TETES",
												  (myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).source)))
				{								  	
					(myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).addItem(obj);													  	
					
				}								  
													
			}	
			
		}
		
		
		private function monterLesProduitsSelectionnes(me : MouseEvent):void{		
			try{	
				var len : int = myGridListeProduitsClientSelectionnes.selectedIndices.length;
				
				if (len > 0){
						myGridListeProduitsClientSelectionnes.selectedItems.forEach(monterLeProduit);
						myGridListeProduitsClientSelectionnes.dataProvider.refresh();
						myGridListeProduitsClient.dataProvider.refresh();
				}
				
			}catch( re : RangeError ){
				trace("l'indeice sort des limites")
			}catch( e : Error){
				trace(e.message,e.getStackTrace());
			}			
		}
		
		private function monterTousLesProduitsSelectionnes(me:MouseEvent):void{
			if ((myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).length > 0){
				(myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).source.forEach(monterLeProduit);
				myGridListeProduitsClientSelectionnes.dataProvider.refresh();
				myGridListeProduitsClient.dataProvider.refresh();			
			}
				
		}
		
		private function monterLeProduit(element : * , index : int , arr : Array):void{
			
				(myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection)
					.removeItemAt(0);								
		}
			
//--- FIN SELECTION PRODUITS -------------------------------------------------------------------------------------------------------------------			
//--- GRID PRODUITS CLIENT ---------------------------------------------------------------------------------------------------------------------
		//gere le fitre
		private function filtreChangeHandler(ev : Event):void{
			try{
				listeProduitsClient.filterFunction = filterFunc;
				listeProduitsClient.refresh();
			}catch(e : Error){
				trace("Erreur filtre liste Elements de facturation");
			}
			
		}
		
		//filtre
		private function filterFunc(item:Object):Boolean {	
			try {			
				if 	((String(item.THEME_LIBELLE).toLowerCase().search(String(txtFiltre.text).toLowerCase()) != -1)  
					   				||
					 (String(item.LIBELLE_PRODUIT).toLowerCase().search(String(txtFiltre.text).toLowerCase()) != -1) 
					   				||					 	   
					 (String(item.CODE_ARTICLE).toLowerCase().search(String(txtFiltre.text).toLowerCase()) != -1))
					   		  
					{
						return (true);
					}else {
						return (false);
					} 
			} catch(e:Error){
				trace(e.message,e.getStackTrace());		
			}
			return true; 
		}						
	
		
//--- FIN GRID PRODUITS CLIENT -----------------------------------------------------------------------------------------------------------------



//--- COMMANDER --------------------------------------------------------------------------------------------------------------------------------
		//gere le click sur le bouton commander
		private function commanderProduit(me : MouseEvent):void{
			if ((myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).length == 0){
				Alert.show("Vous devez selectionner un produit","Attention!!!");
			}else{
				enregistrerCommande();	
			}
		}
//--- FIN COMMANDER ----------------------------------------------------------------------------------------------------------------------------

//--- REMOTINGS --------------------------------------------------------------------------------------------------------------------------------
		//récupere les lignes du noeud selectioné							
		private function getLinesFromNode():void{			
			opLisetLignes = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.inventaire.cycledevie.recherche.facade",
												  "rechercherListeLigne",
												  getLinesFromNodeResultHandler,
												  defaultFaultHandler);
												  
			RemoteObjectUtil.callService(opLisetLignes,
										nodePerimetre,										
										nodeId); 
		}
		
		private function getLinesFromNodeResultHandler(re : ResultEvent):void{
			var nvelleLigneObject : Object = new Object();
			nvelleLigneObject.SOUS_TETE = "Nouvelle ligne";
			nvelleLigneObject.IDSOUS_TETES = 0; 
			
			listeSousTete = re.result as ArrayCollection;	
						
			if (listeSousTete.length > 0) {				
				txtFiltreLigne.enabled = true;								
				//listeSousTete.addItemAt(nvelleLigneObject,0);	
			}
			else {				
				//listeSousTete.addItem(nvelleLigneObject);	
				txtFiltreLigne.enabled = false;				
			}			 
			 
			myGridLigne.dataProvider = listeSousTete;
		}
		
		//récupere les Produits Client pour une ligne							
		private function chargerListeProduitsClient():void{				
				opPclient = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.inventaire.cycledevie.ElementDInventaire",
												  "getListeProduitsClient",
												  chargerListeProduitsClientResultHandler,
												  defaultFaultHandler);
												  
				RemoteObjectUtil.callService(opPclient,selectedSousTete,OLD_LINE); 						
		}			
		
		private function chargerListeProduitsClientResultHandler(re : ResultEvent):void{
			/// charger liste elements de facturation 		
				listeProduitsClient = re.result as ArrayCollection;	 
				myGridListeProduitsClient.dataProvider = listeProduitsClient;
				txtFiltre.enabled = true;			 
		}
		
		//enregistre une commande		
		private function enregistrerCommande():void{				
				opCommander = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.inventaire.cycledevie.OperationAction",
												  "commanderProduit2",
												  enregistrerCommandeResultHandler,
												  defaultFaultHandler);
												  
				RemoteObjectUtil.callService(opCommander,
											listeProduitsClientSelectionnes.source,											
											" ",
											DateFunction.formatDateAsString(new Date()));	
															
				/* RemoteObjectUtil.callService(opCommander,
											selectedSousTete[0].IDSOUS_TETES,
											listeProduitsClientSelectionnes[0].IDPRODUIT_CLIENT,
											listeProduitsClientSelectionnes[0].QUANTITE,
											txtCommentaire.text,
											DateFunction.formatDateAsString(new Date())); */					
				 						
		}			
		
		private function enregistrerCommandeResultHandler(re : ResultEvent):void{			
				if (parseInt(re.result.toString()) > 0){
					var evtObj : SaisieNewRessourceComplete = new SaisieNewRessourceComplete("ProduitCommander");
					evtObj.idRessource = re.result;					
					dispatchEvent(evtObj);	
				}else{
					Alert.show("Une erreur c'est produite lors de l'enregistrement de la commande");
				}				
		}
				
		///Fault--------------------------------------------------		
		private function defaultFaultHandler(fe : FaultEvent):void{
			trace("error remoting");
		}	
//--- FIN REMOTINGS ----------------------------------------------------------------------------------------------------------------------------		
	}
}