package univers.inventaire.inventaire.recherche
{
	import flash.events.Event;

	public class ModifiactionEvent extends Event
	{
		public static const MODIFICATION : String = "modificationEvent";		
		private var _dateAction : String;
		private var _commentaire : String;
		private var _idAction : int;
		
		public function ModifiactionEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(MODIFICATION, bubbles, cancelable);
		}
				
		public function set dateAction(d : String):void{
			_dateAction = d;
		}
		
		public function get dateAction():String{
			return _dateAction;
		}
		
		public function set commentaire(c : String):void{
			_commentaire = c;
		}
		
		public function get commentaire():String{
			return _commentaire;
		}
		
		public function set idAction(idac : int):void{
			_idAction = idac;
		}
		
		public function get idAction():int{
			return _idAction;
		}
	}
}