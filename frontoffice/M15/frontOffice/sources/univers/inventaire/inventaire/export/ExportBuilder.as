package univers.inventaire.inventaire.export
{
	/**
	 * Classe permettant de construire des exports
	 * */
	public class ExportBuilder implements IExportable
	{
		//Une reference vers l'objet à exporter
		private var export :  IExportable;
		
		/**
		 * Constructeur
		 * @param exp l'objet à exporter, cette objet doit implementer l'interface IExportable
		 * */
		public function ExportBuilder(exp : IExportable){
			export = exp;			
		}
		
		/**
		 * Exporte l'objet au format passé en paramètre
		 * @param format 
		 * */
		public function exporter(format:String="PDF"):void
		{
			export.exporter(format);
		}
		
	}
}