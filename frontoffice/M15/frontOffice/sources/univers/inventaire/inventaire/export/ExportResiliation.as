package univers.inventaire.inventaire.export
{
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	import univers.inventaire.inventaire.etapes.OperationVO;
	
	public class ExportResiliation implements IExportable
	{
		private static const URL_EXPORT : String = "/fr/consotel/consoview/cfm/cycledevie/resiliations/Display_Demande.cfm";
		
		private var _resiliation : OperationVO;
		private var _produits : ArrayCollection;
		
		public function ExportResiliation(resi : OperationVO, produits : ArrayCollection)
		{
			_resiliation = resi;
			_produits = produits;
		}

		public function exporter(format:String="PDF"):void
		{
			switch(format.toUpperCase()){
				case "PDF" : {
						afficher("pdf");
					break;
				}
				default : {
					throw new Error("Format Inconnu");
				}
			}	
		}
		
		/**
		 * Affiche la commande dans une nouvelle fenetre au format passé en paramètre
		 * @param format 
		 * */
		public function afficher(format:String="PDF"):void
		{						
			if (_produits != null){
					var url:String = moduleWorkFlowIHM.NonSecureUrlBackoffice + URL_EXPORT;
										
		            var variables:URLVariables = new URLVariables();		            
		            variables.FORMAT = format;
		            variables.TYPE = "Resiliation";
					variables.PERIMETRE_LIBELLE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
					//infos commandes
					
					variables.LIBELLE = _resiliation.LIBELLE_OPERATIONS;
					variables.REF_CLIENT = (_resiliation.REFERENCE_INTERNE != null)?_resiliation.REFERENCE_INTERNE:"---";				
					variables.REF_OPERATEUR =( _resiliation.REF_OPERATEUR != null)?_resiliation.REF_OPERATEUR:"---";					
					variables.COMMENTAIRES = ( _resiliation.COMMENTAIRE != null)?_resiliation.COMMENTAIRE:"---";					
					variables.DATE_EFFECTIVE = (_resiliation.DATE_CREATION)?DateFunction.formatDateAsString(_resiliation.DATE_CREATION):"---";
					
					
					//contact					
					variables.CONTACTID = _resiliation.IDCDE_CONTACT;
					variables.SOCIETEID = _resiliation.IDCDE_CONTACT_SOCIETE;
					
					
					variables.LI_PRODUITS = ConsoviewUtil.extraireColonne(_produits,"LIBELLE_PRODUIT");			
					variables.LI_LIGNES = ConsoviewUtil.extraireColonne(_produits,"SOUS_TETE");
					variables.LI_COMPTES = ConsoviewUtil.extraireColonne(_produits,"COMPTE");
					variables.LI_SOUSCOMPTES = ConsoviewUtil.extraireColonne(_produits,"SOUS_COMPTE");
					variables.LI_OPERATEURS = ConsoviewUtil.extraireColonne(_produits,"OPNOM");
										
					var request:URLRequest = new URLRequest(url);
		            request.data = variables;
		            request.method=URLRequestMethod.POST;
		            
		            navigateToURL(request,"_blank");
			
			}else{
					Alert.show("la demande n'a pas de produit");                 
			}
		}
	}
}