package univers.inventaire.inventaire
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.inventaire.creation.CreationOperation;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.PanelNouveauxProduits;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.operationresiliation.EnregistrerOperationEvent;
	import univers.inventaire.inventaire.etapes.CloturerOperationEvent;
	import univers.inventaire.inventaire.etapes.EtapesOperation;
	import univers.inventaire.inventaire.etapes.EtapesOperationCreation;
	import univers.inventaire.inventaire.etapes.EtapesOperationRapprochements;
	import univers.inventaire.inventaire.etapes.EtapesOperationReclam;
	import univers.inventaire.inventaire.journaux.SelectionOperationEvent;
	import univers.inventaire.inventaire.menu.OperationMenu;
	import univers.inventaire.inventaire.menu.OperationMenuEvent;
	import univers.inventaire.inventaire.recherche.PanelResilierProduit;

	[Bindable]
	public class FunctionWorkFlowImpl extends HBox
	{
		public var myOpMenu : OperationMenu;
		public var myMainBox : Box;
		
		//Reference vers les ecrans des operations de Resiliation (affichage de l historique des actions pour une operation : WorkFlow)
		protected var myMainOpe:EtapesOperation;
		
		//Reference vers l ecran des operations Verification
		protected var myMainOpeRap:EtapesOperationRapprochements;
		
		//Reference vers l ecran des operations de Reclamation
		protected var myMainOpeReclam:EtapesOperationReclam;
		
		//Reference vers l ecran des operations de Creation 
		protected var myMainOpeCrea : EtapesOperationCreation;
		
		//Reference vers l ecran des Commandes (Nouveux produits et Nouvelles Lignes)
		protected var myPanelNouveauxProduits : PanelNouveauxProduits;
		
		//Reference vers l ecran de creation des opérations (Résiliation)
		protected var myMainCreate:CreationOperation;
		
		//Reference vers l ecran de Gestion direct
		protected var myRechercheResi : PanelResilierProduit;	
		
		//Booleen permettant de savoir si on auto-affiche l operation selectionne 
		private var autoSelectOperation : Boolean = false;
		
		//Une reference vres l'identifiant de l'operation a afficher
		private var idOpCree : int;
				
		//le gestionnaire de droit
		public var gestionDroit:GestionDroitCycleDeVie = new GestionDroitCycleDeVie();
		
		
	 
		
		

		public function FunctionWorkFlowImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, afterCreationComplete);
		}
		
		override protected function commitProperties():void{
			
			
			
			/*-- operation ----*/
			//myOpMenu.addEventListener(ListEvent.CHANGE,myOpMenuListEventChangeHandler);
			
			
			myOpMenu.addEventListener(OperationMenuEvent.SHOW_SUIVIT_OPERATION,displayOpePanel);			
			myOpMenu.addEventListener(OperationMenuEvent.SHOW_NOUVELLES_DEMANDE,displayedDemande);
			myOpMenu.addEventListener(OperationMenuEvent.SHOW_DEMANDE,displayedDemande);	
			myOpMenu.addEventListener("GridsOK",girOpestOk);			
			/*----------------*/
		}
		private function myOpMenuListEventChangeHandler(le : ListEvent):void{
			trace("====================================================");
			trace("Menu Target = " + le.target.id);
			trace("====================================================");
		}
				
		protected  function afterCreationComplete(event:FlexEvent):void {
			//super.afterCreationComplete(event);
			trace("(FunctionListeAppels) Perform IHM Initialization (" + getUniversKey() + "," + getFunctionKey() + ")");
			afterPerimetreUpdated();
		}
		
		public function getUniversKey():String{
			return "GEST";
		}
		
		public function getFunctionKey():String{
			return "GEST_WORKFLOW";
		}
		
		public function afterPerimetreUpdated():void {
			
			cleanMainBox();
 			
			myOpMenu.onPerimetreChange();
			
			gestionDroit.getProfiles(true);
			
		}
		
		
		/**
		 * Méthode à lancer lors de la déconnexion
		 * */
		public function logOff():void{
			if (myMainOpe != null) myMainOpe.clean();
			myMainOpe  = null;
			if (myMainOpeCrea != null) myMainOpeCrea.clean();
			myMainOpeCrea = null;
			if (myMainOpeRap != null) myMainOpeRap.clean();
			myMainOpeRap = null;
			if (myMainOpeReclam != null) myMainOpeReclam.clean();
			myMainOpeReclam = null;
		}
		
		
		/*=============== AFFICHAGE DES ITEMS A DROITE =================================================================================*/
		
		
		//Affiche l'ecran des formulaires de commande (nouvelles lignes ou nouveaux produits)
		//Le choix se fait suivant les parametre de l'evenement OperationMenuEvent
		//Voir la class OperationMenuEvent pour les parametres à rensigner
		private function displayedDemande(ome : OperationMenuEvent):void{ 			
			cleanMainBox();
			myPanelNouveauxProduits = new PanelNouveauxProduits(ome.type_Operation,ome.id_Operation);		
			myPanelNouveauxProduits.addEventListener(PanelNouveauxProduits.DEMANDE_CREEE,sortirDemandeApresEnregistrement);	
			myPanelNouveauxProduits.addEventListener(PanelNouveauxProduits.DEMANDE_UPDATER,sortirDemandeApresEnregistrement);						
			myPanelNouveauxProduits.addEventListener(PanelNouveauxProduits.SORTIR,sortir);				
			myPanelNouveauxProduits.addEventListener(PanelNouveauxProduits.OPERATION_CREE,sortirDemandeApresCreation);
			myMainBox.addChild(myPanelNouveauxProduits);
		}
		
		
		//affiche une operation (Resiliation, creation, verification ou reclamation) suivant sont type
		//le type est renseigne par l'evenement OperationMenuEvent attendu par cette methode
		private function displayOpePanel( ome : OperationMenuEvent):void {
			
			//Si il y avait une operation affichee on la retire 	
			cleanMainBox();
			
			//On estancie la bonne operation puis on l'affiche
			switch(ome.type_Operation){
				
				//WorkFlow Resiliation
				case INVTYPE_OPERATION.RESILIATION:{
					
					myMainOpe = new EtapesOperation(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);		
					myMainOpe.gestionDroit = gestionDroit;			
					myMainOpe.addEventListener("EnregistrerOperationRapprochement",enregistrerOPRapprochement);
					myMainOpe.addEventListener("SortirSuivitOperationResi",sortir);
					myMainOpe.addEventListener("mettreAjourGrid",refreshGrid);						
					myMainBox.addChild(myMainOpe);
					
					break;
				}
				//WorkFlow Creation
				case INVTYPE_OPERATION.COMMANDE : {
					myMainOpeCrea = new EtapesOperationCreation(ome.infosDemende);
					myMainOpeCrea.gestionDroit = gestionDroit;					
					myMainOpeCrea.addEventListener("EnregistrerOperationRapprochement",enregistrerOPRapprochement);
					myMainOpeCrea.addEventListener("SortirSuivitOperationCrea",sortir);		
					myMainOpeCrea.addEventListener("mettreAjourGrid",refreshGrid);
					myMainBox.addChild(myMainOpeCrea);
					break	
				}
				
				//Verification de la facturation apres résiliation				
				case INVTYPE_OPERATION.VERIF_FACT_RESILIATION : {
					myMainOpeRap = new EtapesOperationRapprochements(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);
					myMainOpeRap.gestionDroit = gestionDroit;		
					myMainOpeRap.addEventListener("EnregistrerOperationReclam",enregistrerOperationReclam);
					myMainOpeRap.addEventListener("SortirSuivitOperationRapp",sortir);
					myMainOpeRap.addEventListener("CloturerOperationRapp",sortir);
					myMainBox.addChild(myMainOpeRap);
					break;
				}
				
				//Verification de la facturation apres commande			
				case INVTYPE_OPERATION.VERIF_FACT_COMMANDE : {
					myMainOpeRap = new EtapesOperationRapprochements(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);
					myMainOpeRap.gestionDroit = gestionDroit;
					myMainOpeRap.addEventListener("EnregistrerOperationReclam",enregistrerOperationReclam);
					myMainOpeRap.addEventListener("SortirSuivitOperationRapp",sortir);
					myMainOpeRap.addEventListener("CloturerOperationRapp",sortir);
					myMainBox.addChild(myMainOpeRap);
					break;
				}
				
				
				//Reclamation d'une résiliation
				case INVTYPE_OPERATION.RECLAM_RESILIATION: {					
					myMainOpeReclam = new EtapesOperationReclam(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);
					myMainOpeReclam.gestionDroit = gestionDroit;			
				  	myMainOpeReclam.addEventListener("SortirSuivitOperationReclam",sortir);	
			  		myMainOpeReclam.addEventListener("CloturerOperationReclamation",sortirApresClotureReclamation);						  	
					myMainBox.addChild(myMainOpeReclam);
					break	
				}	
				
				//Reclamation d'une commande
				case INVTYPE_OPERATION.RECLAM_COMMANDE : {					
					myMainOpeReclam = new EtapesOperationReclam(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);	
					myMainOpeReclam.gestionDroit = gestionDroit;			
				  	myMainOpeReclam.addEventListener("SortirSuivitOperationReclam",sortir);	
			  		myMainOpeReclam.addEventListener("CloturerOperationReclamation",sortirApresClotureReclamation);						  	
					myMainBox.addChild(myMainOpeReclam);
					break	
				}	
			}
		}
		
		//Une fois les listes d'operations mis a jour,si on a positionne le booleen autoSelectOperation a true 
		// on selectionne l'operation reference par idOpCree et on l' affiche
		private  function girOpestOk(e : Event):void{
			if (autoSelectOperation){
				myOpMenu.selectionnerGridLigne(idOpCree);				
				autoSelectOperation = (!myOpMenu.displayOpePanel(null));				
			}
		}
		
		//*========================================================================================================================*//
		//Sort de lecran qui genere l'evenement passe en parametre  
		//S'il sagit d'un des formulaires de commande on met a jour les tableaux qui listent les differntes operations 
		private function sortir(ev : Event):void{					
		
			cleanMainBox();
			
			if (ev.type != PanelNouveauxProduits.SORTIR){
				myOpMenu.mettreAJourGridOperation();				
			}
		}
		
		private function sortirApresClotureReclamation(cloe : CloturerOperationEvent):void{
			
			cleanMainBox();
			
			//myOpMenu.enabled = true;
			if (cloe.type == "CloturerOperationReclamation"){
				idOpCree = cloe.idOperation;
				autoSelectOperation = true;
				myOpMenu.mettreAJourGridOperation();				
			}
		}
		
		
		//Met à jour les listes d'operations apres avoir enregistrer une commande
		private function sortirDemandeApresEnregistrement(ev : Event):void{		
			autoSelectOperation = false;
			myOpMenu.mettreAJourGridOperation();
			
		}
		
		
		//Sort de la commande apres avoir valider le rapprochement
		//On met à jour les listes d'operation 
		//On auto selectionne l'operation qui vient d etre creee		
		private function sortirDemandeApresCreation(ev : Event):void{
			
			cleanMainBox();
			//myOpMenu.enabled = true;
			if (ev.type != PanelNouveauxProduits.SORTIR){
				idOpCree = Commande(ev.currentTarget.commande).operationID;
				autoSelectOperation = true;
				myOpMenu.mettreAJourGridOperation();				
			}
		}
		
		//annuler la creation d'operation (Resiliation)
		private function annulerCreationHandler(ev : Event):void{
			try{
				
				myMainCreate.clean();				
				myMainBox.removeChild(myMainCreate);
				myOpMenu.enabledPanel();
				 
			}catch(e : Error){
				trace("myMainCreate n'existe pas");
			}
		}
		
		//rend le menu courant inactif si le panel lui concernant est affiché et vis versa
		private function disableMenus(fe : FlexEvent):void{
					
		}
		//Enregistrer la creation 
		private function enregistrerCreationHandler(eo : EnregistrerOperationEvent):void{
			try{				
				idOpCree = eo.idOperation;				
				autoSelectOperation = true;						  
				myOpMenu.mettreAJourGridOperation();
				
				
				cleanMainBox();
				myOpMenu.enabledPanel();
			}catch(e : Error){
				trace("myMainCreate n'existe pas");
			}
		}		
		
		//met a jour le grid des operations apres l'enregistrement d'une reclamation
		private function enregistrerOperationReclam(eoe : EnregistrerOperationEvent):void{
			myOpMenu.mettreAJourGridOperation();
			 		
		}
		
		//montre le panel de rapprochement de suite apres une resiliation	
		private function enregistrerOPRapprochement(eoe : EnregistrerOperationEvent):void{
			idOpCree = eoe.idOperation;
			autoSelectOperation = true;
			myOpMenu.mettreAJourGridOperation();
			
		}
		
		//enregistre puis montre l'op de rapprochement suite a une cloture avec rapprochement		
		private function displayOpeRappPanel(ome : OperationMenuEvent):void{		
			
			cleanMainBox();
			//config rapprochement suite a une resiliation
			myMainOpeRap = new EtapesOperationRapprochements(ome.numero_Operation,ome.id_Operation,ome.op_En_Cours);
			myMainBox.addChild(myMainOpeRap);
			myOpMenu.disablePanel(); 
		}
		
		
		//Affiche le formualire de creation d'operation de resiliation
		public function displayCreatePanel():void {
			cleanMainBox();
			myMainCreate = new CreationOperation();
			myMainCreate.addEventListener("AnnulerCreation",annulerCreationHandler);			
			myMainCreate.addEventListener("EnregistrerOperation",enregistrerCreationHandler);
			myMainBox.addChild(myMainCreate);
			myOpMenu.disablePanel();
		}
		
		
		//Selectionne une operation suivant sont identifaint depuis les journaux
		//Attend un evenement de type SelectionOperationEvent
		private function goToSelectedOpreation(soe : SelectionOperationEvent):void{
		}		
		
		
		//Rafraichit les listes d'operations		
		private function refreshGrid(e : Event):void{			
			myOpMenu.mettreAJourGridOperation();
		}
		
		private function cleanMainBox():void{
			if (myMainBox.getChildren().length>0){		
				
				var currentChild : DisplayObject = myMainBox.getChildAt(0);
				myMainBox.removeChild(currentChild);
				currentChild = null;
			}
		}
	}
}