package univers.inventaire.inventaire.etapes
{
	import composants.cyclevie.TabElFactOperationRapprochement;
	import composants.cyclevie.TabElReclamOperationRapprochement;
	import composants.cyclevie.TabProduitsOperationRaprochement;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Menu;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.inventaire.GestionDroitCycleDeVie;
	import univers.inventaire.inventaire.INVTYPE_OPERATION;
	import univers.inventaire.inventaire.creation.operationresiliation.EnregistrerOperationEvent;
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;
 	
 	
	/**
	 * Classe gerant l'ecran de gestion d'une opération de vérification
	 * */      
	public class EtapesOperationRapprochements extends EtapesOperationRapprochementsIHM
	{ 
		 
		 
		
		 
		//reference vers la boite de dialogue de confirmation
		private var confirmbox : ConfirmBox;	
		
		//Constante définissant le libelle d'une opération cloturer
		private const OPERATION_CLOSE : String = "Opération close";
		
		//Constante définissant l'identifiant de l'etat en reclamation;
		private const RECLAMATION : int = 24;
		
		//Constante définissant l'identifiant de l'etat lettré apres passage dans une réclamation;
		private const RECLAMATIONLETTREE : int = 26;
		
		//Constante définissant l'identifiant de l'etat lettré;
		private const LETTREE : int = 22;
		
		//Reference vers la méthode distante qui ramène la liste des actions possibles
		private var opListeActionPossible : AbstractOperation;
		
		//Reference vers la méthode distante qui enregistre une action
		private var opSaveAction : AbstractOperation;
		
		//Reference vers la méthode distante qui enregistre l'opération
		private var opSaveOp : AbstractOperation;
		
		//Reference vers la méthode distante qui ramène le detail d'une opéartion
		private var opDetailOpe : AbstractOperation;
		
		//Reference vers la méthode distante qui enregistre les lignes de facturation utilisées pour lettrer les ressources
		private var opSaveOpFact : AbstractOperation;
		
		//Reference vers la méthode distante qui ramène le detail d'un etat
		private var opDetailEtape : AbstractOperation;
		
		//Reference vers la méthode distante qui ramène l'historique des actions sur une ressource dans une opération
		private var opHistoOpe : AbstractOperation;
		
		//Reference vers la méthode distante qui ramène la liste des lignes de facturations utilisées pour le lettrage
		private var opListeFactLettree : AbstractOperation;
		
		//Reference vers la méthode distante qui supprime une liste de lignes de facturations utilisées pour le lettrage
		private var opSupListeFactLettree : AbstractOperation;
		
		//Reference vers la méthode distante qui cloture une opération
		private var opCloturerOpe : AbstractOperation;
				
		//Booléen placé à true lorque l'historique pour une ressource est chargé et pret à être utilisé
		private var historiqueActionOk : Boolean = false;
		
		//ArrayCollection contenant l'historique des actions effectuées sur une ressource
		[Bindable]
		private var tabHistoEtap : ArrayCollection;
		
		//ArrayCollection contenant le détail d'une opération
		private var operationDetail : ArrayCollection;
		
		//ArrayCollection contenant le détail d'une opération
		private var OperationMaitreDetail : ArrayCollection;
		
		//Le commentaire de l'action à enregistrer
		private var commentaireAction : String;		
		
		//tableau contenant les identifiant des ressources sélectionnées
		private var selectedProduit : Array = new Array();		
		
		//l'identifiant de l'action sélectionnée
		private var selectedAction : int;
		
		//produit selectionné pour l'action
		private var produitSelectionnerPouAction : Object;
		
		//Référence vers le menu du PopUpMenuButton 'Actions Possibles'
		private var myMenu : Menu;
		
		//ArrayCollection contenant la liste des action possibles
		[Bindable]
		private var tabActionsPossible : ArrayCollection;
		
		//Tableau tempon contenant la liste des ressources sélectionnées		
		private var TMPtabProduitsOpSelectionnee : Array;
		
		//Tableau tempon contenant la liste des lignes de facturation sélectionnées
		private var TMPtabProduitsFactSelectionnee : Array;			 
		 
		//Booléen placé à true si au moins une ressource a été sélectionnée 
		private var okOp : Boolean = false;		
		
		//Booléen placé à true si au moins une ligne de facturation a été sélectionnée 
		private var okFact : Boolean = false;		
		
		//Tableau d'identifiant de ressource à supprimer dans la liste de lettrage
		private var tabAsup : Array = new Array();
	 	
	 	//Reference vers le tableau des contenant la liste des lignes de facturations
		private var myGridFacture : TabElFactOperationRapprochement;
				
		//Reference vers le tableau contenant la liste des ressources de l'opération
		private var myGridProduitsOP : TabProduitsOperationRaprochement;		
				
		//Reference vers le tableau contenant la liste des elements lettrés
		private var myGridEltLettre : TabElReclamOperationRapprochement;
		
		//Référence vers l'identifiant de l'opération
		private var idOperation : int;	
		
		//Référence vers la	date de référence utilié pour le calcul des régules
		private var dateRef : String; 
		
		//Référence vers le type de l'operation qui à conduit à cette vérification
		private var typeOpmaitre :int;
		
		//Référence vers le numéro de l'opération
		private var numeroOperation : String; 
		
		//Référence vers l'etat de l'operation de réclamation (1 = en cours, 0 = cloturée)
		private var operationEnCours : int;
	 	
	 	
	 	private var _gestionDroit:GestionDroitCycleDeVie;
	 	
	 	
	 	public function get modeEcriture():Boolean{
			return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE);
		}
		
	 	/**
		 * Constructeur
		 * @param numero le numero de l'opération de réclamation
		 * @param idOp l'identifiant de l'opération
		 * @enCours l'état de l'operation (1 = en cours, 0 = cloturée)
		 * */	
		public function EtapesOperationRapprochements(numero : String,idOpe : int,enCours : int){			
			super();
			numeroOperation = numero;
			idOperation = idOpe;
			operationEnCours = enCours;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);	
			
			Alert.okLabel = "Fermer";		 			
		}	
		
		
		/**
		 * Supprime les fenêtres surgissantes
		 * */
		public function clean():void{
			if (confirmbox != null){
				if (confirmbox.isPopUp) PopUpManager.removePopUp(confirmbox);
				confirmbox = null;
			}
		}
				
		//pre initialisation de l'IHM
		//pre charge les infos pour l'opération et ensuite initialise les éléments de l'ihm	
		private function initIHM(fe : FlexEvent):void{
			chargerInfoOperation(idOperation);//pour la date ref(DATE_REF_CALCUL)
			addEventListener("detailOpcharge",init);				
		}
		
		//Initilaise l'ihm
		//Affecte les écouteurs d'évènements
		//...
		private function init(ev : Event):void{		
			
			
			btActionPossible.enabled =  false;			
			myGridProduitsOP = new TabProduitsOperationRaprochement();				
			myGridProduitsOP.initialiserGrid(idOperation,dateRef,typeOpmaitre);	
			 
			myGridFacture = new TabElFactOperationRapprochement();
			myGridFacture.initialiserGrid(idOperation,dateRef,typeOpmaitre);
			
			myGridEltLettre = new TabElReclamOperationRapprochement();
						
			myGridProduitsOP.addEventListener("ProduitsOPSelectionnes",triaterSelectionDeProduits);
			
			myGridProduitsOP.addEventListener("ReclamEnCours",enabledBtClourerSansLettrage);
			myGridProduitsOP.addEventListener("AucuneReclamEnCours",enabledBtClourerSansLettrage);
			
			myGridProduitsOP.addEventListener("CloturesAvecLettrageOk",enabledBtClourerAvecLettrage);
			
			myGridFacture.addEventListener("ProduitsFactSelectionnes",triaterSelectionDeProduits);
			myGridEltLettre.addEventListener(FlexEvent.CREATION_COMPLETE,initGrridEltLettre);
			
			
			myGridEltLettre.addEventListener("SuppressionProduitLettre",retablirProduitOperation);//vers etat 6 ou 36
			
			boxTabProduit.addChild(myGridProduitsOP);			
			myGridProduitsOP.myGridProduitsOp.allowMultipleSelection = false;
			myGridProduitsOP.titre = "Produits de l'opération";
			
			boxTabFacture.addChild(myGridFacture);
			myGridFacture.titre = "Eléments de la facturation";
			boxResume.addChild(myGridEltLettre);
			myGridEltLettre.titre = "Eléments de la facturation lettrés";
			btActionPossible.addEventListener(MouseEvent.CLICK,doAction);
			
			btCloturerSansLettrage.enabled = false;		
			btCloturerSansLettrage.addEventListener(MouseEvent.CLICK,cloturerOperationSansLettrage);
			
			btCloturerAvecLettrage.enabled =false;
			btCloturerAvecLettrage.addEventListener(MouseEvent.CLICK,cloturerOperationAvecLettrage);		
			
			btSortir.addEventListener(MouseEvent.CLICK,sortir);
			if (operationEnCours == 0 ) {
				callLater(disablePanel);
			}
		}
		
		//charge les elements lettrées pour une opération  		
		private function initGrridEltLettre(fe : FlexEvent):void{
			chargerElementGridEltLettre(idOperation);			
		}
		
		//Dispatche un evenement de type SortirSuivitOperationReclam signifiant que l'on veut sortir du module	
		private function sortir(me : MouseEvent):void{
			dispatchEvent(new Event("SortirSuivitOperationRapp"));			
		}
		
		
		//rend actif ou inactif le boutton clotuer sans lettrage
		//le bouton est actif s'il ny a pas de ressource dans une réclamation
		private function enabledBtClourerSansLettrage(ev : Event):void{
			trace("------------------------enabledBtClourerSansLettrage--------------------- " +ev.type);
			switch(ev.type){
				case "ReclamEnCours": {
					btCloturerSansLettrage.enabled = false;
					break;
				}
				case "AucuneReclamEnCours" :  {
					if (operationEnCours == 1 )
					{
						btCloturerSansLettrage.enabled = 	true && 
															modeEcriture &&
															gestionDroit.boolCloturerSansLettrage;
					} 
					else btCloturerSansLettrage.enabled = false;
					break;
				}
				default :{
					btCloturerSansLettrage.enabled = false;
				}
			}	
		}
		
		//rend actif ou inactif le boutton clotuer avec lettrage
		//le bouton est actif quand toutes les ressources sont dans l'état lettré.
		private function enabledBtClourerAvecLettrage(ev : Event):void{
			switch(ev.type){
				case "CloturesAvecLettrageOk": {
					if (operationEnCours == 1 )
					{
						btCloturerAvecLettrage.enabled = 	true && 
															modeEcriture && 
															gestionDroit.boolCloturerAvecLettrage;
					} 
					else btCloturerAvecLettrage.enabled = false;
					break;
				}				
				default :{
					btCloturerAvecLettrage.enabled = false;
				}
			}					
		}
		
		
		//Charge les infos sur l'etat de l'objet passe en parametre afin de créer une réclamation avec la bonne action
		//param in item un objet (une des resources selectionnées)
		private function traiterConsequencesEtat(item : Object):void{
			var myEtapeFlag : EtapeFlag = new EtapeFlag(selectedAction,0);
			myEtapeFlag.addEventListener("EtapeFlagComplete",processConsequencesEtat);			
			addChild(myEtapeFlag).visible = false;						
		}
		
		//Enregistrement de l'operation de réclamation
		private function processConsequencesEtat(ev : Event):void{
			var etape : EtapeFlag = EtapeFlag(ev.currentTarget);
			var TYPE_OPERATION : int = operationDetail[0].IDINV_TYPE_OPE;
			
			trace("processConsequencesEtat");
			if((etape.ENTRAINE_CREATION == 1)  && (operationEnCours == 1)){
				switch(etape.IDETAT){
					case RECLAMATION : {
						if (TYPE_OPERATION == INVTYPE_OPERATION.VERIF_FACT_COMMANDE){
							enregistrerOperationReclamation(INVTYPE_OPERATION.RECLAM_COMMANDE,selectedProduit);	
						}else if (TYPE_OPERATION == INVTYPE_OPERATION.VERIF_FACT_RESILIATION){
							enregistrerOperationReclamation(INVTYPE_OPERATION.RECLAM_RESILIATION,selectedProduit);	
						}else{
							throw new Error("Type d'opération inconnu");
						}break;
					}
					case RECLAMATIONLETTREE : { 
						enregistrerOperationReclamation(62,selectedProduit);						
						break;
					}
				}						
			}				
			EtapeFlag(ev.currentTarget).removeEventListener("EtapeFlagComplete",processConsequencesEtat);			
		}
		
		//Gere la selection des éléments des tableaux
		//Si au moins un élément de chaque tableau est sélèctionné et que l'opération est en cours on charge les actions possibles 
		//et on active le bouton des actions possibles
		private function triaterSelectionDeProduits(spe : SelectionProduitEvent):void{			 
			switch(spe.type){
				case "ProduitsOPSelectionnes" : {					
					try{
						produitSelectionnerPouAction = spe.tabProduits[0];
						selectedProduit = [spe.tabProduits[0].IDINVENTAIRE_PRODUIT];
						chargerHistoriqueLigne();		
						if ((produitSelectionnerPouAction.IDINV_ETAT != LETTREE)){
							okFact = true;okOp = true;		
													
						}else if (((spe.tabProduits.length > 0) && (spe.tabProduits.length < 2))&&
							((spe.tabProduits[0].IDINV_ETAT != 21)||
							 (spe.tabProduits[0].IDINV_ETAT != 22)||
							 (spe.tabProduits[0].IDINV_ETAT != 23)))
							{
							TMPtabProduitsOpSelectionnee = spe.tabProduits;
							okOp = true;							
						}else okOp = false;	
					}catch(e :Error){
						okOp = false;						
					}				
					break;
				}
				case "ProduitsFactSelectionnes" :{					
					try{
						if (spe.tabProduits.length > 0){
							TMPtabProduitsFactSelectionnee = spe.tabProduits;
							okFact = true;
						}else okFact = false;
					}catch(e :Error){
						okFact = false;						
					}				
					break;
				}				
			}
			
			//btEffacer.enabled = okOp;			
			activeActionPossible(Boolean(okFact && okOp));
		}
		
		//rend possible ou impossible le lettrage et la reclam
		private function activeActionPossible(ok : Boolean):void{	
			if(ok){	
				if ((produitSelectionnerPouAction.IDINV_ETAT == 24)||
				   (produitSelectionnerPouAction.IDINV_ETAT == 25)||
				   (produitSelectionnerPouAction.IDINV_ETAT == 26)||
				   (produitSelectionnerPouAction.IDINV_ETAT == 27)){							   	
				   		
						btActionPossible.enabled = false;
				}else{
					btActionPossible.enabled = true && modeEcriture;
					chargerActionPossible(produitSelectionnerPouAction.IDINV_ETAT);
				}						
			}else{
				try{
					btActionPossible.enabled = false;
					tabActionsPossible.source = null;
					tabActionsPossible.refresh();	
				}catch(e :Error){
					trace("tabActionsPossible est null");
				}
				
			}							 
		}
		
		//Cloture une opération avec lettrage
		private function cloturerOperationAvecLettrage(me : MouseEvent):void{			
			//faire un lettrage pour c
			tabAsup = new Array();
			tabAsup = ConsoviewUtil.extractIDs("IDINVENTAIRE_PRODUIT",myGridEltLettre.Donnees);
			selectedAction = 23 //VALIDER AVEC LETTRAGE
			selectedProduit = formateData(myGridProduitsOP.Donnees,"IDINVENTAIRE_PRODUIT");
			cloturerOperation(idOperation,selectedAction,selectedProduit,tabAsup,operationDetail[0].IDOPERATIONS_MAITRE); 
			
		}

		//Cloture une opération sans lettrage		
		private function cloturerOperationSansLettrage(me : MouseEvent):void{
			//faire action non lettre pour chaque idivenatire_produit du tab du haut	
			tabAsup = new Array();
			tabAsup = ConsoviewUtil.extractIDs("IDINVENTAIRE_PRODUIT",myGridEltLettre.Donnees);		
			selectedAction = 21 //VALIDER SANS LETTRAGE
			selectedProduit = formateData(myGridProduitsOP.Donnees,"IDINVENTAIRE_PRODUIT");
			
			//cloturer operation			
			cloturerOperation(idOperation,selectedAction,selectedProduit,tabAsup,operationDetail[0].IDOPERATIONS_MAITRE);
		}
		
		//---------------------------------------------------------------------------------------------------------------------------------
		
		//Effacer les lignes selectionnées du tableau GridFactProduitOperation 
		//la ligne va dans OperationReclam (en fait lettrage);
		private function effacerProduitOperation(me :MouseEvent):void{
			myGridProduitsOP.supprimerElementsSelectionnes();						
			deselectionnerLesGrids();
		}
		
						
		private function retablirProduitOperation(spe : SelectionProduitEvent):void{
			
			supprimerEltFacturationLettre(idOperation,formateData(spe.tabProduits,"IDDETAIL_FACTURE_ABO"));
		}
		
				private function processRetablirProduitOperation(element:*, index:Number, arr:Array):void{
			myGridProduitsOP.ajouterElements(element.IDINVENTAIRE_PRODUIT_OP);
		}
				
		//---------------------------------------------------------------------------------------------------------------------------------
		
		
		//Deselctionne les element des deux tableaux
		private function deselectionnerLesGrids():void{
			myGridFacture.myGridFacturation.selectedIndex = -1;
			myGridProduitsOP.myGridProduitsOp.selectedIndex = -1;
			
			myGridFacture.dispatchEvent(new SelectionProduitEvent("ProduitsFactSelectionnes"));
			myGridProduitsOP.dispatchEvent(new SelectionProduitEvent("ProduitsOPSelectionnes"));
		}
		
		
		//Extrait une colone d'un tableau et retourne le résultat sous forme de tableau
		//param in data le tableau source
		//		   colname le nom de la colonne à extraire
		private function formateData( data : Array , colname : String):Array{
			var s : String;
			var a : Array = new Array();
			for (var i : uint = 0 ; i < data.length ; i++ ){
				a.push(data[i][colname]);
			}
			return a;
		}
				
		/*------------------------------------------- ACTION -------------------------------------------*/
		
		//executer l'enregistrement d'une action apres confirmation
		private function executerEnregistrerAction(ce : ConfirmationEvent):void{		
			
			commentaireAction = ce.CommentaireConfirmation;				
			sauvegarderAction(selectedAction);
			
						
		}
		
		//Gere le click sur le bouton action
		private function doAction(me : MouseEvent):void{
			var message : String;			
			switch(selectedAction){
				case 22:{ 				
						message= "Vous devez sélèctionner un ou plusieurs éléments de facturation pour lettrer une ligne";
						break;	
				}
				
				case 24:{
						message = "Vous devez sélèctionner un ou plusieurs éléments de facturation pour faire une réclamation";					
						break;
				}
				case 26:{
						message= "Vous devez sélèctionner un ou plusieurs éléments de facturation pour lettrer une ligne";
						break;
				}
				
				default:{
					
				} 
			}
			trace("selectedAction : "+selectedAction);
			
			if(myGridFacture.myGridFacturation.selectedIndices.length < 1){
				Alert.okLabel = "Fermer";
				Alert.show(message);						
			}else{
			 	confirmbox = new ConfirmBox();
				confirmbox.addEventListener(ConfirmationEvent.CONFIRMATION,executerEnregistrerAction);
				confirmbox.title = "Confirmer!!";
				confirmbox.message = "Etes vous sur de vouloir Enregistrer l'Action ";
				confirmbox.action = btActionPossible.label;
				
				PopUpManager.addPopUp(confirmbox,this,true);					
			}			
		}
		
		//Initialise le menu du PopUpMenuButton	'Actions Possible'
		private function initMenu():void {
			myMenu = new Menu();
			myMenu.dataProvider = tabActionsPossible;
			myMenu.labelField = "LIBELLE_ACTION";
			try{
				myMenu.selectedIndex = 0;  	
				myMenu.addEventListener("itemClick", menuItemClickHandler);
			
				btActionPossible.label = myMenu.dataProvider[myMenu.selectedIndex].LIBELLE_ACTION;
				selectedAction = myMenu.dataProvider[myMenu.selectedIndex].IDINV_ACTIONS; 
			}catch(re : RangeError){
				trace("erreur d'index");
			}
				btActionPossible.popUp = myMenu;
		}

		//Definit le listener du menu du pop up bouton action 
		private function menuItemClickHandler(event:MenuEvent):void {
			var label:String = event.item.LIBELLE_ACTION; 
			var idaction : int = event.item.IDINV_ACTIONS;       				
			btActionPossible.label = label;				
			selectedAction = idaction; 
			myMenu.selectedIndex = event.index;
			trace(selectedAction);
			btActionPossible.close();	
			btActionPossible.toolTip = event.item.COMMENTAIRE_ACTION;					
		}
		
		//Formate les donnees du menu de PopUpMenuButton actions possibles
		private function formatTabActionPossible(d : ArrayCollection):ArrayCollection{
			var newData : ArrayCollection = new ArrayCollection();
			var ligne : Object;
			 for (ligne in d){
			 	if ((d[ligne].IDINV_ACTIONS != 21) && (d[ligne].IDINV_ACTIONS != 23)){
			 		newData.addItem(d[ligne]);
			 	} 
			 }
			return newData;
		}
		
/*--------------------------------------- FIN --- ACTION -------------------------------------------*/

		//Desactiver la page, la page est grisée
		private function disablePanel():void{			
			btCloturerAvecLettrage.enabled = false;
			btCloturerAvecLettrage.visible = false;
			btCloturerSansLettrage.enabled = false;
			btCloturerSansLettrage.visible = false;
			btActionPossible.enabled = false;
			btActionPossible.visible = false;			
			myGridEltLettre.enabled = false;
			myGridFacture.enabled = false;
			myGridProduitsOP.enabled = false;			
		} 
			
//----------------------------- REMOTING -----------------------------------------------//
			
		//Charge l'historique des actions pour une ressource
		private function chargerHistoriqueLigne():void{
					
 			opHistoOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"getHistoriqueProduitAction",
																			chargerHistoriqueLigneResultHandler);				
			RemoteObjectUtil.callService(opHistoOpe,
										 idOperation,	
										 produitSelectionnerPouAction.IDINVENTAIRE_PRODUIT);								
			
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerHistoriqueLigne' à l'ArrayCollection 'tabHistoEtap' 
		//Met à jour le tableau des ressources utilisés pour la réclamation
		//Et configure le bouton action possible en conséquence 
		private function chargerHistoriqueLigneResultHandler(re : ResultEvent):void{			
			tabHistoEtap = re.result as ArrayCollection;
			
			if (historiqueActionOk){
				myGridProduitsOP.mettreAJourGrid();	
				historiqueActionOk = false;	
				traiterConsequencesEtat(produitSelectionnerPouAction);
			} 	
			else historiqueActionOk = false;		
			 						
		}
		
		//--------
		
		//Charge les actions possibles en fonction d'un etat
		//param in idEtat l'identifiant de l'etat pour lequel on veut charger les actions possibles
		private function chargerActionPossible(idEtat : int):void{		
 			gestionDroit.addEventListener("actionsPossibleLoaded",chargerActionPossibleHandler);
 			gestionDroit.fournirListeActionsPossibles(gestionDroit.selectedProfil.IDPOOL,idEtat);
 			
 			/* opListeActionPossible = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"getListeActionPossibles",
																			chargerActionPossibleResultHandler);				
			RemoteObjectUtil.callService(opListeActionPossible,
										 idEtat); */
		}
		
		private function chargerActionPossibleHandler(event:Event):void
		{
			tabActionsPossible = formatTabActionPossible(gestionDroit.listeActionsPossibles);
			
			if(tabActionsPossible.length > 0)
			{
				callLater(initMenu);
			}
			else
			{
				btActionPossible.enabled = false;
			}			
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerActionPossible' à l'ArrayCollection 'tabActionsPossible' 
		//Rend le bouton action possible inactif si aucune action n'est possible
		//Initialise le menu du PopUpMenuButton des action possible
		private function chargerActionPossibleResultHandler(re : ResultEvent):void{			
			
			tabActionsPossible = formatTabActionPossible(re.result as ArrayCollection);	
			if (tabActionsPossible.length < 1) btActionPossible.enabled = false;	
			callLater(initMenu);				
		}
		
		//----------		
		
		//Pour la réclamation, enregistre une action pour la ressource sélectionnée
		//param in idAction l'identifiant de l'action que l'on veut enregistrer
		private function sauvegarderAction(idAction : int):void{		
			var flagEtat : int;	
 			opSaveAction = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"doAction",
																			sauvegarderActionResultHandler);				
			RemoteObjectUtil.callService(opSaveAction,
										 idOperation,
										 selectedProduit,
										 idAction,
										 tabHistoEtap[tabHistoEtap.length-1].IDINV_ACTIONS,
										 commentaireAction,
										 "",
										 tabHistoEtap[tabHistoEtap.length-1].ETAT_ACTUEL,
										 tabHistoEtap[tabHistoEtap.length-1].DANS_INVENTAIRE, 		
										 DateFunction.formatDateAsString(new Date())								 										 
										 )												
			
		}
		
		//Handler de la methode 'sauvegarderAction' 
		//si l'enregistrement c'est bien passé met a jour l'hitorique pour une ressource
		//si laction etait lettrer alors on enregistre les éléments qui ont servis au letrrage
		private function sauvegarderActionResultHandler(re : ResultEvent):void{			
			if (re.result > 0){
				historiqueActionOk = true;
				chargerHistoriqueLigne();					
				if((selectedAction == LETTREE)){					
					sauvegarderElementFacturation(idOperation,TMPtabProduitsFactSelectionnee);
				
				}else{
					
				
				}
			}else{
				historiqueActionOk = false;
				Alert.show("Une erreur c'est produite durant l'enregistrement");
			}			 
		}
		
		
		//Enregistre une opération de réclamation avec une liste de ressources
		//params in type_Operation le type d'operation qui entraine la réclamation
		//			tabProduits une tableau d'identifiants de ressources pour lesquelles on fait la réclamation
		private function enregistrerOperationReclamation(type_Operation : int, tabProduits : Array):void{	
				var idOpe : int = 0;
				var idOperation_maitre : int = idOperation;
				var flagEnCours : int = 1;//en cours
				var lblOperation : String = "Opération de reclamation suite à une l'opération de rapprochement n° "+numeroOperation;
				var txtCommentaire : String = "Opération de reclamation suite à une l'opération de rapprochement n° "+numeroOperation;
				var date_cloture : String = "";
				var idGroupe_client : int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				
 				opSaveOp = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"enregistrerOperation",
																				enregistrerOperationReclamationResultHandler);				

																	
				RemoteObjectUtil.callService(opSaveOp ,idOpe
													  ,type_Operation
													  ,lblOperation													  
													  ,flagEnCours
													  ,txtCommentaire
													  ,date_cloture
													  ,idOperation_maitre
													  ,idGroupe_client
													  ,tabProduits);
				
		}
		
		//Handler de la méthode 'enregistrerOperationReclamation'
		//en cas de succés on sauvegarde dans la réclamation la liste des lignes de facturations qui ont servies
		//à faire cette réclamation
		private function enregistrerOperationReclamationResultHandler(re : ResultEvent):void{
			 var newId : int = parseInt(re.result.toString());
			 if (newId > 0){
			  
			 	sauvegarderElementFacturation(newId ,TMPtabProduitsFactSelectionnee);				
				
			}else{
				Alert.show("Une erreur c'est produite durand l'enregistrement");
			}		
			dispatchEvent(new EnregistrerOperationEvent("EnregistrerOperationReclam"));	
		}			


		//------------------------

		//Enregistrement des lignes de facturations qui vont servir à la re=éclamation
		//param in 	idop l'identifiant de l'opération de réclamation
		//			tab un tableau contenant la liste des lignes de facturations qui vont servir à la réclamation
		private function sauvegarderElementFacturation(idop : int , tab : Array):void{
			opSaveOpFact = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"enregistrerFactureOperation",
																				sauvegarderElementFacturationResultHandler);				

																	
				RemoteObjectUtil.callService(opSaveOpFact ,idop, formateData(tab,"IDDETAIL_FACTURE_ABO"));
		
		}
		
		//Handler de la méthode 'sauvegarderElementFacturation'
		private function sauvegarderElementFacturationResultHandler(re : ResultEvent):void{
			var ok : int = parseInt(re.result.toString());
			deselectionnerLesGrids();	
			if(ok > 0){
				
				if (selectedAction == LETTREE){
				 //if (myGridEltLettre.ajouterItem(selectedProduit,TMPtabProduitsFactSelectionnee)){
						myGridFacture.supprimerElementsSelectionnes();	
						chargerElementGridEltLettre(idOperation);	
						//charger la liste des elements de inv_ligne_facturation pour le grid du bas
				} 	
		    }else{
				Alert.show("Une erreur c'est produite durand l'enregistrement");
			}	
		}	
		
		
		//-------------------------
		
		//charge les element de facturation pour une opération
		//param in idop l'identifiant de l'opeartion
		private function chargerElementGridEltLettre(idop : int):void{
			opListeFactLettree = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"getListFactureLettreeOperation",
																				chargerElementGridEltLettreResultHandler);				

																	
				RemoteObjectUtil.callService(opListeFactLettree ,idop);
		
		}
		
		//Handler de la méthode 'chargerElementGridEltLettre'
		//Affiche les données du tableau des lignes de facturations
		private function chargerElementGridEltLettreResultHandler(re : ResultEvent):void{			 
 			myGridEltLettre.setData(re.result as ArrayCollection);
		 	
			myGridFacture.rafraichir();
			 
		}
		
		//-----------------------------------		
		
		//Charge les informations d'une opération
		//param in idop l'identifiant de l'opération dont on veut charger les infos
		private function chargerInfoOperation(idop : int):void{		
	 		opDetailOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																			"getDetailOperation",
																			chargerInfoOperationResultHandler);				
			RemoteObjectUtil.callService(opDetailOpe,
									 idop);
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerInfoOperation' à l'ArrayCollection 'OperationDetail' 
		//charge les infos concernant l'operation maitre de la réclamation référencée par (OperationDetail)
		private function chargerInfoOperationResultHandler(re : ResultEvent):void{		
			operationDetail = re.result as ArrayCollection;	
			dateRef = DateFunction.formatDateAsString(new Date(re.result[0].DATE_REF_CALCUL)); //"01/11/2006";
			chargerInfoOperationMaitre(re.result[0].IDOPERATIONS_MAITRE);			
		}
		
		//Charge les informations d'une opération
		//param in idop l'identifiant de l'opération dont on veut charger les infos
		private function chargerInfoOperationMaitre(idop : int):void{		
	 		opDetailOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																			"getDetailOperation",
																			chargerInfoOperationMaitreResultHandler);				
			RemoteObjectUtil.callService(opDetailOpe,
									 idop);
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerInfoOperationMaitre' à l'ArrayCollection 'OperationMaitreDetail' 
		//Affiche le titre du module
		//Renseigne la date de référence 
		//Renseigne le type de l'operation maitre (résiliatio ou réclamation) typeOpmaitre
		//charge les infos concernant l'operation maitre de la réclamation référencée par (OperationDetail)
		//dispatch un evenement de type detailOpcharge signifiant que les info sur les operations sont chargées
		private function chargerInfoOperationMaitreResultHandler(re : ResultEvent):void{					
			typeOpmaitre = re.result[0].IDINV_TYPE_OPE;					
			OperationMaitreDetail = re.result as ArrayCollection;
			var typeOp :  String =  (typeOpmaitre == INVTYPE_OPERATION.RESILIATION)? "RESILIATION":"COMMANDE DE PRODUITS";
			title = " ** " + typeOp +" : "+OperationMaitreDetail[0].LIBELLE_OPERATIONS+ " ** Opération de Vérification **";
			lblEtatOperation.text = " Date de référence : "+ dateRef ;		
			dispatchEvent(new Event("detailOpcharge"));
		}
		
		//-------------------------------------------
		
		//Cloture une operation
		//param in idop l'identifiant de l'opération que l'on veut cloturer	
		//param    idAction l'identifiant de l'action de cloture
		//param    listeProduits tableau d'identifiant de ressource 
		//param	   listeEltFacure tableau de ligne de facturation à supprimer
		//param	   idopmaitre l'identifiant de l'opération maitre			
		private function cloturerOperation(idop : int,idAction : int,listeProduits : Array, listeEltFacure : Array,idopmaitre : int):void{
			opCloturerOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																			"cloturerOperationRapp",
																			cloturerOperationResultHandler);
																			
			RemoteObjectUtil.callService(opCloturerOpe,
										 idop,
										 idAction,
										 listeProduits,
										 listeEltFacure,
										 idopmaitre);																					
		}	
		
		//Handler de la methode 'cloturerOperation' 
		//en cas de succes :
		//met a jour les etats des ressources
		//change titre de la page
		//grise l'ihm		
		//Affiche une alerte d'erreur en cas d'echec
		private function cloturerOperationResultHandler(re : ResultEvent):void{
			if (parseInt(String(re.result)) == 1){		
				operationEnCours = 0;												
				lblEtatOperation.text = OPERATION_CLOSE;				
				myGridProduitsOP.mettreAJourGrid();	
				dispatchEvent(new Event("CloturerOperationRapp"));
				disablePanel();
			}else{
				Alert.show("Une erreur s'est produite lors de la cloture de l'operation");
			}	
		}	
		
		//-------------------------------------------------
		
		//supprime les élément qui ont servit au lettrage pour une opération
		//param in idop l'identifiant de l'operation
		//		   liste un tableau contenant les identifiant des ressources lettrées
		private function supprimerEltFacturationLettre(idop : int,liste : Array):void{
			opSupListeFactLettree = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																			"supprimerEltFacturationOperation",
																			supprimerEltFacturationLettreResultHandler);																			
			RemoteObjectUtil.callService(opSupListeFactLettree,
										 idop,liste);																					
		}	
		
		//Handler de la fonction 'supprimerEltFacturationLettre'
		//Met à jour les tableaux 
		private function supprimerEltFacturationLettreResultHandler(re : ResultEvent):void{
			if (parseInt(String(re.result)) > 0){
				//on rafraichit le GridEltLettre 
				chargerElementGridEltLettre(idOperation);	
				//on rafraichit le myGridFacture
				myGridFacture.initialiserGrid(idOperation,dateRef,typeOpmaitre);				
				myGridProduitsOP.initialiserGrid(idOperation,dateRef,typeOpmaitre);				
			}else{
				Alert.show("Une erreur s'est produite lors de la cloture de l'operation");
			}	
		}	
		
		[Bindable]
		public function set gestionDroit(value:GestionDroitCycleDeVie):void
		{
			_gestionDroit = value;
		}

		public function get gestionDroit():GestionDroitCycleDeVie
		{
			return _gestionDroit;
		}	

			
	}
}