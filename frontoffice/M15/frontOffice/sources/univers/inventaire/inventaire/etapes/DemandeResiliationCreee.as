package univers.inventaire.inventaire.creation.etapes
{
	import mx.events.FlexEvent;
	import mx.controls.Button;
	import flash.events.MouseEvent;
	import mx.events.CloseEvent;
	
	public class DemandeResiliationCreee extends EtapeBaseIHM
	{
		private var btValiderSorir : Button;
		private var btValiderEnvoiOperateur : Button;
		private var btAnnuler : Button;
		private var fenetreReduite : Boolean;
		
		
		public function DemandeResiliationCreee()
		{
			//TODO: implement function
			super();
			fenetreReduite = false;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		//initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{
			addEventListener(CloseEvent.CLOSE,adapterFenetreHandler);
			initBoutons();			
		}
		
		//adapte la taille de la fenetre la fenetre  
		private function adapterFenetreHandler(ce : CloseEvent):void{
			if (!fenetreReduite) {
				this.height = 40;
				fenetreReduite = true;
			}else {
				this.height = 250;
				fenetreReduite = false;
			} 
		}
		
		//ajout des boutons
		private function initBoutons():void{
			btValiderSorir = new Button();
			btValiderEnvoiOperateur = new Button();
			btAnnuler = new Button();
			
			btValiderSorir.label = "Valider et Sortir";
			btValiderEnvoiOperateur.label = "Valider et Suivant";
			btAnnuler.label = "Annuler";
			
			myboutonConeneur.addChild(btValiderEnvoiOperateur);
			myboutonConeneur.addChild(btValiderSorir);
			myboutonConeneur.addChild(btAnnuler);
			
			btValiderEnvoiOperateur.addEventListener(MouseEvent.CLICK,validerEnvoiOperateurHandler);
			btValiderSorir.addEventListener(MouseEvent.CLICK,valierSortirHandler);
			btAnnuler.addEventListener(MouseEvent.CLICK,annulerHandler);			
		}
		
		//valide l'etape et envoi ver opérateur 
		//Met a jour les donnees du formulaire
		//enregistre l'étape
		//affiche l'etape suivante
		private function btValiderEnvoiOperateurHandler(me : MouseEvent):void{
			
		}
		
		//valide l'etape et sort 
		//enregistre l'étape
		//Met a jour le formulaire et sort (retour a la page des Operations)
		private function valierSortirHandler(me : MouseEvent):void{
			
		}
		
		//Annulle l'étape et sort
		//retour à la page des Operations
		private function annulerHandler(me : MouseEvent):void{
			
		}
		
		//met a jour le formulaire
		private function remplirFormulaire():void{
			
		}
		
		//enregistre l'étape
		private function enregistrerEtape():void{
			
		}
		
		//affiche l'etape suivante
		private function afficherEtapeSuivante():void{
			
		}
		
		//retour à la page des Operations
		private function retourPageOperation():void{
			
		}
		
		
	}
}