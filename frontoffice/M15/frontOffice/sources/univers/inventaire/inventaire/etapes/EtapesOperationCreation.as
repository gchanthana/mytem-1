package univers.inventaire.inventaire.etapes 
{
	import composants.mail.MailBoxIHM;
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.tb.effect.EffectProvider;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.events.CollectionEvent;
	import mx.events.DataGridEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.inventaire.GestionDroitCycleDeVie;
	import univers.inventaire.inventaire.INVTYPE_OPERATION;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements.RapprochementsProduits;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements.RapprochementsProduitsIHM;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.CommandeExportable;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Contact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	import univers.inventaire.inventaire.creation.operationresiliation.EnregistrerOperationEvent;
	import univers.inventaire.inventaire.export.ExportBuilder;

	
	/**
	 * Classe gerant l'ecran de WorkFlow de type 'Création'
	 * */     
	public class EtapesOperationCreation extends EtapesOperationCreationSurLigne
	{	
		//Constante définissant l'identifiant de l'etat opération en cours
		private const OPERATION_EN_COURS : int = 1;
		
		//Constante définissant l'identifiant de l'etat opération cloturée
		private const OPERATION_CLOSE : int = 0;
		
		//Constante définissant une cloture sans Vérification
		private const SANS_RAPPROCHEMENT : int = 0;
		
		//Constante définissant une cloture avec Vérification
		private const AVEC_RAPPROCHEMENT : int = 1;
		
		//l'action que l'on a sélectionnée
		private var currentAction : Object;
		
		//Reference vers une date (la date de la derniere action)
		private var dateCurrentInfoEtape : Date;
		
		//Reference vers la méthode distante qui enregistre une opération de rapprochement
		private var opSaveOpe : AbstractOperation;			
		
		//Reference vers la méthode distante qui ramenes la listes des ressources de l'operation
		private var opListeProduits : AbstractOperation;
				
		//Reference vers la méthode distante qui ramene la liste des actions possibles
		private var opListeActionPossible : AbstractOperation;
		 
		//Reference vers la méthode distante qui ramene l'historique des actions pour une opération	
		private var opHistoOpe : AbstractOperation;
		
		//Reference vers la méthode distante qui enregistre une action
		private var opSaveAction : AbstractOperation;
		
		//Reference vers la méthode distante qui cloture une opération
		private var opCloturerOpe : AbstractOperation;
		
		//Reference vers la méthode distante qui le detail de l'opération
		private var opDetail : AbstractOperation;
		
		//Reference vers le détail d'une opération
		private var operationVo : OperationVO;
		
		//Reference vers la vignette vide du WorkFlow
		private var myEmptyFlag : EmptyEtapeFlag = new EmptyEtapeFlag();
		
		//Reference vers l'identifiant de l'action selectionée 
		private var selectedAction : int;
		
		//Le commentaire de l'action qui va être enregistrer
		private var commentaireAction : String;
		
		//La date de reference pour le calcul des régules (pour la création d'une opération de vérifiacation)
		private var dateRef : String;
		
		//Reference vers l'etat 'Dans Inventaire' des ressources de l'opération (1 = dans / 0 = hors)
		private var dansInventaire : int;
		
		//Reference vers vers l'identifiant de la ressource de reference pour l'historique
		private var idInventaireProduitRef : int;
		
		//ArrayCollection contenant l'historique des actions pour l'opération		 
		[Bindable]
		private var tabHistoEtap : ArrayCollection;
		
		//ArrayCollection contenant la liste des actions possibles
		[Bindable]
		private var tabActionsPossible : ArrayCollection;
		
		//ArrayCollection la liste des ressources de l'opération
		[Bindable]
		private var listeProduitOperation : ArrayCollection;
		
		//Le numero de l'opération	
		private var numeroOperation : String;
		
		//L'identifiant de l'opération
		private var idOperation : int;
		
		//L'identifiant de la commande liée
		private var idCommande : Number;
		
		//L'état de l'opération 'En Cours = 1'/	'Cloturée = 0'
		private var operationEnCours : int;
		
		
		//Le type de cloture a effectuer (avec ou sans rapprochement)
		private var typeCloture : int = SANS_RAPPROCHEMENT;
		
		//ArrayCollection contenant les references des vignettes affichées
		[Bindable]
		private var tabEtapeFlagsAffiches : ArrayCollection = new ArrayCollection();
		
		
		private var _gestionDroit:GestionDroitCycleDeVie;
		
		//Le nombre maximum de vignette que l'on peut afficher (dans la phrise WorkFlow)
		private const MAX_ETAPEFLAG_AFFICHES : int = 5;
		
		public function get modeEcriture():Boolean{
			return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE);
		}
		
		
		/**
		 * Constructeur
		 * @param numero le numero de l'opération de commande
		 * @param idOp l'identifiant de l'opération
		 * @enCours l'état de l'operation (1 = en cours, 0 = cloturée)
		 * */	
		public function EtapesOperationCreation(infosDemande : Object){
			//TODO: implement function
			super();
			
			Alert.okLabel = "Fermer";
			Alert.buttonWidth = 100;
				
			numeroOperation = infosDemande.REFERENCE_INTERNE;
			idOperation = infosDemande.IDINV_OPERATIONS;
			operationEnCours = infosDemande.EN_COURS;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			initCommande(infosDemande.IDCDE_COMMANDE);
			initPanier(infosDemande.IDCDE_COMMANDE);
					
		}
		
		/**
		 * Reset le composant
		 * Supprime les fenêtres surgissante
		 * */
		 public function clean():void{
		 	pnlContact.logOff();
		 }
		
		
		//Initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{
			listeActionsPossibles.dataProvider = tabActionsPossible;
			listeActionsPossibles.labelField = "LIBELLE_ACTION";
			listeActionsPossibles.dataTipField = "COMMENTAIRE_ACTION";	
			listeActionsPossibles.showDataTips = true;
			
			btValider.addEventListener(MouseEvent.CLICK,btValiderClickHandler);
			
			conteneurFlag.verticalScrollPolicy = "off";			
			tabEtapeFlagsAffiches.addEventListener(CollectionEvent.COLLECTION_CHANGE,rafraichirtabEtapeFlagsAffiches);		
			btSortir.addEventListener(MouseEvent.CLICK,sortir);
			DataGridColumn(myGridProduitOperation.columns[4]).labelFunction = formatDataTip;
			myGridProduitOperation.addEventListener(DataGridEvent.ITEM_EDIT_END,myGridProduitOperationItemEditEndHandler);
			chargerListeProduitOperation();		
			chargerDetailOperation();
			
			if (operationEnCours == OPERATION_CLOSE){
				main.removeChild(prochaineAction);
			}
			
			btValider.enabled = modeEcriture;
			dcDateAction.enabled = modeEcriture;
			btPDF.addEventListener(MouseEvent.CLICK,btPDFClickHandler);
			pnlEditInfos.commande = commande;	
			pnlEditInfos.modeEcriture = (gestionDroit.boolCtrlGestion == true && modeEcriture == true)?true:false;
		}
		
		
		private function btPDFClickHandler(event : MouseEvent):void{
			exporterLePdf();
		}
		
		//Affiche la commande au format PDF
		private function exporterLePdf():void{
			displayExport(Commande.FORMAT_PDF);
		}
		
		
		
		//Affiche la commande dans une nouvelle fenetre au format spécifié
		//param in format , le format sous lequel on souhaite afficher la commande
		private function displayExport(format : String):void {		
			
				 
			var commandeToExp : CommandeExportable = new CommandeExportable(commande,panier);				 
			var expB : ExportBuilder = new ExportBuilder(commandeToExp);
			expB.exporter(format);	
			
        } 		
		
		
		//formatage du telepone dans le grid
		private function formatDataTip(item : Object, column : DataGridColumn):String{
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}
		
		//ajoute une vignette d'etape a l'IHM
		//param in index l'index dans le tableau historique de l'etape que l'on veut afficher
		private function ajouterEtapFlag(index : int):void{
			var ef : EtapeFlag = new EtapeFlag(tabHistoEtap[index].IDINV_ACTIONS,tabHistoEtap[index].IDINV_OP_ACTION);			
			callLater(configFlag,[ef]);
		}
		
		
		//configure le nouveau flag et l'ajoute au tableux des vignettes affichées
		//param in ef une vignette
		private function configFlag(ef : EtapeFlag):void{
			
			ef.addEventListener("EtapeFlagComplete",traiterConsequencesEtat);
			tabEtapeFlagsAffiches.addItem(ef);	
			
			
			try{
				conteneurFlag.removeChildAt(conteneurFlag.numChildren-1);				
			}catch(e : Error){
				trace("Pas de flag a effacer");
			}					
			EffectProvider.FadeThat(conteneurFlag.addChild(ef));
			
			if (operationEnCours == OPERATION_EN_COURS){
				EffectProvider.FadeThat(conteneurFlag.addChild(myEmptyFlag));		
			} else if (tabHistoEtap.length != tabEtapeFlagsAffiches.length){
				myEmptyFlag = new EmptyEtapeFlag();								
				EffectProvider.FadeThat(conteneurFlag.addChild(myEmptyFlag));
				callLater(setTitreLastFlag);				
			}else{
				ef.supprimerFlecheSuivant();
			}
			
			try{
				mettreAJourFiche(ef);
			}catch(e : Error){
				trace("echec mis a jour fiche");
			}							
		}
		
		//Met le titre de la vignette vide
		private function setTitreLastFlag():void{
			myEmptyFlag.txtTitre.text = "Opération Close";
			myEmptyFlag.enabled = false;						
		}
		
		//charge les action possible pour l'etape et traite les consequence de l'etat
		protected function traiterConsequencesEtat(ev : Event):void{
			
			/// charge les actions possibles pour la derniere étape			
			try{
				var id : int = tabHistoEtap[(tabHistoEtap.length - 1)].IDINV_OP_ACTION;
				if (tabHistoEtap.length > 1){
						pnlContact.afficherMail();
				}			
				if ( id  ==  ev.currentTarget.IDINV_OP_ACTION) {
					if ((ev.currentTarget.ENTRAINE_CLOTURE == 1) &&
						(operationEnCours == 1) &&
						(ev.currentTarget.ENTRAINE_CREATION == 0) ){
						cloturerOperation();
						 
					}else if((ev.currentTarget.ENTRAINE_CREATION == 1)  && (operationEnCours == 1)){
						cloturerOperationAvecRapprochement();
														
					} 
					mettreAJourFiche(EtapeFlag(ev.currentTarget));		
				}
				
			}catch(e :Error){
				trace(e.message,e.getStackTrace());
			}		
		}
				
		//cloture l'operation avec rapprochement
		private function cloturerOperationAvecRapprochement():void{
			//Alert.show("Cette action Cloture l'operation et cree une nouvelle operation de rapprochement");
			typeCloture = AVEC_RAPPROCHEMENT;
			cloturerOperation();
		}
		
		//retrouve les info de l'etape dans l'historique via l'idAction
		//param in id un IDINV_OP_ACTION l'identifiant d'une action enregistrée
		//param out Object un objet contenant les infos sur une etape
		private function chercherIndexHistorique(id : int):Object{
			var infoEtape : Object;			
			
			for (var i : int = 0; i <  tabHistoEtap.length; i++){
				if (id == tabHistoEtap[i].IDINV_OP_ACTION ){					
					infoEtape = tabHistoEtap[i];
					return infoEtape;
				}
			}
			return infoEtape;
		}
		
		
		//retrouve l'index de l'etape dans l'historique via l'idAction 
		//param in id un IDINV_OP_ACTION l'identifiant d'une action enregistrée
		//param out index l'index de l'etatpe dans l'historique ou -1
		private function chercherIndexHistoriqueEtat(idopAction : int):int{			 		
			var i : int;
			for (i = 0; i <  tabHistoEtap.length; i++){
				if (id == tabHistoEtap[i].IDINV_OP_ACTION ){										 
					return i;
				}
			}			
			return -1;
		}
		
		//Met à jour les bornes du selecteur de date
		//param in id un IDINV_OP_ACTION l'identifiant d'une action enregistrée
		//Param out un object delimitant les bornes d'un date chooser ou date input
		private function setSelectableRange(idopAction : int):Object{
			var LAST_POSITION : int = tabHistoEtap.length - 1;
			var FIRST_POSITION : int = 0;  
			var positionIndex : int = chercherIndexHistoriqueEtat(idopAction);			
			var selectableRange : Object = new Object();	 
						
		 	
			try{
				selectableRange.rangeStart = new Date(tabHistoEtap[LAST_POSITION].DATE_ACTION);								
			}catch(re : RangeError){
				trace("[LAST_POSITION] pas d'étape avant celle ci ");
				selectableRange.rangeStart =  new Date(2006,0,1);
			}catch(e : Error){
				trace("[LAST_POSITION] erreur ");
			}finally{
				selectableRange.rangeEnd = new Date(2999,0,1); 
			}		
			return selectableRange;
		}	
		
		//met a jour la fiche de detail etape;
		//param in ef une vignette d'étape
		protected function mettreAJourFiche(ef : EtapeFlag):void{
										
			var LAST_POSITION : int = tabHistoEtap.length - 1;
			var infoEtape : Object = chercherIndexHistorique(ef.IDINV_OP_ACTION);
			var idopac : int = tabHistoEtap[(LAST_POSITION)].IDINV_OP_ACTION;
			
			if (( idopac  ==  ef.IDINV_OP_ACTION ) && !((ef.ENTRAINE_CLOTURE)||(ef.ENTRAINE_CREATION))){
				chargerActionPossible(ef.IDETAT);
				btValider.enabled = true && modeEcriture;					
			}else{
				btValider.enabled = false;
			}
						
			var dateAct : Date = new Date(infoEtape.DATE_ACTION);
			ef.setDate(dateAct);	
			dcDateAction.selectableRange = setSelectableRange(ef.IDINV_OP_ACTION);				
			if ((infoEtape.ETAT_ACTUEL == 1) && (operationEnCours == 1)){					
				ef.manageDureeColor(dateAct);										
			}	
			
			ef.setBackGroundColor(dateAct);
			
			if (ef.REF_CALCUL == 1){
				dateRef = DateFunction.formatDateAsString( new Date(infoEtape.DATE_ACTION));
			}
			
			ef.setColorHallow(Number(infoEtape.DANS_INVENTAIRE));		
			txtCommentaireProchaineAction.text = "";	
		}
		
		
		
		//Met à jour l'affichege des vignettes (WorkFlow)
		private function rafraichirtabEtapeFlagsAffiches(ce : CollectionEvent):void{
			if (tabEtapeFlagsAffiches.length > MAX_ETAPEFLAG_AFFICHES){
				EffectProvider.UnFadeThat(conteneurFlag.removeChildAt(0));
				tabEtapeFlagsAffiches.removeItemAt(0);										
			}	
		}
		
		//Gere le click sur le boutton 'Valider'
		//Enregistre l'action sélectionnée
		private function btValiderClickHandler(me : MouseEvent):void{
			var boolAction : Boolean = true;
			var boolDateAction : Boolean = true;			
			var message : String = "\n";						
			
			if (listeActionsPossibles.selectedIndex < 0){
				boolAction = false;
				message = message + "\n-Vous devez sélectionner une action.";
			}
			
			if(!dcDateAction.text){
				boolDateAction = false;
				message = message + "\n-Vous devez sélectionner une date pour l'action.";
			}
			
			if (boolAction && boolDateAction){						
				commentaireAction = txtCommentaireProchaineAction.text;
				currentAction = ObjectUtil.copy(listeActionsPossibles.selectedItem);
				switch(listeActionsPossibles.selectedItem.CODE_ACTION){
					case "RAPP" : rapprocherLignes();break;
					case "ANNUL" : ConsoviewAlert.afficherAlertConfirmation("Êtes vous sur de vouloir annuler cette opération","Demande de confirmation",annulerActionConfirmHandler);break;
					case "MAIL" : {
						if (_infosMail != null) _infosMail = null;
						
						_infosMail = new InfosObject();
						_infosMail.MAIL_EXPEDITEUR = CvAccessManager.getSession().USER.EMAIL;
						_infosMail.NOM_EXPEDITEUR = CvAccessManager.getSession().USER.NOM;
						_infosMail.PRENOM_EXPEDITEUR = CvAccessManager.getSession().USER.PRENOM;
						_infosMail.SOCIETE_EXPEDITEUR = CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE;
						
						_infosMail.commande = commande;
						_infosMail.panier = panier;
						_infosMail.listeProduitsOperation = listeProduitOperation;
						
						
						_infosMail.contact = pnlContact.getConatct();
						_infosMail.societe = pnlContact.getSociete();
			
						if (listeProduitOperation.length > 0){
							_infosMail.compte = listeProduitOperation[0].COMPTE;
							_infosMail.sousCompte = listeProduitOperation[0].SOUS_COMPTE;
						}else{
							_infosMail.compte = " ";
							_infosMail.sousCompte = " ";
						}
						
						afficherMailBox(_infosMail);
					};break;
					default : sauvegarderAction(listeActionsPossibles.selectedItem);break
				}
			}else{
				Alert.okLabel = "Fermer";
				Alert.show(message,"Erreur !");
			}
		}
		
		
		
		private function annulerActionConfirmHandler(ce : CloseEvent):void{
			if (ce.detail == Alert.OK){
				sauvegarderAction(listeActionsPossibles.selectedItem);	
			}
		}
		
		
		
		//Affiches toutes les étapes dans la frise apres une mise a jour
		private function afficherLesEtapes():void{
			//btModifier.enabled = false;
			conteneurFlag.removeAllChildren();
			tabEtapeFlagsAffiches.removeAll();
			
			var len : int = tabHistoEtap.length;
			var i : int;
			for (i = 0; i < len ; i++){
				ajouterEtapFlag(i);			
			}	
		}
		
		
		
		//Dispatche un évenement de type 'SortirSuivitOperationCrea' signifiant que l'on veut sortir du module
		private function sortir(me : MouseEvent):void{
			dispatchEvent(new Event("SortirSuivitOperationCrea"));			
		}
		
		protected function _commendeUpdateCompleteHandler(event:Event):void
		{
			dispatchEvent(new Event("mettreAjourGrid"));
		}
		//---------- COMMANDE ET PANIER -------------------------------------------------------------------------------------------------------------------------------------------
		private var _commande : Commande;
		public function set commande(value : Commande):void{
			_commande = value;
		}
		public function get commande():Commande{
			return _commande;
		}
		private var panier : ElementCommande;
				
		private function initCommande(idCmde: Number):void{
			 commande = new Commande();
			 commande.addEventListener(Commande.CREATION_COMPLETE,commandeCreationCompleteHandler);
			 commande.addEventListener(Commande.UPDATE_COMPLETE, _commendeUpdateCompleteHandler);
			 commande.addEventListener(Commande.CREATION_ERROR,commandeCreationErrorHandler);			 
			 commande.commandeID = idCmde;
			 commande.refresh();
		}
		
		private function initPanier(commandeId : Number):void{						
			if (panier != null) panier = null;
			panier = new ElementCommande();
			panier.prepareList(commandeId);
			panier.addEventListener(ElementCommande.LISTE_COMPLETE,panierListeCompleteHandler);
			panier.addEventListener(ElementCommande.LISTE_ERROR,panierListeErrorHandler);
			
		}
		
		//on charge le panier de la commande
		protected function commandeCreationCompleteHandler(ev : Event):void{
			var titre : String = "Historique.  Date livraison prevue : ";
			var dateLivP : String = (commande.dateLivraisonPrevue!=null)?DateFunction.formatDateAsString(new Date(commande.dateLivraisonPrevue)):"-";
			pnDetailEtape.title = titre + dateLivP;
			
			trace("(EtapesOperationCreation) Chargement de la commande terminé");			
		}		
		
		//commande et panier chargé -> on affiche la fentre des rapprochements
		protected function panierListeCompleteHandler(ev : Event):void{
			pnlContact.afficherContactCommande(commande,panier);
			
			trace("(EtapesOperationCreation) Chargement du panier terminé");
			if(listeProduitOperation != null){
				
				chargerListeProduitOperation("listeProduitOperationUpdateResultHandler");	
			}
		}
		
		
		protected function commandeCreationErrorHandler(ev : Event):void{			
			Alert.okLabel = "Fermer";
			Alert.show("Erreur lors du chargement de la commande associée","Erreur");	
		}
		
		
		//si le panier change alors on recharge les produits de l'opération
		protected function panierListeErrorHandler(ev : Event):void{
			Alert.okLabel = "Fermer";
			Alert.show("Erreur lors du chargement des articles de la commande associée.","Erreur");
		}
		
			
		//---------- MAIL -----------------------------------------------------------------------------------------------------------------------------------------------------------
		private var pUpMailBox : MailBoxIHM;
		private var _infosMail : InfosObject;
		
		protected function afficherMailBox(infosMail : InfosObject):void{
			if (pUpMailBox != null){
				if(pUpMailBox.hasEventListener(MailBoxImpl.MAIL_ENVOYE))pUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE,pUpMailBoxMailEnvoyeHandler);
				pUpMailBox = null;
			}
			
			
			var destinataire : Contact = pnlContact.getConatct();
						
			pUpMailBox = new MailBoxIHM();
			pUpMailBox.contact.contactID = commande.contactID;
			
			if(destinataire != null){
				pUpMailBox.initMail("WorkFlow","Commande",destinataire.email);
			}else{
				pUpMailBox.initMail("WorkFlow","Commande","----");
			}
			
			pUpMailBox.configRteMessage(infosMail,GabaritFactory.TYPE_COMMANDE_FIXEDATE);
			
			pUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE,pUpMailBoxMailEnvoyeHandler);
			
			PopUpManager.addPopUp(pUpMailBox,UIComponent(parentApplication),true);					
			PopUpManager.centerPopUp(pUpMailBox);
		}
				
		protected function pUpMailBoxMailEnvoyeHandler(ev : Event):void{
			if (operationVo != null){
			
				
				
				pnlContact.afficherContactCommande(commande,panier);
				
			} 
			
			if (commande != null){
				commande.contactID = pUpMailBox.contact.contactID;
				commande.societeID = pUpMailBox.contact.societeID;
				commande.flagMail = 1;
				commande.updateCommande();
				pnlContact.afficherContactCommande(commande,panier);
			}
			
			if (operationVo != null){
					
				operationVo.FLAG_MAIL = true;
				operationVo.IDCDE_CONTACT = pUpMailBox.contact.contactID;
				operationVo.IDCDE_CONTACT_SOCIETE = pUpMailBox.contact.societeID;
				operationVo.updateContact();				
			}
			
			
			pUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE,pUpMailBoxMailEnvoyeHandler);
			PopUpManager.removePopUp(pUpMailBox);
			pUpMailBox = null;
			
			
			sauvegarderAction(listeActionsPossibles.selectedItem);
			trace("EtapesOperationCreation)  Mail envoyé");
		}
		
		//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		//---------- RAPPROCHEMENT DES PRODUITS -------------------------------------------------------------------------------------------------------------------------------------
		private var pUpRapprochement : RapprochementsProduitsIHM;
			
		protected function rapprocherLignes():void{
			if(commande.commandeID > 0){
				
				pUpRapprochement = new RapprochementsProduitsIHM();
				pUpRapprochement.setCommande(commande,panier);
				pUpRapprochement.addEventListener(RapprochementsProduits.UPDATE_PRODUITS_RAPPROCHER,pUpRapprochementUpdateProduitsRapprocherHandler);
			    pUpRapprochement.addEventListener(RapprochementsProduits.SORTIR,pUpRapprochementSortirHandler);								
				pUpRapprochement.addEventListener(RapprochementsProduits.VALIDER,pUpRapprochementValiderHandler);
				 
				PopUpManager.addPopUp(pUpRapprochement,UIComponent(parentApplication),true);					
				PopUpManager.centerPopUp(pUpRapprochement);
			}			
		}
		
		protected function pUpRapprochementUpdateProduitsRapprocherHandler(ev : Event):void{
			
		}
		
		protected function pUpRapprochementSortirHandler(ev : Event):void{
			pUpRapprochement.removeEventListener(RapprochementsProduits.UPDATE_PRODUITS_RAPPROCHER,pUpRapprochementUpdateProduitsRapprocherHandler);
			pUpRapprochement.removeEventListener(RapprochementsProduits.SORTIR,pUpRapprochementSortirHandler);								
			pUpRapprochement.removeEventListener(RapprochementsProduits.VALIDER,pUpRapprochementValiderHandler);
			PopUpManager.removePopUp(pUpRapprochement);	
			pUpRapprochement = null;
		}
		
		protected function pUpRapprochementValiderHandler(ev : Event):void{
			pUpRapprochement.removeEventListener(RapprochementsProduits.UPDATE_PRODUITS_RAPPROCHER,pUpRapprochementUpdateProduitsRapprocherHandler);
			pUpRapprochement.removeEventListener(RapprochementsProduits.SORTIR,pUpRapprochementSortirHandler);								
			pUpRapprochement.removeEventListener(RapprochementsProduits.VALIDER,pUpRapprochementValiderHandler);
			PopUpManager.removePopUp(pUpRapprochement);	
			pUpRapprochement = null;
			
			sauvegarderAction(currentAction);
		}
		//---------- REMOTING -----------------------------------------------------------------------------------------------------------------------------------------------------
		
		//Charge la liste des ressources de l'opération		
		private function chargerListeProduitOperation(handlerFunctionName : String = "listeProduitOperationResultHandler"):void{
			opListeProduits= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.ElementDInventaire",
																			"getListeProduitsOperation",
																			this[handlerFunctionName]);				
			RemoteObjectUtil.callService(opListeProduits,
										 idOperation);					
			
			
		}
		
		private function listeProduitOperationUpdateResultHandler(re : ResultEvent):void{
			if (re.result){
				listeProduitOperation = re.result as ArrayCollection;
				if (listeProduitOperation.length > 0){
					idInventaireProduitRef = listeProduitOperation[0].IDINVENTAIRE_PRODUIT;	
					
				}
				myGridProduitOperation.dataProvider = listeProduitOperation;
				pnlContact.listeProduitsOperation = listeProduitOperation; 
				
			}
		}
		
		
		//Handler de la methode 'chargerListeProduitOperation' 
		//Met a jour l'ArrayCollection 'listeProduitOperation' et affiche le résultat dans un DataGrid
		//Charge l'historique des actions pour l'opération
		private function listeProduitOperationResultHandler(re : ResultEvent):void{				
			listeProduitOperation = re.result as ArrayCollection;
			
			myGridProduitOperation.dataProvider = listeProduitOperation;	
			pnlContact.listeProduitsOperation = listeProduitOperation; 		
			if (listeProduitOperation.length > 0){
				
				idInventaireProduitRef = listeProduitOperation[0].IDINVENTAIRE_PRODUIT;
				
				chargerHistoriqueOperation();	
			} 					
		}
		
		
		//Charge l'historique des actions pour l'opération		
		private function chargerHistoriqueOperation():void{		
			if (	idInventaireProduitRef > 0){
				opHistoOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"getHistoriqueProduitAction",
																			chargerHistoriqueOperationResultHandler);				
				RemoteObjectUtil.callService(opHistoOpe,
										 idOperation,	
										 idInventaireProduitRef);							
			
			}		
 			
		}
		
		
		//Handler de la methode 'chargerHistoriqueOperation'
		//Met a jour l'ArrayCollection 'tabHistoEtap' et affiche les vignettes du WorkFlow
		private function chargerHistoriqueOperationResultHandler(re : ResultEvent):void{			
			tabHistoEtap = re.result as ArrayCollection;
			afficherLesEtapes();								
		}
		
		
		//Charge la liste des actions possibles selon un état	
		//param in idEtat l'identifiant de l'état pour lequel on veut connaitre les actions possibles
		private function chargerActionPossible(idEtat : int):void
		{		
 			gestionDroit.addEventListener("actionsPossibleLoaded",chargerActionPossibleHandler);
 			gestionDroit.fournirListeActionsPossibles(gestionDroit.selectedProfil.IDPOOL,idEtat);
 			/* opListeActionPossible = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"getListeActionPossibles",
																			chargerActionPossibleResultHandler);				
			RemoteObjectUtil.callService(opListeActionPossible,
										 idEtat);	 */							
			
		}
		
		private function chargerActionPossibleHandler(event:Event):void
		{
			tabActionsPossible = gestionDroit.listeActionsPossibles;
			if(tabActionsPossible && tabActionsPossible.length > 0) 
			{
				dcDateAction.enabled = true;
				btValider.enabled = true;
			}
			else
			{
				dcDateAction.enabled = false;
				btValider.enabled = false;
			}
			listeActionsPossibles.dataProvider = tabActionsPossible;
			tabActionsPossible.refresh();
		}
		
		//Handler de la methode 'chargerActionPossible'
		//Met a jour l'ArrayCollection 'tabActionsPossible' et affiche cette liste
		private function chargerActionPossibleResultHandler(re : ResultEvent):void{			
			tabActionsPossible = re.result as ArrayCollection;	
			listeActionsPossibles.dataProvider = tabActionsPossible;
			tabActionsPossible.refresh();			
		}
		
		//Charge le détail d'une opération
		private function chargerDetailOperation():void{
			operationVo = new OperationVO(idOperation);
			operationVo.addEventListener(OperationVO.REFRESH_COMPPLETE,operationVoReady);
			/* opDetail= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																			"getDetailOperation",
																			chargerDetailOperationResultHandler);				
			RemoteObjectUtil.callService(opDetail,
										 idOperation);			 */		
			
			
		}
		
		//Handler de la mise à jour de l'opération
		//affiche le contact s'il y en a un
		private function operationVoReady(ev:Event):void{
			pnlContact.operation = operationVo;
			//pnOperation.title = "Opération n° " + operationVo.NUMERO_OPERATION + " / Ref. client : " + operationVo.REFERENCE_INTERNE + " / Ref. Opérateur : " + operationVo.REF_OPERATEUR ;
			if (operationVo.IDCDE_CONTACT > 0 ) pnlContact.setContacts(operationVo.IDCDE_CONTACT);				
			if (operationVo.IDCDE_CONTACT_SOCIETE > 0 ) pnlContact.setSociete(operationVo.IDCDE_CONTACT_SOCIETE);
		}
		
		//Handler de la methode 'chargerDetailOperation'
		//Met a jour l'objet 'operationVo' et affiche le contact s'il y en a un
		private function chargerDetailOperationResultHandler(re : ResultEvent):void{				
			if (re.result){
				operationVo  = re.result[0];
				if (operationVo.IDCDE_CONTACT > 0 ) pnlContact.setContacts(operationVo.IDCDE_CONTACT);
				if (operationVo.IDCDE_CONTACT_SOCIETE > 0 ) pnlContact.setSociete(operationVo.IDCDE_CONTACT_SOCIETE);
			} 	
								
		}
		
		//Enregistre l'action sélectionnée
		//param in  idAction l'identifiant de l'action que l'on veut enregistrer
		private function sauvegarderAction(action  : Object):void{
			
			var a : Array = listeProduitOperation.source;	
			var flagEtat : int;	
			var myDate : String = dcDateAction.text;
			 
			
			if (action.CODE_ACTION == "ANNUL"){
				//si le produit est entré dans l'inventaire
				//on le resort a la meme date
				var index : Number = ConsoviewUtil.getIndexById(tabHistoEtap,"DANS_INVENTAIRE",1);
											
				if (index > -1){
					myDate = DateFunction.formatDateAsString(new Date(tabHistoEtap[index].DATE_ACTION));	
				}	
				
			}
						
 			opSaveAction = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"doAction",
																			sauvegarderActionResultHandler);				
																			
			RemoteObjectUtil.callService(opSaveAction,
										 idOperation,
										 ConsoviewUtil.extractIDs("IDINVENTAIRE_PRODUIT",a),
										 action.IDINV_ACTIONS,
										 tabHistoEtap[tabHistoEtap.length-1].IDINV_ACTIONS,
										 commentaireAction,
										 "",
										 tabHistoEtap[tabHistoEtap.length-1].ETAT_ACTUEL,
										 tabHistoEtap[tabHistoEtap.length-1].DANS_INVENTAIRE,
										 myDate 										 										 
										 )	
		}
		 
		
		//Handler de la methode 'sauvegarderAction'
		//Si l'enregistrement c'est bien passé, charge l'historique des actions 
		private function sauvegarderActionResultHandler(re : ResultEvent):void{			
			if (re.result > 0){
				chargerHistoriqueOperation();				
			}else{
				Alert.buttonWidth = 100;
				Alert.okLabel = "Fermer";				
				Alert.show("Une erreur c'est produite durant l'enregistrement");
			}			 
		}
		
		
		//Cloture l'opération	
		private function cloturerOperation():void{
			opCloturerOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																			"cloturerOperation",
																			cloturerOperationResultHandler);
																			
			RemoteObjectUtil.callService(opCloturerOpe,
										 idOperation);																					
		}
		
		
		
		//Handler de la methode 'cloturerOperation'
		//Si la cloture c'est bien passé, grise le module et change le titre du module
		//En cas de cloture avec verification enregistre une operation de verificataion 
		private function cloturerOperationResultHandler(re : ResultEvent):void{
			if (parseInt(String(re.result)) == 1){
				switch (typeCloture){
					case SANS_RAPPROCHEMENT : {						
						operationEnCours = 0; 
						main.removeChild(prochaineAction);
						afficherLesEtapes();
						dispatchEvent(new Event("mettreAjourGrid"));
						break;
					} 
					case AVEC_RAPPROCHEMENT : {								
						operationEnCours = 0;																
						main.removeChild(prochaineAction);
						enregistrerOperationRapprochement();					
						break;
					}					
					default : {
						operationEnCours = 0;						
						main.removeChild(prochaineAction);
						break;
					}
				} 										
			}else{
				Alert.okLabel = "Fermer";
				Alert.buttonWidth = 100;
				Alert.show("Une erreur s'est produite lors de la cloture de l'operation");
			}	

		}
		
		
		//Enregistre une opération de contrôle de facturation
		private function enregistrerOperationRapprochement():void{

 			opSaveOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																			"enregistrerOperation",
																			enregistrerOperationResultHandler);	
			var date_cloture_bidon : String; 			
			var contactId : int  = 0;
			var societeId : int  = 0;
			 
			if (operationVo.IDCDE_CONTACT > 0 ) contactId = pnlContact.getConatct().contactID;				
			if (operationVo.IDCDE_CONTACT_SOCIETE > 0 ) societeId = pnlContact.getSociete().societeID;			

																
			RemoteObjectUtil.callService(opSaveOpe,0
											   ,INVTYPE_OPERATION.VERIF_FACT_COMMANDE											   
											   ,"[Commande] "+operationVo.LIBELLE_OPERATIONS+"_"+numeroOperation													  
											   ,1
											   ,"[Commande] "+operationVo.LIBELLE_OPERATIONS+"_"+numeroOperation	
											   ,date_cloture_bidon
											   ,idOperation
											   ,CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX											   
											   ,ConsoviewUtil.extractIDs("IDINVENTAIRE_PRODUIT",listeProduitOperation.source)
											   ,societeId
											   ,contactId
											   ,DateFunction.formatDateAsString(new Date())
											   ,operationVo.REFERENCE_INTERNE
											   ,operationVo.REF_OPERATEUR);
			
		}
		
		
		//Handler de la methode 'enregistrerOperationRapprochement'
		//Si l'enregistrement c'est bien passé, dispatche un évenement EnregistrerOperationEvent 
		//de type 'EnregistrerOperationRapprochement' signifiant que une opération de vérification à été enregistré
		private function enregistrerOperationResultHandler(re : ResultEvent):void{
			 if (parseInt(re.result.toString()) > 0){	
			 	var evtObj : EnregistrerOperationEvent = new EnregistrerOperationEvent("EnregistrerOperationRapprochement");
			 	evtObj.idOperation = parseInt(re.result.toString());			 		 	
				dispatchEvent(evtObj);			
			}else{
				Alert.okLabel = "Fermer";
				Alert.buttonWidth = 100;
				Alert.show("Une erreur c'est produite durand l'enregistrement");
			}
			
		}
		
		///-------------- RENOMER LA LIGNE ----------------------------------///
		protected function myGridProduitOperationItemEditEndHandler(event : DataGridEvent):void{
			if (myGridProduitOperation.dataProvider != null){
				
				//if (event.reason == DataGridEventReason.){
	    	        var newVal:String = TextInput(event.currentTarget.itemEditorInstance).text;
	                var oldVal:String = event.currentTarget.editedItemRenderer.data[event.dataField];
					var idSousTete : Number = Number(myGridProduitOperation.selectedItem.IDSOUS_TETE);
					var utility : RenameSousTete = new RenameSousTete(listeProduitOperation,panier != null ? panier.liste : null);
					utility.renameSousTete(oldVal,newVal,idSousTete);
				//}										
				
			}else{		
				event.preventDefault();					
			}   
		}	

		[Bindable]
		public function set gestionDroit(value:GestionDroitCycleDeVie):void
		{
			_gestionDroit = value;
		}

		public function get gestionDroit():GestionDroitCycleDeVie
		{
			return _gestionDroit;
		}	

		
	}
}