package univers.inventaire.inventaire.etapes
{
	import flash.events.Event;
	
	
	
	public class ConfirmationEvent extends Event
	{
		public static const CONFIRMATION : String = "confirmationEvent";
		private var _commentaireDeConfirmation : String;
		
		public function ConfirmationEvent(bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			
			super(CONFIRMATION, bubbles, cancelable);
		}
		
		public function set CommentaireConfirmation(s : String):void{
			_commentaireDeConfirmation = s;
		}
		
		public function get CommentaireConfirmation():String{
			return _commentaireDeConfirmation;
		}
		
	}
}