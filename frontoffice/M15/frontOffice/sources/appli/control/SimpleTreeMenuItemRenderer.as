package appli.control {
	import mx.controls.treeClasses.TreeItemRenderer;

	public class SimpleTreeMenuItemRenderer extends TreeItemRenderer {
		public function SimpleTreeMenuItemRenderer() {
			super();
			trace("(SimpleTreeMenuItemRenderer) Instance Creation");
		}
		
        public override function set data(value:Object):void {
			super.data = value;
			if(value != null) {
				if(value.@groupName != "menuUniversGroup") { // Noeud fonction
					var parentNode:XML = (value as XML).parent() as XML;
					if(parentNode.@enabled == false) // Si pas d'accès à l'univers qui le contient
						setStyle("color","#AA0000");
					else { // Accès à l'univers alors on utilise les accès de la fonction
						if(value.@enabled == true)
			        		setStyle("color","#00AA00");
			   			else
							setStyle("color","#AA0000");
					}
				} else { // Noeud univers
					if(value.@enabled == true)
		        		setStyle("color","#00AA00");
		   			else
						setStyle("color","#AA0000");
				}
			}
        }

        protected override function updateDisplayList(unscaledWidth:Number,unscaledHeight:Number):void {       
            super.updateDisplayList(unscaledWidth,unscaledHeight);
            if(super.data != null)
				super.label.text =  super.data.@label;
        }
	}
}
