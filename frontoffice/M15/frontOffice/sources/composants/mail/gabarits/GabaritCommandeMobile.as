package composants.mail.gabarits
{
	import composants.util.DateFunction;
	
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.collections.XMLListCollection;
	
	import univers.inventaire.commande.system.Commande;
	
	public class GabaritCommandeMobile extends AbstractGabarit
	{
		
		private var _commande:Commande;
		private var _listeArticles:XML;
		
		public function GabaritCommandeMobile()
		{
			super();
		}
		
		override public function getGabarit():String
		{
			_commande = infos.commande;
			_listeArticles = infos.listeArticles;
			
			var listeLignes : String = "";
			if (_listeArticles != null)
			{
				var collection:XMLListCollection = new XMLListCollection(_listeArticles.children());
				listeLignes = printListeLignes(collection);
			}
			
			var cnom : String = (_commande.PATRONYME_CONTACT != null )? _commande.PATRONYME_CONTACT : "";
			var cemail: String = (_commande.EMAIL_CONTACT!=null)?_commande.EMAIL_CONTACT:"";
			var csociete : String = (_commande.LIBELLE_REVENDEUR !=null)?_commande.LIBELLE_REVENDEUR:"";
			var refclient : String = (_commande.REF_CLIENT!=null)?_commande.REF_CLIENT:"";
			var refOperateur : String = (_commande.REF_REVENDEUR!=null)?_commande.REF_CLIENT:"";
			var compte : String = (_commande.LIBELLE_COMPTE!=null)?_commande.LIBELLE_COMPTE:"";
			var sousCompte : String = (_commande.LIBELLE_SOUSCOMPTE!=null)?_commande.LIBELLE_SOUSCOMPTE:"";
			var corpMessage : String = (infos.corpMessage!=null)?infos.corpMessage:"";
			
			
			 
			return	"<b>"+infos.NOM_EXPEDITEUR+" "+infos.PRENOM_EXPEDITEUR +"</b>"
					+"<br/>"+infos.MAIL_EXPEDITEUR
					+"<br/><u>Date</u> : "+DateFunction.formatDateAsString(new Date())
					+"<br/>Société : <b>"+infos.SOCIETE_EXPEDITEUR+"</b>"
					+"<br/>"		
					+"<br/>Libellé de la demande <b>" + _commande.LIBELLE_COMMANDE+"</b>"
					+"<br/>Numéro de la demande <b>" + _commande.NUMERO_COMMANDE+"</b>"
					+"<br/>Référence client : <b>" + refclient +"</b>"
					+"<br/>Référence revendeur/opérateur : <b>" + refOperateur +"</b>"
					+"<br/>"
					+"<br/>"
					+"<br/>"
					+"<br/>"
					+ corpMessage.replace("*numerocommande*",_commande.NUMERO_COMMANDE).replace("*listelignes*",listeLignes)
					+"Cordialement,"
					+"<br/>"	
					+"<b>"+infos.NOM_EXPEDITEUR+" "+infos.PRENOM_EXPEDITEUR +".</b>"
		}
		
		protected function printListeArticles(liste : ICollectionView):String
		{
			var output : String = "<br/>";
			/* 
			if (liste != null){
				var cursor:IViewCursor = liste.createCursor();
				//loop over articles
				while(!cursor.afterLast)
				{
					var article:XML = (cursor.current as XML);
					
					
					var ligne : Object = liste[i];
					var equipements : String = ligne.LIBELLE_PRODUIT;
					var ressources : String = ligne.LIBELLE_PRODUIT;
					var numligne : String = ligne.SOUS_TETE;		
				}	
				}
			} */
			return output;
		}
		
		protected function printListeLignes(liste:ICollectionView):String
		{
			var output : String = "<br/>";
			if (liste != null){
				var cursor:IViewCursor = liste.createCursor();
				//loop over articles
				while(!cursor.afterLast)
				{
					var article:XML = (cursor.current as XML);
					var numligne : String = article.soustete[0];
					output = output + numligne + "<br/>"
					cursor.moveNext();
				}
				output = output + "<br/>";
			}
			
			return output;		
		}
	
	}
}