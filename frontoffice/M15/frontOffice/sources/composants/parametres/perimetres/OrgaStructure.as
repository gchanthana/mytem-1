package composants.parametres.perimetres
{
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	import flash.events.MouseEvent;
	import mx.controls.Alert;
	import mx.events.CollectionEvent;
	import mx.utils.ArrayUtil;
	import mx.controls.Tree;
	import mx.controls.Menu;
	import flash.geom.Point;
	import mx.events.MenuEvent;
	import mx.effects.Move;
	import composants.util.ConsoviewUtil;

	
	public dynamic class OrgaStructure extends OrgaStructureIHM
	{
		private var _dataOraganisation:ArrayCollection;
		private var _refreshTreeFunc:Function;
		private var _searchNodeFunc:Function;
		private var _searchResult:Array;
		protected var _lastSelectedItem:Number;
		protected var _contextMenu:ArrayCollection;
		private var _filterMenu:Menu;
		private var menuFilterData:XML;
		public var _treeNodes:XMLList;
		public var _searchExecQuery:Boolean;
		public var _searchItemIndex:Number;
		public var _contextList:ArrayCollection; 
		public var _searchTreeResult:XMLList;
		public var _parentRef:Object;
		
		
		public function OrgaStructure()
		{
			this._searchExecQuery = true;
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);	
		}
	
		public function initIHM(event:Event):void
		{
			btnValiderRecherche.addEventListener(MouseEvent.CLICK, onbtValiderRechercheClicked);
			txtRechercheOrga.addEventListener(Event.CHANGE, onRechercheOrgaChange);
		 	cmbOrganisation.addEventListener(Event.CHANGE, onOrganisationChange);
			treePerimetre.addEventListener(Event.CHANGE, onTreeSelectionChange);
			txtRechercheOrga.addEventListener(FlexEvent.ENTER, onbtValiderRechercheClicked);
		}
		
		
		/** ######################### EVENTS #########################*/
		
				
		protected function onTreeSelectionChange(event:Event):void
		{
			try {
				parentDocument.treeSelectionChanged();
			}
			catch (e:Error) {}
			var e:Event = new Event("treeSelectionChanged");
			dispatchEvent(e);
		}
		
		protected function onfilterCmbClicked(event:MouseEvent):void
		{
			var point1:Point = new Point();
			 point1.x=btnFilterCmb.x;
             point1.y=btnFilterCmb.y;                
             point1=btnFilterCmb.localToGlobal(point1);
             _filterMenu.show(point1.x, point1.y + 22);
			
		}
		
		
		protected function onfilterCmbChange(event:MenuEvent):void
		{
			 for (var i:Number = 0; i < _filterMenu.dataProvider.length; i++)
				_filterMenu.dataProvider[i].@enabled = true;
			_filterMenu.dataDescriptor.setToggled(_filterMenu.selectedItem,false);
			
				
			if(checkTypesOrgas(event.item.@eventName)||(event.item.@eventName == "ALL")){
				
				/*  for (var i:Number = 0; i < _filterMenu.dataProvider.length; i++)
				_filterMenu.dataProvider[i].@enabled = true; */
				
				_filterMenu.dataDescriptor.setToggled(event.currentTarget.selectedItem,true);
				_filterMenu.dataDescriptor.setEnabled(event.currentTarget.selectedItem,false);
				
				//event.item.@enabled = false;		
				(cmbOrganisation.dataProvider as ArrayCollection).filterFunction = filterDataprovider;	
				cmbOrganisation.dataProvider.refresh();
				cmbOrganisation.selectedIndex = 0;				
				onOrganisationChange(new Event("refresh"));	
			}else{				
				Alert.show("Pas d'organisation de type " + event.item.@label,"Info.");				
				_filterMenu.selectedIndex = 0;
				_filterMenu.dataDescriptor.setToggled(event.currentTarget.selectedItem,true);
				_filterMenu.dataDescriptor.setEnabled(event.currentTarget.selectedItem,false);
				event.preventDefault();
			}
		}
		
		private function checkTypesOrgas(type_orga:String):Boolean{
			if (ConsoviewUtil.isLabelInArray(type_orga,"TYPE_ORGA",(cmbOrganisation.dataProvider as ArrayCollection).source)) return true;
			else return false;		
		}
		
			
		
		/** Refresh the data of the tree */
		protected function onOrganisationChange(e:Event):void
		{
			treePerimetre.dataProvider = null;
			treePerimetre.selectedItem = null;
			onRechercheOrgaChange(new Event("refresh"));
			var ex:Event = new Event("organisationChanged");
			dispatchEvent(ex);
			try
			{
				if (_parentRef != null)
					_parentRef.organisationChanged();
			}
			catch (e:Error) {}
			 refreshTree();		
		}
		
				/** Send a request to serverside with the selected Node Value and The input value */
		protected function onbtValiderRechercheClicked(result:Event):void
		{
			if (_searchExecQuery)
			{
				var name_groupe:String = txtRechercheOrga.text;
				searchNode(name_groupe);
			}
			else
				selectNextNode();
		}
		
		/** ####################### END EVENTS #######################*/
		
		/** used to select the last Select item before refreshing the tree */
		protected function renderDone(event: Event):void
		{
			if (_lastSelectedItem > 0)
			{
				selectNodeByAttribute("VALUE", _lastSelectedItem.toString());
				_lastSelectedItem = -1;
			}
		}
		
		private function filterDataprovider(item:Object):Boolean
		{
			try
			{
				var filter:String = _filterMenu.selectedItem.@eventName;
				if (filter == "ALL")
					return true;
				if (filter == "NAN"&& item.TYPE_ORGA != "ANA")
					return true;
				if (item.TYPE_ORGA == filter)
					return true;
				return false;
			}
			catch (error:Error) {}
			return true;
		}
		
		private function initFilterContextMenu():void
		{
			_filterMenu = Menu.createMenu(this, menuFilterData, false);
			_filterMenu.labelField="@label";
		}
	
		/** ##################### PUBLIC METHODS #####################*/
		
		public function getTreeRef():Tree
		{
			return treePerimetre;
		}
		
		public function getMover():Move
		{
			return myMove;	
		}
		
		public function setParentRef(ref:Object):void
		{
			_parentRef = ref;	
		}
		
		/** used to find nodes by sending request */ 
		public function searchNode(nodeName:String):void {	}
		
		/** used to get the nodes of the tree by sending request */
		public function refreshTree():void { }
		
		public function setContextMenu(data:ArrayCollection):void
		{
			_contextMenu = data;
			treePerimetre.setContextMenu(data);
		}
		
		/** Set an array corresponding to the result of the search */
		public function setDataResult(data:Array):void
		{
			 _searchResult = data;
			 _searchItemIndex = 0;
			 if (_searchResult.length > 0)
				 selectNextNode();
			 _searchExecQuery = false;
		}
		
		/** Select the next found node */
		public function selectNextNode():void
		{
			if (_searchItemIndex >= _searchResult.length)
				return ;
			selectNodeByAttribute("VALUE", _searchResult[_searchItemIndex].IDGROUPE_CLIENT);
			//trace(txtRechercheOrga.text + " -- " + getSelectedItem().@LABEL);
			_searchItemIndex++;
			if (_searchItemIndex >= _searchResult.length)
				_searchItemIndex = 0;
			try
			{
				if (getSelectedItem().@LABEL.search(txtRechercheOrga.text) <= -1)
					parentDocument.onbtAfficherLignesClicked(new Event(""));
			}
			catch (error:Error) {}
		}
		
		/** Select an item in the tree by the value of his attribute */
		public function selectNodeByAttribute(attribute:String, value:String):void
		{	
			var dept:XMLList;
			dept = _treeNodes.descendants().(@[attribute] == value);
			if (dept[0] == null)	
				return ;
			var pr:Object = dept[0];
			while ((pr = pr.parent()) != null)
				treePerimetre.expandItem(pr, true);
			treePerimetre.selectedItem = dept[0];
			treePerimetre.scrollToIndex(treePerimetre.selectedIndex);
			/*try {
				parentDocument.treeSelectionChanged();
			}
			catch (e:Error) { }*/
			onTreeSelectionChange(new Event(""));
		}
		
		public function getSelectedItem():Object
		{
			return treePerimetre.selectedItem;
		}
			
		public function getSelectedTreeItemID():Number
		{
			try	{
					return parseInt(treePerimetre.selectedItem.@ID);
			} catch (error:Error)	{ 
				//Alert.show("Aucun noeud n'est sélectionné.");
			}
			return -1;
		}
		
		public function getSelectedTreeItemValue():Number
		{
			try	{
					return parseInt(treePerimetre.selectedItem.@VALUE);
			} catch (error:Error)	{ 
				//Alert.show("Aucun noeud n'est sélectionné.");
			}
			return -1;
		}
		
		public function getSelectedOrganisationValue():Number
		{
			try	{
				return parseInt(cmbOrganisation.selectedItem.IDGROUPE_CLIENT);
			} catch (error:Error)	{ }
			return -1;
		}
		
		public function getSelectedOrganisation():Object
		{
			try {
				return cmbOrganisation.selectedItem;
			} catch (error:Error) {}
			return -1;
		}
		
		public function getSelectedTreeIndex():Number
		{
			return treePerimetre.selectedIndex;
		}
		
		public function addOrganisationInArray(item:Object):void
		{
			_dataOraganisation.addItem(item);
			
		}
		
		public function clearSearch():void
		{
			_searchExecQuery = false;
			txtRechercheOrga.text = "";
		}
		
		public function getSelectedTypeOrga():String
		{
			try
			{
				return cmbOrganisation.selectedItem.TYPE_ORGA;	
			}
			catch (e:Error) { }
			return null;
		}
		
		public function onRechercheOrgaChange(event: Event):void
		{
			_searchExecQuery = true;	
		}
		
		/** Set the organisation list from the request in the dataProvider of the combo */
		public function setCmbData(data:ArrayCollection, label:String):void
		{
			_dataOraganisation = data;
			cmbOrganisation.dataProvider = _dataOraganisation;
			cmbOrganisation.dataProvider.filterFunction = filterDataprovider;
			cmbOrganisation.dataProvider.refresh();
			cmbOrganisation.labelField = label;
			refreshTree();
		}
		
		/** Set the tree xml data in the dataprovider of the tree */
		public function setTreeData(data:XMLList, label:String):void
		{
			var _x : Number = treePerimetre.x;
			var _y : Number = treePerimetre.y;
			
			cvTreePerimetre.removeChild(treePerimetre);
			treePerimetre = null;
			
			
			
			treePerimetre = new specialTree();
			treePerimetre.x= _x;
			treePerimetre.y= _y;
			treePerimetre.percentWidth = 100;
			treePerimetre.percentHeight = 100;
			treePerimetre.showRoot = true;
			treePerimetre.dataProvider = data;
			treePerimetre.labelField = label;
			if (_contextMenu != null) treePerimetre.setContextMenu(_contextMenu);
			cvTreePerimetre.addChild(treePerimetre);
			
			
			
						
			if (treePerimetre.selectedIndex > 0)
			{
				_lastSelectedItem = treePerimetre.selectedItem.@VALUE;			
				treePerimetre.dataProvider.refresh();				
				treePerimetre.addEventListener(Event.RENDER, renderDone);
			}
			
			
		}
		
		
		
		public function setXMLFilter(value:XML):void
		{
			menuFilterData = value;
			initFilterContextMenu();
			btnFilterCmb.addEventListener(MouseEvent.CLICK, onfilterCmbClicked);
			_filterMenu.addEventListener(MenuEvent.ITEM_CLICK, onfilterCmbChange);
		}
		/** ################### END PUBLIC METHODS ###################*/
	}
}