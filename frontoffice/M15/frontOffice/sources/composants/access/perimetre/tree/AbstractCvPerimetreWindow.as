package composants.access.perimetre.tree {
	import mx.containers.Box;
	import mx.events.FlexEvent;

	public class AbstractCvPerimetreWindow extends Box {
		public var perimetreTree:IPerimetreTreeWindow; // Composant contenant l'IHM de l'arbre des périmètres
		public var searchTree:ISearchPerimetreWindow; // Composant contenant l'IHM de l'arbre de recherche
		
		public function AbstractCvPerimetreWindow() {
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			trace("(AbstractCvPerimetreWindow) Instance Creation");
		}
		
		protected function initIHM(event:FlexEvent):void {
			trace("(AbstractCvPerimetreWindow) Performing IHM Initialization");
		}
	}
}