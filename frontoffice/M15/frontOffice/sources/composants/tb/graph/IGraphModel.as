package composants.tb.graph
{
	import mx.collections.ArrayCollection;
	
	/**
	 * Interface des composants de type Graphique 
	 **/
	public interface IGraphModel
	{
		/**
		 * Met à jour les données du graph
		 **/
		function update():void;
		
		/**
		 * Pour visualiser les données sous forme de 'Chart'
		 **/
		function showChart():void;
		
		/**
		 * Pour visualiser les données sous forme de 'Grid'
		 **/
		function showGrid():void;
		
		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...) 			
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		function updatePerimetre(perimetre : String = null, modeSelection : String = null, identifiant : String= null):void;
		  
		/**
		 * Met à jour le segment
		 * */		 
		function updateSegment(segment : String = null):void;
		
		/**
		 * Met à jour la periode
		 * */		 
		function updatePeriode(moisDeb : String = null, moisFin : String = null):void;
		
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		function clean():void;
		
			
		
		/**
		* Permet de passer les valeurs au  composant. 
		* @param d Une collection de tableau. 
		**/	
		function set dataProviders(d : ArrayCollection):void;
				
		function get dataProviders():ArrayCollection;
			
	}
}