package composants.tb.graph
{
	import mx.events.FlexEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	import composants.tb.graph.charts.PieGraph_IHM;
	import mx.rpc.events.ResultEvent;
	import mx.controls.Alert;
	 
	import mx.events.CloseEvent;
	import mx.rpc.events.HeaderEvent;
	import flash.events.MouseEvent;
	import mx.controls.dataGridClasses.DataGridColumn;
	
	public class GarphCoutConso extends PieGraphBase  
	{
		private var op : AbstractOperation;
		
		[Bindable]
		private var _dataProviders : ArrayCollection;
		
		[Bindable]
		private var _dataProviders2 : ArrayCollection;
								
		private var _segment : String;  
		
		
		
		/**
		 * Constructeur
		 **/
		public function GarphCoutConso(segment : String = "Complet")
		{
			super();
			setSegment(segment);
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
				
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		override public function clean():void{		
			graph.myPie.dataProvider = null;
			myGrid.dataProvider = null;
			
		}	
				
		/**
		* Permet de passer les valeurs au composant.		
		* @param d Une collection d'objets. 
		**/						
		override public function set dataProviders(d : ArrayCollection):void{
			_dataProviders = _formatterDataProvider(d);
			_dataProviders2 = _formatterDataProviderPie(d);
			updateProviders();
		}
		
		/**
		*  Retourne le dataproviders
		**/	
		[Bindable]			
		override public  function get dataProviders():ArrayCollection{
			return _dataProviders;
		}	
		
		/**
		 * Met à jour les donées du tableau
		 **/
		override public function update():void{			
			if (_segment.toUpperCase()=="COMPLET") {
				chargerDonnees(); 	
			} else {
				chargerDonneesBySegment(); 	
			}			 
		}	
		 
		public function affectSwhitchHandler(methode : Function):void{
			 addEventListener(CloseEvent.CLOSE,methode);
		}
		
		public  function removeSwitchHandler(methode : Function):void{
			removeEventListener(CloseEvent.CLOSE,methode);
		}
		
		/*---------- PRIVATE --------------------*/
		protected override function _formatterDataProvider(d: ArrayCollection):ArrayCollection{		
			var tmpCollection : ArrayCollection = new ArrayCollection();	
			var ligne : Object;				
			
			_total = 0;
			for	(ligne in d){	
				if (String(d[ligne].type_theme).toUpperCase() == "CONSOMMATIONS")	
					tmpCollection.addItem(formateObject(d[ligne]));					   
			}
			
			var totalObject : Object = new Object();
			totalObject["Libellé"] = "TOTAL";
			totalObject["Montant"] = cf.format(_total);
			tmpCollection.addItem(totalObject);			
					
			return tmpCollection;
		}	
		
		/* protected function _formatterDataProviderPie(d: ArrayCollection):ArrayCollection{		
			var tmpCollection : ArrayCollection = new ArrayCollection();	
			var ligne : Object;				
						
			_total = 0;
			
			for	(ligne in d){
				 
				if (String(d[ligne].type_theme).toUpperCase() == "CONSOMMATIONS")	
					tmpCollection.addItem(formateObjectPie(d[ligne]));					   
			}			
			if(_total == 0){
				return null;
			}	
			return tmpCollection;
		}	 */
		protected function _formatterDataProviderPie(d: ArrayCollection):ArrayCollection{		
			
			var tmpCollection : ArrayCollection = new ArrayCollection();	
			var ligne : Object;				
			var len : int = d.length;
			var obj : Object = new Object();
			var chartData : ArrayCollection = new ArrayCollection();
			
						
			_total = 0;
			
			//////////////////formatage des données
			for	(ligne in d){	
				if (String(d[ligne].type_theme).toUpperCase() == "CONSOMMATIONS")	
					tmpCollection.addItem(formateObjectPie(d[ligne]));					   
			}
			/////////////////
			chartData = ObjectUtil.copy(tmpCollection) as ArrayCollection;
			
			/////////////////On coupe s'il y a trop de données
			if (chartData.length > 10){
				var sum : Number  = 0;
			
				for (var i : int = 9 ; i < len; i++){
					obj = chartData.getItemAt(i);
					sum = sum + parseFloat(obj["Montant"]);					
				}
				
				var len2 : int =  chartData.length ;
				
				for (var j : int = 10; j < len2 ; j++){
					chartData.removeItemAt(10);
				} 	
										
				chartData.getItemAt(9)["Libellé"] = "Autres...";
				chartData.getItemAt(9)["Montant"] = sum;					
			 	
				
				 return chartData;
			}
			
			
			
			////////////////// si la somme des montant est null on retourne rien
			if(_total == 0){
				return null;
			}
			
			////////
			return tmpCollection;
		}		
		
		protected function formateObject(obj : Object):Object{
			var o : Object = new Object();
				
			_total = _total + parseFloat(obj.montant_final);		
			switch(String(obj.type_theme).substring(0,3).toUpperCase()){
				case "ABO" : o["Libellé"] = obj.nom + " " + String(obj.type_theme).substring(0,3) + ".";break;	
				case "CON" : o["Libellé"] = obj.nom + " " + String(obj.type_theme).substring(0,5) + ".";break;
			}
			
			o["Type de produit"] = obj.type_theme;
			o["Opérateur"] = obj.nom;
			o["Montant"]= cf.format(obj.montant_final);											
			return o;
		}	
		
		protected function formateObjectPie(obj : Object):Object{
			var o : Object = new Object();
				
			_total = _total + parseFloat(obj.montant_final);		
			
			
			switch(String(obj.type_theme).substring(0,3).toUpperCase()){
				case "ABO" : o["Libellé"] = obj.nom + " " + String(obj.type_theme).substring(0,3) + ".";break;	
				case "CON" : o["Libellé"] = obj.nom + " " + String(obj.type_theme).substring(0,5) + ".";break;
			}
			o["Type de produit"] = obj.type_theme;
			o["Opérateur"] = obj.nom;			
			o["Montant"]= Math.abs(parseFloat(obj.montant_final));						
			return o;
		}	
		
		
		 
		/*---------------- PRIVATE --------------------------------------------------------*/
		
		private function init(fe : FlexEvent):void{
			title = "Répartition des Coûts des consommations / ....";
			//showCloseButton = true;	
			//addEventListener(CloseEvent.CLOSE,closeIt);
			titleBar.enabled = true;	
		 	titleBar.addEventListener(MouseEvent.CLICK,closeIt);	
		 	titleBar.useHandCursor = true;
		 	titleBar.buttonMode = true;			
		 	titleBar.mouseChildren = false;		
		}
		
		private function closeIt(ev : Event):void{
			dispatchEvent(new Event("switchGraphEvent"));		
		}
		
		private function updateProviders():void{
			myGrid.dataProvider = dataProviders;
			DataGridColumn(myGrid.columns[1]).setStyle("textAlign","right"); 				
			graph.callLater(updatePie);
		}
		
		private function updatePie():void{
			graph.myPie.dataProvider = _dataProviders2;
		}
		
		private function setSegment(segment : String):void{
			if (segment == "null")
				_segment = "Complet";
			else
				_segment = segment;
		}
		
		 
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		protected override function chargerDonnees():void{
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.accueil.facade",
																			"getRepartOperateur",
																			chargerDonneesResultHandler,null);
			
			
			RemoteObjectUtil.callService(op);
			
		}	
		
		// Chargement des données par remoting
		protected function chargerDonneesBySegment():void{
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.accueil.facade",
																			"getRepartOperateurBySegment",
																			chargerDonneesResultHandler,null);
			
			
			RemoteObjectUtil.callService(op,_segment);
			
		}	
		
		protected override function chargerDonneesResultHandler(re :ResultEvent):void{			 		 		 
		  		dataProviders =  re.result as ArrayCollection;			  				
		  
		} 	
		 				
	}
		
}