package composants.tb.tableaux
{
	import composants.tb.tableaux.renderer.*;
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;	
		
	public class TableauAbo extends TableauAbo_IHM implements ITableauModel
	{
		[Bindable]
		protected var _dataProviders : ArrayCollection;	
		
		protected var opTitre : AbstractOperation;
		
		protected var op : AbstractOperation;
		
		private var perimetre : String;
		
		private var modeSelection : String;
		
		private var identifiant : String;
		
		
		[Bindable]
		protected var _total : Number = 0;		
								
		//--------------------------------------------------------------------------------------------------------------------------------------------------		
		/**
		* Constructeur 
		**/
		public function TableauAbo(){
				super();
				addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		
		/**
		 * Retourne le montant total
		 * */
		 public function get total():Number{
		 	return _total;
		 }
		
		
		/**
		* Permet de passer les valeurs aux tableaux du composant.
		* <p>Pour chaque tableau de la collection, un DataGrid sera affiché à la suite du précédant</p>
		* @param d Une collection de tableau. 
		**/						
		public function set dataProviders(d : ArrayCollection):void{
			
			_dataProviders = _formatterDataProvider(d);
			drawTabs();
			
		}
		
		/**
		*  Retourne le dataproviders
		**/	
		[Bindable]			
		public function get dataProviders():ArrayCollection{
			return _dataProviders;
		}	
		
				
		/**
		* Permet d'affecter une fonction pour traiter les Clicks sur Les DataGrids du composant
		* @param changeFunction La fonction qui traite les clicks.<p> Elle prend un paramètre de type Object qui représente une ligne du DataGrid</p>
		**/		
		public function set changeHandlerFunction ( changeFunction : Function ):void{
			//_changeHandlerFunction = changeFunction;
		}		
		
				
		/**
		*  Retourne une reference vers la fonction qui traite le data provider
		**/			
		public function get changeHandlerFunction():Function{
			return null;//_changeHandlerFunction;
		}	
		
		/**
		 * Met à jour les donées du tableau
		 **/
		public function update():void{			
			chargerDonnees();			 
			 
		}
		
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		public function clean():void{			
			myGrid.dataProvider = null;
		}
		
		/**
		 * Met à jour la periode
		 * */		 
		public function updatePeriode(moisDeb : String = null, moisFin : String = null):void{
				
		}		
		
		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...) 			
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		public function updatePerimetre(perimetre : String = null, modeSelection : String = null):void{
			this.perimetre = perimetre;
			this.modeSelection = modeSelection;		
		}			
		/*---------- PRIVATE -----------------------*/
		
		//initialisation du composant
		protected function init(fe :  FlexEvent):void{
			
		}
		
		private function drawTabs():void{
			rep.dataProvider = dataProviders;
			 
			 
			if (rep.dataProvider != null)	
				for (var i:int = 0; i < rep.dataProvider.length; i++){
						//myGrid[i].addEventListener(Event.CHANGE,_changeHandlerFunction);	
						myGrid[i].addEventListener(Event.CHANGE,foo);														
				}  				
			myFooterGrid.columns[1].headerText = cf.format(_total);		
		}
		
		protected function foo(ev :Event):void{
			
			var eventObj : TableauChangeEvent = new TableauChangeEvent("tableauChange");
			
			eventObj.LIBELLE = ev.currentTarget.selectedItem.LIBELLE;
			eventObj.MONTANT_TOTAL = ev.currentTarget.selectedItem.MONTANT_TOTAL;
			eventObj.ID = ev.currentTarget.selectedItem.ID;
			eventObj.QUANTITE = ev.currentTarget.selectedItem.QUANTITE;
			eventObj.TYPE_THEME	= ev.currentTarget.selectedItem.TYPE_THEME;
			eventObj.SUR_THEME = ev.currentTarget.selectedItem.SUR_THEME;
			eventObj.SEGMENT = ev.currentTarget.selectedItem.SEGMENT;
			eventObj.TYPE = ev.currentTarget.selectedItem.TYPE;
			eventObj.SOURCE = ev.currentTarget;
			eventObj.IDENTIFIANT = identifiant;
			dispatchEvent(eventObj);
			
		}
		 
		// permet de formatter la collection de DataProvider 
		protected function _formatterDataProvider(d: ArrayCollection):ArrayCollection{		
									
			var tmpCollection : ArrayCollection = new ArrayCollection();
			var o : Object;
			
			var mobileArray : Array = new Array();
			var fixeArray : Array = new Array();
			var dataArray : Array = new Array();
			_total = 0;
			
			for	(o in d){		
				switch(d[o].segment_theme.toUpperCase()){
					case "MOBILE" :  mobileArray.push(formateObject(d[o]));break;
					case "FIXE" :  fixeArray.push(formateObject(d[o]));break;
					case "DATA" :  dataArray.push(formateObject(d[o]));break;										
				}  
			}
			
			if (fixeArray.length > 0){
				var obj0 : Object = new Object();
				obj0["LIBELLE"] = fixeArray[0].SEGMENT;
				obj0["SEGMENT"] = fixeArray[0].SEGMENT;
				obj0["TYPE"] = "SEGMENT";	
				obj0["QUANTITE"]=" ";
				obj0["MONTANT_TOTAL"]= calculTotal(fixeArray,"MONTANT_TOTAL");
				_total = _total +  obj0["MONTANT_TOTAL"];					
				fixeArray.unshift(obj0);
				tmpCollection.addItem(fixeArray);
			}			
			
			if (mobileArray.length > 0){
				var obj : Object = new Object();
				obj["LIBELLE"] = mobileArray[0].SEGMENT;
				obj["SEGMENT"] = mobileArray[0].SEGMENT;
				obj["TYPE"] = "SEGMENT";	
				obj["QUANTITE"]=" ";
				obj["MONTANT_TOTAL"]= calculTotal(mobileArray,"MONTANT_TOTAL");
				_total = _total +  obj["MONTANT_TOTAL"];	
			
				mobileArray.unshift(obj);
				tmpCollection.addItem(mobileArray);
			}
									
			if (dataArray.length > 0){
				var obj1 : Object = new Object();
				obj1["LIBELLE"] = dataArray[0].SEGMENT;
				obj1["SEGMENT"] = dataArray[0].SEGMENT;
				obj1["TYPE"] = "SEGMENT";	
				obj1["QUANTITE"]=" ";
				obj1["MONTANT_TOTAL"]= calculTotal(dataArray,"MONTANT_TOTAL");
				_total = _total + obj1["MONTANT_TOTAL"];	
				
				dataArray.unshift(obj1);
				tmpCollection.addItem(dataArray);				
			}  
								
			
			return tmpCollection;
		}	
		
		protected function formateObject(obj : Object):Object{
			var o : Object = new Object();
			o["LIBELLE"] = obj.theme_libelle;
			o["SEGMENT"] = obj.segment_theme;
//			o["QUANTITE"] = nf.format(obj.qte);
			o["QUANTITE"] = ConsoviewFormatter.formatNumber(Number(obj.qte),2);
			o["TYPE_THEME"] = obj.type_theme;
			o["SUR_THEME"] = obj.theme_libelle;
			o["MONTANT_TOTAL"]= obj.montant_final;
			o["TYPE"] = "SURTHEME";	
			o["ID"] = obj.idtheme_produit;	
				
			return o;
		}	
		
		protected function calculTotal(arr : Array, colname : String):Number{
			var total : Number = 0;
			
			for(var i:int=0; i< arr.length;i++){
				
				total = total + parseFloat(arr[i][colname]);
			}
			return total;
		}
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		protected function chargerDonnees():void{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.accueil.facade",
																			"getSurThemeAbos",
																			chargerDonnneesResultHandler,null);
			
			
			RemoteObjectUtil.callService(op);
			
		}
		 protected function chargerDonnneesFaultHandler(fe :FaultEvent):void{
		 	trace(fe.fault.faultString,fe.fault.name);			 		
		 }     
		 
		 protected function chargerDonnneesResultHandler(re :ResultEvent):void{
		 	try{
		 		
		  		dataProviders =  re.result as ArrayCollection;	
		  		getTitre();		
		  	}catch(er : Error){		  	
		  		trace(er.message,"fr.consotel.consoview.tb.accueil.facade  getSurThemeAbos ");
		  	}
		  
		 } 
		 
		  protected function getTitre():void{
		 	opTitre = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.accueil.facade",
																			"getLibelleAbo",
																			getTitreResultHandler,null);
			
			
			RemoteObjectUtil.callService(opTitre,perimetre,modeSelection);
		 }			
		 
		 protected function getTitreResultHandler(re : ResultEvent):void{
		 	title = String(re.result);
		 }	 				
	}
}