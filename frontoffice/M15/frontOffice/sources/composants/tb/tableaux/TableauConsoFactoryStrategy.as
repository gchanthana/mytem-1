package composants.tb.tableaux
{
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	
	public class TableauConsoFactoryStrategy implements ITableauFactoryStrategy
	{
		public function createTableau(ev : TableauChangeEvent):ITableauModel
		{
				
			var tp : String = ev.TYPE;
			var seg : String = ev.SEGMENT;
			var idthemeproduit : String = ev.ID;
			var surtheme : String = ev.SUR_THEME;
			var idproduit : String = ev.IDPRODUIT;
						
			try{
				switch (tp.toUpperCase()){
					case "ACCUEIL" : return new TableauConso();break;
					case "SEGMENT" : return new TableauConsoSegment(ev);break;
					case "SURTHEME" : return new TableauSurThemeConso(ev);break;
					case "THEME" : return new TableauThemeConso(ev);break;
					case "PRODUIT" : return new TableauProduitConso(ev);break;
					default : throw new TableauError();break; 
				}
			}catch(e:Error){				
				throw new TableauError();
			}
			 return null;	
		}
		
	}
}