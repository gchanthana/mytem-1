package composants.cyclevie
{
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;
	
	public class TabElFactOperationRapprochement extends TabElFactOperationRapprochementIHM
	{
		private var opElmtFacruration : AbstractOperation;
		
		private var total : Number;		
		
		private var idOPeration : int;
		private var dateRef : String;
		private var type : int;//0 = resi 1 = crea  
		
		private var _titre : String = "";
		
		[Bindable]
		private var tabElementsFacturation : ArrayCollection; //element de facturation de ces produits;
	
		[Bindable]
		private var tabProduitsARapprocher : ArrayCollection; //element de facturation de ces produits
		
		[Bindable]
		private var tabProduitsSelectionnes : Array; // 
		
		public function TabElFactOperationRapprochement()
		{
			super();									
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		/**
		 * charge les elements dans le grid 
		 * @param idop  L'identifiant d'une operation
		 * @param dRef  La date de ref pour les calculs
		 * @param le type de calcul (rapprochement de creation = 2 ,rapprochement de resiliation = 1)
		 * */
		public function initialiserGrid(idOp : int = 0,dRef : String = null ,tpe : int = 1):void{
			/* idOPeration = idOp;
			dateRef = dRef;
			type = tpe;		 */
			
			//test
			idOPeration = idOp;
			dateRef = dRef;
			type = tpe;				
			chargerElementFacturation();
		}
		
		
		/**
		 * Suprime la ligne visuel selectionnée du grid des produits de l'op
		 * */
		 public function supprimerElementsSelectionnes():void{
		 	myGridFacturation.selectedIndices.forEach(supprimerLigne);	
		 	
		 }
		 
		 /**
		 * Rafraichir le grid des elements de facturation
		 * */
		 public function rafraichir():void{
		 	chargerElementFacturation();
		 }
		 
		 /**
		 * Retourne les donnée du grid
		 * @return les données sous forme de tableau
		 * */
		 public function get Donnees():Array{
		 	
		 	return tabProduitsARapprocher.source;
		 }
		
		/**
		 * Met le titre
		 * */
		 [Bindable]
		 public function set titre(t : String):void{
		 	_titre = t;
		 	if (leTitre != null) leTitre.text = _titre;
		 }
		 
		 public function get titre():String{
		 	return _titre;
		 }
		  
		//init de l'IHM
		private function initIHM(fe : FlexEvent):void{	
			txtFiltre.addEventListener(Event.CHANGE,filtrerGird);		
			myGridFacturation.addEventListener(Event.CHANGE,selectionnerProduits);
			DataGridColumn(myGridFacturation.columns[0]).labelFunction = formateDates;
			DataGridColumn(myGridFacturation.columns[1]).labelFunction = formateDates;
			DataGridColumn(myGridFacturation.columns[2]).labelFunction = formateDates;
			DataGridColumn(myGridFacturation.columns[4]).labelFunction = formateLigne;
			DataGridColumn(myGridFacturation.columns[7]).labelFunction = formateEuros;
			colMontant.sortCompareFunction = numericSortFunction;
		}
		
		
		
		private function supprimerLigne(element:*, index:Number, arr:Array):void{	
			var obj : Object;			
			
			obj = tabProduitsARapprocher.getItemAt(element);
			tabProduitsARapprocher.removeItemAt(element);
			tabProduitsARapprocher.refresh();			
			
		}	
		
		private function formateDates(item : Object, column : DataGridColumn):String{
			var ladate:Date = new Date(item[column.dataField]);
			return DateFunction.formatDateAsShortString(ladate);			
		}
		
		private function formateEuros(item : Object, column : DataGridColumn):String{			
			return ConsoviewFormatter.formatEuroCurrency(item.MONTANT,2);	
		}
		
		private function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);			
		}
		
		private function numericSortFunction(itemA:Object, itemB:Object):int{
			return ObjectUtil.numericCompare(itemA.MONTANT,itemB.MONTANT);
		}
		
				
		//fait la somme de la colonne du tab passer en param
		private function calculTotal(d : ArrayCollection,colname : String):Number{
			var ligne : Object;
			var letotal : Number = 0;
			
			for (ligne in d){
				letotal = letotal + Number(d[ligne][colname]); 
			}	
									
			return letotal;	
		}		
		
		//selection des produits
		private function selectionnerProduits(ev : Event):void{
			//tabProduitsSelectionnes = formateData(ev.currentTarget.selectedItems,"IDINVENTAIRE_PRODUIT");
			tabProduitsSelectionnes = ev.currentTarget.selectedItems;
			
			//pour les tests
				var eventObjbis : SelectionProduitEvent = new SelectionProduitEvent("ProduitsFactSelectionnes");
				eventObjbis.tabProduits = tabProduitsSelectionnes;	
				dispatchEvent(eventObjbis);					
			//fin tests					
		}
		
		//formate les donnees du selectedItems du grid
		private function formateData( data : Array , colname : String):Array{
			var s : String;
			var a : Array = new Array();
			for (var i : uint = 0 ; i < data.length ; i++ ){
				a.push(data[i][colname]);
			}
			return a;
		}	
		
		//------------ filtres ------------------------------------
		
		private function filtrerGird(ev : Event):void{
			if (myGridFacturation.dataProvider != null){
				(myGridFacturation.dataProvider as ArrayCollection).filterFunction = filtrer;
				(myGridFacturation.dataProvider as ArrayCollection).refresh();
				total = calculTotal((myGridFacturation.dataProvider as ArrayCollection),"MONTANT");						
				afficgerTotal();
			}
		}
		
		private function filtrer(item : Object):Boolean{
			if ((String(item.DATE_EMISSION).toLowerCase().search(txtFiltre.text) != -1)
				||
				(String(item.JOURS_REGUL).toLowerCase().search(txtFiltre.text) != -1)
				||
				(String(item.SOUS_TETE).toLowerCase().search(txtFiltre.text) != -1)		
				||
				(String(item.OPNOM).toLowerCase().search(txtFiltre.text) != -1)		
				||
				(String(item.LIBELLE_PRODUIT).toLowerCase().search(txtFiltre.text) != -1)
				||
				(String(item.MONTANT).toLowerCase().search(txtFiltre.text) != -1))
			{
				return true;
			}else{
				return false;
			}
		}
		
		//------------ REMOTINGS -----------------------------------
		private function chargerElementFacturation():void{
			
			opElmtFacruration = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.ElementDInventaire",
																				"getListeElementFacturationReel",
																				chargerElementFacturationResultHandler,null
																				);	
																							
			RemoteObjectUtil.callService(opElmtFacruration,idOPeration,dateRef,type-1);		
		}
		
		
		private function chargerElementFacturationResultHandler(re : ResultEvent):void{
			tabProduitsARapprocher = re.result as ArrayCollection;		
			myGridFacturation.dataProvider = tabProduitsARapprocher;	
			total = calculTotal(tabProduitsARapprocher,"MONTANT");		
			afficgerTotal();
		}
		
		private function afficgerTotal():void{
			DataGridColumn(myFooterGrid.columns[1]).headerText = ConsoviewFormatter.formatEuroCurrency(total,2);
		}
		
		private function defaulFaultHandler(fe : FaultEvent):void{
			Alert.show("Erreur Remoting");
		}
	}
}