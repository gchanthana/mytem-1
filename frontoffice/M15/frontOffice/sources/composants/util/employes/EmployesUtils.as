package composants.util.employes
{
	///////////////////////////////////////////////////////////
	//  EmployesUtils.as
	//  Macromedia ActionScript Implementation of the Class EmployesUtils
	//  Generated by Enterprise Architect
	//  Created on:      15-oct.-2008 15:08:56
	//  Original author: samuel.divioka
	///////////////////////////////////////////////////////////

	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	/**
	 * [Bindable]
	 * @author samuel.divioka
	 * @version 1.0
	 * @created 15-oct.-2008 15:08:56
	 */
	[Bindable]
	public class EmployesUtils
	{
	    private var _listeEmployes: ArrayCollection;
	    private var _selectedEmploye: Employe;
	
		public function EmployesUtils(){
	
		}
	
	    /**
	     * fournit la liste des employés des pools de gestion d'un gestionnaire suivant
	     * une clef de rechercher, si id du gestionnaire = 0, on prend les employes de la
	     * racine.
	     * clef = | NOM | PRENOM | CODE_INTERNE | FONCTION_EMPLOYE | REFERENCE_EMPLOYE
	     * 
	     * @param gestionnaire
	     * @param clef
	     */
	    public function fournirListeEmployes(gestionnaire:Object, clef:String): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				 "fr.consotel.consoview.inventaire.employes.EmployesUtils",
																				 "fournirListeEmployes",
																				 fournirListeEmployesResultHandler);
		 	RemoteObjectUtil.callService(op)	
	    	
	    	
	    }
	
	    /**
	     * liste des employés
	     * 
	     * @param values
	     */
	    public function set listeEmployes(values:ArrayCollection): void
	    {
	    	_listeEmployes = values
	    }
	
	    /**
	     * l'employé sélectionné
	     * 
	     * @param value
	     */
	    public function set selectedEmploye(value:Employe): void
	    {
	    	_selectedEmploye = value
	    }
	
	    /**
	     * l'employé sélectionné
	     */
	    public function get selectedEmploye(): Employe
	    {
	    	return _selectedEmploye
	    }
	
	    /**
	     * liste des employés
	     */
	    public function get listeEmployes(): ArrayCollection
	    {
	    	return _listeEmployes
	    }
	
	    /**
	     * 
	     * @param event
	     */
	    protected function fournirListeEmployesResultHandler(event:ResultEvent): void
	    {
	    	listeEmployes = formatterData(event.result as ArrayCollection);
	    	
	    }
	    
	    private   function formatterData(values : ICollectionView):ArrayCollection{
	    	var retour : ArrayCollection = new ArrayCollection();
	    	if (values != null)
	    	{
	    		var cursor : IViewCursor = values.createCursor();
	    		while(!cursor.afterLast){
	    			var employeObj:Employe = new Employe();
	    			employeObj.NOMPRENOM = cursor.current.NOM;
	    			employeObj.IDEMPLOYE = cursor.current.IDEMPLOYE;
	    			retour.addItem(employeObj);	    			
	    			cursor.moveNext();
	    		}	
	    	}
	    	return retour 
	    }
	
	}//end EmployesUtils	
}
