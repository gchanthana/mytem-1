package composants.util.article
{
	import composants.util.ConsoviewFormatter;
	
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	
	public class ArticleEquipementNue extends Article
	{
		 
		
		public function ArticleEquipementNue(article:XML=null)
		{
			super(article);
		}
		
		
		
		
		/**
		 * Ajoute un equipement pour l'article
		 * si il y a déjà un mobile dans l'article, alors on le remplace par celui sélectionné
		 * @param value les attributs suivant sont obligatoire 
		 * 			   TYPE_EQUIPEMENT   		MOBILE|CARTE SIM|...
		 * 			   IDEQUIPEMENT_CLIENT		NUMBER
		 * 			   IDEQUIPEMENT_PARENT		NUMBER
		 * 			   LIBELLE_EQUIPEMENT		STRING
		 * 			   MODELE_EQUIPEMENT		STRING
		 * 			   PRIX						NUMBER
		 * 			   COMMENTAIRES				NUMBER	
		 * 			   [BONUS]					NUMBER
		 * @return code (1 si ok sinon -1) 
		 * */
		override public function addEquipement(value : Object):Number
		{	
			
			//Si il y a déjà un mobile dans l'article on le supprime avant d'ajouter le nouveau mobile
			//	
		    if(value.IDTYPE_EQUIPEMENT == MOBILE)
			{
				//on met à jour idequipement parents chez les autres équipements
				updateIdEquipementsParent(value.IDEQUIPEMENT_FOURNIS);
					
				var equipementsMobile:XML = article.equipements.equipement.(type.toString().toLowerCase() == "mobile")[0];
				trace(ObjectUtil.toString(equipementsMobile));
				//il y a un équipement de type mobile dans l'article
				if (equipementsMobile)
				{
					
					//on supprime le mobile
					removeEquipement(equipementsMobile);
				}
				
			}
		 
			
		    						
			var equipement : XML = <equipement></equipement>;
			var prixToString:String = ConsoviewFormatter.formatNumber(value.PRIX_CATALOGUE,2);
			
			
			equipement.appendChild(<idarticle_equipement></idarticle_equipement>);
			equipement.appendChild(<idequipementfournisparent></idequipementfournisparent>);
			equipement.appendChild(<idequipementfournis>{value.IDEQUIPEMENT_FOURNISSEUR}</idequipementfournis>);			
			equipement.appendChild(<type_equipement>{value.TYPE_EQUIPEMENT}</type_equipement>);
			equipement.appendChild(<idtype_equipement>{value.IDTYPE_EQUIPEMENT}</idtype_equipement>);
			equipement.appendChild(<code_ihm>{value.TE_CODE_IHM}</code_ihm>);
			equipement.appendChild(<codepin></codepin>);
			equipement.appendChild(<codepuk></codepuk>);
			if(value.IDEQUIPEMENT_CLIENT)
			{
				equipement.appendChild(<idequipementclient>{value.IDEQUIPEMENT_CLIENT}</idequipementclient>);	
			}
			else
			{
				equipement.appendChild(<idequipementclient/>);
			}
			equipement.appendChild(<numeroserie></numeroserie>);			
			equipement.appendChild(<libelle>{value.LIBELLE_EQ}</libelle>);
			equipement.appendChild(<modele></modele>);
			equipement.appendChild(<prix>{(value.PRIX_CATALOGUE!=null)?prixToString:0}</prix>);
			equipement.appendChild(<prixAffiche>{(value.PRIX_CATALOGUE!=null)?value.PRIX_CATALOGUE:0}</prixAffiche>);
			equipement.appendChild(<bonus>{(value.BONUS>0)?value.BONUS:0}</bonus>);	
			equipement.appendChild(<commentaire>{value.COMMENTAIRES}</commentaire>);
			equipement.appendChild(<contrats></contrats>);
			
			var contrat:XML = <contrat></contrat>;						
			contrat.appendChild(<idcontrat></idcontrat>);
			contrat.appendChild(<referencecontrat></referencecontrat>);
			contrat.appendChild(<dureecontrat></dureecontrat>);
			
			 
			
			XML(equipement.contrats).appendChild(contrat);
			equipements.appendChild(equipement);
			
			calculTotal();
				trace(article.toXMLString());
			return 1;
		}
		
		//SI EQ = SIM alors Impossible de supprimer l'element
		override public function removeEquipement(value:Object):void
		{
			var len:Number = XMLList(article.equipements.equipement).length();
			var obj:Object = XMLList(article.equipements.equipement);
			
			if(len > 0)
			{	
				if(len > 1 && String(value.type_equipement).toUpperCase() == "MOBILE")
				{
					//on met à jour idequipement parents chez les autres équipements
					updateIdEquipementsParent(0);
				}
				super.removeEquipement(value);
			}
			else
			{
				Alert.show("L'article doit contenir au moins un équipement")
			}
		}
		
		
		override public function set dureeEngagementRessources(value:Number):void
		{
			throw Error("Pas de ressource");
		}
		override public function get dureeEngagementRessources():Number
		{
			return -1;
		}
		
		override public function addRessource(value:Object):void
		{
			throw Error("Impossible d'ajouter une ressource");
		}
		
		override public function removeRessource(value:Object):void
		{
			throw Error("Impossible d'ajouter une ressource");
		}
		
	}
}