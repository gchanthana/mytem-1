package composants.util
{
	import mx.charts.chartClasses.StackedSeries;
	import mx.formatters.PhoneFormatter;
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.NumberFormatter;
	import mx.formatters.NumberBaseRoundType;
	import mx.controls.Alert;
	import mx.formatters.DateFormatter;
		
	public class ConsoviewFormatter
	{
		//formate le numero de télé98phone
		public static function formatPhoneNumber( num : String):String{
			if(num != null){
				var pf : PhoneFormatter = new PhoneFormatter();
				pf.formatString = "## ## ## ## ##";
				pf.areaCode = -1;
				if (pf.format(num)=="") {
					var formatedString:String = num;/* num.substr(0,2) + " " 
																   + num.substr(2,2) 
																   + " " 
																   + num.substr(4,2) 
																   + " " 
																   + num.substr(6,2) 
																   + " " 
																   + num.substr(8,2);
					if((num.length > 10) && (num.substring(10,1).toUpperCase() != "X")) {
						formatedString = formatedString + " " + num.substring(10,num.length - 1);
					} */
					return formatedString;
				} else {
					return pf.format(num);	
				}				
			}else return " ";	
			
		}
		
		public static function formatSimplePhoneNumber( num : String):String{
			var pf : PhoneFormatter = new PhoneFormatter();
			pf.formatString = "## ## ## ## ##";
			pf.areaCode = -1;
			if (pf.format(num)=="") {
				return num
			} else {
				return pf.format(num);	
			} 		
		} 
				
		//formate la monaie avec la précision passée en parametre	
		public static function formatEuroCurrency( num : Number , precision : uint):String{
			
			var cf : CurrencyFormatter = new CurrencyFormatter();			
			cf.precision = precision;   	
			cf.rounding = NumberBaseRoundType.NONE;		
			cf.decimalSeparatorTo = ",";
			cf.thousandsSeparatorTo = " ";
			cf.useThousandsSeparator = true;
			cf.useNegativeSign = true;
			cf.currencySymbol = " €";
			cf.alignSymbol = "right";
			 
			if ((num > -1) && (num < 0)){
				return "-0"+cf.format(Math.abs(num));
			}
			if ((num > 0) && (num < 1)){
				return "0"+cf.format(num);
			}
			return cf.format(num);	
		}
		
		//formate le pourcentage avec zero ou un chiffre apres la virgule
		public static function formatePourcent(num : Number):String{
			
			var nf : NumberFormatter = new NumberFormatter();			
			
			nf.rounding = NumberBaseRoundType.NONE;		
			nf.decimalSeparatorTo = ",";
			nf.thousandsSeparatorTo = " ";    
			nf.useThousandsSeparator = true;
			nf.useNegativeSign = true;
			
			if ( Math.round(num) == num ){
				nf.precision = 0;
			}else{
				nf.precision = 1;
			}
			
			return nf.format(num);			
		}
		
	
		//formtae un chiffre avec la precision passée en parametre
		public static function formatNumber(num : Number , precision : int):String{
			
			var nf : NumberFormatter = new NumberFormatter();
			var v_abs : Number = Math.round(num);			
			if ( v_abs == num ){
				nf.precision = 0;
			}else{
				nf.precision = precision;
			}
			nf.rounding = NumberBaseRoundType.NONE;		
			nf.decimalSeparatorTo = ",";
			nf.thousandsSeparatorTo = " ";    
			nf.useThousandsSeparator = true;
			nf.useNegativeSign = true;
			
			if ((num > -1) && (num < 0)){
				return "-0"+nf.format(Math.abs(num));
			}
			if ((num > 0) && (num < 1)){
				return "0"+nf.format(num);
			}
			return nf.format(num);				 
		}
		
		
	}
}