package composants.util {

    import mx.controls.treeClasses.*;
    import mx.collections.*;

    public class MyTreeItemRenderer extends TreeItemRenderer {

        // Define the constructor.
        public function MyTreeItemRenderer() {
            super();
        }
        
        // Override the set method for the data property
        // to set the font color and style of each node.
        override public function set data(value:Object):void {
            super.data = value;
            if (value.@actif=="1") {
                setStyle("color", 0x5B832F);
            }
            else if (value.@actif=="2"){
                setStyle("color", 0xFF6500);
            } else {
            	setStyle("color", 0xB8111E);
            }
        }
    }
}

