package modulelogin.event
{
	import flash.events.Event;
	
	public class RetrievePasswordEvent extends Event
	{
		
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES STATIC - EVENT NAME
		//--------------------------------------------------------------------------------------------//
		
		public static const USER_NOT_FOUND 				: String = "userNotFound";
		public static const PASSWORD_EMAILED 			: String = "passwordEmailed";		
		
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES PASSEES EN ARGUMENTS
		//--------------------------------------------------------------------------------------------//		
		
		public var obj								:Object = null;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//		
		
		public function RetrievePasswordEvent(	type		:String,
												objet		:Object  = null,
												bubbles		:Boolean = false,
												cancelable	:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.obj	= objet;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLIC OVERRIDE - COPY OF EVENT
		//--------------------------------------------------------------------------------------------//
		
		override public function clone():Event
		{
			return new RetrievePasswordEvent(type, obj, bubbles, cancelable);
		}
		
	}
}