package modulelogin.vo
{
	[RemoteClass(alias="fr.consotel.consoview.access.CodeLangue")]
	[Bindable]
	public class CodeLangue
	{
		private const URL : String = "https://cache.consotel.fr/flag/";
		private var _CODE:String = "";
		private var _DEVISE : String = "";
		private var _LANGUE: String= "";
		private var _ID_LANGUE :Number=-1;
		private var _FLAG_URL:String="";
		
		public function CodeLangue(code:String,devise:String,langue:String,idlangue:Number,flag_url:String)
		{
			_CODE = code;
			_DEVISE = devise;
			_LANGUE = langue;
			_ID_LANGUE = idlangue;
			_FLAG_URL = URL + flag_url + ".png";
		}
		public function set CODE(value:String):void
		{
			_CODE = value;
		}
		public function get CODE():String
		{
			return _CODE;
		}
		public function set DEVISE(value:String):void
		{
			_DEVISE = value;
		}

		public function get DEVISE():String
		{
			return _DEVISE;
		}
		public function set LANGUE(value:String):void
		{
			_LANGUE = value;
		}
		public function get LANGUE():String
		{
			return _LANGUE;
		}
		public function set ID_LANGUE(value:Number):void
		{
			_ID_LANGUE = value;
		}
		public function get ID_LANGUE():Number
		{
			return _ID_LANGUE;
		}		
		public function set FLAG_URL(value:String):void
		{
			_FLAG_URL = value;
		}
		
		[Transient]
		public function get FLAG_URL():String
		{
			return _FLAG_URL;
		}

	}
}