package univers.parametres.poolgestionnaire.event
{
	import flash.events.Event;
	
	public class GestionProfilEvent extends Event
	{
		public static const PARAMETRE_PROFIL_COMPLETE:String= "parametreProfilComplete";
		public static const INFO_PROFIL_COMPLETE:String= "infoProfilComplete";
		public static const LISTE_PROFIL_COMPLETE:String= "listeProfilCOmplete";
		public static const LISTE_PROFIL_OFUSER_COMPLETE:String= "listeProfilOfUserComplete";
		public static const LISTE_CONTRAINTE_PROFIL_COMPLETE :String= "listeCOntrainteComplete";
		public static const REMOVE_PROFIL_COMPLETE :String= "removeProfilEvent";
		public static const LISTE_ACTION_COMPLETE :String= "listeActionComplete";
		
		public var idProfil:int;
		
		public function GestionProfilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,idProfil:int = -1)
		{
			super(type, bubbles, cancelable);
			this.idProfil = idProfil;
		}

	}
}