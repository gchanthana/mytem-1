package univers.parametres.poolgestionnaire.vo
{
	[Bindable]
	public class TypeCommande
	{
		public var idTypeCmd : int;
		public var libelleTypeCmd : String;
		public var codeTypeCmd: String;
		public var commentaire: String;
		public var nbPool : int;
		public var nbGestionnaire : int;
		public var isInPool:Boolean = false;
		public var boolSelected:Boolean = false;

		public function TypeCommande()
		{
			
		}
	}
}
		