package univers.parametres.login
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.XMLListCollection;
	import mx.controls.Alert;
	import mx.controls.Tree;
	import mx.controls.treeClasses.ITreeDataDescriptor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class Arbre implements ITreeDataDescriptor{              
		
		[Embed(source="../../../assets/images/folder_forbidden.png")]    
		private var imgIconAcces : Class;
       	private var _refOwner : Tree
       	private var arrayAccesInterdit : ArrayCollection;
       	private var arraySesAcces : ArrayCollection;

       	public function Arbre(refOwner : Tree,accesInterdit : ArrayCollection,sesAcces : ArrayCollection){
	       _refOwner = refOwner;  
	       this.arrayAccesInterdit   = accesInterdit;
	       this.arraySesAcces = sesAcces;
     	}
      	public function getData(node:Object, model:Object=null):Object
      	{
			_refOwner.expandItem(node,true);
           	return null;
  		}
	    public function hasChildren(node:Object, model:Object=null):Boolean
       {
			_refOwner.expandItem(node,true);
           	var hasChild:int; 
     	    if (typeof(node) == "xml") hasChild = parseInt(node.@NTY,10);
	        else hasChild = int(node.NTY);
    	    return (hasChild > 0);
	   }
	   
	   // Fonction pour ajouter un enfant sur l'arbre, apres une requete
	   public function addChildAt(parent:Object, newChild:Object, index:int, model:Object=null):Boolean
       {
			return false;
       }
       public function isBranch(node:Object, model:Object=null):Boolean
       {
        	var hasChild:int;

			if (typeof(node) == "xml"){
           		hasChild = Number(node.@NTY);
           	}
           	else
           	{
           		Alert.show("2");
           	 	hasChild = int(node.NTY);
           	}
	        return (hasChild > 0);
		}
		public function removeChildAt(parent:Object, child:Object, index:int, model:Object=null):Boolean
		{              
       		return false;
       	}
       	public function getChildren(node:Object, model:Object=null):ICollectionView
	    {              
       		var childrenList:XMLList = (node as XML).children();
	      	if (childrenList.length() == 0)
	      	{                                                   
				getNodeChild(node);    
		    }
			return new XMLListCollection(childrenList);
       	}
		private var _node : Object = null;            
      	private function getNodeChild(node : Object):void
      	{                     
        	if ((parseInt(node.@NID,10) > 0) && (node as XML).children().length() == 0){
				_node = node;
                var getChildOp:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
                                         "fr.consotel.consoview.parametres.login.GestionClient","getArbre",addNodeChild,null);

                RemoteObjectUtil.callService(getChildOp,parseInt(node.@NID,10));
            }
	     }
		private function addNodeChild(event:ResultEvent):void {
       	
       		var gererLesFreres : Boolean = false; 
        	if (_node != null){
           		for each(var uneRacine:XML in (event.result as XML).children()){
           			if(_node.hasOwnProperty("@isInterdit")){
           				if(_node.@isInterdit==true){
           						
           				}          		
           			}
           			if(_node.hasOwnProperty("@isAcces")){
           				if(uneRacine.@isAcces==true){
           					uneRacine.@isInterdit=true;
           					uneRacine.@isVerouille=true;
           					Alert.show("ARBREE");
           					_refOwner.setItemIcon(uneRacine,imgIconAcces,imgIconAcces);   
           					trace("on verouille");       				
           				}
           				
           			}
           		
           			if(_node.hasOwnProperty("@isVerouille")){
           				if(_node.@isVerouille==true){
           					uneRacine.@isVerouille=true;
           					_refOwner.setItemIcon(uneRacine,imgIconAcces,imgIconAcces);          				
           				}
           			}
           			for (var a : int  = 0 ; a <arrayAccesInterdit.length;a++){
						if 	(uneRacine.@NID ==(arrayAccesInterdit.getItemAt(a) as UtilAcces).getId()){
							uneRacine.@isInterdit=true;
						}
						
					}
					for (var b : int  = 0 ; b <arraySesAcces.length;b++){
						if 	(uneRacine.@NID ==(arraySesAcces.getItemAt(b).ID)){
							uneRacine.@isInterdit =false;
							uneRacine.@isAcces =true;
							uneRacine.@isVerouille=true;
							gererLesFreres = true;
						}
												
					}		
				}
       			if(gererLesFreres==true){
       				for each(var uneRacine1:XML in (event.result as XML).children()){
       					if (uneRacine1.hasOwnProperty("@isAcces")==false){
       						uneRacine1.@isInterdit = false;
       					}
       				}
       			}
       	
       		}
           if (_node != null){
           		if ((_node as XML).children().length() == 0){
				
                  _refOwner.expandItem(_node,false);

                  (_node as XML).appendChild((event.result as XML).children());                                                                           

                  _refOwner.expandItem(_node,true);

               }
                                       

           }

       }              

}

}