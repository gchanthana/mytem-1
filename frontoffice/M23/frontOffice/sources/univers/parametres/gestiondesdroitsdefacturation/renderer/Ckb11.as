package univers.parametres.gestiondesdroitsdefacturation.renderer
{
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.CheckBox;

	public class Ckb11 extends VBox
	{
		public var ckb:CheckBox=new CheckBox();
		
		public function Ckb11()
		{
			super();
			setStyle("horizontalAlign","center");
			setStyle("verticalAlign","middle");
			this.addChild(ckb);
			this.addEventListener(MouseEvent.CLICK,ckbChangeDataHandler);
		}
		
		public function ckbChangeDataHandler(e:MouseEvent):void{
			if(ckb.selected){
				data.obj11 = 1;
			}else{
				data.obj11 = 0;
			}
		}
					
		override public function set data(value:Object):void{
			if(value != null && value != ""){
				super.data=value;
				if(data.obj11 == 1){
					ckb.selected=true;
				}else{
					ckb.selected=false;
				}
				if(data.EDITABLE == false){
					ckb.enabled = false
				}else{
					ckb.enabled = true;
				}
			}
		}
	}
}