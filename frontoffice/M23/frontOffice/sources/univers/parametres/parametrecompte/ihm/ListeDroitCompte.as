package univers.parametres.parametrecompte.ihm
{
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.RadioButtonGroup;
	import mx.events.FlexEvent;
	
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.service.LoginService;
	
	
	[Bindable]
	public class ListeDroitCompte extends VBox
	{
		public var loginService : LoginService = new LoginService();
				//UI COMPONANT
		public var groupe_radioButtonFixData : RadioButtonGroup;
		public var groupe_radioButtonsCycleDeVie: RadioButtonGroup;
		public var groupe_radioButtonsGestionOrgas: RadioButtonGroup;
		public var groupe_radioButtonsLogin: RadioButtonGroup;
		public var groupe_radioButtonsMobile: RadioButtonGroup;
		public var groupe_radioButtonsModuleBase: RadioButtonGroup;
		public var groupe_radioButtonsUsages: RadioButtonGroup;
		public var groupe_checkAdmin : CheckBox;
				
		public function ListeDroitCompte()
		{
			
		}
	
		public function init_data_handler(evt : GestionLoginEvent):void
		{
			groupe_radioButtonsModuleBase.selectedValue=loginService.objGroupeClient.MODULE_FACTURATION;
			groupe_radioButtonsCycleDeVie.selectedValue =loginService.objGroupeClient.MODULE_WORKFLOW;
			groupe_radioButtonsUsages.selectedValue = loginService.objGroupeClient.MODULE_USAGE;
			groupe_radioButtonsGestionOrgas.selectedValue=loginService.objGroupeClient.MODULE_GESTION_ORG;
			groupe_radioButtonFixData.selectedValue=loginService.objGroupeClient.MODULE_FIXE_DATA;
			groupe_radioButtonsMobile.selectedValue=loginService.objGroupeClient.MODULE_MOBILE;
			groupe_radioButtonsLogin.selectedValue=loginService.objGroupeClient.MODULE_GESTION_LOGIN;
			
			groupe_checkAdmin.enabled=false;			
			if(loginService.objGroupeClient.DROIT_GESTION_FOURNIS == "1"){
				groupe_checkAdmin.selected = true;
			}
			else
			{
				groupe_checkAdmin.selected = false;
			}
			
			//Gauche

		}
	}
}