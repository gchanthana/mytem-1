package univers.parametres.parametrecompte.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.net.registerClassAlias;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.parametres.parametrecompte.event.GestionSiteEvent;
	import univers.parametres.parametrecompte.service.SiteService;
	import univers.parametres.parametrecompte.vo.PoolGestionnaire;
	import univers.parametres.parametrecompte.vo.Site;
	[Bindable]
	public class ParametreSite extends TitleWindow
	{
		
		public var inpup_cp 			:TextInput;
		public var input_code			:TextInput;
		public var input_commentaire	:TextArea;
		public var input_libelle		:TextInput;
		public var input_ville			:TextInput;
		public var input_adresse		:TextArea;
		public var input_ref			:TextInput;
		
		private var groupeIndex 		:int;
		public var site 				:Site 		= new Site();
		public var siteService 			:SiteService= new SiteService();
		//UI COMPONANT
		public var cbAll 				:CheckBox;
		public var comboPays 			:ComboBox;
		
		public function ParametreSite()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			siteService.addEventListener(GestionSiteEvent.LISTE_PAYS_COMPLETE,reselectComboPays);
			siteService.listePays();
		}
		public function init(evt : FlexEvent):void
		{
			if(site.id_site>0)
			{
				initMode_updateSite();
			}
			else
			{
				initMode_createSite();
			}
			
		}
		private function initMode_createSite():void
		{
			//Aucune modif à faire en mode création
		}
		private function initMode_updateSite():void
		{
			registerClassAlias("univers.parametres.parametrecompte.vo.Site",Site);  
			site= Site(ObjectUtil.copy(site))  // delete les ref sur le grid
			initAttribut();
		}
		private function initAttribut():void
		{
			siteService.getPoolSite(groupeIndex,site.id_site);
		}
		public function valider(e:Event):void
		{
			var ok:Boolean = true;
			if(input_libelle.text == "")
			{
				ok = false;
				input_libelle.errorString = "Champ obligatoire!";
			}
			else
			{
				input_libelle.errorString = "";
			}
			if(input_adresse.text == "")
			{
				ok = false;
				input_adresse.errorString = "Champ obligatoire!";
			}
			else
			{
				input_adresse.errorString = "";
			}
			if(inpup_cp.text == "")
			{
				ok = false;
				inpup_cp.errorString = "Champ obligatoire!";
			}
			else
			{
				inpup_cp.errorString = "";
			}
			if(input_ville.text == "")
			{
				ok = false;
				input_ville.errorString = "Champ obligatoire!";
			}
			else
			{
				input_ville.errorString = "";
			}
			if(comboPays.selectedIndex == -1)
			{
				ok = false;
				comboPays.errorString = "Champ obligatoire!";
			}
			else
			{
				comboPays.errorString = "";
			}
				
			if(ok)
			{
				siteService.addEventListener(GestionSiteEvent.UPDATE_INFO_SITE_COMPLETE,update_info_site_complete);
				
				var idPays : int = -1;
				if(comboPays.selectedItem)
				{
					idPays = comboPays.selectedItem.PAYSCONSOTELID;
				}
				if(site.id_site>0)
				{
					siteService.updateInfoSite(site.id_site,site.libelle_site,site.ref_site,site.code_interne_site,site.adr_site,site.cp_site,site.commune_site,site.commentaire_site,idPays);
				}
				else
				{
					siteService.addSite(groupeIndex,site.libelle_site,site.ref_site,site.code_interne_site,site.adr_site,site.cp_site,site.commune_site,site.commentaire_site,idPays);
				}
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo("Vous devez saisir tous les champs obligatoires !","Consoview",null);
			}
		}
		private function update_info_site_complete(evt : GestionSiteEvent):void
		{
			dispatchEvent(new GestionSiteEvent(GestionSiteEvent.PARAMETRE_SITE_COMPLETE));
			var msg : String;
			if(site.id_site>0)
			{
				msg = "Les modifications ont bien été enregistrées.";
				ConsoviewAlert.afficherOKImage(msg);
				PopUpManager.removePopUp(this);
				
			}
			else
			{
				site.id_site = evt.idSite;
				initAttribut();
			}
		}
		public function onItemChanged(item : Object):void
		{
			var boolExiste : Boolean = false;
			for(var i:int=0;i<siteService.tab_idPool_of_site.length;i++)
			{
				if(siteService.tab_idPool_of_site[i]==item.IDPool)
				{
					siteService.tab_idPool_of_site.removeItemAt(i);
					siteService.updateXpoolSite(site.id_site,[item.IDPool],0);
					boolExiste = true;
				}
			}
			if(!boolExiste) //Si l'item n'est pas déja dans la liste
			{
				siteService.tab_idPool_of_site.addItem(item.IDPool);
				
				siteService.updateXpoolSite(site.id_site,[item.IDPool],1);
			}
			siteService.tab_idPool_of_site.refresh();
		}
		public function cbAllHandler(evt : Event):void
		{
			siteService.tab_idPool_of_site = new ArrayCollection();
			var tmpArray : ArrayCollection = new ArrayCollection();
			for each (var item:PoolGestionnaire in siteService.col_pool_of_site){
				item.boolSelected = cbAll.selected;
				if(item.boolSelected){
					siteService.tab_idPool_of_site.addItem(item.IDPool);
				}
				tmpArray.addItem(item.IDPool);
			}
			if(cbAll.selected)
			{
				siteService.updateXpoolSite(site.id_site,tmpArray.source,1);
			}
			else
			{
				siteService.updateXpoolSite(site.id_site,tmpArray.source,0);
			}
			siteService.col_pool_of_site.refresh();
		}
		public function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		private function reselectComboPays(evt : GestionSiteEvent):void
		{
			
				for (var i : int = 0; i<siteService.col_liste_pays.length;i++)
				{
					if(site.idpays_site>0) 
					{
						if(site.idpays_site == siteService.col_liste_pays.getItemAt(i).PAYSCONSOTELID)
						{
							comboPays.selectedIndex = i;
							break;
							
						}
					}
					else
					{
						if(siteService.col_liste_pays.getItemAt(i).PAYSCONSOTELID == 1577)
						{
							comboPays.selectedIndex = i;
							break;
						}					
					}
				}
			}
		}
}