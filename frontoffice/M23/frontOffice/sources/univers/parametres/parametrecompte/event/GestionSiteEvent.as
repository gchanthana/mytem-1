package univers.parametres.parametrecompte.event
{
	import flash.events.Event;
	
	public class GestionSiteEvent extends Event
	{
		public static const LISTE_PAYS_COMPLETE:String= "listePaysComplete";
		public static const UPDATE_INFO_SITE_COMPLETE:String= "updateInfoSiteComplete";
		public static const PARAMETRE_SITE_COMPLETE:String= "parametrePoolComplete";
		public static const LISTE_CONTRAINTE_SITE_COMPLETE:String= "listeContrainteSiteComplete";
		public static const REMOVE_SITE_COMPLETE:String= "listeContrainteComplete";
		
				
		public var idSite:int;
		
		public function GestionSiteEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,idSite:int = -1)
		{
			super(type, bubbles, cancelable);
			this.idSite = idSite;
		}

	}
}