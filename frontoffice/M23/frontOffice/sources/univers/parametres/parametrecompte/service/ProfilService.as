package univers.parametres.parametrecompte.service
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.parametrecompte.event.GestionProfilEvent;
	import univers.parametres.parametrecompte.vo.Action;
	import univers.parametres.parametrecompte.vo.Gestionnaire;
	import univers.parametres.parametrecompte.vo.Profil;
	[Bindable]
	public class ProfilService extends EventDispatcher
	{
		public var col_profil : ArrayCollection = new ArrayCollection();
		
		public var col_actionOfProfil_mobil : ArrayCollection = new ArrayCollection();
		public var tab_actionOfProfil_mob_selected : ArrayCollection = new ArrayCollection();
		public var col_actionOfProfil_fixe : ArrayCollection = new ArrayCollection();
		public var tab_actionOfProfil_fixe_selected : ArrayCollection = new ArrayCollection();
		
		public var col_userofProfil : ArrayCollection = new ArrayCollection();
		public var col_contrainte_profil : ArrayCollection = new ArrayCollection();
		public var idProfilSelected : int;
		
		public function ProfilService()
		{
		}
		public function listeAction(idGroupe : int,idProfil:int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																	"getActionProfil",
																	listeAction_handler, listeAction_fault_handler);
			RemoteObjectUtil.callService(op,idProfil);
		}
		public function listeContrainteOfProfil(idProfil:int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																	"listeContrainteOfProfil",
																	listeContrainteOfProfil_handler, listeContrainteOfProfil_fault_handler);
			RemoteObjectUtil.callService(op,idProfil);
		}
		public function remove_profil(idProfil:int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																	"removeProfil",
																	remove_profil_handler, remove_profil_fault_handler);
			RemoteObjectUtil.callService(op,idProfil);
		}
		private function remove_profil_handler(evt : ResultEvent):void
		{
			
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.REMOVE_PROFIL_COMPLETE));	
		}
		private function listeContrainteOfProfil_handler(evt : ResultEvent):void
		{
			col_contrainte_profil.removeAll();
			
			var cursor:IViewCursor = (evt.result as ArrayCollection).createCursor();
						
			while(!cursor.afterLast)
			{
				col_contrainte_profil.addItem(cursor.current);	
				cursor.moveNext();
			}
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.LISTE_CONTRAINTE_PROFIL_COMPLETE));	
		}
		public function listeProfil(groupeIndex:int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																	"fournirListeProfiles",
																	listeProfil_handler, listeProfil_fault_handler);
			RemoteObjectUtil.callService(op,groupeIndex);	
		}
		public function updateActionOfProfil(idProfil:int,tab_idAction : Array,value : int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																	"updateXActionProfil",
																	updateActionOfProfil_handler,updateActionOfProfil_fault_handler);
			RemoteObjectUtil.callService(op,idProfil,tab_idAction, value);
		}
		public function updateInfoProfil(idProfil:int,libelle : String,code:String):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"updateProfile",
																		updateActionProfil_handler,updateActionProfil_fault_handler);
			RemoteObjectUtil.callService(op,idProfil,libelle, code);
		}
		public function createProfil(idGroupe:int,libelle : String,code:String):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"createProfile",
																		createProfile_handler,createProfileProfil_fault_handler);
			RemoteObjectUtil.callService(op,idGroupe,libelle, code);
		}
		public function listeUserOfProfil(idGroupe:int,idProfil : int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"listeUserOfProfil",
																		listeUserOfProfil_handler,listeUserOfProfil_fault_handler);
			RemoteObjectUtil.callService(op,idProfil);
			//RemoteObjectUtil.callService(op,idGroupe,idProfil);
		}
		private function listeUserOfProfil_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_userofProfil.removeAll();//vider la collection
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : Gestionnaire = new Gestionnaire();
				item.id=tmpArr[i].APP_LOGINID;
				item.nom=tmpArr[i].LOGIN_NOM ;
				item.prenom=tmpArr[i].LOGIN_PRENOM;
				item.idPool = tmpArr[i].IDPOOL;
				item.libellePool=tmpArr[i].LIBELLE_POOL;
				col_userofProfil.addItem(item);
			}
		}
		private function updateActionProfil_handler(evt : ResultEvent):void
		{
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.INFO_PROFIL_COMPLETE,true,true,evt.result as int));
		}
		private function createProfile_handler(evt : ResultEvent):void
		{
			if(evt.result>1){
				dispatchEvent(new GestionProfilEvent(GestionProfilEvent.INFO_PROFIL_COMPLETE,true,true,evt.result as int));
			}
		}
	
		
		private function updateActionOfProfil_handler (evt : ResultEvent):void
		{
			
		}
		/*private function listeActionOfProfil_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			for(var a :int = 0;a<col_actionOfProfil.length;a++)
			{
				for(i=0; i < tmpArr.length;i++){
					
					if(tmpArr[i].IDINV_ACTIONS == col_actionOfProfil.getItemAt(a).idAction) // Si ce site est dans la liste des actions du profil
					{
						col_actionOfProfil.getItemAt(a).autorise= true;
						tab_actionOfProfil_selected.addItem(tmpArr[i].IDINV_ACTIONS);
					}
				}
			}
			col_actionOfProfil.refresh();
			tab_actionOfProfil_selected.refresh();
			//dispatchEvent(new GestionProfilEvent(GestionProfilEvent.LISTE_PROFIL_COMPLETE));
		}*/
		private function listeAction_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_actionOfProfil_fixe = new ArrayCollection();
			col_actionOfProfil_mobil = new ArrayCollection();
			tab_actionOfProfil_fixe_selected= new ArrayCollection();
			tab_actionOfProfil_mob_selected= new ArrayCollection();
			
			for(i=0; i < tmpArr.length;i++){	
								
				var  item : Action = new Action();
				item.idAction=tmpArr[i].IDINV_ACTIONS; // ou :B1 (String): 1002
				item.libelle=tmpArr[i].LIBELLE_ACTION;
				
				if(tmpArr[i].WORKFLOW == 'MOB')
				{
					item.workflow = 'Mobile'
					if(tmpArr[i].SELECTED =='1')
					{
						tab_actionOfProfil_mob_selected.addItem(tmpArr[i].IDINV_ACTIONS);
						item.autorise = true;
					}
					col_actionOfProfil_mobil.addItem(item);	
				}
				else if(tmpArr[i].WORKFLOW == 'FIXE')
				{
					item.workflow = 'Fixe / Réseau'
					if(tmpArr[i].SELECTED =='1')
					{
						tab_actionOfProfil_fixe_selected.addItem(tmpArr[i].IDINV_ACTIONS);
						item.autorise = true;
					}
					col_actionOfProfil_fixe.addItem(item);	
				}
							
			}
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.LISTE_ACTION_COMPLETE));
		}
		private function listeProfil_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_profil.removeAll();//vider la collection
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : Profil = new Profil();
				item.idProfil=tmpArr[i].IDPROFIL_COMMANDE;
				item.commentaire=tmpArr[i].COMMENTAIRES;
				item.libelle=tmpArr[i].LIBELLE;
				item.codeInterne = tmpArr[i].CODE_INTERNE;
				if(item.codeInterne == 'DEFAUT'){
					item.autorise_edition = false;
				}
				item.nbUser=tmpArr[i].NB ;
				col_profil.addItem(item);
			}
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.LISTE_PROFIL_COMPLETE));
		}
		private function listeProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeProfil_fault_handler'+evt.toString());
		}
		private function listeAction_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeProfil_fault_handler'+evt.toString());
		}
		private function updateActionOfProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateActionOfProfil_fault_handler'+evt.toString());
		}
		private function updateActionProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateActionProfil_fault_handler'+evt.toString());
		}
		private function createProfileProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.createProfileProfil_fault_handler'+evt.toString());
		}
		private function listeUserOfProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeUserOfProfil_fault_handler'+evt.toString());
		}
		private function listeContrainteOfProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeContrainteOfProfil_fault_handler'+evt.toString());
		}
		private function remove_profil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeContrainteOfProfil_fault_handler'+evt.toString());
		}
	}
}