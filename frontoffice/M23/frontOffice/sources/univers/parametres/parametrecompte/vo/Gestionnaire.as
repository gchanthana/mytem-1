package univers.parametres.parametrecompte.vo
{
	[Bindable]
	public class Gestionnaire
	{
		private var _id : int;
		private var _nom : String;
		private var _prenom : String;
		private var _nom_prenom : String;
		private var _login : String;
		public var libellePool : String;
		public var idPool : int;
		
		public var cp : String;
		public var adresse :  String;
		public var ville :  String;
		public var tel :  String;
		public var direction :  String;
		public var mail :  String;
		public var password :  String;
		
		public var restrictIP : Boolean = false;
		
		public function Gestionnaire()
		{
			
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------GET------------------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
		public function get id():int
		{	
			return _id;
		}
	
		public function get login():String
		{	
			return _login;
		}
		public function get nom():String
		{	
			return _nom;
		}
		public function get prenom():String
		{	
			return _prenom;
		}
		public function get nom_prenom():String
		{	
			_nom_prenom=_nom+" "+_prenom; 
			return _nom_prenom;
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------SET------------------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
		public function set id(id:int):void
		{	
			this._id = id;
		}
		
		public function set nom(nom:String):void
		{	
			this._nom =nom ;
		}
		public function set prenom(prenom:String):void
		{	
			this._prenom =prenom ;
		}
		public function set login(login:String):void
		{	
			this._login =login ;
		}
		

	}
}