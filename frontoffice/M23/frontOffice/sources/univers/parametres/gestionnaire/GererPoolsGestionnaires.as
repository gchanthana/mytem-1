package univers.parametres.gestionnaire
{
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;   
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.UniversFunctionItem;
	import univers.parametres.gestionnaire.classemetier.Gestionnaire;
	import univers.parametres.gestionnaire.classemetier.PoolGestionnaire;
	
	public class GererPoolsGestionnaires extends GererPoolsGestionnairesIHM implements UniversFunctionItem {
	
		[Bindable]
		private var _col_pool_gestionnaire : ArrayCollection = new ArrayCollection();
		[Bindable]
		private var _col_gestionnaire : ArrayCollection = new ArrayCollection();
		
		private var _array_revendeursInPools:Array = [];
		
		private var _col_all_revendeur : ArrayCollection;
		
		public function GererPoolsGestionnaires():void
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
		}
			public function onPerimetreChange():void {
			trace("(GestionLogin) : onPerimetreChange");
		}
		//protected override function afterCreationComplete(event:FlexEvent):void {
		public function afterCreationComplete(event:FlexEvent):void {
		
			//Gestion de la navigation
			addEventListener(CloseEvent.CLOSE,fermer);
			
			bt_add_pool.addEventListener(MouseEvent.CLICK,add_pool_handler);
			bt_delete_pool.addEventListener(MouseEvent.CLICK,delete_pool_handler);
			bt_enregistrer_pool.addEventListener(MouseEvent.CLICK,enregistrer_handler);
			
			bt_add_gestionnaire.addEventListener(MouseEvent.CLICK,add_gestionnaire_andler);
			
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer);
			fiche_modif_pool.visible = false;
			dg_pool_gestionnaire.addEventListener(Event.CHANGE,dg_pool_gestionnaire_handler);
			bt_remove_gestionnaire.addEventListener(MouseEvent.CLICK,remove_gestionnaire_handler);
			
			bt_all_site.addEventListener(MouseEvent.CLICK,bt_all_site_handler);
			bt_equipement.addEventListener(MouseEvent.CLICK,equipement_handler);
			bt_abonnement.addEventListener(MouseEvent.CLICK,abonnement_handler);
			
			chekRevendeur.addEventListener(Event.CHANGE,chekRevendeur_handler);
			comboRevendeur.enabled=false;
			
			
			//initialisation des données
			init_dg_pool_gestionnaire();
			init_dg_gestionnaire();
			init_combo_revendeur();
			
					
			//Filtre sur le grid pool de gestionnaire
			text_input_filtre.addEventListener(Event.CHANGE,filtrePoolDeGestionnaire);
			text_input_filtre_gestionnaire.addEventListener(Event.CHANGE,filtreGestionnaire);
			
			//Ecouteur sur le Enter 
			text_input_rechercher.addEventListener(FlexEvent.ENTER,init_dg_pool_gestionnaire);
			imgRechercher.addEventListener(MouseEvent.CLICK,init_dg_pool_gestionnaire);
			
			mode_lecture.addEventListener(Event.CHANGE,changeMode);
			
			afterPerimetreUpdated();
			
		}
		private function changeMode(evt : Event):void
		{
			if(mode_lecture.selected == true)
			{
				initIHM();
			}
			else
			{
				initIHM(true);
			}
		}
		//public override function afterPerimetreUpdated():void {
		public function afterPerimetreUpdated():void {
			
			
			//Alert.show("--Sur ce groupe Vous êtes en mode : "+CvAccessManager.getSession().CURRENT_ACCESS+" et WRITE_MODE = "+ConsoViewSessionObject.WRITE_MODE);
			if(CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)
			{
				initIHM(true);
			}
			else
			{
				initIHM();
			}
			
		}
		protected function initIHM(modeEcriture : Boolean = false):void 
		{
			bt_add_pool.enabled = modeEcriture;
			bt_delete_pool.enabled = modeEcriture;
			bt_enregistrer_pool.enabled = modeEcriture;
			bt_add_gestionnaire.enabled = modeEcriture;
			bt_remove_gestionnaire.enabled = modeEcriture;
		}
		private function init_combo_revendeur():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getPoolRevendeur",
																		process_getPoolRevendeur);
				RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,"",0);
		}
		private function chekRevendeur_handler(evt : Event):void
		{
			
			if(chekRevendeur.selected==true)
			{
				
				comboRevendeur.enabled=true;
			}
			else
			{
				comboRevendeur.enabled=false;
			}
		}
		private function process_getPoolRevendeur(evt : ResultEvent):void
		{
			_col_all_revendeur = new ArrayCollection();
			 
			var cursor:IViewCursor = (evt.result as ArrayCollection).createCursor();
						
			while(!cursor.afterLast)
			{
				_col_all_revendeur.addItem(cursor.current);	
				cursor.moveNext();
			}
			//comboRevendeur.dataProvider = _col_all_revendeur;
			
		}
		private function equipement_handler(evt : MouseEvent):void
		{
			if(dg_pool_gestionnaire.selectedIndex != -1){
			var panel_equipement : EquipementPoolGestionnaire = new EquipementPoolGestionnaire(dg_pool_gestionnaire.selectedItem as PoolGestionnaire);
			PopUpManager.addPopUp(panel_equipement,this,true);
			PopUpManager.centerPopUp(panel_equipement);
			}
			else
			{
				Alert.show("Sélectionnez un pool de gestionnaire")
			}
		}
		private function remove_gestionnaire_handler(evt : MouseEvent):void
		{
			if(dg_pool_gestionnaire.selectedIndex != -1 || dg_gestionnaire_pool.selectedIndex!=-1){
			var idGestionnaire : int = (dg_gestionnaire_pool.selectedItem as Gestionnaire).id;
			//Alert.show("-"+(dg_pool_gestionnaire.selectedItem as PoolGestionnaire).IDPool+"-"+idGestionnaire+"-"+UniversManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"remove_gestionnaire_in_pool",
																		process_gestionnaire_in_pool);
			RemoteObjectUtil.callService(op,(dg_pool_gestionnaire.selectedItem as PoolGestionnaire).IDPool,idGestionnaire,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
			}
			else
			{
				Alert.show("Sélectionnez un pool de gestionnaire + un gestionnaire")
			}
		}
		private function process_gestionnaire_in_pool(re : ResultEvent):void
		{
			//dg_gestionnaire_pool.dataProvider = (dg_pool_gestionnaire.selectedItem as PoolGestionnaire).col_gestionnaire;
			(dg_pool_gestionnaire.selectedItem as PoolGestionnaire).init_dg_gestionnaire_of_pool();
			init_dg_pool_gestionnaire();
		}
		private function bt_all_site_handler (evt : MouseEvent):void
		{
			if (dg_pool_gestionnaire.selectedIndex != -1)
			{
				var panel_all_site : GestionSites = new GestionSites(dg_pool_gestionnaire.selectedItem as PoolGestionnaire);
				PopUpManager.addPopUp(panel_all_site,this,true);
				PopUpManager.centerPopUp(panel_all_site);
			}
			else
			{
				Alert.show("Sélectionnez un pool de gestionnaire");
			}
		}
		private function add_gestionnaire_andler (evt : MouseEvent):void
		{
			if(dg_pool_gestionnaire.selectedIndex!=-1)
			{
				if(dg_gestionnaire.selectedIndex!=-1)
				{
					
					var panel_choix_profile : ChoixProfile = new ChoixProfile(dg_pool_gestionnaire.selectedItem as PoolGestionnaire,dg_gestionnaire.selectedItem as Gestionnaire);
					PopUpManager.addPopUp(panel_choix_profile,this,true);
					PopUpManager.centerPopUp(panel_choix_profile);
					
					//(dg_pool_gestionnaire.selectedItem as PoolGestionnaire).add_gestionnaire((dg_gestionnaire.selectedItem as Gestionnaire).id);
				}
				else
				{
					Alert.show("Un gestionnaire doit être sélectionné");
				}
			}
			else
			{
				Alert.show("Un pool de gestionnaire doit être sélectionné");
			}
		}
		private function dg_pool_gestionnaire_handler(evt:Event):void
		{
			lab_modif.visible=false;
			fiche_modif_pool.visible=true;
			if(dg_pool_gestionnaire.selectedIndex!=-1)
			{
				text_input_code.text= (dg_pool_gestionnaire.selectedItem as PoolGestionnaire).codeInterne_pool;
				text_input_commentaire.text = (dg_pool_gestionnaire.selectedItem as PoolGestionnaire).commentaire_pool;
				text_input_libelle_pool.text = (dg_pool_gestionnaire.selectedItem as PoolGestionnaire).libelle_pool;
				//init_dg_gestionnaire_of_pool();
				select_combo_revendeur();
				dg_gestionnaire_pool.dataProvider = (dg_pool_gestionnaire.selectedItem as PoolGestionnaire).col_gestionnaire;
				(dg_pool_gestionnaire.selectedItem as PoolGestionnaire).init_dg_gestionnaire_of_pool();
				
			}
		}
		private function select_combo_revendeur():void
		{
			
			
			var IDREVENDEUR_POOL:Number = (dg_pool_gestionnaire.selectedItem as PoolGestionnaire).IDPoolRevendeur;
			var DATAT_COMBO : ArrayCollection = ObjectUtil.copy(_col_all_revendeur) as ArrayCollection;
			chekRevendeur.selected = false;
			comboRevendeur.enabled=false;
			
			if(!DATAT_COMBO) return;
			if(!IDREVENDEUR_POOL) return;
			
			chekRevendeur.selected = true;
			comboRevendeur.enabled=true;
			
			
			var IDREVENDEUR_COMBO:Number = 0;	
			var REVENDEUR_TO_SELECT:Object;
			for (var i : int = 0; i< DATAT_COMBO.length;i++)
			{
				IDREVENDEUR_COMBO = DATAT_COMBO.getItemAt(i).IDFOURNISSEUR;
				
				if(IDREVENDEUR_COMBO == IDREVENDEUR_POOL)
				{
					REVENDEUR_TO_SELECT = DATAT_COMBO[i];
					break;
				}
			}
						
			var cursor:IViewCursor = DATAT_COMBO.createCursor();
			
			while(!cursor.afterLast)
			{	
				if(ConsoviewUtil.isPresent(cursor.current.IDFOURNISSEUR,_array_revendeursInPools))
				{
					if(REVENDEUR_TO_SELECT.IDFOURNISSEUR != cursor.current.IDFOURNISSEUR)
					{
						cursor.remove();						
					}
					else
					{
						cursor.moveNext();
					}
				}
				else
				{
					cursor.moveNext()	
				}
			}
			
			DATAT_COMBO.refresh();
			comboRevendeur.dataProvider = DATAT_COMBO;
			comboRevendeur.selectedItem = REVENDEUR_TO_SELECT;
		}
		
		
		private function abonnement_handler(evt : MouseEvent):void
		{
			if(dg_pool_gestionnaire.selectedIndex != -1)
			{
				var panel_abonnement : AbonnerPoolGestionnaires = new AbonnerPoolGestionnaires(dg_pool_gestionnaire.selectedItem as PoolGestionnaire);
				PopUpManager.addPopUp(panel_abonnement,this,true);
				PopUpManager.centerPopUp(panel_abonnement);
			}
			else
			
			{
				Alert.show("Sélectionnez un pool de gestionnaire");
			}
		}
		private function add_pool_handler(evt : MouseEvent):void
		{
			var panelAddPool : CreerPoolGestionnaire = new CreerPoolGestionnaire(this);
			PopUpManager.addPopUp(panelAddPool,this,true);
			PopUpManager.centerPopUp(panelAddPool);
		}
		private function delete_pool_handler(evt : MouseEvent):void
		{
			if(dg_pool_gestionnaire.selectedIndex!=-1)
			{
				
				(dg_pool_gestionnaire.selectedItem as PoolGestionnaire).remove_pool_gestionnaire();
				col_pool_gestionnaire.removeItemAt(dg_pool_gestionnaire.selectedIndex);
			}
			else
			{
				Alert.show("Sélectionnez un pool de gestionnaire");
			}
		}
		private function enregistrer_handler(evt : MouseEvent):void
		{
			var index : int = dg_pool_gestionnaire.selectedIndex;
			var pool : PoolGestionnaire = col_pool_gestionnaire.getItemAt(index) as PoolGestionnaire;
			pool.commentaire_pool = text_input_commentaire.text;
			pool.codeInterne_pool = text_input_code.text;
			pool.libelle_pool=text_input_libelle_pool.text;
			
			if(chekRevendeur.selected==true)
			{
				pool.IDPoolRevendeur = comboRevendeur.selectedItem.IDFOURNISSEUR; 
			}
			else
			{
				pool.IDPoolRevendeur = 0
			}			
			pool.update_pool_gestionnaire();
			init_dg_pool_gestionnaire();		
			fiche_modif_pool.visible = false;	
			lab_modif.visible=true;
		}
		
	
		private function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		//MouseEvent quand clique sur Loupe...
		public function init_dg_pool_gestionnaire(evt : Event = null):void
		{
			var obj_pool : PoolGestionnaire = new PoolGestionnaire();
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getPoolGestionnaire",
																		process_init_dg_pool_gestionnaire);
					RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,text_input_rechercher.text);
		}
		private function process_init_dg_pool_gestionnaire (re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_pool_gestionnaire.removeAll();//Vider la collection
			_array_revendeursInPools = [];
			for(i=0; i < tmpArr.length;i++){	
								
				var  item : PoolGestionnaire = new PoolGestionnaire();
				item.IDPool=tmpArr[i].IDPOOL;
				item.libelle_pool=tmpArr[i].LIBELLE;
				item.codeInterne_pool=tmpArr[i].CODE_INTERNE;
				item.commentaire_pool=tmpArr[i].COMMENTAIRES;
								
				if (tmpArr[i].IDREVENDEUR == null)
				{
					item.IDPoolRevendeur=0;
				}
				else
				{
					item.IDPoolRevendeur=tmpArr[i].IDREVENDEUR;
					_array_revendeursInPools.push(tmpArr[i].IDREVENDEUR);
				}
				col_pool_gestionnaire.addItem(item);
			}
			dg_pool_gestionnaire.dataProvider = col_pool_gestionnaire;
			
			
		}
		private function init_dg_gestionnaire():void
		{
			var obj_pool : PoolGestionnaire = new PoolGestionnaire();			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getGestionnaire",
																		process_init_dg_gestionnaire,erreur);
			RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,"");
		}
		private function process_init_dg_gestionnaire(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			for(i=0; i < tmpArr.length;i++){	
							
				var  item : Gestionnaire = new Gestionnaire();
				item.id=tmpArr[i].APP_LOGINID;
				item.nom=tmpArr[i].LOGIN_NOM;
				item.prenom=tmpArr[i].LOGIN_PRENOM;
											
				_col_gestionnaire.addItem(item);
			}
			dg_gestionnaire.dataProvider = _col_gestionnaire;
		}	
		private function erreur (evt : FaultEvent):void
		{
			Alert.show("Erreur"+evt.toString());
		}
		public function get col_pool_gestionnaire():ArrayCollection
		{
			return _col_pool_gestionnaire;
		}
		public function set col_pool_gestionnaire(col : ArrayCollection):void
		{
			_col_pool_gestionnaire = col;	
		}
		//Filtre 
		private function filtrePoolDeGestionnaire(e:Event):void{
			_col_pool_gestionnaire.filterFunction=filtreGridPoolGestionnaire;
			_col_pool_gestionnaire.refresh();
		}
		//Filtre : chercher la chaine entrée
		public function filtreGridPoolGestionnaire(item:Object):Boolean {
	
			if (
					(item.libelle_pool &&  (item.libelle_pool as String).toLowerCase().search(text_input_filtre.text.toLowerCase()) !=-1)
					|| 
					(item.codeInterne_pool  && (item.codeInterne_pool as String).toLowerCase().search(text_input_filtre.text.toLowerCase())!=-1) 
				) {
				return (true);
			}
			else
			{
				return (false);
			}
		}
		private function filtreGestionnaire(e:Event):void
		{
			if(! _col_gestionnaire) return;
			_col_gestionnaire.filterFunction=filtreGridGestionnaire;
			_col_gestionnaire.refresh();
		}
		//Filtre : chercher la chaine entrée
		public function filtreGridGestionnaire(item:Object):Boolean {
	
			if (item.nom_prenom &&  (item.nom_prenom as String).toLowerCase().search(text_input_filtre_gestionnaire.text.toLowerCase()) !=-1)
			{
				return (true);
			}
			else
			{
				return (false);
			}
		}
	}
}