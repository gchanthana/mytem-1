package univers.parametres.parametrelogin.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.event.GestionTypeCommandeEvent;
	import univers.parametres.parametrecompte.vo.Gestionnaire;
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.service.LoginService;
	
	[Bindable]
	public class ListeLogin extends VBox
	{
		private const ID_CONSOTEL : int = 310812;
		public var loginService: LoginService = new LoginService();
		
		//UI COMPONANT
		private var popupParametreLogin : ParametreLoginIHM;
		public var dgLogin : DataGrid;
		public var txtFiltre : TextInput;
		public var boolCreaLoginAutorised : Boolean = true;
		
		
		public function ListeLogin()
		{
			
		}
		public function init(evt : FlexEvent=null):void
		{
			//définir le nb max login autorisé sur le groupe :
			loginService.addEventListener(GestionLoginEvent.INFO_ACCES_COMPLETE,info_groupe_complete);
			loginService.getRacineInfos(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		
		private function info_groupe_complete(evt : GestionLoginEvent):void
		{
			//récup la liste des logins
			var tmp : Object = 	CvAccessManager.getUserObject();
			if(CvAccessManager.getUserObject().CLIENTID ==ID_CONSOTEL )
			{
				loginService.getAllLogin(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
			}
			else
			{
				loginService.listeLogin(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
			}
			
			
			//loginService.listeLogin(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
			loginService.addEventListener(GestionLoginEvent.LISTE_LOGIN_COMPLETE,listeLoginComplete);
		}
		private function listeLoginComplete(evt : GestionLoginEvent):void
		{
			var marge : int = loginService.objGroupeClient.MARGE_CONTRACTUELLE;
			var nb_login : int =  loginService.objGroupeClient.NB_LOGIN;
			
			var nb_max_login : int =  ((marge/100)*nb_login)+nb_login;
			
			if(loginService.col_login.length>=nb_max_login)
			{
				boolCreaLoginAutorised = false;
			}
			else
			{
				boolCreaLoginAutorised = true;
			}
		}
		public function modifier(item : Gestionnaire):void
		{
			popupParametreLogin = new ParametreLoginIHM();
			popupParametreLogin.login = item;
			addPop();
		}
		private function listeContrainteOfPoolHandler():void
		{
			
		}
		
		private function remove_login_complete_handler(evt  :GestionLoginEvent):void
		{
			var msg : String = "L'utilisateur à bien été supprimé";
			ConsoviewAlert.afficherOKImage(msg);
			init();
		}
		public function addUser(event:MouseEvent):void
		{
			popupParametreLogin = new ParametreLoginIHM();
			addPop();
		}
		
		public function supprimer(data : Gestionnaire):void
		{
			ConsoviewAlert.afficherAlertConfirmation("Êtes vous sûr de vouloir supprimer l'utilisateur "+data.nom_prenom+" ?","Confirmation",supprimerBtnValiderCloseEvent);
		}
		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				loginService.addEventListener(GestionLoginEvent.DELETE_LOGIN,remove_login_complete_handler);
				loginService.deleteLogin(dgLogin.selectedItem.id);
			}
		}
		private function addPop():void
		{
			popupParametreLogin.addEventListener(GestionTypeCommandeEvent.TYPE_COMMANDE_CHANGE,info_type_commande_change_handler);
			popupParametreLogin.addEventListener(GestionPoolEvent.UPDATE_PROFIL_LOGIN,info_pool_change_handler);
			popupParametreLogin.addEventListener(GestionLoginEvent.PARAMETRE_LOGIN_COMPLETE,param_login_complete_handler);
			PopUpManager.addPopUp(popupParametreLogin,this,true);
			PopUpManager.centerPopUp(popupParametreLogin);
		}
		private function info_pool_change_handler(evt : GestionPoolEvent):void
		{
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.UPDATE_PROFIL_LOGIN,true));
		}
		private function info_type_commande_change_handler(evt : GestionTypeCommandeEvent):void
		{
			dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.TYPE_COMMANDE_CHANGE,true));	
		}
		
		
		private function param_login_complete_handler(evt : GestionLoginEvent):void
		{
			
			init();
		}
		protected function filtreHandler(e:Event = null):void{
			loginService.col_login.filterFunction= filtreGrid;
			loginService.col_login.refresh();
		}
		private function filtreGrid(item:Object):Boolean{
			if (((item.nom_prenom as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1 ||
				((item.mail as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1)
			{
				return true;
			}
			else
			{
				return false;	
			}
			
			
		}
		
	}
}