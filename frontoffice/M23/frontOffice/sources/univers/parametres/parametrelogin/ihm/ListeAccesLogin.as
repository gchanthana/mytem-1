package univers.parametres.parametrelogin.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.service.LoginService;
	import univers.parametres.parametrelogin.vo.AccesCompte;
	
	[Bindable]
	public class ListeAccesLogin extends VBox
	{
		public var loginService : LoginService= new LoginService();
		public var idLogin : int;
		public var groupeIndex : int;
		
		//COMPONANT
		public var dg_acces: DataGrid;
		public var popAbonnement : ParametreAbonnementIHM;
				
		
		public function ListeAccesLogin()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		public function init(evt : FlexEvent=null):void
		{
			if(idLogin>0){
			loginService.getAccesOfLogin(idLogin,groupeIndex);
			}
		}
		public function modifier(item : Object):void
		{
			popAbonnement = new ParametreAbonnementIHM();
			popAbonnement.col_all_acces_of_client = loginService.col_acces_not_formated;
			popAbonnement.idLogin = idLogin;
			popAbonnement.accesCompte = dg_acces.selectedItem as AccesCompte;
			addPop();
		}
		public function ajouter():void
		{
			popAbonnement = new ParametreAbonnementIHM();
			popAbonnement.col_all_acces_of_client = loginService.col_acces_not_formated;
			popAbonnement.idLogin = idLogin;
			addPop();
		}
		private function addPop():void
		{
			popAbonnement.addEventListener(GestionLoginEvent.PARAMETRE_ABONNEMENT_COMPLETE,iniData);
			PopUpManager.addPopUp(popAbonnement,this.parentApplication as DisplayObject,true);
			PopUpManager.centerPopUp(popAbonnement);
		}
		private function iniData(evt : GestionLoginEvent):void
		{
			init();
		}
		public function supprimer(item : AccesCompte):void
		{
			var msg : String = "Êtes vous sûr de vouloir supprimer l'abonnement à "+item.libelle+ " ?";	
			ConsoviewAlert.afficherAlertConfirmation(msg,"Confirmation",supprimerBtnValiderCloseEvent);
		}
		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				loginService.addEventListener(GestionLoginEvent.DELETE_ACCES_COMPLETE,remove_acces_handler);
				loginService.deleteAcces(idLogin,(dg_acces.selectedItem as AccesCompte).idAcces);
				
			}
		}
		private function remove_acces_handler(evt : GestionLoginEvent):void
		{
			ConsoviewAlert.afficherOKImage("L'abonnement a bien été supprimé.");
			init();
		}
		
		
		
	}
}