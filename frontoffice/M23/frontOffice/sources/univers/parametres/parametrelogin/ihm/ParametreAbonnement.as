package univers.parametres.parametrelogin.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.registerClassAlias;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.Label;
	import mx.controls.RadioButtonGroup;
	import mx.controls.Tree;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.service.ArbreAccesService;
	import univers.parametres.parametrelogin.service.LoginService;
	import univers.parametres.parametrelogin.util.UtilAcces;
	import univers.parametres.parametrelogin.vo.AccesCompte;
	import univers.parametres.parametrelogin.vo.Arbre;
	[Bindable]
	public class ParametreAbonnement extends TitleWindow
	{
		private var groupeIndex : int;
		public var idLogin : int;
		public var accesCompte : AccesCompte = new AccesCompte();
		public var loginService : LoginService= new LoginService();
		public var arbreAccesService : ArbreAccesService = new ArbreAccesService();
		public var col_all_acces_of_client : ArrayCollection = new ArrayCollection();
		private var feuilleSelected : Object = new Object;
		
		
		//UI COMPONANT
		public var radioButtonFixData : RadioButtonGroup;
		public var radioButtonsCycleDeVie: RadioButtonGroup;
		public var radioButtonsGestionOrgas: RadioButtonGroup;
		public var radioButtonsLogin: RadioButtonGroup;
		public var radioButtonsMobile: RadioButtonGroup;
		public var radioButtonsModuleBase: RadioButtonGroup;
		public var radioButtonsUsages: RadioButtonGroup;
		public var checkAdmin : CheckBox;
		public var monTreeComplet : Tree;
		//public var labStatutArbre : Label;
		public var labMsgInfos : Label;
		public var btCreerAcces : Button;
		
		
		[Embed(source="../../../../assets/images/warning.png")]    
		private var imgInfo : Class;
		
		[Embed(source="../../../../assets/images/nav_right_green.png")]    
		private var imgIconAcces : Class;
		
		[Embed(source="../../../../assets/images/folder_forbidden.png")]    
		private var imgIconVerouille : Class;
		
		
		
		public function ParametreAbonnement()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		public function init(evt : FlexEvent):void
		{
			loginService.addEventListener(GestionLoginEvent.INFO_ACCES_COMPLETE,init_radio_groupe);
			loginService.getRacineInfos(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		
			initListener();
					
			if(accesCompte.idAcces>0)
			{
				initMode_update();
				
			}
			else
			{
				initMode_create();
			}
			
		}
		private function init_radio_groupe(evt : GestionLoginEvent):void
		{
			
			if(loginService.objGroupeClient == 0)
			{
				radioButtonsModuleBase.getRadioButtonAt(1).enabled = false;
				radioButtonsModuleBase.getRadioButtonAt(2).enabled = false;
			}
			else
			{
				if(loginService.objGroupeClient == 1){
					radioButtonsModuleBase.getRadioButtonAt(2).enabled = false;
				}
			}
			if(loginService.objGroupeClient.MODULE_WORKFLOW == 0)
			{
				radioButtonsCycleDeVie.getRadioButtonAt(1).enabled = false;
				radioButtonsCycleDeVie.getRadioButtonAt(2).enabled = false;
			}
			else
			{
				if(loginService.objGroupeClient.MODULE_WORKFLOW == 1){
					radioButtonsCycleDeVie.getRadioButtonAt(2).enabled = false;
				}
			}
		
			if(loginService.objGroupeClient.MODULE_USAGE == 0)
			{
				radioButtonsUsages.getRadioButtonAt(1).enabled = false;
				radioButtonsUsages.getRadioButtonAt(2).enabled = false;
			}
			else
			{
				if(loginService.objGroupeClient.MODULE_USAGE== 1){
					radioButtonsUsages.getRadioButtonAt(2).enabled = false;
				}
			}
			
			if(loginService.objGroupeClient.MODULE_GESTION_ORG == 0)
			{
				radioButtonsGestionOrgas.getRadioButtonAt(1).enabled = false;
				radioButtonsGestionOrgas.getRadioButtonAt(2).enabled = false;
			}
			else
			{
				if(loginService.objGroupeClient.MODULE_GESTION_ORG == 1){
					radioButtonsGestionOrgas.getRadioButtonAt(2).enabled = false;
				}
			}
		
			if(loginService.objGroupeClient.MODULE_FIXE_DATA == 0)
			{
				radioButtonFixData.getRadioButtonAt(1).enabled = false;
				radioButtonFixData.getRadioButtonAt(2).enabled = false;
			}
			else
			{
				if(loginService.objGroupeClient.MODULE_FIXE_DATA == 1){
					radioButtonFixData.getRadioButtonAt(2).enabled = false;
				}
			}
			
			if(loginService.objGroupeClient.MODULE_MOBILE == 0)
			{
				radioButtonsMobile.getRadioButtonAt(1).enabled = false;
				radioButtonsMobile.getRadioButtonAt(2).enabled = false;
			}
			else
			{
				if(loginService.objGroupeClient.MODULE_MOBILE == 1){
					radioButtonsMobile.getRadioButtonAt(2).enabled = false;
				}
			}
		
			if(loginService.objGroupeClient.MODULE_GESTION_LOGIN == 0)
			{
				radioButtonsLogin.getRadioButtonAt(1).enabled = false;
				radioButtonsLogin.getRadioButtonAt(2).enabled = false;
			}
			else
			{
				if(loginService.objGroupeClient.MODULE_GESTION_LOGIN == 1){
					radioButtonsLogin.getRadioButtonAt(2).enabled = false;
				}
			}
			
			if(loginService.objGroupeClient.DROIT_GESTION_FOURNIS == "1"){
				checkAdmin.enabled = true;
			}
			else
			{
				checkAdmin.enabled = false;
			}
		}
		
		private function initListener():void
		{
			monTreeComplet.addEventListener(MouseEvent.CLICK,updateArbreComplet);
			
			//Les buttonRadios :
			radioButtonsCycleDeVie.addEventListener(Event.CHANGE,updateCycleDeVie);
			radioButtonsGestionOrgas.addEventListener(Event.CHANGE,updateGestionOrgas);
			radioButtonsModuleBase.addEventListener(Event.CHANGE,updateModuleBase);
			radioButtonsUsages.addEventListener(Event.CHANGE,updateUsages);
			radioButtonFixData.addEventListener(Event.CHANGE,updateFixeData);
			radioButtonsMobile.addEventListener(Event.CHANGE,updateMobile);
			radioButtonsLogin.addEventListener(Event.CHANGE,updateLogin);
			checkAdmin.addEventListener(Event.CHANGE,changeDroitFournisseur);
			
			
			radioButtonsCycleDeVie.selectedValue = 0;
			radioButtonsGestionOrgas.selectedValue = 0;
			radioButtonsModuleBase.selectedValue = 2;
			radioButtonsUsages.selectedValue = 0;
			radioButtonFixData.selectedValue = 0;
			radioButtonsMobile.selectedValue = 0;
			radioButtonsLogin.selectedValue = 0;
			checkAdmin.selected = false;
		}
		private function initRadioAcces():void
		{
			radioButtonsModuleBase.selectedValue=accesCompte.module_facturation;
			radioButtonsCycleDeVie.selectedValue =accesCompte.module_workflow; 
			radioButtonsUsages.selectedValue = accesCompte.module_usage;
			radioButtonsGestionOrgas.selectedValue=accesCompte.module_gestion_orga;
			radioButtonFixData.selectedValue=accesCompte.module_fixe_data;
			radioButtonsMobile.selectedValue=accesCompte.module_mobile;
			radioButtonsLogin.selectedValue=accesCompte.module_gestion_login;
			if(accesCompte.module_fournisseur == 1){
				checkAdmin.selected = true;
			}
			else
			{
				checkAdmin.selected = false;
			}
		}
		private function initMode_create():void
		{
			loginService.addEventListener(GestionLoginEvent.LISTE_ACCES_COMPLETE,initArbre);
			loginService.getAccesOfLogin(idLogin,groupeIndex);
		}
		private function initMode_update():void
		{
			registerClassAlias("univers.parametres.parametrecompte.vo.AccesCompte",AccesCompte);  
			accesCompte= AccesCompte(ObjectUtil.copy(accesCompte))  // delete les ref sur le grid
			initRadioAcces();
		}
		
		public function btCreerAccesHandler(evt : MouseEvent):void
		{
			if(monTreeComplet.selectedItem){
				feuilleSelected = monTreeComplet.selectedItem;
				var idBranche : int = parseInt(monTreeComplet.selectedItem.@NID,10); 
				arbreAccesService.addEventListener(GestionLoginEvent.CREATE_ABONNEMENT_COMPLETE,refreshArbre);
				arbreAccesService.creerAcces(idBranche,idLogin);
			}
			else
			{
				
			}
		}
		public function refreshArbre(evt : GestionLoginEvent):void
		{
			accesCompte.idAcces = monTreeComplet.selectedItem.@NID;
			accesCompte.libelle = monTreeComplet.selectedItem.@LBL;
			//arbreAccesService.initArbre(groupeIndex);
			//loginService.addEventListener(GestionLoginEvent.LISTE_ACCES_COMPLETE,initArbre);
			//loginService.getAccesOfLogin(idLogin,groupeIndex);
		}
		
		public function fermer(evt : Event):void
		{
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.PARAMETRE_ABONNEMENT_COMPLETE));
			PopUpManager.removePopUp(this);
		}
		private function updateModuleBase(event : Event):void
		{
			setDroit("fac",radioButtonsModuleBase.selectedValue as int);
		}
		private function updateCycleDeVie(event : Event):void
		{		
			setDroit("wor",radioButtonsCycleDeVie.selectedValue as int);
		}
		private function updateUsages(event : Event):void
		{
			setDroit("usa",radioButtonsUsages.selectedValue as int);
		}
		private function updateGestionOrgas(event : Event):void
		{
			setDroit("gor",radioButtonsGestionOrgas.selectedValue as int);
		}
		private function updateFixeData(event : Event):void
		{
			setDroit("fid",radioButtonFixData.selectedValue as int);
		}
		private function updateMobile(event : Event):void
		{
			setDroit("mob",radioButtonsMobile.selectedValue as int);
		}	
		private function updateLogin(event : Event):void
		{
			setDroit("glo",radioButtonsLogin.selectedValue as int);
		}			
		private function changeDroitFournisseur(event : Event):void
		{
			if (checkAdmin.selected==true)
			{
				setDroit("gfo",1);
			}
			else
			{
				setDroit("gfo",0);
			}
		}
		//Modification dnas l'onglet "Acces" des droit d'un utilisateur : requete lancée à chaque changement 
		private function setDroit(module : String,valeur : int):void{
			loginService.updateParametreAbonnement(module,valeur,idLogin,accesCompte.idAcces);
		}
		private function initArbre(evt : GestionLoginEvent=null):void
		{
			col_all_acces_of_client = loginService.col_acces_not_formated;
			arbreAccesService.addEventListener(GestionLoginEvent.INIT_ARBRE_COMPLETE,initDataArbre);
			arbreAccesService.initArbre(groupeIndex);
		}
		private function initDataArbre(evt : GestionLoginEvent=null):void
		{
			//monTreeComplet.setVisible(false);
			arbreAccesService.addEventListener(GestionLoginEvent.TRAITEMENT_COHERENCE_ARBRE_COMPLETE,initIconeArbre);
			arbreAccesService.traitementCoherence(col_all_acces_of_client);
		}
		private function initIconeArbre(evt : GestionLoginEvent):void
		{
			//dataArbre = objDonnee.dataArbre as XML;
			
			var dataArbre : XML = arbreAccesService.dataArbre
			var arrayAccesInterdit : ArrayCollection = arbreAccesService.arrayAccesInterdit;
			monTreeComplet.dataProvider= dataArbre;

				dataArbre.@isAcces = false;
				for (var b1 : int  = 0 ; b1 <col_all_acces_of_client.length;b1++){
					if 	(dataArbre.@NID ==(col_all_acces_of_client.getItemAt(b1).ID)){
						dataArbre.@isAcces=true;
						dataArbre.@isInterdit=false;
						//trace("--------------------------------------------------------------------LA RACINE EST UN ACCES");
					}
				}
				if (dataArbre.@isAcces != true){
				for (var b : int  = 0 ; b <arrayAccesInterdit.length;b++){
					if 	(dataArbre.@NID ==(arrayAccesInterdit.getItemAt(a) as UtilAcces).getId()){
						dataArbre.@isInterdit=true;
						dataArbre.@isAcces=false;
						//trace("--------------------------------------------------------------------LA RACINE EST EN WARNIG");
					}
				}
				}
				for each(var uneRacine:XML in (dataArbre).children()){
		 			if (dataArbre.@isAcces == true){
						uneRacine.@isVerouille=true;
						monTreeComplet.setItemIcon(uneRacine,imgIconVerouille,imgIconVerouille);
						//trace("--------------------------------------------------------------------uneRacine.@isVerouille=true;");
					}
					else
					{
					
						for (var a : int  = 0 ; a <arrayAccesInterdit.length;a++){
							
							if 	(uneRacine.@NID ==(arrayAccesInterdit.getItemAt(a) as UtilAcces).getId()){
								uneRacine.@isInterdit =true;
								
							}
						}
						for (var b2 : int  = 0 ; b2 <col_all_acces_of_client.length;b2++){
							if 	(uneRacine.@NID ==(col_all_acces_of_client.getItemAt(b2).ID)){
								uneRacine.@isInterdit =false;
								uneRacine.@isAcces =true;
								uneRacine.@isVerouille = true;
							}
						}
					}				
				}
				
				monTreeComplet.dataDescriptor = new Arbre(monTreeComplet,arrayAccesInterdit,col_all_acces_of_client);
				monTreeComplet.setVisible(true);
				monTreeComplet.expandItem(monTreeComplet.getChildAt(0),true,true);
			}
		//Evenement sur l'arbre : Définir le type de noeud selectioné : acces / interdi / verouillé
	//Afficher le message correspondant
	//Gérer l'activation des bt supprimer ou ajouter
	private function updateArbreComplet(event:Event):void{
		//mettreAjourDGloginParOrgas(monTreeComplet.selectedItem.@NID);	
		if(monTreeComplet.selectedIndex!=-1)
		{
			var autoriseCreation : Boolean = true;
			if (monTreeComplet.selectedItem.hasOwnProperty("@isAcces")){
				if(monTreeComplet.selectedItem.@isAcces==true){
					btCreerAcces.enabled=false;
					labMsgInfos.text = "l'utilisateur possède déja cet accès"
					autoriseCreation = false;
				}
				else
				{
					if(monTreeComplet.selectedItem.@isInterdit==true){
						btCreerAcces.enabled=false;
						labMsgInfos.text = "Supprimez les droits inférieurs"
						autoriseCreation = false;
					}
				}
			}
			else
			{
				if (monTreeComplet.selectedItem.hasOwnProperty("@isInterdit")){
					if(monTreeComplet.selectedItem.@isInterdit==true){
						btCreerAcces.enabled=false;
						labMsgInfos.text = "Supprimez les droits inférieurs"
						autoriseCreation = false;
					}
				}
				
					if (monTreeComplet.selectedItem.hasOwnProperty("@isVerouille")){
						if(monTreeComplet.selectedItem.@isVerouille==true){
							btCreerAcces.enabled=false;
							labMsgInfos.text = "l'utilisateur possède des droits supérieurs"
							autoriseCreation = false;
						}
					}
			}
			if (autoriseCreation == true){
				if (monTreeComplet.selectedIndex!=-1){
					btCreerAcces.enabled = true;
					labMsgInfos.text="";
				}
				else
				{
					btCreerAcces.enabled=false;
					labMsgInfos.text = ""
				}
			}
		}
		else
		{
			btCreerAcces.enabled=false;	
		}
	}	
	
	}
}