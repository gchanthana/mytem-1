package composants.tb.tableaux.info
{
	[Bindable]
	public class InfosPanelVo
	{
		public var NOM:String = "";
		public var PRENOM:String = "";
		public var IDSOUS_TETE:Number = 0;
		public var SOUS_TETE:String = "";
		public var IDTETE_LIGNE:Number = 0;
		public var TETE_LIGNE:String = "";
		public var IDTYPE_LIGNE:Number = 0;
		public var LIBELLE_TYPE_LIGNE:String = "";
		public var SITEINSTALLID:Number = 0;
		public var SITEID:Number = 0;
		public var NOM_SITE_INSTALL:String = "";
		public var ADRESSE1_INSTALL:String = "";
		public var ADRESSE2_INSTALL:String = "";
		public var ZIPCODE_INSTALL:String = "";
		public var VILLE_INSTALL:String = "";
		public var CODE_SITE_INSTALL:String = "";
		public var NBR_LIGNE:Number = 0;
		public var IDENTIFIANT_COMPLEMENTAIRE:String = "";
		public var CODE_INTERNE:String = "";
		public var BOOL_PRINCIPAL:Number = 1;
		public var BOOL_BACKUP:Number = 0;
		public var COMMENTAIRES_GENERAL:String = "";
		public var USAGE:String = "";
		public var POPULATION:String = "";
		public var IDTYPE_RACCORDEMENT:Number = 0;
		public var TYPE_RACCORDEMENT:String = "";
		public var ETAT:String = "";		
		public var TYPE_FICHELIGNE:String = "";
		public var DATE_COMMANDE:Date = null;		
		public var DATE_RESILIATION:Date = null;
		public var DATE_RACCORDEMENT:Date = null;
		public var DATE_SUSPENSION:Date = null;		
	} 
}
