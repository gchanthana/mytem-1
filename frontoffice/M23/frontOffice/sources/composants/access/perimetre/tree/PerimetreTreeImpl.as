package composants.access.perimetre.tree {
	import composants.access.GenericPerimetreTree;

	/**
	 * Ne pas modifier cette classe
	 * Créer plutôt une sous classe de celle ci
	 * */
	public class PerimetreTreeImpl extends GenericPerimetreTree {
		public function PerimetreTreeImpl() {
			super();
		}
	}
}
