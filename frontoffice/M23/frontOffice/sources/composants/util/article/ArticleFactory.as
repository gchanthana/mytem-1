package composants.util.article
{
	public class ArticleFactory
	{
		public function ArticleFactory()
		{
		}
		
		public static const DEFAULT:String="DEFAULT";
		public static const MOBILE:String="NEW_MOBILE";
		public static const NEW_MOBILE:String="NEW_MOBILE";
		public static const EQUIP_NUE:String="EQUIP_NUE";
		public static const FIXE:String="FIXE";
		public static const DATA:String="DATA";
		
		public static function createArticle(type:String="DEFAULT"):Article
		{
			switch(type)
			{
				case NEW_MOBILE:
				{
					return new ArticleNouvelleLigneMobile()
				}
				case MOBILE:
				{
					return new ArticleMobile()
				}
				case EQUIP_NUE:
				{
					return new ArticleEquipementNue()
				}
				case DEFAULT:
				{
					return new Article()
				}
				default:return new Article()
			}
		}

	}
}