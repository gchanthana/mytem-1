package composants.util
{
	import mx.core.UIComponent;
	import mx.core.IUIComponent;
	import mx.managers.ToolTipManager;
	import mx.skins.halo.ToolTipBorder;
	import mx.controls.ToolTip;
	import mx.core.IToolTip;
	import flash.geom.Point;
	
	public class CustomToolTipeInfo
	{
		    
	    public static const RIGHT :String = "errorTipRight";
	    public static const BELOW :String = "errorTipBelow";
	    public static const ABOVE :String = "errorTipAbove";
	    
		 
			
		/**
		 * Crée le message d'aide du composant
		 * */
		public static function createBigTip(uicomponent : UIComponent , message : String , tipPlacement : String):ToolTip {
			uicomponent.toolTip = null;		   
		   	var ptLocale : Point = new Point(uicomponent.x,uicomponent.y);
		   	var pt : Point = uicomponent.localToGlobal(ptLocale);
		   	var deltaX : Number = 0;
		   	var deltaY : Number = 0;
		   	
		   	switch(tipPlacement){
		   		case BELOW: {
		   			deltaX = 0;
		   			deltaY = 25;	
		   			break;
		   		}
		   		case ABOVE: {
		   			deltaX = 0;
		   			deltaY = 0;	
		   			break;
		   		}
		   		case RIGHT: {
		   			deltaX = (uicomponent.width) + 10;
		   			deltaY = 0;	
		   			break;
		   		}		   		
		   		default : {
		   			tipPlacement = BELOW;
		   			deltaX = 0;
		   			deltaY = 25;	
		   			break;
		   		}
		   	}
		   
		   	return ToolTipManager.createToolTip(message,pt.x+deltaX,pt.y +deltaY,tipPlacement,uicomponent) as ToolTip;
		
		}
		
		
		/**
		 * Détruit le message d'aide du composant
		 * */
		public static function destroyBigTip(unTooltip : ToolTip):void {			
			if (unTooltip != null){
				ToolTipManager.destroyToolTip(unTooltip);	
			}
		}
	}
}