package composants.util.pays
{
	///////////////////////////////////////////////////////////
	//  GestionPays.as
	//  Macromedia ActionScript Implementation of the Class GestionPays
	//  Generated by Enterprise Architect
	//  Created on:      20-ao�t-2008 16:26:34
	//  Original author: samuel.divioka
	///////////////////////////////////////////////////////////

	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;

	/**
	 * [Bindable]
	 * @author samuel.divioka
	 * @version 1.0
	 * @created 20-ao�t-2008 16:26:34
	 */
	 
	[Bindable]
	public class GestionPays
	{
		private static const PAYS_DEFAUT : String = "FRANCE";
		
		private var _editionPays : Number;
		
	    private var _listePays: ArrayCollection;

	    private var _pays: Pays;
	    
	    private var _paysDefautIndex : Number;
	    
	    public var httpRequest:HTTPService = new HTTPService();
	    

	    	    
	    /**
	     * Fournit la liste des pays
	     */
	    public function getListePays(p:Number = -1): void
	    {
	    	editionPays = p;
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.revendeurs.GestionPays",
					"get_liste_pays", getListePaysResultHandler);
					
			RemoteObjectUtil.callService(opData);	    	
	    }

	    /**
	     * ex : listePays = re.result as ArrayCollection
	     * 
	     * @param re
	     */
	    protected function getListePaysResultHandler(re:ResultEvent): void
	    {
	    	var source:ArrayCollection = re.result as ArrayCollection;  
           	var cursor:IViewCursor = source.createCursor();
            listePays = new ArrayCollection();
            var i : Number = 0;	    	
           while (!cursor.afterLast)
            {
            	var currentObj:Object = cursor.current;
            	var localPays : Pays = new Pays()
            	localPays.LIBELLE = currentObj['PAYS'];
            	localPays.ID = currentObj['PAYSCONSOTELID'];
            	
            	listePays .addItem(localPays);
            	if(_editionPays != -1){
            		if(localPays.ID == _editionPays){
	            		pays = localPays;
	            		paysDefautIndex = i;
            		}
            	}else if(localPays.LIBELLE.toUpperCase() == PAYS_DEFAUT){
            		pays = localPays;
            		paysDefautIndex = i;
            	}
            	i++;
            	cursor.moveNext();
          	}
	    }




	    public function set listePays(liste:ArrayCollection): void
	    {
	    	_listePays = liste;
	    }

	    public function get listePays(): ArrayCollection
	    {
	    	return _listePays;
	    }




	    public function get pays(): Pays
	    {
	    	return _pays;
	    }
    
	    public function set pays(p:Pays): void
	    {
	    	_pays = p;
	    }
	    
	    
	    
	    
	    public function get editionPays():Number{
	    	return _editionPays;
	    }
	    
	    public function set editionPays(p:Number):void{
	    	_editionPays = p;
	    }
	    
	    
	    
	    	    
	    public function get paysDefautIndex():Number{
	    	return _paysDefautIndex;
	    }
	    
	    public function set paysDefautIndex(p:Number):void{
	    	_paysDefautIndex = p;
	    } 	    

	}//end GestionPays
}	