package appli.login {
	import flash.events.Event;

	public class LoginEvent extends Event {
		public static const VALIDE:String = "SUCCESS AUTH";
		public static const INVALIDE:String = "FAILED AUTH";
		public static const IP_ADDRESS_INVALIDE:String = "IP_ADDRESS_INVALIDE FAILED AUTH";
		public static const UNKNOWN_ERROR:String = "AUTH OPERATION UNKNOWN ERROR";
		
		public function LoginEvent(type:String) {
			super(type);
		}
	}
}
