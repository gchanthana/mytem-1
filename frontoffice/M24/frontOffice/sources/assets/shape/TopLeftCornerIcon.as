package assets.shape
{
	import flash.display.Sprite;
	import flash.filters.DropShadowFilter;
	
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	
	public class TopLeftCornerIcon extends UIComponent
	{
		private var _triangleShape:Sprite=new Sprite();		
		
		
		private function _creationCompelete(evt:FlexEvent):void
		{
			
			this.addChild(_triangleShape);
			this.useHandCursor=true;
			this.mouseChildren=true;
		}
		
		public function TopLeftCornerIcon(sidewidth:int,color:uint = 0x2147AB)
		{
			super();
		
			
			
			_triangleShape.graphics.beginFill(color);
			_triangleShape.graphics.lineTo(sidewidth + 5, 0);
			_triangleShape.graphics.lineTo(0, sidewidth + 5);
			_triangleShape.graphics.lineTo(0, 0);
			_triangleShape.alpha=0.8;
			_triangleShape.filters=[new DropShadowFilter(1, 45, 0, 0.5)];
			
			addEventListener(FlexEvent.CREATION_COMPLETE,_creationCompelete);
		}
	}
}