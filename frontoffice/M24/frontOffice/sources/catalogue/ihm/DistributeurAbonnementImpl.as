package catalogue.ihm
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import catalogue.AbstractCatalogue;
	import catalogue.event.CatalogueEvent;
	import catalogue.ihm.catalogueperso.CreateCatClientAbonnementIHM;
	import catalogue.ihm.popup.PopupFilterIHM;
	import catalogue.profil.GestionProfil;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.util.CatalogueFormatteur;
	import catalogue.util.Currency;
	import catalogue.vo.AbonnementVO;
	import catalogue.vo.OperateurVO;
	import catalogue.vo.ParametreSearchCatalogueVO;
	import catalogue.vo.RevendeurVO;
	
	import composants.util.ConsoviewAlert;
	
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	import searchpaginatedatagrid.ParametresRechercheVO;

	[Bindable]
	public class DistributeurAbonnementImpl extends AbstractCatalogue 
	{
		public const NBITEMPARPAGE : int = 30;
		public var comboRevendeur : ComboBox;
		public var comboOperateur : ComboBox;
		
		private var popCreateCatalogue 		:CreateCatClientAbonnementIHM;
		public var labelRevendeur 			:Label;
		private var _popUpFilter			:PopupFilterIHM;
		public var listFilter				:ArrayCollection;
		public var globalCatalogueService : GlobalCatalogueService = new GlobalCatalogueService(); 
		
		public function createCatalogueHandler(evt : Event):void
		{
			popCreateCatalogue = new CreateCatClientAbonnementIHM();
			popCreateCatalogue.idDistributeur = (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR;
			PopUpManager.addPopUp(popCreateCatalogue,this,true);
			PopUpManager.centerPopUp(popCreateCatalogue);
		}
		public function DistributeurAbonnementImpl()
		{
			globalCatalogueService.addEventListener(CatalogueEvent.LISTE_OPERATEUR_COMPLETE,dataOpChange);
		}
		public function dataOpChange(evt : CatalogueEvent):void
		{
			if(globalCatalogueService.listeOperateur.length==1)
			{
				comboOperateur.selectedIndex=0;
			}
		}
		public function initData():void
		{
			thisComponentIsComplete = true;
			updateProfil();
		}
		
		override public function updateProfil():void
		{
			if(thisComponentIsComplete){
				switch (gestionProfil.CURRENT_PROFIL)
				{
					case (GestionProfil.CONSOTEL_PROFIL):
						catalogueCS.rechercheRevendeur();
						comboRevendeur.selectedIndex = -1;
						comboRevendeur.enabled = true;
						comboRevendeur.visible = true;
						comboRevendeur.includeInLayout = true;
						labelRevendeur.text = "";
						
					break;
					case (GestionProfil.DISTRIBUTEUR_PROFIL):
						var rev : RevendeurVO = new RevendeurVO();
						rev.ID_REVENDEUR = gestionProfil.ID_CURRENT_PROFIL;
						rev.LIBELLE_REVENDEUR = gestionProfil.LIBELLE_CURRENT_PROFIL;
						rev.SYMBOL_DEVISE = gestionProfil.REVENDEUR.SYMBOL_DEVISE;
						rev.SELECTED = true;
						
						catalogueCS.listeRevendeur.removeAll();
						catalogueCS.listeRevendeur.addItem(rev);
						comboRevendeur.selectedIndex = 0;
						majSelectDistributeur();
						comboRevendeur.enabled = false;
						comboRevendeur.visible = false;
						comboRevendeur.includeInLayout = false;
						labelRevendeur.text = rev.LIBELLE_REVENDEUR;
						comboRequiredHandler();
					break;
				}
			}
		}
		
		//popup loop distributeur
		public function btImgChooseDistributeur_clickHandler(e:Event):void
		{
			var listDistributeur:ArrayCollection = catalogueCS.listeRevendeur as ArrayCollection;
			this.refreshlistFilter(listDistributeur,1);
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.listeFilter = listFilter;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_distributeur');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', getItemFilterHandler);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		protected function getItemFilterHandler(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', getItemFilterHandler);
			refreshComboDistributeur();
		}
		
		private function refreshComboDistributeur():void
		{
			var k:int;
			k=_popUpFilter.getItemCurrentID();
			for (var i:int=0; i<catalogueCS.listeRevendeur.length;i++)
			{
				(catalogueCS.listeRevendeur[i] as RevendeurVO ).SELECTED =false;
				if ((catalogueCS.listeRevendeur[i] as RevendeurVO).ID_REVENDEUR == _popUpFilter.getItemCurrentID())
				{
					(catalogueCS.listeRevendeur[i] as RevendeurVO).SELECTED =true;
					comboRevendeur.selectedIndex = i;
				}
			}
			comboRevendeurHandler();
		}
		
		public function comboRevendeurHandler():void
		{
			majSelectDistributeur();
			comboRequiredHandler();
		}
		
		private function majSelectDistributeur():void
		{
			for (var i:int=0; i<catalogueCS.listeRevendeur.length;i++)
			{
				(catalogueCS.listeRevendeur[i] as RevendeurVO).SELECTED =false;
				if ((catalogueCS.listeRevendeur[i] as RevendeurVO).ID_REVENDEUR == (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR)
				{
					(catalogueCS.listeRevendeur[i] as RevendeurVO).SELECTED =true;
				}
			}
		}
				
		//popup loop operateur
		
		public function btImgChooseOperateur_clickHandler(e:Event):void
		{
			var listOp:ArrayCollection = globalCatalogueService.listeOperateur as ArrayCollection;
			this.refreshlistFilter(listOp,2);
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.listeFilter = listFilter;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_operateur');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', getItemFilterHandler1);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		protected function getItemFilterHandler1(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', getItemFilterHandler1);
			refreshComboOperateur();
		}
		
		private function refreshComboOperateur():void
		{
			var k:int;
			k=_popUpFilter.getItemCurrentID();
			for (var i:int=0; i<globalCatalogueService.listeOperateur.length;i++)
			{
				(globalCatalogueService.listeOperateur[i] as OperateurVO).SELECTED =false;
				if ((globalCatalogueService.listeOperateur[i] as OperateurVO).ID_OP == _popUpFilter.getItemCurrentID())
				{
					(globalCatalogueService.listeOperateur[i] as OperateurVO).SELECTED =true;
					comboOperateur.selectedIndex = i;					
				}
			}
			comboOperateurHandler();
		}
		
		private function majSelectOperateur():void
		{
			for (var i:int=0; i<globalCatalogueService.listeOperateur.length;i++)
			{
				(globalCatalogueService.listeOperateur[i] as OperateurVO).SELECTED =false;
				if ((globalCatalogueService.listeOperateur[i] as OperateurVO).ID_OP == (comboOperateur.selectedItem as OperateurVO).ID_OP)
				{
					(globalCatalogueService.listeOperateur[i] as OperateurVO).SELECTED =true;
					comboOperateur.selectedIndex = i;					
				}
			}
		}
		
		private function refreshlistFilter(listdata:ArrayCollection, dataFilter:int):void
		{
			listFilter =new ArrayCollection();
			switch (dataFilter)
			{
				case 1:
					for (var i:int=0; i<listdata.length;i++)
					{
						var item:Object = new Object();
						item.SELECTED = (listdata.getItemAt(i) as RevendeurVO).SELECTED;
						item.IDITEM = (listdata.getItemAt(i) as RevendeurVO).ID_REVENDEUR;
						item.LIBELLE_ITEM = (listdata.getItemAt(i) as RevendeurVO).LIBELLE_REVENDEUR;
						listFilter.addItem(item);
					}
					break;
				case 2:
					for (var j:int=0; j<listdata.length;j++)
					{
						var item:Object = new Object();
						item.SELECTED = (listdata.getItemAt(j) as OperateurVO).SELECTED;
						item.IDITEM = (listdata.getItemAt(j) as OperateurVO).ID_OP;
						item.LIBELLE_ITEM = (listdata.getItemAt(j) as OperateurVO).LIBELLE_OP;
						listFilter.addItem(item);
					}
					break;
				default:
			}
			
		}
		
		public function comboOperateurHandler():void
		{
			majSelectOperateur();
		}
		
		public function comboRequiredHandler():void
		{
			if(comboRevendeur.selectedIndex != -1)
			{
				globalCatalogueService.addEventListener(CatalogueEvent.LISTE_OPERATEUR_COMPLETE,liste_operateur_complete);
				globalCatalogueService.listOpDistrib((comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR);
				CatalogueFormatteur.getInstance().distributorCurrency = new Currency(comboRevendeur.selectedItem.SYMBOL_DEVISE); //Bug to be fixed
			}
		}
		private function liste_operateur_complete(rvt : CatalogueEvent):void
		{
			if(globalCatalogueService.listeOperateur.length==1)
			{
				comboOperateur.selectedIndex=0;
				majSelectOperateur();
			}
			else
			{
				//this.enabled = true;
			}
		} 
		
		public function supprimer(data : AbonnementVO):void
		{
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M24', '_tes_vous_s_r_de_vouloir_supprimer_l_abo')+data.LIBELLE+ResourceManager.getInstance().getString('M24', '__'),ResourceManager.getInstance().getString('M24', 'Confirmation'),supprimerBtnValiderCloseEvent);
		}


		override public function rechercher(parametresRechercheVO : ParametresRechercheVO, itemIntervalleVO : ItemIntervalleVO = null):void
		{
				
			if(comboOperateur.selectedItem)
			{
				if(!itemIntervalleVO) // Si c'est une nouvelle recherche
				{
					myPaginatedatagrid.refreshPaginateDatagrid(true);
					itemIntervalleVO = new ItemIntervalleVO();
					itemIntervalleVO.indexDepart = 0;
					itemIntervalleVO.tailleIntervalle = NBITEMPARPAGE;
				}
				//Ajout des 3 paramêtres spécifique au catalogue :
				var parametreSearchCatalogueVO : ParametreSearchCatalogueVO = new ParametreSearchCatalogueVO();
				parametreSearchCatalogueVO.IDOP =(comboOperateur.selectedItem as OperateurVO).ID_OP;
				parametreSearchCatalogueVO.ORDER_BY=parametresRechercheVO.ORDER_BY;
				parametreSearchCatalogueVO.SEARCH_TEXT=parametresRechercheVO.SEARCH_TEXT;
				parametreSearchCatalogueVO.GROUPE_INDEX =groupeIndex;
				catalogueCS.rechercheAbonnementCatalogue(parametreSearchCatalogueVO,itemIntervalleVO);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24',"_Un_op_rateur_doit__tre_selectionn__"));
			}
		}
		private function removeEqCompleteHandler(evt : CatalogueEvent):void
		{
		
		}
		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			
		}
		
	}
}