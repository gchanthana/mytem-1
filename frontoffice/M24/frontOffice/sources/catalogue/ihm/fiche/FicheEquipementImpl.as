package catalogue.ihm.fiche
{
	import catalogue.strategie.ICatalogue;
	import catalogue.vo.EquipementCatalogueVO;
	
	import flash.events.Event;
	
	import mx.containers.TitleWindow;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;


	[Bindable]
	public class FicheEquipementImpl extends TitleWindow
	{
		
		private var _catalogueCS:ICatalogue;
		private var _equipement:EquipementCatalogueVO = new EquipementCatalogueVO();
				
		public function FicheEquipementImpl()
		{
				
		}
		public function init(evt : FlexEvent):void
		{
			
		}
		
		public function fermer(event : Event=null):void
		{
			PopUpManager.removePopUp(this);
		}
		public function set catalogueCS(value:ICatalogue):void
		{
			_catalogueCS = value;
		}

		public function get catalogueCS():ICatalogue
		{
			return _catalogueCS;
		}

		public function set equipement(value:EquipementCatalogueVO):void
		{
			_equipement = value;
		}

		public function get equipement():EquipementCatalogueVO
		{
			return _equipement;
		}
		
	}
}