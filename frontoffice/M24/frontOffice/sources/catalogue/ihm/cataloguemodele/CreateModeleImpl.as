package catalogue.ihm.cataloguemodele
{
	import mx.resources.ResourceManager;
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.profil.GestionProfil;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.vo.RevendeurVO;
	import composants.util.ConsoviewAlert;
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	
	[Bindable]	
	public class CreateModeleImpl extends TitleWindow
	{
		
		public var globalService : GlobalCatalogueService = new GlobalCatalogueService();
		public var idOp : int = -1 ; 
		public var gestionProfil : GestionProfil;
		public var lesDistrib : ArrayCollection;
		public var idClient : int;
		public var idDistrib : int = -1;	
		public var boxRev : VBox;
		public var idModele:int;
		public var libelle:String;
		public var txtLibModele : TextInput; 
		public var comboRevendeur : ComboBox;       
		
		public function CreateModeleImpl()
		{
			
		}
		public function close():void
		{
			PopUpManager.removePopUp(this);
		}
		public function creationCompleteHandler():void 
		{
			
			if(idDistrib>0)
			{
				setCurrentState('editState');
			}
			
			// Function a revoir après recette
			if(gestionProfil)
			{
				if((gestionProfil.CURRENT_PROFIL == GestionProfil.CONSOTEL_PROFIL) && idOp!=-1)
				{
					boxRev.visible = true;
					boxRev.includeInLayout = true;
				}
				else
				{
					if((gestionProfil.CURRENT_PROFIL == GestionProfil.DISTRIBUTEUR_PROFIL))
					{
						idDistrib = gestionProfil.ID_CURRENT_PROFIL;
					}
				}
			}
			else
			{
				boxRev.visible = false;
				boxRev.includeInLayout = false;
			}
			}
		public function fermer():void
		{
			PopUpManager.removePopUp(this);
		}
		public function valider():void
		{
			if(txtLibModele.text.length!=0)
			{
				if(idModele>0)
				{
					globalService.addEventListener(CatalogueEvent.EDIT_MODELE_COMPLETE,createModeleHandler);
					globalService.editeCatalogueModele(idModele,txtLibModele.text);
				}
				else
				{
					if(idDistrib>0)
					{
						globalService.addEventListener(CatalogueEvent.CREATE_MODELE_COMPLETE,createModeleHandler);
						globalService.createCatalogueModele(idDistrib,txtLibModele.text,idClient,idOp);
					}
					else
					{
						Alert.show(ResourceManager.getInstance().getString('M24', 'Le_distributeur_est_obligatoire'));
					}
				}
			}
			else
			{
				txtLibModele.errorString = ResourceManager.getInstance().getString('M24', 'Le_libell__est_obligatoire_');
			}
		}
		public function createModeleHandler(evt : CatalogueEvent):void
		{
			
			if(idModele>0)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Mod_le__dit__avec_succ_s'));
				dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA)); 
			}
			else
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Mod_le_cr___avec_succ_s')); 
			}
			
			PopUpManager.removePopUp(this);
		}
		
		public function comboRevendeurHandler():void
		{
			idDistrib = (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR;
		}
		
		
		
	}
}