package catalogue.ihm.catalogueperso
{
	import mx.resources.ResourceManager;
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.ihm.cataloguemodele.CreateModeleIHM;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.vo.ModeleCatalogueVO;
		import composants.util.ConsoviewAlert;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;		
	
	[Bindable]
	public class ListeModeleImpl extends TitleWindow
	{
		public var idModeleSelected : int;
		public var libelleModeleSelected : String;
		public var idDistrib : int;
		public var globalService : GlobalCatalogueService = new GlobalCatalogueService();
		public var typeAbo : Boolean = false;
		public var dgModele : DataGrid;

		public function ListeModeleImpl()
		{
		}
		public function close():void
		{
			PopUpManager.removePopUp(this);
		}
		public function creationCompleteHandler():void
		{
			globalService.getModele(idDistrib,typeAbo);
		}
		public function fermer():void
		{
			idModeleSelected = -1;
			dispatchEvent(new CatalogueEvent(CatalogueEvent.SELECTE_MODELE_COMPLETE));
		}
		public function valider():void
		{
			if(dgModele.selectedItem)
			{
				idModeleSelected = (dgModele.selectedItem as ModeleCatalogueVO).ID_MODELE
				libelleModeleSelected = (dgModele.selectedItem as ModeleCatalogueVO).LIBELLE_MODELE;
				dispatchEvent(new CatalogueEvent(CatalogueEvent.SELECTE_MODELE_COMPLETE));
			}
			else
			{
				idModeleSelected = -1;
				libelleModeleSelected ="";
				Alert.show(ResourceManager.getInstance().getString('M24', 'Aucun_mod_le_s_lectionn_'));
			}
		}
		public function modifier(data : ModeleCatalogueVO):void
		{
			var pop : CreateModeleIHM = new CreateModeleIHM();
			pop.idModele = data.ID_MODELE;
			pop.libelle = data.LIBELLE_MODELE;
			pop.addEventListener(PopUpCatalogueEvent.REFRESH_PARENT_DATA,edit_complete_handler);
			PopUpManager.addPopUp(pop,this,true);
			PopUpManager.centerPopUp(pop);
		}
		private function edit_complete_handler(evt : PopUpCatalogueEvent):void
		{
			globalService.getModele(idDistrib,typeAbo);
		}
		public function supprimer(data : ModeleCatalogueVO):void
		{
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M24', '_tes_vous_s_r_de_vouloir_supprimer_le_mo')+data.LIBELLE_MODELE+ResourceManager.getInstance().getString('M24', '__'),ResourceManager.getInstance().getString('M24', 'Confirmation'),supprimerBtnValiderCloseEvent);
		}
		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				globalService.addEventListener(CatalogueEvent.REMOVE_MODELE_COMPLETE,removeEqCompleteHandler);
				globalService.removeCatalogueModele((dgModele.selectedItem as ModeleCatalogueVO).ID_MODELE);
			}
		}
		private function removeEqCompleteHandler(evt : CatalogueEvent):void
		{
			
			globalService.removeEventListener(CatalogueEvent.REMOVE_MODELE_COMPLETE,removeEqCompleteHandler);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Le_mod_le_a_bien__t__supprim__'));
			globalService.getModele(idDistrib,typeAbo);
		}
	
		
	}
}