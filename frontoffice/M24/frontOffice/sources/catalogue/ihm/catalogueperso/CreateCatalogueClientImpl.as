package catalogue.ihm.catalogueperso
{
	import mx.resources.ResourceManager;
	import catalogue.event.CatalogueEvent;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.vo.ClientCatalogueVO;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.RadioButton;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class CreateCatalogueClientImpl extends TitleWindow
	{
		public var pop : ListeModeleIHM;
		public var idDistributeur : int;
		public var globalCatalogueService : GlobalCatalogueService = new GlobalCatalogueService(); 
		public var radioModele : RadioButton;
		public var radioStandard : RadioButton;
		public var comboCli : ComboBox;
		public var idModele : int = -1;
		public var libelleModele : String = "";
		
		public function CreateCatalogueClientImpl()
		{
			this.visible = false; // Attente de vérifier si le distributeur à au moins 1 client	
		}
		public function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		public function chargerHandler():void
		{
			pop = new ListeModeleIHM();
			pop.idDistrib = idDistributeur;
			pop.addEventListener(CatalogueEvent.SELECTE_MODELE_COMPLETE,selectModeleCompleteHandler);
			PopUpManager.addPopUp(pop,this,true);	
			PopUpManager.centerPopUp(pop);	
		}
		public function radioStandardHandler():void
		{
			if(radioStandard.selected){
				idModele = -1;
				libelleModele = "";
			}
		}
		private function selectModeleCompleteHandler(evt : CatalogueEvent):void
		{
			if(pop.idModeleSelected!=-1)
			{
				libelleModele = pop.libelleModeleSelected;
				idModele = pop.idModeleSelected;
			}
			else
			{
				libelleModele = "";
				idModele = -1;
				radioStandard.selected = true;
			}
			PopUpManager.removePopUp(pop);
		}
		public function creationCompleteHandler():void
		{
			globalCatalogueService.addEventListener(CatalogueEvent.LISTE_CLIENT_DISTRIBUTEUR,listeClientHandler);
			globalCatalogueService.getClientOfDistributeur(idDistributeur);
		}
		public function validerHandler():void
		{
			if(comboCli.selectedIndex != -1){
				globalCatalogueService.addEventListener(CatalogueEvent.CREATE_CATALOGUE_CLIENT_COMPLETE,createCatalogueClientCompleteHandler);
				if(radioModele.selected)
				{
					globalCatalogueService.createCatalogueClient(idDistributeur,(comboCli.selectedItem as ClientCatalogueVO).ID_CLIENT,idModele);
				}
				else if(radioStandard.selected)
				{
					globalCatalogueService.createCatalogueClient(idDistributeur,(comboCli.selectedItem as ClientCatalogueVO).ID_CLIENT);
				}
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Un_client_doit__tre_s_lectionn_'),ResourceManager.getInstance().getString('M24', 'Impossible_de_cr_er_le_catalogue'));
			}
		}
		private function createCatalogueClientCompleteHandler(evt : CatalogueEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Catalogue_cr___avec_succ_s'));
			PopUpManager.removePopUp(this);
		}
		private function listeClientHandler(evt : CatalogueEvent):void
		{
			if(!globalCatalogueService.listeClient.length>0)
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Ce_distributeur_n_a_pas_de_client'),ResourceManager.getInstance().getString('M24', 'Impossible_de_cr_er_un_catalogue_personn'));
				PopUpManager.removePopUp(this);
			}
			else
			{
				this.visible = true;
			}
		}
		
	}
}