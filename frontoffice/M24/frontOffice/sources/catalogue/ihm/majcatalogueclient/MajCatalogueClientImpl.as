package catalogue.ihm.majcatalogueclient
{
	import mx.resources.ResourceManager;
	import catalogue.event.CatalogueEvent;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.service.ImportService;
	import catalogue.vo.ClientCatalogueVO;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	[Bindable]
	public class MajCatalogueClientImpl extends TitleWindow
	{
		public var idDistributeur : int;
		public var globalCatalogueService : GlobalCatalogueService = new GlobalCatalogueService(); 
		public var importService : ImportService = new ImportService();
		public var listeEquipement : ArrayCollection;
		public var boolPrevenir : Boolean;
		public var boolAdd : Boolean ;
	
		
		public function MajCatalogueClientImpl()
		{
			visible = false
		}
		public function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		public function selectAllcbPrevenir(bool : Boolean):void
		{
			for(var i :int = 0; i <globalCatalogueService.listeClient.length;i++)
			{
				(globalCatalogueService.listeClient.getItemAt(i) as ClientCatalogueVO).boolPrevenir = bool;
			}
			globalCatalogueService.listeClient.refresh();
		}
		public function validerHandler():void
		{			
			var listeClientNofifDispo : ArrayCollection = new ArrayCollection();
			var listeClientNofifAdd : ArrayCollection = new ArrayCollection();
			
			for(var i :int = 0; i <globalCatalogueService.listeClient.length;i++)
			{
				//Si prevenir sans ajouter
				if((globalCatalogueService.listeClient.getItemAt(i) as ClientCatalogueVO).boolPrevenir 	&&	!(globalCatalogueService.listeClient.getItemAt(i) as ClientCatalogueVO).boolAddInCatClient)
				{
					listeClientNofifDispo.addItem(globalCatalogueService.listeClient.getItemAt(i) as ClientCatalogueVO);
				}
				//Si prevenir et ajouter 
				if((globalCatalogueService.listeClient.getItemAt(i) as ClientCatalogueVO).boolPrevenir 	&&	(globalCatalogueService.listeClient.getItemAt(i) as ClientCatalogueVO).boolAddInCatClient)
				{
					listeClientNofifAdd.addItem(globalCatalogueService.listeClient.getItemAt(i) as ClientCatalogueVO);
					//addEquip
					importService.importXEquipInCatClient((globalCatalogueService.listeClient.getItemAt(i) as ClientCatalogueVO).ID_CLIENT,listeEquipement);
				}
				
				//si ajouter sans prévenir
				if((globalCatalogueService.listeClient.getItemAt(i) as ClientCatalogueVO).boolAddInCatClient)
				{	
					//addEquip
					importService.importXEquipInCatClient((globalCatalogueService.listeClient.getItemAt(i) as ClientCatalogueVO).ID_CLIENT,listeEquipement);
				}
			}
			/**
			 * Utilisation de ObjectUtil.copy pour fixer un bug dans CF:
			 * CF déclenche une erreur si un objet est typé xxxVO, il ne supporte que les objet de type Objet.
			 * */
			globalCatalogueService.sendMailNotificationDispo(ObjectUtil.copy(listeEquipement) as ArrayCollection,listeClientNofifDispo);
			globalCatalogueService.sendMailNotificationAdd(ObjectUtil.copy(listeEquipement) as ArrayCollection,listeClientNofifAdd);
			ConsoviewAlert.afficherOKImage('Opération réalisée avec succès');
			PopUpManager.removePopUp(this);
			
		}
		public function selectAllcbAdd(bool : Boolean):void
		{
			for(var i :int = 0; i <globalCatalogueService.listeClient.length;i++)
			{
				(globalCatalogueService.listeClient.getItemAt(i) as ClientCatalogueVO).boolAddInCatClient = bool;
			}
				globalCatalogueService.listeClient.refresh();
		}
		public function creationCompleteHandler():void
		{
			globalCatalogueService.addEventListener(CatalogueEvent.LISTE_CLIENT_DISTRIBUTEUR,listeClientHandler);
			globalCatalogueService.getClientOfDistributeur(idDistributeur);
		}
	
		private function createCatalogueClientCompleteHandler(evt : CatalogueEvent):void
		{
			
			PopUpManager.removePopUp(this);
		}
		private function listeClientHandler(evt : CatalogueEvent):void
		{
			if(!globalCatalogueService.listeClient.length>0)
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Ce_distributeur_n_a_pas_de_client'),ResourceManager.getInstance().getString('M24', 'Impossible_de_mettre___jour_les_catalogu'));
				PopUpManager.removePopUp(this);
			}
			else
			{
				visible = true
			}
		}
		public function onItemChanged(item : Object):void
		{
			
		}
			
			
		
	}
}