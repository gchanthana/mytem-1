package catalogue.ihm.importabo
{
	import mx.resources.ResourceManager;
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.service.ImportService;
	import catalogue.vo.OperateurVO;
	import catalogue.vo.ParametreSearchCatalogueVO;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.managers.PopUpManager;
	import searchpaginatedatagrid.SearchPaginateDatagrid;
		import composants.util.ConsoviewAlert;
	import paginatedatagrid.PaginateDatagrid;
	import paginatedatagrid.event.PaginateDatagridEvent;
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	import searchpaginatedatagrid.ParametresRechercheVO;
	import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;
	
	[Bindable]
	public class ImportSourceIsDistribAboImpl extends TitleWindow
	{
		//Public UI componant
		public var comboOP : ComboBox;
		public var idClient : int;
		public var comboExport : ComboBox;
		public var mySearchPaginateDatagrid : SearchPaginateDatagrid;
		public var myPaginatedatagrid : PaginateDatagrid;
		public var catService:GlobalCatalogueService = new GlobalCatalogueService();
		
		public var importService : ImportService = new ImportService();

		public const NBITEMPARPAGE : int = 30;
		
		public function ImportSourceIsDistribAboImpl()
		{
			
		}
		/**
		 * 
		 * Handler : SearchPaginateDatagridEvent.SEARCH
		 */
		public function onSearchHandler(event:SearchPaginateDatagridEvent):void
		{
			rechercher(event.parametresRechercheVO);
		}
		/**
		*
		* Handler : PaginateDatagridEvent.PAGE_CHANGE
		* Componante 
		* Procédure getData : avec Segment (page cible) + nombre d'items par page
		* Limite haute = segement * nombre d'items par page
		* 
		* */
		public function onIntervalleChangeHandler(evt : PaginateDatagridEvent):void
		{
			rechercher(mySearchPaginateDatagrid.parametresRechercheVO,myPaginatedatagrid.currentIntervalle);
		}
		public function fermer(event : Event=null):void
		{
			PopUpManager.removePopUp(this);
		}
		 public  function rechercher(parametresRechercheVO : ParametresRechercheVO, itemIntervalleVO : ItemIntervalleVO = null):void
		{
			if(comboOP.selectedItem)
			{
				if(!itemIntervalleVO) // Si c'est une nouvelle recherche
				{
					myPaginatedatagrid.refreshPaginateDatagrid(true);
					itemIntervalleVO = new ItemIntervalleVO();
					itemIntervalleVO.indexDepart = 0;
					itemIntervalleVO.tailleIntervalle = NBITEMPARPAGE;
				}
				//Ajout des 3 paramêtres spécifique au catalogue :
				var parametreSearchCatalogueVO : ParametreSearchCatalogueVO = new ParametreSearchCatalogueVO();
				parametreSearchCatalogueVO.IDOP =(comboOP.selectedItem as OperateurVO).ID_OP;
				parametreSearchCatalogueVO.IDCLIENT =idClient;
				parametreSearchCatalogueVO.ORDER_BY=parametresRechercheVO.ORDER_BY;
				parametreSearchCatalogueVO.SEARCH_TEXT=parametresRechercheVO.SEARCH_TEXT;
				importService.rechercheAbonnementCatalogueImport(parametreSearchCatalogueVO,itemIntervalleVO);
				
			}
			else
			{	
				Alert.show(ResourceManager.getInstance().getString('M24', '_Un_op_rateur_doit__tre_selectionn__'));
			}
		}
		
		public function initData():void
		{
			catService.listAllOpClient(idClient);
			
			
			
		}
		public function comboConstructeurHandler():void
		{
			
		}
	
		private function addEquipementCompleteHandler(evt : PopUpCatalogueEvent):void
		{
			if(mySearchPaginateDatagrid.parametresRechercheVO && comboOP.selectedItem){
				rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
			}
		}
		public function btValiderContinuerHandler(boolContinuerValue : Boolean):void
		{
			
			if(myPaginatedatagrid.selectedItems.length>0)
			{
				importService.addEventListener(CatalogueEvent.ADD_ABO_COMPLETE,importAboCompleteHandler)
				importService.importXAboInCatClient(idClient,myPaginatedatagrid.selectedItems);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Aucun__quipement_s_lectionn_'));
			}
		}
		public function importAboCompleteHandler(evt : CatalogueEvent):void
		{
			dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Abonnements_import_s_'));
			//rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
			/* if(!boolContinuer)
			{ */
				fermer();
			//}
		}
		
	
		
		
		
	}
}