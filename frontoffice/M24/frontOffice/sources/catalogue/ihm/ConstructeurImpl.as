package catalogue.ihm
{
	import catalogue.AbstractCatalogue;
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.ihm.fiche.FicheEquipementIHM;
	import catalogue.ihm.parametre.AddEquipementIHM;
	import catalogue.ihm.popup.PopupFilterIHM;
	import catalogue.vo.ClasseVO;
	import catalogue.vo.ConstructeurVO;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.ParametreSearchCatalogueVO;
	import catalogue.vo.SegmentVO;
	import catalogue.vo.TypeEquipementVO;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.events.CloseEvent;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import paginatedatagrid.columns.event.StandardColonneEvent;
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	import searchpaginatedatagrid.ParametresRechercheVO;
	
	[Bindable]
	public class ConstructeurImpl extends AbstractCatalogue
	{
		/**
		 * 
		 */		
		//Public UI componant
		
		public var comboConstructeur : ComboBox;
		public var comboSegment		 : ComboBox; 	
		
		public var popAddEquipement : AddEquipementIHM;
		private var _popDetailEquipement : FicheEquipementIHM;
		private var _popUpFilter		:PopupFilterIHM;

		
		public const NBITEMPARPAGE : int = 30;
		public var listFilter:ArrayCollection;
		public var _itemSelected:Boolean = false;
		
		public function ConstructeurImpl()
		{
			
		}
		override public function rechercher(parametresRechercheVO : ParametresRechercheVO, itemIntervalleVO : ItemIntervalleVO = null):void
		{
			
			if(comboConstructeur.selectedItem){
				//Selectionne 'tous'
				if(comboClasse.selectedIndex==-1)
				{
					comboClasse.selectedIndex = 0;
				}
				comboTypeHandler();
							
				if(!itemIntervalleVO) // Si c'est une nouvelle recherche
				{
					myPaginatedatagrid.refreshPaginateDatagrid(true);
					itemIntervalleVO = new ItemIntervalleVO();
					itemIntervalleVO.indexDepart = 0;
					itemIntervalleVO.tailleIntervalle = NBITEMPARPAGE;
				}
				
				//Ajout des 3 paramêtres spécifique au catalogue :
				var parametreSearchCatalogueVO : ParametreSearchCatalogueVO = new ParametreSearchCatalogueVO();
				parametreSearchCatalogueVO.IDCLASSE =(comboClasse.selectedItem as ClasseVO).ID_CLASSE;
				parametreSearchCatalogueVO.IDCONSTRUCTEUR =(comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR;
				parametreSearchCatalogueVO.IDTYPEEQUIPEMENT =(comboType.selectedItem as TypeEquipementVO).ID_TYPE;
				parametreSearchCatalogueVO.ORDER_BY=parametresRechercheVO.ORDER_BY;
				parametreSearchCatalogueVO.SEARCH_TEXT=parametresRechercheVO.SEARCH_TEXT;
				parametreSearchCatalogueVO.GROUPE_INDEX =groupeIndex;
				parametreSearchCatalogueVO.IDSEGMENT = (comboSegment.selectedItem as SegmentVO).IDSEGMENT;
				catalogueCS.rechercheListeDataCatalogue(parametreSearchCatalogueVO,itemIntervalleVO);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Un_constructeur_doit__tre_s_lectionn__'),ResourceManager.getInstance().getString('M24', 'Attention'));	
			}
		}
		
		public function addEquipementHandler(evt : MouseEvent):void
		{
			popAddEquipement = new AddEquipementIHM();
			addPop();
		}
		public function initData():void
		{
			catalogueCS.rechercheContructeur(true);
			catalogueCS.rechercheClasseEquipement(true);
			catalogueCS.addEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE,listeClasseEquipementCompleteHandler);
			//Par defaut, le tri se fait sur le modèle:			
			mySearchPaginateDatagrid.comboRecherche.selectedIndex = 5; // Sélectionne modèle dans la combo Clé de recherche
			mySearchPaginateDatagrid.comboTrieColonne.selectedIndex = 5; // Sélectionne modèle dans la combo ORDERBY
		}
		
		protected function listeClasseEquipementCompleteHandler(evt:CatalogueEvent):void
		{	
			if(catalogueCS.hasEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE))
			{
				catalogueCS.removeEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE,listeClasseEquipementCompleteHandler);	
			}
			
			comboClasseHandler();
		}
	
		public function comboConstructeurHandler():void
		{
			majSelectConstructeur();
		}
		
		public function btImgChooseConstructeur_clickHandler(e:Event):void
		{
			var listconstucteurs:ArrayCollection = comboConstructeur.dataProvider as ArrayCollection;
			this.refreshlistFilter(listconstucteurs)
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.listeFilter = listFilter;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_constructeur');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', getItemFilterHandler);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		private function majSelectConstructeur():void
		{
			for (var i:int=0; i<catalogueCS.listeConstructeur.length;i++)
			{
				(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =false;
				if ((catalogueCS.listeConstructeur[i] as ConstructeurVO).ID_CONSTRUCTEUR == (comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR)
				{
					(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =true;
					_itemSelected = true;
				}
			}
		}
		
		protected function getItemFilterHandler(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', getItemFilterHandler);
			refreshComboConstructeur();
		}
		
		private function refreshComboConstructeur():void
		{
			var k:int;
			k=_popUpFilter.getItemCurrentID();
			for (var i:int=0; i<catalogueCS.listeConstructeur.length;i++)
			{
				(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =false;
				if ((catalogueCS.listeConstructeur[i] as ConstructeurVO).ID_CONSTRUCTEUR == _popUpFilter.getItemCurrentID())
				{
					(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =true;
					comboConstructeur.selectedIndex = i;
				}
			}
		}
		private function refreshlistFilter(listdata:ArrayCollection):void
		{
			listFilter =new ArrayCollection();
			for (var i:int=0; i<listdata.length;i++)
			{
				var item:Object = new Object();
				item.SELECTED = (listdata.getItemAt(i) as ConstructeurVO).SELECTED;
				item.IDITEM = (listdata.getItemAt(i) as ConstructeurVO).ID_CONSTRUCTEUR;
				item.LIBELLE_ITEM = (listdata.getItemAt(i) as ConstructeurVO).LIBELLE_CONSTRUCTEUR;
				listFilter.addItem(item);
			}
		}
		
		private function addPop():void
		{
			popAddEquipement.addEventListener(PopUpCatalogueEvent.REFRESH_PARENT_DATA,addEquipementCompleteHandler);
			PopUpManager.addPopUp(popAddEquipement,this,true);
			PopUpManager.centerPopUp(popAddEquipement);
		}
		private function addEquipementCompleteHandler(evt : PopUpCatalogueEvent):void
		{
			if(mySearchPaginateDatagrid.parametresRechercheVO && comboConstructeur.selectedItem){
				rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
			}
		}
		override protected function paginateItemEditHandler(evt : StandardColonneEvent):void
		{
			popAddEquipement = new AddEquipementIHM();
			popAddEquipement.equipement = evt.item as EquipementCatalogueVO;
			addPop();
		}
		
		public function supprimer(data : EquipementCatalogueVO):void
		{
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M24', '_tes_vous_s_r_de_vouloir_supprimer_l__qu')+data.MODELE+ResourceManager.getInstance().getString('M24', '__'),ResourceManager.getInstance().getString('M24', 'Confirmation'),supprimerBtnValiderCloseEvent);
			
		}
		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				catalogueCS.addEventListener(CatalogueEvent.REMOVE_EQUIPEMENT_COMPLETE,removeEqCompleteHandler);
				catalogueCS.removeEquipement(myPaginatedatagrid.selectedItem as EquipementCatalogueVO);
			}
		}
		private function removeEqCompleteHandler(evt : CatalogueEvent):void
		{
			
			catalogueCS.removeEventListener(CatalogueEvent.REMOVE_EQUIPEMENT_COMPLETE,removeEqCompleteHandler);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'L__quipement_a_bien__t__supprim__'));
			if(mySearchPaginateDatagrid.parametresRechercheVO && comboConstructeur.selectedItem){
				rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
			}
		}
		
	}
}