package catalogue.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import catalogue.AbstractCatalogue;
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.ihm.cataloguemodele.CreateModeleIHM;
	import catalogue.ihm.importabo.ImportSourceIsDistribAboIHM;
	import catalogue.ihm.parametre.UpdatePlanIHM;
	import catalogue.ihm.parametre.UpdateTarifAbonnementIHM;
	import catalogue.ihm.popup.PopupFilterIHM;
	import catalogue.ihm.typecommande.AboOptionsDefautIHM;
	import catalogue.ihm.typecommande.ListeTypeCommandeAboIHM;
	import catalogue.profil.GestionProfil;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.util.CatalogueFormatteur;
	import catalogue.util.Currency;
	import catalogue.util.DataGridDataExporter;
	import catalogue.vo.AbonnementVO;
	import catalogue.vo.ClientCatalogueVO;
	import catalogue.vo.OperateurVO;
	import catalogue.vo.ParametreSearchCatalogueVO;
	import catalogue.vo.TypeCommandeVO;
	
	import composants.util.ConsoviewAlert;
	
	import paginatedatagrid.columns.event.StandardColonneEvent;
	import paginatedatagrid.event.PaginateDatagridEvent;
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	import searchpaginatedatagrid.ParametresRechercheVO;

	[Bindable]
	public class ClientAbonnementImpl extends AbstractCatalogue 
	{
		public const NBITEMPARPAGE 			:int = 30;
		
		public var labelClient 				:Label;
		public var labelRevendeur 			:Label;
		public var btCreateCatModele 		:Button;
		public var comboClient 				:ComboBox;
		public var comboOperateur 			:ComboBox;
		public var comboTypeCMD 			:ComboBox;
		public var chxTypeCommandeDefault	:CheckBox;
		
		private var popImport 				:ImportSourceIsDistribAboIHM;
		private var popTypeCMD 				:ListeTypeCommandeAboIHM;
		private var popTypeCMDABO			:AboOptionsDefautIHM;
		private var pop 					:CreateModeleIHM; 
		private var _popUpFilter			:PopupFilterIHM;
		public var listFilter				:ArrayCollection;
		public var currenciesAreDifferent	:Boolean = false;
		
		public var popupUpdateTarif			:UpdateTarifAbonnementIHM;
		
		public var popupUpdatePlan			:UpdatePlanIHM;
		
		public var globalCatalogueService 	:GlobalCatalogueService = new GlobalCatalogueService(); 
		
		public function ClientAbonnementImpl()
		{
			globalCatalogueService.addEventListener(CatalogueEvent.LISTE_OPERATEUR_COMPLETE,dataOpChange);
		}

		public function dataOpChange(evt : CatalogueEvent):void
		{
			if(globalCatalogueService.listeOperateurClient.length==1)
				comboOperateur.selectedIndex=0;
		}
		
		public function btCreateModeleHandler(event:MouseEvent):void
		{
			pop  = new CreateModeleIHM();
			pop.gestionProfil = gestionProfil;
			pop.idClient = (comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT;
			pop.idOp = (comboOperateur.selectedItem as OperateurVO).ID_OP; //tague le modèle de type cat Abo
			
			if(gestionProfil.CURRENT_PROFIL == GestionProfil.CONSOTEL_PROFIL)
			{
				catalogueCS.addEventListener(CatalogueEvent.LISTE_REVENDEUR_EQUIPEMENT_COMPLETE,listeRevendeurComplete);
				catalogueCS.rechercheRevendeur((comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT);
			}
			else
				showPopUp();
		}
		
		override protected function paginateItemEditHandler(evt : StandardColonneEvent):void
		{
			var plan : AbonnementVO = evt.item as AbonnementVO;
			plan.LIBELLE_OP =  (comboOperateur.selectedItem as OperateurVO).LIBELLE_OP;
			plan.IDOPERATEUR = (comboOperateur.selectedItem as OperateurVO).ID_OP;
			plan.IDCLIENT = (comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT;
			
			popupUpdateTarif = new UpdateTarifAbonnementIHM();
			popupUpdateTarif.abonnement = plan;
			popupUpdateTarif.libelleOP = (comboOperateur.selectedItem as OperateurVO).LIBELLE_OP;
			popupUpdateTarif.idRacineClient = groupeIndex // a vérifier
			popupUpdateTarif.idRacineComboSelected = (comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT;
			
			/////
			
			if(popupUpdatePlan != null)
			{
				 popupUpdatePlan = null;
			}
			
			popupUpdatePlan = new UpdatePlanIHM();
			popupUpdatePlan.plan = plan;
			addPop(popupUpdatePlan);
		}
		
		private function addPop(popup:TitleWindow):void
		{
			popupUpdateTarif.addEventListener(PopUpCatalogueEvent.REFRESH_PARENT_DATA,popupUpdateTarifCompleteHandler);
			PopUpManager.addPopUp(popup,this,true);
			PopUpManager.centerPopUp(popup);
		}
		
		private function popupUpdateTarifCompleteHandler(evt : PopUpCatalogueEvent):void
		{
			if(mySearchPaginateDatagrid.parametresRechercheVO && comboOperateur.selectedItem && comboClient.selectedItem)
				rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
		}
		
		public function comboNiveauHandler(evt : Event):void
		{
			var combo : ComboBox = evt.target as ComboBox;
			
			if(combo.selectedItem)
				catalogueCS.updateNiveauAbo((myPaginatedatagrid.selectedItem as AbonnementVO).IDABO,(comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT,Number(combo.selectedItem.toString()));
		}
		
		private function listeRevendeurComplete(evt : CatalogueEvent):void
		{
			pop.lesDistrib = catalogueCS.listeRevendeur;
			showPopUp();
		}
		
		public function showPopUp():void
		{
			PopUpManager.addPopUp(pop,this,true);
			PopUpManager.centerPopUp(pop);
		}
		
		public function comboClientHandler():void
		{
			if(comboClient.selectedItem)
			{
				globalCatalogueService.listOpClient((comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT);
				globalCatalogueService.getTypeCommandeOfClient((comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT);
			}
			
			comboRequiredHandler();
			majSelectClient();
		}
		public function comboOperateurHandler():void
		{
			comboRequiredHandler();
			majSelectOperateur();
			CatalogueFormatteur.getInstance().operatorCurrency = new Currency( comboOperateur.selectedItem.DEVISE );
			if (comboOperateur.selectedItem.DEVISE != resourceManager.getString('M24', '_4635'))
				currenciesAreDifferent = true;
		}
		
		public function comboRequiredHandler():void
		{
			if(comboClient.selectedItem && comboOperateur.selectedItem)
				btCreateCatModele.enabled = true;
			else
				btCreateCatModele.enabled = false;
		}
		
		public function initData():void
		{
			//Listerner
			catalogueCS.addEventListener(CatalogueEvent.LISTE_CLIENT_DISTRIBUTEUR,comboClientDataChangeHandler);
			thisComponentIsComplete = true;
			updateProfil();
		}
		//popup loop client
		
		public function btImgChooseClient_clickHandler(e:Event):void
		{
			var listClient:ArrayCollection = catalogueCS.listeClient as ArrayCollection;
			this.refreshlistFilter(listClient,1);
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.listeFilter = listFilter;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_client');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', getItemFilterHandler);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		protected function getItemFilterHandler(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', getItemFilterHandler);
			refreshComboClient();
		}
		
		private function refreshComboClient():void
		{
			var k:int;
			k=_popUpFilter.getItemCurrentID();
			for (var i:int=0; i<catalogueCS.listeClient.length;i++)
			{
				(catalogueCS.listeClient[i] as ClientCatalogueVO).SELECTED =false;
				if ((catalogueCS.listeClient[i] as ClientCatalogueVO).ID_CLIENT == _popUpFilter.getItemCurrentID())
				{
					(catalogueCS.listeClient[i] as ClientCatalogueVO).SELECTED =true;
					comboClient.selectedIndex = i;					
				}
			}
			comboClientHandler();
		}
		
		private function majSelectClient():void
		{
			for (var i:int=0; i<catalogueCS.listeClient.length;i++)
			{
				(catalogueCS.listeClient[i] as ClientCatalogueVO).SELECTED =false;
				if ((catalogueCS.listeClient[i] as ClientCatalogueVO).ID_CLIENT == (comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT)
				{
					(catalogueCS.listeClient[i] as ClientCatalogueVO).SELECTED =true;
				}
			}
		}
		
		//popup loop operateur
		
		public function btImgChooseOperateur_clickHandler(e:Event):void
		{
			var listOp:ArrayCollection = globalCatalogueService.listeOperateurClient as ArrayCollection;
			this.refreshlistFilter(listOp,2);
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.listeFilter = listFilter;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_operateur');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', getItemFilterHandler1);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		protected function getItemFilterHandler1(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', getItemFilterHandler1);
			refreshComboOperateur();
		}
		
		private function refreshComboOperateur():void
		{
			var k:int;
			k=_popUpFilter.getItemCurrentID();
			for (var i:int=0; i<globalCatalogueService.listeOperateurClient.length;i++)
			{
				(globalCatalogueService.listeOperateurClient[i] as OperateurVO).SELECTED =false;
				if ((globalCatalogueService.listeOperateurClient[i] as OperateurVO).ID_OP == _popUpFilter.getItemCurrentID())
				{
					(globalCatalogueService.listeOperateurClient[i] as OperateurVO).SELECTED =true;
					comboOperateur.selectedIndex = i;					
				}
			}
			comboOperateurHandler();
		}
		
		private function majSelectOperateur():void
		{
			for (var i:int=0; i<globalCatalogueService.listeOperateurClient.length;i++)
			{
				(globalCatalogueService.listeOperateurClient[i] as OperateurVO).SELECTED =false;
				if ((globalCatalogueService.listeOperateurClient[i] as OperateurVO).ID_OP == (comboOperateur.selectedItem as OperateurVO).ID_OP)
				{
					(globalCatalogueService.listeOperateurClient[i] as OperateurVO).SELECTED =true;
					comboOperateur.selectedIndex = i;					
				}
			}
		}
		
		private function refreshlistFilter(listdata:ArrayCollection, dataFilter:int):void
		{
			listFilter =new ArrayCollection();
			switch (dataFilter)
			{
				case 1:
					for (var j:int=0; j<listdata.length;j++)
					{
						var item:Object = new Object();
						item.SELECTED = (listdata.getItemAt(j) as ClientCatalogueVO).SELECTED;
						item.IDITEM = (listdata.getItemAt(j) as ClientCatalogueVO).ID_CLIENT;
						item.LIBELLE_ITEM = (listdata.getItemAt(j) as ClientCatalogueVO).LIBELLE_CLIENT;
						listFilter.addItem(item);
					}
					break;
				case 2:
					for (var j:int=0; j<listdata.length;j++)
					{
						var item:Object = new Object();
						item.SELECTED = (listdata.getItemAt(j) as OperateurVO).SELECTED;
						item.IDITEM = (listdata.getItemAt(j) as OperateurVO).ID_OP;
						item.LIBELLE_ITEM = (listdata.getItemAt(j) as OperateurVO).LIBELLE_OP;
						listFilter.addItem(item);
					}
					break;
				default:
			}
			
		}
		
		public function comboClientDataChangeHandler(evt : CatalogueEvent):void
		{
			if(comboClient.dataProvider)
			{
				if((comboClient.dataProvider as ArrayCollection).length==1)
				{
					comboClient.selectedIndex = 0;
					comboClientHandler();
				}
			}
		}
		override public function updateProfil():void
		{
			if(thisComponentIsComplete){
				switch (gestionProfil.CURRENT_PROFIL)
				{
					case (GestionProfil.CONSOTEL_PROFIL):
						comboClientVisible(true);
						catalogueCS.rechercheClient(-1);
					
					break;
					case (GestionProfil.DISTRIBUTEUR_PROFIL):
						comboClientVisible(true);
						catalogueCS.rechercheClient(gestionProfil.ID_CURRENT_PROFIL);
					break;
					case (GestionProfil.CLIENT_PROFIL):
						comboClientVisible(false);
						globalCatalogueService.listOpClient(gestionProfil.ID_CURRENT_PROFIL);
						globalCatalogueService.getTypeCommandeOfClient(gestionProfil.ID_CURRENT_PROFIL);
					break;
				}
			}
		}
				
		private function comboClientVisible(bool : Boolean):void
		{ 
			if(bool)
			{
				comboClient.selectedIndex = -1;
				comboClient.enabled = true;
				comboClient.visible = true;
				comboClient.includeInLayout = true;
				labelClient.text = "";
			}
			else
			{
				var cli : ClientCatalogueVO = new ClientCatalogueVO();
				cli.ID_CLIENT = gestionProfil.ID_CURRENT_PROFIL;
				cli.LIBELLE_CLIENT = gestionProfil.LIBELLE_CURRENT_PROFIL;
				catalogueCS.listeClient.removeAll();
				catalogueCS.listeClient.addItem(cli);
				comboClient.selectedIndex = 0;
				comboClient.enabled = false;
				comboClient.visible = false;
				comboClient.includeInLayout = false;
				labelClient.text = cli.LIBELLE_CLIENT;
			}
		} 

		public function importAboHandler(evt : MouseEvent):void
		{
			popImport = new ImportSourceIsDistribAboIHM;
			popImport.addEventListener(PopUpCatalogueEvent.REFRESH_PARENT_DATA,popImport_close_handler);
			popImport.idClient = (comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT;
			//popImport.listeOperateur = globalCatalogueService.listeOperateurClient;
			addPopImport();
		}
		private function popImport_close_handler(evt : PopUpCatalogueEvent):void
		{
			//Fixe bug de la liste des opérateur qui ne se met pas à jour
			if(comboClient.selectedItem)
				globalCatalogueService.listOpClient((comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT);
		}
		
		private function addPopImport():void
		{
			popImport.addEventListener(PopUpCatalogueEvent.REFRESH_PARENT_DATA,addAboCompleteHandler);
			PopUpManager.addPopUp(popImport,this,true);
			PopUpManager.centerPopUp(popImport);
		}
		
		private function addAboCompleteHandler(evt : PopUpCatalogueEvent):void
		{
			if(mySearchPaginateDatagrid.parametresRechercheVO && comboClient.selectedItem && comboClasse)
				rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
		}
		
		override public function rechercher(parametresRechercheVO : ParametresRechercheVO, itemIntervalleVO : ItemIntervalleVO = null):void
		{
				if(comboClient.selectedItem && comboOperateur.selectedItem)
				{
					if(!itemIntervalleVO) // Si c'est une nouvelle recherche
					{
						myPaginatedatagrid.refreshPaginateDatagrid(true);
						itemIntervalleVO = new ItemIntervalleVO();
						itemIntervalleVO.indexDepart = 0;
						itemIntervalleVO.tailleIntervalle = NBITEMPARPAGE;
					}
					
					if(!comboTypeCMD.selectedItem)
						comboTypeCMD.selectedIndex = 0;
					
					//Ajout des 3 paramêtres spécifique au catalogue :
					var parametreSearchCatalogueVO : ParametreSearchCatalogueVO = new ParametreSearchCatalogueVO();
						parametreSearchCatalogueVO.ORDER_BY=parametresRechercheVO.ORDER_BY;
						parametreSearchCatalogueVO.SEARCH_TEXT=parametresRechercheVO.SEARCH_TEXT;
						parametreSearchCatalogueVO.GROUPE_INDEX =groupeIndex;
						parametreSearchCatalogueVO.IDCLIENT = (comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT;
						parametreSearchCatalogueVO.IDOP =(comboOperateur.selectedItem as OperateurVO).ID_OP;
						parametreSearchCatalogueVO.IDTYPECMD =(comboTypeCMD.selectedItem as TypeCommandeVO).idTypeCmd;
						parametreSearchCatalogueVO.IDOP = (comboOperateur.selectedItem as OperateurVO).ID_OP;
						parametreSearchCatalogueVO.DEFINIDEFAUT = boolToInt(chxTypeCommandeDefault.selected);
					
					catalogueCS.rechercheAbonnementCatalogue(parametreSearchCatalogueVO,itemIntervalleVO);
				}
				else
					Alert.show(ResourceManager.getInstance().getString('M24', 'S_lectionnez_un_client_et_un_op_rateur'));
		}
		
		private function boolToInt(value:Boolean):int
		{
			var bool:int = 0;
			
			if(value)
				bool = 1;
			
			return bool;
				
		}
		
		public function supprimer(data : AbonnementVO):void
		{
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M24', '_tes_vous_s_r_de_vouloir_supprimer_l_abo')+data.LIBELLE+ResourceManager.getInstance().getString('M24', '__'),ResourceManager.getInstance().getString('M24', 'Confirmation'),supprimerBtnValiderCloseEvent);
		}
		
		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK)
			{
				catalogueCS.addEventListener(CatalogueEvent.REMOVE_ABO_COMPLETE,removeEqCompleteHandler); 
				catalogueCS.removeAbonnement((comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT,myPaginatedatagrid.selectedItem as AbonnementVO);
			}
		}
		private function removeEqCompleteHandler(evt : CatalogueEvent):void
		{
			catalogueCS.removeEventListener(CatalogueEvent.REMOVE_EQUIPEMENT_COMPLETE,removeEqCompleteHandler);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'L_abonnement_a_bien__t__supprim__'));
			
			if(mySearchPaginateDatagrid.parametresRechercheVO && comboClient.selectedItem && comboOperateur.selectedItem)
				rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
		}
				
		public function comboActionHandler(event : PaginateDatagridEvent):void
		{
			if(comboClient.selectedItem) 
			{
				if(myPaginatedatagrid.dataprovider.length>0)
					btExportHandler(event,(comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT);
				else
					Alert.show(ResourceManager.getInstance().getString('M24', '_Aucun__quipement_s_l_ctionn__'));
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M24', '_Un_client_doit__tre_selectionn__'));
		}
		
		override public function btExportHandler(evt : PaginateDatagridEvent,idClientTypeCMD:Number=-1):void
		{
			if(comboExport.selectedItem)
			{
				var isTypeCmd 	:Boolean = false;
				var dataExp 	:Object = new Object();
				
				if(comboExport.selectedIndex == 0)
				{
					dataExp = myPaginatedatagrid.selectedItems;
				}
				else if(comboExport.selectedIndex == 1)
				{
					dataExp = myPaginatedatagrid.dataprovider;
				}
				else if(comboExport.selectedIndex == 2)
				{
					popTypeCMD = new ListeTypeCommandeAboIHM()
					popTypeCMD.idClient = idClientTypeCMD;
					
					for(var i:int=0;i<myPaginatedatagrid.selectedItems.length;i++)
					{
						if(popTypeCMD.listeAboEquipement)
							popTypeCMD.listeAboEquipement = (myPaginatedatagrid.selectedItems.getItemAt(i) as AbonnementVO).IDABO + ','+ popTypeCMD.listeAboEquipement;
						else
							popTypeCMD.listeAboEquipement = (myPaginatedatagrid.selectedItems.getItemAt(i) as AbonnementVO).IDABO.toString();
					}
					
					popTypeCMD.listeAbo = myPaginatedatagrid.selectedItems;
					PopUpManager.addPopUp(popTypeCMD,this,true);
					PopUpManager.centerPopUp(popTypeCMD);
					isTypeCmd = true;
				}
				else if(comboExport.selectedIndex == 3)
				{
					popTypeCMDABO 				= new AboOptionsDefautIHM()
					popTypeCMDABO.idClient 		= idClientTypeCMD;
					popTypeCMDABO.groupeIndex 	= (comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT;
					
					var lenItems		:int = myPaginatedatagrid.selectedItems.length;
					var lastIDTHEMETYPE	:int = 0;
					var lastIDSEGMENT	:int = 0;
					var segement		:int = 0;
					var cptr			:int = 0;
					var boolError		:Boolean = false;
					var boolErrorAbo	:Boolean = false;
					var boolErrorSeg	:Boolean = false;
					
					if(lenItems > 0)
					{
						lastIDTHEMETYPE = (myPaginatedatagrid.selectedItems.getItemAt(0) as AbonnementVO).IDTHEMETYPE;
						lastIDSEGMENT 	= (myPaginatedatagrid.selectedItems.getItemAt(0) as AbonnementVO).SEGMENT;
					}
					else
						return;
					
					for(var j:int = 0;j < lenItems;j++)
					{
						if((myPaginatedatagrid.selectedItems.getItemAt(j) as AbonnementVO).IDTHEMETYPE == lastIDTHEMETYPE)
						{
							if((myPaginatedatagrid.selectedItems.getItemAt(j) as AbonnementVO).SEGMENT == lastIDSEGMENT)
							{
								cptr++;
								if(popTypeCMDABO.listeAboEquipement)
									popTypeCMDABO.listeAboEquipement = (myPaginatedatagrid.selectedItems.getItemAt(j) as AbonnementVO).IDABO + ","+ popTypeCMDABO.listeAboEquipement;
								else
									popTypeCMDABO.listeAboEquipement = (myPaginatedatagrid.selectedItems.getItemAt(j) as AbonnementVO).IDABO.toString();
							}
							else
							{
								boolErrorSeg = true;
								break;
							}
						}
						else
						{
							boolError = true;
							break;
						}	
					}
										
					if(!boolError && !boolErrorSeg)
					{
						if(lastIDTHEMETYPE == 1 && cptr != 1)
						{
							boolErrorAbo = true;
							Alert.show(ResourceManager.getInstance().getString('M24', 'Un_seul_abonnement_peut__tre_affect____u'),ResourceManager.getInstance().getString('M24', 'Consoview'));
						}
						
						if(!boolErrorAbo)
						{
							popTypeCMDABO.idthemetype 	= lastIDTHEMETYPE;
							popTypeCMDABO.idsegment 	= lastIDSEGMENT;
							popTypeCMDABO.listeAbo 		= myPaginatedatagrid.selectedItems;
							popTypeCMDABO.idoperateur 	= comboOperateur.selectedItem.ID_OP;
							
							PopUpManager.addPopUp(popTypeCMDABO,this,true);
							PopUpManager.centerPopUp(popTypeCMDABO);
						}
					}
					else
					{
						if(boolError)							
							Alert.show(ResourceManager.getInstance().getString('M24', 'Vous_avez_s_lectionn__des_options_et_des'),ResourceManager.getInstance().getString('M24', 'Consoview'));
						else
						{
							if(boolErrorSeg)
								Alert.show(ResourceManager.getInstance().getString('M24', 'Vous_avez_s_lectionn__plusieurs_segments'),ResourceManager.getInstance().getString('M24', 'Consoview'));
						}
						
					}
					
					isTypeCmd = true;
				}
				
				if(!isTypeCmd)
				{
					if(dataExp.length > 0) 
					{     
						var dataToExport  :String = DataGridDataExporter.getCSVString(myPaginatedatagrid.dgPaginate,dataExp,';','\n',false);
						var url :String = moduleCatalogueIHM.NonSecureUrlBackoffice + '/fr/consotel/consoview/M24/exportCSVString.cfm';
						DataGridDataExporter.exportCSVString(url,dataToExport,'export');
					 }
					 else
					        Alert.show(ResourceManager.getInstance().getString('M24', 'Pas_de_donn_es___exporter__'),ResourceManager.getInstance().getString('M24', 'info'));
				 }
            }
         }
	}
}