package catalogue.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import catalogue.AbstractCatalogue;
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.ihm.catalogueperso.CreateCatalogueClientIHM;
	import catalogue.ihm.compatibilite.ListeAccessoireIHM;
	import catalogue.ihm.importequipement.ImportSourceIsConstructeurIHM;
	import catalogue.ihm.parametre.AddEquipementRevendeurIHM;
	import catalogue.ihm.popup.PopupFilterIHM;
	import catalogue.profil.GestionProfil;
	import catalogue.util.CatalogueFormatteur;
	import catalogue.util.Currency;
	import catalogue.util.DataGridDataExporter;
	import catalogue.vo.ClasseVO;
	import catalogue.vo.ConstructeurVO;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.ParametreSearchCatalogueVO;
	import catalogue.vo.RevendeurVO;
	import catalogue.vo.SegmentVO;
	import catalogue.vo.TypeEquipementVO;
	
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import paginatedatagrid.columns.event.StandardColonneEvent;
	import paginatedatagrid.event.PaginateDatagridEvent;
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	import searchpaginatedatagrid.ParametresRechercheVO;

	[Bindable]
	public class DistributeurImpl extends AbstractCatalogue 
	{
		
		/**
		 * Pour ajouter des vues sur le grid, ajouter des constantes static ci dessous
		**/
	
		public const NBITEMPARPAGE : int = 30;
		
		public var currenciesAreDifferent:Boolean = false;
		 
		public var comboConstructeur : ComboBox;
		
		public var comboRevendeur : ComboBox;
		
		public var comboSegment : ComboBox;
		
		public var comboEtat : ComboBox;
		
		public var popAddEquipement : AddEquipementRevendeurIHM;
		private var popImport : ImportSourceIsConstructeurIHM;
		private var popCreateCatalogue : CreateCatalogueClientIHM;
		public var labelRevendeur : Label;
		private var _popUpFilter		:PopupFilterIHM;
		
		public var listFilter:ArrayCollection;

		
		public function DistributeurImpl()
		{
			
		}
		
		public function btImgChooseConstructeur_clickHandler(e:Event):void
		{
			var listconstucteurs:ArrayCollection = catalogueCS.listeConstructeur as ArrayCollection;
			this.refreshlistFilter(listconstucteurs,1)
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.listeFilter = listFilter;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_constructeur');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', getItemFilterHandler);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		public function btImgChooseDistributeur_clickHandler(e:Event):void
		{
			var listDistributeur:ArrayCollection = catalogueCS.listeRevendeur as ArrayCollection;
			this.refreshlistFilter(listDistributeur,2)
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.listeFilter = listFilter;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_distributeur');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', getItemFilterHandler1);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		protected function getItemFilterHandler(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', getItemFilterHandler);
			refreshComboConstructeur();
		}
		protected function getItemFilterHandler1(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', getItemFilterHandler1);
			refreshComboDistributeur();
		}
		
		private function majSelectConstructeur():void
		{
			for (var i:int=0; i<catalogueCS.listeConstructeur.length;i++)
			{
				(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =false;
				if ((catalogueCS.listeConstructeur[i] as ConstructeurVO).ID_CONSTRUCTEUR == (comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR)
				{
					(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =true;
				}
			}
		}
		
		private function majSelectDistributeur():void
		{
			for (var i:int=0; i<catalogueCS.listeRevendeur.length;i++)
			{
				(catalogueCS.listeRevendeur[i] as RevendeurVO).SELECTED =false;
				if ((catalogueCS.listeRevendeur[i] as RevendeurVO).ID_REVENDEUR == (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR)
				{
					(catalogueCS.listeRevendeur[i] as RevendeurVO).SELECTED =true;
				}
			}
		}
		
		
		private function refreshComboConstructeur():void
		{
			var k:int;
			k=_popUpFilter.getItemCurrentID();
			for (var i:int=0; i<catalogueCS.listeConstructeur.length;i++)
			{
				(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =false;
				if ((catalogueCS.listeConstructeur[i] as ConstructeurVO).ID_CONSTRUCTEUR == _popUpFilter.getItemCurrentID())
				{
					(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =true;
					comboConstructeur.selectedIndex = i;
				}
			}
			comboRequiredHandler();
		}
		
		private function refreshComboDistributeur():void
		{
			var k:int;
			k=_popUpFilter.getItemCurrentID();
			for (var i:int=0; i<catalogueCS.listeRevendeur.length;i++)
			{
				(catalogueCS.listeRevendeur[i] as RevendeurVO ).SELECTED =false;
				if ((catalogueCS.listeRevendeur[i] as RevendeurVO).ID_REVENDEUR == _popUpFilter.getItemCurrentID())
				{
					(catalogueCS.listeRevendeur[i] as RevendeurVO).SELECTED =true;
					comboRevendeur.selectedIndex = i;
				}
			}
			comboRequiredHandler();
		}
		
		private function refreshlistFilter(listdata:ArrayCollection, dataFilter:int):void
		{
			listFilter =new ArrayCollection();
			switch (dataFilter)
			{
				case 1:
					for (var i:int=0; i<listdata.length;i++)
					{
						var item:Object = new Object();
						item.SELECTED = (listdata.getItemAt(i) as ConstructeurVO).SELECTED;
						item.IDITEM = (listdata.getItemAt(i) as ConstructeurVO).ID_CONSTRUCTEUR;
						item.LIBELLE_ITEM = (listdata.getItemAt(i) as ConstructeurVO).LIBELLE_CONSTRUCTEUR;
						listFilter.addItem(item);
					}
					break;
				case 2:
					for (var j:int=0; j<listdata.length;j++)
					{
						var item:Object = new Object();
						item.SELECTED = (listdata.getItemAt(j) as RevendeurVO).SELECTED;
						item.IDITEM = (listdata.getItemAt(j) as RevendeurVO).ID_REVENDEUR;
						item.LIBELLE_ITEM = (listdata.getItemAt(j) as RevendeurVO).LIBELLE_REVENDEUR;
						listFilter.addItem(item);
					}
					break;
				default:
			}
			
		}
		
		public function addEquipementHandler(evt : MouseEvent):void
		{
			popAddEquipement = new AddEquipementRevendeurIHM();
			popAddEquipement.listClts=catalogueCS.listeClient;
			addPop();
		}
		private var popAccessoire : ListeAccessoireIHM;
		
		public function comboActionHandler(event : PaginateDatagridEvent):void
		{
			if(comboRevendeur.selectedItem)
			{
				if(myPaginatedatagrid.selectedItems.length>0)
				{
					this.btExportHandler(event);
				}
				else
				{
					Alert.show(ResourceManager.getInstance().getString('M24', '_Aucun__quipement_s_l_ctionn__'));
				}
				
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', '_Un_client_doit__tre_selectionn__'));
			}
			
		}
		override public function btExportHandler(evt : PaginateDatagridEvent,idClientTypeCMD:Number=-1):void
		{
			if(comboExport.selectedItem)
			{
				var isAcc : Boolean = false;
				
				var dataExp : Object = new Object();
				if(comboExport.selectedIndex == 0)
				{
					dataExp = myPaginatedatagrid.selectedItems;
				}
				else if(comboExport.selectedIndex == 1)
				{
					dataExp = myPaginatedatagrid.dataprovider;
				}
				else if(comboExport.selectedIndex == 2)
				{
					popAccessoire = new ListeAccessoireIHM()
					popAccessoire.idRevendeur = (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR;
					
					for(var i:int=0;i<myPaginatedatagrid.selectedItems.length;i++)
					{
						
						if(popAccessoire.listeIDEquipement)
						{
							
							popAccessoire.listeIDEquipement = (myPaginatedatagrid.selectedItems.getItemAt(i) as EquipementCatalogueVO).IDEQUIPEMENT + ","+ popAccessoire.listeIDEquipement;
						}
						else
						{
							popAccessoire.listeIDEquipement = (myPaginatedatagrid.selectedItems.getItemAt(i) as EquipementCatalogueVO).IDEQUIPEMENT.toString();
						}
					}
					popAccessoire.listeEquipement = myPaginatedatagrid.selectedItems;
					PopUpManager.addPopUp(popAccessoire,this,true);
					PopUpManager.centerPopUp(popAccessoire);
					isAcc = true
				}
				if(!isAcc)
				{
					if(dataExp.length > 0 ) 
					{     
						var dataToExport  :String = DataGridDataExporter.getCSVString(myPaginatedatagrid.dgPaginate,dataExp,";","\n",false);
						var url :String = moduleCatalogueIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M24/exportCSVString.cfm";
						DataGridDataExporter.exportCSVString(url,dataToExport,"export");
					 }
					 else
					 {
					        Alert.show(ResourceManager.getInstance().getString('M24', 'Pas_de_donn_es___exporter__'),"Consoview");
					 }
				 }
            }
  		}
		public function comboRequiredHandler():void
		{
			if(comboConstructeur.selectedIndex!=-1 && comboRevendeur.selectedIndex!=-1)
			{
				mySearchPaginateDatagrid.enabled = true;
				CatalogueFormatteur.getInstance().distributorCurrency = new Currency(comboRevendeur.selectedItem.SYMBOL_DEVISE);
				
				if (comboRevendeur.selectedItem.SYMBOL_DEVISE != resourceManager.getString('M24', '_4635'))
				{
					currenciesAreDifferent = true;
				}
			}
			else
			{
				mySearchPaginateDatagrid.enabled = false;
			}
			majSelectConstructeur();
			majSelectDistributeur();
			catalogueCS.rechercheClient((comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR);
		}
		public function importerEquipementHandler(evt : MouseEvent):void
		{
			popImport = new ImportSourceIsConstructeurIHM();
		
			popImport.idRevendeur = (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR;
			addPopImport();
		}
		private function addPopImport():void
		{
			popImport.addEventListener(PopUpCatalogueEvent.REFRESH_PARENT_DATA,addEquipementCompleteHandler);
			PopUpManager.addPopUp(popImport,this,true);
			PopUpManager.centerPopUp(popImport);
		}
		public function createCatalogueHandler(evt : MouseEvent):void
		{
			popCreateCatalogue = new CreateCatalogueClientIHM();
			popCreateCatalogue.idDistributeur = (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR
			PopUpManager.addPopUp(popCreateCatalogue,this,true);
			PopUpManager.centerPopUp(popCreateCatalogue);
		}
		public function initData():void
		{
			thisComponentIsComplete = true;
			
			catalogueCS.addEventListener(CatalogueEvent.LISTE_CONSTRUCTEUR_EQUIPEMENT_COMPLETE,listeConstructeurHandler);
			catalogueCS.rechercheContructeur(true);
			catalogueCS.addEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE,listeClasseEquipementCompleteHandler);
			catalogueCS.rechercheClasseEquipement(true);
			
			updateProfil();
			
			//Par defaut, le tri se fait sur le modèle:			
			mySearchPaginateDatagrid.comboRecherche.selectedIndex = 6; // Sélectionne modèle dans la combo Clé de recherche
			mySearchPaginateDatagrid.comboTrieColonne.selectedIndex = 6; // Sélectionne modèle dans la combo ORDERBY
			
			myPaginatedatagrid.dgPaginate.rowColorFunction=selectColor; // selection des couleurs des lignes de la dataGrid
			
		}
		
		private function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint			
		{				
			var rColor:uint;				
			rColor = color;				
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			
			if (item != null )				
			{				
				if(item.DATEVALIDITE != null && DateFunction.dateDiff("d",item.DATEVALIDITE,new Date()) +1 < 0 ) // fin de vie
				{	
					rColor = 0xFF0000;
				}
				else if (item.SUSPENDU == 1)//En rupture	
				{	
					rColor = 0xF7FF04;
				}
				else if (item.DATEVALIDITE != null) 	//Bientot fin de vie
				{	 
					rColor = 0xFEC46D;
				}				
				else					
					rColor = color;					
			}				
			return rColor;				
		}
		
		private function listeClasseEquipementCompleteHandler(evt:CatalogueEvent):void
		{
			if(catalogueCS.hasEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE))
			{
				catalogueCS.removeEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE,listeClasseEquipementCompleteHandler);	
			}
			
			comboClasseHandler();	
		}
		
		private function listeConstructeurHandler(evt : CatalogueEvent):void
		{
			comboConstructeur.selectedIndex = 0;
		}
		override public function updateProfil():void
		{
			if(thisComponentIsComplete){
				switch (gestionProfil.CURRENT_PROFIL)
				{
					case (GestionProfil.CONSOTEL_PROFIL):
						catalogueCS.rechercheRevendeur();
						comboRevendeur.selectedIndex = -1;
						comboRevendeur.enabled = true;
						comboRevendeur.visible = true;
						comboRevendeur.includeInLayout = true;
						labelRevendeur.text = "";
						
					break;
					case (GestionProfil.DISTRIBUTEUR_PROFIL):
						var rev : RevendeurVO = new RevendeurVO();
						rev.ID_REVENDEUR = gestionProfil.ID_CURRENT_PROFIL;
						rev.LIBELLE_REVENDEUR = gestionProfil.LIBELLE_CURRENT_PROFIL;
						rev.SYMBOL_DEVISE = gestionProfil.REVENDEUR.SYMBOL_DEVISE;
						catalogueCS.listeRevendeur.removeAll();
						catalogueCS.listeRevendeur.addItem(rev);
						comboRevendeur.selectedIndex = 0;
						CatalogueFormatteur.getInstance().distributorCurrency = new Currency(rev.SYMBOL_DEVISE);
						comboRevendeur.enabled = false;
						comboRevendeur.visible = false;
						comboRevendeur.includeInLayout = false;
						majSelectDistributeur();
						labelRevendeur.text = rev.LIBELLE_REVENDEUR;
						mySearchPaginateDatagrid.enabled = true;
						catalogueCS.rechercheClient(gestionProfil.ID_CURRENT_PROFIL);
						
					break;
				}
				
			}
		} 
		
		public function supprimer(data : EquipementCatalogueVO):void
		{
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M24', '_tes_vous_s_r_de_vouloir_supprimer_l__qu')+data.MODELE+ResourceManager.getInstance().getString('M24', '__'),ResourceManager.getInstance().getString('M24', 'Confirmation'),supprimerBtnValiderCloseEvent);
		}
		override protected function paginateItemEditHandler(evt : StandardColonneEvent):void
		{
			popAddEquipement = new AddEquipementRevendeurIHM();
			popAddEquipement.equipement = evt.item as EquipementCatalogueVO;
			popAddEquipement.equipement.IDREVENDEUR = (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR;
			popAddEquipement.listClts=catalogueCS.listeClient;
			addPop();
		}
		
		
		private function addEquipementCompleteHandler(evt : PopUpCatalogueEvent):void
		{
			if(mySearchPaginateDatagrid.parametresRechercheVO && comboConstructeur.selectedItem){
				rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
			}
		}
		private function addPop():void
		{
			popAddEquipement.addEventListener(PopUpCatalogueEvent.REFRESH_PARENT_DATA,addEquipementCompleteHandler);
			popAddEquipement.equipement.IDREVENDEUR = (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR;
			PopUpManager.addPopUp(popAddEquipement,this,true);
		}
		
		override public function rechercher(parametresRechercheVO : ParametresRechercheVO, itemIntervalleVO : ItemIntervalleVO = null):void
		{
			
			if(comboConstructeur.selectedItem){
				//Selectionne 'tous'
				if(comboClasse.selectedIndex==-1)
				{
					comboClasse.selectedIndex = 0;
				}
				comboTypeHandler();
							
				if(!itemIntervalleVO) // Si c'est une nouvelle recherche
				{
					myPaginatedatagrid.refreshPaginateDatagrid(true);
					itemIntervalleVO = new ItemIntervalleVO();
					itemIntervalleVO.indexDepart = 0;
					itemIntervalleVO.tailleIntervalle = NBITEMPARPAGE;
				}
				
				//Ajout des 3 paramêtres spécifique au catalogue :
				var parametreSearchCatalogueVO : ParametreSearchCatalogueVO = new ParametreSearchCatalogueVO();
				parametreSearchCatalogueVO.IDCLASSE =(comboClasse.selectedItem as ClasseVO).ID_CLASSE;
				parametreSearchCatalogueVO.IDETAT = comboEtat.selectedIndex;
				parametreSearchCatalogueVO.IDCONSTRUCTEUR =(comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR;
				parametreSearchCatalogueVO.IDREVENDEUR =(comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR;
				parametreSearchCatalogueVO.IDTYPEEQUIPEMENT =(comboType.selectedItem as TypeEquipementVO).ID_TYPE;
				parametreSearchCatalogueVO.ORDER_BY=parametresRechercheVO.ORDER_BY;
				parametreSearchCatalogueVO.SEARCH_TEXT=parametresRechercheVO.SEARCH_TEXT;
				parametreSearchCatalogueVO.GROUPE_INDEX =groupeIndex;
				parametreSearchCatalogueVO.IDSEGMENT = (comboSegment.selectedItem as SegmentVO).IDSEGMENT;
					
				catalogueCS.rechercheListeDataCatalogue(parametreSearchCatalogueVO,itemIntervalleVO);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Un_constructeur_doit__tre_s_lectionn__'),ResourceManager.getInstance().getString('M24', 'Attention'));	
			}
		}
		private function removeEqCompleteHandler(evt : CatalogueEvent):void
		{
			catalogueCS.removeEventListener(CatalogueEvent.REMOVE_EQUIPEMENT_COMPLETE,removeEqCompleteHandler);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'L__quipement_a_bien__t__supprim__'));
			if(mySearchPaginateDatagrid.parametresRechercheVO && comboConstructeur.selectedItem){
				rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
			}
		}
		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				catalogueCS.addEventListener(CatalogueEvent.REMOVE_EQUIPEMENT_COMPLETE,removeEqCompleteHandler);
				catalogueCS.removeEquipement(myPaginatedatagrid.selectedItem as EquipementCatalogueVO);
			}
		}
		
	}
}