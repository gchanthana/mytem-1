package catalogue.ihm.parametre
{
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.strategie.ICatalogue;
	import catalogue.strategie.strategieconcrete.ClientCS;
	import catalogue.util.CvDateChooser;
	import catalogue.util.TarifDatagrid;
	import catalogue.vo.ConstructeurVO;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.TypeEquipementVO;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.net.registerClassAlias;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;


	[Bindable]
	public class UpdateTarifEquipementClientImpl extends TitleWindow
	{
		
		private var _catalogueCS:ICatalogue;
		private var _equipement:EquipementCatalogueVO = new EquipementCatalogueVO();
	
		
		
		//Public UI componant
		public var comboType : ComboBox;
		public var cbAddInCatConstr : CheckBox;
		public var cbPrevenirClient : CheckBox;
		public var cbAjouterEquipOtherCat : CheckBox;
		
		public var comboConstructeur : ComboBox;
		public var inputLib:TextInput;
		public var inputRef:TextInput;
		public var dgTarif : TarifDatagrid;
		public var dcvalidClient : CvDateChooser; 
		
		public function UpdateTarifEquipementClientImpl()
		{
				
		}
		public function initData():void
		{
			catalogueCS = new ClientCS();
			
			if(equipement.IDEQUIPEMENT>0)
			{
				initMode_update();
			}
		}
		private function initMode_update():void
		{
			registerClassAlias("catalogue.vo.EquipementCatalogueVO",EquipementCatalogueVO);  
			equipement= EquipementCatalogueVO(ObjectUtil.copy(equipement))  // delete les ref sur le grid
			/*if (equipement.DATEVALIDITE !=null)
				dcvalidClient.selectableRange={rangeEnd:equipement.DATEVALIDITE};*/
		}
		public function fermer(event : Event=null):void
		{
			PopUpManager.removePopUp(this);
		}
		public function valider():void
		{
			
		}
	
		
		public function btValiderFermerHandler():void
		{
			var boolAllTarifValide : Boolean = true;
			var rowRenderers:Array = dgTarif.listRendererArray;
	     	  for(var i:int = 0; i < rowRenderers[0].length; i++)
	           {
	             if(rowRenderers[0][i].tarifIsValide==false)
	             {
	             	boolAllTarifValide = false;
	             }
	           }
			
				if(boolAllTarifValide)
				{
					if(equipement.IDEQUIPEMENT>0)
					{
						//update
						catalogueCS.addEventListener(CatalogueEvent.UPDATE_EQUIPEMENT_COMPLETE,update_equipement_complete_handler);
						catalogueCS.updateEquipement(equipement);
					}
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Tous_les_tarifs_doivent__tre_valides'), ResourceManager.getInstance().getString('M24', 'Erreur'));
			}
		}
	
		
		private function update_equipement_complete_handler(evt : CatalogueEvent):void
		{
			
			if(evt.objectReturn> 0)
			{
			
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Equipement_modifi_'));
				dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			}
					
			fermer();
		}
		private function add_equipement_complete_handler(evt : CatalogueEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Equipement_ajout___R_f_rence_consotel___')+evt.objectReturn.REF);
			dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
		}

		public function set catalogueCS(value:ICatalogue):void
		{
			_catalogueCS = value;
		}

		public function get catalogueCS():ICatalogue
		{
			return _catalogueCS;
		}

		public function set equipement(value:EquipementCatalogueVO):void
		{
			_equipement = value;
		}

		public function get equipement():EquipementCatalogueVO
		{
			return _equipement;
		}
		public function comboTypeHandler():void
		{
			if(comboType.selectedItem)
			{
				equipement.IDTYPE = (comboType.selectedItem as TypeEquipementVO).ID_TYPE
			}
		}
		
		public function comboConstructeurHandler():void
		{
			if(comboConstructeur.selectedItem)
			{
				equipement.IDCONSTRUCTEUR = (comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR
			}
		}
	}
}