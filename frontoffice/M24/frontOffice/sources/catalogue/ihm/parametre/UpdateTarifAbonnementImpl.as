package catalogue.ihm.parametre
{
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.vo.AbonnementVO;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.registerClassAlias;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	[Bindable]
	public class UpdateTarifAbonnementImpl extends TitleWindow
	{
		public var abonnement		:AbonnementVO = new AbonnementVO();
		
		public var globalService 	:GlobalCatalogueService = new GlobalCatalogueService();
		
		public var tiTarif			:TextInput;
		public var tiNameCommande	:TextInput;
		
		public var rbNoAffiche		:RadioButton;
		public var rbAffiche		:RadioButton;
		
		public var libelleOP 		:String;
		
		public var idRacineClient 	:int;
		
		public var idRacineComboSelected 	:int;
		
		public function UpdateTarifAbonnementImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			initData();
		}
		
		private function initData():void
		{
			registerClassAlias("catalogue.vo.AbonnementVO", AbonnementVO);
			
			abonnement	= AbonnementVO(ObjectUtil.copy(abonnement))
			
			//			if(abonnement.IS_SPECIFIC > 0)
			//			{
			//				globalService.getSpecificProductName(abonnement.IDABO);
			//				globalService.addEventListener(CatalogueEvent.ADD_SPECIFIC_NAME, specificNameAddedCompleteHandler);
			//			}
			
			globalService.getSpecificProductName(abonnement.IDABO, idRacineComboSelected);
			globalService.addEventListener(CatalogueEvent.ADD_SPECIFIC_NAME, specificNameAddedCompleteHandler);
		}
		
		public function fermer(e:Event = null):void
		{
			PopUpManager.removePopUp(this);
		}
		
		public function valider():void
		{
			
		}
		
		protected function rdbtnClickHandler(me:MouseEvent):void
		{
			var idStrg:String = (me.target as RadioButton).id;
			
			if(idStrg == 'rbAffiche')
			{
				abonnement.ISVISIBLETOCOMMANDE = true;
			}
			else if(idStrg == 'rbNoAffiche')
				{	
					abonnement.ISVISIBLETOCOMMANDE = false;					
				}
		}
		
		protected function btValiderFermerHandler():void
		{
			if(tiTarif.text.length > 0)
			{
				var isPriceVisible:int = 0;
				
				if(abonnement.ISVISIBLETOCOMMANDE)
					isPriceVisible = 1;
				
				var priceWithPoint:String = tiTarif.text;
				
				priceWithPoint = priceWithPoint.replace(',','.');
				
				globalService.updateAbonnement(abonnement.IDABO, Number(priceWithPoint), tiNameCommande.text, idRacineComboSelected, isPriceVisible);
				globalService.addEventListener(CatalogueEvent.UPDATE_ABO_COMPLETE, updateAboCompleteHandler);
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M24', 'Le_tarif_est_obligatoire'));
		}
		
		private function updateAboCompleteHandler(ce:CatalogueEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Abonnement_modifi__'));
			
			dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			
			fermer();
		}
		
		private function specificNameAddedCompleteHandler(ce:CatalogueEvent):void
		{
			tiNameCommande.text = globalService.LIBELLE_SPECIFIC;
		}
		
	}
}