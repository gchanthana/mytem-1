package catalogue
{
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.DataGridEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import catalogue.ihm.fiche.DetailProduitIHM;
	import catalogue.profil.GestionProfil;
	import catalogue.service.SegmentService;
	import catalogue.strategie.ICatalogue;
	import catalogue.util.CatalogueFormatteur;
	import catalogue.util.DataGridDataExporter;
	import catalogue.vo.ClasseVO;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.TypeEquipementVO;
	
	import paginatedatagrid.PaginateDatagrid;
	import paginatedatagrid.columns.event.StandardColonneEvent;
	import paginatedatagrid.event.PaginateDatagridEvent;
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	import searchpaginatedatagrid.ParametresRechercheVO;
	import searchpaginatedatagrid.SearchPaginateDatagrid;
	import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;

	[Bindable]
	public class AbstractCatalogue extends VBox
	{
		public static const VUE_TARIF : String = "vueTarif";
		public static const VUE_LISTE : String = "vueListe";
		public var libelleVUE : String = VUE_LISTE;
		
		private var _catalogueCS : ICatalogue;
		private var _gestionProfil : GestionProfil;
		public var segmentService : SegmentService = new SegmentService();
			
		protected var groupeIndex:int;
		protected var idClient:int;
		public var comboClasse : ComboBox;
		public var comboType : ComboBox;		
		public var myPaginatedatagrid : PaginateDatagrid;
		public var comboExport : ComboBox;
		public var mySearchPaginateDatagrid : SearchPaginateDatagrid;
		protected var thisComponentIsComplete:Boolean = false; // Plus utilisé pour l'instant car pas de changement profil dynamic
		private var col:String;
		
		
		public function AbstractCatalogue()
		{
			super();
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			segmentService.getAllSegment();
		}
		private function creationCompleteHandler(evt : FlexEvent):void
		{
			mySearchPaginateDatagrid.paginateDatagridInstance.dgPaginate.addEventListener(DataGridEvent.HEADER_RELEASE, dataGrid_headerRelease);
		}
		private function dataGrid_headerRelease(evt:DataGridEvent):void {
            col = mySearchPaginateDatagrid.paginateDatagridInstance.dgPaginate.columns[evt.columnIndex].dataField;
        }
		public function numericSort(itemA:Object, itemB:Object):int
		{
			//SI LES PRIX NE SONT PAS LES MEMES, trier, sinon trier sur le modele
			if(ObjectUtil.numericCompare(itemA[col], itemB[col])!=0){
				return ObjectUtil.numericCompare(itemA[col], itemB[col]);
			}
			else
			{
				return ObjectUtil.stringCompare(itemA.MODELE, itemB.MODELE);
			}
		}
		
		public function formatPrice( item:Object, col:DataGridColumn) :String
		{  
			//return ConsoviewFormatter.formatEuroCurrency(item[col.dataField],2);
			return CatalogueFormatteur.getInstance().distributorCurrency.format(item[col.dataField]);
		}
		public function get catalogueCS():ICatalogue
		{
			return _catalogueCS;
		}
		
		public function set catalogueCS(catalogueCS : ICatalogue):void
		{
			this._catalogueCS = catalogueCS;
		}
		/**
		 * 
		 * Handler : StandardColonneEvent.EDIT_ITEM
		 */
		protected function paginateItemEditHandler(evt : StandardColonneEvent):void
		{
			Alert.show('Abstract EDIT_ITEM - : '+ObjectUtil.toString(evt.item));
		}
		
		/**
		 * 
		 * Handler : StandardColonneEvent.REMOVE_ITEM
		 */
		protected function paginateItemRemoveHandler(evt : StandardColonneEvent):void
		{
			Alert.show('REMOVE_ITEM - : '+ObjectUtil.toString(evt.item));
		}
		
		public function comboClasseHandler():void
		{
			if(comboClasse.selectedIndex == -1)
			{	
				comboClasse.selectedIndex=0;
			}
			
			catalogueCS.rechercheTypeEquipement((comboClasse.selectedItem as ClasseVO).ID_CLASSE,true);
			
		}
		public function comboTypeHandler():void
		{
			if(comboType){
				if(comboType.selectedIndex == -1)
				{
					if(!(comboType.dataProvider as ArrayCollection).getItemAt(0))
					{
						var  specialitem : TypeEquipementVO = new TypeEquipementVO();
						specialitem.ID_TYPE=-1;
						specialitem.LIBELLE_TYPE=ResourceManager.getInstance().getString('M24','Tous');
						(comboType.dataProvider as ArrayCollection).addItemAt(specialitem,0);
					}
					comboType.selectedIndex=0;
				}
			}
		}
		public function showDetailHandler(equip : Object):void
		{
			var pop : DetailProduitIHM = new DetailProduitIHM();
			pop.equipement = equip as EquipementCatalogueVO;
			PopUpManager.addPopUp(pop,this,true);
			PopUpManager.centerPopUp(pop);
		}
		public function onSearchHandler(event:SearchPaginateDatagridEvent):void
		{
			rechercher(event.parametresRechercheVO);
		}
		public function rechercher(parametresRechercheVO : ParametresRechercheVO, itemIntervalleVO : ItemIntervalleVO = null):void
		{
			//see override public function rechercher in concrete strategy
		}
		public function btExportHandler(evt : PaginateDatagridEvent,idClientTypeCMD:Number=-1):void
		{
			if(comboExport.selectedItem)
			{
				var isTypeCmd : Boolean = false;
				
				var dataExp : Object = new Object();
				if(comboExport.selectedIndex == 0)
				{
					dataExp = myPaginatedatagrid.selectedItems;
				}
				else if(comboExport.selectedIndex == 1)
				{
					dataExp = myPaginatedatagrid.dataprovider;
				}
			
				if(dataExp.length > 0 ) 
				{     
					var dataToExport  :String = DataGridDataExporter.getCSVString(myPaginatedatagrid.dgPaginate,dataExp,";","\n",false);
					var url :String = moduleCatalogueIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M24/exportCSVString.cfm";
					DataGridDataExporter.exportCSVString(url,dataToExport,"export");
				 }
				 else
				 {
				        Alert.show(ResourceManager.getInstance().getString('M24', 'Pas_de_donn_es___exporter__'),ResourceManager.getInstance().getString('M24', 'Consoview'));
				 }
			 }
       	}
  		public function onIntervalleChangeHandler(evt : PaginateDatagridEvent):void
		{
			rechercher(mySearchPaginateDatagrid.parametresRechercheVO,myPaginatedatagrid.currentIntervalle);
		}
		public function onItemSelectedHandler(evt : PaginateDatagridEvent):void
		{
		
		}
		
		public function set gestionProfil(value : GestionProfil):void
		{
			this._gestionProfil = value;
		}
		public function get gestionProfil():GestionProfil
		{
			return _gestionProfil;
		}
		public function updateProfil():void
		{
			
		}
		public function btVueListeHandler():void
		{
			libelleVUE = AbstractCatalogue.VUE_LISTE;
		}
		public function btVueTarifHandler():void
		{
			libelleVUE = AbstractCatalogue.VUE_TARIF;
		}
		
		
	}
}