package catalogue.event
{
	import flash.events.Event;

	public class PopUpCatalogueEvent extends Event
	{
		public static const REFRESH_PARENT_DATA:String= "refreshParentData";
			
		public function PopUpCatalogueEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}