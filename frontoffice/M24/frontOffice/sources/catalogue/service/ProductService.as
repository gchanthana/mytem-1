package catalogue.service
{
	import mx.resources.ResourceManager;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class ProductService extends EventDispatcher
	{
		private var _listeFeature			:ArrayCollection = new ArrayCollection();
		private var _listePhotos			:ArrayCollection = new ArrayCollection();
		//CHEMIN D'ACCES AUX PROCEDURES
		private var pathCommande:String = "fr.consotel.consoview.inventaire.commande.GestionCommande";
	
		public function ProductService()
		{
		}
		public function set listePhotos(value:ArrayCollection):void
		{
			_listePhotos = value;
		}

		public function getProductFeature(idEquipement:int): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			pathCommande,
	    																			"getProductFeature",
	    																			getProductFeatureResultHandler);		
	  		RemoteObjectUtil.callService(op,idEquipement,3);//CHANGER '3' POUR L'INTERNATIONNALISATION (LE RECUPERER EN SESSION)	
	    }
    
	    private function getProductFeatureResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	    		listeFeature = re.result[0] as ArrayCollection;
	    		listePhotos = re.result[1] as ArrayCollection;
    			//attribuFeature();
	    		dispatchEvent(new Event("listeFeatureOk"));
	    	}
	    	else
	    	{
	    		Alert.show(ResourceManager.getInstance().getString('M24', 'Pas_de_donn_es_'),ResourceManager.getInstance().getString('M24', 'Consoview'));
	    	}
	    }
		public function get listeFeature():ArrayCollection
		{
			return _listeFeature;
		}
		public function set listeFeature(values:ArrayCollection):void
		{
			_listeFeature = values;
		}
		public function get listePhotos():ArrayCollection
		{
			return _listePhotos;
		}
		
		



	}
}