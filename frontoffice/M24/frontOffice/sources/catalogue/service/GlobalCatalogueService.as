package catalogue.service
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import catalogue.event.CatalogueEvent;
	import catalogue.util.Currency;
	import catalogue.vo.AccessoireVO;
	import catalogue.vo.ClientCatalogueVO;
	import catalogue.vo.ModeleCatalogueVO;
	import catalogue.vo.OperateurVO;
	import catalogue.vo.TypeCommandeVO;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	[Bindable]
	public class GlobalCatalogueService extends EventDispatcher
	{ 
		public var listeClient:ArrayCollection = new ArrayCollection;
		public var listeOperateur:ArrayCollection = new ArrayCollection;
		public var listeModele:ArrayCollection = new ArrayCollection;
		public var listeTypeCommande:ArrayCollection = new ArrayCollection;
		public var listeTypeCommandeSelected:ArrayCollection = new ArrayCollection;
		public var listeAccessoire:ArrayCollection = new ArrayCollection;
		public var listeOperateurClient:ArrayCollection = new ArrayCollection;
		public var listeAccessoireSelected:ArrayCollection = new ArrayCollection;
		public var indicatifTarifAbo:Number;
		
		public var LIBELLE_TYPE_LOGIN 		:String = '';
		public var LIBELLE_REVENDEUR 		:String = '';
		public var LIBELLE_GROUPE_CLIENT 	:String = '';
		public var LIBELLE_SPECIFIC			:String = '';
		
		public var ID_TYPE_LOGIN 			:int;
		public var ID_REVENDEUR 			:int;
		public var CURRENCY_SYMBOL			:String ="€";
		public var CURRENCY_ID  			:int = 1;
		public var CURRENCY_SHORT_DESC 		:String = "EUR";
		public var CURRENCY_NAME			:String = "euro";
		
		public var DEVISE					:Currency;
		
		public function GlobalCatalogueService()
		{
			
		}
		/* 	public function test(idClient : int):void
		{
		var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
		"fr.consotel.consoview.M24.CatalogueService",
		"getContactOfRacine",
		test_handler,null);
		
		RemoteObjectUtil.callService(opData,idClient);
		} */
		private function test_handler(re : ResultEvent):void
		{
			
		}
		public function getTypeCommande(listeIDEquipement : String,idClient : Number):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Client",
				"IsEquipHaveCde",
				getTypeCommande_handler,null);
			
			RemoteObjectUtil.callService(opData,listeIDEquipement,idClient);
		}
		public function getTypeCommandeOfClient(idClient : Number):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Client",
				"getTypeCMDofClient",
				getTypeCommandeOfClient_handler,null);
			
			RemoteObjectUtil.callService(opData,idClient);
		}
		public function getTypeCommandeAbo(listeIDEquipement : String,idClient : Number):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"IsAboHaveCde",
				getTypeCommande_handler,null);
			
			RemoteObjectUtil.callService(opData,listeIDEquipement,idClient);
		}
		
		/**
		 * mettre à jour un type de commande pour une liste d'équipement :
		 * Value (0 pour désaffecter et 1 pour affecter)
		 **/
		public function updateTypeCmdAbo(listeIDAbo : String,idTypeCommande : int,value : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"updateTypeCmdAbo",
				updateTypeCmdAbo_handler,null);
			
			RemoteObjectUtil.callService(opData,listeIDAbo,idTypeCommande,value);
		}
		private function updateTypeCmdAbo_handler(evt : ResultEvent):void
		{
			
		}
		
		private function getTypeCommandeOfClient_handler(re:ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeTypeCommande = new ArrayCollection();
			
			var  specialItemNF : TypeCommandeVO = new TypeCommandeVO();
			specialItemNF.idTypeCmd=-1;
			specialItemNF.libelleTypeCmd=ResourceManager.getInstance().getString('M24','Non_filtr__');
			
			var  specialItemOne : TypeCommandeVO = new TypeCommandeVO();
			specialItemOne.idTypeCmd=-2;
			specialItemOne.libelleTypeCmd=ResourceManager.getInstance().getString('M24','Au_moins_dans_un_type_de_commande_');
			
			var  specialItem : TypeCommandeVO = new TypeCommandeVO();
			specialItem.idTypeCmd=0;
			specialItem.libelleTypeCmd=ResourceManager.getInstance().getString('M24','Aucun_type_de_commande');
			
			listeTypeCommande.addItem(specialItemNF);
			listeTypeCommande.addItem(specialItemOne);		
			listeTypeCommande.addItem(specialItem);
			
			var  item : TypeCommandeVO;
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				item = new TypeCommandeVO();
				item.idTypeCmd=tmpArr[i].IDPROFIL_EQUIPEMENT;
				item.libelleTypeCmd=tmpArr[i].LIBELLE_PROFIL;
				item.commentaire=tmpArr[i].COMMENTAIRE;
				listeTypeCommande.addItem(item);
			}
		}
		public function getAccessoire(listeIDEquipement : String,idRevendeur : Number):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Revendeur",
				"lstAcclstEquip",
				getAccessoire_handler,null);
			
			RemoteObjectUtil.callService(opData,idRevendeur,listeIDEquipement);
		}
		
		public function getTarifProduct(idProduit:Number, idRacine:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"getTarifProduct",
				getTarifProduct_handler);
			
			RemoteObjectUtil.callService(op,idProduit,
				idRacine);
		}
		
		public function addSpecificProductName(idProduit:Number, libelleToCommande:String, idracinecombo:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"addSpecificProductName",
				addSpecificProductName_handler);
			
			RemoteObjectUtil.callService(op,idProduit,
				libelleToCommande,
				idracinecombo);
		}
		
		public function getSpecificProductName(idProduit:Number, idracinecombo:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"getSpecificProductName",
				getSpecificProductName_handler);
			
			RemoteObjectUtil.callService(op,idProduit,
				idracinecombo);
		}	
		
		private function addSpecificProductName_handler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new CatalogueEvent(CatalogueEvent.UPDATE_SPECIFIC_NAME));
		}
		
		private function getSpecificProductName_handler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result as ArrayCollection)
				{
					if((re.result as ArrayCollection).length > 0)
						LIBELLE_SPECIFIC = (re.result as ArrayCollection)[0].LIBELLE;
					else
						LIBELLE_SPECIFIC = '';
					
					dispatchEvent(new CatalogueEvent(CatalogueEvent.ADD_SPECIFIC_NAME));
				}
			}
		}
		
		
		private function getTarifProduct_handler(re:ResultEvent):void
		{
		}
		
		public function updateAbonnement(idAbonnement:Number, tarif:Number, libelleToCommande:String, idracinecombo:int, isPriceVisible:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"updateAbonnement_v2",
				updateAbonnement_handler);
			
			RemoteObjectUtil.callService(op,idAbonnement,
											tarif,
											libelleToCommande,
											idracinecombo,
											isPriceVisible);
		}
		
		private function updateAbonnement_handler(re:ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.UPDATE_ABO_COMPLETE));
		}
		
		/**
		 * mettre à jour un type de commande pour une liste d'équipement :
		 * Value (0 pour désaffecter et 1 pour affecter)
		 **/
		public function updateTypeCmdEquipement(listeIDEquip : String,idTypeCommande : int,value : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Client",
				"updateTypeCmdEquipement",
				updateTypeCmdEquipement_handler,null);
			
			RemoteObjectUtil.callService(opData,listeIDEquip,idTypeCommande,value);
		}
		/**
		 * mettre à jour un type de commande pour une liste d'équipement :
		 * Value (0 pour désaffecter et 1 pour affecter)
		 **/
		public function updateCompatibiliteAcc(listeIDEquip : String,idacc : int,value : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Revendeur",
				"updateCompEquip",
				updateCompatibiliteAcc_handler,null);
			
			RemoteObjectUtil.callService(opData,listeIDEquip,idacc,value);
		}
		public function listOpDistrib(idDistrib:int):void 
		{
			
			var op:AbstractOperation;
			op =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"listOpDistrib",
				listOpDistrib_handler);
			RemoteObjectUtil.callService(op,idDistrib);
		}
		public function listOpClient(idClient:int):void 
		{
			var op:AbstractOperation;
			op =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"listOpClient",
				listOpClient_handler);
			RemoteObjectUtil.callService(op,idClient);
		}
		private function listOpClient_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			listeOperateurClient.removeAll();//Vider la collection
			
			for(i=0; i < tmpArr.length;i++){	
				var  item : OperateurVO = new OperateurVO();
				item.ID_OP=tmpArr[i].OPERATEURID;
				item.LIBELLE_OP=tmpArr[i].NOM;
				item.DEVISE=tmpArr[i].DEVISE;
				listeOperateurClient.addItem(item);
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_OPERATEUR_COMPLETE));
		}
		public function listAllOpClient(idClient:int):void 
		{
			
			var op:AbstractOperation;
			op =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"getListeOperateurAll",
				listAllOpClient_handler);
			RemoteObjectUtil.callService(op,idClient);
		}
		private function listAllOpClient_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			listeOperateurClient.removeAll();//Vider la collection
			
			for(i=0; i < tmpArr.length;i++){	
				var  item : OperateurVO = new OperateurVO();
				item.ID_OP=tmpArr[i].data;
				item.LIBELLE_OP=tmpArr[i].label;
				listeOperateurClient.addItem(item);
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_OPERATEUR_COMPLETE));
		}	
		private function listOpDistrib_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			listeOperateur.removeAll();//Vider la collection
			
			for(i=0; i < tmpArr.length;i++){	
				
				if(tmpArr[i].STATUT == 1)
				{
					var  item : OperateurVO = new OperateurVO();
					item.ID_OP=tmpArr[i].OPERATEURID;
					item.LIBELLE_OP=tmpArr[i].LIBELLE;
					listeOperateur.addItem(item);
				}
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_OPERATEUR_COMPLETE));
			
		}	
		private function updateCompatibiliteAcc_handler(evt : ResultEvent):void
		{
			
		}
		private function updateTypeCmdEquipement_handler(evt : ResultEvent):void
		{
			
		}
		public function getClientOfDistributeur(idDistributeur : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Client",
				"getClientOfDistributeur",
				getClientOfDistributeur_handler,null);
			
			RemoteObjectUtil.callService(opData,idDistributeur);
		}
		public function getIdTypeLogin():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.CatalogueService",
				"getIdTypeProfil",
				getIdTypeLogin_handler);
			RemoteObjectUtil.callService(op);
			
		}
		public function sendMailNotificationDispo(listeNewEquip : ArrayCollection,listeClient : ArrayCollection):void{
			
			var op1:AbstractOperation =
				RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.M24.CatalogueService",
					"prevenirClient",
					sendMailNotification_handler);
			if(listeClient && listeClient.length > 0)
			{
				RemoteObjectUtil.callService(op1,listeNewEquip,listeClient);	
			}
			
		}  
		private function sendMailNotification_handler(re : ResultEvent):void
		{
			
		}
		public function sendMailNotificationAdd(listeNewEquip : ArrayCollection,listeClient : ArrayCollection):void{
			
			var op1:AbstractOperation =
				RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.M24.CatalogueService",
					"prevenirClientAdd",
					sendMailNotificationAdd_handler);
			
			if(listeClient && listeClient.length > 0) 
			{
				RemoteObjectUtil.callService(op1,listeNewEquip,listeClient);	
			}
			
		}
		private function sendMailNotificationAdd_handler(re : ResultEvent):void
		{
			
		}
		private function getIdTypeLogin_handler(evt : ResultEvent):void
		{
			ID_TYPE_LOGIN = Number(evt.result.ID_TYPE_LOGIN);
			LIBELLE_TYPE_LOGIN = evt.result.LIBELLE_TYPE_LOGIN;
			ID_REVENDEUR = Number(evt.result.ID_REVENDEUR);
			LIBELLE_REVENDEUR = evt.result.LIBELLE_REVENDEUR;
			LIBELLE_GROUPE_CLIENT = evt.result.LIBELLE_GROUPE_CLIENT;
			CURRENCY_SYMBOL = evt.result.CURRENCY_SYMBOL;
			CURRENCY_ID =  evt.result.CURRENCY_ID;
			CURRENCY_SHORT_DESC =  evt.result.CURRENCY_SHORT_DESC;
			CURRENCY_NAME		=  evt.result.CURRENCY_NAME;
			
			dispatchEvent(new CatalogueEvent(CatalogueEvent.GET_ID_TYPEPROFIL_COMPLETE));
		}
		private function getClientOfDistributeur_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeClient = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : ClientCatalogueVO = new ClientCatalogueVO();
				
				item.value= i;
				item.ID_CLIENT=tmpArr[i].IDGROUPE_CLIENT;
				item.LIBELLE_CLIENT=tmpArr[i].LIBELLE_GROUPE_CLIENT ;
				listeClient.addItem(item);
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_CLIENT_DISTRIBUTEUR));
		}
		private function getTypeCommande_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeTypeCommande = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : TypeCommandeVO = new TypeCommandeVO();
				
				item.idTypeCmd= tmpArr[i].IDPROFIL_EQUIPEMENT;
				item.libelleTypeCmd= tmpArr[i].LIBELLE_PROFIL;
				item.ETAT = tmpArr[i].ETAT;
				item.codeTypeCmd = tmpArr[i].CODE_INTERNE;
				item.commentaire = tmpArr[i].COMMENTAIRE;
				
				if(item.ETAT == 1)
				{
					listeTypeCommandeSelected.addItem(item.idTypeCmd);
				}
				
				listeTypeCommande.addItem(item);
			}
			/* 	listeTypeCommande.getItemAt(0).ETAT = 0;
			listeTypeCommande.getItemAt(1).ETAT = 1;
			listeTypeCommande.getItemAt(2).ETAT = 2; */
			//dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_CLIENT_DISTRIBUTEUR));*/
		}
		private function getAccessoire_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeAccessoire = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : AccessoireVO = new AccessoireVO();
				
				item.idAccessoire=tmpArr[i].IDACCESSOIRE;
				item.libelleAccessoire=tmpArr[i].LIBELLE;
				item.ETAT = tmpArr[i].STATUT;
				
				if(item.ETAT == 1)
				{
					listeAccessoireSelected.addItem(item.idAccessoire);
				}
				listeAccessoire.addItem(item);
			}
		}
		public function getModele(idDistributeur : int,typeAbo : Boolean):void
		{
			var nameOfProc : String = "getCatModList";
			if(typeAbo)
			{
				nameOfProc = "getCatModListAbo";
			}
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Client",
				nameOfProc,
				getModeleOfDistributeur_handler,null);
			
			RemoteObjectUtil.callService(opData,idDistributeur);
		}
		public function getModeleAboOfDistributeur(idDistributeur : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"getAboModList",
				getModeleOfDistributeur_handler,null);
			
			RemoteObjectUtil.callService(opData,idDistributeur);
		}
		public function createCatalogueClient(idRevendeur:Number,idClient : Number,idModele : Number = -1):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Revendeur",
				"createCatalogueClient",
				createCatalogueClient_handler,null);
			
			RemoteObjectUtil.callService(opData,idRevendeur,idClient,idModele);
		}
		/**
		 * idMethode = 
		 * 1 pour la copie de tout le catalogue 
		 * 2 pour une emulation à partir des données de facturation 
		 * 3 copie d'un modèle
		 **/
		
		public function createCatalogueClientAbonnement(idClient:int,idOperateur :int,idMethode :int,idModele :int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"createCatalogProd",
				createCatalogueClientAbonnement_handler,null);			
			RemoteObjectUtil.callService(opData,idClient,idOperateur,idMethode,idModele);
		}
		private function createCatalogueClientAbonnement_handler(re : ResultEvent):void
		{
			if(re.result>0)
			{
				dispatchEvent(new CatalogueEvent(CatalogueEvent.CREATE_CATALOGUE_CLIENT_COMPLETE));
			}
		}
		//Ne pas renseigner idOp pour créer un cat modèle équipement 
		public function createCatalogueModele(idRevendeur:Number,libelle : String,idClient : Number,idOp : Number):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.CatalogueService",
				"createCatalogueModele",
				createCatalogueModele_handler,null);
			
			RemoteObjectUtil.callService(opData,idRevendeur,libelle,CvAccessManager.getSession().USER.CLIENTACCESSID,idClient,idOp);
		}
		
		public function editeCatalogueModele(idModele:Number,libelle : String):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.CatalogueService",
				"editModele",
				editeCatalogueModele_handler,null);
			RemoteObjectUtil.callService(opData,idModele,libelle);
		}
		public function removeCatalogueModele(idModele:Number):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.CatalogueService",
				"removeCatalogueModele",
				removeCatalogueModele_handler,null);
			RemoteObjectUtil.callService(opData,idModele);
		}
		
		//----------RAJOUT		
		
		public function getLastAboAssociated(listeIDEquipement:String, idClient:Number):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"getLastAboAssociated",
				getLastAboAssociated_handler,null);
			
			RemoteObjectUtil.callService(opData,listeIDEquipement,idClient);
		}
		
		private function getLastAboAssociated_handler(re:ResultEvent):void
		{
			if(re.result)
			{
				dispatchEvent(new CatalogueEvent(CatalogueEvent.REMOVE_MODELE_COMPLETE));
			}
			else
			{
				
			}
		}
		
		
		public function updatePlanCatalog(clientid:int,plans:XML):void
		{
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M24.Abonnement",
				"updatePlanCatalog",updatePlanCatalog_handler,null);
			
			RemoteObjectUtil.callService(opData,clientid,plans);
		}		//----------RAJOUT		
		
		
		private function updatePlanCatalog_handler(re :ResultEvent):void
		{
			if(re.result>0)
			{
				dispatchEvent(new CatalogueEvent(CatalogueEvent.UPDATE_ABO_COMPLETE));
			}
			else
			{
				dispatchEvent(new CatalogueEvent(CatalogueEvent.UPDATE_FAILURE));	
			}
			
		}
		private function removeCatalogueModele_handler(re : ResultEvent):void
		{
			if(re.result>0)
			{
				dispatchEvent(new CatalogueEvent(CatalogueEvent.REMOVE_MODELE_COMPLETE));
			}
		}
		private function editeCatalogueModele_handler(re : ResultEvent):void
		{
			if(re.result>0)
			{
				dispatchEvent(new CatalogueEvent(CatalogueEvent.EDIT_MODELE_COMPLETE));
			}
		}
		private function createCatalogueClient_handler(re : ResultEvent):void
		{
			if(re.result>0)
			{
				dispatchEvent(new CatalogueEvent(CatalogueEvent.CREATE_CATALOGUE_CLIENT_COMPLETE));
			}
		}
		private function createCatalogueModele_handler(re : ResultEvent):void
		{
			if(re.result>0)
			{
				dispatchEvent(new CatalogueEvent(CatalogueEvent.CREATE_MODELE_COMPLETE));
			}
		}
		private function getModeleOfDistributeur_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeModele = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : ModeleCatalogueVO = new ModeleCatalogueVO();
				
				item.ID_MODELE=tmpArr[i].IDMODELE;
				item.LIBELLE_MODELE=tmpArr[i].LIBELLE_MODELE;
				listeModele.addItem(item);
			}
		}
	}
}