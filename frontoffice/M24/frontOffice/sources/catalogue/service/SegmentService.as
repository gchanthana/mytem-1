package catalogue.service
{
	import catalogue.event.CatalogueEvent;
	import catalogue.vo.SegmentVO;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class SegmentService extends EventDispatcher
	{
		private var _listeSegment:ArrayCollection;
		
		public function SegmentService(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		public function getAllSegment():void
		{
			listeSegment = null;
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.SegmentService",
							"getAllSegment",
							getAllSegmentResultHandler,null);
			
			RemoteObjectUtil.callService(opData);	
		}
		
		private function getAllSegmentResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				listeSegment = new ArrayCollection();
				var query:ArrayCollection = event.result as ArrayCollection;				
				var cursor:IViewCursor = query.createCursor();
				var segment:SegmentVO;
				
				while(!cursor.afterLast)
				{
					segment = new SegmentVO();
					segment.fill(cursor.current);
					listeSegment.addItem(segment);
					cursor.moveNext()
				}
				
				dispatchEvent(new CatalogueEvent(CatalogueEvent.SEGMENT_REQUEST_COMPLETE));
			}
		}
			

		public function set listeSegment(value:ArrayCollection):void
		{
			_listeSegment = value;
		}

		public function get listeSegment():ArrayCollection
		{
			return _listeSegment;
		}
	}
}