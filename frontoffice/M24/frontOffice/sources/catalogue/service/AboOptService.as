package catalogue.service
{
	import catalogue.vo.TypeAboOpt;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class AboOptService extends EventDispatcher
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTANTE
		//--------------------------------------------------------------------------------------------//
		
		public static const ABOOPT_TYPECOMMANDE			:String = "ABOOPT_TYPECOMMANDE";
		public static const ABOOPT_LASTABOASSOCIATED	:String = "ABOOPT_LASTABOASSOCIATED";
		public static const ABOOPT_ABOOPTASSOCIATED		:String = "ABOOPT_ABOOPTASSOCIATED";
		public static const ABOOPT_OPTASSOCIATED		:String = "ABOOPT_OPTASSOCIATED";
		public static const ABOOPT_ABOOPTOBLIGATOIRE	:String = "ABOOPT_ABOOPTOBLIGATOIRE";
		public static const ABOOPT_ABONNEMENT			:String = "ABOOPT_ABONNEMENT";
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var ABO_ASSOCIATED						:ArrayCollection;
		public var ABONNEMENT							:ArrayCollection;
		public var TYPECOMMANDE							:ArrayCollection;
		
		public var INTEGER_RESULT						:int;
		
		private var _listeRessources					:String = "";
		private var _idTypeTheme						:int = 0;
		
		private var _isMsgVisible						:Boolean = true;
		
		//--------------------------------------------------------------------------------------------//
		//					LUPO
		//--------------------------------------------------------------------------------------------//
		
		private var text_erreur_title					:String = ResourceManager.getInstance().getString('M24', 'Consoview');
		private var text_erreur_getLastAboAssociated	:String = ResourceManager.getInstance().getString('M24', 'R_cup_ration_des_abonnements_associ_s_im');
		private var text_erreur_associate				:String = ResourceManager.getInstance().getString('M24', 'Affectation_par_d_faut_impossible');
		private var text_erreur_disociate				:String = ResourceManager.getInstance().getString('M24', 'D_saffectation_impossible');
		private var text_erreur_addObligatoire 			:String = ResourceManager.getInstance().getString('M24', 'Erreur');
		private var text_erreur_remObligatoire			:String = ResourceManager.getInstance().getString('M24', 'Erreur');
		
		
		private var text_valid_associate				:String = ResourceManager.getInstance().getString('M24', 'Affectation_effectu_s');
		private var text_valid_disociate				:String = ResourceManager.getInstance().getString('M24', 'D_saffectation_effectu_s');		
		private var text_valid_addObligatoire			:String = ResourceManager.getInstance().getString('M24', 'Abonnement_Option_s__obligatoire');
		private var text_valid_remObligatoire			:String = ResourceManager.getInstance().getString('M24', 'Abonnement_Option_s__non_obligat');

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function AboOptService()
		{
		}
		
	//--------------------------------------------------------------------------------------------//
	//					PUBLIC METHODE
	//--------------------------------------------------------------------------------------------//	
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//	
		
		public function getTypeCommande(listeIDEquipement:String, idsegment:int, idracine:int, idoperateur:int, isabo:int):void
		{
			TYPECOMMANDE = new ArrayCollection();
			
			_listeRessources = listeIDEquipement;
			_idTypeTheme	 = isabo;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M24.Abonnement",
																				"getTypeCommande",
																				getTypeCommandeResultHandler);
			
			RemoteObjectUtil.callService(op,listeIDEquipement,
											idsegment,
											idracine,
											idoperateur,
											isabo);
		}
		
		public function getLastAssociated(idsabonnement:String, idracine:int):void
		{
			ABO_ASSOCIATED = new ArrayCollection();
			
			var ressources:Array = formatInArray(idsabonnement);
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M24.Abonnement",
																				"getLastAboAssociated",
																				getLastAboAssociatedResultHanler);		
			RemoteObjectUtil.callService(op,ressources,
											idracine);
			
		}
		
		public function getassociatedabo(idsTypeCommande:Array, idoperateur:int):void
		{
			ABONNEMENT = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M24.Abonnement",
																				"getassociatedabo_options",
																				getassociatedabo_optionsResultHanler);		
			RemoteObjectUtil.callService(op,idsTypeCommande,
											idoperateur);
			
		}
		
		public function associateabo_options(idressoureces:String, idtypecommande:int):void
		{
			INTEGER_RESULT 	 = -1;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M24.Abonnement",
																				"associateabo_options",
																				associateabo_optionsResultHanler);		
			RemoteObjectUtil.callService(op,idressoureces,
											idtypecommande);
		}
		
		public function removeabo_options(idressoureces:String, idtypecommande:int):void
		{
			INTEGER_RESULT 	 = -1;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M24.Abonnement",
																				"removeabo_options",
																				removeabo_optionsResultHanler);		
			RemoteObjectUtil.callService(op,idressoureces,
											idtypecommande);
		}
		
		public function obligatoireabo_options(idressoureces:String, idtypecommande:int):void
		{
			INTEGER_RESULT 	 = -1;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M24.Abonnement",
																				"obligatoireabo_options",
																				obligatoireabo_optionsResultHanler);		
			RemoteObjectUtil.callService(op,idressoureces,
											idtypecommande);
		}
		
		public function removeobligatoireabo_options(idressoureces:String, idtypecommande:int, isMessageVisible:Boolean = true):void
		{
			_isMsgVisible = isMessageVisible;
			
			INTEGER_RESULT 	 = -1;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M24.Abonnement",
																				"removeobligatoireabo_options",
																				removeobligatoireabo_optionsResultHanler);
			RemoteObjectUtil.callService(op,idressoureces,
											idtypecommande);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					PRIVATE METHODE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					RETOUR DE PROCEDURE
		//--------------------------------------------------------------------------------------------//
		
		private function getTypeCommandeResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var values	:Array = re.result as Array;
				var len		:int = values.length;
				
				for(var i:int = 0;i < len;i++)
				{
					TYPECOMMANDE.addItem(formatToTypeAboOpt(values[i], _idTypeTheme, _listeRessources));
				}
				
				dispatchEvent(new Event(ABOOPT_TYPECOMMANDE));
			}
			else
				showalert(true, text_erreur_getLastAboAssociated, text_erreur_title);
		}
		
		private function getLastAboAssociatedResultHanler(re:ResultEvent):void 
		{
			if(re.result)
			{
				var values	:Array = re.result as Array;
				var len		:int = values.length;
				
				for(var i:int = 0;i < len;i++)
				{
					ABO_ASSOCIATED.addItem(formatToTypeAboOpt(values[i], _idTypeTheme));
				}
				
				dispatchEvent(new Event(ABOOPT_LASTABOASSOCIATED));
			}
			else
				showalert(true, text_erreur_getLastAboAssociated, text_erreur_title);
		}
		
		private function getassociatedabo_optionsResultHanler(re:ResultEvent):void 
		{
			if(re.result)
			{
				ABONNEMENT = new ArrayCollection(re.result as Array);
				
				dispatchEvent(new Event(ABOOPT_ABONNEMENT));
			}
		}
		
		private function associateabo_optionsResultHanler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result > 0)
				{
					INTEGER_RESULT = re.result as int;
					showalert(false, text_valid_associate, text_erreur_title);
					dispatchEvent(new Event(ABOOPT_ABOOPTASSOCIATED));
				}
				else
					showalert(true, text_erreur_associate, text_erreur_title);
			}
			else
				showalert(true, text_erreur_associate, text_erreur_title);
		}
		
		private function removeabo_optionsResultHanler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result > 0)
				{
					INTEGER_RESULT = re.result as int;
					showalert(false, text_valid_disociate, text_erreur_title);
					dispatchEvent(new Event(ABOOPT_ABOOPTASSOCIATED));
				}
				else
					showalert(true, text_erreur_disociate, text_erreur_title);
			}
			else
				showalert(true, text_erreur_disociate, text_erreur_title);
		}
		
		private function obligatoireabo_optionsResultHanler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result > 0)
				{
					INTEGER_RESULT = re.result as int;
					
					showalert(false, text_valid_addObligatoire, text_erreur_title);
					
					dispatchEvent(new Event(ABOOPT_ABOOPTOBLIGATOIRE));
				}
				else
					showalert(true, text_erreur_addObligatoire, text_erreur_title);
			}
			else
				showalert(true, text_erreur_addObligatoire, text_erreur_title);
		}
		
		private function removeobligatoireabo_optionsResultHanler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result > 0)
				{
					INTEGER_RESULT = re.result as int;
					
					if(_isMsgVisible)
						showalert(false, text_valid_remObligatoire, text_erreur_title);
					
					dispatchEvent(new Event(ABOOPT_ABOOPTOBLIGATOIRE));
				}
				else
					if(_isMsgVisible)
						showalert(true, text_erreur_remObligatoire, text_erreur_title);
			}
			else
				if(_isMsgVisible)
					showalert(true, text_erreur_remObligatoire, text_erreur_title);
		}

		//--------------------------------------------------------------------------------------------//
		//					FONCTION
		//--------------------------------------------------------------------------------------------//
		
		private function formatToTypeAboOpt(value:Object, idthemeType:int, listeIds:String = ''):TypeAboOpt
		{
			var tAboOpt:TypeAboOpt 		 = new TypeAboOpt();
				tAboOpt.CODE_INTERNE 	 = value.CODE_INTERNE;
				tAboOpt.LIBELLE_PROFIL 	 = value.LIBELLE_PROFIL;
				tAboOpt.IDPROFIL 		 = value.IDPROFIL_EQUIPEMENT;
				tAboOpt.IDRACINE 		 = value.IDRACINE;
				tAboOpt.IDTHEMETYPE		 = idthemeType;
				tAboOpt.LISTE_RESSOURCES = listeIds;
				
				if(value.hasOwnProperty('OBLIGATOIRE'))
					tAboOpt.OBLIGATOIRE	= value.OBLIGATOIRE;
			
			return tAboOpt;
		}
		
		private function formatInClob(values:Array):String
		{
			var dataInClob:String = "";
			
			for(var i:int = 0; i < values.length;i++)
			{
				if(i == 0)
					dataInClob = values[i];
				else
					dataInClob = dataInClob + values[i];
				
				if(i != values.length-1)
					dataInClob = dataInClob + ",";
			}
			
			return dataInClob;
		}
		
		private function formatInArray(values:String):Array
		{
			return values.split(',');
		}
		
		private function showalert(isoneerror:Boolean, text:String, title:String, action:Function = null):void
		{
			if(isoneerror)
				ConsoviewAlert.afficherAlertInfo(text, title, action);
			else
				ConsoviewAlert.afficherOKImage(text);

		}
		
	}
}