package catalogue.strategie.strategieconcrete
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import catalogue.event.CatalogueEvent;
	import catalogue.vo.AbonnementVO;
	import catalogue.vo.ClasseVO;
	import catalogue.vo.ClientCatalogueVO;
	import catalogue.vo.ConstructeurVO;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.ParametreSearchCatalogueVO;
	import catalogue.vo.RevendeurVO;
	import catalogue.vo.TypeEquipementVO;
	
	import composants.util.DateFunction;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;

	[Bindable]
	public class AbstractCatalogueCS extends EventDispatcher
	{
		/**
		 *
		 *Cette classe contient les services utilisés par plusieurs catalogues 
		 * 
		 */
		private var _listeConstructeur:ArrayCollection = new ArrayCollection();
		private var _listeClient:ArrayCollection = new ArrayCollection;
		private var _listeRevendeur:ArrayCollection = new ArrayCollection();
		private var _listeTypeEquipement:ArrayCollection  = new ArrayCollection();
		private var _listeClasseEquipement:ArrayCollection =new ArrayCollection();
		private var _listeAbonnementCatalogue:ArrayCollection = new ArrayCollection();
		private var _listeTypeCommande:ArrayCollection = new ArrayCollection();
		
		
		public function AbstractCatalogueCS(target:IEventDispatcher=null)
		{
			super(target);
		}
		public function updateNiveauAbo(idAbo : int,idClient : int, niveau : int):void
		{
			
		}
		public function removeAbonnement(idClient : int,abo : AbonnementVO):void
		{
			
		}
		
		public function addEquipementRev(equip:EquipementCatalogueVO,listClt:ArrayCollection):void
		{
				var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																						"fr.consotel.consoview.M24.v2.Revendeur", 
																						"addEquipementRevendeur", 
																						addEquipement_handler, null);
				
				RemoteObjectUtil.callService(opData, 
												equip.MODELE, 
												equip.REFCONSTRUCTEUR, 
												equip.REFDISTRIBUTEUR, 
												equip.IDCONSTRUCTEUR,
												equip.IDTYPE, 
												equip.IDREVENDEUR,
												equip.ADDINCATCONSTR, 
												Number(equip.PRIX_CATALOGUE), 
												Number(equip.PRIX_1), 
												Number(equip.PRIX_2), 
												Number(equip.PRIX_3), 
												Number(equip.PRIX_4),
												Number(equip.PRIX_5), 
												Number(equip.PRIX_6), 
												Number(equip.PRIX_7), 
												Number(equip.PRIX_8),
												DateFunction.formatDateAsInverseString(equip.DATEVALIDITE),
												Number(equip.DELAIALERT),
												Number(equip.SUSPENDU),
												listClt,
												equip.PATH_LOW,
												equip.PATH_THUMB,
												equip.COMMENT,
												equip.REFERENCE,
												equip.imageByUser);
		}
		
		public function updateEquipementRev(equip:EquipementCatalogueVO,listClt:ArrayCollection):void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																					"fr.consotel.consoview.M24.v2.Revendeur", 
																					"updateEquipementRevendeur", 
																					updateEquipement_handler, null);
			
			RemoteObjectUtil.callService(opData, 
											equip.IDEQUIPEMENT, 
											equip.MODELE, 
											equip.REFCONSTRUCTEUR, 
											equip.REFDISTRIBUTEUR, 
											equip.IDTYPE,
											Number(equip.PRIX_1), 
											Number(equip.PRIX_2), 
											Number(equip.PRIX_3), 
											Number(equip.PRIX_4),
											Number(equip.PRIX_5), 
											Number(equip.PRIX_6), 
											Number(equip.PRIX_7), 
											Number(equip.PRIX_8),
											equip.IDCONSTRUCTEUR, 
											Number(equip.PRIX_CATALOGUE),
											DateFunction.formatDateAsInverseString(equip.DATEVALIDITE),
											Number(equip.DELAIALERT),
											Number(equip.SUSPENDU),
											listClt,
											equip.PATH_LOW,
											equip.PATH_THUMB,
											equip.COMMENT,
											equip.REFERENCE,
											equip.imageByUser);
		}
		
		public function rechercheContructeur(addItemAll : Boolean = false):void
		{ 
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Constructeur",
							"listeConstructeurRacine",
							listeConstructeurRacine_handler,null);
			
			RemoteObjectUtil.callService(opData,addItemAll);
		}
		
		public function rechercheClient(idDistributeur : Number):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"getClientOfDistributeur",
							rechercheClient_handler,null);
			
			RemoteObjectUtil.callService(opData,idDistributeur);
		}
		public function rechercheRevendeur(idClient : Number = -1):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Revendeur",
							"getAllRevendeur",
							rechercheRevendeur_handler,null);
			
			RemoteObjectUtil.callService(opData,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,"",CvAccessManager.getSession().USER.CLIENTACCESSID,0);
		}
		public function updateNiveau(idEquip : int,idClient : int, niveau : int):void
		{
				
		}
		public function rechercheTypeEquipement(idCategorie : int=-1,addItemAll : Boolean=false):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.CatalogueService",
							"listeTypeEquipement",
							rechercheTypeEquipement_handler,null);
			
			RemoteObjectUtil.callService(opData,idCategorie,addItemAll);
		}
		public function rechercheClasseEquipement(addItemAll : Boolean=false):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.CatalogueService",
							"listeClasseEquipement",
							rechercheClasseEquipement_handler,null);
			
			RemoteObjectUtil.callService(opData,addItemAll);
		}
		
		public function rechercheAbonnementCatalogue(parametresRechercheVO : ParametreSearchCatalogueVO, itemIntervalleVO : ItemIntervalleVO):void
		{
			/*
			impl Interface Icat
			*/
		}
		public function rechercheTypeCommande():void
		{
			
		}
		
		private function addEquipement_handler(evt:ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.ADD_EQUIPEMENT_COMPLETE, true, false, evt.result));
		}
		
		private function updateEquipement_handler(evt:ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.UPDATE_EQUIPEMENT_COMPLETE, true, false, evt.result));
		}
		private function rechercheClasseEquipement_handler(re : ResultEvent):void
		{
			var boolAddItemAll : Boolean = re.token.message.body[0];
			
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeClasseEquipement.removeAll();
			
			if(boolAddItemAll){
			var  specialItem : ClasseVO = new ClasseVO();
			specialItem.ID_CLASSE=-1;
			specialItem.LIBELLE_CLASSE=ResourceManager.getInstance().getString('M24','Tous');
			listeClasseEquipement.addItem(specialItem);
			}
			
			var  item : ClasseVO;
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				item = new ClasseVO();
				item.ID_CLASSE=tmpArr[i].IDCATEGORIEEQUIPEMENT;
				item.LIBELLE_CLASSE=tmpArr[i].LIBELLECATEGORIEEQUIPEMENT;
				listeClasseEquipement.addItem(item);
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE,true));
		}
		private function rechercheTypeEquipement_handler(re : ResultEvent):void
		{
			var boolAddItemAll : Boolean = re.token.message.body[1];
			
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeTypeEquipement.removeAll();
			
			if(boolAddItemAll){
				var  specialitem : TypeEquipementVO = new TypeEquipementVO();
				specialitem.ID_TYPE=-1;
				specialitem.LIBELLE_TYPE=ResourceManager.getInstance().getString('M24','Tous');
				listeTypeEquipement.addItem(specialitem);
			}
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : TypeEquipementVO = new TypeEquipementVO();
				item.ID_TYPE=tmpArr[i].IDTYPEEQUIPEMENT;
				item.LIBELLE_TYPE=tmpArr[i].LIBELLETYPEEQUIPEMENT;
				listeTypeEquipement.addItem(item);
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_TYPE_EQUIPEMENT_COMPLETE,true));
		}
		public function listeConstructeurRacine_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeConstructeur = new ArrayCollection();
			
			var boolAddItemAll : Boolean = re.token.message.body[0];
			if(boolAddItemAll){
				var  specialitem : ConstructeurVO = new ConstructeurVO();
				specialitem.ID_CONSTRUCTEUR=-1;
				specialitem.LIBELLE_CONSTRUCTEUR=ResourceManager.getInstance().getString('M24','Tous');
				listeConstructeur.addItem(specialitem);
			}
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : ConstructeurVO = new ConstructeurVO();
				item.ID_CONSTRUCTEUR=tmpArr[i].IDCONSTRUCTEUR;
				item.LIBELLE_CONSTRUCTEUR=tmpArr[i].LIBELLECONSTRUCTEUR;
				listeConstructeur.addItem(item);
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_CONSTRUCTEUR_EQUIPEMENT_COMPLETE,true));
		}
		protected function rechercheRevendeur_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeRevendeur = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : RevendeurVO = new RevendeurVO();
		/*		item.ID_REVENDEUR=tmpArr[i].IDFOURNISSEUR;
				item.LIBELLE_REVENDEUR=tmpArr[i].NOM_FOURNISSEUR;*/
				
				item.ID_REVENDEUR=tmpArr[i].IDREVENDEUR;
				item.LIBELLE_REVENDEUR=tmpArr[i].LIBELLE;
				item.SYMBOL_DEVISE=tmpArr[i].DEVISE;
				//item.SYMBOL_DEVISE=tmpArr[i].DEVISE;
				listeRevendeur.addItem(item);
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_REVENDEUR_EQUIPEMENT_COMPLETE,true));
		}
		public function rechercheClient_handler(re : ResultEvent):void
		{
		
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeClient = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : ClientCatalogueVO = new ClientCatalogueVO();
						
				item.ID_CLIENT=tmpArr[i].IDGROUPE_CLIENT;
				item.LIBELLE_CLIENT=tmpArr[i].LIBELLE_GROUPE_CLIENT ;
				listeClient.addItem(item);
			}
			
			
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_CLIENT_DISTRIBUTEUR));
		
		}
		public function set listeConstructeur(value:ArrayCollection):void
		{
			_listeConstructeur = value;
		}

		public function get listeConstructeur():ArrayCollection
		{
			return _listeConstructeur;
		}

		/*public function set listeTypeEquipement(value:ArrayCollection):void
		{
			_listeTypeEquipement = value;
		}*/

		public function get listeTypeEquipement():ArrayCollection
		{
			return _listeTypeEquipement;
		}
		/*public function set listeClasseEquipement(value:ArrayCollection):void
		{
			_listeClasseEquipement = value;
		}*/

		public function get listeClasseEquipement():ArrayCollection
		{
			return _listeClasseEquipement;
		}
		public function set listeRevendeur(value:ArrayCollection):void
		{
			_listeRevendeur = value;
		}

		public function get listeRevendeur():ArrayCollection
		{
			return _listeRevendeur;
		}
		public function set listeClient(value:ArrayCollection):void
		{
			_listeClient = value;
		}

		public function get listeClient():ArrayCollection
		{
			return _listeClient;
		}
		public function set listeAbonnementCatalogue(value:ArrayCollection):void
		{
			_listeAbonnementCatalogue = value;
		}
		public function get listeAbonnementCatalogue():ArrayCollection
		{
			return _listeAbonnementCatalogue;
		}
		public function get listeTypeCommande():ArrayCollection
		{
			return _listeTypeCommande;
		}
		public function set listeTypeCommande(value:ArrayCollection):void
		{
			_listeTypeCommande = value;
		}
	}
}