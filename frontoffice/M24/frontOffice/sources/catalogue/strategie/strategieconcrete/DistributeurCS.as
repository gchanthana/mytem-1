package catalogue.strategie.strategieconcrete
{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import catalogue.event.CatalogueEvent;
	import catalogue.strategie.ICatalogue;
	import catalogue.vo.AbonnementVO;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.ParametreSearchCatalogueVO;
	
	import composants.util.DateFunction;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;

	[Bindable]
	public class DistributeurCS extends AbstractCatalogueCS implements ICatalogue
	{
		/*
		   import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
		   import mx.rpc.AbstractOperation;
		 */

		private var _listeEquipementCatalogue:ArrayCollection;
		private var _listeImageLibrary:ArrayCollection;
		
		public function DistributeurCS()
		{
		}
		override public function rechercheAbonnementCatalogue(parametresRechercheVO : ParametreSearchCatalogueVO, itemIntervalleVO : ItemIntervalleVO):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Abonnement",
							"searchAboOperateur",
							rechercheAbonnementCatalogue_handler,null);
			
			RemoteObjectUtil.callService(opData,
												parametresRechercheVO.SEARCH_TEXT,
												parametresRechercheVO.ORDER_BY,
												itemIntervalleVO.indexDepart,
												itemIntervalleVO.tailleIntervalle,
												parametresRechercheVO.IDOP
												);
		}
		public function rechercheAbonnementCatalogue_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeAbonnementCatalogue = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : AbonnementVO = new AbonnementVO();
				
				item.IDABO= tmpArr[i].IDABONNEMENT;
				//item.REFERENCE= tmpArr[i].REFERENCE;
				item.TYPE= tmpArr[i].TYPE;
				item.LIBELLE= tmpArr[i].LIBELLE;
				//item.NIVEAU= tmpArr[i].NIVEAU;
				item.NBRECORD= tmpArr[i].NBRECORD;
				item.THEME= tmpArr[i].THEME;    
				       
				listeAbonnementCatalogue.addItem(item);
			}
		}
		public function rechercheListeDataCatalogue(parametresRechercheVO:ParametreSearchCatalogueVO, itemIntervalleVO:ItemIntervalleVO):void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M24.Revendeur", "rechercheEquipementRevendeur", rechercheEquipementRevendeur_handler, null);

			RemoteObjectUtil.callService(opData, parametresRechercheVO.SEARCH_TEXT,
												 parametresRechercheVO.ORDER_BY,
												 itemIntervalleVO.indexDepart,
												 itemIntervalleVO.tailleIntervalle,
												 parametresRechercheVO.IDCONSTRUCTEUR,
												 parametresRechercheVO.IDTYPEEQUIPEMENT,
												 parametresRechercheVO.IDCLASSE,
												 parametresRechercheVO.IDREVENDEUR,
												 parametresRechercheVO.IDSEGMENT,
												 parametresRechercheVO.IDETAT);
		}

		public function addEquipement(equip:EquipementCatalogueVO):void
		{
			
		}

		public function updateEquipement(equip:EquipementCatalogueVO):void
		{
			
		}

		public function removeEquipement(equip:EquipementCatalogueVO):void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M24.CatalogueService", "removeEquipement", removeEquipement_handler, null);

			RemoteObjectUtil.callService(opData, equip.IDEQUIPEMENT);
		}
		
		public function rechercheImageFromLibrary(libelle:String, idType_equipement: Number, idConstructeur: Number):void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M24.v2.Revendeur", "rechercheImageFromLibrary", getImageFromLibrary_handler, null);
			
			RemoteObjectUtil.callService(opData, libelle, idType_equipement, idConstructeur);
		}
		
		private function getImageFromLibrary_handler(re:ResultEvent):void
		{
			var tmpArr:Array=(re.result as ArrayCollection).source;
			listeImageLibrary = new ArrayCollection();
			
		}		

		private function removeEquipement_handler(re:ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.REMOVE_EQUIPEMENT_COMPLETE, true));
		}

		private function addEquipement_handler(evt:ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.ADD_EQUIPEMENT_COMPLETE, true, false, evt.result));
		}

		private function updateEquipement_handler(evt:ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.UPDATE_EQUIPEMENT_COMPLETE, true, false, evt.result));
		}

		private function rechercheEquipementRevendeur_handler(re:ResultEvent):void
		{
			var tmpArr:Array=(re.result as ArrayCollection).source;
			listeDataCatalogue=new ArrayCollection();
		
			for (var i:int=0; i < tmpArr.length; i++)
			{
				var item:EquipementCatalogueVO=new EquipementCatalogueVO();
				item.IDEQUIPEMENT=tmpArr[i].IDEQUIPEMENT;
				item.REFERENCE=tmpArr[i].REFERENCE;
				item.REFCONSTRUCTEUR=tmpArr[i].REFERENCECONSTRUCTEUR;
				item.REFDISTRIBUTEUR=tmpArr[i].REFERENCEDISTRIBUTEUR;
				item.TYPE=tmpArr[i].TYPE;
				item.CLASSE=tmpArr[i].CLASSE;
				item.TYPE_CREATION = tmpArr[i].TYPE_CREATION;
				item.ORIGINE=(item.TYPE_CREATION == 1)?ResourceManager.getInstance().getString('M24','Manuel'):tmpArr[i].ORIGINE;
				//item.ORIGINE=tmpArr[i].ORIGINE;
				
				item.DATEVALIDITE=tmpArr[i].DATEVALIDITE;
				item.SUSPENDU=tmpArr[i].SUSPENDU;
				item.DELAIALERT=tmpArr[i].DELAIALERT;
				
				item.MODELE=tmpArr[i].MODELE;
				item.NBRECORD=tmpArr[i].NBRECORD;
				item.IDCONSTRUCTEUR=tmpArr[i].IDCONSTRUCTEUR;
				item.IDCLASSE=tmpArr[i].IDCATEGORIE;
				item.IDTYPE=tmpArr[i].IDTYPE;
				item.PRIX_CATALOGUE=Number(tmpArr[i].PRIX_CATALOGUE).toFixed(2);
				item.PRIX_1=Number(tmpArr[i].TARIF12MOIS).toFixed(2);
				item.PRIX_2=Number(tmpArr[i].TARIF24MOIS).toFixed(2);
				item.PRIX_3=Number(tmpArr[i].TARIF36MOIS).toFixed(2);
				item.PRIX_4=Number(tmpArr[i].TARIF48MOIS).toFixed(2);
				item.PRIX_5=Number(tmpArr[i].TARIF12MOISR).toFixed(2);
				item.PRIX_6=Number(tmpArr[i].TARIF24MOISR).toFixed(2);
				item.PRIX_7=Number(tmpArr[i].TARIF36MOISR).toFixed(2);
				item.PRIX_8=Number(tmpArr[i].TARIF48MOISR).toFixed(2);
				item.PATH_LOW = tmpArr[i].PATH_LOW;
				item.PATH_THUMB = tmpArr[i].PATH_THUMB;
				item.COMMENT = tmpArr[i].COMMERCIAL_COMMENT;
				listeDataCatalogue.addItem(item);
			}
		}
		public function getEquipementCatalogue():ArrayCollection
		{
			return new ArrayCollection()
		}

		public function set listeDataCatalogue(value:ArrayCollection):void
		{
			_listeEquipementCatalogue=value;
		}

		public function get listeDataCatalogue():ArrayCollection
		{
			return _listeEquipementCatalogue;
		}
		
		public function get listeImageLibrary():ArrayCollection { return _listeImageLibrary; }
		
		public function set listeImageLibrary(value:ArrayCollection):void
		{
			if (_listeImageLibrary == value)
				return;
			_listeImageLibrary = value;
		}
	}
}