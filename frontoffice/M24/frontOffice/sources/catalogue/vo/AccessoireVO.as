package catalogue.vo
{
	public class AccessoireVO
	{
		private var _idAccessoire:int;
		private var _libelleAccessoire:String;
		private var _codeAccessoire:String;
		public var ETAT : int;
		
		public function AccessoireVO()
		{
		}


		public function set idAccessoire(value:int):void
		{
			_idAccessoire = value;
		}

		public function get idAccessoire():int
		{
			return _idAccessoire;
		}
		public function set libelleAccessoire(value:String):void
		{
			_libelleAccessoire = value;
		}

		public function get libelleAccessoire():String
		{
			return _libelleAccessoire;
		}
		public function set codeAccessoire(value:String):void
		{
			_codeAccessoire = value;
		}

		public function get codeAccessoire():String
		{
			return _codeAccessoire;
		}
		
	}
}