package catalogue.vo
{
	public class ClasseVO
	{
		private var _ID_CLASSE:Number;
		private var _LIBELLE_CLASSE:String;
		
		public function ClasseVO()
		{
			
		}

		public function set ID_CLASSE(value:Number):void
		{
			_ID_CLASSE = value;
		}
		public function get ID_CLASSE():Number
		{
			return _ID_CLASSE;
		}
		public function set LIBELLE_CLASSE(value:String):void
		{
			_LIBELLE_CLASSE = value;
		}

		public function get LIBELLE_CLASSE():String
		{
			return _LIBELLE_CLASSE;
		}
	}
}