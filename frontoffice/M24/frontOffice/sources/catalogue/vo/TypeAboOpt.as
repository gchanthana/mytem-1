package catalogue.vo
{
	public class TypeAboOpt
	{
		
		public var CODE_INTERNE		:String = '';
		public var LIBELLE_PROFIL	:String = '';
		public var LISTE_RESSOURCES	:String = '';
		
		public var IDPROFIL			:int = 0;
		public var IDRACINE			:int = 0;
		public var IDTHEMETYPE		:int = 0;
		public var ETAT_PROFIL		:int = 0;
		public var ETAT_OBLIGA		:int = 0;
		public var OBLIGATOIRE		:int = 0;
		public var IDRESSOURCE		:int = 0;
		
		public var ISPRESENT		:Boolean = false;
		public var PROFIL_SELECTED	:Boolean = false;
		public var OBLIGA_SELECTED	:Boolean = false;
		
		public function TypeAboOpt()
		{
		}
	}
}
