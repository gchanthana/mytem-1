package catalogue.vo
{
	[Bindable]
	public class TypeCommandeVO
	{
		private var _idTypeCmd:int;
		private var _libelleTypeCmd:String;
		private var _codeTypeCmd:String;
		private var _commentaire:String;
		private var _nbPool:int;
		private var _nbGestionnaire:int;
		private var _isInPool:Boolean;
		private var _boolSelected:Boolean;
		
		public var ETAT : int;

		
		public function TypeCommandeVO()
		{
			
		}
		public function set idTypeCmd(value:int):void
		{
			_idTypeCmd = value;
		}

		public function get idTypeCmd():int
		{
			return _idTypeCmd;
		}
		public function set libelleTypeCmd(value:String):void
		{
			_libelleTypeCmd = value;
		}

		public function get libelleTypeCmd():String
		{
			return _libelleTypeCmd;
		}
		public function set codeTypeCmd(value:String):void
		{
			_codeTypeCmd = value;
		}

		public function get codeTypeCmd():String
		{
			return _codeTypeCmd;
		}
		public function set commentaire(value:String):void
		{
			_commentaire = value;
		}

		public function get commentaire():String
		{
			return _commentaire;
		}
		public function set nbPool(value:int):void
		{
			_nbPool = value;
		}

		public function get nbPool():int
		{
			return _nbPool;
		}
		public function set nbGestionnaire(value:int):void
		{
			_nbGestionnaire = value;
		}

		public function get nbGestionnaire():int
		{
			return _nbGestionnaire;
		}
		public function set isInPool(value:Boolean):void
		{
			_isInPool = value;
		}

		public function get isInPool():Boolean
		{
			return _isInPool;
		}
		public function set boolSelected(value:Boolean):void
		{
			_boolSelected = value;
		}

		public function get boolSelected():Boolean
		{
			return _boolSelected;
		}
	}
}
		