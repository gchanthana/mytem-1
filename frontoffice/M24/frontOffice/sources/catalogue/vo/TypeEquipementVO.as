package catalogue.vo
{
	public class TypeEquipementVO
	{
		private var _ID_TYPE:Number;
		private var _LIBELLE_TYPE:String;
		public function TypeEquipementVO()
		{
		}


		public function set ID_TYPE(value:Number):void
		{
			_ID_TYPE = value;
		}

		public function get ID_TYPE():Number
		{
			return _ID_TYPE;
		}
		public function set LIBELLE_TYPE(value:String):void
		{
			_LIBELLE_TYPE = value;
		}

		public function get LIBELLE_TYPE():String
		{
			return _LIBELLE_TYPE;
		}
	}
}