
package catalogue.vo
{
	import flash.utils.ByteArray;
	import catalogue.util.Formator;
	

	[RemoteClass(alias="fr.consotel.consoview.M24.vo.FileUploadVo")]
	
	[Bindable]
	public class FileUpload
	{
		
		//--------------------------------------------------------------------------------------------//
		//					PROPRIETEES
		//--------------------------------------------------------------------------------------------//
		
		public var fileData			:ByteArray = new ByteArray();
		public var fileSelected		:Boolean = false;
		public var fileName			:String = '';
		public var fileExt			:String = '';
		public var fileUUID			:String = '';
		public var fileBy			:String = '';
		public var fileDateStrg		:String = '';
		public var fileDate			:Date = new Date();
		public var fileSize			:Number = 0;
		public var fileId			:int = 0;
		public var fileIdRacine		:int = 0;
		public var fileJoin			:int = 1;
		public var fileRename		:int = 0;
		public var fileRemove		:int = 0;
		public var fileIdCommande	:int = 0;
		// for image files
		public var imageType			:String = '';
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function FileUpload()
		{
			this.fileId 		= Math.random()*1000000;
			this.fileIdRacine	= CvAccessManager.getUserObject().CLIENTACCESSID;
			this.fileBy 		= CvAccessManager.getUserObject().PRENOM + '_' + CvAccessManager.getUserObject().NOM;
			this.fileDateStrg 	= Formator.formatDate(this.fileDate);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC STATIC FUNCTION
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					MAPPING
			//--------------------------------------------------------------------------------------------//
		
		public static function mappingObjectToFileUpload(value:Object):FileUpload
		{
			var currentFile:FileUpload 		= new FileUpload();
				currentFile.fileData		= value.fileData;
				currentFile.fileSelected	= value.fileSelected;
				currentFile.fileName		= value.fileName;
				currentFile.fileExt			= value.fileExt;
				currentFile.fileUUID		= value.fileUUID;
				currentFile.fileBy			= value.fileBy;
				currentFile.fileDateStrg	= value.fileDateStrg;
				currentFile.fileDate		= value.fileDate;
				currentFile.fileSize		= value.fileSize;
				currentFile.fileId			= value.fileId;
				currentFile.fileIdRacine	= value.fileIdRacine;
				currentFile.fileJoin		= value.fileJoin;
				currentFile.fileRename		= value.fileRename;
				currentFile.fileRemove		= value.fileRemove;
				currentFile.fileIdCommande	= value.fileIdCommande;
				if(value.hasOwnProperty('imageType') && value.hasOwnProperty('imageType'))
					currentFile.imageType 		= value.imageType;

			return currentFile;
		}
		
	}
}