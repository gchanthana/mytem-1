package catalogue.util
{
	/**
	 * Cette classe gère la devise d'origine du catalogue client et sa syntaxe.
	 */
	public class CatalogueFormatteur
	{
		
		private  var _operatorCurrency:Currency = new Currency("€");		
		private  var _distributorCurrency:Currency = new Currency("€");		
		
		public function CatalogueFormatteur()
		{
			if(_instance){
				throw new Error("Singleton... use getInstance()");
			} 
			_instance = this;
		}
		
		public function get operatorCurrency():Currency
		{
			return this._operatorCurrency;
		}
		
		public function set operatorCurrency(value:Currency):void
		{
			if(value != null){
				this._operatorCurrency = value;
			}
			
		}		
		
		
		
		
		
		public function get distributorCurrency():Currency
		{
			return this._distributorCurrency;
		}
		
		public function set distributorCurrency(value:Currency):void
		{
			if(value != null){
				this._distributorCurrency = value;
			}
		}
		
		
		
		
		
		
		
		/////////// STATIC  ///////////////////////////////////////////////////////////	
		
		private static var _instance:CatalogueFormatteur
		
		public static function getInstance():CatalogueFormatteur
		{
			if (!_instance)
			{
				_instance= new CatalogueFormatteur();
			}
			return _instance;
		}
	}
}