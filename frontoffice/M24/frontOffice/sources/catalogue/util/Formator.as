package catalogue.util
{	
import flash.display.BitmapData;
import flash.geom.Matrix;

import mx.formatters.DateFormatter;
	
	public class Formator
	{
		//----------- METHODES -----------//
		
		public function Formator()
		{
		}
		
		/* convertir octets en megasOctets */
		public static function formatOctetsToMegaOctets(size:Number):String
		{
			var value:Number = size/1048576;
			
			return value.toPrecision(2);
		}
		
		/* formater la date */
		public static function format(str_dateString:String, str_dateFormat:String):String
		{
			var df:DateFormatter= new DateFormatter();
			df.formatString = str_dateFormat;
			return df.format(str_dateString);
		}
	
	    public static function formatDate(dateobject:Object):String
		{
			var day:String = dateobject.date;
			
			if(day.length < 2)
				day = "0" + day;
			
			var temp	:int = Number(dateobject.month) + 1;
			var month	:String = temp.toString();
			
			if(month.length < 2)
				month = "0" + month;
			
			var year:String = dateobject.fullYear;
			
			return day + "/" + month + "/" + year;
		}
		
		
		/**
		 * @public
		 * Utility function used for higher quality image scaling. Essentially we
		 * simply step down our bitmap size by half resulting in a much higher result
		 * though taking potentially multiple passes to accomplish.
		 * from web : http://stackoverflow.com/questions/6623529/resize-images-in-flex-using-as3
		 * source spark.primitives.BitmapImage
		 */
		public static function resizeBitmap(bitmapData:BitmapData, newWidth:uint, newHeight:uint):BitmapData
		{
			
			var finalScale:Number = Math.min(newWidth/bitmapData.width, newHeight/bitmapData.height);
			
			var finalData:BitmapData = bitmapData;
			
			if (finalScale > 1)
			{
				finalData = new BitmapData(bitmapData.width * finalScale, bitmapData.height * finalScale, true, 0);
				finalData.draw(bitmapData, new Matrix(finalScale, 0, 0, finalScale), null, null, null, true);
				
				return finalData;
			}
			
			var drop:Number = 0.5;
			var initialScale:Number = finalScale;
			
			while (initialScale/drop < 1)
				initialScale /= drop;
			
			var w:Number = Math.floor(bitmapData.width * initialScale);
			var h:Number = Math.floor(bitmapData.height * initialScale);
			var bd:BitmapData = new BitmapData(w, h, bitmapData.transparent, 0);
			
			bd.draw(finalData, new Matrix(initialScale, 0, 0, initialScale), null, null, null, true);
			
			finalData = bd;
			
			for (var scale:Number = initialScale * drop; Math.round(scale * 1000) >= Math.round(finalScale * 1000);	scale *= drop)
			{
				w = Math.floor(bitmapData.width * scale);
				h = Math.floor(bitmapData.height * scale);
				bd = new BitmapData(w, h, bitmapData.transparent, 0);
				
				bd.draw(finalData, new Matrix(drop, 0, 0, drop), null, null, null, true);
				
				finalData.dispose();
				finalData = bd;
			}
			
			return finalData;
		}
		
		public static function scaleBitmap(src: BitmapData, newWidth: Number, newHeight: Number): BitmapData
		{
			var ratioW:Number = newWidth / src.width ;
			var ratioH:Number = newHeight / src.height;
			trace("# ratioW : " + ratioW);
			trace("# ratioH : " + ratioH);
			var bmd: BitmapData = new BitmapData(src.width*ratioW, src.height*ratioH);
			var mtrx: Matrix = new Matrix();   
			mtrx.scale(ratioW, ratioH);
			bmd.draw(src, mtrx);
			return bmd;
		}

	}
}