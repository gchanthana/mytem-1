package composants.access.perimetre.tree {
	import composants.access.perimetre.PerimetreTreeItemRenderer;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.core.ClassFactory;
	import mx.core.IFactory;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class SearchPerimetreWindowImpl extends SearchPerimetreWindow implements ISearchPerimetreWindow {
		private var itemRenderer:IFactory;
		private var startFromNodeId:int; // Noeud à partir duquel la recherche commence (racine du résultat)
		private var currentKeyword:String; // Mot clé à rechercher
		private var resultArray:ArrayCollection; // Liste des noeuds vérifiant le critère de recherche
		private var currentResultIndex:int; // Index du Noeud résultat actuellement affiché
		private var currentPath:XML; // Path XML du résultat actuel
		
		
		public static const NODE_SELECTED : String = "nodeSelected";
		
		//-- RAJOUT SAMUEL --------pour le changement de périmetre depuis la recherche// 
		public static const NODE_DD_CLICKED : String = "nodeDoubleClicked";
		//--- FIN RAJOUT SAMUEL ----------------//
		
		public function SearchPerimetreWindowImpl() {
			super();
			trace("(SearchPerimetreWindowImpl) Instance Creation");
			itemRenderer = new ClassFactory(composants.access.perimetre.PerimetreTreeItemRenderer);
			currentKeyword = "";
			resetSearchData();
			addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
		}
		
		private function afterCreationComplete(event:FlexEvent):void {
			trace("(SearchPerimetreWindowImpl) Performing IHM Initialization");
			btnBack.addEventListener(MouseEvent.CLICK,backToPerimetreTree);
		
			previousItem.addEventListener(MouseEvent.CLICK,displayPreviousNode);
			nextItem.addEventListener(MouseEvent.CLICK,displayNextNode);
			searchTree.doubleClickEnabled = true;
			searchTree.addEventListener(MouseEvent.DOUBLE_CLICK,nodeClickedHandler);
			searchTree.itemRenderer = itemRenderer;
			searchTree.labelField = "@LBL";
			
			updateSearchControlItems();
		}

		/**
		 * Updates visual components
		 * */
		private function updateSearchControlItems():void {
			updateSearchText();
			searchIndex.visible = true;
			if(resultArray != null) {
				if(resultArray.length > 0) {
					searchInput.setStyle("color","#000000");
					previousItem.visible = nextItem.visible = (resultArray.length > 1);
					if(resultArray.length > 1)
						searchIndex.text = (currentResultIndex + 1).toString() + "/" + resultArray.length.toString();
					else {
						if(resultArray[0].IDGROUPE_CLIENT > 0)
							searchIndex.text = "1 Résultat";
						else
							searchIndex.text = "";
					}
				} else {
					trace("******************* SearchPerimetreWindow updateSearchControlItems() : Un cas Oublié *******************");
				}
			} else {
					searchInput.setStyle("color","#FF0000");
				searchTree.dataProvider = currentPath;
				
				previousItem.visible = nextItem.visible = false;
				searchIndex.text = "Aucun résultat";
			}
		}

	/* 	public function performSearch(nodeId:int,searchKeyword:String):void {
			trace("(SearchPerimetreWindowImpl) Perform Search on node : " + nodeId + " keyword : " + searchKeyword);
			currentKeyword = searchKeyword;
			updateSearchText();
		} */

		private function updateSearchText():void {
			searchInput.text = "\"" + currentKeyword + "\"";
		}

		private function backToPerimetreTree(event:MouseEvent):void {
			dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.BACK_NODE_SEARCH,currentKeyword));
		}


// ===============================================================================================================
		 
		public function onPerimetreChange():void {
			currentKeyword = "";
			resetSearchData();
			if(this.initialized)
				updateSearchControlItems();
		}

		public function performSearch(nodeId:int,searchKeyword:String):void {
			var searchOp:AbstractOperation;
			if(nodeId != startFromNodeId) { // Nouveau noeud de départ
				startFromNodeId = nodeId;
				currentKeyword = searchKeyword;
				resetSearchData();
				searchOp = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
														"fr.consotel.consoview.access.AccessManager",
														"searchNodes",searchResult);
				RemoteObjectUtil.callService(searchOp,nodeId,searchKeyword);
			} else { // Même noeud de départ que la recherche précédente
				if(currentKeyword.toLowerCase() != searchKeyword.toLowerCase()) { // Mot clé différent
					currentKeyword = searchKeyword;
					resetSearchData();
					searchOp = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
															"fr.consotel.consoview.access.AccessManager",
															"searchNodes",searchResult);
					RemoteObjectUtil.callService(searchOp,nodeId,searchKeyword);
				} else { // Même mot clé
					dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.NODE_SEARCH_RESULT));
				}
			}
		}
		
		private function searchResult(event:ResultEvent):void {
			if((event.result as ArrayCollection).length > 0) {
				resultArray = event.result as ArrayCollection;
				currentResultIndex = 0;
				dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.NODE_SEARCH_RESULT));
				getNodeXmlPath(startFromNodeId,resultArray[currentResultIndex].IDGROUPE_CLIENT);
			} else {
				resetSearchData();
				currentResultIndex = 0;
				if(this.initialized)
					updateSearchControlItems();
				dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.NODE_SEARCH_NO_RESULT));
			}
			if(this.initialized)
				updateSearchControlItems();
		}
		
		public function getNodeXmlPath(rootId:int,nodeId:int):void {
			var getPathOp:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													"fr.consotel.consoview.access.AccessManager",
													"getNodeXmlPath",getNodeXmlPathResult);
			RemoteObjectUtil.callService(getPathOp,
					CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,rootId,nodeId);
		}
		
		private function getNodeXmlPathResult(event:ResultEvent):void {
			currentPath = event.result as XML;
			if(this.initialized) {
				searchTree.dataProvider = currentPath;
				searchTree.labelField = "@LBL";
				searchTree.callLater(selectResultNode);
			}
		}

		protected function selectResultNode():void {
			if (searchTree.selectNodeById(resultArray[currentResultIndex].IDGROUPE_CLIENT))
				dispatchEvent(new Event(NODE_SELECTED));
			if(resultArray[0].IDGROUPE_CLIENT < 0)
				searchTree.dataProvider[0].@LBL = "Trop de résultats - Affinez la recherche";
			updateSearchControlItems();
		}

		private function resetSearchData():void {
			resultArray	= null;
			currentResultIndex = -1;
			currentPath = null;
			if(this.initialized) {
				searchTree.dataProvider = null;
			}
		}

		private function displayNextNode(event:MouseEvent):void {
			if(currentResultIndex == (resultArray.length - 1))
				currentResultIndex = 0;
			else
				currentResultIndex = currentResultIndex + 1;
			previousItem.visible = nextItem.visible = searchIndex.visible = false;
			getNodeXmlPath(startFromNodeId,resultArray[currentResultIndex].IDGROUPE_CLIENT);
		}
		
		private function displayPreviousNode(event:MouseEvent):void {
			if(currentResultIndex == 0)
				currentResultIndex = resultArray.length - 1;
			else
				currentResultIndex = currentResultIndex - 1;
			previousItem.visible = nextItem.visible = searchIndex.visible = false;
			getNodeXmlPath(startFromNodeId,resultArray[currentResultIndex].IDGROUPE_CLIENT);
		}
	 
		
		//--- AJOUTER PAR SAMUEL ---
		private function nodeClickedHandler(me : MouseEvent):void{
			trace("un noeud cliquer");
		 
	
			if(searchTree.selectedItem.@NID !=
					CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX) {
				if(searchTree.selectedItem.@STC > 0) {
					trace("ListePerimetreWindow changePerimetre() :\n" +
							"Noeud : " + searchTree.selectedItem.@LBL +
							"\nId : " + searchTree.selectedItem.@NID +
							"\nSTC : " + searchTree.selectedItem.@STC +
							"\nLast Périmètre : " + CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX.toString());
					dispatchEvent(new Event(NODE_DD_CLICKED));
				}
		
			}
		
		 
		}
		//--- FIN AJOUT -----------
	}
}
