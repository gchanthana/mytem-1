package {
	import appli.control.AppControlEvent;
	import appli.events.ConsoViewEvent;
	import appli.login.LoginEvent;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import styles.ConsoViewStyleManager;
	
	
	public class Login extends LoginIHM {
		private var appVersion:String;
		private var authOp:AbstractOperation;
		private var sendMailOp:AbstractOperation;
		private var authInfos:Object;
		private var infosMessage:String;
		private var currentlogin:String;
		
		public function Login(appVersionValue:String, infosMsg:String) {
			trace("(Login) Instance Creation");
			appVersion = appVersionValue;
			authInfos = null;
			authOp = RemoteObjectUtil.getOperation("fr.consotel.consoview.access.AccessManager",
																		"validateLogin",authHandler);
			sendMailOp = RemoteObjectUtil.getOperation("fr.consotel.consoprod.mail.SendMail",
																		"sendSingleMail",mailSent);
			switch(infosMsg) {
				case AppControlEvent.LOGOFF_EVENT :
					infosMessage = "Déconnexion effectuée avec succès";
					break;
				case ConsoViewEvent.NO_ACCESS_EXCEPTION :
					infosMessage = "Aucun accès paramétré pour l'utilisateur";
					break;
				case ConsoViewEvent.UNABLE_TO_CONNECT :
					infosMessage = "Erreur : Données incohérentes pour le périmètre";
					break;
				case ConsoViewEvent.FAULT_REMOTE_OPERATION :
					infosMessage = "Erreur : 1002";
					break;
				case ConsoViewEvent.EXPIRED_SESSION:
					infosMessage = "Session expirée";
					break;
				default :
					infosMessage = "";
					break;
			}
			this.addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
		}

		public function getAuthInfos():Object {
			return authInfos;
		}

		public function setcurrentlogin(curLogin:String=""):void{
			this.currentlogin= curLogin;
		}

		private function afterCreationComplete(event:FlexEvent):void {
			trace("(Login) Performing IHM Initialization");
//			myLoginPanel.title = appVersion;
			loginMsg.text = infosMessage;
			loginText.text = currentlogin;
			pwdText.text = "";
			loginText.setFocus();
//			btnAcrobat.addEventListener(MouseEvent.CLICK,getAcrobat);
//			btnCnil.addEventListener(MouseEvent.CLICK,getCnil);
//			btnFlash.addEventListener(MouseEvent.CLICK,getFlash);
			btnPwd.addEventListener(MouseEvent.CLICK,askPwd);
			stdConnectButton.addEventListener(MouseEvent.CLICK,connectItemHandler);
			this.addEventListener(KeyboardEvent.KEY_DOWN,connectItemHandler);
			activeControls(true);
			ConsoViewStyleManager.changeStyle("SNCF_SFR");
		}
		
		private function activeControls(state:Boolean):Boolean {
			loginText.enabled = pwdText.enabled =
				stdConnectButton.enabled = btnPwd.enabled = state;
			return state;
		}
		
		private function connectItemHandler(event:Event):void {
			if(event is MouseEvent) {
				if((loginText.text.length == 0) || pwdText.text.length == 0)
					Alert.show("Le login et le mot de passe sont obligatoires","Connexion");
				else
					RemoteObjectUtil.invokeService(authOp,loginText.text,pwdText.text,3);
			} else if(event is KeyboardEvent) {
				if((event as KeyboardEvent).keyCode == 13) {
					if((loginText.text.length == 0) || pwdText.text.length == 0)
						Alert.show("Le login et le mot de passe sont obligatoires","Connexion");
					else
						RemoteObjectUtil.invokeService(authOp,loginText.text,pwdText.text,3);
				}
			}
		}
		
		private function authHandler(event:ResultEvent):void {
			if(event.result['AUTH_RESULT'] == 1) {
				loginMsg.text = "";
				authInfos = event.result;
				trace("(Login) AUTH SUCCESS : " + authInfos['EMAIL'].toString());
				dispatchEvent(new LoginEvent(LoginEvent.VALIDE,authInfos));
			} else if(event.result['AUTH_RESULT'] == 0) {
				authInfos = null;
				loginMsg.text = "Accès Refusé : Login ou Mot de passe invalide(s)";
				trace("(Login) AUTH FAILED : Invalid Credentials");
			} else if(event.result['AUTH_RESULT'] == 2) {
				authInfos = null;
				loginMsg.text = "Accès Refusé : Adresse IP non autorisée";
				trace("(Login) AUTH FAILED : Invalid Ip Address");
			} else {
				authInfos = null;
				loginMsg.text = "Accès Refusé : Erreur Interne";
				trace("(Login) AUTH FAILED : UNKNOWN ERROR");
			}
		}

		private function getAcrobat(event:Event):void {
			var url:String = "http://www.adobe.com/fr/products/acrobat/readstep2.html";
            var variables:URLVariables = new URLVariables();
            var request:URLRequest = new URLRequest(url);
            request.method="GET";
            navigateToURL(request,"_blank");
		}
		
		private function getCnil(event:Event):void {
			var url:String = "http://www.cnil.fr/index.php?id=1777";
            var variables:URLVariables = new URLVariables();
            var request:URLRequest = new URLRequest(url);
            request.method="GET";
            navigateToURL(request,"_blank");
		}
		
		private function getFlash(event:Event):void {
			var url:String = "http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&promoid=BIOW";
            var variables:URLVariables = new URLVariables();
            var request:URLRequest = new URLRequest(url);
            request.method="GET";
            navigateToURL(request,"_blank");
		}
		
		private function askPwd(event:Event):void {
			if (loginText.length==0) {
				Alert.show("Veuillez saisir votre login.","Avertissement");
			} else {
				RemoteObjectUtil.invokeService(sendMailOp,[loginText.text,"support@consotel.fr","brice@consotel.fr",
												"","Consoview v3","Demande de mot de passe",
												loginText.text + " : Oubli du mot de passe ConsoView.","no"]);
			}
		}
		
		private function mailSent(e:ResultEvent):void {
			Alert.show("La demande de mot de passe a été envoyée.");
		}
	}
}
