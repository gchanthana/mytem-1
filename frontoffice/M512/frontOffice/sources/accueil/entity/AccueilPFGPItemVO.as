package accueil.entity
{
	[Bindable] public class AccueilPFGPItemVO
	{
		public var nbLigneMobile:String="0";
		public var nbOuvertureLigne:String="0";
		public var nbResiliation:String="0";
		public var coutParLigne:String="0";
		public var nbLigneRenouvellement:String="0";
		public var oldNbLigneMobile:String="0";
		public var oldNbOuvertureLigne:String="0";
		public var oldNbResiliation:String="0";
		public var oldCoutParLigne:String="0";
		public var oldNbLigneRenouvellement:String="0";
		public var status:int=0;
		
		public function AccueilPFGPItemVO()
		{
		}
		public function defineByDefault():void
		{
			nbLigneMobile = "";
			nbLigneRenouvellement = "";
			nbOuvertureLigne = "";
			nbResiliation = "";
			coutParLigne = "";
			oldNbLigneMobile = "";
			oldNbLigneRenouvellement = "";
			oldNbOuvertureLigne = "";
			oldNbResiliation = "";
			oldCoutParLigne = "";
			status = 0;
		}
		public function defineByProc(obj:Object):void
		{
			this.nbLigneMobile = obj.NBLIGNEMOBILE;
			this.oldNbLigneMobile = obj.OLDNBLIGNEMOBILE;
			
			this.nbOuvertureLigne = obj.NBOUVERTURELIGNE;
			this.oldNbOuvertureLigne = obj.OLDNBOUVERTURELIGNE;
			
			this.nbResiliation = obj.NBRESILIATION;
			this.oldNbResiliation = obj.OLDNBRESILIATION;
			
			this.coutParLigne = obj.COUTMOYENLIGNE;
			this.oldCoutParLigne = obj.OLDCOUTMOYENLIGNE;
			
			this.nbLigneRenouvellement = obj.NBLIGNEELIGIBLE;
			this.oldNbLigneRenouvellement = obj.OLDNBLIGNEELIGIBLE;
			
			this.status = obj.STATUS;
		}
	}
}