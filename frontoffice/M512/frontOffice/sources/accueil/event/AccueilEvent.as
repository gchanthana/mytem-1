package accueil.event
{
	import flash.events.Event;

	public class AccueilEvent extends Event
	{
		public static const ACCUEIL_EVENT:String = "accueilEvent";
		public static const PERIMETRE_CHANGE:String = "perimetreChange";
		public static const PERIMETRE_NOT_CHANGE:String = "perimetreNotChange";				
		public static const PERIMETRE_ERROR:String = "perimetreError";
		public static const PERIMETRE_LOADED:String = 'perimetreLoaded';
		public static const PERIMETRE_LOADED_ERROR:String = 'perimetreLoadedError';
		public static const DATE_LOADED:String = 'DATE_LOADED';
		public static const AE_REMOTE_OP_FAULT_EVENT:String = 'aeRemoteOpfaultEvent';
		public static const INDICATEUR_UPDATED:String = 'indicateurUpdated';
		public static const LASTE_DATE_COMPLETE_UPDATED:String = 'lasteDateCompleteUpdated';
		
		public function AccueilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new AccueilEvent(type,bubbles,cancelable);
		}
		
	}
}