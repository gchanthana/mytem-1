package accueil.ihm.composant.AccueilMenu
{
	import accueil.event.AccueilEvent;
	import accueil.ihm.composant.itemRendererAccueilMenu.ItemRendererAccueilMenuIHM;
	import accueil.service.perimetre.PerimetreService;
	
	import composants.gradientcomponents.GradientBox;
	import composants.gradientcomponents.GradientVBox;
	
	import mx.containers.VBox;
	import mx.events.FlexEvent;
	
	public class AccueilMenuImpl extends GradientVBox
	{
		private var peri:PerimetreService;
		public  var vb:GradientBox;
		
		public function AccueilMenuImpl()
		{
			super();
			peri=new PerimetreService();
			peri.handler.addEventListener(AccueilEvent.PERIMETRE_LOADED,setDataprovider);
			peri.handler.addEventListener(AccueilEvent.PERIMETRE_LOADED_ERROR,setDataprovider);
		}
		private function setDataprovider(e:AccueilEvent):void
		{
			peri.handler.removeEventListener(AccueilEvent.PERIMETRE_LOADED,setDataprovider);
			peri.handler.removeEventListener(AccueilEvent.PERIMETRE_LOADED_ERROR,setDataprovider);
			for each(var x:XML in peri.model.PerimetreXML.children())
			{
				if(x.children().length() > 0)
				{
					var item:ItemRendererAccueilMenuIHM = new ItemRendererAccueilMenuIHM();
					item.XML_UNIVERS = x;
					vb.addChild(item);
				}
			}
			dispatchEvent(new AccueilEvent(AccueilEvent.PERIMETRE_LOADED));
		}
		public function init(e:FlexEvent=null):void
		{
			if(!peri.handler.hasEventListener(AccueilEvent.PERIMETRE_LOADED))
			{
				peri.handler.addEventListener(AccueilEvent.PERIMETRE_LOADED,setDataprovider);
				peri.handler.addEventListener(AccueilEvent.PERIMETRE_LOADED_ERROR,setDataprovider);
			}
			peri.getPerimetre();
		}
		public function reset():void
		{
			peri.model.PerimetreXML = null;
			if(vb.getChildren().length > 0)
				vb.removeAllChildren();
		}
	}
}