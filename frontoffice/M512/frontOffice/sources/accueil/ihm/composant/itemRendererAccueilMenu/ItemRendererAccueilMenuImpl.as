package accueil.ihm.composant.itemRendererAccueilMenu
{
	import composants.gradientcomponents.GradientBox;
	import composants.gradientcomponents.GradientVBox;
	
	import mx.containers.Tile;
	import mx.containers.VBox;
	
	public class ItemRendererAccueilMenuImpl extends GradientVBox
	{
		[Bindable] public var XML_UNIVERS:XML;
		public var tl:Tile;
		
		public function ItemRendererAccueilMenuImpl()
		{
			super();
		}
		protected function init():void
		{
			for each(var xFonction:XML in XML_UNIVERS.descendants('FONCTION'))
			{
				var ir:ItemRendererFonctionAccueilMenuIHM = new ItemRendererFonctionAccueilMenuIHM();
				ir._xmlFonction = xFonction;
				tl.addChild(ir);
			}
		}
	}
}