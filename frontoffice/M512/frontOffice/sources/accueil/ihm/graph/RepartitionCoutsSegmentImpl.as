package accueil.ihm.graph
{
	import mx.resources.ResourceManager;
	import accueil.utils.preloader.Spinner;
	
	import mx.charts.series.items.PieSeriesItem;
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.formatters.CurrencyFormatter;
	import composants.util.ConsoviewFormatter;

	[Bindable]
	public class RepartitionCoutsSegmentImpl extends Canvas
	{
		private var _dataProvider:ArrayCollection;
		private var _progressBar:Spinner;
		
		public var cf		:CurrencyFormatter;
		
		public var total	:Number = 0;
		
		public var dateToday	:String = "";
		
		public function RepartitionCoutsSegmentImpl()
		{
			super();
			//launchLoader();
		}

	      private function getMonthNumber(dateobject:Date):Object
	      {
	      		var obj:Object = new Object();
	            var day:String = dateobject.date.toString();
	            if(day.length < 2)
	            {day = "0" + day;}
	            var month:int = Number(dateobject.month) + 1;
	            var temp:String = month.toString();
	            if(temp.length < 2)
	            {temp = "0" + temp;}
	            var year:String = dateobject.fullYear.toString();
	            obj.YEAR	= year;
	            obj.MONTH	= temp;
	            obj.DAY		= day;
	            return obj;
	      }
	      
	      private function getMonthString(idMonth:int):String
	      {
	            var monthString   :String = "";
	            switch(idMonth)
	            {
	                 case 1:     monthString = ResourceManager.getInstance().getString('M511', 'Janvier');    break;
	                 case 2:  monthString = ResourceManager.getInstance().getString('M511', 'F_vrier');  break;
	                 case 3:  monthString = ResourceManager.getInstance().getString('M511', 'Mars');           break;
	                 case 4:  monthString = ResourceManager.getInstance().getString('M511', 'Avril');          break;
	                 case 5:  monthString = ResourceManager.getInstance().getString('M511', 'Mai');      break;
	                 case 6:  monthString = ResourceManager.getInstance().getString('M511', 'Juin');           break;
	                 case 7:  monthString = ResourceManager.getInstance().getString('M511', 'Juillet');  break;
	                 case 8:  monthString = ResourceManager.getInstance().getString('M511', 'Ao_t');           break;
	                 case 9:  monthString = ResourceManager.getInstance().getString('M511', 'Septembre'); break;
	                 case 10: monthString = ResourceManager.getInstance().getString('M511', 'Octobre');  break;
	                 case 11: monthString = ResourceManager.getInstance().getString('M511', 'Novembre');  break;
	                 case 12: monthString = ResourceManager.getInstance().getString('M511', 'D_cembre'); break;
	            }
	            return monthString;
	      }
		
		public function launchLoader():void {			
			 _progressBar = new Spinner();
			_progressBar.x = this.width/2;
			_progressBar.y = this.height/2 ;	
			this.addChild(_progressBar);			
			_progressBar.play();
		}
		
		public function stopLoader():void
		{
			if (_progressBar)
			{
				_progressBar.stop();
				this.removeChild(_progressBar);
				_progressBar = null;
			}	
		}

		public function set dataProvider(value:ArrayCollection):void
		{
			 _dataProvider = value;
			if(_progressBar)
			{
				_progressBar.stop();			
				this.removeChild(_progressBar);
				_progressBar = null;	
			}
		}

		public function get dataProvider():ArrayCollection
		{
			return _dataProvider;
		}
		
		protected function mydataTipFunction(obj:Object):String
		{
			if(obj)
			{
				try
				{
					var psi:PieSeriesItem = obj.chartItem as PieSeriesItem;
					if(psi)
					{
						var libelle :String = obj.item["saw_6"]; 
					    var montant	:Number = obj.item["saw_7"];
					  	var prop 	:Number = Math.abs((montant *  100 / total)); 					
						return ResourceManager.getInstance().getString('M511', 'Libell_____b_')+ libelle + ResourceManager.getInstance().getString('M511', '__b__br_Montant____b__font_color____ff00')+ConsoviewFormatter.formatEuroCurrency(montant,2)+ResourceManager.getInstance().getString('M511', '__font___b__br_Proportion___b_')+prop.toPrecision(4)+"%</b>";;
					}				
					else
					{
						return ""
					}
				}
				catch(e:Error)
				{
					trace(e.getStackTrace());
					return "";
				}
				return "";
			}
			else
			{
				return "";
			}
		}
		
		protected function displayValues(data:Object, field:String, index:Number, percentValue:Number):String {
            var temp:String= (" " + percentValue).substr(0,6);
//            return data["saw_6"] + ": " + '\n' + ResourceManager.getInstance().getString('M511', 'Total___') + ConsoviewFormatter.formatEuroCurrency(data["saw_7"],2)+" €" + '\n' + temp + "%";
			return "OK";
        }
	}
}