package accueil.service.data
{
	import flash.events.EventDispatcher;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import accueil.event.AccueilEvent;
	
	 
	public class DataHandlers extends EventDispatcher
	{
		private var _theModel:DataModel;
				
		public function DataHandlers(value:DataModel)
		{
			_theModel = value;	
		}
		
		internal function getIndicateurCleResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_theModel.updateIndicateur(event.result);
				
			}
		}
		internal function getDateDataCompletedHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_theModel.udpateDateLastDateCompelete(event.result);
			}
		}
		
		internal function faultHandler(event:FaultEvent):void
		{
			dispatchEvent(new AccueilEvent(AccueilEvent.AE_REMOTE_OP_FAULT_EVENT));
		}
	}
}