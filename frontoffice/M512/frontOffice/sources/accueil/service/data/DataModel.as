package accueil.service.data
{
	import flash.events.EventDispatcher;
	
	import accueil.entity.AccueilPFGPItemVO;
	import accueil.entity.DatePFGPVo;
	import accueil.event.AccueilEvent;
	
	[Bindable]
	public class DataModel extends EventDispatcher
	{	
		private var _objIndicateurCle:AccueilPFGPItemVO = new AccueilPFGPItemVO();
		private var _pfgpDateLastLoadComplete:DatePFGPVo  = new DatePFGPVo();
		
			
		
		public function get pfgpDateLastLoadComplete ():DatePFGPVo
		{
			return _pfgpDateLastLoadComplete;
		}
		
		public function get objIndicateurCle ():AccueilPFGPItemVO
		{
			return _objIndicateurCle;
		}
		
		public function DataModel()
		{
		}
		
		public function updateIndicateur(param0:Object):void
		{	
			_objIndicateurCle.defineByDefault();			
			_objIndicateurCle.defineByProc(param0);
			dispatchEvent(new AccueilEvent(AccueilEvent.INDICATEUR_UPDATED));
		}
		
		public function udpateDateLastDateCompelete(param0:Object):void
		{
			_pfgpDateLastLoadComplete = new DatePFGPVo();
			_pfgpDateLastLoadComplete.DATE = param0.DATE;
			_pfgpDateLastLoadComplete.IDPERIODE = param0.IDPERIODE;
			dispatchEvent(new AccueilEvent(AccueilEvent.LASTE_DATE_COMPLETE_UPDATED));
		}
		
		public function reset():void
		{
			_pfgpDateLastLoadComplete.DATE  = null;
			_pfgpDateLastLoadComplete.IDPERIODE = 0;
			_objIndicateurCle.defineByDefault();
		}
		
	}
}