package composants.DroitModuleUser
{
	import composants.DroitModuleUser.event.GestionLoginEvent;
	import composants.DroitModuleUser.service.LoginService;
	
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.RadioButton;
	import mx.core.Repeater;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class CompDroitModuleUserImpl extends VBox
	{
		private var _idRacine: int = 0;
		private var _idUser  : int = 0;
		private var SEARCH_STRING:String = "";
		private var _loginService : LoginService = new LoginService();
		public var rp:Repeater;
		private var _idgroupe_client:Number;
		private var _code_app:Number;
		
	/* GETTER SETTER*/
		public function set code_app(value:Number):void
		{
			_code_app = value;
		}
		
		public function get code_app():Number
		{
			return _code_app;
		}

		
		public function set idgroupe_client(value:Number):void
		{
			_idgroupe_client = value;
		}
		
		public function get idgroupe_client():Number
		{
			return _idgroupe_client;
		}
		public function get idRacine():int
		{
			return _idRacine
		}
		public function set idRacine(obj:int):void
		{
			_idRacine = obj
		}
		public function get idUser():int
		{
			return _idUser
		}
		public function set idUser(obj:int):void
		{
			_idUser = obj
		}
				
		public function get loginService():LoginService
		{
			return _loginService
		}
		/* CONSTRUCTEUR */	
		public function CompDroitModuleUserImpl():void
		{
		}
	/* FONCTION */
	// PUBLIC
		public function refresh():void
		{
			getData()
		}
		public function getData(e:FlexEvent=null):void
		{
			if(idUser > 0 && idRacine > 0)
				loginService.ListeModule(idUser,idgroupe_client,code_app);
		}
		public function sauvegarderDroit():void
		{
			var listeModEcriture:String = getListeModuleEcriture()
			var listeModLecture:String  = getListeModuleLecture()
			loginService.addEventListener(GestionLoginEvent.SAUVEGARDER_DROIT_COMPLETE,save_handler)
			loginService.addEventListener(GestionLoginEvent.SAUVEGARDER_DROIT_ERROR,error_handler)
			loginService.SauvegarderDroit(idUser,idgroupe_client,listeModEcriture,listeModLecture);
		}
		public function filtrer(str:String):void
		{
			SEARCH_STRING = str;
			loginService.arrListeModule.filterFunction = filtreModule;
			loginService.arrListeModule.refresh();
		}
	// PRIVATE	
		private function filtreModule(item:Object):Boolean
		{
			if(((item.CODE_MODULE as String).toLowerCase()).search(SEARCH_STRING.toLowerCase()) != -1 
				||
				((item.NOM_MODULE as String).toLowerCase()).search(SEARCH_STRING.toLowerCase()) != -1 )
			{
				return (true);
			}
			else
			{
				return (false);
			}
		}
		private function getListeModuleEcriture():String
		{
			var str:String = ""
			for each(var obj:Object in loginService.arrListeModule.source)
			{
				if(obj.ACCESS_USER == 2)
					str+=","+obj.MODULE_CONSOTELID
			}
			str = str.substr(1,str.length);
			if(str.length == 0)
				str = "0"
			return str
		}
		private function getListeModuleLecture():String
		{
			var str:String = ""
			for each(var obj:Object in loginService.arrListeModule.source)
			{
				if(obj.ACCESS_USER == 1)
					str+=","+obj.MODULE_CONSOTELID
			}
			str = str.substr(1,str.length);
			if(str.length == 0)
				str = "0"
			return str
		}
		
		private function save_handler(e:GestionLoginEvent):void
		{
			Alert.show("Sauvegarde réussie avec succés","Réussite",Alert.OK,this);
		}
		private function error_handler(e:GestionLoginEvent):void
		{
			Alert.show("Impossible de sauvegarder les droits","Une erreur est survenue",Alert.OK,this);
		}
				
	/* HANDLER */
		protected function radiobutton_clickHandler(event:MouseEvent):void
		{
			if(event != null)
			{
				for each(var obj:Object in loginService.arrListeModule)
				{
					if(obj.MODULE_CONSOTELID == parseInt((event.target as RadioButton).groupName))
					{
						obj.ACCESS_USER = (event.target as RadioButton).value
						break
					}
				}
			}
		}

		

	}
}