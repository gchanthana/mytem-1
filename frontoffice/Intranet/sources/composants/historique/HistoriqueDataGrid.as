package composants.historique {
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.ConsoViewErrorManager;
	import mx.controls.Alert;
	import mx.collections.ArrayCollection;
	import flash.utils.getQualifiedClassName;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.utils.ObjectUtil;
	import flash.events.Event;
	import mx.core.ClassFactory;

	public class HistoriqueDataGrid extends DataGrid {
		public static const HISTO_DATA_GRID_READY:String = "HISTO_DATA_GRID_READY";
		public static const RAISON_SOCIALE_COLUMN:String = "RAISON_SOCIALE";
		public static const OP_NOM_COLUMN:String = "OPNOM";
		public static const OFFRENOM_COLUMN:String = "OFFRENOM";
		public static const TYPEDATA_COLUMN:String = "TYPEDATA";
		public static const LIBELLE:String = "LIBELLE";
		private var dateDeb:Date;
		private var dateDebString:String;
		private var dateFin:Date;
		private var dateFinString:String;
		private var idgroupe:int;
		
		public function HistoriqueDataGrid() {
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:FlexEvent):void {
			this.visible = false;
			this.itemRenderer = new ClassFactory(historicItemRenderer);
			this.showDataTips = true;
			//this.dataTipFunction = nbFormat;
		}
		
		public function nbFormat(c:Event):void
		{
			trace(c);
		}
		
		public function get DATE_DEB():Date {
			return dateDeb;
		}

		public function get DATE_FIN():Date {
			return dateFin;
		}
		
		public function loadHistoData(dateDebParam:Date,dateFinParam:Date, id:int):void {
			this.visible = false;
			dateDeb = dateDebParam;
			dateFin = dateFinParam;
			// Format YYYY/MM/01
			dateDebString = dateDeb.fullYear.toString();
			var dateDebMonthString:String = null;
			dateFinString = dateFin.fullYear.toString();
			var dateFinMonthString:String = null;
			if((dateDeb.month + 1).toString().length == 1)
				dateDebMonthString = "0" + (dateDeb.month + 1).toString();
			else
				dateDebMonthString = (dateDeb.month + 1).toString();
				
			if((dateFin.month + 1).toString().length == 1)
				dateFinMonthString = "0" + (dateFin.month + 1).toString();
			else
				dateFinMonthString = (dateFin.month + 1).toString();
			dateDebString = dateDebString + "/" + dateDebMonthString + "/01";
			dateFinString = dateFinString + "/" + dateFinMonthString + "/01";
			idgroupe=id;
			loadHistoColumnMap();
		}
		
		private function loadHistoColumnMap():void {
			var getHistoColumnMapOp:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.historique.HistoriqueContext",
																	"getHistoriqueColumnMap",
																	buildHistoColumn,ConsoViewErrorManager.processRemotingFaultEvent);
			var tmpTypePerimetre:String = "Groupe";
			//var tmpPerimetreIndex:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			RemoteObjectUtil.callService(getHistoColumnMapOp,this.dateDebString,this.dateFinString);
		}
		
		private function buildHistoColumn(event:ResultEvent):void {
			var tmpDataGridColumn:DataGridColumn;
			var tmpColumnArray:Array = event.result as Array;
			var tmpColumns:Array = new Array(tmpColumnArray.length);
			var i:int = 0;
			for(i = 0; i < tmpColumns.length; i++) {
				if (tmpColumnArray[i].COLUMN_FIELD != TYPEDATA_COLUMN)
				{
					tmpColumns[i] = new DataGridColumn(tmpColumnArray[i].COLUMN_LABEL);
					tmpDataGridColumn = tmpColumns[i] as DataGridColumn;
					tmpDataGridColumn.dataField = tmpColumnArray[i].COLUMN_FIELD;
					
					if(tmpDataGridColumn.dataField == "RAISON_SOCIALE")
						tmpDataGridColumn.width = 250;
					if(tmpDataGridColumn.dataField == HistoriqueDataGrid.OP_NOM_COLUMN)
						tmpDataGridColumn.width = 150;
					if(tmpDataGridColumn.dataField == HistoriqueDataGrid.OFFRENOM_COLUMN)
						tmpDataGridColumn.width = 150;
					/*if(tmpDataGridColumn.dataField == HistoriqueDataGrid.TYPEDATA_COLUMN)
						tmpDataGridColumn.width = 25;*/
					if((tmpDataGridColumn.dataField.charAt(0) == "M") &&
						(tmpDataGridColumn.dataField.charAt(1) == "_")) {
						tmpDataGridColumn.width = 25;
						tmpDataGridColumn.headerText="";
						//tmpDataGridColumn.itemRenderer = new ClassFactory(historicItemRenderer);
					}
				}
			}
			// Colonne Supplémentaire
			var tmp:DataGridColumn = new DataGridColumn("N");
			tmp.headerText = "";
			tmpColumns.push(tmp);
			
			this.columns =
					tmpColumns;
			fillHistoDataGrid();
		}
		
		private function fillHistoDataGrid():void {
			var getHistoOp:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.historique.HistoriqueContext",
																	"getHistoriqueData",
																	displayHistoData,ConsoViewErrorManager.processRemotingFaultEvent);
			var tmpTypePerimetre:String = "Groupe";
			//var tmpPerimetreIndex:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			RemoteObjectUtil.callService(getHistoOp,idgroupe,tmpTypePerimetre,this.dateDebString,this.dateFinString);
		}
		
		private function displayHistoData(event:ResultEvent):void {
			var resultSet:ArrayCollection = event.result as ArrayCollection;
			this.dataProvider = resultSet;
			this.visible = true;
			dispatchEvent(new Event(HistoriqueDataGrid.HISTO_DATA_GRID_READY));
		}
		
		private function formatDataTip(obj:Object):String {
			trace(obj.listData);
			return "test";
		}
	}
}
