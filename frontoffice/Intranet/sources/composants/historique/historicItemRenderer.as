package composants.historique
{
	import mx.controls.dataGridClasses.DataGridListData;
	import mx.controls.dataGridClasses.DataGridItemRenderer;
	import mx.controls.Label;
	import mx.containers.Canvas;
	import mx.controls.TextInput;
	import flash.display.Graphics;
	import mx.events.ToolTipEvent;
	import composants.util.DateFunction;
	import mx.managers.ToolTipManager;
	
	public class historicItemRenderer extends DataGridItemRenderer
	{
		public function historicItemRenderer():void
		{
			 setStyle("borderStyle", "none");   
			 super.background = true;
			this.addEventListener(ToolTipEvent.TOOL_TIP_SHOW, toolTipShown);
			this.addEventListener(ToolTipEvent.TOOL_TIP_SHOWN, toolTiphide);
		}
		
		public function toolTiphide(event:ToolTipEvent):void
		{
			var tmp:DataGridListData = super.listData as DataGridListData;
			if (tmp.columnIndex <= 2 || tmp.dataField == "N")
				event.toolTip.visible = false;	
		}
		
		public function toolTipShown(event:ToolTipEvent):void
		{
			var tmp:DataGridListData = super.listData as DataGridListData;
			if (tmp.columnIndex > 2)
			{
				var d:String = tmp.dataField.substr(2, tmp.dataField.length);
				var newDate:Date = new Date(d.substr(0, 4), d.substr(5, 2));
				var str:String = "Mois de " + DateFunction.moisEnLettre(newDate.month) + " " + newDate.fullYear + "\n";
				
				var provider:Object = (tmp.owner as HistoriqueDataGrid).dataProvider[tmp.rowIndex -1];
				var value:Number = provider[tmp.dataField];
				if (provider.TYPEDATA == "A")
				{
					switch (value)
					{
						case 0:
							str += "Pas abonné";
							break;
						case 1:
							str += "Tickets importés mais non calculés";
							break;
						case 2:
							str += "Tickets importés et calculés";
							break;
					}
					
				}
				else
					str += "Nombre factures importées : " +  value;
			
			
				event.toolTip.text = str;
			}
			else
			{
				/*event.toolTip.text = "";*/
			}
			event.toolTip.x += 10;
			event.toolTip.y -= event.toolTip.height;
		}
		
		override public function set data(value:Object):void {
           try {
	           super.data = value;
	           if (value.dataField == undefined)
	           {
		         	var tmp:DataGridListData = super.listData as DataGridListData;
				
					var color:uint = 0xFFFFFF;
		           	if (super.data.TYPEDATA == "A")
		           	{
		           		if (tmp.label == "2")
			           		color = 0xFF6633;
						else if (tmp.label == "1")
							color = 0x0099FF;
		           	}
		           	else
		           	{
		           		var max:Number = super.data.MAX_VALUE;
		           		var nb:Number = parseInt(tmp.label);
		           		if (!isNaN(nb))
		           		{
		           			if (nb == 0)
			           			color = 0xffffff;
		           			else if (nb > 0 && (max / 4) > nb)
			           			color = 0xdae41f;
			           		else if (nb >= (max / 4) && nb < (max / 2))
			           			color = 0x9abe19;
			           		else if (nb >= (max /2) && nb < (max  * 3 / 4))
			           			color = 0x539311;
			           		else
			           			color = 0x146d0b;
		           		}
		           	}  
		           	super.backgroundColor = color; 
		           if (tmp.columnIndex > 2) {
			           tmp.label = "";
	  	           }
	  	          // tmp.toolTip = "tesssssst";
	           	}
	          	//super.validateDisplayList();
	          	super.validateProperties();
				//super.invalidateDisplayList();
		} catch (e:Error) {
		}
		}
  	}
}