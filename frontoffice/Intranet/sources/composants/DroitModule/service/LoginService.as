package composants.DroitModule.service
{
	import composants.DroitModule.event.GestionLoginEvent;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	import mx.managers.SystemManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class LoginService extends EventDispatcher
	{
		public var arrProfilRecup : ArrayCollection = new ArrayCollection();
		public var arrListeModule:ArrayCollection
		public var arrListeProfil:ArrayCollection
		public var arrProfilRight:ArrayCollection
		public var arrListeCodeAPP:ArrayCollection;
		
		public function LoginService() 
		{
		}
		
		/* ----------------------------------------------------------------------------------------------------------------------------------
		AFFINAGE DES DROITS DES LOGINS SUR LES MODULES
		---------------------------------------------------------------------------------------------------------------------------------  */		
		public function ListeModule(app_loginid:int, id_racine:int,code_app:Number = 1):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.M21.gestionDroitService",
				"ListeModule3",
				ListeModuleHandler,error);
			RemoteObjectUtil.callService(op1,app_loginid,id_racine,code_app);
		}
		public function ListeModuleRacine(id_racine:int,code_app:int):void
		{
			if(arrListeModule)	arrListeModule.removeAll();
			var op1:AbstractOperation =	RemoteObjectUtil.getOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.M21.gestionDroitService",
				"ListeModule3",
				ListeModuleHandler,error);
			RemoteObjectUtil.callService(op1,0,id_racine,code_app);
		}
		public function SauvegarderDroit(app_loginid:int, id_racine:int, liste_Module_Ecriture:String, liste_Module_Lecture:String ):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.M21.gestionDroitService",
				"SauvegarderDroit",
				SauvegarderDroitHandler,error);
			RemoteObjectUtil.callService(op1,app_loginid,id_racine,liste_Module_Ecriture,liste_Module_Lecture);
		}
		public function SauvegarderDroitracine(id_racine:int, liste_Module_Ecriture:String, liste_Module_Lecture:String ):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.M21.gestionDroitService",
				"SauvegarderDroitRacine",
				SauvegarderDroitHandler,error);
			RemoteObjectUtil.callService(op1,id_racine,liste_Module_Ecriture,liste_Module_Lecture);
		}
		public function listeProfil(id_racine:int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.M21.gestionDroitService",
				"listeProfil",
				ListeProfilHandler,error);
			RemoteObjectUtil.callService(op1,id_racine);
		}
		public function SupprimerProfil(id_profil:int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.M21.gestionDroitService",
				"SupprimerProfil",
				SupprimerProfilHandler,error);
			RemoteObjectUtil.callService(op1,id_profil);
		}
		public function setCodeaPplication(code_app:int, idracine:int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.gestionClient",
				"setCodeApplication",
				setCodeApplicationHandler,error);
			RemoteObjectUtil.callService(op1,code_app,idracine);
		}
		public function EnregistrerProfil(libelle_profil:String, id_racine:int, liste_Module_Lecture:String, liste_Module_ecriture:String, description_profil:String):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.M21.gestionDroitService",
				"EnregistrerProfil",
				EnregistrerProfilHandler,error);
			RemoteObjectUtil.callService(op1,libelle_profil,id_racine,liste_Module_ecriture,liste_Module_Lecture, description_profil);
		}
		public function RecupProfil(id_profil:int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.M21.gestionDroitService",
				"RecupProfil",
				RecupProfilHandler,error);
			RemoteObjectUtil.callService(op1,id_profil);
		}
		
		private function ListeModuleHandler(e:ResultEvent):void
		{
			if(e.result != null && e.result.length > 0)
				arrListeModule = e.result as ArrayCollection
		}
		private function SauvegarderDroitHandler(e:ResultEvent):void
		{
			if(e.result == -1)
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SAUVEGARDER_DROIT_ERROR))
			else
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SAUVEGARDER_DROIT_COMPLETE))
		}
		private function ListeProfilHandler(e:ResultEvent):void
		{
			if(e.result != null && e.result.length > 0)
				arrListeProfil = e.result as ArrayCollection
			else
				arrListeProfil = new ArrayCollection()
		}
		private function SupprimerProfilHandler(e:ResultEvent):void
		{
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SUPPRIMER_PROFIL_COMPLETE));
		}
		private function setCodeApplicationHandler(e:ResultEvent):void
		{
			if(e.result == 1)
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SET_CODEAPP_COMPLETE));
			else
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SET_CODEAPP_ERROR));
		}
		private function EnregistrerProfilHandler(e:ResultEvent):void
		{
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.ENREGISTRER_PROFIL_COMPLETE));
		}
		private function RecupProfilHandler(e:ResultEvent):void
		{
			if(e.result != null)
			{
				arrProfilRecup = e.result as ArrayCollection
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.RECUP_PROFIL_COMPLETE));
			}
		}
		private function error(e:FaultEvent):void
		{
			Alert.show("Une erreur est survenue :"+e.message,"erreur",Alert.OK);
		}
		
	}
}