package  composants.util
{
	import flash.events.Event;
	import mx.events.FlexEvent;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import univers.UniversManager;
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	import flash.utils.getQualifiedClassName;
	import composants.access.ListePerimetresTree;
	import composants.tb.recherche.renderer.TIRFolderLines;
	import mx.core.ClassFactory;
	
	public class TruncatedTree extends ListePerimetresTree
	{
		private var label:String;
		
		public function TruncatedTree(label:String) {
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
			this.labelField="@"+label;
		}
		
		override public function initIHM(e:Event):void {
			this.itemRenderer=new ClassFactory(composants.tb.recherche.renderer.TIRFolderLines);
			initDp();
		}
		
		public function setDp(xml:Object):void {
			this.dataProvider = null;
			this.dataProvider = new XMLList(xml);
		}
		
		public function selectFirstNode():Object {
			return this.dataProvider[0];
			this.selectedItem=this.dataProvider[0];
		}
		
		public function initDp():void {
			var _treeNodes:XMLList = UniversManager.getListePerimetres() as XMLList;
			var value:int;
			var dept:XMLList;
			value = UniversManager.getPerimetre().PERIMETRE_INDEX;
			
			if (value == 1) {
				setDp(null);
				setDp(UniversManager.getListePerimetres() as XMLList);
			} else {
				setDp(null);
				dept = _treeNodes.descendants().(@NODE_ID.toString()==value.toString());
				setDp(dept[0]);
			}
		}
	}
}