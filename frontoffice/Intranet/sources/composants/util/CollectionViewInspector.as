package composants.util
{
	import mx.collections.ArrayCollection;
	import mx.collections.CursorBookmark;
	import mx.collections.IViewCursor;
	import mx.events.CollectionEvent;

	public class CollectionViewInspector extends ArrayCollection
	{
		
		
		private var _isAllSelected:Boolean = false;
		private var _isAnySelected:Boolean = false;
		private var _isAllIVisibleNSelected:Boolean = false;
		private var _nbSelected:Number = 0;
		
		private var _cursor:IViewCursor;
		private var _nbItem:Number = 0;
		
		public function CollectionViewInspector()
		{ 
				_cursor = this.createCursor();
		}
		
		[Bindable]
		public function get nbSelected():Number
		{
			return _nbSelected;
		}
		
		protected function set nbSelected(value:Number):void
		{
			if( _nbSelected != value)
			{
				_nbSelected = value;
			}
		}
		
		[Bindable]
		public function get nbItem():Number
		{
			return _nbItem;
		}
		
		
		protected function set nbItem(value:Number):void
		{
			_nbItem = value;
			check();
		}

		[Bindable]
		public function get isAnySelected():Boolean
		{	
			if(nbSelected > 0)
			{
				_isAnySelected = true;
			}
			else
			{
				_isAnySelected = false;
			}
			return _isAnySelected;
		}

		protected function set isAnySelected(value:Boolean):void
		{
			if( _isAnySelected !== value)
			{
				_isAnySelected = value;
			}
		}

		[Bindable]
		public function get isAllSelected():Boolean
		{
			if(nbSelected == this.source.length && this.source.length > 0 )
			{	
				_isAllSelected = true;
			}else
			{
				_isAllSelected = false;
			}
			
			return _isAllSelected;
		}

		protected function set isAllSelected(value:Boolean):void
		{
			if( _isAllSelected !== value)
			{
				_isAllSelected = value;
			}
		}
		
		
		[Bindable]
		public function get isAllIVisibleNSelected():Boolean
		{
			
			_isAllIVisibleNSelected = checkCondition();
			return _isAllIVisibleNSelected;
		}
		
		private function checkCondition():Boolean
		{	
			var c:int = 0;
			_cursor.seek(CursorBookmark.FIRST);		
			while(!_cursor.afterLast)
			{	
				c++;
				trace(_cursor.current.PRODUIT + " selected = " + _cursor.current.SELECTED + " nb = " + c );
				if(_cursor.current.SELECTED == false) return false;
				_cursor.moveNext();
			}
			
			return true;
		}
		
		protected function set isAllIVisibleNSelected(value:Boolean):void
		{
			if( _isAllIVisibleNSelected !== value)
			{
				_isAllIVisibleNSelected = value;
			}
		}
		
		
		
		
		
		
		
		

		public function selectAll(value:Boolean):void
		{		
			var _function:Function = value?selectItem:unSelectItem;
			
			_cursor.seek(CursorBookmark.FIRST);		
			while(!_cursor.afterLast)
			{				
				_function(_cursor.current);
				_cursor.moveNext();				
			}
		}
				
		public function selectItem(item:Object):*
		{
			if(item.hasOwnProperty("SELECTED"))
			{	
				
				if(!item.SELECTED)
				{	
					nbSelected = nbSelected + 1;
				}
								
				item.SELECTED = false;///bug qd la lise est filtrée et que les éléments affichés sont tous sélectionnés
				item.SELECTED = true;
			}
		}
		
		public function unSelectItem(item:Object):*
		{
			if(item.hasOwnProperty("SELECTED"))
			{
				if(item.SELECTED)
				{
					nbSelected = nbSelected - 1;	
				}
				
				item.SELECTED = true;
				item.SELECTED = false;
				
				return item;
			}
		}
		
		
		private function check():void
		{
			
			
			
		}
		
		public function reset():Boolean
		{
			return true;
		}
		
		override public function addItem(item:Object):void
		{
			super.addItem(item);		 
		}
		
		override public function addItemAt(item:Object, index:int):void
		{
			super.addItemAt(item,index);
			
			nbItem  = source.length;
			
		}
		
		override public function removeAll():void
		{
			super.removeAll();
			nbItem  = source.length;
		}
		
		override public function removeItemAt(index:int):Object
		{
			var _o:Object = super.removeItemAt(index);
			nbItem  = source.length;
			return _o; 
		} 
		
		override public function set source(s:Array):void
		{
			nbItem = 0;
			nbSelected = 0;
			super.source  = s;
			if(s)
			{
				nbItem = source.length;
				nbSelected = 0;
			}
		}
		
		private function CollectionChangeHandler(ev:CollectionEvent):void
		{	
			
		}
		
		
		
		private var _updatedItems:ArrayCollection = new ArrayCollection();
		public function get updatedItems():ArrayCollection
		{
			_updatedItems.source = new Array();
			
			for(var i:int = 0; i < source.length; i++)
			{
				if (source[i].DATA_CHANGED == true)
				{
					_updatedItems.addItem(source[i]);
				}	
			}
			return _updatedItems
		}
		
		
		
		private var _selectedItems:ArrayCollection = new ArrayCollection();
		public function get selectedItems():ArrayCollection
		{
			_selectedItems.source = new Array();
			
			for(var i:int = 0; i < source.length; i++)
			{
				if (source[i].SELECTED == true)
				{
					_selectedItems.addItem(source[i]);
				}	
			}
			
			return _selectedItems
		}
	}
}