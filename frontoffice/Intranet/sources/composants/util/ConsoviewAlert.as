package composants.util
{
	import mx.controls.Alert;
	
	public class ConsoviewAlert
	{
		
		
		/**		  
		 * Affiche une alertbox de confirmation 
		 * @param text  Le texte
		 * @param titre Le titre
		 * @param traitement La fonction qui gere le click sur OK ou CANCEL 
		 **/	
		public static function afficherAlertConfirmation(text : String , titre : String,traitement :  Function):void{
			
			[Embed(source='/assets/images/warning.png')]
	 		var myAlertImg : Class;
			var myAlertBox : Alert;					
			
			Alert.show(text,titre,Alert.OK | Alert.CANCEL,
			null,traitement,myAlertImg);			
		}
	}
}