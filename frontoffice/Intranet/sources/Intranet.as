package {
	import appli.IntranetMain;
	import appli.LoginEvent;
	
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.ConsoViewErrorManager;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	public class Intranet extends Canvas {
 
		public static var mySession:sessionManager = new sessionManager();
		public static const LOGOFF_EVENT:String = "LOGOFF_EVENT";
		public static const GROUP_LIST_EMPTY:String = "GROUP_LIST_EMPTY";
		public static const NO_ACCESS_NODE_AVAILABLE:String = "NO_ACCESS_NODE_AVAILABLE";
		private static var logoffDispatcher:EventDispatcher;
		
		public function Intranet() {						
			Intranet.logoffDispatcher = new EventDispatcher();
			try {
				ConsoViewErrorManager.setRemotingErrorManager(this);
				
			} catch(error:IllegalOperationError) {
				
				Intranet.showError(error.toString());
			}
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function validerConnexion(loginString:String):void {
			
			if (loginString != "0") {
			var verifyAccessOp:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.intranet",
																	"GetLoginInfos",
																	validateLoginResult,validateLoginError);
																	
			RemoteObjectUtil.callService(verifyAccessOp,loginString);
			
			} 
			else 
			{
				Alert.show("Votre Login n'est pas valide");
			}
		}
		
		private function validateLoginResult(event:ResultEvent):void {
			
			var result:int = event.result.length;
			if(result > 0) {
								
				mySession.setSession(event.result as ArrayCollection);				
				this.dispatchEvent(new LoginEvent(LoginEvent.VALIDE));
				this.addChild(new IntranetMain());
			} else {
				
				this.dispatchEvent(new LoginEvent(LoginEvent.INVALIDE));
				Alert.show("Echec de la connexion --> Accès Refusé");
			}
			
		}
		
		private function validateLoginError(faultEvent:FaultEvent):void {
			Alert.show(ObjectUtil.toString(faultEvent));
			ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
		}
		

		public static function showError(errorMessage:String):void {
			Alert.show(errorMessage);
		}
		
		private function initIHM(event:Event):void {
			Intranet.logoffDispatcher.addEventListener(Intranet.LOGOFF_EVENT,logoff);
			
			
			validerConnexion(index.login);
			
		}
		private function processLogin(event:Event):void {
			
			
		}
		private function initComponents(event:Event):void {
		
		}
		private function logoff(event:Event):void {
			if(event.type == ConsoViewErrorManager.BACKOFFICE_SESSION_TIMEOUT) {
				
				logoffResult(null);
			} else if(event.type == Intranet.LOGOFF_EVENT) {
				var logoffOp:AbstractOperation =
							RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.Intranet",
																		"endAccessSession",
																		logoffResult,logoffFault);
				RemoteObjectUtil.callService(logoffOp,null);
			} else if(event.type == ConsoViewErrorManager.ADMIN_SESSION_DISCONNECT) {
			
				logoffResult(null);
			} else if(event.type == Intranet.GROUP_LIST_EMPTY) {
				
				var logoffAutoOp:AbstractOperation =
							RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.Intranet",
																		"endAccessSession",
																		logoffResult,logoffFault);
				RemoteObjectUtil.callService(logoffAutoOp,null);
			} else if(event.type == Intranet.NO_ACCESS_NODE_AVAILABLE) {
				
				var logoffNoAccessNodeOp:AbstractOperation =
							RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.Intranet",
																		"endAccessSession",
																		logoffResult,logoffFault);
				RemoteObjectUtil.callService(logoffNoAccessNodeOp,null);
			}
		}

		public static function askForLogoff(event:Event):void {
			logoffDispatcher.dispatchEvent(new Event(Intranet.LOGOFF_EVENT));
		}
		
		private function logoffFault(event:FaultEvent):void {
			ConsoViewErrorManager.processRemotingFaultEvent(event);
		}
		
		private function logoffResult(resultEvent:ResultEvent):void {
			
		}
		
		private function onBackOfficeRemotingException(event:FaultEvent):void {
			var errCode:int = parseInt(event.fault.faultString,10);
			var errID:String = event.fault.faultDetail;
			if(errCode == 102)
				Alert.show("Une ou plusieurs mises à jour dans la base (" + errID + ")");
			else
				Alert.show("Exception Remoting :\n" + event.toString());
		}
	}
}
