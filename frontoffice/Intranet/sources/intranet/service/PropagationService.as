package intranet.service
{
	import flash.events.EventDispatcher;	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import intranet.event.PropagationEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class PropagationService extends EventDispatcher
	{
		public function PropagationService()
		{
		}
		
		public function propagerCliche(idOrga:Number, nbMois:Number):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.GestionClient",
				"propageCliche", propagerClicheHandler, propagerClicheErrorHandler);
			RemoteObjectUtil.callService(op, idOrga,nbMois);
			
		}
		 
		private function propagerClicheHandler(result:ResultEvent):void
		{
			if(result.result > 0)
			{
				dispatchEvent(new PropagationEvent(PropagationEvent.PROPAGATION_SUCCED));
			}
			else
			{
				dispatchEvent(new PropagationEvent(PropagationEvent.PROPAGATION_FAILED));
			}
		}
		
		private function propagerClicheErrorHandler(event:FaultEvent):void
		{
			
		}
	}
}