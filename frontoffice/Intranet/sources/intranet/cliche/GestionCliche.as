package intranet.cliche
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class GestionCliche extends gestionClicheIHM
	{
		//Déclaration des collections :
		[Bindable]
		private var colGroupeMaitre : ArrayCollection = new ArrayCollection(); // Collection de groupe Maitres
		
		[Bindable]
		private var colOrga : ArrayCollection = new ArrayCollection();// Collection d'orgas
		
		[Bindable]
		private var colCliche : ArrayCollection = new ArrayCollection();// Collection de clichés
		
		public function GestionCliche()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt:FlexEvent):void
		{
			initGroupeMaitre();
			dgGroupeMaitre.addEventListener(Event.CHANGE,updateDgGroupeMaitre);
			dgOrga.addEventListener(Event.CHANGE,updateDgOrga);
			txtFiltreGR.addEventListener(Event.CHANGE,filtreGR);
			txtFiltreCliche.addEventListener(Event.CHANGE,filtreCliche);
			txtFiltreOrga.addEventListener(Event.CHANGE,filtreOrga);
			btnAddCliche.addEventListener(MouseEvent.CLICK,addCliche);
			dgCliche.addEventListener(Event.CHANGE,getarbreCliche);
			btnDeleteCliche.addEventListener(MouseEvent.CLICK,deleteCliche);
		}
		private function updateDgOrga(evt : Event):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.GestionClient",
																		"get_liste_cliche",
																		processGetCliche,remoteError);
			RemoteObjectUtil.callService(op,dgOrga.selectedItem.IDGROUPE_CLIENT);
		}
		private function addCliche(evt : MouseEvent):void
		{
			if(dgOrga.selectedIndex!=-1)
			{
				var objPopUp : AjouterCliche = new AjouterCliche(dgOrga.selectedItem.IDGROUPE_CLIENT,this);
				PopUpManager.addPopUp(objPopUp,this,true);
				PopUpManager.centerPopUp(objPopUp);
			}
			else
			{
				Alert.show("Selectionnez une Orga");
			}
		}
		public function selectNouveauCliche(idCliche : int):void
		{
			//Alert.show("selection");
			updateDgOrga(null);
						
		}
		//Initialisation des groupes maitres : 
		private function initGroupeMaitre():void{
			
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.GestionClient",
																	"getGroupesMaitres",
																	processInitGroupes,remoteError);
			RemoteObjectUtil.callService(op,-1);
		}
		private function getarbreCliche(evt : Event):void{
			if(dgCliche.selectedIndex != -1)
			{
				var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.GestionClient",
																		"getarbre_cliche",
																		processInitArbre,remoteError);
				RemoteObjectUtil.callService(op,dgCliche.selectedItem.IDCLICHE,dgCliche.selectedItem.IDORGANISATION);
			}
			else
			{
				Alert.show("Aucun cliché sélectionné");
			}
		}
		private function deleteCliche(evt : Event):void{
			if(dgCliche.selectedIndex != -1)
			{
				var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.GestionClient",
																		"delete_cliche",
																		processDeleteCliche,remoteError);
				RemoteObjectUtil.callService(op,Number(dgCliche.selectedItem.IDCLICHE));
			}
			else
			{
				Alert.show("Aucun cliché sélectionné");
			}
		}
		private function processDeleteCliche(evt : ResultEvent):void
		{
			if (evt.result != -1)
			{
				Alert.show("Cliché supprimé");
				if(dgOrga.selectedIndex != -1)
				{
					updateDgOrga(null);
				}
				else
				{
					updateDgGroupeMaitre(null);
				}
				
			}
			else
			{
				Alert.show("Impossible de supprimer le cliché : PROCEDURE DELETE_CLICHE");
			}
		}
		private function processInitArbre(evt : ResultEvent):void
		{
			if(evt.result != -1)
			{
					monTreeComplet.dataProvider=evt.result as XML;
					monTreeComplet.dataDescriptor = new Arbre(monTreeComplet,Number(dgCliche.selectedItem.IDCLICHE));
			}
			else
			{
				Alert.show("Impossible de créer l'arbre : getarbre_cliche = -1");
			}	
		
		}
		private function processInitGroupes(evt : ResultEvent):void{
			colGroupeMaitre = evt.result as ArrayCollection;
			dgGroupeMaitre.dataProvider=colGroupeMaitre;
		}
		private function remoteError(evt : FaultEvent):void{
			Alert.show("Pb : "+evt.toString());
		}
		private function updateDgGroupeMaitre(evt : Event):void
		{
			//Recupere les orgas du groupes :
			var op:AbstractOperation =RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.GestionClient",
																		"getOrgas",
																		processGetOrgas,remoteError);
			RemoteObjectUtil.callService(op,dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
		
			//Recupere les clichés du groupe : 
			var op2:AbstractOperation =RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.GestionClient",
																		"get_liste_cliche_racine",
																		processGetCliche,remoteError);
			RemoteObjectUtil.callService(op2,dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
		}
		private function processGetOrgas(evt : ResultEvent):void
		{
			colOrga = evt.result as ArrayCollection;
			dgOrga.dataProvider = colOrga;
		}
		private function processGetCliche(evt : ResultEvent):void
		{
			colCliche = evt.result as ArrayCollection;
			dgCliche.dataProvider = colCliche;
		}
		/*
		--------------------------------------------------------------------------------------------------------------------------------
		---------------------------------------------------FILTRE----------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------------------------------
		*/
		private function filtreGR(e:Event):void {
			colGroupeMaitre.filterFunction=utilFiltreGridGroupe;
			colGroupeMaitre.refresh();
		}
		private function utilFiltreGridGroupe(item:Object):Boolean {
			if (((item.LIBELLE_GROUPE_CLIENT as String).toLowerCase()).search(txtFiltreGR.text.toLowerCase())!=-1 ) {
				return (true);
			}
			else
			{
				return (false);
			}
			
		}
		private function filtreCliche(e:Event):void {
			colCliche.filterFunction=utilFiltreCliche;
			colCliche.refresh();
		}
		private function utilFiltreCliche(item:Object):Boolean {
			if (((item.LIBELLE_CLICHE as String).toLowerCase()).search(txtFiltreCliche.text.toLowerCase())!=-1 || ((item.LIBELLE_PERIODE as String).toLowerCase()).search(txtFiltreCliche.text.toLowerCase())!=-1 || ((item.LIBELLE_ORGA as String).toLowerCase()).search(txtFiltreCliche.text.toLowerCase())!=-1 ) {
				return (true);
			}
			else
			{
				return (false);
			}
		}
		private function filtreOrga(e:Event):void {
			colOrga.filterFunction=utilFiltreOrga;
			colOrga.refresh();
		}
		private function utilFiltreOrga(item:Object):Boolean {
			if (((item.LIBELLE_GROUPE_CLIENT as String).toLowerCase()).search(txtFiltreOrga.text.toLowerCase())!=-1 ) {
				return (true);
			}
			else
			{
				return (false);
			}
			
		}
		
	}
}