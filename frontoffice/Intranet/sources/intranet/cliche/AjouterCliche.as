package intranet.cliche
{
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class AjouterCliche extends ajouterClicheIHM
	{
		private var idOrga : int;
		private var panelCliche : GestionCliche;
		public function AjouterCliche(idOrga : int,panelCliche : GestionCliche)
		{
			this.panelCliche=panelCliche;
			//Alert.show("idOrga : "+idOrga);
			this.idOrga=idOrga;
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt : FlexEvent):void
		{
			initPeriode();
			btValider.addEventListener(MouseEvent.CLICK,enregistrer);
			btAnnuler.addEventListener(MouseEvent.CLICK,fermer);
		}
		private function fermer(evt : MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		private function enregistrer(evt : MouseEvent):void{
		
			var op:AbstractOperation =RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.GestionClient",
																		"create_cliche",
																		processEnregistrer,remoteError);
			RemoteObjectUtil.callService(op,idOrga,Number(comboPeriode.selectedItem.IDPERIODE_MOIS),inLibelle.text,inCommentaire.text);
		}
		private function processEnregistrer(evt : ResultEvent):void
		{
			if(evt.result == -2)
			{
				Alert.show("Ce cliché existe déja");
			}
			else
			{
				panelCliche.selectNouveauCliche(evt.result as int);
				Alert.show("Cliché enregistré");
				PopUpManager.removePopUp(this);
			}
			
		}
		private function initPeriode():void
		{
			//Recupere les période  :
			var op:AbstractOperation =RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.GestionClient",
																		"get_liste_periode_mois",
																		processGetPeriode,remoteError);
			RemoteObjectUtil.callService(op);
		}
		private function processGetPeriode(evt : ResultEvent):void
		{
			var now : Date= new Date();
			var dateNow : String = now.getMonth()+"/"+now.getFullYear();
		
			
			comboPeriode.dataProvider = evt.result as ArrayCollection;
			for(var i : int = 0 ; i<(comboPeriode.dataProvider as ArrayCollection).length;i++){
				trace((comboPeriode.dataProvider as ArrayCollection).getItemAt(i).SHORT_MOIS+" == "+dateNow);
				
				if(((comboPeriode.dataProvider as ArrayCollection).getItemAt(i).SHORT_MOIS).search(dateNow)!=-1)
				{
					//Alert.show("la date à été trouvée"+(comboPeriode.dataProvider as ArrayCollection).getItemAt(i).SHORT_LIBELLE);
					//+1 : new Date() janvier = 0
					//Données de la bdd janvier = 1
					comboPeriode.selectedIndex = i+1 ;
				}
			}
			
		}
		private function remoteError(evt : FaultEvent):void{
			Alert.show("Pb : "+evt.toString());
		}
		
	}
}
