package intranet.GestionDroits
{
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class GestionWorkFlow extends VBox
	{
		public var cboListeRacine:ComboBox;
		public var lblAvertissement:Label;
		
		[Bindable]
		public var tabListeRacine:ArrayCollection=new ArrayCollection();
		[Bindable]
		public var tabWorkFlow:ArrayCollection= new ArrayCollection();
		
		public function GestionWorkFlow()
		{
			super();
		}
		
		protected function init(e:FlexEvent):void{
			var tmpRacine:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","getListeRacine",
					tmpRacineYes,No);
			RemoteObjectUtil.callService(tmpRacine);
		} 
		
		private function tmpRacineYes(e:ResultEvent):void{
			tabListeRacine=e.result as ArrayCollection;
			tabListeRacine.refresh();
		}
		
		protected function cboListeRacineChangeHandler(e:ListEvent):void{
			if(cboListeRacine.selectedIndex > -1){
				var tmpRules:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","getRules_v3",
					getRulesYes,No);
			RemoteObjectUtil.callService(tmpRules,parseInt(cboListeRacine.selectedItem.IDGROUPE_CLIENT));
			}
		}
		
		private function getRulesYes(e:ResultEvent):void{
			var result:ArrayCollection = e.result as ArrayCollection;
			
			if(result && result.length > 0 
						&& e.result[0].hasOwnProperty("XML") 
						&&  e.result[0].XML != "<boutons></boutons>")
			{
				var i:XML;
				var xml:XMLList = new XMLList(e.result[0].XML);
				var o:Object;
				
				lblAvertissement.text="";
				if(tabWorkFlow != null){
					tabWorkFlow.removeAll();
				}else{
					tabWorkFlow = new ArrayCollection();
				}
				
				for each (i in xml.children()){
					tabWorkFlow.addItem(i as Object);
				}
				refreshTabWorkFlow();
			}else{
				lblAvertissement.text="Aucun workflow n'a été défini pour cette racine. Template par défaut appliqué.";
			}
		}
		
		private function refreshTabWorkFlow():void{
			var dataSortField:SortField = new SortField("code");
			dataSortField.compareFunction=compareFunctionCode;
            var DataSort:Sort = new Sort();
            DataSort.fields = [dataSortField];
           	
           	tabWorkFlow.sort=DataSort;
			tabWorkFlow.refresh();
		}
		
		private function compareFunctionCode(a:String,b:String):int{
			var arrA:Array= a.split("<code>");
			var strA:String=arrA[1];
			var intA:int=parseInt(strA.substr(1,strA.indexOf("<",2)));
			var arrB:Array= b.split("<code>");
			var strB:String=arrB[1];
			var intB:int=parseInt(strB.substr(1,strB.indexOf("<",2)));
			
			if(intA != intB){
				if(intA > intB){
					return 1;
				}else{
					return -1;
				}
			}else{
				return 0;
			}
		}
		
		protected function btnValiderClickHandler(e:MouseEvent):void{
			
			if(cboListeRacine.selectedIndex > -1){
				XML.ignoreWhitespace=true;
				var xml:XML= 
					<boutons></boutons>;
					
				for(var i:int=0;i<tabWorkFlow.length;i++){
					xml.appendChild(<bouton></bouton>);
					xml.bouton[i].appendChild(tabWorkFlow[i].name);
					xml.bouton[i].appendChild(tabWorkFlow[i].id);
					xml.bouton[i].appendChild(tabWorkFlow[i].code);
					xml.bouton[i].appendChild(tabWorkFlow[i].visee);
					xml.bouton[i].appendChild(tabWorkFlow[i].non_visee);
					xml.bouton[i].appendChild(tabWorkFlow[i].controlee);
					xml.bouton[i].appendChild(tabWorkFlow[i].non_controlee);
					xml.bouton[i].appendChild(tabWorkFlow[i].ctl_inventaire);
					xml.bouton[i].appendChild(tabWorkFlow[i].non_ctl_inventaire);
					xml.bouton[i].appendChild(tabWorkFlow[i].vise_ana);
					xml.bouton[i].appendChild(tabWorkFlow[i].non_vise_ana);
					xml.bouton[i].appendChild(tabWorkFlow[i].export_erp_dec);
					xml.bouton[i].appendChild(tabWorkFlow[i].non_export_erp_dec);
					xml.bouton[i].appendChild(tabWorkFlow[i].export_erp);
					xml.bouton[i].appendChild(tabWorkFlow[i].non_export_erp);
					}
//				trace(xml.toString());
					
				var tmpAddRules:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","addRules_v3",
						tmpAddRulesYes,tmpAddRulesNo);
				RemoteObjectUtil.callService(tmpAddRules,parseInt(cboListeRacine.selectedItem.IDGROUPE_CLIENT),xml.toString());
			}
		}
		
		private function tmpAddRulesYes(e:ResultEvent):void{
			if(e.result != -1){
				Alert.show("Enregistrement réussi","Réussite");
			}else{
				trace('addrules error : '+e.message);
			}
		}
		
		private function tmpAddRulesNo(e:FaultEvent):void{
			trace('GestionWorkflow - btnValiderClickHandler : '+e.message);
		}
		
		protected function btnResetClickHandler(e:MouseEvent):void{
			if(cboListeRacine.selectedIndex > -1){
				Alert.show("Etes vous sur de vouloir appliquer le workflow par défaut ?","confirmation",Alert.YES | Alert.NO,this,resetCloseEventHandler);
			}else{
				Alert.show("Veuillez sélectionner une racine","Avertissement");	
			}
		}
		
		private function resetCloseEventHandler(e:CloseEvent):void{
			if(e.detail == Alert.YES){
				var tmpope:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","setDefautWorkflow_v3",
						defautHandler,No);
				RemoteObjectUtil.callService(tmpope,parseInt(cboListeRacine.selectedItem.IDGROUPE_CLIENT));			
			}
		}
		
		private function defautHandler(e:ResultEvent):void{
			if(cboListeRacine.selectedIndex > -1){
				var tmpRules:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","getRules",
					getRulesYes,No);
			RemoteObjectUtil.callService(tmpRules,parseInt(cboListeRacine.selectedItem.IDGROUPE_CLIENT));
			}
		}
		
		private function No(e:FaultEvent):void{
			trace('GestionWorkFlow : '+e.message);
		}
		
	}
}