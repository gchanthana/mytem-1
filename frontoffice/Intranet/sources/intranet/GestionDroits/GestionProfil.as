package intranet.GestionDroits
{
	import flash.events.MouseEvent;
	
	import intranet.GestionDroits.event.profilEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.containers.Panel;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class GestionProfil extends Panel
	{
		public var cboProfil:ComboBox;
		public var cboRacine:ComboBox;
		public var btnValider:Button;
		public var lblProfil:Label;
		public var fiProfil:FormItem;
		public var tiProfil:TextInput;
		public var adgAfficheProfil:DataGrid;
		
		[Bindable]
		public var origine:String;
		[Bindable]
		public var tabProfil:ArrayCollection=new ArrayCollection();
		[Bindable]
		public var tabRacine:ArrayCollection=new ArrayCollection();
		[Bindable]
		public var tabProf:ArrayCollection=new ArrayCollection();
		
		private var gestionDroit:GestionDroitClass= new GestionDroitClass();
		private var o:Object= new Object();
		private var edit:Boolean=false;
		private var str:String="";
		public var titre:String= "Profil ";
		
		public function GestionProfil()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,create);
		}

/** --------------------------------------     gestion des listeners     -------------------------------------------**/
	
		public function create(e:FlexEvent):void{
			gestionDroit.getListeRacine(opeRacineOk,opeRacineNo);
					
			cboProfil.addEventListener(ListEvent.CHANGE,cboProfilDataChangeHandler);
			cboRacine.addEventListener(ListEvent.CHANGE,cboRacineDataChangeHandler);
			
			initObject();
		}
				
		public function opeRacineOk(e:ResultEvent):void{
			tabRacine = e.result as ArrayCollection;
			tabRacine.refresh();
		}
		
		public function opeRacineNo(e:FaultEvent):void{
			trace('GestionProfil - opeRacine :'+e.message);
		}		

		public function dispCreerClickHandler(e:MouseEvent):void{
			initObject();
			o.EDITABLE = true;
			
			compoVisible(true,false,false,false,false,false);
			compoEnable(true,true);

			tabProf = new ArrayCollection();			
			tabProf.addItem(o);
			tabProf.refresh();
			
			resetErrorString();
			
			cboProfil.selectedIndex=-1;
			cboRacine.selectedIndex=-1;
			
			tiProfil.text="";
			str="CREER";
			this.title = "Profil > "+str
		}
		
		public function dispSupprimerClickHandler(e:MouseEvent):void{
			
			if(origine=="intranet"){
				compoVisible(true,false,false,false,false,false);
			}else{
				compoVisible(true,true,false,false,false,false);
				listeProfil();
			}
			resetErrorString();
			
			cboProfil.selectedIndex=-1;
			cboRacine.selectedIndex=-1;
			
			tiProfil.text="";
			str="SUPPRIMER";
			this.title = "Profil > "+str
		}
		
		public function dispMajClickHandler(e:MouseEvent):void{
			if(origine == "intranet"){
				compoVisible(true,false,false,false,false,false);
			}else{
				compoVisible(true,true,false,false,false,false);
				listeProfil();
			}
			
			resetErrorString();
			
			cboProfil.selectedIndex=-1;
			cboRacine.selectedIndex=-1;
			
			tiProfil.text="";
			str="MAJ";
			this.title = "Profil > "+str
		}
		
		public function dispAfficherClickHandler(e:MouseEvent):void{
			if(origine=="intranet"){
			compoVisible(true,false,false,false,false,false);
			}else{
				compoVisible(true,true,false,false,false,false);
				listeProfil();
			}
			
			resetErrorString();
			
			cboProfil.selectedIndex=-1;
			cboRacine.selectedIndex=-1;
			
			tiProfil.text="";
			str="AFFICHER";
			this.title = "Profil > "+str
		}
		
		public function btnValiderClickHandler(e:MouseEvent):void{
			switch(str){			
				case "CREER" : 
					creerBtnValider();
					break;
				
				case "SUPPRIMER" : 
					supprimerBtnValider();							
					break;
					
				case "MAJ" :
					majBtnValider();
					break;
					
				case "AFFICHER" :
					
					break;
			}
		}
		
		public function cboRacineDataChangeHandler(e:ListEvent):void{
			switch(str){			
				case "CREER" : 
					compoVisible(true,false,true,true,true,true);
					break;
				
				case "SUPPRIMER" : 
					compoVisible(true,true,false,false,false,false);
					break;
					
				case "MAJ" :
					compoVisible(true,true,false,false,false,false);
					break;
					
				case "AFFICHER" :
					compoVisible(true,true,false,false,false,false);
					break;
				}
				listeProfil();
		}
		
		public function cboProfilDataChangeHandler(e:ListEvent):void{
			if(cboProfil.selectedIndex != -1){
				gestionDroit.getProfil(opeGetProfilOk,opeGetProfilNo,parseInt(cboProfil.selectedItem.IDPROFIL));
			}			
			
			switch(str){
				case "SUPPRIMER":
					compoEnable(false,false);
					compoVisible(true,true,true,true,true,true);
					break;
				
				case "MAJ":
					compoEnable(true,true);
					compoVisible(true,true,true,true,true,true);
					break;
					
				case "AFFICHER":
					compoEnable(false,false);
					compoVisible(true,true,true,true,false,true);
					break;
			}
			
			tiProfil.text=cboProfil.selectedItem.NAME;
		}
		
		public function opeGetProfilOk(e:ResultEvent):void{
			tabProf = new ArrayCollection();
			initObject();
			var t:ArrayCollection= e.result as ArrayCollection;
			for(var i:int=0;i<t.length;i++){
				switch(t[i].BOUTON_CODE.toString()){
					case "B1" :
						o.obj1 = 1;
						o.obj1BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
						
					case "B2" :
						o.obj2 = 1;
						o.obj2BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
						
					case "B3" :
						o.obj3 = 1;
						o.obj3BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B4" :
						o.obj4 = 1;
						o.obj4BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B5" :
						o.obj5 = 1;
						o.obj5BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
						
					case "B6" :
						o.obj6 = 1;
						o.obj6BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B7" :
						o.obj7 = 1;
						o.obj7BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B8" :
						o.obj8 = 1;
						o.obj8BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B9" :
						o.obj9 = 1;
						o.obj9BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B10" :
						o.obj10 = 1;
						o.obj10BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B11" :
						o.obj11 = 1;
						o.obj11BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					case "B12" :
						o.obj12 = 1;
						o.obj12BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					case "B13" :
						o.obj13 = 1;
						o.obj13BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					default :
						break;
				}
			}
			o.EDITABLE = edit;
			tabProf.addItem(o);
			tabProf.refresh();
		}
		
		public function opeGetProfilNo(e:FaultEvent):void{
			trace('GestionProfil - opeCreateProfil :'+e.message);
		}

/** --------------------------------------     fonction     -------------------------------------------**/

// CREATION 
		public function creerBtnValider():void{
			var boolExiste:Boolean = false;
			if(tiProfil.text != null && tiProfil.text != "" && cboRacine.selectedIndex > -1){
				for(var i:int=0;i<tabProfil.length;i++){
					if(tiProfil.text == tabProfil[i].NAME){
						boolExiste=true;
						break;
					}
				}
				
				if(boolExiste){
					Alert.show("Le profil que vous souhaitez créer existe actuellement","Profil existant");
					tiProfil.errorString="Le profil existe.";
				}else{
					var s:String= setTab();
					if(origine== "intranet"){
						gestionDroit.creerProfil(creerProfilYes,creerProfilNo,tiProfil.text,s,cboRacine.selectedItem.IDGROUPE_CLIENT)
					}else{
						gestionDroit.creerProfil(creerProfilYes,creerProfilNo,tiProfil.text,s,-1)
					}
				}
			}else{
				Alert.show("Veuillez saisir un nom de profil","Erreur de saisie");	
			}
		}

		public function creerProfilYes(e:ResultEvent):void{
			resetErrorString();
			Alert.show("Profil créé avec réussite","Reussite");
			dispatchEvent(new profilEvent(profilEvent.PROFIL_EVENT,true,false));
			trace('Création du profil réussie');
		}

		public function creerProfilNo(e:FaultEvent):void{
			resetErrorString();
			trace('GestionProfil - creerBtnValider :'+e.message);
		}

// SUPPRESSION
		public function supprimerBtnValider():void{
			if(cboRacine.selectedIndex > -1){
				if(cboProfil.selectedIndex > -1){
					Alert.show("Etes vous sur(e) de vouloir supprimer ce profil ?","Confirmation de suppression",Alert.YES | Alert.NO,this,supprimerBtnValiderCloseEvent);
				}else{
					cboProfil.errorString="Veuillez sélectionner le profil à supprimer dans la liste";
				}
			}else{
				cboRacine.errorString="Veuillez sélectionner la racine du profil à supprimer";
			}
		}

		public function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.YES){
				gestionDroit.supprimerProfil(supprimerBtnValiderYes,supprimerBtnValiderNo,parseInt(cboProfil.selectedItem.IDPROFIL));
			}
		}
		
		public function supprimerBtnValiderYes(e:ResultEvent):void{
			Alert.show("Le profil '"+cboProfil.selectedItem.NAME+"' a été supprimé","Suppression réussie");
			listeProfil();
			resetErrorString();
			compoVisible(true,true,false,false,false,false);
			dispatchEvent(new profilEvent(profilEvent.PROFIL_EVENT,true,false));
		}

		public function supprimerBtnValiderNo(e:FaultEvent):void{
			resetErrorString();
			trace('GestionProfil - supprimerBtnValiderCloseEvent : '+e.message);
		}
// MISE A JOUR 
		public function majBtnValider():void{
			if(cboRacine.selectedIndex > -1){
				if(cboProfil.selectedIndex > -1){
					if(tiProfil.text != ""){
						Alert.show("Etes vous sur(e) de vouloir modifier ce profil ?","Confirmation de modification",Alert.YES | Alert.NO,this,majBtnValiderCloseEvent);
					}else{
						tiProfil.errorString="Veuillez saisir un nom de profil";
					}
				}else{
					cboProfil.errorString="Veuillez sélectionner le profil à modifier dans la liste";
				}
			}else{
				cboRacine.errorString="Veuillez sélectionner la racine du profil à modifier";
			}
		}
		
		public function majBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.YES){
				var s:String = setTab();;
				if(origine=="intranet"){
					gestionDroit.majProfil(majBtnValiderYes,majBtnValiderNo,parseInt(cboProfil.selectedItem.IDPROFIL),tiProfil.text,s,parseInt(cboRacine.selectedItem.IDGROUPE_CLIENT));
				}else{
					gestionDroit.majProfil(majBtnValiderYes,majBtnValiderNo,parseInt(cboProfil.selectedItem.IDPROFIL),tiProfil.text,s,-1);
				}
			}
		}		
		
		public function majBtnValiderYes(e:ResultEvent):void{
			Alert.show("Le profil '"+cboProfil.selectedItem.NAME+"' a été modifié","Modification réussie");
			listeProfil();
			resetErrorString();
			compoVisible(true,true,true,true,true,true);
			dispatchEvent(new profilEvent(profilEvent.PROFIL_EVENT,true,false));
		}

		public function majBtnValiderNo(e:FaultEvent):void{
			resetErrorString();
			trace('GestionProfil - majBtnValiderCloseEvent : '+e.message);
		}

// FONCTION : liste les profils
		public function listeProfil():void{
			if(origine=="intranet"){
				gestionDroit.getListeProfil(opeProfilOk,opeProfilNo,parseInt(cboRacine.selectedItem.IDGROUPE_CLIENT));
			}else{
				gestionDroit.getListeProfil(opeProfilOk,opeProfilNo,-1);
			}
		}
		
		public function opeProfilOk(e:ResultEvent):void{
			tabProfil = e.result as ArrayCollection;
			tabProfil.refresh();
			cboProfil.selectedIndex=-1;
		}
		
		public function opeProfilNo(e:FaultEvent):void{
			trace('GestionProfil - opeProfil :'+e.message);
		}
		
		public function initObject():void{
			o.obj1 = 0;
			o.obj2 = 0;
			o.obj3 = 0;
			o.obj4 = 0;
			o.obj5 = 0;
			o.obj6 = 0;
			o.obj7 = 0;
			o.obj8 = 0;
			o.obj9 = 0;
			o.obj10 = 0;
			o.obj11 = 0;
			o.obj12 = 0;
			o.obj13 = 0;
			o.obj1BOUTON_CODE="B1";
			o.obj2BOUTON_CODE="B2";
			o.obj3BOUTON_CODE="B3";
			o.obj4BOUTON_CODE="B4";
			o.obj5BOUTON_CODE="B5";
			o.obj6BOUTON_CODE="B6";
			o.obj7BOUTON_CODE="B7";
			o.obj8BOUTON_CODE="B8";
			o.obj9BOUTON_CODE="B9";
			o.obj10BOUTON_CODE="B10";
			o.obj11BOUTON_CODE="B11";
			o.obj12BOUTON_CODE="B12";
			o.obj13BOUTON_CODE="B13";
			o.EDITABLE = edit;
		}

		public function setTab():String{
			var s:String="";
			var o:Object=adgAfficheProfil.selectedItem as Object;
			
			if(o != null){
				if(o.obj1 == 1)
					s+=","+o.obj1BOUTON_CODE;
				if(o.obj2 == 1)
					s+=","+o.obj2BOUTON_CODE;
				if(o.obj3 == 1)
					s+=","+o.obj3BOUTON_CODE;
				if(o.obj4 == 1)
					s+=","+o.obj4BOUTON_CODE;
				if(o.obj5 == 1)
					s+=","+o.obj5BOUTON_CODE;
				if(o.obj6 == 1)
					s+=","+o.obj6BOUTON_CODE;
				if(o.obj7 == 1)
					s+=","+o.obj7BOUTON_CODE;
				if(o.obj8 == 1)
					s+=","+o.obj8BOUTON_CODE;
				if(o.obj9 == 1)
					s+=","+o.obj9BOUTON_CODE;
				if(o.obj10 == 1)
					s+=","+o.obj10BOUTON_CODE;
				if(o.obj11 == 1)
					s+=","+o.obj11BOUTON_CODE;
				if(o.obj12 == 1)
					s+=","+o.obj12BOUTON_CODE;
				if(o.obj13 == 1)
					s+=","+o.obj13BOUTON_CODE;
				s=s.substr(1,s.length-1);
			}
			return s;
		}
	
		public function compoEnable(ti:Boolean,adg:Boolean):void{
			tiProfil.enabled=ti;
			adgAfficheProfil.enabled=adg;
			adgAfficheProfil.editable=adg;
			edit= adg;
		}
		
		public function compoVisible(cbolisteracine:Boolean,cbolisteprofil:Boolean,lbl:Boolean,ti:Boolean,btnvalider:Boolean,adg:Boolean):void{
			if(this.origine == "cv"){
				cboRacine.visible=false;
			}else{
				cboRacine.visible=cbolisteracine;
			}
			cboProfil.visible=cbolisteprofil;
			lblProfil.visible=lbl;
			tiProfil.visible=ti;
			fiProfil.visible=ti;
			if(ti == true && tiProfil.enabled == true){
				fiProfil.required=true;
			}else{
				fiProfil.required=false;
			}
			btnValider.visible=btnvalider;
			adgAfficheProfil.visible=adg;
		}	
		
		public function resetErrorString():void{
			cboProfil.errorString="";
			cboRacine.errorString="";
			tiProfil.errorString="";
			adgAfficheProfil.errorString="";
		}	
	}
}