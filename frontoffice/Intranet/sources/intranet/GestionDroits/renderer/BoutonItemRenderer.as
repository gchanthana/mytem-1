package intranet.GestionDroits.renderer
{
	import mx.controls.Label;

	public class BoutonItemRenderer extends Label
	{
		public function BoutonItemRenderer()
		{
			super();
			setStyle("fontWeight","bold");
		}
	}
}