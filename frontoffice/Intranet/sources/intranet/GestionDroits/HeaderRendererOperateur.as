package intranet.GestionDroits
{
	import mx.controls.TextArea;

	public class HeaderRendererOperateur extends TextArea
	{
		public function HeaderRendererOperateur()
		{
			super();
			setStyle('editable',false);
		}
	}
}