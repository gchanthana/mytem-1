package intranet.EspacePerso
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import mx.events.FlexEvent;
	import flash.net.URLVariables;
	import flash.net.URLRequest;
	import flash.net.sendToURL;
	import mx.controls.Alert;
	import flash.net.navigateToURL;
	
	public class LienPerso extends LienPersoIHM
	{
		public function LienPerso()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		public function initIHM(event:Event):void {
			btnLink1.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnLink2.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnLink3.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnLink4.addEventListener(MouseEvent.CLICK,processGz);
		}
			private function ouvrirUrl(event:Event):void {
			switch (event.currentTarget.id) {
							
				// Liens persos
				case "btnLink1":
					allerAfichier("\\\\storage-1\\Racine\\IT");
					break;
				case "btnLink2":
					allerAfichier("\\\\dbcl2\\web_directory\\import\\factures\\Infofacture\\temp");
					break;
				case "btnLink3":
					allerAfichier("\\\\storage-1\\Racine\\Dossiers Clients\\2- En cours de traitement");
					break;
			}
		}
		private function processGz(event:MouseEvent):void {
			var url:String = "http://192.168.3.125/processGz.cfm";
            var variables:URLVariables = new URLVariables();
            var request:URLRequest = new URLRequest(url);
            request.method="GET";
            request.data=variables;
            sendToURL(request);
            Alert.show("Fichiers en cours de traitement");
		}
		private function allerAfichier(adresse:String):void {
			var url:String = adresse;
            var request:URLRequest = new URLRequest(url);
            request.method="GET";
            navigateToURL(request,"_blank");
		}
	}
}