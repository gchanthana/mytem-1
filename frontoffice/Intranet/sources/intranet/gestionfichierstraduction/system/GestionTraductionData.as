package intranet.gestionfichierstraduction.system
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	
	
	[Bindable]
	public class GestionTraductionData
	{
		
		public function GestionTraductionData()
		{
			super();
		}
		
		/**
		 * (Function) Fonctions de remplissage des combos du bloc de recherche
		 * */

		public function alimentComboLangue(resultHandler:Function):void
		{
			var opComboLangue:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.gestiontraduction.ServiceInternationalisation",
																	"fillComboLangue",
																	resultHandler, remoteError);
			RemoteObjectUtil.callService(opComboLangue);
		}
		
		public function alimentComboStatut(resultHandler:Function):void
		{
			var opComboStatut:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.gestiontraduction.ServiceInternationalisation",
																	"fillComboStatut",
																	resultHandler, remoteError);
			RemoteObjectUtil.callService(opComboStatut);
		}
		
		public function alimentComboModule(resultHandler:Function):void
		{
			var opComboModule:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.gestiontraduction.ServiceInternationalisation",
																	"fillComboModule",
																	resultHandler, remoteError);
			RemoteObjectUtil.callService(opComboModule);
		}
		
		
		/**
		 * (String, String, String, String, Function) Fonction de retour de la liste des phrases correpondant a la langue
		 * */
		public function getListePhrasesLangue(lang:String, et:String, mod:int, key:String, resultHandler:Function):void
		{
			var opListePhrasesLangue:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.gestiontraduction.ServiceInternationalisation",
																	"getListePhrasesLangue",
																	resultHandler, remoteError);
			RemoteObjectUtil.callService(opListePhrasesLangue, lang, et, mod, key);
		}
		
		/**
		 * (String, String, String, String, Function) Fonction de retour de la liste des phrases
		 * */
		public function getListePhrases(lang:String, et:String, mod:int, key:String, resultHandler:Function):void
		{
			var opListePhrases:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.gestiontraduction.ServiceInternationalisation",
																	"getListePhrases",
																	resultHandler, remoteError);
			RemoteObjectUtil.callService(opListePhrases, lang, et, mod, key);
		}
		
		/**
		 * (String, int, String, String, String, String, int, String, String, String, int, String, int, Function) Fonction de création / modification d'une phrase
		 * */
		public function createPhrase(statut:String, module:int,
									 phrase:String, commentaire:String,
									 langid:String, traduction:String,
									 application:int, univers:String,
									 fonction:String, page:int,
									 type:String, 
									 resultHandler:Function, id:int=-1):void {
		
			var opCreatePhrase:AbstractOperation = 
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.gestiontraduction.ServiceInternationalisation",
																	"createPhrase",
																	resultHandler, remoteError);
				if(id > 0){
					//Modification d'une phrase
					RemoteObjectUtil.callService(opCreatePhrase, statut, module, phrase, commentaire, langid, traduction, application, univers, fonction, page, type, id);
				}
				else{
					//Création d'une phrase
					RemoteObjectUtil.callService(opCreatePhrase, statut, module, phrase, commentaire, langid, traduction, application, univers, fonction, page, type, -1);
				}	
		}
		
		/**
		 * (int, Function) Fonction de suppression d'une phrase
		 * */
		public function deletePhrase(id:int, resultHandler:Function):void
		{
			var opDeletePhrase:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoprod.gestiontraduction.ServiceInternationalisation",
																					"deletePhrase",
																					resultHandler, remoteError);
			RemoteObjectUtil.callService(opDeletePhrase, id);
		}
		
		/**
		 * (XML, String, Function) Fonction d'écriture du fichier de publication de la traduction au format XML
		 * */
		public function createXmlFile(objetXML:XML, fileName:String, resultHandler:Function):void
		{
			var opCreateXmlFile:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoprod.gestiontraduction.ServiceInternationalisation",
																					"createXmlFile",
																					resultHandler, remoteError);
			RemoteObjectUtil.callService(opCreateXmlFile, objetXML, fileName);
		}
		
		/**
		 * (XML, String, Function) Fonction d'écriture du fichier de localisation au format XML
		 * */
		public function createXmlFileLocalisation(objetXML:XML, fileName:String, resultHandler:Function):void
		{
			var opCreateXmlFileLocalisation:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoprod.gestiontraduction.ServiceInternationalisation",
																					"createXmlFileLocalisation",
																					resultHandler, remoteError);
			RemoteObjectUtil.callService(opCreateXmlFileLocalisation, objetXML, fileName);
		}
		
		/**
		 * (String, String, int, String, String, Function) Fonction d'export de la liste de phrases au format CSV
		 * */
		public function exportCsvFile(lang:String, et:String, mod:int, key:String, fileName:String, resultHandler:Function):void
		{
			var opExportCsvFile:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoprod.gestiontraduction.ServiceInternationalisation",
																					"exportCsvFile",
																					resultHandler, remoteError);
			RemoteObjectUtil.callService(opExportCsvFile, lang, et, mod, key, fileName);
		}
		
		
		/**
		 * (FaultEvent) Fonction de gestion de l'erreur de l'appel des procédures
		 * */
		private function remoteError(evt:FaultEvent):void
		{
			trace(evt.fault.getStackTrace());
		}
	}
}