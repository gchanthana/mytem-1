package intranet.gestionfichierstraduction.ihm
{
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.utils.getQualifiedClassName;
	
	import intranet.gestionfichierstraduction.comp.PopupExportCSV;
	import intranet.gestionfichierstraduction.comp.PopupPublishXML;
	import intranet.gestionfichierstraduction.system.GestionTraductionData;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.NumericStepper;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.IndexChangedEvent;
	import mx.events.ListEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	import mx.validators.StringValidator;
	import mx.validators.Validator; 
	
	[Bindable]
	public class GestionTraductionImpl extends VBox
	{
		//-------- ONGLET TRADUCTION -------------------------------
		
		//les données alimentant les combos et le datagrid
		public var gestionTraductionDonnees:GestionTraductionData;
		
		//bloc "recherche de la phrase"
		public var ti_recherche_phrase:TextInput;
		public var cb_langue:ComboBox; 
		public var cb_statut:ComboBox;
		public var cb_module:ComboBox;
		public var phraseSearched:String;
		public var langueSelectedSearch:String;
		public var statutSelectedSearch:String;
		public var moduleSelectedSearch:int;
		public var selection:Boolean = false;
		public var searchTab:Array = [];
		public var indexSearchTab:Array = [];
		public var lengthTab:int;
		
		//bloc "tableau de résultats de la recherche"
		public var dg_resultatPhrase:DataGrid;
		public var idSelectedResult:String;
		public var statutSelectedResult:String;
		public var moduleSelectedResult:String;
		public var phraseSelectedResult:String;
		public var commentaireSelectedResult:String;
		public var langueSelectedResult:String;
		public var traductionSelectedResult:String;
		public var listePhrases:ArrayCollection;
		
		//bloc "création et modification de la phrase"
		public var cv_edit_phrase:Canvas;
		public var lb_type_phrase:Label;
		public var lb_identifiant:Label;
		public var cb_langue_maj:ComboBox;
		public var cb_statut_maj:ComboBox;
		public var cb_module_maj:ComboBox;
		public var langueSelectedMaj:String;
		public var langueSelectedMaj2:String;
		public var statutSelectedMaj:String;
		public var moduleSelectedMaj:int;
		public var ta_phrase_maj:TextArea;
		public var ta_traduction_maj:TextArea;
		public var phraseMaj:String;
		public var traductionMaj:String;
		public var commentairesMaj:String;
		public var ta_phrase_majV:StringValidator;
		public var cb_langue_majV:StringValidator;
		public var validatorsPhrase:Array;
		public var valsPhrase:Array;
		public var ta_commentaires_maj:TextArea;
		private var selectedLangMaj:Boolean = false;
		
		private var creationPhrase:Boolean = true;
		private var newPhraseTab:Array = [];
		
		public var chbx_csv:CheckBox;
		public var chbx_xml:CheckBox;
		
		//les popups de publication et d'export
		private var objetPublishPopup:PopupPublishXML;
		private var objetExportPopup:PopupExportCSV;
		public var cb_popup_choix_langue:ComboBox;
		private var dictionnaireXML:XML;
		private var ids:Array = [];
		private	var allModules:Array = [];
		private var phrases:Array = [];
		private var statuts:Array = [];
		private var langues:Array = [];
		private var traductions:Array = [];
		private var modules:Array = [];
		private var modulesXML:Array = [];
		private var modulesCorresp:Array = [];
		
		
		//-------- ONGLET LOCALISATION -------------------------------
		public var cb_code_pays:ComboBox;
		public var cb_align_devise:ComboBox;
		//public var cb_dateFormat:ComboBox;
		public var cb_first_day:ComboBox;
		public var cb_decimal_separator:ComboBox;
		public var cb_use_millier_separator:ComboBox;
		public var cb_type_millier_separator:ComboBox;
		public var cb_arrondi:ComboBox;
		public var cb_signe_negatif:ComboBox;
		public var ti_devise:TextInput;
		public var ti_dateFormat:TextInput;
		public var ti_precision:TextInput;
		public var ns_precision:NumericStepper;
		
		private var firstDay:int;
		private var precision:int;
		private var codePays:String;
		private var dateFormat:String;
		private var decimalSeparator:String;
		private var typeMillierSeparator:String;
		private var rounding:String;
		private var alignDevise:String;
		private var useMillierSeparator:String;
		private var useSigneNegative:String;
		
		
		
		public function GestionTraductionImpl()
		{
			super();
			gestionTraductionDonnees = new GestionTraductionData();
		}
		
		/**
		 * Fonction d'alimentation en données de tous les combos 
		 * */
		public function initCombos():void
		{
			gestionTraductionDonnees.alimentComboLangue(displayComboSearchLangue);
			gestionTraductionDonnees.alimentComboStatut(displayComboSearchStatut);
			gestionTraductionDonnees.alimentComboModule(displayComboSearchModule);	
		}
		
		private function displayComboLocalisationLangue(evt:ResultEvent):void
		{
			cb_code_pays.dataProvider = evt.result as ArrayCollection;
		}
		
		
		private function displayComboSearchLangue(evt:ResultEvent):void
		{
			var dataSearchLangue:ArrayCollection = evt.result as ArrayCollection;
			cb_langue.dataProvider = dataSearchLangue;
			
			var itemGlobal:Object = {SHORT_CODE:"", CODE:"Toutes les langues"};
			dataSearchLangue.addItemAt(itemGlobal, 0);
		}
		
		private function displayComboMajLangue(evt:ResultEvent):void
		{
			var dataMajLangue:ArrayCollection = evt.result as ArrayCollection;
			cb_langue_maj.dataProvider = dataMajLangue;
		}
		
		private function displayComboSearchStatut(evt:ResultEvent):void
		{
			cb_statut_maj.dataProvider = evt.result as ArrayCollection;
			cb_statut.dataProvider = evt.result as ArrayCollection;
			
			var itemGlobal:Object = {STATUT:"Tous les statuts"};
			(cb_statut.dataProvider as ArrayCollection).addItemAt(itemGlobal, 0);
		}
		
		private function displayComboMajStatut(evt:ResultEvent):void
		{
			var dataMajStatut:ArrayCollection = evt.result as ArrayCollection;
			cb_statut_maj.dataProvider = dataMajStatut;
		}
		
		private function displayComboSearchModule(evt:ResultEvent):void
		{
			cb_module_maj.dataProvider = evt.result as ArrayCollection;
			cb_module.dataProvider = evt.result as ArrayCollection;
			
			var itemGlobal:Object = {NOM_MODULE:"Tous les modules"};
			(cb_module.dataProvider as ArrayCollection).addItemAt(itemGlobal, 0);
		}
		
		private function displayComboMajModule(evt:ResultEvent):void
		{
			var dataMajModule:ArrayCollection = evt.result as ArrayCollection;
			cb_module_maj.dataProvider = dataMajModule;
		}
		
		
		/********************************************************************************
		 * 				FONCTIONS D'INTERACTIVITE DES BOUTONS DE VALIDATION
		 * ******************************************************************************/
		
		//----------------- ONGLET LOCALISATION -------------------------------------------------------------------------------------------------------
		
		/**
		 * (IndexChangedEvent) Fonction de clic sur l'onglet localisation
		 * */
		protected function alimentComboCodePays(evt:IndexChangedEvent):void {
			gestionTraductionDonnees.alimentComboLangue(displayComboLocalisationLangue);
		}
		
		/**
		 * (MouseEvent) Fonction de clic sur le bouton de publication du fichier xml de localisation
		 * */
		protected function publishXMLLocalisationFile(evt:MouseEvent):void {
			var devise:String = ti_devise.text;
			
			if(devise == "€") {
				devise = "&#128;";
			}
			else if(devise == "") {
				devise = null;
			}
			dateFormat = ti_dateFormat.text;
			if(dateFormat == "") {
				dateFormat = null;
			}
			precision = ns_precision.value;
	
/* 			Alert.show(codePays + "/" + 
					   devise + "/" + 
					   alignDevise + "/" + 
					   dateFormat + "/" + 
					   firstDay + "/" +
					   decimalSeparator + "/" +
					   useMillierSeparator + "/" +
					   typeMillierSeparator + "/" +
					   precision + "/" +
					   rounding + "/" +
					   useSigneNegative, "paramètres localisation"); */
					   
			generateLocalisationArboXML(codePays, 
										devise, 
										alignDevise, 
										dateFormat, 
										firstDay, 
										decimalSeparator, 
										useMillierSeparator, 
										typeMillierSeparator, 
										int(precision), 
										rounding, 
										useSigneNegative);
		}
		
		/**
		 * (???) Fonction de génération du fichier xml de localisation
		 * */
		 protected function generateLocalisationArboXML(_codePays:String,
		 												_devise:String,
		 												_alignDevise:String,
		 												_dateFormat:String,
		 												_firstDay:int,
		 												_decimalSep:String,
		 												_useMilSep:String,
		 												_typeMilSep:String,
		 												_precision:int,
		 												_rounding:String,
		 												_useSigneNeg:String):void {
		 	var localisationArboXML:XML = new XML();
		 	localisationArboXML = <localization>
			 						  	<currencySymbol>{_devise}</currencySymbol>
			 						  	<dateFormat>{_dateFormat}</dateFormat>
			 						  	<firstDayOfWeek>{_firstDay}</firstDayOfWeek>
			 						  	<currencyFormatter>
											<alignSymbol>{_alignDevise}</alignSymbol>
											<currencySymbol>{_devise}</currencySymbol>
											<decimalSeparatorTo>{_decimalSep}</decimalSeparatorTo>
											<precision>{_precision}</precision>
											<rounding>{_rounding}</rounding>
											<thousandsSeparatorTo>{_typeMilSep}</thousandsSeparatorTo>
											<useNegativeSign>{_useSigneNeg}</useNegativeSign>
											<useThousandsSeparator>{_useMilSep}</useThousandsSeparator>
										</currencyFormatter>
									</localization>;
		 
		 	//Alert.show(localisationArboXML.toString(), "arbo xml localisation");		
		 	
		 	gestionTraductionDonnees.createXmlFileLocalisation(localisationArboXML, _codePays.toUpperCase() + ".xml", createXMLFileLocalisationResultHandler);
		 						
		 }
		
		/**
		 * (MouseEvent) Fonction d'initialisation des paramètres de localisation
		 * */
		public function cancelLocalisationParam(evt:MouseEvent):void {
			cb_align_devise.text = cb_align_devise.prompt;
			cb_align_devise.selectedIndex = -1;
			cb_arrondi.text = cb_arrondi.prompt;
			cb_arrondi.selectedIndex = -1;
			cb_code_pays.text = cb_code_pays.prompt;
			cb_code_pays.selectedIndex = -1;
			cb_first_day.text = cb_first_day.prompt;
			cb_first_day.selectedIndex = -1;
			cb_decimal_separator.text = cb_decimal_separator.prompt;
			cb_decimal_separator.selectedIndex = -1;
			cb_use_millier_separator.text = cb_use_millier_separator.prompt;
			cb_use_millier_separator.selectedIndex = -1;
			cb_type_millier_separator.text = cb_type_millier_separator.prompt;
			cb_type_millier_separator.selectedIndex = -1;
			cb_signe_negatif.text = cb_signe_negatif.prompt;
			cb_signe_negatif.selectedIndex = -1;
			ti_devise.text = "";
			ti_dateFormat.text = "";
			ns_precision.value = 0;
		}
		
		//----------------- ONGLET TRADUCTION ----------------------------------------------------------------------------------------------------
		
		/**
		 * (MouseEvent) Fonction de clic sur le bouton de recherche
		 * */
		protected function clickSearchHandler(evt:MouseEvent):void
		{	
			initFormPhrase();		
			checkSearchCriters();
		}
		
		/**
		 * (MouseEvent) Fonction d'export du fichier à traduire au format CSV
		 * */
		protected function exportFileCSV(evt:MouseEvent):void
		{
			var languesExport:Array = [];
			
			//initialiser les tableaux languesExport, ids, allModules, phrases source, statuts, langues et traductions
			if(languesExport.length != 0) {
				languesExport.splice(0, languesExport.length);
			}
			
			if(objetExportPopup.cb_popup_export.selectedIndex == -1) {
				alertMessage("export", "selection_langue_liste_popup");
			}
			else {
				/* for each(var obj:Object in dg_resultatPhrase.dataProvider as ArrayCollection) {
					if(obj.LANGUAGE_CODE == objetExportPopup.codeLangue) {
						languesExport.push(1);
					}
					else {
						languesExport.push(0);
					}
				}
				if(languesExport.indexOf(1)!= -1) {
					//afficher la liste des phrases correspondant a la langue sélectionnée
					gestionTraductionDonnees.getListePhrases(objetExportPopup.codeLangue, "Source validée", -1, null, listePhraseExportResultHandler);
				}
				else {
					alertMessage("export", "selection_langue_liste_tableau");
				} */
				//Alert.show(chbx_csv.selected.toString(),"Sélection source validée");
				
				var choixStatut:String;
				if(chbx_csv.selected == true) {
					//Alert.show("source validée", "statut");
					choixStatut = "Source validée";
				}
				else {
					//Alert.show("source proposée", "statut");
					choixStatut = "Source proposée";
				}
				gestionTraductionDonnees.getListePhrases(null, choixStatut, -1, null, listePhraseExportResultHandler);
				
			}		
		}
		
		/**
		 * (MouseEvent) Fonction d'ouverture du popup d'export du fichier à traduire au format CSV
		 * */
		protected function openExportFileCSVPopup(evt:MouseEvent):void
		{
			var statutsExport:Array = [];
			
			if(statutsExport.length != 0) {
				statutsExport.splice(0, statutsExport.length);
			}
			
			if(dg_resultatPhrase.dataProvider != null) {
				if((dg_resultatPhrase.dataProvider as ArrayCollection).length > 0) {
					//si le checkbox de la source validee est coché
					if(chbx_csv.selected == true) {
						for each(var obj:Object in dg_resultatPhrase.dataProvider as ArrayCollection) {
							if(obj.STATUT == "Source validée") {
								statutsExport.push(1);
							}
							else {
								statutsExport.push(0);
							}
						}
						
						if(statutsExport.indexOf(0)!= -1) {
							alertMessage("export", "selection_statut_recherche_validee");
						}
						else {
							createCSVPopup();
						}
						//Alert.show("cochée", "source validée");
					}
					//si le checkbox de la source validee n'est pas coché
					else {
						for each(var obj1:Object in dg_resultatPhrase.dataProvider as ArrayCollection) {
							if(obj1.STATUT != "Traduction validée") {
								statutsExport.push(1);
							}
							else {
								statutsExport.push(0);
							}
						}
						
						if(statutsExport.indexOf(0)!= -1) {
							alertMessage("export", "selection_statut_recherche_no_trad_val");
						}
						else {
							createCSVPopup();
						}
						//Alert.show("non cochée", "source validée");
					} 
					//Alert.show((dg_resultatPhrase.dataProvider as ArrayCollection).length.toString(), "nb phrases datagrid");
				}
				else {
					alertMessage("export", "affichage_liste");
				}
			}
			else {
				alertMessage("export", "affichage_liste");
			}
		}
		
		/**
		 * Fonction de création du popup du fichier d'export csv
		 * */
		 private function createCSVPopup():void {
		 	objetExportPopup = PopupExportCSV(PopUpManager.createPopUp(this, PopupExportCSV, true));
			PopUpManager.centerPopUp(objetExportPopup);
			objetExportPopup.btn_export.addEventListener(MouseEvent.CLICK, exportFileCSV);
		 }
		
		
		/**
		 * (MouseEvent) Fonction d'ouverture de la popup pour la publication du fichier traduit au format XML
		 * */
		protected function openPublishFileXMLPopup(evt:MouseEvent):void
		{
			var statutsExport:Array = [];
			
			if(statutsExport.length != 0) {
				statutsExport.splice(0, statutsExport.length);
			}
			 
			if(dg_resultatPhrase.dataProvider != null) {
				if((dg_resultatPhrase.dataProvider as ArrayCollection).length > 0) {
					//si le checkbox de la traduction validee est coché
					if(chbx_xml.selected == true) {
						for each(var obj:Object in dg_resultatPhrase.dataProvider as ArrayCollection) {
							if(obj.STATUT == "Traduction validée") {
								statutsExport.push(1);
							}
							else {
								statutsExport.push(0);
							}
						}
						if(statutsExport.indexOf(0)!= -1) {
							alertMessage("publish", "selection_statut_recherche");
						}
						else {
							createXMLPopup();
						}
					}
					//si le checkbox de la traduction validee n'est pas coché
					else {
						createXMLPopup();
					}
				}
				else {
					alertMessage("publish", "affichage_liste");
				}
			}
			else {
				alertMessage("publish", "affichage_liste");
			}
		}
		
		/**
		 * Fonction de création du popup du fichier de publication xml
		 * */
		private function createXMLPopup():void {
			objetPublishPopup = PopupPublishXML(PopUpManager.createPopUp(this, PopupPublishXML, true));
			PopUpManager.centerPopUp(objetPublishPopup);
			objetPublishPopup.btn_publish.addEventListener(MouseEvent.CLICK, publishFileXML);
		}
		
		
		/**
		 * Fonction de création de l'arbo XML du fichier de publication
		 * */
		private function createDictionnaireXML():void 
		{
			var df:DateFormatter = new DateFormatter();
			df.formatString = "DD/MM/YY JJ:SS";
			var now:String = df.format(new Date());
			
			//initialisation des tableaux modules et modulesXML
			if(modules.length != 0) {
				modules.splice(0, modules.length);
			}
			if(modulesXML.length != 0) {
				modulesXML.splice(0, modulesXML.length);
			}
			
			//création du noeud racine du XML
			dictionnaireXML = new XML();
			dictionnaireXML = <dictionnaire langue={objetPublishPopup.codeLangue.toLowerCase()} date={now}></dictionnaire>;
			
			//création de tous les noeuds enfants du noeud racine
			for(var i:int = 0; i < allModules.length; i++) {
				var idModule:int;
				var idNewModule:int;
					
				//reinitialiser le tableau modulesCorresp
				if(modulesCorresp.length > 0) {
					modulesCorresp.splice(0, modulesCorresp.length);
				}	
					
				if(i == 0) {
					//ajouter le module à la liste effective des modules
					modules.push(allModules[i]);
					//créer l'objet XML module
					createModuleXML(allModules[i]);
					//créer l'objet XML phrase
					createPhraseXML(ids[i], traductions[i], i);
				}
				else if(i > 0) {
					for(var j:int = 0; j < modules.length; j++) {
						if(allModules[i] == modules[j]) {
							modulesCorresp.push(1);
							idModule = j;
						}
						else if(allModules[i] != modules[j]) {
							modulesCorresp.push(0);
						}
					}
					//si la valeur 1 n'est pas contenue dans le tableau de correspondance des modules
					if(modulesCorresp.indexOf(1) == -1) {
						//ajouter le module à la liste effective des modules
						modules.push(allModules[i]);
						//recupérer l'index du module nouvellement ajouté dans la liste effective des modules
						idNewModule = modules.indexOf(allModules[i]);
						//créer l'objet XML module
						createModuleXML(allModules[i]);
						//créer l'objet XML phrase
						createPhraseXML(ids[i], traductions[i], idNewModule);
					}
					//si la valeur 1 est contenue dans le tableau de correspondance des modules
					else {
						//créer l'objet XML phrase
						createPhraseXML(ids[i], traductions[i], idModule);
					}
				}
			}
			//ajouter tous les noeuds module XML dans le dictionnaire XML
			for each(var obj:XML in modulesXML as Array) {
				dictionnaireXML.appendChild(obj);
			}
		}
		
		/**
		 * (String) Fonction de création d'un noeud module XML
		 * */
		private function createModuleXML(module:String):void
		{
			var moduleXML:XML = new XML();
			moduleXML = <module typeModule={module}></module>;
			modulesXML.push(moduleXML);
		}
		
		/**
		 * (int, String, int) Fonction de création d'un noeud phrase XML
		 * */
		private function createPhraseXML(id:int, value:String, moduleId:int):void
		{
			var phraseXML:XML = new XML();
			phraseXML = <phrase id={id} valeur={value} />;
			(modulesXML[moduleId] as XML).appendChild(phraseXML);
		}
		
		/**
		 * (MouseEvent) Fonction de publication du fichier traduit au format XML
		 * */
		public function publishFileXML(evt:MouseEvent):void
		{			
			var languesExport:Array = [];
			
			//initialiser les tableaux languesExport, ids, allModules et traductions
			if(languesExport.length != 0) {
				languesExport.splice(0, languesExport.length);
			}
			if(ids.length != 0) {
				ids.splice(0, ids.length);
			}
			if(allModules.length != 0) {
				allModules.splice(0, allModules.length);
			}
			if(traductions.length != 0) {
				traductions.splice(0, traductions.length);
			}
			
			if(objetPublishPopup.cb_popup_publish.selectedIndex == -1) {
				alertMessage("publish", "selection_langue_liste_popup");
			}
			else {
				//stocker les valeurs des ids, modules et traductions pour la langue sélectionnée
				for each(var obj:Object in dg_resultatPhrase.dataProvider as ArrayCollection) {
					if(obj.LANGUAGE_CODE == objetPublishPopup.codeLangue) {
						ids.push(obj.ID);
						allModules.push(obj.NOM_MODULE);
						traductions.push(obj.TRADUCTION);
						
						languesExport.push(1);
					}
					else {
						languesExport.push(0);
					}
				}
				if(languesExport.indexOf(1)!= -1) {
					//générer l'arbo XML
	 				createDictionnaireXML();
	 				//écrire le fichier XML
	 				var fileName:String = "dictionnaire_" + objetPublishPopup.codeLangue.toLowerCase() + ".xml";
	  				gestionTraductionDonnees.createXmlFile(dictionnaireXML, fileName, createXmlFileResultHandler);
	  				//fermer le popup
					objetPublishPopup.closePopup();
				}
				else {
					alertMessage("publish", "selection_langue_liste_tableau");
				}
			}
		}
		
		
		/**
		 * (MouseEvent) Fonction d'initialisation du bloc phrase pour la création d'une phrase
		 * */
		public function initBlocPhrase(evt:MouseEvent):void
		{
			//remplir les combos
			gestionTraductionDonnees.alimentComboLangue(displayComboMajLangue);
			gestionTraductionDonnees.alimentComboStatut(displayComboMajStatut);
			gestionTraductionDonnees.alimentComboModule(displayComboMajModule);	
			
			//afficher le bloc 'phrase'
			cv_edit_phrase.visible = true;
			
			//préciser le mode "création" de la phrase
			creationPhrase = true;

			//initialiser les champs
			initFormPhrase();
		}
		
		
		private function initFormPhrase():void
		{
			cb_statut_maj.selectedIndex = -1;
			cb_module_maj.selectedIndex = -1;
			cb_langue_maj.selectedIndex = -1;
			ta_phrase_maj.text = "";
			ta_commentaires_maj.text = "";
			ta_traduction_maj.text = "";
			lb_identifiant.text = "";
		}
		
		/**
		 * (MouseEvent) Fonction de suppression d'une phrase
		 * */
		public function deletePhrase(evt:MouseEvent):void
		{
			if(creationPhrase == false) {
				Alert.show("Voulez vous supprimer la phrase ID = " + idSelectedResult + " ?", "Suppression phrase", Alert.OK | Alert.CANCEL, this, confirmPhraseToDelete, null, Alert.OK);
			}
			else {
				Alert.show("Veuillez sélectionner une phrase à supprimer", "Suppression phrase");
			}
		}
		
		/**
		 * (CloseEvent) Fonction de confirmation de suppression d'une phrase
		 * */
		private function confirmPhraseToDelete(evt:CloseEvent):void
		{
			if(evt.detail == Alert.OK) {
				gestionTraductionDonnees.deletePhrase(int(idSelectedResult), deletePhraseResultHandler);
			}
		}
		
		/**
		 * (MouseEvent) Fonction de création ou de modification d'une phrase
		 * */
		public function setPhrase(evt:MouseEvent):void
		{
			//vérifier la saisie des champs
			validatePhraseSetted();
			
			phraseMaj = ta_phrase_maj.text;
			traductionMaj = ta_traduction_maj.text;
			commentairesMaj = ta_commentaires_maj.text;
			
			if(traductionMaj == "") {
				traductionMaj = null;
			}
			if(commentairesMaj == "") {
				commentairesMaj = null;
			}
			
			//cas du mode "création" de la phrase
			if(creationPhrase == true) {
				//si tous les champs obligatoires sont renseignés
				if(valsPhrase.length == 0 && cb_statut_maj.selectedIndex !=  -1 && cb_module_maj.selectedIndex != -1) {
					//--> afficher la liste des phrases appartenant au MODULE sélectionné pour la création
					//REMPLACER "STATUSELECTEDMAJ" PAR "MODULESELECTEDMAJ"
					gestionTraductionDonnees.getListePhrases(null, statutSelectedMaj, 0, null, listePhraseModuleResultHandler);
				}
				else {
					Alert.show("Veuillez renseigner les champs obligatoires !", "Avertissement");
				}
			}
			//cas du mode "modification" de la phrase
			else {
				if(valsPhrase.length == 0) {
					var selectedLangue:String;
					var tab:Array = [];
 					
 					if(cb_langue_maj.text == cb_langue_maj.prompt) {
 						langueSelectedResult = null;
 					}
 					else {
 						if(cb_langue_maj.text == langueSelectedMaj2) {
 							langueSelectedResult = langueSelectedMaj;
 						}
 					}
 					//boucle de comparaison 
 					for each(var obj:Object in dg_resultatPhrase.dataProvider as ArrayCollection) {
						if(obj.LIBELLE_INITIAL == phraseMaj) {
							tab.push(1);
						}
						else {
							tab.push(0);				
						}
					}
					
					if(tab.indexOf(1) == -1) {
						//Alert.show("Cette phrase n'existe pas encore dans ce module", "Autorisation création !");
						//modifier la phrase
	 					gestionTraductionDonnees.createPhrase(cb_statut_maj.text, moduleSelectedMaj,
															  ta_phrase_maj.text, ta_commentaires_maj.text,
															  langueSelectedResult, ta_traduction_maj.text,
															  1, "",
															  "", 0, "",
															  updatePhraseResultHandler, int(idSelectedResult));
					}
					else {
						Alert.show("La même phrase existe déja dans ce module", "Interdiction création !");
					}											  
				}
				else {
					Alert.show("Veuillez renseigner les champs obligatoires !", "Avertissement");
				}
			}
		}
		
		/**
		 * (ResultEvent) Fonction du resultat du retour de la liste des phrases
		 * */
		private function listePhraseResultHandler(evt:ResultEvent):void
		{
			dg_resultatPhrase.dataProvider = evt.result as ArrayCollection;
			
		}
		
		/**
		 * (ResultEvent) Fonction du resultat du retour de la liste des phrases en sélectionnant un module pour création/modification de phrase
		 * */
		private function listePhraseModuleResultHandler(evt:ResultEvent):void
		{
			var tab:Array = [];
			
			dg_resultatPhrase.dataProvider = evt.result as ArrayCollection;
			
			 //--> pour chaque item de la liste, comparer la valeur de la phrase avec celle créée
			for each(var obj:Object in dg_resultatPhrase.dataProvider as ArrayCollection) {
				if(obj.LIBELLE_INITIAL == phraseMaj) {
					tab.push(1);
				}
				else {
					tab.push(0);				
				}
			}
			
			if(tab.indexOf(1) == -1) {
				//Alert.show("Cette phrase n'existe pas encore dans ce module", "Autorisation création !");
				//créer la phrase		   
				gestionTraductionDonnees.createPhrase(statutSelectedMaj, moduleSelectedMaj,
													  phraseMaj, commentairesMaj,
													  langueSelectedMaj, traductionMaj,
													  1, "",
													  "", 0, "",
													  addPhraseResultHandler);
			}
			else {
				Alert.show("La même phrase existe déja dans ce module", "Interdiction création !");
			}
		}
		
		
		
		/**
		 * (ResultEvent) Fonction du resultat de la création d'une phrase
		 * */
		private function addPhraseResultHandler(evt:ResultEvent):void
		{
			dg_resultatPhrase.dataProvider = evt.result as ArrayCollection;
			
			//reafficher la liste des phrases correspondant au MODULE
			gestionTraductionDonnees.getListePhrases(null, statutSelectedMaj, 0, null, listePhraseResultHandler);
			
			/* if(evt.result > 0) {
				Alert.show("Création de la phrase réussie", "Internationalisation");
			}
			else {
				Alert.show("Echec de la création de la phrase", "Internationalisation");
			} */
		}
		
		/**
		 * (ResultEvent) Fonction du resultat de la modification d'une phrase
		 * */
		private function updatePhraseResultHandler(evt:ResultEvent):void
		{
			dg_resultatPhrase.dataProvider = evt.result as ArrayCollection;
			
			//reafficher la liste des phrases correspondant au MODULE
			gestionTraductionDonnees.getListePhrases(null, statutSelectedMaj, 0, null, listePhraseResultHandler);
			/* if(evt.result > 0) {
				Alert.show("Modification de la phrase réussie", "Internationalisation");
			}
			else {
				Alert.show("Echec de la modification de la phrase", "Internationalisation");
			} */
		}
		
		/**
		 * (ResultEvent) Fonction du resultat de la suppression d'une phrase
		 * */
		private function deletePhraseResultHandler(evt:ResultEvent):void
		{
			dg_resultatPhrase.dataProvider = evt.result as ArrayCollection;

			//reafficher la liste des phrases correspondant au MODULE
			gestionTraductionDonnees.getListePhrases(null, statutSelectedMaj, 0, null, listePhraseResultHandler);
			
			/* 			if(evt.result > 0) {
				Alert.show("Suppression de la phrase réussie", "Internationalisation");
			}
			else {
				Alert.show("Echec de la suppression de la phrase", "Internationalisation");
			} */
		}
		
		/**
		 * (ResultEvent) Fonction du resultat de l'écriture du fichier XML de publication
		 * */
		private function createXmlFileResultHandler(evt:ResultEvent):void
		{
			trace("TEST :\n" + getQualifiedClassName(evt.result) +
					"\n" + ObjectUtil.toString(evt.result));
			
			if(evt.result > 0) {
				Alert.show("Ecriture du fichier XML réussie", "Internationalisation");
			}
			else {
				Alert.show("Echec de l'écriture du fichier XML", "Internationalisation");
			}
		}
		
		
		private function createXMLFileLocalisationResultHandler(evt:ResultEvent):void
		{
			trace("TEST :\n" + getQualifiedClassName(evt.result) +
					"\n" + ObjectUtil.toString(evt.result));
			
			if(evt.result != "ERROR") {
				Alert.show("Ecriture du fichier XML réussie", "Internationalisation");
			}
			else {
				Alert.show("Echec de l'écriture du fichier XML", "Internationalisation");
			}
		}
		
		
		/**
		 * (ResultEvent) Fonction du resultat du retour de la liste des phrases pour l'export CSV
		 * */
		private function listePhraseExportResultHandler(evt:ResultEvent):void
		{
			var fileName:String = "atraduire_" + objetExportPopup.codeLangue.toLowerCase() + ".csv";
			
			dg_resultatPhrase.dataProvider = evt.result as ArrayCollection;
			
			//exporter le fichier 
			if(cb_langue.selectedIndex == -1) {
				gestionTraductionDonnees.exportCsvFile(objetExportPopup.codeLangue, statutSelectedSearch, moduleSelectedSearch, phraseSearched, fileName, exportCsvFileResultHandler);
			}
			else {
				gestionTraductionDonnees.exportCsvFile(langueSelectedSearch, statutSelectedSearch, moduleSelectedSearch, phraseSearched, fileName, exportCsvFileResultHandler);
			}
			//fermer le popup
			objetExportPopup.closePopup();
		}
		
		private function exportCsvFileResultHandler(evt:ResultEvent):void
		{
			trace("TEST :\n" + getQualifiedClassName(evt.result) +
						"\n" + ObjectUtil.toString(evt.result));

			if(evt.result != "ERROR") {
				var url:String = index.urlBackoffice + "/fr/consotel/consoprod/gestiontraduction/fichiersatraduire/download.cfm" ;
	            var request:URLRequest = new URLRequest(url);         
	           	var variables:URLVariables = new URLVariables();
	            variables.FILE_NAME = evt.result;
	            request.data = variables;
	            request.method = URLRequestMethod.POST;
	            navigateToURL(request,"_blank");
			}
			else {
				Alert.show("Erreur sur l'export !", "Internationalisation");
			}
		}
		
		/********************************************************************************
		 * 			FONCTION D'AFFICHAGE DES MESSAGES D'ALERTE
		 * ******************************************************************************/
		
		private function alertMessage(type:String, value:String):void
		{
			switch(type) {
				case "export":
					switch(value) {
						case "selection_langue_liste_tableau":
							Alert.show("Veuillez sélectionner une langue présente dans la liste du tableau !", "Erreur export");
							break;
						case "selection_langue_liste_popup":
							Alert.show("Veuillez sélectionner une langue dans la liste déroulante !", "Erreur export");
							break;
						case "selection_statut_recherche_validee":
							Alert.show("Veuillez afficher la liste des phrases en statut 'source validee' !", "Erreur export");
							break;
						case "selection_statut_recherche_no_trad_val":
							Alert.show("Veuillez afficher la liste des phrases en statut autre que 'traduction validee' !", "Erreur export");
							break;
						case "affichage_liste":
							Alert.show("Veuillez afficher la liste des phrases !", "Erreur export");
							break;
					}
					break;
				case "publish":
					switch(value) {
						case "selection_langue_liste_tableau":
							Alert.show("Veuillez sélectionner une langue présente dans la liste du tableau !", "Erreur publication");
							break;
						case "selection_langue_liste_popup":
							Alert.show("Veuillez sélectionner une langue dans la liste déroulante !", "Erreur publication");
							break;
						case "selection_statut_recherche":
							Alert.show("Veuillez afficher la liste des phrases en statut 'traduction validee' !", "Erreur publication");
							break;
						case "affichage_liste":
							Alert.show("Veuillez afficher la liste des phrases !", "Erreur publication");
							break;
					}
					break;
			}
		}
		
		
		/********************************************************************************
		 * 			FONCTIONS D'INTERACTIVITE DES BOUTONS / CHAMPS DE SELECTION
		 * ******************************************************************************/
		
			
		//-------------------BLOC 'RECHERCHE D'UNE PHRASE'--------------------------------
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'langue' du bloc de recherche
		 * */
		public function cbLangueSelectedItem(evt:ListEvent):void
		{
			langueSelectedSearch = cb_langue.selectedItem.SHORT_CODE;
			//Alert.show(langueSelectedSearch, "Code langue");
		} 
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'statut' du bloc de recherche
		 * */
 		public function cbStatutSelectedItem(evt:ListEvent):void
		{
			statutSelectedSearch = cb_statut.selectedItem.STATUT;
		} 
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'module' du bloc de recherche
		 * */
 		public function cbModuleSelectedItem(evt:ListEvent):void
		{
			moduleSelectedSearch = cb_module.selectedItem.MODULE_CONSOTELID;
			//Alert.show(cb_module.selectedItem.NOM_MODULE, "module");
		} 
		
		/**
		 * Fonction de vérification des champs sélectionnés dans le bloc de recherche
		 * */
		protected function checkSearchCriters():void
		{		
 			//gestion de la phrase
			phraseSearched = ti_recherche_phrase.text;
		 	if(phraseSearched == "")
 				phraseSearched = null;
 			
 			//gestion de la langue
 			if(cb_langue.selectedIndex < 1) {
 				langueSelectedSearch = null;
 			}
 			
 			//gestion du statut
 			if(cb_statut.selectedIndex < 1) {
 				statutSelectedSearch = null;
 			}
 			
 			//gestion du module
  			if(cb_module.selectedIndex < 1) {
 				moduleSelectedSearch = 0;
 			} 

 			if(phraseSearched == null && langueSelectedSearch != null && statutSelectedSearch == null && moduleSelectedSearch == 0) {
 				
 				//Alert.show("afficher la liste des phrases correspondant a la langue " + langueSelectedSearch, "Liste phrases");
 				gestionTraductionDonnees.getListePhrasesLangue(langueSelectedSearch, statutSelectedSearch, moduleSelectedSearch, phraseSearched, listePhraseResultHandler);
 				 	
 			}
 			else {
 				//Alert.show("afficher la liste entière des phrases", "Liste phrases");
 				gestionTraductionDonnees.getListePhrases(langueSelectedSearch, statutSelectedSearch, moduleSelectedSearch, phraseSearched, listePhraseResultHandler);		
 			}
 			//Alert.show(phraseSearched + "/" + langueSelectedSearch + "/" + statutSelectedSearch + "/" + moduleSelectedSearch, "parametres recherche");
	
		}
		
		
		//------------------BLOC 'TABLEAU DE RESULTATS DE LA RECHERCHE'-------------------
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du tableau de résultats de la recherche
		 * */
		public function dgSelectedItem(evt:ListEvent):void
		{
			//afficher le bloc 'phrase'
			cv_edit_phrase.visible = true;
			
			//préciser le mode "modification" de la phrase
			creationPhrase = false;
			
			//verification de l'alimentation des combobox
			if(cb_langue_maj.dataProvider == "" || cb_langue_maj.dataProvider == null) {
				gestionTraductionDonnees.alimentComboLangue(displayComboMajLangue);
				//Alert.show("combo vide");
			}
			else {
				if(dg_resultatPhrase.selectedItem.LANGUAGE_CODE == "" || dg_resultatPhrase.selectedItem.LANGUAGE_CODE == null) {
					cb_langue_maj.text = cb_langue_maj.prompt;
					langueSelectedResult = null;
				}
				else {
					cb_langue_maj.text = evt.currentTarget.selectedItem.LANGUE;
					langueSelectedResult = evt.currentTarget.selectedItem.LANGUAGE_CODE;
				}
			}

 			//récupérer la valeur de chaque colonne de la ligne sélectionnée du tableau de résultats de recherche
			idSelectedResult = evt.currentTarget.selectedItem.ID;
			statutSelectedResult = evt.currentTarget.selectedItem.STATUT;
			moduleSelectedResult = evt.currentTarget.selectedItem.NOM_MODULE;
			phraseSelectedResult = evt.currentTarget.selectedItem.LIBELLE_INITIAL;
			commentaireSelectedResult = evt.currentTarget.selectedItem.COMMENTAIRE;
			traductionSelectedResult = evt.currentTarget.selectedItem.TRADUCTION;
			
			//afficher les différentes valeurs de la ligne sélectionnée dans le tableau de la gestion de la phrase
			lb_identifiant.text = idSelectedResult;
			cb_statut_maj.text = statutSelectedResult;
			cb_module_maj.text = moduleSelectedResult;
			ta_phrase_maj.text = phraseSelectedResult;
			ta_commentaires_maj.text = commentaireSelectedResult;
			ta_traduction_maj.text = traductionSelectedResult;
			
			//Alert.show(moduleSelectedResult + "/" + phraseSelectedResult, "MODULE/PHRASE");			
		}
		
		//----------------BLOC 'PHRASE' (CREATION / MODIFICATION)-----------------------
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'langue'
		 * */
		public function cbLangueMajSelectedItem(evt:ListEvent):void
		{
			langueSelectedMaj = cb_langue_maj.selectedItem.SHORT_CODE;
			langueSelectedMaj2 = cb_langue_maj.selectedItem.CODE;
		} 
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'statut'
		 *  * */
 		public function cbStatutMajSelectedItem(evt:ListEvent):void
		{
			statutSelectedMaj = cb_statut_maj.selectedItem.STATUT;
		} 
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'module'
		 * */
 		public function cbModuleMajSelectedItem(evt:ListEvent):void
		{
			moduleSelectedMaj = cb_module_maj.selectedItem.MODULE_CONSOTELID;
		} 
		
		
		//----------------BLOC LOCALISATION-----------------------
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'code pays'
		 * */
		public function cbLocalisationCodePaysItem(evt:ListEvent):void
		{
			codePays = cb_code_pays.selectedItem.SHORT_CODE;
			//Alert.show(codePays, "code pays");
		} 
		
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'alignement devise'
		 * */
		public function cbAlignDeviseItem(evt:ListEvent):void
		{
			alignDevise = cb_align_devise.selectedItem.label;
			//Alert.show(alignDevise, "alignement devise");
		}
		
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'format date'
		 * */
		public function cbFirstDayItem(evt:ListEvent):void
		{
			firstDay = cb_first_day.selectedItem.data;
			Alert.show(firstDay.toString(), "1er jour semaine");
		}  
		
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'separateur decimal'
		 * */
		public function cbDecimalSeparatorItem(evt:ListEvent):void
		{
			decimalSeparator = cb_decimal_separator.selectedItem.data;
			//Alert.show(decimalSeparator, "séparateur décimal");
		}
		
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'utilisation separateur millier'
		 * */
		public function cbUseMillierSeparatorItem(evt:ListEvent):void
		{
			useMillierSeparator= cb_use_millier_separator.selectedItem.label;
			//Alert.show(useMillierSeparator, "utilisation separateur millier");
		}
		
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'type separateur millier'
		 * */
		public function cbTypeMillierSeparatorItem(evt:ListEvent):void
		{
			typeMillierSeparator = cb_type_millier_separator.selectedItem.data;
			//Alert.show(typeMillierSeparator, "type séparateur millier");
		}
		
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'arrondi'
		 * */
		public function cbArrondiItem(evt:ListEvent):void
		{
			rounding = cb_arrondi.selectedItem.label;
			//Alert.show(rounding, "arrondi");
		}
		
		
		/**
		 * (ListEvent) Fonction de sélection d'une ligne du combobox 'utilisation signe negatif'
		 * */
		public function cbUseSigneNegativeItem(evt:ListEvent):void
		{
			useSigneNegative = cb_signe_negatif.selectedItem.label;
			//Alert.show(useSigneNegative, "utilisation signe négatif");
		}
		
		
		/**
		 * Fonction de validation de création/modification d'une phrase
		 * */
		protected function validatePhraseSetted():void
		{
			valsPhrase = new Array();
			validatorsPhrase = [ta_phrase_majV];
			valsPhrase = Validator.validateAll(validatorsPhrase);
		}
	}
}