package intranet.M25.ihm
{
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import intranet.M25.entity.ModelLocator;
    import intranet.M25.entity.StaticData;
    import intranet.M25.entity.enum.EBoolean;
    import intranet.M25.entity.vo.CollecteClientVO;
    import intranet.M25.entity.vo.CollecteTypeVO;
    import intranet.M25.entity.vo.FilterCollecteTypeVO;
    import intranet.M25.entity.vo.OperateurVO;
    import intranet.M25.entity.vo.SimpleEntityVO;
    import intranet.M25.event.CollecteEvent;
    import intranet.M25.ihm.component.alert.AlertExtended;
    import intranet.M25.ihm.component.alert.AlertExtendedPopup;
    import intranet.M25.service.collectetype.CollecteTypeService;
    import intranet.M25.utils.MonthChooser;
    
    import mx.collections.ArrayCollection;
    import mx.containers.HBox;
    import mx.containers.TabNavigator;
    import mx.containers.TitleWindow;
    import mx.containers.VBox;
    import mx.controls.Button;
    import mx.controls.ComboBox;
    import mx.controls.DataGrid;
    import mx.controls.Image;
    import mx.controls.Label;
    import mx.controls.LinkButton;
    import mx.controls.TextInput;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.validators.EmailValidator;
    import mx.validators.NumberValidator;
    import mx.validators.Validator;

    [Bindable]
    /**
     * Cette classe étend la classe <code>TitleWindow</code> et permet d'afficher une popup
     * visant à créer/éditer/consulter les informations d'une collecte cliente.
     * Elle assure le <b>Pilotage</b> de l'IHM  associé <code>CollecteClientFicheIHM</code>
     *
     */
    public class CollecteClientFicheImpl extends TitleWindow
    {
        public var dataLocator:ModelLocator;
        // IHM 
        public var i_dgListChaineFacturation:DataGrid;
        public var i_hbMode:HBox;
        public var i_btnOk:Button;
        public var i_btnAnnuler:Button;
        public var i_btnSup:Button;
        public var i_btnExport:Button;
        public var i_btnExportFactures:Button;
        public var i_tabNav:TabNavigator;
        public var i_btnTestId:Button;
        public var i_imgIdentifValid:Image;
        public var i_cbxTemplate:ComboBox;
        public var i_cbxOperateur:ComboBox;
        public var i_cbxDataType:ComboBox;
        public var i_hbIdentifiant:HBox;
        public var i_txiDelaiAlert:TextInput;
        public var i_cbxRetroactivite:ComboBox;
        public var i_vbImportInfo:VBox;
        public var i_dfdPageImport:MonthChooser;
        
        // Validateurs
        public var libelle_validator:Validator;
        public var roic_validator:Validator;
        public var template_validator:NumberValidator;
        public var plageImport_validator:Validator;
        public var delaiAlert_validator:NumberValidator;
        public var mail_validator:EmailValidator;
        public var login_validator:Validator;
        public var mdp_validator:Validator;
        public var validatorEnabled:Boolean = false;
        // DATA
        private var _idRacine:Number;
        private var _currentCollectVO:CollecteClientVO;
        private var _chaineFacturationListe:ArrayCollection;
        private var _templateListFiltered:ArrayCollection = new ArrayCollection();
        private var _filterVO:FilterCollecteTypeVO = new FilterCollecteTypeVO();
        // CONTROL
        public static const MODE_CREATION:String = "mode_creation";
        public static const MODE_EDITION:String = "mode_edition";
        public static const INDEX_PARAM_IMPORT:Number = 0;
        public static const INDEX_CF:Number = 1;
        public static const INDEX_INFO:Number = 2;
        public var identificationOk:Boolean;
        private var _mode:String;
        private var _validatorArr:Array;
        public var infoFicheHasChanged:Boolean = false;
        // SERVICE				
        private var collecteService:CollecteTypeService;

        // Data
        public function CollecteClientFicheImpl()
        {
            super();
        }

        public function init():void
        {
            this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
        }

        public function onCreationCompleteHandler(event:FlexEvent):void
        {
            dataLocator = ModelLocator.getInstance();
            this.removeEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
            // récupération de la liste des chaines de facture
            ModelLocator.getInstance().generateTestChaineFacturationList();
            initDisplay();
            initListeners();
            initData();
        }

        private function initListeners():void
        {
            i_btnTestId.addEventListener(MouseEvent.CLICK, i_btnTestId_clickHandler);
            i_cbxTemplate.addEventListener(Event.CHANGE, i_cbxTemplate_changeHandler);
            i_cbxOperateur.addEventListener(Event.CHANGE, i_cbxOperateur_changeHandler);
            i_cbxDataType.addEventListener(Event.CHANGE, i_cbxDataType_changeHandler);
            i_btnOk.addEventListener(MouseEvent.CLICK, i_btnOk_clickHandler);
            i_btnAnnuler.addEventListener(MouseEvent.CLICK, i_btnAnnuler_clickHandler);
            this.addEventListener(CloseEvent.CLOSE, i_btnAnnuler_clickHandler);
            // i_btnSup.addEventListener(MouseEvent.CLICK, i_btnSup_clickHandler);
            i_btnExport.addEventListener(MouseEvent.CLICK, i_btnExport_clickHandler);
            i_btnExportFactures.addEventListener(MouseEvent.CLICK, i_btnExportFactures_clickHandler);
        }

        /**
         * Initialise les validateurs de champs en fonction
         * du mode de la fiche (création / édition)
         */
        private function initValidators():void
        {
            validatorEnabled = true;
            validatorArr = new Array();
            if(mode == MODE_CREATION)
            {
                validatorArr.push(libelle_validator);
                validatorArr.push(roic_validator);
                validatorArr.push(template_validator);
                validatorArr.push(plageImport_validator);
            }
            else
            {
                validatorArr.push(roic_validator);
            }
            // champs alerte mail
            if(currentCollectVO.activationAlerte.value as Number == 1)
            {
                validatorArr.push(delaiAlert_validator);
                validatorArr.push(mail_validator);
            }
            // champs login/mode de passe ( pour recup pull uniquement)
            if(setIdBoxVisibility(currentCollectVO.template.idcollectMode))
            {
                validatorArr.push(login_validator);
                validatorArr.push(mdp_validator);
            }
        }

        private function initDisplay():void
        {
        }

        private function initData():void
        {
            if(!filterVO)
            {
                filterVO = new FilterCollecteTypeVO();
            }
            filterTemplateCollection(filterVO);
            i_cbxOperateur_changeHandler(null);
        }

        public function updateDisplay():void
        {
            if(mode == MODE_EDITION)
            {
                i_vbImportInfo.visible = true;
                this.titleIcon = StaticData.imgEdit;
                this.title = " Edition de la collecte ";
                i_hbMode.removeAllChildren();
                var lbTemplate:Label = new Label();
                var linkTemplate:LinkButton = new LinkButton();
                var lbRef:Label = new Label();
                var lbOperateur:Label = new Label();
                var lbDataType:Label = new Label();
                var vb1:VBox = new VBox();
                var vb2:VBox = new VBox();
                var hb1:HBox = new HBox();
                lbTemplate.setStyle("fontSize", 13);
                linkTemplate.setStyle("fontSize", 13);
                lbOperateur.setStyle("fontSize", 13);
                lbDataType.setStyle("fontSize", 13);
                lbRef.setStyle("fontSize", 13);
                lbDataType.setStyle("fontWeigth", "bold");
                lbOperateur.setStyle("fontWeigth", "bold");
                linkTemplate.setStyle("fontWeigth", "bold");
                lbTemplate.htmlText = "Template :"
                linkTemplate.label = currentCollectVO.template.libelleTemplate;
                linkTemplate.addEventListener(MouseEvent.CLICK, onLinklbTemplateClickedHandler);
                lbRef.htmlText = "Ref client : <b> " + currentCollectVO.libelleCollect + "</b>";
                lbOperateur.htmlText = "Opérateur : <b> " + currentCollectVO.template.operateurNom + "</b>"; //+ (currentCollectTypeVO.operateur.label as String).toLocaleUpperCase();
                lbDataType.htmlText = "Types de données : <b> " + ModelLocator.getInstance().getTypeCollecteLabel(currentCollectVO.template.idcollectType).
                    toLocaleUpperCase() + "</b>";
                hb1.addChild(lbTemplate);
                hb1.addChild(linkTemplate);
                vb1.width = 300;
                vb1.addChild(hb1);
                vb1.addChild(lbOperateur);
                vb2.addChild(lbRef);
                vb2.addChild(lbDataType);
                i_hbMode.addChild(vb1);
                i_hbMode.addChild(vb2);
            }
            else
            {
                i_vbImportInfo.visible = true;
                this.titleIcon = StaticData.imgNew;
                this.title = " Création de la collecte ";
            }
        }

        /**
         * Déclenche et prépare l'appel des services distant pour la création/édition
         * d'une collecte dont toutes les infos sont incluses dans le VO <code>CollecteClientVO</code>
         * passé en param ( à noter que si <code>vo.idcollectClient=0</code> : mode création
         * sinon édition de la collecte dont l'id est indiqué).
         */
        private function createEditCollecte(vo:CollecteClientVO):void
        {
            // appel du service
            collecteService = new CollecteTypeService();
            collecteService.createEditCollecteClientVo(vo);
            collecteService.myHandlers.addEventListener(CollecteEvent.COLLECTE_CREATED_UPDATED_SUCCESS, i_btnOk_resultHandler);
            collecteService.myHandlers.addEventListener(CollecteEvent.COLLECTE_CREATED_UPDATED_ERROR, i_btnOk_resultHandler);
        }

        /**
         *  Au click sur le bouton Valider
         *  Création/Edition d'une collecte
         *
         */
        private function i_btnOk_clickHandler(event:MouseEvent):void
        {
            if(infoFicheHasChanged)
            {
                // on affiche l'onglet paramètres d'import
                i_tabNav.selectedIndex = INDEX_PARAM_IMPORT;
                // affectation des valeurs
                currentCollectVO.idRacine = idRacine;
                currentCollectVO.userCreate = ModelLocator.getInstance().userId;
                currentCollectVO.userModif = ModelLocator.getInstance().userId;
                currentCollectVO.retroactivite = i_cbxRetroactivite.selectedItem as EBoolean;
                if(currentCollectVO.activationAlerte.value == 1)
                {
                    currentCollectVO.delaiAlerte = new Number(i_txiDelaiAlert.text);
                }
                if(mode == MODE_CREATION)
                {
                    currentCollectVO.idcollectClient = 0;
                    currentCollectVO.userCreate = ModelLocator.getInstance().userId;
                }
                // intitialisation  et rajout des validators complémentaires
                initValidators();
                // test de la validation des champs obligatoires
                if(validatorArr)
                {
                    var validatorErrorArray:Array = Validator.validateAll(validatorArr);
                    var isValidForm:Boolean = validatorErrorArray.length == 0;
                    if(isValidForm)
                    {
                    	currentCollectVO.plageImport = i_dfdPageImport.selectedDate;
                        createEditCollecte(currentCollectVO);
                    }
                    else
                    {
                    }
                }
            }
            else
            {
                PopUpManager.removePopUp(this);
            }
        }

        /**
         *  Receptionne la réponse serveur ( OK / KO )
         *  après la création/édition d'une collecte
         *
         */
        private function i_btnOk_resultHandler(event:CollecteEvent):void
        {
            var mess:String = "";
            var icon:Class;
            collecteService.myHandlers.removeEventListener(CollecteEvent.COLLECTE_CREATED_UPDATED_SUCCESS, i_btnOk_resultHandler);
            collecteService.myHandlers.removeEventListener(CollecteEvent.COLLECTE_CREATED_UPDATED_ERROR, i_btnOk_resultHandler);
            switch(CollecteEvent(event).type)
            {
                case CollecteEvent.COLLECTE_CREATED_UPDATED_SUCCESS:
                    mode == MODE_CREATION ? mess = 'Votre collecte a été créee avec succès.' : mess = 'Votre collecte a été bien été mise à jour';
                    icon = StaticData.imgValid;
                    dataLocator.consalerte(this, mess, currentCollectVO.libelleCollect, icon);
                    PopUpManager.removePopUp(this);
                    break;
                case CollecteEvent.COLLECTE_CREATED_UPDATED_ERROR:
                    mode == MODE_CREATION ? mess = 'Erreur dans la création de votre collecte.' : mess = 'Erreur dans la mise à jour de votre collecte';
                    icon = StaticData.imgNoValid;
                    dataLocator.consalerte(this, mess, currentCollectVO.libelleCollect, icon);
                    break;
                default:
                    break;
            }
            dispatchEvent(event);
            //Alert.show(mess);
        }

        private function i_btnSup_clickHandler(event:MouseEvent):void
        {
        }

        private function i_btnExport_clickHandler(event:MouseEvent):void
        {
            dataLocator.consalerte(this, "Export en cours d'implémentation", "Export de la collecte", StaticData.imgTodo);
        }

        private function i_btnExportFactures_clickHandler(event:MouseEvent):void
        {
            dataLocator.consalerte(this, "Export des factures en cours d'implémentation", "Export des factures attachées à la collecte", StaticData.imgTodo);
        }

        /**
         *
         * Demande d'annulation et fermeture de la fiche collecte
         * Ouverture d'une popup pour demander une demande de confirmation
         * de l'annulation
         *
         */
        private function i_btnAnnuler_clickHandler(event:Event):void
        {
            if(infoFicheHasChanged)
            {
                if(dataLocator.askForConfirmCancelCollecteEditPopup)
                {
                    askForConfirmation(event);
                }
                else
                {
                    this.dispatchEvent(new CollecteEvent(CollecteEvent.COLLECTE_CREATED_UPDATED_CANCEL));
                    PopUpManager.removePopUp(this);
                }
            }
            else
            {
                PopUpManager.removePopUp(this);
            }
        }

        /**
         *
         * Au changement de selection ( combobox ) du template associé
         * à la collecte
         *
         */
        private function i_cbxTemplate_changeHandler(event:Event):void
        {
            currentCollectVO.template = i_cbxTemplate.selectedItem as CollecteTypeVO;
            //i_tabNav.selectedIndex = INDEX_INFO;
        }

        /**
         * Capte le changement de selection du combo de filtre <b>Type de données</b>.
         * Déclenche le filtrage de la collection de templates dispo pour ce type de données
         */
        protected function i_cbxDataType_changeHandler(event:Event):void
        {
            if((i_cbxDataType.selectedItem as SimpleEntityVO).value as Number != 0)
            {
                filterVO.selectedDataTypeId = SimpleEntityVO(i_cbxDataType.selectedItem).value as Number;
            }
            else
            {
                filterVO.selectedDataTypeId = 0;
            }
            filterTemplateCollection(filterVO);
        }

        /**
         * Capte le changement de selection du combo de filtre <b>Operateur</b>.
         * Déclenche le filtrage de la collection de templates dispo pour cet opérateur
         */
        private function i_cbxOperateur_changeHandler(event:Event):void
        {
			if (i_cbxOperateur.selectedIndex != -1) {
            if((i_cbxOperateur.selectedItem as OperateurVO).value as Number != 0)
            {
                filterVO.selectedOperateurId = OperateurVO(i_cbxOperateur.selectedItem).value as Number;
            }
            else
            {
                filterVO.selectedOperateurId = 0;
            }
            filterTemplateCollection(filterVO);
			}
        }

        public function checkFormCompletion(event:Event):void
        {
            infoFicheHasChanged = true;
        }

        /**
         * Ouvre une popup de demande de confirmation d'annulation
         * 2 options:
         * - <b>Quitter sans sauvegarder</b>
         * - <b>Revenir à la fiche</b>
         */
        protected function askForConfirmation(event:Event):void
        {
            var popUp:AlertExtendedPopup = new AlertExtendedPopup();
            popUp.addEventListener(AlertExtended.USER_ACTION_EVENT, onConfirmationCancelPopupBtn_clickHandler);
            PopUpManager.addPopUp(popUp, this, true);
            PopUpManager.centerPopUp(popUp);
            popUp.titlePopup = "Confirmer l'annulation";
            popUp.libelle = "Vous êtes sur le point de quitter la fiche.Que souhaitez vous faire ?"
            popUp.btn1.label = "Quitter sans sauvegarder";
            popUp.btn2.label = " Revenir à la fiche";
        }

        /**
         * Capte la réponse utilisateur de <b>confirmation d'Annulation</b>
         * de création/édition d'une collecte
         */
        protected function onConfirmationCancelPopupBtn_clickHandler(event:Event):void
        {
            var popup:AlertExtendedPopup = AlertExtendedPopup(Event(event).currentTarget);
            popup.removeEventListener(AlertExtended.USER_ACTION_EVENT, onConfirmationCancelPopupBtn_clickHandler);
            // click sur la coche 'ne plus me demander'
            dataLocator.askForConfirmCancelCollecteEditPopup = !popup.rememberDecision;
            // Cas Quitter sans sauvegarder
            if(popup.btn1Clicked)
            {
                switch(mode)
                {
                    case MODE_CREATION:
                        break;
                    case MODE_EDITION:
                        this.dispatchEvent(new CollecteEvent(CollecteEvent.COLLECTE_CREATED_UPDATED_CANCEL));
                        break;
                    default:
                        break;
                }
                popup.close();
                PopUpManager.removePopUp(this);
            }
            else
            {
                // Cas  Revenir à la fiche	
                popup.close();
            }
        }

        /**
         * Parcours la liste  des templates de <code>i_dgListCollecteType</code>
         * pour lui appliquer un filtre <code>filterVO</code> en fonction
         * de l'opérateur,du type de données et du mode de récupération selectionnés
         * par l'utilisateur
         * Met à jour le dataprovider du Datagrid <code>templateListFiltered</code>
         */
        public function filterTemplateCollection(filterVO:FilterCollecteTypeVO):void
        {
            templateListFiltered = new ArrayCollection();
            var collec:ArrayCollection = ModelLocator.getInstance().publishedTemplateList;
            // si la valeur du filtre est à null , retour de tous les champs
            for(var i:int = 0; i < collec.length; i++)
            {
                var vo:CollecteTypeVO = collec.getItemAt(i) as CollecteTypeVO;
                if((filterVO.selectedDataTypeId == 0 || vo.idcollectType == filterVO.selectedDataTypeId) && (filterVO.selectedOperateurId == 0 || vo.operateurId ==
                    filterVO.selectedOperateurId))
                {
                    templateListFiltered.addItem(vo);
                }
            }
            if(i_cbxTemplate)
            {
                i_cbxTemplate.dataProvider = templateListFiltered;
                (i_cbxTemplate.dataProvider as ArrayCollection).refresh();
                if(i_cbxTemplate.selectedIndex != -1)
                {
                    currentCollectVO.template = i_cbxTemplate.selectedItem as CollecteTypeVO;
                        // setIdBoxVisibility(currentTemplateVO.idcollectMode);
                }
            }
        }

        /**
         * Rend visible/invisible les champs identifiant/mot de passe en
         * fonction du mode de récupération ( seul les modes PULL- nécéssitent
         * ces infos (ei idCollecte= 3 , 4 OU 5)
         */
        public function setIdBoxVisibility(idCollecteMode:Number):Boolean
        {
            if(idCollecteMode == 3 || idCollecteMode == 4 || idCollecteMode == 5)
            {
                return true
            }
            else
            {
                return false;
            }
        }

        /**
         * Appel le service distant pour <b>tester les identifiants</b> ( login/mdp)
         * fournis par l'utilisateur auprès de l'adresse <b>Extranet</b>
         * indiqué dans le template sélectionné :
         * <code>currentCollectVO.template.modeSouscriptionUrl</code>
         */
        private function i_btnTestId_clickHandler(event:MouseEvent):void
        {
            collecteService = new CollecteTypeService();
            collecteService.testIdentification(currentCollectVO.template.modeSouscriptionUrl, currentCollectVO.identifiant, currentCollectVO.mdp);
            collecteService.myHandlers.addEventListener(CollecteEvent.TEST_IDENTIFICATION_SUCCESS, onTestIdentificationResultHandler);
            collecteService.myHandlers.addEventListener(CollecteEvent.TEST_IDENTIFICATION_ERROR, onTestIdentificationResultHandler);
        }

        /**
         *Résultat du test d'identification ( fait apparaitre une coche OK/KO )
         *
         */
        protected function onTestIdentificationResultHandler(event:CollecteEvent):void
        {
            switch(CollecteEvent(event).type)
            {
                case CollecteEvent.TEST_IDENTIFICATION_SUCCESS:
                    i_imgIdentifValid.source = StaticData.imgOk;
                    break;
                case CollecteEvent.TEST_IDENTIFICATION_ERROR:
                    i_imgIdentifValid.source = StaticData.imgKO;
                    break;
                default:
                    i_imgIdentifValid.source = StaticData.imgKO;
                    break;
            }
            i_imgIdentifValid.visible = true;
            // Binder à l'image (Ok/KO)
            identificationOk = collecteService.myDatas.identificationSuccess;
            collecteService.myHandlers.removeEventListener(CollecteEvent.TEST_IDENTIFICATION_SUCCESS, onTestIdentificationResultHandler);
            collecteService.myHandlers.removeEventListener(CollecteEvent.TEST_IDENTIFICATION_ERROR, onTestIdentificationResultHandler);
        }

        /**
         *  Au click sur le libellé du template ,
         *  affiche l'onglet  regroupant les caractéristiques
         *  du template
         *
         */
        private function onLinklbTemplateClickedHandler(event:MouseEvent):void
        {
            i_tabNav.selectedIndex = INDEX_INFO;
        }

        protected function creationCompleteHboxHandler(e:Event):void
        {
        }

        protected function closeHandler(event:Event):void
        {
            btnAnnulerClickHandler(event);
        }

        protected function btnAnnulerClickHandler(event:Event):void
        {
            PopUpManager.removePopUp(this);
        }

        public function get mode():String
        {
            return _mode;
        }

        public function set mode(value:String):void
        {
            _mode = value;
        }

        public function get chaineFacturationListe():ArrayCollection
        {
            return _chaineFacturationListe;
        }

        public function set chaineFacturationListe(value:ArrayCollection):void
        {
            _chaineFacturationListe = value;
        }

        public function get currentCollectVO():CollecteClientVO
        {
            return _currentCollectVO;
        }

        /**
         * le vo courant de type <code>CollecteClientVO</code> qui peuple le formulaire
         *  (DataBinding Bidirectionnel le vo affecte les champs , et la modif des champs
         * affecte le vo).
         */
        public function set currentCollectVO(value:CollecteClientVO):void
        {
            _currentCollectVO = value;
        }

        public function get templateListFiltered():ArrayCollection
        {
            return _templateListFiltered;
        }

        public function set templateListFiltered(value:ArrayCollection):void
        {
            _templateListFiltered = value;
        }

        public function get filterVO():FilterCollecteTypeVO
        {
            return _filterVO;
        }

        public function set filterVO(value:FilterCollecteTypeVO):void
        {
            _filterVO = value;
        }

        public function get idRacine():Number
        {
            return _idRacine;
        }

        public function set idRacine(value:Number):void
        {
            _idRacine = value;
        }

        public function get validatorArr():Array
        {
            return _validatorArr;
        }

        public function set validatorArr(value:Array):void
        {
            _validatorArr = value;
        }
    }
}