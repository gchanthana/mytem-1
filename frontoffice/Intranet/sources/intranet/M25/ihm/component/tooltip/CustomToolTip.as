package intranet.M25.ihm.component.tooltip
{
    import flash.filters.GlowFilter;
    import mx.containers.Box;
    import mx.containers.BoxDirection;
    import mx.controls.Image;
    import mx.controls.Text;
    import mx.core.IToolTip;
    import mx.formatters.SwitchSymbolFormatter;

    public class CustomToolTip extends Box implements IToolTip
    {
        public static const INFO:String = "info";
        public static const PJ:String = "pj";
        public static const CONTACT:String = "contact";
        public static const COST:String = "cost";
        // utilisation de scale-9 pour adapter la taille de la bulle
        [Embed(source="bulle.png", scaleGridTop="10", scaleGridBottom="30", scaleGridLeft="10", scaleGridRight="50")]
        public var bulleClass:Class;
        [Embed(source="info.png")]
        public var infoClass:Class;
        [Embed(source="publish.png")]
        public var pjClass:Class;
        [Embed(source="address.png")]
        public var contactClass:Class;
        [Embed(source="issue.png")]
        public var costClass:Class;
        protected var img:Image;
        protected var textField:Text;
        private var _text:String = '';
        private var _type:String;

        public function CustomToolTip(p_type:String = "info")
        {
            mouseEnabled = false;
            mouseChildren = false;
            setStyle("paddingLeft", 10);
            setStyle("paddingTop", 10);
            setStyle("paddingBottom", 20);
            setStyle("paddingRight", 10);
            setStyle('verticalAlign', 'middle');
            direction = BoxDirection.HORIZONTAL;
            maxWidth = 800;
            maxHeight = 600;
            minWidth = 300;
            type = p_type
            // Ajout d'un effet autour de la bulle
            filters = [ new GlowFilter(0xCCCCCC, 1, 16, 16) ];
        }

        public function get text():String
        {
            return null;
        }

        public function set text(value:String):void
        {
            _text = value;
        }

        public function changeIcon(imgClass:Class):void
        {
            infoClass = imgClass;
            createChildren();
        }

        override protected function createChildren():void
        {
            super.createChildren();
            this.setStyle('backgroundImage', bulleClass);
            this.setStyle('backgroundSize', '100%');
            img = new Image();
            switch(type)
            {
                case INFO:
                    img.source = infoClass;
                    break;
                case CONTACT:
                    img.source = contactClass;
                    break;
                case PJ:
                    img.source = pjClass;
                    break;
                case COST:
                    img.source = costClass;
                    break;
                default:
                    img.source = infoClass;
                    break;
            }
            addChild(img);
            textField = new Text();
            textField.percentWidth = 100;
            textField.text = _text;
            textField.setStyle('color', 0x646464);
            textField.setStyle('fontWeight', 'normal');
            addChild(textField);
            //  For a simply rouded rectangle as background
        /*  setStyle('backgroundColor', 0xFFFFFF);
           setStyle('backgroundAlpha', 0.9);
           setStyle('borderStyle', 'solid');
           setStyle('borderThickness', 1);
           setStyle('borderColor', 0xCCCCCC);
         setStyle('cornerRadius', 6);*/
        }

        public function get type():String
        {
            return _type;
        }

        public function set type(value:String):void
        {
            _type = value;
        }
    }
}