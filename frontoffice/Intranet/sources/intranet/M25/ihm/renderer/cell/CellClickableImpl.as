package intranet.M25.ihm.renderer.cell
{
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;
    import intranet.M25.ihm.component.tooltip.CustomToolTip;
    import intranet.M25.entity.StaticData;
    import mx.binding.utils.BindingUtils;
    import mx.containers.HBox;
    import mx.controls.Button;
    import mx.controls.Image;
    import mx.core.UIComponent;
    import mx.events.FlexEvent;
    import mx.events.ToolTipEvent;
    import mx.managers.ToolTipManager;

    [Bindable]
    public class CellClickableImpl extends HBox
    {
        public var i_btnLink:Button;
        public var i_img:Image;
        public var currentTooltip:CustomToolTip;
        private var _linklabel:String;
        private var _tiplinkInfo:String;
        private var _isLinkable:Boolean = true;
        private var _hasInfoBulle:Boolean = false;
        private var _iconToolTip:Class;
        private var _toolTipVisible:Boolean;
        private var _type:String;
        private var _pjUrl:String;

        public function CellClickableImpl()
        {
            super();
        }

        public function init(event:FlexEvent):void
        {
            this.removeEventListener(FlexEvent.INITIALIZE, onCreationCompleteHandler)
            this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler)
        }

        public function onCreationCompleteHandler(event:FlexEvent):void
        {
            this.removeEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
            i_btnLink.addEventListener(MouseEvent.CLICK, onLinkClickedHandler);
            i_img.addEventListener(MouseEvent.CLICK, onLinkClickedHandler);
        }

        protected function onToolTipCreateHandler(event:ToolTipEvent):void
        {
            if(isLinkable)
            {
                createTip(event);
            }
        }

        protected function onToolTipShowHandler(event:ToolTipEvent):void
        {
            if(isLinkable)
            {
                positionToolTip(event);
            }
        }

        public function createTip(event:ToolTipEvent):void
        {
            if(!type)
            {
                type = CustomToolTip.INFO;
            }
            // on créé le tooltip
            event.toolTip = new CustomToolTip(type);
            currentTooltip = event.toolTip as CustomToolTip;
            // on lui affecte le texte de la propriété tooltip de l'objet
            event.toolTip.text = (event.target as UIComponent).toolTip;
        }

        public function positionToolTip(event:ToolTipEvent):void
        {
            if(true)
            {
                event.toolTip.x = (((event.target) as UIComponent) as UIComponent).contentToGlobal(new Point(event.target.x, event.target.y)).x - event.
                    toolTip.width;
                event.toolTip.y = (((event.target) as UIComponent) as UIComponent).contentToGlobal(new Point(event.target.x, event.target.y)).y - event.
                    toolTip.height;
            }
            else
            {
                event.toolTip.x = event.target.x - event.toolTip.width / 2;
                event.toolTip.y = event.target.y - event.toolTip.height;
            }
        }

        public function get linklabel():String
        {
            return _linklabel;
        }

        public function set linklabel(value:String):void
        {
            _linklabel = value;
        }

        /**
         * si l'url de la PJ est définie, au click sur le lien on télécharge
         * la pièce jointe
         * @param event
         *
         */
        private function onLinkClickedHandler(event:MouseEvent):void
        {
            if(pjUrl)
            {
                navigateToURL(new URLRequest(pjUrl));
            }
        }

        public function get tiplinkInfo():String
        {
            return _tiplinkInfo;
        }

        public function set tiplinkInfo(value:String):void
        {
            _tiplinkInfo = value;
        }

        public function get isLinkable():Boolean
        {
            return _isLinkable;
        }

        public function set isLinkable(value:Boolean):void
        {
            _isLinkable = value;
        }

        public function get iconToolTip():Class
        {
            return _iconToolTip;
        }

        public function set iconToolTip(value:Class):void
        {
            _iconToolTip = value;
            currentTooltip.changeIcon(value);
        }

        public function get type():String
        {
            return _type;
        }

        public function set type(value:String):void
        {
            _type = value;
            //(type == "pj") ? i_img.visible == true : i_img.visible == false;
        }

        public function get pjUrl():String
        {
            return _pjUrl;
        }

        public function set pjUrl(value:String):void
        {
            _pjUrl = value;
        }

        public function get hasInfoBulle():Boolean
        {
            return _hasInfoBulle;
        }

        public function set hasInfoBulle(value:Boolean):void
        {
            _hasInfoBulle = value;
            if(hasInfoBulle)
            {
                // i_img.source = StaticData.imgInfoSmall;
            }
        }
    }
}