package intranet.M25.entity.enum
{
    import mx.collections.ArrayCollection;

    public class ERechercheCollecteParam extends Enumeratio
    {
        public static const ALL:ERechercheCollecteParam = new ERechercheCollecteParam("ALL", "Toutes les collectes");
        public static const CLOSED:ERechercheCollecteParam = new ERechercheCollecteParam("CLOSED", "Les collectes inactives");
        public static const FIRST_IMPORT_LATE:ERechercheCollecteParam = new ERechercheCollecteParam("FIRST_IMPORT_LATE", "Les collectes avec 1er import en retard");
        public static const FUTURE:ERechercheCollecteParam = new ERechercheCollecteParam("FUTURE", "Les collectes futures");
        public static const NO_IDENTIFIANTS:ERechercheCollecteParam = new ERechercheCollecteParam("NO_IDENTIFIANTS", "Les collectes sans les identifiants nécessaires");
        public static const NO_OPEN_IMPORT_DATE:ERechercheCollecteParam = new ERechercheCollecteParam("NO_OPEN_IMPORT_DATE", "Les collectes sans date d’ouverture de la plage d’import");
        public static const NO_ROIC:ERechercheCollecteParam = new ERechercheCollecteParam("NO_ROIC", "Les collectes sans la Référence opérateur Id Client");
        public static const OPENED:ERechercheCollecteParam = new ERechercheCollecteParam("OPENED", "Les collectes actives");

        public function ERechercheCollecteParam(value:String, label:String)
        {
            super(value, label);
        }

        public static function getCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(ALL, CLOSED, OPENED, FIRST_IMPORT_LATE, FUTURE, NO_IDENTIFIANTS, NO_OPEN_IMPORT_DATE, NO_ROIC));
        }

        public static function getParamManquantsCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(ALL, NO_IDENTIFIANTS, NO_OPEN_IMPORT_DATE, NO_ROIC));
        }

        public static function getParamEtatCollectesCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(ALL, CLOSED, OPENED, FIRST_IMPORT_LATE, FUTURE));
        }
    }
}