package intranet.M25.entity.enum
{
    import flash.utils.ByteArray;
    import mx.collections.ArrayCollection;
    import mx.controls.dataGridClasses.DataGridColumn;

    [Bindable]
    public class Enumeratio
    {
        protected var _value:Object;
        protected var _label:String;
        protected var _collection:ArrayCollection = new ArrayCollection();

        public function Enumeratio(value:*, label:String)
        {
            _value = value;
            _label = label;
        }

        public static function getIndex(val:Object, collec:ArrayCollection, field:String = "value"):Number
        {
            var index:Number = -1;
            for(var i:int = 0; i++ < collec.length; i++)
            {
                if(compareObject(val, collec.getItemAt(i)[field]))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        public static function getItem(collec:ArrayCollection, value:*, field:String = "value"):Enumeratio
        {
            var length:int = collec.length;
            var currentItem:Enumeratio = null;
            for(var i:int = 0; i < length; i++)
            {
                currentItem = collec.getItemAt(i) as Enumeratio;
                if(Enumeratio.compareObject(currentItem[field], value))
                {
                    break;
                }
            }
            return currentItem;
        }

        /**
         * retourne le champs label d'un objet de type enumération au sein
         * d'un datagrid
         * <code>
         *  <mx:DataGridColumn  dataField = "enum_exemple"
           labelFunction = "Enumeratio.getLabelField"
         * </code>
         * @param item
         * @param col
         * @return
         *
         */
        public static function getLabelField(item:Object, col:DataGridColumn):String
        {
            var str:String = "";
            var obj:Object = item[col.dataField];
            str = obj.label as String
            return str;
        }

        public function get value():*
        {
            return _value;
        }

        public function set value(value:*):void
        {
            _value = value;
        }

        public function get label():String
        {
            return _label;
        }

        public function set label(value:String):void
        {
            _label = value;
        }

        public static function compareObject(obj1:Object, obj2:Object):Boolean
        {
            var buffer1:ByteArray = new ByteArray();
            buffer1.writeObject(obj1);
            var buffer2:ByteArray = new ByteArray();
            buffer2.writeObject(obj2);
            // compare the lengths
            var size:uint = buffer1.length;
            if(buffer1.length == buffer2.length)
            {
                buffer1.position = 0;
                buffer2.position = 0;
                // then the bits
                while(buffer1.position < size)
                {
                    var v1:int = buffer1.readByte();
                    if(v1 != buffer2.readByte())
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }
}