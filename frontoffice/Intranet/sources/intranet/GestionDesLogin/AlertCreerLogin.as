package intranet.GestionDesLogin
{
	import flash.events.Event;
	
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	public class AlertCreerLogin extends Alert_Creer_Login
	{
		public function AlertCreerLogin()
		{
			super();
		}
		
		protected function close():void
		{
			PopUpManager.removePopUp(this);
		}
		
		override public function closeHandler(e:Event):void
		{
			dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
		}
		
	}
}