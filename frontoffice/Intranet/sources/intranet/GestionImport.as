package intranet
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import flash.events.Event;
	import fr.consotel.consoview.util.ConsoViewErrorManager;
	import mx.events.FlexEvent;
	import flash.events.MouseEvent;
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import flash.events.KeyboardEvent;
	import flash.net.FileReference;
	import mx.controls.ProgressBar;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import mx.utils.ObjectUtil;
	
	public class GestionImport extends GestionImportIHM
	{
		private var _orgaGeoExists:Boolean=false;
		private var _orgaOpeExists:Boolean=false;
		private var fr:FileReference = new FileReference();
		public const UPLOAD_URL:String = index.urlBackoffice + "/fr/consotel/consoprod/intranet/processUpload.cfm";
		
		[Bindable]
		private var myFilterArray:ArrayCollection=new ArrayCollection();
		
		[Bindable]
		private var myFilterCFArray:ArrayCollection=new ArrayCollection();
		
		public function GestionImport()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}

		protected function initIHM(event:Event):void {
			logoffBtn.addEventListener(MouseEvent.CLICK,dispatchLogoff);
			btnAddCF.addEventListener(MouseEvent.CLICK,addCF);
			btnDeleteCF.addEventListener(MouseEvent.CLICK,deleteCF);
			//dgGroupeMaitre.addEventListener(Event.CHANGE,updateHisto);
			dgGroupeMaitre.addEventListener(Event.CHANGE,updateOrga);
			/* Gestion des groupes */
			dgCF.addEventListener(Event.CHANGE,checkOrga);
			btnAddGroupe.addEventListener(MouseEvent.CLICK,addGroupe);
			btnDeleteGroupe.addEventListener(MouseEvent.CLICK,deleteGroupe);
			/* Gestion des boutons radios */
			rdAffect.addEventListener(MouseEvent.CLICK,getListeSociete);
			rdNonAffect.addEventListener(MouseEvent.CLICK,getListeSociete);
			rdTousAffect.addEventListener(MouseEvent.CLICK,getListeSociete);
			cmbOperateur.addEventListener(Event.CHANGE,updateOperateur);
			dgRefClient.addEventListener(Event.CHANGE,checkRefClient);
			//btnAddOrga.addEventListener(MouseEvent.CLICK,addOrga);
			//btnDeleteOrga.addEventListener(MouseEvent.CLICK,deleteOrga);
			txtFiltre.addEventListener(Event.CHANGE,filtre);
			txtFiltreCF.addEventListener(Event.CHANGE,filtreCF);
			
			dgRefClient.dataProvider="";
			dgRefClientAll.dataProvider="";
			//cmbTypeOrga.addEventListener(Event.CHANGE,checkTypeOrga);
			//cmbOperateur.addEventListener(Event.CHANGE,checkOperateur);
			var op:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.intranet.GestionImport",
																	"getGroupesMaitres",
																	processInitGroupes,remoteError);
			RemoteObjectUtil.callService(op);
			var op2:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.intranet.GestionImport",
																	"getListeOperateur",
																	processGetListeOperateur,remoteError);
			RemoteObjectUtil.callService(op2);
			
			// pour le upload
/* 			var op2:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.GestionClient",
																	"getFiles",
																	processInitFiles,remoteError);
			RemoteObjectUtil.callService(op2,["csv"]);
 */			//btnstartUpload.addEventListener(MouseEvent.CLICK,startUpload);
			//btnTest.addEventListener(MouseEvent.CLICK,testFile);
			//btnDeleteFile.addEventListener(MouseEvent.CLICK,deleteFile);
			//cmbFileTest.addEventListener(Event.CHANGE,checkFileTest);
			/* fr.addEventListener(Event.SELECT, selectHandler);
		    fr.addEventListener(Event.OPEN, openHandler);
		    fr.addEventListener(Event.COMPLETE, completeHandler); */
		}
		private function updateOperateur(event:Event):void {
			getListeSociete(new MouseEvent(""));
		}
		
		private function getListeSociete(ev:MouseEvent):void {
			if (rdGroup.selectedValue==1) {
				var op3:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
														"fr.consotel.consoprod.intranet.GestionImport",
														"getAffectSoc",
														processGetListeSociete,remoteError);
				RemoteObjectUtil.callService(op3,[cmbOperateur.selectedItem["data"]]);
			} else if (rdGroup.selectedValue==0) {
				var op4:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
														"fr.consotel.consoprod.intranet.GestionImport",
														"getNonAffectSoc",
														processGetListeSociete,remoteError);
				RemoteObjectUtil.callService(op4,[cmbOperateur.selectedItem["data"]]);
			} else {
				var op5:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
														"fr.consotel.consoprod.intranet.GestionImport",
														"getAllSoc",
														processGetListeSociete,remoteError);
				RemoteObjectUtil.callService(op5,[cmbOperateur.selectedItem["data"]]);
			}
			
		}
		private function processGetListeSociete(event:ResultEvent):void {
			dgRefClientAll.dataProvider=event.result;
		}
		
		private function addGroupe(event:MouseEvent):void {
				var op2:AbstractOperation =
							RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoprod.intranet.GestionImport",
																			"AddGroupe",
																			processAddGroupe,remoteError);
				RemoteObjectUtil.callService(op2);
		}
		private function processAddGroupe(event:ResultEvent):void {
			dgGroupeMaitre.dataProvider=event.result;
		}
		
		private function deleteGroupe(event:MouseEvent):void {
			var op2:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.intranet.GestionImport",
																		"DeleteGroupe",
																		processDeleteGroupe,remoteError);
			RemoteObjectUtil.callService(op2,[dgGroupeMaitre.selectedItem["IDGROUPE_MAITRE"]]);
		}
		private function processDeleteGroupe(event:ResultEvent):void {
			dgGroupeMaitre.dataProvider=event.result;
		}
		
		private function checkOrga(event:Event):void {
			if (dgCF.selectedIndex!=-1) {
				btnDeleteCF.enabled=true;
			} else {
				btnDeleteCF.enabled=false;
			}
		}
		
		private function checkRefClient(event:Event):void {
			if (dgRefClient.selectedIndex!=-1) {
				btnAddCF.enabled=true;
			} else {
				btnAddCF.enabled=false;
			}
		}
		
		private function updateOrga(event:Event):void {
			if (dgGroupeMaitre.selectedIndex!=-1) {
				btnDeleteCF.enabled=false;
				btnAddCF.enabled=false;
				dgCF.dataProvider="";
				//cmbTypeOrga.selectedIndex=0;
				//cmbTypeOrga.dispatchEvent(new Event(Event.CHANGE));
				//txtLibelle.text='';
				var op:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.intranet.GestionImport",
																		"getListeCF",
																		processGetOrgas,remoteError);
				RemoteObjectUtil.callService(op,dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
				var op2:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.intranet.GestionImport",
																		"getListeRefClient",
																		processGetRefClient,remoteError);
				RemoteObjectUtil.callService(op2,dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
				/*
				var op3:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.GestionClient",
																		"checkOrgaGeo",
																		processcheckOrgaGeo,remoteError);
				RemoteObjectUtil.callService(op3,dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
				var op4:AbstractOperation =
					RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.GestionClient",
																	"getFiles",
																	processInitFiles,remoteError);
				RemoteObjectUtil.callService(op4,["csv"]); */
			} 
		}
		
		private function addCF(event:Event):void {
			var op:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.intranet.GestionImport",
																		"addCF",
																		processaddCF,remoteError);
				RemoteObjectUtil.callService(op,dgRefClient.selectedItem.IDREF_CLIENT,cmbOperateur.selectedItem.data);
		}
		
		private function deleteCF(event:Event):void {
			var op:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.intranet.GestionImport",
																		"deleteCF",
																		processaddCF,remoteError);
				RemoteObjectUtil.callService(op,dgCF.selectedItem.OPERATEURID,dgCF.selectedItem.IDREF_CLIENT,dgCF.selectedItem.CF);
		}
		
		private function processaddCF(event:ResultEvent):void {
			var op:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.intranet.GestionImport",
																		"getListeCF",
																		processGetOrgas,remoteError);
				RemoteObjectUtil.callService(op,dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
		}
		
		private function processGetOrgas(event:ResultEvent):void {
			myFilterCFArray=event.result as ArrayCollection;
			dgCF.dataProvider=myFilterCFArray;
		}
		
		private function processGetRefClient(event:ResultEvent):void {
			//myFilterCFArray=event.result as ArrayCollection;
			dgRefClient.dataProvider=event.result;
		}
		
		private function processGetListeOperateur(event:ResultEvent):void {
			cmbOperateur.dataProvider=event.result;
			cmbOperateur.dataProvider.addItemAt({label:"Tous",data:"-1"},0);
			cmbOperateur.selectedIndex=0;
			getListeSociete(new MouseEvent(""));
		}
		
		private function processAddOrgas(event:ResultEvent):void {
			if (event.result>=1) {
				updateOrga(new Event(""));
			} else {
				Alert.show('Pb: ' + event.result.toString());
			}
		}
		
		private function processDeleteOrgas(event:ResultEvent):void {
			if (event.result==1) {
				updateOrga(new Event(""));
			} else {
				Alert.show('Pb: ' + event.result.toString());
			}
		}
		
		private function processInitGroupes(event:ResultEvent):void {
			myFilterArray=event.result as ArrayCollection;
			dgGroupeMaitre.dataProvider=event.result;
		}
		
		private function remoteError(faultEvent:FaultEvent):void {
			ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
		}
		
		private function dispatchLogoff(event:Event):void {
			dispatchEvent(new Event(Intranet.LOGOFF_EVENT));
		}
		
		public function filtre(e:Event):void {
			myFilterArray.filterFunction=filtreGrid;
			myFilterArray.refresh();
		}
		
		public function filtreCF(e:Event):void {
			myFilterCFArray.filterFunction=filtreGridCF;
			myFilterCFArray.refresh();
		}

		private function checkOpe(elem:*,index:int,tab:Array):void { //Vérification de l'opérateur
			// vérifie le choix du combo
			if (_orgaOpeExists == false) {
				if (elem.OPERATEURID==cmbOperateur.selectedItem.data) {
					_orgaOpeExists=true;
				} else {
					_orgaOpeExists=false;
				}
			}
		}
		
		public function filtreGrid(item:Object):Boolean {
			if (item.LIBELLE_GROUPE_CLIENT.substr(0,txtFiltre.text.length).toLowerCase()==txtFiltre.text.toLowerCase()
				 || parseInt(item.IDGROUPE_CLIENT.toString().substr(0,txtFiltre.text.length))==parseInt(txtFiltre.text)
				) {
				return (true);
			} else {
				return (false);
			}
		}
		
		public function filtreGridCF(item:Object):Boolean {
			if (item.NOM.substr(0,txtFiltreCF.text.length).toLowerCase()==txtFiltreCF.text.toLowerCase()
				|| item.CF.toString().substr(0,txtFiltreCF.text.length)==txtFiltreCF.text ) {
				return (true);
			} else {
				return (false);
			}
		}
		
		private function dumbProcess(event:ResultEvent):void {
			
		}
/* 		
		public function startUpload(m:MouseEvent):void
		{
		    fr.browse();
		}
		
		public function cancelUpload(m:MouseEvent):void
	    {
	        fr.cancel();
	    }
	    
	    private function selectHandler(event:Event):void
			{
			    var request:URLRequest = new URLRequest();
			    request.url = UPLOAD_URL;
			    fr.upload(request);
			}
		 */
		 /**
     * When the OPEN event has dispatched, change the progress bar's label 
     * and enable the "Cancel" button, which allows the user to abort the 
     * download operation.
     */
	   /*  private function openHandler(event:Event):void
	    {
	        //pbuploadProgress.label = "DOWNLOADING %3%%";
	    } */
	/**
     * While the file is downloading, update the progress bar's status.
     */
    /* private function progressHandler(event:ProgressEvent):void
    {
        //pbuploadProgress.setProgress(event.bytesLoaded, event.bytesTotal);
    } */
		
	/**
     * Once the download has completed, change the progress bar's label one 
     * last time and disable the "Cancel" button since the download is 
     * already completed.
     */
    /* private function completeHandler(event:Event):void
    {
        var op2:AbstractOperation =
				RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																"fr.consotel.consoprod.GestionClient",
																"getFiles",
																processInitFiles,remoteError);
		RemoteObjectUtil.callService(op2,["csv"]);
        Alert.show("Chargement OK");
    } */
	}
}