/*
   Cette classe étend le conteneur principale du module de gestion de groupe
   Action : Créer les quatre datagrids et les ajoute à l'interface
   Rôle : Assure la communication entre les datagrids
 */
package intranet.gestionGroupe
{
    //import composants.util.DataGridDataExporter;
    //import composants.util.DataGridDataExporter;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.events.ProgressEvent;
    import flash.net.FileReference;
    import flash.net.URLRequest;
    
    import fr.consotel.consoview.util.ConsoViewErrorManager;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import intranet.gestionLoginClasse.dgIhm_ItemRenderer;
    
    import mx.collections.ArrayCollection;
    import mx.containers.Grid;
    import mx.controls.Alert;
    import mx.controls.DataGrid;
    import mx.controls.ProgressBar;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.utils.ObjectUtil;

    public class GestionImport extends GestionImportIHM
    {
        //dataGrid groupe Racine
        private var objGridGroupeRacine:GridGroupeRacine;
        //dataGrid toutes les sociétés
        private var objGridSocieteAll:GridSocieteAll;
        //dataGrid compte de facturation
        private var objGridSocieteAffectee:GridSocieteAffectee;
        //datagrid sociétés affectées
        private var objGridCompteFac:GridCompteFac;
        //Initialisation de la variable idGroupeRacineSelectionne
        private var idGroupeRacineSelectionne:int = -1;

        public function GestionImport()
        {
            super();
            objGridSocieteAll = new GridSocieteAll(this);
            //dataGrid groupe Racine
            objGridGroupeRacine = new GridGroupeRacine(this);
            //dataGrid compte de facturation
            objGridSocieteAffectee = new GridSocieteAffectee(this);
            //datagrid sociétés affectées
            objGridCompteFac = new GridCompteFac(this);
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
        }

        public function initIHM(event:Event):void
        {
            //Ajout des datagrids au conteneur principale :
            boxGauche.addChild(objGridSocieteAll);
            boxGauche.addChild(objGridSocieteAffectee);
            boxDroite.addChild(objGridGroupeRacine);
            boxDroite.addChild(objGridCompteFac);
        }

        //Gestion des erreurs	
        private function remoteError(faultEvent:FaultEvent):void
        {
            ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
        }

        //Action déconnexion
        private function dispatchLogoff(event:Event):void
        {
            dispatchEvent(new Event(Intranet.LOGOFF_EVENT));
        }

        //Cette méthode permet de vérifier si la société n'est pas déja affecté au groupe racine
        public function verifDoublonRefClient():Boolean
        {
            var valide:Boolean = true;
            //Tableau des sociétés que l'on veut ajouter
            var tabRefClientAjoute:Array = objGridSocieteAll.dgRefClientAll.selectedItems;
            //Tableau des sociétés existantes
            var tabRefClientExistant:Array = (objGridSocieteAffectee.dgRefClient.dataProvider as ArrayCollection).source;
            for (var i:int = 0; i < tabRefClientAjoute.length; i++)
            {
                for (var a:int = 0; a < tabRefClientExistant.length; a++)
                {
                    //Si une des sociétés que l'on souhaite affecter existe dans le dgSociété afféctées alors valid =false
                    if (tabRefClientAjoute[i].IDREF_CLIENT == tabRefClientExistant[a].IDREF_CLIENT)
                    {
                        valide = false;
                    }
                }
            }
            return valide;
        }

        //Mise à jour du dataGrid groupe Racine
        public function updateGridGroupeRacine():void
        {
            objGridGroupeRacine.getListeGroupe(null);
        }

        //Mise à jour du dataGrid toutes les sociétés
        public function updateGridSocieteAll():void
        {
            objGridSocieteAll.getListeSociete(null);
        }

        //Mise à jour du dataGrid compte de facturation
        public function updateGridCompteFact(idRefClient:int):void
        {
            objGridCompteFac.getCF(idRefClient);
        }

        //Mise à jour du datagrid sociétés affectées
        public function updateGridSocieteAffectee(idGroupeRacine:int):void
        {
            objGridSocieteAffectee.getListeSocieteAffectee(idGroupeRacine);
            setIdGroupeRacineSelectionne(idGroupeRacine);
        }

        //Mutateur idGroupeRacineSelectionne
        public function setIdGroupeRacineSelectionne(idRacine:int):void
        {
            idGroupeRacineSelectionne = idRacine;
        }

        //Accesseur idGroupeRacineSelectionne     
        public function getIdGroupeRacineSelectionne():int
        {
            return idGroupeRacineSelectionne;
        }

        //Appelle de la méthode setReselectionneGroupeRacine(true en paramètre pour reselectionner un groupe racine)
        public function selectionneGroupeRacine():void
        {
            objGridGroupeRacine.setReselectionneGroupeRacine(true);
        }

        //Accesseur objGridSocieteAffectee
        public function getSocieteSelectionne():GridSocieteAffectee
        {
            return objGridSocieteAffectee;
        }

        public function updateAll():void
        {
            objGridCompteFac.dgCF.dataProvider = null;
            objGridSocieteAffectee.dgRefClient.dataProvider = null;
        }

        // Utilitaire exportation de l'arrayCollection passée en paramètres au format csv
        public static function exportCSV(dg:DataGrid):void
        {
            if (dg)
            {
                var collec:ArrayCollection = dg.dataProvider as ArrayCollection
                if (collec && collec.length > 0)
                {
                    var dataToExport:String = DataGridDataExporter.getCSVString(dg, ";", "\n", false);
                    var url:String = index.urlBackoffice + "/fr/consotel/consoview/gridexports/exportCSVString.cfm";
                    DataGridDataExporter.exportCSVString(url, dataToExport, "export");
                        // Alert.show("Classe datagridExporter à importer");
                }
                else
                {
                    // Alert.show(ResourceManager.getInstance().getString('Acceuil', 'Pas_de_donn_es___exporter__'), "Consoview");
                    Alert.show("Pas de données à exporter");
                }
            }
        }
    }
}