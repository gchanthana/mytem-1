package intranet.gestionGroupe
{
    import flash.events.Event;
    import flash.events.MouseEvent;
    import fr.consotel.consoview.util.ConsoViewErrorManager;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.controls.dataGridClasses.DataGridColumn;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;

    public class GridSocieteAffectee extends GridSocieteIHM
    {
        [Bindable]
        private var myFilterArraySociete:ArrayCollection = new ArrayCollection();
        private var objGestionImport:GestionImport;
        private var idRacineGroupe:int;

        public function GridSocieteAffectee(objGestionImport:GestionImport)
        {
            super();
            this.objGestionImport = objGestionImport;
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
        }

        private function initIHM(event:Event):void
        {
            txtFiltreSocAffect.addEventListener(Event.CHANGE, filtreSociete);
            dgRefClient.addEventListener(Event.CHANGE, checkRefClient);
            btnDesaffecter.addEventListener(MouseEvent.CLICK, deAffecterSociete);
            btnExport.addEventListener(MouseEvent.CLICK, onExportBtnClickedHandler);
            MAJ_AUTO_COLLABORATEUR.labelFunction = setTypeAffichage;
            btnGestionMAJCollab.addEventListener(MouseEvent.CLICK, openFenetreMAJCollab);
        }

        private function openFenetreMAJCollab(ev:MouseEvent):void
        {
            var objPopUp:FenetreGestionMAJCollab = new FenetreGestionMAJCollab(dgRefClient.dataProvider as ArrayCollection, this);
            PopUpManager.addPopUp(objPopUp, objGestionImport, true, null);
            PopUpManager.centerPopUp(objPopUp);
        }

        private function setTypeAffichage(item:Object, co:DataGridColumn):String
        {
            return (item[co.dataField] == 1) ? "Oui" : "Non";
        }

        public function filtreSociete(e:Event):void
        {
            myFilterArraySociete.filterFunction = utilFiltreSociete;
            myFilterArraySociete.refresh();
        }

        public function utilFiltreSociete(item:Object):Boolean
        {
            var chaine:String;
            if (item.MAJ_AUTO_COLLABORATEUR == 1)
            {
                chaine = "Oui"
            }
            else
            {
                chaine = "Non"
            }
            if (((item.LIBELLE as String).toLowerCase()).search(txtFiltreSocAffect.text.toLowerCase()) != -1 || ((item.REF_CLIENT as String).toLowerCase()).search(txtFiltreSocAffect.text.toLowerCase()) != -1 || ((chaine).toLowerCase()).search(txtFiltreSocAffect.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        public function getListeSocieteAffectee(idRacine:int):void
        {
            idRacineGroupe = idRacine;
            var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeSociete", "getListeRefClient", processGetRefClient, remoteError);
            RemoteObjectUtil.callService(op2, idRacine);
        }

        private function deAffecterSociete(ev:MouseEvent):void
        {
            if (dgRefClient.selectedItems != null && dgRefClient.selectedItems.length > 0)
            {
                var tab:Array = dgRefClient.selectedItems;
                var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeSociete", "DesaffecteSociete", processDesaffectation, remoteError);
                RemoteObjectUtil.callService(op2, tab, objGestionImport.getIdGroupeRacineSelectionne());
            }
            else
            {
                Alert.show("Selectionnez un groupe maitre + une ou plusieurs sociétés");
            }
        }

        //Export des données du tableau au format cvs
        private function onExportBtnClickedHandler(evt:MouseEvent):void
        {
            intranet.gestionGroupe.GestionImport.exportCSV(dgRefClient);
        }

        private function processDesaffectation(event:ResultEvent):void
        {
            objGestionImport.updateGridGroupeRacine();
            objGestionImport.updateGridSocieteAll();
            getListeSocieteAffectee(objGestionImport.getIdGroupeRacineSelectionne());
            objGestionImport.selectionneGroupeRacine();
            Alert.show("Suppression de l'affectation : Ok")
            //idRacine=dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT;
        }

        private function checkRefClient(event:Event):void
        {
            if (dgRefClient.selectedItem != null && dgRefClient.selectedItems.length < 2)
            {
                objGestionImport.updateGridCompteFact(dgRefClient.selectedItem.IDREF_CLIENT);
                btnDesaffecter.enabled = true;
            }
        }

        private function remoteError(faultEvent:FaultEvent):void
        {
            ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
        }

        private function processGetRefClient(event:ResultEvent):void
        {
            myFilterArraySociete = event.result as ArrayCollection;
            dgRefClient.dataProvider = myFilterArraySociete;
        }

        public function getIdRacine():int
        {
            return idRacineGroupe;
        }
    }
}