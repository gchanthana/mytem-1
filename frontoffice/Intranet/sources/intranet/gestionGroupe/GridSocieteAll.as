package intranet.gestionGroupe
{
    import flash.events.Event;
    import flash.events.MouseEvent;
    import fr.consotel.consoview.util.ConsoViewErrorManager;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    import intranet.gestionGroupe.GestionImport;
    import mx.collections.ArrayCollection;
    import mx.collections.Sort;
    import mx.collections.SortField;
    import mx.controls.Alert;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.utils.ObjectUtil;

    public class GridSocieteAll extends GridSocieteAllIHM
    {
        //private var _orgaGeoExists:Boolean=false;
        //private var _orgaOpeExists:Boolean=false;
        [Bindable]
        private var myFilterArraySocieteAll:ArrayCollection = new ArrayCollection();
        private var objGestionImport:GestionImport;

        public function GridSocieteAll(objGestionImport:GestionImport)
        {
            this.objGestionImport = objGestionImport;
            super();
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
        }

        private function initIHM(event:Event):void
        {
            /* Gestion des boutons radios */
            rdAffect.addEventListener(MouseEvent.CLICK, getListeSociete);
            rdNonAffect.addEventListener(MouseEvent.CLICK, getListeSociete);
            rdTousAffect.addEventListener(MouseEvent.CLICK, getListeSociete);
            cmbOperateur.addEventListener(Event.CHANGE, updateOperateur);
            dgRefClientAll.addEventListener(Event.CHANGE, updateDgSociete);
            btnModifSociete.addEventListener(MouseEvent.CLICK, msgBoxModifSociete);
            btnAffecter.addEventListener(MouseEvent.CLICK, affecterSociete);
            btnExport.addEventListener(MouseEvent.CLICK, onExportBtnClickedHandler);
            txtFiltreSociete.addEventListener(Event.CHANGE, filtreSocieteAll);
            getListeOpera(null);
        }

        private function updateOperateur(event:Event):void
        {
            getListeSociete(new MouseEvent(""));
        }

        private function onExportBtnClickedHandler(evt:MouseEvent):void
        {
            intranet.gestionGroupe.GestionImport.exportCSV(dgRefClientAll);
        }

        private function affecterSociete(ev:MouseEvent):void
        {
            if (dgRefClientAll.selectedItems != null && dgRefClientAll.selectedItems.length > 0)
            {
                if (objGestionImport.getIdGroupeRacineSelectionne() != -1)
                {
                    if (objGestionImport.verifDoublonRefClient() == true)
                    {
                        var tab:Array = dgRefClientAll.selectedItems;
                        var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeSociete", "affecteSociete", processAffectation, remoteError);
                        RemoteObjectUtil.callService(op2, tab, objGestionImport.getIdGroupeRacineSelectionne());
                    }
                    else
                    {
                        Alert.show("Cette société est déja afféctée à ce groupe racine");
                    }
                }
                else
                {
                    Alert.show("Selectionnez un groupe racine");
                }
            }
            else
            {
                Alert.show("Selectionnez un groupe racine + une ou plusieurs sociétés");
            }
        }

        private function processAffectation(event:ResultEvent):void
        {
            Alert.show("Affectation : Ok");
            objGestionImport.updateGridGroupeRacine();
            objGestionImport.updateGridSocieteAffectee(objGestionImport.getIdGroupeRacineSelectionne());
            objGestionImport.selectionneGroupeRacine();
            getListeSociete(null);
        }

        private function updateDgSociete(event:Event):void
        {
            btnAffecter.enabled = true;
            btnModifSociete.enabled = true;
        }

        private function msgBoxModifSociete(event:MouseEvent):void
        {
            if (dgRefClientAll.selectedIndex == -1)
            {
                Alert.show("Sélectionnez une société");
            }
            else
            {
                trace("-------> " + dgRefClientAll.selectedItem.LIBELLE + " ---------->" + dgRefClientAll.selectedItem.NOM);
                var monPopUp:FenetreAjouterSociete = new FenetreAjouterSociete(this, true, dgRefClientAll.selectedItem.LIBELLE, dgRefClientAll.selectedItem.NOM);
                PopUpManager.addPopUp(monPopUp, this, true);
                PopUpManager.centerPopUp(monPopUp);
            }
        }

        private function getListeOpera(event:Event):void
        {
            var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeSociete", "getListeOperateur", processGetListeOperateur, remoteError);
            RemoteObjectUtil.callService(op2);
        }

        public function addSociete(libelleSociete:String, modif:Boolean = false):void
        {
            if (modif == false)
            {
                /*
                   //Ajout supprimé de l'interface
                   trace("new société");
                   var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
                   "fr.consotel.consoprod.intranet.procedureGestionGroupeSociete",
                   "addSociete",
                   processAddSociete,remoteError);
                 RemoteObjectUtil.callService(op,libelleSociete);*/
            }
            else
            {
                var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeSociete", "updateSociete", processModifSociete, remoteError);
                RemoteObjectUtil.callService(op2, dgRefClientAll.selectedItem.IDREF_CLIENT, libelleSociete);
                trace("modif société");
            }
        }

        private function processAddSociete(event:ResultEvent):void
        {
            getListeSociete(null);
            Alert.show("Création société : Ok");
        }

        private function processModifSociete(event:ResultEvent):void
        {
            getListeSociete(null);
            Alert.show("Modification société : Ok");
        }

        public function getListeOperateur():ArrayCollection
        {
            return ObjectUtil.copy(cmbOperateur.dataProvider as ArrayCollection) as ArrayCollection;
        }

        private function processGetListeSociete(event:ResultEvent):void
        {
            var result:ArrayCollection = event.result as ArrayCollection;
            myFilterArraySocieteAll = event.result as ArrayCollection;
            dgRefClientAll.dataProvider = myFilterArraySocieteAll;
            var sort:Sort = new Sort();
            sort.fields = [ new SortField("DATECREATED", false, true) ];
            (dgRefClientAll.dataProvider as ArrayCollection).sort = sort;
            (dgRefClientAll.dataProvider as ArrayCollection).refresh();
        }

        private function processGetListeOperateur(event:ResultEvent):void
        {
            cmbOperateur.dataProvider = event.result as ArrayCollection;
            cmbOperateur.dataProvider.addItemAt({ NOM: "Tous", OPERATEURID: -1 }, 0);
            cmbOperateur.selectedIndex = 0;
            getListeSociete(new MouseEvent(""));
        }

        public function filtreSocieteAll(e:Event):void
        {
            myFilterArraySocieteAll.filterFunction = utilFiltreSocieteAll;
            myFilterArraySocieteAll.refresh();
        }

        public function getListeSociete(ev:MouseEvent):void
        {
            if (rdGroup.selectedValue == 1)
            {
                var op3:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeSociete", "getAffectSoc", processGetListeSociete, remoteError);
                RemoteObjectUtil.callService(op3, cmbOperateur.selectedItem.OPERATEURID);
            }
            else if (rdGroup.selectedValue == 0)
            {
                var op4:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeSociete", "getNonAffectSoc", processGetListeSociete, remoteError);
                RemoteObjectUtil.callService(op4, cmbOperateur.selectedItem.OPERATEURID);
            }
            else
            {
                var op5:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeSociete", "getAllSoc", processGetListeSociete, remoteError);
                //RemoteObjectUtil.callService(op5,[cmbOperateur.selectedItem.]);
                RemoteObjectUtil.callService(op5, cmbOperateur.selectedItem.OPERATEURID);
            }
        }

        private function remoteError(faultEvent:FaultEvent):void
        {
            ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
        }

        private function checkOpe(elem:*, index:int, tab:Array):void
        { //Vérification de l'opérateur
            // vérifie le choix du combo
        /*	if (_orgaOpeExists == false) {
           if (elem.OPERATEURID==cmbOperateur.selectedItem.data) {
           _orgaOpeExists=true;
           } else {
           _orgaOpeExists=false;
           }
         }*/
        }

        public function utilFiltreSocieteAll(item:Object):Boolean
        {
            if ((String(item.LIBELLE).toLowerCase().search(txtFiltreSociete.text.toLowerCase()) != -1) || (String(item.DATECREATED).search(txtFiltreSociete.text) != -1) || (String(item.CONTRAT).toLowerCase().search(txtFiltreSociete.text.toLowerCase()) != -1) || (String(item.REF_CLIENT).toLowerCase().search(txtFiltreSociete.text.toLowerCase()) != -1))
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }
    }
}