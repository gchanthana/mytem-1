package intranet.gestionGroupe
{
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import fr.consotel.consoview.util.ConsoViewErrorManager;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;

    public class GridGroupeRacine extends GridGroupeRacineIHM
    {
        [Bindable]
        private var myFilterArrayGroupe:ArrayCollection = new ArrayCollection();
        private var objGestionImport:GestionImport;
        private var idRacine:int = -1;
        private var reselectionneGroupeRacine:Boolean = false;

        // les instances graphiques
        // public var btnExport:Button;
        public function GridGroupeRacine(objGestionImport:GestionImport)
        {
            this.objGestionImport = objGestionImport;
            super();
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
        }

        private function initIHM(event:Event):void
        {
            txtFiltre.addEventListener(Event.CHANGE, filtre);
            getListeGroupe(null);
            /*Ecouteurs*/
            btnAddGroupe.addEventListener(MouseEvent.CLICK, msgBoxAddGroupe);
            btnDeleteGroupe.addEventListener(MouseEvent.CLICK, msgDeleteGroupe);
            btnModifGroupe.addEventListener(MouseEvent.CLICK, msgBoxModifGroupe);
            btnExport.addEventListener(MouseEvent.CLICK, onExportBtnClickedHandler);
            dgGroupeMaitre.addEventListener(Event.CHANGE, updateSocieteAffectee);
        }

        private function filtre(e:Event):void
        {
            (dgGroupeMaitre.dataProvider as ArrayCollection).filterFunction = utilFiltreGridGroupe;
			(dgGroupeMaitre.dataProvider as ArrayCollection).refresh();
        }

        private function utilFiltreGridGroupe(item:Object):Boolean
        {
			if(item.CODE_APPLI != null)
			{
				if (((item.LIBELLE_GROUPE_CLIENT as String).toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1 || ((item.LIBELLE_APP as String).toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
				{
					return (true);
				}
				else
				{
					return (false);
				}				
			}
			else
			{
				if (((item.LIBELLE_GROUPE_CLIENT as String).toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
				{
					return (true);
				}
				else
				{
					return (false);
				}	
			}
        }

        private function msgBoxAddGroupe(event:MouseEvent):void
        {
            var monPopUp:FenetreAjouterGroupe = new FenetreAjouterGroupe(this, false);
            PopUpManager.addPopUp(monPopUp, this, true);
            PopUpManager.centerPopUp(monPopUp);
        }

        //Affichage du msg de confirmation
        private function msgDeleteGroupe(event:MouseEvent):void
        {
            if (dgGroupeMaitre.selectedIndex != -1)
            {
                Alert.show("Confirmez la suppression du groupe svp", "Confirmation", Alert.OK | Alert.CANCEL, null, deleteGroupe, null, Alert.OK);
            }
            else
            {
                Alert.show("Sélectionnez un groupe");
            }
        }

        //Export des données du tableau au format cvs
        private function onExportBtnClickedHandler(evt:MouseEvent):void
        {
            intranet.gestionGroupe.GestionImport.exportCSV(dgGroupeMaitre);
        }

        private function deleteGroupe(evt_obj:Object):void
        {
            if (evt_obj.detail == Alert.OK)
            {
                var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeGroupeRacine", "deleteGroupeClient", processDeleteGroupe, remoteError);
                RemoteObjectUtil.callService(op2, dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
                trace("delete-------------------------->>");
                    //processDeleteGroupe(null);
            }
            else
            {
                if (evt_obj.detail == Alert.CANCEL)
                {
                    //Alert.show("annulé");
                }
            }
        }

        private function processDeleteGroupe(event:ResultEvent):void
        {
            trace("result**");
            if (event.result as int == -2)
            {
                Alert.show("Impossible de supprimer ce groupe racine. Certains éléments lui font référence.");
            }
            else
            {
                Alert.show("Suppression groupe : ok");
                getListeGroupe(null);
            }
        }

        private function updateSocieteAffectee(event:Event):void
        {
            if (dgGroupeMaitre.selectedIndex != -1)
            {
                objGestionImport.updateAll();
                objGestionImport.updateGridSocieteAffectee(dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
                btnModifGroupe.enabled = true;
                btnDeleteGroupe.enabled = true;
                idRacine = dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT;
            }
            else
            {
                btnModifGroupe.enabled = false;
                btnDeleteGroupe.enabled = false;
                objGestionImport.setIdGroupeRacineSelectionne(-1);
            }
        }

        public function getListeGroupe(event:Event):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeGroupeRacine", "getGroupe", processInitGroupes, remoteError);
            RemoteObjectUtil.callService(op);
        }

        private function processInitGroupes(event:ResultEvent):void
        {
			for each(var obj:Object in event.result as ArrayCollection)
			{
				if(obj.hasOwnProperty("CODE_APPLI"))
				{
					switch(obj.CODE_APPLI)
					{
						case "1":
							if(obj.hasOwnProperty("TYPE_LOGIN"))
							{
								if(obj.TYPE_LOGIN == 1)
									obj.LIBELLE_APP = "CV3";
								else if(obj.TYPE_LOGIN == 2)
									obj.LIBELLE_APP = "CV4";
								else
									obj.LIBELLE_APP = "null";
							}
							break;
						case "51":
							obj.LIBELLE_APP = "PFGP";
							break;
						case "101":
							obj.LIBELLE_APP = "MYTEM360";
							break;
						default :
							obj.LIBELLE_APP = obj.CODE_APPLI;
							break;
					}
				}
			}
            myFilterArrayGroupe = event.result as ArrayCollection;
            dgGroupeMaitre.dataProvider = (event.result as ArrayCollection).source;
            if (reselectionneGroupeRacine == true)
            {
                reselectionneGroupe();
                reselectionneGroupeRacine = false;
            }
        }

        private function reselectionneGroupe():void
        {
            var index:int = 0;
            for (var i:int = 0; i < ((dgGroupeMaitre.dataProvider) as ArrayCollection).length; i++)
            {
                if (((dgGroupeMaitre.dataProvider) as ArrayCollection).getItemAt(i).IDGROUPE_CLIENT == idRacine)
                {
                    index = i;
                }
            }
            dgGroupeMaitre.selectedIndex = (index);
            callLater(scrollToModif);
        }

        private function scrollToModif():void
        {
            dgGroupeMaitre.scrollToIndex(dgGroupeMaitre.selectedIndex);
            //idRacine=-1;
            //updateOrga(null);
        }

        private function remoteError(faultEvent:FaultEvent):void
        {
            ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
        }

        private function msgBoxModifGroupe(event:MouseEvent):void
        {
            var monPopUp:FenetreAjouterGroupe = new FenetreAjouterGroupe(this, true, dgGroupeMaitre.selectedItem.LIBELLE_GROUPE_CLIENT, dgGroupeMaitre.selectedItem.COMMENTAIRES, dgGroupeMaitre.selectedItem.IDRACINE_MASTER);
            monPopUp.enableLib = false;
			PopUpManager.addPopUp(monPopUp, this, true);
            PopUpManager.centerPopUp(monPopUp);
        }

        public function addGroupeMaitre(libelleGroupe:String, commentaire:String, modif:Boolean = false, id_master:String = ""):void
        {
            var idmaster:Number = Number(id_master);
            if (modif == false)
            {
                var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeGroupeRacine", "addGroupeMaitre", processAddGroupe, remoteError);
                RemoteObjectUtil.callService(op, libelleGroupe, commentaire, idmaster);
            }
            else
            {
                var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.procedureGestionGroupeGroupeRacine", "update_racine", processAddGroupe, remoteError);
                RemoteObjectUtil.callService(op2, dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT, libelleGroupe, commentaire, idmaster);
                trace("RemoteObjectUtil.callService(op2,dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT,libelleGroupe,commentaire);");
            }
        }

        private function processAddGroupe(event:ResultEvent):void
        {
            //dgGroupeMaitre.dataProvider=event.result;
            Alert.show("Enregistrement : OK");
            getListeGroupe(null);
        }

        public function setReselectionneGroupeRacine(etat:Boolean):void
        {
            reselectionneGroupeRacine = etat;
        }
    }
}