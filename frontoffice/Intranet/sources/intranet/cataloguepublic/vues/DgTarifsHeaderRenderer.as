package intranet.cataloguepublic.vues
{
	import mx.controls.dataGridClasses.DataGridItemRenderer;

	public class DgTarifsHeaderRenderer extends DataGridItemRenderer
	{
		public function DgTarifsHeaderRenderer()
		{
			
			super();
			
			setStyle("textAlign","center");
		}
		     
	}
}