package intranet.GestionProduit
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class GestionTheme extends HBox
	{
		public var Vgauche:VBox;
		public var Vdroite:VBox;

		private var GTafixe:AdvancedDataGrid;
		private var GTamobile:AdvancedDataGrid;
		private var GTadata:AdvancedDataGrid;
		
		private var GTcfixe:AdvancedDataGrid;
		private var GTcmobile:AdvancedDataGrid;
		private var GTcdata:AdvancedDataGrid;

		[Bindable]
		public var af:ArrayCollection= new ArrayCollection();
		[Bindable]
		public var am:ArrayCollection= new ArrayCollection();
		[Bindable]
		public var ad:ArrayCollection= new ArrayCollection();
		[Bindable]
		public var cf:ArrayCollection= new ArrayCollection();
		[Bindable]
		public var cm:ArrayCollection= new ArrayCollection();
		[Bindable]
		public var cd:ArrayCollection= new ArrayCollection();

		public function GestionTheme()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,creationHandler);
		}
		
		public function creationHandler(e:FlexEvent):void{
			getData();
		}
		
// FONCTION POUR RECUPERER LES DONNEES DE LA BASE DE DONNEE - EFFECTUE A CREATION_COMPLETE		
		public function getData():void{
			var tmp:AbstractOperation=
			RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
			"fr.consotel.consoprod.gestionproduits.GestionProduits",
			"getListeTheme",
			ok,no);		
			RemoteObjectUtil.callService(tmp);	
		}
		
		public function ok(e:ResultEvent):void{
			DataSample.TabGestionTheme1= e.result as ArrayCollection;
			Parser(DataSample.TabGestionTheme1);
		}
		
		public function no(e:FaultEvent):void{
			trace("Gestiontheme - getData() :"+e.message);
		}

// FONCTION DE PARSE DES THEMES PAR Abo/Conso puis Data/Fixe/Mobile		
		public function Parser(t:ArrayCollection):void{
			var s:String;
			var a:Array;
			
			ad.removeAll();
			af.removeAll();
			am.removeAll();
			cd.removeAll();
			cf.removeAll();
			cm.removeAll();
			
			for(var i:int=0;i<t.length;i++){
				s=t[i].THEME;
				a=s.split('/');
	// SI C'EST UN ABONNEMENT				
				if(a[1]=="Abonnements"){
					switch(a[0]){
						case "Data":
							t[i].THEME=a[3];
							ad.addItem(t[i]);
							break;
						case "Fixe":
							t[i].THEME=a[3];
							af.addItem(t[i]);
							break;
						case "Mobile":
							t[i].THEME=a[3];
							am.addItem(t[i]);
							break;
					}
				}
	// SI C'EST UNE CONSOMMATION				
				if(a[1]=="Consommations"){
					switch(a[0]){
						case "Data":
							t[i].THEME=a[3];
							cd.addItem(t[i]);
							break;
						case "Fixe":
							t[i].THEME=a[3];
							cf.addItem(t[i]);
							break;
						case "Mobile":
							t[i].THEME=a[3];
							cm.addItem(t[i]);
							break;
					}
				}
			}
 	// ON TRIE LES TABLEAUX	PAR ORDRE ALPHABETIQUE		
			var dataSortField:SortField = new SortField();
            dataSortField.name = "THEME";
            var DataSort:Sort = new Sort();
            DataSort.fields = [dataSortField]; 
            
            ad.sort=DataSort;
            ad.refresh();
            af.sort=DataSort;
            af.refresh();
            am.sort=DataSort;
            am.refresh();
            cd.sort=DataSort;
            cd.refresh();
            cf.sort=DataSort;
            cf.refresh();
            cm.sort=DataSort;
            cm.refresh();
		}	
	}
}