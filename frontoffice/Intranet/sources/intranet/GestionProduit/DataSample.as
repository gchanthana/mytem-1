package intranet.GestionProduit
{
	
	import mx.collections.ArrayCollection;
	[Bindable]
	public class DataSample
	{
		public function DataSample()
		{
		}

		public static var boolcombo:Boolean;

// TABLEAU OU SONT LISTES LES RESULTATS DE LA RECHERCHE PRODUIT
		public static var TabRechercheProduit:ArrayCollection= new ArrayCollection();		

// TABLEAU OU SONT LISTES LES CLIENTS D'UN PRODUIT
		public static var TabClientProduit:ArrayCollection= new ArrayCollection();

// TABLEAU OU SONT LISTES LES TYPES D'ACCES
		public static var TabAcces:Array=['','Remise','Prod. Acces','Aucun'];		

// TABLEAUX OU SONT LISTES LES THEMES PAR ABO ET CONSO
		public static var TabAbo:ArrayCollection= new ArrayCollection();
		public static var TabConso:ArrayCollection= new ArrayCollection();

// TABLEAU OU SONT LISTES TOUS LES OPERATEURS : utile pour le composant CBTriOpe		
		public static var TabOperateur:ArrayCollection = new ArrayCollection();		
			public static var TabOpeOk:Boolean=false;
			
// TABLEAU OU SONT LISTES TOUS LES THEMES : utile pour le composant GestionTheme2		
		public static var TabTheme:ArrayCollection= new ArrayCollection();
			public static var TabTheOk:Boolean=false;

// TABLEAU OU SONT LISTES TOUS LES THEMES : utile pour le composant GestionTheme2		
		public static var TabGroupe:ArrayCollection= new ArrayCollection();
			public static var TabGroProOk:Boolean=false;



		
// TABLEAU OU SONT LISTES TOUS LES PRODUITS NON AFFECTES		
		public static var TabGestionProduit:ArrayCollection = new ArrayCollection();
					
// TABLEAU OU SONT LISTE LES PRODUITS PAR THEME
		public static var TabGestionTheme1:ArrayCollection=new ArrayCollection();

// TABLEAU OU SONT LISTES TOUS LES PRODUITS AFFECTES AU THEME		
		public static var TabGestionTheme2:ArrayCollection= new ArrayCollection();
			
// TABLEAU OU SONT LISTES LES PRODUITS ET LES ACCES : utile pour le composant GestionAcces
		public static var TabGestionAcces:ArrayCollection= new ArrayCollection();
		
// TABLEAU OU SONT LISTES LES PRODUITS AFFECTES AU GROUPE DE RATIONALISATION
		public static var TabGestionGroupeRatio:ArrayCollection= new ArrayCollection();



		
	}
}