package intranet.GestionProduit
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import intranet.GestionProduit.event.ChangeData_EVENT;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.core.IFlexDisplayObject;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class GestionGroupeRationalisation extends VBox
	{
		public var adg:AdvancedDataGrid;
		public var CBtri:CBTriOpeIHM;
		public var CBListeGroupeProduit:ComboBox;
		public var CBNomTheme:ComboBox;
		public var CheckPNA:CheckBox;
		public var btn:Button;
		public var btnModif:Button;
		public var btnSupp:Button;
		public var txtFiltre:TextInput;	
		public var colProd:AdvancedDataGridColumn;
		private var tmpList:Array=new Array();
				
		public function GestionGroupeRationalisation()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}

// ------------------------------------------------***************************------------------------------------------------

// DATATIP DU PRODUIT
		public function ProdDataTip(item:Object):String{
			var s:String="";
			
			if(item.hasOwnProperty("PRODUIT")){
				 s=item.CATTYPE+"\n -> "+item.PRODUIT;
			}
			return s;
		}	
		
// ------------------------------------------------***************************------------------------------------------------

// FONCTION D'INIT DES COMBOBOX	et DES LISTENERS	
		public function init(e:Event):void{
			if(!DataSample.TabGroProOk){
				var tmpOpe:AbstractOperation=RemoteObjectUtil.getOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestionproduits.GestionProduits",
						"getListeGroupe",
						Result,Fault);
				RemoteObjectUtil.callService(tmpOpe);
				DataSample.TabGroProOk=true;
			}
			
			if(!DataSample.TabTheOk){
				var tmp:AbstractOperation=RemoteObjectUtil.getOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestionproduits.GestionProduits",
						"getListeTheme",
						ok,no);
				RemoteObjectUtil.callService(tmp);
				DataSample.TabTheOk=true;
			}
	// ON ECOUTE LES COMBOBX - LE BOUTON - LA CHECKBOX		
			CBtri.addEventListener(ListEvent.CHANGE,OperateurChange);
			CBListeGroupeProduit.addEventListener(ListEvent.CHANGE,GroupChange);
			CBNomTheme.addEventListener(ListEvent.CHANGE,ThemeChange);
			btn.addEventListener(MouseEvent.CLICK,maj);
			CheckPNA.addEventListener(Event.CHANGE,ChkChange);
			adg.addEventListener(ChangeData_EVENT.ChangeData_EVENT,modifData);
			txtFiltre.addEventListener(Event.CHANGE,filtrer);
			txtFiltre.addEventListener(MouseEvent.ROLL_OVER,filtreReset);
			btnModif.addEventListener(MouseEvent.CLICK,modifGroupe);
			btnSupp.addEventListener(MouseEvent.CLICK,onSupp);
		}
		
		public function Result(e:ResultEvent):void{
			DataSample.TabGroupe= e.result as ArrayCollection;
		}
		
		public function Fault(e:FaultEvent):void{
			trace("GestionGroupeRationalisation - init() :"+e.message);
		}

		public function ok(e:ResultEvent):void{
			var item:Object=new Object();
			item.IDTHEME_PRODUIT=0;
			item.THEME="** Tous **";
			
			DataSample.TabTheme= e.result as ArrayCollection;
			DataSample.TabTheme.addItemAt(item,0);
		}
		
		public function no(e:FaultEvent):void{
			trace("GestionGroupeRationalisation - init() :"+e.message);
		}

// ------------------------------------------------***************************------------------------------------------------

// MODIFICATION DU GROUPE DE PRODUIT
		public function modifGroupe(e:MouseEvent):void{
			var ABox:IFlexDisplayObject ;
			if(!ABox){
				ABox = PopUpManager.createPopUp(this,Alert_Modif_GroupeRatioIHM,true);
				ABox.addEventListener(CloseEvent.CLOSE,onClose);
			}else{
				PopUpManager.addPopUp(ABox,this,true);
			}
			PopUpManager.centerPopUp(ABox);
		}	

		public function onClose(e:CloseEvent):void{
			var ABox:IFlexDisplayObject = e.target as IFlexDisplayObject;
			PopUpManager.removePopUp(ABox);
		}
		
// ------------------------------------------------***************************------------------------------------------------		
		
// FILTRE
		public function filtrer(e:Event):void{
			DataSample.TabGestionGroupeRatio.filterFunction= filtreProduit;
			DataSample.TabGestionGroupeRatio.refresh();
		}
		
		public function filtreProduit(item:Object):Boolean{
			if(String(item.PRODUIT).match(new RegExp(txtFiltre.text,'i'))) return true;
			else return false;
		}
				
		public function filtreReset(e:MouseEvent):void{
			txtFiltre.text="";
		}
		
// ------------------------------------------------***************************------------------------------------------------		
		
// REMPLISSAGE DU TABLEAU AVEC LES LIGNES QUI ONT ETE MODIFIE			
		public function modifData(e:ChangeData_EVENT):void{
			var existe:Boolean=false;
			
			if(tmpList == null)
				tmpList=new Array();

			if(tmpList.length>0){
				for(var i:int=0;i<tmpList.length;i++){
					if(tmpList[i].IDPRODUIT_CATALOGUE == e.item.IDPRODUIT_CATALOGUE){
						tmpList[i].IDGROUPE_PRODUIT=e.item.IDGROUPE_PRODUIT;
						tmpList[i].GROUPE=e.item.GROUPE;
						existe=true;
						break;
					}
				}
				if(!existe)
					tmpList.push(e.item);
			}else{
				tmpList.push(e.item);
			}
		}	
		
// ------------------------------------------------***************************------------------------------------------------		
			
// FONCTION(S) QUI GERE(NT) LES DIFFERENTS CHOIX DANS LES COMBOBOX
		// RECUPERATION DES DONNES APRES SELECTION D'UN GROUPE
		public function GroupChange(e:Event):void{
			var GroupID:int=0;
			
			CBtri.visible=true;
			CBtri.selectedIndex=-1;
			CBNomTheme.visible=true;
			CBNomTheme.selectedIndex=-1;
			CheckPNA.selected=false;

			if(CBListeGroupeProduit.selectedIndex > -1)
				GroupID=CBListeGroupeProduit.selectedItem.IDGROUPE_PRODUIT;

			getGroupeProduit(0,0,CBListeGroupeProduit.selectedItem.IDGROUPE_PRODUIT,0);

		}
		
		// RECUPERATION DES DONNEES APRES SELECTION D'UN THEME : 
		public function ThemeChange(e:Event):void{
			var OpeID:int=0;
			var themeProduitID:int=CBNomTheme.selectedItem.IDTHEME_PRODUIT;;
			var GroupID:int=0;
			var ok:int=0;

			if(!CheckPNA.selected){
				GroupID=CBListeGroupeProduit.selectedItem.IDGROUPE_PRODUIT;
			}else{
				ok=1;
			}
			
			if(CBNomTheme.selectedIndex > -1)
				themeProduitID=CBNomTheme.selectedItem.IDTHEME_PRODUIT;
			
			if(CBtri.selectedIndex > -1){
				OpeID=CBtri.selectedItem.OPERATEURID;
			}			
				
			getGroupeProduit(OpeID,themeProduitID,GroupID,ok);
		}

		// RECUPERATION DES DONNEES APRES SELECTION D'UN OPERATEUR DANS CBTriOpe		
		public function OperateurChange(e:Event):void{
			var OpeID:int=0;
			var themeProduitID:int;
			var GroupID:int=0;
			var ok:int=0;

			if(!CheckPNA.selected){
				GroupID=CBListeGroupeProduit.selectedItem.IDGROUPE_PRODUIT;
			}else{
				ok=1;
			}
			
			if(CBtri.selectedIndex > -1){
				OpeID=CBtri.selectedItem.OPERATEURID;
			}				
			
			if(CBNomTheme.selectedIndex > -1)
				themeProduitID=CBNomTheme.selectedItem.IDTHEME_PRODUIT;
				
			getGroupeProduit(OpeID,themeProduitID,GroupID,ok);
			
		}
		
		// RECUPERATION DES DONNEES APRES SELECTION DE LA CHECKBOX
		public function ChkChange(e:Event):void{
			var OpeID:int=0;
			var themeProduitID:int=0;
			var GroupID:int=0;
			
			if(CheckPNA.selected){
				CBListeGroupeProduit.selectedIndex=-1;
				CBNomTheme.visible=true;
				CBtri.visible=true;
							
				if(CBNomTheme.selectedIndex > -1)
					themeProduitID= CBNomTheme.selectedItem.IDTHEME_PRODUIT;
				
				if(CBtri.selectedIndex > -1)
					OpeID= CBtri.selectedItem.OPERATEURID;
				
				getGroupeProduit(OpeID,themeProduitID,GroupID,int(CheckPNA.selected));
			}else{
				CBNomTheme.visible=false;
				CBtri.visible=false;
				
				if(CBListeGroupeProduit.selectedIndex > -1)
					GroupID=CBListeGroupeProduit.selectedItem.IDGROUPE_PRODUIT;
					
				getGroupeProduit(OpeID,themeProduitID,GroupID,int(CheckPNA.selected));
			}
		}
		
// ------------------------------------------------***************************------------------------------------------------		
		
// FONCTION QUI LANCE LA PROCEDURE getGroupeProduit
		public function getGroupeProduit(OpeID:int,themeProduitID:int,GroupID:int,ok:int):void{
			var tmpope:AbstractOperation=RemoteObjectUtil.getOperationFrom(
					RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestionproduits.GestionProduits",
					"getGroupeProduit",
					ProcResult,ProcFault);
			RemoteObjectUtil.callService(tmpope,OpeID,themeProduitID,GroupID,ok);
			txtFiltre.text="";
		}
		
		public function ProcResult(e:ResultEvent):void{
			DataSample.TabGestionGroupeRatio= e.result as ArrayCollection;
		}
		
		public function ProcFault(e:FaultEvent):void{
			trace("GestionGroupeRationalisation - getGroupeProduit :"+e.message);
		}

// ------------------------------------------------***************************------------------------------------------------

// QUAND ON APPUIE SUR LE BOUTON ENREGISTRER
		public function maj(e:MouseEvent):void{
			if(tmpList.length != 0){
				var tmpListe:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestionproduits.GestionProduits",
						"updGroupe",
						majok,majno);
				RemoteObjectUtil.callService(tmpListe,tmpList);		
			}
			txtFiltre.text="";
			CheckPNA.selected = false;
		}	
		
		public function majok(e:ResultEvent):void{
			for(var i:int=0;i<tmpList.length;i++){
				if(tmpList[i].GROUPID != ""){
					for(var j:int=0;j<DataSample.TabGestionGroupeRatio.length;j++){
						if(DataSample.TabGestionGroupeRatio[j].IDPRODUIT_CATALOGUE == tmpList[i].IDPRODUIT_CATALOGUE){
							DataSample.TabGestionGroupeRatio.removeItemAt(j);
							break;
						}
					}
				}
			}
			this.tmpList=new Array();
			DataSample.TabGestionGroupeRatio.refresh();
		}
		
		public function majno(e:FaultEvent):void{
			trace("GestionGroupeRatio - maj() :"+e.message);
		}
		
// SUPPRESION D'UN GROUPE DE RATIO- envoit d'un mail à guillaume		
		
		public function onSupp(e:MouseEvent):void{
			if(CBListeGroupeProduit.selectedIndex > -1){
				Alert.show( "Etes vous sur(e) de vouloir supprimer le groupe de ratio '"+CBListeGroupeProduit.selectedItem.GROUPE+"' ?",
						"Confirmation de suppression",
					 	Alert.OK || Alert.CANCEL, this,checkSuppGroup);
			}
				
		}
		
		public function checkSuppGroup(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				var tmp:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestionproduits.GestionProduits",
						"SendMailSupp",
						mailok,mailno);
				RemoteObjectUtil.callService(tmp,CBListeGroupeProduit.selectedItem.IDGROUPE_PRODUIT,CBListeGroupeProduit.selectedItem.PRODUIT);
			}
		}
		
		public function mailok(e:ResultEvent):void{
				Alert.show("Un mail, contenant votre demande de suppression de groupe, a été envoyé à l'administrateur DBA (Guillaume).", "Envoi du mail", Alert.OK);
		}
			
		public function mailno(e:FaultEvent):void{
			trace('GestionGroupeRationalisation - checkSuppGroup :'+e.message);
		}
	}
}




