package intranet.GestionProduit
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import intranet.GestionProduit.entities.ProduitsVO;
	import intranet.GestionProduit.event.ChangeData_EVENT;
	import intranet.gestionLoginClasse.UtilAcces;
	
	public class GestionTheme2 extends VBox
	{
		public var adg:AdvancedDataGrid;
		public var CBtri:CBTriOpeIHM;
		public var txtFiltre:TextInput;	 
		public var tmpListe:Array=new Array();
		public var idproduit:int=0;
	
		
		public function GestionTheme2()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}

		public function init(e:FlexEvent):void{
			CBtri.addEventListener(ListEvent.CHANGE,tri);
			adg.addEventListener(ChangeData_EVENT.ChangeData_EVENT,modifData);
			txtFiltre.addEventListener(Event.CHANGE,filtrer);
			txtFiltre.addEventListener(MouseEvent.ROLL_OVER,filtreReset);
		}

// DATATIP DU PRODUIT
		public function ProdDataTip(item:Object):String{
			var s:String="";
			
			if(item.hasOwnProperty("LIBELLE_PRODUIT")){
				 s=item.CATTYPE+"\n -> "+item.PRODUIT;
			}
			return s;
		}	

// FILTRE
		public function filtrer(e:Event):void{
			DataSample.TabGestionTheme2.filterFunction= filtreProduit;
			DataSample.TabGestionTheme2.refresh();
		}
		
		public function filtreProduit(item:Object):Boolean{
			if(String(item.LIBELLE_PRODUIT).match(new RegExp(txtFiltre.text,'i'))) return true;
			else return false;
		}
		
		public function filtreReset(e:MouseEvent):void{
			txtFiltre.text="";
		}
		
// CREATION ET REMPLISSAGE DU TABLEAU QUI CONTIENT L'ID DES LIGNES MODIFIEES		
 		public function modifData(e:ChangeData_EVENT):void{
			var boolExiste:Boolean=false;
			
			if(tmpListe == null)
				tmpListe=new Array();
			
			var item:Object = new Object();
			item = ObjectUtil.copy(e.item);
			
			if(tmpListe.length>0){
				for(var i:int=0;i<tmpListe.length;i++){
					if(tmpListe[i].IDPRODUIT_CATALOGUE == item.IDPRODUIT_CATALOGUE){
						tmpListe[i].IDTHEME_PRODUIT = item.IDTHEME_PRODUIT;
						tmpListe[i].BOOL_DRT = item.BOOL_DRT;
						tmpListe[i].PLAN_TYPE = item.PLAN_TYPE;
						tmpListe[i].PLAN_MCAP = (item.PLAN_MCAP!=-1)?item.PLAN_MCAP*1024:item.PLAN_MCAP; // conversion en KiloOctets
						boolExiste=true;
						break;
					}
				}
				
				if(!boolExiste)
					tmpListe.push(item);
				
			}else{
				tmpListe.push(item);
			}
			//(adg.dataProvider as ArrayCollection).itemUpdated(e.item)
		} 
		
// GESTION DU FILTRE OPERATEUR PAR LA COMBO
		public function tri(e:ListEvent):void{
			if(DataSample.TabGestionTheme2 != null)
			{
				DataSample.TabGestionTheme2.source = new Array();
			}
			var tmpOperation:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestionproduits.GestionProduits",
					"getThemesProduits",
					getResult,getFault);
			RemoteObjectUtil.callService(tmpOperation,idproduit,CBtri.selectedItem.OPERATEURID);
			DataSample.TabGestionTheme2.refresh();
			txtFiltre.text="";
			this.tmpListe=null;
		}
		
		public function getResult(e:ResultEvent):void{
			for each (var item:Object in (e.result as ArrayCollection))
			{
				if(item.PLAN_MCAP > 0)
					item.PLAN_MCAP = item.PLAN_MCAP/1024;
			}
			DataSample.TabGestionTheme2 = ProduitsVO.formatProduitsVO((e.result as ArrayCollection));
		}
		
		public function getFault(e:FaultEvent):void{
			trace("GestionTheme2 - tri():"+e.message);
		}

// ON REMPLIT LA GRILLE - SELECT de la BDD
		public function getData():void{
			CBtri.selectedIndex=-1;
			var tmpOperation:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.gestionproduits.GestionProduits",
				"getThemesProduits",
				ResultHandler,FaultHandler);
			RemoteObjectUtil.callService(tmpOperation,idproduit);
			DataSample.TabGestionTheme2.refresh();
			txtFiltre.text="";
		}

		public function ResultHandler(e:ResultEvent):void{
			for each (var item:Object in (e.result as ArrayCollection))
			{
				if(item.PLAN_MCAP > 0)
					item.PLAN_MCAP = item.PLAN_MCAP/1024;
			}
			DataSample.TabGestionTheme2 = ProduitsVO.formatProduitsVO((e.result as ArrayCollection));
		}
		
		public function FaultHandler(e:FaultEvent):void{
			trace("GestionTheme2 - getData() : "+e.message);	
		}

// ON MET A JOUR LA BDD - UPDATE la BDD
		public function UpdTheme():void{
			var boolMsgAlert:Boolean = false;
			for each(var item:Object in (adg.dataProvider as ArrayCollection))
			{
				if(item.BOOL_DRT!=0 && item.PLAN_MCAP==0) //si case datalert est cochée(autres champs sont donc enable) et que le champ [capacite] est vide
				{
					boolMsgAlert = true;
					break;
				}
			}
			if(boolMsgAlert)
			{
				ConsoviewAlert.afficherAlertConfirmation("un champ capacité est vide, veuillez corriger","Attention",null);
			}
			else
			{
	 			if(tmpListe.length > 0){
					var tmpOpe:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoprod.gestionproduits.GestionProduits",
							"updTheme",
							majOk,majNo);
					RemoteObjectUtil.callService(tmpOpe,tmpListe); 
	 			}
	 			txtFiltre.text="";
			}
		}
		
		public function majOk(e:ResultEvent):void{
			for(var i:int=0;i<tmpListe.length;i++)
			{
				trace("i = " + i)
				if(tmpListe[i].IDTHEME_PRODUIT != "" && tmpListe[i].IDTHEME_PRODUIT != idproduit && tmpListe[i].THEME != null && tmpListe[i].THEME != "")
				{
					for(var j:int=0;j<DataSample.TabGestionTheme2.length;j++)
					{
						if(DataSample.TabGestionTheme2[j].IDPRODUIT_CATALOGUE == tmpListe[i].IDPRODUIT_CATALOGUE)
						{
							DataSample.TabGestionTheme2.removeItemAt(j);
							break;
						}
					}
				}
			//DataSample.TabGestionTheme2.refresh();
			}
			this.tmpListe=new Array();
			invalidateDisplayList();
			Alert.show("la modification a bien été effectuée","Intranet");
		}
		
		public function majNo(e:FaultEvent):void{
			trace("GestionTheme2 - UpdTheme() :"+e.message);
		}
	}
}