package intranet.GestionProduit.service
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.CursorBookmark;
	import mx.collections.IViewCursor;
	import mx.utils.ObjectUtil;
	
	import composants.util.CollectionViewInspector;
	
	import intranet.GestionProduit.entities.ProduitsVO;
	
	[Bindable] 
	public class GestionProduitModel extends EventDispatcher
	{	
		private var _service:GestionProduitService;
		
		private var _listeProduits:CollectionViewInspector = new CollectionViewInspector();
			

		public function get listeProduits():CollectionViewInspector
		{
			return _listeProduits;
		}

		internal function set listeProduits(value:CollectionViewInspector):void
		{
			_listeProduits = value;
		}
		
		
		public function GestionProduitModel(service:GestionProduitService)
		{
			_service = service;
		}
		
		
		public function resetList(list:ArrayCollection):void
		{
			list.source = new Array();
			list.sort = null;
			list.refresh();
		}	
		
		internal function getProduitNonAffecte(value:ArrayCollection):void
		{	
			var cursor:IViewCursor = value.createCursor();
			var produit:ProduitsVO;
			var fake:Number = 1;			
			for (var i:int =0; i < fake ; i++)
			{
				cursor.seek(CursorBookmark.FIRST);
				while(!cursor.afterLast)
				{
					produit = ProduitsVO.mappingProduitsVO(cursor.current);		
					listeProduits.addItem(produit);
					cursor.moveNext();
				}
			}
		}
		
		internal function updPNA(value:Array):void
		{
			if(value is Array)
			{
				var cursor:IViewCursor = new ArrayCollection(value).createCursor();				
				cursor.seek(CursorBookmark.FIRST);
				while(!cursor.afterLast)
				{			
					trace(ObjectUtil.toString(cursor))
					//listeProduits.rem(produit);
					cursor.moveNext();
				}
				
				resetList(listeProduits);
				_service.getLastUpdate();
			}
		}
		
	}
}