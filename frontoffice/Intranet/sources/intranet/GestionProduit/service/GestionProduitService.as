package intranet.GestionProduit.service
{
	import mx.rpc.AbstractOperation;
	import mx.utils.ObjectUtil;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import intranet.GestionProduit.entities.FiltreVo;

	public class GestionProduitService
	{
		private var _model:GestionProduitModel;
		public var handler:GestionProduitHandler;
		private var _f:FiltreVo = new FiltreVo();
		
		public function GestionProduitService()
		{
			_model = new GestionProduitModel(this);
			
			handler = new GestionProduitHandler(model);
		}
		
		public function get model():GestionProduitModel
		{
			return _model;
		}		
		
		/**
		 * Récupère les paramètres de l'utilisateur connecté pour un module (au niveau de l'abonnement)
		 */		
		public function getProduitNonAffecte(value:FiltreVo,boolRest:Boolean = true):void
		{
			
			if(value)
			{	
				_f = value;	
			}
			
			if(boolRest)
			{
				_model.resetList(_model.listeProduits);	
			}
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.gestionproduits.GestionProduits",
				"getProduitNonAffecte",
				handler.getProduitNonAffecte,handler.getProduitNonAffecte);
			
			RemoteObjectUtil.callService(op,value.OPERATEURID);		
		}
		
		internal function getLastUpdate():void
		{
			if(_f)
			{	
				getProduitNonAffecte(_f,false)
			}
		}
		
		public function updataPNA(listValue:Array):void
		{
			if(listValue.length > 0)
			{	
				trace(ObjectUtil.toString(_model.listeProduits.updatedItems));
				
				var op:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestionproduits.GestionProduits",
					"upd_PNA_DRT",
					handler.updPNA,handler.updPNA);
				RemoteObjectUtil.callService(op,listValue);		
			}
		}
	}
}