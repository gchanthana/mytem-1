package intranet.GestionProduit.renderers
{
	import flash.events.MouseEvent;

	public class UISelectBoxIR extends UICheckBoxIR
	{
		public function UISelectBoxIR()
		{
			super();
		}
		
		
		override protected function clickHandler(event:MouseEvent):void
		{
			// TODO Auto Generated method stub
			super.clickHandler(event);
			var _function:String = selected?'selectItem':'unSelectItem';		
			_collectionHelper[_function](data);
		}
	}
}