package intranet.GestionProduit.renderers
{
	import flash.events.MouseEvent;

	public class UISelectDrtIR extends UICheckBoxIR
	{
		public function UISelectDrtIR()
		{
		}
		
		override protected function clickHandler(event:MouseEvent):void
		{
			if(event == null) return;
			super.clickHandler(event);
			
			data.BOOL_DRT = (selected)?1:0;
			data.PLAN_TYPE = (data.BOOL_DRT==1)?1:null;
		}
	}
}