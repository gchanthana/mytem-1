package intranet.GestionProduit.renderers
{
	import mx.controls.Label;

	public class HeaderRendererOperateur extends Label
	{
		public function HeaderRendererOperateur()
		{
			super();
	    	setStyle('fontWeight', 'bold');
	    	setStyle('textAlign', 'center');
		}
	}
}