package intranet.GestionProduit.renderers
{
	import flash.events.MouseEvent;
	
	import intranet.GestionProduit.DataSample;
	import intranet.GestionProduit.event.GTViewProduit_event;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.events.FlexEvent;

	public class ItemRendererChange extends VBox
	{
		private var img:Image=new Image();
		
        [Embed(source="../image/Document_2_Edit_2.png")]
        [Bindable]
        public var Logo:Class;
		
		public function ItemRendererChange()
		{
			super();
			img.source=Logo;
			img.width=20;
			img.height=20;
			this.addChild(img);
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		public function init(e:FlexEvent):void{
			this.addEventListener(MouseEvent.CLICK,Ev);
		}
		
		override public function set data(value:Object):void {
			if(value)
				super.data=value;
		}
		
		public function Ev(e:MouseEvent):void{
			dispatchEvent(new GTViewProduit_event(GTViewProduit_event.GTViewProduit_event,data.IDTHEME_PRODUIT,true,false,data.THEME));
		}
	}
}