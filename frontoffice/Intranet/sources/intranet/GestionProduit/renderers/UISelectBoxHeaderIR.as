package intranet.GestionProduit.renderers
{
	import flash.events.MouseEvent;

	public class UISelectBoxHeaderIR extends UIHeaderCheckBoxIR
	{
		protected var _lastState:Boolean = selected;
			
		public function UISelectBoxHeaderIR()
		{
			super();
		}
		
		override protected function clickHandler(event:MouseEvent):void
		{	
			
			super.clickHandler(event);
			
			if(_isAllSelected)
			{
				_collectionHelper.selectAll(false)
			}
			else if(_isAnySelected)
			{
				//regarder
				if(_collectionHelper.isAllIVisibleNSelected)				
				{	
					_collectionHelper.selectAll(false)
				}
				else
				{
					_collectionHelper.selectAll(true)	
				}
			}
			else
			{
				_collectionHelper.selectAll(true)
			}
			
			
		}
	}
}