package intranet.GestionProduit.renderers
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.listClasses.BaseListData;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.events.FlexEvent;
	
	import composants.util.CollectionViewInspector;
	
	public class UIHeaderCheckBoxIR extends CheckBox implements IListItemRenderer
	{
		protected var _collectionHelper:CollectionViewInspector;
		
		protected var _isAnySelected:Boolean =false;
		protected var _isAllSelected:Boolean =false;
		
		protected var _anySelectedIcon:Sprite = new Sprite();
		
		
		
		public function UIHeaderCheckBoxIR()
		{
			super();
			selected = false;
			_isAnySelected =false;
			_isAllSelected =false;
			
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteEvent);
		}
		
		protected function creationCompleteEvent(event:FlexEvent):void
		{
			useHandCursor = true;
			mouseChildren = true;
			drawAnySelectedIcon();
		}		
		
		
		override public function set data(value:Object):void
		{
			selected = false;
			if(value != null)
			{
				//super.data = value;
				_isAllSelected = _collectionHelper.isAllSelected;
				_isAnySelected = _collectionHelper.isAnySelected;
				commitProperties();
			}
		}
		
		
		override public function set listData(value:BaseListData):void
		{
			// TODO Auto Generated method stub
			if(value != null && value != listData)
			{
				super.listData = value;	
				_collectionHelper =(listData.owner as DataGrid).dataProvider as CollectionViewInspector;
			}
		}		
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			
			if(_isAllSelected)
			{
				 
				_anySelectedIcon.visible = false;
				selected = true;
			}
			else if(_isAnySelected)
			{
				selected = false; 
				_anySelectedIcon.visible = true;
			}
			else
			{
				selected = false;
				_anySelectedIcon.visible = false;
			}
		}
		
		protected function drawAnySelectedIcon(__xValue:Number = 11,__yValue:Number = 3):void
		{	
			_anySelectedIcon.graphics.lineStyle(1,0xffffff);
			_anySelectedIcon.graphics.beginFill(0x000000);
			_anySelectedIcon.graphics.drawRect(0,0,11,11);
			_anySelectedIcon.graphics.endFill();
			_anySelectedIcon.x = __xValue;
			_anySelectedIcon.y = __yValue;
			_anySelectedIcon.cacheAsBitmap =true;
		 
			if(!contains(_anySelectedIcon))
			{
				addChildAt(_anySelectedIcon,numChildren-1);	
			}
					
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			
			var _y:Number = (unscaledHeight - 11)/2 
			drawAnySelectedIcon(11,_y);
		}
	}
}