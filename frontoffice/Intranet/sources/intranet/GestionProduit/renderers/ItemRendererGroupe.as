package intranet.GestionProduit.renderers
{
	import intranet.GestionProduit.DataSample;
	import intranet.GestionProduit.event.ChangeData_EVENT;
	
	import mx.containers.HBox;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;

	public class ItemRendererGroupe extends HBox
	{
		public var cbo:ComboBox;
		
		public function ItemRendererGroupe()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		public function init(e:FlexEvent):void{
			cbo.addEventListener(ListEvent.CHANGE,changeData);
		}
		
		override public function set data(value:Object):void{
			var ok:Boolean=false;
			
			if(value){
				super.data=value;
			
				for(var i:int=0;i<DataSample.TabGroupe.length;i++){
					if(DataSample.TabGroupe[i].GROUPE == data.GROUPE){
						cbo.selectedIndex=i;
						ok=true;
						break;
					}
				}
				
				if(!ok){
					cbo.selectedIndex= -1;
				}
				
			}
		} 
				
		public function changeData(e:ListEvent):void{
			var item:Object= new Object();
			
			data.GROUPE= cbo.selectedItem.GROUPE;
			data.IDGROUPE_PRODUIT= cbo.selectedItem.IDGROUPE_PRODUIT;
			
			item.GROUPE=data.GROUPE;
			item.IDGROUPE_PRODUIT=data.IDGROUPE_PRODUIT;
			item.IDPRODUIT_CATALOGUE = data.IDPRODUIT_CATALOGUE;
			
			dispatchEvent(new ChangeData_EVENT(ChangeData_EVENT.ChangeData_EVENT,item,true,false));
		}
	}
}