package intranet.GestionProduit.renderers
{
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridListData;
	import mx.controls.listClasses.BaseListData;
	import mx.controls.listClasses.IListItemRenderer;
	
	import composants.util.CollectionViewInspector;
	
	public class UICheckBoxIR extends CheckBox implements IListItemRenderer
	{
		protected var _collectionHelper:CollectionViewInspector;
		
		public function UICheckBoxIR()
		{
			super();
			
		}
		
		override public function set data(value:Object):void
		{
			selected = false;// TODO Auto Generated method stub
			if(value != null)
			{
				super.data = value;
				
				if (listData && listData is DataGridListData && 
					DataGridListData(listData).dataField != null)
				{
					selected = data[DataGridListData(listData).dataField];
				}
			}
		}
		
		override public function set listData(value:BaseListData):void
		{	
			super.listData = value;
			if (listData && listData is DataGridListData)
			{
				_collectionHelper =(listData.owner as DataGrid).dataProvider as CollectionViewInspector;
			}
		}
		
	}
}