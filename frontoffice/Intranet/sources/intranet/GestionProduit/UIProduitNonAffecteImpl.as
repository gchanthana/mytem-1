package intranet.GestionProduit
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.describeType;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	
	import intranet.GestionProduit.entities.FiltreVo;
	import intranet.GestionProduit.entities.ProduitsVO;
	import intranet.GestionProduit.popups.PopUpAffectationMultiProduits;
	import intranet.GestionProduit.service.GestionProduitService;

	[Bindable]	
	public class UIProduitNonAffecteImpl extends VBox
	{	
		
		public var cbxActionsMulti:ComboBox;
		public var cbxTri:CBTriOpeIHM;
		public var btnRefresh:Button;
		public var btnSave:Button;
		public var btnActionsMulti:Button;
		
		public var txtFiltre:TextInputLabeled;
		
		private var _popUpAffectaionMultiProduit:PopUpAffectationMultiProduits;
		
		
		private var f:FiltreVo = new FiltreVo();
		
			
		
		public var listeActions:ArrayCollection = new ArrayCollection([
			{action:"-> Affecter les produits sélectionnés", idaction:0}
		]);
		
		
		public var produitService:GestionProduitService = new GestionProduitService();
		
		
		public function UIProduitNonAffecteImpl()
		{	
			addEventListener(FlexEvent.CREATION_COMPLETE,creationComplete);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		protected function creationComplete(ev:FlexEvent):void
		{
			addEventListener(FlexEvent.ADD,addHandler);
			addEventListener(FlexEvent.SHOW,showHandler);
			
			
			cbxActionsMulti.dataProvider = listeActions;
			
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			
			stage.focus = txtFiltre; 

			
			btnSave.addEventListener(MouseEvent.CLICK,btnSaveClickHandler);
			btnRefresh.addEventListener(MouseEvent.CLICK,btnRefreshClickHandler);
			btnActionsMulti.addEventListener(MouseEvent.CLICK,btnActionsMultiClickHandler);
			f.OPERATEURID = 0;
			updateListeProduit(f);
		}
		
		
		
		
		
		
		
		
		
		
		protected function btnRefreshClickHandler(event:MouseEvent):void
		{
			txtFiltre.text = "";
			updateListeProduit(f);
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		protected function btnSaveClickHandler(event:MouseEvent):void
		{	
			
			_printeSelection();
			
			
			var list:Array = produitService.
				model.
				listeProduits.
				updatedItems.source;
			var _ok:Boolean = checkIems(list);
			
			
			
			if(_ok)
			{
				produitService.updataPNA(list);				
			}
		}
		
		private function checkIems(listValue:Array):Boolean
		{	
			
			
			if(listValue.length < 0)
			{
				return false
			}
			
			for each(var item:Object in listValue)
			{
				if(item.BOOL_DRT!=0 && item.PLAN_MCAP==0) //si case datalert est cochée(autres champs sont donc enable) et que le champ [capacite] est vide
				{
					ConsoviewAlert.afficherAlertConfirmation("un champ capacité est vide, veuillez corriger","Attention",null);
					return false;
				}
			}
			
			return true
		}		
		
		
		
		
		
		
		
		
		
		
		
		private function _printeSelection():void
		{
			trace(ObjectUtil.toString(produitService.model.listeProduits.selectedItems));
		}	
		
		
		
		
		
		
		
		
		
		
		
		private function _printeChangedItems():void
		{
			trace(ObjectUtil.toString(produitService.model.listeProduits.updatedItems));
		}
		
		
		
		
		
		
		
		
		
		
		
		protected function cbTriChangeHandler():void
		{
			if(cbxTri.selectedItem != null)
			{
				f.OPERATEURID = cbxTri.selectedItem.OPERATEURID;	
			}
			else
			{
				f.OPERATEURID = 0;
			}
			updateListeProduit(f);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		protected function dgProduitItemClickHandler(le:ListEvent):void
		{	
			var name:String = describeType(le.itemRenderer).@name;
			
			if(name.search("intranet") != -1)
			{	
				le.preventDefault();
			}
			else
			{
				var item : * = le.itemRenderer.data;
				
				var _function:String = !(item.SELECTED) ? 'selectItem':'unSelectItem';
				
				produitService.model.listeProduits[_function](item);	
			}
			
			
		}
		
		
		
		protected function sortBySelected(obj1:Object, obj2:Object):int
		{
			return obj1["SELECTED"]>obj2["SELECTED"]?1:
				obj1["SELECTED"] < obj2["SELECTED"]?-1:
				0
		}
		
		
		
		
		
		
		
		
		
		
		
		protected function txtFiltreChangeHandler(event:Event):void
		{
			// TODO Auto-generated method stub
			produitService.model.listeProduits.filterFunction = filterFunction;
			produitService.model.listeProduits.refresh();
		}
		
		
		
		
		
		
		
		
		
		
		
		
		public function filterFunction(item:Object):Boolean{
			if(String(item.PRODUIT).match(new RegExp(txtFiltre.text,'i'))) return true;
			else return false;
		}
		
		
		
		
			
		
		public function btnActionsMultiClickHandler(e:Event):void
		{
			if(cbxActionsMulti.selectedItem != null)
			{	
				var produitsSelected:ArrayCollection = produitService.model.listeProduits.selectedItems;
				
				if(produitsSelected.length > 0)
				{
					switch(cbxActionsMulti.selectedItem.idaction)
					{
						case 0 : popUpMultiAffectation(produitsSelected);	break;
					}
				}
				else
				{
					Alert.show("Veuillez sélectionner au moins un produit", "Intranet");
				}
			}
			else
			{
				Alert.show("Veuillez sélectionner une action", "Intranet");
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		private function popUpMultiAffectation(produitsSelected:ArrayCollection):void
		{
			if(_popUpAffectaionMultiProduit != null)_popUpAffectaionMultiProduit = null;
			
			_popUpAffectaionMultiProduit = new PopUpAffectationMultiProduits();
			
			_popUpAffectaionMultiProduit.setInfosData((produitsSelected[0] as ProduitsVO).copyProduitsVO(), produitsSelected.source);
			_popUpAffectaionMultiProduit.addEventListener("MULTI_PRODUITS_AFFECTED", popUpMultiAffectationHandler);
			
			PopUpManager.addPopUp(_popUpAffectaionMultiProduit, this, true);
			PopUpManager.centerPopUp(_popUpAffectaionMultiProduit);
			
		}
		
		
		
		protected function addHandler(event:FlexEvent):void
		{
			stage.focus = txtFiltre;
		}	
		
		protected function showHandler(event:FlexEvent):void
		{
			stage.focus = txtFiltre;
		}
		
		
		
		
		
		
		
		private function popUpMultiAffectationHandler(e:Event):void
		{
			PopUpManager.removePopUp(_popUpAffectaionMultiProduit);
			updateListeProduit(f);
			Alert.show("Produits affectés", "Intranet");
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		private function updateListeProduit(f:FiltreVo):void
		{	
			produitService.getProduitNonAffecte(f);
			stage.focus = txtFiltre;
		}
		
		
		 
	}
}