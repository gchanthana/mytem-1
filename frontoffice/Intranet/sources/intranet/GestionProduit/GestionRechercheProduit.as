package intranet.GestionProduit
{
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.GroupingCollection;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class GestionRechercheProduit extends VBox
	{
		public var cbOpe:CBTriOpeIHM;
		public var cboTheme:ComboBox;
		public var ti:TextInput;
		public var btnRecherche:Button;	
		public var btnRecherche2:Button;
		public var btnRetour:Button;	
		public var adg1:AdvancedDataGrid;
		public var adg2:AdvancedDataGrid;
		public var hb11:HBox;
		public var hb12:HBox;
		public var hb21:HBox;
		public var hb22:HBox; 
		public var gc:GroupingCollection;
		
		[Bindable]
		public var TabInfoProduit:Object=new Object();
		
		public function GestionRechercheProduit()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}

// --------------------------------------------------**************************--------------------------------------------------

// INITIALISATION DES COMBOBOX, MISE EN PLACE DES ECOUTEURS.		
		public function init(e:FlexEvent):void{
			if(!DataSample.TabTheOk){
				var ope:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestionproduits.GestionProduits",
						"getListeTheme",
						yes,no);
				RemoteObjectUtil.callService(ope);
			}

			ti.addEventListener(FlexEvent.ENTER,onEnter);
			btnRecherche.addEventListener(MouseEvent.CLICK,onRecherche);
			adg1.addEventListener(ListEvent.ITEM_CLICK,onClick);	
			btnRetour.addEventListener(MouseEvent.CLICK,retour);
		}
		
		public function yes(e:ResultEvent):void{
			var item:Object= new Object();
			item.THEME= "** Tous **";
			item.IDTHEME_PRODUIT=0;
			
			DataSample.TabTheme= e.result as ArrayCollection;
			DataSample.TabTheme.addItemAt(item,0);
			DataSample.TabTheme.refresh();
		}
		
		public function no(e:FaultEvent):void{
			trace("GestionRechercheProduit - Init :"+e.message);
		}

// --------------------------------------------------**************************--------------------------------------------------

// TRAITEMENT DES EVENTS		
		public function onClick(e:ListEvent):void{
			TabInfoProduit=adg1.selectedItem;
			getInfoProduit();
			VisualEffect();
		}

		public function onEnter(e:FlexEvent):void{
			recherche();
		}
		
		public function onRecherche(e:MouseEvent):void{
			recherche();
			if(!adg1.visible){
				adg1.visible=true;
				adg1.height=589;
			}
		}
		
		public function retour(e:MouseEvent):void{
			hb11.height=hb21.height;
			hb12.height=hb22.height;
			hb21.height=0;
			hb22.height=0;
			adg1.height=adg2.height;
			adg2.height=0;
				
		}
		
// --------------------------------------------------**************************--------------------------------------------------

// FONCTION RECHERCHE		
		public function recherche():void{
			var OpeId:int=0;
			var themeId:int=0;
			var s:String="";
			
			if(cboTheme.selectedIndex > -1)
				themeId=cboTheme.selectedItem.IDTHEME_PRODUIT;
			
			if(cbOpe.selectedIndex > -1)
				OpeId=cbOpe.selectedItem.OPERATEURID;
							
			if(ti.text != "" && ti.text != "le produit à rechercher...")
				s=ti.text.toLowerCase();
 			
 			var tmpOpe:AbstractOperation=RemoteObjectUtil.getOperationFrom(
					RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestionproduits.GestionProduits",
					"SearchProduit",
					RechercheOk,RechercheNo);
			RemoteObjectUtil.callService(tmpOpe,s,OpeId,themeId); 				
		}	
		
		public function RechercheOk(e:ResultEvent):void{
			DataSample.TabRechercheProduit= e.result as ArrayCollection;
			DataSample.TabRechercheProduit.refresh();
		}
		
		public function RechercheNo(e:FaultEvent):void{
			trace('GestionRechercheProduit - Recherche() :\n'+e.message);
		}

// CHANGEMENT D'ETAT
	
		public function VisualEffect():void{
			hb21.height=hb11.height;
			hb22.height=hb11.height;
			hb11.height=0;
			hb12.height=0;
			adg2.height=adg1.height;
			adg1.height=0;
		}
		
// RECUPERATION DES INFORMATIONS DU PRODUIT
		public function getInfoProduit():void{
			DataSample.TabClientProduit.removeAll();
 			var ope:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestionproduits.GestionProduits",
					"displayProduit",
					infook,infono);
			RemoteObjectUtil.callService(ope,TabInfoProduit.IDPRODUIT_CATALOGUE); 
		}
	
		public function infook(e:ResultEvent):void{
			DataSample.TabClientProduit=e.result as ArrayCollection;
			DataSample.TabClientProduit.refresh();
			gc.refresh();
		}
		
		public function infono(e:FaultEvent):void{
			trace("GestionRechercheProduit - GetInfoProduit() :"+e.message);
		}
	}
}