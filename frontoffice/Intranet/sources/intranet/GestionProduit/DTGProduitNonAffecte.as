package intranet.GestionProduit
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import intranet.GestionProduit.entities.ProduitsVO;
	import intranet.GestionProduit.event.ChangeData_EVENT;
	import intranet.GestionProduit.popups.PopUpAffectationMultiProduits;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridItemRenderer;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;

	public class DTGProduitNonAffecte extends VBox
	{
		public var btnCheck:Button;
		public var btnRefresh:Button;
		public var adgP1:AdvancedDataGrid;
		public var CBtri:CBTriOpeIHM;	
		public var txtFiltre:TextInput;	
		
		public var btnActionsMulti	:Button;
		public var cbxActionsMulti	:ComboBox;
		
		public var cbDebugMode:CheckBox;
		public var adgcDebug:AdvancedDataGridColumn;
		
		[Bindable]public var nbrAdd		:int = 0;
		
		
		[Bindable]public var actionsMulti:ArrayCollection = new ArrayCollection([
																				{action:"-> Affecter les produits sélectionnés", idaction:0}
																			]);
		
		private var _popUpAffectaionMultiProduit:PopUpAffectationMultiProduits;
		
		private var tmpList:Array=new Array();
		
		private var tmp:AbstractOperation=new AbstractOperation();
			
		public function DTGProduitNonAffecte()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, CreationHandler);
		}

// DATATIP DU PRODUIT
		public function ProdDataTip(item:Object):String{
			var s:String="";
			
			if(item.hasOwnProperty("PRODUIT")){
				 s=item.CATTYPE+"\n -> "+item.PRODUIT;
			}
			return s;
		}		
		
		
// TRI PAR OPERATEUR - Event: TRI_EVENT perso lié au composant CBTriOpeIHM
		public function tri(e:ListEvent):void{
			var tmpOperation:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestionproduits.GestionProduits",
					"getProduitNonAffecte",
					getResult,getFault);
			RemoteObjectUtil.callService(tmpOperation,CBtri.selectedItem.OPERATEURID);
			DataSample.TabGestionProduit.refresh();
			tmpList=null;
		}
		
		public function getResult(e:ResultEvent):void
		{			
			for each (var item:Object in (e.result as ArrayCollection))
			{
				if(item.PLAN_MCAP > 0)
					item.PLAN_MCAP = item.PLAN_MCAP/1024;
			}
			DataSample.TabGestionProduit = ProduitsVO.formatProduitsVO(e.result as ArrayCollection);
			
			DataSample.TabGestionProduit.refresh();
			
			nbrAdd = 0;
			
			setUnselectedHeaderColumn(false);
		}
		
		public function getFault(e:FaultEvent):void{
			trace("DTGproduitnonaffecte - tri() :"+e.message);
		}

// REMPLISSAGE DE LA DATAGRID - SELECT DE LA BDD - Param 0 pour SELECT tout.
		public function CreationHandler(e:Event):void{
			var tmpOperation:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoprod.gestionproduits.GestionProduits",
							"getProduitNonAffecte",
							getPNAResultHandler,getPNAFaultHandler);
			RemoteObjectUtil.callService(tmpOperation,0);

			DataSample.TabGestionProduit.refresh();

			CBtri.addEventListener(ListEvent.CHANGE,tri);
			adgP1.addEventListener(ChangeData_EVENT.ChangeData_EVENT,modifData);
			adgP1.addEventListener('PRODUIT_STATUS_CHANGE', produitStatusChangeHandler);
			btnCheck.addEventListener(MouseEvent.CLICK,CheckAffectation);
			btnRefresh.addEventListener(MouseEvent.CLICK,refresh);
			
			btnActionsMulti.addEventListener(MouseEvent.CLICK, btnActionsMultiClickHandler);
			
			txtFiltre.addEventListener(Event.CHANGE,filtrer);
			txtFiltre.addEventListener(MouseEvent.ROLL_OVER,filtreReset);
		}
		
		public function getPNAResultHandler(e:ResultEvent):void
		{			
			for each (var item:Object in (e.result as ArrayCollection))
			{
				if(item.PLAN_MCAP > 0)
					item.PLAN_MCAP = item.PLAN_MCAP/1024;
			}
			
			DataSample.TabGestionProduit = ProduitsVO.formatProduitsVO(e.result as ArrayCollection);			
			DataSample.TabGestionProduit.refresh();
			
			nbrAdd = 0;
		
			setUnselectedHeaderColumn(false);
		}
		
		public function getPNAFaultHandler(e:FaultEvent):void{
			trace("DTGProduitNonAffecte - CreationHandler() :"+e.message);
		}

// FILTRE
		public function filtrer(e:Event):void{
			DataSample.TabGestionProduit.filterFunction= filtreProduit;
			DataSample.TabGestionProduit.refresh();
		}
		
		public function filtreProduit(item:Object):Boolean{
			if(String(item.PRODUIT).match(new RegExp(txtFiltre.text,'i'))) return true;
			else return false;
		}
				
		public function filtreReset(e:MouseEvent):void{
			txtFiltre.text="";
		}
		
// REFRESH
		public function refresh(e:MouseEvent=null):void{
			if(CBtri.selectedIndex != -1){
				var tmpOperation:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoprod.gestionproduits.GestionProduits",
							"getProduitNonAffecte",
							getPNAResultHandler,getPNAFaultHandler);
				RemoteObjectUtil.callService(tmpOperation,CBtri.selectedItem.OPERATEURID);	
			}else{
				var tmpOpe:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoprod.gestionproduits.GestionProduits",
							"getProduitNonAffecte",
							getPNAResultHandler,getPNAFaultHandler);
				RemoteObjectUtil.callService(tmpOpe,0);
			}
			txtFiltre.text="";
			DataSample.TabGestionProduit.refresh();
		}

		
// GESTION DES CHANGEMENTS DES DONNES 
		public function modifData(e:ChangeData_EVENT):void{
			var existe:Boolean=false;
			
			if(tmpList == null)
				tmpList=new Array();
			
			var item:Object = new Object();
			item = ObjectUtil.copy(e.item);

			if(tmpList.length>0){
				for(var i:int=0;i<tmpList.length;i++){
					if(tmpList[i].IDPRODUIT_CATALOGUE == item.IDPRODUIT_CATALOGUE){
						tmpList[i].BOOL_ACCES= item.BOOL_ACCES;
						tmpList[i].TOUT_ACCES= item.TOUT_ACCES;
						tmpList[i].IDTHEME_PRODUIT= item.IDTHEME_PRODUIT;
						tmpList[i].BOOL_DRT = item.BOOL_DRT;
						tmpList[i].PLAN_TYPE = item.PLAN_TYPE;
						tmpList[i].PLAN_MCAP = (item.PLAN_MCAP!=-1)?item.PLAN_MCAP*1024:item.PLAN_MCAP; // conversion en KiloOctets
						existe=true;
						break;
					}
				}
				
				if(!existe)
					tmpList.push(item);
				
			}else{
				tmpList.push(item);
			}
			adgP1.invalidateList();
		}
		
// AFFECTATION DES PRODUITS - UPDATE DE LA BDD		
		public function CheckAffectation(e:MouseEvent):void{
			var boolMsgAlert:Boolean = false;
			for each(var item:Object in (adgP1.dataProvider as ArrayCollection))
			{
				if(item.BOOL_DRT!=0 && item.PLAN_MCAP==0) //si case datalert est cochée(autres champs sont donc enable) et que le champ [capacite] est vide
				{
					boolMsgAlert = true;
					break;
				}
			}
			if(boolMsgAlert)
			{
				ConsoviewAlert.afficherAlertConfirmation("un champ capacité est vide, veuillez corriger","Attention",null);
			}
			else
			{
 				if(tmpList != null)
				{
	 				if(tmpList.length > 0)
					{
						var tmpListe:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoprod.gestionproduits.GestionProduits",
								"updPNA",
								updPNAResultHandler,updPNAFaultHandler);
						RemoteObjectUtil.callService(tmpListe,tmpList);				
					}
					txtFiltre.text="";
				}
			}
		}	
		
		public function updPNAResultHandler(e:ResultEvent):void{
 			for(var i:int=0;i<tmpList.length;i++){
				if( tmpList[i].IDTHEME_PRODUIT != null && tmpList[i].IDTHEME_PRODUIT != "" && tmpList[i].BOOL_ACCES >-1 && tmpList[i].TOUT_ACCES > -1){  
					for(var j:int=0;j<DataSample.TabGestionProduit.length;j++){
						if(DataSample.TabGestionProduit[j].IDPRODUIT_CATALOGUE == tmpList[i].IDPRODUIT_CATALOGUE){
							DataSample.TabGestionProduit.removeItemAt(j);
							break;
						}
					}
				}
			}
			this.tmpList=new Array();
			DataSample.TabGestionProduit.refresh(); 
			refresh()
		}
		
		public function updPNAFaultHandler(e:FaultEvent):void{
			trace("DTGProduitNonAffecte - CheckAffectation() :"+e.message);
		}
		
		protected function cbDebugModeClickHandler():void
		{
			if(cbDebugMode.selected)
			{
				
				adgcDebug.visible = true;
				
			}
			else
			{
				 
				adgcDebug.visible = false;
			}
			 
		}
		
		//---> RAJOUT 2012.04.05
		
		protected function adgP1ItemClickHandler(le:ListEvent):void
		{
			var itemRenderer	:Object = le.itemRenderer;
			var currentItem		:Object = itemRenderer.data;

			if(itemRenderer as AdvancedDataGridItemRenderer && currentItem != null)
			{
				if(currentItem.SELECTED)
					currentItem.SELECTED = false;
				else
					currentItem.SELECTED = true;
				
				DataSample.TabGestionProduit.itemUpdated(currentItem);
			}
		}
		
		public function headerCheckboxProduitClickHandler(status:Boolean):void
		{
			var lenProduits:int = DataSample.TabGestionProduit.length;
			
			nbrAdd = 0;
			
			for(var i:int = 0;i < lenProduits;i++)
			{
				DataSample.TabGestionProduit.source[i].SELECTED = status;
				
				if(DataSample.TabGestionProduit.source[i].SELECTED)
					nbrAdd++;

				DataSample.TabGestionProduit.itemUpdated(DataSample.TabGestionProduit[i]);
			}
			
			if(lenProduits == nbrAdd)
				setUnselectedHeaderColumn(true);
			else
				setUnselectedHeaderColumn(false);
		}
		
		public function produitStatusChangeHandler(e:Event):void
		{
			var lenProduits:int = DataSample.TabGestionProduit.length;
			
			nbrAdd = 0;
			
			for(var i:int = 0;i < lenProduits;i++)
			{
				if(DataSample.TabGestionProduit.source[i].SELECTED)
					nbrAdd++;
				
				DataSample.TabGestionProduit.itemUpdated(DataSample.TabGestionProduit[i]);
			}
			
			if(lenProduits == nbrAdd)
				setUnselectedHeaderColumn(true);
			else
				setUnselectedHeaderColumn(false);
		}
		
		public function btnActionsMultiClickHandler(e:Event):void
		{
			if(cbxActionsMulti.selectedItem != null)
			{
				var produitsSelected	:ArrayCollection = new ArrayCollection();
				var lenProduits			:int = DataSample.TabGestionProduit.length;
				
				for(var i:int = 0;i < lenProduits;i++)
				{
					if(DataSample.TabGestionProduit[i].SELECTED)
						produitsSelected.addItem(DataSample.TabGestionProduit[i]);
				}
				
				if(produitsSelected.length > 0)
				{
					switch(cbxActionsMulti.selectedItem.idaction)
					{
						case 0 : popUpMultiAffectation(produitsSelected);	break;
					}
				}
				else
				{
					Alert.show("Veuillez sélectionner au moins un produit", "Intranet");
				}
			}
			else
			{
				Alert.show("Veuillez sélectionner une action", "Intranet");
			}
		}
		
		private function popUpMultiAffectation(produitsSelected:ArrayCollection):void
		{
			_popUpAffectaionMultiProduit = new PopUpAffectationMultiProduits();
			
			_popUpAffectaionMultiProduit.setInfosData((produitsSelected[0] as ProduitsVO).copyProduitsVO(), produitsSelected.source);
			_popUpAffectaionMultiProduit.addEventListener("MULTI_PRODUITS_AFFECTED", popUpMultiAffectationHandler);
			
			PopUpManager.addPopUp(_popUpAffectaionMultiProduit, this, true);
			PopUpManager.centerPopUp(_popUpAffectaionMultiProduit);
			
		}
		
		private function popUpMultiAffectationHandler(e:Event):void
		{
			PopUpManager.removePopUp(_popUpAffectaionMultiProduit);
			
			refresh();
			
			Alert.show("Produits affectés", "Intranet");
		}
		
		private function setUnselectedHeaderColumn(isSelected:Boolean):void
		{
			if(adgP1.headerItems.length > 0)
			{
				var heads	:Array = adgP1.headerItems[0];
				var len		:int = heads.length;
				
				if(len > 0)
				{
					var currentObject:Object = heads[0];
					
					if(currentObject.hasOwnProperty('mycuschx'))
					{
						currentObject.mycuschx.headerCheckbox.selected = isSelected;
						
						currentObject.mycuschx.headerCheckbox.validateNow();
					}
				}
			}
		}
	}
}
	