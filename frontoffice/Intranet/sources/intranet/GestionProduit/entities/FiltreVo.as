package intranet.GestionProduit.entities
{
	import mx.collections.ArrayCollection;

	public class FiltreVo
	{
		public var OPERATEURID:Number = 0;
		
		public var produits:ArrayCollection = new ArrayCollection();
		
		public function FiltreVo()
		{
		}
	}
}