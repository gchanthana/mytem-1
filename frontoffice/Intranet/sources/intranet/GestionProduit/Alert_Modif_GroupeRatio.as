package intranet.GestionProduit
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class Alert_Modif_GroupeRatio extends TitleWindow
	{
		public var cboGroupe:ComboBox;
		public var ti:TextInput;
		public var btn:Button;
		
		public function Alert_Modif_GroupeRatio()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		public function init(e:FlexEvent):void{
			var item:Object=new Object();
			item.GROUPE="NOUVEAU ...";
			
			DataSample.TabGroupe.addItemAt(item,DataSample.TabGroupe.length);
			cboGroupe.addEventListener(ListEvent.CHANGE,onChange);
			this.addEventListener(CloseEvent.CLOSE,close);
		}
		
		public function close(e:CloseEvent):void{
			DataSample.TabGroupe.removeItemAt(DataSample.TabGroupe.length-1);
		}
		
		public function onChange(e:ListEvent):void{
			ti.text="Entrez le nom du nouveau groupe";
		}
		
		public function ValidGroupe():void{
		// MODIFICATION D'UN GROUPE
			if(cboGroupe.selectedItem.GROUPE == "NOUVEAU ..."){
				if(ti.text!= ""){
					Alert.show("Etes vous sur de vouloir créer le groupe '"+ti.text+"' ?",
					"Confirmation de création",Alert.OK | Alert.CANCEL,this,CreateClickHandler);					
				}else{
					Alert.show("Entrez un nom de groupe.");
				}
			}
		// CREATION D'UN GROUPE			
			if(cboGroupe.selectedItem.GROUPE != "NOUVEAU ..." && cboGroupe.selectedItem.GROUPE != ""){
				if(ti.text!= ""){			
					Alert.show("Etes vous sur de vouloir modifier '"+cboGroupe.selectedItem.GROUPE+"' en '"+ti.text+"' ?",
					"Confirmation de modification",Alert.OK | Alert.CANCEL,this,ModifClickHandler);
				}else{
					Alert.show("Entrez un nom de groupe.");
				}
			}
		}
		
		public function CreateClickHandler(e:CloseEvent):void{
 			if(e.detail == Alert.OK){
				var tmp:AbstractOperation=RemoteObjectUtil.getOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestionproduits.GestionProduits",
						"majGroupe",
						createyes,createno);
				RemoteObjectUtil.callService(tmp,ti.text);				
			} 
		}
		
		public function createyes(e:ResultEvent):void{
			var tmp:AbstractOperation=RemoteObjectUtil.getOperationFrom(
					RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestionproduits.GestionProduits",
					"getListeGroupe",
					ok,not);
			RemoteObjectUtil.callService(tmp);
		}
		
		public function ok(e:ResultEvent):void{
			DataSample.TabGroupe= e.result as ArrayCollection;
			DataSample.TabGroupe.refresh();	
		}
		
		public function not(e:FaultEvent):void{
			trace(e.message);
		}
		
		public function createno(e:FaultEvent):void{
			trace("Alert_Modif_GroupeRatio -> CreateHandler:"+e.message);
		}
		
		public function ModifClickHandler(e:CloseEvent):void{
 			if(e.detail == Alert.OK){
				var tmp:AbstractOperation=RemoteObjectUtil.getOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestionproduits.GestionProduits",
						"majGroupe",
						modifyes,modifno);
				RemoteObjectUtil.callService(tmp,ti.text,cboGroupe.selectedItem.IDGROUPE_PRODUIT);
			} 
		}
		
		public function modifyes(e:ResultEvent):void{
			var tmp:AbstractOperation=RemoteObjectUtil.getOperationFrom(
					RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestionproduits.GestionProduits",
					"getListeGroupe",
					ok,not);
			RemoteObjectUtil.callService(tmp);
		}
		
		public function modifno(e:FaultEvent):void{
			trace("Alert_Modif_GroupeRatio -> ModifHandler:"+e.message);
		}
		
		public function resetTxt():void{
			ti.text="";
		}
	}
}