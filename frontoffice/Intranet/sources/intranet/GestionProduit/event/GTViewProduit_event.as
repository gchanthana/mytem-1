package intranet.GestionProduit.event
{
	import flash.events.Event;
	
	public class GTViewProduit_event extends Event
	{
		public var id:int;
		public var str:String;
		public static const GTViewProduit_event:String= "GTViewProduit_event";
		
		public function GTViewProduit_event(type:String,id:int,bubbles:Boolean=false, cancelable:Boolean=false,str:String="")
		{
			super(type, bubbles, cancelable);
			this.str=str;
			this.id=id;
		}
		
	}
}