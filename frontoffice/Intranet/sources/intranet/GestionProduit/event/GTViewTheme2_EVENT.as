package intranet.GestionProduit.event
{
	import flash.events.Event;
	
	public class GTViewTheme2_EVENT extends Event
	{
		public var id:int;
		public static const GTViewTheme2_EVENT:String= "GTViewTheme2_EVENT";
		
		public function GTViewTheme2_EVENT(type:String,id:int,bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.id=id;
		}
		
	}
}