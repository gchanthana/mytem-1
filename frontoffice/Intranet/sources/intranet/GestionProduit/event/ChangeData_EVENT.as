package intranet.GestionProduit.event
{
	import flash.events.Event;

	public class ChangeData_EVENT extends Event
	{
		public var item:Object;
		public static const ChangeData_EVENT:String="ChangeData_EVENT";
		
		public function ChangeData_EVENT(type:String, item:Object, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.item=item;
		}
		
	}
}