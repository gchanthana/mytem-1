package intranet.gestionLoginClasse.event
{
    import flash.events.Event;

    public class UserEvent extends Event
    {
        //--------------------------------------------------------------------------------------------//
        //					VARIABLES STATIC - EVENT NAME
        //--------------------------------------------------------------------------------------------//
        public static const USER_DATA_LOADED_SUCCESS:String = "user_data_loaded_success";
        public static const USER_DATA_LOADED_ERROR:String = "user_data_loaded_error";
        public static const USER_NOT_FOUND:String = "user_not_found";
        public static const PASSWORD_EMAILED:String = "password_emailed";
        public static const DUPLICATE_PROFILE_SUCCESS:String = "duplicate_profile_success";
        public static const DUPLICATE_PROFILE_ERROR:String = "duplicate_profile_error";
        public static const AFFECT_STYLE_ERROR:String = "affect_style_error";
        public static const AFFECT_STYLE_SUCCESS:String = "affect_style_success";
        public static const AFFECT_ACCESS_SUCCESS:String = "affect_access_success";
        public static const AFFECT_ACCESS_ERROR:String = "affect_access_error";
        public static const USER_ACCESS_LOAD_SUCCESS:String = "user_access_load_success";
        public static const USER_ACCESS_LOAD_ERROR:String = "user_access_load_error";
        //--------------------------------------------------------------------------------------------//
        //					VARIABLES PASSEES EN ARGUMENTS
        //--------------------------------------------------------------------------------------------//		
        public var obj:Object = null;

        //--------------------------------------------------------------------------------------------//
        //					CONSTRUCTEUR
        //--------------------------------------------------------------------------------------------//		
        public function UserEvent(type:String, objet:Object = null, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
            this.obj = objet;
        }

        //--------------------------------------------------------------------------------------------//
        //					METHODES PUBLIC OVERRIDE - COPY OF EVENT
        //--------------------------------------------------------------------------------------------//
        override public function clone():Event
        {
            return new UserEvent(type, obj, bubbles, cancelable);
        }
    }
}