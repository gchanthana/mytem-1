package intranet.gestionLoginClasse.services.useraccountstate
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import intranet.gestionLoginClasse.event.UserAccountStateEvent;
	
	public class UserAccountStateModel extends EventDispatcher
	{
		[Bindable]public var newState:Number = 0;
	 
		public function UserAccountStateModel(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		internal function updateUserAccountState(value:Number):void
		{
			if(value >= 0)
			{
				newState = value;
				dispatchEvent(new UserAccountStateEvent(UserAccountStateEvent.CHANGE_STATE_SUCCED));
			}
			else
			{
				dispatchEvent(new UserAccountStateEvent(UserAccountStateEvent.CHANGE_STATE_FAILED));
			}
		}
	}
}