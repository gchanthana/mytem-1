package intranet.gestionLoginClasse.services.useraccountstate
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class UserAccountService
	{
		public static const ACTIVE:Number = 1;
		public static const INACTIVE:Number = 2;
		public static const EXPIRED:Number = 0;
		
		private var _handler:UserAccountStateHandler;		
		private var _model:UserAccountStateModel;
		
		public function UserAccountService()
		{
			_model = new UserAccountStateModel();
			_handler = new UserAccountStateHandler(_model);
		}
		
		
		/**
		 * Changer l'état du login Activer/Désactiver/Expiré (plus géré)
		 * @param value le nouvel état du login
		 * 
		 */		
		public function updateUserAccountState(idLogin:Number,newState:Number):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.M21.UserAccoutStateService", 
				"updateUserAccountState", 
				_handler.updateUserAccountStateHandler, null);
			
			RemoteObjectUtil.callService(op,idLogin,newState);
		}
	
	 
		public function get model():UserAccountStateModel
		{
			return _model;
		}
		
		
	}
}