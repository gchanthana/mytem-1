package intranet.gestionLoginClasse
{
	public class GestionLogin_Organisation
	{
		private var idOrga : int;
		private var libelleOrga : String;
		private var adrOrga : String;
		
		function GestionLogin_Organisation(idOrga:int,libelle :String ,adrOrga : String ){
			this.adrOrga=adrOrga;
			this.libelleOrga=libelle;
			this.idOrga = idOrga;
		}
		public function getIdOrga():int{
			return idOrga;
		}
		public function getLibelleOrga():String{
			return libelleOrga;
		}
		public function getAdrOrga():String{
			return adrOrga;
		}
		
	}
}