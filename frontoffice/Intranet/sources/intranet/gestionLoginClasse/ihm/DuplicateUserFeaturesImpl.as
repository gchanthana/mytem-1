package intranet.gestionLoginClasse.ihm
{
    import composants.util.ConsoviewAlert;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import fr.consotel.consoview.util.ConsoViewErrorManager;
    import intranet.GestionDesLogin.services.Service;
    import intranet.gestionLoginClasse.entity.AccessVO;
    import intranet.gestionLoginClasse.entity.StyleVO;
    import intranet.gestionLoginClasse.entity.UserVO;
    import intranet.gestionLoginClasse.event.PaginateListEvent;
    import intranet.gestionLoginClasse.event.UserEvent;
    import intranet.gestionLoginClasse.services.pagination.ListServices;
    import intranet.gestionLoginClasse.services.user.UserServices;
    import mx.collections.ArrayCollection;
    import mx.containers.HBox;
    import mx.containers.TitleWindow;
    import mx.containers.VBox;
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.CheckBox;
    import mx.controls.DataGrid;
    import mx.controls.Label;
    import mx.events.CloseEvent;
    import mx.events.CollectionEvent;
    import mx.events.DragEvent;
    import mx.events.DropdownEvent;
    import mx.events.FlexEvent;
    import mx.formatters.SwitchSymbolFormatter;
    import mx.managers.DragManager;
    import mx.managers.PopUpManager;
    import mx.utils.ObjectUtil;
    import paginatedatagrid.PaginateDatagrid;
    import paginatedatagrid.PaginateDatagridControleur;
    import paginatedatagrid.columns.event.StandardColonneEvent;
    import paginatedatagrid.columns.stantardcolumns.SelectPaginateDatagridColumn;
    import paginatedatagrid.event.PaginateDatagridEvent;
    import paginatedatagrid.pagination.Pagination;
    import paginatedatagrid.pagination.event.PaginationEvent;
    import paginatedatagrid.pagination.vo.ItemIntervalleVO;
    import searchpaginatedatagrid.ParametresRechercheVO;
    import searchpaginatedatagrid.SearchPaginateDatagrid;
    import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;

    /**
     * Classe d'implémentation de l'interface de gestion de duplication
     * des styles et accès d'un ensemble d'utilisateur à partir d'un
     * utilisateur référent
     */
    [Bindable]
    public class DuplicateUserFeaturesImpl extends TitleWindow
    {
        // Utilisateur référents pour la duplication
        private var _refUser:UserVO;
        private var _refStyle:StyleVO;
        // le groupe de référence de l'utilisateur
        private var _refGroup:Object;
        // liste de tous les stlyes
        private var _styleList:ArrayCollection;
        // liste de tous les utilisateurs appartenant aux meme groupe que l'utilisateur référents
        public var usersCollec:ArrayCollection = new ArrayCollection();
        // Déclarations des composants mxml  
        public var btTransferUser:Button;
        public var btRemoveUser:Button;
        public var btAnnuler:Button;
        public var btValider:Button;
        public var users_dg:PaginateDatagrid;
        public var usersToAffect_dg:DataGrid;
        public var mySearchPaginateDatagrid:SearchPaginateDatagrid;
        public var accessStyleDetailsBox:HBox;
        // Fiche utilisateur
        public var userName_lb:Label;
        public var userSurname_lb:Label;
        public var userMail_lb:Label;
        public var userGroup_lb:Label;
        // Accès
        public var moduelBase_lb:Label;
        public var workflow_lb:Label;
        public var usage_lb:Label;
        public var gestionOrga_lb:Label;
        public var parcFixe_lb:Label;
        public var parcMobile_lb:Label;
        public var gestionLogin_lb:Label;
        // Style
        public var libelStyle_lb:Label;
        public var commentStyle_lb:Label;
        public var idStyle_lb:Label;
        public var codeStyle_lb:Label;
        // les services distants
        private var userServices:UserServices;
        private var listServices:ListServices;
        // la liste des utilisateur sélectionnés
        private var _selectedUsers:ArrayCollection;
        private var _indexPaginateStart:int;
        public var colSelection:SelectPaginateDatagridColumn;
        public var us:ArrayCollection = new ArrayCollection();
        // controleur de la pagination
        private var _paginateControleur:PaginateDatagridControleur;
        private var _inter:ItemIntervalleVO;
        private var _nextIndex:int;
        public var nbTotalElement:int;
        private var firstLoading:Boolean = true;
        // variable qui indique si on duplique tous les accès ou que celui du groupe séléctionné
        private var _duplicateAll:Boolean = false;

        public function DuplicateUserFeaturesImpl()
        {
            super();
            this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
        }

        /**********************************************************************************************
         *		                         INITIALISATION
         *********************************************************************************************/ /**
         * Initialisation du composant
         * @param evt
         */
        private function init(evt:FlexEvent):void
        {
            try
            {
                styleList = new ArrayCollection();
                selectedUsers = new ArrayCollection();
                listServices = new ListServices();
                usersToAffect_dg.dataProvider = new ArrayCollection();
                initListeners();
            }
            catch (e:Error)
            {
                trace("DuplicateUserFeaturesImpl-init() Problème d'initialisation du composant : " + e);
            }
        }

        /**
         * Initialisation des écouteurs
         */
        private function initListeners():void
        {
            btValider.addEventListener(MouseEvent.CLICK, onValidateHandler);
            btAnnuler.addEventListener(MouseEvent.CLICK, onCancelHandler);
            btTransferUser.addEventListener(MouseEvent.CLICK, onBtnTransferUserClickHandler);
            btRemoveUser.addEventListener(MouseEvent.CLICK, onBtnRemoveUserClickHandler);
            this.addEventListener(CloseEvent.CLOSE, onCancelHandler);
            usersToAffect_dg.addEventListener(DragEvent.DRAG_DROP, onDragOverDestinationHandler);
            usersToAffect_dg.addEventListener(DragEvent.DRAG_EXIT, onDragExitDestinationHandler);
            configurePaginateDatagrid();
        }

        /**
         * Initialisation des composants d'affichage
         */
        private function initDisplay():void
        {
            // remplissage des champs utilisateur référent
            if (refUser)
            {
                userName_lb.text = refUser.login_nom;
                userSurname_lb.text = refUser.login_prenom;
                userMail_lb.text = refUser.login_email;
            }
            else
            {
                userName_lb.text = "";
                userSurname_lb.text = "";
                userMail_lb.text = "";
            }
        }

        /**
         * Initialisation des composants d'affichage de
         * la fiche listant les types d'accès et de style
         */
        private function initAccessDisplay():void
        {
            if (!duplicateAll)
            {
                // remplissage des champs 
                if (refUser.access)
                {
                    var a:AccessVO = refUser.access;
                    moduelBase_lb.text = AccessVO.getLibell(a.module_facturation);
                    workflow_lb.text = AccessVO.getLibell(a.module_workflow);
                    usage_lb.text = AccessVO.getLibell(a.module_usage);
                    gestionOrga_lb.text = AccessVO.getLibell(a.module_gestion_org);
                    parcFixe_lb.text = AccessVO.getLibell(a.module_fixe_data);
                    parcMobile_lb.text = AccessVO.getLibell(a.module_mobile);
                    gestionLogin_lb.text = AccessVO.getLibell(a.module_gestion_login);
                    userGroup_lb.text = refGroup.LIBELLE_GROUPE_CLIENT;
                    codeStyle_lb.text = a.idconso_style;
                }
                else if (refUser.style)
                {
                    var s:StyleVO = refUser.style;
                    libelStyle_lb.text = s.libelle_style;
                    commentStyle_lb.text = s.commentaire;
                    idStyle_lb.text = s.idconso_style;
                    codeStyle_lb.text = s.code_style;
                }
            }
        }

        /**
         * Initialisation des composants d'affichage de
         * la fiche listant le stlye de l'utilisateur référent
         */
        private function initStlyeDisplay():void
        {
            if (!duplicateAll)
            {
                if (refUser.style)
                {
                    var s:StyleVO = refUser.style;
                    libelStyle_lb.text = s.libelle_style;
                    commentStyle_lb.text = s.commentaire;
                    idStyle_lb.text = s.idconso_style;
                    codeStyle_lb.text = s.code_style;
                }
            }
        }

        /**********************************************************************************************
         *		                         PAGINATE DATAGRID HANDLERS
         *********************************************************************************************/ /**
         *  Paramétrages du Datagrid Paginé listant l'ensemble
         *  des utilisateurs
         *
         * */
        private function configurePaginateDatagrid():void
        {
            // le module de recherche dans le datagrid paginée
            mySearchPaginateDatagrid.addEventListener(SearchPaginateDatagridEvent.SEARCH, onSearchHandler);
            mySearchPaginateDatagrid.comboRecherche.selectedIndex = 2;
            mySearchPaginateDatagrid.comboTrieColonne.selectedIndex = 2;
            users_dg.nbTotalItem = 200;
            users_dg.addEventListener(PaginateDatagridEvent.PAGE_CHANGE, onIntervalleChangeHandler);
            // Chargement de la liste de tous les utilisateurs
            getLoginListServices(mySearchPaginateDatagrid.parametresRechercheVO);
        }

        /**
         * Appel du service de récupération de la séquences de pages au clic
         * sur les flêches de navigation
         * @param evt
         */
        public function onIntervalleChangeHandler(evt:PaginateDatagridEvent):void
        {
            getLoginListServices(mySearchPaginateDatagrid.parametresRechercheVO, users_dg.currentIntervalle);
        }

        /**
         * Appel du service au click sur le bouton "rechercher"
         * @param evt
         */
        public function onSearchHandler(evt:SearchPaginateDatagridEvent):void
        {
            getLoginListServices(mySearchPaginateDatagrid.parametresRechercheVO);
        }

        /**
         * Appel du sercive gérant la récupération de la liste paginé des logins
         * en fonction des paramètres spécifiés et gérés en interne du composant PaginateDG
         * @param parametresRechercheVO : les paramètres de recherches
         * @param itemIntervalleVO : les paramètres de pagination
         */
        private function getLoginListServices(parametresRechercheVO:ParametresRechercheVO, itemIntervalleVO:ItemIntervalleVO = null):void
        {
            if (!itemIntervalleVO) // Si c'est une nouvelle recherche
            {
                users_dg.refreshPaginateDatagrid();
                itemIntervalleVO = new ItemIntervalleVO();
                itemIntervalleVO.indexDepart = 0;
                itemIntervalleVO.tailleIntervalle = 5;
            }
            listServices = new ListServices();
            listServices.myHandlers.addEventListener(PaginateListEvent.LOGIN_LIST_LOADED_SUCCESS, onListServiceResultHandler);
            listServices.myHandlers.addEventListener(PaginateListEvent.LOGIN_LIST_LOADED_ERROR, onListServiceResultHandler);
            listServices.getListeLogins(parametresRechercheVO, itemIntervalleVO, -1, firstLoading);
            firstLoading = false;
        }

        /**
         * Assure la non duplication d'un item dans le datagrid de destination
         * @param evt
         */
        private function onDragOverDestinationHandler(evt:DragEvent):void
        {
            var item:Object = DragEvent(evt).dragSource.dataForFormat("items")[0];
            if (evt.dragInitiator != evt.currentTarget)
            {
                removeItemIfExists(evt.currentTarget as DataGrid, item);
            }
        }

        /**
         * Supprime un item du datagrid par simple drag à l'extérieur
         * @param evt
         */
        private function onDragExitDestinationHandler(evt:DragEvent):void
        {
            // 
            var item:Object = DragEvent(evt).dragSource.dataForFormat("items")[0];
            var index:int = DataGrid(evt.currentTarget).selectedIndex;
            var collec:ArrayCollection = DataGrid(evt.currentTarget).dataProvider as ArrayCollection;
            if (index != -1)
            {
                collec.removeItemAt(index);
            }
        }

        /**********************************************************************************************
         *		                          USER ACTION HANDLERS
         *********************************************************************************************/ /**
         * Déclenche la dupliction des caractéristiques
         * des utilisateurs sélectionnés dans le datagrid
         * ( envoi vers la couche metier pour traitement via web services )
         * @param evt
         * @throws (e)
         */
        private function onValidateHandler(evt:MouseEvent):void
        {
            try
            {
                if (usersToAffect_dg.selectedIndex - 1)
                {
                    userServices = new UserServices();
                    userServices.myHandlers.addEventListener(UserEvent.DUPLICATE_PROFILE_SUCCESS, onServiceResultHandler);
                    userServices.myHandlers.addEventListener(UserEvent.DUPLICATE_PROFILE_ERROR, onServiceResultHandler);
                    var collec:ArrayCollection = usersToAffect_dg.dataProvider as ArrayCollection;
                    var l:int = collec.length;
                    var userRefId:String = refUser.app_login;
                    var id:String;
                    for (var i:int = 0; i < l; i++)
                    {
                        id = collec.getItemAt(i).APP_LOGINID;
                        if (!duplicateAll)
                        {
                            userServices.duplicateUserProfile(Number(userRefId), Number(id), Number(refGroup.IDGROUPE_CLIENT));
                        }
                        else
                        {
                            userServices.duplicateAllUserProfile(Number(userRefId), Number(id));
                        }
                            //userServices.duplicateUserProfile(userRefId, id, 0);
                    }
                }
            }
            catch (e:Error)
            {
                throw new Error("DuplicateUserFeaturesImpl-onValidateHandler() # une erreur est survenue : impossible de dupliquer les styles et accès : " + e);
            }
        }

        /**
         * Click sur le BOuton Annuler
         * @param evt
         */
        private function onCancelHandler(evt:Event):void
        {
            PopUpManager.removePopUp(this);
        }

        /**
         * Au clic sur le bouton de transfer , les items sélectionnées
         * dans la liste de gauche sont ajoutés/supprimés de la liste de droite
         * (sans duplication)
         * @param evt
         */
        private function onBtnTransferUserClickHandler(evt:Event):void
        {
            selectedUsers = users_dg.selectedItems;
            var collec:ArrayCollection = usersToAffect_dg.dataProvider as ArrayCollection;
            for (var i:int = 0; i < selectedUsers.length; i++)
            {
                var item:Object = selectedUsers.getItemAt(i);
                if (!itemExists(usersToAffect_dg, item))
                {
                    collec.addItem(item);
                }
            }
        }

        /**********************************************************************************************
         *		                         WEBSERVICES RESULT HANDLERS
         *********************************************************************************************/ /**
         *  Handler gérant le retour des appels distants concernant
         *  l'objet Utilisateur ( récupération accès et style , duplication...)
         *  @param evt
         */
        private function onServiceResultHandler(evt:UserEvent):void
        {
            try
            {
                switch (evt.type)
                {
                    // Cas : Erreur dans la duplication des profils
                    case UserEvent.DUPLICATE_PROFILE_ERROR:
                        Alert.show("Erreur dans la duplication du profil d'un utilisateur", "Erreur de duplication");
                        break;
                    // Cas : Duplication du profil réussie
                    case UserEvent.DUPLICATE_PROFILE_SUCCESS:
                        PopUpManager.removePopUp(this);
                        Alert.show("Les Attributs styles et accès ont été affectés avec succès", "Duplication des Accès");
                        break;
                    // Cas : Chargement des accès de l'utilisateur référent réussie
                    case UserEvent.USER_ACCESS_LOAD_SUCCESS:
                        refUser.access = userServices.myDatas.userVO.access;
                        initAccessDisplay();
                        refStyle = getStyleVO(refUser.access.idconso_style, styleList);
                        userServices.myHandlers.removeEventListener(UserEvent.USER_ACCESS_LOAD_SUCCESS, onServiceResultHandler);
                        userServices.myHandlers.removeEventListener(UserEvent.USER_DATA_LOADED_ERROR, onServiceResultHandler);
                        break;
                    // Cas : Chargement des accès de l'utilisateur référent échoué
                    case UserEvent.USER_ACCESS_LOAD_ERROR:
                        userServices.myHandlers.removeEventListener(UserEvent.USER_ACCESS_LOAD_SUCCESS, onServiceResultHandler);
                        userServices.myHandlers.removeEventListener(UserEvent.USER_DATA_LOADED_ERROR, onServiceResultHandler);
                        break;
                    default:
                        break;
                }
                removeServicesListeners();
            }
            catch (e:Error)
            {
                throw new Error(" ### DuplicateUserFeaturesImpl - onServiceResultHandler() evt: " + evt.type + " Erreur : " + e);
            }
        }

        /**
         *  Handler gérant le retour des appels distants concernant
         *  les listes de logins alimentant le composant Datagrid paginé
         *  @param evt
         */
        private function onListServiceResultHandler(evt:PaginateListEvent):void
        {
            switch (evt.type)
            {
                // Cas : Erreur dans la récupération de la liste de logins
                case PaginateListEvent.LOGIN_LIST_LOADED_ERROR:
                    Alert.show("Erreur dans le chargement de la liste d'utilisateurs", "Erreur de chargement");
                    break;
                // Cas : Récupération de la liste réussie
                case PaginateListEvent.LOGIN_LIST_LOADED_SUCCESS:
                    // traitement pour la colonne de selection	
                    users_dg.refreshPaginateDatagrid();
                    usersCollec = addSelectAttrTo(listServices.myDatas.listLogin, false);
                    usersCollec.refresh();
                    if (usersCollec.length > 0)
                    {
                        nbTotalElement = usersCollec.getItemAt(0).NBRECORD;
                            // TODO à retirer si instanciation via vo (additem)
                    }
                    else
                    {
                        nbTotalElement = 0;
                    }
                    // users_dg.dataChangeHandler();
                    break;
                default:
                    break;
            }
        }

        /**
         * Instanciation d'un nouvel UserVO après chargement de
         * ses caractéristiques
         * @param evt
         */
        private function onUserRefLoadedHandler(evt:UserEvent):void
        {
            switch (evt.type)
            {
                case UserEvent.USER_DATA_LOADED_SUCCESS:
                    refUser = new UserVO();
                    break;
                case UserEvent.USER_DATA_LOADED_ERROR:
                    break;
                default:
                    break;
            }
        }

        /**********************************************************************************************
         *		                             UTILS
         *********************************************************************************************/ /**
         *  Rajoute une propriété SELECTED à une collection
         *  d'objets en vue d'intégration à un datagrid avec options
         *  de selection
         * */
        public function addSelectAttrTo(p_collec:ArrayCollection, p_bool:Boolean):ArrayCollection
        {
            for (var i:int = 0; i < p_collec.length; i++)
            {
                p_collec.getItemAt(i).SELECTED = p_bool;
            }
            return p_collec;
        }

        public function initServicesListeners():void
        {
            userServices.myHandlers.addEventListener(UserEvent.DUPLICATE_PROFILE_SUCCESS, onServiceResultHandler);
            userServices.myHandlers.addEventListener(UserEvent.DUPLICATE_PROFILE_ERROR, onServiceResultHandler);
        }

        /**
         *
         * Récupération et construction d'un VO Style à partir
         * de l'id et d'une liste de style
         * @param p_idConsoStyle
         * @param p_listStyle
         * @return
         */
        private function getStyleVO(p_idConsoStyle:String, p_listStyle:ArrayCollection):StyleVO
        {
            var style:StyleVO = new StyleVO();
            var collec:ArrayCollection = p_listStyle as ArrayCollection;
            for (var i:int = 0; i < collec.length; i++)
            {
                var item:Object = collec.getItemAt(i);
                if (item.IDCONSO_STYLE == p_idConsoStyle)
                {
                    style.deserialize(item);
                    break;
                }
            }
            return style;
        }

        /**
         * Supprime l'utilisateur sélectionné de la liste de droite
         * @param evt
         */
        private function onBtnRemoveUserClickHandler(evt:Event):void
        {
            if (usersToAffect_dg.selectedIndex != -1)
            {
                var collec:ArrayCollection = usersToAffect_dg.dataProvider as ArrayCollection;
                collec.removeItemAt(usersToAffect_dg.selectedIndex);
            }
        }

        /**
         *
         * @param data
         */
        private function removeUserFromSelectedList(data:Object):void
        {
            for (var i:int = 0; i < selectedUsers.length; i++)
            {
                if (selectedUsers.getItemAt(i).APP_LOGINID == data.APP_LOGINID)
                {
                    selectedUsers.removeItemAt(i);
                    break;
                }
            }
        }

        /**
         * Si l'item existe dans le datagrid , il est supprimé
         * (utile pour la non duplication d'un item via drag drop )
         * @param datagrid
         * @param user
         * @return
         */
        private function removeItemIfExists(datagrid:DataGrid, user:Object):Boolean
        {
            // 
            var collec:ArrayCollection = datagrid.dataProvider as ArrayCollection;
            if (collec && user)
            {
                var length:int = collec.length;
                for (var i:int = 0; i < length; i++)
                {
                    if (user.APP_LOGINID == collec.getItemAt(i).APP_LOGINID)
                    {
                        ArrayCollection(usersToAffect_dg.dataProvider).removeItemAt(i);
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * si l'item existe dans le datagrid , il est supprimé
         * (utile pour la non duplication d'un item via drag drop )
         * @param datagrid
         * @param user
         * @return
         */
        private function itemExists(datagrid:DataGrid, user:Object):Boolean
        {
            var exists:Boolean = false;
            var collec:ArrayCollection = datagrid.dataProvider as ArrayCollection;
            if (collec && user)
            {
                var length:int = collec.length;
                if (length > 0 && user)
                {
                    for (var i:int = 0; i < length; i++)
                    {
                        if (user.APP_LOGINID == collec.getItemAt(i).APP_LOGINID)
                        {
                            exists = true;
                            break;
                        }
                    }
                }
            }
            return exists;
        }

        /**
         * Suppime l'utilisateur référent de la liste
         * @param datagrid
         * @param app_loginid de l'user
         * @return
         */
        private function removeRefUser(collec:ArrayCollection, userId:String):void
        {
            if (collec && userId)
            {
                for (var i:int = 0; i < usersCollec.length; i++)
                {
                    if (userId == usersCollec.getItemAt(i).APP_LOGINID)
                    {
                        usersCollec.removeItemAt(i);
                    }
                }
            }
        }

        /**
         * Suppime l'utilisateur référent de la liste
         * @param datagrid
         * @param app_loginid de l'user
         * @return
         */
        private function removeRefU(collec:ArrayCollection):ArrayCollection
        {
            var users:ArrayCollection = new ArrayCollection();
            if (collec)
            {
                for (var i:int = 0; i < collec.length; i++)
                {
                    if (refUser.app_login != collec.getItemAt(i).APP_LOGINID)
                    {
                        users.addItem(collec.getItemAt(i));
                    }
                }
            }
            return users;
        }

        /**
         * @param p_item
         * @param p_collec
         */
        private function removeItem(p_item:Object, p_collec:ArrayCollection):void
        {
            for (var i:int = 0; i < p_collec.length; i++)
            {
                if (p_item.APP_LOGINID == p_collec.getItemAt(i).APP_LOGINID)
                {
                    p_collec.removeItemAt(i);
                    break;
                }
            }
        }

        /**
         * Destruction d'écouteurs
         */
        public function removeServicesListeners():void
        {
            userServices.myHandlers.removeEventListener(UserEvent.DUPLICATE_PROFILE_SUCCESS, onServiceResultHandler);
            userServices.myHandlers.removeEventListener(UserEvent.DUPLICATE_PROFILE_ERROR, onServiceResultHandler);
            userServices.myHandlers.removeEventListener(UserEvent.AFFECT_ACCESS_ERROR, onServiceResultHandler);
            userServices.myHandlers.removeEventListener(UserEvent.AFFECT_ACCESS_SUCCESS, onServiceResultHandler);
            userServices.myHandlers.removeEventListener(UserEvent.AFFECT_STYLE_ERROR, onServiceResultHandler);
            userServices.myHandlers.removeEventListener(UserEvent.AFFECT_STYLE_SUCCESS, onServiceResultHandler);
        }

        /**********************************************************************************************
         *		                        GETTER / SETTER
         *********************************************************************************************/
        public function get refUser():UserVO
        {
            return _refUser;
        }

        /**
         * A chaque mise à jour de l'utilisateur de Réf ,
         * récupération de ses  caractéristiques via
         * appels distants
         * @param value
         */
        public function set refUser(value:UserVO):void
        {
            _refUser = value;
            initDisplay();
            if (refUser && !duplicateAll)
            {
                userServices = new UserServices();
                userServices.getUserAccess(refUser.app_login, refGroup.IDGROUPE_CLIENT);
                userServices.myHandlers.addEventListener(UserEvent.USER_ACCESS_LOAD_SUCCESS, onServiceResultHandler);
                userServices.myHandlers.addEventListener(UserEvent.USER_DATA_LOADED_ERROR, onServiceResultHandler);
            }
        }

        public function get refStyle():StyleVO
        {
            return _refStyle;
        }

        public function set refStyle(value:StyleVO):void
        {
            _refStyle = value;
            if (refUser)
            {
                refUser.style = value;
            }
            // maj des champs associés
            initStlyeDisplay();
        }

        public function get styleList():ArrayCollection
        {
            return _styleList;
        }

        public function set styleList(value:ArrayCollection):void
        {
            _styleList = value;
        }

        public function get selectedUsers():ArrayCollection
        {
            return _selectedUsers;
        }

        public function set selectedUsers(value:ArrayCollection):void
        {
            _selectedUsers = value;
        }

        public function get paginateControleur():PaginateDatagridControleur
        {
            return _paginateControleur;
        }

        public function set paginateControleur(value:PaginateDatagridControleur):void
        {
            _paginateControleur = value;
        }

        public function get inter():ItemIntervalleVO
        {
            return _inter;
        }

        public function set inter(value:ItemIntervalleVO):void
        {
            _inter = value;
        }

        public function get refGroup():Object
        {
            return _refGroup;
        }

        public function set refGroup(value:Object):void
        {
            _refGroup = value;
        }

        public function get duplicateAll():Boolean
        {
            return _duplicateAll;
        }

        public function set duplicateAll(value:Boolean):void
        {
            if (value)
            {
                accessStyleDetailsBox.width = 0;
            }
            else
            {
                accessStyleDetailsBox.width = 600;
            }
            _duplicateAll = value;
        }
    }
}