package intranet.gestionLoginClasse
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class PanelMultipleAcces extends PanelMultipleAccesIHM
	{
		private var idGroupeAcces : int;
		private var colCompte : ArrayCollection;
		private var colClient : ArrayCollection;
		private var libelleGroupClient : String;
		private var colStyle : ArrayCollection;
		
		[Bindable]
		private var colAdresseIp : ArrayCollection = new ArrayCollection();
		public function PanelMultipleAcces(idGroupeAcces : int,libelleGroupClient : String,colCompte  :ArrayCollection,colClient :ArrayCollection,colStyle :ArrayCollection)
		{
			this.colStyle= colStyle;
			this.idGroupeAcces=idGroupeAcces;
			this.libelleGroupClient=libelleGroupClient;
			this.colCompte=colCompte;
			this.colClient=colClient;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			
		}
		private function initIHM(evt : FlexEvent):void{
			labTitre.text = "Création d'un accès au groupe : "+libelleGroupClient+".";
			labssTitre.text ="Selection du compte client source";
			dgGroupeMaitre.dataProvider=colCompte;
			dgGroupeMaitre.addEventListener(Event.CHANGE,updateDgLoginParOrga)
			btCancel.addEventListener(MouseEvent.CLICK,removeMe);
			inputFiltreGroupeClient.addEventListener(Event.CHANGE,filtreGroupeClient);
			
			btParamAcces.addEventListener(MouseEvent.CLICK,paramAcces);
			btCreerMultipleAcces.addEventListener(MouseEvent.CLICK,enregistrerInfosAcces);
			/*btNouveauIP.addEventListener(MouseEvent.CLICK,newPopUpGestionIP);
			btSupprimerIP.addEventListener(MouseEvent.CLICK,deleteAdrIP);*/
			addEventListener(CloseEvent.CLOSE,fermer);
		
		}
		 private function removeMe(event:MouseEvent):void {
            PopUpManager.removePopUp(this);
        }
        private function updateDgLoginParOrga(evt : Event):void
        {
        	//Alert.show("-->"+colClient.length);
        	if(dgGroupeMaitre.selectedIndex != -1)
        	{
	        	var size : int =  colClient.length;
	        	var colTemp : ArrayCollection = new ArrayCollection();
	        	for(var i : int=0;i < size;i++){
	        		if(colClient.getItemAt(i).LIBELLE_GROUPE_CLIENT == dgGroupeMaitre.selectedItem.LIBELLE_GROUPE_CLIENT)
	        		{
	        			colTemp.addItem(colClient.getItemAt(i));
	        		}
	        	}
	        	dgLoginParCompte.dataProvider=colTemp;
	        	
	        }
        }
        public function filtreGroupeClient(e:Event):void {
			colCompte.filterFunction=filtreGridGroupeClient;
			colCompte.refresh();
		}
		public function filtreGridGroupeClient(item:Object):Boolean {
			
			if (((item.LIBELLE_GROUPE_CLIENT as String).toLowerCase()).search(inputFiltreGroupeClient.text.toLowerCase())!=-1 ) {
				return (true);
			}
			else
			{
				return (false);
			}
			
		}
		private function paramAcces(evt : MouseEvent):void
		{
			if(dgGroupeMaitre.selectedIndex != -1 && (dgLoginParCompte.dataProvider as ArrayCollection).length>0)
			{
				viewstack.selectedIndex=1;
				dgStyle.dataProvider=colStyle;
				//dgAdresseIP.dataProvider = colAdresseIp;
			}
			else
			{
				if (dgGroupeMaitre.selectedIndex == -1){
					Alert.show("Selectionnez un groupe");
				}
				else
				{
					Alert.show("Aucun login à parametrer");
				}
				
			}
		}
		/*private function newPopUpGestionIP(evt : MouseEvent):void
		{
			var unPanel : PanelAdresseIPseveralLogin = new PanelAdresseIPseveralLogin(colAdresseIp);
			PopUpManager.addPopUp(unPanel,this,true);
			PopUpManager.centerPopUp(unPanel);
		}
		private function updateDgAdresseIP(evt : Event):void{
			if(dgAdresseIP.selectedIndex != -1){
				btSupprimerIP.enabled = true;
			}
			else
			{
				btSupprimerIP.enabled = false;
			}
		}
		
		
			//Affichage du msg de confirmation
		private function deleteAdrIP(event : MouseEvent):void{
			if (dgAdresseIP.selectedIndex !=-1){
				Alert.show("Confirmez la suppression svp","Confirmation",Alert.OK | Alert.CANCEL,null,confirmSuppressionIp,null,Alert.OK);
			}
			else
			{
				Alert.show("Sélectionnez une IP");
			}
		}*/
		//Gestion du résultat de la demande de confirmation de suppression d'un client
		private function confirmSuppressionIp(evt_obj : Object):void{
					
		if (evt_obj.detail == Alert.OK) {
 							
						
		 }
		 else
		 { 
		 	if (evt_obj.detail == Alert.CANCEL) 
		 	{
 				Alert.show("annulé");
 			}
			}
		}
		
		/********************************************************/
		
	private function processSupIp(evt : ResultEvent):void
	{
		Alert.show("Adresse IP supprimée");
		//initGridAdresseIP(); 
	}
	private function enregistrerInfosAcces(evt : Event):void
	{
		var valeurDroitFournisseur : int;
		if(checkAdmin.selected == true){
			valeurDroitFournisseur = 1;
		}
		else
		{
			valeurDroitFournisseur = 0;
		}
		if(dgStyle.selectedIndex != -1){
		var idGroupeSource : int = dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT;
		var idStyle :int = dgStyle.selectedItem.IDCONSO_STYLE;
		var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.GestionClient",
																	"add_several_acces",
																	processCreatMultipleAcces,null);
		RemoteObjectUtil.callService(op,
									idGroupeSource,
									idGroupeAcces,
									idStyle,
									radioButtonsUsages.selectedValue as int,
									radioButtonsModuleBase.selectedValue as int,
									radioButtonsCycleDeVie.selectedValue as int,
									radioButtonsGestionOrgas.selectedValue as int,
									radioButtonFixData.selectedValue as int,
									radioButtonsMobile.selectedValue as int,
									radioButtonsLogin.selectedValue as int,
									valeurDroitFournisseur
									);
		}
		else
		{
			Alert.show("Sélectionnez un Style"); 
		}
	}
	private function processCreatMultipleAcces(evt : ResultEvent):void
	{
		if (evt.result != -1)
		{
			//Alert.show("Création des accès réussi. Voulez vous créer une restriction IP sur les logins du groupe","Informations enregistrées",Alert.OK | Alert.CANCEL,null,gestionIp,null,Alert.OK);
			Alert.show("Accès créés");
			PopUpManager.removePopUp(this);
		}
		else
		{
			Alert.show("erreur :"+evt.toString());
		}
	}
	
	private function gestionIp(evt_obj : Object):void{
					
		if (evt_obj.detail == Alert.OK) {
 			tabParametre.selectedIndex = 1;
		 }
		 else
		 { 
		 	if (evt_obj.detail == Alert.CANCEL) 
		 	{
 				PopUpManager.removePopUp(this);
 			}
			}
	}
	private function fermer(evt : CloseEvent):void
	{
		PopUpManager.removePopUp(this);	
	}
	}	
}