package intranet {
	import mx.events.FlexEvent;
	import flash.events.Event;
	import composants.historique.HistoriqueDataGrid;
	import composants.util.DateFunction;
	
	public class Historique extends HistoriqueIHM {
		
		protected var dateDeb:Date = new Date(2005,01,01);
		protected var dateFin:Date = new Date();
			
		public function Historique() {
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		protected function initIHM(event:Event):void {
			histoDataGrid.addEventListener(HistoriqueDataGrid.HISTO_DATA_GRID_READY,onHistoDataGridReady);
		}
		
		public function getHisto(idgroupe:int):void {
			histoDataGrid.loadHistoData(dateDeb,dateFin,idgroupe);
		}
		
		private function onHistoDataGridReady(event:Event):void {
			var dateDebMonthIndex:int = histoDataGrid.DATE_DEB.month;
			var dateFinMonthIndex:int = histoDataGrid.DATE_FIN.month;
			
			histoPanel.title = "Historique de : " + DateFunction.MONTH_NAMES[dateDebMonthIndex]  + " " +
			histoDataGrid.DATE_DEB.fullYear + " a " +
			DateFunction.MONTH_NAMES[dateFinMonthIndex]  + " " + histoDataGrid.DATE_FIN.fullYear;
		}
		
		public function clearHisto():void {
			try {
				histoDataGrid.dataProvider=null;	
			} catch (e:Error) {
				
			}
		}
	}
}
