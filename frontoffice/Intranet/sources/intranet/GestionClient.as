package intranet
{
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.events.ProgressEvent;
    import flash.net.FileReference;
    import flash.net.URLRequest;
    
    import fr.consotel.consoview.util.ConsoViewErrorManager;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import intranet.event.PropagationEvent;
    import intranet.service.PropagationService;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;

    public class GestionClient extends GestionClientIHM
    {
        [Embed(source="/assets/images/alert_icon.png")]
        public static const imgAlert:Class;
        private var _orgaGeoExists:Boolean = false;
        private var _orgaOpeExists:Boolean = false;
        private var fr:FileReference = new FileReference();
        public const UPLOAD_URL:String = index.urlBackoffice + "/fr/consotel/consoprod/intranet/processUpload.cfm";
        [Bindable]
        private var myFilterArray:ArrayCollection = new ArrayCollection();
        [Bindable]
        public var arrListExercice:ArrayCollection = new ArrayCollection()
		
		private var _propagationService:PropagationService = new PropagationService();	
			
        public function GestionClient()
        {
            super();
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
        }

        protected function initIHM(event:Event):void
        {
            //dgGroupeMaitre.addEventListener(Event.CHANGE,updateHisto);
            dgGroupeMaitre.addEventListener(Event.CHANGE, updateOrga);
            dgListeExercice.addEventListener(GestionClientEvent.REFRESH_DATAGRID, refreshDatagrid);
            dgOrga.addEventListener(Event.CHANGE, enableChangeOnOrga);
            btnAddOrga.addEventListener(MouseEvent.CLICK, addOrga);
            btnDeleteOrga.addEventListener(MouseEvent.CLICK, deleteOrga);
            txtFiltre.addEventListener(Event.CHANGE, filtre);
            cmbTypeOrga.addEventListener(Event.CHANGE, checkTypeOrga);
            cmbOperateur.addEventListener(Event.CHANGE, checkOperateur);
            txtLibelle.addEventListener(KeyboardEvent.KEY_UP, checkLibelle);
            var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getGroupesMaitres", processInitGroupes, remoteError);
            RemoteObjectUtil.callService(op, -1);
            // pour le upload
            var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getFiles", processInitFiles, remoteError);
            RemoteObjectUtil.callService(op2, [ "csv" ]);
            btnstartUpload.addEventListener(MouseEvent.CLICK, startUpload);
            btnTest.addEventListener(MouseEvent.CLICK, testFile);
            btnDeleteFile.addEventListener(MouseEvent.CLICK, deleteFile);
            cmbFileTest.addEventListener(Event.CHANGE, checkFileTest);
            fr.addEventListener(Event.SELECT, selectHandler);
            fr.addEventListener(Event.OPEN, openHandler);
            fr.addEventListener(Event.COMPLETE, completeHandler);
			btPropager.addEventListener(MouseEvent.CLICK,btPropagerClickHandler);
			bxPropager.visible = false;
			_propagationService.addEventListener(PropagationEvent.PROPAGATION_FAILED,propagationEventHandler);
			_propagationService.addEventListener(PropagationEvent.PROPAGATION_SUCCED,propagationEventHandler);
        }
		
		
		
        private function refreshDatagrid(e:Event):void
        {
            if (dgGroupeMaitre.selectedIndex > -1)
            {
                Alert.show("exercice supprimé avec succés", "succés");
                var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getListExercice", listExerciceHandler, remoteError);
                RemoteObjectUtil.callService(op, dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
            }
        }

        private function updateOrga(event:Event):void
        {
            if (dgGroupeMaitre.selectedIndex != -1)
            {
                btnAddOrga.enabled = true;
                frmAddOrga.enabled = true;
                frmAddOrga2.enabled = true;
                dgOrga.dataProvider = "";
                cmbTypeOrga.selectedIndex = 0;
                cmbTypeOrga.dispatchEvent(new Event(Event.CHANGE));
                txtLibelle.text = '';
                var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getOrgas", processGetOrgas, remoteError);
                RemoteObjectUtil.callService(op, dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
                var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getListeOperateur", processGetListeOperateur, remoteError);
                RemoteObjectUtil.callService(op2, dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
                var op3:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "checkOrgaGeo", processcheckOrgaGeo, remoteError);
                RemoteObjectUtil.callService(op3, dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
                var op4:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getFiles", processInitFiles, remoteError);
                RemoteObjectUtil.callService(op4, [ "csv" ]);
                var op5:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getListExercice", listExerciceHandler, remoteError);
                RemoteObjectUtil.callService(op5, dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
            }
            else
            {
                btnAddOrga.enabled = false;
                frmAddOrga.enabled = false;
                frmAddOrga2.enabled = true;
            }
        }

        private function listExerciceHandler(e:ResultEvent):void
        {
            arrListExercice = new ArrayCollection();
            arrListExercice = e.result as ArrayCollection;
            dgListeExercice.dataProvider = arrListExercice;
        }

        private function testFile(event:MouseEvent):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "TestFile", processTestFile, remoteError);
            RemoteObjectUtil.callService(op, dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT, cmbFileTest.selectedItem.label);
        }

        private function deleteFile(event:MouseEvent):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "deleteFile", processDeleteFile, remoteError);
            RemoteObjectUtil.callService(op, cmbFileTest.selectedItem.label);
        }

        private function processcheckOrgaGeo(event:ResultEvent):void
        {
            if (event.result == "true")
            {
                _orgaGeoExists = true;
            }
            else
            {
                _orgaGeoExists = false;
            }
        }

        private function processTestFile(event:ResultEvent):void
        {
            if (event.result.length == 0)
            {
                Alert.show("Fichier OK");
            }
            else
            {
                dgTestFile.dataProvider = event.result;
            }
        }

        private function processDeleteFile(event:ResultEvent):void
        {
            var op4:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getFiles", processInitFiles, remoteError);
            RemoteObjectUtil.callService(op4, [ "csv" ]);
            Alert.show("Fichier Supprimé.");
        }

        private function addOrga(event:Event):void
        {
            if (cmbFileTest.selectedItem.data == '')
            {
                var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "addOrga", processAddOrgas, remoteError);
                RemoteObjectUtil.callService(op, dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT, cmbTypeOrga.selectedItem.data, cmbOperateur.selectedItem.data, txtLibelle.text);
            }
            else
            {
                var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "addOrgaFromFile", processAddOrgas, remoteError);
                Alert.show("groupe Maitre -->" + dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT + "-- " + cmbFileTest.selectedItem.label + "--" + cmbTypeOrga.selectedItem.data + "---libellé" + txtLibelle.text);
                RemoteObjectUtil.callService(op2, dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT, cmbFileTest.selectedItem.label, cmbTypeOrga.selectedItem.data, txtLibelle.text);
            }
        }

        private function deleteOrga(event:Event):void
        {
            Alert.okLabel = "Valider";
            Alert.buttonWidth = 100;
            Alert.cancelLabel = "Annuler";
            Alert.show("Etes-vous sur de vouloir supprimer cette organisation? La suppression de cette organisation entrainera la suppression de l'ensemble des RAGO se référant à cette organisation et impactera l'historisation des clichés.", "Suppression d'une organisation ", Alert.OK | Alert.CANCEL, this, confirmDeleteOrgaHandler, imgAlert, Alert.OK);
        }

        public function confirmDeleteOrgaHandler(event:CloseEvent):void
        {
            // si oui suppression l'orga
            if (event.detail == Alert.OK)
            {
                var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "deleteOrga", processDeleteOrgas, remoteError);
                RemoteObjectUtil.callService(op, dgOrga.selectedItem.IDGROUPE_CLIENT, dgOrga.selectedItem.TYPE);
            }
            else if (event.detail == Alert.CANCEL)
            {
            }
        }

        private function processGetOrgas(event:ResultEvent):void
        {
            dgOrga.dataProvider = event.result;
            btnDeleteOrga.enabled = false;
        }

        private function processGetListeOperateur(event:ResultEvent):void
        {
            cmbOperateur.dataProvider = event.result;
        }

        private function processAddOrgas(event:ResultEvent):void
        {
            Alert.show(event.result.toString());
            if (event.result >= 1)
            {
                updateOrga(new Event(""));
            }
            else
            {
                Alert.show('Pb: ' + event.result.toString());
            }
        }

        private function processDeleteOrgas(event:ResultEvent):void
        {
            if (event.result == 1)
            {
                updateOrga(new Event(""));
            }
            else
            {
                Alert.show('Pb: ' + event.result.toString());
            }
        }

        private function enableChangeOnOrga(event:Event):void
        {
            if (dgOrga.selectedIndex != -1)
            {
                btnDeleteOrga.enabled = true;
                var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getInfosOrga", processgetInfosOrga, remoteError);
                RemoteObjectUtil.callService(op, dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT, dgOrga.selectedItem.IDGROUPE_CLIENT);
            }
            else
            {
                btnDeleteOrga.enabled = false;
            }
        }

        private function processgetInfosOrga(event:ResultEvent):void
        {
            lbnodes.text = event.result[0].NBNODES;
            lbaffect.text = event.result[0].NBSOUS_TETE_ORGA;
            lbtotal.text = event.result[0].NBSOUS_TETE_TOTAL;
        }

        /* private function updateHisto(event:Event):void {
           if (dgGroupeMaitre.selectedIndex!=-1) {
           myHisto.getHisto(dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT);
           } else {
           myHisto.clearHisto();
           }
           }
         */
        private function processInitFiles(event:ResultEvent):void
        {
            var myA:ArrayCollection = event.result as ArrayCollection;
            myA.addItemAt({ label: "Choisir un fichier", data: "" }, 0);
            cmbFileTest.dataProvider = myA;
        }

        private function processInitGroupes(event:ResultEvent):void
        {
            myFilterArray = event.result as ArrayCollection;
            dgGroupeMaitre.dataProvider = event.result;
        }

        private function remoteError(faultEvent:FaultEvent):void
        {
            Alert.show(faultEvent.toString());
            ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
        }

        private function dispatchLogoff(event:Event):void
        {
            dispatchEvent(new Event(Intranet.LOGOFF_EVENT));
        }

        public function filtre(e:Event):void
        {
            myFilterArray.filterFunction = filtreGrid;
            myFilterArray.refresh();
        }

        private function checkTypeOrga(event:Event):void
        { //Vérification du type d'orga sélectionée
            // vérifie le choix du combo
            switch (cmbTypeOrga.selectedItem.data)
            {
                case '':
                    btnAddOrga.enabled = false;
                    break;
                case 'OPE':
                    txtLibelle.text = '';
                    txtLibelle.enabled = false;
                    cmbOperateur.enabled = true;
                    btnAddOrga.enabled = true;
                    break;
                case 'GEO':
                    txtLibelle.text = '';
                    txtLibelle.enabled = false;
                    cmbOperateur.enabled = false;
                    // Si il existe déja une organisation géographique
                    if (_orgaGeoExists)
                    {
                        btnAddOrga.enabled = false;
                        Alert.show('Il existe déjà une organisation géographique pour ce compte. Il faut la supprimer ou la transformer d\'abord.');
                    }
                    else
                    {
                        btnAddOrga.enabled = true;
                    }
                    break;
                case 'CUS':
                    cmbOperateur.enabled = false;
                    if (cmbFileTest.selectedItem.data != '')
                    {
                        txtLibelle.enabled = false;
                        btnAddOrga.enabled = true;
                    }
                    else
                    {
                        txtLibelle.enabled = true;
                    }
                    break;
                default:
                    txtLibelle.enabled = true;
                    cmbOperateur.enabled = false;
                    if (txtLibelle.text.length == 0)
                    {
                        btnAddOrga.enabled = false;
                    }
                    else
                    {
                        btnAddOrga.enabled = true;
                    }
                    break;
            }
        }

        private function checkFileTest(event:Event):void
        { //Vérification du type d'orga sélectionée
            // vérifie le choix du combo
            switch (cmbFileTest.selectedItem.data)
            {
                case '':
                    btnTest.enabled = false;
                    txtLibelle.enabled = true;
                    break;
                default:
                    btnTest.enabled = true;
                    btnDeleteFile.enabled = true;
                    txtLibelle.enabled = false;
                    if (cmbTypeOrga.selectedItem.data == 'CUS')
                    {
                        btnAddOrga.enabled = true;
                    }
                    break;
            }
        }

        private function checkLibelle(event:KeyboardEvent):void
        { //Vérification du libelle
            if (txtLibelle.text.length == 0 && (cmbTypeOrga.selectedItem.data == 'CUS' || cmbTypeOrga.selectedItem.data == 'ANA'))
            {
                btnAddOrga.enabled = false;
            }
            else
            {
                btnAddOrga.enabled = true;
            }
        }

        private function checkOperateur(event:Event):void
        { //Vérification de l'opérateur
            // vérifie le choix du combo
            (dgOrga.dataProvider as ArrayCollection).source.forEach(checkOpe);
            if (_orgaOpeExists)
            {
                btnAddOrga.enabled = false;
                Alert.show('Il existe déjà une organisation opérateur pour cet opérateur pour ce compte.');
            }
            else
            {
                btnAddOrga.enabled = true;
            }
            _orgaOpeExists = false;
        }

        private function checkOpe(elem:*, index:int, tab:Array):void
        { //Vérification de l'opérateur
            // vérifie le choix du combo
            if (_orgaOpeExists == false)
            {
                if (elem.OPERATEURID == cmbOperateur.selectedItem.data)
                {
                    _orgaOpeExists = true;
                }
                else
                {
                    _orgaOpeExists = false;
                }
            }
        }

        public function filtreGrid(item:Object):Boolean
        {
            if (item.LIBELLE_GROUPE_CLIENT.substr(0, txtFiltre.text.length).toLowerCase() == txtFiltre.text.toLowerCase() || parseInt(item.IDGROUPE_CLIENT.toString().substr(0, txtFiltre.text.length)) == parseInt(txtFiltre.text))
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        private function dumbProcess(event:ResultEvent):void
        {
        }

        public function startUpload(m:MouseEvent):void
        {
            fr.browse();
        }

        public function cancelUpload(m:MouseEvent):void
        {
            fr.cancel();
        }

        private function selectHandler(event:Event):void
        {
            var request:URLRequest = new URLRequest();
            request.url = UPLOAD_URL;
            fr.upload(request);
        }

        /**
         * When the OPEN event has dispatched, change the progress bar's label
         * and enable the "Cancel" button, which allows the user to abort the
         * download operation.
         */
        private function openHandler(event:Event):void
        {
            //pbuploadProgress.label = "DOWNLOADING %3%%";
        }

        /**
         * While the file is downloading, update the progress bar's status.
         */
        private function progressHandler(event:ProgressEvent):void
        {
            //pbuploadProgress.setProgress(event.bytesLoaded, event.bytesTotal);
        }

        /**
         * Once the download has completed, change the progress bar's label one
         * last time and disable the "Cancel" button since the download is
         * already completed.
         */
        private function completeHandler(event:Event):void
        {
            var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getFiles", processInitFiles, remoteError);
            RemoteObjectUtil.callService(op2, [ "csv" ]);
            Alert.show("Chargement OK");
        }
		
		/**
		 *Handler de l'evenement click sur le bouton propoger
		 * propage le cliché de l'orga sur les x mois précedants 
		 */
		private function btPropagerClickHandler(event:MouseEvent):void
		{
			if(dgOrga.selectedItem != null 
				&& dgOrga.selectedItem.TYPE != null 
					&& String(dgOrga.selectedItem.TYPE).toUpperCase() != "OPE")
			{
				var nbMois:Number = (nsNnbMois.value> 0)?nsNnbMois.value:1;
				var idOrga:Number = dgOrga.selectedItem.IDGROUPE_CLIENT; 
				_propagationService.propagerCliche(idOrga,nbMois);
			}
			else
			{
				Alert.show("Vous devez sélectionner une organisation cliente / sauvegardée / Annuaire");			
			}
		}
		
		protected function propagationEventHandler(event:PropagationEvent):void
		{
			if(event.type == PropagationEvent.PROPAGATION_SUCCED)
			{
				Alert.show("Propagation effectuée","Infos");
			}
			else
			{
				Alert.show("Erreur lors de la tentative de propagation","erreur");
			}
		}
    }
}