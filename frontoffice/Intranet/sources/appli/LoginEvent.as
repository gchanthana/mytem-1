package appli {
	import flash.events.Event;

	public class LoginEvent extends Event {
		public static const VALIDE:String = "Login/Pwd Valide";
		public static const INVALIDE:String = "Login/Pwd Invalide";
		
		public function LoginEvent(type:String) {
			super(type);
		}
	}
}
