package
{
	import mx.events.FlexEvent;
	import mx.collections.ArrayCollection;
	
	public class sessionManager
	{
		public var code:String;
		public var logon:String;
		public var id:String;
		public var username:String;
		public var wwwPage:String;
		public var urlTelephone:String;
		public var nom:String;
		public var description:String;
		public var email:String;
		public var department:String;
		
		public function sessionManager():void {
			
		}
		
		public function setSession(obj:ArrayCollection):void {
			code="interne";
			logon="1";
			id=obj[0].physicalDeliveryOfficeName;
			username=obj[0].mailNickname;
			wwwPage=obj[0].wWWHomePage;
			urlTelephone=obj[0].info;
			nom=obj[0].cn;
			description=obj[0].description;
			email=obj[0].mail;
			department=obj[0].department;
		}
		
	}
}