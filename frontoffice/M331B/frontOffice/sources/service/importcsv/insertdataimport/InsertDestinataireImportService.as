package service.importcsv.insertdataimport
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;

	public class InsertDestinataireImportService
	{
		
		public var _model:InsertDestinataireImportModel;
		public var handler:InsertDestinataireImportHandler;	
		
		public function InsertDestinataireImportService()
		{
			this._model  = new InsertDestinataireImportModel();
			this.handler = new InsertDestinataireImportHandler(model);
		}
		public function get model():InsertDestinataireImportModel
		{
			return this._model;
		}	
		public function insertDestinataireImport(xml:String): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.import.insertDestinataireImport","insertDestinataireImport",handler.getResultImportHandler); 
			
			RemoteObjectUtil.callService(op,xml);	
		}
	}
}