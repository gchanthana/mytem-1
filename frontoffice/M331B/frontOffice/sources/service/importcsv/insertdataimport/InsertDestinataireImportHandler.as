package service.importcsv.insertdataimport
{
	import mx.rpc.events.ResultEvent;

	public class InsertDestinataireImportHandler
	{
	
		private var _model:InsertDestinataireImportModel;
		
		public function InsertDestinataireImportHandler(_model:InsertDestinataireImportModel):void
		{
			this._model = _model;
		}
		
		internal function getResultImportHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.getResultImportHandler(evt);
			}
		}	
	}
}