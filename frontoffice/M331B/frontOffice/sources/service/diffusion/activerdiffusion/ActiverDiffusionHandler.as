package service.diffusion.activerdiffusion
{
	import mx.rpc.events.ResultEvent;

	internal class ActiverDiffusionHandler
	{
		private var _model:ActiverDiffusionModel;

		public function ActiverDiffusionHandler(_model:ActiverDiffusionModel)
		{
			this._model=_model;
		}

		internal function activerDiffusionHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.activerDiffusion(event);
			}
		}
	}
}
