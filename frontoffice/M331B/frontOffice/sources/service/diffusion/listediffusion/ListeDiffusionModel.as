package service.diffusion.listediffusion
{
	import event.diffusion.DiffusionEvent;
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import vo.diffusion.Diffusion;
	import vo.diffusion.Orga;

	internal class ListeDiffusionModel extends EventDispatcher
	{

		private var _diffusions:ArrayCollection;

		public function ListeDiffusionModel()
		{
			_diffusions=new ArrayCollection();
		}

		public function get diffusions():ArrayCollection
		{
			return _diffusions;
		}

		internal function listeDiffusion(value:ArrayCollection):void
		{
			_diffusions.removeAll();

			var uneDiffusion:Diffusion;

			if (value.length > 0)
			{
				for (var i:int=0; i < value.length; i++)
				{
					uneDiffusion=new Diffusion();
					uneDiffusion.fill(value[i]);
					_diffusions.addItem(uneDiffusion);
				}
			}
			dispatchEvent(new DiffusionEvent(DiffusionEvent.LISTE_DIFFUSION_RAPPORT_EVENT)); // capturé par listeDiffusionImpl
		}
	}
}
