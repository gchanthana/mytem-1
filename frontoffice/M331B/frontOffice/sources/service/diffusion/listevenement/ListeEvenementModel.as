package service.diffusion.listevenement
{
	import event.diffusion.DiffusionEvent;
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import vo.diffusion.Evenement;

	internal class ListeEvenementModel extends EventDispatcher
	{

		private var _events:ArrayCollection;
		private var _idEventSelected:int;
		private var _selectedIndexEvent:int; // l'index -1 par défaut

		public function ListeEvenementModel()
		{
			_events=new ArrayCollection();
		}

		public function get events():ArrayCollection
		{
			return _events;
		}

		public function get selectedIndexEvent():int
		{
			return _selectedIndexEvent;
		}

		public function set selectedIndexEvent(value:int):void
		{
			_selectedIndexEvent=value;
		}

		public function get idEventSelected():int
		{
			return _idEventSelected;
		}

		public function set idEventSelected(value:int):void
		{
			_idEventSelected=value;
		}

		internal function listeEvenement(value:ArrayCollection):void
		{
			_events.removeAll();

			var unEvent:Evenement;

			if (value.length > 0)
			{
				for (var i:int=0; i < value.length; i++)
				{
					unEvent=new Evenement();
					unEvent.fill(value[i]);
					_events.addItem(unEvent);

					if (unEvent.idEvent == Math.abs(this._idEventSelected))
					{
						_selectedIndexEvent=i;
					}
				}
			}
			dispatchEvent(new DiffusionEvent(DiffusionEvent.LISTE_EVENTS_EVENT)); // capturé par diffusionImpl
		}
	}
}
