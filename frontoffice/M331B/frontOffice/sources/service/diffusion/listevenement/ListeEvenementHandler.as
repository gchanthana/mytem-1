package service.diffusion.listevenement
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ListeEvenementHandler
	{
		private var _model:ListeEvenementModel;
		
		public function ListeEvenementHandler(_model:ListeEvenementModel)
		{
			this._model=_model;
		}
		
		internal function getListeEventHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.listeEvenement(event.result as ArrayCollection);
			}
		}
	}
}