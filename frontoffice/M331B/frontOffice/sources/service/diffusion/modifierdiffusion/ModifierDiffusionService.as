package service.diffusion.modifierdiffusion
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class ModifierDiffusionService
	{
		public var _model:ModifierDiffusionModel;
		public var handler:ModifierDiffusionHandler;

		public function ModifierDiffusionService()
		{
			this._model=new ModifierDiffusionModel();
			this.handler=new ModifierDiffusionHandler(model);
		}

		public function get model():ModifierDiffusionModel
		{
			return this._model;
		}

		public function modifierDiffusion(idRapport:int, idEvent:int,idOrga:int,idProfil:int,nbTop:int,finPeriode:int,periodeEtude:int,jourLancement:int,
										 destinatairesCaches:String,objetMail:String,nomAttachementFile:String,corpsMail:String,valeurTemplate :String,format:int , noContrat :int):void
		{
			var callOp:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.ModifierDiffusionService", "modifierDiffusion", handler.modifierDiffusionHandler);

			RemoteObjectUtil.callService(callOp,idRapport,idEvent,idOrga,idProfil,nbTop,finPeriode,periodeEtude,jourLancement,
				destinatairesCaches,objetMail,nomAttachementFile,corpsMail,valeurTemplate,format,noContrat);
		}
	}
}
