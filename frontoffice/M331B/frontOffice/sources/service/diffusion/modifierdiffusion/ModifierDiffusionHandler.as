package service.diffusion.modifierdiffusion
{
	import mx.rpc.events.ResultEvent;

	internal class ModifierDiffusionHandler
	{
		private var _model:ModifierDiffusionModel;

		public function ModifierDiffusionHandler(_model:ModifierDiffusionModel)
		{
			this._model=_model;
		}

		internal function modifierDiffusionHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.modifierDiffusion(event);
			}
		}
	}
}
