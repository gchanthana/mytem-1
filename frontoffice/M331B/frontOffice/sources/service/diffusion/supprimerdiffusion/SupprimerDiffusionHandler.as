package service.diffusion.supprimerdiffusion
{
	import mx.rpc.events.ResultEvent;

	internal class SupprimerDiffusionHandler
	{
		private var _model:SupprimerDiffusionModel;

		public function SupprimerDiffusionHandler(_model:SupprimerDiffusionModel)
		{
			this._model=_model;
		}

		internal function supprimerDiffusionHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.supprimerDiffusion(event);
			}
		}
	}
}
