package service.diffusion.renvoyerdiffusion
{
	import mx.rpc.events.ResultEvent;

	internal class RenvoyerDiffusionHandler
	{
		private var _model:RenvoyerDiffusionModel;

		public function RenvoyerDiffusionHandler(_model:RenvoyerDiffusionModel)
		{
			this._model=_model;
		}

		internal function renvoyerDiffusionHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.renvoyerDiffusion(event);
			}
		}
	}
}
