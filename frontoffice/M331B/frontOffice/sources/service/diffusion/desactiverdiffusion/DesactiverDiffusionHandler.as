package service.diffusion.desactiverdiffusion
{
	import mx.rpc.events.ResultEvent;

	internal class DesactiverDiffusionHandler
	{
		private var _model:DesactiverDiffusionModel;

		public function DesactiverDiffusionHandler(_model:DesactiverDiffusionModel)
		{
			this._model=_model;
		}

		internal function desactiverDiffusionHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.desactiverDiffusion(event);
			}
		}
	}
}
