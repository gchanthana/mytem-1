package service.diffusion.desactiverdiffusion
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class DesactiverDiffusionService
	{
		public var _model:DesactiverDiffusionModel;
		public var handler:DesactiverDiffusionHandler;

		public function DesactiverDiffusionService()
		{
			this._model=new DesactiverDiffusionModel();
			this.handler=new DesactiverDiffusionHandler(model);
		}

		public function get model():DesactiverDiffusionModel
		{
			return this._model;
		}

		public function desactiverDiffusion(idEvent:int,noContrat:int):void
		{
			var callOp:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.ActionsDiffusionService", "desactiverDiffusion", handler.desactiverDiffusionHandler);
			
			RemoteObjectUtil.callService(callOp,idEvent,noContrat);
		}
	}
}
