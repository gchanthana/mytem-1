package service.diffusion.listeorgaclient
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class ListeOrgaClientService
	{
		public var _model:ListeOrgaClientModel;
		public var handler:ListeOrgaClientHandler;	
		
		public function ListeOrgaClientService()
		{
			this._model  = new ListeOrgaClientModel();
			this.handler = new ListeOrgaClientHandler(model);
		}
		public function get model():ListeOrgaClientModel
		{
			return this._model;
		}	
		public function getListeOrgaClient(): void
		{
			var callOp:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.ListeOragClientService","getListeOrgaClient",handler.getListeOragClientHandler); 
			
			RemoteObjectUtil.callService(callOp);	
		}
	}
}