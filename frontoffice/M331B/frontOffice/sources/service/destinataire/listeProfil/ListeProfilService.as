package service.destinataire.listeProfil
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;


	public class ListeProfilService
	{
		private var _model:ListeProfilModel;
		public var handler:ListeProfilHandler;	
		
		public function ListeProfilService()
		{
			this._model  = new ListeProfilModel();
			this.handler = new ListeProfilHandler(model);
		}		
		
		public function get model():ListeProfilModel
		{
			return this._model;
		}
		/**
		 * récupérer tous les profils associer à une racine ( cliquez Gérer profil) 
		 */		
		public function getListProfil():void
		{		
			var callRemote:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.ListeProfil","getAllProfils",handler.getListeProfilResultHandler); 
			
			RemoteObjectUtil.callService(callRemote);
			
		}
	}
}