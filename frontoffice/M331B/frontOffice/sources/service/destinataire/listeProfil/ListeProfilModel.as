package service.destinataire.listeProfil
{
	import event.destinataire.ProfilEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.destinataire.Profil;
	
	internal class ListeProfilModel  extends EventDispatcher
	{
		private var _listProfil :ArrayCollection;
		
		public function ListeProfilModel()
		{
			_listProfil = new ArrayCollection();	
		}
		public function get listProfil():ArrayCollection
		{
			return _listProfil;
		}
		
		internal function listeAllProfil(value:ArrayCollection):void
		{	
			_listProfil.removeAll();// vider l'ArrayCollection
			
			var unProfil:Profil;	
			
			if(value.length > 0)
			{
				for(var i:int=0;i<value.length;i++)
				{
					unProfil = new Profil();
					unProfil.fill(value[i]);
					_listProfil.addItem(unProfil); 
				}
			}			
			this.dispatchEvent(new ProfilEvent(ProfilEvent.LIST_PROFIL_EVENT));// event capturé dans ListProfilsImpl.as
		}
	}
}