package service.destinataire.associedestinatairenoeud
{
	import mx.rpc.events.ResultEvent;


	internal class AssocierDestinataireNoeudHandler
	{

		private var _model:AssocierDestinataireNoeudModel;

		public function AssocierDestinataireNoeudHandler(_model:AssocierDestinataireNoeudModel):void
		{
			this._model=_model;
		}

		internal function getAssocierResultHandler(evt:ResultEvent):void
		{
			if (evt.result)
			{
				_model.getResultHandler(evt);
			}
		}

		internal function getAssociationResultHandler(evt:ResultEvent):void
		{
			if (evt.result)
			{
				_model.getAssociationResultHandler(evt);
			}
		}

		internal function getRemoveResultHandler(evt:ResultEvent):void
		{
			if (evt.result)
			{
				_model.getRemoveResultHandler(evt);
			}
		}
	}
}
