package service.destinataire.destinataireprofil
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class DestinataireProfilService
	{
		private var _model:DestinataireProfilModel;
		public var handler:DestinataireProfilHandler;	
		
		public function DestinataireProfilService()
		{
			this._model  = new DestinataireProfilModel();
			this.handler = new DestinataireProfilHandler(model);
		}		
		
		public function get model():DestinataireProfilModel
		{
			return this._model;
		}
		
		/**
		 * associer un profil à un destinataire 
		 * @param idDestinataire
		 * @param idProfil
		 * @param idNoeud
		 * 
		 */		
		public function associerDestinataireProfil(idDestinataire:int,idProfil:int,idNoeud:int):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.DestinataireProfilService","associerProfilDestinataire",handler.getAssocierResultHandler); 
			
			RemoteObjectUtil.callService(op,idDestinataire,idProfil,idNoeud);
		}
		
		/**
		 * dissocier un profil d'un destinataire 
		 * @param idDestinataire
		 * @param idProfil
		 * @param idNoeud
		 */		
		public function dissocierDestinataireProfil(idDestinataire:int,idProfil:int,idNoeud:int):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.DestinataireProfilService","dissocierProfilDestinataire",handler.getRemoveResultHandler); 
			
			RemoteObjectUtil.callService(op,idDestinataire,idProfil,idNoeud);
		}
		
		/**
		 * récupérer les profils d'un destinataire
		 * @param idDestinataire
		 * @param idProfil
		 * @param idNoeud
		 */		
		public function getProfilDestinataire(idDestinataire:int,idNoeud:int):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.DestinataireProfilService","getProfils",handler.getProfilsResultHandler); 
			
			RemoteObjectUtil.callService(op,idDestinataire,idNoeud);
		}
	}
}