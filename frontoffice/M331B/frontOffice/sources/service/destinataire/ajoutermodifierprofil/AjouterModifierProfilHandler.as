package service.destinataire.ajoutermodifierprofil
{
	import mx.rpc.events.ResultEvent;
	

	internal class AjouterModifierProfilHandler
	{
		private var _model:AjouterModifierProfilModel;
		
		public function AjouterModifierProfilHandler(_model:AjouterModifierProfilModel)
		{
			this._model = _model;
		}
		
		internal function ajouterProfilResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.ajouterProfilResult(evt);
			}
		}
		
		internal function modifierProfilResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.modifierProfilResult(evt);
			}
		}
		
		internal function supprimerResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.supprimerProfilResult(evt);
			}
		}
		internal function supprimerPlusieursProfilsResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.supprimerPlusieursProfilsResult(evt);
			}
		}
		
	}
}