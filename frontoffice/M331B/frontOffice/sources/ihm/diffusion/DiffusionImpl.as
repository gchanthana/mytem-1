package ihm.diffusion
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.resources.ResourceManager;
	import mx.validators.NumberValidator;
	import mx.validators.StringValidator;
	import mx.validators.Validator;
	
	import composants.util.ConsoviewAlert;
	
	import event.diffusion.DiffusionEvent;
	
	import rapport.event.RapportEvent;
	import rapport.service.chercherselector.RechercherSelectorService;
	import rapport.service.formattemplatetelector.FormatTemplateSelectorService;
	import rapport.service.templateselector.TemplateSelectorService;
	import rapport.vo.Rapport;
	import rapport.vo.SelectorParametre;
	import rapport.vo.ValeurSelector;
	
	import service.diffusion.corpsmail.CorpsMailService;
	import service.diffusion.listeorgaclient.ListeOrgaClientService;
	import service.diffusion.listeprofilsorga.ListeProfilsOrgaService;
	import service.diffusion.listevenement.ListeEvenementService;
	import service.diffusion.nombredestinataire.NombreDestinataireService;
	import service.diffusion.validerdiffusion.ValiderDiffusionService;
	
	import utils.CompositeEmailValidator;
	import utils.evenements.AbstractEvenement;
	import utils.evenements.SimpleFactoryEvenement;
	
	import vo.diffusion.Evenement;
	import vo.diffusion.Orga;
	import vo.diffusion.Profil;
	
	public class DiffusionImpl extends VBox
	{
		public var rechercherSelectorService:RechercherSelectorService;
		public var listeOragnisationClient:ListeOrgaClientService;
		public var listeProfilOrganisation:ListeProfilsOrgaService;
		public var listeEventsService:ListeEvenementService;
		public var corpsMailAppService:CorpsMailService;
		public var formatSelectorService:FormatTemplateSelectorService;
		public var validerDiffusionRapport:ValiderDiffusionService;
		public var templateRapportService:TemplateSelectorService;
		public var nbDestinataireService:NombreDestinataireService;
		
		public var allSelector:ArrayCollection;
		public var newComposant:AbstractEvenement;
		
		public var objetMailValidator:StringValidator;
		public var nomFileValidator:StringValidator;
		public var listeOrgaClientValidator:NumberValidator;
		
		public var nbDestinataire:Label;
		public var mailCopieCache:TextInput;
		public var hBoxperiodDiffusion:HBox;
		public var btn_retour:Button;
		public var bodyMail:TextArea;
		public var periodEtude:Label;
		public var lbl_error:Label;
		
		public var idOrga:int;
		
		private var idRapport:int;
		private var nbTop:int;
		
		[Bindable]
		public var mailLogin:String;
		
		[Bindable]
		public var nbProfil:int;
		
		[Bindable]
		public var objetEmail:TextInput;
		
		[Bindable]
		public var nomFile:TextInput;
		
		[Bindable]
		public var nomRapport:String;
		
		[Bindable]
		public var listeOrgaClient:ComboBox; // pour afficher la liste des organisattions client
		
		[Bindable]
		public var profilDestinataires:ComboBox;
		
		[Bindable]
		public var typeDiffusion:ComboBox;
		
		[Bindable]
		public var formatSortie:ComboBox;
		
		[Bindable]
		public var templateRapport:ComboBox=new ComboBox;
		
		[Bindable]
		public var isError:Boolean=true;
		
		[Bindable]
		public var isErrorMail:Boolean=false;
		
		[Bindable]
		public var libelleDiffusion:Label;
		
		public function DiffusionImpl()
		{
			super();
		}
		
		/**
		 * instancier les services et abonner les composants aux evenements
		 */
		protected function diffusionCreationCompleteHandler(event:FlexEvent):void
		{
			addEventListener(DiffusionEvent.IDORGA_EVENT, IdOrgaTansferHandle, true);
			
			listeOragnisationClient=new ListeOrgaClientService();
			listeOragnisationClient.model.addEventListener(DiffusionEvent.LISTE_ORGA_CLIENT_EVENT, fillOrgaClient);
			
			listeProfilOrganisation=new ListeProfilsOrgaService();
			listeProfilOrganisation.model.addEventListener(DiffusionEvent.LISTE_PROFILS_ORGA_EVENT, fillProfilOrga);
			
			rechercherSelectorService=new RechercherSelectorService();
			rechercherSelectorService.model.addEventListener(RapportEvent.USER_SELECTOR_UPDATED_EVENT, selectorHandler);
			
			listeEventsService=new ListeEvenementService();
			listeEventsService.model.addEventListener(DiffusionEvent.LISTE_EVENTS_EVENT, fillEvenement);
			listeEventsService.getListeEvenement(); /** chercher les evenements */
			
			corpsMailAppService=new CorpsMailService();
			corpsMailAppService.model.addEventListener(DiffusionEvent.CORPS_MAIL_EVENT, fillCorpsMail);
			corpsMailAppService.getCorpsMailApp(); /** chercher le corps de l'email par application */
			
			formatSelectorService=new FormatTemplateSelectorService();
			formatSelectorService.model.addEventListener(RapportEvent.USER_FORMAT_SELECTOR_EVENT, fillFormatTemplate);
			
			templateRapportService=new TemplateSelectorService();
			templateRapportService.model.addEventListener(RapportEvent.USER_TEMPLATE_SELECTOR_EVENT, fillTemplate);
			
			validerDiffusionRapport=new ValiderDiffusionService();
			validerDiffusionRapport.model.addEventListener(DiffusionEvent.SELECT_CHILD_EVENT, redirigerListeDiffusion);
			
			nbDestinataireService=new NombreDestinataireService();
			nbDestinataireService.model.addEventListener(DiffusionEvent.NB_DESTINATAIRE_EVENT, setNbDestinataire);
			
			typeDiffusion_changeHandler(null); /** build L'IHM lors que on change le type de la diffusion par defaut 16*/
			
			mailLogin=CvAccessManager.getSession().USER.EMAIL; // le mail du l'utilisateur connecté
		}
		
		/**
		 * dispatcher un event pour rediriger vers la liste des diffusion
		 */
		private function redirigerListeDiffusion(evt:DiffusionEvent):void
		{
			dispatchEvent(new DiffusionEvent(DiffusionEvent.SELECT_CHILD_EVENT, true)); // capture par GestionDiffusionRappotImpl
		}
		
		/**
		 * permet d'appeler les services pour récupérer les données du back
		 */
		public function initData(selectedRapport:Rapport):void
		{
			nomRapport=selectedRapport.nomLong;
			idRapport=selectedRapport.id;
			
			rechercherSelectorService.getSelector(idRapport); /** cherhcer la liste des selecteurs du rapport passé en param */
			formatSelectorService.getValuesFormatTemplate(selectedRapport.idRapportRacine, -1); /** chercher format rapport */
			templateRapportService.getValuesTemplate(selectedRapport.idRapportRacine); /** chercher template rapport */
			
			listeOragnisationClient.getListeOrgaClient(); /** chercher la liste des orga */
			listeProfilOrganisation.getListeProfilsOrga(0); /** cherhcer la liste des profils par idOrga (0) par defaut */
			
			objetEmail.text="";
			nomFile.text="";
			mailCopieCache.text=mailLogin;
			lbl_error.text="";
			nbDestinataire.visible=false;
			
			/** Nettoyage des messages d'erreurs de validateur */  
			listeOrgaClient.errorString="";
			mailCopieCache.setStyle("borderColor", "0xAAB3B3");
		}
		
		/**
		 * liste des formats du rapport
		 */
		private function fillFormatTemplate(evt:RapportEvent):void
		{
			formatSortie.dataProvider=formatSelectorService.model.formats;
		}
		
		/**
		 * liste des templates du rapport
		 */
		private function fillTemplate(evt:RapportEvent):void
		{
			templateRapport.dataProvider=templateRapportService.model.templates;
		}
		
		/**
		 * remplir le corps de l'email
		 */
		private function fillCorpsMail(evt:DiffusionEvent):void
		{
			bodyMail.text=corpsMailAppService.model.bodyMail;
		}
		
		/**
		 * mettre à jour la liste des evenements
		 */
		private function fillEvenement(evt:DiffusionEvent):void
		{
			typeDiffusion.dataProvider=listeEventsService.model.events;
			libelleDiffusion.text=(typeDiffusion.selectedItem as Evenement).nomEvent;
		}
		
		/**
		 * permet de disptacher un event lors que l'utilisateur change l'organisation
		 * cet event est capturé par diffusionImpl
		 */
		protected function listeOrgaClient_changeHandler(evt:ListEvent):void
		{
			idOrga=(listeOrgaClient.selectedItem as Orga).idOrga;
			listeOrgaClient.dispatchEvent(new DiffusionEvent(DiffusionEvent.IDORGA_EVENT, true));
			getNbDestinataire();
		}
		
		/**
		 * modifier le nb destinataire d'une orag
		 */
		protected function onChange_getNbProfil(evt:ListEvent):void
		{
			getNbDestinataire();
		}
		
		private function getNbDestinataire():void
		{
			var idProfil:int=(profilDestinataires.selectedItem as Profil).idProfil;
			nbDestinataireService.getNbProfil(idOrga, idProfil);
			nbDestinataire.visible=true;
		}
		
		private function setNbDestinataire(evt:DiffusionEvent):void
		{
			nbProfil=nbDestinataireService.model.nbProfil;
		}
		
		/**
		 * recupérer la liste des profil d'une organisation
		 * Attention : idOrga est initialisé dans la methode  listeOrgaClient_changeHandler ci-dessus
		 * @param evt
		 */
		private function IdOrgaTansferHandle(evt:DiffusionEvent):void
		{
			listeProfilOrganisation.getListeProfilsOrga(idOrga);
		}
		
		/**
		 * mettre a jour la liste des profils
		 * @param evt
		 */
		private function fillProfilOrga(evt:DiffusionEvent):void
		{
			profilDestinataires.dataProvider=listeProfilOrganisation.model.profilsOrga;
		}
		
		/**
		 * permet de mettre à jour la liste des organisations client
		 * @param evt
		 */
		private function fillOrgaClient(evt:DiffusionEvent):void
		{
			listeOrgaClient.dataProvider=listeOragnisationClient.model.organisationsClient;
		}
		
		/**
		 * permet de builder l'IHM par rapport le type de l'event choisi par l'utilisateur
		 * @param evt
		 */
		protected function typeDiffusion_changeHandler(evt:ListEvent):void
		{
			var idEvenement:int=16;
			var simpleFactory:SimpleFactoryEvenement=new SimpleFactoryEvenement();
			
			if (hBoxperiodDiffusion.numChildren > 0) // tester si le hbox a des enfant 
			{
				var child:DisplayObject=hBoxperiodDiffusion.getChildAt(0);
				hBoxperiodDiffusion.removeChildAt(0);
				child=null;
			}
			if (evt != null)
			{
				idEvenement=(typeDiffusion.selectedItem as Evenement).idEvent;
				newComposant=simpleFactory.createComposants(idEvenement);
				hBoxperiodDiffusion.addChild(newComposant);
			}
			else
			{
				newComposant=simpleFactory.createComposants(idEvenement);
				hBoxperiodDiffusion.addChild(newComposant);
				hBoxperiodDiffusion.includeInLayout=true;
			}
			this.selectorHandler(null);
		}
		
		/**
		 * récupérer la liste des selecteur du rapport et
		 * savoir si le rapport a un selecteur de type top, PeriodeSelector,MonoPeriodeSelector
		 */
		public function selectorHandler(event:RapportEvent):void
		{
			allSelector=rechercherSelectorService.model.selectors; // liste des selecteur du rapport
			var param:Object=new Object();
			param.text="";
			param.periodeTraite=1;/** pour afficher le combobox de la de periode traité dans l'IHM*/
			
			for (var i:int=0; i < allSelector.length; i++)
			{
				var typeSelector:String=(allSelector[i] as SelectorParametre).typeSelecteur;
				
				switch (typeSelector) // pour mettre à jour le contenu de l'ihm
				{
					case 'TopSelector':
						nbTop=100;
						break;
					case 'PeriodeSelector':
						param.text=ResourceManager.getInstance().getString('M331', '12_mois');
						param.id=1;
						param.jLancementRapport=1;
						param.isPeriodTwoCursor=true;
						newComposant.setValues(param);
						break;
					case 'MonoPeriodeSelector':
						param.text=ResourceManager.getInstance().getString('M331', '1_mois');
						param.id=1;
						param.jLancementRapport=1;
						param.isPeriodTwoCursor=false;
						newComposant.setValues(param);
						break;
					case 'PeriodeComboBoxSelector':
						param.text=ResourceManager.getInstance().getString('M331', '12_mois');
						param.id=12;
						param.jLancementRapport=1;
						param.isPeriodTwoCursor=false;
						newComposant.setValues(param);
						break;
					default:
						trace(' ne tiens pas compte de ce selecteur');
						break;
				}
			}
			if (param.text == "") /** permet de donner des valuers par défaurt == cas gestion du parc ( selecteurs de periodes invisible) */
			{
				param.text=ResourceManager.getInstance().getString('M331', '12_mois');
				param.id=12;
				param.jLancementRapport=1;
				param.periodeTraite=0; /** pour ne pas afficher le combobox de la fin de periode traité dans l'IHM  */
				newComposant.setValues(param);
			}
		}
		
		/**
		 * le button retour dispatch un event permettant de retourner à l'etape1
		 * @param event
		 */
		public function onClick_retour(event:MouseEvent):void
		{
			dispatchEvent(new DiffusionEvent(DiffusionEvent.BACK_CLICK_EVENT)); // cette event est captué par le nouvelleDiffusion
		}
		
		/**
		 * le button retour dispatch un event permettant de retourner à l'etape1 et d'initialiser l'IHM
		 */
		public function onClick_annuler(event:MouseEvent):void
		{
			dispatchEvent(new DiffusionEvent(DiffusionEvent.CANCEL_CLICK_EVENT)); // cette event est captué par le nouvelleDiffusion
		}
		
		public function initialIHM():void
		{
			onClick_annuler(null);
		}
		
		/**
		 * valider si les mails sont corrects et séparés par,
		 */
		public function doValidationMail():void
		{
			var validator:CompositeEmailValidator=new CompositeEmailValidator();
			var result:Boolean=true;
			
			if (mailCopieCache.text != "")
			{
				result=validator.doValidationMail(mailCopieCache.text);
			}
			if (!result)
			{
				mailCopieCache.setStyle("borderColor", "red");
				focusManager.setFocus(mailCopieCache); // mettre le fouce dans l'inputText
				lbl_error.text=ResourceManager.getInstance().getString('M331', 'Adresse_destinataire_incorrecte_');
				isErrorMail=true;
			}
			else
			{
				mailCopieCache.setStyle("borderColor", "0xAAB3B3");
				lbl_error.text="";
				isErrorMail=false;
			}
		}
		
		/**
		 * verifier si les validateurs sont corrects
		 */
		public function doValidation():void
		{
			var validationResult:Array=Validator.validateAll([objetMailValidator, nomFileValidator, listeOrgaClientValidator]);
			
			if ((validationResult.length == 0) && listeOrgaClient.selectedIndex > -1) // la methode validateAll renvoi un tableau vide si tout est ok
			{
				isError=false;
				listeOrgaClient.errorString="";
			}
			else if (listeOrgaClient.selectedIndex == -1)
			{
				isError=true;
				listeOrgaClient.errorString=ResourceManager.getInstance().getString('M331', 'Le_choix_de_votre_organisation_est_oblig');
			}
			else if (validationResult.length > 0)
			{
				isError=true;
			}
		}
		
		/**
		 * permet de valider et creer une diffusion
		 */
		public function onClick_valider():void
		{
			doValidation();
			doValidationMail();
			
			if (!isError && !isErrorMail) // si pas d'erreur
			{
				var idEvent:int=(typeDiffusion.selectedItem as Evenement).idEvent;
				var idProfil:int=(profilDestinataires.selectedItem as Profil).idProfil;
				var destinatairesCaches:String=mailCopieCache.text;
				var objetMail:String=objetEmail.text;
				var nomAttachementFile:String=nomFile.text;
				var corpsMail:String=bodyMail.text;
				var idFormatSortieRapport:int=(formatSortie.selectedItem as ValeurSelector).id;
				var idTemplate:int=(templateRapport.selectedItem as ValeurSelector).id;
				var valeurTemplate:String=(templateRapport.selectedItem as ValeurSelector).valeur;
				var valuesEventIHM:Object=newComposant.getValues(); // pour récupérer les valeurs des composants de Evenement16IHM
				
				validerDiffusionRapport.validerDiffusion(idRapport, idEvent, idOrga, idProfil, nbTop, valuesEventIHM.idPeriodFin, valuesEventIHM.idPeriodeEtude, valuesEventIHM.jourLancement, destinatairesCaches, objetMail, nomAttachementFile, corpsMail, valeurTemplate, idFormatSortieRapport);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M331', 'Veuillez_saisir_les_champs_obligatoires'), ResourceManager.getInstance().getString('M331', 'Erreur_validation'));
			}
		}
	}
}


