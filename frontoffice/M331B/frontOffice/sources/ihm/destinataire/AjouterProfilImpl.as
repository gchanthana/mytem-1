package ihm.destinataire
{
	import flash.events.Event;
	import flash.events.MouseEvent;

	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.validators.StringValidator;
	import mx.validators.Validator;

	import service.destinataire.ajoutermodifierprofil.AjouterModifierProfilService;

	import vo.destinataire.Profil;

	public class AjouterProfilImpl extends TitleWindow
	{
		public var btAnnuler:Button;
		[Bindable]public var btValider:Button;

		private var id_profil:Number=0; // l'id du profil = 0 lors de l'ajout sinon = profil.idProfil

		[Bindable]
		public var txtLibelle:TextInput;
		public var txtCodeInterne:TextInput;
		public var txtCommentaire:TextArea;

		public var validerLibelleProfil:StringValidator;
		[Bindable]
		public var isOk:Boolean=false;

		public var actionAExecuter:String='A'; /** A : ajouter ; M : Modifier*/

		public var ajoutProfilService:AjouterModifierProfilService;

		public function AjouterProfilImpl()
		{
			ajoutProfilService=new AjouterModifierProfilService();
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		private function initIHM(evt:Event):void
		{
			addEventListener(CloseEvent.CLOSE, closeWindow);
			btAnnuler.addEventListener(MouseEvent.CLICK, closeWindow);
		}

		/**
		 * permet d'ajouter un profil
		 * @param e
		 */
		protected function btValid_ClickHandler(e:MouseEvent):void
		{
			var newProfil:Profil=new Profil();

			newProfil.idProfil=id_profil;
			newProfil.nomProfil=txtLibelle.text;
			newProfil.codeInterne=txtCodeInterne.text;
			newProfil.commentaire=txtCommentaire.text;

			if (actionAExecuter == 'A') // ajout d'un profil
			{
				ajoutProfilService.ajouterProfil(newProfil);
			}
			else
			{
				ajoutProfilService.modifierProfil(newProfil);
			}
		}

		/**
		 * permet de valider le libelle du profil et activer , désactiver le button valider
		 */
		public function validateLibelleProfil():void
		{
			var validationResult:Array=Validator.validateAll([validerLibelleProfil]);

			if (validationResult.length == 0) // la methode validateAll renvoi un tableau vide si tout est ok
			{
				btValider.enabled=true;
			}
			else
			{
				btValider.enabled=false;
			}
		}

		/**
		 * permet d'initialiser le fromulaire lors de la modification d'un profil
		 * @param profil
		 */
		public function initForm(profil:Profil):void
		{
			id_profil=profil.idProfil;
			txtLibelle.text=profil.nomProfil;
			txtCodeInterne.text=profil.codeInterne;
			txtCommentaire.text=profil.commentaire;
		}

		/**
		 * permet de fermer le popup
		 */
		public function closeWindow(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}
	}
}
