package composants.pagindatagrid
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.Tile;
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Spacer;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import composants.com.hillelcoren.utils.ArrayCollectionUtils;
	import composants.pagindatagrid.columns.event.StandardColonneEvent;
	import composants.pagindatagrid.columns.stantardcolumns.SelectPaginateDatagridColumn;
	import composants.pagindatagrid.event.PaginateDatagridEvent;
	import composants.pagindatagrid.pagination.Pagination;
	import composants.pagindatagrid.pagination.event.PaginationEvent;
	import composants.pagindatagrid.pagination.vo.ItemIntervalleVO;
	import composants.pagindatagrid.util.UtilPaginateDatagrid;
	
	
	 /**
	 * Lire la documentation de ce composant : testtrack
	 * Pour Personnaliser Flex builder 3 pour ce composant : 
	 * http://www.iteratif.fr/blog/index.php?post/2008/12/12/Personnaliser-les-composants-dans-Flex-builder-3
	 * @author Vincent Le Gallic 
	 * @version : 1.2
	 * @dateLasteVersion : 21/05/2010
	 * @datecreation : 4/12/2009
	 * 
	 */	 
	[Event(name="onIntervalleChange", type="composants.pagindatagrid.event.PaginateDatagridEvent")]
	[Event(name="onItemEdit", type="composants.pagindatagrid.columns.event.StandardColonneEvent")]
	[Event(name="onItemRemove", type="composants.pagindatagrid.columns.event.StandardColonneEvent")]
	[Event(name="onItemCheckBoxChange", type="composants.pagindatagrid.columns.event.StandardColonneEvent")]
	[Event(name="onSelectAllChange", type="composants.pagindatagrid.event.PaginateDatagridEvent")]
	[Event(name="onItemSelected", type="composants.pagindatagrid.event.PaginateDatagridEvent")]
	[Event(name="onBTExportPressed", type="composants.pagindatagrid.event.PaginateDatagridEvent")]
	[Bindable]
	public class PaginateDatagridControleur extends VBox
	{ 
		
		//Propriété Custom :
		private var _buttons : Array;
		private var _nbTotalItem : int;
		private var _nbItemParPage : int;
		private var _itemIntervalleSelected : ItemIntervalleVO;
		private var _allowSelection : Boolean = false;
		private var _allowExport : Boolean = false;
		private var _currentIntervalle : ItemIntervalleVO;
		private var _widthBoxBT : int;
		private var _enableHScroll:Boolean;
		private var _idSelectUnique:String;
		
		//Propriété du datagrid :
		private var _columns : Array;
		private var _comboBoxActions : ComboBox;
		private var _selectedItem:Object;
		private var _selectedIndex : int;
		private var _selectedItems:ArrayCollection = new ArrayCollection;
		private var _dataprovider : ArrayCollection = new ArrayCollection();
		
		//Public UI COMPONANT 
		public var dgPaginate : DataGrid;
		public var paginationBox : Pagination;
		public var txtFiltre : TextInput;
		public var cbAll : CheckBox;
		public var boxButtons : Tile;
		public var boxComboBox : HBox;
		public var utilSpacer : Spacer;
		private var _tempListeSelected:ArrayCollection=new ArrayCollection();
		private var _copyTempListeSelecd:ArrayCollection = new ArrayCollection();
		
		public var exportListeCommande:Boolean = false;
				
		public var listeExport:ArrayCollection = new ArrayCollection([
																		{label:ResourceManager.getInstance().getString('M16','Export_d_taill__de_la_s_l_ction'), data:1},
																		{label:ResourceManager.getInstance().getString('M16','Export_d_taill__de_toutes_les_pages'), data:0},
																		{label:ResourceManager.getInstance().getString('M16','Export_non_d_taill__de_la_s_l_ction'), data:2},
																		{label:ResourceManager.getInstance().getString('M16','Export_de_masse_des_references'), data:3},
																		{label:ResourceManager.getInstance().getString('M16','Import_de_masse_des_references'), data:4}
																	]);
		
		public var chxSelectAll		:CheckBox;
		public var cbxExport		:ComboBox;
		public var isAutorised		:Boolean = false;
		public var compteurSelected	:int = 0;
		
		public function PaginateDatagridControleur() 
		{
			addEventListener(PaginationEvent.INTERVALLE_SELECTED_CHANGE,intervalleChangeHandler);
		}
		
		public function setNumberSeleted(mycptr:int, nbTot:int, nbItemByPage:int):void
		{
			compteurSelected = mycptr;
			nbTotalItem 	 = nbTot;
			nbItemParPage	 = nbItemByPage;
		}
		
		public function creation_complete_handler(evt : FlexEvent):void
		{
			//dgPaginate.selectedItems
			dgPaginate.columns = _columns;
			dgPaginate.validateNow();
			dgPaginate.addEventListener(Event.CHANGE,dgPaginateHandler);
			dgPaginate.addEventListener(ListEvent.ITEM_CLICK,dgClickHandler);
			
			updateBoxButtons();
			updateBoxCombo();	
		}
		/**
		 * 
		 * dataChangeHandler
		 * Si la colonne selection est affichée alors //Vérification que la data est correctement formattée avec SELECTED en propriété
		 * Verification que NBRECORD existe
		 * appel de utilPaginateDatagrid.formatdata qui met à jour la collection de selectedItems si certains items on leur propriété SELECTED à true
		 * Maj du dataprovider du datagrid
		 * Initialisation de la pagination avec la nouvelle data affichée
		 *  
		 */
		private function dataChangeHandler(evt : CollectionEvent):void
		{
			if(allowSelection)
			{
				//todo vérifier le type boolean
				if(dataprovider.length>0){
					if((!dataprovider.getItemAt(0).hasOwnProperty('SELECTED')) && allowSelection)
					{
						throw  new Error('COMPOSANT PAGINATE DATAGRID ERROR : \nSelectPaginateDatagridColumn est utilisé sans la proprité SELECTED \nRetirez la colonne SelectPaginateDatagridColumn ou ajoutez la propriété SELECTED à la data');
					}
				}
				var utilPaginateDatagrid : UtilPaginateDatagrid = new UtilPaginateDatagrid();
				utilPaginateDatagrid.formatdata(dataprovider,selectedItems);
			}
			if(dataprovider.source[0])
			{
				if(!dataprovider.source[0].hasOwnProperty('NBRECORD'))
				{
					throw  new Error('COMPOSANT PAGINATE DATAGRID ERROR : \nLa propriété NBRECORD est obligatoire sur l objet dataprovider[0] \nNBRECORD est le nombre total d enregistrements correspondant aux critères de recherche');
				}
				nbTotalItem = dataprovider.source[0].NBRECORD;
			}
			paginationBox.initPagination(nbTotalItem,nbItemParPage);
		}
		/**
		 * 
		 * TODO POUR LA V2: VERIFIER SI L INTERVALLE EXISTE DANS LE CACHE : V2 paginate-Dg-Composant
		 * La gestion du cache est à implémenter dans intervalleChangeHandler
		 * Si dans le cache -> ne pas propager d'event mais afficher le contenue du cache
		 *
		 */
		private function intervalleChangeHandler(evt : PaginationEvent):void
		{
			copyTempListeSelecd = ObjectUtil.copy(tempListeSelected) as ArrayCollection;
			tempListeSelected.refresh();
			refreshPaginateDatagrid();
			currentIntervalle = evt.itemIntervalleVO;
			dispatchEvent(new PaginateDatagridEvent(PaginateDatagridEvent.PAGE_CHANGE,evt.itemIntervalleVO,true));
			
		}
		/**
		 * 
		 * Event : Avant que la donnée change : On remet le visuelle a l'état initial
		 * On décoche la case selectAll
		 * On vide la collection d'item selectionné
		 * On remet le selected Item a null
		 * On selctionne l'intervalle 1
		 */
		public function refreshPaginateDatagrid (initCombo : Boolean = false):void
		{
			selectedItems = new ArrayCollection();
			cbAll.selected = false;
			selectedItem = null;
			selectedIndex  = -1;
			
			if(initCombo){
				nbTotalItem = 0;
				paginationBox.myIndex = 0;
				paginationBox.initPagination(nbTotalItem,nbItemParPage);
			}
		}
		
		/**
		 * mettre le nombre de commandes séléctionnées à Zero
		 */
		public function refreshTempListeSelected():void
		{
			copyTempListeSelecd = ObjectUtil.copy(tempListeSelected) as ArrayCollection;
			tempListeSelected.source = [];
		}
		/**
		 *
		 * @param : arrColonne liste des boutons qui serront affichées au dessus du DG
		 * */
		public function set buttons(arrColonne : Array):void
		{
			this._buttons = arrColonne;
			updateBoxButtons();
		}
		private function updateBoxButtons():void
		{
			if(boxButtons && buttons){
				boxButtons.visible = false;				
				for(var i:int=0;i<buttons.length;i++)
				{
					boxButtons.addChild(buttons[i]);
				}
				callLater(placeBoxButton);			
			}
			else
			{
				if(utilSpacer)
				{
					utilSpacer.percentWidth = 100;
				}
			}
		}
		private function placeBoxButton():void
		{
			boxButtons.visible = true;		
			utilSpacer.percentWidth = 100;
		}
		private function updateBoxCombo():void
		{
			if(boxComboBox && comboBoxActions)
			{
				boxComboBox.addChildAt(comboBoxActions,1);
				allowExport = true;
			}
			else
			{
				allowExport = false;
			}
			
		}
		/**
		 * Si dans la liste des colonnes passées en parametres il existe une colonne de type SelectPaginateDatagridColumn
		 * alors on autorise la selection ALL avec la CheckBox
		 * @param : arrColonne liste des colonnes qui serront affichées dans le dg
		 * */
		public function set columns(arrColonne : Array):void
		{
			this._columns = arrColonne;
			for(var i:int=0;i<_columns.length;i++)
			{
				if(this._columns[i] is SelectPaginateDatagridColumn)
				{
					_allowSelection = true;
					break;
				}
			}
		}
		private function dgPaginateHandler(evt : Event=null):void
		{
			if(dgPaginate.selectedItem){
				//Force la mise à jour des données du composant -> on peut binder un autre composant la propriété selectedIndex du paginateDatagrid
				selectedItem = dgPaginate.selectedItem;
				selectedIndex = dgPaginate.selectedIndex;
				dispatchEvent(new PaginateDatagridEvent(PaginateDatagridEvent.ON_ITEM_SELECTED,itemIntervalleSelected));
		
			}
		}
		public function dgClickHandler(evt : Event):void
		{
				selectedItem.SELECTED  = !selectedItem.SELECTED;
				dataprovider.itemUpdated(selectedItem);
				var propertyNameUnique:String = '';
				
				if(selectedItem.SELECTED)
				{
					compteurSelected ++;
					if(selectedItem.hasOwnProperty(idSelectUnique))
					{
						if(!elementExists(selectedItem, tempListeSelected, idSelectUnique))
							tempListeSelected.addItem(dgPaginate.selectedItem)						
					}
				}
				else
				{
					compteurSelected --;
					if(selectedItem.hasOwnProperty(idSelectUnique))
						tempListeSelected.source = this.removeElement(tempListeSelected.source, idSelectUnique, selectedItem[idSelectUnique]);
				}
				
				tempListeSelected.refresh();
				if (compteurSelected == dataprovider.length)
					chxSelectAll.selected=true;
				else
					chxSelectAll.selected=false;
		}
		
		/**
		 * Supprime un élement d'un tableau et renvoi le nouveau tableau
		 */
		public function removeElement(array: Array, propertyName: String, value: String): Array
		{
			var newArray: Array = [];
			
			for (var i: int = 0; i < array.length; i++) {
				var item: Object = array[i];
				
				if (item && item.hasOwnProperty(propertyName)) {
					if (item[propertyName] != value)
						newArray.push(item);
				}
			}
			return newArray;
		}
			
		protected function filtreHandler(e:Event):void{
			dataprovider.filterFunction= filtreGrid;
			dataprovider.refresh();
		}
		/**
		 * appelée par filterFunction lorsque txtFiltre change
		 * Cette fonction recherche dans toutes les colonnes d'un dg qui ont un dataField != null le texte entré dans txtFiltre
		 * @param : item est un objet du dataprovider. 
		 *  
		 * */
		private function filtreGrid(item:Object):Boolean
		{
			for(var i:int=0;i<columns.length;i++)
			{
				if(columns[i].dataField != null){
					var propriete : String = (columns[i] as DataGridColumn).dataField;			
					if(item[propriete]){
						
						if (((item[propriete].toString()).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1)
						{
							return true;
						}
					}
				}
			}
			return false;
		}
		public function btExportHandler(event:MouseEvent):void
		{
			dispatchEvent(new PaginateDatagridEvent(PaginateDatagridEvent.ON_BT_EXPORT_PRESSED,itemIntervalleSelected));
		}
		
		public function chxSelectAllClickHandler(event:MouseEvent):void
		{
			if(dataprovider == null) return;
			
			var bool:Boolean = chxSelectAll.selected;
			var len	:int = dataprovider.length;
		
			for(var i:int = 0;i < len;i++)
			{
				dataprovider[i].SELECTED = bool;
				if(dataprovider[i].hasOwnProperty(idSelectUnique))
				{
					if(bool)
					{
						if(! elementExists(dataprovider[i], tempListeSelected, idSelectUnique))
							tempListeSelected.addItem(dataprovider[i])
					}
					else
					{
						if(elementExists(dataprovider[i], tempListeSelected, idSelectUnique))
							tempListeSelected.source = this.removeElement(tempListeSelected.source, idSelectUnique, dataprovider[i][idSelectUnique]);
					}
				}
				
				tempListeSelected.refresh();
			}
			
			if (bool) /** affichage du nombre d'elements selectionnes */
			{
				compteurSelected=len;
			}
			else
			{
				compteurSelected=0;
			}
		}
	
		private function elementExists(element:Object, elements:ArrayCollection, propertyName:String):Boolean
		{
			var trouv:Boolean = false;
			for each (var elet:Object in elements) 
			{
				if(elet[propertyName] == element[propertyName]){
					trouv = true;
					break;
				}
			}
			return trouv;
		}
		
		public function btnExportClickHandler(event:MouseEvent):void
		{
			dispatchEvent(new Event('BUTTON_EXPORT_CLICKED', true));
		}
		
		protected function cbxExportChangeHandler(le:ListEvent):void
		{
			if(cbxExport.selectedItem == null)
				cbxExport.selectedIndex = 0;
			else
			{
				if(cbxExport.selectedItem.data == 0)
				{
					chxSelectAll.selected = chxSelectAll.enabled = false;
					
					unselectCommande();
					
					compteurSelected = 0;
				}
				else
					chxSelectAll.enabled = true;
			}
		}
		
		private function unselectCommande():void
		{
			var len:int = dataprovider.length;
			
			for(var i:int = 0;i < len;i++)
			{
				dataprovider[i].SELECTED = false;
			}
		}
		
		protected function btnCatalogueClickhandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('CLICKED_VIEW_CATALOGUE', true));
		}
		
		protected function btnHistoriqueClickhandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('CLICKED_VIEW_HISTORIQUE', true));
		}
		
		protected function btnCommandeClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('CLICKED_VIEW_COMMANDE', true));
		}
		
		
		public function cbAllHandler(evt : Event):void
		{
			var utilPaginateDatagrid : UtilPaginateDatagrid = new UtilPaginateDatagrid();
			utilPaginateDatagrid.onCBAllChanged(dataprovider,selectedItems,cbAll.selected);
			dispatchEvent(new PaginateDatagridEvent(PaginateDatagridEvent.ON_SELECT_ALL_CHANGE,itemIntervalleSelected));
		}
		public function majTempListeSelected(listeInterval:ArrayCollection):void
		{
			for each(var tempv:Object in tempListeSelected)
			{
				for each (var cmd:Object in listeInterval) 
				{
					if(tempv.IDCOMMANDE == cmd.IDCOMMANDE )
						cmd.SELECTED = true;
				}
			}
		}
		
		public function get dataprovider():ArrayCollection
		{
			return this._dataprovider;
		}
		public function set dataprovider(valueData : ArrayCollection):void
		{
			this._dataprovider = valueData;
			_dataprovider.addEventListener(CollectionEvent.COLLECTION_CHANGE,dataChangeHandler);
		
		}
		public function get nbTotalItem():int
		{
			return this._nbTotalItem;
		}
		private function set nbTotalItem(nbTotalItem : int):void
		{
			this._nbTotalItem = nbTotalItem ;
		}
		public function get nbItemParPage():int
		{
			return this._nbItemParPage;
		}
		public function set nbItemParPage(nbItemParPage : int):void
		{
			this._nbItemParPage = nbItemParPage ;
		}
		public function get itemIntervalleSelected():ItemIntervalleVO
		{
			return this._itemIntervalleSelected;
		}
		public function set itemIntervalleSelected(itemIntervalleSelected : ItemIntervalleVO):void
		{
			this._itemIntervalleSelected = itemIntervalleSelected;
		}
		public function set selectedItems(value:ArrayCollection):void
		{
			_selectedItems = value;
		}
		public function get selectedItems():ArrayCollection
		{
			return _selectedItems;
		}
		public function set currentIntervalle(value: ItemIntervalleVO):void
		{
			this._currentIntervalle = value;
		}
		public function get currentIntervalle():ItemIntervalleVO
		{
			return this._currentIntervalle;
		}
		public function get allowSelection():Boolean
		{
			return _allowSelection;
		}
		
		internal function set allowSelection(allowSelection : Boolean):void
		{
			this._allowSelection = allowSelection;
		}
		public function get allowExport():Boolean
		{
			return _allowExport;
		}
		public function set allowExport(allowExport : Boolean):void
		{
			this._allowExport = allowExport;
		}
		public function get columns():Array
		{
			return _columns;
		}
		public function get buttons():Array
		{
			return _buttons;
		}
		public function set selectedItem(value: Object):void
		{
			this._selectedItem = value;
			//dgPaginate.selectedItem = _selectedItem;
		}
		public function get selectedItem():Object
		{
			return _selectedItem;
		} 
		public function set selectedIndex(value: int):void
		{
			this._selectedIndex = value;
			dgPaginate.selectedIndex = _selectedIndex;
		}
		public function get selectedIndex():int
		{
			return _selectedIndex;
		}
		public function set comboBoxActions(value: ComboBox):void
		{
			this._comboBoxActions = value;
			updateBoxCombo();
		}
		public function get comboBoxActions():ComboBox
		{
			return _comboBoxActions
		}

		public function get enableHScroll():Boolean
		{
			return _enableHScroll;
		}

		public function set enableHScroll(value:Boolean):void
		{
			_enableHScroll = value;
		}
		
		public function get tempListeSelected():ArrayCollection { return _tempListeSelected; }
		
		public function set tempListeSelected(value:ArrayCollection):void
		{
			if (_tempListeSelected == value)
				return;
			_tempListeSelected = value;
		}
		
		// ID_UNIQUE utilisé pour vérifier la sélection ou non d'un élément de la liste paginé.
		// La sélection sur les pages de la liste paginé entraine l'utilisation de deux propriété de vérification: SELECTED, ID_UNIQUE 
		public function get idSelectUnique():String { return _idSelectUnique; }
		
		public function set idSelectUnique(value:String):void
		{
			if (_idSelectUnique == value)
				return;
			_idSelectUnique = value;
		}
		
		public function get copyTempListeSelecd():ArrayCollection { return _copyTempListeSelecd; }
		
		public function set copyTempListeSelecd(value:ArrayCollection):void
		{
			if (_copyTempListeSelecd == value)
				return;
			_copyTempListeSelecd = value;
		}

	}
}