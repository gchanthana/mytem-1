package composants.pagindatagrid.columns
{
	import mx.controls.dataGridClasses.DataGridColumn;

	public class SortableColumn extends DataGridColumn
	{
		private var _columDBName : String;
		
		public function SortableColumn(columnName:String=null)
		{
			super(columnName);
		}
		
		public function get columDBName():String
		{
			return _columDBName;	
		}
		
		public function set columDBName(columDBName : String):void
		{
			this._columDBName = columDBName;
			
		}
	}
}