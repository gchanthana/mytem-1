package composants.mail.gabarits
{
	import mx.resources.ResourceManager;
	
	public class GabaritBlank extends AbstractGabarit
	{
		public function GabaritBlank()
		{
			super();
		}
		
		
		
		
		
		
		override public function getGabarit():String{
			return	ResourceManager.getInstance().getString('M16', '_p_Soci_t_____b_')+infos.SOCIETE_EXPEDITEUR+"</b></p>"					
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M16', '_p_Bonjour___p_')	
					+"<br/>"				
					+"<br/>"
					+"<br/>"
					+"<br/>"
					+"<br/>"
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M16', '_p_Cordialement___p_')
					+"<br/>"
					+"<br/><b>"+infos.PRENOM_EXPEDITEUR+" "+infos.NOM_EXPEDITEUR+".</b>"
					+"<br/><b>"+infos.MAIL_EXPEDITEUR+".</b>"
					+"<br>"
		}
	}
}
