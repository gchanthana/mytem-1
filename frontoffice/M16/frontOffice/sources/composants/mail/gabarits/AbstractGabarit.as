package composants.mail.gabarits
{
	public class AbstractGabarit
	{
		public static const TYPE_COMMANDE_FIXEDATE 		:String = "CommandeFixeData";
		public static const TYPE_COMMANDE_MOBILE 		:String = "CommandeMobile";
		public static const TYPE_RESILIATION_FIXEDATE 	:String = "ResiliationFixeData";
		public static const TYPE_MVT_SOUSTETE 			:String = "MouvementDeSousTete";
		public static const BLANK 						:String = "Blank";
		
		private var _infos 								:InfosObject;
		
		public function getGabarit():String
		{
			return "";
		}
		
		public function get infos():InfosObject
		{
			return _infos;
		}
		
		public function set infos(myInfos:InfosObject):void
		{
			_infos = myInfos;
		}
		
	}
}