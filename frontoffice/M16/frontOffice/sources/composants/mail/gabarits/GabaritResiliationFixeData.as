package composants.mail.gabarits
{
	import gestioncommande.entity.Commande;
	import commandemobile.system.Contact;
	import commandemobile.system.OperationVO;
	import gestioncommande.entity.TypesCommandesMobile;
	
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.net.registerClassAlias;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;

	public class GabaritResiliationFixeData extends AbstractGabarit
	{
			 
		public var _commande		: Commande;
		private var _listeProduits 	: ArrayCollection;
		private var _contact 		: Contact;
		private var _operation 		: OperationVO;
		
		public function GabaritResiliationFixeData()
		{
			super();
		}
		
		override public function getGabarit():String{
			
			registerClassAlias("commandemobile.entity",Commande);
			_commande = infos.commande as Commande;
			
			var listeLignes : String = "";
			
//			if (_listeProduits != null)
//				listeLignes = printListeSousTete(_listeProduits.source);

			var cnom 			: String = (_commande.PATRONYME_CONTACT != null )? _commande.PATRONYME_CONTACT : "";
			var cprenom			: String = (_commande.PATRONYME_CONTACT != null )? _commande.PATRONYME_CONTACT : "";
			var cemail			: String = (_commande.EMAIL_CONTACT!=null)?_commande.EMAIL_CONTACT:"";
			var csociete 		: String = (_commande.LIBELLE_REVENDEUR !=null)?_commande.LIBELLE_REVENDEUR:"";
			var refclient 		: String = (_commande.REF_CLIENT1!=null)?_commande.REF_CLIENT1:"";
			var refclient2 		: String = (_commande.REF_CLIENT2!=null)?_commande.REF_CLIENT2:"";
			var refOperateur 	: String = (_commande.REF_OPERATEUR!=null)?_commande.REF_OPERATEUR:"";
			var compte 			: String = (_commande.LIBELLE_COMPTE!=null)?_commande.LIBELLE_COMPTE:"";
			var sousCompte 		: String = (_commande.LIBELLE_SOUSCOMPTE!=null)?_commande.LIBELLE_SOUSCOMPTE:"";
			var corpMessage 	: String = (infos.corpMessage!=null)?infos.corpMessage:"";
			var commentaires 	: String = (_commande.COMMENTAIRES!=null)?_commande.COMMENTAIRES:" ";
			
			var typecmde		: String = 'Objet : Demande de ' + typeCommande(_commande.IDTYPE_COMMANDE);

			return	"<br/>"
					+typecmde
					+"<br/>"+"<br/>"
					+"<b>"+infos.PRENOM_EXPEDITEUR+" "+infos.NOM_EXPEDITEUR +"</b>"
					+"<br/>"+infos.MAIL_EXPEDITEUR
					+ResourceManager.getInstance().getString('M16', '_br___u_Date__u____')+DateFunction.formatDateAsString(new Date())
					+ResourceManager.getInstance().getString('M16', '_br__Soci_t_____b_')+infos.SOCIETE_EXPEDITEUR+"</b>"
					+ "<br/><br/>" +										
					ResourceManager.getInstance().getString('M16', '_br___Contact') + cemail + "</b>"
					+ "<br/><br/>" +
					ResourceManager.getInstance().getString('M16', '_br__Libell__de_la_demande__b_') + _commande.TYPE_OPERATION + "</b>" +
					ResourceManager.getInstance().getString('M16', '_br__R_f_rence_client____b_') + refclient + "</b>" +
					ResourceManager.getInstance().getString('M16', '_br__R_f_rence_op_rateur____b_') + refOperateur + "</b>" +
					ResourceManager.getInstance().getString('M16', '_br__Remarque____b_') + commentaires + "</b>" +
					ResourceManager.getInstance().getString('M16', '_br__Date_de_la_demande____b_') + DateFunction.formatDateAsString(_commande.DATE_CREATE)+"</b>"
					+ "<br/><br/><br/>" +
					ResourceManager.getInstance().getString('M16', 'Bonjour_')
					+ "<br/><br/>" +
					ResourceManager.getInstance().getString('M16', 'Je_vous_confirme_avoir_valid__la_demande_de_')
					+ typeCommande(_commande.IDTYPE_COMMANDE) +
					ResourceManager.getInstance().getString('M16', 'ci_jointe_')
					+ "<br/>" +
					ResourceManager.getInstance().getString('M16', 'Merci_de_bien_vouloir_la_prendre_en_compte_')
					+ "<br/><br/>" +
					ResourceManager.getInstance().getString('M16', 'En_vous_remerciant_par_avance_')	
					+ "<br/><br/><br/>" +
					ResourceManager.getInstance().getString('M16', 'Cordialement_')
					+ "<br/><br/>"	
					+ "<b>" + infos.PRENOM_EXPEDITEUR + " " + infos.NOM_EXPEDITEUR +".</b>"
		}
					
		private function typeCommande(ID:int):String
		{
			var typeOperation:String = "";
			
			switch(ID)
			{
				case TypesCommandesMobile.MODIFICATION_OPTIONS: typeOperation = ResourceManager.getInstance().getString('M16', 'Modifier');			break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	typeOperation = ResourceManager.getInstance().getString('M16', 'Modifier');			break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			typeOperation = ResourceManager.getInstance().getString('M16', 'R_siliation'); 		break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	typeOperation = ResourceManager.getInstance().getString('M16', 'R_siliation'); 		break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		typeOperation = ResourceManager.getInstance().getString('M16', 'Renouvellement'); 	break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	typeOperation = ResourceManager.getInstance().getString('M16', 'Renouvellement'); 	break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			typeOperation = ResourceManager.getInstance().getString('M16', 'Suspension'); 		break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		typeOperation = ResourceManager.getInstance().getString('M16', 'Suspension'); 		break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		typeOperation = ResourceManager.getInstance().getString('M16', 'R_activation'); 	break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	typeOperation = ResourceManager.getInstance().getString('M16', 'R_activation'); 	break;//--- REACT
			}
			
			return typeOperation + " ";
		}
		
		protected function printListeSousTete(liste : Array):String{
			var output : String = "<br/>";
			if (liste != null)
			{
				for (var i:Number=0; i<liste.length;i++)
				{
					var ligne 		:Object = liste[i];
					var compte 		:String = ligne.COMPTE != null ? ligne.COMPTE  : " ";
					var sousCompte 	:String = ligne.SOUS_COMPTE != null ? ligne.SOUS_COMPTE :" ";
					var produit 	:String = ligne.LIBELLE_PRODUIT;
					var numligne 	:String = ligne.SOUS_TETE;
					
										
					output = output + ResourceManager.getInstance().getString('M16', '_li_Ligne___b_')+ ConsoviewFormatter.formatPhoneNumber(numligne) 
									+  ResourceManager.getInstance().getString('M16', '__b__t_t___Compte___b_') 
									+ compte 
									+ ResourceManager.getInstance().getString('M16', '__b__t_t____Sous_compte___b_') 
									+  sousCompte
									+ ResourceManager.getInstance().getString('M16', '__b__t_t____Produit___b_')
									+  produit
									+ "</b></li>";
				}
			}
			return output;
		}
	}
}
