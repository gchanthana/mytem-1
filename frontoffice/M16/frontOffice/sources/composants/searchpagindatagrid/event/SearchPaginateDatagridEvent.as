package composants.searchpagindatagrid.event
{
	import flash.events.Event;
	
	import composants.searchpagindatagrid.ParametresRechercheVO;
	
	public class SearchPaginateDatagridEvent extends Event
	{
	
		public static const SEARCH:String = 'onSearch';
		
		public var parametresRechercheVO:ParametresRechercheVO;
		
		public function SearchPaginateDatagridEvent(type : String,parametresRechercheVO : ParametresRechercheVO, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.parametresRechercheVO = parametresRechercheVO;
		}
		
		override public function clone():Event
		{
			return new  SearchPaginateDatagridEvent(type,parametresRechercheVO,bubbles,cancelable);
		}

	}
}