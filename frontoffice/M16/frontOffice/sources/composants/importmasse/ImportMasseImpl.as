package composants.importmasse
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.net.registerClassAlias;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.managers.SystemManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import gestioncommande.entity.Configuration;
	import gestioncommande.events.ImportDeMasseEvent;
	
	/**
	 * <code>ImportMasseImpl</code> est la classe principale pour l'import de masse.
	 * Elle instancie les classes extérieurs pour gérer tous ce qui permettra l'import de masse de valeurs dans un DataGrid.
	 * L'import de masse se fait par rapport à un fichier .xls ou un fichier .csv
	 *
	 * @author GUIOU Nicolas
	 * @version 1.0 (02-03-2010) 
	 */
	[Bindable]
	public class ImportMasseImpl extends Box
	{

		///////////////////////////////////////////////////////////
		/////////////		VARIABLES PUBLIQUES   /////////////////
		///////////////////////////////////////////////////////////
		
		// DataGrid
		public var dgElement 					: DataGrid;		

		// Buttons
		public var btDownloadMatriceImport		: Button;
		public var btUploadMatriceImport		: Button;
		public var btAddRow						: Button;
		public var btHelp						: Button;
		public var btClear						: Button;
		public var btAutoFill					: Button;		
		public var btAutoFillErase				: Button;		

		// Label
		public var nbElement					: Label;
		
		// Image
		public var imgDelete					: Image;
		
		// variables utiles
		public var myDataProvider				: ArrayCollection = new ArrayCollection();
		public var tabNomColAttendu				: ArrayCollection = new ArrayCollection([
																							{header:ResourceManager.getInstance().getString('M16','Libell_')},
																							{header:ResourceManager.getInstance().getString('M16','Matricule')},
																							{header:ResourceManager.getInstance().getString('M16','Portabilit_')},
																							{header:ResourceManager.getInstance().getString('M16','Num_ro')},
																							{header:ResourceManager.getInstance().getString('M16','Code_RIO')},
																							{header:ResourceManager.getInstance().getString('M16','Date_de_portabilit_')}
																						]);
		public var tabDataFieldColAttendu		: ArrayCollection = new ArrayCollection([
																							{datafield:'LIBELLE'},
																							{datafield:'MATRICULE'},
																							{datafield:'PORTABILITE'},
																							{datafield:'NUMERO'},
																							{datafield:'CODERIO'},
																							{datafield:'DATEPORTABILITE'}
																						]);
				
		public var myConfiguration				: Configuration;
				
				
		// instances de classes
		public var myImportMasseUploadDownload	: ImportMasseUploadDownload;
		public var myImportMasseService			: ImportMasseService;
		public var myImportMasseUtils			: ImportMasseUtils;
		

		///////////////////////////////////////////////////////////
		////////     VARIABLES PUBLIQUES PARAMETRABLE   ///////////
		///////////////////////////////////////////////////////////
		
		private var _textNbElement:String;
		private var _urlDownload:String;
		private var _urlUpload:String;
		private var _basePath:String;
		
		public var isAutoFillVisible			:Boolean = true;
		public var isPortability				:Boolean = false;
		public var objLinePort					:Object=new Object();

		/**
		 * <b>Constructeur <code>ImportMasseImpl()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Appelle <code>init()</code> lorsque l'IHM a été créée.
		 * </pre>
		 * </p>
		 * 
		 */		
		public function ImportMasseImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		///////////////////////////////////////////////////////////
		/////////////	    GETTER / SETTER       /////////////////
		///////////////////////////////////////////////////////////

		/**
		 * <b>Setter <code>textNbElement(value:String)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Attribut une valeur à la variable privée _textNbElement.
		 * </pre>
		 * </p>
		 * 
		 * @param value:<code>String</code>.
		 * 
		 * @return void.
		 * 
		 */			
		public function set textNbElement(value:String):void
		{
			_textNbElement = value;
		}

		/**
		 * <b>Getter <code>textNbElement()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Retourne la valeur de la variable privée _textNbElement.
		 * </pre>
		 * </p>
		 * 
		 * @return String qui représente la variable privée _textNbElement.
		 * 
		 */	
		public function get textNbElement():String
		{
			return _textNbElement;
		}
		
		/**
		 * <b>Setter <code>urlDownload(value:String)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Attribut une valeur à la variable privée _urlDownload.
		 * </pre>
		 * </p>
		 * 
		 * @param value:<code>String</code>.
		 * 
		 * @return void.
		 * 
		 */	
		public function set urlDownload(value:String):void
		{
			_urlDownload = value;
		}
		
		/**
		 * <b>Getter <code>urlDownload()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Retourne la valeur de la variable privée _urlDownload.
		 * </pre>
		 * </p>
		 * 
		 * @return String qui représente la variable privée _urlDownload.
		 * 
		 */	
		public function get urlDownload():String
		{
			return _urlDownload;
		}
		
		/**
		 * <b>Setter <code>urlUpload(value:String)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Attribut une valeur à la variable privée _urlUpload.
		 * </pre>
		 * </p>
		 * 
		 * @param value:<code>String</code>.
		 * 
		 * @return void.
		 * 
		 */	
		public function set urlUpload(value:String):void
		{
			_urlUpload = value;
		}
		
		/**
		 * <b>Getter <code>urlUpload()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Retourne la valeur de la variable privée _urlUpload.
		 * </pre>
		 * </p>
		 * 
		 * @return String qui représente la variable privée _urlUpload.
		 * 
		 */	
		public function get urlUpload():String
		{
			return _urlUpload;
		}
		
		/**
		 * <b>Setter <code>basePath(value:String)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Attribut une valeur à la variable privée _basePath.
		 * </pre>
		 * </p>
		 * 
		 * @param value:<code>String</code>.
		 * 
		 * @return void.
		 * 
		 */	
		public function set basePath(value:String):void
		{
			_basePath = value;
		}
		
		/**
		 * <b>Getter <code>basePath()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Retourne la valeur de la variable privée _basePath.
		 * </pre>
		 * </p>
		 * 
		 * @return String qui représente la variable privée _basePath.
		 * 
		 */	
		public function get basePath():String
		{
			return _basePath;
		}
		
		///////////////////////////////////////////////////////////
		/////////////	   FONCTIONS PUBLIQUES    /////////////////
		///////////////////////////////////////////////////////////	

		/**
		 * <b>Fonction <code>updateNbElementText()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Met à jour le libellé indiquand le nombre d'élément.
		 * </pre>
		 * </p>
		 * 
		 * @param evt:<code>Event</code>.
		 * 
		 * @return Boolean indiquant si tous les champs sont bien valides.
		 *
		 */		
		public function updateNbElementText(ide:ImportDeMasseEvent = null):void
		{
			if(myDataProvider == null)
				nbElement.text = "0 " + textNbElement;
			else
				nbElement.text = myDataProvider.length + " " + textNbElement;
		}



		/**
		 * <b>Fonction <code>addColumnInDG(myColumns:Array)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Ajoutes toutes les DataGridColumn contenu dans le tableau passé en paramètre.
		 * </pre>
		 * </p>
		 * 
		 * @param myColumns:<code>Array</code> contenant toutes les DataGridColumn du DataGrid.
		 * 
		 * @return void.
		 *
		 */			
		public function addColumnInDG(myColumns:Array):void
		{
			var dgColumns:Array = dgElement.columns;
			for each(var obj : Object in myColumns)
			{
				dgColumns.unshift(obj);
			}
			
			dgElement.columns = dgColumns;
			
			initTabNomColAttendu(myColumns);
		}

		/**
		 * <b>Fonction <code>getResultXML()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Retourne toutes les valeurs du DataGrid sous forme XML.
		 * </pre>
		 * </p>
		 * 
		 * @return XML contenant tous les valeurs du DataGrid.
		 *
		 */			
		public function getResultXML():XML
		{
			var resultXML : XML = <elements></elements>;
			var myString : String = "";
			var longueur : int = tabNomColAttendu.length;
			var nameNode : String = "";
			var myPatternSpace:RegExp = / /gi;
			var myPatternA:RegExp = /[àâä]/gi;
			var myPatternC:RegExp = /[ç]/gi;
			var myPatternE:RegExp = /[éèêë]/gi;
			var myPatternI:RegExp = /[îï]/gi;
			var myPatternO:RegExp = /[ôö]/gi;
			var myPatternU:RegExp = /[ûüù]/gi;
			
			for each(var item:Object in myDataProvider)
			{
				myString = "<element>";
				for(var index:int=0; index<longueur ; index++)
				{
					nameNode = tabNomColAttendu[index];
					nameNode = nameNode.toLowerCase();
					nameNode = nameNode.replace(myPatternSpace,"_");
					nameNode = nameNode.replace(myPatternA,"a");
					nameNode = nameNode.replace(myPatternC,"c");
					nameNode = nameNode.replace(myPatternE,"e");
					nameNode = nameNode.replace(myPatternI,"i");
					nameNode = nameNode.replace(myPatternO,"o");
					nameNode = nameNode.replace(myPatternU,"u");
					
					myString += "<" + nameNode + ">";
					if(item[tabNomColAttendu[index]])
					{
						myString += item[tabNomColAttendu[index]];
					}
					myString += "</" + nameNode + ">";
				}
				myString += "</element>";
				
				var myChild : Object = myString;
				
				resultXML.appendChild(myChild);
			}
			
			return resultXML;
		}
					
		///////////////////////////////////////////////////////////
		/////////////		FONCTIONS PRIVEES     /////////////////
		///////////////////////////////////////////////////////////
		
		/**
		 * <b>Fonction <code>init(evt:FlexEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Initialise les instances de classe <code>ImportMasseUploadDownload</code>, <code>ImportMasseService</code> et <code>ImportMasseUtils</code>.
		 * Initialise le DataGrid.
		 * Appelle <code>initListener()</code> qui initialisera les listeners.
		 * </pre>
		 * </p>
		 * 
		 * @param evt:<code>FlexEvent</code>.
		 * 
		 * @return void.
		 *
		 */
		private function init(evt:FlexEvent):void
		{
			myImportMasseUploadDownload = new ImportMasseUploadDownload(this);
			myImportMasseService = new ImportMasseService(this);
			myImportMasseUtils = new ImportMasseUtils(this);
			
			addEventListener("REFRESH_ITEMSELECTED", refreshItemSelecetdHandler);
			
			dgElement.initialize();
			
			initListener();
			
			
			/*myDataProvider[0].PORTABILITE.IDSOUSTETE=objLinePort.idSousTete;
			myDataProvider[0].PORTABILITE.IS_PORTABILITE=true;
			myDataProvider[0].PORTABILITE.NUMERO=objLinePort.ligne;*/
			trace("item reender-------"+ObjectUtil.toString(myDataProvider));
			trace("item reender-------"+(myDataProvider.length).toString());
		}
		
		private function refreshItemSelecetdHandler(e:Event):void
		{
			if(dgElement.selectedItem != null)
				myDataProvider.itemUpdated(dgElement.selectedItem);
		}

		/**
		 * <b>Fonction <code>initListener()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Initialise tous les listeners.
		 * </pre>
		 * </p>
		 * 
		 * @return void.
		 *
		 */		
		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);
			
			btClear.addEventListener(MouseEvent.CLICK, btClearHandler);
			btDownloadMatriceImport.addEventListener(MouseEvent.CLICK, myImportMasseUploadDownload.btDownloadMatriceImportClickHandler);
			btUploadMatriceImport.addEventListener(MouseEvent.CLICK, myImportMasseUploadDownload.btUploadClickHandler);
			btAddRow.addEventListener(MouseEvent.CLICK, btAddRowClickHandler);
			btHelp.addEventListener(MouseEvent.CLICK, btHelpClickHandler);
			btAutoFill.addEventListener(MouseEvent.CLICK, btAutoFillClickHandler);
			btAutoFillErase.addEventListener(MouseEvent.CLICK, btAutoFillEraseClickHandler);
			
			addEventListener(ImportDeMasseEvent.IMPORT_REFRESH, updateNbElementText);
			
			dgElement.addEventListener(KeyboardEvent.KEY_DOWN,myImportMasseUtils.dataGridKeyDownHandler);
		}

		/**
		 * <b>Fonction <code>initTabNomColAttendu(myColumns:Array)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Initialise le tableau "tabNomColAttendu" avec le nom des colonnes
		 * </pre>
		 * </p>
		 * 
		 * @param myColumns:<code>Array</code> contenant toutes les DataGridColumn du DataGrid.
		 * 
		 * @return void.
		 *
		 */		
		private function initTabNomColAttendu(myColumns:Array):void
		{
			var myNames 	:Array = new Array();
			var myDataField :Array = new Array();
			var longueur 	:int = myColumns.length;
			
			for(var i:int=2;i<longueur;i++)
			{
				myNames.unshift(myColumns[i].headerText);
				myDataField.unshift(myColumns[i].dataField);
			}
		}

		/**
		 * <b>Fonction <code>cancelPopup(evt:MouseEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Appelle <code>closePopup()</code> pour fermer la popup.
		 * </pre>
		 * </p>
		 * 
		 * @param evt:<code>MouseEvent</code>.
		 * 
		 * @return void.
		 *
		 */
		private function cancelPopup(evt:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * <b>Fonction <code>closePopup(evt:CloseEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Fermer la popup grâce à la méthode static <code>removePopUp</code> de la classe <code>PopUpManager</code>.
		 * </pre>
		 * </p>
		 * 
		 * @param evt:<code>CloseEvent</code>.
		 * 
		 * @return void.
		 *
		 */
		private function closePopup(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * <b>Fonction <code>btClearHandler(evt:Event)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Supprime tous les items qui étaient dans le dataProvider du DataGrid.
		 * Appelle la méthode <code>updateNbElementText()</code> pour mettre à jour le label indiquant le nombre de commande identique.
		 * </pre>
		 * </p>
		 * 
		 * @param evt:<code>Event</code>.
		 * 
		 * @return void.
		 *
		 */		
		private function btClearHandler(me:MouseEvent):void
		{
			dispatchEvent(new ImportDeMasseEvent(ImportDeMasseEvent.IMPORT_REMOVE_ALL, true));
		}
		
		/**
		 * <b>Fonction <code>imgDeleteClickHandler()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Supprime la ligne séléctionnée du DataGrid
		 * </pre>
		 * </p>
		 * 
		 * @return void.
		 *
		 */
		public function imgDeleteClickHandler():void
		{
			dispatchEvent(new ImportDeMasseEvent(ImportDeMasseEvent.IMPORT_REMOVE_ITEM, true));
		}
		
		/**
		 * <b>Fonction <code>btAddRowClickHandler(evt:Event)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Ajoute un nouvel item au dataProvider du DataGrid.
		 * Appelle la méthode <code>updateNbElementText()</code> pour mettre à jour le label indiquant le nombre de commande identique.
		 * </pre>
		 * </p>
		 * 
		 * @param evt:<code>Event</code>.
		 * 
		 * @return void.
		 *
		 */			
		private function btAddRowClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new ImportDeMasseEvent(ImportDeMasseEvent.IMPORT_ADD_ITEM, true));
		}		

		/**
		 * <b>Fonction <code>btHelpClickHandler(evt:Event)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Créer une popUp et l'affiche. Cette popUp contient les aides pour l'import de masse.
		 * </pre>
		 * </p>
		 * 
		 * @param evt:<code>Event</code>.
		 * 
		 * @return void.
		 *
		 */			
		private function btHelpClickHandler(evt:Event):void
		{
			var poppHelp:ImportMassePopUpHelp = new ImportMassePopUpHelp();
			
			PopUpManager.addPopUp(poppHelp, this.parent.parent.parent.parent.parent.parent.parent, true);
			PopUpManager.centerPopUp(poppHelp);
		}
		
		private function btAutoFillClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('AUTO_FILL'));
		}
		
		private function btAutoFillEraseClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('ERASE_LIBELLE_FILL'));
		}
				
	}
}