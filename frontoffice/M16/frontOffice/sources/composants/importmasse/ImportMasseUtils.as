package composants.importmasse
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.KeyboardEvent;
	import flash.events.TextEvent;
	import flash.net.registerClassAlias;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	import gestioncommande.entity.Configuration;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.services.Formator;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import session.SessionUserObject;

	/**
	 * <code>ImportMasseUtils</code> est la classe qui gère les méthodes utiles et les constantes pour l'import de masse.
	 * Elle instancie une référence unique de la classe <code>ImportMasseImpl</code>.
	 * Grâce à cette instance elle peut appeller toutes les méthodes qui lui seront utiles dans les classes <code>ImportMasseService</code> et <code>ImportMasseUploadDownload</code>.
	 *
	 * @author GUIOU Nicolas
	 * @version 1.0 (02-03-2010) 
	 */		
	[Bindable]
	public class ImportMasseUtils
	{

		///////////////////////////////////////////////////////////
		/////////////		VARIABLES PRIVEES     /////////////////
		///////////////////////////////////////////////////////////
		 
		private var _myImportMasse				:ImportMasseImpl;
//		public var myConfiguration				:Configuration;
		/**
		 * <b>Constructeur <code>ImportMasseUtils(instanceMyImportMasse:ImportMasseImpl)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Instancie la variable privée myImportMasse pour avoir une référence unique de l'objet ImportMasseImpl.
		 * </pre>
		 * </p>
		 * 
		 * @param instanceMyImportMasse:<code>ImportMasseImpl</code>.
		 * 
		 */
		public function ImportMasseUtils(instanceMyImportMasse:ImportMasseImpl)
		{
			myImportMasse = instanceMyImportMasse;
		}
		
		///////////////////////////////////////////////////////////
		/////////////	    GETTER / SETTER       /////////////////
		///////////////////////////////////////////////////////////

		/**
		 * <b>Setter <code>myImportMasse(value:ImportMasseImpl)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Attribut une valeur à la variable privée _myImportMasse.
		 * </pre>
		 * </p>
		 * 
		 * @param value:<code>ImportMasseImpl</code>.
		 * 
		 * @return void.
		 * 
		 */		
		public function set myImportMasse(value:ImportMasseImpl):void
		{
			_myImportMasse = value;
		}

		/**
		 * <b>Getter <code>myImportMasse()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Retourne la valeur de la variable privée _myImportMasse.
		 * </pre>
		 * </p>
		 * 
		 * @return ImportMasseImpl qui représente la référence unique de l'objet ImportMasseImpl.
		 * 
		 */	
		public function get myImportMasse():ImportMasseImpl
		{
			return _myImportMasse;
		}	

		///////////////////////////////////////////////////////////
		/////////////	   FONCTIONS PUBLIQUES    /////////////////
		///////////////////////////////////////////////////////////

		/**
		 * <b>Fonction <code>fillDG(myArray:Array)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Remplis le DataGrid avec les valeurs du fichier importé. Ces valeurs sont passées en paramètre dans un Array.
		 * </pre>
		 * </p>
		 * 
		 * @param myArray:<code>Array</code>.
		 * 
		 * @return void.
		 *
		 */		
		public function fillDG(myArray:Array):void
		{
			var longueur 	:int = myArray.length ;
			var longueur2 	:int = myImportMasse.tabNomColAttendu.length;
 			
 			if((longueur/longueur2) < 100)
			{			
				var newElements	:ConfigurationElements = new ConfigurationElements();
				var listeOrgas	:ArrayCollection = new ArrayCollection();
				
				if(myImportMasse.myDataProvider.length > 0)
					listeOrgas = (myImportMasse.myDataProvider[0] as ConfigurationElements).ORGANISATIONS;
				
				for(var index:int = 0; index < longueur;)
				{
					newElements = new ConfigurationElements();
					
					for(var index2:int = 0; index2 < longueur2;index2++)
					{
						if(myImportMasse.tabDataFieldColAttendu[index2].datafield == 'DATEPORTABILITE')
						{
							if(myArray[index+index2] as String)
							{
								var newDate		:Date 	= Formator.formatDateStringInDate(myArray[index+index2]);
								var strgDate	:String	= Formator.formatDate(newDate);
								
								if(strgDate.indexOf("NaN") > -1)
									newElements.PORTABILITE.DATEPORTABILITE = null;
								else
									newElements.PORTABILITE.DATEPORTABILITE = newDate;
							}
						}
						else if(myImportMasse.tabDataFieldColAttendu[index2].datafield == 'PORTABILITE')
							{
								if(myArray[index+index2] != "")
									newElements.IS_PORTABILITE 	= true;
							}
							else if(myImportMasse.tabDataFieldColAttendu[index2].datafield == 'MATRICULE')
								{
									newElements.MATRICULE = myArray[index+index2];
								
									if(myArray[index+index2] != "")
										newElements.IS_COLLABORATEUR = true;
								}
								else  if(myImportMasse.tabDataFieldColAttendu[index2].datafield == 'NUMERO')
									{
										newElements.PORTABILITE.NUMERO = newElements.PORTABILITE.NUMERO_HIDDEN = myArray[index+index2];
									}
									else if(myImportMasse.tabDataFieldColAttendu[index2].datafield == 'CODERIO')
										{
											newElements.PORTABILITE.CODERIO = myArray[index+index2];
										}
										else
										{
											newElements[myImportMasse.tabDataFieldColAttendu[index2].datafield] = myArray[index+index2];
										}
					}
					
					if(SessionUserObject.singletonSession.IDSEGMENT > 1)
						newElements.IS_MOBILE = false;
					else
						newElements.IS_MOBILE = true;
					
					newElements.ORGANISATIONS = ObjectUtil.copy(listeOrgas) as ArrayCollection;
					
					myImportMasse.myDataProvider.addItem(newElements);
					
					index+=longueur2;
				}
				
				myImportMasse.updateNbElementText();
			 }
			else
				Alert.show(ResourceManager.getInstance().getString('M16', 'Le_fichier_est_trop_lourd__il_contient_p'),ResourceManager.getInstance().getString('M16', 'Erreur'));
		}

		/**
		 * <b>Fonction <code>checkHeader(myObj:Object)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Vérifie si le fichier importé contient bien les mêmes en-têtes que la DataGrid.
		 * </pre>
		 * </p>
		 * 
		 * @param myObj:<code>Object</code>.
		 * 
		 * @return Boolean indiquant si le fichier importé contient bien les mêmes en-têtes que la DataGrid.
		 *
		 */		
		public function checkHeader(myObj:Object):Boolean
		{
			var longueur:int = myImportMasse.tabNomColAttendu.length;
			
			if(myObj.COLUMNNAMES) // fichier .xls
			{
				if(myObj.COLUMNNAMES.length > 0)
				{
					for(var index: int=0;index<longueur;index++)
					{
						if(myObj.COLUMNNAMES[index].toString() != myImportMasse.tabNomColAttendu[index].header)
						{
							Alert.show(ResourceManager.getInstance().getString('M16', 'Le_fichier_n_a_pas_le_format_d_une_matri'),ResourceManager.getInstance().getString('M16', 'Erreur'));
							
							return false;
						}
					}
				}
				else
				{
					Alert.show(ResourceManager.getInstance().getString('M16', 'Le_fichier_n_a_pas_le_format_d_une_matri'),ResourceManager.getInstance().getString('M16', 'Erreur'));
					
					return false;
				}
			}
			else // fichier .csv
			{
				if(myObj.length > 0)
				{
					for(var index2: int=0;index2<longueur;index2++)
					{
						if(myObj[0][index2].toString().toUpperCase()!= myImportMasse.tabNomColAttendu[index2])
						{
							Alert.show(ResourceManager.getInstance().getString('M16', 'Le_fichier_n_a_pas_le_format_d_une_matri'),ResourceManager.getInstance().getString('M16', 'Erreur'));
							
							return false;
						}
					}
				}
				else
				{
					Alert.show(ResourceManager.getInstance().getString('M16', 'Le_fichier_n_a_pas_le_format_d_une_matri'),ResourceManager.getInstance().getString('M16', 'Erreur'));
					
					return false;
				}
			}
			
			return true;
		}

		/**
		 * <b>Fonction <code>dataGridKeyDownHandler(evt:KeyboardEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Détecte l'événement clavier "Ctrl + V" et colle dans le DataGrid ce qui y a dans le presse papier.
		 * </pre>
		 * </p>
		 * 
		 * @param evt:<code>KeyboardEvent</code>.
		 * 
		 * @return void.
		 *
		 */		
		public function dataGridKeyDownHandler(evt:KeyboardEvent):void
		{
//			if(evt.ctrlKey && !myImportMasse.dgElement.getChildByName("clipboardProxy") && (evt.target is DataGrid))
//			{
//				// Ajout d'un objet TextField invisible pour le DataGrid
//				var textField:TextField=new TextField();
//				textField.name="clipboardProxy";
//				myImportMasse.dgElement.addChild(textField);
//				textField.visible=false;
//				textField.type=TextFieldType.INPUT;
//				textField.multiline=true;
//
//				// Mettre dans le TextField les données copiées depuis le format TSV 
//				textField.text=getTextFromItems(myImportMasse.dgElement.selectedItems);
//				textField.setSelection(0, textField.text.length - 1);
//				
//				textField.addEventListener(TextEvent.TEXT_INPUT, clipboardProxyPasteHandler);
//
//				// Mettre le focus au TextField
//				myImportMasse.systemManager.stage.focus=textField;
//			}
		}

		///////////////////////////////////////////////////////////
		/////////////		FONCTIONS PRIVEES     /////////////////
		///////////////////////////////////////////////////////////   

		/**
		 * <b>Fonction <code>clipboardProxyPasteHandler(evt:TextEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Vérifie la cohérence entre ce qui est dans le presse papier et les en-têtes de colonnes attendues.
		 * Appelle la méthode <code>cleanClipBoard()</code> qui effacera le contenu du presse papier.
		 * </pre>
		 * </p>
		 * 
		 * @param evt:<code>TextEvent</code>.
		 * 
		 * @return void.
		 *
		 */	
		private function clipboardProxyPasteHandler(evt:TextEvent):void
		{
			// Extraction des valeurs depuis le format TSV
			var items		:Array 	= getItemsFromText(evt.text);
			var longueur	:int 	= items.length;
			var longueur2 	:int 	= myImportMasse.tabNomColAttendu.length ;
			
 			if(longueur > 0)
			{
				// test pour vérifier que les en tetes des colonnes soient en cohérence
				for(var index2:int=0; index2 < longueur2; index2++)
				{
					if((items[0][(myImportMasse.tabDataFieldColAttendu[index2] as String).toUpperCase()] as String).toUpperCase() != (myImportMasse.tabNomColAttendu[index2] as String).toUpperCase())
					{
						ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Les_en_t_tes_des_colonnes_ne_sont_pas_en'), ResourceManager.getInstance().getString('M16', 'Erreur'));
						cleanClipBoard();
						return;
					}
				}
				
				var config:Configuration;
				// en tetes en cohérence donc affectation des valeurs
				for(var index:int = 1 ; index < longueur ; index++)
				{
					config 					= new Configuration();
					registerClassAlias("Configuration", gestioncommande.entity.Configuration);
					config 					= ObjectUtil.copy(_myImportMasse.myConfiguration) as Configuration;
					config.CODERIO			= items[index].CODERIO;
					config.COLLAB_MATRICULE	= items[index].COLLAB_MATRICULE;
					config.LIBELLE			= items[index].LIBELLE;
					config.NUMERO			= items[index].NUMERO;
					config.PORTABILITE		= items[index].PORTABILITE;
					
					if(config.PORTABILITE != "")
						config.IS_PORTABILITE = true;
						
					if(config.COLLAB_MATRICULE != "")
					{
						config.IS_COLLABORATEUR = true;
						config.IS_LIBELLE 		= false;
					}
					
					var thisDate	:Date = null;
					
					if(items[index].DATE_PORTABILITE as String)
					{
						if(items[index].DATE_PORTABILITE != "")
						{
							var newDate		:Date 	= Formator.formatDateStringInDate(items[index].DATE_PORTABILITE);
							var strgDate	:String	= Formator.formatDate(newDate);
							
							if(strgDate.indexOf("NaN") > -1)
							{
								thisDate 	= null;
							}
							else
							{
								thisDate 	= newDate;
							}
						}
					}
					
					config.DATE_PORTABILITE	= thisDate;
					
					myImportMasse.myDataProvider.addItem(config);
				}
				myImportMasse.updateNbElementText();
				cleanClipBoard();
			}
		}
		
		/**
		 * <b>Fonction <code>cleanClipBoard()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Efface le contenu du presse papier.
		 * </pre>
		 * </p>
		 * 
		 * @return void.
		 *
		 */	
		private function cleanClipBoard():void
		{
			var textField:TextField=TextField(myImportMasse.dgElement.getChildByName("clipboardProxy"));
			
			if (textField)
			{
				textField.removeEventListener(TextEvent.TEXT_INPUT, clipboardProxyPasteHandler);
				myImportMasse.dgElement.removeChild(textField);
				textField = null;
			}
		}

		/**
		 * <b>Fonction <code>getItemsFromText(text:String)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Créée un Array contenant toutes les cellules à coller dans le DataGrid, d'après le contenu du presse papier.
		 * </pre>
		 * </p>
		 * 
		 * @param text:<code>String</code> représentant le contenu du presse papier.
		 * 
		 * @return Array contenant toutes les cellules à coller dans le DataGrid.
		 *
		 */	
		private function getItemsFromText(text:String):Array
		{
			var rows:Array=text.split("\n");
			var columns:Array=myImportMasse.dgElement.columns;
			var itemsFromText:Array=[];
			
			if (!rows[rows.length - 1])
			{
				rows.pop();
			}

			for each (var rw:String in rows)
			{
				var fields:Array=rw.split("\t");
				var n:int=Math.min(columns.length, fields.length);
				var item:Object={};
				
				for (var i:int=0; i < n; i++)
				{
					
					item[columns[i].dataField]=fields[i];
				}
				itemsFromText.push(item);
			}
			return itemsFromText;
		}

		/**
		 * <b>Fonction <code>getTextFromItems(items:Array)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u> 
		 * Créée une String contenant toutes les en-têtes du DataGrid.
		 * </pre>
		 * </p>
		 * 
		 * @param items:<code>Array</code>.
		 * 
		 * @return String contenant toutes les en-têtes du DataGrid.
		 *
		 */	
		private function getTextFromItems(items:Array):String
		{
			var columns			:Array = myImportMasse.dgElement.columns;
			var textFromItems	:String = "";

			for each (var it:Object in items)
			{
				for each (var c:DataGridColumn in columns)
				{
					if(c.dataField != "DELETE")
					{
						textFromItems+=it[c.dataField] + "\t";
					}
				}
				
				textFromItems+="\n";
			}
			return textFromItems;
		}

	}
}