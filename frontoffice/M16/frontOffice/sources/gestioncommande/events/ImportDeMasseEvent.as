package gestioncommande.events
{
	import flash.events.Event;
	
	public class ImportDeMasseEvent extends Event
	{
		
		
		public static const IMPORT_ADD_ITEM				:String = "IMPORT_ADD_ITEM";
		
		public static const IMPORT_REMOVE_ITEM			:String = "IMPORT_REMOVE_ITEM";
		public static const IMPORT_REMOVE_ALL			:String = "IMPORT_REMOVE_ALL";
		
		public static const IMPORT_REFRESH				:String = "IMPORT_REFRESH";
		
		public static const IMPORT_						:String = "IMPORT_";
		public static const NONE						:String = "NONE";
		
		
		public function ImportDeMasseEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}