package gestioncommande.events
{
	import flash.events.Event;
	
	public class TypeOperationSurLigneIHMEvent extends Event
	{
		public static const BT_ANNULER_CLICK_EVENT:String = "btAnnulerClickEvent";
		public static const BT_VALIDER_CLICK_EVENT:String = "btValiderClickEvent";
		
		public static const OPERATION_LIGNE_EVENT:String = "OPERATION_LIGNE_EVENT";
		//public static const SUSPENDRE_LIGNE_EVENT:String = "SUSPENDRE_LIGNE_EVENT";
		//public static const ACTIVER_LIGNE_EVENT:String = "ACTIVER_LIGNE_EVENT";
		
		public function TypeOperationSurLigneIHMEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new TypeOperationSurLigneIHMEvent(type, bubbles, cancelable);
		}
	}
}