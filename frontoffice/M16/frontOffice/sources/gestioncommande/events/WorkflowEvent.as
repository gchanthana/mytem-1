package gestioncommande.events
{
	import flash.events.Event;
	
	public class WorkflowEvent extends Event
	{

		public static const ETAPE1		:String = "1";
		public static const ETAPE2		:String = "2";
		public static const ETAPE3		:String = "3";
		public static const ETAPE4		:String = "4";
		public static const ETAPE5		:String = "5";
		public static const ETAPE6		:String = "6";
		public static const ETAPE7		:String = "7";

		public static const CHANGE		:String = "CHANGE";
		public static const ERROR		:String = "ERROR";

		
		public var idButton	:int = -1;		
		
		public function WorkflowEvent(type:String, bubbles:Boolean = false, ID:int = -1, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.idButton= ID;
		}

		override public function clone():Event
		{
			return new WorkflowEvent(type, bubbles, idButton, cancelable);
		}
		

	}
}