package gestioncommande.events
{
	import flash.events.Event;
	
	public class CommandeEvent extends Event
	{
		
		//>>>>>>>>>>>>>>>>>ANNUL LA COMMANDE EN COURS PAR LA GESTION DE FLOTTE
		public static const ANNUL_COMMANDE		:String = "ANNUL_COMMANDE";
		public static const ANNUL_LINES			:String = "ANNUL_LINES";
		public static const ANNUL_LINEBYLINE	:String = "ANNUL_LINEBYLINE";
		
		//>>>>>>>>>>>>>>>>>VALIDE LA COMANDE EN COURS
		public static const VALIDATE_COMMANDE	:String = "VALIDATE_COMMANDE";
		public static const VALIDATE_LINES		:String = "VALIDATE_LINES";
		public static const VALIDATE_LINEBYLINE	:String = "VALIDATE_LINEBYLINE";

		//>>>>>>>>>>>>>>>>>PROCEDURE COMMANDE
		public static const COMMANDE_UPDATED	:String = "COMMANDE_UPDATED";
		public static const COMMANDE_CREATED	:String = "COMMANDE_CREATED";
		public static const COMMANDE_ERROR		:String = "COMMANDE_ERROR";
		public static const COMMANDE_MANAGED	:String = "COMMANDE_MANAGED";
		public static const COMMANDE_ARRIVED	:String = "COMMANDE_ARRIVED";
		
		public static const ARTICLES_XML_GROUP	:String = "ARTICLES_XML_GROUP";
		
		public static const COMMANDE_LISTED_FIL	:String = "COMMANDE_LISTED_FIL";
		public static const COMMANDE_LISTED		:String = "COMMANDE_LISTED";
		public static const COMMANDE_PARTIELLE	:String = "COMMANDE_PARTIELLE";
		public static const ENABLE_CMD_PART		:String = "ENABLE_CMD_PART";
		public static const DISABLE_CMD_PART	:String = "DISABLE_CMD_PART";
		
		public static const COMMANDE_DETAILS	:String = "COMMANDE_DETAILS";
		public static const COMMANDE_ARTICLES	:String = "COMMANDE_ARTICLES";

		public static const SOUSTETE_OPERATEUR	:String = "SOUSTETE_OPERATEUR";
		
		//>>>>>>>>>>>>>>>>>PROCEDURE MAJ ARTICLES
		public static const ARTICLES_UPDATED	:String = "ARTICLES_UPDATED";

		//>>>>>>>>>>>>>>>>>PROCEDURE MODELE
		public static const MODELE_LISTED		:String = "MODELE_LISTED";
		public static const MODELE_CREATED		:String = "MODELE_CREATED";
		public static const MODELE_ADD			:String = "MODELE_ADD";
		public static const MODELE_UPDATED		:String = "MODELE_UPDATED";
		public static const MODELE_ERASED		:String = "MODELE_ERASED";
		public static const MODELE_ERROR		:String = "MODELE_ERROR";
		public static const MODELE_SELECTED		:String = "MODELE_SELECTED";
		
		//>>>>>>>>>>>>>>>>>LIGNES A MODIFIER
		public static const LISTED_LIGNESBY_OPE	:String = "LISTED_LIGNESBY_OPE";
		
		//>>>>>>>>>>>>>>>>>PARAMETRES
		public static const LISTED_POOLS		:String = "LISTED_POOLS";
		public static const LISTED_SITES		:String = "LISTED_SITES";
		public static const LISTED_PROFILES		:String = "LISTED_PROFILES";
		public static const LISTED_PRF_ACTIONS	:String = "LISTED_PRF_ACTIONS";

		
		public static const LISTED_OPERATEURSPAYS:String = "LISTED_OPERATEURSPAYS";
		public static const LISTED_OPEAUTORISES	:String = "LISTED_OPEAUTORISES";
		public static const LISTED_REVAUTORISES	:String = "LISTED_REVAUTORISES";
		public static const LISTED_NBR_REVAUTO	:String = "LISTED_NBR_REVAUTORISES";

		public static const LISTED_OPERATEURS	:String = "LISTED_OPERATEURS";
		public static const LISTED_COMPTES		:String = "LISTED_COMPTES";
		public static const LISTED_SOUS_COMPTES	:String = "LISTED_SOUS_COMPTES";
		public static const LISTED_LIBELLE_OPE	:String = "LISTED_LIBELLE_OPE";
		
		public static const LISTED_REVENDEURS	:String = "LISTED_REVENDEURS";
		public static const LISTED_AGENCES		:String = "LISTED_AGENCES";
		public static const LISTED_CONTACTS		:String = "LISTED_CONTACTS";

		public static const LISTED_REVENDEURSLA	:String = "LISTED_REVENDEURSLA";

		
		//>>>>>>>>>>>>>>>>EQUIPEMENTS
		public static const LISTED_ENGAGEMENTS	:String = "LISTED_ENGAGEMENTS";
		public static const LISTED_EQUIPEMENTS	:String = "LISTED_EQUIPEMENTS";
		public static const LISTED_SIM			:String = "LISTED_SIM";
		public static const LISTED_SIM_SPARE	:String = "LISTED_SIM_SPARE";
		public static const LISTED_SIMASSOCIATE	:String = "LISTED_SIMASSOCIATE";
		public static const LISTED_MAJ_SIMTERM	:String = "LISTED_MAJ_SIMTERM";
		public static const LISTED_DETAILS_EQ	:String = "LISTED_DETAILS_EQ";
		
		//>>>>>>>>>>>>>>>>EQUIPEMENTS
		public static const LISTED_ACCESSOIRES	:String = "LISTED_ACCESSOIRES";
		
		//>>>>>>>>>>>>>>>>OPTIONS
		public static const LISTED_RESSOURCES	:String = "LISTED_RESSOURCES";
		public static const LISTED_ABO_ASS		:String = "LISTED_ABO_ASS";
		public static const LISTED_OPT_ASS		:String = "LISTED_OPT_ASS";
		public static const LISTED_OPTIONS		:String = "LISTED_OPTIONS";
		public static const LISTED_OPTIONSERASE	:String = "LISTED_OPTIONSERASE";
		public static const LISTED_FAVORIS		:String = "LISTED_FAVORIS";
		//public static const RESSOURCES_NUMBER	:String = "RESSOURCES_NUMBER";
		
		public static const LISTED_ADD_FAVORIS	:String = "LISTED_ADD_FAVORIS";
		public static const LISTED_REM_FAVORIS	:String = "LISTED_REM_FAVORIS";
		
		//>>>>>>>>>>>>>>>>PRIX RESILIATION
		public static const LISTED_CLIENT_OPE	:String = "LISTED_CLIENT_OPE";
		
		//>>>>>>>>>>>>>>>>DETAILS DES EQUIPEMENTS
		public static const VIEW_EQUIPEMENT		:String = "VIEW_EQUIPEMENT";
		
		//>>>>>>>>>>>>>>>>LISTE DES ACTIONS POSSIBLE
		//public static const LISTED_ACTIONS		:String = "LISTED_ACTIONS";
		
		//>>>>>>>>>>>>>>>>ENVOI DE MAIL
		public static const MAIL_SEND			:String = "MAIL_SEND";
		
		//>>>>>>>>>>>>>>>>ENVOI DE COMMANDE
		public static const SEND_COMMANDE		:String = "SEND_COMMANDE";
		
		//>>>>>>>>>>>>>>>>ENVOI DE L'ACTION
		public static const SEND_ACTION			:String = "SEND_ACTION";
		
		//>>>>>>>>>>>>>>>>GENERATION DU NUM DE LIGNE UNIQUE
		public static const SEND_LIGNE_UNIQUE	:String = "SEND_LIGNE_UNIQUE";
		public static const SEND_LIGNES_UNIQUE	:String = "SEND_LIGNES_UNIQUE";
		
		public static const GET_TYPE_LIGNE		:String = "GET_TYPE_LIGNE";
		public static const ASSOCIATED_LIGNE	:String = "ASSOCIATED_LIGNE";
		public static const LIGNE_FINDED		:String = "LIGNE_FINDED";
		public static const GET_CODE_RIO		:String = "GET_CODE_RIO";
		
		//>>>>>>>>>>>>>>>>VERIFICATION DU NUMERO DE LIGNE S'IL EST UNIQUE
		public static const LIGNE_UNIQUE		:String = "LIGNE_UNIQUE";
		public static const LIGNES_UNIQUE		:String = "LIGNES_UNIQUE";
		
		public static const IDTYPEPROFIL		:String = "IDTYPEPROFIL";
		
		//>>>>>>>>>>>>>>>>VERIFICATION DU NUMERO DE LIGNE S'IL EST UNIQUE
		public static const EMPLOYES_LISTED		:String = "EMPLOYES_LISTED";
		public static const EMPLOYE_LISTED		:String = "EMPLOYE_LISTED";

		public static const MARICULES_LISTED	:String = "MARICULES_LISTED";
		
		public static const EMPLOYE_CREATED		:String = "EMPLOYE_CREATED";
		public static const EMPLOYE_UPDATED		:String = "EMPLOYE_UPDATED";
		public static const EMPLOYE_CHAMPS		:String = "EMPLOYE_CHAMPS";

		public static const LISTED_NUMEROS		:String = "LISTED_NUMEROS";
		
		public static const LISTED_ACTE_POUR	:String = "LISTED_ACTE_POUR";
		
		public static const LISTED_TRANSPORTS	:String = "LISTED_TRANSPORTS";
		public static const TRANSPORTS_UPDATED	:String = "TRANSPORTS_UPDATED";
		
		public static const LISTED_WFLOW_HIST	:String = "LISTED_WFLOW_HIST";

		public static const LISTED_POOL_HIST	:String = "LISTED_POOL_HIST";
		
		public static const INFOS_COMMANDE		:String = "INFOS_COMMANDE";

		public static const ELIGIBILITE_LIGNE	:String = "ELIGIBILITE_LIGNE";
		
		public static const ELIGIBLE_COMPTE_LIGNE	:String = "ELIGIBLE_COMPTE_LIGNE";
		
		public static const ADRESSE_REVENDEUR	:String = "ADRESSE_REVENDEUR";

		public static const MATRICULE_AUTO		:String = "MATRICULE_AUTO";
		
		public static const COMMANDE_INFOS		:String = "COMMANDE_INFOS";
		
		public static const TERMSIMCARDENGAG	:String = "TERMSIMCARDENGAG";

		public static const COMMANDE_FILTRES	:String = "COMMANDE_FILTRES";
		
		public static const COMMANDES_LISTE		:String = "COMMANDES_LISTE";
		
		public static const INFOS_THREAD_CMD	:String = "INFOS_THREAD_CMD";

		public static const INFOS_REFERENCES	:String = "INFOS_REFERENCES";
		
		
		public static const INFOS_IMPORT		:String = "INFOS_IMPORT";
		public static const IMPORT_FINISHED		:String = "IMPORT_FINISHED";
		
		public static const CMD_INEXISTANT_V		:String = "CMD_INEXISTANT_V";
		
		public function CommandeEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new CommandeEvent(type, bubbles, cancelable);
		}

	}
}
