package gestioncommande.events
{
	import flash.events.Event;
	
	public class ActionsEvent extends Event
	{
		
		public static const ACTIONSEVENT_EDIT		:String = "AE_EDIT";
		public static const ACTIONSEVENT_DOWNLOAD	:String = "AE_DWL";
		public static const ACTIONSEVENT_ERASE		:String = "AE_ERASE";
		
		public static const POOL_CHANGED			:String = 'POOL_CHANGED'; 
		
		public static const DATE_EXPORT_ALL_CHOSEN	:String = "DATE_EXPORT_ALL_CHOSEN";
		
		public var object							:Object = null;
		
		public function ActionsEvent(type:String, bubbles:Boolean = false, obj:Object = null, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.object	= obj;
		}

		override public function clone():Event
		{
			return new ActionsEvent(type, bubbles, object, cancelable);
		}

	}
}