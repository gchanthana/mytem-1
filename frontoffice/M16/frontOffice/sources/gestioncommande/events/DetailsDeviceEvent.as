package gestioncommande.events
{
	import flash.events.Event;
	
	import gestioncommande.entity.EquipementsElements;
	
	public class DetailsDeviceEvent extends Event
	{
		public static const MY_DETAILS_DEVICE		:String = 'MY_DETAILS_DEVICE';
		
		public var selectedEquipementsElements							:EquipementsElements =  null;
		public function DetailsDeviceEvent(type:String, selectedEquipementsElements:EquipementsElements, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.selectedEquipementsElements = selectedEquipementsElements;
		}
	}
}