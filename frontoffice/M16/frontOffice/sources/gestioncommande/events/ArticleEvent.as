package gestioncommande.events
{
	import flash.events.Event;
	
	public class ArticleEvent extends Event
	{
		
		public static const LISTED_ARTICLES	:String = 'LISTED_ARTICLES';
		
		public function ArticleEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}