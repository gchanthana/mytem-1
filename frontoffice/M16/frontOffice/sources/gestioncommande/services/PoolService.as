package gestioncommande.services
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.StringUtil;
	import mx.utils.ObjectUtil;
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Pool;
	import gestioncommande.events.CommandeEvent;
	
	import session.SessionUserObject;
	
	public class PoolService extends EventDispatcher
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					GLOBALES
		//--------------------------------------------------------------------------------------------//
		
		public var listePools				:ArrayCollection;
		public var listeProfiles			:ArrayCollection;
		public var listeActionsProfile		:ArrayCollection;
		public var listePoolsValues			:ArrayCollection;
		
		//--------------------------------------------------------------------------------------------//
		//					LIBELLE A TRADUIRE - LUPO
		//--------------------------------------------------------------------------------------------//
		
		private var text_libelleError		:String = ResourceManager.getInstance().getString('M16', 'Erreur');
		private var text_errorPool			:String = ResourceManager.getInstance().getString('M16', 'Erreur_pools__');
		private var text_errorProfiles		:String = ResourceManager.getInstance().getString('M16', 'Erreur_type_de_commande__');
		private var text_errorActionsProfile:String = ResourceManager.getInstance().getString('M16', 'Aucune_liste_d_actions_possible__');
		
	//--------------------------------------------------------------------------------------------//
	//					CONSRUCTEUR
	//--------------------------------------------------------------------------------------------//	
		
		public function PoolService()
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLICS
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					PROCEDURES
		//--------------------------------------------------------------------------------------------//
			
		public function fournirListePoolsGestionnaire():void
		{
			listePools = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.PoolService",
																				"fournirListePoolsDuGestionnaire",
																				fournirListePoolsGestionnaireResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		public function fournirProfilEquipements(): void
	    {
			listeProfiles = new ArrayCollection();
	    	
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.PoolService",
	    																		"fournirListeProfils",
	    																		fournirProfilEquipementsResultHandler);
	    	
	    	RemoteObjectUtil.callService(op,SessionUserObject.singletonSession.IDSEGMENT,
	    									CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,
	    									CvAccessManager.getSession().USER.CLIENTACCESSID);	
	    }
		
		public function fournirProfilEquipementsForSAV(): void
		{
			listeProfiles = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M16.v2.PoolService",
				"fournirListeProfils",
				fournirProfilEquipementsForSavResultHandler);
			
			RemoteObjectUtil.callService(op,SessionUserObject.singletonSession.IDSEGMENT,
				CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,
				CvAccessManager.getSession().USER.CLIENTACCESSID);	
		}
		
		public function fournirListeActionProfilCommande(idprofil:int): void
		{
			listeActionsProfile = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.PoolService",
																				"fournirListeActionProfilCommande",
																				fournirListeActionProfilCommandeResultHandler);
			
			RemoteObjectUtil.callService(op, idprofil);	
		}
		
		public function fournirPools(idprofil:int): void
		{
			listePoolsValues = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.PoolService",
																				"fournirPools",
																				fournirPoolsResultHandler);
			
			RemoteObjectUtil.callService(op, idprofil);	
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATES
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					RESULT EVENT PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function fournirListePoolsGestionnaireResultHandler(re:ResultEvent):void
		{
			if(re.result)
	    	{
				var poolArray	:ArrayCollection 	= re.result as ArrayCollection;
				var lenPool		:int 				= poolArray.length;
				
				for(var i:int = 0;i < lenPool;i++)
				{
					var strgTemp:String = "";
						strgTemp = StringUtil.trim(poolArray[i].LIBELLE_POOL);
						strgTemp = strgTemp + " - " + StringUtil.trim(poolArray[i].LIBELLE_PROFIL);
					
					var pool:Pool 			= new Pool();
						pool.LIBELLE_POOL 		= poolArray[i].LIBELLE_POOL;
						pool.LIBELLE_PROFIL 	= poolArray[i].LIBELLE_PROFIL;
						pool.LIBELLE_CONCAT 	= strgTemp;
						pool.IDPOOL 			= poolArray[i].IDPOOL;
						pool.IDPROFIL 			= poolArray[i].IDPROFIL;
						pool.IDREVENDEUR 		= poolArray[i].IDREVENDEUR;
						
					listePools.addItem(pool);
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_POOLS));
			}
	    	else
	    		ConsoviewAlert.afficherError(text_errorPool, text_libelleError);
	    }

		private function fournirProfilEquipementsResultHandler(re:ResultEvent):void
	    {
			if(re.result)
			{
				var profilesArray	:ArrayCollection 	= re.result as ArrayCollection;
				var lenProfiles		:int 				= profilesArray.length;
				
				for(var i:int = 0; i < lenProfiles;i++)
				{
					if(profilesArray[i].SELECTED == 1)
		    			listeProfiles.addItem(profilesArray[i]);
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_PROFILES));
			}
			else
	    		ConsoviewAlert.afficherError(text_errorProfiles, text_libelleError);
	    }
		
		private function fournirProfilEquipementsForSavResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var profilesArray	:ArrayCollection 	= re.result as ArrayCollection;
				var lenProfiles		:int 				= profilesArray.length;
				trace("############## _lignesSelected : " + ObjectUtil.toString(profilesArray));
				for(var i:int = 0; i < lenProfiles;i++)
				{
					if(profilesArray[i].SELECTED == 1 && profilesArray[i].CODE_INTERNE != null && (profilesArray[i].CODE_INTERNE).toUpperCase() == "SAV")
						listeProfiles.addItem(profilesArray[i]);
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_PROFILES));
			}
			else
				ConsoviewAlert.afficherError(text_errorProfiles, text_libelleError);
		}
		
		private function fournirListeActionProfilCommandeResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var actionsArray	:ArrayCollection 	= re.result as ArrayCollection;
				var lenActions		:int 				= actionsArray.length;
				
				for(var i:int = 0; i < lenActions;i++)
				{
					if(actionsArray[i].IDINV_ACTIONS == 2066 || actionsArray[i].IDINV_ACTIONS == 2216)
					{
						var actionProfile:Object = new Object();
						if(actionsArray[i].IDINV_ACTIONS == 2066)
						{
							actionProfile.IDSEGEMENT 	= 1;
							actionProfile.MOBILE		= "SEGMENT";
							actionProfile.ACTIF			= Formator.formatInteger(actionsArray[i].SELECTED);
						}
						else
						{
							actionProfile.IDSEGEMENT 	= 1;
							actionProfile.FIXE			= "SEGMENT";
							actionProfile.ACTIF			= Formator.formatInteger(actionsArray[i].SELECTED);
						}
						
						listeActionsProfile.addItem(actionProfile);
					}
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_PRF_ACTIONS));
			}
			else
				ConsoviewAlert.afficherError(text_errorActionsProfile, text_libelleError);
		}
		
		private function fournirPoolsResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var actionsArray	:ArrayCollection 	= re.result as ArrayCollection;
				var lenActions		:int 				= actionsArray.length;
				
				for(var i:int = 0; i < lenActions;i++)
				{
					if(actionsArray[i].IDINV_ACTIONS == 2066 || actionsArray[i].IDINV_ACTIONS == 2216)
					{
						var actionProfile:Object = new Object();
						if(actionsArray[i].IDINV_ACTIONS == 2066)
						{
							actionProfile.IDSEGEMENT 	= 1;
							actionProfile.MOBILE		= "SEGMENT";
							actionProfile.ACTIF			= Formator.formatInteger(actionsArray[i].SELECTED);
						}
						else
						{
							actionProfile.IDSEGEMENT 	= 1;
							actionProfile.FIXE			= "SEGMENT";
							actionProfile.ACTIF			= Formator.formatInteger(actionsArray[i].SELECTED);
						}
						
						listeActionsProfile.addItem(actionProfile);
					}
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_PRF_ACTIONS));
			}
			else
				ConsoviewAlert.afficherError(text_errorActionsProfile, text_libelleError);
		}
		
	}
}