package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Collaborateur;
	import gestioncommande.entity.Matricule;
	import gestioncommande.entity.OrgaVO;
	import gestioncommande.events.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class EmployeService extends EventDispatcher
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		public var matrciuleAndInfos	:ArrayCollection;
		public var listeEmployes		:ArrayCollection;
		
		public var employe				:Collaborateur;
		
		public var matricule			:Matricule;
		
		public var champsPersoEmploye	:Object;
		
		private static var pathprocedure:String = "fr.consotel.consoview.M16.v2.EmployeService";

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function EmployeService()
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODE PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC METHODE - APPEL DE PROCEDURE
		//--------------------------------------------------------------------------------------------//
		
		public function getEmployFromMat(matriculesClob:String):void
		{
			matrciuleAndInfos = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				pathprocedure,
																				"getEmployFromMat",
																				getEmployFromMatResultHanler);		
			
			RemoteObjectUtil.callService(op, matriculesClob);
		}
		
		public function fournirListeEmployes(idPool:Number, clef:String = ""): void
		{
			listeEmployes = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				pathprocedure,
																				"fournirListeEmployes",
																				fournirListeEmployesResultHandler);
			
			RemoteObjectUtil.callService(op,idPool,
											clef);
		}		
		
		public function fournirListeEmployesV2(idPool:int, columnsearch:String, ordercolumn:String, typeorder:String, indexdebut:int, numberofrecords:int, clef:String = ''): void
		{
			listeEmployes = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				pathprocedure,
																				"fournirListeEmployesV2",
																				fournirListeEmployesResultHandler);
			
			RemoteObjectUtil.callService(op,idPool,
											columnsearch,
											ordercolumn,
											typeorder,
											indexdebut,
											numberofrecords,
											clef);
		}
		
		public function fournirDetailEmploye(idEmploye:int, idgroupeClient:int, idPool:int):void
		{
			employe = new Collaborateur();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				pathprocedure,
																				"fournirDetailEmploye",
																				fournirDetailEmployesResultHandler);

			RemoteObjectUtil.callService(op,idEmploye,
											idgroupeClient,
											idPool);  
		}
		
		public function createEmploye(value:Collaborateur, idPool:int, orgas:String):void
		{
			if(value.CIVILITE.length < 1 ||value.NOM == "" || value.PRENOM == ""  || value.MATRICULE == "")
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_remplir_les_champs_obligatoires__'), 'Consoview', null);
			else
			{
				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					pathprocedure,
																					"creationEmploye",
																					createEmployeHandler);
				
				RemoteObjectUtil.callService(op,idPool,
												value.IDSITE_PHYSIQUE,
												value.CLE_IDENTIFIANT,
												Number(value.CIVILITE),
												value.NOM,
												value.PRENOM,
												value.EMAIL,
												value.FONCTION_EMPLOYE,
												value.STATUS_EMPLOYE,
												value.COMMENTAIRE,
												value.CODE_INTERNE,
												value.REFERENCE_EMPLOYE,
												value.MATRICULE,
												value.INOUT,
												DateFunction.formatDateAsInverseString(value.DATE_ENTREE),
												DateFunction.formatDateAsInverseString(value.DATE_SORTIE),
												value.C1,
												value.C2,
												value.C3,
												value.C4,
												value.NIVEAU,
												orgas);
			}
		}
		
		public function createEmploye1(value:Collaborateur, idPool:int):void
		{
			if(value.CIVILITE.length < 1 ||value.NOM == "" || value.PRENOM == ""  || value.MATRICULE == "")
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_remplir_les_champs_obligatoires__'), 'Consoview', null);
			else
			{
				var xml:XML = formatCollaborateurToXML(value);				
				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					pathprocedure,
																					"creationEmploye1",
																					createEmployeHandler);
				
				RemoteObjectUtil.callService(op,idPool,
												xml.toString());
			
			}
		}
		
		public function updateEmploye(value:Collaborateur, idcollaborateur:int, idgroupeClient:int, orgas:String):void
		{
			if(value.CIVILITE.length < 1 ||value.NOM == "" || value.PRENOM == ""  || value.MATRICULE == "")
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_remplir_les_champs_obligatoires__'), 'Consoview', null);
			else
			{
				var xml:XML = formatCollaborateurToXML(value);				
				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					pathprocedure,
																					"updateEmploye",
																					updateEmployeHandler);
				
				RemoteObjectUtil.callService(op,idgroupeClient,
												idcollaborateur,
												value.IDSITE_PHYSIQUE,
												value.CLE_IDENTIFIANT,
												Number(value.CIVILITE),
												value.NOM,
												value.PRENOM,
												value.EMAIL,
												value.FONCTION_EMPLOYE,
												value.STATUS_EMPLOYE,
												value.COMMENTAIRE,
												value.CODE_INTERNE,
												value.REFERENCE_EMPLOYE,
												value.MATRICULE,
												value.INOUT,
												DateFunction.formatDateAsInverseString(value.DATE_ENTREE),
												DateFunction.formatDateAsInverseString(value.DATE_SORTIE),
												value.C1,
												value.C2,
												value.C3,
												value.C4,
												value.NIVEAU,
												orgas);
			}
		}
		
		public function updateEmploye1(value:Collaborateur, idCollaborateur:int):void
		{
			if(value.CIVILITE.length < 1 ||value.NOM == "" || value.PRENOM == ""  || value.MATRICULE == "")
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_remplir_les_champs_obligatoires__'), 'Consoview', null);
			else
			{
				var xml:XML = formatCollaborateurToXML(value);				
				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					pathprocedure,
																					"updateEmploye1",
																					updateEmployeHandler);
				
				RemoteObjectUtil.callService(op,idCollaborateur,
												xml.toString());
			}
		}
		
		public function fournirChampsPersoEmploye():void
		{			
				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					pathprocedure,
																					"fournirChampsPersoEmploye",
																					fournirChampsPersoEmployeResultHandler);
				
				RemoteObjectUtil.callService(op);
		}		
		
		public function getMatriculeAuto():void
		{			
			matricule = new Matricule();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				pathprocedure,
																				"getMatriculeAuto",
																				getMatriculeAutoResultHandler);
			
			RemoteObjectUtil.callService(op);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODE PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODE - RETOUR DE PROCEDURE
		//--------------------------------------------------------------------------------------------//
		
		private function getEmployFromMatResultHanler(re:ResultEvent):void
		{
			if(re.result)
			{
				matrciuleAndInfos = (re.result as ArrayCollection);
				dispatchEvent(new CommandeEvent(CommandeEvent.MARICULES_LISTED));
				dispatchEvent(new Event("matrciuleAndInfosLoaded"));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Impossible_de_r_cup_rer_les_matricules__'), 'Consoview');
			}
		}
		
		private function fournirListeEmployesResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				listeEmployes = mappingEmploye(re.result as ICollectionView);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.EMPLOYES_LISTED));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Impossible_de_r_cup_rer_les_collaborateu'), 'Consoview');
		}
		
		private function fournirDetailEmployesResultHandler(re:ResultEvent):void
		{     
			if(re.result)
			{
				var myInfos:ArrayCollection = new ArrayCollection(re.result as Array);
				
				if(myInfos.length > 0)
				{
					employe = mappingCollaborateur(myInfos[1][0], mappingOrganisations(myInfos[3]));
					
					dispatchEvent(new CommandeEvent(CommandeEvent.EMPLOYE_LISTED));
				}
				else
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Impossible_de_r_cup_rer_les_infos_du_col'), 'Consoview');
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Impossible_de_r_cup_rer_les_infos_du_col'), 'Consoview');
		}

		private function createEmployeHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new CommandeEvent(CommandeEvent.EMPLOYE_CREATED));
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Cr_ation_du_collaborateur_impossible'), 'Consoview');     
		}
		
		private function updateEmployeHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new CommandeEvent(CommandeEvent.EMPLOYE_UPDATED));
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Mise___jour_du_collaborateur_impossible'), 'Consoview');    
		}		
		
		private function fournirChampsPersoEmployeResultHandler(re:ResultEvent):void
		{
			champsPersoEmploye = formatChampsEmployePerso(re.result[0]);

			dispatchEvent(new CommandeEvent(CommandeEvent.EMPLOYE_CHAMPS));
		}
		
		private function getMatriculeAutoResultHandler(re:ResultEvent):void
		{
			matricule = Matricule.mapping(re.result);
			
			dispatchEvent(new CommandeEvent(CommandeEvent.MATRICULE_AUTO));
		}
		
		
		//--------------------------------------------------------------------------------------------//
		//					MAPPING
		//--------------------------------------------------------------------------------------------//
		
		private function mappingEmploye(values:ICollectionView):ArrayCollection
		{
			var retour : ArrayCollection = new ArrayCollection();
			
			if (values != null)
			{
				var cursor:IViewCursor = values.createCursor();
				
				while(!cursor.afterLast)
				{
					var employeObj:Collaborateur 	= new Collaborateur();                   
						employeObj.NOMPRENOM 		= cursor.current.NOM;
						employeObj.IDEMPLOYE 		= cursor.current.IDEMPLOYE;
						employeObj.IDGROUPE_CLIENT 	= cursor.current.IDGROUPE_CLIENT;
						employeObj.MATRICULE 		= cursor.current.MATRICULE;
						employeObj.CLE_IDENTIFIANT 	= cursor.current.CLE_IDENTIFIANT;
						employeObj.INOUT 			= cursor.current.INOUT;
						employeObj.NBROWS			= cursor.current.NB_ROWS;
						employeObj.NBRECORD			= cursor.current.NB_ROWS;
					
					if(employeObj.INOUT > 0)
						retour.addItem(employeObj);
					
					cursor.moveNext();
				}
			}
			
			return retour;
		}
		
		private function mappingCollaborateur(value:Object, orgas:ArrayCollection):Collaborateur
		{
			var employeObj:Collaborateur 	= new Collaborateur();
				employeObj.LCIVILITE 		= value.CIVILITE;
				employeObj.CIVILITE 		= value.IDCIVILITE;
				employeObj.NOMPRENOM 		= value.PRENOM + " " + value.NOM;
				employeObj.NOM				= value.NOM;
				employeObj.PRENOM			= value.PRENOM;
				employeObj.EMAIL 			= value.EMAIL;
				employeObj.FONCTION_EMPLOYE = value.FONCTION_EMPLOYE;
				employeObj.MATRICULE 		= value.MATRICULE;
				employeObj.CODE_INTERNE 	= value.CODE_INTERNE;
				employeObj.CLE_IDENTIFIANT 	= value.CLE_IDENTIFIANT;
				employeObj.INOUT 			= value.INOUT;
				employeObj.STATUS_EMPLOYE	= value.STATUS_EMPLOYE;
				employeObj.NIVEAU 			= value.NIVEAU;
				employeObj.BOOL_PUBLICATION = value.BOOL_PUBLICATION ;
				employeObj.COMMENTAIRE  	= value.COMMENTAIRE;
				employeObj.DATE_CREATION	= value.DATE_CREATION;
				employeObj.DATE_ENTREE		= value.DATE_ENTREE;
				employeObj.DATE_SORTIE		= value.DATE_SORTIE;
				employeObj.IDEMPLOYE 		= value.IDEMPLOYE;
				employeObj.IDGROUPE_CLIENT 	= value.IDGROUPE_CLIENT;
				employeObj.IDSITE_PHYSIQUE 	= value.IDSITE_PHYSIQUE;
				employeObj.C1 				= value.C1;
				employeObj.C2 				= value.C2;
				employeObj.C3 				= value.C3;
				employeObj.C4 				= value.C4;
				
				employeObj.LISTEORGANISATIONS = orgas;

			return employeObj;
		}
		
		public function mappingOrganisations(listeOrgas:ArrayCollection):ArrayCollection
		{
			var orgas	:ArrayCollection = new ArrayCollection();
			var len		:int = listeOrgas.length;
			
			for(var i :int = 0; i < len; i++)
			{
				var myOrga:OrgaVO 			 = new OrgaVO();
					myOrga.idCible 			 = listeOrgas[i].IDCIBLE;
					myOrga.libelleOrga 		 = listeOrgas[i].LIBELLE_ORGA;
//					myOrga.idCible 			 = listeOrgas[i].IDORGA_CIBLE;
					myOrga.idCible 			 = listeOrgas[i].IDCIBLE;
					myOrga.idRegleOrga 	 	 = listeOrgas[i].IDREGLE;
					myOrga.idOrga 			 = listeOrgas[i].IDORGA;
					myOrga.chemin 			 = listeOrgas[i].CHEMIN_CIBLE;
					myOrga.position 		 = listeOrgas[i].POSITION;
					myOrga.dateModifPosition = listeOrgas[i].DATE_MODIF_POSITION;
					myOrga.idSource 	 	 = listeOrgas[i].IDSOURCE;
				
				orgas.addItem(myOrga);
			}
			
			return orgas;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FORMAT
		//--------------------------------------------------------------------------------------------//
		
		private function formatChampsEmployePerso(value:Object):Object
		{
			var myChamps	:Object = new Object();
			var nbrChamps	:int = 10;
			
			for(var i:int = 0;i < nbrChamps;i++)
			{
				var id:String = 'C' + (i+1).toString();
				
				myChamps[id] = value[id];
				
				if(myChamps[id] == '' || myChamps[id] == null)
					myChamps[id] = 'Libellé ' + (i+1).toString();
			}
			
			return myChamps;
		}
		
		private function formatCollaborateurToXML(value:Collaborateur):XML
		{
			var xml:XML = <collaborateur></collaborateur>
				xml.appendChild("<civilite>"+value.CIVILITE+"</civilite>");
				xml.appendChild("<identifiant>"+value.CLE_IDENTIFIANT+"</identifiant>");
				xml.appendChild("<nom>"+value.NOM+"</nom>" );
				xml.appendChild("<prenom>"+value.PRENOM+"</prenom>");
				xml.appendChild("<email>"+value.EMAIL+"</email>");
				xml.appendChild("<niveau>"+value.NIVEAU+"</niveau>");
				xml.appendChild("<publication>"+value.BOOL_PUBLICATION+"</publication>");
				xml.appendChild("<inout>"+value.INOUT+"</inout>");
				xml.appendChild("<date_creation>"+ DateFunction.formatDateAsInverseString(value.DATE_CREATION)+"</date_creation>");
				xml.appendChild("<date_entree>"+ DateFunction.formatDateAsInverseString(value.DATE_ENTREE)+"</date_entree>");
				xml.appendChild("<date_sortie>"+ DateFunction.formatDateAsInverseString(value.DATE_SORTIE)+"</date_sortie>");
				xml.appendChild("<statut>"+value.STATUS_EMPLOYE+"</statut>");
				xml.appendChild("<fonction_employe>"+value.FONCTION_EMPLOYE+"</fonction_employe>");
				xml.appendChild("<code_interne>"+value.CODE_INTERNE+"</code_interne>");
				xml.appendChild("<reference>"+value.REFERENCE_EMPLOYE+"</reference>");
				xml.appendChild("<matricule>"+value.MATRICULE+"</matricule>");
				xml.appendChild("<telephone_fixe>"+value.TELEPHONE_FIXE+"</telephone_fixe>");
				xml.appendChild("<commentaire>"+value.COMMENTAIRE+"</commentaire>");
				xml.appendChild("<fax>"+value.FAX+"</fax>");
				xml.appendChild("<c1>"+value.C1+"</c1>");
				xml.appendChild("<c2>"+value.C2+"</c2>");
				xml.appendChild("<c3>"+value.C3+"</c3>" );
				xml.appendChild("<c4>"+value.C4+"</c4>");
				
			return xml;
		}

	}
}