package gestioncommande.services
{
	import composants.util.ConsoviewUtil;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.RessourcesElements;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import session.SessionUserObject;
	
	public class PublicMethodeStatic extends EventDispatcher
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		public static var myAbonnements		:ArrayCollection = new ArrayCollection();
		public static var myOptions			:ArrayCollection = new ArrayCollection();
		
		public static var myFilterAbo		:ArrayCollection = new ArrayCollection();
		public static var myFilterOpt		:ArrayCollection = new ArrayCollection();
		
		private static var _isObligatoire	:Boolean = false;

		//--------------------------------------------------------------------------------------------//
		//					CONSRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function PublicMethodeStatic()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		public static function fournirAbonnementsOptions(idOperateur:int, idProfilEquipement:int, isObligatoire:Boolean):void
		{
			_isObligatoire = isObligatoire;
			
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation("fr.consotel.consoview.M16.v2.RessourceService",
																			"fournirListeAbonnementsOptions",
																			fournirAbonnementsOptionsResultHandler);
			RemoteObjectUtil.callService(op,idOperateur,
											idProfilEquipement,
											SessionUserObject.singletonSession.IDSEGMENT);	
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					RESULT EVENT PROCEDURES
			//--------------------------------------------------------------------------------------------//  
		
		private static function fournirAbonnementsOptionsResultHandler(re:ResultEvent):void
		{
			SessionUserObject.singletonSession.ABONNEMENTS			= new ArrayCollection();
			SessionUserObject.singletonSession.OPTIONS				= new ArrayCollection();
			SessionUserObject.singletonSession.FILTERABONNEMENTS	= new ArrayCollection();
			SessionUserObject.singletonSession.FILTEROPTIONS		= new ArrayCollection();
			
			myAbonnements	= new ArrayCollection();
			myOptions		= new ArrayCollection();
			
			if(re.result)
			{
				var i:int = 0;
				
				if(re.result.length > 0)
				{
					var abonnements	:ArrayCollection = re.result.ABONNEMENTS as ArrayCollection;
					var lenAbo		:int = abonnements.length;
					
					for(i = 0;i < lenAbo;i++)
					{
						if(_isObligatoire)
							myAbonnements.addItem(RessourcesElements.formatAbonnementObligatoire(abonnements[i]));
						else
							myAbonnements.addItem(RessourcesElements.formatAbonnement(abonnements[i]));
					}
					
					var options		:ArrayCollection = re.result.OPTIONS as ArrayCollection;
					var lenOpt		:int = options.length;
					
					for(i = 0;i < lenOpt;i++)
					{
						if(_isObligatoire)
							myOptions.addItem(RessourcesElements.formatOptionsObligatoire(options[i]));
						else
							myOptions.addItem(RessourcesElements.formatOptions(options[i]));
					}
					
					addFilterAboOpt(myAbonnements, myOptions);
					
					SessionUserObject.singletonSession.ABONNEMENTS			= ObjectUtil.copy(myAbonnements);
					SessionUserObject.singletonSession.OPTIONS				= ObjectUtil.copy(myOptions);
					SessionUserObject.singletonSession.FILTERABONNEMENTS	= ObjectUtil.copy(myFilterAbo);
					SessionUserObject.singletonSession.FILTEROPTIONS		= ObjectUtil.copy(myFilterOpt);
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTIONS
		//--------------------------------------------------------------------------------------------//  
		
		private static function addFilterAboOpt(abonnements:ICollectionView, options:ICollectionView):void
		{
			var cursor 	:IViewCursor;
			var produit	:Object;
			var index	:int = 0;
			
			myFilterAbo	= new ArrayCollection();
			myFilterOpt	= new ArrayCollection();
			
			if(abonnements != null)
			{
				cursor = abonnements.createCursor();
				
				while(!cursor.afterLast)
				{
					index = ConsoviewUtil.getIndexById(myFilterAbo, "IDTHEME_PRODUIT", cursor.current.IDTHEME_PRODUIT);
					
					if(index < 0)
					{
						produit 					= new Object();
						produit.IDTHEME_PRODUIT 	= cursor.current.IDTHEME_PRODUIT;
						produit.LIBELLE 			= cursor.current.LIBELLE;
						produit.SELECTED		 	= true;
						
						myFilterAbo.source.push(produit);
					}	 
					
					cursor.moveNext();
				}
			}
			
			if(options != null)
			{
				cursor = options.createCursor();
				
				while(!cursor.afterLast)
				{
					index = ConsoviewUtil.getIndexById(myFilterOpt, "IDTHEME_PRODUIT", cursor.current.IDTHEME_PRODUIT);
					
					if(index < 0)
					{
						produit 					= new Object();
						produit.IDTHEME_PRODUIT 	= cursor.current.IDTHEME_PRODUIT;
						produit.LIBELLE 			= cursor.current.LIBELLE;
						produit.SELECTED		 	= true;
						
						myFilterOpt.source.push(produit);
					}
					
					cursor.moveNext();
				}
			}
		}
		
	}
}