package gestioncommande.services.article
{
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Commande;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.remoting.RemoteObject;
	
	
	public class ArticleService extends EventDispatcher
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		[Bindable] 
		public var myDatas		:ArticleDatas;
		public var myHandlers	:ArticleHandler;
		
		private var op:RemoteObject = new RemoteObject('ColdFusion');
		private var _serviceBack:String = 'fr.consotel.consoview.M16.v2.ArticleService';
	
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//		    
		
		public function ArticleService()
		{
			this.myDatas 		= new ArticleDatas();
			this.myHandlers 	= new ArticleHandler(myDatas);
		}
			
		public function getListeArticle(cmd:Commande, segment:int): void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_serviceBack,
				'fournirListeArticle', myHandlers.fournirListeArticleHandler);
			
			var data:Object = new Object();
			data.p_operateurid 			= cmd.IDOPERATEUR;
			data.p_idprofil_equipement 	= cmd.IDPROFIL_EQUIPEMENT;
			data.p_segment 				= segment;
			
			RemoteObjectUtil.callService(op, 'OK' ,data);				
			
		}
		
		public function enregisterArticles(		p_idcommande			:int, 								
												p_articles          	:ArrayCollection,
												p_idpoolgestionnaire	:int,
												p_is_last  :int = 0
											): void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_serviceBack,
				'enregisterArticles', myHandlers.enregisterArticlesHandler);
			
			
			var data:Object = new Object();
			data.p_idcommande 			= p_idcommande;
			data.p_articles 			= p_articles;
			data.p_idpoolgestionnaire 	= p_idpoolgestionnaire;
			data.p_is_last 				= p_is_last;
			
			RemoteObjectUtil.callService(op, 'OK', data);				
			
		}
		
	}
}