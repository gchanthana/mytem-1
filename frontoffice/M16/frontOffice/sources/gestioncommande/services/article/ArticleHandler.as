package gestioncommande.services.article
{
	import composants.util.ConsoviewAlert;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;

	public class ArticleHandler
	{
		private var myDatas:ArticleDatas;
		
		public function ArticleHandler(equipmDatas:ArticleDatas)
		{
			this.myDatas = equipmDatas;
		}
		
		public function fournirListeArticleHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatListeEquipements(re.result as ArrayCollection);
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_de_la_liste_des__quipements'), 'Consoview');
		}
		
		public function enregisterArticlesHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatEnregistrementArticles(re.result as ArrayCollection);
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_de_la_liste_des__quipements'), 'Consoview');
		}
	}
}