package gestioncommande.services
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import commandemobile.system.ElementHistorique;
	
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.Historique;
	import gestioncommande.events.CommandeEvent;
	
	import session.SessionUserObject;
	
	public class WorkflowService extends EventDispatcher
	{	
		public static const CFC_WorkFlowService:String="fr.consotel.consoview.M16.v2.WorkFlowService";

		
		public var listeActions				:ArrayCollection = new ArrayCollection();
		public var listeHistoPool			:ArrayCollection = new ArrayCollection();	
		public var listeHistoUser			:ArrayCollection = new ArrayCollection();
		public var listeHistoType			:ArrayCollection = new ArrayCollection();

		public var boolLivree				:Boolean = false;
	    public var boolExpedie				:Boolean = false;
		public var isRefAut					:Number=0;
		
		private var _selectedCommande		:Commande;		
		private var _firstSelectedAction	:Number;
		private var _historiqueWorkflow		:ArrayCollection;
		
		//--------------------------------------------------------------------------------------------//
		//					GETTER/SETTER
		//--------------------------------------------------------------------------------------------//
		
	    public function set historiqueWorkFlow(values:ArrayCollection): void
	    {
	    	_historiqueWorkflow = values
	    }

	    public function get historiqueWorkFlow(): ArrayCollection
	    {
	    	return _historiqueWorkflow 
	    }

		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
	    
		public function WorkflowService()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC METHODE - APPEL DE PROCEDURE
		//--------------------------------------------------------------------------------------------//		
	
		public function fournirActionsNoRemote(idEtat:Number, cmd:Commande, idsegment : Number = 0): void
		{
			var myActions:ArrayCollection = ObjectUtil.copy(SessionUserObject.singletonSession.LISTEACTIONSUSER) as ArrayCollection;
			var _idSegment:int = idsegment;		
			
			//si pas de segment dans le context, on regarde le segment de la commande qui est monosegment pour le moment 01/10/2010
			
			if(_idSegment == 0)// pas de params passée on prend le segment de la commande sélectionné
			{
				if(cmd.SEGMENT_MOBILE == 1)
				{
					_idSegment = 1;
				}
				else if(cmd.SEGMENT_FIXE == 1)
				{
					_idSegment = 2;
				}
				else if(cmd.SEGMENT_DATA == 1)
				{
					_idSegment = 3;
				}
				
			}
			
			_selectedCommande 	= cmd;
			listeActions 		= new ArrayCollection();
			
			if(idEtat > 0)
			{
				var actions	:ArrayCollection = formaterActions(myActions as ArrayCollection);
				var len		:int = actions.length;
				
				for(var i:int = 0;i < len;i++)
				{
					if((actions[i] as Action).IDETAT == idEtat && (actions[i] as Action).SEGMENT == _idSegment)
						listeActions.addItem(actions[i]);
				}
			}
			
			//dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ACTIONS));	
		}
		
		public function fournirActionsNoRemoteAllPools(idEtat:Number, cmd:Commande, listeActionsProfil:ArrayCollection, idsegment : Number = 0): ArrayCollection
		{
			var myActions:ArrayCollection = ObjectUtil.copy(listeActionsProfil) as ArrayCollection;
			var _idSegment:int = idsegment;		
			
			//si pas de segment dans le context, on regarde le segment de la commande qui est monosegment pour le moment 01/10/2010
			
			if(_idSegment == 0)// pas de params passée on prend le segment de la commande sélectionné
			{
				if(cmd.SEGMENT_MOBILE == 1)
				{
					_idSegment = 1;
				}
				else if(cmd.SEGMENT_FIXE == 1)
				{
					_idSegment = 2;
				}
				else if(cmd.SEGMENT_DATA == 1)
				{
					_idSegment = 3;
				}
				
			}
			
			_selectedCommande 	= cmd;
			listeActions 		= new ArrayCollection();
			
			if(idEtat > 0)
			{
				var actions	:ArrayCollection = formaterActions(myActions as ArrayCollection);
				var len		:int = actions.length;
				
				for(var i:int = 0;i < len;i++)
				{
					if((actions[i] as Action).IDETAT == idEtat && (actions[i] as Action).SEGMENT == _idSegment)
						listeActions.addItem(actions[i]);
				}
			}
			
			return 	listeActions;
		}
		
		public function fournirActions(idEtat:Number, cmd:Commande, idsegment : Number = 0): void
	    {
			var _idSegment:int = idsegment;		
			
			//si pas de segment dans le context, on regarde le segment de la commande qui est monosegment pour le moment 01/10/2010
			
			if(_idSegment == 0)// pas de params passée on prend le segment de la commande sélectionné
			{
				if(cmd.SEGMENT_MOBILE == 1)
				{
					_idSegment = 1
				}
				else if(cmd.SEGMENT_FIXE == 1)
				{
					_idSegment = 2
				}
				else if(cmd.SEGMENT_DATA == 1)
				{
					_idSegment = 3
				}
				
			}

	    	_selectedCommande 	= cmd;
	    	listeActions 		= new ArrayCollection();
	    	
	    	if(idEtat > 0)
	    	{
	    		var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			CFC_WorkFlowService,
	    																			"fournirListeActionsPossibles",
	    																			fournirActionsResultHandler);
	    		RemoteObjectUtil.callService(op,idEtat,
									    		cmd.IDPOOL_GESTIONNAIRE,
									    		_idSegment)
	    	}
	    	
	    }
	    
	    public function faireActionWorkFlow(action:Action, cmd:Commande, loadhistorque:Boolean = true):void
	    {
	    	_selectedCommande 	= cmd;
	    	action.DATE_ACTION 	= new Date();
	    	action.DATE_HOURS	= formatDate(action.DATE_ACTION) + " " + formatHour(action.DATE_ACTION);
	    
	    	if(action.IDACTION > 0)
	    	{
    			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			CFC_WorkFlowService,
	    																			"faireActionWorkFlow",
	    																			faireActionWorkFlowResultHandler);
	    																			
	    		RemoteObjectUtil.callService(op,action.IDACTION,
	    										action.DATE_HOURS,
	    										action.COMMENTAIRE_ACTION,
	    										cmd.IDCOMMANDE,
	    										cmd.BOOL_MAIL,
	    										cmd.ID_MAIL);	
	    	}
	    }
		
		/*
		     procédure pour savoir si une commande a déjà fait le renseignement de ref
		     0:non , 1:oui
		   */
		public function isRefActionAut(idcmde:int):void
		{
			if(idcmde > 0)
			{
				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					CFC_WorkFlowService,
					"iscmdRef",
					isRefAutHandler);
				
				RemoteObjectUtil.callService(op,idcmde);	
			}
		}
		
		private function isRefAutHandler(re:ResultEvent): void
		{
			isRefAut=re.result as Number;
			dispatchEvent(new Event("SHOW_REF_ITEM"));
		}
		
		
	    
	    /**
	     * fournit la liste des etats succesifs d'une commande ordonn�es par
	     * SYSDATE_ACTION d�croissantes. On fournira pour chaque enregistrement, et le
	     * detail de l'etat, et le d�tail de l'action qui est � l'origine de cet �tat.
	     * 
	     * @param commande    commande
	     */
	    public function fournirHistoriqueEtatsCommande(commande:Commande): void
	    {
	    	_selectedCommande = commande;
	    	
	    	if(commande != null)
	    	{
		    	if(commande.IDCOMMANDE > 0)
		    	{
		    		var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
		    																			CFC_WorkFlowService,
		    																			"fournirHistoriqueEtatsCommande",
		    																			fournirHistoriqueEtatsCommandeResultHandler);
					RemoteObjectUtil.callService(op,commande.IDCOMMANDE)	
		    	}
	    	}
	    }
		
		/*
		 * appel d'une fonction en remoting qui fournit l'historique des etats d'une commande
		*/
		public function fournirHistoriqueEtatsCommandeSilent(commande:Commande): void
		{
			_selectedCommande = commande;
			
			if(commande != null)
			{
				if(commande.IDCOMMANDE > 0)
				{
					var op:AbstractOperation = RemoteObjectUtil.getSilentOperation(CFC_WorkFlowService,
																					"fournirHistoriqueEtatsCommande",
																					fournirHistoriqueEtatsCommandeResultHandler);
					RemoteObjectUtil.callSilentService(op,commande.IDCOMMANDE)	
				}
			}
		}
		
		public function getHistoriquePool(idPool:int, dateDebut:Date, dateFin:Date, clef:String, boolSilent:Boolean = true): void
		{
			var op:AbstractOperation;
			listeHistoPool = new ArrayCollection();
			listeHistoUser = new ArrayCollection();
			listeHistoType = new ArrayCollection();

			if(boolSilent){
				op = RemoteObjectUtil.getSilentOperation(CFC_WorkFlowService,
																			"getHistoriqueAction",
																				getHistoriquePoolResultHandler);		
				RemoteObjectUtil.callSilentService(op,idPool,
											dateDebut,
											dateFin,
											clef);
			}else{
				op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			CFC_WorkFlowService,
																			"getHistoriqueAction",
																				getHistoriquePoolResultHandler);		
				RemoteObjectUtil.callService(op,idPool,
											dateDebut,
											dateFin,
											clef);
			}
			
				
		}

	    //--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODE - RETOUR DE PROCEDURE
		//--------------------------------------------------------------------------------------------//
	    
	    private function fournirActionsResultHandler(re:ResultEvent): void
	    {
			if(re.result)
			{
				listeActions = formaterActions(re.result as ArrayCollection);
				
				//dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ACTIONS));	
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur___') + re.result.toString(), 'Consoview');			
	    }
	    
	    private function faireActionWorkFlowResultHandler(re:ResultEvent): void
	    {
	    	if(re.result > 0)
	    	{
	    		dispatchEvent(new CommandeEvent(CommandeEvent.SEND_ACTION));
	    	}
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur___') + re.result.toString(), 'Consoview');	
	    }
		
		private function getHistoriquePoolResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				listeHistoPool = addObjectSelected(re.result as ArrayCollection);
				
				getObjectForFiltre();
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_POOL_HIST));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview', null);
		}
	    
	    //--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODE
		//--------------------------------------------------------------------------------------------//	
		    
	    private function addModificationObject():Action
	    {
		    var actionObj:Action 				= new Action();					
				actionObj.CODE_ACTION 			= "VIEW_MODIFCMDE";	
				actionObj.COMMENTAIRE_ACTION 	= "VIEW_MODIFCMDE";	
				actionObj.IDACTION 				= -1;	
				actionObj.IDETAT				= -1;
				actionObj.LIBELLE_ACTION		= ResourceManager.getInstance().getString('M16', 'Modifier_la_commande');
				actionObj.LIBELLEETAT_ENGENDRE 	= "VIEW_MODIFCMDE";					
				actionObj.MESSAGE 				= "VIEW_MODIFCMDE";
				actionObj.EXP 					= "VIEW_MODIFCMDE";
				actionObj.DEST 					= "VIEW_MODIFCMDE";

			return actionObj;		
	    }
	    
	     /**
	     * Met � jour l'historique des �tat d'une commande
	     * 
	     * @param event    event
	     */
	    private function fournirHistoriqueEtatsCommandeResultHandler(re:ResultEvent): void
	    {
	    	historiqueWorkFlow = formaterHistorique(re.result as ArrayCollection);
	    	
			boolLivree = ConsoviewUtil.isLabelInArray("R","CODE_ETAT", historiqueWorkFlow.source);
	    	
			boolExpedie = ConsoviewUtil.isLabelInArray("EXP","CODE_ETAT", historiqueWorkFlow.source);
	    	
			dispatchEvent(new Event("historiqueFinished"));
			
			dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_WFLOW_HIST));
	    }
	    
	    //--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODE
		//--------------------------------------------------------------------------------------------//
	    
	    private function formaterActions(values:ICollectionView):ArrayCollection
	    {
	    	var retour	:ArrayCollection = new ArrayCollection();
			
			if (values != null)
			{	
				_firstSelectedAction = -1;
				var cursor 	:IViewCursor 	 = values.createCursor();
				
				while(!cursor.afterLast)
					
				{
					var actionObj:Action 				= new Action();
									
						actionObj.CODE_ACTION 			= cursor.current.CODE_ACTION;	
						actionObj.COMMENTAIRE_ACTION 	= cursor.current.COMMENTAIRE_ACTION;	
						actionObj.IDACTION 				= cursor.current.IDINV_ACTIONS;	
						actionObj.IDETAT				= cursor.current.IDINV_ETAT;
						actionObj.IDETAT_ENGENDRE		= cursor.current.IDETAT_ENGENDRE;
						actionObj.LIBELLE_ACTION		= cursor.current.LIBELLE_ACTION;
						actionObj.LIBELLEETAT_ENGENDRE 	= cursor.current.LIBELLE_ETAT;					
						actionObj.MESSAGE 				= cursor.current.MESSAGE;
						actionObj.EXP 					= cursor.current.EXP;
						actionObj.DEST 					= cursor.current.DEST;
						
					if(cursor.current.hasOwnProperty('SEGMENT'))
					{
						actionObj.SEGMENT = cursor.current.SEGMENT;
					}
					
					retour.addItem(actionObj);		
					
					cursor.moveNext();
					
					_firstSelectedAction = 0;
				
				}
				
					
			}
			return retour 
	    }
	    
		private function formaterHistorique(values:ICollectionView):ArrayCollection
		{
			var retour:ArrayCollection = new ArrayCollection();
			
			if(values != null)
			{
				var cursor:IViewCursor = values.createCursor();
				
				while(!cursor.afterLast)
					
				{
					var hitoriqueObj:Historique 				= new Historique();
					
					
						hitoriqueObj.PATRONYME_USERACTION 		= cursor.current.NOM;
						hitoriqueObj.DATE_INSERT 				= cursor.current.DATE_INSERT;
						hitoriqueObj.DATE_ACTION 				= cursor.current.DATE_INSERT;
						hitoriqueObj.DATE_STRING				= Formator.formatDate(hitoriqueObj.DATE_ACTION);
						hitoriqueObj.IDACTION 					= cursor.current.IDINV_ACTIONS;
						hitoriqueObj.LIBELLE_ACTION 			= cursor.current.LIBELLE_ACTION;					
						hitoriqueObj.PATRONYME_USERACTION 		= cursor.current.NOM;					
						hitoriqueObj.IDETAT 					= cursor.current.IDINV_ETAT;
						hitoriqueObj.LIBELLE_ETAT 				= cursor.current.LIBELLE_ETAT;	
						hitoriqueObj.REF_CALCUL 				= cursor.current.REF_CALCUL;
						hitoriqueObj.ENTRAINE_CLOTURE 			= cursor.current.ENTRAINE_CLOTURE;
						hitoriqueObj.ENTRAINE_CREATION 			= cursor.current.ENTRAINE_CREATION;	
						hitoriqueObj.CHANGER_ETAT_INVENTAIRE 	= cursor.current.CHANGER_ETAT_INVENTAIRE;
						hitoriqueObj.COMMENTAIRE_ETAT 			= cursor.current.COMMENTAIRE_ETAT;
						hitoriqueObj.LIBELLE_ETAT 				= cursor.current.LIBELLE_ETAT;
						hitoriqueObj.DISPLAY_MAIL 				= cursor.current.DISPLAY_MAIL;
						hitoriqueObj.CODE_ETAT 					= cursor.current.CODE_ETAT;
						hitoriqueObj.EMAIL 						= cursor.current.EMAIL;
						hitoriqueObj.IDUSER_CREATE 				= cursor.current.IDUSER_CREATE;
						hitoriqueObj.CHANGER_ETAT_PARC 			= cursor.current.CHANGER_ETAT_PARC;
						
												
						retour.addItem(hitoriqueObj);	    			
						cursor.moveNext();
						
					
				}	
			}
			return retour;
		}
		

		private function getObjectForFiltre():void
		{
			var listUserTemp	:ArrayCollection = new ArrayCollection();
			var listTypeTemp	:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0;i < listeHistoPool.length;i++)
			{
				listUserTemp.addItem(listeHistoPool[i]);
				listTypeTemp.addItem(listeHistoPool[i]);
			}
			
			listeHistoUser = containThis(listUserTemp, true);
			listeHistoType = containThis(listTypeTemp, false);
		}
		
		private function containThis(values:ArrayCollection, bool:Boolean):ArrayCollection
		{
			var arrayAdded	:ArrayCollection = new ArrayCollection();
			
			if(bool)
				arrayAdded = containQUI(values);
			else
				arrayAdded = containType(values);
			
			return arrayAdded;
		}
		
		private function containQUI(values:ArrayCollection):ArrayCollection
		{
			var arrayAdded:ArrayCollection = new ArrayCollection();
			
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
				
				while(!cursor.afterLast)
				{
					if(ConsoviewUtil.getIndexById(arrayAdded,"APP_LOGINID",cursor.current.APP_LOGINID) < 0)
						arrayAdded.source.push({APP_LOGINID:cursor.current.APP_LOGINID,QUI:cursor.current.QUI});
					
					cursor.moveNext();
				}
			}
			
			return arrayAdded;
		}
		
		private function containType(values:ArrayCollection):ArrayCollection
		{
			var arrayAdded	:ArrayCollection = new ArrayCollection();
			var libelle		:String = "";
			
			if (values != null)
			{
				var tri:Sort = new Sort();
				tri.fields = [new SortField("ACTION", true)];
				values.sort = tri;
				values.refresh();
				values.sort = null;
				
				for(var i:int = 0;i < values.length;i++)
				{
					if(libelle != values[i].ACTION)
					{
						libelle = values[i].ACTION;
						
						if(values[i].ACTION)
							arrayAdded.addItem(values[i]);
					}
				}
			}
			
			return arrayAdded;
		}
		
		private function numberInString(valueLibelleType:String):Boolean
		{
			var OK:Boolean = false;
			
			for(var i:int = 0;i < 9;i++)
			{
				if(valueLibelleType.indexOf(i.toString()) > 0)
					return true;
			}
			
			return OK;
		}
		
		private function addObjectSelected(arrayToAddObject:ArrayCollection):ArrayCollection
		{
			var object		:Object;
			var arrayAdded	:ArrayCollection = new ArrayCollection();
			var diff:int = -1;
			var diffC:int = -1;
			
			for(var i:int = 0;i < arrayToAddObject.length;i++)
			{
				object = new Object();
				object = arrayToAddObject[i] as Object;
				if((arrayToAddObject[i].IDACTION!=diff)||(parseInt(arrayToAddObject[i].IDCDE)!=diffC)){
				
				if(arrayToAddObject[i].COMMANDE == null)
					object.COMMANDE = "";
				
				if(arrayToAddObject[i].SAV == null)
					object.SAV = "";
				
				diff=arrayToAddObject[i].IDACTION;
				diffC=arrayToAddObject[i].IDCDE;
				object.SELECTED = false;
				
				arrayAdded.addItem(object);
				
				}
			}
			
			return arrayAdded;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FORMATEURS
		//--------------------------------------------------------------------------------------------//
		
	    private function formatDate(dateobject:Object):String
		{
			var day:String = dateobject.date;
			if(day.length < 2)
			{day = "0" + day;}
			var month:int = Number(dateobject.month) + 1;
			var temp:String = month.toString();
			if(temp.length < 2)
			{temp = "0" + temp;}
			var year:String = dateobject.fullYear;
			return year + "/" + temp + "/" + day;
		}
		
		private function formatHour(dateobject:Date):String
		{
			var hours	:String = dateobject.hours.toString();
			var minutes	:String = dateobject.minutes.toString();
			var seconds	:String = dateobject.seconds.toString();

			return hours + ":" + minutes + ":" + seconds;
		}

	}
}
