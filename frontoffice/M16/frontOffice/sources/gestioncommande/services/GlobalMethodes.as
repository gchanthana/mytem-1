package gestioncommande.services
{
	import composants.util.ConsoviewUtil;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.RessourcesElements;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.mxml.RemoteObject;
	import mx.utils.ObjectUtil;
	
	import session.SessionUserObject;
	
	public class GlobalMethodes extends EventDispatcher
	{

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
				
		//--------------------------------------------------------------------------------------------//
		//					CONSRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function GlobalMethodes()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		public function fournirAbonnementsOptions(idOperateur:int, idProfilEquipement:int):void
		{	
//			var remoteObj:RemoteObject=new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION); // "ColdFusion"
//				remoteObj.source="fr.consotel.consoview.M16.v2.RessourceService";
//				remoteObj.showBusyCursor=true;
//				remoteObj.addEventListener(ResultEvent.RESULT,fournirAbonnementsOptionsResultHandler);
//				remoteObj.fournirAbonnementsOptions(idOperateur, idProfilEquipement, SessionUserObject.singletonSession.IDSEGMENT);
			// var f:Function=remoteObj["fournirAbonnementsOptions"]; f();
			
			
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation("fr.consotel.consoview.M16.v2.RessourceService",
																			"fournirAbonnementsOptions",
																			fournirAbonnementsOptionsResultHandler);
			RemoteObjectUtil.callSilentService(op,idOperateur,
											idProfilEquipement,
											SessionUserObject.singletonSession.IDSEGMENT);
			
			
			
			trace("passage 2");
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					RESULT EVENT PROCEDURES
			//--------------------------------------------------------------------------------------------//  
		
		private function fournirAbonnementsOptionsResultHandler(re:ResultEvent):void
		{
			SessionUserObject.singletonSession.ABONNEMENTS			= new ArrayCollection();
			SessionUserObject.singletonSession.ABONNEMENTS_OBL		= new ArrayCollection();
			SessionUserObject.singletonSession.ABONNEMENTS_DEF		= new ArrayCollection();
			SessionUserObject.singletonSession.OPTIONS				= new ArrayCollection();
			SessionUserObject.singletonSession.OPTIONS_OBL			= new ArrayCollection();
			SessionUserObject.singletonSession.OPTIONS_DEF			= new ArrayCollection();
			SessionUserObject.singletonSession.FILTERABONNEMENTS	= new ArrayCollection();
			SessionUserObject.singletonSession.FILTEROPTIONS		= new ArrayCollection();

			var i:int = 0;
			
			if(re.result)
			{
				var abonnements	:ArrayCollection = re.result.ABONNEMENTS as ArrayCollection;
				var lenAbo		:int = abonnements.length;
				
				for(i = 0;i < lenAbo;i++)
				{
					SessionUserObject.singletonSession.ABONNEMENTS_OBL.addItem(RessourcesElements.formatAbonnementObligatoire(abonnements[i]));
					SessionUserObject.singletonSession.ABONNEMENTS.addItem(RessourcesElements.formatAbonnement(abonnements[i]));
				}
				
				abonnements = re.result.ABONNEMENTS_DEFAULT as ArrayCollection;
				lenAbo		= abonnements.length;
				
				for(i = 0;i < lenAbo;i++)
				{
					SessionUserObject.singletonSession.ABONNEMENTS_DEF.addItem(RessourcesElements.formatAbonnementObligatoire(abonnements[i]));
				}
				
				var options		:ArrayCollection = re.result.OPTIONS as ArrayCollection;
				var lenOpt		:int = options.length;
				
				for(i = 0;i < lenOpt;i++)
				{
					SessionUserObject.singletonSession.OPTIONS_OBL.addItem(RessourcesElements.formatOptionsObligatoire(options[i]));
					SessionUserObject.singletonSession.OPTIONS.addItem(RessourcesElements.formatOptions(options[i]));
				}
				
				options	= re.result.OPTIONS_DEFAULT as ArrayCollection;
				lenOpt	= abonnements.length;
				
				for(i = 0;i < lenOpt;i++)
				{
					SessionUserObject.singletonSession.OPTIONS_DEF.addItem(RessourcesElements.formatAbonnementObligatoire(abonnements[i]));
				}
				
				addFilterAboOpt(SessionUserObject.singletonSession.ABONNEMENTS, SessionUserObject.singletonSession.OPTIONS);
			}
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//  
		
		private function addFilterAboOpt(abonnements:ICollectionView, options:ICollectionView):void
		{
			var cursor 	:IViewCursor;
			var produit	:Object;
			var index	:int = 0;
			
			SessionUserObject.singletonSession.FILTERABONNEMENTS = new ArrayCollection();
			SessionUserObject.singletonSession.FILTEROPTIONS	 = new ArrayCollection();
			
			if(abonnements != null)
			{
				cursor = abonnements.createCursor();
				
				while(!cursor.afterLast)
				{
					index = ConsoviewUtil.getIndexById(SessionUserObject.singletonSession.FILTERABONNEMENTS, "IDPRODUIT", cursor.current.IDPRODUIT);
					
					if(index < 0)
					{
						produit 					= new Object();
						produit.IDTHEME_PRODUIT 	= cursor.current.IDPRODUIT;
						produit.LIBELLE 			= cursor.current.LIBELLETHEME;
						produit.SELECTED		 	= true;
						
						SessionUserObject.singletonSession.FILTERABONNEMENTS.source.push(produit);
					}	 
					
					cursor.moveNext();
				}
			}
			
			if(options != null)
			{
				cursor = options.createCursor();
				
				while(!cursor.afterLast)
				{
					index = ConsoviewUtil.getIndexById(SessionUserObject.singletonSession.FILTEROPTIONS, "IDPRODUIT", cursor.current.IDPRODUIT);
					
					if(index < 0)
					{
						produit 					= new Object();
						produit.IDTHEME_PRODUIT 	= cursor.current.IDPRODUIT;
						produit.LIBELLE 			= cursor.current.LIBELLETHEME;
						produit.SELECTED		 	= true;
						
						SessionUserObject.singletonSession.FILTEROPTIONS.source.push(produit);
					}
					
					cursor.moveNext();
				}
			}
		}
		
	}
}