package gestioncommande.services.organisation
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparcmobile.resiliationLigne.messages.GestionParcMobileMessages;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	/**
	 * <b>Classe <code>OrganisationServices</code></b>
	 * <p><pre>
	 * <u>Description :</u>
	 * Appelle les procédures liées aux organisations.
	 * 
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 20.07.2010
	 * 
	 * </pre></p>
	 * 
	 */
	public class OrganisationServices
	{
		// VARIABLES------------------------------------------------------------------------------
		
		public var myDatas 		: OrganisationDatas;
		
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLICS FONCTIONS-------------------------------------------------------------------------
		
		/**
		 * <b>Constructeur de la class <code>OrganisationServices()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Instancie les datas.
		 * Instancie les handlers.
		 * </pre></p>
		 * 
		 */	
		public function OrganisationServices()
		{
			myDatas 	= new OrganisationDatas();
		}
		
		/**
		 * <b>Fonction <code>getNodesOrga(idOrga:int)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer :
		 * - Tous les noeuds d'une organisation précédent une feuille.
		 * - La structure de l'organisation.
		 * </pre></p>
		 * 
		 * @param idOrga:int.
		 * @return void.
		 */	
		public function getNodesOrga(idOrga:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M111.Organisation",
																				"getNodesOrga",
																				getNodesOrgaResultHandler);
			
			RemoteObjectUtil.callService(op, idOrga);
		}
		
		/**
		 * <b>Fonction <code>getListOrgaCliente()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer :
		 * - Toutes les informations liées aux organisations du collaborateurs.
		 * </pre></p>
		 * 
		 * @return void.
		 */	
		public function getListOrgaCliente():void 
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.RAGO.Rules",
																				"getListOrgaCliente", 
																				getListOrgaClienteResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		// PUBLICS FONCTIONS-------------------------------------------------------------------------
		
		
		private function getNodesOrgaResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDatasGetNodesOrga(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(GestionParcMobileMessages.LOAD_NODE_ORGA_IMPOSSIBLE,
												GestionParcMobileMessages.ERREUR);
			}
		}
		
		private function getListOrgaClienteResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatGetListOrgaClienteResultHandler(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(GestionParcMobileMessages.GET_ORGA_IMPOSSIBLE,
												GestionParcMobileMessages.ERREUR);
			}
		}
		
	}
}