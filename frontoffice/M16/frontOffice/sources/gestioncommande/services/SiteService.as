package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.events.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class SiteService extends EventDispatcher
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					GLOBALES
		//--------------------------------------------------------------------------------------------//
				
		public var listeSitesLivraison		:ArrayCollection;
		public var listeSitesFacturation	:ArrayCollection;
		
		//--------------------------------------------------------------------------------------------//
		//					LIBELLE A TRADUIRE - LUPO
		//--------------------------------------------------------------------------------------------//
		
		private var text_libelleError	:String = ResourceManager.getInstance().getString('M16', 'Erreur');
		private var text_errorSite		:String = ResourceManager.getInstance().getString('M16', 'Erreur_sites_de_livraisons_et_facturations__');	

	//--------------------------------------------------------------------------------------------//
	//					CONSRUCTEUR
	//--------------------------------------------------------------------------------------------//	
		
		public function SiteService()
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLICS
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		public function fournirListeSiteLivraisonsFacturation(idPool:Number): void
	    {
	    	listeSitesFacturation	= new ArrayCollection();
	    	listeSitesLivraison		= new ArrayCollection();
	    	
	    	if(idPool > 0)
	    	{
	    		var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		    "fr.consotel.consoview.inventaire.sites.SitesUtils",
	    																		    "fournirListeSitesLivraisonsPoolGestionnaire",
	    																		    fournirListeSiteLivraisonsFacturationResultHandler);
	    		
	    		RemoteObjectUtil.callService(op, idPool);	
	    	}
	    }

	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATES
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					RESULT EVENT PROCEDURES
		//--------------------------------------------------------------------------------------------//
	
	    private function fournirListeSiteLivraisonsFacturationResultHandler(re:ResultEvent):void
	    {
	    	if(re.result)
	    	{
		    	var sortArray	:ArrayCollection 	= Formator.sortFunction(re.result as ArrayCollection, "NOM_SITE");
		    	var lenSite		:int 				= sortArray.length;
	
		    	for(var i:int = 0; i < lenSite;i++)
		    	{
		    		if(Formator.formatInteger(int(sortArray[i].IS_LIVRAISON)))
		    			listeSitesLivraison.addItem(sortArray[i]);
		    		
		    		if(Formator.formatInteger(int(sortArray[i].IS_FACTURATION)))
		    			listeSitesFacturation.addItem(sortArray[i]);
		    	}
	    		
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_SITES));
	    	}
	    	else
	    		ConsoviewAlert.afficherError(text_errorSite, text_libelleError);
	    }
		

	}
}