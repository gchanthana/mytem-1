package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.Transporteur;
	import gestioncommande.events.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class TransporteurSevice extends EventDispatcher
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
	
		public var listeTransporteurs		:ArrayCollection = new ArrayCollection();
		
		private static var pathprocedure	:String = "fr.consotel.consoview.inventaire.commande.GestionTransporteurs";
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function TransporteurSevice()
		{
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODE PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC METHODE - APPEL DE PROCEDURE
		//--------------------------------------------------------------------------------------------//
		
		public function fournirListeTransporteurs(): void
		{
			listeTransporteurs = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				pathprocedure,
																				"fournirListeTransporteurs",
																				fournirListeTransporteursResultHandler);
			RemoteObjectUtil.callService(op);
		}
		
		public function majInfosLivraisonCommande(commande:Commande): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				'fr.consotel.consoview.inventaire.commande.GestionCommande',
																				"majInfosLivraisonCommande",
																				majInfosLivraisonCommandeResultHandler);
			RemoteObjectUtil.callService(op,commande.IDCOMMANDE,
											commande.IDSITELIVRAISON,
											commande.IDTRANSPORTEUR,
											commande.NUMERO_TRACKING);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODE PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODE - RETOUR DE PROCEDURE
		//--------------------------------------------------------------------------------------------//
		
		private function fournirListeTransporteursResultHandler(re:ResultEvent): void
		{
			if(re.result)	    	
			{																			    			
				listeTransporteurs = formaterTransporteurs(re.result as ArrayCollection);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_TRANSPORTS));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_e'), 'Consoview');
		}
		
		private function majInfosLivraisonCommandeResultHandler(re:ResultEvent): void
		{
			if(re.result > 0)	    	
			{							
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Informations_modifi_es__'));												    			
				
				dispatchEvent(new CommandeEvent(CommandeEvent.TRANSPORTS_UPDATED));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur'), 'Consoview');
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FORMATOR
		//--------------------------------------------------------------------------------------------//
		
		private function formaterTransporteurs(values:ICollectionView):ArrayCollection
		{
			var retour:ArrayCollection = new ArrayCollection();
			
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
				while(!cursor.afterLast)
				{
					var transporteurObj:Transporteur 			= new Transporteur();
						transporteurObj.IDTRANSPORTEUR 			= cursor.current.IDTRANSPORTEUR;	
						transporteurObj.LIBELLE_TRANSPORTEUR 	= cursor.current.LIBELLE_TRANSPORTEUR;	
						transporteurObj.URL_TRACKING 			= cursor.current.URL_TRACKING;	
					
					retour.addItem(transporteurObj);	    			
					cursor.moveNext();
				}	
			}
			
			return retour;
		}
		
	}
}