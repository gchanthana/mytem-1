package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.events.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class RevendeurAutoriseService extends EventDispatcher
	{
		
		private static const CFC_RevendeurAutorise	:String = "fr.consotel.consoview.M21.RevendeurAutorise";
		private static const idRacine				:int 	= CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
		private static const idGestionnaire			:int 	= CvAccessManager.getSession().USER.CLIENTACCESSID;
		
		public var listeOperateursNormal	:ArrayCollection;

		public var listeOperateurs			:ArrayCollection;
		public var listeRevendeurs			:ArrayCollection;
		public var listeOperateursPays		:ArrayCollection;
		
		public var numberRevendeurs	:int;
		
		public function RevendeurAutoriseService()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC METHODE - APPEL DE PROCEDURE
		//--------------------------------------------------------------------------------------------//
	
		
		public  function fournirListeOperateurs(idSegment:int): void
		{
			listeOperateursNormal = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.OperateurService",
																				"fournirListeOperateursSegment",
																				fournirListeOperateursResultHandler);
			RemoteObjectUtil.callService(op,idSegment);
		}

		public function fournirOperateursAutorises(idProfil:int): void
	    {
	    	listeOperateurs = new ArrayCollection();

    		var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
    																			CFC_RevendeurAutorise,
    																			"fournirOperateursAutorises",
    																			fournirOperateursAutorisesResultHandler);
    		RemoteObjectUtil.callService(op,idGestionnaire, idProfil)	    	
	    }
		
		public function fournirOperateursPays(idPays:Number): void
		{
			listeOperateursPays = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																				"fr.consotel.consoview.M16.v2.OperateurService",
																				"fournirListeOperateursPays",
																				fournirOperateursPaysResultHandler);
			RemoteObjectUtil.callService(op,idPays)	    	
		}
		
		private var _myRevendeurPool:int = 0;

		public function fournirRevendeursAutorises(idProfil:int, idOperateur:int, idrevendeurpool:int): void
	    {
	    	listeRevendeurs = new ArrayCollection();

			_myRevendeurPool = idrevendeurpool;
			
    		var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
    																			CFC_RevendeurAutorise,
    																			"fournirRevendeursAutorises",
    																			fournirRevendeursAutorisesResultHandler);
    		RemoteObjectUtil.callService(op,idRacine,
								    		idGestionnaire,
								    		idProfil,
								    		idOperateur);
	    }
	    
	    public function fournirNumberRevendeursAutorises(): void
	    {
	    	numberRevendeurs = -1;

    		var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
    																			CFC_RevendeurAutorise,
    																			"fournirNumberRevendeursAutorises",
    																			fournirNumberRevendeursAutorisesResultHandler);
    		RemoteObjectUtil.callService(op,idRacine,
								    		idGestionnaire);
	    }

	    //--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODE - RETOUR DE PROCEDURE
		//--------------------------------------------------------------------------------------------//
	    
		protected  function fournirListeOperateursResultHandler(re:ResultEvent): void
		{	
			if(re.result)
			{
				listeOperateursNormal = re.result as ArrayCollection;
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_OPERATEURS));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur___'), 'Consoview');	
		}
		
	    private function fournirOperateursAutorisesResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	    		listeOperateurs = new ArrayCollection(re.result as Array);
				
	    		dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_OPEAUTORISES));
	    	}
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur___'), 'Consoview');	
	    }
		
		private function fournirOperateursPaysResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				listeOperateursPays = new ArrayCollection((re.result as ArrayCollection).source);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_OPERATEURSPAYS));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur___'), 'Consoview');	
		}
	    
	    private function fournirRevendeursAutorisesResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	    		listeRevendeurs = new ArrayCollection((re.result as ArrayCollection).source);

				if(_myRevendeurPool > 0)
				{
					var myObjRev	:Object = null;
					var lenRev		:int = listeRevendeurs.length;
					
					for(var j:int = 0;j < lenRev;j++)
					{
						if(listeRevendeurs[j].IDREVENDEUR == _myRevendeurPool)
						{
							myObjRev = new Object();
							myObjRev = listeRevendeurs[j];
						}
					}
					
					if(myObjRev != null)
					{
						listeRevendeurs = new ArrayCollection();
						listeRevendeurs.addItem(myObjRev);
					}
					else
						listeRevendeurs = new ArrayCollection();
				}
				
	    		dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_REVAUTORISES));
	    		trace('listeRevendeurs bool = true');
	    	}
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur___'), 'Consoview');	
	    }
		
		private function fournirNumberRevendeursAutorisesResultHandler(re:ResultEvent): void
	    {
	    	if(re.result > -1)
	    	{
	    		numberRevendeurs = re.result as Number;
	    		dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_NBR_REVAUTO));
	    		trace('numberRevendeurs ' + numberRevendeurs.toString());
	    	}
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur___'), 'Consoview');	
	    }
		
	}
}