package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.RessourcesElements;
	import gestioncommande.events.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import session.SessionUserObject;
	
	public class RessourceService extends EventDispatcher
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					GLOBALES
		//--------------------------------------------------------------------------------------------//
		
		public var listeAboOptTout					:ArrayCollection;
		public var listeFiltre						:ArrayCollection;
		
		public var listeProduitsAbonnement			:ArrayCollection;
		public var listeProduitsAbonnementByLines	:ArrayCollection;
		
		public var ABO_ASSOCIATED					:ArrayCollection;
		public var OPT_ASSOCIATED					:ArrayCollection;
		
		private var _isObligatoire					:Boolean = false;

		//--------------------------------------------------------------------------------------------//
		//					LIBELLE A TRADUIRE - LUPO
		//--------------------------------------------------------------------------------------------//
		
		private var text_libelleError	:String = ResourceManager.getInstance().getString('M16', 'Erreur');
		private var text_errorPool		:String = ResourceManager.getInstance().getString('M16', 'Erreur_pools__');
		private var text_errorProfiles	:String = ResourceManager.getInstance().getString('M16', 'Erreur_type_de_commande__');	
				
	//--------------------------------------------------------------------------------------------//
	//					CONSRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function RessourceService()
		{
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLICS
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		public function fournirListeAbonnements(idOperateur:int, idProfilEquipement:int, isObligatoire:Boolean):void
		{
			listeAboOptTout = new ArrayCollection();
			listeFiltre		= new ArrayCollection();
			
			_isObligatoire = isObligatoire;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.RessourceService",
																				"fournirListeAbonnementsOptions",
																				fournirListeAbonnementsResultHandler);
			//--- 1-> abonnements, 2-> options
			RemoteObjectUtil.callService(op,idOperateur,
											idProfilEquipement,
											SessionUserObject.singletonSession.IDSEGMENT,
											1);	
		}
		
		public function fournirListeOptions(idOperateur:int, idProfilEquipement:int, isObligatoire:Boolean):void
		{
			listeAboOptTout = new ArrayCollection();
			listeFiltre		= new ArrayCollection();
			
			_isObligatoire = isObligatoire;

			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.RessourceService",
																				"fournirListeAbonnementsOptions",
																				fournirListeOptionsResultHandler);
			//--- 1-> abonnements, 2-> options
			RemoteObjectUtil.callService(op,idOperateur,
											idProfilEquipement,
											SessionUserObject.singletonSession.IDSEGMENT,
											2);	
		}
	    
	    public function ajouterProduitAMesFavoris(value:Object):void
	   	{
			var idproduit:int = 0;
			
			if(value.hasOwnProperty('IDPRODUIT_CATALOGUE'))
				idproduit = value.IDPRODUIT_CATALOGUE;
			else
				idproduit = value.IDCATALOGUE;
			
			if(idproduit < 1) return;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.RessourceService",
																				"ajouterProduitAMesFavoris",
																				ajouterProduitAMesFavorisResultHandler);				
			RemoteObjectUtil.callService(op, idproduit);
		}
		
		public function supprimerProduitDeMesFavoris(value:Object):void
		{
			var idproduit:int = 0;
			
			if(value.hasOwnProperty('IDPRODUIT_CATALOGUE'))
				idproduit = value.IDPRODUIT_CATALOGUE;
			else
				idproduit = value.IDCATALOGUE;
			
			if(idproduit < 1) return;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.RessourceService",
																				"supprimerProduitDeMesFavoris",
																				supprimerProduitDeMesFavorisResultHandler);				
			RemoteObjectUtil.callService(op, idproduit);
		}
		
		public function fournirLignesProduitAbonnement(sousTete:Array): void
	    {
	    	listeProduitsAbonnement			= new ArrayCollection();
	    	listeProduitsAbonnementByLines	= new ArrayCollection();
	    	
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.LigneService",
	    																		"fournirLignesProduitAbonnement",
	    																		fournirLignesProduitAbonnementResultHandler);
	    	
	    	RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,
	    									formatInClob(sousTete));	
	    }
		
		public function getassociatedabo(idtypecommande:int, idoperateur:int):void
		{
			ABO_ASSOCIATED = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.RessourceService",
																				"getassociatedabo_options",
																				getassociatedaboResultHandler);				
			RemoteObjectUtil.callService(op,idtypecommande,
											1,
											idoperateur);
		}

		public function getassociatedoptions(idtypecommande:int, idoperateur:int):void
		{
			OPT_ASSOCIATED = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.RessourceService",
																				"getassociatedabo_options",
																				getassociatedoptionsResultHandler);				
			RemoteObjectUtil.callService(op,idtypecommande,
											0,
											idoperateur);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATES
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					RESULT EVENT PROCEDURES
		//--------------------------------------------------------------------------------------------//  

		
		private function fournirListeAbonnementsResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result.length > 0)
				{
					var abonnements	:ArrayCollection = re.result as ArrayCollection;
					var len			:int = abonnements.length;
					
					for(var i:int = 0;i < len;i++)
					{
						if(_isObligatoire)
							listeAboOptTout.addItem(RessourcesElements.formatAbonnementObligatoire(abonnements[i]));
						else
							listeAboOptTout.addItem(RessourcesElements.formatAbonnement(abonnements[i]));
					}

					addFilter2(listeAboOptTout);
					
					dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_RESSOURCES));
				}
				else
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_de_la_liste_des_abonnements'), 'Consoview');
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_de_la_liste_des_abonnements'), 'Consoview');
		}
		
		private function fournirListeOptionsResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result.length > 0)
				{
					var options	:ArrayCollection = re.result as ArrayCollection;
					var len		:int = options.length;
					
					for(var i:int = 0;i < len;i++)
					{
						if(_isObligatoire)
							listeAboOptTout.addItem(RessourcesElements.formatOptionsObligatoire(options[i]));
						else
							listeAboOptTout.addItem(RessourcesElements.formatOptions(options[i]));
					}
					
					addFilter2(listeAboOptTout);

					dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_RESSOURCES));
				}
				else
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_de_la_liste_des_abonnements'), 'Consoview');
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_de_la_liste_des_abonnements'), 'Consoview');
		}

	   	private function ajouterProduitAMesFavorisResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_FAVORIS));
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ADD_FAVORIS));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Erreur') + " : " + re.result.toString(), 'Consoview');	
		}
		
		private function supprimerProduitDeMesFavorisResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_FAVORIS));
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_REM_FAVORIS));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Erreur') + " : " + re.result.toString(), 'Consoview');	
		}
		
		private function fournirLignesProduitAbonnementResultHandler(re:ResultEvent):void
	    {
	    	if(re.result)
			{
				
				var len1:int = (re.result.LIGNESANDOPTIONS as ArrayCollection).length;
				
				for(var i:int = 0; i < len1;i++)
				{
					listeProduitsAbonnementByLines.addItem(RessourcesElements.formatOptionsLignes(re.result.LIGNESANDOPTIONS[i]));
				}
				
				var len2:int = (re.result.NUMBERLIGNES as ArrayCollection).length;
				
				for(var j:int = 0; j < len2;j++)
				{
					listeProduitsAbonnement.addItem(RessourcesElements.formatOptionsInclude(re.result.NUMBERLIGNES[j], listeProduitsAbonnementByLines));
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_OPTIONSERASE));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_de_la_liste_des_lignes_impo'), 'Consoview');
	    }

		private function getassociatedaboResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var len:int = (re.result as ArrayCollection).length;
				
				for(var i:int = 0;i < len;i++)
				{
					ABO_ASSOCIATED.addItem(RessourcesElements.formatAbonnementObligatoire(re.result[i]));
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ABO_ASS));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Erreur') + " : " + re.result.toString(), 'Consoview');	
		}

		private function getassociatedoptionsResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var len:int = (re.result as ArrayCollection).length;
				
				for(var i:int = 0;i < len;i++)
				{
					OPT_ASSOCIATED.addItem(RessourcesElements.formatOptionsObligatoire(re.result[i]));
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_OPT_ASS));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Erreur') + " : " + re.result.toString(), 'Consoview');	
		}
	    
	    //--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//  

	    private function addFilter(values:ICollectionView):void
	    {
	    	var index			:int = 0;
	    	var objectProduit	:Object;
	    	
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
								
				while(!cursor.afterLast)
				{
					index = ConsoviewUtil.getIndexById(listeFiltre,"IDTHEME_PRODUIT", cursor.current.IDTHEME_PRODUIT );
					if(index < 0)
					{
						objectProduit 					= new Object();
						objectProduit.IDTHEME_PRODUIT 	= cursor.current.IDTHEME_PRODUIT;
						objectProduit.LIBELLE 			= cursor.current.LIBELLE;
						objectProduit.SELECTED		 	= true;
						
						listeFiltre.source.push(objectProduit);
					}	    			
					cursor.moveNext();
				}
			}
	    }
		
		private function addFilter2(values:ICollectionView):void
		{
			var index			:int = 0;
			var objectProduit	:Object;
			
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
				
				while(!cursor.afterLast)
				{
					index = ConsoviewUtil.getIndexById(listeFiltre,"IDPRODUIT", cursor.current.IDPRODUIT);
					
					if(index < 0)
					{
						objectProduit 			= new Object();
						objectProduit.IDPRODUIT = (cursor.current as RessourcesElements).IDPRODUIT;
						objectProduit.LIBELLE 	= (cursor.current as RessourcesElements).LIBELLETHEME;
						objectProduit.SELECTED	= true;
						
						listeFiltre.source.push(objectProduit);
					}
					
					cursor.moveNext();
				}
			}
		}
	    
	    private function searchLineWithThisOptions(idProduit:int):ArrayCollection
	    {
	    	var data :ArrayCollection = new ArrayCollection();
	    	
	    	for(var i:int = 0; i < listeProduitsAbonnementByLines.length;i++)
			{
				if(listeProduitsAbonnementByLines[i].IDPRODUIT_CATALOGUE == idProduit)
					data.addItem(listeProduitsAbonnementByLines[i]);
			}
	    	
	    	return data;
	    }

	    //--------------------------------------------------------------------------------------------//
		//					FORMATEUR
		//--------------------------------------------------------------------------------------------//
	    
	    private function formatInClob(arrayInString:Array):String
	    {
	    	var dataInClob:String = "";
	    	
	    	for(var i:int = 0; i < arrayInString.length;i++)
			{
				if(i == 0)
					dataInClob = arrayInString[i];
				else
					dataInClob = dataInClob + arrayInString[i];
				 
				if(i != arrayInString.length-1)
					dataInClob = dataInClob + ",";
			}
			
			return dataInClob;
	    }

	}
}