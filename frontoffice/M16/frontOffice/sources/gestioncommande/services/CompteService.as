package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.CompteFacturationVO;
	import gestioncommande.events.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import session.SessionUserObject;
	
	public class CompteService extends EventDispatcher
	{
		[Bindable]
		public var listeComptesSousComptes			:ArrayCollection;
	   // public var listReferenceComptesSousComptes	:ArrayCollection;
		public var listeSousComptes					:ArrayCollection;
		public var listeLibellesOperateur			:ArrayCollection;
		//public var listeSousComptesOperateur		:ArrayCollection;
		
		
	    private var _allCOmptesSousComptes			:ArrayCollection;
		private var _idCompteFacturation			:int = -1;

		public function CompteService()
		{
		}
		
	//--------------------------------------------------------------------------------------------//
	//					PUBLIC METHODE - APPEL DE PROCEDURE
	//--------------------------------------------------------------------------------------------//
		
		public function searchSousCompteCorrespondant(idCompte:int):ArrayCollection
	    {
	    	var sousComptes		:ArrayCollection	= new ArrayCollection();
	    	var lenComptes		:int				= _allCOmptesSousComptes.length

	    	for(var i:int = 0;i < lenComptes;i++)
	    	{
	    		if(_allCOmptesSousComptes[i].IDCOMPTE_FACTURATION  == idCompte)
	    			sousComptes.addItem(_allCOmptesSousComptes[i]);
	    	}
	    	
			sousComptes = Formator.sortFunction(sousComptes, "SOUS_COMPTE");
			
			return sousComptes;
	    }
	    
		//--------------------------------------------------------------------------------------------//
		//					PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
//		public function fournirCompteOperateurByLoginAndPool(idOperateur:int, idPool:int, idCompte:int = -1):void
//	    {
//	    	listeComptesSousComptes 	= new ArrayCollection();
//	    	_allCOmptesSousComptes 		= new ArrayCollection();
//			listeSousComptesOperateur 	= new ArrayCollection();
//	    	
//	    	_idCompteFacturation 	= idCompte;
//	    	
//	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
//		    																		"fr.consotel.consoview.inventaire.comptes.ComptesUtils",
//	    																			"fournirCompteOperateurByLoginAndPool",
//	    																			fournirCompteOperateurByLoginAndPoolResultHanler);		
//	    	RemoteObjectUtil.callService(op, CvAccessManager.getSession().USER.CLIENTACCESSID,
//											 idOperateur,
//											 idPool);
//	    }
		
		public function fournirCompteOperateurByLoginAndPoolV2(idOperateur:int, idPool:int, idCompte:int = -1):void
		{
			listeComptesSousComptes 	= new ArrayCollection();
			_allCOmptesSousComptes 		= new ArrayCollection();
			//listeSousComptesOperateur 	= new ArrayCollection();
			
			_idCompteFacturation 	= idCompte;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CompteService",
																				"fournirCompteOperateurByLoginAndPoolV2",
																				fournirCompteOperateurByLoginAndPoolResultHanler);		
			RemoteObjectUtil.callService(op,idOperateur,
											idPool);
		}
		
		public function fournirLibelleCompteOperateur(idOperateur:int = -1):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CompteService",
																				"fournirLibelleCompteOperateur",
																				fournirLibelleCompteOperateurResultHanler);		
			RemoteObjectUtil.callService(op, idOperateur);
		}

		//--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODE - RETOUR DE PROCEDURE
		//--------------------------------------------------------------------------------------------//
	    
	    private function fournirCompteOperateurByLoginAndPoolResultHanler(re:ResultEvent):void
	    {
	    	if(re.result.SOUS_COMPTES)
	    	{	
	    		var listComptes:ArrayCollection	= (re.result.COMPTES as ArrayCollection);
	    		_allCOmptesSousComptes			= (re.result.SOUS_COMPTES as ArrayCollection);
//	    		listReferenceComptesSousComptes	= re.result as ArrayCollection;
//				listeSousComptesOperateur		= re.result as ArrayCollection;
				
				var tailleComptes:int = listComptes.length;
				var model:CompteFacturationVO
				for (var i:int = 0; i < tailleComptes; i++) 
				{
					model = new CompteFacturationVO();
					model.fill(listComptes[i]);
					listeComptesSousComptes.addItem(model);
				}
				
				
	    		dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_COMPTES));
	    	}
	    	else
	    	{
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Recup_ration_des_comptes_et_des_sous_com'), ResourceManager.getInstance().getString('M16', 'Consoview'));	
	    	}
	    }
		
		private function fournirLibelleCompteOperateurResultHanler(re:ResultEvent):void
		{
			if(re.result)
			{	
				listeLibellesOperateur = re.result as ArrayCollection;
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_LIBELLE_OPE));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Recup_ration_des_comptes_et_des_sous_com'), ResourceManager.getInstance().getString('M16', 'Consoview'));	
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					FORMATEURS
		//--------------------------------------------------------------------------------------------//

		private function formatCompte(values:ArrayCollection):ArrayCollection
	    {
	    	var dataFormated		:ArrayCollection = new ArrayCollection();
	    	var idCompteFacturation	:int 			 = 0;
	    	 
	    	
	    	for(var i:int = 0;i < values.length;i++)
	    	{	
	    		if(idCompteFacturation != values[i].IDCOMPTE_FACTURATION)
	    		{
	    			idCompteFacturation = values[i].IDCOMPTE_FACTURATION;
	    			dataFormated.addItem(values[i]);
	    			
					if(_idCompteFacturation == idCompteFacturation)
	    				SessionUserObject.singletonSession.RANGECOMPTE = i;
	    		}
	    	}
	    	return dataFormated;
	    }

	}
}