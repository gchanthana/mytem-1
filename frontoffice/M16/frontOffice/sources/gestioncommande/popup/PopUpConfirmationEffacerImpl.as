package gestioncommande.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Box;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpConfirmationEffacerImpl extends Box
	{
		
		public static const ERASE_THIS_MODELE		:String = "ERASE_THIS_MODELE";
		
		public var libellePopUp						:String = "";
		
		
		public function PopUpConfirmationEffacerImpl()
		{
		}
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event(ERASE_THIS_MODELE));
			PopUpManager.removePopUp(this);
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

	}
}