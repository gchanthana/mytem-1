package gestioncommande.popup
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestioncommande.services.PoolService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.TitleWindow;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class AlimentationPools2Impl extends TitleWindow
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var boxElements	:Box;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _poolSrv	:PoolService = new PoolService();
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		private var poolsValue	:ArrayCollection = new ArrayCollection();
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function AlimentationPools2Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function closeThisHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			validation();
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER IHM
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			initialization()
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTIONS
		//--------------------------------------------------------------------------------------------//
		
		private function initialization():void
		{
			getPools();
		}
		
		private function validation():void
		{
			if(checkdata())
				sendData();
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Erreur___d_finir'), 'Consoview', null);
		}
		
		private function checkdata():Boolean
		{
			var bool:Boolean = false;
			
			return bool;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS DE PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getPools():void
		{
			_poolSrv.fournirPools(10);//---> ICI CE SERA CLIENT / OU DISTRIBUTEUR
			_poolSrv.addEventListener("POOLS_LISTED", getPoolsHandler);
		}
		
		private function sendData():void
		{
			
		}
		
		//--------------------------------------------------------------------------------------------//
		//					RETOURS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getPoolsHandler(e:Event):void
		{
			var lenPools:int = _poolSrv.listePoolsValues.length;
			var cptr	:int = 0;
			
			for(var i:int = 0;i < len;i++)
			{
				
			}
			
			
		}
		
		private function sendDataHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
	}
}