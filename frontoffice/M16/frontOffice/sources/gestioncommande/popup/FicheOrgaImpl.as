package gestioncommande.popup
{
	import composants.controls.TextInputLabeled;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestioncommande.entity.OrgaVO;
	import gestioncommande.events.GestionParcMobileEvent;
	import gestioncommande.services.organisation.OrganisationServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	/**
	 * @author 		Guiou Nicolas
	 * @version 	1.0
	 * @date 		15.07.2010
	 * 
	 * <b>Classe <code>FicheOrgaImpl</code></b>
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de la fiche organisation.
	 * </pre></p>
	 * 
	 */
	[Bindable]
	public class FicheOrgaImpl extends TitleWindow
	{
		// VARIABLES------------------------------------------------------------------------------
		
		// IHM components
		public var txtFiltre				:TextInputLabeled;
		
		public var dgListTarget				:DataGrid;
		
		public var dgcHeaderToChange		:DataGridColumn;
		
		public var lbNodeSelected			:Label;
		public var orgaSelectedNbNode		:Label;
		
		public var btValid					:Button;
		public var btCancel					:Button;
		
		// publics variables 
		public var libelleOrga				:String = "";
		public var idOrga					:int = 0;
		public var myService 				:OrganisationServices = null;
		
		public var myOrga					:OrgaVO;
		
		// privates variables
		private var nbNode					:int = 0;
		
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLICS FONCTIONS-------------------------------------------------------------------------
		
		/**
		 * <b>Constructeur <code>FicheOrgaImpl()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe FicheOrgaImpl.
		 * </pre></p>
		 */
		public function FicheOrgaImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		// PUBLICS FONCTIONS-------------------------------------------------------------------------
		
		// PRIVATE FONCTIONS-------------------------------------------------------------------------
		
		/**
		 * <b>Fonction <code>init(event:FlexEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation générale de cette classe :
		 * - Instanciation du service lié aux organisations.
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 * </pre></p>
		 * 
		 * @param event:<code>FlexEvent</code>.
		 * @return void.
		 */
		private function init(event:FlexEvent):void
		{
			myService = new OrganisationServices();
			myService.getNodesOrga(idOrga);
			
			initListener();
			initDisplay();
		}
		
		/**
		 * <b>Fonction <code>initListener()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise les écouteurs.
		 * </pre></p>
		 * 
		 * @return void.
		 */	
		private function initListener():void
		{
			// listeners liés à FicheDetailleeNouvelleLigneIHM
			this.addEventListener(CloseEvent.CLOSE,closePopup);
			btCancel.addEventListener(MouseEvent.CLICK,cancelPopup);
			btValid.addEventListener(MouseEvent.CLICK,validClickHandler);
			txtFiltre.addEventListener(Event.CHANGE,filtrerGird);
			dgListTarget.addEventListener(ListEvent.ITEM_CLICK, selectItemHandler);
			
			// listeners liés aux services
			myService.myDatas.addEventListener(GestionParcMobileEvent.LISTED_NODES_ORGA_LOADED, updateGrid);
		}
		
		/**
		 * <b>Fonction <code>initDisplay()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise l'affichage de cette IHM.
		 * </pre></p>
		 * 
		 * @return void.
		 */	
		private function initDisplay():void
		{
			orgaSelectedNbNode.text = ResourceManager.getInstance().getString('M16','L_organisation');
			orgaSelectedNbNode.text +=  " " + libelleOrga + " ";
			orgaSelectedNbNode.text += ResourceManager.getInstance().getString('M16','contient');
			orgaSelectedNbNode.text += " " + nbNode + " ";
			orgaSelectedNbNode.text += ResourceManager.getInstance().getString('M16','noeud_s_');
		}
		
		/**
		 * <b>Fonction <code>cancelPopup(event:MouseEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup()</code> pour femrer la popup courante.
		 * </pre></p>
		 * 
		 * @param event:<code>MouseEvent</code>.
		 * @return void.
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}
		
		/**
		 * <b>Fonction <code>closePopup(event:Event)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Ferme la popup courante.
		 * </pre></p>
		 * 
		 * @param event:<code>Event</code>.
		 * @return void.
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		/**
		 * <b>Fonction <code>validerHandler(event:MouseEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Affecte à la varible <code>cheminSelected</code> de <code>OrganisationDatas</code>
		 * la valeur et le nom du chemin sélectionné dans le DataGrid pour l'organisation 
		 * précédemment sélectionnée dans la fiche Collaborateur.
		 * Dispatch de l'événement VALIDE_FICHE_ORGA, pour prévenir la fiche collaborateur qu'un chemin
		 * a été sélectionné pour cette organisation.
		 * </pre></p>
		 * 
		 * @param event:<code>MouseEvent</code>.
		 * @return void.
		 */			
		private function validClickHandler(event:MouseEvent):void
		{
			myService.myDatas.cheminSelected = new Object();
			myService.myDatas.cheminSelected.chemin = dgListTarget.selectedItem.chemin;
			myService.myDatas.cheminSelected.idFeuille = dgListTarget.selectedItem.idFeuille;
			myService.myDatas.cheminSelected.idOrga = idOrga;
			
			if(myOrga != null)
			{
				myOrga.idOrga 	= idOrga;
				myOrga.chemin 	= dgListTarget.selectedItem.chemin;
				myOrga.idCible 	= dgListTarget.selectedItem.idFeuille;
			}
			
			dispatchEvent(new GestionParcMobileEvent(GestionParcMobileEvent.VALIDE_FICHE_ORGA));
			closePopup(null);
		}
		
		/**
		 * <b>Fonction <code>runValidators()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Teste les validateurs et permet ainsi la cohérence des valeurs attendues.
		 * </pre></p>
		 * 
		 * @param event:<code>Event</code>.
		 * @return void.
		 */	
		private function activeValidButton(event:Event):void
		{
			btValid.enabled = true;
		}
	
		/**
		 * <b>Fonction <code>filtrerGird(event:Event)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la fonction <code>filtrer()</code> qui filtra le DataGrid selon la valeur du champ txtFiltre et met à jour le DataGrid.
		 * </pre></p>
		 * 
		 * @param event:<code>Event</code>.
		 * @return void.
		 */			
		private function filtrerGird(event:Event):void
		{
			if (dgListTarget.dataProvider != null)
			{
				(dgListTarget.dataProvider as ArrayCollection).filterFunction = filtrer;
				(dgListTarget.dataProvider as ArrayCollection).refresh();
			}
		}
		
		/**
		 * <b>Fonction <code>filtrer(item:Object)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Filtre le DataGrid selon la valeur du champ txtFiltre.
		 * </pre></p>
		 * 
		 * @param item:<code>Object</code>.
		 * @return Boolean.
		 */	
		private function filtrer(item:Object):Boolean
		{
			if(String(item.chemin).toLowerCase().search(txtFiltre.text) != -1)	
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/**
		 * <b>Fonction <code>updateGrid(event:GestionParcMobileEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Mise à jour du tableau comprennant les feuilles de l'organisation sélectionnée.
		 * </pre></p>
		 * 
		 * @param event:<code>GestionParcMobileEvent</code>.
		 * @return void.
		 */			
		private function updateGrid(event:GestionParcMobileEvent):void
		{
			dgListTarget.dataProvider = myService.myDatas.listeFeuilles;
			nbNode = myService.myDatas.listeFeuilles.length;
			dgcHeaderToChange.headerText = myService.myDatas.structureOrga;
			initDisplay();
		}
		
		/**
		 * <b>Fonction <code>selectItemHandler(event:ListEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * La sélection d'un item met à jour le champ "noeud sélectionné" et active le bouton "Valider".
		 * </pre></p>
		 * 
		 * @param event:<code>ListEvent</code>.
		 * @return void.
		 */			
		private function selectItemHandler(event:ListEvent):void
		{
			lbNodeSelected.text = ResourceManager.getInstance().getString('M16','Noeud_s__s_lectionn__s_');
			lbNodeSelected.text += dgListTarget.selectedItem.chemin;
			
			activeValidButton(null);
		}
		
		// PRIVATE FONCTIONS-------------------------------------------------------------------------

	}
}