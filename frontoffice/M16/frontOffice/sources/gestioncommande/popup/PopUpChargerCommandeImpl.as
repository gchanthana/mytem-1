package gestioncommande.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.entity.RessourcesElements;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.Formator;
	import gestioncommande.services.ModeleService;
	import gestioncommande.services.RessourceService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class PopUpChargerCommandeImpl extends TitleWindow
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//

		public var dgListConfiguration			:DataGrid;
		
		public var btnValider					:Button;
		
		public var txtptSearch					:TextInput;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _popUpDetailConfiguration	:PopUpViewConfigurationElementsIHM;
		private var _popUpModifModele			:PopUpLibelleModeleIHM;
	
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _modele						:ModeleService = new ModeleService();
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var myElements					:ElementsCommande2;
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listConfiguration			:ArrayCollection = new ArrayCollection();
		public var listArticles					:ArrayCollection = new ArrayCollection();
		public var configurationSelected		:ArrayCollection = new ArrayCollection();
		
		public var configObject					:Object = new Object();

		public var operateurName				:String = "";
		
		public var libelle						:String = "";
		
		public var newCommande					:Boolean = true;
		
		public var idPool						:int = 0;
		public var idProfil						:int = 0;
		public var idOperateur					:int = 0;
		public var idRevendeur					:int = 0;
		
		private var _articlesXML				:XML = new XML();
		
		private var _idModeleSelected			:int = -1;

		//--------------------------------------------------------------------------------------------//
		//					CONSTANTES
		//--------------------------------------------------------------------------------------------//
		
		public static const SELECT_MODELE		:String = "SELECT_MODELE";
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function PopUpChargerCommandeImpl()
		{			
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//

		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			var idModele:int = 0;
			
			for(var i:int = 0;i < listConfiguration.length;i++)
			{
				if(listConfiguration[i].SELECTED)
				{
					idModele = listConfiguration[i].IDMODELE;
					libelle  = listConfiguration[i].LIBELLE;
					break;
				}
			}
			
			if(idModele > 0)
				getThisModele(idModele);
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_une_configuration_'),'Consoview');
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function closeHandler(ce:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function dgListConfigurationClickHandler(e:Event):void
		{
			if(dgListConfiguration.selectedItem != null)
			{
				var idModele:int = dgListConfiguration.selectedItem.IDMODELE;
				
				for(var i:int = 0;i < listConfiguration.length;i++)
				{
					listConfiguration[i].SELECTED = false;
 
					if(listConfiguration[i].IDMODELE == idModele)
					{
						listConfiguration[i].SELECTED = true;
						btnValider.enabled = true;
					}
						
					listConfiguration.itemUpdated(listConfiguration[i]);
				}
			}
		}

		protected function txtptSearchHandler(e:Event):void
		{
			if(dgListConfiguration.dataProvider != null)
			{
				(dgListConfiguration.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListConfiguration.dataProvider as ArrayCollection).refresh();
			}
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS FLEX
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener('VIEW_CONFIGURATION', viewThisModeleHandler);
			addEventListener("SHARE_MODELE", shareThisModeleHandler);
			addEventListener("EDIT_MODELE",	editThisModeleHandler);
			addEventListener("ERASE_MODELE", eraseThisModeleHandler);
			
			getModeles();
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function viewThisModeleHandler(e:Event):void
		{
			if(dgListConfiguration.selectedItem != null)
			{
				_popUpDetailConfiguration 				= new PopUpViewConfigurationElementsIHM();
				_popUpDetailConfiguration.idModele 		= dgListConfiguration.selectedItem.IDMODELE;
				_popUpDetailConfiguration.idOperateur	= dgListConfiguration.selectedItem.OPEURATEURID;
				_popUpDetailConfiguration.idProfil		= idProfil;
				_popUpDetailConfiguration.idPool		= dgListConfiguration.selectedItem.IDPOOL;
				_popUpDetailConfiguration.libelle		= dgListConfiguration.selectedItem.LIBELLE;
				
				PopUpManager.addPopUp(_popUpDetailConfiguration, this, true);
				PopUpManager.centerPopUp(_popUpDetailConfiguration);
			}
		}
		
		private function editThisModeleHandler(e:Event):void
		{
			if(dgListConfiguration.selectedItem != null)
			{
				_popUpModifModele 			= new PopUpLibelleModeleIHM();
				_popUpModifModele.idModele 	= dgListConfiguration.selectedItem.IDMODELE;
				_popUpModifModele.thisPool	= idPool;
				_popUpModifModele.libelle	= dgListConfiguration.selectedItem.LIBELLE;
				_popUpModifModele.addEventListener(PopUpLibelleModeleImpl.CLOSE_EDIT_MODELE, editPopUpModeleHandler);
				
				PopUpManager.addPopUp(_popUpModifModele, this, true);
				PopUpManager.centerPopUp(_popUpModifModele);
			}	
		}
		
		private function editPopUpModeleHandler(e:Event):void
		{	
			getModeles();
		}
		
		private function shareThisModeleHandler(e:Event):void
		{
			if(dgListConfiguration.selectedItem != null)
				getShareModele();
		}
		
		private function eraseThisModeleHandler(e:Event):void
		{
			if(dgListConfiguration.selectedItem != null)
			{
				_idModeleSelected = dgListConfiguration.selectedItem.IDMODELE;
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M16','Le_mod_le_va__tre_supprim___nEtes_vous_sur_'), 'Consoview', eraseModeleCVHandler);
			}
		}
		
		private function eraseModeleCVHandler(ce:CloseEvent):void
		{
			if(ce.detail == 4)
				eraseThisModele();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getModeles():void
		{
			_modele.fournirListeModele(SessionUserObject.singletonSession.POOL.IDPOOL, idProfil);
			_modele.addEventListener(CommandeEvent.MODELE_LISTED, modelesHandler);
		}
		
		private function getThisModele(idModele:int):void
		{
			_modele.getModeleCommande(idModele);
			_modele.addEventListener(CommandeEvent.MODELE_ADD, thisModeleHandler);
		}

		private function saveModele():void
		{
			_modele.sauvegardeModele(createCommande(),_modele.listeModele, true);
			_modele.addEventListener(CommandeEvent.MODELE_CREATED, saveModeleHandler);
		}
		
		private function getShareModele():void
		{
			_modele.getModeleCommande(dgListConfiguration.selectedItem.IDMODELE);
			_modele.addEventListener(CommandeEvent.MODELE_ADD, modeleHandler);
		}

		private function shareThisModele():void
		{
			_modele.getModeleCommande(dgListConfiguration.selectedItem.IDMODELE);
			_modele.addEventListener(CommandeEvent.MODELE_ADD, thisModeleHandler);
		}

		private function eraseThisModele():void
		{
			_modele.deleteModeleCommande(dgListConfiguration.selectedItem.IDMODELE);
			_modele.addEventListener(CommandeEvent.MODELE_ERASED, eraseModeleThisHandler);			
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function modelesHandler(cmde:CommandeEvent):void
		{
			var myModeles:ArrayCollection = _modele.listeModeles;
			
			if(idOperateur != 0)
			{
				myModeles.filterFunction = filterOperateur;
				myModeles.refresh();
			}
			
			if(idRevendeur != 0)
			{
				myModeles.filterFunction = filterRevendeur;
				myModeles.refresh();
			}
			
			var len:int = myModeles.length;
			
			if(len == 0)
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucun_mod_le_disponible'), 'Consoview', null);
			else
			{
				listConfiguration = new ArrayCollection();
				
				for(var i:int = 0;i < len;i++)
				{
					listConfiguration.addItem(myModeles[i]);
				}
			}
		}
		
		private function thisModeleHandler(cmde:CommandeEvent):void
		{
			var articles:XML = _modele.listeModele.article[0];
			
			myElements = new ElementsCommande2();
			
			getEquipements(articles.equipements[0], myElements);
			getRessources(articles.ressources[0], myElements);
			getEngagement(articles, myElements);
			
			dispatchEvent(new Event(SELECT_MODELE));
			
			PopUpManager.removePopUp(this);
		}

		private function eraseModeleThisHandler(cmde:CommandeEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Modele_supprimmer__'), this);
			getModeles();
		}
		
		private function modeleHandler(cmde:CommandeEvent):void
		{
			saveModele();
		}
		
		private function saveModeleHandler(cmde:CommandeEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Mod_le_partag_'), this);
		}

		//--------------------------------------------------------------------------------------------//
		//					FUCNTION - FILTRE
		//--------------------------------------------------------------------------------------------//
		
		private function filterOperateur(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && ( (item.OPEURATEURID != null && item.OPEURATEURID.toLocaleLowerCase().search(idOperateur.toString().toLocaleLowerCase()) != -1));
			
			return rfilter;
		}
		
		private function filterRevendeur(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && ( (item.IDREVENDEUR != null && item.IDREVENDEUR.toLocaleLowerCase().search(idRevendeur.toString().toLocaleLowerCase()) != -1));
			
			return rfilter;
		}
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && 	( 
										(item.LIBELLE != null && item.LIBELLE.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.REVENDEUR != null && item.REVENDEUR.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
									);
			
			return rfilter;
		}

		//--------------------------------------------------------------------------------------------//
		//					FUCNTION
		//--------------------------------------------------------------------------------------------//
		
		private function createCommande():Commande
		{
			var _cmd	:Commande				= new Commande();
			var libelle	:String = ResourceManager.getInstance().getString('M16', 'Mod_le_de_commande');
			
			if(dgListConfiguration.selectedItem != null)
				libelle = dgListConfiguration.selectedItem.LIBELLE

			_cmd.LIBELLE_CONFIGURATION 	= libelle;
			_cmd.IDPROFIL_EQUIPEMENT 	= idProfil;
			_cmd.IDPOOL_GESTIONNAIRE 	= idPool;
			_cmd.IDOPERATEUR 			= idOperateur;
			_cmd.IDREVENDEUR 			= idRevendeur;
			
			return _cmd;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION - FORMAT XML
		//--------------------------------------------------------------------------------------------//

		private function getEquipements(articlesEq:XML, valuesElements:ElementsCommande2):void
		{
			var lenXML		:int = articlesEq[0].equipement.length();
			var idParent	:int = 0;
			
			for(var i:int = 0;i < lenXML;i++)
			{
				var myXML	:XML = articlesEq[0].equipement[i];
				var price	:String = myXML[0].prix.toString().replace(",",".");
				
				var eqElts:EquipementsElements 	= new EquipementsElements();
					eqElts.LIBELLE	 			= String(myXML[0].libelle);
					eqElts.EQUIPEMENT 			= String(myXML[0].type_equipement);
					eqElts.IDEQUIPEMENT 		= int(myXML[0].idtype_equipement);
					eqElts.REF_REVENDEUR		= String(myXML[0].reference);
					eqElts.IDFOURNISSEUR		= int(myXML[0].idequipementfournis);
					eqElts.IDCLIENT				= int(myXML[0].idequipementclient);
					eqElts.PRIX					= Number(price);
					eqElts.PRIX_S				= Formator.formatTotalWithSymbole(eqElts.PRIX);
				
				if(eqElts.IDEQUIPEMENT == 2191)
					valuesElements.ACCESSOIRES.addItem(eqElts);//---> ACCESSOIRES
				else//---> TERMINAUX / CARTE SIM
				{
					if(eqElts.IDEQUIPEMENT == 71)//---> CARTE SIM
						valuesElements.TERMINAUX.addItem(eqElts);
					else//---> TERMINAUX
					{
						idParent = eqElts.IDFOURNISSEUR;
						
						eqElts.EDITABLE = (moduleCommandeMobileIHM.userAcess.hasOwnProperty('E_PRIX_T'))? (moduleCommandeMobileIHM.userAcess.E_PRIX_T == 1) : false;
						valuesElements.TERMINAUX.addItem(eqElts);
					}
				}
			}
			
			if(idParent > 0)
			{
				var j		:int = 0;
				var lenT	:int = valuesElements.TERMINAUX.length;
				var lenA	:int = valuesElements.ACCESSOIRES.length;
				
				for(j = 0;j < lenT;j++)
				{
					valuesElements.TERMINAUX[j].IDPARENT = idParent;
				}
				
				for(j = 0;j < lenA;j++)
				{
					valuesElements.ACCESSOIRES[j].IDPARENT = idParent;
				}
			}
		}
		
		private function getRessources(articlesRe:XML, valuesElements:ElementsCommande2):void
		{
			var lenXML:int = articlesRe[0].ressource.length();
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML:XML = articlesRe[0].ressource[i];
				var priceOpt:String = myXML[0].prix.toString().replace(",",".");
					
				var reElts:RessourcesElements	= new RessourcesElements();	
					reElts.LIBELLETHEME 		= String(myXML[0].theme);
					reElts.LIBELLE 				= String(myXML[0].libelle);
					reElts.REFERENCE_PRODUIT	= String(myXML[0].reference);
					reElts.IDCATALOGUE 			= int(myXML[0].idproduit_catalogue);
					reElts.IDPRODUIT 			= int(myXML[0].idThemeProduit);
					reElts.BOOLACCES 			= int(myXML[0].bool_acces);
					reElts.PRIX_UNIT			= Number(priceOpt);
					reElts.PRIX_STRG			= Formator.formatTotalWithSymbole(reElts.PRIX_UNIT);
					reElts.PRIX					= Formator.formatTotalWithSymbole(reElts.PRIX_UNIT);
					
				//---> 'BOOLACCES' = 0 -> OPTION, 'BOOLACCES' = 1 -> ABONNEMENT
				if(reElts.BOOLACCES > 0)
					valuesElements.ABONNEMENTS.addItem(reElts);
				else
					valuesElements.OPTIONS.addItem(reElts);
			}
		}
		
		private function getEngagement(engagement:XML, valuesElements:ElementsCommande2):void
		{
			var commitment	:Engagement = new Engagement();
			var duree		:int = int(engagement.engagement_res[0]);
			var idType		:int = int(engagement.idtype_ligne[0]);
			var eligibilite	:int = int(engagement.duree_elligibilite[0]);
			var fpc			:String = String(engagement.fpc[0]);
			
			if(eligibilite > 0)
				commitment.ELEGIBILITE = eligibilite;
			
			if(fpc != "")
			{
				commitment.DUREE = ResourceManager.getInstance().getString('M16', '24_mois');
				commitment.VALUE = '24';
				commitment.CODE  = 'EVQMFCP';
				commitment.FPC   = String(engagement.fpc[0]);
			}
			else
			{
				switch(duree)
				{
					case 12: commitment.CODE = 'EDM';  commitment.DUREE = ResourceManager.getInstance().getString('M16', '12_mois'); commitment.VALUE = '12'; break;
					case 24: commitment.CODE = 'EVQM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '24_mois'); commitment.VALUE = '24'; break;
					case 36: commitment.CODE = 'ETSM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '36_mois'); commitment.VALUE = '36'; break;
					case 48: commitment.CODE = 'EQHM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '48_mois'); commitment.VALUE = '48'; break;
				}
			}
			
			if(idType > 0)
			{
				valuesElements.IDTYPELIGNE  = idType;
				valuesElements.TYPELIGNE 	= String(engagement.type_ligne[0]);
			}
			else
			{
				valuesElements.IDTYPELIGNE  = -1;
				valuesElements.TYPELIGNE 	= '';
			}
			
			valuesElements.ENGAGEMENT 	= commitment;	
		}

	}
}
