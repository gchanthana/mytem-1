package gestioncommande.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestioncommande.custom.CustomChekBox;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.ItemSelected;
	import gestioncommande.entity.RessourcesElements;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.RessourceService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpChoiceOptionsImpl extends TitleWindow
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var ckbxSelectAll		:CheckBox;
		
		public var dgListProduit		:DataGrid;
		
		public var txtptSearch			:TextInput;
		
		public var lblFavoris			:Label;
		public var lblFavorisUnder		:Label;
		public var lblTout				:Label;
		public var lblToutUnder			:Label;
		
		public var hbxContainVbox		:HBox;
		
		private var _customCheckbox		:CustomChekBox;
		
		private var _vbxCheckbox		:VBox;

		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var cmd					:Commande;
		public var myElements			:ElementsCommande2	= new ElementsCommande2();
		
		private var _favoriSelected		:RessourcesElements;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _ressources			:RessourceService = new RessourceService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listProduit			:ArrayCollection = new ArrayCollection();
		
		public var isFavorite			:Boolean = true;
		
		public var nbrProduitSelected	:int = 0;

		private var _listeFilter		:ArrayCollection;
		
		private var _checkboxSelected	:Array = new Array();
				
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//

		public function PopUpChoiceOptionsImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODE PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function dgListProduitClickHandler(me:MouseEvent):void
		{
			if(dgListProduit.selectedItem != null)
			{
				if(dgListProduit.selectedItem.SELECTED)
				{
					dgListProduit.selectedItem.SELECTED = false;
					nbrProduitSelected = nbrProduitSelected - 1;
				}
				else
				{
					dgListProduit.selectedItem.SELECTED = true;
					nbrProduitSelected = nbrProduitSelected + 1;
				}
				
				(dgListProduit.dataProvider as ArrayCollection).itemUpdated(dgListProduit.selectedItem);
				dgListProduit.selectedIndex = -1;
				calculTotalSelected();
			}
		}

		protected function txtptSearchHandler(e:Event):void
		{
			if(dgListProduit.dataProvider != null)
			{
				_checkboxSelected = findCheckBoxSelected();
				(dgListProduit.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListProduit.dataProvider as ArrayCollection).refresh();
			}
		}

		//REINITIALISE LE CHAMP DE RECHERCHE AINSI QUE LE DATAGRID (>REAFFICHE TOUS)
		protected function btReinitialisationClickHandler(me:MouseEvent):void
		{
			txtptSearch.text = "";
			
			txtptSearchHandler(me);
		}
		
		//AFFICHE TOUT
		protected function ckbxSelectAllChangeHandler(e:Event):void
		{			
			selectUnselectAll(ckbxSelectAll.selected);
			txtptSearchHandler(e);
		}

		//AFFICHE LES FAVORIS
		protected function lblFavorisClickHandler(me:MouseEvent):void
		{
			isFavorite = true;
			txtptSearchHandler(me);
		}

		//AFFICHE LES FAVORIS
		protected function lblToutClickHandler(me:MouseEvent):void
		{
			isFavorite = false;
			txtptSearchHandler(me);
		}
		
		//VALIDE LA SELECTION DES OPTIONS
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			ressourcesSelected(me);
			
			dispatchEvent(new Event('POPUP_OPTIONS_CLOSED_AND_VALIDATE'));
			
			PopUpManager.removePopUp(this);
		}
		
		//ANNULE LA SELECTION DES OPTIONS
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODE PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener("REMOVE_ADD_FAVORI", removeAddFavoriHandler);
			
			if(cmd.IDTYPE_COMMANDE == TypesCommandesMobile.MODIFICATION_OPTIONS || cmd.IDTYPE_COMMANDE == TypesCommandesMobile.MODIFICATION_FIXE)
				getProduit(false);
			else
				getProduit(true);
		}
		
		private function removeAddFavoriHandler(e:Event):void
		{
			if(dgListProduit.selectedItem != null)
			{
				_favoriSelected = dgListProduit.selectedItem as RessourcesElements;
				
				if(_favoriSelected.FAVORI == 0)
					addFavori()
				else
					removeFavori();
			}
		}
		
		private function filtreDataHandler(e:Event):void
		{
			txtptSearchHandler(e);
		}
		
		private function ressourcesSelected(e:Event):void
		{
			txtptSearch.text 		= "";
			isFavorite				= false;
			
			selectUnselectAll(true);
			txtptSearchHandler(e);
			
			myElements.OPTIONS = new ArrayCollection();
			
			var len:int = listProduit.length;
			
			for(var i:int = 0;i < listProduit.length;i++)
			{
				if(listProduit[i].SELECTED)
				{
					myElements.OPTIONS.addItem(listProduit[i]);
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function listeOptionsHandler(cmde:CommandeEvent):void
		{
			listProduit  = _ressources.listeAboOptTout;
			_listeFilter = _ressources.listeFiltre;
			
			chekLastItemSelected();
			txtptSearchHandler(cmde);
			
			if(hbxContainVbox.numChildren == 0)
				createCheckBoxDynamique();

			if(listProduit.length == 0)
			{
				isFavorite = false;
				txtptSearchHandler(cmde);
			}
		}
		
		private function produitsFavorisHandler(cmde:CommandeEvent):void
		{
			if(cmde.type == CommandeEvent.LISTED_ADD_FAVORIS)
				_favoriSelected.FAVORI = 1;
			else
				_favoriSelected.FAVORI = 0;
			
			(dgListProduit.dataProvider as ArrayCollection).itemUpdated(_favoriSelected);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getProduit(isObl:Boolean):void
		{
			_ressources.fournirListeOptions(cmd.IDOPERATEUR, cmd.IDPROFIL_EQUIPEMENT, isObl);
			_ressources.addEventListener(CommandeEvent.LISTED_RESSOURCES, listeOptionsHandler);
		}
		
		private function addFavori():void
		{
			_ressources.ajouterProduitAMesFavoris(_favoriSelected);
			_ressources.addEventListener(CommandeEvent.LISTED_ADD_FAVORIS, produitsFavorisHandler);
		}
		
		private function removeFavori():void
		{
			_ressources.supprimerProduitDeMesFavoris(_favoriSelected);
			_ressources.addEventListener(CommandeEvent.LISTED_REM_FAVORIS, produitsFavorisHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    

			rfilter = rfilter && ( (item.LIBELLE != null && item.LIBELLE.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1) 
									||		
									(item.REFERENCE_PRODUIT != null && item.REFERENCE_PRODUIT.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
								)
									
								&& searchOptions(item) && searchIsFavoris(item);
			
			return rfilter;

		}

		private function searchOptions(item:Object):Boolean
		{
			var bool:Boolean = false;

			var lenCkbx	:int = _checkboxSelected.length;
			
			if(lenCkbx == 0) return false;
			
			for(var i:int = 0;i < lenCkbx;i++)
			{
				if(item.IDPRODUIT == _checkboxSelected[i])
				{
					bool = true;
					break;
				}
			}
				
			return bool;
		}

		private function searchIsFavoris(item:Object):Boolean
		{
			var bool:Boolean = true;
			
			if(isFavorite)
			{
				if(item.FAVORI == 1)
					bool = true;
				else
					bool = false;
			}
			
			return bool;
		}

		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		//---> CREATION DYNAMIQUE DES CHECKBOX FILTRES
		private function createCheckBoxDynamique():void
		{
			var lenFiltre	:int = _listeFilter.length;
			var nbrHbx		:int = Math.round(lenFiltre/5);
			var nbrHbxLim	:Number = lenFiltre/5;
			var cptr		:int = 0;
			var limite		:int = 5;
			var len			:int = 0;
			
			if(nbrHbx == 0)
				nbrHbx = 1;
			else
			{
				if(nbrHbxLim > nbrHbx)
					nbrHbx++;
			}
			
			for(var i:int = 0;i < nbrHbx;i++)
			{
				len = limite * (i+1);
				_vbxCheckbox = new VBox();
				
				for(cptr;cptr < lenFiltre;cptr++)
				{
					if(cptr == len)
						break;
					
					_customCheckbox 				= new CustomChekBox();
					_customCheckbox.filterObject 	= _listeFilter[cptr];
					_customCheckbox.addEventListener(MouseEvent.CLICK, filtreDataHandler);
					_vbxCheckbox.addChild(_customCheckbox);
				}
				
				hbxContainVbox.addChild(_vbxCheckbox);
			}
			
			hbxContainVbox.validateNow();
		}
		
		private function chekLastItemSelected():void
		{
			var lenOpt:int = myElements.OPTIONS.length;
			
			if(lenOpt > 0)
			{
				var len:int = listProduit.length;
				
				for(var i:int = 0;i < len;i++)
				{
					var idItemCat:int = listProduit[i].IDCATALOGUE;
					
					for(var j:int = 0;j < lenOpt;j++)
					{
						if(myElements.OPTIONS[j].IDCATALOGUE == idItemCat)
						{
							listProduit[i].SELECTED = true;
							break;
						}
					}
				}
				
				calculTotalSelected();
			}
		}
		
		private function findCheckBoxSelected():Array
		{
			var idToSearch		:ArrayCollection = new ArrayCollection();
			var childrenHbx		:Array 			 = hbxContainVbox.getChildren();
			var lenHbxChildren	:int 			 = hbxContainVbox.numChildren;
			var cptr			:int			 = 0;
			
			for( var i:int = 0;i < lenHbxChildren;i++)
			{
				if(childrenHbx[i] as VBox)
				{
					var lenVbxChildren	:int 	= (childrenHbx[i] as VBox).numChildren;
					var childrenVbx		:Array 	= (childrenHbx[i] as VBox).getChildren();
					
					for( var j:int = 0;j < lenVbxChildren;j++)
					{
						if(childrenVbx[j] as CustomChekBox)
						{
							if((childrenVbx[j] as CustomChekBox).filterObject.SELECTED)
							{
								cptr++;
								idToSearch.addItem((childrenVbx[j] as CustomChekBox).filterObject.IDPRODUIT);
							}
						}
					}
				}
			}
			
			if(cptr == _listeFilter.length)
				ckbxSelectAll.selected = true;
			else
				ckbxSelectAll.selected = false;
			
			return idToSearch.source;
		}
		
		//CALCUL AU FUR ET A MESURE LE PRIX LORS DE LA SELECTION DES OPTIONS
		private function calculTotalSelected():void
		{
			var len:int = listProduit.length;
			
			nbrProduitSelected = 0;
			
			for(var i:int = 0;i < len;i++)
			{
				if(listProduit[i].SELECTED)
					nbrProduitSelected = nbrProduitSelected +1;
			}
		}
		
		private function selectUnselectAll(isSelect:Boolean):void
		{
			var childrenHbx		:Array 			 = hbxContainVbox.getChildren();
			var lenHbxChildren	:int 			 = hbxContainVbox.numChildren;
			
			for( var i:int = 0;i < lenHbxChildren;i++)
			{
				if(childrenHbx[i] as VBox)
				{
					var lenVbxChildren	:int 	= (childrenHbx[i] as VBox).numChildren;
					var childrenVbx		:Array 	= (childrenHbx[i] as VBox).getChildren();
					
					for( var j:int = 0;j < lenVbxChildren;j++)
					{
						if(childrenVbx[j] as CustomChekBox)
						{
							(childrenVbx[j] as CustomChekBox).filterObject.SELECTED = isSelect;
							
							if(isSelect)
								(childrenVbx[j] as CustomChekBox).dispatchEvent(new Event(CustomChekBox.SELECTED_TRUE));
							else
								(childrenVbx[j] as CustomChekBox).dispatchEvent(new Event(CustomChekBox.SELECTED_FALSE));
						}
					}
				}
			}
			
			hbxContainVbox.validateNow();
		}

	}
}
