package gestioncommande.popup
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.CommandeArticles;
	import gestioncommande.services.ModeleService;
	
	import mx.containers.Box;
	import mx.controls.CheckBox;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class PopUpLibelleModeleImpl extends Box
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var txtConfigurationName	:TextInput;
		
		public var ckbxAllPools			:CheckBox;
						
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _modele				:ModeleService = new ModeleService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//	

		public var myCommande			:Commande;

		public var libelle				:String = "";

		public var idModele				:int = 0;
		public var thisPool				:int = -1;
				
		private var _idpool				:int = 0;

		//--------------------------------------------------------------------------------------------//
		//					CONSTANTES
		//--------------------------------------------------------------------------------------------//
				
		public static const CLOSE_EDIT_MODELE		:String = "CLOSE_EDIT_MODELE";

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function PopUpLibelleModeleImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
	
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
			
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//
	
		protected function btnValiderClickHandler(e:Event):void
		{
			if(txtConfigurationName.text != "")
			{
				if(idModele > 0)
				{
					if(ckbxAllPools.selected)
						_idpool = 0;
					else
						_idpool = thisPool;
					
					majModele();
				}
				else
					saveModele();
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_libell___'), "Conoview", null);
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
	
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			if(idModele > 0)
			{
				ckbxAllPools.visible = ckbxAllPools.includeInLayout = false;
				
				getThisModele();
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getThisModele():void
		{
			_modele.getModeleCommande(idModele);
			_modele.addEventListener(CommandeEvent.MODELE_ADD, thisModeleHandler);
		}

		private function majModele():void
		{
			_modele.majModele(idModele, txtConfigurationName.text, _idpool, _modele.listeModele);
			_modele.addEventListener(CommandeEvent.MODELE_UPDATED, 	majModeleHandler);
		}
		
		private function saveModele():void
		{
			_modele.sauvegardeModele(createCommande(), createXML(), ckbxAllPools.selected);
			_modele.addEventListener(CommandeEvent.MODELE_CREATED, saveModeleHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//

		private function thisModeleHandler(cmde:CommandeEvent):void
		{
		}
		
		private function majModeleHandler(cmde:CommandeEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Mod_le_mis___jour'), this);
			dispatchEvent(new Event(CLOSE_EDIT_MODELE));
			PopUpManager.removePopUp(this);
		}
		
		private function saveModeleHandler(cmde:CommandeEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Mod_le_sauvegard_'), this);
			PopUpManager.removePopUp(this);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - CREATION DU MODELE
		//--------------------------------------------------------------------------------------------//

		private function createCommande():Commande
		{
			var _cmd:Commande				= new Commande();
				_cmd.IDGESTIONNAIRE 		= 0;
				_cmd.IDPROFIL_EQUIPEMENT 	= myCommande.IDPROFIL_EQUIPEMENT;
				_cmd.IDPOOL_GESTIONNAIRE 	= SessionUserObject.singletonSession.POOL.IDPOOL;
				_cmd.IDOPERATEUR 			= myCommande.IDOPERATEUR;
				_cmd.IDREVENDEUR 			= myCommande.IDREVENDEUR;
				_cmd.IDCONTACT 				= 0;
				_cmd.IDSITELIVRAISON 		= 0;
				_cmd.LIBELLE_CONFIGURATION 	= txtConfigurationName.text;
				_cmd.REF_CLIENT1 			= "";
				_cmd.REF_CLIENT2 			= "";
				_cmd.REF_OPERATEUR 			= myCommande.REF_OPERATEUR;
				_cmd.COMMENTAIRES 			= "";
				_cmd.MONTANT 				= 0;
				_cmd.IDTYPE_COMMANDE		= myCommande.IDTYPE_COMMANDE;
				_cmd.IDGROUPE_REPERE 		= myCommande.IDGROUPE_REPERE;
				
			return _cmd;
		}
		
		private function createXML():XML
		{
			var myElements	:ElementsCommande2 = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			var cart		:CommandeArticles = new CommandeArticles();
			var conf		:ConfigurationElements = new ConfigurationElements();
			var articles	:XML = <articles></articles>;
			var article		:XML = <article></article>;
			var xmlEq		:XML = cart.addEquipements(myElements);
			var xmlRs		:XML = cart.addRessources(myElements);
			var engagement	:Engagement = myElements.ENGAGEMENT;
	
			article.appendChild(xmlEq);
			article.appendChild(xmlRs);
			
			conf.ID_TYPELIGNE = myElements.IDTYPELIGNE;
			conf.TYPELIGNE 	  = myElements.TYPELIGNE;
			
			article = cart.addConfiguration(article, conf, engagement, 1);
			
			articles.appendChild(article);
			
			return articles;
		}

	}
}