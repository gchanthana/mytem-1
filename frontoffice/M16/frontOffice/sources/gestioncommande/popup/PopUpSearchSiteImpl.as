package gestioncommande.popup
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	public class PopUpSearchSiteImpl extends TitleWindow
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMS
			//--------------------------------------------------------------------------------------------//
			
		public var dgSites							:DataGrid;
		
		public var txtSearch						:TextInput;
		
		public var btnValider						:Button;
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
			
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
		
		private var _siteSelected					:Object = null;
		
			//--------------------------------------------------------------------------------------------//
			//					DIVERS
			//--------------------------------------------------------------------------------------------//
		
		[Bindable] public var listeSites			:ArrayCollection = new ArrayCollection();
		
		public var isSiteLivraison					:Boolean = true;
				
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function PopUpSearchSiteImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		public function setInfos(values:ArrayCollection, currentItem:Object, isSiteLivraison:Boolean):void
		{
			listeSites = values;
			
			this.isSiteLivraison = isSiteLivraison;

			if(currentItem != null)
			{
				_siteSelected = currentItem;

				setSelectedSite(_siteSelected);
			}
		}
		
		public function getInfos():Object
		{
			return _siteSelected; 
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PROTECTED FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		protected function closeWindowHandler(ce:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			if(!validateCurrentPopUp())
				ConsoviewAlert.afficherAlertInfo("Veuillez sélectionner un site.", 'Consoview', null);
			else
				dispatchEvent(new Event('SEARCH_SITE_SELECTED'));
		}
		
		protected function dgSitesItemClickHandler(le:ListEvent):void
		{
			var itemRenderer	:Object = le.itemRenderer;
			var currentItem		:Object = itemRenderer.data;

			if(currentItem != null)
			{
				_siteSelected = currentItem;
				
				setSelectedSite(_siteSelected);
			}
			else
				_siteSelected = null;

			btnValider.enabled = validateCurrentPopUp();
		}
		
		protected function txtSearchHandler(e:Event):void
		{
			if(listeSites != null)
			{
				listeSites.filterFunction = filterFunction;
				listeSites.refresh();
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			btnValider.enabled = validateCurrentPopUp();
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUCNTIONS
			//--------------------------------------------------------------------------------------------//	
		
		private function setSelectedSite(currentSite:Object):void
		{
			var lenSites:int = listeSites.length;
			
			for(var i:int = 0;i < lenSites;i++)
			{
				if(listeSites[i].IDSITE_PHYSIQUE == currentSite.IDSITE_PHYSIQUE)
					listeSites[i].SELECTED = true;
				else
					listeSites[i].SELECTED = false;
				
				listeSites.itemUpdated(listeSites[i]);
			}
		}
		
		private function validateCurrentPopUp():Boolean
		{
			var status:Boolean = false;
			
			if(_siteSelected != null)
				status = true;
			
			return status;
		}

			//--------------------------------------------------------------------------------------------//
			//					FUCNTIONS - FILTRE
			//--------------------------------------------------------------------------------------------//	

		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && 	( 
										(item.NOM_SITE != null && item.NOM_SITE.toLocaleLowerCase().search(txtSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.SP_REFERENCE != null && item.SP_REFERENCE.toLocaleLowerCase().search(txtSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.ADRESSE != null && item.ADRESSE.toLocaleLowerCase().search(txtSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.SP_CODE_POSTAL != null && item.SP_CODE_POSTAL.toLocaleLowerCase().search(txtSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.SP_COMMUNE != null && item.SP_COMMUNE.toLocaleLowerCase().search(txtSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.PAYS != null && item.PAYS.toLocaleLowerCase().search(txtSearch.text.toLocaleLowerCase()) != -1)
									);
			
			return rfilter;
		}
	}
}