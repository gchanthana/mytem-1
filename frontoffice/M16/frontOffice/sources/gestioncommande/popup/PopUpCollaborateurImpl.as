package gestioncommande.popup
{	
	import composants.pagindatagrid.PaginateDatagrid;
	import composants.pagindatagrid.pagination.event.PaginationEvent;
	import composants.searchpagindatagrid.SearchPaginateDatagrid;
	import composants.searchpagindatagrid.event.SearchPaginateDatagridEvent;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestioncommande.entity.Collaborateur;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.EmployeService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	[Bindable]
	public class PopUpCollaborateurImpl extends TitleWindow
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var myPaginatedatagrid		:PaginateDatagrid;
		
		public var spdClef 					:SearchPaginateDatagrid;
						
		//--------------------------------------------------------------------------------------------//
		//					POPUP
		//--------------------------------------------------------------------------------------------//
		
		private var _popUpInfosCollaborateur:PopUpInfosCollaborateurIHM; 
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _employeSrv				:EmployeService = new EmployeService();

		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var selectedConfiguration	:Object;

		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//	
		
		public var listCollaborateur		:ArrayCollection = new ArrayCollection();

		public var idPool					:int = 0;
		
		public static var NBITEMPARPAGE		:int = 30;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function PopUpCollaborateurImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					LISTENER IHM
	//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener("EDITCOLLABORATEUR", editEmpoyeHandler);
			
			myPaginatedatagrid.addEventListener(PaginationEvent.INTERVALLE_SELECTED_CHANGE, retrieveProcedure);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnNewCollabClickHandler(me:MouseEvent):void
		{
			editEmpoyeHandler(me, true);
		}

		protected function spdClefSearchHandler(spde:SearchPaginateDatagridEvent):void
		{
			getEmployes();
		}
		
		protected function myPaginatedatagridClickHandler(me:MouseEvent):void
		{
			if(myPaginatedatagrid.selectedItem != null)
			{
				var idCollaborateur	:int = myPaginatedatagrid.selectedItem.IDEMPLOYE;
				var lenEmploye		:int = listCollaborateur.length;
				
				for(var i:int = 0; i < lenEmploye;i++)
				{
					listCollaborateur[i].SELECTED = false;
					
					if(listCollaborateur[i].IDEMPLOYE == idCollaborateur)
						listCollaborateur[i].SELECTED = true;
				}
				
				listCollaborateur.itemUpdated(myPaginatedatagrid.selectedItem);
			}
		}
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			if(findCollaborateurSelected())
			{
				dispatchEvent(new Event("REFRESH_ITEMSELECTED", true));
				PopUpManager.removePopUp(this);
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_collaborateur'), 'Consoview', null);
			}
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			closeHandler(me);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PUBLIC
	//--------------------------------------------------------------------------------------------//		
	
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION LISTENERS
		//--------------------------------------------------------------------------------------------//
		
		private function employesListedHandler(ce:CommandeEvent):void
		{			
			listCollaborateur = new ArrayCollection();
			listCollaborateur = _employeSrv.listeEmployes;
			
			var lenEmploye:int = listCollaborateur.length;
			
			if(selectedConfiguration.ID_COLLABORATEUR > 0)
			{
				for(var i:int = 0; i < lenEmploye;i++)
				{
					if(listCollaborateur[i].IDEMPLOYE == selectedConfiguration.ID_COLLABORATEUR)
					{
						listCollaborateur[i].SELECTED = true;
						listCollaborateur.itemUpdated(listCollaborateur[i]);
						return;
					}
				}
			}
		
			(myPaginatedatagrid.dgPaginate.dataProvider as ArrayCollection).refresh();
			
			if(lenEmploye > 0)
				myPaginatedatagrid.paginationBox.initPagination(listCollaborateur[lenEmploye-1].NBROWS, 30);
			else
				myPaginatedatagrid.paginationBox.initPagination(0, 0);
			
			(myPaginatedatagrid.dgPaginate.dataProvider as ArrayCollection).refresh();
		}
		
		protected function editEmpoyeHandler(e:Event, isNew:Boolean = false):void
		{
			var idempl:int = -1;
			var idGrpe:int = -1;
			
			_popUpInfosCollaborateur = new PopUpInfosCollaborateurIHM();

			if(!isNew)
			{
				if(myPaginatedatagrid.selectedItem != null)
				{
					idempl = myPaginatedatagrid.selectedItem.IDEMPLOYE;
					idGrpe = myPaginatedatagrid.selectedItem.IDGROUPE_CLIENT;
				}
			}
			else
				_popUpInfosCollaborateur.listeOrganisations = selectedConfiguration.ORGANISATIONS;

			_popUpInfosCollaborateur.idPool 		 	= idPool;
			_popUpInfosCollaborateur.idCollaborateur 	= idempl;
			_popUpInfosCollaborateur.idgroupeClient  	= idGrpe;			
			_popUpInfosCollaborateur.addEventListener("REFRESH_EMPLOYES", refreshEmpoyeHandler);
			
			PopUpManager.addPopUp(_popUpInfosCollaborateur, this, true);
			PopUpManager.centerPopUp(_popUpInfosCollaborateur);
		}
		
		private function refreshEmpoyeHandler(e:Event):void
		{
			getEmployes();
		}

		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//

		private function retrieveProcedure(pe:PaginationEvent):void
		{
			getEmployes();
		}
		
		private function getEmployes():void
		{
			if(listCollaborateur != null)
				listCollaborateur.removeAll();
			
			var typeorder	:String = spdClef.comboOrderBy.selectedItem.value;										//--> 'Ordre'
			var column		:String = filtreFromComboboxFilter(spdClef.comboRecherche.selectedItem.columDBName);	//--> 'Dans' 
			var tri			:String = filtreFromComboboxFilter(spdClef.comboTrieColonne.selectedItem.columDBName);	//--> 'Trier par'
			
			var depart		:int = 1;
			var fin			:int = 30;
			
			if(myPaginatedatagrid.currentIntervalle != null)
			{
				depart 	= myPaginatedatagrid.currentIntervalle.indexDepart;
				fin		= myPaginatedatagrid.currentIntervalle.indexDepart + myPaginatedatagrid.currentIntervalle.tailleIntervalle;
			}
			
			_employeSrv.fournirListeEmployesV2(idPool, column, tri, typeorder, depart, NBITEMPARPAGE, spdClef.txtCle.text);
			_employeSrv.addEventListener(CommandeEvent.EMPLOYES_LISTED, employesListedHandler);
		}
		
		private function filtreFromComboboxFilter(libelle:String):String
		{
			var column:String = 'MATRICULE';
			
			switch(libelle)
			{
				case 'MATRICULE'	: column = 'MATRICULE'; break;
				case 'NOMPRENOM'	: column = 'NOM'; 		break;
			}
			
			return column;
		}

		private function findCollaborateurSelected():Boolean
		{
			var lenEmp	:int = listCollaborateur.length;
			
			for(var i:int = 0;i < lenEmp;i++)
			{
				if(listCollaborateur[i].SELECTED)
				{
					selectedConfiguration.ID_COLLABORATEUR 	= listCollaborateur[i].IDEMPLOYE;
					selectedConfiguration.COLLABORATEUR 	= listCollaborateur[i].NOMPRENOM;
					selectedConfiguration.MATRICULE 		= listCollaborateur[i].MATRICULE;
					
					return true;
				}
			}
			
			return false;
		}

	}
}
