package gestioncommande.popup
{
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.MultiCompte;
	import gestioncommande.entity.MultiSousCompte;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.ViewStackEvent;
	import gestioncommande.services.Formator;
	import gestioncommande.services.LigneService;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	import mx.messaging.AbstractConsumer;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class ModifyLineImpl extends TitleWindow
	{
		
		//COMPOSANTS
		public var cbxSearchIn			:ComboBox;
		public var cbxOperateurs		:ComboBox;
		
		public var chkbxEligibilite		:CheckBox;
		public var chkbxSelectAll		:CheckBox;
		
		public var txtbxClef			:TextInput;
		public var txtptSearch 			:TextInput;

		public var rdAct				:RadioButton;
		public var rdSus				:RadioButton;
		public var rdRes				:RadioButton;
		
		public var btnSearch 			:Button;
		
		public var dgLines				:DataGrid;

		//OBJECTS
		private var _lignesSrv			:LigneService = new LigneService();

		//VARIABLES LOCALES
		public var operateurs			:ArrayCollection = null;
		public var searchIn				:ArrayCollection = null;
		public var linesSelected		:ArrayCollection = null;
		public var arrayCompte			:ArrayCollection = null;
		public var arraySousCompte		:ArrayCollection = null;
		public var compteSousCompte		:ArrayCollection = null;
		public var linesCompteur		:ArrayCollection = new ArrayCollection();

	   	public var operateurObject		:Object = null;
		
		public var isMultiCompte		:Boolean = false;
		public var isMultiSousCompte	:Boolean = false;
		public var isNoCompte			:Boolean = false;
		public var isPMU				:Boolean = false;
				
		public var numberSelected		:int = 0;
		public var idracine				:int = 0;
		
		private var _arrayColl			:ArrayCollection = new ArrayCollection();
		
		private var IDOPERATEUR			:int = 595;//---> OBS DATA MANAGER
		private var IDRACINE			:int = 415;//---> RACINE PMU
		
		//EVENEMENTS
		public static const NEXTISCLICKED		:String = "NEXTISCLICKED";
		public static const ANNULISCLICKED		:String = "ANNULISCLICKED";
		
		public function ModifyLineImpl()
		{
			idracine = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		
		protected function creationCompleteHandler(e:Event):void
		{			
			btnSearch.enabled = false;
			
			_lignesSrv.fournirListeOperateurs();
			createObjectSearch();
			
			SessionUserObject.singletonSession.CURRENTCONFIGURATION = new ElementsCommande2();
			
			addEventListener(ViewStackEvent.CLOSETHIS,		closeHandler);
			
			_lignesSrv.addEventListener(CommandeEvent.LISTED_OPERATEURS,		listOperateursHandler);
			_lignesSrv.addEventListener(CommandeEvent.LISTED_LIGNESBY_OPE,	listLignesByOperateurHandler);
		}

		private function listOperateursHandler(cmde:CommandeEvent):void
		{
			if(_lignesSrv.listeOperateurs.length == 1)
			{
				cbxOperateurs.dataProvider 	= _lignesSrv.listeOperateurs[0];
				cbxOperateurs.errorString 	= "";
				btnSearch.enabled = true;
				
				var idOperateur:int = _lignesSrv.listeOperateurs[0].OPERATEURID;
				
				if(idOperateur == IDOPERATEUR && idracine == IDRACINE)
					isPMU = true;
				else
					isPMU = false;
				
				addRemoveCodeInterne();
			}
			else
			{
				if(_lignesSrv.listeOperateurs.length != 0)
				{
					cbxOperateurs.prompt 		= ResourceManager.getInstance().getString('M16', 'S_lectionnez');
					cbxOperateurs.dataProvider 	= _lignesSrv.listeOperateurs;
				}
				else
				{
					cbxOperateurs.prompt 		= ResourceManager.getInstance().getString('M16', 'Aucun_op_rateur');
					cbxOperateurs.dataProvider 	= _lignesSrv.listeOperateurs;
				}
			}
		}
		
		private function listLignesByOperateurHandler(cmdEvt:CommandeEvent):void
		{
			if(_lignesSrv.listeLignesByOperateur.length != 0)
			{
				dgLines.dataProvider = _lignesSrv.listeLignesByOperateur;
				linesCompteur = dgLines.dataProvider as ArrayCollection;
			}
			
			filtreData();
		}

		//--------------------------------------------------------------------------------------------//
		//					PROTECTED EVENEMENTS
		//--------------------------------------------------------------------------------------------//

		protected function closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnAnnulerClickHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnNextClickHandler(e:Event):void
		{
			if(!checkCompteSousCompte())
				continueCompteSousCompte();
		}
		
		//---RAJOUT

		protected function rdChangeHandler(e:Event):void
		{
			filtreData();
		}
		
		protected function filtreData():void
		{
			if(dgLines.dataProvider != null)
			{
				(dgLines.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgLines.dataProvider as ArrayCollection).refresh();
				
				linesCompteur = dgLines.dataProvider as ArrayCollection;
				linesCompteur.refresh();
			}
		}
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = false;		    
			
			var collaborateur		:String = stringIsNull(item.COLLABORATEUR);
			var ligne				:String = stringIsNull(item.LIGNE);
			var compteFacturation	:String = stringIsNull(item.COMPTE_FACTURATION);
			var sousCompte			:String = stringIsNull(item.SOUS_COMPTE);
			var dateEligibilite		:String = DateFunction.formatDateAsString(item.DATE_ELLIGIBILITE);
			var reste				:String = stringIsNull(item.RESTE);
			var codeInterne			:String = stringIsNull(item.CODE_INTERNE);			
			
			rfilter =	(	collaborateur.toLowerCase().indexOf(txtptSearch.text.toLowerCase()) 		!= -1
							|| ligne.toLowerCase().indexOf(txtptSearch.text.toLowerCase()) 				!= -1
							|| compteFacturation.toLowerCase().indexOf(txtptSearch.text.toLowerCase()) 	!= -1
							|| sousCompte.toLowerCase().indexOf(txtptSearch.text.toLowerCase())			!= -1
							|| dateEligibilite.toLowerCase().indexOf(txtptSearch.text.toLowerCase()) 	!= -1
							|| reste.toLowerCase().indexOf(txtptSearch.text.toLowerCase()) 				!= -1
							|| codeInterne.toLowerCase().indexOf(txtptSearch.text.toLowerCase()) 		!= -1

					 	) &&  otherFilter(item);
			
			return rfilter;
		}
		
		private var _idetat:int = 1;
		
		//1-> ACTIVE, 2-> SUSPENDUE, 3-> RÉSILIÉE
		private function otherFilter(item:Object):Boolean
		{
			var bool:Boolean = false;
			
			if(rdAct.selected)
			{
				if(item.IDETAT_LIGNE == 1)
				{
					bool = true;
					_idetat = 1;
				}
			}
			
			if(rdSus.selected)
			{
				if(item.IDETAT_LIGNE == 2)
				{
					bool = true;
					_idetat = 2;
				}
			}
			
			if(rdRes.selected)
			{
				if(item.IDETAT_LIGNE == 3)
				{
					bool = true;
					_idetat = 3;
				}
			}
			
			return bool;
		}
		
		//---RAJOUT

		private function checkCompteSousCompte():Boolean
		{
			if(dgLines.dataProvider == null) return false;
			
			var lines		:ArrayCollection = dgLines.dataProvider as ArrayCollection;
			var lenLines	:int = lines.length;
			var lastEtat	:int = 0;
			var boolWCpte	:Boolean = false;
			var boolNCpte	:Boolean = false;
			var boolRtrn	:Boolean = false;
			var boolEtat	:Boolean = false;
			var boolTest	:Boolean = false;
			
			for(var i:int = 0;i < lenLines;i++)
			{
				if(lines[i].SELECTED)
				{
					if(!boolTest)
					{
						lastEtat = lines[i].IDETAT_LIGNE;
						boolTest = true;
					}
					
					if(boolTest)
					{
						if(lastEtat != lines[i].IDETAT_LIGNE)
						{
							ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Attention___Vous_avez_s_lectionn__des_lignes_d'), 'Consoview', null);
							return true;
						}
					}
					
					if(lines[i].COMPTE_FACTURATION == "" && lines[i].SOUS_COMPTE == "" || 
						lines[i].COMPTE_FACTURATION != "" && lines[i].SOUS_COMPTE == "")
					{
						boolNCpte = true;
					}
					
					if(lines[i].COMPTE_FACTURATION != "" && lines[i].SOUS_COMPTE != "")
					{
						boolWCpte = true;
					}						
				}
			}
			
			SessionUserObject.singletonSession.ETATLIGNERESSUSMOD = lastEtat;
			
			if(lastEtat == 3)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Attention___Il_est_impossible_d_effectuer_une_'), 'Consoview', null);
				return true;
			}
			
			if(boolWCpte == false && boolNCpte == true)
				isNoCompte = true;
			else
				isNoCompte = false;
			
			boolRtrn = boolWCpte&&boolNCpte;
			
			if(boolRtrn)
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Attention_nous_vous_invitons_a_ne_choisi'), 'Consoview', null);
			
			return boolRtrn;
		}
		
		private function continueCompteSousCompte():void
		{
			if(addLinesSelected())
			{
				operateurObject = cbxOperateurs.selectedItem;
				dispatchEvent(new Event(NEXTISCLICKED));
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_au_minimun_une_lig'), ResourceManager.getInstance().getString('M16', 'Consoview'), null);
		}
		
		protected function cbxOperateursChangeHandler(e:Event):void
		{
			if(cbxOperateurs.selectedItem != null && cbxSearchIn.selectedItem != null)
			{
				btnSearch.enabled = true;
				
				var idOperateur:int = cbxOperateurs.selectedItem.OPERATEURID;
				
				if(idOperateur == IDOPERATEUR && idracine == IDRACINE)
					isPMU = true;
				else
					isPMU = false;
			
				addRemoveCodeInterne();
			}
			else
				btnSearch.enabled = false;
		}
		
		protected function enterHandler(e:Event):void
		{
			if(cbxOperateurs.selectedItem != null)
				btnSearchClickHandler(e);
		}
		
		protected function cbxSearchInChangeHandler(e:Event):void
		{
			if(cbxSearchIn.selectedItem != null && cbxOperateurs.selectedItem != null)
				btnSearch.enabled = true;
			else
				btnSearch.enabled = false;
		}
		
		protected function btnSearchClickHandler(e:Event):void
		{
			if(cbxOperateurs.selectedItem != null && cbxSearchIn.selectedItem != null)
			{
				numberSelected 			= 0;
				txtptSearch.text 		= "";
				chkbxSelectAll.selected = false;
				
				if(dgLines.dataProvider != null)
					(dgLines.dataProvider as ArrayCollection).removeAll();
				
				_lignesSrv.fournirListeLignesByOperateur(SessionUserObject.singletonSession.POOL.IDPOOL,
															cbxOperateurs.selectedItem.OPERATEURID,
																chkbxEligibilite.selected,
																	cbxSearchIn.selectedItem.IDSEARCH,
																		txtbxClef.text);
			}
		}

		protected function chkbxEligibiliteClickHandler(e:Event):void
		{
		}
		
		protected function chkbxSelectAllClickHandler(e:Event):void
		{
			var lines:ArrayCollection = dgLines.dataProvider as ArrayCollection;
			
			if(lines == null) return;
			
			for(var i:int = 0;i < lines.length;i++)
			{
				lines[i].SELECTED =  chkbxSelectAll.selected;
			}
			
			lines.refresh();
			compteLinesSelected();
		}

		private function stringIsNull(value:String):String
		{
			var stringFormated:String = "";
			
			if(value != null)
				stringFormated = value;
			
			return stringFormated;
		}
		
		protected function dgLinesClickHandler(e:Event):void
		{
			if(dgLines.selectedItem != null)
			{
				if(dgLines.selectedItem.SELECTED)
				{
					dgLines.selectedItem.SELECTED = false;
					
					if(chkbxSelectAll.selected)
						chkbxSelectAll.selected	= false;
				}
				else
					dgLines.selectedItem.SELECTED = true;

				(dgLines.dataProvider as ArrayCollection).itemUpdated(dgLines.selectedItem);
				checkSelectedInReference(dgLines.selectedItem.IDSOUS_TETE, dgLines.selectedItem.SELECTED);
				compteLinesSelected();
			}
		}
		
		private function checkSelectedInReference(valueID:int, valueSELECTED:Boolean):void
		{
			var lines	:ArrayCollection = dgLines.dataProvider as ArrayCollection;
			var len		:int = lines.length;
			
			for(var i:int = 0; i < len;i++)
			{
				if(lines[i].IDSOUS_TETE  == valueID)
				{
					lines[i].SELECTED = valueSELECTED;
					break;
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATE
		//--------------------------------------------------------------------------------------------//
		
		private function createObjectSearch():void
		{
			var objSearch:Object;
			searchIn = new ArrayCollection();

			objSearch = new Object();
			objSearch.IDSEARCH			= 1;
			objSearch.LIBELLE_SEARCH	= ResourceManager.getInstance().getString('M16', 'Collaborateur');
			searchIn.addItem(objSearch);
			
			objSearch = new Object();
			objSearch.IDSEARCH			= 2;
			objSearch.LIBELLE_SEARCH	= ResourceManager.getInstance().getString('M16', 'Ligne');
			searchIn.addItem(objSearch);
			
			objSearch = new Object();
			objSearch.IDSEARCH			= 4;
			objSearch.LIBELLE_SEARCH	= ResourceManager.getInstance().getString('M16', 'Compte');
			searchIn.addItem(objSearch);
			
			objSearch = new Object();
			objSearch.IDSEARCH			= 3;
			objSearch.LIBELLE_SEARCH	= ResourceManager.getInstance().getString('M16', 'Sous_compte');
			searchIn.addItem(objSearch);
		}
		
		private function addRemoveCodeInterne():void
		{
			var len			:int = searchIn.length;
			var idx			:int = -1;
			
			for(var i:int = 0;i < len;i++)
			{
				if(searchIn[i].IDSEARCH == 5)
					idx = i;
			}
			
			if(isPMU)
			{
				if(idx < 0)
				{
					var obj:Object 			= new Object();
						obj.IDSEARCH		= 5;
						obj.LIBELLE_SEARCH	= ResourceManager.getInstance().getString('M16', 'Code_interne');
					
					searchIn.addItem(obj);
				}
			}
			else
			{
				if(idx > -1)
					searchIn.removeItemAt(idx);
			}
			
			searchIn.refresh();
			(cbxSearchIn.dataProvider as ArrayCollection).refresh();
		}
		
		private function compteLinesSelected():void
		{
			var lines:ArrayCollection = dgLines.dataProvider as ArrayCollection;
			
			numberSelected = 0;
			
			for(var i:int = 0;i < lines.length;i++)
			{
				if(lines[i].SELECTED)
					numberSelected = numberSelected + 1;
			}
		}
		
		private function addLinesSelected():Boolean
		{
			var bool		:Boolean 		 = false;
			var linesValue	:ArrayCollection = new ArrayCollection();
			var lines		:ArrayCollection = dgLines.dataProvider as ArrayCollection;
			
			linesSelected = new ArrayCollection();
			
			SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS = new ArrayCollection();
			
			if(lines == null) return false;
			
			for(var i:int = 0;i < lines.length;i++)
			{
				if(lines[i].SELECTED)
				{
					SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.addItem(lines[i]);
					linesSelected.addItem(lines[i]);
				}
			}
			
			linesValue = Formator.sortFunction(linesSelected ,'IDPOOL');
			
			if(linesSelected.length != 0)
			{
				isMultiCompte 		= false;
				isMultiSousCompte	= false;
				contain(linesSelected);
				bool = true;
			}
			
			return bool;
		}

		
		private function contain(values:ArrayCollection):void
	    {
			arrayCompte 	 = new ArrayCollection();
			arraySousCompte  = new ArrayCollection();
			compteSousCompte = new ArrayCollection();
			
	    	if(values != null)
	    	{
		    	arrayCompte		 = listeAllCompte(values);
				arraySousCompte	 = listeAllSousCompte(values);
				compteSousCompte = listeAll();
	    	}
		}
		
		private function listeAll():ArrayCollection
		{
			var liste	:ArrayCollection = new ArrayCollection();
			var temp 	:ArrayCollection = new ArrayCollection();			
			
			if(_arrayColl != null)
			{
				for(var i:int = 0;i < _arrayColl.length;i++)
				{
					liste.addItem(listeCompteSousCompte(_arrayColl[i],_arrayColl[i].LIGNES));
				}
			}
			
			return liste;
		}

		private function listeAllCompte(values:ICollectionView):ArrayCollection
		{	
			var liste	:ArrayCollection = new ArrayCollection();
			var item	:MultiCompte;

	     	values = Formator.sortFunction(values as ArrayCollection, 'IDCOMPTE_FACTURATION') as ICollectionView;
			
			if(values != null)
			{
				var cursor:IViewCursor = values.createCursor();
								
				while(!cursor.afterLast)
				{
					var integer:int = ConsoviewUtil.getIndexById(liste,"IDCOMPTE_FACTURATION", cursor.current.IDCOMPTE_FACTURATION)
					
					if(integer < 0)
					{
						if(cursor.current.IDCOMPTE_FACTURATION > -1)
						{
							item = new MultiCompte();
							item.COMPTE_FACTURATION 	= cursor.current.COMPTE_FACTURATION;
							item.IDCOMPTE_FACTURATION	= cursor.current.IDCOMPTE_FACTURATION;
							item.NUMBER_OF_LINES = item.LIGNES.length;
							item.LIGNES.addItem(cursor.current);
							_arrayColl.addItem(item);
							liste.addItem(cursor.current);
						}
					}
					else
						item.LIGNES.addItem(cursor.current);					
					
					cursor.moveNext();
				}
				
				if(liste.length > 1)
					isMultiCompte = true;
			}
			
			return liste;
		}
		
		private function listeAllSousCompte(values:ICollectionView):ArrayCollection
		{	
			var liste:ArrayCollection = new ArrayCollection();
			
			if (values != null)
			{
				var cursor:IViewCursor = values.createCursor();
								
				while(!cursor.afterLast)
				{
					var integer:int = ConsoviewUtil.getIndexById(liste,"IDSOUS_COMPTE", cursor.current.IDSOUS_COMPTE)
					
					if(integer < 0)
					{
						if(cursor.current.IDSOUS_COMPTE > -1)
							liste.addItem(cursor.current);
					}	
					    			
					cursor.moveNext();
				}
				
				if(liste.length > 1)
					isMultiSousCompte = true;
			}
			
			return liste;
		}

		private function listeCompteSousCompte(object:Object, compte:ICollectionView):Object
		{
			var liste	:ArrayCollection = new ArrayCollection();
			var temp 	:ArrayCollection = new ArrayCollection();			
			var item	:MultiSousCompte;

			compte = Formator.sortFunction(compte as ArrayCollection, 'IDSOUS_COMPTE') as ICollectionView;
			
			if (compte != null)
			{
				var cursor : IViewCursor = compte.createCursor();
								
				while(!cursor.afterLast)
				{
					var integer:int = ConsoviewUtil.getIndexById(liste,"IDSOUS_COMPTE",cursor.current.IDSOUS_COMPTE)
					if(integer < 0)
					{
						if(cursor.current.IDSOUS_COMPTE > -1)
						{
							item = new MultiSousCompte();
							
							item.SOUS_COMPTE 			= cursor.current.SOUS_COMPTE;
							item.IDSOUS_COMPTE			= cursor.current.IDSOUS_COMPTE;
							item.LIGNES.addItem(cursor.current);
							object.MULTI_SOUS_COMPTE.addItem(item);
							liste.addItem(cursor.current);
						}
					}
					else
						item.LIGNES.addItem(cursor.current);					

					cursor.moveNext();
				}
			}
			
			return object;
		}

	}
}