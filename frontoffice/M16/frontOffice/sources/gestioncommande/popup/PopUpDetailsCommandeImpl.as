package gestioncommande.popup
{
	import commandemobile.ihm.mail.ActionMailBoxIHM;
	import commandemobile.itemRenderer.detailscommande.GeneralitemREndererIHM;
	import commandemobile.itemRenderer.detailscommande.HistoriqueItemRendererIHM;
	import commandemobile.itemRenderer.detailscommande.ViewArticlesItemRendererIHM;
	
	import composants.button.FreeButtonMenu;
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.util.CommandeFormatter;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import gestioncommande.composants.PiecesJointesIHM;
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.PieceJointeVO;
	import gestioncommande.entity.Transporteur;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.Export;
	import gestioncommande.services.Formator;
	import gestioncommande.services.WorkflowService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TabNavigator;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.TextArea;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class PopUpDetailsCommandeImpl extends TitleWindow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var tabNavCommande			:TabNavigator;
		
		public var btnValiderAction			:Button
		public var btnValider				:Button
		public var btnAnnuler				:Button
		
		public var cbxActions				:ComboBox;
		
		public var btExpedition				:FreeButtonMenu;
		public var btExporter				:FreeButtonMenu;
		
		public var general					:GeneralitemREndererIHM;
		public var historique				:HistoriqueItemRendererIHM;
		public var details					:ViewArticlesItemRendererIHM;
		public var piecesJointes			:PiecesJointesIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _popUpMailBox			:ActionMailBoxIHM;
		
		private var _popUpTransporteurs		:PopUpLivraisonIHM;

		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var myCommande				:Commande;
		
		private var selectedAction			:Action;
		
		private var _infosMail				:InfosObject = new InfosObject();
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _wflwSrv				:WorkflowService = new WorkflowService();

		private var _cmdSrv					:CommandeService = new CommandeService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listeActionsWorkflow		:ArrayCollection = new ArrayCollection();
		
		public var isModified				:Boolean = false;
		public var isActionVisible			:Boolean = false;
		public var isActive					:Boolean = true;
		
		//---> MODIFICATION MOBILE/FIXE, LIVRAISON PARTIELLE MOBILE/FIXE
		private var restrictedIDs			:Array = [2166, 2229, 2316, 2317];
		private var restrictedEtats			:Array = [2166, 2229, 2316, 2317];
		
		private var _myEtat					:int = 0;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function PopUpDetailsCommandeImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function closeHandler(ce:CloseEvent):void
		{
			dispatchEvent(new Event('DETAILS_CLOSED'));
			
			PopUpManager.removePopUp(this);
		}
		
		protected function cbxActionsChangeHandler(e:Event):void
		{
			if(cbxActions.selectedItem != null)
				btnValiderAction.enabled = true;
			else
				btnValiderAction.enabled = false;
		}
		
		protected function btnValiderActionClickHandler(me:MouseEvent):void
		{
			if(cbxActions.selectedItem != null)
			{
				if((cbxActions.selectedItem as Action).CODE_ACTION == 'ANNUL')
				{
					ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M16', 'Etes_vous_sur_de_vouloir_annuler_cette_c'), 'Consoview', resultAlert);
				}
				else if((cbxActions.selectedItem as Action).CODE_ACTION != null)
					{
						if((cbxActions.selectedItem as Action).CODE_ACTION == 'EXPED')
							afficherPopUpTransporteur();
						else
							afficherPopUpMail();
					}
					else
						sendAction();
			}
		}
		
		protected function btExpeditionClickHandler(me:MouseEvent):void
		{
			var boolTrack	:Boolean = false;
			var boolTrans	:Boolean = false;
			var message		:String = '';
			
			if(myCommande.NUMERO_TRACKING != "")
				boolTrack = true	    		
			else
				message += ResourceManager.getInstance().getString('M16', '___Pas_de_num_ro_de_suivi_sp_cifi__n')
			
			if(myCommande.IDTRANSPORTEUR)
				boolTrans = true	    		
			else
				message += ResourceManager.getInstance().getString('M16', '___Pas_de_transporteur_sp_cifi_')
			
			if(boolTrans && boolTrack)
				navigateToURL(new URLRequest(general.cbxTransporteur.selectedItem.URL_TRACKING + myCommande.NUMERO_TRACKING)/* */,"_blank")
			else
				ConsoviewAlert.afficherAlertInfo(message,'Consoview', null);
		}
		
		protected function btExporterClickHandler(e:Event):void
		{
			if(myCommande.IDCOMMANDE > 0)
			{
				if(myCommande.IDLAST_ETAT == 0)
					myCommande.IDLAST_ETAT =_myEtat;
				
				if(checkEtatCommande(myCommande.IDLAST_ETAT))
				{
					var myBDC	:PieceJointeVO = null;
					var len		:int = piecesJointes.piecesjointes.length;
					var numBDC	:String = 'BDC_' + myCommande.NUMERO_COMMANDE + '.pdf';
					
					for(var i:int = 0;i < len;i++)
					{
						if(piecesJointes.piecesjointes[i].LABEL_PIECE == numBDC)
							myBDC = piecesJointes.piecesjointes[i] as PieceJointeVO;
					}
					
					if(myBDC != null)
					{
						var url			:String = moduleCommandeMobileIHM.urlBackoffice + '/fr/consotel/consoview/M16/export/BDC_BipGenerate.cfm';
							
						var variables	:URLVariables 	= new URLVariables();
							variables.UUID			= myBDC.UUID;
							variables.LIBELLE		= 'BDC_' + myCommande.NUMERO_COMMANDE;
							variables.LIBELLEFILE	= myBDC.LABEL_PIECE;
							variables.FORMAT		= myBDC.FORMAT.replace('.','');
							variables.ARRAY			= SessionUserObject.singletonSession.LISTEACTIONSUSER;
							
						var request		:URLRequest = new URLRequest(url);
							request.data = variables;    
							request.method = 'POST';
							navigateToURL(request,'_blank');
					}
				}
				else
				{
					var export:Export = new Export();
						export.exporterCommandeEnPDF(myCommande);
				}
			}	
		}

		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			if(getCommandeParametres())
				setCommande();
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('DETAILS_CLOSED'));
			
			PopUpManager.removePopUp(this);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//		
		
		private function getInfosCommande():void
		{
			var idseg:int = 1;
			
			if(myCommande.SEGMENT_MOBILE == 1 && myCommande.SEGMENT_FIXE == 0 && myCommande.SEGMENT_DATA == 0)
				idseg = 1;//---MOBILE
			
			if(myCommande.SEGMENT_MOBILE == 0 && myCommande.SEGMENT_FIXE == 1 && myCommande.SEGMENT_DATA == 0)
				idseg = 2;//---FIXE
			
			if(myCommande.SEGMENT_MOBILE == 0 && myCommande.SEGMENT_FIXE == 0 && myCommande.SEGMENT_DATA == 1)
				idseg = 3;//---DATA
			
			getActionsWorflow();
			
			_cmdSrv.getInfosCommande(myCommande, idseg);
			_cmdSrv.addEventListener(CommandeEvent.COMMANDE_INFOS, infosCommandeHandler);
		}
		
		private function infosCommandeHandler(cmde:CommandeEvent):void
		{
			CommandeFormatter.getInstance().setCommandeCurrency(_cmdSrv.LISTEADRESSEREVENDEUR[0].DEVISE);
			if (_cmdSrv.LISTEADRESSEREVENDEUR[0].DEVISE != ResourceManager.getInstance().getString('M16', '_signe_de_la_devise__'))
			{
				details.currenciesAreDifferent = true;
			}
			general.listeAdresseRevendeur 	= _cmdSrv.LISTEADRESSEREVENDEUR;
			general.listeTransporteurs 		= _cmdSrv.LISTETRANSPORTEURS;
			general.listeLibellesComptes 	= _cmdSrv.LISTELIBELLESCOMPTES;
			
			historique.listeHistorique		= _cmdSrv.LISTEHISTORIQUE;
			 
			setProcedure();
			
			details.profilCommande			= _cmdSrv.currentCommande.LIBELLE_PROFIL;
		}
		
		/*
		 * récuperer les actions du wokflow d'une commande
		*/
		private function getActionsWorflow():void
		{
			historique.setProcedure(myCommande);
			
			enabledDisabledFreeButtonMenu();
			
			_myEtat = myCommande.IDLAST_ETAT;
			
			_wflwSrv.fournirActionsNoRemote(myCommande.IDLAST_ETAT, myCommande);
			
			listeActionsWorkflow = eraseWorkFlowRestricted(_wflwSrv.listeActions);
			
			if(listeActionsWorkflow.length > 0)
				isActionVisible = true;
			else
				isActionVisible = false;
			
			if(listeActionsWorkflow.length > 1)
			{
				cbxActions.prompt = ResourceManager.getInstance().getString('M16', 'S_lectionnez');
				cbxActions.selectedIndex = -1;
			}			
			
			if(listeActionsWorkflow.length == 1)
				btnValiderAction.enabled = true;
		}
		
		/*
		 * concernant une commande, envoyer le choix demandé (concernant cette commande, ex : annuler, accuser reception, etc) et diffuser l'action associé
		*/
		private function sendAction():void
		{
			_wflwSrv.faireActionWorkFlow(cbxActions.selectedItem as Action, myCommande);
			_wflwSrv.addEventListener(CommandeEvent.SEND_ACTION, sendActionHandler);
		}
		
		private function setCommande():void
		{
			isModified = true;
			
			myCommande.IS_LIV_DISTRIB			= general.chxAdresseRevendeur.selected;
			myCommande.LIVRAISON_DISTRIBUTEUR 	= Formator.formatBoolean(myCommande.IS_LIV_DISTRIB);
			
			_cmdSrv.majCommande(myCommande);
			_cmdSrv.addEventListener(CommandeEvent.COMMANDE_UPDATED, commandeHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		/*
		 * diffusion de l'action demandé concernant une commande
		*/
		private function sendActionHandler(cmde:CommandeEvent):void
		{
			myCommande.IDLAST_ETAT = (cbxActions.selectedItem as Action).IDETAT_ENGENDRE;
			
			isModified = true;
			
			historique.getHistorique();
			
			getActionsWorflow();
		}
		
		private function commandeHandler(cmde:CommandeEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Commande_modifi_e'), this);	
			
			dispatchEvent(new Event('DETAILS_CLOSED'));
			
			PopUpManager.removePopUp(this);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener('COMMANDE_CHANGE', enabledBtnValider);
			addEventListener('VIEW_CMD_PDF', btExporterClickHandler);
			
			general.setCommande(myCommande);
			
			getInfosCommande();
		}
		
		private function enabledBtnValider(e:Event):void
		{
			btnValider.enabled = true;
		}

		private function resultAlert(ce:CloseEvent):void
		{
			if(ce.detail == Alert.OK)
				sendAction();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENNERS POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private function transporteurSelectedHandler(e:Event):void
		{
			afficherPopUpMail();
		}
		
		private function mailSendHandler(e:Event):void
		{
			_popUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE, mailSendHandler);
			
			if(!_popUpMailBox.cbMail.selected)
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Validation_enregistr__'),this);
			else
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Votre_mail_a__t__envoy__'),this);
			
			PopUpManager.removePopUp(_popUpMailBox);
			
			myCommande.IDLAST_ETAT = (cbxActions.selectedItem as Action).IDETAT_ENGENDRE;
			
			isModified = true;
			
			historique.getHistorique();
			
			getActionsWorflow();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private function afficherPopUpTransporteur():void
		{
			_popUpTransporteurs 			= new PopUpLivraisonIHM();
			_popUpTransporteurs.myCommande 	= myCommande;		
			_popUpTransporteurs.addEventListener('TRANSPORTEUR_IGNORED', transporteurSelectedHandler);
			_popUpTransporteurs.addEventListener('TRANSPORTEUR_VALIDATE', transporteurSelectedHandler);
			
			PopUpManager.addPopUp(_popUpTransporteurs, this, true);
			PopUpManager.centerPopUp(_popUpTransporteurs);
		}
		
		private function afficherPopUpMail():void
		{
			_popUpMailBox = new ActionMailBoxIHM();
			
			_infosMail.commande 		 	= myCommande;
			_infosMail.historiqueCommande 	= historique.listeHistorique;
			_infosMail.corpMessage 		  	= (cbxActions.selectedItem as Action).MESSAGE;
			_infosMail.UUID				  	= piecesJointes.UUID;
			
			_popUpMailBox.idsCommande 	= [myCommande.IDCOMMANDE];
			_popUpMailBox.numCommande 	= myCommande.NUMERO_COMMANDE;
			_popUpMailBox.idtypecmd		= myCommande.IDTYPE_COMMANDE;
			
			var moduleText	:String = giveMeTheSegementTheme();
			var sujet		:String = giveMeSubject();
			var libelle		:String	= myCommande.LIBELLE_COMMANDE;
			var numero		:String	= myCommande.NUMERO_COMMANDE;
			var objet		:String = sujet + " / " + libelle + " / " + numero;
			
			_popUpMailBox.initMail(moduleText, objet, "");
			_popUpMailBox.configRteMessage(_infosMail, GabaritFactory.TYPE_COMMANDE_MOBILE);			
			_popUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE, mailSendHandler);
			_popUpMailBox.selectedAction = cbxActions.selectedItem as Action;
			
			PopUpManager.addPopUp(_popUpMailBox, this, true);
			PopUpManager.centerPopUp(_popUpMailBox);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION -POPUPs
		//--------------------------------------------------------------------------------------------//

		//--- 1-> NVL COMMANDE, 2-> MODIF OPTIONS, 3-> RENVLT, 4-> RESI,SUSP,REACT, -1-> DEFAUT
		private function giveMeSubject():String
		{
			var TC:String = "";
			
			switch(myCommande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	TC = ResourceManager.getInstance().getString('M16', 'Commande_Mobile');				break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_Fixe_R_seau');		break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_');		break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_'); 		break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--- REACT
				case TypesCommandesMobile.SAV: 					TC = ResourceManager.getInstance().getString('M16', 'SAV_'); 						break;//--- SAV
				case TypesCommandesMobile.NOUVELLE_CARTE_SIM: 	TC = ResourceManager.getInstance().getString('M16', 'Nouvelle_carte_SIM_');			break;//--- REACT
			}
			
			return TC;
		}
		
		private function giveMeTheSegementTheme():String
		{
			var moduleText:String = "";
			var idseg:int = 1;
			
			if(myCommande.SEGMENT_MOBILE == 1 && myCommande.SEGMENT_FIXE == 0 && myCommande.SEGMENT_DATA == 0)
				idseg = 1;//---MOBILE
			
			if(myCommande.SEGMENT_MOBILE == 0 && myCommande.SEGMENT_FIXE == 1 && myCommande.SEGMENT_DATA == 0)
				idseg = 2;//---FIXE
			
			if(myCommande.SEGMENT_MOBILE == 0 && myCommande.SEGMENT_FIXE == 0 && myCommande.SEGMENT_DATA == 1)
				idseg = 3;//---DATA
			
			switch(idseg)//--- 1-> MOBILE, 2-> FIXE, 3-> DATA
			{
				case 1: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Mobile'); break;
				case 2: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Fixe'); 	break;
				case 3: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Data'); 	break;
			}
			
			return moduleText;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION
		//--------------------------------------------------------------------------------------------//
		
		private function checkEtatCommande(idetatcommande:int):Boolean
		{
			var OK	:Boolean = false;//--> METTRE 'true'
			var len	:int = restrictedEtats.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(restrictedEtats[i] == idetatcommande)
					return false;
			}
			
			return OK;
		}
		
		private function enabledDisabledFreeButtonMenu():void
		{
			if(myCommande.IDLAST_ETAT != 3366)
			{
				btExpedition.enabled = true;
				btExporter.enabled 	 = true;
			}
			else
			{
				btExpedition.enabled = false;
				btExporter.enabled 	 = false;
			}
		}
		
		private function setProcedure():void//-> IL FAUDRA RECUPERER LE SEGMENT 'SessionUserObject.singletonSession.IDSEGMENT'
		{
			var children:Array = tabNavCommande.getChildren();
			var lenChild:int = children.length;
			
			for(var i:int = 0;i < lenChild;i++)
			{
				(children[i] as Object).setProcedure(myCommande);
			}			
		}
		
		private function eraseWorkFlowRestricted(values:ArrayCollection):ArrayCollection
		{
			var transpor :ArrayCollection = new ArrayCollection();
			var lenTrans :int = values.length
			var lenIDs	 :int = restrictedIDs.length;
			
			for(var i:int = 0;i < lenTrans;i++)
			{
				var id			:int = values[i].IDACTION;
				var isRestrcit	:Boolean = false;
				
				for(var j:int = 0;j < lenIDs;j++)
				{
					if(restrictedIDs[j] == id)
						isRestrcit = true;
				}
				
				if(!isRestrcit)
					transpor.addItem(values[i]);
			}
			
			return transpor;
		}
		
		private function getCommandeParametres():Boolean
		{
			var isOk	:Boolean = true;
			var child	:Object = tabNavCommande.getChildAt(0);
			
			if(child.txtCommentaires.text.length > 500)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Le_champs_commentaire_poss_de_plus_de_____car'),'Consoview',null);
				child.txtCommentaires.errorString = ResourceManager.getInstance().getString('M16', 'Le_champs_commentaire_poss_de_plus_de_____car');
				isOk = false;
			}
			else
				child.txtCommentaires.errorString = "";
			
			if(child.txtLibelle.text != myCommande.LIBELLE_COMMANDE)
				myCommande.LIBELLE_COMMANDE = child.txtLibelle.text;
			
			if(child.txtNumeroCommande.text != myCommande.NUMERO_COMMANDE)
				myCommande.NUMERO_COMMANDE = child.txtNumeroCommande.text;
			
			if(!checkChamps())
				isOk = false;
			
			if(child.txtRefRevendeurOperateur.text != myCommande.REF_OPERATEUR)
				myCommande.REF_OPERATEUR = child.txtRefRevendeurOperateur.text;
			
			if(child.dcLivaraison.selectedDate != myCommande.LIVRAISON_PREVUE_LE)
				myCommande.LIVRAISON_PREVUE_LE = child.dcLivaraison.selectedDate;
			
			if(child.txtCommentaires.text != myCommande.COMMENTAIRES)
				myCommande.COMMENTAIRES = child.txtCommentaires.text;
			
			if(child.txtTracking.text != myCommande.NUMERO_TRACKING)
				myCommande.NUMERO_TRACKING = child.txtTracking.text;
			
			if(child.txtTo.text != myCommande.LIBELLE_TO)
				myCommande.LIBELLE_TO = child.txtTo.text;
			
			if(child.cbxTransporteur.selectedItem != null)
			{
				myCommande.LIBELLE_TRANSPORTEUR = child.cbxTransporteur.selectedItem.LIBELLE_TRANSPORTEUR;
				myCommande.IDTRANSPORTEUR 		= child.cbxTransporteur.selectedItem.IDTRANSPORTEUR;
			}
			
			return isOk;
		}
		
		private function checkChamps():Boolean
		{
			var OK				:Boolean = true;
			var i				:int 	 = 0;
			var childChamps1 	:Array 	 = general.hbxChamps1.getChildren();
			var childChamps2 	:Array 	 = general.hbxChamps2.getChildren();
			
			if(general.CHAMPS1_ACTIF)
			{
				for(i = 0;i < general.champs1.length;i++)
				{
					if(general.champs1[i].SAISIE_ZONE)
					{
						if((childChamps1[i*2] as TextArea).text == "")
						{
							(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
							
							if(OK)
								OK = false;
						}
						else
						{
							(childChamps1[i*2] as TextArea).errorString = "";
							
							if(general.champs1[i].EXACT_NBR)
							{
								if((childChamps1[i*2] as TextArea).text.length != general.champs1[i].COMPTEUR_OBLIG)
								{
									(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + general.champs1[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + general.champs1[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps1[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
					else
					{
						if((childChamps1[i*2] as TextArea).text == "")
						{
							if(OK)
								OK = true;
						}
						else
						{
							(childChamps1[i*2] as TextArea).errorString = "";
							
							if(general.champs1[i].EXACT_NBR)
							{
								if((childChamps1[i*2] as TextArea).text.length != general.champs1[i].COMPTEUR_OBLIG)
								{
									(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + general.champs1[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + general.champs1[i].CONTENU_ZONE.LIBELLE_CARATERE;
									if(OK)
										OK = false;
								}
								else
									(childChamps1[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
				}
			}
			
			if(general.CHAMPS2_ACTIF)
			{
				for(i = 0;i < general.champs2.length;i++)
				{
					if(general.champs2[i].SAISIE_ZONE)
					{
						if((childChamps2[i*2] as TextArea).text == "")
						{
							(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
							
							if(OK)
								OK = false;
						}
						else
						{
							(childChamps2[i*2] as TextArea).errorString = "";
							
							if(general.champs2[i].EXACT_NBR)
							{
								if((childChamps2[i*2] as TextArea).text.length != general.champs2[i].COMPTEUR_OBLIG)
								{
									(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + general.champs2[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + general.champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps2[i*2] as TextArea).errorString = "";						
							}
							else
								OK = true;
						}
					}
					else
					{
						if((childChamps2[i*2] as TextArea).text == "")
						{
							if(OK)
								OK = true;
						}
						else
						{
							(childChamps2[i*2] as TextArea).errorString = "";
							
							if(general.champs2[i].EXACT_NBR)
							{
								if((childChamps2[i*2] as TextArea).text.length != general.champs2[i].COMPTEUR_OBLIG)
								{
									(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + general.champs2[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + general.champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps2[i*2] as TextArea).errorString = "";						
							}
							else
								OK = true;
						}
					}
				}
			}
			
			if(OK)
				attributDataToCommandeObject();
			
			return OK;
		}
		
		private function attributDataToCommandeObject():void
		{
			var i				:int 	= 0;
			var childChamps1 	:Array 	= general.hbxChamps1.getChildren();
			var childChamps2 	:Array 	= general.hbxChamps2.getChildren();
			var reference1		:String	= "";
			var reference2		:String	= "";
			
			for(i = 0;i < childChamps1.length;i++)
			{
				reference1 = reference1 + childChamps1[i].text;
			}
			
			for(i = 0;i < childChamps2.length;i++)
			{
				reference2 = reference2 + childChamps2[i].text;
			}
			
			myCommande.REF_CLIENT1 = reference1;
			myCommande.REF_CLIENT2 = reference2;
		}

	}
}
