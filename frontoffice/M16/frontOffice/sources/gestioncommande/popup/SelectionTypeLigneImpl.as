package gestioncommande.popup
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.LigneService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class SelectionTypeLigneImpl extends TitleWindow
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgTypesDisponibles		:DataGrid;
		public var chbFiltre				:CheckBox;
		public var txtIptSelectLabel		:TextInput;

		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _ligne					:LigneService;

		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var myElements				:ElementsCommande2	= new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//	
		
		public var listeTypesDisponibles	:ArrayCollection;
		
		public var libelleAbonnement		:String = ResourceManager.getInstance().getString('M16', 'inconnu');
		public var idAbonnement				:int 	= 0;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function SelectionTypeLigneImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}

//--------------------------------------------------------------------------------------------//
//					FUNCTION PROPRE A L'ETAPE 
//--------------------------------------------------------------------------------------------//
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function dgTypesDisponiblesClickHandler(me:MouseEvent):void
		{
			var len	:int = listeTypesDisponibles.length;
			var id	:int = dgTypesDisponibles.selectedItem.IDTYPE_LIGNE;
			
			for(var i:int = 0; i < len;i++)
			{
				listeTypesDisponibles[i].SELECTED = false;
				if(listeTypesDisponibles[i].IDTYPE_LIGNE == id)
					listeTypesDisponibles[i].SELECTED = true;
				
				listeTypesDisponibles.itemUpdated(listeTypesDisponibles[i]);
			}
		}
		
		protected function txtIptSelectLabelHandler(e:Event):void
		{
			if(dgTypesDisponibles.dataProvider != null)
	    	{
	    		(dgTypesDisponibles.dataProvider as ArrayCollection).filterFunction = filterFunction;
	    		(dgTypesDisponibles.dataProvider as ArrayCollection).refresh();
	    	}
		}
		
		protected function txtIptSelectLabelFocusOutHandler(e:Event):void
		{
			if(txtIptSelectLabel.text.length == 0)
			{
				txtIptSelectLabel.text = ResourceManager.getInstance().getString('M16', 'Saisissez_un_libell_');
				txtIptSelectLabel.setStyle('color','#999999');
			}
		}
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			if(dgTypesDisponibles.selectedItem != null)
				_ligne.associerTypeLigneAbo(dgTypesDisponibles.selectedItem.IDTYPE_LIGNE , idAbonnement);
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_type_de_ligne__'), 'Consoview');
		}
		
		protected function btnAnnulerClickHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function chbFiltreClickHandler(me:MouseEvent):void
		{
			var abo:int = 0;
			
			if(chbFiltre.selected)
				abo = idAbonnement;
			
			_ligne.getTypeLigneAbo(abo);
			listeTypesDisponibles.removeAll();
		}

	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//	

		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - LISTENER IHM
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			_ligne	= new LigneService();
			_ligne.addEventListener(CommandeEvent.ASSOCIATED_LIGNE, associatedLigneHandler);
			_ligne.addEventListener(CommandeEvent.GET_TYPE_LIGNE, typeLigneHandler);
			
			_ligne.getTypeLigneAbo(0);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - LISTENER
		//--------------------------------------------------------------------------------------------//
		
		private function typeLigneHandler(cmde:CommandeEvent):void
		{
			listeTypesDisponibles = _ligne.listeTypesDisponibles;
			
			selectLastTypeLigne();
		}
		
		private function associatedLigneHandler(cmde:CommandeEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Type_de_ligne_associ___'), this);
			
			myElements.IDTYPELIGNE 	= dgTypesDisponibles.selectedItem.IDTYPE_LIGNE;
			myElements.TYPELIGNE 	= dgTypesDisponibles.selectedItem.LIBELLE_TYPE_LIGNE;
			
			dispatchEvent(new Event("GET_TYPELIGNE")); 
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		private function selectLastTypeLigne():void
		{
			if(myElements.IDTYPELIGNE > 0)
			{
				var len:int = listeTypesDisponibles.length;
				
				for(var i:int = 0;i < len; i++)
				{
					if(listeTypesDisponibles[i].IDTYPE_LIGNE == myElements.IDTYPELIGNE)
					{
						listeTypesDisponibles[i].SELECTED = true;
						return;
					}
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//
				
		private function filterFunction(item:Object):Boolean
	    {
		    var rfilter:Boolean = true;		    

		    rfilter = rfilter && (item.LIBELLE_TYPE_LIGNE != null && item.LIBELLE_TYPE_LIGNE.toLocaleLowerCase().search(txtIptSelectLabel.text.toLocaleLowerCase()) != -1)
		   
		    return rfilter;
		}

	}
}