package gestioncommande.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	
	import gestioncommande.entity.Critere;
	
	import session.SessionUserObject;
	
	public class PopUpCriteresDeRechercheImpl extends TitleWindow
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMs
			//--------------------------------------------------------------------------------------------//
		
		[Bindable]public var tbxFiltre				:TextInput;
		
		[Bindable]public var rdbtnNone				:RadioButton;
		[Bindable]public var rdbtnLigne				:RadioButton;
		[Bindable]public var rdbtnIMEI				:RadioButton;
		[Bindable]public var rdbtnSIM				:RadioButton;
		[Bindable]public var rdbtnRef				:RadioButton;
		[Bindable]public var rdbtnRef2				:RadioButton;
		[Bindable]public var rdbtnCollaborateur		:RadioButton;
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
		
		private var _currentCritere					:Critere;
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
					
			//--------------------------------------------------------------------------------------------//
			//					AUTRES
			//--------------------------------------------------------------------------------------------//
		
		private var _mois_historique:int;
		static public var ANNUL_CRITERE				:String = 'ANNUL_CRITERE';
		static public var VALID_CRITERE				:String = 'VALID_CRITERE';
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function PopUpCriteresDeRechercheImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC METHODES
		//--------------------------------------------------------------------------------------------//
		
		public function setInfosData(currrentCritere:Critere):void
		{
			_currentCritere = currrentCritere;
		}
		
		public function getInfosData():Critere
		{
			return _currentCritere;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PROTECTED METHODES
		//--------------------------------------------------------------------------------------------//
		
		protected function imgResetFilterClickHandler(me:MouseEvent):void
		{
			_currentCritere = new Critere();
			
			setInfos();
		}
		
		protected function rbtnClikHandler(me:MouseEvent):void
		{
			var rdbtnId		:String = (me.currentTarget as RadioButton).id;
			var rdbtnLabel	:String = (me.currentTarget as RadioButton).label;
			
			switch(rdbtnId)
			{
				case 'rdbtnNone' 			: setCritere(rdbtnLabel, 0); 			break;
				case 'rdbtnLigne' 			: setCritere(rdbtnLabel, 1);			break;
				case 'rdbtnIMEI' 			: setCritere(rdbtnLabel, 2); 			break;
				case 'rdbtnSIM'				: setCritere(rdbtnLabel, 3); 			break;
				case 'rdbtnCollaborateur' 	: setCritere(rdbtnLabel, 4); 			break;
				case 'rdbtnRef' 			: setCritere(rdbtnLabel, 5); 			break;
				case 'rdbtnRef2' 			: setCritere(rdbtnLabel, 6); 			break;
			}
		}
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			if(_currentCritere != null)
			{
				if(rdbtnNone.selected)
					tbxFiltre.text = '';
				
				_currentCritere.TXTFILTER = tbxFiltre.text;
			}
			else
			{
				setInfos();
			}
			SessionUserObject
			dispatchEvent(new Event(VALID_CRITERE, false));
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			closePopUp(me);
		}
		
		protected function closePopUp(e:Event):void
		{
			dispatchEvent(new Event(ANNUL_CRITERE, false));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODES
		//--------------------------------------------------------------------------------------------//
		
		private function setInfos():void
		{
			if(_currentCritere != null)
			{
				switch(_currentCritere.IDCRITERE)
				{
					case 0 : rdbtnNone.selected = true; 			break;
					case 1 : rdbtnLigne.selected = true; 			break;
					case 2 : rdbtnIMEI.selected = true; 			break;
					case 3 : rdbtnSIM.selected = true; 				break;
					case 4 : rdbtnCollaborateur.selected = true; 	break;
					case 5 : rdbtnRef.selected = true; 				break;
					case 6 : rdbtnRef2.selected = true; 			break;
					default : rdbtnNone.selected = true;
				}
				
				tbxFiltre.text = _currentCritere.TXTFILTER;
			}
			else
			{
				rdbtnNone.selected = true;
				
				tbxFiltre.text = '';
				
				_currentCritere = new Critere();
			}
		}
		
		private function setCritere(critere:String, idCritere:int):void
		{
			if(rdbtnNone.selected)
				tbxFiltre.text = '';
			
			_currentCritere 			= new Critere();
			_currentCritere.IDCRITERE 	= idCritere;
			_currentCritere.CRITERE 	= critere;
			_currentCritere.TXTFILTER 	= tbxFiltre.text;
		}
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			setInfos();
		}
		
		[Bindable]
		public function get mois_historique():int
		{
			return _mois_historique;
		}
		
		public function set mois_historique(value:int):void
		{
			_mois_historique = value;
		}
			
	}
}