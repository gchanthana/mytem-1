package gestioncommande.popup
{
	import gestioncommande.events.PopUpTargetDatagridEvent;
	import gestioncommande.services.Rules;
	import gestioncommande.entity.Wizzard;
	
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	import gestioncommande.entity.Collaborateur;
	import composants.util.employes.EmployesUtils;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestioncommande.popup.PopUpTargetDatagridIHM;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.events.ValidationResultEvent;
	import mx.managers.PopUpManager;
	import mx.managers.SystemManager;
	import mx.resources.ResourceManager;
	
	[Bindable]
	public class InfosPersoCollaborateur extends TitleWindow
	{
		public var ti_prenom:TextInput;
		public var ti_nom:TextInput;
		public var ti_fonction:TextInput;
		public var ti_email:TextInput;
		public var ti_id:TextInput;
		public var ti_code_interne:TextInput;
		public var ti_matricule:TextInput;
		public var ti_position:TextInput;
		public var ta_commentaires:TextArea;

		public var IDPOOL_GESTIONNAIRE:int = 0;
		private var idCibleOrga:Number;
		
		public var df_in_society:CvDateChooser;
		public var df_out_society:CvDateChooser;
		
		public var cb_civilite:ComboBox;
		public var cb_statut:ComboBox;
		public var cb_publication:ComboBox;
		public var cb_organisation:ComboBox;
		public var cb_niveau:ComboBox;
		
		public var listNivo				:ArrayCollection = new ArrayCollection();
		
		public var rules				:Rules = new Rules();
		
		private var action				:String;
		//liste des infos personnelles
		public var ip_collaborateur		:InfosPersoCollaborateurIHM;
		
		private var newCollab		:Collaborateur = new Collaborateur();
		public var employeUtils		:EmployesUtils = new EmployesUtils();
		
		public static const DISPLAY_POPUP_ORGA:String = "displayPopupOrga";
		public static const DISPLAY_CIBLE_ORGA:String = "displayCibleOrga";
		
		public function InfosPersoCollaborateur()
		{
			super();
			rules.addEventListener("IUOrgaException",IUOrgaExceptionHandler);
			addNiveauObject();
		}

		private function addNiveauObject():void
		{
			listNivo = new ArrayCollection();
			var obj:Object = new Object();
			for(var i:int = 0;i < 5;i++)
			{
				obj = new Object();
				obj.NIVEAU = i+1;
				listNivo.addItem(obj);
			}
		}
		public function clickModifierOrgaHandler(me:MouseEvent):void 
		{
			displayPopupOrgaHandler(me);
		}
		
		public function selectItemCbOrgaHandler(le:ListEvent):void 
		{
			///RECUPERE LA SOURCE
		}
		
		[Inspectable(defaultValue="Monsieur",enumeration="Monsieur,Madame",type="String")]
		public function set civilite(value:String):void {
			cb_civilite.text = value;
		}
		
		public function set prenom(value:String):void {
			ti_prenom.text = value;
		}
		
		public function get prenom():String {
			return ti_prenom.text;
		}
		
		public function set nom(value:String):void {
			ti_nom.text = value;
		}
		
		public function get nom():String {
			return ti_nom.text;
		}
		
		public function set fonction(value:String):void {
			ti_fonction.text = value;
		}
		
		public function get fonction():String {
			return ti_fonction.text;
		}
		
		public function set email(value:String):void {
			ti_email.text = value;
		}
		
		public function get email():String {
			return ti_email.text;
		}
		
		public function set codeInterne(value:String):void {
			ti_code_interne.text = value;
		}
		
		public function get codeInterne():String {
			return ti_code_interne.text;
		}
		
		public function set matricule(value:String):void {
			ti_matricule.text = value;
		}
		
		public function get matricule():String {
			return ti_matricule.text;
		}
		
		public function set niveau(value:String):void {
			cb_niveau.dataProvider = value;
		}
		
		public function set identifiant(value:String):void {
			ti_id.text = value;
		}
		
		public function get identifiant():String {
			return ti_id.text;
		}
		
		public function set dateEntree(value:Date):void {
			df_in_society.selectedDate = value;
		}
		
		public function get dateEntree():Date {
			return df_in_society.selectedDate;
		}
		
		public function set dateSortie(value:Date):void {
			df_out_society.selectedDate = value;
		}
		
		public function set listOrgaCliente(value:ArrayCollection):void {
			cb_organisation.dataProvider = value;
		}
		
		public function set position(value:String):void {
			ti_position.text = value;
		}
		
		public function get position():String {
			return ti_position.text;
		}
		
		//[Inspectable(defaultValue="0",enumeration="0,1",type="Number")]
		public function set statut(value:String):void {
			cb_statut.text = value;
		}
		
		[Inspectable(defaultValue="Oui",enumeration="Oui,Non",type="String")]
		public function set publication(value:String):void {
			cb_publication.text = value;
		}
		
		public function set commentaire(value:String):void {
			ta_commentaires.text = value;
		}
		
		public function get commentaire():String {
			return ta_commentaires.text;
		}
		
		public function btValidActionHandler(evt:MouseEvent):void 
		{
			Alert.show(ResourceManager.getInstance().getString('M16', 'Action__') + action, ResourceManager.getInstance().getString('M16', 'Action'));
		}
		
		public function set collaborateur(value:Collaborateur):void
		{
			employeUtils.selectedEmploye = value;
		}

		public function get collaborateur():Collaborateur
		{
			return employeUtils.selectedEmploye;
		}
		
		protected function closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function creationCompleteHandler(fe:FlexEvent):void
		{
			rules.ListOraganisationCliente();
		}
		
		private function IUOrgaExceptionHandler(e:Event):void
		{
			listOrgaCliente = rules.ListOrgaCliente;
		}
		
		protected function btnEnregClickHandler(me:MouseEvent):void
		{
			var emailValidation		:ValidationResultEvent = ip_collaborateur.validation_email.validate();
			var matriculeValidation	:ValidationResultEvent = ip_collaborateur.validation_matricule.validate();
		
			if(emailValidation.type == ValidationResultEvent.INVALID) 
			{
				Alert.show(ResourceManager.getInstance().getString('M16', 'Erreur_dans_la_saisie_du_champ_email'), ResourceManager.getInstance().getString('M16', 'Attention__'));
			}
			else if(matriculeValidation.type == ValidationResultEvent.INVALID) 
				 {
					Alert.show(ResourceManager.getInstance().getString('M16', 'Vous_devez_saisir_un_num_ro_de_matricule'), ResourceManager.getInstance().getString('M16', 'Attention__'));
				 }
				 else 
				 {
					addEmploye();
					employeUtils.createEmploye(newCollab,IDPOOL_GESTIONNAIRE);
					employeUtils.addEventListener(EmployesUtils.EMPLOYE_CREATED, updateDone);
				 }
		}
		
		private function addEmploye():void
		{
			newCollab.CIVILITE = (ip_collaborateur.cb_civilite.selectedIndex != -1)? ip_collaborateur.cb_civilite.selectedItem.data:'';
			newCollab.PRENOM = ip_collaborateur.prenom;
			newCollab.NOM = ip_collaborateur.nom;
			newCollab.EMAIL = ip_collaborateur.email;
			newCollab.NIVEAU = Number(ip_collaborateur.cb_niveau.selectedItem.NIVEAU);
			newCollab.CODE_INTERNE = ip_collaborateur.codeInterne;
			newCollab.MATRICULE = ip_collaborateur.matricule;
			newCollab.IDEMPLOYE = Number(ip_collaborateur.identifiant);
			newCollab.FONCTION_EMPLOYE = ip_collaborateur.fonction;
			newCollab.COMMENTAIRE = ip_collaborateur.commentaire;
			newCollab.DATE_ENTREE = ip_collaborateur.df_in_society.selectedDate;
			newCollab.DATE_SORTIE = ip_collaborateur.df_out_society.selectedDate;
			newCollab.INOUT = (ip_collaborateur.cb_statut.selectedItem != null)? ip_collaborateur.cb_statut.selectedItem.data: 0;
			newCollab.STATUS_EMPLOYE = (ip_collaborateur.cb_statut.selectedIndex != -1)? String(ip_collaborateur.cb_statut.selectedItem.label):'';
			newCollab.INOUT = (ip_collaborateur.cb_statut.selectedIndex != -1)? ip_collaborateur.cb_statut.selectedItem.data:1;
			newCollab.BOOL_PUBLICATION = (ip_collaborateur.cb_publication != null && ip_collaborateur.cb_publication.selectedIndex != -1)? Number(ip_collaborateur.cb_publication.selectedItem.data):-1;
		}
		
		protected function btnAnnulClickHandler(me:MouseEvent):void
		{
			closeHandler(me);
		}
		
		private function updateDone(e:Event):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Collaborateur_enregistr__'), SystemManager.getSWFRoot(this));
			
			dispatchEvent(new Event("REFRESH_EMPLOYES", true));
			
			PopUpManager.removePopUp(this);
		}
		
		private function displayPopupOrgaHandler(evt:Event):void 
		{
			var popupOrga:PopUpTargetDatagridIHM = new PopUpTargetDatagridIHM();
			var wizzard:Wizzard = new Wizzard(this);
			
			popupOrga._wizzard = wizzard;
			popupOrga._strgToSearch = "";
			popupOrga.resultClient = ip_collaborateur.cb_organisation.selectedItem;
			PopUpManager.addPopUp(popupOrga, this, true);
			PopUpManager.centerPopUp(popupOrga);
			popupOrga.addEventListener(PopUpTargetDatagridEvent.ITEM_SELECTED, displayLibellePositionHandler);
		}
		
		private function displayLibellePositionHandler(evt:PopUpTargetDatagridEvent):void 
		{
			ip_collaborateur.position = evt.labelSelected;
			idCibleOrga = evt.idCibleSelected;
		}
		
	}
}
