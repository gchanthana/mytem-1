package gestioncommande.popup
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.entity.RessourcesElements;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.CommandeArticles;
	import gestioncommande.services.Formator;
	import gestioncommande.services.ModeleService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class PopUpViewConfigurationElementsImpl extends TitleWindow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgRecapCommande		:DataGrid;
		
		public var lblTotal				:Label;
				
		public var btnValider			:Button;
		public var btnAbo				:Button;
		public var btnOpt				:Button;

		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _popUpAbo		:PopUpChoiceSubscriptionIHM;
		
		private var _popUpOpt		:PopUpChoiceOptionsIHM;
			
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _modele				:ModeleService = new ModeleService();
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmd				:Commande;
		
		private var _myElements			:ElementsCommande2 = new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listArticles			:Object = new Object();

		public var libelle				:String = '';
		
		public var idModele				:int = 0;
		public var idGestionnaire		:int = 0;
		public var idOperateur			:int = 0;
		public var idProfil				:int = 0;
		public var idPool				:int = 0;
			   	
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//

		public function PopUpViewConfigurationElementsImpl()
		{			
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
	
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			updateModele();
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function closeHandler(ce:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		//OUVRE LA POPUP PERMETTANT DE SELECTIONNER LES ABONNEMENTS
		protected function btnChoisirAboClickHandler(me:MouseEvent):void
		{
			_popUpAbo 			 = new PopUpChoiceSubscriptionIHM();
			_popUpAbo.cmd 		 = createCommandeObject();
			_popUpAbo.myElements = _myElements;
			_popUpAbo.addEventListener("POPUP_ABONNEMENTS_CLOSED_AND_VALIDATE", getSubscriptionHandler); 

			PopUpManager.addPopUp(_popUpAbo, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpAbo);
		}
		
		//OUVRE LA POPUP PERMETTANT DE SELECTIONNER LES OPTIONS
		protected function btnChoisirOptClickHandler(me:MouseEvent):void
		{
			_popUpOpt 			 = new PopUpChoiceOptionsIHM();
			_popUpOpt.cmd 		 = createCommandeObject();
			_popUpOpt.myElements = _myElements;
			_popUpOpt.addEventListener("POPUP_OPTIONS_CLOSED_AND_VALIDATE", getOptionsHandler); 
			
			PopUpManager.addPopUp(_popUpOpt, this.parent, true);
			PopUpManager.centerPopUp(_popUpOpt);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener("ERASE_ARTICLE", eraseThisArticleHandler);
			
			getThisModele();
		}
		
		private function collectionChangeHandler(ce:CollectionEvent):void
		{
			btnValider.enabled = true;
		}
		
		private function eraseThisArticleHandler(e:Event):void
		{
			if(dgRecapCommande.selectedItem != null)
			{
				var obj	:Object = dgRecapCommande.selectedItem;
				var idx	:int = dgRecapCommande.selectedIndex;
				
				if(obj.BOOLACCES < 0)
				{
					if(obj.IDEQRE != 2191)
						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Vous_ne_pouvez_pas_supprimmer_cette_arti'), 'Consoview', null);
					else
					{
						(dgRecapCommande.dataProvider as ArrayCollection).removeItemAt(idx);
						eraseThisAccessoire(obj.IDFOUR);
					}
				}
				else
				{
					if(obj.BOOLACCES > 0)
						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Vous_ne_pouvez_pas_supprimmer_cette_arti'), 'Consoview', null);
					else
					{
						(dgRecapCommande.dataProvider as ArrayCollection).removeItemAt(idx);
						eraseThisOption(obj.IDFOUR);
					}
				}
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENERS POPUPs
		//--------------------------------------------------------------------------------------------//
		
		//SUPPRESSION DE L'ANCIENNE SUSCRIPTION ET RAJOUT DE LA NOUVELLE
		private function getSubscriptionHandler(e:Event):void
		{
			listArticles = new Object();
			listArticles = formatToArrayCollection();
			
			(dgRecapCommande.dataProvider as ArrayCollection).refresh();
		}
		
		private function getOptionsHandler(e:Event):void
		{
			listArticles = new Object();
			listArticles = formatToArrayCollection();
			
			(dgRecapCommande.dataProvider as ArrayCollection).refresh();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getThisModele():void
		{
			_modele.getModeleCommande(idModele);
			_modele.addEventListener(CommandeEvent.MODELE_ADD, thisModeleHandler);
		}
		
		private function updateModele():void
		{
			_modele.majModele(idModele, libelle, idPool, createXML());
			_modele.addEventListener(CommandeEvent.MODELE_UPDATED, 	updateModeleHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function thisModeleHandler(cmde:CommandeEvent):void
		{
			var articles:XML = _modele.listeModele.article[0];
			
			if(articles == null)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Erreur_lors_du_chargement_du_mod_le'), 'Consoview', null);
				return;
			}
			
			_myElements = new ElementsCommande2();
			
			getEquipements(articles.equipements[0], _myElements);
			getRessources(articles.ressources[0], _myElements);
			getEngagement(articles, _myElements);
			
			listArticles = new Object();
			listArticles = formatToArrayCollection();
			
			if(SessionUserObject.singletonSession.IDSEGMENT > 1)
			{
				if(_myElements.TERMINAUX.length > 0 || _myElements.ACCESSOIRES.length > 0)
				{
					btnAbo.enabled = false;
					btnOpt.enabled = false;
				}
			}
			
			(dgRecapCommande.dataProvider as ArrayCollection).refresh();
		}
		
		private function updateModeleHandler(cmde:CommandeEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Mod_le_mis___jour'), this);
			PopUpManager.removePopUp(this);
		}

		//--------------------------------------------------------------------------------------------//
		//					FUCNTIONS
		//--------------------------------------------------------------------------------------------//
		
		private function createCommandeObject():Commande
		{
			if(idGestionnaire == 0)
				idGestionnaire = CvAccessManager.getSession().USER.CLIENTACCESSID;
			
			var _cmd:Commande 		 = new Commande();
				_cmd.IDGESTIONNAIRE		 = idGestionnaire;
				_cmd.IDOPERATEUR		 = idOperateur;
				_cmd.IDPROFIL_EQUIPEMENT = idProfil;
				
			return _cmd;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTIONS - SUPPRESSION DES ELEMENTS 
		//--------------------------------------------------------------------------------------------//
		
		private function eraseThisAccessoire(idAccessoire:int):void
		{
			var len:int = _myElements.ACCESSOIRES.length;
			
			for(var i:int = 0; i < len;i++)
			{
				if((_myElements.ACCESSOIRES[i] as EquipementsElements).IDFOURNISSEUR == idAccessoire)
				{
					_myElements.ACCESSOIRES.removeItemAt(i);
					return;
				}
				
			}
		}
		
		private function eraseThisOption(idOption:int):void
		{
			var len:int = _myElements.OPTIONS.length;
			
			for(var i:int = 0; i < len;i++)
			{
				if((_myElements.OPTIONS[i] as RessourcesElements).IDCATALOGUE == idOption)
				{
					_myElements.OPTIONS.removeItemAt(i);
					return;
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTIONS - FORMAT XML
		//--------------------------------------------------------------------------------------------//

		private function getEquipements(articlesEq:XML, valuesElements:ElementsCommande2):void
		{
			var lenXML:int = articlesEq[0].equipement.length();
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML	:XML = articlesEq[0].equipement[i];
				var price	:String = myXML[0].prix.toString().replace(",",".");
				
				var eqElts:EquipementsElements 	= new EquipementsElements();
				eqElts.LIBELLE	 			= String(myXML[0].libelle);
				eqElts.REF_REVENDEUR		= String(myXML[0].reference);
				eqElts.EQUIPEMENT 			= String(myXML[0].type_equipement);
				eqElts.IDEQUIPEMENT 		= int(myXML[0].idtype_equipement);
				eqElts.IDFOURNISSEUR		= int(myXML[0].idequipementfournis);
				eqElts.IDCLIENT				= int(myXML[0].idequipementclient);
				eqElts.PRIX					= Number(price);
				
				if(eqElts.IDEQUIPEMENT == 2191)
					valuesElements.ACCESSOIRES.addItem(eqElts);//---> ACCESSOIRES
				else
					valuesElements.TERMINAUX.addItem(eqElts);//---> TERMINAUX / CARTE SIM
			}
		}
		
		private function getRessources(articlesRe:XML, valuesElements:ElementsCommande2):void
		{
			var lenXML:int = articlesRe[0].ressource.length();
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML:XML = articlesRe[0].ressource[i];
				var pricee	:String = myXML[0].prix.toString().replace(",",".");
				
				var reElts:RessourcesElements	= new RessourcesElements();	
				reElts.LIBELLETHEME 		= String(myXML[0].theme);
				reElts.LIBELLE 				= String(myXML[0].libelle);
				reElts.REFERENCE_PRODUIT 	= String(myXML[0].reference);
				reElts.IDCATALOGUE 			= int(myXML[0].idproduit_catalogue);
				reElts.IDPRODUIT 			= int(myXML[0].idThemeProduit);
				reElts.BOOLACCES 			= int(myXML[0].bool_acces);
				reElts.PRIX_UNIT			= Number(pricee);
				reElts.PRIX					= Formator.formatTotalWithSymbole(reElts.PRIX_UNIT);
				reElts.PRIX_STRG			= Formator.formatTotalWithSymbole(reElts.PRIX_UNIT);
				
				//---> 'BOOLACCES' = 0 -> OPTION, 'BOOLACCES' = 1 -> ABONNEMENT
				if(reElts.BOOLACCES > 0)
					valuesElements.ABONNEMENTS.addItem(reElts);
				else
					valuesElements.OPTIONS.addItem(reElts);
			}
		}
		
		private function getEngagement(engagement:XML, valuesElements:ElementsCommande2):void
		{
			var commitment	:Engagement = new Engagement();
			var duree		:int = int(engagement.engagement_res[0]);
			var idType		:int = int(engagement.idtype_ligne[0]);
			var eligibilite	:int = int(engagement.duree_elligibilite[0]);
			var fpc			:String = String(engagement.fpc[0]);
			
			if(eligibilite > 0)
				commitment.ELEGIBILITE = eligibilite;
			
			if(fpc != "")
			{
				commitment.DUREE = ResourceManager.getInstance().getString('M16', '24_mois');
				commitment.VALUE = '24';
				commitment.CODE  = 'EVQMFCP';
				commitment.FPC   = String(engagement.fpc[0]);
			}
			else
			{
				switch(duree)
				{
					case 12: commitment.CODE = 'EDM';  commitment.DUREE = ResourceManager.getInstance().getString('M16', '12_mois'); commitment.VALUE = '12'; break;
					case 24: commitment.CODE = 'EVQM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '24_mois'); commitment.VALUE = '24'; break;
					case 36: commitment.CODE = 'ETSM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '36_mois'); commitment.VALUE = '36'; break;
					case 48: commitment.CODE = 'EQHM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '48_mois'); commitment.VALUE = '48'; break;
				}
			}
			
			if(idType > 0)
			{
				valuesElements.IDTYPELIGNE  = idType;
				valuesElements.TYPELIGNE 	= String(engagement.type_ligne[0]);
			}
			else
			{
				valuesElements.IDTYPELIGNE  = -1;
				valuesElements.TYPELIGNE 	= '';
			}
			
			valuesElements.ENGAGEMENT 	= commitment;	
		}
		
		private function formatToArrayCollection():Object
		{
			var articles:ArrayCollection = new ArrayCollection();
			var obj		:Object = new Object();	
			var price	:Number = 0;
			var len		:int = 0
			var i		:int = 0;
			
			len = _myElements.TERMINAUX.length;
			
			for(i = 0; i < len;i++)
			{
				obj	= new Object();	
				obj = formatEquipements(_myElements.TERMINAUX[i] as EquipementsElements);
				
				articles.addItem(obj);
				
				price = price + obj.PRIX_T;
			}	
			
			len = _myElements.ACCESSOIRES.length;
			
			for(i = 0; i < len;i++)
			{
				obj	= new Object();	
				obj = formatEquipements(_myElements.ACCESSOIRES[i] as EquipementsElements);
				
				articles.addItem(obj);
				
				price = price + obj.PRIX_T;
			}
			
			len = _myElements.ABONNEMENTS.length;
			
			for(i = 0; i < len;i++)
			{
				obj	= new Object();	
				obj = formatRessources(_myElements.ABONNEMENTS[i] as RessourcesElements);
				
				articles.addItem(obj);
				
				price = price + obj.PRIX_T;
			}
			
			len = _myElements.OPTIONS.length;
			
			for(i = 0; i < len;i++)
			{
				obj	= new Object();	
				obj = formatRessources(_myElements.OPTIONS[i] as RessourcesElements);
				
				articles.addItem(obj);
				
				price = price + obj.PRIX_T;
			}
			
			var dataToGrid:Object = new Object();
				dataToGrid.ITEMS = articles;
				dataToGrid.TOTAL = Formator.formatTotalWithSymbole(price);
				dataToGrid.ENGAG = _myElements.ENGAGEMENT.DUREE;
				dataToGrid.TYPEL = _myElements.TYPELIGNE;
			
			dataToGrid.ITEMS.addEventListener(CollectionEvent.COLLECTION_CHANGE, collectionChangeHandler);
			
			return dataToGrid;
		}

		private function formatEquipements(eltsEq:EquipementsElements):Object
		{
			var eq:Object = new Object();	
				eq.TYPE 	 = eltsEq.EQUIPEMENT;
				eq.LIBELLE 	 = eltsEq.LIBELLE;
				eq.REF_DISTRIBUTEUR = eltsEq.REF_REVENDEUR;
				eq.BOOLACCES = -1;
				eq.IDFOUR	 = eltsEq.IDFOURNISSEUR;
				eq.IDEQRE 	 = eltsEq.IDEQUIPEMENT
				eq.PRIX_S 	 = Formator.formatTotalWithSymbole(eltsEq.PRIX);
				eq.PRIX_T	 = eltsEq.PRIX;
			
			return eq;
		}
		
		private function formatRessources(eltsRe:RessourcesElements):Object
		{
			var re:Object = new Object();	
				re.TYPE 	 = eltsRe.LIBELLETHEME;
				re.LIBELLE 	 = eltsRe.LIBELLE;
				re.REF_DISTRIBUTEUR = eltsRe.REFERENCE_PRODUIT;
				re.BOOLACCES = eltsRe.BOOLACCES;
				re.IDFOUR	 = eltsRe.IDCATALOGUE;
				re.IDEQRE 	 = eltsRe.IDPRODUIT;
				re.PRIX_S 	 = eltsRe.PRIX_STRG;
				re.PRIX_T	 = eltsRe.PRIX_UNIT;
			
			return re;
		}
		
		private function createXML():XML
		{
			var cart		:CommandeArticles = new CommandeArticles();
			var conf		:ConfigurationElements = new ConfigurationElements();
			var articles	:XML = <articles></articles>;
			var article		:XML = <article></article>;
			var xmlEq		:XML = cart.addEquipements(_myElements);
			var xmlRs		:XML = cart.addRessources(_myElements);
			var engagement	:Engagement = _myElements.ENGAGEMENT;
			
			article.appendChild(xmlEq);
			article.appendChild(xmlRs);
			
			conf.ID_TYPELIGNE = _myElements.IDTYPELIGNE;
			conf.TYPELIGNE 	  = _myElements.TYPELIGNE;
			
			article = cart.addConfiguration(article, conf, engagement, 1);
			
			articles.appendChild(article);
			
			return articles;
		}

	}
}
