package gestioncommande.popup
{
	import gestioncommande.events.PopUpTargetDatagridEvent;
	import gestioncommande.entity.Wizzard;
	import gestioncommande.services.destinataireOrga;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import gestioncommande.entity.OrgaVO;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	[Bindable]
	public class PopUpTargetDatagridImpl extends TitleWindow
	{

//VARIABLES GLOBALES----
		
		public var _wizzard:Wizzard;
		
		public var searchabletreeihm2:destinataireOrga;
		public var dgListTarget:DataGrid;
		public var lblNbrNode:Label;
		public var lblNbrNode2:Label;
		public var btChoisir:Button;
		public var btCancel:Button;
		public var txtFiltre:TextInput;
		
		public var _clientresult:Object;
		public var _nbNoeudTarget:int;
		public var _nbrOfStage:int = 0;
		public var _strgToSearch:String;

		private var _resultClient:Object;
		private var _libelleTarget:ArrayCollection;
		private var _numberOfResultFound:int;
		private var ownOrNot:Boolean = false;	
		private var dataConcat:ArrayCollection = new ArrayCollection();
		
		//FIN VARIABLES GLOBALES----
		
		//PROPRIETEES PUBLICS----
		
		//Récupère ce qui a été sélectionné en temps que client dans le viewstack 'SelectTypeIHM'
		public function set resultClient(value:Object):void
		{ _resultClient = value; }
		
		public function get resultClient():Object
		{ return _resultClient; }
		
		//Libelle de la cible
		public function set libelleTarget(value:ArrayCollection):void
		{ _libelleTarget = value; }
		
		public function get libelleTarget():ArrayCollection
		{ return _libelleTarget; }
		
		//Nombre de résultat trouvé
		public function set numberOfResultFound(value:int):void
		{ _numberOfResultFound = value; }
		
		public function get numberOfResultFound():int
		{ return _numberOfResultFound; }
		
		//FIN PROPRIETEES PUBLICS----
		
		//METHODES PUBLICS----
		
		//Constructeur
		public function PopUpTargetDatagridImpl()
		{ addEventListener("finishToRefresh",search); }

//FIN METHODES PUBLICS----

//METHODES PROTECTED----
		
		protected function _closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		//Appel à la fonction : de récupération de l'XML pour le traitement de conversion
		protected function initTargetFind(event:FlexEvent):void
		{ chargeDataToDatagrid(); }
		
		protected function closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		//Lors du click sur le bouton 'choisir' vérifie que c'est bien une feuille et dipatch 
		//un event si c'est bon ou fait apparaitre un message si cela ne l'ai pas 
		protected function btChoisirClickHandler(event:Event):void
		{ 
			if(ownOrNot)
			{
				dispatchEvent(new PopUpTargetDatagridEvent(PopUpTargetDatagridEvent.ITEM_SELECTED, false,_wizzard.myRegle.LIBELLE_CIBLE,_wizzard.myRegle.IDCIBLE));
				PopUpManager.removePopUp(this); 
			}
			else
			{ 
				//Alert.show("Veuillez séléctionner une feuille",'Consoview'); 
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_s_l_ctionner_une_feuille_'),'Consoview');
			}
		}
		
		//Dipatch un event si l'on click sur le bouton 'annuler'
		protected function btCancelClickHandler(event:Event):void
		{ 
			dispatchEvent(new Event("AnnulChoiceTarget",true));
			PopUpManager.removePopUp(this); 
		}
		
		//Filtre (recherche dans le grid)
		protected function filtreChangeHandler(event:Event):void
		{
			var arrayCollectionSearch:ArrayCollection = new ArrayCollection();
			
			if(txtFiltre.text == "")
			{
				dgListTarget.dataProvider = dataConcat;
				lblNbrNode.text = dataConcat.length.toString() + ResourceManager.getInstance().getString('M16', 'noeuds');
			}
			else
			{
				for(var i:int = 0;i<dataConcat.length;i++)
				{
					if(dataConcat[i].LABEL.toString().toLowerCase().indexOf(txtFiltre.text.toLowerCase()) != -1)
					{ arrayCollectionSearch.addItem(dataConcat[i]); }
				}
				
				if(arrayCollectionSearch.length == 0)
				{ dgListTarget.dataProvider = null; }
				else
				{ dgListTarget.dataProvider = arrayCollectionSearch; }
				
				lblNbrNode.text = arrayCollectionSearch.length.toString() + ResourceManager.getInstance().getString('M16', 'noeuds');
			}
		}
		
		//Remet tous a 0
		protected function searchClickHandler(event:Event):void
		{
			if(libelleTarget != null)
			{
				txtFiltre.text = "";
				dgListTarget.dataProvider = dataConcat;
				lblNbrNode.text = dataConcat.length.toString() + ResourceManager.getInstance().getString('M16', 'noeuds');
			}
		}
		
		protected function datachangeHandler(event:Event):void
		{ }
		
		//Affecte les données sélectionnée lors d'un click sur un noeuds ou une feuille
		protected function dgClickHandler(event:Event):void
		{
			if(dgListTarget.selectedItem != null)
			{	
				if(dgListTarget.selectedItem.LABEL == ResourceManager.getInstance().getString('M16', 'Non_r_pertori_e'))
				{
					btChoisir.enabled = false;
					ownOrNot = false;
				}
				else
				{
					btChoisir.enabled = true;
					//ownOrNot = searchInXML(dgListTarget.selectedItem);//A SUPPRIMER!!!?????
					ownOrNot = true;
					_wizzard.myRegle.IDCIBLE = dgListTarget.selectedItem.VALUE;
					_wizzard.myRegle.LIBELLE_CIBLE = dgListTarget.selectedItem.LABEL;
				}
			}
		}
		
		//METHODES PROTECTED----
		
		//METHODES PRIVATE----
		
		//Récupération de l'XML pour le traitement de conversion
		private function chargeDataToDatagrid():void
		{
			if(resultClient!= null)
			{
				searchabletreeihm2.clearSearch();
				
				if(resultClient as OrgaVO)
					searchabletreeihm2._selectedOrga = resultClient.idOrga;
				else
					searchabletreeihm2._selectedOrga = resultClient.IDGROUPE_CLIENT;

				searchabletreeihm2.refreshTree();
			}
		}
		
		//Cherche soit tout ou un cas particulier sélectionnée dans le viewstack 'SelectTargetIHM'
		private function search(event:Event):void
		{
			if( _strgToSearch == "")
			{ searchAll(); }
			else
			{ searchData(); }
			lblNbrNode.text = dataConcat.length.toString() + ResourceManager.getInstance().getString('M16', 'noeuds');
		}
		
		//Regarde si c'est une feuille ou non
		private function thisIsASheet(XMLToCheck:XML):Boolean
		{
			var bool:Boolean = false;
			try
			{ bool = XMLToCheck.hasSimpleContent(); }
			catch(error:Error){}
			
			return  bool;
		}
		
		//Recherche et affiche les données XML que l'on recherche
		private function searchData():void
		{		
			var dataSource:ArrayCollection = new ArrayCollection();
			var dataFound:ArrayCollection = new ArrayCollection();
			var dataFoundConcat:ArrayCollection = new ArrayCollection();
			
			libelleTarget = new ArrayCollection();
			
			searchabletreeihm2.txtRechercheOrga.text = _strgToSearch;
			libelleTarget.addItem(searchabletreeihm2.treeDataArray[0]);
			
			//recupere le dossier maitre
			var parents:XML = libelleTarget[0] as XML;
			//Recupere le dossier parents
			var mere:XMLList = parents.children();
			var obj:Object = new Object();
			
			for(var i:int = 0;i < mere.length();i++)
			{	
				if((mere[i] as XML).hasSimpleContent())
				{	
					obj = new Object();
					dataFound.addItem(mere[i]);
					obj.VALUE = mere[i].@VALUE;
					obj.NIV = mere[i].@NIV;
					obj.LABEL = mere[i].@LABEL;
					dataFoundConcat.addItem(obj);
				}
				else
				{
					var enfants0:XMLList = mere[i].children();
					for(var j:int = 0;j < enfants0.length();j++)
					{	
						if((enfants0[j] as XML).hasSimpleContent())
						{
							obj = new Object();
							dataFound.addItem(enfants0[j]);
							obj.VALUE = enfants0[j].@VALUE;
							obj.NIV = enfants0[j].@NIV;
							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL;
							dataFoundConcat.addItem(obj);
						}
						else
						{
							var enfants1:XMLList = enfants0[j].children();
							for(var k:int = 0;k < enfants1.length();k++)
							{
								if((enfants1[k] as XML).hasSimpleContent())
								{
									obj = new Object();
									dataFound.addItem(enfants1[k]);
									obj.VALUE = enfants1[k].@VALUE;
									obj.NIV = enfants1[k].@NIV;
									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL;
									dataFoundConcat.addItem(obj);
									
								}
								else
								{
									var enfants2:XMLList = enfants1[k].children();
									for(var l:int = 0;l < enfants2.length();l++)
									{
										if((enfants2[l] as XML).hasSimpleContent())
										{
											obj = new Object();
											dataFound.addItem(enfants2[l]);
											obj.VALUE = enfants2[l].@VALUE;
											obj.NIV = enfants2[l].@NIV;
											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL;
											dataFoundConcat.addItem(obj);
										}
										else
										{
											var enfants3:XMLList = enfants2[l].children();
											for(var m:int = 0;m < enfants3.length();m++)
											{
												if((enfants3[m] as XML).hasSimpleContent())
												{
													obj = new Object();
													dataFound.addItem(enfants3[m]);
													obj.VALUE = enfants3[m].@VALUE;
													obj.NIV = enfants3[m].@NIV;
													obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL;
													dataFoundConcat.addItem(obj);
												}
												else
												{
													var enfants4:XMLList = enfants3[m].children();
													for(var n:int = 0;n < enfants4.length();n++)
													{
														if((enfants4[n] as XML).hasSimpleContent())
														{
															obj = new Object();
															dataFound.addItem(enfants4[n]);
															obj.VALUE = enfants4[n].@VALUE;
															obj.NIV = enfants4[n].@NIV;
															obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL;
															dataFoundConcat.addItem(obj);
															
														}
														else
														{
															var enfants5:XMLList = enfants4[n].children();
															for(var o:int = 0;o < enfants5.length();o++)
															{
																if((enfants5[o] as XML).hasSimpleContent())
																{		
																	obj = new Object();
																	dataFound.addItem(enfants5[o]);
																	obj.VALUE = enfants5[o].@VALUE;
																	obj.NIV = enfants5[o].@NIV;
																	obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL;
																	dataFoundConcat.addItem(obj);
																	
																}
																else
																{
																	var enfants6:XMLList = enfants5[o].children();
																	for(var p:int = 0;p < enfants6.length();p++)
																	{
																		if((enfants6[p] as XML).hasSimpleContent())
																		{
																			obj = new Object();
																			dataFound.addItem(enfants6[p]);
																			obj.VALUE = enfants6[p].@VALUE;
																			obj.NIV = enfants6[p].@NIV;
																			obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL;
																			dataFoundConcat.addItem(obj);
																		}
																		else
																		{ 
																			var enfants7:XMLList = enfants6[p].children();
																			for(var q:int = 0;q < enfants7.length();q++)
																			{
																				if((enfants7[q] as XML).hasSimpleContent())
																				{
																					obj = new Object();
																					dataFound.addItem(enfants7[q]);
																					obj.VALUE = enfants7[q].@VALUE;
																					obj.NIV = enfants7[q].@NIV;
																					obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL;
																					dataFoundConcat.addItem(obj);
																				}
																				else
																				{
																					var enfants8:XMLList = enfants7[q].children();
																					for(var r:int = 0;r < enfants8.length();r++)
																					{
																						if((enfants8[r] as XML).hasSimpleContent())
																						{
																							obj = new Object();
																							dataFound.addItem(enfants8[r]);
																							obj.VALUE = enfants8[r].@VALUE;
																							obj.NIV = enfants8[r].@NIV;
																							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL;
																							dataFoundConcat.addItem(obj);
																						}
																						else
																						{
																							var enfants9:XMLList = enfants8[r].children();
																							for(var s:int = 0;s < enfants9.length();s++)
																							{
																								if((enfants9[s] as XML).hasSimpleContent())
																								{
																									obj = new Object();
																									dataFound.addItem(enfants9[s]);
																									obj.VALUE = enfants9[s].@VALUE;
																									obj.NIV = enfants9[s].@NIV;
																									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL;
																									dataFoundConcat.addItem(obj);
																								}
																								else
																								{
																									var enfants10:XMLList = enfants9[s].children();
																									for(var t:int = 0;t < enfants10.length();t++)
																									{
																										if((enfants10[t] as XML).hasSimpleContent())
																										{
																											obj = new Object();
																											dataFound.addItem(enfants10[t]);
																											obj.VALUE = enfants10[t].@VALUE;
																											obj.NIV = enfants10[t].@NIV;
																											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL;
																											dataFoundConcat.addItem(obj);
																										}
																										else
																										{
																											var enfants11:XMLList = enfants10[t].children();
																											for(var u:int = 0;u < enfants11.length();u++)
																											{
																												if((enfants11[u] as XML).hasSimpleContent())
																												{
																													obj = new Object();
																													dataFound.addItem(enfants11[u]);
																													obj.VALUE = enfants11[u].@VALUE;
																													obj.NIV = enfants11[u].@NIV;
																													obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL  + "/" + enfants11[u].@LABEL;
																													dataFoundConcat.addItem(obj);
																												}
																												else
																												{
																													var enfants12:XMLList = enfants11[u].children();
																													for(var v:int = 0;v < enfants12.length();v++)
																													{
																														if((enfants12[v] as XML).hasSimpleContent())
																														{
																															obj = new Object();
																															dataFound.addItem(enfants12[v]);
																															obj.VALUE = enfants12[v].@VALUE;
																															obj.NIV = enfants12[v].@NIV;
																															obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL  + "/" + enfants11[u].@LABEL  + "/" + enfants12[v].@LABEL;
																															dataFoundConcat.addItem(obj);
																														}
																														else
																														{
																															var enfants13:XMLList = enfants12[v].children();
																															for(var w:int = 0;w < enfants13.length();w++)
																															{
																																if((enfants13[w] as XML).hasSimpleContent())
																																{
																																	obj = new Object();
																																	dataFound.addItem(enfants13[w]);
																																	obj.VALUE = enfants13[w].@VALUE;
																																	obj.NIV = enfants13[w].@NIV;
																																	obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL  + "/" + enfants11[u].@LABEL  + "/" + enfants12[v].@LABEL  + "/" + enfants13[w].@LABEL;
																																	dataFoundConcat.addItem(obj);
																																}
																																else
																																{
																																	
																																}
																															}
																														}
																													}
																												}
																												
																											}
																										}
																										
																									}
																								}
																								
																							}
																						}
																						
																					}
																				}
																				
																			}
																		}
																		
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			var dataCorresponded:ArrayCollection = new ArrayCollection();
			
			for(var zz:int = 0;zz < dataFoundConcat.length;zz++)
			{
				if(dataFoundConcat[zz].LABEL.toString().toLowerCase().indexOf(_strgToSearch.toLowerCase()) != -1)
				{ dataCorresponded.addItem(dataFoundConcat[zz]); }
			}
			
			if(dataCorresponded.length == 0)
			{
				var newObject:Object = new Object();
				newObject.LABEL = ResourceManager.getInstance().getString('M16', 'Non_r_pertori_e');
				dgListTarget.dataProvider = newObject;
			}
			else
			{
				dataConcat = dataCorresponded;
				dgListTarget.dataProvider = dataCorresponded;
			}
			
			lblNbrNode.text = dataCorresponded.length.toString() + ResourceManager.getInstance().getString('M16', '_noeuds');
			lblNbrNode2.text = dataCorresponded.length.toString() + ResourceManager.getInstance().getString('M16', '_noeuds');
		}
		
		//Recherche et affiche tout l'XML
		private function searchAll():void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			var dataSource:ArrayCollection = new ArrayCollection();
			var dataFound:ArrayCollection = new ArrayCollection();
			var dataFoundConcat:ArrayCollection = new ArrayCollection();
			
			libelleTarget = new ArrayCollection();
			
			searchabletreeihm2.txtRechercheOrga.text = _strgToSearch;
			libelleTarget.addItem(searchabletreeihm2.treeDataArray[0]);
			
			//recupere le dossier maitre
			var parents:XML = libelleTarget[0] as XML;
			//Recupere le dossier parents
			var mere:XMLList = parents.children();
			var obj:Object = new Object();
			
			for(var i:int = 0;i < mere.length();i++)
			{	
				if((mere[i] as XML).hasSimpleContent())
				{	
					obj = new Object();
					dataFound.addItem(mere[i]);
					obj.VALUE = mere[i].@VALUE;
					obj.NIV = mere[i].@NIV;
					obj.LABEL = mere[i].@LABEL;
					dataFoundConcat.addItem(obj);
				}
				else
				{
					var enfants0:XMLList = mere[i].children();
					for(var j:int = 0;j < enfants0.length();j++)
					{	
						if((enfants0[j] as XML).hasSimpleContent())
						{
							obj = new Object();
							dataFound.addItem(enfants0[j]);
							obj.VALUE = enfants0[j].@VALUE;
							obj.NIV = enfants0[j].@NIV;
							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL;
							dataFoundConcat.addItem(obj);
						}
						else
						{
							var enfants1:XMLList = enfants0[j].children();
							for(var k:int = 0;k < enfants1.length();k++)
							{
								if((enfants1[k] as XML).hasSimpleContent())
								{
									obj = new Object();
									dataFound.addItem(enfants1[k]);
									obj.VALUE = enfants1[k].@VALUE;
									obj.NIV = enfants1[k].@NIV;
									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL;
									dataFoundConcat.addItem(obj);
									
								}
								else
								{
									var enfants2:XMLList = enfants1[k].children();
									for(var l:int = 0;l < enfants2.length();l++)
									{
										if((enfants2[l] as XML).hasSimpleContent())
										{
											obj = new Object();
											dataFound.addItem(enfants2[l]);
											obj.VALUE = enfants2[l].@VALUE;
											obj.NIV = enfants2[l].@NIV;
											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL;
											dataFoundConcat.addItem(obj);
										}
										else
										{
											var enfants3:XMLList = enfants2[l].children();
											for(var m:int = 0;m < enfants3.length();m++)
											{
												if((enfants3[m] as XML).hasSimpleContent())
												{
													obj = new Object();
													dataFound.addItem(enfants3[m]);
													obj.VALUE = enfants3[m].@VALUE;
													obj.NIV = enfants3[m].@NIV;
													obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL;
													dataFoundConcat.addItem(obj);
												}
												else
												{
													var enfants4:XMLList = enfants3[m].children();
													for(var n:int = 0;n < enfants4.length();n++)
													{
														if((enfants4[n] as XML).hasSimpleContent())
														{
															obj = new Object();
															dataFound.addItem(enfants4[n]);
															obj.VALUE = enfants4[n].@VALUE;
															obj.NIV = enfants4[n].@NIV;
															obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL;
															dataFoundConcat.addItem(obj);
															
														}
														else
														{
															var enfants5:XMLList = enfants4[n].children();
															for(var o:int = 0;o < enfants5.length();o++)
															{
																if((enfants5[o] as XML).hasSimpleContent())
																{		
																	obj = new Object();
																	dataFound.addItem(enfants5[o]);
																	obj.VALUE = enfants5[o].@VALUE;
																	obj.NIV = enfants5[o].@NIV;
																	obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL;
																	dataFoundConcat.addItem(obj);
																	
																}
																else
																{
																	var enfants6:XMLList = enfants5[o].children();
																	for(var p:int = 0;p < enfants6.length();p++)
																	{
																		if((enfants6[p] as XML).hasSimpleContent())
																		{
																			obj = new Object();
																			dataFound.addItem(enfants6[p]);
																			obj.VALUE = enfants6[p].@VALUE;
																			obj.NIV = enfants6[p].@NIV;
																			obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL;
																			dataFoundConcat.addItem(obj);
																		}
																		else
																		{ 
																			var enfants7:XMLList = enfants6[p].children();
																			for(var q:int = 0;q < enfants7.length();q++)
																			{
																				if((enfants7[q] as XML).hasSimpleContent())
																				{
																					obj = new Object();
																					dataFound.addItem(enfants7[q]);
																					obj.VALUE = enfants7[q].@VALUE;
																					obj.NIV = enfants7[q].@NIV;
																					obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL;
																					dataFoundConcat.addItem(obj);
																				}
																				else
																				{
																					var enfants8:XMLList = enfants7[q].children();
																					for(var r:int = 0;r < enfants8.length();r++)
																					{
																						if((enfants8[r] as XML).hasSimpleContent())
																						{
																							obj = new Object();
																							dataFound.addItem(enfants8[r]);
																							obj.VALUE = enfants8[r].@VALUE;
																							obj.NIV = enfants8[r].@NIV;
																							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL;
																							dataFoundConcat.addItem(obj);
																						}
																						else
																						{
																							var enfants9:XMLList = enfants8[r].children();
																							for(var s:int = 0;s < enfants9.length();s++)
																							{
																								if((enfants9[s] as XML).hasSimpleContent())
																								{
																									obj = new Object();
																									dataFound.addItem(enfants9[s]);
																									obj.VALUE = enfants9[s].@VALUE;
																									obj.NIV = enfants9[s].@NIV;
																									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL;
																									dataFoundConcat.addItem(obj);
																								}
																								else
																								{
																									var enfants10:XMLList = enfants9[s].children();
																									for(var t:int = 0;t < enfants10.length();t++)
																									{
																										if((enfants10[t] as XML).hasSimpleContent())
																										{
																											obj = new Object();
																											dataFound.addItem(enfants10[t]);
																											obj.VALUE = enfants10[t].@VALUE;
																											obj.NIV = enfants10[t].@NIV;
																											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL;
																											dataFoundConcat.addItem(obj);
																										}
																										else
																										{
																											var enfants11:XMLList = enfants10[t].children();
																											for(var u:int = 0;u < enfants11.length();u++)
																											{
																												if((enfants11[u] as XML).hasSimpleContent())
																												{
																													obj = new Object();
																													dataFound.addItem(enfants11[u]);
																													obj.VALUE = enfants11[u].@VALUE;
																													obj.NIV = enfants11[u].@NIV;
																													obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL  + "/" + enfants11[u].@LABEL;
																													dataFoundConcat.addItem(obj);
																												}
																												else
																												{
																													var enfants12:XMLList = enfants11[u].children();
																													for(var v:int = 0;v < enfants12.length();v++)
																													{
																														if((enfants12[v] as XML).hasSimpleContent())
																														{
																															obj = new Object();
																															dataFound.addItem(enfants12[v]);
																															obj.VALUE = enfants12[v].@VALUE;
																															obj.NIV = enfants12[v].@NIV;
																															obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL  + "/" + enfants11[u].@LABEL  + "/" + enfants12[v].@LABEL;
																															dataFoundConcat.addItem(obj);
																														}
																														else
																														{
																															var enfants13:XMLList = enfants12[v].children();
																															for(var w:int = 0;w < enfants13.length();w++)
																															{
																																if((enfants13[w] as XML).hasSimpleContent())
																																{
																																	obj = new Object();
																																	dataFound.addItem(enfants13[w]);
																																	obj.VALUE = enfants13[w].@VALUE;
																																	obj.NIV = enfants13[w].@NIV;
																																	obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL  + "/" + enfants11[u].@LABEL  + "/" + enfants12[v].@LABEL  + "/" + enfants13[w].@LABEL;
																																	dataFoundConcat.addItem(obj);
																																}
																																else
																																{
																																	
																																}
																															}
																														}
																													}
																												}
																												
																											}
																										}
																										
																									}
																								}
																								
																							}
																						}
																						
																					}
																				}
																				
																			}
																		}
																		
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			if(dataFound.length != 0)
			{
				dataConcat = dataFoundConcat;
				dgListTarget.dataProvider = dataFoundConcat;
			}
			
			lblNbrNode.text = dataFoundConcat.length.toString() + ResourceManager.getInstance().getString('M16', '_noeuds');
			lblNbrNode2.text = ResourceManager.getInstance().getString('M16', '0_noeuds');
		}
		
		//Recherche les données dans l'XML
		private function searchInXML(objectSelected:Object):Boolean
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			var dataSource:ArrayCollection = new ArrayCollection();
			var dataFound:ArrayCollection = new ArrayCollection();
			var dataFoundConcat:ArrayCollection = new ArrayCollection();
			
			_nbrOfStage = 0;
			
			libelleTarget = new ArrayCollection();
			
			searchabletreeihm2.txtRechercheOrga.text = _strgToSearch;
			libelleTarget.addItem(searchabletreeihm2.treeDataArray[0]);
			
			//recupere le dossier maitre
			var parents:XML = libelleTarget[0] as XML;
			//Recupere le dossier parents
			var mere:XMLList = parents.children();
			var obj:Object = new Object();
			
			
			dataFound.addItem(parents);
			
			obj.VALUE = parents.@VALUE;
			obj.NIV = parents.@NIV;
			obj.LABEL = parents.@LABEL;
			dataFoundConcat.addItem(obj);
			
			_nbrOfStage = 1;
			
			for(var i:int = 0;i < mere.length();i++)
			{	
				if((mere[i] as XML).hasSimpleContent())
				{	
					obj = new Object();
					dataFound.addItem(mere[i]);
					obj.VALUE = mere[i].@VALUE;
					obj.NIV = mere[i].@NIV;
					obj.LABEL = mere[i].@LABEL;
					dataFoundConcat.addItem(obj);
				}
				else
				{
					var enfants0:XMLList = mere[i].children();
					for(var j:int = 0;j < enfants0.length();j++)
					{	
						if((enfants0[j] as XML).hasSimpleContent())
						{
							obj = new Object();
							dataFound.addItem(enfants0[j]);
							obj.VALUE = enfants0[j].@VALUE;
							obj.NIV = enfants0[j].@NIV;
							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL;
							dataFoundConcat.addItem(obj);
						}
						else
						{
							var enfants1:XMLList = enfants0[j].children();
							for(var k:int = 0;k < enfants1.length();k++)
							{
								if((enfants1[k] as XML).hasSimpleContent())
								{
									obj = new Object();
									dataFound.addItem(enfants1[k]);
									obj.VALUE = enfants1[k].@VALUE;
									obj.NIV = enfants1[k].@NIV;
									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL;
									dataFoundConcat.addItem(obj);
									
								}
								else
								{
									var enfants2:XMLList = enfants1[k].children();
									for(var l:int = 0;l < enfants2.length();l++)
									{
										if((enfants2[l] as XML).hasSimpleContent())
										{
											obj = new Object();
											dataFound.addItem(enfants2[l]);
											obj.VALUE = enfants2[l].@VALUE;
											obj.NIV = enfants2[l].@NIV;
											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL;
											dataFoundConcat.addItem(obj);
										}
										else
										{
											var enfants3:XMLList = enfants2[l].children();
											for(var m:int = 0;m < enfants3.length();m++)
											{
												if((enfants3[m] as XML).hasSimpleContent())
												{
													obj = new Object();
													dataFound.addItem(enfants3[m]);
													obj.VALUE = enfants3[m].@VALUE;
													obj.NIV = enfants3[m].@NIV;
													obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL;
													dataFoundConcat.addItem(obj);
												}
												else
												{
													var enfants4:XMLList = enfants3[m].children();
													for(var n:int = 0;n < enfants4.length();n++)
													{
														if((enfants4[n] as XML).hasSimpleContent())
														{
															obj = new Object();
															dataFound.addItem(enfants4[n]);
															obj.VALUE = enfants4[n].@VALUE;
															obj.NIV = enfants4[n].@NIV;
															obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL;
															dataFoundConcat.addItem(obj);
															
														}
														else
														{
															var enfants5:XMLList = enfants4[n].children();
															for(var o:int = 0;o < enfants5.length();o++)
															{
																if((enfants5[o] as XML).hasSimpleContent())
																{		
																	obj = new Object();
																	dataFound.addItem(enfants5[o]);
																	obj.VALUE = enfants5[o].@VALUE;
																	obj.NIV = enfants5[o].@NIV;
																	obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL;
																	dataFoundConcat.addItem(obj);
																	
																}
																else
																{
																	var enfants6:XMLList = enfants5[o].children();
																	for(var p:int = 0;p < enfants6.length();p++)
																	{
																		if((enfants6[p] as XML).hasSimpleContent())
																		{
																			obj = new Object();
																			dataFound.addItem(enfants6[p]);
																			obj.VALUE = enfants6[p].@VALUE;
																			obj.NIV = enfants6[p].@NIV;
																			obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL;
																			dataFoundConcat.addItem(obj);
																		}
																		else
																		{ 
																			var enfants7:XMLList = enfants6[p].children();
																			for(var q:int = 0;q < enfants7.length();q++)
																			{
																				if((enfants7[q] as XML).hasSimpleContent())
																				{
																					obj = new Object();
																					dataFound.addItem(enfants7[q]);
																					obj.VALUE = enfants7[q].@VALUE;
																					obj.NIV = enfants7[q].@NIV;
																					obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL;
																					dataFoundConcat.addItem(obj);
																				}
																				else
																				{
																					var enfants8:XMLList = enfants7[q].children();
																					for(var r:int = 0;r < enfants8.length();r++)
																					{
																						if((enfants8[r] as XML).hasSimpleContent())
																						{
																							obj = new Object();
																							dataFound.addItem(enfants8[r]);
																							obj.VALUE = enfants8[r].@VALUE;
																							obj.NIV = enfants8[r].@NIV;
																							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL;
																							dataFoundConcat.addItem(obj);
																						}
																						else
																						{
																							var enfants9:XMLList = enfants8[r].children();
																							for(var s:int = 0;s < enfants9.length();s++)
																							{
																								if((enfants9[s] as XML).hasSimpleContent())
																								{
																									obj = new Object();
																									dataFound.addItem(enfants9[s]);
																									obj.VALUE = enfants9[s].@VALUE;
																									obj.NIV = enfants9[s].@NIV;
																									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL;
																									dataFoundConcat.addItem(obj);
																								}
																								else
																								{
																									var enfants10:XMLList = enfants9[s].children();
																									for(var t:int = 0;t < enfants10.length();t++)
																									{
																										if((enfants10[t] as XML).hasSimpleContent())
																										{
																											obj = new Object();
																											dataFound.addItem(enfants10[t]);
																											obj.VALUE = enfants10[t].@VALUE;
																											obj.NIV = enfants10[t].@NIV;
																											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL;
																											dataFoundConcat.addItem(obj);
																										}
																										else
																										{
																											var enfants11:XMLList = enfants10[t].children();
																											for(var u:int = 0;u < enfants11.length();u++)
																											{
																												if((enfants11[u] as XML).hasSimpleContent())
																												{
																													obj = new Object();
																													dataFound.addItem(enfants11[u]);
																													obj.VALUE = enfants11[u].@VALUE;
																													obj.NIV = enfants11[u].@NIV;
																													obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL  + "/" + enfants11[u].@LABEL;
																													dataFoundConcat.addItem(obj);
																												}
																												else
																												{
																													var enfants12:XMLList = enfants11[u].children();
																													for(var v:int = 0;v < enfants12.length();v++)
																													{
																														if((enfants12[v] as XML).hasSimpleContent())
																														{
																															obj = new Object();
																															dataFound.addItem(enfants12[v]);
																															obj.VALUE = enfants12[v].@VALUE;
																															obj.NIV = enfants12[v].@NIV;
																															obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL  + "/" + enfants11[u].@LABEL  + "/" + enfants12[v].@LABEL;
																															dataFoundConcat.addItem(obj);
																														}
																														else
																														{
																															var enfants13:XMLList = enfants12[v].children();
																															for(var w:int = 0;w < enfants13.length();w++)
																															{
																																if((enfants13[w] as XML).hasSimpleContent())
																																{
																																	obj = new Object();
																																	dataFound.addItem(enfants13[w]);
																																	obj.VALUE = enfants13[w].@VALUE;
																																	obj.NIV = enfants13[w].@NIV;
																																	obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL + "/" + enfants7[q].@LABEL  + "/" + enfants8[r].@LABEL  + "/" + enfants9[s].@LABEL  + "/" + enfants10[t].@LABEL  + "/" + enfants11[u].@LABEL  + "/" + enfants12[v].@LABEL  + "/" + enfants13[w].@LABEL;
																																	dataFoundConcat.addItem(obj);
																																}
																																else
																																{
																																	
																																}
																															}
																														}
																													}
																												}
																												
																											}
																										}
																										
																									}
																								}
																								
																							}
																						}
																						
																					}
																				}
																				
																			}
																		}
																		
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			var numberOfResultFound:int = 0;
			var strg:String = objectSelected.LABEL;
			for(var tp:int;tp < dataFoundConcat.length;tp++)
			{
				if(dataFound[tp].@VALUE == objectSelected.VALUE)
				{
					dataFound.addItem(dataFound[tp]);
					numberOfResultFound = tp;
				}
			}
			var bool:Boolean = false;
			try
			{ bool = (dataFound[numberOfResultFound] as XML).hasSimpleContent(); }
			catch(error:Error)
			{  }
			return bool;
		}
		
		//FIN METHODES PRIVATE----
		
	}
}
