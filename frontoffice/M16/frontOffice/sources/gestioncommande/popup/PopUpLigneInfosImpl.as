package gestioncommande.popup
{
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Image;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class PopUpLigneInfosImpl extends TitleWindow
	{
		
		[Embed(source="/assets/images/noneligibilite24x24.png", mimeType="image/png")] 
		public var imgInvalide	:Class;
		[Embed(source="/assets/images/eligibilite24x24.png", mimeType="image/png")] 
		public var imgValide	:Class; 
		
		public var imgEtatEligibilite					:Image;
		
		[Bindable]public var currentLigneInfos			:Object = new Object();
		
		public function PopUpLigneInfosImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		public function setCurrentData(currentInfos:Object):void
		{
			currentLigneInfos = currentInfos;
		}
		
		protected function creationCompleteHandler(fe:FlexEvent):void
		{
			switch(int(currentLigneInfos.ELIGIBILITE))
			{
				case 0  : imgEtatEligibilite.source = imgInvalide; 	break;
				case 1  : imgEtatEligibilite.source = imgValide; 	break;
			}
		}
		
		protected function closeHandler(ce:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnFermerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
	}
}