package gestioncommande.popup
{
	
	import flash.events.Event;
	
	import gestioncommande.events.CommandeEvent;
	
	import mx.containers.Box;
	import mx.managers.PopUpManager;
	
	public class PopUpConfirmationCommandeImpl extends Box
	{
		public function PopUpConfirmationCommandeImpl()
		{
		}
		
		protected function btnValiderClickHandler():void
		{
			//APPEL DE PROCEDURE POUR VALIDER ET ENVOYER LA COMMANDE
			dispatchEvent(new CommandeEvent(CommandeEvent.SEND_COMMANDE));
			dispatchEvent(new Event("sendCommande"));
			PopUpManager.removePopUp(this);
			dispatchEvent(new Event("closePopUpCommande"));
		}
		
		protected function btnAnnulerClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}

	}
}