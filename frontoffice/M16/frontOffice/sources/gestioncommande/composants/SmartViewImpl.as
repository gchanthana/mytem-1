package gestioncommande.composants
{
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.events.DetailsDeviceEvent;
	
	import mx.containers.Canvas;
	import mx.controls.Button;
	import mx.controls.Image;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	[Bindable]
	public class SmartViewImpl extends Canvas
	{
		public var img_phone				:Image
		public var btn_select				:Button;
		public var btn_dispo				:Button;
		public var btn_select0				:ActionButon;
		
		public const SRC_NO_PHOTO			:String = 'http://images.consotel.fr/noPhoto_128.png';
		
		[Embed(source="/assets/images/info_32_32.png", mimeType="image/png")] 
		public var imgInfo:Class;
		
		[Embed('/assets/commande/v_selected_60.png')]
		public var selectedIcon:Class;
		
		[Embed('/assets/commande/v_nselected_60_2.png')]
		public var notselectedIcon:Class;
		
		public function SmartViewImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, initsv);
		}
		
		protected function initsv(event:FlexEvent):void
		{
			
		}
		
		override public function set data(value:Object):void
		{
			
			if(data != value){
				super.data = value;
				invalidateDisplayList();
			}
			
		}
		
		protected function getDetailsDevice(event:Event):void
		{
			dispatchEvent(new DetailsDeviceEvent(DetailsDeviceEvent.MY_DETAILS_DEVICE, (data as EquipementsElements), true));
		}
		
		protected function onSelectDevice(event:MouseEvent):void
		{
			if(data.SUSPENDU_CLT == 1)
			{
				data.SELECTED =  false;	
			}
			else
			{
				if(data.SELECTED ==  false)
				{
					data.SELECTED =  true;
				}
				else
				{
					data.SELECTED =  false;
				}
			}
			
			dispatchEvent(new Event('REFRESH_ITEM_SELECTED', true));
		}
		
		public function setStatus(item:EquipementsElements):String			
		{	
			var statLabel:String = '';
			if (item != null )				
			{				
				if (item.DATEVALIDITE_CLT != null && DateFunction.dateDiff("d",item.DATEVALIDITE_CLT,new Date()) +1 < 0 ) // fin de vie	
				{	
//					rColor = 0xFF0000;
					statLabel = ResourceManager.getInstance().getString('M16','Fin_de_vie');
					btn_select0.labelBtn = ResourceManager.getInstance().getString('M16', 'Choisir');
				}
				else if (item.SUSPENDU_CLT == 1)//En rupture
				{	
					btn_dispo.setStyle("styleName", 'btnRupture');
					statLabel = ResourceManager.getInstance().getString('M16','En_rupture');
					
					btn_select0.labelBtn = ResourceManager.getInstance().getString('M16','En_rupture');
				}
				else if (item.DATEVALIDITE_CLT != null) 	//Bientot fin de vie
				{	 
					btn_dispo.setStyle("styleName", 'btnBientotFin');
					if(calculerReste(item) != '')
						statLabel = ResourceManager.getInstance().getString('M16','Bient_t_fin_de_vie') + ': '+ calculerReste(item)+' '+ ResourceManager.getInstance().getString('M16','_jour_') ;
					else
						statLabel = ResourceManager.getInstance().getString('M16','Bient_t_fin_de_vie');
					
					btn_select0.labelBtn = ResourceManager.getInstance().getString('M16', 'Choisir');
					
				}
				else
				{
					btn_dispo.setStyle("styleName", 'btnDispo');
					statLabel = ResourceManager.getInstance().getString('M16','Disponibles');
					btn_select0.labelBtn = ResourceManager.getInstance().getString('M16', 'Choisir');
				}
			}
			
			return statLabel;
		}
		
		public function calculerReste(item:Object):String	
		{
			var val:String = '';
			if (item.SUSPENDU_CLT != 1)
			{	
				if(item.DELAIALERT != null)
				{	
					if (item.DATEVALIDITE_CLT != null && DateFunction.dateDiff("d",item.DATEVALIDITE_CLT,new Date())+1 < item.DELAIALERT)				
						val = (DateFunction.dateDiff("d",item.DATEVALIDITE_CLT,new Date())+1).toString();
					else	
						val = '';
				}else
					val = '';	
			}
			else
				val = '';
			return val;
		}
		
		public function getPrixChiffre(prix:String):String
		{
			var pattern:RegExp = /[\.|\,]/g; 
			var params:Array = prix.split(pattern);
			var strNum:String = '';
			switch(params.length)
			{
				case 2:
				{
					strNum = params[0] + ResourceManager.getInstance().getString('M16','formateur_decimalSeparatorTo');
					break;
				}
				case 3:
				{
					strNum = params[0] + ResourceManager.getInstance().getString('M16','formateur_thousandsSeparatorTo')
						+ params[1]+ ResourceManager.getInstance().getString('M16','formateur_decimalSeparatorTo');
					break;
				}
				case 4: // cas extreme : format exemple 5 999,500.00 ==>
				{
					strNum = params[0] + params[1] + ResourceManager.getInstance().getString('M16','formateur_thousandsSeparatorTo')
						+ params[2]+ ResourceManager.getInstance().getString('M16','formateur_decimalSeparatorTo');
					break;
				}
				default:// pas de formatage
				{
					for (var i:int = 0; i < params.length-1; i++) 
					{
						strNum += params[i];
					}
					strNum = strNum + ResourceManager.getInstance().getString('M16','formateur_decimalSeparatorTo');
					break;
				}
			}
			
			return strNum;
			
		}
		
		public function getPrixVirgule(prix:String):String
		{
			var pattern:RegExp = /[\.|\,]/g; 
			var params:Array = prix.split(pattern);
			
			return params[params.length-1];
		}
	}
}