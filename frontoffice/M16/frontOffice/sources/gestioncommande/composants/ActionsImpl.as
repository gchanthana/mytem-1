package gestioncommande.composants
{
	import gestioncommande.events.ActionsEvent;
	
	import flash.events.MouseEvent;
	
	import mx.containers.Box;
	
	public class ActionsImpl extends Box
	{
		public function ActionsImpl()
		{
		}
		
		protected function imgEditClickHandler(me:MouseEvent):void
		{
			createEventDispatcher(ActionsEvent.ACTIONSEVENT_EDIT);
		}
		
		protected function imgDwlClickHandler(me:MouseEvent):void
		{
			createEventDispatcher(ActionsEvent.ACTIONSEVENT_DOWNLOAD);
		}
		
		protected function imgEraseClickHandler(me:MouseEvent):void
		{
			createEventDispatcher(ActionsEvent.ACTIONSEVENT_ERASE);
		}

		private function createEventDispatcher(type:String):void
		{
			dispatchEvent(new ActionsEvent(type, true, this.data));
		}

	}
}