package gestioncommande.iinterface
{
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	
	import mx.events.FlexEvent;
	
	public interface IWizzard extends IEventDispatcher
	{
		function creationCompleteHandler(fe:FlexEvent):void
		
		function clickButtonNextHandler(me:MouseEvent):void

		function clickButtonPreviousHandler(me:MouseEvent):void
		
		function clickButtonCancelHandler(me:MouseEvent):void
		
		function clickButtonValidHandler(me:MouseEvent):void
		
		function registerCommande(e:Event = null):void
		
	}
}