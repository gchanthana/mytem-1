package gestioncommande.iinterface
{
	import flash.events.IEventDispatcher;
	
	import mx.events.FlexEvent;
	
	public interface IWorflow extends IEventDispatcher
	{
		
		function creationCompleteHandler(fl:FlexEvent):void
		
		function showHandler(fl:FlexEvent):void
		
		function viewStackChange():void

		function checkData():Boolean
		
		function buildParameters():void

	}
}