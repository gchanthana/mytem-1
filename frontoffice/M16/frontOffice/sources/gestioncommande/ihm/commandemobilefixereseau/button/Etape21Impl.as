package gestioncommande.ihm.commandemobilefixereseau.button
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.List;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.styles.StyleManager;
	import mx.utils.ObjectUtil;
	
	import composants.colorDatagrid.ColorDatagrid;
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.DetailsDeviceEvent;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.DetailProduitIHM;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.Formator;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class Etape21Impl extends AbstractWorflow implements IWorflow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListTerminal			:ColorDatagrid;
		
		public var txtptSearch				:TextInput;
		
		public var list_terminaux			:List;
		public var img_view_list			:Image;
		public var img_view_smart			:Image;
		
		public var glow_0:GlowFilter = new GlowFilter();
		public var glow_1:GlowFilter = new GlowFilter();
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _equipementsSrv			:EquipementService = new EquipementService();
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _mobile					:EquipementsElements = null;
		
		private var _cmd					:Commande;
		
		private var _myElements				:ElementsCommande2	= new ElementsCommande2();

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES BUTTON CUSSTOM
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE					:int = 2;
		
		public var ACCESS					:Boolean = false;
		
		private var _razl					:Boolean = false;
		
		public var bool_viewsmart			:Boolean = true;
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//		
		
		public var terminaux				:ArrayCollection = new ArrayCollection();
		
		private var selectedEquipement		:EquipementsElements;
		
		//--------------------------------------------------------------------------------------------//
		//					CHANGEMENT D'UN ELEMENT DANS L'ECRAN PRECEDANT
		//--------------------------------------------------------------------------------------------//
		
		private var _idOpe					:int = -1;
		private var _idGes					:int = -1;
		private var _idRev					:int = -1;
		private var _idPro					:int = -1;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
				
		public function Etape21Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					LISTENER DE L'IHM
	//--------------------------------------------------------------------------------------------//

		public function creationCompleteHandler(fl:FlexEvent):void
		{
			addEventListener("DETAILS", descriptionHandler);
			addEventListener(DetailsDeviceEvent.MY_DETAILS_DEVICE, detailsTerminalSelectedSmart);
			addEventListener('REFRESH_ITEM_SELECTED', refreshListDevice );
			
			glow_1.color = StyleManager.getColorName('#558ed5');
			glow_1.alpha = 0.8;
			glow_1.blurX = 4;
			glow_1.blurY = 4;
			glow_1.strength = 6;
			glow_1.quality = BitmapFilterQuality.HIGH;
			
			glow_0.color = StyleManager.getColorName('#FFFFFF');
			glow_0.alpha = 0.8;
			glow_0.blurX = 4;
			glow_0.blurY = 4;
			glow_0.strength = 6;
			glow_0.quality = BitmapFilterQuality.HIGH;
			
			img_view_smart.filters = [glow_1];
			img_view_list.filters = [glow_0];
		}
		
		protected function refreshListDevice(event:Event):void
		{
			if(list_terminaux!=null && list_terminaux.selectedItem != null)
				selectedEquipement = list_terminaux.selectedItem as EquipementsElements;
			for each (var obj:EquipementsElements in terminaux.source) 
			{
				if(obj.IDFOURNISSEUR != selectedEquipement.IDFOURNISSEUR)
				{
					obj.SELECTED = false;
				}
			}
		}
		
		public function showHandler(fe:FlexEvent):void
		{
			SessionUserObject.singletonSession.LASTVSINDEX = 1;
		}

		public function viewStackChange():void
		{
			if(_equipementsSrv == null) return;
			
			_cmd 		= SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;

			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;

			if(_idOpe != _cmd.IDOPERATEUR || _idRev != _cmd.IDREVENDEUR || _idPro != _cmd.IDPROFIL_EQUIPEMENT)
			{				
				getTerminauxSimCardAndEngagements();

				_idOpe = _cmd.IDOPERATEUR;
				_idRev = _cmd.IDREVENDEUR;
				_idPro = _cmd.IDPROFIL_EQUIPEMENT;
			}
		}
		
		public function reset():void
		{
			_mobile = null;
			_idOpe 	= 0;
			
			terminaux.removeAll();
		}
		
		public function setConfiguration():void
		{
			_razl = true;
			
			getTerminauxSimCardAndEngagements();
		}

	//--------------------------------------------------------------------------------------------//
	//					VERIFICATION ET CONTRUCTION DES DONNÉES CHOISIES
	//--------------------------------------------------------------------------------------------//
				
		public function checkData():Boolean
		{
			if(findTerminalSelected())
			{
				_myElements.TERMINAUX.addItem(_mobile);
				
				if(_mobile.IDFOURNISSEUR > 0)
					_cmd.IDEQUIPEMENTPARENT = _mobile.IDFOURNISSEUR;
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_terminal'), 'Consoview', null);
				return false;
			}
			
			return true;
		}
		
		public function buildParameters():void
		{
		}
		
//--------------------------------------------------------------------------------------------//
//					FUNCTION PROPRE A L'ETAPE 
//--------------------------------------------------------------------------------------------//
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//

		protected function txtptSearchHandler(e:Event):void
		{
			if(dgListTerminal.dataProvider != null)
	    	{
	    		(dgListTerminal.dataProvider as ArrayCollection).filterFunction = filterFunction;
	    		(dgListTerminal.dataProvider as ArrayCollection).refresh();
	    	}
		}
		
		protected function dgListTerminalClickHandler(e:Event):void
		{
			if(dgListTerminal.selectedItem != null)
			{
				var idEqFour	:int = dgListTerminal.selectedItem.IDFOURNISSEUR;
				var idEqClie	:int = dgListTerminal.selectedItem.IDCLIENT;
				var lenEq		:int = _equipementsSrv.TERMINAUX.length;
				
				for(var i:int = 0;i < lenEq;i++)
				{
					_equipementsSrv.TERMINAUX[i].SELECTED = false;
					
					if(_equipementsSrv.TERMINAUX[i].IDFOURNISSEUR == idEqFour)
					{
						if(_equipementsSrv.TERMINAUX[i].IDCLIENT == idEqClie)
							if (_equipementsSrv.TERMINAUX[i].SUSPENDU_CLT != 1)
								_equipementsSrv.TERMINAUX[i].SELECTED = true;
					}
					
					_equipementsSrv.TERMINAUX.itemUpdated(_equipementsSrv.TERMINAUX[i]);
				}
				
				dispatchEvent(new Event('MOBILE_CHANGED', true));
			}
		}

	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//	

		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getTerminauxSimCardAndEngagements():void
		{
			terminaux.removeAll();
			
			if(_cmd != null)
			{
				_equipementsSrv.fournirTerminauxSimCardEngagements(_cmd.IDPOOL_GESTIONNAIRE, _cmd.IDPROFIL_EQUIPEMENT, _cmd.IDREVENDEUR, _cmd.IDOPERATEUR,
																	SessionUserObject.singletonSession.IDSEGMENT, 1);
				_equipementsSrv.addEventListener(CommandeEvent.TERMSIMCARDENGAG, termianuxSimCardAndEngagementsHandler);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function termianuxSimCardAndEngagementsHandler(cmde:CommandeEvent):void
		{			
			terminaux = _equipementsSrv.TERMINAUX;
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 10)
				addItemEmpty();
			
			if(_razl)
				getTerminalSelected(terminaux);
			
			this.setSmartView();
		}
		
		// fonction pour afficher les téléphones en viewSmart au debut plutot que la liste simple
		private function setSmartView():void
		{
			bool_viewsmart = true;
			
			calculerPrix();
		}
		
		private function calculerPrix():void
		{
			for (var i:int = 0; i < terminaux.length; i++) 
			{
				if(terminaux[i].IDEQUIPEMENT == 0) 
					terminaux[i].PRIX_S = '';
				
				terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_C));
				terminaux[i].PRIX = Number(terminaux[i].PRIX_C);
				
				terminaux.itemUpdated(terminaux[i]);
			}
			
			
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		public function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint			
		{				
			var rColor:uint;				
			rColor = color;				
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			
			if (item != null )				
			{				
				if(item.SUSPENDU_CLT == 1)//En repture
				{	
					rColor = 0xF7FF04;
				}			
				else					
					rColor = color;					
			}				
			return rColor;				
		}
		
		private function addItemEmpty():void
		{
			terminaux.addItemAt(EquipementsElements.formatTerminalEmpty(), 0);
		}
		
		private function findTerminalSelected():Boolean
		{			
			_myElements.TERMINAUX = new ArrayCollection();
			
			_mobile = new EquipementsElements();
			
			var len:int = terminaux.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(terminaux[i].SELECTED)
				{
					_mobile = terminaux[i] as EquipementsElements;
					return true;
				}
			}
			
			return false;
		}
		
		private function getTerminalSelected(values:ArrayCollection):void
		{
			var lenTer		:int = values.length;
			var lenTerSel	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX.length;
			var idTerminal	:int = 0;
			
			for(var i:int = 0;i < lenTerSel;i++)
			{
				if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].IDEQUIPEMENT != 71)
				{
					idTerminal  = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].IDFOURNISSEUR;
					break;
				}
			}
			
			for(var j:int = 0;j < lenTer;j++)
			{
				if(values[j].IDFOURNISSEUR == idTerminal)
				{
					values[j].SELECTED = true;
					return;
				}
			}
		}
		
		public function dataFieldPrice(equipement:Object, column:DataGridColumn):String
		{				
			if(equipement.IDEQUIPEMENT == 0) return "";
			
			equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_C));
			equipement.PRIX = Number(equipement.PRIX_C);
			return equipement.PRIX_S;
			
		}
		
		public function sortPrice(itemA:Object, itemB:Object):int
		{
			var priceA:Number = itemA.PRIX;
			var priceB:Number = itemB.PRIX;
			
			return ObjectUtil.numericCompare(priceA, priceB);
		}
		
		protected function onSmartViewClickHandler(event:MouseEvent):void
		{
			bool_viewsmart = true;
			
			img_view_smart.filters = [glow_1];
			img_view_list.filters = [glow_0];
		}
		
		protected function onListViewClickHandler(event:MouseEvent):void
		{
			bool_viewsmart = false;
			
			img_view_smart.filters = [glow_0];
			img_view_list.filters = [glow_1];
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER
		//--------------------------------------------------------------------------------------------//
		
		private function descriptionHandler(e:Event):void
		{
			if(dgListTerminal.selectedItem != null)
			{
				var popUpDetails:DetailProduitIHM 	= new DetailProduitIHM();
					popUpDetails.equipement			= dgListTerminal.selectedItem;
				
				PopUpManager.addPopUp(popUpDetails, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}
		
		protected function detailsTerminalSelectedSmart(evtDetails:DetailsDeviceEvent):void
		{
			if(evtDetails.selectedEquipementsElements !=null)
			{
				var popUpDetails:DetailProduitIHM 	= new DetailProduitIHM();
				popUpDetails.equipement			= evtDetails.selectedEquipementsElements;
				popUpDetails.addEventListener('REFRESH_ITEM_SELECTED', refreshListDevice );
				PopUpManager.addPopUp(popUpDetails, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//
				
		private function filterFunction(item:Object):Boolean
	    {
		    var rfilter:Boolean = true;		    

		    rfilter = rfilter && ( 
		    						(item.LIBELLE_EQ != null && item.LIBELLE_EQ.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
		    						||
		    						(item.NIVEAU != null && item.NIVEAU.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
		    					 )
		    return rfilter;
		}
	}
}