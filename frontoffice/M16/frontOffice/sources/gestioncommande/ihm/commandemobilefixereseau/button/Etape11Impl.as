package gestioncommande.ihm.commandemobilefixereseau.button
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import commandemobile.popup.sitemodification.GestionSiteEvent;
	import commandemobile.popup.sitemodification.ParametreSiteIHM;
	import commandemobile.popup.sitemodification.Site;
	
	import composants.controls.TextInputLabeled;
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Pool;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.CompteEvent;
	import gestioncommande.ihm.commandemobilefixereseau.viewstacks.VSSelectTypeCommandeFixeReseauIHM;
	import gestioncommande.ihm.commandemobilefixereseau.viewstacks.VSSelectTypeCommandeMobileIHM;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.FicheSiteIHM;
	import gestioncommande.popup.PopUpChargerCommandeIHM;
	import gestioncommande.popup.PopUpChooseCompteIHM;
	import gestioncommande.popup.PopUpSearchSiteIHM;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.CompteService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.PoolService;
	import gestioncommande.services.RevendeurAutoriseService;
	import gestioncommande.services.RevendeurService;
	import gestioncommande.services.SiteService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class Etape11Impl extends AbstractWorflow implements IWorflow
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cbxSites						:ComboBox;
		public var cbxSitesFacturation			:ComboBox;
		public var cbxCmdType					:ComboBox;
		public var cbxRevendeur					:ComboBox;
		public var cbxAgence					:ComboBox;
		public var cbxActeFor					:ComboBox;
		public var cbxContact					:ComboBox;
		public var cbxOperateur					:ComboBox;
		public var cbxTitulaire					:ComboBox;
		public var cbxPointFacturation			:ComboBox;
				
		public var txtLibelle					:TextInput;
		public var txtRefOp						:TextInput;
		public var txtbxTo						:TextInputLabeled;
		
		public var dcLivraisonPrevue			:CvDateChooser;

		public var lblNumCmd					:Label;		
		public var txtDateCommande				:Label;
		
		public var btnNewSite					:Button;
		public var btnModSite					:Button;
		public var btnParcourir					:Button;
		
		public var hbxSelectTypeCommande		:HBox;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUP
		//--------------------------------------------------------------------------------------------//	
		
		private var _popUp						:PopUpChargerCommandeIHM;
		private var _popUpFicheSites			:FicheSiteIHM;
		private var _popupParamSite 			:ParametreSiteIHM;
		private var _popUpSearchSite			:PopUpSearchSiteIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					VUES
		//--------------------------------------------------------------------------------------------//

		private var _viewMobile					:VSSelectTypeCommandeMobileIHM 		= new VSSelectTypeCommandeMobileIHM();
		private var _viewFixRes					:VSSelectTypeCommandeFixeReseauIHM 	= new VSSelectTypeCommandeFixeReseauIHM();

		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _revAutorises				:RevendeurAutoriseService = new RevendeurAutoriseService();
		private var _cmdeServices				:CommandeService = new CommandeService();			
		private var _poolService				:PoolService = new PoolService();
		private var _siteService				:SiteService = new SiteService();
		private var _cpteSrv					:CompteService = new CompteService();
		private var _revService					:RevendeurService = new RevendeurService();

		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//

		public var cmd							:Commande;
		private var _myElements					:ElementsCommande2	= new ElementsCommande2();
		private var _popUpChooseCompte			:PopUpChooseCompteIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					CHAMPS DYNAMIQUE
		//--------------------------------------------------------------------------------------------//
		
		public var hbxChamps1					:HBox;
		public var hbxChamps2					:HBox;
		
		public var LIBELLE_CHAMPS1				:String = ResourceManager.getInstance().getString('M16','R_f_rence_client_1');
		public var LIBELLE_CHAMPS2				:String = ResourceManager.getInstance().getString('M16','R_f_rence_client_2');
		public var EXEMPLE_CHAMPS1				:String = "";
		public var EXEMPLE_CHAMPS2				:String = "";
		
		public var CHAMPS1_ACTIF				:Boolean = false;
		public var CHAMPS2_ACTIF				:Boolean = false;
		public var CHAMPS1_OBLIGATOIRE			:Boolean = false;
		public var CHAMPS2_OBLIGATOIRE			:Boolean = false;
		public var CHAMPS1_EXEMPLE_VISIBLE		:Boolean = false;
		public var CHAMPS2_EXEMPLE_VISIBLE		:Boolean = false;
		
		private var champs1						:ArrayCollection;
		private var champs2						:ArrayCollection;
		
		public var compteName					:String = ResourceManager.getInstance().getString('M16','Compte');
		public var souscompteName				:String = ResourceManager.getInstance().getString('M16','Sous_compte');
	
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES BUTTON CUSSTOM
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE						:int = 1;
		
		public var ACCESS						:Boolean = true;
		
		private var _razl						:Boolean = false;
	
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		//VARIABLES LOCALES PUBLICS
		public var periodeObject				:Object = {rangeStart:new Date(new Date().getTime() - (60 * DateFunction.millisecondsPerDay) )};
		
		public var dateDuJourPlus2JoursOuvrees	:Date			= DateFunction.addXJoursOuvreesToDate(new Date(),2);
		
		public var typeLigne					:String			= "";
		
		public var select						:Boolean		= true;
		public var createLine					:Boolean		= true;
		public var acte							:Boolean		= false;
		
		public var taille						:int 			= 150;
		public var idTypeLigne					:int			= -1;

		//VARIABLES LOCALES PRIVEES
		private var _date						:Date 			= new Date();
		private var _listArticles				:ArrayCollection= new ArrayCollection();
		private var _configObject				:Object 		= new Object();
		private var _boolRevAutorises			:Boolean 		= false;
		
		private var _libelleCompteOp			:ArrayCollection = new ArrayCollection();
		private var _listeComptes				:ArrayCollection = new ArrayCollection();
		private var _compteSelected				:Boolean = false;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
	
		public function Etape11Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
		private function getLibelleProcedure():void
		{
			_cpteSrv.fournirLibelleCompteOperateur();
			_cpteSrv.addEventListener(CommandeEvent.LISTED_LIBELLE_OPE, getLibelleProcedureHandler);
		}
		
		private function getLibelleProcedureHandler(cmde:CommandeEvent):void
		{
			_libelleCompteOp = _cpteSrv.listeLibellesOperateur;
		}
		
		private function findLibelleOperateur():void
		{
			if(cbxOperateur.selectedItem != null)
			{
				var idop	:int = cbxOperateur.selectedItem.OPERATEURID;
				var len 	:int = _libelleCompteOp.length;
				
				for(var i:int = 0; i < len;i++)
				{
					if(_libelleCompteOp[i].OPERATEURID == idop)
					{
						if(_libelleCompteOp[i].COMPTE != null && _libelleCompteOp[i].COMPTE != '')
							compteName 		= _libelleCompteOp[i].COMPTE;
						else
							compteName 		= ResourceManager.getInstance().getString('M16','Compte');;
						
						if(_libelleCompteOp[i].SOUS_COMPTE != null && _libelleCompteOp[i].SOUS_COMPTE != '')
							souscompteName 	= _libelleCompteOp[i].SOUS_COMPTE;
						else
							souscompteName 	= ResourceManager.getInstance().getString('M16','Sous_compte');
						
						return;
					}
				}	
			}
		}

	//--------------------------------------------------------------------------------------------//
	//					LISTENER DE L'IHM
	//--------------------------------------------------------------------------------------------//
		
		public function creationCompleteHandler(fl:FlexEvent):void
		{
			SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 2;
			getLibelleProcedure();
			
			_boolRevAutorises = SessionUserObject.singletonSession.BOOLDISTRIBAUTORISES;
			
			cmd = SessionUserObject.singletonSession.COMMANDE;
			txtDateCommande.text = DateFunction.formatDateAsString(_date);
			
			txtbxTo.text = CvAccessManager.getUserObject().PRENOM + ' ' + CvAccessManager.getUserObject().NOM;
			
			createChampsPerso();
			createViewStackCommander();
			retrieveNewSite();
			
			_poolService.fournirProfilEquipements();
			
			if(!_boolRevAutorises)
				getOperateurs();
			
			_siteService.addEventListener(CommandeEvent.LISTED_SITES, listeSiteLivraisonFacturationHandler);
			
			_poolService.addEventListener(CommandeEvent.LISTED_PROFILES, listeProfilesHandler);
			
			_cmdeServices.addEventListener(CommandeEvent.LISTED_REVENDEURSLA, revendeurSLAHandler);
			
			_revAutorises.addEventListener(CommandeEvent.LISTED_OPEAUTORISES, listeOperateursAutorisesHandler);
			_revAutorises.addEventListener(CommandeEvent.LISTED_REVAUTORISES, listeRevendeurAutorisesHandler);
			
			_cpteSrv.addEventListener(CommandeEvent.LISTED_COMPTES, listeComptesHandler);
			_cpteSrv.addEventListener(CommandeEvent.LISTED_SOUS_COMPTES, listeSousComptesHandler);
			
			_revService.addEventListener(CommandeEvent.LISTED_REVENDEURS, listeRevendeurHandler);
			_revService.addEventListener(CommandeEvent.LISTED_AGENCES, listeAgencesHandler);
			_revService.addEventListener(CommandeEvent.LISTED_CONTACTS, listeContactHandler);	
		}
		
		private function getOperateurs():void
		{
			_revAutorises.fournirListeOperateurs(SessionUserObject.singletonSession.IDSEGMENT);
			_revAutorises.addEventListener(CommandeEvent.LISTED_OPERATEURS, operateursHandler);
		}
		
		private function operateursHandler(cmde:CommandeEvent):void
		{	
			attributDataToCombobox(cbxOperateur, _revAutorises.listeOperateursNormal, ResourceManager.getInstance().getString('M16', 'Aucun_op_rateurs'));
			
			if(_revAutorises.listeOperateursNormal.length == 1)
			{
				dynamicChamps(_revAutorises.listeOperateursNormal[0].OPERATEURID);
				_cpteSrv.fournirCompteOperateurByLoginAndPoolV2(cbxOperateur.selectedItem.OPERATEURID, SessionUserObject.singletonSession.POOL.IDPOOL);
			}
			
			listeSLA();
		}

		public function showHandler(fe:FlexEvent):void
		{
			SessionUserObject.singletonSession.LASTVSINDEX = 0;
		}

		public function viewStackChange():void
		{
		}
		
		public function reset():void
		{
		}
		
		public function setConfiguration():void
		{
		}
		
	//--------------------------------------------------------------------------------------------//
	//					VERIFICATION ET CONTRUCTION DES DONNÉES CHOISIES
	//--------------------------------------------------------------------------------------------//
		
		public function checkData():Boolean
		{
			var bool	:Boolean = false;
			var check	:Boolean = true;
			
			try{
					if(!acte)
						cmd.IDGESTIONNAIRE = CvAccessManager.getSession().USER.CLIENTACCESSID;
					
					if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 4)
					{
						if(SessionUserObject.singletonSession.IDSEGMENT > 1)
						{
							if(_viewFixRes.lblNameConfig.text == '')
								check =  false;
						}
						else
						{
							if(_viewMobile.lblNameConfig.text == '')
								check =  false;
						}
					}

					if(!acte)
					{
						cmd.IDPOOL_GESTIONNAIRE = SessionUserObject.singletonSession.POOL.IDPOOL;
						cmd.IDPROFIL 			= SessionUserObject.singletonSession.POOL.IDPROFIL;
					}
					else
					{
						if(cbxActeFor.selectedItem != null)
						{
							cmd.IDPOOL_GESTIONNAIRE = cbxActeFor.selectedItem.IDPOOL;
							cmd.IDPROFIL 			= cbxActeFor.selectedItem.IDPROFILE;
						}
						else
						{
							cbxActeFor.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_gestionnaire__');
							check =  false;
						}
					}

					if(txtLibelle.text != "")
						cmd.LIBELLE_COMMANDE = txtLibelle.text;
					else
					{
						txtLibelle.errorString = resourceManager.getString('M16','Veuillez_saisir_le_libell__de_la_command');
						check =  false;
					}
					if(check)
						check = checkChamps();
					else
						checkChamps();
					
					if(txtRefOp.text != null)
						cmd.REF_OPERATEUR = txtRefOp.text;
					else
					{
						txtRefOp.errorString = resourceManager.getString('M16','Veuillez_saisir_la_r_f_rence_op_rateur_');
						check =  false;
					}
					
					if(lblNumCmd.text != "")
						cmd.NUMERO_COMMANDE = lblNumCmd.text;
					else
					{
						lblNumCmd.errorString = resourceManager.getString('M16','Pas_de_num_ro_de_commande_');
						check =  false;
					}
					
					if(dcLivraisonPrevue.selectedDate != null)
						cmd.LIVRAISON_PREVUE_LE = dcLivraisonPrevue.selectedDate;
					else
					{
						dcLivraisonPrevue.errorString = resourceManager.getString('M16','Veuillez_saisir_une_date_de_livraison_');
						check =  false;
					}
					
					if(cbxSites.selectedItem != null)
					{
						cmd.LIBELLE_SITELIVRAISON 	= cbxSites.selectedItem.NOM_SITE;
						cmd.IDSITELIVRAISON 		= cbxSites.selectedItem.IDSITE_PHYSIQUE;
					}
					else
					{
						cbxSites.errorString = resourceManager.getString('M16','Veuillez_saisir_un_site_de_livraison_');
						check =  false;
					}
					
					if(cbxSitesFacturation.selectedItem != null)
					{
						cmd.LIBELLE_SITEFACTURATION  = cbxSitesFacturation.selectedItem.NOM_SITE;
						cmd.IDSITEFACTURATION		 = cbxSitesFacturation.selectedItem.IDSITE_PHYSIQUE;
					}
					else
					{
						cbxSitesFacturation.errorString =  resourceManager.getString('M16','Veuillez_saisir_un_site_de_facturation__');
						check =  false;
					}
					
					if(cbxCmdType.selectedItem != null)
						cmd.IDPROFIL_EQUIPEMENT = cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT;
					else
					{
						cbxCmdType.errorString = resourceManager.getString('M16','Veuillez_saisir_un_type_de_commande_');
						check =  false;
					}
					
					if(cbxOperateur.selectedItem != null)
					{
						cmd.IDOPERATEUR 			= cbxOperateur.selectedItem.OPERATEURID;
						cmd.LIBELLE_OPERATEUR 		= cbxOperateur.selectedItem.LIBELLE;
					}
					else
					{
						cbxOperateur.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_op_rateur__');
						check =  false;
					}
					
					if(cbxTitulaire.selectedItem != null)
					{
						cmd.IDCOMPTE_FACTURATION	= cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION;
						cmd.LIBELLE_COMPTE			= cbxTitulaire.selectedItem.COMPTE_FACTURATION;
					}
					else
					{
						if(createLine)
						{
							cbxTitulaire.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_titulaire__');
							check =  false;
						}
						else
							cbxTitulaire.errorString = "";
					}
					
					if(cbxPointFacturation.selectedItem != null)
					{
						cmd.IDSOUS_COMPTE		= cbxPointFacturation.selectedItem.IDSOUS_COMPTE;
						cmd.LIBELLE_SOUSCOMPTE	= cbxPointFacturation.selectedItem.SOUS_COMPTE;
					}
					else
					{
						if(createLine)
						{
							cbxPointFacturation.errorString = resourceManager.getString('M16','Veuillez_saisir_un_point_de_facturation__');
							check =  false;
						}
						else
							cbxPointFacturation.errorString = "";
					}

					if(select)
					{
						if(cbxRevendeur.selectedItem != null)
						{
							cmd.LIBELLE_REVENDEUR 		= cbxRevendeur.selectedItem.LIBELLE;
							cmd.IDSOCIETE 				= cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE;
							cmd.IDREVENDEUR				= cbxRevendeur.selectedItem.IDREVENDEUR;
						}
						else
						{
							cbxRevendeur.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_revendeur_');
							check =  false;
						}
						
						if(cbxAgence.selectedItem != null)
							cbxAgence.errorString = '';
						else
						{
							cbxAgence.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_une_agence_');
							check =  false;
						}
						
						if(cbxContact.selectedItem != null)
						{
							cmd.EMAIL_CONTACT 			= cbxContact.selectedItem.EMAIL;
							cmd.IDCONTACT 				= cbxContact.selectedItem.IDCDE_CONTACT;
						}
					}

					if(acte)
					{
						if(cbxActeFor.selectedItem != null)
						{
							cmd.IDACTEPOUR				= cbxActeFor.selectedItem.APPLOGINID;
							cmd.IDGESTIONNAIRE 			= cbxActeFor.selectedItem.APPLOGINID;
							cmd.IDPOOL_GESTIONNAIRE 	= cbxActeFor.selectedItem.IDPOOL;
						}
						else
						{
							cbxActeFor.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_gestionnaire__');
							check =  false;
						}
						
						if(cbxRevendeur.selectedItem != null)
						{
							cmd.LIBELLE_REVENDEUR 		= cbxRevendeur.selectedItem.LIBELLE;
							cmd.IDSOCIETE 				= cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE;
							cmd.IDREVENDEUR				= cbxRevendeur.selectedItem.IDREVENDEUR;
						}
						else
						{
							cbxRevendeur.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_revendeur_');
							check =  false;
						}
						
						if(cbxContact.selectedItem != null)
						{
							cmd.EMAIL_CONTACT 			= cbxContact.selectedItem.EMAIL;
							cmd.IDCONTACT 				= cbxContact.selectedItem.IDCDE_CONTACT;
						}
					}

					if(check)
					{
						if(select)
						{
							cmd.DATE_COMMANDE 				= _date; 
							cmd.ENCOURS 					= 1;
							cmd.ACTION			 			= "A";
							cmd.IDTYPE_COMMANDE				= addTypeCommande();
							cmd.LIBELLE_TO					= txtbxTo.text;
						}
						else
						{
							cmd.IDPOOL_GESTIONNAIRE 		= _configObject.IDPOOL;
							cmd.DATE_COMMANDE 				= _date; 
							cmd.ENCOURS 					= 1;
							cmd.ACTION						= "A";
							cmd.MONTANT 					= _configObject.MONTANT;
							cmd.IDGROUPE_REPERE 			= _configObject.IDGROUPE_REPERE;
							cmd.IDTYPE_COMMANDE				= _configObject.IDTYPE_OPERATION;
							cmd.IDCONTACT					= _configObject.IDCDE_CONTACT
							cmd.IDREVENDEUR					= _configObject.IDREVENDEUR;
							cmd.REF_OPERATEUR 				= _configObject.REF_REVENDEUR;
							cmd.LIBELLE_TO					= txtbxTo.text;
						}
					}

					if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 4)
					{
						if(SessionUserObject.singletonSession.IDSEGMENT > 1)
						{
							if(_viewFixRes.lblNameConfig.text == "")
							{
								check =  false;
								_viewFixRes.rdbtnChargerConfiguration.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_mod_le');
							}
							else
								_viewFixRes.rdbtnChargerConfiguration.errorString = "";
						}
						else
						{
							if(_viewMobile.lblNameConfig.text == "")
							{
								check =  false;
								_viewMobile.rdbtnChargerConfiguration.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_mod_le');
							}
							else
								_viewMobile.rdbtnChargerConfiguration.errorString = "";
						}
					}

					cmd.V1 = cmd.REF_CLIENT1;
					cmd.V2 = cmd.REF_CLIENT2;
					
					bool = check;
			}
			catch(err:Error)
			{
				bool = false;
			}
			
			if(bool)
				buildParameters();
			
			return bool;
		}
		
		private function addTypeCommande():int
		{
			var idtype_commande:int = -1;
							
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 10)
			{
				switch(SessionUserObject.singletonSession.IDSEGMENT)
				{
					case 1 : idtype_commande = TypesCommandesMobile.EQUIPEMENTS_NUS;
					case 2 : idtype_commande = TypesCommandesMobile.EQU_NUS_FIXE;
					case 3 : idtype_commande = TypesCommandesMobile.EQU_NUS_FIXE;
				}
			}
			else
			{
				switch(SessionUserObject.singletonSession.IDSEGMENT)
				{
					case 1 : idtype_commande = TypesCommandesMobile.NOUVELLES_LIGNES;
					case 2 : idtype_commande = TypesCommandesMobile.NVL_LIGNE_FIXE;
					case 3 : idtype_commande = TypesCommandesMobile.NVL_LIGNE_FIXE;
				}
			}
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 4)
			{
				var elts:ElementsCommande2 = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
				
				if(elts.ABONNEMENTS.length > 0 || elts.OPTIONS.length > 0)
				{
					idtype_commande = TypesCommandesMobile.NVL_LIGNE_FIXE;
					createLine = true;
				}
				else
				{
					idtype_commande = TypesCommandesMobile.EQU_NUS_FIXE;
					createLine = false
				}
			}
			
			return idtype_commande;
		}
		
		public function buildParameters():void
		{
			SessionUserObject.singletonSession.COMMANDE = cmd;
		}

//--------------------------------------------------------------------------------------------//
//					FUNCTION PROPRE A L'ETAPE 
//--------------------------------------------------------------------------------------------//
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//

		protected function txtfocusIn(txtiptNumber:int):void
		{
			switch(txtiptNumber)
			{
				case 0: txtLibelle.errorString 	= "";break;
				case 1: txtRefOp.errorString 	= "";break;
			}
		}
		
		protected function lblclickHandler(lblNumber:int):void
		{
			switch(lblNumber)
			{
				case 0: txtLibelle.text 			= cmd.NUMERO_COMMANDE;
						txtLibelle.errorString 		= "";
						break;
				case 1: insertNumeroCommandeInTextbox();
						break;
			}
		}
		
		protected function btnNewSiteClickHandler(e:Event):void
		{
			_popUpFicheSites = new FicheSiteIHM();
			_popUpFicheSites.idPoolGestionnaire = SessionUserObject.singletonSession.POOL.IDPOOL;
			_popUpFicheSites.addEventListener("refreshSite", retrieveNewSite);
			PopUpManager.addPopUp(_popUpFicheSites, this, true);
			PopUpManager.centerPopUp(_popUpFicheSites);
		}
		
		protected function btnModSiteClickHandler(e:Event):void
		{
			_popupParamSite 		= new ParametreSiteIHM();
			_popupParamSite.site 	= mappingSiteSelectedToSite();
			_popupParamSite.addEventListener(GestionSiteEvent.PARAMETRE_SITE_COMPLETE, retrieveNewSite);
			PopUpManager.addPopUp(_popupParamSite, this, true);
			PopUpManager.centerPopUp(_popupParamSite);
		}

		private function retrieveNewSite(e:Event = null):void
		{
			var object:Object = SessionUserObject.singletonSession;
			
			_siteService.fournirListeSiteLivraisonsFacturation(SessionUserObject.singletonSession.POOL.IDPOOL);
		}
		
		protected function cbxCmdTypeChangeHandler(e:Event):void
		{
			if(cbxCmdType.selectedItem != null)
			{
				SessionUserObject.singletonSession.IDPROFILESELECETED = cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT;
				
				if(_boolRevAutorises)
				{
					cbxOperateur.dataProvider 			= null;
					cbxTitulaire.dataProvider 			= null;
					cbxPointFacturation.dataProvider 	= null;
					cbxRevendeur.dataProvider 			= null;
					cbxAgence.dataProvider 				= null;
					cbxContact.dataProvider 			= null;
					
					cbxOperateur.prompt 		= "";
					cbxTitulaire.prompt 		= "";
					cbxPointFacturation.prompt 	= "";
					cbxRevendeur.prompt 		= "";
					cbxAgence.prompt 			= "";
					cbxContact.prompt 			= "";
					
					_revAutorises.fournirOperateursAutorises(cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT);
				}
				else
				{
					cbxRevendeur.dataProvider 			= null;
					cbxAgence.dataProvider 				= null;
					cbxContact.dataProvider 			= null;
	
					cbxRevendeur.prompt 		= "";
					cbxAgence.prompt 			= "";
					cbxContact.prompt 			= "";
					
					_revService.fournirRevendeurs(SessionUserObject.singletonSession.POOL.IDPOOL,
													cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT,
														SessionUserObject.singletonSession.POOL.IDREVENDEUR);
				}
				
				dispatchEvent(new Event('PROFILE_CHANGED', true));
				
				cbxCmdType.errorString = "";
			}
			else
			{
				SessionUserObject.singletonSession.IDPROFILESELECETED = -1;
				cbxCmdType.errorString = ResourceManager.getInstance().getString('M16', 'ERROR');
			}
		}
		
		protected function cbxSitesChangeHandler(e:Event):void
		{
			if(cbxSites.selectedItem != null)
			{
				var sites	:ArrayCollection = cbxSitesFacturation.dataProvider as ArrayCollection;
				var len		:int 			 = sites.length;
				var isHere	:Boolean		 = false;
				
				if(sites == null || sites[0] == null)
					return;
				
				for(var i:int = 0;i <len;i++)
		    	{
		    		if(sites[i].IDSITE_PHYSIQUE == cbxSites.selectedItem.IDSITE_PHYSIQUE)
		    		{
		    			cbxSitesFacturation.selectedIndex 	= i;
		    			cbxSitesFacturation.errorString 	= "";
		    			isHere = true;
		    			break;
		    		}
		    	}
		    	
		    	cbxSites.errorString = "";
		    	
		    	if(!isHere)
		    	{
					if(cbxSitesFacturation.selectedItem == null)
						cbxSitesFacturation.selectedIndex = -1;
		    	}
			}
			else
				cbxSites.errorString = ResourceManager.getInstance().getString('M16', 'ERROR');
		}
		
		protected function imgSearchClickHandler(me:MouseEvent):void
		{
			var composant		:Image = me.currentTarget as Image;
			var currentData		:ArrayCollection = new ArrayCollection();
			
			_popUpSearchSite = new PopUpSearchSiteIHM();
			
			if(composant != null && composant.id == 'imgSearchL')
			{
				_popUpSearchSite.setInfos(ObjectUtil.copy(cbxSites.dataProvider as ArrayCollection) as ArrayCollection, cbxSites.selectedItem, true);
			}
			else if(composant != null && composant.id == 'imgSearchF')
			{
				_popUpSearchSite.setInfos(ObjectUtil.copy(cbxSitesFacturation.dataProvider as ArrayCollection) as ArrayCollection, cbxSitesFacturation.selectedItem, false);
			}
			
			if(composant != null)
			{
				_popUpSearchSite.addEventListener('SEARCH_SITE_SELECTED', searchSiteHandler);
				
				PopUpManager.addPopUp(_popUpSearchSite, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(_popUpSearchSite);
			}
		}
		
		private function searchSiteHandler(e:Event):void
		{
			var mySiteSelected	:Object = _popUpSearchSite.getInfos();			
			var lenSitesL		:int = (cbxSites.dataProvider as ArrayCollection).length;
			var lenSitesF		:int = (cbxSitesFacturation.dataProvider as ArrayCollection).length;
			var i				:int = 0;
			
			if(_popUpSearchSite.isSiteLivraison)
			{
				for(i = 0;i < lenSitesL;i++)
				{
					if((cbxSites.dataProvider as ArrayCollection)[i].IDSITE_PHYSIQUE == mySiteSelected.IDSITE_PHYSIQUE)
					{
						cbxSites.selectedIndex = i;
						
						break;
					}
				}
				
				cbxSitesChangeHandler(e);
			}
			else
			{
				for(i = 0;i < lenSitesF;i++)
				{
					if((cbxSitesFacturation.dataProvider as ArrayCollection)[i].IDSITE_PHYSIQUE == mySiteSelected.IDSITE_PHYSIQUE)
					{
						cbxSitesFacturation.selectedIndex = i;
						
						break;
					}
				}
			}
			
			PopUpManager.removePopUp(_popUpSearchSite);
		}

		protected function cbxSitesFacturationChangeHandler(e:Event):void
		{
			if(cbxSitesFacturation.selectedItem != null)
				cbxSitesFacturation.errorString = "";
			else
				cbxSitesFacturation.errorString = ResourceManager.getInstance().getString('M16', 'ERROR');
		}
		
		protected function cbxOperateurChangeHandler(e:Event):void
		{
			if(cbxOperateur.selectedItem != null)
			{
				cbxPointFacturation.dataProvider 	= null;
				cbxTitulaire.dataProvider 			= null;

				cbxPointFacturation.prompt 	= "";
				cbxTitulaire.prompt 		= "";
				
				if(_boolRevAutorises)
				{
					cbxRevendeur.dataProvider 	= null;
					cbxAgence.dataProvider 		= null;
					cbxContact.dataProvider 	= null;

					cbxRevendeur.prompt 		= "";
					cbxAgence.prompt 			= "";
					cbxContact.prompt 			= "";
					
					_revAutorises.fournirRevendeursAutorises(cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT, 
																cbxOperateur.selectedItem.OPERATEURID,
																	SessionUserObject.singletonSession.POOL.IDREVENDEUR);
				}
				
				listeSLA();
				
				_cpteSrv.fournirCompteOperateurByLoginAndPoolV2(cbxOperateur.selectedItem.OPERATEURID, 
																		SessionUserObject.singletonSession.POOL.IDPOOL);
				cbxOperateur.errorString = "";
				
				dispatchEvent(new Event('OPERATEUR_CHANGED', true));
			}
			else
			{
				cbxOperateur.errorString = ResourceManager.getInstance().getString('M16', 'ERROR');
			}
			
			if(cbxOperateur.selectedItem != null)
				dynamicChamps(cbxOperateur.selectedItem.OPERATEURID);
		}
		
		protected function cbxTitulaireChangeHandler(e:Event):void
		{
			if(cbxTitulaire.selectedItem != null)
			{
				cbxPointFacturation.dataProvider 	= null;
				
				cbxPointFacturation.prompt 			= "";
				cbxTitulaire.errorString 			= "";
				
				getSousComptes();
			}
			else
				cbxTitulaire.errorString = ResourceManager.getInstance().getString('M16', 'ERROR');
		}
		
		protected function cbxPointFacturationChangeHandler(e:Event):void
		{
			if(cbxPointFacturation.selectedItem != null)
				cbxPointFacturation.errorString = "";
			else
				cbxPointFacturation.errorString = ResourceManager.getInstance().getString('M16', 'ERROR');
		}
		
		protected function cbxRevendeurChangeHandler(e:Event):void
		{
			if(cbxRevendeur.selectedItem != null)
			{
				cbxAgence.dataProvider 		= null;
				cbxContact.dataProvider 	= null;
				
				cbxAgence.prompt 			= "";
				cbxContact.prompt 			= "";
				
				_revService.fournirAgencesRevendeur(cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE);
				_revService.fournirContactsRevendeur(cbxRevendeur.selectedItem.IDREVENDEUR, cmd.IDROLE);
				
				cbxRevendeur.errorString = "";
				
				dispatchEvent(new Event('REVENDEUR_CHANGED', true));
				
				listeSLA();
			}
			else
				cbxRevendeur.errorString = ResourceManager.getInstance().getString('M16', 'ERROR');
		}
		
		protected function cbxAgenceChangeHandler(e:Event):void
		{
			if(cbxAgence.selectedItem != null)
			{
				_revService.fournirContactsRevendeur(cbxRevendeur.selectedItem.IDREVENDEUR, cmd.IDROLE);
				cbxAgence.errorString = "";
			}
			else
				cbxAgence.errorString = ResourceManager.getInstance().getString('M16', 'ERROR');
		}
		
		protected function cbxActeForChangeHandler(e:Event):void
		{
			if(cbxActeFor.selectedItem != null)
				cbxActeFor.errorString = "";
			else
				cbxActeFor.errorString = ResourceManager.getInstance().getString('M16', 'ERROR');
		}

	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PUBLIC
	//--------------------------------------------------------------------------------------------//

		public function disableAll(isEnbabled:Boolean = false):void
		{
			cbxSites.enabled			= isEnbabled;
			cbxSitesFacturation.enabled = isEnbabled;
			cbxCmdType.enabled 			= isEnbabled;
			cbxRevendeur.enabled 		= isEnbabled;
			cbxAgence.enabled 			= isEnbabled;
			cbxActeFor.enabled 			= isEnbabled;
			cbxContact.enabled 			= isEnbabled;
			cbxOperateur.enabled 		= isEnbabled;
			cbxTitulaire.enabled 		= isEnbabled;
			cbxPointFacturation.enabled = isEnbabled;
			txtLibelle.enabled 			= isEnbabled;
			txtRefOp.enabled 			= isEnbabled;
			txtbxTo.enabled 			= isEnbabled;
			dcLivraisonPrevue.enabled 	= isEnbabled;
			lblNumCmd.enabled 			= isEnbabled;
			btnNewSite.enabled 			= isEnbabled;
			btnModSite.enabled 			= isEnbabled;
		}
	
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					CHAMPS PERSONNALISABLE
		//--------------------------------------------------------------------------------------------//
		
		//---CREATION DYNAMIQUE DES CHAMPS PERSONNALISABLE
		private function createChampsPerso():void
		{
			var i:int = 0;

			champs1 		= SessionUserObject.singletonSession.CHAMPS.CHAMPS1;
			champs2 		= SessionUserObject.singletonSession.CHAMPS.CHAMPS2;
			
			if(champs1 != null && champs2 != null)
			{ 
				LIBELLE_CHAMPS1 = SessionUserObject.singletonSession.CHAMPS.LIBELLE1;
				EXEMPLE_CHAMPS1 = SessionUserObject.singletonSession.CHAMPS.EXEMPLE1;
				CHAMPS1_ACTIF   = SessionUserObject.singletonSession.CHAMPS.ACTIVE1 as Boolean;
				LIBELLE_CHAMPS2 = SessionUserObject.singletonSession.CHAMPS.LIBELLE2;
				EXEMPLE_CHAMPS2 = SessionUserObject.singletonSession.CHAMPS.EXEMPLE2;
				CHAMPS2_ACTIF 	= SessionUserObject.singletonSession.CHAMPS.ACTIVE2 as Boolean;
			}
			
			if(EXEMPLE_CHAMPS1 == "")
				CHAMPS1_EXEMPLE_VISIBLE = false;
			else
				CHAMPS1_EXEMPLE_VISIBLE = true;
			
			if(EXEMPLE_CHAMPS2 == "")
				CHAMPS2_EXEMPLE_VISIBLE = false;
			else
				CHAMPS2_EXEMPLE_VISIBLE = true;
			
			if(SessionUserObject.singletonSession.CHAMPS.hbxChamps1 != null)
			{
				var nbrChild1	:int  	= (SessionUserObject.singletonSession.CHAMPS.hbxChamps1 as HBox).numChildren;
				var hbx1		:HBox	= SessionUserObject.singletonSession.CHAMPS.hbxChamps1 as HBox;
				var arrayTemp1	:Array 	= copyChampsPerso((hbx1 as HBox).getChildren());
				var nbrTemp1	:int 	= arrayTemp1.length;
				
				for(i = 0;i < nbrTemp1;i++)
				{
					var libelle1:String = arrayTemp1[i].text;
					hbxChamps1.addChild(arrayTemp1[i]);
					(hbxChamps1.getChildAt(i) as TextArea).text = libelle1;
				}
			}

			if(SessionUserObject.singletonSession.CHAMPS.hbxChamps2 != null)
			{
				var nbrChild2	:int  	= (SessionUserObject.singletonSession.CHAMPS.hbxChamps2 as HBox).numChildren;
				var hbx2		:HBox	= SessionUserObject.singletonSession.CHAMPS.hbxChamps2 as HBox;
				var arrayTemp2	:Array 	= copyChampsPerso((hbx2 as HBox).getChildren());
				var nbrTemp2	:int 	= arrayTemp2.length;
				
				for(i = 0;i < nbrTemp2;i++)
				{
					var libelle2:String = arrayTemp2[i].text;
					hbxChamps2.addChild(arrayTemp2[i]);
					(hbxChamps2.getChildAt(i) as TextArea).text = libelle2;
				}
			}
		}
		
		private function copyChampsPerso(champs:Array):Array
		{
			var newChildren	:ArrayCollection = new ArrayCollection();
			var lenChildren	:int = champs.length;
			
			for(var i:int = 0;i < lenChildren;i++)
			{
				var txtarea:TextArea 	= new TextArea();				
					txtarea.width		= (champs[i] as TextArea).width;
					txtarea.height		= (champs[i] as TextArea).height;
					txtarea.restrict	= (champs[i] as TextArea).restrict;
					txtarea.maxChars	= (champs[i] as TextArea).maxChars;
					txtarea.id			= (champs[i] as TextArea).id;
					txtarea.text		= (champs[i] as TextArea).text;
					txtarea.editable	= (champs[i] as TextArea).editable;
					txtarea.selectable	= (champs[i] as TextArea).selectable;	
					txtarea.addEventListener(Event.CHANGE, textinputChangeHandler);									
				
				if(txtarea != null)
					newChildren.addItem(txtarea);
				else
					newChildren.addItem(champs[i]);
			}
			
			return newChildren.source;
		}
		
		private function textinputChangeHandler(e:Event):void
		{
			(e.currentTarget as TextArea).errorString = "";
		}
		
		public function autoCompleteRefClient():void
		{
			if(SessionUserObject.singletonSession.CHAMPS.AUTO1)
				insertNumeroCommandeInTextbox();
			
			if(SessionUserObject.singletonSession.CHAMPS.AUTO2)
				insertNumeroCommandeInTextbox2();
		}

		//VERIFICATION DES CHAMPS PERSONNALISABLE
		private function checkChamps():Boolean
		{
			var OK				:Boolean = true;
			var i				:int 	 = 0;
			var childChamps1 	:Array 	 = hbxChamps1.getChildren();
			var childChamps2 	:Array 	 = hbxChamps2.getChildren();
			
			if(CHAMPS1_ACTIF)
			{
				for(i = 0;i < champs1.length;i++)
				{
					if(champs1[i].SAISIE_ZONE)
					{
						if((childChamps1[i*2] as TextArea).text == "")
						{
							(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
							
							if(OK)
								OK = false;
						}
						else
						{
							if(champs1[i].EXACT_NBR)
							{
								if((childChamps1[i*2] as TextArea).text.length != champs1[i].COMPTEUR_OBLIG)
								{
									(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs1[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps1[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
					else
					{
						if((childChamps1[i*2] as TextArea).text == "")
						{
							if(OK)
								OK = true;
						}
						else
						{
							if(champs1[i].EXACT_NBR)
							{
								if((childChamps1[i*2] as TextArea).text.length != champs1[i].COMPTEUR_OBLIG)
								{
									(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs1[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps1[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
				}
			}
			
			if(CHAMPS2_ACTIF)
			{
				for(i = 0;i < champs2.length;i++)
				{
					if(champs2[i].SAISIE_ZONE)
					{
						if((childChamps2[i*2] as TextArea).text == "")
						{
							(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
							
							if(OK)
								OK = false;
						}
						else
						{
							if(champs2[i].EXACT_NBR)
							{
								if((childChamps2[i*2] as TextArea).text.length != champs2[i].COMPTEUR_OBLIG)
								{
									(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs2[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps2[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
					else
					{
						if((childChamps2[i*2] as TextArea).text == "")
						{
							if(OK)
								OK = true;
						}
						else
						{
							if(champs2[i].EXACT_NBR)
							{
								if((childChamps2[i*2] as TextArea).text.length != champs2[i].COMPTEUR_OBLIG)
								{
									(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs2[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps2[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
				}
			}
			
			if(OK)
				attributDataToCommandeObject();
			
			return OK;
		}
		
		//---ATTRIBUTION DES DONNEES SAISIE DANS LES CHAMPS PERSONNALISABLE PUIS AFFECTATION DES DONNEES A L'OBJET COMMANDE
		private function attributDataToCommandeObject():void
		{
			var i				:int 	= 0;
			var childChamps1 	:Array 	= hbxChamps1.getChildren();
			var childChamps2 	:Array 	= hbxChamps2.getChildren();
			var reference1		:String	= "";
			var reference2		:String	= "";
			
			for(i = 0;i < childChamps1.length;i++)
			{
				reference1 = reference1 + childChamps1[i].text;
			}
			
			for(i = 0;i < childChamps2.length;i++)
			{
				reference2 = reference2 + childChamps2[i].text;
			}
			
			cmd.REF_CLIENT1 = reference1;
			cmd.REF_CLIENT2 = reference2;
		}
		
		private function insertNumeroCommandeInTextbox():void
		{
			var childChamps1 	:Array 	 = hbxChamps1.getChildren();
			var lenChild	 	:int 	 = childChamps1.length;
			var lenNumCmd		:int 	 = cmd.NUMERO_COMMANDE.length;
			var cptr			:int	 = -1
			var lenTxtIpt		:int 	 = -1;
			var i				:int 	 = 0;
						
			for(i = 0; i < lenChild; i++)
			{
				cptr = i*2;
				
				if(cptr < lenChild)
				{
					lenTxtIpt = (childChamps1[cptr] as TextArea).maxChars;
					
					if(lenNumCmd <= lenTxtIpt)
					{
						(childChamps1[cptr] as TextArea).text 		= cmd.NUMERO_COMMANDE;
						(childChamps1[cptr] as TextArea).errorString = "";
					}
				}
			}
		}
		
		private function insertNumeroCommandeInTextbox2():void
		{
			var childChamps2 	:Array 	 = hbxChamps2.getChildren();
			var lenChild	 	:int 	 = childChamps2.length;
			var lenNumCmd		:int 	 = cmd.NUMERO_COMMANDE.length;
			var cptr			:int	 = -1
			var lenTxtIpt		:int 	 = -1;
			var i				:int 	 = 0;
			
			for(i = 0; i < lenChild; i++)
			{
				cptr = i*2;
				
				if(cptr < lenChild)
				{
					lenTxtIpt = (childChamps2[cptr] as TextArea).maxChars;
					
					if(lenNumCmd <= lenTxtIpt)
					{
						(childChamps2[cptr] as TextArea).text 		= cmd.NUMERO_COMMANDE;
						(childChamps2[cptr] as TextArea).errorString = "";
					}
				}
			}
		}
		
		private function createViewStackCommander():void
		{
			if(SessionUserObject.singletonSession.IDSEGMENT > 1)
				hbxSelectTypeCommande.addChild(_viewFixRes);
			else
				hbxSelectTypeCommande.addChild(_viewMobile);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					OPERATEUR DYNAMIQUE
		//--------------------------------------------------------------------------------------------//	
		
		//---AFFICHE LE LIBELLE DU COMPTE/SOUS COMPTE SUIVANT L'OPERATEUR SELECTIONNE
		private function dynamicChamps(idOperateur:int):void
		{
			compteName		= resourceManager.getString('M16','Compte');
			souscompteName 	= resourceManager.getString('M16','Sous_compte');
			
			findLibelleOperateur();
		}
		
		//---RECUPERE LA LISTE DE HEURE DE PRISE EN COMPTE DE COMMANDE AINSI QUE DES DELAIS POUR LA LIVRAISON DE LA COMMANDE
		private function listeSLA():void
		{
			if(cbxOperateur.selectedItem != null && cbxRevendeur.selectedItem != null)
				_cmdeServices.fournirRevendeurSLA(cbxRevendeur.selectedItem.IDREVENDEUR, cbxOperateur.selectedItem.OPERATEURID);
		}
		
		//---FAIS UN MAPPING ENTRE LE SITE SELECTIONNE DANS LA COMBOBOX ET L'OBJET SITE
		private function mappingSiteSelectedToSite():Site
		{
			var site:Site 				= new Site();
				site.cp_site 			= cbxSites.selectedItem.SP_CODE_POSTAL;
				site.adr_site 			= cbxSites.selectedItem.ADRESSE;
				site.id_site			= cbxSites.selectedItem.IDSITE_PHYSIQUE;
				site.ref_site 			= cbxSites.selectedItem.SP_REFERENCE;
				site.code_interne_site 	= cbxSites.selectedItem.SP_CODE_INTERNE;
				site.commentaire_site 	= cbxSites.selectedItem.SP_COMMENTAIRE;
				site.commune_site 		= cbxSites.selectedItem.SP_COMMUNE;
				site.libelle_site 		= cbxSites.selectedItem.NOM_SITE;
				site.idpays_site		= cbxSites.selectedItem.PAYCONSOTELID;
				site.IS_FACTURATION		= Formator.formatInteger(cbxSites.selectedItem.IS_FACTURATION);
				site.IS_LIVRAISON		= Formator.formatInteger(cbxSites.selectedItem.IS_LIVRAISON);
			
			return site;
		}
		
		//---ATTRIBUT LES DONNEES A LA COMBOBOX PASSEE EN PARAMETRE
		private function attributDataToCombobox(cbx:ComboBox, dataValues:ArrayCollection, msgError:String):void
		{
			var lenDataValues:int = dataValues.length;
			
			cbx.errorString = "";
			
			if(lenDataValues != 0)
			{
				cbx.prompt 					= ResourceManager.getInstance().getString('M16', 'S_lectionnez');
				cbx.dataProvider 			= dataValues;
				cbx.dropdown.dataProvider 	= dataValues;

				if(lenDataValues > 1)
					cbx.selectedIndex = -1;
				else
					cbx.selectedIndex = 0;
				
				if(lenDataValues > 5)
					cbx.rowCount = 5;
				else
					cbx.rowCount = lenDataValues;
				
				findLibelleOperateur();
			}
			else
				cbx.prompt = msgError;
		}
		
		private function getSousComptes():void
		{
			var listeSSCPTE:ArrayCollection = _cpteSrv.searchSousCompteCorrespondant(cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION);
			
			attributDataToCombobox(cbxPointFacturation, listeSSCPTE, ResourceManager.getInstance().getString('M16', 'Aucun_sous_comptes'));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER OBJECT
		//--------------------------------------------------------------------------------------------//
		
		private function revendeurSLAHandler(cmde:CommandeEvent):void
		{
			if(_cmdeServices.revendeurSLA != null)
			{
				var hoursParsing	:Object = Formator.formatHour(_date);
				var delaiTime		:Array 	= (_cmdeServices.revendeurSLA.DELAI_HOUR_LIVRAISON as String).split(ResourceManager.getInstance().getString('M16', '_'));
				var delai			:int 	= Number(_cmdeServices.revendeurSLA.DELAI_LIVRAISON);

				if(Number(hoursParsing.HOURS) > Number(delaiTime[0]))
					delai = delai + 1;
				else
				{
					if(Number(hoursParsing.HOURS) == Number(delaiTime[0]))
					{
						if(Number(hoursParsing.MIN) > Number(delaiTime[1]))
							delai = delai + 1;
					}
				}

				dcLivraisonPrevue.selectedDate 		= DateFunction.addXJoursOuvreesToDate(new Date(), delai);
				dcLivraisonPrevue.selectableRange 	= {rangeStart:new Date(new Date().getTime() + (delai * DateFunction.millisecondsPerDay))};
			}
			else
			{
				dcLivraisonPrevue.selectedDate 		= DateFunction.addXJoursOuvreesToDate(new Date(), 2);
				dcLivraisonPrevue.selectableRange 	= {rangeStart:new Date(new Date().getTime() + (2 * DateFunction.millisecondsPerDay))};
			}
		}
		
		private function listeSiteLivraisonFacturationHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxSites, _siteService.listeSitesLivraison, ResourceManager.getInstance().getString('M16', 'Aucun_site'));
			attributDataToCombobox(cbxSitesFacturation, _siteService.listeSitesFacturation, ResourceManager.getInstance().getString('M16', 'Aucun_site'));
		}
		
		private function listeProfilesHandler(cmde:CommandeEvent):void
		{	
			var listeProfiles	:ArrayCollection 	= _poolService.listeProfiles;
			var lenProfiles		:int				= listeProfiles.length
			
			attributDataToCombobox(cbxCmdType, listeProfiles, ResourceManager.getInstance().getString('M16', 'Aucun_profile'));
			
			if(lenProfiles == 1)
				SessionUserObject.singletonSession.IDPROFILESELECETED = cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT;
			
			if(_boolRevAutorises && lenProfiles == 1)
			{
				_revAutorises.fournirOperateursAutorises(cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT);
			}
			else if(!_boolRevAutorises && lenProfiles == 1)
			{
				_revService.fournirRevendeurs(SessionUserObject.singletonSession.POOL.IDPOOL,
													cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT,
														SessionUserObject.singletonSession.POOL.IDREVENDEUR);
			}
		}
		
		private function listeOperateursAutorisesHandler(cmde:CommandeEvent):void
		{
			var listeOperateurAut	:ArrayCollection 	= _revAutorises.listeOperateurs;
			var lenOperateurAut		:int 				= listeOperateurAut.length;
			
			attributDataToCombobox(cbxOperateur, listeOperateurAut, ResourceManager.getInstance().getString('M16', 'Aucun_op_rateur'));
			
			if(lenOperateurAut == 1)
			{
				_revAutorises.fournirRevendeursAutorises(cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT, 
															cbxOperateur.selectedItem.OPERATEURID,
															SessionUserObject.singletonSession.POOL.IDREVENDEUR);
				_cpteSrv.fournirCompteOperateurByLoginAndPoolV2(cbxOperateur.selectedItem.OPERATEURID, 
																		SessionUserObject.singletonSession.POOL.IDPOOL);
			}
			
			listeSLA();
		}
		
		private function listeRevendeurAutorisesHandler(cmde:CommandeEvent):void
		{
			var listeRevendeurAut	:ArrayCollection 	= _revAutorises.listeRevendeurs;
			var lenRevendeurAut		:int 				= listeRevendeurAut.length;
			
			acte = searchPoolRevendeur(listeRevendeurAut);
			attributDataToCombobox(cbxRevendeur, listeRevendeurAut, ResourceManager.getInstance().getString('M16', 'Aucun_revendeur'));
			
			if(lenRevendeurAut == 1)
			{
				_revService.fournirAgencesRevendeur(cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE);
				_revService.fournirContactsRevendeur(cbxRevendeur.selectedItem.IDREVENDEUR, cmd.IDROLE);
			}
			
			listeSLA();
		}
		
		private function searchPoolRevendeur(listeRevendeur:ArrayCollection):Boolean
		{
			var values	:ArrayCollection = new ArrayCollection(listeRevendeur.source);
			var bool	:Boolean 		 = false;
			var pool	:Pool 	 		 = SessionUserObject.singletonSession.POOL;
			var len		:int	 		 = listeRevendeur.length;
	
			if(pool.IDREVENDEUR > 0)
			{
				for(var i:int = 0;i < len;i++)
				{
					if(listeRevendeur[i].IDREVENDEUR != pool.IDREVENDEUR)
					{
						listeRevendeur.removeItemAt(i);
						i--;
						len--;
					}
					else
						bool = true;
				}
				
				if(listeRevendeur.length == 0)
				{
					bool = false;
					listeRevendeur = values;
				}
			}
			
			return bool;
		}
		
		private function listeComptesHandler(cmde:CommandeEvent):void
		{
			listeComptes.source = [];
			listeComptes = _cpteSrv.listeComptesSousComptes;//Formator.sortFunction(_cpteSrv.listeComptesSousComptes, 'COMPTE_FACTURATION');
			var lenComptes		:int = listeComptes.length;
			
			attributDataToCombobox(cbxTitulaire, listeComptes, ResourceManager.getInstance().getString('M16', 'Aucun_') + compteName);

			if(lenComptes == 1)
				getSousComptes();
		}
		
		private function listeSousComptesHandler(cmde:CommandeEvent):void 
		{
			attributDataToCombobox(cbxPointFacturation, _cpteSrv.listeSousComptes, ResourceManager.getInstance().getString('M16', 'Aucun_') + souscompteName);
		}
		
		private function listeRevendeurHandler(cmde:CommandeEvent):void
		{
			var listeRevendeur	:ArrayCollection 	= _revService.listeRevendeurs;
			var lenRevendeur	:int 				= listeRevendeur.length;
			
			acte = searchPoolRevendeur(listeRevendeur);
			attributDataToCombobox(cbxRevendeur, listeRevendeur, ResourceManager.getInstance().getString('M16', 'Aucun_revendeur'));
			
			if(lenRevendeur == 1)
			{
				_revService.fournirAgencesRevendeur(cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE);
				_revService.fournirContactsRevendeur(cbxRevendeur.selectedItem.IDREVENDEUR, cmd.IDROLE);
			}
			
			listeSLA();
		}
		
		private function listeAgencesHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxAgence, _revService.listeAgences, ResourceManager.getInstance().getString('M16', 'Aucune_agence'));
		}
		
		private function listeContactHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxContact, _revService.listeContacts, ResourceManager.getInstance().getString('M16', 'Aucun_contact'));
		}
		
		public function btImgChooseCompteClickHandler(e:Event):void
		{
			if(cbxOperateur.selectedIndex >-1)
			{
				if(listeComptes.length > 0)
				{
					this.majSelectCompte(listeComptes);
					_popUpChooseCompte = new PopUpChooseCompteIHM();
					_popUpChooseCompte.listeComptes = listeComptes;
					
					_popUpChooseCompte.addEventListener(CompteEvent.VALID_CHOIX_COMPTE, getItemCompteHandler);
					PopUpManager.addPopUp(_popUpChooseCompte, this, true);
				}
				else
				{
					cbxTitulaire.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_titulaire__');
				}
			}
			else
			{
				cbxOperateur.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_op_rateur__');
			}
		}
		
		public function getItemCompteHandler(ce:CompteEvent):void
		{
			_popUpChooseCompte.removeEventListener(CompteEvent.VALID_CHOIX_COMPTE, getItemCompteHandler);
			
			for (var j:int = 0; j < listeComptes.length; j++) 
			{
				if((listeComptes[j].IDCOMPTE_FACTURATION) ==(ce.idCompte))
				{
					cbxTitulaire.selectedIndex = j;
					break;
				}
			}
			
			cbxTitulaireChangeHandler(null);
		}
		
		
		
		private function majSelectCompte(_comptes:ArrayCollection):void
		{
			var longPool:int = _comptes.length;
			var i:int = 0;
			while(i < longPool)
			{
				_comptes[i].SELECTED = false;
				if((cbxTitulaire.selectedItem != null) && _comptes[i].IDCOMPTE_FACTURATION == cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION)
				{
					_comptes[i].SELECTED = true;
					_compteSelected = true;
				}
				
				i++;
			}
			
		}
		
		public function get listeComptes():ArrayCollection { return _listeComptes; }
		
		public function set listeComptes(value:ArrayCollection):void
		{
			if (_listeComptes == value)
				return;
			_listeComptes = value;
		}

	}
}