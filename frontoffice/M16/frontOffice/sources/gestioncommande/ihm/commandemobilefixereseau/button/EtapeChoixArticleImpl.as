package gestioncommande.ihm.commandemobilefixereseau.button
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	
	import gestioncommande.entity.ArticleItemBaseVO;
	import gestioncommande.entity.ArticleItemVO;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.events.ArticleEvent;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.services.article.ArticleService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class EtapeChoixArticleImpl extends AbstractWorflow implements IWorflow
	{
		public const SEGMENT				:int = 3; 
		private var _articleService 		:ArticleService;
		private var _enteredOrders			:ArrayCollection;
		private var _listeArticlesChoisis	:ArrayCollection;
		private var _sommeTotaleStr			:String;
		private var _firstProfil			:Number = -1;
		public var dg_ListeArticle			:DataGrid;
		
		public const IDPAGE					:int = 2;
		public var ACCESS					:Boolean = true;
		private var _razl					:Boolean = false;
		
		public var cmd						:Commande;
		private var _myElements				:ElementsCommande2	= new ElementsCommande2();
		
		
		public function EtapeChoixArticleImpl()
		{
			this.init();
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		public function creationCompleteHandler(fl:FlexEvent):void
		{
			_articleService= new ArticleService();
			cmd = SessionUserObject.singletonSession.COMMANDE;
			initData();
			initListeners();
		}
		
		private function initListeners():void
		{
		}
		
		private function initData():void
		{
		}
		
		protected function getListArticlesHandler(event:ArticleEvent):void
		{
			var long:int = enteredOrders.length;
			// Ajout du premier choix d'article dans le dataGrid.
			this.ajouterLigneArticle();
			
			_articleService.myDatas.removeEventListener(ArticleEvent.LISTED_ARTICLES, getListArticlesHandler);
		}
		
		public function ajouterLigneArticle():void
		{
			var dp:ArrayCollection = _articleService.myDatas.listeArticles;
			var longRes:uint = dp.length;
			
			if(enteredOrders != null)
			{
				var nvChoix:ArticleItemVO = new ArticleItemVO();
				
				nvChoix.listReference = dp;
				
				enteredOrders.addItemAt(nvChoix, enteredOrders.length - 1);
				this.calculSommeTotale();
			}
		}
		
		public function calculSommeTotale():void
		{
			var longDP:uint = enteredOrders.length;
			
			var sommeTotale:Number = 0;
			for(var i:uint=0; i<longDP; i++)
			{
				if(enteredOrders[i].isItem)
				{
					sommeTotale = sommeTotale + (enteredOrders[i] as ArticleItemVO).prixTotale;			
				}
			}
			
			sommeTotaleStr = ConsoviewFormatter.formatEuroCurrency(sommeTotale, 2);
		}
		
		private function init():void
		{
			enteredOrders = new ArrayCollection();
			enteredOrders.addItem(new ArticleItemBaseVO());
			
		}
		
		protected function closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function alertConfirmation(evt:CloseEvent):void
		{
			if(evt.detail == Alert.OK){
				this.closeHandler(evt);
			}
		}
		
		public function get enteredOrders():ArrayCollection { return _enteredOrders; }
		
		public function set enteredOrders(value:ArrayCollection):void
		{
			if (_enteredOrders == value)
				return;
			_enteredOrders = value;
		}
		
		public function get sommeTotaleStr():String
		{
			return _sommeTotaleStr;
		}
		
		public function set sommeTotaleStr(value:String):void
		{
			_sommeTotaleStr = value;
		}
		
		public function checkData():Boolean
		{
			var bool:Boolean = false;
			
			if(validerChoixArticles())
			{
				buildCommandData();
				bool = true;
			}
			
			return bool;
		}
		
		private function buildCommandData():void
		{
			_myElements.POSTES_INFRAST.removeAll();
			_myElements.POSTES_INFRAST.addAll(_listeArticlesChoisis);
			SessionUserObject.singletonSession.COMMANDE = cmd;
			SessionUserObject.singletonSession.CURRENTCONFIGURATION = _myElements;
		}
		
		private function validerChoixArticles():Boolean
		{
			var validation			:Boolean = false;
			_listeArticlesChoisis = new ArrayCollection();
			
			var taille:uint = enteredOrders.length;
			if(taille > 1)
			{
				for(var i:int = 0; i < taille; i++)
				{
					if( (enteredOrders[i].isItem) && (enteredOrders[i].selected == true) && ((enteredOrders[i].quantite) > 0))
					{
						_listeArticlesChoisis.addItem(enteredOrders[i]);
					}
					else if( (enteredOrders[i].isItem) && (enteredOrders[i].selected == true) && ((enteredOrders[i].quantite) == 0))
					{
						_listeArticlesChoisis.removeAll();
						ConsoviewAlert.afficherError(" Veuillez indiquer la quantité à commander pour chaque article ajouté.", "Attention");
						break;
					}
					else if( (enteredOrders[i].isItem) && (enteredOrders[i].selected == false))
					{
						_listeArticlesChoisis.removeAll();
						ConsoviewAlert.afficherError(" Veuillez choisir un article pour chaque ajout.", "Attention");
						break;
					}
				}
				
				if(_listeArticlesChoisis.length > 0){
					validation = true;
				}
			}
			else
			{
				ConsoviewAlert.afficherError("Veuillez ajouter des articles pour valider cette commande.", "Attention");
			}
			return validation;
		}
		
		public function viewStackChange():void
		{
			cmd = SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			if(!(_articleService.myDatas.hasEventListener(ArticleEvent.LISTED_ARTICLES)) && (_firstProfil != cmd.IDPROFIL_EQUIPEMENT))
			{
				var taille:uint = enteredOrders.length;
				var indexsItems:Array =  new Array();
				for(var i:uint = 0; i < taille ; i++ )
				{
					if(enteredOrders[i].isItem)
					{
						indexsItems.push(enteredOrders.getItemIndex(enteredOrders[i]))
					}
				}
				indexsItems.sort(Array.DESCENDING);
				
				for(var j:uint = 0; j < indexsItems.length; j++)
				{
					enteredOrders.removeItemAt(indexsItems[j]);
				}
				enteredOrders.refresh();
				
				_articleService.myDatas.addEventListener(ArticleEvent.LISTED_ARTICLES, getListArticlesHandler);
				_articleService.getListeArticle(cmd, SEGMENT);
				_firstProfil = cmd.IDPROFIL_EQUIPEMENT;
			}
		}
		
		
		public function showHandler(fl:FlexEvent):void
		{
		}
		
		public function buildParameters():void
		{
			var nEngagement:Engagement = new Engagement();
				
			_myElements.ENGAGEMENT = nEngagement;
		}

		public function get listeArticlesChoisis():ArrayCollection
		{
			return _listeArticlesChoisis;
		}

		public function set listeArticlesChoisis(value:ArrayCollection):void
		{
			_listeArticlesChoisis = value;
		}

	}
}