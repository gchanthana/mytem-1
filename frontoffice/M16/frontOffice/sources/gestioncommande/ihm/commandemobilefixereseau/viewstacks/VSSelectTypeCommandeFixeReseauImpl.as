package gestioncommande.ihm.commandemobilefixereseau.viewstacks
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.popup.PopUpChargerCommandeIHM;
	import gestioncommande.popup.PopUpChargerCommandeImpl;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import session.SessionUserObject;
	
	public class VSSelectTypeCommandeFixeReseauImpl extends VBox
	{
		
		public var rdbtnNewTerminal				:RadioButton;
		public var rdbtnterminalOnly			:RadioButton;
		public var rdbtnChargerConfiguration	:RadioButton;
		
		public var lblNameConfig				:Label;
		
		public var btnParcourir					:Button;
		
		public var listArticles					:ArrayCollection;
				
		private var _popUpModele				:PopUpChargerCommandeIHM;
		
		private const SPIE_1					:Number = 5821693;
		private const SPIE_2					:Number = 6149989;
				
		public function VSSelectTypeCommandeFixeReseauImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			/*SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 2;
			rdbtnNewTerminal.selected 			= true;*/
			
			var groupIdx:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			if((groupIdx == SPIE_1) || (groupIdx == SPIE_2))
			{
				this.rdbtnterminalOnly.visible = false;
				this.rdbtnterminalOnly.includeInLayout = false;
				
				this.rdbtnChargerConfiguration.visible = false;
				this.rdbtnChargerConfiguration.includeInLayout = false;
				
				this.lblNameConfig.visible = false;
				this.lblNameConfig.includeInLayout = false;
				
				this.btnParcourir.visible = false;
				this.btnParcourir.includeInLayout = false;
			}
		}
		
		protected function rdbtnNewTerminalChangeHandler(e:Event):void
		{
			SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 2;
			rdbtnNewTerminal.selected 			= true;
			razModele();
		}
		
		protected function rdbtnterminalOnlyChangeHandler(e:Event):void
		{
			SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 10;
			rdbtnterminalOnly.selected 			= true;
			razModele();
		}
		
		protected function rdbtnChargerConfigurationChangeHandler(e:Event):void
		{
			SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 4;
			if(SessionUserObject.singletonSession.IDPROFILESELECETED < 0)
			{
				rdbtnChargerConfiguration.selected 	= false;
				callLater(rdbtnNewTerminalChangeHandler,[e]);
				btnParcourir.enabled 				= false;
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_type_de_command'), 'Consoview');
			}
			else
			{
				rdbtnChargerConfiguration.selected = true;
				btnParcourir.enabled = true;
			}
		}
		
		protected function btnParcourirClickHandler(e:Event):void
		{
			_popUpModele 			= new PopUpChargerCommandeIHM();//IL FAUDRA PASSER L'IDPOOL EN PARAMETRE
			_popUpModele.idProfil 	= SessionUserObject.singletonSession.IDPROFILESELECETED;
			
			_popUpModele.addEventListener(PopUpChargerCommandeImpl.SELECT_MODELE, modeleSelectedHandler);
			
			PopUpManager.addPopUp(_popUpModele, this.parent.parent.parent.parent.parent.parent.parent, true);
			PopUpManager.centerPopUp(_popUpModele);
		}
		
		private function modeleSelectedHandler(e:Event):void
		{
			lblNameConfig.text = _popUpModele.libelle;
			SessionUserObject.singletonSession.CURRENTCONFIGURATION = _popUpModele.myElements;
		}
		
		private function razModele():void
		{
			listArticles 		 = null;
			lblNameConfig.text 	 = '';
			btnParcourir.enabled = false;
			
			dispatchEvent(new Event('TYPE_CMD_CHANGED', true));
		}

	}
}