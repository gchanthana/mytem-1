package gestioncommande.ihm.modificationcommande.button
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.containers.HBox;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.controls.List;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import commandemobile.popup.sitemodification.ParametreSiteIHM;
	import commandemobile.popup.sitemodification.Site;
	
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.FicheSiteIHM;
	import gestioncommande.popup.PopUpSearchSiteIHM;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.CompteService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.SiteService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class Etape1Impl extends AbstractWorflow implements IWorflow
	{
		
		//---POPUP
		private var _popUpFicheSites			:FicheSiteIHM;
		private var _popupParamSite 			:ParametreSiteIHM;
		
		//---IHM
		public var txtRefOp						:TextInput;
		public var txtLibelle					:TextInput;
		public var txtbxTo						:TextInputLabeled;
		
		public var cbxSites						:ComboBox;
		public var cbxSitesFacturation			:ComboBox;
		public var cbxTitulaire					:ComboBox;
		public var cbxPointFacturation			:ComboBox;

		public var hbxChamps1					:HBox;
		public var hbxChamps2					:HBox;

		public var chbxSelected					:CheckBox;
		
		public var listConfig					:List;
		
		public var dcLivraisonPrevue			:CvDateChooser;
		
		private var _popUpSearchSite			:PopUpSearchSiteIHM;
		
		private var _cmd						:Commande;
		
		private var _libelleCompteOp			:ArrayCollection = new ArrayCollection();
		
		//---CLASS PROCEDURE
		private var _cpteSrv					:CompteService = new CompteService();
		private var _site						:SiteService 	= new SiteService();
		private var _cmdeServices				:CommandeService = new CommandeService();
		
		private var _date						:Date = new Date();
		
		//---VARIABLES
		public var periodeObject				:Object = {rangeStart:new Date(new Date().getTime() - (60 * DateFunction.millisecondsPerDay) )};
		public var dateDuJourPlus2JoursOuvrees	:Date	= DateFunction.addXJoursOuvreesToDate(new Date(),2);

		public var taille						:int 	= 150;
		
		public var createLine					:Boolean= false;
		public var CHAMPS1_ACTIF				:Boolean= false;
		public var CHAMPS2_ACTIF				:Boolean= false;
		
		public var listeSousComptes				:ArrayCollection;
		public var listCommande					:ArrayCollection;
		private var champs1						:ArrayCollection;
		private var champs2						:ArrayCollection;
		
		public var LIBELLE_CHAMPS1				:String = ResourceManager.getInstance().getString('M16','R_f_rence_client_1');
		public var EXEMPLE_CHAMPS1				:String = "";
		public var LIBELLE_CHAMPS2				:String = ResourceManager.getInstance().getString('M16','R_f_rence_client_2');
		public var EXEMPLE_CHAMPS2				:String = "";
		public var NAME_BUTTON					:String = ResourceManager.getInstance().getString('M16','R_capitulatif');
		public var NAME_ETAPE					:String = "";
		
		public var compteName					:String = ResourceManager.getInstance().getString('M16','Compte');
		public var souscompteName				:String = ResourceManager.getInstance().getString('M16','Sous_compte');
		
		public var CHAMPS1_EXEMPLE_VISIBLE		:Boolean = false;
		public var CHAMPS2_EXEMPLE_VISIBLE		:Boolean = false;
				
		public var enable						:Boolean = true;
		public var checkable					:Boolean = true;
		
		private var _libelle					:String  = ResourceManager.getInstance().getString('M16','_Param_tres');
		private var _libelleButton				:String  = "";
		private var _visibleButton				:Boolean = true;
		private var _idButton					:int	 = 1;

		public const IDPAGE						:int = 1;
		public var ACCESS						:Boolean = true;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function Etape1Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function lblclickHandler(me:MouseEvent):void
		{
			txtLibelle.text = _cmd.LIBELLE_COMMANDE;
		}
		
		protected function cbxSitesCloseHandler(e:Event):void
		{
			cbxSites.errorString = "";
		}
		
		protected function imgSearchClickHandler(me:MouseEvent):void
		{
			var composant		:Image = me.currentTarget as Image;
			var currentData		:ArrayCollection = new ArrayCollection();
			
			_popUpSearchSite = new PopUpSearchSiteIHM();
			
			if(composant != null && composant.id == 'imgSearchL')
			{
				_popUpSearchSite.setInfos(ObjectUtil.copy(cbxSites.dataProvider as ArrayCollection) as ArrayCollection, cbxSites.selectedItem, true);
			}
			else if(composant != null && composant.id == 'imgSearchF')
			{
				_popUpSearchSite.setInfos(ObjectUtil.copy(cbxSitesFacturation.dataProvider as ArrayCollection) as ArrayCollection, cbxSitesFacturation.selectedItem, false);
			}
			
			if(composant != null)
			{
				_popUpSearchSite.addEventListener('SEARCH_SITE_SELECTED', searchSiteHandler);
				
				PopUpManager.addPopUp(_popUpSearchSite, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(_popUpSearchSite);
			}
		}
		
		private function searchSiteHandler(e:Event):void
		{
			var mySiteSelected	:Object = _popUpSearchSite.getInfos();			
			var lenSitesL		:int = (cbxSites.dataProvider as ArrayCollection).length;
			var lenSitesF		:int = (cbxSitesFacturation.dataProvider as ArrayCollection).length;
			var i				:int = 0;
			
			if(_popUpSearchSite.isSiteLivraison)
			{
				for(i = 0;i < lenSitesL;i++)
				{
					if((cbxSites.dataProvider as ArrayCollection)[i].IDSITE_PHYSIQUE == mySiteSelected.IDSITE_PHYSIQUE)
					{
						cbxSites.selectedIndex = i;
						
						break;
					}
				}
				
				searchInSiteFacturation(cbxSites.selectedItem.IDSITE_PHYSIQUE);
			}
			else
			{
				for(i = 0;i < lenSitesF;i++)
				{
					if((cbxSitesFacturation.dataProvider as ArrayCollection)[i].IDSITE_PHYSIQUE == mySiteSelected.IDSITE_PHYSIQUE)
					{
						cbxSitesFacturation.selectedIndex = i;
						
						break;
					}
				}
			}
			
			PopUpManager.removePopUp(_popUpSearchSite);
		}
		
		protected function chbxSelectedChangeHandler(e:Event):void
		{
			SessionUserObject.singletonSession.IDTYPEDECOMMANDE = -1;
			
			unselectAllArticles();
			
			dispatchEvent(new Event('CHANGE_CONFIGUR', true));
		}
		
		protected function cbxSitesFacturationCloseHnadler(e:Event):void
		{
			if(cbxSitesFacturation.selectedItem != null)
				cbxSitesFacturation.errorString = "";
		}
		
		protected function cbxTitulaireChangeHandler(e:Event):void
		{
			if(cbxTitulaire.selectedItem != null)
				findSousCompte(cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION);
		}

		protected function cbxPointFacturationChangeHandler(e:Event):void
		{
			if(cbxPointFacturation.selectedItem != null)
				cbxPointFacturation.errorString = "";
			else
				cbxPointFacturation.errorString = ResourceManager.getInstance().getString('M16','Veuillez_s_lectionner_un') + souscompteName;
		}
		
		protected function btnNewSiteClickHandler(me:MouseEvent):void
		{
			_popUpFicheSites = new FicheSiteIHM();
			_popUpFicheSites.idPoolGestionnaire = _cmd.IDPOOL_GESTIONNAIRE;
			
			PopUpManager.addPopUp(_popUpFicheSites, this, true);
			PopUpManager.centerPopUp(_popUpFicheSites);
//			_popUpFicheSites.addEventListener("refreshSite", ficheSiteHandler);
		}
		
		protected function btnModSiteClickHandler(me:MouseEvent):void
		{
			if(cbxSites.selectedItem != null)
			{
				_popupParamSite = new ParametreSiteIHM();
				_popupParamSite.site = mappingSiteSelectedToSite();
				addPop();
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16','S_lectionnez_un_site_'), 'Consoview', null);
		}
		
		protected function textInputHandler(txtValue:int):void
		{
			switch(txtValue)
			{
				case 0: txtLibelle.errorString 		= "";break;
				case 2: txtRefOp.errorString 		= "";break;
			}
		}
		
		protected function listConfigItemClickHandler(e:Event):void
		{
			if(listConfig.selectedItem == null) return;
			
			unselectAllArticles();
			
			listConfig.selectedItem.SELECTED = true;
			
			(listConfig.dataProvider as ArrayCollection).itemUpdated(listConfig.selectedItem);
			
			listConfig.errorString = "";
			
			chbxSelected.selected = false;
			
			dispatchEvent(new Event('CHANGE_CONFIGUR', true));
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getLibelleCompteOperateur():void
		{
			_cpteSrv.fournirLibelleCompteOperateur();
			_cpteSrv.addEventListener(CommandeEvent.LISTED_LIBELLE_OPE, libelleCompteOperateurHandler);
		}
		
		private function getSitesLivraisonsFacturations():void
		{
			_site.fournirListeSiteLivraisonsFacturation(_cmd.IDPOOL_GESTIONNAIRE);
			_site.addEventListener(CommandeEvent.LISTED_SITES, sitesLivraisonsFacturationsHandler);
		}

		private function getCompte():void
		{
			_cpteSrv.fournirCompteOperateurByLoginAndPoolV2(_cmd.IDOPERATEUR, _cmd.IDPOOL_GESTIONNAIRE, _cmd.IDCOMPTE_FACTURATION);
			_cpteSrv.addEventListener(CommandeEvent.LISTED_COMPTES, compteHandler);
		}
		
		private function getSLA():void
		{			
			_cmdeServices.fournirRevendeurSLA(_cmd.IDREVENDEUR, _cmd.IDOPERATEUR);
			_cmdeServices.addEventListener(CommandeEvent.LISTED_REVENDEURSLA, slaHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function libelleCompteOperateurHandler(cmde:CommandeEvent):void
		{
			_libelleCompteOp = _cpteSrv.listeLibellesOperateur;
			findLibelleOperateur();
		}
		
		private function sitesLivraisonsFacturationsHandler(cmde:CommandeEvent):void
		{			
			attributDataToCombobox(cbxSites, _site.listeSitesLivraison, ResourceManager.getInstance().getString('M16','Aucun_sites'));
			
			dataSelected(cbxSites, _site.listeSitesLivraison, 'IDSITE_PHYSIQUE', _cmd.IDSITELIVRAISON);
			
			attributDataToCombobox(cbxSitesFacturation, _site.listeSitesFacturation, ResourceManager.getInstance().getString('M16','Aucun_sites'));
			
			dataSelected(cbxSitesFacturation, _site.listeSitesFacturation, 'IDSITE_PHYSIQUE', _cmd.IDSITEFACTURATION);
		}

		private function compteHandler(cmde:CommandeEvent):void
		{
			var listeComptes	:ArrayCollection = _cpteSrv.listeComptesSousComptes;// Formator.sortFunction(_cpteSrv.listeComptesSousComptes, 'COMPTE_FACTURATION');
			
			attributDataToCombobox(cbxTitulaire, listeComptes, ResourceManager.getInstance().getString('M16','Aucun_titulaire'));
			
			dataSelected(cbxTitulaire, listeComptes, 'IDCOMPTE_FACTURATION', _cmd.IDCOMPTE_FACTURATION);
			
			if(cbxTitulaire.selectedItem != null)
			{
				if(cbxTitulaire.selectedItem != null)
					findSousCompte(cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION);
			}
			else
				getCompteSousCompteSelected();
		}

		private function slaHandler(cmde:CommandeEvent):void
		{
			if(_cmdeServices.revendeurSLA != null)
			{
				var hoursParsing	:Object = Formator.formatHour(_date);
				var delaiTime		:Array 	= (_cmdeServices.revendeurSLA.DELAI_HOUR_LIVRAISON as String).split(".");
				var delai			:int 	= Number(_cmdeServices.revendeurSLA.DELAI_LIVRAISON);
				
				if(Number(hoursParsing.HOURS) > Number(delaiTime[0]))
					delai = delai + 1;
				else
				{
					if(Number(hoursParsing.HOURS) == Number(delaiTime[0]))
					{
						if(Number(hoursParsing.MIN) > Number(delaiTime[1]))
							delai = delai + 1;
					}
				}
				
				dcLivraisonPrevue.selectedDate 		= DateFunction.addXJoursOuvreesToDate(new Date(), delai);
				dcLivraisonPrevue.selectableRange 	= {rangeStart:new Date(new Date().getTime() + (delai * DateFunction.millisecondsPerDay))};
			}
			else
			{
				dcLivraisonPrevue.selectedDate 		= DateFunction.addXJoursOuvreesToDate(new Date(), 2);
				dcLivraisonPrevue.selectableRange 	= {rangeStart:new Date(new Date().getTime() + (2 * DateFunction.millisecondsPerDay))};
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION - DATA & COMBOBOX
		//--------------------------------------------------------------------------------------------//
		
		private function getCompteSousCompteSelected():void
		{
			var myObject:Object = new Object();
			
			cbxTitulaire.visible = cbxTitulaire.includeInLayout = false;
			
			(cbxTitulaire.dataProvider as ArrayCollection).removeAll();
			(cbxPointFacturation.dataProvider as ArrayCollection).removeAll();
			
			myObject						= new Object();
			myObject.LIBELLE_COMPTE			= _cmd.LIBELLE_COMPTE;
			myObject.COMPTE_FACTURATION 	= _cmd.LIBELLE_COMPTE;
			myObject.IDCOMPTE_FACTURATION 	= _cmd.IDCOMPTE_FACTURATION;
			
			(cbxTitulaire.dataProvider as ArrayCollection).addItem(myObject);
			
			cbxTitulaire.selectedIndex = 0;
			
			myObject					= new Object();
			myObject.LIBELLE_SOUSCOMPTE	= _cmd.LIBELLE_SOUSCOMPTE;
			myObject.SOUS_COMPTE 		= _cmd.LIBELLE_SOUSCOMPTE;
			myObject.IDSOUS_COMPTE 		= _cmd.IDSOUS_COMPTE;
			
			(cbxPointFacturation.dataProvider as ArrayCollection).addItem(myObject);
			
			cbxPointFacturation.selectedIndex = 0;
		}
		
		//---ATTRIBUT LES DONNEES A LA COMBOBOX PASSEE EN PARAMETRE
		private function attributDataToCombobox(cbx:ComboBox, dataValues:ArrayCollection, msgError:String):void
		{
			var lenDataValues:int = dataValues.length;
			
			cbx.dataProvider = null;
			cbx.prompt 		 = "";
			cbx.errorString  = "";
			
			if(lenDataValues != 0)
			{
				cbx.prompt 					= ResourceManager.getInstance().getString('M16', 'S_lectionnez');
				cbx.dataProvider 			= dataValues;
				cbx.dropdown.dataProvider 	= dataValues;
				
				if(lenDataValues > 6)
					cbx.rowCount = 6;
				else
					cbx.rowCount = lenDataValues;
				
				(cbx.dataProvider as ArrayCollection).refresh();
			}
			else
				cbx.prompt = msgError;
		}
		
		private function dataSelected(cbx:ComboBox, dataValues:ArrayCollection, label:String, idSelected:int):void
		{
			var len:int = dataValues.length;
			
			if(len == 0) return;
			
			if(len == 1)
			{
				cbx.selectedIndex = 0;
				return;
			}
			
			for(var i:int = 0;i < len;i++)
			{
				if(dataValues[i][label] == idSelected)
					cbx.selectedIndex = i;
			}
		}
		
		private function findSousCompte(idCpte:int):void
		{
			var sscpte	:ArrayCollection = new ArrayCollection();
			var values	:ArrayCollection = _cpteSrv.listeComptesSousComptes;
			var lenVal	:int = values.length;
			
			for(var i:int = 0;i < lenVal;i++)
			{
				if(idCpte == values[i].IDCOMPTE_FACTURATION)
					sscpte.addItem(values[i]);
			}
			
			var tri	:Sort = new Sort();
			tri.fields = [new SortField("SOUS_COMPTE")];
			
			sscpte.sort = tri;
			sscpte.refresh();
			sscpte.sort = null;
			
			attributDataToCombobox(cbxPointFacturation, sscpte, ResourceManager.getInstance().getString('M16','Aucun_titulaire'));
			
			dataSelected(cbxPointFacturation, sscpte, 'IDSOUS_COMPTE', _cmd.IDSOUS_COMPTE);
		}
		
		private function findArticlesSelected():void
		{
			var elements	:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARRAYCOL;
			var lenElmts	:int = elements.length;
			
			for(var i:int = 0;i < lenElmts;i++)
			{
				if(elements[i].SELECTED)
				{
					SessionUserObject.singletonSession.IDTYPEDECOMMANDE 	= elements[i].IDTCMDE;
					SessionUserObject.singletonSession.CURRENTCONFIGURATION = elements[i].ELEMENTS; //---> IL FAUDRA FAIRE UNE COPY DE L'OBJET POUR CASSER LA REFERENCE
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//

		private function findLibelleOperateur():void
		{
			var idop	:int = _cmd.IDOPERATEUR;
			var len 	:int = _libelleCompteOp.length;
			
			for(var i:int = 0; i < len;i++)
			{
				if(_libelleCompteOp[i].OPERATEURID == idop)
				{
					if(_libelleCompteOp[i].COMPTE != null && _libelleCompteOp[i].COMPTE != '')
						compteName 		= _libelleCompteOp[i].COMPTE;
					else
						compteName 		= ResourceManager.getInstance().getString('M16','Compte');;
					
					if(_libelleCompteOp[i].SOUS_COMPTE != null && _libelleCompteOp[i].SOUS_COMPTE != '')
						souscompteName 	= _libelleCompteOp[i].SOUS_COMPTE;
					else
						souscompteName 	= ResourceManager.getInstance().getString('M16','Sous_compte');
					
					return;
				}
			}	
		}
		
		///------->>>>>> AJOUT LIBELLE COMPTE/SOUS COMPTE
		public function creationCompleteHandler(fl:FlexEvent):void
		{
			_cmd = SessionUserObject.singletonSession.COMMANDE;
		}
		
		public function showHandler(fl:FlexEvent):void
		{
		}
		
		public function viewStackChange():void
		{
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
		}
		
		public function addElements():void
		{
			getLibelleCompteOperateur();
			getSitesLivraisonsFacturations();
			getCompte();
		}
	
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION 
		//--------------------------------------------------------------------------------------------//

		public function checkData():Boolean
		{
			var bool	:Boolean = true;
			var check	:Boolean = false;
			
			if(txtRefOp.text != "")
				txtRefOp.errorString = "";
			else
			{
				txtRefOp.errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
				bool =  false;
			}
			
			if(txtLibelle.text != "")
				txtLibelle.errorString = "";
			else
			{
				txtLibelle.errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
				bool =  false;
			}
			
			if(bool)
				bool = checkChamps();
			else
				checkChamps();
			
			if(cbxSites.selectedItem != null)
				cbxSites.errorString = "";
			else
			{
				cbxSites.errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
				bool =  false;
			}
			
			if(cbxSitesFacturation.selectedItem != null)
				cbxSitesFacturation.errorString = "";
			else
			{
				cbxSitesFacturation.errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
				bool =  false;
			}
			
			if(createLine)
			{
				if(cbxTitulaire.selectedItem != null)
					cbxTitulaire.errorString = "";
				else
				{
					cbxTitulaire.errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
					bool =  false;
				}
				
				if(cbxPointFacturation.selectedItem != null)
					cbxPointFacturation.errorString = "";
				else
				{
					cbxPointFacturation.errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
					bool =  false;
				}
			}
			
			if(dcLivraisonPrevue.selectedDate != null)
				dcLivraisonPrevue.errorString = "";
			else
			{
				dcLivraisonPrevue.errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
				bool =  false;
			}
			
			if(chbxSelected.selected)
			{
				SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 0;
				listConfig.errorString = "";
				check = true;
			}
			else
			{
				if(bool)
				{
					var dataPovider:ArrayCollection = listConfig.dataProvider as ArrayCollection;
					
					for(var i:int = 0;i < dataPovider.length;i++)
					{
						if(dataPovider[i].SELECTED)
						{
							SessionUserObject.singletonSession.IDTYPEDECOMMANDE = dataPovider[i].IDTCMDE;
							check = true;
						}
					}
					
					if(!check)
						listConfig.errorString = ResourceManager.getInstance().getString('M16','Saisissez_une_commande___modifier__');
					else
						listConfig.errorString = "";
				}
			}
			
			if(bool && check)
			{
				bool = true;
				buildParameters();
			}
			else
				bool = false;

			return bool;
		}
		
		public function buildParameters():void
		{
			var objectTitu		:Object = new Object();
			var objectPointFact	:Object = new Object();
			
			if(cbxTitulaire.selectedItem == null)
			{
				objectTitu.LIBELLE_COMPTE 		= "";
				objectTitu.IDCOMPTE_FACTURATION = 0;
			}
			else
			{
				objectTitu.LIBELLE_COMPTE 		= cbxTitulaire.selectedItem.COMPTE_FACTURATION;
				objectTitu.IDCOMPTE_FACTURATION = cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION;
			}
			
			if(cbxPointFacturation.selectedItem == null)
			{
				objectPointFact.LIBELLE_COMPTE 		 = "";
				objectPointFact.IDCOMPTE_FACTURATION = 0;
			}
			else
			{
				objectPointFact.LIBELLE_SOUSCOMPTE 	= cbxPointFacturation.selectedItem.SOUS_COMPTE;
				objectPointFact.IDSOUS_COMPTE 		= cbxPointFacturation.selectedItem.IDSOUS_COMPTE;
			}
			
			SessionUserObject.singletonSession.COMMANDE.V1 						= SessionUserObject.singletonSession.COMMANDE.REF_CLIENT1;
			SessionUserObject.singletonSession.COMMANDE.V2 						= SessionUserObject.singletonSession.COMMANDE.REF_CLIENT2;
			SessionUserObject.singletonSession.COMMANDE.REF_OPERATEUR 			= txtRefOp.text;
			SessionUserObject.singletonSession.COMMANDE.LIBELLE_COMMANDE 		= txtLibelle.text;
			SessionUserObject.singletonSession.COMMANDE.LIBELLE_TO				= txtbxTo.text;
			SessionUserObject.singletonSession.COMMANDE.LIVRAISON_PREVUE_LE		= dcLivraisonPrevue.selectedDate;
			SessionUserObject.singletonSession.COMMANDE.LIBELLE_SITELIVRAISON 	= cbxSites.selectedItem.NOM_SITE;
			SessionUserObject.singletonSession.COMMANDE.IDSITELIVRAISON 		= cbxSites.selectedItem.IDSITE_PHYSIQUE;
			SessionUserObject.singletonSession.COMMANDE.LIBELLE_SITEFACTURATION = cbxSitesFacturation.selectedItem.NOM_SITE;
			SessionUserObject.singletonSession.COMMANDE.IDSITEFACTURATION		= cbxSitesFacturation.selectedItem.IDSITE_PHYSIQUE;
			SessionUserObject.singletonSession.COMMANDE.LIBELLE_COMPTE 			= objectTitu.LIBELLE_COMPTE;
			SessionUserObject.singletonSession.COMMANDE.IDCOMPTE_FACTURATION 	= objectTitu.IDCOMPTE_FACTURATION;
			SessionUserObject.singletonSession.COMMANDE.LIBELLE_SOUSCOMPTE 		= objectPointFact.LIBELLE_SOUSCOMPTE;
			SessionUserObject.singletonSession.COMMANDE.IDSOUS_COMPTE			= objectPointFact.IDSOUS_COMPTE;
			
			findArticlesSelected();
		}

//------------------------------------------------------

		public function champsPersoHandler():void
		{
			var i:int = 0;
			
			champs1 		= SessionUserObject.singletonSession.CHAMPS.CHAMPS1;
			champs2 		= SessionUserObject.singletonSession.CHAMPS.CHAMPS2;
			
			if(champs1 != null && champs2 != null)
			{ 
				LIBELLE_CHAMPS1 = SessionUserObject.singletonSession.CHAMPS.LIBELLE1;
				EXEMPLE_CHAMPS1 = SessionUserObject.singletonSession.CHAMPS.EXEMPLE1;
				CHAMPS1_ACTIF 	= SessionUserObject.singletonSession.CHAMPS.ACTIVE1 as Boolean;
				LIBELLE_CHAMPS2 = SessionUserObject.singletonSession.CHAMPS.LIBELLE2;
				EXEMPLE_CHAMPS2 = SessionUserObject.singletonSession.CHAMPS.EXEMPLE2;
				CHAMPS2_ACTIF 	= SessionUserObject.singletonSession.CHAMPS.ACTIVE2 as Boolean;
			}
			
			if(EXEMPLE_CHAMPS1 == "")
				CHAMPS1_EXEMPLE_VISIBLE = false;
			else
				CHAMPS1_EXEMPLE_VISIBLE = true;
			
			if(EXEMPLE_CHAMPS2 == "")
				CHAMPS2_EXEMPLE_VISIBLE = false;
			else
				CHAMPS2_EXEMPLE_VISIBLE = true;
			
			if(CHAMPS1_ACTIF)
			{
				if(SessionUserObject.singletonSession.CHAMPS.hbxChamps1 != null)
				{
					var nbrChild1	:int  	= (SessionUserObject.singletonSession.CHAMPS.hbxChamps1 as HBox).numChildren;
					var hbx1		:HBox	= SessionUserObject.singletonSession.CHAMPS.hbxChamps1 as HBox;
					var arrayTemp1	:Array 	= copyChampsPerso((hbx1 as HBox).getChildren());
					var nbrTemp1	:int 	= arrayTemp1.length;
					var cptr1		:int	= 1;
					
					for(i = 0;i < nbrTemp1;i++)
					{
						var libelle1:String = arrayTemp1[i].text;
						
						if(i == cptr1)
						{
							var texte1:String = splitString(arrayTemp1[i].text, true);
							(hbxChamps1.getChildAt(i-1) as TextArea).text = texte1;
							cptr1= cptr1 + 2;
						}
						
						hbxChamps1.addChild(arrayTemp1[i]);
						(hbxChamps1.getChildAt(i) as TextArea).text = libelle1;
					}
				}
			}
			
			if(CHAMPS2_ACTIF)
			{
				if(SessionUserObject.singletonSession.CHAMPS.hbxChamps2 != null)
				{
					var nbrChild2	:int  	= (SessionUserObject.singletonSession.CHAMPS.hbxChamps2 as HBox).numChildren;
					var hbx2		:HBox	= SessionUserObject.singletonSession.CHAMPS.hbxChamps2 as HBox;
					var arrayTemp2	:Array 	= copyChampsPerso((hbx2 as HBox).getChildren());
					var nbrTemp2	:int 	= arrayTemp2.length;
					var cptr2		:int	= 1;
					
					for(i = 0;i < nbrTemp2;i++)
					{
						var libelle2:String = arrayTemp2[i].text;
						
						if(i == cptr2)
						{
							var texte2:String = splitString(arrayTemp2[i].text, false);
							(hbxChamps2.getChildAt(i-1) as TextArea).text = texte2;
							cptr2= cptr2 + 2;
						}
						
						hbxChamps2.addChild(arrayTemp2[i]);
						(hbxChamps2.getChildAt(i) as TextArea).text = libelle2;
					}
				}
			}
		}
		
		private function copyChampsPerso(champs:Array):Array
		{
			var newChildren	:ArrayCollection = new ArrayCollection();
			var lenChildren	:int = champs.length;
			
			for(var i:int = 0;i < lenChildren;i++)
			{
				var txtarea:TextArea 	= new TextArea();				
					txtarea.width		= (champs[i] as TextArea).width;
					txtarea.height		= (champs[i] as TextArea).height;
					txtarea.restrict	= (champs[i] as TextArea).restrict;
					txtarea.maxChars	= (champs[i] as TextArea).maxChars;
					txtarea.id			= (champs[i] as TextArea).id;
					txtarea.text		= (champs[i] as TextArea).text;
					txtarea.editable	= (champs[i] as TextArea).editable;
					txtarea.selectable	= (champs[i] as TextArea).selectable;	
					txtarea.addEventListener(Event.CHANGE, textinputChangeHandler);									
				
				if(txtarea != null)
					newChildren.addItem(txtarea);
				else
					newChildren.addItem(champs[i]);
			}
			
			return newChildren.source;
		}
		
		private function textinputChangeHandler(e:Event):void
		{
			(e.currentTarget as TextArea).errorString = "";
		}
		
		private function splitString(strgToFind:String, isRef1:Boolean):String
		{
			var part		:String 	= "";
			var partremove	:String 	= "";
			var refClient	:String		= "";
			var firstIndex	:int		= -1;
			
			if(isRef1)
				refClient = (_cmd.REF_CLIENT11!=null)?_cmd.REF_CLIENT11:"";
			else
				refClient = (_cmd.REF_CLIENT21!=null)?_cmd.REF_CLIENT21:"";
			
			firstIndex = refClient.indexOf(strgToFind);
			
			if(firstIndex > 0)
			{
				part 		= refClient.slice(0, firstIndex);
				partremove	= part + strgToFind;
				refClient 	= refClient.replace(partremove, '');
			}
			else
			{
				part 		= refClient.slice(0);
				refClient 	= refClient.slice();
			}
			
			if(isRef1)
				_cmd.REF_CLIENT11 = refClient;
			else
				_cmd.REF_CLIENT21 = refClient;
			
			return part;
		}
		
		
		
		private function checkChamps():Boolean
		{
			var OK				:Boolean = true;
			var i				:int 	 = 0;
			var childChamps1 	:Array 	 = hbxChamps1.getChildren();
			var childChamps2 	:Array 	 = hbxChamps2.getChildren();
			
			
			if(CHAMPS1_ACTIF)
			{
				for(i = 0;i < champs1.length;i++)
				{
					if(champs1[i].SAISIE_ZONE)
					{
						if((childChamps1[i*2] as TextArea).text == "")
						{
							(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');;
							
							if(OK)
								OK = false;
						}
						else
						{
							(childChamps1[i*2] as TextArea).errorString = "";
							if(champs1[i].EXACT_NBR)
							{
								if((childChamps1[i*2] as TextArea).text.length != champs1[i].COMPTEUR_OBLIG)
								{
									(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs1[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps1[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
					else
					{
						if((childChamps1[i*2] as TextArea).text == "")
						{
							if(OK)
								OK = true;
						}
						else
						{
							(childChamps1[i*2] as TextArea).errorString = "";
							if(champs1[i].EXACT_NBR)
							{
								if((childChamps1[i*2] as TextArea).text.length != champs1[i].COMPTEUR_OBLIG)
								{
									(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs1[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps1[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
				}
			}
			
			if(CHAMPS2_ACTIF)
			{
				for(i = 0;i < champs2.length;i++)
				{
					if(champs2[i].SAISIE_ZONE)
					{
						if((childChamps2[i*2] as TextArea).text == "")
						{
							(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');;
							
							if(OK)
								OK = false;
						}
						else
						{
							(childChamps2[i*2] as TextArea).errorString = "";
							if(champs2[i].EXACT_NBR)
							{
								if((childChamps2[i*2] as TextArea).text.length != champs2[i].COMPTEUR_OBLIG)
								{
									(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs2[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps2[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
					else
					{
						if((childChamps2[i*2] as TextArea).text == "")
						{
							if(OK)
								OK = true;
						}
						else
						{
							(childChamps2[i*2] as TextArea).errorString = "";
							if(champs2[i].EXACT_NBR)
							{
								if((childChamps2[i*2] as TextArea).text.length != champs2[i].COMPTEUR_OBLIG)
								{
									(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs2[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps2[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
				}
			}
			
			if(OK)
				attributDataToCommandeObject();
			
			return OK;
		}
		
		private function attributDataToCommandeObject():void
		{
			var i				:int 	= 0;
			var childChamps1 	:Array 	= hbxChamps1.getChildren();
			var childChamps2 	:Array 	= hbxChamps2.getChildren();
			var reference1		:String	= "";
			var reference2		:String	= "";
			
			for(i = 0;i < childChamps1.length;i++)
			{
				reference1 = reference1 + childChamps1[i].text;
			}
			
			for(i = 0;i < childChamps2.length;i++)
			{
				reference2 = reference2 + childChamps2[i].text;
			}
			
			_cmd.REF_CLIENT1 = reference1;
			_cmd.REF_CLIENT2 = reference2;
		}
		
		private function dynamicChamps(idOperateur:int):void
		{
			compteName		= resourceManager.getString('M16','Compte');
			souscompteName 	= resourceManager.getString('M16','Sous_compte');
		}

		private function mappingSiteSelectedToSite():Site
		{
			var site	:Site 			= new Site();
				site.cp_site 			= cbxSites.selectedItem.SP_CODE_POSTAL;
				site.adr_site 			= cbxSites.selectedItem.ADRESSE;
				site.id_site			= cbxSites.selectedItem.IDSITE_PHYSIQUE;
				site.ref_site 			= cbxSites.selectedItem.SP_REFERENCE;
				site.code_interne_site 	= cbxSites.selectedItem.SP_CODE_INTERNE;
				site.commentaire_site 	= cbxSites.selectedItem.SP_COMMENTAIRE;
				site.commune_site 		= cbxSites.selectedItem.SP_COMMUNE;
				site.libelle_site 		= cbxSites.selectedItem.NOM_SITE;
				site.idpays_site		= cbxSites.selectedItem.PAYCONSOTELID;
				site.IS_FACTURATION		= formatIntToBool(cbxSites.selectedItem.IS_FACTURATION);
				site.IS_LIVRAISON		= formatIntToBool(cbxSites.selectedItem.IS_LIVRAISON);
			return site;
		}
		
		private function formatIntToBool(value:int):Boolean
		{
			var ok:Boolean = false;
			
			if(value == 1)
				ok = true;
				
			return ok;
		}

		private function addPop():void
		{
//			_popupParamSite.addEventListener(GestionSiteEvent.PARAMETRE_SITE_COMPLETE, parametreSiteCompleteHandler);
			
			PopUpManager.addPopUp(_popupParamSite, this, true);
			PopUpManager.centerPopUp(_popupParamSite);
		}
		




		private function searchInSiteFacturation(idSite:int):void
		{
			var sites	:ArrayCollection = cbxSitesFacturation.dataProvider as ArrayCollection;
			var len		:int 			 = sites.length;
			var isHere	:Boolean		 = false;
			
			
			for(var i:int = 0;i <len;i++)
	    	{
	    		if(sites[i].IDSITE_PHYSIQUE == idSite)
	    		{
	    			cbxSitesFacturation.selectedIndex = i;
	    			isHere = true;
	    			break;
	    		}
	    	}
		}

		private function unselectAllArticles():void
		{
			var elements	:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARRAYCOL;
			var lenElmts	:int = elements.length;
			
			for(var i:int = 0;i < lenElmts;i++)
			{
				elements[i].SELECTED = false;
				elements.itemUpdated(elements[i]);
			}
		}
		
		private function whatiscas(obj:Object):void
		{
			if(obj.CAS == 3)
			{
				if(obj.SELECTED)
					createLine = false;
				else
					createLine = true;
			}
			else
			{
				createLine = true;
			}
		}
		
		

	}
}