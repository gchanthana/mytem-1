package gestioncommande.ihm.modificationcommande.button
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.VBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.colorDatagrid.ColorDatagrid;
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.DetailProduitIHM;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.Formator;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class Etape2Impl extends AbstractWorflow implements IWorflow
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cboxEngagement			:ComboBox;	
		
		public var txtptSearch				:TextInput;
		
		public var dgListTerminal			:ColorDatagrid;
		
		public var vbxEquipementsSelection	:VBox;	
		
		public var boxVisible				:Box;	
		public var boxEngagementVisible		:Box;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmd					:Commande;
		
		private var _myElements				:ElementsCommande2	= new ElementsCommande2();
		
		private var _mobile					:EquipementsElements = null;
		private var _cartesim				:EquipementsElements = null;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _equipementsSrv			:EquipementService = new EquipementService();

		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var terminaux				:ArrayCollection = new ArrayCollection();
		public var listEngagement			:ArrayCollection = new ArrayCollection();
		
		public var isRenouvellement			:Boolean = false;
		
		private var _isTerSimEngReturn		:Boolean = false;

		private var _selectEgagement		:Boolean = true;
				
		private var _idRandom				:int = -1;

		//--------------------------------------------------------------------------------------------//
		//					CONCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE						:int = 2;
		
		public var ACCESS						:Boolean = true;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//		
				
		public function Etape2Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function creationCompleteHandler(fl:FlexEvent):void
		{
			addEventListener("VIEWDESCRIPTION_TER", descriptionHandler);
		}
		
		public function showHandler(fl:FlexEvent):void
		{
		}

		public function viewStackChange():void
		{
			_cmd 		= SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			if(_idRandom != SessionUserObject.singletonSession.CURRENTCONFIGURATION.RANDOM)
			{
				_idRandom = SessionUserObject.singletonSession.CURRENTCONFIGURATION.RANDOM;		
				
				getTerminauxSimCardAndEngagements();
				
				if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 1)
				{
					boxEngagementVisible.visible = boxEngagementVisible.includeInLayout = _selectEgagement = true;
					
					if(cboxEngagement.selectedItem == null)
						boxVisible.visible = false;
					else
						boxVisible.visible = true;
				}
				else
				{
					boxEngagementVisible.visible = boxEngagementVisible.includeInLayout = _selectEgagement = false;
					boxVisible.visible = true;
				}
			}
		}
		
		public function refreshStack():void
		{
		}

		public function checkData():Boolean
		{
			if(cboxEngagement.selectedItem != null && _selectEgagement)
			{
				if(cboxEngagement.selectedItem.hasOwnProperty("FPC"))
				{
					_myElements.ENGAGEMENT.FPC 	= cboxEngagement.selectedItem.FPC;
					_myElements.ENGAGEMENT 		= cboxEngagement.selectedItem as Engagement;
				}
				else
					_myElements.ENGAGEMENT = cboxEngagement.selectedItem as Engagement;
			}
			else
			{
				if(_selectEgagement)
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_engagement'), 'Consoview', null);
					return false;
				}
			}
			
			if(findTerminalSelected())
			{
				if(_mobile.IDFOURNISSEUR > 0)
				{
					_cmd.IDEQUIPEMENTPARENT = _mobile.IDFOURNISSEUR;
					
					if(_cartesim != null)
						_cartesim.IDPARENT = _mobile.IDFOURNISSEUR;
					
					_myElements.TERMINAUX.addItem(_mobile);
					
					if(_selectEgagement && _cmd.IDTYPE_COMMANDE != 1382 && _cmd.IDTYPE_COMMANDE != 1587 && _cmd.IDTYPE_COMMANDE != 1182 && _cmd.IDTYPE_COMMANDE != 1584)
						_myElements.TERMINAUX.addItem(_cartesim);
				}
				else
					_myElements.TERMINAUX.addItem(_mobile);
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_terminal'), 'Consoview', null);
				return false;
			}
			
			return true;
		}
		
		public function buildParameters():void
		{	
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//
	
		protected function cboxEngagementCloseHandler(e:Event):void
		{
			if(cboxEngagement.selectedItem != null)
				boxVisible.visible = true;
			else
				boxVisible.visible = false;
			
			if(terminaux.length > 0)
				terminaux.itemUpdated(terminaux[0]);
		}
		
		protected function dgListTerminalClickHandler(me:MouseEvent):void
		{
			if(me == null) return;
			
			terminalSelected();
		}
		
		protected function txtptSearchHandler(e:Event):void
		{
			if(dgListTerminal.dataProvider != null)
			{
				(dgListTerminal.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListTerminal.dataProvider as ArrayCollection).refresh();
			}
		}
	
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getTerminauxSimCardAndEngagements():void
		{
			_isTerSimEngReturn = false;
			
			terminaux.removeAll();
			listEngagement.removeAll();

			if(_cmd != null)
			{
				if(_cmd.IDTYPE_COMMANDE == 1382 || _cmd.IDTYPE_COMMANDE == 1587)
					isRenouvellement = true;
				else
					isRenouvellement = false;
				
				_equipementsSrv.fournirTerminauxSimCardEngagements(_cmd.IDPOOL_GESTIONNAIRE, _cmd.IDPROFIL_EQUIPEMENT, _cmd.IDREVENDEUR, _cmd.IDOPERATEUR,
																	SessionUserObject.singletonSession.IDSEGMENT, Formator.formatBoolean(!isRenouvellement));
				_equipementsSrv.addEventListener(CommandeEvent.TERMSIMCARDENGAG, termianuxSimCardAndEngagementsHandler);
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//

		private function termianuxSimCardAndEngagementsHandler(cmde:CommandeEvent):void
		{
			_isTerSimEngReturn = true;
			
			if(_equipementsSrv.ENGAGEMENTS.length > 1)
				cboxEngagement.prompt = ResourceManager.getInstance().getString('M16', 'Choisir_la_dur_e_d_engagement');
			
			listEngagement = _equipementsSrv.ENGAGEMENTS;
			
			if(_equipementsSrv.ENGAGEMENTS.length == 1)
				cboxEngagement.selectedIndex = 0;
			
			getEngagementSelected(listEngagement);
			
			terminaux = _equipementsSrv.TERMINAUX;

			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 3 || SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 10)
				addItemEmpty();
			
			getTerminalSelected();
			
			terminaux.refresh();
			
			_cartesim = _equipementsSrv.simcard;
			
			checkIfOnly();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function descriptionHandler(e:Event):void
		{
			if(dgListTerminal.selectedItem != null)
			{
				var popUpDetails:DetailProduitIHM 	= new DetailProduitIHM();
					popUpDetails.equipement			= dgListTerminal.selectedItem;
				
				PopUpManager.addPopUp(popUpDetails, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					FUCNTION
		//--------------------------------------------------------------------------------------------//

		public function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint			
		{				
			var rColor:uint;				
			rColor = color;				
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			
			if (item != null )				
			{				
				if(item.SUSPENDU_CLT == 1)//En repture
				{	
					rColor = 0xF7FF04;
				}			
				else					
					rColor = color;					
			}				
			return rColor;				
		}
		
		private function checkIfOnly():void
		{
			if(_isTerSimEngReturn)
			{
				if(_cmd.IDTYPE_COMMANDE == TypesCommandesMobile.EQUIPEMENTS_NUS)
				{
					if(terminaux.length == 1)
						dispatchEvent(new Event('ONE_ENGAGEMENT_EQUIPEMENT', true));
				}
				else
				{
					if(listEngagement.length == 1 && terminaux.length == 1)
						dispatchEvent(new Event('ONE_ENGAGEMENT_EQUIPEMENT', true));
				}
			}
		}
		
		private function addItemEmpty():void
		{
			terminaux.addItemAt(EquipementsElements.formatTerminalEmpty(), 0);
		}
		
		private function terminalSelected():void
		{
			if(dgListTerminal.selectedItem != null)
			{
				var len	:int = terminaux.length;
				var idT	:int = dgListTerminal.selectedItem.IDFOURNISSEUR;
				
				for(var i:int = 0;i < len;i++)
				{
					terminaux[i].SELECTED = false;
					
					if(terminaux[i].IDFOURNISSEUR == idT)
						if (terminaux[i].SUSPENDU_CLT != 1)
							terminaux[i].SELECTED = true;
					
					(dgListTerminal.dataProvider as ArrayCollection).itemUpdated(terminaux[i]);
				}
				
				_myElements.ACCESSOIRES = new ArrayCollection();
			}
		}
		
		private function findTerminalSelected():Boolean
		{			
			_myElements.TERMINAUX = new ArrayCollection();
			
			_mobile = new EquipementsElements();
			
			var len:int = terminaux.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(terminaux[i].SELECTED)
				{
					_mobile = terminaux[i] as EquipementsElements;
					
					SessionUserObject.singletonSession.PRIXELIGIBLE 	= Number(_mobile.PRIX);
					SessionUserObject.singletonSession.PRIXNONELIGIBLE 	= Number(_mobile.PRIX_C);
					
					return true;
				}
			}
			
			return false;
		}
		
		private function getEngagementSelected(values:ArrayCollection):void
		{
			var duree	:String = SessionUserObject.singletonSession.CURRENTCONFIGURATION.ENGAGEMENT.VALUE;
			var len		:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(values[i].VALUE == duree)
				{
					cboxEngagement.selectedIndex = i;
					boxVisible.visible = true;
					break;
				}
			}
		}
		
		private function getTerminalSelected():void
		{
			var values		:ArrayCollection = dgListTerminal.dataProvider as ArrayCollection;
			var lenTer		:int = values.length;
			var lenTerSel	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX.length;
			var idTerminal	:int = 0;
			
			for(var i:int = 0;i < lenTerSel;i++)
			{
				if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].IDEQUIPEMENT != 71)
				{
					idTerminal  = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].IDFOURNISSEUR;
					break;
				}
			}
			
			for(var j:int = 0;j < lenTer;j++)
			{
				if(values[j].IDFOURNISSEUR == idTerminal)
				{
					values[j].SELECTED = true;
					values.refresh();
					values.itemUpdated(values[j]);
					dgListTerminal.selectedIndex = j;
					break;
				}
			}
			
			findPrice();
			updatePriceTerminaux();
		}
		
		private function findPrice():void
		{
			var code:String = SessionUserObject.singletonSession.CURRENTCONFIGURATION.ENGAGEMENT.CODE;
			
			if(dgListTerminal.selectedIndex == -1) return;

			var terminal	:EquipementsElements = dgListTerminal.selectedItem as EquipementsElements;
			var lenTerSel	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX.length;
			var myterminal	:Object;
			
			for(var i:int = 0;i < lenTerSel;i++)
			{
				if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].IDEQUIPEMENT != 71)
					myterminal  = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i];
			}
			
			if(myterminal == null) return;
			
			switch(code)
			{
				case "EDM": 	terminal.PRIX_2 = myterminal.PRIX; break;
				case "EVQM":	terminal.PRIX_3 = myterminal.PRIX; break;
				case "ETSM": 	terminal.PRIX_4 = myterminal.PRIX; break;
				case "EQHM": 	terminal.PRIX_5 = myterminal.PRIX; break;
				case "EDMAR": 	terminal.PRIX_6 = myterminal.PRIX; break;
				case "EVQMAR":	terminal.PRIX_7 = myterminal.PRIX; break;
				case "ETSMAR": 	terminal.PRIX_8 = myterminal.PRIX; break;
				case "EQHMAR": 	terminal.PRIX_9	= myterminal.PRIX; break;
				case "EVQMFCP": terminal.PRIX_3	= myterminal.PRIX; break;
				default :		terminal.PRIX_C = myterminal.PRIX; break;
			}
			
			if(isRenouvellement)
			{
				if(SessionUserObject.singletonSession.PRIX_REEL_ELIGIBLE > 0)
					terminal.PRIX_7 = SessionUserObject.singletonSession.PRIXELIGIBLE.toString();
 				
				terminal.PRIX_C	= SessionUserObject.singletonSession.PRIXNONELIGIBLE.toString();
				terminal.PRIXC 	= SessionUserObject.singletonSession.PRIXNONELIGIBLE;
			}
		}
		
		private function updatePriceTerminaux():void
		{
			var values		:ArrayCollection = dgListTerminal.dataProvider as ArrayCollection;
			var lenTer		:int = values.length;

			for(var j:int = 0;j < lenTer;j++)
			{
				var equipement:EquipementsElements = values[j] as EquipementsElements;
				
				if(equipement.IDEQUIPEMENT != 0)
				{
					if(!boxEngagementVisible.visible) 
					{
						equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_C));
						equipement.PRIX 	= Number(equipement.PRIX_C);				
					}
					else
					{
						if(cboxEngagement.selectedItem != null)
						{
							switch(cboxEngagement.selectedItem.CODE)
							{
								case "EDM": 	equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_2));
												equipement.PRIX 	= Number(equipement.PRIX_2);
												break;
								case "EVQM":	equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_3));
												equipement.PRIX 	= Number(equipement.PRIX_3);
												break;
								case "ETSM": 	equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_4));
												equipement.PRIX 	= Number(equipement.PRIX_4);
												break;
								case "EQHM": 	equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_5));
												equipement.PRIX 	= Number(equipement.PRIX_5);
												break;
								case "EDMAR": 	equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_6));
												equipement.PRIX 	= Number(equipement.PRIX_6);
												break;
								case "EVQMAR":	equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_7));
												equipement.PRIX 	= Number(equipement.PRIX_7);
												break;
								case "ETSMAR": 	equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_8));
												equipement.PRIX 	= Number(equipement.PRIX_8);
												break;
								case "EQHMAR": 	equipement.PRIX_S	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_9));
												equipement.PRIX 	= Number(equipement.PRIX_9);
												break;
								case "EVQMFCP": if(isRenouvellement)
												{
													equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_7));
													equipement.PRIX 	= Number(equipement.PRIX_7);
												}
												else
												{
													equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_3));
													equipement.PRIX 	= Number(equipement.PRIX_3);
												}
												break;
								default :		equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_C));
												equipement.PRIX 	= Number(equipement.PRIX_C);
												break;
							}
						}
					}
				
				}
			}
		}


		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && 	( 
				(item.NIVEAU != null && item.NIVEAU.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
				||
				(item.LIBELLE != null && item.LIBELLE.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
				||
				(item.PRIX_S != null && item.PRIX_S.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
				||
				(item.PRIX_CS != null && item.PRIX_CS.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
				||
				(item.REF_REVENDEUR != null && item.REF_REVENDEUR.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
				
				
			);
			
			return rfilter;
		}

	}
}