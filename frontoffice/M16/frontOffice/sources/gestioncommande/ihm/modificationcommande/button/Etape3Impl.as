package gestioncommande.ihm.modificationcommande.button
{//MODIFIER COMMANDE
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.colorDatagrid.ColorDatagrid;
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.PopUpFicheProduitAccessoireIHM;
	import gestioncommande.services.EquipementService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class Etape3Impl extends AbstractWorflow implements IWorflow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListAccessoire			:ColorDatagrid;
		
		public var txtptSearch				:TextInput;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var _cmd						:Commande;
		
		private var _myElements				:ElementsCommande2= new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _equipementsSrv			:EquipementService = new EquipementService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var accessoire				:ArrayCollection = new ArrayCollection();
		
		public var goprevious				:Boolean = true;
		
		public var selectedNumber			:Number = 0;
		public var totalNumber				:Number = 0;
		
		public var phoneLibelle				:String = "";
		public var nivo						:String = "";
		public var engementLibelle			:String = "";
		
		public var PASS_ETAPE				:Boolean = false;
		
		private var _idRandom				:int = -1;
		private var _idRev					:int = -1;
		private var _idTer					:int = -1;
		private var _idTerminal				:int = -1;
		
		//--------------------------------------------------------------------------------------------//
		//					COCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE					:int = 2;
		
		public var ACCESS					:Boolean = true;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//

		public function Etape3Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function creationCompleteHandler(fl:FlexEvent):void
		{
			addEventListener("DETAILS",	descriptionHandler);
		}
		
		public function showHandler(fl:FlexEvent):void
		{
		}
		
		public function viewStackChange():void
		{
			_cmd 		= SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			findTerminal();
			findEngagement();
			
			if(_idTer != _idTerminal)
			{
				if(_idTerminal == 0)
					getAccessoires();
				else
					getAccessoiresCompatibles();
				
				_idTer = _idTerminal;
			}
			else
			{
				if(accessoire.length == 0)
				{
					if(SessionUserObject.singletonSession.LASTVSINDEX <= IDPAGE)
					{
						dispatchEvent(new Event('NO_ACCESSORIES_NEXT', true));
					}
					else if(SessionUserObject.singletonSession.LASTVSINDEX > IDPAGE)
						{
							dispatchEvent(new Event('NO_ACCESSORIES_PREV', true));
						}
				}
			}
		}
		
		public function refreshStack():void
		{
		}

		public function checkData():Boolean
		{
			goprevious = true;
			_myElements.ACCESSOIRES = new ArrayCollection();
			
			for(var i:int = 0; i < accessoire.length;i++)
			{
				if(accessoire[i].SELECTED)
				{ 
					accessoire[i].IDPARENT = _cmd.IDEQUIPEMENTPARENT;
					_myElements.ACCESSOIRES.addItem(accessoire[i]);
				}
			}
			
			if(_myElements.TERMINAUX[0].IDFOURNISSEUR < 1 && accessoire.length == 0)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_au_moins_un_access'), 'Consoview', null);
				return false;
			}
			else
				return true;
			
			return true;
		}
		
		public function buildParameters():void
		{
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function txtptSearchHandler(e:Event):void
		{
			if(dgListAccessoire.dataProvider != null)
			{
				(dgListAccessoire.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListAccessoire.dataProvider as ArrayCollection).refresh();
			}
		}
		
		protected function dgListAccessoireHandler(me:MouseEvent):void
		{
			if(dgListAccessoire.selectedItem != null)
			{
				if(dgListAccessoire.selectedItem.SELECTED)
					dgListAccessoire.selectedItem.SELECTED = false;
				else if(dgListAccessoire.selectedItem.SUSPENDU_CLT != 1)					
					dgListAccessoire.selectedItem.SELECTED = true;
				
				accessoire.itemUpdated(dgListAccessoire.selectedItem);
			}
			
			calculElements();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getAccessoires():void
		{
			accessoire.removeAll();
			
			_equipementsSrv.fournirListeAccessoires(_cmd.IDREVENDEUR, _cmd.IDPROFIL_EQUIPEMENT);
			_equipementsSrv.addEventListener(CommandeEvent.LISTED_ACCESSOIRES, listeAccessoiresHandler);
		}
		
		private function getAccessoiresCompatibles():void
		{
			accessoire.removeAll();
			
			_equipementsSrv.fournirListeAccessoiresCompatibles(_idTerminal, _cmd.IDPROFIL_EQUIPEMENT);
			_equipementsSrv.addEventListener(CommandeEvent.LISTED_ACCESSOIRES, listeAccessoiresHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function listeAccessoiresHandler(cmde:CommandeEvent):void
		{
			accessoire.removeAll();
			
			accessoire = _equipementsSrv.listeAccessoires;
			
			if(accessoire.length == 0)
			{
				if(SessionUserObject.singletonSession.LASTVSINDEX <= IDPAGE)
				{
					dispatchEvent(new Event('NO_ACCESSORIES_NEXT', true));
				}
				else if(SessionUserObject.singletonSession.LASTVSINDEX > IDPAGE)
				{
					dispatchEvent(new Event('NO_ACCESSORIES_PREV', true));
				}
			}
			
			getAccessoiresSelected(accessoire);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function descriptionHandler(e:Event):void
		{
			if(dgListAccessoire.selectedItem != null)
			{
				var popUpDetails:PopUpFicheProduitAccessoireIHM = new PopUpFicheProduitAccessoireIHM();
					popUpDetails.equipement 					= dgListAccessoire.selectedItem;
				
				PopUpManager.addPopUp(popUpDetails, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION
		//--------------------------------------------------------------------------------------------//
		
		public function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint			
		{				
			var rColor:uint;				
			rColor = color;				
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			
			if (item != null )				
			{				
				if(item.SUSPENDU_CLT == 1)//En repture
				{	
					rColor = 0xF7FF04;
				}			
				else					
					rColor = color;					
			}				
			return rColor;				
		}
		
		private function findTerminal():void
		{
			var len	:int = _myElements.TERMINAUX.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(_myElements.TERMINAUX[i].IDEQUIPEMENT != 71)
				{
					phoneLibelle = _myElements.TERMINAUX[i].LIBELLE;
					_idTerminal  = _myElements.TERMINAUX[i].IDFOURNISSEUR;
					nivo 		 = _myElements.TERMINAUX[i].NIVEAU;
					return;
				}
			}
		}
		
		private function findEngagement():void
		{
			if(_myElements.ENGAGEMENT.FPC != "")
				engementLibelle =  giveMeDate();
			else
			{
				if(_myElements.ENGAGEMENT.DUREE == "")
					engementLibelle = ResourceManager.getInstance().getString('M16', 'Aucune');
				else
					engementLibelle = _myElements.ENGAGEMENT.DUREE;
			}
		}
		
		private function getAccessoiresSelected(values:ArrayCollection):void
		{
			var lenSel	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.ACCESSOIRES.length;
			var lenAcc	:int = values.length;
			
			for(var i:int = 0;i < lenAcc;i++)
			{
				var idAcc:int = values[i].IDFOURNISSEUR;
				
				for(var j:int = 0;j < lenSel;j++)
				{
					if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.ACCESSOIRES[j].IDFOURNISSEUR == idAcc)
						values[i].SELECTED = true;
				}	
			}
			
			calculElements();
		}
		
		private function calculElements():void
		{
			var len	:int = accessoire.length;
			var cpt	:int = 0;
			var tot	:Number = 0;

			
			for(var i:int = 0;i < len;i++)
			{
				if(accessoire[i].SELECTED)
				{
					tot = tot + Number(accessoire[i].PRIX);
					cpt++;
				}
			}
			
			totalNumber 	= tot;
			selectedNumber	= cpt;
		}
		
		private function giveMeDate():String
		{
			var fpc:String 		= _myElements.ENGAGEMENT.FPC;
			var day:String 		= fpc.substr(8,2);
			var month:String	= fpc.substr(5,2);
			var year:String		= fpc.substr(0,4);
			
			return day + "/" + month + "/" + year;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && 	( 
										(item.LIBELLE != null && item.LIBELLE.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.REF_REVENDEUR != null && item.REF_REVENDEUR.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.PRIX != null && item.PRIX.toString().toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
									);
			
			return rfilter;
		}

	}
}