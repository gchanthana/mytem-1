package gestioncommande.ihm.modificationcommande.button
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.entity.RessourcesNumber;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.PopUpChoiceOptionsIHM;
	import gestioncommande.popup.PopUpChoiceSubscriptionIHM;
	import gestioncommande.popup.SelectionTypeLigneIHM;
	import gestioncommande.services.EquipementService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import session.SessionUserObject;
	
	
	[Bindable]
	public class Etape4Impl extends AbstractWorflow implements IWorflow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cboxEngagement				:ComboBox;
		
		public var dgListOptions				:DataGrid;
		
		public var boxVisible					:Box;
		
		public var btnChoisirAbo				:Button;
		public var btnChoisirOpt				:Button;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUP
		//--------------------------------------------------------------------------------------------//
		
		private var _popUpSubscrib				:PopUpChoiceSubscriptionIHM;		
		private var _popUpOptions				:PopUpChoiceOptionsIHM;	
		private var _popUpType					:SelectionTypeLigneIHM;				
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var  _cmd 						:Commande;
		
		private var _myElements					:ElementsCommande2	= new ElementsCommande2();
		
		private var _cartesim					:EquipementsElements = null;
		
		private var _ressNumber					:RessourcesNumber;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _equipements				:EquipementService 	= new EquipementService();

		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		//---VARIABLES
		public var strgAbo						:String = "";
		public var strgPrixAbo					:String = "";
		public var strgTypeLigne				:String = "";
		
		public var isRenouvellement				:Boolean = false;
		public var isEnable						:Boolean = true;
		public var isCmdMobile					:Boolean = false;
		
		public var listOptions					:ArrayCollection;
		public var listEngagement				:ArrayCollection;

		public var isEngagementVisible			:Boolean = false;	
		public var isFicheLigneVisible			:Boolean = false;	
		
		private var _isAboReturn				:Boolean = false;
		private var _isOptReturn				:Boolean = false;
		private var _isEngReturn				:Boolean = false;
		private var _isSimReturn				:Boolean = false;
		
		private var _idRandom					:int 	= -1;
		
		//--------------------------------------------------------------------------------------------//
		//					COCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE						:int = 3;
		public var ACCESS						:Boolean = true;
		
		private var _libelle					:String  = ResourceManager.getInstance().getString('M16','_Abonnements');
		private var _libelleButton				:String  = "";
		private var _visibleButton				:Boolean = true;
		private var _idButton					:int	 = 4;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//		
		
		public function Etape4Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function creationCompleteHandler(fl:FlexEvent):void
		{
			addEventListener("REMOVE_OPTIONS", removeOptionsHandler);
		}
		
		public function showHandler(fl:FlexEvent):void
		{
		}
		
		public function viewStackChange():void
		{
			_cmd 			= SessionUserObject.singletonSession.COMMANDE;
			_myElements 	= SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			if(_idRandom != SessionUserObject.singletonSession.CURRENTCONFIGURATION.RANDOM)
			{
				_idRandom 	= _myElements.RANDOM;
				listOptions = _myElements.OPTIONS;
				
				_ressNumber = new RessourcesNumber(_cmd.IDOPERATEUR, _cmd.IDPROFIL_EQUIPEMENT);
				
				if(SessionUserObject.singletonSession.IDSEGMENT > 1)
				{
					strgTypeLigne = _myElements.TYPELIGNE;
					isFicheLigneVisible = true;
				}
				else
				{
					strgTypeLigne = '';
					isFicheLigneVisible = false;
				}
				
				if(_cmd.IDTYPE_COMMANDE != 1382 && _cmd.IDTYPE_COMMANDE != 1587)
				{
					if(_myElements.ABONNEMENTS.length > 0)
					{
						var ref:String = (_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT)?_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT + ' | ' :'';
						strgAbo 	=  ref  + _myElements.ABONNEMENTS[0].LIBELLE;
						strgPrixAbo = _myElements.ABONNEMENTS[0].PRIX_STRG;
					}
					else
						btnChoisirClickHandler(new MouseEvent(MouseEvent.CLICK));
					
					isRenouvellement = false;
				}
				else
					isRenouvellement = true;
				
				if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
				{
					getEngagements();
					isEngagementVisible = true;
				}
				else
					isEngagementVisible = false;
				
				if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2 && SessionUserObject.singletonSession.IDSEGMENT == 1)
					getCarteSim();
			}
		}

		public function checkData():Boolean
		{
			var bool:Boolean = false;
			
			if(_cmd.IDTYPE_COMMANDE != 1382 && _cmd.IDTYPE_COMMANDE != 1587)
			{
				if(_myElements.ABONNEMENTS.length > 0)
					bool = true;
				else
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_un_abonnement__'), 'Consoview', null);
					return false;
				}
			}
			else
				bool = true;
			
			if(SessionUserObject.singletonSession.IDSEGMENT > 1 && _myElements.IDTYPELIGNE == 0)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_type_de_ligne'), 'Consoview', null);
				return false;
			}
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
			{
				if((SessionUserObject.singletonSession.IDSEGMENT == 1) && (cboxEngagement.selectedItem == null))
				{
					bool = false;
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_engagement_'), 'Consoview', null);
				}
				else if (cboxEngagement.selectedItem != null) 					
					_myElements.ENGAGEMENT = cboxEngagement.selectedItem as Engagement;
			}
			
			return bool;
		}
		
		public function buildParameters():void
		{
		}
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		//OUVRE LA POPUP PERMETTANT DE SELECTIONNER LES ABONNEMENTS
		protected function btnChoisirClickHandler(me:MouseEvent):void
		{
			_popUpSubscrib 			  = new PopUpChoiceSubscriptionIHM();
			_popUpSubscrib.cmd 		  = _cmd;
			_popUpSubscrib.myElements = _myElements;
			
			_popUpSubscrib.addEventListener("POPUP_ABONNEMENTS_CLOSED_AND_VALIDATE", closedPopUpSubscribHandler);
			
			PopUpManager.addPopUp(_popUpSubscrib, this.parent.parent.parent, true);
			PopUpManager.centerPopUp(_popUpSubscrib);
		}
		
		//OUVRE LA POPUP PERMETTANT DE SELECTIONNER LES OPTIONS
		protected function btnOptionsClickHandler(me:MouseEvent):void
		{
			_popUpOptions 			 = new PopUpChoiceOptionsIHM();
			_popUpOptions.cmd 		 = _cmd;
			_popUpOptions.myElements = _myElements;
			
			_popUpOptions.addEventListener("POPUP_OPTIONS_CLOSED_AND_VALIDATE", closedPopUpOptionsHandler);
			
			PopUpManager.addPopUp(_popUpOptions, this.parent.parent.parent, true);
			PopUpManager.centerPopUp(_popUpOptions);
		}
		
		//OUVRE LA POPUP PERMETTANT DE SELECTIONNER LA FICHE LIGNE
		protected function btnFicheLigneClickHandler(me:MouseEvent):void
		{
			_popUpType = new SelectionTypeLigneIHM();
			_popUpType.myElements = _myElements
			_popUpType.idAbonnement 		= _myElements.ABONNEMENTS[0].IDCATALOGUE;
			_popUpType.libelleAbonnement	= _myElements.ABONNEMENTS[0].LIBELLE;
			
			_popUpType.addEventListener("GET_TYPELIGNE", getTypeLigneHandler); 
			
			PopUpManager.addPopUp(_popUpType, this.parent.parent.parent, true);
			PopUpManager.centerPopUp(_popUpType);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getEngagements():void
		{
			if(_cmd.IDTYPE_COMMANDE == 1382 || _cmd.IDTYPE_COMMANDE == 1587)
			{
				isRenouvellement = true;
				_equipements.fournirReengagement(_cmd.IDOPERATEUR);
			}
			else
			{
				isRenouvellement = false;
				_equipements.fournirNouvelleEngagement(_cmd.IDOPERATEUR);
			}
			
			_equipements.addEventListener(CommandeEvent.LISTED_ENGAGEMENTS, listEngagementHandler);
		}	
		
		private function getCarteSim():void
		{
			_equipements.fournirSimCard(_cmd.IDREVENDEUR, _cmd.IDOPERATEUR, _cmd.IDPROFIL_EQUIPEMENT);
			_equipements.addEventListener(CommandeEvent.LISTED_SIM, simHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function listEngagementHandler(cmde:CommandeEvent):void
		{
			if(_equipements.listEngagement.length > 1)
				cboxEngagement.prompt = ResourceManager.getInstance().getString('M16','Choisir_la_dur_e_d_engagement');
			else
			{
				if(_equipements.listEngagement.length == 0)
					cboxEngagement.prompt = ResourceManager.getInstance().getString('M16', 'Aucun_engagement');
				else
					cboxEngagement.prompt = "";
			}

			cboxEngagement.dataProvider = _equipements.listEngagement;
			
			checkLastEngagement();
		}
		
		private function simHandler(cmde:CommandeEvent):void
		{
			_cartesim = _equipements.simcard;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM FERMETURE POPUPs
		//--------------------------------------------------------------------------------------------//
		
		//---> AFFECTE LES OPTIONS SELECTIONNEES A '_myElements.ABO' ET 
		//---> AFFICHE LE DATAGRID LORS DE LA FERMETURE DE LA POPUP 'Sélectionnez vos abonnements'
		private function closedPopUpSubscribHandler(e:Event = null):void
		{
			if(_myElements.ABONNEMENTS.length > 0)
			{
				var ref:String = (_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT)?_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT + ' | ' :'';
				strgAbo 	=  ref  + _myElements.ABONNEMENTS[0].LIBELLE;
				strgPrixAbo = _myElements.ABONNEMENTS[0].PRIX_STRG;
				
				boxVisible.visible = isEnable = true;
			}
			else
			{
				boxVisible.visible = isEnable = false;
				_myElements.IDTYPELIGNE = 0;
				_myElements.TYPELIGNE 	= '';
			}
		}		
		
		//---> AFFECTE LES OPTIONS SELECTIONNEES A '_myElements.OPTIONS' ET 
		//---> AFFICHE LE DATAGRID LORS DE LA FERMETURE DE LA POPUP 'Sélectionnez vos options'
		private function closedPopUpOptionsHandler(e:Event = null):void
		{
			listOptions = _myElements.OPTIONS;
			(dgListOptions.dataProvider as ArrayCollection).refresh();
		}
		
		private function getTypeLigneHandler(e:Event):void
		{
			strgTypeLigne = _myElements.TYPELIGNE;
			
			PopUpManager.removePopUp(_popUpType);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function removeOptionsHandler(e:Event):void
		{
			if(dgListOptions.selectedItem != null)
				(dgListOptions.dataProvider as ArrayCollection).removeItemAt(dgListOptions.selectedIndex);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//

		private function checkIfOnly():void
		{
			var isPass:Boolean = false;
			
			if(_isAboReturn && _isEngReturn && _isOptReturn && _isSimReturn)
			{
				if(_cmd.IDTYPE_COMMANDE == TypesCommandesMobile.RENOUVELLEMENT || _cmd.IDTYPE_COMMANDE == TypesCommandesMobile.RENOUVELLEMENT_FIXE)
				{
					if(_ressNumber.NB_OPT == 0)
						isPass = true;
					else
						isPass = false;
				}
				else
				{
					if(_ressNumber.NB_ABO == 1)
					{
						isPass = true;
					}
					else if(_ressNumber.NB_ABO > 1 && _ressNumber.NB_ABO_OBL > 0)
						{
							isPass = true;
						}
					
					btnChoisirAbo.enabled = !isPass;
					
					if(isPass)
					{
						if(_ressNumber.NB_OPT == 0 || _ressNumber.NB_OPT == _ressNumber.NB_OPT_OBL)
							isPass = true;
						else
							isPass = false;
					}
					
					btnChoisirOpt.enabled = !isPass;
					
					if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					{
						isCmdMobile=true;
						if(isEngagementVisible)
						{
							if(isPass && _equipements.listEngagement.length == 1)
								isPass = true;
							else
								isPass = false;
						}
					}
					else
						isPass = false;//CETTE ETAPE NE PEUT PAS ETRE SAUTE DANS LE FIXE/RESEAU
				}
			}
			
			if(isPass)
				dispatchEvent(new Event('ONE_ENGAGEMENT_RESSOURCES', true));
		}
		
		private function checkLastEngagement():void
		{
			var engage	:ArrayCollection = _equipements.listEngagement;
			var i		:int 			 = 0;
			var len		:int 			 = engage.length;
			
			for(i = 0;i < len;i++)
			{
				if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.ENGAGEMENT.VALUE == engage[i].VALUE)
					cboxEngagement.selectedIndex = i;
			}
		}

	}
}