package gestioncommande.ihm
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.containers.Box;
	import mx.containers.ViewStack;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	
	import commandemobile.ihm.MainNewCommandeIHM;
	
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	
	import gestioncommande.entity.LignesByOperateur;
	import gestioncommande.entity.MultiCompte;
	import gestioncommande.entity.MultiSousCompte;
	import gestioncommande.entity.Pool;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.TypeCommandeEvent;
	import gestioncommande.events.TypeOperationSurLigneIHMEvent;
	import gestioncommande.events.ViewStackEvent;
	import gestioncommande.ihm.commandemobilefixereseau.MainNouvelleCommandeIHM;
	import gestioncommande.ihm.commandemobilefixereseau.MainNouvelleCommandeServiceIHM;
	import gestioncommande.ihm.historique.HistoriqueIHM;
	import gestioncommande.ihm.listecommande.ListeCommandeIHM;
	import gestioncommande.ihm.modificationcommande.MainModificationIHM;
	import gestioncommande.popup.OperationSurLigneIHM;
	import gestioncommande.popup.SelectTypeCommandeIHM;
	import gestioncommande.services.EligibiliteService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.ListeCommandesService;
	
	import gestionparcmobile.nouvellesim.ihm.MainNouvelleSimIHM;
	import gestionparcmobile.portabilite.ihm.MainPortabiliteLigneIHM;
	import gestionparcmobile.resiliationLigne.ihm.MainResiliationIHM;
	import gestionparcmobile.sav.ihm.MainSavIHM;
	
	import session.SessionUserObject;
	
	
	[Bindable]
	public class SelectViewImpl extends Box
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHM
			//--------------------------------------------------------------------------------------------//
		
		public var vs							:ViewStack;

			//--------------------------------------------------------------------------------------------//
			//					COMPOSANTS VS
			//--------------------------------------------------------------------------------------------//
				
		public var boxCommande					:Box;
		
		public var historiqueCommande			:HistoriqueIHM;
		
		public var listeCommande				:ListeCommandeIHM;
		
			//--------------------------------------------------------------------------------------------//
			//					COMPOSANTS COMMANDE
			//--------------------------------------------------------------------------------------------//
		
		private var _etapeCommandeMobile		:MainNewCommandeIHM = null;
		
		private var _etapeCommandeFixe			:MainNouvelleCommandeIHM = null;
		
		private var _etapeResiliation			:MainResiliationIHM = null;
		
		private var _etapeModification			:MainModificationIHM = null;
		
		private var _etapeCommandeSPIE			:MainNouvelleCommandeServiceIHM = null;
		
		private var _etapeNouvelleSim			:MainNouvelleSimIHM = null;
		
		private var _etapeSAV					:MainSavIHM = null;
		
		private var _etapePortabilite			:MainPortabiliteLigneIHM = null;
		
			//--------------------------------------------------------------------------------------------//
			//					POPUPS
			//--------------------------------------------------------------------------------------------//
		
		private var _popupSelectTypeCommande	:SelectTypeCommandeIHM = null;	
		
		private var _popUpOperationSurLigne		:OperationSurLigneIHM;
			
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//

		public var _infosCmdSrv				:ListeCommandesService = new ListeCommandesService();

		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		private var etapeToDoFUnction			:Function;
		private var _lignesSelected				:ArrayCollection = new ArrayCollection();
		private var _listeMultiCompte			:ArrayCollection = new ArrayCollection();
		private var _arrayComptes				:ArrayCollection = new ArrayCollection();
		private var _arraySousComptes			:ArrayCollection = new ArrayCollection();
		private var _comptesSousComptes			:ArrayCollection = new ArrayCollection();
		
		private var _operateurSelected			:Object = new Object();
		
		//FALSE DEFAUT LISTE DES COMMANDES ; TRUE => NVL COMMANDE;
		private var boolFaireNouvelleCommande	:Boolean = false;
		
		private var _isMultiComptes				:Boolean = false;
		private var _isMultiSousComptes			:Boolean = false;
		private var _isNoCompte					:Boolean = false;
		
		private var _isModifLigne				:Boolean = false;
		
		private var _idtypecommande				:int = 0;
		private var _idsegement					:int = 1;
		private var _idsoustete					:int = 0;		
		private var selectedIndex				:int = 0;
		
		private const SPIE_1					:Number = 5821693;
		private const SPIE_2					:Number = 6149989;
		
		private const TYPE_SAV					:int = 13;		
		private const TYPE_PORTABILITE			:int = 12;
		private const TYPE_SIM					:int = 11;
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function SelectViewImpl()
		{
			var obj	:Object = CvAccessManager.getSession().INFOS_DIVERS;
			var groupIdx:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			
			SessionUserObject.singletonSession 		= new SessionUserObject();
			SessionUserObject.singletonSession.POOL = new Pool();

			if(obj != null)
			{
				if(obj.hasOwnProperty("BOOLNEWCOMMANDE"))
				{
					boolFaireNouvelleCommande = (CvAccessManager.getSession().INFOS_DIVERS.BOOLNEWCOMMANDE)?true:false;

					if(boolFaireNouvelleCommande)
					{
						SessionUserObject.singletonSession.POOL = SessionUserObject.singletonSession.POOLSELECTED 	= mappPool(CvAccessManager.getSession().INFOS_DIVERS.POOL);
						
						_idsegement 	= CvAccessManager.getSession().INFOS_DIVERS.IDSEGMENT;
						_idtypecommande 	= CvAccessManager.getSession().INFOS_DIVERS.IDTYPECOMMANDE;
						_idsoustete		= CvAccessManager.getSession().INFOS_DIVERS.IDSOUSTETE;
					
						SessionUserObject.singletonSession.IDSEGMENT = _idsegement;
						
						var lgneOpe:LignesByOperateur 	= new LignesByOperateur();
							lgneOpe.IDPOOL 				= SessionUserObject.singletonSession.POOL.IDPOOL;
							lgneOpe.IDCOLLABORATEUR 	= CvAccessManager.getSession().INFOS_DIVERS.IDCOLLABORATEUR;
							lgneOpe.IDEMPL				= CvAccessManager.getSession().INFOS_DIVERS.IDCOLLABORATEUR;
							lgneOpe.COLLABORATEUR 		= CvAccessManager.getSession().INFOS_DIVERS.LIBELLECOLLABORATEUR;
							lgneOpe.IDOPERATEUR 		= CvAccessManager.getSession().INFOS_DIVERS.IDOPERATEUR;
							lgneOpe.OPERATEUR 			= CvAccessManager.getSession().INFOS_DIVERS.LIBELLEOPERATEUR;
							lgneOpe.IMEI 				= CvAccessManager.getSession().INFOS_DIVERS.IMEI;
							
						//--- 1-> NOUVELLE LIGNE OU EQ (mobile, fixe, data), 2-> REENGAGEMENT, 3-> MODIFICATION OPTION, 4-> SUSPENSION, 5-> REACTIVATION, 6-> RESILIATION
						if(_idtypecommande == 1)
						{
							_isModifLigne = false;
							var isCommandSpie:Boolean = ((groupIdx == this.SPIE_1) || (groupIdx == this.SPIE_2))? true:false;
							switch(_idsegement)
							{
								case 1 : this.etapeToDoFUnction = etapeCommandeMobile; 		break;
								case 2 : this.etapeToDoFUnction = etapeCommandeFixeReseau;	break;
								case 3 : this.etapeToDoFUnction = (isCommandSpie == true)? etapeCommandeFixeReseauSPIE:etapeCommandeFixeReseau; break;
							}
						}
						else
						{
							_isModifLigne = true;

							switch(_idtypecommande)
							{
								case 2 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 5;	break;//--> REENGAGEMENT
								case 3 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 6;	break;//--> MODIFICATION OPTION
								case 4 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 7;	break;//--> SUSPENSION
								case 5 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 8;	break;//--> REACTIVATION
								case 6 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 9;	break;//--> RESILIATION
								// Commande d'une nouvelle carte SIM à partir de M111
								case 11 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 11;	break;//--> Commande Nouvelle SIM
								case 12 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 12;	break;//--> Portabilité de la ligne
								case 13 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 13;	break;//--> SAV
							}
							
							//var lbo:LignesByOperateur = new LignesByOperateur();
							
							if(CvAccessManager.getSession().INFOS_DIVERS.IDOPERATEUR > 0 
								&& _idsoustete > 0)
							{
								lgneOpe.classProcEligibilite.checkEligibiliteLignesByLignesAndGetCompteSousCompteM111(_idsoustete, lgneOpe, CvAccessManager.getSession().INFOS_DIVERS.IDOPERATEUR);
								lgneOpe.addEventListener('INFOS_LIGNE_REFRESH', refreshInfosLigneHandler);
							}
							else if(_idtypecommande == TYPE_SAV && _idsoustete == 0){
								etapeToDoFUnction = toDoSAV;
								
								lgneOpe.SELECTED = true;
								_isNoCompte = true;//pas de compte sous compte car pas d'opérateur
								_lignesSelected	= new ArrayCollection();
								_lignesSelected.addItem(lgneOpe);
								
								_operateurSelected 					= new Object();
								_operateurSelected.LIBELLE 			= lgneOpe.OPERATEUR;
								_operateurSelected.OPERATEURID 		= lgneOpe.IDOPERATEUR;
								_operateurSelected.IDFOURNISSEUR 	= 0;
								
								SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS = new ArrayCollection();
								SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS = ObjectUtil.copy(_lignesSelected) as ArrayCollection;//operationSurLigneM111();
							
							
							}else
							{
								lgneOpe.classProcEligibilite.getOperateurLastFactureLigneM111(_idsoustete);
								lgneOpe.classProcEligibilite.addEventListener(CommandeEvent.SOUSTETE_OPERATEUR, opertateurLigneHandler);
							}
						}
						
						getInfosLoad(SessionUserObject.singletonSession.POOL.IDPOOL, SessionUserObject.singletonSession.POOL.IDPROFIL);
					}
					else
					{
						CvAccessManager.getSession().INFOS_DIVERS.BOOLNEWCOMMANDE = false;
						boolFaireNouvelleCommande 	= false;
						_isModifLigne 				= true;
						
						getInfosLoad();
					}
				}
				else
				{
					CvAccessManager.getSession().INFOS_DIVERS.BOOLNEWCOMMANDE = false;
					boolFaireNouvelleCommande 	= false;
					_isModifLigne 				= true;
					
					getInfosLoad();
				}
			}
			else
			{
				CvAccessManager.getSession().INFOS_DIVERS.BOOLNEWCOMMANDE = false;
				boolFaireNouvelleCommande 	= false;
				_isModifLigne 				= true;
				
				getInfosLoad();
			}
			
			CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE = false;
			CvAccessManager.getSession().INFOS_DIVERS.IS_IN_COMMANDE_PROCESS = false;

			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		private function opertateurLigneHandler(e:Event):void
		{
			if((e.currentTarget as EligibiliteService))
			{
				var currentOperateur:Object = (e.currentTarget as EligibiliteService).currentOperateur;
				
				if(currentOperateur == null || !currentOperateur.hasOwnProperty('OPERATEURID') || 
					!currentOperateur.hasOwnProperty('OPERATEUR') || currentOperateur.OPERATEURID == null ||
						currentOperateur.OPERATEURID <= 0)
				{
					ConsoviewAlert.afficherAlertInfo("Aucun opérateur disponible pour cette ligne", 'Consoview', null);
				}
				else
				{
					var lgneOpe:LignesByOperateur 	= new LignesByOperateur();
						lgneOpe.IDPOOL 				= SessionUserObject.singletonSession.POOL.IDPOOL;
						lgneOpe.IDCOLLABORATEUR 	= CvAccessManager.getSession().INFOS_DIVERS.IDCOLLABORATEUR;
						lgneOpe.IDEMPL				= CvAccessManager.getSession().INFOS_DIVERS.IDCOLLABORATEUR;
						lgneOpe.COLLABORATEUR 		= CvAccessManager.getSession().INFOS_DIVERS.LIBELLECOLLABORATEUR;
						lgneOpe.IDOPERATEUR 		= currentOperateur.OPERATEURID;
						lgneOpe.OPERATEUR 			= currentOperateur.OPERATEUR;
	
					var lbo:LignesByOperateur = new LignesByOperateur();
						lbo.classProcEligibilite.checkEligibiliteLignesByLignesAndGetCompteSousCompteM111(_idsoustete, lgneOpe, currentOperateur.OPERATEURID);
						lbo.addEventListener('INFOS_LIGNE_REFRESH', refreshInfosLigneHandler);
				}
			}
		}
		
		private function refreshInfosLigneHandler(e:Event):void
		{
			if((e.currentTarget as LignesByOperateur).classProcEligibilite)
			{	
				_lignesSelected	= new ArrayCollection();
				_lignesSelected.addItem((e.currentTarget as LignesByOperateur).classProcEligibilite.myLigneOperateur);
				
				if(checkLignesSelected())
					operationSurLigneM111();
			}
		}

		private function checkLignesSelected():Boolean
		{			
			if(_lignesSelected[0].IDCOMPTE_FACTURATION == 0 || _lignesSelected[0].IDSOUS_COMPTE == 0)
				_isNoCompte = true;
			else
				_isNoCompte = false;

			_operateurSelected 					= new Object();
			_operateurSelected.LIBELLE 			= _lignesSelected[0].OPERATEUR;
			_operateurSelected.OPERATEURID 		= _lignesSelected[0].IDOPERATEUR;
			_operateurSelected.IDFOURNISSEUR 	= 0;
			
			var myComptesObject:Object = getCompteSousCompte(_lignesSelected, 'IDCOMPTE_FACTURATION');
			
			_arrayComptes   = myComptesObject.LISTE;
			_isMultiComptes = myComptesObject.MULTI;
			
			var mySousComptesObject:Object = getCompteSousCompte(_lignesSelected, 'IDSOUS_COMPTE');
			
			_arraySousComptes   = mySousComptesObject.LISTE;
			_isMultiSousComptes = mySousComptesObject.MULTI;
			
			var lenMulti:int = _listeMultiCompte.length;
			
			_comptesSousComptes = new ArrayCollection();
			
			SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS = new ArrayCollection();
			SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS = ObjectUtil.copy(_lignesSelected) as ArrayCollection;
			
			for(var i:int = 0;i < lenMulti;i++)
			{
				_comptesSousComptes.addItem(getMultiSousCompte(_listeMultiCompte[i], _listeMultiCompte[i].LIGNES));
			}
			
			return true;
		}
		
		private function getCompteSousCompte(values:ICollectionView, labelField:String):Object
		{
			var liste	:ArrayCollection = new ArrayCollection();
			var facture	:ICollectionView = ObjectUtil.copy(_lignesSelected) as ICollectionView;
			var object	:Object = new Object();
			var multi	:MultiCompte;
			
			facture = Formator.sortFunction(facture as ArrayCollection, labelField) as ICollectionView;
			
			object.MULTI = false;;
			
			if(facture != null)
			{
				var cursor:IViewCursor = facture.createCursor();
				
				while(!cursor.afterLast)
				{
					var integer:int = ConsoviewUtil.getIndexById(liste, labelField, cursor.current[labelField])
					
					if(integer < 0)
					{
						liste.addItem(cursor.current);
						
						if(labelField == 'IDCOMPTE_FACTURATION')
						{
							multi = new MultiCompte();
							multi.COMPTE_FACTURATION 	= cursor.current.COMPTE_FACTURATION;
							multi.IDCOMPTE_FACTURATION	= cursor.current.IDCOMPTE_FACTURATION;
							multi.NUMBER_OF_LINES 		= multi.LIGNES.length;
							multi.LIGNES.addItem(cursor.current);
							
							_listeMultiCompte.addItem(multi);							
						}
					}
					else
					{
						if(labelField == 'IDCOMPTE_FACTURATION')
							multi.LIGNES.addItem(cursor.current);
					}
					
					cursor.moveNext();
				}
				
				if(liste.length > 1)
					object.MULTI = true;
			}
			
			object.LISTE = liste;
			
			return object;
		}
		
		private function getMultiSousCompte(object:Object, compte:ICollectionView):Object
		{
			var liste		:ArrayCollection = new ArrayCollection();
			var multi		:MultiSousCompte;
			var lblField	:String = 'IDSOUS_COMPTE';
			
			compte = Formator.sortFunction(compte as ArrayCollection, lblField) as ICollectionView;
			
			if(compte != null)
			{
				var cursor:IViewCursor = compte.createCursor();
				
				while(!cursor.afterLast)
				{
					var integer:int = ConsoviewUtil.getIndexById(liste, lblField, cursor.current[lblField])
					
					if(integer < 0)
					{
						if(cursor.current.IDSOUS_COMPTE > -1)
						{
							multi = new MultiSousCompte();
							
							multi.SOUS_COMPTE 			= cursor.current.SOUS_COMPTE;
							multi.IDSOUS_COMPTE			= cursor.current.IDSOUS_COMPTE;
							multi.LIGNES.addItem(cursor.current);
							
							object.MULTI_SOUS_COMPTE.addItem(multi);
							
							liste.addItem(cursor.current);
						}
					}
					else
						multi.LIGNES.addItem(cursor.current);					
					
					cursor.moveNext();
				}
			}
			
			return object;
		}

		private function operationSurLigneM111():void
		{
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 11)
				toDoNouvelleSim();
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 12)
				toDoPortabilite(); 
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 13)
				toDoSAV(); 
			else
				toDoResiliation();
		}
		
		private function toDoPortabilite():void
		{
			_etapePortabilite 						= new MainPortabiliteLigneIHM();
			_etapePortabilite.objectPool			= SessionUserObject.singletonSession.POOL;
			
			_etapePortabilite.objectOperateur		= this._operateurSelected;
			_etapePortabilite.compte				= this._arrayComptes;
			_etapePortabilite.sousCompte			= this._arraySousComptes;
			_etapePortabilite.compteSousCompte		= this._comptesSousComptes;
			_etapePortabilite.isMultiCompte			= this._isMultiComptes;
			_etapePortabilite.isMultiSousCompte		= this._isMultiSousComptes;
			_etapePortabilite.isNoCompte			= this._isNoCompte;
			_etapePortabilite.ligne					= this._lignesSelected;
			
			etapePortabilite();
		}
		
		private function toDoSAV():void
		{
			if(_etapeSAV == null){
				_etapeSAV 						= new MainSavIHM();
				
			}
			
			_etapeSAV.objectPool			= SessionUserObject.singletonSession.POOL;
			
			_etapeSAV.objectOperateur		= this._operateurSelected;
			_etapeSAV.compte				= this._arrayComptes;
			_etapeSAV.sousCompte			= this._arraySousComptes;
			_etapeSAV.compteSousCompte		= this._comptesSousComptes;
			_etapeSAV.isMultiCompte			= this._isMultiComptes;
			_etapeSAV.isMultiSousCompte		= this._isMultiSousComptes;
			_etapeSAV.isNoCompte			= this._isNoCompte;
			_etapeSAV.ligne					= this._lignesSelected;
			etapeSAV();
		}
		 
		private function toDoNouvelleSim():void
		{
			_etapeNouvelleSim 						= new MainNouvelleSimIHM();
			_etapeNouvelleSim.objectPool			= SessionUserObject.singletonSession.POOL;
			
			_etapeNouvelleSim.objectOperateur		= this._operateurSelected;
			_etapeNouvelleSim.compte				= this._arrayComptes;
			_etapeNouvelleSim.sousCompte			= this._arraySousComptes;
			_etapeNouvelleSim.compteSousCompte		= this._comptesSousComptes;
			_etapeNouvelleSim.isMultiCompte			= this._isMultiComptes;
			_etapeNouvelleSim.isMultiSousCompte		= this._isMultiSousComptes;
			_etapeNouvelleSim.isNoCompte			= this._isNoCompte;
			
			etapeNouvelleSim();
		}
		private function toDoResiliation():void
		{
			_etapeResiliation 						= new MainResiliationIHM();
			_etapeResiliation.objectPool			= SessionUserObject.singletonSession.POOL;
			_etapeResiliation.fromM16				= false;
			_etapeResiliation.objectOperateur		= this._operateurSelected;
			_etapeResiliation.compte				= this._arrayComptes;
			_etapeResiliation.sousCompte			= this._arraySousComptes;
			_etapeResiliation.compteSousCompte		= this._comptesSousComptes;
			_etapeResiliation.isMultiCompte			= this._isMultiComptes;
			_etapeResiliation.isMultiSousCompte		= this._isMultiSousComptes;
			_etapeResiliation.isNoCompte			= this._isNoCompte;
			
			etapeResiliation();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTIONS PRIVATE 
		//--------------------------------------------------------------------------------------------//

			//--------------------------------------------------------------------------------------------//
			//					LISTENERS EVENTS IHMs
			//--------------------------------------------------------------------------------------------//
		
		private function viewSelectTypeCommandeHandler(tce:TypeCommandeEvent):void
		{
			typeCommandePopUp();
		}
		
		private function viewSelectTypeCommandeSPIEHandler(tce:TypeCommandeEvent):void
		{
			this.typeCommandeSPIEPopUp();
		}

		private function viewHistoriqueCommandeHandler(vse:ViewStackEvent):void
		{
			vs.selectedIndex = 2;
		}
		
		private function viewListeCommandeHandler(vse:ViewStackEvent):void
		{
			CvAccessManager.getSession().INFOS_DIVERS.BOOLNEWCOMMANDE = false;
			CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE   = true;
			
			boolFaireNouvelleCommande = false;
			
			vs.selectedIndex = 0;
			
			razChildrenBox();
			
			if(vse.type == ViewStackEvent.COMMANDE_BUILT)
				listeCommande.refreshListeCommande();
		}
		
		private function viewnewListeCommandeHandler(vse:ViewStackEvent):void
		{
			vs.selectedIndex = 3;
		}
		
		private function viewModificationCommandeHandler(vse:ViewStackEvent):void
		{
			etapeModification();
		}
		
		private function viewListeCommandeOrM11Handler(vse:ViewStackEvent):void
		{
			if(boolFaireNouvelleCommande)
				changeUniversFunction();
			else
				viewListeCommandeHandler(vse);
		}
		
		private function razChildrenBox():void
		{
			boxCommande.removeAllChildren();
			
			_etapeCommandeMobile = null;
			_etapeCommandeFixe 	 = null;
			_etapeResiliation 	 = null;
			_etapeModification	 = null;
		}
		
			//--------------------------------------------------------------------------------------------//
			//					LISTENERS EVENTS POPUPs
			//--------------------------------------------------------------------------------------------//
		
		private function typeCommandeHandler(te:TypeCommandeEvent):void
		{
			var typeCommande	:String = te.typeCommande;
			var typeOperation	:String = te.typeOperation;
			
			CvAccessManager.getSession().INFOS_DIVERS.BOOLNEWCOMMANDE = false;
			
			if(typeOperation == TypeCommandeEvent.OPE_NEWCMD)
			{
				switch(typeCommande)
				{
					case TypeCommandeEvent.TYPE_MOBILE 	: 	SessionUserObject.singletonSession.IDSEGMENT = 1; etapeCommandeMobile(); 	 break;
					case TypeCommandeEvent.TYPE_FIXE 	:  	SessionUserObject.singletonSession.IDSEGMENT = 2; etapeCommandeFixeReseau(); break;
					case TypeCommandeEvent.TYPE_RESEAU 	: 	SessionUserObject.singletonSession.IDSEGMENT = 3; etapeCommandeFixeReseau(); break;
				}
			}
			else
			{
				switch(typeCommande)
				{
					case TypeCommandeEvent.TYPE_MOBILE 	: 	SessionUserObject.singletonSession.IDSEGMENT = 1; modificationLignePopUp(); break;
					case TypeCommandeEvent.TYPE_FIXE 	: 	SessionUserObject.singletonSession.IDSEGMENT = 2; modificationLignePopUp(); break;
					case TypeCommandeEvent.TYPE_RESEAU 	: 	SessionUserObject.singletonSession.IDSEGMENT = 3; modificationLignePopUp(); break;
				}
			}
			
			PopUpManager.removePopUp(_popupSelectTypeCommande);
		}
		
		private function operationResilierLigneHandler(e:Event):void
		{
			//case 2 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 5;	break;//--> REENGAGEMENT
			//case 3 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 6;	break;//--> MODIFICATION OPTION
			//case 4 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 7;	break;//--> SUSPENSION
			//case 5 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 8;	break;//--> REACTIVATION
			//case 6 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 9;	break;//--> RESILIATION
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 8)// Activer ligne
			{
				_etapeResiliation 						= new MainResiliationIHM();
				_etapeResiliation.objectPool			= SessionUserObject.singletonSession.POOL;
				
				_etapeResiliation.objectOperateur		= _popUpOperationSurLigne.operateurSelected;
				_etapeResiliation.compte				= _popUpOperationSurLigne.arrayComptes;
				_etapeResiliation.sousCompte			= _popUpOperationSurLigne.arraySousComptes;
				_etapeResiliation.compteSousCompte		= _popUpOperationSurLigne.comptesSousComptes;
				_etapeResiliation.isMultiCompte			= _popUpOperationSurLigne.isMultiComptes;
				_etapeResiliation.isMultiSousCompte		= _popUpOperationSurLigne.isMultiSousComptes;
				_etapeResiliation.isNoCompte			= _popUpOperationSurLigne.isNoCompte;
				
				etapeResiliation();
			}

			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 7 )// Suspendre ligne
			{
				_etapeResiliation 						= new MainResiliationIHM();
				_etapeResiliation.objectPool			= SessionUserObject.singletonSession.POOL;
				
				_etapeResiliation.objectOperateur		= _popUpOperationSurLigne.operateurSelected;
				_etapeResiliation.compte				= _popUpOperationSurLigne.arrayComptes;
				_etapeResiliation.sousCompte			= _popUpOperationSurLigne.arraySousComptes;
				_etapeResiliation.compteSousCompte		= _popUpOperationSurLigne.comptesSousComptes;
				_etapeResiliation.isMultiCompte			= _popUpOperationSurLigne.isMultiComptes;
				_etapeResiliation.isMultiSousCompte		= _popUpOperationSurLigne.isMultiSousComptes;
				_etapeResiliation.isNoCompte			= _popUpOperationSurLigne.isNoCompte;
				
				etapeResiliation();
				
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 6)// Modification d'option
			{
				_etapeResiliation 						= new MainResiliationIHM();
				_etapeResiliation.objectPool			= SessionUserObject.singletonSession.POOL;
				
				_etapeResiliation.objectOperateur		= _popUpOperationSurLigne.operateurSelected;
				_etapeResiliation.compte				= _popUpOperationSurLigne.arrayComptes;
				_etapeResiliation.sousCompte			= _popUpOperationSurLigne.arraySousComptes;
				_etapeResiliation.compteSousCompte		= _popUpOperationSurLigne.comptesSousComptes;
				_etapeResiliation.isMultiCompte			= _popUpOperationSurLigne.isMultiComptes;
				_etapeResiliation.isMultiSousCompte		= _popUpOperationSurLigne.isMultiSousComptes;
				_etapeResiliation.isNoCompte			= _popUpOperationSurLigne.isNoCompte;
				etapeResiliation();
				//etapeModification();
			}
			else // résiliation et autre
			{
				_etapeResiliation 						= new MainResiliationIHM();
				_etapeResiliation.objectPool			= SessionUserObject.singletonSession.POOL;
				
				_etapeResiliation.objectOperateur		= _popUpOperationSurLigne.operateurSelected;
				_etapeResiliation.compte				= _popUpOperationSurLigne.arrayComptes;
				_etapeResiliation.sousCompte			= _popUpOperationSurLigne.arraySousComptes;
				_etapeResiliation.compteSousCompte		= _popUpOperationSurLigne.comptesSousComptes;
				_etapeResiliation.isMultiCompte			= _popUpOperationSurLigne.isMultiComptes;
				_etapeResiliation.isMultiSousCompte		= _popUpOperationSurLigne.isMultiSousComptes;
				_etapeResiliation.isNoCompte			= _popUpOperationSurLigne.isNoCompte;
				
				etapeResiliation();
			}
		}
		
			
			//--------------------------------------------------------------------------------------------//
			//					ADD CHILDREN
			//--------------------------------------------------------------------------------------------//
		
		private function etapeCommandeMobile():void
		{
			_etapeCommandeMobile = new MainNewCommandeIHM();
			
			CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE = true;
			
			boxCommande.removeAllChildren();
			boxCommande.addChild(_etapeCommandeMobile);
			
			vs.selectedIndex = 1;
			vs.validateNow();
		}
		
		private function etapeCommandeFixeReseau():void
		{
			_etapeCommandeFixe = new MainNouvelleCommandeIHM();
			
			CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE = true;
			
			boxCommande.removeAllChildren();
			boxCommande.addChild(_etapeCommandeFixe);
			
			vs.selectedIndex = 1;
			vs.validateNow();
		}
		
		private function etapeCommandeFixeReseauSPIE():void
		{
			_etapeCommandeSPIE = new MainNouvelleCommandeServiceIHM();
			
			CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE = true;
			
			boxCommande.removeAllChildren();
			boxCommande.addChild(_etapeCommandeSPIE);
			
			vs.selectedIndex = 1;
			vs.validateNow();
		}
		
		private function etapeResiliation():void
		{	
			CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE = true;
			
			boxCommande.removeAllChildren();
			boxCommande.addChild(_etapeResiliation);
			
			vs.selectedIndex = 1;
			vs.validateNow();
		}
		
		private function etapeModification():void
		{
			_etapeModification = new MainModificationIHM();
			
			CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE = true;
			
			boxCommande.removeAllChildren();
			boxCommande.addChild(_etapeModification);
			
			vs.selectedIndex = 1;
			vs.validateNow();
		}
		
		private function etapeNouvelleSim():void
		{	
			CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE = true;
			
			boxCommande.removeAllChildren();
			boxCommande.addChild(_etapeNouvelleSim);
			
			vs.selectedIndex = 1;
			vs.validateNow();
		}
		
		private function etapeSAV():void
		{	
			CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE = true;
			
			boxCommande.removeAllChildren();
			boxCommande.addChild(_etapeSAV);
			
			vs.selectedIndex = 1;
			vs.validateNow();
		}
		
		private function etapePortabilite():void
		{	
			CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE = true;
			
			boxCommande.removeAllChildren();
			boxCommande.addChild(_etapePortabilite);
			
			vs.selectedIndex = 1;
			vs.validateNow();
		}
		
			//--------------------------------------------------------------------------------------------//
			//					POPUPs
			//--------------------------------------------------------------------------------------------//
			
		private function typeCommandePopUp():void
		{
			_popupSelectTypeCommande 			= new SelectTypeCommandeIHM();
			_popupSelectTypeCommande.idProfil 	= listeCommande.cbxPools.selectedItem.IDPROFIL;
			
			_popupSelectTypeCommande.addEventListener(TypeCommandeEvent.OPE_TYPE_CMD, typeCommandeHandler);
			
			PopUpManager.addPopUp(_popupSelectTypeCommande, listeCommande, true);
			PopUpManager.centerPopUp(_popupSelectTypeCommande);
		}
		
		private function typeCommandeSPIEPopUp():void
		{
			// Commande de type Reseau(ou DATA) (id segment Reseau = 3)
			SessionUserObject.singletonSession.IDSEGMENT = 3;
			etapeCommandeFixeReseauSPIE();
		}

		private function modificationLignePopUp():void
		{		
			_popUpOperationSurLigne = new OperationSurLigneIHM();
			_popUpOperationSurLigne.addEventListener(TypeOperationSurLigneIHMEvent.OPERATION_LIGNE_EVENT,  operationResilierLigneHandler);
			
			PopUpManager.addPopUp(_popUpOperationSurLigne, listeCommande, true);
			PopUpManager.centerPopUp(_popUpOperationSurLigne);
		}
		
		//AFFICHE LA FENETRE DE LA LISTE DES COMMANDES ET APPEL LA PROCEDURE PERMETTANT L'AFFICHAGE DE TOUTES LES COMMANDES
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			initializationListeners();
		}
		
			//--------------------------------------------------------------------------------------------//
			//					APPELS PROCEDURES
			//--------------------------------------------------------------------------------------------//

		private function getInfosLoad(idpool:int = -1, idprofil:int = 0):void
		{
			_infosCmdSrv.getLoadInfosThread(idpool, idprofil);
			_infosCmdSrv.addEventListener(CommandeEvent.INFOS_THREAD_CMD, infosLoadedHandler);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					LISTENERS PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		private function infosLoadedHandler(cmde:CommandeEvent):void
		{
			if(_infosCmdSrv.numberRevendeurs > 0)
				SessionUserObject.singletonSession.BOOLDISTRIBAUTORISES = true;
			else
				SessionUserObject.singletonSession.BOOLDISTRIBAUTORISES = false;
			
			SessionUserObject.singletonSession.USERPOOLS = _infosCmdSrv.listePools;

			if(SessionUserObject.singletonSession.USERPOOLS.length == 1)
				SessionUserObject.singletonSession.POOL = SessionUserObject.singletonSession.USERPOOLS[0];
			
			listeCommande.setInfosCommande(_infosCmdSrv.isUserProfil, _infosCmdSrv.listeEtats, _infosCmdSrv.listeGestionnaires, 
											_infosCmdSrv.listeOperateurs, _infosCmdSrv.listeComptes, _infosCmdSrv.listeSousComptes,
											_infosCmdSrv.listeActions, _infosCmdSrv.listeActionsProfil);
			
			historiqueCommande.setPoolsGestionnaire();
			
			if(boolFaireNouvelleCommande && !_isModifLigne)
			{
				this.etapeToDoFUnction();
			}
			else if(boolFaireNouvelleCommande && _isModifLigne)
			{
				if(_etapeResiliation != null)
					_etapeResiliation.setReferencesClientChild();
				else
				if(_etapeNouvelleSim != null)
					_etapeNouvelleSim.setReferencesClientChild();
				else
				if(_etapePortabilite != null)
					_etapePortabilite.setReferencesClientChild();
				else
				if(_etapeSAV != null)
					_etapeSAV.setReferencesClientChild();
				else if (etapeToDoFUnction != null) this.etapeToDoFUnction(); 
					
				
			}
		}

			//--------------------------------------------------------------------------------------------//
			//					LISTENERS CHANGEMENT UNIVERS M16-> M11
			//--------------------------------------------------------------------------------------------//
		
		private function changeUniversFunction():void 
		{
			//SessionUserObject.singletonSession				= null;
			CvAccessManager.getSession().INFOS_DIVERS.BOOLNEWCOMMANDE = false;
			//CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE   = true;
			//razChildrenBox();
			CvAccessManager.changeModule("GEST_PARC");
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//
		
		private function initializationListeners():void
		{
			addEventListener(TypeCommandeEvent.VIEW_OPETYPECMD, viewSelectTypeCommandeHandler);
			addEventListener(TypeCommandeEvent.VIEW_OPETYPECMD_SPIE, viewSelectTypeCommandeSPIEHandler);
			
			addEventListener(ViewStackEvent.VS_VIEW_HISTORIQUE, viewHistoriqueCommandeHandler);
			addEventListener(ViewStackEvent.VS_VIEW_LISTECMDES, viewListeCommandeHandler);
			addEventListener(ViewStackEvent.VS_VIEW_MODIFCMDES, viewModificationCommandeHandler);
			addEventListener(ViewStackEvent.VS_VIEW_LISTE, viewnewListeCommandeHandler);
			
			addEventListener(ViewStackEvent.COMMANDE_BUILT, viewListeCommandeHandler);
			addEventListener(ViewStackEvent.ANNUL_COMMANDE, viewListeCommandeOrM11Handler);
		}
		
		private function whatIsCodeApp():void
		{
			if(CvAccessManager.getSession().USER.CODEAPPLICATION == 3)
				SessionUserObject.singletonSession.BOOLSNCF = true;
		}
		
			//--------------------------------------------------------------------------------------------//
			//					MAPPING
			//--------------------------------------------------------------------------------------------//

		private function mappPool(valueObject:Object):Pool
		{
			var poolObject:Pool = new Pool();
				poolObject.IDPOOL			= valueObject.IDPOOL;
				poolObject.IDPROFIL			= valueObject.IDPROFIL;
				poolObject.IDREVENDEUR		= valueObject.IDREVENDEUR;
				poolObject.LIBELLE_CONCAT	= StringUtil.trim(valueObject.LIBELLE_POOL) + " - " + StringUtil.trim(valueObject.LIBELLE_PROFIL);
				poolObject.LIBELLE_POOL		= valueObject.LIBELLE_POOL;
				poolObject.LIBELLE_PROFIL	= valueObject.LIBELLE_PROFIL; 
			
			return poolObject;
		}
	
	}
}
