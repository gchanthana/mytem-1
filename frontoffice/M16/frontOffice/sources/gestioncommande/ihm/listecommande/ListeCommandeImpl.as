package gestioncommande.ihm.listecommande
{
import composants.pagindatagrid.pagination.vo.ItemIntervalleVO;

import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	
	import commandemobile.composants.Livraisonpartiel.LivraisonpartielIHM;
	import commandemobile.composants.importdemasse.ImportDeMasseUpload;
	import commandemobile.ihm.mail.ActionMailBoxIHM;
	import commandemobile.itemRenderer.PanelAffectationEvent;
	
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.pagindatagrid.PaginateDatagrid;
	import composants.pagindatagrid.pagination.event.PaginationEvent;
	import composants.searchpagindatagrid.SearchPaginateDatagrid;
	import composants.searchpagindatagrid.event.SearchPaginateDatagridEvent;
	import composants.util.ConsoviewAlert;
	import composants.util.preloader.Spinner;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.Pool;
	import gestioncommande.entity.Transporteur;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.ActionsEvent;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.TypeCommandeEvent;
	import gestioncommande.events.ViewStackEvent;
	import gestioncommande.popup.PeriodeIHM;
	import gestioncommande.popup.PopUpAnnulerCommandeIHM;
	import gestioncommande.popup.PopUpCatalogueIHM;
	import gestioncommande.popup.PopUpCriteresDeRechercheIHM;
	import gestioncommande.popup.PopUpCriteresDeRechercheImpl;
	import gestioncommande.popup.PopUpDetailsCommandeIHM;
	import gestioncommande.popup.PopUpLivraisonIHM;
	import gestioncommande.popup.PopupChoosePoolIHM;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.DataGridDataExporter;
	import gestioncommande.services.Export;
	import gestioncommande.services.Formator;
	import gestioncommande.services.ImportDeMasseService;
	import gestioncommande.services.ListeCommandesService;
	import gestioncommande.services.PoolService;
	import gestioncommande.services.RevendeurService;
	import gestioncommande.services.TransporteurSevice;
	import gestioncommande.services.WorkflowService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class ListeCommandeImpl extends Box
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMs
			//--------------------------------------------------------------------------------------------//
		
		public var cbxPools					:ComboBox;
		
		public var spin						:Spinner;
		
		public var spdClef					:SearchPaginateDatagrid;
		
		public var myPaginatedatagrid		:PaginateDatagrid;
		
		
		
			//--------------------------------------------------------------------------------------------//
			//					POPUPs
			//--------------------------------------------------------------------------------------------//
		
		private var _popUpMailBox			:ActionMailBoxIHM;
		private var _popUpTransporteurs		:PopUpLivraisonIHM;
		private var _popUpLivPart			:LivraisonpartielIHM;
		private var _popUpDetails			:PopUpDetailsCommandeIHM; 
		private var _popUpClotureAnnul		:PopUpAnnulerCommandeIHM;
		private var _popUpUpload			:ImportDeMasseUpload;
		private var _popUpChoosePool		:PopupChoosePoolIHM;
		private var _popupPeriodeExport			:PeriodeIHM;
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		public var info						:ListeCommandesService;
		
		private var _transSrv				:TransporteurSevice = new TransporteurSevice();
		
		private var _revSrv					:RevendeurService = new RevendeurService();
		
		private var _wflowSrv				:WorkflowService = new WorkflowService();
		
		private var _cmdSrv					:CommandeService = new CommandeService();
		
		private	var _poolsSrv				:PoolService = new PoolService();
		
		private var _listeCmdeSrv			:ListeCommandesService = new ListeCommandesService();
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
		
		private var _selectedAction			:Action = new Action();
		
		private var _cmd					:Commande = new Commande();
		
		private var _infosMail				:InfosObject = new InfosObject();
		
			//--------------------------------------------------------------------------------------------//
			//					AUTRES
			//--------------------------------------------------------------------------------------------//
		
		public var listeCommandes			:ArrayCollection = new ArrayCollection();
		
		public var isAutorised				:Boolean = false;
		public var isLivPartielle			:Boolean = false;
		public var isLivraison				:Boolean = false;
 		
		public var currenciesAreDifferent	:Boolean = false;
		
		public var nbCmdSelected			:Number  = 0;
		
		public var compteurCommandeSelected	:int = 0;
		
		private var _pools					:ArrayCollection = new ArrayCollection();
		private var _listeHistorique		:ArrayCollection = new ArrayCollection();
		
		private var _clobIdsCommande		:String  = '';
		
		private var _idUserProfile			:int = 0;
		
		private var _poolSelected			:Boolean = false;
		
			//--------------------------------------------------------------------------------------------//
			//					STATICS
			//--------------------------------------------------------------------------------------------//
		
		public static var NBITEMPARPAGE		:int = 21;
		
		private const SPIE_1					:Number = 5821693;
		private const SPIE_2					:Number = 6149989;
//		private const SPIE						:Number = 2458788; // idGroupe pour demo_consotel
		private var _IS_SPIE					:Boolean = false;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//

		public function ListeCommandeImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);			
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		public function setInfosCommande(idprofile:int, etats:ArrayCollection, gestionnaires:ArrayCollection, operateurs:ArrayCollection,
										 comptes:ArrayCollection, souscomptes:ArrayCollection, actions:ArrayCollection,
										 actionsProfil:ArrayCollection):void
		{
			_idUserProfile = SessionUserObject.singletonSession.USERPROFIL = idprofile;
			
			listeCommandes.removeAll();
			
			myPaginatedatagrid.isAutorised = false;
					
			setPoolsGestionnaire(etats, gestionnaires, operateurs, comptes, souscomptes, actions, actionsProfil);
		}

		public function refreshListeCommande():void
		{
			listeCommandes.removeAll();
			
			getListeCommandes();
		}
		
		private function sortListePool():void
		{
			var tous:Pool 		= new Pool();
			tous.IDPOOL 		= -1;
			tous.LIBELLE_CONCAT = ResourceManager.getInstance().getString('M16', 'Tous_les_pools');
			
			var _arrayPools:Array = _pools.toArray();
			_arrayPools.sortOn("LIBELLE_CONCAT", Array.CASEINSENSITIVE);
			_pools.source = _arrayPools;
			_pools.refresh();
			_pools.addItemAt(tous, 0);
		}
		
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		protected function cbxPoolsChangeHandler(evt:Event):void
		{
			if(cbxPools.selectedItem != null){
				razAndGetListeFiltres();
				SessionUserObject.singletonSession.POOL = Pool.mappPool(cbxPools.selectedItem);
			}
			else
			{
				cbxPools.selectedIndex = 0;
				razAndGetListeFiltres();
			}
		}
		
		private function majSelectPool(_pools:ArrayCollection):void
		{
			var longPool:int = _pools.length;
			var i:int = 0;
			while(i < longPool)
			{
				_pools[i].SELECTED = false;
				if(_pools[i].IDPOOL == cbxPools.selectedItem.IDPOOL)
				{
					_pools[i].SELECTED = true;
					_poolSelected = true;
				}
				i++;
			}
		}
		
		protected function spdClefSearchHandler(spde:SearchPaginateDatagridEvent):void
		{
			getListeCommandes();
		}

		protected function myPaginatedatagridClickHandler(me:MouseEvent):void
		{
			if(listeCommandes == null) return;
			
			var len:int = listeCommandes.length;
			
			if(me.target as CheckBox)
			{
				compteurCommandeSelected = 0;
				
				for(var i:int = 0;i < len;i++)
				{
					if(listeCommandes[i].SELECTED)
						compteurCommandeSelected++;
				}
				
				if(compteurCommandeSelected == len)
					myPaginatedatagrid.chxSelectAll.selected = true;
				else
					myPaginatedatagrid.chxSelectAll.selected = false;
				
				var nbTotal:int = 0;
				
				if(len > 0)
					nbTotal = listeCommandes[len-1].NBRECORD;
				
				myPaginatedatagrid.setNumberSeleted(compteurCommandeSelected, nbTotal, NBITEMPARPAGE);
			}
		}
		
		protected function getItemPoolHandler(e:Event):void
		{
			_popUpChoosePool.removeEventListener('VALID_CHOIX_POOL', getItemPoolHandler);
			cbxPools.selectedItem = _popUpChoosePool.itemCurrent;
			cbxPoolsChangeHandler(null);
			SessionUserObject.singletonSession.POOL = Pool.mappPool(cbxPools.selectedItem);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - PUBLIC
		//--------------------------------------------------------------------------------------------//
		
		public function btImgChoosePool_clickHandler(e:Event):void
		{
			var pools:ArrayCollection = cbxPools.dataProvider as ArrayCollection;
			this.majSelectPool(pools);
			_popUpChoosePool = new PopupChoosePoolIHM();
			var point:Point = new Point(stage.mouseX,stage.mouseY);
			_popUpChoosePool.x = point.x - cbxPools.width; 
			_popUpChoosePool.y = point.y;
			
			_popUpChoosePool.listePool = pools;
			if(_poolSelected == false)
				pools[0].SELECTED = true;
			_popUpChoosePool.addEventListener('VALID_CHOIX_POOL', getItemPoolHandler);
			PopUpManager.addPopUp(_popUpChoosePool, this, true);
		}

		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - PRIVATE
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					LISTENERS IHM
			//--------------------------------------------------------------------------------------------//
		

		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			initInfos();
			initListeners();			
		}
		
		private function initInfos():void
		{
			var groupIdx:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			if((groupIdx == SPIE_1) || (groupIdx == SPIE_2))
			{
				IS_SPIE = true;
			}
			
		}
		
		private function btnClickedHandler(e:Event):void
		{
			if(e.type == 'CLICKED_VIEW_CATALOGUE')
			{
				popUpCatalogue();
			}
			else if(e.type == 'CLICKED_VIEW_HISTORIQUE')
				{
					if(cbxPools.selectedItem != null)
						SessionUserObject.singletonSession.POOLSELECTED = cbxPools.selectedItem as Pool;
					else
						SessionUserObject.singletonSession.POOLSELECTED = null;
					
					dispatchEvent(new ViewStackEvent(ViewStackEvent.VS_VIEW_HISTORIQUE, true));
				}
				else if(e.type == 'CLICKED_VIEW_COMMANDE')
					{
						// Traitement d'une commande SPIE
						if((cbxPools.selectedItem != null && cbxPools.selectedItem.IDPOOL != -1) && (IS_SPIE))
						{
							SessionUserObject.singletonSession.POOL 		= mappPool(cbxPools.selectedItem);
							SessionUserObject.singletonSession.POOLSELECTED = mappPool(cbxPools.selectedItem);
							
							dispatchEvent(new TypeCommandeEvent(TypeCommandeEvent.VIEW_OPETYPECMD_SPIE, '', '', true));
						}
						// Traitement d'une commande normale (non spie)
						else if(cbxPools.selectedItem != null && cbxPools.selectedItem.IDPOOL != -1)
						{
							SessionUserObject.singletonSession.POOL 		= mappPool(cbxPools.selectedItem);
							SessionUserObject.singletonSession.POOLSELECTED = mappPool(cbxPools.selectedItem);

							dispatchEvent(new TypeCommandeEvent(TypeCommandeEvent.VIEW_OPETYPECMD, '', '', true));
						}
						else
							ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_pool__'), 'Consoview', null);
					}
		}
		
		private function buttonExportClickedHandler(e:Event):void
		{
			if(cbxPools.selectedIndex != -1)
			{
				if(listeCommandes.length > 0)
				{
					switch(myPaginatedatagrid.cbxExport.selectedItem.data)
					{
						case 0: exportDetailleAll(); 			break;
						case 1: exportDetailleOfSelection(); 	break;
						case 2: exportNonDetailleOfSelection(); break;
						case 3: exportReferences();  			break; 
						case 4: importReferences();  			break;  
					}
				}
				else
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Vous_devez_s_lectionner_au_moins_un__l_m'), 'Consoview');
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_pool__'), 'Consoview');
		}
		
		private var _importSrv					:ImportDeMasseService = new ImportDeMasseService();

		
		private var _uuid		:String = '';
		private var _fileName	:String = '';
		
		private var _infosImport				:ArrayCollection = new ArrayCollection();

		
		private function importReferences():void
		{
			_uuid = '';
			
			_popUpUpload 					= new ImportDeMasseUpload();
			_popUpUpload.urlMatriceUpload 	= moduleCommandeMobileIHM.NonSecureUrlBackoffice  + '/fr/consotel/consoview/M16/importdemasse/processUpload.cfm';
			
			_popUpUpload.addEventListener('POPUP_CLOSED', popUpClosedHandler);
			_popUpUpload.addEventListener('POPUP_CLOSED_UPLOADED', popUpClosedHandler);
			
			PopUpManager.addPopUp(_popUpUpload, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpUpload);
		}
		
		private function popUpClosedHandler(e:Event):void
		{
			if(e.type == 'POPUP_CLOSED_UPLOADED')
			{
				_uuid 		= _popUpUpload.uuid;
				_fileName 	= _popUpUpload.fileName;
				
//				importDataToUrl(_uuid, _fileName);
				importDataToProcedure(_uuid, _fileName);
			}
		}
		
		private function importDataToUrl(refUuid:String, refFilename:String):void
		{
			var url			:String = moduleCommandeMobileIHM.NonSecureUrlBackoffice + '/fr/consotel/consoview/M16/importdemasse/imports/ImportReferences.cfm';
			var request		:URLRequest = new URLRequest(url);         
			var variables	:URLVariables 	= new URLVariables();
				variables.UUID			 	= refUuid;
				variables.FILE_NAME 		= refFilename;
			
			request.data 	= variables;
			request.method 	= URLRequestMethod.POST;
			
			navigateToURL(request, "_blank");
		}
		
		private var _subject:String = ResourceManager.getInstance().getString('M16', 'R_sultat___Import_de_masse_des_r_f_rence'); 
		private var _succesContent:String = ResourceManager.getInstance().getString('M16', 'Bonjour_') +
											ResourceManager.getInstance().getString('M16', '_br__br_L_import_de_masse_de_r_f_rences_') +
											ResourceManager.getInstance().getString('M16', '_br__br_Cordialement__br__br_L_equipe_de');
		private var _errorContent:String = ResourceManager.getInstance().getString('M16', 'Bonjour_') +
											ResourceManager.getInstance().getString('M16', '_br__br_Lors_de_l_import_de_masse_de_r_f') +
											ResourceManager.getInstance().getString('M16', '_br__br_Veuillez_trouver_ci_joint_le_fic') +
											ResourceManager.getInstance().getString('M16', '_br__br_Cordialement__br__br_L_equipe_de');
		
		private var _causes:String = ResourceManager.getInstance().getString('M16', 'Erreur_mise___jour_') +
									ResourceManager.getInstance().getString('M16', 'N__existant_') +
									ResourceManager.getInstance().getString('M16', 'Format_non_conforme_') +
									ResourceManager.getInstance().getString('M16', 'Non_trait_');
		
		private function importDataToProcedure(refUuid:String, refFilename:String):void
		{
			if(_uuid != '' && _fileName != '')
				getInfosFileImported();
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue_lors_du_traiteme'), 'Consoview');
		}
		
		private function getInfosFileImported():void
		{
			_importSrv.getInfosFileImportedMultiReferences(_uuid, _fileName, _causes, _subject, _succesContent, _errorContent, ResourceManager.getInstance().getString('M16', 'resultat'));
			_importSrv.addEventListener(CommandeEvent.IMPORT_FINISHED, infosFileImportedHandler);
		}
		
		private function infosFileImportedHandler(cmde:CommandeEvent):void
		{
			ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Votre_fichier_est_en_cours_de_d_importat'), 'Consoview',null);
		}

		
		private var _libelleHeadersMobile			:Array = [ 	ResourceManager.getInstance().getString('M16','Commande_N_'),
																ResourceManager.getInstance().getString('M16','Libell_'),
																ResourceManager.getInstance().getString('M16','Collaborateur'),
																ResourceManager.getInstance().getString('M16','Equipement'),
																ResourceManager.getInstance().getString('M16','IMEI'),
																ResourceManager.getInstance().getString('M16','Ligne2'),
																ResourceManager.getInstance().getString('M16','Num_ro_de_SIM'),
																ResourceManager.getInstance().getString('M16','Code_PUK'),
																'idcommande',
																'idsoustete'
															 ];
		
		private var _libelleHeadersFixeData			:Array = 	[	ResourceManager.getInstance().getString('M16','Commande_N_'),
																	ResourceManager.getInstance().getString('M16','Equipement'),
																	ResourceManager.getInstance().getString('M16','Num_ro_de_s_rie'),
																	ResourceManager.getInstance().getString('M16','Ligne2'),
																	' ',
																	' ',
																	' ',
																	' ',
																	'idcommande',
																	'idsoustete'
																];
		
		private function exportReferences():void
		{
			if(listeCommandes.length > 0 && cbxPools.selectedItem != null) 
			{
				var url			:String = moduleCommandeMobileIHM.NonSecureUrlBackoffice + '/fr/consotel/consoview/M16/importdemasse/exports/ExportMultiReferences.cfm';
				var request		:URLRequest = new URLRequest(url);         
				
				var rsltObject	:Object = isSameSegmentAndGetIds();
				
				if(rsltObject.isSame)
				{
					var idscommande	:Array = rsltObject.idscommande;
					
					if(idscommande.length > 0) 
					{
						var idsegment	:int = rsltObject.segment;
						var headers		:Array;
						
						if(idsegment > 1)
							headers = _libelleHeadersFixeData;
						else
							headers = _libelleHeadersMobile

						var variables	:URLVariables 	= new URLVariables();
							variables.LIBELLE_HEADER 	= headers;
							variables.DATA_TO_EXPORT	= idscommande;
							variables.IDPOOL		 	= cbxPools.selectedItem.IDPOOL;
							variables.SEGMENT		 	= idsegment;
							variables.FILE_NAME 		= 'export_multi_references_' + Formator.getSuffixeFileName();

						request.data 	= variables;
						request.method 	= URLRequestMethod.POST;
						
						navigateToURL(request, "_blank");
					}
				}
				else
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Attention__nVous_essayez_d_export__des_c'), 'Consoview', null);
				}
			}
		}
		
		private function isSameSegmentAndGetIds():Object
		{
			var isSameAndEtat	:Object = new Object();
			var isSame			:Boolean = true;
			var idscommande		:Array = new Array();
			var lenCommande		:int = listeCommandes.length;
			var idSegment		:int = 0;
			
			for(var i:int = 0; i < lenCommande;i++)
			{
				if(listeCommandes[i].SELECTED && !isContainsRestrictedActions(listeCommandes[i]))
				{
					if(idSegment == 0)
					{
						idSegment = getSegmentFromCommande(listeCommandes[i]);
						idscommande[idscommande.length] = listeCommandes[i].IDCOMMANDE;
					}
					else
					{
						if(idSegment != getSegmentFromCommande(listeCommandes[i]))
							isSame = false;
						else
							idscommande[idscommande.length] = listeCommandes[i].IDCOMMANDE;
					}
				}
				else
					listeCommandes[i].SELECTED = false;
			}

			isSameAndEtat.isSame 		= isSame;
			isSameAndEtat.idscommande 	= idscommande;
			isSameAndEtat.segment	 	= idSegment;
			
			return isSameAndEtat;
		}
				
		private function isContainsRestrictedActions(valueCommande:Object):Boolean
		{
			var isContain	:Boolean = false;
			
			switch(valueCommande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	isContain = false;	break;//--->  NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		isContain = false;	break;//--->  NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		isContain = false;	break;//--->  NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		isContain = false;	break;//--->  NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: isContain = true;	break;//--->  MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	isContain = true;	break;//--->  MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			isContain = true;	break;//--->  RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	isContain = true;	break;//--->  RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		isContain = false;	break;//--->  RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	isContain = true;	break;//--->  RENVLT
				case TypesCommandesMobile.SUSPENSION: 			isContain = true;	break;//--->  SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		isContain = true;	break;//--->  SUSP
				case TypesCommandesMobile.REACTIVATION: 		isContain = true;	break;//--->  REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	isContain = true;	break;//--->  REACT
			}
			
			if(!isContain)
			{
				if(valueCommande.IDLAST_ETAT >= 3072 && valueCommande.IDLAST_ETAT != 3073 && valueCommande.IDLAST_ETAT != 3266 && valueCommande.IDLAST_ETAT != 3366 && valueCommande.IDLAST_ETAT < 3171)
					isContain = false;
				else
					isContain = true;
			}
			
			return isContain;
		}
		
		private function getSegmentFromCommande(valueCommande:Object):int
		{
			var idseg:int = 1;
			
			if(valueCommande.SEGMENT_MOBILE == 1 && valueCommande.SEGMENT_FIXE == 0 && valueCommande.SEGMENT_DATA == 0)
			{
				idseg = 1;//---> MOBILE
			}
			else if(valueCommande.SEGMENT_MOBILE == 0 && valueCommande.SEGMENT_FIXE == 1 && valueCommande.SEGMENT_DATA == 0)
				{
					idseg = 2;//---> FIXE
				}
				else if(valueCommande.SEGMENT_MOBILE == 0 && valueCommande.SEGMENT_FIXE == 0 && valueCommande.SEGMENT_DATA == 1)
					{
						idseg = 3;//---> DATA
					}
			
			return idseg;
		}
		
		private function resultAlertExport(ce:CloseEvent):void
		{
			if(ce.detail == Alert.OK)
				exportData();
		}
		
		private function resultExportAllHandler(ae:ActionsEvent):void
		{
			exportAllData(ae.object as String);
		}

		private function resultAlertWorkflow(ce:CloseEvent):void
		{
			if(ce.detail == Alert.OK)
				sendAction();
		}
		
		private function panelItemClickHandler(pae:PanelAffectationEvent):void
		{
			if(pae.codeItem == 'VIEW_CMD' || pae.codeItem == 'VIEW_CMD_PDF' || pae.codeItem == 'VIEW_CMD_TRACKING' || pae.codeItem == 'VIEW_SAISETECHNIQUE')
			{
				switch(pae.codeItem)
				{
					case "VIEW_CMD"				: viewCommande();	break;
					case "VIEW_CMD_PDF"			: viewPDF();		break;
					case "VIEW_CMD_TRACKING"	: viewTracking();	break;
					case "VIEW_SAISETECHNIQUE"	: viewLivraison();	break;
				}
			}
			else
			{
				_selectedAction = pae.actionItem;
				
				if(_selectedAction.IDACTION == 2166 || _selectedAction.IDACTION == 2229)//---> MODIFICATION COMMANDE
				{
					viewModifCmde();
				}
				else if(_selectedAction.IDACTION == 2316 || _selectedAction.IDACTION == 2317)//---> LIVRAISON PARTIELLE
					{
						isLivPartielle = true;
						viewLivraison();
					}
					else if(_selectedAction.IDACTION == 2077 || _selectedAction.IDACTION == 2226)//---> CLOTURE DE COMMANDE
						{
							popUpCloseAnnul(_selectedAction);
						}
						else//---> TOUTES LES AUTRES ACTIONS
						{
							getWorflowHistorique();
						}
			}
		}
		
		private function retrieveProcedure(pe:PaginationEvent):void
		{
			getListeCommandes();
		}
		
		private var _popUpCriteresDeRecherche:PopUpCriteresDeRechercheIHM;
				
		private function moreCriteresHandler(e:Event):void
		{
			_popUpCriteresDeRecherche = new PopUpCriteresDeRechercheIHM();
			var point:Point = new Point(stage.mouseX,stage.mouseY);
			_popUpCriteresDeRecherche.x = point.x; 
			_popUpCriteresDeRecherche.y = point.y;
			_popUpCriteresDeRecherche.mois_historique = spdClef.ns_historique.value;
			_popUpCriteresDeRecherche.setInfosData(SessionUserObject.singletonSession.CURRENTCRITERE.copyCritere());
			_popUpCriteresDeRecherche.addEventListener(PopUpCriteresDeRechercheImpl.VALID_CRITERE, critereHandler);
			_popUpCriteresDeRecherche.addEventListener(PopUpCriteresDeRechercheImpl.ANNUL_CRITERE, critereHandler);

			PopUpManager.addPopUp(_popUpCriteresDeRecherche, this, true);
		}
		
		private function critereRemovedHandler(e:Event):void
		{
			getListeCommandes();
		}
		
		private function critereHandler(e:Event):void
		{
			if(e.type != PopUpCriteresDeRechercheImpl.ANNUL_CRITERE)
			{
				SessionUserObject.singletonSession.CURRENTCRITERE = _popUpCriteresDeRecherche.getInfosData().copyCritere();
				
				getListeCommandes();
			}
			
			PopUpManager.removePopUp(_popUpCriteresDeRecherche);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					APPELS PROCEDURES
			//--------------------------------------------------------------------------------------------//

		private function getListeCommandes():void
		{
			listeCommandes.removeAll();
			
			myPaginatedatagrid.chxSelectAll.selected = false;
			
			compteurCommandeSelected = 0;
			
			var idpool			:int = -1;
			var idsegment		:int = -1;
			var idstatut		:int = -1;
			var idetat			:int = -1;
			var idoperateur		:int = -1;
			var idcompte		:int = -1;
			var idsouscompte	:int = -1;
			var idusercreate	:int = -1;
			var startindex		:int = 1;
			var mois_historique	:int = 3;
			
			if(cbxPools.selectedItem != null)
				idpool = cbxPools.selectedItem.IDPOOL;
			
			if(spdClef.chxMobile.selected && spdClef.chxFixeReseau.selected)
			{
				idsegment = 0;
			}
			else if(spdClef.chxMobile.selected && !spdClef.chxFixeReseau.selected)
				{
					idsegment = 1;
				}
				else if(!spdClef.chxMobile.selected && spdClef.chxFixeReseau.selected)
					{
						idsegment = 2;
					}
			
			if(spdClef.cbxStatut.selectedItem != null)
				idstatut = spdClef.cbxStatut.selectedItem.data;
			
			if(spdClef.cbxEtat.selectedItem != null)
				idetat = spdClef.cbxEtat.selectedItem.IDINV_ETAT;
			
			if(spdClef.cbxOperateur.selectedItem != null)
				idoperateur = spdClef.cbxOperateur.selectedItem.OPERATEURID;
			
			if(spdClef.cbxCompte.selectedItem != null)
				idcompte = spdClef.cbxCompte.selectedItem.IDCOMPTE_FACTURATION;
			
			if(spdClef.cbxSousCompte.selectedItem != null)
				idsouscompte = spdClef.cbxSousCompte.selectedItem.IDSOUS_COMPTE;
			
			if(spdClef.cbxGestionnaire.selectedItem != null)
				idusercreate = spdClef.cbxGestionnaire.selectedItem.APP_LOGINID;
	
			if(myPaginatedatagrid.currentIntervalle != null && myPaginatedatagrid.currentIntervalle.indexDepart > 0)
				startindex 	= myPaginatedatagrid.currentIntervalle.indexDepart;
			
			spdClef.setSpinnerEtat(true);
			
			if(spdClef.ns_historique.value > -1)
				mois_historique = spdClef.ns_historique.value;
			
			_listeCmdeSrv.getCommandesPaginate(idpool, idsegment, idstatut, idetat, idoperateur, idcompte, idsouscompte, idusercreate, startindex, NBITEMPARPAGE, spdClef.txtClefSearch.text, SessionUserObject.singletonSession.CURRENTCRITERE.IDCRITERE, SessionUserObject.singletonSession.CURRENTCRITERE.TXTFILTER, mois_historique);
			_listeCmdeSrv.addEventListener(CommandeEvent.COMMANDES_LISTE, listeCommandeHandler);
		}

		private function getDetailsCommande():void
		{
			_cmdSrv.fournirDetailOperation((myPaginatedatagrid.dgPaginate.selectedItem as Commande).copyCommande(), (myPaginatedatagrid.dgPaginate.selectedItem as Commande).copyCommande().IDCOMMANDE);
			_cmdSrv.addEventListener(CommandeEvent.COMMANDE_DETAILS, detailsCommandeHandler);
		}
		
		private function getTransporteurs():void
		{
			_transSrv.fournirListeTransporteurs();
			_transSrv.addEventListener(CommandeEvent.LISTED_TRANSPORTS, transporteursHandler);
		}
		
		private function getWorflowHistorique():void
		{
			_wflowSrv.fournirHistoriqueEtatsCommande(myPaginatedatagrid.dgPaginate.selectedItem as Commande);
			_wflowSrv.addEventListener(CommandeEvent.LISTED_WFLOW_HIST, worflowHistoriqueHandler);
		}
		
		private function sendAction():void
		{
			_wflowSrv.faireActionWorkFlow(_selectedAction, myPaginatedatagrid.dgPaginate.selectedItem as Commande);
			_wflowSrv.addEventListener(CommandeEvent.SEND_ACTION, sendActionHandler);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					LISTENERS PROCEDURES
			//--------------------------------------------------------------------------------------------//

		private function listeCommandeHandler(cmde:CommandeEvent):void
		{			
			spdClef.setSpinnerEtat(false);
			
			listeCommandes = _listeCmdeSrv.listeCommandes;
			
			var lenCommandes:int = listeCommandes.length;

			if(lenCommandes > 0)
			{
				myPaginatedatagrid.paginationBox.initPagination(listeCommandes[lenCommandes-1].NBRECORD, NBITEMPARPAGE);
				myPaginatedatagrid.setNumberSeleted(compteurCommandeSelected, listeCommandes[lenCommandes-1].NBRECORD, NBITEMPARPAGE);
				myPaginatedatagrid.majTempListeSelected(listeCommandes);
				
				var i:int = 0;
				currenciesAreDifferent = false;
				while (i < lenCommandes)
				{
					if (listeCommandes[i].DEVISE != resourceManager.getString('M16', '_signe_de_la_devise__'))//A VERIFIER
					{
						currenciesAreDifferent = true;
						break;
					}
					i++;
				}
			}
			else
				myPaginatedatagrid.paginationBox.initPagination(0, 0);
			
		}
		
		private function detailsCommandeHandler(cmde:CommandeEvent):void
		{
			_cmd = _cmdSrv.currentCommande.copyCommande();
			
			popUpmanageLivraison();
		}
		
		private function transporteursHandler(cmde:CommandeEvent):void
		{
			var transporteur :Transporteur = new Transporteur();
			var lenTrans	 :int = _transSrv.listeTransporteurs.length;
			
			for(var i:int = 0;i < lenTrans;i++)
			{
				if(_transSrv.listeTransporteurs[i].IDTRANSPORTEUR == myPaginatedatagrid.dgPaginate.selectedItem.IDTRANSPORTEUR)
				{
					transporteur = _transSrv.listeTransporteurs[i];
					break;
				}
			}
			
			if(transporteur.URL_TRACKING != '')
				navigateToURL(new URLRequest(transporteur.URL_TRACKING + myPaginatedatagrid.dgPaginate.selectedItem.NUMERO_TRACKING),"_blank");
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucun_transporteur_'), 'Consoview', null);
		}
		
		private function worflowHistoriqueHandler(cmde:CommandeEvent):void
		{
			if(_selectedAction != null)
			{
				_listeHistorique = new ArrayCollection();
				_listeHistorique = _wflowSrv.historiqueWorkFlow;
				
				if(_selectedAction.CODE_ACTION == 'ANNUL')
				{
					popUpMail();
				}
				else if(_selectedAction.CODE_ACTION != null)
				{
					if(_selectedAction.CODE_ACTION == 'EXPED')
					{
						popUpTransporteur();
					}
					else if(_selectedAction.CODE_ACTION == 'LIVRE')
					{
						popUpMail(true);
					}
					else
					{
						popUpMail();
					}
				}
				else
					sendAction();
			}
		}
		
		private function sendActionHandler(cmde:CommandeEvent):void
		{
			getListeCommandes();
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//

		private function initListeners():void
		{
			addEventListener('BUTTON_EXPORT_CLICKED', buttonExportClickedHandler);
			
			addEventListener('CLICKED_VIEW_CATALOGUE', btnClickedHandler);
			addEventListener('CLICKED_VIEW_HISTORIQUE', btnClickedHandler);
			addEventListener('CLICKED_VIEW_COMMANDE', btnClickedHandler);
			
			addEventListener('SET_ACTIONS_WORKFLOW', actionsWokflowHandler);
			
			addEventListener(PanelAffectationEvent.DISPLAY_PANEL, panelItemClickHandler);
			
			spdClef.addEventListener('MORE_CRITERES', moreCriteresHandler);
			spdClef.addEventListener('CRITERE_REMOVED', critereRemovedHandler);

			myPaginatedatagrid.addEventListener(PaginationEvent.INTERVALLE_SELECTED_CHANGE, retrieveProcedure);
		}
		
		private function setPoolsGestionnaire(etats:ArrayCollection, gestionnaires:ArrayCollection, operateurs:ArrayCollection,
											  comptes:ArrayCollection, souscomptes:ArrayCollection, actions:ArrayCollection,
											  actionsProfil:ArrayCollection):void
		{
			_pools = new ArrayCollection()
			_pools = ObjectUtil.copy(SessionUserObject.singletonSession.USERPOOLS) as ArrayCollection;
			
			var lenPools:int = _pools.length;
			
			if(lenPools > 1)
			{
				sortListePool();	//sortListePool permet de trier la liste des pools, puis d'ajouter en première position l'élément "Tous les pools"
			}
			
			attributDataToCombobox(cbxPools, _pools, ResourceManager.getInstance().getString('M16', 'Aucun_pool'), true);
			
			lenPools = _pools.length;
			
			if(lenPools == 1)
			{				
				spdClef.setFilters(etats, gestionnaires, operateurs, comptes, souscomptes, actions, actionsProfil);
			}
			else
			{
				if(SessionUserObject.singletonSession.POOLSELECTED != null)
				{
					var idPool:int = SessionUserObject.singletonSession.POOLSELECTED.IDPOOL;
					
					for(var i:int = 0;i < lenPools;i++)
					{
						if(_pools[i].IDPOOL == idPool)
						{
							cbxPools.selectedIndex = i;
							
							break;
						}
					}
				}
				else
				{
					for(var j:int = 0;j < lenPools;j++)
					{
						if(_idUserProfile == 2 && _pools[j].IDREVENDEUR > 0)
						{
							cbxPools.selectedIndex = j;
							 
							break;
						}
					}
				}
				
				if(lenPools > 0)
					spdClef.setFilters(etats, gestionnaires, operateurs, comptes, souscomptes, actions, actionsProfil);
			}
		}
		
		private function actionsWokflowHandler(e:Event):void
		{
			SessionUserObject.singletonSession.LISTEACTIONSUSER 	= spdClef.listeActions;
			SessionUserObject.singletonSession.LISTEACTIONSPROFIL 	= spdClef.listeActionsProfil;
			
			isAutorisedToCommande(SessionUserObject.singletonSession.LISTEACTIONSPROFIL);
			
			getListeCommandes();
		}
		
		private function isAutorisedToCommande(values:ArrayCollection):void
		{
			var fixIsAutorised	:Boolean = false;
			var mobIsAutorised	:Boolean = false;
			var len				:int = values.length;
			
			for (var i:int = 0;i < len;i++)
			{
				if(values[i].hasOwnProperty("FIXE"))
					fixIsAutorised = values[i].ACTIF;
				
				if(values[i].hasOwnProperty("MOBILE"))
					mobIsAutorised = values[i].ACTIF;
			}
			
			if(fixIsAutorised == false && mobIsAutorised == false)
				isAutorised = myPaginatedatagrid.isAutorised = false;
			else
				isAutorised = myPaginatedatagrid.isAutorised = true;
		}
		
		private function razAndGetListeFiltres():void
		{
			listeCommandes.removeAll();
			
			myPaginatedatagrid.isAutorised = false;
            var intervalle:ItemIntervalleVO = new ItemIntervalleVO();
            intervalle.indexDepart = 1;
            myPaginatedatagrid.currentIntervalle  = intervalle;
            myPaginatedatagrid.refreshPaginateDatagrid(true);
            myPaginatedatagrid.refreshTempListeSelected();

			spdClef.razAndGetFiltres(cbxPools.selectedItem.IDPOOL, cbxPools.selectedItem.IDPROFIL);
		}
		
		//---ATTRIBUT LES DONNEES A LA COMBOBOX PASSEE EN PARAMETRE
		private function attributDataToCombobox(cbx:ComboBox, dataValues:ArrayCollection, msgError:String, isFirstItem:Boolean = true):void
		{
			var lenDataValues:int = dataValues.length;
			
			cbx.errorString = "";
			
			if(lenDataValues != 0)
			{
				if(isFirstItem)
				{
					cbx.dataProvider  			= dataValues;
					if(cbx.dropdown) cbx.dropdown.dataProvider 	= dataValues;
					cbx.selectedIndex 			= 0;
				}
				else
				{
					cbx.prompt 					= ResourceManager.getInstance().getString('M16', 'S_lectionnez');
					cbx.dataProvider 			= dataValues;
					if(cbx.dropdown)  cbx.dropdown.dataProvider 	= dataValues;
					
					if(lenDataValues > 1)
						cbx.selectedIndex = -1;
					else
						cbx.selectedIndex = 0;
				}
				
				if(lenDataValues > 10)
					cbx.rowCount = 10;
				else
					cbx.rowCount = lenDataValues;
				
				(cbx.dataProvider as ArrayCollection).refresh();
			}
			else
				cbx.prompt = msgError;
		}
		
		//--- 1-> NVL COMMANDE, 2-> MODIF OPTIONS, 3-> RENVLT, 4-> RESI,SUSP,REACT, -1-> DEFAUT
		private function giveMeSubject(objectCommande:Commande):String
		{
			var TC:String = "";
			
			switch(objectCommande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	TC = ResourceManager.getInstance().getString('M16', 'Commande_Mobile');				break;//--->  NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_Fixe_R_seau');		break;//--->  NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_');		break;//--->  NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_'); 		break;//--->  NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--->  MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--->  MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--->  RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--->  RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--->  RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--->  RENVLT
				case TypesCommandesMobile.SUSPENSION: 			TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--->  SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--->  SUSP
				case TypesCommandesMobile.REACTIVATION: 		TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--->  REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--->  REACT
				case TypesCommandesMobile.SAV: 					TC = ResourceManager.getInstance().getString('M16', 'SAV_'); 						break;//--- SAV
				case TypesCommandesMobile.NOUVELLE_CARTE_SIM: 	TC = ResourceManager.getInstance().getString('M16', 'Nouvelle_carte_SIM_');			break;//--- REACT
			}
			
			return TC;
		}
		
		private function giveMeTheSegementTheme(objectCommande:Commande):String
		{
			var moduleText:String = "";
			var idseg:int = 1;
			if(objectCommande.SEGMENT_MOBILE == 1 && objectCommande.SEGMENT_FIXE == 0 && objectCommande.SEGMENT_DATA == 0)
				idseg = 1;//---> MOBILE
			
			if(objectCommande.SEGMENT_MOBILE == 0 && objectCommande.SEGMENT_FIXE == 1 && objectCommande.SEGMENT_DATA == 0)
				idseg = 2;//---> FIXE
			
			if(objectCommande.SEGMENT_MOBILE == 0 && objectCommande.SEGMENT_FIXE == 0 && objectCommande.SEGMENT_DATA == 1)
				idseg = 3;//---> DATA
			
			
			switch(idseg)//--->  1-> MOBILE, 2-> FIXE, 3-> DATA
			{
				case 1: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Mobile'); break;
				case 2: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Fixe'); 	break;
				case 3: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Data'); 	break;
			}
			
			return moduleText;
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS - PANELAFFECTATION
			//--------------------------------------------------------------------------------------------//
		
		private function viewCommande():void
		{
			if(myPaginatedatagrid.dgPaginate.selectedItem == null) return;
			
			popUpDetails();
		}
		
		private function viewPDF():void
		{
			if(myPaginatedatagrid.dgPaginate.selectedItem == null) return;
			
			var export:Export = new Export();
				export.exporterCommandeEnPDF(myPaginatedatagrid.dgPaginate.selectedItem as Commande);
		}
		
		private function viewTracking():void
		{
			if(myPaginatedatagrid.dgPaginate.selectedItem == null) return;
			
			var boolNumTracking	:Boolean = false;
			var boolTransporteur:Boolean = false;
			var message			:String  = "";
			
			if(myPaginatedatagrid.dgPaginate.selectedItem.NUMERO_TRACKING != "")
				boolNumTracking = true	    		
			else
				message += ResourceManager.getInstance().getString('M16', 'Pas_de_num_ro_de_suivi_sp_cifi__n')
			
			if(myPaginatedatagrid.dgPaginate.selectedItem.IDTRANSPORTEUR)
				boolTransporteur = true	    		
			else
				message += ResourceManager.getInstance().getString('M16', 'Pas_de_transporteur_sp_cifi_')
			
			if(boolTransporteur && boolNumTracking)
				getTransporteurs();
			else
				ConsoviewAlert.afficherAlertInfo(message, 'Consoview', null);
		}
		
		private function viewLivraison():void
		{
			if(myPaginatedatagrid.dgPaginate.selectedItem != null)
				getDetailsCommande();
		}
		
		private function viewModifCmde():void
		{
			if(myPaginatedatagrid.dgPaginate.selectedItem != null)
			{
				SessionUserObject.singletonSession.COMMANDEARRAYCOL = new ArrayCollection();
				SessionUserObject.singletonSession.COMMANDEARTICLE	= new ArrayCollection();
				SessionUserObject.singletonSession.ARTICLES			= new ArrayCollection();
				
				SessionUserObject.singletonSession.COMMANDE	= new Commande();
				SessionUserObject.singletonSession.COMMANDE = (myPaginatedatagrid.dgPaginate.selectedItem as Commande).copyCommande();
				
				dispatchEvent(new ViewStackEvent(ViewStackEvent.VS_VIEW_MODIFCMDES, true));
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_une_commande__'), 'Consoview', null);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS - GESTION POPUPS
			//--------------------------------------------------------------------------------------------//
		
		private function popUpDetails():void
		{
			_popUpDetails 			 = new PopUpDetailsCommandeIHM();
			_popUpDetails.myCommande = (myPaginatedatagrid.dgPaginate.selectedItem as Commande).copyCommande();
			_popUpDetails.addEventListener('DETAILS_CLOSED', popUpDetailsHandler);
			
			PopUpManager.addPopUp(_popUpDetails, this, true);
			PopUpManager.centerPopUp(_popUpDetails);
		}
		
		private function popUpMail(isLiv:Boolean = false):void
		{
			_popUpMailBox = new ActionMailBoxIHM();
			
			_infosMail.commande 		 	= (myPaginatedatagrid.dgPaginate.selectedItem as Commande);
			_infosMail.historiqueCommande 	= _listeHistorique;
			_infosMail.corpMessage 		  	= _selectedAction.MESSAGE;
			
			_popUpMailBox.idsCommande 		= [(myPaginatedatagrid.dgPaginate.selectedItem as Commande).IDCOMMANDE];
			_popUpMailBox.numCommande 		= (myPaginatedatagrid.dgPaginate.selectedItem as Commande).NUMERO_COMMANDE;
			_popUpMailBox.idtypecmd			= (myPaginatedatagrid.dgPaginate.selectedItem as Commande).IDTYPE_COMMANDE;
			_popUpMailBox.isCommandePartielle = isLivPartielle;
			
			var moduleText	:String = giveMeTheSegementTheme(myPaginatedatagrid.dgPaginate.selectedItem as Commande);
			var sujet		:String = giveMeSubject(myPaginatedatagrid.dgPaginate.selectedItem as Commande);
			var libelle		:String	= (myPaginatedatagrid.dgPaginate.selectedItem as Commande).LIBELLE_COMMANDE;
			var numero		:String	= (myPaginatedatagrid.dgPaginate.selectedItem as Commande).NUMERO_COMMANDE;
			var objet		:String = sujet + " / " + libelle + " / " + numero;
			
			if(_selectedAction.IDACTION == 2078)
			{
				objet = ResourceManager.getInstance().getString('M16', 'Annulation_')  + ' / ' + libelle + ' / ' + numero;
			}
			
			_popUpMailBox.initMail(moduleText, objet, "");
			_popUpMailBox.configRteMessage(_infosMail, GabaritFactory.TYPE_COMMANDE_MOBILE);			
			_popUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE, popUpmailHandler);
			
			_popUpMailBox.selectedAction = _selectedAction;
			
			isLivraison = isLiv;
			
			PopUpManager.addPopUp(_popUpMailBox, this, true);
			PopUpManager.centerPopUp(_popUpMailBox);
		}
		
		private function popUpmanageLivraison():void
		{
			_popUpLivPart 			= new LivraisonpartielIHM();
			_popUpLivPart.commande 	= _cmd.copyCommande();
			
			if(_cmd.IDTYPE_COMMANDE == 1382 || _cmd.IDTYPE_COMMANDE == 1587)
				_popUpLivPart.isCommandeLivre = true;
			
			if(isLivPartielle && SessionUserObject.singletonSession.USERPROFIL > 0)
				_popUpLivPart.isAccesLivraison = true;
			
			_popUpLivPart.addEventListener("MAIL_LIV_PART", popUpLivraisonHandler);
			
			isLivPartielle 	= false;
			isLivraison 	= false;
			
			PopUpManager.addPopUp(_popUpLivPart, this, true);
			PopUpManager.centerPopUp(_popUpLivPart);
		}
		
		private function popUpTransporteur():void
		{	
			if(myPaginatedatagrid.dgPaginate.selectedItem != null)
			{
				_popUpTransporteurs 			 = new PopUpLivraisonIHM();
				_popUpTransporteurs.myCommande = myPaginatedatagrid.dgPaginate.selectedItem as Commande;		
				_popUpTransporteurs.addEventListener('TRANSPORTEUR_IGNORED', popUpTransporteurHandler);
				_popUpTransporteurs.addEventListener('TRANSPORTEUR_VALIDATE',popUpTransporteurHandler);
				
				PopUpManager.addPopUp(_popUpTransporteurs,this,true);
				PopUpManager.centerPopUp(_popUpTransporteurs);
			}
		}
		
		private function popUpCatalogue():void
		{
			if(cbxPools.selectedItem != null && cbxPools.selectedItem.IDPOOL != -1)
			{
				var catalogue:PopUpCatalogueIHM = new PopUpCatalogueIHM();
					catalogue.idPool 			= cbxPools.selectedItem.IDPOOL;
					catalogue.idrevendeurpool	= cbxPools.selectedItem.IDREVENDEUR;
				
				PopUpManager.addPopUp(catalogue, this, true);
				PopUpManager.centerPopUp(catalogue);
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_pool__'), 'Consoview');
		}
		
		private function popUpCloseAnnul(mySelectedAction:Action):void
		{
			if(mySelectedAction != null)
			{
				_popUpClotureAnnul 					= new PopUpAnnulerCommandeIHM();
				_popUpClotureAnnul.selectedAction 	= mySelectedAction;
				_popUpClotureAnnul.libelleAction 	= mySelectedAction.LIBELLE_ACTION;
				_popUpClotureAnnul.addEventListener('messageSaisie', popUpCloseAnnulHandler);
				
				PopUpManager.addPopUp(_popUpClotureAnnul, this, true);
				PopUpManager.centerPopUp(_popUpClotureAnnul);
			}
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS - LISTENERS POPUPS
			//--------------------------------------------------------------------------------------------//
			
			private function popUpDetailsHandler(e:Event):void
			{
				if(_popUpDetails.isModified)
					getListeCommandes();
			}
			
			private function popUpmailHandler(e:Event):void
			{
				_popUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE, popUpmailHandler);
				
				if(!_popUpMailBox.cbMail.selected)
					ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Validation_enregistr__'), this);
				else
					ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Votre_mail_a__t__envoy__'), this);
				
				PopUpManager.removePopUp(_popUpMailBox);
				
				if(isLivraison)
				{
					var cmdSelected:Commande = (myPaginatedatagrid.dgPaginate.selectedItem as Commande).copyCommande();
					
					if(cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.MODIFICATION_OPTIONS &&
						cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.MODIFICATION_FIXE &&
						cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.RESILIATION &&
						cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.RESILIATION_FIXE &&
						cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.SUSPENSION &&
						cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.SUSPENSION_FIXE &&
						cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.REACTIVATION &&
						cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.REACTIVATION_FIXE &&
						cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.SAV)
					{
						viewLivraison();
					}
				}
				
				getListeCommandes();
			}
			
			private function popUpLivraisonHandler(e:Event):void
			{
				if(!_popUpLivPart.commandePartielleSend)
					PopUpManager.removePopUp(_popUpLivPart);
				else
				{
					_selectedAction = _popUpLivPart.myaction;
					PopUpManager.removePopUp(_popUpLivPart);
					popUpMail();
				}
			}
			
			private function popUpTransporteurHandler(e:Event):void
			{
				popUpMail(true);
			}
			
			private function popUpCloseAnnulHandler(e:Event):void
			{
				_selectedAction = _popUpClotureAnnul.selectedAction;
				
				PopUpManager.removePopUp(_popUpClotureAnnul);
				
				sendAction();
			}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS - EXPORTS
			//--------------------------------------------------------------------------------------------//
		
		private function exportNonDetailleOfSelection():void
		{
			if(myPaginatedatagrid.tempListeSelected.length > 0) 
			{	
				var dataToExport	:String = DataGridDataExporter.getCSVStringFromList(myPaginatedatagrid.dgPaginate, myPaginatedatagrid.tempListeSelected, ";", "\n", true);
				var url				:String = moduleCommandeMobileIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M16/csv/ExportCSV.cfm";
				
				DataGridDataExporter.exportCSVString(url, dataToExport, "export");
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es___exporter__'), 'Consoview', null);
		}
		
		private function exportDetailleAll():void
		{
			_popupPeriodeExport = new PeriodeIHM();
			var point:Point = new Point(stage.mouseX,stage.mouseY);
			_popupPeriodeExport.x = point.x; 
			_popupPeriodeExport.y = point.y;	
			PopUpManager.addPopUp(_popupPeriodeExport, this, true);
			
			_popupPeriodeExport.addEventListener('DATE_EXPORT_ALL_CHOSEN',resultExportAllHandler);
			
		}
		
		private function exportDetailleOfSelection():void
		{
			var len :int  = myPaginatedatagrid.tempListeSelected.length;
			
			_clobIdsCommande = '';
			
			for(var i:int = 0; i < len;i++)
			{
					if(i == 0)
						_clobIdsCommande = myPaginatedatagrid.tempListeSelected[i].IDCOMMANDE;
					else
						_clobIdsCommande = _clobIdsCommande + myPaginatedatagrid.tempListeSelected[i].IDCOMMANDE;
					
					if(i != len-1)
						_clobIdsCommande = _clobIdsCommande + ',';
			}
			
			if(_clobIdsCommande == '')
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es___exporter__'), 'Consoview', null);
			else
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M16', 'Cela_peut_prendre_quelques_minutes__Voul'), 'Consoview', resultAlertExport);
		}
		
		private function exportAllData(chosenMonth:String):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M16.export.ExportAllOrders", "runExport", exportAllHandler);
			
			var idpool:int= -1;
			if(cbxPools.selectedItem != null)
				idpool = cbxPools.selectedItem.IDPOOL;
			
			RemoteObjectUtil.callService(op, idpool, chosenMonth);
		}
		
		internal function exportAllHandler(re:ResultEvent):void
		{
			if(re.result!=-1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Votre_demande_de_rapport____t__prise_en_'));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M16', 'Votre_demande_de_rapport_n___pu__tre_tra'));
			}
		}
		
		private function exportData():void
		{
			var url		  :String = moduleCommandeMobileIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M16/csv/ExportCSV2.cfm";
			var request	  :URLRequest = new URLRequest(url);         
			var variables :URLVariables = new URLVariables();
				variables.FILE_NAME 	= ResourceManager.getInstance().getString('M16', 'Commandes');
			
			var lastIdx:int = _clobIdsCommande.lastIndexOf(',');
			
			if(_clobIdsCommande.lastIndexOf(',')+1 == _clobIdsCommande.length)
				_clobIdsCommande = _clobIdsCommande.substr(0,_clobIdsCommande.length-1);
			
			variables.ID_COMMANDES  = _clobIdsCommande;
			variables.METHODE 		= "fournirDataSetToExport";
			
			request.data = variables;
			request.method = URLRequestMethod.POST;
			
			navigateToURL(request,"_blank");
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS - MAPPING
			//--------------------------------------------------------------------------------------------//
		
		private function mappPool(valueObject:Object):Pool
		{
			var poolObject:Pool 			= new Pool();
				poolObject.IDPOOL			= valueObject.IDPOOL;
				poolObject.IDPROFIL			= valueObject.IDPROFIL;
				poolObject.IDREVENDEUR		= valueObject.IDREVENDEUR;
				poolObject.LIBELLE_CONCAT	= StringUtil.trim(valueObject.LIBELLE_POOL) + " - " + StringUtil.trim(valueObject.LIBELLE_PROFIL);
				poolObject.LIBELLE_POOL		= valueObject.LIBELLE_POOL;
				poolObject.LIBELLE_PROFIL	= valueObject.LIBELLE_PROFIL; 
			
			return poolObject;
		}
		
		public function get IS_SPIE():Boolean { return _IS_SPIE; }
		
		public function set IS_SPIE(value:Boolean):void
		{
			if (_IS_SPIE == value)
				return;
			_IS_SPIE = value;
		}
												 
	}
}