package gestioncommande.entity
{
	import mx.collections.ArrayCollection;

	public class ArticleItemBaseVO
	{
		private var _idArticle			:int;
		private var _referenceArticle	:String; // Catégorie de l'article Infrastructure ou Poste
		private var _designationArticle	:String;
		private var _prixUnitaire		:Number = 0;
		
		private var _quantite			:int = 0;
		private var _prixTotale			:Number = 0; // prixUnit * quantite
		private var _listReference		:ArrayCollection = new ArrayCollection(); // Poste, infrastructure, mobile, ...
		
		private var _isItem			:Boolean = false;
		private var _selected		:Boolean = false;
		
		private var _commentaire:String;
		private var _idProduitCatalogue:Number;
		private var _idThemeProduit:Number;
		private var _libelleProduit:String;
		private var _segmentTheme:String;
		private var _surTheme:String;
		private var _themeLibelle:String;
		private var _typeAbonnement:String;
		private var _typeTheme:String;
		private var _boolAcces:String;
		
		public function ArticleItemBaseVO()
		{
		}
		
		public function get selected():Boolean { return _selected; }
		
		public function set selected(value:Boolean):void
		{
			if (_selected == value)
				return;
			_selected = value;
		}
		
		public function get isItem():Boolean { return _isItem; }
		
		public function set isItem(value:Boolean):void
		{
			if (_isItem == value)
				return;
			_isItem = value;
		}
		
		[Bindable]
		public function get quantite():int
		{
			return _quantite;
		}
		
		public function set quantite(value:int):void
		{
			_quantite = value;
		}
		
		[Bindable]
		public function get listReference():ArrayCollection
		{
			return _listReference;
		}
		
		public function set listReference(value:ArrayCollection):void
		{
			_listReference = value;
		}
		[Bindable]		
		public function get prixUnitaire():Number { return _prixUnitaire; }
		
		public function set prixUnitaire(value:Number):void
		{
			if (_prixUnitaire == value)
				return;
			_prixUnitaire = value;
		}
		[Bindable]
		public function get prixTotale():Number
		{
			return _prixTotale;
		}
		
		public function set prixTotale(value:Number):void
		{
			_prixTotale = value;
		}
		
		[Bindable]
		public function get idArticle():int { return _idArticle; }
		
		public function set idArticle(value:int):void
		{
			if (_idArticle == value)
				return;
			_idArticle = value;
		}
		
		[Bindable]
		public function get designationArticle():String { return _designationArticle; }
		
		public function set designationArticle(value:String):void
		{
			if (_designationArticle == value)
				return;
			_designationArticle = value;
		}
		
		[Bindable]
		public function get referenceArticle():String { return _referenceArticle; }
		
		public function set referenceArticle(value:String):void
		{
			if (_referenceArticle == value)
				return;
			_referenceArticle = value;
		}
		
		public function get typeTheme():String { return _typeTheme; }
		public function set typeTheme(value:String):void
		{
			if (_typeTheme == value)
				return;
			_typeTheme = value;
		}
		
		public function get typeAbonnement():String { return _typeAbonnement; }
		public function set typeAbonnement(value:String):void
		{
			if (_typeAbonnement == value)
				return;
			_typeAbonnement = value;
		}
		
		public function get themeLibelle():String { return _themeLibelle; }
		public function set themeLibelle(value:String):void
		{
			if (_themeLibelle == value)
				return;
			_themeLibelle = value;
		}
		
		public function get surTheme():String { return _surTheme; }
		public function set surTheme(value:String):void
		{
			if (_surTheme == value)
				return;
			_surTheme = value;
		}
		
		public function get segmentTheme():String { return _segmentTheme; }
		public function set segmentTheme(value:String):void
		{
			if (_segmentTheme == value)
				return;
			_segmentTheme = value;
		}
		
		
		public function get libelleProduit():String { return _libelleProduit; }
		public function set libelleProduit(value:String):void
		{
			if (_libelleProduit == value)
				return;
			_libelleProduit = value;
		}
		
		public function get idThemeProduit():Number { return _idThemeProduit; }
		public function set idThemeProduit(value:Number):void
		{
			if (_idThemeProduit == value)
				return;
			_idThemeProduit = value;
		}
		
		
		public function get idProduitCatalogue():Number { return _idProduitCatalogue; }
		public function set idProduitCatalogue(value:Number):void
		{
			if (_idProduitCatalogue == value)
				return;
			_idProduitCatalogue = value;
		}
		
		public function get commentaire():String { return _commentaire; }
		public function set commentaire(value:String):void
		{
			if (_commentaire == value)
				return;
			_commentaire = value;
		}
		
		public function get boolAcces():String { return _boolAcces; }
		public function set boolAcces(value:String):void
		{
			if (_boolAcces == value)
				return;
			_boolAcces = value;
		}
		
	}
}