package gestioncommande.entity
{
	
	import flash.utils.describeType;
	
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class ElementsCommande2
	{
		
		public var ID				:int = -1;
		
		public var RANDOM			:int = 0;
		
		public var TYPE				:int = 0;
		
		public var IDTYPELIGNE		:int = 0;
		
		public var TYPELIGNE		:String = "";
			
		//---> TERMINAUX + CARTE SIM
		public var TERMINAUX 		:ArrayCollection = new ArrayCollection();
		
		//---> ACCESSOIRES
		public var ACCESSOIRES 		:ArrayCollection = new ArrayCollection();
		
		//---> ABONNEMENTS
		public var ABONNEMENTS		:ArrayCollection = new ArrayCollection();
		
		//---> OPTIONS
		public var OPTIONS			:ArrayCollection = new ArrayCollection();
		
		//---> OPTIONS DEJA PRESENTE DANS LE FORFAIT
		public var LINEOPTIONS		:ArrayCollection = new ArrayCollection();
		
		//---> NOMBRE DE CONFIGURATION
		public var CONFIGURATIONS	:ArrayCollection = new ArrayCollection();
		
		//---> DUREE D'ENGAGEMENT POUR LES EQUIPEMENTS ET RESSOURCES SÉLECTIONNÉ
		public var ENGAGEMENT		:Engagement = new Engagement();
		
		public var POSTES_INFRAST			:ArrayCollection = new ArrayCollection(); // pour SPIE
		
		
		public function ElementsCommande2()
		{
			this.RANDOM = randomNumber();
		}
		
		public function randomNumber():int
		{
			return Math.random() * 1000000;
		}
		
		public function copyElementsCommande():ElementsCommande2
		{
			var copyElmts	:ElementsCommande2 = new ElementsCommande2();
			var classInfo 	:XML = describeType(this);
			
			for each (var v:XML in classInfo..accessor)
			{
				if (this.hasOwnProperty(v.@name))
				{
					copyElmts[v.@name] = this[v.@name];
				}
			}   
			
			return copyElmts;         				 		
		}
		
	}
}