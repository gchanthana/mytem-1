package gestioncommande.entity
{
	import mx.collections.ArrayCollection;

	
	[Bindable]
	public class ArticleLivre
	{

		public var ID_TYPE				:int = 0;//1-> MOBILE/CARTE SIM, 2-> ACCESSOIRE, 3-> ABO/OPTION, 4-> SANS EQUIPEMENT
		public var ARTICLE_ID			:int = 0;
		public var ARTICLE_IDTYPE		:int = 0;
		public var ARTICLE_IDSSTETE		:int = 0;
		public var IDSOUSTETE			:int = 0;
		
		public var ARTICLE_ACCES		:Boolean = false;
		public var ARTICLE_PUK_VISIBLE	:Boolean = false;
				
		public var BOOL_SOUSTETE		:Boolean = true;
		
		public var ARTICLE_TYPE			:String = '';
		public var ARTICLE_LIBELLE		:String = '';
		public var ARTICLE_NSERIE		:String = '';
		public var ARTICLE_PUK			:String = '';
		public var ARTICLE_PIN			:String = '';
		public var ARTICLE_SSTETE		:String = '';
		
		public var CHAR_MAX				:int = 15;
		public var CHAR_RESTRICT		:String = "0-9";
		
		public function ArticleLivre()
		{
		}
	}
}