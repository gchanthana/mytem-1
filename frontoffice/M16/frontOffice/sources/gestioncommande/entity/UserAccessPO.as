package gestioncommande.entity
{
	public dynamic class UserAccessPO
	{
		public function UserAccessPO()
		{
		}
		
		public function removeAll():Boolean
		{
			for(var propertie:String in this)
			{
				this[propertie] = null;
			}
			
			return true
		}
	}
}