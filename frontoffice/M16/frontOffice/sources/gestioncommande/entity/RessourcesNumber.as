package gestioncommande.entity
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class RessourcesNumber extends EventDispatcher
	{
		public var NB_ABO		:int = 0;
		public var NB_ABO_OBL	:int = 0;
		public var NB_ABO_DEF	:int = 0;
		public var NB_OPT		:int = 0;
		public var NB_OPT_OBL	:int = 0;
		public var NB_OPT_DEF	:int = 0;
		
		private var _idOperator	:int = 0;
		private var _idProfilEq	:int = 0;
		
		public function RessourcesNumber(idOperator:int, idProfilEq:int)
		{
			this._idOperator = idOperator;
			this._idProfilEq = idProfilEq;
			
			getInfosNumberRessources();
		}
		
		private function getInfosNumberRessources():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.RessourceService",
																				"getInfosNumberRessources",
																				infosNumberRessourcesHandler);
																			
			RemoteObjectUtil.callService(op,_idOperator,
											_idProfilEq);
		}
		
		private function infosNumberRessourcesHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var infos:Object = re.result as Object;
				
				this.NB_ABO 	= infos.NB_ABO;
				this.NB_ABO_OBL = infos.NB_ABO_OBL;
				this.NB_ABO_DEF = infos.NB_ABO_DEF;
				this.NB_OPT 	= infos.NB_OPT;
				this.NB_OPT_OBL = infos.NB_OPT_OBL;
				this.NB_OPT_DEF = infos.NB_OPT_DEF;
				
				dispatchEvent(new Event('RESSOURCES_NUMBER'));
			}
		}
		
	}
}