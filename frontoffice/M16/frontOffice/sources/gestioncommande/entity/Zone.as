package gestioncommande.entity
{
	public class Zone
	{
		
		public var ID_UNIQUE_ZONE	:int 	 	 = 0;
		public var COMPTEUR_ZONE	:int 	 	 = 0;
		public var COMPTEUR_OBLIG	:int 	 	 = 0;
		public var STEPPEUR_ZONE	:int 	 	 = 20;
		public var TYPE_ZONE		:String  	 = "-";
		public var IDTYPE_ZONE		:int 	 	 = 0;
		public var CONTENU_ZONE		:ContenuZone = new ContenuZone();
		public var SAISIE_ZONE		:Boolean 	 = false;
		public var EXACT_NBR		:Boolean 	 = false;
		
		
		public function Zone()
		{
			ID_UNIQUE_ZONE = Math.random()*1000000;
		}

	}
}