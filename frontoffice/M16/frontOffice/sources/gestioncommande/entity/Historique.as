package gestioncommande.entity
{
	[Bindable]
	public class Historique
	{
		
		public var IDETAT					:Number = 0;
		public var IDACTION					:Number = 0;
		public var IDUSER_ACTION			:Number = 0;
		public var REF_CALCUL				:Number = 0;
		public var ENTRAINE_CLOTURE			:Number = 0;
		public var ENTRAINE_CREATION		:Number = 0;	
		public var CHANGER_ETAT_INVENTAIRE	:Number = 0;
		public var CHANGER_ETAT_PARC		:Number = 0;
		public var IDUSER_CREATE			:Number = 0;
		public var DISPLAY_MAIL				:Number = 0;
		
		public var CODE_ETAT				:String = '';
		public var LIBELLE_ETAT				:String = '';
		public var LIBELLE_ACTION			:String = '';	    
		public var PATRONYME_USERACTION		:String = '';
		public var COMMENTAIRE_ETAT			:String = '';
		public var EMAIL					:String = '';
		public var DATE_STRING				:String = '';
		
		public var DATE_ACTION				:Date = null;
		public var DATE_INSERT				:Date = null;
		
		public function Historique()
		{
		}
	}
}