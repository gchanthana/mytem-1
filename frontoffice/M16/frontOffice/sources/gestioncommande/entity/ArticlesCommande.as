package gestioncommande.entity
{
	
	import composants.util.lignes.LignesUtils;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;

	[Bindable]
	public class ArticlesCommande
	{
		
		public var ARTICLE_ID				:int = 0;		
		public var ARTICLE_IDSSTETE			:int = 0;
		public var VERIFICATION				:int = 1;//---> 1-> VALIDE, 2-> VERIFICATION EN COURS, 3-> ERREUR
		public var IDSOUSTETE				:int = 0;

		public var CHAR_RESTRICT			:String = '0-9';
		public var CHAR_MAX					:int = 40;
		
		public var ARTICLE_SSTETE_TEMP		:String = '';
		public var ARTICLE_SSTETE			:String = '';
		public var ARTICLE_ENG				:String = '';
		public var ARTICLE_NOMEMPLOYE		:String = '';
		public var ARTICLES_ENG_RESS		:String = '';
		public var ARTICLES_ENG_EQUI		:String = '';
		public var ARTICLE_LIV_STRDATE		:String = '';
				
		public var ARTICLES_EQUIPEMENTS		:ArrayCollection = new ArrayCollection();
		public var ARTICLES_RESSIOURCES		:ArrayCollection = new ArrayCollection();
		
		public var ACTION_SELECTED			:Action = new Action();
		
		public var ARTICLE_ACCESLIVRAISON	:Boolean = false;
		public var ARTICLE_LIVRER			:Boolean = false;
		
		public var TERMINAL_PRESENT			:Boolean = false;
		public var EQUIPEMENTS_VISIBLE		:Boolean = false;
		public var RESSOURCES_VISIBLE		:Boolean = false;

		public var ARTICLE_IN_EMPLOYE		:Boolean = true;
		
		public var ARTICLE_AFFECTATION		:Boolean = true;
		public var ARTICLE_DESAFFECTATION	:Boolean = false;
		public var ARTICLE_REBUT			:Boolean = false;
		public var ARTICLE_IS_AT_EMPLOYE	:Boolean = false;
		
		public var ISVALIDE					:Boolean = true;
		
		public var ISMOBILE					:Boolean = true;
		public var ISSIM					:Boolean = true;
		
		public var MYCOMMANDE				:Commande = new Commande();
		
		public var LIGNESUTILS				:LignesUtils = new LignesUtils();

		
		public function ArticlesCommande()
		{
			var date:Date = new Date();
			
			ACTION_SELECTED.DEST				= "C";
			ACTION_SELECTED.DATE_ACTION			= date;
			ACTION_SELECTED.IDACTION 			= 2073;
			ACTION_SELECTED.CODE_ACTION			= "EXPED";
			ACTION_SELECTED.EXP					= "R";
			ACTION_SELECTED.DATE_HOURS 			= formatDate(date) + " " + formatHour(date);
			ACTION_SELECTED.COMMENTAIRE_ACTION 	= ResourceManager.getInstance().getString('M16','Expedier_la_commande');
			ACTION_SELECTED.LIBELLE_ACTION 		= ResourceManager.getInstance().getString('M16','Expedier_la_commande');
			ACTION_SELECTED.MESSAGE				= "";
		}
		
		private function formatDate(date:Date):String
		{
			var day:String = date.date.toString();
			if(day.length < 2)
			{day = "0" + day;}
			var month:int = Number(date.month) + 1;
			var temp:String = month.toString();
			if(temp.length < 2)
			{temp = "0" + temp;}
			var year:String = date.fullYear.toString();
			return year + "/" + temp + "/" + day;
		}
		
		private function formatHour(date:Date):String
		{
			var hours	:String = date.hours.toString();
			var minutes	:String = date.minutes.toString();
			var seconds	:String = date.seconds.toString();
			
			return hours + ":" + minutes + ":" + seconds;
		}
	}
}