package gestioncommande.entity
{
	[Bindable]
	public class Gestionnaire
	{
		private var _id : int;
		private var _nom : String;
		private var _prenom : String;
		private var _nom_prenom : String;
		private var _login : String;
				
		public function Gestionnaire()
		{
			
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------GET------------------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
		public function get id():int
		{	
			return _id;
		}
		public function get login():String
		{	
			return _login;
		}
		public function get nom():String
		{	
			return _nom;
		}
		public function get prenom():String
		{	
			return _prenom;
		}
		public function get nom_prenom():String
		{	
			_nom_prenom=_prenom+" "+_nom; 
			return _nom_prenom;
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------SET------------------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
		public function set id(id:int):void
		{	
			this._id = id;
		}
		public function set nom(nom:String):void
		{	
			this._nom =nom ;
		}
		public function set prenom(prenom:String):void
		{	
			this._prenom =prenom ;
		}
		public function set login(login:String):void
		{	
			this._login =login ;
		}
		

	}
}