package gestioncommande.entity
{
	import composants.util.DateFunction;
	
	
	import mx.collections.ArrayCollection;
	
	
	[Bindable]
	public class Configuration
	{
		
		public var CONFIGURATION_NAME		:String = "";
		
		public var ENGAGEMENT				:Engagement = new Engagement();
		public var PORTABILITE_OBJECT		:Portabilite = new Portabilite();
		
		
		
		public var FPC						:String	= "";
		
		public var IS_LIBELLE				:Boolean = true;
		public var IS_COLLABORATEUR			:Boolean = false;
		public var IS_PORTABILITE			:Boolean = false;
		public var IS_EQUIP_NU				:Boolean = false;
		
		public var PORTABILITE				:String = "";
		public var LIBELLE					:String = "";
		
		public var COLLAB_COLLABORATEUR		:String = "";
		public var COLLAB_NAME				:String = "";
		public var COLLAB_CLE_IDENTIFIANT	:String = "";
		public var COLLAB_EMAIL				:String = "";
		public var COLLAB_MATRICULE			:String = "";
		public var COLLAB_ID				:int 	= 0;
		public var COLLAB_INOUT				:int 	= 0;
		public var COLLAB_IDEMPLOYE			:int 	= 0;
		public var COLLAB_IDGROUPE_CLIENT	:int 	= 0;

		public var NUMERO_HIDE				:String = "";
		public var NUMERO					:String = "";
		public var CODERIO					:String = "";
		
		public var IDSOUS_TETE				:int = 0;

		
		public var DATE_PORTABILITE			:Date 	= null;
		public var DATE_PORTABILITE_HIDE	:Date 	= DateFunction.addXJoursOuvreesToDate(new Date(),10);

		
		public var ID						:int = 0;
		public var NUMEROCONFIG_UNIQUE		:int = 0;
		public var NOMBRE_CONFIGURATION		:int = 1;
		public var IDCONFIGNUMBER			:int = 1;
		public var CONFIGURATION_ID			:int = 1;	
		
		
		
		public var SELECTEDINDEX			:int = -1;
		public var ORGACLIENTE				:ArrayCollection = new ArrayCollection();

		public var LIBELLETYPELIGNE			:String = "";
		public var LIBELLE_SOURCE			:String = "";
		public var LIBELLE_CIBLE			:String = "";
		public var IDSOURCE					:int = 0;
		public var IDCIBLE					:int = 0;

		public var IDTYPECOMMANDE			:int = 0;
		public var IDSEGMENT				:int = 0;
		public var IDTYPELIGNE				:int = 0;
		
		public var IDUNIQUE					:int = 0;
		
		public function Configuration()
		{
		}

	}
}
