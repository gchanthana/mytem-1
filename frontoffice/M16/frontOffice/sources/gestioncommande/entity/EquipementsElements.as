package gestioncommande.entity
{
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.resources.ResourceManager;
	
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.Formator;

	[Bindable]
	public class EquipementsElements
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		public var EQUIPEMENT		:String = "";
		
		public var LIBELLE			:String = "";
		
		public var PHOTO			:String = "";
		private var _PHOTO_LOW:String = '';
		public var MARQUE			:String = "";
		public var REF_REVENDEUR	:String = "";
		public var REFERENCE_PRODUIT	:String = "";
		
		public var PRIX_REN			:String = "0";//---> PRIX RENOUVELLEMNT
		public var PRIX_S			:String = "0";//---> PRIX AVEC SYMBOLE
		public var PRIX_CS			:String = "0";//---> PRIX CATALOGUE AVEC SYMBOLE
		public var PRIX_C			:String = "0";//---> PRIX CATALOGUE
		public var PRIX_2			:String = "0";//---> PRIX 12
		public var PRIX_3			:String = "0";//---> PRIX 24
		public var PRIX_4			:String = "0";//---> PRIX 32
		public var PRIX_6			:String = "0";//---> PRIX 48
		public var PRIX_5			:String = "0";//---> PRIX 12 R
		public var PRIX_7			:String = "0";//---> PRIX 24 R
		public var PRIX_8			:String = "0";//---> PRIX 32 R
		public var PRIX_9			:String = "0";//---> PRIX 48 R
		
		public var IDEQUIPEMENTPARC			:String = "";
		public var IMEI						:String = "";
		
		public var PRIXR			:Number = 0;
		public var PRIXC			:Number = 0;
		public var PRIX				:Number = 0;
		
		public var DATEVALIDITE_CLT :Date = null;
		public var DELAIALERT		:Number  = 0;
		public var SUSPENDU_CLT		:Number = 0; 
		
		public var NIVEAU			:String = "";

		public var IDEQUIPEMENT		:int = 0;
		public var IDCLIENT			:int = 0;
		public var IDFOURNISSEUR	:int = -10;
		public var IDPARENT			:int = 0;
		
		public var SELECTED			:Boolean = false;
		public var DETAILS			:Boolean = true;
		public var EDITABLE			:Boolean = false;
		
		private var _osPhone			:String;
		private var _networkPhone		:String;
		private var _screenSizePhone	:String;
		private var _cameraPhone		:String; // résolution caméra
		private var _dasPhone			:String;
		private var _pricePhoneToShow	:String;
		private var _pathToPhoto		:String = 'http://images.consotel.fr';
		private var _pathToPhotoLow		:String = 'http://images.consotel.fr';
		
		public const SRC_NO_PHOTO		:String = 'http://images.consotel.fr/noPhoto_128.png';
		public var _equipementsSrv	:EquipementService = new EquipementService();
		
		private var _tabListeDetailEquipement	:ArrayCollection;
		private var _tabListeTarif				:ArrayCollection;
		private var _tabListeTarif2				:ArrayCollection;
		private var _tabPhoto:ArrayCollection;
		
		private var XML1						:XML;
		private var XML2						:XML;
		public var XMLEquipement				:XMLListCollection;
		private var _SYMBOL_DEVICE:String;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function EquipementsElements()
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODE PUBLIC STATIC FORMATEUR
	//--------------------------------------------------------------------------------------------//
		
		public static function formatTerminal(value:Object):EquipementsElements
		{
			var elements:EquipementsElements = new EquipementsElements();			
				elements.EQUIPEMENT 		 = value.TYPE_EQUIPEMENT;
				elements.LIBELLE 			 = value.LIBELLE_EQ;
				elements.PRIX 				 = (value.PRIX_CATALOGUE == null)?0:value.PRIX_CATALOGUE;
				elements.PRIXC				 = (value.PRIX_CATALOGUE == null)?0:value.PRIX_CATALOGUE;
				elements.PRIX_S				 = Formator.formatTotalWithSymbole(elements.PRIX);
				elements.PRIX_C 			 = (value.PRIX_CATALOGUE == null)?'0':value.PRIX_CATALOGUE;
				elements.PRIX_2 			 = (value.PRIX_2 == null)?'0':value.PRIX_2;
				elements.PRIX_3 			 = (value.PRIX_3 == null)?'0':value.PRIX_3;
				elements.PRIX_4 			 = (value.PRIX_4 == null)?'0':value.PRIX_4;
				elements.PRIX_5 			 = (value.PRIX_5 == null)?'0':value.PRIX_5;
				elements.PRIX_6 			 = (value.PRIX_6 == null)?'0':value.PRIX_6;
				elements.PRIX_7 			 = (value.PRIX_7 == null)?'0':value.PRIX_7;
				elements.PRIX_8 			 = (value.PRIX_8 == null)?'0':value.PRIX_8;
				elements.PRIX_9 			 = (value.PRIX_9 == null)?'0':value.PRIX_9;
				elements.NIVEAU 			 = value.NIVEAU;
				elements.IDFOURNISSEUR 		 = value.IDEQUIPEMENT_FOURNISSEUR;
				elements.IDCLIENT 			 = value.IDEQUIPEMENT_CLIENT;
				elements.IDEQUIPEMENT		 = value.IDTYPE_EQUIPEMENT;
				elements.EDITABLE			 = (moduleCommandeMobileIHM.userAcess.hasOwnProperty('E_PRIX_T'))? (moduleCommandeMobileIHM.userAcess.E_PRIX_T == 1) : false;
				elements.PHOTO				 = value.PHOTO;
				elements.MARQUE				 = value.MARQUE;
				elements.DATEVALIDITE_CLT	 = value.DATEVALIDITE_CLT;
				elements.DELAIALERT			 = value.DELAIALERT;
				elements.SUSPENDU_CLT		 = value.SUSPENDU_CLT;
				elements.REF_REVENDEUR		 = value.REF_REVENDEUR;
				elements.REFERENCE_PRODUIT	= value.REF_REVENDEUR;
				elements.osPhone		 	= (value.SYS_EXPLOITATION != null)?value.SYS_EXPLOITATION:'';
				elements.networkPhone		= (value.RESEAU != null)?value.RESEAU:'';
				elements.cameraPhone		= (value.CAMERA != null)?value.CAMERA:'';
				elements.screenSizePhone	= (value.TAILLE_ECRAN != null)?value.TAILLE_ECRAN:'';
				elements.dasPhone		 	= (value.DAS != null)?value.DAS:'';
				elements.pathToPhoto		= (elements.PHOTO != null)? elements.pathToPhoto+elements.PHOTO : elements.SRC_NO_PHOTO;
				elements.pathToPhotoLow		= (elements.PHOTO_LOW != null)? elements.pathToPhotoLow+elements.PHOTO_LOW : elements.SRC_NO_PHOTO;
				elements.SYMBOL_DEVICE		= (value.SYMBOL != null)?value.SYMBOL:'';
				
				
			return elements;
		}
		
		public function fill(value:Object):void
		{
			this.EQUIPEMENT 		 = value.TYPE_EQUIPEMENT;
			this.LIBELLE 			 = value.LIBELLE_EQ;
			this.PRIXC				 = (value.PRIX_CATALOGUE == null)?0:value.PRIX_CATALOGUE;
			this.PRIX_C 			 = (value.PRIX_CATALOGUE == null)?'0':value.PRIX_CATALOGUE;
			this.PRIX_2 			 = (value.PRIX_2 == null)?'0':value.PRIX_2;
			this.PRIX_3 			 = (value.PRIX_3 == null)?'0':value.PRIX_3;
			this.PRIX_4 			 = (value.PRIX_4 == null)?'0':value.PRIX_4;
			this.PRIX_5 			 = (value.PRIX_5 == null)?'0':value.PRIX_5;
			this.PRIX_6 			 = (value.PRIX_6 == null)?'0':value.PRIX_6;
			this.PRIX_7 			 = (value.PRIX_7 == null)?'0':value.PRIX_7;
			this.PRIX_8 			 = (value.PRIX_8 == null)?'0':value.PRIX_8;
			this.PRIX_9 			 = (value.PRIX_9 == null)?'0':value.PRIX_9;
			this.NIVEAU 			 = value.NIVEAU;
			this.IDFOURNISSEUR 		 = value.IDEQUIPEMENT_FOURNISSEUR;
			this.IDCLIENT 			 = value.IDEQUIPEMENT_CLIENT;
			this.IDEQUIPEMENT		 = value.IDTYPE_EQUIPEMENT;
			this.EDITABLE			 = (moduleCommandeMobileIHM.userAcess.hasOwnProperty('E_PRIX_T'))? (moduleCommandeMobileIHM.userAcess.E_PRIX_T == 1) : false;
			this.PHOTO				 = value.PHOTO;
			this.PHOTO_LOW			 = value.PHOTO_LOW;
			this.MARQUE				 = value.MARQUE;
			this.DATEVALIDITE_CLT	 = value.DATEVALIDITE_CLT;
			this.DELAIALERT			 = value.DELAIALERT;
			this.SUSPENDU_CLT		 = value.SUSPENDU_CLT;
			this.REF_REVENDEUR		 = value.REF_REVENDEUR;
			this.REFERENCE_PRODUIT   = value.REF_REVENDEUR;
			this.IDEQUIPEMENTPARC	 = (value.IDTERM != null)?value.IDTERM:'';
			
			this.osPhone		 	= (value.SYS_EXPLOITATION != null)?value.SYS_EXPLOITATION:'';
			this.networkPhone		= (value.RESEAU != null)?value.RESEAU:'';
			this.cameraPhone		= (value.CAMERA != null)?value.CAMERA:'';
			this.screenSizePhone	= (value.TAILLE_ECRAN != null)?value.TAILLE_ECRAN:'';
			this.dasPhone		 	= (value.DAS != null)?value.DAS:'';
			this.pathToPhoto		= (this.PHOTO != null)?this.pathToPhoto+this.PHOTO : this.SRC_NO_PHOTO;
			this.pathToPhotoLow		= (this.PHOTO_LOW != null)?this.pathToPhotoLow+this.PHOTO_LOW : this.SRC_NO_PHOTO;
			this.SYMBOL_DEVICE		= (value.SYMBOL != null)?value.SYMBOL:'';
		}
		
		public function fillEquipFromStock(value:Object):void
		{
			this.EQUIPEMENT 		 = value.TYPE_EQUIPEMENT;
			this.LIBELLE 			 = value.LIBELLE_EQ;
			this.PRIXC				 = 0;
			this.PRIX_C 			 = '0';
			this.PRIX_2 			 = '0';
			this.PRIX_3 			 = '0';
			this.PRIX_4 			 = '0';
			this.PRIX_5 			 = '0';
			this.PRIX_6 			 = '0';
			this.PRIX_7 			 = '0';
			this.PRIX_8 			 = '0';
			this.PRIX_9 			 = '0';
			this.NIVEAU 			 = value.NIVEAU;
			this.IDFOURNISSEUR 		 = value.IDEQUIPEMENT_FOURNISSEUR;
			this.IDCLIENT 			 = value.IDEQUIPEMENT_CLIENT;
			this.IDEQUIPEMENT		 = value.IDTYPE_EQUIPEMENT;
			this.EDITABLE			 = (moduleCommandeMobileIHM.userAcess.hasOwnProperty('E_PRIX_T'))? (moduleCommandeMobileIHM.userAcess.E_PRIX_T == 1) : false;
			this.PHOTO				 = value.PHOTO;
			this.PHOTO_LOW			 = value.PHOTO_LOW;
			this.MARQUE				 = value.MARQUE;
			this.DATEVALIDITE_CLT	 = value.DATEVALIDITE_CLT;
			this.DELAIALERT			 = value.DELAIALERT;
			this.SUSPENDU_CLT		 = value.SUSPENDU_CLT;
			this.REF_REVENDEUR		 = value.REF_REVENDEUR;
			this.REFERENCE_PRODUIT   = value.REF_REVENDEUR;
			this.IDEQUIPEMENTPARC	 = (value.IDTERM != null)?value.IDTERM:'';
			this.IMEI				 = (value.IMEI != null)?value.IMEI:'';
			
			this.osPhone		 	= (value.SYS_EXPLOITATION != null)?value.SYS_EXPLOITATION:'';
			this.networkPhone		= (value.RESEAU != null)?value.RESEAU:'';
			this.cameraPhone		= (value.CAMERA != null)?value.CAMERA:'';
			this.screenSizePhone	= (value.TAILLE_ECRAN != null)?value.TAILLE_ECRAN:'';
			this.dasPhone		 	= (value.DAS != null)?value.DAS:'';
			this.pathToPhoto		= (this.PHOTO != null)?this.pathToPhoto+this.PHOTO : this.SRC_NO_PHOTO;
			this.pathToPhotoLow		= (this.PHOTO_LOW != null)?this.pathToPhotoLow+this.PHOTO_LOW : this.SRC_NO_PHOTO;
			this.SYMBOL_DEVICE		= (value.SYMBOL != null)?value.SYMBOL:'';
		}
		
		public static function formatAccessoire(value:Object):EquipementsElements
		{
			var elements:EquipementsElements = new EquipementsElements();			
				elements.EQUIPEMENT 		 = value.TYPE_EQUIPEMENT;
				elements.LIBELLE 			 = value.LIBELLE_EQ;
				elements.PRIX 				 = (value.PRIX_CATALOGUE == null)?0:value.PRIX_CATALOGUE;
				elements.PRIXC				 = (value.PRIX_CATALOGUE == null)?0:value.PRIX_CATALOGUE;
				elements.PRIX_S				 = Formator.formatTotalWithSymbole(elements.PRIX);
				elements.PRIX_C 			 = (value.PRIX_CATALOGUE == null)?'0':value.PRIX_CATALOGUE;
				elements.PRIX_2 			 = (value.PRIX_2 == null)?'0':value.PRIX_2;
				elements.PRIX_3 			 = (value.PRIX_3 == null)?'0':value.PRIX_3;
				elements.PRIX_4 			 = (value.PRIX_4 == null)?'0':value.PRIX_4;
				elements.PRIX_5 			 = (value.PRIX_5 == null)?'0':value.PRIX_5;
				elements.PRIX_6 			 = (value.PRIX_6 == null)?'0':value.PRIX_6;
				elements.PRIX_7 			 = (value.PRIX_7 == null)?'0':value.PRIX_7;
				elements.PRIX_8 			 = (value.PRIX_8 == null)?'0':value.PRIX_8;
				elements.PRIX_9 			 = (value.PRIX_9 == null)?'0':value.PRIX_9;
				elements.NIVEAU 			 = value.NIVEAU;
				elements.REF_REVENDEUR		 = value.REF_REVENDEUR;
				elements.REFERENCE_PRODUIT	 = value.REF_REVENDEUR;

				elements.IDFOURNISSEUR 		 = value.IDEQUIPEMENT_FOURNISSEUR;
				elements.IDCLIENT 			 = value.IDEQUIPEMENT_CLIENT;
				elements.IDEQUIPEMENT		 = value.IDTYPE_EQUIPEMENT;
				elements.PHOTO				 = value.PHOTO;
				elements.PHOTO_LOW			 = value.PHOTO_LOW;
				elements.MARQUE				 = value.MARQUE;
				elements.DATEVALIDITE_CLT	 = value.DATEVALIDITE_CLT;
				elements.DELAIALERT			 = value.DELAIALERT;
				elements.SUSPENDU_CLT		 = value.SUSPENDU_CLT;
				elements.SYMBOL_DEVICE		= (value.SYMBOL != null)?value.SYMBOL:'';
			
			return elements;
		}
		
		public static function formatSim(value:Object):EquipementsElements
		{
			var elements:EquipementsElements = new EquipementsElements();			
				elements.EQUIPEMENT 		 = value.TYPE_EQUIPEMENT;
				elements.LIBELLE 			 = value.LIBELLE_EQ;
				elements.IDFOURNISSEUR 		 = value.IDEQUIPEMENT_FOURNISSEUR;
				elements.IDEQUIPEMENT		 = value.IDTYPE_EQUIPEMENT;
				elements.PRIX				 = 0;
				elements.PRIX_S				 = Formator.formatTotalWithSymbole(elements.PRIX);
			
			return elements;
		}
		
		public static function formatTerminalEmpty():EquipementsElements
		{
			var elements:EquipementsElements = new EquipementsElements();			
				elements.EQUIPEMENT 		 = ResourceManager.getInstance().getString('M16', 'Aucun_equipement');
				elements.LIBELLE 			 = ResourceManager.getInstance().getString('M16', 'Aucun_equipement');
				elements.PRIX				 = 0;
				elements.PRIX_S				 = Formator.formatTotalWithSymbole(elements.PRIX);
				elements.DETAILS			 = false;
				elements.osPhone			 = '';
				elements.screenSizePhone	 = '';
				elements.cameraPhone		 = '';
				elements.dasPhone			 = '';
				elements.networkPhone		 = '';
				elements.pathToPhoto		 = 'http://images.consotel.fr/noPhoto_128.png';
				elements.pathToPhotoLow		 = 'http://images.consotel.fr/noPhoto_128.png';
			
			return elements;
		}
		
		private function constuireXML(tabDetail:ArrayCollection):void
		{
			if(tabDetail!=null && tabDetail.length >0){
				XMLEquipement= new XMLListCollection();
				
				var str:String= tabDetail[0].GROUPE;
				var i:int=0;
				var j:int=0;
				var ok:Boolean=true;
				
				while(tabDetail[i].GROUPE == str && ok)
				{
					var XML1:XML= new XML(<groupe></groupe>)
					XML1.appendChild(<nom>{tabDetail[i].GROUPE}</nom>);
					
					var XML2:XMLList=new XMLList(<caracteristiques></caracteristiques>);
					
					while(tabDetail[j].GROUPE == str)
					{
						var XML3:XML= new XML(
							<caracteristique> 
								<groupe>{tabDetail[j].GROUPE}</groupe>
								<feature_id>{tabDetail[j].FEATURE_ID}</feature_id>
								<nom>{tabDetail[j].NOM}</nom>
								<valeur>{tabDetail[j].VALEUR}</valeur>
								<unite>{tabDetail[j].UNITE}</unite>
							</caracteristique>);
						XML2.appendChild(XML3);
						
						if(j<tabDetail.length-1){
							j++;	
						}else
							break;														
					}
					XML1.appendChild(XML2);
					
					XMLEquipement.addItem(XML1);
					
					if(j<tabDetail.length-1)
					{
						i=j;
						str=tabDetail[i].GROUPE;
					}
					else
					{
						ok=false;
					}
				}
				
			}
			
		}
		
		public function get dasPhone():String { return _dasPhone; }
		
		public function set dasPhone(value:String):void
		{
			if (_dasPhone == value)
				return;
			_dasPhone = value;
		}
		
		public function get cameraPhone():String { return _cameraPhone; }
		
		public function set cameraPhone(value:String):void
		{
			if (_cameraPhone == value)
				return;
			_cameraPhone = value;
		}
		
		public function get screenSizePhone():String { return _screenSizePhone; }
		
		public function set screenSizePhone(value:String):void
		{
			if (_screenSizePhone == value)
				return;
			_screenSizePhone = value;
		}
		
		public function get pathToPhoto():String { return _pathToPhoto; }
		
		public function set pathToPhoto(value:String):void
		{
			if (_pathToPhoto == value)
				return;
			_pathToPhoto = value;
		}
		
		
		public function get osPhone():String { return _osPhone; }
		
		public function set osPhone(value:String):void
		{
			if (_osPhone == value)
				return;
			_osPhone = value;
		}
		
		public function get networkPhone():String { return _networkPhone; }
		
		public function set networkPhone(value:String):void
		{
			if (_networkPhone == value)
				return;
			_networkPhone = value;
		}

		public function get tabListeDetailEquipement():ArrayCollection
		{
			return _tabListeDetailEquipement;
		}

		public function set tabListeDetailEquipement(value:ArrayCollection):void
		{
			_tabListeDetailEquipement = value;
		}

		public function get tabListeTarif():ArrayCollection
		{
			return _tabListeTarif;
		}

		public function set tabListeTarif(value:ArrayCollection):void
		{
			_tabListeTarif = value;
		}

		public function get tabListeTarif2():ArrayCollection
		{
			return _tabListeTarif2;
		}

		public function set tabListeTarif2(value:ArrayCollection):void
		{
			_tabListeTarif2 = value;
		}
		
		public function get tabPhoto():ArrayCollection { return _tabPhoto; }
		
		public function set tabPhoto(value:ArrayCollection):void
		{
			if (_tabPhoto == value)
				return;
			_tabPhoto = value;
		}
		
		public function get PHOTO_LOW():String { return _PHOTO_LOW; }
		
		public function set PHOTO_LOW(value:String):void
		{
			if (_PHOTO_LOW == value)
				return;
			_PHOTO_LOW = value;
		}
		
		public function get pathToPhotoLow():String { return _pathToPhotoLow; }
		
		public function set pathToPhotoLow(value:String):void
		{
			if (_pathToPhotoLow == value)
				return;
			_pathToPhotoLow = value;
		}
		
		public function get SYMBOL_DEVICE():String { return _SYMBOL_DEVICE; }
		
		public function set SYMBOL_DEVICE(value:String):void
		{
			if (_SYMBOL_DEVICE == value)
				return;
			_SYMBOL_DEVICE = value;
		}
		
	}
}