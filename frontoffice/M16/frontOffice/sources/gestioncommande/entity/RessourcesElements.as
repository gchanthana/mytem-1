package gestioncommande.entity
{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import gestioncommande.services.Formator;
	
	import session.SessionUserObject;

	public class RessourcesElements
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		public var LIBELLE					:String = "";
		public var REFERENCE_PRODUIT		:String = "";
	
		public var LIBELLETHEME	:String = "";
		public var TYPETHEME	:String = "";
		
		public var ACTION		:String = "A";
		public var LIB_ACTION	:String = ResourceManager.getInstance().getString('M16', 'Ajout');
		
		public var SOUSTETE		:String = "";
		
		public var PRIX_STRG	:String = ResourceManager.getInstance().getString('M16', 'n_c');
		public var PRIX_UNIT	:Number = 0;
		
		public var PRICE		:String = "0";
		
		public var PRIX_UNIT_1		:Number = 0;
		public var PRIX_UNIT_2		:Number = 0;
		public var PRIX_UNIT_3		:Number = 0;
		public var PRIX_UNIT_4		:Number = 0;
		public var PRIX_UNIT_5		:Number = 0;
		public var PRIX_UNIT_6		:Number = 0;
		public var PRIX_UNIT_7		:Number = 0;
		public var PRIX_UNIT_8		:Number = 0;
		public var PRIX_UNIT_9		:Number = 0;
		
		public var PRICE_TYPE	:Number = 0; //MYT-1287 Sonar selection du type de prix en fonction de la durée d'engagement;
		
		public var PRIX			:String = ResourceManager.getInstance().getString('M16', 'n_c');
		
					 
		public var IDPRODUIT	:int = 0;
		public var IDCATALOGUE	:int = 0;
		public var IDINVENTAIRE	:int = 0; 

		public var ISELIGIBLE	:int = 1;
		
		public var IDSOUSTETE	:int = 0;
		
		public var BOOLACCES	:int = 0;
		public var boolInOrder	:int = 0;
		public var FAVORI		:int = 0;
		public var NB_LIGNE		:int = 0;
		
		public var SELECTED		:Boolean = false;
		public var OBLIGATORY	:Boolean = false;
		public var RESTRICTED	:Boolean = false;
		public var PRIXVISIBLE	:Boolean = true;
		public var LIGNES		:ArrayCollection = new ArrayCollection();
		
		public var OPERATEURID	:Number = 0;
		public var OPERATEUR	:String = '';
				
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//		
		
		public function RessourcesElements()
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODE PUBLIC - COPY OBJECT
	//--------------------------------------------------------------------------------------------//
		
		public function copyRessources():RessourcesElements
		{
			var copyRess:RessourcesElements = new RessourcesElements();
				copyRess.LIBELLE			= this.LIBELLE;
				copyRess.LIBELLETHEME		= this.LIBELLETHEME;
				copyRess.TYPETHEME			= this.TYPETHEME;
				copyRess.ACTION				= this.ACTION;
				copyRess.LIB_ACTION			= this.LIB_ACTION;
				copyRess.SOUSTETE			= this.SOUSTETE;
				copyRess.PRIX				= this.PRIX;
				copyRess.IDPRODUIT			= this.IDPRODUIT;
				copyRess.IDCATALOGUE		= this.IDCATALOGUE;
				copyRess.IDINVENTAIRE		= this.IDINVENTAIRE;
				copyRess.IDSOUSTETE			= this.IDSOUSTETE;
				copyRess.BOOLACCES			= this.BOOLACCES;
				copyRess.boolInOrder		= this.boolInOrder;
				copyRess.FAVORI				= this.FAVORI;
				copyRess.NB_LIGNE			= this.NB_LIGNE;
				copyRess.SELECTED			= this.SELECTED;
				copyRess.OBLIGATORY			= this.OBLIGATORY;
				copyRess.PRIXVISIBLE		= this.PRIXVISIBLE;
				copyRess.PRIX_STRG			= this.PRIX_STRG;
				
				copyRess.PRIX_UNIT			= this.PRIX_UNIT;
				copyRess.PRIX_UNIT_1		= this.PRIX_UNIT_1;
				copyRess.PRIX_UNIT_2		= this.PRIX_UNIT_2;
				copyRess.PRIX_UNIT_3		= this.PRIX_UNIT_3;
				copyRess.PRIX_UNIT_4		= this.PRIX_UNIT_4;
				copyRess.PRIX_UNIT_5		= this.PRIX_UNIT_5;
				copyRess.PRIX_UNIT_6		= this.PRIX_UNIT_6;
				copyRess.PRIX_UNIT_7		= this.PRIX_UNIT_7;
				copyRess.PRIX_UNIT_8		= this.PRIX_UNIT_8;
				copyRess.PRIX_UNIT_9		= this.PRIX_UNIT_9;
				copyRess.REFERENCE_PRODUIT	= this.REFERENCE_PRODUIT;
				
				copyRess.LIGNES				= copyArraycollection(this.LIGNES);
			
			return copyRess;         				 		
		}
		
		private function copyArraycollection(values:ArrayCollection):ArrayCollection
		{
			var myCopy	:ArrayCollection = new ArrayCollection();
			var lenOri	:int = values.length;
			
			for(var i:int = 0;i < lenOri;i++)
			{
				myCopy.addItem(values[i]);
			}
			
			return myCopy;
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODE PUBLIC STATIC FORMATEUR
	//--------------------------------------------------------------------------------------------//
		
		public static function formatAbonnement(value:Object):RessourcesElements
		{
			var elements:RessourcesElements = new RessourcesElements();			
				elements.LIBELLE 			= value.LIBELLE_PRODUIT;
				elements.LIBELLETHEME 		= value.THEME_LIBELLE;
				elements.TYPETHEME 			= value.TYPE_THEME;
				elements.IDPRODUIT 			= value.IDTHEME_PRODUIT;
				elements.IDCATALOGUE 		= value.IDPRODUIT_CATALOGUE;
				elements.FAVORI 			= value.FAVORI;
				elements.REFERENCE_PRODUIT 	= value.REFERENCE_PRODUIT;	

				elements.BOOLACCES			= 1;
				elements.PRIXVISIBLE		= Formator.formatInteger(value.SHOW_PRIX);
			
			if(elements.PRIXVISIBLE)
			{
				//MYT-1287
				elements.PRIX_STRG = Formator.formatTotalWithSymbole(Number(value.PRIX_UNIT));
				elements.PRIX_UNIT = Number(value.PRIX_UNIT);
			}
			else
			{
				//MYT-1287
				elements.PRIX_STRG = ResourceManager.getInstance().getString('M16', 'n_c');
				elements.PRIX_UNIT = 0;
			}
				
			return elements;
		}
		
		public static function formatAbonnementObligatoire(value:Object):RessourcesElements
		{
			var elements:RessourcesElements = new RessourcesElements();			
				elements.LIBELLE 			= value.LIBELLE_PRODUIT;
				elements.LIBELLETHEME 		= value.THEME_LIBELLE;
				elements.TYPETHEME 			= value.TYPE_THEME;
				elements.IDPRODUIT 			= value.IDTHEME_PRODUIT;
				elements.IDCATALOGUE 		= value.IDPRODUIT_CATALOGUE;
				elements.FAVORI 			= value.FAVORI;
				elements.BOOLACCES			= 1;
				elements.OBLIGATORY			= formatObligatoire(value);
				elements.RESTRICTED			= true;
				elements.PRIXVISIBLE		= Formator.formatInteger(value.SHOW_PRIX);
				if(value.hasOwnProperty("REFERENCE_PRODUIT"))
				{
					elements.REFERENCE_PRODUIT 	= value.REFERENCE_PRODUIT;
				}
				else if(value.hasOwnProperty("REF_PRODUIT"))
				{
					elements.REFERENCE_PRODUIT 	= value.REF_PRODUIT;					
				}
				
			if(elements.OBLIGATORY == 1)
				elements.SELECTED = true;

			if(elements.PRIXVISIBLE)
			{
				//MYT-1287
				elements.PRIX_STRG = Formator.formatTotalWithSymbole(Number(value.PRIX_UNIT));
				elements.PRIX_UNIT = Number(value.PRIX_UNIT);
			}
			else
			{
				elements.PRIX_STRG = ResourceManager.getInstance().getString('M16', 'n_c');
				elements.PRIX_UNIT = 0;
			}
			
			return elements;
		}
		
		public static function formatOptions(value:Object):RessourcesElements
		{
			var elements:RessourcesElements = new RessourcesElements();			
				elements.LIBELLE 			= value.LIBELLE_PRODUIT;
				elements.LIBELLETHEME 		= value.THEME_LIBELLE;
				elements.TYPETHEME 			= value.TYPE_ABO;
				elements.IDPRODUIT 			= value.IDTHEME_PRODUIT;
				elements.IDCATALOGUE 		= value.IDPRODUIT_CATALOGUE;
				elements.FAVORI 			= value.FAVORI;
				elements.BOOLACCES			= 0;
				elements.PRIXVISIBLE		= Formator.formatInteger(value.SHOW_PRIX);
				elements.REFERENCE_PRODUIT 	= value.REFERENCE_PRODUIT;
				elements.OPERATEURID 		= value.OPERATEURID;
				elements.OPERATEUR			= value.OPERATEUR;
			
			if(elements.PRIXVISIBLE)
			{
				elements.PRIX_STRG = Formator.formatTotalWithSymbole(Number(value.PRIX_UNIT));
				elements.PRIX_UNIT = Number(value.PRIX_UNIT);
			}
			else
			{
				elements.PRIX_STRG = ResourceManager.getInstance().getString('M16', 'n_c');
				elements.PRIX_UNIT = 0;
			}
				
			return elements;
		}
		
		public static function formatOptionsObligatoire(value:Object):RessourcesElements
		{
			var elements:RessourcesElements = new RessourcesElements();			
				elements.LIBELLE 			= value.LIBELLE_PRODUIT;
				elements.LIBELLETHEME 		= value.THEME_LIBELLE;
				elements.TYPETHEME 			= value.TYPE_ABO;
				elements.IDPRODUIT 			= value.IDTHEME_PRODUIT;
				elements.IDCATALOGUE 		= value.IDPRODUIT_CATALOGUE;
				elements.FAVORI 			= value.FAVORI;
				elements.BOOLACCES			= 0;
				elements.OBLIGATORY			= formatObligatoire(value);
				elements.PRIXVISIBLE		= Formator.formatInteger(value.SHOW_PRIX);
				elements.OPERATEURID 		= value.OPERATEURID;
				elements.OPERATEUR			= value.OPERATEUR;
				if(value.hasOwnProperty("REFERENCE_PRODUIT"))
				{
					elements.REFERENCE_PRODUIT 	= value.REFERENCE_PRODUIT;
				}
				else if(value.hasOwnProperty("REF_PRODUIT"))
				{
					elements.REFERENCE_PRODUIT 	= value.REF_PRODUIT;					
				}
			
			if(elements.OBLIGATORY == 1)
				elements.SELECTED = true;
			
			if(elements.PRIXVISIBLE)
			{
				elements.PRIX_STRG = Formator.formatTotalWithSymbole(Number(value.PRIX_UNIT));
				elements.PRIX_UNIT = Number(value.PRIX_UNIT);
			}
			else
			{
				elements.PRIX_STRG = ResourceManager.getInstance().getString('M16', 'n_c');
				elements.PRIX_UNIT = 0;
			}
			
			return elements;
		}
		
		public static function formatOptionsLignes(value:Object):RessourcesElements
		{
			var elements:RessourcesElements = new RessourcesElements();			
				elements.LIBELLE 			= value.LIBELLE_PRODUIT;
				elements.LIBELLETHEME 		= 'Options';
				elements.TYPETHEME 			= 'Options';
				elements.IDPRODUIT 			= value.IDTHEME_PRODUIT;
				elements.IDCATALOGUE 		= value.IDPRODUIT_CATALOGUE;
				elements.BOOLACCES			= 0;
				elements.boolInOrder		= value.BOOL_IN_ORDER;
				elements.ACTION				= "R";
				elements.LIB_ACTION			= ResourceManager.getInstance().getString('M16', '_Supprimer_');
				elements.IDSOUSTETE			= value.IDSOUS_TETE;
				elements.SOUSTETE			= value.SOUS_TETE;
				elements.IDINVENTAIRE		= value.IDINVENTAIRE_PRODUIT;
				elements.PRIXVISIBLE		= Formator.formatInteger(value.SHOW_PRIX);
				elements.REFERENCE_PRODUIT 	= value.REFERENCE_PRODUIT;	

			
			if(elements.PRIXVISIBLE)
			{
				elements.PRIX_STRG = Formator.formatTotalWithSymbole(Number(value.PRIX_UNIT));
				elements.PRIX_UNIT = Number(value.PRIX_UNIT);
			}
			else
			{
				elements.PRIX_STRG = ResourceManager.getInstance().getString('M16', 'n_c');
				elements.PRIX_UNIT = 0;
			}
				
			switch(SessionUserObject.singletonSession.IDTYPEDECOMMANDE)//---> R:RESILIER, A:AJOUTER, S:SUSPENDRE, V:REACTIVATION 
			{
				case 6  : elements.ACTION = "R";break;
				case 7  : elements.ACTION = "S";break;
				case 8  : elements.ACTION = "V";break;
				case 9  : elements.ACTION = "R";break;
				default : elements.ACTION = "R";break;
			}
			
			return elements;
		}
		
		public static function formatOptionsInclude(value:Object, listeLignes:ArrayCollection):RessourcesElements
		{
			var elements:RessourcesElements = new RessourcesElements();			
				elements.LIBELLE 			= value.LIBELLE_PRODUIT;
				elements.LIBELLETHEME 		= 'Options';
				elements.TYPETHEME 			= 'Options';
				elements.IDPRODUIT 			= value.IDTHEME_PRODUIT;
				elements.IDCATALOGUE 		= value.IDPRODUIT_CATALOGUE;
				elements.BOOLACCES			= 0;
				elements.ACTION				= "R";
				elements.LIB_ACTION			= ResourceManager.getInstance().getString('M16', '_Supprimer_');
				elements.NB_LIGNE			= value.NB;
				elements.LIGNES				= searchLineWithThisOptions(elements.IDCATALOGUE, listeLignes);
				elements.PRIXVISIBLE		= Formator.formatInteger(value.SHOW_PRIX);
				elements.REFERENCE_PRODUIT 	= value.REFERENCE_PRODUIT;	
				elements.OPERATEURID 		= value.OPERATEURID;
				elements.OPERATEUR			= value.OPERATEUR;
				
			if(elements.PRIXVISIBLE)
			{
				elements.PRIX_STRG = Formator.formatTotalWithSymbole(Number(value.PRIX_UNIT));
				elements.PRIX_UNIT = Number(value.PRIX_UNIT);
			}
			else
			{
				elements.PRIX_STRG = ResourceManager.getInstance().getString('M16', 'n_c');
				elements.PRIX_UNIT = 0;
			}
				
			switch(SessionUserObject.singletonSession.IDTYPEDECOMMANDE)//---> R:RESILIER, A:AJOUTER, S:SUSPENDRE, V:REACTIVATION 
			{
				case 6  : elements.ACTION = "R";break;
				case 7  : elements.ACTION = "S";break;
				case 8  : elements.ACTION = "V";break;
				case 9  : elements.ACTION = "R";break;
				default : elements.ACTION = "R";break;
			}
				
			return elements;
		}
		
		public static function searchLineWithThisOptions(idProduit:int, listeLignes:ArrayCollection):ArrayCollection
		{
			var data :ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0; i < listeLignes.length;i++)
			{
				if(listeLignes[i].IDCATALOGUE == idProduit)
					data.addItem(listeLignes[i]);
			}
			
			return data;
		}

		public static function formatObligatoire(value:Object):Boolean
		{
			var bool:Boolean = false;
			
			if(value.OBLIGATOIRE == 1)
				bool = true;
			
			return bool;
		}
		
	}
}