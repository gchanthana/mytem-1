package gestioncommande.entity
{
	[Bindable]
	public class Portabilite
	{
		//numéro porté FR saisi
		public var NUMERO 						:String = '';
		
		//numéro généré auto
		public var NUMERO_HIDDEN				:String = '';
		public var CODERIO 						:String = '';
		public var IDSOUSTETE  					:int = 0;
		public var DATEPORTABILITE 				:Date = null;
		
		private var _NUMERO_PORTER				:String = '';
		// infos operateur précédent 
		private var _OPERATEUR_PRECEDENT		:String = '';
		private var _IDOPERATEUR_PRECEDENT		:Number = 0;
		private var _NUM_COMPTE_FACT_PRECEDENT	:String = '';
		private var _CODE_PIN_PWD				:String = '';
		private var _ADR_FACT_PRECEDENT			:String = '';
		// infos operateur cible
		private var _NUM_COMPTE_FACT_NV			:String = '';
		private var _ADR_FACT_NV				:String = '';
		
		public function Portabilite()
		{
		}
		
		public function get ADR_FACT_NV():String { return _ADR_FACT_NV; }
		
		public function set ADR_FACT_NV(value:String):void
		{
			if (_ADR_FACT_NV == value)
				return;
			_ADR_FACT_NV = value;
		}
		
		
		public function get NUM_COMPTE_FACT_NV():String { return _NUM_COMPTE_FACT_NV; }
		
		public function set NUM_COMPTE_FACT_NV(value:String):void
		{
			if (_NUM_COMPTE_FACT_NV == value)
				return;
			_NUM_COMPTE_FACT_NV = value;
		}
		
		public function get ADR_FACT_PRECEDENT():String { return _ADR_FACT_PRECEDENT; }
		
		public function set ADR_FACT_PRECEDENT(value:String):void
		{
			if (_ADR_FACT_PRECEDENT == value)
				return;
			_ADR_FACT_PRECEDENT = value;
		}
		
		public function get CODE_PIN_PWD():String { return _CODE_PIN_PWD; }
		
		public function set CODE_PIN_PWD(value:String):void
		{
			if (_CODE_PIN_PWD == value)
				return;
			_CODE_PIN_PWD = value;
		}
		
		public function get NUM_COMPTE_FACT_PRECEDENT():String { return _NUM_COMPTE_FACT_PRECEDENT; }
		
		public function set NUM_COMPTE_FACT_PRECEDENT(value:String):void
		{
			if (_NUM_COMPTE_FACT_PRECEDENT == value)
				return;
			_NUM_COMPTE_FACT_PRECEDENT = value;
		}
		
		public function get OPERATEUR_PRECEDENT():String { return _OPERATEUR_PRECEDENT; }
		
		public function set OPERATEUR_PRECEDENT(value:String):void
		{
			if (_OPERATEUR_PRECEDENT == value)
				return;
			_OPERATEUR_PRECEDENT = value;
		}
		
		public function get IDOPERATEUR_PRECEDENT():Number { return _IDOPERATEUR_PRECEDENT; }
		
		public function set IDOPERATEUR_PRECEDENT(value:Number):void
		{
			if (_IDOPERATEUR_PRECEDENT == value)
				return;
			_IDOPERATEUR_PRECEDENT = value;
		}
		
		public function get NUMERO_PORTER():String { return _NUMERO_PORTER; }
		
		public function set NUMERO_PORTER(value:String):void
		{
			if (_NUMERO_PORTER == value)
				return;
			_NUMERO_PORTER = value;
		}
		
	}
}