package gestioncommande.entity
{
	import mx.resources.ResourceManager;

	[Bindable]
	public class Critere
	{
		
		public var IDCRITERE	:int		= 0;
		
//		public var CRITERE		:String		= 'Aucun';
		public var CRITERE		:String		= ResourceManager.getInstance().getString('M16','Aucun_');
		public var TXTFILTER	:String		= '';
	
		
		public function Critere()
		{
		}
		
		public function copyCritere():Critere
		{
			var copyCritere:Critere 	= new Critere();
				copyCritere.IDCRITERE	= this.IDCRITERE;
				copyCritere.CRITERE		= this.CRITERE;
				copyCritere.TXTFILTER	= this.TXTFILTER;
			
			return copyCritere;
		}
	}
}