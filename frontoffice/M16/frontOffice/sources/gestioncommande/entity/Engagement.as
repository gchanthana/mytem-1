package gestioncommande.entity
{
	[Bindable]
	public class Engagement
	{
		public var DUREE		:String = "";
		public var VALUE		:String = "";
		public var CODE			:String = "";
		public var FPC			:String = "";
		
		public var ELEGIBILITE	:int = 18;
		
		public function fill(obj:Object):void
		{
			if(obj.hasOwnProperty('DUREE'))
				this.DUREE = obj.DUREE;
					
			if(obj.hasOwnProperty('VALUE'))
				this.VALUE = obj.VALUE;
					
			if(obj.hasOwnProperty('CODE'))
				this.CODE = obj.CODE;
					
			if(obj.hasOwnProperty('FPC'))
				this.FPC = obj.FPC;
		}
		
		public function Engagement()
		{
		}

	}
}