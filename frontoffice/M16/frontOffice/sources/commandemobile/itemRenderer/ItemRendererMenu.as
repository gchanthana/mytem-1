package commandemobile.itemRenderer
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.engine.BreakOpportunity;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.Spacer;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.WorkflowService;
	
	import session.SessionUserObject;

	[Bindable]
	public class ItemRendererMenu extends HBox
	{
		private var lb_libelle				:Label;
		private var spacer					:Spacer;
		private var img						:Image;
		
		public var dataMenuCollabo 			:Array;

		private var customMenu				:CustomMenu2;
		private var pt						:Point;
		
		[Embed(source="/assets/images/icon_show_action.png")]
		private var iconClass				:Class;
		private var _gworkFlow				:WorkflowService = new WorkflowService();
		private var _addEvent				:Boolean;
		private var typeFiche				:String;
			
		//---> MODIFICATION MOBILE/FIXE, LIVRAISON PARTIELLE MOBILE/FIXE
		//---> 2166 ModifCommande mobile, 2229 ModifCommande fixe , 2316 liv part mobile, 2317 liv part fixe
		private var restrictedActions			:Array = [2166, 2229, 2316, 2317];
		
		//---> TYPE DE COMMANDE POSSEDANT DES ACTIONS RESTRICTIVES
		private var restrictedTypeCommande		:Array = [1383, 1483, 1283, 1282, 1589, 1588, 1585, 1586];
		
		[Embed(source='/assets/images/arrow_down_blue.png',mimeType='image/png')]
		private var iconClassRecepCmd		:Class;
		[Embed(source='/assets/images/arrow_down_green.png',mimeType='image/png')]
		private var iconClassRecepLiv		:Class;
		[Embed(source='/assets/images/gear_stop.png',mimeType='image/png')]
		private var iconClassAnnul			:Class;
		[Embed(source='/assets/images/Download_2.png',mimeType='image/png')]
		private var iconClassExped			:Class;
		[Embed(source='/assets/images/RAGO - Desactiver.png',mimeType='image/png')]
		private var iconClassDesactive		:Class;
		[Embed(source='/assets/images/RAGO - Activer.png',mimeType='image/png')]
		private var iconClassActive			:Class;
		[Embed(source='/assets/images/RAGO - Modifier.png',mimeType='image/png')]
		private var iconClassDetails		:Class;
		[Embed(source='/assets/images/RAGO - Supprimer.png',mimeType='image/png')]
		private var iconClassErase			:Class;
		[Embed(source='/assets/images/RAGO - Supprimer.png',mimeType='image/png')]
		private var iconClassValide			:Class;
		[Embed(source='/assets/images/RAGO - Supprimer.png',mimeType='image/png')]
		private var iconClassValTarif		:Class;
		[Embed(source='/assets/images/RAGO - Supprimer.png',mimeType='image/png')]
		private var iconClassSend			:Class;
		[Embed(source='/assets/images/modifier_commande.jpeg',mimeType='image/jpeg')]
		private var iconModifCommande		:Class;
		private static const COMMANDE_REFUSEE				 	:Number = 3266;
		private static const ARTICLES_ENCOURS_ENREGISTREMENT 	:Number = 3366;
		private static const COMMANDE_ANNULEE 					:Number = 3078;
		private static const TRAITEMENT_FOURNISSEUR_EN_COURS	:Number = 3072;
		private static const COMMANDE_CLOSE 					:Number = 3077;

		public function ItemRendererMenu()
		{
			super();
			
			this.percentWidth = 100;
			
			lb_libelle = new Label();
			lb_libelle.mouseChildren = false;
			spacer = new Spacer();
			spacer.percentWidth = 100;
			img = new Image();
			img.source = iconClass;
			img.width = 20;
			img.height = 20;
			img.addEventListener(MouseEvent.CLICK, clickIconActionHandler);
			this.addChild(lb_libelle);
			this.addChild(spacer);
			this.addChild(img);
		}
		
		private function actionsHandler(cmde:CommandeEvent):void
		{	
			
		}

		private function isContainsRestrictedActions():Boolean
		{
			var myCommande	:Commande = data as Commande;
			var isContain	:Boolean = false;
			
			switch(myCommande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	isContain = false;	break;//--->  NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		isContain = false;	break;//--->  NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		isContain = false;	break;//--->  NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		isContain = false;	break;//--->  NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: isContain = true;	break;//--->  MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	isContain = true;	break;//--->  MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			isContain = true;	break;//--->  RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	isContain = true;	break;//--->  RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		isContain = false;	break;//--->  RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	isContain = false;	break;//--->  RENVLT
				case TypesCommandesMobile.SUSPENSION: 			isContain = true;	break;//--->  SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		isContain = true;	break;//--->  SUSP
				case TypesCommandesMobile.REACTIVATION: 		isContain = true;	break;//--->  REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	isContain = true;	break;//--->  REACT
				case TypesCommandesMobile.SAV: 	isContain = false;	break;//--->  REACT
			}
			
			return isContain;
		}
		
		private function removeActionsRestricted(actions:ArrayCollection):void
		{
			var lenActions 	:int = actions.length;
			var lenRestrict :int = restrictedActions.length;
			
			for(var i:int = 0;i < lenActions;i++)
			{
				for(var j:int= 0;j < lenRestrict;j++)
				{
					if(actions[i].IDACTION == restrictedActions[j])
					{
						actions.removeItemAt(i);
						i--;
						lenActions = actions.length;
					}
				}
			}
		}
		
		private function attributObjectToArray(arrayCollection:ArrayCollection, isContain:Boolean):Array
		{
			var object	:Object;
			var menu	:ArrayCollection = new ArrayCollection();
			
			menu.addItem(attributDetails());
			
			for(var i:int = 0;i < arrayCollection.length;i++)
			{
				object = new Object();
				object.label				= arrayCollection[i].LIBELLE_ACTION ;
				object.IDACTION				= arrayCollection[i].IDACTION;
				object.code					= arrayCollection[i].CODE_ACTION;
				object.CODE_ACTION			= arrayCollection[i].CODE_ACTION;					
				object.COMMENTAIRE_ACTION	= arrayCollection[i].COMMENTAIRE_ACTION;
				object.DATE_ACTION			= arrayCollection[i].DATE_ACTION;
				object.DEST					= arrayCollection[i].DEST;
				object.EXP					= arrayCollection[i].EXP;
				object.IDETAT				= arrayCollection[i].IDETAT;
				object.LIBELLE_ACTION		= arrayCollection[i].LIBELLE_ACTION;
				object.LIBELLEETAT_ENGENDRE	= arrayCollection[i].LIBELLEETAT_ENGENDRE;
				object.MESSAGE				= arrayCollection[i].MESSAGE;
				
				menu.addItem(object);
			}
						
			if(data.IDLAST_ETAT >= TRAITEMENT_FOURNISSEUR_EN_COURS && data.IDLAST_ETAT <= COMMANDE_CLOSE && data.IDLAST_ETAT != COMMANDE_REFUSEE && data.IDLAST_ETAT != ARTICLES_ENCOURS_ENREGISTREMENT && !isContain && data.IDTYPE_COMMANDE != TypesCommandesMobile.SAV)
				menu.addItem(changeSaisieTechnique());
			
			//condition pour afficher dans le menu renseignement des ref 
			if(data.IDLAST_ETAT >= TRAITEMENT_FOURNISSEUR_EN_COURS && data.IDLAST_ETAT <= COMMANDE_CLOSE && data.IDLAST_ETAT != COMMANDE_REFUSEE && data.IDLAST_ETAT != ARTICLES_ENCOURS_ENREGISTREMENT && !isContain && _gworkFlow.isRefAut == 0 && data.IDTYPE_COMMANDE==TypesCommandesMobile.SAV)
				menu.addItem(changeSaisieTechnique());
				
			menu.addItem(viewPDF());

			if(data.IDLAST_ETAT != COMMANDE_REFUSEE && !isContain && data.IDLAST_ETAT != ARTICLES_ENCOURS_ENREGISTREMENT)
				menu.addItem(viewTracking());
			
			return menu.source;
		}
		
		private function attributDetails():Object
		{
			var object:Object	= new Object();
			object.label 	= ResourceManager.getInstance().getString('M16', 'D_tails_de_la_commande');
			object.code		= "VIEW_CMD";
			object.icon 	= attributIcon(object.code);
			
			return object;
		}
		
		private function changeSaisieTechnique():Object
		{
			var object:Object	= new Object();
				object.label 	= ResourceManager.getInstance().getString('M16', 'Renseignement_des_r_f_rences');
				object.code		= "VIEW_SAISETECHNIQUE";
				object.icon 	= attributIcon(object.code);
			
			return object;
		}	
		
		private function viewPDF():Object
		{
			var object:Object	= new Object();
				object.label 	= ResourceManager.getInstance().getString('M16', 'Afficher_la_commande_en_PDF');
				object.code		= "VIEW_CMD_PDF";
				object.icon 	= attributIcon(object.code);
			
			return object;
		}
		
		private function viewTracking():Object
		{
			var object:Object	= new Object();
				object.label 	= ResourceManager.getInstance().getString('M16', 'Suivre_l_exp_dition');
				object.code		= "VIEW_CMD_TRACKING";
				object.icon 	= attributIcon(object.code);
			
			return object;
		}
		
		private function attributIcon(actionCode:String):Class
		{
			var icone:Class;
			
			switch(actionCode)
			{
				case "EMA4"				:icone = iconClassRecepCmd;				break;
				case "LIVRE"			:icone = iconClassRecepLiv;				break;
				case "ANNUL"			:icone = iconClassAnnul;				break;
				case "EXPED"			:icone = iconClassExped;				break;
				case null				:icone = iconClassDesactive;			break;
				case "EMA7"				:icone = iconClassActive;				break;
				case "VIEW_CMD"			:icone = iconClassDetails;				break;
				case "ERASE_CMD"		:icone = iconClassErase;				break;
				case "EXPORT_CMD"		:icone = null;							break;				
				case "EMA2B"			:icone = null/*iconClassValide*/;		break;
				case "EMA1"				:icone = null/*iconClassValTarif*/;		break;
				case "EMA3B"			:icone = null/*iconClassSend*/;			break;
				case "EMA3T"			:icone = null/*iconClassValSend*/;		break;
				case "VIEW_MODIFCMDE"	:icone = iconModifCommande;				break;
				case "VIEW_CMD_PDF"		:icone = null/*iconClassValSend*/;		break;
				case "VIEW_CMD_TRACKING":icone = null/*iconClassValSend*/;		break;
				default :icone 				   = null/*iconClassValSend*/;		break;
			}

			return icone;
		}

		//////////////////////////////////////////////////////////
		//
		//					METHODES PUBLIQUES
		//
		//////////////////////////////////////////////////////////
		
		public function set addEvent(value:Boolean):void
		{
			if(value == true)
			{
				this.addEventListener(MouseEvent.ROLL_OVER, rollHBoxEvent);
				this.addEventListener(MouseEvent.ROLL_OUT, rollHBoxEvent);
				img.visible = false;
				_addEvent = true;
			}
			else
			{
				img.visible = true;
				_addEvent = false;
			}
		}
		
		public function set textLabel(value:String):void
		{
			lb_libelle.text = value;
		}
		
		public function get textLabel():String
		{
			return lb_libelle.text;
		}
		
		[Inspectable(defaultValue="false",enumeration="true,false",type="Boolean")]
		public function set cursorLabel(value:Boolean):void
		{
			lb_libelle.buttonMode = value;
			lb_libelle.useHandCursor = value;
		}
		
		public function set textDecoration(value:String):void
		{
			lb_libelle.setStyle("textDecoration", value);
		}
		
		[Inspectable(defaultValue="collaborateur",enumeration="collaborateur,ligne,equipement,sim,emei",type="String")]
		public function set clickLabel(fiche:String):void
		{
			typeFiche = fiche;
			lb_libelle.addEventListener(MouseEvent.CLICK, clickLabelHandler);
		}
		
		[Inspectable(defaultValue="true",enumeration="true,false",type="Boolean")]
		public function set cursorImage(value:Boolean):void
		{
			img.buttonMode = value;
			img.useHandCursor = value;
		}

		public function set listDataMenu(list:Array):void
		{
			customMenu = CustomMenu2.createCustomMenu(this, list, false);
		}

		
		//////////////////////////////////////////////////////////
		//
		//					METHODES PRIVEES
		//
		//////////////////////////////////////////////////////////
		
		private function rollHBoxEvent(evt:MouseEvent):void
		{
			var type:String = evt.type;
			
			if(type == MouseEvent.ROLL_OVER)
			{
				
				if(textLabel == "")
				{
					this.visible = false;
					this.removeEventListener(MouseEvent.ROLL_OVER, rollHBoxEvent);
					this.removeEventListener(MouseEvent.ROLL_OUT, rollHBoxEvent);
				}
				else 
					img.visible = true;
			}
			else if(type == MouseEvent.ROLL_OUT) 
				{
					img.visible = false;
				}
		}

		private function clickLabelHandler(evt:MouseEvent):void
		{
			this.dispatchEvent(new PanelAffectationEvent(PanelAffectationEvent.DISPLAY_PANEL, true, "VIEW_CMD"));
		}
		
		private function clickIconActionHandler(evt:MouseEvent):void {
	
			pt = new Point(mouseX, mouseY);
			pt = contentToGlobal(pt);
			
			if(_addEvent == true) 
			{
				customMenu.addEventListener(MenuRollEvent.MENU_ROLL_OVER, menuRollEvent);
				customMenu.addEventListener(MenuRollEvent.MENU_ROLL_OUT, menuRollEvent);
				
				if(data.IDLAST_ETAT != COMMANDE_ANNULEE && data.IDLAST_ETAT != ARTICLES_ENCOURS_ENREGISTREMENT && data.IDTYPE_COMMANDE != TypesCommandesMobile.SAV)
					getActions();
				else if (data.IDLAST_ETAT != COMMANDE_ANNULEE && data.IDLAST_ETAT != ARTICLES_ENCOURS_ENREGISTREMENT && data.IDTYPE_COMMANDE==TypesCommandesMobile.SAV)
				{
					_gworkFlow.isRefActionAut(data.IDCOMMANDE);
					_gworkFlow.addEventListener("SHOW_REF_ITEM", getActionsForSAV);
				}
				else
					showMenu();
			}
		}
		
		private function showMenu():void
		{	
			var menu:ArrayCollection = new ArrayCollection();
				menu.addItem(attributDetails());
				
			if(data.IDLAST_ETAT != ARTICLES_ENCOURS_ENREGISTREMENT)
				menu.addItem(viewPDF());

			listDataMenu = menu.source;
			
			customMenu.showMenu(pt.x, pt.y + 5);
			customMenu.addEventListener(CustomMenuEvent.MENU_ITEM_CLICK, menuItemEvent);
		}
		
		private function getActionsForSAV(e:Event):void
		{
			getActions();		
		}
		
		private function getActions():void
		{
			// cas de tous les pools: idPool <= 0
			if(SessionUserObject.singletonSession.POOL.IDPOOL <= 0)
			{
				// récupération du profil sur le pool sur lequel la commande était faite
				var idProfilCmd:Number = getProfilUser((data as Commande).IDPOOL_GESTIONNAIRE);
				
				// la liste des actions possibles pour le profil trouvé
				var listeActionsProfil:ArrayCollection = filterActions(idProfilCmd);
				
				// Liste des actions possibles
				
				var listeActionsCmd:ArrayCollection = _gworkFlow.fournirActionsNoRemoteAllPools(data.IDLAST_ETAT, data as Commande, listeActionsProfil);
				
				var isRestrict:Boolean = isContainsRestrictedActions();
				if(isRestrict)
					removeActionsRestricted(listeActionsCmd);
				
				listDataMenu = attributObjectToArray(listeActionsCmd, isRestrict);
				
				customMenu.showMenu(pt.x, pt.y + 5);
				customMenu.addEventListener(CustomMenuEvent.MENU_ITEM_CLICK, menuItemEvent);
			}
			else
			{
				_gworkFlow.fournirActionsNoRemote(data.IDLAST_ETAT, data as Commande);
				
				var isContain:Boolean = isContainsRestrictedActions();
				
				if(isContain)
					removeActionsRestricted(_gworkFlow.listeActions);
				
				listDataMenu = attributObjectToArray(_gworkFlow.listeActions, isContain);
				
				customMenu.showMenu(pt.x, pt.y + 5);
				customMenu.addEventListener(CustomMenuEvent.MENU_ITEM_CLICK, menuItemEvent);
			}
			
		}
		
		private function getProfilUser(idPool:Number):int
		{
			var myIdProfil:Number = 0;
			var lenPools:int = SessionUserObject.singletonSession.USERPOOLS.length;
			for (var i:int = 0; i < lenPools; i++) 
			{
				if(idPool == SessionUserObject.singletonSession.USERPOOLS[i].IDPOOL)
				{
					myIdProfil = SessionUserObject.singletonSession.USERPOOLS[i].IDPROFIL;
					break;
				}
			}
			
			return myIdProfil;
		}
		
		private function filterActions(idProfil:Number):ArrayCollection
		{
			var filtredActions:ArrayCollection = new ArrayCollection();
			var lenAct:int = SessionUserObject.singletonSession.LISTEACTIONSUSER.length;
			for (var i:int = 0; i < lenAct; i++) 
			{
				if(SessionUserObject.singletonSession.LISTEACTIONSUSER[i].IDPROFIL_COMMANDE == idProfil)
					filtredActions.addItem(SessionUserObject.singletonSession.LISTEACTIONSUSER[i]);
			}
			return filtredActions;
		}
		
		private function menuRollEvent(evt:MenuRollEvent):void
		{
			var type:String = evt.type;
			
			if(type == MenuRollEvent.MENU_ROLL_OVER)
			{
				img.visible = true;
			}
			else if(type == MenuRollEvent.MENU_ROLL_OUT) 
				{
					img.visible = false;
				}
		}
			
		private function menuItemEvent(evt:CustomMenuEvent):void
		{
			var type:String = evt.type;
			
			if(type == CustomMenuEvent.MENU_ITEM_CLICK)
				this.dispatchEvent(new PanelAffectationEvent(PanelAffectationEvent.DISPLAY_PANEL, true, evt.codeItemMenu, evt.actionItemMenu));
			
			if(img.visible == false) 
				img.visible = true;
		}
	}
}
