package commandemobile.itemRenderer.detailscommande
{
	import gestioncommande.entity.TypesCommandesMobile;
	
	import flash.events.Event;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.CommandeService;
	
	import mx.containers.Box;
	import mx.containers.ViewStack;
	import mx.events.FlexEvent;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class SelectViewDetailsImpl extends Box
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var vs							:ViewStack;
		
		public var etapeDetailsCommande			:DetailsItemREndererIHM;
		public var etapeModificationOptions		:ModificationItemRendererIHM;
		public var etapeResiliationLigne		:ResiliationItemRendererIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmd						:Commande;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmdSrv						:CommandeService = new CommandeService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//

		private var _myArticles					:XML = new XML();

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
	
		public function SelectViewDetailsImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function setProcedure(selectedCommande:Commande):void
		{
			_cmd = selectedCommande;
			
			getArticlesCommande();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
		}
		
		private function showHandler(fe:FlexEvent):void
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function articlesCommandeHandler(cmde:CommandeEvent):void
		{
			_myArticles = _cmdSrv.listeArticles;
			
			setViewstack();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getArticlesCommande():void
		{
			_cmdSrv.fournirArticlesCommande(_cmd.IDCOMMANDE);
			_cmdSrv.addEventListener(CommandeEvent.COMMANDE_ARRIVED, articlesCommandeHandler);
		}	
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTIONS
		//--------------------------------------------------------------------------------------------//

		private function setViewstack():void
		{
			vs.selectedIndex = giveMeIndexViewstack();
			
			(vs.getChildAt(vs.selectedIndex) as Object).setValueConfiguration(_myArticles.copy(), _cmd);
		}
		
		private function giveMeIndexViewstack():int
		{
			var index:int = 0;
			
			switch(_cmd.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	index = 0; break;//---> NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		index = 0; break;//---> NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		index = 0; break;//---> NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		index = 0; break;//---> NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: index = 1; break;//---> MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	index = 1; break;//---> MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			index = 2; break;//---> RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	index = 2; break;//---> RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		index = 0; break;//---> RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	index = 0; break;//---> RENVLT
				case TypesCommandesMobile.SUSPENSION: 			index = 2; break;//---> SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		index = 2; break;//---> SUSP
				case TypesCommandesMobile.REACTIVATION: 		index = 2; break;//---> REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	index = 2; break;//---> REACT
			}
			
			return index;
		}
		
		
	}
}
