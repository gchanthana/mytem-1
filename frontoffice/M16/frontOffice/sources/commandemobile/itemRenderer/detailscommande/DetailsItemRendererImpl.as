package commandemobile.itemRenderer.detailscommande
{
	import flash.events.Event;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.entity.RessourcesElements;
	import gestioncommande.services.Formator;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.resources.ResourceManager;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class DetailsItemRendererImpl extends Box
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
 		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listeArticles		:ArrayCollection = new ArrayCollection();
		
		private var _myArticles			:XML = new XML();

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//

		public function DetailsItemRendererImpl()
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function setValueConfiguration(articles:XML, myCommande:Commande):void
		{
			_myArticles = articles;
			formatToConfigurationSelected();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					FUCNTION - TRANSFORMATION ARTICLES TO ARRAYCOLLECTION POUR ETAPE 1 
		//--------------------------------------------------------------------------------------------//
		
		private function formatToConfigurationSelected():void
		{
			var listObject	:ArrayCollection = new ArrayCollection();
			var xmlarticles	:XMLList = _myArticles.children();
			var bool		:Boolean = false;
			var len			:int = xmlarticles.length();
			var id			:int = 0;
			var i			:int = 0;
			
			for(i = 0;i < len;i++)
			{
				var articles	:XML = xmlarticles[i];
				var myElements:ElementsCommande2 = new ElementsCommande2();
				
				getEquipements(articles.equipements[0], myElements);
				getRessources(articles.ressources[0], myElements);
				getEngagement(articles, myElements);
				getConfiguration(articles, myElements);					
					
				listObject.addItem(myElements);	
			}
			
			len = listObject.length;
			
			for(i = 0;i < len;i++)
			{
				getItems(listObject[i] as ElementsCommande2);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION - FORMAT XML
		//--------------------------------------------------------------------------------------------//
		
		private function getEquipements(articlesEq:XML, valuesElements:ElementsCommande2):void
		{
			var lenXML:int = articlesEq[0].equipement.length();
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML	:XML = articlesEq[0].equipement[i];
				var price	:String = myXML[0].prix.toString().replace(",",".");
				
				var eqElts:EquipementsElements 	= new EquipementsElements();
					eqElts.LIBELLE	 			= String(myXML[0].libelle);
					eqElts.EQUIPEMENT 			= String(myXML[0].type_equipement);
					eqElts.IDEQUIPEMENT 		= int(myXML[0].idtype_equipement);
					eqElts.IDFOURNISSEUR		= int(myXML[0].idequipementfournis);
					eqElts.IDCLIENT				= int(myXML[0].idequipementclient);
					eqElts.PRIX					= Number(price);
				
				if(eqElts.IDEQUIPEMENT == 2191)
					valuesElements.ACCESSOIRES.addItem(eqElts);//---> ACCESSOIRES
				else
					valuesElements.TERMINAUX.addItem(eqElts);//---> TERMINAUX / CARTE SIM
			}
		}
		
		private function getRessources(articlesRe:XML, valuesElements:ElementsCommande2):void
		{
			var lenXML:int = articlesRe[0].ressource.length();
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML:XML = articlesRe[0].ressource[i];
				
				var reElts:RessourcesElements	= new RessourcesElements();	
					reElts.LIBELLETHEME 		= String(myXML[0].theme);
					reElts.LIBELLE 				= String(myXML[0].libelle);
					reElts.PRIX 				= int(myXML[0].prix);
					reElts.IDCATALOGUE 			= int(myXML[0].idproduit_catalogue);
					reElts.IDPRODUIT 			= int(myXML[0].idThemeProduit);
					reElts.BOOLACCES 			= int(myXML[0].bool_acces);
				
				//---> 'BOOLACCES' = 0 -> OPTION, 'BOOLACCES' = 1 -> ABONNEMENT
				if(reElts.BOOLACCES > 0)
					valuesElements.ABONNEMENTS.addItem(reElts);
				else
					valuesElements.OPTIONS.addItem(reElts);
			}
		}
		
		private function getEngagement(engagement:XML, valuesElements:ElementsCommande2):void
		{
			var commitment	:Engagement = new Engagement();
			var duree		:int = int(engagement.engagement_res[0]);
			var idType		:int = int(engagement.idtype_ligne[0]);
			var eligibilite	:int = int(engagement.duree_elligibilite[0]);
			var fpc			:String = String(engagement.fpc[0]);
			var numCmd		:int = int(engagement.numero_config[0]);
			
			valuesElements.ID 	= numCmd;
			
			if(eligibilite > 0)
				commitment.ELEGIBILITE = eligibilite;
			
			if(fpc != "")
			{
				commitment.DUREE = ResourceManager.getInstance().getString('M16', '24_mois');
				commitment.VALUE = '24';
				commitment.CODE  = 'EVQMFCP';
				commitment.FPC   = String(engagement.fpc[0]);
			}
			else
			{
				switch(duree)
				{
					case 12: commitment.CODE = 'EDM';  commitment.DUREE = ResourceManager.getInstance().getString('M16', '12_mois'); commitment.VALUE = '12'; break;
					case 24: commitment.CODE = 'EVQM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '24_mois'); commitment.VALUE = '24'; break;
					case 36: commitment.CODE = 'ETSM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '36_mois'); commitment.VALUE = '36'; break;
					case 48: commitment.CODE = 'EQHM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '48_mois'); commitment.VALUE = '48'; break;
				}
			}
			
			if(idType > 0)
			{
				valuesElements.IDTYPELIGNE  = idType;
				valuesElements.TYPELIGNE 	= String(engagement.type_ligne[0]);
			}
			else
			{
				valuesElements.IDTYPELIGNE  = -1;
				valuesElements.TYPELIGNE 	= '';
			}
			
			valuesElements.ENGAGEMENT 	= commitment;	
		}
		
		private function getConfiguration(configuration:XML, valuesElements:ElementsCommande2):void
		{
			var conElts:ConfigurationElements 		= new ConfigurationElements();
				conElts.LIBELLE 					= String(configuration.code_interne[0]);
				conElts.PORTABILITE.NUMERO 			= String(configuration.soustete[0]);
				conElts.ID_COLLABORATEUR			= int(configuration.idemploye[0]);
				conElts.COLLABORATEUR 				= String(configuration.nomemploye[0]);
			
			valuesElements.CONFIGURATIONS.addItem(conElts);
		}
		
		private function getItems(elts:ElementsCommande2):void
		{
			var newElts:Object 			= getMyArticles(elts);
				newElts.COLLABORATEUR 	= ResourceManager.getInstance().getString('M16', 'Configuration');
				newElts.SOUSTETE		= ResourceManager.getInstance().getString('M16', 'Inconnu');
			
			if(elts.CONFIGURATIONS != null && elts.CONFIGURATIONS.length == 1)
			{
				var config:ConfigurationElements = elts.CONFIGURATIONS[0];
				
				newElts.SOUSTETE = config.PORTABILITE.NUMERO;
				
				if(config.ID_COLLABORATEUR > 0)
					newElts.COLLABORATEUR	= ResourceManager.getInstance().getString('M16', 'Collaborateur___') + config.COLLABORATEUR;
				else
					newElts.COLLABORATEUR	= ResourceManager.getInstance().getString('M16', 'Libell____') + config.LIBELLE;
				
				if(elts.ENGAGEMENT.VALUE != '')
					newElts.ENGAGEMENT	= elts.ENGAGEMENT.VALUE;
				else
					newElts.ENGAGEMENT	= ResourceManager.getInstance().getString('M16', 'Aucun');
				
				newElts.ROWCOUNT = (newElts.ITEMS as ArrayCollection).length;
			}

			listeArticles.addItem(newElts);
		}
		
		private function getMyArticles(elts:ElementsCommande2):Object
		{
			var myItems	:ArrayCollection = new ArrayCollection();
			var eltsObj	:Object = new Object();
			var obj		:Object = new Object();
			var price	:Number = 0;
			var i		:int = 0;
			var len		:int = 0;
			
			len = elts.TERMINAUX.length;
			
			for(i = 0;i < len;i++)
			{
				obj 		 = new Object();
				obj.TYPE	 = (elts.TERMINAUX[i] as EquipementsElements).EQUIPEMENT;
				obj.LIBELLE	 = (elts.TERMINAUX[i] as EquipementsElements).LIBELLE;
				obj.PRIX_RE	 = ''
				obj.PRIX_EQ	 = Formator.formatTotalWithSymbole((elts.TERMINAUX[i] as EquipementsElements).PRIX);
				
				price = price + (elts.TERMINAUX[i] as EquipementsElements).PRIX;
				
				myItems.addItem(obj);
			}
			
			len = elts.ACCESSOIRES.length;
			
			for(i = 0;i < len;i++)
			{
				obj 		 = new Object();
				obj.TYPE	 = (elts.ACCESSOIRES[i] as EquipementsElements).EQUIPEMENT;
				obj.LIBELLE	 = (elts.ACCESSOIRES[i] as EquipementsElements).LIBELLE;
				obj.PRIX_RE	 = ''
				obj.PRIX_EQ	 = Formator.formatTotalWithSymbole((elts.ACCESSOIRES[i] as EquipementsElements).PRIX);
				
				price = price + (elts.ACCESSOIRES[i] as EquipementsElements).PRIX;
				
				myItems.addItem(obj);
			}
			
			len = elts.ABONNEMENTS.length;
			
			for(i = 0;i < len;i++)
			{
				obj 		 = new Object();
				obj.TYPE	 = (elts.ABONNEMENTS[i] as RessourcesElements).LIBELLETHEME;
				obj.LIBELLE	 = (elts.ABONNEMENTS[i] as RessourcesElements).LIBELLE;
				obj.PRIX_RE	 = (elts.ABONNEMENTS[i] as RessourcesElements).PRIX
				obj.PRIX_EQ	 = '';
				
				myItems.addItem(obj);
			}
			
			len = elts.OPTIONS.length;
			
			for(i = 0;i < len;i++)
			{
				obj 		 = new Object();
				obj.TYPE	 = (elts.OPTIONS[i] as RessourcesElements).LIBELLETHEME;
				obj.LIBELLE	 = (elts.OPTIONS[i] as RessourcesElements).LIBELLE;
				obj.PRIX_RE	 = (elts.OPTIONS[i] as RessourcesElements).PRIX
				obj.PRIX_EQ	 = '';
				
				myItems.addItem(obj);
			}

			
			eltsObj.PRIX 	 = Formator.formatTotalWithSymbole(price);
			eltsObj.ITEMS 	 = myItems;
			eltsObj.TOTAL 	 = Formator.formatTotalWithSymbole(price);
			
			return eltsObj;
		}

	}
}