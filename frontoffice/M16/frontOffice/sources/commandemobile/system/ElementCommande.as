package commandemobile.system
{
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class ElementCommande extends EventDispatcher
	{		
		public static const CREATION_COMPLETE : String = "elementOk";
		public static const UPDATE_COMPLETE : String = "elementUpdated";
		public static const SAVE_COMPLETE : String = "elementSaved";
		public static const SAVEALL_COMPLETE : String = "allElementSaved";
		public static const LISTE_COMPLETE : String = "elementListOk";		
		public static const DELETE_COMPLETE : String = "elementDeleteOk";
		public static const RAPPROCHER_COMPLETE : String = "elementRappOk";		
		public static const ANNULER_RAPPROCHER_COMPLETE : String = "elementAnRappOk";
		public static const RAPPROCHER_PRODUITS_COMMANDE_COMPLETE : String = "elementsCommandeRappOk";
				
		public static const CREATION_ERROR : String = "elementCreationError";
		public static const UPDATE_ERROR : String ="elementUpdateError";  
		public static const SAVE_ERROR : String = "elementSavedError";
		public static const LISTE_ERROR : String = "elementListError";
		public static const DELETE_ERROR : String = "elementDeleteError";
		public static const RAPPROCHER_ERROR : String = "elementRappError";		
		public static const ANNULER_RAPPROCHER_ERROR : String = "elementAnRappError";
		public static const RAPPROCHER_PRODUITS_COMMANDE_ERROR  : String = "elementsCommandeRappError";
		
		private static const COLDFUSION_COMPONENT : String = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.ElementCommande";
  		private static const COLDFUSION_GET : String = "getProduit";
  		private static const COLDFUSION_UPDATE : String = "updateProduit";
  		private static const COLDFUSION_CREATE : String = "saveProduits";
		private static const COLDFUSION_GETLISTE : String = "getProduitsCommande";	
		
		private static const COLDFUSION_RAPPROCHER_PRODUIT : String = "rapprocherProduit";
		private static const COLDFUSION_RAPPROCHER_PRODUITS_COMMANDE : String = "validerRapprochementProduitsCommande";
		private static const COLDFUSION_ANNULER_RAPPROCHER_PRODUIT : String = "annulerRapprochementProduit";
		private static const COLDFUSION_ANNULER_TS_RAPPROCHER_PRODUIT : String = "annulerTousLesRapprochements";
		private static const COLDFUSION_DELETE : String = "deleteProduit";
		private static const COLDFUSION_DELETEALL : String = "deleteAllProduits";
		
		private const NOUVELLE_LIGNE : int = 1;
		private const NOUVEAU_PRODUIT : int = 2;
		private const MIXTE : int = 3;
		
		private var opGet : AbstractOperation;
		private var opCreate : AbstractOperation;
		private var opUpdate : AbstractOperation;		
		private var opList : AbstractOperation;
		private var opDelete : AbstractOperation;
		private var opRapp : AbstractOperation;
		
		public function ElementCommande(id : int = 0){
			super();
			if (id != 0){
				_elementID = id;
				getElementCommande(elementID)
			}else{
				_liste = new ArrayCollection();
			}
		}
		
		public function clean():void{			
			 
		}
		
		private var _elementID : int;
		public function set elementID(id : int):void{
			_elementID = id;
		}
		public function get elementID():int{
			return _elementID;
		}
				
		private var _commandeID : int;
		public function set commandeID(id : int):void{
			_commandeID = id;
		}
		public function get commandeID():int{
			return _commandeID;
		}
		
		
		private var _utilisateur : String;
		public function set utilisateur(nomprenom : String):void{
			_utilisateur = nomprenom;
		}
		public function get utilisateur():String{
			return _utilisateur;
		}
		
		private var _fonction : String;
		public function set fonction(fct : String):void{
			_fonction = fct;
		}
		public function get fonction():String{
			return _fonction;
		}
		
		private var _utilisateurID : int;
		public function set utilisateurID(id : int):void{
			_utilisateurID = id;
		}
		public function get utilisateurID():int{
			return _utilisateurID;
		}
		
		private var _quantite : int = 1;
		public function set quantite(qte: int):void{
			_quantite = qte;
		}
		public function get quantite():int{
			return _quantite;
		}
		
		private var _flagRapproche : int;
		public function set flagRapproche(flag : int):void{
			_flagRapproche = flag;
		}
		public function get flagRapproche():int{
			return _flagRapproche;
		}
		
		private var _flagAffectee : int;
		public function set flagAffectee (flag : int):void{
			_flagAffectee  = flag;
		}
		public function get flagAffectee():int{
			return _flagAffectee;
		}
		
		private var _libelleLigne : String;
		public function set libelleLigne(l : String):void{
			_libelleLigne = l;
		}
		public function get libelleLigne():String{
			return _libelleLigne;
		}
		
		private var _boolAccess : Number;
		public function set boolAccess(bool: Number):void{
			_boolAccess = bool;
		}
		public function get boolAccess():Number{
			return _boolAccess;
		}
		
		private var _themeID : int;
		public function set themeID(id : int):void{
			_themeID = id;
		}
		public function get themeID():int{
			return _themeID;
		}
		
		private var _produitCatID : int;
		public function set produitCatID (id : int):void{
			_produitCatID  = id;
		}
		public function get produitCatID ():int{
			return _produitCatID ;
		}
		
		private var _libelleProduit : String;
		public function set libelleProduit(l : String):void{
			_libelleProduit = l;
		}
		public function get libelleProduit():String{
			return _libelleProduit;
		}
		
		private var _libelletheme : String;
		public function set libelletheme(l : String):void{
			_libelletheme = l;
		}
		public function get libelletheme():String{
			return _libelletheme;
		}
		
		private var _ligneID : int;
		public function set ligneID(id : int):void{
			_ligneID = id;
		}
		public function get ligneID():int{
			return _ligneID;
		}
		
		private var _numLigne : String;
		public function set numLigne(l : String):void{
			_numLigne = l;
		}
		public function get numLigne():String{
			return _numLigne;
		}
		
		private var _commentaire : String = "----";
		public function set commentaire(l : String):void{
			_commentaire = l;
		}
		public function get commentaire():String{
			return _commentaire;
		}
		
		private var _ressourceID : int;
		public function set ressourceID(id : int):void{
			_ressourceID = id;
		}
		public function get ressourceID():int{
			return _ressourceID;
		}
		
		//---------------------------------------------------------//		
		private function fill(value : Object):void{
						
			elementID = Number(value.IDCDE_CDE_DETAIL);	
			commandeID = Number(value.IDCDE_COMMANDE); 		
			ressourceID = Number(value.IDINVENTAIRE_PRODUIT);
			produitCatID = Number(value.IDPRODUIT_CATALOGUE);
			ligneID = (value.IDSOUS_TETE)?Number(value.IDSOUS_TETE):0;
			themeID = Number(value.IDTHEME_PRODUIT);
				
			boolAccess = Number(value.BOOL_ACCES);	
			flagRapproche = Number(value.FLAG_RAPPROCHE);
			flagAffectee = Number(value.FLAG_AFFECTE);
						
			libelleProduit = value.LIBELLE_PRODUIT;		
			libelletheme = value.THEME_LIBELLE;
			libelleLigne = value.SOUS_TETE;
					
			commentaire = value.COMMENTAIRES;
			
			
			
			
		}
		
		///---------------- REMOTING ------------------------------/
		public function getElementCommande(id : int):void{
			opGet = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_GET,
													getElementCommandeResultHandler,null);
			RemoteObjectUtil.callService(opGet,id);
		}
		
		private function getElementCommandeResultHandler(re : ResultEvent):void{
			if (re.result){							
				fill(re.result[0]);				
				dispatchEvent(new Event(ElementCommande.CREATION_COMPLETE));				
			}else{
				dispatchEvent(new Event(ElementCommande.CREATION_ERROR));
			}			
		}
		
		//--------	
		public function deleteElementCommande(id : int):void{
			 
			opDelete = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_DELETE,
													deleteElementCommandeResultHandler,null);
			RemoteObjectUtil.callService(opDelete,id);
		}
		
		private function deleteElementCommandeResultHandler(re : ResultEvent):void{
			if (re.result < 0){							
				dispatchEvent(new Event(ElementCommande.DELETE_ERROR));				
			}else{
				dispatchEvent(new Event(ElementCommande.DELETE_COMPLETE));
				
			}			
		}
		
		public function deleteAllElementCommande(datas :ArrayCollection):void{
		 
			opDelete = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_DELETEALL,
													deleteElementCommandeResultHandler,null);
													
		 	RemoteObjectUtil.callService(opDelete,ConsoviewUtil.extractIDs("elementID",datas.source)); 
		}
				
		//--------					
		public function update():void{
			var fakeValue : int = 0;
			opUpdate = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_UPDATE,
													updateResultHandler,null);

			RemoteObjectUtil.callService(opUpdate,fakeValue,this);						
		}	
		
		private function updateResultHandler(re : ResultEvent):void{
			if (re.result < 0){
				dispatchEvent(new Event(ElementCommande.UPDATE_ERROR));
			}else{
				dispatchEvent(new Event(ElementCommande.UPDATE_COMPLETE));
				
			}
		}
		
		//----------
		public function sauvegarder():void{			
			opCreate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_CREATE,
													sauvegarderResultHandler,null);
						
				
			RemoteObjectUtil.callService(opCreate,commandeID, 
												[produitCatID],
												[libelleLigne],														
												[commentaire],	
												[flagAffectee]);					 												
		}		
		
		private function sauvegarderResultHandler(re : ResultEvent):void{
			if (re.result < 0){								
				dispatchEvent(new Event(ElementCommande.SAVE_ERROR));
			}else{
				dispatchEvent(new Event(ElementCommande.SAVE_COMPLETE));
				
			}			
		}
		
		public function sauvegarderAll():void{			
			opCreate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_CREATE,
													sauvegarderAllResultHandler,null);
													
													
			RemoteObjectUtil.callService(opCreate,commandeID, 	
													ConsoviewUtil.extraireColonne(liste,"produitCatID"),													
													ConsoviewUtil.extraireColonne(liste,"libelleLigne"),
													ConsoviewUtil.extraireColonne(liste,"commentaire"),													
													ConsoviewUtil.extraireColonne(liste,"flagAffectee"));					 												
			
		}		
		
		private function sauvegarderAllResultHandler(re : ResultEvent):void{
			if (re.result < 0){
				dispatchEvent(new Event(ElementCommande.SAVE_ERROR));
			}else{
				dispatchEvent(new Event(ElementCommande.SAVEALL_COMPLETE));
				
			}			
		}
		
		//---------RAPPROCHEMENT ------------------------------------------------------------------
		public function rapprocher(ressID : int):void{			
			opRapp = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_RAPPROCHER_PRODUIT,
													rapprocherResultHandler,null);
						
				
			RemoteObjectUtil.callService(opRapp,elementID,ressID);					 												
		}		
		
		
		private function rapprocherResultHandler(re : ResultEvent):void{
			if (re.result < 0){								
				dispatchEvent(new Event(ElementCommande.RAPPROCHER_ERROR));
			}else{
				dispatchEvent(new Event(ElementCommande.RAPPROCHER_COMPLETE));
			}			
		}
		
		
		public function validerRapprochementCommande():void{						
			opRapp = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_RAPPROCHER_PRODUITS_COMMANDE,
													validerRapprochementCommandeResultHandler,null);
																										
			RemoteObjectUtil.callService(opRapp,commandeID);
		}
		
		private function validerRapprochementCommandeResultHandler(re : ResultEvent):void{
			if (re.result < 0){								
				dispatchEvent(new Event(ElementCommande.RAPPROCHER_PRODUITS_COMMANDE_ERROR));
			}else{
				dispatchEvent(new Event(ElementCommande.RAPPROCHER_PRODUITS_COMMANDE_COMPLETE));
			}			
		}
		
		public function annulerRapprochement():void{			
			opRapp = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_ANNULER_RAPPROCHER_PRODUIT,
													annulerRapprochementResultHandler,null);
						
				
			RemoteObjectUtil.callService(opRapp,elementID);					 												
		}		
		
		private function annulerRapprochementResultHandler(re : ResultEvent):void{
			if (re.result < 0){
				dispatchEvent(new Event(ElementCommande.ANNULER_RAPPROCHER_ERROR));
			}else{
				prepareList(commandeID);
				dispatchEvent(new Event(ElementCommande.ANNULER_RAPPROCHER_COMPLETE));
				
			}			
		}
		
		
		public function annulerTousLesRapprochements(liste : Array,cmdeID : int):void{			
			opRapp = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_ANNULER_TS_RAPPROCHER_PRODUIT,
													getListResultHandler,null);
													
			RemoteObjectUtil.callService(opRapp, liste, cmdeID);					 												
		}
		
		private function annulerTousLesRapprochementResultHandler(re : ResultEvent):void{
			if(Number(re.result) && (re.result < 0))
			{
				dispatchEvent(new Event(ElementCommande.ANNULER_RAPPROCHER_ERROR));
			}else{					
				setliste(re.result as ArrayCollection);
				dispatchEvent(new Event(ElementCommande.LISTE_COMPLETE));
			}
		}
		
		//---------LISTE---------------------------------------------------------------------------
		private var _liste : ArrayCollection;
		private function setliste(values : ArrayCollection):void{
			
			var ligne : Object;
			_liste = new ArrayCollection();
			
			for (ligne in values){													
				var ec : ElementCommande = new ElementCommande();
								
				ec.elementID = Number(values[ligne].IDCDE_CDE_DETAIL);	
				ec.commandeID = Number(values[ligne].IDCDE_COMMANDE); 		
				ec.ressourceID = Number(values[ligne].IDINVENTAIRE_PRODUIT);
				ec.produitCatID = Number(values[ligne].IDPRODUIT_CATALOGUE);
				ec.ligneID = (values[ligne].IDSOUS_TETE)?Number(values[ligne].IDSOUS_TETE):0;
				ec.themeID = Number(values[ligne].IDTHEME_PRODUIT);
				
				ec.boolAccess = Number(values[ligne].BOOL_ACCES);				
				ec.flagRapproche = Number(values[ligne].FLAG_RAPPROCHE);
				ec.flagAffectee = Number(values[ligne].FLAG_AFFECTE);
							
				ec.libelleProduit = values[ligne].LIBELLE_PRODUIT;		
				ec.libelletheme = values[ligne].THEME_LIBELLE;
				ec.libelleLigne = values[ligne].SOUS_TETE;
				ec.commentaire = values[ligne].COMMENTAIRES;
				
				_liste.addItem(ec);		
				_liste.itemUpdated(ec);			
			}			
		}
		public function get liste():ArrayCollection{
			return _liste;
		} 
		public function set liste(a : ArrayCollection):void{
			_liste = a;
		}
		
		
		public function prepareList(idcommande : int):void{
					 
			commandeID = idcommande;
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_GETLISTE,
													getListResultHandler,null);													
			RemoteObjectUtil.callService(opList,idcommande);
		}
		/*
		public function  prepareGroupeList(idGpe : int):void{			
			 
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_GETGROUPELISTE,
													getListResultHandler,null);
													
			RemoteObjectUtil.callService(opList,idGpe);
		}*/
		
		
		private function getListResultHandler(re : ResultEvent):void{
			try{
				setliste(re.result as ArrayCollection);
				dispatchEvent(new Event(ElementCommande.LISTE_COMPLETE));
			}catch(e : Error){
				dispatchEvent(new Event(ElementCommande.LISTE_ERROR));
			}			
		}
	}
}