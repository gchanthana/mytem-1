package commandemobile.system
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	[Bindable]
	public class Site extends EventDispatcher
	{
		private var _id_site 			:int;
		private var _libelle_site 		:String;
		private var _ref_site			:String;
		private var _code_interne_site	:String = "";
		private var _adr_site			:String;
		private var _pays_site			:String;
		private var _cp_site			:String;
		private var _commune_site		:String;
		private var _commentaire_site	:String;
		
		public var IS_LIVRAISON			:Boolean = false;
		public var IS_FACTURATION		:Boolean = false;
		
		public var col_liste_pays 		:ArrayCollection = new ArrayCollection();

		public function Site()
		{
			
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------GET------------------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
		public function get id():int
		{	
			return _id_site;
		}
		public function get libelle_site():String
		{	
			return _libelle_site;
		}
		public function get ref_site():String
		{	
			return _ref_site;
		}
		public function get code_interne_site():String
		{	
			return _code_interne_site;
		}
		public function get adr_site():String
		{	
			return _adr_site;
		}
		public function get commune_site():String
		{	
			return _commune_site;
		}
		public function get cp_site():String
		{	
			return _cp_site;
		}
		public function get pays_site():String
		{	
			return _pays_site;
		}
		public function get commentaire_site():String
		{	
			return _commentaire_site;
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------SET------------------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
		public function set id_site(id_site :int):void
		{	
			this._id_site = id_site;
		}
		public function set libelle_site(libelle_site:String):void
		{	
			this._libelle_site =libelle_site ;
		}
		public function set ref_site(ref_site:String):void
		{	
			this._ref_site = ref_site;
		}
		public function set code_interne_site(code_interne_site:String):void
		{	
			this._code_interne_site =code_interne_site ;
		}
		public function set adr_site(adr_site:String):void
		{	
			this._adr_site = adr_site;
		}
		public function set commune_site(commune_site:String):void
		{	
			this._commune_site = commune_site;
		}
		public function set commentaire_site(commentaire_site:String):void
		{	
			this._commentaire_site = commentaire_site;
		}
		public function set pays_site(pays_site:String):void
		{	
			this._pays_site = pays_site;
		}
		public function set cp_site(cp_site:String):void
		{	
			this._cp_site = cp_site;
		}
		/*--------------------------------------------------------------------------------------------------------
		-------------------------------------------------méthodes public-------------------------------------------
		--------------------------------------------------------------------------------------------------------*/
//		public function save_site():void
//		{
//			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
//																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
//																		"addSite", 
//																		process_add_site);
//			RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,_libelle_site,_ref_site,_code_interne_site,_adr_site,_cp_site,_commune_site,_commentaire_site);
//		}
//		public function save_site():void
//		{
//			
//			
//			
//			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
//																				"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
//																				"addSite", 
//																				process_add_site);
//			RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,
//											_libelle_site,
//											_ref_site,
//											_code_interne_site,
//											_adr_site,
//											_cp_site,
//											_commune_site,
//											_commentaire_site,
//											_pays_site);
//		}
		
		
		public function addSite():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M21.Site",
																				"addSite", 
																				process_add_site);
			RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,_libelle_site,_ref_site,_code_interne_site,_adr_site,_cp_site,_commune_site,_commentaire_site,_pays_site, formatBoolToInt(IS_LIVRAISON), formatBoolToInt(IS_FACTURATION));
		}
		
		private function formatBoolToInt(value:Boolean):int
		{
			var formatedInt:int = -1;
			
			if(value)
				formatedInt = 1;
			else
				formatedInt = 0;
			
			return formatedInt;
		}
		
		public function save_modif_site():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"updateSite", 
																		process_modif_site);
			RemoteObjectUtil.callService(op,_id_site,_libelle_site,_ref_site,_code_interne_site,_adr_site,_cp_site,_commune_site,_commentaire_site);
		}
		public function supprimer_site():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"removeSite", 
																		process_supprimer_site);
			RemoteObjectUtil.callService(op,_id_site);
		}
		public function listePays():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.revendeurs.GestionPays",
																				"get_liste_pays",
																				listePays_handler);
			RemoteObjectUtil.callService(op);
		}
		private function process_supprimer_site(re : ResultEvent):void
		{
			if(re.result != -1)
			{
				Alert.show(ResourceManager.getInstance().getString('M16', 'Supprim_'));
			}
		}
		private function process_add_site(re : ResultEvent):void
		{
			if(re.result != -1)
			{
				_id_site = re.result as int;
				dispatchEvent(new Event("addSite",true));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M16', 'Cr_ation_du_site_impossible_'),'Consoview');
			}
		}
		private function process_modif_site(re : ResultEvent):void
		{
			if(re.result != -1)
			{
				Alert.show(ResourceManager.getInstance().getString('M16', 'Modifi_'));
			}
		}
		private function erreur(evt : FaultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M16', 'Erreur_traitement_site'));
		}
		private function listePays_handler(re:ResultEvent):void
		{
			col_liste_pays = re.result as ArrayCollection;
		}
	}
}