package commandemobile.composants.Livraisonpartiel
{
	import commandemobile.composants.importdemasse.ImportDeMasseIHM;
	import commandemobile.utils.VerificationImport;
	
	import composants.com.hillelcoren.utils.ArrayCollectionUtils;
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	import composants.util.lignes.LignesUtils;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.References;
	import gestioncommande.entity.SaisieTechniqueEquipement;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.ImportDeMasseEvent;
	import gestioncommande.itemrenderer.datagridheaderrenderer.checkboxrenderer.CheckboxHeaderRenderer;
	import gestioncommande.itemrenderer.testREnderere;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.WorkflowService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	
	[Bindable]
	public class LivraisonpartielImpl extends TitleWindow
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var composantImportDeMasse		:ImportDeMasseIHM;
		
		public var dgcLivPart				:DataGridColumn;
		public var dgcRebus					:DataGridColumn;
		public var dgcMateriel				:DataGridColumn;
		public var dgcAffectation			:DataGridColumn;
		public var dgcPuk					:DataGridColumn;
		public var dgcSim					:DataGridColumn;
		public var dgcLigne					:DataGridColumn;
		public var dgcIMEI					:DataGridColumn;
		public var dgcEquipement			:DataGridColumn;
		public var dgcCollaborateur			:DataGridColumn;
		public var dgcProduit				:DataGridColumn;
		
		public var btnValid					:Button;
		
		public var chxLivPart				:CheckBox;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _equService				:EquipementService = new EquipementService();
		
		private var _cmdService				:CommandeService = new CommandeService();
		
		private var _wfwService				:WorkflowService = new WorkflowService();		
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var commande					:Commande;
		
		public var myaction					:Action;
		
		private var _infosMail				:InfosObject = new InfosObject();
		
		private var compteurVerif				:Number = 0;
		
		private var _verifImport				:VerificationImport = new VerificationImport();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listReferences			:ArrayCollection = new ArrayCollection();
		public var listReferencesSpie		:ArrayCollection = new ArrayCollection();
		public var libelleRestricted		:ArrayCollection = new ArrayCollection();
		public var libelleHeaders			:ArrayCollection = new ArrayCollection();
		
		
		private var _libelleHeaders			:ArrayCollection = new ArrayCollection([
			{header:ResourceManager.getInstance().getString('M16','Libell_'), datafield:'LIBELLE_CMD'},
			{header:ResourceManager.getInstance().getString('M16','Collaborateur'), datafield:'LIBELLE_EMP'},
			{header:ResourceManager.getInstance().getString('M16','Equipement'), datafield:'LIBELLE_EQU'},
			{header:ResourceManager.getInstance().getString('M16','IMEI'), datafield:'NUM_IMEI'},
			{header:ResourceManager.getInstance().getString('M16','Ligne2'), datafield:'SOUSTETE'},
			{header:ResourceManager.getInstance().getString('M16','Num_ro_de_SIM'), datafield:'NUM_SIM'},
			{header:ResourceManager.getInstance().getString('M16','Code_PUK'), datafield:'CODE_PUK'}
		]);
		
		private var _libelleRestricted			:ArrayCollection = new ArrayCollection([
			{header:ResourceManager.getInstance().getString('M16','Libell_'), datafield:'LIBELLE_CMD'},
			{header:ResourceManager.getInstance().getString('M16','Collaborateur'), datafield:'LIBELLE_EMP'},
			{header:ResourceManager.getInstance().getString('M16','Equipement'), datafield:'LIBELLE_EQU'},
			{header:ResourceManager.getInstance().getString('M16','IMEI'), datafield:'NUM_IMEI'},
			{header:ResourceManager.getInstance().getString('M16','Ligne2'), datafield:'SOUSTETE'},
			{header:ResourceManager.getInstance().getString('M16','Num_ro_de_SIM'), datafield:'NUM_SIM'},
			{header:ResourceManager.getInstance().getString('M16','Code_PUK'), datafield:'CODE_PUK'}
		]);
		
		public var isCommandeLivre				:Boolean = false;
		public var isMobile						:Boolean = true;
		public var isAccesLivraison				:Boolean = false;
		public var commandePartielleSend		:Boolean = false;
		
		public var text_libelle_Colonne			:String = '';
		
		public var nbrSelected					:int = 0;
		public var myHeight						:int = 385;	
		
		private var _ARTICLESLIVRES				:ArrayCollection;
		private var _IDSARTICLESLIV				:ArrayCollection;
		
		private var _closeValidate				:Boolean  = false;
		private var _isNoPhone					:Boolean  = false;
		
		private var _libelleFirstCommande		:String = '';
		private var _libelleSecondCommande		:String = '';
		private var _textHelpImport				:String = ResourceManager.getInstance().getString('M16', 'Le_principe_des_imports_de_masse_est_l_i')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', 'L_import_de_masse_suppose_un_soin_partic')
			+ "<br><br>"
			+ ResourceManager.getInstance().getString('M16', 'Les_r_gles_sont_les_suivantes__le_non_re')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '___b_Ne_pas_modifier_le_nom_des_colonnes')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '___b_L_ordre_des_lignes_ne_doivent_pas__')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '___b_Les_num_ros_de_lignes_doivent_toujo')
			+ "<br><br>"	
			+ ResourceManager.getInstance().getString('M16', 'Pour_plus_de_souplesse__l_ordre_des_colo')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', 'Des_colonnes_peuvent__tre_rajout_es_mais')
			+ "<br><br>"	
			+ ResourceManager.getInstance().getString('M16', 'Certaines_colonnes_sont_l____titre_indic')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '__Mobile___Libell___collaborateur___quip')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '__Fixe_Data____quipement_')
			+ "<br><br>"
			+ ResourceManager.getInstance().getString('M16', 'D_autres_seront_prises_en_compte__')
			+ "<br><br>"
			+ ResourceManager.getInstance().getString('M16', '_b__s_Mobile____s___b_')
			+ "<br><br>"
			+ ResourceManager.getInstance().getString('M16', '__Commande_de_nouvelle_ligne____quipemen')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '__Commande_de_nouvelle_ligne___ligne__Nu')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '__Commande_d__quipement___IMEI_')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '__Commande_d_accessoires___Aucun_')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '__Renouvellement___IMEI__Num_ro_de_SIM__')
			+ "<br><br>"
			+ ResourceManager.getInstance().getString('M16', '_b__s_Fixe_Data____s___b_')
			+ "<br><br>"
			+ ResourceManager.getInstance().getString('M16', '__Commande_de_nouvelle_ligne___Ligne_')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '__Commande_d__quipements___Num_ro_de_s_r')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '__Commande_d_accessoires___Aucun_')
			+ "<br>"
			+ ResourceManager.getInstance().getString('M16', '__Renouvellement___Aucun_');
		
		private var _idsegment					:int = 1;
		public var isDoublon					:Boolean = false;		

		private const SPIE_1					:Number = 5821693;
		private const SPIE_2					:Number = 6149989;
		private var groupIdx					:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		public var isGrpSpie					:Boolean = false;
		public var _columnsProduitsSpie			:Array = [];
		public var dg_produitSpie				:DataGrid;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function LivraisonpartielImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER DE L'IHM
		//--------------------------------------------------------------------------------------------//
		
		protected function creationCompleteHandler(fe:FlexEvent):void
		{
			initialisationHandler();
			
			getReferencesInfos();
			
			isMobile = whatIsTypeCommande();
			
			whatIsSegment();
			
			sizeDatagrid();
			
			switch(isMobile)
			{
				case true	: text_libelle_Colonne = ResourceManager.getInstance().getString('M16', 'IMEI_terminal__'); break;
				case false 	: text_libelle_Colonne = ResourceManager.getInstance().getString('M16', 'Num_ro_de_s_rie'); break;
			}
			
			composantImportDeMasse.textHelpImport = _textHelpImport;
			
			initDataGridImportMasse();
		}
		
		public function tabb():ArrayCollection
		{
			return composantImportDeMasse.dgElements.dataProvider as ArrayCollection;
		}
		
		private function initialisationHandler():void
		{
			composantImportDeMasse.dgElements.addEventListener(CheckboxHeaderRenderer.HEADERCHECKBOX_SELECT, selectAll);
			composantImportDeMasse.dgElements.addEventListener(CheckboxHeaderRenderer.HEADERCHECKBOX_UNSELECT, unselectAll);
			composantImportDeMasse.dgElements.addEventListener('ARTICLE_UDPDATE', updateItem);
			
			composantImportDeMasse.addEventListener(CommandeEvent.IMPORT_FINISHED, importFinishedHandler);
			
			systemManager.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyPressed);
			
			//---> LISTENER IMPORT DE MASSE
			addEventListener(ImportDeMasseEvent.IMPORT_ADD_ITEM, importAddElementHandler);
			addEventListener(ImportDeMasseEvent.IMPORT_REMOVE_ITEM, importRemoveItemHandler);
			addEventListener(ImportDeMasseEvent.IMPORT_REMOVE_ALL, importRemoveAllHandler);
			
			//--->LISTENER ITEMRENDERER IMPORT DE MASSE
			addEventListener('REFRESH_ITEMSELECTED', refreshItemSelecetdHandler);	
			addEventListener('ARTICLE_LIVRER_CHANGE', article_livrer_changeHandler);
		}
		
		/*
		METHODES OBLIGATOIRES SI L'ON FAIT UN EXPORT DE DONNÉES POUR CRÉER AUTOMATIQUEMENT UN FICHIER *.XLS
		*/
		
		public function formatObject(myObject:Object):References
		{
			return new References(listReferences);
		}
		
		public function getDataToExport():Array
		{
			return [commande.IDCOMMANDE];
		}
		
		public function getAdditionalHeader():Array
		{
			return [10];
		}
		
		public function getRestrictedHeader():Array
		{
			return [10];
		}
		
		public function getAdditionalVar():Array
		{
			return [commande.IDPOOL_GESTIONNAIRE, commande.IDTYPE_COMMANDE, _isNoPhone];
		}
		
		public function getUuid():String
		{
			return 'uuid';
		}
		
		public function getFileName():String
		{
			return commande.NUMERO_COMMANDE + '_export_references_' + Formator.getSuffixeFileName();
		}
		
		public function formatInfosImported(values:ArrayCollection, isImported:Boolean):ArrayCollection
		{
			var dataFormated	:ArrayCollection = new ArrayCollection();
			var headerCol		:String = '';
			var lenValues		:int = values.length;
			
			if(lenValues > 0)
				headerCol = findHeaderLigne(values[0]);
			if(headerCol == '')
			{
				return values;
			}
			else
			{
				for(var i:int = 0;i < lenValues;i++)
				{
					if(i != 0)
						values[i][headerCol] = formatPhoneNumber(values[i][headerCol]);
					dataFormated.addItem(values[i]);
				}
			}
			
			return dataFormated;
		}
		
		private function findHeaderLigne(value:Object):String
		{
			var proper	:Object = ObjectUtil.getClassInfo(value);
			var colName	:ArrayCollection = new ArrayCollection(proper.properties);
			var lenCol	:int = colName.length;
			var headCol	:String = '';
			
			for(var i:int = 0;i < lenCol;i++)
			{
				if(value[colName[i].localName] == ResourceManager.getInstance().getString('M16','Ligne2'))
					headCol = colName[i].localName;
			}
			
			return headCol;
		}
		
		private function formatPhoneNumber(phoneNumber:String):String
		{
			var newPhoneNumber:String = '';
			
			if(phoneNumber != null)
			{
				if(phoneNumber  == '')
				{
					newPhoneNumber = 'empty';
				}
				else if(phoneNumber  != '')
				{
					newPhoneNumber =  StringUtil.trim(phoneNumber);
				}
			}
			
			return newPhoneNumber;
		}
		
		private function removeSpace(value:String):String
		{
			var nbrNoSpace	:String = '';
			var splitedNbr	:Array = value.split(' ');
			var splitLen	:int = splitedNbr.length;
			
			for(var i:int = 0;i < splitLen;i++)
			{
				nbrNoSpace = nbrNoSpace + splitedNbr[i];
			}
			
			return nbrNoSpace;
		}
		
		private function initDataGridImportMasse():void
		{
			whatIsSegment();
			
			if(_idsegment > 1)//---> FIXE / RÉSEAU
			{
				if(commande.IDTYPE_COMMANDE == TypesCommandesMobile.NVL_LIGNE_FIXE)
				{
					if((groupIdx == this.SPIE_1) || (groupIdx == this.SPIE_2))
					{
						composantImportDeMasse.setDataGridColumns([dgcLivPart, dgcIMEI, dgcEquipement, dgcCollaborateur]);
						this._columnsProduitsSpie = [dgcProduit, dgcLigne];
						composantImportDeMasse.textNumberElements = ResourceManager.getInstance().getString('M16', '_quipement_s_');
						dgcLigne.width = 200;
						
						libelleHeaders 		= ObjectUtil.copy(_libelleHeaders) as ArrayCollection;
						libelleRestricted 	= ObjectUtil.copy(_libelleRestricted) as ArrayCollection;
						
						libelleHeaders.removeItemAt(0);//---> Libell_
						libelleHeaders.removeItemAt(2);//---> IMEI
						libelleHeaders.removeItemAt(3);//---> Num_ro_de_SIM
						libelleHeaders.removeItemAt(3);//---> Code_PUK
						
						libelleRestricted.removeItemAt(0);//---> Libell_
						libelleRestricted.removeItemAt(0);//---> Collaborateur
						libelleRestricted.removeItemAt(2);//---> Ligne2
						libelleRestricted.removeItemAt(2);//---> Num_ro_de_SIM
						libelleRestricted.removeItemAt(2);//---> Code_PUK
					}
					else
					{
						composantImportDeMasse.setDataGridColumns([dgcLivPart, dgcLigne, dgcEquipement, dgcCollaborateur]);
						
						libelleHeaders 		= ObjectUtil.copy(_libelleHeaders) as ArrayCollection;
						libelleRestricted 	= ObjectUtil.copy(_libelleRestricted) as ArrayCollection;
						
						libelleHeaders.removeItemAt(0);//---> Libell_
						libelleHeaders.removeItemAt(2);//---> IMEI
						libelleHeaders.removeItemAt(3);//---> Num_ro_de_SIM
						libelleHeaders.removeItemAt(3);//---> Code_PUK
						
						libelleRestricted.removeItemAt(0);//---> Libell_
						libelleRestricted.removeItemAt(0);//---> Collaborateur
						libelleRestricted.removeItemAt(2);//---> Ligne2
						libelleRestricted.removeItemAt(2);//---> Num_ro_de_SIM
						libelleRestricted.removeItemAt(2);//---> Code_PUK
					}

				}
				else if(commande.IDTYPE_COMMANDE == TypesCommandesMobile.EQU_NUS_FIXE)
				{
					composantImportDeMasse.setDataGridColumns([dgcLivPart, dgcIMEI, dgcEquipement, dgcCollaborateur]);
					
					libelleHeaders 		= ObjectUtil.copy(_libelleHeaders) as ArrayCollection;
					libelleRestricted 	= ObjectUtil.copy(_libelleRestricted) as ArrayCollection;

					libelleHeaders.removeItemAt(0);//---> Libell_
					libelleHeaders.removeItemAt(3);//---> Ligne
					libelleHeaders.removeItemAt(3);//---> Num_ro_de_SIM
					libelleHeaders.removeItemAt(3);//---> Code_PUK
					
					libelleRestricted.removeItemAt(0);//---> Libell_
					libelleRestricted.removeItemAt(0);//---> Collaborateur
					libelleRestricted.removeItemAt(1);//---> IMEI
					libelleRestricted.removeItemAt(2);//---> Num_ro_de_SIM
					libelleRestricted.removeItemAt(2);//---> Code_PUK
					
				}
				else if(commande.IDTYPE_COMMANDE == TypesCommandesMobile.RENOUVELLEMENT_FIXE)
				{
					composantImportDeMasse.setDataGridColumns([dgcLivPart, dgcLigne, dgcIMEI, dgcEquipement, dgcCollaborateur]);
					
					libelleHeaders 		= ObjectUtil.copy(_libelleHeaders) as ArrayCollection;
					libelleRestricted 	= ObjectUtil.copy(_libelleRestricted) as ArrayCollection;

					libelleHeaders.removeItemAt(0);//---> Libell_
					libelleHeaders.removeItemAt(4);//---> Num_ro_de_SIM
					libelleHeaders.removeItemAt(4);//---> Code_PUK

					libelleRestricted.removeItemAt(0);//---> Libell_
					libelleRestricted.removeItemAt(4);//---> Num_ro_de_SIM
					libelleRestricted.removeItemAt(4);//---> Code_PUK
				}
			}
			else //---> MOBILE
			{
				if((commande.IDTYPE_COMMANDE == TypesCommandesMobile.NOUVELLES_LIGNES) ||(commande.IDTYPE_COMMANDE == TypesCommandesMobile.PORTABILITE)) 
				{
					composantImportDeMasse.setDataGridColumns([dgcLivPart, dgcPuk, dgcSim, dgcLigne, dgcIMEI, dgcEquipement, dgcCollaborateur]);
					
					libelleHeaders 		= ObjectUtil.copy(_libelleHeaders) as ArrayCollection;
					libelleRestricted 	= ObjectUtil.copy(_libelleRestricted) as ArrayCollection;
					
					libelleRestricted.removeItemAt(4);//---> Ligne2
					libelleRestricted.removeItemAt(4);//---> Num_ro_de_SIM
					libelleRestricted.removeItemAt(4);//---> Code_PUK
				}
				else if(commande.IDTYPE_COMMANDE == TypesCommandesMobile.EQUIPEMENTS_NUS)
				{
					composantImportDeMasse.setDataGridColumns([dgcLivPart, dgcIMEI, dgcEquipement, dgcCollaborateur]);
					
					libelleHeaders 		= ObjectUtil.copy(_libelleHeaders) as ArrayCollection;
					libelleRestricted 	= ObjectUtil.copy(_libelleRestricted) as ArrayCollection;

					libelleHeaders.removeItemAt(4);//---> Ligne2
					libelleHeaders.removeItemAt(4);//---> Num_ro_de_SIM
					libelleHeaders.removeItemAt(4);//---> Code_PUK
					
					libelleRestricted.removeItemAt(4);//---> Ligne2
					libelleRestricted.removeItemAt(4);//---> Num_ro_de_SIM
					libelleRestricted.removeItemAt(4);//---> Code_PUK
				}
				else if(commande.IDTYPE_COMMANDE == TypesCommandesMobile.RENOUVELLEMENT)
				{
					composantImportDeMasse.setDataGridColumns([dgcLivPart, dgcRebus, dgcMateriel, dgcAffectation, dgcPuk, dgcSim, dgcLigne, dgcIMEI, dgcEquipement, dgcCollaborateur]);
					
					libelleHeaders 		= ObjectUtil.copy(_libelleHeaders) as ArrayCollection;
					libelleRestricted 	= ObjectUtil.copy(_libelleRestricted) as ArrayCollection;

					libelleRestricted.removeItemAt(3);//---> IMEI
					libelleRestricted.removeItemAt(4);//---> Num_ro_de_SIM
					libelleRestricted.removeItemAt(4);//---> Code_PUK
				}else if(commande.IDTYPE_COMMANDE == TypesCommandesMobile.NOUVELLE_CARTE_SIM)
				{
					composantImportDeMasse.setDataGridColumns([dgcLivPart, dgcRebus, dgcMateriel, dgcAffectation, dgcPuk, dgcSim, dgcLigne, dgcIMEI, dgcEquipement, dgcCollaborateur]);
					
					libelleHeaders 		= ObjectUtil.copy(_libelleHeaders) as ArrayCollection;
					libelleRestricted 	= ObjectUtil.copy(_libelleRestricted) as ArrayCollection;
					
					libelleRestricted.removeItemAt(3);//---> IMEI
					libelleRestricted.removeItemAt(4);//---> Num_ro_de_SIM
					libelleRestricted.removeItemAt(4);//---> Code_PUK
				}else if(commande.IDTYPE_COMMANDE == TypesCommandesMobile.SAV)
				{
					composantImportDeMasse.setDataGridColumns([dgcLivPart, dgcIMEI, dgcEquipement, dgcCollaborateur]);
					
					libelleHeaders 		= ObjectUtil.copy(_libelleHeaders) as ArrayCollection;
					libelleRestricted 	= ObjectUtil.copy(_libelleRestricted) as ArrayCollection;
					
					libelleHeaders.removeItemAt(4);//---> Ligne2
					libelleHeaders.removeItemAt(4);//---> Num_ro_de_SIM
					libelleHeaders.removeItemAt(4);//---> Code_PUK
					
					libelleRestricted.removeItemAt(4);//---> Ligne2
					libelleRestricted.removeItemAt(4);//---> Num_ro_de_SIM
					libelleRestricted.removeItemAt(4);//---> Code_PUK
				}
				
			}
			
		}// End initDataGridImportMasse()
		
		
		//** Lorsque l'import de matrice est fini - Handler **//
		private function importFinishedHandler(cmde:CommandeEvent):void
		{
			
			if((commande.IDTYPE_COMMANDE == TypesCommandesMobile.NOUVELLES_LIGNES) ||(commande.IDTYPE_COMMANDE == TypesCommandesMobile.PORTABILITE))
			{
				var len	:int = listReferences.length;
				
				for(var i:int = 0;i < len;i++)
				{
					// supprimer les espaces blancs au debut et à la fin de la String
					var listSOUSTETE:String = StringUtil.trim(listReferences[i]['SOUSTETE'] as String);
					var listOLD_SOUSTETE:String = StringUtil.trim(listReferences[i].OLD_SOUSTETE as String);
					var listNUM_IMEI:String = StringUtil.trim(listReferences[i]['NUM_IMEI'] as String);
					var listOLD_IMEI:String = StringUtil.trim(listReferences[i].OLD_IMEI as String);
					var listNUM_SIM:String = StringUtil.trim(listReferences[i]['NUM_SIM'] as String);
					var listOLD_SIM:String = StringUtil.trim(listReferences[i].OLD_SIM as String);
					
					listReferences[i]['SOUSTETE'] = listSOUSTETE;
					listReferences[i].OLD_SOUSTETE = listOLD_SOUSTETE;
					listReferences[i]['NUM_IMEI'] = listNUM_IMEI;
					listReferences[i].OLD_IMEI = listOLD_IMEI;
					listReferences[i]['NUM_SIM'] = listNUM_SIM;
					listReferences[i].OLD_SIM = listOLD_SIM;
					
					
					// verifier le statut de la SOUSTETE (error/empty/en base[ou pas]) à la position[i] dans le fichier
					if(listReferences[i]['SOUSTETE'] == 'error') // si SOUSTETE n'est pas lue(ex : mauvaise syntaxe)
					{
						listReferences[i].SOUSTETE = listReferences[i].OLD_SOUSTETE;
						listReferences[i].ISVALIDE = false;
						listReferences[i].LIGNESUTILS.myResultVerification = 3;
					}
					else if(listReferences[i]['SOUSTETE'] == 'empty') // si SOUSTETE n'est pas renseignée
					{
						listReferences[i].SOUSTETE = listReferences[i].OLD_SOUSTETE;
					}
					else{}
					
					
					// verifier le statut de IMEI (error/empty/en base[ou pas]) à la position[i] dans le fichier
					if(listReferences[i]['NUM_IMEI'] == 'error') // si IMEI n'est pas lue(ex : mauvaise syntaxe)
					{
						listReferences[i].NUM_IMEI = listReferences[i].OLD_IMEI;
						listReferences[i].ISVALIDE = false;
						listReferences[i].LIGNESUTILS.myResultVerificationIMEI = 3;
					}
					else if(listReferences[i]['NUM_IMEI'] == 'empty') // si IMEI n'est pas renseignée
					{
						listReferences[i].NUM_IMEI = listReferences[i].OLD_IMEI;
					}
					else{}
					
					
					// verifier le statut de SIM (error/empty/en base[ou pas]) à la position[i] dans le fichier
					if(listReferences[i]['NUM_SIM'] == 'error') // si SIM n'est pas lue(ex : mauvaise syntaxe)
					{
						listReferences[i].NUM_SIM = listReferences[i].OLD_SIM;
						listReferences[i].ISVALIDE = false;
						listReferences[i].LIGNESUTILS.myResultVerificationSIM = 3;
					}
					else if(listReferences[i]['NUM_SIM'] == 'empty') // si SIM n'est pas renseignée
					{
						listReferences[i].NUM_SIM = listReferences[i].OLD_SIM;
					}
					else{}
					
					listReferences.itemUpdated(listReferences[i]);
				}
				
				dispatchEvent(new Event('DISABLE_ENABLE_BUTTON_VALIDATION', true));
			}
		}
		
		
		private function handleKeyPressed(ke:KeyboardEvent):void
		{
			
		}
		
		private function importAddElementHandler(idme:ImportDeMasseEvent):void
		{
			
		}
		
		private function importRemoveItemHandler(idme:ImportDeMasseEvent):void
		{
			
		}
		
		private function importRemoveAllHandler(idme:ImportDeMasseEvent):void
		{
			
		}
		
		private function refreshItemSelecetdHandler(e:Event):void
		{
			
		}
		
		private function getReferencesInfos():void
		{
			if((groupIdx == this.SPIE_1) || (groupIdx == this.SPIE_2))
			{
				_cmdService.fournirReferenceInfosSpie(commande.IDCOMMANDE, commande.IDPOOL_GESTIONNAIRE, commande);
				_cmdService.addEventListener(CommandeEvent.INFOS_REFERENCES, infosReferencesHandler);
			}
			else
			{
				_cmdService.fournirReferenceInfos(commande.IDCOMMANDE, commande.IDPOOL_GESTIONNAIRE, commande);
				_cmdService.addEventListener(CommandeEvent.INFOS_REFERENCES, infosReferencesHandler);				
			}
			
		}
		
		private function infosReferencesHandler(cmde:CommandeEvent):void
		{
			listReferences = _cmdService.LISTEREFERENCESINFOS;
			listReferences.addEventListener(CollectionEvent.COLLECTION_CHANGE, onCollectionChange);
			
			listReferencesSpie = _cmdService.LISTEREFERENCESINFOS_PRODUIT;
			if(listReferencesSpie.length > 0)
				this.isGrpSpie = true;
			listReferencesSpie.addEventListener(CollectionEvent.COLLECTION_CHANGE, onCollectionSpieChange)
				
			if(listReferences.length > 0)
			{
				if((commande.IDTYPE_COMMANDE == TypesCommandesMobile.NOUVELLES_LIGNES) ||(commande.IDTYPE_COMMANDE == TypesCommandesMobile.PORTABILITE)) 
				{
					if((listReferences[0] as References).IDEQUMODILE == 0)
					{
						_isNoPhone = true;
						
						libelleHeaders.removeItemAt(3);
					}
					else
						libelleRestricted.removeItemAt(3);
				}
				else if((commande.IDTYPE_COMMANDE == TypesCommandesMobile.EQUIPEMENTS_NUS) ||(commande.IDTYPE_COMMANDE == TypesCommandesMobile.SAV) )
				{
					if((listReferences[0] as References).IDEQUMODILE > 0)
					{
						_isNoPhone = false;
						
						libelleRestricted.removeItemAt(3);
					}
					else
					{
						_isNoPhone = true;
					}
				}
			}
			// test spie
			if(listReferencesSpie.length > 0)
			{
				if((commande.IDTYPE_COMMANDE == TypesCommandesMobile.NOUVELLES_LIGNES) || (commande.IDTYPE_COMMANDE == TypesCommandesMobile.PORTABILITE)) 
				{
					if((listReferencesSpie[0] as References).IDEQUMODILE == 0)
					{
						_isNoPhone = true;
						
						libelleHeaders.removeItemAt(3);
					}
					else
						libelleRestricted.removeItemAt(3);
				}
				else if(commande.IDTYPE_COMMANDE == TypesCommandesMobile.EQUIPEMENTS_NUS)
				{
					if((listReferencesSpie[0] as References).IDEQUMODILE > 0)
					{
						_isNoPhone = false;
						
						libelleRestricted.removeItemAt(3);
					}
					else
					{
						_isNoPhone = true;
					}
				}
			}
		}
		
		protected function onCollectionChange(event:CollectionEvent):void
		{
			isDoublon = _verifImport.checkDoublon(listReferences);
		}
		
		protected function onCollectionSpieChange(event:CollectionEvent):void
		{
			isDoublon = _verifImport.checkDoublon(listReferencesSpie);
		}
		
		private function updateItem(e:Event):void
		{
			if(composantImportDeMasse.dgElements.selectedItem != null)
				(composantImportDeMasse.dgElements.dataProvider as ArrayCollection).itemUpdated(composantImportDeMasse.dgElements.selectedItem);
		}
		
		private function selectAll(e:Event):void
		{
			var articles	:ArrayCollection = composantImportDeMasse.dgElements.dataProvider as ArrayCollection;
			var lenArticles	:int = articles.length;
			var isSelected	:Boolean = true;
			
			for(var i:int = 0; i < lenArticles;i++)
			{
				(articles[i] as References).IS_LIV_PART = isSelected;
				
				articles.itemUpdated(articles[i]);
			}
			
			nbrSelected = (composantImportDeMasse.dgElements.dataProvider as ArrayCollection).length;
		}
		
		private function unselectAll(e:Event):void
		{
			var articles	:ArrayCollection = composantImportDeMasse.dgElements.dataProvider as ArrayCollection;
			var lenArticles	:int = articles.length;
			var isSelected	:Boolean = false;
			
			for(var i:int = 0; i < lenArticles;i++)
			{
				(articles[i] as References).IS_LIV_PART = isSelected;
				
				articles.itemUpdated(articles[i]);
			}
			
			nbrSelected = 0;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION PROPRE A LA POPOUP
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		protected function btnAnnulClickHandler(e:Event):void
		{
			_closeValidate = false;
			
			closeHandler();
		}
		
		protected function btnValidClickHandler(e:Event):void
		{
			if((groupIdx == this.SPIE_1) || (groupIdx == this.SPIE_2))
				checkArticlesSpie(e);
			else
				checkArticlesSerialNumber(e);
		}
		
		protected function chxLivPartChangeHandler(e:Event):void
		{
			var articles	:ArrayCollection = composantImportDeMasse.dgElements.dataProvider as ArrayCollection;
			var lenArticles	:int = articles.length;
			
			for(var i:int = 0; i < lenArticles;i++)
			{
				(articles[i] as References).LIV_PART_ACTIF = chxLivPart.selected;
				
				articles.itemUpdated(articles[i]);
			}
			
			if(!chxLivPart.selected)
				unselectAll(null)
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - PRIVATE
		//--------------------------------------------------------------------------------------------//
		
		private function sizeDatagrid():void
		{
			if(isCommandeLivre)
				myHeight = 385;
			else
				myHeight = 460;
			
			this.validateNow();
		}
		
		private function whatIsSegment():void
		{
			if(commande.SEGMENT_MOBILE == 1 && commande.SEGMENT_FIXE == 0 && commande.SEGMENT_DATA == 0)
				_idsegment = 1;//MOBILE
			
			if(commande.SEGMENT_MOBILE == 0 && commande.SEGMENT_FIXE == 1 && commande.SEGMENT_DATA == 0)
				_idsegment = 2;//FIXE
			
			if(commande.SEGMENT_MOBILE == 0 && commande.SEGMENT_FIXE == 0 && commande.SEGMENT_DATA == 1)
				_idsegment = 3;//DATA
		}
		
		private function closeHandler():void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function articleUpdate(value:XML):void
		{
			_equService.manageLivraisonEquipements(value);
			_equService.addEventListener(CommandeEvent.COMMANDE_MANAGED, livraisonManagedHandler);
		}
		
		private function checkArticlesSerialNumber(e:Event):void
		{
			var articles	:ArrayCollection = composantImportDeMasse.dgElements.dataProvider as ArrayCollection;
			var lenArt		:int = articles.length;
			var bool		:Boolean = true;
			
			_ARTICLESLIVRES = new ArrayCollection();
			_IDSARTICLESLIV = new ArrayCollection();
			
			
			for(var i:int = 0;i < lenArt;i++)
			{
				var references:References = articles[i] as References;
				
				if(references.LIGNESUTILS.myResultVerification > 1)
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Attention__nTous_les_num_ros_ne_sont_pas'), 'Consoview', null);
					
					return;
				}
				
				if(references.IS_LIV_PART)
					_IDSARTICLESLIV.addItem(references.IDARTICLE);
				
				
				if(references.IDEQUMODILE > 0)
				{
					if(references.NUM_IMEI == '')
						bool = false;
				}
				
				if(references.IDEQUSIM > 0 || references.IDEQUSIM == -1)
				{
					if(references.NUM_SIM == '' || references.CODE_PUK == '')
						bool = false;
				}
				
				if ((commande.IDTYPE_COMMANDE == TypesCommandesMobile.SAV) && (references.NUM_IMEI == references.OLD_IMEI))
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Num_ro_IMEI_d_j__pr_sent_dans_la_base'), 'Mytem', null);
					
					return;
				}
				
				myaction = references.ACTION_SELECTED;
				
				_ARTICLESLIVRES.addItem(references);
			}
			if(!bool)
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M16', 'Attention___Tous_les_informations_ne_sont_pas_saisie'), 'Consoview', resultAlert);
			else
			{
				//articleUpdate(formatSerialNumberToXML(_ARTICLESLIVRES));
				Alert.show(ResourceManager.getInstance().getString('M16', 'Voulez_vous_vraiment_confirmer_ce_renseignement_'),
					ResourceManager.getInstance().getString('M16', 'Confirmation_'),
					(Alert.YES | Alert.NO), this, closeConfirmationEventAlert);
				Alert.yesLabel = "Oui";
				Alert.noLabel = "Non";
			}
		}
		
		private function checkArticlesSpie(e:Event):void
		{
			var produitsSpie	:ArrayCollection = dg_produitSpie.dataProvider as ArrayCollection;
			var lenArtSpie		:int = produitsSpie.length;
			
			var articles	:ArrayCollection = composantImportDeMasse.dgElements.dataProvider as ArrayCollection;
			var lenArt		:int = articles.length;
			
			_IDSARTICLESLIV = new ArrayCollection();
			_ARTICLESLIVRES = new ArrayCollection();
			
			var bool1		:Boolean = true;
			var bool2		:Boolean = true;
			
			// Equipements
			
			for(var i:int = 0;i < lenArt;i++)
			{
				var references_eqpt:References = articles[i] as References;
				
				if(references_eqpt.LIGNESUTILS.myResultVerification > 1)
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Attention__nTous_les_num_ros_ne_sont_pas'), 'Consoview', null);
					
					return ;
				}
				
				if(references_eqpt.IS_LIV_PART)
					_IDSARTICLESLIV.addItem(references_eqpt.IDARTICLE);
				
				
				if(references_eqpt.IDEQUMODILE > 0)
				{
					if(references_eqpt.NUM_IMEI == '')
						bool2 = false;
				}
				
				if(references_eqpt.IDEQUSIM > 0 || references_eqpt.IDEQUSIM == -1)
				{
					if(references_eqpt.NUM_SIM == '' || references_eqpt.CODE_PUK == '')
						bool2 = false;
				}
				
				myaction = references_eqpt.ACTION_SELECTED;
				
				_ARTICLESLIVRES.addItem(references_eqpt);
			}
			
			//Produits
			for(var j:int = 0;j < lenArtSpie;j++)
			{
				var referencesSpie:References = produitsSpie[j] as References;
				
				if(referencesSpie.LIGNESUTILS.myResultVerification > 1)
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Attention__nTous_les_num_ros_ne_sont_pas'), 'Consoview', null);
					
					return ;
				}
				
				if(referencesSpie.IS_LIV_PART)
					_IDSARTICLESLIV.addItem(referencesSpie.IDARTICLE);
				
				if(referencesSpie.IDEQUMODILE > 0)
				{
					if(referencesSpie.NUM_IMEI == '')
						bool1 = false;
				}
				
				if(referencesSpie.IDEQUSIM > 0 || referencesSpie.IDEQUSIM == -1)
				{
					if(referencesSpie.NUM_SIM == '' || referencesSpie.CODE_PUK == '')
						bool1 = false;
				}
				
				myaction = referencesSpie.ACTION_SELECTED;
				
				_ARTICLESLIVRES.addItem(referencesSpie);
			}
			
			if(!(bool1 && bool2))
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M16', 'Attention___Tous_les_informations_ne_sont_pas_saisie'), 'Consoview', resultAlert);
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M16', 'Voulez_vous_vraiment_confirmer_ce_renseignement_'),
					ResourceManager.getInstance().getString('M16', 'Confirmation_'),
					(Alert.YES | Alert.NO), this, closeConfirmationEventAlert);
				Alert.yesLabel = "Oui";
				Alert.noLabel = "Non";
			}
		}
		
		private function closeConfirmationEventAlert(e:CloseEvent):void
		{
			if(e.detail == Alert.YES){
				articleUpdate(formatSerialNumberToXML(_ARTICLESLIVRES));
			}
			else{
				;
			}
		}
		
		private function counterDataSelected():void
		{
			var articles	:ArrayCollection = composantImportDeMasse.dgElements.dataProvider as ArrayCollection;
			var lenArticles	:int = articles.length;
			var cptr		:int = 0;
			
			for(var i:int = 0; i < lenArticles;i++)
			{
				if((articles[i] as References).IS_LIV_PART)
					cptr++;
			}
			
			nbrSelected = cptr;
		}
		
		private function getActionsExpedierCommande():void
		{
			_wfwService.fournirActionsNoRemote(commande.IDLAST_ETAT, commande);
			
			var actions	:ArrayCollection = _wfwService.listeActions;
			var lenAct	:int = actions.length;
			
			for(var i:int = 0; i < lenAct;i++)
			{
				if(actions[i].IDACTION == 2073)
					myaction = actions[i];
			}
			
			commandePartielleSend = true;
			
			dispatchEvent(new Event('MAIL_LIV_PART'));
		}
		
		private function addNewLibelleCommande():void
		{
			_libelleFirstCommande 	= '';
			_libelleSecondCommande 	= '';
			
			if(!isRenouvellement())
			{
				if(countChar(commande.NUMERO_COMMANDE) == 2)
				{
					_libelleFirstCommande 	= commande.NUMERO_COMMANDE + '-001';
					_libelleSecondCommande 	= commande.NUMERO_COMMANDE + '-002';
				}
				else
				{
					var renbr:String = whatIsCommandeNumber(commande.NUMERO_COMMANDE);
					
					_libelleFirstCommande 	= commande.NUMERO_COMMANDE;
					_libelleSecondCommande 	= renbr;
				}
			}
			else
			{
				if(countChar(commande.NUMERO_COMMANDE) == 1)
				{
					_libelleFirstCommande 	= commande.NUMERO_COMMANDE + '-001';
					_libelleSecondCommande 	= commande.NUMERO_COMMANDE + '-002';
				}
				else
				{
					var ncnbr:String = whatIsCommandeNumber(commande.NUMERO_COMMANDE);
					
					_libelleFirstCommande 	= commande.NUMERO_COMMANDE;
					_libelleSecondCommande 	= ncnbr;
				}
			}
		}
		
		private function countChar(libelle:String):int
		{
			var mySplit:Array = libelle.split("-");
			
			return mySplit.length;
		}
		
		private function whatIsCommandeNumber(libelle:String):String
		{
			var myNbr	:String = '';
			var idx	 	:int	= libelle.lastIndexOf("-");
			
			var cut		:String = libelle.slice(idx+1);
			var myLib	:String = libelle.slice(0, idx);
			
			var nbr		:int	= int(cut);
			
			if(!isNaN(nbr))
			{
				var newNbr:int = nbr+1;
				var stg	:String = newNbr.toString();
				
				myNbr = stg;
				
				switch(stg.length)
				{
					case 1 : myNbr = "-00" + myNbr;	break;
					case 2 : myNbr = "-0" + myNbr;	break;
				}
				
				myLib = myLib + myNbr;
			}
			else
				myLib = libelle;
			
			return myLib;
		}
		
		//--- true-> MOBILE, false-> FIXE/DATA
		private function isRenouvellement():Boolean
		{
			var OK:Boolean = true;
			
			switch(commande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	OK = true;  break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		OK = true;  break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		OK = true;  break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		OK = true;  break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: OK = false; break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	OK = false; break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			OK = false; break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	OK = false; break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		OK = false; break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	OK = false; break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			OK = false; break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		OK = false; break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		OK = false; break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	OK = false; break;//--- REACT
				case TypesCommandesMobile.PORTABILITE: 	OK = false; break;//--- REACT
				case TypesCommandesMobile.SAV: 	OK = false; break;//--- REACT
			}
			
			return OK;
		}
		
		private function updateEtatMateriel():void
		{
			var articles	:ArrayCollection = composantImportDeMasse.dgElements.dataProvider as ArrayCollection;
			var lenArticles	:int = articles.length;
			
			var materiels	:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0;i < lenArticles;i++)
			{
				var myReferences:References = articles[i] as References;
				
				if(myReferences.TRAITE == 0)
				{
					if(myReferences.IS_ALLOCATE == true || myReferences.IS_RESTORE == true || myReferences.IS_SCRAP == true)
						materiels.addItem(myReferences);
				}
			}
			
			if(materiels.length > 0)
				updateEtatMaterielResult(materiels);
			else
				dispatchEvent(new Event('MAIL_LIV_PART'));
		}
		
		private function updateEtatMaterielResult(values:ArrayCollection):void
		{
			var etatMat:ArrayCollection = createArrayEtatMateriel(values);
			
			_equService.desaffectesimterm(etatMat.source);
			_equService.addEventListener(CommandeEvent.LISTED_MAJ_SIMTERM, majSimTermHandler);
		}
		
		private function createArrayEtatMateriel(values:ArrayCollection):ArrayCollection
		{
			var etatMat	:ArrayCollection = new ArrayCollection();
			var len		:int = values.length;
			
			for(var i:int = 0; i < len;i++)
			{
				var myReferences:References = values[i] as References;
				
				if(myReferences.IDARTICLE > 0 && (myReferences.IDEQUSIM > 0 || myReferences.IDEQUSIM == -1))
				{
					var simterobject:Object 	= new Object();
					simterobject.IDARTICLE 	= myReferences.IDARTICLE;
					simterobject.IDSIM	 	= myReferences.IDEQUSIM;
					simterobject.BOOLTERM	= Formator.formatBoolean(myReferences.IS_ALLOCATE);
					simterobject.BOOLCOLL	= Formator.formatBoolean(myReferences.IS_RESTORE);
					simterobject.BOOLREBU	= Formator.formatBoolean(myReferences.IS_SCRAP);
					
					etatMat.addItem(simterobject);
				}
			}
			
			return etatMat;
		}
		
		private function majSimTermHandler(cmde:CommandeEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16','Mise___jour_effectu_e'), this);
			
			dispatchEvent(new Event('MAIL_LIV_PART'));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FORMATE XML POUR LE RENSEIGNEMENT DES REFERENCES
		//--------------------------------------------------------------------------------------------//
		
		//BOOL_CARTESIM, IDEQUIPEMENT_SIM, NUM_CARTESIM, PUK, BOOL_TERMINAL, IDEQUIPEMENT_MOB, NAME_EQUIPEMENT, IMEI
		/* 
			NB: s'il s'agit d'une commade de NV ligne  + Equi alors on passe les 2 objets XML
			correspondant aux 2 cas ((myReferences.IDEQUSIM > 0) et (myReferences.IDEQUMODILE > 0) )
		*/
		private function formatSerialNumberToXML(value:ArrayCollection):XML
		{
			var len			:int = value.length;
			var row		 	:XML = <row></row>;
			var equipement 	:XML;
			var iduser		:Number = CvAccessManager.getUserObject().CLIENTACCESSID;
			var idracine	:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			
			for(var i:int = 0;i < len;i++)
			{
				var myReferences:References = value[i] as References;
				
				// objet XML pour une commande de :
				// - Equipement mobile nus
				// - Equipement fixe nus
				if(myReferences.IDEQUMODILE > 0) 
				{
					equipement 	= <equipement></equipement>;
					equipement.appendChild(<iduser>{iduser}</iduser>);
					equipement.appendChild(<idracine>{idracine}</idracine>);
					if ((myReferences.MYCOMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.PORTABILITE) || (myReferences.MYCOMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.SAV))
					{
						equipement.appendChild(<idtypecommande>{myReferences.MYCOMMANDE.IDTYPE_COMMANDE.toString()}</idtypecommande>);
						equipement.appendChild(<idcommande>{myReferences.MYCOMMANDE.IDCOMMANDE.toString()}</idcommande>);
					}
					equipement.appendChild(<idequipement>{searchError(myReferences.IDEQUMODILE.toString())}</idequipement>);
					equipement.appendChild(<no_serie>{searchError(myReferences.NUM_IMEI)}</no_serie>);
					equipement.appendChild(<no_sim></no_sim>);					
					equipement.appendChild(<codepin></codepin>);
					equipement.appendChild(<codepuk></codepuk>);
					equipement.appendChild(<idsous_tete>{searchError(myReferences.IDSOUSTETE.toString())}</idsous_tete>);
					equipement.appendChild(<ligne>{searchError(myReferences.SOUSTETE)}</ligne>);
					equipement.appendChild(<idst>{myReferences.OLD_IDSOUSTETE.toString()}</idst>);
					
					row.appendChild(equipement);
				}
				
				// objet XML pour une commande de :
				// - Nouvelle Ligne mobile AVEC une carte SIM sans Equipement
				if(myReferences.IDEQUSIM > 0) 
				{
					equipement 	= <equipement></equipement>;
					equipement.appendChild(<iduser>{iduser}</iduser>);
					equipement.appendChild(<idracine>{idracine}</idracine>);
					if ((myReferences.MYCOMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.PORTABILITE) || (myReferences.MYCOMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.SAV))
					{
						equipement.appendChild(<idtypecommande>{myReferences.MYCOMMANDE.IDTYPE_COMMANDE.toString()}</idtypecommande>);
						equipement.appendChild(<idcommande>{myReferences.MYCOMMANDE.IDCOMMANDE.toString()}</idcommande>);
					}
					equipement.appendChild(<idequipement>{searchError(myReferences.IDEQUSIM.toString())}</idequipement>);
					equipement.appendChild(<no_serie></no_serie>);
					equipement.appendChild(<no_sim>{searchError(myReferences.NUM_SIM)}</no_sim>);
					equipement.appendChild(<codepin>{searchError(myReferences.CODE_PIN)}</codepin>);
					equipement.appendChild(<codepuk>{searchError(myReferences.CODE_PUK)}</codepuk>);
					equipement.appendChild(<idsous_tete>{searchError(myReferences.IDSOUSTETE.toString())}</idsous_tete>);
					equipement.appendChild(<ligne>{searchError(myReferences.SOUSTETE)}</ligne>);
					equipement.appendChild(<idst>{myReferences.OLD_IDSOUSTETE.toString()}</idst>);
					row.appendChild(equipement);
				}
				// objet XML pour une commande Renouvellement de ligne
				else if(myReferences.IDEQUSIM == -1) 
				{
					equipement 	= <equipement></equipement>;
					equipement.appendChild(<iduser>{iduser}</iduser>);
					equipement.appendChild(<idracine>{idracine}</idracine>);
					equipement.appendChild(<idequipement>{searchError(myReferences.IDEQUSIM.toString())}</idequipement>);
					equipement.appendChild(<no_serie></no_serie>);
					equipement.appendChild(<no_sim>{searchError(myReferences.NUM_SIM)}</no_sim>);
					equipement.appendChild(<codepin>{searchError(myReferences.CODE_PIN)}</codepin>);
					equipement.appendChild(<codepuk>{searchError(myReferences.CODE_PUK)}</codepuk>);
					equipement.appendChild(<idsous_tete>{searchError(myReferences.IDSOUSTETE.toString())}</idsous_tete>);
					equipement.appendChild(<ligne>{searchError(myReferences.SOUSTETE)}</ligne>);
					equipement.appendChild(<idst>{myReferences.OLD_IDSOUSTETE.toString()}</idst>);
					
					equipement.appendChild(<idterminal>{myReferences.IDEQUMODILE.toString()}</idterminal>);
					equipement.appendChild(<idcommande>{commande.IDCOMMANDE.toString()}</idcommande>);
					equipement.appendChild(<idrevendeur>{commande.IDREVENDEUR.toString()}</idrevendeur>);
					equipement.appendChild(<idoperateur>{commande.IDOPERATEUR.toString()}</idoperateur>);
					equipement.appendChild(<idpool>{commande.IDPOOL_GESTIONNAIRE.toString()}</idpool>);
					
					row.appendChild(equipement);
				}
				// Objet XML pour une commande de : Nouvelles ligne fixe
				else if ((myReferences.MYCOMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.NVL_LIGNE_FIXE) && (myReferences.NUM_IMEI == '') )
				{
					equipement.appendChild(<iduser>{iduser}</iduser>);	
					equipement.appendChild(<idracine>{idracine}</idracine>);					
					equipement.appendChild(<idequipement>{searchError(myReferences.IDEQUMODILE.toString())}</idequipement>);
					equipement.appendChild(<no_serie>{searchError(myReferences.NUM_IMEI)}</no_serie>);
					equipement.appendChild(<no_sim></no_sim>);
					equipement.appendChild(<codepin></codepin>);
					equipement.appendChild(<codepuk></codepuk>);
					equipement.appendChild(<idsous_tete>{searchError(myReferences.IDSOUSTETE.toString())}</idsous_tete>);
					equipement.appendChild(<ligne>{searchError(myReferences.SOUSTETE)}</ligne>);
					equipement.appendChild(<idst>{myReferences.OLD_IDSOUSTETE.toString()}</idst>);
					
					row.appendChild(equipement);
				}
			} 
			
			return row;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER OBJECT
		//--------------------------------------------------------------------------------------------//		
		
		private function article_livrer_changeHandler(e:Event):void
		{	
			counterDataSelected();
		}
		
		private function resultAlert(ce:CloseEvent):void
		{
			if(ce.detail == Alert.OK)
				articleUpdate(formatSerialNumberToXML(_ARTICLESLIVRES));
		}
		
		private function refreshCollectionHandler(ce:CollectionEvent):void
		{
			(composantImportDeMasse.dgElements.dataProvider as ArrayCollection).refresh();
		}
		
		private function commandePartielleHandler(cmde:CommandeEvent):void
		{
			if(_cmdService.newidcommande > 0)
			{
				if(_cmdService.newidcommande > 1)
				{
					ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Votre_commande_a__t__expedi_e'), this);
					
					commandePartielleSend = true;
				}
				
				if(isCommandeLivre)
					updateEtatMateriel();
				else
					dispatchEvent(new Event('MAIL_LIV_PART'));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Votre_commande_n_a_pas__t__expedi_e'), 'Consoview', null);
		}
		
		private function livraisonManagedHandler(cmde:CommandeEvent):void
		{
			if(_IDSARTICLESLIV.length > 0)
			{	
				var livre	:int = _IDSARTICLESLIV.length;
				var total	:int = (composantImportDeMasse.dgElements.dataProvider as ArrayCollection).length;
				
				if(livre != total)
				{
					var nlivre	:int = total - livre;
					
					addNewLibelleCommande();
					
					ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M16', 'Attention_vous_avez_indiqu__une_livraison_part') +
						"\n" +
						ResourceManager.getInstance().getString('M16', 'Votre_commande_va_donc__tre_divis_e_en___comm') +
						"\n" +
						ResourceManager.getInstance().getString('M16', 'Commande_') + 
						_libelleFirstCommande + 
						" " +
						ResourceManager.getInstance().getString('M16', '_pour_les_') + 
						livre.toString() + 
						" " +
						ResourceManager.getInstance().getString('M16', '__l_ments_livr_s_') +
						"\n" +
						ResourceManager.getInstance().getString('M16', 'Commande_') + 
						_libelleSecondCommande + 
						" " +
						ResourceManager.getInstance().getString('M16', '_pour_les_') + 
						nlivre.toString() + 
						" " +
						ResourceManager.getInstance().getString('M16', '__l_ments_non_livr_s_'), 
						'Consoview', resultAlertCommandePartielle);
				}
				else
					getActionsExpedierCommande();
			}
			else
				commandePartielleHandler(cmde);
		}
		
		private function resultAlertCommandePartielle(ce:CloseEvent):void
		{
			var myclob:String = Formator.formatInClob(_IDSARTICLESLIV.source);
			
			if(ce.detail == Alert.OK)
			{
				_cmdService.livraisonPartielle(commande.IDCOMMANDE, myclob);
				_cmdService.addEventListener(CommandeEvent.COMMANDE_PARTIELLE, commandePartielleHandler);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FORMATOR
		//--------------------------------------------------------------------------------------------//
		
		//--- true-> MOBILE, false-> FIXE/DATA
		private function whatIsTypeCommande():Boolean
		{
			var OK:Boolean = true;
			
			switch(commande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	OK = true;  break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		OK = false; break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		OK = true;  break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		OK = false; break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: OK = false; break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	OK = false; break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			OK = false; break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	OK = false; break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		OK = true;  break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	OK = false; break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			OK = false; break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		OK = false; break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		OK = false; break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	OK = false; break;//--- REACT
				case TypesCommandesMobile.PORTABILITE: 	OK = true; break;//--- REACT
				case TypesCommandesMobile.SAV: 	OK = true; break;//--- REACT
			}
			
			return OK;
		}
		
		private function searchError(value:String):String
		{
			var newValue:String = '';
			
			if(value != null && value != 'null' && value!= 'NaN')
				newValue = value;
			
			return newValue;
		}
		
		private function lookForCollaborateur(name:String):String
		{
			var nameCollaborateur:String = '';
			
			if(name == '')
				nameCollaborateur = ' - ' 
			else
				nameCollaborateur = name;
			
			return nameCollaborateur;
		}
		
	}
}