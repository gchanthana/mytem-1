package commandemobile.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.List;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.styles.StyleManager;
	import mx.utils.ObjectUtil;
	
	import composants.colorDatagrid.ColorDatagrid;
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.DetailsDeviceEvent;
	import gestioncommande.popup.DetailProduitIHM;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.Formator;
	
	import gestionparcmobile.resiliationLigne.ihm.EquipementsChoice_1ReengagementIHM;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class EquipmentChoiceImpl extends Box
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cboxEngagement			:ComboBox;	
		
		public var txtptSearch				:TextInput;
		
		public var dgListTerminal			:ColorDatagrid;
		
		public var boxVisible				:Box;	
		public var boxEngagementVisible		:Box;
		
		public var list_terminaux			:List;
		public var img_view_list			:Image;
		public var img_view_smart			:Image;
		
		public var glow_0:GlowFilter = new GlowFilter();
		public var glow_1:GlowFilter = new GlowFilter();
		
		public var nbterminaux:Number = 0;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmd					:Commande;
		
		private var _myElements				:ElementsCommande2	= new ElementsCommande2();

		private var _mobile					:EquipementsElements = null;
		private var _cartesim				:EquipementsElements = null;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _equipementsSrv			:EquipementService = new EquipementService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
	
		public var terminaux				:ArrayCollection = new ArrayCollection();
		public var listEngagement			:ArrayCollection = new ArrayCollection();
		
		private var _isTerSimEngReturn		:Boolean = false;
		
		private var _selectEgagement		:Boolean = true;		
		private var _razl					:Boolean = false;
		
		private var _cptrProc				:int = 0;
		private var _idOpe					:int = -1;
		private var _idGes					:int = -1;
		private var _idRev					:int = -1;
		private var _idPro					:int = -1;
		private var _viewS					:int = -1;
		
		private var selectedEquipement		:EquipementsElements;
		
		//--------------------------------------------------------------------------------------------//
		//					COCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE					:int = 2;
		public var ACCESS					:Boolean = false;
		
		public var bool_viewsmart			:Boolean = true;
		
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//		

		public function EquipmentChoiceImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function validateCurrentPage():Boolean
		{
			if(cboxEngagement.selectedItem != null && _selectEgagement)
			{
				if(cboxEngagement.selectedItem.hasOwnProperty("FPC"))
				{
					_myElements.ENGAGEMENT.FPC 	= cboxEngagement.selectedItem.FPC;
					_myElements.ENGAGEMENT 		= cboxEngagement.selectedItem as Engagement;
				}
				else
					_myElements.ENGAGEMENT = cboxEngagement.selectedItem as Engagement;
			}
			else
			{
				if(_selectEgagement)
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_engagement'), 'Consoview', null);
					
					return false;
				}
			}
			
			if(findTerminalSelected())
			{
				if(_mobile.IDFOURNISSEUR > 0)
				{
					if(_cartesim != null)
						_cmd.IDEQUIPEMENTPARENT = _cartesim.IDPARENT = _mobile.IDFOURNISSEUR;
					else
						_cmd.IDEQUIPEMENTPARENT = _mobile.IDFOURNISSEUR;
					
					_myElements.TERMINAUX.addItem(_mobile);
					
					if(_selectEgagement && _cartesim != null)
						_myElements.TERMINAUX.addItem(_cartesim);
				}
				else
					_myElements.TERMINAUX.addItem(_mobile);
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_terminal'), 'Consoview', null);
				return false;
			}
			
			return true;
		}
		
		//reinitialise l'onglet
		public function reset():void
		{
			_mobile 	= null;
			_cartesim 	= null;
			
			terminaux.removeAll();
			listEngagement.removeAll();
			
			_idOpe = 0;
			
			boxVisible.visible = false;
		}
		
		public function setConfiguration():void
		{
			_razl = true;
			_cptrProc = 0;
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 1)
				_selectEgagement = true;
			else
				_selectEgagement = false;
			
			getTerminauxSimCardAndEngagements();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//

		protected function cboxEngagementCloseHandler(e:Event):void
		{
			if(cboxEngagement.selectedItem != null)
			{
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.ENGAGEMENT.fill(cboxEngagement.selectedItem);
				boxVisible.visible = true;
			}
			else
				boxVisible.visible = false;
			
			if(terminaux.length > 0)
				terminaux.itemUpdated(terminaux[0]);
			
			calculerPrix();
		}
		
		private function calculerPrix():void
		{
			for (var i:int = 0; i < terminaux.length; i++) 
			{
				if(terminaux[i].IDEQUIPEMENT == 0) 
					terminaux[i].PRIX_S = '';
				
				if(!boxEngagementVisible.visible) 
				{
					terminaux[i].PRIX_S 	= Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_C));
					terminaux[i].PRIX 	= Number(terminaux[i].PRIX_C);
				}else if(cboxEngagement.selectedItem != null)
				{
					switch(cboxEngagement.selectedItem.CODE)
					{
						case "EDM": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_2));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_2);
							break;
						case "EVQM":	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_3));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_3);
							break;
						case "ETSM": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_4));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_4);
							break;
						case "EQHM": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_5));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_5);
							break;
						case "EDMAR": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_6));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_6);
							break;
						case "EVQMAR":	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_7));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_7);
							break;
						case "ETSMAR": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_8));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_8);
							break;
						case "EQHMAR": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_9));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_9);
							break;
						case "EVQMFCP": terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_3));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_3);
							break;
						default :		terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_C));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_C);
							break;
					}
				}
				
				terminaux.itemUpdated(terminaux[i]);
			}
		}
		
		protected function dgListTerminalClickHandler(me:MouseEvent):void
		{
			if(me == null) return;

			terminalSelected();
		}
		
		protected function txtptSearchHandler(e:Event):void
		{
			if(dgListTerminal.dataProvider != null)
			{
				(dgListTerminal.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListTerminal.dataProvider as ArrayCollection).refresh();
			}
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODE PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER IHM
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener('DETAILS', detailsTerminalSelected);
			addEventListener(DetailsDeviceEvent.MY_DETAILS_DEVICE, detailsTerminalSelectedSmart);
			addEventListener('REFRESH_ITEM_SELECTED', refreshListDevice );
			
			glow_1.color = StyleManager.getColorName('#558ed5');
			glow_1.alpha = 0.8;
			glow_1.blurX = 4;
			glow_1.blurY = 4;
			glow_1.strength = 6;
			glow_1.quality = BitmapFilterQuality.HIGH;
			
			glow_0.color = StyleManager.getColorName('#FFFFFF');
			glow_0.alpha = 0.8;
			glow_0.blurX = 4;
			glow_0.blurY = 4;
			glow_0.strength = 6;
			glow_0.quality = BitmapFilterQuality.HIGH;
			
			img_view_smart.filters = [glow_1];
			img_view_list.filters = [glow_0];
		}
		
		protected function refreshListDevice(event:Event):void
		{
			if(list_terminaux!=null && list_terminaux.selectedItem != null)
				selectedEquipement = list_terminaux.selectedItem as EquipementsElements;
			
			for each (var obj:EquipementsElements in terminaux.source) 
			{
				if((obj.IDFOURNISSEUR != selectedEquipement.IDFOURNISSEUR) || (obj.SUSPENDU_CLT == 1))
				{
					obj.SELECTED = false;
				}
			}
		}
		
		private function showHandler(fe:FlexEvent):void
		{
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			_cmd 		= SessionUserObject.singletonSession.COMMANDE;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			var isSent :Boolean = false;
						
			if(_idOpe != _cmd.IDOPERATEUR || _idRev != _cmd.IDREVENDEUR || _idPro != _cmd.IDPROFIL_EQUIPEMENT)
			{
				getTerminauxSimCardAndEngagements();
				
				isSent = true;
				
				_idOpe = _cmd.IDOPERATEUR;
				_idRev = _cmd.IDREVENDEUR;
				_idPro = _cmd.IDPROFIL_EQUIPEMENT;
			}
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 1)
			{
				boxEngagementVisible.visible = boxEngagementVisible.includeInLayout = _selectEgagement = true;
				
				if(cboxEngagement.selectedItem == null)
					boxVisible.visible = false;
				else
					boxVisible.visible = true;
			}
			else
			{
				boxEngagementVisible.visible = boxEngagementVisible.includeInLayout = _selectEgagement = false;
				boxVisible.visible = true;
			}
			
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM
		//--------------------------------------------------------------------------------------------//
		
		private function detailsTerminalSelected(e:Event):void
		{
			if(dgListTerminal.selectedItem != null)
			{
				var popUpDetails:DetailProduitIHM 	= new DetailProduitIHM();
					popUpDetails.equipement			= dgListTerminal.selectedItem;
					popUpDetails.addEventListener('REFRESH_ITEM_SELECTED', refreshListDevice );
				PopUpManager.addPopUp(popUpDetails, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}
		
		protected function detailsTerminalSelectedSmart(evtDetails:DetailsDeviceEvent):void
		{
			if(evtDetails.selectedEquipementsElements !=null)
			{
				var popUpDetails:DetailProduitIHM 	= new DetailProduitIHM();
				popUpDetails.equipement			= evtDetails.selectedEquipementsElements;
				popUpDetails.addEventListener('REFRESH_ITEM_SELECTED', refreshListDevice );
				PopUpManager.addPopUp(popUpDetails, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//

		private function termianuxSimCardAndEngagementsHandler(cmde:CommandeEvent):void
		{
			_isTerSimEngReturn = true;
			
			_cartesim = _equipementsSrv.simcard;
			
			terminaux = _equipementsSrv.TERMINAUX;
			if(terminaux){
				terminaux.refresh();
			}
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 3)
				addItemEmpty();
			else{
				nbterminaux = terminaux.length;
			}
			
			if(_razl)
				getTerminalSelected(terminaux);
			
			if(_equipementsSrv.ENGAGEMENTS.length > 1)
				cboxEngagement.prompt = ResourceManager.getInstance().getString('M16', 'Choisir_la_dur_e_d_engagement');
			else
			{
				if(_equipementsSrv.ENGAGEMENTS.length == 0)
					cboxEngagement.prompt = ResourceManager.getInstance().getString('M16', 'Aucun_engagement');
				else
					cboxEngagement.prompt = "";
			}
			
			listEngagement = _equipementsSrv.ENGAGEMENTS;
			
			if(_razl)
				getEngegementSelected(listEngagement);
			
			if(cboxEngagement.selectedItem != null)
			{
				boxVisible.visible = true;
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.ENGAGEMENT.fill(cboxEngagement.selectedItem);
			}
			
			checkIfOnly();
			
			this.setSmartView();
		}

		// fonction pour afficher les téléphones en viewSmart au debut plutot que la liste
		private function setSmartView():void
		{
			bool_viewsmart = true;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//	

		private function getTerminauxSimCardAndEngagements():void
		{
			_isTerSimEngReturn = false;
			
			listEngagement.removeAll();
			terminaux.removeAll();
			
			if(_cmd != null)
			{
				_equipementsSrv.fournirTerminauxSimCardEngagements(_cmd.IDPOOL_GESTIONNAIRE, _cmd.IDPROFIL_EQUIPEMENT, _cmd.IDREVENDEUR, _cmd.IDOPERATEUR,
																	SessionUserObject.singletonSession.IDSEGMENT, 1);
				_equipementsSrv.addEventListener(CommandeEvent.TERMSIMCARDENGAG, termianuxSimCardAndEngagementsHandler);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTIONS
		//--------------------------------------------------------------------------------------------//
		
		public function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint			
		{				
			var rColor:uint;				
			rColor = color;				
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			
			if (item != null )				
			{				
				if(item.SUSPENDU_CLT == 1)//En repture
				{	
					rColor = 0xF7FF04;
				}			
				else					
					rColor = color;					
			}				
			return rColor;				
		}
		
		private function addItemEmpty():void
		{
			terminaux.addItemAt(EquipementsElements.formatTerminalEmpty(), 0);
			nbterminaux = terminaux.length -1;
		}
		
		private function checkIfOnly():void
		{
			if(_isTerSimEngReturn)
			{
				if(_cmd.IDTYPE_COMMANDE == TypesCommandesMobile.EQUIPEMENTS_NUS)
				{
					if(terminaux.length == 1)
						dispatchEvent(new Event('ONE_ENGAGEMENT_EQUIPEMENT', true));
				}
				else
				{
					if(listEngagement.length == 1 && terminaux.length == 1)
						dispatchEvent(new Event('ONE_ENGAGEMENT_EQUIPEMENT', true));
				}
				
				calculerPrix();
			}
		}
		
		private function terminalSelected():void
		{
			if(dgListTerminal.selectedItem != null)
			{
				var len	:int = terminaux.length;
				var idT	:int = dgListTerminal.selectedItem.IDFOURNISSEUR;
				
				for(var i:int = 0;i < len;i++)
				{
					terminaux[i].SELECTED = false;
					
					if(terminaux[i].IDFOURNISSEUR == idT)
						if (terminaux[i].SUSPENDU_CLT != 1)
							terminaux[i].SELECTED = true;
					
					(dgListTerminal.dataProvider as ArrayCollection).itemUpdated(terminaux[i]);
				}
				
				dispatchEvent(new Event('MOBILE_CHANGED', true));
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTIONS - FILTRE
		//--------------------------------------------------------------------------------------------//	
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && 	( 
										(item.NIVEAU != null && item.NIVEAU.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.LIBELLE != null && item.LIBELLE.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.PRIX_S != null && item.PRIX_S.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.REF_REVENDEUR != null && item.REF_REVENDEUR.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
									);
			
			return rfilter;
		}

		//--------------------------------------------------------------------------------------------//
		//					FUCNTIONS - RECUPÉRATION DES ANCIENS ÉLÉMENTS SÉLECTIONNÉS
		//--------------------------------------------------------------------------------------------//	
		
		private function findTerminalSelected():Boolean
		{			
			_myElements.TERMINAUX = new ArrayCollection();
			
			_mobile = new EquipementsElements();
			
			var len:int = terminaux.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(terminaux[i].SELECTED)
				{
					_mobile = terminaux[i] as EquipementsElements;
					return true;
				}
			}
			
			return false;
		}
		
		private function getEngegementSelected(values:ArrayCollection):void
		{
			var duree	:String = SessionUserObject.singletonSession.CURRENTCONFIGURATION.ENGAGEMENT.VALUE;
			var len		:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(values[i].VALUE == duree)
				{
					cboxEngagement.selectedIndex = i;
					return;
				}
			}
			
			_cptrProc++;
			
			if(_cptrProc == 2)
				_razl = false;
		}
		
		private function getTerminalSelected(values:ArrayCollection):void
		{
			var lenTer		:int = values.length;
			var lenTerSel	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX.length;
			var idTerminal	:int = 0;
			
			for(var i:int = 0;i < lenTerSel;i++)
			{
				if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].IDEQUIPEMENT != 71)
				{
					idTerminal  = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].IDFOURNISSEUR;
					break;
				}
			}

			for(var j:int = 0;j < lenTer;j++)
			{
				if(values[j].IDFOURNISSEUR == idTerminal)
				{
					values[j].SELECTED = true;
					return;
				}
			}
			
			_cptrProc++;
			
			if(_cptrProc == 2)
				_razl = false;
		}
		
		protected function dataFieldPrice(equipement:Object, column:DataGridColumn):String
		{				
			if(equipement.IDEQUIPEMENT == 0) return "";
			
			if(!boxEngagementVisible.visible) 
			{
				equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_C));
				equipement.PRIX 	= Number(equipement.PRIX_C);
				return equipement.PRIX_S;
			}
			
			if(cboxEngagement.selectedItem != null)
			{
				switch(cboxEngagement.selectedItem.CODE)
				{
					case "EDM": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_2));
									equipement.PRIX = Number(equipement.PRIX_2);
									break;
					case "EVQM":	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_3));
									equipement.PRIX = Number(equipement.PRIX_3);
									break;
					case "ETSM": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_4));
									equipement.PRIX = Number(equipement.PRIX_4);
									break;
					case "EQHM": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_5));
									equipement.PRIX = Number(equipement.PRIX_5);
									break;
					case "EDMAR": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_6));
									equipement.PRIX = Number(equipement.PRIX_6);
									break;
					case "EVQMAR":	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_7));
									equipement.PRIX = Number(equipement.PRIX_7);
									break;
					case "ETSMAR": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_8));
									equipement.PRIX = Number(equipement.PRIX_8);
									break;
					case "EQHMAR": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_9));
									equipement.PRIX = Number(equipement.PRIX_9);
									break;
					case "EVQMFCP": equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_3));
									equipement.PRIX = Number(equipement.PRIX_3);
									break;
					default :		equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_C));
									equipement.PRIX = Number(equipement.PRIX_C);
									break;
				}
			}
			
			return equipement.PRIX_S;
		}
		
		protected function sortPrice(itemA:Object, itemB:Object):int
		{
			var priceA:Number = itemA.PRIX;
			var priceB:Number = itemB.PRIX;
			
			return ObjectUtil.numericCompare(priceA, priceB);
		}
		
		protected function onSmartViewClickHandler(event:MouseEvent):void
		{
			bool_viewsmart = true;
			
			img_view_smart.filters = [glow_1];
			img_view_list.filters = [glow_0];
		}
		
		protected function onListViewClickHandler(event:MouseEvent):void
		{
			bool_viewsmart = false;
			
			img_view_smart.filters = [glow_0];
			img_view_list.filters = [glow_1];
		}

	}
}
