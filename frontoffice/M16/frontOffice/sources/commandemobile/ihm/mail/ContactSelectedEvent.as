package commandemobile.ihm.mail
{
	import flash.events.Event;

	public class ContactSelectedEvent extends Event
	{
		public static const CONTACT_SELECTED:String="contactSelected";
		
		private var _listeContactsSelectionnes:Array;
		
		public function set listeContactsSelectionnes(value:Array):void
		{
			_listeContactsSelectionnes = value;
		}

		public function get listeContactsSelectionnes():Array
		{
			return _listeContactsSelectionnes;
		}
		
		public function ContactSelectedEvent(type:String, listeContacts:Array)
		{
			super(type);
			
			listeContactsSelectionnes = listeContacts;
		}
		
		override public function clone():Event
		{	
			return new ContactSelectedEvent(type,listeContactsSelectionnes); 
		}
		
	}
}