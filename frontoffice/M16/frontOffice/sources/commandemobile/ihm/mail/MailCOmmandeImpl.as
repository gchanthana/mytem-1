package commandemobile.ihm.mail
{
	import composants.mail.MailVO;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.services.Formator;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.containers.Box;
	import mx.controls.CheckBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.validators.EmailValidator;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class MailCOmmandeImpl extends Box
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARAIBLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMs
			//--------------------------------------------------------------------------------------------//
		
		public var lblExpediteur 		:Label;
		public var lblDest 				:Label;
		
		public var tbxcc 				:TextInput;
		public var tbxSujet 			:TextInput;
		
		public var chxMailCommande		:CheckBox;
		public var chxCopieMe 			:CheckBox;
		
		public var txaMessage 			:TextArea;
		
		public var imgContacts 			:Image;

		
			//--------------------------------------------------------------------------------------------//
			//					POPUPs
			//--------------------------------------------------------------------------------------------//
		
		public var popUpDestinataire	:SelectDestinataireIHM = new SelectDestinataireIHM();
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
		
		public var mailCommande 		:MailVO = new MailVO();
		
		public var myAction				:Action = null;
		
			//--------------------------------------------------------------------------------------------//
			//					AUTRES
			//--------------------------------------------------------------------------------------------//
		
		public var expediteurText		:String = '';
		public var sujetText			:String = 'Commande';
		public var corpsMessageText		:String = '';
		
		private var _listeActions		:Array;
		
		private var _headerMailText		:String = '';
		private var _footerMailText		:String = '';
		private var _segment			:String = '';
		private var _racineLibelle		:String = '';

		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function MailCOmmandeImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
		public function setAction(actionSelected:Action):void
		{
			myAction = actionSelected;
			
			sujetText = 'Commande';
			
			attributHeaderAndFooter();
			
			popUpDestinataire.listeMails = '';
			lblDest.text 				 = '';
			
			if(myAction != null && myAction.IDACTION > 0)
			{
				configurerDestinataires();
				
				expediteurText = CvAccessManager.getSession().USER.PRENOM + ' ' + CvAccessManager.getSession().USER.NOM;
				
				var libelle		:String	= SessionUserObject.singletonSession.COMMANDE.LIBELLE_COMMANDE;
				var numero		:String	= SessionUserObject.singletonSession.COMMANDE.NUMERO_COMMANDE;
				
				_segment = giveMeSegmentLibelle();
				
				sujetText = '[' + _racineLibelle + ']' + ' ' + _segment + ' / ' + libelle + ' / ' + numero;
				
				corpsMessageText = _headerMailText + myAction.MESSAGE + _footerMailText;
			}
		}

		public function checkMail():Boolean
		{
			var isOk:Boolean = false;
			
			mailCommande = new MailVO();
			
			if(chxMailCommande.selected)
			{
				mailCommande.destinataire = popUpDestinataire.listeMails;
				
				if(mailCommande.destinataire != '' && mailCommande.destinataire != null)
				{					
					if(tbxcc.text != '')
					{
						isOk = checkEmailAdress(tbxcc.text);
						
						if(isOk)
							tbxcc.errorString = '';
						else
							tbxcc.errorString = ResourceManager.getInstance().getString('M16', 'Erreurs_dans_vos_adresses_email_saisies_');
					}
					else
						isOk = true;
				}
				else
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Vous_n_avez_pas_choisi_de_destinataire__'), 'Consoview', null);
			}
			else
				isOk = true;

			return isOk;
		}

		public function createMailInfos():void
		{
			mailCommande.expediteur 		 = CvAccessManager.getSession().USER.EMAIL;
			mailCommande.cc 				 = tbxcc.text.replace(';',',');
			mailCommande.bcc 				 = '';
			mailCommande.module 			 = _segment;
			mailCommande.repondreA 			 = lblExpediteur.text;
			mailCommande.copiePourExpediteur = (chxCopieMe.selected)?'YES':'NO';
			mailCommande.copiePourOperateur  = 'NO';			
			mailCommande.sujet 				 = tbxSujet.text;
			mailCommande.message 			 = txaMessage.text;
			mailCommande.type 				 = 'text';
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		protected function imgContactsClickHandler(me:MouseEvent):void
		{	
			afficherSelectionContact();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATE
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//
		
		private function giveMeSegmentLibelle():String
		{
			var TC:String = '';
			
			switch(SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	TC = ResourceManager.getInstance().getString('M16', 'Commande_Mobile');				break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_Fixe_R_seau');		break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_');		break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_'); 		break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--- REACT
				case TypesCommandesMobile.SAV: 					TC = ResourceManager.getInstance().getString('M16', 'SAV_'); 						break;//--- REACT
				case TypesCommandesMobile.NOUVELLE_CARTE_SIM: 	TC = ResourceManager.getInstance().getString('M16', 'Nouvelle_carte_SIM_');			break;//--- REACT
			}
			
			return TC;
		}
		
		private function attributHeaderAndFooter():void
		{
			_headerMailText =	'<b>'
								+ CvAccessManager.getSession().USER.PRENOM
								+ ' '
								+ CvAccessManager.getSession().USER.NOM
								+ '</b>'
								+ '<br/>'
								+ CvAccessManager.getSession().USER.EMAIL
								+ '<br/>'
								+ ResourceManager.getInstance().getString('M16', '_br___u_Date__u____')
								+ DateFunction.formatDateAsString(new Date())
								+ '<br/>'
								+ ResourceManager.getInstance().getString('M16', '_br__Soci_t_____b_')
								+ '<b>'
								+ CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE
								+ '</b>'
								+ '<br/>'
								+ ResourceManager.getInstance().getString('M16', '_br__Libell__de_la_demande__b_')						
								+ '<b>'
								+ SessionUserObject.singletonSession.COMMANDE.LIBELLE_COMMANDE
								+ '</b>'
								+ '<br/>'
								+ ResourceManager.getInstance().getString('M16', '_br__Num_ro_de_la_demande__b_')							 
								+ '<b>'
								+ SessionUserObject.singletonSession.COMMANDE.NUMERO_COMMANDE
								+ '</b>'
								+ '<br/>'
								+ ResourceManager.getInstance().getString('M16', '_br__R_f_rence_client____b_')														
								+ '<b>'
								+ SessionUserObject.singletonSession.COMMANDE.REF_CLIENT1
								+ '</b>'
								+ '<br/><br/><br/>';
			
			_footerMailText = 	'<br/>'
								+ ResourceManager.getInstance().getString('M16', 'Cordialement_')
								+ '<br/>'	
								+ '<b>'
								+ CvAccessManager.getSession().USER.PRENOM
								+' '
								+ CvAccessManager.getSession().USER.NOM
								+'.</b>';
		}
		
		/*
		'CC' : Liste des contacts client pouvant faire l'action 4
		'CR' : Liste des contact revendeurs ayant un role 'Commande'
		*/
		
		private function configurerDestinataires():void
		{
			var localCode:String = String(myAction.EXP + myAction.DEST);
			
			switch(localCode)
			{
				case 'CC': 	getListeDestinataire(); 			break;
				case 'CR': 	getListeDestinatairesRevendeur();	break;
			}
		}
		
		private function whatIsTypeCommande():int
		{
			var idrole:int = 101;
			
			switch(SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	idrole = 101;	break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		idrole = 101;	break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		idrole = 101;	break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		idrole = 101; 	break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: idrole = 201;	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	idrole = 201;	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			idrole = 6; 	break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	idrole = 6; 	break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		idrole = 202; 	break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	idrole = 202; 	break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			idrole = 203; 	break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		idrole = 203; 	break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		idrole = 203; 	break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	idrole = 203; 	break;//--- REACT
			}
			
			return idrole;
		}
		
		protected function afficherSelectionContact():void
		{				
			if(popUpDestinataire.dataProvider != null)
			{
				if(popUpDestinataire.dataProvider.length == 1)
				{
					popUpDestinataire.contactsSelectionnes = (popUpDestinataire.dataProvider as ArrayCollection).source;
					popUpDestinataire.listeMails 			= ConsoviewUtil.ICollectionViewToList(popUpDestinataire.dataProvider, 'EMAIL', ',');
				}
				else if(popUpDestinataire.dataProvider.length > 1)
					{
						PopUpManager.addPopUp(popUpDestinataire, this.parent.parent.parent.parent.parent.parent, true);
						PopUpManager.centerPopUp(popUpDestinataire);
					}
					else if(popUpDestinataire.dataProvider.length == 0)
						{
							ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucun_mail_configur_'), 'Consoview', null);
						}
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucun_mail_configur_'), 'Consoview', null);
		}
		
		private function checkEmailAdress(emails:String):Boolean
		{
			var emailSplited		:Array = emails.split(',');
			var resultValidator		:Array = new Array();
			var validator			:EmailValidator = new EmailValidator();
				
			validator.property = 'text';
			
			for(var i:int = 0; i < emailSplited.length;i++)
			{
				if(emailSplited[i] != '')
				{
					var adressmail	:String 	= emailSplited[i].toString();
					var firstIndex	:int		= adressmail.indexOf(' ');
					
					if(firstIndex > -1)
					{
						while(adressmail.indexOf(' ') > -1)
						{
							adressmail = adressmail.replace(' ','');
						}
					}
					
					resultValidator = EmailValidator.validateEmail(validator, adressmail, 'email');
					
					if(resultValidator.length > 0)
					{
						if(resultValidator[0].isError)
							return false;
					}
				}
			}
			
			return true;
		}
		
			//--------------------------------------------------------------------------------------------//
			//					APPELS PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		public function getListeDestinataire():void
		{
			var procedure:String = '';
			
			if(myAction.IDACTION == 2366 || myAction.IDACTION == 2367)
				procedure = 'getHistContact';
			else
				procedure = 'getContactsActSuivante';
			
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation('fr.consotel.consoview.inventaire.commande.GestionWorkFlow',
																			procedure,
																			getListeDestinataireResultEvent);
			
			RemoteObjectUtil.callSilentService(op,SessionUserObject.singletonSession.COMMANDE.IDPOOL_GESTIONNAIRE,
													myAction.IDACTION,
													SessionUserObject.singletonSession.COMMANDE.IDCOMMANDE);
		}

		public function getListeDestinatairesRevendeur():void
		{
			var idRev	:int = SessionUserObject.singletonSession.COMMANDE.IDREVENDEUR;
			var idrole	:int = whatIsTypeCommande();
			
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation('fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur',
																			'getContactsRevendeurWithRole',
																			getListeDestinatairesRevendeurResultEvent);
			
			RemoteObjectUtil.callSilentService(op,idRev,
													idrole);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					LISTENERS PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		private function getListeDestinataireResultEvent(re:ResultEvent):void
		{
			if(re.result)
			{    
				var myContatcs:ArrayCollection = formatConatcts(re.result as ArrayCollection);
				
				popUpDestinataire.dataProvider = myContatcs;
				
				if(myAction.IDACTION == 2366 || myAction.IDACTION == 2367)
				{
					imgContacts.enabled			= false;
					imgContacts.visible 		= false;
					imgContacts.includeInLayout = false;
					
					popUpDestinataire.listeMails = ConsoviewUtil.arrayToList(myContatcs, 'EMAIL', ',');
				}
				else
					afficherSelectionContact();    			
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Pas_de_destinataire_configur__'), 'Consoview', null);
		}
		
		private function getListeDestinatairesRevendeurResultEvent(re:ResultEvent):void
		{
			if(re.result)    	
			{
				popUpDestinataire.dataProvider = re.result as ArrayCollection;
				
				afficherSelectionContact();
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Pas_de_destinataire_configur__pour_ce_re'), 'Consoview', null);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					LISTENERS IHMs
			//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			
			_racineLibelle = CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE;
		}

			//--------------------------------------------------------------------------------------------//
			//					FORMATORS
			//--------------------------------------------------------------------------------------------//
		
		private function formatConatcts(values:ICollectionView):ArrayCollection
		{
			var retour:ArrayCollection = new ArrayCollection();
			
			if(values != null)
			{	
				var cursor:IViewCursor = values.createCursor();
				
				while(!cursor.afterLast)
				{
					var ctcObj:Object 	= new Object();					
						ctcObj.NOM 		= cursor.current.NOM;	
						ctcObj.EMAIL 	= cursor.current.LOGIN_EMAIL;
					
					retour.addItem(ctcObj);
					
					cursor.moveNext();
				}
			}
			
			return retour;
		}
		
	}
}