package commandemobile.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.VBox;
	import mx.controls.Label;
	import mx.controls.List;
	import mx.controls.TextArea;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import commandemobile.ihm.mail.MailCOmmandeIHM;
	
	import gestioncommande.custom.CustomRadioButton;
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.WorkflowService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class ValidationImpl extends Box
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var mailCommande				:MailCOmmandeIHM;
		
		public var listConfig				:List;
		
		public var vbxAction				:VBox;
		
		public var vbxRecapitulatif			:VBox;
		
		public var txaCommentaires			:TextArea;
		
		public var lblRecapitulatif			:Label;
		public var lblEditerMail			:Label;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var selectedAction			:Action;
		
		private var _cmd					:Commande;
		
		private var _configurationElements				:ElementsCommande2 = new ElementsCommande2();

		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _workFlow				:WorkflowService 	= new WorkflowService();
		
		private var _engagement				:EquipementService 	= new EquipementService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var myItems					:ArrayCollection = new ArrayCollection();
		public var listActions				:ArrayCollection = new ArrayCollection();

		public var isActionSelected			:Boolean = false;
		public var isViewMail				:Boolean = false;
		
		private var _razl					:Boolean = false;
		
		private var _idRandom				:int = 0;
		
		//--------------------------------------------------------------------------------------------//
		//					COCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE					:int = 5;
		public var ACCESS					:Boolean = false;
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function ValidationImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);			
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function validateCurrentPage():Boolean
		{
			SessionUserObject.singletonSession.CURRENTCONFIGURATION = new ElementsCommande2();
			
			return true;
		}
		
		//reinitialise l'onglet
		public function reset():void
		{
		}

		public function setConfiguration():void
		{
			_razl = false;
			//eraseLastConfiguration();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//

		protected function btValidAndPurchaseClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('VALID_CONTINU', true));
		}

		protected function lblEditerMailClickHandler(me:MouseEvent):void
		{
			if(!lblEditerMail.enabled) return;
			
			isViewMail = true;
			
			lblRecapitulatif.setStyle('textDecoration', 'underline');
			lblRecapitulatif.useHandCursor 	= true;
			lblRecapitulatif.buttonMode 	= true;
			lblRecapitulatif.validateNow();
			
			lblEditerMail.setStyle('textDecoration', 'none');
			lblEditerMail.useHandCursor = false;
			lblEditerMail.buttonMode 	= false;
			lblEditerMail.validateNow();
		}
		
		protected function lblRecapitulatifClickHandler(me:MouseEvent):void
		{
			isViewMail = false;
			
			lblRecapitulatif.setStyle('textDecoration', 'none');
			lblRecapitulatif.useHandCursor 	= false;
			lblRecapitulatif.buttonMode 	= false;
			lblRecapitulatif.validateNow();
			
			lblEditerMail.setStyle('textDecoration', 'underline');
			lblEditerMail.useHandCursor = true;
			lblEditerMail.buttonMode 	= true;
			lblEditerMail.validateNow();
		}
		
		private function hiddeOtherCustomRadioButton():void
		{
			var myChidldren	:Array = vbxAction.getChildren();
			var lenChildren :int = vbxAction.numChildren;
			
			for(var i:int = 0; i < lenChildren;i++)
			{
				if(myChidldren[i] as CustomRadioButton)
				{
					if(!(myChidldren[i] as CustomRadioButton).selected)
						(myChidldren[i] as CustomRadioButton).visible = (myChidldren[i] as CustomRadioButton).includeInLayout = false;
				}
			}
		}
		
		private function visibleOtherCustomRadioButton():void
		{
			var myChidldren	:Array = vbxAction.getChildren();
			var lenChildren :int = vbxAction.numChildren;
			
			for(var i:int = 0; i < lenChildren;i++)
			{
				if(myChidldren[i] as CustomRadioButton)
					(myChidldren[i] as CustomRadioButton).visible = (myChidldren[i] as CustomRadioButton).includeInLayout = true;
			}
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{	
			addEventListener('ERASE_CONFIGURATION',	eraseConfigurationHandler);
		}
		
		//AJOUTE LA CURRENTCONFIG À LA COMMANDE
		private function showHandler(fe:FlexEvent):void
		{	
			var elementsCommande	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
			var len			:int = elementsCommande.length;
			
			_configurationElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION; //configuration courante
			_cmd 		= SessionUserObject.singletonSession.COMMANDE; //object commande et toutes ses infos
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			if(len > 0)
			{
				if(!isRandomContains(elementsCommande, _configurationElements))
					elementsCommande.addItem(setIdConfiguration(_configurationElements, len));
			}
			else
			{
				_idRandom = _configurationElements.RANDOM;
				elementsCommande.addItem(setIdConfiguration(_configurationElements, len));
			}

			if(vbxAction.numChildren == 0)
				fournirListeActionsPossibles();
			
			setDataToGrid();
			
			goToRefreshPanier();
			
		}
		
		// End Show Event in Step Validation ==> go to refresh panier pour prendre en compte les elements
		private function goToRefreshPanier():void
		{
			dispatchEvent(new Event('STEP_VALIDATION_SHOWED', true));
		}
		
		private function cstmRdbtnClickHandler(e:Event):void
		{
			var myChidldren	:Array = vbxAction.getChildren();
			var lenChildren :int = vbxAction.numChildren;
			
			selectedAction = (e.target as CustomRadioButton).ACTION
			
			for(var i:int = 0; i < lenChildren;i++)
			{
				if(myChidldren[i] as CustomRadioButton)
				{
					if((myChidldren[i] as CustomRadioButton).ACTION.IDACTION != selectedAction.IDACTION)
						(myChidldren[i] as CustomRadioButton).selected = false;
				}
			}

			if(selectedAction.IDACTION > 0)
			{				
				isActionSelected = true;
				
				lblEditerMail.enabled = true;
				
				lblRecapitulatif.setStyle('textDecoration', 'none');
				lblRecapitulatif.useHandCursor 	= false;
				lblRecapitulatif.buttonMode 	= false;
				lblRecapitulatif.validateNow();
				
				lblEditerMail.setStyle('textDecoration', 'underline');
				lblEditerMail.useHandCursor = true;
				lblEditerMail.buttonMode 	= true;
				lblEditerMail.validateNow();
			}
			else
			{
				isActionSelected = false;
				
				lblEditerMail.enabled = false;
				
				lblRecapitulatifClickHandler(new MouseEvent(MouseEvent.CLICK));
				
				lblRecapitulatif.setStyle('textDecoration', 'none');
				lblRecapitulatif.useHandCursor 	= false;
				lblRecapitulatif.buttonMode 	= false;
				lblRecapitulatif.validateNow();
				
				lblEditerMail.setStyle('textDecoration', 'none');
				lblEditerMail.useHandCursor = false;
				lblEditerMail.buttonMode 	= false;
				lblEditerMail.validateNow();
			}
			
			this.validateNow();
			
			mailCommande.setAction(selectedAction);
			
			dispatchEvent(new Event('VALIDER_ENABLED', true));
		}
		
		//---> EFFACE LE CONFIGURATION SELECTIONNEE
		private function eraseConfigurationHandler(e:Event):void
		{
			if(listConfig.selectedItem != null)
			{
				var elements	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
				var len			:int = elements.length;
				var rand		:int = listConfig.selectedItem.RANDOM;
				
				(listConfig.dataProvider as  ArrayCollection).removeItemAt(listConfig.selectedIndex);
				
				for(var i:int = 0;i < len;i++)
				{
					if(elements[i].RANDOM == rand)
					{
						elements.removeItemAt(i);
						if(len > 1)
							_cmd.IDTYPE_COMMANDE = giveMeTypeCommande(elements[0]["TYPE"]);
						return;
					}
				}
			}
		}
		
		//ATTRIBUE LE BON IDTYPECOMMANDE
		private function giveMeTypeCommande(idTypeCmde:int):Number
		{
			var typedecommande:Number = 0;
			
			switch(idTypeCmde)
			{
				case 1: typedecommande = TypesCommandesMobile.NOUVELLES_LIGNES; break;//--- NVL COMMANDE
				case 2: typedecommande = TypesCommandesMobile.NOUVELLES_LIGNES; break;//--- NVL COMMANDE
				case 3: typedecommande = TypesCommandesMobile.EQUIPEMENTS_NUS;  break;//--- NVL COMMANDE
				case 4: typedecommande = TypesCommandesMobile.NOUVELLES_LIGNES; break;//--- NVL COMMANDE
				case 12:typedecommande = TypesCommandesMobile.PORTABILITE; break;//--- NVL COMMANDE
			}
			return typedecommande;
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//

		private function getActionsResultHandler(cmde:CommandeEvent):void
		{	
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function fournirListeActionsPossibles():void
		{
			_workFlow.fournirActionsNoRemote(3066,_cmd,SessionUserObject.singletonSession.IDSEGMENT);
			
			addRecordOnly();
			
			if(_workFlow.listeActions.length > 0)
			{
				for(var i:int = 0;i < _workFlow.listeActions.length;i++)
				{
					if(_workFlow.listeActions[i].CODE_ACTION != 'ANNUL' && _workFlow.listeActions[i].CODE_ACTION != null && _workFlow.listeActions[i].CODE_ACTION != "")
					{
						var cstmRdbtn:CustomRadioButton = new CustomRadioButton();
						cstmRdbtn.ACTION			= _workFlow.listeActions[i] as Action;
						cstmRdbtn.addEventListener('RADIOBUTTON_CLICKHANDLER', cstmRdbtnClickHandler);
						
						vbxAction.addChild(cstmRdbtn);
					}
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - RECUPERATION DES ELEMENTS ET SET TO LIST
		//--------------------------------------------------------------------------------------------//
		
		//AFFICHAGE DE LA COMMANDE(PAR EQUIPEMENTS/OPTIONS/ABOS/ETC) DANS L'ONGLET VALIDATION
		private function setDataToGrid():void
		{
			//details des articles commandés
			var elements	:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARTICLE;
			var len			:int = elements.length;
			
			myItems.removeAll();
			
			for(var i:int = 0;i < len;i++)
			{
				var elts:Object = new Object();
					elts			= addEquipements(elements[i] as ElementsCommande2);
					elts.LIBELLE 	= ResourceManager.getInstance().getString('M16', 'Configuration_') + (i+1).toString();
				
				myItems.addItem(elts); //la configuration commandée
			}
		}
		
		//AJOUT DES EQUIPEMENTS DE LA COMMANDE
		private function addEquipements(value:ElementsCommande2):Object
		{
			var equi	:ArrayCollection = new ArrayCollection();
			var elts	:Object = new Object();
			var lenTer	:int = value.TERMINAUX.length;
			var lenAcc	:int = value.ACCESSOIRES.length;
			var i		:int = 0;
			var qte		:int = value.CONFIGURATIONS.length;
			var prix	:Number = 0;
			var itemTotal:Object = new Object();
			var myTotals:ArrayCollection = new ArrayCollection();
			var totalCommande:Number = 0;
			
			for(i = 0;i < lenTer;i++)//TERMINAUX
			{
				if(value.TERMINAUX[i].IDEQUIPEMENT > 0)
				{
					elts 			= new Object();
					elts.REF =  value.TERMINAUX[i].REF_REVENDEUR;
					elts.EQUIPEMENT = value.TERMINAUX[i].EQUIPEMENT;
					elts.LIBELLE 	= value.TERMINAUX[i].LIBELLE;
					elts.QUANTITE 	= qte;
					elts.MENSUEL	= "";
					elts.PRIX		= Formator.formatTotalWithSymbole(value.TERMINAUX[i].PRIX);
					
					prix  = prix + value.TERMINAUX[i].PRIX;
					totalCommande  = totalCommande + value.TERMINAUX[i].PRIX;
					
					equi.addItem(elts);
				}
			}
			
			for(i = 0;i < lenAcc;i++)//ACCESSOIRES
			{
				elts 			= new Object();
				elts.REF =  value.ACCESSOIRES[i].REF_REVENDEUR;
				elts.EQUIPEMENT = value.ACCESSOIRES[i].EQUIPEMENT;
				elts.LIBELLE 	= value.ACCESSOIRES[i].LIBELLE;
				elts.QUANTITE 	= qte;
				elts.MENSUEL	= "";
				elts.PRIX		= Formator.formatTotalWithSymbole(value.ACCESSOIRES[i].PRIX);
				
				prix  = prix + value.ACCESSOIRES[i].PRIX;
				totalCommande  = totalCommande + value.ACCESSOIRES[i].PRIX;
				
				equi.addItem(elts);
			}
			
			itemTotal.totalCommande = Formator.formatTotalWithSymbole(totalCommande);
			
			var prixRessources:Number = addRessources(value, equi, itemTotal);
			myTotals.addItem(itemTotal);
			
			prix = qte *(prix + prixRessources);
			
			
			var obj:Object 	= new Object();
				obj.ITEMS 	= equi;
				obj.TOTAL	= Formator.formatTotalWithSymbole(prix);
				obj.RANDOM	= value.RANDOM;
				obj.MYTOTALS = myTotals;
		
			return obj;
		}
		
		//AJOUT DES RESSOURCES DE LA COMMANDE
		private function addRessources(value:ElementsCommande2, ress:ArrayCollection, itemTot:Object):Number
		{
			var elts	:Object = new Object();
			var lenAbo	:int = value.ABONNEMENTS.length;
			var lenOpt	:int = value.OPTIONS.length;
			var i		:int = 0;
			var qte		:int = value.CONFIGURATIONS.length;
			var prix	:Number = 0;
			var totalMensuel :Number = 0;
			
			for(i = 0;i < lenAbo;i++)//ABONNEMENT
			{
				elts 			= new Object();
				elts.EQUIPEMENT = value.ABONNEMENTS[i].LIBELLETHEME;
				elts.LIBELLE 	= value.ABONNEMENTS[i].LIBELLE;
				elts.REF		= value.ABONNEMENTS[i].REFERENCE_PRODUIT;
				elts.QUANTITE 	= qte;
				elts.MENSUEL	= value.ABONNEMENTS[i].PRIX_STRG;
				elts.PRIX		= "";
				
				prix  = prix + value.ABONNEMENTS[i].PRIX_UNIT;
				totalMensuel  = totalMensuel + value.ABONNEMENTS[i].PRIX_UNIT;
				
				ress.addItem(elts);
			}
			
			for(i = 0;i < lenOpt;i++)//OPTIONS
			{
				elts 			= new Object();
				elts.EQUIPEMENT = value.OPTIONS[i].LIBELLETHEME;
				elts.LIBELLE 	= value.OPTIONS[i].LIBELLE;
				elts.REF		= value.OPTIONS[i].REFERENCE_PRODUIT;
				elts.QUANTITE 	= qte;
				elts.MENSUEL	= value.OPTIONS[i].PRIX_STRG;
				elts.PRIX		= "";
				
				prix  = prix + value.OPTIONS[i].PRIX_UNIT;
				totalMensuel  = totalMensuel + value.OPTIONS[i].PRIX_UNIT;
				
				ress.addItem(elts);
			}
			
			itemTot.totalMensuel = Formator.formatTotalWithSymbole(totalMensuel);
			return prix;
		}
		
		private function isRandomContains(values:ArrayCollection, randObject:ElementsCommande2):Boolean
		{
			return values.contains(randObject);
		}
		
		private function setIdConfiguration(value:ElementsCommande2, longueur:int):ElementsCommande2
		{
			value.ID 	= longueur + 1;
			value.TYPE 	= SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
				
			return value;
		}
		
		private function eraseLastConfiguration():void
		{
			var elements	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
			var elts		:ElementsCommande2 = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			var lenElts		:int = elements.length;
			var idrand		:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.RANDOM;
			
			if(isRandomContains(elements, elts))
			{
				for(var i:int = 0;i < lenElts;i++)
				{
					if(SessionUserObject.singletonSession.COMMANDEARTICLE[i].RANDOM == idrand)
					{
						SessionUserObject.singletonSession.COMMANDEARTICLE.removeItemAt(i);
						return;
					}
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - MISE EN FORME DES RADIOBUTTON
		//--------------------------------------------------------------------------------------------//
		
		private function addRecordOnly():void
		{
			var cstmRdbtn:CustomRadioButton 		= new CustomRadioButton();
				cstmRdbtn.ACTION					= new Action();
				cstmRdbtn.ACTION.LIBELLE_ACTION		= ResourceManager.getInstance().getString('M16','Enregistrer_la_commande_sans_l_envoyer');
				cstmRdbtn.ACTION.IDACTION			= -1;
				cstmRdbtn.addEventListener('RADIOBUTTON_CLICKHANDLER', cstmRdbtnClickHandler);
			
			vbxAction.addChild(cstmRdbtn);
		}

	}
}
