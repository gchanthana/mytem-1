package commandemobile.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.popup.PopUpLibelleModeleIHM;
	import gestioncommande.services.Formator;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class QuantityChoiceImpl extends Box
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListEquipements	:DataGrid;
		public var dgListAboOptions		:DataGrid;
		
		public var txtptNumberConfig	:TextInput;

		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		protected var _popUplibelle		:PopUpLibelleModeleIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		protected var _cmd				:Commande;
		
		protected var _myElements			:ElementsCommande2 = new ElementsCommande2();

		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listEquipements		:ArrayCollection = new ArrayCollection();
		public var listAboOptions		:ArrayCollection = new ArrayCollection();
		
		public var prixEquipements		:Number = 0;
		public var prixRessources		:Number = 0;
		
		public var convertedToInt		:Boolean = true;
				
		//--------------------------------------------------------------------------------------------//
		//					COCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE				:int = 4;
		
		public var ACCESS				:Boolean = false;
		
		private var _razl				:Boolean = false;
	
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function QuantityChoiceImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//

		public function validateCurrentPage():Boolean
		{
			var isOk:Boolean = true;
			
			if(convertedToInt)
				_cmd.CONFIG_NUMBER = Number(txtptNumberConfig.text);
			else
			{
				isOk = false;
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_nombre_'), 'Consoview', null);
			}
			
			return isOk;
		}
		
		//reinitialise l'onglet
		public function reset():void
		{
			if(_cmd != null)
				_cmd.CONFIG_NUMBER = 1; //reinitialise le nombre d'article(configuration) à 1
			
			listEquipements.removeAll();
			listAboOptions.removeAll();
		}
		
		public function setConfiguration():void
		{
			_razl = false;
			_cmd.CONFIG_NUMBER = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//

		//---> VERIFIE QUE LE TEXTE SAISIE EST BIEN UN ENTIER ET VERIFIE QU'IL EST >= 1 
		protected function txtptNumberConfigHandler(e:Event):void
		{
			convertedToInt = true;
			
			try
			{
				if(Number(txtptNumberConfig.text) < 1)
				{
					txtptNumberConfig.text = "1";
					convertedToInt = false;
				}
			}
			catch(erreur:Error)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_nombre_'), 'Consoview', null);
				convertedToInt = false;
			}
		}

	    protected function btnRecordModeleClickHandler(me:MouseEvent):void
	    {
			_popUplibelle 			 = new PopUpLibelleModeleIHM();
			_popUplibelle.myCommande = _cmd;
			
			PopUpManager.addPopUp(_popUplibelle, this);
			PopUpManager.centerPopUp(_popUplibelle);
	    }

	//--------------------------------------------------------------------------------------------//
	//					METHODE PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS FLEX
		//--------------------------------------------------------------------------------------------//
		
		protected function creationCompleteHandler(fe:FlexEvent):void
		{
			dgListEquipements.addEventListener('TERMINAL_PRICE_CHANGED' , terminalPriceChangedHandler);
		}
		
		//---> AFFECTE TOUS LES EQUIPEMENTS ET RESSOURCES SELECTIONNES PRECEDEMENT AUX PROPRIETEES 
		//---> CONCERNEES ET COMPTE LE TOTAL DE LA CONFIGURATION ET RAFRAICHI LE DATAGRID
		protected function showHandler(fe:FlexEvent):void
		{
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			_cmd 		= SessionUserObject.singletonSession.COMMANDE;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			attributToEquipments();
			attributToAboOptions();
			compteTotal();
			
			callLater(refreshDatagrid);
			
			txtptNumberConfig.text = _cmd.CONFIG_NUMBER.toString();			
		}
		
		protected function terminalPriceChangedHandler(e:Event):void
		{
			var price	:Object = e.target.data;
			var lenTer	:int = _myElements.TERMINAUX.length;
			
			for(var i:int = 0;i < lenTer;i++)
			{
				if(_myElements.TERMINAUX[i].IDFOURNISSEUR ==  price.IDFOURNISSEUR)
				{
					 _myElements.TERMINAUX[i].PRIX		= price.PRIX;
					 _myElements.TERMINAUX[i].PRIX_S	= Formator.formatTotalWithSymbole(price.PRIX);
				}
			}
			
			compteTotal();
			
			dispatchEvent(new Event('PRICE_CHANGE', true));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		protected function attributToEquipments():void
		{			
			var lenTer	:int = _myElements.TERMINAUX.length;
			var lenAcc	:int = _myElements.ACCESSOIRES.length;
			var i		:int = 0;
			
			listEquipements = new ArrayCollection();
			
			for(i = 0;i < lenTer;i++)
			{
				if(_myElements.TERMINAUX[i].IDEQUIPEMENT > 0)
					listEquipements.addItem(_myElements.TERMINAUX[i]);
			}
			
			for(i = 0;i < lenAcc;i++)
			{
				listEquipements.addItem(_myElements.ACCESSOIRES[i]);
			}
			
			(dgListEquipements.dataProvider as ArrayCollection).refresh();
		}
		
		protected function attributToAboOptions():void
		{	
			var lenAbo	:int = _myElements.ABONNEMENTS.length;
			var lenOpt	:int = _myElements.OPTIONS.length;
			var i		:int = 0;

			listAboOptions = new ArrayCollection();
			
			for(i= 0;i < lenAbo;i++)
			{
				listAboOptions.addItem(_myElements.ABONNEMENTS[i]);
			}
			
			for(i = 0;i < lenOpt;i++)
			{
				listAboOptions.addItem(_myElements.OPTIONS[i]);
			}
			
			(dgListAboOptions.dataProvider as ArrayCollection).refresh();
		}

		protected function refreshDatagrid():void
		{
			dgListEquipements.dataProvider 	= listEquipements;
			dgListAboOptions.dataProvider 	= listAboOptions;
		}
		
		//---> COMPTE LE TOTAL DES CONFIGURATIONS QUE L'ON VIENT DE FAIRE
		protected function compteTotal():void
		{	
			prixEquipements = 0;
			prixRessources  = 0;
			
			for(var i:int = 0;i < listEquipements.length;i++)
			{
				if(listEquipements[i].PRIX.toString() != "NaN" && listEquipements[i].PRIX != null)
					prixEquipements = prixEquipements + Number(listEquipements[i].PRIX);
			}
			
			for(var j:int = 0;j < listAboOptions.length;j++)
			{
				if(listAboOptions[j].PRIX_UNIT.toString() != "NaN" && listAboOptions[j].PRIX_UNIT != null)
					prixRessources = prixRessources + Number(listAboOptions[j].PRIX_UNIT);
			}
		}
		
		//---> CHERCHE LE TYPE D'OPTIONS ET RETOURNE UN STRING PLUS COURT
		protected function checkAbo(valueObject:Object):String
		{
			var value:String = "";

			switch(valueObject.THEME_LIBELLE)
			{
				case ResourceManager.getInstance().getString('M16', 'Options_Voix'): 		value = ResourceManager.getInstance().getString('M16', 'Voix'); 	break;
				case ResourceManager.getInstance().getString('M16', 'Options_Data'): 		value = ResourceManager.getInstance().getString('M16', 'Data'); 	break;
				case ResourceManager.getInstance().getString('M16', 'Options_Diverses'): 	value = ResourceManager.getInstance().getString('M16', 'Diverses'); break;
			}
			
			return value;
		}

	}
}
