package commandemobile.ihm
{
	import composants.colorDatagrid.ColorDatagrid;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.popup.PopUpFicheProduitAccessoireIHM;
	import gestioncommande.services.EquipementService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class EquipmentChoice_2Impl extends Box
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListAccessoire			:ColorDatagrid;
		
		public var txtptSearch				:TextInput;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var _cmd						:Commande;
		
		private var _myElements				:ElementsCommande2	= new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _equipementsSrv			:EquipementService = new EquipementService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//

		public var accessoire				:ArrayCollection = new ArrayCollection();
		
		public var goprevious				:Boolean = true;
		
		public var selectedNumber			:Number = 0;
		public var totalNumber				:Number = 0;
		
		public var phoneLibelle				:String = "";
		public var nivo						:String = "";
		public var engementLibelle			:String = "";
		
		private var _razl					:Boolean = false;
		
		private var _idTer					:int = -1;
		private var _idTerminal				:int = -1;
		
		//--------------------------------------------------------------------------------------------//
		//					CONCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE					:int = 2;
		
		public var ACCESS					:Boolean = false;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function EquipmentChoice_2Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//

		public function validateCurrentPage():Boolean
		{
			goprevious = true;
			_myElements.ACCESSOIRES = new ArrayCollection();
			
			for(var i:int = 0; i < accessoire.length;i++)
			{
				if(accessoire[i].SELECTED)
				{ 
					accessoire[i].IDPARENT = _cmd.IDEQUIPEMENTPARENT;
					_myElements.ACCESSOIRES.addItem(accessoire[i]);
				}
			}
			
			if(_myElements.TERMINAUX[0].IDFOURNISSEUR == 0 && accessoire.length == 0)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_au_moins_un_access'), 'Consoview', null);
				return false;
			}
			else
				return true;
			
			return true;
		}
		
		//reinitialise l'onglet
		public function reset():void
		{
			accessoire.removeAll();
			
			phoneLibelle = "";
			nivo 		 = "";
			
			_idTer = _idTerminal = 0;
		}

		public function setConfiguration():void
		{
			_razl = true;
			
			var id:int = getIdTerminalSelected();
			
			_idTer = _idTerminal = id;

			if(_idTerminal == 0)
				getAccessoires();
			else
				getAccessoiresCompatibles();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//
	
		protected function txtptSearchHandler(e:Event):void
		{
			if(dgListAccessoire.dataProvider != null)
			{
				(dgListAccessoire.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListAccessoire.dataProvider as ArrayCollection).refresh();
			}
		}
		
		protected function dgListAccessoireHandler(me:MouseEvent):void
		{
			if(dgListAccessoire.selectedItem != null)
			{
				if(dgListAccessoire.selectedItem.SELECTED)
					dgListAccessoire.selectedItem.SELECTED = false;
				else if(dgListAccessoire.selectedItem.SUSPENDU_CLT != 1)					
					dgListAccessoire.selectedItem.SELECTED = true;
				
				accessoire.itemUpdated(dgListAccessoire.selectedItem);
				
				calculElements();
			}
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODE PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
	
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener("DETAILS", detailsAccessorySelected);
		}

		private function showHandler(fe:FlexEvent):void
		{				
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			_cmd 		= SessionUserObject.singletonSession.COMMANDE;
						
			findTerminal();
			findEngagement();
			
			if(_idTer != _idTerminal)
			{
				if(_idTerminal < 1)
					getAccessoires();
				else
					getAccessoiresCompatibles();
				
				_idTer = _idTerminal;
			}
			else
			{
				if(accessoire.length == 0)
				{
					if(SessionUserObject.singletonSession.LASTVSINDEX <= IDPAGE)
					{
						dispatchEvent(new Event('NO_ACCESSORIES_NEXT', true));
					}
					else if(SessionUserObject.singletonSession.LASTVSINDEX > IDPAGE)
						{
							dispatchEvent(new Event('NO_ACCESSORIES_PREV', true));
						}
				}
			}
		}
		
		private function detailsAccessorySelected(e:Event):void
		{
			if(dgListAccessoire.selectedItem != null)
			{
				var popUpDetails:PopUpFicheProduitAccessoireIHM = new PopUpFicheProduitAccessoireIHM();
					popUpDetails.equipement 					= dgListAccessoire.selectedItem;
				
				PopUpManager.addPopUp(popUpDetails, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENER PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function listeAccessoiresHandler(cmde:CommandeEvent):void
		{
			accessoire.removeAll();
			
			accessoire = _equipementsSrv.listeAccessoires;
			
			if(_razl == false && accessoire.length == 0)
			{
				if(SessionUserObject.singletonSession.LASTVSINDEX <= IDPAGE)
				{
					dispatchEvent(new Event('NO_ACCESSORIES_NEXT', true));
				}
				else if(SessionUserObject.singletonSession.LASTVSINDEX > IDPAGE)
					{
						dispatchEvent(new Event('NO_ACCESSORIES_PREV', true));
					}
			}
			else
			{
				if(_razl)
					getAccessoiresSelected(accessoire);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//

		private function getAccessoires():void
		{
			_equipementsSrv.fournirListeAccessoires(_cmd.IDREVENDEUR, _cmd.IDPROFIL_EQUIPEMENT);
			_equipementsSrv.addEventListener(CommandeEvent.LISTED_ACCESSOIRES, listeAccessoiresHandler);
		}
		
		private function getAccessoiresCompatibles():void
		{
			_equipementsSrv.fournirListeAccessoiresCompatibles(_idTerminal, _cmd.IDPROFIL_EQUIPEMENT);
			_equipementsSrv.addEventListener(CommandeEvent.LISTED_ACCESSOIRES, listeAccessoiresHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		public function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint			
		{				
			var rColor:uint;				
			rColor = color;				
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			
			if (item != null )				
			{				
				if(item.SUSPENDU_CLT == 1)//En repture
				{	
					rColor = 0xF7FF04;
				}			
				else					
					rColor = color;					
			}				
			return rColor;				
		}
		
		private function findTerminal():void
		{
			var len	:int = _myElements.TERMINAUX.length;

			for(var i:int = 0;i < len;i++)
			{
				if(_myElements.TERMINAUX[i].IDEQUIPEMENT != 71)
				{
					phoneLibelle = _myElements.TERMINAUX[i].LIBELLE;
					_idTerminal  = _myElements.TERMINAUX[i].IDFOURNISSEUR;
					nivo 		 = _myElements.TERMINAUX[i].NIVEAU;
					return;
				}
			}
		}
		
		private function findEngagement():void
		{
			if(_myElements.ENGAGEMENT.FPC != "")
				engementLibelle =  giveMeDate();
			else
			{
				if(_myElements.ENGAGEMENT.DUREE == "")
					engementLibelle = ResourceManager.getInstance().getString('M16', 'Aucune');
				else
					engementLibelle = _myElements.ENGAGEMENT.DUREE;
			}
		}
		
		private function calculElements():void
		{
			var len	:int = accessoire.length;
			var cpt	:int = 0;
			var tot	:Number = 0;
			
			for(var i:int = 0;i < len;i++)
			{
				if(accessoire[i].SELECTED)
				{
					tot = tot + Number(accessoire[i].PRIX_C);
					cpt++;
				}
			}
			
			totalNumber 	= tot;
			selectedNumber	= cpt;
		}
		
		private function giveMeDate():String
		{
			var fpc:String 		= _myElements.ENGAGEMENT.FPC;
			var day:String 		= fpc.substr(8,2);
			var month:String	= fpc.substr(5,2);
			var year:String		= fpc.substr(0,4);
			
			return day + "/" + month + "/" + year;
		}
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && 	( 
										(item.LIBELLE != null && item.LIBELLE.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
										||
										(item.PRIX_C != null && item.PRIX_C.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
									);
			
			return rfilter;
		}
		
		private function getIdTerminalSelected():int
		{
			var lenTerSel	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX.length;
			var idTerminal	:int = 0;
						
			for(var i:int = 0;i < lenTerSel;i++)
			{
				if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].IDEQUIPEMENT != 71)
					idTerminal  = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].IDFOURNISSEUR;
			}	
			
			return idTerminal;
		}
		
		private function getAccessoiresSelected(values:ArrayCollection):void
		{
			var lenSel	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.ACCESSOIRES.length;
			var lenAcc	:int = values.length;
						
			for(var i:int = 0;i < lenAcc;i++)
			{
				var idAcc:int = values[i].IDFOURNISSEUR;
				
				for(var j:int = 0;j < lenSel;j++)
				{
					if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.ACCESSOIRES[j].IDFOURNISSEUR != idAcc)
						values[i].SELECTED = true;
				}	
			}
			
			_razl = false;
		}

	}
}