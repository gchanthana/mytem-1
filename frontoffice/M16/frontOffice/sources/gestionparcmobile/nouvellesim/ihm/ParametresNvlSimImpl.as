package gestionparcmobile.nouvellesim.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.FormItem;
	import mx.containers.HBox;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import commandemobile.popup.sitemodification.GestionSiteEvent;
	import commandemobile.popup.sitemodification.ParametreSiteIHM;
	import commandemobile.popup.sitemodification.Site;
	
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.popup.FicheSiteIHM;
	import gestioncommande.popup.PopUpSearchSiteIHM;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.CompteService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.PoolService;
	import gestioncommande.services.RevendeurAutoriseService;
	import gestioncommande.services.RevendeurService;
	import gestioncommande.services.SiteService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class ParametresNvlSimImpl extends Box
	{
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cbxPools						:ComboBox;
		public var cbxCmdType					:ComboBox;
		public var cbxSitesFacturation			:ComboBox;
		public var cbxSites						:ComboBox;
		public var cbxOperateur					:ComboBox;
		public var cbxTitulaire					:ComboBox;
		public var cbxPointFacturation			:ComboBox;
		public var cbxRevendeur					:ComboBox;
		public var cbxAgence					:ComboBox;
		
		public var txtbxRIO						:TextInput;		
		public var txtLibelle					:TextInput;
		public var txtERPPrefixe				:TextInput;
		public var txtERPSuffixe				:TextInput;
		public var txtNumeroMarche				:TextInput;
		public var txtRefOp						:TextInput;
		
		public var txtbxTo						:TextInputLabeled;
		
		public var lblNumCmd					:Label;		
		public var txtDateCommande				:Label;
		
		public var dcLivraisonPrevue			:CvDateChooser;
		
		public var btnNewSite					:Button;
		
		public var frmiDyn1						:FormItem;
		public var frmiDyn2						:FormItem;
		
		public var boxRevendeur					:Box;
		
		public var dgLines						:DataGrid;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _popUpFicheSites			:FicheSiteIHM;
		
		private var _popupParamSite 			:ParametreSiteIHM;
		
		private var _popUpSearchSite			:PopUpSearchSiteIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmd						:Commande;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _revAutorises				:RevendeurAutoriseService = new RevendeurAutoriseService();
		
		private var _revSrv						:RevendeurService = new RevendeurService();
		
		private var _poolSrv					:PoolService = new PoolService();
		
		private var _siteSrv					:SiteService = new SiteService();
		
		private var _cmdeSrv					:CommandeService = new CommandeService();
		
		private var _cpteSrv					:CompteService = new CompteService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var operateur					:ArrayCollection = new ArrayCollection();
		public var pool							:ArrayCollection = new ArrayCollection();
		public var compte						:ArrayCollection = new ArrayCollection();
		public var sousCompte					:ArrayCollection = new ArrayCollection();
		
		public var dateDuJourPlus2JoursOuvrees	:Date = DateFunction.addXJoursOuvreesToDate(new Date(),2);
		
		public var periodeObject				:Object = {rangeStart:new Date(new Date().getTime() + (2 * DateFunction.millisecondsPerDay))};
		
		public var libelleTypeOperation			:String = '';
		public var dateOfToday					:String = '';
		public var compteName					:String = ResourceManager.getInstance().getString('M16', 'Compte');		
		public var souscompteName				:String = ResourceManager.getInstance().getString('M16', 'Sous_compte');
		
		public var isMultiCompte				:Boolean = false;
		public var isMultiSousCompte			:Boolean = false;
		public var multiCompte					:Boolean = false;
		public var isNoCompte					:Boolean = false;
		public var isMobile						:Boolean = true;
		
		public var IDSOUTETE					:int = 0;
		public var IDCOMPTE						:int = 0;
		public var IDSOUSCOMPTE					:int = 0;
		
		private var _libelleCompteOp			:ArrayCollection = new ArrayCollection();
		
		private var _date						:Date = new Date();
		
		private var _boolRevAutorises			:Boolean = false;
		
		//--------------------------------------------------------------------------------------------//
		//					CHAMPS DYNAMIQUES
		//--------------------------------------------------------------------------------------------//
		
		public var hbxChamps1					:HBox;
		public var hbxChamps2					:HBox;
		
		public var LIBELLE_CHAMPS1				:String = ResourceManager.getInstance().getString('M16','R_f_rence_client_1');
		public var EXEMPLE_CHAMPS1				:String = "";
		public var LIBELLE_CHAMPS2				:String = ResourceManager.getInstance().getString('M16','R_f_rence_client_2');
		public var EXEMPLE_CHAMPS2				:String = "";
		
		public var CHAMPS1_ACTIF				:Boolean = false;
		public var CHAMPS2_ACTIF				:Boolean = false;
		public var CHAMPS1_EXEMPLE_VISIBLE		:Boolean = false;
		public var CHAMPS2_EXEMPLE_VISIBLE		:Boolean = false;
		
		private var champs1						:ArrayCollection;
		private var champs2						:ArrayCollection;
		
		//--------------------------------------------------------------------------------------------//
		//					CONCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE					:int = 1;
		
		public var ACCESS					:Boolean = true;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//	
		
		public function ParametresNvlSimImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
		public function getCompte(idop:int, idpool:int):void
		{
			_cpteSrv.fournirCompteOperateurByLoginAndPoolV2(idop, idpool);
			_cpteSrv.addEventListener(CommandeEvent.LISTED_COMPTES, comptesSousComptesHandler);
		}
		
		public function setCommande():void
		{
			_cmd = SessionUserObject.singletonSession.COMMANDE;
			
			autoCompleteRefClient();
		}
		
		public function validateCurrentPage():Boolean
		{
			return checkFields();
		}
		
		public function reset():void
		{
		}
		
		public function setReferencesClientChild():void
		{
			var nbrChildren:int = hbxChamps1.numChildren;
			
			if(nbrChildren == 0)
			{
				champsPersoHandler();
			}
			
			var bool:Boolean = false;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		protected function cbxCmdTypeChangeHandler(e:Event):void
		{
			if(cbxCmdType.selectedItem != null)
			{
				cbxCmdType.errorString = "";
				
				cbxRevendeur.dataProvider 	= null;
				cbxAgence.dataProvider 		= null;
				
				cbxRevendeur.prompt = "";
				cbxAgence.prompt 	= "";
				
				if(_boolRevAutorises)
					getRevendeursAutorises();
				else
					getRevendeurs();
				
				dispatchEvent(new Event('PROFILE_CHANGED', true));
			}
		}
		
		protected function cbxSitesCloseHandler(e:Event):void
		{
			cbxSites.errorString = "";
			
			if(cbxSites.selectedItem != null)
				searchInSiteFacturation(cbxSites.selectedItem.IDSITE_PHYSIQUE);
		}
		
		protected function cbxSitesFacturationCloseHnadler(e:Event):void
		{
			cbxSitesFacturation.errorString = "";
		}
		
		protected function imgSearchClickHandler(me:MouseEvent):void
		{
			var composant		:Image = me.currentTarget as Image;
			var currentData		:ArrayCollection = new ArrayCollection();
			
			_popUpSearchSite = new PopUpSearchSiteIHM();
			
			if(composant != null && composant.id == 'imgSearchL')
			{
				_popUpSearchSite.setInfos(ObjectUtil.copy(cbxSites.dataProvider as ArrayCollection) as ArrayCollection, cbxSites.selectedItem, true);
			}
			else if(composant != null && composant.id == 'imgSearchF')
			{
				_popUpSearchSite.setInfos(ObjectUtil.copy(cbxSitesFacturation.dataProvider as ArrayCollection) as ArrayCollection, cbxSitesFacturation.selectedItem, false);
			}
			
			if(composant != null)
			{
				_popUpSearchSite.addEventListener('SEARCH_SITE_SELECTED', searchSiteHandler);
				
				PopUpManager.addPopUp(_popUpSearchSite, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(_popUpSearchSite);
			}
		}
		
		private function searchSiteHandler(e:Event):void
		{
			var mySiteSelected	:Object = _popUpSearchSite.getInfos();			
			var lenSitesL		:int = (cbxSites.dataProvider as ArrayCollection).length;
			var lenSitesF		:int = (cbxSitesFacturation.dataProvider as ArrayCollection).length;
			var i				:int = 0;
			
			if(_popUpSearchSite.isSiteLivraison)
			{
				for(i = 0;i < lenSitesL;i++)
				{
					if((cbxSites.dataProvider as ArrayCollection)[i].IDSITE_PHYSIQUE == mySiteSelected.IDSITE_PHYSIQUE)
					{
						cbxSites.selectedIndex = i;
						
						break;
					}
				}
				
				searchInSiteFacturation(cbxSites.selectedItem.IDSITE_PHYSIQUE);
			}
			else
			{
				for(i = 0;i < lenSitesF;i++)
				{
					if((cbxSitesFacturation.dataProvider as ArrayCollection)[i].IDSITE_PHYSIQUE == mySiteSelected.IDSITE_PHYSIQUE)
					{
						cbxSitesFacturation.selectedIndex = i;
						
						break;
					}
				}
			}
			
			PopUpManager.removePopUp(_popUpSearchSite);
		}
		
		protected function cbxTitulaireChangeHandler(e:Event):void
		{
			cbxTitulaire.errorString = "";
			
			if(cbxTitulaire.selectedItem != null)
				getSousComptes();
			
			setNewCompteSousCompteToLigne();
		}
		
		protected function cbxPointFacturationChangeHandler(e:Event):void
		{
			cbxPointFacturation.errorString = "";
			
			setNewCompteSousCompteToLigne();
		}
		
		protected function cbxRevendeurChangeHandler(e:Event):void
		{
			cbxAgence.dataProvider 	= null;
			cbxAgence.prompt		= "";
			
			dispatchEvent(new Event('REVENDEUR_CHANGED', true));
			
			if(cbxRevendeur.selectedItem != null)
			{
				getAgences();
				getSLA();
			}
		}
		
		protected function cbxAgenceChangeHandler(e:Event):void
		{
			if(cbxAgence.selectedItem != null)
				cbxAgence.errorString = "";
		}
		
		protected function btnNewSiteClickHandler(e:Event):void
		{
			if(cbxPools.selectedItem !=null)
			{
				_popUpFicheSites = new FicheSiteIHM();
				_popUpFicheSites.idPoolGestionnaire = SessionUserObject.singletonSession.POOL.IDPOOL;
				_popUpFicheSites.addEventListener("refreshSite", ficheSiteHandler);
				
				PopUpManager.addPopUp(_popUpFicheSites,this,true);
				PopUpManager.centerPopUp(_popUpFicheSites);
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_pool_'), 'Consoview', null);
		}
		
		protected function btnModSiteClickHandler(me:MouseEvent):void
		{
			if(cbxSites.selectedItem != null)
			{
				_popupParamSite = new ParametreSiteIHM();
				_popupParamSite.site = mappingSiteSelectedToSite();
				addPop();
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16','S_lectionnez_un_site_'), 'Consoview', null);
		}
		
		protected function textInputHandler(txtbxNumber:int):void
		{
			switch(txtbxNumber)
			{
				case 0: txtLibelle.errorString 		= "";break;
				case 2: txtRefOp.errorString 		= "";break;
				case 3: lblNumCmd.errorString 		= "";break;
				case 4: txtERPPrefixe.errorString 	= "";break;
				case 5: txtERPSuffixe.errorString 	= "";break;
				case 6: txtNumeroMarche.errorString = "";break;
			}		
		}
		
		protected function lblclickHandler(lblNumber:int):void
		{
			switch(lblNumber)
			{
				case 0: txtLibelle.text 			= _cmd.NUMERO_COMMANDE;
					txtLibelle.errorString 		= "";
					break;
				case 1: insertNumeroCommandeInTextbox();
					break;
				case 2: txtRefOp.text 				= _cmd.NUMERO_COMMANDE;
					txtRefOp.errorString 		= "";
					break;
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATE
		//--------------------------------------------------------------------------------------------//	
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{			
			addEventListener('ERASE_LIGNE_SELECTED', eraseLigneSelectedHandler);
			
			_boolRevAutorises = SessionUserObject.singletonSession.BOOLDISTRIBAUTORISES;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			txtbxTo.text = CvAccessManager.getUserObject().PRENOM + ' ' + CvAccessManager.getUserObject().NOM;
			
			setOperationSurLignes();
			champsPersoHandler();
			getLibelleProcedure();
			
			if(boxRevendeur.visible)
				getProfilsEquipements();
			
			getSiteLivraisonFacturation();
			setLibelleTypeOperation();
			
			dateOfToday = Formator.formatDate(_date);
		}
		
		private function showHandler(fe:FlexEvent):void
		{
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
		}
		
		private function eraseLigneSelectedHandler(e:Event):void
		{
			if(dgLines.selectedItem != null)
			{
				var index	:int = dgLines.selectedIndex;
				
				IDSOUTETE 	 = dgLines.selectedItem.IDSOUS_TETE;
				IDCOMPTE 	 = dgLines.selectedItem.IDCOMPTE_FACTURATION;
				IDSOUSCOMPTE = dgLines.selectedItem.IDSOUS_COMPTE;
				
				dispatchEvent(new Event('SOUS_TETE_ERASED', true));
				
				(dgLines.dataProvider as ArrayCollection).removeItemAt(index);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private function ficheSiteHandler(e:Event):void
		{
			getSiteLivraisonFacturation();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getLibelleProcedureHandler(cmde:CommandeEvent):void
		{
			_libelleCompteOp = _cpteSrv.listeLibellesOperateur;
			findLibelleOperateur();
		}
		
		private function comptesSousComptesHandler(cmde:CommandeEvent):void
		{
			var listeComptes	:ArrayCollection = _cpteSrv.listeComptesSousComptes;//Formator.sortFunction(_cpteSrv.listeComptesSousComptes, 'COMPTE_FACTURATION');
			var lenComptes		:int = listeComptes.length;
			
			attributDataToCombobox(cbxTitulaire, listeComptes, ResourceManager.getInstance().getString('M16', 'Aucun_comptes'));
			
			if(lenComptes == 1)
			{
				getSousComptes();
				setNewCompteSousCompteToLigne();
			}
		}
		
		private function siteLivraisonFacturationHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxSites, _siteSrv.listeSitesLivraison, ResourceManager.getInstance().getString('M16', 'Aucun_site'));
			attributDataToCombobox(cbxSitesFacturation, _siteSrv.listeSitesFacturation, ResourceManager.getInstance().getString('M16', 'Aucun_site'));
		}
		
		private function profilsEquipementsHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxCmdType, _poolSrv.listeProfiles, ResourceManager.getInstance().getString('M16', 'Aucun_profile'));
			
			if(_boolRevAutorises && _poolSrv.listeProfiles.length == 1)
			{
				getRevendeursAutorises();
			}
			else if(!_boolRevAutorises && _poolSrv.listeProfiles.length == 1)
			{
				getRevendeurs();
			}
		}
		
		private function revendeursAutorisesHandler(cmde:CommandeEvent):void
		{		
			attributDataToCombobox(cbxRevendeur, _revAutorises.listeRevendeurs, ResourceManager.getInstance().getString('M16', 'Aucun_revendeurs_autoris_s'));
			
			if(_revAutorises.listeRevendeurs.length == 1)
			{
				getAgences();
				getSLA();
			}
		}
		
		private function revendeursHandler(e:Event):void
		{
			attributDataToCombobox(cbxRevendeur, _revSrv.listeRevendeurs, ResourceManager.getInstance().getString('M16', 'Aucun_revendeur'));
			
			if(_revSrv.listeRevendeurs.length == 1)
			{
				getAgences();
				getSLA();
			}	
		}
		
		private function agencesHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxAgence, _revSrv.listeAgences, ResourceManager.getInstance().getString('M16', 'Aucune_agences'));
		}
		
		private function slaHandler(cmde:CommandeEvent):void
		{
			if(_cmdeSrv.revendeurSLA != null)
			{
				var hoursParsing	:Object = Formator.formatHour(_date);
				var delaiTime		:Array 	= (_cmdeSrv.revendeurSLA.DELAI_HOUR_LIVRAISON as String).split(".");
				var delai			:int 	= Number(_cmdeSrv.revendeurSLA.DELAI_LIVRAISON);
				
				if(Number(hoursParsing.HOURS) > Number(delaiTime[0]))
					delai = delai + 1;
				else
				{
					if(Number(hoursParsing.HOURS) == Number(delaiTime[0]))
					{
						if(Number(hoursParsing.MIN) > Number(delaiTime[1]))
							delai = delai + 1;
					}
				}
				
				dcLivraisonPrevue.selectedDate 		= DateFunction.addXJoursOuvreesToDate(new Date(), delai);
				dcLivraisonPrevue.selectableRange 	= {rangeStart:new Date(new Date().getTime() + (delai * DateFunction.millisecondsPerDay))};
			}
			else
			{
				dcLivraisonPrevue.selectedDate 		= DateFunction.addXJoursOuvreesToDate(new Date(), 2);
				dcLivraisonPrevue.selectableRange 	= {rangeStart:new Date(new Date().getTime() + (2 * DateFunction.millisecondsPerDay))};
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getLibelleProcedure():void
		{
			_cpteSrv.fournirLibelleCompteOperateur();
			_cpteSrv.addEventListener(CommandeEvent.LISTED_LIBELLE_OPE, getLibelleProcedureHandler);
		}
		
		private function getComptesSousComptes():void
		{
			_cpteSrv.fournirCompteOperateurByLoginAndPoolV2(cbxOperateur.selectedItem.OPERATEURID, SessionUserObject.singletonSession.POOL.IDPOOL);
			_cpteSrv.addEventListener(CommandeEvent.LISTED_COMPTES, comptesSousComptesHandler);
		}
		
		private function getSiteLivraisonFacturation():void
		{
			_siteSrv.fournirListeSiteLivraisonsFacturation(SessionUserObject.singletonSession.POOL.IDPOOL);
			_siteSrv.addEventListener(CommandeEvent.LISTED_SITES, siteLivraisonFacturationHandler);
		}
		
		private function getProfilsEquipements():void
		{ 
			_poolSrv.fournirProfilEquipements();
			_poolSrv.addEventListener(CommandeEvent.LISTED_PROFILES, profilsEquipementsHandler);
		}
		
		private function getSitesLivraisons():void
		{ 
			_siteSrv.fournirListeSiteLivraisonsFacturation(SessionUserObject.singletonSession.POOL.IDPOOL);
			_siteSrv.addEventListener(CommandeEvent.LISTED_SITES, siteLivraisonFacturationHandler);
		}
		
		private function getRevendeurs():void
		{
			_revSrv.fournirRevendeurs(SessionUserObject.singletonSession.POOL.IDPOOL,
				cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT,
				SessionUserObject.singletonSession.POOL.IDREVENDEUR);
			_revSrv.addEventListener(CommandeEvent.LISTED_REVENDEURS, revendeursHandler);
		}
		
		private function getUserRevendeurs():void
		{
			_revSrv.fournirRevendeurs(SessionUserObject.singletonSession.POOL.IDPOOL, 
				0,
				SessionUserObject.singletonSession.POOL.IDREVENDEUR);
			_revSrv.addEventListener(CommandeEvent.LISTED_REVENDEURS, revendeursHandler);
		}
		
		private function getRevendeursAutorises():void
		{
			_revAutorises.fournirRevendeursAutorises(cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT,
				cbxOperateur.selectedItem.OPERATEURID,
				SessionUserObject.singletonSession.POOL.IDREVENDEUR);
			_revAutorises.addEventListener(CommandeEvent.LISTED_REVAUTORISES, revendeursAutorisesHandler);
		}
		
		private function getAgences():void
		{ 
			_revSrv.fournirAgencesRevendeur(cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE);
			_revSrv.addEventListener(CommandeEvent.LISTED_AGENCES, agencesHandler);
		}
		
		private function getSLA():void
		{
			if(cbxRevendeur.selectedItem == null || cbxOperateur.selectedItem == null) return;
			
			_cmdeSrv.fournirRevendeurSLA(cbxRevendeur.selectedItem.IDREVENDEUR, cbxOperateur.selectedItem.OPERATEURID);
			_cmdeSrv.addEventListener(CommandeEvent.LISTED_REVENDEURSLA, slaHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					CHAMPS PERSO
		//--------------------------------------------------------------------------------------------//
		
		private function champsPersoHandler():void
		{
			var i:int = 0;
			
			champs1 		= SessionUserObject.singletonSession.CHAMPS.CHAMPS1;
			champs2 		= SessionUserObject.singletonSession.CHAMPS.CHAMPS2;
			
			if(champs1 != null && champs2 != null)
			{
				LIBELLE_CHAMPS1 = SessionUserObject.singletonSession.CHAMPS.LIBELLE1;
				EXEMPLE_CHAMPS1 = SessionUserObject.singletonSession.CHAMPS.EXEMPLE1;
				CHAMPS1_ACTIF 	= SessionUserObject.singletonSession.CHAMPS.ACTIVE1;
				LIBELLE_CHAMPS2 = SessionUserObject.singletonSession.CHAMPS.LIBELLE2;
				EXEMPLE_CHAMPS2 = SessionUserObject.singletonSession.CHAMPS.EXEMPLE2;
				CHAMPS2_ACTIF 	= SessionUserObject.singletonSession.CHAMPS.ACTIVE2;
			}
			
			if(EXEMPLE_CHAMPS1 == "")
				CHAMPS1_EXEMPLE_VISIBLE = false;
			else
				CHAMPS1_EXEMPLE_VISIBLE = true;
			
			if(EXEMPLE_CHAMPS2 == "")
				CHAMPS2_EXEMPLE_VISIBLE = false;
			else
				CHAMPS2_EXEMPLE_VISIBLE = true;
			
			if(SessionUserObject.singletonSession.CHAMPS.hbxChamps1 != null)
			{
				var nbrChild1	:int  	= (SessionUserObject.singletonSession.CHAMPS.hbxChamps1 as HBox).numChildren;
				var hbx1		:HBox	= SessionUserObject.singletonSession.CHAMPS.hbxChamps1 as HBox;
				var arrayTemp1	:Array 	= copyChampsPerso((hbx1 as HBox).getChildren());
				var nbrTemp1	:int 	= arrayTemp1.length;
				
				for(i = 0;i < nbrTemp1;i++)
				{
					var libelle1:String = arrayTemp1[i].text;
					hbxChamps1.addChild(arrayTemp1[i]);
					(hbxChamps1.getChildAt(i) as TextArea).text = libelle1;
				}
			}
			
			if(SessionUserObject.singletonSession.CHAMPS.hbxChamps2 != null)
			{
				var nbrChild2	:int  	= (SessionUserObject.singletonSession.CHAMPS.hbxChamps2 as HBox).numChildren;
				var hbx2		:HBox	= SessionUserObject.singletonSession.CHAMPS.hbxChamps2 as HBox;
				var arrayTemp2	:Array 	= copyChampsPerso((hbx2 as HBox).getChildren());
				var nbrTemp2	:int 	= arrayTemp2.length;
				for(i = 0;i < nbrTemp2;i++)
				{
					var libelle2:String = arrayTemp2[i].text;
					hbxChamps2.addChild(arrayTemp2[i]);
					(hbxChamps2.getChildAt(i) as TextArea).text = libelle2;
				}
			}
		}
		
		private function copyChampsPerso(champs:Array):Array
		{
			var newChildren	:ArrayCollection = new ArrayCollection();
			var lenChildren	:int = champs.length;
			
			for(var i:int = 0;i < lenChildren;i++)
			{
				var txtarea:TextArea 	= new TextArea();				
				txtarea.width		= (champs[i] as TextArea).width;
				txtarea.height		= (champs[i] as TextArea).height;
				txtarea.restrict	= (champs[i] as TextArea).restrict;
				txtarea.maxChars	= (champs[i] as TextArea).maxChars;
				txtarea.id			= (champs[i] as TextArea).id;
				txtarea.text		= (champs[i] as TextArea).text;
				txtarea.editable	= (champs[i] as TextArea).editable;
				txtarea.selectable	= (champs[i] as TextArea).selectable;	
				txtarea.addEventListener(Event.CHANGE, textinputChangeHandler);									
				
				if(txtarea != null)
					newChildren.addItem(txtarea);
				else
					newChildren.addItem(champs[i]);
			}
			
			return newChildren.source;
		}
		
		private function textinputChangeHandler(e:Event):void
		{
			(e.currentTarget as TextArea).errorString = "";
		}
		
		private function autoCompleteRefClient():void
		{
			if(SessionUserObject.singletonSession.CHAMPS.AUTO1)
				insertNumeroCommandeInTextbox();
			
			if(SessionUserObject.singletonSession.CHAMPS.AUTO2)
				insertNumeroCommandeInTextbox2();
		}
		
		private function checkChamps():Boolean
		{
			var OK				:Boolean = true;
			var i				:int 	 = 0;
			var childChamps1 	:Array 	 = hbxChamps1.getChildren();
			var childChamps2 	:Array 	 = hbxChamps2.getChildren();
			
			
			if(CHAMPS1_ACTIF)
			{
				for(i = 0;i < champs1.length;i++)
				{
					if(champs1[i].SAISIE_ZONE)
					{
						if((childChamps1[i*2] as TextArea).text == "")
						{
							(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
							
							if(OK)
								OK = false;
						}
						else
						{
							if(champs1[i].EXACT_NBR)
							{
								if((childChamps1[i*2] as TextArea).text.length != champs1[i].COMPTEUR_OBLIG)
								{
									(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs1[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps1[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
					else
					{
						if((childChamps1[i*2] as TextArea).text == "")
						{
							if(OK)
								OK = true;
						}
						else
						{
							if(champs1[i].EXACT_NBR)
							{
								if((childChamps1[i*2] as TextArea).text.length != champs1[i].COMPTEUR_OBLIG)
								{
									(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs1[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps1[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
				}
			}
			
			if(CHAMPS2_ACTIF)
			{
				for(i = 0;i < champs2.length;i++)
				{
					if(champs2[i].SAISIE_ZONE)
					{
						if((childChamps2[i*2] as TextArea).text == "")
						{
							(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
							
							if(OK)
								OK = false;
						}
						else
						{
							if(champs2[i].EXACT_NBR)
							{
								if((childChamps2[i*2] as TextArea).text.length != champs2[i].COMPTEUR_OBLIG)
								{
									(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs2[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps2[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
					else
					{
						if((childChamps2[i*2] as TextArea).text == "")
						{
							if(OK)
								OK = true;
						}
						else
						{
							if(champs2[i].EXACT_NBR)
							{
								if((childChamps2[i*2] as TextArea).text.length != champs2[i].COMPTEUR_OBLIG)
								{
									(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs2[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps2[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
				}
			}
			
			if(OK)
				attributDataToCommandeObject();
			
			return OK;
		}
		
		private function attributDataToCommandeObject():void
		{
			var i				:int 	= 0;
			var childChamps1 	:Array 	= hbxChamps1.getChildren();
			var childChamps2 	:Array 	= hbxChamps2.getChildren();
			var reference1		:String	= "";
			var reference2		:String	= "";
			
			for(i = 0;i < childChamps1.length;i++)
			{
				reference1 = reference1 + childChamps1[i].text;
			}
			
			for(i = 0;i < childChamps2.length;i++)
			{
				reference2 = reference2 + childChamps2[i].text;
			}
			
			_cmd.REF_CLIENT1 = reference1;
			_cmd.REF_CLIENT2 = reference2;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		private function findLibelleOperateur():void
		{
			if(cbxOperateur.selectedItem != null)
			{
				var idop	:int = cbxOperateur.selectedItem.OPERATEURID;
				var len 	:int = _libelleCompteOp.length;
				
				for(var i:int = 0; i < len;i++)
				{
					if(_libelleCompteOp[i].OPERATEURID == idop)
					{
						if(_libelleCompteOp[i].COMPTE != null && _libelleCompteOp[i].COMPTE != '')
							compteName 		= _libelleCompteOp[i].COMPTE;
						else
							compteName 		= ResourceManager.getInstance().getString('M16','Compte');;
						
						if(_libelleCompteOp[i].SOUS_COMPTE != null && _libelleCompteOp[i].SOUS_COMPTE != '')
							souscompteName 	= _libelleCompteOp[i].SOUS_COMPTE;
						else
							souscompteName 	= ResourceManager.getInstance().getString('M16','Sous_compte');
						
						return;
					}
				}	
			}
		}
		
		private function getSousComptes():void
		{
			var listeSSCPTE:ArrayCollection = _cpteSrv.searchSousCompteCorrespondant(cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION);
			
			attributDataToCombobox(cbxPointFacturation, listeSSCPTE, ResourceManager.getInstance().getString('M16', 'Aucun_sous_comptes'));
			
			if(listeSSCPTE.length == 1)
				setNewCompteSousCompteToLigne();
		}
		
		private function insertNumeroCommandeInTextbox():void
		{
			var childChamps1 	:Array 	 = hbxChamps1.getChildren();
			var lenChild	 	:int 	 = childChamps1.length;
			var lenNumCmd		:int 	 = _cmd.NUMERO_COMMANDE.length;
			var cptr			:int	 = -1
			var lenTxtIpt		:int 	 = -1;
			var i				:int 	 = 0;
			
			for(i = 0; i < lenChild; i++)
			{
				cptr = i*2;
				
				if(cptr < lenChild)
				{
					lenTxtIpt = (childChamps1[cptr] as TextArea).maxChars;
					
					if(lenNumCmd <= lenTxtIpt)
					{
						(childChamps1[cptr] as TextArea).text 		= _cmd.NUMERO_COMMANDE;
						(childChamps1[cptr] as TextArea).errorString = "";
					}
				}
			}
		}
		
		private function insertNumeroCommandeInTextbox2():void
		{
			var childChamps2 	:Array 	 = hbxChamps2.getChildren();
			var lenChild	 	:int 	 = childChamps2.length;
			var lenNumCmd		:int 	 = _cmd.NUMERO_COMMANDE.length;
			var cptr			:int	 = -1
			var lenTxtIpt		:int 	 = -1;
			var i				:int 	 = 0;
			
			for(i = 0; i < lenChild; i++)
			{
				cptr = i*2;
				
				if(cptr < lenChild)
				{
					lenTxtIpt = (childChamps2[cptr] as TextArea).maxChars;
					
					if(lenNumCmd <= lenTxtIpt)
					{
						(childChamps2[cptr] as TextArea).text 		= _cmd.NUMERO_COMMANDE;
						(childChamps2[cptr] as TextArea).errorString = "";
					}
				}
			}
		}
		
		private function searchInSiteFacturation(idSite:int):void
		{
			var sites	:ArrayCollection = cbxSitesFacturation.dataProvider as ArrayCollection;
			var len		:int 			 = sites.length;
			var isHere	:Boolean		 = false;
			
			
			for(var i:int = 0;i <len;i++)
			{
				if(sites[i].IDSITE_PHYSIQUE == idSite)
				{
					cbxSitesFacturation.selectedIndex = i;
					isHere = true;
					break;
				}
			}
		}
		
		private function setNewCompteSousCompteToLigne():void
		{
			var cpteSelect	 	:String = '';
			var sscpteSelect 	:String = '';
			var idcpteSelect	:int = 0;
			var idsscpteSelect 	:int = 0;			
			
			if(cbxTitulaire.selectedItem != null)
			{
				cpteSelect   = cbxTitulaire.selectedItem.COMPTE_FACTURATION;
				idcpteSelect = cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION;
			}
			
			if(cbxPointFacturation.selectedItem != null)
			{
				sscpteSelect   = cbxPointFacturation.selectedItem.SOUS_COMPTE;
				idsscpteSelect = cbxPointFacturation.selectedItem.IDSOUS_COMPTE;
			}
			
			var len	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
			
			for(var i:int = 0;i < len;i++)
			{
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS[i].COMPTE_FACTURATION 	= cpteSelect;
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS[i].IDCOMPTE_FACTURATION 	= idcpteSelect;
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS[i].SOUS_COMPTE 			= sscpteSelect;
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS[i].IDSOUS_COMPTE 		= idsscpteSelect;
				
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.itemUpdated(SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS[i]);
			}
		}
		
		//---ATTRIBUT LES DONNEES A LA COMBOBOX PASSEE EN PARAMETRE
		private function attributDataToCombobox(cbx:ComboBox, dataValues:ArrayCollection, msgError:String):void
		{
			var lenDataValues:int = dataValues.length;
			
			cbx.errorString = "";
			
			if(lenDataValues != 0)
			{
				cbx.prompt 					= ResourceManager.getInstance().getString('M16', 'S_lectionnez');
				cbx.dataProvider 			= dataValues;
				cbx.dropdown.dataProvider 	= dataValues;
				
				if(lenDataValues > 1)
					cbx.selectedIndex = -1;
				else
					cbx.selectedIndex = 0;
				
				if(lenDataValues > 5)
					cbx.rowCount = 5;
				else
					cbx.rowCount = lenDataValues;
				
				(cbx.dataProvider as ArrayCollection).refresh();
			}
			else
				cbx.prompt = msgError;
		}
		
		private function addPop():void
		{
			_popupParamSite.addEventListener(GestionSiteEvent.PARAMETRE_SITE_COMPLETE, ficheSiteHandler);
			PopUpManager.addPopUp(_popupParamSite, this, true);
			PopUpManager.centerPopUp(_popupParamSite);
		}
		
		private function setOperationSurLignes():void//special carte sim afficher le site de livraison
		{
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 11)
			{
				boxRevendeur.visible = boxRevendeur.includeInLayout = false;
				cbxSites.visible = boxRevendeur.includeInLayout = true;
				getUserRevendeurs();
			}
			else
				boxRevendeur.visible = boxRevendeur.includeInLayout = true;
		}
		
		//---> 5-> REENGAGEMENT, 6-> MODIFICATION OPTION, 7-> SUSPENSION, 8-> REACTIVATION, 9-> RESILIATION	
		private function setLibelleTypeOperation():void
		{
			switch(SessionUserObject.singletonSession.IDTYPEDECOMMANDE)
			{
				case 5 : libelleTypeOperation = ResourceManager.getInstance().getString('M16', 'R_engagement');			break;
				case 6 : libelleTypeOperation = ResourceManager.getInstance().getString('M16', 'Modification_d_option');break;
				case 7 : libelleTypeOperation = ResourceManager.getInstance().getString('M16', 'Suspension_');			break;
				case 8 : libelleTypeOperation = ResourceManager.getInstance().getString('M16', 'R_activation');			break;
				case 9 : libelleTypeOperation = ResourceManager.getInstance().getString('M16', 'R_siliation');			break;
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					VERIFICATION DES DONNEES
		//--------------------------------------------------------------------------------------------//
		
		private function checkFields():Boolean
		{
			var check	:Boolean = true;
			var isOk	:Boolean = false;
			
			try
			{
				if(boxRevendeur.visible)
				{			
					if(cbxCmdType.selectedItem != null)
						_cmd.IDPROFIL_EQUIPEMENT 	= cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT;
					else
					{
						cbxCmdType.errorString = ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_type_de_commande_');
						check =  false;
					}
					
					if(txtRefOp.text != "")
						_cmd.REF_OPERATEUR 	 = txtRefOp.text;
					else
					{
						txtRefOp.errorString = ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_la_r_f_rence_op_rateur_');
						check =  false;
					}
					
					if(check)
						check = checkChamps();
					else
						checkChamps();
					
					if(cbxSites.selectedItem != null)
					{
						_cmd.LIBELLE_SITELIVRAISON 	= cbxSites.selectedItem.NOM_SITE;
						_cmd.IDSITELIVRAISON 		= cbxSites.selectedItem.IDSITE_PHYSIQUE;
					}
					else
					{
						cbxSites.errorString = ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_site_de_livraison_');
						check =  false;
					}
				}
				else
				{
					if(check)
						check = checkChamps();
					else
						checkChamps();
				}
				
				if(isNoCompte)
				{
					if(cbxTitulaire.selectedItem != null)//COMPTE_FACTURATION - IDCOMPTE_FACTURATION
					{
						_cmd.LIBELLE_COMPTE			= cbxTitulaire.selectedItem.COMPTE_FACTURATION;
						_cmd.IDCOMPTE_FACTURATION	= cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION;
					}
					else
					{
						
						cbxTitulaire.errorString = compteName;
						check =  false;
					}
					
					if(cbxPointFacturation.selectedItem != null)//SOUS_COMPTE - IDSOUS_COMPTE
					{
						_cmd.LIBELLE_SOUSCOMPTE	= cbxPointFacturation.selectedItem.SOUS_COMPTE;
						_cmd.IDSOUS_COMPTE		= cbxPointFacturation.selectedItem.IDSOUS_COMPTE;
					}
					else
					{
						
						cbxPointFacturation.errorString = souscompteName;
						check =  false;
					}
				}
				
				if(cbxPools.selectedItem != null)
				{
					_cmd.IDPOOL_GESTIONNAIRE 	= SessionUserObject.singletonSession.POOL.IDPOOL;
					_cmd.IDPROFIL 				= SessionUserObject.singletonSession.POOL.IDPROFIL;
				}
				else
				{
					cbxPools.errorString = ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_votre_pool_');
					check =  false;
				}
				
				if(cbxRevendeur.selectedItem != null)
				{
					_cmd.LIBELLE_REVENDEUR 		= cbxRevendeur.selectedItem.LIBELLE;
					_cmd.IDSOCIETE 				= cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE;
					_cmd.IDREVENDEUR			= cbxRevendeur.selectedItem.IDREVENDEUR;
				}
				else
				{
					cbxRevendeur.errorString = ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_revendeur_');
					check =  false;
				}
				
				if(cbxAgence.selectedItem == null)
				{
					cbxAgence.errorString = ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_une_agence_');
					check =  false;
				}
				
				if(txtLibelle.text != "")
					_cmd.LIBELLE_COMMANDE   = txtLibelle.text;
				else
				{
					txtLibelle.errorString = ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_le_libell__de_la_command');
					check =  false;
				}
				
				if(lblNumCmd.text != "")
					_cmd.NUMERO_COMMANDE   = lblNumCmd.text;
				else
				{
					lblNumCmd.errorString = ResourceManager.getInstance().getString('M16', 'Pas_de_num_ro_de_commande_');
					check =  false;
				}
				
				if(dcLivraisonPrevue.selectedDate != null)
					_cmd.LIVRAISON_PREVUE_LE 	  = dcLivraisonPrevue.selectedDate;
				else
				{
					dcLivraisonPrevue.errorString = ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_une_date_de_livraison_');
					check =  false;
				}
				if(cbxSites.selectedItem != null)
				{
					_cmd.LIBELLE_SITELIVRAISON 	= cbxSites.selectedItem.NOM_SITE;
					_cmd.IDSITELIVRAISON 		= cbxSites.selectedItem.IDSITE_PHYSIQUE;
				}
				else
				{
					cbxSites.errorString = ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_site_de_livraison_');
					check =  false;
				}
				if(cbxSitesFacturation.selectedItem != null)
				{
					_cmd.LIBELLE_SITEFACTURATION  = cbxSitesFacturation.selectedItem.NOM_SITE;
					_cmd.IDSITEFACTURATION		 = cbxSitesFacturation.selectedItem.IDSITE_PHYSIQUE;
				}
				else
				{
					cbxSitesFacturation.errorString = ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_site_de_facturation__');
					check =  false;
				}
				
				if(check)
				{
					_cmd.IDOPERATEUR		= cbxOperateur.selectedItem.OPERATEURID;
					_cmd.IDGESTIONNAIRE		= CvAccessManager.getSession().USER.CLIENTACCESSID;
					_cmd.DATE_COMMANDE 		= _date; 
					_cmd.ENCOURS 			= 1;
					
					if(txtbxTo.text == ResourceManager.getInstance().getString('M16','nom_pr_nom___n__d_appel__mail'))
						_cmd.LIBELLE_TO 		= "";
					else
						_cmd.LIBELLE_TO		= txtbxTo.text;
				}
				
				_cmd.V1 = _cmd.REF_CLIENT1;
				_cmd.V2 = _cmd.REF_CLIENT2;
				
				isOk = check;
			}
			catch(e:Error)
			{
				isOk = false;
			}
			
			return isOk;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					MAPPING
		//--------------------------------------------------------------------------------------------//
		
		private function mappingSiteSelectedToSite():Site
		{
			var site:Site 			= new Site();
			site.cp_site 			= cbxSites.selectedItem.SP_CODE_POSTAL;
			site.adr_site 			= cbxSites.selectedItem.ADRESSE;
			site.id_site			= cbxSites.selectedItem.IDSITE_PHYSIQUE;
			site.ref_site 			= cbxSites.selectedItem.SP_REFERENCE;
			site.code_interne_site 	= cbxSites.selectedItem.SP_CODE_INTERNE;
			site.commentaire_site 	= cbxSites.selectedItem.SP_COMMENTAIRE;
			site.commune_site 		= cbxSites.selectedItem.SP_COMMUNE;
			site.libelle_site 		= cbxSites.selectedItem.NOM_SITE;
			site.idpays_site		= cbxSites.selectedItem.PAYCONSOTELID;
			site.IS_FACTURATION		= Formator.formatInteger(cbxSites.selectedItem.IS_FACTURATION);
			site.IS_LIVRAISON		= Formator.formatInteger(cbxSites.selectedItem.IS_LIVRAISON);
			return site;
		}
	}
}