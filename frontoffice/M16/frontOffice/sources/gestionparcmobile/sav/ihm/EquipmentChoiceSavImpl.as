package gestionparcmobile.sav.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.List;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.styles.StyleManager;
	import mx.utils.ObjectUtil;
	
	import composants.colorDatagrid.ColorDatagrid;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.DetailsDeviceEvent;
	import gestioncommande.popup.DetailProduitIHM;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.Formator;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class EquipmentChoiceSavImpl extends Box
	{
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cboxEngagement			:ComboBox;	
		
		public var rdbtnParc				:RadioButton;
		public var rdbtnStock				:RadioButton;
		
		public var txtptSearch				:TextInput;
		
		public var dgListTerminal			:ColorDatagrid;
		
		public var boxVisible				:Box;	
		public var boxEngagementVisible		:Box;
		
		public var list_terminaux			:List;
		public var img_view_list			:Image;
		public var img_view_smart			:Image;
		
		public var glow_0:GlowFilter = new GlowFilter();
		public var glow_1:GlowFilter = new GlowFilter();
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmd					:Commande;
		
		private var _myElements				:ElementsCommande2	= new ElementsCommande2();
		
		private var _mobile					:EquipementsElements = null;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _equipementsSrv			:EquipementService = new EquipementService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var terminaux				:ArrayCollection = new ArrayCollection();
		public var listEngagement			:ArrayCollection = new ArrayCollection();
		
		private var _isTerSimEngReturn		:Boolean = false;
		
		private var _selectEgagement		:Boolean = true;		
		
		private var _idOpe					:int = -1;
		private var _idGes					:int = -1;
		private var _idRev					:int = -1;
		private var _idPro					:int = -1;
		private var _idTyp					:int = -1;
		
		private var selectedEquipement		:EquipementsElements;
		
		//--------------------------------------------------------------------------------------------//
		//					CONCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE					:int = 2;
		public var ACCESS					:Boolean = false;
		
		public var bool_viewsmart			:Boolean = true;
		public var bool_viewsmartVis			:Boolean = true;
		
		
		public function EquipmentChoiceSavImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
		public function validateCurrentPage():Boolean
		{
			return checkFields();
		}
		
		public function reset():void
		{
			listEngagement.removeAll();
			terminaux.removeAll();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		protected function cboxEngagementChangeHandler(e:Event):void
		{
			if(cboxEngagement.selectedItem != null)
			{
				boxVisible.visible = true;
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.ENGAGEMENT.fill(cboxEngagement.selectedItem);
			}
			else
				boxVisible.visible = false;
			
			if(terminaux.length > 0)
				terminaux.itemUpdated(terminaux[0]);
			
			calculerPrix();
		}
		
		private function calculerPrix():void
		{
			for (var i:int = 0; i < terminaux.length; i++) 
			{
				if(terminaux[i].IDEQUIPEMENT == 0) 
					terminaux[i].PRIX_S = '';
				
				if(!boxEngagementVisible.visible) 
				{
					terminaux[i].PRIX_S 	= Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_C));
					terminaux[i].PRIX 	= Number(terminaux[i].PRIX_C);
				}
				
				if(cboxEngagement.selectedItem != null)
				{
					switch(cboxEngagement.selectedItem.CODE)
					{
						case "EDM": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_2));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_2);
							break;
						case "EVQM":	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_3));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_3);
							break;
						case "ETSM": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_4));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_4);
							break;
						case "EQHM": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_5));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_5);
							break;
						case "EDMAR": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_6));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_6);
							break;
						case "EVQMAR":	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_7));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_7);
							break;
						case "ETSMAR": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_8));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_8);
							break;
						case "EQHMAR": 	terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_9));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_9);
							break;
						case "EVQMFCP": terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_3));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_3);
							break;
						default :		terminaux[i].PRIX_S = Formator.formatTotalWithSymbole(Number(terminaux[i].PRIX_C));
							terminaux[i].PRIX = Number(terminaux[i].PRIX_C);
							break;
					}
				}
				
				terminaux.itemUpdated(terminaux[i]);
			}
		}
		
		protected function dgListTerminalClickHandler(me:MouseEvent):void
		{
			if(me == null) return;
			
			terminalSelected();
		}
		
		protected function txtptSearchHandler(e:Event):void
		{
			if(dgListTerminal.dataProvider != null)
			{
				(dgListTerminal.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListTerminal.dataProvider as ArrayCollection).refresh();
			}
		}
		
		protected function rdbtnStockChangeHandler():void
		{
			if (rdbtnStock.selected)
			{
				rdbtnParc.selected=false;
				rdbtnStock.selected=true;
				bool_viewsmartVis=true;
				bool_viewsmart = true;
				img_view_smart.filters = [glow_1];
				img_view_list.filters = [glow_0];
				getTerminauxSimCardAndEngagements();
			}	
		}
		
		protected function rdbtnParcChangeHandler():void
		{
			if (rdbtnParc.selected)
			{
				rdbtnParc.selected=true;
				rdbtnStock.selected=false;
				bool_viewsmartVis=false;
				bool_viewsmart = false;
				img_view_smart.filters = [glow_0];
				img_view_list.filters = [glow_1];
				getTerminaux();
			}
			
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODE PRIVATE
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER IHM
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener("DETAILS", detailsTerminalSelected);
			
			addEventListener(DetailsDeviceEvent.MY_DETAILS_DEVICE, detailsTerminalSelectedSmart);
			addEventListener('REFRESH_ITEM_SELECTED', refreshListDevice );
			
			glow_1.color = StyleManager.getColorName('#558ed5');
			glow_1.alpha = 0.8;
			glow_1.blurX = 4;
			glow_1.blurY = 4;
			glow_1.strength = 6;
			glow_1.quality = BitmapFilterQuality.HIGH;
			
			glow_0.color = StyleManager.getColorName('#FFFFFF');
			glow_0.alpha = 0.8;
			glow_0.blurX = 4;
			glow_0.blurY = 4;
			glow_0.strength = 6;
			glow_0.quality = BitmapFilterQuality.HIGH;
			
			img_view_smart.filters = [glow_1];
			img_view_list.filters = [glow_0];
		}
		
		protected function refreshListDevice(event:Event):void
		{
			if(list_terminaux!=null && list_terminaux.selectedItem != null)
				selectedEquipement = list_terminaux.selectedItem as EquipementsElements;
			for each (var obj:EquipementsElements in terminaux.source) 
			{
				if(obj.IDFOURNISSEUR != selectedEquipement.IDFOURNISSEUR)
				{
					obj.SELECTED = false;
				}
			}
			
		}
		
		private function showHandler(fe:FlexEvent):void
		{
			_cmd		= SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			if(_idOpe != _cmd.IDOPERATEUR || _idRev != _cmd.IDREVENDEUR || _idPro != _cmd.IDPROFIL_EQUIPEMENT || listEngagement.length == 0)
			{
				getTerminauxSimCardAndEngagements();
				
				_idOpe = _cmd.IDOPERATEUR;
				_idRev = _cmd.IDREVENDEUR;
				_idPro = _cmd.IDPROFIL_EQUIPEMENT;
			}
			
			if(SessionUserObject.singletonSession.IDSEGMENT == 1)
			{
				boxEngagementVisible.visible = boxEngagementVisible.includeInLayout = _selectEgagement = false;
				
				if(cboxEngagement.selectedItem == null)
					boxVisible.visible = false;
				else
					boxVisible.visible = true;
			}
			else
			{
				boxEngagementVisible.visible = boxEngagementVisible.includeInLayout = _selectEgagement = false;
				boxVisible.visible = true;
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM
		//--------------------------------------------------------------------------------------------//
		
		private function detailsTerminalSelected(e:Event):void
		{
			if(dgListTerminal.selectedItem != null)
			{
				var popUpDetails:DetailProduitIHM 	= new DetailProduitIHM();
				popUpDetails.equipement			= dgListTerminal.selectedItem;
				popUpDetails.addEventListener('REFRESH_ITEM_SELECTED', refreshListDevice );
				PopUpManager.addPopUp(popUpDetails, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}
		
		protected function detailsTerminalSelectedSmart(evtDetails:DetailsDeviceEvent):void
		{
			if(evtDetails.selectedEquipementsElements !=null)
			{
				var popUpDetails:DetailProduitIHM 	= new DetailProduitIHM();
				popUpDetails.equipement			= evtDetails.selectedEquipementsElements;
				popUpDetails.addEventListener('REFRESH_ITEM_SELECTED', refreshListDevice );
				PopUpManager.addPopUp(popUpDetails, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					EVENEMENTS SLIDE
		//--------------------------------------------------------------------------------------------//
		
		private function checkFields():Boolean
		{
			var isOk:Boolean = false;
			
			for(var i:int = 0;i < terminaux.length;i++)
			{
				if(terminaux[i].SELECTED == true)
				{
					_mobile = terminaux[i];
					
					_cmd.IDEQUIPEMENTPARENT = _mobile.IDFOURNISSEUR;
					
					_myElements.TERMINAUX = new ArrayCollection();
					_myElements.TERMINAUX.addItem(_mobile);
					isOk = true;
					break;
				}
			}
			
			_myElements.ENGAGEMENT = cboxEngagement.selectedItem as Engagement;
			
			return isOk;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getTerminauxSimCardAndEngagements():void
		{
			_isTerSimEngReturn = false;
			
			listEngagement.removeAll();
			terminaux.removeAll();
			
			if(_cmd != null)
			{
				_equipementsSrv.fournirTerminauxSimCardEngagementsForSAV(_cmd.IDPOOL_GESTIONNAIRE, _cmd.IDPROFIL_EQUIPEMENT, _cmd.IDREVENDEUR, _cmd.IDOPERATEUR,
					SessionUserObject.singletonSession.IDSEGMENT, 0, 1);
				_equipementsSrv.addEventListener(CommandeEvent.TERMSIMCARDENGAG, termianuxSimCardAndEngagementsHandler);
			}
		}
		
		private function getTerminaux():void
		{
			_isTerSimEngReturn = false;
			
			listEngagement.removeAll();
			terminaux.removeAll();
			
			if(_cmd != null)
			{
				_equipementsSrv.fournirTerminauxSimCardEngagementsForSAV(_cmd.IDPOOL_GESTIONNAIRE, _cmd.IDPROFIL_EQUIPEMENT, _cmd.IDREVENDEUR, _cmd.IDOPERATEUR,
					SessionUserObject.singletonSession.IDSEGMENT, 0, 0);
				_equipementsSrv.addEventListener(CommandeEvent.TERMSIMCARDENGAG, termianuxSimCardAndEngagementsHandler);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function termianuxSimCardAndEngagementsHandler(cmde:CommandeEvent):void
		{
			_isTerSimEngReturn = true;
			
			if(_equipementsSrv.ENGAGEMENTS.length > 1)
				cboxEngagement.prompt = ResourceManager.getInstance().getString('M16', 'Choisir_la_dur_e_d_engagement');
			else
			{
				if(_equipementsSrv.ENGAGEMENTS.length == 0)
					cboxEngagement.prompt = ResourceManager.getInstance().getString('M16', 'Aucun_engagement');
				else
				{
					cboxEngagement.prompt = "";
					boxVisible.visible = true;
				}
			}
			
			listEngagement = _equipementsSrv.ENGAGEMENTS;
			terminaux = _equipementsSrv.TERMINAUX;
			
			if(cboxEngagement.selectedItem != null)
			{
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.ENGAGEMENT.fill(cboxEngagement.selectedItem);
			}
			
			//checkIfOnly();
			if (rdbtnStock.selected)
				this.setSmartView();
				
		}
		
		// fonction pour afficher les téléphones en viewSmart au debut plutot que la liste
		private function setSmartView():void
		{
			bool_viewsmart = true;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTIONS
		//--------------------------------------------------------------------------------------------//
		
		public function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint			
		{				
			var rColor:uint;				
			rColor = color;				
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			
			if (item != null )				
			{				
				if(item.SUSPENDU_CLT == 1)//En repture
				{	
					rColor = 0xF7FF04;
				}			
				else					
					rColor = color;					
			}				
			return rColor;				
		}
		
		private function checkIfOnly():void
		{
			if(_isTerSimEngReturn)
			{
				if(listEngagement.length == 1 && terminaux.length == 1)
					dispatchEvent(new Event('ONE_ENGAGEMENT_EQUIPEMENT', true));
			}
			
			calculerPrix();
		}
		
		private function addItemEmpty():void
		{
			terminaux.addItemAt(EquipementsElements.formatTerminalEmpty(), 0);
		}
		
		private function terminalSelected():void
		{
			if(dgListTerminal.selectedItem != null)
			{
				var len	:int = terminaux.length;
				var idTsel	:int = (rdbtnParc.selected)?dgListTerminal.selectedItem.IDEQUIPEMENTPARC:dgListTerminal.selectedItem.IDFOURNISSEUR;
				
				
				for(var i:int = 0;i < len;i++)
				{
					terminaux[i].SELECTED = false;
					var idT:int =(rdbtnParc.selected)?terminaux[i].IDEQUIPEMENTPARC:terminaux[i].IDFOURNISSEUR; 
					
					if(idT == idTsel)
						if (terminaux[i].SUSPENDU_CLT != 1)
							terminaux[i].SELECTED = true;
					
					(dgListTerminal.dataProvider as ArrayCollection).itemUpdated(terminaux[i]);
				}
				
				dispatchEvent(new Event('MOBILE_CHANGED', true));
			}
		}
		
		public function dataFieldPrice(equipement:Object, column:DataGridColumn):String
		{				
			if(equipement.IDEQUIPEMENT == 0) return "";
			
			if(!boxEngagementVisible.visible) 
			{
				equipement.PRIX_S 	= Formator.formatTotalWithSymbole(Number(equipement.PRIX_C));
				equipement.PRIX 	= Number(equipement.PRIX_C);
				return equipement.PRIX_S;
			}
			
			if(cboxEngagement.selectedItem != null)
			{
				switch(cboxEngagement.selectedItem.CODE)
				{
					case "EDM": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_2));
						equipement.PRIX = Number(equipement.PRIX_2);
						break;
					case "EVQM":	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_3));
						equipement.PRIX = Number(equipement.PRIX_3);
						break;
					case "ETSM": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_4));
						equipement.PRIX = Number(equipement.PRIX_4);
						break;
					case "EQHM": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_5));
						equipement.PRIX = Number(equipement.PRIX_5);
						break;
					case "EDMAR": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_6));
						equipement.PRIX = Number(equipement.PRIX_6);
						break;
					case "EVQMAR":	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_7));
						equipement.PRIX = Number(equipement.PRIX_7);
						break;
					case "ETSMAR": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_8));
						equipement.PRIX = Number(equipement.PRIX_8);
						break;
					case "EQHMAR": 	equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_9));
						equipement.PRIX = Number(equipement.PRIX_9);
						break;
					case "EVQMFCP": equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_7));
						equipement.PRIX = Number(equipement.PRIX_7);
						break;
					default :		equipement.PRIX_S = Formator.formatTotalWithSymbole(Number(equipement.PRIX_C));
						equipement.PRIX = Number(equipement.PRIX_C);
						break;
				}
			}
			
			return equipement.PRIX_S;
		}
		
		public function dataFieldPriceCatalogue(equipement:Object, column:DataGridColumn):String
		{				
			if(equipement.IDEQUIPEMENT == 0) return "";
			
			equipement.PRIX_CS 	= Formator.formatTotalWithSymbole(equipement.PRIXC);
			
			return equipement.PRIX_CS;
		}
		
		public function sortPrice(itemA:Object, itemB:Object):int
		{
			var priceA:Number = itemA.PRIX;
			var priceB:Number = itemB.PRIX;
			
			return ObjectUtil.numericCompare(priceA, priceB);
		}
		
		public function sortPriceCatalogue(itemA:Object, itemB:Object):int
		{
			var priceA:Number = itemA.PRIXC;
			var priceB:Number = itemB.PRIXC;
			
			return ObjectUtil.numericCompare(priceA, priceB);
		}
		
		protected function onSmartViewClickHandler(event:MouseEvent):void
		{
			bool_viewsmart = true;
			
			img_view_smart.filters = [glow_1];
			img_view_list.filters = [glow_0];
		}
		
		protected function onListViewClickHandler(event:MouseEvent):void
		{
			bool_viewsmart = false;
			
			img_view_smart.filters = [glow_0];
			img_view_list.filters = [glow_1];
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//	
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && 	( 
				(item.NIVEAU != null && item.NIVEAU.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
				||
				(item.REF_REVENDEUR != null && item.REF_REVENDEUR.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
				||
				(item.LIBELLE != null && item.LIBELLE.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
				||
				(item.PRIX_S != null && item.PRIX_S.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
			);
			
			return rfilter;
		}
		
	}
}

