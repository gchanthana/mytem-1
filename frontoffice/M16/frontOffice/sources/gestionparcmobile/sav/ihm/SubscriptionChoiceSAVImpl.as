package gestionparcmobile.sav.ihm
{
	import flash.events.Event;
	
	import mx.events.FlexEvent;
	
	import gestioncommande.entity.RessourcesNumber;
	
	import gestionparcmobile.resiliationLigne.ihm.SubscriptionChoiceReengagementImpl;
	
	import session.SessionUserObject;
	
	public class SubscriptionChoiceSAVImpl extends SubscriptionChoiceReengagementImpl
	{
		public function SubscriptionChoiceSAVImpl()
		{
			super();
		}
		
		private var _previousPageIndex:Number = 0;		
		public function get previousPageIndex():Number{
			return _previousPageIndex;
		}
		
		
		override protected function showHandler(fe:FlexEvent):void
		{
			_cmd		= SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			_previousPageIndex = SessionUserObject.singletonSession.LASTVSINDEX;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			getTypeCommande();
			
			if(SessionUserObject.singletonSession.IDSEGMENT == 1)
			{
				isCmdMobile=true;
			}
			
			if(_idoperator != _cmd.IDOPERATEUR)
			{
				_isRessourcesNumber = false;
				
				_ressNumber = new RessourcesNumber(_cmd.IDOPERATEUR, _cmd.IDPROFIL_EQUIPEMENT);
				_ressNumber.addEventListener('RESSOURCES_NUMBER', ressourcesNumberHandler);
				
				//getEngagements();				
				//checkIfOnly();
				
				// Inutile en SAV: liste des options actuellement sur la ligne				
				/*if((dgListOptionsToErase.dataProvider as ArrayCollection).length == 0)
				{
					numberSelected	= 0;
					getListeOptions();
				}*/

				
				//Inutile en SAV ?:  Récuperation des options par defaut
				/*var typeidcmd:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;				
				if(typeidcmd == 5)
					getDefaultOptions();*/
				
				_idoperator = _cmd.IDOPERATEUR;
			}else{
				checkIfOnly();
			}
	
			
		}
		
		
		override protected function checkIfOnly():void
		{
			//TO-DO GFC-48  pass if no option and no subscription 
			var isPass:Boolean = false;
			// pas d'engagement pour le SAV
			if(/*_isEngagement &&*/ _isRessourcesNumber)
			{
				if(_ressNumber.NB_OPT == 0)
					isPass = true;
				else
					isPass = false;
				
				btnOptions.enabled = !isPass;
				
				
				if(isPass /*&& _eqSrv.listEngagement!= null && _eqSrv.listEngagement.length == 1*/)
						isPass = true;
					else
						isPass = false;
			
			} 
			
			if(isPass)
			{
				dispatchEvent(new Event('ONE_ENGAGEMENT_RESSOURCES', true));
			}
		}
	}	
}