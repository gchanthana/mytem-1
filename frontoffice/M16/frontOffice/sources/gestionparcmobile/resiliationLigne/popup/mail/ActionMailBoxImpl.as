package gestionparcmobile.resiliationLigne.popup.mail
{
	import commandemobile.system.ElementHistorique;
	
	import composants.mail.MailBoxImpl;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	import composants.util.CvDateChooser;
	import composants.util.PopUpImpl;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.registerClassAlias;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.PiecesJointesEvent;
	import gestioncommande.services.Formator;
	import gestioncommande.services.PiecesJointesService;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.TextArea;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.validators.EmailValidator;
	
	[Bindable]
	public class ActionMailBoxImpl extends MailBoxImpl
	{
		/**
	     * Référence vers une autre popUp
	     */
	    protected var _popUp: PopUpImpl;	    
		
		private var _popUpDestinataire:SelectDestinataireIHM = new SelectDestinataireIHM();
		
		private var _selectedAction:Action;
		
		private var _listeDestinataires:String;
		
		private var _selectedFunction:Function;
		private	var fileRef		:FileReference;
	    public var action		:Action	= new Action();
	    public var idsCommande	:Array = null;
		public var numCommande	:String = '';
	    public var poolGestId	:int = 0;
	    public var actionId		:int = 0;
	    public var revendeurId	:int = 0;
	    public var roleId		:int = 0;
	    public var idtypecmd	:int = 0;
	    /**
	     * La date de l'action
	     */
	    public var dcDateAction: CvDateChooser;
	    
	
	    
	    /**
	     * Un commentaire pour l'action.
	     */
	    public var txtCommentaire: TextArea;
	    	    
	   	public var envoyerMail:Boolean;	   	
		
		public var cbMail:CheckBox;
		
		public function ActionMailBoxImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		override protected function creationCompleteHandler(fe:FlexEvent):void
		{	
			PopUpManager.centerPopUp(this);
			imgCancel.addEventListener(MouseEvent.CLICK,imgCancelClickHandler);
			imgContacts.addEventListener(MouseEvent.CLICK,imgContactsClickHandler);
			
			fournirAttachements();
		}
		/**
	     * Si le mode écriture est à false alors on grise tous les boutons de
	     * modifications (Enregistrer, editer ...)
	     */
	    private var _modeEcriture: Boolean = true;
	    
	    /**
	     * setter pour _modeEcriture
	     * 
	     * @param mode
	     */
	    public function set modeEcriture(mode:Boolean): void
	    {
	    	_modeEcriture = mode;
	    }

	    /**
	     * getter pour _modeEcriture
	     */
	    public function get modeEcriture(): Boolean
	    {
	    	return _modeEcriture;		
	    }
	    
	    public function set popUpDestinataire(value:SelectDestinataireIHM):void
		{
			_popUpDestinataire = value;
		}

		public function get popUpDestinataire():SelectDestinataireIHM
		{
			return _popUpDestinataire;
		}
	    
	    public function set selectedAction(value:Action):void
		{
			_selectedAction = value;
			configurerLesDestinatairesDuMail();
		}
		
		public function get selectedAction():Action
		{
			return _selectedAction;
		}
	    
	    protected function removePopUp():void
		{
			if(_popUp != null)
			{
				if(_popUp.isPopUp)
					PopUpManager.removePopUp(_popUp)
				
				_popUp = null;
			}
		}
		
		private var _pjSrv			:PiecesJointesService;
		private var _lastIdCommande	:int = 0;
		
		
		private function fournirAttachements():void
		{			
			_pjSrv = new PiecesJointesService();
			_pjSrv.createUUID();
			_pjSrv.addEventListener(PiecesJointesEvent.FILE_UUID_CREATED, createUUIDHandler);
		}
		
		private function createUUIDHandler(pje:PiecesJointesEvent):void
		{
			infosObject.UUID = _pjSrv.UUID;
		}
		
		
		protected function cbMailClickHandler(event:MouseEvent):void
		{
			if(cbMail.selected)
				currentState = "mailState";
			else
				currentState = "noMailState";
		}
		
		protected function lblJoinClickHandler():void
		{
			fileRef = new FileReference();
			fileRef.browse();
			fileRef.addEventListener(Event.SELECT, 			fileExcelHandler);
			fileRef.addEventListener(Event.COMPLETE, 		fileUploadedHandler);
			fileRef.addEventListener(IOErrorEvent.IO_ERROR, fileNotUploadedHandler);
		}
		
		private function fileExcelHandler(e:Event):void
		{
			try
			{
				var fileSize:uint = fileRef.size;
				if(fileSize <= 5224288)
				{	
					var url:URLRequest = new URLRequest("http://sun-dev-nicolas.consotel.fr/fr/consotel/consoview/inventaire/sncf/processUpload.cfm");
					fileRef.upload(url);
				}
				else
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_fichier_inf_rie'),ResourceManager.getInstance().getString('M16', 'Consoview'),null);
			}
			catch (err:Error)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Impossible_de_joindre_le_fichier__'),ResourceManager.getInstance().getString('M16', 'Consoview'),null);
			}
		}
		
		private function fileUploadedHandler(e:Event):void
		{
			
		}
		
		private function fileNotUploadedHandler(e:Event):void
		{
			ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Impossible_de_joindre_le_fichier__'),ResourceManager.getInstance().getString('M16', 'Consoview'),null);
		}
		
		
		/**
	     * Pour enregistrer l'action à la date du jour
	     * 
	     * @param event
	     */
	   	    override protected function btnEnvoyerClickHandler(event:MouseEvent): void
	    {
	    	if(currentState ==  "mailState")
	    	{
		    	_mail.destinataire = popUpDestinataire.listeMails;
		    	
		    	if(_mail.destinataire != "" && _mail.destinataire != null)
		    	{	
		    		var bool:Boolean = false;
		    		if(txtcc.text != "")
		    		{
						bool = checkEmailAdress(txtcc.text);
						if(!bool)
						{
							txtcc.errorString = ResourceManager.getInstance().getString('M16', 'Erreurs_dans_vos_adresses_email_saisies_');
						}
						else
						{
							txtcc.errorString = "";
							if(txtcci.text != "")
			    			{
			    		 		bool = checkEmailAdress(txtcci.text);
				    		 	if(!bool)
				    		 	{
									txtcci.errorString = ResourceManager.getInstance().getString('M16', 'Erreurs_dans_vos_adresses_email_saisies_');
				    		 	}
				    		 	else
				    		 	{
									txtcci.errorString = "";
				    		 	}
				    		 }
				  		}
		    		}
		    		else
		    		{
		    			if(txtcci.text != "")
		    			{
		    		 		bool = checkEmailAdress(txtcci.text);
			    		 	if(!bool)
			    		 	{
								txtcci.errorString = ResourceManager.getInstance().getString('M16', 'Erreurs_dans_vos_adresses_email_saisies_');
			    		 	}
			    		 	else
			    		 	{
								txtcci.errorString = "";
			    		 	}
			    		 }
		    		}
		    		
		    		if(bool)
		    		{
		    			sendMail();
		    		}
		    		else
		    		{
		    			if(txtcc.text == "" && txtcci.text == "")
		    			{
		    				sendMail();
		    			}
		    		}
		    	}
		    	else
		    	{
		    		Alert.buttonWidth = 100;
		    		Alert.show(ResourceManager.getInstance().getString('M16', 'Vous_n_avez_pas_choisi_de_destinataire__'));
		    	}
		    }
		    else
		    {
		    	goToSend();
		    }
	    }
	    
	    private function checkEmailAdress(emails:String):Boolean
	    {
	    	var emailSplited		:Array = emails.split(",");
	    	var resultValidator		:Array = new Array();
	    	var validator:EmailValidator = new EmailValidator();
	    		validator.property = "text";
	    	for(var i:int = 0; i < emailSplited.length;i++)
	    	{
	    		if(emailSplited[i] != "")
	    		{
	    			var adressmail:String 	= emailSplited[i].toString();
	    			var firstIndex:int		= adressmail.indexOf(" ");
	    			if(firstIndex > -1)
	    			{
	    				while(adressmail.indexOf(" ") > -1)
	    				{
	    					adressmail = adressmail.replace(" ","");
	    				}
	    			}
	    			
					resultValidator = EmailValidator.validateEmail(validator, adressmail, "email");
					
					if(resultValidator.length > 0)
					{
						if(resultValidator[0].isError)
							return false;
					}
	    		}
	    	}
			return true;
		}
		
		private function sendMail():void
		{
			_mail.cc = txtcc.text.replace(";",",");
			_mail.bcc = txtcci.text.replace(";",",");
			_mail.module = txtModule.text;			
			_mail.copiePourExpediteur = (cbCopie.selected)?"YES":"NO";
			_mail.copiePourOperateur = "NO";			
			_mail.sujet = txtSujet.text;
			_mail.message = rteMessage.text;
			_mail.repondreA = txtExpediteur.text;
			_mail.type = "text/plain";
//			_mail.fichierJoin = fileName;
					
			if(_mail.destinataire != null)
			{
				goToSend();	
			}
			else
			{
				Alert.buttonWidth = 100;
				Alert.okLabel = ResourceManager.getInstance().getString('M16', 'Fermer');
				Alert.show(ResourceManager.getInstance().getString('M16', 'Le_destinataire_est_obligatoire_'));		
			}
		}
		
		private function goToSend():void
		{
			registerClassAlias("fr.consotel.consoview.util.Mail",composants.mail.MailVO);
			
			if(action.DATE_ACTION == null)
				action.DATE_ACTION = new Date();
			
			action.DATE_ACTION 	= new Date();
    		action.DATE_HOURS	= formatDate(action.DATE_ACTION) + " " + formatHour(action.DATE_ACTION);
			
			var UUID:String = "";
			
			if(infosObject.hasOwnProperty("UUID"))
				UUID = infosObject.UUID;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.commande_sncf",
																				"envoyer2",
																				sendTheMailResultHandler);
																			
			RemoteObjectUtil.callService(op,_mail,
											idsCommande,
											idtypecmd,
											action,
											Formator.formatBoolean(cbMail.selected),
											(infosObject.commande as Commande).IDOPERATEUR,
											UUID,
											numCommande);
		}
		
		private function formatDate(dateobject:Object):String
		{
			var day:String = dateobject.date;
			if(day.length < 2)
			{day = "0" + day;}
			var month:int = Number(dateobject.month) + 1;
			var temp:String = month.toString();
			if(temp.length < 2)
			{temp = "0" + temp;}
			var year:String = dateobject.fullYear;
			return year + "/" + temp + "/" + day;
		}
		
		private function formatHour(dateobject:Date):String
		{
			var hours	:String = dateobject.hours.toString();
			var minutes	:String = dateobject.minutes.toString();
			var seconds	:String = dateobject.seconds.toString();

			return hours + ":" + minutes + ":" + seconds;
		}
		
	    override protected function imgContactsClickHandler(event:MouseEvent):void
		{	
			afficherSelectionContact();
	    }
		
		protected function configurerLesDestinatairesDuMail():void
		{
			removePopUp();
			if(selectedAction)
			{
				var localCode:String = String(_selectedAction.EXP+_selectedAction.DEST);
				
				switch(localCode)
				{
					case "CC"://---> Liste des contacts client pouvant faire l'action 4
					{
						getListeDestinataire(poolGestId, actionId); break;
					}
					case "CR"://--> Liste des contact revendeurs ayant un role "Commande"
					{
						getListeDestinatairesRevendeur(revendeurId, roleId); break;
					}
					case "RC":
					{
						
						if(_selectedAction.IDACTION == 2068)//---> personne ayant fait l'action 'Demande
							getListeUserAyantFaitAction([2067])					
						else//---> Personne ayant fait l'action 'Préparer une commande' et 'Valider et envoyer'							
							getListeUserAyantFaitAction([2066,2070,2116,2216]);
						
						break;
					}			
				}
	
			}
		}
	
		protected function afficherSelectionContact():void
		{	
			if(_popUpDestinataire.dataProvider != null)
			{
				if(_popUpDestinataire.dataProvider.length == 1)
				{
					_popUpDestinataire.contactsSelectionnes = (_popUpDestinataire.dataProvider as ArrayCollection).source;
					_popUpDestinataire.listeMails 			= ConsoviewUtil.ICollectionViewToList(_popUpDestinataire.dataProvider, 'EMAIL', ',');
				}
				else if(_popUpDestinataire.dataProvider.length > 1)
					{
						PopUpManager.addPopUp(_popUpDestinataire, this, true);
						PopUpManager.centerPopUp(_popUpDestinataire);
					}
					else if(_popUpDestinataire.dataProvider.length == 0)
						{
							ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucun_mail_configur_'), 'Consoview', null);
						}
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucun_mail_configur_'), 'Consoview', null);
		}

		public function getListeDestinataire(idPoolGestionnaire:int, idAction:int):void
	    {
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.commande.GestionWorkFlow",
																				"getContactsActSuivante",
																				getListeDestinataireResultEvent);
			RemoteObjectUtil.callService(op,idPoolGestionnaire,
											idAction);
	    }

	    private function getListeDestinataireResultEvent(re:ResultEvent):void
	    {
	    	if(re.result)
	    	{    	
    			_popUpDestinataire.dataProvider = formatConatcts(re.result as ArrayCollection);
    			
				afficherSelectionContact();
    		}
	    	else
	    		Alert.show(ResourceManager.getInstance().getString('M16', 'Pas_de_destinataire_configur__'));
	    }
	    
	    public function getListeDestinatairesRevendeur(idRevendeur:int, idRole:int):void
	    {
			idRole = whatIsTypeCommande(infosObject.commande.IDTYPE_COMMANDE);
			
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur",
																					"getContactsRevendeurWithRole",
																					getListeDestinatairesRevendeurResultEvent);
			RemoteObjectUtil.callService(op,idRevendeur,
											idRole);
	    }
		
		private function whatIsTypeCommande(idtypecmde:int):int
		{
			var idrole:int = 101;
			
			switch(idtypecmde)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	idrole = 101;	break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		idrole = 101;	break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		idrole = 101;	break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		idrole = 101; 	break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: idrole = 201;	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	idrole = 201;	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			idrole = 6; 	break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	idrole = 6; 	break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		idrole = 202; 	break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	idrole = 202; 	break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			idrole = 203; 	break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		idrole = 203; 	break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		idrole = 203; 	break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	idrole = 203; 	break;//--- REACT
			}
			
			return idrole;
		}
	    
	    private function getListeDestinatairesRevendeurResultEvent(re:ResultEvent):void
	    {
	    	if(re.result)    	
			{
				_popUpDestinataire.dataProvider = re.result as ArrayCollection;
				
				afficherSelectionContact();
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M16', 'Pas_de_destinataire_configur__pour_ce_re'));
	    }
	    
		public function getListeUserAyantFaitAction(selectedAction:Array):void
		{
			var dataProvider:ArrayCollection = new ArrayCollection();
			
			if(infosObject.hasOwnProperty("historiqueCommande"))
			{
				var cursor:IViewCursor = (infosObject.historiqueCommande as ArrayCollection).createCursor();
				while(!cursor.afterLast)
				{
					if(ConsoviewUtil.isPresent(cursor.current.IDACTION.toString(),selectedAction))
					{
						if(!ConsoviewUtil.isLabelInArray(cursor.current.EMAIL,"EMAIL",dataProvider.source))
						{
							dataProvider.addItem({NOM:cursor.current.PATRONYME_USERACTION,
								EMAIL:cursor.current.EMAIL})	
						}
						
					}
					cursor.moveNext();
				}
				
				_popUpDestinataire.dataProvider = dataProvider;
				_popUpDestinataire.contactsSelectionnes = dataProvider.source;
				_popUpDestinataire.listeMails = ConsoviewUtil.ICollectionViewToList(dataProvider,"EMAIL",",");
			}
		}		

		private function formatConatcts(values:ICollectionView):ArrayCollection
	    {
	    	var retour : ArrayCollection = new ArrayCollection();
			
			if (values != null)
			{	
				var cursor : IViewCursor = values.createCursor();
				
				while(!cursor.afterLast)
				{
					var ctcObj:Object = new Object();					
					ctcObj.NOM = cursor.current.NOM;	
					ctcObj.EMAIL = cursor.current.LOGIN_EMAIL;
					retour.addItem(ctcObj);		
					cursor.moveNext();
				}
			}
			
			return retour;
	    }
		
	}
}
