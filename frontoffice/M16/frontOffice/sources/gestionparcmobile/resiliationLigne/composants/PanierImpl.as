package gestionparcmobile.resiliationLigne.composants
{
	
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.services.Formator;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.Panel;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class PanierImpl extends Panel
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		public var NOMBRE_CONFIGURATION		:int = 0;
		public var MONTANT_COMMANDE_ENCOURS	:String = "0";
		public var MONTANT_COMMANDE_TOTAL	:String = "0";
		public var MONTANT_IN_COMMANDE		:String = "0";

		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//	
		
		public function PanierImpl()
		{
			NOMBRE_CONFIGURATION = 0;
			MONTANT_IN_COMMANDE = MONTANT_COMMANDE_TOTAL = MONTANT_COMMANDE_ENCOURS = Formator.formatTotalWithSymbole(0);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLICS
	//--------------------------------------------------------------------------------------------//
		
		public function attributMontant():void
		{
			var type:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 5)
			{
				calculReengagement();
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 9)
				{
					calculResiliation()
				}
				else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 6)
					{
						calculReengagement();
					}
					else
					{
						NOMBRE_CONFIGURATION 		= 0;
						MONTANT_COMMANDE_ENCOURS 	= Formator.formatTotalWithSymbole(0);
						MONTANT_COMMANDE_TOTAL 		= Formator.formatTotalWithSymbole(0);
					}
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		private function calculResiliation():void
		{
			var prix	:Number 	= 0;
			var len		:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
			
			var obj:Object = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS;
			
			for(var i:int = 0;i < len;i++)
			{
				prix = prix + Number(SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS[i].FRAIS_FIXE_RESILIATION)
			}
			
			MONTANT_COMMANDE_TOTAL = MONTANT_COMMANDE_ENCOURS = Formator.formatTotalWithSymbole(prix);
		}
		
		private function calculReengagement():void
		{
			var elegibilite	:Object = calculNombreLignesEligible();
			
			addEquipements(SessionUserObject.singletonSession.CURRENTCONFIGURATION, elegibilite.LINESELIGIBLE, elegibilite.LINESNONELIGIBLE);
		}
		
		private function calculNombreLignesEligible():Object
		{
			var eligi	:Object = new Object();
			var len		:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
			var nbEl	:int 	= 0;
			var nbNEl	:int 	= 0;
			
			for(var i:int = 0;i < len;i++)
			{
				if(int(SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS[i].ELIGIBILITE) == 0)
					nbNEl++;
				else
					nbEl++;
			}
			
			eligi.LINESELIGIBLE  	= nbEl;
			eligi.LINESNONELIGIBLE 	= nbNEl;
			
			return eligi;		
		}
		
		private function addEquipements(value:ElementsCommande2, nbEligible:int, nbNonEligible:int):void
		{
			var equi	:ArrayCollection = new ArrayCollection();
			var elts	:Object = new Object();
			var lenTer	:int = value.TERMINAUX.length;
			var lenAcc	:int = value.ACCESSOIRES.length;
			var i		:int = 0;
			var prixN	:Number = 0;
			var prixC	:Number = 0;
			
			for(i = 0;i < lenTer;i++)//--> TERMINAUX
			{
				if(value.TERMINAUX[i].IDEQUIPEMENT > 0)
				{
					elts 			= new Object();
					elts.EQUIPEMENT = value.TERMINAUX[i].EQUIPEMENT;
					elts.LIBELLE 	= value.TERMINAUX[i].LIBELLE;
					elts.MENSUEL	= "";
					elts.PRIX		= Formator.formatTotalWithSymbole(value.TERMINAUX[i].PRIX);
					elts.PRIXCAT	= Formator.formatTotalWithSymbole(value.TERMINAUX[i].PRIXC);
					
					prixN  = prixN + value.TERMINAUX[i].PRIX;
					prixC  = prixC + value.TERMINAUX[i].PRIXC;
					
					equi.addItem(elts);
				}
			}
			
			for(i = 0;i < lenAcc;i++)//--> ACCESSOIRES
			{
				elts 			= new Object();
				elts.EQUIPEMENT = value.ACCESSOIRES[i].EQUIPEMENT;
				elts.LIBELLE 	= value.ACCESSOIRES[i].LIBELLE;
				elts.MENSUEL	= "";
				elts.PRIX		= Formator.formatTotalWithSymbole(value.ACCESSOIRES[i].PRIX);
				elts.PRIXCAT	= Formator.formatTotalWithSymbole(value.ACCESSOIRES[i].PRIXC);
				
				prixN  = prixN + value.ACCESSOIRES[i].PRIX;
				prixC  = prixC + value.ACCESSOIRES[i].PRIXC;
				
				equi.addItem(elts);
			}
			
			addItems(equi, nbEligible, prixN, nbNonEligible, prixC, 0);
		}

		
		private function addItems(value:ArrayCollection, nbEligible:int, prixN:Number, nbNonEligible:int, prixC:Number, prixRessources:Number):void
		{
			var total:Number = (nbEligible*(prixN+prixRessources)) + (nbNonEligible*(prixC+prixRessources));
			
			MONTANT_COMMANDE_TOTAL = MONTANT_COMMANDE_ENCOURS = Formator.formatTotalWithSymbole(total);
		}
		
	}
}