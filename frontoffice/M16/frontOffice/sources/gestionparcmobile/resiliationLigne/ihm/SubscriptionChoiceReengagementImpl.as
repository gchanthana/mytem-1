package gestionparcmobile.resiliationLigne.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.colorDatagrid.ColorDatagrid;
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.RessourcesNumber;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.popup.PopUpChoiceOptionsIHM;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.RessourceService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class SubscriptionChoiceReengagementImpl extends Box
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListOptions		:DataGrid;
		public var dgListOptionsToErase	:ColorDatagrid;
		
		public var cboxEngagement		:ComboBox;
		
		public var chkbxAll				:CheckBox;
		
		public var btnOptions			:Button;
		
		public var txtptSearch			:TextInput;
		
		public var boxAbonnements		:HBox;
		
		public var boxOptions			:Box;
				
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		protected var _popUpOptions		:PopUpChoiceOptionsIHM;

		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		protected var _cmd				:Commande;
		
		protected var _myElements			:ElementsCommande2	= new ElementsCommande2();
		
		protected var _ressNumber			:RessourcesNumber;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		protected var _eqSrv				:EquipementService = new EquipementService();
		
		protected var _resSrv				:RessourceService 	= new RessourceService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listEngagement		:ArrayCollection 	= new ArrayCollection();
		public var listActuelle			:ArrayCollection 	= new ArrayCollection();

		public var isEngagementVisible	:Boolean = false;
		public var isCmdMobile			:Boolean = false;
		
		public var isConflict			:int = 0;
		public var numberSelected		:int = 0
		
		protected var _lignes				:ArrayCollection 	= new ArrayCollection();
		
		protected var _isEngagement		:Boolean = false;
		protected var _isLigneProduitAbo	:Boolean = false;
		protected var _isRessourcesNumber	:Boolean = false;
		
		
		protected var _idoperator			:int = 0;
		
		//--------------------------------------------------------------------------------------------//
		//					CONCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE						:int = 3;
		
		public var ACCESS						:Boolean = false;
				
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function SubscriptionChoiceReengagementImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLICS
	//--------------------------------------------------------------------------------------------//
		
		public function validateCurrentPage():Boolean
		{
			return checkFields();
		}
		
		public function reset():void
		{
			numberSelected 	= 0;
			_idoperator	 	= 0;
			
			listEngagement.removeAll();
			listActuelle.removeAll();
		}		
		
		public function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint
		{
			var rColor:uint;
			rColor = color;
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			if (item != null)
			{
				if(item.LIGNES[0].boolInOrder == 1 )					
					rColor = 0xFFFF99;
				else
					rColor = color;
				
			}	
			return rColor;
		}
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function btnOptionsClickHandler(me:MouseEvent):void//--> OUVRE LA POPUP PERMETTANT DE SELECTIONNER LES OPTIONS
		{
			_popUpOptions 			 = new PopUpChoiceOptionsIHM();
			_popUpOptions.cmd 		 = _cmd;
			_popUpOptions.myElements = _myElements;
			
			_popUpOptions.addEventListener("POPUP_OPTIONS_CLOSED_AND_VALIDATE", closedPopUpOptionsHandler);
			
			PopUpManager.addPopUp(_popUpOptions, this.parent.parent.parent, true);
			PopUpManager.centerPopUp(_popUpOptions);
		}
		
		protected function txtptSearchHandler():void
		{
			if(dgListOptionsToErase.dataProvider != null)
			{
				(dgListOptionsToErase.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListOptionsToErase.dataProvider as ArrayCollection).refresh();
			}
		}
		
		protected function chkbxAllChangeHandler(e:Event):void
		{
			for(var j:int = 0;j < listActuelle.length;j++)
			{
				listActuelle[j].SELECTED = chkbxAll.selected;
				listActuelle.itemUpdated(listActuelle[j]);
			}
			
			compteOptionsToEraseSelected();
		}
		
		protected function dgListOptionsToEraseItemClickHandler(e:Event):void
		{
			if(dgListOptionsToErase.selectedItem != null)
			{
				if(dgListOptionsToErase.selectedItem.SELECTED)
				{
					dgListOptionsToErase.selectedItem.SELECTED = false;
					numberSelected = numberSelected - 1;
					if(chkbxAll.selected)
						chkbxAll.selected	= false;
				}
				else
				{
					dgListOptionsToErase.selectedItem.SELECTED = true;
					numberSelected = numberSelected + 1;
				}
				
				listActuelle.itemUpdated(dgListOptionsToErase.selectedItem);
			}
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODE PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM
		//--------------------------------------------------------------------------------------------//
		
		protected function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener("REMOVE_OPTIONS", removeOptionsHandler);
		}

		protected function showHandler(fe:FlexEvent):void
		{
			_cmd		= SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;

			getTypeCommande();
			
			if(SessionUserObject.singletonSession.IDSEGMENT == 1)
			{
				isCmdMobile=true;
			}

			if(_idoperator != _cmd.IDOPERATEUR)
			{
				_isRessourcesNumber = false;
				
				_ressNumber = new RessourcesNumber(_cmd.IDOPERATEUR, _cmd.IDPROFIL_EQUIPEMENT);
				_ressNumber.addEventListener('RESSOURCES_NUMBER', ressourcesNumberHandler);
				
				if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 6)
				{
					if((dgListOptionsToErase.dataProvider as ArrayCollection).length == 0)
					{
						numberSelected	= 0;
						getListeOptions();
					}
					
					getEngagements();
				}
				else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 5)
				{
					if((dgListOptionsToErase.dataProvider as ArrayCollection).length == 0)
					{
						numberSelected	= 0;
						getListeOptions();
					}
					
					if(SessionUserObject.singletonSession.IDSEGMENT > 1)
					{
						getEngagements();
					}
					else
					{
						isEngagementVisible = false;
						_isEngagement 		= true;
						
						checkIfOnly();
					}
				}
				
				var typeidcmd:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
				
				if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 5)
					getDefaultOptions();
				
				_idoperator = _cmd.IDOPERATEUR;
			}
		}
		
		protected function ressourcesNumberHandler(e:Event):void
		{
			_isRessourcesNumber = true;
			
			checkIfOnly();
		}
		
		protected function removeOptionsHandler(e:Event):void
		{
			if(dgListOptions.selectedItem != null)
			{
				var idopt	:int = dgListOptions.selectedItem.IDCATALOGUE;
				var len		:int = _myElements.OPTIONS.length;
				
				for(var i:int = 0;i < len;i++)
				{
					if(_myElements.OPTIONS[i].IDCATALOGUE == idopt)
					{
						_myElements.OPTIONS.removeItemAt(i);
						break;
					}
				}
			}
		}
		
		protected function closedPopUpOptionsHandler(e:Event):void
		{
			_cmd 		= _popUpOptions.cmd;
			_myElements = _popUpOptions.myElements;
			
			(dgListOptions.dataProvider as ArrayCollection).refresh();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		protected function engagementHandler(cmde:CommandeEvent):void
		{
			_isEngagement = true;
			
			if(_eqSrv.listEngagement.length > 1)
				cboxEngagement.prompt = ResourceManager.getInstance().getString('M16','Choisir_la_dur_e_d_engagement');
			else
			{
				if(_eqSrv.listEngagement.length == 0)
					cboxEngagement.prompt = ResourceManager.getInstance().getString('M16', 'Aucun_engagement');
				else
					cboxEngagement.prompt = "";
			}
			
			cboxEngagement.dataProvider = _eqSrv.listEngagement;
			
			checkIfOnly();
		}
		
		protected function listeOptionsHandler(cmde:CommandeEvent):void
		{
			_isLigneProduitAbo = true;
			
			listActuelle = _resSrv.listeProduitsAbonnement;
			_lignes		 = _resSrv.listeProduitsAbonnementByLines;
			
			checkIfOnly();
		}

		protected function optionsAssoHandler(ce:CommandeEvent):void
		{
			var value	:ArrayCollection = _resSrv.OPT_ASSOCIATED;
			var len		:int = value.length;
			
			if(len > 0)
			{
				for(var i:int = 0;i < len;i++)
				{
					if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.OPTIONS == null)
						SessionUserObject.singletonSession.CURRENTCONFIGURATION.OPTIONS = new ArrayCollection();
					
					value[i].SELECTED = true;
					
					SessionUserObject.singletonSession.CURRENTCONFIGURATION.OPTIONS.addItem(value[i]);
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//

		protected function getEngagements():void
		{
			isEngagementVisible = true;
			
			_isEngagement = false;
			
			_eqSrv.fournirReengagement(_cmd.IDOPERATEUR);
			_eqSrv.addEventListener(CommandeEvent.LISTED_ENGAGEMENTS, engagementHandler);
		}
		
		protected function getListeOptions():void
		{
			_isLigneProduitAbo = false;
			
			_resSrv.fournirLignesProduitAbonnement(addAllSousTete());
			_resSrv.addEventListener(CommandeEvent.LISTED_OPTIONSERASE, listeOptionsHandler);
		}
		
		protected function getDefaultOptions():void
		{
			_resSrv.getassociatedoptions(_cmd.IDPROFIL_EQUIPEMENT, _cmd.IDOPERATEUR);
			_resSrv.addEventListener(CommandeEvent.LISTED_OPT_ASS, optionsAssoHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		protected function checkIfOnly():void
		{
			var isPass:Boolean = false;
			
			if(_isLigneProduitAbo && _isEngagement && _isRessourcesNumber)
			{
				if(_ressNumber.NB_OPT == 0)
					isPass = true;
				else
					isPass = false;
				
				btnOptions.enabled = !isPass;

				if(SessionUserObject.singletonSession.IDSEGMENT > 1)
				{
					if(isPass && _eqSrv.listEngagement!= null && _eqSrv.listEngagement.length == 1)
						isPass = true;
					else
						isPass = false;
				}//TO-DO GFC-48  pass if no option and no subscription 
			} 
			
			if(_cmd.IDTYPE_COMMANDE != TypesCommandesMobile.MODIFICATION_OPTIONS && _cmd.IDTYPE_COMMANDE != TypesCommandesMobile.MODIFICATION_FIXE)
			{
				if(isPass)
					dispatchEvent(new Event('ONE_ENGAGEMENT_RESSOURCES', true));
			}
		}
		
		//---> 5-> REENGAGEMENT, 6-> MODIFICATION OPTION, 7-> SUSPENSION, 8-> REACTIVATION, 9-> RESILIATION
		protected function getTypeCommande():void
		{
			var idtype	:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
			var cmdType	:int = 0;
			
			switch(idtype)
			{
				case 5: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					cmdType = TypesCommandesMobile.RENOUVELLEMENT;
				else
					cmdType = TypesCommandesMobile.RENOUVELLEMENT_FIXE; 
					
					break;
				case 6: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					cmdType = TypesCommandesMobile.MODIFICATION_OPTIONS;
				else
					cmdType = TypesCommandesMobile.MODIFICATION_FIXE; 
					
					break;
				case 7: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					cmdType = TypesCommandesMobile.SUSPENSION;
				else
					cmdType = TypesCommandesMobile.SUSPENSION_FIXE; 
					
					break;
				case 8: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					cmdType = TypesCommandesMobile.REACTIVATION;
				else
					cmdType = TypesCommandesMobile.REACTIVATION_FIXE; 
					
					break;
				case 9: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					cmdType = TypesCommandesMobile.RESILIATION;
				else
					cmdType = TypesCommandesMobile.RESILIATION_FIXE;					
					break;
				case 13: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					cmdType = TypesCommandesMobile.SAV;
				else
					cmdType = TypesCommandesMobile.SAV; 
					break;
			}
			
			_cmd.IDTYPE_COMMANDE = cmdType;
		}
		
		protected function addAllSousTete():Array
		{
			var ids	:ArrayCollection = new ArrayCollection();
			var len	:int = _myElements.CONFIGURATIONS.length;
			
			for(var i:int = 0;i < len;i++)
			{
				ids.addItem( _myElements.CONFIGURATIONS[i].IDSOUS_TETE);
			}
			
			return ids.source;
		}
		
		protected function compteOptionsToEraseSelected():void
		{
			numberSelected = 0;
			
			for(var i:int = 0;i < listActuelle.length;i++)
			{
				if(listActuelle[i].SELECTED)
					numberSelected = numberSelected + 1;
			}
		}	
		
		protected function addAndEraseSearch():Boolean
		{
			var noFound	:Boolean = true;
			var lenOpt	:int = _myElements.OPTIONS.length;
			
			for(var i:int = 0 ;i < lenOpt;i++)
			{
				var idOption	:int = _myElements.OPTIONS[i].IDCATALOGUE;
				var len 		:int = _myElements.LINEOPTIONS.length;
				
				for(var j:int = 0 ;j < len;j++)
				{
					if(_myElements.LINEOPTIONS[j].IDCATALOGUE == idOption)
						return false;
				}
			}
			
			return noFound;
		}
		
		protected function addOptionsEraseSearch():void
		{
			var lenAct:int = listActuelle.length;
			
			_myElements.LINEOPTIONS = new ArrayCollection();

			for(var i:int = 0 ;i < lenAct;i++)
			{
				if(listActuelle[i].SELECTED)
				{
					var len	:int = _lignes.length;
					var id	:int = listActuelle[i].IDCATALOGUE;
					
					for(var j:int = 0 ;j < len;j++)
					{
						if(_lignes[j].IDCATALOGUE == id)
							_myElements.LINEOPTIONS.addItem(_lignes[j]);
					}
				}
			}
		}
		
		protected function setOptionsSelected():void
		{
			var lenConfg	:int = _myElements.CONFIGURATIONS.length;
			var lenLines	:int = _myElements.LINEOPTIONS.length;
			
			for(var i:int = 0;i < lenConfg;i++)
			{
				_myElements.CONFIGURATIONS[i].ADDOPTIONS = _myElements.OPTIONS;
				_myElements.CONFIGURATIONS[i].REMOPTIONS = new ArrayCollection();
				
				var num:String = _myElements.CONFIGURATIONS[i].LIGNE;
				
				for(var j:int = 0;j < lenLines;j++)
				{
					if(_myElements.LINEOPTIONS[j].SOUSTETE == num)
						_myElements.CONFIGURATIONS[i].REMOPTIONS.addItem(_myElements.LINEOPTIONS[j]);
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//
		
		protected function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && 	( (item.LIBELLE != null && item.LIBELLE.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1) 
				|| (item.REFERENCE_PRODUIT != null && item.REFERENCE_PRODUIT.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
			);
			
			return rfilter;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					VALIDATION
		//--------------------------------------------------------------------------------------------//

		protected function checkFields():Boolean
		{
			var isOk:Boolean = true;
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 6)// 6-> MODIFICATION OPTION
			{
				_myElements.LINEOPTIONS = new ArrayCollection();
				
				for(var i:int = 0;i < listActuelle.length;i++)
				{
					if(listActuelle[i].SELECTED)
						_myElements.LINEOPTIONS.addItem(listActuelle[i]);
				}
				
				if(_myElements.LINEOPTIONS.length == 0 && _myElements.OPTIONS.length == 0)
				{
					isOk = false;
				}
				else
				{
					addOptionsEraseSearch();
					
					isOk = addAndEraseSearch();
					
					if(!isOk)
						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Il_existe_un_conflit_entre_les_options__'), 'Consoview', null);
					else
						setOptionsSelected();
				}
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 5)// 5-> REENGAGEMENT, 
			{
				if(SessionUserObject.singletonSession.IDSEGMENT > 1)
				{
					if(_myElements.OPTIONS.length == 0)
					{
						isOk = false;
					}
				}
				
				_myElements.LINEOPTIONS = new ArrayCollection();
				
				for(var j:int = 0;j < listActuelle.length;j++)
				{
					if(listActuelle[j].SELECTED)
						_myElements.LINEOPTIONS.addItem(listActuelle[j]);
				}
				
				addOptionsEraseSearch();
				
				isOk = addAndEraseSearch();
				
				if(!isOk)
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Il_existe_un_conflit_entre_les_options__'), 'Consoview', null);
				else
					setOptionsSelected();
			}
			
			if(isOk && isEngagementVisible)
			{
				if((SessionUserObject.singletonSession.IDSEGMENT == 1) && (cboxEngagement.selectedItem == null))
				{
					isOk = false;
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_engagement_'), 'Consoview', null);
				}
				else if (cboxEngagement.selectedItem != null) 					
					_myElements.ENGAGEMENT = cboxEngagement.selectedItem as Engagement;
			}
			
			return isOk;
		}

	}
}