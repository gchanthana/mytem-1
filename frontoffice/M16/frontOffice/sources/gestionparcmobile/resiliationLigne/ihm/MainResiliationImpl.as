package gestionparcmobile.resiliationLigne.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.containers.ViewStack;
	import mx.controls.Button;
	import mx.controls.TextArea;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.composants.PiecesJointesMinIHM;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.ViewStackEvent;
	import gestioncommande.popup.PopUpConfirmationCommandeIHM;
	import gestioncommande.services.CommandeArticles;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.Formator;
	
	import gestionparcmobile.resiliationLigne.composants.PanierIHM;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class MainResiliationImpl  extends Box
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var vs						:ViewStack;
		
		public var hbxButtonWizzard			:HBox;
		
		public var txtAreaCommentaires		:TextArea;
		
		//---> BOUTON VIEWSTACK
		public var btPre					:Button;
		public var btNext					:Button;
		public var btValid					:Button;
		public var btCancel					:Button;
		
		//--------------------------------------------------------------------------------------------//
		//					ELEMENTS VS
		//--------------------------------------------------------------------------------------------//
		
		public var etapeParametres			:ParametresReengagementIHM;
		
		public var etapeValidation			:ValiationReengagementIHM;

		//--------------------------------------------------------------------------------------------//
		//					COMPONENTS
		//--------------------------------------------------------------------------------------------//
		
		public var composantPanier			:PanierIHM;
		public var piecesJointes			:PiecesJointesMinIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
				
		private var _cmd					:Commande = new Commande(true);
		
		private var _myElements				:ElementsCommande2 = new ElementsCommande2();
				
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmdSrv					:CommandeService = new CommandeService();

		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//

		//---> OBJETS PASSES EN ARGUMENTS
		public var compte					:ArrayCollection = new ArrayCollection();
		public var sousCompte				:ArrayCollection = new ArrayCollection();
		public var compteSousCompte			:ArrayCollection = new ArrayCollection();
	
		public var objectOperateur			:Object = new Object;
		public var objectPool				:Object	= new Object;

		public var isMultiCompte			:Boolean = false;
		public var isMultiSousCompte		:Boolean = false;
		public var isNoCompte				:Boolean = false;
		
		public var fromM16					:Boolean = true;
		
		public var vsIndex					:int = 0;
			
		private var _myLignes				:ArrayCollection = new ArrayCollection();
		
		private var _isRecorded				:Boolean = false;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function MainResiliationImpl()
		{
			_cmd.addEventListener(CommandeEvent.INFOS_COMMANDE, infosCommandeHandler);
			
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		private function infosCommandeHandler(cmde:CommandeEvent):void
		{
			etapeParametres.setCommande();
		}
		
		public function setReferencesClientChild():void
		{
			etapeParametres.setReferencesClientChild();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					BUTTUN WIZZARD - UP
		//--------------------------------------------------------------------------------------------//
		
		protected function btn1ClickHandler(me:MouseEvent):void
		{
			vsIndex = 0;
			
			buttonNavigationEnableDisable();
			buttonWizzardColor();
		}
		
		protected function btn2ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(1);
		}
		
		protected function btn3ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(3);
		}
		
		protected function btn4ClickHandler(me:MouseEvent):void
		{
		}
		
		protected function btn5ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(4);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					BUTTUN WIZZARD - DOWN
		//--------------------------------------------------------------------------------------------//
		
		protected function btPreClickHandler(me:MouseEvent):void
		{
			decompteVs();
			refreshPanier();
		}
		
		protected function btNextClickHandler(me:MouseEvent):void
		{
			var isOk:Boolean = (vs.getChildAt(vsIndex) as Object).validateCurrentPage();

			if(isOk)
				compteVs();
		}
		
		protected function btValidClickHandler(e:Event):void
		{
			if(!_isRecorded)
			{
				if(vsIndex == 4)
				{
					if(etapeValidation.mailCommande.myAction != null && etapeValidation.mailCommande.myAction.IDACTION > 0)
					{
						if(etapeValidation.mailCommande.checkMail())
						{
							if(etapeValidation.mailCommande.myAction.CODE_ACTION == "EMA3T")
							{ 
								var poUpConfir:PopUpConfirmationCommandeIHM = new PopUpConfirmationCommandeIHM();
									poUpConfir.addEventListener(CommandeEvent.SEND_COMMANDE, selectedTypeCommande);
								
								PopUpManager.addPopUp(poUpConfir,this,true);
								PopUpManager.centerPopUp(poUpConfir);
								
							}
							else
								selectedTypeCommande();
						}
					}
					else
						selectedTypeCommande();
				}
			}
			else
				ConsoviewAlert.afficherAlertConfirmation(resourceManager.getString('M16','Commande_d_j__enregistr_e___Voulez_vous_aller_sur_la'), 'Consoview', commandeIsRecorded);
		}
		
		private function currentvalidatedata():void
		{
			if(!validateLignes()) return;
				
			var mylignes :ArrayCollection = new ArrayCollection();
			var lencsc	 :int = compteSousCompte.length;
			
			SessionUserObject.singletonSession.COMMANDEARTICLE = new ArrayCollection();
			
			for(var i:int = 0;i < lencsc;i++)
			{
				var lignes	:ArrayCollection = compteSousCompte[i].MULTI_SOUS_COMPTE;
				var lenLi	:int = lignes.length;
				
				for(var j:int = 0;j < lenLi;j++)
				{
					mylignes.addItem(lignes[j].LIGNES);
				}
			}
			
			findLignesSeleceted(mylignes);
		}
		
		private function validateLignes():Boolean
		{
			var isOk:Boolean = false;
			
			if((vs.getChildAt(vsIndex) as Object).validateCurrentPage())
			{				
				_myLignes = new ArrayCollection();
				
				attributCommandeToCommande();
				
				isOk = true;
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_au_moins_une_ligne'), 'Consoview', null);
			
			return isOk;
		}
		
		private function attributCommandeToCommande():void
		{
			var idFixe:int = 0;
			var idMobi:int = 0;
			var idData:int = 0;
			
			switch(SessionUserObject.singletonSession.IDSEGMENT)//---> 1-> MOBILE, 2-> FIXE, 3-> DATA
			{
				case 1: idFixe = idData = 0; idMobi = 1; break;
				case 2: idFixe = 1; idMobi = idData = 0; break;
				case 3: idFixe = idMobi = 0; idData = 1; break;
			}
			
			_cmd.SEGMENT_DATA 	= idData;
			_cmd.SEGMENT_FIXE 	= idFixe;
			_cmd.SEGMENT_MOBILE = idMobi;
			
			_cmd.DATE_HEURE 	= Formator.formatReverseDate(_cmd.DATE_COMMANDE) + " " + Formator.formatHourConcat(_cmd.DATE_COMMANDE);
			
			_cmd.IDTYPE_COMMANDE = addTypeCommande();
		}
		
		private function addTypeCommande():int
		{
			var idtype_commande:int = -1;
			//case 2 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 5;	break;//--> REENGAGEMENT
			//case 3 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 6;	break;//--> MODIFICATION OPTION
			//case 4 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 7;	break;//--> SUSPENSION
			//case 5 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 8;	break;//--> REACTIVATION
			//case 6 : SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 9;	break;//--> RESILIATION
			if(SessionUserObject.singletonSession.IDSEGMENT == 1)
			{
				switch(SessionUserObject.singletonSession.IDTYPEDECOMMANDE)
				{
					case 5 : idtype_commande = TypesCommandesMobile.RENOUVELLEMENT;			break;
					case 6 : idtype_commande = TypesCommandesMobile.MODIFICATION_OPTIONS;	break;
					case 7 : idtype_commande = TypesCommandesMobile.SUSPENSION;				break;
					case 8 : idtype_commande = TypesCommandesMobile.REACTIVATION;			break;
					case 9 : idtype_commande = TypesCommandesMobile.RESILIATION;			break;
				}
			}
			else
			{
				switch(SessionUserObject.singletonSession.IDTYPEDECOMMANDE)
				{
					case 5 : idtype_commande = TypesCommandesMobile.RENOUVELLEMENT_FIXE;	break;
					case 6 : idtype_commande = TypesCommandesMobile.MODIFICATION_FIXE;		break;
					case 7 : idtype_commande = TypesCommandesMobile.SUSPENSION_FIXE;		break;
					case 8 : idtype_commande = TypesCommandesMobile.REACTIVATION_FIXE;		break;
					case 9 : idtype_commande = TypesCommandesMobile.RESILIATION_FIXE;		break;
				}
			}
			
			return idtype_commande;
		}
		
		private function findLignesSeleceted(values:ArrayCollection):void
		{
			var lenSel:int = _myLignes.length;
			
			for(var i:int = 0;i < lenSel;i++)
			{
				var num	:String = _myLignes[i].LIGNE;
				var len	:int = values.length;
				
				for(var j:int = 0;j < len;j++)
				{
					var lignes		:ArrayCollection = values[j];
					var lenLignes	:int = lignes.length;
					
					for(var k:int = 0;k < lenLignes;k++)
					{
						if(lignes[k].LIGNE == num)
							lignes[k] = _myLignes[i];
					}
				}
			}
			
			attributDataToCommande(values);
		}
		
		private function attributDataToCommande(values:ArrayCollection):void
		{
			var len	 :int = values.length;
			var cptr :int = 1;
			
			for(var i:int = 0;i < len;i++)
			{
				var lignes		:ArrayCollection = values[i];
				var lenLignes	:int = lignes.length;
				var linesSelect	:ArrayCollection = new ArrayCollection();
				
				for(var j:int = 0;j < lenLignes;j++)
				{
					if(lignes[j].SELECTED)
					{
						lignes[j].ADDOPTIONS = new ArrayCollection();
						lignes[j].REMOPTIONS = new ArrayCollection();
						
						linesSelect.addItem(lignes[j]);
					}
				}
				
				var nbrSelect:int = linesSelect.length;
				
				if(nbrSelect > 0)
				{
					attributOptionsToLines(linesSelect);
					
					var myCmde:Commande 			= _cmd.copyCommande();
					
					if(!isNoCompte)
					{
						myCmde.IDCOMPTE_FACTURATION = linesSelect[0].IDCOMPTE_FACTURATION;
						myCmde.IDSOUS_COMPTE		= linesSelect[0].IDSOUS_COMPTE;
					}
					
					addSuffixe(myCmde, cptr);
						
					cptr++;
					
					var myCommandeObject:Object = new Object();
						myCommandeObject.cmd 	= myCmde.copyCommande();
						
					if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE > 5)
						myCommandeObject.articles	= addArticlesRessources(linesSelect);
					else
						myCommandeObject.articles	= addArticles(linesSelect);
						
					SessionUserObject.singletonSession.COMMANDEARTICLE.addItem(myCommandeObject);
				}
			}
			
			sendCommande();
		}
		
		private function attributOptionsToLines(lines:ArrayCollection):void
		{
			var elemtsSelected	:ArrayCollection = _myElements.LINEOPTIONS;
			var lenSelected		:int = lines.length;
			var lenOptions		:int = elemtsSelected.length;
			
			for(var i:int = 0; i < lenSelected;i++)
			{
				var numero:String = lines[i].LIGNE;
				
				lines[i].ADDOPTIONS = ObjectUtil.copy(_myElements.OPTIONS) as ArrayCollection;
				
				for(var j:int = 0; j < lenOptions;j++)
				{
					if(elemtsSelected[j].SOUSTETE == numero)
					{
						(lines[i].REMOPTIONS as ArrayCollection).addItem(elemtsSelected[j]);
					}
				}
			}
		}
		
		private function addSuffixe(myCmdCopy:Commande, compteur:int):void
		{
			var libelle:String = '';
			
			if(compteur.toString().length == 1)
			{
				libelle = ' - 00' + compteur.toString();
			}
			else if(compteur.toString().length == 2)
				{
					libelle = ' - 0' + compteur.toString();
				}
				else if(compteur.toString().length == 0)
					{
						libelle = ' - ' +compteur.toString();
					}
			
			myCmdCopy.NUMERO_COMMANDE  = myCmdCopy.NUMERO_COMMANDE  + libelle;
			myCmdCopy.LIBELLE_COMMANDE = myCmdCopy.LIBELLE_COMMANDE + libelle;
		}

		private function addArticles(values:ArrayCollection):Array
		{
			var cart		:CommandeArticles = new CommandeArticles();
			var xmlArray	:ArrayCollection = new ArrayCollection();
			var len			:int = values.length;
			var isMulti		:Boolean = isMultiLignesEligible(values);
			
			for(var i:int = 0;i < len;i++)
			{
				var isElibible	:Boolean = true;
				var idConfig	:int = 1;
				
				if(isMulti)
				{
					if(values[i].ELIGIBILITE > 0)
					{
						isElibible = true;
						idConfig = 1;
					}
					else
					{
						isElibible = false;
						idConfig = 2;
					}	
				}
				else
				{
					idConfig = 1;
					
					if(values[i].ELIGIBILITE == 0)
						isElibible = false;
					else
						isElibible = true;
				}

				var xmlEq		:XML = cart.addEquipements(_myElements, isElibible);
				var xmlRs		:XML = cart.addRessourcesByLignes(values[i].ADDOPTIONS, values[i].REMOPTIONS);
//				var xmlRs		:XML = cart.addRessources(_myElements);
				var articles	:XML = <articles></articles>;
				var article		:XML = <article></article>;
				
				article.appendChild(xmlEq);
				article.appendChild(xmlRs);

				article = cart.addConfiguration(article,  formatToConfiguration(values[i]), _myElements.ENGAGEMENT, idConfig);
				
				articles.appendChild(article);
				
				xmlArray.addItem(articles);
			}
			
			return xmlArray.source;
		}
		
		private function isMultiLignesEligible(values:ArrayCollection):Boolean
		{
			var isMulti		:Boolean = false;
			var nbrEli		:int = 0;
			var nbrNonEli	:int = 0;
			var len			:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(values[i].ELIGIBILITE == 0)
					nbrNonEli++;
				else
					nbrEli++;
			}
			
			if(nbrEli > 0 && nbrNonEli > 0)
			{
				isMulti = true;
			}
			
			return isMulti;
		}
		
		private function addArticlesRessources(values:ArrayCollection):Array
		{
			var cart		:CommandeArticles = new CommandeArticles();
			var xmlArray	:ArrayCollection = new ArrayCollection();
			var len			:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				var articles :XML = <articles></articles>;
				var article	 :XML = <article></article>;
				var xmlRs	 :XML = cart.addRessourcesByLignes(values[i].ADDOPTIONS, values[i].REMOPTIONS);
				
				if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 9)
					xmlRs = addPriceResiliation(xmlRs, values[i].FRAIS_FIXE_RESILIATION);
	
				article.appendChild(xmlRs);
				
				article = cart.addConfiguration(article,  formatToConfiguration(values[i]), _myElements.ENGAGEMENT, 1);
				
				articles.appendChild(article);
				
				xmlArray.addItem(articles)
			}
			
			return xmlArray.source;
		}
		
		private function addPriceResiliation(ressources:XML, prixResiliation:Number):XML
		{
			var children :XMLList = ressources.children();
			var lenChild :int = children.length();
			
			for(var i:int = 0;i < lenChild;i++)
			{
				children[i].prix[0] = prixResiliation;
				break;
			}
			
			return ressources;
		}
		
		private function formatToConfiguration(value:Object):ConfigurationElements
		{
			var config:ConfigurationElements 	= new ConfigurationElements();
				config.COLLABORATEUR 			= value.COLLABORATEUR;
				config.ID_COLLABORATEUR 		= value.IDEMPL;
				config.PORTABILITE.NUMERO		= value.LIGNE;
				config.PORTABILITE.IDSOUSTETE 	= value.IDSOUS_TETE;
				config.LIBELLE					= value.COLLABORATEUR;
			
			return config;
		}
		
		protected function btCancelClickHandler(me:MouseEvent):void
		{
			returnToListeCommandes(false);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					GESTION DU WIZZARD
		//--------------------------------------------------------------------------------------------//
		
		private function compteVs():void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			for(var i:int = vsIndex+1;i < lenChild;i++)
			{
				if((children[i] as Object).ACCESS)
				{
					vsIndex = i;
					break;
				}
			}
			
			enabledDisableButtonWizzard();
		}
		
		private function decompteVs():void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			for(var i:int = vsIndex-1;i > -1;i--)
			{
				if((children[i] as Object).ACCESS)
				{
					vsIndex = i;
					break;
				}
			}
			
			enabledDisableButtonWizzard();
		}
		
		private function enabledDisableButtonWizzard():void
		{
			var children	:Array = hbxButtonWizzard.getChildren();
			var lenChild	:int = children.length;
			var idPage		:int = (vs.getChildAt(vsIndex) as Object).IDPAGE;
			var isCurrent	:Boolean = false;
			
			for(var i:int = lenChild;i > -1;i--)
			{
				if(children[i] as Button)
				{
					var myButton:Button = children[i] as Button;
					var idButton:Number = Number(myButton.id.replace('btn',''));
					
					if(idButton <= idPage)
					{
						var childrenVs	:Array = vs.getChildren();
						
						for(var j:int = vsIndex;j > -1;j--)
						{
							if((childrenVs[j] as Object).IDPAGE == idButton)
								(children[i] as Button).enabled = (childrenVs[j] as Object).ACCESS;	
						}
					}
					else
						myButton.enabled = false;
				}
			}
			
			buttonWizzardColor();
			buttonNavigationEnableDisable();
		}
		
		private function buttonWizzardColor():void
		{
			var children	:Array = hbxButtonWizzard.getChildren();
			var lenChild	:int = children.length;
			var idPage		:int = (vs.getChildAt(vsIndex) as Object).IDPAGE;
			
			for(var i:int = 0;i < lenChild;i++)
			{
				if(children[i] as Button)
				{
					var myButton:Button = children[i] as Button;
					var idButton:Number = Number(myButton.id.replace('btn',''));
					
					var childrenVs	:Array = vs.getChildren();
					var lenVs		:int = childrenVs.length;
					
					for(var j:int = 0;j < lenVs;j++)
					{
						if(idButton == 4)
							myButton.styleName = "btnWizzardDesactivate";
						else
						{
							if((childrenVs[j] as Object).IDPAGE == idButton)
							{
								if(idButton == idPage)
									myButton.styleName = "btnWizzardActif";
								else
								{
									if(!(childrenVs[j] as Object).ACCESS)
										myButton.styleName = "btnWizzardDesactivate";
									else
									{
										if(myButton.enabled)
											myButton.styleName = "btnWizzardInactif";
										else
											myButton.styleName = "btnWizzardDesactivate";
									}
								}
							}
						}
					}
				}
			}
		}
		
		private function buttonNavigationEnableDisable():void
		{
			refreshPanier();
			
			if(vsIndex == 0)
			{
				btPre.visible 	= btPre.includeInLayout   = false;
				btNext.visible 	= btNext.includeInLayout  = true;
				btValid.visible = btValid.includeInLayout = false;
			}
			else if(vsIndex > 0)
			{
				if(vsIndex == 4)
				{
					btPre.visible 	= btPre.includeInLayout   = true;
					btNext.visible 	= btNext.includeInLayout  = false;
					btValid.visible = btValid.includeInLayout = true;
				}
				else
				{
					btPre.visible 	= btPre.includeInLayout   = true;
					btNext.visible 	= btNext.includeInLayout  = true;
					btValid.visible = btValid.includeInLayout = false;
				}
			}
		}
		
		private function whatIsTypeCommande():void
		{
			var accessed	:Array = new Array(false, false, false, false, false);
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			var id:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 5)
			{
				if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					accessed = new Array(true, true, true, true, true);
				else
					accessed = new Array(true, false, false, true, true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 6)
			{
				accessed = new Array(true, false, false, true, true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 7)
			{
				accessed = new Array(true, false, false,false, true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 8)
			{
				accessed = new Array(true, false, false, false, true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 9)
			{
				accessed = new Array(true, false, false, false, true);	
			}
			
			for(var i:int = 0;i < lenChild;i++)
			{
				(children[i] as Object).ACCESS = accessed[i];
			}
			
			setIndexToViewStackValidation();
		}
		
		//---> 5-> REENGAGEMENT, 6-> MODIFICATION OPTION, 7-> SUSPENSION, 8-> REACTIVATION, 9-> RESILIATION
		private function setTypeCommande():void
		{
			var idtype	:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
			var cmdType	:int = 0;
			
			switch(idtype)
			{
				case 5: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					cmdType = TypesCommandesMobile.RENOUVELLEMENT;
				else
					cmdType = TypesCommandesMobile.RENOUVELLEMENT_FIXE; 
					
					break;
				case 6: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					cmdType = TypesCommandesMobile.MODIFICATION_OPTIONS;
				else
					cmdType = TypesCommandesMobile.MODIFICATION_FIXE; 
					
					break;
				case 7: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					cmdType = TypesCommandesMobile.SUSPENSION;
				else
					cmdType = TypesCommandesMobile.SUSPENSION_FIXE; 
					
					break;
				case 8: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					cmdType = TypesCommandesMobile.REACTIVATION;
				else
					cmdType = TypesCommandesMobile.REACTIVATION_FIXE; 
					
					break;
				case 9: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
					cmdType = TypesCommandesMobile.RESILIATION;
				else
					cmdType = TypesCommandesMobile.RESILIATION_FIXE; 
					
					break;
			}
			
			_cmd.IDTYPE_COMMANDE = cmdType;
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM
		//--------------------------------------------------------------------------------------------//

		private function creationCompleteHandler(e:Event):void
		{
			SessionUserObject.singletonSession.COMMANDE = _cmd;
			
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			vsIndex = 0;
			
			doVisibleCompteFacturation();
			enabledDisableButtonWizzard();
			buttonNavigationEnableDisable();
			attributObject();
			whatIsTypeCommande();
			setTypeCommande();
			
			addEventListener('SOUS_TETE_ERASED', sousTeteErasedHandler);
			
			//---> PAS D'ACCESSOIRES ALORS ON SAUTE CETTE ETAPE
			addEventListener('NO_ACCESSORIES_NEXT', noAccessoriesHandler);
			addEventListener('NO_ACCESSORIES_PREV', noAccessoriesHandler);
			
			addEventListener('ONE_ENGAGEMENT_EQUIPEMENT', onlyHandler);
			addEventListener('ONE_ENGAGEMENT_RESSOURCES', onlyHandler);
						
			//---> LORSQUE L'ON CHANGE  UN DE CES 5 ELEMENTS ALORS ON BLOQUE LE WIZZARD
			addEventListener('MOBILE_CHANGED', dataCommandeChanged);
			addEventListener('PROFILE_CHANGED', dataCommandeChanged);
			addEventListener('REVENDEUR_CHANGED', dataCommandeChanged);
			addEventListener('TYPE_CMD_CHANGED', dataCommandeChanged);
			addEventListener('VALIDER_ENABLED', enabledBtnValide);
			
			//---> LORSQU'IL FAUT RAFRAICHIR LE PANIER refreshPanier
			addEventListener('REFRESH_PANIER', refreshPanier);
			
			_cmdSrv.addEventListener(CommandeEvent.COMMANDE_CREATED, sendAction);
			_cmdSrv.addEventListener('COMMANDE_ERROR', returnListeCommandes);
			_cmdSrv.addEventListener(CommandeEvent.SEND_ACTION, returnListeCommandes);
		}
		
		private function dataCommandeChanged(e:Event):void
		{
			if(e.type == 'TYPE_CMD_CHANGED')
			{
				var children	:Array = vs.getChildren();
				var lenChild	:int = children.length;
				
				for(var i:int = 0;i < lenChild;i++)
				{
					(children[i] as Object).reset();
				}
				
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX 	= new ArrayCollection();
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.ACCESSOIRES = new ArrayCollection();
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.ABONNEMENTS = new ArrayCollection();
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.OPTIONS 	= new ArrayCollection();
				SessionUserObject.singletonSession.CURRENTCONFIGURATION.LINEOPTIONS = new ArrayCollection();
			}
			
			enabledDisableButtonWizzard();
		}
		
		private function returnListeCommandes(e:Event):void
		{
			returnToListeCommandes(true);
		}
		
		private function selectedTypeCommande(e:Event = null):void
		{
			_cmd.COMMENTAIRES = txtAreaCommentaires.text;
			
			currentvalidatedata();	
		}
		
		private function enabledBtnValide(e:Event):void
		{
			btValid.enabled = true;
		}
		
		private function commandeIsRecorded(ce:CloseEvent):void
		{
			if(ce.detail == 4)
				returnToListeCommandes(true);
		}
		
		private function noAccessoriesHandler(e:Event):void
		{
			if(e.type == 'NO_ACCESSORIES_NEXT')
			{
				compteVs();
			}
			else if(e.type == 'NO_ACCESSORIES_PREV')
				{
					decompteVs();
				}
		}
		
		private function onlyHandler(e:Event):void
		{
			btNextClickHandler(new MouseEvent(MouseEvent.CLICK));
		}
		
		private function sousTeteErasedHandler(e:Event):void
		{
			var bool:Boolean = false;
			
			var lenCompteSousCompte:int = compteSousCompte.length;
			
			for(var i:int = 0;i < lenCompteSousCompte;i++)
			{
				if(compteSousCompte[i].IDCOMPTE_FACTURATION == etapeParametres.IDCOMPTE)
				{
					var lenLignes:int = compteSousCompte[i].LIGNES.length;
					
					for(var j:int = 0;j < lenLignes;j++)
					{
						if(compteSousCompte[i].LIGNES[j].IDSOUS_TETE == etapeParametres.IDSOUTETE)
						{
							(compteSousCompte[i].LIGNES as ArrayCollection).removeItemAt(j);
							break;
						}
					}
					
					if(compteSousCompte[i].LIGNES.length > 0)
					{
						
					}
					else
					{
						compteSousCompte.removeItemAt(i);
						break;
					}
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function sendAction(cmde:CommandeEvent):void
		{
			_isRecorded = true;

			returnToListeCommandes(true);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function sendCommande():void
		{
			if(SessionUserObject.singletonSession.COMMANDEARTICLE.length > 0)
			{
				if(etapeValidation.mailCommande.myAction != null && etapeValidation.mailCommande.myAction.IDACTION > 0)
				{
					etapeValidation.mailCommande.myAction.COMMENTAIRE_ACTION = etapeValidation.txaCommentaires.text;
					etapeValidation.mailCommande.myAction.DATE_ACTION 		 = new Date();
					etapeValidation.mailCommande.myAction.DATE_HOURS		 = 	Formator.formatReverseDate(etapeValidation.mailCommande.myAction.DATE_ACTION) 
																				+ " " 
																				+ Formator.formatHourConcat(etapeValidation.mailCommande.myAction.DATE_ACTION);
					
					etapeValidation.mailCommande.createMailInfos();
				}
				
				this.verifierAvantEnvoyer();
			}
		}
		
		private function verifierAvantEnvoyer():void
		{
			_cmdSrv.addEventListener(CommandeEvent.CMD_INEXISTANT_V, traitCmdInexistant);
			_cmdSrv.verifierCommande(_cmd.NUMERO_COMMANDE);
		}
		
		protected function traitCmdInexistant(event:Event):void
		{
			_cmdSrv.enregistrerCommandesResiliationV2(SessionUserObject.singletonSession.COMMANDEARTICLE,
														piecesJointes.piecesjointes.source,
														etapeValidation.mailCommande.mailCommande,
														Formator.formatBoolean(etapeValidation.mailCommande.chxMailCommande.selected),
														etapeValidation.mailCommande.myAction,
														piecesJointes.UUID);
			
			_cmdSrv.addEventListener('COMMANDE_ERROR_VERIF', traitErreurCommande);
			
		}
		
		protected function traitErreurCommande(event:Event):void
		{
			this.verifierAvantEnvoyer();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTIONS
		//--------------------------------------------------------------------------------------------//
		
		private function refreshPanier(e:Event = null):void
		{
			composantPanier.attributMontant();
		}
		
		private function validatePreviousPage(pageMax:int):void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			var childError	:int = -1;
			
			for(var i:int = 0;i < pageMax;i++)
			{
				if((children[i] as Object).ACCESS)
				{
					if(!(children[i] as Object).validateCurrentPage())
					{
						childError = i;
						break;
					}
				}
			}
			
			if(childError > 0)
				vsIndex = childError;
			else
				vsIndex = pageMax;
			
			buttonNavigationEnableDisable();
			buttonWizzardColor();
		}
		
		private function setIndexToViewStackValidation():void
		{
			switch(SessionUserObject.singletonSession.IDTYPEDECOMMANDE)
			{
				case 5: etapeValidation.indexSelected = 0; break;
				case 6: etapeValidation.indexSelected = 1; break;
				case 7: etapeValidation.indexSelected = 2; break;
				case 8: etapeValidation.indexSelected = 2; break;
				case 9: etapeValidation.indexSelected = 2; break;
			}
		}
		
		private function attributObject():void
		{
			etapeParametres.operateur.addItem(objectOperateur);
			etapeParametres.pool.addItem(objectPool);
			etapeParametres.multiCompte = false;
			
			etapeParametres.setCommande();
			
			if(isNoCompte)
				etapeParametres.getCompte(objectOperateur.OPERATEURID, objectPool.IDPOOL);
			else
			{
				if(isMultiCompte)
				{
					etapeParametres.compte.addItem(createNewObject(true));
					etapeParametres.sousCompte.addItem(createNewObject(false));
					etapeParametres.multiCompte = true;
					return;
				}
				else
					etapeParametres.compte.addItem(compte[0]);
				
				if(isMultiSousCompte)
				{
					etapeParametres.sousCompte.addItem(createNewObject(false));
					etapeParametres.multiCompte = true;
				}
				else
					etapeParametres.sousCompte.addItem(sousCompte[0]);
			}
		}
		
		private function createNewObject(isCompte:Boolean):Object
		{
			var myCompte:Object = new Object();
			
			if(isCompte)
			{
				myCompte.IDCOMPTE_FACTURATION	= -1;
				myCompte.COMPTE_FACTURATION		= ResourceManager.getInstance().getString('M16', 'Multi_compte');
			}
			else
			{
				myCompte.IDSOUS_COMPTE			= -1;
				myCompte.SOUS_COMPTE			= ResourceManager.getInstance().getString('M16', 'Multi_sous_compte');
			}
			
			return myCompte;
		}
		
		private function doVisibleCompteFacturation():void
		{
			etapeParametres.isNoCompte = isNoCompte;
		}

		private function returnToListeCommandes(isCommandeBuild:Boolean):void
		{
			if(isCommandeBuild)
				dispatchEvent(new ViewStackEvent(ViewStackEvent.COMMANDE_BUILT, true));
			else
				dispatchEvent(new ViewStackEvent(ViewStackEvent.ANNUL_COMMANDE, true));
		}

	}
}
