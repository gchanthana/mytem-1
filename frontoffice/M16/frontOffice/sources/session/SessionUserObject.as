package session
{
	import mx.collections.ArrayCollection;
	
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.Critere;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Pool;
	
	[Bindable]
	public class SessionUserObject
	{

//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - GLOABAL STATIC
		//--------------------------------------------------------------------------------------------//

		public static var singletonSession	:SessionUserObject;

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - LOCALE GETTER/SETTER
		//--------------------------------------------------------------------------------------------//

		//---- PRIVATE 
		//-> OBJECTS
		private var _commande				:Commande;
		private var _currentConfiguration	:ElementsCommande2;
		private var _selectedAction			:Action;
		private var _pool					:Pool;
		private var _poolSelected			:Pool;
		private var _currentCritere			:Critere;
		
		private var _userpools				:ArrayCollection;
		
		//---- PRIVATE 
		//-> VARIABLES
		private var _boolGestionParc		:Boolean = false;
		private var _commandeNormal			:Boolean = true;
		private var _commandeForfait		:Boolean = false;
		private var _commandeAccessoires	:Boolean = false;
		private var _commandeModele			:Boolean = false;
		private var _boolsncf				:Boolean = false;
		private var _isEditable				:Boolean = true;
		
		private var _boolDistribAutorises	:Boolean = false;

		private var _compteurConfiguration	:int 	 = 0;
		private var _idProfileSeleceted		:int 	 = -1;
		private var _idsoustete				:int=-1;
		private var _idTypeDeCommande		:int 	 = 1;
		private var _idSegment				:int 	 = -1;
		private var _idCommandeModele		:int 	 = -1;
		private var _rangeCompte			:int 	 = -1;
		private var _rangeSousCompte		:int 	 = -1;
		private var _lastConfiguration		:int 	 = 0;
		private var _lastVsIndex			:int 	 = -1;
		private var _eligibilite			:int 	 = -1;
		private var _etatLigneResSusMod		:int 	 = -1;
		private var _nbrLigneEligible		:int 	 = 0;
		private var _nbrLigneNonEligible	:int 	 = 0;
		private var _idrole					:int 	 = 0;
		
		private var _prix_reel_eligible		:Number = 0;
		private var _prixeligible			:Number = 0;
		private var _prixnoneligible		:Number = 0;
		
		private var _userProfil				:int 	 = -1;

		private var _typeLigne				:Object	 = new Object();
		private var _champs					:Object	 = new Object();
		private var _infos_divers			:Object	 = new Object();
				
		private var _commandeArrayCol		:ArrayCollection;
		private var _collabByArticles		:ArrayCollection;
		private var _commandeArticle		:ArrayCollection;
		private var _articles				:ArrayCollection;
		private var _compteSousCompte		:ArrayCollection;
		private var _listeActionsProfil		:ArrayCollection;
		private var _listeActionsUser		:ArrayCollection;
		
		private var _configuration			:ArrayCollection;
	
		private var _articleCommande		:XML;
		
		public function set COLLABBYARTICLES(value:ArrayCollection):void
		{
	    	_collabByArticles = value;
	    }
	    
	    public function get COLLABBYARTICLES():ArrayCollection
		{
			return _collabByArticles;
		}

		public function set INFOS_DIVERS(value:Object):void 
		{
	    	_infos_divers = value;
	    }
	    
	    public function get INFOS_DIVERS():Object
		{
			return _infos_divers;
		}

		public function set COMMANDEARRAYCOL(value:ArrayCollection):void
		{
	    	_commandeArrayCol = value;
	    }
	    
	    public function get COMMANDEARRAYCOL():ArrayCollection
		{
			if(_commandeArrayCol == null)
				_commandeArrayCol = new ArrayCollection();
			
			return _commandeArrayCol;
		}

		public function set COMPTESOUSCOMPTE(value:ArrayCollection):void
		{
			_compteSousCompte = value;
		}
		
		public function get COMPTESOUSCOMPTE():ArrayCollection
		{
			if(_compteSousCompte == null)
				_compteSousCompte = new ArrayCollection();
			
			return _compteSousCompte;
		}
	
		public function set LISTEACTIONSPROFIL(value:ArrayCollection):void
		{
			_listeActionsProfil = value;
		}
		
		public function get LISTEACTIONSPROFIL():ArrayCollection
		{
			if(_listeActionsProfil == null)
				_listeActionsProfil = new ArrayCollection();
			
			return _listeActionsProfil;
		}
		
		public function set LISTEACTIONSUSER(value:ArrayCollection):void
		{
			_listeActionsUser = value;
		}
		
		public function get LISTEACTIONSUSER():ArrayCollection
		{
			if(_listeActionsUser == null)
				_listeActionsUser = new ArrayCollection();
			
			return _listeActionsUser;
		}
		
		public function set COMMANDEARTICLE(value:ArrayCollection):void
		{
	    	_commandeArticle = value;
	    }
	    
	    public function get COMMANDEARTICLE():ArrayCollection
		{
			if(_commandeArticle == null)
				_commandeArticle = new ArrayCollection();
			
			return _commandeArticle;
		}
		
		public function set ARTICLES(value:ArrayCollection):void
		{
			_articles = value;
		}
		
		public function get ARTICLES():ArrayCollection
		{
			if(_articles == null)
				_articles = new ArrayCollection();
			
			return _articles;
		}
		
		
		public function set CONFIGURATION(value:ArrayCollection):void 
		{
	    	_configuration = value;
	    }
	    
	    public function get CONFIGURATION():ArrayCollection
		{
			if(_configuration == null)
			{
				_configuration = new ArrayCollection();
			}
			
			return _configuration;
		}

		public function set ARTICLECOMMANDE(value:XML):void
		{
	    	_articleCommande = value;
	    }
	    
	    public function get ARTICLECOMMANDE():XML
		{
			return _articleCommande;
		}

		public function set COMMANDENORMAL(value:Boolean):void
		{
	    	_commandeNormal = value;
	    }
	    
	    public function get COMMANDENORMAL():Boolean
		{
			return _commandeNormal;
		}
		
		public function set COMMANDEFORFAIT(value:Boolean):void
		{
	    	_commandeForfait = value;
	    }
	    
	    public function get COMMANDEFORFAIT():Boolean
		{
			return _commandeForfait;
		}
		
		public function set COMMANDEACCESSOIRES(value:Boolean):void
		{
	    	_commandeAccessoires = value;
	    }
	    
	    public function get COMMANDEACCESSOIRES():Boolean
		{
			return _commandeAccessoires;
		}
		
		public function set COMMANDEMODELE(value:Boolean):void
		{
	    	_commandeModele = value;
	    }
	    
	    public function get COMMANDEMODELE():Boolean
		{
			return _commandeModele;
		}
		
		public function set BOOLGESTIONPARC(value:Boolean):void
		{
	    	_boolGestionParc = value;
	    }
	    
	    public function get BOOLGESTIONPARC():Boolean
		{
			return _boolGestionParc;
		}
		
		public function set BOOLSNCF(value:Boolean):void
		{
	    	_boolsncf = value;
	    }
	    
	    public function get BOOLSNCF():Boolean
		{
			return _boolsncf;
		}
		
		public function set ISEDITABLE(value:Boolean):void
		{
			_isEditable = value;
		}
		
		public function get ISEDITABLE():Boolean
		{
			return _isEditable;
		}

		public function set BOOLDISTRIBAUTORISES(value:Boolean):void
		{
	    	_boolDistribAutorises = value;
	    }
	    
	    public function get BOOLDISTRIBAUTORISES():Boolean
		{
			return _boolDistribAutorises;
		}

		public function set ELIGIBILITE(value:int):void
		{
	    	_eligibilite = value;
	    }
	    
	    public function get ELIGIBILITE():int
		{
			return _eligibilite;
		}
		
		public function set NBRLIGNEELIGIBLE(value:int):void
		{
			_nbrLigneEligible = value;
		}
		
		public function get NBRLIGNEELIGIBLE():int
		{
			return _nbrLigneEligible;
		}
		
		public function set NBRLIGNENONELIGIBLE(value:int):void
		{
			_nbrLigneNonEligible = value;
		}
		
		public function get NBRLIGNENONELIGIBLE():int
		{
			return _nbrLigneNonEligible;
		}
		
		public function set IDROLE(value:int):void
		{
			_idrole = value;
		}
		
		public function get IDROLE():int
		{
			return _idrole;
		}
		
		public function set PRIX_REEL_ELIGIBLE(value:Number):void
		{
			_prix_reel_eligible = value;
		}
		
		public function get PRIX_REEL_ELIGIBLE():Number
		{
			return _prix_reel_eligible;
		}
		
		public function set PRIXELIGIBLE(value:Number):void
		{
			_prixeligible = value;
		}
		
		public function get PRIXELIGIBLE():Number
		{
			return _prixeligible;
		}
		
		public function set PRIXNONELIGIBLE(value:Number):void
		{
			_prixnoneligible = value;
		}
		
		public function get PRIXNONELIGIBLE():Number
		{
			return _prixnoneligible;
		}
		
		
		
		//--- 1 -> LIGNES ACTIVES, 2 -> LIGNES SUSPENDUES, 3 -> LIGNES RÉSILIÉES
		public function set ETATLIGNERESSUSMOD(value:int):void
		{
			_etatLigneResSusMod = value;
		}
		
		public function get ETATLIGNERESSUSMOD():int
		{
			return _etatLigneResSusMod;
		}
				
		//--- 1-> LIGNE + EQ, 2-> LIGNE, 3-> EQUIPEMENT, 4-> CHARGER MODÈLE 
		public function set IDPROFILESELECETED(value:int):void
		{
	    	_idProfileSeleceted = value;
	    }
	    
	    public function get IDPROFILESELECETED():int
		{
			return _idProfileSeleceted;
		}
		
		//--- COMPTEUR POUR LE REMPLISSAGE AUTO 
		public function set COMPTEURCONFIGURATION(value:int):void
		{
			_compteurConfiguration = value;
		}
		
		public function get COMPTEURCONFIGURATION():int
		{
			return _compteurConfiguration;
		}
		
		//--- 1-> LIGNE + EQ, 2-> LIGNE, 3-> EQUIPEMENT, 4-> CHARGER MODÈLE, 5-> REENGAGEMENT, 6-> MODIFICATION OPTION, 7-> SUSPENSION, 8-> REACTIVATION, 9-> RESILIATION, 10-> EQUIPEMENT FIXE
		public function set IDTYPEDECOMMANDE(value:int):void
		{
	    	_idTypeDeCommande = value;
	    }
	    
	    public function get IDTYPEDECOMMANDE():int
		{
			return _idTypeDeCommande;
		}
		
		//--- 1-> MOBILE, 2-> FIXE, 3-> DATA
		public function set IDSEGMENT(value:int):void 
		{
	    	_idSegment = value;
	    }
	    
	    public function get IDSEGMENT():int
		{
			return _idSegment;
		}
		
		public function set IDSOUSTETE(value:int):void 
		{
			_idsoustete = value;
		}
		
		public function get IDSOUSTETE():int
		{
			return _idsoustete;
		}
		
		//--> 1-> Consotel, 2-> revendeur, 3-> client
		public function set USERPROFIL(value:int):void 
		{
			_userProfil = value;
		}
		
		public function get USERPROFIL():int
		{
			return _userProfil;
		}
		
		
		
		
		public function set TYPELIGNE(value:Object):void
		{
	    	_typeLigne = value;
	    }
	    
	    public function get TYPELIGNE():Object
		{
			return _typeLigne;
		}
		
		

		public function set IDCOMMANDEMODELE(value:int):void
		{
	    	_idCommandeModele = value;
	    }
	    
	    public function get IDCOMMANDEMODELE():int
		{
			return _idCommandeModele;
		}

		public function set RANGECOMPTE(value:int):void
		{
	    	_rangeCompte = value;
	    }
	    
	    public function get RANGECOMPTE():int
		{
			return _rangeCompte;
		}
		
		public function set RANGESOUSCOMPTE(value:int):void
		{
	    	_rangeSousCompte = value;
	    }
	    
	    public function get RANGESOUSCOMPTE():int
		{
			return _rangeSousCompte;
		}
		
		public function set LASTCONFIGURATION(value:int):void
		{
	    	_lastConfiguration = value;
	    }
	    
	    public function get LASTCONFIGURATION():int
		{
			return _lastConfiguration;
		}
		
		public function set LASTVSINDEX(value:int):void 
		{
	    	_lastVsIndex = value;
	    }
	    
	    public function get LASTVSINDEX():int
		{
			return _lastVsIndex;
		}

		public function set COMMANDE(value:Commande):void
		{
	    	_commande = value;
	    }
	    
	    public function get COMMANDE():Commande
		{
			return _commande;
		}
	    
		public function set CURRENTCONFIGURATION(value:ElementsCommande2):void
		{
			_currentConfiguration = value;
		}
		
		public function get CURRENTCONFIGURATION():ElementsCommande2
		{
			if(_currentConfiguration == null)
			{
				_currentConfiguration = new ElementsCommande2();
			}
			return _currentConfiguration;
		}
		
		public function set ACTION(value:Action):void
		{
	    	_selectedAction = value;
	    }
	    
	    public function get ACTION():Action
		{
			return _selectedAction;
		}
		
		public function set USERPOOLS(value:ArrayCollection):void
		{
			_userpools = value;
		}
		
		public function get USERPOOLS():ArrayCollection
		{
			return _userpools;
		}
		
		public function set POOL(value:Pool):void
		{
	    	_pool = value;
	    }
	    
	    public function get POOL():Pool
		{
			return _pool;
		}
		
		public function set POOLSELECTED(value:Pool):void
		{
			_poolSelected = value;
		}
		
		public function get POOLSELECTED():Pool
		{
			return _poolSelected;
		}
		
		public function set CHAMPS(value:Object):void
		{
	    	_champs = value;
	    }
	    
	    public function get CHAMPS():Object
		{
			return _champs;
		}
		
		public function set CURRENTCRITERE(value:Critere):void
		{
			_currentCritere = value;
		}
		
		public function get CURRENTCRITERE():Critere
		{
			if(_currentCritere == null)
				_currentCritere = new Critere();
				
			return _currentCritere;
		}
		
//--------------------------------------------------------------------------------------------//
//					SETTER
//--------------------------------------------------------------------------------------------//
	
	    
		
//--------------------------------------------------------------------------------------------//
//					GETTER
//--------------------------------------------------------------------------------------------//		
		
		
		
//--------------------------------------------------------------------------------------------//
//					CONSTRUCTEUR
//--------------------------------------------------------------------------------------------//	
	
		public function SessionUserObject()
		{
			if(singletonSession == null)
				singletonSession = this;
		}	
		
	}
}