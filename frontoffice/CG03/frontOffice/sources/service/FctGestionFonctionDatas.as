package service
{
	import entity.FonctionVO;
	import entity.NiveauVO;
	import entity.ParametreFonctionVO;
	import entity.UniversVO;
	
	import event.FctGestionFonctionEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;

	public class FctGestionFonctionDatas extends EventDispatcher
	{
		// VARIBLES ------------------------------------------------------------------------
		private var _listeNiveaux:ArrayCollection = new ArrayCollection();
		private var _listeFonctions:ArrayCollection = new ArrayCollection();
		private var _listeDroits:ArrayCollection = new ArrayCollection();
		private var _listeUnivers:ArrayCollection = new ArrayCollection();

		// FUNCTIONS -----------------------------------------------------------------------
		
		public function treatListeNiveaux(ret:Object):void
		{
			_listeNiveaux.source = new Array();
			
			// le premier objet "tous les niveaux"
			var niveauAll:NiveauVO = new NiveauVO();
			niveauAll.Id = "-1";
			niveauAll.libelle = ResourceManager.getInstance().getString("CG03", "_Tous_les_niveaux_");
			_listeNiveaux.addItem(niveauAll);
			
			// recupérer les niveaux de la base
			var lonResult:uint=(ret.RESULT as ArrayCollection).length;
			for(var i:uint=0; i<lonResult; i++)
			{
				var niveau:NiveauVO = new NiveauVO();
				niveau.fill(ret.RESULT[i]);
				_listeNiveaux.addItem(niveau);
			}
			this.dispatchEvent(new FctGestionFonctionEvent(FctGestionFonctionEvent.LISTE_NIVEAUX));
			
		}
		
		public function treatListeFonctions(ret:ArrayCollection):void
		{
			_listeFonctions.source = new Array();
			var lonResult:uint=ret.length;
			for(var i:uint=0; i<lonResult; i++)
			{
				var fct:FonctionVO = new FonctionVO();
				fct.fill(ret[i]);
				_listeFonctions.addItem(fct);
			}
			this.dispatchEvent(new FctGestionFonctionEvent(FctGestionFonctionEvent.LISTE_FONCTIONS));
		}
		
		public function treatDroitsSpecifique(retour:ArrayCollection):void
		{
			_listeDroits.source = new Array();
			var lonResult:uint=(retour as ArrayCollection).length;
			for(var i:uint=0; i<lonResult; i++)
			{
				var fct:ParametreFonctionVO = new ParametreFonctionVO();
				fct.fill(retour[i]);
				_listeDroits.addItem(fct);
			}
			
			this.dispatchEvent(new FctGestionFonctionEvent(FctGestionFonctionEvent.LISTE_DROITS_SPEC));
			
		}
		
		public function treatDeleteFct(obj:Object):void
		{
			this.dispatchEvent(new FctGestionFonctionEvent(FctGestionFonctionEvent.FONCTION_DELETED));
			
		}
		
		public function treatCreateFct(result:Object):void
		{
			this.dispatchEvent(new FctGestionFonctionEvent(FctGestionFonctionEvent.FONCTION_SAVED));
			
		}
		
		public function treatUpdateFct(result:Object):void
		{
			this.dispatchEvent(new FctGestionFonctionEvent(FctGestionFonctionEvent.FONCTION_UPDATED));
			
		}
		
		public function treaCreateDroitFct(result:Object):void
		{
			this.dispatchEvent(new FctGestionFonctionEvent(FctGestionFonctionEvent.DROIT_FCT_CREATED));
			
		}
		
		public function treaUpdateDroitFct(result:Object):void
		{
			this.dispatchEvent(new FctGestionFonctionEvent(FctGestionFonctionEvent.DROIT_FCT_UPDATED));
		}
		
		public function treaDeleteDroitFct(result:Object):void
		{
			this.dispatchEvent(new FctGestionFonctionEvent(FctGestionFonctionEvent.DROIT_FCT_DELETED));
			
		}
		
		public function treatListeUnivers(retour:Object):void
		{
			listeUnivers.source = new Array();
			var lonResult:uint=(retour.RESULT as ArrayCollection).length;
			for(var i:uint=0; i<lonResult; i++)
			{
				var univ:UniversVO = new UniversVO;
				univ.fill(retour.RESULT[i]);
				listeUnivers.addItem(univ);
			}
			
			this.dispatchEvent(new FctGestionFonctionEvent(FctGestionFonctionEvent.LISTE_UNIVERS));
			
		}
		
		/***************************************************************/
		// GETTERS et SETTERS --------------------------------------------------------------
		/***************************************************************/
		
		public function get listeNiveaux():ArrayCollection
		{
			return _listeNiveaux;
		}

		public function set listeNiveaux(value:ArrayCollection):void
		{
			_listeNiveaux = value;
		}
		
		public function get listeFonctions():ArrayCollection
		{
			return _listeFonctions;
		}
		
		public function set listeFonctions(value:ArrayCollection):void
		{
			_listeFonctions = value;
		}
		
		[Bindable]
		public function get listeDroits():ArrayCollection { return _listeDroits; }
		
		public function set listeDroits(value:ArrayCollection):void
		{
			if (_listeDroits == value)
				return;
			_listeDroits = value;
		}
		
		[Bindable]
		public function get listeUnivers():ArrayCollection { return _listeUnivers; }
		
		public function set listeUnivers(value:ArrayCollection):void
		{
			if (_listeUnivers == value)
				return;
			_listeUnivers = value;
		}

	}
}