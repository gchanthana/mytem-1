package carol.popUp
{
	import carol.event.CarolEvent;
	
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.managers.PopUpManager;
	
	public class resetAnalyseImpl extends Box
	{
		
//--------------------------------------------------------------------------------------------//
//					CONSTRUCTEUR
//--------------------------------------------------------------------------------------------//	
		
		public function resetAnalyseImpl()
		{
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PROTECTED
//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED - EVENT COMPOSANTS
		//--------------------------------------------------------------------------------------------//
		
		protected function btnValidClickHandler(e:Event):void
		{
			dispatchEvent(new CarolEvent(CarolEvent.POPUP__VALIDATE));
		}
		
		protected function btnAnnulClickHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}

	}
}