package carol.ihm.menu
{
	import carol.composants.typeGraphIHM;
	import carol.event.CarolEvent;
	import carol.popUp.PopupAideIHM;
	import carol.popUp.PopupExportPdf;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.ui.Keyboard;
	
	import ilog.charts.PivotChart;
	
	import mx.containers.VBox;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	
	public class actionsImpl extends VBox
	{
//--------------------------------------------------------------------------------------------//
//					CONSTANTE
//--------------------------------------------------------------------------------------------//
		private const UrlDocumentAide:String = "https://cache-pilotage.sfrbusinessteam.fr/Aide/";	
		
//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - STATIC - LIBELLE EVENT
		//--------------------------------------------------------------------------------------------//	
		public static const GRAPH_PER_PAGE 	:String = "4";
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - COMPOSANTS
		//--------------------------------------------------------------------------------------------//		
		public var txtbxPageNumber		:TextInput;
		//COMPOSANTS ANALYSE
		public var typeGraph			:typeGraphIHM
		[Bindable] public var myPivotChart			:PivotChart
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - LOCAL
		//--------------------------------------------------------------------------------------------//
		[Bindable]
		public var pageNumberStr:String = GRAPH_PER_PAGE;
		private var popup:PopupExportPdf

//--------------------------------------------------------------------------------------------//
//					CONSTRUCTEUR
//--------------------------------------------------------------------------------------------//
				
		public function actionsImpl()
		{
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PROTECTED
//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					CHANGEMENT DU NOMBRE MAX DE GRAPHIQUE PAR PAGE
		//--------------------------------------------------------------------------------------------//
		// EN APPUYANT SUR ENTER
		protected function txtbxPageNumberEnterHandler(e:KeyboardEvent):void
		{
			if(e.keyCode == Keyboard.ENTER)
			{
				pageNumberStr = txtbxPageNumber.text;
				if(pageNumberStr != null && pageNumberStr.length > 0)
				{
					var pageNumber:int = parseInt(pageNumberStr);
					if(pageNumber.toString() != "NaN")
					{
						txtbxPageNumber.errorString = "";
						var obj:Object 		= new Object();
							obj.PAGE_NUMBER = pageNumber;
						var ce:CarolEvent = new CarolEvent(CarolEvent.PAGE_NUMBER_CHANGED,true,obj);
						dispatchEvent(ce);
					}
					else
					{
						pageNumberStr = GRAPH_PER_PAGE;
					}
				}
				else
				{
					pageNumberStr = GRAPH_PER_PAGE;
				}
			}
		}
		// EN CLIQUANT
		protected function txtbxPageNumberChangeHandler():void
		{
			pageNumberStr = txtbxPageNumber.text;
			if(pageNumberStr != null && pageNumberStr.length > 0)
			{
				var pageNumber:int = parseInt(pageNumberStr);
				if(pageNumber.toString() != "NaN")
				{
					txtbxPageNumber.errorString = "";
					var obj:Object 		= new Object();
						obj.PAGE_NUMBER = pageNumber;
					var ce:CarolEvent = new CarolEvent(CarolEvent.PAGE_NUMBER_CHANGED,true,obj);
					dispatchEvent(ce);
				}
				else
				{
					pageNumberStr = GRAPH_PER_PAGE;
				}
			}
			else
			{
				pageNumberStr = GRAPH_PER_PAGE;
			}
		}
		//--------------------------------------------------------------------------------------------//
		//			GESTION DES EVENEMENTS POUR L'OUVERTURE DES POPUP
		//--------------------------------------------------------------------------------------------//	
		protected function btnOpenClickHandler(e:Event):void
		{
			dispatchEvent(new CarolEvent(CarolEvent.OPEN__DATA, true));
		}
		
		protected function btnResetClickHandler(e:Event):void
		{
			dispatchEvent(new CarolEvent(CarolEvent.RESET_DATA, true));
		}
		
		protected function btnSaveAsClickHandler(e:Event):void
		{
			dispatchEvent(new CarolEvent(CarolEvent.SAVE__DATA, true));
		}
		
		protected function btnPDFClickHandler():void
		{
			dispatchEvent(new CarolEvent(CarolEvent.EXPORT_PDF_EVENT, true));
		}
		//--------------------------------------------------------------------------------------------//
		//					REFRESH
		//--------------------------------------------------------------------------------------------//
		public function refresh():void
		{
			typeGraph.SELECTEDINDEX=0
			pageNumberStr = "4" 
			txtbxPageNumber.text = "4";
		}
		public function openPopupAide():void
		{
			var request:URLRequest = new URLRequest(UrlDocumentAide+"Aide_en_ligne_construire_une_analyse.pdf");
			navigateToURL(request,null);
		}
	}
}
