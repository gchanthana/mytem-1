package carol.ihm.login
{
	import carol.event.CarolEvent;
	
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.resources.ResourceManager;
	
	public class loginImpl extends Box
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - COMPOSANTS FLEX
		//--------------------------------------------------------------------------------------------//
		
		//COMBOBOX
		public var cbxLogin				:TextInput;
		//CHECKBOX
		public var ckx					:CheckBox;
		//TEXTINPUT
		public var txtbxPwd				:TextInput;
		//TEXTAREA
		public var txtAreaError			:TextArea;
		//LABEL
		public var lblEmail				:Label;
		//BUTTON	
		public var btnConnexion			:Button;
		public var imgAcrobat			:Image;
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		private var codeLangue:String
		private var codePays:String = "FR"
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function loginImpl()
		{
		}

		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLIC
		//--------------------------------------------------------------------------------------------//
		public function getCodeLangue():String
		{
			return codeLangue
		}
		
		public function getCodePays():String
		{
			return codePays
		}

		public function refresh():void
		{
			cbxLogin.text = "smith@consotel.fr"
			txtbxPwd.text = ""
			txtAreaError.text = ""
		}
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED - EVENT COMPOSANTS
		//--------------------------------------------------------------------------------------------//

		protected function btnlanguageClickHandler(btnID:int):void
		{
			switch(btnID)
			{
				case 0 :
					codeLangue = "FR"
					ResourceManager.getInstance().localeChain = ["fr_FR"]
					break;
				case 1 :
					codeLangue = "UK"
					ResourceManager.getInstance().localeChain = ["en_US"]
					break;
				case 2 :
					codeLangue = "ES"
					ResourceManager.getInstance().localeChain = ["es_ES"]
					break;
				case 3 :
					codeLangue = "US"
					ResourceManager.getInstance().localeChain = ["en_US"]
					break;
			}
		}
		
		protected function btnConnexionClickHandler(e:Event):void
		{
			if(cbxLogin.text == "" || txtbxPwd.text == "")
			{
				if(cbxLogin.text == "" && txtbxPwd.text == "")
				{
					txtAreaError.text = ResourceManager.getInstance().getString('M37', 'Veuillez_saisir_un_identifiant__net_un_m');
					return;
				}
				if(cbxLogin.text == "")
				{
					txtAreaError.text = ResourceManager.getInstance().getString('M37', 'Veuillez_saisir_un_identifiant');
					return;
				}
				if(txtbxPwd.text == "")
				{
					txtAreaError.text = ResourceManager.getInstance().getString('M37', 'Veuillez_saisir_un_mot_de_passe');
					return;
				}
			}
			else
			{
				txtAreaError.text 		= "";
				txtbxPwd.text 			= "";
				dispatchEvent(new CarolEvent(CarolEvent.LOGIN_VALIDED));
			}
			
		}
	}
}