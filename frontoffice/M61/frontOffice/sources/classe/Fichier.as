package classe
{
	public class Fichier
	{
		private var _NOM:String
		private var _EXTENSION:String
		private var _FICHIER:Fichier
		
		public function get NOM():String
		{
			return _NOM;
		}
		public function get EXTENSION():String
		{
			return _EXTENSION;
		}
		public function get FICHIER():Fichier
		{
			return _FICHIER;
		}
		
		public function set NOM(str:String):void
		{
			_NOM = str;
		}
		public function set EXTENSION(str:String):void
		{
			_EXTENSION = str;
		}
		public function set DEMANDEUR(f:Fichier):void
		{
			_FICHIER = f;
		}
		
		
		public function Fichier()
		{
		}

	}
}