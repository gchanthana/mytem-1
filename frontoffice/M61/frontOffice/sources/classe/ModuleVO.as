package classe
{
	import mx.messaging.AbstractConsumer;
	
	public class ModuleVO
	{
		private var _IDMODULE	:	int;
		private var _LIBELLE	:	String;
		
		public function ModuleVO()
		{
		}


		public function set IDMODULE(value:int):void
		{
			_IDMODULE = value;
		}

		public function get IDMODULE():int
		{
			return _IDMODULE;
		}

		public function set LIBELLE(value:String):void
		{
			_LIBELLE = value;
		}

		public function get LIBELLE():String
		{
			return _LIBELLE;
		}
	}
}