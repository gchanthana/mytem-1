package classe
{
	import composants.util.ConsoviewAlert;
	
	import events.container.ContainerEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.DateField;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable] public class Doc extends EventDispatcher implements IEventDispatcher
	{
		
		//----------- VARIABLES -----------//
		
		private var _FILEID				:int;				// FILEID
		private var _LIBELLE			:String = "";		// NOM_DETAILLE_RAPPORT
		private var _LIBELLE_SHORT		:String;			// SHORT_NAME_RAPPORT
		private var _STATUS				:String = "";		// STATUS
		private var _STATUS_ID			:int;				// IDSTATUS
		private var _IDRACINE			:int;				// IDRACINE
		private var _APP_LOGINID		:int;				// APP_LOGINID
		private var _DEMANDEUR_NOM		:String = "";		// UTILISATEUR
		private var _DEMANDEUR_PRENOM	:String = "";		// UTILISATEUR
		private var _DATE_DEMANDE		:Date;				// DATE_DEMANDE
		private var _DATE_PLANIF		:Date;				// DATE_PLANIFICATION
		private var _BISERVER			:String;			// BI_SERVER
		private var _APPLI				:String;			// APPLICATION
		private var _TAILLE				:Number = 0;		// TAILLE
		private var _TAILLEZIP			:Number = 0;		// TAILLEZIP
		private var _FORMAT				:String = "";		// FORMAT_FICHIER
		private var _CODE_RAPPORT		:String = "";		// CODE_RAPPORT
		private var _UUID 				:String = ""		// UUID
		private var _JOBID				:int;				// JOBID
		private var _MODULE				:String = ""		// MODULE
		private var _NBRECORD			:int;				// NBRECORD
		// Object contenant les paramètres du document
		private var _OBJPARAMS			:String = ""
		
		// permet de savoir si le Doc a été sélectionné dans la liste	
		private var _SELECTED			:Boolean = false;
		// indique si le Doc est en erreur : false = en erreur // true = encours ou disponible
		private var _BOOL_ERROR			:Boolean = true;	// ISINERROR
		// indique si le Doc est en cours de création
		private var _ENCOURS			:Boolean = true		// ISENCOURS
		// indique si le Doc est partagé
		private var _SHARED				:Boolean = false;	// ISSHARED
	
		
		
		//---------------- METHODES ----------------//
		
		/* */
		public function Doc()
		{
		}
		
		/* envoit le fichier sur le FTP */
		public function uploadToFTP():void
		{
			/* var url:URLRequest = new URLRequest(Container.NonSecureUrlBackoffice+"/fr/consotel/consoprod/M61/processUpload.cfm");
			f.addEventListener(Event.COMPLETE,uploadComplete);
			f.addEventListener(IOErrorEvent.IO_ERROR,IOerrorHandler);
			f.addEventListener(ProgressEvent.PROGRESS,progressHandler);
			f.upload(url); */
		}
		
		/* ajoute le document dans la table container */
		public function addToTableContainer():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.containerService",
																		"ADDDocumentTableContainer", 
																		ADDDocumentTableContainer_handler);
			RemoteObjectUtil.callService(op,LIBELLE,LIBELLE_SHORT,JOBID,CODE_RAPPORT,APP_LOGINID,IDRACINE,MODULE,FORMAT,DATE_PLANIF,"",APPLI,0,STATUS_ID,TAILLE,TAILLEZIP); 
		}
		
		/* ajoute le document dans la table container */
		public function addToTableContainerOtherDoc():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"containerPublishRapportConsotel", 
																		ADDDocumentTableContainerOtherDoc_handler);
			RemoteObjectUtil.callService(op,UUID,LIBELLE,LIBELLE_SHORT,JOBID,CODE_RAPPORT,APP_LOGINID,IDRACINE,MODULE,FORMAT,DateField.dateToString(DATE_PLANIF,"YYYY/MM/DD"),"",APPLI,true,STATUS_ID,TAILLE,TAILLEZIP); 
		}
		
		/* modifie le nom_detaille_rapport / libelle du document dans la table container */
		public function renameDoc():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"EDITNameDocumentTableContainer", 
																		EDITNameDocumentTableContainer_handler);
			RemoteObjectUtil.callService(op,_LIBELLE,_FILEID); 
		}
		
		
		
		//---------------- RETOURS - HANDLERS ----------------//
		
		/* */
		private function uploadComplete(e:Event):void
		{
			dispatchEvent(new ContainerEvent(ContainerEvent.UPLOAD_COMPLETE));
		}
		
		/* */
		private function IOerrorHandler(e:IOErrorEvent):void
		{
			dispatchEvent(new ContainerEvent(ContainerEvent.UPLOAD_ERROR));
		}
		
		/* */
		private function progressHandler(e:ProgressEvent):void
		{
			dispatchEvent(e);
		}
		
		/* */
		private function ADDDocumentTableContainer_handler(e:ResultEvent):void
		{
			dispatchEvent(new ContainerEvent(ContainerEvent.DOCUMENT_ADDED,e.result.toString()));
		}
		
		/* ajout de document dans Container dans categorie "autres documents" - Handler */
		private function ADDDocumentTableContainerOtherDoc_handler(re:ResultEvent):void
		{
			if(re.result != null)
				dispatchEvent(new ContainerEvent(ContainerEvent.DOCUMENT_ADDED,re.result.toString()));
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M61','erreur_survenue'),"Attention",null);
		}
		
		/* */
		private function EDITNameDocumentTableContainer_handler(e:ResultEvent):void
		{
			dispatchEvent(new ContainerEvent(ContainerEvent.DOCUMENT_RENAMED));
		}
		
		
		
		//---------------- GETTERS - SETTERS ----------------//
		
		/* LIBELLE */
		public function get LIBELLE():String
		{
			return _LIBELLE;
		}
		public function set LIBELLE(str:String):void
		{
			_LIBELLE = str;
		}
		
		/* LIBELLE_SHORT */
		public function get LIBELLE_SHORT():String
		{
			return _LIBELLE_SHORT;
		}
		public function set LIBELLE_SHORT(value:String):void
		{
			_LIBELLE_SHORT = value;
		}
		
		/* STATUS */
		public function get STATUS():String
		{
			return _STATUS;
		}
		public function set STATUS(str:String):void
		{
			_STATUS = str;
		}
		
		/* DEMANDEUR_NOM */
		public function get DEMANDEUR_NOM():String
		{
			return _DEMANDEUR_NOM;
		}
		public function set DEMANDEUR_NOM(str:String):void
		{
			_DEMANDEUR_NOM = str;
		}
		
		/* DEMANDEUR_PRENOM */
		public function get DEMANDEUR_PRENOM():String
		{
			return _DEMANDEUR_PRENOM;
		}
		public function set DEMANDEUR_PRENOM(str:String):void
		{
			_DEMANDEUR_PRENOM = str;
		}
		
		/* DATE_DEMANDE */
		public function get DATE_DEMANDE():Date
		{
			return _DATE_DEMANDE;
		}
		public function set DATE_DEMANDE(dt:Date):void
		{
			_DATE_DEMANDE = dt
		}
		
		/* TAILLE */
		public function get TAILLE():Number
		{
			return _TAILLE;
		}
		public function set TAILLE(i:Number):void
		{
			_TAILLE = i;
		}
		
		/* FORMAT */
		public function get FORMAT():String
		{
			return _FORMAT;
		}
		public function set FORMAT(str:String):void
		{
			_FORMAT = str;
		}
		
		/* CODE_RAPPORT */
		public function get CODE_RAPPORT():String
		{
			return _CODE_RAPPORT;
		}
		public function set CODE_RAPPORT(str:String):void
		{
			_CODE_RAPPORT = str;
		}
		
		/* MODULE */
		public function get MODULE():String
		{
			return _MODULE;
		}
		public function set MODULE(str:String):void
		{
			_MODULE = str;
		}
		
		/* UUID */
		public function get UUID():String
		{
			return _UUID;
		}
		public function set UUID(str:String):void
		{
			_UUID = str;
		}
		
		/* SELECTED */
		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}
		public function set SELECTED(bool:Boolean):void
		{
			_SELECTED = bool;
		}
		
		/* BOOL_ERROR */
		public function get BOOL_ERROR():Boolean
		{
			return _BOOL_ERROR;
		}
		public function set BOOL_ERROR(bool:Boolean):void
		{
			_BOOL_ERROR = bool;
		}
		
		/* SHARED */
		public function get SHARED():Boolean
		{
			return _SHARED;
		}
		public function set SHARED(bool:Boolean):void
		{
			_SHARED = bool
		}
		
		/* ENCOURS */
		public function get ENCOURS():Boolean
		{
			return _ENCOURS;
		}
		public function set ENCOURS(bool:Boolean):void
		{
			_ENCOURS = bool
		}
		
		/* OBJPARAMS */
		public function get OBJPARAMS():String
		{
			return _OBJPARAMS;
		}
		public function set OBJPARAMS(arr:String):void
		{
			_OBJPARAMS = arr;
		}
		
		/* TAILLEZIP */
		public function get TAILLEZIP():Number
		{
			return _TAILLEZIP;
		}
		public function set TAILLEZIP(value:Number):void
		{
			_TAILLEZIP = value;
		}
		
		/* APP_LOGINID */
		public function get APP_LOGINID():int
		{
			return _APP_LOGINID;
		}
		public function set APP_LOGINID(value:int):void
		{
			_APP_LOGINID = value;
		}
		
		/* IDRACINE */
		public function get IDRACINE():int
		{
			return _IDRACINE;
		}
		public function set IDRACINE(value:int):void
		{
			_IDRACINE = value;
		}
		
		/* JOBID */
		public function get JOBID():int
		{
			return _JOBID;
		}
		public function set JOBID(value:int):void
		{
			_JOBID = value;
		}
		
		/* DATE_PLANIF */
		public function get DATE_PLANIF():Date
		{
			return _DATE_PLANIF;
		}
		public function set DATE_PLANIF(value:Date):void
		{
			_DATE_PLANIF = value;
		}
		
		/* BISERVER */
		public function get BISERVER():String
		{
			return _BISERVER;
		}
		public function set BISERVER(value:String):void
		{
			_BISERVER = value;
		}
		
		/* APPLI */
		public function get APPLI():String
		{
			return _APPLI;
		}
		public function set APPLI(value:String):void
		{
			_APPLI = value;
		}
		
		/* FILEID */
		public function get FILEID():int
		{
			return _FILEID;
		}
		public function set FILEID(value:int):void
		{
			_FILEID = value;
		}
		
		/* NBRECORD */
		public function get NBRECORD():int
		{
			return _NBRECORD;
		}
		public function set NBRECORD(value:int):void
		{
			_NBRECORD = value;
		}
		
		/* STATUS_ID */
		public function get STATUS_ID():int
		{
			return _STATUS_ID;
		}
		public function set STATUS_ID(value:int):void
		{
			_STATUS_ID = value;
		}
	}
}