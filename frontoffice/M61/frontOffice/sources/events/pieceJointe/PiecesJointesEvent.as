package events.pieceJointe
{
	import flash.events.Event;
	
	public class PiecesJointesEvent extends Event
	{
		//------------ VARIABLES ------------//
		
		public static const FILE_UUID_CREATED:String = "FILE_UUID_CREATED";
		
		
		//------------ METHODES ------------//
		
		/* */
		public function PiecesJointesEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}