package popup
{
	import composant.AttachementIHM;
	
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import entity.FileUpload;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import service.upload.UploadFileService;
	
	import utils.formator.Formator;

	public class PopupAddFileImpl extends TitleWindowBounds
	{
		//----------- VARIABLES -----------//
		
		private var _attachements				:AttachementIHM;
		
		public var vbxParcourir					:VBox;
		public var btnValid						:Button;
		public var btnClose						:Button;
		
		public var currentFilesToSave			:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		public var sizeTotal					:String = '0.00';
		
		public static const sizeMaxPJ			:Number = 100;
		
		public static const ADD_ATTACHEMENT		:String = 'ADD_ATTACHEMENT';
		
		public var fileupl						:FileUpload = new FileUpload();
		
		private var _fileUploadSrv				:UploadFileService = new UploadFileService();
		
		
		//----------- METHODES -----------//
		
		/* */
		public function PopupAddFileImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, initListeners);
		}
		
		/* init listeners */
		private function initListeners(fe:FlexEvent):void
		{
			btnValid.addEventListener(MouseEvent.CLICK, btnValidClickHandler);
			btnClose.addEventListener(MouseEvent.CLICK, closePopupHandler);
			
			addChildParcourir();
		}
		
		/* au click sur btn Valider - Handler */
		protected function btnValidClickHandler(me:MouseEvent):void
		{
			var lenChild	:int 	= vbxParcourir.numChildren;
			var children 	:Array	= vbxParcourir.getChildren();
			var totalNbr	:Number = 0;
			var totalstr	:String = '0.00';
			
			for(var i:int=0; i<lenChild; i++) //lenChild-1
			{
				if(children[i] as AttachementIHM)
				{
					if((children[i] as AttachementIHM).isStatus)
					{
						currentFilesToSave.addItem((children[i] as AttachementIHM).currentFile);
						fileupl = (children[i] as AttachementIHM).currentFile;
					}
					else
					{
						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M61', 'Les_fichiers_sont_en_cours_de_v_rificati'), 'Attention', null);
						return;
					}
					totalNbr = totalNbr + (children[i] as AttachementIHM).currentFile.fileSize;
				}
			}
			
			totalstr = Formator.formatOctetsToMegaOctets(totalNbr);
			
			if(currentFilesToSave.length == 0)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M61', 'Veuillez_s_lectionner_au_moins_un_fichier'), 'Attention', null);
			}
			else
			{
				if(Number(totalstr) < sizeMaxPJ)
				{
					ConsoviewAlert.afficherAlertInfo(resourceManager.getString('M61','vous_pourrez_retrouver_le_fichier_upload__en_cliquant_sur_le_filtre__Autres_documents_'),"Attention",processUploadFile);
				}
				else
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M61', 'Votre_capacit__total_de_pi_ces_jointes_e') + sizeTotal + ResourceManager.getInstance().getString('M28', '_20_Mo_'), 'Attention', null);
			}
		}
		
		/* */
		private function processUploadFile(ce:CloseEvent):void
		{
			dispatchEvent(new Event("PROCESS_UPLOAD_FILE",true));
		}
				
		/* fermeture de popup - Handler */
		protected function closePopupHandler(e:Event):void 
		{
			PopUpManager.removePopUp(this);
		}

		/*  ajout de l'enfant Attachement piece jointe */
		private function addChildParcourir():void
		{
			var idChildren:int = Math.random() * 1000000;
			
			_attachements 		= new AttachementIHM();
			_attachements.ID	= idChildren;
			
			vbxParcourir.addChild(_attachements); //ajouter l'enfant au VBox
			
			_attachements.addEventListener(AttachementIHM.ADDED_ATTACHEMENT, addedAttachementHandler);
			_attachements.addEventListener(AttachementIHM.REMOVE_ATTACHEMENT, removeAttachementHandler);
		}
		
		/* ajout de piece jointe - Handler */
		private function addedAttachementHandler(e:Event):void
		{
			var lenChild		:int 	= vbxParcourir.numChildren;
			var children 		:Array	= vbxParcourir.getChildren();
			var totalNbr		:Number = 0;
			var totalCount		:String = "";
			
			for(var i:int=0; i<lenChild; i++)
			{
				totalNbr = totalNbr + (children[i] as AttachementIHM).currentFile.fileSize;
			}
			totalCount = Formator.formatOctetsToMegaOctets(totalNbr);
			
			if(Number(totalCount) < sizeMaxPJ)
			{
				_attachements.dispatchEvent(new Event(AttachementIHM.TOTALSIZEPJ_INFERIOR_MAXSIZE));
				sizeTotal = Formator.formatOctetsToMegaOctets(totalNbr);
//				addChildParcourir(); //ajout d'un enfant Attachement PJ
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M61', 'Votre_capacit__total_de_pi_ces_jointes_e') + totalCount + ResourceManager.getInstance().getString('M28', '_20_Mo_'), 'Attention', null);
		}
		
		/* supression de piece jointe - Handler */
		private function removeAttachementHandler(evt:Event):void
		{
			var idTarget	:int 	= evt.currentTarget.ID;
			removeAttachement(idTarget);
			
			// recuperer les valeurs mises à jour apres suppression de l'enfant
			var lenChild	:int 	= vbxParcourir.numChildren;
			var children 	:Array	= vbxParcourir.getChildren();
			var totalNbr	:Number = 0;
			
			for(var i:int=0; i<lenChild-1; i++)
			{
				totalNbr = totalNbr + (children[i] as AttachementIHM).currentFile.fileSize;
			}
			
			sizeTotal = Formator.formatOctetsToMegaOctets(totalNbr);
			
			if(lenChild == 0)
				addChildParcourir();
			
			if(Number(sizeTotal) > sizeMaxPJ)
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M61', 'Votre_capacit__total_de_pi_ces_jointes_e') + sizeTotal + ResourceManager.getInstance().getString('M28', '_20_Mo_'), 'Attention', null);
		}
		
		/* suppression d'un enfant [attachement piece jointe] dans l'IHM */
		private function removeAttachement(idTarget:int):void
		{
			var lenChild	:int 	= vbxParcourir.numChildren;
			var children 	:Array	= vbxParcourir.getChildren();
			
			for(var i:int = 0;i < lenChild;i++)
			{
				if((children[i] as AttachementIHM).ID == idTarget)
				{
					vbxParcourir.removeChildAt(i);
					return;
				}
			}
		}
		
		
		//----------- GETTERS - SETTERS -----------//
		
		public function get fileUploadSrv():UploadFileService
		{
			return _fileUploadSrv;
		}
	}
}