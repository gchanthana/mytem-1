package paginatedatagrid.pagination.event
{
	import flash.events.Event;
	
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	public class PaginationEvent extends Event
	{
		/*public static const SELECT_NEXT_PAGE_EVENT :String = 'selectNextPage';
		public static const SELECT_PREVIOUS_PAGE_EVENT :String = 'selectPaginationChange'; 
		public static const SELECT_FIRST_PAGE_EVENT :String = 'goFirstPage';
		public static const SELECT_LAST_PAGE_EVENT :String = 'goLastPage';*/
		
		public static const INTERVALLE_SELECTED_CHANGE :String = 'intervalleSelectedChange';
		
		public var itemIntervalleVO : ItemIntervalleVO;
		
		public function PaginationEvent(type:String,itemIntervalleVO :ItemIntervalleVO , bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.itemIntervalleVO = itemIntervalleVO;
		}

	}
}