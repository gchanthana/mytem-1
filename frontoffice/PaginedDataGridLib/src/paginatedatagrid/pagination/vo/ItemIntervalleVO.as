package paginatedatagrid.pagination.vo
{
	public class ItemIntervalleVO
	{
		private var _libelle : String;
		private var _indexDepart : int;
		private var _tailleIntervalle : int;
		
		public function ItemIntervalleVO()
		{
		}
		public function get libelle():String
		{
			return _libelle;
		}
		public function set libelle(libelle:String):void
		{
			this._libelle = libelle;
		}
		public function get indexDepart():int
		{
			return _indexDepart;
		}
		public function set indexDepart(indexDepart:int):void
		{
			this._indexDepart = indexDepart;
		}
		public function get tailleIntervalle():int
		{
			return _tailleIntervalle;
		}
		public function set tailleIntervalle(tailleIntervalle:int):void
		{
			this._tailleIntervalle = tailleIntervalle;
		}

	}
}