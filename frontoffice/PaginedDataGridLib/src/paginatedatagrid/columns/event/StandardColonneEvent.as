package paginatedatagrid.columns.event
{
	import flash.events.Event;
	
	public class StandardColonneEvent extends Event
	{
		public static const EDIT_ITEM:String = 'onItemEdit';
		public static const REMOVE_ITEM:String = 'onItemRemove';
		public static const SELECT_CHANGE_ITEM:String = 'onItemCheckBoxChange';
		
		public var item:Object;
		
		public function StandardColonneEvent(type : String,item : Object, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.item = item;
		}
		
		override public function clone():Event
		{
			return new  StandardColonneEvent(type,item,bubbles,cancelable);
		}

	}
}