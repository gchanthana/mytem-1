package searchpaginatedatagrid
{
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import mx.collections.ArrayCollection;
    import mx.containers.HBox;
    import mx.controls.ComboBox;
    import mx.controls.DateField;
    import mx.controls.TextInput;
    import paginatedatagrid.PaginateDatagrid;
    import paginatedatagrid.columns.SortableColumn;
    import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;

    [Bindable]
    [Event(name="onSearch", type="searchpaginatedatagrid.event.SearchPaginateDatagridEvent")]
    public class SearchPaginateDatagridControleur extends HBox
    {
        private var _paginateDatagridInstance:PaginateDatagrid;
        private var _colonnesRecherchables:ArrayCollection;
        private var _colonnesTriables:ArrayCollection;
        private var _typeRecherche:String = 'defaut'; //par défaut la recherche ne gère pas de période
        private var _parametresRechercheVO:ParametresRechercheVO = new ParametresRechercheVO();
        public var paddingLeftForm:Number = 90;
        public var widthFormItem:Number = 200;
        //PUBLIC UI COMPONANT
        public var txtCle:TextInput;
        public var comboRecherche:ComboBox;
        public var comboTrieColonne:ComboBox;
        public var comboOrderBy:ComboBox;
        public var dfDateDeb:DateField;
        public var dfDateFin:DateField;
        public var dfPeriode:DateField;

        public function SearchPaginateDatagridControleur()
        {
            this.setCurrentState(_typeRecherche);
        }

        public function initData():void
        {
            txtCle.addEventListener(KeyboardEvent.KEY_DOWN, reportKeyDown);
        }

        private function reportKeyDown(event:KeyboardEvent):void
        {
            if ((event as KeyboardEvent).keyCode == 13)
            {
                newSearch();
            }
        }

        public function newSearch(evt:MouseEvent = null):void
        {
            parametresRechercheVO = new ParametresRechercheVO();
            //Recherche du texte
            if (comboRecherche.selectedIndex == -1)
            {
                comboRecherche.selectedIndex = 0;
            }
            parametresRechercheVO.setSearchTexte((comboRecherche.selectedItem as SortableColumn).columDBName, txtCle.text);
            //Recherche du orderBY
            if (comboTrieColonne.selectedIndex == -1)
            {
                comboTrieColonne.selectedIndex = 0;
            }
            if (comboOrderBy.selectedIndex == -1)
            {
                comboOrderBy.selectedIndex = 0;
            }
            parametresRechercheVO.setOrderBY((comboTrieColonne.selectedItem as SortableColumn).columDBName, comboOrderBy.selectedItem.value);
            //Recherche sur les dates
            if (typeRecherche == 'periode')
            {
                parametresRechercheVO.setDateDebut(dfDateDeb.selectedDate);
                parametresRechercheVO.setDateFin(dfDateFin.selectedDate);
            }
            if (typeRecherche == 'mois')
            {
                parametresRechercheVO.setDateDebut(dfPeriode.selectedDate);
                parametresRechercheVO.setDateFin(dfPeriode.selectedDate);
            }
            paginateDatagridInstance.refreshPaginateDatagrid(true);
            dispatchEvent(new SearchPaginateDatagridEvent(SearchPaginateDatagridEvent.SEARCH, _parametresRechercheVO, true));
        }

        public function get paginateDatagridInstance():PaginateDatagrid
        {
            return this._paginateDatagridInstance;
        }

        public function set paginateDatagridInstance(paginateDatagridInstance:PaginateDatagrid):void
        {
            this._paginateDatagridInstance = paginateDatagridInstance;
            initColonnesRecherchables();
        }

        public function get colonnesRecherchables():ArrayCollection
        {
            return this._colonnesRecherchables;
        }

        public function set colonnesRecherchables(colonnesRecherchables:ArrayCollection):void
        {
            this._colonnesRecherchables = colonnesRecherchables;
        }

        public function get colonnesTriables():ArrayCollection
        {
            return this._colonnesTriables;
        }

        public function set colonnesTriables(colonnesTriables:ArrayCollection):void
        {
            this._colonnesTriables = colonnesTriables;
        }

        private function initColonnesRecherchables():void
        {
            colonnesTriables = new ArrayCollection();
            colonnesRecherchables = new ArrayCollection();
            var arrColonne:Array = paginateDatagridInstance.columns;
            for (var i:int = 0; i < arrColonne.length; i++)
            {
                if (paginateDatagridInstance.columns[i] is SortableColumn)
                {
                    var col:SortableColumn = paginateDatagridInstance.columns[i];
                    if (col.isSearchable)
                    {
                        colonnesRecherchables.addItem(paginateDatagridInstance.columns[i]);
                    }
                    if (col.isOrderable)
                    {
                        colonnesTriables.addItem(paginateDatagridInstance.columns[i]);
                    }
                }
            }
            //Initialisation du composant recherche :	
            parametresRechercheVO = new ParametresRechercheVO();
            //Recherche du texte
            parametresRechercheVO.setSearchTexte(colonnesRecherchables[0].columDBName, null);
            //Recherche du orderBY
            parametresRechercheVO.setOrderBY(colonnesTriables[0].columDBName, comboOrderBy.selectedItem.value);
        }

        [Inspectable(type="String", enumeration="defaut,periode,mois", defaultValue="defaut")]
        public function set typeRecherche(value:String):void
        {
            if (value != null)
                _typeRecherche = value;
            this.setCurrentState(_typeRecherche);
        }

        public function get typeRecherche():String
        {
            return _typeRecherche;
        }

        public function txtCleHandler():void
        {
            parametresRechercheVO.setSearchTexte((comboRecherche.selectedItem as SortableColumn).columDBName, txtCle.text);
        }

        public function comboRechercheHandler():void
        {
            if (comboRecherche.selectedIndex == -1)
            {
                comboRecherche.selectedIndex = 0;
            }
            else
            {
                comboTrieColonne.selectedIndex = comboRecherche.selectedIndex;
            }
        }

        public function comboTrieColonneHandler():void
        {
            if (comboTrieColonne.selectedIndex == -1)
            {
                comboTrieColonne.selectedIndex = 0;
            }
        }

        public function comboOrderByHandler():void
        {
            if (comboOrderBy.selectedIndex == -1)
            {
                comboOrderBy.selectedIndex = 0;
            }
        }

        public function dfDateDebHandler():void
        {
        }

        public function dfDateFinHandler():void
        {
        }

        public function dfDatePeriodeHandler():void
        {
        }

        public function set parametresRechercheVO(value:ParametresRechercheVO):void
        {
            _parametresRechercheVO = value;
        }

        public function get parametresRechercheVO():ParametresRechercheVO
        {
            return _parametresRechercheVO;
        }
    }
}