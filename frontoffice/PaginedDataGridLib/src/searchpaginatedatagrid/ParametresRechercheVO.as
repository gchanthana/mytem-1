/**
* @author Vincent Le Gallic
* @version Version 1.
* Cette classe gère les paramêtres de la recherche
* The ParametresRechercheVO class is for internal use only
*/
package searchpaginatedatagrid
{
	
	public class ParametresRechercheVO
	{
		private var _ORDER_BY:String;
		private var _SEARCH_TEXT:String;
		private var _DATE_DEB:Date;
		private var _DATE_FIN:Date;
		
		public function ParametresRechercheVO()
		{
		}
		
		/**
		 * The setOrderBY function is for internal use only
		 * Cette fonction permet de définit le parametre de recherche _ORDER_BY
		 * Ce parametre permet de trier le résultat de la procédure
		 * @param colonne La colonne sur la quelle la procédure doit trier
		 * @param valeur La valeur peut prendre ASC ou DESC
		 * Une fonction en Coldfusion permet de séparer la clé et la valeur grâce à la virgule entre les deux paramêtres
		 **/		
		internal function setOrderBY(colonne : String,valeur:String):void
		{
			if(colonne)
			{
				if(valeur != "ASC" && valeur != "DESC" )
				{
					valeur = "ASC";
				}
				_ORDER_BY=colonne+","+valeur;	
			}		
		}
		/**
		 * The setSearchTexte function is for internal use only
		 * Cette fonction permet de définir le parametre de recherche _SEARCH_TEXT
		 * Ce parametre permet de trier le résultat de la procédure
		 * @param colonne La colonne sur la quelle la procédure doit rechercher
		 * @param valeur La valeur est une chaine.
		 * Une fonction en Coldfusion permet de séparer la clé et la valeur grâce à la virgule entre les deux paramêtres
		 * 
		 **/		
		internal function setSearchTexte(colonne : String,valeur:String):void
		{
			
			if(colonne)
			{
				if(!valeur)
				{
					valeur="";
				}
				_SEARCH_TEXT=colonne+","+valeur;	
			}		
		}
		/**
		 * The setDateDebut function is for internal use only
		 * Cette fonction permet de définir le parametre de recherche _DATE_DEBUT
		 * @param valeur La valeur est une Date.
		 * 
		 **/	
		internal function setDateDebut(valeur:Date):void
		{
			_DATE_DEB = valeur;
		}
		/**
		 * The setDateFin function is for internal use only
		 * Cette fonction permet de définir le parametre de recherche _DATE_DEBUT
		 * @param valeur La valeur est une Date.
		 * 
		 **/	
		internal function setDateFin(valeur:Date):void
		{
			_DATE_FIN = valeur;
		}
		public function get DATE_DEB():Date
		{
			return _DATE_DEB;
		}
		public function get DATE_FIN():Date
		{
			return _DATE_FIN;
		}
		public function get ORDER_BY():String
		{
			if(_ORDER_BY)
			{
				return _ORDER_BY;
			}
			else
			{
				return "ASC"
			}
			
		}
		public function get SEARCH_TEXT():String
		{
			if(_SEARCH_TEXT)
			{
				return _SEARCH_TEXT;
			}
			else
			{
				return "";
			}
			
		}
	}
}