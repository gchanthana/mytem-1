package ihm
{
	import composants.util.ConsoviewAlert;
	
	import entity.ActualiteVO;
	import entity.ApplicationVO;
	import entity.CodeAction;
	
	import event.FctGestionCommunicationEvent;
	
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	
	import ihm.fiche.FicheActualiteIHM;
	import ihm.popup.PopupCopyActualiteIHM;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.DragEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.FocusManager;
	import mx.managers.IFocusManagerContainer;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import service.FctGestionCommunicationService;
	
	import utils.abstract.AbstractFonction;
	import utils.interfaceClass.IFonction;
	
	public class FctGestionCommunicationImpl extends AbstractFonction implements IFonction
	{
		//----------- VARIABLES PUBLIQUES -----------//
		
		public var dgListeActualite				:DataGrid;
		public var dgActuVisible				:DataGrid;
		public var dgActuNonVisible				:DataGrid;
		
		public var boxActuHistorique			:Box;
		public var boxActuSlider				:VBox;
		
		public var btnVisualiser				:Button;
		public var btnValiderAction				:Button;
		public var btnNewActu					:Button;
		public var btnUpdateSlider				:Button;
		
		public var cboApplication				:ComboBox;
		public var cboLangue					:ComboBox;
		public var cboAction					:ComboBox;
		
		public var ckxAll						:CheckBox;
				
		public var tiFiltre						:TextInput;
		
		public var objComService				:FctGestionCommunicationService = new FctGestionCommunicationService();
		public var ficheActu					:FicheActualiteIHM;
		public var popupCopyActu				:PopupCopyActualiteIHM;
		
		[Bindable]
		public var visuAvailable				:Boolean = false;
		[Bindable]
		public var ckbAllEnable					:Boolean = false;
		
		[Bindable] 
		public var nbActuSelect					:int;
		
		//----------- VARIABLES PRIVEES -----------//
		
		private var _listeActualite					:ArrayCollection = new ArrayCollection();
		private var _listeActualiteVisibleByApp		:ArrayCollection = new ArrayCollection();
		private var _listeActualiteNonVisibleByApp	:ArrayCollection = new ArrayCollection();
		private var _listeApplication				:ArrayCollection = new ArrayCollection();
//		private var _listeLangue					:ArrayCollection = new ArrayCollection();
		private var _listeDragDrop					:Array = null;
		
		private var _boolInitApp					:Boolean = false;
		private var _boolInitLang					:Boolean = false;
		
		private var _codeappVisualised				:int = -1;
		private var _idlangueVisualised				:int = -1;
		
		private var _codelangueVisualised			:String = '';
		
		
		//----------- METHODES -----------//
		
		/* */
		public function FctGestionCommunicationImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initGestionCommunication);
		}
		
		/* appels de fonction au Creation Complete */
		private function initGestionCommunication(evt:Event):void
		{
			initListeners();
			initData();
		}
		
		/* listeners - Creation Complete */
		private function initListeners():void
		{
			objComService.myDatas.addEventListener(FctGestionCommunicationEvent.LISTE_APPLICATION_EVENT, listeApplicationHandler);
			objComService.myDatas.addEventListener(FctGestionCommunicationEvent.LISTE_LANGUE_EVENT, listeLangueHandler);
			objComService.myDatas.addEventListener(FctGestionCommunicationEvent.LISTE_ACTUALITE_EVENT, listeActualiteHandler);
			objComService.myDatas.addEventListener(FctGestionCommunicationEvent.COPY_ACTUALITE_EVENT, copyActualiteHandler);
			objComService.myDatas.addEventListener(FctGestionCommunicationEvent.DELETE_ACTUALITE_EVENT, deleteActualiteHandler);
			this.addEventListener(FctGestionCommunicationEvent.ACTUALITE_MISE_A_JOUR_EVENT,updateDataProviderhandler);
			
//			dgListeActualite.addEventListener(ListEvent.ITEM_CLICK, dgClickItem);
			cboApplication.addEventListener(ListEvent.CHANGE, getLangueByAppHandler);
			cboAction.addEventListener(ListEvent.CHANGE, getActionHandler);
			btnVisualiser.addEventListener(MouseEvent.CLICK, visualiserHandler);
			btnValiderAction.addEventListener(MouseEvent.CLICK, validerActionHandler);
			tiFiltre.addEventListener(Event.CHANGE, filterFunctionHandler);
			tiFiltre.addEventListener(FocusEvent.FOCUS_IN,filtreFocusIn);
			ckxAll.addEventListener(MouseEvent.CLICK, selectAllHandler);
			btnNewActu.addEventListener(MouseEvent.CLICK,openNewFicheActuHandler);
			btnUpdateSlider.addEventListener(MouseEvent.CLICK, validDisplaySliderHandler);
			
			dgActuNonVisible.addEventListener(DragEvent.DRAG_COMPLETE,dgCompleteNonVisibleHandler);
			dgActuVisible.addEventListener(DragEvent.DRAG_COMPLETE,dgCompleteVisibleHandler);
			
			this.addEventListener('FIRST_INIT_LISTES_APPS_ET_LANGUES', firstInitListesHandler);
		}
		
		/* recuperer data - Creation Complete */
		private function initData():void
		{
			initDataListeApplication();
		}
		
		/* appel du service pour liste application */
		private function initDataListeApplication():void
		{
			objComService.getListeApplication(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_APPLICATIONS);
		}
		
		/* appel du service pour liste langue */
		private function initDataListeAllLangue():void
		{
			var idlang:Number = (CvAccessManager.getSession().AppPARAMS.codeLangue.ID_LANGUE!=0) ? CvAccessManager.getSession().AppPARAMS.codeLangue.ID_LANGUE : 3; //langue de connexion
			objComService.getLangueByApp(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_LANGUES,-1,idlang);
		}
		
		/* ouvrir nouvelle Fiche Actu */
		private function openNewFicheActuHandler(me:MouseEvent):void
		{
			ficheActu = new FicheActualiteIHM();
			ficheActu.fctGestionCom = this; //objFctGestCom;
			PopUpManager.addPopUp(ficheActu,Application.application as DisplayObject,true);
			PopUpManager.centerPopUp(ficheActu);
		}
		
		/* voir le détail d'une adctualité */
		public function openDetailFicheActu(objActu:ActualiteVO):void
		{
			ficheActu = new FicheActualiteIHM();
			ficheActu.actuSelected = objActu;
			ficheActu.fctGestionCom = this;
			PopUpManager.addPopUp(ficheActu,Application.application as DisplayObject,true);
			PopUpManager.centerPopUp(ficheActu);
		}
		
		/* clic sur combobox action de masse - Handler */
		private function selectAllHandler(me:MouseEvent):void
		{
			var cpt:int = 0;
			for each(var item:ActualiteVO in listeActualite)
			{
				item.SELECTED = ckxAll.selected;
				if(item.SELECTED) cpt++;
			}
			listeActualite.refresh(); //pour afficher le cochage ou non
			
			// nb d'actualités selectionnée dans le DataGrid
			nbActuSelect = cpt;
		}
		
		/* liste des applications - Handler*/
		private function listeApplicationHandler(evt:Event):void
		{
//			cboApplication.dataProvider = ObjectUtil.copy(objComService.myDatas.listeApplication) as ArrayCollection;
			cboApplication.dataProvider = ObjectUtil.copy(objComService.myDatas.listeApplication) as ArrayCollection; //ne recupere que les données service (sans objApp en position 0 du Cbx)
			_listeApplication = objComService.myDatas.listeApplication;
//			cboApplication.dropdown.dataProvider = cboApplication.dataProvider;
			
			var lgthCbAppli:int = (cboApplication.dataProvider as ArrayCollection).length;
			
			if(lgthCbAppli > 1)
			{
				var objApp:Object = {LABEL:ResourceManager.getInstance().getString('CG05', 'Toutes_les_applications'), CODEAPP:-1}; //option supplémentaire ds ComboBox
				(cboApplication.dataProvider as ArrayCollection).addItemAt(objApp,0);
				cboApplication.selectedIndex = 0;
				_boolInitApp = true;
			}
			else if(lgthCbAppli == 1)
			{
				cboApplication.selectedIndex = 0;
				_boolInitApp = true;
			}
			else
			{
				cboApplication.selectedIndex = -1;
				cboApplication.prompt = ' - ';
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('CG05', 'Erreur_de_chargement_de_donn_es__veuillez_vous_reconnecter'),ResourceManager.getInstance().getString('CG05', 'Erreur'));
			}
			
			initDataListeAllLangue();
		}
		
		/* liste des actualités - Handler */
		private function listeActualiteHandler(evt:Event):void
		{
			listeActualite = objComService.myDatas.listeActualite;
			//on set la selection des checkbox à false et on met le label [selectionné] à 0
			ckxAll.selected=false;
			nbActuSelect=0;
		}
		
		/* liste des actualités non visible d'une application - Handler */
		private function listeActualiteNonVisibleByAppHandler(evt:Event):void
		{
			objComService.myDatas.removeEventListener(FctGestionCommunicationEvent.LISTE_ACTUALITE_EVENT, listeActualiteNonVisibleByAppHandler);
			listeActualite = objComService.myDatas.listeActualite;
			listeActualiteNonVisibleByApp.removeAll();
			for each(var itemNonVisible:ActualiteVO in listeActualite)
			{
				if(!itemNonVisible.VISIBLE)
				{
					listeActualiteNonVisibleByApp.addItem(itemNonVisible);
				}
			}
		}
		
		/* liste des actualités visible d'une application - Handler */
		private function listeActualiteVisibleByAppHandler(evt:Event):void
		{
			objComService.myDatas.removeEventListener(FctGestionCommunicationEvent.LISTE_ACTUALITE_EVENT, listeActualiteVisibleByAppHandler);
			listeActualiteVisibleByApp.addEventListener(CollectionEvent.COLLECTION_CHANGE, updateCollectionHandler);
			listeActualite = objComService.myDatas.listeActualite;
			listeActualiteVisibleByApp.removeAll();
			for each(var itemVisible:ActualiteVO in listeActualite)
			{
				if(itemVisible.VISIBLE)
				{
					listeActualiteVisibleByApp.addItem(itemVisible);
				}
			}
		}
		
		/* liste des langues - Handler */
		private function listeLangueHandler(evt:Event):void
		{
			cboLangue.dataProvider = objComService.myDatas.listeLangue;
//			cboLangue.dataProvider = ObjectUtil.copy(objComService.myDatas.listeLangue);
//			cboLangue.dropdown.dataProvider = cboLangue.dataProvider;
//			_listeLangue = objComService.myDatas.listeLangue;
			
			var lengthCboLangue:int = (cboLangue.dataProvider as ArrayCollection).length;			
			
			if(lengthCboLangue > 1)
			{
				var objLangue:Object = {LABEL:ResourceManager.getInstance().getString('CG05', 'Toutes_les_langues'),IDLANGUE:-1,CODELANGUE:''}; //option supplémentaire ds ComboBox
				(cboLangue.dataProvider as ArrayCollection).addItemAt(objLangue,0);
				cboLangue.selectedIndex = 0;
				_boolInitLang = true;
			}
			else if(lengthCboLangue == 1)
			{
				cboLangue.selectedIndex = 0;
				_boolInitLang = true;
			}
			else
			{
				cboLangue.prompt = ' - ';
				cboLangue.selectedIndex = -1;
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('CG05', 'Erreur_de_chargement_de_donn_es__veuillez_vous_reconnecter'),ResourceManager.getInstance().getString('CG05', 'Erreur'));
			}
			
			this.firstInitListesHandler(evt);
		}
		
		/* */
		private function updateDataProviderhandler(evt:Event):void
		{
			this.removeEventListener(FctGestionCommunicationEvent.ACTUALITE_MISE_A_JOUR_EVENT,updateDataProviderhandler);
			(dgListeActualite.dataProvider as ArrayCollection).refresh();
			(dgActuVisible.dataProvider as ArrayCollection).refresh();
			(dgActuNonVisible.dataProvider as ArrayCollection).refresh();
		}
		
		/* premiere initialisation des liste application et langues - Handler */
		private function firstInitListesHandler(evt:Event):void
		{
			var codeapp:int = (cboApplication.selectedItem.CODEAPP) ? cboApplication.selectedItem.CODEAPP : -1;
			var codelangue:String = (cboLangue.selectedItem) ? cboLangue.selectedItem.CODELANGUE : '';
			objComService.getListeActualite(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_ACTUALITES, codeapp, codelangue);
		}
		
		/* afficher liste des langues selon CODEAPP - Handler */
		private function getLangueByAppHandler(le:ListEvent):void
		{
			var codeapp:int = le.currentTarget.selectedItem.CODEAPP;
			var idlang:Number = (CvAccessManager.getSession().AppPARAMS.codeLangue.ID_LANGUE!=0) ? CvAccessManager.getSession().AppPARAMS.codeLangue.ID_LANGUE : 3; //langue de connexion
			objComService.getLangueByApp(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_LANGUES, codeapp, idlang);
		}
		
		/* lors de la selection d'un item du comboBox - Handler */
		private function getActionHandler(le:ListEvent):void
		{
			btnValiderAction.enabled = true;
		}

		/* afficher liste article selon parametres application/langues - Handler */
		private function visualiserHandler(me:MouseEvent):void
		{
			var lengthCbxAppli:int = (cboApplication.dataProvider as ArrayCollection).length;
			var lengthCbxLangue:int = (cboLangue.dataProvider as ArrayCollection).length;
			var boolVisuAvailable:Boolean = false;
			btnUpdateSlider.enabled = false;
			btnValiderAction.enabled = true;
			
			listeActualiteNonVisibleByApp.removeAll();
			listeActualiteVisibleByApp.removeAll();
			
			if(lengthCbxAppli < 1 || lengthCbxLangue < 1)
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('CG05', 'impossible_d_afficher_un_r_sultat___donn_es_manquantes'),ResourceManager.getInstance().getString('CG05', 'Erreur'));
			}
			else
			{
				if((cboAction.dataProvider as ArrayCollection).length > 1)
				{
					(cboAction.dataProvider as ArrayCollection).removeItemAt(0);
					cboAction.selectedIndex = 0;
				}
				
				codeappVisualised = cboApplication.selectedItem.CODEAPP;
				idlangueVisualised = cboLangue.selectedItem.IDLANGUE;
				codelangueVisualised = this.getFormatLangue(cboLangue.selectedItem.CODELANGUE);
				
				objComService.getListeActualite(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_ACTUALITES, codeappVisualised, codelangueVisualised);
				// si une appli et une langue sont choisis
				if(codeappVisualised != -1 && idlangueVisualised != -1)
				{
					btnValiderAction.enabled = false;
					btnUpdateSlider.enabled = true;
					boolVisuAvailable = true;
					
					cboAction.prompt = ResourceManager.getInstance().getString('CG05', 'Choisir_une_action');
					var objAction:Object = {LABEL:ResourceManager.getInstance().getString('CG05', 'Copier_vers_autre_s__application_s_'), ID:0};
					(cboAction.dataProvider as ArrayCollection).addItemAt(objAction,0);
					cboAction.selectedIndex = -1;
					
					// listeners sur les deux datagrids
					objComService.myDatas.addEventListener(FctGestionCommunicationEvent.LISTE_ACTUALITE_EVENT, listeActualiteNonVisibleByAppHandler);
					objComService.myDatas.addEventListener(FctGestionCommunicationEvent.LISTE_ACTUALITE_EVENT, listeActualiteVisibleByAppHandler);
				}
				visuAvailable = boolVisuAvailable;
			}
		}
		
		private function getFormatLangue(codeLangue:String):String
		{
			var langue:String  = '';
			switch (codeLangue)//le code langue n'a pas le format que requiert la procédure, donc on adapte
			{
				case 'FR': langue='fr_FR';
					break;
				case 'EN': langue='en_GB';
					break;
				case 'ES': langue='es_ES';
					break;
				case 'NL': langue='nl_NL';
					break;
			}
			
			return langue;
		}
		
		/* au click sur bouton valider pour affichage actus dans Slider - Handler */
		private function validDisplaySliderHandler(me:MouseEvent):void
		{
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('CG05', 'Etes_vous_sur_de_vouloir_afficher_actualites_accueil'), ResourceManager.getInstance().getString('CG05', 'Communication'), resultAlertUpdateSliderActu);
			objComService.myDatas.addEventListener(FctGestionCommunicationEvent.UPDATE_SLIDER_ACTUALITE_EVENT,updateSliderActualiteHandler);
		}
		
		/* appel de la procedure de modification des actualités du slider */
		private function updateSliderActualite():void
		{
			var codeapp:int = codeappVisualised;
			var idlangue:int = idlangueVisualised;
			objComService.updateSliderActualite(CvAccessManager.CURRENT_FUNCTION, CodeAction.UPDATE_SLIDER_ACTUALITE, codeapp, idlangue, xmlPositionActu());
		}
		
		/* dès que la modif du slider est validée - Handler */
		private function updateSliderActualiteHandler(fgce:FctGestionCommunicationEvent):void
		{
			listeActualite.refresh();
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('CG05', 'La_mise_a_jour_du_slider_a_bien_ete_effectu_e'),Application.application as DisplayObject);
		}
		
		/* qd drag and drop est fini - Handler */
		protected function dgCompleteNonVisibleHandler(de:DragEvent):void
		{
			listeDragDrop = de.dragSource.dataForFormat('items') as Array;
			var itemDragDrop:ActualiteVO = listeDragDrop[0] as ActualiteVO;
			listeActualiteVisibleByApp.addEventListener(CollectionEvent.COLLECTION_CHANGE, updateCollectionHandler);
			
			if(itemDragDrop != null) //si un item a été dropé
			{
				for(var i:int=0;i<listeActualiteVisibleByApp.length;i++)
				{
					if(i > 4) // au dela de 5 actualités dans le datagrid - le '6eme' passe en 1ere position dans le datagrid des actus non visible
					{
						(listeActualiteVisibleByApp[i] as ActualiteVO).POSITION = 0;
						(listeActualiteVisibleByApp[i] as ActualiteVO).VISIBLE = false;
						listeActualiteNonVisibleByApp.addItemAt(listeActualiteVisibleByApp[i],0);
						listeActualiteVisibleByApp.removeItemAt(i);
					}
				}
				itemDragDrop.VISIBLE = true;
			}
		}
		
		/* qd drag and drop est fini - Handler */
		protected function dgCompleteVisibleHandler(de:DragEvent):void
		{
			listeDragDrop = de.dragSource.dataForFormat('items') as Array;
			var itemDragDrop:ActualiteVO = listeDragDrop[0] as ActualiteVO;
			listeActualiteVisibleByApp.addEventListener(CollectionEvent.COLLECTION_CHANGE, updateCollectionHandler);
			
			if(itemDragDrop != null)
			{
				itemDragDrop.POSITION = 0;
				itemDragDrop.VISIBLE = false;
			}
		}
		
		/* */
		private function updateCollectionHandler(ce:CollectionEvent):void
		{
			if(ce.kind == CollectionEventKind.ADD || ce.kind == CollectionEventKind.REMOVE)
			{
				for(var i:int; i<listeActualiteVisibleByApp.length;i++)
				{
					listeActualiteVisibleByApp[i].POSITION = i+1;
				}
			}
		}
		
		/* créer un xml */
		private function xmlPositionActu():String
		{
			var myXML : String = '<actusvisibles>';
			for each(var item:ActualiteVO in listeActualiteVisibleByApp)
			{
				myXML += '<actuvisible>';
				myXML += '<idactualite>' + item.IDACTUALITE + '</idactualite>';
				myXML += '<position>' + item.POSITION + '</position>';
				myXML += '<visible>' + int(item.VISIBLE) + '</visible>';
				myXML += '</actuvisible>';
			}
			myXML += '</actusvisibles>';
			return myXML;
		}
		
		/* selection d'une action de masse - Handler */ 
		private function validerActionHandler(me:MouseEvent):void
		{
			var idAction:int = cboAction.selectedItem.ID;
			var boolDelete:Boolean = false;
			
			for each(var item:ActualiteVO in listeActualite)
			{
				if(item.SELECTED)
				{
					boolDelete = true;
					break;
				}
			}
			
			if(boolDelete)
			{
				switch(idAction)
				{
					case 0: openPopupCopyActu(); break;
					case 1: ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('CG05', 'Etes_vous_sur_de_vouloir_supprimer_actualites_selectionnees'), ResourceManager.getInstance().getString('CG05', 'Gestion_des_communications'), resultAlertDelete); break;
					default: ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('CG05', 'veuillez_choisir_une_action'), ResourceManager.getInstance().getString('CG05', 'Erreur')); break;
				}
			}
			else{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('CG05', 'veuillez_selectionner_au_moins_une_case___cocher'), ResourceManager.getInstance().getString('CG05', 'Erreur'));
			}
		}
		
		/* ouvrir la popup contenant la liste les applis où copier les actualités */
		private function openPopupCopyActu():void
		{
			popupCopyActu = new PopupCopyActualiteIHM();
			
			popupCopyActu.listeApp = _listeApplication;
			popupCopyActu.codeapp = _codeappVisualised;
			popupCopyActu.idlangue = _idlangueVisualised;
			PopUpManager.addPopUp(popupCopyActu,Application.application as DisplayObject,true);
			PopUpManager.centerPopUp(popupCopyActu);
			
			popupCopyActu.btnValider.addEventListener(MouseEvent.CLICK, btnValiderCopy);
		}
		
		/* lors de la validation de l'action 'copier vers une autre application' - Handler */
		private function btnValiderCopy(me:MouseEvent):void
		{
			var listeCopyApp:ArrayCollection = popupCopyActu.proposedApp;
			var listeActu:String = '';
			var comma:String;
			var cpt:int = 0;
			
			for each(var itemActu:ActualiteVO in listeActualite)
			{
				if(itemActu.SELECTED)
				{
					if(cpt == 0) comma = ''; 
					else comma = ',';
					
					listeActu += comma + itemActu.IDACTUALITE;
					cpt++;
				}
			}
			for each(var itemApp:ApplicationVO in listeCopyApp)
			{
				if(itemApp.SELECTED)
				{
					objComService.copyActualite(CvAccessManager.CURRENT_FUNCTION, CodeAction.COPY_ACTUALITE_TO_OTHER_APPLICATION, listeActu, itemApp.CODEAPP);
				}
			}
		}
		
		/* suppression des actualités selectionnées */
		private function deleteActualite():void
		{
			var listeActu:String = '';
			var cpt:int = 0;
			var comma:String;
			var arrayIdActu:Array = new Array();
			
			for each(var item:ActualiteVO in listeActualite)
			{
				if(item.SELECTED)
				{
					arrayIdActu.push(item.IDACTUALITE);
					
					if(cpt == 0) comma = '';
					else comma = ',';
					
					listeActu += comma + item.IDACTUALITE;
					cpt++;
				}
			}
			objComService.deleteActualite(CvAccessManager.CURRENT_FUNCTION, CodeAction.DELETE_ACTUALITE, listeActu, arrayIdActu);
		}
		
		/* */
		private function copyActualiteHandler(evt:Event):void
		{
			dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.ACTUALITE_MISE_A_JOUR_EVENT));
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('CG05', 'La_copie_de_l_actualit__vers_une_autre_application_a_bien_ete_effectu_e'),Application.application as DisplayObject);
			PopUpManager.removePopUp(popupCopyActu);
		}
		
		/* */
		private function deleteActualiteHandler(evt:Event):void
		{
			btnVisualiser.dispatchEvent(new MouseEvent(MouseEvent.CLICK));// simuler un click du btn visualiser pour reload la liste
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('CG05', 'La_suppression_d_actualit__s__a_bien_ete_effectu_e'),Application.application as DisplayObject);
		}
		
		/* */
		private function resultAlertUpdateSliderActu(ce:CloseEvent):void
		{
			if(ce.detail == Alert.OK)
				updateSliderActualite();
		}
		
		/* Resultat de confirmation - Handler */
		private function resultAlertDelete(ce:CloseEvent):void
		{
			if(ce.detail == Alert.OK)
				deleteActualite();
		}
		
		/* filtre lors de la saisie - Handler */
		private function filterFunctionHandler(evt:Event):void
		{
			listeActualite.filterFunction = filterFunctionActualite;
			listeActualite.refresh();
		}
		
		/* fonction de filtre */
		private function filterFunctionActualite(item:Object):Boolean
		{
			if(String(item.TITRE).toLowerCase().search(tiFiltre.text.toLowerCase()) != -1 || detectStyleStringFilter())
				return true;
			else
				return false;
		}
		
		/* quand le focus est dans le filtre - Handler */
		public function filtreFocusIn(fe:FocusEvent):void
		{
			if(detectStyleStringFilter()) tiFiltre.text = '';
			tiFiltre.setStyle('color','black');
			tiFiltre.setStyle('fontStyle','normal');
			tiFiltre.addEventListener(FocusEvent.MOUSE_FOCUS_CHANGE,filtreFocusOut); // methode appelée qu'apres le focusin. Evite les appels inutiles.
			Application.application.addEventListener('handleFocus',handleFocusHandler);
		}
		
		/* quand le focus est hors du filtre - Handler */
		public function filtreFocusOut(fe:FocusEvent):void
		{
			Application.application.dispatchEvent(new MouseEvent('handleFocus'));
			
			tiFiltre.removeEventListener(FocusEvent.MOUSE_FOCUS_CHANGE,filtreFocusOut);
			if(tiFiltre.text != '' && !detectStyleStringFilter())
			{
				tiFiltre.setStyle('color','black');
				tiFiltre.setStyle('fontStyle','normal');
			}
			else
			{
				tiFiltre.text = resourceManager.getString('CG05','Titre_de_l_actualit_');
				tiFiltre.setStyle('color','gray');
				tiFiltre.setStyle('fontStyle','italic');
			}
		}
		
		/* gere le focus lors du clic hors TextInput Filtre */
		private function handleFocusHandler(evt:MouseEvent):void 
		{
			Application.application.removeEventListener('handleFocus',handleFocusHandler);
			var fm:FocusManager = new FocusManager(Application.application as IFocusManagerContainer);
			
			//Since Flash Player can set focus on subcomponents as well as on components themselves, 
			//findFocusManagerComponent is used to find the component that either has focus or contains the subcomponent 
			//that has focus. If, for example, a TextField contained within a TextArea component has focus, findFocusManagerComponent 
			//will return the TextArea component, and not the TextField.  This being the case, we can definitely determine 
			//whether the target object of the MouseUp event is a component (or is part of a component).  If the target
			//object is NOT a component (nor contained within one), then we clear all component focus.
			
			if(fm.findFocusManagerComponent(evt.target as InteractiveObject) is UIComponent){
				//target of MouseUp is either a UIComponent, or is contained within a UIComponent, so do nothing.
			}else{
				//target of MouseUp is neither a UIComponent, nor is it contained within a UIComponent, so set component focus to null.
				stage.focus = null; //fm.setFocus(null);
			}
		}
		
		/* detecte si le filtre par defaut est present */
		private function detectStyleStringFilter():Boolean
		{
			if(tiFiltre.getStyle('color') == '8421504' 
				&& tiFiltre.getStyle('fontStyle') == 'italic'
				&& tiFiltre.text == resourceManager.getString('CG05','Titre_de_l_actualit_')
				) //8421504 = gray
				return true;
			else
				return false;
		}
		
		/* recuperer les datas d'un article où le ckb a été coché */
//		public function dgClickItem(le:ListEvent):ActualiteVO
//		{
//			return (dgListeActualite.selectedItem as ActualiteVO);
//		}
		
		
		
		//----------- GETTERS AND SETTERS -----------//

		[Bindable]
		public function get listeActualite():ArrayCollection
		{
			return _listeActualite;
		}
		
		public function set listeActualite(value:ArrayCollection):void
		{
			_listeActualite = value;
		}
		
		[Bindable]
		public function get listeActualiteVisibleByApp():ArrayCollection
		{
			return _listeActualiteVisibleByApp;
		}

		public function set listeActualiteVisibleByApp(value:ArrayCollection):void
		{
			_listeActualiteVisibleByApp = value;
		}
		
		[Bindable]
		public function get listeActualiteNonVisibleByApp():ArrayCollection
		{
			return _listeActualiteNonVisibleByApp;
		}

		public function set listeActualiteNonVisibleByApp(value:ArrayCollection):void
		{
			_listeActualiteNonVisibleByApp = value;
		}

		public function get listeDragDrop():Array
		{
			return _listeDragDrop;
		}

		public function set listeDragDrop(value:Array):void
		{
			_listeDragDrop = value;
		}
		
		public function get codeappVisualised():int
		{
			return _codeappVisualised;
		}

		public function set codeappVisualised(value:int):void
		{
			_codeappVisualised = value;
		}

		public function get idlangueVisualised():int
		{
			return _idlangueVisualised;
		}

		public function set idlangueVisualised(value:int):void
		{
			_idlangueVisualised = value;
		}
		
		public function get codelangueVisualised():String
		{
			return _codelangueVisualised;
		}
		
		public function set codelangueVisualised(value:String):void
		{
			_codelangueVisualised = value;
		}
		
//		[Bindable]
//		public function get listeApplication():ArrayCollection
//		{
//			return _listeApplication;
//		}
//		
//		public function set listeApplication(value:ArrayCollection):void
//		{
//			_listeApplication = value;
//		}

//		[Bindable]
//		public function get listeLangue():ArrayCollection
//		{
//			return _listeLangue;
//		}
//		
//		public function set listeLangue(value:ArrayCollection):void
//		{
//			_listeLangue = value;
//		}
	}
}