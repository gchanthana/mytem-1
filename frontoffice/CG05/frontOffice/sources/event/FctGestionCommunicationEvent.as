package event
{
	import flash.events.Event;

	public class FctGestionCommunicationEvent extends Event
	{
		// VARIABLES
		public static const LISTE_APPLICATION_EVENT 		:String = "LISTE_APPLICATION_EVENT";
		public static const LISTE_LANGUE_EVENT 				:String = "LISTE_LANGUE_EVENT";
		public static const LISTE_ACTUALITE_EVENT 			:String = "LISTE_ACTUALITE_EVENT";
		public static const INFOS_ACTUALITE_EVENT 			:String = "INFOS_ACTUALITE_EVENT";
		public static const CREATE_ACTUALITE_EVENT			:String = "CREATE_ACTUALITE_EVENT";
		public static const ADD_FILE_TO_ACTUALITE_EVENT		:String = "ADD_FILE_TO_ACTUALITE_EVENT";
		public static const ACTUALITE_MISE_A_JOUR_EVENT		:String = "ACTUALITE_MISE_A_JOUR_EVENT";
		public static const UPDATE_SLIDER_ACTUALITE_EVENT	:String = "UPDATE_SLIDER_ACTUALITE_EVENT";
		public static const COPY_ACTUALITE_EVENT			:String = "COPY_ACTUALITE_EVENT";
		public static const DELETE_ACTUALITE_EVENT			:String = "DELETE_ACTUALITE_EVENT";
		public static const DELETE_PJ_ACTUALITE_EVENT		:String = "DELETE_PJ_ACTUALITE_EVENT";
		public static const PJ_ACTUALITE_EVENT				:String = "PJ_ACTUALITE_EVENT";
		public static const RENAME_PJ_ACTUALITE_EVENT		:String = 'RENAME_PJ_ACTUALITE_EVENT';
		
		// FONCTION PUBLIQUE
		public function FctGestionCommunicationEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}