package event.modGestionOffre.fiche.ficheDetailClient
{
	import flash.events.Event;
	
	public class FicheDetailClientEvent extends Event
	{
		public static var proc_DETAIL_CLIENT_CONTRACTUEL_EVENT:String = "proc_DETAIL_CLIENT_CONTRACTUEL_EVENT";
		
		public function FicheDetailClientEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}