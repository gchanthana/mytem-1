package event.modGestionOffre.fonction.FctGestionProfil
{
	import flash.events.Event;

	public class FctGestionProfilEvent extends Event
	{
		public static const LISTE_CLIENTS_EVENT : String = "LISTE_CLIENTS_EVENT";
		public static const LISTE_PROFILS_EVENT : String = "LISTE_PROFILS_EVENT";
		public static const CREATION_PROFIL_EVENT : String = "CREATION_PROFIL_EVENT";
		public static const SUPPRESSION_PROFIL_EVENT : String = "SUPPRESSION_PROFIL_EVENT";
		public static const MODIFICATION_PROFIL_EVENT : String = "MODIFICATION_PROFIL_EVENT";
		
		public function FctGestionProfilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}