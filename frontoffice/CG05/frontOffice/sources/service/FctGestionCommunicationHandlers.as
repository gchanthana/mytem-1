package service
{
	import composants.util.ConsoviewAlert;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	import utils.MessageAlerte;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Envoie les données reçues à la classe <code>FctGestionCommunicationDatas</code>,
	 * au retour des procédures appelées dans <code>FctGestionCommunicationServices</code>.
	 * 
	 * Version 	: 1.0
	 * Date 	: 13.08.2013
	 * 
	 * </pre></p>
	 */
	
	public class FctGestionCommunicationHandlers
	{
		//------------ VARIABLES ------------//
		private var myDatas:FctGestionCommunicationDatas;
		
		
		//------------ METHODES ------------//
		/* */
		public function FctGestionCommunicationHandlers(instanceDatas:FctGestionCommunicationDatas)
		{
			this.myDatas = instanceDatas;
		}
		
		/* */
		public function getListeApplicationHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatGetListeApplication(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR),MessageAlerte.msgErrorPopupTitle);
			}
		}
		
		/* */
		public function getListeActualiteHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatGetListeActualite(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR),MessageAlerte.msgErrorPopupTitle);
			}
		}
		
		/* */
		public function getLangueByAppHandler(re:ResultEvent):void
		{
			if(re.result.DATA.RESULT is ArrayCollection && (re.result.DATA.RESULT as ArrayCollection).length > 0)
			{
				myDatas.treatGetLangueByApp(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle);
			}
		}
		
		/* */
		public function getInfosActualiteHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatGetInfosActualite(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle);
			}
		}
		
		/* */
		public function getPJActualiteHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatGetPJActualite(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle);
			}
		}
		
		/* */
		public function createActualiteHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatCreateActualite(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle, null);
			}
		}
		
		/* */
//		public function addFileToActualiteHandler(re:ResultEvent):void
//		{
//			if(re.result != null) //RESULT
//			{
//				myDatas.treatAddFileToActualite(re.result);
//			}
//			else
//			{
//				ConsoviewAlert.afficherError(MessageCommunication.AddFileToActualite, MessageAlerte.msgErrorPopupTitle, null);
//			}
//		}
		
		/* */
		public function updateSliderActualiteHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatUpdateSliderActualite(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle, null);
			}
		}
		
		/* */
		public function copyActualiteHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatCopyActualite(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle, null);
			}
		}
		
		/* */
		public function deleteActualiteHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatDeleteActualite(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle, null);
			}
		}
		
		/* */
		public function deletePJActualiteHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatDeletePJActualite(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle, null);
			}
		}
		
		public function renameLinkPJActualiteHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatRenamePJActualite(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle, null);
			}
		}
	}
}