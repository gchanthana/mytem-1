package service
{
	import entity.ActualiteVO;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	import mx.utils.ObjectUtil;
	
	import utils.abstract.AbstractRemoteService;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Appelle les procédures liées aux Gestion des communications. 
	 * 
	 * Version 	: 1.0
	 * Date 	: 13.08.2013
	 * </pre></p>
	 */
	
	public class FctGestionCommunicationService extends AbstractRemoteService
	{
		//------------ VARIABLES ------------//
		
		public var myDatas		:FctGestionCommunicationDatas;
		public var myHandlers 	:FctGestionCommunicationHandlers;
		
		//------------ METHODES ------------//
		
		/* */
		public function FctGestionCommunicationService()
		{
			this.myDatas 	= new FctGestionCommunicationDatas();
			this.myHandlers = new FctGestionCommunicationHandlers(myDatas);
		}
		
		/* recuperer lal iste des applications disponibles */
		public function getListeApplication(key:String, code:String):void
		{
			doRemoting(myHandlers.getListeApplicationHandler,key, code);
		}
		
		/* recuperer la liste des langue disponible selon l'application choisie */
		public function getLangueByApp(key:String, code:String, codeapp:int, idlangue:Number):void
		{
			var data:Object = new Object();
			data.CODEAPP = codeapp;
			data.IDLANGUE = idlangue;
			doRemoting(myHandlers.getLangueByAppHandler,key, code,data);
		}
		
		/* recuperer la liste de toutes les actualités selon les parametres envoyés (app et idlangue) */
		public function getListeActualite(key:String, code:String, codeapp:int, codelangue:String):void
		{
			var data:Object = new Object();
			data.CODEAPP = codeapp;
			data.CODELANGUE = codelangue;
			doRemoting(myHandlers.getListeActualiteHandler,key, code,data);
		}
		
		/* recuperer les infos d'une actualité */
		public function getInfosActualite(key:String, code:String, idactualite:int):void
		{
			var data:Object = new Object();
			data.IDACTUALITE = idactualite;
			doRemoting(myHandlers.getInfosActualiteHandler,key, code,data);
		}
		
		/* recuperer les infos d'une piece jointe d'une actualité */
		public function getPJActualite(key:String, code:String, idactualite:int):void
		{
			var data:Object = new Object();
			data.IDACTUALITE = idactualite;
			doRemoting(myHandlers.getPJActualiteHandler,key, code,data);
		}
		
		/* créer une actualité */
		public function createActualite(key:String, code:String, data:ActualiteVO):void
		{
			doRemoting(myHandlers.createActualiteHandler,key, code, data);
		}
		
		/* ajouter une piece jointe en BDD à une actualité*/
//		public function addFileToActualite(key:String, code:String, data:Object):void
//		{
//			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
//				_ApiConsoleGestion,
//				"execute",
//				myHandlers.addFileToActualiteHandler);
//			
//			RemoteObjectUtil.callService(op,key,code,data);
//		}
		
		/* mettre à jour ce qui apparaittra dans le slider News de l'accueil*/
		public function updateSliderActualite(key:String, code:String, codeapp:int, idlangue:int, xmlUpdateSlider:String):void
		{
			var data:Object = new Object;
			data.CODEAPP = codeapp;
			data.IDLANGUE = idlangue;
			data.ACTUVISIBLE = xmlUpdateSlider;
			
			doRemoting(myHandlers.updateSliderActualiteHandler,key, code,data);
		}
		
		/* copier une actualité */
		public function copyActualite(key:String, code:String, listeIdActu:String, codeapp:int):void
		{
			var data:Object = new Object;
			data.LISTEACTU = listeIdActu;
			data.CODEAPP = codeapp;
			
			doRemoting(myHandlers.copyActualiteHandler,key, code,data);
		}
		
		/* supprimer une actualité et ce qui lui ait affilié (PJ)*/
		public function deleteActualite(key:String, code:String, listeIdActu:String, arrayIdActu:Array):void
		{
			var data:Object = new Object;
			data.LISTEACTU = listeIdActu;
			data.ARRAYIDACTU = arrayIdActu;
			
			doRemoting(myHandlers.deleteActualiteHandler,key, code,data);
		}
		
		/* supprimer la piece jointe d'une actualité */
		public function deletePJActualite(key:String, code:String, idActualite:int, UUID:String):void
		{
			var data:Object = new Object;
			data.IDACTUALITE = idActualite;
			data.UUID = UUID;
			
			doRemoting(myHandlers.deletePJActualiteHandler,key, code,data);
		}
		
		/* renome la piece jointe si elle est lié par un clik*/
		public function renameLinkPJActualite(key:String, code:String, nameLinkedPJ:String):void
		{
			var data:Object = new Object;
			data.nameLinkedPJ = nameLinkedPJ;
			
			doRemoting(myHandlers.renameLinkPJActualiteHandler,key, code,data);
		}
	}
}