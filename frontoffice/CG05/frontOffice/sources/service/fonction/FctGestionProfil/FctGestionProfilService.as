package service.fonction.FctGestionProfil
{
	import entity.ProfilVO;
	
	import mx.utils.ObjectUtil;
	
	import utils.abstract.AbstractRemoteService;
	
	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Appelle les procédures liées à la Gestion des profils. 
	 * 
	 * Version 	: 1.0
	 * Date 	: 05.06.2013
	 * </pre></p>
	 */
	
	public class FctGestionProfilService extends AbstractRemoteService
	{
		// VARIABLES -----------------------------------------------------------------------------
		
		public var myDatas		:FctGestionProfilDatas;
		public var myHandlers 	:FctGestionProfilHandlers;
		
		// PUBLIC FUNCTIONS ----------------------------------------------------------------------
		
		public function FctGestionProfilService()
		{
			this.myDatas 	= new FctGestionProfilDatas();
			this.myHandlers = new FctGestionProfilHandlers(myDatas);
		}
		
		public function getClientsParUtilisateur(key:String, code:String, idUser:Number=0):void
		{
			var data:Object = new Object();
			idUser = CvAccessManager.getSession().USER.CLIENTACCESSID;
			data.id_user 	= idUser;
			doRemoting(myHandlers.getClientsParUtilisateurHandler,key,code,data);
		}
		
		public function getProfilsParClient(key:String, code:String, idClient:int, filtre:String=""):void
		{
			var data:Object = new Object();
			data.id_client = idClient;
			data.searchInput = filtre;
			doRemoting(myHandlers.getProfilsParClientHandlers,key,code,data);
		}
		
		public function creerProfil(key:String, code:String, idRacine:Number, myProfil:ProfilVO):void
		{
			var data:Object 	= new Object();
			data.nom 			= myProfil.libelle;
			data.description 	= myProfil.description;
			data.idRacine		= idRacine;
			data.idProfil		=	0; // nouveau
			data.createur		= CvAccessManager.getSession().USER.CLIENTACCESSID;
			doRemoting(myHandlers.creerProfilHandler,key, code, data);
		}
		
		public function modifierProfil(key:String, code:String, idRacine:Number, myProfil:ProfilVO):void
		{
			var data:Object 	= new Object();
			data.nom 			= myProfil.libelle;
			data.description 	= myProfil.description;
			data.nbrUtilisateur	= myProfil.nbrUtilisateur;
			data.dateCreation	= myProfil.dateCreation;
			data.createur		= CvAccessManager.getSession().USER.CLIENTACCESSID;
			data.idProfil		= myProfil.Id;//l'id du profil à modifier
			data.idRacine		= idRacine;
			doRemoting(myHandlers.modifierProfilHandler,key, code, data);
		}
		
		public function supprimerProfil(key:String, code:String, idProfil:Number):void
		{
			var data:Object 	= new Object();
			data.idProfil			= idProfil;
			doRemoting(myHandlers.supprimerProfilHandler,key, code, data);	
		}
		
		public function getListeFonctionUser(key:String, code:String, idRacine:Number):void
		{
			var data:Object 	= new Object();
			data.idRacine			= idRacine;
			doRemoting(myHandlers.getListeFonctionUserHandler,key, code, data);
		}
		
		public function getListeFonctionProfil(key:String, code:String, idProfil:Number):void
		{
			var data:Object 	= new Object();
			data.idProfil			= idProfil;
			doRemoting(myHandlers.getListeFonctionProfilHandler,key, code, data);
		}
		
	}
}