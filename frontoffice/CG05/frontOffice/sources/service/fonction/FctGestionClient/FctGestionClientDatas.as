package service.fonction.FctGestionClient
{
	import entity.Client;
	import entity.ClientVO;
	
	import event.fonction.FctGestionClient.FctGestionClientEvent;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class FctGestionClientDatas extends EventDispatcher
	{
		private var _listeClients:ArrayCollection = new ArrayCollection();
		
		public function treatListeClients(retour:Object):void
		{
			listeClients.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int = 0; i < tailleRet; i++)
			{
				var client:ClientVO = new ClientVO();
				client.fill(retour.RESULT[i]);
				this.listeClients.addItem(client);
			}
			
			
			this.dispatchEvent(new FctGestionClientEvent(FctGestionClientEvent.LISTE_CLIENT));
		}
		
		[Bindable]
		public function get listeClients():ArrayCollection { return _listeClients; }
		
		public function set listeClients(value:ArrayCollection):void
		{
			if (_listeClients == value)
				return;
			_listeClients = value;
		}
	}
}