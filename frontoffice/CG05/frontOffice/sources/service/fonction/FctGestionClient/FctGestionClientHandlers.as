package service.fonction.FctGestionClient
{
	import composants.util.ConsoviewAlert;
	
	import mx.rpc.events.ResultEvent;
	
	import utils.MessageAlerte;

	public class FctGestionClientHandlers
	{
		private var myDatas:FctGestionClientDatas;
		
		public function FctGestionClientHandlers(iDatas:FctGestionClientDatas)
		{
			this.myDatas = iDatas;
		}
		
		public function getListeClientsHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatListeClients(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
		}
	}
}