package service.fonction.FctGestionFonction
{
	import entity.FonctionVO;
	import entity.ParametreFonctionVO;
	
	import event.fonction.fctGestionFonction.FctGestionFonctionEvent;
	
	import flash.events.Event;
	
	import utils.abstract.AbstractRemoteService;

	public class FctGestionFonctionService extends AbstractRemoteService
	{
		public var myHandlers	:FctGestionFonctionHandlers;
		[Bindable]	public var myDatas		:FctGestionFonctionDatas;
		
		public function FctGestionFonctionService()
		{
			this.myDatas = new FctGestionFonctionDatas();
			this.myHandlers = new FctGestionFonctionHandlers(myDatas);
			
		}
		
		public function getListeNiveaux(key:String, code:String, filtre:String = ""):void
		{
			var data:Object = new Object();
			data.searchInput = filtre;
			doRemoting(myHandlers.getListeNiveauxHandler,key, code, data);
		}
		public function getListeFonctions(key:String, code:String, filtre:String = ""):void
		{
			var data:Object = new Object();
			doRemoting(myHandlers.getListeFonctionsHandler,key, code, data);
		}
		public function getDroitsSpecifique(key:String, code:String, idFonction:int):void
		{
			var data:Object = new Object();
			data.idFonction = idFonction;
			data.idlangue = CvAccessManager.getSession().USER.GLOBALIZATION;
			doRemoting(myHandlers.getDroitsSpecifique,key, code, data);
		}
		
		public function createFonction(key:String, code:String, newFct:FonctionVO):void
		{
			var data:Object = new Object();
			data.nomFonction 	= newFct.nomFonction;
			data.commentFct 	= newFct.commentFct;
			data.idCategorie 	= newFct.idCategorie; // categorie equivalent à Univers
			data.keyFct 		= newFct.keyFonction;// 
			data.idFonction		= 0;//nouvelle fonction
			data.droit_niveau	= "1";
//			doRemoting(myHandlers.createFonctionHandler,key, code, data);	
		}
		
		public function updateFonction(key:String, code:String, arrFctChanged:Array):void
		{
			var dt:Object = new Object();
			for each(var obj:FonctionVO in arrFctChanged)
			{
				var data:Object = new Object();
				data.nomFonction 	= obj.nomFonction;
				data.commentFct 	= obj.commentFct;
				data.idCategorie 	= obj.idCategorie;
				data.keyFct 		= obj.keyFonction;
				data.idFonction		= obj.idFonction;
				data.droit_niveau	= obj.getNiveauxHasAcces();
			}	
//			doRemoting(myHandlers.updateFonctionHandler,key, code, data);
		}
		
		public function deleteFonction(key:String, code:String, idfonction:Number):void
		{
			var data:Object = new Object();
			data.idfonction 	= idfonction;
//			doRemoting(myHandlers.deleteFonctionHandler,key, code, data);
		}
		
		public function createDroitFonction(key:String, code:String, droitFct:ParametreFonctionVO, listNiveaux:String):void
		{
			var data:Object = new Object();
			data.nomParamFct = droitFct.libelle;
			data.description = droitFct.description;
			data.niveaux 	 = listNiveaux;
			data.idFonction  = droitFct.idFonction;
//			doRemoting(myHandlers.createDroitFonctionHandler,key, code, data);
		}
		
		public function updateDroitFonction(key:String, code:String, paramFct:ParametreFonctionVO, listNiveaux:String):void
		{
			var data:Object = new Object();
			data.nomParamFonction	=paramFct.libelle;
			data.idFonction 		= paramFct.idFonction;
			data.idParamsFonction 	= paramFct.idParametreFonction;
			data.descriptParamFct	= paramFct.description;
			data.liste_niveau 		= listNiveaux;
//			doRemoting(myHandlers.updateDroitFonctionHandler,key, code, data);
		}
		
		public function deleteDroitFonction(key:String, code:String, idParamFct:int):void
		{ 
			var data:Object = new Object();
			data.idParamFct = idParamFct;
//			doRemoting(myHandlers.deleteDroitFonctionHandler,key, code, data);			
		}
		
		public function getListeUnivers(key:String, code:String):void
		{
			var data:Object = new Object();
			doRemoting(myHandlers.getListeUniversHandler , key, code, data);
		}
	}
}