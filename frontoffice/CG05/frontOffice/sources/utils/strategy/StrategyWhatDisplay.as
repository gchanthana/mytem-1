package utils.strategy
{
	import entity.Key;
	
	import flash.display.DisplayObject;
	import flash.utils.getDefinitionByName;
	
	import ihm.fonction.FctAccueil.FctAccueil;
	import ihm.fonction.FctGestionCatalogue.FctGestionCatalogue;
	import ihm.fonction.FctGestionClient.FctGestionClient;
	import ihm.fonction.FctGestionCollecte.FctGestionCollecte;
	import ihm.fonction.FctGestionCommunication.FctGestionCommunication;
	import ihm.fonction.FctGestionFonction.FctGestionFonction;
	import ihm.fonction.FctGestionPartenaire.FctGestionPartenaire;
	import ihm.fonction.FctGestionProfil.FctGestionProfil;
	import ihm.fonction.FctListeClient.FctListeClient;
	
	import mx.containers.VBox;
	
	
	public class StrategyWhatDisplay
	{
		private static var _listeKey:Array = [ 	new Key("HOME","FctAccueil"),
												new Key("LCLI","FctListeClient"),
												new Key("GCAT","FctGestionCatalogue"),
												new Key("GFCT","FctGestionFonction"),
												new Key("GPRO","FctGestionProfil"),
												new Key("GPAR","FctGestionPartenaire"),
												new Key("GCOM","FctGestionCommunication"),
												new Key("GCOL","FctGestionCollecte"),
												new Key("GCLI","FctGestionClient")
											];
		
		private static var dispObj:DisplayObject;
		
		
		public function StrategyWhatDisplay()
		{
			FctAccueil;
			FctListeClient;
			FctGestionFonction;
			FctGestionProfil;
			FctGestionPartenaire;
			FctGestionCommunication;
			FctGestionCollecte;
			FctGestionClient;
			
			
		}
		public static function execute(key:String):DisplayObject
		{
			dispObj = new VBox();
			
			for(var i:int = _listeKey.length-1;i > -1;i--)
			{
				if(_listeKey[i].KEY == key)
				{
					var pathClass:String;
					if((_listeKey[i] as Key).isFunction)
						pathClass = "ihm.fonction."+_listeKey[i].CLASSE+"."+_listeKey[i].CLASSE;
					else 
						pathClass = "ihm.menu."+_listeKey[i].CLASSE+"."+_listeKey[i].CLASSE;
					var ClassReference:Class = getDefinitionByName(pathClass) as Class;
					dispObj = new ClassReference();
					break;
				}
			}
			
			return dispObj;
		}
	}
}