package entity
{
	import mx.formatters.DateFormatter;

	[Bindable]
	public class ContratVO
	{
		private var _id					:int;
		private var _dateDebut			:Date;
		private var _dateFin			:Date;
		private var _dateRenouvellement	:Date;
		private var _objPlanTarifaire	:PlanTarifaireVO = new PlanTarifaireVO();
		private var _objCertification	:CertificationVO = new CertificationVO();
		private var _dateDebutPT		:Date;
		private var _dateFinPT			:Date;
		private var _isActive			:Boolean;
		
		public function ContratVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDFC_CONTRAT"))
					this.id = obj.IDFC_CONTRAT;	
				
				if(obj.hasOwnProperty("DATE_DEBUT"))
					this.dateDebut = obj.DATE_DEBUT as Date;
				
				if(obj.hasOwnProperty("DATE_FIN"))
					this.dateFin = obj.DATE_FIN as Date;

				if(obj.hasOwnProperty("DATE_RENOUVELLEMENT "))
					this.dateRenouvellement = obj.DATE_RENOUVELLEMENT  as Date;

				this.objCertification.fill(obj);
				
				this.objPlanTarifaire.fill(obj);
				
				if(obj.hasOwnProperty("DATE_DEBUT_PT"))
					this.dateDebutPT = obj.DATE_DEBUT_PT as Date;
				
				if(obj.hasOwnProperty("DATE_FIN_PT"))
					this.dateFinPT = obj.DATE_FIN as Date;
				
				if(obj.hasOwnProperty("IS_ACTIVE"))
					this.isActive = (obj.IS_ACTIVE == 1)?true:false;
				
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet ContratVO erroné ");
			}
			
		}
		
		// GETTERS ET SETTERS
		
		
		public function get isActive():Boolean { return _isActive; }
		
		public function set isActive(value:Boolean):void
		{
			if (_isActive == value)
				return;
			_isActive = value;
		}
		
		public function get id():int { return _id; }
		
		public function set id(value:int):void
		{
			if (_id == value)
				return;
			_id = value;
		}
		
		public function get dateFin():Date { return _dateFin; }
		
		public function set dateFin(value:Date):void
		{
			if (_dateFin == value)
				return;
			_dateFin = value;
		}
		
		public function get dateDebut():Date { return _dateDebut; }
		
		public function set dateDebut(value:Date):void
		{
			if (_dateDebut == value)
				return;
			_dateDebut = value;
		}
		
		
		public function get objPlanTarifaire():PlanTarifaireVO { return _objPlanTarifaire; }
		
		public function set objPlanTarifaire(value:PlanTarifaireVO):void
		{
			if (_objPlanTarifaire == value)
				return;
			_objPlanTarifaire = value;
		}
		
		public function get objCertification():CertificationVO { return _objCertification; }
		
		public function set objCertification(value:CertificationVO):void
		{
			if (_objCertification == value)
				return;
			_objCertification = value;
		}
		
		public function get dateFinPT():Date { return _dateFinPT; }
		
		public function set dateFinPT(value:Date):void
		{
			if (_dateFinPT == value)
				return;
			_dateFinPT = value;
		}
		
		public function get dateDebutPT():Date { return _dateDebutPT; }
		
		public function set dateDebutPT(value:Date):void
		{
			if (_dateDebutPT == value)
				return;
			_dateDebutPT = value;
		}
		
		public function get dateRenouvellement():Date { return _dateRenouvellement; }
		
		public function set dateRenouvellement(value:Date):void
		{
			if (_dateRenouvellement == value)
				return;
			_dateRenouvellement = value;
		}
		
	}
}