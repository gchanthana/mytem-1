package entity
{
	public class PlanTarifaireVO
	{
		private var _id:Number;
		private var _dateDebut		:Date;
		private var _dateFin		:Date;
		
		private var _objTypePlanTarif:TypePlanTarifVO = new TypePlanTarifVO();;
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDFC_PLAN_TARIFAIRE"))
					this.id = obj.IDFC_PLAN_TARIFAIRE;
				
				if(obj.hasOwnProperty("DATE_DEBUT_PT"))
					this.dateDebut = obj.DATE_DEBUT_PT as Date;
				
				if(obj.hasOwnProperty("DATE_FIN_PT"))
					this.dateFin = obj.DATE_FIN_PT as Date;
				
				this.objTypePlanTarif.fill(obj);
				
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet PlanTarifaireVO erroné ");
			}
			
		}
		
		
		
		public function PlanTarifaireVO()
		{
		}
		
		public function get objTypePlanTarif():TypePlanTarifVO { return _objTypePlanTarif; }
		
		public function set objTypePlanTarif(value:TypePlanTarifVO):void
		{
			if (_objTypePlanTarif == value)
				return;
			_objTypePlanTarif = value;
		}
		
		public function get id():Number { return _id; }
		
		public function set id(value:Number):void
		{
			if (_id == value)
				return;
			_id = value;
		}
		
		public function get dateFin():Date { return _dateFin; }
		
		public function set dateFin(value:Date):void
		{
			if (_dateFin == value)
				return;
			_dateFin = value;
		}
		
		public function get dateDebut():Date { return _dateDebut; }
		
		public function set dateDebut(value:Date):void
		{
			if (_dateDebut == value)
				return;
			_dateDebut = value;
		}
	}
}