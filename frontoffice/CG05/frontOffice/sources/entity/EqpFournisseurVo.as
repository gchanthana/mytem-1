package entity
{
	public class EqpFournisseurVo
	{
		private var _ID:int;
		private var _LIBELLE:String;
		
		
		
		public function EqpFournisseurVo()
		{
		}
		
		public function fill(obj:Object):void
		{
			if(obj.hasOwnProperty("ID"))
				this.ID = obj.ID;
			if(obj.hasOwnProperty("LIBELLE"))
				this.LIBELLE = obj.LIBELLE;
		}
		
		public function get ID():int { return _ID; }
		public function set ID(value:int):void
		{
			if (_ID == value)
				return;
			_ID = value;
		}
		
		public function get LIBELLE():String { return _LIBELLE; }
		public function set LIBELLE(value:String):void
		{
			if (_LIBELLE == value)
				return;
			_LIBELLE = value;
		}
	}
}