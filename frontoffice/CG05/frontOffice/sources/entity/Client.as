package entity
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class Client
	{
		private var _SIRET:String;
		private var _ADRESSE:String;
		private var _NOM:String;
		private var _ID:int;

		private var _LISTE_PARTENAIRE:ArrayCollection;
		
		
		
		public function Client()
		{
		}
		public function fill(obj:Object,listePartenaire:ArrayCollection=null):Boolean
		{
			try
			{
				if(obj.hasOwnProperty("CLIENT_ADRESSE"))
					this._ADRESSE = obj.CLIENT_ADRESSE;
				if(obj.hasOwnProperty("CLIENT_ID"))
					this._ID = obj.CLIENT_ID;
				if(obj.hasOwnProperty("CLIENT_NAME"))
					this._NOM = obj.CLIENT_NAME;
				if(obj.hasOwnProperty("CLIENT_SIRET"))
					this._SIRET = obj.CLIENT_SIRET;
				
				if(listePartenaire != null)
				{
					this._LISTE_PARTENAIRE = new ArrayCollection();
					for each(var tmpObj:Object in listePartenaire)
					{
						var part:ClientPartenaireVO = new ClientPartenaireVO()
						part.fill(tmpObj,false);
						this._LISTE_PARTENAIRE.addItem(part);
					}
				}
				else
					this._LISTE_PARTENAIRE = new ArrayCollection();
				return true;
			}
			catch(e:Error)
			{
				return false
			}
			return false
		}
		
		public function get LISTE_PARTENAIRE():ArrayCollection { return _LISTE_PARTENAIRE; }
		public function set LISTE_PARTENAIRE(value:ArrayCollection):void
		{
			if (_LISTE_PARTENAIRE == value)
				return;
			_LISTE_PARTENAIRE = value;
		}
		public function get NOM():String { return _NOM; }
		public function set NOM(value:String):void
		{
			if (_NOM == value)
				return;
			_NOM = value;
		}
		public function get ADRESSE():String { return _ADRESSE; }
		public function set ADRESSE(value:String):void
		{
			if (_ADRESSE == value)
				return;
			_ADRESSE = value;
		}
		public function get SIRET():String { return _SIRET; }
		public function set SIRET(value:String):void
		{
			if (_SIRET == value)
				return;
			_SIRET = value;
		}
		public function get ID():int { return _ID; }
		public function set ID(value:int):void
		{
			if (_ID == value)
				return;
			_ID = value;
		}
	}
}