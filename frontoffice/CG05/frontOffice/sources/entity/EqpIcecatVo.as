package entity
{
	import mx.controls.DateField;

	public class EqpIcecatVo
	{
		private var _ID:int;
		private var _LIBELLE:String;
		private var _IMAGE:String; 
		private var _DATE_MODIF:Date;
		private var _DATE_AJOUT:Date;
		private var _SELECTED:Boolean;
		
		
		public function EqpIcecatVo()
		{
		}
		
		public function fill(obj:Object):void
		{
			if(obj.hasOwnProperty("ID"))
				this.ID = obj.ID;
			if(obj.hasOwnProperty("LIBELLE"))
				this.LIBELLE = obj.LIBELLE;
			if(obj.hasOwnProperty("DATE_AJOUT"))
				this.DATE_AJOUT = DateField.stringToDate(obj.DATE_AJOUT,"DD/MM/YYYY") ;
			if(obj.hasOwnProperty("DATE_MODIF"))
				this.DATE_MODIF = DateField.stringToDate(obj.DATE_MODIF,"DD/MM/YYYY") ;
			if(obj.hasOwnProperty("IMAGE"))
				this.IMAGE = obj.IMAGE;
			this.SELECTED = false;
		}
		
		public function get ID():int { return _ID; }
		public function set ID(value:int):void
		{
			if (_ID == value)
				return;
			_ID = value;
		}
		public function get LIBELLE():String { return _LIBELLE; }
		public function set LIBELLE(value:String):void
		{
			if (_LIBELLE == value)
				return;
			_LIBELLE = value;
		}
		public function get DATE_AJOUT():Date { return _DATE_AJOUT; }
		public function set DATE_AJOUT(value:Date):void
		{
			if (_DATE_AJOUT == value)
				return;
			_DATE_AJOUT = value;
		}
		public function get DATE_MODIF():Date { return _DATE_MODIF; }
		public function set DATE_MODIF(value:Date):void
		{
			if (_DATE_MODIF == value)
				return;
			_DATE_MODIF = value;
		}
		public function get IMAGE():String { return _IMAGE; }
		public function set IMAGE(value:String):void
		{
			if (_IMAGE == value)
				return;
			_IMAGE = value;
		}
		public function get SELECTED():Boolean { return _SELECTED; }
		public function set SELECTED(value:Boolean):void
		{
			if (_SELECTED == value)
				return;
			_SELECTED = value;
		}
	}
}