package entity
{
	public class ActualiteVO
	{
		//----------- VARIABLES -----------//
		
		private var _IDACTUALITE:int;
		private var _CODEAPP:int;
		private var _CODELANGUE:String;
		private var _IDLANGUE:int;
		private var _VISIBLE:Boolean;
		private var _POSITION:int;
		private var _SELECTED:Boolean;
		private var _TITRE:String;
		private var _DETAILS:String;
		private var _LIEN:String;
		private var _FICHIERUUID:String;
		private var _FICHIERNOM:String;
		private var _EXTENSION:String;
		private var _DATEPARUTION:String;
		private var _DATECREATION:String;
		private var _APPLICATION:String;
		
		
		//----------- METHODES -----------//
		
		/* */
		public function ActualiteVO()
		{
		}
		
		/* remplir le VO */
		public function fill(obj:Object):Boolean
		{
			try
			{
				if(obj.hasOwnProperty("IDARTICLE"))
					this._IDACTUALITE = obj.IDARTICLE;
				if(obj.hasOwnProperty("ID"))
					this._IDACTUALITE = obj.ID;
				if(obj.hasOwnProperty("CODE_APP"))
					this._CODEAPP = obj.CODE_APP;
				if(obj.hasOwnProperty("ID_LANGUE"))
					this._IDLANGUE = obj.ID_LANGUE;
				if(obj.hasOwnProperty("VISIBLE"))
					this._VISIBLE = Boolean(int(obj.VISIBLE));
				if(obj.hasOwnProperty("POSITION"))
					this._POSITION = obj.POSITION;
				if(obj.hasOwnProperty("LABEL"))
					this._TITRE = obj.LABEL;
				if(obj.hasOwnProperty("DETAILS"))
					this._DETAILS = obj.DETAILS;
				if(obj.hasOwnProperty("LIEN"))
					this._LIEN = obj.LIEN;
				if(obj.hasOwnProperty("FICHIER_UUID"))
					this._FICHIERUUID = obj.FICHIER_UUID;
				if(obj.hasOwnProperty("EXTENSION"))
					this._EXTENSION = obj.EXTENSION;
				if(obj.hasOwnProperty("FICHIER_NOM"))
					this._FICHIERNOM = obj.FICHIER_NOM;
				if(obj.hasOwnProperty("DATE_PARUTION"))
					this._DATEPARUTION = obj.DATE_PARUTION;
				if(obj.hasOwnProperty("DATE_CREATION"))
					this._DATECREATION = obj.DATE_CREATION;
				if(obj.hasOwnProperty("NOM_APPLICATION"))
					this._APPLICATION = obj.NOM_APPLICATION;
				
				this._SELECTED = false;
				
				return true;
			}
			catch(e:Error)
			{
				return false;
			}
			return false;
		}
		
		/* */
		public function telechargerFichier(obj:Object):void
		{
			
		}
		
		
		//----------- GETTERS - SETTERS -----------//
		/* */
		public function get IDACTUALITE():int
		{
			return _IDACTUALITE;
		}
		public function set IDACTUALITE(value:int):void
		{
			_IDACTUALITE = value;
		}
		/* */
		public function get CODEAPP():int
		{
			return _CODEAPP;
		}
		public function set CODEAPP(value:int):void
		{
			_CODEAPP = value;
		}
		/* */
		public function get IDLANGUE():int
		{
			return _IDLANGUE;
		}
		public function set IDLANGUE(value:int):void
		{
			_IDLANGUE = value;
		}
		/* */
		public function get CODELANGUE():String
		{
			switch(this.IDLANGUE)
			{
				case 1: _CODELANGUE = 'GB';
					break;
				case 2: _CODELANGUE = 'NL';
					break;
				
				case 3: _CODELANGUE = 'FR';
					break;
				case 6: _CODELANGUE = 'ES';
					break;
			}
			return _CODELANGUE;
		}
		public function set CODELANGUE(value:String):void
		{
			if (_CODELANGUE == value)
				return;
			_CODELANGUE = value;
		}
		/* */
		public function get VISIBLE():Boolean
		{
			return _VISIBLE;
		}
		public function set VISIBLE(value:Boolean):void
		{
			_VISIBLE = value;
		}
		/* */
		public function get POSITION():int
		{
			return _POSITION;
		}
		public function set POSITION(value:int):void
		{
			_POSITION = value;
		}
		/* */
		[Bindable]
		public function get TITRE():String
		{
			return _TITRE;
		}
		public function set TITRE(value:String):void
		{
			_TITRE = value;
		}
		/* */
		public function get DETAILS():String
		{
			return _DETAILS;
		}
		public function set DETAILS(value:String):void
		{
			_DETAILS = value;
		}
		/* */
		public function get LIEN():String
		{
			return _LIEN;
		}
		public function set LIEN(value:String):void
		{
			_LIEN = value;
		}
		/* */
		public function get FICHIERUUID():String
		{
			return _FICHIERUUID;
		}
		public function set FICHIERUUID(value:String):void
		{
			_FICHIERUUID = value;
		}
		/* */
		public function get FICHIERNOM():String
		{
			return _FICHIERNOM;
		}
		public function set FICHIERNOM(value:String):void
		{
			_FICHIERNOM = value;
		}
		/* */
		public function get APPLICATION():String
		{
			return _APPLICATION;
		}
		public function set APPLICATION(value:String):void
		{
			_APPLICATION = value;
		}
		/* */
		public function get DATEPARUTION():String
		{
			return _DATEPARUTION;
		}
		public function set DATEPARUTION(value:String):void
		{
			_DATEPARUTION = value;
		}
		/* */
		public function get DATECREATION():String
		{
			return _DATECREATION;
		}
		public function set DATECREATION(value:String):void
		{
			_DATECREATION = value;
		}
		/* */
		[Bindable]
		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}
		public function set SELECTED(value:Boolean):void
		{
			_SELECTED = value;
		}

		public function get EXTENSION():String
		{
			return _EXTENSION;
		}
		public function set EXTENSION(value:String):void
		{
			_EXTENSION = value;
		}
	}
}