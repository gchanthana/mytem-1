package service
{
	import entity.CodeAction;
	import entity.FileUpload;
	
	import flash.net.registerClassAlias;
	
	import service.FctGestionClientDatas;
	import service.FctGestionClientHandlers;
	
	import utils.abstract.AbstractRemoteService;

	public class FctGestionClientService extends AbstractRemoteService
	{
		private var key	:String = CvAccessManager.CURRENT_FUNCTION; 
		
		[Bindable] 
		public var myDatas		:FctGestionClientDatas;
		public var myHandlers	:FctGestionClientHandlers;
		
		
		public function FctGestionClientService()
		{
			this.myDatas 		= new FctGestionClientDatas();
			this.myHandlers 	= new FctGestionClientHandlers(myDatas);
		}
		
		/*
		 * recherche de la liste des clients (par 30 lignes) et selon les critères de recherche et filtrage indiqués. 
		*/
		public function getListeClients(textSearch:String, filterText:String, orderText:String, indexDebut:Number, nbrParPage:Number):void
		{
			var data:Object = new Object();
				data.p_text 			= textSearch;
				data.p_search_column 	= filterText;
				data.p_order_column 	= orderText;
				data.p_idx_debut 		= indexDebut;
				data.p_nb 				= nbrParPage;
			
			doRemoting(myHandlers.getListeClientsHandler,key, CodeAction.GCLI_LISTE_CLIENTS, data);
		}
		
		public function createClient(idRacineMaster:Number, idRacine:Number, libelleRacine:String, commentaire:String ,  fileToUpload:FileUpload = null):void
		{
			var data:Object = new Object();
			data.p_idracine_master 	= idRacineMaster;
			data.p_idracine 		= idRacine; // pour la creation egale 0 tjs.
			data.P_LIBELLE_racine 	= libelleRacine;
			data.P_COMMENTAIRE 		= commentaire;
			data.fileToUpload		= fileToUpload;
			registerClassAlias("fr.consotel.consoview.M28.vo.FileUploadVo", entity.FileUpload);

			doRemoting(myHandlers.createClientHandler,key, CodeAction.GCLI_CREATE_CLIENT, data);
		}
		
		public function editClient(idRacineMaster:Number, idRacine:Number, libelleRacine:String, commentaire:String , fileToUpload:FileUpload):void
		{
			var data:Object = new Object();
			data.p_idracine_master 	= idRacineMaster;
			data.p_idracine 		= idRacine;
			data.P_LIBELLE_racine 	= libelleRacine;
			data.P_COMMENTAIRE 		= commentaire;
			data.fileToUpload		= fileToUpload;
			registerClassAlias("fr.consotel.consoview.M28.vo.FileUploadVo", entity.FileUpload);
			
			doRemoting(myHandlers.createClientHandler,key, CodeAction.GCLI_CREATE_CLIENT, data);
		}
		
		public function getListeMasters():void
		{
			doRemoting(myHandlers.getListeMastersHandler,key, CodeAction.GCLI_LISTE_MASTERS);
			
		}
		
		
	}
}