package service
{
	import composants.util.ConsoviewAlert;
	
	import entity.ClientVO;
	import entity.MasterVO;
	
	import event.FctGestionClientEvent;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	
	public class FctGestionClientDatas extends EventDispatcher
	{
		private var _listeClients:ArrayCollection = new ArrayCollection();
		private var _listeMasters:ArrayCollection = new ArrayCollection();

		public function treatListeClients(retour:Object):void
		{
			listeClients.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int = 0; i < tailleRet; i++)
			{
				var client:ClientVO = new ClientVO();
				client.fill(retour.RESULT[i]);
				this.listeClients.addItem(client);
			}
			this.dispatchEvent(new FctGestionClientEvent(FctGestionClientEvent.LISTE_CLIENTS));
		}
		
		public function treatCreateClient(retour:Object):void
		{
			this.dispatchEvent(new FctGestionClientEvent(FctGestionClientEvent.EDITION_CLIENT));
		}
		
		public function treatListeMasters(retour:Object):void
		{
			listeMasters.source = new Array();
			
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int = 0; i < tailleRet; i++)
			{
				var master:MasterVO = new MasterVO();
				master.fill(retour.RESULT[i]);
				this.listeMasters.addItem(master);
			}
			this.dispatchEvent(new FctGestionClientEvent(FctGestionClientEvent.LISTE_MASTRERS));
		}
				
		[Bindable]
		public function get listeClients():ArrayCollection { return _listeClients; }
		
		public function set listeClients(value:ArrayCollection):void
		{
			if (_listeClients == value)
				return;
			_listeClients = value;
		}
		
		public function get listeMasters():ArrayCollection { return _listeMasters; }
		
		public function set listeMasters(value:ArrayCollection):void
		{
			if (_listeMasters == value)
				return;
			_listeMasters = value;
		}
	}
}