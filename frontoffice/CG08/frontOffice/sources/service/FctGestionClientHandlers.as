package service
{
	import composants.util.ConsoviewAlert;
	
	import mx.messaging.events.MessageAckEvent;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	
	import utils.MessageAlerte;
	
	public class FctGestionClientHandlers
	{
		private var myDatas:FctGestionClientDatas;
		
		public function FctGestionClientHandlers(iDatas:FctGestionClientDatas)
		{
			this.myDatas = iDatas;
		}
		
		public function getListeClientsHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatListeClients(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
		}
		
		public function createClientHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatCreateClient(re.result.DATA);
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('CG08', 'Votre_demande_a_bien__t__prise_en_compte'));
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
		}
		
		public function getListeMastersHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.STATUS == 1)
			{
				myDatas.treatListeMasters(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
		}
	}
}