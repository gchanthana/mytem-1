package service.thread
{
	import entity.ApplicationVO;
	import entity.NiveauVO;
	import entity.UniversVO;
	
	import event.thread.ThreadEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import utils.Constantes;
	
	public class ThreadDatas extends EventDispatcher
	{
		public function ThreadDatas()
		{
		}
		public function treatProcessThread(objResult:Object):void
		{
			Constantes.listeApplication.removeAll();
			Constantes.listeNiveau.removeAll();
			Constantes.listeAllNiveau.removeAll();
			Constantes.listeUnivers.removeAll();
			
			for(var i:int = 0; i < objResult.DATA.LISTEAPPLICATION.length;i++)
			{
				var obj:ApplicationVO = new ApplicationVO();
				obj.fill(objResult.DATA.LISTEAPPLICATION[i]);
				Constantes.listeApplication.addItem(obj);
			}
			var app:ApplicationVO = new ApplicationVO();
			app.CODEAPP = 0;
			app.LABEL = ResourceManager.getInstance().getString("M28","Toutes_les_applications");
			Constantes.listeApplication.addItemAt(app,0);
			
			var niv:NiveauVO = new NiveauVO();
			niv.Id = "-1";
			niv.libelle = ResourceManager.getInstance().getString("M28","Tous_les_niveaux");
			
			for(var j:int = 0; j < objResult.DATA.LISTENIVEAU.length;j++)
			{
				var obj1:NiveauVO = new NiveauVO();
				obj1.fill(objResult.DATA.LISTENIVEAU[j]);
				Constantes.listeNiveau.addItem(obj1);
			}
			Constantes.listeNiveau.addItemAt(niv,0)
			for(var k:int = 0; k < objResult.DATA.LISTEALLNIVEAU.length;k++)
			{
				var obj2:NiveauVO = new NiveauVO();
				obj2.fill(objResult.DATA.LISTEALLNIVEAU[k]);
				Constantes.listeAllNiveau.addItem(obj2);
			}
			Constantes.listeAllNiveau.addItemAt(niv,0)
			
			CvAccessManager.singletonInstance.updateGroupList(objResult.DATA.LISTE_RACINE);	

			var xmlUnivers:XMLList = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData.descendants("UNIVERS");
			for each(var x:XML in xmlUnivers)
			{
				var univ:UniversVO = new UniversVO();
				univ.idUnivers = x.@ID;
				univ.nomUnivers = x.@LBL;
				univ.keyUnivers = x.@KEY
				Constantes.listeUnivers.addItem(univ);
			}
			
			this.dispatchEvent(new ThreadEvent(ThreadEvent.THREAD_FINISH_EVENT));
		}
	}
}