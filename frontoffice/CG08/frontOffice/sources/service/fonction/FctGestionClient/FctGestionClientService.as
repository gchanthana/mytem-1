package service.fonction.FctGestionClient
{
	import entity.CodeAction;
	import entity.FileUpload;
	
	import utils.abstract.AbstractRemoteService;

	public class FctGestionClientService extends AbstractRemoteService
	{
		private var key	:String = CvAccessManager.CURRENT_FUNCTION; 
		
		[Bindable] 
		public var myDatas		:FctGestionClientDatas;
		public var myHandlers	:FctGestionClientHandlers;
		
		
		public function FctGestionClientService()
		{
			this.myDatas 		= new FctGestionClientDatas();
			this.myHandlers 	= new FctGestionClientHandlers(myDatas);
		}
		
	}
}