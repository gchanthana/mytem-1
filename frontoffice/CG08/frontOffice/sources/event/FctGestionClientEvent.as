package event
{
	import flash.events.Event;
	
	public class FctGestionClientEvent extends Event
	{
		// les types Event liés au service remote
		public static const LISTE_CLIENTS				:String = 'LISTE_CLIENTS';
		public static const EDITION_CLIENT				:String = 'EDITION_CLIENT';
		public static const LISTE_MASTRERS				:String = 'LISTE_MASTERS';
		
		public static const CLICK_EDITER_CLIENT				:String = 'CLICK_EDITER_CLIENT';
		public static const CLICK_VERROUILLER_CLIENT		:String = 'CLICK_VERROUILLER_CLIENT';
		public static const CLICK_DESACTIVER_CLIENT			:String = 'CLICK_DESACTIVER_CLIENT';
		
		
		public function FctGestionClientEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}