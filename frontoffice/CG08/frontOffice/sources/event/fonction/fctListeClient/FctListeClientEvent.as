package event.fonction.fctListeClient
{
	import flash.events.Event;
	
	public class FctListeClientEvent extends Event
	{
		public static var proc_LISTE_CLIENT_CONTRACTUEL_EVENT:String = "proc_LISTE_CLIENT_CONTRACTUEL_EVENT";
		public static var proc_GETDATA_EVENT:String = "proc_GETDATA_EVENT";
		public static var proc_GETDATA_FROMAPPLICATION_EVENT:String = "proc_GETDATA_FROMAPPLICATION_EVENT";
		
		public function FctListeClientEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}