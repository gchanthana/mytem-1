package event.fonction.fctGestionCollecte
{
    import flash.events.Event;

    public class FctGestionCollecteEvent extends Event
    {
        //--------------------------------------------------------------------------------------------//
        //					VARIABLES STATIC - EVENT NAME
        //--------------------------------------------------------------------------------------------//
        public static const TEMPLATE_CREATED_UPDATED_SUCCESS:String = "template_created_updated_success";
        public static const TEMPLATE_CREATED_UPDATED_ERROR:String = "template_created_updated_error";
        public static const TEMPLATE_CREATED_UPDATED_CANCEL:String = "template_created_updated_cancel";
        public static const TEMPLATE_LIST_LOADED_SUCCESS:String = "template_list_loaded_success";
        public static const TEMPLATE_LIST_LOADED_ERROR:String = "template_list_loaded_error";
        public static const TEMPLATE_UID_CREATED_SUCCESS:String = "template_uid_created_success";
        public static const TEMPLATE_UID_CREATED_ERROR:String = "template_uid_created_error";
        public static const TEMPLATE_UID_DESTROY_SUCCESS:String = "template_uid_destroy_success";
        public static const TEMPLATE_UID_DESTROY_ERROR:String = "template_uid_destroy_error";
        public static const OPERATEUR_LIST_LOADED_SUCCESS:String = "operateur_list_loaded_success";
        public static const OPERATEUR_LIST_LOADED_ERROR:String = "operateur_list_loaded_error";
        public static const OPERATEUR_WITH_TEMPLATE_LIST_LOADED_SUCCESS:String = "operateur_with_template_list_loaded_success";
        public static const OPERATEUR_WITH_TEMPLATE_LIST_LOADED_ERROR:String = "operateur_with_template_list_loaded_error";
        public static const DATATYPE_LIST_LOADED_SUCCESS:String = "datatype_list_loaded_success";
        public static const DATATYPE_LIST_LOADED_ERROR:String = "datatype_list_loaded_error";
        public static const RECUPMODE_LIST_LOADED_SUCCESS:String = "recupmode_list_loaded_success";
        public static const RECUPMODE_LIST_LOADED_ERROR:String = "recupmode_list_loaded_error";
        public static const TEST_IDENTIFICATION_SUCCESS:String = "test_identification_success";
        public static const TEST_IDENTIFICATION_ERROR:String = "test_identification_error";
        public static const COLLECTE_CREATED_UPDATED_SUCCESS:String = "collecte_created_updated_success";
        public static const COLLECTE_CREATED_UPDATED_ERROR:String = "collecte_created_updated_error";
        public static const COLLECTE_CREATED_UPDATED_CANCEL:String = "collecte_created_updated_cancel";
        public static const COLLECTE_LIST_LOADED_SUCCESS:String = "collecte_list_loaded_success";
        public static const COLLECTE_LIST_LOADED_ERROR:String = "collecte_list_loaded_error";
        public static const COLLECTE_UID_CREATED_SUCCESS:String = "collecte_uid_created_success";
        public static const COLLECTE_UID_CREATED_ERROR:String = "collecte_uid_created_error";
        // CLIENT : Gestion collecte
        public static const TEMPLATE_FOR_CLIENT_LIST_LOADED_SUCCESS:String = "template_for_client_list_loaded_success";
        public static const TEMPLATE_FOR_CLIENT_LIST_LOADED_ERROR:String = "template_for_client_list_loaded_error";
        public static const RACINE_LIST_LOADED_SUCCESS:String = "racine_list_loaded_success";
        public static const RACINE_LIST_LOADED_ERROR:String = "racine_list_loaded_error";
        //--------------------------------------------------------------------------------------------//
        //					VARIABLES PASSEES EN ARGUMENTS
        //--------------------------------------------------------------------------------------------//		
        public var obj:Object = null;

        //--------------------------------------------------------------------------------------------//
        //					CONSTRUCTEUR
        //--------------------------------------------------------------------------------------------//		
        public function FctGestionCollecteEvent(type:String, objet:Object = null, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
            this.obj = objet;
        }

        //--------------------------------------------------------------------------------------------//
        //					METHODES PUBLIC OVERRIDE - COPY OF EVENT
        //--------------------------------------------------------------------------------------------//
        override public function clone():Event
        {
            return new FctGestionCollecteEvent(type, obj, bubbles, cancelable);
        }
    }
}