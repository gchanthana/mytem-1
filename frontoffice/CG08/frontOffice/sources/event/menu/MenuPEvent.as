package event.menu
{
	import flash.events.Event;
	
	public class MenuPEvent extends Event
	{
		public static var MENU_FINISH_EVENT:String = "MENU_FINISH_EVENT";
		
		public function MenuPEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}