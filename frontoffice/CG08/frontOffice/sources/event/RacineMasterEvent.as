package event
{
	import entity.MasterVO;
	
	import flash.events.Event;
	
	public class RacineMasterEvent extends Event
	{
		public static const SELECT_MASTER	:String = 'SELECT_MASTER';
		public static const CLICK_MASTER	:String = 'CLICK_MASTER';
		private var _objMaster				:MasterVO;
		
		public function RacineMasterEvent(type:String, master:MasterVO, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this._objMaster = master;
		}
		
		public function get objMaster():MasterVO { return _objMaster; }
		
		/*public function set objMaster(value:MasterVO):void
		{
			if (_objMaster == value)
				return;
			_objMaster = value;
		}*/
	}
}