package entity
{
	[Bindable]
	public class ParticipantFormationVO
	{
		private var _id				:int;
		private var _nom			:String;
		private var _prenom			:String;
		private var _email			:String;
		private var _participated	:int; //  0 ou 1
		private var _nomComplet		:String;
		private var _selected		:Boolean = false;

		public function ParticipantFormationVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("APP_LOGINID"))
					this.id = obj.APP_LOGINID;
				
				if(obj.hasOwnProperty("LOGIN_NOM"))
					this.nom = obj.LOGIN_NOM;
				
				if(obj.hasOwnProperty("LOGIN_PRENOM"))
					this.prenom = obj.LOGIN_PRENOM;
				
				if(obj.hasOwnProperty("LOGIN_EMAIL"))
					this.email = obj.LOGIN_EMAIL;
				
				if(obj.hasOwnProperty("BOOL_PARTICIPANT"))
					this.participated = obj.BOOL_PARTICIPANT;
				
				if((obj.hasOwnProperty("LOGIN_NOM")) && (obj.hasOwnProperty("LOGIN_PRENOM")))
				{
					this.nomComplet = obj.LOGIN_NOM + " " + obj.LOGIN_PRENOM;
				}
				
				
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet ParticipantFormationVO erroné ");
			}
		}
		
		public function get SELECTED():Boolean { return _selected; }
		
		public function set SELECTED(value:Boolean):void
		{
			if (_selected == value)
				return;
			_selected = value;
		}
		
		public function get participated():int { return _participated; }
		
		public function set participated(value:int):void
		{
			if (_participated == value)
				return;
			_participated = value;
		}
		
		public function get email():String { return _email; }
		
		public function set email(value:String):void
		{
			if (_email == value)
				return;
			_email = value;
		}
		
		public function get prenom():String { return _prenom; }
		
		public function set prenom(value:String):void
		{
			if (_prenom == value)
				return;
			_prenom = value;
		}
		
		public function get nom():String { return _nom; }
		
		public function set nom(value:String):void
		{
			if (_nom == value)
				return;
			_nom = value;
		}
		
		
		public function get id():int { return _id; }
		
		public function set id(value:int):void
		{
			if (_id == value)
				return;
			_id = value;
		}
		
		public function get nomComplet():String { return _nomComplet; }
		
		public function set nomComplet(value:String):void
		{
			if (_nomComplet == value)
				return;
			_nomComplet = value;
		}
		
	}
}