package entity.GestionCollecte
{
    import entity.CodeAction;
    import entity.GestionCollecte.enum.EBoolean;
    import entity.GestionCollecte.enum.EPeriodicity;
    import entity.GestionCollecte.vo.CollecteTypeVO;
    import entity.GestionCollecte.vo.CompteFacturationVO;
    import entity.GestionCollecte.vo.FilterCollecteTypeVO;
    import entity.GestionCollecte.vo.OperateurVO;
    import entity.GestionCollecte.vo.SimpleEntityVO;
    
    import event.fonction.fctGestionCollecte.FctGestionCollecteEvent;
    
    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.filters.DisplacementMapFilter;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;
    import flash.net.navigateToURL;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.controls.dataGridClasses.DataGridColumn;
    import mx.core.UIComponent;
    import mx.formatters.SwitchSymbolFormatter;
    import mx.managers.PopUpManager;
    import mx.messaging.errors.NoChannelAvailableError;
    import mx.resources.ResourceManager;
    import mx.utils.object_proxy;
    import mx.validators.EmailValidator;
    
    import service.fonction.FctGestionCollecte.FctGestionCollecteService;
    
    import utils.GestionCollecte.ConversionUtils;

    [Bindable]
    /**
     * Cette classe <b>Singleton</b> centralise les données de l'applicatif qui sont
     * accessibles en tout point ( liste de templates , liste d'opérateurs ...)
     * Elle regroupe également les procédures d'alimentation de ces données.
     *  Elle instancie un objet <code>CollecteTypeService</code> qui assure
     *  les requêtes à la base.
     *  Ex d'utilisation <code> ModelLocator.getInstance().feedOperateurList()</code>
     * <code> ModelLocator.getInstance().operateurList()</code>
     *
     */
    public class ModelLocator extends EventDispatcher
    {
        private static var _instance:ModelLocator;
        public static const ENTITY_OPERATEUR:String = "entity_operateur";
        public static const ENTITY_RECUPMODE:String = "entity_recupmode";
        public static const ENTITY_DATATYPE:String = "entity_datatype";
        // liste utile
        public const URL_BACKOFFICE:String = RevisionM28.urlBackoffice;
        // GLOBALE
        private var _userId:Number; //   <operateurVO>
        private var _userRacineId:Number;
        private var _operateurList:ArrayCollection = new ArrayCollection(); //   <operateurVO>
        private var _operateurWithTemplateList:ArrayCollection = new ArrayCollection(); //   <operateurVO>
        private var _datatypeList:ArrayCollection = new ArrayCollection(); //   <SimpleEntityVO>
        private var _recupModeList:ArrayCollection = new ArrayCollection(); //   <SimpleEntityVO>
        private var _operateurListAll:ArrayCollection = new ArrayCollection(); //   <operateurVO>
        private var _datatypeListAll:ArrayCollection = new ArrayCollection(); //   <SimpleEntityVO>
        private var _recupModeListAll:ArrayCollection = new ArrayCollection(); //   <SimpleEntityVO>
        private var _coutsList:ArrayCollection = new ArrayCollection(); //   <SimpleEntityVO>
        // Admin TEMPLATE
        private var _collecteTypeList:ArrayCollection = new ArrayCollection();
        private var _currentCollecteTypeVO:CollecteTypeVO;
        // variable mémorisant les actions utilisateur quant à l'ouverture des popup
        public var askForPublishPopup:Boolean = true;
        public var askForConfirmCancelPopup:Boolean = true;
        // variable mémorisant les actions utilisateur quant à l'ouverture des popup       
        public var askForConfirmCancelCollecteEditPopup:Boolean = true;
        // Client COLLECTE
        private var _racineList:ArrayCollection = new ArrayCollection();
        private var _collecteClientList:ArrayCollection = new ArrayCollection();
        private var _publishedTemplateList:ArrayCollection = new ArrayCollection();
        // services
        private var _collecteTypeService:FctGestionCollecteService;

        public function ModelLocator()
        {
            // Instanciation du service
            collecteTypeService = new FctGestionCollecteService();
            // Recuperation de l'utilisateur logge
            userId = new Number(CvAccessManager.getSession().USER.CLIENTACCESSID);
            if(isNaN(userId))
            {
                userId = 0;
            }
        }

        public static function getInstance():ModelLocator
        {
            if(_instance == null)
            {
                _instance = new ModelLocator();
            }
            return _instance;
        }

        /**
         * Initialisation des données globales de l'application.
         * <b>Lance les appels distants pour initialiser les collections</b>
         * suivantes :
         *  - liste des opérateurs
         *  - liste des type de données
         *  - liste des modes de récupération
         *  - liste des couts
         *  - liste des templates
         *  - liste des templates publiés et dipo pour le client
         *  - liste des collectes du client
         *
         */
        public function initGlobalDataApplication():void
        {
            feedOperateurList();
            feedOperateurWithTemplateList();
            feedDataTypeList();
            feedRecupModeList();
            feedCouts();
            feedTemplateListInitial();
            // Partie cliente
            //feedPublishedTemplate();
            //feedClientCollecte(641);
            feedRacineList();
        }

        /**
         * Récupère la <b>liste de tous les opérateurs</b> depuis la base
         * via l'objet service <code>collecteTypeService.getOperateurList()</code>		 *
         */
        public function feedOperateurList():void
        {
            collecteTypeService.getOperateurList("GCOL", CodeAction.LISTE_OPERATEURS);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_SUCCESS, onListLoadedHandler);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_ERROR, onListLoadedHandler);
        }

        /**
         * Récupère la <b>liste de tous les opérateurs</b> possédant au moins un template depuis la base
         * via l'objet service <code>collecteTypeService.getOperateurWithTemplateList()</code>
         */
        public function feedOperateurWithTemplateList():void
        {
            collecteTypeService.getOperateurWithTemplateList("GCOL", CodeAction.LISTE_OPERATEURS_TEMPLATE);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_SUCCESS, onListLoadedHandler);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_ERROR, onListLoadedHandler);
        }

        /**
         * Récupère <b>la liste de tous les types de données </b> depuis la base
         * via l'objet service <code>collecteTypeService.getDataTypeList()</code>
         */
        public function feedDataTypeList():void
        {
            collecteTypeService.getDataTypeList("GCOL", CodeAction.LISTE_DATATYPE);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.DATATYPE_LIST_LOADED_SUCCESS, onListLoadedHandler);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.DATATYPE_LIST_LOADED_ERROR, onListLoadedHandler);
        }

        /**
         * Récupère <b>la liste de tous les modes de récupération</b> depuis la base
         * via l'objet service <code>collecteTypeService.getRecupModeList()</code>		 *
         */
        public function feedRecupModeList():void
        {
            collecteTypeService.getRecupModeList("GCOL", CodeAction.LISTE_RECUPMODE);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_SUCCESS, onListLoadedHandler);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_ERROR, onListLoadedHandler);
        }

        /**
         * Récupère <b>la liste de tous les racine</b> depuis la base
         * via l'objet service <code>collecteTypeService.getRacineList()</code>		 *
         */
        public function feedRacineList():void
        {
            collecteTypeService.getRacineList("GCOL", CodeAction.LISTE_RACINE);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_SUCCESS, onListLoadedHandler);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_ERROR, onListLoadedHandler);
        }

        /**
         * Configure la collection cout ( Payant ou Gratuit )
         */
        public function feedCouts():void
        {
            // liste locale
            var item_gratuit:SimpleEntityVO = new SimpleEntityVO();
            var item_payant:SimpleEntityVO = new SimpleEntityVO();
            item_gratuit.label = ResourceManager.getInstance().getString('CG08','Gratuit');
            item_gratuit.value = 0;
            item_payant.label = ResourceManager.getInstance().getString('CG08','Payant');
            item_payant.value = 1;
            coutsList = new ArrayCollection([ item_gratuit, item_payant ]);
        }

        /**
         * Récupère <b>la liste de tous les templates </b> de collecte depuis la base
         * via l'objet service <code>collecteTypeService.getCollecteTypeList(filter)</code>
         * avec un vo <code> FilterCollecteTypeVO</code> initialisé
         */
        public function feedTemplateListInitial():void
        {
            // à l'initialisation on récupère tous les templates (ei  ts les param du filtre sont à null)
            var filter:FilterCollecteTypeVO = new FilterCollecteTypeVO();
            filter.selectedDataTypeId = 0;
            filter.selectedOperateurId = 0;
            filter.selectedRecuperationModeId = 0;
            collecteTypeService.getCollecteTypeList("GCOL", CodeAction.LISTE_COLLECTETYPE, filter);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_SUCCESS, onListLoadedHandler);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_ERROR, onListLoadedHandler);
        }

        /**
         * Pour la partie cliente de l'application ,
         * Récupère <b>la liste de tous les templates de collecte publiés </b> depuis la base
         * via l'objet service <code>collecteTypeService.getPublishedTemplate(filter)</code>
         * avec un vo <code> FilterCollecteTypeVO</code> qui determine les templates
         * dont l'utilisateur peut disposer pour créer sa collecte ( depend de l'operateur,
         * du mode de récup det du type de choisis )
         */
        public function feedPublishedTemplate(filter:FilterCollecteTypeVO = null):void
        {
            var filterVO:FilterCollecteTypeVO = new FilterCollecteTypeVO();
            if(!filter)
            {
                filterVO.selectedDataTypeId = 0;
                filterVO.selectedOperateurId = 0;
                filterVO.selectedRecuperationModeId = 0;
            }
            else
            {
                filterVO = filter;
            }
            collecteTypeService.getPublishedTemplate("GCOL", CodeAction.LISTE_PUBLISHEDTEMPLATE, filterVO);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_SUCCESS, onListLoadedHandler);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_ERROR, onListLoadedHandler);
        }

        /**
         * Retourne <b>la liste des collectes pour un utilisateur</b>
         * donné
         */
        public function feedClientCollecte(racineId:Number):void
        {
            collecteTypeService.getCollecteClientList("GCOL", CodeAction.LISTE_COLLECTECLIENT, racineId);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS, onListLoadedHandler);
            collecteTypeService.myDatas.addEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_ERROR, onListLoadedHandler);
        }

        /**
         * Exporte au format spécifié la liste des templates
         * en fonction de l'operateur , le type de donnée et
         * le mode de récupération
         *
         */
        public function exportListeCollecteType(filter:FilterCollecteTypeVO, format:String = "csv"):void
        {
            var url:String = URL_BACKOFFICE + "/fr/consotel/consoview/M28/export/Export_ListeCollectesType.cfm";
            var request:URLRequest = new URLRequest(url);
            var variables:URLVariables = new URLVariables();
            variables.FORMAT = format;
            variables.OPERATEURID = filter.selectedOperateurId;
            variables.DATATYPEID = filter.selectedDataTypeId;
            variables.RECUPMODEID = filter.selectedRecuperationModeId;
            request.data = variables;
            request.method = URLRequestMethod.POST;
            navigateToURL(request, "_blank");
        }

        /**
         * Exporte au format spécifié de la liste des collectes clientes
         * en fonction de l'id racine passé en param
         *
         */
        public function exportListeCollecteClient(idRacine:Number, format:String = "csv"):void
        {
            var url:String = URL_BACKOFFICE + "/fr/consotel/consoview/M28/export/Export_ListeCollectesClient.cfm";
            var request:URLRequest = new URLRequest(url);
            var variables:URLVariables = new URLVariables();
            variables.FORMAT = format;
            variables.RACINEID = idRacine;
            request.data = variables;
            request.method = URLRequestMethod.POST;
            navigateToURL(request, "_blank");
        }

        /**
         * Crée un identifiant unique  UID
         *
         */
        public function createUIDStorage():void
        {
            collecteTypeService.createUIDStorage("GCOL", CodeAction.CREATE_UIDSTORAGE);
            collecteTypeService.myHandlers.addEventListener(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_SUCCESS, onListLoadedHandler);
            collecteTypeService.myHandlers.addEventListener(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_ERROR, onListLoadedHandler);
        }

        /**
         * Crée le dossier UID associé à un template
         * dont la création vient d'être annulée
         *
         */
        public function destroyLastUID(uid:String):void
        {
            collecteTypeService.destroyLastUID("GCOL", CodeAction.DESTROY_LASTUID, uid);
            collecteTypeService.myHandlers.addEventListener(FctGestionCollecteEvent.TEMPLATE_UID_DESTROY_SUCCESS, onListLoadedHandler);
            collecteTypeService.myHandlers.addEventListener(FctGestionCollecteEvent.TEMPLATE_UID_DESTROY_ERROR, onListLoadedHandler);
        }

        /**
         * Assure la reception des retours serveurs et dispatch les actions
         * à effectuer en fonction du type de message ( affectation des collections
         * du model)
         */
        public function onListLoadedHandler(event:FctGestionCollecteEvent):void
        {
            switch(FctGestionCollecteEvent(event).type)
            {
                case FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_SUCCESS:
                    operateurList = collecteTypeService.myDatas.operateurList;
                    operateurListAll = getCollecOperateurWithAllItem(operateurList, ResourceManager.getInstance().getString('CG08','Tous'), 0);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_ERROR:
                    Alert.show(FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_ERROR);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_SUCCESS:
                    operateurWithTemplateList = collecteTypeService.myDatas.operateurWithTemplateList;
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_ERROR:
                    Alert.show(FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_ERROR);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.DATATYPE_LIST_LOADED_SUCCESS:
                    datatypeList = collecteTypeService.myDatas.dataTypeList;
                    datatypeListAll = getCollecWithAllItem(datatypeList, ResourceManager.getInstance().getString('CG08','Tout_type'), 0);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.DATATYPE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.DATATYPE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.DATATYPE_LIST_LOADED_ERROR:
                    Alert.show(FctGestionCollecteEvent.DATATYPE_LIST_LOADED_ERROR);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.DATATYPE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.DATATYPE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_SUCCESS:
                    recupModeList = collecteTypeService.myDatas.recupModeList;
                    recupModeListAll = getCollecWithAllItem(recupModeList, ResourceManager.getInstance().getString('CG08','Tous'), 0);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_ERROR:
                    Alert.show(FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_ERROR);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.RACINE_LIST_LOADED_SUCCESS:
                    racineList = collecteTypeService.myDatas.racineList;
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.RACINE_LIST_LOADED_SUCCESS:
                    //dataLocator.consaalerte(this, ResourceManager.getInstance().getString('CG08','Erreur_dans_la_r_cup_ration_de_la_liste_'), "Erreur", StaticData.imgError);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.RACINE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_SUCCESS:
                    collecteTypeList = collecteTypeService.myDatas.collecteTypeVoList;
                    publishedTemplateList = filterPublishedTemplate(collecteTypeList);
                    //feedPublishedTemplate();
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR:
                    // Alert.show(FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_ERROR);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.TEMPLATE_UID_CREATED_SUCCESS:
                    collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.TEMPLATE_UID_CREATED_ERROR:
                    // Alert.show(FctGestionCollecteEvent.COLLECTE_UID_DESTROY_ERROR);
                    collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.TEMPLATE_UID_DESTROY_SUCCESS:
                    collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_UID_DESTROY_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_UID_DESTROY_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.TEMPLATE_UID_DESTROY_ERROR:
                    // Alert.show(FctGestionCollecteEvent.COLLECTE_UID_DESTROY_ERROR);
                    collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_UID_DESTROY_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_UID_DESTROY_ERROR, onListLoadedHandler);
                    break;
                // CLIENT 
                case FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_SUCCESS:
                    publishedTemplateList = collecteTypeService.myDatas.publishedTemplateList;
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_ERROR:
                    //Alert.show(FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_ERROR);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS:
                    collecteClientList = collecteTypeService.myDatas.collecteClientList;
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                case FctGestionCollecteEvent.COLLECTE_LIST_LOADED_ERROR:
                    //Alert.show(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_ERROR);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_SUCCESS, onListLoadedHandler);
                    collecteTypeService.myDatas.removeEventListener(FctGestionCollecteEvent.COLLECTE_LIST_LOADED_ERROR, onListLoadedHandler);
                    break;
                default:
                    break;
            }
            dispatchEvent(event);
        }

        /******************************************************************************
         *         Utilitaires de récupération/formatage des données
         ****************************************************************************/ /**
         *  Retourne le template <code>CollecteTypeVO</code>
         *  associé à l'id passé en paramètre (recherche locale)
         */
        public function getTemplateFor(idcollect_template:Number):CollecteTypeVO
        {
            var template:CollecteTypeVO = new CollecteTypeVO();
            for(var i:int; i < collecteTypeList.length; i++)
            {
                var vo:CollecteTypeVO = collecteTypeList.getItemAt(i) as CollecteTypeVO
                if(vo.idcollecteTemplate == idcollect_template)
                {
                    template = vo;
                    break;
                }
            }
            return template;
        }

        /**
         *  Retourne le template <code>CollecteTypeVO</code>
         *  associé à l'id passé en paramètre (recherche locale)
         */
        public function getTemplate(idTemplate:Number):CollecteTypeVO
        {
            var template:CollecteTypeVO = new CollecteTypeVO();
            for(var i:int = 0; i < publishedTemplateList.length; i++)
            {
                var vo:CollecteTypeVO = publishedTemplateList.getItemAt(i) as CollecteTypeVO
                if(vo.idcollecteTemplate == idTemplate)
                {
                    template = vo;
                    break;
                }
            }
            return template;
        }

        public function filterPublishedTemplate(collec:ArrayCollection):ArrayCollection
        {
            var publishedTemplates:ArrayCollection = new ArrayCollection();
            var template:CollecteTypeVO = new CollecteTypeVO();
            for(var i:int = 0; i < collec.length; i++)
            {
                template = collec.getItemAt(i) as CollecteTypeVO;
                if(template.isPublished.value == 1)
                {
                    publishedTemplates.addItem(template);
                }
            }
            return publishedTemplates;
        }

        /**
         *  Formate le libélle des colonnes du datagrid des templates
         */
        public function getEntityLabelField(item:Object, col:DataGridColumn):String
        {
            var str:String = "";
            var obj:Object = item[col.dataField];
            switch(col.dataField)
            {
                case "idcollectMode":
                    str = getRecupCollecteLabel(obj as Number);
                    break;
                case "idcollectType":
                    str = getTypeCollecteLabel(obj as Number);
                    break;
                case "couts":
                    str = getCoutLabel(obj.value as Number);
                    break;
                default:
                    break;
            }
            return str;
        }

        /**
         *  Retourne le <b>libélle du type de données</b> correspondant
         *  à l'id passé en paramètre (recherche locale dans la liste chargée
         *  à l'initialisation)
         */
        public function getTypeCollecteLabel(value:Number):String
        {
            return getEntityLabel(ENTITY_DATATYPE, value);
        }

        /**
         *  Retourne le <b>libélle du mode de souscription</b> correspondant
         *  à l'id passé en paramètre
         */
        public function getSouscriptionLabel(value:Number):String
        {
            var lb:String = "";
            switch(value)
            {
                case 0:
                    lb = ResourceManager.getInstance().getString('CG08','Automatique');
                    break;
                case 1:
                    lb = ResourceManager.getInstance().getString('CG08','Extranet');
                    break;
                case 2:
                    lb = ResourceManager.getInstance().getString('CG08','Contrat');
                    break;
                default:
                    lb = "";
                    break;
            }
            return lb;
        }

        /**
         *  Retourne le <b>libélle du cout</b> correspondant
         *  à l'id passé en paramètre (Payant ou Gratuit)
         */
        public function getCoutLabel(value:Number):String
        {
            var str:String = "";
            value == 1 ? str = ResourceManager.getInstance().getString('CG08','Payant') : str = ResourceManager.getInstance().getString('CG08','Gratuit');
            return str;
        }

        /**
         *  Retourne le <b>libélle du mode de récupération</b> correspondant
         *  à l'id passé en paramètre (recherche locale dans la liste chargée
         *  à l'initialisation)
         */
        public function getRecupCollecteLabel(value:Number):String
        {
            return getEntityLabel(ENTITY_RECUPMODE, value);
        }

        /**
         *  Retourne le <b>libélle </b> correspondant
         *  à la valeur et au type passé en paramètre
         *  ( type :ENTITY_OPERATEUR | ENTITY_DATATYPE | ENTITY_RECUPMODE )
         */
        public function getEntityLabel(type:String, value:Number):String
        {
            var collec:ArrayCollection = new ArrayCollection();
            var label:String = "";
            switch(type)
            {
                case ENTITY_OPERATEUR:
                    collec = operateurList;
                    break;
                case ENTITY_DATATYPE:
                    collec = datatypeList;
                    break;
                case ENTITY_RECUPMODE:
                    collec = recupModeList;
                    break;
                default:
                    break;
            }
            for(var i:int; i < collec.length; i++)
            {
                if(value == collec.getItemAt(i).value as Number)
                {
                    label = collec.getItemAt(i).label;
                }
            }
            return label;
        }

        /**
         *  Retrouve dans la collection locale des modes de récupération
         *  l'<b>index de positionnement</b> ( pour initialisation de la combo)
         */
        public function getRecupModeIndex(value:Number):Number
        {
            var index:Number = -1;
            if(recupModeList)
            {
                var item:SimpleEntityVO;
                for(var i:int = 0; i < recupModeList.length; i++)
                {
                    item = recupModeList.getItemAt(i) as SimpleEntityVO;
                    if(item.value == value)
                    {
                        index = i;
                        break;
                    }
                }
            }
            return index;
        }

        /**
         * Override de la popup d'Alert.show pour pouvoir lui appliquer
         * un style spécifique.
         * @param parent
         * @param text
         * @param title
         * @param iconClass
         *
         */
        public function consalerte(parent:DisplayObject, text:String, title:String = "", iconClass:Class = null, style:String = "alert1"):void
        {
            var a:Alert = new Alert();
            a.text = text
            a.iconClass = iconClass;
            a.title = title;
            a.styleName = style;
            PopUpManager.addPopUp(a, parent, true);
            PopUpManager.centerPopUp(a);
        }

        public function generateTestChaineFacturationList():ArrayCollection
        {
            var list:ArrayCollection = new ArrayCollection();
            /* for(var i:int = 0; i < 17; i++)
               {
               var vo:CompteFacturationVO = new CompteFacturationVO();
               vo.dataType = 1;
               vo.nbImport = 5;
               vo.expectedNbImport = 12;
               vo.ecart = Number(vo.expectedNbImport) - Number(vo.nbImport);
               vo.emissionDate = new Date(2011, 7, 7);
               vo.firstImportDate = new Date(2011, 7, 7);
               vo.firstImportEmissionFactureDate = new Date(2012, 8, 11);
               vo.firstImportEmissionFacturePeriod = ConversionUtils.setPeriodString(new Date(2012, 8, 11), new Date(2011, 5, 10));
               vo.lastImportDate = new Date(2009, 4, 2);
               vo.lastImportEmissionFactureDate = new Date(2012, 8, 3);
               vo.lastImportEmissionFacturePeriod = ConversionUtils.setPeriodString(new Date(2012, 8, 11), new Date(2011, 5, 10));
               vo.periodicity = EPeriodicity.BIMESTRIELLE;
               vo.rocfId = 1235 + i;
               list.addItem(vo);
             }*/
            return list;
        }

        public function get collecteTypeList():ArrayCollection
        {
            return _collecteTypeList;
        }

        public function set collecteTypeList(value:ArrayCollection):void
        {
            _collecteTypeList = value;
        }

        public function get currentCollecteTypeVO():CollecteTypeVO
        {
            return _currentCollecteTypeVO;
        }

        public function set currentCollecteTypeVO(value:CollecteTypeVO):void
        {
            _currentCollecteTypeVO = value;
        }

        public function get operateurList():ArrayCollection
        {
            return _operateurList;
        }

        public function set operateurList(value:ArrayCollection):void
        {
            _operateurList = value;
        }

        public function get datatypeList():ArrayCollection
        {
            return _datatypeList;
        }

        public function set datatypeList(value:ArrayCollection):void
        {
            _datatypeList = value;
        }

        public function get recupModeList():ArrayCollection
        {
            return _recupModeList;
        }

        public function set recupModeList(value:ArrayCollection):void
        {
            _recupModeList = value;
        }

        public function get collecteTypeService():FctGestionCollecteService
        {
            return _collecteTypeService;
        }

        public function set collecteTypeService(value:FctGestionCollecteService):void
        {
            _collecteTypeService = value;
        }

        public function set operateurListAll(value:ArrayCollection):void
        {
            _operateurListAll = value;
        }

        public function get operateurListAll():ArrayCollection
        {
            return _operateurListAll;
        }

        public function set datatypeListAll(value:ArrayCollection):void
        {
            _datatypeListAll = value;
        }

        public function get datatypeListAll():ArrayCollection
        {
            return _datatypeListAll;
        }

        public function set recupModeListAll(value:ArrayCollection):void
        {
            _recupModeListAll = value;
        }

        public function get recupModeListAll():ArrayCollection
        {
            return _recupModeListAll;
        }

        /**
         * Rajoute un item de type <code>SimpleEntityVO</code> par défaut à une collection ( ex : 'tout type')
         * @param  collec : la collection à laquelle rejouter cet item
         * @param label : le label de cette item
         * @param value  :la valeur associé ( ex : 0 )
         * @return une collection
         *
         */
        private function getCollecWithAllItem(collec:ArrayCollection, label:String, value:*):ArrayCollection
        {
            var collection:ArrayCollection = new ArrayCollection();
            for(var i:int; i < collec.length; i++)
            {
                collection.addItem(collec.getItemAt(i));
            }
            var defaultItem:SimpleEntityVO = new SimpleEntityVO();
            defaultItem.label = label;
            defaultItem.value = value;
            collection.addItemAt(defaultItem, 0);
            return collection;
        }

        /**
         * Rajoute un item de type <code>OperateurVO</code> par défaut à une collection ( ex : 'Tous')
         * @param  collec : la collection à laquelle rejouter cet item
         * @param label : le label de cette item
         * @param value  :la valeur associé ( ex : 0 )
         * @return une collection
         *
         */
        private function getCollecOperateurWithAllItem(collec:ArrayCollection, label:String, value:*):ArrayCollection
        {
			trace("Entrée dans getCollecOperateurWithAllItem");
            var collection:ArrayCollection = new ArrayCollection();
            for(var i:int; i < collec.length; i++)
            {
                collection.addItem(collec.getItemAt(i));
            }
            var defaultItem:OperateurVO = new OperateurVO();
            defaultItem.label = label;
            defaultItem.value = value;
            defaultItem.libelleRocf = value;
            collection.addItemAt(defaultItem, 0);
            return collection;
        }

        public function get coutsList():ArrayCollection
        {
            return _coutsList;
        }

        public function set coutsList(value:ArrayCollection):void
        {
            _coutsList = value;
        }

        public function get collecteClientList():ArrayCollection
        {
            return _collecteClientList;
        }

        public function set collecteClientList(value:ArrayCollection):void
        {
            _collecteClientList = value;
        }

        public function get publishedTemplateList():ArrayCollection
        {
            return _publishedTemplateList;
        }

        public function set publishedTemplateList(value:ArrayCollection):void
        {
            _publishedTemplateList = value;
        }

        public function get userRacineId():Number
        {
            return _userRacineId;
        }

        public function set userRacineId(value:Number):void
        {
            _userRacineId = value;
        }

        public function get racineList():ArrayCollection
        {
            return _racineList;
        }

        public function set racineList(value:ArrayCollection):void
        {
            _racineList = value;
        }

        public function get userId():Number
        {
            return _userId;
        }

        public function set userId(value:Number):void
        {
            _userId = value;
        }

        public function get operateurWithTemplateList():ArrayCollection
        {
            return _operateurWithTemplateList;
        }

        public function set operateurWithTemplateList(value:ArrayCollection):void
        {
            _operateurWithTemplateList = value;
        }
    }
}