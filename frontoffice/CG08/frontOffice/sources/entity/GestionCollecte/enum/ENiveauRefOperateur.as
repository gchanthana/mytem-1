package entity.GestionCollecte.enum
{
    import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
    public class ENiveauRefOperateur extends Enumeratio
    {
        public static const CHAINE_FACTURATION:ENiveauRefOperateur = new ENiveauRefOperateur("CHAINE_FACTURATION", ResourceManager.getInstance().getString('CG08','Cha_ne_Facturation'));
        public static const GROUPEMENT_CHAINES_FACTURATION:ENiveauRefOperateur = new ENiveauRefOperateur("GROUPEMENT_CHAINES_FACTURATION", ResourceManager.getInstance().getString('CG08','Groupement_Cha_nes_Facturation'));

        public function ENiveauRefOperateur(value:String, label:String)
        {
            super(value, label);
        }

        public static function getCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(CHAINE_FACTURATION, GROUPEMENT_CHAINES_FACTURATION));
        }
    }
}