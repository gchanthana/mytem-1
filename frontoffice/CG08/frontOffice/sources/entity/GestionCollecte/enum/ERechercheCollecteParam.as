package entity.GestionCollecte.enum
{
    import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
    public class ERechercheCollecteParam extends Enumeratio
    {
        public static const ALL:ERechercheCollecteParam = new ERechercheCollecteParam("ALL", ResourceManager.getInstance().getString('CG08','Toutes_les_collectes'));
        public static const CLOSED:ERechercheCollecteParam = new ERechercheCollecteParam("CLOSED", ResourceManager.getInstance().getString('CG08','Les_collectes_inactives'));
        public static const FIRST_IMPORT_LATE:ERechercheCollecteParam = new ERechercheCollecteParam("FIRST_IMPORT_LATE", ResourceManager.getInstance().getString('CG08','Les_collectes_avec_1er_import_en_retard'));
        public static const FUTURE:ERechercheCollecteParam = new ERechercheCollecteParam("FUTURE", ResourceManager.getInstance().getString('CG08','Les_collectes_futures'));
        public static const NO_IDENTIFIANTS:ERechercheCollecteParam = new ERechercheCollecteParam("NO_IDENTIFIANTS", ResourceManager.getInstance().getString('CG08','Les_collectes_sans_les_identifiants_n_ce'));
        //public static const NO_OPEN_IMPORT_DATE:ERechercheCollecteParam = new ERechercheCollecteParam("NO_OPEN_IMPORT_DATE", ResourceManager.getInstance().getString('CG08','Les_collectes_sans_date_d_ouverture_de_l'));
        //public static const NO_ROIC:ERechercheCollecteParam = new ERechercheCollecteParam("NO_ROIC", ResourceManager.getInstance().getString('CG08','Les_collectes_sans_la_R_f_rence_op_rateu'));
        public static const OPENED:ERechercheCollecteParam = new ERechercheCollecteParam("OPENED", ResourceManager.getInstance().getString('CG08','Les_collectes_actives'));

        public function ERechercheCollecteParam(value:String, label:String)
        {
            super(value, label);
        }

        public static function getCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(ALL, CLOSED, OPENED, FIRST_IMPORT_LATE, FUTURE, NO_IDENTIFIANTS));
        }

        public static function getParamManquantsCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(ALL, NO_IDENTIFIANTS));
        }

        public static function getParamEtatCollectesCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(ALL, CLOSED, OPENED, FIRST_IMPORT_LATE, FUTURE));
        }
    }
}