package entity
{
	public class UniversVO
	{
		
		private var _idUnivers:Number;
		private var _nomUnivers:String;
		private var _keyUnivers:String;
		
		public function UniversVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDCG_UNIVERS")){
					this._idUnivers = obj.IDCG_UNIVERS;
				}
				
				if(obj.hasOwnProperty("NOM_UNIVERS")){
					this.nomUnivers = obj.NOM_UNIVERS;
				}
				
				if(obj.hasOwnProperty("UNIVERS_KEY")){
					this.keyUnivers = obj.UNIVERS_KEY;
				}
				
			}
			catch(err:Error)
			{
				trace("##Initialisation d'un objet Univers erronée ");
			}
		}
		
		public function get keyUnivers():String { return _keyUnivers; }
		
		public function set keyUnivers(value:String):void
		{
			if (_keyUnivers == value)
				return;
			_keyUnivers = value;
		}
		
		public function get nomUnivers():String { return _nomUnivers; }
		
		
		public function set nomUnivers(value:String):void
		{
			if (_nomUnivers == value)
				return;
			_nomUnivers = value;
		}
		
		public function get idUnivers():Number { return _idUnivers; }
		
		public function set idUnivers(value:Number):void
		{
			if (_idUnivers == value)
				return;
			_idUnivers = value;
		}
	}
}