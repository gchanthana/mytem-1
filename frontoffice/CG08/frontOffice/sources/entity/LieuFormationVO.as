package entity
{
	[Bindable]
	public class LieuFormationVO
	{
		private var _id:int;
		private var _libelleLieu:String;
		private var _description:String;
		private var _lieuActif:int;

		public function LieuFormationVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDFC_LIEU_FORMATION"))
					this.id = obj.IDFC_LIEU_FORMATION;
				
				if(obj.hasOwnProperty("LIBELLE_LIEU"))
					this.libelleLieu = obj.LIBELLE_LIEU;
				
				if(obj.hasOwnProperty("DESCRIPTION_LIEU"))
					this.description = obj.DESCRIPTION_LIEU;
				
				if(obj.hasOwnProperty("BOOL_ACTIVE"))
					this.lieuActif = obj.BOOL_ACTIVE;
				
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet LieuFormationVO erroné ");
			}
		}
			
		public function get lieuActif():int { return _lieuActif; }
		
		public function set lieuActif(value:int):void
		{
			if (_lieuActif == value)
				return;
			_lieuActif = value;
		}
		
		public function get description():String { return _description; }
		
		public function set description(value:String):void
		{
			if (_description == value)
				return;
			_description = value;
		}
		
		public function get libelleLieu():String { return _libelleLieu; }
		
		public function set libelleLieu(value:String):void
		{
			if (_libelleLieu == value)
				return;
			_libelleLieu = value;
		}
		
		public function get id():int { return _id; }
		
		public function set id(value:int):void
		{
			if (_id == value)
				return;
			_id = value;
		}
		
	}
}