package entity
{
	[Bindable]
	public class ClientVO
	{
		private var _SIRET			:String;
		private var _ADRESSE		:String;
		private var _NOM			:String;
		private var _ID				:int;
		private var _SELECTED		:Boolean = false;
		private var _master			:String;
		private var _idMaster		:Number;
		private var _application	:String = '';
		private var _nbreCollecte	:int = 0;
		private var _distributeur	:String = '';
		private var _nomCRM			:String = '';
		private var _commentaire	:String = '';
		private var _nombreLogin	:Number = 0;
		private var _dateCreation	:Date;
		private var _dateModification:Date;
		
		private var _NBRECORD		:int;
		private var _NBROWS			:Number;
		
		public function ClientVO()
		{
		}
		
		public function fill(obj:Object):Boolean
		{
			try
			{
				if(obj.hasOwnProperty('CLIENT_ADRESSE'))
					this._ADRESSE = obj.CLIENT_ADRESSE;
				
				if(obj.hasOwnProperty('CLIENT_ID'))
					this._ID = obj.CLIENT_ID;
				else if(obj.hasOwnProperty('IDRACINE'))
					this._ID = obj.IDRACINE;
				
				if(obj.hasOwnProperty('CLIENT_NAME'))
					this._NOM = obj.CLIENT_NAME;
				else if(obj.hasOwnProperty('RACINE'))
					this._NOM = obj.RACINE;
				
				if(obj.hasOwnProperty('CLIENT_SIRET'))
					this._SIRET = obj.CLIENT_SIRET;
				
				if(obj.hasOwnProperty('MASTER'))
					this.master = obj.MASTER;
				else if(obj.hasOwnProperty('RACINE_MASTER'))
					this.master = obj.RACINE_MASTER;
				
				if(obj.hasOwnProperty('IDRACINE_MASTER'))
					this.idMaster = obj.IDRACINE_MASTER;
				
				if(obj.hasOwnProperty('APPLICATION'))
					this.application = obj.APPLICATION;
				else if(obj.hasOwnProperty('NOM_APPLICATION'))
					this.application = obj.NOM_APPLICATION;
				
				if(obj.hasOwnProperty('NB_COLLECTE'))
					this.nombreCollecte = obj.NB_COLLECTE;

				if(obj.hasOwnProperty('DISTRIBUTEUR'))
					this.distributeur = obj.DISTRIBUTEUR;
				
				if(obj.hasOwnProperty('COMMENTAIRES'))
					this.commentaire = obj.COMMENTAIRES;
				
				if(obj.hasOwnProperty('NB_LOGIN'))
					this.nombreLogin = obj.NB_LOGIN;
				
				if(obj.hasOwnProperty('DATE_CREATION'))
					this.dateCreation = obj.DATE_CREATION as Date;
				else if(obj.hasOwnProperty('DATE_MODIF'))
					this.dateModification = obj.DATE_MODIF as Date;
				
				if(obj.hasOwnProperty('NBRECORD'))
					this.NBRECORD = obj.NBRECORD;
				
				if(obj.hasOwnProperty('NB_ROW_TOTAL'))
					this.NBROWS = obj.NB_ROW_TOTAL;
				
				return true;
			}
			catch(e:Error)
			{
				trace("Erreur pendant le chargement du ClientVO");
				return false
			}
			return false
		}
		
		public function get NOM():String { return _NOM; }
		public function set NOM(value:String):void
		{
			if (_NOM == value)
				return;
			_NOM = value;
		}
		public function get ADRESSE():String { return _ADRESSE; }
		public function set ADRESSE(value:String):void
		{
			if (_ADRESSE == value)
				return;
			_ADRESSE = value;
		}
		public function get SIRET():String { return _SIRET; }
		public function set SIRET(value:String):void
		{
			if (_SIRET == value)
				return;
			_SIRET = value;
		}
		public function get ID():int { return _ID; }
		public function set ID(value:int):void
		{
			if (_ID == value)
				return;
			_ID = value;
		}
		
		public function get NBRECORD():int { return _NBRECORD; }
		
		public function set NBRECORD(value:int):void
		{
			if (_NBRECORD == value)
				return;
			_NBRECORD = value;
		}
		
		public function get SELECTED():Boolean { return _SELECTED; }
		
		public function set SELECTED(value:Boolean):void
		{
			if (_SELECTED == value)
				return;
			_SELECTED = value;
		}
		
		public function get nombreCollecte():int { return _nbreCollecte; }
		
		public function set nombreCollecte(value:int):void
		{
			if (_nbreCollecte == value)
				return;
			_nbreCollecte = value;
		}
		
		
		public function get application():String { return _application; }
		
		public function set application(value:String):void
		{
			if (_application == value)
				return;
			_application = value;
		}
		
		public function get master():String { return _master; }
		
		public function set master(value:String):void
		{
			if (_master == value)
				return;
			_master = value;
		}
		
		public function get distributeur():String { return _distributeur; }
		
		public function set distributeur(value:String):void
		{
			if (_distributeur == value)
				return;
			_distributeur = value;
		}
		
		public function get dateModification():Date { return _dateModification; }
		
		public function set dateModification(value:Date):void
		{
			if (_dateModification == value)
				return;
			_dateModification = value;
		}
		
		
		public function get dateCreation():Date { return _dateCreation; }
		
		public function set dateCreation(value:Date):void
		{
			if (_dateCreation == value)
				return;
			_dateCreation = value;
		}
		
		
		public function get nombreLogin():Number { return _nombreLogin; }
		
		public function set nombreLogin(value:Number):void
		{
			if (_nombreLogin == value)
				return;
			_nombreLogin = value;
		}
		
		
		public function get commentaire():String { return _commentaire; }
		
		public function set commentaire(value:String):void
		{
			if (_commentaire == value)
				return;
			_commentaire = value;
		}
		
		
		public function get nomCRM():String { return _nomCRM; }
		
		public function set nomCRM(value:String):void
		{
			if (_nomCRM == value)
				return;
			_nomCRM = value;
		}
		
		public function get idMaster():Number { return _idMaster; }
		
		public function set idMaster(value:Number):void
		{
			if (_idMaster == value)
				return;
			_idMaster = value;
		}
		
		public function get NBROWS():Number { return _NBROWS; }
		
		public function set NBROWS(value:Number):void
		{
			if (_NBROWS == value)
				return;
			_NBROWS = value;
		}
	}
}