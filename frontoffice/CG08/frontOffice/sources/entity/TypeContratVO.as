package entity
{
	[Bindable]
	public class TypeContratVO
	{
		private var _id:int;
		private var _libelle:String;
		private var _description:String;
		
		public function TypeContratVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDFC_TYPE_CONTRAT"))
					this.id = obj.IDFC_TYPE_CONTRAT;	
				
				if(obj.hasOwnProperty("LIBELLE_CONTRAT"))
					this.libelle = obj.LIBELLE_CONTRAT;
				
				if(obj.hasOwnProperty("DESCRIPTION_CONTRAT"))
					this.description = obj.DESCRIPTION_CONTRAT;
				
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet TypeContratVO erroné ");
			}
			
		}
		
		public function get description():String { return _description; }
		
		public function set description(value:String):void
		{
			if (_description == value)
				return;
			_description = value;
		}

		public function get libelle():String { return _libelle; }
		
		public function set libelle(value:String):void
		{
			if (_libelle == value)
				return;
			_libelle = value;
		}
		
		public function get id():int { return _id; }
		
		public function set id(value:int):void
		{
			if (_id == value)
				return;
			_id = value;
		}
		
		
	}
}