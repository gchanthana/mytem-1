package ihm.fonction.FctGestionCommunication.popup
{
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import entity.CodeAction;
	import entity.FileUpload;
	import entity.PieceJointeActualiteVO;
	
	import event.fonction.fctGestionCommunication.FctGestionCommunicationEvent;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import service.fonction.FctGestionCommunication.FctGestionCommunicationService;
	import service.pieceJointe.PiecesJointesService;
	
	public class ListePiecesJointesImpl extends TitleWindowBounds
	{
		//----------- VARIABLES -----------//
		
		private var _currentFilesToSave	:ArrayCollection;
		
		private var _objComService		:FctGestionCommunicationService;
		private var _objService			:PiecesJointesService;
		private var _pjToDelete			:PieceJointeActualiteVO = null;
		
		public var dgListePJ			:DataGrid;
		public var idActu				:int;
		[Bindable]
		public var listePJActu			:ArrayCollection = new ArrayCollection();
		private var _listePJActuInBDD	:ArrayCollection = new ArrayCollection();
		
		
		//----------- METHODES -----------//
		
		/* */
		public function ListePiecesJointesImpl()
		{
			super();
			_objComService = new FctGestionCommunicationService();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		/* au creation complete - Handler */
		private function init(fle:FlexEvent):void
		{
			initData();
			initListeners();
		}
		
		/* */
		private function initListeners():void
		{
			_objComService.myDatas.addEventListener(FctGestionCommunicationEvent.PJ_ACTUALITE_EVENT, getPJActuHandler);
		}
		
		/* */
		private function initData():void
		{
			initListePJInBDD(); // liste PJ deja en base pour l'actualité choisie
		}
		
		/* appel du service pour recuperer la liste des PJ affilié à cette actualité */
		private function initListePJInBDD():void
		{
			_objComService.getPJActualite(CvAccessManager.CURRENT_FUNCTION, CodeAction.PJ_ACTUALITES, idActu);
		}
		
		/* retour liste PJ d'une actualité - Handler */
		private function getPJActuHandler(evt:Event):void
		{
			listePJActuInBDD = _objComService.myDatas.listePJActualite;
			fillDataProvider();
		}
		
		/* */
		private function fillDataProvider():void
		{
			listePJActu = mergeArrays(listePJActuInBDD,currentFilesToSave);
		}
		
		/* */
		private function mergeArrays(arrayA:ArrayCollection, arrayB:ArrayCollection):ArrayCollection
		{			
			for each(var item:FileUpload in arrayB)
			{
				var pjActu:PieceJointeActualiteVO = new PieceJointeActualiteVO();
				pjActu.parseToPJVO(item,idActu);
				arrayA.addItem(pjActu);
			}
			return arrayA;
		}
		
		/* processus de copie du lien hypertexte du chemin du fichier */
		/* recupereration datas de la PJ + fermeture popup apres selection hyperlien de la PJ */
		public function dgClickToDeleteFile(le:ListEvent):void
		{
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('CG08', 'Etes_vous_sur_de_vouloir_supprimer_PJ_selectionnee_'), "Gestion des communications", resultAlertDeletePJ);
			_objComService.myDatas.addEventListener(FctGestionCommunicationEvent.DELETE_PJ_ACTUALITE_EVENT,deletePJActualiteHandler);
		}
		
		/* Resultat de confirmation - Handler */
		private function resultAlertDeletePJ(ce:CloseEvent):void
		{
			if(ce.detail == Alert.OK)
				deletePJ();
		}
		
		/* suppression d'une piece jointe selectionnée */
		private function deletePJ():void
		{
			_pjToDelete = (dgListePJ.selectedItem as PieceJointeActualiteVO);
			
			if(_pjToDelete.ENBASE) // PJ enregistrées sur serveur et en BDD
			{
				_objComService.deletePJActualite(CvAccessManager.CURRENT_FUNCTION, CodeAction.DELETE_PJ_ACTUALITE, idActu, _pjToDelete.UUID);
			}
			else // PJ pas encore enregistrées sur serveur et en BDD
			{
				var i:int=0;
				for each(var item:FileUpload in currentFilesToSave)
				{
					if(item.fileId == _pjToDelete.IDFILE)
					{
						currentFilesToSave.removeItemAt(i);
						break;
					}
					i++;
				}
				deletePJActualiteHandler(null);
			}
		}
		
		/* aussitôt apres suppression de PJ - Handler */
		protected function deletePJActualiteHandler(fgce:FctGestionCommunicationEvent):void 
		{
			(dgListePJ.dataProvider as ArrayCollection).removeItemAt(dgListePJ.selectedIndex);
			(dgListePJ.dataProvider as ArrayCollection).itemUpdated(_pjToDelete);
			
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('CG08', 'La_pi_ce_jointe_a_ete_supprim_e'),Application.application as DisplayObject);
		}
		
		/* fermeture de popup - Handler */
		protected function closeHandler(e:Event):void 
		{
			PopUpManager.removePopUp(this);
		}

		public function get currentFilesToSave():ArrayCollection
		{
			return _currentFilesToSave;
		}

		public function set currentFilesToSave(value:ArrayCollection):void
		{
			_currentFilesToSave = value;
		}

		public function get listePJActuInBDD():ArrayCollection
		{
			return _listePJActuInBDD;
		}

		public function set listePJActuInBDD(value:ArrayCollection):void
		{
			_listePJActuInBDD = value;
		}
	}
}