package ihm.fonction.FctGestionCommunication.popup
{
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import entity.CodeAction;
	import entity.FileUpload;
	import entity.PieceJointeActualiteVO;
	
	import event.fonction.fctGestionCommunication.FctGestionCommunicationEvent;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	import ihm.fonction.FctGestionCommunication.fiche.FicheActualiteImpl;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.core.Application;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import service.fonction.FctGestionCommunication.FctGestionCommunicationService;
	import service.pieceJointe.PiecesJointesService;
	
	import utils.utilsForFonction.UtilsCommunication;
	
	public class ListePiecesJointesToLinkImpl extends TitleWindowBounds
	{
		//----------- VARIABLES -----------//
		
		private var _currentFilesToSave	:ArrayCollection;
		private var _listePJActuInBDD	:ArrayCollection = new ArrayCollection();
		
		private var _objComService		:FctGestionCommunicationService;
		private var _objService			:PiecesJointesService;
		
		public static const pathFTP		:String = "https://cache.consotel.fr/documents/news/";
		
		public var dgListePJToLink		:DataGrid;
		public var idActu				:int;
		public var fiche				:FicheActualiteImpl;
		[Bindable]
		public var listePJActu			:ArrayCollection = new ArrayCollection();
		
		
		//----------- METHODES -----------//
		
		/* */
		public function ListePiecesJointesToLinkImpl()
		{
			super();
			_objComService = new FctGestionCommunicationService();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		/* au creation complete - Handler */
		private function init(fle:FlexEvent):void
		{
			initData();
			initListeners();
		}
		
		/* */
		private function initListeners():void
		{
			_objComService.myDatas.addEventListener(FctGestionCommunicationEvent.PJ_ACTUALITE_EVENT, getPJActuHandler);
//			dgListePJToLink.addEventListener(ListEvent.ITEM_CLICK, dgClickItem);
		}
		
		/* */
		private function initData():void
		{
			initListePJInBDD(); // liste PJ deja en base pour l'actualité choisie
		}
		
		/* appel du service pour recuperer la liste des PJ affilié à cette actualité */
		private function initListePJInBDD():void
		{
			_objComService.getPJActualite(CvAccessManager.CURRENT_FUNCTION, CodeAction.PJ_ACTUALITES, idActu);
		}
		
		/* retour liste PJ d'une actualité - Handler */
		private function getPJActuHandler(evt:Event):void
		{
			listePJActuInBDD = _objComService.myDatas.listePJActualite;
			fillDataProvider();
		}
		
		/* */
		private function fillDataProvider():void
		{
			listePJActu = mergeArrays(listePJActuInBDD,currentFilesToSave);
		}
		
		/* */
		private function mergeArrays(arrayA:ArrayCollection, arrayB:ArrayCollection):ArrayCollection
		{
			for each(var item:FileUpload in arrayB)
			{
				var pjActu:PieceJointeActualiteVO = new PieceJointeActualiteVO();
				pjActu.parseToPJVO(item,idActu);
				arrayA.addItem(pjActu);
			}
			return arrayA;
		}
		
		/* processus de copie du lien hypertexte du chemin du fichier */
		/* recupereration datas de la PJ + fermeture popup apres selection hyperlien de la PJ */
		public function dgClickItem(le:ListEvent):void
		{
			var pjToLink:PieceJointeActualiteVO = (dgListePJToLink.selectedItem as PieceJointeActualiteVO);
			var nomPJ:String = UtilsCommunication.replaceDiacritic(pjToLink.NOM);
			var uuidPJ:String = pjToLink.UUID;
			
			fiche.rtEditor.linkTextInput.text = pathFTP + uuidPJ + "/" + nomPJ;
			
			// doc Flex -> Lorsque l’utilisateur (...) appuie sur la touche Entrée, 
			// Flex insère l’équivalent d’une balise HTML <a href="user_text" target="blank"></a>
			pressKeyEnter();
			
			closeHandler(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('CG08', 'L_hyperlien_a_ete_ins_r_'),Application.application as DisplayObject);
		}
		
		/* simuler la pression sur la touche enter sur le textinput du richTextEditor */
		public function pressKeyEnter():void
		{
			var keyCode:uint = Keyboard.ENTER;
			var ke:KeyboardEvent = new KeyboardEvent(KeyboardEvent.KEY_DOWN, true, false, 0, keyCode);
			fiche.rtEditor.linkTextInput.dispatchEvent(ke);
		}
		
		/* fermeture de popup - Handler */
		public function closeHandler(e:Event):void 
		{
			PopUpManager.removePopUp(this);
		}

		public function get currentFilesToSave():ArrayCollection
		{
			return _currentFilesToSave;
		}

		public function set currentFilesToSave(value:ArrayCollection):void
		{
			_currentFilesToSave = value;
		}

		public function get listePJActuInBDD():ArrayCollection
		{
			return _listePJActuInBDD;
		}

		public function set listePJActuInBDD(value:ArrayCollection):void
		{
			_listePJActuInBDD = value;
		}
	}
}