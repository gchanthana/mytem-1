package ihm.fonction.FctGestionPartenaire.popup.formation
{
	import composants.ui.TitleWindowBounds;
	import composants.util.CvDateChooser;
	
	import entity.CodeAction;
	import entity.FormationVO;
	import entity.LieuFormationVO;
	import entity.SujetFormationVO;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.ComboBox;
	import mx.controls.DateField;
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.DateValidator;
	import mx.validators.NumberValidator;
	import mx.validators.Validator;
	
	import service.fonction.FctGestionPartenaire.FctGestionPartenaireService;
	
	public class PopUpCreateFormationImpl extends TitleWindowBounds
	{
		private var _iServiceGestPartenaire		:FctGestionPartenaireService;
		private var _idSelectedPartn			:int;
		private var numv_module					:NumberValidator = new NumberValidator();
		private var numv_lieu					:NumberValidator = new NumberValidator();
		
		[Bindable]public var combo_sujetFormation	:ComboBox;
		[Bindable]public var df_dateFormation		:CvDateChooser;
		[Bindable]public var combo_lieu			:ComboBox;
		public var sv_dateFormation 	:DateValidator;
		
		
		public function PopUpCreateFormationImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		private function init(event:FlexEvent):void
		{
			
		}
		
		protected function close_clickHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnValider_clickHandler(evt:MouseEvent):void
		{
			if(validateEnteredFormation() == true)
			{
				var nvFormation:FormationVO = new FormationVO();
				nvFormation.id = 0;
				nvFormation.objSujetFormation.id = (combo_sujetFormation.selectedItem != null)?(combo_sujetFormation.selectedItem as SujetFormationVO).id : -1;
				nvFormation.dateFormation = DateField.stringToDate(df_dateFormation.text, ResourceManager.getInstance().getString('CG08', '_DD_MM_YYYY_'));
				nvFormation.objLieuFormation.id = (combo_lieu.selectedItem != null)?(combo_lieu.selectedItem as LieuFormationVO).id :-1;

				iServiceGestPartenaire.ajouterEditerFormation(CvAccessManager.CURRENT_FUNCTION, CodeAction.CREATE_UPD_FORMATION, idSelectedPartn, nvFormation);
				this.close_clickHandler(evt);
				
			}
			
		}
		
		private function validateEnteredFormation():Boolean
		{
			numv_module.source = combo_sujetFormation;
			numv_module.property = 'selectedIndex';
			numv_module.minValue = 0;
			numv_module.lowerThanMinError = ResourceManager.getInstance().getString('CG08','Ce_champ_est_obligatoire');
			
			numv_lieu.source = combo_lieu;
			numv_lieu.property = 'selectedIndex';
			numv_lieu.minValue = 0;
			numv_lieu.lowerThanMinError = ResourceManager.getInstance().getString('CG08','Ce_champ_est_obligatoire');
			
			var result:Boolean = false;
			var validationResult:Array = Validator.validateAll([numv_module, numv_lieu, sv_dateFormation]);
			
			if (validationResult.length == 0)
			{
				result = true
			}
			else
			{
				result = false
			}
			
			return result;
		}
		
		protected function dateField_labelFormat(item:Date):String
		{
			var dateFormatter:DateFormatter = new DateFormatter();
			dateFormatter.formatString = ResourceManager.getInstance().getString('CG08', '_DD_MM_YYYY_');
			return dateFormatter.format(item);
			
		}
		
		public function get iServiceGestPartenaire():FctGestionPartenaireService
		{
			return _iServiceGestPartenaire;
		}
		
		public function set iServiceGestPartenaire(value:FctGestionPartenaireService):void
		{
			_iServiceGestPartenaire = value;
		}
		
		public function get idSelectedPartn():int { return _idSelectedPartn; }
		
		public function set idSelectedPartn(value:int):void
		{
			if (_idSelectedPartn == value)
				return;
			_idSelectedPartn = value;
		}
		
	}
}