package ihm.popup
{
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import entity.ClientVO;
	import entity.FileUpload;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import service.FctGestionClientService;
	
	public class PopUpEditClientImpl extends TitleWindowBounds
	{
		
		private var _imgFileRef				:FileReference;
		private var _fileToUpload			:FileUpload;
		private var _iServiceGestClient		:FctGestionClientService;
		private var _currentClient			:ClientVO;
		
		[Bindable]
		public var text_lblErase			:String = ResourceManager.getInstance().getString('CG08', 'Supprimer');
		public var lbl_uploadedImg 			:Label;
		public var img_uploaded 			:Image;
		public var hb_btnUpload				:HBox;
		public var hb_imgUpload				:HBox;
		[Bindable]public var ta_commentaire	:TextArea;
		[Bindable]public var toHide			:Boolean = false;
		
		// création du conteneur de l'image
		public var conteneurImage			:Loader;
		// url de l'image à charger
		public var urlImage					:URLRequest;
		
		public function PopUpEditClientImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		protected function init(event:Event):void
		{
			var strUrl:String = 'http://images.consotel.fr/assets/logo/'+ currentClient.ID +'.png'; 
			img_uploaded.source = strUrl;
		}
		
		protected function close_clickHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnValider_clickHandler(evt:MouseEvent):void
		{
			if(validateEnteredInfosClient())
			{
				if((fileToUpload != null) || (currentClient.commentaire != ta_commentaire.text))
				{
					ConsoviewAlert.afficherAlertConfirmation("Voulez-vous confirmer l\'édition du client?", "Edition client", alertEditContrat);
				}
				else
				{
					ConsoviewAlert.afficherAlertInfo("Aucun chnagement à apporter", "Information", null);
				}
			}
		}
		
		private function alertEditContrat(evt:CloseEvent):void
		{
			if (evt.detail == Alert.OK)
			{
				this.iServiceGestClient.editClient(0, currentClient.ID, currentClient.NOM, ta_commentaire.text, fileToUpload);
				this.close_clickHandler(evt);
			}
		}
		
		private function validateEnteredInfosClient():Boolean
		{
			// TODO Auto Generated method stub
			return true;
		}
		
		protected function onUploadLogoClick(event:MouseEvent):void
		{
			var arrayfilters:Array = [];
			arrayfilters.push(new FileFilter('Types de fichiers supportés: *.png', '*.png'));
			_imgFileRef = new FileReference();
			_imgFileRef.browse(arrayfilters);
			_imgFileRef.addEventListener(Event.SELECT, onFileSelected);
		}
		
		protected function onFileSelected(event:Event):void
		{
			_imgFileRef.removeEventListener(Event.SELECT, onFileSelected);
			_imgFileRef.addEventListener(Event.COMPLETE, onFileLoaded);
			
			_imgFileRef.load();
			
			fileToUpload = new FileUpload();
			fileToUpload.fileData 		= _imgFileRef.data;
			fileToUpload.fileName 		= _imgFileRef.name;
			fileToUpload.fileExt 		= _imgFileRef.type;
			fileToUpload.fileSize 		= _imgFileRef.size;
		}
		
		protected function onFileLoaded(evt:Event):void
		{
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loader_complete);
			loader.loadBytes(_imgFileRef.data);
		}
		
		protected function loader_complete(evt:Event):void 
		{
			var sourceBMP:Bitmap = evt.currentTarget.loader.content as Bitmap;
			if((sourceBMP.width <= 300)||(sourceBMP.height <= 52))
			{
				toHide = true;
				lbl_uploadedImg.text =  'Logo téléchargé ' + _imgFileRef.name + ' (' + _imgFileRef.size +' octets).'
				img_uploaded.source = _imgFileRef.data;
				fileToUpload.fileData 		= _imgFileRef.data;
			}
			else
			{
				Alert.show( 'Logo trop grand '+'('+ sourceBMP.width+ ' px , ' +sourceBMP.height + ' px)' +'\.\n Merci de respecter les dimensions maximales: 300px en longeur et 52px en hauteur.)');
			}
		}
		
		protected function onDeleteLogoClick(event:MouseEvent):void
		{
			_imgFileRef = new FileReference();
			
			hb_btnUpload.visible = true;
			hb_btnUpload.includeInLayout = true;
			
			hb_imgUpload.visible = false;
			hb_imgUpload.includeInLayout = false;
		}
		
		public function get fileToUpload():FileUpload { return _fileToUpload; }
		
		public function set fileToUpload(value:FileUpload):void
		{
			if (_fileToUpload == value)
				return;
			_fileToUpload = value;
		}
		
		public function get iServiceGestClient():FctGestionClientService { return _iServiceGestClient; }
		
		public function set iServiceGestClient(value:FctGestionClientService):void
		{
			if (_iServiceGestClient == value)
				return;
			_iServiceGestClient = value;
		}
		
		[Bindable]
		public function get currentClient():ClientVO { return _currentClient; }
		
		public function set currentClient(value:ClientVO):void
		{
			if (_currentClient == value)
				return;
			_currentClient = value;
		}
	}
}