package utils.GestionCollecte
{
	import mx.controls.DateChooser;
	import mx.core.mx_internal;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.DateChooserEvent;
	import mx.resources.ResourceManager;
	
	
	use namespace mx_internal;
	
	public class MonthChooser extends DateChooser
	{
		
		public function MonthChooser()
		{
			monthNames=[ResourceManager.getInstance().getString('CG08', 'Janvier'), ResourceManager.getInstance().getString('CG08', 'F_vrier'), 
				ResourceManager.getInstance().getString('CG08', 'Mars'), ResourceManager.getInstance().getString('CG08', 'Avril'), 
				ResourceManager.getInstance().getString('CG08', 'Mai'), ResourceManager.getInstance().getString('CG08', 'Juin'),
				ResourceManager.getInstance().getString('CG08', 'Juillet'), ResourceManager.getInstance().getString('CG08', 'Ao_t'),
				ResourceManager.getInstance().getString('CG08', 'Septembre'), ResourceManager.getInstance().getString('CG08', 'Octobre'),
				ResourceManager.getInstance().getString('CG08', 'Novembre'), ResourceManager.getInstance().getString('CG08', 'D_cembre')];
		}
		override protected function createChildren():void
		{
			super.createChildren();
			dateGrid.addEventListener(DateChooserEvent.SCROLL,dateGrid_scrollHandler);
		}
		
	 	override protected function measure():void
	    {
         	super.measure();
         	dateGrid.visible = false;
       		measuredHeight = measuredHeight - dateGrid.getExplicitOrMeasuredHeight();
		}
		
		private function dateGrid_scrollHandler(event:DateChooserEvent):void
		{
		    var month:int = DateChooserEvent(event).currentTarget.displayedMonth;
		    var year:int = DateChooserEvent(event).currentTarget.displayedYear;
		    
		    selectedDate = new Date(year, month, 1);
		    
		    
		    var e:CalendarLayoutChangeEvent = new CalendarLayoutChangeEvent(CalendarLayoutChangeEvent.CHANGE);
        	e.newDate = selectedDate;
	        dispatchEvent(e);
		    
		}
	
	}
}