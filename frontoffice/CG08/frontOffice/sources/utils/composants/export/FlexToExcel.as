package utils.composants.export
{
	import com.as3xls.xls.ExcelFile;
	import com.as3xls.xls.Sheet;
	
	import flash.errors.IllegalOperationError;
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.controls.dataGridClasses.DataGridListData;
	import mx.controls.listClasses.ListData;
	import mx.core.ClassFactory;
	import mx.core.IFactory;
	import mx.utils.ObjectUtil;
	
	import paginatedatagrid.PaginateDatagrid;
	
	public class FlexToExcel {
		public function FlexToExcel() {
			//			throw new IllegalOperationError ("Class "ExcelExporterUtil" is static. You can't instance this");
		}
		
		public static function exportDataGrid (dg:DataGrid, filename:String="exportedData.xls", listData:DataGridListData=null):void 
		{
			
			var head:Array = new Array();
			for each (var item:DataGridColumn in dg.columns) 
			{
				head.push(item.headerText);
			}
			
			var data:Array = new Array();
			
//			var i:int = 0;
			for each (var obj:Object in dg.dataProvider) 
			{
//				i = i +1;
//				trace("\n -------------------  Obj: " + i  );
				var arr:Array=new Array();
				for each (var dgc:DataGridColumn in dg.columns) 
				{
//					var infoCol:String = ' -- '+ dgc.headerText +' - ' + dgc.dataField + ' :: ';
					
					if (dgc.itemRenderer != null) 
					{
						var classFactory:ClassFactory=dgc.itemRenderer as ClassFactory;
						var itemRenderer:Object = classFactory.newInstance();
						var listData:DataGridListData = new DataGridListData(dgc.editorDataField, dgc.dataField, arr.length+1,"id", dg,1);
						itemRenderer.listData = listData;
						
						if (itemRenderer.hasOwnProperty(dgc.editorDataField)) 
						{
							itemRenderer[dgc.editorDataField] = obj[dgc.dataField];
							itemRenderer.data = obj; // must be last apply for complex itemRenderer
							arr.push(itemRenderer[dgc.editorDataField]);
//							trace(infoCol+ "itemRenderer[dgc.editorDataField] : " + itemRenderer[dgc.editorDataField]);
						} 
						else 
						{
//							trace(infoCol + "itemRenderer.toString() : \t" + itemRenderer.toString());
							arr.push(itemRenderer.toString());
						}
					} 
					else if (dgc.labelFunction != null) 
					{
//						trace(infoCol + "dgc.labelFunction(obj,null) : " + dgc.labelFunction(obj,dgc));
						arr.push(dgc.labelFunction(obj,dgc));
					} 
					else
					{
//						trace(infoCol + "obj[dgc.dataField] : \t" + obj[dgc.dataField]);
						(obj[dgc.dataField] != null)?arr.push(obj[dgc.dataField]) : arr.push('');
					}
					
				}
				data.push(arr);
			}
			
			export(head,data,filename);
		}
		
		public static function exportPaginatedDataGrid (dg:PaginateDatagrid, filename:String="exportedData.xls", listData:DataGridListData=null):void 
		{
			
			var head:Array = new Array();
			for each (var item:DataGridColumn in dg.columns) 
			{
				head.push(item.headerText);
			}
			
			var data:Array = new Array();
			
			//			var i:int = 0;
			for each (var obj:Object in dg.dataprovider) 
			{
				//				i = i +1;
				//				trace("\n -------------------  Obj: " + i  );
				var arr:Array=new Array();
				for each (var dgc:DataGridColumn in dg.columns) 
				{
					//					var infoCol:String = ' -- '+ dgc.headerText +' - ' + dgc.dataField + ' :: ';
					
					if (dgc.itemRenderer != null) 
					{
						var classFactory:ClassFactory=dgc.itemRenderer as ClassFactory;
						var itemRenderer:Object = classFactory.newInstance();
						var listData:DataGridListData = new DataGridListData(dgc.editorDataField, dgc.dataField, arr.length+1,"id", dg,1);
						if (itemRenderer.hasOwnProperty(listData)) {
							itemRenderer.listData = listData;
						}
						
						
						if (itemRenderer.hasOwnProperty(dgc.editorDataField)) 
						{
							itemRenderer[dgc.editorDataField] = obj[dgc.dataField];
							itemRenderer.data = obj; // must be last apply for complex itemRenderer
							arr.push(itemRenderer[dgc.editorDataField]);
							//							trace(infoCol+ "itemRenderer[dgc.editorDataField] : " + itemRenderer[dgc.editorDataField]);
						} 
						else 
						{
							//							trace(infoCol + "itemRenderer.toString() : \t" + itemRenderer.toString());
							arr.push(itemRenderer.toString());
						}
					} 
					else if (dgc.labelFunction != null) 
					{
						//						trace(infoCol + "dgc.labelFunction(obj,null) : " + dgc.labelFunction(obj,dgc));
						arr.push(dgc.labelFunction(obj,dgc));
					} 
					else
					{
						//						trace(infoCol + "obj[dgc.dataField] : \t" + obj[dgc.dataField]);
						(obj[dgc.dataField] != null)?arr.push(obj[dgc.dataField]) : arr.push('');
					}
					
				}
				data.push(arr);
			}
			
			export(head,data,filename);
		}
		
		
		static private function export(head:Array, data:Array,filename:String):void {
			// Create sheet
			var cols:int = head.length;
			var rows:int = data.length+1;
			var sheet:Sheet = new Sheet();
			sheet.resize(rows,cols);
			
			// Header
			var row:int=0;
			var col:int=0;
			for each (var item:String in head) {
				sheet.setCell(row,col,item);
				col++;
			}
			
			// Data
			row=1;
			col=0;
			for each (var dataRow:Array in data) {
				for each (var dataCol:String in dataRow) {
					sheet.setCell(row,col,dataCol);
					col++;
				}
				col=0;
				row++;
			}
			
			// Add sheet
			var xls:ExcelFile = new ExcelFile();
			xls.sheets.addItem(sheet);
			var bytes:ByteArray = xls.saveToByteArray();
			// Generate file
			var fileRef:FileReference = new FileReference();
			fileRef.save(bytes, filename);
		}
	}
}