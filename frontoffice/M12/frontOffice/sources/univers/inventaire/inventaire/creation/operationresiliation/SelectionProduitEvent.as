package univers.inventaire.inventaire.creation.operationresiliation
{
	import flash.events.Event;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	
	/**
	 * Classe évènement 
	 * Cette classe est utilisée pour transmettre une liste de ressources 
	 * */
	public class SelectionProduitEvent extends Event
	{
		//tableau contenant une liste d'identifiants de ressources
		private var _tabproduitsSelectionnee : Array;		
		
		//Element selectionné 
		private var _ligneSelectionnee : ElementCommande;
		
		//Date enregistrement demande
		private var _dateDemande : String = "";
		
		/**
		 * Constructeur
		 * voir la classe flash.events.Event pour les parametres 
		 * */ 
		public function SelectionProduitEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		/**
		 * Setter pour le tabelau des ressources 
		 * @param tab un tableau d'identifiants de ressources
		 * */
		public function set tabProduits(tab : Array):void{
			_tabproduitsSelectionnee = tab;
		}
		
		/**
		 * Retourne le tableau d'identifiants de ressources
		 * @return _tabproduitsSelectionnee une tabelau d'identifiant de ressource
		 * */
		public function get tabProduits():Array{
			return _tabproduitsSelectionnee;
		}
		
		/**
		 * Setter pour la ligne selectionnée
		 * @param l la lignes sélectionnée
		 * */
		public function set ligneSelectionnee(l : ElementCommande):void{
			_ligneSelectionnee  = l;
		}
		
		/**
		 * Retourne la ligne selectionnée
		 * @return _ligneSelectionnee  la lignes selectionnée
		 * */
		public function get ligneSelectionnee ():ElementCommande{
			return _ligneSelectionnee ;
		}
		
		/**
		 * Setter pour la date de la demande 
		 * @param d la date de la demande au format(jj/mm/aaaa)
		 * */
		public function set dateDemande(d : String):void{
			_dateDemande = d;
		}
		
		/**
		 * Retourne la date de la demande 
		 * @return date au format(jj/mm/aaaa)
		 * */
		public function get dateDemande():String{
			return _dateDemande ;
		}
	}
}