package univers.inventaire.inventaire.menu.event
{
	import flash.events.Event;
	
	import univers.facturation.suivi.controles.vo.Operateur;

	public class JournauxEvent extends Event
	{
		public static const DATE_CHANGE:String = "dateChange";
		
		private var _date_reference:Date;
		private var _dansInventaire:Number;
		private var _operateur:Operateur;
		
		public function JournauxEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{	
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new JournauxEvent(type,bubbles,cancelable);
		}

		public function set date_reference(value:Date):void
		{
			_date_reference = value;
		}

		public function get date_reference():Date
		{
			return _date_reference;
		}
		public function set dansInventaire(value:Number):void
		{
			_dansInventaire = value;
		}

		public function get dansInventaire():Number
		{
			return _dansInventaire;
		}
		public function set operateur(value:Operateur):void
		{
			_operateur = value;
		}

		public function get operateur():Operateur
		{
			return _operateur;
		}
	}
}