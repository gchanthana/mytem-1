package univers.inventaire.inventaire.journaux
{
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.collections.ArrayCollection;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	
	
	/**
	 * Class non utilisée
	 * 
	 * */
	public class MauvaiseDateEffetResiliation extends MauvaiseDateEffetResiliationIHM
	{
		private var op : AbstractOperation;
		[Bindable]
		private var dataGridData : ArrayCollection;
		
		public function MauvaiseDateEffetResiliation()
		{
			//TODO: implement function
			super();			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		public function refresh():void{
			chargerData();
		}
		
		private function initIHM(fe : FlexEvent):void{
			myGrid.dataProvider = dataGridData;
			 
		}
		
		private function chargerData():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
						
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Journaux",
																				"getListeMauvaiseDateResi",
																				chargerDataResultHandler);				
			RemoteObjectUtil.callService(op,idGroupeMaitre,"");		
		}
		
		private function chargerDataResultHandler(re :ResultEvent):void{
			
			dataGridData = re.result as ArrayCollection;
			dataGridData.refresh();			 
		}
						
		
	}
}