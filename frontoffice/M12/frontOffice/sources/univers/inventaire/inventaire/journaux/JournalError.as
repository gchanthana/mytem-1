package univers.inventaire.inventaire.journaux
{
	/**
	 * Classe dérivant de Error,
	 * Elle sert à lancer un exception de type JournalError  
	 * */
	public class JournalError extends Error
	{
		public function JournalError(message:String="", id:int=0)
		{
			//TODO: implement function
			super(message, id);
		}
		
	}
}