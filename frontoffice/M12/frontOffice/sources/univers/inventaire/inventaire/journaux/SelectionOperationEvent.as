package univers.inventaire.inventaire.journaux
{
	import flash.events.Event;
	
	
	/**
	 * Classe evenment SelectionOperationEvent
	 * Transmet les informations nécessaires à l'affichage d'une opération depuis une ligne d'un journal
	 * */
	public class SelectionOperationEvent extends Event
	{
		//l'identifiant de l'opération à afficher
		private var _idOperation : int;
		
		/**
		 * Constante static deffinissant le type OPERATION_SELECTED
		 * 
		 * */
		public static const OPERATION_SELECTED : String = "operationSelected";
		
		
		/**
		 * Constructeur 
		 * voir la classe flash.events.Event pour les parametres
		 * */
		public function SelectionOperationEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		/**
		 * Getter pour l'attribut idOperation 
		 * @param idop l'identifiant de l'opération que l'on souhaite afficher
		 * */
		public function set idOperation(idop : int):void{
			_idOperation = idop;
		}
		
		/**
		 * Setter pour l'attribut idOperation 
		 * @return _idOperation l'identifiant de l'opération que l'on souhaite afficher
		 * */
		public function get idOperation():int{
			return _idOperation;
		}
		
	}
}