package univers.inventaire.inventaire
{
	import mx.containers.Box;
	import mx.containers.VBox;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.inventaire.journaux.AfficherJournalEvent;
	import univers.inventaire.inventaire.journaux.InventaireComplet;
	import univers.inventaire.inventaire.journaux.JournauxMain;
	import univers.inventaire.inventaire.menu.JournauxMenu;
	import univers.inventaire.inventaire.menu.event.JournauxEvent;

	public class FunctionJournauxImpl extends VBox 
	{
		public var myJournauxMenu : JournauxMenu;
		public var myMainBox : Box;
		
		//Reference vers l ecran contenant les Journeaux
		protected var myMainJournaux:JournauxMain = new JournauxMain();
		
		public function FunctionJournauxImpl()
		{	
			super();
		}
		
		protected override function commitProperties():void{
			/*-- Journaux ---*/
			myJournauxMenu.addEventListener(AfficherJournalEvent.AFFICHER_JOURNAUX,displayMenuPanel);	
			addEventListener(JournauxEvent.DATE_CHANGE, _localeDateChangeHandler);
		}
		
		protected  function afterCreationComplete(event:FlexEvent):void {
			trace("(FunctionListeAppels) Perform IHM Initialization (" + getUniversKey() + "," + getFunctionKey() + ")");
		}
		
		public  function getUniversKey():String {
			return "GEST";
		}
		
		public  function getFunctionKey():String {
			return "GEST_REPORT";
		}
		
		public  function afterPerimetreUpdated():void {
			 myJournauxMenu.onPerimetreChange();
			 myMainJournaux.onPerimetreChange();
			 
			 	 
			trace("(FunctionListeAppels) Perform After Perimetre Updated Tasks");
			trace("(FunctionListeAppels) SESSION FLEX :\n" +
						ObjectUtil.toString(CvAccessManager.getSession()));
		}
		
		
		
		
		//Affiche le journal suivant le type et eventuellement l'id du noeud passés en parametre
		//via l'evenement AfficherJournalEvent
		private function displayMenuPanel(aje : AfficherJournalEvent):void{			
			if (myMainBox.getChildren().length > 0){
				myMainJournaux.afficherJournal(aje.typeJournal,aje.nodeInfos,myJournauxMenu.dcDateRef.selectedDate);				
			}else{
				myMainBox.addChild(myMainJournaux);
				myMainJournaux.afficherJournal(aje.typeJournal,aje.nodeInfos,myJournauxMenu.dcDateRef.selectedDate);	
			} 
		}
		
		protected function _localeDateChangeHandler(event:JournauxEvent):void
		{
			if(event.target is JournauxMenu)
			{
				myMainJournaux.updateDate(event.date_reference);
			}
			else if(event.target is InventaireComplet)
			{
				myJournauxMenu.updateDate(event.date_reference);
			}
		}
				
		
	}
}