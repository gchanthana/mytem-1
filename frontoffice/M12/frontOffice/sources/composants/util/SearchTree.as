package composants.util
{
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.controls.Tree;
	import mx.collections.ArrayCollection;
	import mx.managers.CursorManager;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import composants.tb.recherche.renderer.TIRFolderLines;
	import mx.core.ClassFactory;
	import univers.UniversManager;
	import mx.utils.ObjectUtil;
	import composants.access.perimetre.PerimetreTreeItemRenderer;
	import mx.core.IFactory;
	import composants.access.PerimetreTreeEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	public class SearchTree extends SearchTreeIHM {
		public static const NODE_CHANGED : String = "nodeChanged";
		public var nodeInfos:Object; // Infos sur le noeud séléctionné
		private var perimetreTreeItemRenderer:IFactory;
		private var currentSelectedIndex:int = 0; // Index du groupe
		private var currentXmlResultIndex:int = 0; // Index de la recherche
		private var searchResult:int = 0; // Nombre de résultat de la recherche de noeuds
		
		 
		protected var xmlResult:XMLList = null;
		
		public function SearchTree(){
			super();
			
			perimetreTreeItemRenderer = new ClassFactory(composants.access.perimetre.PerimetreTreeItemRenderer);
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);			
		}
				
		public function initIHM(e:FlexEvent):void {
			myTree.addEventListener(ListEvent.CHANGE,loadNodeInfos);
			myTree.addEventListener(PerimetreTreeEvent.DATA_PROVIDER_SET,onDataProviderReady);
			initDp();
		}
		
		private function loadNodeInfos(event:ListEvent):void {
			var clientID : Number = CvAccessManager.getSession().USER.CLIENTACCESSID;
			var loadNodeInfosOp:AbstractOperation =
				RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
										"fr.consotel.consoview.access.PerimetreManager",
										"getNodeInfos",processNodeInfos);
			if(myTree.searchTree.selectedItem == null)
				myTree.searchTree.selectNodeById(UniversManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
			var perimetreIndex:int = myTree.searchTree.selectedItem.@NID;
			
			if (perimetreIndex > 0){
				RemoteObjectUtil.callService(loadNodeInfosOp,clientID,perimetreIndex);
			} else{
				nodeInfos = null;
			}
		}
		
		private function processNodeInfos(event:ResultEvent):void {
			nodeInfos = event.result;
			dispatchEvent(new Event(NODE_CHANGED));
		}
		
		private function onDataProviderReady(event:PerimetreTreeEvent):void {
			if(myTree.searchTree.selectedItem == null)
				myTree.searchTree.selectNodeById(UniversManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
			loadNodeInfos(null);
		}
		
		public function initDp():void {
			if(UniversManager.getSession().CURRENT_PERIMETRE.TYPE_LOGIQUE == ConsoViewPerimetreObject.TYPE_ROOT) {
				myTree.searchTree.dataProvider = UniversManager.getSession().CURRENT_PERIMETRE.nodeList
				myTree.searchTree.labelField = "LBL";
			//	myTree.searchTree.dataDescriptor = UniversManager.getTreeDataDescriptor();
				
			} else {
				var xmlPerimetreData:XML = UniversManager.getSession().CURRENT_PERIMETRE.nodeList as XML;
				var resultList:XMLList =
					xmlPerimetreData.descendants().(@NID == UniversManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
				if(resultList.length() > 0) {
					myTree.searchTree.dataProvider = resultList[0];
					myTree.searchTree.labelField = "LBL";
					
				//	myTree.searchTree.dataDescriptor =  UniversManager.getTreeDataDescriptor();
				}
			}
			myTree.searchTree.itemRenderer = perimetreTreeItemRenderer;
		}
		
		public function onPerimetreChange():void {
			myTree.onPerimetreChange();
		}
		
		 
		public function clearTree(ev:MouseEvent):void {			
		}
		
		
		public function setDp(xml:Object):void {			
			myTree.searchTree.dataProvider = new XMLList(xml);
		}
		
	}
}