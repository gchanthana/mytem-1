package composants.util.employes
{
	///////////////////////////////////////////////////////////
	//  EmployesUtils.as
	//  Macromedia ActionScript Implementation of the Class EmployesUtils
	//  Generated by Enterprise Architect
	//  Created on:      15-oct.-2008 15:08:56
	//  Original author: samuel.divioka
	///////////////////////////////////////////////////////////

	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	/**
	 * [Bindable]
	 * @author samuel.divioka
	 * @version 1.0
	 * @created 15-oct.-2008 15:08:56
	 */
	[Bindable]
	public class EmployesUtils extends EventDispatcher
	{
	    private var _listeEmployes: ArrayCollection;
	    private var _selectedEmploye: Employe;
		private var _tmpEmploye:Employe = new Employe();
		
		public static const EMPLOYE_CREATED:String="employeCreated";
		public static const EMPLOYE_UPDATED:String="employeUpdated";
		public static const EMPLOYE_DELETED:String="employeDeleted";
		public static const EMPLOYE_LOADED:String="employeLoaded";
		
		
		public function EmployesUtils(){
	
		}
	
	    /**
	     * fournit la liste des employés des pools de gestion d'un gestionnaire suivant
	     * une clef de rechercher, si id du gestionnaire = 0, on prend les employes de la
	     * racine.
	     * clef = | NOM | PRENOM | CODE_INTERNE | FONCTION_EMPLOYE | REFERENCE_EMPLOYE
	     * 
	     * @param gestionnaire
	     * @param clef
	     */
	    public function fournirListeEmployes(idPool:Number, clef:String): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				 "fr.consotel.consoview.inventaire.employes.EmployesUtils",
																				 "fournirListeEmployes",
																				 fournirListeEmployesResultHandler);
		 	RemoteObjectUtil.callService(op,idPool,clef)
	    }
	    
	    /***
	    * 
	    * Affichage de la fiche employe 
	    * */
	    public function fournirDetailEmploye(value:Employe = null):void
	    {
	    	
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				 "fr.consotel.consoview.inventaire.employes.EmployesUtils",
																				 "fournirDetailEmploye",
																				 function fournirDetailEmployesResultHandler(event:ResultEvent):void
																			     {																			    	
																			    	selectedEmploye = event.result as Employe;
																			    	switch(selectedEmploye.CIVILITE)
																			    	{
																			    		case "1":
																			    		{
																			    			selectedEmploye.LCIVILITE = "Mr.";
																			    			break;	
																			    		}
																			    		case "2":
																			    		{
																			    			selectedEmploye.LCIVILITE = "Mme.";
																			    			break;	
																			    		}
																			    		case "3":
																			    		{
																			    			selectedEmploye.LCIVILITE = "Mlle";
																			    			break;	
																			    		}
																			    		default :
																			    		{
																			    			selectedEmploye.LCIVILITE = "";
																			    			break;
																			    		}
																			    	}
																			    	tmpEmploye =  ObjectUtil.copy(selectedEmploye) as Employe ;
																					dispatchEvent(new Event(EMPLOYE_LOADED));
																			    }
   																			 );
		 	if(value)
		 	{
		 		RemoteObjectUtil.callService(op,value)	
		 	}
		 	else
		 	{
		 		RemoteObjectUtil.callService(op,selectedEmploye)
		 	}
		 	
	    }
	    
	  
	    /***
	    * 
	    * Mise à jour de la fiche employe 
	    * */
	    public function updateInfosEmploye(value:Employe = null):void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				 "fr.consotel.consoview.inventaire.employes.EmployesUtils",
																				 "updateInfosEmploye",
																				 function updateInfosEmployeResultHandler(event:ResultEvent):void
																				 {
																				 	if(event.result > 0)
																				 	{
																				 		ConsoviewAlert.afficherOKImage("Modifications enregistrées");
																				 		
																				 		selectedEmploye = tmpEmploye;
																				 		dispatchEvent(new Event(EMPLOYE_UPDATED));
																				 	}
																				 	else
																				 	{
																				 		Alert.show("Erreur","Erreur");		
																				 	}
																				 	
																				 });
		 	if(value)
		 	{
		 		RemoteObjectUtil.callService(op,value)	
		 	}
		 	else
		 	{
		 		RemoteObjectUtil.callService(op,tmpEmploye)
		 	}
	    }
	    

	
	    /**
	     * 
	     * @param event
	     */
	    protected function fournirListeEmployesResultHandler(event:ResultEvent): void
	    {
	    	listeEmployes = formatterData(event.result as ArrayCollection);
	    	
	    	if(listeEmployes.length > 0)
	    	{
	    		fournirDetailEmploye(listeEmployes[0])	
	    	}
	    	
	    }
	    
	    private   function formatterData(values : ICollectionView):ArrayCollection{
	    	var retour : ArrayCollection = new ArrayCollection();
	    	if (values != null)
	    	{
	    		var cursor : IViewCursor = values.createCursor();
	    		while(!cursor.afterLast){
	    			
	    			var employeObj:Employe = new Employe();	    			
	    			employeObj.NOMPRENOM = cursor.current.NOM;
	    			employeObj.IDEMPLOYE = cursor.current.IDEMPLOYE;
	    			employeObj.IDGROUPE_CLIENT = cursor.current.IDGROUPE_CLIENT;
	    			employeObj.MATRICULE = cursor.current.MATRICULE;
	    			employeObj.CLE_IDENTIFIANT = cursor.current.CLE_IDENTIFIANT;
	    			employeObj.INOUT = 	cursor.current.INOUT;    			
	    			if(employeObj.INOUT > 0)
	    			{
	    				retour.addItem(employeObj);	
	    			}
	    			cursor.moveNext();
	    		}	
	    	}
	    	return retour 
	    }
	    
	    	
	    /**
	     * liste des employés
	     * 
	     * @param values
	     */
	    public function set listeEmployes(values:ArrayCollection): void
	    {
	    	_listeEmployes = values
	    }
	
	    /**
	     * l'employé sélectionné
	     * 
	     * @param value
	     */
	    public function set selectedEmploye(value:Employe): void
	    {
	    	_selectedEmploye = value
	    }
	
	    /**
	     * l'employé sélectionné
	     */
	    public function get selectedEmploye(): Employe
	    {
	    	return _selectedEmploye
	    }
	
	    /**
	     * liste des employés
	     */
	    public function get listeEmployes(): ArrayCollection
	    {
	    	return _listeEmployes
	    }
	    public function set tmpEmploye(value:Employe):void
		{
			_tmpEmploye = value;
		}

		public function get tmpEmploye():Employe
		{
			return _tmpEmploye;
		}
	
	}//end EmployesUtils	
}
