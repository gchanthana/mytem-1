package composants.tb.graph
{
	import composants.tb.graph.charts.ColunmGraph_IHM;
	import mx.events.FlexEvent;
	import mx.controls.dataGridClasses.DataGridBase;
	import mx.controls.dataGridClasses.DataGridColumn;
	import flash.display.DisplayObject;
	import mx.controls.DataGrid;
	import mx.charts.HitData;
	
	public class BarGraphBase extends GraphBase
	{
		protected var graph : ColunmGraph_IHM = new ColunmGraph_IHM();
		
		public function BarGraphBase()
		{
			//TODO: implement function
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE , initGraph);
		}
		
		private function initGraph(fe : FlexEvent):void{
			graph.x = 0;
			graph.y = 0;
			myGraphContener.addChild(graph);	
			graph.myChartHistorique.dataTipFunction = formatDataTip;			 
		}
		
		//formate les tootips du graph des themes
		private function formatDataTip(obj:HitData):String{
		    var periode:String = obj.item["Pèriode"]; 
		    var montantConso:Number = obj.item["Consommations"];
		    var montantAbo:Number = obj.item["Abonnements"];
		    
		    if (obj.element.name == "abo"){
		    	return "Période : <b>"+periode+
		    	"</b><br>Montant Abo.: <b><font color ='#ff0000'>"+cf.format(montantAbo)+"</font></b>";
		    }else if(obj.element.name == "conso"){
		    	return "Période : <b>"+periode+
		    	"</b><br>Montant Conso.: <b><font color ='#ff8000'>"+cf.format(montantConso)+"</font></b>"
		    }
		    
		    return "Période : <b>"+periode+
		    "</b><br>Montant Conso.: <b><font color ='#ff8000'>"+cf.format(montantConso)+"</font></b>"+
		    "<br>Montant Abo.: <b><font color ='#ff0000'>"+cf.format(montantAbo)+"</font></b>";
		}
	}
}