package composants.tb.tableaux.renderer
{
	import mx.skins.halo.DataGridHeaderSeparator;	 
	import flash.display.Graphics;
	
	/**
	 * ItemRenderer pour les entêtes des datagrid
	 * 
	 **/
	public class FlatHeaderSeparator extends DataGridHeaderSeparator
	{
		public function FlatHeaderSeparator()
		{
			
			super();			
			
		}
	
	
		/**
		 *  @private
		 */
		override public function setActualSize(newWidth:Number,
											   newHeight:Number):void
		{
			var g:Graphics = graphics;
			
			g.clear();
			
			// Clear rect for hit area
			g.beginFill(0, 0);
			g.drawRect(-2, 0, newWidth + 2, newHeight);
			g.endFill();
			
			g.lineStyle(1, 0x000000, 1);
			g.moveTo(2, 0);
			g.lineTo(2, newHeight);
			
			
		}
		
		
		
	}
}