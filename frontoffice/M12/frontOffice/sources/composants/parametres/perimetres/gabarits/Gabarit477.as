package composants.parametres.perimetres.gabarits
{
	import composants.util.ConsoviewFormatter;
	
	public class Gabarit477 extends AbstractGabarit
	{
		public function Gabarit477()
		{
			super();
		}
		
		override public function getGabarit(infoObject: Object):String{
			var listeLignes : String = printListeSousTete(infoObject.LIGNES);
			return	"<p>Société : <b>"+infoObject.raison_sociale+"</b></p>"					
					+"<br/>"
					+"<br/>"
					+"<p>Bonjour,</p>"	
					+"<br/>"				
					+"<p>Les lignes ci-dessous font l'objet d'un transfert de compte</p>"
					+"<br/>"
					+"Origine des lignes <br/>"
					+ infoObject.INFOS_DEST[2][0].TYPE_NIVEAU + " : <b>" +infoObject.INFOS_DEST[2][0].LIBELLE_GROUPE_CLIENT  + "</b>. Gestionnaire : <b>" + infoObject.gestionnaire + "</b>. <br/>"
					+"Destination <br/>" 
					+ infoObject.INFOS_DEST[1][0].TYPE_NIVEAU + " : <b><font color='#FF1200'>Ref BV :230?</font>" +infoObject.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT  + "</b>. Gestionnaire : <b>" + infoObject.gestionnaireCible + "</b>. <br/>"
					+"<br/>"
					+ listeLignes
					+"<br/>_______________________________________________________________________________________________"
					+"<br/><font color='#FF1200'><b>Message à "+ infoObject.gestionnaireCible +".</b></font>"
					+"<br/>"
					+"<br/>"
					+"<p>Merci de prendre bonne note de ce transfert. Les lignes ont été affectées dans <b><i>CONSOVIEW</i></b> sur : <b> "+ infoObject.INFOS_DEST[1][0].TYPE_NIVEAU + " " +infoObject.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT + " </b> par <b>" + infoObject.gestionnaire + "</b></p>"
					+"<p>En cas d’erreur merci de prendre contact avec <b> "+ infoObject.gestionnaire + "</b> "+ infoObject.usermail + ".</p>"
					+"<br/>_______________________________________________________________________________________________"
					+"<br/><font color='#FF1200'><b>Message à l'operateur</b></font>"
					+"<br/>"
					+"<br/>"
					+"<p>1.    Merci de procéder à la correction des la Référence <b>"+infoObject.raison_sociale+"</b> de ces lignes (si renseigné).</p>"	
					+"<p>2.    Merci de procéder au changement de compte (si renseigné).</p>"	
					+"<br/>"
					+"<br/>"
					+"<p>Cordialement,</p>"
					+"<br/>"
					+"<b>"+infoObject.gestionnaire+ ".</b>"
					+"<br>"
					+"<b>"+infoObject.raison_sociale+"</b>";
		}
		
		override protected function printListeSousTete(liste : Array):String{
			var output : String = "<br/>";
			if (liste != null){
				for (var i:Number=0; i<liste.length;i++){
					var titulaire: String = "";
					if (liste[i].COMMENTAIRES != null && String(liste[i].COMMENTAIRES).length > 0) titulaire = " (" + liste[i].COMMENTAIRES + ")";
					output = output + liste[i].LIBELLE_TYPE_LIGNE + " : <b>"+ ConsoviewFormatter.formatPhoneNumber(liste[i].SOUS_TETE) + titulaire  + "</b>\t\t\t\tcompte actuel : ____________,\t\t" + "compte cible : ____________ <br/>";
				}
			}
			return output;
		}
		
	}
}