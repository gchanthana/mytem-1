package composants.parametres.perimetres.gabarits
{
	public class GabaritAffectations extends AbstractGabarit
	{
		public function GabaritAffectations()
		{
			//TODO: implement function
			super();
		}
		
		override public function getGabarit():String{
			return _gabarit;	
		}
		
		
		private var _gabarit : String  = 
				"<html xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:st1='urn:schemas-microsoft-com:office:smarttags' xmlns='http://www.w3.org/TR/REC-html40'>
<head>
<meta http-equiv=Content-Type content='text/html; charset=windows-1256'>
<meta name=Generator content='Microsoft Word 11 (filtered medium)'>
<o:SmartTagType namespaceuri='urn:schemas-microsoft-com:office:smarttags'
 name='PersonName'/>
<!--[if !mso]>
<style>
st1\:*{behavior:url(#default#ieooui) }
</style>
<![endif]-->
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:'Times New Roman';}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:Arial;
	color:windowtext;}
@page Section1
	{size:595.3pt 841.9pt;
	margin:70.85pt 70.85pt 70.85pt 70.85pt;}
div.Section1
	{page:Section1;}
 /* List Definitions */
 @list l0
	{mso-list-id:1655795624;
	mso-list-type:hybrid;
	mso-list-template-ids:2099922310 67895311 67895321 67895323 67895311 67895321 67895323 67895311 67895321 67895323;}
@list l0:level1
	{mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
<!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext='edit' spidmax='1026' />
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext='edit'>
  <o:idmap v:ext='edit' data='1' />
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=FR link=blue vlink=purple>

<div class=Section1>

<p class=MsoNormal><b><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial;font-weight:bold'>Objet</span></font></b><font size=2
face=Arial><span style='font-size:10.0pt;font-family:Arial'>&nbsp;: Transfert
de compte &amp; modification de référence <font color=red><span
style='color:red'>BUREAU VERITAS</span></font><o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'>Bonjour,<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'>Société&nbsp;: <font color=red><span style='color:red'>BUREAU
VERITAS</span></font><o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'>La ligne&nbsp;: <font color=red><span style='color:red'>0603101010</span></font>
(<font color=red><span style='color:red'>JEAN POLOCHON</span></font>) fait l’objet
d’un transfert de compte.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=377
 style='width:283.0pt;margin-left:-.75pt;border-collapse:collapse'>
 <tr height=31 style='height:23.25pt'>
  <td width=140 nowrap height=31 style='width:105.0pt;padding:0cm 3.5pt 0cm 3.5pt;
  height:23.25pt'>
  <p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
  font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>
  </td>
  <td width=119 nowrap height=31 style='width:89.0pt;padding:0cm 3.5pt 0cm 3.5pt;
  height:23.25pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><font size=2
  face=Arial><span style='font-size:10.0pt;font-family:Arial;font-weight:bold'>ACTUEL<o:p></o:p></span></font></b></p>
  </td>
  <td width=119 nowrap height=31 style='width:89.0pt;padding:0cm 3.5pt 0cm 3.5pt;
  height:23.25pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><font size=2
  face=Arial><span style='font-size:10.0pt;font-family:Arial;font-weight:bold'>CIBLE<o:p></o:p></span></font></b></p>
  </td>
 </tr>
 <tr height=31 style='height:23.25pt'>
  <td width=140 nowrap height=31 style='width:105.0pt;border:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:23.25pt'>
  <p class=MsoNormal><b><font size=2 face=Arial><span style='font-size:10.0pt;
  font-family:Arial;font-weight:bold'>Gestionnaire<o:p></o:p></span></font></b></p>
  </td>
  <td width=119 nowrap height=31 style='width:89.0pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 3.5pt 0cm 3.5pt;height:23.25pt'>
  <p class=MsoNormal align=center style='text-align:center'><font size=2
  color=red face=Arial><span style='font-size:10.0pt;font-family:Arial;
  color:red'>Marc DUBOIS<o:p></o:p></span></font></p>
  </td>
  <td width=119 nowrap height=31 style='width:89.0pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 3.5pt 0cm 3.5pt;height:23.25pt'>
  <p class=MsoNormal align=center style='text-align:center'><font size=2
  color=red face=Arial><span style='font-size:10.0pt;font-family:Arial;
  color:red'>Janes DUPONT<o:p></o:p></span></font></p>
  </td>
 </tr>
 <tr height=31 style='height:23.25pt'>
  <td width=140 nowrap height=31 style='width:105.0pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 3.5pt 0cm 3.5pt;height:23.25pt'>
  <p class=MsoNormal><b><font size=2 face=Arial><span style='font-size:10.0pt;
  font-family:Arial;font-weight:bold'>Ligne<o:p></o:p></span></font></b></p>
  </td>
  <td width=119 nowrap height=31 style='width:89.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:23.25pt' x:str='0603061010'>
  <p class=MsoNormal align=center style='text-align:center'><font size=2
  color=red face=Arial><span style='font-size:10.0pt;font-family:Arial;
  color:red'>0603061010<o:p></o:p></span></font></p>
  </td>
  <td width=119 nowrap height=31 style='width:89.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:23.25pt' x:str='0603061010'>
  <p class=MsoNormal align=center style='text-align:center'><font size=2
  color=red face=Arial><span style='font-size:10.0pt;font-family:Arial;
  color:red'>0603061010<o:p></o:p></span></font></p>
  </td>
 </tr>
 <tr height=31 style='height:23.25pt'>
  <td width=140 nowrap height=31 style='width:105.0pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 3.5pt 0cm 3.5pt;height:23.25pt'>
  <p class=MsoNormal><b><font size=2 face=Arial><span style='font-size:10.0pt;
  font-family:Arial;font-weight:bold'>Compte opérateur<o:p></o:p></span></font></b></p>
  </td>
  <td width=119 nowrap height=31 bgcolor='#FFCC99' style='width:89.0pt;
  border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;background:#FFCC99;padding:0cm 3.5pt 0cm 3.5pt;
  height:23.25pt'>
  <p class=MsoNormal align=center style='text-align:center'><font size=2
  face=Arial><span style='font-size:10.0pt;font-family:Arial'>&nbsp;<o:p></o:p></span></font></p>
  </td>
  <td width=119 nowrap height=31 bgcolor='#FFCC99' style='width:89.0pt;
  border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;
  border-right:solid windowtext 1.0pt;background:#FFCC99;padding:0cm 3.5pt 0cm 3.5pt;
  height:23.25pt'>
  <p class=MsoNormal align=center style='text-align:center'><font size=2
  face=Arial><span style='font-size:10.0pt;font-family:Arial'>&nbsp;<o:p></o:p></span></font></p>
  </td>
 </tr>
 <tr height=31 style='height:23.25pt'>
  <td width=140 nowrap height=31 style='width:105.0pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 3.5pt 0cm 3.5pt;height:23.25pt'>
  <p class=MsoNormal><b><font size=2 color=red face=Arial><span
  style='font-size:10.0pt;font-family:Arial;color:red;font-weight:bold'>&quot;TYPE
  DE NŒUD&quot;<o:p></o:p></span></font></b></p>
  </td>
  <td width=119 nowrap height=31 style='width:89.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:23.25pt'>
  <p class=MsoNormal align=center style='text-align:center'><font size=2
  color=red face=Arial><span style='font-size:10.0pt;font-family:Arial;
  color:red'>DFR465<o:p></o:p></span></font></p>
  </td>
  <td width=119 nowrap height=31 style='width:89.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:23.25pt'>
  <p class=MsoNormal align=center style='text-align:center'><font size=2
  color=red face=Arial><span style='font-size:10.0pt;font-family:Arial;
  color:red'>DFR985<o:p></o:p></span></font></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<div style='mso-element:para-border-div;border:none;border-top:solid windowtext 1.0pt;
padding:1.0pt 0cm 0cm 0cm'>

<p class=MsoNormal style='border:none;padding:0cm'><b><font size=2 face=Arial><span
style='font-size:10.0pt;font-family:Arial;font-weight:bold'>Message à <font
color=red><span style='color:red'>Janes DUPONT </span></font>&nbsp;:<o:p></o:p></span></font></b></p>

</div>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'>Merci de prendre bonne note de ce transfert. La ligne a été
affectée dans ConsoView sur&nbsp;: <b><font color=red><span style='color:red;
font-weight:bold'>&quot;TYPE DE NŒUD&quot; - </span></font></b><font color=red><span
style='color:red'>DFR985</span></font> par <font color=red><span
style='color:red'>Marc DUBOIS</span></font><o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'>En cas d’erreur merci de prendre contact avec <font
color=red><span style='color:red'>Marc DUBOIS – <a
href='mailto:marc.dubois@bv.fr'>marc.dubois@bv.fr</a> </span></font><o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<div style='mso-element:para-border-div;border:none;border-top:solid windowtext 1.0pt;
padding:1.0pt 0cm 0cm 0cm'>

<p class=MsoNormal style='border:none;padding:0cm'><b><font size=2 face=Arial><span
style='font-size:10.0pt;font-family:Arial;font-weight:bold'>Message à l’opérateur</span></font></b><font
size=2 face=Arial><span style='font-size:10.0pt;font-family:Arial'>&nbsp;:<o:p></o:p></span></font></p>

</div>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<ol style='margin-top:0cm' start=1 type=1>
 <li class=MsoNormal style='color:red;mso-list:l0 level1 lfo1'><font size=2
     color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;
     color:windowtext'>Merci de procéder à la correction de <st1:PersonName
     ProductID='la Référence BUREAU' w:st='on'><st1:PersonName
      ProductID='la Référence' w:st='on'>la Référence</st1:PersonName> <font
      color=red><span style='color:red'>BUREAU</span></font></st1:PersonName><font
     color=red><span style='color:red'> VERITAS </span></font>de cette ligne (si
     renseigné dans le tableau ci-dessus).</span></font><font size=2
     face=Arial><span style='font-size:10.0pt;font-family:Arial'><o:p></o:p></span></font></li>
</ol>

<p class=MsoNormal><font size=2 color=red face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:red'><o:p>&nbsp;</o:p></span></font></p>

<ol style='margin-top:0cm' start=2 type=1>
 <li class=MsoNormal style='mso-list:l0 level1 lfo1'><font size=2 face=Arial><span
     style='font-size:10.0pt;font-family:Arial'>Merci de procéder au changement
     de compte (si renseigné dans le tableau ci-dessus).<o:p></o:p></span></font></li>
</ol>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span style='font-size:10.0pt;
font-family:Arial'>Cordialement,<o:p></o:p></span></font></p>

<p class=MsoNormal><b><font size=2 color=gray face=Arial><span
style='font-size:10.0pt;font-family:Arial;color:gray;font-weight:bold'><o:p>&nbsp;</o:p></span></font></b></p>

<p class=MsoNormal><font size=2 color=red face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:red'>Marc DUBOIS</span></font><font size=2
color=gray face=Arial><span style='font-size:10.0pt;font-family:Arial;
color:gray'> <o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=red face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:red'>BUREAU VERITAS</span></font><font size=2
face=Arial><span lang=EN-GB style='font-size:10.0pt;font-family:Arial'><o:p></o:p></span></font></p>

</div>

</body>

</html>
"
		
	}
}