package utils
{
	import utils.selector.chartselector.ChartSelectorIHM;
	import utils.selector.monoperiodeselector.MonoPeriodeSelectorIHM;
	import utils.selector.noSelector.NoSelector;
	import utils.selector.poolselector.PoolSelectorIHM;
	import utils.selector.segmentselector.SegmentSelectorIHM;
	import utils.selector.themeselector.ThemeSelectorIHM;
	import utils.selector.uniteselector.UniteSelectorIHM;

	public class SimpleFactoryAccueil
	{
		public function SimpleFactoryAccueil()
		{
		}

		private var returnSelector:SelectorAccueilAbstract

		public function createComposants(selector:Object):SelectorAccueilAbstract
		{
			switch (selector.typeSelecteur)
			{

				case "MonoPeriodeSelector":
					returnSelector=new MonoPeriodeSelectorIHM();
					(returnSelector as MonoPeriodeSelectorIHM).init(selector);
					break;

				case "SegmentSelector":
					returnSelector=new SegmentSelectorIHM();
					(returnSelector as SegmentSelectorIHM).init(selector);
					break;

				case "ChartSelector":
					returnSelector=new ChartSelectorIHM();
					(returnSelector as ChartSelectorIHM).init(selector);
					break;
				
				case "UniteSelector":
					returnSelector=new UniteSelectorIHM();
					(returnSelector as UniteSelectorIHM).init(selector);
					break;
				
				case "ThemeRoamingSelector":
					returnSelector=new ThemeSelectorIHM();
					(returnSelector as ThemeSelectorIHM).init(selector);
					break;
				
				case "PoolSelector":
					returnSelector=new PoolSelectorIHM();
					(returnSelector as PoolSelectorIHM).init(selector);
					break;

				default:
				case "NoSelector":
					returnSelector=new NoSelector();
					(returnSelector as NoSelector).init(selector);
					break;
			}
			return returnSelector;
		}
	}
}
