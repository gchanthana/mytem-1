package utils
{
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;


	public class FormatterDate
	{
		public function FormatterDate()
		{
		}

		/**
		 * calculer la date en fonction de number passé en paramètre et la date
		 */
		public function getDateByDefaultValue(datepart:String="", number:Number=0, date:Date=null):Date
		{
			if (date == null)
			{
				date=new Date(); // today date 
			}
			var returnDate:Date=new Date(date);
			switch (datepart.toLowerCase())
			{
				case 'fullyear':
				case 'month':
				case 'date':
				case 'hours':
				case 'minutes':
				case 'seconds':
				case 'milliseconds':
					returnDate[datepart]+=number;
					break;
				default:
					break;
			}
			return returnDate;
		}

		/**
		 * permet de récupérer le libelle dans la langue de connexion
		 * @param pDate
		 * @return la date au format de la langue de connexion
		 */
		public function dateFormat(pDate:Date):String
		{
			var lDf:DateFormatter=new DateFormatter();

			lDf.formatString="M"; // le n° du mois dans l'année
			var mois:String=lDf.format(pDate);
			var moisConverti:String=getMois(mois);

			return moisConverti;
		}

		public function dateFormatWithYear(pDate:Date):String
		{
			var lDf:DateFormatter=new DateFormatter();

			lDf.formatString="M"; // le n° du mois dans l'année
			var mois:String=lDf.format(pDate);
			var moisEnFr:String=getDate(mois);

			lDf.formatString="YYYY";
			var annee:String=lDf.format(pDate);
			return moisEnFr + " " + annee;
		}

		/**
		 * permet de récupérer le dernier jour du mois
		 * @param year
		 * @param month
		 * @return
		 */
		private function getDayCount(year:int, month:int):int
		{
			var d:Date=new Date(year, month, 0);
			return d.getDate();
		}

		/**
		 * récupérer le libelle du mois dans la langue de connexion
		 * @param number
		 * @return
		 */
		private function getMois(number:String):String
		{
			var months:Array=new Array(12);
			months[1] = ResourceManager.getInstance().getString('M54','JAN');
			months[2] = ResourceManager.getInstance().getString('M54','FEV');
			months[3] = ResourceManager.getInstance().getString('M54','MARS');
			months[4] = ResourceManager.getInstance().getString('M54','AVR');
			months[5] = ResourceManager.getInstance().getString('M54','MAI');
			months[6] = ResourceManager.getInstance().getString('M54','JUIN');
			months[7] = ResourceManager.getInstance().getString('M54','JUIL');
			months[8] = ResourceManager.getInstance().getString('M54','AOUT');
			months[9] = ResourceManager.getInstance().getString('M54','SEPT');
			months[10] = ResourceManager.getInstance().getString('M54','OCT');
			months[11] = ResourceManager.getInstance().getString('M54','NOV');
			months[12] = ResourceManager.getInstance().getString('M54','DEC');

			return months[number];
		}
		
		private function getDate(number:String):String
		{
			var months:Array=new Array(12);
			months[1] = ResourceManager.getInstance().getString('M54','Janvier');
			months[2] = ResourceManager.getInstance().getString('M54','F_vrier');
			months[3] = ResourceManager.getInstance().getString('M54','Mars');
			months[4] = ResourceManager.getInstance().getString('M54','Avril');
			months[5] = ResourceManager.getInstance().getString('M54','Mai');
			months[6] = ResourceManager.getInstance().getString('M54','Juin');
			months[7] = ResourceManager.getInstance().getString('M54','Juillet');
			months[8] = ResourceManager.getInstance().getString('M54','Ao_t');
			months[9] = ResourceManager.getInstance().getString('M54','Septembre');
			months[10] = ResourceManager.getInstance().getString('M54','Octobre');
			months[11] = ResourceManager.getInstance().getString('M54','Novembre');
			months[12] = ResourceManager.getInstance().getString('M54','D_cembre');
			
			return months[number];
		}
	}
}
