package utils.kpi
{
	import ihm.kpi.commandesencours.CommandesEnCoursIHM;
	import ihm.kpi.coutmoyenparligne.CoutMoyenParLigneIHM;
	import ihm.kpi.coutstelecom12moisglissants.CoutsTelecom12MoisGlissantsIHM;
	import ihm.kpi.couttelecomanneecalendaire.CoutTelecomAnneeCalendaireIHM;
	import ihm.kpi.couttelecomsmensuelabosconsos.CoutTelecomsMensuelAbosConsosIHM;
	import ihm.kpi.empty.KpiEmptyIHM;
	import ihm.kpi.encoursdescommandes.EncoursDesCommandesIHM;
	import ihm.kpi.kpiabstract.KpiAbstract;
	import ihm.kpi.lignessansconsosfacturees.LignesSansConsosFactureesIHM;
	import ihm.kpi.nombredelignesfacturee.NombreDeLignesFactureeIHM;
	import ihm.kpi.roamingmobiledata.RoamingMobileDataIHM;
	import ihm.kpi.roamingmobilevoixinout.RoamingMobileVoixInOutIHM;
	import ihm.kpi.totaldescommandes.TotalDesCommandesIHM;

	public class KpiFactory
	{
		public function KpiFactory()
		{
		}

		private var returnKpi:KpiAbstract;

		public function createKpi(codeKpi:String):KpiAbstract
		{
			switch (codeKpi)
			{
				case "kpi_1": /** Coût moyen par ligne */
					returnKpi=new CoutMoyenParLigneIHM();
					break;

				case "kpi_2": /** Coûts télécoms mensuel (abos+consos)*/
					returnKpi=new CoutTelecomsMensuelAbosConsosIHM();
					break;

				case "kpi_3": /** Coûts Télécom sur 12 mois glissants */
					returnKpi=new CoutsTelecom12MoisGlissantsIHM();
					break;

				case "kpi_4": /** Roaming mobile data */
					returnKpi=new RoamingMobileDataIHM();
					break;

				case "kpi_5": /** Roaming mobile voix (in & out) */
					returnKpi=new RoamingMobileVoixInOutIHM();
					break;

				case "kpi_6": /** Nb de lignes */
					returnKpi=new NombreDeLignesFactureeIHM();
					break;

				case "kpi_7": /** Couts télécoms année calendaire */
					returnKpi=new CoutTelecomAnneeCalendaireIHM();
					break;

				case "kpi_8": /** Lignes sans consos facturées */
					returnKpi=new LignesSansConsosFactureesIHM();
					break;
				
				case "kpi_9": /** Commandes en cours */
					returnKpi=new CommandesEnCoursIHM();
					break;
				
				case "kpi_10": /** Encours des commandes ( cela donne le montant ) */
					returnKpi=new EncoursDesCommandesIHM();
					break;
				
				case "kpi_11": /** Total des commandes (montant | nombre) */
					returnKpi=new TotalDesCommandesIHM();
					break;
				
				default:
					returnKpi=new KpiEmptyIHM();
					break;
			}
			return returnKpi;
		}
	}
}
