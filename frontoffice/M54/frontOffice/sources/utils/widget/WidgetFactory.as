package utils.widget
{
	import ihm.widget.top10inventairedevicemobiles.Top10InventaireDeviceMobilesIHM;
	import ihm.widget.empty.WidgetEmptyIHM;
	import ihm.widget.etatdescommandes.EtatDesCommandesIHM;
	import ihm.widget.evolutioncoutoperateursanssegment.EvolutionCoutOperateurSansSegmentIHM;
	import ihm.widget.evolutioncoutsconsosthemes.EvolutionCoutsConsosThemesIHM;
	import ihm.widget.evolutioncoutsegment12mois.EvolutionCoutsSegment12MoisIHM;
	import ihm.widget.evolutiondescommandes.EvolutionDesCommandesIHM;
	import ihm.widget.inventairedeparc.InventaireDeParcIHM;
	import ihm.widget.repartitioncoutsgeographique.RepartitionCoutsGeographiqueIHM;
	import ihm.widget.repartitioncoutsoperateur.RepartitionCoutsOperateurIHM;
	import ihm.widget.roaminginternationalvoixdata.RoamingInternationalVoixDataIHM;
	import ihm.widget.widgetabstract.WidgetAbstract;

	public class WidgetFactory
	{
		public function WidgetFactory()
		{
		}

		private var returnWidget:WidgetAbstract

		public function createWidget(codeWidget:String):WidgetAbstract
		{
			switch (codeWidget)
			{
				case "widget_1": /** Roaming international voix & data */
					returnWidget=new RoamingInternationalVoixDataIHM();
					break;

				case "widget_2": /** Evolution des coûts par segment sur 12 mois */
					returnWidget=new EvolutionCoutsSegment12MoisIHM();
					break;

				case "widget_3": /** Répartition des coûts par opérateur */
					returnWidget=new RepartitionCoutsOperateurIHM();
					break;

				case "widget_4": /** Evolution des coûts de consos par thèmes */
					returnWidget=new EvolutionCoutsConsosThemesIHM();
					break;

				case "widget_5": /** Evolution des coûts par opérateur */
					returnWidget=new EvolutionCoutOperateurSansSegmentIHM();
					break;

				case "widget_7": /**  Répartition des coûts géographique (sites France) */
					returnWidget=new RepartitionCoutsGeographiqueIHM();
					break;
				
				case "widget_9": /** Evolution des commandes */
					returnWidget=new EvolutionDesCommandesIHM();
					break;
				
				case "widget_10": /** Etat des commandes */
					returnWidget=new EtatDesCommandesIHM();
					break;

				case "widget_11": /** Inventaire de parc */
					returnWidget=new InventaireDeParcIHM();
					break;
				
				case "widget_12": /** Top 10 inventaire des devices moblies*/
					returnWidget=new Top10InventaireDeviceMobilesIHM();
					break;
				
				default:
					returnWidget=new WidgetEmptyIHM();
					break;
			}
			return returnWidget;
		}
	}
}
