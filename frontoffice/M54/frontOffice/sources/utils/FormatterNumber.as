package utils
{
	import mx.formatters.NumberBaseRoundType;
	import mx.formatters.NumberFormatter;

	public class FormatterNumber
	{
		public function FormatterNumber()
		{
		}


		public function formatNumber(num:Number, precision:int):String
		{
			var nf:NumberFormatter=new NumberFormatter();
			var v_abs:Number=Math.round(num);
			if (v_abs == num)
			{
				nf.precision=0;
			}
			else
			{
				nf.precision=precision;
			}
			nf.rounding=NumberBaseRoundType.NONE;
			nf.decimalSeparatorTo=",";
			nf.thousandsSeparatorTo=" ";
			nf.useThousandsSeparator=true;
			nf.useNegativeSign=true;
			return nf.format(num);
		}
	}
}
