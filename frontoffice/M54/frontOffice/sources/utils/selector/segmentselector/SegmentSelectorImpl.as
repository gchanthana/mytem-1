package utils.selector.segmentselector
{
	import event.selector.SegmentEvent;

	import flash.events.MouseEvent;

	import mx.collections.ArrayCollection;
	import mx.controls.List;
	import mx.events.FlexEvent;

	import utils.CreateShadow;
	import utils.SelectorAccueilAbstract;

	import vo.selector.Parametre;
	import vo.selector.ValeurSelectorVo;

	public class SegmentSelectorImpl extends SelectorAccueilAbstract
	{
		[Bindable] public var segment:ArrayCollection;
		[Bindable] public var heigthSegment:Number=90;
		[Bindable] public var selected:Number=-1;
		
		public var listSegment:List;
		private var _shadow:CreateShadow;

		public function SegmentSelectorImpl(selector:Object=null)
		{
			super(selector);
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}

		private function onCreationComplete(e:FlexEvent):void
		{
			_shadow=new CreateShadow(0xC3C8CC, 0.5, 2, 2, false, 2.8, 10);
			_shadow.addShadow(this); /** ajouter n shadow */

			if (segment != null)
			{
				switch (segment.length)
				{
					case 1:
						heigthSegment=23;
						break;
					case 2:
						heigthSegment=46;
						break;
					case 3:
						heigthSegment=68;
						break;
				}
				
				/** selectionner la valeur par defaut */
				for (var i:int=0; i < segment.length; i++)
				{
					if ((segment[i] as ValeurSelectorVo).id.toString() == defaultValue)
					{
						selected=(segment[i] as ValeurSelectorVo).indexInList;
					}
				}
			}
			if (listSegment != null)
			{
				if (isKpi)
				{
					listSegment.styleName="M54MenuSegmentKpi";
				}
				else
				{
					listSegment.styleName="M54MenuSegmentWidget";
				}
			}
		}

		/**
		 * permet de dispatcher un event pour recupérer la valeur de kpi
		 * @param e
		 */
		protected function listSegment_clickHandler(e:MouseEvent):void
		{
			dispatchEvent(new SegmentEvent(SegmentEvent.VALIDER_VALUE_SEGMENT_EVENT)); /** capturé dans kpiAbstract*/
		}

		/**
		 * récupérer la valeur selectionné par l'utilisateur
		 * @return
		 */
		override public function getSelectedValue():Array
		{
			var tabValeur:Array=[];
			var param:Parametre=new Parametre();

			if (listSegment != null)
			{
				if (listSegment.selectedItem != null)
				{
					selected=(listSegment.selectedItem as ValeurSelectorVo).indexInList;
					param.value=[(listSegment.selectedItem as ValeurSelectorVo).id];
					param.cle=arrayCleLibelle[0].NOM_CLE;
					param.libelle=arrayCleLibelle[0].LIBELLE_CLE;
					tabValeur[0]=param;
				}
			}
			else
			{
				param.value=[defaultValue];
				param.cle=arrayCleLibelle[0].NOM_CLE;
				param.libelle=arrayCleLibelle[0].LIBELLE_CLE;
				tabValeur[0]=param;
			}
			return tabValeur;
		}
	}
}
