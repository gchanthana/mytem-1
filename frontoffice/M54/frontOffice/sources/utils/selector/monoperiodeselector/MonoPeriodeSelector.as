package utils.selector.monoperiodeselector
{	
	import mx.collections.ArrayCollection;
	
	import utils.selector.monoperiodeselector.AMonth;
	import utils.selector.monoperiodeselector.PeriodeSelector_IHM;
	
	import vo.selector.ValeurSelectorVo;
	
	public class MonoPeriodeSelector extends PeriodeSelector_IHM
	{
		private var aMonth : AMonth;	
		private var _firstDay : Boolean = true;
		
		[Bindable]private var moisDebut : String = "";	
		[Bindable]private var moisFin : String = "";
		[Bindable]private var idPeriodeMois :Number;
		[Bindable]private var libellePeriodeMois :String;
		[Bindable]private var monthData:Array; //tableau contenant les mois dispo.
		
		public function MonoPeriodeSelector()
		{				
			super();
		}
		/**
		 * Fonction qui retourne un tableau avec les dates de la periode [mois actuelle -13,mois actuelle-1]
		 * 
		 * @return  monthData : Array un tableau contenant les deates incluses dans la periode [mois actuelle -13,mois actuelle-1];
		 * 
		 * */
		public function getTabPeriode():Array
		{
			return monthData;
		}
		
		// formate les tooltips du slider	
		private function getSliderLabelFirstDay(value:Number):String
		{
			if(value!=0)
	 			return (monthData[value-1] as AMonth).getDateDebut();
			else
				return (monthData[value] as AMonth).getDateDebut();	
		}

		/*
		 permet de initialiser le selector mono periode
		*/
		public function setValuePeriode(datesCollection:ArrayCollection):void
		{
			var tabPeriode : Array = new Array();
			var idPeriodeMois :Number;
			var firstValue:String;
			var maDate:String;
			var libellePeriode:String;
			var firstDate:Date;
			var array:Array;
			var theMonth : AMonth;
			
			// creer un array des dates sous forme d'objet de type AMonth
			for(var i : int =0; i<datesCollection.length;i++)
			{
				maDate =(datesCollection.getItemAt(i) as ValeurSelectorVo).dispo_mois;			
				idPeriodeMois=(datesCollection.getItemAt(i) as ValeurSelectorVo).idPeriodeMois;
				libellePeriode=(datesCollection.getItemAt(i) as ValeurSelectorVo).libelle_periode;
				array = maDate.split('/');
				firstDate = new Date(array[0],array[1]);			
				theMonth = new AMonth(firstDate);
				
				theMonth.idDateSelected = idPeriodeMois;// associer un id à la date créé 
				theMonth.libellePeriodeSelected=libellePeriode;
				
				tabPeriode[i] = theMonth;		
			}			
			monthData = tabPeriode; 
			slider.maximum = monthData.length-1;	// le nombre max autorise
			slider.value = monthData.length - 1; // positionner le curseur
			
			slider.thumbCount=1;// pour un seul curseur
			slider.setStyle("showTrackHighlight",false);
			
			if (_firstDay)
			{
				slider.dataTipFormatFunction = getSliderLabelFirstDay; 
			}

			if(slider.getThumbAt(0).name!=null)
			{
				slider.getThumbAt(0).name = "leftthumb";
				slider.getThumbAt(0).setStyle("fillColors",["#909587","#909587"]);		
			}
			   	  	
		}
		
		public function getSelectedIdDate():Object
		{			
			var objSelectedDates:Object ={};
			
			if (_firstDay)			 
			{			
				idPeriodeMois = AMonth(getTabPeriode()[slider.values[0]]).idDateSelected;
			}
			objSelectedDates.Idmois = idPeriodeMois;
			return objSelectedDates;
		}
		
		public function getSelectedLibellePeriode():Object
		{			
			var objSelectedLibellePeriode:Object ={};
			
			if (_firstDay)			 
			{			
				libellePeriodeMois = AMonth(getTabPeriode()[slider.values[0]]).libellePeriodeSelected;
			}
			objSelectedLibellePeriode.libellePeriode = libellePeriodeMois;
			return objSelectedLibellePeriode;
		}
	}
}


