package utils.selector.poolselector
{
	import event.selector.PoolEvent;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.List;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import utils.CreateShadow;
	import utils.SelectorAccueilAbstract;
	
	import vo.selector.Parametre;
	import vo.selector.ValeurSelectorVo;
	
	public class PoolSelectorImpl extends SelectorAccueilAbstract
	{
		[Bindable] public var pools:ArrayCollection;
		[Bindable] public var selected:Number=-1;
		
		public var listPool:List;
		private var _shadow:CreateShadow;
		
		public function PoolSelectorImpl(selector:Object=null)
		{
			super(selector);
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}
		
		private function onCreationComplete(e:FlexEvent):void
		{
			_shadow=new CreateShadow(0xC3C8CC, 0.5, 2, 2, false, 2.8, 10);
			_shadow.addShadow(this); /** ajouter n shadow */
			
			listPool.setFocus();
			
			if (pools != null)
			{
				if(pools.length <5)
					listPool.height=23*(pools.length);
				else
					listPool.height=130;
				
				/** selectionner la valeur par defaut */
				for (var i:int=0; i < pools.length; i++)
				{
					if ((pools[i] as ValeurSelectorVo).id.toString() == defaultValue)
					{
						selected=(pools[i] as ValeurSelectorVo).indexInList;
					}
				}
			}
			if (listPool != null)
			{
				if (isKpi)
				{
					listPool.styleName="M54MenuSegmentKpi";
				}
				else
				{
					listPool.styleName="M54MenuSegmentWidget";
				}
			}
		}
		
		/**
		 * permet de dispatcher un event pour recupérer la valeur de kpi
		 */
		protected function listPool_clickHandler(e:MouseEvent):void
		{
			dispatchEvent(new PoolEvent(PoolEvent.VALIDER_VALUE_POOL_EVENT)); /** capturé dans CommandesEncoursImpl*/
		}
		
		/**
		 * récupérer la valeur selectionné par l'utilisateur
		 * @return
		 */
		override public function getSelectedValue():Array
		{
			var tabValeur:Array=[];
			var param:Parametre=new Parametre();
			
			if (listPool != null)
			{
				if (listPool.selectedItem != null)
				{
					selected=(listPool.selectedItem as ValeurSelectorVo).indexInList;
					param.value=[(listPool.selectedItem as ValeurSelectorVo).id];
					param.cle=arrayCleLibelle[0].NOM_CLE;
					param.libelle=arrayCleLibelle[0].LIBELLE_CLE;
					tabValeur[0]=param;
				}
			}
			else
			{
				param.value=[defaultValue];
				param.cle=arrayCleLibelle[0].NOM_CLE;
				param.libelle=arrayCleLibelle[0].LIBELLE_CLE;
				tabValeur[0]=param;
			}
			return tabValeur;
		}
		
		protected function labelPoolFunc(item:Object):String
		{
			// ici item est de type ValeurSelectorVo
			var labelPool:String = item.libelle + ' : ' + getLabelTypePool(item.typePool);
			// cas ddu choix Tous les pools
			if(item.id == -1)
				labelPool = item.libelle;
			
			return 	labelPool;		
		}
		
		private function getLabelTypePool(idTypePool:Number):String
		{
			return (idTypePool == 1)?ResourceManager.getInstance().getString('M54','Pool_revendeur_'):ResourceManager.getInstance().getString('M54','Pool_client_');
		}
	}
}