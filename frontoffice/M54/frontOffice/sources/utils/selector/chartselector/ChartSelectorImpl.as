package utils.selector.chartselector
{
	import event.selector.ChartsEvent;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.List;
	import mx.events.FlexEvent;
	
	import utils.CreateShadow;
	import utils.SelectorAccueilAbstract;
	
	import vo.selector.Parametre;
	import vo.selector.ValeurSelectorVo;

	public class ChartSelectorImpl extends SelectorAccueilAbstract
	{
		[Bindable]
		public var chartsWidget:ArrayCollection;

		private var _shadow:CreateShadow;

		public var listChart:List;
		
		[Bindable]
		public var heightListChart :Number=70;
		
		[Bindable]
		public var selected:Number=-1;
		
		public function ChartSelectorImpl(selector:Object=null)
		{
			super(selector);
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}

		private function onCreationComplete(e:FlexEvent):void
		{
			_shadow=new CreateShadow(0xC3C8CC, 0.5, 2, 2, false, 2.8, 10);
			_shadow.addShadow(this); // ajouter n shadow
			
			if(chartsWidget != null)
			{
				if (chartsWidget.length == 1)
				{
					heightListChart=24;
				}
				if (chartsWidget.length == 2)
				{
					heightListChart=46;
				}
				else if (chartsWidget.length == 3)
				{
					heightListChart=70;
				}
			}	
		}

		protected function listChart_clickHandler(e:MouseEvent):void
		{
			dispatchEvent(new ChartsEvent(ChartsEvent.VALIDER_VALUE_CHART_EVENT));
		}

		override public function getSelectedValue():Array
		{
			var tabValeur:Array=[];
			var param:Parametre=new Parametre();

			if (listChart != null)
			{
				if (listChart.selectedItem != null)
				{
					selected=(listChart.selectedItem as ValeurSelectorVo).indexInList;
					param.value=[(listChart.selectedItem as ValeurSelectorVo).id];
					param.cle=arrayCleLibelle[0].NOM_CLE;
					param.libelle=arrayCleLibelle[0].LIBELLE_CLE;
					tabValeur[0]=param;
				}
			}
			else
			{
				param.value=[defaultValue];
				param.cle=arrayCleLibelle[0].NOM_CLE;
				param.libelle=arrayCleLibelle[0].LIBELLE_CLE;
				tabValeur[0]=param;
			}
			return tabValeur;
		}

	}
}
