	package ihm.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	
	import composants.util.ConsoviewAlert;
	
	import event.kpi.KpiChangeEvent;
	
	import vo.kpi.KpiVo;

	public class AddKpiPopUpImpl extends AddKpiWidgetPopUpImpl
	{
		public var lbl_selected:Label;
		
		private var _dpAllKPI:ArrayCollection = new ArrayCollection();
		private var _dpCategorie:ArrayCollection = new ArrayCollection();
		
		public function AddKpiPopUpImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompletePopUp);
		}
		
		protected function onCreationCompletePopUp(event:FlexEvent):void
		{
			txtFilter.addEventListener(Event.CHANGE, filterDataProvider);
			// Trouver tous les catégories des KPI
			getCategories();
			
			// Trier les KPI par catégories
			setAllKpi();
		}
		
		private function getCategories():void
		{
			for (var i:int = 0; i < dataProviderList.source.length; i++) 
			{
				if(verifierCat(dataProviderList[i].nomCategory) == false)
					{
						var unkpi:KpiVo = new KpiVo();
							unkpi.codeKpi = 'kpi_00';
							unkpi.nom = dataProviderList[i].nomCategory;
							unkpi.nomCategory = dataProviderList[i].nomCategory;
							unkpi.activeForUser = false;
							unkpi.selectedItemInPopUp = false;
							
						dpCategorie.addItem(unkpi);
					}
			}
		}
		
		private function verifierCat(cat:String):Boolean
		{
			var existe:Boolean = false
			if(dpCategorie.length > 0)
			{
				for (var j:int = 0; j < dpCategorie.length; j++) 
				{
					if(cat == dpCategorie[j].nom)
					{
						existe = true;
						break;
					}
				}
			}
			return existe;
		}
		
		private function setAllKpi():void
		{
			for (var i:int = 0; i < dpCategorie.source.length; i++) 
			{
				dpAllKPI.addItem(dpCategorie[i] as KpiVo);
				
				for (var j:int = 0; j < dataProviderList.source.length; j++) 
				{
					if(dataProviderList[j].nomCategory == dpCategorie[i].nomCategory)
					{
						dpAllKPI.addItem(dataProviderList[j]);
					}
				}
			}
		}
		
		private function filterDataProvider(event:Event):void
		{
			if (dpAllKPI != null)
			{
				dpAllKPI.filterFunction=filterMyArrayCollection; // le nom de la fonction ;
				dpAllKPI.refresh();
			}
		}
		
		/**
		 * permet de filtrer la liste
		 * @param item : Objet (chaque ligne de data provider)
		 * @return Boolean
		 */
		protected function filterMyArrayCollection(item:Object):Boolean
		{
			var itemNom:String="";
			
			var searchString:String=StringUtil.trim(txtFilter.text.toLowerCase());
			
			if ((item.nom != null))
			{
				itemNom=(item.nom as String).toLowerCase();
			}
			
			if ((itemNom.indexOf(searchString) > -1) || ((item.codeKpi == 'kpi_00')))
			{
				return true;
			}
			return false;
		}
		
		public function onClickBtnValiderHandler(e:MouseEvent):void
		{
			if ((listeElement.selectedItem != null) && (listeElement.selectedItem.codeKpi != 'kpi_00'))
			{
					selectedElement=(listeElement.selectedItem as KpiVo);
					this.dispatchEvent(new KpiChangeEvent(KpiChangeEvent.ADD_KPI_EVENT));
			}
			else
			{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M54', 'Veuillez_choisir_un_KPI'), ResourceManager.getInstance().getString('M54', 'Alerte'), null);
			}
		}
		
		[Bindable]
		public function get dpAllKPI():ArrayCollection { return _dpAllKPI; }
		
		public function set dpAllKPI(value:ArrayCollection):void
		{
			if (_dpAllKPI == value)
				return;
			_dpAllKPI = value;
		}
		
		public function get dpCategorie():ArrayCollection { return _dpCategorie; }
		
		public function set dpCategorie(value:ArrayCollection):void
		{
			if (_dpCategorie == value)
				return;
			_dpCategorie = value;
		}
	
	}
}