package ihm.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	
	import composants.util.ConsoviewAlert;
	
	import event.widget.WidgetChangeEvent;
	
	import vo.widgets.WidgetVo;

	public class AddWidgetPopUpImpl extends AddKpiWidgetPopUpImpl
	{
		public var lbl_selected:Label;
		
		private var _dpAllWidget:ArrayCollection = new ArrayCollection();
		private var _dpCategorie:ArrayCollection = new ArrayCollection();
		
		public function AddWidgetPopUpImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompletePopUp);
		}
		
		protected function onCreationCompletePopUp(event:FlexEvent):void
		{
			txtFilter.addEventListener(Event.CHANGE, filterDataProvider);
			// Trouver les catégories des widgets
			getCategories();
			
			// Trier les widgets par catégories
			setAllWidget();
		}
		
		private function getCategories():void
		{
			for (var i:int = 0; i < dataProviderList.source.length; i++) 
			{
				if(verifierCat(dataProviderList[i].nomCategory) == false)
				{
					var unwidget:WidgetVo = new WidgetVo();
					unwidget.codeWidget = 'widget_00';
					unwidget.nom = dataProviderList[i].nomCategory;
					unwidget.nomCategory = dataProviderList[i].nomCategory;
					unwidget.activeForUser = false;
					unwidget.selectedItemInPopUp = false;
					
					dpCategorie.addItem(unwidget);
				}
			}
		}
		
		private function verifierCat(cat:String):Boolean
		{
			var existe:Boolean = false
			if(dpCategorie.length > 0)
			{
				for (var j:int = 0; j < dpCategorie.length; j++) 
				{
					if(cat == dpCategorie[j].nom)
					{
						existe = true;
						break;
					}
				}
			}
			return existe;
		}
		
		private function setAllWidget():void
		{
			for (var i:int = 0; i < dpCategorie.source.length; i++) 
			{
				dpAllWidget.addItem(dpCategorie[i]);
				
				for (var j:int = 0; j < dataProviderList.source.length; j++) 
				{
					if(dataProviderList[j].nomCategory == dpCategorie[i].nomCategory)
					{
						dpAllWidget.addItem(dataProviderList[j]);
					}
				}
			}
		}
		
		private function filterDataProvider(event:Event):void
		{
			if (dpAllWidget != null)
			{
				dpAllWidget.filterFunction=filterMyArrayCollection; // le nom de la fonction ;
				dpAllWidget.refresh();
			}
		}
		
		/**
		 * permet de filtrer la liste
		 * @param item : Objet (chaque ligne de data provider)
		 * @return Boolean
		 */
		protected function filterMyArrayCollection(item:Object):Boolean
		{
			var itemNom:String="";
			
			var searchString:String=StringUtil.trim(txtFilter.text.toLowerCase());
			
			if ((item.nom != null))
			{
				itemNom=(item.nom as String).toLowerCase();
			}
			
			if ((itemNom.indexOf(searchString) > -1) || ((item.codeWidget == 'widget_00')))
			{
				return true;
			}
			return false;
		}
		
		public function onClickBtnValiderHandler(e:MouseEvent):void
		{
			if ((listeElement.selectedItem != null) && (listeElement.selectedItem.codeWidget != 'widget_00'))
			{
				selectedElement=(listeElement.selectedItem as WidgetVo);
				this.dispatchEvent(new WidgetChangeEvent(WidgetChangeEvent.ADD_WIDGET_EVENT));
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M54', 'Veuillez_choisir_un_Widget'), ResourceManager.getInstance().getString('M54', 'Alerte'), null);
			}
		}
		
		public function get dpCategorie():ArrayCollection
		{
			return _dpCategorie;
		}
		
		public function set dpCategorie(value:ArrayCollection):void
		{
			_dpCategorie = value;
		}
		
		[Bindable]
		public function get dpAllWidget():ArrayCollection
		{
			return _dpAllWidget;
		}
		
		public function set dpAllWidget(value:ArrayCollection):void
		{
			_dpAllWidget = value;
		}
		
		/** ------------------------------------------ pour filtrer -----------------------------------*/
		
		
	}
}