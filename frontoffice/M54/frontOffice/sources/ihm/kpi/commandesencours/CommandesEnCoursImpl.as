package ihm.kpi.commandesencours
{
	import flash.events.MouseEvent;
	
	import mx.containers.HBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import event.selector.PoolEvent;
	
	import ihm.kpi.kpiabstract.KpiAbstract;
	
	import utils.infobull.ImgHelp;
	
	import vo.selector.ValeurSelectorVo;

	public class CommandesEnCoursImpl extends KpiAbstract
	{

		private var _poolHbox:HBox;
		[Bindable]protected var partsNomKpi:String = '--';
		
		private var helpText:String = ResourceManager.getInstance().getString('M54', 'Les_commandes_en_cours_montant__nombre_');
		
		public function CommandesEnCoursImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, initCreation);
			
		}
		
		protected function initCreation(event:FlexEvent):void
		{
			var ind:int = nomKpi.indexOf('(', 0);
			var part1:String = nomKpi.substring(0,ind-1);
			var part2:String = nomKpi.substring(ind, nomKpi.length);
			partsNomKpi = part1 + '\n' + part2;
		}		
		
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			choixPeriode.visible=false;
			lblPeriode.visible=false;
			
			_poolHbox=new HBox();
			_poolHbox.percentWidth=93;
			_poolHbox.setStyle('bottom', 5);
			_poolHbox.setStyle('right', 8);
			_poolHbox.setStyle('horizontalAlign', 'left');
			this.addChild(_poolHbox); /** this = canva*/
			
			lblPool=new Label();
			lblPool.width=170;
			commun.settingLabel(lblPool, "", "M54LabelPeriode", "labelPoolCommandeEnCours", _poolHbox);
			lblPool.addEventListener(MouseEvent.CLICK, onClickPoolIconHandler);
			
			choixPool=new Image();
			commun.settingImage(choixPool, ok, -1, -1, -1, -1, -1, -1, _poolHbox, true, true);
			choixPool.addEventListener(MouseEvent.CLICK, onClickPoolIconHandler);
			
			if (isEmptyKpi == 0) // kpi normal
			{
				helpIcon=new ImgHelp();
				commun.setHepICon(helpIcon, 10, -1, 10, -1, -1, -1, this, helpText, true, true);
				helpIcon.visible = true;
			}
		}
		
		/** --------------------------------------------------------- pool icon kpi -----------------------------------------*/
		/**
		 * permet d(afficher le selecteur et ajouter les listener et calculer la position de selecteur
		 */
		public function onClickPoolIconHandler(evt:MouseEvent):void
		{
			if (selectorPool != null)
			{
				commun.calculatePositionMenuPoolKpi(selectorPool, this,_poolHbox);
				selectorPool.addEventListener(MouseEvent.ROLL_OUT, rollOutMenuPool);
				selectorPool.addEventListener(PoolEvent.VALIDER_VALUE_POOL_EVENT, validerValuePoolHandler);
				
				if (selectorPool.pools != null)
				{
					if (selectorPool.pools.length >= 1)
						PopUpManager.addPopUp(selectorPool, this);
				}
			}
		}
		
		/**
		 * mettre a jour le libelle du pool et appeler la fonction  validerHandler pour mettre ajour la valeur de kpi
		 */
		private function validerValuePoolHandler(e:PoolEvent):void
		{
			if(selectorPool.listPool.selectedItem!=null)
			{
				lblPool.text=(selectorPool.listPool.selectedItem as ValeurSelectorVo).libelle;
				validerHandler();
				rollOutMenuPool(null);
			}
		}
		
		/**
		 *permet de masquer le menu de segments
		 */
		private function rollOutMenuPool(e:MouseEvent):void
		{
			closePopUp(selectorPool);
		}
	}
}