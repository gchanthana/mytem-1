package ihm.kpi.encoursdescommandes
{
	import event.selector.PoolEvent;
	
	import flash.events.MouseEvent;
	
	import ihm.kpi.kpiabstract.KpiAbstract;
	
	import mx.containers.HBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.managers.PopUpManager;
	
	import vo.selector.ValeurSelectorVo;

	public class EncoursDesCommandesImpl extends KpiAbstract
	{
		private var _poolHbox:HBox;
		
		public function EncoursDesCommandesImpl()
		{
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			choixPeriode.visible=false;
			lblPeriode.visible=false;
			
			_poolHbox=new HBox();
			_poolHbox.percentWidth=93;
			_poolHbox.setStyle('bottom', 5);
			_poolHbox.setStyle('right', 8);
			_poolHbox.setStyle('horizontalAlign', 'left');
			this.addChild(_poolHbox); /** this = canva*/
			
			lblPool=new Label();
			lblPool.width=170;
			commun.settingLabel(lblPool, "", "M54LabelPeriode", "labelPoolEncoursCommande", _poolHbox);
			lblPool.addEventListener(MouseEvent.CLICK, onClickPoolIconHandler);
			
			choixPool=new Image();
			commun.settingImage(choixPool, ok, -1, -1, -1, -1, -1, -1, _poolHbox, true, true);
			choixPool.addEventListener(MouseEvent.CLICK, onClickPoolIconHandler);
		}
		
		/** --------------------------------------------------------- pool icon kpi -----------------------------------------*/
		/**
		 * permet d(afficher le selecteur et ajouter les listener et calculer la position de selecteur
		 */
		public function onClickPoolIconHandler(evt:MouseEvent):void
		{
			if (selectorPool != null)
			{
				commun.calculatePositionMenuPoolKpi(selectorPool, this,_poolHbox);
				selectorPool.addEventListener(MouseEvent.ROLL_OUT, rollOutMenuPool);
				selectorPool.addEventListener(PoolEvent.VALIDER_VALUE_POOL_EVENT, validerValuePoolHandler);
				
				if (selectorPool.pools != null)
				{
					if (selectorPool.pools.length >=1)
						PopUpManager.addPopUp(selectorPool, this);
				}
			}
		}
		
		/**
		 * mettre a jour le libelle du pool et appeler la fonction  validerHandler pour mettre ajour la valeur de kpi
		 */
		private function validerValuePoolHandler(e:PoolEvent):void
		{
			if(selectorPool.listPool.selectedItem!=null)
			{
				lblPool.text=(selectorPool.listPool.selectedItem as ValeurSelectorVo).libelle;
				validerHandler();
				rollOutMenuPool(null);
			}
		}
		
		/**
		 *permet de masquer le menu de segments
		 */
		private function rollOutMenuPool(e:MouseEvent):void
		{
			closePopUp(selectorPool);
		}
	}
}