package ihm.kpi
{
	import event.kpi.KpiEvent;
	
	import manager.kpi.KpiManager;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.events.FlexEvent;
	
	import service.kpi.getkpiracine.GetKpiService;
	
	import vo.kpi.KpiUserVo;
	import vo.kpi.KpiVo;

	public class KpiImpl extends Canvas
	{
		 /** ces images pour afficher le background des KPI */
		
		[Embed(source="/assets/backGroundAllKpi.png")]
		[Bindable]
		public var backGroundBox:Class;

		[Embed(source="/assets/HomeGraphical.swf", symbol="backKpiRight")]
		[Bindable]
		public var backKpiRight:Class;

		[Embed(source="/assets/HomeGraphical.swf", symbol="backKpiLeft")]
		[Bindable]
		public var backKpiLeft:Class;

		[Bindable]public var kpiVoDataProvider:ArrayCollection;
		
		[Bindable]
		public var kpiUserVoDataProvider:ArrayCollection;
		
		private var _serviceGetKpi:GetKpiService;
		
		private var _emptyVoKpi : ArrayCollection= new ArrayCollection();
		private var _numTab:Number=1;
		private var _nbShowedItem: int=4; /** nb des kpi a afficher */
		private var _kpiManager:KpiManager;
		
		public function KpiImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteKpi);
		}

		/**
		 * permet de creer le manager des kpis et le service pour chercher la liste des kpi d'une racine
		 * @param evt
		 */
		private function onCreationCompleteKpi(evt:FlexEvent):void
		{
			_serviceGetKpi=new GetKpiService();
			_serviceGetKpi.getListKpiByRacine();
			_serviceGetKpi.model.addEventListener(KpiEvent.UPDATE_LIST_KPI_EVENT,setKpiManager)

			_kpiManager=new KpiManager();
			_kpiManager.container=this;
			_kpiManager.nbShowedItem=_nbShowedItem;
		}

		/**
		 * permet de creer un tableau de 4 element correspondent aux 4 elements (KPI) qui seront affichés dans l'IHM
		 * @param KpiManager
		 */
		private function setKpiManager(e:KpiEvent):void
		{
			kpiVoDataProvider=_serviceGetKpi.model.kpis; /** liste des kpi récupéré par le service */
			kpiUserVoDataProvider=_serviceGetKpi.model.kpiUser;
			var newVoKpi :KpiUserVo;
			
			for(var i:int ; i<_nbShowedItem; i++)
			{
				newVoKpi=new KpiUserVo();
				newVoKpi.positionIHM=i; /** préciser le position de KpiVo */
				newVoKpi.codeKpi="";
				_emptyVoKpi.addItem(newVoKpi);
			}
			_kpiManager.setKpiVoInArray(kpiVoDataProvider,kpiUserVoDataProvider,_emptyVoKpi,numTab);
		}
		
		public function get numTab():Number
		{
			return _numTab;
		}
		
		public function set numTab(value:Number):void
		{
			_numTab=value;
		}

	}
}
