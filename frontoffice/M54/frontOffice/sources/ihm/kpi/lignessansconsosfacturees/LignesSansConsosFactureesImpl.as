package ihm.kpi.lignessansconsosfacturees
{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewFormatter;	

	import event.kpi.KpiEvent;	

	import ihm.kpi.kpiabstract.KpiAbstract;

	import utils.infobull.ImgHelp;
	
	import vo.kpi.KpiValueVo;
	
	
	public class LignesSansConsosFactureesImpl extends KpiAbstract
	{
		public function LignesSansConsosFactureesImpl()
		{
			super();
		}

		private var helpText:String = ResourceManager.getInstance().getString('M54', 'Lines_without_consumptions_are_the___');
		
		override protected function setKpiValueHandler(e:KpiEvent):void
		{
			var newArray:ArrayCollection=_getValueKpiService.model.backValueKpi;
			var devise : String;
			
			variation.visible=false;
			separatorIcon.visible=false;
			variation.includeInLayout=false;
			separatorIcon.includeInLayout=false;
			
			if (newArray.length > 0)
			{
				if ((newArray.getItemAt(0) as KpiValueVo).value != null)
				{
					valueKpi=(newArray.getItemAt(0) as KpiValueVo).value;
					devise=(newArray.getItemAt(0) as KpiValueVo).devise;
					
					valueKpi=ConsoviewFormatter.formatNumber(Number(valueKpi), 0);// +" "+devise;
				}
				else
				{
					valueKpi="-";
				}
			}
			else
			{
				valueKpi="-";
			}
			spinner.visible=false; /** stoper le spinner et le rendre invisible*/
			spinner.stop();
		}


		/**
		 * permet de creer les enfants (images) de KPI ( les enfants de Canvas) et de l'abonner à plusieurs event
		 */
		override protected function createChildren():void
		{
			super.createChildren();
			
			if (isEmptyKpi == 0) // kpi normal
			{
				helpIcon=new ImgHelp();
				commun.setHepICon(helpIcon, 10, -1, 10, -1, -1, -1, this, helpText, true, true);
				helpIcon.visible = true;
			}
		}
	}
}