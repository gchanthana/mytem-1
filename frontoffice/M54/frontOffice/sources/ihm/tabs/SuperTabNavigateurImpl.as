package ihm.tabs
{
	import mx.collections.ArrayCollection;
	import mx.containers.TabNavigator;
	import mx.events.FlexEvent;
	
	import event.tab.TabEvent;
	
	import ihm.accueil.AccueilIHM;
	
	import service.selecteur.monoperiodeselector.cache.CacheServiceSingleton;
	import service.tab.gettabracine.GetTabService;
	
	import vo.tab.TabVo;

	public class SuperTabNavigateurImpl extends TabNavigator
	{

		private var _serviceGetTab:GetTabService;	
			
		private var child:AccueilIHM;
		
		[Bindable]
		public var tabDataProvider:ArrayCollection;

		public function SuperTabNavigateurImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteTab);
			_serviceGetTab=new GetTabService();		
		}

		private function onCreationCompleteTab(e:FlexEvent):void
		{
			CacheServiceSingleton.getInstance().flush();
			_serviceGetTab.getListTabByRacine();
			_serviceGetTab.model.addEventListener(TabEvent.CHANGER_DATA_TAB_EVENT, createTab);	
		}

		/**
		 * permet de creer les onglets de chaque user 
		 * @param e
		 */		
		private function createTab(e:TabEvent):void
		{	
			tabDataProvider=_serviceGetTab.model.tabs;

			if (tabDataProvider.length > 0)
			{
				for (var i:int=0; i < tabDataProvider.length; i++)
				{
					addTabNavigator(tabDataProvider[i] as TabVo);
				}
			}
		}
		/**
		 * permet d'ajouter une nouvelle onglet 
		 * @param tab
		 */
		private function addTabNavigator(tab:TabVo):void
		{
			child=new AccueilIHM();
			child.label=tab.nom;
			child.setNumTab(tab.tabId);
			this.addChild(child);
		}	
	}
}
