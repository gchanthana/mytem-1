package ihm.accueil
{
	import mx.containers.VBox;

	[Bindable]
	public class AccueilImpl extends VBox
	{
		public var numTab:int;

		public function AccueilImpl()
		{
			super();
		}

		public function setNumTab(numTab:int):void
		{
			this.numTab=numTab;
		}
	}
}
