package ihm.widget.repartitioncoutsgeographique.composants
{
	import ilog.maps.MapBase;
	import ilog.maps.Map;

	/**
	 * Generated on: lundi 22 juillet 2013 11 h 15 CEST
	 * Projection: geographicCS(name=None, PM=IlvMeridian(name="", longitude=0.0 deg), datum=None)
	 * Simplification threshold: 5000.0 meters  * Interpolation degree: 1
	 **/
	/**
	 * Implementation of a specialized <code>MapBase</code>.
	 *
	 *  @mxml
	 *
	 *  <p>
	 *  The <code>&lt;ilog:FranceMap1Map&gt;</code> tag inherits all the tag attributes of its superclass and
	 *  adds the following tag attributes:
	 *  </p>
	 *  <pre>
	 *  &lt;ilog:FranceMap1Map
	 *    <b>Properties</b>
	 *    allowMultipleSelection="true|false"
	 *    allowSelection="true|false"
	 *    mapFeatures="[]"
	 *    selectedFeatures="[]"
	 *    symbols="[]"
	 *    zoomableSymbols="true|false"
	 *    allowNavigation="true|false"
	 *    filterEvents="true|false"
	 *    featureNames="[]"
	 *    resizeCompatibility="true|false"
	 *    matrix="flash.geom.Matrix"
	 *
	 *    <b>Styles</b>
	 *    animationDuration="0"
	 *    backgroundFill="0x6A5ACD"
	 *    fill="0xFAEBD7"
	 *    stroke="0xA9967D"
	 *    highlightFill="0xA354FF"
	 *    highlightStroke="0xFFD39B"
	 *    filters="[]"
	 *    highlightFilters="[]"
	 *    panCursor="[]"
	 *    showZoomReticle="true|false"
	 *
	 *    <b>Events</b>
	 *    mapChange="<i>No default</i>"
	 *    mapItemDoubleClick="<i>No default</i>"
	 *    mapItemRollOut="<i>No default</i>"
	 *    mapItemRollOver="<i>No default</i>"
	 *    mapItemClick="<i>No default</i>"
	 *  /&gt;
	 *  </pre>
	 *
	 *  @see ilog.maps.MapBase

	 */
	public class FranceMap1Map extends MapBase
	{
		/**
		 * @inheritDoc
		 */
		public override function createMap():Map
		{
			return new FranceMap1();
		}

		/**
		 * @inheritDoc
		 */
		public override function getString(k:String):String
		{
			if (resourceManager != null)
				return resourceManager.getString("FranceMap1", k);
			return k;
		}
	}
}
