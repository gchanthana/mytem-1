package vo.selector
{
	public class Parametre
	{
		private var _cle:String; // répresente le nom du paramètre
		private var _value:Array;
		private var _libelle:String; // c'est le libelle de la clé 
		
		public function Parametre()
		{
		}
				
		public function get value():Array
		{
			return _value;
		}
		
		public function set value(value:Array):void
		{
			_value = value;
		}
				
		public function get cle():String
		{
			return _cle;
		}
		
		public function set cle(value:String):void
		{
			_cle = value;
		}
		
		public function get libelle():String
		{
			return _libelle;
		}
		
		public function set libelle(value:String):void
		{
			_libelle = value;
		}
	}
}