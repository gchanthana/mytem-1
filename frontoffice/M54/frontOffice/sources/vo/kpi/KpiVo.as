package vo.kpi
{

	[Bindable]
	public class KpiVo
	{
		private var _id:Number;
		private var _nom:String;
		private var _codeKpi:String
		private var _positionIHM:Number;
		private var _description:String;
		private var _idCategory:Number;
		private var _nomCategory:String;
		private var _numTab:Number;
		private var _activeForUser:Boolean; /** le client a access */
		private var _selectedItemInPopUp:Boolean=false; /**  pour selectionner le kpi lors que la popup s'ouvre ( cas : remplacer kpi par un autre)*/
		private var _kpiNumTab_id:Number; /**  l'id de kpi dans un onglet cela permet d'ajouter le meme kpi plusieurs fois dans la meme onglet*/

		public function KpiVo()
		{
		}

		public function fill(kpi:Object):void
		{
			this._nom=kpi.NAME_WIDGET;
			this._positionIHM=kpi.POSITION_WIDGET;
			this._numTab=kpi.ONGLETID;
			this._codeKpi=kpi.CODE;
			this._activeForUser=(kpi.HAVE_ACCESS== 0)?false:true;
			this._id=kpi.WIDGETID;
			this._description=kpi.DESCRIPTION_WIDGET;
			this._nomCategory=kpi.CATEGORY_NAME;
			this._idCategory=kpi.CATEGORY_ID;
			this.kpiNumTab_id=kpi.ONGLET_WIDGETID;
		}

		/** ------------------------------ getter et setter -------------------------------------------*/

		public function get nom():String
		{
			return _nom;
		}

		public function set nom(value:String):void
		{
			_nom=value;
		}

		public function get positionIHM():Number
		{
			return _positionIHM;
		}

		public function set positionIHM(value:Number):void
		{
			_positionIHM=value;
		}

		public function get idCategory():Number
		{
			return _idCategory;
		}

		public function set idCategory(value:Number):void
		{
			_idCategory=value;
		}


		public function get numTab():Number
		{
			return _numTab;
		}

		public function set numTab(value:Number):void
		{
			_numTab=value;
		}

		public function get codeKpi():String
		{
			return _codeKpi;
		}

		public function set codeKpi(value:String):void
		{
			_codeKpi=value;
		}

		public function get activeForUser():Boolean
		{
			return _activeForUser;
		}

		public function set activeForUser(value:Boolean):void
		{
			_activeForUser=value;
		}


		public function get selectedItemInPopUp():Boolean
		{
			return _selectedItemInPopUp;
		}

		public function set selectedItemInPopUp(value:Boolean):void
		{
			_selectedItemInPopUp=value;
		}

		public function get id():Number
		{
			return _id;
		}

		public function set idKpi(value:Number):void
		{
			_id=value;
		}

		public function get nomCategory():String
		{
			return _nomCategory;
		}

		public function set nomCategory(value:String):void
		{
			_nomCategory=value;
		}

		public function get description():String
		{
			return _description;
		}

		public function set description(value:String):void
		{
			_description=value;
		}

		public function get kpiNumTab_id():Number
		{
			return _kpiNumTab_id;
		}

		public function set kpiNumTab_id(value:Number):void
		{
			_kpiNumTab_id=value;
		}

	/** ------------------------------------------------------------------------------------------*/
	}
}
