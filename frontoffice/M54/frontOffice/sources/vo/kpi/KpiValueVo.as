package vo.kpi
{
	public class KpiValueVo
	{
		
		private var _value: String;
		private var _variation :String;
		private var _devise : String="€";
		
		public function KpiValueVo()
		{
		}
		
		public function fill(valueKpi:Object):void
		{
			this._value=valueKpi.value;
			this._variation=valueKpi.variation;
			this._devise=valueKpi.devise;
		}
		
		public function get variation():String
		{
			return _variation;
		}
		
		public function set variation(value:String):void
		{
			_variation = value;
		}
		
		public function get value():String
		{
			return _value;
		}
		
		public function set value(value:String):void
		{
			_value = value;
		}
		public function get devise():String
		{
			return _devise;
		}
		
		public function set devise(value:String):void
		{
			_devise = value;
		}
	}
}