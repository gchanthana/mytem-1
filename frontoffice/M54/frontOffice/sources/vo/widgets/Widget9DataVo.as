package vo.widgets
{
	public class Widget9DataVo
	{

		private var _moisCommande:String;
		private var _anneeCommande:String;
		private var _moisAnneeCommande:String;
		private var _nbCommande:Number;
		private var _montantCommande:Number;
		private var _devise:String;
		
		public function Widget9DataVo()
		{
		}
		
		public function fill(obj:Object):void
		{
			this._moisCommande=obj.moisCommande;
			this._anneeCommande=obj.anneeCommande;
			this._moisAnneeCommande=obj.moisAnneeCommande;
			this._nbCommande=obj.nbCommande;
			this._montantCommande = obj.montantCommande;
			this._devise = obj.devise;
		}
		
		public function get moisCommande():String
		{
			return _moisCommande;
		}

		public function set moisCommande(value:String):void
		{
			_moisCommande = value;
		}

		public function get anneeCommande():String
		{
			return _anneeCommande;
		}

		public function set anneeCommande(value:String):void
		{
			_anneeCommande = value;
		}

		public function get moisAnneeCommande():String
		{
			return _moisAnneeCommande;
		}

		public function set moisAnneeCommande(value:String):void
		{
			_moisAnneeCommande = value;
		}

		public function get nbCommande():Number
		{
			return _nbCommande;
		}

		public function set nbCommande(value:Number):void
		{
			_nbCommande = value;
		}
		
		public function get montantCommande():Number { return _montantCommande; }
		
		public function set montantCommande(value:Number):void
		{
			if (_montantCommande == value)
				return;
			_montantCommande = value;
		}
		
		public function get devise():String { return _devise; }
		
		public function set devise(value:String):void
		{
			if (_devise == value)
				return;
			_devise = value;
		}
	}
}