package vo.widgets
{
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;

	public class WidgetUserVo
	{

		private var _nom:String;
		private var _codeWidget :String;
		private var _positionIHM: Number;
		private var _description :String;
		private var _idCategory :Number;
		private var _nomCategory: String;
		private var _numTab :Number;
		private var _activeForUser :Boolean;
		private var _selectedItemInPopUp: Boolean=false;
		private var _id: Number;
		private var _widgetNumTab_id: Number;
		
		public function WidgetUserVo()
		{
		}
		
		public function fill(widget:Object,allWidget :ArrayCollection):void
		{
			this._id=widget.WIDGETID;
			this._widgetNumTab_id=widget.ONGLET_WIDGETID;
			this._positionIHM=widget.POSITION ;
			this._numTab=widget.ONGLETID;
			
			initWidgetUserVo(allWidget);
		}
		
		
		private function initWidgetUserVo(values:ArrayCollection):void
		{
			var _cursor:IViewCursor = values.createCursor();
						
			while(!_cursor.afterLast)
			{		
				if(_cursor.current.id == this._id)
				{
					this._nom=_cursor.current.nom ;
					this._codeWidget=_cursor.current.codeWidget;
					this._activeForUser=_cursor.current.activeForUser;			
					this._description=_cursor.current.description;
					this._nomCategory=_cursor.current.nomCategory;
					this._idCategory=_cursor.current.idCategory;
					break;
				}
				else
				{
					_cursor.moveNext();	
				}
			}
		}
		
		/** ------------------------------ getter et setter -------------------------------------------*/
		
		public function get nom():String
		{
			return _nom;
		}
		
		public function set nom(value:String):void
		{
			_nom = value;
		}
		
		public function get positionIHM():Number
		{
			return _positionIHM;
		}
		
		public function set positionIHM(value:Number):void
		{
			_positionIHM = value;
		}
		
		public function get idCategory():Number
		{
			return _idCategory;
		}
		
		public function set idCategory(value:Number):void
		{
			_idCategory = value;
		}
		
		public function get numTab():Number
		{
			return _numTab;
		}
		
		public function set numTab(value:Number):void
		{
			_numTab = value;
		}
		
		
		public function get activeForUser():Boolean
		{
			return _activeForUser;
		}
		
		public function set activeForUser(value:Boolean):void
		{
			_activeForUser = value;
		}
		
		public function get codeWidget():String
		{
			return _codeWidget;
		}
		
		public function set codeWidget(value:String):void
		{
			_codeWidget = value;
		}
		
		public function get selectedItemInPopUp():Boolean
		{
			return _selectedItemInPopUp;
		}
		
		public function set selectedItemInPopUp(value:Boolean):void
		{
			_selectedItemInPopUp = value;
		}
		
		public function get id():Number
		{
			return _id;
		}
		
		public function set id(value:Number):void
		{
			_id = value;
		}
		
		public function get nomCategory():String
		{
			return _nomCategory;
		}
		
		public function set nomCategory(value:String):void
		{
			_nomCategory = value;
		}
		
		public function get description():String
		{
			return _description;
		}
		
		public function set description(value:String):void
		{
			_description = value;
		}
		public function get widgetNumTab_id():Number
		{
			return _widgetNumTab_id;
		}
		
		public function set widgetNumTab_id(value:Number):void
		{
			_widgetNumTab_id = value;
		}
		/** ------------------------------------------------------------------------------------------*/
	}
}