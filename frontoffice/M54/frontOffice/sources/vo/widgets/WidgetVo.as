package vo.widgets
{
	public class WidgetVo
	{

		private var _nom:String;
		private var _codeWidget :String;
		private var _positionIHM: Number;
		private var _description :String;
		private var _idCategory :Number;
		private var _nomCategory: String;
		private var _numTab :Number;
		private var _activeForUser :Boolean;
		private var _selectedItemInPopUp: Boolean=false;
		private var _id: Number;
		private var _widgetNumTab_id: Number;
		
		public function WidgetVo()
		{
		}
		
		public function fill(widget:Object):void
		{
			this._nom=widget.NAME_WIDGET ;
			this._positionIHM=widget.POSITION_WIDGET ;
			this._numTab=widget.ONGLETID;
			this._codeWidget=widget.CODE;
			this._activeForUser=(widget.HAVE_ACCESS== 0)?false:true;
			this._id=widget.WIDGETID;
			this._description=widget.DESCRIPTION_WIDGET;
			this._nomCategory=widget.CATEGORY_NAME;
			this._idCategory=widget.CATEGORY_ID;
			this._widgetNumTab_id=widget.ONGLET_WIDGETID;
		}
		
		/** ------------------------------ getter et setter -------------------------------------------*/
		
		public function get nom():String
		{
			return _nom;
		}
		
		public function set nom(value:String):void
		{
			_nom = value;
		}
		
		public function get positionIHM():Number
		{
			return _positionIHM;
		}
		
		public function set positionIHM(value:Number):void
		{
			_positionIHM = value;
		}
		
		public function get idCategory():Number
		{
			return _idCategory;
		}
		
		public function set idCategory(value:Number):void
		{
			_idCategory = value;
		}
		
		public function get numTab():Number
		{
			return _numTab;
		}
		
		public function set numTab(value:Number):void
		{
			_numTab = value;
		}
		
		
		public function get activeForUser():Boolean
		{
			return _activeForUser;
		}
		
		public function set activeForUser(value:Boolean):void
		{
			_activeForUser = value;
		}
		
		public function get codeWidget():String
		{
			return _codeWidget;
		}
		
		public function set codeWidget(value:String):void
		{
			_codeWidget = value;
		}
		
		public function get selectedItemInPopUp():Boolean
		{
			return _selectedItemInPopUp;
		}
		
		public function set selectedItemInPopUp(value:Boolean):void
		{
			_selectedItemInPopUp = value;
		}
		
		public function get id():Number
		{
			return _id;
		}
		
		public function set id(value:Number):void
		{
			_id = value;
		}
		
		public function get nomCategory():String
		{
			return _nomCategory;
		}
		
		public function set nomCategory(value:String):void
		{
			_nomCategory = value;
		}
		
		public function get description():String
		{
			return _description;
		}
		
		public function set description(value:String):void
		{
			_description = value;
		}
		public function get widgetNumTab_id():Number
		{
			return _widgetNumTab_id;
		}
		
		public function set widgetNumTab_id(value:Number):void
		{
			_widgetNumTab_id = value;
		}
		/** ------------------------------------------------------------------------------------------*/
	}
}