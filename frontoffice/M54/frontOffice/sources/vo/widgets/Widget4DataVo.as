package vo.widgets
{
	public dynamic class Widget4DataVo
	{
		private var _mois:String;

		public function Widget4DataVo()
		{
		}

		public function fill(widgetData:Object):void
		{
			for (var elem:String in widgetData)
			{
				////trace(i + ": " + widgetData[i]);
				if (elem != "MOIS")
				{
					if (Number(widgetData[elem]) != 0)
					{
						this[elem.toLowerCase()]=widgetData[elem];
					}
				}
			}
			this._mois=widgetData.MOIS;
		}

		public function get mois():String
		{
			return _mois;
		}

		public function set mois(value:String):void
		{
			_mois=value;
		}
	}
}
