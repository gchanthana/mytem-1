package service.widget.updateposition
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class UpdatePositionWidgetService
	{
		
		private var _model:UpdatePositionWidgetModel;
		public var handler:UpdatePositionWidgetHandler;
		private var remote:RemoteObject;
	
		public function UpdatePositionWidgetService()
		{
			this._model = new UpdatePositionWidgetModel();
			this.handler = new UpdatePositionWidgetHandler(_model);
		}		
		
		public function get model():UpdatePositionWidgetModel
		{
			return this._model;
		}

		public function updatePositionWidget(widgetSource:Object,widgetDestination :Object):void
		{		
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			
			remote.source="fr.consotel.consoview.M54.commun.PositionManager";
			remote.addEventListener(ResultEvent.RESULT,handler.UpdatePositionWidgetResultHandler,false,0,true);
			
			var tabObjet:Array=[];
			
			if(widgetDestination==null)
			{
				tabObjet[0]=widgetSource;
				remote.updatePosition(tabObjet,1);
			}
			else
			{
				tabObjet[0]=widgetSource;
				tabObjet[1]=widgetDestination;
				remote.updatePosition(tabObjet,1);	
			}
		}	
	}
}