package service.widget.getwidgetracine
{
	import event.widget.WidgetEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.widgets.WidgetUserVo;
	import vo.widgets.WidgetVo;
	
	internal class GetWidgetModel extends EventDispatcher
	{
		private var _widgets:ArrayCollection;
		private var _widgetsUser :ArrayCollection;

		public function GetWidgetModel()
		{
			_widgets=new ArrayCollection();
			_widgetsUser=new ArrayCollection();
		}

		public function get widgetsUser():ArrayCollection
		{
			return _widgetsUser;
		}

		public function get widgets():ArrayCollection
		{
			return _widgets;
		}

		internal function updateListWidget(value:Array):void
		{
			_widgets.removeAll();
			_widgetsUser.removeAll();
			
			var widgetVo:WidgetVo;
			var widgetUserVo :WidgetUserVo;

			for (var i:int=0; i < value[0].length; i++)
			{
				widgetVo=new WidgetVo();
				widgetVo.fill(value[0][i]);
				_widgets.addItem(widgetVo);
			}
			for (var k:int=0; k < value[1].length; k++)
			{
				widgetUserVo=new WidgetUserVo();
				widgetUserVo.fill(value[1][k],widgets);
				_widgetsUser.addItem(widgetUserVo);
			}
			
			dispatchEvent(new WidgetEvent(WidgetEvent.UPDATE_DATA_WIDGET_EVENT));
		}

	}
}
