package service.widget.getwidgetracine
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class GetWidgetService
	{	
		private var _model:GetWidgetModel;
		public var handler:GetWidgetHandler;
		private var remote:RemoteObject;
		
		public function GetWidgetService()
		{
			this._model = new GetWidgetModel();
			this.handler = new GetWidgetHandler(_model);
		}		
		
		public function get model():GetWidgetModel
		{
			return this._model;
		}
		
		public function getListWidget():void
		{		
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			
			remote.source="fr.consotel.consoview.M54.commun.ListeComposantService";
			remote.addEventListener(ResultEvent.RESULT,handler.getListWidgetResultHandler,false,0,true);
			remote.getAllComposant(2);/** 2 : pour récupérer la liste des widget */
		}	
	}
}