package service.widget.updatedataselectorwidget
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class UpdateDataSelectorWidgetService
	{
		
		private var _model:UpdateDataSelectorWidgetModel;
		public var handler:UpdateDataSelectorWidgetHandler;
		private var remote:RemoteObject;
	
		public function UpdateDataSelectorWidgetService()
		{
			this._model = new UpdateDataSelectorWidgetModel();
			this.handler = new UpdateDataSelectorWidgetHandler(_model);
		}		
		
		public function get model():UpdateDataSelectorWidgetModel
		{
			return this._model;
		}

		public function saveDataSelector(widget_ongletid :Number, widget_paramid :Number ,param:Array ):void
		{					
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			
			remote.source="fr.consotel.consoview.M54.commun.DataManager";
			remote.addEventListener(ResultEvent.RESULT,handler.updateDataWidgetResultHandler,false,0,true);
			remote.updateParametrePermanent(widget_ongletid,widget_paramid,param);	
		}	
	}
}