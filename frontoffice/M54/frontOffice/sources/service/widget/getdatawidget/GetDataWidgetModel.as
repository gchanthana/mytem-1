package service.widget.getdatawidget
{
	import event.widget.WidgetEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.widgets.Widget10DataVo;
	import vo.widgets.WidgetTopDataVo;
	import vo.widgets.Widget1DataVo;
	import vo.widgets.Widget4DataVo;
	import vo.widgets.Widget7DataVo;
	import vo.widgets.Widget9DataVo;
	import vo.widgets.WidgetDataVo;

	[Bindable]
	internal class GetDataWidgetModel extends EventDispatcher
	{
		private var _dataWidget:ArrayCollection;
		private var _codeWidget:String;

		public function GetDataWidgetModel()
		{
		}

		public function get codeWidget():String
		{
			return _codeWidget;
		}

		public function set codeWidget(value:String):void
		{
			_codeWidget=value;
		}

		public function get dataWidget():ArrayCollection
		{
			return _dataWidget;
		}

		internal function updateListWidget(value:ArrayCollection):void
		{
			_dataWidget=new ArrayCollection();

			var widgetData:WidgetDataVo;
			var widget1Data	:Widget1DataVo;
			var widget4Data:Widget4DataVo;
			var widget7Data :Widget7DataVo;
			var widget9Data :Widget9DataVo;
			var widget10Data :Widget10DataVo;
			var widgetInventaireTopData :WidgetTopDataVo;
			var i:int;

			if (value != null)
			{
				switch (_codeWidget)
				{
					case "widget_1":
						for (i=0; i < value.length; i++)
						{
							widget1Data=new Widget1DataVo();
							widget1Data.fillData(value[i]);
							_dataWidget.addItem(widget1Data);
						}
						break;
					case "widget_4":
						for (i=0; i < value.length; i++)
						{
							widget4Data=new Widget4DataVo();
							widget4Data.fill(value[i]);
							_dataWidget.addItem(widget4Data);
						}
						break;
					case "widget_7":
						for (i=0; i < value.length; i++)
						{
							widget7Data=new Widget7DataVo();
							widget7Data.fill(value[i]);
							_dataWidget.addItem(widget7Data);
						}
						break;
					case "widget_9":
						for (i=0; i < value.length; i++)
						{
							widget9Data=new Widget9DataVo();
							widget9Data.fill(value[i]);
							_dataWidget.addItem(widget9Data);
						}
						break;
					case "widget_10":
						for (i=0; i < value.length; i++)
						{
							widget10Data=new Widget10DataVo();
							widget10Data.fill(value[i]);
							_dataWidget.addItem(widget10Data);
						}
						break;
					case "widget_11":
						for (i=0; i < value.length; i++)
						{
							widgetInventaireTopData=new WidgetTopDataVo();
							widgetInventaireTopData.fill(value[i]);
							_dataWidget.addItem(widgetInventaireTopData);
						}
						break;
					case "widget_12":
						for (i=0; i < value.length; i++)
						{
							widgetInventaireTopData=new WidgetTopDataVo();
							widgetInventaireTopData.fill(value[i]);
							_dataWidget.addItem(widgetInventaireTopData);
						}
						break;
					default:
						for (i=0; i < value.length; i++)
						{
							widgetData=new WidgetDataVo();
							widgetData.fill(value[i]);
							_dataWidget.addItem(widgetData);
						}
						break;
				}

				dispatchEvent(new WidgetEvent(WidgetEvent.GET_DATA_WIDGET_EVENT));
			}
		}
	}
}
