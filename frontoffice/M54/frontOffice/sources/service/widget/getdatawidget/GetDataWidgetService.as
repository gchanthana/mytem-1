package service.widget.getdatawidget
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;

	public class GetDataWidgetService
	{		
		private var _model:GetDataWidgetModel;
		public var handler:GetDataWidgetHandler;
		private var remote:RemoteObject;
		
		public function GetDataWidgetService()
		{
			this._model = new GetDataWidgetModel();
			this.handler = new GetDataWidgetHandler(_model);
		}		
		
		public function get model():GetDataWidgetModel
		{
			return this._model;
		}
		
		public function getDataWidget(codeWidget :String , tabParametre : Array):void
		{		
			_model.codeWidget=codeWidget;
			
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
		
			remote.source="fr.consotel.consoview.M54.widget.WidgetDataManager";
			remote.addEventListener(ResultEvent.RESULT,handler.GetDataWidgetResultHandler,false,0,true);
			remote.getDataWidget(codeWidget,tabParametre);
		}	
	}
}