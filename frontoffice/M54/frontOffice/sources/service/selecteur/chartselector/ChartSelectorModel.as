package service.selecteur.chartselector
{
	import event.selector.ChartsEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.charts.events.ChartEvent;
	import mx.collections.ArrayCollection;
	
	import vo.selector.ValeurSelectorVo;

	internal class ChartSelectorModel extends EventDispatcher
	{
		private var _charts:ArrayCollection;

		public function ChartSelectorModel():void
		{
		}

		public function get charts():ArrayCollection
		{
			return _charts;
		}

		internal function updateValues(value:ArrayCollection):void
		{
			_charts=new ArrayCollection();
			
			var valeurSelector:ValeurSelectorVo;

			for (var i:int=0; i < value.length; i++)
			{
				valeurSelector=new ValeurSelectorVo();
				valeurSelector.fill(value[i],i);
				_charts.addItem(valeurSelector);
			}
			this.dispatchEvent(new ChartsEvent(ChartsEvent.CHANGER_VALUE_CHART_EVENT));/** capturé dans ChartSelectorImpl */
		}
	}
}
