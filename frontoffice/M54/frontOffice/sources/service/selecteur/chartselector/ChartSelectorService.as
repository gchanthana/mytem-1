package service.selecteur.chartselector
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class ChartSelectorService
	{	
		private var _model:ChartSelectorModel;
		public var handler:ChartSelectorHandler;
		private var remote:RemoteObject;
		private var amfChannelUri:String="http://sun-dev.consotel.fr:8310" ;
		
		
		public function ChartSelectorService():void
		{
			this._model = new ChartSelectorModel();
			this.handler = new ChartSelectorHandler(_model);
		}		
			
		public function get model():ChartSelectorModel
		{
			return this._model;
		}
		
		public function getValuesChart(widgetParamId :Number):void
		{	
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			remote.source="fr.consotel.consoview.M54.commun.ListeSelector";
			
			remote.addEventListener(ResultEvent.RESULT,handler.getValueChartHandler,false,0,true);
			remote.getListeTypeChart(widgetParamId);
		}	
	}
}