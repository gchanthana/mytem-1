package service.selecteur.segmentselector
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class SegmentSelectorService
	{	
		private var _model:SegmentSelectorModel;
		public var handler:SegmentSelectorHandler;
		private var remote:RemoteObject;
		
		public function SegmentSelectorService():void
		{
			this._model = new SegmentSelectorModel();
			this.handler = new SegmentSelectorHandler(model);
		}		
		
		public function get model():SegmentSelectorModel
		{
			return this._model;
		}
		
		public function getValueSegment(idParam :Number):void
		{					
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			
			remote.source="fr.consotel.consoview.M54.commun.ListeSelector";
			remote.addEventListener(ResultEvent.RESULT,handler.getValueSegmentHandler,false,0,true);
			remote.geListeSegment(idParam);
		}	
	}
}