package service.selecteur.monoperiodeselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import service.selecteur.monoperiodeselector.cache.CacheServiceSingleton;

	public class MonoPeriodeSelectorService
	{
		
		private var _model:MonoPeriodeSelectorModel;
		public var handler:MonoPeriodeSelectorHandler;
		private var remote:RemoteObject;
		internal static const CACHE_KEY:String = "PERIODS";
		
		public function MonoPeriodeSelectorService()
		{
			this._model = new MonoPeriodeSelectorModel();
			this.handler = new MonoPeriodeSelectorHandler(model);
 
		}		
		public function get model():MonoPeriodeSelectorModel
		{
			return this._model;
		}
		
		public function getValuesMonoPeriode():void
		{	
			if(CacheServiceSingleton.getInstance().getCache(CACHE_KEY) != null)
			{
				_model.updateValues(CacheServiceSingleton.getInstance().getCache(CACHE_KEY) as ArrayCollection);	
			}
			else
			{
				
				remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
				remote.source="fr.consotel.consoview.M54.commun.ListeSelector";
				remote.addEventListener(ResultEvent.RESULT,handler.getValuesMonoPeriodeHandler,false,0,true);
				remote.getValuesMonoPeriode();
			}
			
		}
	}
}