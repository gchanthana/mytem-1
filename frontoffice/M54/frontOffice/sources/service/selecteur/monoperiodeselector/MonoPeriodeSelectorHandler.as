package service.selecteur.monoperiodeselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class MonoPeriodeSelectorHandler
	{
		private var _model: MonoPeriodeSelectorModel;
		
		public function MonoPeriodeSelectorHandler(_model:MonoPeriodeSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValuesMonoPeriodeHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateValues(evt.result as ArrayCollection);
			}
		}	
	}
}