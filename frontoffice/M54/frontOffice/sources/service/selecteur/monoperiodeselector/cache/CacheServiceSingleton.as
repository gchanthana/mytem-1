package service.selecteur.monoperiodeselector.cache
{
	public class CacheServiceSingleton
	{
		private static var _cacheSingleton:CacheServiceSingleton = new CacheServiceSingleton();
		private var _cache:Object;
		
		public function CacheServiceSingleton()
		{	
			if(_cache != null)
			{
				throw new Error("Must use the getInstance method");	
			}
			else
			{
				_cache = {};
			}
		}
		
		public static function getInstance():CacheServiceSingleton
		{	
			return _cacheSingleton
		}
		
		public  function getCache(key:String):*
		{
			if(! _cache.hasOwnProperty(key))
			{
				_cache[key]	
			}
			
			return _cache[key];
			 
		}
		
		public function setCache(key:String,values:*):void
		{
			_cache[key] = values;
		}
		
		public function flush():void
		{
			_cache = new Object();	
		}
	}
}