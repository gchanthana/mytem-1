package service.selecteur.poolselector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;

	public class PoolSelectorService
	{	
		private var _model:PoolSelectorModel;
		private var remote:RemoteObject;
		
		public var handler:PoolSelectorHandler;
		
		public function PoolSelectorService():void
		{
			this._model = new PoolSelectorModel();
			this.handler = new PoolSelectorHandler(model);
		}		
		
		public function get model():PoolSelectorModel
		{
			return this._model;
		}
		
		public function getValuePool(idParam :Number):void
		{					
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			remote.source="fr.consotel.consoview.M54.commun.ListeSelector";
			remote.addEventListener(ResultEvent.RESULT,handler.getValuePoolHandler,false,0,true);
			remote.getListePool(idParam);
		}	
	}
}