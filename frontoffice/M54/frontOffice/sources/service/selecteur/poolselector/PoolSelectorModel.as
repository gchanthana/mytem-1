package service.selecteur.poolselector
{
	import event.selector.PoolEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import vo.selector.ValeurSelectorVo;

	internal class PoolSelectorModel  extends EventDispatcher
	{
		private var _pools :ArrayCollection;
		
		public function PoolSelectorModel():void
		{
		}
		
		public function get pools():ArrayCollection
		{
			return _pools;
		}
		
		internal function updateValues(value:ArrayCollection):void
		{	
			_pools=new ArrayCollection();
			
			var valeurSelector:ValeurSelectorVo;
			
			valeurSelector = new ValeurSelectorVo();
			valeurSelector.id=-1;
			valeurSelector.indexInList=0;/** initialiser la position de l'element dans l'array*/
			valeurSelector.libelle=ResourceManager.getInstance().getString('M54', 'Tous_les_pools');
			
			_pools.addItemAt(valeurSelector,0);
			
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelectorVo();
				valeurSelector.fill(value[i],i+1);
				_pools.addItem(valeurSelector); 
			}
			this.dispatchEvent(new PoolEvent(PoolEvent.GET_VALUE_POOL_EVENT));/** capturé dans KpiAbstract */
		}
	}
}