package service.selecteur.themeselector
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class ThemeSelectorService
	{
		private var _model:ThemeSelectorModel;
		public var handler:ThemeSelectorHandler;
		private var remote:RemoteObject;
		
		public function ThemeSelectorService():void
		{
			this._model = new ThemeSelectorModel();
			this.handler = new ThemeSelectorHandler(model);
		}		
		
		public function get model():ThemeSelectorModel
		{
			return this._model;
		}
		
		public function getValuesTheme(idParam :Number):void
		{				
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			
			remote.source="fr.consotel.consoview.M54.commun.ListeSelector";
			remote.addEventListener(ResultEvent.RESULT,handler.getValueThemeHandler,false,0,true);
			remote.getListeTheme(idParam);
		}	
	}
}