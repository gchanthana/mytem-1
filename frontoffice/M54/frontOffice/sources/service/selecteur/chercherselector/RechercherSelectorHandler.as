package service.selecteur.chercherselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class RechercherSelectorHandler
	{
		private var _model:RechercherSelectorModel;
		
		public function RechercherSelectorHandler(_model:  RechercherSelectorModel):void
		{
			this._model = _model;
		}	
		internal function getSelectorResulthandler(evt:ResultEvent):void
		{
			
			if(evt.result)
			{
				
				_model.updateSelector(evt.result as ArrayCollection);
			}
		}
	}
}