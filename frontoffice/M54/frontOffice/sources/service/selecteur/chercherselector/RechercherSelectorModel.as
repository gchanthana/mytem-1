package service.selecteur.chercherselector
{
	import event.selector.SelectorEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.selector.SelectorParametreVo;

	internal class RechercherSelectorModel extends EventDispatcher
	{

		private var _selectors:ArrayCollection;

		public var type: Number=1;
		public function RechercherSelectorModel()
		{
		}

		public function get selectors():ArrayCollection
		{
			return _selectors;
		}

		internal function updateSelector(value:ArrayCollection):void
		{
			_selectors=new ArrayCollection();

			var selectorParam:SelectorParametreVo;

			for (var i:int=0; i < value.length; i++)
			{
				selectorParam=new SelectorParametreVo();
				selectorParam.fill(value[i]);
				_selectors.addItem(selectorParam);
			}
			if(type==1)
			{
				dispatchEvent(new SelectorEvent(SelectorEvent.UPDATE_SELECTOR_KPI_EVENT));
			}
			else
			{	
				dispatchEvent(new SelectorEvent(SelectorEvent.UPDATE_SELECTOR_WIDGET_EVENT));
			}
		}
	}
}
