package service.selecteur.chercherselector
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class RechercherSelectorService
	{
		
		private var _model:RechercherSelectorModel;
		public var handler:RechercherSelectorHandler;
		private var remote:RemoteObject;
		
		public function RechercherSelectorService()
		{
			this._model = new RechercherSelectorModel();
			this.handler = new RechercherSelectorHandler(_model);
		}		
		
		public function get model():RechercherSelectorModel
		{
			return this._model;
		}
		
		public function getSelector(widgetNumTab_id: Number):void
		{	
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			remote.source="fr.consotel.consoview.M54.commun.DataManager";
			remote.addEventListener(ResultEvent.RESULT,handler.getSelectorResulthandler);
			remote.getParametres(widgetNumTab_id);
		}
	}
}