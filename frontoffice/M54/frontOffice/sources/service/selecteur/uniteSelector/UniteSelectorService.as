package service.selecteur.uniteSelector
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class UniteSelectorService
	{
		
		private var _model:UniteSelectorModel;
		public var handler:UniteSelectorHandler;
		private var remote:RemoteObject;
		
		public function UniteSelectorService():void
		{
			this._model = new UniteSelectorModel();
			this.handler = new UniteSelectorHandler(model);
		}		
		
		
		public function get model():UniteSelectorModel
		{
			return this._model;
		}
		
		public function getValuesUnite(idParam :Number):void
		{					
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			
			remote.source="fr.consotel.consoview.M54.commun.ListeSelector";
			remote.addEventListener(ResultEvent.RESULT,handler.getValueUniteHandler,false,0,true);
			remote.getListeUnitesData(idParam);
			
		}	
	}
}