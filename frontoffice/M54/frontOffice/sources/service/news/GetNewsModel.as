package service.news
{
	import event.news.NewsEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.news.NewsVo;

	internal class GetNewsModel extends EventDispatcher
	{
		private var _news:ArrayCollection;

		public function GetNewsModel()
		{
			_news=new ArrayCollection();
		}

		public function get news():ArrayCollection
		{
			return _news;
		}

		internal function updateListNews(value:ArrayCollection):void
		{
			_news.removeAll();
			var newsVo:NewsVo;

			for (var i:int=0; i < value.length; i++)
			{
				newsVo=new NewsVo();
				newsVo.fill(value[i]);
				_news.addItem(newsVo);
			}
			dispatchEvent(new NewsEvent(NewsEvent.GET_LIST_NEWS_EVENT));
		}
	}
}
