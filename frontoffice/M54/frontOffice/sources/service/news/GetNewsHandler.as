package service.news
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class GetNewsHandler
	{
		private var _model:GetNewsModel;
		
		public function GetNewsHandler(_model:  GetNewsModel):void
		{
			this._model = _model;
		}	
		internal function getListNewsResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateListNews(evt.result.RESULT as ArrayCollection);
			}
		}
	}
}