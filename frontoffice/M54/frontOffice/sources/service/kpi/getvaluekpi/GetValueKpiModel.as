package service.kpi.getvaluekpi
{
	import event.kpi.KpiEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.kpi.KpiValueVo;
	
	[Bindable]
	internal class GetValueKpiModel extends EventDispatcher
	{
		private var _backValueKpi:ArrayCollection;

		public function GetValueKpiModel()
		{
		}

		public function get backValueKpi():ArrayCollection
		{
			return _backValueKpi;
		}

		internal function updateValueKpi(value:ArrayCollection):void
		{					
			_backValueKpi=new ArrayCollection();
					
			var kpiValue:KpiValueVo;
			
			if(value !=null)
			{			
				for (var i:int=0; i < value.length; i++)
				{
					kpiValue=new KpiValueVo();
					kpiValue.fill(value[i]);
					_backValueKpi.addItem(kpiValue);
				}			
			}
			dispatchEvent(new KpiEvent(KpiEvent.GET_VALUE_KPI_EVENT));
		}
	}
}
