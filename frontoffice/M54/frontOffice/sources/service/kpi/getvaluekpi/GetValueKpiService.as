package service.kpi.getvaluekpi
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class GetValueKpiService
	{
		private var _model:GetValueKpiModel;
		public var handler:GetValueKpiHandler;
		private var remote:RemoteObject;
		
		public function GetValueKpiService()
		{
			this._model=new GetValueKpiModel();
			this.handler=new GetValueKpiHandler(_model);
		}

		public function get model():GetValueKpiModel
		{
			return this._model;
		}

		public function getValue(codeKpi:String, tabParametre:Array):void
		{	
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			
			remote.source="fr.consotel.consoview.M54.kpi.KpiDataManager";
			remote.addEventListener(ResultEvent.RESULT,handler.GetValueKpiResultHandler,false,0,true);
			remote.getDataKPI(codeKpi, tabParametre);	
			//trace("appel kpi " +codeKpi);
		}
	}
}
