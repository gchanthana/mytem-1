package service.kpi.getvaluekpi
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class GetValueKpiHandler
	{
		private var _model:GetValueKpiModel;
		
		public function GetValueKpiHandler(_model:  GetValueKpiModel):void
		{
			this._model = _model;
		}	
		internal function GetValueKpiResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateValueKpi(evt.result as ArrayCollection);
			}
		}
	}
}