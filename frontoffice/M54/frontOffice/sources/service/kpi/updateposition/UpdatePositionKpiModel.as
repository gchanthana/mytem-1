package service.kpi.updateposition
{
	import event.kpi.KpiEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	import vo.kpi.KpiVo;
	
	internal class UpdatePositionKpiModel extends EventDispatcher
	{
		private var _kpiNumTab_id:Number;

		public function UpdatePositionKpiModel()
		{
		}

		public function get kpiNumTab_id():Number
		{
			return _kpiNumTab_id;
		}

		internal function updateValueKpi(res:ResultEvent):void
		{
			_kpiNumTab_id=res.result[0];
			
			if(_kpiNumTab_id > 0)
			{
				dispatchEvent(new KpiEvent(KpiEvent.UPDATE_PARAMTAB_KPI_EVENT));/**  capture dans kpiAbstact */
			}
		}
	}
}
