package service.kpi.getkpiracine
{
	import event.kpi.KpiEvent;
	
	import flash.events.EventDispatcher;
	
	import ihm.kpi.kpiabstract.KpiAbstract;
	
	import mx.collections.ArrayCollection;
	
	import vo.kpi.KpiUserVo;
	import vo.kpi.KpiVo;

	internal class GetKpiModel extends EventDispatcher
	{
		private var _kpis:ArrayCollection;
		private var _kpiUser:ArrayCollection

		public function GetKpiModel()
		{
			_kpis=new ArrayCollection();
			_kpiUser=new ArrayCollection();
		}

		public function get kpiUser():ArrayCollection
		{
			return _kpiUser;
		}

		public function get kpis():ArrayCollection
		{
			return _kpis;
		}

		internal function updateListKpi(value:Array):void
		{
			_kpis.removeAll();
			_kpiUser.removeAll();
			
			var kpiVo:KpiVo;
			var kpiUserVo :KpiUserVo;

			for (var i:int=0; i < value[0].length; i++)
			{
				kpiVo=new KpiVo();
				kpiVo.fill(value[0][i]);
				_kpis.addItem(kpiVo);
			}
			for (var k:int=0; k < value[1].length; k++)
			{
				kpiUserVo=new KpiUserVo();
				kpiUserVo.fill(value[1][k], _kpis);
				_kpiUser.addItem(kpiUserVo);
			}

			dispatchEvent(new KpiEvent(KpiEvent.UPDATE_LIST_KPI_EVENT));
		}
	}
}
