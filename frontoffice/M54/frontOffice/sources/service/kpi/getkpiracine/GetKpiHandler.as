package service.kpi.getkpiracine
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class GetKpiHandler
	{
		private var _model:GetKpiModel;
		
		public function GetKpiHandler(_model:  GetKpiModel):void
		{
			this._model = _model;
		}	
		internal function getListKpiResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateListKpi(evt.result as Array);
			}
		}
	}
}