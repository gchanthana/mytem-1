package event.mapfrance
{
	import flash.events.Event;

	/**
	 * classe relative à l'événement MouseOutEvent
	 */
	public class MouseOutEvent extends Event
	{
		//bubbles indiquent si l'evenement peut être propagé, cancelable s'il peut être arrêté en cours de propagation
		public function MouseOutEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event

		{

			return new MouseOutEvent(type, bubbles, cancelable);

		}
	}
}
