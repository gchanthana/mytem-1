package event.widget
{
	import flash.events.Event;
	
	public class WidgetEvent extends Event
	{
		
		public static const UPDATE_DATA_WIDGET_EVENT:String = "updateDataWidgetEvent";
		public static const UPDATE_PARAMTAB_WIDGET_EVENT:String="updateParamTabWidgetEvent";
		public static const GET_DATA_WIDGET_EVENT:String="getDataWidgetEvent";
		public static const UPDATE_LIST_WIDGET_USER_EVENT:String="updateListWidgetUserEvent";
		
		public function WidgetEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new WidgetEvent(type, bubbles, cancelable);
		}
	}
}