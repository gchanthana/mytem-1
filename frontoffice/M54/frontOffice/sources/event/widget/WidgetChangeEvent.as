package event.widget
{
	import flash.events.Event;
	
	public class WidgetChangeEvent extends Event
	{
		public static const CLOSE_WIDGET_EVENT:String="closeWidgetEvent";
		public static const GOTO_WIDGET_MODULE_EVENT:String="gotoWidgetModuleEvent";
		public static const LISTER_WIDGET_EVENT:String="listerWidgetEvent";
		public static const LISTER_ALL_WIDGET_EVENT:String="listerAllWidgetEvent";
		public static const RESTORE_WIDGET_EVENT:String = "restoreWidgetEvent";
		public static const MAXIMIZE_WIDGET_EVENT:String = "maximizeWidgetEvent";
		public static const ADD_WIDGET_EVENT:String = "addWidgetEvent";
		public static const EXPORTER_DATE_WIDGET_EVENT :String="exporterDateWidgetEvent"
		
		public function WidgetChangeEvent(type:String)
		{
			super(type, true, true);
		}
	}
}