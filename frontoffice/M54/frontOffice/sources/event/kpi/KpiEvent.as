package event.kpi
{
	import flash.events.Event;

	public class KpiEvent extends Event
	{

		public static const UPDATE_LIST_KPI_EVENT:String="updateListKpiEvent";
		public static const GET_VALUE_KPI_EVENT:String="getValueKpiEvent";
		public static const UPDATE_PARAMTAB_KPI_EVENT:String="updateParamTabKpiEvent";
		public static const UPDATE_LIST_KPI_USER_EVENT:String="updateListKpiUserEvent";

		public function KpiEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new KpiEvent(type, bubbles, cancelable);
		}
	}
}
