package event.selector
{
	import flash.events.Event;

	public class MonoPeriodeEvent extends Event
	{

		public static const USER_MONO_PERIODE_SELECTOR_EVENT:String="UserMonoPeriodeSelectorEvent";
		public static const CHANGER_VALUE_HSLIDER_EVENT :String ="changerValueHsliderEvent";
		public static const VALIDER_VALUE_HSLIDER_EVENT :String ="validerValueHsliderEvent";
		
		public function MonoPeriodeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new MonoPeriodeEvent(type, bubbles, cancelable);
		}
	}
}
