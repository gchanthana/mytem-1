package event.selector
{
	import flash.events.Event;

	public class ThemeEvent extends Event
	{
		public static const CHANGER_VALUE_THEME_EVENT :String ="changerValueThemeEvent";
		public static const VALIDER_VALUE_THEME_EVENT :String ="validerValueThemeEvent";
		
		public function ThemeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ThemeEvent(type, bubbles, cancelable);
		}
	}
}