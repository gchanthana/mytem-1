package event.selector
{
	import flash.events.Event;
	
	public class SelectorEvent extends Event
	{
		
		public static const UPDATE_SELECTOR_WIDGET_EVENT:String = "updateSelectorWidgetEvent";
		public static const UPDATE_SELECTOR_KPI_EVENT:String = "updateSelectorKpiEvent";
		
		public function SelectorEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new SelectorEvent(type, bubbles, cancelable);
		}
	}
}