package event.selector
{
	import flash.events.Event;

	public class ChartsEvent extends Event
	{
		public static const CHANGER_VALUE_CHART_EVENT :String ="changerValueChartEvent";
		public static const VALIDER_VALUE_CHART_EVENT :String ="validerValueChartEvent";
		
		public function ChartsEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ChartsEvent(type, bubbles, cancelable);
		}
	}
}