package event.selector
{
	import flash.events.Event;

	public class UniteEvent extends Event
	{
		public static const CHANGER_VALUE_UNITE_EVENT :String ="changerValueUniteEvent";
		public static const VALIDER_VALUE_UNITE_EVENT :String ="validerValueUniteEvent";
		
		public function UniteEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new UniteEvent(type, bubbles, cancelable);
		}
	}
}