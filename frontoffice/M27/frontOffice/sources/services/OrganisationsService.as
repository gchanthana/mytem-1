package services
{
	import composants.util.ConsoviewAlert;
	
	import events.OrganisationsEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class OrganisationsService extends EventDispatcher
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					PUBLICS
			//--------------------------------------------------------------------------------------------//
		
		public var listeOrganisations				:ArrayCollection = new ArrayCollection();
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function OrganisationsService()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCÉDURES
		//--------------------------------------------------------------------------------------------//
		
		//Liste de toutes les organisations clientes
		public function getListeOraganisationsCliente():void
		{
			listeOrganisations = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.RAGO.Rules",
																				"getListOrgaCliente",
																				listeOraganisationsClienteResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					RETOURS PROCÉDURES
		//--------------------------------------------------------------------------------------------//
		
		//Traitement de la liste des organisations clientes
		private function listeOraganisationsClienteResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				listeOrganisations = re.result as ArrayCollection;
				
				dispatchEvent(new OrganisationsEvent(OrganisationsEvent.ORGANISATIONSCLIENTES));
			}
			else 
				ConsoviewAlert.afficherAlertInfo("Impossible de récupérer les organisations clientes.", "Consoview", null);
		}
		
	}
}