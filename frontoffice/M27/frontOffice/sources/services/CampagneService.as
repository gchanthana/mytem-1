package services
{
	import composants.util.ConsoviewAlert;
	
	import entities.Campagne;
	import entities.DataInfos;
	import entities.HistoricCampagne;
	import entities.HistoricLigne;
	import entities.ImportData;
	
	import events.CampagneEvent;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;

	public class CampagneService extends EventDispatcher
	{

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					PUBLICS
			//--------------------------------------------------------------------------------------------//
		
		public var listeImportData					:ArrayCollection = new ArrayCollection();
		public var listeCampagnes					:ArrayCollection = new ArrayCollection();
		public var listeLignesCamapagne				:ArrayCollection = new ArrayCollection();
		public var listeHistorique					:ArrayCollection = new ArrayCollection();
		public var listeFiltres						:ArrayCollection = new ArrayCollection();
		
		public var currentUUID						:String = '';
		
		public var currentNbrLignes					:int = 0;
		
			//--------------------------------------------------------------------------------------------//
			//					PUBLICS
			//--------------------------------------------------------------------------------------------//
		
		private var _cfcPath						:String = 'fr.consotel.consoview.M27.CampagneService';
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function CampagneService()
		{
		}
	
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCÉDURES
		//--------------------------------------------------------------------------------------------//
		
		public function getCampagnes():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_cfcPath,
																				'getCampagnes',
																				getCampagnesResultHandler); 
			
			RemoteObjectUtil.callService(op);	
		}
		
		public function getUUID():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_cfcPath,
																				'getUUID',
																				getUUIDResultHandler); 
			
			RemoteObjectUtil.callService(op);	
		}
		
		public function getImportData(uuid:String, fileName:String):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_cfcPath,
																				'getImportData',
																				getImportDataResultHandler); 
				
			RemoteObjectUtil.callService(op,uuid,
											fileName);	
		}
		
		public function setImportDataToCSV(uuid:String, lignes:ArrayCollection):void
		{
			var dataArray:Array = getArrayCollectionToArray(lignes, 'SOUSTETE');
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_cfcPath,
																				'setImportDataToCSV',
																				setImportDataToCSVResultHandler); 
			
			RemoteObjectUtil.callService(op,uuid,
											dataArray);	
		}
		
		public function getNombreLignes(currentDataInfos:DataInfos):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_cfcPath,
																				'getNombreLignes',
																				getNombreLignesResultHandler); 
			
			RemoteObjectUtil.callService(op, currentDataInfos.IDPERIMETRE);	
		}
		
		public function saveCampagne(currentDataInfos:DataInfos):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_cfcPath,
																				'saveCampagne',
																				saveCampagneResultHandler); 
			
			RemoteObjectUtil.callService(op,currentDataInfos.LIBELLE,
											currentDataInfos.DESCRIPTION,
											currentDataInfos.PERIMETRE,
											currentDataInfos.UUID,
											currentDataInfos.IDPERIMETRE);	
		}
		
		public function sendCampagne(currentDataInfos:DataInfos):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_cfcPath,
																				'sendCampagne',
																				sendCampagneResultHandler); 
			
			RemoteObjectUtil.callService(op,currentDataInfos.LIBELLE,
											currentDataInfos.DESCRIPTION,
											currentDataInfos.PERIMETRE,
											currentDataInfos.UUID,
											currentDataInfos.IDPERIMETRE,
											currentDataInfos.IDCAMPAGNE);
		}
		
		public function getLignesCampagne(currentCampagne:Campagne):void
		{
			listeLignesCamapagne = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_cfcPath,
																				'getLignesCampagne',
																				lignesCampagneResultHandler); 
			
			RemoteObjectUtil.callService(op,currentCampagne.IDCAMPAGNE);
		}
		
		public function getHistoriqueLigneCampagne(currentHistoricCampagne:HistoricCampagne):void
		{
			listeHistorique = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_cfcPath,
																				'getHistoriqueLigneCampagne',
																				historiqueLigneCampagneResultHandler); 
			
			RemoteObjectUtil.callService(op,currentHistoricCampagne.IDMDMLOG);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					RETOURS PROCÉDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getCampagnesResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				listeCampagnes = Campagne.formatCampagne(re.result as ArrayCollection);

				dispatchEvent(new CampagneEvent(CampagneEvent.LISTECAMPAGNE));
			}
			else 
				ConsoviewAlert.afficherAlertInfo("Impossible de récupérer la liste des campagnes.", 'Consoview', null);
		}
		
		private function getUUIDResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				currentUUID = re.result as String;
				
				dispatchEvent(new CampagneEvent(CampagneEvent.UUID));
			}
			else 
				ConsoviewAlert.afficherAlertInfo("Impossible de récupérer le fichier.", 'Consoview', null);
		}
	
		private function getImportDataResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				listeImportData = ImportData.formatImportData(re.result as ArrayCollection);
				
				dispatchEvent(new CampagneEvent(CampagneEvent.IMPORTDATA));
			}
			else 
				ConsoviewAlert.afficherAlertInfo("Impossible de récupérer les informations du fichier.", 'Consoview', null);
		}
		
		private function setImportDataToCSVResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result > 0)
					dispatchEvent(new CampagneEvent(CampagneEvent.SETDATATOCSV));
				else
					ConsoviewAlert.afficherAlertInfo("Impossible d'envoyer le fichier.", 'Consoview', null);
			}
			else 
				ConsoviewAlert.afficherAlertInfo("Impossible d'envoyer le fichier.", 'Consoview', null);
		}
		
		private function getNombreLignesResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result > 0)
				{
					currentNbrLignes = int(re.result);
					
					dispatchEvent(new CampagneEvent(CampagneEvent.NBRLIGNECAMPAGNE));
				}
				else
					ConsoviewAlert.afficherAlertInfo("Aucune ligne.", 'Consoview', null);
			}
			else 
				ConsoviewAlert.afficherAlertInfo("Impossible de récupérer le nombre de lignes impactées.", 'Consoview', null);
		}
				
		private function saveCampagneResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result > 0)
					dispatchEvent(new CampagneEvent(CampagneEvent.CAMPAGNESAVED));
				else
					ConsoviewAlert.afficherAlertInfo("Impossible de sauvegarder votre campagne.", 'Consoview', null);
			}
			else 
				ConsoviewAlert.afficherAlertInfo("Impossible de sauvegarder votre campagne.", 'Consoview', null);
		}
		
		private function sendCampagneResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result > 0)					
					dispatchEvent(new CampagneEvent(CampagneEvent.CAMPAGNESENT));
				else
					ConsoviewAlert.afficherAlertInfo("Impossible d'envoyer votre campagne.", 'Consoview', null);
			}
			else 
				ConsoviewAlert.afficherAlertInfo("Impossible d'envoyer votre campagne.", 'Consoview', null);
		}
		
		private function lignesCampagneResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				listeLignesCamapagne = HistoricCampagne.formatHistoricCampagne(re.result as ArrayCollection);
				listeFiltres		 = getFilter(ObjectUtil.copy(listeLignesCamapagne) as ArrayCollection);

				dispatchEvent(new CampagneEvent(CampagneEvent.CAMPAGNELISTELIGNE));
			}
			else 
				ConsoviewAlert.afficherAlertInfo("Impossible de récupérer les lignes de la campagne.", 'Consoview', null);
		}
		
		private function historiqueLigneCampagneResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result)
				{
					listeHistorique = HistoricLigne.formatHistoricLigne(re.result as ArrayCollection);
					
					dispatchEvent(new CampagneEvent(CampagneEvent.LIGNEHISTORIQUE));
				}						
				else
					ConsoviewAlert.afficherAlertInfo("Impossible de récupérer l'historique de la ligne.", 'Consoview', null);
			}
			else 
				ConsoviewAlert.afficherAlertInfo("Impossible de récupérer l'historique de la ligne.", 'Consoview', null);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		private function getArrayCollectionToArray(currentArrayCollection:ArrayCollection, property:String):Array
		{
			var dataArray	:Array = new Array();
			var len			:int = currentArrayCollection.length;
			
			for(var i:int = 0;i < len;i++)
			{
				dataArray[i] = currentArrayCollection[i][property];
			}
			
			return dataArray;
		}
		
		private function getFilter(values:ArrayCollection):ArrayCollection
		{
			var filters		:ArrayCollection = new ArrayCollection();
			var lenValues	:int = values.length;
			var statut		:int = -10;
			
			if(values != null)
			{
				var tri:Sort = new Sort();
					tri.fields 	 = [new SortField('STATUT', false)];
				
				values.sort = tri;
				values.refresh();
				values.sort = null;
				
				for each (var currentObject:Object in values)
				{
					if(currentObject.STATUT != statut)
					{
						filters.addItem(currentObject);
						
						statut = currentObject.STATUT;
					}
				}
			}

			return filters;
		}	

	}
}