package composants.parametres.perimetres
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	 
	public dynamic class linesGrid extends linesGridIHM
	{
		public var _contextList:ArrayCollection; 
		public function get modeEcriture():Boolean{ return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)}		

		public function linesGrid()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		override protected function commitProperties():void{
			super.commitProperties();
		}
		
		protected function initIHM(event:Event):void
		{
			txtFiltreLignes.addEventListener(Event.CHANGE, filterGrid);
			gdrLignes.addEventListener(Event.CHANGE, onSelectedGridChange);			
			
			gdrLignes.dragEnabled = modeEcriture;	
			gdrLignes.dragMoveEnabled = modeEcriture;
		}
		
		protected function onSelectedGridChange(event: Event):void
		{
			parentDocument.selectedLineChanged();
		}
		
	 
		
		public function setContextMenu(data:ArrayCollection):void
		{
			gdrLignes.setContextMenu(data);
		}
		
		protected function filterGrid(event: Event):void
		{
			gdrLignes.dataProvider.filterFunction = filterFunc;
			gdrLignes.dataProvider.refresh();
		}
		
		private function filterFunc(item:Object):Boolean {		
			return (item.SOUS_TETE.toLowerCase().search(txtFiltreLignes.text.toLowerCase()) != -1);
		}
		
		
		
		
		public function getSelectedLabel():String
		{
			if (gdrLignes.selectedIndex < 0)
				return null;
			return gdrLignes.selectedItem.SOUS_TETE;
		}
		public function getSelectedValue():Number
		{
			if (gdrLignes.selectedIndex < 0)
				return -1;
			return gdrLignes.selectedItem.IDSOUS_TETE;
		}
		
		public function getSelectedType():String
		{
			if (gdrLignes.selectedIndex < 0)
				return null;
			return gdrLignes.selectedItem.COEFFAUTO;
		}
		
		public function setData(data:ArrayCollection):void
		{
			gdrLignes.dataProvider = data;
		}
		
		public function getSelectedListGridItem():String
		{
			var items:String = "";
			var bFlag:Boolean = true;
			for each (var obj:Object in gdrLignes.selectedItems)
			{
				if (!bFlag)
					items += ",";
				items += obj.IDSOUS_TETE;
				bFlag = false;
			}	
			return items;
		}
		
		
		public function getSelectedLinesSum():Number
		{
			return gdrLignes.selectedIndices.length;
		}
		
		public  function clearGridLines():void
		{
			gdrLignes.dataProvider = null;
			gdrLignes.selectedItem = null;
			txtFiltreLignes.text = "";
		}
		
	}
}