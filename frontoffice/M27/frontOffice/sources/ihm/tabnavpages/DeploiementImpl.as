package ihm.tabnavpages
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.ViewStack;
	import mx.controls.ComboBox;
	
	public class DeploiementImpl extends Box
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMS
			//--------------------------------------------------------------------------------------------//
			
		public var cbxTypeDeploy							:ComboBox;
			
		public var vsTypeDeploy								:ViewStack;

		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
			
			
			//--------------------------------------------------------------------------------------------//
			//					DIVERS
			//--------------------------------------------------------------------------------------------//

		[Bindable]public var typeDeploy						:ArrayCollection = new ArrayCollection([
																										{libelle:"Choisir un périmètre", id:0},
																										{libelle:"Toutes les lignes mobiles", id:1},
																										{libelle:"Importer un fichier de lignes", id:2},
																										{libelle:"Utiliser le périmètre suivant", id:3}
																									]);
		
		private var _lastComboboxIndex						:int = 0;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function DeploiementImpl()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					PROTECTED FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		protected function cbxTypeDeployChangeHandler(e:Event):void
		{
			if(cbxTypeDeploy.selectedItem != null)
			{
				_lastComboboxIndex = cbxTypeDeploy.selectedIndex;
				
				vsTypeDeploy.selectedIndex = _lastComboboxIndex;
			}
			else
			{
				cbxTypeDeploy.selectedIndex = _lastComboboxIndex;
				
				cbxTypeDeployChangeHandler(null);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
	}
}