package ihm.popups
{
	import composants.custom.CustomCheckbox;
	import composants.util.DateFunction;
	
	import entities.Campagne;
	import entities.HistoricCampagne;
	
	import events.CampagneEvent;
	import events.HeaderEvent;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import services.CampagneService;
	
	public class PopUpDetailsCampagneImpl extends TitleWindow
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMS
			//--------------------------------------------------------------------------------------------//
		
		public var dgListeLignesCampagnes				:DataGrid;
		
		public var txtFiltre							:TextInput;
		
		public var csCkxSelectAll						:CustomCheckbox;						
		
		public var vbxContent							:VBox;
		
			//--------------------------------------------------------------------------------------------//
			//					POPUPS
			//--------------------------------------------------------------------------------------------//
		
		private var _popUpHistoriqueLigne				:PopUpHistoriqueLigneIhm;
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		
		private var _campagneSrv						:CampagneService = new CampagneService();
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
		
		private var  _currentCampagne					:Campagne = new Campagne();
		
			//--------------------------------------------------------------------------------------------//
			//					DIVERS
			//--------------------------------------------------------------------------------------------//
		
		[Bindable]public var listeLignesCampagne		:ArrayCollection = new ArrayCollection();
		[Bindable]public var listeLignesSelected		:ArrayCollection = new ArrayCollection();

		[Bindable]public var libelle					:String = '';
		[Bindable]public var perimetre					:String = '';
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function PopUpDetailsCampagneImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		public function setInfos(currentCampagne:Campagne):void
		{
			_currentCampagne = new Campagne();
			_currentCampagne = currentCampagne.copyCampagne();
			
			libelle 	= _currentCampagne.LIBELLE;
			perimetre 	= _currentCampagne.CAMPAGNE;
		}
		
		public function getInfos():Campagne
		{
			return _currentCampagne;
		}
		
		public function updateCampagnesSelected(isSelected:Boolean):void
		{
			listeLignesSelected = new ArrayCollection();
			
			if(isSelected)
			{
				for each (var currentHistoriqueCampagne:HistoricCampagne in listeLignesCampagne)
				{
					if(currentHistoriqueCampagne.SELECTED)
						listeLignesSelected.addItem(currentHistoriqueCampagne);
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PROTECTED FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		protected function closeHandler(ce:CloseEvent):void
		{			
			PopUpManager.removePopUp(this);
		}
		
		protected function btnFermerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function dgListeLignesCampagnesItemClickHandler(le:ListEvent):void
		{
			dgListeLignesCampagnes.dispatchEvent(new HeaderEvent(HeaderEvent.SELECTED_CHANGE, true));
		}
		
		protected function dateConvertor(item:Object, column:DataGridColumn):String  
		{  
			var field			:String =  column.dataField; 
			var dateFormated	:String = getDateTime(item[field] as Date);
			
			return dateFormated;
		}
		
		protected function DATE_CREATECompareFunction(obj1:Object, obj2:Object):int 
		{
			return ObjectUtil.dateCompare(obj1.DATE_CREATE, obj2.DATE_CREATE);
		}
		
		protected function filtreChangeHandler(e:Event):void
		{
			if(dgListeLignesCampagnes.dataProvider)
			{
				dgListeLignesCampagnes.dataProvider.filterFunction = filterFunction;
				dgListeLignesCampagnes.dataProvider.refresh();
			}	
		}
		
		protected function btnRefreshClickHandler(me:MouseEvent):void
		{
			getListeLignesCampagne();
		}
		
		protected function csCkxSelectAllChangeHandler(e:Event):void
		{
			var children		:Array = vbxContent.getChildren();
			var lenChildren		:int = children.length;
			
			for each (var currentCustom:Object in children)
			{
				currentCustom.selected = csCkxSelectAll.selected;
			}
			
			filtreChangeHandler(e);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					GESTIONS DES POPUPS
			//--------------------------------------------------------------------------------------------//
		
		private function popUpHistoriqueLigne(currentHistoric:HistoricCampagne):void
		{
			_popUpHistoriqueLigne = new PopUpHistoriqueLigneIhm();
			_popUpHistoriqueLigne.setInfos(currentHistoric);
			
			PopUpManager.addPopUp(_popUpHistoriqueLigne, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpHistoriqueLigne);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					EVENTS
			//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			initListeners();
			
			getListeLignesCampagne();
		}

		private function checkboxHeaderClickedHandler(e:Event):void
		{
			var value:Boolean = e.target.ckbxHeaderSelected.selected;
			
			for each (var currentHistoriqueCampagne:HistoricCampagne in listeLignesCampagne)
			{
				currentHistoriqueCampagne.SELECTED = value;
				
				listeLignesCampagne.itemUpdated(currentHistoriqueCampagne);
			}
			
			updateCampagnesSelected(value);
		}
		
		private function detailsStatutLigneClickedHandler(ce:CampagneEvent):void
		{
			var currentHistoric:HistoricCampagne = ce.target.data as HistoricCampagne;
			
			popUpHistoriqueLigne(currentHistoric);
		}
		
		private function filtreDataChangeHandler(e:Event):void
		{
			var currentObject	:Object = e.currentTarget;
			var children		:Array = vbxContent.getChildren();
			var lenChildren		:int = children.length;
			var cptr			:int = 0;
			
			for each (var currentCustom:Object in children)
			{
				if(currentCustom.selected)
					cptr++;
			}
			
			if(lenChildren == cptr)
				csCkxSelectAll.selected = true;
			else
				csCkxSelectAll.selected = false;
			
			filtreChangeHandler(e);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//
		
		private function initListeners():void
		{
			dgListeLignesCampagnes.addEventListener(HeaderEvent.CHECKBOX_HEADER_CLICKED, checkboxHeaderClickedHandler);
			
			dgListeLignesCampagnes.addEventListener(CampagneEvent.DETAILS_STATUT, detailsStatutLigneClickedHandler);
		}
		
		private function getDateTime(currentDate:Date):String
		{
			var dateTime:String = '';
			
			if(currentDate != null)
			{
				var currentHours	:String = currentDate.hours.toString();
				var currentMinutes	:String = currentDate.minutes.toString();
				var currentSeconds	:String = currentDate.seconds.toString();
				
				if(currentHours.length != 2)
					currentHours = '0' + currentHours;
				
				if(currentMinutes.length != 2)
					currentMinutes = '0' + currentMinutes;
				
				if(currentSeconds.length != 2)
					currentSeconds = '0' + currentSeconds;
				
				dateTime = DateFunction.formatDateAsString(currentDate) + '  ' + currentHours + ':' + currentMinutes + ':' + currentSeconds;
			}
			
			return dateTime;
		}
		
		private function createDynamiqueFilterStatut(values:ArrayCollection):void
		{
			var cstmCkbx:CustomCheckbox;
			
			for each (var currentObject:Object in values)
			{
				cstmCkbx 				= new CustomCheckbox();
				cstmCkbx.filterObject 	= currentObject;
				cstmCkbx.labelField		= 'STATUT';
				cstmCkbx.label			= currentObject.ETAT;
				cstmCkbx.selected		= true;
				cstmCkbx.addEventListener(Event.CHANGE, filtreDataChangeHandler);
				
				vbxContent.addChild(cstmCkbx);
			}
			
			vbxContent.validateNow();
		}

			//--------------------------------------------------------------------------------------------//
			//					APPELS SERVICE
			//--------------------------------------------------------------------------------------------//		
		
		private function getListeLignesCampagne():void
		{
			listeLignesCampagne = new ArrayCollection();
			
			txtFiltre.text = '';
			
			_campagneSrv.getLignesCampagne(_currentCampagne);
			_campagneSrv.addEventListener(CampagneEvent.CAMPAGNELISTELIGNE, listeLignesHandler);
		}
		
		private function listeLignesHandler(ce:CampagneEvent):void
		{
			listeLignesCampagne = _campagneSrv.listeLignesCamapagne;
			
			if(vbxContent.getChildren().length == 0)
				createDynamiqueFilterStatut(_campagneSrv.listeFiltres);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FILTER
			//--------------------------------------------------------------------------------------------//
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;
			
			if(String(item.SOUSTETE).toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1 ||
				DateFunction.formatDateAsString(item.DATE_ENROLL).toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1 ||
				DateFunction.formatDateAsString(item.DATE_STATUT).toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1 ||
				String(item.ETAT).toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1 
			)
			{
				rfilter = true;
			}
			else
			{
				rfilter = false;
			}
			
			return (rfilter && statutSelected(item));
		}
		
		private function statutSelected(item:Object):Boolean
		{
			var rfilter:Boolean = true;
			
			if(!csCkxSelectAll.selected)
			{
				var children		:Array = vbxContent.getChildren();
				var lenChildren		:int = children.length;
				var cptr			:int = 0;
				
				rfilter = false;
				
				for each (var currentCustom:Object in children)
				{
					if(currentCustom.selected && (currentCustom.filterObject.STATUT == item.STATUT))
						rfilter = true;
				}
			}
			
			return rfilter;
		}
			
	}
}