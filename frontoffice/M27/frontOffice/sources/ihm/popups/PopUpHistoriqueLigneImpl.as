package ihm.popups
{
	import composants.util.DateFunction;
	
	import entities.HistoricCampagne;
	
	import events.CampagneEvent;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import services.CampagneService;
	
	public class PopUpHistoriqueLigneImpl extends TitleWindow
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMS
			//--------------------------------------------------------------------------------------------//
		
		public var dgHistoriqueLigne					:DataGrid;
				
			//--------------------------------------------------------------------------------------------//
			//					POPUPS
			//--------------------------------------------------------------------------------------------//
			
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		
		private var _campagneSrv						:CampagneService = new CampagneService();
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
		
		private var  _currentHistorique					:HistoricCampagne = new HistoricCampagne();
		
			//--------------------------------------------------------------------------------------------//
			//					DIVERS
			//--------------------------------------------------------------------------------------------//
		
		[Bindable]public var listeHistorique			:ArrayCollection = new ArrayCollection();
		
		[Bindable]public var ligne						:String = '';

		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function PopUpHistoriqueLigneImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		public function setInfos(currentHistorique:HistoricCampagne):void
		{
			_currentHistorique = new HistoricCampagne();
			_currentHistorique = currentHistorique.copyHistoricCampagne();
			
			ligne = _currentHistorique.SOUSTETE;
		}
		
		public function getInfos():HistoricCampagne
		{
			return _currentHistorique;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PROTECTED FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		protected function closeHandler(ce:CloseEvent):void
		{			
			PopUpManager.removePopUp(this);
		}
		
		protected function btnFermerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function dgHistoriqueLigneItemClickHandler(le:ListEvent):void
		{
			
		}
		
		protected function dateConvertor(item:Object, column:DataGridColumn):String  
		{  
			var field			:String =  column.dataField; 
			var dateFormated	:String = getDateTime(item[field] as Date);
			
			return dateFormated;
		}
		
		protected function DATECompareFunction(obj1:Object, obj2:Object):int 
		{
			return ObjectUtil.dateCompare(obj1.DATE_CREATE, obj2.DATE_CREATE);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					EVENTS
			//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			initListeners();
			
			getHistorique();
		}
		
		private function getDateTime(currentDate:Date):String
		{
			var dateTime:String = '';
			
			if(currentDate != null)
			{
				var currentHours	:String = currentDate.hours.toString();
				var currentMinutes	:String = currentDate.minutes.toString();
				var currentSeconds	:String = currentDate.seconds.toString();
				
				if(currentHours.length != 2)
					currentHours = '0' + currentHours;
				
				if(currentMinutes.length != 2)
					currentMinutes = '0' + currentMinutes;
				
				if(currentSeconds.length != 2)
					currentSeconds = '0' + currentSeconds;
				
				dateTime = DateFunction.formatDateAsString(currentDate) + '  ' + currentHours + ':' + currentMinutes + ':' + currentSeconds;
			}
			
			return dateTime;
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//
		
		private function initListeners():void
		{
			
		}
			
			//--------------------------------------------------------------------------------------------//
			//					APPELS SERVICE
			//--------------------------------------------------------------------------------------------//
		
		private function getHistorique():void
		{
			listeHistorique = new ArrayCollection();
			
			_campagneSrv.getHistoriqueLigneCampagne(_currentHistorique);
			_campagneSrv.addEventListener(CampagneEvent.LIGNEHISTORIQUE, historiqueHandler);
		}
		
		private function historiqueHandler(ce:CampagneEvent):void
		{
			listeHistorique = _campagneSrv.listeHistorique;
		}
		
	}
}