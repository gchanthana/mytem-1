package ihm.popups
{
	import composants.destinataireOrga;
	
	import entities.DataInfos;
	
	import events.OrganisationsEvent;
	import events.PopUpEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.ui.Mouse;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.containers.TitleWindow;
	import mx.containers.ViewStack;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import services.OrganisationsService;
	
	public class PopUpSelectOrganiationImpl extends TitleWindow
	{
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMS
			//--------------------------------------------------------------------------------------------//
		
		public var vsViewOrganisqations						:ViewStack;
		
		public var searchabletreeihm2						:destinataireOrga;
		
		public var dgListTarget								:DataGrid;
		
		public var cbxCliente								:ComboBox;
		
		public var txtFiltre								:TextInput;
		
		public var lblTreeView								:Label;
		public var lblListeView								:Label;
		
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		
		private var _organisationsSrv						:OrganisationsService = new OrganisationsService();
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
		
		
			//--------------------------------------------------------------------------------------------//
			//					DIVERS
			//--------------------------------------------------------------------------------------------//
		
		[Bindable]public var listeOrganisationsClientes		:ArrayCollection = new ArrayCollection();
	
		[Bindable]public var nodeSelected					:Object = null;
			
		public var isListeView								:Boolean = true;
		
		[Bindable]public var textNodeSelected				:String = '';
		
		private var _datainfos								:DataInfos = null;
		
		private var _txtFilter								:String = '';
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function PopUpSelectOrganiationImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		public function setInfos(dataInfos:DataInfos):void
		{
			_datainfos = new DataInfos();
			_datainfos = dataInfos.copyDataInfos();
		}
		
		public function getInfos():DataInfos
		{
			_datainfos				= new DataInfos();
			_datainfos.ISIMPORT 	= false;
			_datainfos.DATA		 	= cbxCliente.selectedItem;
			_datainfos.DATASELECTED = nodeSelected;
			_datainfos.NBR_LIGNE 	= 0;
			_datainfos.STATUT		= 1;
			_datainfos.PERIMETRE	= nodeSelected.LABEL;
			_datainfos.IDPERIMETRE	= nodeSelected.VALUE;
			
			return _datainfos;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PROTECTED FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		protected function closeHandler(ce:CloseEvent):void
		{			
			PopUpManager.removePopUp(this);
		} 
		
		protected function lblViewClickHandler(me:MouseEvent):void
		{
			var currentLabel	:Label = (me.currentTarget as Label);
			
			//INVERSÉ A CAUSE D'OBJETS NULL DANS ORGASTRUCTURE
			if(currentLabel.id == 'lblListeView')
			{
				vsViewOrganisqations.selectedIndex = 1;
			}
			else if(currentLabel.id == 'lblTreeView')
				{
					vsViewOrganisqations.selectedIndex = 0;
				}
			
			updateLabelStyle((vsViewOrganisqations.selectedIndex == 1)?true:false);
		}
		
		protected function cbxClienteChangeHandler(e:Event):void
		{
			if(cbxCliente.selectedItem != null)
				chargeDataToDatagrid(cbxCliente.selectedItem.IDGROUPE_CLIENT);
		}
		
		protected function filtreChangeHandler(e:Event):void
		{			
			findOrganisations();
			
			searchabletreeihm2.txtRechercheOrga.text = txtFiltre.text;
		}
		
		protected function searchClickHandler(e:Event):void
		{
			if(dgListTarget.dataProvider)
			{
				dgListTarget.dataProvider.filterFunction = filterFunction;
				dgListTarget.dataProvider.refresh();
			}
		}
		
		protected function dgClickHandler(me:MouseEvent):void
		{
			if(dgListTarget.selectedItem != null)
				searchabletreeihm2.treePerimetre.setCurrentSelectedIndex(dgListTarget.selectedItem.XML);
			else
				searchabletreeihm2.treePerimetre.setCurrentSelectedIndex(null);
			
			setTextNodeSelected();
		}
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new PopUpEvent(PopUpEvent.POPUP_VALIDATE));
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					EVENTS
			//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			initListeners();
			
			getOrganisationsClientes();
		}
		
		private function itemTreeClickHandler(e:Event):void
		{
			var infosNode	:Object = null;
			var currentNode	:XML = (e.currentTarget as destinataireOrga).treePerimetre.currentSelectedItem as XML;
			
			if(currentNode != null)
			{
				infosNode			= new Object();
				infosNode.VALUE 	= int(currentNode.@VALUE);
				infosNode.NIV 		= int(currentNode.@NIV);
				infosNode.LABEL 	= (currentNode.@LABEL).toString().toLowerCase();
				infosNode.XML 		= currentNode;
			}
			
			setSelectedIndexToDataprovider(infosNode);
		}
		
		private function nodesOrganisationsHandler(e:Event):void
		{
			listeOrganisationsClientes = new ArrayCollection();
			
			if(searchabletreeihm2.treeDataArray != null  && searchabletreeihm2.treeDataArray.length > 0)
			{
				var parents		:XML = searchabletreeihm2.treeDataArray[0];
				var mere		:XMLList = parents.children();
				var nbrNode		:int = mere.length();
				
				for(var i:int = 0;i < nbrNode;i++)
				{
					if ((mere[i] as XML).hasSimpleContent())
						listeOrganisationsClientes.addItem(getInfosNode(mere[i] as XML, ''));
					else
						treatementXML(mere[i], '');
				}
				
				findOrganisations();
				
				this.dispatchEvent(new Event('AUTO_SELECT_NODE'));
			}
		}
		
		private function affectLastDataHandler(e:Event):void
		{
			removeEventListener('AUTO_SELECT_NODE', affectLastDataHandler);
			
			setSelectedIndexToDataprovider(_datainfos.DATASELECTED);
			
			dgListTarget.scrollToIndex(dgListTarget.selectedIndex);
			
			dgListTarget.validateNow();
			
			setTextNodeSelected();
			
			var currentList				:XMLList = (searchabletreeihm2.treePerimetre.dataProvider as XMLListCollection).descendants().(@['VALUE'] == _datainfos.DATASELECTED.XML.@VALUE);
			var currentListDescendants	:XMLList = (searchabletreeihm2.treePerimetre.dataProvider as XMLListCollection).descendants();
			var currentListNodes		:XMLListCollection = (searchabletreeihm2.treePerimetre.dataProvider as XMLListCollection);
			var currentObject			:Object = new Object();
			
			if (currentList[0] == null)
			{
				for each (currentObject in currentListNodes)
				{
					if(currentObject.@VALUE == _datainfos.DATASELECTED.XML.@VALUE)
					{
						searchabletreeihm2.treePerimetre.selectedItem = currentObject;
						searchabletreeihm2.treePerimetre.expandChildrenOf(currentObject, true);
					}
				}
			}
			else
			{
				var currentNode	:Object = currentList[0];
				var parents		:ArrayCollection = new ArrayCollection();
				
				while (currentNode != null && currentNode.@NIV != 1)
				{
					currentNode = currentNode.parent();
					
					parents.addItem(currentNode);
				}
				
				parents.removeItemAt(parents.length - 1);
				
				searchabletreeihm2.treePerimetre.openItems = parents.source;	
				
				searchabletreeihm2.treePerimetre.selectedItem = currentList[0];
				
			}
			
			searchabletreeihm2.treePerimetre.validateNow();
			
			searchabletreeihm2.treePerimetre.scrollToIndex(searchabletreeihm2.treePerimetre.selectedIndex);
		}
		
		private function searchChangeHandler(e:Event):void
		{
			txtFiltre.text = searchabletreeihm2.txtRechercheOrga.text;
			
			findOrganisations();
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//
		
		private function initListeners():void
		{
			addEventListener("finishToRefresh", nodesOrganisationsHandler);
			
			addEventListener('SEARCH_CHANGE', searchChangeHandler);
			
			searchabletreeihm2.addEventListener('ITEM_TREE_CLICK',  itemTreeClickHandler);
		}
		
		private function setTextNodeSelected():void
		{
			if(dgListTarget.selectedItem != null)
			{
				textNodeSelected = dgListTarget.selectedItem.LABEL;
				
				nodeSelected = dgListTarget.selectedItem;
			}
			else
			{
				textNodeSelected = '';
				
				nodeSelected = null;
			}
		}

		private function setSelectedIndexToDataprovider(currentItemSelected:Object):void
		{
			if(currentItemSelected != null)
			{
				var len:int = (dgListTarget.dataProvider as ArrayCollection).length;
				
				for(var i:int = 0;i < len;i++)
				{
					var currentData:Object = (dgListTarget.dataProvider as ArrayCollection)[i];
					
					if(currentData.VALUE == currentItemSelected.VALUE)
					{
						dgListTarget.selectedIndex = i;
						
						break;
					}
				}
			}
			else
			{
				dgListTarget.selectedIndex = -1;
			}
			
			setTextNodeSelected();
		}

		//Récupération de l'XML pour le traitement de conversion
		private function chargeDataToDatagrid(idORganisation:int):void
		{
			if(idORganisation > 0)
			{
				searchabletreeihm2.clearSearch();
				searchabletreeihm2._selectedOrga = idORganisation;
				searchabletreeihm2.refreshTree();
			}
		}
		
		private function selectOrganisationsCliente():void
		{
			var idOrdanisation:int = _datainfos.DATA.IDGROUPE_CLIENT;
			
			for each (var currentObject:Object in _organisationsSrv.listeOrganisations)
			{
				if(currentObject.IDGROUPE_CLIENT == idOrdanisation)
				{
					cbxCliente.selectedItem = currentObject;
					
					chargeDataToDatagrid(cbxCliente.selectedItem.IDGROUPE_CLIENT);
					
					this.addEventListener('AUTO_SELECT_NODE', affectLastDataHandler);
				}
			}
		}
		
		private function expandParents(node:XML):void
		{
			var parents:ArrayCollection = new ArrayCollection();
			
			if (node && !searchabletreeihm2.treePerimetre.isItemOpen(node)) 
			{
				parents.addItem(node);
				
				expandParents(node.parent());
			}
			
			searchabletreeihm2.treePerimetre.openItems = parents.source;
		}

		private function findOrganisations():void
		{
			if(listeOrganisationsClientes != null)
			{
				listeOrganisationsClientes.filterFunction = filterFunction;
				listeOrganisationsClientes.refresh();
			}
		}

		private function treatementXML(parentXml:Object, motherLabel:String):void
		{
			var child:XMLList;
			
			var indx:int = motherLabel.indexOf(';');
			
			if(indx == 0)
				motherLabel = motherLabel.substr(1, motherLabel.length);
			
			if(parentXml as XML)
			{
				if ((parentXml as XML).hasSimpleContent())
				{
					listeOrganisationsClientes.addItem(getInfosNode(parentXml as XML,  motherLabel + ';' + parentXml.@LABEL));
				}
				else
				{
					listeOrganisationsClientes.addItem(getInfosNode(parentXml as XML,  motherLabel));
					
					child = parentXml.children();
					
					treatementXML(child, motherLabel + ';' + (parentXml as XML).@LABEL);
				}
			}
			else if(parentXml as XMLList)
			{
				var lenChild:int = (parentXml as XMLList).length();
				
				for(var i:int = 0;i < lenChild;i++)
				{
					if (((parentXml as XMLList)[i] as XML).hasSimpleContent())
					{
						listeOrganisationsClientes.addItem(getInfosNode((parentXml as XMLList)[i] as XML,  motherLabel + ';' ));
					}
					else
					{
						listeOrganisationsClientes.addItem(getInfosNode(((parentXml as XMLList)[i] as XML),  motherLabel));
						
						child = ((parentXml as XMLList)[i] as XML).children();
						
						treatementXML(child, motherLabel + ';' + ((parentXml as XMLList)[i] as XML).@LABEL);
					}
				}
			}
		}
		
		private function getInfosNode(currentNode:XML, motherLabel:String):Object
		{
			var infosNode:Object 	= new Object();
			infosNode.VALUE 	= currentNode.@VALUE;
			infosNode.NIV 		= currentNode.@NIV;
			infosNode.XML 		= currentNode;
			
			if(motherLabel == '')
				infosNode.LABEL 	= (currentNode.@LABEL).toString().toLowerCase();
			else
				infosNode.LABEL 	= (motherLabel + ';' + currentNode.@LABEL.toString()).replace(';;',	';').toLowerCase();
			
			return infosNode
		}
		
		private function updateInfosCombobox(cbx:ComboBox, currentData:ArrayCollection):void
		{
			var lenData:int = currentData.length;
			
			if(lenData > 0)
			{
				if(lenData == 1)
				{
					cbx.dataProvider 	= currentData;
					cbx.selectedIndex 	= 0;
				}
				else
				{
					cbx.dataProvider 	= currentData;
					cbx.selectedIndex 	= -1;
					
					if(lenData > 8)
						cbx.rowCount = 8;
					else
						cbx.rowCount = lenData;
				}
			}
			else
			{
				cbx.prompt = "Aucune oraganisations";
			}
		}
		
		private function updateLabelStyle(isViewListe:Boolean):void
		{
			if(isViewListe)
			{		
				lblTreeView.mouseChildren = false;
				lblTreeView.setStyle('textDecoration', 'none'); 
				
				lblListeView.mouseChildren = true;
				lblListeView.setStyle('textDecoration', 'underline'); 
			}
			else
			{	
				lblTreeView.mouseChildren = true;
				lblTreeView.setStyle('textDecoration', 'underline'); 
				
				lblListeView.mouseChildren = false;
				lblListeView.setStyle('textDecoration', 'none'); 
			}
		}
		
			//--------------------------------------------------------------------------------------------//
			//					APPELS SERVICE
			//--------------------------------------------------------------------------------------------//
		
		private function getOrganisationsClientes():void
		{			
			_organisationsSrv.getListeOraganisationsCliente();
			_organisationsSrv.addEventListener(OrganisationsEvent.ORGANISATIONSCLIENTES, organisationsClientesHandler);
		}

		private function organisationsClientesHandler(e:Event):void
		{
			updateInfosCombobox(cbxCliente, _organisationsSrv.listeOrganisations);
			
			if(_datainfos.STATUT > 0)
				selectOrganisationsCliente();
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FILTER
			//--------------------------------------------------------------------------------------------//	
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && ((item.LABEL != null && item.LABEL.toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1));
			
			return rfilter;
		}
	}
}