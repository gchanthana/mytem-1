package ihm {
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.Form;
	import mx.containers.FormHeading;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	
	/**
	 * View : Entité du modèle MVC qui écoute les évènements émis par l'entité Model.
	 * Lors de ces évènements cette entité se met à jour pour afficher les valeurs des données concernées.
	 * Cette entité émet des évènements lors des actions utilisateurs. Ils sont écoutés par l'entité Controller.
	 * */
	public class Main extends Box {
		public static const VIEW_COMPLETE:String="VIEW_COMPLETE"; // Event : La est complètement initialisée et prête à afficher les données
		// Event : L'utilisateur souhaite effectuer un test connexion avec le serveur MDM
		public static const UPDATE_WITH_SERVER_CHECK:String="UPDATE_WITH_SERVER_CHECK";
		// Event : L'utilisateur ne souhaite pas effectuer un test connexion avec le serveur MDM
		public static const UPDATE_WITHOUT_SERVER_CHECK:String="UPDATE_WITHOUT_SERVER_CHECK";
		public static const USER_CANCEL_CHANGES:String="USER_CANCEL_CHANGES"; // Event : L'utilisateur annule la modification des infos
		public static const USER_UPDATE_INFOS:String="USER_UPDATE_INFOS"; // Event : L'utilisateur valide la mise à jour des infos
		public static const USER_UNLOCK_INFOS:String="USER_UNLOCK_INFOS"; // Event : L'utilisateur dévérrouille la lecture seule des infos
		public static const USER_RESET_DEFAULT_VALUES:String="USER_RESET_DEFAULT_VALUES"; // Event : L'utilisateur remplace les infos par ceux du serveur MDM par défaut
		
		[Bindable]public var mdmApiKey:TextInput;
		[Bindable]public var mdmForm:Form;
		
		public var mdmFormTitle:FormHeading;
		public var mdmProviders:ComboBox;
		public var mdmUseSSLIn:CheckBox;
		public var mdmIn:TextInput;
		
		public var mdmAdminEmailIn:TextInput;
		public var mdmAdminEmailStatus:Image;
		public var mdmWSUserIn:TextInput;
		public var mdmWSPwdIn:TextInput;
		public var mdmWSPwdConfirmIn:TextInput;
		public var mdmWSPwdConfirmStatus:Image;
		public var mdmEnrollUserIn:TextInput;
		public var mdmEnrollPwdIn:TextInput;
		public var mdmEnrollPwdConfirmIn:TextInput;
		public var mdmEnrollPwdConfirmStatus:Image;
		public var cancelAction:Button;
		public var updateAction:Button;
		public var unlockAction:Button;
		public var resetAction:Button;
		
		protected var _model:MainModel;
		protected var _controller:MainImpl;

		[Embed(source="/assets/noneligibilite24x24.png")]
		protected static const EMAIL_NOT_VALIDATED:Class;
		
		[Embed(source="/assets/eligibilite24x24.png")]
		protected static const EMAIL_VALIDATED:Class;
		
		[Embed(source="/assets/eligibilite24x24.png")]
		protected static const PWD_CONFIRMED:Class;
		
		[Embed(source="/assets/noneligibilite24x24.png")]
		protected static const PWD_NOT_CONFIRMED:Class;
		
		public function Main() {
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,creationComplete);
		}
		
		// Model : Emet des évènements qui sont écoutés par l'entité View
		[Inspectable]
		public function set model(implInstance:MainModel):void {
			_model=implInstance;
		}
		
		// Model : Emet des évènements qui sont écoutés par l'entité View
		public function get model():MainModel {
			return _model
		}
		
		// Controller : Ecoute les évènements émis par l'entité View
		[Inspectable]
		public function set controller(implInstance:MainImpl):void {
			_controller=implInstance;
		}

		/**
		 * Demande à l'utilisateur s'il souhaite ou non effectuer un test de connexion avec le serveur MDM avec la mise à jour des infos
		 * Dispatche un des évènements suivants en fonction du choix de l'utilisateur :
		 * 		- Main.UPDATE_WITH_SERVER_CHECK : L'utilisateur souhaite effectuer un test connexion avec le serveur MDM
		 * 		- Main.UPDATE_WITHOUT_SERVER_CHECK : L'utilisateur ne souhaite pas effectuer un test connexion avec le serveur MDM
		 * */
		public function displayServerCheckBeforeUpdate():void {
			ConsoviewAlert.afficherAlertConfirmation(
				resourceManager.getString("M27","checkMdmServerBeforeUpdate"),resourceManager.getString("M27","mdmUpdateOptions"),validateUserUpdatesOptions
			);
		}
		
		public function displayDeleteConfirmStatus(e:Event):void
		{
			ConsoviewAlert.afficherAlertConfirmation(
				resourceManager.getString("M27","_tes_vous_s_r_de_vouloir_supprimer_les_param_tres__"),resourceManager.getString("M27","Supprimer_configuration"), deleteParams
			);
		}
		

		
		/**
		 * Affiche le statut de validité de la valeur de l'email administrateur MDM.
		 * Si isValid vaut TRUE : Le statut de validité de la valeur est valide
		 * Sinon : Le statut de validité de la valeur est invalide 
		 * */
		public function displayMdmAdminEmailStatus(isValid:Boolean):void {
			mdmAdminEmailStatus.source=(isValid ? Main.EMAIL_VALIDATED:Main.EMAIL_NOT_VALIDATED);
		}
		
		/**
		 * Affiche le statut de confirmation de la valeur du mot de passe WebService
		 * Si isConfirmed vaut TRUE : La valeur est confirmée et la double saisie est désactivée
		 * Sinon : La valeur n'est pas confirmée et l'utilisateur est invité à effectuer la confirmation (Par double saisie)
		 * */
		public function displayPasswordConfirmStatus(isConfirmed:Boolean):void {
			mdmWSPwdConfirmStatus.source=(isConfirmed ? Main.PWD_CONFIRMED:Main.PWD_NOT_CONFIRMED);
			if(! isConfirmed) {
				if(! mdmWSPwdConfirmIn.enabled) // Initie la double saisie
					mdmWSPwdConfirmIn.text="";
				mdmWSPwdConfirmIn.enabled=true;
			} else
				mdmWSPwdConfirmIn.enabled=false;
		}
		
		/**
		 * Affiche le statut de confirmation de la valeur du mot de passe d'enrollment
		 * Si isConfirmed vaut TRUE : La valeur est confirmée et la double saisie est désactivée
		 * Sinon : La valeur n'est pas confirmée et l'utilisateur est invité à effectuer la confirmation (Par double saisie) 
		 * */
		public function displayEnrollPasswordConfirmStatus(isConfirmed:Boolean):void {
			mdmEnrollPwdConfirmStatus.source=(isConfirmed ? Main.PWD_CONFIRMED:Main.PWD_NOT_CONFIRMED);
			if(! isConfirmed) {
				if(! mdmEnrollPwdConfirmIn.enabled) // Initie la double saisie
					mdmEnrollPwdConfirmIn.text="";
				mdmEnrollPwdConfirmIn.enabled=true;
			} else
				mdmEnrollPwdConfirmIn.enabled=false;
		}
		
		/**
		 * Permet d'activer/désactiver le controle de validation des modifications
		 * */
		public function set updateEnabled(status:Boolean):void {
			updateAction.enabled=status;
		}
		
		/**
		 * Permet d'activer/désactiver la saisie de la clé API
		 * */
		public function set apiKeyEnabled(status:Boolean):void {
			mdmApiKey.enabled=status;
		}
		
		/**
		 * Permet d'activer/désactiver le controle d'annulation des modifications
		 * Si status vaut TRUE : Active le controle d'annulation des modifications
		 * Sinon : Désactive le controle d'annulation des modifications
		 * */
		public function set cancelEnabled(status:Boolean):void {
			cancelAction.enabled=status;
		}
		
		/**
		 * Permet d'activer/désactiver le controle de réinitialisation des valeurs
		 * Si status vaut TRUE : Active le controle de réinitialisation
		 * Sinon : Désactive le controle de réinitialisation
		 * */
		public function set resetValuesEnabled(status:Boolean):void {
			resetAction.enabled=status;
		}

		/**
		 * Permet d'activer/désactiver le controle de déverrouillage des infos
		 * Si status vaut TRUE : Active le controle de déverrouillage des infos
		 * Sinon : Désactive le controle de déverrouillage des infos
		 * */
		public function set unlockEnabled(status:Boolean):void {
			unlockAction.enabled=status;
		}
		
		/**
		 * Permet de verrouiller ou déverrouiller les infos afin de pouvoir les modifier
		 * Si enable vaut TRUE : Verrouille les infos
		 * Sinon : Déverrouille les infos
		 * */
		public function set lockInfos(enable:Boolean):void {
			mdmForm.enabled=(! enable);
		}
		
		/**
		 * Affiche dans une fenetre un message d'erreur générique
		 * */
		public function displayErrorMessage():void {
			ConsoviewAlert.afficherError(resourceManager.getString("M27","generic_error_msg"),resourceManager.getString("M27","Erreur"));
		}
		
		/**
		 * Valide les options d'enregistrement choisis par l'utilisateur
		 * */
		protected function validateUserUpdatesOptions(eventObject:CloseEvent):void {
			var eventType:String=(eventObject.detail == Alert.OK ? Main.UPDATE_WITH_SERVER_CHECK:Main.UPDATE_WITHOUT_SERVER_CHECK);
			dispatchEvent(new Event(eventType)); // Evènement correspond au choix du type de mise à jour à effectuer
		}
		
		/**
		 * Listener des actions utilisateurs sur les controles.
		 * Met à jour les libellés liés au controle concerné puis dispatche l'évènement correspondant à l'action utilisateur.
		 * */
		protected function userControlAction(eventObject:MouseEvent):void {
			var eventType:String=null;
			switch(eventObject.target) {
				case unlockAction:
					eventType=Main.USER_UNLOCK_INFOS;
					break;
				case cancelAction:
					eventType=Main.USER_CANCEL_CHANGES;
					break;
				case updateAction:
					eventType=Main.USER_UPDATE_INFOS;
					break;
			}
			if(eventType)
				dispatchEvent(new Event(eventType));
		}
		
		/**
		 * Listener des modifications utilisateur sur les valeurs des infos. Cette méthode effectue les actions suivantes dans l'ordre :
		 * 		- Modifie la valeur de la propriété concernée via l'entité Model.
		 * 		- Met à jour l'affichage de la valeur de l'infos qui a été modifiée
		 * Les mots de passes ne sont pas affichés (caractères masqués) que lors de leur modification. Voir displayServerInfos()
		 * */
		protected function userDataAction(eventObject:Event):void {
			var eventType:String=null;
			if(eventObject) {
				switch(eventObject.target) {
					case mdmUseSSLIn:
						model.setCurrentMdmServerProperty("USE_SSL",mdmUseSSLIn.selected);
						break;
					case mdmIn:					
						model.setCurrentMdmServerProperty("MDM",mdmIn.text);
						break;						
					case mdmApiKey:
						model.setCurrentMdmServerProperty("TOKEN",mdmApiKey.text);
						break;
					case mdmAdminEmailIn:
						model.setCurrentMdmServerProperty("ADMIN_EMAIL",mdmAdminEmailIn.text);
						break;
					case mdmWSUserIn:
						model.setCurrentMdmServerProperty("USERNAME",mdmWSUserIn.text);
						break;
					case mdmWSPwdIn: // Initie la double saisie du mot de passe
						mdmWSPwdConfirmIn.text="";
						model.setCurrentMdmServerProperty("PASSWORD",mdmWSPwdIn.text);
						break;
					case mdmWSPwdConfirmIn: // Confirme la double saisie du mot de passe
						model.confirmMdmPassword("PASSWORD",mdmWSPwdConfirmIn.text);
						break;
					case mdmEnrollUserIn:
						model.setCurrentMdmServerProperty("ENROLL_USERNAME",mdmEnrollUserIn.text);
						break;
					case mdmEnrollPwdIn: // Initie la double saisie du mot de passe
						mdmEnrollPwdConfirmIn.text="";
						model.setCurrentMdmServerProperty("ENROLL_PASSWORD",mdmEnrollPwdIn.text);
						break;
					case mdmEnrollPwdConfirmIn: // Confirme la double saisie du mot de passe
						model.confirmMdmPassword("ENROLL_PASSWORD",mdmEnrollPwdConfirmIn.text);
						break;
					case resetAction:
						model.setCurrentMdmServerProperty("IS_DEFAULT",true);
						break;
					case mdmProviders:
						model.setCurrentMdmServerProperty("MDM_TYPE",mdmProviders.selectedItem.VALUE);
						apiKeyEnabled = model.isApiKeyCompulsory(model.getCurrentMdmServerProperty("MDM_TYPE") as String);
						mdmApiKey.text = ! mdmApiKey.enabled ? "" : model.getCurrentMdmServerProperty("TOKEN") as String;
						break;
				}
				// Mise à jour de l'affichage : Titre du formulaire des infos MDM
				var mdmIsDefault:Boolean=Boolean(model.getCurrentMdmServerProperty("IS_DEFAULT"));
				mdmFormTitle.label=resourceManager.getString("M27","mdmForm")+(mdmIsDefault ? " "+resourceManager.getString("M27","mdmDefaultServer"):"");
			}
		}
		
		/**
		 * Listener : Met à jour l'affichage des infos serveur MDM à chaque fois que l'entité Model dispatche un des évènements suivants :
		 * MainModel.MDM_SERVER_INFOS, MainModel.MDM_SERVER_INFOS_UPDATED,
		 * MainModel.MDM_SERVER_INFOS_RESETED_NOT_CHANGED, MainModel.MDM_SERVER_INFOS_RESETED
		 * Les mots de passes ne sont pas affichés (caractères masqués) que lors de leur modification
		 * */
		protected function displayServerInfos(eventObject:Event):void {
			// Séléction du provider MDM
			var providers:ArrayCollection=model.getMdmProviders();
			var anIndexWasSelected:Boolean = false;
			if(providers != null) {
				mdmProviders.dataProvider=providers;
				for(var i:int=0; i < providers.length; i++) 
				{
					var provider:Object=providers[i];
					var modelMdmProvider:String=model.getCurrentMdmServerProperty("MDM_TYPE") as String;
					if(ObjectUtil.stringCompare(provider.VALUE,modelMdmProvider,true) == 0)
					{
						mdmProviders.selectedIndex=i;
						anIndexWasSelected = true;
					}
					apiKeyEnabled = model.isApiKeyCompulsory(modelMdmProvider);
				}
				if (!anIndexWasSelected)
					mdmProviders.selectedIndex=-1;
			} else {
				mdmProviders.dataProvider=new ArrayCollection([{NAME:"ERROR",VALUE:"ERROR"}]);
				mdmProviders.selectedIndex=0;
			}
			// Alimentation du formulaire
			var mdmIsDefault:Boolean=Boolean(model.getCurrentMdmServerProperty("IS_DEFAULT"));
			mdmFormTitle.label=resourceManager.getString("M27","mdmForm")+(mdmIsDefault ? " "+resourceManager.getString("M27","mdmDefaultServer"):"");			
			mdmUseSSLIn.selected=Boolean(model.getCurrentMdmServerProperty("USE_SSL"));			
			mdmIn.text=model.getCurrentMdmServerProperty("MDM") as String;
			mdmApiKey.text=model.getCurrentMdmServerProperty("TOKEN") as String;
			mdmAdminEmailIn.text=model.getCurrentMdmServerProperty("ADMIN_EMAIL") as String;
			mdmWSUserIn.text=model.getCurrentMdmServerProperty("USERNAME") as String;
			mdmEnrollUserIn.text=model.getCurrentMdmServerProperty("ENROLL_USERNAME") as String;
			mdmWSPwdIn.text=mdmWSPwdConfirmIn.text=model.getCurrentMdmServerProperty("PASSWORD") as String; // Double saisie du mot de passe
			mdmEnrollPwdIn.text=mdmEnrollPwdConfirmIn.text=model.getCurrentMdmServerProperty("ENROLL_PASSWORD") as String; // Double saisie du mot de passe
			
			// Affichage des infos concernant l'enregistrement des modifications
			if(eventObject && (eventObject.type == MainModel.MDM_SERVER_INFOS_UPDATED)) {
				var mdmInfosUpdatedMsg:String=resourceManager.getString("M27","mdmInfosUpdatedMsg");
				var connectionTestMsg:String=model.getCurrentMdmServerProperty("TEST_MSG") as String;
				if(connectionTestMsg != null) { // Si l'option "Test de connexion" a été choisie
					var connectionTestStatus:String=resourceManager.getString("M27","IS_TEST_OK_MSG");
					if(! Boolean(model.getCurrentMdmServerProperty("IS_TEST_OK"))) // Si le test de connexion a échoué
						connectionTestStatus=resourceManager.getString("M27","IS_TEST_NOT_OK_MSG");
					mdmInfosUpdatedMsg=connectionTestStatus+"\n"+mdmInfosUpdatedMsg;
				}
				// Mise à jour de l'affichage concernant le type de modification qui a été effectué
				ConsoviewAlert.afficherSimpleAlert(mdmInfosUpdatedMsg,resourceManager.getString("M27",mdmIsDefault ? "mdmInfosResetUpdatedTitle":"mdmInfosUpdatedTitle"));
			}
		}
		
		protected function registerModelListeners():void {
			model.addEventListener(MainModel.MDM_SERVER_INFOS,displayServerInfos);
			model.addEventListener(MainModel.MDM_SERVER_INFOS_RESETED_NOT_CHANGED,displayServerInfos);
			model.addEventListener(MainModel.MDM_SERVER_INFOS_RESETED,displayServerInfos);
			model.addEventListener(MainModel.MDM_SERVER_INFOS_UPDATED,displayServerInfos);
		}
		
		protected function registerListeners():void {
			// Listeners des actions utilisateur sur les controles
			unlockAction.addEventListener(MouseEvent.CLICK,userControlAction);
			cancelAction.addEventListener(MouseEvent.CLICK,userControlAction);
			updateAction.addEventListener(MouseEvent.CLICK,userControlAction);
			// Listeners des controles de modifications des infos
			//resetAction.addEventListener(MouseEvent.CLICK,resetFields);
			resetAction.addEventListener(MouseEvent.CLICK,displayDeleteConfirmStatus);
			mdmUseSSLIn.addEventListener(Event.CHANGE,userDataAction);
			mdmIn.addEventListener(Event.CHANGE,userDataAction);
			mdmApiKey.addEventListener(Event.CHANGE,userDataAction);
			mdmAdminEmailIn.addEventListener(Event.CHANGE,userDataAction);			
			mdmWSUserIn.addEventListener(Event.CHANGE,userDataAction);
			mdmEnrollUserIn.addEventListener(Event.CHANGE,userDataAction);
			mdmWSPwdIn.addEventListener(Event.CHANGE,userDataAction);
			mdmWSPwdConfirmIn.addEventListener(Event.CHANGE,userDataAction);
			mdmEnrollPwdIn.addEventListener(Event.CHANGE,userDataAction);
			mdmEnrollPwdConfirmIn.addEventListener(Event.CHANGE,userDataAction);
			mdmProviders.addEventListener(Event.CHANGE,userDataAction);
		}
		
		protected function resetFields(e:Event):void
		{
			mdmProviders.selectedIndex 	= -1;
			/*mdmIn.text 					= " ";
			mdmApiKey.text 				= "";
			mdmAdminEmailIn.text 		= " ";
			mdmWSUserIn.text 			= " ";
			mdmWSPwdIn.text 			= " ";
			mdmWSPwdConfirmIn.text 		= " ";
			mdmEnrollUserIn.text 		= " ";
			mdmEnrollPwdIn.text 		= " ";
			mdmEnrollPwdConfirmIn.text 	= " ";*/
			mdmUseSSLIn.dispatchEvent(new Event(Event.CHANGE));
			mdmIn.dispatchEvent(new Event(Event.CHANGE));
			mdmApiKey.dispatchEvent(new Event(Event.CHANGE));
			mdmAdminEmailIn.dispatchEvent(new Event(Event.CHANGE));			
			mdmWSUserIn.dispatchEvent(new Event(Event.CHANGE));
			mdmEnrollUserIn.dispatchEvent(new Event(Event.CHANGE));
			mdmWSPwdIn.dispatchEvent(new Event(Event.CHANGE));
			mdmWSPwdConfirmIn.dispatchEvent(new Event(Event.CHANGE));
			mdmEnrollPwdIn.dispatchEvent(new Event(Event.CHANGE));
			mdmEnrollPwdConfirmIn.dispatchEvent(new Event(Event.CHANGE));
			updateAction.enabled		= true;
		}
		
		public function deleteParams(e:CloseEvent):void
		{
			if (e.detail == 4)
			{
				model.setCurrentMdmServerProperty("IS_DEFAULT",true);
				// Mise à jour de l'affichage : Titre du formulaire des infos MDM
				var mdmIsDefault:Boolean=Boolean(model.getCurrentMdmServerProperty("IS_DEFAULT"));
				mdmFormTitle.label=resourceManager.getString("M27","mdmForm")+(mdmIsDefault ? " "+resourceManager.getString("M27","mdmDefaultServer"):"");
			}
		}
		
		/**
		 * Quand l'IHM est complètement initialisée alors :
		 * 		- Les listeners sont configurés
		 * 		- L'entité Controller est définie
		 * 		- Les données sont récupérées à partir de l'entité Model
		 * */
		protected function creationComplete(eventObject:FlexEvent):void {
			registerListeners(); // Listeners utilisateur
			registerModelListeners(); // Listener sur l'entité Model
			_controller.view=this; // Controller
			model.loadMdmServerInfos(); // Récupération des données
		}
	}
}