package ihm
{
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import entities.Campagne;
	import entities.DataInfos;
	
	import events.CampagneEvent;
	import events.HeaderEvent;
	import events.PopUpEvent;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import ihm.popups.PopUpDetailsCampagneIhm;
	import ihm.popups.PopUpTypeCampagneIhm;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import services.CampagneService;
	
	public class MainGestionMDMImpl extends Box
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMS
			//--------------------------------------------------------------------------------------------//
		
		public var dgListeCampagnes						:DataGrid;
		
		public var txtFiltre							:TextInput;

		
			//--------------------------------------------------------------------------------------------//
			//					POPUPS
			//--------------------------------------------------------------------------------------------//
			
		private var _popUpTypeCampagne					:PopUpTypeCampagneIhm;
		
		private var _popUpDetailsCampagne				:PopUpDetailsCampagneIhm;
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		
		private var _campagneSrv						:CampagneService = new CampagneService();
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
			
			//--------------------------------------------------------------------------------------------//
			//					DIVERS
			//--------------------------------------------------------------------------------------------//
		
		[Bindable]public var listeCampagnes				:ArrayCollection = new ArrayCollection();
		[Bindable]public var listeCampagnesSelected		:ArrayCollection = new ArrayCollection();

		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function MainGestionMDMImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		public function updateCampagnesSelected(isSelected:Boolean):void
		{
			listeCampagnesSelected = new ArrayCollection();
			
			if(isSelected)
			{
				for each (var currentCampagne:Campagne in listeCampagnes)
				{
					if(currentCampagne.SELECTED)
						listeCampagnesSelected.addItem(currentCampagne);
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PROTECTED FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		protected function btnNewCampagneClickHandler(me:MouseEvent):void
		{
			popUpTypeCampagne();
		}
		
		protected function btnRefreshClickHandler(me:MouseEvent):void
		{
			getCampagnes();
		}

		protected function filtreChangeHandler(e:Event):void
		{
			if(dgListeCampagnes.dataProvider)
			{
				dgListeCampagnes.dataProvider.filterFunction = filterFunction;
				dgListeCampagnes.dataProvider.refresh();
			}	
		}
		
		protected function dgListeCampagnesItemClickHandler(le:ListEvent):void
		{
			dgListeCampagnes.dispatchEvent(new HeaderEvent(HeaderEvent.SELECTED_CHANGE, true));
		}
		
		protected function dateConvertor(item:Object, column:DataGridColumn):String  
		{  
			var field			:String =  column.dataField; 
			var dateFormated	:String = getDateTime(item[field] as Date);
			
			if(item == null) return '';
			
			if(field == 'DATE_SEND' && item[field] == null)
				dateFormated = "--";/* Non executée*/

			return dateFormated;
		}

		protected function DATE_CREATECompareFunction(obj1:Object, obj2:Object):int 
		{
			return ObjectUtil.dateCompare(obj1.DATE_CREATE, obj2.DATE_CREATE);
		}
		
		protected function DATE_SENDCompareFunction(obj1:Object, obj2:Object):int 
		{
			return ObjectUtil.dateCompare(obj1.DATE_SEND, obj2.DATE_SEND);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					EVENTS
			//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			initListeners();
			
			getCampagnes();
		}
		
		private function checkboxHeaderClickedHandler(e:Event):void
		{
			var value:Boolean = e.target.ckbxHeaderSelected.selected;
			
			for each (var currentCampagne:Campagne in listeCampagnes)
			{
				currentCampagne.SELECTED = value;
				
				listeCampagnes.itemUpdated(currentCampagne);
			}
			
			updateCampagnesSelected(value);
		}
		
		private function launchCampagneClickedHandler(ce:CampagneEvent):void
		{
			var currentData:Campagne = ce.target.data as Campagne;
			
			if(currentData != null)
				sendCampagne(currentData);
			else
				ConsoviewAlert.afficherAlertInfo("Impossible d'envoyer la campagne.", 'Consoview', null);
		}
		
		private function datailsCampagneClickedHandler(ce:CampagneEvent):void
		{
			var currentData:Campagne = ce.target.data as Campagne;
			
			popUpDetailsCampagne(currentData);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//

		private function initListeners():void
		{
			dgListeCampagnes.addEventListener(HeaderEvent.CHECKBOX_HEADER_CLICKED, checkboxHeaderClickedHandler);
			
			dgListeCampagnes.addEventListener(CampagneEvent.LAUNCH_CAMPAGNE, launchCampagneClickedHandler);
			dgListeCampagnes.addEventListener(CampagneEvent.DETAILS_CAMPAGNE, datailsCampagneClickedHandler);
		}

		private function formatCampagneToDataInfos(currentCampagneSelected:Campagne):DataInfos
		{
			var currentDataInfos:DataInfos 		= new DataInfos();
				currentDataInfos.LIBELLE		= currentCampagneSelected.LIBELLE;
				currentDataInfos.DESCRIPTION	= currentCampagneSelected.DESCRIPTION;
				currentDataInfos.PERIMETRE		= currentCampagneSelected.REFCAMPAGNE;
				currentDataInfos.UUID			= currentCampagneSelected.UUIDFILE;
				currentDataInfos.IDPERIMETRE	= currentCampagneSelected.IDPERIMETRE;
				currentDataInfos.IDETAT			= currentCampagneSelected.IDSTATUT;
				currentDataInfos.IDCAMPAGNE		= currentCampagneSelected.IDCAMPAGNE;
			
			return currentDataInfos;
		}

		private function getDateTime(currentDate:Date):String
		{
			var dateTime:String = '';
			
			if(currentDate != null)
			{
				var currentHours	:String = currentDate.hours.toString();
				var currentMinutes	:String = currentDate.minutes.toString();
				var currentSeconds	:String = currentDate.seconds.toString();
				
				if(currentHours.length != 2)
					currentHours = '0' + currentHours;
				
				if(currentMinutes.length != 2)
					currentMinutes = '0' + currentMinutes;
				
				if(currentSeconds.length != 2)
					currentSeconds = '0' + currentSeconds;
				
				dateTime = DateFunction.formatDateAsString(currentDate) + '  ' + currentHours + ':' + currentMinutes + ':' + currentSeconds;
			}
			
			return dateTime;
		}
		
			//--------------------------------------------------------------------------------------------//
			//					APPELS SERVICE
			//--------------------------------------------------------------------------------------------//
		
		private function getCampagnes():void
		{
			listeCampagnes = new ArrayCollection();
			
			dgListeCampagnesItemClickHandler(null);
			
			_campagneSrv.getCampagnes();
			_campagneSrv.addEventListener(CampagneEvent.LISTECAMPAGNE, campagnesHandler);
		}
		
		private function sendCampagne(currentCampagneSelected:Campagne):void
		{
			var currentDataInfos:DataInfos = formatCampagneToDataInfos(currentCampagneSelected);
			
			_campagneSrv.sendCampagne(currentDataInfos);
			_campagneSrv.addEventListener(CampagneEvent.CAMPAGNESENT, sendCampagneHandler);
		}
		
		private function campagnesHandler(ce:CampagneEvent):void
		{
			listeCampagnes = _campagneSrv.listeCampagnes;
		}
		
		private function sendCampagneHandler(ce:CampagneEvent):void
		{
			getCampagnes();
			
			ConsoviewAlert.afficherOKImage("Campagne envoyer", this);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					GESTIONS DES POPUPS
			//--------------------------------------------------------------------------------------------//
		
		private function popUpTypeCampagne():void
		{
			_popUpTypeCampagne 			 	= new PopUpTypeCampagneIhm();
			_popUpTypeCampagne.addEventListener(PopUpEvent.POPUP_VALIDATE, popUpTypeCampagneValidateHandler);
			
			PopUpManager.addPopUp(_popUpTypeCampagne, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpTypeCampagne);
		}
		
		private function popUpTypeCampagneValidateHandler(pue:PopUpEvent):void
		{			
			getCampagnes();
			
			PopUpManager.removePopUp(_popUpTypeCampagne);
		}
		
		private function popUpDetailsCampagne(currentCampagne:Campagne):void
		{
			_popUpDetailsCampagne = new PopUpDetailsCampagneIhm();
			_popUpDetailsCampagne.setInfos(currentCampagne);
			
			PopUpManager.addPopUp(_popUpDetailsCampagne, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpDetailsCampagne);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FILTER
			//--------------------------------------------------------------------------------------------//
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;
			
			if(String(item.LIBELLE).toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1 ||
				String(item.REFCAMPAGNE).toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1 ||
				DateFunction.formatDateAsString(item.DATE_CREATE).toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1 ||
				DateFunction.formatDateAsString(item.DATE_SEND).toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1 
				)
			{
				rfilter = true;
			}
			else
			{
				rfilter = false;
			}
			
			return rfilter;
		}

	}
}