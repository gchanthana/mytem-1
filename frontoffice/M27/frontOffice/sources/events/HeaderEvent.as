package events
{
	import flash.events.Event;

	public class HeaderEvent extends Event
	{
		public static const CHECKBOX_HEADER_CLICKED		:String = 'CHECKBOX_HEADER_CLICKED'; 
		public static const SELECTED_CHANGE				:String = 'SELECTED_CHANGE';

				
		public function HeaderEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new HeaderEvent(type, bubbles, cancelable);
		}
		
	}
}