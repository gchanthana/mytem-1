package events
{
	import flash.events.Event;
	
	public class PopUpEvent extends Event
	{

		public static const POPUP_VALIDATE					:String = "POPUP_VALIDATE";
		public static const POPUP_CLOSE						:String = "POPUP_CLOSE";
		
		
		public function PopUpEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new PopUpEvent(type, bubbles, cancelable);
		}

	}
}
