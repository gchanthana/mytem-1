package entities
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class DataInfos
	{
		
		public var DATA								:Object = new Object();
		public var DATASELECTED						:Object = new Object();
		
		public var ISIMPORT							:Boolean = true;
		
		public var LIBELLE							:String = '';
		public var PERIMETRE						:String = '';
		public var USER								:String = '';
		public var UUID								:String = '';
		public var DESCRIPTION						:String = '';
		
		public var DATE_CREATE						:Date = new Date();
		
		public var IDPERIMETRE						:int = 0;
		public var IDUSER							:int = 0;
		public var NBR_LIGNE						:int = 0;
		public var STATUT							:int = 0;
		public var IDETAT							:int = 0;
		public var IDCAMPAGNE						:int = 0;
		public var IDDEPLOY							:int = 0;
		
		public function DataInfos()
		{
		}
		
		public function copyDataInfos():DataInfos
		{
			var cpyDataInfos:DataInfos 		= new DataInfos();
				cpyDataInfos.DATA 			= this.DATA;
				cpyDataInfos.DATASELECTED 	= this.DATASELECTED;
				cpyDataInfos.ISIMPORT 		= this.ISIMPORT;
				cpyDataInfos.USER			= this.USER;
				cpyDataInfos.LIBELLE 		= this.LIBELLE;
				cpyDataInfos.PERIMETRE		= this.PERIMETRE;
				cpyDataInfos.USER 			= this.USER;
				cpyDataInfos.DATE_CREATE 	= this.DATE_CREATE;
				cpyDataInfos.IDPERIMETRE 	= this.IDPERIMETRE;
				cpyDataInfos.IDUSER 		= this.IDUSER;
				cpyDataInfos.NBR_LIGNE 		= this.NBR_LIGNE;
				cpyDataInfos.STATUT 		= this.STATUT;
				cpyDataInfos.IDETAT 		= this.IDETAT;
				cpyDataInfos.UUID 			= this.UUID;
				cpyDataInfos.DESCRIPTION 	= this.DESCRIPTION;
				cpyDataInfos.IDDEPLOY 		= this.IDDEPLOY;

			return cpyDataInfos;
		}
		
		public static function mappingDataInfos(value:Object):DataInfos
		{
			var newDataInfos:DataInfos 		= new DataInfos();
				newDataInfos.DATA 			= value.DATA;
				newDataInfos.DATASELECTED 	= value.DATASELECTED;
				newDataInfos.ISIMPORT 		= value.ISIMPORT;
				newDataInfos.USER			= value.USER;
				newDataInfos.LIBELLE 		= value.LIBELLE;
				newDataInfos.PERIMETRE		= value.PERIMETRE;
				newDataInfos.USER 			= value.USER;
				newDataInfos.DATE_CREATE 	= value.DATE_CREATE;
				newDataInfos.IDPERIMETRE 	= value.IDPERIMETRE;
				newDataInfos.IDUSER 		= value.IDUSER;
				newDataInfos.NBR_LIGNE 		= value.NBR_LIGNE;
			
			return newDataInfos;
		}
		
		public static function formatDataInfos(values:ArrayCollection):ArrayCollection
		{
			var dataInfos	:ArrayCollection = new ArrayCollection();
			var len			:int = values.length;
			
			for(var i:int = 1;i < len;i++)
			{
				dataInfos.addItem(mappingDataInfos(values[i]));
			}
			
			return dataInfos;
		}
	}
}