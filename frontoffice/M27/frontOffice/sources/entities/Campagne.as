package entities
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class Campagne
	{
		
		public var SELECTED							:Boolean = true;
		
		public var LIBELLE							:String = '';
		public var DESCRIPTION						:String = '';
		public var TYPE								:String = '';
		public var USER								:String = '';
		public var STATUT							:String = '';
		public var UUIDFILE							:String = '';
		public var REFCAMPAGNE						:String = '';
		public var PREFIXES							:String = '';
		public var CAMPAGNE							:String = '';
		
		public var DATE_CREATE						:Date = null;
		public var DATE_SEND						:Date = null;
		
		public var IDCAMPAGNE						:int = 0;
		public var IDTYPE							:int = 0;
		public var IDUSER							:int = 0;
		public var IDSTATUT							:int = 0;
		public var NBR_LIGNE						:int = 0;
		public var TER_COMPATIBLE					:int = 0;
		public var IDPERIMETRE						:int = 0;

		public function Campagne()
		{
		}
		
		public function copyCampagne():Campagne
		{
			var cpyCampagne:Campagne 		= new Campagne();
				cpyCampagne.SELECTED 		= this.SELECTED;
				cpyCampagne.LIBELLE 		= this.LIBELLE;
				cpyCampagne.TYPE 			= this.TYPE;
				cpyCampagne.USER			= this.USER;
				cpyCampagne.STATUT 			= this.STATUT;
				cpyCampagne.DATE_CREATE 	= this.DATE_CREATE;
				cpyCampagne.DATE_SEND 		= this.DATE_SEND;
				cpyCampagne.IDCAMPAGNE 		= this.IDCAMPAGNE;
				cpyCampagne.IDTYPE 			= this.IDTYPE;
				cpyCampagne.IDUSER 			= this.IDUSER;
				cpyCampagne.IDSTATUT 		= this.IDSTATUT;
				cpyCampagne.NBR_LIGNE 		= this.NBR_LIGNE;
				cpyCampagne.IDPERIMETRE 	= this.IDPERIMETRE;
				cpyCampagne.UUIDFILE 		= this.UUIDFILE;
				cpyCampagne.REFCAMPAGNE 	= this.REFCAMPAGNE;
				cpyCampagne.PREFIXES 		= this.PREFIXES;
				cpyCampagne.CAMPAGNE 		= this.CAMPAGNE;
				cpyCampagne.TER_COMPATIBLE 	= this.TER_COMPATIBLE;

			return cpyCampagne;
		}
		
		public static function mappingCampagne(value:Object):Campagne
		{
			var newCampagne:Campagne 		= new Campagne();
				newCampagne.SELECTED 		= value.SELECTED;
				newCampagne.LIBELLE 		= value.LIBELLE_CAMPAGNE;
				newCampagne.TYPE 			= value.TYPE;
				newCampagne.USER			= value.CREATEUR_CAMPAGNE;
				newCampagne.STATUT 			= value.STATUT;
				newCampagne.DATE_CREATE 	= value.DATE_CREATION;
				newCampagne.DATE_SEND 		= value.DATE_LANCEMENT;
				newCampagne.IDCAMPAGNE 		= value.IDMDM_CAMPAGNE;
				newCampagne.IDTYPE 			= value.IDTYPE;
				newCampagne.IDUSER 			= value.APP_LOGINID_CREATE;
				newCampagne.NBR_LIGNE 		= value.NBLIGNE;				
				newCampagne.IDPERIMETRE 	= value.PERIMETRE_CAMPAGNE;
				newCampagne.UUIDFILE 		= value.UUID_FICHIER;
				newCampagne.REFCAMPAGNE 	= value.REFERENCE_CAMPAGNE;
				newCampagne.TER_COMPATIBLE 	= value.TER_COMPATIBLE;
				
			if(newCampagne.DATE_SEND != null)
				newCampagne.IDSTATUT = 1;
			else
				newCampagne.IDSTATUT = 0;
			
			if(newCampagne.IDPERIMETRE > 0)
			{
				if(newCampagne.IDPERIMETRE == CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX)
					newCampagne.PREFIXES = "Tous périmètres : ";
				else
					newCampagne.PREFIXES = "Périmètre : ";
			}
			else
				newCampagne.PREFIXES = "Fichier : ";

			newCampagne.CAMPAGNE = newCampagne.PREFIXES + newCampagne.REFCAMPAGNE;
				
			return newCampagne;
		}
		
		public static function formatCampagne(values:ArrayCollection):ArrayCollection
		{
			var campagnes	:ArrayCollection = new ArrayCollection();
			var len			:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				campagnes.addItem(mappingCampagne(values[i]));
			}
			
			return campagnes;
		}
	}
}