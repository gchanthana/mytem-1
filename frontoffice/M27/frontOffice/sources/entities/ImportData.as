package entities
{
	import mx.collections.ArrayCollection;

	public class ImportData
	{
				
		public var SELECTED							:Boolean = true;
		public var ISVALIDATE						:Boolean = true;

		public var SOUSTETE							:String = '';
		public var EMPLOYE							:String = '';
		public var MATRICULE						:String = '';
		
		public var IDGENERATE						:int = 0;
		public var IDEMPLOYE						:int = 0;
		public var IDSOUSTETE						:int = 0;
		public var ETAT								:int = 0;

		
		public function ImportData()
		{
		}
		
		public function copyImportData(value:Object):ImportData
		{
			var cpyimpData:ImportData 	= new ImportData();
				cpyimpData.SOUSTETE 	= this.SOUSTETE;
				cpyimpData.EMPLOYE 		= this.EMPLOYE;
				cpyimpData.MATRICULE 	= this.MATRICULE;
				cpyimpData.IDEMPLOYE	= this.IDEMPLOYE;
				cpyimpData.IDSOUSTETE 	= this.IDSOUSTETE;
				cpyimpData.ETAT 		= this.ETAT;
				cpyimpData.IDGENERATE 	= this.IDGENERATE;
				
			return cpyimpData;
		}
		
		public static function mappingImportData(value:Object):ImportData
		{
			var newimpData:ImportData 	= new ImportData();
				newimpData.SOUSTETE 	= value.col_1;
				newimpData.EMPLOYE 		= value.col_3;
				newimpData.MATRICULE 	= value.col_2;
				newimpData.IDEMPLOYE	= 20;
				newimpData.IDSOUSTETE 	= 10;
				newimpData.ETAT 		= 1;
				newimpData.IDGENERATE 	= Math.random() * 1000000;;
			
			return newimpData;
		}
		
		public static function formatImportData(values:ArrayCollection):ArrayCollection
		{
			var importData	:ArrayCollection = new ArrayCollection();
			var lenOri		:int = values.length;
			
			for(var i:int = 1;i < lenOri;i++)
			{
				importData.addItem(mappingImportData(values[i]));
			}
			
			return importData;
		}
	}
}