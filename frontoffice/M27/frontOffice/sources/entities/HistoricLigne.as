package entities
{
	import mx.collections.ArrayCollection;

	public class HistoricLigne
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		public var ETAT 					:String = '';

		public var DATE_STATUT 				:Date = null;

		public var STATUT 					:int = 0;
		public var IDMDMLOG					:int = 0;
		public var IDUSER					:int = 0;		
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function HistoricLigne()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					COPIE OBJET COURANT
		//--------------------------------------------------------------------------------------------//
		
		public function copyHistoricLigne():HistoricLigne
		{
			var copyHistoricCampagne:HistoricLigne 		= new HistoricLigne();
				copyHistoricCampagne.ETAT				= this.ETAT;
				copyHistoricCampagne.DATE_STATUT		= this.DATE_STATUT;
				copyHistoricCampagne.STATUT				= this.STATUT;
				copyHistoricCampagne.IDMDMLOG			= this.IDMDMLOG;
				copyHistoricCampagne.IDUSER				= this.IDUSER;
			
			return copyHistoricCampagne;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					MAPPING ET FORMATTEUR
		//--------------------------------------------------------------------------------------------//
		
		public static function mappingHistoricLigne(value:Object):HistoricLigne
		{
			var mpHistoricCampagne:HistoricLigne 	= new HistoricLigne();
				mpHistoricCampagne.ETAT				= value.LIBELLE_STATUT;
				mpHistoricCampagne.DATE_STATUT		= value.DATE_STATUT;
				mpHistoricCampagne.STATUT			= value.IDMDM_STATUT;
				mpHistoricCampagne.IDMDMLOG			= value.IDMDM_LOG_CAMPAGNE;
				mpHistoricCampagne.IDUSER			= value.APP_LOGIN_STATUT;
			
			return mpHistoricCampagne;
		}
		
		public static function formatHistoricLigne(values:ArrayCollection):ArrayCollection
		{
			var historique	:ArrayCollection = new ArrayCollection();
			var len			:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				historique.addItem(mappingHistoricLigne(values[i]));
			}
			
			return historique;
		}
	}
}