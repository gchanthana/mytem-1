package ihm.destinataire
{
	import event.destinataire.DestinataireEvent;
	
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	
	public class DestinataireOrgaImpl extends HBox
	{
		public var listeOrganisations :OrgaImpl;
		public var destinatairesSide :DestinataireImpl;
		
		private var _isOrgaClicked:Boolean=true; 
		
		public function DestinataireOrgaImpl()
		{
			super();
		}
		
		/**
		 * lors de la creation complete: on ajoute les listeners
		 * @param event
		 */		
		protected function destinataireOrganisation_creationCompleteHandler(event:FlexEvent):void
		{
			listeOrganisations.listeDestinataireNoeudService.model.addEventListener(DestinataireEvent.LISTE_DESTINATAIRE_NOEUD_EVENT, fillDestinataireNoeud);
			
			/** pour mettre à jour le nb profil dans la liste des destinataire*/
			destinatairesSide.destinataireProfilService.model.addEventListener(DestinataireEvent.LISTE_DESTINATAIRE_PROFIL_EVENT, fillDestinataire); 
			destinatairesSide.destinataireProfilService.model.addEventListener(DestinataireEvent.DELETE_DESTINATAIRE_PROFIL_EVENT, fillDestinataire);
			
			addEventListener(DestinataireEvent.MAJ_DESTINATRAIRE_PROFIL,initIHMDestinataire);
			
			addEventListener(DestinataireEvent.LISTE_DESTINATAIRE_PROFIL_EVENT,fillDestinataire);
			
			/**  mettre a jour la liste des destinataires associés au noeud 
			 *   button associer dans la popup ( ListeDestinartaireIHM.mxml) qui est la origine  
			*/ 
			destinatairesSide.assoicerDestinataireNoeudService.model.addEventListener(DestinataireEvent.LISTE_DESTINATAIRE_NOEUD_EVENT, fillDestinataire);
			
			destinatairesSide.assoicerDestinataireNoeudService.model.addEventListener(DestinataireEvent.DISSOCIER_DESTINATAIRE_NOEUD_EVENT, fillDestinataireDeleteProfil);
		}
		
		/**
		 * mettre a jour la liste des destinataires après la dissociation d'un destinataire d'un noeud
		 * @param evt
		 * 
		 */		
		private function fillDestinataireDeleteProfil(evt:DestinataireEvent):void
		{
			_isOrgaClicked=true;
			destinatairesSide.selectedIndexDG=-1;
			listeOrganisations.listeDestinataireNoeudService.getListeDestinatairesNoeud(listeOrganisations.idItemTree);
		}
		/**
		 * permet d'alimenter la liste des destinataires associés à l'organistaion choisie
		 * @param evt
		 */		
		private function fillDestinataireNoeud (evt:DestinataireEvent):void
		{
			
			destinatairesSide.dgDestinataires.dataProvider=listeOrganisations.listeDestinataireNoeudService.model.destinatairesNoeud;
			
			/** pour re-sélectionner le destinataire dans le cas ou on clique sur les buttons 
			 * (id=btAddProfil_dest,id=btRemoveProfil_dest  dans DestinataireIHM.mxml)
			 *  en cliquant sur un des buttons un event est dispatché et capturé par cette class( methode =fillDestinataire)
			*/ 	
			if (destinatairesSide.selectedIndexDG != -1)
			{
				destinatairesSide.dgDestinataires.selectedIndex=destinatairesSide.selectedIndexDG;	
			}
			
			if(_isOrgaClicked)
			{
				destinatairesSide.resetProfils();
			}
			destinatairesSide.initLabel();
			destinatairesSide.IDnode=listeOrganisations.idItemTree;//idItemTree un getter
			destinatairesSide.nomNode=listeOrganisations.NomItemTree;
			
		}
		
		/**
		 * cette fonction est appellé, lors que l'on clique sur les buttons (id=btAddProfil_dest,id=btRemoveProfil_dest dans DestinataireIHM.mxml)
		 * permettant de déplacer les profils, suite à un dispatch event 
		 * @param evt
		 */		
		private function fillDestinataire(evt:DestinataireEvent):void
		{
			_isOrgaClicked=false; 
			destinatairesSide.selectedIndexDG=destinatairesSide.dgDestinataires.selectedIndex;
			listeOrganisations.listeDestinataireNoeudService.getListeDestinatairesNoeud(listeOrganisations.idItemTree); // mettre a jour la liste des destinataires assciés à une orga
		}
		
		/**
		 * permet d'initialiser l'IHM (partie destinataire) 
		 * @param evt
		 */		
		private function initIHMDestinataire(evt:DestinataireEvent):void
		{
			destinatairesSide.IDnode=-1;
			destinatairesSide.nomNode="";
			destinatairesSide.dgDestinataires.dataProvider=null;
			destinatairesSide.resetProfils();
			destinatairesSide.initLabel();
			destinatairesSide.lb_dest.text="";
		}
	}
}