package ihm.destinataire
{
	import event.destinataire.DestinataireEvent;
	import event.diffusion.DiffusionEvent;
	
	import flash.events.Event;
	
	import ihm.destinataire.perimetre.FilteredTreeDataDescriptor;
	import ihm.destinataire.perimetre.SpecialTreeMain;
	
	import mx.collections.ICollectionView;
	import mx.collections.XMLListCollection;
	import mx.containers.Panel;
	import mx.controls.ComboBox;
	import mx.core.ClassFactory;
	import mx.core.IFactory;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	
	import rapport.ihm.infofacture.SpecialSearch;
	
	import service.destinataire.listedestinatairenoeud.ListeDestinatairesNoeudService;
	import service.diffusion.listeorgaclient.ListeOrgaClientService;
	
	import vo.diffusion.Orga;
	
	public class OrgaImpl extends Panel
	{
		public var listeOrga :ComboBox;
		public var listeOrgaService : ListeOrgaClientService;
		public var listeDestinataireNoeudService:ListeDestinatairesNoeudService;
		public var treePerimetreClient : SpecialTreeMain;
		
		private var perimetreTreeItemRenderer:IFactory;
		private var _treeNodes:XMLList;
		private var _lastSelectedItem:Number;
		private var arrOpenItems:Object;
		private var _searchResult:Array;
		
		private var _idItemTree : Number=-1; // l'id  du noeud selectionné
		private var _NomItemTree : String="";
		
		private var _modeRestreint:Boolean = (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE.toUpperCase() == "GROUPELIGNE");
		private var modeActif:String;
		
		public static const MODE_ACTIF:String = "actif";
		public static const MODE_ALL:String = "all";
		public static const MODE_INACTIF:String = "inactif";
		
		public function OrgaImpl()
		{
			super();
		}

		public function get NomItemTree():String
		{
			return _NomItemTree;
		}

		public function set NomItemTree(value:String):void
		{
			_NomItemTree = value;
		}

		public function get idItemTree():Number
		{
			return _idItemTree;
		}

		public function set idItemTree(value:Number):void
		{
			_idItemTree = value;
		}

		/**
		 * permet d'abonner les composants aux evenements et d'appeler les services  
		 * @param event
		 */		
		protected function orgaimpl1_creationCompleteHandler(event:FlexEvent):void
		{
			listeOrgaService= new ListeOrgaClientService();
			listeOrgaService.model.addEventListener(DiffusionEvent.LISTE_ORGA_CLIENT_EVENT, fillListeOrga);
			listeOrgaService.getListeOrgaClient();
			
			/**
			 *  ClassFactory : r générer des instances d’une autre classe, ayant toutes des propriétés identiques.
			 *  dans cette exemple cela crée l'item renderer de l'arbre XML 
			 */
			perimetreTreeItemRenderer = new ClassFactory(ihm.destinataire.perimetre.PerimetreTreeItemRenderer_V2);
			
			/** instancier le service pour cherhcer la liste des destinataires associer à l'orga */ 
			listeDestinataireNoeudService=new ListeDestinatairesNoeudService();
								
		}
		/**
		 * permet de mettre à jour la liste des organisations client
		 * @param evt : DiffusionEvent
		 */
		private function fillListeOrga(evt:DiffusionEvent):void
		{
			listeOrga.dataProvider=listeOrgaService.model.organisationsClient;
		}
		/**
		 * cette fonction  vide la dataprovider et appeler la methode pour mettre a jour la liste des organisation 
		 * @param event : ListeEvent
		 */		
		protected function listeOrga_changeHandler(event:ListEvent):void
		{
			treePerimetreClient.dataProvider = null;
			treePerimetreClient.selectedItem = null;
			refreshTree();		
		}
		
		/**
		 * permet de mettre à jour l'arbre de l'origanisation client  
		 * @param myData:Object
		 */		
		public function refreshTree(myData:Object = null):void
		{
			/**  event capture dans DestinataireOrgaImpl.as , pour vider la liste des destinataires + profils */
			
			dispatchEvent(new DestinataireEvent(DestinataireEvent.MAJ_DESTINATRAIRE_PROFIL,true));
			
			var rootId:int;
	
			if(_modeRestreint)
			{
				rootId = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			}
			else
			{
				rootId = getSelectedOrganisationValue(); /** récuperer l'id de l'organisation choisie */ 
			}
			
			var dataset:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;
			
			if(myData == null)
			{
				var resultList:XMLList = dataset.descendants().(@NID == rootId);
				_treeNodes = resultList;// Cas : Selection de tous les noeuds
				if(resultList.length() == 1)
				{
					treePerimetreClient.setTreeDataProvider(resultList[0], "LBL", new FilteredTreeDataDescriptor(fitrerByStatut));
					treePerimetreClient.itemRenderer = perimetreTreeItemRenderer;// creer les itemrenderer de l'arbre
					if(treePerimetreClient.selectedIndex > 0)
					{
						_lastSelectedItem = treePerimetreClient.selectedItem.@NID;
						arrOpenItems = treePerimetreClient.openItems;
						treePerimetreClient.addEventListener(Event.RENDER, renderDone);
					}
				}
			}
		}
		/**
		 * permet de récupérer l'id de l'organisation choisi dans le combox listeOrga
		 * @return int
		 */		
		public function getSelectedOrganisationValue():int
		{
			try
			{
				return (listeOrga.selectedItem as Orga).idOrga;
			}
			catch(error:Error)
			{
			}
			return -1;
		}
		
		private function renderDone(event:Event):void
		{
			if(_lastSelectedItem > 0)
			{
				selectNodeByAttribute("NID", _lastSelectedItem.toString());
				_lastSelectedItem = -1;
			}
		}
		public function selectNodeByAttribute(attribute:String, value:String):void
		{
			try
			{
				var dept:XMLList;
				dept = _treeNodes.descendants().(@[attribute] == value);
				//Si le noeud recherche n'est pas encore chargé alors on fait un recherche spéciale		
				if(dept[0] == null && _searchResult)
				{
					var search:SpecialSearch;
					search = new SpecialSearch(this);
					treePerimetreClient.addEventListener(FlexEvent.UPDATE_COMPLETE, treePerimetreClientUpdateCompleteHandler);
					search.initSearch(treePerimetreClient, Number(value));
					return;
				}
				//recherche dans les noeuds déjà en mémoire
				var pr:Object = dept[0];
				while((pr = pr.parent()) != null)
					treePerimetreClient.expandItem(pr, true);
				treePerimetreClient.selectedItem = dept[0];
				treePerimetreClient.scrollToIndex(treePerimetreClient.selectedIndex);
				parentDocument.treeSelectionChanged();
			}
			catch(e:Error)
			{
				trace(e.getStackTrace());
			}
		}
		private function treePerimetreClientUpdateCompleteHandler(fe:FlexEvent):void
		{
			if(treePerimetreClient.initialized)
			{
				parentDocument.treeSelectionChanged();
			}
		}
		
		/**
		 *  Filtre  l'arbre en fonction du statut de l'item
		 *  Tous/actif/inactifs
		 */
		public function fitrerByStatut(item:Object):ICollectionView
		{
			var mode:String;
			var xmlListCollex:XMLListCollection;
			var xml:XML = item as XML;
			xmlListCollex = new XMLListCollection(new XMLList(xml.children()));		
			if(xml && modeActif == MODE_ACTIF)// Cas : on affiche les actifs
			{
				if(xml.node.IS_ACTIVE)
				{
					xmlListCollex = new XMLListCollection(xml.children().(@["IS_ACTIVE"] == "1" || (hasOwnProperty("HAS_ACTIVE") && @["HAS_ACTIVE"] =="1")));
				}
			}	
			else if(modeActif == MODE_INACTIF)// Cas : on affiche les inactifs
			{
				if(xml.node.IS_ACTIVE)
				{
					xmlListCollex.filterFunction = filterByInactif;
					xmlListCollex.refresh();
				}
			}
			return xmlListCollex;
		}
		
		/**
		 *  test si le noeud passé en paramètre est inactif
		 *  les dossiers même actifs sont affichés pour
		 *  respecter l'arborescence
		 */
		private function filterByInactif(node:Object):Boolean
		{
			var xml:XML = new XML();
			var visible:Boolean = true;
			
			if((node.@NTY && node.@NTY == "1" && node.@HAS_INACTIVE == "1") || (node.@IS_ACTIVE == "0"))// si c'est un dossier
			{
				visible = true;
			}
			else
			{
				visible = false;
			}
			return visible;
		}
		
		/**
		 * permet de récupérer l'id de l'element selectionné dans l'arabe et 
		 * Appeler le service permettant de récupérer la liste des destinataires associés 
		 * @return 
		 */		
		public function onClick_getSelectedTreeItemValue():void
		{
			if(treePerimetreClient.selectedItem != null)
			{
				idItemTree=parseInt(treePerimetreClient.selectedItem.@NID);
				NomItemTree=treePerimetreClient.selectedItem.@LBL;
			}

			if(idItemTree!=-1)
			{
				dispatchEvent(new DestinataireEvent(DestinataireEvent.MAJ_DESTINATRAIRE_PROFIL,true));
				listeDestinataireNoeudService.getListeDestinatairesNoeud(idItemTree);	
			}
			else
			{
				trace('pas de destinataire');
			}
		}
		
	}
}
