package event.diffusion
{
	import flash.events.Event;

	public class DiffusionEvent extends Event
	{

		public static const BACK_CLICK_EVENT:String="BackClickEvent";
		public static const LISTE_ORGA_CLIENT_EVENT:String="ListeOrgaClientEvent";
		public static const LISTE_PROFILS_ORGA_EVENT:String="ListeProfilsOrgaEvent";
		public static const IDORGA_EVENT:String="IdorgaEvent";
		public static const LISTE_EVENTS_EVENT:String="ListeEventsEvent";
		public static const CORPS_MAIL_EVENT:String="CorpsMailEvent";
		public static const CANCEL_CLICK_EVENT:String="CancelClickEvent";
		public static const LISTE_DIFFUSION_RAPPORT_EVENT:String="ListeDiffusionRapportEvent";
		public static const HISTORIQUE_ENVOI_EVENT:String="HistoriqueEnvoiEvent";
		public static const SELECT_CHILD_EVENT:String="SelectChildEvent";
		public static const REFRESH_LIST_DIFFUSION_EVENT:String="RefreshListDiffusionEvent";
		public static const NB_DESTINATAIRE_EVENT:String="NbDestinataireEvent";
		

		public function DiffusionEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new DiffusionEvent(type, bubbles, cancelable);
		}
	}
}