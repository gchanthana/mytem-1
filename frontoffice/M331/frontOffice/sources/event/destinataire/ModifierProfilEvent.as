package event.destinataire
{
	import flash.events.Event;

	public class ModifierProfilEvent extends Event
	{
		public var id:Number;
		public var libelle:String;
		public var codeInterne:String;
		public var commentaire:String;
		
		public function ModifierProfilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}