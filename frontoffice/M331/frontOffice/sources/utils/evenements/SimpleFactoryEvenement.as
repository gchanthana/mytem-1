package utils.evenements
{
	import ihm.evenements.Evenement16IHM;
	import ihm.evenements.Evenement25IHM;

	public class SimpleFactoryEvenement
	{
		private var returnIHMEvent:AbstractEvenement;
		
		public function SimpleFactoryEvenement()
		{
		}
		
		public function createComposants(idEvenement:Number):AbstractEvenement
		{
			switch(idEvenement)
			{
				case 16 : returnIHMEvent=  new Evenement16IHM();break;
				case 25 : returnIHMEvent=  new Evenement25IHM();break;
				default :  trace('ok');break;
			}
			return returnIHMEvent;
		}
	}
}