package service.parameter
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class UserAccessHandler
	{
		private var _model:UserAccessModel;

		public function UserAccessHandler(model:UserAccessModel)
		{
			_model=model;
		}

		internal function getUserAccessResultHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.updateUserAccess(event.result as ArrayCollection);
			}
		}

		internal function checkPassWordResultHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.updateCheckPassWordResult(Number(event.result));
			}
		}


	}
}
