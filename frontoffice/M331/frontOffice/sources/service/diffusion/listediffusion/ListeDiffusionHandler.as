package service.diffusion.listediffusion
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ListeDiffusionHandler
	{
		private var _model:ListeDiffusionModel;
		
		public function ListeDiffusionHandler(_model:ListeDiffusionModel)
		{
			this._model=_model;
		}
		
		internal function getlisteRapportsEnDiffusionHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.listeDiffusion(event.result as ArrayCollection);
			}
		}
	}
}