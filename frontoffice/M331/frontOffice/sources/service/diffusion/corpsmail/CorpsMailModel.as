package service.diffusion.corpsmail
{
	import event.diffusion.DiffusionEvent;
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;

	internal class CorpsMailModel extends EventDispatcher
	{
		private var _bodyMail:String;

		public function CorpsMailModel()
		{
		}

		public function get bodyMail():String
		{
			return _bodyMail;
		}

		public function set bodyMail(value:String):void
		{
			_bodyMail=value;
		}

		internal function corpsMail(value:ArrayCollection):void
		{
			
			if (value.length > 0)
			{
				_bodyMail=value[0]['CORPSMAIL'];
			}
			dispatchEvent(new DiffusionEvent(DiffusionEvent.CORPS_MAIL_EVENT));// capturé par diffusionImpl	
		}
	}
}
