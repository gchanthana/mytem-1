package service.diffusion.testerDiffusion
{
	import mx.rpc.events.ResultEvent;

	internal class TesterDiffusionHandler
	{
		private var _model:TesterDiffusionModel;

		public function TesterDiffusionHandler(_model:TesterDiffusionModel)
		{
			this._model=_model;
		}

		internal function testerDiffusionHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.testerDiffusion(event);
			}
		}
	}
}
