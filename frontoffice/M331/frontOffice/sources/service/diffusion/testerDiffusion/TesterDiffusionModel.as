package service.diffusion.testerDiffusion
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.EventDispatcher;
	
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;

	internal class TesterDiffusionModel extends EventDispatcher
	{

		public function TesterDiffusionModel()
		{

		}
		internal function testerDiffusion(re:ResultEvent):void
		{
			if (re.result == 1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M331', 'Votre_demande_a_bien__t__prise_en_compte'));
			}
		}
	}
}
