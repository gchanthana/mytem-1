package service.diffusion.listeprofilsorga
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class ListeProfilsOrgaService
	{
		public var _model:ListeProfilsOrgaModel;
		public var handler:ListeProfilsOrgaHandler;	
		
		public function ListeProfilsOrgaService()
		{
			this._model  = new ListeProfilsOrgaModel();
			this.handler = new ListeProfilsOrgaHandler(model);
		}
		public function get model():ListeProfilsOrgaModel
		{
			return this._model;
		}	
		public function getListeProfilsOrga(idOrga:int): void
		{
			var callOp:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.ListeProfilsOrgaService","getListeProfilsOrga",handler.getListeProfilsOrgaHandler); 
			
			RemoteObjectUtil.callService(callOp,idOrga);	
		}
	}
}