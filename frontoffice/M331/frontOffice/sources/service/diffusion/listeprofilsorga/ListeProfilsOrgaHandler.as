package service.diffusion.listeprofilsorga
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ListeProfilsOrgaHandler
	{
		private var _model:ListeProfilsOrgaModel;
		
		public function ListeProfilsOrgaHandler(_model:ListeProfilsOrgaModel)
		{
			this._model=_model;
		}
		
		internal function getListeProfilsOrgaHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.listeProfilsOrga(event.result as ArrayCollection);
			}
		}
	}
}