package service.diffusion.validerdiffusion
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class ValiderDiffusionService
	{
		public var _model:ValiderDiffusionModel;
		public var handler:ValiderDiffusionHandler;

		public function ValiderDiffusionService()
		{
			this._model=new ValiderDiffusionModel();
			this.handler=new ValiderDiffusionHandler(model);
		}

		public function get model():ValiderDiffusionModel
		{
			return this._model;
		}

		public function validerDiffusion(idRapport:int,idEvent:int,idOrga:int,idProfil:int,nbTop:int,finPeriode:int,periodeEtude:int,jourLancement:int,
										 destinatairesCaches:String,objetMail:String,nomAttachementFile:String,corpsMail:String,valeurTemplate :String,format:int):void
		{
			var callOp:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.ValiderDiffusionService", "validerDiffusion", handler.validerDiffusionHandler);

			RemoteObjectUtil.callService(callOp,idRapport,idEvent,idOrga,idProfil,nbTop,finPeriode,periodeEtude,jourLancement,
				destinatairesCaches,objetMail,nomAttachementFile,corpsMail,valeurTemplate,format);
		}
	}
}
