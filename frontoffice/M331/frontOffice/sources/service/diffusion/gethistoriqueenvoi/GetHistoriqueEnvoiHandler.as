package service.diffusion.gethistoriqueenvoi
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class GetHistoriqueEnvoiHandler
	{
		private var _model:GetHistoriqueEnvoiModel;

		public function GetHistoriqueEnvoiHandler(_model:GetHistoriqueEnvoiModel)
		{
			this._model=_model;
		}

		internal function getHistoriqueEnvoiHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.getHistoriqueEnvoi(event.result as ArrayCollection);
			}
		}
	}
}
