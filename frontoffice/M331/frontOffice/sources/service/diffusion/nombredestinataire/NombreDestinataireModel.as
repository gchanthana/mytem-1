package service.diffusion.nombredestinataire
{
	import event.diffusion.DiffusionEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;

	internal class NombreDestinataireModel extends EventDispatcher
	{
		private var _nbProfil:int;

		public function NombreDestinataireModel()
		{
		}

		public function get nbProfil():int
		{
			return _nbProfil;
		}

		public function set nbProfil(value:int):void
		{
			_nbProfil = value;
		}

		internal function nbProfilDiffusion(value: ArrayCollection):void
		{
			if (value.length > 0)
			{
				_nbProfil=value[0]['NBDESTINATAIRE'];
			}
			dispatchEvent(new DiffusionEvent(DiffusionEvent.NB_DESTINATAIRE_EVENT));// capturé par diffusionImpl	
		}
	}
}
