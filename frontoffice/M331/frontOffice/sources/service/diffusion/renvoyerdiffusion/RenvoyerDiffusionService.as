package service.diffusion.renvoyerdiffusion
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class RenvoyerDiffusionService
	{
		public var _model:RenvoyerDiffusionModel;
		public var handler:RenvoyerDiffusionHandler;

		public function RenvoyerDiffusionService()
		{
			this._model=new RenvoyerDiffusionModel();
			this.handler=new RenvoyerDiffusionHandler(model);
		}

		public function get model():RenvoyerDiffusionModel
		{
			return this._model;
		}

		public function renvoyerDiffusion(idEnvoi:int):void
		{
			var callOp:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.ActionsDiffusionService", "resendDiffusion", handler.renvoyerDiffusionHandler);
			
			RemoteObjectUtil.callService(callOp,idEnvoi);
		}
	}
}
