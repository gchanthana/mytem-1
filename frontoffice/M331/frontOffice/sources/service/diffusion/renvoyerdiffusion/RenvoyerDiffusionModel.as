package service.diffusion.renvoyerdiffusion
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;	import flash.events.EventDispatcher;	import mx.rpc.events.ResultEvent;	internal class RenvoyerDiffusionModel extends EventDispatcher	{		public function RenvoyerDiffusionModel()		{		}		internal function renvoyerDiffusion(re:ResultEvent):void		{			if (re.result == 1)			{				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M331', 'Votre_diffusion_a_bien__t__envoy_e'));			}			else			{				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M331', 'Une_erreur_est_intervenue_lors_de_votre_'), ResourceManager.getInstance().getString('M331', 'Erreur_diffusion'));			}		}	}}
