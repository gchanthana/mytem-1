package service.destinataire.listedestinataires 
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ListeDestinatairesHandler
	{
		private var _model:ListeDestinatairesModel;
		
		public function ListeDestinatairesHandler(_model:ListeDestinatairesModel):void
		{
			this._model = _model;
		}
		
		internal function getListeDestinatairesResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.listeDestinataires(evt.result as ArrayCollection);
			}
		}	
	}
}