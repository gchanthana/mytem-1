package service.destinataire.listedestinataires
{
	import event.destinataire.DestinataireEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.destinataire.Destinataire;
	

	internal class ListeDestinatairesModel  extends EventDispatcher
	{
		private var _destinataires :ArrayCollection;
		
		public function ListeDestinatairesModel()
		{
			_destinataires = new ArrayCollection();	
		}
		public function get destinataires():ArrayCollection
		{
			return _destinataires;
		}
		
		internal function listeDestinataires(value:ArrayCollection):void
		{	
			_destinataires.removeAll();
			
			var unDestinataire:Destinataire;	
			
			if(value.length > 0)
			{
				for(var i:int=0;i<value.length;i++)
				{
					unDestinataire = new Destinataire();
					unDestinataire.fill(value[i]);
					_destinataires.addItem(unDestinataire); 
				}
			}			
			dispatchEvent(new DestinataireEvent(DestinataireEvent.USER_LISTE_DESTINATRAIRE_EVENT));
		}
	}
}