package service.destinataire.listedestinatairenoeud
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ListeDestinatairesNoeudHandler
	{
		private var _model:ListeDestinatairesNoeudModel;
		
		public function ListeDestinatairesNoeudHandler(_model:ListeDestinatairesNoeudModel):void
		{
			this._model = _model;
		}
		
		internal function getListeDestinatairesNoeudResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.listeDestinatairesNoeud(evt.result as ArrayCollection);
			}
		}	
	}
}