package service.destinataire.listedestinatairenoeud
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;


	public class ListeDestinatairesNoeudService
	{
		private var _model:ListeDestinatairesNoeudModel;
		public var handler:ListeDestinatairesNoeudHandler;	
		
		public function ListeDestinatairesNoeudService()
		{
			this._model  = new ListeDestinatairesNoeudModel();
			this.handler = new ListeDestinatairesNoeudHandler(model);
		}		
		
		public function get model():ListeDestinatairesNoeudModel
		{
			return this._model;
		}

		public function getListeDestinatairesNoeud(idNoeud:Number):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.ListeDestinataireNoeud","ListeDestNoeud",handler.getListeDestinatairesNoeudResultHandler); 
			
			RemoteObjectUtil.callService(op,idNoeud);// appel au service coldfusion
			
		}
	}
}