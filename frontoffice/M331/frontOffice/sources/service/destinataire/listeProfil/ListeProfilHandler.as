package service.destinataire.listeProfil
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ListeProfilHandler
	{
		private var _model:ListeProfilModel;
		
		public function ListeProfilHandler(_model:ListeProfilModel):void
		{
			this._model = _model;
		}
		
		internal function getListeProfilResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.listeAllProfil(evt.result as ArrayCollection);
			}
		}	
	}
}