package {
	import flash.events.Event;

	/**
	 * Evènement qui permet de stocket un objet
	 * L'objet est passé en paramètre dans le constructeur et une méthode
	 * setter permet de modifier la référence de l'objet.
	 * */
	public class ConsoViewDataEvent extends Event {
		public static const REMOTING_CALL:String = "RemoteObjectUtil REMOTING CALL";
		public static const REMOTING_RESULT:String = "RemoteObjectUtil REMOTING RESULT";
		public static const MAIN_UNIVERS_CHANGED:String = "MAIN_UNIVERS CHANGED";
		public static const UNIVERS_FUNCTION_CHANGED:String = "UNIVERS_FUNCTION CHANGED";
		public static const FACTURE_DEB_CHANGED:String = "FACTURE_DEB CHANGED"; // La date début facture a changée
		public static const FACTURE_FIN_CHANGED:String = "FACTURE_FIN CHANGED"; // La date de fin de facture a changée
		public static const PERFORM_NODE_SEARCH:String = "PERFORM_NODE SEARCH"; // Recherche de noeud dans un arbre de périmètres
		public static const BACK_NODE_SEARCH:String = "BACK_NODE SEARCH"; // Retour pour la Recherche de noeud dans un arbre de périmètres
		public static const NODE_SEARCH_RESULT:String = "NODE_SEARCH RESULT"; // Résult pour la Recherche de noeud dans un arbre de périmètres
		public static const NODE_SEARCH_NO_RESULT:String = "NODE_SEARCH NO_RESULT"; // Résult vide pour la Recherche de noeud dans un arbre de périmètres			
		
		private var dataObject:Object = null;
		
		public function ConsoViewDataEvent(type:String,dataParam:Object = null) {
			super(type);
			dataObject = dataParam;
		}
		
		public function get data():Object {
			return dataObject;
		}
	}
}