package appli.ConsoleGestion.CreateMenuConsoleGestion
{
	import appli.app.AppConsoleGestion;
	import appli.events.LoginEvent;
	import appli.impersonnification.event.RelogEvent;
	import appli.impersonnification.popup.PopupChooseOtherAccountIHM;
	import appli.impersonnification.refreshManager;
	import appli.mytem.createmenumytem.CreateMenuMytemIHM;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import general.interfaceClass.IMenu;
	
	import mx.containers.Canvas;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.Label;
	import mx.core.IFlexDisplayObject;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class CreateMenuConsoleGestionImpl extends Canvas implements IMenu
	{
		[Bindable] protected var BACKGROUND_COLOR:uint = new uint(333333);
		[Bindable] public var hbheader:HBox;
		public var hbMenu:HBox;
		[Bindable] public var lblLoggedUserName:Label;
		[Bindable]
		public var urlImage:String=cv.urlLogoHeader + "/logomytem360.png";
		public var labelReport:Label;
		public var labelHelp:Label;
		public var lblChangeAccount:Label;
		[Embed(source="/assets/menu/HomeGraphical.swf", symbol="header104")]
		[Bindable] public var header:Class;
		[Embed(source="/assets/menu/HomeGraphical.swf", symbol="backGroundBlue")]
		[Bindable] public var backGroundBlueImage:Class;
		[Bindable] public var urlLogo:String=cv.urlLogoHeader + "/logomytem360.png";
		public var popupChooseOtherAccount:PopupChooseOtherAccountIHM = new PopupChooseOtherAccountIHM();
		
		private var positionPoint:Point;
		
		public function CreateMenuConsoleGestionImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		public function init(e:FlexEvent):void
		{
			if (CvAccessManager.getSession().USER.EMAIL.indexOf("@saaswedo.com") == -1 && refreshManager.impersonnification == false)
			{
				hbheader.removeChild(lblChangeAccount);				
				hbheader.x = 605;
			}
			if (refreshManager.impersonnification == false)
				lblLoggedUserName.text = "Welcome  : " + CvAccessManager.getUserObject().PRENOM + " " + CvAccessManager.getUserObject().NOM; /** le label du login connecté*/
			else
			{
				refreshManager.currentAccountName = CvAccessManager.getUserObject().PRENOM + " " + CvAccessManager.getUserObject().NOM;
				lblLoggedUserName.text = "Welcome : " + refreshManager.getImpersString();
				hbheader.x = 430;
			}
			updateMenu();
		}
		public function updatePerimetre():void
		{
			updateMenu();
		}
		public function updateMenu():void
		{
			var menuXML:XML=CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			
			var xmlList:XMLList=menuXML.children(); /** tous les enfants du root */
			
			var itemMenu:CreateMenuMytemIHM;
			
			hbMenu.removeAllChildren();
			
			for each (var item:XML in xmlList)
			{
				itemMenu=new CreateMenuMytemIHM();
				
				itemMenu.nomImageGris=item.@KEY + "_gris.png";
				itemMenu.nomImageBleu=item.@KEY + "_bleu.png";
				itemMenu.labelText=item.@LBL;
				itemMenu.toggle=item.@toggled;
				
				itemMenu.XMLUnivers=item; /** un morceau de le XML = le XML d'un item de menu */
				if (item.@USR == 1)
				{
					hbMenu.addChild(itemMenu);
				}
			}
		}
		public function displayListePerimetres(event:MouseEvent):void
		{
		}
		public function logoff():void
		{	
			closeAllPopups();
			CvAccessManager.deconnectSession();
		} 
		public function chooseOtherAccount():void
		{
			PopUpManager.addPopUp(popupChooseOtherAccount, this, true);
			PopUpManager.centerPopUp(popupChooseOtherAccount);
			
			popupChooseOtherAccount.addEventListener(RelogEvent.LOGIN_CHOSEN, accountChosenHandler);
		}
		public function chooseOtherAccount_mouseOverHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#358dd7'); /**set*/
		}
		public function chooseOtherAccount_mouseOutHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#ffffff');
		}
		public function accountChosenHandler(evt:Event):void
		{
			trace("refresh en cours");
			refreshManager.refreshApp();
		}
		public function deconnexion_mouseOverHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#358dd7'); /**set*/
		}
		
		public function deconnexion_mouseOutHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#ffffff');
		}
		public function refreshMenu():void
		{
			for each (var bt:CreateMenuMytemIHM in hbMenu.getChildren())
			{
				bt.refreshToggle();
			}
		}
		/* ---------------------------------------------------------------------------------
		ferme toutes les popups 
		--------------------------------------------------------------------------------- */
		private function closeAllPopups():void
		{
			var len:Number=systemManager.numChildren - 1;
			var i:Number=len;
			
			while (i >= 0)
			{
				if (systemManager.getChildAt(i).hasOwnProperty("isPopUp"))
				{
					PopUpManager.removePopUp(IFlexDisplayObject(systemManager.getChildAt(i)));
				}
				i--;
			}
		}
	}
}