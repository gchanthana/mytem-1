package appli.Pilotage
{
	import appli.Pilotage.CreateMenuPFGP.CreateMenuPFGPIHM;
	import appli.events.LoginEvent;
	import appli.impersonnification.refreshManager;
	
	import general.abstract.AbstractCreateApplication;
	import general.abstract.AbstractModuleLoader;
	import general.interfaceClass.ICreateApplication;
	
	import mx.containers.VBox;
	
	public class CreatePFGPImpl extends AbstractCreateApplication implements ICreateApplication
	{
		public function CreatePFGPImpl()
		{
			super();
		}
// ---------------------- --------------- ---------------------------------		
// ---------------------- FONCTION PUBLIC ---------------------------------
// ---------------------- --------------- ---------------------------------
		override public function updatePerimetre():void
		{
			menu.updatePerimetre();
			initDisplay();
		}
		override public function displayModule(key:String=null):void
		{
			var menuXML:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			var selectedMenu:XMLList = menuXML.descendants("*").(@SYS == 1).(@toggled == 'true'); 
			if (refreshManager.isReloading == true)
			{//Exception dans le cas où il s'agit d'une connexion sur un autre compte, le point de départ doit devenir le HOME
				refreshManager.isReloading = false;
				CvAccessManager.changeModule("HOME_PILOTAGE");
			}
			else
			{
				moduleLoader.loadModule(selectedMenu.@KEY);
			}
			super.displayModule();
		}
// ---------------------- --------------- ---------------------------------		
// ---------------------- FONCTION PRIVEE ---------------------------------
// ---------------------- --------------- ---------------------------------
		override protected function initDisplay():void
		{
			displayModule();
		}
	}
}