package appli.Pilotage.CreateMenuPFGP.composant
{
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import mx.containers.TitleWindow;

	public class PopupAideImpl extends TitleWindow
	{
		private const UrlDocumentAide:String = "https://cache-pilotage.sfrbusinessteam.fr/Aide/";
		
		public function PopupAideImpl()
		{
			super();
		}
		protected function btAnalyser_clickHandler(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest(UrlDocumentAide+"Aide_en_ligne_Analyser_ma_facture.pdf");
			letsgoURL(request);
		}
		protected function btCarlo_clickHandler(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest(UrlDocumentAide+"Aide_en_ligne_construire_une_analyse.pdf");
			letsgoURL(request);
		}
		protected function btParc_clickHandler(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest(UrlDocumentAide+"Aide_en_ligne_gerer_mon_parc.pdf");
			letsgoURL(request);
		}
		protected function btcommander_clickHandler(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest(UrlDocumentAide+"Aide_en_ligne_commander_mes_services_et_options.pdf");
			letsgoURL(request);
		}
		protected function btComptes_clickHandler(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest(UrlDocumentAide+"Aide_en_ligne_gerer_mes_comptes.pdf");
			letsgoURL(request);
		}
		protected function btCata_clickHandler(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest(UrlDocumentAide+"Aide_en_ligne_gerer_mes_catalogues.pdf");
			letsgoURL(request);
		}
		protected function btOrga_clickHandler(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest(UrlDocumentAide+"Aide_en_ligne_parametrer_mon_organisation.pdf");
			letsgoURL(request);
		}
		private function letsgoURL(url:URLRequest):void
		{
			navigateToURL(url,null);
		}
	}
}