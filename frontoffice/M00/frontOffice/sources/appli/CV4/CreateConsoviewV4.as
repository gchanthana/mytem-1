package appli.CV4
{
	import appli.CV4.CreateBanniereCV4.CreateBanniereCV4IHM;
	import appli.impersonnification.refreshManager;
	
	import general.abstract.AbstractCreateApplication;
	import general.interfaceClass.ICreateApplication;
	
	public class CreateConsoviewV4 extends AbstractCreateApplication implements ICreateApplication
	{
		
		public var bann:CreateBanniereCV4IHM;
		
		public function CreateConsoviewV4()
		{
			super();
		}
// ---------------------- --------------- ---------------------------------		
// ---------------------- FONCTION PUBLIC ---------------------------------
// ---------------------- --------------- ---------------------------------
		override public function updatePerimetre():void
		{
			menu.updatePerimetre();
			initDisplay();
		}
		override public function displayModule(key:String=null):void
		{
			var menuXML:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			var selectedMenu:XMLList = menuXML.descendants("*").(@SYS == 1).(@toggled == 'true'); 
			if (refreshManager.isReloading == true)
			{//Exception dans le cas où il s'agit d'une connexion sur un autre compte, le point de départ doit devenir le HOME
				refreshManager.isReloading = false;
				moduleLoader.loadModule("HOME");
				trace("isReloading avait pour valeur true, on le met sur false");
			}
			else
			{
				moduleLoader.loadModule(selectedMenu.@KEY);
			}
			
			super.displayModule();
		}
// ---------------------- --------------- ---------------------------------		
// ---------------------- FONCTION PRIVEE ---------------------------------
// ---------------------- --------------- ---------------------------------
		override protected function initDisplay():void
		{
			displayModule();
		}
	}
}