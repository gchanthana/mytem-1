package appli.mytem.createmenumytem
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.containers.VBox;
	import mx.controls.Image;
	import mx.controls.Menu;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.managers.PopUpManager;
	
	import appli.ConsoleGestion.CreateMenuConsoleGestion.CreateMenuConsoleGestionIHM;
	import appli.mytem.createmytemheader.CreateMytemHeaderIHM;

	public class CreateMenuMytemImpl extends VBox
	{

		private var _nomImageGris:String;
		private var _nomImageBleu:String;
		private var _labelText:String;
		private var _toggle:Boolean;
		private var _customMenu:Menu;
		private var _XMLUnivers:XML;
		private var _selectedItem:Object;
		private var _boolUniversOnly:Boolean=true;
		private var _boolIsPopup:Boolean=false;
		private var _delta:Number=0;
		private var _selectedUnivers:Boolean=false;
		private var _image:Image;
		private var _selecteditemSubMenu:Boolean=false;

		[Bindable]
		[Embed(source="/assets/menu/arrow.png")]
		public var myRadioIcon:Class;

		[Bindable]
		[Embed(source="/assets/menu/sousMenu_active.png")]
		public var sousMenu:Class;

		public function CreateMenuMytemImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * permet d'initialiser l'IHM
		 * @param e
		 */
		private function init(e:FlexEvent):void
		{
			addEventListener(MouseEvent.CLICK, clickHandler);
			addEventListener(MouseEvent.ROLL_OVER, rollOverMenuHandler);
			addEventListener(MouseEvent.ROLL_OUT, rollOutMenuHandler);

			(_XMLUnivers.@toggled == 'true') ? setItemStyle(true) : setItemStyle(false);

			_image=new Image();
			_image.source=sousMenu;
			_image.visible=false;
			addChild(DisplayObject(_image));
		}

		/**
		 * permet de changer la couleur des icons dans le menu
		 * @param isBlue
		 */
		private function setItemStyle(isBlue:Boolean):void
		{
			toggle=isBlue;
		}

		/**
		 * permet de gérer le click des items de menu
		 * @param event
		 *
		 */
		private function clickHandler(event:MouseEvent):void
		{
			refresh(); /** mettre a jour le menu */

			if (!_boolUniversOnly) /** si c'est un univers avec sous meun associé à l'elemet cliqué */
			{				
				if (!_boolIsPopup)
				{
					AfficherMenu(true, null, true); /** afficher le sous menu */
				}
				else
				{
					
					AfficherMenu(false, null, true); /** masquer le sous menu*/
					refreshMenu();
				}
			}
			else /** univers seul*/
			{
				AfficherMenu(true);
			}
		}
		
		/**
		 * permet de gerer le toggel sur le menu (icon bleu ou gris) 
		 * @param evt
		 */		
		private function rollOverMenuHandler(evt:MouseEvent):void
		{
			toggle=true;
		}
		
		private function rollOutMenuHandler(evt:MouseEvent):void
		{
			toggle=true;
			refreshMenu();
		}
		
		/**
		 * permet d'afficher le sous menu en cliquant sur
		 * @param open : pour afficher le sous menu 
		 * il y a deux choses( lors que l'on clique sur univers ( item menu sans sous menu) et function ( item menu avec sous menu)
		 */
		public function AfficherMenu(open:Boolean, xmlUniv:XML=null, selectedUnivers:Boolean=false):void
		{
			this._selectedUnivers=selectedUnivers; /** determiner si l'element cliqué du menu est un univers (sans sous menu) */

			if (!_boolUniversOnly) /** s'il y a un sous menu associé au element selectionné => si ce n'est pas univers*/
			{
				if (open) /** ouvrir le sous menu */
				{
					createCustomMenu();

					PopUpManager.addPopUp(_customMenu, this);

					/** calculer le positionnement de sous menu*/

					var tmpPoint:Point=this.localToGlobal(new Point(parentDocument.x, parentDocument.y + this.height));
					var distance:Number=(parentApplication.width) - (tmpPoint.x + _customMenu.width); /** permet de positionner le dernier sous menu dans l'application-sans dépasser le bordure */

					if (distance < 0)
					{
						_delta=distance - 17;
					}
					_customMenu.x=tmpPoint.x + _delta;
					_customMenu.y=tmpPoint.y;
					_customMenu.selectedItem=this._selectedItem;

					_image.visible=true;
					_boolIsPopup=true;
					toggle=true;
				}
				else
				{
					PopUpManager.removePopUp(_customMenu);
					_image.visible=false;
					_boolIsPopup=false;

					(selectedUnivers) ? toggle=true : toggle=false;
				}
			}
			else
			{
				var currentFunction:String=CvAccessManager.CURRENT_FUNCTION; /** répresente l'attribut KEY="......"  de l'element du menu déjà cliqué */
				var nodeCurrent:XML=_XMLUnivers as XML; /** le XML de l'element selectionné de menu */

				if (open) /** on vient de la fonction clickhandler */
				{
					if (nodeCurrent.@KEY != currentFunction)
						changeUnivers(nodeCurrent.@KEY);
					toggle=true;
				}
				else /** on vient de la fonction refresh du parent */
				{
					var node:XML=xmlUniv as XML; /** le XML de l'element qui vient d'être cliqué */

					(node.@KEY == nodeCurrent.@KEY) ? toggle=true : toggle=false;
				}
			}
		}
		
		/**
		 * cette fonction est public et appelé  a partir de CreateMytemHeaderImpl
		 */		
		public function refreshToggle():void
		{
			if(_XMLUnivers.@KEY == CvAccessManager.CURRENT_UNIVERS) /** si on change pas le module alors on garde la couleur bleu de l'element sélectionné */
			{
				toggle=true;
			}
			else
			{
				toggle=false;
			}
		}
		/**
		 * permet de changer le module
		 * @param evt
		 */
		private function changeUnivers(key:String):void
		{
			CvAccessManager.changeModule(key);
		}

		/**
		 * permet de rafraichir le menu  pour effectuer le changement d'etat des icons et fermer le sous menu d'un element selectionné
		 */
		private function refresh():void
		{
			if (this.parent.parent is CreateMytemHeaderIHM)
			{
				(this.parent.parent as CreateMytemHeaderIHM).refresh(this);
			}
		}
		
		/**
		 * permet de rafraichir le menu  pour effectuer le changement d'etat des icons
		 */
		private function refreshMenu():void
		{
			if(this.parent && this.parent.parent)
			{
				if (this.parent.parent is CreateMytemHeaderIHM)
				{
					(this.parent.parent as CreateMytemHeaderIHM).refreshMenu();
				}
				else if (this.parent.parent is CreateMenuConsoleGestionIHM)
				{
					(this.parent.parent as CreateMenuConsoleGestionIHM).refreshMenu();
				}
			}
		}
		
		/**
		 * permet de creer le sous menu
		 */
		private function createCustomMenu():void
		{
			_customMenu=new Menu();
			_customMenu=Menu.createMenu(null, _XMLUnivers.children(), false);
			_customMenu.labelField="@LBL";
			_customMenu.styleName="MenuHeader";
			_customMenu.setVisible(true); /** permet d'afficher le sous menu */
			_customMenu.invalidateDisplayList();
			_customMenu.setStyle('radioIcon', myRadioIcon); // Specify the radio button icon.
			_customMenu.addEventListener(MenuEvent.ITEM_CLICK, changeUniversFunction);
			_customMenu.addEventListener(MouseEvent.ROLL_OUT, rollOutSubMenuHandler);
		}
		
		/**
		 * permet de masquer le sous menu en roll out de la souris et garder la couleur bleu de l'element selectionné
		 * @param e
		 */
		private function rollOutSubMenuHandler(e:MouseEvent):void
		{
			AfficherMenu(false, null, true);
			refreshMenu();
		}

		/**
		 * permet de changer (charger un module) on cliquant dans le sous menu (ex :  paramètre -> Gestion des organisations)
		 * @param event
		 */
		public function changeUniversFunction(event:Event):void
		{
			var node:XML=(event as MenuEvent).item as XML;
			CvAccessManager.changeModule(node.@KEY);
			_boolIsPopup=false;
		}

		/**------------------------------------------------ getter et setter ---------------------------------------------------- */
		[Bindable]
		public function set nomImageBleu(value:String):void
		{
			_nomImageBleu=value;
		}

		public function get nomImageBleu():String
		{
			return _nomImageBleu;
		}

		[Bindable]
		public function set nomImageGris(value:String):void
		{
			_nomImageGris=value;
		}

		public function get nomImageGris():String
		{
			return _nomImageGris;
		}

		[Bindable]
		public function set toggle(value:Boolean):void
		{
			_toggle=value;
		}

		public function get toggle():Boolean
		{
			return _toggle;
		}

		[Bindable]
		public function get labelText():String
		{
			return _labelText;
		}

		public function set labelText(value:String):void
		{
			_labelText=value;
		}

		public function get XMLUnivers():XML
		{
			return _XMLUnivers;
		}

		public function set XMLUnivers(value:XML):void
		{
			_XMLUnivers=value;

			(_XMLUnivers.children().length() > 0) ? _boolUniversOnly=false : _boolUniversOnly=true;
		}

	/**--------------------------------------------------------------------------------------------------------------------- */
	}
}
