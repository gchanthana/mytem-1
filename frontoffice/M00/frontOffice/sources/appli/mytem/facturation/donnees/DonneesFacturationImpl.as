package appli.mytem.facturation.donnees
{
	import com.mytem.utils.preloader.Spinner;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.OLAPDataGrid;
	import mx.events.CubeEvent;
	import mx.messaging.messages.ErrorMessage;
	import mx.olap.IOLAPCube;
	import mx.olap.IOLAPQuery;
	import mx.olap.IOLAPQueryAxis;
	import mx.olap.OLAPAttribute;
	import mx.olap.OLAPCube;
	import mx.olap.OLAPQuery;
	import mx.olap.OLAPResult;
	import mx.olap.OLAPSet;
	import mx.rpc.AsyncResponder;
	import mx.rpc.AsyncToken;
	
	[Bindable]
	public class DonneesFacturationImpl extends Canvas
	{
		private	var _dataProvider:ArrayCollection;
		private var _boolDataProviderUpdated:Boolean = false;
		private var _progressBar:Spinner;
		public 	var myOLAPDG:OLAPDataGrid;
		public 	var olapPeriodAttr:OLAPAttribute;
		public 	var myMXMLCube:OLAPCube;
		private var _boolDataUpdated:Boolean = false;
		public var bool_showError:Boolean = false;
		
		public function DonneesFacturationImpl()
		{
			super();
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			if (_boolDataProviderUpdated)
			{
				if (dataProvider)
				{
					myMXMLCube.dataProvider = dataProvider;
					myMXMLCube.refresh();
				}
				_boolDataProviderUpdated = false;
			}
		}
		
		public function launchLoader():void
		{
			boolDataUpdated = false;
			_progressBar = new Spinner();
			_progressBar.x = this.width / 2;
			_progressBar.y = this.height / 2;
			this.addChild(_progressBar);
			_progressBar.play();
		}
		
		public function stopLoader():void
		{
			if (_progressBar)
			{
				_progressBar.stop();
				this.removeChild(_progressBar);
				_progressBar = null;
			}
		}
		
		public function set dataProvider(value:ArrayCollection):void
		{
			olapPeriodAttr.name = "Period";
			_boolDataProviderUpdated = false;
			if (_dataProvider != value)
			{
				_dataProvider = value;
				_boolDataProviderUpdated = true;
				if (_progressBar)
				{
					_progressBar.stop();
					this.removeChild(_progressBar);
					_progressBar = null;
				}
				boolDataUpdated = true;
				invalidateDisplayList();
			}
		}
		
		public function get dataProvider():ArrayCollection
		{
			return _dataProvider;
		}
		
		private function getQuery(cube:IOLAPCube):IOLAPQuery
		{
			olapPeriodAttr.name = "Period";
			var query:OLAPQuery = new OLAPQuery();
			var rowQueryAxis:IOLAPQueryAxis = query.getAxis(OLAPQuery.ROW_AXIS);
			var operateurSet:OLAPSet = new OLAPSet;
			operateurSet.addElements(cube.findDimension("OperateurDim").findAttribute("Operateur").children);
			rowQueryAxis.addSet(operateurSet);
			var colQueryAxis:IOLAPQueryAxis = query.getAxis(OLAPQuery.COLUMN_AXIS);
			var periodeSet:OLAPSet = new OLAPSet;
			// gestion du label de la colonne problématique
			var period_name:String = resourceManager.getString('M511', 'P_riod');
			if (!period_name)
			{
				period_name = "Period";
			}
			if (olapPeriodAttr && olapPeriodAttr.name != period_name)
			{
				olapPeriodAttr.name = period_name;
			}
			if (olapPeriodAttr)
			{
				if (olapPeriodAttr.name != "Période")
				{
					periodeSet.addElements(cube.findDimension("PeriodeDim").findAttribute("Period").children);
					colQueryAxis.addSet(periodeSet);
				}
				else
				{
					periodeSet.addElements(cube.findDimension("PeriodeDim").findAttribute("Period").children);
					colQueryAxis.addSet(periodeSet);
				}
			}
			return query;
		}
		
		public function runQuery(event:CubeEvent):void
		{
			if (dataProvider && dataProvider.length > 0)
			{
				var cube:IOLAPCube = IOLAPCube(event.currentTarget);
				var query:IOLAPQuery = getQuery(cube);
				var token:AsyncToken = cube.execute(query);
				token.addResponder(new AsyncResponder(showResult, showFault));
			}
		}
		
		private function showFault(error:ErrorMessage, token:Object):void
		{
			trace(error.faultString);
		}
		
		private function showResult(result:Object, token:Object):void
		{
			if (!result)
			{
				return;
			}
			olapPeriodAttr.displayName = resourceManager.getString('M511', 'P_riod');
			myOLAPDG.dataProvider = result as OLAPResult;
			myOLAPDG.validateNow();
			if (myOLAPDG.columns.length > 0)
				myOLAPDG.columns[0].width = 140;
		}
		
		public function set boolDataUpdated(value:Boolean):void
		{
			_boolDataUpdated = value;
		}
		
		public function get boolDataUpdated():Boolean
		{
			return _boolDataUpdated;
		}
	}
}