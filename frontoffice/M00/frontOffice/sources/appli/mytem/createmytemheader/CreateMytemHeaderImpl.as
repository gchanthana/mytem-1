package appli.mytem.createmytemheader
{

	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.containers.Canvas;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.Spacer;
	import mx.core.IFlexDisplayObject;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	
	import appli.impersonnification.refreshManager;
	import appli.impersonnification.event.RelogEvent;
	import appli.impersonnification.popup.PopupChooseOtherAccountIHM;
	import appli.mytem.createmenumytem.CreateMenuMytemIHM;
	import appli.mytem.createmenumytem.CreateMenuMytemImpl;
	
	import composants.util.ConsoviewAlert;
	
	import general.appliComposants.CvPerimetreNodeVO;
	import general.appliComposants.ClickableCheminPerimetre.ClickableCheminPerimetreIHM;
	import general.appliComposants.ListePerimetreWindow.ListePerimetresWindow;
	import general.appliComposants.parametresuser.PopUpInfosUserIHM;
	import general.event.PerimetreEvent;
	import general.event.UserEvent;
	import general.interfaceClass.IMenu;
	import general.service.InfosChargementService;
	
	import rapport.vo.ApiReportingSingleton;

	public class CreateMytemHeaderImpl extends Canvas implements IMenu
	{
		
		[Bindable]
		protected var moduleRapportEnable:Boolean;

		[Bindable]
		protected var BACKGROUND_COLOR:uint=new uint(333333);

		[Bindable] public var hbheader:HBox;
		[Bindable] public var spChangeAccount:Spacer;
		[Bindable]
		public var boxRapport:VBox;

		[Bindable]
		protected var chemin:String;

		[Bindable]
		public var infosChargementService:InfosChargementService;

		public var hbMenu:HBox; /** contient le menu */
		public var btnModifierPerimetre:Button;
		public var ccp:ClickableCheminPerimetreIHM; // afficher le label du perimètre
		public var lblLoggedUserName:Label;
		public var popupInfosUser : PopUpInfosUserIHM;
		public var lblChangeAccount:Label;

		private var _boolAfficherDates:Boolean;

		private var listePerimetreWindow:ListePerimetresWindow;

		public var imageService:HTTPService;

		private var timeStampForNocache:Date=new Date();

		private var params:Object={};
		
		public var popupChooseOtherAccount:PopupChooseOtherAccountIHM = new PopupChooseOtherAccountIHM();

		[Bindable]
		public var urlImage:String=cv.urlLogoHeader+"/"+ CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX + ".png";

		public var labelReport:Label;
		public var labelHelp:Label;
		public var lbl_logout:Label;

		/** Embed image permet de charger un resource ( image) dans la memoire et creer une reference de type class qui pointe sur l'image*/

		[Embed(source="/assets/menu/HomeGraphical.swf", symbol="header104")]

		[Bindable]
		public var header:Class;

		[Embed(source="/assets/menu/HomeGraphical.swf", symbol="backGroundBlue")]

		[Bindable]
		public var backGroundBlueImage:Class;

		[Bindable]
		public var urlLogo:String;

		public function CreateMytemHeaderImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * initialiser l'IHM avec le login de l'utilisateur connecté et le xml necessaire pour la creation du menu
		 * @param e
		 */
		public function init(e:FlexEvent):void
		{
			infosChargementService=new InfosChargementService();
			infosChargementService.getInfosDates();
			listePerimetreWindow=new ListePerimetresWindow();
			if (refreshManager.impersonnification == false)
				lblLoggedUserName.text=ResourceManager.getInstance().getString('M00', 'Bonjour') + " " + CvAccessManager.getUserObject().PRENOM + " " + CvAccessManager.getUserObject().NOM; /** le label du login connecté*/
			else
			{
				refreshManager.currentAccountName = CvAccessManager.getUserObject().PRENOM + " " + CvAccessManager.getUserObject().NOM;
				lblLoggedUserName.text = ResourceManager.getInstance().getString('M00', 'Bonjour') + " " + refreshManager.getImpersString();
				hbheader.x = 430;
			}
			params.noCache=timeStampForNocache.getTime().toString();
			/** le params envoyer pour eviter le mise en cache de navigateur */
			imageService.send(params); /** permet d'envoyer une requete http pour savoir si le logo de la racine connecté existe bien sur le serveur */

			if (CvAccessManager.getSession().USER.EMAIL.indexOf("@saaswedo.com") == -1 && refreshManager.impersonnification == false)
			{
				trace ("Suppression du label permettant de changer de compte : " + refreshManager.impersonnification);
				hbheader.removeChild(lblChangeAccount);
				hbheader.removeChild(spChangeAccount);
				hbheader.x = 605;
			}
			
			
			
		}

		/**
		 * permet de mettre a jour le logo de la racine
		 * cette methode est appelée lors que l'image de la racine existe bien sur le serveur
		 * @param evt
		 */
		public function resultHandler(evt:ResultEvent):void
		{
			if (evt.statusCode == 200)
			{
				urlLogo=cv.urlLogoHeader +"/"+ CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX + ".png";
			}
			else
			{
				urlLogo=cv.urlLogoHeader + "/logomytem360.png";
			}
		}

		public function faultHandler(evt:FaultEvent):void
		{

			if (CvAccessManager.getSession().CODE_APPLICATION == 1)
			{
				urlLogo=cv.urlLogoHeader + "/2458788.png";
			}
			else if (CvAccessManager.getSession().CODE_APPLICATION == 101)
			{
				urlLogo=cv.urlLogoHeader + "/logomytem360.png";
			}
			else
			{
				urlLogo=cv.urlLogoHeader + "/logomytem360.png";
			}
		}

		/**
		 * permet de actualiser le menu et les modules associés
		 *
		 */
		public function updatePerimetre():void
		{
			if (listePerimetreWindow.isPopUp)
			{
				PopUpManager.removePopUp(listePerimetreWindow);
			}
			updateMenu();
			listePerimetreWindow.updatePerimetre();
			initAffichagePerimetre();
			afterUniversFunctionUpdated("");
		}

		/**
		 * permet de mettre a jour le menu lors que le périmètre changer
		 * on appelle cette fonction dans la methode updatePerimetre
		 */
		public function updateMenu():void
		{
			var menuXML:XML=CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;

			var xmlList:XMLList=menuXML.children(); /** tous les enfants du root */

			var itemMenu:CreateMenuMytemIHM;

			hbMenu.removeAllChildren();

			for each (var item:XML in xmlList)
			{
				itemMenu=new CreateMenuMytemIHM();

				itemMenu.nomImageGris=item.@KEY + "_gris.png";
				itemMenu.nomImageBleu=item.@KEY + "_bleu.png";
				itemMenu.labelText=item.@LBL;
				itemMenu.toggle=item.@toggled;

				itemMenu.XMLUnivers=item; /** un morceau de le XML = le XML d'un item de menu */
				if (item.@USR == 1)
				{
					hbMenu.addChild(itemMenu);
				}
			}
		}

		/**
		 * afficher un popup pour changer le périmètre d'etude
		 * @param event
		 */
		public function displayListePerimetres(event:MouseEvent):void
		{
			PopUpManager.addPopUp(listePerimetreWindow, this.parentApplication as DisplayObject, true);
			PopUpManager.bringToFront(listePerimetreWindow);
			PopUpManager.centerPopUp(listePerimetreWindow);
		}


		public function afterUniversFunctionUpdated(actionType:String):void
		{
			if (CvAccessManager.CURRENT_UNIVERS == "HOME" || CvAccessManager.CURRENT_UNIVERS == "HOME_E0")
			{
				boolAfficherDates=true;
			}
			if (CvAccessManager.CURRENT_FUNCTION == "FACT_DASHBOARD_E0" || CvAccessManager.CURRENT_FUNCTION == "FACT_DASHBOARD_E0")
			{
				boolAfficherDates=true;
			}
			var accessData:XML=CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			if ((accessData.UNIVERS.(@KEY == 'FACT_REPORT_E0').@USR == 1) || (accessData.UNIVERS.(@KEY == 'FACT_REPORT_360').@USR == 1))
			{
				moduleRapportEnable=true;
			}
			else
			{
				moduleRapportEnable=false;
			}
		}

		/**
		 * fermer toutes les popup et se deconnecter
		 */
		public function logoff():void
		{
			refreshManager.impersonnification = false;
			closeAllPopups();
			CvAccessManager.deconnectSession();
		}

		/**
		 * fermer toutes les popup
		 */
		private function closeAllPopups():void
		{
			var len:Number=systemManager.numChildren - 1;
			var i:Number=len;

			while (i >= 0)
			{
				if (systemManager.getChildAt(i).hasOwnProperty("isPopUp"))
				{
					PopUpManager.removePopUp(IFlexDisplayObject(systemManager.getChildAt(i)));
				}
				i--;
			}
		}

		/**
		 * permet de renvoyer vers l'aide proposé par saaswedo
		 */
		protected function btnTEMClickHandler():void
		{
			this.goToHelp();
		}
		
		private function goToHelp():void
		{
				var url:String = cv.urlBackoffice + "/fr/consotel/consoview/help/index.cfm";
				var request:URLRequest=new URLRequest(url);
				request.method=URLRequestMethod.GET;
				navigateToURL(request, "_blank");
		}

		/**
		 * Fonction qui désaffiche les autres menus pour ne laisser que le menu de ce bouton affiché
		 * cette fonction est appelée dans createMenuMytemImpl
		 */
		public function refresh(btn:CreateMenuMytemImpl):void
		{
			for each (var bt:CreateMenuMytemIHM in hbMenu.getChildren())
			{
				if (bt != btn)
				{
					bt.AfficherMenu(false, btn.XMLUnivers); /**XMLUnivers  le XML de l'element selectionné du menu*/
				}
			}
		}

		public function refreshMenu():void
		{
			for each (var bt:CreateMenuMytemIHM in hbMenu.getChildren())
			{
				bt.refreshToggle();
			}
		}

		/**
		 * permet d'afficher le module rapport lors que l'on clique sur l'icon Rapport
		 */
		protected function btnRapportClickHandler():void
		{
			var current_module:String=CvAccessManager.CURRENT_FUNCTION;
			if ((CvAccessManager.CURRENT_FUNCTION != 'FACT_REPORT') && (CvAccessManager.CURRENT_FUNCTION != 'FACT_REPORT_E0') && (CvAccessManager.CURRENT_FUNCTION != 'FACT_REPORT_PILOTAGE') && (CvAccessManager.CURRENT_FUNCTION != 'FACT_REPORT_360'))
			{
				ApiReportingSingleton.getInstance().afficherPOpup();
			}
		}

		public function initAffichagePerimetre():void
		{
			var arr:Array=[]
			var sess:ConsoViewSessionObject=CvAccessManager.getSession() as ConsoViewSessionObject
			var idperimetre:int=sess.CURRENT_PERIMETRE.PERIMETRE_INDEX
			var nodeListXml:XML=sess.CURRENT_PERIMETRE.nodeList
			var xmlSelected:XMLList=nodeListXml.descendants("*").(@NID == idperimetre)
			var parentXml:Object=xmlSelected.parent()

			// Si nous sommes à la racine, on ajoute qu'un élément à l'affichage
			if (nodeListXml.@NID == idperimetre)
			{
				var x:CvPerimetreNodeVO=new CvPerimetreNodeVO()
				x.LBL=nodeListXml.@LBL
				x.NID=nodeListXml.@NID
				if (nodeListXml.@STC == 0)
					x.HASRIGHT=false
				else
					x.HASRIGHT=true
				arr.push(x)
			}
			// Si nous sommes en sous périmètre, on récupère le parent
			else
			{
				// on ajoute le premier élément
				var x1:CvPerimetreNodeVO=new CvPerimetreNodeVO()
				x1.LBL=xmlSelected.@LBL
				x1.NID=xmlSelected.@NID
				if (xmlSelected.@STC == 0)
					x1.HASRIGHT=false
				else
					x1.HASRIGHT=true
				arr.push(x1)
				// tant qu'un parent existe, on ajoute un élément 	
				while (parentXml != null)
				{
					if (parentXml.@NID > 0)
					{
						var x2:CvPerimetreNodeVO=new CvPerimetreNodeVO()
						x2.LBL=parentXml.@LBL
						x2.NID=parentXml.@NID
						if (parentXml.@STC == 0)
							x2.HASRIGHT=false
						else
							x2.HASRIGHT=true
						arr.push(x2)
					}
					parentXml=parentXml.parent()
				}
			}
			ccp.updateAffichage(arr);
		}
		
		/**
		 * Show popup to change password
		 */
		protected function onClickInfosUserHandler(event:MouseEvent):void
		{
			popupInfosUser = new PopUpInfosUserIHM();
			PopUpManager.addPopUp(popupInfosUser, this.parent, true);
			addEventListener(UserEvent.USER_INFOS_UPDATED_EVENT, confirmUpdate);
		}
		
		protected function confirmUpdate(event:UserEvent):void
		{
			trace("########## CreateMytemHeaderImpl.confirmUpdate(event)");
			ConsoviewAlert.afficherOKImage("Password updated");
			
		}
		
		/**
		 * changer la couleur de label
		 * @param event
		 */
		public function label_mouseOverHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#000000'); /**set*/
		}

		public function label_mouseOutHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#ffffff');
		}
		
		public function chooseOtherAccount():void
		{
			PopUpManager.addPopUp(popupChooseOtherAccount, this, true);
			PopUpManager.centerPopUp(popupChooseOtherAccount);
			
			popupChooseOtherAccount.addEventListener(RelogEvent.LOGIN_CHOSEN, accountChosenHandler);
		}
		public function chooseOtherAccount_mouseOverHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#358dd7'); /**set*/
		}
		public function chooseOtherAccount_mouseOutHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#ffffff');
		}
		private function refreshName():void
		{
			trace("refresh en cours");
			refreshManager.refreshApp();
		}
		public function accountChosenHandler(evt:Event):void
		{
			trace("accountChosenHandler()");
			refreshName();
		}
		
		public function deconnexion_mouseOverHandler(evt:MouseEvent):void
		{
			lbl_logout.setStyle('color', '#358dd7'); 
		}
		
		public function deconnexion_mouseOutHandler(evt:MouseEvent):void
		{
			lbl_logout.setStyle('color', '#ffffff');
		}
		
		
		protected function userInfosMouseOverHandler(evt:MouseEvent):void
		{
			lblLoggedUserName.setStyle('color', '#358dd7'); 
		}
		
		protected function userInfosMouseOutHandler(evt:MouseEvent):void
		{
			lblLoggedUserName.setStyle('color', '#ffffff');
			
		}
		/** ------------------------------------------------- getter et setter ---------------------------*/

		public function getSelectedNode():XML
		{
			return listePerimetreWindow.getSelectedNode();
		}

		[Bindable]
		public function set boolAfficherDates(value:Boolean):void
		{
			_boolAfficherDates=value;
		}

		public function get boolAfficherDates():Boolean
		{
			return _boolAfficherDates;
		}
	}
}
