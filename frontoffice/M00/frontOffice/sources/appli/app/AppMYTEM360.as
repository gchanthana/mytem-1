package appli.app
{
	import appli.control.AppControlEvent;
	import appli.impersonnification.refreshManager;
	import appli.mytem.CreateMytemIHM;
	
	import entity.AppParams;
	
	import general.abstract.AbstractApplication;
	import general.appliComposants.UtilM00;
	import general.entity.Constantes;
	import general.event.UtilM00Event;
	import general.helper.QueryString;
	
	import rapport.vo.ApiReportingSingleton;
	
	public class AppMYTEM360 extends AbstractApplication
	{
		public function AppMYTEM360()
		{
			
			var api:ApiReportingSingleton = new ApiReportingSingleton();
			
			super();
			refreshManager.setChosenAppMytem360(this);
		}
		override public function deconnect(evt:AppControlEvent=null):void
		{
			
			if(params.codeConnection == Constantes.CODECONNECTION_SSO_SESOUVENIR)
			{
				params.codeConnection = Constantes.CODECONNECTION_INTERNATIONAL;
				super.deconnect(evt);
				init(params);
			}
			else
			{
				super.deconnect(evt);
				afficherEcranLogin();
			}
		}
		/**
		 *	définit les paramètres de l'application
		 * 
		 * 	Si le paramètre "codeConnection" ou "cc" est présent dans l'URL, alors le codeConnection d'AppParam est ce paramètre.
		 * 	Sinon il est égal à Constantes.CODECONNECTION_INTERNATIONAL 
		 * 
		 */
		override public function init(param:AppParams):void
		{
			this.params = param;
			
			// DEFINITION DU CODE CONNECTION
			if(param.codeConnection == 0)
			{
				var codeConnec:Object 	= UtilM00.getInstance().recupUrlParam(["codeConnection","cc","codeconnection"]);
				var token:Object = UtilM00.getInstance().recupUrlParam(["token"]);
				//trying to find token by regular querystring
				var qs:QueryString = new QueryString();
				
				if (qs.parameters.cc != null && qs.parameters.token != null)
				{
						param.codeConnection = Number(qs.parameters.cc) as int ;
						trace(param.codeConnection);
						param.token = qs.parameters.token as String;
						trace(param.token);
						super.init(param);
						
				}
				else if( codeConnec != null && codeConnec != "")
				{
					
					param.codeConnection = codeConnec as int;
					trace(codeConnec);


					if( token != null
					&& token != "")
					{
						param.token = token as String;
						trace(token);
					}

					super.init(param);

					
				}
				else
				{
					var listeParamSSO:Array = ["CLESSO101","CléSSO101"];
					UtilM00.getInstance().addEventListener(UtilM00Event.FINDCOOKIE_EVENT,init2);
					UtilM00.getInstance().findCookieParam(listeParamSSO);
				}
			}
			else
			{
				super.init(this.params);
			}
		}
		private function init2(evt:UtilM00Event):void
		{
			var codeSSO:Object = UtilM00.getInstance().valueCookie; 
			if(codeSSO != null
				&& codeSSO is String
				&& codeSSO.toString().length > 0)
			{
				this.params.codeConnection = Constantes.CODECONNECTION_SSO_SESOUVENIR;
				this.params.token = codeSSO.toString();
			}
			else
				this.params.codeConnection = Constantes.CODECONNECTION_INTERNATIONAL;
			
			super.init(this.params);
		}
 		override protected function defineDisplay():void
		{			
			this.compIHM = new CreateMytemIHM();
			
			super.defineDisplay();
		}
	}
}