package appli.app
{
	import appli.Pilotage.CreatePFGPIHM;
	import appli.connection.ConnectionLoginMDPfrance;
	import appli.connection.ConnectionSSOSeSouvenir;
	import appli.connection.ConnectionSSOsfr;
	import appli.control.AppControlEvent;
	import appli.events.ConnectionEvent;
	import appli.impersonnification.refreshManager;
	
	import entity.AppParams;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	import general.abstract.AbstractApplication;
	import general.appliComposants.UtilM00;
	import general.entity.Constantes;
	import general.event.UtilM00Event;
	
	import interfaceClass.IApplication;
	
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.utils.StringUtil;
	
	import rapportPilotage.vo.ApiReportingSingleton;
	
	import styles.ConsoViewStyleManager;
	
	public class AppPilotage extends AbstractApplication implements IApplication
	{
		public function AppPilotage()
		{
			trace("AppPilotage.AppPilotage()");
			
			var api:ApiReportingSingleton = new ApiReportingSingleton();
			
			super();
			refreshManager.setChosenAppPilotage(this);
		}
		/**
		 *	<code>deconnect</code>
		 * 
		 * 	Gère la déconnection de l'appli.
		 * 
		 * 	Dans le cas où la première connection à l'application est en SSO, on spécifie que dorénavant la connection sera de manière classique ( écran de login etc ...) 
		 * 
		 */
		override public function deconnect(evt:AppControlEvent=null):void
		{
			trace("AppPilotage.deconnect(evt)");
			
			if(params.codeConnection == Constantes.CODECONNECTION_SSO_SFR
			|| params.codeConnection == Constantes.CODECONNECTION_SSO_SESOUVENIR)
			{
				params.codeConnection = Constantes.CODECONNECTION_CLASSIQUE;
				super.deconnect(evt);
				init(params);
			}
			else
			{
				super.deconnect(evt);
				afficherEcranLogin();
			}
		}
		/**
		 *	définit les paramètres de l'application
		 * 
		 * 	Si le codeconnection est celui par défaut "0", alors on le cherche dans l'URL.
		 * 	Si le paramètre "token" est présent dans l'URL, on précise bien que le type de connection est SSO, sinon classique. 
		 * 
		 */
		override public function init(param:AppParams):void
		{
			trace("AppPilotage.setParams(param)");
			this.params = param;
			
			this.params = param;
			
			// DEFINITION DU CODE CONNECTION ET DU TOKEN SI IL EST TROUVE
			if(param.codeConnection == 0)
			{
				var boolParamFound:Boolean = false;
				var tokenParam:Object 	= UtilM00.getInstance().recupUrlParam(["token","TOKEN"]);
				if(tokenParam != null)
				{
					if(verifToken(tokenParam.toString()))
					{
						param.token = tokenParam.toString();
						param.codeConnection = Constantes.CODECONNECTION_SSO_SFR;
						boolParamFound = true;
						super.init(param);
					}
				}
				if(!boolParamFound)
				{
					var listeParamSSO:Array = ["CLESSO51","CléSSO51","cleSSO51","CLESSO51"];
					UtilM00.getInstance().addEventListener(UtilM00Event.FINDCOOKIE_EVENT,init2);
					UtilM00.getInstance().findCookieParam(listeParamSSO);
				}
			}
			else
			{
				super.init(this.params);
			}
		}
		private function init2(evt:UtilM00Event):void
		{
			var codeSSO:Object = UtilM00.getInstance().valueCookie; 
			if(codeSSO != null
				&& codeSSO is String
				&& codeSSO.toString().length > 0)
			{
				this.params.codeConnection = Constantes.CODECONNECTION_SSO_SESOUVENIR;
				this.params.token = codeSSO.toString();
			}
			else
				this.params.codeConnection = Constantes.CODECONNECTION_CLASSIQUE;
			
			super.init(this.params);
		}
		override protected function defineDisplay():void
		{
			trace("AppPilotage.defineDisplay()");
			
			this.compIHM = new CreatePFGPIHM();
			
			super.defineDisplay();
		}
		override public function processAppStyle():void
		{
			trace("AppPilotage.processAppStyle()");
			
			ConsoViewStyleManager.changeStyle(ConsoViewStyleManager.SFR_STYLE);
		}
		override protected function initDisplay():void
		{
			trace("AppPilotage.initDisplay()");
			
			
			this.setStyle("horizontalAlign","center");
			this.setStyle("verticalAlign","middle");
			
			this.width = 1000;
			this.height = 900;
			
			this.setStyle("backgroundAlpha","0");
			this.setStyle("backgroundColor","#FFFFFF");
			
			this.setStyle("verticalScrollPolicy","off");
			this.setStyle("horizontalScrollPolicy","off");
		}
		/**
		 *	<code>verifToken</code>
		 * 
		 * 	Vérifie le paramètre Token envoyé à l'appli : 
		 * 		- nombre de caractère = 36
		 * 		- nombre de partie = 5
		 * 		- taille des parties dans l'ordre : 8 4 4 4 12
		 * 	Exemple de token valide : 28B5B4D2-1C3B-F26C-FAF4-97FFC1803A1C
		 * 
		 */
		private function verifToken(token:String):Boolean
		{
			trace("AppPilotage.verifToken(token)");
			
			if(token!=null
				&& token.length == 36)
			{
				var arrTmp:Array = token.split("-");
				if(arrTmp.length == 5)
				{
					if(		arrTmp[0].toString().length == 8
						&&  arrTmp[1].toString().length == 4
						&&  arrTmp[2].toString().length == 4
						&&  arrTmp[3].toString().length == 4
						&&  arrTmp[4].toString().length == 12)
						return true;
				}
			}
			return false;
		}
	}
}