package appli.app
{
	import appli.ConsoleGestion.CreateConsoleGestionIHM;
	import appli.control.AppControlEvent;
	import appli.impersonnification.refreshManager;
	
	import entity.AppParams;
	
	import flash.events.Event;
	
	import general.abstract.AbstractApplication;
	import general.entity.Constantes;
	
	import interfaceClass.IApplication;
	
	import styles.ConsoViewStyleManager;
	
	public class AppConsoleGestion extends AbstractApplication implements IApplication
	{
		public function AppConsoleGestion()
		{	
			super();
			refreshManager.setChosenAppCG(this);
		}
		
		override public function init(param:AppParams):void
		{
			this.params = param;
			
			this.params.codeConnection = Constantes.CODECONNECTION_CONSOLE_GESTION_INTERNATIONAL;
			
			super.init(param);
		}
		
		override protected function defineDisplay():void
		{
			this.compIHM = new CreateConsoleGestionIHM();
			
			super.defineDisplay();
		}		
		override protected function initDisplay():void
		{
			this.percentHeight = 100;
			this.percentWidth = 100;
		}

		override public function changeModule(evt:AppControlEvent):void
		{
			var menuDataProvider:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			var toggledNodes:XMLList = menuDataProvider.descendants("*").(@toggled == 'true');
			var selectedNode:XMLList = menuDataProvider.descendants("*").(@KEY == evt.key );
			
			// on mets tous les univers / function en toggled = false
			for(var i:int=0; i < toggledNodes.length(); i++)
			{
				toggledNodes[i].@toggled = false;
			}
			// dans le cas d'un univers =>  toggled = true
			// dans le cas d'une fonction => univers et function en toggled = true
			selectedNode.@toggled = 'true';
			if(selectedNode.parent().@KEY != "ROOT")
				selectedNode.parent().@toggled = 'true';
			// On demande au Display d'afficher le module ( comportement par défaut de la classe Abstract
			
			compIHM.displayModule(evt.key as String);
		}
		override public function processAppStyle():void
		{
			ConsoViewStyleManager.changeStyle("cg");
		}
		
		override public function deconnect(evt:AppControlEvent=null):void
		{
			super.deconnect(evt);
			afficherEcranLogin();
		}
		
		public function refreshDisplay():void
		{
			defineDisplay();
		}
	}
}