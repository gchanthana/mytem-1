package appli.connection
{
	import mx.core.Container;
	
	import composants.ConsoProgressBar.ConsoProgressBar;
	
	import entity.AppParams;
	
	import general.abstract.AbstractConnection;
	
	import interfaceClass.IConnection;
	
	public class ConnectionLoginMDPInternational extends AbstractConnection implements IConnection
	{
		public function ConnectionLoginMDPInternational(progress:ConsoProgressBar)
		{
			super(progress);
		}
	// PUBLIC
		override public function goLogin(param:AppParams, container:Container = null):void
		{
			trace("ConnectionCVInternational.goLogin(param, container)");
			
			super.goLogin(param,container);
			
			loadModule("login");
		}
		override public function validateLogin(objLogin:Object):void
		{
			super.validateLogin(objLogin);
			loginService.login(params.login,params.mdp,params.codeApp,params.codeConnection,params.codeLangue,params.seSouvenir);
		}
	}
}	