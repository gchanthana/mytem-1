package general.strategy
{
	import flash.external.ExternalInterface;
	
	import general.appliComposants.UtilM00;
	
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.resources.ResourceManager;

	public class StrategyCodeApp
	{
		public function StrategyCodeApp()
		{
		}
		public function execute():int
		{
			try
			{
				var cdAppExt:int = getCodeAppByExternalInterface();
				var cdAppUrl:int = getCodeAppByParamURL();
				var cdAppFv:int = getCodeAppByFlashVars();
				
				if( cdAppExt > 0)
				{
					return cdAppExt;
				}
				else if(cdAppUrl > 0)
				{
					return cdAppUrl	
				}
				else if(cdAppFv > 0)
				{
					return cdAppFv;
				}
				else
					throw new Error("Aucune application trouvée : CodeApp introuvable");
			}
			catch(error:Error)
			{
				Alert.show(error.message,ResourceManager.getInstance().getString("M00","Erreur"));
			}
			return -1;
		}
		private function getCodeAppByFlashVars():int
		{
			try
			{
				var resObj:Object = UtilM00.getInstance().recupFlashVarsParam(["codeApp","codeapp","CODEAPP","ca"]);
				if(resObj  != null)
				{
					return parseInt(resObj as String);
				}
			} 
			catch(error:Error) 
			{
			}
			return -1;
		}
		private function getCodeAppByExternalInterface():int
		{
			try
			{
				return ExternalInterface.call("getCodeApp");	
			} 
			catch(error:Error) 
			{
			}
			return -1;
		}
		private function getCodeAppByParamURL():int
		{
			try
			{
				var objParam:Object = UtilM00.getInstance().recupUrlParam(["codeApp","codeapp","CODEAPP","ca"]);
				if(objParam != null)
				{
					return objParam as int;
				} 
			} 
			catch(error:Error) 
			{
			}
			return -1;
		}
	}
}