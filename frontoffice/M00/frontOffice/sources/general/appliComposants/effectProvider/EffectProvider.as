package general.appliComposants.effectProvider
{
	import mx.effects.Dissolve;
	
	public class EffectProvider
	{
		

        // Click event listener that passes the target component
        // and the coordinates of the center of the parent container
        //     to the function that creates the effect. 
        private static function playFade(target:Object,delay : int = 1000):void {
			var myFade:Dissolve = new Dissolve();
            myFade.end();
            myFade.target=target;
            myFade.duration = delay;
            myFade.alphaFrom = 0;
            myFade.alphaTo = 1;              
            myFade.play();
        }
        
        // Click event listener that passes the target component
        // and the coordinates of the center of the parent container
        //     to the function that creates the effect. 
        private static function playUnFade(target:Object,delay : int = 1000):void {
			var myFade:Dissolve = new Dissolve();
            myFade.end();
            myFade.target=target;
            myFade.duration = delay;
            myFade.alphaFrom = 1;
            myFade.alphaTo = 0;              
            myFade.play();
        }
        
        private static function playRoll():void {
        	
        }

        // Create the Move effect and play it on the target 
        // component passed to the function.
        public static function FadeThat(Obj:Object,delay : int = 1000):void{
        	
            var targetComponent:Object = Obj;               
            playFade(Obj,delay); 
        }
        
        // Create the Move effect and play it on the target 
        // component passed to the function.
        public static function UnFadeThat(Obj:Object,delay : int = 1000):void{
        	
            var targetComponent:Object = Obj;               
            playFade(Obj,delay); 
        }
        
        public static function zoomThat(Obj:Object,playRevers : Boolean):void{
        		
        }
        
        
	}
}