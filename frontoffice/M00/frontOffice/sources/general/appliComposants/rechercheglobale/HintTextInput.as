package general.appliComposants.rechercheglobale
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.controls.TextInput;
	
	[Event(name="click", type="flash.events.Event")]
	
	public class HintTextInput extends TextInput
	{
		private var _textIsEmpty:Boolean=true;
		private var _hint:String;
		
		public var image:Image;
		
		[Embed(source="/assets/menu/HomeGraphical.swf", symbol="search")]
		
		[Bindable]
		private var searchIcon:Class;
		
		
		/**
		 * pour afficher recherche lors que le textinput est vide
		 */
		public function get hint():String
		{
			return _hint;
		}

		public function set hint(value:String):void
		{
			_hint=value;
			toolTip=value;
			updateContents();
		}

		[Bindable]
		override public function get text():String
		{
			if (_textIsEmpty)
			{
				return "";
			}
			else
			{
				return super.text;
			}
		}

		override public function set text(value:String):void
		{
			if (value == "")
			{
				_textIsEmpty=true;
			}
			else
			{
				_textIsEmpty=false;
				super.text=value;
			}
			updateContents();
		}
		
		/**
		 * permet de positioner le hint dans le textInput
		 */
		private function updateContents():void
		{
			if (_textIsEmpty)
			{
				setStyle("color", 0xaaaaaa);//couleur du hint
				setStyle("paddingLeft", '10');
				setStyle("paddingTop", '3');
				super.text=_hint;
			}
			else
			{
				setStyle("color", 0x000000);
			}
		}


		private function handleTextChange(event:Event):void
		{
			if (super.text == "")
			{
				_textIsEmpty=true;
			}
			else
			{
				_textIsEmpty=false;
			}
		}
		
		/**
		 * permet de definir le style lorque le textInput est focus 
		 */
		private function handleFocusIn(event:FocusEvent):void
		{
			if (_textIsEmpty)
			{
				super.text="";
				setStyle("color", 0x000000);
			}
		}

		private function handleFocusOut(event:FocusEvent):void
		{
			updateContents();
		}

		override public function initialize():void
		{
			super.initialize();
			addEventListener(Event.CHANGE, handleTextChange);
			addEventListener(FocusEvent.FOCUS_IN, handleFocusIn);
			addEventListener(FocusEvent.FOCUS_OUT, handleFocusOut);
			updateContents();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			image = new Image();
			image.source = searchIcon;
			
			addChild(DisplayObject(image));
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			//taille de l'image search (loupe)
			this.image.width = 14;
			this.image.height = 14;
					
			// positionnement de l'image
			this.image.x = this.width - this.image.width -10;
			this.image.y = this.height - this.image.height-5;
			
			// permet de rendre l'image cliquable
			this.image.mouseChildren=false;
			this.image.useHandCursor=true;
			this.image.buttonMode=true;
			//this.image.addEventListener(MouseEvent.CLICK, handleClickImage); // quand on clique sur la loupe					
			this.textField.width = this.width - this.image.width - 23; // permet de déterminer l'espace disponible pour la saisie du text 
		}	
	}
}
