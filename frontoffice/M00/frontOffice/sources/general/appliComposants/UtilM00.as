package general.appliComposants
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import general.event.UtilM00Event;
	
	import mx.core.Application;
	import mx.managers.BrowserManager;
	import mx.managers.IBrowserManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.URLUtil;

	public class UtilM00 extends EventDispatcher
	{
		private var _valueCookie:Object;
		private static var _instance:UtilM00;
		
		public function UtilM00()
		{
			if(_instance == null)
				_instance = this;
		}
		/**
		 * 	Cette fonction permet de rechercher la première valeur d'un paramètre selon plusieurs orthographes, dans l'URL.
		 * 
		 * 	Par exemple : on recherche le code connection passé dans l'URL. 
		 * 	On sait que le param peut être écrit de plusieurs façons : cc, codeconnection et codeConnection.
		 * 	On passe alors un tableau avec ces 3 chaines de caractère. ["codeConnection","cc","codeconnection"]
		 * 	Dés que l'on trouve une des valeurs, on la retourne.
		 */
		public function recupUrlParam(listeProperty:Array):Object
		{ 
			if(listeProperty != null)
			{
				var str:String = ""
				
				var bm:IBrowserManager = BrowserManager.getInstance(); 
				if(bm != null)
				{	
					bm.init();
					if(bm.url != null && bm.url != "")
					{	
						str = bm.url.substr(bm.url.indexOf("?")+1,bm.url.length);
						var objParam:Object = URLUtil.stringToObject(str, "#");
						
						if(objParam != null)
						{
							for each(var strParam:String in listeProperty)
							{
								if( objParam.hasOwnProperty(strParam)
									&& objParam[strParam] != null
									&& objParam[strParam] != "")
								{
									return objParam[strParam];
								}
							}
						}    
					}
				}
			}
			return null;
		}
		/**
		 * 	Cette fonction permet de rechercher la première valeur d'un paramètre selon plusieurs orthographes dans les flashvars.
		 * 
		 * 	Par exemple : on recherche le code app passé par flashvar. 
		 * 	On sait que le param peut être écrit de plusieurs façons : codeApp, codeApp.
		 * 	On passe alors un tableau avec ces 2 chaines de caractère. ["codeApp","codeApp"]
		 * 	Dés que l'on trouve une des valeurs, on la retourne.
		 */
		public function recupFlashVarsParam(listeProperty:Array):Object
		{
			if(listeProperty != null)
			{
				var str:String = ""
				
				for each(var strParam:String in listeProperty)
				{
					var app:Object = Application.application;
					if(Application.application.parameters.hasOwnProperty(strParam)
						&& Application.application.parameters[strParam] != null
						&& Application.application.parameters[strParam] != ""	)
					{
						return Application.application.parameters[strParam];
					}
				}
			}
			return null;
		}
		/**
		 * 	Cette fonction permet de rechercher la première valeur d'un paramètre selon plusieurs orthographes dans un cookie.
		 * 
		 * 	Par exemple : on recherche le code app passé par flashvar. 
		 * 	On sait que le param peut être écrit de plusieurs façons : codeApp, codeApp.
		 * 	On passe alors un tableau avec ces 2 chaines de caractère. ["codeApp","codeApp"]
		 * 	Dés que l'on trouve une des valeurs, on la retourne.
		 */
		public function findCookieParam(listeProperty:Array):void
		{
			if(listeProperty != null)
			{
				var roCookie:AbstractOperation = RemoteObjectUtil.getOperation(
									"fr.consotel.consoview.M00.connectApp.AppUtil", 
									"getParamFromCookie", 
									fctHandler
									);
				RemoteObjectUtil.callService(roCookie,listeProperty);
			}
		}
		private function fctHandler(evt:ResultEvent):void
		{
			if(evt.result != null)
			{
				trace(evt.result.toString());
				_valueCookie = evt.result
			}
			else
				_valueCookie = null;
			UtilM00.getInstance().dispatchEvent(new UtilM00Event(UtilM00Event.FINDCOOKIE_EVENT));
		}
		public function get valueCookie():Object
		{
			return _valueCookie;
		}
		public function set valueCookie(value:Object):void
		{
			_valueCookie = value;
		}
		public static function getInstance():UtilM00
		{
			return _instance;
		}
	}
}