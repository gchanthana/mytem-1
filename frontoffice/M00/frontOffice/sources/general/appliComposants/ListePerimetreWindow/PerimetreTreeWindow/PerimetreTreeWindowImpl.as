package general.appliComposants.ListePerimetreWindow.PerimetreTreeWindow {
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import general.event.ConsoViewDataEvent;
	import general.event.PerimetreTreeEvent;
	import general.interfaceClass.INodeInfos;
	import general.interfaceClass.IPerimetreTreeWindow;
	
	import mx.events.FlexEvent;
	
	public class PerimetreTreeWindowImpl extends PerimetreTreeWindow implements IPerimetreTreeWindow {
		/**
		 * Si true alors la racine est toujours le noeud sur lequel
		 * on est connecté dans l'application
		 * Si false alors la racine est toujours la racine de l'arbre des périmètres
		 * */
		private var _connectedNodeIsRoot:Boolean = true;
		/**
		 * Si true alors à la séléction d'un noeud l'arbre récupère les infos supplémentaires
		 * concernant le noeud en question. Ces infos sont :
		 * Les infos du noeud sont obtenues par appel à get nodeInfos()
		 * Si false alors la séléction ne fait rien et la get nodeInfos() renvoie null
		 * */
		private var _loadDataOnSelection:Boolean = false;
		/**
		 * Si true alors le groupe racine est toujours affiché
		 * Sinon c'est le 1er noeud disponible dans l'organisation opérateur qui est
		 * affiché comme racine et qui est séléctionné
		 * */
		private var _showGroupeRacine:Boolean = true;
		
		public function PerimetreTreeWindowImpl() {
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		public function get connectedNodeIsRoot():Boolean {
			return _connectedNodeIsRoot;
		}
		
		public function set connectedNodeIsRoot(value:Boolean):void {
			_connectedNodeIsRoot = value;
		}
		
		public function get loadDataOnSelection():Boolean {
			return _loadDataOnSelection;
		}
		
		public function set loadDataOnSelection(value:Boolean):void {
			_loadDataOnSelection = value;
		}
		
		public function get showGroupeRacine():Boolean {
			return _showGroupeRacine;
		}
		
		public function set showGroupeRacine(value:Boolean):void {
			_showGroupeRacine = value;
		}
		
		public function onPerimetreChange():void {
			if(this.initialized) {
				searchInput.text = "";
				perimetreTree.onPerimetreChange();
			}
		}
		
		
		override protected function commitProperties():void{
			
		}
		public function onChangePerimetre(evt:Event):void
		{
			
		}
		private function initIHM(event:FlexEvent):void {
			searchInput.addEventListener(KeyboardEvent.KEY_DOWN,keyPerformSearch);
			nodeSearchItem.addEventListener(MouseEvent.CLICK,performSearch);
			perimetreTree.addEventListener(PerimetreTreeEvent.SELECTED_ITEM_CHANGE,onTreeChange);
			perimetreTree.addEventListener(PerimetreTreeEvent.NODE_INFOS_RESULT,onNodeInfosResult);
			
			perimetreTree.connectedNodeIsRoot = connectedNodeIsRoot;
			perimetreTree.loadDataOnSelection = loadDataOnSelection;
			perimetreTree.showGroupeRacine = showGroupeRacine;
			perimetreTree.initTree();
		}
		
		protected function onTreeChange(event:PerimetreTreeEvent):void {
			dispatchEvent(new PerimetreTreeEvent(PerimetreTreeEvent.SELECTED_ITEM_CHANGE));
		}
		
		public function getSelectedItem():Object {
			return perimetreTree.getSelectedItem();
		}
		
		protected function onNodeInfosResult(event:PerimetreTreeEvent):void {
			dispatchEvent(new PerimetreTreeEvent(PerimetreTreeEvent.NODE_INFOS_RESULT));
		}
		
		public function get nodeInfos():INodeInfos {
			return perimetreTree.nodeInfos;
		}
		
		private function keyPerformSearch(event:Event):void {
			if((event as KeyboardEvent).keyCode == 13) {
				var paramObject:Object = new Object();
				if(perimetreTree.selectedItem != null)
					paramObject.NID = perimetreTree.selectedItem.@NID;
				else
					paramObject.NID = 0;
				paramObject.KEY = searchInput.text;
				dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.PERFORM_NODE_SEARCH,paramObject));
			}
		}
		
		private function performSearch(event:Event):void {
			var paramObject:Object = new Object();
			if(perimetreTree.selectedItem != null)
				paramObject.NID = perimetreTree.selectedItem.@NID;
			else
				paramObject.NID = 0;
			paramObject.KEY = searchInput.text;
			dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.PERFORM_NODE_SEARCH,paramObject));
		}
	}
}
