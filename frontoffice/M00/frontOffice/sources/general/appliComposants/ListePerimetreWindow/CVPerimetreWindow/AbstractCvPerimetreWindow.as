package general.appliComposants.ListePerimetreWindow.CVPerimetreWindow {
	import general.interfaceClass.IPerimetreTreeWindow;
	import general.interfaceClass.ISearchPerimetreWindow;
	
	import mx.containers.Box;
	import mx.containers.TitleWindow;
	import mx.events.FlexEvent;

	public class AbstractCvPerimetreWindow extends TitleWindow {
		public var perimetreTree:IPerimetreTreeWindow; // Composant contenant l'IHM de l'arbre des périmètres
		public var searchTree:ISearchPerimetreWindow; // Composant contenant l'IHM de l'arbre de recherche
		
		public function AbstractCvPerimetreWindow() {
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		protected function initIHM(event:FlexEvent):void {
		}
	}
}