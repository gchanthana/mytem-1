package general.appliComposants.NodeRenderer
{
	import general.event.NodeEvent;
	
	import cv.CvPerimetreNodeVO;
	
	import flash.events.MouseEvent;
	
	import mx.containers.HBox;
	import mx.controls.Label;
	import mx.controls.LinkButton;
	
	public class NodeRendererPFGPImpl extends HBox
	{
		private var _TITRE:String;
		private var _NID:int;
		private var _ENABLED:Boolean;
		private var _ISLAST:Boolean;
		
		[Bindable] public var lk:LinkButton
		[Bindable] public var lbl:Label
		
		public function NodeRendererPFGPImpl()
		{
		}

		public function lkClickHandler(e:MouseEvent):void
		{
			var node:CvPerimetreNodeVO = new CvPerimetreNodeVO()
			node.LBL = _TITRE
			node.NID = _NID
			node.HASRIGHT = _ENABLED
			dispatchEvent(new NodeEvent(NodeEvent.NODE_CHANGED_EVENT, node,true))
		}
		public function init():void
		{
			if(ENABLED)
			{
				if(ISLAST)
				{
					lk.visible = false
					lbl.visible = true
					lk.setStyle("textDecoration","none");
					if(lk.hasEventListener(MouseEvent.CLICK))
						lk.removeEventListener(MouseEvent.CLICK, lkClickHandler)
				}
				else
				{
					lk.visible = true
					lbl.visible = false
					lk.setStyle("textDecoration","underline");
					lk.addEventListener(MouseEvent.CLICK, lkClickHandler)
				}
			}
			else
			{
				lk.visible = false
				lbl.visible = true
				lk.setStyle("textDecoration","none");
				if(lk.hasEventListener(MouseEvent.CLICK))
					lk.removeEventListener(MouseEvent.CLICK, lkClickHandler)
			}
		}
/*	------------------------------------------------------------------------------------------
									GETTER SETTER 
------------------------------------------------------------------------------------------  */	
		public function set ENABLED(value:Boolean):void
		{
			_ENABLED = value;
		}

		[Bindable] public function get ENABLED():Boolean
		{
			return _ENABLED;
		}

		public function set NID(value:int):void
		{
			_NID = value;
		}

		[Bindable] public function get NID():int
		{
			return _NID;
		}

		public function set TITRE(value:String):void
		{
			_TITRE = value;
		}

		[Bindable] public function get TITRE():String
		{
			return _TITRE;
		}
		public function set ISLAST(value:Boolean):void
		{
			_ISLAST = value;
		}
		
		[Bindable] public function get ISLAST():Boolean
		{
			return _ISLAST;
		}
	}
}