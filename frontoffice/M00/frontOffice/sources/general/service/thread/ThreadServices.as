package general.service.thread
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	import utils.abstract.AbstractRemoteService;

	public class ThreadServices extends AbstractRemoteService
	{
		[Bindable] public var myDatas 		: ThreadDatas;
		[Bindable] public var myHandlers 	: ThreadHandlers;
		
		public function ThreadServices()
		{
			myDatas 	= new ThreadDatas();
			myHandlers 	= new ThreadHandlers(myDatas);
		}
		 public function getThreadInfos():void
		 {
			 doRemoting(myHandlers.getThreadHandler,CvAccessManager.CURRENT_FUNCTION,"AI002");
		 }
	}
}