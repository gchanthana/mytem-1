package general.service.facturation
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class FacturationHandlers
	{
		private var myDatas:FacturationDatas;
		
		public function FacturationHandlers(instanceData:FacturationDatas)
		{
			myDatas = instanceData;
		}
		
		public function getDatasFacturationResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				
				myDatas.treadDatasFacturation(evt.result as ArrayCollection);
			}
		}
		
		public function faultHandler(ev:FaultEvent):void
		{
			myDatas.processError(new ArrayCollection);
		}
		
	}
}