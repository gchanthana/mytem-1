package general.service.facturation
{
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class FacturationService
	{
		public var myDatas		:FacturationDatas;
		public var myHandlers 	:FacturationHandlers;
		
		public function FacturationService()
		{
			myDatas = new FacturationDatas();
			myHandlers = new FacturationHandlers(myDatas);
		}
		
		public function getDatasFacturation():void
		{
			var myRemote:RemoteObject = new  RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			myRemote.source="fr.consotel.consoview.M00.Facturation";
			myRemote.getDatasFacturation();
			
			myRemote.addEventListener("result", myHandlers.getDatasFacturationResultHandler);
			myRemote.addEventListener("fault", myHandlers.faultHandler);
			
		}
		
	}
}