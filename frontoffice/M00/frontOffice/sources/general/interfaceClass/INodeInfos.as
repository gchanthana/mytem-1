package general.interfaceClass {
	public interface INodeInfos {
		function get NID():int;
		
		function get LBL():String;
		
		function get STC():int;
		
		function get TYPE_PERIMETRE():String;
		
		function get TYPE_LOGIQUE():String;
		
		
		
		function get MODULE_FIXE_DATA():String;
		
		function get MODULE_GESTION_LOGIN():String;
		
		function get MODULE_MOBILE():String;
		
		function get DROIT_GESTION_FOURNIS():String;
		
		function get INFOS_STATUS():Number;
		
		function get CODE_STYLE():String;
		
		function get MODULE_FACTURATION():String;
		
		function get MODULE_GESTION_ORG():String;
		
		function get MODULE_WORKFLOW():String;
		
		function get MODULE_USAGE():String;
		
		function get GEST():Number;
		
		function get STRUCT():Number; 
		
		function get FACT():String;
		
		function get OPERATEURID():String;
		
		
		
		  
		
		function getUniversAccess(universKey:String):int;
	}
}
