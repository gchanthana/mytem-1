package general.interfaceClass
{
	import flash.events.MouseEvent;
	
	public interface IMenu
	{
		function updatePerimetre():void
		function updateMenu():void
		function displayListePerimetres(event:MouseEvent):void 
	}
}