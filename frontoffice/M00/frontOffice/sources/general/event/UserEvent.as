package general.event
{
	import flash.events.Event;
	
	public class UserEvent extends Event
	{
		public static const USER_INFOS_UPDATED_EVENT	:String = "USER_UPDATED_EVENT";
		
		public function UserEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}