package general.event
{
	import flash.events.Event;
	
	public class FacturationEvent extends Event
	{
		public static const FACTURATION_RESULT:String = "FACTURATION_RESULT";
		public static const FACTURATION_ERROR:String = "FACTURATION_ERROR";
		
		public function FacturationEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}