package general.abstract
{
	import mx.containers.Canvas;
	import mx.containers.VBox;
	import mx.events.FlexEvent;
	
	import general.interfaceClass.ICreateApplication;
	import general.interfaceClass.IMenu;

	public class AbstractCreateApplication extends Canvas implements ICreateApplication
	{
		public var vbModule:VBox;
		public var menu:IMenu;
		
		private var _moduleLoader:AbstractModuleLoader = new AbstractModuleLoader();
		
		public function AbstractCreateApplication()
		{
		}
// ---------------------- --------------- ---------------------------------		
// ---------------------- FONCTION PUBLIC ---------------------------------
// ---------------------- --------------- ---------------------------------
		public function displayModule(key:String=null):void
		{
			menu.updateMenu();
		}
		public function updatePerimetre():void
		{
		}
// ---------------------- --------------- ---------------------------------		
// ---------------------- FONCTION PRIVEE ---------------------------------
// ---------------------- --------------- ---------------------------------
		protected function init(event:FlexEvent=null):void
		{
			_moduleLoader.container = this.vbModule;
			initData();
			updatePerimetre();
		}
		protected function initDisplay():void
		{
		}
		protected function initListeners(boolActivate:Boolean = true):void
		{
			if(boolActivate)
			{

			}
			else
			{
				
			}
		}
		protected function initData():void
		{
			
		}
// ---------------------- --------------- ---------------------------------		
// ---------------------- GETTER / SETTER ---------------------------------
// ---------------------- --------------- ---------------------------------
		public function get moduleLoader():AbstractModuleLoader
		{
			return _moduleLoader;
		}
	}
}