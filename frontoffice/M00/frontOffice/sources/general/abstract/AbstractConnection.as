package general.abstract
{
	import mx.core.Container;
	
	import appli.events.ConnectionEvent;
	import appli.events.LoginEvent;
	import appli.impersonnification.service.relog.RelogServices;
	
	import composants.ConsoProgressBar.ConsoProgressBar;
	
	import entity.AppParams;
	import entity.ObjectError;
	
	import flash.events.Event;
	
	import general.service.LoginServices;
	
	import interfaceClass.IConnection;
	import interfaceClass.IModuleLogin;
	
	public class AbstractConnection extends AbstractModuleLoader implements IConnection
	{
		private var _loginService:LoginServices;
		private var _params:AppParams;
		private var relogServices:RelogServices = new RelogServices();
		
	// CONSTRUCTEUR	
		public function AbstractConnection(progress:ConsoProgressBar)
		{
			super();
			this.progress = progress;
		}
	// PUBLIC
		/**
		 *	<code>deconnect</code> 
		 * 
		 * 	gère l'action de déconnection de l'appli
		 */
		public function deconnect(param:AppParams, cont:Container=null):void
		{
			relogServices.stopImpersonnification();//qu'elle ait été active ou pas, on s'assure qu'elle est sur false lors de la déco
			if(_params)
			{
				if(this.container == null && cont != null)
					this.container = cont;
			}
			else
			{
				this._params = param;
				this.container = cont;	
			}
			
			_params.login = "";
			_params.mdp = "";
			_params.token = "";
			
			_loginService.deleteTokenFromCookie(_params.codeApp);
		}
		public function goLogin(param:AppParams, container:Container = null):void
		{	
			this.container = container;
			this._params = param;
			this.loginService = new LoginServices();
			
			initListener();
		}
		public function MustBeDisplay():Boolean
		{
			return true;
		}
		private function initListener():void
		{
			trace("AbstractConnection.initListener()");
			if(!this.loginService.myDatas.hasEventListener(LoginEvent.VALIDE))
				this.loginService.myDatas.addEventListener(LoginEvent.VALIDE,CreateSession);
		}
		public function validateLogin(objLogin:Object):void
		{
			if(_params == null)
				_params = new AppParams();
			
			_params.login = objLogin.login;
			_params.mdp = objLogin.mdp;
			if(objLogin.codeLangue != _params.codeLangue)
				_params.codeLangue = objLogin.codeLangue;
			_params.seSouvenir = objLogin.boolSeSouvenir;
		}
		public function changePerimetre(idPerimetre:int):void
		{
			loginService.changePerimetre(idPerimetre,params.codeApp,params.codeConnection);
		}
		public function changeGroupe(idPerimetre:int):void
		{
			loginService.changeGroupe(idPerimetre,params.codeApp,params.codeConnection);
		}
		public function CreateSession(evt:LoginEvent):void
		{
			try
			{
				// Si on récupère un object SESSION 
				if(evt.authInfos != null
					&& !evt.authInfos.hasOwnProperty("CODEERREUR") )
				{
					if(CvAccessManager.singletonInstance != null)
					{
						// Si la session n'est pas créée correctement, on retourne l'erreur -998 
						if(!updateSession(evt.authInfos))
						{
							avertirApplication(false,-998);
						}
							// Sinon, tout va bien
						else
						{
							avertirApplication(true);
						}
					}
					else
					{
						avertirApplication(false,-1000,error);
					}
				}
					// Sinon, on récupère un object ERROR, on avertit de l'erreur en passant l'object en paramètre afin que l'app gère l'affichage de l'erreur
				else
				{
					avertirApplication(false,evt.authInfos.CODEERREUR,evt.authInfos);
					trace ("Erreur lors de l'authentification détectée.");
				}
			}
			// En cas d'erreur, on retourne un objet ERROR à l'application
			catch(err:Error)
			{
				var error:ObjectError = new ObjectError();
				error.codeErreur = -999;
				error.message = err.message;
				avertirApplication(false,-999,error);
			}
		}
		public function avertirApplication(boolSuccess:Boolean, codeErreur:int = -999, authObj:Object= null):void
		{
			if(boolSuccess)
				dispatchEvent(new ConnectionEvent(ConnectionEvent.VALID_AUTH,null));
			else
			{
				var obj:ObjectError = new ObjectError();
				obj.defineDefaultObjectError(codeErreur);
				
				if(authObj
					&& authObj.hasOwnProperty("CODEERREUR"))
					obj.defineObjectError(authObj);
				
				dispatchEvent(new ConnectionEvent(ConnectionEvent.VALID_ERROR,obj));
			}
		}
		/**
		 * @private ConnectionCVClassique.updateSession() 
		 */
		protected function updateSession(obj:Object):Boolean
		{
			try
			{
				if(CvAccessManager.singletonInstance != null && obj != null)
				{
					if(obj.hasOwnProperty("user"))
						CvAccessManager.singletonInstance.updateUser(obj.user);
					if(obj.hasOwnProperty("liste_racine"))
						CvAccessManager.singletonInstance.updateGroupList(obj.liste_racine);
					//					_singletonInstance.setGroupIndex(obj as int);
					//					_singletonInstance.setTypeLogin(obj as int);
					if(obj.hasOwnProperty("perimetre"))
					{
						CvAccessManager.singletonInstance.updatePerimetre(obj.perimetre);
						//						CvAccessManager.singletonInstance.updatePerimetreFromNode(obj.perimetre);
					}
					if(obj.hasOwnProperty("PERIMETRE"))
					{
						CvAccessManager.singletonInstance.updatePerimetre(obj.PERIMETRE);
						//						CvAccessManager.singletonInstance.updatePerimetreFromNode(obj.perimetre);
					}
					
					if(obj.hasOwnProperty('codeapplication'))
					{
						CvAccessManager.singletonInstance.updateCodeApplication(obj.codeapplication);
					}
					if(obj.hasOwnProperty("xml_access"))
						buildMenuXml(obj.xml_access as XML);
					if(obj.hasOwnProperty("XML_ACCESS"))
						buildMenuXml(obj.XML_ACCESS as XML);
					if(obj.hasOwnProperty("xml_perimetre"))
						CvAccessManager.singletonInstance.updatePerimetreXML(obj.xml_perimetre as XML);
					if(obj.hasOwnProperty("XML_PERIMETRE"))
						CvAccessManager.singletonInstance.updatePerimetreXML(obj.XML_PERIMETRE as XML);
				}
				else
					trace("_singletonInstance == NULL");
			}
			catch(error:Error)
			{
				return false;
			}
			return true;
		}
		/**
		 * @private ConnectionCVClassique.buildMenuXml() 
		 */
		protected function buildMenuXml(menuXml:XML):Boolean
		{
			try
			{
				
				for(var k:int=menuXml.child("UNIVERS").length()-1;k>-1; k--)

				{

					if(menuXml.UNIVERS[k].@USR < 1)

						delete menuXml.UNIVERS[k]

				}

				for each(var x1:XML in menuXml.descendants("*"))

				{

					if(!x1.hasOwnProperty("@toggled"))

						x1.@toggled = x1.@TOGGLED

					if(!x1.hasOwnProperty("@enabled"))

						x1.@enabled = x1.@ENABLED;

					if(!x1.hasOwnProperty("@type"))

						x1.@type = x1.@TYPE;

					if(!x1.hasOwnProperty("@groupName"))

						x1.@groupName = x1.@GROUPNAME;

				}

				if(menuXml.UNIVERS.length() == 0)

				{

					CvAccessManager.singletonInstance.updateAccess(null);					

				}

				else

				{

					var selectedUniversNodeList:XMLList = menuXml.descendants("*").(@SYS == 1).(@toggled == 'true');

					if(selectedUniversNodeList.length() == 0) 

					{

						menuXml.UNIVERS[0].@toggled = 'true'

						if(menuXml.UNIVERS[0].@SYS == 0)

							menuXml.UNIVERS[0].FONCTION[0].@toggled = 'true'

					}

					else if(selectedUniversNodeList.length() == 1)

					{

						var path:XML;

						path =(selectedUniversNodeList[0] as XML).parent();

						trace(path.toXMLString())

						while(path)

						{

							if(path.hasOwnProperty("@toggled"))

							{

								path.@toggled = true;	

							}

							path = path.parent();

							

						}

					}

					else

					{

						return false;

					}

					CvAccessManager.singletonInstance.updateAccess(menuXml);

				}

			}

			catch(err:Error)

			{

				return false;

			}

			return true;

		}

		public function setMessageErreur(msg:String):void
		{
			if(this.MustBeDisplay())
			{
				(dispobj as IModuleLogin).displayError(msg);
			}
		}

		
		public function get loginService():LoginServices
		{
			return _loginService;
		}
		public function set loginService(value:LoginServices):void
		{
			_loginService = value;
		}
		public function get params():AppParams
		{
			return _params;
		}
	}
}