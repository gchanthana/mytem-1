﻿<cftry>
	<cfquery datasource="BI_TEST" name="qQuery">
		set variable 	p_idracine_master=310812,
						p_idracine=310812,
						p_idorga=310812,
						p_idperimetre=310812,
						p_niveau='A',
						p_idniveau=0,
						p_langue_pays='fr_FR';
	
		SELECT ORGAOPERATEUR.OPERATEUR saw_0, 
					FACTURATION.NOMBRE_FACTURES saw_1, 
					PERIODE.DESC_MOIS saw_2, 
					PERIODE.SHORT_MOIS saw_3, 
					PERIODE.DESC_ANNEE saw_4, 
					PERIODE.IDPERIODE_MOIS saw_5
		FROM E0_AGGREGS_DEFRAG
		WHERE RCOUNT(PERIODE.IDPERIODE_MOIS) <= 4
		ORDER BY saw_5 DESC
	</cfquery> 
	<cfif qQuery.recordCount gte 1>
		<cfoutput>#ucase('ok')#</cfoutput>
		<cfelse>
		<cfoutput>#ucase('test query failed')#</cfoutput>
	</cfif>
	<cfcatch type="any">
		<cfoutput>Error : <P>#cfcatch.message#</P></cfoutput>
		
	</cfcatch>
</cftry>
