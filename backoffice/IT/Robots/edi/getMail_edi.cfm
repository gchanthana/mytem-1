<!--- Vérifie si le service est actif, si il est inactif, alors l'objet arrete le traitement --->

<cfset localDir="/Infofacture">

Etape 1 - Récupération des mails<BR>

<cftry>	

	<cfset javaSystem = createObject("java", "java.lang.System") />
	<cfset jProps = javaSystem.getProperties() />
	<cfset jProps.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory") />
	<cfset jProps.setproperty("mail.pop3.port",995) />
	<cfset jProps.setProperty("mail.pop3.socketFactory.port", 995) />
	
	
	<!---20/09/2012--->
	<!---Problème des emails reçu au moment ou le robot tourne, ses emails sont supprimé sans être récupérés--->
	
	<cfpop action="GETHEADERONLY" name="qGetMail" server="pod51015.outlook.com" 
	port="995" timeout="120" username="infofacture@saaswedo.com" password="Qowo57241"> 
<!---	
	<cfpop action="GETALL" name="qGetMail" server="pod51015.outlook.com" attachmentpath="#localDir#/temp" 
	port="995" timeout="5" username="infofacture@saaswedo.com" password="Qowo57241">
	
	<cfloop query="qGetMail">

		<cfpop action="GETALL" name="qGetSingleMail" attachmentpath="#localDir#/temp" 
		generateuniquefilenames="no" server="pod51015.outlook.com" 
		port="995" username="infofacture@saaswedo.com" password="Qowo57241" uid="#qGetMail.uid#" timeout="60">

	</cfloop>
	

 	<cfdump var="#qGetMail#">
	<cfabort showerror="fin">
 --->
	
	<cfcatch type="Any">
		<cfmail port="25" server="mail-cv.consotel.fr" from="infofacture@saaswedo.com" to="djalel.meftouh@saaswedo.com" cc="monitoring@saaswedo.com" subject="Pb Mails EDI" type="html">
			<cfdump var="#cfcatch#">
			#cfcatch.message#<br>
			pb avec cf_pop.<br>
			Fin de traitement de la page !<br>
		</cfmail>
		<cfdump var="#cfcatch#">
		<cfoutput>#cfcatch.Message#</cfoutput>
		<cfabort>

	</cfcatch>
</cftry>

Etape 1 - OK<BR>

Etape 2 - Ecriture des mail.wddx + MAJ table file_source<BR>

<cfflush interval="10">
<cfinclude template="udf/QueryRowToStruct.cfm">
<cfset i=1>
<cfset allFiles = "">

<cfloop query="qGetMail">
	<cfpop action="GETALL" name="qGetSingleMail" attachmentpath="#localDir#/temp"
		generateuniquefilenames="no" server="pod51015.outlook.com"
		port="995" username="infofacture@saaswedo.com" password="Qowo57241" uid="#qGetMail.uid#" timeout="60">

	<cfset stMail=queryRowToStruct(qGetSingleMail,i)>
	<cfwddx action="CFML2WDDX" input="#stMail#" output="xmlMail">
	<cfset l_attachment="">
	<cfset flag_file=0>
	<cfloop list="#qGetSingleMail.attachments#" delimiters="#chr(9)#" index="j">
		<cfif right(j,2) eq "gz" or right(j,3) eq "zip">
			<cfset l_attachment=ListAppend(l_attachment,j,",")>
			<cfset allFiles = ListAppend(allFiles,j,",")>
			<cfset filename=GetToken(j,1,".") & ".wddx">
			<cffile action="WRITE" file="#localDir#/temp/mail/#filename#" output="#xmlMail#" addnewline="No">
			<cfoutput>
			j:#j#<br>
			</cfoutput>
		   
			<cfquery datasource="ROCOFFRE" name="qGetFile">
				select *
				from file_source f
				where f.operateurid=63
				and f.extranet_filename='#j#'
			</cfquery>
			<cfif qGetFile.recordcount eq 0>
				<cfquery datasource="ROCOFFRE" name="qListe">
					insert into
					file_source(extranet_filename,operateurid,date_recuperation,extranet_path,etat,date_mise_a_dispo,date_etat,source_filename,archive_path)
					VALUES('#j#',63,sysdate,'\\pelican\import\factures\Infofacture\temp',1,
							sysdate,sysdate,'#j#','\\pelican\id\archives\recup\obs\facture')
				</cfquery>
			</cfif>
			<cfset flag_file=1>
		</cfif>
	</cfloop>

	<cfif not flag_file>
		<cfmail port="25" to="djalel.meftouh@saaswedo.com" from="infofacture@saaswedo.com" cc="monitoring@saaswedo.com" subject="#qGetSingleMail.subject#" server="mail-cv.consotel.fr" type="HTML">
			#qGetSingleMail.body#
		</cfmail>
	</cfif>
	<cfset i=i+1>
</cfloop>

Etape 2 - OK<BR>	
Etape 3 - Envoi du mail avec liste des fichiers<BR>
 
<cfflush interval="10">

<cfif qGetMail.recordcount gt 0>
	<cfmail port="25" from="infofacture@saaswedo.com" to="monitoring@saaswedo.com" cc="djalel.meftouh@saaswedo.com,production@consotel.com"
			subject="Fichiers" server="mail-cv.consotel.fr" type="HTML" username="infofacture@saaswedo.com" password="Qowo57241">
	Les fichiers suivants sont arrivés et attendent d'etre importes:<br>
		<cfloop list="#allFiles#" delimiters="#chr(9)#" index="j">
			<p>#j#</p><br>
		</cfloop>
	<hr>
	Nombre de mails reçus : #qGetMail.recordcount#<br>
	</cfmail>
</cfif>	
Etape 3 - OK<BR>
Etape 4 - Pause 1 mn<BR>
<cfflush interval="10">
<!--- Fait une pause --->
<cfset thread = CreateObject("java", "java.lang.Thread")>
<cfset thread.sleep(JavaCast('long', 60000)) />
Etape 4 - OK<BR>
Etape 5 - Supression emails<BR>
<cfflush interval="10">
<!--- Supprime les emails --->

<!--- <cfset javaSystem = createObject("java", "java.lang.System") />
	<cfset jProps = javaSystem.getProperties() />
	<cfset jProps.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory") />
	<cfset jProps.setproperty("mail.pop3.port",995) />
	<cfset jProps.setProperty("mail.pop3.socketFactory.port", 995) /> --->

<!---20/09/2012--->
<!---Problème des emails reçu au moment ou le robot tourne, ses emails sont supprimé sans être récupérés--->
<!--- <cfloop index="valUID" array="#ArrayUID#">
	
		<cfpop action="delete" server="mail-ext.consotel.fr" 
		port="110" username="infofacture@consotel.fr" password="Azerty123" uid="#valUID#"
		messagenumber="1" timeout="5">
	

</cfloop> --->

<cfloop query="qGetMail">
	<cftry>	
	<cfpop action="delete" server="pod51015.outlook.com" 
		port="995" username="infofacture@saaswedo.com" password="Qowo57241" uid="#qGetMail.uid#"
		messagenumber="1" timeout="15">
	<cfcatch type="any">
Timeout sur une suppression:#qGetMail.uid#<br>
	</cfcatch>
	</cftry>
</cfloop>
<!--- <cfloop index="valUID" array="ArrayUID">
	<cfpop action="GETHEADERONLY" name="qGetHeader" server="mail-ext.consotel.fr" 
	port="110" timeout="5" username="infofacture@consotel.fr" password="Azerty123" uid="#valUID#">
	
	<cfloop query="qGetHeader">
		<cfpop action="delete" server="mail-ext.consotel.fr" 
		port="110" username="infofacture@consotel.fr" password="Azerty123"
		messagenumber="1" timeout="5">
	</cfloop>
<cfabort showerror="fin">

</cfloop> --->
Etape 5 - OK<BR>
<cfflush interval="10">
