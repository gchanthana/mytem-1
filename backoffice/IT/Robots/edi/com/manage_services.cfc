<cfcomponent name="manage_services">
	
	<cffunction name="switchFromactif" returntype="string" access="remote">
		<cfargument name="indexvar" type="array">
		<CFSET MyXmlFile = ExpandPath("../services.xml")>
		<!--- 2. Read XML file into string variable called MyXmlCode --->
		<CFFILE
		ACTION="READ"
		FILE="#MyXmlFile#"
		VARIABLE="MyXmlCode">
		<CFSET MyXml = XmlParse(MyXmlCode)>
		<cfset xnRoot=MyXml.XmlRoot>
		<cfset xnThisService=xnRoot.Xmlchildren[indexvar[1]]>
		<cfset xnThisService.etat.XmlText="inactif">
		<!--- Ecriture des infos dans le fichier --->
		<CFFILE
		ACTION="Write"
		FILE="#ExpandPath('../services.xml')#"
		Output="#replace(ToString(MyXml),"UTF-8","ISO-8851-1")#">
		<cfreturn "/edi/images/gear_stop.jpg">
	</cffunction>
	
	<cffunction name="switchFrominactif" returntype="string" access="remote">
		<cfargument name="indexvar" type="array">
		<CFSET MyXmlFile = ExpandPath("../services.xml")>
		<!--- 2. Read XML file into string variable called MyXmlCode --->
		<CFFILE
		ACTION="READ"
		FILE="#MyXmlFile#"
		VARIABLE="MyXmlCode">
		<CFSET MyXml = XmlParse(MyXmlCode)>
		<cfset xnRoot=MyXml.XmlRoot>
		<cfset xnThisService=xnRoot.Xmlchildren[indexvar[1]]>
		<cfset xnThisService.etat.XmlText="actif">
		<!--- Ecriture des infos dans le fichier --->
		<CFFILE
		ACTION="Write"
		FILE="#ExpandPath('../services.xml')#"
		Output="#replace(ToString(MyXml),"UTF-8","ISO-8851-1")#">
		<cfreturn "/edi/images/gear_ok.jpg">
	</cffunction>
	
	<cffunction name="checkState" returntype="void" access="remote">
		<cfargument name="i" type="numeric">
		<!--- V�rifie si le service est actif --->
		
		<!--- which is in the same folder as this CF template --->
		<CFSET MyXmlFile = ExpandPath("services.xml")>
		<!--- 2. Read XML file into string variable called MyXmlCode --->
		
		
		<CFFILE
		ACTION="READ"
		FILE="#MyXmlFile#"
		VARIABLE="MyXmlCode">
	
		<!--- 3. Parse the XML into an XML "Object" --->
		<!--- (very similar to a CFML Structure) --->
		<CFSET MyXml = XmlParse(MyXmlCode)>
		<cfset xnRoot=MyXml.XmlRoot>
		<cfset xnThisService=xnRoot.Xmlchildren[i]>
		<cfif xnThisService.etat.XmlText eq "inactif">
			<cfabort >
		</cfif>
	
	</cffunction>
</cfcomponent>