<cfcomponent displayname="GetmailCDR" output="false">
	<cfset variables.repertoire="/app/fichiers/1-recup/test">
	
	<cffunction name="checkTypeMail" displayname="checkTypeMail" access="package" output="true" returntype="string">
		<cfargument name="replyto" type="string" required="true">
		<cfargument name="subject" type="string" required="true">
		<!--- Neuf Telecom : Facturation et CDR --->
		<cfif Find("neuf.com",replyto,1)+Find("neuftelecom.com",replyto,1)+Find("neufcegetel.fr",replyto,1)+Find("9 CEGETEL",subject,1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/NeufCegetel/ZIP">
		<cfelseif Find("Rapport de facturation des consommations",subject,1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/NeufCegetel/ZIP">
		<cfelseif Find("Fichier details facture NEUF CEGETEL",subject,1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/NeufCegetel/ZIP">
		<!--- Transpac --->
		<cfelseif Find("Fichier zippe",subject,1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/Transpac/zip">
		<!--- SBPM --->
		<cfelseif Find("sbpm.fr",replyto,1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/SFR/ZIP">
		<!--- Parc Vision --->
		<cfelseif Find("parc vision",lcase(subject),1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/SFR/PARC VISION">
		<!--- LTI --->
		<cfelseif Find("lti-tele.com",replyto,1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/LTI Telecom/ZIP">
		<!--- Orange --->
		<cfelseif Find("orange",lcase(subject),1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/ORANGE/ZIP">
		<!--- Orange --->
		<cfelseif Find("transpac",lcase(subject),1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/Transpac/ZIP">
		<!--- CDR OBS : Analyse trafic --->
		<cfelseif Find("analyse trafic",lcase(subject),1)+Find("_TDC",subject,1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/Orange Business S/ZIP">
		<!--- CDR Colt --->
		<cfelseif Find("colt",lcase(subject),1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/COLT/ZIP">
		<!--- CDR Verizon --->
		<cfelseif Find("verizon",lcase(subject),1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/Verizon/ZIP">
		<!--- CDR Divers --->
		<cfelseif Find("import",lcase(subject),1)+Find("cdr",lcase(subject),1) gt 0>
			<cfset cheminAttachement="#variables.repertoire#/ZIP">
		<cfelse>
			<cfset cheminAttachement="#variables.repertoire#/FichiersInconnus">
		</cfif>
		<cfreturn cheminAttachement/>
	</cffunction>
</cfcomponent>