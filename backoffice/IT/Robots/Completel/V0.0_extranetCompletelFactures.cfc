<cfcomponent output="false">
	<cfset variables.FichiersTotal=0>
	<cfset variables.FichiersImportes=0>
	<cfset variables.FichiersPresents=0>
	<cfset variables.FileArray=ArrayNew(1)>
	<cfset variables.statusFile=ArrayNew(1)>
		
	<cffunction name="getCompletel" access="remote" returntype="String" output="false">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<!--- Initialise les variables globales --->
		<cfset variables.FichiersTotal=0>
		<cfset variables.FichiersImportes=0>
		<cfset variables.FichiersPresents=0>
		<cfset variables.FileArray=ArrayNew(1)>
		<cfset variables.statusFile=ArrayNew(1)>
		<cfset toggle=1>
		<cfset tempChaine = "<html><body><table border='0'>">
		<!--- On se log au compte --->
		<cfset jCookie=LoginCompletel(login,pwd)>
		------------------------------------------------ <br>
		Login : #jCookie# <br>
		<!--- On s'occupe du compte Pere d'abord --->
		<!--- R�cup�re la liste des compte fils --->
		<cfset aCompte=FindCompteFils(jCookie)>
		<cfloop list="#ArrayToList(aCompte)#" index="i">#login# : Compte fils : #i#<br></cfloop>
		<!--- Changement de page, on va au detail des appels --->
		<cfset pageDetail=AllerDetailAppel(jCookie)>
		#login# : Aller Detail Appels : <cfif Len(pageDetail) gt 0>200 OK</cfif><br>
		<!--- Parsing de la page de detail des appels --->
		<cfset variables.FileArray=ParseDetailAppels(pageDetail)>
		#login# : Nbre Fichiers : #ArrayLen(variables.FileArray)#<br>
		<!--- Si il y a des fichiers on continue --->
		<cfif ArrayLen(variables.FileArray) gt 0>
			<cfset getFileStatus=getFiles(jCookie,variables.FileArray)>
			#login# : R�cup�ration Fichiers : #getFileStatus#<br>
			#login# : Fichiers total : #variables.FichiersTotal#<br>
			#login# : Fichiers pr�sents : #variables.FichiersPresents#<br>
			#login# : Fichiers import�s : #variables.FichiersImportes#<br>
			<cfloop from="1" to="#ArrayLen(variables.FileArray)#" index="j">
				<cfif variables.statusFile[j] eq 1>
					<cfif toggle eq 1>
						<cfset toggle=0>
						<cfset tempChaine = tempChaine & "<tr><td>#login#</td><td>Compte P�re</td><td>telechargement fichiers.</td></tr>">
					</cfif>
					#login# : telechargement fichier : #variables.statusFile[j]# : #variables.FileArray[j]#<br>
					<cfset tempChaine = tempChaine & "<tr><td>#login#</td><td></td><td>#variables.FileArray[j]#</td></tr>">
				</cfif>
			</cfloop>
		<!--- Sinon on ne fait rien, on continue avec les comptes fils --->
		<cfelse>
			<cfset tempChaine = tempChaine & "<tr><td>#login#</td><td>Compte P�re</td><td>Pas de fichiers.</td></tr>">
			#login# : Compte P�re : Pas de fichiers.<br>
		</cfif>
		<cfif variables.FichiersPresents eq variables.FichiersTotal and variables.FichiersTotal gt 0>
			<cfset tempChaine = tempChaine & "<tr><td>#login#</td><td>Compte P�re</td><td>Tous les fichiers sont pr�sents.</td></tr>">
		</cfif>
		
		
		<!--- Boucle sur les compte fils trouv�s --->
		<cfloop from="1" to="#ArrayLen(aCompte)#" index="i">
			<!--- Initialise les variables globales --->
			<cfset variables.FichiersTotal=0>
			<cfset variables.FichiersImportes=0>
			<cfset variables.FichiersPresents=0>
			<cfset variables.FileArray=ArrayNew(1)>
			<cfset variables.statusFile=ArrayNew(1)>
			<cfset toggle=1>
			<cfset tempChaine = tempChaine & "<tr><td colspan='3'><hr></td></tr>">
			<!--- Retourne au pere a chaque fois sauf la premiere --->
			<cfif i gt 1>
				<cfset changerComptePereStatus=changerComptePere(jCookie)>
				#aCompte[i]# : Retour Compte P�re : #changerComptePereStatus#<br>
			</cfif>
			<!--- Changement de compte de facturation --->
			<cfset changerCompteFilsStatus=changerCompteFils(jCookie,aCompte[i])>
			#aCompte[i]# : Changement Compte : #changerCompteFilsStatus#<br>
			<!--- Changement de page --->
			<cfset pageDetail=AllerDetailAppel(jCookie)>
			#aCompte[i]# : Aller Detail Appels : <cfif Len(pageDetail) gt 0>200 OK</cfif><br>
			<!--- Parsing de la page de detail des appels --->
			<cfset variables.FileArray=ParseDetailAppels(pageDetail)>
			#aCompte[i]# : Nbre Fichiers : #ArrayLen(variables.FileArray)#<br>
			<!--- Si il y a des fichiers on continue --->
			<cfif ArrayLen(variables.FileArray) gt 0>
				<cfset getFileStatus=getFiles(jCookie,variables.FileArray)>
				#aCompte[i]# : R�cup�ration Fichiers : #getFileStatus#<br>
				#aCompte[i]# : Fichiers total : #variables.FichiersTotal#<br>
				#aCompte[i]# : Fichiers pr�sents : #variables.FichiersPresents#<br>
				#aCompte[i]# : Fichiers import�s : #variables.FichiersImportes#<br>
				<cfloop from="1" to="#ArrayLen(variables.FileArray)#" index="j">
					<cfif variables.statusFile[j] eq 1>
						<cfif toggle eq 1>
								<cfset toggle=0>
							<cfset tempChaine = tempChaine & "<tr><td>#aCompte[i]#</td><td>Compte Fils</td><td>telechargement fichiers.</td></tr>">
						</cfif>
						#aCompte[i]# : fichiers : #variables.statusFile[j]# : #variables.FileArray[j]#<br>
						<cfset tempChaine = tempChaine & "<tr><td>#aCompte[i]#</td><td></td><td>#variables.FileArray[j]#</td></tr>">
					</cfif>
				</cfloop>
			<cfelse>
				<cfset tempChaine = tempChaine & "<tr><td>#aCompte[i]#</td><td>Compte Fils</td><td>Pas de fichiers.</td></tr>">
				#aCompte[i]# : Compte Fils : Pas de fichiers.<br>
			</cfif>
			<cfif variables.FichiersPresents eq variables.FichiersTotal and variables.FichiersTotal gt 0>
				<cfset tempChaine = tempChaine & "<tr><td>#aCompte[i]#</td><td>Compte Fils</td><td>Tous les fichiers sont pr�sents.</td></tr>">
			</cfif>
		</cfloop>
		<!--- On se deconnecte --->
		<cfset logStatus=LogoffCompletel(jCookie)>
		Deconnexion : #jCookie# : #logStatus#<br>
		<cfset tempchaine = tempchaine & "</table></body></html>">
		<cfmail from="consoProd@consotel.fr" to="production@consotel.fr" bcc="brice.miramont@consotel.fr" server="192.168.3.119" port="26"  type="HTML" subject="[Extranet Completel]- #login#">
			<style>
			html body {
				margin:0;
				padding:0;
				background: ##D4D0C8;
				font:x-small Verdana,Sans-serif;
			}
			p {
				font:bold x-small EurostileBold,verdana,arial,helvetica,serif;
			}
			td {
				font-size : x-small;
				/*color: ##629FB5;*/
			}
			</style>
					
			<p>Login : #login#<br>
			Compte Fils : #ArrayLen(aCompte)#
			</p>
			#tempchaine#
		</cfmail>
		<cfreturn "Ok">
	</cffunction>
	
	<!--- M�thode pour se logger, on ram�ne le cookie --->
	<cffunction name="LoginCompletel" access="remote" returntype="String" output="true">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		
		<!--- Va chercher le cookie --->
		<cfhttp method="get" resolveurl="false" url="https://espaceclient.completel.fr" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)"></cfhttp>
		<!--- Parse pour identifier le cookie --->
		<cfset chaine=cfhttp.ResponseHeader["Set-Cookie"]>
		<cfset jCookie=GetToken(GetToken(chaine,1,";"),2,"=")>
		<!--- On se log pour de bon --->
		<cfhttp method="get" resolveurl="true" url="https://espaceclient.completel.fr/loginservlet" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="url" name="login" value="#login#">
			<cfhttpparam type="url" name="pwd" value="#pwd#">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie)#">
		</cfhttp>
		<cfif LEN(cfhttp.fileContent) gt 5000>
			<cfmail from="consoProd@consotel.fr" to="production@consotel.fr" bcc="brice.miramont@consotel.fr" server="192.168.3.119" port="26"  type="HTML" subject="[Extranet Completel]- #login#">
				<style>
				html body {
					margin:0;
					padding:0;
					background: ##D4D0C8;
					font:x-small Verdana,Sans-serif;
				}
				p {
					font:bold x-small EurostileBold,verdana,arial,helvetica,serif;
				}
				td {
					font-size : x-small;
					/*color: ##629FB5;*/
				}
				</style>
						
				<p>Login : #login#</p>
				Probl�me de login:<br>
				login: #login#<br>
				password: #pwd#<br>
			</cfmail>
		</cfif>
		<cfreturn jCookie>
	</cffunction>
	
	<!--- M�thode pour se logger, on ram�ne le cookie --->
	<cffunction name="LogoffCompletel" access="remote" returntype="String" output="true">
		<cfargument name="jsession" type="String">
		<cfhttp method="get" resolveurl="true" url="https://espaceclient.completel.fr/deconnectservlet" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
		<cfreturn cfhttp.StatusCode>
	</cffunction>
	
	<!--- M�thode pour identifier les compte fils --->
	<cffunction name="FindCompteFils" access="remote" returntype="array" output="true">
		<cfargument name="jsession" type="String">
		<!--- R�cup�re la page qui contient la liste des comptes fils --->
		<cfhttp method="get" resolveurl="true" url="https://espaceclient.completel.fr:443/mainframe.jsp" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
		<!--- Trouve les bords de la chaine a parser --->
		<cfset deb=findNoCase("Choisissez",cfhttp.FileContent,1)>
		<cfset fin=findNoCase("select>",cfhttp.FileContent,deb)>
		<cfset temp=mid(cfhttp.FileContent,deb,fin)>
		<cfset chaine=ListToArray(temp,"<")>
		<cfset aCompte=ArrayNew(1)>
		<cfset compteur=1>
		<!--- Parse la chaine pour mettre les compte dans une array --->
		<cfloop from="1" to="#ArrayLen(chaine)#" index="l">
			<cfset v=findNoCase("value=",chaine[l])>
			<cfif v gt 0>
				<cfset fin=find('">',chaine[l],1)>
				<cfset vTemp=mid(chaine[l],1+14,fin-15)>
				<cfset aCompte[compteur]=vTemp>
				<cfset compteur=compteur+1>
			</cfif>
		</cfloop>
		<cfreturn aCompte>
	</cffunction>
	
	<!--- M�thode pour changer de compte fils --->
	<cffunction name="changerCompteFils" access="remote" returntype="String" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="comptefils" type="String">
		<cfhttp method="get" redirect="no" resolveurl="true" url="https://espaceclient.completel.fr:443/processcomptefilsservlet" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
			<cfhttpparam type="url" name="choixcompte" value="comptefils">
			<cfhttpparam type="url" name="listFils" value="#comptefils#">
			<cfhttpparam type="url" name="page" value="">
		</cfhttp>
		<cfreturn cfhttp.StatusCode>
	</cffunction>
	
	<!--- M�thode pour retourner au compte pere --->
	<cffunction name="changerComptePere" access="remote" returntype="String" output="true">
		<cfargument name="jsession" type="String">
		<cfhttp method="get" redirect="no" resolveurl="true" url="https://espaceclient.completel.fr:443/processcomptefilsservlet" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
			<cfhttpparam type="url" name="choixcompte" value="compteorigine">
			<cfhttpparam type="url" name="page" value="">
		</cfhttp>
		<cfreturn cfhttp.StatusCode>
	</cffunction>
	
	<!--- M�thode pour aller a la page des factures --->
	<cffunction name="AllerDetailAppel" access="remote" returntype="String" output="true">
		<cfargument name="jsession" type="String">
		<cfhttp method="get" resolveurl="false" url="https://espaceclient.completel.fr/redirectservlet" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
			<cfhttpparam type="url" name="page" value="DETAILSPARNDI">
		</cfhttp>
		<cfreturn cfhttp.FileContent>
	</cffunction>
	
	<!--- M�thode pour parse la page de detail des appels pour avoir les noms de fichier --->
	<cffunction name="ParseDetailAppels" access="remote" returntype="Array" output="true">
		<cfargument name="pageDetail" type="String">
		<cfset tempFileArray=ArrayNew(1)>
		<!--- Restreint la zone de travail --->
		<cfset deb=find("file=",pageDetail,1)>
		<cfset fin=find("</a><br></div></td></tr>",pageDetail,deb)>
		<cfif fin gt 0>
			<cfset chaine=mid(pageDetail,deb,fin)>
			<cfset temp=ListToArray(chaine,"=")>
			<cfset compteur=1>
			<cfloop from="1" to="#ArrayLen(temp)#" index="k">
				<cfif find(".txt",temp[k],1) neq 0>
					<cfset fin=find(".txt",temp[k],1)>
					<cfset chaine=mid(temp[k],1,fin+3)>
					<cfset tempFileArray[compteur]=chaine>
					<cfset compteur=compteur+1>
					<cfset variables.FichiersTotal=variables.FichiersTotal+1>
				</cfif>
			</cfloop>
		</cfif>
		<cfreturn tempFileArray>
	</cffunction>
	
	<!--- M�thode pour r�cup�rer les fichiers --->
	<cffunction name="getFiles" access="remote" returntype="string" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="tempFileArray" type="array">
		<cfloop from="1" to="#ArrayLen(tempFileArray)#" index="n">
			<!--- A remplacer par une requete dans la base lors du vrai import --->
			<cfdirectory action="list" filter="#tempFileArray[n]#" name="qListe" sort="name asc" 
							directory="\\dbcl2\web_directory\import\factures\Completel\sources\ok\">
			<!--- Fin du remplacer --->
			<cfif qListe.recordcount eq 0>
				<cfhttp method="get" resolveurl="no" url="https://espaceclient.completel.fr/download.jsp" 
						useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)"
						getasbinary="no">
						<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
						<cfhttpparam type="url" name="file" value="#tempFileArray[n]#">
				</cfhttp>
				<cffile action="write" file="\\dbcl2\web_directory\import\factures\Completel\sources\#tempFileArray[n]#" output="#cfhttp.FileContent#">
				<cfset variables.statusFile[n]=1>
				<cfset variables.FichiersImportes=variables.FichiersImportes+1>
			<cfelse>
				<cfset variables.statusFile[n]=0>
				<cfset variables.FichiersPresents=variables.FichiersPresents+1>
			</cfif>
		</cfloop>
		<cfreturn "OK">
	</cffunction>
</cfcomponent>