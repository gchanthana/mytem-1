<cfcomponent output="true" displayname="extranetCompletelCDRv2"> 
	<cfset variables.FichiersTotal=0>
	<cfset variables.FichiersImportes=0>
	<cfset variables.FichiersPresents=0>
	<cfset variables.FileArray=ArrayNew(1)>
	<cfset variables.statusFile=ArrayNew(1)>
	<cfset variables.repertoire="/app/fichiers/1-recup/completel/cdr">
	
	<cffunction name="getCompletel" access="remote" returntype="String" output="false">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="string">
		<!--- Initialise les variables globales --->
		<cfset variables.FichiersTotal=0>
		<cfset variables.FichiersImportes=0>
		<cfset variables.FichiersPresents=0>
		<cfset variables.FileArray=ArrayNew(1)>
		<cfset variables.statusFile=ArrayNew(1)>
		<cfset toggle=1>
		<cfset tempChaine = "<html><body><table border='0'>">
		
		<!--- On se log au compte --->
		<cfset jCookie=LoginCompletel(login,pwd)>
		------------------------------------------------ <br>
		Login : #jCookie# <br>
		

		<!--- On s'occupe du compte Pere d'abord --->
		<!--- R?cup?re la liste des compte fils --->
		<cfset aListeCompte=ArrayNew(1)>
		<cfset aListeCompteRacine=FindCompteFils(jCookie)>
		<cfset aListeCompte.addAll(aListeCompteRacine)>	
		
		
		<!--- Changement de page, on va au detail des appels --->
		<cfset pageDetail=AllerDetailAppel(jCookie)>
		
		<!--- Parsing de la page de detail des appels --->
		<cfset variables.FileArray=ParseDetailAppels(pageDetail)>

		

		<!--- Si il y a des fichiers on continue --->
		<cfif ArrayLen(variables.FileArray) gt 0>
			<cfset getFileStatus=getFiles(jCookie,variables.FileArray,compte)>
		
		</cfif>
				

		<!--- Boucle sur les compte fils trouv?s --->
		<cfloop from="1" to="#ArrayLen(aListeCompte)#" index="i">
				<!--- Initialise les variables globales --->
				<cfset variables.FichiersTotal=0>
				<cfset variables.FichiersImportes=0>
				<cfset variables.FichiersPresents=0>
				<cfset variables.FileArray=ArrayNew(1)>
				<cfset variables.statusFile=ArrayNew(1)>
				<cfset toggle=1>
				<cfset tempChaine = tempChaine & "<tr><td colspan='3'><hr></td></tr>">
				
				<!--- Retourne au pere a chaque fois sauf la premiere --->
				<cfif i gt 1>
					<cfset changerComptePereStatus=changerComptePere(jCookie)>
				</cfif>
				
				<!--- Changement de compte de facturation --->
				<cfset changerCompteFilsStatus=changerCompteFils(jCookie,aListeCompte[i])>
				
				<cfoutput >#aListeCompte[i]#</cfoutput>

				
				<!--- Aller chercher les comptes de niveau 3 --->
				<cfset aTempCompte=FindCompteFils(jCookie)>
				
				<!--- Changement de page --->
				<cfset pageDetail=AllerDetailAppel(jCookie)>
				
				<!--- Parsing de la page de detail des appels --->
				<cfset variables.FileArray=ParseDetailAppels(pageDetail)>

				
				<!--- Si il y a des fichiers on continue --->
				<cfif ArrayLen(variables.FileArray) gt 0>
					<cfdump var="#variables.FileArray#">

					<cfset getFileStatus=getFiles(jCookie,variables.FileArray,compte)>

					
				</cfif>
				
				
				
				
				
				
				<cfif ArrayLen(aTempCompte) gt 0>
				
					
						<cfif i gt 1>
							<cfset changerComptePereStatus=changerComptePere(jCookie)>
						</cfif>
							
						<!--- Changement de compte de facturation --->
						<cfset changerCompteFilsStatus=changerCompteFils(jCookie,aListeCompte[i])>
						#aListeCompte[i]# : Changement Compte : #changerCompteFilsStatus#<br>
					
					

						<cfloop from="1" to="#ArrayLen(aTempCompte)#" index="k">
						
								<cfif k gt 1>
									<!--- Revenir � la racine --->
									<cfset changerComptePereStatus=changerComptePere(jCookie)>
									
									<!--- Se connecter au compte du 1er niveau --->
									<cfset changerCompteFilsStatus=changerCompteFils(jCookie,aListeCompte[i])>
								</cfif>
									
								<!--- Changement de compte de facturation --->
								<cfset changerCompteFilsStatus=changerCompteFils(jCookie,aTempCompte[k])>

								<!--- <cfoutput >#cfhttp.FileContent#</cfoutput>
								<cfabort showerror="fin0"> --->
								
								<!--- Changement de page --->
								<cfset pageDetail=AllerDetailAppel(jCookie)>
								
								<!--- Parsing de la page de detail des appels --->
								<cfset variables.FileArray=ParseDetailAppels(pageDetail)>
				
								
								<!--- Si il y a des fichiers on continue --->
								<cfif ArrayLen(variables.FileArray) gt 0>
									<cfdump var="#variables.FileArray#">
				
									<cfset getFileStatus=getFiles(jCookie,variables.FileArray,compte)>								
								</cfif>
											
							
						</cfloop>
				
				</cfif>
				
				
				
				
		</cfloop>
		<!--- On se deconnecte --->
		<cfset logStatus=LogoffCompletel(jCookie)>
		Deconnexion : #jCookie# : #logStatus#<br>
		<cfset tempchaine = tempchaine & "</table></body></html>">

		<cfreturn "Ok">
	</cffunction>
	
	<!--- M�thode pour se logger, on ram�ne le cookie --->
	<cffunction name="LoginCompletel" access="remote" returntype="String" output="true">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		
		<!--- Va chercher le cookie --->
		<cfhttp method="get" resolveurl="false" url="https://espaceclient.completel.fr" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)"></cfhttp>
		<!--- Parse pour identifier le cookie --->
		<cfset chaine=cfhttp.ResponseHeader["Set-Cookie"]>
		<cfset jCookie=GetToken(GetToken(chaine,1,";"),2,"=")>
		<!--- On se log pour de bon --->
		<cfhttp method="get" resolveurl="true" url="https://espaceclient.completel.fr/loginservlet" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="url" name="login" value="#login#">
			<cfhttpparam type="url" name="pwd" value="#pwd#">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie)#">
		</cfhttp>
		<cfif LEN(cfhttp.fileContent) gt 5000>
			<cfmail from="consoProd@consotel.fr" to="production@consotel.fr" bcc="brice.miramont@consotel.fr,djalel.meftouh@consotel.fr" server="192.168.3.119" port="26"  type="HTML" subject="[Extranet Completel]- #login#">
				<style>
				html body {
					margin:0;
					padding:0;
					background: ##D4D0C8;
					font:x-small Verdana,Sans-serif;
				}
				p {
					font:bold x-small EurostileBold,verdana,arial,helvetica,serif;
				}
				td {
					font-size : x-small;
					/*color: ##629FB5;*/
				}
				</style>
						
				<p>Login : #login#</p>
				Probl�me de login:<br>
				login: #login#<br>
				password: #pwd#<br>
			</cfmail>
		</cfif>
		<cfreturn jCookie>
	</cffunction>
	
	<!--- M�thode pour se logger, on ram�ne le cookie --->
	<cffunction name="LogoffCompletel" access="remote" returntype="String" output="true">
		<cfargument name="jsession" type="String">
		<cfhttp method="get" resolveurl="true" url="https://espaceclient.completel.fr/deconnectservlet" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
		<cfreturn cfhttp.StatusCode>
	</cffunction>
	
	<!--- M�thode pour identifier les compte fils --->
	<cffunction name="FindCompteFils" access="remote" returntype="array" output="true">
		<cfargument name="jsession" type="String">
		<!--- R�cup�re la page qui contient la liste des comptes fils --->
		<cfhttp method="get" resolveurl="true" url="https://espaceclient.completel.fr:443/main_frame.jsp" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>

		
		<cfset chaine = REMatch('<option value="[0-9]{0,50}">',cfhttp.FileContent) />

		<cfset aCompte=ArrayNew(1)>
		<cfset temp=ArrayNew(1)>
		<cfloop from="1" to="#ArrayLen(chaine)#" index="l">
				<cfset temp=REMatch('[0-9]{1,50}',chaine[l])>
				<cfset aCompte[l]=temp[1]>

		</cfloop>
		
		<cfreturn aCompte>
	</cffunction>
	
	<!--- M�thode pour changer de compte fils --->
	<cffunction name="changerCompteFils" access="remote" returntype="String" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="comptefils" type="String">
		<cfhttp method="get" redirect="no" resolveurl="true" url="https://espaceclient.completel.fr:443/processcomptefilsservlet" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
			<cfhttpparam type="url" name="choixcompte" value="comptefils">
			<cfhttpparam type="url" name="listFils" value="#comptefils#">
			<cfhttpparam type="url" name="page" value="">
		</cfhttp>
		
		<cfhttp method="get" resolveurl="true" url="https://espaceclient.completel.fr/main_frame.jsp" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
			
		<cfreturn cfhttp.StatusCode>
	</cffunction>
	
	<!--- M�thode pour retourner au compte pere --->
	<cffunction name="changerComptePere" access="remote" returntype="String" output="true">
		<cfargument name="jsession" type="String">
		<cfhttp method="get" redirect="no" resolveurl="true" url="https://espaceclient.completel.fr:443/processcomptefilsservlet" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
			<cfhttpparam type="url" name="choixcompte" value="compteorigine">
			<cfhttpparam type="url" name="page" value="">
		</cfhttp>
		<cfreturn cfhttp.StatusCode>
	</cffunction>
	
	<!--- M�thode pour aller a la page des factures --->
	<cffunction name="AllerDetailAppel" access="remote" returntype="String" output="true">
		<cfargument name="jsession" type="String">
		<cfhttp method="get" resolveurl="false" url="https://espaceclient.completel.fr/redirectservlet" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
			<cfhttpparam type="url" name="page" value="DETAILSAPPELS">
		</cfhttp>
		<cfreturn cfhttp.FileContent>
	</cffunction>
	
	<!--- M�thode pour parse la page de detail des appels pour avoir les noms de fichier --->
	<cffunction name="ParseDetailAppels" access="remote" returntype="Array" output="true">
		<cfargument name="pageDetail" type="String">
		<cfset tempFileArray=ArrayNew(1)>
		<!--- Restreint la zone de travail --->
		<cfset deb=find("file=",pageDetail,1)>
		<cfset fin=find("</a><br></div></td></tr>",pageDetail,deb)>
		<cfif fin gt 0>
			<cfset chaine=mid(pageDetail,deb,fin)>
			<cfset temp=ListToArray(chaine,"=")>
			<cfset compteur=1>
			<cfloop from="1" to="#ArrayLen(temp)#" index="k">
				<cfif find(".csv",temp[k],1) neq 0>
					<cfset fin=find(".csv",temp[k],1)>
					<cfset chaine=mid(temp[k],1,fin+3)>
					<cfset tempFileArray[compteur]=chaine>
					<cfset compteur=compteur+1>
					<cfset variables.FichiersTotal=variables.FichiersTotal+1>
				</cfif>
			</cfloop>
		</cfif>
		<cfreturn tempFileArray>
	</cffunction>
	
	<!--- M�thode pour r�cup�rer les fichiers --->
	<cffunction name="getFiles" access="remote" returntype="string" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="tempFileArray" type="array">
		<cfargument name="compte" type="string">
		<cfloop from="1" to="#ArrayLen(tempFileArray)#" index="n">

			<cfquery datasource="ROCOFFRE" name="qListe">
				SELECT *
				FROM file_source t
				WHERE lower(t.extranet_filename)=lower('#tempFileArray[n]#')
				AND t.archive_path IS NOT NULL
				AND t.operateurid=32
			</cfquery>
			<cfif qListe.recordcount eq 0>
				<cfhttp method="get" resolveurl="no" url="https://espaceclient.completel.fr/download.jsp" 
						useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)"
						getasbinary="no">
						<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
						<cfhttpparam type="url" name="file" value="#tempFileArray[n]#">
				</cfhttp>
				<cfdirectory action="list" directory="#variables.repertoire#" 
			recurse="false" name="qget" filter="#compte#">
				<cfif qget.recordcount eq 0>
					<cfdirectory action="create" directory="#variables.repertoire#/#compte#" mode="777">
				</cfif>
				<cffile mode="777" action="write" file="#variables.repertoire#/#compte#/#tempFileArray[n]#" output="#cfhttp.FileContent#" charset="iso-8859-1">
				<cfquery datasource="ROCOFFRE" name="qCheck">
					SELECT *
					FROM file_source t
					WHERE lower(t.extranet_filename)=lower('#tempFileArray[n]#')
					AND t.operateurid=32
				</cfquery>
				<cfif qCheck.recordcount eq 0>
					<cfquery datasource="ROCOFFRE" name="qListe">
						insert into
						file_source(extranet_filename,operateurid,date_recuperation,extranet_path,etat,date_mise_a_dispo,date_etat,source_filename)
						VALUES('#tempFileArray[n]#',32,sysdate,'#variables.repertoire#\#compte#',1,
								sysdate,sysdate,'#tempFileArray[n]#')
					</cfquery>
				<cfelse>
					<cfquery datasource="ROCOFFRE" name="qListe">
						update
						file_source t
						set date_recuperation=sysdate,
						date_etat=sysdate
						where lower(t.extranet_filename)=lower('#tempFileArray[n]#')
						AND t.operateurid=32
					</cfquery>
				</cfif>
				<cfset variables.statusFile[n]=1>
				<cfset variables.FichiersImportes=variables.FichiersImportes+1>
			<cfelse>
				<cfset variables.statusFile[n]=0>
				<cfset variables.FichiersPresents=variables.FichiersPresents+1>
			</cfif>
		</cfloop>
		<cfreturn "OK">
	</cffunction>
</cfcomponent>