<cfcomponent displayname="CegetelCDRProcessStrategy" extends="ProcessStrategy" output="false">
	<cfset variables.repertoire="/app/fichiers/1-recup/sfr_fixe/cdr">
	
	<!--- Mï¿½thode pour se logger, on ramï¿½ne le cookie --->
	<cffunction name="Login" access="remote" returntype="String" output="true">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		
		<cfhttp method="get" resolveurl="true"  url="https://extranet.sfrbusinessteam.fr:443/extranet/connexion/formulaireAuthentification_New.jsp" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
		</cfhttp>

			<!--- Parse pour identifier le cookie --->
			<cfset chaine=cfhttp.ResponseHeader["Set-Cookie"]>
			<cfset jCookie=GetToken(GetToken(chaine,1,";"),2,"=")>
			<cfoutput>
				Strategie : Cegetel<br>
			Cookie 1: #jCookie#<br>
			</cfoutput>
			
		
		<cfhttp method="POST" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Authentification_New" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
				<!--- <cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie)#"> --->
				<cfhttpparam type="formfield" name="login" value="#login#">
				<cfhttpparam type="formfield" name="password" value="#pwd#">
				<cfhttpparam type="formfield" name="Submit" value="Ok">
		</cfhttp>
		
		<cfif Len(cfhttp.FileContent) gte  5600 and Len(cfhttp.FileContent) lte  5800>
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" bcc="brice.miramont@consotel.fr,production@consotel.fr" server="192.168.3.119" 
						port="26"  type="HTML" subject="[Extranet NeufCegetel]- #login#">
				<style>
				html body {
					margin:0;
					padding:0;
					background: ##D4D0C8;
					font:x-small Verdana,Sans-serif;
				}
				p {
					font:bold x-small EurostileBold,verdana,arial,helvetica,serif;
				}
				td {
					font-size : x-small;
					/*color: ##629FB5;*/
				}
				</style>
						
				<p>Login : #login#</p>
				Probl�me de login:<br>
				login: #login#<br>
				password: #pwd#<br>
			</cfmail> 
		</cfif>
		
		<cfhttp method="POST" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/cnet/loginNeufCegetel1.jsp" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
				<!--- <cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie)#"> --->
				<cfhttpparam type="formfield" name="login" value="#login#">
				<cfhttpparam type="formfield" name="passwd" value="#pwd#">
				<cfhttpparam type="formfield" name="ldcg" value="">
				<cfhttpparam type="formfield" name="actionForm" value="ValidLogin">
		</cfhttp>

			<!--- Parse pour identifier le cookie --->
			<cfset chaine=cfhttp.ResponseHeader["Set-Cookie"]>
			<cfset jCookie=GetToken(GetToken(chaine,1,";"),2,"=")>
			<cfoutput>
			Cookie 2: #jCookie#<br>
			</cfoutput>
		<cfreturn jCookie>
	</cffunction>
	
	<!--- Mï¿½thode pour prï¿½parer l'extranet avant d'accï¿½der aux factures --->
	<cffunction name="PrepareExtranet" access="remote" returntype="array" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/cnet/Connexion.action" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
				<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
				<cfhttpparam type="url" name="login" value="#login#">
				<cfhttpparam type="url" name="passwd" value="#pwd#">
				<cfhttpparam type="url" name="ldcg" value="">
				<cfhttpparam type="url" name="actionForm" value="ValidLogin">
		</cfhttp>
		
		<cfset deb=find("dateDansAdresse",cfhttp.FileContent,1)>
		<cfset fin=find(">",cfhttp.FileContent,deb)>

		<cfif deb gt 0>
		
			<cfset dateDansAdresse1=Mid(cfhttp.FileContent,deb+16,fin-deb-17)>
			
			<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/cnet/Connexion.action" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="url" name="actionForm" value="ControleProfil">
					<cfhttpparam type="url" name="dateDansAdresse" value="#dateDansAdresse1#">
			</cfhttp>
			
			<cfset deb=find("dateDansAdresse",cfhttp.FileContent,1)>
			<cfset fin=find(">",cfhttp.FileContent,deb)>
			<cfset dateDansAdresse2=Mid(cfhttp.FileContent,deb+16,fin-deb-17)>
			
			<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/cnet/Menu.action" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="url" name="actionForm" value="Princ">
					<cfhttpparam type="url" name="dateDansAdresse" value="#dateDansAdresse2#">
			</cfhttp>
			
			<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/cnet/menu/redirMenuBandeauGerer.jsp" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="url" name="actionForm" value="BandeauGerer">
			</cfhttp>
			
			<cfset deb=find("dateDansAdresse",cfhttp.FileContent,1)>
			<cfset fin=find(">",cfhttp.FileContent,deb)>
			<cfset dateDansAdresse3=Mid(cfhttp.FileContent,deb+16,fin-deb-17)>
			
			<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/cnet/Menu.action" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="url" name="actionForm" value="BandeauGerer">
					<cfhttpparam type="url" name="dateDansAdresse" value="#dateDansAdresse3#">
			</cfhttp>
			
			<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/cnet/menu/redirAccueilConsulter.jsp" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="url" name="actionForm" value="EspaceConsulter">
			</cfhttp>
			<cfset deb=find("dateDansAdresse",cfhttp.FileContent,1)>
			<cfset fin=find(">",cfhttp.FileContent,deb)>
			<cfset dateDansAdresse4=Mid(cfhttp.FileContent,deb+16,fin-deb-17)>
			
			<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/cnet/Menu.action" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="url" name="actionForm" value="EspaceConsulter">
					<cfhttpparam type="url" name="dateDansAdresse" value="#dateDansAdresse4#">
			</cfhttp>
			
			<cfset deb=find("dateDansAdresse",cfhttp.FileContent,1)>
			<cfset fin=find("#chr(34)#",cfhttp.FileContent,deb)>
			<cfset variables.instance.dateDansAdresse=Mid(cfhttp.FileContent,deb+16,fin-deb-16)>
	
			<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/cnet/Menu.action" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="url" name="actionForm" value="MenuConsulter">
					<cfhttpparam type="url" name="dateDansAdresse" value="#variables.instance.dateDansAdresse#">
			</cfhttp>
			
			<cfset v=ArrayNew(1)>
			<cfset v=ListToArray(cfhttp.FileContent,"<")>
			<cfset aLibelle=ArrayNew(1)>
			<cfset aLien=ArrayNew(1)>
			<cfset compteur=1>
			<cfoutput>
			<cfloop from="1" to="#ArrayLen(v)#" index="i">
				<cfif find("Duplicata de vos factures",v[i])>
					<cfset aLibelle[compteur]=GetToken(v[i],2,"-")>
					<cfset aLien[compteur]=GetToken(v[i-1],4,"'")>
					<cfset compteur=compteur+1>
				</cfif>
			</cfloop>
			</cfoutput>
		<cfelse>
			<cfset aLien=ArrayNew(1)>
		</cfif>
		<cfreturn aLien>
	</cffunction>
		
	<!--- Mï¿½thode se connecter au compte, ramï¿½ne le nouveau cookie si besoin --->
	<cffunction name="connectCompte" access="remote" returntype="string" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="IdCompte" type="String">
		<!--- On test la suite --->
		
		<cfhttp method="POST" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/cnet/Menu.action" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
				<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
				<cfhttpparam type="formfield" name="dateDansAdresse" value="#variables.instance.dateDansAdresse#">
				<cfhttpparam type="formfield" name="champID" value="#IdCompte#">
				<cfhttpparam type="formfield" name="actionForm" value="LiensMasques">
		</cfhttp>
		
		<cfset deb=find("<FORM",cfhttp.FileContent,1)>
		<cfset fin=Len(cfhttp.FileContent)>
		<cfset chaine=Mid(cfhttp.FileContent,deb,fin-deb)>
		<cfset v=ArrayNew(1)>
		<cfset v=ListToArray(chaine,"=")>
		<cfloop from="1" to="#ArrayLen(v)#" index="i">
			<cfset chaineTemp=GetToken(v[i],1,"#chr(34)#")>
			<cfif Trim(chaineTemp) eq "frmEnvoi">
				<cfset adresse=Trim(GetToken(v[i+2],1,"#chr(34)#"))>
				<cfset mode=Trim(GetToken(v[i+6],1,"#chr(34)#"))>
				<cfset from=Trim(GetToken(v[i+9],1,"#chr(34)#"))>
				<cfset tokenid=Trim(GetToken(v[i+12],1,"#chr(34)#"))>
				<cfset appname=Trim(GetToken(v[i+15],1,"#chr(34)#"))>
				<cfbreak>
			</cfif>
		</cfloop>
		<cfhttp method="POST" redirect="no" resolveurl="true" url="#adresse#" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
				<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
				<cfhttpparam type="formfield" name="mode" value="#mode#">
				<cfhttpparam type="formfield" name="from" value="#from#">
				<cfhttpparam type="formfield" name="tokenid" value="#tokenid#">
				<cfhttpparam type="formfield" name="appname" value="#appname#">
		</cfhttp>
			<!--- Parse pour identifier le cookie --->
			<cfset chaine=cfhttp.ResponseHeader["Set-Cookie"]>
			<cfset jCookie=GetToken(GetToken(chaine,1,";"),2,"=")>
			<cfoutput>
			Cookie:#jCookie#<br>
			</cfoutput>
		<cfreturn jCookie>
	</cffunction>
	
	<!--- Mï¿½thode pour aller chercher la liste des factures --->
	<cffunction name="getListeFactures" access="remote" returntype="array" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="nbreMois" type="numeric">
		
			<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Facture" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="url" name="tache" value="image">
					<cfhttpparam type="url" name="rapport" value="1">
			</cfhttp>
			
			<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Facture" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="url" name="tache" value="image">
					<cfhttpparam type="url" name="rapport" value="1">
					<cfhttpparam type="url" name="TROISDERNIERS" value="1">
					<cfhttpparam type="url" name="action" value="formDIVGE">
			</cfhttp>
			
			<cfset thread = CreateObject("java", "java.lang.Thread")>
			<cfset thread.sleep(2000)>
			
			<cfset v=ArrayNew(1)>
			<cfset v=ListToArray(cfhttp.FileContent,"<")>
			<cfloop from="1" to="#ArrayLen(v)#" index="i">
				<cfif Left(v[i],5) eq "input">
					<cfset u=ArrayNew(1)>
					<cfset u=ListToArray(v[i],"#chr(34)#")>
					<cfbreak>
				</cfif>
			</cfloop>
			<cfset idW=u[6]>
			
			<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Wait" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="formfield" name="idWaiter" value="#idW#">
			</cfhttp>
			
			<cfset datefin=LsDateFormat(DateAdd("m",-#shiftMois#,now()),"yyyymm")>
			<cfset datedeb=LsDateFormat(DateAdd("m",-#Evaluate(nbreMois+shiftMois)#,now()),"yyyymm")>
			
			<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Facture" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="formfield" name="tache" value="image">
					<cfhttpparam type="formfield" name="action" value="search">
					<cfhttpparam type="formfield" name="CONTRAT_T" value="TOUS">
					<cfhttpparam type="formfield" name="FACTURE_T" value="Saisir un Nï¿½ de facture">
					<cfhttpparam type="formfield" name="rapport" value="0">
					<cfhttpparam type="formfield" name="DATE_DEB" value="#datedeb#">
					<cfhttpparam type="formfield" name="DATE_FIN" value="#datefin#">
					<cfhttpparam type="formfield" name="COMPTE_T" value="TOUS">
			</cfhttp>
			
			<cfset v=ArrayNew(1)>
			<cfset v=ListToArray(cfhttp.FileContent,"<")>
			<cfloop from="1" to="#ArrayLen(v)#" index="i">
				<cfif Left(v[i],5) eq "input">
					<cfset u=ArrayNew(1)>
					<cfset u=ListToArray(v[i],"#chr(34)#")>
					<cfbreak>
				</cfif>
			</cfloop>
			<cfset idW=u[6]>
			
			<cfset thread.sleep(6000)>
			
			<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Wait" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="formfield" name="idWaiter" value="#idW#">
			</cfhttp>
			
			<cfset v=ArrayNew(1)>
			<cfset v=ListToArray(cfhttp.FileContent,"<")>
			<cfset aFacture=ArrayNew(1)>
			<cfset afichiers=ArrayNew(1)>
			<cfset compteur=1>
			<cfset compteur2=1>
			<cfoutput>
			<cfloop from="1" to="#ArrayLen(v)#" index="i">
				<cfif left(v[i],1) eq "a">
					<cfset aFacture[compteur]=GetToken(v[i],6,"#chr(34)#")>
					<cfif Find("CSV",aFacture[compteur],1) gt 0>
						<cfset afichiers[compteur2]=aFacture[compteur]>
						<cfset compteur2=compteur2+1>
					</cfif>
					<cfset compteur=compteur+1>
				</cfif>
			</cfloop>
			</cfoutput>
		<cfreturn afichiers>
	</cffunction>
	
	<!--- Mï¿½thode pour aller tï¿½lï¿½charger le fichier --->
	<cffunction name="getFile" access="remote" returntype="string" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="ElementFacture" type="string">
		<cfargument name="compte" type="string">
		<cfset crm=GetToken(ElementFacture,2,"'")>
		<cfset numFact=GetToken(ElementFacture,4,"'")>
		<cfset date=GetToken(ElementFacture,6,"'")>
		<cfset fast=GetToken(ElementFacture,9,"'")>
		<cfset ordDoc=GetToken(ElementFacture,11,"'")>
		<cfset ordSDoc=GetToken(ElementFacture,13,"'")>
		<cfset filename=numFact & "_" & ordDoc & "_" & ordSDoc>
		<cfif ordSDoc eq "1">
			<cfquery datasource="ROCOFFRE" name="qListe">
				SELECT *
				FROM file_source t
				WHERE lower(t.extranet_filename)=lower('#filename#.zip')
				AND t.operateurid=534
			</cfquery>
			<cfif qListe.recordcount eq 0>
				<cfset retour=#filename# & ".zip">
				<cfhttp method="POST" redirect="no" getasbinary="yes" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/CRMAccesFacture" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="formfield" name="tache" value="getFile">
					<cfhttpparam type="formfield" name="action" value="directSousDocCE">
					<cfhttpparam type="formfield" name="CRM" value="#crm#">
					<cfhttpparam type="formfield" name="numFact" value="#numFact#">
					<cfhttpparam type="formfield" name="dateEmis" value="#date#">
					<cfhttpparam type="formfield" name="refCli" value="#date#">
					<cfhttpparam type="formfield" name="fact" value="#fast#">
					<cfhttpparam type="formfield" name="idDoc" value="#fast#">
					<cfhttpparam type="formfield" name="ordDoc" value="#ordDoc#">
					<cfhttpparam type="formfield" name="ordSDoc" value="#ordSDoc#">
					<cfhttpparam type="formfield" name="format" value="CSV">
				</cfhttp>
				<cfdirectory action="list" directory="#variables.repertoire#" 
			recurse="false" name="qget" filter="#compte#">
				<cfif qget.recordcount eq 0>
					<cfdirectory action="create" directory="#variables.repertoire#/#compte#" mode="777">
				</cfif>
				<cffile mode="777" action="write" file="#variables.repertoire#/#compte#/#filename#.zip" output="#cfhttp.FileContent#">
				<cfzip action="list" file="#variables.repertoire#/#compte#/#filename#.zip" name="qFile"/>
				<cfquery datasource="ROCOFFRE" name="qListe">
					insert into
					file_source(extranet_filename,operateurid,date_recuperation,extranet_path,etat,date_mise_a_dispo,date_etat,source_filename)
					VALUES('#filename#.zip',534,sysdate,'#variables.repertoire#/#compte#',1,
							to_date('#lsDateFormat(qFile.DATELASTMODIFIED,"dd/mm/yyyy")#','dd/mm/yyyy'),sysdate,'#filename#.zip')
				</cfquery>
			<cfelse>
				<cfset retour="">
			</cfif>
		<cfelse>
			<cfset retour="">
		</cfif>
		<cfreturn retour>
	</cffunction>
</cfcomponent>