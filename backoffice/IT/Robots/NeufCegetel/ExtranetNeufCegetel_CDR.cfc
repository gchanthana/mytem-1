<cfcomponent name="ExtranetNeufCegetel_CDR" output="false">
	<cfset variables.instance.dateDansAdresse1=0>
	<cffunction name="Main" access="remote" returntype="xml" output="false" description="Main" hint="Main" displayname="Main">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="extranet" type="String">
		<cfargument name="nbreMois" type="numeric">
		<cfargument name="shiftMois" type="numeric">
		<cfargument name="compte" type="string">
		
	
		<!--- trim: supprime les espaces au debut et a la fin de la chaine --->
		<cfif Trim(compte) eq "">
			<CFSET compte="INCONNU">
		</cfif>
		<!--- Client Neuf Cegetel --->
		<cfif Trim(extranet) eq 1>
			<cfset extranet='Neuf'>
			<!--- Client Cegetel --->
			<cfelseif Trim(extranet) eq 2>
				<cfset extranet='Cegetel'>
				<cfelseif Trim(extranet) eq 4>
					<cfset extranet='NeufMulti'>
		</cfif>
		
		<cfset tempChaine = "<html><body><table border='0'>">
		<cfset compteurFichier=0>

		<!--- On definie les objets --->
		<cfset setStrategy(extranet)>

		<!--- On se log --->	
		<cfset jCookie = "">					
		<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>

		 <cfif jCookie eq "" >	
			 <!--- Probl�me de connexion � l'extranet --->	
		 	<cfoutput >Pas possible de se connecter � l'extranet neufCegetel <br/></cfoutput>
			<cfscript>
				 MyDoc = XmlNew();
				 MyDoc.xmlRoot = XmlElemNew(MyDoc,"MyRoot");
			</cfscript>		 
			<cfreturn MyDoc>
		</cfif>
					
		<cfoutput >preparer strategie OK <br/></cfoutput>
		
		<!--- Preparation de l'extranet avant d'aller a la page des rapports CDR --->
		<!--- Ramener la liste des comptes concernes --->
		<cfset aClients=ArrayNew(1)>
		<cfset aClients=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
		
		
	
		
		<cfif ArrayLen(aClients) eq 0>
			<!--- Pas possible de recuperer la liste des comptes, On sort de la fonction et on passe au prochain acces --->
			 <cfscript>
				 MyDoc = XmlNew();
				 MyDoc.xmlRoot = XmlElemNew(MyDoc,"MyRoot");
			</cfscript>		 
			<cfreturn MyDoc>
			<!---Type extranet 4 avec un seul compte--->
			<cfelseif ArrayLen(aClients) eq 2>
					<cfset jCookie=variables.instance.ProcessStrategy.connectCompte(jCookie,login,pwd,compte,aClients[1])>							
					<cfif jCookie neq "">			
						<!---Aller chercher la liste des liens des rapports CDR--->
						<cfset TabLienCDR = ArrayNew(2)>
						<cfset TabLienCDR=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,login,pwd,compte,aClients[2])>
						<!--- Aller chercher les rapports CDR a partir de leurs liens --->	
						<cfset aListeCDR = ArrayNew(1)>	
						<cfloop from="1" to="#ArrayLen(TabLienCDR)#" index="j">		
							<cfset FileName="">									
							<cfset FileName=variables.instance.ProcessStrategy.getFile(jCookie,TabLienCDR[j],compte)>
							<cfset aListeCDR[j]=#FileName#>																						
						</cfloop>
						
						<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
									server="192.168.3.119" 
									port="26"  type="html" 
									subject="[Robot2 CDR SFR Fixe]- Liste des facture">
							
									Bonjour. <br/>
									
									Compte = #compte#<br/>
									Login = #login#<br/>
									Mot de passe = #pwd#<br/>
									
									<cfdump var="#aListeCDR#">
									
						</cfmail> 	
					</cfif>	
							
						
				<!---Type extranet 4 avec un plusieurs comptes--->
				<cfelseif ArrayLen(aClients) gt 2>
						<cfset TabLienCDR = ArrayNew(1)>					
						<cfloop from="3" to="#ArrayLen(aClients)#" index="i">
								<cfset aLienCDR = ArrayNew(1)>							
								<cfset jCookie=variables.instance.ProcessStrategy.connectCompte(jCookie,login,pwd,compte,aClients[i])>																
								<cfif jCookie neq "">			
									<!---Aller chercher la liste des liens des rapports CDR--->
									<cfset aLienCDR=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,login,pwd,compte,aClients[i])>
									<cfset TabLienCDR.addAll(aLienCDR)>
								</cfif>															
						</cfloop>	
						
						
						<!--- Aller chercher les rapports CDR a partir de leurs liens --->	
						<!--- Verifier si la connexion n'a pas été perdu en route --->	
						<cfif jCookie neq "">
							<cfset aListeCDR = ArrayNew(1)>	
							<cfloop from="1" to="#ArrayLen(TabLienCDR)#" index="j">		
								<cfset FileName="">									
								<cfset FileName=variables.instance.ProcessStrategy.getFile(jCookie,TabLienCDR[j],compte)>
								<cfset aListeCDR[j]=#FileName#>																						
							</cfloop>
							
							<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
										server="192.168.3.119" 
										port="26"  type="html" 
										subject="[Robot2 CDR SFR Fixe]- Liste des facture">
								
										Bonjour. <br/>
										
										Compte = #compte#<br/>
										Login = #login#<br/>
										Mot de passe = #pwd#<br/>
										
										<cfdump var="#aListeCDR#">
										
							</cfmail> 					
						</cfif>	
					<cfelse>
						<!---Type extranet 1--->
						<!---Aller chercher la liste des liens des rapports CDR--->
						<cfset aLienCDR = ArrayNew(1)>
						<cfset aLienCDR=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,login,pwd,compte)>
										
						<!--- Aller chercher les rapports CDR a partir de leurs liens --->
						<cfset aListeCDR = ArrayNew(1)>
						<cfloop from="1" to="#ArrayLen(aLienCDR)#" index="j">	
							<cfset FileName="">			
							<cfset FileName=variables.instance.ProcessStrategy.getFile(jCookie,aLienCDR[j],compte)>	
							<cfset aListeCDR[j]=#FileName#>									
						</cfloop>
						
						
						<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
									server="192.168.3.119" 
									port="26"  type="html" 
									subject="[Robot1 CDR SFR Fixe]- Liste des facture">
							
									Bonjour. <br/>
									
									Compte = #compte#<br/>
									Login = #login#<br/>
									Mot de passe = #pwd#<br/>
									
									<cfdump var="#aListeCDR#">
									
						</cfmail> 
		
		</cfif>
					
		<cfoutput > Fin OK </b></cfoutput>
		<!--- Pause d'1 minute avant de sortir--->
		<!--- <cfset oThread = CreateObject("java", "java.lang.Thread")>
		<cfset oThread.sleep(240000)> --->
			
		<cfscript>
			MyDoc = XmlNew();
			MyDoc.xmlRoot = XmlElemNew(MyDoc,"MyRoot");
		</cfscript>		 
		<cfreturn MyDoc>
	</cffunction>
		
	<!--- Pour mettre en place la strategie --->
	<cffunction name="setStrategy" access="public" returntype="void" output="false" >
		<cfargument name="TYPE_EXTRANET" required="false" type="string" default="" displayname="string TYPE_EXTRANET" hint="type de pï¿½rimetre" />		
		<cfset temp=createObject("component","#TYPE_EXTRANET#ProcessStrategy_CDR")>		
		<cfset setProcessStrategy(temp)>
	</cffunction>
	
	<!--- Setter pour la strategie --->
	<cffunction name="setProcessStrategy" access="public" output="false" returntype="void">
		<cfargument name="newProcessStrategy" type="ProcessStrategy" required="true" />
		<cfset variables.instance.ProcessStrategy = arguments.newProcessStrategy />
	</cffunction>
	
	
</cfcomponent>


