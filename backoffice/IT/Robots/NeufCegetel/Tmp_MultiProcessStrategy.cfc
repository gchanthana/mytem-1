<cfcomponent displayname="NeufMultiProcessStrategy" extends="ProcessStrategy" output="true">				

	<cfset variables.repertoire="/app/fichiers/1-recup/sfr_fixe/facture">
	<cfset variables.cookie1="">
	<cfset variables.cookie2="">
	<cfset variables.nbTentative = 10>
	<cfset variable.idInterface = 0>
	
	<!--- Methode pour se loguer --->
	<cffunction name="Login" access="remote" returntype="String" output="true">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="String">
		
		<cfset jCookie1 = "">
		<cfset jCookie2 = "">

		<cfloop index="i" from="1" to="30">
			<!--- Aller sur la page de login --->
			<cfhttp redirect="true"  method="get" resolveurl="true"
					url="https://extranet.sfrbusinessteam.fr/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="10">
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
			</cfhttp>			
			<cfif structkeyexists(cfhttp.ResponseHeader,"Set-Cookie")>
				<cfbreak>
			</cfif>
		</cfloop>		
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Set-Cookie")>		
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Problème de connection">
				
						Bonjour. <br/>
						
						Apres 30 tentatives le robot n'a pas pu se connecté à l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfreturn jCookie1>		
		</cfif>
		 	
		
		<!--- Recuperer le cookie --->
		<cfset chaine=cfhttp.ResponseHeader["Set-Cookie"]>
		<cfset jCookie1=GetToken(GetToken(chaine,1,";"),2,"=")>
	 	
		
		<cfloop index="i" from="1" to="30">	
			<!--- Redirection vers la page suivante --->
			<cfhttp method="POST" resolveurl="true"  redirect="false"			 
					url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage/"									
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="10">										
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie1)#">
					<cfhttpparam type="formfield" name="id2_hf_0" value="">
					<cfhttpparam type="formfield" name="login" value="#login#">				
					<cfhttpparam type="formfield" name="password" value="#pwd#">
					<cfhttpparam type="formfield" name="codeSecu" value="">
					<cfhttpparam type="formfield" name="valider" value="submit">
					<cfhttpparam type="url"	name="wicket:interface" value=":0:loginForm::IFormSubmitListener::">
					<cfhttpparam type='header' name='Accept' value='text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'>
					<cfhttpparam type='header' name='Accept-Language' value='fr'>
					<cfhttpparam type='header' name='Accept-Encoding' value='gzip, deflate'>
					<cfhttpparam type='header' name='Accept-Charset' value='ISO-8859-1,utf-8;q=0.7,*;q=0.7'>
					<cfhttpparam type='header' name='Keep-Alive' value='115'>
					<cfhttpparam type='header' name='Connection' value='keep-alive'>			 	
			</cfhttp>  	
			
			<cfif structkeyexists(cfhttp.ResponseHeader,"Set-Cookie")>
				<cfbreak>
			</cfif>
		</cfloop>
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Set-Cookie")>
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Apres 30 tentatives le robot n'a pas pu se connecté à l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage/<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
				
			<cfreturn jCookie2>		
		</cfif>		
		
		<!--- Permet de récupérer le 2eme Cookie, car dans le POST un autre cookie est généré qui est différent de cookie1 ce qui a généré un problème  --->
		<cfset chaine2=cfhttp.ResponseHeader["Set-Cookie"]>
		<cfset jCookie2=GetToken(GetToken(chaine2,1,";"),2,"=")>
		

		<!--- <cfdump var="#cfhttp.ResponseHeader#">
		<cfabort showerror="fin0"> --->
		
		
		<!--- Redirection vers la page suivante avec le nouveau cookie--->	
		<cfloop index="i" from="1" to="30">	
			<cfhttp method="GET" resolveurl="true"  redirect="true" url="https://extranet.sfrbusinessteam.fr/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage/wicket:interface/../../../" timeout="10">	
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="url"    name="wicket:interface"  value=":1::::">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie2)#">
					<cfhttpparam type='header' name='Accept-Language' value='fr'>
					<cfhttpparam type='header' name='Accept-Encoding' value='gzip, deflate'>
					<cfhttpparam type='header' name='Accept-Charset' value='ISO-8859-1,utf-8;q=0.7,*;q=0.7'>
					<cfhttpparam type='header' name='Keep-Alive' value='115'>
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
			</cfhttp>
			<cfif structkeyexists(cfhttp.ResponseHeader,"Connection")>
				<cfbreak>
			</cfif>
		</cfloop>
		
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>	
			<cfdump var="#cfhttp.ResponseHeader#">	
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage/wicket:interface/../../../<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset jCookie2 = "">
			<cfreturn jCookie2>		
		</cfif>
		
		
		<!--- Redirection vers la page suivante --->
		<cfloop index="i" from="1" to="30">	
			<cfhttp method="GET" resolveurl="true"  redirect="false"		
					url="https://extranet.sfrbusinessteam.fr/extranet/cvg/accueil" timeout="10">
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type='header' name='Accept' value='image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, application/x-ms-application, application/x-ms-xbap, application/vnd.ms-xpsdocument, application/xaml+xml, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie2)#">
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type='header' name='Accept-Encoding' value='gzip, deflate'>
					<cfhttpparam type='header' name='Content-Type' value='application/x-www-form-urlencoded'>
					<cfhttpparam type='header' name='Cache-Control' value='no-cache'>
			</cfhttp>
			<cfif structkeyexists(cfhttp.ResponseHeader,"Connection")>
				<cfbreak>
			</cfif>
		</cfloop>
			
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>		
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/cvg/accueil<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset jCookie2 = "">
			<cfreturn jCookie2>		
		</cfif>
		
		
		<cfreturn jCookie2>
	</cffunction>		


	<!--- Methode pour prï¿½parer l'extranet avant d'accï¿½der aux factures --->
	<cffunction name="PrepareExtranet" access="remote" returntype="array" output="false">
		<cfargument name="jsession" type="String">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="String">
		
		<!--- Menu "Vos factures" --->	
		<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">			
		</cfhttp>
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(2)>
			<cfreturn aTemp>	
		</cfif>

		<!--- Redirection vers la page suivante --->
		<cfhttp method="GET" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr/extranet/cvg/sr/factures" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
			
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/cvg/sr/factures<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(2)>
			<cfreturn aTemp>
		 </cfif>
		
		
		
		<!--- Menu "Vos factures fixes" --->
		<cfhttp method="GET" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr/extranet/cvg/sr/generique/codeNoeud/94.4" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/cvg/sr/generique/codeNoeud/94.4/br>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(2)>
			<cfreturn aTemp>
		 </cfif>

	  	<!--- Lien: Export facture CSV --->
		<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type='formfield' name='code_service' value='1368999'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">					
		</cfhttp>

		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(2)>
			<cfreturn aTemp>
		</cfif>
		
		<!--- Redirection vers la page suivante --->	
		<cfhttp method="GET" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr/extranet/cvg/?wicket:bookmarkablePage=:com.sfr.facture.EntiteOrganiquePage&typeAffichage=RAPPORT&1308664880100" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
				<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/cvg/?wicket:bookmarkablePage=:com.sfr.facture.EntiteOrganiquePage&typeAffichage=RAPPORT&1308664880100<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(2)>
			<cfreturn aTemp>
		</cfif>

		
		<!--- Récupérer la liste des compte --->
		<cfset v=ArrayNew(1)>
		<cfset v=ListToArray(cfhttp.FileContent,"<")>
		
		
		<cfset k=ArrayNew(1)>
		<cfset u=ArrayNew(2)>
		<cfset j = 1>
		<cfloop from="1" to="#ArrayLen(v)#" index="i">
			<cfif Left(v[i],13) eq "option value=">
				<cfset k=ListToArray(v[i],"#chr(34)#")>
				<cfset u[j][1]=k[2]>
				<cfset u[j][2]=k[3]>
				<cfset j = j+1>
			</cfif>
		</cfloop>

		
	  	<!--- On va chercher l'id de l'interface pour le parmètres wicket:interface  --->
		<cfset pos = findNocase('wicket:interface=',cfhttp.FileContent)>
		<cfif #pos# eq 0 >
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Déconnexion avant d'aller chercher le paramètre "idInterface".<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(2)>
			<cfreturn aTemp>
		</cfif>
		
		<cfset chaine = mid(cfhttp.FileContent,pos,50)>			
		<cfset tab = listToArray(chaine,':')>
		<cfset idInterface = tab[3]>	
			
		<!--- On va chercher l'ensemble des parametre a envoyer dans le cfhttp'...  --->	
		<cfset pos = findNocase('<form wicket:id="form"',cfhttp.FileContent)>
		<cfif pos gt 0>				
				<cfset pos1 = findNocase('name=',cfhttp.FileContent,pos)>
				<cfset pos1 = pos1-pos>		
				<cfset chaine = mid(cfhttp.FileContent,pos,pos1)>
				<cfset tab = listToArray(chaine,'#chr(34)#')>
				<cfset Varid = tab[4]>
				<cfset VarAction= tab[8]>
				<cfoutput >Varid=#Varid#</cfoutput>
				<cfoutput >VarAction=#VarAction#</cfoutput>
			<cfelse >
				<cfset pos = findNocase('form id="',cfhttp.FileContent)>
				<cfset pos1 = findNocase('name=',cfhttp.FileContent,pos)>
				<cfset pos1 = pos1-pos>		
				<cfset chaine = mid(cfhttp.FileContent,pos,pos1)>
				<cfset tab = listToArray(chaine,'#chr(34)#')>	
				<cfset Varid = tab[2]>
				<cfset VarAction= tab[6]>
				<cfoutput >Varid=#Varid#</cfoutput>
				<cfoutput >VarAction=#VarAction#</cfoutput>
		</cfif>
	
		<!--- On va chercher le parametre entiteOrganique, parametre utilise par le developpeur de l'extranet sfrbusinessteam'...  --->
		<cfset pos = findNocase('form:entiteOrganique:',cfhttp.FileContent)>
		<cfset chaine = mid(cfhttp.FileContent,pos,50)>
		<cfset tab = listToArray(chaine,'#chr(58)#',true)>
		<cfset IdentiteOrganique = tab[3]>

		
		<cfreturn u>
	</cffunction>
		
	
	<!--- Methode se connecter au compte --->
	<cffunction name="connectSociete" access="remote" returntype="string" output="false">
		<cfargument name="jsession" type="String">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="String">
		<cfargument name="IdSociete" type="String">
		<cfargument name="NomSociete" type="String">

		
		<!--- On va chercher le parametre entiteOrganique, parametre utilise par le developpeur de l'extranet sfrbusinessteam'...  --->
		<cfset pos = findNocase('form:entiteOrganique:',cfhttp.FileContent)>
			
		<cfif pos gt 0>
				<cfset chaine = mid(cfhttp.FileContent,pos,50)>
				<cfset tab = listToArray(chaine,'#chr(58)#',true)>
				<cfset IdentiteOrganique = tab[3]>
				
				
				<!--- On va chercher l'ensemble des parametre a envoyer dans le cfhttp'...  --->
				<cfset pos = findNocase('<form wicket:id="form"',cfhttp.FileContent)>
				<cfif pos gt 0>				
						<cfset pos1 = findNocase('name=',cfhttp.FileContent,pos)>
						<cfset pos1 = pos1-pos>		
						<cfset chaine = mid(cfhttp.FileContent,pos,pos1)>
						<cfset tab = listToArray(chaine,'#chr(34)#')>
						<cfset Varid = tab[4]>
						<cfset VarAction= tab[8]>
						<cfoutput >Varid=#Varid#</cfoutput>
						<cfoutput >VarAction=#VarAction#</cfoutput>
					<cfelse >
						<cfset pos = findNocase('form id="',cfhttp.FileContent)>
						<cfset pos1 = findNocase('name=',cfhttp.FileContent,pos)>
						<cfset pos1 = pos1-pos>		
						<cfset chaine = mid(cfhttp.FileContent,pos,pos1)>
						<cfset tab = listToArray(chaine,'#chr(34)#')>	
						<cfset Varid = tab[2]>
						<cfset VarAction= tab[6]>
						<cfoutput >Varid=#Varid#</cfoutput>
						<cfoutput >VarAction=#VarAction#</cfoutput>
				</cfif>
				
				
				<cfloop index="i" from="1" to="5">	
						<!--- On se connecte au compte "IdSociete" --->			
						<cfhttp method="POST" redirect="false" resolveurl="true" url="#VarAction#"
								useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
								<cfhttpparam type="formfield" name="id29_hf_0" value=":form:entiteOrganique:#IdentiteOrganique#:IOnChangeListener::">
								<cfhttpparam type="formfield" name="entiteOrganique" value="#IdSociete#">						
								<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
								<cfhttpparam type='header' name='te' value='deflate;q=0'> 
								<cfhttpparam type='header' name='Connection' value='keep-alive'>
								<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
						</cfhttp>				
						<cfif structkeyexists(cfhttp.ResponseHeader,"Location")>
							<cfbreak>
						</cfif>
				</cfloop>		
					
					
				<cfif not structkeyexists(cfhttp.ResponseHeader,"Location")>	
											
						<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
									server="192.168.3.119" 
									port="26"  type="HTML" 
									subject="[Robot2 SFR Fixe]- Erreur Robot">
							
									Bonjour. <br/>
									
									Probleme de connexion à la société  #NomSociete#<br/>
									
									Compte = #compte#<br/>
									Login = #login#<br/>
									Mot de passe = #pwd#<br/>
									
									Cordialement,
						</cfmail> 
						<cfset jCookie="">
						<cfreturn jCookie>
				</cfif>		
						
				<!--- On affiche la page --->
				<cfhttp method="get" redirect="false" resolveurl="true" url="#cfhttp.ResponseHeader['Location']#"
							useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">			
							<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
							<cfhttpparam type='header' name='te' value='deflate;q=0'> 
							<cfhttpparam type='header' name='Connection' value='keep-alive'>
							<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
				</cfhttp>
						
				<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>		
						
						<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
									server="192.168.3.119" 
									port="26"  type="HTML" 
									subject="[Robot2 SFR Fixe]- Erreur Robot">
							
									Bonjour. <br/>
									
									Probleme d'affichage de la page de la société #NomSociete#<br/>
									
									Compte = #compte#<br/>
									Login = #login#<br/>
									Mot de passe = #pwd#<br/>
									
									Cordialement,
						</cfmail> 
						<cfset jCookie="">
						<cfreturn jCookie>
				</cfif>	
			<cfelse >
					<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr"
									server="192.168.3.119" 
									port="26"  type="HTML" 
									subject="[Robot2 SFR Fixe]- Erreur Robot">
							
									Bonjour. <br/>
									
									Il y a eu une déconnexion avant de se connecter à la Société : #NomSociete#<br/><br/>
									
									Compte = #compte#<br/>
									Login = #login#<br/>
									Mot de passe = #pwd#<br/>
									
									Cordialement,
						</cfmail> 
						<cfset jCookie="">
						<cfreturn jCookie>
		</cfif>
		

						
		<cfreturn jsession>
		
	</cffunction>
	
	
	<!--- Aller chercher la liste des comptes --->
	<cffunction name="ListeCompte" access="remote" returntype="array" output="false">
		<cfargument name="jsession" type="String">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="String">
		<cfargument name="NomSociete" type="String">
		
		<!--- Récupérer la liste des compte --->		
		<cfset pos = findNocase('Compte facturation',cfhttp.FileContent)>
		<cfset chaine = mid(cfhttp.FileContent,pos,9999999)>		
		<cfset v=ArrayNew(1)>
		<cfset v=ListToArray(chaine,"<")>		
		<cfset k=ArrayNew(1)>
		<cfset u=ArrayNew(1)>
		<cfset j = 1>
		<cfloop from="1" to="#ArrayLen(v)#" index="i">
			<cfif Left(v[i],13) eq "option value=">
				<cfset k=ListToArray(v[i],"#chr(34)#")>
				<cfset u[j]=k[2]>
				<cfset j = j+1>
			</cfif>
		</cfloop>
						
		<cfreturn u>
	</cffunction>
	
	
	
	<!--- Methode pour aller chercher la liste des factures --->
	<cffunction name="getListeFactures" access="remote" returntype="array" output="false">
		<cfargument name="jsession" type="String">
		<cfargument name="nbreMois" type="numeric">
		<cfargument name="shiftMois" type="numeric" default="0">
		<cfargument name="NoCompte" type="String">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="String">		
		<cfargument name="NomSociete" type="String">
			

		<!--- On calcul la période des factures à téléchrager --->
		<cfset datedeb=DateFormat(DateAdd("m",-#Evaluate(nbreMois+shiftMois)#,now()),"mm")>
		<cfif month(datedeb)neq 10 and month(datedeb)neq 11 and month(datedeb) neq 12 >
				<cfset datedeb=LsDateFormat(DateAdd("m",-#Evaluate(nbreMois+shiftMois)#,now()),"1/m/yy")>
				
		<cfelse> 
				<cfset datedeb=LsDateFormat(DateAdd("m",-#Evaluate(nbreMois+shiftMois)#,now()),"1/mm/yy")>
		</cfif>
		
		<cfset datefin=DateFormat(DateAdd("m",-#shiftMois#,now()),"mm")>
		<cfif month(datefin)neq 10 and month(datefin)neq 11 and month(datefin) neq 12 >
				<cfset datefin=LsDateFormat(DateAdd("m",-#shiftMois#,now()),"1/m/yy")>
				
		<cfelse> 
				<cfset datefin=LsDateFormat(DateAdd("m",-#shiftMois#,now()),"1/mm/yy")>
		</cfif>
		
		
		<!--- <cfset datedeb = "1/6/10">
		<cfset datefin = "1/7/11"> --->
		<cfset TabListeLienFacture = arrayNew(1)>
		<cfset TempTabListeLienFacture = arrayNew(1)>
		<cfset TempTabListeLienFacture2 = arrayNew(1)>
		
			
		<!--- On va chercher la liste des factures --->
		<!--- On lance la requête--->			
		<!--- Recuperer le lien a executer pour aller sur la page de des facture--->
		
		<!--- On va chercher le parametre entiteOrganique, parametre utilise par le developpeur de l'extranet sfrbusinessteam'...  --->
		<cfset IdentiteOrganique = 0>
		<cfset pos = findNocase('form:entiteOrganique:',cfhttp.FileContent)>
		<cfif #pos# gt 0>
			<cfset chaine = mid(cfhttp.FileContent,pos,50)>
			<cfset tab = listToArray(chaine,'#chr(58)#',true)>
			<cfset IdentiteOrganique = tab[3]>		
		<cfelse>
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Erreur Robot">
							
						Bonjour. <br/>
					
						Probleme de récupération de: IdentiteOrganique.<br/>
						Une deconnexion a eu lieu ou l'extranet a changé, Merci de vérifier.<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						Société : #NomSociete#<br/>
						
						Cordialement,

			</cfmail> 
			<cfset TabListeLienFacture[1]= -1>
			<cfreturn TabListeLienFacture>			
		</cfif>		
			
			
		<!--- On va chercher l'ensemble des parametre a envoyer dans le cfhttp'...  --->
		<!--- La 1ere occurence de la chaine rechercher concerne le tableau des compte --->
		<!--- ce qui nous interesse la c'est le 2eme tableau qui concerne les facture  --->
		<!--- la recherche commence juste apres la 1ere occurence de la chaine  --->				
		<cfset pos = findNocase('<form wicket:id="form"',cfhttp.FileContent)>
		<cfif pos gt 0>	
				<cfset pos = pos +1>
				<cfset pos = findNocase('<form wicket:id="form"',cfhttp.FileContent,pos)>			
				<cfset pos1 = findNocase('name=',cfhttp.FileContent,pos)>			
				<cfif #pos# gt 0 and #pos1# gt 0 >
					<cfset pos1 = pos1-pos>		
					<cfset chaine = mid(cfhttp.FileContent,pos,pos1)>
					<cfset tab = listToArray(chaine,'#chr(34)#')>
					<cfset Varid = tab[4]>
					<cfset VarAction= tab[8]>
				</cfif>
			<cfelse >
				<cfset pos = findNocase('form id="',cfhttp.FileContent)>
				<cfset pos = pos +1>
				<cfset pos = findNocase('form id="',cfhttp.FileContent,pos)>			
				<cfset pos1 = findNocase('name=',cfhttp.FileContent,pos)>				
				<cfif #pos# gt 0 and #pos1# gt 0 >
					<cfset pos1 = pos1-pos>		
					<cfset chaine = mid(cfhttp.FileContent,pos,pos1)>
					<cfset tab = listToArray(chaine,'#chr(34)#')>	
					<cfset Varid = tab[2]>
					<cfset VarAction= tab[6]>
				</cfif>
		</cfif>					
		
		
		<!--- Bouton "Rechercher", Lister les factures  --->
		<cfif #VarAction# neq "">
				<cfloop index="i" from="1" to="5">							
					<cfhttp method="POST" redirect="false" resolveurl="true" url="#VarAction#" 
							useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="600" >
							<cfhttpparam type="formfield" name="id#Varid#_hf_0" value=":form:entiteOrganique:#IdentiteOrganique#:IOnChangeListener::">
							<cfhttpparam type="formfield" name="moisDebut" value="#datedeb#">
							<cfhttpparam type="formfield" name="moisFin" value="#datefin#">
							<cfhttpparam type="formfield" name="noCf" value="#NoCompte#">
							<cfhttpparam type="formfield" name="noContrat" value="TOUS">
							<cfhttpparam type="formfield" name="noFacture" value="">												
							<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
							<cfhttpparam type='header' name='te' value='deflate;q=0'> 
							<cfhttpparam type='header' name='Connection' value='keep-alive'>
							<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					</cfhttp>			
					<cfif structkeyexists(cfhttp.ResponseHeader,"Location")>
						<cfbreak>
					</cfif>
				</cfloop>	
					
				<cfif not structkeyexists(cfhttp.ResponseHeader,"Location")>
					<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
								server="192.168.3.119" 
								port="26"  type="HTML" 
								subject="[Robot2 SFR Fixe]- Erreur Robot">
									
								Bonjour. <br/>
							
								Probleme de connexion a l'url (Liste des factures):<br/>
								#VarAction#<br/>
								
								Compte = #compte#<br/>
								Login = #login#<br/>
								Mot de passe = #pwd#<br/>
								Société : #NomSociete#<br/>
								
								Cordialement,
		
					</cfmail> 
					<cfset TabListeLienFacture[1]= -1>
					<cfreturn TabListeLienFacture>			
				</cfif>		
				
				
				<!--- Affichier la page --->
				<cfhttp method="get" redirect="false" resolveurl="true" url="#cfhttp.ResponseHeader['Location']#"
							useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="600" >					
							<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
							<cfhttpparam type='header' name='te' value='deflate;q=0'> 
							<cfhttpparam type='header' name='Connection' value='keep-alive'>
							<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
				</cfhttp>
				
				<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
					<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
								server="192.168.3.119" 
								port="26"  type="html" 
								subject="[Robot2 SFR Fixe]- Erreur Robot">
						
								Bonjour. <br/>
								
								Probleme d'affichage de la page des factures'"<br/>
								
								Compte = #compte#<br/>
								Login = #login#<br/>
								Mot de passe = #pwd#<br/>
								Société : #NomSociete#<br/>
								
								Cordialement,
					</cfmail> 
					<cfset TabListeLienFacture[1]= -1>
					<cfreturn TabListeLienFacture>
				</cfif>
								
					
				<!--- Recuperer la liste des liens des factures de la 1ere page --->
				<cfset TempTabListeLienFacture = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/\?wicket:interface=:[0-9]{1,4}:facture:factures:datatable:body:rows:[0-9]{1,4}:cells:[0-9]{1,4}:cell:docLinks:1:docLink:[0-9]{1,4}:ILinkListener::',cfhttp.FileContent) />
			    <cfset TabListeLienFacture.addAll(TempTabListeLienFacture)>		
				
				
				
				<!--- Recuperer le lien de la derniere page--->
				<cfset LienDernierePage = ArrayNew(1)>
				<cfset LienDernierePage = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/\?wicket:interface=:[0-9]{1,4}:facture:factures:datatable:topToolbars:[0-9]{1,4}:toolbar:span:navigator:last:[0-9]{1,4}:ILinkListener::',cfhttp.FileContent) />			
				
				<cfif arraylen(LienDernierePage) gt 0 >
					<cfset URLLastPage = LienDernierePage[1] >	
					<!--- Aller a la derniere page--->
					<cfhttp method="get" redirect="false" resolveurl="true" url="#URLLastPage#"
						 	useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="600">																  																									  
							<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
							<cfhttpparam type='header' name='te' value='deflate;q=0'> 
							<cfhttpparam type='header' name='Connection' value='keep-alive'>
							<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					</cfhttp>
					
					<cfif not structkeyexists(cfhttp.ResponseHeader,"Location")>			
						<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
									server="192.168.3.119" 
									port="26"  type="html" 
									subject="[Robot2 SFR Fixe]- Erreur Robot">
							
									Bonjour. <br/>
									
									Probleme de connexion a l'url :<br/>
									#URLLastPage#<br/>
									
									Compte = #compte#<br/>
									Login = #login#<br/>
									Mot de passe = #pwd#<br/>
									Société : #NomSociete#<br/>
									
									Cordialement,
						</cfmail> 
						<cfset ArrayInsertAt(TabListeLienFacture,1,-1)>
						<cfreturn TabListeLienFacture>
					</cfif>
					
					 <!--- Afficher la derniere page --->
					<cfhttp method="get" redirect="false" resolveurl="true" url="#cfhttp.ResponseHeader['Location']#"
							useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >						
							<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
							<cfhttpparam type='header' name='te' value='deflate;q=0'> 
							<cfhttpparam type='header' name='Connection' value='keep-alive'>
							<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					</cfhttp>
					
					<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
						<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
									server="192.168.3.119" 
									port="26"  type="html" 
									subject="[Robot2 SFR Fixe]- Erreur Robot">
							
									Bonjour. <br/>
									
									Probleme d'affichage de la derniere page des factures' :<br/>
									
									Compte = #compte#<br/>
									Login = #login#<br/>
									Mot de passe = #pwd#<br/>
									Société : #NomSociete#<br/>
									
									Cordialement,
						</cfmail>
						<cfset ArrayInsertAt(TabListeLienFacture,1,-1)>
						<cfreturn TabListeLienFacture>
					</cfif>
			
					<!--- Recuperer la liste des liens des factures de la derniere page --->
					<cfset TempTabListeLienFacture = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/\?wicket:interface=:[0-9]{1,4}:facture:factures:datatable:body:rows:[0-9]{1,4}:cells:[0-9]{1,4}:cell:docLinks:1:docLink:[0-9]{1,4}:ILinkListener::',cfhttp.FileContent) />
															
				    <cfset TabListeLienFacture.addAll(TempTabListeLienFacture)>
		
					
					<!--- Recupere le nombre de page, pour aller sur chaqune des page et recuperer la liste des liens des factures --->
					<cfset arrData = ArrayNew(1)>
					<cfset ListePage = ArrayNew(1)>
					<cfset listeFactures = ''>	
					
					<cfset arrData = arrayToList(REMatch('\<span wicket\:id\=\"pageNumber\"\>[0-9]{1,3}\<\/span\>',cfhttp.FileContent)) />
					<cfset ListePage = REMatch('[0-9]{1,3}',arrData)>
					<cfif arraylen(ListePage) eq 0>
						<cfset arrData = arrayToList(REMatch('title="Page [0-9]{1,3}"\>\<span\>',cfhttp.FileContent)) />
						<cfset ListePage = REMatch('[0-9]{1,3}',arrData)>
					</cfif>
					
					
					<cfif ArrayLen(ListePage) gt 0>
							<cfset NbPage = ListePage[arraylen(ListePage)]>
							<cfset n=3>	
							<cfloop index="n" from="3" to="#NbPage#">	
								<!--- Recuperer le lien a executer pour aller sur la page precedente--->
								<cfset TabURL = ArrayNew(1)>
								<cfset TabURL = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/\?wicket:interface=:(\\*|[0-9]{0,4}):facture:factures:datatable:topToolbars:(\\*|[0-9]{0,4}):toolbar:span:navigator:prev:[0-9]{1,4}:ILinkListener::',cfhttp.FileContent) />
								<cfset VarURL = TabURL[1]>
											
								<cfhttp method="GET" redirect="false" resolveurl="true" url="#VarURL#"																																 
										 	useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
											<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
											<cfhttpparam type='header' name='te' value='deflate;q=0'> 
											<cfhttpparam type='header' name='Connection' value='keep-alive'>
											<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
								</cfhttp>
											
								<cfif not structkeyexists(cfhttp.ResponseHeader,"Location")>			
									<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
												server="192.168.3.119" 
												port="26"  type="html" 
												subject="[Robot2 SFR Fixe]- Erreur Robot">
										
												Bonjour. <br/>
												
												Probleme de connexion a l'url :<br/>
												#VarURL#<br/>
												
												Compte = #compte#<br/>
												Login = #login#<br/>
												Mot de passe = #pwd#<br/>
												Société : #NomSociete#<br/>
												
												#cfhttp.ResponseHeader#
												
												Cordialement,
									</cfmail>
									<cfset ArrayInsertAt(TabListeLienFacture,1,-1)>
									<cfreturn TabListeLienFacture>
								</cfif>	
							 
								 <!--- Afficher la page --->					
								<cfhttp method="get" redirect="false" resolveurl="true" url="#cfhttp.ResponseHeader['Location']#"
									 	useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >							
										<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
										<cfhttpparam type='header' name='te' value='deflate;q=0'> 
										<cfhttpparam type='header' name='Connection' value='keep-alive'>
										<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
								</cfhttp>
														
								<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
									<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
												server="192.168.3.119" 
												port="26"  type="html" 
												subject="[Robot2 SFR Fixe]- Erreur Robot">
										
												Bonjour. <br/>
												
												Pas possible d'accéder à une des pages des factues (lien: page précédente)<br/>
												
												Compte = #compte#<br/>
												Login = #login#<br/>
												Mot de passe = #pwd#<br/>
												Société : #NomSociete#<br/>
												
												Cordialement,
									</cfmail>
									<cfset ArrayInsertAt(TabListeLienFacture,1,-1)>
									<cfreturn TabListeLienFacture>
								</cfif>
															
								
								<cfset TempTabListeLienFacture = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/\?wicket:interface=:[0-9]{1,4}:facture:factures:datatable:body:rows:[0-9]{1,4}:cells:[0-9]{1,4}:cell:docLinks:1:docLink:[0-9]{1,4}:ILinkListener::',cfhttp.FileContent) />
				
								<cfset TabListeLienFacture.addAll(TempTabListeLienFacture)>					
								
							</cfloop>
					  <cfelse >
					  		<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
									server="192.168.3.119" 
									port="26"  type="HTML" 
									subject="[Robot2 SFR Fixe]- Erreur Robot">
							
									Bonjour. <br/>
									
									Une erreur s'est produit lors de la récupération de nombre de pages listant les factures. <br/>
									
									Compte = #compte#<br/>
									Login = #login#<br/>
									Mot de passe = #pwd#<br/>
									Société : #NomSociete#<br/>
									
									Cordialement,
							</cfmail> 
							<cfset ArrayInsertAt(TabListeLienFacture,1,-1)>
							<cfreturn TabListeLienFacture>
				   </cfif>
				</cfif>	
				
		<cfelse>
				<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
							server="192.168.3.119" 
							port="26"  type="HTML" 
							subject="[Robot2 SFR Fixe]- Erreur Robot">
					
							Bonjour. <br/>
							
							Pas possible de récupérer l'url de bouton rechercher' (Liste des factures):<br/>
														
							Compte = #compte#<br/>
							Login = #login#<br/>
							Mot de passe = #pwd#<br/>
							
							Cordialement,
					</cfmail> 
					<cfset ArrayInsertAt(TabListeLienFacture,1,-1)>
					<cfreturn TabListeLienFacture>		
		 </cfif>	
			
		<cfreturn TabListeLienFacture>
	</cffunction>
	
	<!--- Methode pour aller telecharger le fichier --->
	<cffunction name="getFile" access="remote" returntype="string" output="false">
		<cfargument name="jsession" type="String">
		<cfargument name="ElementFacture" type="string">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="String">
		<cfargument name="NomSociete" type="String">
		 
		 
		<cfset FileName = "">    
	    <!---  Récupérer le paramètre "wicket:interface" à partir de lien de la facture  --->
	    <cfset ListeParam = ArrayNew(1)>
		<cfset ListeParam= REMatch(':[0-9]{1,4}:facture:factures:datatable:body:rows:[0-9]{1,4}:cells:[0-9]{1,4}:cell:docLinks:1:docLink:[0-9]{1,4}:ILinkListener::',#ElementFacture#)>
        <cfset ParamURL = ListeParam[1]>
	
		<!---  Récupérer l'url à partir de lien de la facture --->
		<cfset ListeURL = ArrayNew(1)>
		<cfset ListeURL = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/',#ElementFacture#)/>
		<cfset URLFacture = ListeURL[1]>	
		
		
		<!--- Récupéré le nom de fichier sans le télécharger --->
		<cfloop index="i" from="1" to="5">
			<cfhttp method="get" redirect="false" resolveurl="true" url="#URLFacture#" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="600">
					<cfhttpparam type="URL" name="wicket:interface" value="#ParamURL#">																       
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
			</cfhttp>
			<cfif structkeyexists(cfhttp.ResponseHeader,"Content-Disposition")>
				<cfbreak>
			</cfif>
		</cfloop>		
			
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Content-Disposition")>
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
							server="192.168.3.119" 
							port="26"  type="HTML" 
							subject="[Robot2 SFR Fixe]- Erreur Robot">
					
							Problème de récupération de fichier de lien suivant:<br/>
							#ElementFacture#<br/>
							Société : #NomSociete#
					
	
			</cfmail> 
			<cfset retour=#FileName#>
			<cfreturn retour>
		</cfif>
		
		<cfset u=ArrayNew(1)>		
		<cfset u=ListToArray(cfhttp.ResponseHeader["Content-Disposition"],"#chr(34)#")>
		<cfset FileName=u[2]>
		<cfoutput >Nom de fichier : #FileName#</cfoutput>
		
		<!--- Vérifier si le fichier a déjà été téléchargé --->
		<cfquery datasource="ROCOFFRE" name="qListe">
			SELECT *
			FROM file_source t
			WHERE lower(t.extranet_filename)=lower('#FileName#')
			<!--- AND t.archive_path IS NOT NULL --->
			AND t.operateurid=534
		</cfquery>
		
		
		<cfif qListe.recordcount eq 0>

			<!--- TELECHARGEMENT --->
			<cfloop index="i" from="1" to="5">
				<cfhttp method="get" redirect="false" getasbinary="yes" resolveurl="true" url="#URLFacture#" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
						<cfhttpparam type="URL" name="wicket:interface" value="#ParamURL#">																		       
						<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
						<cfhttpparam type='header' name='te' value='deflate;q=0'> 
						<cfhttpparam type='header' name='Connection' value='keep-alive'>
						<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
				</cfhttp>
				<cfif structkeyexists(cfhttp.ResponseHeader,"Content-Disposition")>
					<cfbreak>
				</cfif>
			</cfloop>		
			
			<cfif not structkeyexists(cfhttp.ResponseHeader,"Content-Disposition")>
				<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
							server="192.168.3.119" 
							port="26"  type="HTML" 
							subject="[Robot2 SFR Fixe]- Erreur Robot">
					
							Problème de récupération de fichier de lien suivant:<br/>
							#ElementFacture#<br/>
							
							Compte = #compte#<br/>
							Login = #login#<br/>
							Mot de passe = #pwd#<br/>
							Société : #NomSociete#<br/>
							Facture = #FileName#<br/>
	
				</cfmail> 
				<cfset FileName = "">
				<cfset retour=#FileName#>
				<cfreturn retour>
			</cfif>	
			
			
			
			<!--- DEPOT DU FICHIER --->
			<cfdirectory action="list" directory="#variables.repertoire#" 
			recurse="false" name="qget" filter="#compte#">
			<cfif qget.recordcount eq 0>
				<cfdirectory action="create" directory="#variables.repertoire#/#compte#" mode="777">
			</cfif>
			
			<cffile mode="777" action="write" file="#variables.repertoire#/#compte#/#FileName#" output="#cfhttp.FileContent#">
						
			<cfzip action="list" file="#variables.repertoire#/#compte#/#FileName#" name="qFile"/>
			
			<cfquery datasource="ROCOFFRE" name="qListe">
				insert into
				file_source(extranet_filename,operateurid,date_recuperation,extranet_path,etat,date_mise_a_dispo,date_etat,source_filename)
				VALUES('#FileName#',534,sysdate,'#variables.repertoire#/#compte#',1,
				to_date('#lsDateFormat(qFile.DATELASTMODIFIED,"dd/mm/yyyy")#','dd/mm/yyyy'),sysdate,'#FileName#')
			</cfquery>
				
		</cfif>
			
		<!--- Permet de revenir à la page précédente pour récupérer les autres factureet pourvoir se connecter aux autres compte--->	
		<!--- Lien: Export facture CSV --->
		<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type='formfield' name='code_service' value='1368999'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">					
		</cfhttp>

		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						Société : #NomSociete#<br/>
						
						Cordialement,
			</cfmail>  
			<cfreturn #FileName# >
		</cfif>
		
		<!--- Redirection vers la page suivante --->	
		<cfhttp method="GET" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr/extranet/cvg/?wicket:bookmarkablePage=:com.sfr.facture.EntiteOrganiquePage&typeAffichage=RAPPORT&1308664880100" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
				<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot2 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/cvg/?wicket:bookmarkablePage=:com.sfr.facture.EntiteOrganiquePage&typeAffichage=RAPPORT&1308664880100<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						Société : #NomSociete#<br/>
						
						Cordialement,
			</cfmail> 
			<cfreturn #FileName# >
		</cfif>
		
		
		<cfreturn #FileName#>
	</cffunction>
</cfcomponent>