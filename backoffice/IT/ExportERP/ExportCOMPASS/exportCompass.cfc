<cfcomponent name="exportCompass" displayname="exportCompass">
	<!--- <cfset biServer="http://xmlpserver-3.consotel.fr/xmlpserver/services/PublicReportService?wsdl"> --->
	<!--- <cfset biServer="http://bip-int.consotel.fr/xmlpserver/services/PublicReportService?wsdl"> --->

	<!--- Ram�ne la liste des factures qui sont marqu�es bonnes pour export ERP --->
	<cffunction name="main" access="remote" returntype="void" output="true">
		<cfset thread = CreateObject("java", "java.lang.Thread")>
		<!--- Array pour le traitement des fichiers burst --->
		<cfset stJustif=StructNew()>
		<cfset compteur=1>
		<!--- Initialiser le nom dossier --->
		<cfset path = "/pelican/services/ftp/export/106539/CONSOTEL" >


		
		<!--- On récupère la liste des facture --->
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_CV_REP_V3.Liste_facture_a_exporter_2">
			<cfprocparam value="106539" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetFactures">
		</cfstoredproc>

		<cfdump var="#qGetFactures#" label="Avant la boucle">


		<!--- <Si la liste des factures n'est pas vide> --->
		<cfif qGetFactures.recordcount gt 0>
			
			<cfset countFact=0>
			<!--- <Nombre de factures à traiter> --->
			<cfoutput>
			Nombre de factures : #qGetFactures.recordcount#<br />
			</cfoutput>
			
		
			<cfloop query="qGetFactures">
				
				<cfoutput>Boucle numéro : #compteur#</cfoutput>
				<!--- <Affichier la facture à traiter> --->
				<cfoutput> Facture en cours de traitement:  #qGetFactures.numero_facture# <br /></cfoutput> 
				<cfflush interval="20">
				
				<!--- Initialiser le nom de fichier --->
				<cfset filename = "#qGetFactures.nom#_#qGetFactures.compte_facturation#_#qGetFactures.numero_facture#_#qGetFactures.date_emission_origine#.csv">
				<cfoutput >idperiode_mois = #qGetFactures.idinventaire_periode# <br /></cfoutput>
		  		<cfoutput >#path#/#filename#</cfoutput>			
				<!--- <Appel de la proc�dure qui génère le rapport> --->
				<cfstoredproc datasource="ROCOFFRE" procedure="PKG_CV_REP_V3.EXPORT_COMPASS">
					 <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#qGetFactures.idinventaire_periode#">
                     <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="106539">
	                 <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#qGetFactures.idperiode_mois#">
                 	 <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#path#">
               		 <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#filename#">
					<cfprocparam type="out" variable="p_result" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
 				<cfif #p_result# eq 1>
							<!--- <cfabort showerror="fin"> --->
					<cfoutput>MAJ Base pour la facture #qGetFactures.numero_facture# <br /></cfoutput>
					 <cfstoredproc datasource="ROCOFFRE" procedure="PKG_CV_REP_V3.switch_to_exportee">
						<cfprocparam value="#qGetFactures.idinventaire_periode#" type="in" cfsqltype="CF_SQL_INTEGER">
						<cfprocparam type="out" variable="t" cfsqltype="CF_SQL_INTEGER">
					</cfstoredproc>
				<cfelse >
					<cfoutput>Erreur lors de la g�n�ration de la facture #qGetFactures.numero_facture# <br /></cfoutput>
					<cfmail from="monitoring@saaswedo.com"
							to="djalel.meftouh@saaswedo.com" 
							cc="monitoring@saaswedo.com" 
							subject="[Erreur Export ERP] - COMPASS"
							server="mail-cv.consotel.fr" port="25" type="html">
							Erreur lors de la g�n�ration de la facture #qGetFactures.numero_facture#

					</cfmail>
				 </cfif>
				
			</cfloop>
			
		<cfelse> 
			<cfoutput >Pas de facture � exporter</cfoutput>	
			
		</cfif>
		
		 <cfoutput >Fin OK</cfoutput>	
	</cffunction>
	
</cfcomponent>