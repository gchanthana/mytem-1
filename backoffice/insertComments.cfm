﻿<!--- 

insertComments

Parcours tous les package des modules de consoview
Met à jour la description de la fonction dans log_fonction s'il y en apas
Insert une nouvelle entrée dans log fonction si la fonction n'est pas présente dans log_fonction

Ne prend en compte que les fonction public et remote.

Samuel DIVIOKA
01/06/2011

 --->
<cfflush interval="100">
<cfset currentPath = GetDirectoryFromPath(GetCurrentTemplatePath())/>

<cfset filobj=createObject("component","cfcfoc.components.FileSystemObject" )/>
<cfset filobj.init(currentPath & 'fr/consotel/consoview/M311','/' )/>
<cfset result=filobj.list(true,'*.cfc','fr.consotel.consoview.M311')/>

 
<!--- MODULE --->

<cfset module_name = ''>
<cfset code_module = ''>
<cfset old_code_module = ''/>
<!--- CFC --->
<cfset aCFCfile = ''>
<cfset cfcFullName = ''>
<cfset methodeName = ''>
<cfset desc = ''>

<cfset fileContents = ''>

<cfdump var="#result#">
 
 

<cfloop query="result">
	<cfset cfcFullName =  replace(result.PACKAGE & '.' & result.NAME,'.cfc','','all' )/>	
	<cfset aCFCfile = result.DIRECTORY & '/' & result.NAME>
	<cfoutput>#cfcFullName#</br></cfoutput>
	<cfoutput>#aCFCfile#</br></cfoutput>
	
	
	
	<cftry>
		<cffile action="read" file="#aCFCfile#"  variable="fileContents">
		<cfcatch type="any">
			<cfdump var="#cfcatch.Detail#" />
		</cfcatch>
	</cftry>
	
	<cfset obj=createObject("component","cfcfoc.components.CFCParser" ).init()/>
	<cfset cfcInfos=obj.parse(fileContents)/>
	 
	<cfset arrData = REMatch("M[0-9]{1,3}",cfcFullName) />
	<cfif arrayLen(arrData) gt 0>
		<cfset code_module = arrData[1]/>	
		
		<cfif compareNocase(code_module,old_code_module) neq 0>
			<cfset old_code_module = code_module>
			
			<cfquery datasource="ROCOFFRE" name="qModule" result="t">	
				select * from module_consotel where upper(code_module) like upper('#code_module#')
			</cfquery>
			
			<cfif qModule.recordCount gt 0>
				<cfset module_name = qModule.NOM_MODULE>
			</cfif>		
			<cfdump var="#qModule#">
		</cfif>
		<!--- Faire une fonction pour mettre à jour les logs --->
			<cfloop collection="#cfcInfos.METHODS#" item="theMethode">
				<cfset methodeName = theMethode>
				<cfset desc = cfcInfos.METHODS['#theMethode#'].HINT>
				<cfset theAccess = cfcInfos.METHODS['#theMethode#'].ACCESS>
				
				<cfdump var="#theAccess#">
				<cfif compareNoCase(theAccess,'private') neq 0>
				
				</br></br><cfoutput>#code_module# - <b>#theAccess#</b> - #methodeName# - #desc#</cfoutput></br>
				
				<cfquery datasource="ROCOFFRE" name="qCFC">	
					select * from log_fonction lf where lf.classe_log like '#trim(cfcFullName)#' and lf.fonction like '#trim(methodeName)#'
				</cfquery>
				
				<cfif qCFC.recordCount  gt 0>
					<cfif qCFC.SOUS_FONCTION  eq "">
						<cfoutput>c'est vide</cfoutput>
						<!---
						<cfquery name="qInsertLogEvent" datasource="ROCOFFRE" >
							update LOG_FONCTION
							set SOUS_FONCTION = '#left(desc,100)#'
							where CLASSE_LOG like '#trim(cfcFullName)#' and FONCTION like '#trim(cfcFullName)#'
						</cfquery>--->
						
					<cfelse>
						<cfoutput>c'est pas vide</cfoutput>
					</cfif>
				
					<cfelse>
						<cfoutput>Pas d'entrée on insert dans la table log_fonction on insert les valeurs suivante :<br></cfoutput>
						<cfoutput>#cfcFullName# </br> #code_module# </br> #methodeName#</br>#module_name#</br>#desc#</br>#code_module#<br></cfoutput>
						
					<!---	<cfquery datasource="ROCOFFRE" name="qInsertLogFunction">	
							insert into  log_fonction(CLASSE_LOG,CODE_MODULE,FONCTION,NOM_MODULE,SOUS_FONCTION,UNIVERS) values('#trim(cfcFullName)#','#trim(code_module)#','#trim(methodeName)#','#trim(module_name)#','#left(desc,100)#','#trim(code_module)#')
						</cfquery>--->
						
						</br>
				</cfif>
				<cfdump var="#qCFC#">
			</cfif>
		</cfloop>
	</cfif>	
</cfloop>