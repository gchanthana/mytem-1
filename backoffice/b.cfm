<cftry>
	<cfquery name="testQuery" datasource="ROCOFFRE">
		select 1 from dual
	</cfquery>
	
	<cfquery datasource="BI_TEST" name="qQuery">
		set variable 	p_idracine_master=310812,
						p_idracine=310812,
						p_idorga=310812,
						p_idperimetre=310812,
						p_niveau='A',
						p_idniveau=0,
						p_langue_pays='fr_FR';
	
		SELECT ORGAOPERATEUR.OPERATEUR saw_0, 
					FACTURATION.NOMBRE_FACTURES saw_1, 
					PERIODE.DESC_MOIS saw_2, 
					PERIODE.SHORT_MOIS saw_3, 
					PERIODE.DESC_ANNEE saw_4, 
					PERIODE.IDPERIODE_MOIS saw_5
		FROM E0_AGGREGS_DEFRAG
		WHERE RCOUNT(PERIODE.IDPERIODE_MOIS) <= 4
		ORDER BY saw_5 DESC
	</cfquery> 
	
	<cfif testQuery.recordCount eq 1 and qQuery.RecordCount gte 1>
		<cfoutput>#ucase('ok')#</cfoutput>
		<cfelse>
			<cfoutput>#ucase('test query failed')# 	backoffice #CGI.SERVER_NAME#<br></cfoutput>
			<cfmail from="sonde@saaswedo.com" subject="[SONDE] - backoffice down#CGI.SERVER_NAME#" to="sysadmin@saaswedo.com" >
				backoffice #CGI.SERVER_NAME#<br>
			</cfmail>
	</cfif>
	
	<cfcatch type="any">
		<cfmail from="sonde@saaswedo.com" subject="[SONDE] - backoffice down #CGI.SERVER_NAME#" to="sysadmin@saaswedo.com" >
			Error : <P>#cfcatch.message#</P>
			<cfdump var="#CFCATCH#"><br>
			backoffice #CGI.SERVER_NAME#<br>
		</cfmail>
		<cfoutput>Error : <P>#cfcatch.message#</P>	backoffice #CGI.SERVER_NAME#<br></cfoutput>
		<cfdump var="#CFCATCH#"><br>
	</cfcatch>
</cftry>