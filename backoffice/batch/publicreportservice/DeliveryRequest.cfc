<cfcomponent name="DeliveryRequest" displayname="DeliveryRequest" output="true">
	<cfset variables.DeliveryRequest=structNew()>
	
	<cffunction name="setEmailDeliveryRequest" access="public" returntype="void">
		<cfargument name="emailBCC" type="String" required="false" default="">
		<cfargument name="emailBody" type="String" required="false" default="">
		<cfargument name="emailCC" type="String" required="false" default="">
		<cfargument name="emailFrom" type="String" required="true" default="">
		<cfargument name="emailReplyTo" type="String" required="false" default="">
		<cfargument name="emailSubject" type="String" required="true" default="">
		<cfargument name="emailTo" type="String" required="true" default="">
		
		<cfobject name="emailDelivery" type="component" component="batchcommande.publicreportservice.EmailDeliveryOption">
		<cfset emailDelivery.setEmailDeliveryOption(emailBCC,emailBody,emailCC,emailFrom,emailReplyTo,emailSubject,emailTo)>
		<cfset setDeliveryRequest("email",emailDelivery.getEmailDeliveryOption())>
	</cffunction>
	
	<cffunction name="setFaxDeliveryRequest" access="public" returntype="void">
		<cfargument name="faxNumber" type="String" required="false" default="">
		<cfargument name="faxServer" type="String" required="false" default="">
		
		<cfobject name="faxDelivery" type="component" component="batchcommande.publicreportservice.FaxDeliveryOption">
		<cfset faxDelivery.setFaxDeliveryOption(faxNumber,faxServer)>
		<cfset setDeliveryRequest("fax",faxDelivery.getFaxDeliveryOption())>
	</cffunction>
	
	<cffunction name="setFtpDeliveryRequest" access="public" returntype="void">
		<cfargument name="ftpServerName" type="String" required="true">
		<cfargument name="ftpUserName" type="String" required="true">
		<cfargument name="ftpUserPassword" type="String" required="true">
		<cfargument name="remoteFile" type="String" required="true">
		<cfargument name="sftpOption" type="boolean" required="false" default="false">
		
		<cfobject name="ftpDelivery" type="component" component="batchcommande.publicreportservice.FtpDeliveryOption">
		<cfset ftpDelivery.setFtpDeliveryOption(ftpServerName,ftpUserName,ftpUserPassword,remoteFile,sftpOption)>
		<cfset setDeliveryRequest("ftp",ftpDelivery.getFtpDeliveryOption())>
	</cffunction>
	
	<cffunction name="setLocalDeliveryRequest" access="public" returntype="void">
		<cfargument name="destination" type="String" required="true">
		
		<cfobject name="localDelivery" type="component" component="batchcommande.publicreportservice.LocalDeliveryOption">
		<cfset localDelivery.setLocalDeliveryOption(destination)>
		<cfset setDeliveryRequest("local",localDelivery.getLocalDeliveryOption())>
	</cffunction>
	
	<cffunction name="setDeliveryRequest" access="private" returntype="void">
		<cfargument name="Type" type="String" required="true" default="">
		<cfargument name="option" type="struct" required="false" default="">
		<cfswitch expression="#type#">
			<cfcase value="email">
				<cfset structInsert(variables.DeliveryRequest,"emailOption",option,true)>
			</cfcase>
			<cfcase value="fax">
				<cfset structInsert(variables.DeliveryRequest,"faxOption",option,true)>
			</cfcase>
			<cfcase value="ftp">
				<cfset structInsert(variables.DeliveryRequest,"ftpOption",option,true)>
			</cfcase>
			<cfcase value="local">
				<cfset structInsert(variables.DeliveryRequest,"localOption",option,true)>
			</cfcase>
		</cfswitch>
	</cffunction>
	
	<cffunction name="getDeliveryRequest" access="package" returntype="struct">
		<cfreturn variables.DeliveryRequest>
	</cffunction>
</cfcomponent>