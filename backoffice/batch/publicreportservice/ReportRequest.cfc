<cfcomponent name="ReportRequest" displayname="ReportRequest">
	<cfset variables.ReportRequest=structNew()>
	<cfset variables.ArrayOfParamNameValues=ArrayNew(1)>
	
	<cffunction name="setReportRequest" access="public" returntype="void">
		<cfargument name="reportAbsolutePath" type="String" required="true">
		<cfargument name="attributeTemplate" type="String" required="true">
		<cfargument name="attributeFormat" type="String" required="true">
		<cfargument name="attributeLocale" type="String" required="false" default="fr-FR">
		
		<cfset variables.ReportRequest.reportAbsolutePath=reportAbsolutePath>
		<cfset variables.ReportRequest.attributeTemplate=attributeTemplate>
		<cfset variables.ReportRequest.attributeFormat=attributeFormat>
		<cfset variables.ReportRequest.attributeLocale=attributeLocale>
		<cfset variables.ReportRequest.sizeOfDataChunkDownload=-1>
		<cfset variables.ReportRequest.parameterNameValues=variables.ArrayOfParamNameValues>
	</cffunction>
	
	<cffunction name="AddParameter" access="remote" returntype="void">
		<cfargument name="paramName" type="string" required="true">
		<cfargument name="paramValues" type="string" required="true">
		<cfset ParamNameValue=structNew()>
		<cfif ArrayLen(ListToArray(paramValues)) gt 1>
			<cfset ParamNameValue.multiValuesAllowed=true>
		<cfelse>
			<cfset ParamNameValue.multiValuesAllowed=false>
		</cfif>
		<cfset ParamNameValue.name=paramName>
		<cfset t=ArrayNew(1)>
		<cfset v=ArrayNew(1)>
		<cfset v=ListToArray(paramValues)>
		<cfloop from="1" to="#ArrayLen(v)#" index="i">
			<cfset t[i]=v[i]>
		</cfloop>
		<cfset ParamNameValue.values=t>		
		<cfset ArrayOfParamNameValues[ArrayLen(ArrayOfParamNameValues)+1]=ParamNameValue>
		<cfset variables.ReportRequest.parameterNameValues=variables.ArrayOfParamNameValues>
	</cffunction>
	
	<cffunction name="getReportRequest" access="package" returntype="struct">
		<cfreturn variables.ReportRequest>
	</cffunction>
</cfcomponent>
	