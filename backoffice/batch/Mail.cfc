<cfcomponent output="false" alias="batchcommande.Mail">
	
	<cfproperty name="expediteur" type="string" default="">
	<cfproperty name="destinataire" type="string" default="">
	<cfproperty name="sujet" type="string" default="">
	<cfproperty name="module" type="string" default="">
	<cfproperty name="type" type="string" default="html">
	<cfproperty name="repondreA" type="string" default="">
	<cfproperty name="cc" type="string" default="">
	<cfproperty name="bcc" type="string" default="">
	<cfproperty name="charset" type="string" default="utf-8">
	<cfproperty name="message" type="string" default="">
	<cfproperty name="copiePourExpediteur" type="string" default="YES">
	<cfproperty name="copiePourOperateur" type="string" default="YES">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.expediteur = "";
		variables.destinataire = "";
		variables.sujet = "";
		variables.module = "";
		variables.type = "html";
		variables.repondreA = "";
		variables.cc = "";
		variables.bcc = "";
		variables.charset = "utf-8";
		variables.message = "";
		variables.copiePourExpediteur = "YES";
		variables.copiePourOperateur  = "YES";
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 23/10/2007
		
		Description : GETTER et SETTER
	
	--->
	<cffunction name="getExpediteur" output="false" access="public" returntype="any">
		<cfreturn variables.expediteur>
	</cffunction>

	<cffunction name="setExpediteur" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.expediteur = arguments.val>
	</cffunction>
	
	<cffunction name="getCopiePourOperateur" output="false" access="public" returntype="any">
		<cfreturn variables.copiePourOperateur>
	</cffunction>

	<cffunction name="setCopiePourOperateur" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.copiePourOperateur = arguments.val>
	</cffunction>
		
	<cffunction name="getDestinataire" output="false" access="public" returntype="any">
		<cfreturn variables.destinataire>
	</cffunction>

	<cffunction name="setDestinataire" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.destinataire = arguments.val>
	</cffunction>

	<cffunction name="getSujet" output="false" access="public" returntype="any">
		<cfreturn variables.sujet>
	</cffunction>

	<cffunction name="setSujet" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.sujet = arguments.val>
	</cffunction>

	<cffunction name="getModule" output="false" access="public" returntype="any">
		<cfreturn variables.module>
	</cffunction>

	<cffunction name="setModule" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.module = arguments.val>
	</cffunction>

	<cffunction name="getType" output="false" access="public" returntype="any">
		<cfreturn variables.type>
	</cffunction>

	<cffunction name="setType" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.type = arguments.val>
	</cffunction>

	<cffunction name="getRepondreA" output="false" access="public" returntype="any">
		<cfreturn variables.repondreA>
	</cffunction>

	<cffunction name="setRepondreA" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.repondreA = arguments.val>
	</cffunction>

	<cffunction name="getCc" output="false" access="public" returntype="any">
		<cfreturn variables.cc>
	</cffunction>

	<cffunction name="setCc" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.cc = arguments.val>
	</cffunction>

	<cffunction name="getBcc" output="false" access="public" returntype="any">
		<cfreturn variables.bcc>
	</cffunction>

	<cffunction name="setBcc" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.bcc= arguments.val>
	</cffunction>

	<cffunction name="getCharset" output="false" access="public" returntype="any">
		<cfreturn variables.charset>
	</cffunction>

	<cffunction name="setCharset" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.charset= arguments.val>
	</cffunction>

	<cffunction name="getMessage" output="false" access="public" returntype="any">
		<cfreturn variables.message>
	</cffunction>

	<cffunction name="setMessage" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.message= arguments.val>
	</cffunction>	
	
	<cffunction name="getCopiePourExpediteur" output="false" access="public" returntype="any">
		<cfreturn variables.copiePourExpediteur>
	</cffunction>

	<cffunction name="setCopiePourExpediteur" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.copiePourExpediteur = arguments.val>
	</cffunction>
</cfcomponent>