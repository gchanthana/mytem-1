<cfcomponent displayname="ListQueryErorMail">
	
	<cffunction name="getData" returntype="query" access="remote">
		<cfargument name="x_minute_ago"		required="true" type="numeric" default="1"/>
		
		<cfset _x_minute_ago = abs(val(x_minute_ago))>		
		
		<cfif _x_minute_ago eq 0>
			
			<cfset _x_minute_ago = 1>
			
		</cfif>
		
		<cfquery name="qCommandes" datasource="ROCOFFRE">	
			SELECT DISTINCT CMDE.OPEURATEURID,ITO.TYPE_OPERATION,CMDE.IDTYPE_OPERATION,CMDE.NUMERO_OPERATION,CMDE.REF_CLIENT,
			OP.NOM OPERATEUR,
			ITO.TYPE_OPERATION,
			CVL.IDGESTIONNAIRE,
			CVL.RACINE,
			CVL.GESTIONNAIRE,
			CVL.MAIL,
			CVL.IDCOMMANDE,
			CVL.MAIL_FROM,
			CVL.MAIL_TO,
			CVL.CC,
			CVL.BCC,
			CVL.MESSAGE,
			CVL.MODULE,
			CVL.SUBJECT,
			CVL.STATUS,
			CVL.JOB_ID,
			CVL.DATE_LOG,
			CVL.IDRACINE,
			CVL.IDCV_LOG_MAIL
			FROM CV_LOG_MAIL CVL, COMMANDE CMDE, INV_TYPE_OPE ITO,OPERATEUR OP 
			WHERE trunc(cvl.date_log,'mm') = trunc(sysdate,'mm') AND CVL.DATE_LOG < sysdate - (#_x_minute_ago#/1440)
			AND CVL.IDCOMMANDE = CMDE.IDCOMMANDE
			AND CMDE.IDTYPE_OPERATION = ITO.IDINV_TYPE_OPE
			AND CMDE.IDRACINE <> 2458788
			AND upper(CVL.STATUS) LIKE upper('n.c.')
			AND CVL.JOB_ID = 1
			AND OP.OPERATEURID = CMDE.OPEURATEURID
			GROUP BY 
			CMDE.OPEURATEURID,ITO.TYPE_OPERATION,CMDE.IDTYPE_OPERATION,CMDE.NUMERO_OPERATION,CMDE.REF_CLIENT,
			OP.NOM,
			ITO.TYPE_OPERATION,
			CVL.IDGESTIONNAIRE,
			CVL.RACINE,
			CVL.GESTIONNAIRE,
			CVL.MAIL,
			CVL.IDCOMMANDE,
			CVL.MAIL_FROM,
			CVL.MAIL_TO,
			CVL.CC,
			CVL.BCC,
			CVL.MESSAGE,
			CVL.MODULE,
			CVL.SUBJECT,
			CVL.STATUS,
			CVL.JOB_ID,
			CVL.DATE_LOG,
			CVL.IDCV_LOG_MAIL,
			CVL.IDRACINE
			ORDER BY DATE_LOG DESC
		</cfquery>
		
	<cfreturn qCommandes>
</cffunction>

	<cffunction name="updateData" output="false" access="remote"  returntype="numeric" >
		<cfargument name="idcvlogmail" required="true" type="numeric"/>
		<cfargument name="newJobID" required="false" type="numeric"/>
		
			<cftry>
				
				<cfquery name="qInsertLogEvent" datasource="ROCOFFRE" >
					UPDATE cv_log_mail c
					SET status = <cfqueryparam cfsqltype="cf_sql_varchar" value="R">
					WHERE idcv_log_mail = <cfqueryparam cfsqltype="cf_sql_integer" value="#idcvlogmail#">
				</cfquery>
				
		    	<cfcatch type="database">
		    		
		    		<cfmail type="text/html"  subject="[LOG CV-DEV] udapte log mail cmde"server="mail.consotel.fr" port="26" from="cv-dev@consotel.fr" to="samuel.divioka@consotel.fr">
		    			Erreur lors de la mis à jour d'un log de commande
		    			idcv_log_mail = #idcvlogmail#<br>
						<cfdump var="#cfcatch.Detail#">
					</cfmail>
					
					<cfreturn -1>
						
		    	</cfcatch>
				
			</cftry>
		
		<cfreturn 1>
	</cffunction>
	
</cfcomponent>

<!--- 
		
<cfquery name="qCommandes" datasource="ROCOFFRE">	
		SELECT GESTIONNAIRE ,RACINE ,IDGESTIONNAIRE ,IDCOMMANDE,MAIL_FROM,MAIL_TO,CC,BCC,MESSAGE,SUBJECT,IDRACINE,min(IDCV_LOG_MAIL) as IDCV_LOG_MAIL FROM cv_log_mail c 
		WHERE to_char(c.date_log,'yyyy/mm/dd hh24:mi:ss')>='2011/10/04 17:23:00' 
	      AND c.status<>'M' AND c.status<>'R'
	      AND c.job_id = 1
	      AND c.idracine <> 2458788
	     and c.idracine <> 477
        GROUP BY GESTIONNAIRE ,RACINE ,IDGESTIONNAIRE ,IDCOMMANDE,MAIL_FROM,MAIL_TO,CC,BCC,MESSAGE,SUBJECT,IDRACINE
</cfquery> 
		
--->