<cfcomponent displayname="ListQuery">
	
	<cffunction name="getData" returntype="query" access="remote">
		
			<!--- 
			<CFQUERY name="qCommandes" datasource="ROCOFFRE">
				SELECT DISTINCT ioa.date_action AS date_envoi, ia.*,ap.login_email AS mail_exp,ap.login_nom ||' '|| ap.login_prenom AS expediteur, c.email AS mail_dest,c.nom ||' '||c.prenom AS destinat ,gc.libelle_groupe_client,a.* FROM
				(SELECT * 
				FROM commande cmd
				MINUS
				SELECT * 
				FROM commande cmd 
				WHERE cmd.idcommande IN (SELECT l.idcommande FROM cv_log_mail l)) a,
					groupe_client gc,
				   cde_contact c,
				   app_login ap,
				   inv_actions ia,
				   inv_operation_action ioa       
				WHERE to_char(a.date_create,'yyyy/mm/dd')>'2010/08/01' 
				AND a.idinv_etat IN (3067,3068,3069,3070,3071,3072,3073,3074,3075,3076)
				AND a.idracine=gc.idgroupe_client
				AND a.idcde_contact = c.idcde_contact
				AND a.idcommande = ioa.idcommande
				AND ioa.userid = ap.app_loginid
				AND ia.idinv_etat = a.idinv_etat
				AND ia.idinv_actions = ioa.idinv_actions
				ORDER BY a.idcommande
			</CFQUERY> 
			 --->
			 
			 <CFQUERY name="qCommandes" datasource="ROCOFFRE">
				SELECT DISTINCT ioa.date_action AS date_envoi, ia.*,ap.login_email AS mail_exp,ap.login_nom ||' '|| ap.login_prenom AS expediteur, c.email AS mail_dest,c.nom ||' '||c.prenom AS destinat ,gc.libelle_groupe_client,a.* FROM
				(SELECT * 
				FROM commande cmd
				MINUS
				SELECT * 
				FROM commande cmd 
				WHERE cmd.idcommande IN (SELECT l.idcommande FROM cv_log_mail l)) a,
					groupe_client gc,
				   cde_contact c,
				   app_login ap,
				   inv_actions ia,
				   inv_operation_action ioa       
				WHERE to_char(a.date_create,'yyyy/mm/dd')>'2010/08/01' 
				AND a.idinv_etat IN (3067,3068,3069,3070,3071,3072,3073,3074,3075,3076)
				AND a.idracine=gc.idgroupe_client
				AND a.idcde_contact = c.idcde_contact
				AND a.idcommande = ioa.idcommande
				AND ioa.userid = ap.app_loginid
				AND ia.idinv_etat = a.idinv_etat
				AND ia.idinv_actions = ioa.idinv_actions
				ORDER BY a.idcommande
			</CFQUERY> 
		
		<cfreturn qCommandes>
	</cffunction>
	
</cfcomponent>


