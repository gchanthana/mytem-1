<!--- Diffusion d'une liste de rapport (Diffusés par mail avec BIP au format HTML et en disposition INLINE) --->
<cfsetting enablecfoutputonly="TRUE" requesttimeout="3600">
<cfset logUUID=createUUID()>
<cfset execute(logUUID)>

<!--- Fonctions --->
<cffunction access="private" name="execute" returntype="Numeric" hint="Exécution de la diffusion de rapports">
	<cfargument name="logUUID" type="String" required="true" hint="UUID du stockage des logs (Doit etre un UUID ColdFusion)">
	<cfset var httpCode=200>
	<cfset var responseMsg="">
	<cfset var returnCode=0>
	<cfset var logType="information">
	<!--- UUID commun pour les logs concernant cette diffusion --->
	<cfset var logStorageUUID=ARGUMENTS.logUUID>
	<cfset var logStorage=new fr.saaswedo.utils.log.LogStorage()>
	<cfset var reportList="">
	<cfset var isReportListDefined=FALSE>
	<cftry>
		<cfif compareNoCase(CGI.REQUEST_METHOD,"GET") EQ 0>
			<cfif isDefined("URL")>
				<cfset var urlParams=DUPLICATE(URL)>
				<!--- Affiche le nom du backoffice suivi de son port d'écoute --->
				<cfif structIsEmpty(urlParams)>
					<cfset httpCode=200>
					<cfset responseMsg="BackOffice " & CGI.SERVER_NAME & ":" & CGI.SERVER_PORT>
				<!--- Exécute la diffusion des rapport d'une liste --->
				<cfelse>
					<cfif NOT structKeyExists(urlParams,"TYPE_MAIL")>
						<cfset httpCode=400>
						<cfset responseMsg="Missing TYPE_MAIL parameter">
					<cfelse>
						<!--- Identifiant de la liste de rapport --->
						<cfset var reportListId=VAL(URL.TYPE_MAIL)>
						<!--- Mode et Liste des rapports : Le log UUID est aussi fourni à la procédure stockée qui ramène la liste de rapports --->
						<cfset var currentMode=(structKeyExists(urlParams,"MODE") AND isValidMode(urlParams.MODE)) ? urlParams.MODE:DEFAULT_MODE()>
						<cfset reportList=loadReportList(urlParams,logStorageUUID)>
						<cfset isReportListDefined=TRUE>
						<!--- Paramètres du rapport --->
						<cfset var reportParamNames=extractReportParamNames(reportList.columnList)>
						<cfset var paramCount=listLen(reportParamNames,",",FALSE)>
						<!--- Les rapports sont planifiés si aucune erreur de traitement n'a eu lieu jusque là --->
						<!--- Librairie de remoting --->
						<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
						<!--- Stockage de la notification de diffusion d'une liste de rapport --->
						<cfset var contextParameters={
							REL_MSG="[CaseId: 0] Exécution d'une liste de diffusion (Lancée le #NOW()#)",
							REPORT_LIST=reportList,REPORT_PARAM_NAMES=reportParamNames
						}>
						<cfset storeReportListNotification(0,logStorage,logStorageUUID,urlParams,contextParameters)>
						<!--- Diffusion de la liste de rapport --->
						<cfset var idDiffusion=-1>
						<cfloop index="i" from="1" to="#reportList.recordcount#">
							<cfset idDiffusion=VAL(reportList["ID"][i])>
							<cftry>
								<!--- Le format sera toujours HTML pour cette implémentation --->
								<cfset var scheduleRequest={
									userJobName=getJobName(logStorageUUID,reportListId,idDiffusion,currentMode),
									useUTF8Option=TRUE,saveDataOption=FALSE,saveOutputOption=FALSE,
									scheduleBurstringOption=FALSE,notificationTo="",notifyWhenFailed=TRUE,notifyWhenWarning=TRUE,notifyWhenSuccess=TRUE,
									jobLocale=DEFAULT_LOCALE(),schedulePublicOption=FALSE,jobTZ=DEFAULT_TZ(),
									httpNotificationServer=getHttpDestination(currentMode),
									reportRequest={
										reportAbsolutePath=reportList["DOSSIER"][i] & "/" & reportList["XDO"][i] & "/" & reportList["XDO"][i] & ".xdo",
										attributeFormat="html",attributeLocale=DEFAULT_LOCALE()
									},
									deliveryRequest={
										emailOption={
											emailServerName=DEFAULT_MAIL_SERVER(),emailFrom=reportList["EMAIL_FROM"][i],
											emailTo=reportList["EMAIL_TO"][i],emailSubject=reportList["EMAIL_SUBJECT"][i]
										}
									}
								}>
								<!--- Autres propriétés de diffusion mail --->
								<cfif structKeyExists(reportList,"EMAIL_SERVER") AND (LEN(TRIM(reportList["EMAIL_SERVER"][i])) GT 0)>
									<cfset scheduleRequest.deliveryRequest.emailOption.emailServerName=reportList["EMAIL_SERVER"][i]>
								</cfif>
								<cfif structKeyExists(reportList,"EMAIL_CC") AND (LEN(TRIM(reportList["EMAIL_CC"][i])) GT 0)>
									<cfset scheduleRequest.deliveryRequest.emailOption.emailCC=reportList["EMAIL_CC"][i]>
								</cfif>
								<cfif structKeyExists(reportList,"BCC") AND (LEN(TRIM(reportList["BCC"][i])) GT 0)>
									<cfset scheduleRequest.deliveryRequest.emailOption.emailBCC=reportList["BCC"][i]>
								</cfif>
								<!--- Autres propriétés d'exécution --->
								<cfif structKeyExists(reportList,"TEMPLATE") AND (LEN(TRIM(reportList["TEMPLATE"][i])) GT 0)>
									<cfset scheduleRequest.reportRequest.attributeTemplate=reportList["TEMPLATE"][i]>
								</cfif>
								<cfif structKeyExists(reportList,"LOCALISATION") AND (LEN(TRIM(reportList["LOCALISATION"][i])) GT 0)>
									<cfset scheduleRequest.jobLocale=reportList["LOCALISATION"][i]>
									<cfset scheduleRequest.reportRequest.attributeLocale=reportList["LOCALISATION"][i]>
								</cfif>
								<cfif structKeyExists(reportList,"REPORT_TZ") AND (LEN(TRIM(reportList["REPORT_TZ"][i])) GT 0)>
									<cfset scheduleRequest.jobTZ=reportList["REPORT_TZ"][i]>
								</cfif>
								<!--- Paramètres du rapport --->
								<cfset var reportParameters=[]>
								<cfloop index="j" from="1" to="#paramCount#">
									<cfset var paramName=listGetAt(reportParamNames,j,",",FALSE)>
									<cfset var paramValues=listToArray(reportList[paramName][i],",",FALSE,FALSE) >
									<cfset var param={name=paramName,values=paramValues,multiValuesAllowed=(arrayLen(paramValues) GT 0)}>
									<cfset arrayAppend(reportParameters,param)>
								</cfloop>
								<!--- Planification du rapport dans BIP --->
								<cfset scheduleRequest.reportRequest.parameterNameValues=reportParameters>
								<cfset var scheduleResult=scheduleReport(remoting,scheduleRequest,mode)>
								<!--- Traitement des cas d'erreurs lors de la planification d'un rapport --->
								<cfif scheduleResult[remoting.STATUS()] NEQ remoting.DEFINED()>
									<!--- Notification d'erreur dans le résultat de la planification d'un rapport --->
									<cfset var contextParameters={
										ID=idDiffusion,REPORT_LIST=reportList,REMOTING_RESULT=scheduleResult,
										REL_MSG="[CaseId: -4] Erreur dans le résultat de la planification du rapport #idDiffusion# (Index: #i#)"
									}>
									<cfset storeReportListNotification(-4,logStorage,logStorageUUID,urlParams,contextParameters)>
								</cfif>
								<cfcatch type="Any">
									<!--- Notification d'erreur durant la planification d'un rapport --->
									<cfset var contextParameters={
										ID=idDiffusion,REPORT_LIST=reportList,ERROR=CFCATCH,
										REL_MSG="[CaseId: -3] Erreur durant la planification du rapport #idDiffusion# (Index dans la liste: #i#)"
									}>
									<!--- La variable scheduleResult peut etre indéfinie en cas d'exception lancée par scheduleReport() --->
									<cfset storeReportListNotification(-3,logStorage,logStorageUUID,urlParams,contextParameters)>
								</cfcatch>
							</cftry>
						</cfloop>
						<cfset httpCode=200>
						<cfset responseMsg="Diffusion " & URL.TYPE_MAIL & " with MODE " & currentMode & " successfully executed">
					</cfif>
				</cfif>
			<cfelse>
				<cfset httpCode=400>
				<cfset responseMsg="Missing parameters (HTTP GET)">
			</cfif>
		<cfelse>
			<cfset httpCode=400>
			<cfset responseMsg="Invalid HTTP METHOD " & CGI.REQUEST_METHOD>
		</cfif>
		<!--- Le code HTTP est différent de 200 si la liste de rapports n'est pas traitée (e.g Paramètres invalides) --->
		<cfif httpCode NEQ 200>
			<cfset logType="error">
			<cfset returnCode=-1>
			<cfset var contextParameters={REL_MSG="[CaseId: -2] " & responseMsg}>
			<cfif isReportListDefined EQ TRUE>
				<cfset contextParameters.REPORT_LIST=reportList>
			</cfif>
			<cfset storeReportListNotification(-2,logStorage,logStorageUUID,(isDefined("URL") ? DUPLICATE(URL):{}),contextParameters)>
		</cfif>
		<!--- Réponse HTTP --->
		<cflog type="#logType#" text="[REPORTING >> HTTP #httpCode#][#logStorageUUID#] #responseMsg#">
		<cfset sendHttpResponse(httpCode,serializeJSON({message=responseMsg,logUUID=logStorageUUID}))>
		<cfreturn returnCode>
		<cfcatch type="Any">
			<!--- Notification des exceptions capturées durant l'exécution --->
			<cfset var contextParameters={ERROR=CFCATCH,REL_MSG="[CaseId: -1] Exception durant l'exécution"}>
			<cfset storeReportListNotification(-1,logStorage,logStorageUUID,(isDefined("URL") ? DUPLICATE(URL):{}),contextParameters)>
			<!--- Réponse HTTP en cas d'exception durant le processus --->
			<cfset var httpCode=500>
			<cfset var responseMsg="[CaseId: -1] - " & CFCATCH.message>
			<cflog type="error" text="[REPORTING >> HTTP #httpCode#][#logStorageUUID#] #responseMsg#">
			<cfset sendHttpResponse(httpCode,serializeJSON({message=responseMsg,logUUID=logStorageUUID}))>
			<cfreturn -1>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction access="private" name="storeReportListNotification" returntype="void" output="false" hint="Stockage des notifications durant l'exécution">
	<cfargument name="notificationCase" type="Numeric" required="true" hint="Valeur indiquant le type métier de la notification">
	<cfargument name="logging" type="fr.saaswedo.utils.log.LogStorage" required="true" hint="Librairie de stockage des logs">
	<cfargument name="logUUID" type="String" required="true" hint="UUID à utiliser pour le stockage de la notification">
	<cfargument name="requestParameters" type="Struct" required="true" hint="Paramètres HTTP GET (URL)">
	<cfargument name="contextParameters" type="Struct" required="true" hint="Paramètres contextuels (Liste de rapports,Exception,etc...)">
	<!--- Notification --->
	<cfset var body="">
	<cfset var logStorage=ARGUMENTS.logging>
	<cfset var logStorageUUID=ARGUMENTS.logUUID>
	<cfset var caseId=ARGUMENTS.notificationCase>
	<!--- Paramètres et contexte d'execution --->
	<cfset var urlParams=ARGUMENTS.requestParameters>
	<cfset var contextParams=ARGUMENTS.contextParameters>
	<cftry>
		<!--- Liste des cas de notifications
		Cas 0 (Info) : Notification de la liste de rapports
		Cas -1 (Error) : Exception capturée durant le traitement de la liste des rapports
		Cas -2 (Error) : Erreur durant le traitement de la liste de rapports
		Cas -3 (Error) : Erreur durant la planification d'un rapport
		Cas -4 (Error) : Erreur dans le résultat de la planification d'un rapport
		--->
		<cfif (caseId EQ 0) OR (caseId EQ -1) OR (caseId EQ -2) OR (caseId EQ -3) OR (caseId EQ -4)>
			<cfset var logType=(caseId GTE 0) ? logStorage.INFO_LOG():logStorage.ERROR_LOG()>
			<!--- BackOffice --->
			<cfset var serverName=CGI.SERVER_NAME & ":" & CGI.SERVER_PORT>
			<cfset var serverVersion=
				(structKeyExists(SERVER,"coldfusion") AND structKeyExists(SERVER.coldfusion,"productversion")) ? SERVER.coldfusion.productversion:"N.D">
			<!--- Contexte d'execution --->
			<cfset var reportListId=structKeyExists(urlParams,"TYPE_MAIL")? VAL(urlParams.TYPE_MAIL):-1>
			<cfset var mode=(structKeyExists(urlParams,"MODE") AND isValidMode(urlParams.MODE)) ? urlParams.MODE:DEFAULT_MODE()>
			<cfset var endpoint=getModeEndpoint(mode)>
			<!--- ID de diffusion et Message associé à l'erreur --->
			<cfset var diffusionId=structKeyExists(contextParams,"ID") ? VAL(contextParams.ID):-1>
			<cfset var relatedMessage=structKeyExists(contextParams,"REL_MSG") ? contextParams.REL_MSG:"Non défini">
			<cfset var reportParamNames=structKeyExists(contextParams,"REPORT_PARAM_NAMES") ? contextParams.REPORT_PARAM_NAMES:"Non défini">
			<!--- Contenu de la notification --->
			<cfset var subjectId=getJobName(logStorageUUID,reportListId,diffusionId,mode)>
			<cfsavecontent variable="body">
				<cfoutput>
					<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
					<body>
						<center><h2>Notification concernant l'exécution d'une diffusion de rapports</h2></center><hr>
						<center><h3>Infos générales</h3></center>
						<b>Identifiant du cas (0,-1,-2,-3,-4):</b> #caseId#<br>
						<b>UUID des logs de la diffusion :</b> #logStorageUUID#<br>
						<b>Identifiant de la liste (TYPE_MAIL) :</b> #reportListId#<br>
						<b>Identifiant de la diffusion (ID) :</b> #diffusionId#<br>
						<b>MODE (API) :</b> #mode#<br>
						<b>URI API (HTTP Method:#CGI.REQUEST_METHOD#):</b> #CGI.SCRIPT_NAME#<br>
						<b>WSDL BIP :</b> #endpoint#<br>
						<b>BackOffice :</b> #serverName# (version: #serverVersion#)<hr>
						<cfif caseId LT 0>
							<center><h3>Infos concernant l'erreur</h3></center>
							<b>Message associée à l'erreur :</b> #relatedMessage#<br>
							<cfif structKeyExists(contextParams,"ERROR")>
								<b>Exception :</b><br>
								<cfdump var="#contextParams.ERROR#" format="text"><hr>
							<cfelse>
								Non défini<hr>
							</cfif>
						</cfif>
						<center><h3>Infos concernant la liste de rapport et les paramètres</h3></center>
						<b>Liste des colonnes obligatoires trouvées dans la liste et utilisées par cette API<br>
						(Toute colonne non mentionnée ci-dessous et qui ne figure pas dans les
						paramètres ci-dessous est utilisée si elle est facultative sinon elle est ignorée):</b><br>#REQ_DIFFUSION_COLUMNS()#<br>
						<b>Liste des colonnes considérées comme paramètres des rapports :</b><br>#reportParamNames#<br><br>
						<b>Liste de diffusion :</b><br>
						<cfif structKeyExists(contextParams,"REPORT_LIST")>
							<cfdump var="#contextParams.REPORT_LIST#"><hr>
						<cfelse>
							Non défini<hr>
						</cfif>
						<b>Paramètres HTTP :</b><br>
						<cfdump var="#urlParams#"><hr>
						<cfif caseId NEQ 0>
							<center><h3>Infos concernant le remoting si c'est le cas</h3></center>
							<b>Résultat de l'invocation SOAP :</b><br>
							<cfif structKeyExists(contextParams,"REMOTING_RESULT")>
								<cfdump var="#contextParams.REMOTING_RESULT#">
							<cfelse>
								Non défini
							</cfif>
						</cfif>
					</body>
				</cfoutput>
			</cfsavecontent>
			<cfset logStorage.logIt({
				IDMNT_CAT=IDMNT_CAT(),IDMNT_ID=IDMNT_ID(),LOG_TYPE=logType,UUID=logStorageUUID,SUBJECTID=subjectId,BODY=body
			})> 
		<!--- Sinon aucune notification n'est stockée et une exception sera lancée (et stockée) --->
		<cfelse>
			<cfset subjectId="Failed to store notification. Unknown type (caseId) '#caseId#'">
			<cfthrow type="Custom" message="#subjectId#" detail="logUUID: #logStorageUUID#">
		</cfif>
		<cfcatch type="Any">
			<cfset var subjectId="Failed to store notification " & logStorageUUID>
			<cfset var relatedMessage=structKeyExists(contextParams,"REL_MSG") ? contextParams.REL_MSG:"Non défini">
			<cfsavecontent variable="body">
				<cfoutput>
					<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
					<center><h2>Echec du stockage d'une notification concernant une diffusion de rapports</h2></center>
					<b>Identifiant du cas (0,-1,-2,-3,-4) :</b> #caseId#<br>
					<b>UUID des logs de la diffusion :</b> #logStorageUUID#<br>
					<b>BackOffice :</b> #serverName# (version: #serverVersion#)<hr>
					<center><h3>Infos concernant l'erreur</h3></center>
					<b>Message associée à l'erreur :</b> #relatedMessage#<hr>
					<b>Exception :</b><br>
					<cfdump var="#CFCATCH#" format="text"><hr>
					<center><h3>Autres infos liées au contexte d'exécution :</h3></center>
					<b>Scope SERVER (#CGI.SERVER_NAME#) :</b><br>
					<cfdump var="#SERVER#" format="text"><hr>
					<b>Paramètres HTTP GET :</b><br>
					<cfdump var="#urlParams#"><hr>
					<b>Contexte fourni lors du stockage de la notification</b><br>
					<cfdump var="#contextParams#">
				</cfoutput>
			</cfsavecontent>
			<cfset logStorage.logIt({
				IDMNT_CAT=IDMNT_CAT(),IDMNT_ID=IDMNT_ID(),LOG_TYPE=logStorage.ERROR_LOG(),UUID=logStorageUUID,SUBJECTID=subjectId,BODY=body
			})>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction access="private" name="scheduleReport" returntype="Struct" hint="Planifie le rapport dans BIP pour un envoi par mail">
	<cfargument name="remotingClient" type="fr.saaswedo.utils.remoting.SoapClient" required="true" hint="Librairie de remoting">
	<cfargument name="scheduleRequest" type="Struct" required="true" hint="ScheduleRequest BIP">
	<cfargument name="mode" type="String" required="false" default="" hint="Mode (e.g PROD)">
	<cfset var remoting=ARGUMENTS.remotingClient>
	<cfset var bipRequest=ARGUMENTS.scheduleRequest>
	<cfset var currentMode=isValidMode(ARGUMENTS.mode) ? ARGUMENTS.mode:DEFAULT_MODE()>
	<!--- API BIP via Librairie Remoting --->
	<cfset var service={endpoint=getModeEndpoint(currentMode)}>
	<cfset var procedure="scheduleReport">
	<cfset var parameters=[
		{name="scheduleRequest",value=bipRequest},
		{name="userID",value="scheduler"},{name="password",value="public"}
	]>
	<!--- Planification du rapport (API BI Publisher) --->
	<cfset var startTick=getTickCount()>
	<cfset var invokeResult=remoting.invoke(service,procedure,parameters)>
	<cfset var durationS1=getTickCount() - startTick>
	<cfset invokeResult.S1_DURATION_MS=durationS1>
	<!--- JOBID --->
	<cfset var jobId=-1>
	<cfif invokeResult[remoting.STATUS()] EQ remoting.DEFINED()>
		<cfset jobId=VAL(invokeResult[remoting.RESULT()])>
	</cfif>
	<!--- [Overhead négligeable] Les données XML sont converties au format texte car elles ne s'affichent pas correctement dans certains outils --->
	<cfset startTick=getTickCount()>
	<cfif structKeyExists(invokeResult,remoting.CLIENT_REQUEST()) AND isXML(invokeResult[remoting.CLIENT_REQUEST()])>
		<cfset invokeResult[remoting.CLIENT_REQUEST()]=toString(invokeResult[remoting.CLIENT_REQUEST()])>
	</cfif>
	<cfif structKeyExists(invokeResult,remoting.SERVER_RESPONSE()) AND isXML(invokeResult[remoting.SERVER_RESPONSE()])>
		<cfset invokeResult[remoting.SERVER_RESPONSE()]=toString(invokeResult[remoting.SERVER_RESPONSE()])>
	</cfif>
	<cfset var durationS2=getTickCount() - startTick>
	<cfset invokeResult.S2_DURATION_MS=durationS2>
	<cflog type="information"
	text="[JOBID:#jobId# / S1: #(durationS1 / 1000)# secs / S2: #(durationS2 / 1000)# secs] BI Publisher WSDL : #service.endpoint#">
	<cfreturn invokeResult>
</cffunction>

<cffunction access="private" name="loadReportList" returntype="Query" hint="Liste des rapports à diffuser">
	<cfargument name="parameters" type="Struct" required="true" hint="Paramètres de récupération de la liste">
	<cfargument name="logUUID" type="String" required="true" hint="UUID à utiliser pour le stockage de la notification">
	<cfset var storedProcName="PKG_PFGP_SUIVI_CHARGEMENT.TYPE_LISTE">
	<cfset var params=DUPLICATE(ARGUMENTS.parameters)>
	<!--- Ajout de l'UUID des logs communs à tous les rapports de cette liste --->
	<cfset params.UUID=ARGUMENTS.logUUID>
	<cfset var jsonParameters=serializeJSON(params)>
	<!--- La valeur de la colonne ID doit etre un entier strictement positif lors du traitement de la notification BIP correspondante --->
	<cfstoredproc datasource="ROCOFFRE" procedure="#storedProcName#">
		<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#jsonParameters#">
		<cfprocresult name="reportList">
	</cfstoredproc>
	<!--- Message d'erreur généré par la procédure stockée --->
	<cfif structKeyExists(reportList,"ERROR")>
		<cfset var storeProcMsg=0>
		<cfif reportList.recordcount GT 0>
			<cfset storeProcMsg=reportList["ERROR"][1]>
		</cfif>
		<cfthrow type="Custom" message="StoredProc #storedProcName# error message #storeProcMsg#">
	<cfelse>
		<!--- Mock Data (Example) : Report parameters
		<cfif reportList.recordcount EQ 0>
			<cfset queryAddRow(reportList,1)>
			<!--- Colonnes requises --->
			<cfset querySetCell(reportList,"ID",1,1)>
			<cfset querySetCell(reportList,"DOSSIER","/CONSOTEL/Services/RAPPORTS_SPE/RapportSpe_CONSOTEL/BAKER_MCKENZIE",1)>
			<cfset querySetCell(reportList,"XDO","ENDUSER_SPE",1)>
			<cfset querySetCell(reportList,"TEMPLATE","default",1)>
			<cfset querySetCell(reportList,"EMAIL_FROM","cedric.rapiera@saaswedo.com",1)>
			<cfset querySetCell(reportList,"EMAIL_SUBJECT","[#NOW()#] INLINE HTML REPORT #ARGUMENTS.logUUID#",1)>
			<cfset querySetCell(reportList,"EMAIL_TO","monitoring@saaswedo.com",1)>
			<cfset querySetCell(reportList,"EMAIL_CC","monitoring@saaswedo.com",1)>
			<!--- Paramètres de rapports --->
			<cfset querySetCell(reportList,"G_IDMASTER",5788886,1)>
			<cfset querySetCell(reportList,"G_IDRACINE",5788886,1)>
			<cfset querySetCell(reportList,"G_IDORGA",6140517,1)>
			<cfset querySetCell(reportList,"G_IDPERIMETRE",6141771,1)>
			<cfset querySetCell(reportList,"G_IDCLICHE",0,1)>
			<cfset querySetCell(reportList,"U_IDPERIOD_DEB",171,1)>
			<cfset querySetCell(reportList,"U_IDPERIOD_FIN",171,1)>
			<cfset querySetCell(reportList,"G_NIVEAU","D",1)>
			<cfset querySetCell(reportList,"G_LANGUE_PAYS","fr_FR",1)>
			<cfset querySetCell(reportList,"U_VARCHAR1","0619346471",1)>
			<cfset querySetCell(reportList,"U_VARCHAR2","3665597",1)>
		</cfif>
		--->
		<!--- Vérification de la présence des colonnes obligatoires --->
		<cfset var reqColumns=REQ_DIFFUSION_COLUMNS()>
		<cfset var reqColumnsCount=listLen(reqColumns,",",FALSE)>
		<cfloop index="i" from="1" to="#reqColumnsCount#">
			<cfset var reqColumn=listGetAt(reqColumns,i,",",FALSE)>
			<cfif NOT structKeyExists(reportList,reqColumn)>
				<cfthrow type="Custom" message="Column '#reqColumn#' is missing in storedProc #storedProcName# result">
			</cfif>
		</cfloop>
		<cfreturn reportList>
	</cfif>
</cffunction>

<cffunction access="private" name="extractReportParamNames" returntype="String" hint="Liste des colonnes de paramètres du rapport">
	<cfargument name="columnList" type="String" required="true" hint="Liste des colonnes de la liste des rapports">
	<cfset var columnsMap={}>
	<cfset var columns=ARGUMENTS.columnList>
	<cfset var columnsCount=listLen(columns,",",FALSE)>
	<cfloop index="i" from="1" to="#columnsCount#">
		<cfset var column=listGetAt(columns,i,",",FALSE)>
		<cfset columnsMap[column]=1>
	</cfloop>
	<!--- Suppression des colonnes de diffusion --->
	<cfset var diffusionColumns=REQ_DIFFUSION_COLUMNS() & "," & OPT_DIFFUSION_COLUMNS()>
	<cfset columnsCount=listLen(diffusionColumns,",",FALSE)>
	<cfloop index="i" from="1" to="#columnsCount#">
		<cfset var column=listGetAt(diffusionColumns,i,",",FALSE)>
		<cfif structKeyExists(columnsMap,column)>
			<cfset structDelete(columnsMap,column)>
		</cfif>
	</cfloop>
	<!--- Il ne reste que les colonnes de paramètres du rapport --->
	<cfreturn structKeyList(columnsMap,",")>
</cffunction>

<cffunction access="private" name="REQ_DIFFUSION_COLUMNS" returntype="String" hint="Liste des colonnes obligatoires de diffusion">
	<cfreturn "ID,DOSSIER,XDO,EMAIL_CC,EMAIL_FROM,EMAIL_SUBJECT,EMAIL_TO">
</cffunction>

<cffunction access="private" name="OPT_DIFFUSION_COLUMNS" returntype="String" hint="Liste des colonnes facultatives de diffusion">
	<cfset var columnList="LOCALISATION,TEMPLATE,FORMAT,MAIL_SERVER,BCC,HAVE_ATTACHEMENT,POST_PROCESS,SHORT_NAME,USERID">
	<cfset columnList=columnList & "," & "CODE_APPLI,DELIVERY,FILE_EXT,FILE_NAME">
	<cfreturn columnList>
</cffunction>

<cffunction access="private" name="IDMNT_CAT" returntype="Numeric" hint="IDMNT_CAT">
	<cfreturn 15>
</cffunction>

<cffunction access="private" name="IDMNT_ID" returntype="Numeric" hint="IDMNT_ID">
	<cfreturn 25>
</cffunction>

<cffunction access="private" name="DEFAULT_MODE" returntype="String" hint="Mode par défaut">
	<cfreturn "TEST">
</cffunction>

<cffunction access="private" name="DEFAULT_MAIL_SERVER" returntype="String" hint="Serveur mail par défaut dans BIP">
	<cfreturn "MAIL_CONSOTEL">
</cffunction>

<cffunction access="private" name="DEFAULT_LOCALE" returntype="String" hint="Locale par défaut dans BIP">
	<cfreturn "fr_FR">
</cffunction>

<cffunction access="private" name="DEFAULT_TZ" returntype="String" hint="Timezone par défaut dans BIP">
	<cfreturn "Europe/Paris">
</cffunction>

<cffunction access="private" name="getJobName" returntype="String" hint="Retourne le JOBNAME utilisé pour le mode">
	<cfargument name="logUUID" type="String" required="true" hint="UUID à utiliser pour le stockage de la notification">
	<cfargument name="reportListId" type="Numeric" required="true" hint="TYPE_MAIL">
	<cfargument name="idDiffusion" type="Numeric" required="true" hint="ID de diffusion">
	<cfargument name="mode" type="String" required="false" default="" hint="Mode (e.g PROD)">
	<cfset var modeValue=isValidMode(ARGUMENTS.mode) ? ARGUMENTS.mode:DEFAULT_MODE()>
	<cfset var httpDest=getHttpDestination(modeValue)>
	<!--- Elements composant le JOBNAME
	- MODE : Identifie le endpoint de l'API BIP
	- HTTP_DEST : Nom du serveur HTTP dans BIP
	- LIST : TYPE_MAIL
	- ID : ID de diffusion (i.e Un rapport dans une liste de diffusion)
	- UUID : LOG UUID (MNT_DETAIL)
	--->
	<cfreturn "MODE:" & modeValue & "/HTTP_DEST:" & httpDest &
		"/LIST:" & ARGUMENTS.reportListId & "/ID:" & ARGUMENTS.idDiffusion & "/UUID:" & ARGUMENTS.logUUID>
</cffunction>

<cffunction access="private" name="getHttpDestination" returntype="String" hint="Retourne la destination HTTP utilisée pour le mode">
	<cfargument name="mode" type="String" required="false" default="" hint="Mode (e.g PROD)">
	<cfset var modeValue=isValidMode(ARGUMENTS.mode) ? ARGUMENTS.mode:DEFAULT_MODE()>
	<cfreturn UCASE(modeValue) & ".DEFAULT">
</cffunction>

<cffunction access="private" name="getModeEndpoint" returntype="String" hint="Retourne l'URL du WSDL de l'API BI Publisher pour le mode spécifié">
	<cfargument name="mode" type="String" required="false" default="" hint="Mode (e.g PROD)">
	<cfset var modeValue=isValidMode(ARGUMENTS.mode) ? ARGUMENTS.mode:DEFAULT_MODE()>
	<!--- Liste des endpoints BIP --->
	<cfset var bipServers={
		PROD="http://bip-prod.consotel.fr/xmlpserver/services/PublicReportService_v11?wsdl",
		TEST="http://bip-int.consotel.fr/xmlpserver/services/PublicReportService_v11?wsdl",
		DEV="http://developpement.consotel.fr/xmlpserver/services/PublicReportService_v11?wsdl"
	}>
	<cfreturn bipServers[modeValue]>
</cffunction>

<cffunction access="private" name="isValidMode" returntype="Boolean" hint="Retourne TRUE si la valeur du mode est valide">
	<cfargument name="mode" type="String" required="false" default="" hint="Mode (e.g PROD)">
	<cfset var modeValue=ARGUMENTS.mode>
	<cfreturn (compareNoCase(modeValue,DEFAULT_MODE()) EQ 0) OR (compareNoCase(modeValue,"PROD") EQ 0) OR (compareNoCase(modeValue,"DEV") EQ 0)>
</cffunction>

<cffunction access="private" name="sendHttpResponse" returntype="void" output="true" hint="Envoi au client une réponse HTTP avec le contenu jsonContent">
	<cfargument name="httpCode" type="Numeric" required="true" hint="Code HTTP envoyé au client">
	<cfargument name="jsonContent" type="String" required="true" hint="Contenu de la réponse HTTP au format JSON">
	<cfheader statuscode="#ARGUMENTS.httpCode#">
	<cfheader name="Content-Type" value="application/json">
	<cfoutput>#TRIM(ARGUMENTS.jsonContent)#</cfoutput>
</cffunction>