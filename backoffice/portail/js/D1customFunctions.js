/*DATATABLES*/
$(document).ready( function () {
	var oTable = $('#tableSortable1').dataTable( {
		"aaSorting": [[2,'desc']],
		"aoColumnDefs": [
		    {"sType": "eu_date", "aDataSort": [2,3], "aTargets": [2]},
		    {"aDataSort": [3,2], "aTargets": [3]},
		    {"sType": "numeric-comma", "aDataSort": [5,6], "aTargets": [5]},
		    {"aDataSort": [6,5], "aTargets": [6]},
		    {"sType": "numeric-comma", "aTargets": [7]},
		    {"sType": "numeric-comma", "aTargets": [8]},
		    {"sType": "numeric-comma", "aTargets": [9]}
	    ],
	    "bStateSave": false,
    	"bPaginate": true,
		"bLengthChange": false,
		"iDisplayLength": 20,
        "oLanguage": {
        	"sSearch": "Rechercher :",
            "sLengthMenu": " _MENU_ enregistrement(s) par page",
            "sZeroRecords": "Aucun résultat trouvé",
            "sInfo": "Entrée de _START_ à _END_ sur _TOTAL_ enregistrement(s)",
            "sInfoEmpty": "",
            "sInfoFiltered": "(Filtré parmi _MAX_ enregistrements)",
            "oPaginate": {
            	"sFirst": "première page",
		       	"sLast": "dernière page",
		       	"sPrevious": "page précédante",
		       	"sNext": "page suivante"
	     	}
	    }
	} );
	
	//position du filtre
	$('#tableSortable1_filter.dataTables_filter').css({"margin-bottom":"3px"});
	
} );


/* onglet table logiciel */
$(document).ready( function () {
	//calcul de la longueur de la table selon le nombre de logiciel du terminal
	var scroll = "";
	var lgt = $("#tableSortable2 > tbody > tr").length;
	if (lgt > 10){scroll = "200px";} else {scroll = "auto";}
	
	//initialisation des caracteristiques de la table
	var oTable = $('#tableSortable2').dataTable( {
		"aoColumns": [
	         { "sWidth": "233px" },
	         { "sWidth": "110px" },
	         { "sWidth": "110px" },     
	         { "sWidth": "160px" }
        ],
	    "bStateSave": false,
    	"bPaginate": false,
    	"sScrollY": scroll,
		"bLengthChange": false,
		"iDisplayLength": 15,
        "oLanguage": {
        	"sSearch": "Rechercher :",
        	"sInfoFiltered": "",
            "sZeroRecords": "Aucun résultat trouvé",
            "sInfo": "_TOTAL_ logiciel(s) installé(s)",
            "sInfoEmpty": "Aucun résultat trouvé"
	    }
	} );
	
	//position du filtre selon nombre de logiciel et selon le browser
	if (!$.browser.msie){
		if (lgt > 10){
			$('#tableSortable2_filter.dataTables_filter').css({"width":"97.5%", "margin-bottom":"3px", "text-align":"right"});
		} else {
			$('#tableSortable2_filter.dataTables_filter').css({'width':'100%',  "margin-bottom":"3px", "text-align":"right"});
		}
	}
	else { $('#tableSortable2_filter.dataTables_filter').css({"width":"667px", "margin-bottom":"3px", "text-align":"right"}); }
	
	// correction d''un bug : lors du clic sur l'onglet, resize les colonnes d'entetes
	$('#onglet_logiciel').click( function() {
		oTable.fnAdjustColumnSizing();
	});
} );

/* onglet table contrat de service */
$(document).ready( function () {
	//initialisation des caracteristiques de la table
	var oTable = $('#tableSortable3').dataTable( {
        "aoColumnDefs": [
		    {"sType": "eu_date", "aTargets": [5]},
		    {"sType": "numeric-comma", "aTargets": [7]}
	    ],
	    "bStateSave": false,
    	"bPaginate": false,
		"bLengthChange": false,
		"iDisplayLength": 15,
        "oLanguage": {
        	"sSearch": "Rechercher :",
        	"sInfoFiltered": "",
            "sZeroRecords": "Aucun résultat trouvé",
            "sInfo": "_TOTAL_ contrat(s) répertorié(s)",
            "sInfoEmpty": "Aucun résultat trouvé"
	    }
	} );
	
	// correction d''un bug : lors du clic sur l'onglet, resize les colonnes d'entetes
	$('#onglet_contrats_de_service').click( function() {
		oTable.fnAdjustColumnSizing();
	});
} );


$(document).ready( function() {
	var longueur;
	if ($.browser.msie){ longueur = "252";} else {longueur = "250";}
	$('select#selectTerminal').selectmenu({
		width: longueur,
		format: addressFormatting
	});
	$('select#selectLigne').selectmenu({
		width: longueur,
		format: addressFormatting
	});
	$('select#selectFacture').selectmenu({
		width: longueur,
		format: addressFormatting
	});
});
//a custom format option callback
var addressFormatting = function(text){
	var newText = text;
	//array of find replaces
	var findreps = [
		{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
		{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
		{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
		{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
		{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
	];
	for(var i in findreps){
		newText = newText.replace(findreps[i].find,findreps[i].rep);
	}
	return newText;
}
