/**/
$(document).ready(function () {
  function reorient(e) {
    var portrait = (window.orientation % 180 == 0);
    $("body > div").css("-webkit-transform", !portrait ? "rotate(-90deg)" : "");
  }
  window.onorientationchange = reorient;
  window.setTimeout(reorient, 0);
});

/**/
/**/
function verificationLog() {
	var login = document.getElementById('loginIndex');
	var password = document.getElementById('passwordIndex');
	var codeSecu = document.getElementById('codeSecu');
	var isCheckCaptcha = codeSecu != undefined;
	
	if (login.value == ""
	|| password.value == ""
	|| (isCheckCaptcha && (codeSecu.value == "" || codeSecu.value == "Veuillez saisir le code ci-dessous"))) {
		if (isCheckCaptcha) {
			alert("Vous devez saisir le login, le mot de passe et le code de sécurité.");
		} else {
			alert("Vous devez saisir le login et le mot de passe.");
		}
		if (login.value == ""){
			login.focus();
		} else if (password.value == "") {
			password.focus();
		} else {
			codeSecu.focus();
		}
		return false;
	}
	
	return true;
}

/**/
/**/
function verificationEmail() {
	var login = document.getElementById('loginOublie');
	
	if (login.value == "") {
		alert("Vous devez saisir votre identifiant.");
		if (login.value == ""){
			login.focus();
		}
		return false;
	}
	return true;
}

/**/
/**/
function logOublie() {
	return true;
}

/**/
/**/
function backIndex(){
	return true;
}




/*
function afficher(){
	alert("ok");
}

function afficherFacture(){
	alert("affiche_facture");
	var fact = document.getElementById("facture").style.display = 'block';
}

function fermerFacture(){
	var fact = document.getElementById("facture").style.display = 'none';
}
function creerCompte() {
	document.location = "/extranet/servlet/EntreeRegisterOnline?tache=tache.registeronline.registration&action=action.inputmanuel";
}

function init() {
	if (top.frames.length !=0) {
		top.location = self.document.location;
	}
	if (document.getElementById('codeSecu') != undefined) {
		document.getElementById('codeSecu').value="Veuillez saisir le code ci-dessous";
	}
	document.getElementById('login').focus();
}
*/