<cfcomponent extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- empty --->
	</cffunction>

	
	<cffunction name="getAccueilDivLastFacture" access="public" output="true"  returnType="string"
				displayname="" 
				hint="Génère la div contenant le detail de la derniere facture concernant une ligne d'un collaborateur / template D1" >
		
		<cfargument name="idSous_tete" required="false" type="numeric" default=0>
		
		<cfif idSous_tete eq 0 > <!--- si premiere fois sur la page (idSous_tete par defaut = 0)--->
		
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
							
					<!--- on recupere la premiere idSous_tete associé à un terminal pour affichage dans select "MA LIGNE" (page accueil) --->
					<cfset idSous_tete = SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow]>
					<cfbreak>
					
				</cfif>
			</cfloop>
			
		</cfif>
		
		<!--- QUERY DATE DE FACTU --->
		<!--- si la STRUCTURE EN SESSION QUERYFACT n'existe pas ou si la variable de SESSION IDSOUS_TETE pas egale au parametre idsous_tete --->
		<cfif NOT isDefined("SESSION.queryFact") || (isDefined("SESSION.clone_IdSous_tete") && SESSION.clone_IdSous_tete neq idSous_tete) >
			<cfset SESSION.queryFact = createObject("component","portail.class.classData.DataService").getDateFactByLigne(idSous_tete,SESSION.PERIMETRE.ID_GROUPE,6,SESSION.USER.GLOBALIZATION) >
		</cfif>
		
		<cfset moyMois = " -- ">
		<cfset euro = "">
		<cfset ht = "">
		<cfset totalAbo = " -- ">
		<cfset totalConso = " -- ">
		<cfset totalfacture = " -- ">
		<cfset coutMoyen = " -- ">
		<cfset dateFact = " Aucune facture de moins de 6 mois disponible " >
		
		<cfset lastFact = "" >
		<cfset idPeriode = 0 >
		
		<cfif SESSION.queryFact.recordcount gt 0> <!--- si query date de facturation est non vide --->
			
			<cfset euro = "&nbsp;€">
			<cfset ht = "&nbsp;HT">
			<cfset moyMois = SESSION.queryFact.recordcount>
			
			<!--- loop sur query periode --->
			<cfloop query = "SESSION.queryFact">
				<cfset currentRow = SESSION.queryFact.currentrow >
				
				<cfset lastFact = SESSION.queryFact["DATE_EMISSION"][currentRow] >
				<cfset idPeriode = SESSION.queryFact["IDPERIODE_MOIS"][currentRow] >
				<cfset totalfacture = SESSION.queryFact["MONTANT"][currentRow]&euro&ht >
				<cfset totalAbo = SESSION.queryFact["MONTANT_ABOS"][currentRow]&euro&ht >
				<cfset totalConso = SESSION.queryFact["MONTANT_CONSOS"][currentRow]&euro&ht >
				<cfset coutMoyen = DecimalFormat(SESSION.queryFact["MONTANTFACTU_MOYEN"][1])&euro>
				<cfbreak>
			</cfloop><!--- fin loop sur query periode --->
			
			
			<!--- FORMATAGE DATE POUR LE PREMIER CHOIX DU SELECT --->
			<cfif lastFact neq "">
				<cfset date1 = lastFact >
				<cfset dateFact = "Votre facture du&nbsp;"&LSDateFormat(date1, "dd/mm/yyyy") >
			</cfif>
			
		</cfif>
		
		<!--- GRAPHIQUE DATA --->
		<cfset chart = createObject("component","portail.class.classData.DataService").getChartAera(idSous_tete,moyMois)>
		
		
		<cfset divLastFact = '
				<div class="divLastFact" style="">
					<div style="">
						<div class="pictoFacture" style=""><img src="img/mytem/ma_derniereFacture.png"></div>
						<div class="intituleFact" style="">Ma derniere facture</div>
						<div class="clear" style=""></div>
					</div>
					<div >
						<div >
							<div class="pictoFacture" style="" ></div>
							<div class="divTableFacture" style="">
								<table class="tableFacture" cellspacing="0" cellpadding="0" style="" width="100%">
									<colgroup>
										<col width="60%"></col>
										<col width="40%"></col>
									</colgroup>
									<thead>
										<tr style="">
											<td colspan="2" style="border-top:0px;border-left:0px;border-right:0px;">#dateFact#</td>
										</tr>
									</thead>
									<tbody>
										<tr style="">
											<td style="">Montant total facture </td>
											<td align="center" style="">#totalfacture#</td>
										</tr>
										<tr style="">
											<td style="">Dont abonnements</td>
											<td align="center" style="">#totalabo#</td>
										</tr>
										<tr style="">
											<td style="">Dont consommations </td>
											<td align="center" style="">#totalconso#</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="clear" style=""></div>
						</div>
						<div class="grapheFacture" style="">
								#chart#
						</div>
						<div align="center" class="divCoutMensuelFact" style="">
							<div class="intitule" style="">Mon coût mensuel moyen sur les #moyMois# derniers mois</div>
							<div class="cout" style="font-weight:bold;">#coutMoyen#</div>
							<div class="clear" style=""></div>
						</div>
					</div>
				</div>
		'>
		
		<cfreturn divLastFact>
	</cffunction>

</cfcomponent>