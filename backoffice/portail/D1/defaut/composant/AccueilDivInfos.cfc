<cfcomponent extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- empty --->
	</cffunction>
	
	
	<!---  --->
	<cffunction name="getAccueilDivInfos" access="public" output="true" returnType="string"
				displayname="" 
				hint="Génère la div contenant les infos d'un colaborateur et de son terminal / template D1" >
		
		<cfargument name="idSousTete" required="false" type="numeric" default=0 >
		
		<cfif NOT isDefined("SESSION.clone_IdSous_tete") || (isDefined("SESSION.clone_IdSous_tete") && SESSION.clone_IdSous_tete neq idSousTete)>
			<cfset SESSION.clone_IdSous_tete = idSoustete>
		</cfif>
		
		<!--- --------- --->
		<!--- MES INFOS --->
		<!--- --------- --->
		
		<cfif NOT isDefined("SESSION.queryMesInfos") >
			<cfset query = createObject("component","fr.consotel.consoview.M111.fiches.FicheCollaborateur").getInfosFicheCollaborateur(SESSION.IDEMPLOYE,SESSION.IDPOOL,0) >
			<!--- ON RECUPERE LE 2eme ARRAY RETOURNÉ --->
			<cfset SESSION.queryMesInfos = query[2]>
		</cfif>
		
		<!--- INITIALISATION VARIABLES INFOS USER --->
		<cfset color = "color1">
		<cfset nom = " -- ">
		<cfset prenom = " -- " >
		<cfset email = " -- " >
		<cfset fonction = " -- " >
		<cfset telFixe = " -- " >
		<cfset faxx = " -- " >
		<cfset matricule = " -- " >
		
		<cfif NOT SESSION.queryMesInfos["NOM"][1] eq "" >
			<cfset nom = SESSION.queryMesInfos["NOM"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["PRENOM"][1] eq "" >
			<cfset prenom = SESSION.queryMesInfos["PRENOM"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["EMAIL"][1] eq "" >
			<cfset email = SESSION.queryMesInfos["EMAIL"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["FONCTION_EMPLOYE"][1] eq "" >
			<cfset fonction = SESSION.queryMesInfos["FONCTION_EMPLOYE"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["TELEPHONE_FIXE"][1] eq "" >
			<cfset telFixe = SESSION.queryMesInfos["TELEPHONE_FIXE"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["FAX"][1] eq "" >
			<cfset faxx = SESSION.queryMesInfos["FAX"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["MATRICULE"][1] eq "" >
			<cfset matricule = SESSION.queryMesInfos["MATRICULE"][1] >
		</cfif>
		
		
		
		<cfset divMesInfos = '
				<div class="divInfo_pilotage" style="">
					<div>
						<div class="pictoInfo" style="float:left;height:30px;width:40px;"><img src="img/mytem/mes_infos.png"></div>
						<div class="intituleInfo" style="">Mes infos</div>
						<div class="clear" style=""></div>
					</div>
					<div>
						<div class="pictoInfo" style="float:left;height:30px;width:50px;"></div>
						<div class="divTableInfo" style="">
							<table class="tableInfo_pilotage" style="" width="100%">
								<tbody>
									<tr style="">
										<td class="divInfoIntitule" style="">Nom </td>
										<td class="divInfoValeur" style="">:&nbsp;#nom#</td>
									</tr>
									<tr style="">
										<td class="divInfoIntitule" style="">Prénom</td>
										<td class="divInfoValeur" style="">:&nbsp;#prenom#</td>
									</tr>
									<tr style="">
										<td class="divInfoIntitule" style="">Email</td>
										<td class="divInfoValeur" style="">:&nbsp;#email#</td>
									</tr>
									<tr style="">
										<td class="divInfoIntitule" style="">Fonction</td>
										<td class="divInfoValeur" style="">:&nbsp;#fonction#</td>
									</tr>
									<tr style="">
										<td class="divInfoIntitule" >Matricule</td>
										<td class="divInfoValeur" style="">:&nbsp;#matricule#</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="clear" style=""></div>
					</div>
				</div>
		'>

		<cfreturn divMesInfos>
	</cffunction>
	
	
</cfcomponent>