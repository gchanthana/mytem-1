<cfcomponent extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- empty --->
	</cffunction>
	
	
	<!---  --->
	<cffunction name="getAccueilDivSelectLigne" access="public" output="true" returnType="string"
				displayname="" 
				hint="Génère la div contenant la liste des lignes appartenant à un collaborateur / template D1" >
		
		<cfargument name="sous_tete" required="false" type="string" default="0">
		
		<cfif SESSION.IDEMPLOYE eq "17010"><cfset SESSION.IDEMPLOYE = 75325></cfif>
		<cfif NOT isdefined("SESSION.queryMesTerminaux")>
			<cfset SESSION.queryMesTerminaux = createObject("component","portail.class.classData.DataService").getListEqptFromCollab(SESSION.IDEMPLOYE,SESSION.USER.CLIENTID) >
			
			<cfset cpt = 0>
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
			
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
						
						<cfset cpt = cpt + 1>
				
				</cfif>
			</cfloop>
			<cfset SESSION.terminauxAvailable = cpt> <!--- nb de terminaux avec sous_tete, idSous_tete et sim --->
		</cfif>
		
		<cfif isDefined("SESSION.clone_sous_tete") && SESSION.clone_sous_tete neq sous_tete>	
			<cfset SESSION.clone_sous_tete = sous_tete>
		</cfif>
		
		<!--- SI LE COLLABORATEUR A PLUSIEURS LIGNES --->
		<cfif SESSION.terminauxAvailable gt 1>
		
			<cfset divMaLigneCombo = '
						
						<div align="left" class="divMaLigneComboAccueil" style="">
							<form>
								<span style="font-weight:bold;">Ma ligne</span>&nbsp;&nbsp;
								<select id="selectLigne" class="selectMaLigneCombo" onchange="comboLigne();loadsablier();" style="">
			'>
			
			
			<cfloop query="SESSION.queryMesTerminaux" >
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow >
				
				<cfset sousTete = "" >
				<cfset idSousTete = "" >
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
						
					<cfset sousTete = SESSION.queryMesTerminaux["SOUS_TETE"][currentRow] >
					<cfset idSousTete = SESSION.queryMesTerminaux["IDSOUS_TETE"][currentRow] >
					
					<!--- CREATION D'UNE STRUCTURE POUR LA CONSTRUCTION DE L'URL --->
					<cfset struct = {idPage = "2",
									sous_tete = "#sousTete#",
									idSous_tete = "#idSousTete#"}
					>
					<cfset urlString = createRefURL(struct)>
					
					<!--- formatage numero de telephone / espace entre les chiffres --->
					<cfset numm = createObject("component","portail.class.classData.DataService").phoneFormat("#sousTete#")>
					
					<cfif sousTete eq sous_tete>
						<cfset divMaLigneCombo &= '<option selected="selected" id="#sousTete#" value="#urlString#">#numm#</option>' > <!--- selectionné --->
					<cfelse>
						<cfset divMaLigneCombo &= '<option value="#urlString#">#numm#</option>'> <!--- non selectionné --->
					</cfif>
				</cfif>
				
			</cfloop>
			
			
			<cfset divMaLigneCombo &= '
								</select>
							</form>
						</div>
			'>
		
		<!--- OU SI LE  COLLABORATEUR A UNE SEULE LIGNE --->
		<cfelse>
		
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
						
					<cfset sous_tete = SESSION.queryMesTerminaux["SOUS_TETE"][currentRow] >
						
				</cfif>
			</cfloop>
			
			<!--- formatage numero de telephone / espace entre les chiffres --->
			<cfset numm = createObject("component","portail.class.classData.DataService").phoneFormat("#sous_tete#")>
			<cfif sous_tete eq "0"> <cfset numm = " -- -- -- -- -- "> </cfif>
			<cfset divMaLigneCombo = '<div align="left" class="divMaLigneUniqueAccueil" style="">
										Ma ligne&nbsp;&nbsp;<span style="color:##009fe3;">#numm#</span>
									</div>
			'>
		
		</cfif>
		
		<cfreturn divMaLigneCombo>
	</cffunction>
	
</cfcomponent>