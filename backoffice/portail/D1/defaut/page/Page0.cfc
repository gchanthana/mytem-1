<cfcomponent implements="portail.class.specialClass.InterfacePage"
			 output="false" 
			 displayname="Page" 
			 hint="Classe qui contient la structure de la page de Login dans le template D1" >
	
	
	<!---  --->
	<cffunction name="afficher" access="public" output="true" 
				displayname="" 
				hint="Affiche les composants de la page de Login / template D1" >
		<cfargument name="head" required="true" type="string" displayname="" hint="Contenu de la balise header de la page à afficher">
		<cfargument name="context" required="true" type="struct" displayname="" hint="Structure contenant les données passés dans l'URL"> <!--- le contexte = ref --->
		
		<cfset objLogin = createObject("component","portail.D1.defaut.composant.IndexLogin") >
		<cfset objCnx = createObject("component","portail.class.classData.AccessManagerService") >
		
		<cfset bool = -1 >
		<cfset varCnx = 0>
		<cfset btnCliked = 0>
		<cfset sous_tete = "">
		<cfset type = "">
				
		
		<!--- CREATION DUNE STRUC ET RECUPERATION DES DONNEES PASSEES EN URL DANS CETTE STRUCTURE --->
		<cfset refParam = createObject("component","portail.class.specialClass.AbstractComposant").getInfoSructURL(context) >
		
		
	    <!--- SI LE FORMULAIRE "code perso oublié" A ETE SOUMIS --->
	    <cfif isDefined("FORM.EnvoiSMS") && FORM.EnvoiSMS eq "Envoyer" >
			
		    <!--- Decrypt the captcha check value. Since this was submitted via a FORM, we have to be careful about attempts to hack it. Always put a Decrypt() call
		    inside of a CFTry / CFCatch block. --->
		    <cftry >
			    <cfset btnCliked = 2>
		    <cfcatch> <!--- Catch des erreurs --->
			    <cfdump var="#CFCATCH#">
		    </cfcatch>
		    </cftry>
	    
	    </cfif>
		
		
		<!--- SI LE FORMULAIRE DE "premiere connexion" A ETE SOUMIS --->
	    <cfif isDefined("FORM.firstCnx") && FORM.firstCnx eq "Enregistrer" >
		    <cftry>
			    <cfset btnCliked = 1>
		    <cfcatch><!--- Catch des erreurs --->
			    <cfdump var="#CFCATCH#">
		    </cfcatch>
		    </cftry>
	    
	   </cfif>
	   
	   
		<!--- SI LE FORMULAIRE DE VALIDATION/CONNEXION A ETE SOUMIS --->
	    <cfif isDefined("FORM.cnx") && FORM.cnx eq "Connexion" >
		    <cftry>
			    <cfset btnCliked = 3>
				<cfset telimail = FORM.telimail > <!--- champs email/tel --->
				
			    
				<cfif REFindNocase("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.(([a-z]{2,3})|(test|coop|info|museum|name))$", telimail)>
				   	<!--- mail + mdp valides, fonction verifie dans bdd si email existe ou pas / 
				   		si valide(varCnx 1) connexion, connexion multi lignes(initialisation variable de session), 
				   		SESSION OU VARIABLE INITIALISEE A UNE VALEUR PRECISANT LE MULTILIGNE.
				   		si invalide(varCnx -1) renvoi erreur --->
				   	<cfset varRetour = objCnx.checkUserIdentByEmail("#FORM.telimail#","#FORM.pwd#") >
				   	
					<cfif varRetour eq 1 > <!--- Si valeur de retour = 1, ON REDIRIGE VERS LA PAGE D'ACCEUIL / PAGE 2 --->
						<cfset objLogin.goToPage(2,context)>
					<cfelse>
						<cfset varCnx = -1 >
					</cfif>
					
				<cfelseif REFindNocase("^0[6-7]([-. ]?[0-9]{2}){4}$", telimail)>					
				   	<!--- tel + code perso valides, fonction verifie dans bdd si email existe ou pas / 
				   		si valide(varCnx 1) connexion, connexion unique ligne(initialisation variable de session),
				   		SESSION OU VARIABLE INITIALISEE A UNE VALEUR PRECISANT LIGNE UNIQUE.
				   		si invalide(varCnx -2) renvoi erreur --->
				   	<cfset queryRetour = objCnx.checkUserIdentByLigne("#FORM.telimail#","#FORM.pwd#","1") >
				   	
					<cfif queryRetour.Recordcount gte 1 ><!--- Si valeur de retour = 1, ON REDIRIGE VERS LA PAGE D'ACCEUIL / PAGE 2 --->
						<cfset objLogin.goToPage(2,context) >
					<cfelse>
						<cfset varCnx = -2 >
					</cfif>
				<cfelse>
					<cfset varCnx = -3 >
				</cfif>
		     	
		    <!--- Catch des erreurs --->
		    <cfcatch>
			    <cfdump var="#CFCATCH#">
		    </cfcatch>
		    </cftry>
	    
	    </cfif>
		
		
		<!--- SI LE FORMULAIRE DE RENVOI DE MAIL A ETE SOUMIS --->
	    <cfif isDefined("FORM.renvoiEmail") && FORM.renvoiEmail eq "Renvoyer le mail" >
			<cfset btnCliked = 4>
		</cfif>
		

		<!--- SI URL DE CONFIRMATION DENVOI DE MAIL A ETE CRÉEE --->
	    <cfif isDefined("refParam.sendSMSAfterEmail") && btnCliked neq 1 && btnCliked neq 2 && btnCliked neq 3 && btnCliked neq 4>
		    <cfset sous_tete = refParam.sous_tete>
			<cfset btnCliked = 5>
		</cfif>
		
		<cfset login = objLogin.getIndexLogin(btnCliked,varCnx,sous_tete,type,1)>
		
			<cfoutput>
				<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
					#head#
					<body id="index" onload="cloneTel();disableLoadsablier();">
						<div class="ombre" id="ombre">
						    <div class="background_ecran">
						        <img class="loading_image" id="imgLoading1" src="img/loading-icon.gif" />
						    </div>
						</div>
						<div id="global">
							#login#
						</div>
					</body>
				</html>
			</cfoutput>
			
	</cffunction>

</cfcomponent>