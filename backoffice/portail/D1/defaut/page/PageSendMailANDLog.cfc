<cfcomponent implements="portail.class.specialClass.InterfacePage"
			 output="false" 
			 displayname="Page" 
			 hint="Classe qui s'occupe de l'envoi de mail / template D1" >

	<cffunction name="afficher" access="public" output="true" 
				displayname="" 
				hint="Page spéciale envoie de mail / template D1" >
		<cfargument name="head" required="true" type="string" displayname="" hint="Contenu de la balise header de la page à afficher">
		<cfargument name="context" required="true" type="struct" displayname="" hint="Structure contenant les données passés dans l'URL"> <!--- le contexte = ref --->
		
		<cfset objLogin = createObject("component","portail.D1.defaut.composant.IndexLogin") >
		
		
		<!--- CREATION DUNE STRUC ET RECUPERATION DES DONNEES PASSEES EN URL DANS CETTE STRUCTURE --->
		<cfset refParam = createObject("component","portail.class.specialClass.AbstractComposant").getInfoSructURL(context) >
		
		
		<!--- initialisation variables pour parametres fonction logLastAction--->
		<cfset idTerminal = refParam.idTerminal >
		<cfset idSIM = refParam.idSIM >
		<cfset idSous_tete = refParam.idSous_tete >
		<cfset pageRetour = refParam.pageRetour >
		<cfset idPage = refParam.idPage >
		<cfset sous_tete = refParam.sous_tete >
		
		<!--- STRUCTURE POUR URL --->
		<cfset struct = {idPage = "#refParam.idPage#",
						sous_tete = "#refParam.sous_tete#",
						idSous_tete = "#refParam.idSous_tete#"
		} >
		
		<!--- VERIFICATION DES DONNÉES LORS DU SUBMIT ET REDIRECTION ÉVENTUELLE --->
		<cfif isDefined("FORM.sendEmailToGest") && FORM.sendEmailToGest eq "Envoyer" >

			<!--- envoi email --->
			<cfset createObject("component","portail.class.classData.MailService").sendEmailGest(FORM.formExp,FORM.obj,FORM.dest,FORM.exped,FORM.email) >

			<!--- logger --->
			<cfset UID_ACTION = createUUID()>
			<cfset logger = createObject("component","portail.class.classData.LoggerService").logLastAction(SESSION.USER.CLIENTACCESSID,31,"1",FORM.dest,FORM.obj,idTerminal,idSIM,SESSION.IDEMPLOYE,idSous_tete,UID_ACTION) >
			
			<cfset objLogin.goToPage(pageRetour,struct) >
		
		<!--- ---------------------------------------------------------- --->
		<!--- RETOUR INDEX, PENSER À SUPPRIMER LES VARIABLES DE SESSIONS --->
		<!--- ---------------------------------------------------------- --->
		<cfelse>
		
			<cfset objLogin.goToPage(0,struct) >
			
		</cfif>
	</cffunction>

</cfcomponent>