<cfcomponent implements="portail.class.specialClass.InterfacePage"
			 output="false"
			 displayname="Page" 
			 hint="Classe qui contient la structure de la page Mot de passe oublié dans le template D1" >
	
	<!---  --->
 	<cffunction name="afficher" access="public" output="true" 
				displayname="" 
				hint="Affiche les colmposants de la page de Mot de passe oublié / template D1" >
		<cfargument name="head" required="true" type="string" displayname="" hint="Contenu de la balise header de la page à afficher">
		<cfargument name="context" required="true" type="struct" displayname="" hint="Structure contenant les données passés dans l'URL">
		
		<cfset objLogOublie = createObject("component","portail.D1.defaut.composant.TableLogOublie") >
		<cfset bool = 2 >
		<cfset redirection = "" >
		<cfset refParam = structNew() >
		
		
		<cfif isDefined("FORM.login") && #FORM.login# neq "" >
			<cfset bool = objLogOublie.checkUserLogin("#FORM.login#") >
			
			<cfif #bool# eq 1 >
				<cfset redirection = '<meta http-equiv="Refresh" content="5;URL=index.cfm">'>
			</cfif>
		</cfif>
		
		<cfset logOublie = objLogOublie.getTableLogOublie(#bool#) >
		
		<cfoutput>
			<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
				#head#
				#redirection# 
				<body id="index">
					<div id="global">
						#logOublie#
					</div>
				</body>
			</html>
		</cfoutput>
	</cffunction>

</cfcomponent>