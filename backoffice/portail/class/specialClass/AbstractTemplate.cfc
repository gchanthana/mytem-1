<cfcomponent colddoc:abstract="true"
			name="AbstractTemplate" 
			displayname="AbstractTemplate" 
			hint="Contient des méthodes utilisés dans les classes de Template" > 
			
	
	<cfset typeIphone = "iphone">
	<cfset typeHTC = "htc">
	
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returntype="void">
		
		<cfreturn THIS>
	</cffunction>
	
	
	<!--- INSTANCIE LA CLASSE DE LA BONNE PAGE --->
	<cffunction name="getPage" access="public" output="false" returntype="string" 
				displayname="Méthode abstraite"
				hint="Méthode abstraite" >
		<cfargument name="context" required="true" type="struct" displayname="" hint="Structure contenant les données passés dans l'URL">
		<!--- methode abstraite - no implementation --->
	</cffunction>
	
	
	<!--- RECUPERE LE BON FICHIER CSS SELON LE DEVICE ET LE CLIENT --->
	<cffunction name="getCss" access="public" output="false" returntype="string" 
				displayname="Méthode implémenté" 
				hint="Récupère le bon fichier CSS selon le template et éventuellement le device">
		<cfargument name="template" required="true" type="string" displayname="" hint="Nom du Template">
		<!--- <cfargument name="idClient" required="false" type="string" displayname="" hint="Cet ID est unique"> --->
		<cfset clientId = "defaut" >
		
		<!--- CHOIX DU FICHIER .CSS SELON ID DU CLIENT QUI SE CONNECTE / EST CONNECTÉ --->
		<cfif isDefined("SESSION.USER.CLIENTID")><!--- si client existe --->
			<cfset clientId = #SESSION.USER.CLIENTID# > <!--- clientId du user connecté --->
		</cfif>
		
		<cfif NOT DirectoryExists("portail/#template#/#clientId#")><!--- si le clientId existe mais n'a pas de template --->
			<cfset clientId = "defaut" > <!--- on lui donne le template par defaut --->
		</cfif>
		
		<cfif #template# eq "D2">
			<cfset setTypeSmartphone = detectTypeSmartphone() />
			<cfset cssName = #template#&"_"&#setTypeSmartphone#&"_"&#clientId# >
		<cfelse>
			<cfset cssName = #template#&"_"&#clientId# >
		</cfif>
		
		
		<cfreturn cssName >
	</cffunction>
	
	
	<!--- UNIQUEMENT POUR TEMPLATE D2 -> DETECTE LE TYPE DE SMARTPHOEN POUR RECUPERER LE BON FICHIER CSS --->
	<cffunction name="detectTypeSmartphone" access="private" output="false" returntype="string"
				displayname="Méthode implémenté" 
				hint="Uniquement pour template D2 - Détecte le type de smartphone(utilisé lors de l'appel à la methode GetCss())">
		<!---Verifie si la valeur comparée se retrouve dans la variable useragent--->
		<cfif REFindNoCase("#typeIphone#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 >
			<cfreturn #typeIphone# >
		</cfif>
		
		<cfif REFindNoCase("#typeHTC#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 > 
			<cfreturn #typeHTC# >
		</cfif>
			
	</cffunction>
	
	
	<!--- AFFICHE LES BALISE HEAD DANS CHAQUE PAGE --->
 	<cffunction name="getHead" access="public" output="false" returntype="string"
				displayname="Méthode implémenté"
				hint="Génère le code dans la balise Head pour selobn le type de template" >
		<cfargument name="cssName" required="true" type="string" displayname="" hint="Nom du fichier CSS">
		<cfargument name="template" required="true" type="string" displayname="" hint="Nom du Template">
		
		<cfset selectCustom = "">
		<cfif #template# eq "D1">
			<cfset selectCustom = ' 
					
					
					<!--- SELECTMENU --->
					<link type="text/css" href="css/selectMenu/jquery.ui.selectmenu.css" rel="Stylesheet">
					<link type="text/css" href="css/selectMenu/jquery-ui.css" rel="Stylesheet">
					
					<!--- JQUERY --->
					<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
					<script src="js/jquery-1.7.2.min.js"></script>
					
					<script src="js/selectMenu/jquery.ui.core.js"></script>
					<script src="js/selectMenu/jquery.ui.widget.js"></script>
					<script src="js/selectMenu/jquery.ui.position.js"></script>
					<script src="js/selectMenu/jquery.ui.menu.js"></script>
					<script src="js/selectMenu/jquery.ui.selectmenu.js"></script>
					
					
					<!--- TOOLTIP --->
					<script type="text/javascript" src="js/tooltip/tooltip.js"></script>
					<link rel="stylesheet" type="text/css" media="all" href="css/tooltip/tooltip.css" />
					
					<!--- DATATABLES --->
					<script language="javascript" type="text/javascript" src="js/datatable/jquery.dataTables.min.js"></script>
					<!--- sort / tri --->
					<script language="javascript" type="text/javascript" src="js/datatable/sort/dateSort.js"></script>
					<script language="javascript" type="text/javascript" src="js/datatable/sort/dateSortTypeDetect.js"></script>
					<script type="text/javascript" src="js/datatable/sort/numericCommaSort.js"></script>
					<script type="text/javascript" src="js/datatable/sort/numericCommaTypeDetect.js"></script>
					
					<link rel="stylesheet" type="text/css" media="all" href="css/datatable/demo_table.css" />
					<link rel="stylesheet" type="text/css" media="all" href="css/datatable/demo_page.css" />
					
					<!--- SCRIPT ADDITIONNEL (select+datatable)--->
					<script type="text/javascript" src="js/D1customFunctions.js"></script>
					
			'>
		</cfif>
		
		<cfset metaviewport = "">
		<cfif #template# eq "D2">
			<!--- 	POURQUOI LE VIEWPORT ?
			Éviter le redimensionnement automatique de la mise en page, 
			qui rend les contenus trop petits, de fixer la largeur du mobile 
			et de pouvoir s’y adapter par la suite. 
			Cette instruction court-circuite le viewport par défaut, 
			souvent bien trop large, et fournit une base commune plus proche de la réalité
			Fixer le viewport impose au smartphone de disposer d'une résolution "normale"(= largeur de son écran) dès le départ,
			puis de s'y adapter. Sinon, cette résolution sera entre 800px et 1024px, donc inutilisable sur mobile
			--->
			<!--- <cfset metaviewport = '<meta name="viewport" content="width=device-width" /> '> --->
			<cfset metaviewport = '
			<meta name="viewport" content="width=device-width, initial-scale=1">
			
			<link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.css" />
			<!--- <link rel="stylesheet" href="css/cssBrowserDevice/D2_Theme_SAAS_Mobile.min.css" /> --->
			<!--- <link rel="stylesheet" href="css/cssBrowserDevice/D2_Theme_Default.css" /> --->
			<!--- <link rel="stylesheet" href="css/cssBrowserDevice/D2_Theme_Without_A.css" /> --->
			<script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
			<script src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script> '>
		</cfif>
		
		<cfset head = "" >
		<cfset head = ' <head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						#metaviewport#
					    #selectCustom#
					    <script language="javascript" type="text/javascript" src="js/'&#template#&'functions.js"></script>
					    <link rel="shortcut icon" type="image/gif" href="img/saaswedo_ico.gif">
					    <title>Votre Portail Utilisateur</title>'
		/>
		
		<cfset head &= '<link rel="stylesheet" media="all" href="css/cssBrowserDevice/'&#cssName#&'.css" />' >
		<!--- CORRECTIF IE --->
		<cfset head &= '<!--[if IE]>
						  <link rel="stylesheet" media="all" href="css/cssBrowserDevice/'&#cssName#&'_IE.css" />
						  <link rel="shortcut icon" type="image/x-icon" href="img/saaswedo_ico.ico">
						<![endif]-->
		' >
		<cfset head	&= '</head>' >
		
		<cfreturn head >
	</cffunction>
	
</cfcomponent>


<!--- 
if ($(''##dataTables_paginate span a'').length == 1){
    $(''##dataTables_paginate'').hide();
}
if ($(''.dataTable tbody tr'').length == 1){
    $(''##dataTables_info'').hide();
}
if ($(''.dataTable tbody tr'').length <= 10){
    $(''##dataTables_length'').hide();
 } 
--->
<!--- 
<link rel="stylesheet" type="text/css" media="all" href="css/dialog/test.css" />
<link rel="stylesheet" type="text/css" media="all" href="css/dialog/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" media="all" href="css/dialog/jquery.ui.base.css" />
<link rel="stylesheet" type="text/css" media="all" href="css/dialog/jquery.ui.theme.css" />

<script language="javascript" type="text/javascript" src="js/dialog/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="js/dialog/jquery.bgiframe-2.1.2.js"></script> 
<script language="javascript" type="text/javascript" src="js/dialog/jquery.ui.core.js"></script>
<script language="javascript" type="text/javascript" src="js/dialog/jquery.ui.widget.js"></script>
<script language="javascript" type="text/javascript" src="js/dialog/jquery.ui.mouse.js"></script> 
<script language="javascript" type="text/javascript" src="js/dialog/jquery.ui.selectmenu.js"></script> 
<script language="javascript" type="text/javascript" src="js/dialog/jquery.ui.button.js"></script>
<script language="javascript" type="text/javascript" src="js/dialog/jquery.ui.draggable.js"></script>
<script language="javascript" type="text/javascript" src="js/dialog/jquery.ui.position.js"></script> 
<script language="javascript" type="text/javascript" src="js/dialog/jquery.ui.resizable.js"></script>
<script language="javascript" type="text/javascript" src="js/dialog/jquery.ui.dialog.js"></script> 
<script language="javascript" type="text/javascript" src="js/dialog/jquery.effects.core.js"></script> --->