<cfcomponent output="false"
			 displayname="Récupération du pool" 
			 hint="Classe utilisé pour récuperé le pool du collaborateur" >
	
	
	<!--- LA LISTE DES POOL DE GESTION D'UN GESTIONNAIRE DANS UNE RACINE EN FONCTION DU SEGMENT --->
	<cffunction name="fournirListePoolsDuGestionnaire" access="remote" returntype="string" 
				description="Le premier idPool de la liste des pools de gestionnaire du gestionnaire"
				displayname="Retourne idPool du collaborateur"
				hint="Retourne ID_POOL" >	
		<cfargument name="idgestionnaire" 	type="numeric" required="false">
		<cfargument name="idracine" 		type="numeric" required="false">
		
		<cfif isdefined("idgestionnaire")>
			<cfset _idgestionnaire= idgestionnaire>
		<cfelse>
			<cfset _idgestionnaire= SESSION.USER.CLIENTACCESSID>
		</cfif>
		
		<cfif isdefined("idracine")>		
			<cfset _idracine = idracine>
			<cfelse>
			<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
		</cfif>
		
		<cfstoredproc 	datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getPool_of_gestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idgestionnaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idracine#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfset idPool = p_result['IDPOOL'][1] >
		
		<cfreturn idPool>
	</cffunction>
	
</cfcomponent>