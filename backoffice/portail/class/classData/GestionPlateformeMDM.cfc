<cfcomponent  output="false"  
			displayname="GestionPlateformeMDM" 
			hint="Classe permettant la gestion des fonctions MDM selon la plateforme du terminal ">
	
	
	<cffunction name="gestionPlateformeMDM" access="public" returntype="any" output="true" hint="affiche et gere les fonctions du MDM selon la plateforme du terminal">
		<cfargument name="plateforme"	 required="true" type="string"/>
		<cfargument name="boolIsManaged" required="true" type="numeric"/>
		<cfargument name="class"	 required="true" type="string"/>
		<cfargument name="btnType1" required="true" type="numeric"/>
		<cfargument name="disabled"	 required="true" type="string"/>
		<cfargument name="btnType2" required="true" type="numeric"/>
		<cfargument name="changeCodeLock" required="true" type="numeric"/>
			
			
		
		<!--- MES BOUTONS ACTIONS --->
		<cfset action &= '
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input type="#btnType1#" #disabled# name="btnLockTerminal" value="Verrouiller mon terminal" class="#class#" onclick="#lock#">
						</form>
					</div>
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input type="#btnType2#" #disabled# name="btnChangeLockTerminal" value="Changer le code verrou" class="#class#" onclick="#changeCodeLock#">
						</form>
					</div>
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input type="#btnType2#" #disabled# name="" onclick="popupWipeAll()" value="Effacer TOUTES les données" class="#class#" >
						</form>
					</div>
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input type="#btnType2#" #disabled# name="" onclick="popupWipePartiel()" value="Effacer les données Entreprise" class="#class#" >
						</form>
					</div>
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input type="#btnType2#" #disabled# name="" onclick="popupRevok()" value="Revoquer mon terminal" class="#class#" >
						</form>
					</div>
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input type="button" name="" onclick="popupEmail(''ombreee'')" value="Contacter mon gestionnaire" class="btnContacterGestionnaireTerminal" >
						</form>
					</div>
		'>			
			
			
			
		<cfreturn action>	
	</cffunction>
	
	
</cfcomponent>