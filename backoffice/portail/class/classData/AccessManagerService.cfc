<cfcomponent output="false" 
			 displayname="DataService" 
			 hint="Classe regroupant les methodes récupérant les données concernant l'utilisateur ">
	
	
	<cfset THIS.AUTH_DSN = "ROCOFFRE">
	<cfset THIS.PKG_GLOBAL = "PKG_CV_GLOBAL">
	<cfset THIS.DEFAULT_UNIVERS ="HOME">
	<cfset THIS.CONNECT_CLIENT ="#THIS.PKG_GLOBAL#.connectClient_v2">
	
	
	
	<!--- VERIFIE LES INFORMATIONS SAISIES ET RENVOI UN BOOLEEN --->
	<cffunction name="checkUserIdentByEmail" access="public" output="false" returnType="numeric"
				displayname="" 
				hint="Vérifie l'existence des identifiants dans la BDD pour la connexion par mail / template D1" >
		<cfargument name="loginValue" required="true" type="string">
		<cfargument name="pwdValue" required="true" type="string">
		<cfargument name="codeAppp" required="true" type="numeric" default="1" >
		
		<cfset valid = createObject("component","fr.consotel.consoview.access.AccessManager").validateLogin(loginValue,pwdValue,1) />	
		
		<!--- SI IDENTIFICATION EST OK --->
		<cfif isDefined("valid.AUTH_RESULT") && valid.AUTH_RESULT eq 1 >
			<cfset SESSION.AUTH_RESULT = valid.AUTH_RESULT >
			
			<cfset SESSION.TYPECNX = 1><!--- connexion avec email --->
			
			<!--- à voir si on peut supprimer --->
			<cfset listPerimetre = createObject("component","portail.class.classData.AccessManagerService").connectOnGroup(valid.CLIENTACCESSID,valid.CLIENTID,1) />
			
			<!--- récupération des booléens MDM (RACINE, USER ET TERMINAL) --->
			<cfset aInfos = createObject("component","fr.consotel.consoview.M111.UserAccessService").getModuleUserAccess()>
			<cfset SESSION.BOOL_MDM_USER = aInfos[1]["IS_ACTIF"]>
			<cfset SESSION.BOOL_MDM_CODEPARAMS = aInfos[1]["CODE_PARAMS"]>
			<cfset SESSION.BOOL_MDM_RAC = aInfos[2]["BOOL_MDM"]>
			
			
			<!--- gestion de droits acces aux onglets --->
			<cfset gstDroits = createObject("component","portail.class.classData.GestionDroitService").gestionDroits(SESSION.USER.CLIENTACCESSID,SESSION.USER.CLIENTID) >
			<!--- <cfdump var="#gstDroits#"> <cfdump var="#SESSION.BOOL_MDM_RAC#"> <cfabort> --->
			
			<!--- récupération de l'id_pool du collaborateur --->
			<cfset pool = createObject("component","fr.consotel.consoview.M16.v2.PoolService").fournirListePoolsDuGestionnaire(SESSION.USER.CLIENTACCESSID,SESSION.USER.CLIENTID) >
			<cfset  SESSION.IDPOOL = val(pool['IDPOOL'][1])>
			<!--- <cfset idPool = createObject("component","portail.class.classData.PoolService").getIdPool(#valid.CLIENTACCESSID#)>
			<cfset SESSION.IDPOOL = idPool> --->
			
			<!--- initialisation variable session pour methode get_produit(M311) --->
			<cfset SESSION.PERIMETRE.SELECTED_NIVEAU = "IDSOUS_TETE" >
			
			<!--- pour supprimer les éventuelles informations d'un utilisateur précédant(qui ne s'est pas délogué) qui etaient dans le SESSION.UUID --->
			<cfset objSess = createObject("component","portail.class.classData.SessionService").getInstance()>
			<cfset objSess.suppStructUUID(SESSION.UUID) >
			<cfreturn 1 ><!--- CNX OK ---> 
		<cfelse>
			<cfreturn 0 ><!--- IDENTIFIANT ERRONÉS --->
		</cfif>
		
	</cffunction>
	
	
	<!--- VERIFIE LES INFORMATIONS SAISIES ET RENVOI UN BOOLEEN --->
	<cffunction name="checkUserIdentByLigne" access="public" output="false" returnType="any"
				displayname="" 
				hint="Vérifie l'existence des identifiants dans la BDD pour la connexion par numero de ligne / template D1" >
		<cfargument name="loginValue" required="true" type="string">
		<cfargument name="pwdValue" required="true" type="string">
		<cfargument name="codeApp" required="true" type="string" defaut="1" >
				
		
		<cfswitch expression="#codeApp#">
			 
			<cfcase value="2">
				<cfset THIS.AUTH_DSN = "MBP_MERCURE">
				<cfset SESSION.OffreDSN = "MBP_MERCURE">
				<cfset SESSION.CODEAPPLICATION = 2 >
				<cfset COOKIE.CODEAPPLICATION = 2 >
				<cfset THIS.CONNECT_CLIENT = "offre.pkg_connection_appli.connectClient">
				
			</cfcase>
			
			<cfcase value="3"> 
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 3>
				<cfset COOKIE.CODEAPPLICATION = 3>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfcase>
			
			<cfcase value="51"> 
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 51>
				<cfset COOKIE.CODEAPPLICATION = 51>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfcase>
			
			<cfdefaultcase>
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 1>
				<cfset COOKIE.CODEAPPLICATION = 1>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfdefaultcase>
			
		</cfswitch>
		
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_PORTAIL_END_USER.connectLine">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_ligne" 	value="#loginValue#">   
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_pwd" 	value="#pwdValue#">                                                 	
           	<cfprocresult name="p_result">
		</cfstoredproc>
		
		
		<cfset SESSION.TYPECNX = 2> <!--- connexion avec numero ligne --->
		<cfset SESSION.NUMEROLIGNE = loginValue>
		<cfset SESSION.AUTH_RESULT = p_result['AUTH'][1]>
		<cfset SESSION.IDPOOL = p_result['IDPOOL'][1]>
		<cfset SESSION.BOOL_MDM_RAC = p_result['BOOL_MDM_RAC'][1]>
		<cfset SESSION.BOOL_MDM_USER = p_result['BOOL_MDM_USER'][1]>
		<cfset SESSION.USER.EMAIL = p_result['EMAIL'][1]>
		<cfset SESSION.USER.CLIENTACCESSID = p_result['CLIENTACCESID'][1]>
		<cfset SESSION.USER.CLIENTID = p_result['ID'][1]>						
		<cfset SESSION.USER.PRENOM = p_result['PRENOM'][1]>
		<cfset SESSION.USER.NOM = p_result['NOM'][1]>
		<cfset SESSION.USER.LIBELLE = p_result['LIBELLE'][1]>
		
		<cfset SESSION.USER.IDTYPEPROFIL = 1>
		<cfset SESSION.USER.GLOBALIZATION = "fr_FR">
		<cfset SESSION.USER.IDGLOBALIZATION = 3>
		<cfset SESSION.USER.IDREVENDEUR = "">
		
		<!--- à voir si on peut supprimer --->
		<!--- <cfset listPerimetre = createObject("component","portail.class.classData.AccessManagerService").connectOnGroup(val(p_result['CLIENTACCESID'][1]),val(p_result['ID'][1]),1) /> --->
		<cfset createObject("component","portail.class.classData.AccessManagerService").connectOnGroup(val(p_result['CLIENTACCESID'][1]),val(p_result['ID'][1]),1) />
		
		<!--- initialisation variable session pour methode get_produit(M311) --->
		<cfset SESSION.PERIMETRE.SELECTED_NIVEAU = "IDSOUS_TETE" >
		
		<!--- gestion de droits acces aux onglets --->
		<cfset gstDroits = createObject("component","portail.class.classData.GestionDroitService").gestionDroits(val(SESSION.USER.CLIENTACCESSID),val(SESSION.USER.CLIENTID)) >
		
		
		<!--- pour supprimer les éventuelles informations d'un utilisateur précédant(qui ne s'est pas délogué) qui etaient dans le SESSION.UUID --->
		<cfset objSess = createObject("component","portail.class.classData.SessionService").getInstance()>
		<cfset objSess.suppStructUUID(SESSION.UUID) >


		<cfreturn p_result>
		
	</cffunction>
	
	
	<!---  --->
	<cffunction name="verifMDPEmail" access="public" output="false" returnType="any"
				displayname="" 
					hint="Permet de verifier le type de connexion email ou numero de ligne selon valeur de retour / template D1" >
		<cfargument name="loginValue" required="true" type="string" >
		<cfargument name="pwdValue" required="true" type="string">
		
		<cfset SESSION.OffreDSN = "ROCOFFRE">
		
		<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.CONNECT_CLIENT#">
			<cfprocparam type="in" value="#loginValue#" cfsqltype="CF_SQL_VARCHAR" >
			<cfprocparam type="in" value="#pwdValue#" 	cfsqltype="CF_SQL_VARCHAR" >
			<cfprocparam type="out"	variable="p_retour" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qValidateLogin">
		</cfstoredproc>

		<cfreturn #p_retour#>
				
	</cffunction>
	
	
	<!---  --->
	<cffunction name="verifMDPLigne" access="public" output="false" returnType="any"
				displayname="" 
				hint="Permet de verifier le type de connexion email ou numero de ligne selon valeur de retour / template D1" >
		<cfargument name="loginValue" required="true" type="string" >
		<cfargument name="pwdValue" required="true" type="string" >
		
		<cfset SESSION.OffreDSN = "ROCOFFRE">
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_PORTAIL_END_USER.connectLine">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_ligne" 	value="#loginValue#">   
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_pwd" 	value="#pwdValue#">                                                 	
	        <cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn #p_result#>
				
	</cffunction>
	
	
	<!---  --->
	<cffunction name="createUserAccesByLigne" access="public" output="false" returnType="any"
				displayname="" 
				hint="Creation d'un acces pour un nouveau user possedant une ligne non répertoriée dans l'annuaire / template D1" >
		<cfargument name="numero" required="true" type="string" >
		<cfargument name="nom" required="true" type="string" >
		<cfargument name="prenom" required="true" type="string" >
		<cfargument name="email" required="true" type="string">
		<cfargument name="uuid" required="true" type="string">
		
		<cfset SESSION.OffreDSN = "ROCOFFRE">
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_PORTAIL_END_USER.createUserAccess">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_numero" 	value="#numero#">   
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_nom" 	value="#nom#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_prenom" 	value="#prenom#">   
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_email" 	value="#email#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_uuid" 	value="#uuid#">
           	<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER"  variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
				
	</cffunction>
	
	
	<!---  --->
	<cffunction name="getLineCode" access="public" output="false" returnType="string"
				displayname="" 
				hint="Récupérer le code affilié à la ligne d'un user dans l'annuaire grace au numéro de la ligne / template D1" >
		<cfargument name="numero" required="true" type="numeric" >
		
		<cfset SESSION.OffreDSN = "ROCOFFRE">
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_PORTAIL_END_USER.getLineCode">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_numero" 	value="#numero#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfset p_retour = #p_result["PASSWORD"]#>
		
		<cfreturn p_retour>
	</cffunction>
	
	
	<!---  --->
	<!--- RECUPERER LE NUMERO DE LIGNE AVEC UUID PASSE DANS LE LIEN DU MAIL PREMIERE CONNEXION --->
	<cffunction name="getLineFromUuid" access="public" output="false" returnType="any"
				displayname="" 
				hint="Récupérer la sous_tete affilié à la ligne d'un user dans l'annuaire grace à l'UUID / template D1" >
		<cfargument name="uuid" required="true" type="string">
		
		<cfset structRetour = structNew()>
		<cfset SESSION.OffreDSN = "ROCOFFRE">
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_PORTAIL_END_USER.getLineFromUuid">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_uuid" 	value="#uuid#">
           	<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfset sous_tete = #p_result["LIGNE"]#>
		
		<cfif sous_tete neq "">
			<cfset structRetour = createObject("component","portail.class.specialClass.AbstractComposant").createURLWithLienMail(uuid,sous_tete) >
		</cfif>
		
		<cfreturn structRetour>
				
	</cffunction>
	

	<!--- LE CODE COMMENTÉ EST INUTILE --->
	<cffunction name="connectOnGroup" access="remote" output="false" returntype="struct">
		
		<cfargument name="accessId" required="true" type="numeric">
		<cfargument name="groupId" required="true" type="numeric">
		<cfargument name="typelogin" required="false" type="numeric" default="-1">
		<cflog text="connectOnGroup : typelogin = #typelogin#">
		
		<cfset groupInfos=getGroupInfos(groupId)>
		<cfset structDelete(SESSION,"PERIMETRE")>
		<cfset SESSION.PERIMETRE = structNew()>
		<cfset SESSION.PERIMETRE.ID_GROUPE=groupInfos['IDGROUPE_CLIENT'][1]>
		<cfset SESSION.PERIMETRE.GROUPE=groupInfos['LIBELLE_GROUPE_CLIENT'][1]>
		<cfset SESSION.PERIMETRE.IDCLIENT_PV=groupInfos['IDCLIENTS_PV'][1]>
		<cfset SESSION.PERIMETRE.IDRACINE_MASTER=groupInfos['IDRACINE_MASTER'][1]>
		<cfif typelogin gt -1>
			<cfset SESSION.PERIMETRE.TYPE_LOGIN = typelogin>
		<cfelse>
			<cfset SESSION.PERIMETRE.TYPE_LOGIN = 1>
		</cfif>
		
		<cfquery name="qGetBoolInventaire" datasource="#SESSION.OFFREDSN#">
		SELECT 0 AS isCycleVieInitialized
		FROM DUAL
		</cfquery>
		<cfset SESSION.PERIMETRE.BOOLINVENTAIRE = qGetBoolInventaire['isCycleVieInitialized'][1]>
		<cfset groupStructData=structNew()>
		<cfset groupStructData.BOOLINVENTAIRE = SESSION.PERIMETRE.BOOLINVENTAIRE>
		<!--- <cfset groupStructData.PERIMETRE_DATA=buildGroupXmlTree(accessId,groupId)> --->
		
		<cfset setPerimetre(val(#SESSION.USER.CLIENTACCESSID#),val(#SESSION.USER.CLIENTID#)) > <!--- accessId,accessNodeId --->
	
		<cflog text="SESSION.PERIMETRE.ID_GROUPE = #SESSION.PERIMETRE.ID_GROUPE#">
		
		
		<cfreturn groupStructData>
		
	</cffunction>
	
	
	<!---  --->
	<cffunction name="getGroupInfos" access="private" returntype="query">
		<cfargument name="groupId" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="#THIS.PKG_GLOBAL#.GET_GROUPE_INFO">
			<cfprocparam  value="#groupId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetGroupInfos">
		</cfstoredproc>
		<cfreturn qGetGroupInfos>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="setPerimetre" access="private" returntype="struct">
		<cfargument name="accessId" required="true" type="numeric">
		<cfargument name="idGroupeClient" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="#THIS.PKG_GLOBAL#.detail_noeud_v2">
			<cfprocparam  value="#idGroupeClient#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#accessId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNodeInfos">
		</cfstoredproc>
		<cfset perimetreStruct=structNew()>
		<cfset perimetreStruct.PERIMETRE_INFOS_STATUS=qGetNodeInfos.recordcount>
		<cfif qGetNodeInfos.recordcount EQ 1>
			<cfset SESSION.PERIMETRE.ID_PERIMETRE=qGetNodeInfos['IDGROUPE_CLIENT'][1]>
			<cfset SESSION.PERIMETRE.RAISON_SOCIALE=qGetNodeInfos['LIBELLE_GROUPE_CLIENT'][1]>
			
			<cfif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPE">			
				<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="Groupe">
			<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPELIGNE">			
				<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="GroupeLigne">
			<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "SOUSTETE">			
				<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="SousTete">
			<cfelse>
				<cfset SESSION.PERIMETRE.TYPE_PERIMETRE=qGetNodeInfos['TYPE_PERIMETRE'][1]>
			</cfif>
			
			<cfset perimetreStruct.ID_PERIMETRE=qGetNodeInfos['IDGROUPE_CLIENT'][1]>
			<cfset perimetreStruct.RAISON_SOCIALE=qGetNodeInfos['LIBELLE_GROUPE_CLIENT'][1]>
			<cfset perimetreStruct.OPERATEURID=qGetNodeInfos['OPERATEURID'][1]>
			
			<cfif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPE">
				<cfset perimetreStruct.TYPE_PERIMETRE="Groupe">
			<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPELIGNE">
				<cfset perimetreStruct.TYPE_PERIMETRE="GroupeLigne">
			<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "SOUSTETE">			
				<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="SousTete">
			<cfelse>
				<cfset perimetreStruct.TYPE_PERIMETRE=qGetNodeInfos['TYPE_PERIMETRE'][1]>
			</cfif>
			
			
			<cfset perimetreStruct.GROUP_CODE_STYLE=qGetNodeInfos['CODE_STYLE'][1]>
			<cfcookie name="CODE_STYLE" value="#perimetreStruct.GROUP_CODE_STYLE#" domain="consotel.fr" expires="never">
			
			
			
			<cfif idGroupeClient EQ SESSION.PERIMETRE.ID_GROUPE>
				<cfset perimetreStruct.TYPE_LOGIQUE="ROOT">
			<cfelse>
				<cfset perimetreStruct.TYPE_LOGIQUE=qGetNodeInfos['TYPE_ORGA'][1]>
			</cfif>
			<cfset SESSION.PERIMETRE.GROUP_CODE_STYLE=perimetreStruct.GROUP_CODE_STYLE>
			<!--- <cfset perimetreStruct.PERIODS=setPeriod(idGroupeClient)> --->
			<!--- <cfset perimetreStruct.ACCESS_LIST=setAccess(qGetNodeInfos)> --->
			<!--- <cfset perimetreStruct.DROIT_GESTION_FOURNIS=SESSION.PERIMETRE.DROIT_GESTION_FOURNIS> --->
		</cfif>
		<cfreturn perimetreStruct>
	</cffunction>

	
</cfcomponent>