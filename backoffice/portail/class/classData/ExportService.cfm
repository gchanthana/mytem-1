	<cfset xdo_path='/consoview/M331/EXPORT-CDR-ALL/EXPORT-CDR-ALL.xdo'/>
	
	<cfset url_logo= getCacheUrl(SESSION.CODEAPPLICATION)/>
	
	<cfset biServer = "http://developpement.consotel.fr/xmlpserver/services/PublicReportService?WSDL" > <!--- /services/PublicReportService?WSDL --->
	
	<cfset locale = "fr_FR"> <!--- SESSION.PARAMS_EXPORT.LANG --->
	
	
	<cfset idracine_master = session.perimetre.idracine_master>
	<cfset idracine = session.perimetre.id_groupe>
	<cfset format = "csv">
	
	<cfset dateDeb_fact = LSDateFormat(#FORM.date_deb#, "dd/mm/yyyy") >
	<cfset dateFin_fact = LSDateFormat(#FORM.date_fin#, "dd/mm/yyyy") >
	
	
	
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idracine_master">
	<cfset t=ArrayNew(1)>
	<cfset t[1]= idracine_master > <!--- SESSION.PARAMS_EXPORT.IDRACINE_MASTER --->
	<cfset ParamNameValue.values=t>	
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="g_idracine">
	<cfset t=ArrayNew(1)>
	<cfset t[1]= idracine > <!--- SESSION.PARAMS_EXPORT.IDRACINE --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[2]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idinventaire_periode">
	<cfset t=ArrayNew(1)>
	<cfset t[1]= #FORM.idInventaire_periode#> <!--- SESSION.PARAMS_EXPORT.ORDER_BY_TEXTE_COLONNE --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[3]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idsous_tete">
	<cfset t=ArrayNew(1)>
	<cfset t[1]= #FORM.idSousTete#> <!--- SESSION.PARAMS_EXPORT.ORDER_BY_TEXTE --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[4]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_order_by">
	<cfset t=ArrayNew(1)>
	<cfset t[1]= "DATE_APPEL"> <!--- SESSION.PARAMS_EXPORT.ORDER_BY_TEXTE_COLONNE --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[5]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_asc_desc">
	<cfset t=ArrayNew(1)>
	<cfset t[1]= "ASC" > <!--- SESSION.PARAMS_EXPORT.ORDER_BY_TEXTE --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[6]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_champs_filtre">
	<cfset t=ArrayNew(1)>
	<cfset t[1]= "DATE_APPEL"> <!--- SESSION.PARAMS_EXPORT.SEARCH_TEXT_COLONNE --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[7]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_valeur_filtre">
	<cfset t=ArrayNew(1)>
	<cfset t[1]= ""> <!--- SESSION.PARAMS_EXPORT.SEARCH_TEXT --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[8]=ParamNameValue>	
	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_index_debut">
	<cfset t=ArrayNew(1)>
	<cfset t[1]= 1> <!--- val(SESSION.PARAMS_EXPORT.OFFSET) --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[9]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_number_of_records">
	<cfset t=ArrayNew(1)>
	<cfset t[1]= -1> <!--- SESSION.PARAMS_EXPORT.LIMIT --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[10]=ParamNameValue>
	


	<cfset myParamReportRequest=structNew()>
	
	<cfset myParamReportRequest.reportAbsolutePath=xdo_path>
	
	<cfset myParamReportRequest.attributeTemplate="DEFAULT">
	
	<cfset myParamReportRequest.attributeLocale=locale>
	<cfset myParamReportRequest.attributeFormat=LCase(FORMAT)>
	<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
	<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
	<cfset myParamReportParameters=structNew()>
	<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
	
	<cfset myParamReportParameters.userID="consoview">
	<cfset myParamReportParameters.password="public">
	<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
	<cfset reporting=createObject("component","fr.consotel.consoview.api.Reporting")>

			
	<cfset filename = FORM.sous_tete&"-appels-du-"&dateDeb_fact&"-au-"&dateFin_fact&".csv" >
	
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#" charset="utf-8">
	<cfif compareNoCase(trim(FORMAT),"csv") eq 0>
		<cfcontent type="text/csv; charset=utf-8" variable="#resultRunReport.getReportBytes()#">
		<cfelse>
		<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">		
	</cfif>
	
	
	<!--- FUNCTIONS ================================================================================================================== --->
	<cffunction name="getCacheUrl" returntype="String" hint="Retourne l'URL du cache selon le code application">
		<cfargument name="code_application" type="string" required="true">
		<cfset url_cache = "https://cache.consotel.fr">
		<cfif structkeyexists(SESSION,"URL_CACHE_#code_application#")>
			<cfset url_cache = "URL_CACHE">
		</cfif>
		<cfset url_logo = url_cache &  '/assets/application-logo-#code_application#.jpg'>
		<cfreturn url_logo>
	</cffunction>