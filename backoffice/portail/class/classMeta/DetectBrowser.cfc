<cfcomponent output="false" 
			displayname="DetectBrowser" 
			hint="Classe utilisé lorsqu'un utilisateur se connecte au PORTAIL END USER">
	<cfset userAgent="" />
	<cfset httpAccept="" />
	<cfset engineGecko="gecko" />
	<cfset engineWebkit="ebkit" />
	<cfset enginePresto="presto" />
	
	
	<!---  --->
	<cffunction name="init" access="public" output="false" returnType="void"
				displayname="pseudo contructeur" 
				hint="Initialise le USERAGENT et le HTTPACCEPT en sesssion">
		<cfif isDefined("CGI.HTTP_USER_AGENT") and CGI.HTTP_USER_AGENT neq "" >
			<cfset APPLICATION.useragent = lcase(CGI.HTTP_USER_AGENT) /> <!--- Initialisation + lowercase --->
			<cfelse>
				<cfset useragent = "" />
		</cfif>
		<cfif isDefined("CGI.HTTP_ACCEPT") and CGI.HTTP_ACCEPT neq "" >
			<cfset APPLICATION.httpaccept = lcase(CGI.HTTP_ACCEPT) /> <!--- Initialisation + lowercase --->
			<cfelse>
				<cfset httpaccept = "" />
		</cfif>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="detectBrowser" access="public" output="true" returnType="string"
				displayname="Retourne le nom du moteur de rendu du navigateur" 
				hint="Récupère le type de Browser de l'utilisateur qui s'est connecté au PORTAIL"  >
		<cfset setDetectGecko = detectModzilla() />
		<cfset setDetectWebkit = detectWebkit() />
		<cfset setDetectTrident = detectIE() />
		<cfset setDetectPresto = detectPresto() />
		<cfset scan = "" />
		
		<cfif (setDetectGecko eq true) >
			<cfset scan = "gecko">
		<cfelseif (setDetectTrident eq true) >
			<cfset scan = "trident">
		<cfelseif (setDetectWebkit eq true) >
			<cfset scan = "webkit">
		<cfelseif (setDetectPresto eq true) >
			<cfset scan = "presto">
		</cfif>
		
		<cfreturn scan>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="detectModzilla" access="public" output="false" returnType="boolean"
				displayname="" 
				hint="Détecte si l'utilisateur utilise un browser tournant sur Gecko(Modzilla FireFox)">
		<cfset retour = false>
		<!---Verifie si la valeur comparée se retrouve dans la variable useragent--->
		<cfif REFindNoCase("#engineGecko#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 > 
			<cfset retour = true>
		</cfif>
		<cfreturn retour>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="detectWebkit" access="public" output="false" returnType="boolean"
				displayname="" 
				hint="Détecte si l'utilisateur utilise un browser tournant sur Webkit(Safari, Chrome, ect)">
		<cfset retour = false>
		<!---Verifie si la valeur comparée se retrouve dans la variable useragent--->
		<cfif REFindNoCase("#engineWebkit#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 > 
			<cfset retour = true>
		</cfif>
		<cfreturn retour>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="detectPresto" access="public" output="false" returnType="boolean"
				displayname="" 
				hint="Détecte si l'utilisateur utilise un browser tournant sur Presto(Opera)">
		<cfset retour = false>
		<!---Verifie si la valeur comparée se retrouve dans la variable useragent--->
		<cfif REFindNoCase("#enginePresto#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 > 
			<cfset retour = true>
		</cfif>
		<cfreturn retour>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="detectIE" access="public" output="false" returnType="boolean"
				displayname="" 
				hint="Détecte si l'utilisateur utilise un browser tournant sur Trident(IE)">
		<cfset retour = false>
		<!---Verifie si la valeur comparée se retrouve dans la variable useragent--->
		<cfif REFindNoCase("MSIE 9.0","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 
				|| REFindNoCase("MSIE 8.0","#lcase(CGI.HTTP_USER_AGENT)#") gt 0
				|| REFindNoCase("MSIE 7.0","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 
				||REFindNoCase("MSIE 6.0","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 > 
			<cfset retour = true>
		</cfif>
		<cfreturn retour>
	</cffunction>
	
</cfcomponent>