<cfcomponent output="false" 
			displayname="DetectDevice" 
			hint="Classe utilisé lorsqu'un utilisateur se connecte au PORTAIL END USER">
	<cfset userAgent="" />
	<cfset httpAccept="" />
	<cfset deviceIpad ="ipad" />
	<cfset deviceIphone = "iphone" />
	<cfset devicePCWindows = "windows" />
	<cfset devicePCCompatible = "compatible" />
	<cfset deviceLinux = "linux" />
	<cfset deviceAndroid = "android" />
	<cfset deviceMAC = "macintosh">

	<!---  --->
	<cffunction name="init" access="public" output="false" returnType="void"
				displayname="pseudo contructeur" 
				hint="Initialise le USERAGENT et le HTTPACCEPT en sesssion">
		<cfif isDefined("CGI.HTTP_USER_AGENT") and CGI.HTTP_USER_AGENT neq "" >
			<cfset SESSION.useragent = lcase(CGI.HTTP_USER_AGENT) /> <!---Initialisation + lowercase--->
			<cfelse>
				<cfset SESSION.useragent = "" />
		</cfif>
		<cfif isDefined("CGI.HTTP_ACCEPT") and CGI.HTTP_ACCEPT neq "" >
			<cfset SESSION.httpaccept = lcase(CGI.HTTP_ACCEPT) /> <!---Initialisation + lowercase--->
			<cfelse>
				<cfset SESSION.httpaccept = "" />
		</cfif>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="detectDevice" access="public" output="true" returntype="string" 
				displayname="" 
				hint="Récupère le type de Device de l'utilisateur qui s'est connecté au PORTAIL">
		
		<cfset setDetectTablet = detectTablet() />
		<cfset setDetectSmartphone = detectSmartphone() />
		<cfset setDetectPC = detectPC() />
		<cfset scan = "D1" />
		
		<cfif (setDetectPC eq true) >
			<cfset scan = "D1">
		<cfelseif (setDetectSmartphone eq true) >
			<cfset scan = "D1" >
		<cfelseif (setDetectTablet eq true) >
			<cfset scan = "D1">
		</cfif>
		
		<cfreturn scan>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="detectTablet" access="private" output="false" returntype="boolean" 
				displayname="" 
				hint="Détecte si l'utilisateur est connecté sur un device Tablet">
		<cfset retour = false>
		<!---Verifie si la valeur comparée se retrouve dans la variable useragent--->
		<cfif REFindNoCase("#deviceIpad#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 > 
			<cfset retour = true>
		</cfif>
		<cfreturn retour>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="detectSmartphone" access="private" output="false" returntype="boolean" 
				displayname="" 
				hint="Détecte si l'utilisateur est connecté sur un device Smartphone">
		<cfset retour = false>
		<!---Verifie si la valeur comparée se retrouve dans la variable useragent--->
		<cfif REFindNoCase("#deviceIphone#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 
				|| (REFindNoCase("#deviceAndroid#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 && REFindNoCase("#deviceLinux#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0)> 
			<cfset retour = true>
		</cfif>
		<cfreturn retour>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="detectPC" access="private" output="false" returntype="boolean" 
				displayname="" 
				hint="Détecte si l'utilisateur est connecté sur un device PC/MAC">
		<cfset retour = false>
		
		<!---Verifie si la valeur comparée se retrouve dans la variable useragent--->
		<cfif REFindNoCase("#devicePCWindows#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 
				|| REFindNoCase("#devicePCCompatible#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 
				|| REFindNoCase("#deviceMAC#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 
				|| (REFindNoCase("#deviceLinux#","#lcase(CGI.HTTP_USER_AGENT)#") gt 0 && REFindNoCase("#deviceAndroid#","#lcase(CGI.HTTP_USER_AGENT)#") lte 0)> 
			<cfset retour = true>
		</cfif>
		<cfreturn retour>
	</cffunction>
	
</cfcomponent>