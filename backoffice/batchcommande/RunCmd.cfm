<cfset rq=createobject("component","ListQuery")>
<cfset qCommandes=rq.getData()>


<cfloop query="qCommandes">

<cfset idracine1=IDRACINE>
<cfset ref = NUMERO_OPERATION>
<cfset idtypecommande = IDTYPE_OPERATION>
<cfset numero = NUMERO_OPERATION>
<cfset libelle = LIBELLE>
<cfset ref_client = CODE_INTERNE>
<cfset ref_distributeur = REF_REVENDEUR>

<cfset IDCMDE = IDCOMMANDE>
<cfset IDGESTION = USER_CREATE>
<cfset racine = LIBELLE_GROUPE_CLIENT>
<cfset expediteurMail = MAIL_EXP>
<cfset expediteurNom = EXPEDITEUR>
<cfset destinataire = MAIL_DEST>
<cfset MSG = MESSAGE>

<cfset cc = ''>
<cfset bcc = 'monitoring@saaswedo.com'>
<cfset laDate = lsdateFormat(DATE_ENVOI,'DD/MM/YYYY')>
<cfset bcopie = 'YES'>

<cfset message1=
"
#expediteurNom# 
#expediteurMail#
Date : #laDate#
Societe : #racine#

Libelle de la demande #libelle#

Numero de la demande #numero#

Reference client : #ref_client#


Reference revendeur/operateur : #ref_distributeur#

#MSG#

Cordialement,
#expediteurNom#.">


<cfset mailObj = createObject("component","batchcommande.Mail")/>
<cfset mailObj.settype("text")/>
<cfset mailObj.setmodule("Commande Mobile")  />
<cfset mailObj.setmessage("#message#")  />
<cfset mailObj.setcharset("utf-8")   /> 
<cfset mailObj.setCc(cc)  />
<cfset mailObj.setBcc(bcc)  />
<cfset mailObj.setexpediteur(expediteurMail)  />
<cfset mailObj.setcopiePourExpediteur(bcopie)/>
<cfset mailObj.setrepondreA(expediteurMail)/>
<cfset mailObj.setDestinataire(destinataire)/>
<cfset mailObj.setSujet("Ref. : #ref_client#")/>
 
<cfset tab=arrayNew(1)>
<cfset arrayAppend(tab,IDCMDE)>
<cfset result = envoyer(mailObj,
		racine,
			tab,
				idtypecommande,
					IDGESTION,
						expediteurNom,
							destinataire,"")>
							
<cfoutput> -------------------------------------------------------</cfoutput><br/>
<cfoutput> --- traitement commande n°#numero# -- job id : #result#</cfoutput><br/>
<cfoutput> -------------------------------------------------------</cfoutput><br/>

</cfloop>


<cffunction name="envoyer" access="public" returntype="any">
		<cfargument name="mail" 				type="batchcommande.Mail" 	required="true">
		<cfargument name="racine" 				type="string" 							required="true">
		<cfargument name="idsCommande" 			type="Array" 							required="true">		
		<cfargument name="idtypecommande" 		type="Numeric" 							required="true">
		<cfargument name="idGestionnaire" 		type="numeric" 							required="true">
		<cfargument name="gestionnaire" 		type="string" 							required="true">
		<cfargument name="desti" 				type="string" 							required="true">		
		<cfargument name="pathJoin" 			type="String" 							required="false">

		
			<cfset p_retour = -1>
			<cfloop index="idx" from="1" to="#ArrayLen(idsCommande)#">
				<cftry>
					<cfset idCommande = idsCommande[idx]>
					<cfset str1 = 'SIZE="10"'>
					<cfset str2 = 'SIZE="2"'>		
					<cfset message1 = #Replace(mail.getMessage(),str1,str2,"all")#>
			
					<cfset str1 = "&apos;">
					<cfset str2 = "'">
					<cfset message = #Replace(message1,str1,str2,"all")#>
			
					<cfset listeMailEmp = "">
					<cfset cc = mail.getCc()>
					
					<cfif mail.getCopiePourExpediteur() eq "YES"> 
						<cfif len(mail.getCc()) eq 0>
							<cfset cc = mail.getExpediteur()>
						<cfelse>
							<cfset cc = mail.getCc() & "," & mail.getExpediteur()>
						</cfif>
					</cfif>
					
					<!--- Cr?ation de l'id unique pour le bon de commande --->
					<cfset BDC_UUID = createUUID()>
					
					<!--- Enregistrement de la commande dans la table des logs des bons de commandes --->
					<cfset logger = createObject('component',"LogsCommande")>
					
					<cfset logresult=logger.insertlogCommande( idracine1,
																racine,
																idgestionnaire,
																gestionnaire,
																expediteurMail,
																mail,
																idCommande,
																message1,
																1,
																'n.c.',
																BDC_UUID)>
					
					
					<cfobject name="sr" type="component" component="batchcommande.publicreportservice.ScheduleRequest">
					
					
					<cfswitch expression="#idtypecommande#">
						 <cfcase value="1083"><!--- NOUVELLE COMMANDE --->
						 	<cfset sr.setReportRequest("/consoview/gestion/flotte/BDCMobile-v3/BDCMobile-v3.xdo","AVALIDER","pdf")>
						 </cfcase>
						
						 <cfdefaultcase>
						  	<cfset sr.setReportRequest("/consoview/gestion/flotte/BDCMobile-v3/BDCMobile-v3.xdo","AVALIDER","pdf")>
						 </cfdefaultcase>
					</cfswitch>
					
					<cfset sr.AddParameter("P_IDCOMMANDE","#idCommande#")>
					<cfset sr.AddParameter("P_IDGESTIONNAIRE","#idGestionnaire#")>
					<cfset sr.AddParameter("P_RACINE","#racine#")>
					<cfset sr.AddParameter("P_GESTIONNAIRE","#gestionnaire#")>
					<cfset sr.AddParameter("P_MAIL","#desti#")>
					
					<cfset sr.setScheduleRequest("consoview","public","#BDC_UUID#")>
					<cfset sr.setNotificationScheduleRequest("postmaster@consotel.fr",true,false,false)>
					<cfset sr.setEmailDeliveryRequest("#mail.getDestinataire()#","#cc#","#mail.getBcc()#","#mail.getDestinataire()#","#mail.getExpediteur()#",
					                                                     "#message#","#mail.getModule()# : #mail.getSujet()#")>
					                                                     
					<cfset sr.setBurstScheduleRequest(false)>
	
					<cfset jobId = sr.scheduleReport()>										
					<cfset logresult = logger.updateJobIDBDCMobile(BDC_UUID,jobId)>		
					
				<cfcatch type="any">
					<cfoutput>
							erreur
							#CFCATCH.ExtendedInfo#
					</cfoutput>
				</cfcatch>
				</cftry>	
			</cfloop>
		<cfreturn p_retour>
</cffunction>