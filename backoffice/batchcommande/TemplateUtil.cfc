﻿<cfcomponent displayname="fr.consotel.consoview.M16.v2.TemplateUtil">

	<!--- DONNE LE NOM DU TEMPLATE EN FONCTION DU TYPE DE COMMANDE ET DE L'OPÉRATEUR --->

	<cffunction name="getTemplateNameByOpeNType" access="remote" hint="DONNE LE NOM DU TEMPLATE EN FONCTION DU TYPE DE COMMANDE ET DE L'OPÉRATEUR" returntype="String">
		<cfargument name="idtypecommande" 	type="Numeric" 							required="true">
		<cfargument name="idoperateur" 		type="Numeric"  						required="true"/>
		
		<cfset _type = "BDC"/>			
		
		<cfswitch expression="#idtypecommande#">
			<cfcase value="1083;1182;1282;1382;1583;1584;1585;1587" delimiters=";">
				<cfset _type = "BDC">						
			</cfcase>
			<cfcase value="1283;1383;1483;1586;1588;1589" delimiters=";">
				<cfset _type = "BDR">						
			</cfcase>
			<cfdefaultcase>
				<cfset _type = "BDC">
			</cfdefaultcase>
		</cfswitch>		
		<cfreturn _type & "-" & idoperateur>
	</cffunction>
	
	<!--- DONNE LE TYPE DU TEMPLATE EN FONCTION DU TYPE DE COMMANDE --->
	
	<cffunction name="getTemplateType" access="remote" hint="DONNE LE TYPE DU TEMPLATE EN FONCTION DU TYPE DE COMMANDE" returntype="String">
		<cfargument name="idtypecommande" 	type="Numeric" 							required="true">
		
		<cfset _type = "BDC"/>			
		
		<cfswitch expression="#idtypecommande#">
			<cfcase value="1083;1182;1282;1382;1583;1584;1585;1587" delimiters=";">
				<cfset _type = "BDC">						
			</cfcase>
			<cfcase value="1283;1383;1483;1586;1588;1589" delimiters=";">
				<cfset _type = "BDR">						
			</cfcase>
			<cfdefaultcase>
				<cfset _type = "BDC">
			</cfdefaultcase>
		</cfswitch>
		
		<cfreturn _type>
	</cffunction>
	
	
</cfcomponent>