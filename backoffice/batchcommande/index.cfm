
<style type="text/css">
table {
border: medium solid #6495ed;
border-collapse: collapse;
 
}
th {
font-family: monospace;
border: thin solid #6495ed;
 
padding: 5px;
background-color: #D0E3FA;
}
td {
font-family: verdana;
font-size: 10px;
border: thin solid #6495ed;
 
padding: 5px;
text-align: center;
background-color: #ffffff;
}
caption {
font-family: sans-serif;
}

</style>

<cfset rq=createobject("component","ListQuery")>
<cfset qCommandes=rq.getData()>
	
<table summary="liste des commandes en erreur.">
<caption>
	<cfoutput>
		liste des commandes en erreur au #lsdateformat(now(),'DD/MM/YYYY')#
	</cfoutput>	
</caption>

  <thead>
    <tr>     
     <th>Racine</th>
     <th>N°commande</th>
     <th>Date création</th>
	 <th>Ref client</th>
     <th>Expéditeur</th>
     <th>Destinataire</th>
	 <th>Action</th>
	 <th>Date action</th>
	 
    </tr>
  </thead>

  <tfoot>
    <tr>      
     <th>Racine</th>
     <th>N°commande</th>
     <th>Date création</th>
	 <th>Ref client</th>
     <th>Expéditeur</th>
     <th>Destinataire</th>
	 <th>Action</th>
	 <th>Date action</th>	 
    </tr>
  </tfoot>

 

<cfoutput query="qCommandes">	
  <tbody>
  
    <tr>         
	 <td>#LIBELLE_GROUPE_CLIENT#</td>	 
	 <td>#NUMERO_OPERATION#</td>
	 <td>#DATE_CREATE#</td>
	 <td>#REF_CLIENT#</td>
     <td>#EXPEDITEUR# #MAIL_EXP#</td>
	 <td>#DESTINAT# #MAIL_DEST#</td>
	 <td>#LIBELLE_ACTION#</td>
	 <td><cfif isDefined('date_action')>#DATE_ACTION#</cfif></td>		     
    </tr>
  </tbody>
</cfoutput>	
</table> 

<cfform action="RunCmd.cfm" method="post">
	<cfinput type="Submit" name="SubmitForm" value="renvoyer">
</cfform>