<cfcomponent name="PublicReportService" displayname="PublicReportService">
	<cfset variables.biServer="http://bip-prod.consotel.fr/xmlpserver/services/PublicReportService?wsdl">
	<cfset variables.reportDefinition=StructNew()>
	<cfobject name="runReportRequest" type="component" component="batchcommande.publicreportservice.RunReportRequest">
	<cfobject name="scheduleRequest" type="component" component="batchcommande.publicreportservice.ScheduleRequest">
	
	<!--- Structure pour les fonction de validation --->
	<cfset variables.ReportStruct=structNew()>
	<cfset variables.ReportStruct.userID="consoview">
	<cfset variables.ReportStruct.password="public">
	
	<cffunction name="setCredentials" access="remote" returntype="void">
		<cfargument name="userID" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfset variables.ReportStruct.userID=userID>
		<cfset variables.ReportStruct.password=password>
	</cffunction>
	
	<cffunction name="setAbsolutePath" access="remote" returntype="void">
		<cfargument name="reportAbsolutePath" type="string" required="true">
		<cfset variables.ReportStruct.reportAbsolutePath=reportAbsolutePath>
	</cffunction>
	
	<cffunction name="validateLogin" access="remote" returntype="boolean">
		<cfinvoke webservice="#variables.biServer#" returnvariable="result" method="validateLogin"
			argumentCollection="#variables.ReportStruct#">
		<cfreturn result>
	</cffunction>
	
	<cffunction name="hasReportAccess" access="remote" returntype="boolean">
		<cfinvoke webservice="#variables.biServer#" returnvariable="result" method="hasReportAccess"
			argumentCollection="#variables.ReportStruct#">
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getReportDefinition" access="remote" returntype="struct" output="true">
		<cfinvoke webservice="#variables.biServer#" returnvariable="variables.reportDefinition" method="getReportDefinition"
			argumentCollection="#variables.ReportStruct#">
		<cfset resultReportDef=variables.reportDefinition>
		<cfset stReturn=structNew()>
		<cfset stReturn.isAutoRun=resultReportDef.isAutoRun()>
		<cfset stReturn.isCacheDocument=resultReportDef.isCacheDocument()>
		<cfset stReturn.defaultOutputFormat=resultReportDef.getDefaultOutputFormat()>
		<cfset stReturn.defaultTemplateId=resultReportDef.getDefaultTemplateId()>
		<cfset stReturn.parameterColumns=resultReportDef.getParameterColumns()>
		<cfset stReturn.reportDescription=resultReportDef.getReportDescription()>
		<cfset stReturn.reportName=resultReportDef.getReportName()>
		<cfset stReturn.isDiagnostics=resultReportDef.isDiagnostics()>
		<cfset stReturn.isCacheDocument=resultReportDef.isCacheDocument()>
		<cfset stReturn.isOnLine=resultReportDef.isOnLine()>
		<cfset stReturn.isOpenLinkInNewWindow=resultReportDef.isOpenLinkInNewWindow()>
		<cfset stReturn.isShowControls=resultReportDef.isShowControls()>
		<cfset stReturn.isShowReportLinks=resultReportDef.isShowReportLinks()>
		<cfset stReturn.isUseExcelProcessor=resultReportDef.isUseExcelProcessor()>
		
		<cfset aTemp=ArrayNew(1)>
		<cfloop from="1" to="#arrayLen(resultReportDef.getListOfTemplateFormatsLabelValues())#" index="i">
			<cfset stTemp=structNew()>
			<cfset t=resultReportDef.getListOfTemplateFormatsLabelValues()>
			<cfset stTemp.TemplateUrl =t[i].getTemplateUrl()>
			<cfset stTemp.TemplateType =t[i].getTemplateType()>
			<cfset stTemp.TemplateID =t[i].getTemplateID()>
			<cfset bTemp=ArrayNew(1)>
			<cfloop from="1" to="#arrayLen(t[i].getListOfTemplateFormatLabelValue())#" index="j">
				<cfset st4=structNew()>
				<cfset st3=t[i].getListOfTemplateFormatLabelValue()>
				<cfset st4.TemplateFormatLabel=st3[j].getTemplateFormatLabel()>
				<cfset st4.TemplateFormatValue=st3[j].getTemplateFormatValue()>
				<cfset st4.TemplateExtension=this.decodeExtension(st3[j].getTemplateFormatValue())>
				<cfset bTemp[j]=st4>
			</cfloop>
			<cfset stTemp.TemplateFormat=bTemp>
			<cfset aTemp[i]=stTemp>
		</cfloop>
		<cfset stReturn.ListOfTemplateFormatsLabelValues=aTemp>
		
		<cfset aTemp=ArrayNew(1)>
		<cfloop from="1" to="#arrayLen(resultReportDef.getReportParameterNameValues())#" index="i">
			<cfset stTemp2=structNew()>
			<cfset t=resultReportDef.getReportParameterNameValues()>
			<cfset stTemp2.ParamName =t[i].getName()>
			<cfset stTemp2.isMultiValuesAllowed =t[i].isMultiValuesAllowed()>
			<cfset aTemp[i]=stTemp2>
		</cfloop>
		<cfset stReturn.ReportParameters=aTemp>
		<cfreturn stReturn>
	</cffunction>
	
	<cffunction name="getListeFormat" access="remote" returntype="array" output="true">
		<cfargument name="templateID" type="string" required="true">
		<cfif Not StructKeyExists(variables.reportDefinition,"AUTORUN")>
			<cfset getReportDefinition()>
		</cfif>
		<cfset resultReportDef=variables.reportDefinition>
		<cfloop from="1" to="#arrayLen(resultReportDef.getListOfTemplateFormatsLabelValues())#" index="i">
			<cfset stTemp=structNew()>
			<cfset t=resultReportDef.getListOfTemplateFormatsLabelValues()>
			<cfif t[i].getTemplateID() eq templateID>
				<cfset bTemp=ArrayNew(1)>
				<cfloop from="1" to="#arrayLen(t[i].getListOfTemplateFormatLabelValue())#" index="j">
					<cfset st4=structNew()>
					<cfset st3=t[i].getListOfTemplateFormatLabelValue()>
					<cfset st4.TemplateFormatLabel=st3[j].getTemplateFormatLabel()>
					<cfset st4.TemplateFormatValue=st3[j].getTemplateFormatValue()>
					<cfset st4.TemplateExtension=this.decodeExtension(st3[j].getTemplateFormatValue())>
					<cfset bTemp[j]=st4>
				</cfloop>
			</cfif>
		</cfloop>
		<cfreturn bTemp>
	</cffunction>
	
	<cffunction name="getListeTemplate" access="remote" returntype="array" output="true">
		<cfif Not StructKeyExists(variables.reportDefinition,"AUTORUN")>
			<cfset getReportDefinition()>
		</cfif>
		<cfset resultReportDef=variables.reportDefinition>
		<cfset aTemp=ArrayNew(1)>
		<cfloop from="1" to="#arrayLen(resultReportDef.getListOfTemplateFormatsLabelValues())#" index="i">
			<cfset stTemp=structNew()>
			<cfset t=resultReportDef.getListOfTemplateFormatsLabelValues()>
			<cfset stTemp.TemplateUrl =t[i].getTemplateUrl()>
			<cfset stTemp.TemplateType =t[i].getTemplateType()>
			<cfset stTemp.TemplateID =t[i].getTemplateID()>
			<cfset aTemp[i]=stTemp>
		</cfloop>
		
		<cfreturn aTemp>
	</cffunction>
	
	<cffunction name="getListeParameter" access="remote" returntype="array" output="true">
		<cfif Not StructKeyExists(variables.reportDefinition,"AUTORUN")>
			<cfset getReportDefinition()>
		</cfif>
		<cfset resultReportDef=variables.reportDefinition>		
		<cfset aTemp=ArrayNew(1)>
		<cfloop from="1" to="#arrayLen(resultReportDef.getReportParameterNameValues())#" index="i">
			<cfset stTemp2=structNew()>
			<cfset t=resultReportDef.getReportParameterNameValues()>
			<cfset stTemp2.ParamName =t[i].getName()>
			<cfset stTemp2.isMultiValuesAllowed =t[i].isMultiValuesAllowed()>
			<cfset aTemp[i]=stTemp2>
		</cfloop>
		<cfset stReturn.ReportParameters=aTemp>
		
		<cfreturn aTemp>
	</cffunction>
	
	<cffunction name="getFolderContent" access="remote" returntype="query">
		<cfargument name="FolderAbsolutePath" type="string" required="false" default="/consoview">
		<cfset t=structNew()>
		<cfset t.FolderAbsolutePath=FolderAbsolutePath>
		<cfset t.userID=variables.ReportStruct.userID>
		<cfset t.password=variables.ReportStruct.password>
		<cfinvoke webservice="#variables.biServer#" returnvariable="result" method="getFolderContents"
			argumentCollection="#variables.RunReportRequest#">
		<cfset q=queryNew("TYPE,DISPLAYNAME,ABSOLUTEPATH,FILENAME,CREATIONDATE,LASTMODIFIED,LASTMODIFIER,OWNER,PARENTABSOLUTEPATH")>
		<cfloop from="1" to="#arrayLen(result)#" index="i">
			<cfset queryAddRow(q)>
			<cfset querySetCell(q,"TYPE",result[i].getType())>
			<cfset querySetCell(q,"DISPLAYNAME",result[i].getDisplayName())>
			<cfset querySetCell(q,"ABSOLUTEPATH",result[i].getAbsolutePath())>
			<cfset querySetCell(q,"FILENAME",result[i].getFileName())>
			<cfset querySetCell(q,"CREATIONDATE",result[i].getCreationDate().DATE & "/" & result[i].getCreationDate().MONTH+1)>
			<cfset querySetCell(q,"LASTMODIFIED",result[i].getLastModified().DATE & "/" & result[i].getLastModified().MONTH+1)>
			<cfset querySetCell(q,"LASTMODIFIER",result[i].getLastModifier())>
			<cfset querySetCell(q,"OWNER",result[i].getOwner())>
			<cfset querySetCell(q,"PARENTABSOLUTEPATH",result[i].getParentAbsolutePath())>
		</cfloop>
		<cfreturn q>
	</cffunction>
	
	<cffunction name="createRunReportRequest" access="remote" returntype="void">
		<cfargument name="userID" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfargument name="reportAbsolutePath" type="String" required="true">
		<cfargument name="attributeTemplate" type="String" required="true">
		<cfargument name="attributeFormat" type="String" required="true">
		<cfargument name="attributeLocale" type="String" required="false" default="fr-FR">
		<cfset setCredentials(userID,password)>
		<cfset setAbsolutePath(reportAbsolutePath)>
		<cfset variables.ReportStruct.reportAbsolutePath=reportAbsolutePath>
		<cfset runReportRequest.setRunReportRequest(userID,password,reportAbsolutePath,attributeTemplate,attributeFormat,attributeLocale)>
	</cffunction>
	
	<cffunction name="AddRunReportParameter" access="remote" returntype="void">
		<cfargument name="paramName" type="string" required="true">
		<cfargument name="paramValues" type="string" required="true">
		<cfset runReportRequest.AddParameter(paramName,paramValues)>
	</cffunction>
	
	<cffunction name="runReport" access="remote" returntype="void">
		<cfdump var="#runReportRequest.getRunReportRequest()#">
		<cfinvoke webservice="#variables.biServer#" returnvariable="variables.RunReportReturn" method="runReport"
				argumentCollection="#runReportRequest.getRunReportRequest()#">
		</cfinvoke>
	</cffunction>
	
	<cffunction name="decodeExtension" access="package" returntype="string">
		<cfargument name="valeur" type="string" required="true">
		<cfswitch expression="#valeur#">
			<cfcase value="excel">
				<cfset v="xls">
			</cfcase>
			<cfcase value="excel2000">
				<cfset v="xls">
			</cfcase>
			<cfdefaultcase>
				<cfset v=valeur>
			</cfdefaultcase>
		</cfswitch>
		<cfreturn v>
	</cffunction>
	
	<!--- Getters --->
	<cffunction name="getExtension" access="remote" returntype="string" output="true">
		<cfargument name="templateID" type="string" required="true">
		<cfargument name="template" type="string" required="true">
		<cfset a=getListeFormat(templateID)>
		<cfloop from="1" to="#ArrayLen(a)#" index="i">
			<cfif a[i].TEMPLATEFORMATLABEL eq template>
				<cfreturn a[i].TEMPLATEEXTENSION>
				<cfbreak>
			</cfif>
		</cfloop>
	</cffunction>
	
	<cffunction name="getScheduleRequest" access="remote" returntype="struct">
		<cfreturn scheduleRequest.getScheduleRequest()>
	</cffunction>
	
	<cffunction name="getRunReportRequest" access="remote" returntype="struct">
		<cfreturn runReportRequest.getRunReportRequest()>
	</cffunction>
	
	<cffunction name="getRunReportData" access="remote" returntype="binary">
		<cfreturn variables.RunReportReturn.getReportBytes()>
	</cffunction>
	
	<cffunction name="getRunReportContentType" access="remote" returntype="string">
		<cfreturn variables.RunReportReturn.getReportContentType()>
	</cffunction>
	
</cfcomponent>
