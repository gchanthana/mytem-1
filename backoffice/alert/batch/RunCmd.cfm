	
	<cfset qCommandes = createobject("component", "ListQueryErrorMail").getData(5)>
	
	<cfset lastidcommande = 0>
	
	<cfloop query="qCommandes">

		<cfset qCommandesCommandeId	= IDCOMMANDE>			
		<cfset lastidcommande 		= qCommandesCommandeId>
		
		<cfset currentCommandeTypeId 		= IDTYPE_OPERATION>
		<cfset currentCommandeRacine 		= RACINE>
		<cfset currentCommandeRacineId 		= IDRACINE>
		<cfset currentCommandeNumero 		= NUMERO_OPERATION>
		<cfset currentCommandeUUID 			= ''>
		<cfset currentCommandePath 			= '/container/M16/'>
		<cfset currentCommandeBDC 			= 'BDC_' & #currentCommandeNumero# & '.pdf'>
		
		<cfset currentCommandeId	 		= IDCOMMANDE>
		<cfset currentCommandeUserId 		= IDGESTIONNAIRE>
		<cfset currentCommandeUser 			= GESTIONNAIRE>
		<cfset currentCommandeUserMail		= MAIL>
		
		<cfset currentOperateurId 	 		= OPEURATEURID>
		
		<cfset currentExpediteurMail 		= MAIL_FROM>
		<cfset currentDestinataire	 		= MAIL_TO>
		<cfset currentDestinataireCC 		= CC>
		<cfset currentDestinataireBCC 		= 'monitoring@saaswedo.com'>
		<cfset currentModule		 		= MODULE>
		<cfset currentSubject		 		= SUBJECT>
		<cfset currentMessage 		 		= MESSAGE>
		
		<cfif BCC NEQ '' AND BCC NEQ ' '>

			<cfset currentDestinataireBCC = BCC & ',monitoring@saaswedo.com'>
			
		</cfif>
		
		<cfset currentGlobalization	 		= 'fr_FR'>

		<cfset bdcCommande	= LCase('BDC_' & #currentCommandeNumero# & '.pdf')>
		
		<cfset result 		= 0>		
		<cfset isBDCExist 	= 0>

		<!--- INSERTION DES INFOS DU MAIL --->
		<cfset mailObj = createObject("component", "Mail")/>
		<cfset mailObj.settype("text")/>
		<cfset mailObj.setmodule('')  />
		<cfset mailObj.setmessage(currentMessage)  />
		<cfset mailObj.setcharset("utf-8")   /> 
		<cfset mailObj.setCc(currentDestinataireCC)  />
		<cfset mailObj.setBcc(currentDestinataireBCC)  />
		<cfset mailObj.setexpediteur(currentExpediteurMail)  />
		<cfset mailObj.setcopiePourExpediteur("NO")/>
		<cfset mailObj.setrepondreA(currentExpediteurMail)/>
		<cfset mailObj.setDestinataire(currentDestinataire)/>
		<cfset mailObj.setSujet(currentSubject)/>

		<!--- RECUPERATION DE L'UUID DE LA COMMANDE --->
		<cfset attachements = createObject('component',"fr.consotel.consoview.M16.v2.AttachementService")>
		
		<cfset piecesjointes = attachements.fournirAttachements(Val(#currentCommandeId#))>
		
		<!--- 
		<br><br><cfoutput> ------------------ Pièces jointes -----------</cfoutput><br/><br/>
		<cfdump var="#piecesjointes#"><br/><br/>
		--->
		
		<cfif piecesjointes.RecordCount GT 0>
		
			<cfloop query="piecesjointes">
			
				<cfset currentBDC = LCase(piecesjointes['FILE_NAME'][piecesjointes.currentRow])>
			
				<cfif currentBDC EQ bdcCommande>
			
					<cfset currentCommandeUUID = piecesjointes['PATH'][piecesjointes.currentRow]>
					
					<cfset isBDCExist = 1>
										
				</cfif>
				
			</cfloop>
		
		</cfif>
		
		<cfif isBDCExist EQ 0>
		
			<cfset currentCommandeUUID = createUUID()>
		
		</cfif>
		
		<!--- LE RÉPERTOIRE N'ÉXISTE PAS --->
		<cfif NOT DirectoryExists('#currentCommandePath##currentCommandeUUID#')>
				
			<!--- CRÉATION DU RÉPERTOIRE --->
			<cfdirectory action="Create" directory="#currentCommandePath##currentCommandeUUID#" type="dir" mode="777">

		</cfif>
		
		<!--- PAS DE BDC DONC CREATION D'UN BDC DANS LA BDD --->
		<cfif isBDCExist EQ 0>
			
			<!--- CREATION D'UN BDC DANS LA BDD --->
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.enregistrementListAttachments">
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_fileName"  	value="#currentCommandeBDC#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_path"	  	value="#currentCommandeUUID#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_userid" 	value="#currentCommandeUserId#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_size" 		value="0"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_joinMail" 	value="1"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_format" 	value=".pdf"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" value="#currentCommandeId#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour_bdc">
			</cfstoredproc>
			
			<!--- 		
			<cfoutput> ---- >>>> CREATION D'UN BDC DANS LA BDD <<<< ----</cfoutput><br/>
			<cfoutput> ---- >#p_retour_bdc#</cfoutput><br/><br/>
			 --->	
			 			
		</cfif>
		
		<!--- IL FAUDRA RECUPERER LA GLOBALIATION DE L'UTILISATEUR --->
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.getUserCodeLang">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_userid" 				value="#currentCommandeUserId#"/>
			<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" 	variable="p_retour_globalization">
		</cfstoredproc>

		<cfif p_retour_globalization NEQ 0>
		
			<cfset currentGlobalization = p_retour_globalization>
		
		</cfif>	

		<cfset result = envoyer(mailObj, currentCommandeId, currentCommandeTypeId, currentOperateurId, currentCommandeUserId, currentCommandeRacineId, currentCommandeUser,
									currentCommandeUserMail, currentCommandeRacine, currentCommandeNumero, currentCommandeUUID, currentExpediteurMail, currentMessage,
										currentGlobalization)>
					
				
		<cfif result GT 1>

			<cfset qUpdate = createobject("component", "ListQueryErrorMail").updateData(IDCV_LOG_MAIL)>
		
			<cfmail type="text/html"  subject="[CMD - 0001] Renvoi de commande" server="mail-cv.consotel.fr" port="25" from="no-reply@saaswedo.com" to="monitoring@saaswedo.com">
				<cfoutput> -------------------------------------------------------</cfoutput><br/>
				<cfoutput> --- traitement commande #SUBJECT# -- job id : #result#</cfoutput><br/>
				<cfoutput> -------------------------------------------------------</cfoutput><br/>
				<cfoutput> -------------------------------------------------------</cfoutput><br/>
				<cfoutput> --- maj statu commande #SUBJECT# -- etat : #qUpdate# -- idlog : #IDCV_LOG_MAIL# </cfoutput><br/>
				<cfoutput> -------------------------------------------------------</cfoutput><br/>
			</cfmail>
		
		</cfif>

	</cfloop>
	
	<cflocation url="index.cfm">

	<cffunction name="envoyer" access="public" returntype="any">
		<cfargument name="mail" 				type="Mail" 	required="true">
		<cfargument name="idcommande" 			type="Numeric" 	required="true">
		<cfargument name="idtypecommande" 		type="Numeric" 	required="true">
		<cfargument name="idoperateur" 			type="Numeric" 	required="true">
		<cfargument name="idgestionnaire" 		type="Numeric" 	required="true">
		<cfargument name="idracine" 			type="Numeric" 	required="true">
		<cfargument name="gestionnaire" 		type="string" 	required="true">
		<cfargument name="gestionnairemail"		type="string" 	required="true">
		<cfargument name="racine" 				type="string" 	required="true">
		<cfargument name="numerocommande"		type="String" 	required="true">
		<cfargument name="uuidcommande"			type="String" 	required="true">
		<cfargument name="expediteur"			type="String" 	required="true">
		<cfargument name="message"				type="String" 	required="true">
		<cfargument name="globalization"		type="String" 	required="false" default="fr_FR">
		
			<!--- CRÉATION DE L'ID UNIQUE POUR LE LOG --->
			<cfset BDC_UUID = createUUID()>
			<cfset p_retour = -1>
					
			<cfset logger = createObject('component', "LogsCommande")>
				
			<!--- ENREGISTREMENT DE LA COMMANDE DANS LA TABLE DES LOGS DES BONS DE COMMANDES --->
			<cfset logresult = logger.insertlogCommande(idracine, racine, idgestionnaire, gestionnaire, expediteur, mail, idcommande, message, 1, 'M', BDC_UUID)>






			
			<!--- RÉCUPÉRATION DU TEMPLATE --->
			<cfset _bdcInfos = new fr.consotel.consoview.M16.v2.util.BDCInfos(idracine,idoperateur,idtypecommande)>				 
			<cfset templateName = _bdcInfos.template>
			<cfset reportAbsolutePath = _bdcInfos.absolutepath>
					
			<!--- BIP REPORT --->
			<cfset parameters["bipReport"] = structNew()>
			<cfset parameters["bipReport"]["xdoAbsolutePath"]	= reportAbsolutePath>
			<cfset parameters["bipReport"]["xdoTemplateId"]		= templateName>
			<cfset parameters["bipReport"]["outputFormat"] 		= "pdf">
			<cfset parameters["bipReport"]["localization"]		= globalization>
			
			<!--- PARAMÈTRES DE GÉNÉRATION FTP DU RAPPORT BIP --->
			<cfset parameters.bipReport.delivery.parameters.ftpUser				= "container">
			<cfset parameters.bipReport.delivery.parameters.ftpPassword			= "container">
			<cfset parameters.bipReport.delivery.parameters.fileRelativePath	= "M16/#uuidcommande#/BDC_#numerocommande#.pdf">
			
			<!--- PARAMÈTRES DE FILTRAGE DES DONNÉES DU RAPPORT BIP --->
			<!--- GESTION DE LA NOTIFICATION DE FIN D'EXÉCUTION DU RAPPORT BIP --->
			<cfset parameters.EVENT_TARGET 	= BDC_UUID>
			<cfset parameters.EVENT_TYPE 	= "BDC">
			<cfset parameters.EVENT_HANDLER = "M16">
			
			<!--- PARAMÈTRES POUR LA GÉNÉRATION DU BON DE COMMANDE BIP --->
			<cfset parameters["bipReport"]["reportParameters"]	= structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_IDCOMMANDE"] = structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_IDCOMMANDE"]["parameterValues"] = [idcommande]>
			<cfset parameters["bipReport"]["reportParameters"]["P_IDGESTIONNAIRE"] = structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_IDGESTIONNAIRE"]["parameterValues"] = [idGestionnaire]>
			<cfset parameters["bipReport"]["reportParameters"]["P_RACINE"] = structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_RACINE"]["parameterValues"] = [racine]>
			<cfset parameters["bipReport"]["reportParameters"]["P_GESTIONNAIRE"] = structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_GESTIONNAIRE"]["parameterValues"] = [gestionnaire]>
			<cfset parameters["bipReport"]["reportParameters"]["P_MAIL"] = structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_MAIL"]["parameterValues"] = [gestionnairemail]>
					
			<!--- EXÉCUTION DU SERVICE --->
			<cfset api = createObject("component", "fr.consotel.consoview.api.CV")>
			<cfset jobid = api.invokeService("IbIs", "scheduleReportCmd", parameters)>
			
			<cfif jobid GT 0>
			
				<!--- MISE A JOUR DU JOBID DE LA COMMANDE --->
				<cfset logger.updateJobIDBDCMobile(BDC_UUID, jobid)>
			
				<cfset p_retour = jobid>

			</cfif>
		
		<cfreturn p_retour>
	</cffunction>
	