<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">

  <title>Mytem360</title>
  <meta name="description" content="Mytem360">
  <meta name="author" content="saaswedo">


  <link rel="stylesheet" href="css/styles.css?v=1.0">
  <script src="js/scripts.js"></script>

  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <style>
      #overlay {
          background: #ffffff;
          color: #666666;
          position: fixed;
          height: 100%;
          width: 100%;
          z-index: 5000;
          top: 0;
          left: 0;
          float: left;
          text-align: center;
          padding-top: 25%;
      }
  </style>
  <cfparam name="cryptedQS" type="string" >
  <cfparam name="_transactionid" type="string" >
  <cfparam name="_sp" type="string" >
</head>

<body onload='document.theForm.submit()'>

<div id="overlay">
    <img src="loader.gif" alt="Loading" /><br/>
    Please wait ...
</div>
<form action="endpoint.cfm" method="post" id="theForm" name="theForm"  >
	<input type="hidden" name="cryptedQS" value="<cfoutput>#cryptedQS#</cfoutput>">
	<input type="hidden" name="_transactionid" value="<cfoutput>#_transactionid#</cfoutput>">
  <input type="hidden" name="_sp" value="<cfoutput>#_sp#</cfoutput>">
</form>


</body>
</html>
