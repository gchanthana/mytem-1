<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Mytem360</title>
  <meta name="description" content="Mytem360">
  <meta name="author" content="saaswedo">
  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <style>
      #overlay {
          background: #ffffff;
          position: fixed;
          height: 100%;
          width: 100%;
          z-index: 5000;
          top: 0;
          left: 0;
          float: left;
          text-align: center;
          padding-top: 25%;
      }
  </style>
</head>

<body onload='document.theForm.submit()'>

<div id="overlay">
    <img src="../loader.gif" alt="Loading" /><br/>
    <strong>Please wait ...</strong>
</div>

<cftry>
<cfparam name="email"     type="string" >
<cfparam name="site"      type="string" >
<cfparam name="job"       type="string" default="DIR.MAG">


<cfcatch>
<div id="overlay">
    <img src="../loader.gif" alt="Loading" /><br/>
    <p style="color:##FF0000;"><strong>Missing parameters ...</strong></p>
</div>
</cfcatch>
</cftry>

<cfset time = now()>

<cfset querystring = {
						ISSUER = " idpdecathlon.preprod.org",
						UID=" #form.email#", 
						ROLE=" DEC",
            RTIME=" #time#",
						JOB=" #form.job#",
						SITE=" #form.site#",
						VALID=" YES",
						MAIL=" #form.email#",
						USER=
							{
								LASTNAME=" lastname-test-sso",
								FIRSTNAME=" firstname-test-sso",
								MAIL=" #form.email#"
							}
					}>

<cfset algo = 'AES'>
<cfset encoding = 'HEX'>

<cfset Jquerystring =serializeJSON(querystring)>
<cfset secretKey = generateSecretKey(algo)> 
<cfset cryptedQS = encrypt(Jquerystring, secretKey, algo, encoding)>
<cfset _sp = createUUID()>



<form action="init.cfm" method="post" id="theForm" name="theForm"  >
	<input type="hidden" name="cryptedQS" value="<cfoutput>#cryptedQS#</cfoutput>">
	<input type="hidden" name="_transactionid" value="<cfoutput>#UrlEncodedFormat(secretKey)#</cfoutput>">
  <input type="hidden" name="_sp" value="<cfoutput>#UrlEncodedFormat(_sp)#</cfoutput>">
  <input type="hidden" name="job" value="<cfoutput>#UrlEncodedFormat(job)#</cfoutput>">
</form>
</form>


</body>
</html>