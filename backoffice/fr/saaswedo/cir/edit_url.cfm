<cfquery name="qGetResolution" dataSource="ROCOFFRE">
	SELECT id_resolution, descrip_resolution
	FROM CIR_RESOLUTION
	order by id_resolution
</cfquery>

<cfquery name="qGetShoot" dataSource="ROCOFFRE">
	SELECT * --commentaire, date_page, idcir_url, page_saved, texte_highlight, ticket_jira, titre_page, url_page, user_jira,id_resolution
	FROM CIR_URL
	where idcir_url=#url.id#
</cfquery>

<cfset nbcol = 4>

<html>
	<head>
		<title>CIR Shooter v1.1</title>
		<script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
		<script type="text/javascript">
			//<![CDATA[
		        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
			//]]>
		 </script>
		 <style type="text/css">
		 	td {
			 	font-family: Verdana,Geneva,sans-serif;
			 	font-size: 10px;
			 }
			 .ligne {
				 background-color: gray;
			 }
			 .titre {
				 font-size: 24px;
				 text-align: right;
				 vertical-align: middle;
			 }
			 .titre2 {
				 font-size: 18px;
				 text-align: right;
				 vertical-align: bottom;
			 }
			 body {
				 background-color: rgba(191,191,191,0.36);
			 }
			 .detail {
				 border-style: solid double none dashed;
				 border-width: 1px 0px 0px 0px;
				 padding: 5px 2px 5px 2px;
				 border-color: rgba(94,94,94,0.86) rgba(191,191,191,0.36) rgba(191,191,191,0.36) rgba(191,191,191,0.36);
				 background-color:white;
			 }
			 table {
				 border-spacing: 0px 0px;
			 }
			 a {
				 color: #4b4b4b;
				 font-weight: 600;
				 text-decoration: none;
			 }
		 </style>
  	</head>
	<body>
<cfoutput>
	<form action="process_edit_url.cfm" method="post">
		<input type="hidden" name="id" value="#qGetShoot.idcir_url#">
		<table width="650">
			<tr>
				<td><img src="img/saaswedo_relief_sansfond_moyen.png" width="150"></td>
				<td align="right"><span class="titre">CIR SHOOTER v1.2</span><span class="titre2">_Edit Shoot</span></td>
			</tr>
			<tr height="3">
				<td class="ligne" width="100%" colspan="2"></td>
			</tr>
			<tr height="5">
				<td width="100%" colspan="2"></td>
			</tr>
			<tr>
				<td>Collaborateur</td>
				<td><input name="user" value="#qGetShoot.user_jira#" size="32" type="email"></td>
			</tr>
			<tr>
				<td>Ticket JIRA</td>
				<td><input type="text" name="jirakey" value="#qGetShoot.ticket_jira#"></td>
			</tr>
			<tr>
			<td>Contexte</td>
			<td>Cette page 
					<select name="resolution">
						<cfloop query="qGetResolution">
							<option value="#qGetResolution.id_resolution#" 
								<cfif (qGetShoot.id_resolution eq "" and qGetResolution.id_resolution eq 3) or (qGetShoot.id_resolution neq "" and qGetShoot.id_resolution eq qGetResolution.id_resolution)>
									selected="true"</cfif>>#qGetResolution.descrip_resolution#</option>
						</cfloop>
					</select>
				</td>
			</tr>
			<tr height="1">
				<td class="ligne" width="100%" colspan="2"></td>
			</tr>
			<tr height="5">
				<td width="100%" colspan="2"></td>
			</tr>
			<tr height="20"">
				<td>Titre</td>
				<td><b><input type="text" name="titre" value="#qGetShoot.titre_page#" width="150" size="70"></b></td>
			</tr>
			<tr height="20">
				<td>URL</td>
				<td>#qGetShoot.url_page#</td>
			</tr>
			<tr>
				<td>Sauvegarder la page:</td>
				<td><input type="checkbox" name="savepage" value="#qGetShoot.page_saved#"></td>
			</tr>
			<tr height="5">
				<td width="100%" colspan="2"></td>
			</tr>
			<tr>
				<td valign="top">Commentaire <br><i>(4000 caractères max)</i></td>
				<td>
					<textarea name="comment" style="width: 100%;height:40;">#qGetShoot.commentaire#</textarea>
				</td>
			</tr>
			<tr>
				<td valign="top">Contenu <br><i>(4000 caractères max)</i></td>
				<td>
					<textarea name="contenu" style="width: 100%;height:120;">#qGetShoot.texte_highlight#</textarea>
				</td>
			</tr>
			<tr height="5">
				<td width="100%" colspan="2"></td>
			</tr>
			
			<tr>
				<td></td>
				<td>
					<table width="100%">
					<tr>
						<td align="left"><input type="submit" name="btnSubmit" value="Enregistrer"> </td>
						<td align="center"><input type="submit" name="btnCancel" value="Annuler"> </td>
						<td align="right"><!---<input type="submit" name="btnDelete" value="Supprimer le shoot">---></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	</form>
</cfoutput>
	</body>
</html>
