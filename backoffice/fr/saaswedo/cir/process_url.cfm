<cfif IsDefined('form.savepage')>
	<cfhttp method="GET" redirect="false" resolveURL="true"  url="#form.u#" timeout="30" ></cfhttp>
	<cfset page = cfhttp.filecontent>
	<cfset page_saved = 1>
<cfelse>
	<cfset page = "">
	<cfset page_saved = 0>
</cfif>

<cfquery name="qSaveShoot" dataSource="ROCOFFRE">
	INSERT INTO CIR_URL(USER_JIRA,TICKET_JIRA,TITRE_PAGE,URL_PAGE,TEXTE_HIGHLIGHT,COMMENTAIRE,CONTENU_PAGE,ID_RESOLUTION)
	VALUES('#form.user#',upper('#form.jirakey#'),'#form.titre#','#form.u#','#form.contenu#','#form.comment#',<cfqueryparam value="#page#" CFSQLType='CF_SQL_CLOB'>,#form.resolution#)
</cfquery>

<cfquery name="qGetShoot" dataSource="ROCOFFRE">
	SELECT user_jira, count(*) nb 
	FROM CIR_URL
	group by user_jira
	order by nb desc
</cfquery>

<html>
	<head>
		<title>CIR SHOOTER v1.1</title>
		 <style type="text/css">
		 	td {
			 	font-family: Verdana,Geneva,sans-serif;
			 	font-size: 10px;
			 }
			 .ligne {
				 background-color: gray;
			 }
			 .titre {
				 font-size: 24px;
				 text-align: right;
				 vertical-align: middle;
			 }
			 body {
				 background-color: rgba(191,191,191,0.36);
			 }
		 </style>
  	</head>
	<body>
	<table width="650">
		<tr>
			<td><img src="img/saaswedo_relief_sansfond_moyen.png" width="150"></td>
			<td class="titre">CIR SHOOTER v1.2 - TOP 5</td>
		</tr>
		<tr height="3">
			<td class="ligne" width="100%" colspan="2"></td>
		</tr>
		<tr height="5">
			<td width="100%" colspan="2"></td>
		</tr>
		<tr>
			<td><b>Collaborateur<b></td>
			<td><b>Nombre de shoot</b></td>
		</tr>
		<tr height="1">
			<td class="ligne" width="100%" colspan="2"></td>
		</tr>
		<tr height="5">
			<td width="100%" colspan="2"></td>
		</tr>
		<cfoutput query="qGetShoot" maxRows="5">
		<tr>
			<td>#user_jira#</td>
			<td>#nb#</td>
		</tr>
		</cfoutput>
	</table>
	</body>
</html>