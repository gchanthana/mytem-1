<cfcomponent author="Cedric" extends="fr.saaswedo.utils.data.DataUtils"
hint="Utilitaire de remoting (Héritant des fonctionnalités de l'utilitaire de manipulation de données)">
	<cffunction access="public" name="invoke" returntype="Struct" output="false" hint="Appel la procédure et retourne une structure contenant le résultat.
	Cette fonction est une implémentation abstraite qui lève une exception. Elle doit etre implémentée par les sous composants">
		<cfargument name="service" type="Struct" required="true" hint="Informations concernant le service">
		<cfargument name="procedure" type="String" required="true" hint="Procédure du service à appeler">
		<cfargument name="parameters" type="Array" required="true" hint="Liste des paramètres à fournir à la procédure">
		<cfthrow type="Custom" message="Unable to invoke service" detail="This method is not implemented">
	</cffunction>

	<cffunction access="public" name="ERROR" returntype="Numeric"
	hint="Valeur associée à STATUS() pour un résultat en erreur. L'exception ColdFusion correspondante (i.e CFCATCH) sera associée à la clé RESULT().
	Tout statut de résultat en erreur doit avoir une valeur inférieure à ERROR()">
		<cfreturn -1>
	</cffunction>

	<cffunction access="public" name="UNDEFINED" returntype="Numeric"
	hint="Valeur associée à STATUS() pour un résultat sans erreur mais indéfini. La structure retournée par invoke() ne contiendra pas de clé RESULT()">
		<cfreturn 0>
	</cffunction>

	<cffunction access="public" name="DEFINED" returntype="Numeric"
	hint="Valeur associée à STATUS() pour un résultat sans erreur et défini. Le résultat de la procédure sera associée à la clé RESULT().
	Tout statut de résultat sans erreur et défini doit avoir une valeur supérieure à DEFINED()">
		<cfreturn 1>
	</cffunction>

	<cffunction access="public" name="STATUS" returntype="String"
	hint="Clé dans la structure retournée par invoke() qui est associée au statut du résultat de la procédure appelée par invoke().
	Tout statut de résultat sans erreur et défini doit avoir une valeur supérieure à DEFINED().
	Tout statut de résultat en erreur doit avoir une valeur inférieure à ERROR().
	La valeur de ERROR() doit etre inférieure à UNDEFINED() qui elle meme doit etre inférieure à DEFINED()">
		<cfreturn "STATUS">
	</cffunction>
	
	<cffunction access="public" name="RESULT" returntype="String" hint="Clé correspondant au résultat dans la structure retournée par invoke().
	Le type du résultat dépend de la valeur associée à la clé TYPE() dans la structure retournée par invoke()">
		<cfreturn "RESULT">
	</cffunction>
	
	<cffunction access="public" name="CLIENT_REQUEST" returntype="String"
	hint="Clé présente dans la structure retournée par invoke() et associée au contenu de la requete cliente si elle est accessible et définie">
		<cfreturn "CLIENT_REQUEST">
	</cffunction>
	
	<cffunction access="public" name="SERVER_RESPONSE" returntype="String"
	hint="Clé présente dans la structure retournée par invoke() et associée au contenu de la réponse serveur si elle est accessible et définie">
		<cfreturn "SERVER_RESPONSE">
	</cffunction>
	
	<cffunction access="public" name="getEndpoint" returntype="String"
	hint="Retourne le endpoint fourni dans le paramètre service de invoke() ou une chaine vide sinon">
		<cfargument name="service" type="Struct" required="true" hint="Informations concernant le service">
		<cfreturn getServiceProperty(ARGUMENTS.service,"ENDPOINT")>
	</cffunction>
	
	<cffunction access="public" name="getUsername" returntype="String"
	hint="Retourne le username fourni dans le paramètre service de invoke() ou une chaine vide sinon">
		<cfargument name="service" type="Struct" required="true" hint="Informations concernant le service">
		<cfreturn getServiceProperty(ARGUMENTS.service,"USERNAME")>
	</cffunction>
	
	<cffunction access="public" name="getPassword" returntype="String"
	hint="Retourne le password fourni dans le paramètre service de invoke() ou une chaine vide sinon">
		<cfargument name="service" type="Struct" required="true" hint="Informations concernant le service">
		<cfreturn getServiceProperty(ARGUMENTS.service,"PASSWORD")>
	</cffunction>
	
	<cffunction access="public" name="getParamName" returntype="String" hint="Retourne le nom du paramètre ou une chaine vide sinon">
		<cfargument name="parameter" type="Struct" required="true" hint="Paramètre d'une liste de paramètres à fournir à une procédure">
		<cfif structKeyExists(ARGUMENTS.parameter,"NAME")>
			<cfif LEN(TRIM(ARGUMENTS.parameter.NAME)) GT 0>
				<cfreturn ARGUMENTS.parameter.NAME>
			<cfelse>
				<cfreturn "">
			</cfif>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="isParamValueDefined" returntype="Boolean" hint="Retourne TRUE si la valeur du paramètre est définie">
		<cfargument name="parameter" type="Struct" required="true" hint="Paramètre d'une liste de paramètres à fournir à une procédure">
		<cfreturn structKeyExists(ARGUMENTS.parameter,"VALUE")>
	</cffunction>
	
	<cffunction access="public" name="getParamValue" returntype="Any" hint="Retourne la valeur du paramètre ou lève une exception sinon">
		<cfargument name="parameter" type="Struct" required="true" hint="Paramètre d'une liste de paramètres à fournir à une procédure">
		<cfif isParamValueDefined(ARGUMENTS.parameter)>
			<cfreturn ARGUMENTS.parameter.VALUE>
		<cfelse>
			<cfthrow type="Custom" message="Invalid remoting parameter" detail="Parameter value is undefined">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.utils.remoting.RemotingClient" hint="Constructeur">
		<cfset SUPER.init()>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getServiceProperty" returntype="Any" hint="Retourne la valeur de la clé key du service ou une chaine vide sinon">
		<cfargument name="service" type="Struct" required="true" hint="Informations concernant le service">
		<cfargument name="key" type="String" required="true" hint="Clé d'une propriété du service">
		<cfif structKeyExists(ARGUMENTS.service,ARGUMENTS.key)>
			<cfreturn ARGUMENTS["service"][key]>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
</cfcomponent>