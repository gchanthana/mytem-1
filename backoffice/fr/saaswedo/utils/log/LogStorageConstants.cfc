<cfcomponent author="Cedric" extends="fr.saaswedo.utils.data.DataUtils" hint="Définition des constantes de la librairie de stockage des logs.
Les valeurs spécifiques pour IDMNT_CAT et IDMNT_ID doivent etre définies ou redéfinies dans les sous composants">
	<cffunction access="public" name="DEFAULT_IDGROUP" returntype="Numeric" hint="Valeur par défaut associée à la clé IDGROUP">
		<cfreturn -1>
	</cffunction>

	<!--- IDMNT_CAT par défaut --->
	<cffunction access="public" name="DEFAULT_LOG_CAT" returntype="Numeric" hint="IDMNT_CAT par défaut">
		<cfreturn 0>
	</cffunction>
	
	<!--- IDMNT_ID par défaut --->
	<cffunction access="public" name="DEFAULT_LOG_ID" returntype="Numeric" hint="IDMNT_ID par défaut">
		<cfreturn 0>
	</cffunction>

	<!--- Format du contenu de BODY --->
	<cffunction access="public" name="HTML_BODY" returntype="Numeric" hint="Valeur pour BODY_FORMAT pour un contenu de type HTML">
		<cfreturn 1>
	</cffunction>

	<!--- Catégories de log --->	
	<cffunction access="public" name="DEFAULT_LOG_TYPE" returntype="Numeric" hint="Type de log par défaut (Error)">
		<cfreturn ERROR_LOG()>
	</cffunction>
	
	<cffunction access="public" name="ERROR_LOG" returntype="Numeric" hint="Logs de type Erreur">
		<cfreturn 0>
	</cffunction>
	
	<cffunction access="public" name="INFO_LOG" returntype="Numeric" hint="Logs de type Information">
		<cfreturn 1>
	</cffunction>
	
	<cffunction access="public" name="WARNING_LOG" returntype="Numeric" hint="Logs de type Avertissements">
		<cfreturn 2>
	</cffunction>
	
	<!--- Propriétés par défaut --->
	<cffunction access="public" name="DEFAULT_LOG_PREFIX" returntype="String" hint="Préfixe par défaut dans les logs du backoffice">
		<cfreturn "[EVT-" & DEFAULT_EVT() & "]">
	</cffunction>
	
	<cffunction access="public" name="DEFAULT_EVT" returntype="Numeric" hint="Code EVT par défaut">
		<cfreturn 9999>
	</cffunction>
	
	<cffunction access="public" name="DEFAULT_EMAIL_PROPS" returntype="Struct" hint="Propriétés mail par défaut">
		<cfreturn {
			priority="highest",charset="UTF-8",type="html",from="MLogger <monitoring@saaswedo.com>",to="monitoring@saaswedo.com",
			subject=DEFAULT_LOG_PREFIX() & " Echec de stockage d'un log (MNT_DETAIL)"
		}>
	</cffunction>
	
	<cffunction access="public" name="DEFAULT_DS" returntype="String" hint="Datasource par défaut">
		<cfreturn "ROCOFFRE">
	</cffunction>
	
	<cffunction access="public" name="MAX_DB_TIMEOUT" returntype="Numeric" hint="Timeout max en secs pour le stockage d'un log">
		<cfreturn 60>
	</cffunction>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.utils.log.LogStorageConstants"
	hint="Constructeur qui n'effectue aucune action car ce composant est utilisé pour accéder aux constantes comme des fonctions statiques">
		<cfset SUPER.init()>
		<cfreturn THIS>
	</cffunction>
</cfcomponent>