<cfcomponent author="Cedric" extends="fr.saaswedo.utils.log.LogStorageConstants" hint="Utilitaire de stockage des logs dans la base">
	<cffunction access="public" name="init" returntype="fr.saaswedo.utils.log.LogStorage" hint="Constructeur">
		<cfset SUPER.init()>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="BODY_MAX_BYTES" returntype="Numeric" hint="Nombre d'octets max pour la valeur associée à la clé BODY">
		<cfreturn (1024 * 1024 * 5)>
	</cffunction>
	
	<cffunction access="public" name="logIt" returntype="void" output="false" hint="Stocke un log et l'ajoute dans le log du backoffice (Synchrone).
	Si cette fonction est appelée dans un thread séparé alors le nom du thread doit etre identique à la valeur de la clé UUID du log.
	Toute exception est capturée puis notifiée par mail mais n'est pas à nouveau relancée">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<cfset var logInfos=ARGUMENTS.log>
		<cftry>
			<cfif structKeyExists(logInfos,"UUID")>
				<cfif LEN(logInfos.UUID) EQ LEN(TRIM(logInfos.UUID))>
					<cfif isValid("UUID",logInfos.UUID)>
						<cfif isDefined("THREAD.NAME") AND (compare(THREAD.NAME,logInfos.UUID) NEQ 0)>
							<cfthrow type="Custom" message="Unable to store log" detail="Thread name '#THREAD.NAME#' is not equal to log UUID '#logInfos.UUID#'">
						</cfif>
						<!--- Contenu du log --->
						<cfset var uuid=logInfos.UUID>
						<cfset var idmnt_id=structKeyExists(logInfos,"IDMNT_ID") ? VAL(logInfos.IDMNT_ID):SUPER.DEFAULT_LOG_ID()>
						<cfset var idmnt_cat=structKeyExists(logInfos,"IDMNT_CAT") ? VAL(logInfos.IDMNT_CAT):SUPER.DEFAULT_LOG_CAT()>
						<cfset var backoffice=structKeyExists(logInfos,"BACKOFFICE") ? TRIM(logInfos.BACKOFFICE):(CGI.SERVER_NAME & ":" & CGI.SERVER_PORT)>
						<cfset var subjectId=structKeyExists(logInfos,"SUBJECTID") ? TRIM(logInfos.SUBJECTID):"">
						<cfset var logType=structKeyExists(logInfos,"LOG_TYPE") ? VAL(logInfos.LOG_TYPE):SUPER.DEFAULT_LOG_TYPE()>
						<cfset var body=structKeyExists(logInfos,"BODY") ? TRIM(logInfos.BODY):"">
						<cfset var idGroup=structKeyExists(logInfos,"IDGROUP") ? VAL(TRIM(logInfos.IDGROUP)):SUPER.DEFAULT_IDGROUP()>
						<!--- Ajout l'encodage dans le contenu si son type est HTML_BODY() --->
						<cfif structKeyExists(logInfos,"BODY_FORMAT") AND (VAL(logInfos.BODY_FORMAT) EQ SUPER.HTML_BODY())>
							<cfset var logBody=body>
							<cfsavecontent variable="body">
								<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
								<cfoutput>#logBody#</cfoutput>
							</cfsavecontent>
						</cfif>
						<!--- Ajustement de la taille des valeurs --->
						<cfset backoffice=(LEN(backoffice) GT 200) ? LEFT(backoffice,200):backoffice>
						<cfset subjectId=(LEN(subjectId) GT 200) ? LEFT(subjectId,200):subjectId>
						<cfset body=(stringByteLength(body) GT BODY_MAX_BYTES()) ? LEFT(body,BODY_MAX_BYTES()):body>
						<!--- Stockage du log --->
						<cfquery name="qStoreLog" datasource="#SUPER.DEFAULT_DS()#" timeout="#SUPER.MAX_DB_TIMEOUT()#">
							INSERT into MNT_DETAIL(UUID,IDMNT_CAT,IDMNT_ID,MESSAGEID,SUBJECTID,BODY,MAIL_LU,IDRACINE)
							VALUES (
								<cfqueryparam cfsqltype="cf_sql_varchar"	value="#uuid#">,
								<cfqueryparam cfsqltype="cf_sql_integer"	value="#idmnt_cat#">,
								<cfqueryparam cfsqltype="cf_sql_integer"	value="#idmnt_id#">,
							 	<cfqueryparam cfsqltype="cf_sql_varchar"	value="#backoffice#">,
							 	<cfqueryparam cfsqltype="cf_sql_varchar"	value="#subjectId#">,
								<cfqueryparam cfsqltype="cf_sql_clob"		value="#body#">,
								<cfqueryparam cfsqltype="cf_sql_integer"	value="#logType#">,
								<cfqueryparam cfsqltype="cf_sql_integer"	value="#idGroup#">)
						</cfquery>
						<!--- Ajout dans le log du backoffice : Par défaut le type de log est "Erreur" --->
						<cfset var logStringType="error">
						<cfif logType EQ SUPER.INFO_LOG()>
							<cfset logStringType="information">
						<cfelseif logType EQ SUPER.WARNING_LOG()>
							<cfset logStringType="warning">
						</cfif>
						<cflog type="#logStringType#" text="#SUPER.DEFAULT_LOG_PREFIX()#[UUID:#uuid#] #subjectId#">
					<cfelse>
						<cfthrow type="Custom" message="Unable to store log" detail="Invalid UUID">
					</cfif>
				<cfelse>
					<cfthrow type="Custom" message="Unable to store log" detail="UUID value have spaces before/after its value">
				</cfif>
			<cfelse>
				<cfthrow type="Custom" message="Unable to store log" detail="Missing UUID">
			</cfif>
			<cfcatch type="Any">
				<!--- Notification d'échec de stockage d'un log --->
				<cfset notifyLogError(logInfos,CFCATCH)>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getEmailProperties" returntype="Struct" hint="Retourne les propriétés mail utilisées pour les notifications">
		<cfif NOT structKeyExists(VARIABLES,"EMAIL_PROPS")>
			<!--- Propriétés mail par défaut --->
			<cfset setEmailProperties(SUPER.DEFAULT_EMAIL_PROPS())>
		</cfif>
		<cfreturn VARIABLES.EMAIL_PROPS>
	</cffunction>
	
	<cffunction access="private" name="setEmailProperties" returntype="void" hint="Définit les propriétés mail utilisées pour les notifications">
		<cfargument name="emailProperties" type="Struct" required="true" hint="Propriétés mail">
		<cfset VARIABLES.EMAIL_PROPS=ARGUMENTS.emailProperties>
	</cffunction>
	
	<cffunction access="private" name="notifyLogError" returntype="void" output="false"
	hint="Appelée par logIt() pour notifier par mail un échec de stockage d'un log puis ajouter le log d'échec dans le log du BackOffice">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<cfargument name="error" type="Any" required="true" hint="Exception ColdFusion durant le stockage (CFCATCH)">
		<cfset var logInfos=ARGUMENTS.log>
		<cfset var storageError=ARGUMENTS.error> 
		<cftry>
			<cfset var body=structKeyExists(logInfos,"BODY") ? TRIM(logInfos.BODY):"">
			<cfset var bodyByteLength=(SUPER.stringByteLength(body) / 1024)>
			<!--- Notification de l'échec de stockage d'un log --->
			<cfset var emailProps=getEmailProperties()>
			<cfset var notificationTime=createObject("java","java.util.Date").init()>
			<!--- Une erreur se produit quand un thread séparé est utilisé avec NOW(),lsDateFormat(),lsTimeFormat() --->
			<cfset var timeFormat=createObject("java","java.text.SimpleDateFormat").init("dd/MM/yyyy HH:mm")>
			<!--- La valeur associé à log.BODY est un contenu : Il est retiré pour etre mise en forme en HTML (CFDUMP) --->
			<cfset var logBodyAttchment="">
			<cfset var logBodyAttchmentName="logBody.html">
			<cfsavecontent variable="logBodyAttchment">
				<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
				<cfoutput>
					<center><h3>Contenu BODY du log à stocker :</h3></center>
					<b>Taille de la valeur BODY du log à stocker (Taille MAX #(BODY_MAX_BYTES() / 1024)# Ko) :</b> #bodyByteLength# Ko<br>
					<cfif isSimpleValue(body)>
						#body#<hr>
					<cfelse>
						<cfdump var="#body#" format="text"><hr>
					</cfif>
				</cfoutput>
			</cfsavecontent>
			<cfset structDelete(logInfos,"BODY",FALSE)>
			<cfmail attributeCollection="#emailProps#">
				<cfmailparam disposition="attachment" type="text/html" file="#logBodyAttchmentName#" content="#logBodyAttchment#">
				<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
				<cfoutput>
					<center>Ce mail a été envoyé automatiquement en raison d'un échec de stockage d'un log.</center><hr>
					Le backoffice #CGI.SERVER_NAME#:#CGI.SERVER_PORT# a envoyé ce mail le #timeFormat.format(notificationTime)#.
					<cfif isDefined("THREAD.NAME") AND (LEN(THREAD.NAME) GT 0)>
						<br>Le stockage du log a été exécuté dans un thread séparé
					<cfelse>
						<br>Le stockage du log a été exécuté dans le thread principal (Sans CFTHREAD)
					</cfif>
					<hr><b>La cause de l'échec est ci-dessous :</b><br>
					<cfdump var="#storageError#" format="text"><hr>
					<b>Contenu BODY du log à stocker :</b> En attachement (#logBodyAttchmentName#)<br>
					<b>Reste du contenu du log à stocker (Sans la clé BODY):</b><br>
					<cfdump var="#logInfos#" format="text">
				</cfoutput>
			</cfmail>
			<!--- La clé BODY et sa valeur sont remis dans log --->
			<cfset logInfos.BODY=body>
			<!--- Ajout dans le log du backoffice --->
			<cfset var logUUID=structKeyExists(logInfos,"UUID") ? logInfos.UUID:"">
			<cfset var logSubjectId=structKeyExists(logInfos,"SUBJECTID") ? logInfos.SUBJECTID:"">
			<cfset var storageErrorMsg=storageError.message>
			<cfif structKeyExists(storageError,"detail") AND (LEN(TRIM(storageError.detail)) GT 0)>
				<cfset storageErrorMsg=storageErrorMsg & " [" & storageError.detail & "]">
			</cfif>
			<cfif structKeyExists(storageError,"tagContext") AND isArray(storageError.tagContext) AND (arrayLen(storageError.tagContext) GT 0)>
				<cfset var tagContext=storageError.tagContext>
				<cfif structKeyExists(tagContext[1],"TEMPLATE") AND structKeyExists(tagContext[1],"LINE")>
					<cfset storageErrorMsg=storageErrorMsg & " [FirstTagContext:" & tagContext[1]["TEMPLATE"] & ":" & tagContext[1]["LINE"] & "]">
				</cfif>
			</cfif>
			<cflog type="error" text="#SUPER.DEFAULT_LOG_PREFIX()# Log storage failed #logUUID# [SUBJECTID:#logSubjectId#,Cause:#storageErrorMsg#]">
			<!--- Traitement effectué quand la notification a échouée --->
			<cfcatch type="Any">
				<cfset var notifyFailedCause=CFCATCH.message>
				<cfif structKeyExists(CFCATCH,"detail") AND (LEN(TRIM(CFCATCH.detail)) GT 0)>
					<cfset notifyFailedCause=notifyFailedCause & " [" & CFCATCH.detail & "]">
				</cfif>
				<cfif structKeyExists(CFCATCH,"tagContext") AND isArray(CFCATCH.tagContext) AND (arrayLen(CFCATCH.tagContext) GT 0)>
					<cfset var tagContext=CFCATCH.tagContext>
					<cfif structKeyExists(tagContext[1],"TEMPLATE") AND structKeyExists(tagContext[1],"LINE")>
						<cfset notifyFailedCause=notifyFailedCause & " [FirstTagContext:" & tagContext[1]["TEMPLATE"] & ":" & tagContext[1]["LINE"] & "]">
					</cfif>
				</cfif>
				<cflog type="error" text="#SUPER.DEFAULT_LOG_PREFIX()# Failed to notify error: #notifyFailedCause#">
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>