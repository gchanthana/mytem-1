<cfcomponent author="Cedric" extends="fr.saaswedo.utils.log.LogStorage" hint="Constantes utilisées pour l'API MDM">
	<cffunction access="public" name="DRT_002" returntype="Numeric" hint="IDMNT_ID pour toute fonctionnalité non répertoriée">
		<cfreturn 201>
	</cffunction>
	
	<!--- IDMNT_CAT par défaut --->
	<cffunction access="public" name="DEFAULT_LOG_CAT" returntype="Numeric" hint="IDMNT_CAT par défaut de l'API MDM">
		<cfreturn 62>
	</cffunction>
	
	<!--- IDMNT_ID par défaut --->
	<cffunction access="public" name="DEFAULT_LOG_ID" returntype="Numeric" hint="IDMNT_CAT par défaut de l'API MDM. Identique à DRT_002()">
		<cfreturn MDM_LOG_ID()>
	</cffunction>
	
	<!--- TODO: Cette fonction peut etre spécialisée en fonction des valeurs de log.LOG_TYPE et log.IDMNT_ID --->
	<cffunction access="public" name="logIt" returntype="void" output="false" hint="Redéfinition spécifique à l'API MDM et qui utilise la fonction parent.
	Remplace la valeur associée à log.IDMNT_CAT par MDM.DEFAULT_LOG_CAT(). Une valeur spécifique à l'API MDM est utilisée pour log.SUBJECTID.
	Un contenu spécifique à l'API MDM est rajouté au contenu associé à log.BODY">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<cfset var logInfos=ARGUMENTS.log>
		<cftry>
			<cfset var idGroup=-1>
			<cfset var subjectId="">
			<cfset var body=structKeyExists(logInfos,"BODY") ? logInfos.BODY:"">
			<!--- TODO: Ne fonctionne pas pour l'instant sans le refactoring de l'API MDM
			Cause: L'API MDM existante utilise l'implémentation fr.saaswedo.api.mdm.impl.MdmServer
			--->
			<cfif structKeyExists(logInfos,"MDM_SERVER") AND isInstanceOf(logInfos.MDM_SERVER,"fr.saaswedo.api.mdm.MdmServer")>
				<cfset var mdmServer=logInfos.MDM_SERVER>
				<cfset subjectId=mdmServer.getVendor()>
				<cfset idGroup=mdmServer.getIdGroup()>
			<cfelse>
				<cfset subjectId="N.D">
			</cfif>
			<!--- Valeur alternative de l'identifiant du groupe --->
			<cfif structKeyExists(logInfos,"IDGROUP")>
				<cfset idGroup=VAL(logInfos.IDGROUP)>
			</cfif>
			<cfset subjectId=subjectId & "|" & (structKeyExists(logInfos,"SERIAL") ? TRIM(logInfos.SERIAL):"N.D")>
			<cfset subjectId=subjectId & "|" & (structKeyExists(logInfos,"IMEI") ? TRIM(logInfos.IMEI):"N.D")>
			<!--- Remplacement de IDMNT_CAT,SUBJECTID,BODY : La taille des valeurs sera ajustée par la fonction parent --->
			<cfset logInfos.IDMNT_CAT=DEFAULT_LOG_CAT()>
			<cfset logInfos.SUBJECTID=subjectId>
			<cfsavecontent variable="body">
				<cfoutput>
					<cfif LEN(TRIM(body)) GT 0>
						#body#<hr>
					</cfif>
					<b>SUBJECTID (VENDOR MDM/SERIAL/IMEI):</b>
					(La valeur dépend du contexte d'exécution e.g Le log ne concerne pas un device)<br>#logInfos.SUBJECTID#<hr>
					<b>Identifiant du groupe (Provenant des infos serveur MDM):</b>
					<cfif idGroup NEQ -1>
						#idGroup#<hr>
					<cfelse>
						N.D (Dépend du contexte d'exécution e.g Infos serveur MDM non définis)<hr>
					</cfif>
					<b>Exception:</b>
					<cfif structKeyExists(logInfos,"EXCEPTION")>
						<cfdump var="#logInfos.EXCEPTION#" format="text"><hr>
					<cfelse>
						N.D (Dépend du contexte d'exécution e.g Pas d'exception capturée)<hr>
					</cfif>
					<b>Réponse de l'API du vendor MDM:</b>
					<cfif structKeyExists(logInfos,"SERVER_RESPONSE")>
						<cfif structKeyExists(logInfos.SERVER_RESPONSE,"fileContent")>
							<cfset var responseContent=logInfos.SERVER_RESPONSE.fileContent>
							<cfset var javaByteArrayClass="java.io.ByteArrayOutputStream">
							<!--- Une comparaison avec getClass().getName() peut etre erronée car héritage possible --->
							<cfif isInstanceOf(responseContent,javaByteArrayClass)>
								<br><b>Contenu (FileContent)</b><br>#responseContent.toString()#
							</cfif>
						</cfif>
						<br><b>Dump (CFHTTP)</b><br>
						<cfdump var="#logInfos.SERVER_RESPONSE#" format="text"><hr>
					<cfelse>
						N.D (Dépend du contexte d'exécution e.g Pas d'appel à l'API)<hr>
					</cfif>
					<b>Requete cliente envoyée à l'API du vendor MDM:</b>
					<cfif structKeyExists(logInfos,"CLIENT_REQUEST")>
						<cfdump var="#logInfos.CLIENT_REQUEST#" format="text"><hr>
					<cfelse>
						N.D (Dépend du type d'API de remoting et du contexte d'exécution e.g Pas d'appel à l'API)<hr>
					</cfif>
				</cfoutput>
			</cfsavecontent>
			<cfset logInfos.BODY=body>
			<cfset SUPER.logIt(logInfos)>
			<cfcatch type="Any">
				<!--- Notification d'échec de stockage d'un log --->
				<cfset SUPER.notifyLogError(logInfos,CFCATCH)>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>