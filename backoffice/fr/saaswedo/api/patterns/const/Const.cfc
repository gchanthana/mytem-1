<!--- Cette version n'est pas un singleton donc il faut définir toutes les constantes initialement et les utiliser ensuite
Donc pour l'instant : IL NE FAUT PAS définir/ajouter des constantes en cours d'exécution après que que toutes les constantes soient définies --->
<cfcomponent author="Cedric" displayname="fr.saaswedo.api.patterns.const.Const" hint="Implémentation de constantes pouvant etre accédées
avec la fonction CONST() de ce composant et dont les valeurs sont en lecture seule.<br>Les fonctions doivent etre appelées sur une instance
retournée par init() sinon des exceptions peuvent se produire.<br>La fonction CONST() demande une clé qui est une chaine de la forme XXX.YYY
avec XXX un préfixe utilisé pour regrouper et YYY le nom de la constante. Elle retourne la valeur de la constante en tant que Any mais la valeur
d'une constante est toujours de type simple sinon une exception sera levée.<br>Ce composant ne définit aucune constante.">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne la valeur utilisée comme verrou CFLOCK">
		<cfreturn "fr.saaswedo.api.patterns.const.Const">
	</cffunction>
	
	<cffunction access="public" name="TIMEOUT" returntype="Numeric" hint="Retourne la valeur utilisée comme timeout (CFLOCK)">
		<cfreturn 120>
	</cffunction>
	
	<cffunction access="public" name="CONST" returntype="Any" description="LOCK:ReadOnly" hint="Retourne la valeur de la constante key">
		<cfargument name="key" type="String" required="true" hint="Clé de la constante de la forme XXX.YYY (Pas de point dans les valeurs de XXX et YYY)">
		<cfset var ERRORID="CONST.ERROR">
		<cfif constExists(ARGUMENTS.key)>
			<cfset var category=listGetAt(ARGUMENTS.key,1,".")>
			<cfset var const=listGetAt(ARGUMENTS.key,2,".")>
			<cfset var constants=getConstants()>
			<!--- Récupération de la valeur de la constante --->
			<cftry>
				<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
					<cfreturn constants[category][const]>
				</cflock>
				<cfcatch type="Any">
					<cfset var detail=TRIM(CFCATCH.detail) NEQ "" ? " [" & CFCATCH.detail & " ]":"">
					<cfthrow type="Custom" errorCode="#ERRORID#" message="Unable to get constant '#ARGUMENTS.key#'" detail="#CFCATCH.message##detail#">
				</cfcatch>
			</cftry>
		<cfelse>
			<cfthrow type="Custom" errorCode="#ERRORID#" message="Key not found" detail="Key '#ARGUMENTS.key#' not found">
		</cfif>
	</cffunction>

	<cffunction access="public" name="constExists" returntype="Boolean" description="LOCK:ReadOnly" hint="Retourne TRUE si la constante key existe">
		<cfargument name="key" type="String" required="true" hint="Clé de la constante de la forme XXX.YYY (Pas de point dans les valeurs de XXX et YYY)">
		<cfset var ERRORID="CONST.ERROR">
		<cfset var constKey=ARGUMENTS.key>
		<cfif LEN(TRIM(constKey)) GT 0>
			<!--- Extraction de la clé de la forme XXX.YYY --->
			<cfset var del=".">
			<cfif listLen(constKey,del) EQ 2>
				<!--- Les espaces devant et après NE DOIVENT PAS etre supprimés pour ne pas fausser le résultat --->
				<cfset var category=listGetAt(constKey,1,del)>
				<cfset var const=listGetAt(constKey,2,del)>
				<cfif (LEN(TRIM(category)) GT 0) AND (LEN(TRIM(const)) GT 0)>
					<cfset var constants=getConstants()>
					<!--- Extraction de la valeur de la clé de la forme XXX.YYY --->
					<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
						<cfreturn structKeyExists(constants,category) AND structKeyExists(constants[category],const)>
					</cflock>
				<cfelse>
					<cfthrow type="Custom" errorCode="#ERRORID#" message="Key is invalid" detail="A part of key '#constKey#' is empty">
				</cfif>
			<cfelse>
				<cfthrow type="Custom" errorCode="#ERRORID#" message="Key is invalid" detail="Key '#constKey#' is not like XXX.YYY">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorCode="#ERRORID#" message="Key is invalid" detail="Key '#constKey#' is empty">
		</cfif>
	</cffunction>

	<cffunction access="public" name="LIST" returntype="Array" description="LOCK:ReadOnly" hint="Retourne la liste des constantes triés par nom">
		<cfset var constants=getConstants()>
		<!--- Récupération de la liste des constantes existantes : Elle est initialisée par getConst() et mise à jour par defineConst() --->
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfreturn VARIABLES.constantList>
		</cflock>
	</cffunction>

	<!--- Cette version n'est pas un singleton --->
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.patterns.const.Const" description="LOCK:Exclusive" hint="Constructeur">
		<!--- CONST est le verrou utilisé dans tout ce composant --->
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfif NOT structKeyExists(VARIABLES,"INITIALIZED")>
				<!--- Structure qui stocke les constantes --->
				<cfset VARIABLES.constants={}>
				<!--- Liste des clés des constantes contenues dans VARIABLES.constants --->
				<cfset VARIABLES.constantList=[]>
				<!--- Fonction d'initialisation e.g Définition des constantes --->
				<cfset initConst()>
				<cfset VARIABLES.INITIALIZED=TRUE>
			</cfif>
			<cfreturn THIS>
		</cflock>
	</cffunction>
	
	<!--- =========== Fin des fonction publiques =========== --->
	
	<!--- Redéfinir cette fonction pour définir/ajouter des constantes supplémentaires (Utiliser l'exemple de cette fonction)
	Appeler SUPER.initConst() en 1er (PREMIER) pour que les constantes définies par ce composant soient créées.
	Utiliser constExists() pour vérifier sont existence afin d'eviter un exception.
	Utiliser defineConstant() pour ajouter chaque constante. Cette fonction est synchronisée (CFLOCK) --->
	<cffunction access="private" name="initConst" returntype="void" hint="Appelée par init() pour initialiser l'instance">
		<!--- Exemple de définition d'une constante
		<cfset defineConst("XXX.YYY","Valeur")>
		--->
	</cffunction>
	
	<cffunction access="private" name="defineConst" returntype="void" description="LOCK:Exclusive" hint="Défini et ajoute une constante">
		<cfargument name="key" type="String" required="true" hint="Clé de la constante de la forme XXX.YYY (Pas de point dans les valeurs de XXX et YYY)">
		<cfargument name="value" type="Any" required="true" hint="Valeur de la constante (Doit etre de type simple)">
		<cfset var ERRORID="CONST.ERROR">
		<cfset var constValue=ARGUMENTS.value>
		<cfif isSimpleValue(constValue)>
			<cfset var constKey=ARGUMENTS.key>
			<!--- Définition et ajout de la constante dans la structure getConstants() --->
			<cfif NOT constExists(constKey)>
				<cfset var constants=getConstants()>
				<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
					<!--- SEULEMENT POUR AJOUTER UNE CONSTANTE : Les clés dans getConstants() ne doivent pas contenir d'espaces devant et après --->
					<cfset var category=TRIM(listGetAt(constKey,1,"."))>
					<cfset var const=TRIM(listGetAt(constKey,2,"."))>
					<!--- Création/Ajout des clés constituant le chemin XXX.YYY et de la valeur de la constante --->
					<cfif NOT structKeyExists(constants,category)>
						<cfset constants[category]={}>
					</cfif>
					<!--- Définition de la valeur. Lève une exception si const existe déjà. Ex: Si constExists() ne fonctionnait pas correctement --->
					<cfset structInsert(constants[category],const,constValue,FALSE)>
					<!--- Ajout de la clé dans la liste des constantes présentes dans getConstants() et tri de la liste --->
					<cfset arrayAppend(VARIABLES.constantList,category & "." & const)>
					<cfset arraySort(VARIABLES.constantList,"textnocase","asc")>
				</cflock>
			<cfelse>
				<cfthrow type="Custom" errorCode="#ERRORID#" message="Key already exists" detail="Key '#constKey#' already exists">
			</cfif>
		<cfelse>
			<cfset var valueType=getMetadata(constValue).NAME>
			<cfthrow type="Custom" errorCode="#ERRORID#" message="Invalid value" detail="Value type is not a primitive : #valueType#">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getConstants" returntype="Struct" description="LOCK:ReadOnly" hint="Retourne la structure des constantes">
		<!--- CONST est le verrou utilisé dans tout ce composant --->
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfreturn VARIABLES.constants>
		</cflock>
	</cffunction>
</cfcomponent>