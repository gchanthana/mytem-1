<!--- =========== Use this comment as a rule to get a 100 characters line MAX width =========== --->
<cfcomponent author="Cedric"
displayname="fr.saaswedo.api.patterns.singleton.Singleton" hint="Implémentation du pattern Singleton">
	<cffunction access="public" name="getInstance" returntype="fr.saaswedo.api.patterns.singleton.Singleton"
	hint="Retourne une instance unique de ce composant">
		<!--- Par défaut : La référence du singleton est placée dans le scope APPLICATION --->
		<cfset var scope=APPLICATION>
		<cfset var scopeName="Application">
		<cfset var instanceKey=getInstanceKey()>
		<cfset var instanceFullName=getInstanceFullName()>
		<!--- Sinon : La référence du singleton est placée dans le scope SERVER --->
		<cfif NOT isDefined("APPLICATION")>
			<cfset scope=SERVER>
			<cfset scopeName="Server">
			<cflog type="warning"
			text="[#getLogName()#] Singleton will be added in scope #scopeName#">
		</cfif>
		<!--- Ajout de l'instance dans le scope --->
		<cflock type="exclusive" scope="#scopeName#" timeout="60" throwOnTimeout="true">
			<!--- Règle : Clé, Type de l'instance --->
			<cfif (NOT structKeyExists(scope,instanceKey)) OR
			(NOT isInstanceOf(scope[instanceKey],instanceFullName))>
				<!--- Initialisation de l'instance --->
				<cfset initInstance()>
				<cfset scope[instanceKey]=THIS>
				<cflog type="information"
				text="[#getLogName()#] Singleton added in scope #scopeName# : #instanceKey#">
				<cfreturn scope[instanceKey]>
			</cfif>
		</cflock>
	</cffunction>
	
	<!--- =========== Fin des fonction publiques =========== --->
	
	<!--- Toujours redéfinir cette fonction --->
	<cffunction access="private" name="initInstance" returntype="void"
	hint="Appelée dans getInstance() pour initialiser l'instance.
	Cette fonction devrait toujours etre redéfinie pour implémenter l'initialisation">
		<!--- TODO : Redéfinir cette fonction pour implémenter l'initialisation de l'instance --->
	</cffunction>
	
	<!--- Toujours redéfinir cette fonction --->
	<cffunction access="private" name="getInstanceFullName" returntype="String"
	hint="Retourne le nom complet de ce composant. La valeur devrait toujours valide">
		<cfreturn "fr.saaswedo.api.patterns.Singleton">
	</cffunction>
	
	<cffunction access="private" name="getInstanceKey" returntype="String"
	hint="Retourne la clé qui référence l'instance dans le scope où elle est placée.
	La valeur devrait toujours etre la meme que getSingletonClassName()">
		<cfreturn getInstanceFullName()>
	</cffunction>
	
	<!--- Toujours redéfinir cette fonction --->
	<cffunction access="private" name="getLogName" returntype="String"
	hint="Retourne le nom utilisé dans les logs concernant ce composant.
	La valeur devrait toujours correspondre au nom (Sans le package) du composant">
		<cfreturn "Singleton">
	</cffunction>
</cfcomponent>