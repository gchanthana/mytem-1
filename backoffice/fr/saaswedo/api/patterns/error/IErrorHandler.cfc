<cfinterface author="Cedric" displayName="fr.saaswedo.api.patterns.error.IErrorHandler" hint="Interface d'un composant de traitement des exceptions
représenté par handleError(). Une instance de cette interface peut etre enregistrée auprès d'une implémentation IErrorHandler pour que le traitement
soit effectué pour certains codes d'erreurs uniquement. Dans ce cas getId() doit etre implémenté et la valeur qu'elle retourne doit etre unique pour
différencier ce traitement des autres qui sont déjà enregistrés auprès de la meme instance IErrorHandler pour un meme code d'erreur.<br>
<b>Donc il faut éviter d'appeler errorTarget.dispatchError() dans handlerError() car cela peut entrainer une boucle sans fin<br>De meme
une implémentation IErrorTarget doit éviter de s'enregistrer à lui meme en tant que traitement car il y a risque de boucle sans fin</b>">
	<cffunction name="getId" returntype="String" hint="Retourne une valeur unique (e.g UUID) utilisée par une implémentation IErrorHandler
	pour différencier l'instance de cette interface des autres qui sont déjà enregistrés auprès d'elle">
	</cffunction>

	<cffunction name="handleError" returntype="void" hint="Traitement de l'exception cfCatchObject. Cette fonction est aussi appelée
	quand cfCatchObject.errorCode correspond à un des code d'erreur enregistré auprès d'une instance IErrorHandler<br>
	<b>Attention à ne pas appeler errorTarget.dispatchError() ici car cela peut entrainer une boucle sans fin (StackOverflowError)</b>">
		<cfargument name="errorTarget" type="fr.saaswedo.api.patterns.error.IErrorTarget" required="true" hint="Instance qui a dispatché l'erreur">
		<cfargument name="cfCatchObject" type="Any" required="true" hint="Exception de type CFCATCH contenant : message,detail,errorCode,tagContext">
		<cfargument name="errorInfos" type="Struct" required="false" hint="Infos supplémentaires facultatives pouvant etre liées à l'exception">
	</cffunction>
</cfinterface>