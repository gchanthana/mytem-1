<cfinterface author="Cedric" displayName="fr.saaswedo.api.patterns.error.IErrorTarget" hint="Interface d'un composant qui dispatche les erreurs
vers des traitements de type IErrorHandler qui se sont enregistrés auprès d'elle pour etre effectués quand des codes d'erreur donné sont dispatché par
dispatchError().Les codes d'erreurs sont case-insensitive et l'implémentation précise l'ordre d'exécution des instances IErrorHandler<br>
<b>Donc une implémentation IErrorTarget doit éviter de s'enregistrer à lui meme en tant que traitement car il y a risque de boucle sans fin</b>">
	<cffunction name="hasErrorTypeHandler" returntype="Boolean" hint="Retourne TRUE si au moins un traitement IErrorHandler est enregistré pour errorCode.
	Retourne TRUE si au moins un traitement est enregistré pour le code d'erreur ERROR.ALL_ERRORS (Voir fr.saaswedo.api.patterns.const.Globals)">
		<cfargument name="errorCode" type="String" required="true" hint="Code d'erreur">
	</cffunction>
	
	<cffunction name="addErrorHandler" returntype="void" hint="Enregistre le traitement errorHandler pour etre effectué quand
	errorCode est dispatché. Aucune action n'est effectuée si la valeur de errorHandler.getId() est déjà enregistrée pour errorCode">
		<cfargument name="errorHandler" type="fr.saaswedo.api.patterns.error.IErrorHandler" required="true" hint="Traitement a effectuer">
		<cfargument name="errorCode" type="String" required="true" hint="Code d'erreur">
	</cffunction>
	
	<!--- Cette fonction est dans l'interface car elle doit etre accessible si on doit dispatcher une erreur et qu'on n'hérite pas de ce composant --->
	<cffunction name="dispatchError" returntype="void" hint="Exécute la fonction handleError() de chaque instance IErrorHandler enregistrée pour le code
	d'erreur cfCatchObject.errorCode<br>Par défaut le code d'erreur est considéré égal ERROR.ALL_ERRORS (Voir fr.saaswedo.api.patterns.const.Globals)
	quand errorCode n'est pas renseigné dans cfCatchObject. L'implémentation précise si l'exécution de IErrorHandler.handleError() est bloquante ou non">
		<cfargument name="cfCatchObject" type="Any" required="true" hint="Exception de type CFCATCH contenant : message,detail,errorCode,tagContext">
		<cfargument name="errorInfos" type="Struct" required="false" hint="Infos liées à l'exception">
	</cffunction>
</cfinterface>