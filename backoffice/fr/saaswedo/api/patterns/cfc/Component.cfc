<cfcomponent author="Cedric" displayname="fr.saaswedo.api.patterns.cfc.Component" hint="Implémentation de base d'un composant réutilisé dans le package
fr.saaswedo.api ou ailleurs.<br>La fonction init() doit etre utilisée pour avoir une instance initialisée sinon des exceptions peuvent se produire">
	<cffunction access="public" name="LOCKID" returntype="String"
	hint="Retourne la valeur utilisée comme verrou CFLOCK. Cette fonction n'est pas implémentée et lève une exception.
	<b>GLOBALS().CONST() peut etre utilisée pour utiliser une valeur différente de celle de la redéfinition de cette fonction</b>">
		<cfthrow type="Custom" errorCode="#GLOBALS().CONST('ERROR.CODE')#" message="LOCKID() is not implemented">
	</cffunction>
	
	<cffunction access="public" name="TIMEOUT" returntype="Numeric" hint="Retourne la constante GLOBAL.TIMEOUT.
	<b>GLOBALS().CONST() peut etre utilisée pour utiliser une valeur différente de celle de la redéfinition de cette fonction</b>">
		<cfreturn GLOBALS().CONST("GLOBAL.TIMEOUT")>
	</cffunction>
	
	<cffunction access="public" name="componentName" returntype="String"
	hint="Retourne le nom complet du composant. Cette fonction peut etre appelée avant init()">
		<cfif NOT structKeyExists(VARIABLES,"cfcName")>
			<!--- Stocke le nom complet du CFC pour ne pas appeler getMetadata() à chaque fois. Voir componentName() --->
			<cfset VARIABLES.cfcName=getMetadata(THIS).NAME>
		</cfif>
		<cfreturn VARIABLES.cfcName>
	</cffunction>

	<cffunction access="public" name="isInitialized" returntype="Boolean" hint="Retourne TRUE si l'instance est initialisée">
		<cfif structKeyExists(VARIABLES,"INITIALIZED")>
			<cfreturn VARIABLES.INITIALIZED>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
	</cffunction>

	<cffunction access="public" name="init" returntype="fr.saaswedo.api.patterns.cfc.Component" hint="Appele une fonction d'initialisation
	interne quand isInitialized() retourne FALSE. Ce constructeur est appelé automatiquement quand l'opérateur <b>new</b> est utilisé.
	<br>Les implémentations peuvent appeler ce constructeur en 1er pour effectuer un traitement dans si l'instance est déjà initialisée lors de l'appel">
		<cfset initInstance()>
		<cfreturn THIS>
	</cffunction>

	<!--- =========== Fin des fonction publiques =========== --->
	
	<cffunction access="private" name="GLOBALS" returntype="fr.saaswedo.api.patterns.const.Globals"
	hint="Retourne le conteneur des constantes globales. Cette fonction peut etre appelée avant init()">
		<cfset var globalsKey="fr.saaswedo.api.patterns.const.Globals">
		<cfif NOT structKeyExists(VARIABLES,globalsKey)>
			<cfset VARIABLES[globalsKey]=new fr.saaswedo.api.patterns.const.Globals()>
		</cfif>
		<cfreturn VARIABLES[globalsKey]>
	</cffunction>
	
	<cffunction access="private" name="setInitialized" returntype="void" hint="Remplace la valeur existante retournée par isInitialized()">
		<cfargument name="isInitialized" type="Boolean" required="true" hint="La valeur de ce paramètre sera retournée par isInitialized()">
		<cfset VARIABLES.INITIALIZED=ARGUMENTS.isInitialized>
	</cffunction>
	
	<cffunction access="private" name="initInstance" returntype="void" hint="Initialisation appelée par init(). La fonction whenAlreadyInitialized()
	est appelée si isInitialized() retourne TRUE. Cette fonction passe la valeur TRUE à setInitialized() et écrit un log contenant componentName()
	<br>Les implémentations peuvent appeler cette fonction en 1er pour effectuer un traitement dans si l'instance est déjà initialisée lors de l'appel">
		<cfif NOT isInitialized()>
			<cfset var logName=GLOBALS().CONST("Patterns.LOG")>
			<cfset setInitialized(TRUE)>
			<!--- Ce log permet de tracer les instanciation des implémentations de ce composant --->
			<cflog type="information" text="[#logName#] #componentName()# initialized">
		<cfelse>
			<cfset whenAlreadyInitialized()>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="whenAlreadyInitialized" returntype="void" hint="Appelée par initInstance() si isInitialized() retourne TRUE
	<br>Cette implémentation lève une exception car elle est appelée dans initInstance() quand l'instance est déjà initialisée lors de l'appel">
		<cfset var code=GLOBALS().CONST("ERROR.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()# instance is already initialized">
	</cffunction>
</cfcomponent>