<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.Users" extends="fr.saaswedo.api.support.BaseClass" hint="Fonctionnalités de gestion des utilisateurs">
	<cffunction access="public" name="show" returntype="Struct" hint="Retourne les infos concernant l'utilisateur dont l'identifiant est spécifié en paramètre">
		<cfargument name="userId" type="Numeric" required="true" hint="Identifiant utilisateur">
		<cfif ARGUMENTS["userId"] GTE 0>
			<cfset var userApi=getUsers()>
			<cfset var requestResult=userApi.show(ARGUMENTS["userId"])>
			<cfif requestResult["STATUS"]["CODE"] EQ userApi.SUCCESS()>
				<cfreturn requestResult["RESULT"]["user"]>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#requestResult.FAULT.MESSAGE#" detail="HTTP CODE : #requestResult.STATUS.HTTP_STATUS#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les valeurs du paramètre userId doit etre positif" detail="Méthode Users.show()">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getUserId" returntype="Numeric" hint="Recherche un utilisateur et retourne son identifiant ou 0 s'il n'existe pas">
		<cfargument name="email" type="String" required="true" hint="Email utilisateur">
		<cfset var userApi=getUsers()>
		<cfreturn userApi.getUserId(ARGUMENTS["email"])>
	</cffunction>
	
	<cffunction access="public" name="update" returntype="Struct" hint="Met à jour un utilisateur et retourne les infos correspondantes">
		<cfargument name="userId" type="Numeric" required="true" hint="Identifiant utilisateur">
		<cfargument name="userInfos" type="Struct" required="true" hint="Infos utilisateur">
		<cfset var userApi=getUsers()>
		<cfset var requestResult=userApi.update(ARGUMENTS["userId"],ARGUMENTS["userInfos"])>
		<cfif requestResult["STATUS"]["CODE"] EQ userApi.SUCCESS()>
			<cfreturn requestResult["RESULT"]["user"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#requestResult.FAULT.MESSAGE#" detail="HTTP CODE : #requestResult.STATUS.HTTP_STATUS#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="delete" returntype="void" hint="Supprime l'utilisateur spécifié et retourne les infos le concernant">
		<cfargument name="userId" type="Numeric" required="true" hint="Identifiant utilisateur">
		<cfif ARGUMENTS["userId"] GTE 0>
			<cfset var userApi=getUsers()>
			<cfset var requestResult=userApi.delete(ARGUMENTS["userId"])>
			<cfif requestResult["STATUS"]["CODE"] NEQ userApi.SUCCESS()>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#requestResult.FAULT.MESSAGE#" detail="HTTP CODE : #requestResult.STATUS.HTTP_STATUS#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les valeurs du paramètre userId doit etre positif" detail="Méthode Users.delete()">
		</cfif>
	</cffunction>
</cfcomponent>