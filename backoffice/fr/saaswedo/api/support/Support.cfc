<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.Support" extends="fr.saaswedo.api.support.BaseClass"
hint="Ensemble de fonctionnalités pour un service client (Tickets d'incidents, etc...).
L'implémentation utilise l'API Zendesk (Un login par défaut est utilisé et automatiquement authentifié pour l'accès au service)">
	<cffunction access="public" name="show" returntype="Struct" hint="Retourne les infos contenues dans un ticket">
		<cfargument name="ticketId" type="Numeric" required="true" hint="Identifiant du ticket (Valeur positive)">
		<cfif ARGUMENTS["ticketId"] GT 0>
			<cfset var tickets=getTickets()>
			<cfset var requestResult=tickets.show(ARGUMENTS["ticketId"])>
			<cfif requestResult["STATUS"]["CODE"] EQ tickets.SUCCESS()>
				<cfreturn requestResult["RESULT"]["ticket"]>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#requestResult.FAULT.MESSAGE#" detail="HTTP CODE : #requestResult.STATUS.HTTP_STATUS#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La valeur du paramètre ticketId doit etre positive">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="comment" returntype="void" hint="Ajoute un commentaire à un ticket">
		<cfargument name="ticketId" type="Numeric" required="true" hint="Identifiant du ticket (Valeur positive)">
		<cfargument name="public" type="Boolean" required="true" hint="TRUE pour un commentaire public et FALSE sinon">
		<cfargument name="content" type="String" required="true" hint="Contenu du commentaire">
		<cfargument name="email" type="String" required="false" default="" hint="Email de l'utilisateur auteur du commentaire">
		<cfset var tickets=getTickets()>
		<cfset var ticketRequest={
			"ticket"={
				"comment"={"public"=ARGUMENTS["public"],"value"=ARGUMENTS["content"]}
			}
		}>
		<cfset tickets.updateRequest(ARGUMENTS["ticketId"],ticketRequest,email)>
	</cffunction>

	<cffunction access="public" name="addAttachments" returntype="void" hint="Ajoute un ou plusieurs attachements et retourne les infos correspondantes">
		<cfargument name="ticketId" type="Numeric" required="true" hint="Identifiant du ticket (Valeur positive)">
		<cfargument name="comment" type="String" required="true" hint="Commentaire associé à l'ajout de l'attachement">
		<cfargument name="token" type="String" required="true" hint="<b>Un et un seul token unique</b> référençant les attachements à ajouter. Voir Attachments.uploadContent()">
		<cfargument name="public" type="Boolean" required="false" default="true" hint="TRUE pour un commentaire public et FALSE sinon">
		<cfargument name="email" type="String" required="false" default="" hint="Utilisateur auteur du commentaire ou une chaine vide s'il n'est pas autorié à se connecter">
		<cfif (ARGUMENTS["ticketId"] GT 0) AND (TRIM(ARGUMENTS["token"]) NEQ "")>
			<cfset var tickets=getTickets()>
			<cfset var uploadRequest={
				"ticket"={"comment"={"public"=ARGUMENTS["public"],"value"=ARGUMENTS["comment"]}}
			}>
			<cfset uploadRequest["ticket"]["comment"]["uploads"]=[ARGUMENTS["token"]]>
			<cfset tickets.update(ARGUMENTS["ticketId"],uploadRequest,ARGUMENTS["email"])>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Un des paramètres ticketId et/ou token n'a pas été renseigné">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="createTicketFromUser" returntype="Numeric" hint="Crée un ticket avec comme demandeur l'utilisateur référencé par email et retourne son identifiant">
		<cfargument name="name" type="String" required="true" hint="nom et prénom de l'utilisateur e.g Dupond Martin">
		<cfargument name="email" type="String" required="true" hint="Email de l'utilisateur">
		<cfargument name="tags" type="Array" required="true" hint="Liste de tags">
		<cfargument name="subject" type="String" required="true" hint="Sujet du ticket. Obligatoire et ne doit pas etre une chaine vide">
		<cfargument name="description" type="String" required="true" hint="Description du ticket">
		<cfif (TRIM(ARGUMENTS["description"]) NEQ "") AND (TRIM(ARGUMENTS["subject"]) NEQ "")>
			<cfset var tickets=getTickets()>
			<cfset var ticketRequest={
				"ticket"={
					"subject"=ARGUMENTS["subject"],"description"=ARGUMENTS["description"],"tags"=ARGUMENTS["tags"]
				}
			}>
			<cfset var requester_id=getUsers().getUserId(ARGUMENTS["email"])>
			<cfif requester_id LTE 0>
				<cfset ticketRequest["ticket"]["requester"]={"name"=ARGUMENTS["name"], "email"=ARGUMENTS["email"]}>
			<cfelse>
				<cfset ticketRequest["ticket"]["requester_id"]=requester_id>
			</cfif>
			<cfset var ticketInfos=tickets.create(ticketRequest,ARGUMENTS["email"])>
			<cfif ticketInfos["STATUS"]["CODE"] EQ tickets.SUCCESS()>
				<cfreturn VAL(ticketInfos["RESULT"]["ticket"]["id"])>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#ticketInfos.FAULT.MESSAGE#" detail="HTTP CODE : #ticketInfos.STATUS.HTTP_STATUS#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Création de ticket utilisateur"
			detail="Les paramètres description et subject ne doivent pas etre renseignés avec une chaine vide">
		</cfif>
	</cffunction>

	<cffunction access="public" name="createTicketFromUserV2" returntype="Numeric" hint="Crée un ticket avec comme demandeur l'utilisateur référencé par email et retourne son identifiant">
		<cfargument name="prenom" type="String" required="true"
		hint="Prénom de l'utilisateur ou une chaine vide pour ignorer ce paramètre (e.g : L'utilisateur référencé par email existe déjà. Le prénom est donc inutile)">
		<cfargument name="nom" type="String" required="true"
		hint="Nom de l'utilisateur ou une chaine vide pour ignorer ce paramètre (e.g : L'utilisateur référencé par email existe déjà. Le nom est donc inutile)">
		<cfargument name="email" type="String" required="true"
		hint="Email de l'utilisateur. L'utilisateur est automatiquement créé si aucun utilisateur existant ne correspond à la valeur de ce paramètre">
		<cfargument name="application" type="String" required="true"
		hint="Application concernée (Case Insensitive). La valeur doit être une des suivantes : consoview, mytem,mytem360,pilotage,telcost">
		<cfargument name="module" type="String" required="true"
		hint="Module concerné (Case Insensitive). La valeur doit être une des suivantes : m16,m111,m331,m311,m312">
		<cfargument name="description" type="String" required="true" hint="Description du ticket (i.e 1er commentaire ajouté au ticket).
		Obligatoire et ne doit pas etre une chaine vide. Le saut de ligne utilisé doit etre le code ASCII 10">
		<cfargument name="subject" type="String" required="true" hint="Sujet du ticket. Obligatoire et ne doit pas etre une chaine vide">
		<cfset var applicationCustomTag="20934978">
		<cfset var moduleCustomTag="20941458">
		<cfif (TRIM(ARGUMENTS["description"]) NEQ "") AND (TRIM(ARGUMENTS["subject"]) NEQ "")>
			<cfset var tickets=getTickets()>
			<cfset var ticketRequest={
				"ticket"={
					"subject"=ARGUMENTS["subject"],"description"=ARGUMENTS["description"],
					"fields"={"#applicationCustomTag#"=ARGUMENTS["application"],"#moduleCustomTag#"=ARGUMENTS["module"]}
				}
			}>
			<cfset var requester_id=getUsers().getUserId(ARGUMENTS["email"])>
			<cfif requester_id LTE 0>
				<cfset ticketRequest["requester"]={name=ARGUMENTS["prenom"] & " " & ARGUMENTS["nom"], email=ARGUMENTS["email"]}>
			<cfelse>
				<cfset ticketRequest["requester_id"]=requester_id>
			</cfif>
			<cfset var ticketInfos=tickets.create(ticketRequest,ARGUMENTS["email"])>
			<cfif ticketInfos["STATUS"]["CODE"] EQ tickets.SUCCESS()>
				<cfreturn VAL(ticketInfos["RESULT"]["ticket"]["id"])>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#ticketInfos.FAULT.MESSAGE#" detail="HTTP CODE : #ticketInfos.STATUS.HTTP_STATUS#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Création de ticket utilisateur"
			detail="Les paramètres description et subject ne doivent pas etre renseignés avec une chaine vide">
		</cfif>
	</cffunction>
</cfcomponent>