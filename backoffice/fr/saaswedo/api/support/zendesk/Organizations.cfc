<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.zendesk.Organizations"
extends="fr.saaswedo.api.support.zendesk.RestService" hint="Fonctionnalités de Gestion des Organisations Zendesk">
	<cffunction access="public" name="show" returntype="Struct" hint="Retourne les infos concernant une organisation">
		<cfargument name="organizationId" type="Numeric" required="true" hint="Identifiant de l'organisation">
		<cfif ARGUMENTS["organizationId"] GTE 0>
			<cfset var jsonTarget="api/v2/organizations/#ARGUMENTS.organizationId#">
			<cfreturn sendJSONRequest(jsonTarget,GET_METHOD(),{},"")>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les valeurs du paramètre organizationId doit etre positif">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="create" returntype="Struct" hint="Crée une organisation et retourne les infos le concernant">
		<cfargument name="orgaInfos" type="Struct" required="true" hint="Structure contenant les infos de l'organisation à créer">
		<cfset var jsonTarget="api/v2/organizations">
		<cfreturn sendJSONRequest(jsonTarget,POST_METHOD(),ARGUMENTS["orgaInfos"],"")>
	</cffunction>
</cfcomponent>