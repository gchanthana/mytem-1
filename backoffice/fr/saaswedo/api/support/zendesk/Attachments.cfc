<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.zendesk.Attachments" extends="fr.saaswedo.api.support.zendesk.RestService" hint="Fonctionnalités de Gestion des attachements">
	<cffunction access="public" name="show" returntype="Struct"
	hint="Récupère les infos concernant un attachement et retourne la réponse HTTP (application/xml). (Version Beta : fonctionnalité Zendesk non officielle)">
		<cfargument name="attachmentId" type="String" required="true" hint="Identifiant de l'attachement">
		<cfif TRIM(ARGUMENTS["attachmentId"]) NEQ "">
			<cfset var jsonTarget="attachments/" & ARGUMENTS["attachmentId"] & ".xml">
			<cfset var requestResponse=sendRequest(jsonTarget,GET_METHOD(),{ContentType="application/xml"},{})>
			<cfreturn requestResponse>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La valeur du paramètre attachmentId doit etre renseignée">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="uploadContent" returntype="Struct" hint="Upload un fichier et retourne les infos correspondant à l'opération">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier pour l'upload (Incluant l'extension)">
		<cfargument name="fileContent" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="uploadToken" type="String" required="false" default="" hint="Token correspondant à un upload existant <b>(A renseigner en cas d'attachements multiples)</b>">
		<cfif TRIM(ARGUMENTS["fileName"]) NEQ "">
			<cfset var tokenParam=(TRIM(ARGUMENTS["uploadToken"]) EQ "") ? "":"&token=" & TRIM(ARGUMENTS["uploadToken"])>
			<cfset var jsonTarget="api/v2/uploads.json?filename=" & ARGUMENTS["fileName"] & tokenParam>
			<cfset var requestResponse=sendRequest(jsonTarget,POST_METHOD(),{ContentType="application/binary"},{CONTENT=ARGUMENTS["fileContent"]})>
			<cfset var requestResult=getJsonHttpResponse(requestResponse)>
			<cfif requestResult["STATUS"]["CODE"] EQ SUCCESS()>
				<cfreturn requestResult["RESULT"]["upload"]>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#requestResult.FAULT.MESSAGE#" detail="HTTP CODE : #requestResult.STATUS.HTTP_STATUS#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La valeur du paramètre filename doit etre renseignée">
		</cfif>
	</cffunction>
</cfcomponent>