<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.zendesk.RestService" hint="Méthodes d'accès à l'API REST Zendesk">
	<cffunction access="public" name="setRequestAuthorization" returntype="void"
	hint="Définit le login utilisateur, le token utilisateur et l'URL du serveur Zendesk. Le serveur et les credentiels Zendesk de Saaswedo sont utilisés par défaut">
		<cfargument name="username" type="String" required="true" hint="Email du login utilisateur Zendesk">
		<cfargument name="authToken" type="String" required="true" hint="Token d'authentification du login">
		<cfargument name="serverUrl" type="String" required="false" hint="URL du serveur zendesk <b>incluant le protocole et avec un / à la fin</b> e.g https://societe.zendesk.com/">
		<cfif NOT structKeyExists(VARIABLES,"CREDENTIALS")>
			<cfset VARIABLES["CREDENTIALS"]={USER=ARGUMENTS["username"],AUTH_TOKEN=ARGUMENTS["authToken"], SERVER_URL=ARGUMENTS["serverUrl"]}>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="setUserLogin" returntype="void"
	hint="Met à jour le login utilisateur utilisé pour accéder à l'API. Cette méthode doit etre appelée après au moins un appel à setRequestAuthorization()<br>
	<b>Il est recommandé d'utiliser setRequestAuthorization() au lieu de cette méthode</b>">
		<cfargument name="username" type="String" required="true" hint="Email du login utilisateur Zendesk">
		<cfset VARIABLES["CREDENTIALS"]["USER"]=ARGUMENTS["username"]>
	</cffunction>
	
	<cffunction access="public" name="SUCCESS" returntype="Numeric" hint="Retourne la valeur correspondant à une opération effectuée avec succès">
		<cfreturn 1>
	</cffunction>
	
	<cffunction access="public" name="FAILURE" returntype="String" hint="Retourne la valeur correspondant à une opération qui a retourné un code d'erreur">
		<cfreturn -1>
	</cffunction>

	<cffunction access="package" name="serviceEndpoint" returntype="String" hint="Endpoint de l'API REST Zendesk">
		<cfset var propertyKey="SERVER_URL">
		<cfif structKeyExists(VARIABLES["CREDENTIALS"],propertyKey) AND (TRIM(VARIABLES["CREDENTIALS"][propertyKey]) NEQ "")>
			<cfreturn VARIABLES["CREDENTIALS"][propertyKey]>
		<cfelse>
			<cfreturn "https://saaswedo.zendesk.com/">
		</cfif>
	</cffunction>

	<cffunction access="package" name="sendRequest" returntype="Struct" hint="Envoi une requête HTTP vers l'API REST Zendesk. Retourne la réponse HTTP">
		<cfargument name="contextPath" type="String" required="true" hint="Chemin relatif de la cible de la requete">
		<cfargument name="httpMethod" type="String" required="true" hint="Méthode HTTP à utiliser e.g GET_METHOD()">
		<cfargument name="requestHeaders" type="Struct" required="true" hint="Headers de la requete HTTP. Chaque clé correspond au nom d'un header HTTP et est associée à la valeur correspondante.<br>
		La clé ContentType est obligatoire et correspond au type MIME utilisé pour la requete. La clé OnBehalfOf est facultative et correspond à l'mail d'un utilisateur (Impersonate Login)">
		<cfargument name="requestParameters" type="Struct" required="true" hint="Paramètres de la requete HTTP. Structure où chaque clé est le nom d'un paramètre et est associée à la valeur correspondante<br>
		Une clé CONTENT doit etre présente et associée au contenu de la requete lorsque la méthode utilisée est POST_METHOD() ou PUT_METHOD()">
		<cfset var httpHeaders=ARGUMENTS.requestHeaders>
		<cfset var httpParams=ARGUMENTS.requestParameters>
		<cfset var requestUrl=serviceEndpoint() & ARGUMENTS.contextPath>
		<!--- Paramètres d'une requete utilisant la méthode HTTP/GET --->
		<cfif (NOT structIsEmpty(httpParams)) AND ((ARGUMENTS["httpMethod"] EQ GET_METHOD()) OR (ARGUMENTS["httpMethod"] EQ DELETE_METHOD()))>
			<cfset var httpGetParams="">
			<cfloop collection="#httpParams#" item="paramName">
				<cfset httpGetParams=listAppend(httpGetParams,LCASE(paramName) & "=" & httpParams[paramName],"&")>
			</cfloop>
			<cfset requestUrl=requestUrl & "?" & httpGetParams>
		</cfif>
		<!--- Envoi de la requete HTTP --->
		<cfhttp url="#requestUrl#" method="#ARGUMENTS.httpMethod#" charset="utf-8">
			<cfhttpparam type="header" name="Authorization" value="#requestAuthorization()#">
			<cfhttpparam type="header" name="Content-Type" value="#httpHeaders.ContentType#">
			<cfif structKeyExists(httpHeaders,"OnBehalfOf") AND isValid("email",httpHeaders.OnBehalfOf)>
				<cfhttpparam type="header" name="X-On-Behalf-Of" value="#httpHeaders.OnBehalfOf#">
			</cfif>
			<!--- Paramètres d'une requete utilisant la méthode HTTP/POST --->
			<cfif (NOT structIsEmpty(httpParams)) AND ((ARGUMENTS["httpMethod"] EQ POST_METHOD()) OR (ARGUMENTS["httpMethod"] EQ PUT_METHOD()))>
				<cfhttpparam type="body" value="#httpParams.CONTENT#">
			</cfif>
		</cfhttp>
		<!--- Debug : Log URL de la requete HTTP --->
		<cfset var impersonateLogin=structKeyExists(httpHeaders,"OnBehalfOf") ? httpHeaders["OnBehalfOf"]:"N.D">
		<cfset var statusCode=-1>
		<cfif structKeyExists(CFHTTP,"responseHeader") AND structKeyExists(CFHTTP["responseHeader"],"Status_Code")>
			<cfset statusCode=VAL(CFHTTP["responseHeader"]["Status_Code"])>
		</cfif>
		<cflog type="information"
			text="Zendek #ARGUMENTS.httpMethod# (Status:#statusCode#,MIME:#httpHeaders.ContentType#,OnBehalfOf:#impersonateLogin#): #requestUrl#">
		<cfreturn CFHTTP>
	</cffunction>
	
	<cffunction access="package" name="sendJSONRequest" returntype="Struct" hint="Envoi une requête HTTP au format JSON vers l'API REST Zendesk. Retourne une structure au même format que celle retournée par defaultRequestResult()">
		<cfargument name="contextPath" type="String" required="true" hint="Chemin relatif de la cible de la requete (Sans l'extension json)">
		<cfargument name="httpMethod" type="String" required="true" hint="Méthode HTTP à utiliser e.g GET_METHOD()">
		<cfargument name="requestContent" type="Struct" required="true" hint="Structure contenant les clés attendues par l'API Zendesk. Elle sera sérialisée au format JSON si httpMethod est différent de GET_METHOD() et DELETE_METHOD()">
		<cfargument name="onBehalfOf" type="String" required="true" hint="Chaine vide ou email d'un utilisateur pour lequel effectuer l'opération (Impersonate Login)">
		<cfset var requestHeaders={ContentType=JSON_MIME(),OnBehalfOf=ARGUMENTS["onBehalfOf"]}>
		<cfset var requestParams=ARGUMENTS["requestContent"]>
		<cfif (ARGUMENTS["httpMethod"] NEQ GET_METHOD()) AND (ARGUMENTS["httpMethod"] NEQ DELETE_METHOD())>
			<cfset requestParams={CONTENT=serializeJSON(ARGUMENTS["requestContent"])}>
		</cfif>
		<cfset var httpResponse=sendRequest(ARGUMENTS.contextPath & ".json",ARGUMENTS.httpMethod,requestHeaders,requestParams)>
		<!--- Réponse HTTP --->
		<cfreturn getJsonHttpResponse(httpResponse)>
	</cffunction>

	<cffunction access="package" name="getJsonHttpResponse" returntype="Struct"
	hint="Retourne la réprésentation de la réponse HTTP JSON : Structure contenant STATUS, RESULT, FAULT">
		<cfargument name="httpResponseStruct" type="Struct" required="true" hint="Réponse HTTP">
		<cfset var requestResult=defaultRequestResult()>
		<cfset var httpResponse=ARGUMENTS["httpResponseStruct"]>
		<cfset var httpStatusCode=-1>
		<cfif structKeyExists(httpResponse,"responseHeader") AND structKeyExists(httpResponse["responseHeader"],"Status_Code")>
			<cfset httpStatusCode=VAL(httpResponse["responseHeader"]["Status_Code"])>
		</cfif>
		<cfset var jsonResponse="">
		<cfif structKeyExists(httpResponse,"fileContent")>
			<cfset jsonResponse=httpResponse["fileContent"]>
		</cfif>
		<cfset requestResult["STATUS"]["HTTP_STATUS"]=httpStatusCode>
		<cfif isInstanceOf(httpResponse["fileContent"],"java.io.ByteArrayOutputStream")>
			<cfset jsonResponse=httpResponse["fileContent"].toString()>
		</cfif>
		<!--- Opération effectuée avec succès --->
		<cfif (httpStatusCode GTE 200) AND (httpStatusCode LT 300)>
			<cfset requestResult["STATUS"]["CODE"]=SUCCESS()>
			<cfset requestResult["FAULT"]["MESSAGE"]="">
			<cfset requestResult["RESULT"]=jsonResponse>
			<cfif isJSON(jsonResponse)>
				<cfset requestResult["RESULT"]=deserializeJSON(jsonResponse)>
			</cfif>
		<!--- Opération retournant un statut d'erreur --->
		<cfelse>
			<cfset requestResult["FAULT"]["MESSAGE"]=jsonResponse>
			<cfif TRIM(httpResponse["ErrorDetail"]) NEQ "">
				<cfset requestResult["FAULT"]["MESSAGE"]=requestResult["FAULT"]["MESSAGE"] & " (Error Detail : " & httpResponse["ErrorDetail"] & ")">
			</cfif>
		</cfif>
		<cfreturn requestResult>
	</cffunction>

	<cffunction access="package" name="sendXMLRequest" returntype="Struct" hint="Envoi une requête HTTP au format XML vers l'API REST Zendesk. Cette méthode n'est pas implémentée et lève une exception">
		<cfargument name="contextPath" type="String" required="true" hint="Chemin relatif de la cible de la requete (Sans l'extension xml)">
		<cfargument name="httpMethod" type="String" required="true" hint="Méthode HTTP à utiliser">
		<cfargument name="requestParameters" type="Struct" required="true" hint="Structure contenant les paramètres  à fournir dans la requete">
		<cfthrow type="Custom" message="Cette méthode n'est pas implémentée" detail="L'envoi de requetes au format XML à l'API REST Zendesk n'est pas implémenté">
	</cffunction>

	<cffunction access="package" name="defaultRequestResult" returntype="Struct" hint="Retourne le résultat par défaut retourné pour les requete effectuées vers l'API REST Zendesk. La structure contient les champs suivants :<br>
	STATUS : Structure contenant les clés CODE (Code du statut de l'opération effectuée), HTTP_STATUS (Code de retour HTTP e.g 404. Par défaut : 0)<br>
	RESULT : Structure contenant le résultat de la requete (Par défaut : Chaine chaine vide). Le type de la valeur associée dépend du résultat de l'opération effectuée (e.g : Struct, Array, etc...)<br>
	FAULT : Structure contenant les clés MESSAGE (Message lié au code de retour de l'opération et au statut HTTP)">
		<cfreturn {
			STATUS={CODE=FAILURE(), HTTP_STATUS=0}, RESULT={}, FAULT={MESSAGE="Une erreur s'est produite mais le détails n'a pas pu être déterminé"}
		}>
	</cffunction>
	
	<cffunction access="package" name="GET_METHOD" returntype="String" hint="Retourne une valeur correspondant à la méthode HTTP GET">
		<cfreturn "GET">
	</cffunction>
	
	<cffunction access="package" name="POST_METHOD" returntype="String" hint="Retourne une valeur correspondant à la méthode HTTP POST">
		<cfreturn "POST">
	</cffunction>
	
	<cffunction access="package" name="PUT_METHOD" returntype="String" hint="Retourne une valeur correspondant à la méthode HTTP PUT">
		<cfreturn "PUT">
	</cffunction>
	
	<cffunction access="package" name="DELETE_METHOD" returntype="String" hint="Retourne une valeur correspondant à la méthode HTTP DELETE">
		<cfreturn "DELETE">
	</cffunction>
	
	<cffunction access="package" name="JSON_MIME" returntype="String" hint="Retourne le type MIME pour le format JSON">
		<cfreturn "application/json">
	</cffunction>
	
	<cffunction access="package" name="XML_MIME" returntype="String" hint="Retourne le type MIME pour le format XML">
		<cfreturn "application/xml">
	</cffunction>
	
	<cffunction access="package" name="TEXT_MIME" returntype="String" hint="Retourne le type MIME pour le format Texte">
		<cfreturn "text/plain">
	</cffunction>

	<cffunction access="package" name="getUserLogin" returntype="String" hint="Retourne le login utilisateur utilisé pour l'API">
		<cfset var propertyKey="USER">
		<cfif structKeyExists(VARIABLES["CREDENTIALS"],propertyKey) AND (TRIM(VARIABLES["CREDENTIALS"][propertyKey]) NEQ "")>
			<cfreturn VARIABLES["CREDENTIALS"][propertyKey]>
		<cfelse>
			<cfreturn "brice.miramont@saaswedo.com">
		</cfif>
	</cffunction>
	
	<cffunction access="package" name="getUserToken" returntype="String" hint="Retourne le token utilisateur utilisé pour l'API">
		<cfset var propertyKey="AUTH_TOKEN">
		<cfif structKeyExists(VARIABLES["CREDENTIALS"],propertyKey) AND (TRIM(VARIABLES["CREDENTIALS"][propertyKey]) NEQ "")>
			<cfreturn VARIABLES["CREDENTIALS"][propertyKey]>
		<cfelse>
			<cfreturn "b7QoGhCmasPXdIHqVQtxHx9ex2xLdTUuqzW85dEN">
		</cfif>
	</cffunction>
	
	<cffunction access="package" name="requestAuthorization" returntype="String" hint="Retourne la chaine d'authorisation utilisée pour l'authentification des requêtes HTTP">
		<cflog type="information" text="Request Authorization - #getUserLogin()# : #getUserToken()#">
		<cfreturn "Basic " & toBase64(getUserLogin() & "/token:" & getUserToken())>
	</cffunction>
</cfcomponent>