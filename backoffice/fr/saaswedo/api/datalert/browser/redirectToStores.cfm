<cfoutput>
<div id="messageDiv">
	
</div>
<script type="text/javascript">

if (navigator.appVersion.indexOf("Android") != -1)
	document.location.href="https://play.google.com/store/apps/details?id=fr.memobox.databox";
else if (navigator.appVersion.indexOf("iPhone") != -1 || navigator.appVersion.indexOf("iPad") != -1 || navigator.appVersion.indexOf("iPod") != -1)
	document.location.href="https://itunes.apple.com/fr/app/datalert!/id771029268?mt=8";
else
{
	var div = document.getElementById("messageDiv");
	div.textContent = "Datalert! n'est disponible que sur Android et iOS. Votre version : " + navigator.appVersion;
}

</script>
</cfoutput>