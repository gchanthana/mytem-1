<cfcomponent output="true">
			
	<cffunction name="getStoredInfo" access="public" returntype="query" hint="récupérer info device par son id" output="true">
	
		<cfargument name="devId"  required="true" type="string"  hint="device id">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m26.getproxyLog">		
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#devId#" variable="p_uuid" >
				<cfprocresult name="qResulat">
			</cfstoredproc>	
		<cfcatch>
			<cfreturn false>
		</cfcatch>
		</cftry>
		<cfreturn "#qResulat#">
		
	</cffunction>
	
	<!---  enregister info device  --->
	
	<cffunction name="save" output="true" access="public" returntype="boolean">
		
		<cfargument name="action" 		type="numeric" required="true"> <!---  if action =1 ajout else modif --->
		<cfargument name="devId" 		type="string"  required="true">
		<cfargument name="confirmNum" 	type="string"  required="false" default="">
		<cfargument name="phone_no" 	type="string"  required="false" default="">
		<cfargument name="mccmnc" 		type="string"  required="false" default="">
		<cfargument name="tokedid" 		type="string"  required="false" default="">
		<cfargument name="user_agent" 	type="string"  required="false" default="">
		<cfargument name="ip_origin" 	type="string"  required="false" default="">
		<cfargument name="status" 		type="string"  required="false" default="">

		<cfset RequestedTime = now()>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m26.IUProxyLg">		
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" 		value="#trim(devId)#"   variable="p_devid">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" 		value="#confirmNum#" 	variable="p_confirmNum">
			<cfprocparam type="in"  cfsqltype="CF_SQL_TIMESTAMP" 	value="#RequestedTime#" variable="p_datetTimeRequest">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR"  	value="#phone_no#" 		variable="p_phoneNum ">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" 		value="#mccmnc#" 		variable="p_mccmnc">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" 		value="#tokedid#" 		variable="p_tokenid">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" 		value="#user_agent#" 	variable="p_user_agent">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" 		value="#ip_origin#" 	variable="p_ip_origin">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" 		value="#status#" 		variable="p_Status">
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" 		value="#action#" 		variable="p_action">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 		variable="p_retour">				
		</cfstoredproc>	
			
		<cfif p_retour eq 1 >
			<cfreturn true >
			<cfelse>
				<cfreturn false >
		</cfif>
			
	</cffunction>
	
</cfcomponent>