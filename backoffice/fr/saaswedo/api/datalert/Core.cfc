<cfcomponent output="false">
	
	<cfset url_server			= "https://datalert.saaswedo.com"> 
	
	<!--- <cfset url_server		= "https://"&CGI.HTTP_HOST>  ---><!--- prod --->
	
	<!--- <cfset url_server		= "http://"&CGI.HTTP_HOST&"/backoffice"> ---> <!--- pour dev --->
	
	<cfset Debut			=" [Datalert Proxy]-Démarrage de " >
	<cfset lab_recevedData	=" [Datalert Proxy]-les données interceptées par le proxy : " >
	<cfset lab_sentData		=" [Datalert Proxy]-les données transmet par le proxy à l'API SWD : " >
	<cfset invoke			=" [Datalert Proxy]-invoker l'API SWD - methode : " >
	<cfset responseSWD		=" [Datalert Proxy]-réponse renvoyée au proxy - methode : " >
	<cfset responseProxy	=" [Datalert Proxy]-réponse renvoyée au mobile - methode : " >
	<cfset exce				=" [Datalert Proxy]-Exception levée dans la methode : " >
	<cfset respHttp			=" response http : " >
	<cfset link			    ="URL cfm : " >
	<cfset info				="Attention : la même réponse est renvoyée au mobile" >
	
	<!--- 	<cfset authorization="Basic TG9naW46cGFzc3dvcmQ=55555"> <!---  {"Message":"Authentication failed"}  --->  --->
	<!--- 	<cfset authorization="Basic TG9naW46cGFzc3dvcmQ=fghdfg"> <!--- {"Message":"Internal Error"}  ---> --->
	<cfset authorization="Basic TG9naW46cGFzc3dvcmQ=">

	<cfset newLine = Chr(13) & Chr(10)>
	
	<!---  table mnt_event  --->
	<cfset idMntMobileProxy		 	= 241>
	<cfset idMntEventProxyAPI    	= 242>
	<cfset idMntEventAPIProxy    	= 243>
	<cfset idMntEventProxyMobile 	= 244>
	<cfset idMntEventProxyException = 254>
	
	<!--- permet de traiter l'exception et former un string  --->
	
	<cffunction access="public" name="bodyException" returntype="String">
		
		<cfargument name="cfCatchObject" type="any"  required="true">
		
		<cfset message="">
		<cfset detail="">
		<cfset tag_context="">
		
		<cftry>
			<cfset message="Error Handling FAILED. Cause : #cfCatchObject.Message# #newLine#" >
			
			<cfif LEN(TRIM(cfCatchObject.Detail)) GT 0>
				<cfset detail= "Detail : #cfCatchObject.Detail# #newLine#" >
			</cfif>
			
			<cfsavecontent variable="bodyContext">
				<cfoutput>
					<cfif structKeyExists(cfCatchObject,"TagContext")>
						<cfdump var="#cfCatchObject.TagContext#" format="text">
					</cfif>
				</cfoutput>
			</cfsavecontent>

			<cfset body= "#message# #detail#  Tag content : #trim(bodyContext)#">

			<cfmail from="Datalert proxy exception <monitoring@saaswedo.com>" 
					to="monitoring@saaswedo.com" 
					server="mail-cv.consotel.fr" 
					port="25" type="text/html"
					subject="[Datalert Proxy] Exception-Core cfc">
					<cfdump var="#cfCatchObject#">	
			</cfmail>	
			
			<cfreturn body>
		<cfcatch>
			<cfmail from="MLog <monitoring@saaswedo.com>" 
					to="monitoring@saaswedo.com" 
					server="mail-cv.consotel.fr" 
					port="25" type="text/html"
					subject="[Datalert Proxy] Exception-Core cfc">
					<cfdump var="#CFCATCH#">						
			</cfmail>
		</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- permet d'enregistrer le log dans la table mt_detail  --->
	<cffunction access="public" name="saveLog" output="true" returntype="void">
		
		<cfargument name="uid" 		type="string"  required="true">
		<cfargument name="subject" 	type="string"  required="true">
		<cfargument name="body" 	type="string"  required="true">	
		<cfargument name="idmnt" 	type="numeric" required="true">
				
		<cftry>
			<cfset mLog=createObject('component','fr.consotel.api.monitoring.MLog')>
			<cfset mLog.setIDMNT_CAT(21)> <!--- id=21 le catégorie pour datalert  --->
			<cfset mLog.setIDMNT_ID(idmnt)>
			<cfset mLog.setSUBJECTID(subject)>
			<cfset mLog.setBODY(body)>
			<cfset mLog.setUUID(uid)>
						
			<cfset createObject('component','fr.saaswedo.api.datalert.DatalertLogger').logIt(mLog)>
			
			<cfcatch type="any">
				<cfmail from="MLog <monitoring@saaswedo.com>" 
						to="monitoring@saaswedo.com" 
						server="mail-cv.consotel.fr" 
						port="25" type="text/html"
						subject="[Datalert Proxy] saveLog - Core CFC">
						<cfdump var="#cfcatch#">						
				</cfmail>
			</cfcatch>
		</cftry>
	
	</cffunction>
		
</cfcomponent>