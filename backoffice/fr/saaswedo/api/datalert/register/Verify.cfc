<cfcomponent output="true" extends="fr.saaswedo.api.datalert.Core">
	
	<cffunction access="private" name="URl_VERIFYUSER" returntype="String">
		<cfset url_verify="#url_server#/api/da/vd.cfm">
		<cfreturn  url_verify>
	</cffunction>
	
	<cffunction name="init" access="private" output="true" returntype="void">
	
		<cfargument name="httpRequestData" type="String" required="true">
		
		<cfset bdd=createobject("component","fr.saaswedo.api.datalert.Bdd")>
		<cfset jsonRequest=deserializejson(httpRequestData)>
		<cfset serverName=URl_VERIFYUSER()>
		
		<cfset confirmNum=0 >
		<cfset mccmnc="" >
		<cfset tokenId="">

		<cfif structKeyExists(#jsonRequest#,"devId") and jsonRequest.devId neq "">
			<cfset  devId = #jsonRequest.devId# >
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"tokenId") and jsonRequest.tokenId neq "">
			<cfset  tokenId = Replace(#jsonRequest.tokenId#," ","-", "ALL")>
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"mccmnc") and jsonRequest.mccmnc neq "">
			<cfset  homeMCCMNC = Replace(#jsonRequest.mccmnc#,"-","","ALL")>
			<cfset jsonRequest.mccmnc=#homeMCCMNC#>
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"cfmNum") and jsonRequest.cfmNum neq "">
			<cfset  confirmNum = #jsonRequest.cfmNum# >
		</cfif>
		
		<cfset requestData=serializejson(jsonRequest)>

		<cfset causeB="cause :Num confirmation (cfmNum) est obligatoire #newLine#">
				
		<cflog  text="initialisation variables pour verifyUser = OK " type="information">
		
	</cffunction>
	
	<cffunction name="verify_user" access="public" returntype="any" output="true">
				
		<cfargument name="httpRequestData" type="String" required="true">
		<cfargument name="user_agent" type="String" required="true">
		<cfargument name="ip_origin" type="String" required="true">
		
		<cfset uuid =createuuid() >
			
		<cftry>
			<cfset init(httpRequestData)>	
			
			<!--- logger les données reçues --->
			<cfset saveLog(uuid,"#Debut# VerifyUser","#lab_recevedData# #httpRequestData# #newLine# user_agent: #user_agent# #newLine# ip_origin : #ip_origin# ",idMntMobileProxy) >
			
			<cfif confirmNum neq 0 > <!---  si n° confirmation n'est pas fourni  --->
				
				<cfhttp method="post" url="#serverName#" useragent="#user_agent#">
					<cfhttpparam type="header" name="Authorization" value="#authorization#"><!--- Authentication basique obligatoire sinon erreur  --->
					<cfhttpparam type="header" name="Accept" value="application/json">
					<cfhttpparam type="header" name="UUID" value="#devId#"> <!--- obligatoire sinon erreur ({"Message":"UUID not present in the HTTP header."} ou  {"Message":"UUID : bad format"} par exemple devId= "  " ) --->
	 				<cfhttpparam type="formfield" name="data" value="#requestData#"> 	
			 	</cfhttp>
			
				<!---  logger les données envoyées à l'API SWD  --->	
				<cfset saveLog(uuid,"#invoke# verify_user","#lab_sentData# #requestData# #newLine# avec UUID comme Header http = #devId# #newLine# #link# #serverName#",idMntEventProxyAPI) >
				
				<cfif cfhttp.Responseheader.Status_Code eq "200">
					
					<cflog type="information" text="reponse http OK - methode : verify_user, on continue pour verifier la validite de Num confirmation envoye au telephone">
					<!--- logger la réponse de l'API SWD  --->
					<cfset saveLog(uuid,"#responseSWD# verify_user","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName#",idMntEventAPIProxy) >
					<cfset devInfo=bdd.getStoredInfo(devId)>
					
					<cfif (devInfo.confirmNum eq confirmNum ) AND  (dateadd("n",3,devInfo.dateTimeRequest) gt now() )  >
						<cfset  phoneNum = devInfo.PhoneNum >
						<cfset  resRegister=registerDevice(devId,tokenId,phoneNum,homeMCCMNC,user_agent,ip_origin,confirmNum,2)>
						<cflog type="information" text="update data ok - mmcmnc, ip_origin et phone num - methode : verify_user">
						<cfif resRegister eq true>
							<cflog type="information" text="response http OK - methode : verify_user, on renvoie resp:ok ">
							<cfset responeJson = '{"resp":"OK"}' >
							<!---  logger la réponse envoyée au mobile --->
							<cfset saveLog(uuid,"#responseProxy# verify_user","#respHttp# #responeJson#",idMntEventProxyMobile) >
							<cfreturn responeJson>	
						</cfif>
					<cfelse>
						<cflog type="information" text="Num confirmation est invalide, on renvoie 425 - methode : verify_user">
						<cfset responeJson = '{"resp":"Invalid cfmNum"}'>  
						<cfheader name="status" value="425">
						<!---  logger la réponse envoyée au mobile --->
						<cfset saveLog(uuid,"#responseProxy# verify_user","#respHttp# #responeJson# #newLine# cause : soit Num confirmation reçu (#confirmNum#) != Num confirmation dans BDD (#devInfo.confirmNum#) ou temps de verification > 3 minutes",idMntEventProxyMobile) >
						<cfreturn responeJson>	
					</cfif>
				<cfelse> <!--- logger le message de l'API SWD en cas d'erreur, par exemple: {"Message":"UUID not present in the HTTP header."} et le meme message est renvoyé au mobile--->
			 		<cfset saveLog(uuid,"#responseSWD# verify_user","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName# #newLine# #info#",idMntEventAPIProxy) >
					<cflog type="information" text="reponse http not OK - methode : verify_user ">
					<cfset responeJson = cfhttp.Filecontent >
					<cfreturn responeJson>
				</cfif>
			<cfelse>
				<cflog type="information" text="Num confirmation est obligatoire, on renvois 426 - methode : verify_user ">
				<cfset responeJson = '{"resp":"need cfmNum"}'>
				<cfheader name="status" value="426">
				<!---  logger la réponse envoyée au mobile --->
				<cfset saveLog(uuid,"#responseProxy# verify_user","#respHttp# #responeJson# #newLine# #causeB#",idMntEventProxyMobile) >
				<cfreturn responeJson>
			</cfif>		
		<cfcatch type="any"> <!---  logger l'exception --->
			<cfthread action="sleep" duration="1000"/>
			<cfset saveLog(uuid,"#exce# verify_user",bodyException(CFCATCH),idMntEventProxyException) >
			<cfset responeJson = '{"resp":"#cfcatch.Message#" }' >
			<cfreturn responeJson>
		</cfcatch>
		</cftry>
		
	</cffunction>	

	<cffunction name="registerDevice" returntype="boolean" access="private" output="true">
					
		<cfargument name="devId" 		type="string" required="true">
		<cfargument name="tokenId"  	type="string" required="true">
		<cfargument name="phoneNum" 	type="string" required="true">
		<cfargument name="homeMCCMNC" 	type="string" required="true">
		<cfargument name="user_agent" 	type="string" required="true">
		<cfargument name="ip_origin" 	type="string" required="true">
		<cfargument name="confirmNum" 	type="string" required="true">
		<cfargument name="action" 	    type="numeric" required="true">
		
		<cfset bdd=createobject("component","fr.saaswedo.api.datalert.Bdd")>
		<cfset res=bdd.save(action,devId,confirmNum,phoneNum,homeMCCMNC,tokenId,user_agent,ip_origin,'Registered')>
		<cfreturn res>
			
	</cffunction>
		
</cfcomponent>