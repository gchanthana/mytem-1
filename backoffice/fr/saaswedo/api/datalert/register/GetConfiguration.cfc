<cfcomponent output="true" extends="fr.saaswedo.api.datalert.Core">
		
	<cffunction name="get_config" access="public" returntype="any" output="true">
			
		<cfargument name="httpRequestData" 	type="String" required="false">
		<cfargument name="user_agent"		type="String" required="false">
		<cfargument name="ip_origin" 		type="String" required="false">
	
		<cfset uuid =createuuid() >

		<cftry>	
			<cfset jsonRequest=deserializejson(httpRequestData)>
				
			<cfif structKeyExists(#jsonRequest#,"devId") and jsonRequest.devId neq "">
				<cfset devId= #jsonRequest.devId# >
				<cfset cause=" cause :  devId = #devId# non trouvé dans la base SWD">
			</cfif>
		
			<cfset bdd=createobject("component","fr.saaswedo.api.datalert.Bdd")>
			
			<!--- logger les données reçues --->
			<cfset saveLog(uuid,"#Debut# get_config","#lab_recevedData# #httpRequestData# #newLine# user_agent: #user_agent# #newLine# ip_origin : #ip_origin#",idMntMobileProxy) >

			<cfset devInfo=bdd.getStoredInfo(devId)>

			<cfif devInfo.devid neq "">
				
				<cfset responeJson = '{"resp":"OK", "PSCfg":{"bg":{"md":"u","intv":10,"pkt":40,"w":0}},"CUCfg":{"bg":{"wo":0,"intv":10,"w":0}},"DWCfg":{"cfgnxt":1440,"wkup_intv":10,"gps":0},"BUCfg":{"bg":{"wo":1,"cmpr":"gzip","intv":1440,"w":0}},"AUCfg":{"bg":{"wo":1,"cmpr":"gzip","intv":1440,"w":0}}}'>
			
				<cflog type="information" text=" resp ok - methode : get_config">
				<!---  logger la réponse envoyée au mobile --->
				<cfset saveLog(uuid,"#responseProxy# get_config","#respHttp# #responeJson#",idMntEventProxyMobile) >
				<cfreturn responeJson>
			<cfelse>
				<cflog type="information" text="device non trouve dans base SWD - methode : get_config">
				<cfset responeJson = '{"resp":"Device is not registered"}' >
				<!---  logger la réponse envoyée au mobile --->
				<cfset saveLog(uuid,"#responseProxy# get_config","#respHttp# #responeJson# #newLine# #cause#",idMntEventProxyMobile) >
				<cfreturn responeJson>
			</cfif>
		<cfcatch type="any"> <!---  logger l'exception --->
			<cfthread action="sleep" duration="1000"/>
			<cfset saveLog(uuid,"#exce# get_config",bodyException(CFCATCH),idMntEventProxyException) >
			<cfset responeJson = '{"resp":"#cfcatch.Message#" }' >
			<cfreturn responeJson>
		</cfcatch>
		</cftry>
		
	</cffunction>

</cfcomponent>