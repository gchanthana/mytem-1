<cfcomponent author="Cedric" hint="POC de chargement du parc MDM (Citrix)">
	<cffunction access="public" name="import" returntype="void" hint="Importe tous les devices enrollés du parc MDM">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM">
		<!--- v1 : Import synchrone séquentiel des devices par pages de 50 devices --->
		<cfset var infos=ARGUMENTS.mdmInfos>
		<cfset var MAX_DEVICE_PER_PAGE=50>
		<cfset var deviceList=[]>
		<!--- Identifiant du payload --->
		<cfset var uuid=createUUID()>
		<!--- Numéro de page et nombre de devices importés pour la page en cours --->
		<cfset var pageNum=1>
		<cfset var importedCount=0>
		<!--- Liste de payload (i.e Liste de liste de devices) : Chaque payload correspond à une page (i.e Liste de devices) --->
		<cfset var payloadList=[]>
		<cfset var initTick=getTickCount()>
		<cfset var startTick=initTick>
		<cfset var everyWanDevice=getRemoteService(infos,"EveryWanDevice")>
		<cfset var wsTickCount=getTickCount() - startTick><!--- Tick: Instanciation du Web Service --->
		<!--- Récupération de la liste des devices --->
		<cfset startTick=getTickCount()>
		<cfset var deviceIdList=everyWanDevice.getAllDevices()>
		<cfset var allDeviceTickCount=getTickCount() - startTick><!--- Tick: Récupération de tous les devices (+ filtrage) --->
		<cfset var loadDeviceListDuration=allDeviceTickCount / 1000>
		<cflog type="information" text="[Citrix] MDM devices list loaded (#loadDeviceListDuration# secs)">
		<cfif isDefined("deviceIdList")>
			<cfset var deviceCount=arrayLen(deviceIdList)>
			<cfset var payloadStartTick=getTickCount()>
			<cfloop index="i" from="1" to="#deviceCount#">
				<cfset startTick=getTickCount()>
				<cfset var deviceId=deviceIdList[i]>
				<cfset var serial=toString(deviceId.getSerialNumber())>
				<cfset var imei=toString(deviceId.getImei())>
				<cfset var managedStatus=toString(everyWanDevice.getManagedStatus(serial,imei))>
				<!--- Filtrage du statut d'enrollment --->
				<cfif compareNoCase(managedStatus,"ZDM") EQ 0>
					<cfset var lastUser=toString(everyWanDevice.getLastUser(serial,imei))>
					<cfset var device={
						marque="",modele="",platform="",imei=" " & imei,num_serie=" " & serial,collaborateur="",matricule="",username_mdm=lastUser,
						id_mdm=" " & toString(deviceId.getStrongID()),udid="",num_sim="",num_tel="",id_activesync="",operateur="",
						mcc=" 0",mnc=" 0",imsi="",rownum=1
					}>
					<!--- Champs MYT-652 --->
					<cfset structAppend(device,{
						principal="N.A",country="N.A",manufacturer="N.A",email_address="N.A",status_code="N.A",employee_owned="N.A",
						compliance="N.A",client_version="N.A",mdm_enabled="N.A",last_connected_at="N.A",activeSync_UUID="N.A",
						activeSync_lastSync_attempt="N.A",wifimac="N.A",device_encryption="N.A",last_MDM_CheckIn="N.A",registered_on="N.A"
					},TRUE)>
					<!--- Récupération des infos de chaque device --->
					<cfset var deviceInfos=everyWanDevice.getDeviceInfo(serial,imei)>
					<cfset var deviceProps=javaCast("null",0)>
					<cfif isDefined("deviceInfos")>
						<cfset deviceProps=deviceInfos.getDeviceProperties()>
						<!--- Propriétés rattachées au niveau des infos du device (API Citrix) --->
						<cfset var last_connected_at=deviceInfos.getLastAuthDate()>
						<cfif isDefined("last_connected_at")>
							<cfset device.last_connected_at=last_connected_at.getTime()>
						</cfif>
						<cfset var registered_on=deviceInfos.getFirstConnectionDate()>
						<cfif isDefined("registered_on")>
							<cfset device.registered_on=deviceInfos.getFirstConnectionDate().getTime()>
						</cfif>
					</cfif>
					<cfif isDefined("deviceProps")>
						<cfset var isSupportedOS=FALSE>
						<cfset var osVersion="">
						<cfset var productName="">
						<cfset var propCount=arrayLen(deviceProps)>
						<!--- Parcours de la liste des propriétés de chaque device pour en garder certaines (e.g PhoneNumber,ICCID) --->
						<cfloop index="j" from="1" to="#propCount#">
							<cfset var deviceProperty=deviceProps[j]>
							<cfset var propName=toString(deviceProperty.getName())>
							<cfif LEN(TRIM(propName)) GT 0>
								<cfset var propValue=toString(deviceProperty.getValue())>
								<cfif compareNoCase(propName,"TEL_NUMBER") EQ 0>
									<cfset device.num_tel=" " & propValue>
								<cfelseif compareNoCase(propName,"SYSTEM_PLATFORM") EQ 0>
									<cfset device.platform=propValue>
								<cfelseif compareNoCase(propName,"SYSTEM_OS_VERSION") EQ 0>
									<cfset osVersion=" " & propValue>
								<cfelseif compareNoCase(propName,"ICCID") EQ 0>
									<cfset device.num_sim=" " & propValue>
								<cfelseif compareNoCase(propName,"CARRIER") EQ 0>
									<cfset device.operateur=propValue>
								<cfelseif compareNoCase(propName,"SYSTEM_OEM") EQ 0>
									<cfset device.marque=propValue>
								<cfelseif compareNoCase(propName,"PRODUCT_NAME") EQ 0>
									<cfset productName=propValue>
								<cfelseif compareNoCase(propName,"UDID") EQ 0>
									<cfset device.udid=propValue>
								<cfelseif compareNoCase(propName,"SIM_MCC") EQ 0>
									<cfset device.mcc=" " & propValue>
								<cfelseif compareNoCase(propName,"SIM_MNC") EQ 0>
									<cfset device.mnc=" " & propValue>
								</cfif>
							</cfif>
						</cfloop>
						<!--- Propriétés dont les clés dépendent de l'OS --->
						<cfif findNoCase("iOS",device.platform) GT 0>
							<cfset device.modele=productName>
							<cfset isSupportedOS=TRUE>
						<cfelseif findNoCase("Android",device.platform) GT 0>
							<cfset device.modele=device.marque>
							<cfset isSupportedOS=TRUE>
						</cfif>
						<!--- Ajout de la version de l'OS --->
						<cfset device.platform=device.platform & ((LEN(TRIM(osVersion)) GT 0) ? " " & osVersion:"")>
						<cfif isSupportedOS>
							<!--- Ajout du device dans la liste : Si la liste n'est pas remplie --->
							<cfif importedCount LT MAX_DEVICE_PER_PAGE>
								<cfset importedCount=importedCount+1>
								<cfset device.rownum=importedCount>
								<cfset var perDeviceTickCount=getTickCount() - startTick><!--- Tick: Pour chaque device --->
								<cfset device.perDeviceTickCount=perDeviceTickCount>
								<cfset deviceList[importedCount]=device>
							</cfif>
						</cfif>
					</cfif>
				</cfif>
				<!--- Ajout du payload de chaque page remplie ou quand il n'y a plus de device à ajouter --->
				<cfif arrayLen(deviceList) GT 0>
					<cfif (importedCount EQ MAX_DEVICE_PER_PAGE) OR (i EQ deviceCount)>
						<cfset var importDate=NOW()>
						<cfset var payload={
							mdm_vendor=infos.MDM_TYPE,version_flux="1.0",mdm_url=getMdmEndpointURL(infos),
							date_extraction=lsDateFormat(importDate,"dd/mm/yyyy") & " " & lsTimeFormat(importDate,"HH:mm:ss"),
							UUID_chargement=uuid,page_number=pageNum,nb_devices=importedCount,IDRACINE=infos.IDGROUPE,devices=deviceList
						}>
						<cfset var perPayloadTickCount=getTickCount() - payloadStartTick><!--- Tick: Pour chaque payload --->
						<cfset payload.perPayloadTickCount=perPayloadTickCount>
						<cfset payload.wsTickCount=wsTickCount>
						<cfset payload.allDeviceTickCount=allDeviceTickCount>
						<cfset importPayload(payload)>
						<!--- Création d'une nouvelle liste s'il y a des devices restants --->
						<cfif importedCount LT deviceCount>
							<cfset pageNum=pageNum+1>
							<cfset importedCount=0>
							<cfset deviceList=[]>
							<cfset payloadStartTick=getTickCount()>
						</cfif>
					</cfif>
				</cfif>
			</cfloop>
		</cfif>
		<cfset var totalTick=(getTickCount() - initTick) / 1000>
		<cflog type="information" text="MDM Pool imported (#totalTick# secs)"> 
	</cffunction>
	
	<cffunction access="private" name="importPayload" returntype="void" hint="Import du payload dans la base">
		<cfargument name="payload" type="Struct" required="true" hint="Payload : Une page de liste de devices">
		<cfset var procedureName="PKG_MDM.import_parc_mdm">
		<cftry>
			<cfset var startTick=getTickCount()>
			<cfset var jsonPayload=serializeJSON(ARGUMENTS.payload)>
			<cfset var jsonTick=getTickCount() - startTick><!--- Tick : Sérialisation JSON --->
			<cflog type="information" text="[POC_MdmPool] MDM Pool payload JSON-Serialized (#jsonTick# ms)">
			<cfset startTick=getTickCount()>
			<cfstoredproc datasource="ROCOFFRE" procedure="#procedureName#">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#jsonPayload#">
				<cfprocresult name="qImportMdmPool">
			</cfstoredproc>
			<cfset var dbTick=(getTickCount() - startTick) / 1000><!--- Tick : Insertion dans la base --->
			<cflog type="information" text="[POC_MdmPool] MDM Pool payload imported (#dbTick# secs)">
			<cfif qImportMdmPool.recordcount GT 0>
				<cfif VAL(qImportMdmPool["STATUS"][1]) GT 0>
					<cfset var errorMsg=qImportMdmPool["MESSAGE"][1]>
					<cfthrow type="Custom" message="#procedureName# error: #errorMsg#">
				</cfif>
			</cfif>
			<cfcatch type="Any">
				<cflog type="Error" text="[POC_MdmPool] Payload import error: #CFCATCH.message# (#CFCATCH.detail#)">
				<cfmail from="monitoring@saaswedo.com" to="cedric.rapiera@saaswedo.com"
				priority="highest" type="html" subject="[POC MDM] Erreur">
					<cfoutput>
						<b>Exception :</b><br>#CFCATCH.message# (#CFCATCH.detail#)<hr>
					</cfoutput>
					<cfif isDefined("jsonPayload")>
						<cfoutput>
							<hr><b>JSON :</b><br>#jsonPayload#<hr>
						</cfoutput>
					</cfif>
					<cfif isDefined("qImportMdmPool")>
						<cfdump var="#qImportMdmPool#" label="#procedureName#"><hr>
					</cfif>
					<cfdump var="#CFCATCH.tagContext#" label="Tag context">
				</cfmail>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- Accès au Web Service du MDM --->
	
	<cffunction access="private" name="getRemoteService" returntype="Any">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM">
		<cfargument name="serviceName" type="String" required="true" hint="Identifiant de la racine">
		<cfset var infos=ARGUMENTS.mdmInfos>
		<cfset var stubEndpoint=getMdmEndpointURL(infos) & "/services/" & ARGUMENTS.serviceName & "?wsdl">
		<cfset var stub=createObject("webservice",stubEndpoint)>
		<cfif LEN(TRIM(mdmInfos.USERNAME)) GT 0>
			<cfset stub.setUsername(mdmInfos.USERNAME)>
			<cfset stub.setPassword(mdmInfos.PASSWORD)>
		</cfif>
		<cfreturn stub>
	</cffunction>
	
	<cffunction access="private" name="getMdmEndpointURL" returntype="String" hint="Retourne l'URL d'accès à l'API du serveur MDM">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM">
		<cfset var infos=ARGUMENTS.mdmInfos>
		<cfreturn (infos.USE_SSL ? "https":"http") & "://" & infos.MDM>
	</cffunction>
</cfcomponent>