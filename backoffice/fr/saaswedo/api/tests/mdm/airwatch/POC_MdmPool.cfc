<cfcomponent author="Cedric" hint="POC de chargement du parc MDM (Airwatch)">
	<cffunction access="public" name="import" returntype="void" hint="Importe tous les devices enrollés du parc MDM">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM">
		<cfset var IsoToDate=CreateObject("component","fr.saaswedo.api.tests.mdm.IsoDateUtil")>
		<cfset var iOS_Device=FALSE>
		<cfset var android_Device=FALSE>
		<!--- v1 : Import synchrone séquentiel des devices par pages de 50 devices --->
		<cfset var infos=ARGUMENTS.mdmInfos>
		<cfset var MAX_DEVICE_PER_PAGE=50>
		<cfset var deviceList=[]>
		<!--- Identifiant du payload --->
		<cfset var uuid=createUUID()>
		<!--- Numéro de page et nombre de devices importés pour la page en cours --->
		<cfset var pageNum=1>
		<cfset var importedCount=0>
		<!--- Liste de payload (i.e Liste de liste de devices) : Chaque payload correspond à une page (i.e Liste de devices) --->
		<cfset var payloadList=[]>
		<cfset var initTick=getTickCount()>
		<cfset var wsTickCount=0><!--- Tick: Instanciation du Web Service --->
		<!--- Récupération de la liste des devices --->
		<cfset var startTick=initTick>
		<cfset var mdmDeviceList=getActiveDevices(infos)>
		<cfset var allDeviceTickCount=getTickCount() - startTick><!--- Tick: Récupération de tous les devices (+ filtrage) --->
		<cfset var loadDeviceListDuration=allDeviceTickCount / 1000>
		<cflog type="information" text="[Airwatch] MDM devices list loaded (#loadDeviceListDuration# secs)">
		<cfset var deviceCount=arrayLen(mdmDeviceList)>
		<cfset var payloadStartTick=getTickCount()>
		<cfloop index="i" from="1" to="#deviceCount#">
			<cfset startTick=getTickCount()>
			<cfset var mdmDevice=mdmDeviceList[i]>
			<cfset var device={
				marque=mdmDevice.platform,modele=mdmDevice.model,platform=mdmDevice.platform,imei=" " & mdmDevice.imei,num_serie=" " & mdmDevice.serialNumber,
				collaborateur="",matricule="",username_mdm=mdmDevice.username,id_mdm=" " & VAL(mdmDevice.id.value),udid=mdmDevice.udid,num_sim="",
				num_tel=" " & mdmDevice.PhoneNumber,id_activesync="",operateur="",mcc=" " & VAL(mdmDevice.deviceMCC.SIMMCC),mnc=" 0",imsi="",rownum=1
			}>
			<!--- Champs MYT-652 --->
			<cfset structAppend(device,{
				principal="N.A",country="N.A",manufacturer=mdmDevice.platform,email_address=mdmDevice.userEmailAddress,status_code=mdmDevice.EnrollmentStatus,
				employee_owned=mdmDevice.ownership,compliance=mdmDevice.complianceStatus,client_version="N.A",mdm_enabled=mdmDevice.EnrollmentStatus,
				last_connected_at=IsoToDate.ISOToDateTime(dmdmDevice.lastSeen),activeSync_UUID="N.A",activeSync_lastSync_attempt="N.A",wifimac=" " & mdmDevice.macAddress,
				device_encryption="N.A",last_MDM_CheckIn=IsoToDate.ISOToDateTime(dmdmDevice.lastSeen),registered_on=IsoToDate.ISOToDateTime(dmdmDevice.lastEnrolledOn)
			},TRUE)>
			<!--- Ajout du device dans la liste : Si la liste n'est pas remplie --->
			<cfif importedCount LT MAX_DEVICE_PER_PAGE>
				<cfset importedCount=importedCount+1>
				<cfset device.rownum=importedCount>
				<cfset var perDeviceTickCount=getTickCount() - startTick><!--- Tick: Pour chaque device --->
				<cfset device.perDeviceTickCount=perDeviceTickCount>
				<cfset deviceList[importedCount]=device>
			</cfif>
			<!--- Ajout du payload de chaque page remplie ou quand il n'y a plus de device à ajouter --->
			<cfif arrayLen(deviceList) GT 0>
				<cfif (importedCount EQ MAX_DEVICE_PER_PAGE) OR (i EQ deviceCount)>
					<cfset var importDate=NOW()>
					<cfset var payload={
						mdm_vendor=infos.MDM_TYPE,version_flux="1.0",mdm_url=getMdmEndpointURL(infos),
						date_extraction=lsDateFormat(importDate,"dd/mm/yyyy") & " " & lsTimeFormat(importDate,"HH:mm:ss"),
						UUID_chargement=uuid,page_number=pageNum,nb_devices=importedCount,IDRACINE=infos.IDGROUPE,devices=deviceList
					}>
					<cfset var perPayloadTickCount=getTickCount() - payloadStartTick><!--- Tick: Pour chaque payload --->
					<cfset payload.perPayloadTickCount=perPayloadTickCount>
					<cfset payload.wsTickCount=wsTickCount>
					<cfset payload.allDeviceTickCount=allDeviceTickCount>						
					<cfset importPayload(payload)>
					<!--- Création d'une nouvelle liste s'il y a des device restants --->
					<cfif importedCount LT deviceCount>
						<cfset pageNum=pageNum+1>
						<cfset importedCount=0>
						<cfset deviceList=[]>
						<cfset payloadStartTick=getTickCount()>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		<cfset var totalTick=(getTickCount() - initTick) / 1000>
		<cflog type="information" text="MDM Pool imported (#totalTick# secs)">
	</cffunction>
	
	<cffunction access="private" name="importPayload" returntype="void" hint="Import du payload dans la base">
		<cfargument name="payload" type="Struct" required="true" hint="Payload : Une page de liste de devices">
		<cfset var procedureName="PKG_MDM.import_parc_mdm">
		<cftry>
			<cfset var startTick=getTickCount()>
			<cfset var jsonPayload=serializeJSON(ARGUMENTS.payload)>
			<cfset var jsonTick=getTickCount() - startTick><!--- Tick : Sérialisation JSON --->
			<cflog type="information" text="[POC_MdmPool] MDM Pool payload JSON-Serialized (#jsonTick# ms)">
			<cfset startTick=getTickCount()>
			<cfstoredproc datasource="ROCOFFRE" procedure="#procedureName#">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#jsonPayload#">
				<cfprocresult name="qImportMdmPool">
			</cfstoredproc>
			<cfset var dbTick=(getTickCount() - startTick) / 1000><!--- Tick : Insertion dans la base --->
			<cflog type="information" text="[POC_MdmPool] MDM Pool payload imported (#dbTick# secs)">
			<cfif qImportMdmPool.recordcount GT 0>
				<cfif VAL(qImportMdmPool["STATUS"][1]) GT 0>
					<cfset var errorMsg=qImportMdmPool["MESSAGE"][1]>
					<cfthrow type="Custom" message="#procedureName# error: #errorMsg#">
				</cfif>
			</cfif>
			<cfcatch type="Any">
				<cflog type="Error" text="[POC_MdmPool] Payload import error: #CFCATCH.message# (#CFCATCH.detail#)">
				<cfmail from="monitoring@saaswedo.com" to="cedric.rapiera@saaswedo.com"
				priority="highest" type="html" subject="[POC MDM] Erreur">
					<cfoutput>
						<b>Exception :</b><br>#CFCATCH.message# (#CFCATCH.detail#)<hr>
					</cfoutput>getMdmEndpointURL
					<cfif isDefined("jsonPayload")>
						<cfoutput>
							<hr><b>JSON :</b><br>#jsonPayload#<hr>
						</cfoutput>
					</cfif>
					<cfif isDefined("qImportMdmPool")>
						<cfdump var="#qImportMdmPool#" label="#procedureName#"><hr>
					</cfif>
					<cfdump var="#CFCATCH.tagContext#" label="Tag context">
				</cfmail>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- TODO: A refactoriser avec les autres provider pour retourner la liste des devices par page de 50 --->
	<cffunction access="private" name="getActiveDevices" returntype="Array" hint="Cette implémentation ne ramène que les devices enrollés">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM">
		<cfset var devices=[]>
		<cfset var infos=ARGUMENTS.mdmInfos>
		<cfset var rootURL=getMdmEndpointURL(infos)>
		<!--- Récupération de la liste des devices dans le MDM (Librairie Remoting : REST) --->
		<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
		<cfset var service={
			endpoint=rootURL & "/api/v1/mdm/devices/search",username=infos.USERNAME,password=infos.PASSWORD
		}>
		<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
		<!--- MAJ du endpoint: Recherche de devices (Par défaut dans Airwatch: 500 devices max par page) --->
		<cfset var pageNum=0>
		<cfset var devicesPerPage=500>
		<cfset var devicesTotal=0>
		<!--- Nombre de device de la liste qui est retournée par cette fonction --->
		<cfset var deviceProcessedCount=0>
		<!--- TRUE s'il y a plus d'une page de devices (500 max par page) --->
		<cfset var hasModeDevices=TRUE>
		<cfloop condition="hasModeDevices EQ TRUE">
			<cfset var params=[
				{type="header",name="accept",value="application/json"},
				{type="header",name="User-Agent",value="Fiddler"},
				{type="header",name="aw-tenant-code",value=infos.TOKEN},
				{type="url",name="page",value=pageNum},
				{type="url",name="pagesize",value=devicesPerPage}
			]>
			<!--- {TYPE="url",NAME="user",VALUE="monitoring"} --->
			<cfset var result=remoting.invoke(service,"",params)>
			<!--- Erreur durant le reporting --->
			<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
				<cfset var exception=structKeyExists(result,remoting.RESULT()) ? result[remoting.RESULT()]:{}>
				<cfset var errorMsg=structKeyExists(exception,"message") ? exception.message:"Error occured during remoting (HTTP)">
				<cfset var errorDetail=structKeyExists(exception,"detail") ? exception.detail:"Remoting status is #result[remoting.STATUS()]#">
				<cfset hasModeDevices=FALSE>
				<cfthrow type="Custom" message="#errorMsg#" detail="#errorDetail#">
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset var httpCode=remoting.getResponseHttpCode(result)>
				<cfset httpResponse=result[remoting.RESULT()]>
				<!--- Code HTTP d'erreur --->
				<cfif httpCode NEQ 200>
					<cfset hasModeDevices=FALSE>
					<cfthrow type="Custom" message="HTTP response code: #httpCode#">
				<cfelse>
					<!--- Désérialisation de la réponse HTTP (JSON) --->
					<cfif structKeyExists(httpResponse,"FileContent")>
						<cfset var responseContent=httpResponse.FileContent>
						<!--- Type du contenu pour une réponse sans erreur (HTTP 200) --->
						<cfif isInstanceOf(result.result.fileContent,"java.io.ByteArrayOutputStream")>
							<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
							<cfset var jsonResponse=dataUtils.byteArrayToString(responseContent)>
							<!--- Désérialisation JSON --->
							<cfif isJSON(jsonResponse)>
								<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
								<cfset var deviceList=jsonResponse.devices>
								<cfset var currentPageSize=arrayLen(deviceList)>
								<cfset devicesTotal=VAL(jsonResponse.total)>
								<cfif isArray(deviceList)>
									<!--- Ajout des devices dans le résultat retourné --->
									<cfloop index="i" from="1" to="#currentPageSize#">
										<!--- On ne prend que les devices enrollés --->
										<cfset var device=deviceList[i]>
										<cfif structKeyExists(device,"EnrollmentStatus") AND (compareNoCase(device.EnrollmentStatus,"Enrolled") EQ 0)>
											<cfset arrayAppend(devices,device)>
										</cfif>
										<cfset deviceProcessedCount=deviceProcessedCount + 1>
									</cfloop>
									<!--- Condition d'arret: Indique s'il reste encore des pages de devices non traitées --->
									<cfset hasModeDevices=(devicesTotal GT deviceProcessedCount)>
									<cfif hasModeDevices EQ TRUE>
										<!--- Incrémentation du numéro de page --->
										<cfset pageNum=pageNum + 1>
									</cfif>
								<cfelseif structKeyExists(jsonResponse.devices.device,"uuid")>
									<cfset hasModeDevices=FALSE>
									<cfthrow type="Custom" message="Invalid devices list type from HTTP reponse: Should be an Array">
								</cfif>
							<cfelse>
								<cfset hasModeDevices=FALSE>
								<cfthrow type="Custom" message="Invalid HTTP reponse format: Should be java.io.ByteArrayOutputStream">
							</cfif>
						<cfelse>
							<cfset hasModeDevices=FALSE>
							<cfthrow type="Custom" message="Invalid HTTP reponse type: Should be JSON">
						</cfif>
					</cfif>
				</cfif>
			<cfelse>
				<cfset hasModeDevices=FALSE>
				<cfthrow type="Custom" message="Unknwon remoting status value: #result[remoting.STATUS()]#">
			</cfif>
		</cfloop>
		<cfreturn devices>
	</cffunction>
	
	<cffunction access="private" name="getMdmEndpointURL" returntype="String" hint="Retourne l'URL d'accès à l'API du serveur MDM">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM">
		<cfset var infos=ARGUMENTS.mdmInfos>
		<cfreturn (infos.USE_SSL ? "https":"http") & "://" & infos.MDM>
	</cffunction>
</cfcomponent>