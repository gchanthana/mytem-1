<cfcomponent author="Cedric" hint="POC d'activation/désactivation du roaming (MobileIron)">
	<cffunction access="public" name="updateRoaming" returntype="void" hint="Nécessite d'identifier le device">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfargument name="uuid" type="String" required="true" hint="UUID du device qui porte la ligne">
		<cfargument name="osFamilly" type="String" required="true" hint="OS du device (iOS,ANDROID)">
		<cfargument name="enableRoaming" type="Boolean" required="true" hint="TRUE pour activer le roaming et FALSE sinon">
		<cfthrow type="Custom" errorCode="ERROR" message="NOT IMPLEMENTED">
		
		<cfset var success=FALSE>
		<cfset var deviceUUID=ARGUMENTS.uuid>
		<cfset var mdmInfos=getMdmServerInfos(ARGUMENTS.idGroupe)>
		<cfif structKeyExists(mdmInfos,"MDM_TYPE")>
			<cfset var mdm=(mdmInfos.USE_SSL ? "https":"http") & "://" & mdmInfos.MDM>
			<cfset var updateRoamingOp=mdm>
			<cfset var enableValue=ARGUMENTS.enableRoaming ? "true":"false">
			<!--- Activation/Désactivation pour iOS --->
			<cfif findNoCase("iOS",ARGUMENTS.osFamilly) GT 0>
				<cfset updateRoamingOp=mdm & "/api/v1/dm/devices/enableroaming/" & deviceUUID>
				<cfhttp method="put" url="#updateRoamingOp#" username="#mdmInfos.USERNAME#" password="#mdmInfos.PASSWORD#">
					<cfhttpparam type="header" name="accept" value="application/json">
					<cfhttpparam type="url" name="voice" value="#enableValue#">
					<cfhttpparam type="url" name="data" value="#enableValue#">
				</cfhttp>
				<cfif structKeyExists(CFHTTP,"Responseheader") AND structKeyExists(CFHTTP.Responseheader,"Status_Code")>
					<cfif structKeyExists(CFHTTP,"fileContent")>
						<cfset var stringResponseContent=getStringContent(CFHTTP.fileContent)>
						<cfif isJSON(stringResponseContent)>
							<cfset var jsonResponse=deserializeJSON(stringResponseContent)>
						</cfif>
						<cfif VAL(CFHTTP.ResponseHeader.Status_Code) EQ 200>
							<cfset success=TRUE>
						<cfelse>
							<cfthrow type="Custom" errorCode="ERROR" message="Error : #stringResponseContent#">
						</cfif>
					</cfif>
				</cfif>
			<!--- Activation/Désactivation pour ANDROID --->
			<cfelseif findNoCase("ANDROID",ARGUMENTS.osFamilly) GT 0>
				<cfset var removeLabelStatus=FALSE>
				<cfset var applyLabelStatus=FALSE>
				<cfset var labelToApply="Roaming enabled">
				<cfset var labelToRemove="Roaming disabled">
				<cfif NOT enableValue>
					<cfset labelToApply="Roaming disabled">
					<cfset labelToRemove="Roaming enabled">
				</cfif>
				<cfset var removeLabelOp=mdm & "/api/v1/dm/labels/" & labelToRemove & "/" & deviceUUID>
				<!--- Désaffectation du label : Activation ou Désactivation --->
				<cfhttp method="put" url="#removeLabelOp#" username="#mdmInfos.USERNAME#" password="#mdmInfos.PASSWORD#">
					<cfhttpparam type="header" name="accept" value="application/json">
					<cfhttpparam type="url" name="action" value="remove">
				</cfhttp>
				<cfif structKeyExists(CFHTTP,"Responseheader") AND structKeyExists(CFHTTP.Responseheader,"Status_Code")>
					<cfif structKeyExists(CFHTTP,"fileContent")>
						<cfset var stringResponseContent=getStringContent(CFHTTP.fileContent)>
						<cfif isJSON(stringResponseContent)>
							<cfset var jsonResponse=deserializeJSON(stringResponseContent)>
						</cfif>
						<cfif VAL(CFHTTP.ResponseHeader.Status_Code) EQ 200>
							<cfset removeLabelStatus=TRUE>
						<cfelse>
							<cfthrow type="Custom" errorCode="ERROR" message="Error : #stringResponseContent#">
						</cfif>
					</cfif>
				</cfif>
				<!--- Affectation du label : Activation ou Désactivation --->
				<cfset var applyLabelOp=mdm & "/api/v1/dm/labels/" & labelToApply & "/" & deviceUUID>
				<cfhttp method="put" url="#applyLabelOp#" username="#mdmInfos.USERNAME#" password="#mdmInfos.PASSWORD#">
					<cfhttpparam type="header" name="accept" value="application/json">
					<cfhttpparam type="url" name="action" value="apply">
				</cfhttp>
				<cfif structKeyExists(CFHTTP,"Responseheader") AND structKeyExists(CFHTTP.Responseheader,"Status_Code")>
					<cfif structKeyExists(CFHTTP,"fileContent")>
						<cfset var stringResponseContent=getStringContent(CFHTTP.fileContent)>
						<cfif isJSON(stringResponseContent)>
							<cfset var jsonResponse=deserializeJSON(stringResponseContent)>
						</cfif>
						<cfif VAL(CFHTTP.ResponseHeader.Status_Code) EQ 200>
							<cfset applyLabelStatus=TRUE>
						<cfelse>
							<cfthrow type="Custom" errorCode="ERROR" message="Error : #stringResponseContent#">
						</cfif>
					</cfif>
				</cfif>
				<cfset success=removeLabelStatus AND applyLabelStatus>
			<cfelse>
				<cfthrow type="Custom" message="Invalid OS #ARGUMENTS.osFamilly#">
			</cfif>
		</cfif>
		<cfif NOT success>
			<cfthrow type="Custom" errorCode="ERROR" message="Error performing update roaming">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getMdmServerInfos" returntype="Struct" hint="Retourne les infos du serveur MDM de la racine">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfset var idRacine=ARGUMENTS.idGroupe>
		<cfset var dataSource="ROCOFFRE">
		<!--- Récupération des infos MDM de la racine --->
		<cfset var procedureName="PKG_MDM.get_server_racine_v2">
		<cfstoredproc datasource="#datasource#" procedure="#procedureName#">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="procedure_status">
			<cfprocresult name="qMdmServerInfos">
		</cfstoredproc>
		<cfif (qMdmServerInfos.recordcount GT 0) AND (procedure_status GT 0)>
			<!--- Code d'erreur --->
			<cfset var code="MDM.ERROR">
			<!--- Structure des infos MDM : La valeur ramenée de la propriété MDM doit etre de la forme "PROVIDER://URI" --->
			<cfset var mdmServerInfos={
				MDM_TYPE=qMdmServerInfos["MDM_TYPE"][1],MDM=qMdmServerInfos["MDM"][1],USE_SSL=yesNoFormat(VAL(qMdmServerInfos["USE_SSL"][1])),
				USERNAME=qMdmServerInfos["USERNAME"][1],PASSWORD=qMdmServerInfos["PASSWORD"][1],USERID=VAL(qMdmServerInfos["USERID"][1]),
				ENROLL_USERNAME=qMdmServerInfos["ENROLL_USERNAME"][1],ENROLL_PASSWORD=qMdmServerInfos["ENROLL_PASSWORD"][1],
				IS_DEFAULT=yesNoFormat(VAL(qMdmServerInfos["IS_DEFAULT"][1])),ADMIN_EMAIL=qMdmServerInfos["ADMIN_EMAIL"][1],
				TEST_MSG=qMdmServerInfos["TEST_MSG"][1],IS_TEST_OK=yesNoFormat(VAL(qMdmServerInfos["IS_TEST_OK"][1]))
			}>
			<!--- Vérification de la valeur de la propriété MDM --->
			<cfset var mdmValue=mdmServerInfos.MDM>
			<cfif (TRIM(mdmValue) EQ "") OR (TRIM(mdmValue) NEQ mdmValue)>
				<cfset var extendedInfos=isDefined("qMdmServerInfos") ? serializeJSON(qMdmServerInfos):"">
				<cfthrow type="Custom" errorcode="#code#" message="#procedureName# returns invalid MDM '#mdmValue#' for idGroupe #idRacine#">
			<cfelse>
				<!--- Propriétés MDM et MDM_TYPE --->
				<cfset mdmServerInfos.MDM_TYPE=extractMdmProvider(mdmValue)>
				<cfset mdmServerInfos.MDM=extractMdm(mdmValue)>
				<!--- Traitement et conversion des valeurs (Le type Booléen de Java est utilisé pour compatibilité avec ActionScript) --->
				<cfset mdmServerInfos.IS_DEFAULT=createObject("java","java.lang.Boolean").init(mdmServerInfos.IS_DEFAULT ? "true":"false")>
				<cfset mdmServerInfos.USE_SSL=createObject("java","java.lang.Boolean").init(mdmServerInfos.USE_SSL ? "true":"false")>
				<cfset mdmServerInfos.IS_TEST_OK=createObject("java","java.lang.Boolean").init(mdmServerInfos.IS_TEST_OK ? "true":"false")>
				<cfreturn mdmServerInfos>
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="#code#" message="#procedureName# returns status #procedure_status# for idGroupe #idRacine#">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="extractMdmProvider" returntype="String" hint="Retourne la valeur du Provider MDM correspondant">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=extractMdmTokens(ARGUMENTS.mdm)>
		<cfset var mdmProvider=mdmTokens[1]>
		<cfif TRIM(mdmProvider) NEQ mdmProvider>
			<cfset var code="MDM.ERROR">
			<cfset var errorMsg="'#mdmProvider#' extracted from MDM value '#ARGUMENTS.mdm#'">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid Provider #errorMsg#. Cannot contain extra spaces">
		</cfif>
		<cfreturn mdmProvider>
	</cffunction>
	
	<cffunction access="private" name="extractMdm" returntype="String" hint="Retourne l'URI d'accès au MDM sans le protocole (ou le Scheme)">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=extractMdmTokens(ARGUMENTS.mdm)>
		<cfset var mdmValue=mdmTokens[2]>
		<cfif TRIM(mdmValue) NEQ mdmValue>
			<cfset var code="MDM.ERROR">
			<cfset var errorMsg="'#mdmValue#' extracted from MDM value '#ARGUMENTS.mdm#'">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid URI #errorMsg#. Cannot contain extra spaces">
		</cfif>
		<cfreturn mdmValue>
	</cffunction>
	
	<cffunction access="private" name="extractMdmTokens" returntype="Array" hint="Retourne les éléments du découpage d'une la valeur MDM d'origine">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=listToArray(ARGUMENTS.mdm,"://",FALSE,TRUE)>
		<cfif arrayLen(mdmTokens) EQ 2>
			<cfreturn mdmTokens>
		<cfelse>
			<cfset var code="MDM.ERROR">
			<cfthrow type="Custom" errorCode="#code#" message="Cannot extract Provider and URI tokens from invalid MDM value '#ARGUMENTS.mdm#'">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getStringContent" returntype="String" hint="Retourne la désérialisation String du contenu httpContent.
	Ecrit un log de warning si httpContent est de type complexe différent de java.io.ByteArrayOutputStream">
		<cfargument name="httpContent" type="Any" required="true" hint="Contenu de la réponse HTTP (CFHTTP.FileContent)">
		<cfset var content=ARGUMENTS.httpContent>
		<cfif isSimpleValue(content) AND isValid("String",content)>
			<cfreturn content>
		<cfelse>
			<!--- Conversion en String. Ne pas comparer avec getClass().getName() car héritage possible --->
			<cfif isInstanceOf(content,"java.io.ByteArrayOutputStream")>
				<cfreturn content.toString()>
			<cfelse>
				<cfset var code="MDM.ERROR">
				<cfthrow type="Custom" errorCode="#code#" message="Error converting HTTP response to String">
			</cfif>
		</cfif>
	</cffunction>
</cfcomponent>