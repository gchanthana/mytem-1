<cfinterface author="Cedric" hint="Interface des infos serveur MDM. Elles permettent l'accès aux infos du parc et à l'API du vendor et ">
	<cffunction name="init" returntype="fr.saaswedo.api.mdm.MdmServer" hint="Constructeur que doivent avoir toutes les implémentations">
		<cfargument name="idGroup" type="Numeric" required="true" hint="Identifiant du groupe pour identifier le Parc et les infos serveur MDM">
	</cffunction>
	
	<cffunction name="getIdGroup" returntype="Numeric" hint="Retourne l'identifiant du groupe">
	</cffunction>

	<cffunction name="getVendor" returntype="String" hint="Retourne un code identifiant le vendor.
	La liste des valeurs possible doit etre définie par les implémentations de cette interface">
	</cffunction>
	
	<cffunction name="getAdminEmail" returntype="String" hint="Retourne l'adresse mail d'administrateur qui sera notifié par mail en cas d'erreur.
	L'envoi du mail n'est pas garantie car elle dépend de l'implémentation e.g Stockage des logs (Voir la spécification/documentation de l'API)">
	</cffunction>

	<cffunction name="endpointURL" returntype="String" hint="Retourne l'URL complète de l'API du vendor construite à partir de endpoint() et useSSL()">
	</cffunction>

	<cffunction name="endpoint" returntype="String" hint="URI complète vers l'API du vendor (sans le protocole i.e sans http://).
	La valeur de cette URI dépend de l'implémentation et du vendor (Voir la documentation développeur de l'API)">
	</cffunction>

	<cffunction name="useSSL" returntype="Boolean" hint="Retourne TRUE si l'accès à l'API du vendor se fait en HTTPS">
	</cffunction>
	
	<cffunction name="getUsername" returntype="String" hint="Retourne le login utilisateur pour l'accès à l'API du vendor">
	</cffunction>
	
	<cffunction name="getPassword" returntype="String" hint="Retourne le password utilisateur pour l'accès à l'API du vendor">
	</cffunction>
	
	<cffunction name="getEnrollmentUsername" returntype="String" hint="Retourne le login utilisateur pour l'enrollment">
	</cffunction>
	
	<cffunction name="getEnrollmentPassword" returntype="String" hint="Retourne le password utilisateur pour l'enrollment">
	</cffunction>
	
	<cffunction name="getClientPort" returntype="Numeric" hint="Retourne le port client ou 0 s'il n'est pas défini pour le vendor">
	</cffunction>
	
	<cffunction name="updateServerInfos" returntype="void" hint="Modifie et met à jour les infos serveurs MDM du groupe fourni à l'instanciation">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM (Voir la documentation développeur de l'API)">
	</cffunction>
	
	<cffunction name="resetServerInfos" returntype="void" hint="Réinitialise et met à jour les infos serveurs MDM du groupe fourni à l'instanciation">
	</cffunction>
	
	<cffunction name="getDeviceId" returntype="Any" output="false" hint="Retourne les infos d'identification d'un device les plus récentes dans
	le parc local ou une structure vide si le device n'est pas trouvé. La valeur retournée est indéfinie en cas d'erreur (Le log est stocké puis loggé)">
		<cfargument name="serial" type="String" required="true" hint="Numéro de série du device">
		<cfargument name="imei" type="String" required="false" default="" hint="IMEI du device">
	</cffunction>
</cfinterface>