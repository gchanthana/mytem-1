<cfinterface author="Cedric" displayname="fr.saaswedo.api.mdm.core.services.IMdmServerInfo" hint="Interface qui décrit les infos d'un serveur MDM">
	<cffunction name="getProvider" returntype="String" hint="Retourne une valeur identifiant le provider MDM de ce serveur">
	</cffunction>
	
	<cffunction name="getEndpoint" returntype="String" hint="Raccourci qui retourne l'URL de l'endpoint de base d'accès au serveur">
	</cffunction>
	
	<cffunction name="getMdm" returntype="String" hint="Retourne l'adresse ou l'IP du serveur MDM (La valeur peut dépendre de l'implémentation)">
	</cffunction>
	
	<cffunction name="useSSL" returntype="Boolean" hint="Retourne TRUE si le protocole utilisé est HTTPS et FALSE sinon">
	</cffunction>
	
	<cffunction name="getUsername" returntype="String" hint="Retourne le login utilisateur MDM utilisé pour les accès aux WebServices'">
	</cffunction>
	
	<cffunction name="getPassword" returntype="String" hint="Retourne le mot de passe utilisateur MDM utilisé pour les accès aux WebServices'">
	</cffunction>
	
	<cffunction name="getEnrollUsername" returntype="String" hint="Retourne le login utilisateur MDM utilisé pour les Enrollments">
	</cffunction>
	
	<cffunction name="getEnrollPassword" returntype="String" hint="Retourne le mot de passe utilisateur MDM utilisé pour les Enrollments">
	</cffunction>
	
	<cffunction name="getAdminEmail" returntype="String" hint="Retourne l'email de l'administrateur MDM">
	</cffunction>
	
	<!--- =========== Liste des statuts de devices =========== --->
	
	<cffunction name="MANAGED" returntype="String" hint="Valeur correspondant au statut enrollé">
	</cffunction>

	<cffunction name="PENDING" returntype="String" hint="Valeur correspondant au statut Enrollment en cours. Cette fonction n'est pas forcément
	utilisée par les implémentations. Elle permet d'éviter d'envoyer plusieurs fois la demande d'enrollment pour un device déjà en cours d'enrollment">
	</cffunction>
	
	<cffunction name="UNMANAGED" returntype="String" hint="Valeur correspondant au statut non enrollé">
	</cffunction>
	
	<!--- =========== Liste des systèmes d'exploitation =========== --->
	
	<cffunction name="ANDROID" returntype="String" hint="Valeur correspondant à l'OS Android">
	</cffunction>
	
	<cffunction name="IOS" returntype="String" hint="Valeur correspondant à l'OS iOS">
	</cffunction>
	
	<cffunction name="UNKNOWN_OS" returntype="String" hint="Valeur correspondant à un OS non reconnu">
	</cffunction>
</cfinterface>