<cfinterface author="Cedric" displayname="fr.saaswedo.api.mdm.core.services.IDeviceManagement"
hint="Interface décrivant les fonctions de gestion des devices">
	<cffunction name="lock" returntype="Boolean" hint="Verrouille un device et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="newPinCode" type="String" required="false" default="0000"
		hint="Nouveau PIN de vérrouillage de remplacement">
	</cffunction>
	
	<cffunction name="unlock" returntype="Boolean" hint="Déverrouille un device et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
	</cffunction>
	
	<cffunction name="corporateWipe" returntype="Boolean" hint="Supprime les données MDM d'un device et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
	</cffunction>

	<cffunction name="wipe" returntype="Boolean" hint="Réinitialise un device et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="erasedMemoryCard" type="Boolean" required="false" default="false" hint="TRUE pour supprimer le contenu de la carte mémoire">
	</cffunction>
	
	<cffunction name="locate" returntype="Boolean" hint="Déclenche une tentative de localisation d'un device et retourne TRUE en cas de succès.
	Quand le résultat de la localisation est déterminé alors il doit etre renseigné dans les propriétés correspondantes du device">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
	</cffunction>
	
	<cffunction name="revoke" returntype="Boolean" hint="Désactive l'enrollment d'un device et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
	</cffunction>
	
	<cffunction name="register" returntype="Boolean" hint="Autorise l'enrollment d'un device et retourne TRUE en cas de succès et FALSE sinon.
	<br>Cette fonction est destinée à etre appelée avant la procédure d'enrollment. Son implémentation dépend du provider MDM utilisé">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
	</cffunction>
	
	<cffunction name="add" returntype="String" hint="Ajoute un Device dans le MDM.
	La valeur retournée dépend du provider MDM mais elle doit etre une chaine vide en cas d'erreur">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="osFamily" type="String" required="true" hint="OS du device">
	</cffunction>
	
	<cffunction name="remove" returntype="void" hint="Supprime un device du MDM">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
	</cffunction>
	
	<cffunction name="putDeviceProperties" returntype="void" hint="Ajoute les propriétés properties à un device<br>
	Chaque propriété est une structure avec les clés : NAME (Nom),VALUE (Valeur)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="properties" type="Array" required="true" hint="Propriétés à ajouter. Chaque propriété est une structure avec NAME,VALUE">
	</cffunction>
</cfinterface>