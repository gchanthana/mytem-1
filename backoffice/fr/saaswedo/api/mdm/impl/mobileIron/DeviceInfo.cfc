<!--- Liste des clés de la forme "Service" ajoutées par registerServices() :
softwareService : Structure pour le service d'accès à l'inventaire logiciel des devices --->
<cfcomponent author="Cedric" displayName="fr.saaswedo.api.mdm.impl.mobileIron.DeviceInfo" extends="fr.saaswedo.api.mdm.impl.mobileIron.MdmService"
implements="fr.saaswedo.api.mdm.core.services.IDeviceInfo" hint="Implémentation MobileIron.
<br>Une instance de ce composant ne référence que les services d'un seul serveur MDM et utilise les mêmes crédentiels pour y accéder">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.mobileIron.DeviceInfo">
		<cfreturn "fr.saaswedo.api.mdm.impl.mobileIron.DeviceInfo">
	</cffunction>

	<cffunction access="public" name="getAllDevices" returntype="Array"
	hint="Ramène la liste des devices présents sur le MDM. Certains devices ne sont pas forcément présents dans le parc MDM local">
		<cfset var deviceList=[]>
		<cftry>
			<cfset var httpResponse={}>
			<cfset var jsonDeviceList=[]>
			<!--- Liste des statuts valides de devices --->
			<cfset var statusList=validStatus()>
			<!--- Récupération de la liste des devices dans le MDM (Librairie Remoting : REST) --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
			<cfset var service=getDeviceService()>
			<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
			<cfloop index="i" from="1" to="#arrayLen(statusList)#">
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{TYPE="header",NAME="status",VALUE=statusList[i]}
				]>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()]})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif httpCode NEQ 200>
						<cfset storeError({UUID=createUUID(),SERVER_RESPONSE=httpResponse})>
					<cfelse>
						<!--- Désérialisation de la réponse HTTP --->
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset var responseContent=httpResponse.FileContent>
							<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
							<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
							<!--- Device enrollé sur le MDM --->
							<cfif structKeyExists(jsonResponse,"devices") AND structKeyExists(jsonResponse.devices,"device")>
								<cfif isArray(jsonResponse.devices.device)>
									<cfset jsonDeviceList=jsonResponse.devices.device>
								<!--- A confirmer: Il est possible que l'API MDM ne retourne pas un tableau quand il n'y a qu'un seul device --->
								<cfelseif structKeyExists(jsonResponse.devices.device,"uuid")>
									<cfset jsonDeviceList=[jsonResponse.devices.device]>
								</cfif>
								<!--- Ajout des devices dans le résultat retourné --->
								<cfset var deviceCount=arrayLen(jsonDeviceList)>
								<cfloop index="j" from="1" to="#deviceCount#">
									<cfset var jsonDevice=jsonDeviceList[j]>
									<cfset var device=extractDevice(jsonDevice)>
									<!--- Seuls les devices valides sont retournés dans le résultat --->
									<cfif (TRIM(device.UUID) NEQ "") AND ((TRIM(device.SERIAL) NEQ "") OR (TRIM(device.IMEI) NEQ ""))>
										<cfset arrayAppend(deviceList,{SerialNumber=device.SERIAL,IMEI=device.IMEI,UUID=device.UUID})>
									</cfif>
								</cfloop>
							</cfif>
						</cfif>
					</cfif>
				</cfif>
			</cfloop>
			<cfreturn deviceList>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn deviceList>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="deviceExists" returntype="Boolean" hint="Retourne TRUE le device existe et FALSE sinon">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cftry>
			<!--- C'est cette fonction qui est utilisée car elle ne lève pas d'exception si le device n'existe pas dans le MDM --->
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfreturn NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei})>
				<!--- Erreur non critique --->
				<cfreturn FALSE>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="getManagedStatus" returntype="String" hint="Retourne un des status : MANAGED(),UNMANAGED()">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cftry>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var device=deviceFromCache[xmlDeviceKey()]>
				<cfreturn getStatusValue(device.status)>
			<cfelse>
				<cfreturn UNMANAGED()>
			</cfif>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei})>
				<!--- [feature/MYT-843] Erreur non critique + BUG de l'implémentation MobileIron corrigé ici --->
				<cfreturn UNMANAGED()>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getLastUser" returntype="String" hint="Retourne le login d'enrollment ou une chaine vide s'il n'est pas enrollé">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var lastUser="">
		<cftry>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var device=deviceFromCache[xmlDeviceKey()]>
				<cfset lastUser=device.principal>
			</cfif>
			<cfreturn lastUser>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei})>
				<!--- Erreur non critique --->
				<cfreturn lastUser>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getDeviceInfo" returntype="Struct"
	hint="La structure retournée contient les clés supplémentaires suivantes :<br>- GENERAL (Struct): UUID (UUID MobileIron)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var mdmDevice={}>
		<cftry>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var device=deviceFromCache[xmlDeviceKey()]>
				<!--- Utilisé pour parser les dates au format ISO8601 provenant de MobileIron --->
				<cfset var javaDateParser=createObject("java","javax.xml.bind.DatatypeConverter")>
				<!--- Extraction des propriétés : GENERAL, PROPERTIES --->
				<cfset mdmDevice=extractMdmDevice(device)>
				<!--- Autres propriété de la clé GENERAL : firstConnectionDate,lastAuthDate --->
				<cfset structAppend(mdmDevice.GENERAL,{firstConnectionDate=0,lastAuthDate=0},TRUE)>
				<cfif structKeyExists(device,"registeredOn")>
					<cfset mdmDevice.GENERAL.firstConnectionDate=javaDateParser.parseDateTime(device.registeredOn).getTime()>
				</cfif>
				<cfif structKeyExists(device,"lastConnectedAt")>
					<cfset mdmDevice.GENERAL.lastAuthDate=javaDateParser.parseDateTime(device.lastConnectedAt).getTime()>
				</cfif>
				<!--- Inventaire logiciel: L'UUID est récupéré depuis le cache des devices --->
				<cfset mdmDevice.SOFTWARE=getDeviceSoftwares(device.uuid)>
			</cfif>
			<cfreturn mdmDevice>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei})>
				<!--- Erreur non critique --->
				<cfreturn mdmDevice>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="getDeviceSoftwares" returntype="Array"
	hint="Retourne l'inventaire logiciel du device. Chaque logiciel est une structure contenant :<br>
	NAME,AUTHOR,VERSION,SIZE (0),InstallTimeStamp (0)">
		<cfargument name="uuid" type="String" required="true" hint="UUID MobileIron du device">
		<cfset var softwareList=[]>
		<cfset var deviceUUID=ARGUMENTS.uuid>
		<cftry>
			<cfset var httpResponse={}>
			<!--- Récupération du device dans le MDM (Librairie Remoting : REST) --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
			<cfset var service=getSoftwareService()>
			<cfset var params=[{type="header",name="accept",value="application/json"}]>
			<!--- Les applications d'un device sont un sous-service du service Software --->
			<cfset service.endpoint=service.endpoint & "/devices/" & deviceUUID>
			<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
			<cfset var result=remoting.invoke(service,"",params)>
			<!--- Erreur durant le reporting --->
			<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
				<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()]})>
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset var httpCode=remoting.getResponseHttpCode(result)>
				<cfset httpResponse=result[remoting.RESULT()]>
				<!--- Réponse HTTP du remoting : Utilisé pour le log de warning plus bas --->
				<cfset device.RESULT=result[remoting.RESULT()]>
				<!--- Code HTTP d'erreur --->
				<cfif httpCode NEQ 200>
					<cfset storeError({UUID=createUUID(),SERVER_RESPONSE=httpResponse})>
				<cfelse>
					<!--- Désérialisation de la réponse HTTP --->
					<cfif structKeyExists(httpResponse,"FileContent")>
						<cfset var responseContent=httpResponse.FileContent>
						<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
						<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
						<!--- Extraction de l'inventaire logiciel --->
						<cfif structKeyExists(jsonResponse,"clientApps") AND
							structKeyExists(jsonResponse.clientApps,"clientApp") AND isArray(jsonResponse.clientApps.clientApp)>
							<cfset var softwares=jsonResponse.clientApps.clientApp>
							<cfset var softwareCount=arrayLen(softwares)>
							<cfloop index="i" from="1" to="#softwareCount#">
								<cfset var softNode=softwares[i]>
								<cfset arrayAppend(softwareList,{
									NAME=softNode.appName,AUTHOR=softNode.reportedAppName,VERSION=softNode.version,SIZE=0,InstallTimeStamp=0
								})>
							</cfloop>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<cfreturn softwareList>			
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn softwareList>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- =========== Fonctions de gestion des services =========== --->
	
	<cffunction access="private" name="getSoftwareService" returntype="Struct" hint="Retourne une copie des propriétés du Software qui seront
	fournies à la librairie de Remoting (REST). La structure retournée est une copie car son contenu peut etre modifié par les fonctions appelantes">
		<cftry>
			<cfset var mdmServer=getMdmServer()>
			<cfset var infos=mdmServer.getMdmServerInfo()>
			<cfset var service={
				endpoint=infos.getEndpoint() & "/api/v1/apps/inventory",username=infos.getUsername(),password=infos.getPassword()
			}>
			<cfreturn DUPLICATE(service)>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH})>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- =========== Fonctions d'extraction des infos des devices =========== --->
	
	<cffunction access="private" name="extractMdmDevice" returntype="Struct" hint="Retourne une structure contenant :
	<br>- GENERAL (Struct): OS,UUID<br>- PROPERTIES (Array): Chaque élément est une structure contenant les clés NAME,VALUE.
	Celle implémentation est une redéfinition de la fonction parent et est utilisée par la fonction getDeviceInfo() de ce composant">
		<cfargument name="jsonDevice" type="Struct" required="true" hint="Device de l'API MDM">
		<!--- TODO: Peut etre utiliser l'adresse MAC Wifi comme valeur du SERIAL et IMEI quand ces derniers ne sont pas renseignés --->
		<cfset var jsonStruct=ARGUMENTS.jsonDevice>
		<cfset var properties=[]>
		<!--- UUID et STATUS --->
		<cfset var deviceUUID=structKeyExists(jsonStruct,"uuid") ? jsonStruct.uuid:"">
		<cfset var deviceStatus=structKeyExists(jsonStruct,"status") ? jsonStruct.status:"">
		<cfset var mdmDevice={GENERAL={UUID=deviceUUID,Serial_Number="",IMEI="",OS="",STATUS=getStatusValue(deviceStatus)}}>
		<!--- Système d'exploitation du device --->
		<cfif structKeyExists(jsonStruct,"platform")>
			<cfset var mdmProperty=extractMdmProperty("platform",jsonStruct.platform)>
			<cfif NOT structIsEmpty(mdmProperty)>
				<cfset mdmDevice.GENERAL.OS=mdmProperty.VALUE>
				<cfset arrayAppend(properties,mdmProperty)>
			</cfif>
		<cfelse>
			<!--- TODO: Log de warning à stocker avec la librairie de stockage des logs --->
			<cfset var log=GLOBALS().CONST("MobileIron.LOG")>
			<cfset var deviceLog="SERIAL='#mdmDevice.GENERAL.Serial_Number#' and IMEI='#mdmDevice.GENERAL.IMEI#'">
			<cflog type="warning" text="[#log#] *** Missing property 'platform' in device #deviceLog# ***">
		</cfif>
		<!--- Détails du device --->
		<cfif structKeyExists(jsonStruct,"details") AND isArray(jsonStruct.details) AND (arrayLen(jsonStruct.details) EQ 1)>
			<cfset var details=[]>
			<cfif structKeyExists(jsonStruct["details"][1],"entry") AND isArray(jsonStruct["details"][1]["entry"])>
				<cfset details=jsonStruct["details"][1]["entry"]>
			</cfif>
			<cfset var detailsCount=arrayLen(details)>
			<!--- Parcours des propriétés du device --->
			<cfloop index="i" from="1" to="#detailsCount#">
				<cfset var property=details[i]>
				<cfset var key=property.key>
				<cfset var value=property.value>
				<!--- Extraction spécifique aux propriétés : Serial_Number et IMEI --->
				<cfif key EQ "SerialNumber">
					<cfset mdmDevice.GENERAL.Serial_Number=value>
					<cfset serialDefined=TRUE>
				<cfelseif (key EQ "IMEI") OR (key EQ "ImeiOrMeid")>
					<cfset mdmDevice.GENERAL.IMEI=value>
					<cfset imeiDefined=TRUE>
				<!--- Extraction spécifique aux autres propriétés --->
				<cfelse>
					<cfset var mdmProperty=extractMdmProperty(key,value)>
					<cfif NOT structIsEmpty(mdmProperty)>
						<!--- Ajout de la propriété dans le device retourné --->
						<cfset arrayAppend(properties,mdmProperty)>
						<!--- TODO: L'utilisation de l'adresse MAC Wifi est à confirmer car il y a un risque d'imcompatibilité avec le parc MDM local
						<cfif (NOT serialDefined) AND (NOT imeiDefined) AND ((key EQ "WiFiMAC") OR (key EQ "wifi_mac_addr"))>
							<cfset mdmDevice.GENERAL.Serial_Number=mdmProperty.value>
							<cfset mdmDevice.GENERAL.IMEI=mdmProperty.value>
						</cfif>
						--->
					</cfif>
				</cfif>
			</cfloop>
		</cfif>
		<!--- Ajout du provider MDM dans les propriétés du device --->
		<cfset arrayAppend(properties,{NAME="MDM_PROVIDER",VALUE=getMdmServer().getMdmServerInfo().getProvider()})>
		<cfset mdmDevice.PROPERTIES=properties>
		<cfreturn mdmDevice>
	</cffunction>
	
	<!---
	<cffunction access="private" name="getLastLocation" returntype="Struct" hint="Localise un device et retourne le résultat">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var device=ARGUMENTS.deviceStruct>
		<cfset var deviceUUID=device.MDM.UUID>
		<cfif deviceUUID NEQ "">
			<!--- API Remoting : MobileIron --->
			<cfset var restClient=getRemoteClient()>
			<!--- Récupération des infos de la dernière localisation sans faire de localisation --->
			<cfset var params=[
				{TYPE=restClient.SUFFIX_TYPE(),NAME="SUFFIX",VALUE="/locate/" & deviceUUID},
				{TYPE=restClient.FORMFIELD_TYPE(),NAME="locatenow",VALUE=FALSE},
				{TYPE=restClient.FORMFIELD_TYPE(),NAME="Reason",VALUE="LAST LOCATION"}
			]>
			<cfset var httpCall=restClient.getServiceCall(getDeviceService())>
			<cfset var xmlResult=restClient.invoke(httpCall,restClient.GET_METHOD(),params)>
			<!--- Le résultat retourné peut ne pas etre un objet XML en cas d'erreur (e.g HTTP 404) --->
			<cfset var valid=isXML(xmlResult)>
			<cfif valid>
				<cfset var xmlRoot=xmlResult.xmlRoot>
				<cfset var locationAvailable=structKeyExists(xmlRoot,"locations") AND
				structKeyExists(xmlRoot.locations,"location")>
				<!--- Infos de dernière localisation disponibles --->
				<cfif locationAvailable>
					<cfset var locationNode=xmlRoot.locations.location>
					<cfset var locationInfos=locationNode.xmlChildren>
					<cfset var infosCount=arrayLen(locationInfos)>
					<cfset var suffix="CELLULAR">
					<!--- Condition de validité des infos --->
					<cfset var lastLocation=locationNode.lookupResult.xmlText>
					<cfif lastLocation NEQ "LookupFailure">
						<cfif lastLocation EQ "GPS">
							<cfset suffix="GPS">
						</cfif>
						<cfloop index="i" from="1" to="#infosCount#">
							<cfset var infos=locationInfos[i]>
							<cfset var mdmProperty=extractMdmProperty(infos.xmlName & "_" & suffix,infos.xmlText)>
							<cfif NOT structIsEmpty(mdmProperty)>
								<cfset arrayAppend(device.PROPERTIES,mdmProperty)>
							</cfif>
						</cfloop>
					<cfelse>
						<cfset var log=GLOBALS().CONST("MobileIron.LOG")>
						<cflog type="warning" text="[#log#] Last Location #lastLocation# [UUID:#deviceUUID#]">
					</cfif>
				<cfelse>
					<cfset var log=GLOBALS().CONST("MobileIron.LOG")>
					<cflog type="warning" text="[#log#] Last Location UNAVAILABLE [UUID:#deviceUUID#]">
				</cfif>
			<cfelse>
				<cfset var code=GLOBALS().CONST("MobileIron.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="Invalid LAST LOCATION result [UUID:#deviceUUID#]">
			</cfif>
		</cfif>
	</cffunction>
	--->
</cfcomponent>