<!--- Structure des infos MDM :
MDM_TYPE="Provider MDM",MDM="DNS ou IP du serveur MDM",USE_SSL="TRUE pour utiliser le protocole HTTPS et FALSE sinon",
USERNAME="Login d'accès à l'API du serveur",PASSWORD="Mot de passe d'accès à l'API du serveur" --->
<cfcomponent author="Cedric" displayName="fr.saaswedo.api.mdm.impl.MdmServerInfo"
extends="fr.saaswedo.api.patterns.cfc.Component" implements="fr.saaswedo.api.mdm.core.services.IMdmServerInfo"
hint="Implémentation par défaut de l'API pour les infos d'un serveur MDM. Les fonctions qui non implémentées ne sont pas indispensables à l'API MDM">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.MdmServerInfo">
		<cfreturn "fr.saaswedo.api.mdm.impl.MdmServerInfo">
	</cffunction>
	
	<cffunction access="public" name="getProvider" returntype="String" hint="Retourne le provider MDM">
		<cfreturn getMdmInfos().MDM_TYPE>
	</cffunction>
	
	<cffunction access="public" name="getEndpoint" returntype="String" description="LOCK:ReadOnly" hint="URL construite avec getMdm() et useSSL()">
		<cfreturn (useSSL() ? "https":"http") & "://" & getMdm()>
	</cffunction>
	
	<cffunction access="public" name="getMdm" returntype="String" description="LOCK:ReadOnly">
		<cfreturn getMdmInfos().MDM>
	</cffunction>
	
	<cffunction access="public" name="useSSL" returntype="Boolean" description="LOCK:ReadOnly">
		<cfreturn getMdmInfos().USE_SSL>
	</cffunction>

	<cffunction access="public" name="getToken" returntype="String" description="LOCK:ReadOnly">
		<cfreturn getMdmInfos().TOKEN>
	</cffunction>
	
	<cffunction access="public" name="getUsername" returntype="String" description="LOCK:ReadOnly">
		<cfreturn getMdmInfos().USERNAME>
	</cffunction>
	
	<cffunction access="public" name="getPassword" returntype="String" description="LOCK:ReadOnly">
		<cfreturn getMdmInfos().PASSWORD>
	</cffunction>
	
	<cffunction access="public" name="getEnrollUsername" returntype="String" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.getEnrollUsername() is not implemented">
	</cffunction>
	
	<cffunction access="public" name="getEnrollPassword" returntype="String" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.getEnrollPassword() is not implemented">
	</cffunction>
	
	<cffunction access="public" name="getAdminEmail" returntype="String" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.getAdminEmail() is not implemented">
	</cffunction>

	<!--- =========== Liste des statuts de devices =========== --->

	<cffunction access="public" name="MANAGED" returntype="String"
	hint="Valeur correspondant au statut enrollé">
		<cfreturn "MANAGED">
	</cffunction>

	<cffunction access="public" name="PENDING" returntype="String" hint="Valeur correspondant au statut Enrollment en cours.
	Elle peut etre considérée comme UNMANAGED() et permet d'éviter d'enroller plusieurs fois un device en cours d'enrollment si elle est utilisée">
		<cfreturn "PENDING">
	</cffunction>
	
	<cffunction access="public" name="UNMANAGED" returntype="String" hint="Valeur correspondant au statut non enrollé">
		<cfreturn "UNMANAGED">
	</cffunction>
	
	<!--- =========== Liste des systèmes d'exploitation =========== --->
	
	<cffunction access="public" name="ANDROID" returntype="String" hint="Valeur correspondant à l'OS Android">
		<cfreturn "ANDROID">
	</cffunction>
	
	<cffunction access="public" name="IOS" returntype="String" hint="Valeur correspondant à l'OS iOS">
		<cfreturn "IOS">
	</cffunction>
	
	<cffunction access="public" name="UNKNOWN_OS" returntype="String" hint="Valeur correspondant à un OS non reconnu">
		<cfreturn "N.D">
	</cffunction>

	<cffunction access="public" name="init" returntype="fr.saaswedo.api.mdm.impl.MdmServerInfo" description="LOCK:Exclusive" hint="Constructeur">
		<cfargument name="provider" type="String" required="true" hint="Provider MDM">
		<cfargument name="mdm" type="String" required="true" hint="DNS ou adresse IP du serveur MDM incluant le context-path si c'est le cas">
		<cfargument name="useSSL" type="Boolean" required="true" hint="TRUE pour utiliser le protocole HTTPS et FALSE pour HTTP">
		<cfargument name="username" type="String" required="true" hint="Login d'accès aux services du serveur">
		<cfargument name="password" type="String" required="true" hint="Mot de passe d'accès aux services du serveur">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset SUPER.init()>
			<!--- Défini les infos du serveur MDM --->
			<cfset setMdmInfos(DUPLICATE(ARGUMENTS))>
			<cfreturn THIS>
		</cflock>
	</cffunction>
	
	<!--- =========== Infos du serveur MDM =========== --->
	
	<cffunction access="private" name="getMdmInfos" returntype="Struct" description="LOCK:ReadOnly" hint="Structure contenant les infos du serveur">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfif structKeyExists(VARIABLES,"MDM_INFOS")>
				<cfreturn VARIABLES.MDM_INFOS>
			<cfelse>
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.getMdmInfos() : MDM server infos are not defined">
			</cfif>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="setMdmInfos" returntype="void" description="LOCK:Exclusive" hint="Défini et remplace les infos du serveur">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos du serveur MDM">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset var infos=ARGUMENTS.mdmInfos>
			<cfset VARIABLES.MDM_INFOS={MDM_TYPE=infos.provider,MDM=infos.mdm,USE_SSL=infos.useSSL,USERNAME=infos.username,PASSWORD=infos.password}>
		</cflock>
	</cffunction>
	
	<!--- Stockage des logs --->
	<cffunction access="private" name="storeWarning" returntype="void"
	hint="Stocke le log d'erreur. La valeur de log.LOG_TYPE est remplacée par celle de logType">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<cfset var logInfos=ARGUMENTS.log>
		<cfset var logStorage=new fr.saaswedo.utils.log.MDM()>
		<cfset logInfos.LOG_TYPE=logStorage.WARNING_LOG()>
		<cfset storeLog(logStorage,logInfos)>
	</cffunction>
	
	<cffunction access="private" name="storeError" returntype="void"
	hint="Stocke le log d'erreur. La valeur de log.LOG_TYPE est remplacée par la constante ERROR_LOG() de la librairie de stockage des logs">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<cfset var logInfos=ARGUMENTS.log>
		<cfset var logStorage=new fr.saaswedo.utils.log.MDM()>
		<cfset logInfos.LOG_TYPE=logStorage.ERROR_LOG()>
		<cfset storeLog(logStorage,logInfos)>
	</cffunction>
	
	<cffunction access="private" name="storeLog" returntype="void" hint="Stocke le log avec la librairie de stockage de log de l'API MDM.
	Ajoute dans le log la clé IDGROUP associée à la valeur de l'identifiant du groupe dans la session utilisateur s'il est défini.
	Ajoute dans le log la clé IDMNT_ID associée à la valeur correspondante commune à toutes les fonctionnalités de l'API MDM">
		<cfargument name="logger" type="fr.saaswedo.utils.log.MDM" required="true" hint="Librairie de stockage du log">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<!--- Stockage et log de l'erreur: Cette implémentation remplace les valeurs de IMNT_CAT,SUBJECTID,BODY
		Toute exception durant le stockage du log est notifiée par mail par la librairie de stockage des logs
		--->
		<cfset var logStorage=ARGUMENTS.logger>
		<cfset var logInfos=ARGUMENTS.log>
		<cfset logInfos.IDMNT_ID=logStorage.DRT_002()>
		<cfset logInfos.BODY_FORMAT=logStorage.HTML_BODY()>
		<!--- TODO: Dans l'attente du refactoring de l'API MDM, la racine est fournie ici --->
		<cfif isDefined("SESSION") AND structKeyExists(SESSION,"PERIMETRE") AND structKeyExists(SESSION.PERIMETRE,"ID_GROUPE")>
			<cfset logInfos.IDGROUP=VAL(SESSION.PERIMETRE.ID_GROUPE)>
		<cfelse>
			<cfset logInfos.IDGROUP=-1>
		</cfif>
		<cfset logStorage.logIt(logInfos)>
	</cffunction>
</cfcomponent>