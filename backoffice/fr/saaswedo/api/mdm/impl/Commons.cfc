<cfcomponent author="Cedric" extends="fr.saaswedo.utils.data.DataUtils" hint="Fonctions et constantes communes à l'API MDM">
	<cffunction access="public" name="iOS" returntype="String" hint="Valeur correspondant à l'OS iOS">
		<cfreturn "iOS">
	</cffunction>
	
	<cffunction access="public" name="ANDROID" returntype="String" hint="Valeur correspondant à l'OS ANDROID">
		<cfreturn "ANDROID">
	</cffunction>
	
	<cffunction access="public" name="UNKNOWN" returntype="String" hint="Valeur correspondant à un OS ou un vendor inconnu">
		<cfreturn "UNKNOWN">
	</cffunction>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.mdm.impl.Commons" hint="Constructeur">
		<cfreturn THIS>
	</cffunction>
</cfcomponent>