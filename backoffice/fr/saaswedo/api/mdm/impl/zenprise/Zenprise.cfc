<cfcomponent author="Cedric" displayname="fr.saaswedo.api.mdm.impl.zenprise.Zenprise" extends="fr.saaswedo.api.mdm.impl.MdmServer"
hint="Implémentation d'un serveur Zenprise. L'implémentation IRemoteClient par défaut est fr.saaswedo.api.remoting.soap.WSClient
<br>L'implémentation IDeviceInfo est fr.saaswedo.api.mdm.impl.zenprise.DeviceInfo
<br>L'implémentation IDeviceManagement est fr.saaswedo.api.mdm.impl.zenprise.DeviceManagement">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.zenprise.Zenprise">
		<cfreturn "fr.saaswedo.api.mdm.impl.zenprise.Zenprise">
	</cffunction>
	
	<cffunction access="public" name="checkConnection" returntype="Struct" hint="Vérifie la connexion avec le serveur MDM et retourne une structure
	contenant :<br>- IS_TEST_OK: TRUE si la connexion en succès<br>- TEST_MSG: Message explicatif lié à la vérification de la connexion">
		<cfset var checkResult={IS_TEST_OK=createObject("java","java.lang.Boolean").init("false"),TEST_MSG="NONE"}>
		<cftry>
			<cfset var infos=getMdmServerInfo()>
			<!--- Librairie Remoting : SOAP --->
			<cfset var service={
				endpoint=infos.getEndpoint() & "/services/EveryWanDevice?WSDL",username=infos.getUsername(),password=infos.getPassword()
			}>
			<cfset var params=[{NAME="login",VALUE=service.username},{NAME="password",VALUE=service.password}]>
			<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
			<cfset var result=remoting.invoke(service,"authenticateUser",params)>
			<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
				<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: checkConnection()">
			<!--- Soit le résultat est défini soit il est indéfini : Dans tous les cas la connexion est en succès --->
			<cfelse>
				<cfset checkResult.IS_TEST_OK=createObject("java","java.lang.Boolean").init("true")>
			</cfif>
			<cfreturn checkResult>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfset checkResult.TEST_MSG=CFCATCH.message>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
						<cfif structKeyExists(result,remoting.RESULT()) AND structKeyExists(result[remoting.RESULT()],"MESSAGE")>
							<cfset checkResult.TEST_MSG=result[remoting.RESULT()]["MESSAGE"]>
						</cfif>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- L'erreur est inclue dans la valeur de retour --->
				<cfreturn checkResult>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="PROVIDER" returntype="String" hint="Retourne Zenprise">
		<cfreturn "Zenprise">
	</cffunction>

	<cffunction access="public" name="getEnrollmentURL" returntype="String" hint="Retourne le endpoint MDM si aucune URL ne correspond aux paramètres">
		<cfargument name="osFamily" type="String" required="true" hint="OS du device">
		<cfargument name="requireAgent" type="Boolean" required="false" default="true" hint="TRUE pour un enrollment qui requiert un agent MDM">
		<cfset var os=ARGUMENTS.osFamily>
		<cfset var urlSuffix="">
		<cfset var mdmInfos=getMdmServerInfo()>
		<cfif os EQ mdmInfos.ANDROID()>
			<cfset urlSuffix="/enroll">
		<cfelseif os EQ mdmInfos.IOS()>
			<cfif ARGUMENTS.requireAgent>
				<cfset urlSuffix="/enroll">
			<cfelse>
				<cfset urlSuffix="/ios/otae">
			</cfif>
		</cfif>
		<cfreturn mdmInfos.getEndpoint() & urlSuffix>
	</cffunction>

	<cffunction access="public" name="getEnrollmentServer" returntype="String" hint="Retourne le DNS ou l'IP du serveur d'enrollment">
		<cfset var mdmInfos=getMdmServerInfo()>
		<cfset var endpoint=mdmInfos.getEndpoint()>
		<cfset var mdmServerURL=createObject("java","java.net.URL").init(endpoint)>
		<cfreturn mdmServerURL.getHost()>
	</cffunction>

	<cffunction access="public" name="getEnrollmentServerPort" returntype="Numeric" hint="Retourne le numéro de port du serveur d'enrollment">
		<cfreturn 80>
	</cffunction>

	<!--- =========== Fonctions de gestion des services du serveur =========== --->
		
	<!--- La fonction init() qui appelle cette fonction avec un verrou CFLOCK exclusif --->
	<cffunction access="private" name="initMdmServices" returntype="void" hint="Instancie et initialise les implémentations des services">
		<!--- API d'accès aux infos des devices --->
		<cfset setIDeviceInfo(new fr.saaswedo.api.mdm.impl.zenprise.DeviceInfo(THIS))>
		<!--- API de gestion des devices --->
		<cfset setIDeviceManagement(new fr.saaswedo.api.mdm.impl.zenprise.DeviceManagement(THIS))>
	</cffunction>
</cfcomponent>