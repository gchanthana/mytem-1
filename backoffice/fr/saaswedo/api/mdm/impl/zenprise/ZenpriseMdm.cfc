<cfcomponent author="Cedric" displayname="fr.saaswedo.api.mdm.impl.zenprise.ZenpriseMdm"
extends="fr.saaswedo.api.mdm.impl.zenprise.Zenprise" implements="fr.saaswedo.api.mdm.core.services.IMDM" hint="Implémentation IMDM Zenprise">
	<!--- Pas d'interface pour ces méthodes pour le moment --->
	<cffunction access="public" name="checkServerConnection" returntype="Struct" hint="Effectue un test de connexion avec le serveur MDM<br/>
	Retourne une structure contenant les clés : IS_TEST_OK (TRUE si la connexion a été effectuée avec succès et FALSE sinon),
	TEST_MSG : Message associé au test de connexion">
		<cfargument name="mdm" type="String" required="true" hint="DNS ou adresse IP du serveur MDM">
		<cfargument name="useSSL" type="Boolean" required="true" hint="TRUE pour utiliser le protocole HTTPS et FALSE sinon">
		<cfargument name="wsUser" type="String" required="true" hint="Login de connexion au WebService MDM">
		<cfargument name="wsPassword" type="String" required="true" hint="Mot de passe de connexion au WebService MDM">
		<cfset var testService="EveryWanDevice">
		<cfset var wsdlBaseUrl=(ARGUMENTS["useSSL"] ? "https":"http") & "://" & ARGUMENTS["mdm"] & "/zdm/services/">
		<cftry>
			<cfset var soapClient=getSoapClient()>
			<cfset var wsdlURL=wsdlBaseUrl & testService & "?WSDL">
			<!--- Paramètres du test de connexion --->
			<cfset var wsParams={
				login={VALUE=ARGUMENTS["wsUser"]},password={VALUE=ARGUMENTS["wsPassword"]}
			}>
			<!--- Enregistrement de l'instance du WebService --->
			<cfset var serviceId=soapClient.register(wsdlURL,wsUser,wsPassword)>
			<cfset var testResult={
				IS_TEST_OK=createObject("java","java.lang.Boolean").init("true"),
				TEST_MSG="SUCCESS"
			}>
			<!--- Test de connexion --->
			<cfset var wsTestResult=soapClient.invoke(serviceId,"authenticateUser",wsParams,TRUE,soapClient.CONNECTION_TEST_ERROR())>
			<cflog type="information" text="[ZenpriseMdm] Test server connection with user #ARGUMENTS.wsUser# : #serializeJSON(wsTestResult)#">
			<cfreturn testResult>
			<!--- En cas d'exception le traitement est délégué à SoapClient si CFCATCH.errorCode est différent de API_REMOTING --->
			<cfcatch type="Any">
				<cfset var testMsg=CFCATCH.message & "[" & CFCATCH.detail & "]">
				<cfset testMsg=(LEN(testMsg) GT 4000 ? LEFT(testMsg,4000):testMsg)>
				<cfset testResult={
					IS_TEST_OK=createObject("java","java.lang.Boolean").init("false"),
					TEST_MSG=testMsg
				}>
				<!--- Délégation du traitement de l'exception : La clé WSDL fait partie des specification de SoapClient et IErrorHandler --->
				<cfif (CFCATCH.type NEQ "Custom") AND (TRIM(CFCATCH.errorCode) NEQ "API_REMOTING")>
					<cfset errorEventType=soapClient.BACKOFFICE_ERROR()>
					<cfset soapClient.handleSoapClientErrors(CFCATCH,"authenticateUser",wsParams,errorEventType,{WSDL=wsdlURL})>
				</cfif>
				<cfreturn testResult>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- fr.saaswedo.api.mdm.core.services.IMDM --->
	<cffunction access="public" name="getIDeviceManagement" returntype="fr.saaswedo.api.mdm.core.services.IDeviceManagement"
	hint="Retourne l'instance de l'implémentation IDeviceManagement stockée dans ce composant. L'instanciation n'est effectuée qu'au 1er appel à cette méthode">
		<cfif NOT structKeyExists(VARIABLES,getDeviceManagementKey())>
			<cflog type="error" text="[ZenpriseMdm] Instance property #getDeviceManagementKey()# is not defined">
			<cfthrow type="Custom" errorcode="API_MDM" message="L'instance IDeviceManagement n'est pas définie">
		<cfelse>
			<cfreturn VARIABLES[getDeviceManagementKey()]>
		</cfif>
	</cffunction>

	<cffunction access="public" name="getIDeviceInfo" returntype="fr.saaswedo.api.mdm.core.services.IDeviceInfo"
	hint="Retourne l'instance de l'implémentation IDeviceInfo stockée dans ce composant. L'instanciation n'est effectuée qu'au 1er appel à cette méthode">
		<cfif NOT structKeyExists(VARIABLES,getDeviceInfoKey())>
			<cflog type="error" text="[ZenpriseMdm] Instance property #getDeviceInfoKey()# is not defined">
			<cfthrow type="Custom" errorcode="API_MDM" message="L'instance IDeviceInfo n'est pas définie">
		<cfelse>
			<cfreturn VARIABLES[getDeviceInfoKey()]>
		</cfif>
	</cffunction>
	
	<!--- fr.saaswedo.api.mdm.impl.zenprise.Zenprise --->
	<cffunction access="public" name="setErrorHandler" returntype="void"
	hint="Appelle la méthode de la classe parent puis celle des implémentations : IDeviceManagement, IDeviceInfo">
		<cfargument name="errorHandler" type="fr.saaswedo.api.remoting.soap.IErrorHandler" hint="Implémentation de traitement des exceptions">
		<cfset SUPER.setErrorHandler(ARGUMENTS["errorHandler"])>
		<cfset getIDeviceManagement().setErrorHandler(ARGUMENTS["errorHandler"])><!--- Méthode héritée de Zenprise --->
		<cfset getIDeviceInfo().setErrorHandler(ARGUMENTS["errorHandler"])><!--- Méthode héritée de Zenprise --->
	</cffunction>
	
	<cffunction access="public" name="getInstance" returntype="fr.saaswedo.api.mdm.impl.zenprise.Zenprise"
	hint="Appelle la méthode du composant parent puis instancie au 1er appel de cette méthode les implémentations IDeviceManagement, IDeviceInfo<br>
	Les propriétés serveur MDM de ces implémentations sont mises à jour à chaque appel à cette méthode">
		<cfargument name="zdm" type="String" required="true" hint="Adresse du serveur Zenprise (DNS ou IP)">
		<cfargument name="username" type="String" required="false" default="" hint="Login d'accès au Web Service">
		<cfargument name="password" type="String" required="false" default="" hint="Mot de passe d'accès au Web Service">
		<cfargument name="useSSL" type="Boolean" required="false" default="TRUE" hint="Si la valeur est FALSE alors le protocole HTTP sera utilisé au lieu de HTTPS">
		<cfargument name="adminEmail" type="String" required="false" default="" hint="Email de l'administrateur du serveur Zenprise">
		<cfset SUPER.getInstance(ARGUMENTS["zdm"],ARGUMENTS["username"],ARGUMENTS["password"],ARGUMENTS["useSSL"],ARGUMENTS["adminEmail"])>
		<!--- Instanciation et mise à jour des propriétés serveur MDM de l'implémentation IDeviceManagement --->
		<cfif NOT structKeyExists(VARIABLES,getDeviceManagementKey())>
			<cfset var deviceManagement=createObject("component","fr.saaswedo.api.mdm.impl.zenprise.DeviceManagement")>
			<cfset VARIABLES[getDeviceManagementKey()]=deviceManagement.getInstance(
				SUPER.getMdm(),SUPER.getUsername(),SUPER.getPassword(),SUPER.useSSL(),SUPER.getAdminEmail()
			)>
		</cfif>
		<!--- Instanciation et mise à jour des propriétés serveur MDM de l'implémentation IDeviceManagement --->
		<cfif NOT structKeyExists(VARIABLES,getDeviceInfoKey())>
			<cfset var deviceInfos=createObject("component","fr.saaswedo.api.mdm.impl.zenprise.DeviceInfo")>
			<cfset VARIABLES[getDeviceInfoKey()]=deviceInfos.getInstance(
				SUPER.getMdm(),SUPER.getUsername(),SUPER.getPassword(),SUPER.useSSL(),SUPER.getAdminEmail()
			)>
		</cfif>
		<cflog type="information" text="[ZenpriseMdm] Server set to #SUPER.getMdm()# - useSSL set to #SUPER.useSSL()# - username set to #SUPER.getUsername()#">
		<cfreturn THIS>
	</cffunction>
	
	<!--- fr.saaswedo.api.mdm.impl.zenprise.DeviceManagement --->
	<cffunction access="private" name="getDeviceInfoKey" returntype="String"
	hint="Retourne le nom du champ utilisé dans ce composant pour désigner l'instance IDeviceInfo">
		<cfreturn "DEVICE_INFO">
	</cffunction>
	
	<cffunction access="private" name="getDeviceManagementKey" returntype="String"
	hint="Retourne le nom du champ utilisé dans ce composant pour désigner l'instance IDeviceManagement">
		<cfreturn "DEVICE_MANAGEMENT">
	</cffunction>
</cfcomponent>