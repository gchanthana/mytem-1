<cfcomponent author="Cedric" extends="fr.saaswedo.api.mdm.impl.airwatch.MdmService" implements="fr.saaswedo.api.mdm.core.services.IDeviceManagement">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.airwatch.DeviceManagement">
		<cfreturn "fr.saaswedo.api.mdm.impl.airwatch.DeviceManagement">
	</cffunction>
	
	<cffunction access="public" name="lock" returntype="Boolean" hint="Verrouille un device">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="newPinCode" type="String" required="false" default="0000" hint="Ignoré">
		<cfset var httpResponse={}>
		<cfset var actionStatus=FALSE>
		<cftry>
			<cfset var msg="NO MESSAGE">
			<cfset var serial=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var deviceUUID=deviceFromCache.id_mdm>
				<cfset var mdmServer=getMdmServer()>
				<cfset var infos=mdmServer.getMdmServerInfo()>
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset service.endpoint=service.endpoint & "/" & deviceUUID & "/lockDevice">
				<cfset service.HTTP_METHOD=remoting.HTTP_POST()>
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{type="header",name="User-Agent",value="SaaSwedo"},
					{type="header",name="aw-tenant-code",value=infos.getToken()},
					{type="formfield",name="id",value=deviceUUID}
				]>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({
						UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],
						SERIAL=arguments.serialNumber,IMEI=arguments.imei,CLIENT_REQUEST={service=service,params=params}
					})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif (httpCode NEQ 200) AND (httpCode NEQ 202)>
						<cfset storeError({
							UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],SERVER_RESPONSE=httpResponse,
							SERIAL=arguments.serialNumber,IMEI=arguments.imei,CLIENT_REQUEST={service=service,params=params}
						})>
					<!--- CODE HTTP AIRWATCH : 202 --->
					<cfelse>
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset actionStatus=TRUE>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<cfreturn actionStatus>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="unlock" returntype="Boolean"
	hint="Déverrouille un device et supprime le passcode (PIN) existant">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var httpResponse={}>
		<cfset var actionStatus=FALSE>
		<cftry>
			<cfset var msg="NO MESSAGE">
			<cfset var serial=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var deviceUUID=deviceFromCache.id_mdm>
				<cfset var mdmServer=getMdmServer()>
				<cfset var infos=mdmServer.getMdmServerInfo()>
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset service.endpoint=service.endpoint & "/" & deviceUUID & "/clearpasscode">
				<cfset service.HTTP_METHOD=remoting.HTTP_POST()>
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{type="header",name="User-Agent",value="SaaSwedo"},
					{type="header",name="aw-tenant-code",value=infos.getToken()},
					{type="formfield",name="id",value=deviceUUID}
				]>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({
						UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],
						SERIAL=arguments.serialNumber,IMEI=arguments.imei,CLIENT_REQUEST={service=service,params=params}
					})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif (httpCode NEQ 200) AND (httpCode NEQ 202)>
						<cfset storeError({
							UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],SERVER_RESPONSE=httpResponse,
							SERIAL=arguments.serialNumber,IMEI=arguments.imei,CLIENT_REQUEST={service=service,params=params}
						})>
					<!--- CODE HTTP AIRWATCH : 202 --->
					<cfelse>
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset actionStatus=TRUE>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<cfreturn actionStatus>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="locate" returntype="Boolean" hint="Localise un device. L'opération est synchrone (Bloquante)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var httpResponse={}>
		<cfset var actionStatus=FALSE>
		<cftry>
			<cfset var msg="NO MESSAGE">
			<cfset var serial=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var deviceUUID=deviceFromCache.id_mdm>
				<cfset var mdmServer=getMdmServer()>
				<cfset var infos=mdmServer.getMdmServerInfo()>
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset service.endpoint=service.endpoint & "/" & deviceUUID & "/gps">
				<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{type="header",name="User-Agent",value="SaaSwedo"},
					{type="header",name="aw-tenant-code",value=infos.getToken()}
				]>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({
						UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],
						SERIAL=arguments.serialNumber,IMEI=arguments.imei,CLIENT_REQUEST={service=service,params=params}
					})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif httpCode NEQ 200>
						<cfset storeError({
							UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],
							SERIAL=arguments.serialNumber,IMEI=arguments.imei,CLIENT_REQUEST={service=service,params=params}
						})>
					<cfelse>
						<!--- Désérialisation de la réponse HTTP --->
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset var responseContent=httpResponse.FileContent>
							<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
							<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
							<cfset actionStatus=(isArray(jsonResponse) AND (arrayLen(jsonResponse) GT 0))>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<cfreturn actionStatus>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn actionStatus>
			</cfcatch>
		</cftry>
	</cffunction>

	<!--- Réinitialise le device puis effectue l'action REVOKE pour le retirer du MDM. Retourne le statut des 2 actions --->
	<cffunction access="public" name="wipe" returntype="Boolean" hint="Réinitialise un device puis le retire du MDM avec revoke()
	pour éviter la présence de doublons (SERIAL,IMEI). Mais il n'est pas requis car les devices concernés ne sont pas ramenés par cette API">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="erasedMemoryCard" type="Boolean" required="false" default="false" hint="Ignoré">
		<cfset var httpResponse={}>
		<cfset var actionStatus=FALSE>
		<cftry>
			<cfset var msg="NO MESSAGE">
			<cfset var serial=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var deviceUUID=deviceFromCache.id_mdm>
				<cfset var mdmServer=getMdmServer()>
				<cfset var infos=mdmServer.getMdmServerInfo()>
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset service.endpoint=service.endpoint & "/" & deviceUUID & "/devicewipe">
				<cfset service.HTTP_METHOD=remoting.HTTP_POST()>
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{type="header",name="User-Agent",value="SaaSwedo"},
					{type="header",name="aw-tenant-code",value=infos.getToken()},
					{type="formfield",name="id",value=deviceUUID}
				]>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({
						UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],
						SERIAL=arguments.serialNumber,IMEI=arguments.imei,CLIENT_REQUEST={service=service,params=params}
					})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif (httpCode NEQ 200) AND (httpCode NEQ 202)>
						<cfset storeError({
							UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],SERVER_RESPONSE=httpResponse,
							SERIAL=arguments.serialNumber,IMEI=arguments.imei,CLIENT_REQUEST={service=service,params=params}
						})>
					<!--- CODE HTTP AIRWATCH : 202 --->
					<cfelse>
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset actionStatus=TRUE>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<cfreturn actionStatus>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="corporateWipe" returntype="Boolean" hint="Non supporté">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var httpResponse={}>
		<cfset var actionStatus=FALSE>
		<cftry>
			<cfset var msg="NO MESSAGE">
			<cfset var serial=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var deviceUUID=deviceFromCache.id_mdm>
				<cfset var mdmServer=getMdmServer()>
				<cfset var infos=mdmServer.getMdmServerInfo()>
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset service.endpoint=service.endpoint & "/" & deviceUUID & "/enterprisewipe">
				<cfset service.HTTP_METHOD=remoting.HTTP_POST()>
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{type="header",name="User-Agent",value="SaaSwedo"},
					{type="header",name="aw-tenant-code",value=infos.getToken()},
					{type="formfield",name="id",value=deviceUUID}
				]>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({
						UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],
						SERIAL=arguments.serialNumber,IMEI=arguments.imei,CLIENT_REQUEST={service=service,params=params}
					})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif (httpCode NEQ 200) AND (httpCode NEQ 202)>
						<cfset storeError({
							UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],SERVER_RESPONSE=httpResponse,
							SERIAL=arguments.serialNumber,IMEI=arguments.imei,CLIENT_REQUEST={service=service,params=params}
						})>
					<!--- CODE HTTP AIRWATCH : 202 --->
					<cfelse>
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset actionStatus=TRUE>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<cfreturn actionStatus>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="revoke" returntype="Boolean" hint="La révocation correspond au retrait un device dans MobileIron">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<!--- TODO: A implémenter --->
		<cfthrow type="Custom" message="#componentName()#.revoke() is not supported for this version for this provider">
	</cffunction>
	
	<cffunction access="public" name="register" returntype="Boolean" hint="Retourne toujours TRUE car n'existe pas dans MobileIron">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<!--- TODO: A implémenter --->
		<cfthrow type="Custom" message="#componentName()#.register() is not supported for this version for this provider">
	</cffunction>
	
	<cffunction access="public" name="remove" returntype="void" hint="Retire un device du MDM en utilisant revoke()">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<!--- TODO: A implémenter --->
		<cfthrow type="Custom" message="#componentName()#.remove() is not supported for this version for this provider">
	</cffunction>
	
	<!--- =========== Action MDM non supportées ou non implémentées =========== --->
	
	<cffunction access="public" name="putDeviceProperties" returntype="void" hint="Non supporté">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="properties" type="Array" required="true" hint="Propriétés à ajouter">
		<!--- TODO: A implémenter --->
		<cfthrow type="Custom" message="#componentName()#.putDeviceProperties() is not supported for this version for this provider">
	</cffunction>
	
	<cffunction access="public" name="add" returntype="String" hint="Non supporté">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="osFamily" type="String" required="true" hint="OS du device">
		<!--- TODO: A implémenter --->
		<cfthrow type="Custom" message="#componentName()#.add() is not supported for this version for this provider">
	</cffunction>
	
	<!--- =========== Fin des fonction publiques =========== --->
</cfcomponent>