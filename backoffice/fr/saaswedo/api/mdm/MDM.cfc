<cfinterface author="Cedric" hint="Interface d'accès aux fonctionnalités MDM.
Par défaut dans toutes les implémentations : L'IMEI est considéré comme une chaine vide et est remplacée par le numéro de série quand c'est le cas">
	<cffunction name="init" returntype="fr.saaswedo.api.mdm.MDM" hint="Constructeur que doivent avoir toutes les implémentations">
		<cfargument name="mdmServer" type="fr.saaswedo.api.mdm.MdmServer" required="true" hint="Infos serveur MDM">
	</cffunction>
	
	<cffunction name="getMdmServer" returntype="fr.saaswedo.api.mdm.MdmServer" hint="Retourne les infos serveur MDM fourni à l'instanciation"> 
	</cffunction>
	
	<cffunction name="getInventory" returntype="fr.saaswedo.api.mdm.Inventory" hint="Retourne le composant d'inventaire MDM"> 
	</cffunction>
	
	<cffunction name="getManager" returntype="fr.saaswedo.api.mdm.Manager" hint="Retourne le composant de gestion MDM">
	</cffunction>
</cfinterface>