component implements="fr.saaswedo.api.order.bridge.ICall" { 
	
	public ICall function init(required struct Context)
        output="false"
	{
		WriteLog(type="INFO", file="sonar", text="[INIT] DOE CALL"); 

		return( this );
	}
 
	public boolean function execute()
	{
		return true;
	}

	public void function notifySucess()    	
	{
		 writeOutput("OK");
	}
	
	public void function notifyFailure()
	{
		 writeOutput("KO");
	}
	
	public void function addEventListener(required struct handler)
	{
		
	}
 	
	public string function getDefinition()		
	{
		return 'doe';
	}
		
	
	
}