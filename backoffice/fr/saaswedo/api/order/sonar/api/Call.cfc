/** * 
* @output false
*/ 
component implements="fr.saaswedo.api.order.bridge.ICall" { 

	/**
	* getter true
	* setter false
	**/
	property
        name = "context"
        type = "struct"
        hint = "order context";
	
	
	/**
	* getter true
	* setter false
	**/
	property
        name = "httpService"
        type = "http"
        hint = "http request object";
    
    /**
	* getter true
	* setter false
	**/   
    property
        name = "operation"
        type = "fr.saaswedo.api.order.bridge.IOperation"
        hint = "api operation";
        
 	 /**
	* getter true
	* setter false
	**/   
    property
        name = "handler"
        type = "struct"
        hint = "result token";
	
	
	public ICall function init(required struct Context)
        output="false"
	{
		this.context = arguments.Context;		
		this.context.endpoint = "https://sonar.managemobility.com/api/";
		//this.context.endpoint = "http://backoffice-dev.local/fr/saaswedo/api/order/bridge/FakeServer.cfm?";
		this.context.apiKey = "AF6EB676-D0B7-4E48-9F51-6A157570005E";
		//this.context.boundary = "00ab558a-4ba9-46c6-9c5a-73e8419241bf";
		
		
		
		if(CompareNoCase(this.context.callType,'CREATE_ORDER') eq 0)
		{
		    this.operation = new CreateOrder(context);
			
		}else		
		if(CompareNoCase(this.context.callType,'LIST_ORDER') eq 0)
		{
			 this.operation = new OrderByAccount(context);
		}else
		
		if(CompareNoCase(this.context.callType,'GET_ORDER') eq 0)
		{
		 	 this.operation = new OrderById(context);		 
		}else
		if(CompareNoCase(this.context.callType,'CREATE_TICKET') eq 0)
		{
		    this.operation = new CreateTicket(context);
			
		}else
		if(CompareNoCase(this.context.callType,'UPDATE') eq 0)
		{
			 
		}
		
		WriteLog(type="INFO", file="sonar", text="[INFO] SONAR CALL #this.context.callType#"); 

		
		return( this );
    }
	
	
	public boolean function execute()
	{
		
		params = this.context.HttpCall;		
		this.httpService = new http (method : this.context.HttpCall.method, 
							url : this.context.HttpCall.url,
							multipart : this.context.HttpCall.multipart							
							);
		this.httpService.clearParams();	
		
		args = this.context.HttpCall.HttpParams;
		
		
	 	for(var i = 1; i < arraylen(args); i++ ) {
	 		
		    this.httpService.addParam(argumentCollection = args[i]);
		}		
	 	
		result = this.httpService.send().getPrefix();
				 
		this.handler.result = result;
		
		this.operation.purgeTemporyItems();
		
		return resultHandler(this.handler.result);
 			
    }
    
    public void function notifySucess()    	
	{
		mock_notifySucess();
	}
	
	public void function notifyFailure()
	{
		mock_notifyFailure();
	}
	
	public void function addEventListener(required struct handler)
	{
		arguments.handler.result = this.operation.getResult();
	}
 
	
	
	
	
	
	
	public string function getDefinition()		
	{
		return 'sonar';
	}
		
	
	private boolean function resultHandler(required struct result)
	{
		var data = this.operation.manageResult(arguments.result);

		WriteLog(type="INFO", file="sonar", text="[RESULT] SONAR CALL #this.operation.result.DATA#");

		WriteLog(type="INFO", file="sonar", text="[RESULT] SONAR CALL #this.operation.result.STATUS#");
		return data;
	}
	
	
	
	
	
	
	
	
	
	
	private void function mock_notifySucess()
    	output="true"
	{
		writeOutput("OK :-) ");

	}
	
	private void function  mock_notifyFailure()
		output="true"
	{
		writeOutput("KO :-( ");
	}
}