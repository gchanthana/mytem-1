component  displayname="Product"
{
	property name="ArticleId" type="numeric" displayname="ArtilceId" hint="Identifiant de l'article";

	property name="Order" type="struct" displayname="Order" hint="All Order products";

	public Product function init(required numeric ArticleId, required struct Order)
	{
		this.ArticleId = arguments.ArticleId;
		this.Order = arguments.Order;
		return this;
	}

	public Array function getSonarProduct()
		description="struct sonar product"
	{
		var Products = [];
		arrayAppend(Products,getProductDevice(this.ArticleId,this.Order['ARTICLE'],this.Order['DEVICE'],this.Order['PLAN']));
		Products = addAccessories(this.ArticleId,Products,this.Order['ACCESSORY']);
		return Products;
	}

	private struct function getProductDevice(required numeric ArticleId,
							 required Query qAllArticles,
							 required Query qAllDevices,
							 required Query qAllPlans)
	{

		var prefix = "::str::";
		var device = {};
		var phoneNumbers = [];

		var queryService = new query();
		queryService.setName("qGetProductDevice");
		queryService.setDBType("query");
		queryService.setAttributes(sourceQuery=arguments.qAllDevices);
		var qProductDevice =
		queryService.execute(sql="SELECT * FROM sourceQuery  WHERE	IDARTICLE = #arguments.ArticleId# AND IDTYPE_EQUIPEMENT in (70,2192,2292)").getResult();

		//ARTICLE
		var queryService2 = new query();
		queryService2.setName("qGetArticleData");
		queryService2.setDBType("query");
		queryService2.setAttributes(sourceQuery2=arguments.qAllArticles);
		var qArticleData=
		queryService2.execute(sql="SELECT * FROM sourceQuery2  WHERE	IDARTICLE = #arguments.ArticleId#").getResult();


    var allRows = qProductDevice.recordcount;
		var plans = [];

		if(allRows gt 0){

			for (	var intRow = 1;
				intRow <= allRows;
				intRow ++)
			{
			  device['Type'] 		= prefix & 		"1";
				device['SKU'] 		= prefix & 		qProductDevice['REF_REVENDEUR'][intRow];
				device['Price'] 	= prefix & 		qProductDevice['PRIX'][intRow];
				device['PriceType'] = prefix & 		manageDevicePriceType(GetQueryRow(qProductDevice,intRow),qArticleData);

				device['Qty'] 		= 1;
			}

		}else{
			device = getNoDevice();
		}




		if(this.Order['COMMANDE']['IDTYPE_OPERATION'][1] eq 1382)//renew
		{
			plans = getUPGPlans();
			phoneNumbers = getPhoneNumbers(arguments.ArticleId,arguments.qAllArticles);
			if(StructKeyExists(device, "SKU")) device['PhoneNumbers'] = phoneNumbers;

		}else if (this.Order['COMMANDE']['IDTYPE_OPERATION'][1] eq 1182)//device only
		{
			plans = getNoVoicePlans();
			if(StructKeyExists(device, "SKU")) 	device['PriceType'] =  prefix & "2";
		}
		else{
			plans = getProductPlans(arguments.ArticleId,arguments.qAllPlans);
		}

		if(ArrayLen(plans) == 0)
		{
			plans = getNoVoicePlans();
			if(StructKeyExists(device, "SKU"))  device['PriceType'] =  prefix & "2";
		}


		device['Plans'] = plans;

		device['EndUser'] = getEndUser(arguments.ArticleId,arguments.qAllArticles);

		return device;
	}

	private struct function getNoDevice()
	{
		var prefix = "::str::";
		var device = {};

		device['Type'] 		= prefix & 		"1";
		device['SKU'] 		= prefix & 		"ACC ONLY";
		device['Price'] 	= prefix & 		"0.00";
		device['PriceType'] = prefix & 		"1";
		device['Qty'] 		= 1;

		return device;
	}

	private Array function getUPGPlans()
	{
		var prefix = "::str::";
		var plans = [];
		var plan = {};

		plan['Type'] 		= prefix & 		"1";
		plan['Code'] 		= prefix & 		"UPG";
		plan['Price'] 		= prefix & 		"0.00";
		arrayAppend(plans,plan);

		return plans;
	}

	private Array function getNoVoicePlans()
	{
		var prefix = "::str::";
		var plans = [];
		var plan = {};

		plan['Type'] 		= prefix & 		"1";
		plan['Code'] 		= prefix & 		"NOVOICE";
		plan['Price'] 		= prefix & 		"0.00";
		arrayAppend(plans,plan);

		return plans;
	}


	private Array function getProductPlans(required numeric ArticleId,
											required Query qAllPlans)
	{
		var prefix = "::str::";
		var plans = [];

		var queryService = new query();
		queryService.setName("qGetProductPlans");
		queryService.setDBType("query");
		queryService.setAttributes(sourceQuery=arguments.qAllPlans);
		var qProductPlans = queryService.execute(sql="SELECT * FROM sourceQuery  WHERE IDARTICLE = #arguments.ArticleId#").getResult();


		var allRows = qProductPlans.recordcount;

	    for (
	    		var intRow = 1;
				intRow <= allRows;
				intRow ++
			)
		{
			var plan = {};
		    plan['Type'] 		= prefix & 		managePlansType(val(qProductPlans['IDTHEME_PRODUIT'][intRow]));
			plan['Code'] 		= prefix & 		qProductPlans['REFERENCE'][intRow];
			plan['Price'] 		= prefix & 		qProductPlans['PRIX'][intRow];
			arrayAppend(plans,plan);
		}


		return plans;
	}

	private struct function getEndUser(		required numeric ArticleId,
											required Query qAllArticles)
	{
		var prefix = "::str::";
		var owner = {};

		var queryService = new query();
		queryService.setName("qGetProductOwner");
		queryService.setDBType("query");
		queryService.setAttributes(sourceQuery=arguments.qAllArticles);
		var qProductOwner = queryService.execute(sql="SELECT * FROM sourceQuery  WHERE IDARTICLE = #arguments.ArticleId#").getResult();


		var allRows = qProductOwner.recordcount;

	    for (	var intRow = 1;
				intRow <= allRows;
				intRow ++)
		{
			if(val(qProductOwner['IDEMPLOYE'][intRow]) > 0)
			{

				owner['EmployeeEmail'] 		= prefix & 		qProductOwner['EMAIL'][intRow];
				owner['EmployeeID'] 		= prefix & 		qProductOwner['MATRICULE'][intRow];
				owner['FirstName'] 			= prefix & 		qProductOwner['PRENOM'][intRow];
				owner['LastName'] 			= prefix & 		qProductOwner['NOM'][intRow];

			}
			else
			{

				owner['EmployeeID'] 			= prefix & 		qProductOwner['CODE_INTERNE'][intRow];

			}

			owner['PhoneNumber']		= prefix & 		qProductOwner['NUMERO'][intRow];

		}

		return owner;
	}

	private array function getPhoneNumbers(		required numeric ArticleId,
											required Query qAllArticles)
	{
		var prefix = "::str::";
		var phoneNumbers = [];

		var queryService = new query();
		queryService.setName("qGetPhoneNumbers");
		queryService.setDBType("query");
		queryService.setAttributes(sourceQuery=arguments.qAllArticles);
		var qPhoneNumbers = queryService.execute(sql="SELECT * FROM sourceQuery  WHERE IDARTICLE = #arguments.ArticleId#").getResult();
		var thePhone = "";
		var tmp = "";

		var allRows = qPhoneNumbers.recordcount;

			for (	var intRow = 1;
				intRow <= allRows;
				intRow ++)
		{
			thePhone = formatPhoneNumber(trim(qPhoneNumbers['NUMERO'][intRow]));

			if(len(thePhone) gt 10)		WriteLog(type="ERROR", file="sonar", text="[SONAR API PLAN UPG] #thePhone# must be ten digit");
			thePhone = prefix & formatPhoneNumber(trim(qPhoneNumbers['NUMERO'][intRow]));

			ArrayAppend(phoneNumbers, thePhone);
		}

		return phoneNumbers;
	}


	private array function addAccessories(required numeric ArticleId,required array Products, required Query qAllAccessories)
	{
		var prefix = "::str::";
		var accessory = {};
		var p = arguments.Products;

		var queryService = new query();
		queryService.setName("qGetAccessory");
		queryService.setDBType("query");
		queryService.setAttributes(sourceQuery=arguments.qAllAccessories);
		var qProductAccessory = queryService.execute(sql="SELECT * FROM sourceQuery  WHERE IDARTICLE = #arguments.ArticleId#").getResult();


	    var allRows = qProductAccessory.recordcount;

	    for (	var intRow = 1;
				intRow <= allRows;
				intRow ++)
		{
			var accessory = {};
			accessory['Type'] 			= prefix & 		"2";
			accessory['SKU'] 			= prefix & 		qProductAccessory['REF_REVENDEUR'][intRow];
			accessory['Price'] 			= prefix & 		qProductAccessory['PRIX'][intRow];
			accessory['PriceType'] 		= prefix & 		manageAccessoryPriceType(GetQueryRow(qProductAccessory,intRow));
			accessory['Qty'] 			= prefix & 		"1";
			arrayappend(p,accessory);
		}

		return p;
	}







	private string function manageDevicePriceType(required struct DeviceRow,required query qArticle)
	{

		var type = "1";
		var engagement = arguments.qArticle['ENGAGEMENT_EQ'][1];
		var ordertype = this.Order['COMMANDE']['IDTYPE_OPERATION'][1];

		if( ordertype eq 1382)
		{
			type = "2";
		}else	if( engagement eq 12)
		{
			type = "3";
		}else if(engagement eq 24)
		{
			type = "4";
		}
		//return DeviceRow.BONUS
		return type;
	}

	private string function manageAccessoryPriceType(required struct AccesoryRow)
	{

		//return AccesoryRow.BONUS
		return "5";
	}

	private string function managePlansType(required numeric idtheme)
	{
		if(listfindNoCase('41',arguments.idtheme,',') > 0)
		{
			return 		 "1";
		}
		else if(listfindNoCase('139,54',arguments.idtheme,',') > 0)
		{
			return 		 "2";
		}
		else
		{
			return 		 "3";
		}
	}

	private struct function GetQueryRow(query, rowNumber)
	{
		var i = 0;
		var rowData = {};
		var cols = ListToArray(query.columnList);

		for (i = 1; i <= ArrayLen(cols); i++)
		{
			rowData[cols[i]] = query[cols[i]][rowNumber];
		}
		return rowData;
	}

	private string function formatPhoneNumber(required string input){
		var _input = arguments.input;
		var arrSearch = rematch("[\d]+",_input);
		var output = ArrayToList(arrSearch,"");
		return output;
	}

}
