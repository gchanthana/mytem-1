﻿component extends="Proc"
{

	public LinkOrder function init(string datasource)
	output="false"
	{
		super.init();

		if(isdefined('arguments.datasource'))
		{
			this.datasource = arguments.datasource;
		}

		return( this );
	}

	public any function execute(required numeric MytemOrderId, required string SonarOrderId)
	 displayname="execute" description="execute the stored procedure" hint="execute" output="true"
	{
		var spService = new storedproc();
		var result = {};

		spService.setDatasource(this.datasource);
		spService.setProcedure("PKG_M16.LINK_SONARORDER_V2");

		spService.addParam(type="in", cfsqltype="cf_sql_numeric", dbvarname= "p_idcommande", value=arguments.MytemOrderId);
		spService.addParam(type="in", cfsqltype="cf_sql_varchar", dbvarname= "p_sonar_order", value=arguments.SonarOrderId);
		spService.addParam(type="out", cfsqltype="cf_sql_numeric", dbvarname= "p_retour", variable="p_retour");

		procresult = spService.execute();
		result.status = procresult.getprocOutVariables().p_retour;

		return result;
	}

}
