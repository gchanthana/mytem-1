/** *
* @output false
*/
component{

	/**
	* getter true
	* setter false
	**/
	property
        name = "context"
        type = "struct"
        hint = "order context";


	public CallFactory function init(
								required string CallType
								,required struct Context )
        output="false"
	{
		this.context = arguments.Context;
		this.context.callType = arguments.CallType;
        return( this );
    }


	public ICall function createCall()
	{
		_endpoint = getEndPoint();

		WriteLog(type="INFO", file="sonar", text="[INFO] call fr.saaswedo.api.order.#_endpoint#.api.Call(this.context)");

		call = Evaluate( "new fr.saaswedo.api.order.#_endpoint#.api.Call(this.context)");
		WriteLog(type="INFO", file="sonar", text="[INFO] #_endpoint#");

		return call;
	}


	private string function getEndPoint()
	output="false"
	{
		return  "sonar";
	}


	private string function mock_getEndPoint()
		output="false"
	{

		switch(this.context.RacineID) {
			case "2458788":
			case "6396614":
			case "6310158":
					return "sonar";
				 break;

			case "310812":
					return "doe";
				 break;

			default:
				 return "doe";
		}

	}



}
