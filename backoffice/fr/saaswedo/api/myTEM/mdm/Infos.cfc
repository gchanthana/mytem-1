<cfcomponent author="Cedric" displayName="fr.saaswedo.api.myTEM.mdm.Infos" extends="fr.saaswedo.api.mdm.impl.MdmServerInfo"
hint="Implémentation dans MyTEM des infos d'un serveur MDM. Les infos sont stockées et récupérées à partir de la session utilisateur.
<br>L'API MDM de MyTEM requiert que les conditions suivantes soient vérifiées pour fonctionner :<br>
<br>- Les scopes APPLICATION et SESSION doivent etre définis
<br>- Une clé USER doit etre présente dans le scope SESSION et associée à une structure
<br>- Le contenu de la clé SESSION.USER doit etre supprimé et recréé à chaque déconnexion/reconnexion de l'utilisateur
<br>- Une clé ID_GROUPE doit etre présente dans la structure SESSION.PERIMETRE et doit identifier le groupe (racine) de la session<br>
<br>Les infos MDM sont initialisées ou mises à jour par une fonction privée appelée par le constructeur si une des conditions suivantes est vérifiée :
<br>- Les infos MDM ne sont pas présentes en session (e.g Suite à une déconnection/Reconnexion de l'utilisateur)
<br>- Le groupe en session est différent de celui mentionné dans les infos MDM (e.g Suite à un changement de groupe par l'utilisateur)
<br><b>De ce fait le constructeur de cette implémentation ne lève pas d'exception quand elle est appelée sur une instance déjà initialisée</b>">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.myTEM.mdm.Infos">
		<cfreturn "fr.saaswedo.api.myTEM.mdm.Infos">
	</cffunction>

	<cffunction access="public" name="init" returntype="fr.saaswedo.api.myTEM.mdm.Infos" description="LOCK:Exclusive" hint="Effectue dans l'ordre :
	<br>- Vérification des conditions de fonctionnement de l'API<br>- Appel du constructeur parent<br>Initialisation/MAJ des infos MDM de la session">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<!--- Vérification des conditions de fonctionnement --->
			<cfset checkMdmRequirements()>
			<!--- Constructeur parent appelé avec des paramètres factices car inutilisés : Il appele setMdmInfos() --->
			<cfset SUPER.init("","",TRUE,"","")>
			<cfreturn THIS>
		</cflock>
	</cffunction>

	<!--- =========== Fonctions de fr.saaswedo.api.mdm.core.services.IMdmServerInfo utilisées dans MyTEM =========== --->

	<cffunction access="public" name="getEnrollUsername" returntype="String" hint="Login d'enrollment">
		<cfreturn getMdmInfos().ENROLL_USERNAME>
	</cffunction>
	
	<cffunction access="public" name="getEnrollPassword" returntype="String" hint="Mot de passe d'enrollment">
		<cfreturn getMdmInfos().ENROLL_PASSWORD>
	</cffunction>
	
	<cffunction access="public" name="getAdminEmail" returntype="String" hint="Email de l'administrateur MDM">
		<cfreturn getMdmInfos().ADMIN_EMAIL>
	</cffunction>

	<!--- =========== Infos MDM spécifiques à MyTEM =========== --->
		
	<cffunction access="public" name="areDefault" returntype="Boolean" hint="Retourne TRUE si les infos MDM sont celles du serveur par défaut">
		<cfreturn getMdmInfos().IS_DEFAULT>
	</cffunction>

	<!--- =========== Fonctions spécifiques à MyTEM (e.g Module M27) pour la gestion du référentiel des infos MDM d'un groupe =========== --->
	
	<cffunction access="public" name="getProviders" returntype="Array" hint="Retourne la liste des providers. ArrayOfStruct: NAME,VALUE">
		<cfreturn [
			{NAME="Zenprise (Citrix)",VALUE="zenprise"},{NAME="MobileIron",VALUE="mobileiron"},{NAME="Airwatch",VALUE="airwatch"}
		]>
	</cffunction>
	
	<cffunction access="public" name="getMdmServerInfos" returntype="Struct" hint="Retourne les infos du serveur MDM de la racine">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cftry>
			<cfset var idRacine=ARGUMENTS.idGroupe>
			<cfset var dataSource=(isDefined("SESSION") AND structKeyExists(SESSION,"OFFREDSN")) ? SESSION.OFFREDSN:"ROCOFFRE">
			<!--- Récupération des infos MDM de la racine --->
			<cfset var procedureName="PKG_MDM.get_server_racine_v3">
			<cfstoredproc datasource="#datasource#" procedure="#procedureName#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="procedure_status">
				<cfprocresult name="qMdmServerInfos">
			</cfstoredproc>
			<cfif (qMdmServerInfos.recordcount GT 0) AND (procedure_status GT 0)>
				<!--- Code d'erreur --->
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<!--- Structure des infos MDM : La valeur ramenée de la propriété MDM doit etre de la forme "PROVIDER://URI" --->
				<cfset var mdmServerInfos={
					MDM_TYPE=qMdmServerInfos["MDM_TYPE"][1],MDM=qMdmServerInfos["MDM"][1],USE_SSL=yesNoFormat(VAL(qMdmServerInfos["USE_SSL"][1])),
					USERNAME=qMdmServerInfos["USERNAME"][1],PASSWORD=qMdmServerInfos["PASSWORD"][1],USERID=VAL(qMdmServerInfos["USERID"][1]),
					ENROLL_USERNAME=qMdmServerInfos["ENROLL_USERNAME"][1],ENROLL_PASSWORD=qMdmServerInfos["ENROLL_PASSWORD"][1],
					IS_DEFAULT=yesNoFormat(VAL(qMdmServerInfos["IS_DEFAULT"][1])),ADMIN_EMAIL=qMdmServerInfos["ADMIN_EMAIL"][1],
					TEST_MSG=qMdmServerInfos["TEST_MSG"][1],IS_TEST_OK=yesNoFormat(VAL(qMdmServerInfos["IS_TEST_OK"][1])),TOKEN=qMdmServerInfos["TOKEN"][1]
				}>
				<!--- Vérification de la valeur de la propriété MDM --->
				<cfset var mdmValue=mdmServerInfos.MDM>
				<cfif (TRIM(mdmValue) EQ "") OR (TRIM(mdmValue) NEQ mdmValue)>
					<cfset var extendedInfos=isDefined("qMdmServerInfos") ? serializeJSON(qMdmServerInfos):"">
					<cfthrow type="Custom" errorcode="#code#" message="#procedureName# returns invalid MDM '#mdmValue#' for idGroupe #idRacine#">
				<cfelse>
					<!--- Propriétés MDM et MDM_TYPE --->
					<cfset mdmServerInfos.MDM_TYPE=extractMdmProvider(mdmValue)>
					<cfset mdmServerInfos.MDM=extractMdm(mdmValue)>
					<!--- Traitement et conversion des valeurs (Le type Booléen de Java est utilisé pour compatibilité avec ActionScript) --->
					<cfset mdmServerInfos.IS_DEFAULT=createObject("java","java.lang.Boolean").init(mdmServerInfos.IS_DEFAULT ? "true":"false")>
					<cfset mdmServerInfos.USE_SSL=createObject("java","java.lang.Boolean").init(mdmServerInfos.USE_SSL ? "true":"false")>
					<cfset mdmServerInfos.IS_TEST_OK=createObject("java","java.lang.Boolean").init(mdmServerInfos.IS_TEST_OK ? "true":"false")>
					<cfreturn mdmServerInfos>
				</cfif>
			<cfelseif procedure_status GT 0>
				<cfset var mdmServerInfos={
					MDM_TYPE="",MDM="",USE_SSL=FALSE,
					USERNAME="",PASSWORD="",USERID="",
					ENROLL_USERNAME="",ENROLL_PASSWORD="",
					IS_DEFAULT=TRUE,ADMIN_EMAIL="",
					TEST_MSG="",IS_TEST_OK=FALSE,TOKEN=""
				}>
				<cfreturn mdmServerInfos>
			<cfelse>
				<cfthrow type="Custom" errorcode="#code#" message="#procedureName# returns status #procedure_status# for idGroupe #idRacine#">
			</cfif>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="resetMdmServerInfos" returntype="Struct" hint="Retourne les infos MDM par défaut.
	Si updateValues vaut TRUE (Par défaut) : Les valeurs des infos MDM du groupe idGroupe seront remplacées par les infos MDM par défaut">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfargument name="userId" type="Numeric" required="true" hint="Identifiant de l'utilisateur effectuant la réinitialisation">
		<cfargument name="updateValues" type="Boolean" required="false" default="true" hint="TRUE pour remplacer les valeurs existantes et FALSE sinon">
		<cftry>
			<cfset var dataSource=(isDefined("SESSION") AND structKeyExists(SESSION,"OFFREDSN")) ? SESSION.OFFREDSN:"ROCOFFRE">
			<!--- Ramène les infos MDM par défaut et les modifier celles du groupe idGroupe --->
			
			<!---Fix MYT-956--->
			<cfset ARGUMENTS["updateValues"]=TRUE>
			<cfif ARGUMENTS["updateValues"]>
				<cfset var procedureName="PKG_MDM.delete_server_racine">
				<cfstoredproc datasource="#datasource#" procedure="#procedureName#">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS.idGroupe#">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS.userId#">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="procedure_status">
					<cfprocresult name="qResetMdmServerInfos">
				</cfstoredproc>
				<cfset procedure_status=VAL(qResetMdmServerInfos["STATUS"][1])>
				<!--- Vérification du statut de la procédure --->
				<cfif (qResetMdmServerInfos.recordcount GT 0) AND (VAL(qResetMdmServerInfos["STATUS"][1]) GT 0)>
					<!--- Récupération des infos MDM de la racine (Après réinitialisation) --->
					<cfreturn getMdmServerInfos(ARGUMENTS.idGroupe)>
				<cfelse>
					<cfset var code=GLOBALS().CONST("MDM.ERROR")>
					<cfset var extendedInfos=isDefined("qResetMdmServerInfos") ? serializeJSON(qResetMdmServerInfos):"">
					<cfthrow type="Custom" errorcode="#code#"
						message="#procedureName# status #procedure_status# (idGroupe:#ARGUMENTS.idGroupe#)" detail="#extendedInfos#">
				</cfif>
			<!--- Ramène les infos MDM par défaut sans modifier celles du groupe idGroupe --->
			<cfelse>
				<cfreturn getMdmServerInfos(0)>
			</cfif>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="updateMdmServerInfos" returntype="void" hint="Met à jour les valeurs des infos MDM">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfargument name="userId" type="Numeric" required="true" hint="Identifiant de l'utilisateur effectuant la mise à jour">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Structure contenant les infos MDM avec leur valeurs">
		<cftry>
			<cfset var dataSource=(isDefined("SESSION") AND structKeyExists(SESSION,"OFFREDSN")) ? SESSION.OFFREDSN:"ROCOFFRE">
			<cfset var isMdmInfosValid=TRUE>
			<cfset var mdmProps=ARGUMENTS["mdmInfos"]>
			<!--- Vérification des infos MDM --->
			<cfset var mdmPropertyList="ADMIN_EMAIL,MDM,USE_SSL,USERNAME,PASSWORD,ENROLL_USERNAME,ENROLL_PASSWORD,MDM_TYPE,TOKEN">
			<cfset var mdmMissingPropertyList="">
			<cfloop list="#mdmPropertyList#" index="listItem" delimiters=",">
				<cfif CompareNoCase(listItem,'TOKEN') eq 0>
					<cfif (structKeyExists(mdmProps,listItem) AND (TRIM(mdmProps[listItem]) EQ ""))>
						<cfset mdmProps[listItem]='-'>
					</cfif>
				</cfif>							
				<cfset isMdmInfosValid=(isMdmInfosValid AND structKeyExists(mdmProps,listItem) AND (TRIM(mdmProps[listItem]) NEQ ""))>			
				
				<cfif NOT (structKeyExists(mdmProps,listItem) AND (TRIM(mdmProps[listItem]) NEQ ""))>
					<cfset mdmMissingPropertyList=listAppend(mdmMissingPropertyList,listItem,",")>
				</cfif>
			</cfloop>

			<cfif isMdmInfosValid>
				<!--- Concaténation des propriétés MDM_TYPE et MDM --->
				<cfset mdmProps.MDM=mdmProps.MDM_TYPE & "://" & mdmProps.MDM>
				<!--- Conversion des valeurs de type Boolean --->
				<cfset mdmProps["USE_SSL"]=(mdmProps["USE_SSL"] ? 1:0)>
				<cfset mdmProps["IS_TEST_OK"]=(mdmProps["IS_TEST_OK"] ? 1:0)>
				<!--- Mise à jour des infos MDM de la racine --->
				<cfset var procedureName="PKG_MDM.set_server_racine">
				<!--- Les mots de passes sont préfixés par 4 caractères * à la suite --->
				<cfset var password=mdmProps.PASSWORD>
				<cfset var enrollPassword=mdmProps.ENROLL_PASSWORD>
				<cfset mdmProps.PASSWORD=repeatString("*",4) & mdmProps.PASSWORD>
				<cfset mdmProps.ENROLL_PASSWORD=repeatString("*",4) & mdmProps.ENROLL_PASSWORD>
				<cfstoredproc datasource="#datasource#" procedure="#procedureName#">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS.idGroupe#">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS.userId#">
					<cfprocparam type="In"  cfsqltype="CF_SQL_CLOB" value="#serializeJSON(mdmProps)#">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="procedure_status">
					<cfprocresult name="qUpdateMdmServerInfos">
				</cfstoredproc>
				<cfset mdmProps.PASSWORD=password>
				<cfset mdmProps.ENROLL_PASSWORD=enrollPassword>
				<cfset procedure_status=VAL(qUpdateMdmServerInfos["STATUS"][1])>
				<!--- Vérification du statut de la procédure --->
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<cfif qUpdateMdmServerInfos.recordcount EQ 0>
					<cfthrow type="Custom" errorcode="#code#" message="#procedureName# returns no data for idGroupe #ARGUMENTS.idGroupe#">
				<cfelseif VAL(qUpdateMdmServerInfos["STATUS"][1]) LTE 0>
					<cfthrow type="Custom" errorcode="#code#"
					message="#procedureName# returns status #procedure_status# for idGroupe #ARGUMENTS.idGroupe#">
				</cfif>
			<cfelse>
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<cfthrow type="Custom" errorcode="#code#" message="MISSING OR EMPTY MDM PROPERTIES" detail="#mdmMissingPropertyList#">
			</cfif>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- =========== Fonctions de gestion des infos MDM de la session utilisateur =========== --->
	
	<cffunction access="private" name="getMdmInfos" returntype="Struct" description="LOCK:ReadOnly" hint="Retourne les infos MDM de la session">
		<cflock scope="SESSION" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfif structKeyExists(SESSION.USER,mdmSessionInfoKey())>
				<cfreturn SESSION["USER"][mdmSessionInfoKey()]>
			<cfelse>
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="MDM infos are not defined in session (SESSION.USER)">
			</cfif>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="setMdmInfos" returntype="void" description="LOCK:Exclusive"
	hint="Initialise ou MAJ des infos MDM de session. Aucune action n'est effectuée si les infos sont déjà initialisées ou déjà à jour">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Ce paramètre est ignoré par cette implémentation">
		<cfargument name="forceUpdate" type="Boolean" required="false" default="FALSE" hint="TRUE pour forcer la mise à jour des infos MDM en session">
		<!--- Initialisation ou mise à jour des infos MDM de la session --->
		<cflock scope="SESSION" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<!--- La valeur est TRUE si les infos MDM sont présentes en session --->
			<cfset var mdmSessionInfoAreUpdated=structKeyExists(SESSION.USER,mdmSessionInfoKey())>
			<cfset var updateLogCause=mdmSessionInfoAreUpdated ? "Updated":"Initialization">
			<cfif mdmSessionInfoAreUpdated>
				<cfset var mdmSessionInfos=SESSION["USER"][mdmSessionInfoKey()]>
				<!--- La valeur est FALSE si les infos MDM ne sont pas à jour : Ici suite à un changement de groupe par l'utilisateur --->
				<cfset mdmSessionInfoAreUpdated=(mdmSessionInfos.ID_GROUPE EQ VAL(SESSION.PERIMETRE.ID_GROUPE))>
				<cfset updateLogCause=mdmSessionInfoAreUpdated ? "Updated":"Groupe changed">
			</cfif>
			<!--- Paramètre qui détermine si la mise à jour sera forcée --->
			<cfif forceUpdate>
				<cfset mdmSessionInfoAreUpdated=FALSE>
				<cfset updateLogCause="Forced update">
			</cfif>
			<!--- Condition pour la récupération et initialisation ou mise à jour des infos MDM --->
			<cfif NOT mdmSessionInfoAreUpdated>
				<cfset var idGroupe=VAL(SESSION.PERIMETRE.ID_GROUPE)>
				<cfset var mdmServerInfos=getMdmServerInfos(idGroupe)>
				<!--- Ajout de l'identifiant du groupe actuel de la session dans les infos MDM qui ont été récupérées --->
				<cfset mdmServerInfos.ID_GROUPE=idGroupe>
				<!--- Date d'initialisation ou de mise à jour des infos MDM en session --->
				<cfset var currentTime=NOW()>
				<cfset mdmServerInfos.LAST_UPDATE_TIME=lsDateFormat(currentTime,"dd/mm/yyyy") & " - " & lsTimeFormat(currentTime,"HH:mm:ss")>
				<!--- Initialise ou remplace (Mise à jour) les infos MDM en session --->
				<cfset SESSION["USER"][mdmSessionInfoKey()]=mdmServerInfos>
				<!--- Log d'information concernant l'action effectuée --->
				<cfset var log=GLOBALS().CONST('Remoting.LOG')>
				<cfset var sessionid=structKeyExists(SESSION,"SESSIONID") ? SESSION.SESSIONID:"N.D">
				<cfset var logMsg="'#getProvider()#' updated because '#updateLogCause#' [idGroupe:#idGroupe#,SESSIONID:#sessionid#]">
				<cflog type="information" text="[#log#] MDM infos with provider #logMsg#">
			</cfif>
		</cflock>
	</cffunction>

	<cffunction access="private" name="checkMdmRequirements" returntype="void" hint="Appelée par init() pour vérifier les conditions de fonctionnement">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<!--- Existence du scope APPLICATION --->
		<cfif NOT isDefined("APPLICATION")>
			<cfthrow type="Custom" errorCode="#code#" message="APPLICATION scope should be defined in order to use this API">
		</cfif>
		<!--- Existence du scope SESSION --->
		<cfif NOT isDefined("SESSION")>
			<cfthrow type="Custom" errorCode="#code#" message="SESSION scope should be defined in order to use this API">
		</cfif>
		<!--- Existence de la clé SESSION.USER --->
		<cfif NOT structKeyExists(SESSION,"USER")>
			<cfthrow type="Custom" errorCode="#code#" message="SESSION.USER should exists in order to use this API">
		<cfelseif NOT isValid("Struct",SESSION.USER)>
			<cfthrow type="Custom" errorCode="#code#" message="SESSION.USER should be a valid Struct in order to use this API">
		</cfif>
		<!--- Existence des clés SESSION.PERIMETRE et SESSION.PERIMETRE --->
		<cfif NOT structKeyExists(SESSION,"PERIMETRE")>
			<cfthrow type="Custom" errorCode="#code#" message="SESSION.PERIMETRE should exists in order to use this API">
		<cfelseif NOT isValid("Struct",SESSION.PERIMETRE)>
			<cfthrow type="Custom" errorCode="#code#" message="SESSION.PERIMETRE should be a valid Struct in order to use this API">
		<cfelseif NOT structKeyExists(SESSION.PERIMETRE,"ID_GROUPE")>
			<cfthrow type="Custom" errorCode="#code#" message="SESSION.PERIMETRE.ID_GROUPE should exists in order to use this API">
		<cfelseif NOT isSimpleValue(SESSION.PERIMETRE.ID_GROUPE)>
			<cfthrow type="Custom" errorCode="#code#" message="SESSION.PERIMETRE.ID_GROUPE should be a simple value in order to use this API">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="mdmSessionInfoKey" returntype="String" hint="Clé placée dans SESSION.USER contenant les infos MDM du groupe">
		<cfreturn "MDM_SESSION_INFOS">
	</cffunction>
	
	<!--- =========== Fonctions de conversion des propriétés MDM =========== --->
	
	<cffunction access="private" name="extractMdmProvider" returntype="String" hint="Retourne la valeur du Provider MDM correspondant">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=extractMdmTokens(ARGUMENTS.mdm)>
		<cfset var mdmProvider=mdmTokens[1]>
		<cfif TRIM(mdmProvider) NEQ mdmProvider>
			<cfset var code=GLOBALS().CONST("MDM.ERROR")>
			<cfset var errorMsg="'#mdmProvider#' extracted from MDM value '#ARGUMENTS.mdm#'">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid Provider #errorMsg#. Cannot contain extra spaces">
		</cfif>
		<cfreturn mdmProvider>
	</cffunction>
	
	<cffunction access="private" name="extractMdm" returntype="String" hint="Retourne l'URI d'accès au MDM sans le protocole (ou le Scheme)">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=extractMdmTokens(ARGUMENTS.mdm)>
		<cfset var mdmValue=mdmTokens[2]>
		<cfif TRIM(mdmValue) NEQ mdmValue>
			<cfset var code=GLOBALS().CONST("MDM.ERROR")>
			<cfset var errorMsg="'#mdmValue#' extracted from MDM value '#ARGUMENTS.mdm#'">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid URI #errorMsg#. Cannot contain extra spaces">
		</cfif>
		<cfreturn mdmValue>
	</cffunction>
	
	<cffunction access="private" name="extractMdmTokens" returntype="Array" hint="Retourne les éléments du découpage d'une la valeur MDM d'origine">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=listToArray(ARGUMENTS.mdm,"://",FALSE,TRUE)>
		<cfif arrayLen(mdmTokens) EQ 2>
			<cfreturn mdmTokens>
		<cfelse>
			<cfset var code=GLOBALS().CONST("MDM.ERROR")>
			<cfthrow type="Custom" errorCode="#code#" message="Cannot extract Provider and URI tokens from invalid MDM value '#ARGUMENTS.mdm#'">
		</cfif>
	</cffunction>
	
	<!--- Fonction héritée de Component qui est appelée quand le constructeur est appelé sur une instance déjà initialisée --->
	<cffunction access="private" name="whenAlreadyInitialized" returntype="void" hint="Redéfinie pour n'effectuer aucune action">
		<!--- Cette implémentation n'effectue aucune action pour que l'on puisse appeler init() sur une instance déjà initialisée --->
	</cffunction>
</cfcomponent>