<!--- TODO : Supprimer cette implémentation et utiliser fr.saaswedo.api.myTEM.mdm.MDM --->
<cfcomponent author="Cedric" displayname="fr.saaswedo.api.myTEM.mdm.ServerManager"
extends="fr.saaswedo.api.patterns.cfc.Component" hint="Implémentation d'un gestionnaire de serveurs MDM">
	<cffunction access="public" name="getMdmServerInfos" returntype="Struct" hint="Retourne les infos du serveur MDM de la racine">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfset var dataSource=(isDefined("SESSION") AND structKeyExists(SESSION,"OFFREDSN")) ? SESSION.OFFREDSN:"ROCOFFRE">
		<!--- Récupération des infos MDM de la racine --->
		<cfset var procedureName="PKG_MDM.get_server_racine_v2">
		<cfstoredproc datasource="#datasource#" procedure="#procedureName#">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS.idGroupe#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="procedure_status">
			<cfprocresult name="qMdmServerInfos">
		</cfstoredproc>
		<cfif (qMdmServerInfos.recordcount GT 0) AND (procedure_status GT 0)>
			<cfset var code=GLOBALS().CONST("MDM.ERROR")>
			<!--- LOG --->
			<cfset var isDefault=qMdmServerInfos["USE_SSL"][1]>
			<cfset var log=GLOBALS().CONST('Remoting.LOG')>
			<cflog type="information" text="[#log#] #procedureName# (status:#procedure_status#,idGroupe:#ARGUMENTS.idGroupe#,isDefault:#isDefault#)">
			<!--- Structure des infos MDM --->
			<cfset var mdmServerInfos={
				USERID=VAL(qMdmServerInfos["USERID"][1]),ADMIN_EMAIL=qMdmServerInfos["ADMIN_EMAIL"][1],MDM=qMdmServerInfos["MDM"][1],
				USE_SSL=yesNoFormat(VAL(qMdmServerInfos["USE_SSL"][1])),USERNAME=qMdmServerInfos["USERNAME"][1],PASSWORD=qMdmServerInfos["PASSWORD"][1],
				ENROLL_USERNAME=qMdmServerInfos["ENROLL_USERNAME"][1],ENROLL_PASSWORD=qMdmServerInfos["ENROLL_PASSWORD"][1],
				IS_DEFAULT=yesNoFormat(VAL(qMdmServerInfos["IS_DEFAULT"][1])),IS_TEST_OK=yesNoFormat(VAL(qMdmServerInfos["IS_TEST_OK"][1])),
				TEST_MSG=qMdmServerInfos["TEST_MSG"][1],MDM_TYPE=qMdmServerInfos["MDM_TYPE"][1],TOKEN=qMdmServerInfos["TOKEN"][1]
			}>
			<!--- Vérification de la valeur de la propriété MDM --->
			<cfif (TRIM(mdmServerInfos.MDM) EQ "") OR (TRIM(mdmServerInfos.MDM) NEQ mdmServerInfos.MDM)>
				<cfset var extendedInfos=isDefined("qMdmServerInfos") ? serializeJSON(qMdmServerInfos):"">
				<cfthrow type="Custom" errorcode="#code#"
				message="#procedureName# contains invalid MDM value '#mdmServerInfos.MDM#' for idGroupe #ARGUMENTS.idGroupe#" detail="#extendedInfos#">
			<cfelse>
				<!--- Conversion des valeurs de type Booléens dans le type Java correspondant pour compatibilité avec ActionScript --->
				<cfset mdmServerInfos["IS_DEFAULT"]=createObject("java","java.lang.Boolean").init(mdmServerInfos["IS_DEFAULT"] ? "true":"false")>
				<cfset mdmServerInfos["USE_SSL"]=createObject("java","java.lang.Boolean").init(mdmServerInfos["USE_SSL"] ? "true":"false")>
				<cfset mdmServerInfos["IS_TEST_OK"]=createObject("java","java.lang.Boolean").init(mdmServerInfos["IS_TEST_OK"] ? "true":"false")>
				<cfreturn mdmServerInfos>
			</cfif>
		<cfelse>
			<cfset var extendedInfos=isDefined("qMdmServerInfos") ? serializeJSON(qMdmServerInfos):"">
			<cfthrow type="Custom" errorcode="#code#"
			message="#procedureName# status #procedure_status# (idGroupe:#ARGUMENTS.idGroupe#)" detail="#extendedInfos#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="resetMdmServerInfos" returntype="Struct"
	hint="Retourne et par défaut remplace les valeurs par défaut des infos MDM pour idGroupe">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfargument name="userId" type="Numeric" required="true" hint="Identifiant de l'utilisateur effectuant la réinitialisation">
		<cfargument name="updateValues" type="Boolean" required="false" default="true" hint="TRUE pour remplacer les valeurs existantes et FALSE sinon">
		<cfset var dataSource=(isDefined("SESSION") AND structKeyExists(SESSION,"OFFREDSN")) ? SESSION.OFFREDSN:"ROCOFFRE">
		<!--- Réinitialisation puis récupération des valeurs des infos MDM --->
		<cfif ARGUMENTS["updateValues"]>
			<cfset var procedureName="PKG_MDM.delete_server_racine">
			<cfstoredproc datasource="#datasource#" procedure="#procedureName#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS.idGroupe#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS.userId#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="procedure_status">
				<cfprocresult name="qResetMdmServerInfos">
			</cfstoredproc>
			<cfset procedure_status=VAL(qResetMdmServerInfos["STATUS"][1])>
			<!--- Vérification du statut de la procédure --->
			<cfif (qResetMdmServerInfos.recordcount GT 0) AND (VAL(qResetMdmServerInfos["STATUS"][1]) GT 0)>
				<!--- LOG --->
				<cfset var log=GLOBALS().CONST('Remoting.LOG')>
				<cflog type="information" text="[#log#] #procedureName# (status:#procedure_status#,idGroupe:#ARGUMENTS.idGroupe#)">
				<!--- Récupération des infos MDM de la racine (Après réinitialisation) --->
				<cfreturn getMdmServerInfos(ARGUMENTS.idGroupe)>
			<cfelse>
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<cfset var extendedInfos=isDefined("qResetMdmServerInfos") ? serializeJSON(qResetMdmServerInfos):"">
				<cfthrow type="Custom" errorcode="#code#"
					message="#procedureName# status #procedure_status# (idGroupe:#ARGUMENTS.idGroupe#)" detail="#extendedInfos#">
			</cfif>
		<!--- Récupération des valeurs par défaut --->
		<cfelse>
			<cfset var log=GLOBALS().CONST('Remoting.LOG')>
			<cflog type="information" text="[#log#] Reset MDM infos without persitence (idGroupe:#ARGUMENTS.idGroupe#)">
			<cfreturn getMdmServerInfos(0)>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="updateMdmServerInfos" returntype="void" hint="Met à jour les valeurs des infos MDM">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfargument name="userId" type="Numeric" required="true" hint="Identifiant de l'utilisateur effectuant la mise à jour">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Structure contenant les infos MDM avec leur valeurs">
		<cfset var dataSource=(isDefined("SESSION") AND structKeyExists(SESSION,"OFFREDSN")) ? SESSION.OFFREDSN:"ROCOFFRE">
		<cfset var isMdmInfosValid=TRUE>
		<cfset var mdmProps=ARGUMENTS["mdmInfos"]>
		<!--- Vérification des infos MDM --->
		<cfset var mdmPropertyList="ADMIN_EMAIL,MDM,USE_SSL,USERNAME,PASSWORD,ENROLL_USERNAME,ENROLL_PASSWORD,TOKEN">
		<cfset var mdmMissingPropertyList="">
		<cfloop list="#mdmPropertyList#" index="listItem" delimiters=",">
			<cfset isMdmInfosValid=(isMdmInfosValid AND structKeyExists(mdmProps,listItem) AND (TRIM(mdmProps[listItem]) NEQ ""))>
			<cfif NOT (structKeyExists(mdmProps,listItem) AND (TRIM(mdmProps[listItem]) NEQ ""))>
				<cfif CompareNoCase(listItem,'TOKEN') eq 0>
					<cfset StructInsert(mdmProps,'TOKEN','-')>
					<cfset mdmProps['TOKEN'] = "">
					<cfelse>
					<cfset mdmMissingPropertyList=listAppend(mdmMissingPropertyList,listItem,",")>
				</cfif>
			</cfif>
		</cfloop>
		<cfif isMdmInfosValid>
			<!--- Conversion des valeurs de type Boolean --->
			<cfset mdmProps["USE_SSL"]=(mdmProps["USE_SSL"] ? 1:0)>
			<cfset mdmProps["IS_TEST_OK"]=(mdmProps["IS_TEST_OK"] ? 1:0)>
			<!--- Mise à jour des infos MDM de la racine --->
			<cfset var procedureName="PKG_MDM.set_server_racine">
			<!--- Les mots de passes sont préfixés par 4 caractères * à la suite --->
			<cfset var password=mdmProps.PASSWORD>
			<cfset var enrollPassword=mdmProps.ENROLL_PASSWORD>
			<cfset mdmProps.PASSWORD=repeatString("*",4) & mdmProps.PASSWORD>
			<cfset mdmProps.ENROLL_PASSWORD=repeatString("*",4) & mdmProps.ENROLL_PASSWORD>
			<cfstoredproc datasource="#datasource#" procedure="#procedureName#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS.idGroupe#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS.userId#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_CLOB" value="#serializeJSON(mdmProps)#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="procedure_status">
				<cfprocresult name="qUpdateMdmServerInfos">
			</cfstoredproc>
			<cfset mdmProps.PASSWORD=password>
			<cfset mdmProps.ENROLL_PASSWORD=enrollPassword>
			<cfset procedure_status=VAL(qUpdateMdmServerInfos["STATUS"][1])>
			<!--- Vérification du statut de la procédure --->
			<cfif (qUpdateMdmServerInfos.recordcount GT 0) AND (VAL(qUpdateMdmServerInfos["STATUS"][1]) GT 0)>
				<cfset var log=GLOBALS().CONST('Remoting.LOG')>
				<cflog type="information" text="[#log#] #procedureName# (status:#procedure_status#,idGroupe:#ARGUMENTS.idGroupe#)">
			<cfelse>
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<cfset var extendedInfos=isDefined("qUpdateMdmServerInfos") ? serializeJSON(qUpdateMdmServerInfos):"">
				<cfthrow type="Custom" errorcode="#code#"
					message="#procedureName# status #procedure_status# (idGroupe:#ARGUMENTS.idGroupe#)" detail="#extendedInfos#">
			</cfif>
		<cfelse>
			<cfset var code=GLOBALS().CONST("MDM.ERROR")>
			<cfthrow type="Custom" errorcode="#code#" message="MISSING OR EMPTY MDM PROPERTIES" detail="#mdmMissingPropertyList#">
		</cfif>
	</cffunction>
</cfcomponent>