<!--- Définition d'origine de ce composant
<cfcomponent author="Cedric" displayname="fr.saaswedo.api.myTEM.mdm.MyTEM" implements="fr.saaswedo.api.mdm.core.services.IMDM"
extends="fr.saaswedo.api.myTEM.mdm.ErrorHandler"
--->
<cfcomponent author="Cedric" displayname="fr.saaswedo.api.myTEM.mdm.MyTEM" extends="fr.saaswedo.api.myTEM.mdm.ErrorHandler"
hint="Implémentation IMDM à utiliser par défaut dans MyTEM et qui délègue les méthodes correspondantes à une implémentation IMDM spécifique<br>
Les méthodes de ce composant doivent etre appelées sur une instance retournée par getInstance() sinon des exceptions seront levées<br>
Les propriétés serveur MDM dépendent de la valeur retournée par getIdGroupe() et elles sont récupérées et mises à jour à chaque appel à getInstance()<br>
Le traitement de certaines exceptions capturées par l'API MDM choisie est délégué à une implémentation définie dans le composant parent<br> 
<br><b>Best Practice :</b> L'instance de ce composant peut etre placée dans le scope Session pour optimiser son instanciation et son utilisation">
	<!--- Pas d'interface pour ces méthodes pour le moment --->
	<cffunction access="public" name="getServerManager" returntype="fr.saaswedo.api.myTEM.mdm.ServerManager" hint="Retourne le gestionnaire de serveurs MDM">
		<cfif NOT structKeyExists(VARIABLES,getServerManagerKey())>
			<cfset VARIABLES[getServerManagerKey()]=createObject("component","fr.saaswedo.api.myTEM.mdm.ServerManager")>
		</cfif>
		<cfreturn VARIABLES[getServerManagerKey()]>
	</cffunction>
	
	<cffunction access="public" name="checkServerConnection" returntype="Struct" hint="Effectue un test de connexion avec le serveur MDM<br/>
	Retourne une structure contenant les clés : IS_TEST_OK (TRUE si la connexion a été effectuée avec succès et FALSE sinon),
	TEST_MSG : Message associé au test de connexion">
		<cfargument name="mdm" type="String" required="true" hint="DNS ou adresse IP du serveur MDM">
		<cfargument name="useSSL" type="Boolean" required="true" hint="TRUE pour utiliser le protocole HTTPS et FALSE sinon">
		<cfargument name="wsUser" type="String" required="true" hint="Login de connexion au WebService MDM">
		<cfargument name="wsPassword" type="String" required="true" hint="Mot de passe de connexion au WebService MDM">
		<cfargument name="mdmType" type="String" required="true" hint="Type du serveur MDM : ZENPRISE">
		<cfif TRIM(ARGUMENTS["mdmType"]) EQ "ZENPRISE">
			<cfreturn getMdmInstance().checkServerConnection(ARGUMENTS["mdm"],ARGUMENTS["useSSL"],ARGUMENTS["wsUser"],ARGUMENTS["wsPassword"])>
		<cfelse>
			<cfthrow type="Custom" errorcode="API_MDM" message="UNSUPPORTED MDM TYPE #ARGUMENTS.mdmType#">
		</cfif>
	</cffunction>
	
	<!--- fr.saaswedo.api.mdm.core.services.IMDM --->
	<cffunction access="public" name="getIDeviceManagement" returntype="fr.saaswedo.api.mdm.core.services.IDeviceManagement">
		<cfreturn getMdmInstance().getIDeviceManagement()>
	</cffunction>

	<cffunction access="public" name="getIDeviceInfo" returntype="fr.saaswedo.api.mdm.core.services.IDeviceInfo">
		<cfreturn getMdmInstance().getIDeviceInfo()>
	</cffunction>
	
	<cffunction access="public" name="getMdm" returntype="String">
		<cfreturn getMdmInstance().getMdm()>
	</cffunction>
	
	<cffunction access="public" name="getUsername" returntype="String">
		<cfreturn getMdmInstance().getUsername()>
	</cffunction>
	
	<cffunction access="public" name="useSSL" returntype="Boolean">
		<cfreturn getMdmInstance().useSSL()>
	</cffunction>
	
	<cffunction access="public" name="getAdminEmail" returntype="String">
		<cfreturn getMdmInstance().getAdminEmail()>
	</cffunction>
	
	<!--- fr.saaswedo.api.myTEM.mdm.MyTEM --->
	<cffunction access="public" name="getInstance" returntype="fr.saaswedo.api.myTEM.mdm.MyTEM"
	hint="Les méthodes de ce composant doivent etre appelée sur une instance retournée par cette méthode sinon des exceptions seront levées<br>
	Cette méthode effectue dans l'ordre :<br>Récupération des propriétés serveur MDM<br>
	Au 1er appel : Instanciation de l'implémentation IMDM à laquelle sont déléguées les méthodes correspondantes<br>
	A chaque appel : Mise à jour des propriétés serveur MDM fournies et utilisées à l'implémentation IMDM">
		<cfargument name="idRacine" type="Numeric" required="false" default="-1" hint="Identifiant racine à utiliser si la session n'est pas définie.<br>
		Une valeur par défaut est utilisée si la session n'est pas définie et si ce paramètre n'est pas renseigné ou n'est pas positif'">
		<!--- Initialisation de la valeur de l'identifiant du groupe utilisé --->
		<cfif ARGUMENTS["idRacine"] GT 0>
			<cfset VARIABLES[getIdGroupeKey()]=ARGUMENTS["idRacine"]>
		</cfif>
		<!--- Récupération des propriétés serveur MDM : Ces propriétés sont retournés par getMdmProperties()
		<cfset var mdmProps=requestMdmProperties(getIdGroupe())>
		<cfset VARIABLES[getMdmPropertiesKey()]=mdmProps>
		--->
		<!--- Instanciation de l'implémentation IMDM retournée par getMdmInstance() : Modifier cette partie pour changer d'implémentation IMDM
		<cfif NOT structKeyExists(VARIABLES,getMdmInstanceKey())>
			<cftry>
				<cfset VARIABLES[getMdmInstanceKey()]=createObject("component","fr.saaswedo.api.mdm.impl.zenprise.ZenpriseMdm")>
				<cfcatch type="Any">
					<cflog type="error" text="[MyTEM] IMDM instaciation failed : #CFCATCH.message# [#CFCATCH.detail#]">
					<cfrethrow>
				</cfcatch>
			</cftry>
			<cflog type="information" text="[MyTEM] IMDM instance created">
		</cfif>
		--->
		<!--- Mise à jour des propriétés serveur MDM de l'instance IMDM : Modifier cette partie en fonction de l'implémentation IMDM utilisée
		<cfset VARIABLES[getMdmInstanceKey()]=getMdmInstance().getInstance(
			structKeyExists(mdmProps,"MDM") ? mdmProps["MDM"]:"",
			structKeyExists(mdmProps,"USERNAME") ? mdmProps["USERNAME"]:"",
			structKeyExists(mdmProps,"PASSWORD") ? mdmProps["PASSWORD"]:"",
			structKeyExists(mdmProps,"USE_SSL") ? yesNoFormat(mdmProps["USE_SSL"]):TRUE,
			structKeyExists(mdmProps,"ADMIN_EMAIL") ? mdmProps["ADMIN_EMAIL"]:"INVALID_MDM_ADMIN_EMAIL"
		)>
		--->
		<!--- Implémentation de traitement des exceptions MDM : Spécifique à l'implémentation IMDM de Zenprise : Modifier si l'implémentation MDM choisie change
		<cfset VARIABLES[getMdmInstanceKey()].setErrorHandler(THIS)>
		--->
		<cflog type="information" text="[MyTEM] IMDM instance updated">
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="getDevicePropertyMap" returntype="Struct"
	hint="Retourne une structure correspondant à une table de correspondance pour les propriétés d'un Device<br>
	La structure retournée contient autant de clés que de propriétés du Device. Chaque clé est associée à une structure contenant les clés suivantes :
	<br>LABEL : Libellé de la propriété<br>CATEGORY : Libellé de la catégorie de la propriété<br>
	La table de correspondance qui est retournée est stockée dans le scope SESSION sous la clé USER.MDM.PROPERTY_MAP.MAP<br>
	La valeur associée à la clé SESSION.USER.MDM.PROPERTY_MAP.IDLANG est celle associée à la clé SESSION.USER.IDGLOBALIZATION au moment de l'appel à cette méthode<br>
	Le contenu de la table est créée et placée dans le scope SESSION lors du 1er appel à cette méthode<br>
	Il est mis à jour lorsque la valeur associée à SESSION.USER.IDGLOBALIZATION est différente de celle associée à de SESSION.USER.MDM.PROPERTY_MAP.IDLANG au moment de l'appel à cette méthode">
		<cfset var propertyKey="PROPERTY_MAP">
		<cfset var idLangKey="IDLANG">
		<cfset var mapKey="MAP">
		<cfset var dataSource=(isDefined("SESSION") AND structKeyExists(SESSION,"OFFREDSN")) ? SESSION.OFFREDSN:"ROCOFFRE">
		<cfset var idGroupe=(isDefined("SESSION") AND structKeyExists(SESSION,"PERIMETRE") AND structKeyExists(SESSION["PERIMETRE"],"ID_GROUPE")) ? SESSION.PERIMETRE.ID_GROUPE:getDefaultIdGroupe()>
		<!--- Si le scope SESSION est défini : La table est placée dans le scope SESSION --->
		<cfif isDefined("SESSION") AND structKeyExists(SESSION,"USER")>
			<!--- La table est créée et placée dans le scope SESSION si elle n'existe pas ou si la langue a changée (i.e A la connexion utilisateur) --->
			<cfif (
				(NOT structKeyExists(SESSION.USER,propertyKey)) OR
				(structKeyExists(SESSION.USER,propertyKey) AND VAL(SESSION["USER"][propertyKey][idLangKey]) NEQ VAL(SESSION.USER.IDGLOBALIZATION))
			)>
				<cfset var propertyMap=structKeyExists(SESSION.USER,propertyKey) ? SESSION["USER"][propertyKey]:{}>
				<cfset propertyMap[idLangKey]=VAL(SESSION.USER.IDGLOBALIZATION)>
				<!--- Chargement des données de la table de correspondance --->
				<cfstoredproc datasource="#datasource#" procedure="PKG_MDM.get_list_properties">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#propertyMap[idLangKey]#">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="dataset">
				</cfstoredproc>
				<!--- Contruction de la table de correspondance --->
				<cfset propertyMap[mapKey]={}>
				<cfloop index="i" from="1" to="#dataset.recordcount#">
					<cfset var property=dataset["PROPERTY"][i]>
					<cfif TRIM(dataset["LABEL"][i]) NEQ "">
						<cfset propertyMap[mapKey][property]={LABEL=dataset["LABEL"][i],CATEGORY=dataset["CATEGORY"][i]}>
					</cfif>
				</cfloop>
				<cflog type="information" text="[MyTEM] Property Map built - Session is defined (IDLANG : #propertyMap[idLangKey]#)">
				<cflock scope="SESSION" type="exclusive" timeout="5" throwontimeout="true">
					<cfset SESSION["USER"][propertyKey]=propertyMap>
					<cfreturn SESSION["USER"][propertyKey][mapKey]>
				</cflock>
			<cfelse>
				<cflock scope="SESSION" type="readonly" timeout="5" throwontimeout="true">
					<cfset var idLang=SESSION["USER"][propertyKey][idLangKey]>
					<cflog type="information" text="[MyTEM] Property Map retrieved from Session scope (IDLANG : #idLang#)">
					<cfreturn SESSION["USER"][propertyKey][mapKey]>
				</cflock>
			</cfif>
		<!--- Si le scope SESSION n'est pas défini alors la table est recréée à chaque appel à cette méthode --->
		<cfelse>
			<cfif NOT structKeyExists(VARIABLES,propertyKey)>
				<cfset var propertyMap={}>
				<cfset propertyMap[idLangKey]=3><!--- Langue par défaut quand le scope SESSION n'est pas défini --->
				<!--- Chargement des données de la table de correspondance --->
				<cfstoredproc datasource="#datasource#" procedure="PKG_MDM.get_list_properties">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#propertyMap[idLangKey]#">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="dataset">
				</cfstoredproc>
				<!--- Contruction de la table de correspondance --->
				<cfset propertyMap[mapKey]={}>
				<cfloop index="i" from="1" to="#dataset.recordcount#">
					<cfset var property=dataset["PROPERTY"][i]>
					<cfif TRIM(dataset["LABEL"][i]) NEQ "">
						<cfset propertyMap[mapKey][property]={LABEL=dataset["LABEL"][i],CATEGORY=dataset["CATEGORY"][i]}>
					</cfif>
				</cfloop>
				<cflog type="warning" text="[MyTEM] Property Map built - Session is not defined (IDLANG : #propertyMap[idLangKey]#)">
				<cfset VARIABLES[propertyKey]=propertyMap>
			</cfif>
			<cfreturn VARIABLES[propertyKey][mapKey]>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getIdGroupe" returntype="Numeric"
	hint="Retourne l'identifiant du groupe sur lequel l'utilisateur est connecté si le scope Session est défini. Sinon retourne une valeur utilisée par défaut">
		<!--- Si le scope Session est défini alors l'identifiant du groupe est celui sur lequel l'utilisateur est actuellement connecté --->
		<cfif isDefined("SESSION") AND structKeyExists(SESSION,"PERIMETRE") AND structKeyExists(SESSION["PERIMETRE"],"ID_GROUPE")>
			<cflock type="readOnly" scope="Session" timeout="60" throwontimeout="true">
				<!--- Identifiant du groupe sur lequel l'utilisateur est actuellement connecté --->
				<cfreturn VAL(SESSION["PERIMETRE"]["ID_GROUPE"])>
			</cflock>
		<cfelse>
			<cfif NOT structKeyExists(VARIABLES,getIdGroupeKey())>
				<!--- Valeur par défaut utilisée comme l'identifiant du groupe --->
				<cfset VARIABLES[getIdGroupeKey()]=getDefaultIdGroupe()>
			</cfif>
			<cfreturn VARIABLES[getIdGroupeKey()]>
		</cfif>
	</cffunction>

	<cffunction access="private" name="getDefaultIdGroupe" returntype="Numeric"
	hint="Méthode utilisée pour retourner la valeur par défaut de l'identifiant de la racine lorsque le scope SESSION n'est pas défini">
		<!--- Valeur par défaut utilisée comme l'identifiant du groupe (Test sur __DEMO-CONSOTEL : 2458788) --->
		<cfreturn 2458788>
	</cffunction>

	<cffunction access="private" name="getMdmPropertiesKey" returntype="String"
	hint="Retourne la clé utilisée dans ce composant pour référencer les propriétés serveur MDM du groupe identifié par getIdGroupe()">
		<cfreturn "MDM_PROPERTIES">
	</cffunction>
	
	<cffunction access="private" name="getMdmInstanceKey" returntype="String"
	hint="Retourne la clé utilisée dans ce composant pour référencer l'instance IMDM créée en utilisant l'implémentation retournée par getMdmInstance()">
		<cfreturn "IMDM_INSTANCE">
	</cffunction>
	
	<cffunction access="private" name="getIdGroupeKey" returntype="String" hint="Retourne la clé utilisée dans ce composant pour référencer l'identifiant de la racine utilisée">
		<cfreturn "IDGROUPE">
	</cffunction>
	
	<cffunction access="private" name="getServerManagerKey" returntype="String" hint="Retourne la clé utilisée dans ce composant pour référencer le gestionnaire de serveur MDM">
		<cfreturn "SERVER_MANAGER">
	</cffunction>

	<cffunction access="private" name="getMdmInstance" returntype="fr.saaswedo.api.mdm.core.services.IMDM"
	hint="Retourne l'instance de l'implémentation IMDM à laquelle ce composant délégue les appels aux méthodes correspondantes">
		<cfif NOT structKeyExists(VARIABLES,getMdmInstanceKey())>
			<cflog type="error" text="[MyTEM] Key #getMdmInstanceKey()# is not defined">
			<cfthrow type="Custom" errorcode="API_MDM" message="La clé qui référence l'instance IMDM n'est pas définie">
		<cfelse>
			<cfreturn VARIABLES[getMdmInstanceKey()]>
		</cfif>
	</cffunction>

	<cffunction access="private" name="getMdmProperties" returntype="Query"
	hint="Retourne les propriétés concernant le serveur Zenprise pour la racine idRacine<br>Les colonnes de la requête retournée sont :<br>
	IDGROUPE : Identifiant de la racine<br>MDM : Adresse du serveur MDM (DNS ou IP)<br>USE_SSL : 0 pour utiliser HTTP (Par défaut : 1 pour HTTPS)
	<br>USERNAME : Login utilisateur MDM<br>PASSWORD : Mot de passe utilisateur MDM<br>ADMIN_EMAIL : Email admin MDM">
		<cfif NOT structKeyExists(VARIABLES,getMdmPropertiesKey())>
			<cflog type="error" text="[MyTEM] Key #getMdmPropertiesKey()# is not defined">
			<cfthrow type="Custom" errorcode="API_MDM" message="La clé qui référence les propriétés du serveur MDM n'est pas définie">
		<cfelse>
			<cfreturn VARIABLES[getMdmPropertiesKey()]>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="requestMdmProperties" returntype="Struct"
	hint="Récupère et retourne les propriétés serveur MDM qui sont liées à l'identifiant du groupe retourné par getIdGroupe()<br>
	Chaque appel à cette méthode récupère à nouveau les données précédemment décrites. La structure retournée contient les clés suivantes :<br>
	MDM : Adresse du serveur MDM (DNS ou IP)<br>USERNAME : Login utilisateur MDM<br>PASSWORD : Mot de passe utilisateur MDM<br>
	ADMIN_EMAIL : Email admin MDM<br>USE_SSL : TRUE pour utiliser HTTP (Par défaut : FALSE pour HTTPS)">
		<cfargument name="idRacine" type="numeric" default="0"
		hint="Identifiant du groupe pour lequel les propriétés MDM sont retournées. La valeur 0 pour les valeurs par défaut">
		<cftry>
			<cfset var idGroupe=ARGUMENTS["idRacine"]>
			<cfreturn getServerManager().getMdmServerInfos(idGroupe)>
			<cfcatch type="Any">
				<cflog type="error" text="[MyTEM] Request MDM Properties FAILED : #CFCATCH.message# [#CFCATCH.detail#]">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>