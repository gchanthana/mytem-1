<!--- Définition d'origine de ce composant
<cfcomponent author="Cedric" displayname="fr.saaswedo.api.myTEM.mdm.ErrorHandler" implements="fr.saaswedo.api.remoting.soap.IErrorHandler"
--->
<cfcomponent author="Cedric" displayname="fr.saaswedo.api.myTEM.mdm.ErrorHandler"
hint="Implémentation d'un Handler d'erreur auquel sont délégués le traitement de certaines exceptions MDM. La méthode privée getMdmInstance() doit etre implémentée par les sous classes">
	<cffunction access="public" name="handleSoapClientErrors" returntype="void" hint="Envoi un mail l'adresse getAdminEmail() contenant le détails de l'erreur">
		<cfargument name="cfCatchObject" type="Any" required="true" hint="Exception capturée de type : CFCATCH (ColdFusion)">
		<cfargument name="operation" type="String" required="false" default="" hint="Nom de la méthode ou de l'opération si elle est définie. Par défaut : Chaine vide">
		<cfargument name="parameters" type="Struct" required="false" hint="Paramètres fournis à la méthode ou à l'opération s'ils sont définis. Par défaut : Non définis">
		<cfargument name="errorEventType" type="String" required="false" default="" hint="Code d'erreur correspondant à l'exception qui est notifiée">
		<cfargument name="targetProperties" type="Struct" required="false" hint="Structure contenant des propriétés concernant l'instance du WebService">
		<cfset var wsMethodParameters={}>
		<cfif structKeyExists(ARGUMENTS,"parameters")>
			<cfset wsMethodParameters=ARGUMENTS["parameters"]>
		</cfif>
		<cfset sendMdmAdminError(
			getMdmInstance(),ARGUMENTS["cfCatchObject"],ARGUMENTS["operation"],wsMethodParameters,ARGUMENTS["errorEventType"],ARGUMENTS["targetProperties"]
		)>
	</cffunction>
	
	<cffunction access="public" name="CONNECTION_TEST_ERROR" returntype="String"
	hint="Doit renvoyer une chaine non vide correspondant au code d'erreur rencontré lors d'un test de connexion avec le serveur MDM">
		<cfreturn "CONNECTION_TEST_ERROR">
	</cffunction>
	
	<cffunction access="public" name="BACKOFFICE_ERROR" returntype="String"
	hint="Doit renvoyer une chaine non vide correspondant à un code d'erreur qui ne doit etre notifié qu'en interne uniquement">
		<cfreturn "BACKOFFICE_ERROR">
	</cffunction>
	
	<cffunction access="private" name="sendMdmAdminError" returntype="void" hint="Envoi un mail l'adresse getAdminEmail() contenant le détails de l'erreur">
		<cfargument name="mdm" type="fr.saaswedo.api.mdm.core.services.IMDM" required="true" hint="Implémentation IMDM permettant de récupérer les infos serveur MDM">
		<cfargument name="cfCatchObject" type="Any" required="true" hint="Exception capturée de type : CFCATCH (ColdFusion)">
		<cfargument name="operation" type="String" required="false" default="" hint="Nom de la méthode ou de l'opération si elle est définie. Par défaut : Chaine vide">
		<cfargument name="parameters" type="Struct" required="false" hint="Paramètres fournis à la méthode ou à l'opération s'ils sont définis. Par défaut : Non définis">
		<cfargument name="errorEventType" type="String" required="false" default="" hint="Code d'erreur correspondant à l'exception qui est notifiée">
		<cfargument name="targetProperties" type="Struct" required="false" hint="Structure contenant des propriétés concernant l'instance du WebService">
		<cfset var expeditor="Notifications Saaswedo<monitoring@saaswedo.com>">
		<cfset var iMDM=ARGUMENTS["mdm"]>
		<cfset var mdmMailTo=TRIM(iMDM.getAdminEmail()) EQ "" ? "monitoring@saaswedo.com":TRIM(iMDM.getAdminEmail())>
		<cftry>
			<cfsavecontent variable="mailContent">
				<cfoutput>
					Cet email a été envoyé automatiquement suite au message d'erreur suivant rencontré sur votre instance MDM le
					#lsDateFormat(NOW(),"dd/MM/YYYY")# à #lsTimeformat(NOW(),"HH:mm")#.<br/>
					Votre support technique Saaswedo procède actuellement à la résolution de cet incident. Pour plus d'informations vous êtes invité à le contacter.<br/>
					<cfif TRIM(ARGUMENTS["errorEventType"]) EQ CONNECTION_TEST_ERROR()>
						L'erreur a été rencontrée durant <b>un test de connexion</b> effectué par l'utilisateur depuis l'interface de notre application
						<cfif isDefined("ARGUMENTS.targetProperties")>
							<cfset var wsdlLocation="N.D">
							<cfset var wsdlKey="WSDL">
							<cfif structKeyExists(ARGUMENTS["targetProperties"],wsdlKey)>
								<cfset wsdlLocation=ARGUMENTS["targetProperties"][wsdlKey]>
								<br/><br/>URL WSDL : #wsdlLocation#<br/>
							</cfif>
						</cfif>
					<cfelse>
						Instance MDM :  #iMDM.getMdm()#<br/>
						Protocole : #useSSL() ? "HTTPS":"HTTP"#<br/>
						Login MDM : #iMDM.getUsername()#<br/>
					</cfif>
					<cfif TRIM(ARGUMENTS["operation"]) NEQ "">
						Opération : #ARGUMENTS["operation"]#<br>
					</cfif>
					<cfif structKeyExists(ARGUMENTS,"parameters") AND (NOT structIsEmpty(ARGUMENTS["parameters"]))>
						Paramètres :<br/>
						<cfloop collection="#ARGUMENTS.parameters#" item="param">
							#param# : #(structKeyExists(ARGUMENTS["parameters"][param],"VALUE") ? ARGUMENTS["parameters"][param]["VALUE"]:"")#<br/>
						</cfloop>
					</cfif>
					Message d'erreur : #ARGUMENTS.cfCatchObject.message#<br/>
					Detail de l'erreur : #ARGUMENTS.cfCatchObject.detail#<br/>
				</cfoutput>
			</cfsavecontent>
			<!--- Notification administrateur MDM --->
			<cfif TRIM(ARGUMENTS.errorEventType) NEQ BACKOFFICE_ERROR()>
				<cfmail type="html" from="#expeditor#" to="#mdmMailTo#" subject="[MDM] Notification d'erreur MDM">
					<cfoutput>#mailContent#</cfoutput>
				</cfmail>
			</cfif>
			<!--- Notification interne MDM --->
			<cfmail type="html" from="MDM ErrorHandler<monitoring@saaswedo.com>" to="monitoring@saaswedo.com" subject="[MDM] Notification interne d'erreur MDM">
				<cfoutput>
					<cfif TRIM(ARGUMENTS.errorEventType) EQ BACKOFFICE_ERROR()>
						<b>Toute notification n'a été envoyée qu'en interne uniquement en raison du code de l'erreur</b><hr/>
					</cfif>
					#mailContent#
					<br/> Error Event Type : #ARGUMENTS.errorEventType#
					#getWebAppInfos()#
				</cfoutput>
				<cfif structKeyExists(ARGUMENTS.cfCatchObject,"tagContext")>
					<hr/>Contexte d'exécution:<br/><b><cfdump var="#ARGUMENTS.cfCatchObject.tagContext#" format="text"></b>
				</cfif>
			</cfmail>
			<cflog type="error" text="[ErrorHandler] MDM Admin notification - Exception Message : #ARGUMENTS.cfCatchObject.message#">
			<cflog type="error" text="[ErrorHandler] MDM Admin notification - Exception Detail : #ARGUMENTS.cfCatchObject.detail#">
			<cflog type="error" text="[ErrorHandler] Soap Client Error Email notification sent to #iMDM.getAdminEmail()#">
			<cfcatch type="Any">
				<cflog type="error" text="[ErrorHandler] Unable to send Soap Client Error : #CFCATCH.message# [#CFCATCH.detail#]">
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="getWebAppInfos" returntype="Any" hint="Retourne un contenu avec les infos liées à l'application web">
		<cfsavecontent variable="webAppInfos">
			<cfoutput>
				<hr/>Serveur : #CGI.SERVER_NAME#:#CGI.SERVER_PORT#<br/>
				Accès : #CGI.SCRIPT_NAME# (Host:#CGI.HTTP_HOST# - Method: #CGI.REQUEST_METHOD#)<br/>
				Client : #CGI.REMOTE_HOST#:#CGI.HTTP_USER_AGENT#<br/>[#CGI.HTTP_COOKIE#]<br/>
				Error Event Type : #ARGUMENTS["errorEventType"]#
				<cfif isDefined("APPLICATION")>
					<hr/>Application : #(structKeyExists(APPLICATION,"applicationName") ? APPLICATION.applicationName:"")#
					<cfif isDefined("SESSION")>
						<cfset var sessionModel=createObject("component","fr.consotel.consoview.M00.SessionModel")>
						<br/>Groupe : #sessionModel.getGroupe()#<br/>
						Utilisateur :<br/><b><cfdump var="#sessionModel.getUser()#" format="text"></b>
					</cfif>
				</cfif>
			</cfoutput>
		</cfsavecontent>
		<cfreturn webAppInfos>
	</cffunction>
	
	<cffunction access="private" name="getMdmInstance" returntype="fr.saaswedo.api.mdm.core.services.IMDM"
	hint="Retourne l'implémentation IMDM permettant de récupérer les infos serveur MDM. Cette méthode doit etre implémentée par les sous classes">
		<cfthrow type="Custom" errorcode="API_MDM" message="Cette méthode n'est pas implémentée">
	</cffunction>
</cfcomponent>