<cfcomponent name="Application">
	<cfset THIS.name = "RMOTE_API_ACCES">
	<cfset THIS.applicationtimeout="#createtimespan(2,0,0,0)#">
	<cfset THIS.sessionmanagement = TRUE>
	<cfset THIS.setclientcookies = TRUE>
	<cfset THIS.sessionTimeout="#createTimeSpan(0,2,0,0)#">
	 		
	<cffunction name="onApplicationStart">
	</cffunction>

	<cffunction name="onApplicationEnd">
	   	<cflog type="Information" text="APPLICATION #ARGUMENTS.ApplicationScope.applicationName# ends">
	</cffunction>
	
	<cffunction name="onSessionStart">
	   	<cflog type="Information" text="SESSION/#CGI.REMOTE_ADDR# #SESSION.SESSIONID# starts on #THIS.name#">
	</cffunction>

	<cffunction name="onRequestStart" access="remote" returntype="boolean">
	   	<cflog type="Information" text="REQUEST/#CGI.REMOTE_HOST# starts on #THIS.name#">	   	 
	   	<cfreturn TRUE>
	</cffunction>

	<cffunction name="onRequestEnd">
		<cfargument type="String" name="targetTemplate"required="true"/>
	   	<cflog type="Information" text="REQUEST/#CGI.REMOTE_HOST# ends on #THIS.name#">
	</cffunction>
	
	<cffunction name="onSessionEnd">
		<cfargument name="SessionScope" required="true"/>
	   	<cflog type="Information" text="SESSION #arguments.SessionScope.SESSIONID# ends on #THIS.name#">
	</cffunction>
   
</cfcomponent>
