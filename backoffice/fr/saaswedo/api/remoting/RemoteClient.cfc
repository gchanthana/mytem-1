<!--- Structures utilisées pour implémenter le fonctionnement
Structure VARIABLES.ENDPOINTS : Permet d'accéder aux "endpoint" par clé. Chaque endpoint permet d'accéder aux "username" par clé.
Chaque username permet d'accéder aux infos obtenus par register() i.e ID du service {
	<endpoint>:{
		<username>:{
			ID:Identifiant du service
		}
	}
}
Structure VARIABLES.SERVICES : Accès à l'instance de chaque service par ID {
	<serviceId>:Service créé avec createService()
}
--->
<cfcomponent author="Cedric" displayname="fr.saaswedo.api.remoting.RemoteClient" extends="fr.saaswedo.api.patterns.error.ErrorTarget"
implements="fr.saaswedo.api.remoting.IRemoteClient" hint="Composant qui implémente la fonction register(). Les fonctions publiques
à implémenter : invoke(),getServiceCall(),LOCKID(). Les fonctions privées à implémenter : createService(),setServiceCrendentials()">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.remoting.RemoteClient">
		<cfreturn "fr.saaswedo.api.remoting.RemoteClient">
	</cffunction>

	<cffunction access="public" name="getServiceCall" returntype="Any" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfargument name="serviceId" type="String" required="true">
		<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.getServiceCall() is not implemented">
	</cffunction>

	<cffunction access="public" name="invoke" returntype="Any" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfargument name="serviceCall" type="Any" required="true">
		<cfargument name="method" type="String" required="true">
		<cfargument name="parameters" type="Array" required="true">
		<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.invoke() is not implemented">
	</cffunction>

	<cffunction access="public" name="register" returntype="String" description="LOCK:Exclusive" hint="Toute exception est dispatchée">
		<cfargument name="endpoint" type="String" required="true">
		<cfargument name="username" type="String" required="false" default="">
		<cfargument name="password" type="String" required="false" default="">
		<cfset var endpointValue=ARGUMENTS.endpoint>
		<cfset var userValue=ARGUMENTS.username>
		<cftry>
			<!--- Clé utilisée pour username --->
			<cfset var userKey=userValue NEQ "" ? userValue:getEmptyUsernameValue()>
			<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
				<cfset var serviceId="">
				<cfset var service="">
				<cfset var endpoints=getEndpoints()>
				<!--- Création et enregistrement du endpoint --->
				<cfif NOT endpointExists(endpointValue)>
					<cfset endpoints[endpointValue]={}>
				</cfif>
				<!--- Création et enregistrement du service : endpoint,username --->
				<cfif NOT usernameExists(endpointValue,userValue)>
					<cfset var services=getServices()>
					<cfset service=createService(ARGUMENTS)>
					<cfset serviceId=createUUID()>
					<cfset services[serviceId]=service>
					<cfset endpoints[endpointValue][userKey]={ID=serviceId}>
					<!--- Log de l'enregistrement du service et retourne son identifiant --->
					<cfset var log=GLOBALS().CONST('Remoting.LOG')>
					<cflog type="information" text="[#log#][ID:#serviceId#] #endpointValue# (userKey:#userKey#) created and registered">
				</cfif>
				<!--- Récupération de l'ID du service pour mettre à jour les credentiels : Dépend de l'implémentation de setServiceCrendentials() --->
				<cfset serviceId=endpoints[endpointValue][userKey]["ID"]>
				<cfset setServiceCrendentials(serviceId,userValue,ARGUMENTS.password)>
				<cfreturn serviceId>
			</cflock>
			<cfcatch type="Any">
				<cfset dispatchError(CFCATCH)>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getService" returntype="Any" description="LOCK:ReadOnly" hint="Retourne le service identifié par serviceId.
	L'instance retournée sert à encapsuler les infos concernant un service et ne doit pas etre utilisée pour effectuer les appels au service">
		<cfargument name="serviceId" type="String" required="true" hint="Identifiant d'un service obtenu avec register()">
		<cfset var id=ARGUMENTS.serviceId>
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfset var services=getServices()>
			<cfif structKeyExists(services,id)>
				<cfreturn services[id]>
			<cfelse>
				<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="Service ID not found" detail="Service ID: '#id#'">
			</cfif>
		</cflock>
	</cffunction>
	
	<!--- =========== Fin des fonction publiques =========== --->
	
	<cffunction access="private" name="initInstance" returntype="void" description="LOCK:Exclusive"
	hint="Appele la fonction parent puis crée les structures utilisées par cette implémentation">
		<!--- TODO: Cette librairie est obsolète --->
		<cfthrow type="Custom" message="Obsolete implementation" detail="This implementation will be removed and should not be used">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset SUPER.initInstance()>
			<!--- Gestion des valeurs "endpoint" --->
			<cfif NOT structKeyExists(VARIABLES,"ENDPOINTS")>
				<cfset VARIABLES.ENDPOINTS={}>
			</cfif>
			<!--- Gestion des services --->
			<cfif NOT structKeyExists(VARIABLES,"SERVICES")>
				<cfset VARIABLES.SERVICES={}>
			</cfif>
		</cflock>
	</cffunction>
	
	<!--- =========== Fonctions de gestion du service =========== --->
	
	<cffunction access="private" name="getServices" returntype="Struct"
	description="LOCK:ReadOnly" hint="Retourne la structure contenant les services créés par register()">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfreturn VARIABLES.SERVICES>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="createService" returntype="Any"
	hint="Crée et retourne l'implémentation du service. Cette fonction n'est pas implémentée et lève une exception">
		<cfargument name="parameters" type="Struct" required="true" hint="Paramètres pour créer le service. Proviennent de register()">
		<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.createService() is not implemented">
	</cffunction>
	
	<cffunction access="private" name="setServiceCrendentials" returntype="void" hint="Définit les credentiels du service">
		<cfargument name="serviceId" type="Any" required="true" hint="Identifiant du service obtenu avec register()">
		<cfargument name="username" type="String" required="false" default="" hint="Login">
		<cfargument name="password" type="String" required="false" default="" hint="Password">
		<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="setServiceCrendentials() not implemented">
	</cffunction>
	
	<!--- =========== Fonctions de gestion du username =========== --->
	
	<cffunction access="private" name="usernameExists" returntype="Boolean" description="LOCK:ReadOnly"
	hint="Retourne TRUE si endpoint et username ont été enregistré avec register(). Lève une exception si leur valeurs sont invalides">
		<cfargument name="endpoint" type="String" required="true" hint="URL de base à accéder">
		<cfargument name="username" type="String" required="false" default="" hint="Login">
		<!--- Vérification de la validité de username --->
		<cfset checkUsername(ARGUMENTS.username)>
		<!--- Vérification de l'existence de endpoint --->
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readonly" throwontimeout="true">
			<cfset var exists=endpointExists(ARGUMENTS.endpoint)>
			<cfif exists>
				<cfset var endpoints=getEndpoints()>
				<!--- Clé utilisée pour username --->
				<cfset var userKey=ARGUMENTS.username NEQ "" ? ARGUMENTS.username:getEmptyUsernameValue()>
				<cfset exists=structKeyExists(endpoints[ARGUMENTS.endpoint],userKey)>
			</cfif>
			<cfreturn exists>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="checkUsername" returntype="void" hint="Retourne la valeur username ou getEmptyUsernameValue()
	si elle est vide ou non renseignée. Lève une exception si username est égal à getEmptyUsernameValue()">
		<cfargument name="username" type="String" required="false" default="" hint="Login d'accès au service">
		<cfset var userValue=TRIM(ARGUMENTS.username)>
		<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
		<cfif userValue NEQ ARGUMENTS.username>
			<cfthrow type="Custom" errorCode="#code#" message="Invalid username '#ARGUMENTS.username#'" detail="Contains extra spaces">
		<cfelseif TRIM(ARGUMENTS.username) EQ getEmptyUsernameValue()>
			<cfthrow type="Custom" errorCode="#code#" message="Invalid username '#ARGUMENTS.username#'" detail="This value is forbidden">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getEmptyUsernameValue" returntype="String"
	hint="Retourne la valeur utilisée pour représenter username si sa valeur est vide ou non renseigné">
		<cfreturn "_">
	</cffunction>
	
	<!--- =========== Fonctions de gestion du endpoint =========== --->
	
	<cffunction access="private" name="checkEndpoint" returntype="void" hint="Vérifie endpoint et lève une exception si elle est invalide">
		<cfargument name="endpoint" type="String" required="true" hint="Endpoint d'un service">
		<cfset var endpointValue=TRIM(ARGUMENTS.endpoint)>
		<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
		<cfif endpointValue EQ "">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid endpoint '#ARGUMENTS.endpoint#'" detail="Value is empty">
		<cfelseif endpointValue NEQ ARGUMENTS.endpoint>
			<cfthrow type="Custom" errorCode="#code#" message="Invalid endpoint '#ARGUMENTS.endpoint#'" detail="Contains extra spaces">
		</cfif>
	</cffunction>

	<cffunction access="private" name="endpointExists" returntype="Boolean" description="LOCK:ReadOnly"
	hint="Retourne TRUE si endpoint a été enregistré avec register(). Lève une exception si endpoint est invalide">
		<cfargument name="endpoint" type="String" required="true" hint="Endpoint d'un service">
		<!--- Vérification de la validité de username --->
		<cfset checkEndpoint(ARGUMENTS.endpoint)>
		<!--- Vérification de l'existence de endpoint --->
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readonly" throwontimeout="true">
			<cfset var endpoints=getEndpoints()>
			<cfreturn structKeyExists(endpoints,ARGUMENTS.endpoint)>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="getEndpoints" returntype="Struct"
	description="LOCK:ReadOnly" hint="Retourne la structure des endpoints enregistrés avec register()">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfreturn VARIABLES.ENDPOINTS>
		</cflock>
	</cffunction>
</cfcomponent>