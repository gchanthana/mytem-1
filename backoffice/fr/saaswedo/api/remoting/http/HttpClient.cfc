<cfcomponent author="Cedric" displayname="fr.saaswedo.api.remoting.http.HttpClient"
extends="fr.saaswedo.api.remoting.RemoteClient" hint="Extension de RemoteClient spécifique aux requêtes HTTP">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.remoting.http.HttpClient">
		<cfreturn "fr.saaswedo.api.remoting.http.HttpClient">
	</cffunction>

	<cffunction access="public" name="getServiceCall" returntype="Any" description="LOCK:ReadOnly" hint="fr.saaswedo.api.remoting.ext.ExtendedCFHTTP">
		<cfargument name="serviceId" type="String" required="true" hint="Identifiant du service obtenu avec register()">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<!--- L'implémentation appel de service est paramétré à partir de createService() --->
			<cfset var sourceService=getService(ARGUMENTS.serviceId)>
			<cfset var serviceUser=sourceService.getUsername()>
			<cfset var servicePwd=sourceService.getPassword()>
			<cfset serviceUser=isDefined("serviceUser") ? serviceUser:"">
			<cfset servicePwd=isDefined("servicePwd") ? servicePwd:"">
			<!--- Instanciation et paramétrage de l'implémentation appel de service --->
			<cfset var httpCall=new fr.saaswedo.api.remoting.ext.ExtendedCFHTTP()>
			<cfset httpCall.setUrl(sourceService.getUrl())>
			<cfset httpCall.setUserAgent("SAASWEDO")>
			<cfset httpCall.setCharset("UTF-8")>
			<!--- Crédentiels de l'appel de service --->
			<cfset setCallCrendentials(httpCall,serviceUser,servicePwd)>
			<cfreturn httpCall>
		</cflock>
	</cffunction>

	<cffunction access="public" name="invoke" returntype="Any" hint="Envoie une requete HTTP en utilisant serviceCall et retourne la réponse CFHTTP.
	Lève une erreur HTTP.REQUEST_ERROR si une exception est capturée durant l'appel. Dans ce cas le paramètre errorTarget de
	IErrorHandler.handleError() est l'instance de l'appel. Toute exception capturée est dispatchée<br>
	L'URL doit etre définie avant l'appel de cette fonction i.e serviceCall.getUrl() est définie">
		<cfargument name="serviceCall" type="Any" required="true" hint="fr.saaswedo.api.remoting.ext.ExtendedCFHTTP">
		<cfargument name="method" type="String" required="true" hint="Méthode HTTP e.g GET_METHOD()">
		<cfargument name="parameters" type="Array" required="true" hint="Paramètres de la requete qui seront ajoutés par ordre d'apparition.
		Chaque élément doit etre une structure contenant les clés : TYPE e.g HEADER_TYPE(),NAME,VALUE. <b>Il est recommandé de grouper par TYPE</b>">
		<cfset var errorTarget=THIS>
		<cftry>
			<!--- L'URL doit etre définie i.e serviceCall.getUrl() --->
			<cfset var httpCall=ARGUMENTS.serviceCall>
			<!--- Vérification de la validité de la méthode HTTP --->
			<cfset checkMethod(ARGUMENTS.method)>
			<!--- La méthode HTTP doit etre définie avant les paramètres et headers HTTP. Voir setCallParameters() --->
			<cfset httpCall.setMethod(ARGUMENTS.method)>
			<!--- Paramètres de l'appel au service : Doivent etre définis après la méthode et les headers HTTP. Voir setCallParameters() --->
			<cfset setCallParameters(httpCall,ARGUMENTS.parameters)>		
			<!--- IErrorTarget devient l'appel de service et reste inchangé pour la suite --->
			<cfset var errorCode=GLOBALS().CONST("HTTP.REQUEST_ERROR")>
			<cfset errorTarget=httpCall>
			<!--- Envoi de la requete HTTP avec l'appel --->
			<cftry>
				<cfset var requestResult=httpCall.send()>
				<cfcatch type="Any">
					<cfthrow type="Custom" errorCode="#errorCode#" message="#CFCATCH.message#" detail="#CFCATCH.detail#">
				</cfcatch>
			</cftry>
			<cfif NOT isDefined("requestResult")>
				<cfthrow type="Custom" errorCode="#errorCode#" message="#componentName()#.invoke() HTTP result is not defined">
			</cfif>
			<cfreturn requestResult.getPrefix()>
			<cfcatch type="Any">
				<cfset dispatchErrorAs(errorTarget,CFCATCH)>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="getResponseCode" returntype="Numeric" hint="Retourne le code HTTP de httpResponse. Par défaut: 0">
		<cfargument name="httpResponse" type="Struct" required="true" hint="Réponse HTTP au format CFHTTP">
		<cfset var httpObject=ARGUMENTS.httpResponse>
		<cfif structKeyExists(httpObject.responseHeader,"status_code")>
			<cfreturn VAL(httpObject.responseHeader.status_code)>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>

	<cffunction access="public" name="getResponseMime" returntype="String" hint="Retourne le type MIME de httpResponse. Par défaut: Chaine vide">
		<cfargument name="httpResponse" type="Struct" required="true" hint="Réponse HTTP (CFHTTP)">
		<cfif structKeyExists(ARGUMENTS.httpResponse,"mimeType")>
			<cfreturn ARGUMENTS.httpResponse.mimeType>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getStringContent" returntype="Any" hint="Retourne la désérialisation String du contenu httpContent.
	Ecrit un log de warning si httpContent est de type complexe différent de java.io.ByteArrayOutputStream">
		<cfargument name="httpContent" type="Any" required="true" hint="Contenu de la réponse HTTP (CFHTTP.FileContent)">
		<cfset var content=ARGUMENTS.httpContent>
		<cfif isSimpleValue(content) AND isValid("String",content)>
			<cfreturn content>
		<cfelse>
			<!--- Conversion en String. Ne pas comparer avec getClass().getName() car héritage possible --->
			<cfif isInstanceOf(content,"java.io.ByteArrayOutputStream")>
				<cfreturn content.toString()>
			<cfelse>
				<cfset var log=GLOBALS().CONST('Remoting.LOG')>
				<cfset var contentClass=getMetadata(content).NAME>
				<cflog type="warning" text="[#log#] String deserializing from unknown type #contentClass#">
				<cfreturn toString(content)>
			</cfif>
		</cfif>
	</cffunction>

	<cffunction access="public" name="getResponseContent" returntype="Any"
	hint="Retourne le contenu HTTP de httpResponse (CFHTTP.fileContent) ou une chaine vide sinon">
		<cfargument name="httpResponse" type="Struct" required="true" hint="Réponse HTTP (CFHTTP)">
		<cfset var httpObject=ARGUMENTS.httpResponse>
		<cfif structKeyExists(httpObject,"fileContent")>
			<cfreturn httpObject.fileContent>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>

	<cffunction access="public" name="BODY_TYPE" returntype="String" hint="Retourne le type correspondant à un paramètre de type body">
		<cfreturn "body">
	</cffunction>

	<cffunction access="public" name="FORMFIELD_TYPE" returntype="String" hint="Retourne le type correspondant à un paramètre de type formfield">
		<cfreturn "formfield">
	</cffunction>

	<cffunction access="public" name="URL_TYPE" returntype="String" hint="Retourne le type correspondant à un paramètre de type url">
		<cfreturn "url">
	</cffunction>

	<cffunction access="public" name="HEADER_TYPE" returntype="String" hint="Retourne le type correspondant à un paramètre de type header">
		<cfreturn "header">
	</cffunction>
	
	<cffunction access="public" name="CGI_TYPE" returntype="String" hint="Retourne le type correspondant à un paramètre de type CGI (Encoded header)">
		<cfreturn "CGI">
	</cffunction>

	<cffunction access="public" name="COOKIE_TYPE" returntype="String" hint="Retourne le type correspondant à un paramètre de type cookie">
		<cfreturn "cookie">
	</cffunction>

	<cffunction access="public" name="CONTENT_TYPE_HEADER" returntype="String" hint="Retourne l'en-tete HTTP Content-Type">
		<cfreturn "Content-Type">
	</cffunction>

	<cffunction access="public" name="ACCEPT_HEADER" returntype="String" hint="Retourne l'en-tete HTTP Accept">
		<cfreturn "Accept">
	</cffunction>

	<cffunction access="public" name="GET_METHOD" returntype="String" hint="Retourne la valeur pour la méthode HTTP GET">
		<cfreturn "GET">
	</cffunction>
	
	<cffunction access="public" name="POST_METHOD" returntype="String" hint="Retourne la valeur pour la méthode HTTP POST">
		<cfreturn "POST">
	</cffunction>
	
	<cffunction access="public" name="PUT_METHOD" returntype="String" hint="Retourne la valeur pour la méthode HTTP PUT">
		<cfreturn "PUT">
	</cffunction>
	
	<cffunction access="public" name="DELETE_METHOD" returntype="String" hint="Retourne la valeur pour la méthode HTTP DELETE">
		<cfreturn "DELETE">
	</cffunction>
	
	<!--- =========== Fin des fonction publiques =========== --->

	<cffunction access="private" name="BASIC_AUTH_HEADER" returntype="String" hint="Retourne l'en-tete d'authentification HTTP Basic">
		<cfreturn "Authorization">
	</cffunction>

	<!--- =========== Fonctions concernant l'implémentation : Appel de service =========== --->

	<!--- Implémentée car CFHTTP.setMethod() n'est pas redéfinie --->
	<cffunction access="private" name="checkMethod" returntype="void"
	hint="Vérifie la validité de la méthode method et lève une exception si elle est invalide ou non supportée">
		<cfargument name="method" type="String" required="true" hint="Méthode HTTP">
		<cfset var httpMethod=ARGUMENTS.method>
		<!--- Liste des méthodes HTTP valides et supportées --->
		<cfset var isValidMethod=(httpMethod EQ GET_METHOD()) OR (httpMethod EQ POST_METHOD())>
		<cfset isValidMethod=isValidMethod OR (httpMethod EQ PUT_METHOD()) OR (httpMethod EQ DELETE_METHOD())>
		<!--- Vérification de la validité de la méthode HTTP fournie --->
		<cfif NOT isValidMethod>
			<cfset var code=GLOBALS().CONST("HTTP.ERROR")>
			<cfthrow type="Custom" errorCode="#code#" message="Invalid http method '#httpMethod#'" detail="Not supported">
		</cfif>
	</cffunction>

	<cffunction access="private" name="isHeaderSupported" returntype="Boolean" hint="Retourne TRUE si le header est supporté">
		<cfargument name="headerName" type="String" required="true" hint="Header HTTP">
		<!--- Liste des headers HTTP supportés --->
		<cfreturn ARGUMENTS.headerName NEQ BASIC_AUTH_HEADER()>
	</cffunction>

	<!--- Implémentée car CFHTTP.addParam() n'est pas redéfinie --->
	<cffunction access="private" name="checkHttpHeader" returntype="void" hint="Vérifie la validité de l'en-tête HTTP header et lève une exception
	si elle est invalide ou non supportée. La fonction isHeaderSupported() est utilisée pour vérifier si le header est supporté
	Cette fonction lève une exception pour le header BASIC_AUTH_HEADER() : Il est réservé à setCallCredentials()">
		<cfargument name="header" type="Struct" required="true" hint="En-tête HTTP (Clés : NAME,VALUE)">
		<cfset var httpHeader=ARGUMENTS.header>
		<cfset var code=GLOBALS().CONST("HTTP.ERROR")>
		<!--- Vérification de la validité de la structure et du nom du header --->
		<cfset var nameIsDefined=structKeyExists(httpHeader,"NAME")>
		<cfset var valueIsDefined=structKeyExists(httpHeader,"VALUE")>
		<cfif nameIsDefined AND valueIsDefined>
			<cfset var headerName=httpHeader.NAME>
			<cfif headerName NEQ "">
				<cfif headerName NEQ TRIM(httpHeader.NAME)>
					<cfthrow type="Custom" errorCode="#code#" message="Invalid http header '#headerName#'" detail="Contains extra spaces">
				<cfelseif NOT isHeaderSupported(httpHeader.NAME)>
					<cfthrow type="Custom" errorCode="#code#" message="Invalid http header '#headerName#'" detail="Reserved header name">
				</cfif>
			<cfelse>
				<cfthrow type="Custom" errorCode="#code#" message="Invalid http header '#headerName#'" detail="Empty name">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorCode="#code#" message="Missing keys in http header structure" detail="Required keys: NAME,VALUE">
		</cfif>
	</cffunction>

	<cffunction access="private" name="isParameterTypeSupported" returntype="Boolean" hint="Retourne TRUE si le paramètre est supporté">
		<cfargument name="parameterType" type="String" required="true" hint="Type de paramètre">
		<cfset var typeValue=ARGUMENTS.parameterType>
		<!--- Liste des types de paramètres CFHTTP supportés --->
		<cfset var typeIsValid=(typeValue EQ BODY_TYPE()) OR (typeValue EQ FORMFIELD_TYPE()) OR (typeValue EQ URL_TYPE())>
		<cfreturn typeIsValid OR (typeValue EQ HEADER_TYPE()) OR (typeValue EQ CGI_TYPE()) OR (typeValue EQ COOKIE_TYPE())>
	</cffunction>

	<!--- Implémentée car CFHTTP.addParam() n'est pas redéfinie --->
	<cffunction access="private" name="checkParameter" returntype="void" hint="Vérifie si parameter est valide et lève une exception
	s'il est invalide ou non supporté. La fonction isParameterTypeSupported() est utilisée pour vérifier si le paramètre est supporté">
		<cfargument name="parameter" type="Struct" required="true" hint="Paramètre HTTP. Structure contenant les clés : TYPE,NAME,VALUE">
		<cfset var httpParam=ARGUMENTS.parameter>
		<cfset var typeIsDefined=structKeyExists(httpParam,"TYPE")>
		<cfset var nameIsDefined=structKeyExists(httpParam,"NAME")>
		<cfset var valueIsDefined=structKeyExists(httpParam,"VALUE")>
		<!--- Cette fonction peut etre réutilisée par les implémentation qui héritent de ce composant --->
		<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
		<!--- Vérification de la validité du paramètre --->
		<cfif typeIsDefined AND nameIsDefined AND valueIsDefined>
			<cfset var nameValue=httpParam.NAME>
			<cfif nameValue EQ "">
				<cfthrow type="Custom" errorCode="#code#" message="Invalid parameter name '#nameValue#'" detail="Empty name">
			<cfelseif nameValue NEQ TRIM(nameValue)>
				<cfthrow type="Custom" errorCode="#code#" message="Invalid parameter name '#nameValue#'" detail="Contains extra spaces">
			<cfelse>
				<cfset var typeValue=httpParam.TYPE>
				<cfif NOT isParameterTypeSupported(typeValue)>
					<cfthrow type="Custom" errorCode="#code#" message="Invalid parameter type '#typeValue#'" detail="Not supported">
				</cfif>
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorCode="#code#" message="Invalid parameter structure" detail="Required keys: TYPE,NAME,VALUE">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="setCallParameters" returntype="void" hint="Défini les paramètres de l'appel de service serviceCall">
		<cfargument name="serviceCall" type="Any" required="true" hint="Retourné par getServiceCall()">
		<cfargument name="parameters" type="Array" required="true" hint="Paramètres de la requete. Tableau de structures contenant: NAME,VALUE,TYPE">
		<cfset var httpCall=ARGUMENTS.serviceCall>
		<cfset var httpParams=ARGUMENTS.parameters>
		<cfset var paramCount=arrayLen(httpParams)>
		<cfloop index="i" from="1" to="#paramCount#">
			<cfset var httpParam=httpParams[i]>
			<cfset setCallParameter(httpCall,httpParam)>
		</cfloop>
	</cffunction>

	<cffunction access="private" name="setCallParameter" returntype="void" hint="Défini un paramètre de l'appel de service serviceCall">
		<cfargument name="serviceCall" type="fr.saaswedo.api.remoting.ext.ExtendedCFHTTP" required="true" hint="Retourné par getServiceCall()">
		<cfargument name="parameter" type="Struct" required="true" hint="Paramètre à fournir à serviceCall. Structure avec les clés NAME,VALUE,TYPE">
		<cfset var httpCall=ARGUMENTS.serviceCall>
		<cfset var httpParam=ARGUMENTS.parameter>
		<!--- Vérification de la validité du paramètre --->
		<cfset checkParameter(httpParam)>
		<cfset var paramType=httpParam.TYPE>
		<!--- Défini la valeur d'un paramètre ou attribut CFHTTP pour l'appel de service --->
		<cfif paramType EQ BODY_TYPE()>
			<cfset httpCall.addParam(type=paramType,value=httpParam.VALUE)>
		<cfelse>
			<!--- Vérification de validité si le type est HEADER_TYPE() --->
			<cfif paramType EQ HEADER_TYPE()>
				<cfset checkHttpHeader(httpParam)>
			</cfif>
			<cfset httpCall.addParam(type=paramType,name=httpParam.NAME,value=httpParam.VALUE)>
		</cfif>
	</cffunction>
	
	<!--- Ces versions de setCallParameters() et setCallParameter() spécifient les paramètres différemment pour les méthodes HTTP PUT et DELETE
	Problème du code existant : Les paramètres et le Body ne sont pas récupérés dans ColdFusion quand la méthode HTTP est DELETE.
	Problème du code existant : Les paramètres ne sont pas récupérés dans ColdFusion quand la méthode HTTP est PUT.
	Ces implémentations garantissent que les paramètres sont dans le scope FORM ou URL et que le Body soit dans getHttpRequestData().content
	Elles sont commentées car il faut d'abord :
	- Vérifier si un client HTTP peut transmettre les paramètres et le Body de la manière requise par cette implémentation
	- Vérifier leur comportement par rapport à des API respectant le standard HTTP e.g MobileIron,Zendesk
	
	<cffunction access="private" name="setCallParameters" returntype="void" hint="Défini les paramètres de l'appel de service serviceCall.<br>
	La méthode HTTP et l'URL doivent etre définies avant l'appel de cette fonction i.e serviceCall.getMethod() et serviceCall.getUrl() sont définis">
		<cfargument name="serviceCall" type="Any" required="true" hint="Retourné par getServiceCall()">
		<cfargument name="parameters" type="Array" required="true" hint="Paramètres de la requete. Tableau de structures contenant: NAME,VALUE,TYPE">
		<cfset var httpCall=ARGUMENTS.serviceCall>
		<!--- La méthode HTTP et l'URL doivent etre définis avant de spécifier les paramètres et headers HTTP --->
		<cfset var httpMethod="">
		<cfset var httpURL="">
		<cfset httpMethod=httpCall.getMethod()>
		<cfset httpURL=httpCall.getUrl()>
		<cfif (TRIM(httpMethod) NEQ "") AND (TRIM(httpURL) NEQ "")>
			<cfset var httpParams=ARGUMENTS.parameters>
			<cfset var paramCount=arrayLen(httpParams)>
			<!--- Ajout des paramètres dans l'appel HTTP --->
			<cfloop index="i" from="1" to="#paramCount#">
				<cfset var httpParam=httpParams[i]>
				<cfset setCallParameter(httpCall,httpParam)>
			</cfloop>
		<cfelse>
			<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
			<cfthrow type="Custom" errorCode="#code#" detail="One of URL and/or HTTP method is empty"
			message="Illegal operation: HTTP parameters and headers cannot be set if URL and method are not defined">
		</cfif>
	</cffunction>

	<cffunction access="private" name="setCallParameter" returntype="void" hint="Défini un paramètre de l'appel de service serviceCall.<br>
	La méthode HTTP et l'URL doivent etre définies avant l'appel de cette fonction i.e serviceCall.getMethod() et serviceCall.getUrl() sont définis">
		<cfargument name="serviceCall" type="fr.saaswedo.api.remoting.ext.ExtendedCFHTTP" required="true" hint="Retourné par getServiceCall()">
		<cfargument name="parameter" type="Struct" required="true" hint="Paramètre à fournir à serviceCall. Structure avec les clés NAME,VALUE,TYPE">
		<cfset var httpCall=ARGUMENTS.serviceCall>
		<cfset var httpParam=ARGUMENTS.parameter>
		<!--- Vérification de la validité du paramètre --->
		<cfset checkParameter(httpParam)>
		<cfset var paramType=httpParam.TYPE>		
		<!--- Défini la valeur d'un paramètre ou attribut CFHTTP pour l'appel de service --->
		<cfif paramType EQ BODY_TYPE()>
			<cfset httpCall.addParam(type=paramType,value=httpParam.VALUE)>
		<cfelse>
			<!--- La spécification des paramètres de type FORMFIELD_TYPE() dépend de la méthode HTTP de l'appel --->
			<cfif paramType EQ FORMFIELD_TYPE()>
				<cfset var httpMethod=httpCall.getMethod()>
				<!--- Méthodes HTTP PUT et DELETE : Les paramètres de type FORMFIELD_TYPE() doivent etre spécifiés encodés dans l'URL --->
				<cfif (compareNoCase(httpMethod,PUT_METHOD()) EQ 0) OR (compareNoCase(httpMethod,DELETE_METHOD()) EQ 0)>
					<cfset var httpURL=createObject("java","java.net.URL").init(httpCall.getUrl())>
					<cfset var paramString=httpParam.NAME & "=" & urlEncodedFormat(httpParam.VALUE,"UTF-8")>
					<cfif LEN(httpURL.getQuery()) EQ 0>
						<cfset httpCall.setUrl(httpURL.toString() & "?" & paramString)>
					<cfelse>
						<cfset httpCall.setUrl(httpURL.toString() & "&" & paramString)>
					</cfif>
					<cflog type="information" text="HTTP #httpMethod# : #httpCall.getUrl()#"> 
				<!--- La méthode addParam() fonctionne correctement pour les autres méthode HTTP : GET,POST --->
				<cfelse>
					<cfset httpCall.addParam(type=paramType,name=httpParam.NAME,value=httpParam.VALUE)>
				</cfif>
			<!--- Spécification des autres types de paramètres : HEADER,COOKIE,CGI,etc... --->
			<cfelse>
				<!--- Vérification de validité si le type est HEADER_TYPE() --->
				<cfif paramType EQ HEADER_TYPE()>
					<cfset checkHttpHeader(httpParam)>
				</cfif>
				<cfset httpCall.addParam(type=paramType,name=httpParam.NAME,value=httpParam.VALUE)>
			</cfif>
		</cfif>
	</cffunction>
	--->
	
	<cffunction access="private" name="setCallCrendentials" returntype="void" hint="Défini les crédentiels HTTP en utilisant le header
	BASIC_AUTH_HEADER(). Aucune action n'est effectuée si username n'est pas renseigné. Le username fourni est vérifiée avec checkUsername()">
		<cfargument name="serviceCall" type="fr.saaswedo.api.remoting.ext.ExtendedCFHTTP" required="true" hint="Retourné par getServiceCall()">
		<cfargument name="username" type="String" required="false" default="" hint="Login (HTTP Basic)">
		<cfargument name="password" type="String" required="false" default="" hint="Password (HTTP Basic)">
		<!--- Vérification de la validité du username --->
		<cfset checkUsername(ARGUMENTS.username)>
		<!--- Les crédentiels sont définis si le username fourni est renseigné --->
		<cfif ARGUMENTS.username NEQ "">
			<cfset var httpCall=ARGUMENTS.serviceCall>
			<cfset var httpBasicAuth="Basic " & toBase64(ARGUMENTS.username & ":" & ARGUMENTS.password)>
			<cfset httpCall.addParam(type=HEADER_TYPE(),name=BASIC_AUTH_HEADER(),value=httpBasicAuth)>
		</cfif>
	</cffunction>
	
	<!--- =========== Fonctions de gestion du service =========== --->
	
	<cffunction access="private" name="createService" returntype="fr.saaswedo.api.remoting.ext.ExtendedCFHTTP"
	hint="Vérifie la validité de parameters.endpoint avec checkEndpoint() et lève une exception si la valeur n'est pas une URL">
		<cfargument name="parameters" type="Struct" required="true" hint="Structure contenant les clés: endpoint,username,password">
		<cfset var params=ARGUMENTS.parameters>
		<!--- Vérification des paramètres fournis --->
		<cfif structKeyExists(params,"endpoint") AND structKeyExists(params,"username") AND structKeyExists(params,"password")>
			<cfset var endpointValue=params.endpoint>
			<!--- Vérification de la validité du endpoint --->
			<cfset checkEndpoint(endpointValue)>
			<!--- Création du service --->
			<cfset var httpService=new fr.saaswedo.api.remoting.ext.ExtendedCFHTTP()>
			<cfset httpService.setUrl(endpointValue)>
			<!--- Valeur du header HTTP Basic qui interdit tout appel HTTP : Force l'utilisation de getServiceCall() --->
			<cfset httpService.addParam(type="header",name=BASIC_AUTH_HEADER(),value="FORBIDDEN")>
			<cfreturn httpService>
		<cfelse>
			<cfset var code=GLOBALS().CONST("HTTP.ERROR")>
			<cfthrow type="Custom" errorCode="#code#"
			message="Required keys are missing in createService() parameters" detail="Required keys: endpoint,username,password">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="setServiceCrendentials" returntype="void" hint="Définit les crédentiels du service.
	<br><br>Aucune action n'est effectuée dans les cas suivants :<br>- Le username fourni n'est pas renseigné (e.g Chaine vide).
	<br>- Le username a déjà été défini avec cette fonction et la valeur fournie est différente de l'existante.
	<br><br>Le password n'est pas défini dans les cas suivants :<br>- Le password fourni n'est pas renseigné (e.g Chaine vide).
	<br>- La valeur fournie est identique à l'existante<br>Le username fourni est vérifié avec checkUsername() avant toute opération">
		<cfargument name="serviceId" type="Any" required="true" hint="Identifiant du service obtenu avec register()">
		<cfargument name="username" type="String" required="false" default="" hint="Login (HTTP Basic) : case sensitive">
		<cfargument name="password" type="String" required="false" default="" hint="Password (HTTP Basic) : case sensitive">
		<cfset var httpService=getService(ARGUMENTS.serviceId)>
		<cfset var userValue=ARGUMENTS.username>
		<!--- Vérification de la validité du username --->
		<cfset checkUsername(userValue)>
		<!--- Les crédentiels sont définis si le username fourni est renseigné --->
		<cfif userValue NEQ "">
			<cfset var logAction="">
			<cfset var log=GLOBALS().CONST('Remoting.LOG')>
			<!--- Crédentiels existants --->
			<cfset var currentUsername=httpService.getUsername()>
			<cfset var currentPassword=httpService.getPassword()>
			<!--- Le username est mis à jour s'il n'est pas encore défini --->
			<cfif NOT isDefined("currentUsername")>
				<cfset logAction="username">
				<cfset httpService.setUsername(userValue)>
				<!--- Le username existant a été mis à jour --->
				<cfset currentUsername=userValue>
			<!--- Le username existant DOIT ETRE identique à celui qui est fourni --->
			<cfelseif compare(currentUsername,userValue) NEQ 0>
				<cfset var code=GLOBALS().CONST("HTTP.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="Forbidden username update"
				detail="Current value '#currentUsername#' IS NOT EQUAL to the provided value '#userValue#'">
			</cfif>
			<!--- Le password est mis à jour si :
			- Le username existant est identique à celui fourni : Pas de test à faire car une exception aurait été levée ci-dessus sinon
			- Le password n'a pas encore été défini ou sa valeur existante est différente de celle fournie
			--->
			<cfif (NOT isDefined("currentPassword")) OR (compare(currentPassword,ARGUMENTS.password) NEQ 0)>
				<cfset httpService.setPassword(ARGUMENTS.password)>
				<cfset logAction=logAction & " and password">
			</cfif>
			<!--- Log de la mise à jour des crédentiels --->
			<cfif logAction NEQ "">
				<cflog type="information" text="[#log#][#ARGUMENTS.serviceId#] #httpService.getUrl()# #logAction# updated">
			</cfif>
		</cfif>
	</cffunction>
	
	<!--- =========== Fonctions de gestion du endpoint =========== --->
	
	<cffunction access="private" name="checkEndpoint" returntype="void"
	hint="Appele la fonction parent et lève une exception si endpoint n'est pas une URL valide">
		<cfargument name="endpoint" type="String" required="true" hint="Endpoint d'un service">
		<cfset var endpointValue=ARGUMENTS.endpoint>
		<cfset SUPER.checkEndpoint(endpointValue)>
		<cfif NOT isValid("URL",endpointValue)>
			<cfset var code=GLOBALS().CONST("HTTP.ERROR")>
			<cfthrow type="Custom" errorCode="#code#" message="Invalid endpoint '#endpointValue#'" detail="Invalid URL">
		</cfif>
	</cffunction>
</cfcomponent>