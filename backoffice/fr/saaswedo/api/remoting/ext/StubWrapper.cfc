<cfcomponent author="Cedric" displayname="fr.saaswedo.api.remoting.ext.StubWrapper"
extends="fr.saaswedo.api.patterns.cfc.Component" implements="fr.saaswedo.api.patterns.error.IErrorTarget"
hint="Wrapper pour une instance (Java) org.apache.axis.client.Stub afin d'implémentation IErrorTarget">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.remoting.ext.StubWrapper">
		<cfreturn "fr.saaswedo.api.remoting.ext.StubWrapper">
	</cffunction>
	
	<cffunction access="public" name="getStub" returntype="Any" description="LOCK:ReadOnly" hint="Retourne l'instance org.apache.axis.client.Stub">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfif structKeyExists(VARIABLES,"STUB")>
				<cfreturn VARIABLES.STUB>
			<cfelse>
				<cfset var code=GLOBALS().CONST("WS.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="Stub instance is not defined">
			</cfif>
		</cflock>
	</cffunction>
	
	<!--- fr.saaswedo.api.patterns.error.IErrorTarget --->
	<cffunction name="hasErrorTypeHandler" returntype="Boolean" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfargument name="errorCode" type="String" required="true">
		<cfset var code=GLOBALS().CONST("WS.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.hasErrorTypeHandler() is not implemented">
	</cffunction>
	
	<cffunction name="addErrorHandler" returntype="void" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfargument name="errorHandler" type="fr.saaswedo.api.patterns.error.IErrorHandler" required="true">
		<cfargument name="errorCode" type="String" required="true">
		<cfset var code=GLOBALS().CONST("WS.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.addErrorHandler() is not implemented">
	</cffunction>
	
	<cffunction name="dispatchError" returntype="void" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfargument name="cfCatchObject" type="Any" required="true">
		<cfargument name="errorInfos" type="Struct" required="false">
		<cfset var code=GLOBALS().CONST("WS.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.dispatchError() is not implemented">
	</cffunction>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.patterns.cfc.Component"
	description="LOCK:Exclusive" hint="Appele une fonction d'initialisation interne quand isInitialized() retourne FALSE.
	Ce constructeur est appelé automatiquement quand l'opérateur <b>new</b> est utilisé">
		<cfargument name="stub" type="Any" required="true" hint="Instance Stub à encapsuler dans ce Wrapper">
		<!--- TODO: Cette librairie est obsolète --->
		<cfthrow type="Custom" message="Obsolete implementation" detail="This implementation will be removed and should not be used">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset SUPER.init()>
			<!--- Cette vérification n'est valable que si la version de ColdFusion utilise Axis --->
			<cfset var axisStubClass=createObject("java","java.lang.Class").forName("org.apache.axis.client.Stub")>
			<cfif axisStubClass.isAssignableFrom(ARGUMENTS.stub.getClass())>
				<cfset VARIABLES.STUB=ARGUMENTS.stub>
				<cfreturn THIS>
			<cfelse>
				<cfset var code=GLOBALS().CONST("WS.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="Provided Stub is not an implementation of org.apache.axis.client.Stub">
			</cfif>
		</cflock>
	</cffunction>
	
	<!--- =========== Fin des fonction publiques =========== --->
</cfcomponent>