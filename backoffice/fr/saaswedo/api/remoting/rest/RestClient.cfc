<cfcomponent author="Cedric" displayname="fr.saaswedo.api.remoting.rest.RestClient" extends="fr.saaswedo.api.remoting.http.HttpClient"
hint="Extension de HttpClient rajoutant des fonction de traitements des réponses HTTP au format JSON et XML.<br>Le type de paramètre
SUFFIX_TYPE() est utilisé quand une partie du chemin d'une URL doit etre variable au lieu d'appeler register() pour chaque valeur">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.remoting.rest.RestClient">
		<cfreturn "fr.saaswedo.api.remoting.rest.RestClient">
	</cffunction>
	
	<cffunction access="public" name="invoke" returntype="Any" hint="Utilise la fonction parent pour envoyer une requete HTTP et retourne
	la réponse CFHTTP. Lève une erreur HTTP.REQUEST_ERROR avec l'appel HTTP en paramètre errorTarget de IErrorHandler.handleError() si :
	<br>- La clé errorDetail est renseignée dans la réponse CFHTTP<br>- Le code HTTP de la réponse est supérieur ou égal à 500">
		<cfargument name="serviceCall" type="Any" required="true" hint="fr.saaswedo.api.remoting.ext.ExtendedCFHTTP">
		<cfargument name="method" type="String" required="true" hint="Méthode HTTP e.g GET_METHOD()">
		<cfargument name="parameters" type="Array" required="true" hint="Paramètres de la requete qui seront ajoutés par ordre d'apparition.
		Chaque élément doit etre une structure contenant les clés : TYPE e.g HEADER_TYPE(),NAME,VALUE. <b>Il est recommandé de grouper par TYPE</b>">
		<!--- Les exceptions capturée par cet appel sont déjà traitées et dispatchées --->
		<cfset var httpResponse=SUPER.invoke(argumentCollection=ARGUMENTS)>
		<!--- Vérification de la validité de la réponse --->
		<cftry>
			<cfset var httpCode=getResponseCode(httpResponse)>
			<cfset var errorDetail=structKeyExists(httpResponse,"errorDetail") ? httpResponse.errorDetail:"">
			<cfif (TRIM(errorDetail) NEQ "") OR (httpCode GTE 500)>
				<cfset var code=GLOBALS().CONST("HTTP.REQUEST_ERROR")>
				<cfset var uri=ARGUMENTS.serviceCall.getUrl()>
				<cfset var stringContent=getStringContent(getResponseContent(httpResponse))>
				<cfset var errorMsg="[URL: #uri#] - #stringContent#">
				<cfif httpCode GT 0>
					<cfset errorMsg="HTTP #httpCode# " & errorMsg>
				</cfif>
				<cfthrow type="Custom" errorCode="#code#" message="#errorMsg#" detail="#errorDetail#">
			<cfelse>
				<cfreturn httpResponse>
			</cfif>
			<cfcatch type="Any">
				<cfset dispatchErrorAs(ARGUMENTS.serviceCall,CFCATCH)>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getJsonContent" returntype="Struct" hint="Retourne la désérialisation JSON du contenu httpContent">
		<cfargument name="httpContent" type="String" required="true" hint="Contenu de la réponse HTTP (CFHTTP.FileContent)">
		<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
		<cfset var errorMsg="JSON deserialization failed">
		<cfif isJSON(ARGUMENTS.httpContent)>
			<cftry>
				<cfreturn deserializeJSON(ARGUMENTS.httpContent)>
				<cfcatch type="Any">
					<cfthrow type="Custom" errorCode="#code#" message="#errorMsg#. Cause: #CFCATCH.message#" detail="#CFCATCH.detail#">
				</cfcatch>
			</cftry>
		<cfelse>
			<cfthrow type="Custom" errorCode="#code#" message="#errorMsg#. Cause: Invalid JSON content" detail="Content : '#ARGUMENTS.httpContent#'">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getXmlContent" returntype="XML" hint="Retourne la désérialisation XML du contenu httpContent">
		<cfargument name="httpContent" type="String" required="true" hint="Contenu de la réponse HTTP (CFHTTP.FileContent)">
		<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
		<cfset var errorMsg="XML deserialization failed">
		<cfif TRIM(ARGUMENTS.httpContent) NEQ "">
			<cftry>
				<cfreturn xmlParse(ARGUMENTS.httpContent)>
				<cfcatch type="Any">
					<cfthrow type="Custom" errorCode="#code#" message="#errorMsg#. Cause: #CFCATCH.message#" detail="#CFCATCH.detail#">
				</cfcatch>
			</cftry>
		<cfelse>
			<cfthrow type="Custom" errorCode="#code#" message="#errorMsg#. Cause: Empty string" detail="Content : '#ARGUMENTS.httpContent#'">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="SUFFIX_TYPE" returntype="String" hint="Type de paramètre dont la valeur sera suffixée à l'URL d'un appel
	de service (e.g Une partie du chemin de l'URL doit etre variable). La valeur doit etre un fragment d'URI valide">
		<cfreturn "URL_SUFFIX">
	</cffunction>
	
	<cffunction access="public" name="JSON_CONTENT_TYPE" returntype="String" hint="Retourne le Content-Type JSON">
		<cfreturn "application/json">
	</cffunction>

	<cffunction access="public" name="XML_CONTENT_TYPE" returntype="String" hint="Retourne le Content-Type XML">
		<cfreturn "application/xml">
	</cffunction>
	
	<!--- =========== Fin des fonction publiques =========== --->

	<cffunction access="private" name="setCallParameter" returntype="void" hint="Rajoute le support du paramètre de type SUFFIX_TYPE()">
		<cfargument name="serviceCall" type="fr.saaswedo.api.remoting.ext.ExtendedCFHTTP" required="true" hint="Retourné par getServiceCall()">
		<cfargument name="parameter" type="Struct" required="true" hint="Paramètre à fournir à serviceCall. Structure avec les clés NAME,VALUE,TYPE">
    	<cfset var httpCall=ARGUMENTS.serviceCall>
    	<cfset var httpParam=ARGUMENTS.parameter>
		<!--- Vérification de la validité du paramètre --->
		<cfset checkParameter(httpParam)>
    	<cfset var paramType=httpParam.TYPE>
    	<!--- Support du paramètre de type SUFFIX_TYPE() --->
    	<cfif paramType EQ SUFFIX_TYPE()>
			<cfset var httpUrl=httpCall.getUrl()>
			<cfset var suffix=httpParam.VALUE>
			<cfset var suffixLen=LEN(suffix)>
			<cfset var code=GLOBALS().CONST("Remoting.ERROR")>
			<cfset var errorMsg="Invalid SUFFIX_TYPE() parameter value '#httpParam.VALUE#'">
			<!--- Vérification de la validité du paramètre --->
			<cfif suffixLen GT 0>
				<cfif suffix EQ TRIM(suffix)>
					<cfset httpUrl=httpUrl & httpParam.VALUE>
					<cfif isValid("URL",httpUrl)>
						<!--- Affectation de la valeur du paramètre custom : SUFFIX_TYPE() --->
						<cfset httpCall.setUrl(httpUrl)>
					<cfelse>
						<cfset errorMsg="Invalid URL using SUFFIX_TYPE() value '#httpParam.VALUE#'">
						<cfthrow type="Custom" errorCode="#code#" message="#errorMsg#" detail="URL: '#httpUrl#'">
					</cfif>
				<cfelse>
					<cfthrow type="Custom" errorCode="#code#" message="#errorMsg#" detail="Contains extra spaces">
				</cfif>
			<cfelse>
				<cfthrow type="Custom" errorCode="#code#" message="#errorMsg#" detail="Empty value">
			</cfif>
		<!--- Traitement des autres types de paramètre --->
		<cfelse>
			<cfset SUPER.setCallParameter(httpCall,httpParam)>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="isParameterTypeSupported" returntype="Boolean" hint="Rajoute le support du paramètre de type SUFFIX_TYPE()">
		<cfargument name="parameterType" type="String" required="true" hint="Type de paramètre">
		<cfset var typeValue=ARGUMENTS.parameterType>
		<cfreturn (typeValue EQ SUFFIX_TYPE()) OR SUPER.isParameterTypeSupported(typeValue)>
	</cffunction>
</cfcomponent>