<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.monitoring.DatabaseLogger" extends="fr.consotel.api.monitoring.MLogger"
hint="Implémentation MLogger pour Datalert. Il faut spécifier une valeur pour l'UUID avec Mlog.setUUID().
<br>Le valeur du champ MESSAGEID (En base) sera automatiquement renseignée avec la concaténation de CGI.SERVER_NAME et CGI.SCRIPT_NAME">
	<cffunction access="public" name="logIt" returnType="void" hint="log un evenement de type Mlog">
		<cfargument name="mLog" type="fr.consotel.api.monitoring.MLog" required="true" hint="Evènement à logger">
		<cfset var t_name="DatabaseLogger_" & createUUID()>
		<cfset var cfcName=getMetadata(THIS).NAME>
		<cfthread action="run" name="#t_name#" pmlog="#mLog#" cfcName="#cfcName#">
			<cfset var datalertUUID="N.D">
			<cftry>
				<cfset var clientInfo=getClientStringInfo()>
				<cfset var messageid=CGI.SERVER_NAME & CGI.SCRIPT_NAME>
				<cfset var subjectid=pmlog.getSUBJECTID()>
				<cfset var idracine=pmLog.getIDRACINE()>
				<cfset var bool_racine_null=TRUE>
				<cfif idracine GT 0>
					<cfset bool_racine_null=FALSE>
				</cfif>
				<cfif LEN(subjectid) GT 199>
					<cfset subjectid=LEFT(TRIM(subjectid),199)>
				</cfif>
				<cfif LEN(datalertUUID) GT 39>
					<cfset datalertUUID=LEFT(TRIM(datalertUUID),39)>
				</cfif>
				<cfif LEN(messageid) GT 199>
					<cfset messageid=LEFT(TRIM(messageid),199)>
				</cfif>
				<!--- UUID Datalert --->
				<cfset datalertUUID=pmlog.getUUID()>
				<!--- Ce stockage permet de requeter sur les champs les plus consultés lors du debug de Datalert --->
				<cfquery name="qInsert" dataSource="ROCOFFRE">
					INSERT into MNT_DETAIL(IDMNT_ID,IDMNT_CAT,BODY,SUBJECTID,IDRACINE,UUID,MESSAGEID)
					VALUES (
						<cfqueryparam cfsqltype="cf_sql_integer" value="#pmlog.getIDMNT_ID()#">,
					 	<cfqueryparam cfsqltype="cf_sql_integer" value="#pmlog.getIDMNT_CAT()#">,
					 	<cfqueryparam cfsqltype="cf_sql_clob" value="#pmlog.getBODY()#">,
					 	<cfqueryparam cfsqltype="cf_sql_varchar" value="#subjectid#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#idracine#" null="#bool_racine_null#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#datalertUUID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#messageid#">)
				</cfquery>
				<cflog type="information" text="[DatabaseLogger] Error Notification stored with UUID : #datalertUUID#">
				<cfcatch type="Any">
					<cflog type="error" text="[DatabaseLogger] Error: #CFCATCH.message# - Detail: #CFCATCH.detail#">
					<cfset var mailProperties={
						from="MLogger<monitoring@saaswedo.com>",to="monitoring@saaswedo.com",server="mail-cv.consotel.fr",port="25",type="text/html"
					}>
				  	<cfmail attributeCollection="#mailProperties#" subject="[Datalert - Error] MLogger.logIt() - UUID : #datalertUUID#">
					  	<cfoutput>
							BackOffice (Debug:#isDebugMode()#) : #CGI.SERVER_NAME#:#CGI.SERVER_PORT#<br>
							Temp Directory : #getTempDirectory()#<br>
							URI : #CGI.SCRIPT_NAME#<br>
							UUID (Datalert) : #datalertUUID#<br>
							Client Info : #clientInfo#<br>
							Client HTTP Method : #CGI.REQUEST_METHOD#
							<hr>Exception :<br><cfdump var="#CFCATCH#" format="text">
					  	</cfoutput>
					</cfmail>
					<!--- L'exception n'est pas levée à nouveau car elle ne peut etre capturée que par une autre thread (action=join) --->
				</cfcatch>
			</cftry>
		</cfthread>
	</cffunction>
	
	<!--- Utilitaires --->
	
	<cffunction access="private" name="getClientStringInfo" returntype="String" hint="Retourne une chaine contenant les infos concernant le client HTTP">
		<cfset var remoteHost="'#CGI.REMOTE_HOST#' (CGI) ">
		<cfset var clientInfo="[HTTP Method:#CGI.REQUEST_METHOD#][UserAgent:#CGI.HTTP_USER_AGENT#]">
		<cfif isDefined("COOKIE")>
			<cfif structKeyExists(COOKIE,"IP_ORIGINE")>
				<cfset remoteHost="#COOKIE.IP_ORIGINE# (IP_ORIGINE) ">
			</cfif>
		</cfif>
		<cfset clientInfo=remoteHost & clientInfo>
		<cfreturn clientInfo>
	</cffunction>
</cfcomponent>