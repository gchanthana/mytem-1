<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.impl.RoamingDetection" extends="fr.saaswedo.api.da.impl.DataService"
hint="Implémentation des services de traitement des données de Roaming dans Datalert! RT">
	<!--- =========== URI des services =========== --->
	
	<cffunction access="public" name="ROAMING_DETECTION_URI" returntype="String" hint="URI du service Roaming Detection">
		<cfreturn "/api/da/rd.cfm">
	</cffunction>

	<!--- Clés correspondant à des champs métiers --->

	<cffunction access="public" name="CODE_KEY" returntype="String" hint="Champ CODE: MCC ou MNC">
		<cfreturn "CODE">
	</cffunction>
	
	<!--- =========== Codes HTTP =========== --->
	
	<cffunction access="public" name="MISSING_CODE" returntype="Numeric" hint="Code pour l'absence de la clé Code">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<cffunction access="public" name="INVALID_CODE" returntype="Numeric" hint="Code pour une valeur invalide de la clé Code">
		<cfreturn BAD_REQUEST()>
	</cffunction>

	<!--- =========== Implémentation du service =========== --->
	
	<cffunction access="private" name="serviceRequest" returntype="void" hint="Délègue le traitement du service et envoi une réponse HTTP au client">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<!--- Délégation du traitement : Le 
		- Soit cette fonction lève une exception pour arreter le traitement et donc ne pas envoyer la réponse par défaut de succès
		- Sinon (i.e Dans tous les cas) : La réponse HTTP par défaut de traitement avec succès sera envoyée au client
		--->
		<cfset processData(targetURI,requestData)>
	</cffunction>

	<!--- =========== Vérification des données métiers =========== --->
	
	<cffunction access="private" name="checkData" returntype="Boolean" hint="Effectue les vérifications métiers pour le flux correspondant à targetPage.
	<br>- La clé UTS_KEY() est obligatoire et sa valeur doit etre une chaine correspondant à une date au format UTC (ISO 8601)">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<cfset var clientInfo=getClientStringInfo()>
		<!--- Vérification des données métiers de l'implémentation parent : Renvoi une réponse HTTP si les données sont invalides --->
		<cfset var areRequestDataValid=SUPER.checkData(targetURI,requestData)>
		<cfif areRequestDataValid>
			<!--- Utilisés pour la réponse HTTP en cas d'invalidité des données --->
			<cfset var httpCode=AUTHORIZED()>
			<cfset var jsonResponse={"Message"="Bad HTTP request"}>
			<!--- Vérification des données métiers spécifiques à cette implémentation --->
			<cfset var payload=getPayload(requestData)>
			<!--- Vérifie le format des données : JSON (STRING) --->
			<cfset var isDataFormatValid=(NOT structIsEmpty(payload))>
			<cfif NOT isDataFormatValid>
				<!--- Réponse HTTP : Si la vérification Check 3 n'est pas validée --->
				<cfset httpCode=INVALID_DATA_FORMAT()>
				<cfset jsonResponse={"Message"="Data usage : bad format"}>
			</cfif>
			<cfif isDataFormatValid>
				<!--- Vérification du champ : UTS --->
				<cfset var utsIsValid=structKeyExists(payload,UTS_KEY())>
				<cfif utsIsValid>
					<cfset utsIsValid=isValidUTC(payload[UTS_KEY()])>
					<cfif NOT utsIsValid>
						<!--- Champ UTS invalide --->
						<cfset httpCode=INVALID_UTS()>
						<cfset jsonResponse={"Message"="Unix Timestamp : bad format."}>
					<cfelse>
						<!--- Vérification du champ CODE (MCC/MNC) --->
						<cfif structKeyExists(payload,CODE_KEY())>
							<cfif NOT isValidMCCMNC(payload[CODE_KEY()])>
								<!--- Champ CODE invalide --->
								<cfset httpCode=INVALID_CODE()>
								<cfset jsonResponse={"Message"="MCC/MNC bad : format."}>
							</cfif>
						<!--- Le champ CODE est facultatif --->
						<cfelse>
							<cflog type="warning" text="[#log#][#targetURI#] Missing NON-MANDATORY key #CODE_KEY()# from #clientInfo#">
						</cfif>
					</cfif>
				<cfelse>
					<!--- Le champ UTS est absent --->
					<cfset httpCode=MISSING_UTS()>
					<cfset jsonResponse={"Message"="Unix Timestamp not present in the Payload."}>
				</cfif>
			</cfif>
			<!--- MAJ du statut de validité de la requete HTTP cliente : areRequestDataValid vaut TRUE avant cette affectation --->
			<cfset areRequestDataValid=httpCode EQ AUTHORIZED()>
			<!--- Réponse en cas d'invalidité des données --->
			<cfif NOT areRequestDataValid>
				<!--- Les cas d'invalidité de requete sont toujours mentionnés dans les logs --->
				<cflog type="error" text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - #jsonResponse.message#">
				<!--- Traitement des données invalides --->
				<cfset handleInvalidRequestData(targetURI,requestData,httpCode,jsonResponse)>
			</cfif>
		</cfif>
		<cfreturn areRequestDataValid>
	</cffunction>
	
	<!--- Traitement des données métiers valides --->
	
	<cffunction access="private" name="processData" returntype="void" hint="Effectue le traitement du flux Roaming Detection et envoi une réponse HTTP.
	Les exceptions ne sont pas capturée par cette fonction. Elles sont traitées par l'application qui va ensuite envoyer une réponse HTTP au client
	<br>Ce traitement préfixe toujours la valeur du champ UUID_HEADER() avec un caractère spécial pour éviter les problème de désérialization JSON">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var payload=getPayload(requestData)>
		<!--- Si le champs CODE est présent alors les données sont traitées --->
		<cfif structKeyExists(payload,CODE_KEY())>
			<!--- Données métiers --->
			<cfset var roamingDetectionData=DUPLICATE(payload)>
			<!--- Ajout du champs UUID dans les données métiers : Sa valeur est toujours préfixée par un caractère spécial --->
			<cfset var escapeAsciiCode=jsonNumberEscapeChar()>
			<cfset roamingDetectionData[UUID_HEADER()]=CHR(escapeAsciiCode) & getHeaderValue(UUID_HEADER(),requestData)>
			<!--- Ajout du champs CODE dans les données métiers : Sa valeur est toujours préfixée par un caractère spécial --->
			<cfset roamingDetectionData[CODE_KEY()]=CHR(escapeAsciiCode) & payload[CODE_KEY()]>
			<!--- Stockage des données --->
			<cfset saveRoamingDetection(roamingDetectionData)>
		</cfif>
		<!--- Envoie au client la réponse HTTP par défaut de traitement avec succès --->
		<cfset SUPER.serviceRequest(DEFAULT_URI(),requestData)>
	</cffunction>
	
	<!--- Stockage des données --->
	
	<cffunction access="private" name="saveRoamingDetection" returntype="void" hint="Stocke le flux Roaming Detection au format JSON">
		<cfargument name="datalertPayload" type="Struct" required="true" hint="Données du flux">
		<cfset var currentDateString=lsDateFormat(NOW(),"DD_MM_YYYY")>
		<cfset var fileName="RoamingDetection_" & currentDateString & ".txt">
		<cfset var fileTitle="Datalert Roaming Detection data of " & NOW()>
		<cfset saveDataInFile(serializeJSON(ARGUMENTS.datalertPayload),fileName,fileTitle)>
	</cffunction>
</cfcomponent>