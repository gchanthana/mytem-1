<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.impl.Authentication" extends="fr.saaswedo.api.da.impl.Core"
hint="Service d'authentification de l'API. Les logins et mots de passe d'accès ne doivent pas contenir de caractère ':'
<br>La fonction handleHttpRequest() de ce composant est automatiquement appelée par l'application à chaque requete HTTP et avant toute autre traitement.
<br>Si elle retourne TRUE alors la ressource targetPage sera accédée. Sinon le traitement est arreté et une réponse HTTP est envoyée au client.
<br>Quand elle retourne TRUE alors l'envoi de la réponse HTTP au client doit etre effectuée par le traitement suivant">
	<cffunction access="private" name="checkRequest" returntype="Boolean" hint="Vérifie la présence et l'authenticité des crédentiels">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<!--- Par défaut les crédentiels sont considérés comme absents : Une demande d'authentification sera effectuée --->
		<cfset var httpCode=AUTHENTICATION_REQUIRED()>
		<cfset var jsonResponse={"Message"="Authentication required"}>
		<!--- Vérification effectuée par l'implémentation parent :
		- Si les données sont invalides : Renvoi une réponse HTTP au client et targetPage ne sera pas accédée
		- Sinon la vérification de la présence des crédentiels est effectuée ET un traitement spécifique est effectué s'ils sont absents
		--->
		<cfset var isRequestValid=SUPER.checkRequest(argumentCollection=ARGUMENTS)>
		<cfif isRequestValid>
			<cfset var log=GLOBALS().CONST('Datalert.LOG')>
			<cfset var clientInfo=getClientStringInfo()>
			<cfset var authorization=getAuthorization(requestData)>
			<!--- Les crédentiels sont présents : La vérification de l'authentification sera effectuée dans serviceRequest()--->
			<cfif TRIM(authorization) NEQ "">
				<cfset httpCode=AUTHORIZED()>
			</cfif>
			<!--- MAJ du statut de validité de la requete HTTP cliente : isRequestValid vaut TRUE avant cette affectation --->
			<cfset isRequestValid=(httpCode EQ AUTHORIZED())>
			<!--- Requete sans crédentiels : Les données sont invalides et ce traitement est spécifique à l'authentification --->
			<cfif NOT isRequestValid>
				<!--- L'absence des crédentiels est toujours mentionnée dans les logs --->
				<cflog type="warning" text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - Missing HTTP Basic credentials">
				<!--- Demande d'authentification --->
				<cfheader name="WWW-Authenticate" value="Basic realm=""saaswedo""">
				<cfset sendHttpResponse(httpCode,serializeJSON(jsonResponse))>
			<!--- Requete avec crédentiels : L'authentification est effectuée et un traitement spécifique est effectué s'il sont invalides --->
			<cfelse>
				<cfset var authorization=getAuthorization(requestData)>
				<cfset var login=getLogin(authorization)>
				<cfset var password=getPassword(authorization)>
				<cfset var isAuthenticated=authenticate(login,password)>
				<cfif NOT isAuthenticated>
					<cfset var logMessage="Authentication refused for login '#login#' and password '#password#'">
					<cfset httpCode=FORBIDDEN()>
					<!--- L'invalidité des crédentiels est toujours mentionnée dans les logs --->
					<cflog type="error"
					text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - #logMessage#">
					<!--- Notification des crédentiels invalides : Une exception est levée pour etre notifiée
					La réponse envoyée au client correspondra à l'echec d'authentification et non une erreur serveur
					--->
					<cftry>
						<cfthrow type="Custom" errorCode="DATALERT" message="Authentication refused for login '#login#' and password '#password#'">
						<cfcatch type="Any">
							<!--- Traitement de l'exception --->
							<cfset processServerError(targetURI,requestData,CFCATCH,"Datalert Authentication")>
							<!--- Réponse HTTP : Accès refusé (L'exception sert uniquement à effectuer la notification) --->
							<cfset jsonResponse["Message"]="Authentication failed">
							<cfset sendHttpResponse(httpCode,serializeJSON(jsonResponse))>
						</cfcatch>
					</cftry>
				</cfif>
				<!--- La ressource targetPage n'est pas accedée si l'authentification n'est pas réussie --->
				<cfreturn isAuthenticated>
			</cfif>
		</cfif>
		<cfreturn isRequestValid>
	</cffunction>
	
	<cffunction access="private" name="authenticate" returntype="Boolean" hint="Retourne TRUE si les crédentiels sont validés">
		<cfargument name="login" type="String" required="true" hint="Login">
		<cfargument name="password" type="String" required="true" hint="Password">
      <cfstoredproc datasource="ROCOFFRE" procedure="pkg_m26.validateLogin">
         <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.login#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.password#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="authStatus">
      </cfstoredproc>
		<cfreturn (authStatus EQ 1)>
	</cffunction>
</cfcomponent>