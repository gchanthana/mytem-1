<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.impl.Core" extends="fr.saaswedo.api.patterns.cfc.Component"
hint="Implémentation de base des services de l'API. Les traitements suivants sont effectuées en fonction des requetes HTTP clientes :
<br>- Fonction handleHttpRequest() : Traitement effectué pour une requete accédant à une ressource HTTP valide.
<br>- Fonction handleServerError() : Traitement effectué en cas d'erreur.
<br>- Fonction handleMissingResource() : Traitement effectué pour une ressource inexistante.
<br>Ces fonctions retournent TRUE en si le traitement a été effectué avec succès et FALSE sinon (e.g Données invalides dans la requete HTTP).
<br>Le code et la réponse HTTP renvoyés par ces fonctions dépendent du traitement effectué. Le Content-Type est toujours application/json
<br>Cette implémentation vérifie que la valeur header Content-Type envoyé par le client est application/json">
	<!--- fr.saaswedo.api.patterns.cfc.Component --->
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.patterns.cfc.Component" hint="Constructeur">
		<cfset SUPER.init()>
		<cfreturn THIS>
	</cffunction>

	<!--- =========== Constantes =========== --->
	<cffunction access="public" name="DB_JSON_DATA" returntype="String" hint="Clé ajoutée dans getHttpRequestData() et utilisée dans processServerError()
	<br>Elle est associée avec les données au format JSON qui sont envoyées aux traitements en base. Cela afin de faciliter le debug des erreurs">
		<cfreturn "DB_JSON_DATA">
	</cffunction>

	<!--- =========== Headers HTTP =========== --->

	<cffunction access="public" name="BASIC_AUTH_HEADER" returntype="String" hint="Retourne l'en-tete d'authentification HTTP Basic">
		<cfreturn "Authorization">
	</cffunction>

	<cffunction access="public" name="ACCEPT_HEADER" returntype="String" hint="Retourne l'en-tete HTTP Accept">
		<cfreturn "Accept">
	</cffunction>
	
	<cffunction access="public" name="UUID_HEADER" returntype="String" hint="Nom du header UUID">
		<cfreturn "UUID">
	</cffunction>
	<cffunction access="public" name="USER_AGENT_HEADER" returntype="String" hint="Nom du header User-Agent">
		<cfreturn "User-Agent">
	</cffunction>

	<!--- =========== Méthodes HTTP =========== --->

	<cffunction access="public" name="GET_METHOD" returntype="String" hint="Retourne la valeur pour la méthode HTTP GET">
		<cfreturn "GET">
	</cffunction>
	
	<cffunction access="public" name="POST_METHOD" returntype="String" hint="Retourne la valeur pour la méthode HTTP POST">
		<cfreturn "POST">
	</cffunction>
	
	<cffunction access="public" name="PUT_METHOD" returntype="String" hint="Retourne la valeur pour la méthode HTTP PUT">
		<cfreturn "PUT">
	</cffunction>
	
	<cffunction access="public" name="DELETE_METHOD" returntype="String" hint="Retourne la valeur pour la méthode HTTP DELETE">
		<cfreturn "DELETE">
	</cffunction>

	<!--- =========== Codes HTTP =========== --->

	<cffunction access="public" name="BAD_REQUEST" returntype="Numeric" hint="Code pour une requete invalide">
		<cfreturn 400>
	</cffunction>

	<cffunction access="public" name="AUTHENTICATION_REQUIRED" returntype="Numeric" hint="Code pour une authentification requise">
		<cfreturn 401>
	</cffunction>
	
	<cffunction access="public" name="FORBIDDEN" returntype="Numeric" hint="Code pour un accès refusé">
		<cfreturn 403>
	</cffunction>
	
	<cffunction access="public" name="NOT_FOUND" returntype="Numeric" hint="Code pour une ressource introuvable">
		<cfreturn 404>
	</cffunction>

	<cffunction access="public" name="SERVER_ERROR" returntype="Numeric" hint="Code pour une erreur interne">
		<cfreturn 500>
	</cffunction>
	
	<cffunction access="public" name="AUTHORIZED" returntype="Numeric" hint="Code pour une requete traitée avec succès">
		<cfreturn 200>
	</cffunction>
	
	<!--- =========== Implémentation de base du traitement des requetes HTTP clientes =========== --->
	<cffunction access="public" name="handleHttpRequest" returntype="Boolean" hint="Vérifie la validité des données de la requete HTTP cliente.
	<br>Si les données sont validées alors serviceRequest() est appelée pour effectuer le traitement correspondant et envoyer au client la réponse HTTP
	<br>Sinon une réponse HTTP informant des données invalides est envoyée au client.<br>Retourne TRUE quand les données sont valides et FALSE sinon">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<!--- Vérification et validation des données clientes : TRUE si elles sont valides et FALSE sinon --->
		<cfset var requestCheckStatus=checkRequest(argumentCollection=ARGUMENTS)>
		<cfif requestCheckStatus>
			<!--- Implémentation du service : N'est effectué que si les données clientes ont été validées par le traitement de vérification --->
			<cfset serviceRequest(argumentCollection=ARGUMENTS)>
		</cfif>
		<!--- Principe de la valeur retournée par les traitement des requetes clientes
		- Soit les données sont valides : La ressource targetPage sera accédée après cette fonction
		- Soit les données sont invalides :
			- Le traitement qui a effectué leur vérification doit renvoyer la réponse correspondante au client
			- Le traitement s'arrete après cette fonction
		--->
		<cfreturn requestCheckStatus>
	</cffunction>

	<!--- =========== Traitement des erreurs/exceptions qui est effectué par défaut en l'absence de TRY/CATCH =========== --->
	<cffunction access="public" name="handleServerError" returntype="void"
	hint="Effectue le traitement de l'exception exceptionObject puis envoi une réponse HTTP au client avec le Le code SERVER_ERROR()">
		<cfargument name="exceptionObject" type="Any" required="true" hint="Exception de type CFCATCH">
		<cfargument name="eventName" type="String" required="false" hint="Nom de l'évènement correspondant (Ce paramètre n'est pas utilisé ici)">
		<cfset var httpCode=SERVER_ERROR()>
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<cfset var jsonResponse={"Message"="Internal Error"}>
		<cfset var exceptionEventName=isDefined("ARGUMENTS.eventName") ? ARGUMENTS.eventName:"N.D">
		<cfset var targetURI=CGI.SCRIPT_NAME>
		<cfset var clientInfo=getClientStringInfo()>
		<!--- Traitement de l'exception --->
		<cfset processServerError(targetURI,getHttpRequestData(),ARGUMENTS.exceptionObject,exceptionEventName)>
		<!--- Réponse HTTP envoyée au client --->
		<cflog type="error" text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - #jsonResponse.Message#">
		<cfset sendHttpResponse(httpCode,serializeJSON(jsonResponse))>
	</cffunction>

	<cffunction access="public" name="handleMissingResource" returntype="Boolean" hint="Retourne TRUE et la réponse JSON correspondante au client HTTP">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfset var jsonResponse={"Message"="Resource not found"}>
		<cfset sendHttpResponse(NOT_FOUND(),serializeJSON(jsonResponse))>
		<cfreturn TRUE>
	</cffunction>
	
	<cffunction access="public" name="processServerError" returntype="void"
	hint="Effectue le traitement de l'exception exceptionObject sans envoyer de réponse HTTP au client">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource accédée par le client">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfargument name="exceptionObject" type="Any" required="true" hint="Exception de type CFCATCH">
		<cfargument name="eventName" type="String" required="false" hint="Nom de l'évènement correspondant (Ce paramètre n'est pas utilisé ici)">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var debugMode=isDebugMode()>
		<!--- Identité de l'application --->
		<cfset var appName="N.D">
		<cfif isDefined("APPLICATION") AND structKeyExists(APPLICATION,"applicationName")>
			<cfset var appName=APPLICATION.applicationName>
		</cfif>
		<!--- Traitement de l'erreur --->
		<cfset var errorTitle="Server Error">
		<cfset var errorLog=errorTitle>
		<cfset var clientInfo=getClientStringInfo()>
		<cfset var requestUUID="N.D">
		<cfset var cfCatchObject=ARGUMENTS.exceptionObject>
		<!--- Extraction de la cause de l'exception --->
		<cfif structKeyExists(cfCatchObject,"Cause")>
			<cfset cfCatchObject=cfCatchObject.Cause>
		</cfif>
		<cftry>
			<!--- Paramètres de la requete cliente --->
			<cfset var requestParams=getRequestParameters(requestData)>
			<!--- UUID (Numéro de ligne) --->
			<cfif structKeyExists(requestData,"headers")>
				<cfif structKeyExists(requestData.headers,"UUID")>
					<cfif LEN(TRIM(requestData.headers.UUID)) GT 0>
						<cfset requestUUID=requestData.headers.UUID>
					</cfif>
				</cfif>
			</cfif>
			<!--- Log de l'exception --->
			<cfset var log=GLOBALS().CONST('Datalert.LOG')>
			<cfif structKeyExists(cfCatchObject,"message")>
				<cfset errorLog=errorLog & " message[#cfCatchObject.message#]">
			</cfif>
			<cfif structKeyExists(cfCatchObject,"detail")>
				<cfset errorLog=errorLog & " detail[#cfCatchObject.detail#]">
			</cfif>
			<cflog type="error" application="true" text="[#log#][UUID:#requestUUID#] #errorLog#">
			<!--- Contenu de la notification de l'exception --->
			<cfsavecontent variable="body">
				<cfoutput>
					BackOffice (Debug: #debugMode#): #CGI.SERVER_NAME#:#CGI.SERVER_PORT#<br>
					Application : #appName#<br>
					Temp Directory : #getTempDirectory()#<br>
					URI : #targetURI#<br>
					Implementation : #componentName()#
					<hr>UUID (Datalert) : #requestUUID#<br>
					Client Info : #clientInfo#<br>
					Client HTTP Method : #CGI.REQUEST_METHOD#
					<cfif structKeyExists(requestData,DB_JSON_DATA())>
						<cfset var dbJsonData=requestData[DB_JSON_DATA()]>
						<hr>JSON Data sent to database :<br>
						<cfif isValid("String",dbJsonData)>
							<b>#dbJsonData#</b>
						<cfelse>
							<cfdump var="#dbJsonData#" format="text">
						</cfif>
					</cfif>
					<cfif structKeyExists(requestData,"headers")>
						<hr>HTTP Headers :<br><cfdump var="#requestData.headers#" format="text">
					</cfif>
					<hr>HTTP Parameters :<br><cfdump var="#requestParams#" format="text">
					<cfif structIsEmpty(requestParams)>
						<cfif structKeyExists(requestData,"content")>
							<hr>HTTP Request content :<br><cfdump var="#requestParams#" format="text">
						</cfif>
					</cfif>
					<hr><h3>Exception</h3>Message :<br>#cfCatchObject.message#
					<cfif LEN(TRIM(cfCatchObject.detail)) GT 0>
						<br>Detail : #cfCatchObject.detail#
					</cfif>
					<cfif structKeyExists(cfCatchObject,"TagContext")>
						<br>Tag context :<br><cfdump var="#cfCatchObject#" format="text">
					</cfif>
				</cfoutput>
			</cfsavecontent>
			<!--- Stockage de la notification --->
			<cfset var mLog=createObject('component','fr.consotel.api.monitoring.MLog')>
			<cfset mLog.setIDMNT_CAT(21)>
			<cfset mLog.setIDMNT_ID(161)>
			<cfset mLog.setUUID(requestUUID)>
			<cfset mlog.setSUBJECTID("[Datalert - Error] Server Error - UUID : #requestUUID#")>
			<cfset mLog.setBODY(body)>
			<cfset createObject('component','fr.saaswedo.api.da.monitoring.DatabaseLogger').logIt(mLog)>
			<!--- Traitement d'une exception durant la notification --->
			<cfcatch type="Any">
				<cfset errorLog="Error Handling FAILED. Cause: #CFCATCH.message# - Detail: #CFCATCH.detail#">
				<cflog type="error" application="true" text="[#log#][#targetURI#] #errorLog#">
				<cfsavecontent variable="body">
					An exception notification failed<br><br>
					<cfoutput>
						BackOffice (Debug: #debugMode#): #CGI.SERVER_NAME#:#CGI.SERVER_PORT#<br>
						Application : #appName#<br>
						Temp Directory : #getTempDirectory()#<br>
						URI : #targetURI#<br>
						UUID (Datalert) : #datalertUUID#<br>
						Client Info : #clientInfo#<br>
						Client HTTP Method : #CGI.REQUEST_METHOD#
					</cfoutput>
				</cfsavecontent>
				<!--- Un notification mail est effectuée car on peut considérer que ce type d'exception n'est pas fréquent --->
				<cfset var mailProperties={
					from=notificationFrom(),to=notificationTo(),server="mail-cv.consotel.fr",port="25",type="text/html"
				}>
			  	<cfmail attributeCollection="#mailProperties#" subject="[Datalert - Error] Unable to notify exception - UUID : #datalertUUID#">
				  	<cfoutput>#body#</cfoutput>
					<hr>Notification Failure :<br><cfdump var="#CFCATCH#" format="text">
				</cfmail>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- =========== Traitement des requetes HTTP clientes =========== --->
	
	<cffunction access="private" name="checkRequest" returntype="Boolean" hint="Appelée par handleHttpRequest() pour valider la requete HTTP cliente.
	Retourne TRUE si la requete est validée et FALSE sinon. Auquel cas (FALSE) cette fonction génére et renvoie la réponse HTTP correspondante.
	<br>Sinon (TRUE) cette fonction n'envoi pas de réponse HTTP au client et handleHttpRequest() appelera serviceRequest() pour suite du traitement. 
	<br>Cette implémentation vérifie que la valeur du header Accept est application/json sinon la requete client est considerée comme invalide">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<!--- Utilisés pour la réponse HTTP en cas d'invalidité de la requete HTTP cliente --->
		<cfset var httpCode=BAD_REQUEST()>
		<cfset var jsonResponse="">
		<!--- Vérification du header Accept --->
		<cfif hasHeaders(requestData)>
			<cfset var acceptValue=getHeaderValue(ACCEPT_HEADER(),requestData)>
			<!--- TODO : Vérification simple. Autrement il faudrait parcourir la liste des types spécifiés dans la valeur du header Accept --->
			<cfif REFind("application/json",acceptValue) GT 0>
				<cfset httpCode=AUTHORIZED()>
			</cfif>
		</cfif>
		<cfset var isRequestValid=(httpCode EQ AUTHORIZED())>
		<!--- Réponse HTTP en cas d'invalidité de la requete HTTP cliente --->
		<cfif NOT isRequestValid>
			<!--- Les cas d'invalidité de requete sont toujours mentionnés dans les logs --->
			<cfset var log=GLOBALS().CONST('Datalert.LOG')>
			<cfset var clientInfo=getClientStringInfo()>
			<cflog type="error" text="[#log#] HTTP #httpCode# returned to #clientInfo#. Cause: Invalid #ACCEPT_HEADER()# header #acceptValue#">
			<!--- Traitement des données invalides : Attention jsonResponse est de type Struct ici --->
			<cfset handleInvalidRequestData(targetURI,requestData,httpCode,jsonResponse)>
		</cfif>
		<!--- ATTENTION : handleInvalidRequestData() envoi une réponse HTTP au client donc elle n'est appelée que si isRequestValid vaut FALSE --->
		<cfreturn isRequestValid>
	</cffunction>
	
	<cffunction access="private" name="serviceRequest" returntype="void" hint="Appelée par handleHttpRequest() quand checkRequest() retourne TRUE.
	<br>Cette fonction doit etre implémentée effectuer le traitement des données validées et envoyer la réponse HTTP correspondant au résultat au client
	<br>Dans certains cas (e.g Authentification), cette fonction ne fait aucune action : Donc la ressource targetPage qui va etre accédée">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<!--- L'implémentation par défaut n'effectue aucune action : La ressource targetPage est donc simplement accédée --->
	</cffunction>
	
	<cffunction access="private" name="sendHttpResponse" returntype="void" hint="Envoi au client une réponse HTTP avec le contenu jsonContent">
		<cfargument name="httpCode" type="Numeric" required="true" hint="Code HTTP envoyé au client">
		<cfargument name="jsonContent" type="String" required="true" hint="Contenu de la réponse HTTP au format JSON">
		<cfheader statuscode="#ARGUMENTS.httpCode#">
		<cfheader name="Content-Type" value="application/json">
		<cfoutput>#TRIM(ARGUMENTS.jsonContent)#</cfoutput>
	</cffunction>
	
	<!--- =========== Traitement des données invalides (Requetes HTTP clientes) =========== --->
	<cffunction access="private" name="handleInvalidRequestData" returntype="void" hint="Traitement des données invalides">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfargument name="responseCode" type="Numeric" required="true" hint="Code HTTP envoyé au client suite au données invalides">
		<cfargument name="responsePayload" type="Struct" required="true" hint="Payload de la réponse HTTP qui sera envoyée au client">
		<cfset var log="N.D">
		<cfset var debugMode=isDebugMode()>
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var clientInfo=getClientStringInfo()>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var requestParams=getRequestParameters(requestData)>
		<cfset var httpCode=ARGUMENTS.responseCode>
		<cfset var jsonPayload=serializeJSON(ARGUMENTS.responsePayload)>
		<!--- Extraction de l'UUID (Numéro de ligne) --->
		<cfset var requestUUID="N.D">
		<cfif structKeyExists(requestData,"headers")>
			<cfif structKeyExists(requestData.headers,"UUID")>
				<cfif LEN(TRIM(requestData.headers.UUID)) GT 0>
					<cfset requestUUID=requestData.headers.UUID>
				</cfif>
			</cfif>
		</cfif>
		<!--- Contenu de la notification --->
		<cfsavecontent variable="body">
			<cfoutput>
				BackOffice (Debug: #debugMode#): #CGI.SERVER_NAME#:#CGI.SERVER_PORT#<br>
				URI : #targetURI#<br>
				Implementation : #componentName()#<br>
				Temp Directory : #getTempDirectory()#
				<hr>UUID (Datalert) : #requestUUID#<br>
				Client Info : #clientInfo#<br>
				Client HTTP Method : #CGI.REQUEST_METHOD#
				<hr>HTTP Response Code : #httpCode#<br>
				HTTP Response Content (JSON) :<br>#jsonPayload#
			</cfoutput>
			<cfif structKeyExists(requestData,"headers")>
				<hr>HTTP Headers :<br><cfdump var="#requestData.headers#" format="text">
			</cfif>
			<hr>HTTP Parameters :<br><cfdump var="#requestParams#" format="text">
			<cfif structIsEmpty(requestParams)>
				<cfif structKeyExists(requestData,"content")>
					<hr>HTTP Request content :<br><cfdump var="#requestParams#" format="text">
				</cfif>
			</cfif>
		</cfsavecontent>
		<!--- Stockage de la notification --->
		<cfset var mLog=createObject('component','fr.consotel.api.monitoring.MLog')>
		<cfset mLog.setIDMNT_CAT(21)>
		<cfset mLog.setIDMNT_ID(161)>
		<cfset mLog.setUUID(requestUUID)>
		<cfset mlog.setSUBJECTID("[Datalert - Error] Invalid Request Data - UUID : #requestUUID#")>
		<cfset mLog.setBODY(body)>
		<cfset createObject('component','fr.saaswedo.api.da.monitoring.DatabaseLogger').logIt(mLog)>
		<!--- Réponse HTTP envoyée au client
		- Soit aucune exception n'a été capturée jusqu'ici alors la réponse HTTP passée en paramètre est envoyée au client
		- Sinon il y a eu exception et en l'absence de TRY/CATCH ici alors le traitement par défaut handleServerError() sera effectué
		--->
		<cfset sendHttpResponse(httpCode,jsonPayload)>
	</cffunction>
	
	<!--- =========== Utilitaires d'extraction d'infos clientes =========== --->
	
	<cffunction access="private" name="getLogin" returntype="String" hint="Retourne le login contenu dans authorization">
		<cfargument name="authorization" type="String" required="true" hint="Valeur provenant de getAuthorization()">
		<cfset var credentials=getCredentials(ARGUMENTS.authorization)>
		<cfreturn listGetAt(credentials,1,":")>
	</cffunction>
	
	<cffunction access="private" name="getPassword" returntype="String" hint="Retourne le password contenu dans authorization">
		<cfargument name="authorization" type="String" required="true" hint="Valeur provenant de getAuthorization()">
		<cfset var credentials=getCredentials(ARGUMENTS.authorization)>
		<cfreturn listGetAt(credentials,2,":")>
	</cffunction>
	
	<cffunction access="private" name="getCredentials" returntype="String" hint="Retourne les crédentiels (login,password) contenus dans authorization.
	La valeur retournée par cette implémentation est la chaine login:password. Lève une exception si le format des crédentiels est invalide">
		<cfargument name="authorization" type="String" required="true" hint="Valeur provenant de getAuthorization()">
		<cfset var code=GLOBALS().CONST("ERROR.ERROR")>
		<cfset var basicAuth=ARGUMENTS.authorization>
		<cfset var basicAuthLen=LEN(basicAuth)>
		<cfset var minLength=LEN("Basic ")>
		<cfif basicAuthLen GT minLength>
			<cfset var userLogin="">
			<cfset var userPwd="">
			<cfset var base64Credentials=RIGHT(basicAuth,basicAuthLen - minLength)>
			<cfset var credentials=toString(toBinary(base64Credentials))>
			<cfset var credentialLen=listLen(credentials,":")>
			<!--- Soit le login et password sont fournis soit il n'y a que le login --->
			<cfif (credentialLen EQ 2) OR (credentialLen EQ 1)>
				<cfset userLogin=listGetAt(credentials,1,":")>
				<cfif credentialLen EQ 2>
					<cfset userPwd=listGetAt(credentials,2,":")>
				</cfif>
				<cfreturn userLogin & ":" & userPwd>
			<!--- Invalidité du format de la valeur des crédentiels --->
			<cfelseif credentialLen GT 2>
				<cfthrow type="Custom" errorcode="#code#" message="Credentials contains more thant one ':' character" detail="Credentials: '#credentials#'">
			<cfelse>
				<cfthrow type="Custom" errorcode="#code#" message="Unable to extract login/pwd from credentials" detail="Credentials: '#credentials#'">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="#code#" message="Authorization value has invalid length" detail="Authorization: '#basicAuth#'">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getAuthorization" returntype="String" hint="Retourne la valeur du header Authorization ou une chaine vide sinon">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfreturn getHeaderValue(BASIC_AUTH_HEADER(),ARGUMENTS.httpRequestData)>
	</cffunction>
	
	<cffunction access="private" name="getHeaderValue" returntype="String" hint="Retourne la valeur d'un header ou une chaine vide sinon">
		<cfargument name="header" type="String" required="true" hint="Nom du header HTTP à trouver dans httpRequestData">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfset var headerName=ARGUMENTS.header>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfif hasHeader(headerName,requestData)>
			<cfset var httpHeaders=getHeaders(requestData)>
			<cfreturn httpHeaders[headerName]>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>

	<cffunction access="private" name="hasHeader" returntype="Boolean" hint="Retourne TRUE si header est présent dans httpRequestData">
		<cfargument name="header" type="String" required="true" hint="Nom du header HTTP à trouver dans httpRequestData">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfset var headerName=ARGUMENTS.header>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var httpHeaders=getHeaders(requestData)>
		<cfreturn structKeyExists(httpHeaders,headerName)>
	</cffunction>
	
	<cffunction access="private" name="hasHeaders" returntype="Boolean" hint="Retourne TRUE si httpRequestData contient au moins un header">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfreturn structKeyExists(ARGUMENTS.httpRequestData,"headers")>
	</cffunction>
	
	<cffunction access="private" name="getHeaders" returntype="Struct" hint="Retourne les headers de la requete HTTP cliente ou une structure vide sinon">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfreturn hasHeaders(ARGUMENTS.httpRequestData) ? ARGUMENTS.httpRequestData.headers:{}>
	</cffunction>
	
	<!--- TODO : Le client HTTP peut avoir ses propres restrictions pour l'envoi des paramètres en fonction de la méthode HTTP --->
	<cffunction access="private" name="getRequestParameters" returntype="Struct" hint="Retourne les paramètres HTTP de la requete cliente comme suit :
	<br>- Si la méthode HTTP est POST : Retourne une copie du scope FORM (sans la clé FIELDNAMES)
	<br>- Sinon : Retourne une copie du scope URL
	<br>- Il est possible qu'une structure vide soit retournée en fonction de la manière utilisée par le client pour envoyer les paramètres.
	<br>Ex: Dans ColdFusion la méthode PUT ne supporte que les paramètres URL et BODY, la méthode GET uniquement ceux de type URL, etc...">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var httpMethod=getHttpMethod(requestData)>
		<cfif compareNoCase(httpMethod,POST_METHOD()) EQ 0>
			<cfset var postParameters=DUPLICATE(FORM)>
			<cfif structKeyExists(postParameters,"FIELDNAMES")>
				<cfset structDelete(postParameters,"FIELDNAMES")>
			</cfif>
			<cfreturn postParameters>
		<cfelse>
			<cfreturn DUPLICATE(URL)>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getRequestBody" returntype="String" hint="Retourne le contenu de la requete cliente (Body)">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var requestContent=requestData.content>
		<cfif isBinary(requestData.content)>
			<cfset requestContent=toString(requestData.content)>
		</cfif>
		<cfreturn requestContent>
	</cffunction>
	
	<cffunction access="private" name="getHttpMethod" returntype="String" hint="Retourne la méthode HTTP spécifiée dans httpRequestData (Par défaut GET)">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Requete HTTP cliente conforme au résultat de getHttpRequestData()">
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfif structKeyExists(requestData,"method")>
			<cfreturn requestData.method>
		<cfelse>
			<cfreturn GET_METHOD()>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getClientStringInfo" returntype="String" hint="Retourne une chaine contenant les infos concernant le client HTTP">
		<cfset var remoteHost="'#CGI.REMOTE_HOST#' (CGI) ">
		<cfset var clientInfo="[HTTP Method:#CGI.REQUEST_METHOD#][UserAgent:#CGI.HTTP_USER_AGENT#]">
		<cfif isDefined("COOKIE")>
			<cfif structKeyExists(COOKIE,"IP_ORIGINE")>
				<cfset remoteHost="#COOKIE.IP_ORIGINE# (IP_ORIGINE) ">
			</cfif>
		</cfif>
		<cfset clientInfo=remoteHost & clientInfo>
		<cfreturn clientInfo>
	</cffunction>
	
	<!--- =========== Utilitaires =========== --->
	
	<cffunction access="private" name="isValidUTC" returntype="Boolean" hint="Retourne TRUE si utcString est une date UTC (ISO 8601) valide">
		<cfargument name="utcString" type="String" required="true" hint="Date UTC (ISO 8601)">
		<cfset var utcDateString=ARGUMENTS.utcString>
		<!--- Problème : Une valeur comme 47182 est considérée comme valide. Solution : La valeur doit contenir exactement une lettre T --->
		<cfset var utcContainsOneT=REMatch("T{1}",utcDateString)>
		<cfif arrayLen(utcContainsOneT) EQ 1>
			<cfset var datatypeConverter=createObject("java","javax.xml.bind.DatatypeConverter")>
			<!--- Pour une raison inconnue l'appel à DatatypeConverter.parseDateTime() ne fonctionne ici que par introspection Java --->
			<cfset var datatypeConverterClass=createObject("java","javax.xml.bind.DatatypeConverter").getClass()>
			<cfset var stringClass=createObject("java","java.lang.String").getClass()>
			<cfset var parseMethod=datatypeConverterClass.getMethod("parseDateTime",[stringClass])>
			<cftry>
				<!--- Appel à DatatypeConverter.parseDateTime() par introspection Java --->
				<cfset var javaCalendar=parseMethod.invoke(datatypeConverter,[utcDateString])>
				<cfreturn TRUE>
				<cfcatch type="Any">
					<cfset var exceptionObject=CFCATCH>
					<!--- Extraction de la cause de l'exception : Exception Java --->
					<cfif structKeyExists(CFCATCH,"cause")>
						<cfset exceptionObject=CFCATCH.cause>
					</cfif>
					<!--- Type de l'exception Java levée par DatatypeConverter quand le format UTC est invalide --->
					<cfif structKeyExists(exceptionObject,"TYPE") AND (compare(exceptionObject.TYPE,"java.lang.IllegalArgumentException") EQ 0)>
						<cfset var log=GLOBALS().CONST('Datalert.LOG')>
						<cflog type="error" text="[#log#] Invalid UTC (ISO 8601) format : '#utcDateString#'">
						<cfreturn FALSE>
					<!--- Toute autre exception est levée à nouveau --->
					<cfelse>
						<cfrethrow>
					</cfif>
				</cfcatch>
			</cftry>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="utcToLocalDate" returntype="Date" hint="Retourne la date locale correspondant à utcString">
		<cfargument name="utcString" type="String" required="true" hint="Date UTC (ISO 8601)">
		<cfset var utcDateString=ARGUMENTS.utcString>
		<cfset var datatypeConverter=createObject("java","javax.xml.bind.DatatypeConverter")>
		<!--- Pour une raison inconnue l'appel à DatatypeConverter.parseDateTime() ne fonctionne ici que par introspection Java --->
		<cfset var datatypeConverterClass=createObject("java","javax.xml.bind.DatatypeConverter").getClass()>
		<cfset var stringClass=createObject("java","java.lang.String").getClass()>
		<cfset var parseMethod=datatypeConverterClass.getMethod("parseDateTime",[stringClass])>
		<!--- Appel à DatatypeConverter.parseDateTime() par introspection Java --->
		<cfset var javaCalendar=parseMethod.invoke(datatypeConverter,[utcDateString])>
		<cfreturn javaCalendar.getTime()>
	</cffunction>
	
	<cffunction access="private" name="isBase64" returntype="String" hint="Retourne TRUE si stringData est au format base64 et FALSE sinon">
		<cfargument name="base64data" type="String" required="true" hint="Chaine au format base64">
		<!--- ColdFusion 9.0.x (JRun4,Weblogic) :
		- Solution : http://commons.apache.org/proper/commons-codec/
		- Autre solution : Vérifier si toBinary() lève une exception ou pas
		--->
		<cfset var Base64=createObject("java","org.apache.commons.codec.binary.Base64")>
		<!--- Méthode disponible dans la version fournie avec ColdFusion 9.0.X --->
		<cfreturn Base64.isArrayByteBase64(ARGUMENTS.base64data.getBytes("UTF-8"))>
	</cffunction>
	
	<cffunction access="private" name="base64ToString" returntype="String" hint="Retourne la conversion de la valeur base64data en String">
		<cfargument name="base64data" type="String" required="true" hint="Chaine au format base64">
		<!--- ColdFusion 9 (JRun4,Weblogic) :
		- Solution : http://commons.apache.org/proper/commons-codec/
		- Autre solution (Java 6 SDK): http://docs.oracle.com/javase/6/docs/api/index.html?javax/xml/bind/DatatypeConverter.html
		--->
		<cfset var Base64=createObject("java","org.apache.commons.codec.binary.Base64")>
		<!--- Méthode disponible dans la version fournie avec ColdFusion 9.0.X --->
		<cfreturn createObject("java","java.lang.String").init(Base64.decodeBase64(ARGUMENTS.base64data.getBytes("UTF-8")))>
	</cffunction>
	
	<cffunction access="private" name="getFileSeparator" returntype="String" hint="Retourne la valeur de la propriété système Java : file.separator">
		<cfreturn createObject("java","java.lang.System").getProperty("file.separator")>
	</cffunction>

	<cffunction access="private" name="notificationFrom" returntype="String" hint="Retourne l'expéditeur mail des notifications">
		<cfreturn "no-reply@saaswedo.com">
	</cffunction>
	
	<cffunction access="private" name="notificationTo" returntype="String" hint="Retourne le destinataire mail des notifications">
		<cfreturn "nobody@saaswedo.com">
	</cffunction>
	
	<!--- Un caractère est utilisé comme contournement pour représenter les nombres en tant que chaines lors de la sérialisation JSON --->
	<cffunction access="private" name="jsonNumberEscapeChar" returntype="Numeric" hint="Retourne le code ASCII du caractère spécial utilisé pour
	représenter les nombres en tant que chaine lors d'une sérialisation JSON. Le caractère doit ensuite etre supprimé après la sérialisation">
		<!--- CARACTERE : ESPACE, HEX: 20, DECIMAL: 32 --->
		<cfreturn inputBaseN("20",16)>
	</cffunction>
</cfcomponent>