<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.impl.Service" extends="fr.saaswedo.api.da.impl.Core"
hint="A la différence de l'implémentation parent, ici la fonction serviceRequest() contient le traitement pour les données valides.
<br>L'utilisation de cette implémentation consiste à appeler la fonction handleHttpRequest() dans la ressource targetPage">
	<!--- =========== URI des services =========== --->
	
	<cffunction access="public" name="DEFAULT_URI" returntype="String" hint="URI du service par défaut">
		<cfreturn "/api/da/index.cfm">
	</cffunction>
	
	<!--- L'implémentation est différente de celle du parent : Il s'agit d'effectuer le traitement et envoyer la réponse HTTP correspondante --->
	<cffunction access="private" name="serviceRequest" returntype="void" hint="Renvoi une réponse HTTP de succès avec le code AUTHORIZED()
	<br>Cette réponse n'est envoyée que si targetPage vaut DEFAULT_URI() sinon aucune action n'est effectuée.
	La raison est que ce paramètre permet de retourner la réponse par défaut en cas succès sinon celle d'une autre implémentation si besoin
	<br>En cas d'erreur, il faut lever une exception pour que handleServerError() soit appelé automatiquement pour envoyer une réponse HTTP
	<br>Le header Content-Type est déjà envoyé au client par handleHttpRequest() et avant l'exécution de cette fonction">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfif compare(targetURI,DEFAULT_URI()) EQ 0>
			<cfset var requestData=ARGUMENTS.httpRequestData>
			<cfset var jsonResponse={"Message"="Payload accepted"}>
			<cfset var responseContent=serializeJSON(jsonResponse)>
			<cfset sendHttpResponse(AUTHORIZED(),responseContent)>
		</cfif>
	</cffunction>
	
	<!--- Utilitaires --->
	
	<cffunction access="private" name="inflateBase64Gzip" returntype="String" hint="Retourne le résultat de la décompression GZIP de deflatedBase64data.
	Lève une exception Java de type java.util.zip.DataFormatException si le format GZIP est invalide">
		<cfargument name="deflatedBase64data" type="String" required="true" hint="Données compressées au format GZIP et encodées au format base64">
		<!--- Taille fixe du tampon de décompression --->
		<cfset var DEFLATER_BUFFER_INIT_SIZE=64 * 1024>
		<cfset var inflatedString="">
		<!--- Décodage Base64 --->
		<cfset var base64data=ARGUMENTS.deflatedBase64data>
		<cfset var Base64=createObject("java","org.apache.commons.codec.binary.Base64")>
		<cfset var deflaterBuffer=Base64.decode(base64data.getBytes("UTF-8"))>
		<!--- Buffer de décompression de taille dynamique : La taille est une expression pour forcer le cast en nombre dans Java --->
		<cfset var outputStringBuffer=createObject("java","java.lang.StringBuffer").init(64 * 1024)>
		<!--- Tampon de décompression --->
		<cfset var ByteBuffer=createObject("java","java.nio.ByteBuffer").allocate(DEFLATER_BUFFER_INIT_SIZE)>
		<cfset var ByteBufferWithZero=createObject("java","java.nio.ByteBuffer").allocate(DEFLATER_BUFFER_INIT_SIZE)>
		<cfset var inflaterBuffer=ByteBuffer.array()>
		<!--- Décompression GZIP --->
		<cfset var hasMoreDataToInflate=TRUE>
		<cfset var inflater=createObject("java","java.util.zip.Inflater").init()>
		<cfset inflater.setInput(deflaterBuffer,0,arrayLen(deflaterBuffer))>
		<cfloop condition="hasMoreDataToInflate EQ TRUE">
			<cfset inflaterByteNb=inflater.inflate(inflaterBuffer)>
			<!--- Le nombre de bytes décompressés est utilisé dans la condition car il arrive que inflater.finished() ne retourne jamais TRUE --->
			<cfset hasMoreDataToInflate=(inflaterByteNb GT 0)>
			<cfif hasMoreDataToInflate>
				<cfset inflatedString=createObject("java","java.lang.String").init(inflaterBuffer,0,inflaterByteNb,"UTF-8")>
				<cfset outputStringBuffer.append(inflatedString)>
				<!--- Réinitialisation du buffer et de son contenu --->
				<cfset ByteBuffer.clear()>
				<cfset ByteBuffer.put(ByteBufferWithZero.array())>
				<cfset inflaterBuffer=ByteBuffer.array()>
			</cfif>
		</cfloop>
		<cfset inflater.end()>
		<cfreturn outputStringBuffer.toString()>
	</cffunction>
</cfcomponent>