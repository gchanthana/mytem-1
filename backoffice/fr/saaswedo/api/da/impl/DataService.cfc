<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.impl.DataService" extends="fr.saaswedo.api.da.impl.Service"
hint="Implémentation des fonctions communes aux services pour le traitement des données des requetes HTTP clientes (Post Authentification)">
	<!--- Clés correspondant à des champs métiers --->

	<cffunction access="public" name="UTS_KEY" returntype="String" hint="Champ UTS: Timestamp Unix local au terminal">
		<cfreturn "UTS">
	</cffunction>
	
	<cffunction access="public" name="DATA_KEY" returntype="String" hint="Champ Data: Payload JSON">
		<cfreturn "Data">
	</cffunction>
	
	<cffunction access="public" name="COMPLETE_USAGE_KEY" returntype="String" hint="Clé Data contenu dans le Payload du flux Complete Usage Upload">
		<cfreturn "DATA">
	</cffunction>
	<cffunction access="public" name="USER_AGENT_KEY" returntype="String" hint="Clé Data contenu dans le Payload du flux Complete Usage Upload">
		<cfreturn "USER_AGENT">
	</cffunction>

	<!--- =========== Codes HTTP =========== --->
	
	<cffunction access="public" name="MISSING_UUID_HEADER" returntype="Numeric" hint="Code pour l'absence de la clé UUID">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<cffunction access="public" name="INVALID_UUID_FORMAT" returntype="Numeric" hint="Code pour un UUID invalide">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<cffunction access="public" name="MISSING_UTS" returntype="Numeric" hint="Code pour l'absence de la clé UTS">
		<cfreturn BAD_REQUEST()>
	</cffunction>

	<cffunction access="public" name="INVALID_UTS" returntype="Numeric" hint="Code pour un UTS invalide">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<cffunction access="public" name="MISSING_DATA_FIELD" returntype="Numeric" hint="Code pour l'absence de la clé Data">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<cffunction access="public" name="INVALID_DATA_FORMAT" returntype="Numeric" hint="Code pour des données invalides (Payload)">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<cffunction access="public" name="MISSING_COMPLETE_USAGE" returntype="Numeric" hint="Code pour l'absence de la clé Data (Complete Usage)">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<cffunction access="public" name="INVALID_B64_COMPLETE_USAGE" returntype="Numeric" hint="Code pour des données invalides (Complete Usage)">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<cffunction access="public" name="INVALID_CODE" returntype="Numeric" hint="Code pour une valeur invalide de la clé Code">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	
	<cffunction access="private" name="checkRequest" returntype="Boolean" hint="Retourne TRUE si les conditions suivantes sont vérifiées :
	<br>- L'implémentation parent checkRequest() retourne TRUE avec les memes paramètres.
	<br>- La méthode HTTP est POST<br>- L'implémentation checkData() retourne TRUE avec les memes paramètres
	<br>Cette implémentation décrit les étapes de vérification. Celle des données métiers est déléguée à une fonction privée : checkData()">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<!--- Vérification de l'implémentation parent : Renvoi une réponse HTTP si la méthode est invalide --->
		<cfset var checkResult=SUPER.checkRequest(targetURI,requestData)>
		<cfif checkResult>
			<!--- Vérification de la méthode HTTP : Renvoi une réponse HTTP si la méthode est invalide --->
			<cfset var checkResult=checkRequestMethod(POST_METHOD(),requestData)>
			<cfif checkResult>
				<!--- Vérification des données métiers : Renvoi une réponse HTTP si les données sont invalides --->
				<cfset checkResult=checkData(targetURI,requestData)>
			</cfif>
		</cfif>
		<cfreturn checkResult>
	</cffunction>
	
	<cffunction access="private" name="checkData" returntype="Boolean" hint="Vérifie la validité des données métiers de la requete HTTP client.<br>
	Retourne TRUE si les données sont validées et FALSE sinon. Auquel cas le code et la réponse HTTP correspondante seront renvoyés au client HTTP<br>
	Cette fonction vérifie les données métiers :<br>- UUID: Header contenant l'UUID du device<br>- Data: Paramètre HTTP contenant le payload JSON">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var httpCode=AUTHORIZED()>
		<cfset var jsonResponse={"Message"="Bad HTTP request"}>
		<!--- Check 1 : Vérifie la présence du header UUID --->
		<cfif hasHeader(UUID_HEADER(),requestData)>
			<!--- Check 1.1 : Vérifie la valeur du header UUID --->
			<cfset var uuid=getHeaderValue(UUID_HEADER(),requestData)>
			<cfif TRIM(uuid) EQ "">
				<!--- Réponse HTTP : Si la vérification Check 1 n'est pas validée --->
				<cfset httpCode=INVALID_UUID_FORMAT()>
				<cfset jsonResponse={"Message"="UUID : bad format"}>
			</cfif>
		<cfelse>
			<!--- Réponse HTTP : Si la vérification Check 1 n'est pas validée --->
			<cfset httpCode=MISSING_UUID_HEADER()>
			<cfset jsonResponse={"Message"="UUID not present in the HTTP header."}>
		</cfif>
		<!--- Si les vérifications Check 1 et Check 1.1 ont été validées --->
		<cfif httpCode EQ AUTHORIZED()>
			<!--- Check 2 : Vérifie la présence du paramètre HTTP : Data --->
			<cfset var requestParams=getRequestParameters(requestData)>
			<cfset var areDataDefined=structKeyExists(requestParams,DATA_KEY())>
			<cfif NOT areDataDefined>
				<!--- Réponse HTTP : Si la vérification Check 2 n'est pas validée --->
				<cfset httpCode=MISSING_DATA_FIELD()>
				<cfset jsonResponse={"Message"="Data usage not present in the Payload."}>
			</cfif>
		</cfif>
		<!--- MAJ du statut de validité de la requete HTTP cliente --->
		<cfset var isClientRequestValid=(httpCode EQ AUTHORIZED())>
		<!--- Réponse HTTP : Si une des vérifications n'est pas validée --->
		<cfif NOT isClientRequestValid>
			<!--- Les cas d'invalidité de requete sont toujours mentionnés dans les logs --->
			<cfset var log=GLOBALS().CONST('Datalert.LOG')>
			<cfset var clientInfo=getClientStringInfo()>
			<cflog type="error" text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - #jsonResponse.message#">
			<!--- Traitement des données invalides : Envoi une réponse HTTP au client --->
			<cfset handleInvalidRequestData(targetURI,requestData,httpCode,jsonResponse)>
		</cfif>
		<cfreturn isClientRequestValid>
	</cffunction>
	
	<cffunction access="private" name="checkRequestMethod" returntype="Boolean" hint="Vérifie que la méthode HTTP de httpRequestData est requiredMethod.
	Retourne TRUE si c'est le cas et FALSE sinon. Auquel cas le code et la réponse HTTP correspondante seront renvoyés au client HTTP<br>
	Cette fonction n'est pas utilisée dans les vérifications effectuées par checkRequest() : Toute implémentation doit l'appeler explicitement si besoin">
		<cfargument name="requiredMethod" type="String" required="true" hint="Méthode HTTP">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var expectedMethod=ARGUMENTS.requiredMethod>
		<cfset var httpMethod=getHttpMethod(ARGUMENTS.httpRequestData)>
		<cfif compareNoCase(httpMethod,expectedMethod) NEQ 0>
			<cfset var httpCode=BAD_REQUEST()>
			<cfset var jsonResponse={"Message"="Bad HTTP request"}>
			<!--- Les cas d'invalidité de requete sont toujours mentionnés dans les logs --->
			<cfset var log=GLOBALS().CONST('Datalert.LOG')>
			<cfset var clientInfo=getClientStringInfo()>
			<cfset var targetURI=CGI.SCRIPT_NAME>
			<cflog type="error" text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - Invalid HTTP Method #httpMethod#">
			<!--- Traitement des données invalides : Envoi une réponse HTTP au client --->
			<cfset var requestData=getHttpRequestData()>
			<cfset handleInvalidRequestData(targetURI,requestData,httpCode,jsonResponse)>
			<cfreturn FALSE>
		<cfelse>
			<cfreturn TRUE>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getPayload" returntype="Struct" hint="Retourne le payload : Valeur du paramètre HTTP Data ou une structure vide">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var requestParams=getRequestParameters(requestData)>
		<cfif structKeyExists(requestParams,DATA_KEY()) AND isJSON(requestParams[DATA_KEY()])>
			<cfset var jsonPayload=deserializeJSON(requestParams[DATA_KEY()])>
			<cfreturn jsonPayload>
		<cfelse>
			<cfreturn {}>
		</cfif>
	</cffunction>
	
	<!--- Traitement de données métiers : Stockage en base --->
	<cffunction access="private" name="storeUsageInDatabase" returntype="void" hint="Stocke les données d'un flux au format JSON dans la base">
		<cfargument name="procedure" type="String" required="true" hint="Nom complet de la procédure stockée">
		<cfargument name="datalertPayload" type="String" required="true" hint="Données du flux au format JSON">
		<cfset var targetURI=CGI.SCRIPT_NAME>
		<cfset var procedureName=ARGUMENTS.procedure>
		<cfset var clientInfo=getClientStringInfo()>
		<!--- Traitement des données : La base supprimera le caractère spécial qui a été ajouté en préfixe --->
		<cftry>
	      <cfstoredproc datasource="ROCOFFRE" procedure="#procedureName#">
	         <cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#ARGUMENTS.datalertPayload#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="status">
	      </cfstoredproc>
	      <cfif status LT 0>
				<cfset var log=GLOBALS().CONST('Datalert.LOG')>
				<cflog type="error" text="[#log#][#targetURI#] #clientInfo# - Stored Procedure #procedureName# returned #status#">
				<cfthrow type="Custom" errorCode="DATALERT" message="#procedureName# returns #status#" detail="Data will be stored using another method">
	      </cfif>
			<cfcatch type="Any">
				<cfthrow type="Custom" errorCode="DATALERT" message="Stored Procedure : #procedureName# - #CFCATCH.message#" detail="#CFCATCH.detail#">
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- Utilitaires --->
	
	<cffunction access="private" name="isValidMCCMNC" returntype="Boolean" hint="Retourne TRUE si mccMnc est une valeur MCC/MNC valide et FALSE sinon">
		<cfargument name="mccMnc" type="String" required="true" hint="Valeur au format MCC/MNC">
		<cfset var valueToCheck=ARGUMENTS.mccMnc>
		<!--- Le code MCC fait 3 chiffres et le MNC fait 1 ou 3 chiffres --->
		<cfset var codeIsValid=(arrayLen(REMatch("([0-9]+){0,6}",valueToCheck)) EQ 1)>
		<cfset var result = (codeIsValid AND ((LEN(valueToCheck) gte 0) AND (LEN(valueToCheck) lte 6)))>
		<cfreturn result>
	</cffunction>
	
	<cffunction access="private" name="saveDataInFile" returntype="void" hint="Stocke un flux dans un fichier au format texte">
		<cfargument name="datalertPayload" type="String" required="true" hint="Données du flux au format JSON">
		<cfargument name="dataFilename" type="String" required="true" hint="Nom du fichier de stockage incluant son extension si besoin">
		<cfargument name="dataTitle" type="String" required="true" hint="En-tête du fichier de stockage">
		<cfset var payloadToStore=ARGUMENTS.datalertPayload>
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<!--- Verrou du répertoire de stockage : Un répertoire pour tous les fichiers de données --->
		<cfset var DATALERT_LOCK="DATALERT_LOCK">
		<!--- Verrou du fichier de stockage : Un fichier par jour --->
		<cfset var FILE_LOCK="RD_LOCK">
		<!--- Répertoire de stockage  --->
		<cfset var datalertDir=getTempDirectory() & "DATALERT">
		<!--- Création du répertoire de stockage --->
		<cflock name="#DATALERT_LOCK#" type="exclusive" timeout="10" throwontimeout="true">
			<cfif NOT directoryExists(datalertDir)>
				<cfset directoryCreate(datalertDir)>
			</cfif>
		</cflock>
		<!--- Stockage des données --->
		<cflock name="#FILE_LOCK#" type="exclusive" timeout="20" throwontimeout="true">
			<cfset var charset="UTF-8">
			<cfset var currentDateString=lsDateFormat(NOW(),"DD_MM_YYYY")>
			<cfset var fileName=ARGUMENTS.dataFilename>
			<cfset var filePath=datalertDir & getFileSeparator() & fileName>
			<!--- Création du fichier de stockage (1 fichier de données généré par jour) --->
			<cfif NOT fileExists(filePath)>
				<cfset var fileDesc=fileOpen(filePath,"write",charset)>
				<cfset var initialContent=ARGUMENTS.dataTitle & CHR(10) & repeatString("=",80) & CHR(10) & CHR(10)>
				<cfset fileWrite(fileDesc,initialContent)>
				<cfset fileClose(fileDesc)>
			</cfif>
			<!--- Ajout de données dans le fichier --->
			<cfset var fileDesc=fileOpen(filePath,"append",charset)>
			<cfset fileWrite(fileDesc,payloadToStore & CHR(10) & CHR(10) & repeatString("=",80) & CHR(10) & CHR(10))>
			<cfset fileClose(fileDesc)>
			<cflog type="information" text="[#log#][#CGI.SCRIPT_NAME#] Datalert data file #filePath# CREATED or UPDATED">
		</cflock>
	</cffunction>
</cfcomponent>