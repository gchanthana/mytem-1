<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.impl.Billing" extends="fr.saaswedo.api.da.impl.DataService" hint="Service de Billing Plan">
	<!--- =========== URI des services =========== --->
	
	<cffunction access="public" name="BILLING_PLAN_URI" returntype="String" hint="URI du service Get User Plan (Billing)">
		<cfreturn "/api/da/plan.cfm">
	</cffunction>
	
	<!--- Clés correspondant à des champs métiers --->
	<cffunction access="public" name="STRATEGY" returntype="String" hint="Clé pour strategy">
		<cfreturn "strategy">
	</cffunction>
		
	<cffunction access="public" name="DEVID" returntype="String" hint="Clé pour devId">
		<cfreturn "devId">
	</cffunction>

	<cffunction access="public" name="CALERTS" returntype="String" hint="Clé pour CAlerts">
		<cfreturn "CAlerts">
	</cffunction>
	
	<cffunction access="public" name="UALERTS" returntype="String" hint="Clé pour UAlerts">
		<cfreturn "UAlerts">
	</cffunction>
	
	<cffunction access="public" name="PTYPE" returntype="String" hint="Clé pour PTYPE">
		<cfreturn "pType">
	</cffunction>
	
	<cffunction access="public" name="ISROAM" returntype="String" hint="Clé pour ISROAM">
		<cfreturn "isRoam">
	</cffunction>
	
	<cffunction access="public" name="CLIM" returntype="String" hint="Clé pour CLIM">
		<cfreturn "cLim">
	</cffunction>
	
	<cffunction access="public" name="KBLIM" returntype="String" hint="Clé pour kbLim">
		<cfreturn "kbLim">
	</cffunction>
	
	<cffunction access="public" name="FUP" returntype="String" hint="Clé pour FUP">
		<cfreturn "FUP">
	</cffunction>
	
	<cffunction access="public" name="BUC" returntype="String" hint="Clé pour BUC">
		<cfreturn "BUC">
	</cffunction>
	
	<cffunction access="public" name="BILLING_DATE" returntype="String" hint="Clé pour Billing Date">
		<cfreturn "BDate">
	</cffunction>
	
	<cffunction access="public" name="RCAP" returntype="String" hint="Clé pour Roaming Cap">
		<cfreturn "RCap">
	</cffunction>

	<cffunction access="public" name="MONTHLY_CAP" returntype="String" hint="Clé pour Monthly Cap">
		<cfreturn "MCap">
	</cffunction>

	<cffunction access="public" name="IS_DEFAULT" returntype="String" hint="Colonne IS_DEFAULT (1 si c'est la plan par défaut et 0 sinon)">
		<cfreturn "IS_DEFAULT">
	</cffunction>
	
	<cffunction access="public" name="PHONE_NO" returntype="String" hint="Clé pour Phone Number (phone_no)">
		<cfreturn "phone_no">
	</cffunction>
	
	<cffunction access="public" name="CFM_NUM" returntype="String" hint="Clé pour cfm_Num (Contournement d'un bug chez Datami)">
		<cfreturn "cfmNum">
	</cffunction>

	<!--- =========== Codes HTTP =========== --->
	
	<cffunction access="public" name="PLAN_NOT_FOUND" returntype="Numeric" hint="Code pour l'absence de Plan Info pour la ligne">
		<cfreturn NOT_FOUND()>
	</cffunction>
	
	<cffunction access="public" name="MISSING_PHONE_NO" returntype="Numeric" hint="Code pour l'absence de la clé PHONE_NO()">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<cffunction access="public" name="INVALID_PHONE_NO" returntype="Numeric" hint="Code pour une valeur invalide de la clé PHONE_NO()">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<!--- =========== Implémentation du service =========== --->
	
	<cffunction access="private" name="serviceRequest" returntype="void" hint="Renvoi la réponse correspondant à un traitement effectué avec succès">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var requestParams=getRequestParameters(requestData)>
		<cfset var phoneNumber=requestParams[PHONE_NO()]>
		<!--- Ces paramètres HTTP sont factulatifs --->
		<cfset var devId=structKeyExists(requestParams,DEVID()) ? requestParams[DEVID()]:"">
		<cfset var strategyId=structKeyExists(requestParams,STRATEGY()) ? VAL(requestParams[STRATEGY()]):0>
		<cfset httpCode=AUTHORIZED()>
		<cfset var jsonResponse={}>
		<cfset var billingPlan=getBillingPlan(phoneNumber,devId,strategyId)>
		<!--- Notification en cas de plan par défaut : Aucun plan n'est affecté à la ligne --->
		<cfif structKeyExists(billingPlan,IS_DEFAULT())>
			<cfset var isDefault=VAL(billingPlan[IS_DEFAULT()][1])>
			<!--- Plan par défaut : Signifie que la ligne n'a pas de plan affecté --->
			<cfif isDefault EQ 1>
				<cfset var clientInfo=getClientStringInfo()>
				<cflog type="warning" text="[#log#][#targetURI#] No Billing Plan found for '#phoneNumber#' from #clientInfo#">
				<!--- Contournement d'un bug chez Datami quand aucun Billing plan n'est affecté à la ligne --->
				<cfset httpCode=NOT_FOUND()>
				<cfset jsonResponse={"Message"="Billing Plan not found"}>
			<cfelse>


				<!--- Les valeurs sont entières et ne doivent pas dépasser java.lang.Integer.MAX_VALUE --->
				<cfset jsonResponse[BILLING_DATE()]=javaCast("int",VAL(billingPlan[BILLING_DATE()][1]))>
				<cfset jsonResponse[RCAP()]=javaCast("int",VAL(billingPlan[RCAP()][1]))>
				<cfset jsonResponse[MONTHLY_CAP()]=javaCast("int",VAL(billingPlan[MONTHLY_CAP()][1]))>
				<cfset jsonResponse[FUP()]=javaCast("int",VAL(billingPlan[FUP()][1]))>


				<cfset jsonResponse[BUC()]=javaCast("int",VAL(billingPlan[BUC()][1]))>
				<cfset jsonResponse[CALERTS()]=deSerializeJSON(billingPlan[CALERTS()][1])>
				<cfset jsonResponse[UALERTS()]=deSerializeJSON(billingPlan[UALERTS()][1])>






			</cfif>





		<cfelse>

			<cfthrow type="Custom" errorcode="#code#" message="Invalid Billing Plan: #IS_DEFAULT()# column is missing">
		</cfif>
		<!--- Réponse HTTP --->
		<cfset var responseContent=serializeJSON(jsonResponse)>
		<!--- Stockage du log : targetPage est remplacé par CGI.SCRIPT_NAME 
		<cfset logProcessing(CGI.SCRIPT_NAME,requestData,httpCode,responseContent)>
		--->
		<cfset sendHttpResponse(httpCode,responseContent)>
	</cffunction>
	
	<cffunction access="private" name="getBillingPlan" returntype="Query" hint="Retourne les infos Billing Plan de la ligne">
		<cfargument name="phoneNumber" type="String" required="true" hint="Numéro de ligne">
		<cfargument name="devId" type="String" required="true" hint="Device ID. N'est pas envoyé à la base si chaine vide">
		<cfargument name="strategy" type="Numeric" required="true" hint="0 si devId doit etre envoyé à la base sinon seul phoneNumber le sera">
		<cfset var phone=ARGUMENTS.phoneNumber>
		<cfset var deviceId=ARGUMENTS.devId>
		<cfset var strategyId=ARGUMENTS.strategy>
		<cfset var code=GLOBALS().CONST("ERROR.ERROR")>
		<cfset var procedureName="pkg_m26.getUserPlanInfo"> 
      <cfstoredproc datasource="ROCOFFRE" procedure="#procedureName#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#phone#">
			<cfif (strategyId GT 0) AND (LEN(TRIM(deviceId)) GT 0)>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#deviceId#">
			</cfif>
			<cfprocresult name="billingPlan">
      </cfstoredproc>
      <cfdump var="#billingPlan#" output="console" >
		<cfif billingPlan.recordcount GT 0>
			<cfif structKeyExists(billingPlan,BILLING_DATE()) AND structKeyExists(billingPlan,RCAP())
			AND structKeyExists(billingPlan,MONTHLY_CAP()) AND structKeyExists(billingPlan,FUP()) AND structKeyExists(billingPlan,BUC())
			AND structKeyExists(billingPlan,CALERTS()) AND structKeyExists(billingPlan,UALERTS())>
				<cfif isJSON(billingPlan[CALERTS()][1]) AND isJSON(billingPlan[UALERTS()][1])>
					<cfreturn billingPlan>
				<!--- Une exception est levée si les colonnes CALERTS() et UALERTS() ne sont pas au format JSON --->
				<cfelse>
					<cfset var columnList=CALERTS() & "," & UALERTS()>
					<cfthrow type="Custom" errorcode="#code#" message="The value of one of the following column is not JSON in #procedureName# : #columnList#">
				</cfif>
			<!--- Une exception est levée si la requete Billing Plan ne retourne pas toutes les colonnes requises --->
			<cfelse>
				<cfset var columnList=arrayToList([BILLING_DATE(),RCAP(),MONTHLY_CAP(),FUP(),BUC(),CALERTS(),UALERTS()],",")>
				<cfthrow type="Custom" errorcode="#code#" message="One of the following columns are missing in #procedureName# : #columnList#">	
			</cfif>
		<!--- Une exception est levée si la requete Billing Plan ne retourne aucun enregistrement --->
		<cfelse>
			<cfthrow type="Custom" errorcode="#code#" message="No Data returned by Billing Plan query. Procedure : #procedureName#">
		</cfif>
	</cffunction>
		
	<!--- =========== Implémentation du traitement des données des requetes HTTP clientes =========== --->
	
	<cffunction access="private" name="checkData" returntype="Boolean" hint="Effectue les vérifications métiers pour le flux correspondant à targetPage.
	Cette fonction vérifie les données métiers :<br>- phone_no: Le paramètre HTTP PHONE_NO() est fourni dans la requete">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var httpCode=AUTHORIZED()>
		<cfset var phoneNumber="UNDEFINED">
		<cfset var jsonResponse={"Message"="Bad HTTP request"}>
		<!--- Vérification de la présence du paramètre HTTP : PHONE_NO() --->
		<cfset var requestParams=getRequestParameters(requestData)>
		<cfset var areDataDefined=structKeyExists(requestParams,PHONE_NO())>
		<cfif NOT areDataDefined>
			<cfset httpCode=MISSING_PHONE_NO()>
			<cfset jsonResponse={"Message"="#PHONE_NO()# is not present"}>
		<cfelse>
			<!--- Vérifie le format de la valeur du paramètre PHONE_NO() --->
			<cfset phoneNumber=requestParams[PHONE_NO()]>
			<cfset var isDataFormatValid=(TRIM(phoneNumber) EQ phoneNumber) AND (LEN(TRIM(phoneNumber)) GT 0)>
			<cfif NOT isDataFormatValid>
				<cfset httpCode=INVALID_PHONE_NO()>
				<cfset jsonResponse={"Message"="Invalid #PHONE_NO()#"}>
			</cfif>
		</cfif>
		<!--- MAJ du statut de validité de la requete HTTP cliente --->
		<cfset var isClientRequestValid=(httpCode EQ AUTHORIZED())>
		<!--- Réponse HTTP : Si une des vérifications n'est pas validée --->
		<cfif NOT isClientRequestValid>
			<!--- Les cas d'invalidité de requete sont toujours mentionnés dans les logs --->
			<cfset var log=GLOBALS().CONST('Datalert.LOG')>
			<cfset var clientInfo=getClientStringInfo()>
			<cflog type="error" text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - #jsonResponse.message# [phone_no: '#phoneNumber#']">
			<!--- Traitement des données invalides : Envoi une réponse HTTP au client --->
			<cfset handleInvalidRequestData(targetURI,requestData,httpCode,jsonResponse)>
		</cfif>
		<cfreturn isClientRequestValid>
	</cffunction>
</cfcomponent>