<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.impl.v1.Usage" extends="fr.saaswedo.api.da.impl.Usage"
hint="Implémentation utilisant CFTHREAD. Sur Solaris : java.util.ConcurrentModificationException">
	<!--- TODO : Implémentation de test de stockage des données (2 threads par requete HTTP cliente)
	Log d'erreur possible dans Tomcat : "the response object has been recycled and is no longer associated with this facade"
	Provoqué par des fonctions ColdFusion ou certaines instructions Java :
	ColdFusion ToString(), ColdFusion lsDateFormat(), java.text.SimpleDateFormat.format()
	Solution : Utiliser des instructions qui ne provoquent pas cette erreur (e.g Passer le résultat dans les attributs CFTHREAD) --->
	<cffunction access="private" name="saveUsageData" returntype="void" hint="Stocke les données reçues pour le flux Complete Usage Upload">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var payload=getPayload(requestData)>
		<!--- L'UUID est utilisé dans le nom du thread pour l'identifier de manière unique --->
		<cfset var threadId=getHeaderValue(UUID_HEADER(),requestData)>
		<cfset var coThread="DATALERT-USG-" & threadId>
		<cfset var threadMonitor="DATALERT-USG-MGR-" & threadId>
		<!--- Date utilisée dans le nom du fichier : 1 fichier de données généré par jour --->
		<cfset var currentDateString=lsDateFormat(NOW(),"DD_MM_YYYY")>
		<!--- Thread de traitement des données : Stocke les données (Toute exception est reportée par threadManager) --->
		<cfthread action="run" name="#coThread#" parameters="#ARGUMENTS#" currentDateString="#currentDateString#">
			<!--- Délai pour s'assurer que threadMonitor démarre avant la fin de coThread --->
			<cfset sleep(500)>
			<!--- Verrou du répertoire de stockage : Un répertoire pour tous les fichiers de données --->
			<cfset var DATALERT_LOCK="DATALERT_LOCK">
			<cfset var datalertDir=getTempDirectory() & "DATALERT">
			<cflock name="#DATALERT_LOCK#" type="exclusive" timeout="10" throwontimeout="true">
				<cfif NOT directoryExists(datalertDir)>
					<cfset directoryCreate(datalertDir)>
				</cfif>
			</cflock>
			<cfset var charset="UTF-8">
			<!--- Nom du fichier de sauvegarde --->
			<cfset var fileName="">
			<!--- Données --->
			<cfset var storeData=FALSE>
			<cfset var initialContent="">
			<cfset var stringData="">
			<cfset var uuid=getHeaderValue(UUID_HEADER(),parameters.httpRequestData)>
			<cfset var payload=getPayload(parameters.httpRequestData)>
			<!--- Verrou du fichier de stockage : Un fichier par jour --->
			<cfset var FILE_LOCK="">
			<!--- Cumulative Usage Upload --->
			<cfif compare(parameters.targetPage,CUMULATIVE_USAGE_URI()) EQ 0>
				<cfset initialContent="UUID;CRU;CDU;CWU;UTS" & CHR(10)>
				<cfset stringData=uuid & ";" & payload[CRU_KEY()] & ";" &
				payload[CDU_KEY()] & ";" & payload[CWU_KEY()] & ";" & payload[UTS_KEY()] & CHR(10)>
				<cfset fileName="CU_" & currentDateString & ".csv">
				<cfset FILE_LOCK="CU_LOCK">
				<cfset storeData=TRUE>
			<!--- Complete Application Usage Upload --->
			<cfelseif compare(parameters.targetPage,COMPLETE_APP_USAGE_URI()) EQ 0>
				<cfset initialContent="UUID;APP;CRU;CDU;CWU;UTS" & CHR(10)>
				<cfset stringData=uuid & ";" & payload[APP_KEY()] & ";" & payload[CRU_KEY()] & ";" &
				payload[CDU_KEY()] & ";" & payload[CWU_KEY()] & ";" & payload[UTS_KEY()] & CHR(10)>
				<cfset fileName="CO_APP_" & currentDateString & ".csv">
				<cfset FILE_LOCK="CO_APP_LOCK">
				<cfset storeData=TRUE>
			<!--- Complete Usage Upload --->
			<cfelseif compare(parameters.targetPage,COMPLETE_USAGE_URI()) EQ 0>
				<cfset initialContent="">
				<cfset var payloadData=payload[COMPLETE_USAGE_KEY()]>
				<!--- Les données sont au format CSV --->
				<cfset stringData=base64ToString(payloadData) & CHR(10)>
				<cfset fileName="CO_" & currentDateString & ".txt">
				<cfset FILE_LOCK="CO_LOCK">
				<cfset storeData=TRUE>
			</cfif>
			<!--- Création du fichier --->
			<cfset var log=GLOBALS().CONST('Datalert.LOG')>
			<cfif storeData>
				<cflock name="#FILE_LOCK#" type="exclusive" timeout="30" throwontimeout="true">
					<cfset var filePath=datalertDir & getFileSeparator() & fileName>
					<cfif NOT fileExists(filePath)>
						<cfset var fileDesc=fileOpen(filePath,"write",charset)>
						<cfset fileWrite(fileDesc,initialContent)>
						<cfset fileClose(fileDesc)>
					</cfif>
					<!--- Ajout de données dans le fichier --->
					<cfset var fileDesc=fileOpen(filePath,"append",charset)>
					<cfset fileWrite(fileDesc,stringData)>
					<cfset fileClose(fileDesc)>
					<cflog type="information" text="[#log#] #parameters.targetPage# - Thread '#THREAD.NAME#' : #filePath# CREATED/UPDATED">
				</cflock>
			<cfelse>
				<cflog type="warning" text="[#log#] #parameters.targetPage# - Thread '#THREAD.NAME#' : storeData value is #storeData#">
			</cfif>
		</cfthread>
		<!--- Thread de monitoring : Attend la fin du thread de sauvegarde des données et envoi un mail si le statut est TERMINATED --->
		<cfthread action="run" name="#threadMonitor#" coThread="#coThread#" targetPage="#ARGUMENTS.targetPage#">
			<cfthread action="join" name="#coThread#" timeout="30000"/>
			<cfset var coThreadScope=CFTHREAD[coThread]>
			<cfif coThreadScope.STATUS NEQ "COMPLETED">
				<cfset var mailTo=notificationTo()>
				<cfset var mailSubject="Complete Usage Upload - Thread Monitor ('#THREAD.NAME#')">
				<cfmail type="html" from="#notificationFrom()#" to="#mailTo#" subject="[Datalert] #mailSubject#">
					<cfoutput>
							BackOffice : #CGI.SERVER_NAME#<br>
							Implémentation : #componentName()#<br>
							targetPage : #targetPage#<br>
							Client Info : #getClientStringInfo()#
					</cfoutput><hr>
					<cfdump var="#coThreadScope#" label="Complete Usage Upload - Thread ('#coThread#')" format="text">
				</cfmail>
			</cfif>
		</cfthread>
	</cffunction>
</cfcomponent>