<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.app.Datalert" hint="Implémentation de ColdFusion Application API pour Datalert! RT
<br>L'authentification est implémentée dans l'API mais elle n'est initiée que par ce composant via onRequestStart()">
	<!--- =========== ColdFusion Application API Handlers =========== --->
	
	<cffunction access="public" name="onRequestStart" returntype="Boolean" hint="Handler du début de traitement d'une requete">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<!--- Authentification : Effectuée à chaque requete HTTP
		Retourne TRUE si targetPage sera accédée après le traitement et FALSE si aucune action n'est effectuée après le traitement --->
		<cfset var service=getAuthenticationService()>
		<cfset var httpRequestData=getHttPRequestData()>
		<cfreturn service.handleHttpRequest(ARGUMENTS.targetPage,httpRequestData)>
	</cffunction>
	
	<cffunction access="public" name="onMissingTemplate" returntype="Boolean" hint="Handler de ressource introuvable">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<!--- Authentification : Effectuée à chaque requete HTTP
		Retourne TRUE si targetPage sera accédée après le traitement et FALSE si aucune action n'est effectuée après le traitement --->
		<cfset var service=getAuthenticationService()>
		<cfset var httpRequestData=getHttPRequestData()>
		<cfreturn service.handleMissingResource(ARGUMENTS.targetPage,httpRequestData)>
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="ON_ERROR handler">
		<cfargument name="exceptionObject" type="Any" required="true" hint="Implémentation de l'exception correspondante">
		<cfargument name="eventName" type="String" required="false" hint="Nom de l'évènement correspondant">
		<cfset var service=new fr.saaswedo.api.da.impl.Core()>
		<!--- Traitement de l'erreur --->
		<cfset service.handleServerError(argumentCollection=ARGUMENTS)>
	</cffunction>
	
	<!--- =========== API Datalert! RT =========== --->
	<cffunction access="private" name="getAuthenticationService" returntype="fr.saaswedo.api.da.impl.Authentication" hint="Service d'authentification">
		<cfreturn new fr.saaswedo.api.da.impl.Authentication()>
	</cffunction>
	
	<!--- =========== Utilitaires d'application et de session =========== --->
	
	<cffunction access="private" name="getApplicationName" returntype="String" hint="Retourne le nom de l'application applicationScope">
		<cfargument name="applicationScope" type="Any" required="true" hint="Scope Application">
		<cfset var appName="N.D">
		<cfif structKeyExists(ARGUMENTS.applicationScope,"applicationName")>
			<cfset appName=ARGUMENTS.applicationScope.applicationName>
		</cfif>
		<cfreturn appName>
	</cffunction>
	
	<cffunction access="private" name="getSessionId" returntype="String" hint="Retourne l'identifiant de la session sessionScope">
		<cfargument name="sessionScope" type="Any" required="true" hint="Scope Session">
		<cfset var sessionId="N.D">
		<cfif structKeyExists(ARGUMENTS.sessionScope,"SESSIONID")>
			<cfset sessionId=ARGUMENTS.sessionScope.SESSIONID>
		</cfif>
		<cfreturn sessionId>
	</cffunction>
</cfcomponent>