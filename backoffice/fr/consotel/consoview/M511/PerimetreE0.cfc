<cfcomponent output="false">	
	
	<cffunction name="setUpPerimetre" access="public">
		<cfargument name="idNewPerimetre" type="numeric" required="true" default="-1">		
		<cftry>
			<cfif structKeyExists(SESSION.PERIMETRE,"OLDPERIMETRE") and idNewPerimetre eq SESSION.PERIMETRE.OLDPERIMETRE>				
				<cfset a1 = StructInsert(SESSION.PERIMETRE, "HAS_CHANGED",0, 1)> 									
				<cfreturn 0>
			<cfelse>
				<cfset niveau = getNiveauPerimetre(session.perimetre.ID_PERIMETRE)>
				<cfset a = StructInsert(SESSION.PERIMETRE, "OLDPERIMETRE",idNewPerimetre, 1)>
				<cfset a1 = StructInsert(SESSION.PERIMETRE, "HAS_CHANGED",1, 1)>
				<cfset a2 = StructInsert(SESSION.PERIMETRE, "ID_ORGANISATION",getIdOrga(session.perimetre.ID_PERIMETRE), 1)>								
				<cfset a3 = StructInsert(SESSION.PERIMETRE, "NIVEAU",niveau, 1)>	
				<cfset a5 = StructInsert(SESSION.PERIMETRE, "ID_NIVEAU",getIdNiveau(niveau), 1)>												
				<cfset a4 = StructInsert(SESSION.PERIMETRE, "ID_LAST_CLICHE",getLastCliche(SESSION.PERIMETRE.ID_ORGANISATION), 1)>								
				<cfreturn 1>
			</cfif>
			<cfcatch>
				<cfreturn -1>		
			</cfcatch>
		</cftry>
	</cffunction>
	
	
<!---
	GLOBAL STATIC s
--->
	<cfset dataSourceName = "BI_TEST"><!--- A CHANGER EN FONCTION DU DATASOURCE --->
 
<!---
	FUNCTION PUBLIC
--->

	<cffunction name="createWebService" output="true" access="public" returntype="any">
		<cfset ws = CreateObject(	"webservice",
										"http://sun-bi.consotel.fr/analytics/saw.dll?WSDL",
										"SAWSessionServiceSoap",
										"--NStoPkg com.siebel.analytics.web/soap/v5=com.siebel.analytics.web.soap.v5")>
		 <cfreturn ws>
	</cffunction>
	
	<cffunction name="logOn" output="true" access="public" returntype="string">
		<cfargument name="ws" type="any">
			<cftry>
				<cfset sessionId = ws.logon("production","prod")>
				<cfcatch>
					<cfset sessionId = '0'>
				</cfcatch>
			</cftry>
		<cfreturn sessionId>
	</cffunction>
	
	<cffunction name="getQuery" output="true" access="public">
			<cfargument name="source" type="string" required="true">
			<cfargument name="seesionID" type="string">
			<cfset res = CreateObject(	"webservice",
										"http://sun-bi.consotel.fr/analytics/saw.dll?WSDL",
										"ReportEditingServiceSoap")>
			
			<cfset reportRef = zcreateComplexObject(source,seesionID)>
			<cfset reportParams = structNew()>
 			<cfset query = res.generateReportSQL(reportRef, reportParams, seesionID)>
		<cfreturn query>
	</cffunction>


	<cffunction name="executeQuery" output="true" access="public" returntype="Query" >
			<cfargument name="theQuery" type="string">
						
			<cfset var queryrslt="NULL">			
			
			<cfquery datasource="#dataSourceName#" name="queryrslt">
							
				set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#SESSION.PERIMETRE.ID_ORGANISATION#,
								p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,								 
								p_idcliche=#SESSION.PERIMETRE.ID_LAST_CLICHE#;
			 	 #theQuery#
			</cfquery>
			
		<cfreturn queryrslt>
	</cffunction>

	<cffunction name="logOff" output="true" access="public">
			<cfargument name="seesionID" type="string">
			<cfset ws.logoff(seesionID)>
	</cffunction>

<!---
	FUNCTION PRIVATE
--->

	<cffunction name="zcreateComplexObject" output="true">
		<cfargument name="source" type="string" required="true">
		<cfargument name="sessionId" type="string" required="true">
		
			<cfset complexObject = structNew()>
			<cfset wcs = CreateObject(	"webservice",
										"http://sun-bi.consotel.fr/analytics/saw.dll?WSDL",
										"WebCatalogServiceSoap")>
													
			<cfswitch expression="#source#">
				<cfcase value="ONE">
					<!--- ACCEUIL --->
					<cfset path = "/users/production/Rapports/page_d_accueil_evolution_des_couts_par_segments_par_periode">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
				
				
				
				<cfcase value="TWO">
					<!--- THEME --->
					<cfset path = "/users/production/Rapports/Page_d_accueil_Integration_des_Factures">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
				
				<cfcase value="THREE">
					<!--- SURTHEME --->
					<cfset path = "/users/production/Rapports/page_d_accueil_Repartition_des_couts_par_segments_par_periode">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
				
			</cfswitch>
				
		<cfreturn complexObject>
	</cffunction>
	
	<cffunction name="zgetIdPeriodeMois" output="true" access="private" returntype="Numeric">
		<cfargument name="thisDate" type="String" required="true" hint="yyyy/mm/dd">
		<cfset var tmpDate = parseDateTime(ARGUMENTS.thisDate,"yyyy/mm/dd")>
		<cfset var tmpDateString = MID(lsDateFormat(tmpDate,"mm/yyyy"),1,7)>
		<cfquery datasource="#dataSourceName#" name="qGetIdperiodeMois">	
		 	 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS
		 	 FROM	E0_AGGREGS
		 	 WHERE	PERIODE.SHORT_MOIS='#tmpDateString#'
		 	 ORDER BY IDPERIODE_MOIS
		</cfquery>
		<cfif qGetIdperiodeMois.recordcount EQ 1>
			<cfreturn qGetIdperiodeMois["IDPERIODE_MOIS"][1]>
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>
	
	<cffunction name="getIdOrga" output="false" access="private" returntype="Any">
		<cfargument name="idgroupe_client" type="numeric" required="true">		
		
		<cfquery datasource="#SESSION.OFFREDSN#" name="rsltOrga">
			SELECT PKG_CV_RAGO.GET_ORGA(#idgroupe_client#) as IDORGA FROM DUAL
		</cfquery>
		<cfif rsltOrga.recordCount gt 0>
		
			<cfif rsltOrga['IDORGA'][1] gt 0>
				<cfreturn rsltOrga['IDORGA'][1]>
			<cfelse>
				<cfreturn SESSION.PERIMETRE.ID_GROUPE>
			</cfif>
			
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>
	
	<cffunction name="getNiveauPerimetre" output="false" access="private" returntype="string">
		<cfargument name="idgroupe_client" type="numeric" required="true">			 		
		
		<cfquery datasource="#SESSION.OFFREDSN#" name="rsltNiveau">
			SELECT 
			niv.NUMERO_NIVEAU
			FROM 
			groupe_client gc,
			orga_niveau niv 
			WHERE gc.idgroupe_client = #idgroupe_client#
			AND niv.idorga_niveau = gc.idorga_niveau
		</cfquery>
		
		<cfif rsltNiveau.recordCount gt 0>		
			<cfif rsltNiveau['NUMERO_NIVEAU'][1] neq ''>
				<cfreturn rsltNiveau['NUMERO_NIVEAU'][1]>
			<cfelse>
				<cfreturn 'B'>
			</cfif>
		<cfelse>
			<cfreturn 'B'>
		</cfif>
	</cffunction>
	
	<cffunction name="getIdNiveau" output="false" access="private" returntype="numeric">		
		<cfargument name="niveau" type="string" required="true">			 				
		<cfstoredproc procedure="pkg_consoview_v3.niveau_to_integer" datasource="#SESSION.OFFREDSN#">	
			<cfprocparam type="in" value="#niveau#" variable="p_niveau" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="out"  variable="p_idniveau" cfsqltype="CF_SQL_iNTEGER">
		</cfstoredproc>
		<cfreturn p_idniveau>
	</cffunction>
	
	
	
	<cffunction name="getLastCliche" output="true" access="private" returntype="Any">
		<cfargument name="idorga" type="numeric" required="true">		
		<cfquery datasource="E0" name="clicheRslt">
			SELECT idcliche FROM 
			( SELECT c.idcliche,c.idperiode_mois,MAX(c.idperiode_mois) over () MAX_p FROM cliche c WHERE c.idorganisation=#idorga# ) 
			WHERE max_p=idperiode_mois
		</cfquery>
			
		<cfif clicheRslt.recordCount gt 0>
			<cfreturn clicheRslt['IDCLICHE'][1]>
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>

	
	
	
	<cffunction name="zgetIdPeriodeMois2" output="false" access="private" returntype="Query">
		<cfargument name="thisDate" type="String" required="true">
				<cfquery datasource="#SESSION.OFFREDSN#" name="rsltIdPeriodeMois">
					SELECT CV_GET_IDPERIODE_MOIS(#thisDate#) FROM DUAL
				</cfquery>
				<cfabort showerror="#rsltIdPeriodeMois#">
		<cfreturn rsltIdPeriodeMois>
	</cffunction>
	
	<cffunction name="zGetCliche" output="true" access="private" returntype="Any">
		<cfargument name="idorga" type="numeric" required="true">
			<cfquery datasource="E0" name="clicheRslt">
				SELECT idcliche FROM 
				( SELECT c.idcliche,c.idperiode_mois,MAX(c.idperiode_mois) over () MAX_p FROM cliche c WHERE c.idorganisation=#idorga# ) 
				WHERE max_p=idperiode_mois
			</cfquery>
		<cfreturn clicheRslt['IDCLICHE'][1]>
	</cffunction>
	
</cfcomponent>