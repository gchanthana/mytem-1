<cfcomponent name="JournalDemandesEnRetard">
	<cffunction name="afficher"> 						
		<cfsetting enablecfoutputonly="true"/>
		<cfset label="Journal">	
		<cfset qGetGrid=createObject("component","fr.consotel.consoview.util.utils").getQuery("session.dataInventaire.journal")>				
		<cfset NewLine = Chr(13) & Chr(10)>		
		<cfheader name="Content-Disposition" value="inline;filename=rapport.csv">
		<cfcontent type="text/plain">
		<cfset societeObjet = createObject("component","fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.societe")>
		<cfoutput>Libellé de la commande;Distributeur/Agence;Opérateur distribué;Contact chez le distributeur;Date de la commande;Date de livraison prévue;Commentaire;#NewLine#</cfoutput>				<!--- <cfoutput>
			#TRIM(LIBELLE_COMMANDE)#;#TRIM(LIBELLE_OPERATIONS)#;#TRIM(NOMOP)#;#TRIM(CONTACT)#;#TRIM(LSDateFormat(createDate(left(DATE_COMMANDE,4),mid(DATE_COMMANDE,6,2),mid(DATE_COMMANDE,9,2)),"dd/mm/yyyy"))#;#TRIM(LSDateFormat(createDate(left(DATE_LIVRAISON_PREVUE,4),mid(DATE_LIVRAISON_PREVUE,6,2),mid(DATE_LIVRAISON_PREVUE,9,2)),"dd/mm/yyyy"))#;#TRIM(COMMENTAIRES)#;#NewLine#
		</cfoutput>	 --->
		<cfloop query="qGetGrid"><cfset societe = societeObjet.getFormCommande(IDCDE_COMMANDE)><cfif len(DATE_LIVRAISON_PREVUE) gt 0 ><cfset dateLivraisonPrevue = TRIM(LSDateFormat(createDate(left(DATE_LIVRAISON_PREVUE,4),mid(DATE_LIVRAISON_PREVUE,6,2),mid(DATE_LIVRAISON_PREVUE,9,2)),"dd/mm/yyyy"))><cfelse><cfset dateLivraisonPrevue = ""></cfif><cfoutput>#TRIM(LIBELLE_COMMANDE)#;#TRIM(societe.RAISON_SOCIALE)#;#TRIM(NOMOP)#;#TRIM(CONTACT)#;#TRIM(LSDateFormat(createDate(left(DATE_COMMANDE,4),mid(DATE_COMMANDE,6,2),mid(DATE_COMMANDE,9,2)),"dd/mm/yyyy"))#;#dateLivraisonPrevue#;#TRIM(COMMENTAIRES)#;#NewLine#</cfoutput></cfloop>
	</cffunction>    		 
</cfcomponent>			
