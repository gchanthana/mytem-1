<cfcomponent name="JournalOperationAvecRetard">	
	<cffunction name="afficher"> 						
		<cfsetting enablecfoutputonly="true"/>
		<cfset label="Journal">		
		<cfset qGetGrid=createObject("component","fr.consotel.consoview.util.utils").getQuery("session.dataInventaire.journal")>				
		<cfset NewLine = Chr(13) & Chr(10)>		
		<cfheader name="Content-Disposition" value="inline;filename=rapport.csv">
		<cfcontent type="text/plain">
		<cfoutput>Type opération;Libellé opération;N°opération;Dernière Action;Effectué par;Dernier état;Durée réf. état;Retard(jours);#NewLine#</cfoutput>		
		<cfoutput query="qGetGrid">#TRIM(TYPE_OPERATION)#;#TRIM(LIBELLE_OPERATIONS)#;#TRIM(NUMERO_OPERATION)#;#TRIM(LSDateFormat(createDate(left(DATE_ACTION,4),mid(DATE_ACTION,6,2),mid(DATE_ACTION,9,2)),"dd/mm/yyyy"))#;#TRIM(LOGIN_NOM)# #TRIM(LOGIN_PRENOM)#;#TRIM(LIBELLE_ETAT)#;#TRIM(DUREE_ETAT)#;#TRIM(RETARD)#;#NewLine#</cfoutput>	
	</cffunction>    		 
</cfcomponent>			
		