<cfcomponent name="JournalRessourcesResilieesFacturees">
	<cffunction name="afficher"> 				
		<cfsetting enablecfoutputonly="true" />
		<cfset label="Journal">		
		<cfset qGetGrid=createObject("component","fr.consotel.consoview.util.utils").getQuery("session.dataInventaire.journal")>				
		<cfset NewLine = Chr(13) & Chr(10)>		
		<cfheader name="Content-Disposition" value="inline;filename=rapport.csv">
		<cfcontent type="text/plain">
		<cfoutput>Compte de facturation;Opérateur;Produit;ligne;Date de resiliation;N°facture;Date d' émission de la facture;Période facturée;Montant facturé (H.T.);Etat;Type opération;Libellé opération;N°operation#NewLine#</cfoutput>		
		<cfoutput query="qGetGrid">#TRIM(COMPTE_FACTURATION)#;#TRIM(OPNOM)#;#TRIM(LIBELLE_PRODUIT)#;#TRIM(SOUS_TETE)#;#TRIM(LSDateFormat(createDate(left(DATE_ACTION,4),mid(DATE_ACTION,6,2),mid(DATE_ACTION,9,2)),"dd/mm/yyyy"))#;#TRIM(NUM_FACTURE)#;#TRIM(LSDateFormat(createDate(left(DATE_EMISSION,4),mid(DATE_EMISSION,6,2),mid(DATE_EMISSION,9,2)),"dd/mm/yyyy"))#;#TRIM(LSDateFormat(createDate(left(DATEDEB,4),mid(DATEDEB,6,2),mid(DATEDEB,9,2)),"dd/mm/yyyy"))# - #TRIM(LSDateFormat(createDate(left(DATEFIN,4),mid(DATEFIN,6,2),mid(DATEFIN,9,2)),"dd/mm/yyyy"))#;#TRIM(MONTANT)#;#TRIM(LIBELLE_ETAT)#;#TRIM(TYPE_OPERATION)#;#TRIM(LIBELLE_OPERATIONS)#;#TRIM(NUMERO_OPERATION)#;#NewLine#</cfoutput>	
	</cffunction>	
</cfcomponent>

