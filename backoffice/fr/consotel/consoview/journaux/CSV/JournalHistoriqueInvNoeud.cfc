<cfcomponent name="JournalHistoriqueInvNoeud">
	<cffunction name="afficher"> 						
		<cfsetting enablecfoutputonly="true" />
		<cfset label="Journal">		
		<cfset qGetGrid=createObject("component","fr.consotel.consoview.util.utils").getQuery("session.dataInventaire.journal")>				
		<cfset NewLine = Chr(13) & Chr(10)>		
		<cfheader name="Content-Disposition" value="inline;filename=rapport.csv">
		<cfcontent type="text/plain">
		<cfoutput>Ligne;produit;Opérateur;Action;Date Action;Dans inventaire;Etat#NewLine#</cfoutput>		
		<cfoutput query="qGetGrid">#TRIM(SOUS_TETE)#;#TRIM(LIBELLE_PRODUIT)#;#TRIM(OPNOM)#;#TRIM(ACTION)#;#TRIM(LSDateFormat(createDate(left(DATE_ACTION,4),mid(DATE_ACTION,6,2),mid(DATE_ACTION,9,2)),"dd/mm/yyyy"))#;#TRIM(DANS_INVENTAIRE)#;#TRIM(ETAT)##NewLine#</cfoutput>	
	</cffunction>	
</cfcomponent>