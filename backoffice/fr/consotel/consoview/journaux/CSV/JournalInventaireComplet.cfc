<cfcomponent name="JournalInventaireComplet">	
	<cffunction name="afficher">		
		<cfsetting enablecfoutputonly="true" />
		<cfset label="Journal">		
		
		<cfset qGetGrid=createObject("component","fr.consotel.consoview.inventaire.cycledevie.Journaux").getListeInventaire(PERIMETRE,DATE_REFERENCE)>
		
		<cfset NewLine = Chr(13) & Chr(10)>
		<cfset di = "">
		<cfheader name="Content-Disposition" value="inline;filename=rapport.csv" charset="iso-8859-1">
		
		<cfcontent type="text/csv">
		<cfoutput>Opérateur;Thème;Segment;Produit;Entree inv.;Sortie inv.;Dans inventaire;Sous reference;Compte;Sous compte;Ligne;Prix Unitaire(euro H.T);#NewLine#</cfoutput>			
		<cfoutput query="qGetGrid"><cfif DANS_INVENTAIRE eq 1><cfset di='OUI'><cfelse><cfset di='NON'></cfif>#TRIM(OPNOM)#;#TRIM(THEME_LIBELLE)#;#TRIM(SEGMENT_THEME)#;#TRIM(LIBELLE_PRODUIT)#;#TRIM(LSDATEFORMAT(ENTREE_INVENTAIRE,"DD/MM/YYYY"))#;#TRIM(LSDATEFORMAT(SORTIE_INVENTAIRE,"DD/MM/YYYY"))#;#TRIM(di)#;#TRIM(CODE_INTERNE)#;#TRIM(COMPTE_FACTURATION)#;#TRIM(SOUS_COMPTE)#;#SOUS_TETE#;#TRIM(PRIX_UNIT)##NewLine#</cfoutput>
	</cffunction>			 
</cfcomponent>