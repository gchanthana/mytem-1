<cfcomponent name="JournalReclamationEnCours">	
	<cffunction name="afficher"> 						
		<cfsetting enablecfoutputonly="true" />
		<cfset label="Journal">
		
		<cfset qGetGrid=createObject("component","fr.consotel.consoview.inventaire.cycledevie.Journaux").getListeOperationReclamationsEnCours(PERIMETRE)>
		
		<cfset NewLine = Chr(13) & Chr(10)>		
		<cfheader name="Content-Disposition" value="inline;filename=rapport.csv" charset="iso-8859-1">
		<cfcontent type="text/csv">
		<cfoutput>Type Operation;Libellé Operation;N°Operation;Date Creation;Dernière Action;Effectué par;Dernier état#NewLine#</cfoutput>		
		<cfoutput query="qGetGrid">#TRIM(TYPE_OPERATION)#;#TRIM(LIBELLE_OPERATIONS)#;#TRIM(NUMERO_OPERATION)#;#TRIM(LSDateFormat(createDate(left(DATE_CREATION,4),mid(DATE_CREATION,6,2),mid(DATE_CREATION,9,2)),"dd/mm/yyyy"))#;#TRIM(LSDateFormat(createDate(left(DATE_ACTION,4),mid(DATE_ACTION,6,2),mid(DATE_ACTION,9,2)),"dd/mm/yyyy"))#;#TRIM(LOGIN_NOM)# #TRIM(LOGIN_PRENOM)#;#TRIM(LIBELLE_ETAT)##NewLine#</cfoutput>	
	</cffunction>
</cfcomponent>  
