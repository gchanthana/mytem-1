<table align="center" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><cfoutput>Historique du noeud #session.datainventaire.noeud.LBL#</cfoutput></strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">
								
								<th>&nbsp;Libellé du produit&nbsp;</th>
								
								<th>&nbsp;Ligne&nbsp;</th>				
								
								<th>&nbsp;Operateur&nbsp;</th>
								 
								<th>&nbsp;Action&nbsp&nbsp;</th>
								
								<th>&nbsp;Date action&nbsp;</th>	
								 
								<th>&nbsp;Dans inventaire&nbsp;</th>
								
								<th>&nbsp;Etat&nbsp;</th>						
															
							</tr>
<cfset numRow = 1>
<cfoutput query="qGetGrid">	
	<cfif (numRow mod 30) eq 0>
<tr><td colspan="22" height="1" bgcolor="808080"></td></tr>	
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>
		<cfdocumentitem type="pagebreak"/>		
<table align="center" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong>Commande avec retard au #Lsdateformat(now(), 'dd mmmm yyyy')#</strong></td>
				</tr>
				<tr>
					<td valign="top">					
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">
																 
								<th>&nbsp;Libellé du produit&nbsp;</th>
								
								<th>&nbsp;Ligne&nbsp;</th>				
								
								<th>&nbsp;Operateur&nbsp;</th>
								 
								<th>&nbsp;Action&nbsp&nbsp;</th>
								
								<th>&nbsp;Date action&nbsp;</th>	
								 
								<th>&nbsp;Dans inventaire&nbsp;</th>
								
								<th>&nbsp;Etat&nbsp;</th>			
															
							</tr>
		<cfset numRow = numRow +1>
	<cfelse>				
					<cfif (numRow mod 2) eq 0>
							<tr>								
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#LIBELLE_PRODUIT#&nbsp;</td>								
								 										
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#SOUS_TETE#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#NOMOP#&nbsp;</td>								
								 
								<td class="grid_normal_alt" align="center" valign="top">&nbsp;#ACTION#&nbsp;</td>
								
								<td class="grid_normal_alt" align="center" valign="top">
									<cfif len(DATE_ACTION) gt 0 >
										&nbsp;#LSDateFormat(createDate(left(DATE_ACTION,4),mid(DATE_ACTION,6,2),mid(DATE_ACTION,9,2)),'dd/mm/yyyy')#&nbsp;
								 	</cfif>
								</td>
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#DANS_INVENTAIRE#&nbsp;</td>	
								 
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#ETAT#&nbsp;</td>
															
							</tr>
						<cfelse>
							<tr>								
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#LIBELLE_PRODUIT#&nbsp;</td>								
								 										
								<td class="grid_normal" align="right" valign="top">&nbsp;#SOUS_TETE#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#NOMOP#&nbsp;</td>								
								 
								<td class="grid_normal" align="center" valign="top">&nbsp;#ACTION#&nbsp;</td>
								
								<td class="grid_normal" align="center" valign="top">
									<cfif len(DATE_ACTION) gt 0 >
										&nbsp;#LSDateFormat(createDate(left(DATE_ACTION,4),mid(DATE_ACTION,6,2),mid(DATE_ACTION,9,2)),'dd/mm/yyyy')#&nbsp;
								 	</cfif>
								</td>
								<td class="grid_normal" align="right" valign="top">&nbsp;#DANS_INVENTAIRE#&nbsp;</td>	
								 
								<td class="grid_normal" align="right" valign="top">&nbsp;#ETAT#&nbsp;</td>
															
							</tr>
						</cfif>							 					
							
		<cfset numRow = numRow +1>		
	</cfif>
</cfoutput>
						 					
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>