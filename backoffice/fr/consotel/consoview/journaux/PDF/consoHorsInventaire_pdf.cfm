<table align="center" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><cfoutput>Consommations hors inventaire <br>#INFO_FACTURE#</cfoutput></strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
														
							<tr align="center">
								
								<th>&nbsp;Compte de facturation&nbsp;</th>
								
								<th>&nbsp;Opérateur&nbsp;</th>																
								
								<th>&nbsp;Produit&nbsp;</th>	
								
								<th>&nbsp;Ligne&nbsp;</th>	
									
								<th>&nbsp;Nombre d'appels&nbsp;</th>
									
								<th>&nbsp;Volume (mn ou ko)&nbsp;</th>
																																
								<th>&nbsp;Montant facturé (H.T.)&nbsp;</th>
																
								<th>&nbsp;N°facture&nbsp;</th>	
								
								<th>&nbsp;Date émission facture&nbsp;</th>	
																
								<th>&nbsp;Période facturée&nbsp;</th>								
																							
							</tr>
<cfset numRow = 1>
<cfoutput query="qGetGrid">	
	<cfif (numRow mod 21) eq 0>
							<tr><td colspan="22" height="1" bgcolor="808080"></td></tr>	
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>
<cfdocumentitem type="pagebreak"/>
<table align="center" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><cfoutput>Consommations hors inventaire <br>#INFO_FACTURE#</cfoutput></strong></td>
				</tr>
				<tr>
					<td valign="top">
	 					<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">
								
								<th>&nbsp;Compte de facturation&nbsp;</th>
								
								<th>&nbsp;Opérateur&nbsp;</th>																
								
								<th>&nbsp;Produit&nbsp;</th>	
								
								<th>&nbsp;Ligne&nbsp;</th>	
									
								<th>&nbsp;Nombre d'appels&nbsp;</th>
									
								<th>&nbsp;Volume (mn ou ko)&nbsp;</th>
																																
								<th>&nbsp;Montant facturé (H.T.)&nbsp;</th>
																
								<th>&nbsp;N°facture&nbsp;</th>	
								
								<th>&nbsp;Date d'émission de la facture&nbsp;</th>	
																
								<th>&nbsp;Période facturée&nbsp;</th>		
																						
							</tr>							
		<cfset numRow = numRow +1>
	<cfelse>
						<cfif (numRow mod 2) eq 0>
							<tr>	
															
								<td class="grid_normal_alt" valign="top">&nbsp;#COMPTE_FACTURATION#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#OPNOM#&nbsp;</td>								
																		
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#LIBELLE_PRODUIT#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#SOUS_TETE#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#NOMBRE_APPEL#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#(DUREE_APPEL)#&nbsp;</td>								
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#LsEuroCurrencyFormat(MONTANT, 'none')#&nbsp;</td>								
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#NUMERO_FACTURE#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#TRIM(LSDateFormat(createDate(left(DATE_EMISSION,4),mid(DATE_EMISSION,6,2),mid(DATE_EMISSION,9,2)),"dd/mm/yyyy"))#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#TRIM(LSDateFormat(createDate(left(DATEDEB,4),mid(DATEDEB,6,2),mid(DATEDEB,9,2)),"dd/mm/yyyy"))# - #TRIM(LSDateFormat(createDate(left(DATEFIN,4),mid(DATEFIN,6,2),mid(DATEFIN,9,2)),"dd/mm/yyyy"))#&nbsp;</td>								
								
							</tr>
						<cfelse>
							<tr>	
															
								<td class="grid_normal" valign="top">&nbsp;#COMPTE_FACTURATION#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#OPNOM#&nbsp;</td>								
																		
								<td class="grid_normal" align="right" valign="top">&nbsp;#LIBELLE_PRODUIT#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#SOUS_TETE#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#NOMBRE_APPEL#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#(DUREE_APPEL)#&nbsp;</td>								
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#LsEuroCurrencyFormat(MONTANT, 'none')#&nbsp;</td>								
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#NUMERO_FACTURE#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#TRIM(LSDateFormat(createDate(left(DATE_EMISSION,4),mid(DATE_EMISSION,6,2),mid(DATE_EMISSION,9,2)),"dd/mm/yyyy"))#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#TRIM(LSDateFormat(createDate(left(DATEDEB,4),mid(DATEDEB,6,2),mid(DATEDEB,9,2)),"dd/mm/yyyy"))# - #TRIM(LSDateFormat(createDate(left(DATEFIN,4),mid(DATEFIN,6,2),mid(DATEFIN,9,2)),"dd/mm/yyyy"))#&nbsp;</td>								
								
							</tr>
						</cfif>							 												
							
		<cfset numRow = numRow +1>		
	</cfif>
</cfoutput>
							 						
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>