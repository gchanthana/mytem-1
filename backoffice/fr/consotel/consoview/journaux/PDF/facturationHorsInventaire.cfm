<table align="center" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><cfoutput><strong> Facturation hors inventaire au #Lsdateformat(now(), 'dd mmmm yyyy')#</cfoutput></strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">
								<th>&nbsp;Produit&nbsp;</th>
								
								<th>&nbsp;Opérateur&nbsp;</th>
								
								<th>&nbsp;Ligne&nbsp;</th>								
								
								<th>&nbsp;Montant facturé(&euro; H.T)&nbsp;</th>
								
								<th>&nbsp;Date facture&nbsp;</th>
								
								<th>&nbsp;N°facture&nbsp;</th>								
							</tr>
<cfset numRow = 1>
<cfoutput query="qGetGrid">	
	<cfif (numRow mod 30) eq 0>
<tr><td colspan="22" height="1" bgcolor="808080"></td></tr>	
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>
		<cfdocumentitem type="pagebreak"/>		
<table align="center" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><cfoutput><strong> Commandes facturées avant livraison - #Lsdateformat(now(), 'dd mmmm yyyy')#</cfoutput></strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">
								<th>&nbsp;Produit&nbsp;</th>
								
								<th>&nbsp;Opérateur&nbsp;</th>
								
								<th>&nbsp;Ligne&nbsp;</th>								
								
								<th>&nbsp;Montant facturé(&euro; H.T)&nbsp;</th>
								
								<th>&nbsp;Date facture&nbsp;</th>
								
								<th>&nbsp;N°facture&nbsp;</th>								
							</tr>											
		<cfset numRow = numRow +1>
	<cfelse>				
						<cfif (numRow mod 2) eq 0>
							<tr>								
								<td class="grid_normal_alt" valign="top">&nbsp;#LIBELLE_PRODUIT#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#OPNOM#&nbsp;</td>								
																		
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#SOUS_TETE#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">#LsEuroCurrencyFormat(MONTANT, 'none')#&euro;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#LSDateFormat(createDate(left(DATE_FACTURE,4),mid(DATE_FACTURE,6,2),mid(DATE_FACTURE,9,2)),"dd/mm/yyyy")#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#NUM_FACTURE#&nbsp;</td>								
							</tr>
						<cfelse>
							<tr>								
								<td class="grid_normal" valign="top">&nbsp;#LIBELLE_PRODUIT#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#OPNOM#&nbsp;</td>								
																		
								<td class="grid_normal" align="right" valign="top">&nbsp;#SOUS_TETE#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">#LsEuroCurrencyFormat(MONTANT, 'none')#&euro;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#LSDateFormat(createDate(left(DATE_FACTURE,4),mid(DATE_FACTURE,6,2),mid(DATE_FACTURE,9,2)),"dd/mm/yyyy")#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#NUM_FACTURE#&nbsp;</td>								
							</tr>
						</cfif>		
		<cfset numRow = numRow +1>		
	</cfif>
</cfoutput>
							 				
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>