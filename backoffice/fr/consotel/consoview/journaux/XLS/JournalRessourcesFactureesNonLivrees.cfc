<cfcomponent name="JournalRessourcesFactureesNonLivrees">
	<cffunction name="afficher">
		<cfArgument name="PERIMETRE_LIBELLE" type="string" required="false">
		<cfArgument name="JOURNAL_LIBELLE" type="string" required="false">
			
		<cfset label="Journal">	
	
		<cfset sessionInfo = createObject("component","fr.consotel.consoview.util.utils")>
		<cfset qGetGrid= sessionInfo.getQuery("session.dataInventaire.journal")>	
	
		<cfset USERNOM = sessionInfo.getString("session.user.nom")> 
		<cfset USERPRENOM = sessionInfo.getString("session.user.prenom")>	
	 	<cfset INFO_FACTURE = sessionInfo.getString("session.dataInventaire.INFO_FACTURE")>
	 	<cfset R_SOCIALE = sessionInfo.getString("session.perimetre.RAISON_SOCIALE")>
	 	
		<cfheader name="Content-Disposition" value="inline;filename=rapport.xls">
		<cfcontent type="application/vnd.ms-excel">
			<?xml version="1.0" encoding="iso-8859-1"?>
			<?mso-application progid="Excel.Sheet"?>
			<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:x="urn:schemas-microsoft-com:office:excel"
				xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
				xmlns:html="http://www.w3.org/TR/REC-html40">
			<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
				<LastAuthor>CONSOTEL</LastAuthor>
				<Created>2006-02-27T14:44:16Z</Created>
				<Version>11.6360</Version>
			</DocumentProperties>
			<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
				<ProtectStructure>False</ProtectStructure>
				<ProtectWindows>False</ProtectWindows>
			</ExcelWorkbook>
				<Styles>		
					<Style ss:ID="Default" ss:Name="Normal">
					   <Alignment ss:Vertical="Bottom"/>
					   <Borders/>
					   <Font/>
					   <Interior/>
					   <NumberFormat/>
					   <Protection/>
					</Style>
					<Style ss:ID="s22">
					   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
					   <Borders/>
					   <Font ss:FontName="Helvetica" ss:Size="11" ss:Color="#000000" ss:Bold="1"/>
					</Style>
										
					<Style ss:ID="s23">
					   	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
					   	<Borders>
					  		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
					  	</Borders>
					  	<Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
					  	<Font ss:FontName="Helvetica" ss:Size="11" ss:Color="#000000" ss:Bold="1"/>
					</Style>
					
					<Style ss:ID="s24">
					   	<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>					   	
					   	<Font ss:FontName="Helvetica" ss:Size="8" ss:Color="#000000"/>
					   	<Borders>
					  		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
					  	</Borders>
					</Style>
					
					<Style ss:ID="s25">
					   	<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>					   	
					   	<Font ss:FontName="Helvetica" ss:Size="8" ss:Color="#000000"/>
					   	<Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
					</Style>
					
					
					<Style ss:ID="s26">
		   				<Alignment ss:Vertical="Bottom"/>
		   				<Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="18" ss:Color="#008000"
					    ss:Bold="1" ss:Italic="1"/>
		  			</Style>
					
					<Style ss:ID="s27">
					   	<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>					   	
					   	<Font ss:FontName="Helvetica" ss:Size="8" ss:Color="#000000"/>
					</Style>
					
					<Style ss:ID="s211" ss:Name="Euro">
					  	<Font ss:FontName="Helvetica" ss:Size="8" ss:Color="#000000"/>
					  	<ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
					  	 <Borders>
					  		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
					  	</Borders>					  	 
					</Style>
					
					<Style ss:ID="s16" ss:Parent="s211">
						<NumberFormat ss:Format="_-* #,##0\ _?_-;\-* #,##0\ _?_-;_-* &quot;-&quot;??\ _?_-;_-@_-"/>
					  	<Borders>
					  		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
					  		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
					  	</Borders>	
					</Style>
					
					<Style ss:ID="s34">
					   	<Alignment ss:Vertical="Center"/>
						<Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
					</Style>
					<Style ss:ID="s37">
					   	<Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
					   	<Font x:Family="Swiss" ss:Size="12" ss:Bold="1"/>
						<Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
					</Style>
					
					<Style ss:ID="s40">
						<Alignment ss:Vertical="Center"/>
					   	<Font x:Family="Swiss" ss:Bold="1"/>
					   	<Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
					</Style>
					
					<Style ss:ID="s41">
					   	<Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
					   	<Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="11" ss:Color="#000000" ss:Bold="1"/>
				  	</Style>
					
					<Style ss:ID="s42">
		   				<Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
					   	<Font ss:Size="14"/>
					   	<Interior/>
					</Style>
				</Styles>
			
			<cfset i=1>
			<cfset j=1>
			
			<Worksheet ss:Name="<cfoutput>#left(label,31)#</cfoutput>">
						
			<Table x:FullColumns="1"
			x:FullRows="1" ss:DefaultColumnWidth="60">
				
			
			<!--- <Row ss:AutoFitHeight="0" ss:Height="18">
				
			<Cell ss:MergeAcross="2" ss:StyleID="s78"><ss:Data ss:Type="String"
				xmlns="http://www.w3.org/TR/REC-html40"><B><I><Font html:Color="#008000">Conso</Font><Font
				html:Color="#FF9900">View</Font></I></B></ss:Data></Cell>
				
			</Row>
			
			 <Row>
				 <Cell ss:Index="6" ss:StyleID="s63"><Data ss:Type="String">Date d'ï¿½dition :</Data></Cell>
				 <Cell ss:StyleID="s64" ss:Formula="=NOW()"></Cell>
			</Row>
			
			<Row ss:Index="4" ss:Height="14.25">			
				<Cell ss:Index="7" ss:StyleID="s51"><Data ss:Type="String"><cfoutput>#session.user.prenom# #session.user.nom#</cfoutput></Data></Cell>			 
			</Row>
			
			<Row ss:Index="6" ss:AutoFitHeight="0" ss:Height="22.5">
			   <Cell ss:Index="4" ss:StyleID="s52"/>
			   <Cell ss:StyleID="s52"/>
			   <Cell ss:StyleID="s52"/>
			   <Cell ss:StyleID="s53"><Data ss:Type="String">Journal des ressources facturï¿½es mais non livrï¿½es</Data></Cell>
			</Row>
			 
			<Row ss:Index="8" ss:AutoFitHeight="0" ss:Height="22.5">
				<Cell ss:Index="3" ss:StyleID="s50"/>
				<Cell ss:Index="4" ss:StyleID="s50"/>
				<Cell ss:Index="5" ss:StyleID="s50"/>
				<Cell ss:StyleID="s50"/>
				<Cell ss:StyleID="s54"><Data ss:Type="String"><cfoutput>#session.perimetre.type_perimetre# : #raisonsociale#</cfoutput></Data></Cell>
			</Row> --->
			
			<!--- <Row>
				<Cell ss:StyleID="s25"><Data ss:Type="String">P&eacute;riode d'&eacute;mission des factures : <cfoutput>#Lsdateformat(datedeb, 'dd mmmm yyyy')# - #Lsdateformat(datefin, 'dd mmmm yyyy')#</cfoutput></Data></Cell>
			</Row> --->
			
						
			<Column ss:Width="200"/>
			<Column ss:Width="100"/>
			<Column ss:Width="300"/>		   	
			<Column ss:Width="120"/>
			<Column ss:Width="200"/>
			<Column ss:Width="200"/>
			<Column ss:Width="200"/>
			<Column ss:Width="120"/>		  	
			<Column ss:Width="120"/>			
			<Column ss:Width="120"/>			
			<Column ss:Width="500"/>
			<Column ss:Width="200"/>
		   	
		   	<Row ss:Index="3">
			    <Cell ss:Index="1" ss:StyleID="s26">
			    	<ss:Data ss:Type="String" xmlns="http://www.w3.org/TR/REC-html40"><B><I><Font html:Color="#008000">Conso</Font><Font html:Color="#FF9900">View</Font></I></B></ss:Data>
				</Cell>			    
			</Row>
					    
			<Row ss:Index="5" ss:Height="15">
		   		<Cell ss:MergeAcross="10" ss:Index="1" ss:StyleID="s27">
			   		<ss:Data ss:Type="String" xmlns="http://www.w3.org/TR/REC-html40">Editï¿½ par :<B><Font html:Color="#000000"><cfoutput>#USERNOM# #USERPRENOM#</cfoutput></Font></B></ss:Data>
		   		</Cell>
		   	</Row>
		   			    
			<Row ss:Index="7" ss:Height="15">
		 		<Cell ss:MergeAcross="1" ss:Index="1" ss:StyleID="s27">
		 			<ss:Data ss:Type="String" xmlns="http://www.w3.org/TR/REC-html40">Le : <B><Font html:Color="#000000"><cfoutput>#LSDateFormat(now(),"dd/mm/yyyy")#</cfoutput></Font></B></ss:Data>
		 		</Cell>							     		
		   	</Row>
			
			<Row ss:Index="9" ss:Height="18">		    	
		    	<Cell ss:MergeAcross="10" ss:StyleID="s37">
		    		<ss:Data ss:Type="String" xmlns="http://www.w3.org/TR/REC-html40"><B><cfoutput>#R_SOCIALE#</cfoutput></B></ss:Data>
		    	</Cell>		    	
		   	</Row>
								   	
			<Row ss:Index="11" ss:Height="18">		    	
		    	<Cell ss:MergeAcross="10" ss:StyleID="s40">
		    		<ss:Data ss:Type="String" xmlns="http://www.w3.org/TR/REC-html40"><B><cfoutput>#JOURNAL_LIBELLE#</cfoutput></B></ss:Data>
		    	</Cell>		    	
		   	</Row>
		   	
		   	<Row ss:Index="13" ss:Height="18">		    	
		    	<Cell ss:MergeAcross="10" ss:StyleID="s27">
		    		<ss:Data ss:Type="String" xmlns="http://www.w3.org/TR/REC-html40"><B><cfoutput>#INFO_FACTURE#</cfoutput></B></ss:Data>
		    	</Cell>		    	
		   	</Row>
		   			   			   	
			<Row/>
			<Row/>
			<Row/>	
		   		   		   
			<Row ss:AutoFitHeight="0" ss:Height="15">				
				<Cell ss:StyleID="s23"><Data ss:Type="String">Compte de facturation</Data></Cell>
			    <Cell ss:StyleID="s23"><Data ss:Type="String">Opï¿½rateur</Data></Cell>	
				<Cell ss:StyleID="s23"><Data ss:Type="String">Produit</Data></Cell>			   			
				<Cell ss:StyleID="s23"><Data ss:Type="String">Ligne</Data></Cell>	
				<Cell ss:StyleID="s23"><Data ss:Type="String">Nï¿½Facture</Data></Cell>	
				<Cell ss:StyleID="s23"><Data ss:Type="String">Date d' ï¿½mission de la facture</Data></Cell>
				<Cell ss:StyleID="s23"><Data ss:Type="String">Pï¿½riode facturï¿½e</Data></Cell>
				<Cell ss:StyleID="s23"><Data ss:Type="String">Montant facturï¿½e</Data></Cell>
				<Cell ss:StyleID="s23"><Data ss:Type="String">Etat</Data></Cell>					
				<Cell ss:StyleID="s23"><Data ss:Type="String">Type opï¿½ration</Data></Cell>
				<Cell ss:StyleID="s23"><Data ss:Type="String">Libellï¿½ opï¿½ration</Data></Cell> 
				<Cell ss:StyleID="s23"><Data ss:Type="String">Nï¿½opï¿½ration</Data></Cell>
			</Row>
						
			<cfoutput query="qGetGrid">
			<cfset a=j>			
			<cfif i mod 50000 eq 0>
				
			  </Table>
			<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
				<PageSetup>
					<PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
						x:Right="0.78740157499999996" x:Top="0.984251969"/>
				</PageSetup>
				<Print>
					<ValidPrinterInfo/>
					<HorizontalResolution>300</HorizontalResolution>
					<VerticalResolution>300</VerticalResolution>
					<NumberofCopies>0</NumberofCopies>
				</Print>
				<Selected/>
				<ProtectObjects>False</ProtectObjects>
				<ProtectScenarios>False</ProtectScenarios>
			</WorksheetOptions>
		</Worksheet>
		<Worksheet ss:Name="#left(label,evaluate(30-len(j)))#_#j#">
		
			<Table x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="60">
				
			<Column ss:Width="200"/>
			<Column ss:Width="100"/>
			<Column ss:Width="300"/>		   	
			<Column ss:Width="120"/>
			<Column ss:Width="200"/>
			<Column ss:Width="200"/>
			<Column ss:Width="200"/>
			<Column ss:Width="120"/>		  	
			<Column ss:Width="120"/>			
			<Column ss:Width="120"/>			
			<Column ss:Width="500"/>
			<Column ss:Width="200"/>
		    	    		   
			<Row ss:AutoFitHeight="0" ss:Height="15">				
				<Cell ss:StyleID="s23"><Data ss:Type="String">Compte de facturation</Data></Cell>
			    <Cell ss:StyleID="s23"><Data ss:Type="String">Opï¿½rateur</Data></Cell>	
				<Cell ss:StyleID="s23"><Data ss:Type="String">Produit</Data></Cell>			   			
				<Cell ss:StyleID="s23"><Data ss:Type="String">Ligne</Data></Cell>	
				<Cell ss:StyleID="s23"><Data ss:Type="String">Nï¿½Facture</Data></Cell>	
				<Cell ss:StyleID="s23"><Data ss:Type="String">Date d' ï¿½mission de la facture</Data></Cell>
				<Cell ss:StyleID="s23"><Data ss:Type="String">Pï¿½riode facturï¿½e</Data></Cell>
				<Cell ss:StyleID="s23"><Data ss:Type="String">Montant facturï¿½e</Data></Cell>
				<Cell ss:StyleID="s23"><Data ss:Type="String">Etat</Data></Cell>					
				<Cell ss:StyleID="s23"><Data ss:Type="String">Type opï¿½ration</Data></Cell>
				<Cell ss:StyleID="s23"><Data ss:Type="String">Libellï¿½ opï¿½ration</Data></Cell> 
				<Cell ss:StyleID="s23"><Data ss:Type="String">Nï¿½opï¿½ration</Data></Cell>
			</Row>
					
			<Row ss:AutoFitHeight="0" ss:Height="12.9375">
				<Cell ss:StyleID="s24"><Data ss:Type="String">#COMPTE_FACTURATION#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#OPNOM#</Data></Cell>
			    <Cell ss:StyleID="s24"><Data ss:Type="String">#LIBELLE_PRODUIT#</Data></Cell>			   
				<Cell ss:StyleID="s24"><Data ss:Type="String">#SOUS_TETE#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#NUMERO_FACTURE#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#TRIM(LSDateFormat(createDate(left(DATE_EMISSION,4),mid(DATE_EMISSION,6,2),mid(DATE_EMISSION,9,2)),"dd/mm/yyyy"))#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#TRIM(LSDateFormat(createDate(left(DATEDEB,4),mid(DATEDEB,6,2),mid(DATEDEB,9,2)),"dd/mm/yyyy"))# -#TRIM(LSDateFormat(createDate(left(DATEFIN,4),mid(DATEFIN,6,2),mid(DATEFIN,9,2)),"dd/mm/yyyy"))#</Data></Cell>
				<Cell ss:StyleID="s211"><Data ss:Type="Number">#MONTANT#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#LIBELLE_ETAT#</Data></Cell>				   
				<Cell ss:StyleID="s24"><Data ss:Type="String">#TYPE_OPERATION#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#LIBELLE_OPERATIONS#</Data></Cell>			 			
				 <Cell ss:StyleID="s24"><Data ss:Type="String">#NUMERO_OPERATION#</Data></Cell>
			 </Row>
				<cfset j=j+1>
			<cfelse>
			<Row ss:AutoFitHeight="0" ss:Height="12.9375">
				<Cell ss:StyleID="s24"><Data ss:Type="String">#COMPTE_FACTURATION#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#OPNOM#</Data></Cell>
			    <Cell ss:StyleID="s24"><Data ss:Type="String">#LIBELLE_PRODUIT#</Data></Cell>			   
				<Cell ss:StyleID="s24"><Data ss:Type="String">#SOUS_TETE#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#NUMERO_FACTURE#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#TRIM(LSDateFormat(createDate(left(DATE_EMISSION,4),mid(DATE_EMISSION,6,2),mid(DATE_EMISSION,9,2)),"dd/mm/yyyy"))#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#TRIM(LSDateFormat(createDate(left(DATEDEB,4),mid(DATEDEB,6,2),mid(DATEDEB,9,2)),"dd/mm/yyyy"))# -#TRIM(LSDateFormat(createDate(left(DATEFIN,4),mid(DATEFIN,6,2),mid(DATEFIN,9,2)),"dd/mm/yyyy"))#</Data></Cell>
				<Cell ss:StyleID="s211"><Data ss:Type="Number">#MONTANT#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#LIBELLE_ETAT#</Data></Cell>			    
				<Cell ss:StyleID="s24"><Data ss:Type="String">#TYPE_OPERATION#</Data></Cell>
				<Cell ss:StyleID="s24"><Data ss:Type="String">#LIBELLE_OPERATIONS#</Data></Cell>			 			
				<Cell ss:StyleID="s24"><Data ss:Type="String">#NUMERO_OPERATION#</Data></Cell>
			 </Row>
			</cfif>
			<cfset i=i+1>
			</cfoutput>
			</Table>
		<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
			<PageSetup>
				<PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
					x:Right="0.78740157499999996" x:Top="0.984251969"/>
			</PageSetup>
			<Print>
				<ValidPrinterInfo/>
				<HorizontalResolution>300</HorizontalResolution>
				<VerticalResolution>300</VerticalResolution>
				<NumberofCopies>0</NumberofCopies>
			</Print>
	    	<Selected/>
			<ProtectObjects>False</ProtectObjects>
			<ProtectScenarios>False</ProtectScenarios>
		</WorksheetOptions>
	</Worksheet>
</Workbook>				
	</cffunction>
</cfcomponent>