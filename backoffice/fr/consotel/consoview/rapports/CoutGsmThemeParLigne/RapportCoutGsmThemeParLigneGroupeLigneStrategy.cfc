<!---
Package : fr.consotel.consoview.rapports.coutgsmthemeparligne
--->
<cfcomponent name="RapportCoutGsmThemeParLigneGroupeLigneStrategy">
	<cffunction name="getReportData" access="private" returntype="any" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset ID_ORGA = ID_PERIMETRE>
		<cfset biServer="http://xmlpserver.consotel.fr/services/PublicReportService?wsdl">
		
		<!--- Envoi des paramètres --->
		<cfset ArrayOfParamNameValues=ArrayNew(1)>
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_IDRACINE_MASTER">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PERIMETRE.IDRACINE_MASTER>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[1]=ParamNameValue>
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_IDPERIMETRE">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=arguments.RapportParams.PERIMETRE_INDEX>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[2]=ParamNameValue>
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_DATEDEB">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=arguments.RapportParams.DATE_M>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[3]=ParamNameValue>
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_DATEFIN">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=arguments.RapportParams.DATE_M2>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[4]=ParamNameValue>

		<!--- Rapport --->
		<cfset myParamReportRequest=structNew()>
		<cfset myParamReportRequest.reportAbsolutePath="/~consoview/M33/CoutGsmThemeParLigne_LIG/CoutGsmThemeParLigne_LIG.xdo">
		<!--- <cfset myParamReportRequest.reportAbsolutePath="/~consoview/M33/RP1/RP1.xdo">
		 --->
		<cfset myParamReportRequest.attributeTemplate="DEFAULT">
		<cfset myParamReportRequest.attributeLocale="fr-FR">
		<cfset myParamReportRequest.attributeFormat=arguments.RapportParams.FORMAT>
		<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
		<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
		<cfset myParamReportParameters=structNew()>
		<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
		<cfset myParamReportParameters.userID="consoview">
		<cfset myParamReportParameters.password="public">
		<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport"
				argumentCollection="#myParamReportParameters#">
					
		</cfinvoke>
		<cfreturn resultRunReport>
		<!--- 
        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_REP_V3.REP_COUT_GSM_THEME_LIG">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.DATE_M#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATE_M2#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfreturn qGetReportData>
		 --->
	</cffunction>

	<cffunction name="GetListeSurTheme" access="private" returntype="query" output="true">
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<!--- METHODE REQUETTE query of queries --->
		<cfquery name="qGetListeSurTheme" dbtype="query">
			select sur_theme
			from p_detail_lignes
			group by sur_theme
		</cfquery>
		<cfreturn qGetListeSurTheme>
	</cffunction>

	<cffunction name="GetListeSous_teteAbos" access="private" returntype="struct" output="true">
		<cfargument name="p_detail_lignes" required="true"/>
		<!--- METHODE REQUETTE query of queries --->
		<cfquery name="qGetListeSous_tete" dbtype="query">
			select idsous_tete, sous_tete, patronyme,sum(montant_final) as montant_abo_total
			from p_detail_lignes
			where lower(type_theme)=lower('abonnements')
			group by idsous_tete, sous_tete, patronyme
		</cfquery>
		<cfset st=structNew()>
		<cfloop query="qGetListeSous_tete">
			<!---==============
				Si la sous-tete apparait plus d'une fois parcequ'elle est affect�e � plus d'un collaborateur.
				Alors il ne faut pas sommer les montant pour cette sous-tete car le montant repr�sente d�j�
				le cout de la ligne pour l'ensemble des collaborateurs affect�s � cette sous-tete.
				Pour l'instant on prend le 1er collaborateur affect� que l'on trouve.
			==============--->
			<cfif NOT structKeyExists(st,"#idsous_tete#")>
				<cfset structInsert(st,idsous_tete,montant_abo_total)>
			</cfif>
		</cfloop>
		<cfreturn st>
	</cffunction>
	
	<cffunction name="GetListeSous_tete" access="private" returntype="query" output="false" hint="Ram�ne les donn�es du rapport" >
		<cfargument name="p_detail_lignes" required="false" type="query" default="" displayname="struct RapportParams" hint=%qt%%paramNotes%%qt% />
		<!--- METHODE REQUETTE query of queries --->
		<cfquery name="qGetListeSous_tete" dbtype="query">
			select idsous_tete, sous_tete, patronyme,0 as montant_abo_total
			from p_detail_lignes
			group by idsous_tete, sous_tete, patronyme, montant_abo_total
		</cfquery>
		<cfreturn qGetListeSous_tete>
	</cffunction>
	
	<cffunction name="getResult" access="private" returntype="query" output="true">
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<!--- METHODE REQUETTE query of queries --->
		<cfquery name="q1" dbtype="query">
			select idsous_tete, sous_tete
			from p_detail_lignes
			group by idsous_tete, sous_tete
		</cfquery>
		<cfquery name="q2" dbtype="query">
			select idsous_tete, sous_tete, idtheme_produit, sur_theme, montant_final
			from p_detail_lignes, q1
			where p_detail_lignes. 
			group by idsous_tete, sous_tete, patronyme,montant_abo_total
		</cfquery>
		<cfreturn qGetListeSous_tete>
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<!---====== FORMATAGE DES DATES ======--->
		<cfset RapportParams.date_m = LSDateFormat(ParseDateTime(RapportParams.DATEDEB),"yyyy/mm/dd")>
		<cfset RapportParams.date_m2=
				LSDateFormat(DateAdd("m",RapportParams.periodicite,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_un=
				LSDateFormat(DateAdd("m",-1,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_deux=
				LSDateFormat(DateAdd("m",(-1 - (RapportParams.periodicite)),ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_debut_annee = year(ParseDateTime(RapportParams.DATEDEB)) & "/01/01">
		<cfset rapportParams.chaine_date = rapportParams.DATE_M>
		<!---====================================================--->
		<cfset qGetData=getReportData(ID_PERIMETRE,RapportParams)>
		<cfset reporting=createObject("component","fr.consotel.consoview.api.Reporting")>
		<cfset filename="Extract GSM par Thème-LIG-" & replace(session.perimetre.raison_sociale,' ','_',"all") &
												reporting.getFormatFileExtension(arguments.RapportParams.FORMAT)>
		<cfheader name="Content-Disposition" value="attachment;filename=#filename#">
		<cfcontent type="#qGetData.getReportContentType()#" variable="#qGetData.getReportBytes()#">
		<!--- 
		<cfset dataset=getReportData(ID_PERIMETRE,RapportParams)>
		<cfif dataset.recordcount NEQ 0>
			<cfif UCASE(arguments.RapportParams.FORMAT) EQ "CSV">
				<cfset filename="Extract-GSM-Par-Theme_" & replace(session.perimetre.raison_sociale,' ','_',"all")>
				<cfset exportService=createObject("component","fr.consotel.consoview.api.reporting.export.ExportService")>
				<cfset exportService.exportQueryToText(dataset,filename & ".csv")>
			<cfelse>
				<cfquery dbtype="query" name="qGetData">
					select libelle, idsous_tete, sous_tete, patronyme, type_theme, idtheme_produit, theme_libelle as sur_theme, sum(montant_final) as montant_final
					from dataset
					group by libelle, idsous_tete, sous_tete, patronyme, type_theme, idtheme_produit, theme_libelle
				</cfquery>
				<cfset qGetListeSurTheme=GetListeSurTheme(qGetData)>
				<cfset qGetListeSous_tete=GetListeSous_tete(qGetData)>
				<cfset queryModifier=createObject("component","fr.consotel.consoview.facturation.optimisation.utils")>
				<cfset fetchedQuery=queryModifier.transposeQuery(qGetData,"sous_tete","sur_theme","montant_final",GetListeSous_teteAbos(qGetData))>
				<cfif RapportParams.format eq "Excel">
					<cfinclude template="./coutgsmparligne_xls.cfm">
				</cfif>
			</cfif>
		<cfelse>
			<cfoutput>
				<center><strong><h2>Aucunes données répondant à votre demande</h2></strong></center>
			</cfoutput>
		</cfif>
		 --->
	</cffunction>
</cfcomponent>
