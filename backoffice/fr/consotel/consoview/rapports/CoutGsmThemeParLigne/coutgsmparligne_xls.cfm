<cfheader name="Content-Disposition" value="inline;filename=rapportCoutGsmThemeParLigne.xls" charset="iso-8859-1">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="iso-8859-1"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor>consotel</LastAuthor>
  <LastPrinted>2006-02-22T18:23:58Z</LastPrinted>
  <Created>2005-06-08T09:23:28Z</Created>
  <LastSaved>2006-05-05T09:47:58Z</LastSaved>
  <Company></Company>
  <Version>11.6408</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>4800</WindowHeight>
  <WindowWidth>7530</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>105</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
		<ss:Style ss:ID="s26" ss:Name="Euro">
		   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
  <Style ss:ID="s21">
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
		   </ss:Borders>
   <Interior/>
  </Style>
  <Style ss:ID="s24">
   <Font x:Family="Swiss" ss:Size="12" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s27" ss:Parent="s26">
   <Interior/>
  </Style>
  <Style ss:ID="s28" ss:Parent="s26">
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s30">
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
		   </ss:Borders>
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Interior/>
  </Style>
  <Style ss:ID="s31">
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
		   </ss:Borders>
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font x:Family="Swiss" ss:Size="12" ss:Bold="1"/>
   <Interior/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Rapport GSM Par Theme">
  <Names>
	<cfset nbsur_theme=qGetListeSurTheme.recordcount>
	<cfset colNb=6+#nbsur_theme#>
	<cfset rangeVar="R1C1:R#qGetListeSous_tete.recordcount#C#colNb#">
	<cfset expandedRow=#qGetListeSous_tete.recordcount#+1>
	<cfoutput>
	   <NamedRange ss:Name="_FilterDatabase"
	    ss:RefersTo="='Extract données'!#rangeVar#" ss:Hidden="1"/>
	</cfoutput>
  </Names>
	<cfoutput>
	  <Table  x:FullColumns="1"
	   x:FullRows="1" ss:StyleID="s21" ss:DefaultColumnWidth="60">
	</cfoutput>
   <!--- DEFINITION DES COLONNES --->
	
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="105.75"/>
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="250"/>
   <Column ss:StyleID="s30" ss:Width="80.25"/>
   <Column ss:StyleID="s21" ss:Width="200.25"/>
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="222.75"/>
	<cfoutput query="qGetListeSurTheme">
	   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="150"/>
	</cfoutput>
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="300"/>

   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:StyleID="s24"><Data ss:Type="String">Dpt</Data><NamedCell
      ss:Name="_FilterDatabase"/></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">Société</Data><NamedCell
      ss:Name="_FilterDatabase"/></Cell>
    <Cell ss:StyleID="s31"><Data ss:Type="String">Numéro</Data><NamedCell
      ss:Name="_FilterDatabase"/></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">Nom</Data><NamedCell
      ss:Name="_FilterDatabase"/></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">Total Abonnements (remise incluse)</Data><NamedCell
      ss:Name="_FilterDatabase"/></Cell>
	<cfloop from="1" to="#nbsur_theme#" index="j">
		<cfoutput>
		    <Cell ss:StyleID="s24"><Data ss:Type="String">#qGetListeSurTheme['sur_theme'][j]#</Data><NamedCell
		      ss:Name="_FilterDatabase"/></Cell>
		</cfoutput>
	</cfloop>	
    <Cell ss:StyleID="s24"><Data ss:Type="String">Total Consommation (Remises incluses)</Data><NamedCell
      ss:Name="_FilterDatabase"/></Cell>
   </Row>
   <!--- DEFINITION DES LIGNES DE DONNEES PAR COLONNE --->

	<cfloop from="1" to="#fetchedQuery.recordcount#" index="i">
		<cfoutput>
		   <Row>
		    <Cell ss:StyleID="s21"><Data ss:Type="String">Rempli par #RAISON_SOCIALE#</Data></Cell>
		    <Cell ss:StyleID="s21"><Data ss:Type="String">#fetchedQuery['libelle'][i]#</Data></Cell>
		    <Cell ss:Index="3" ss:StyleID="s30"><Data ss:Type="String">#fetchedQuery['sous_tete'][i]#</Data></Cell>
		    <Cell ss:StyleID="s21"><Data ss:Type="String">#fetchedQuery['patronyme'][i]#</Data></Cell>
		    <Cell ss:StyleID="s27" ss:Index="5"><Data ss:Type="Number">#fetchedQuery['montant_abo_total'][i]#</Data></Cell>
			<cfloop from="1" to="#nbsur_theme#" index="k">
				<cfoutput>
					<cfset val=UCase(replace(qGetListeSurTheme['sur_theme'][k]," ","_","all"))>
					<cfset numVal=fetchedQuery['#val#'][i]>
					
				    <Cell ss:StyleID="s27">
				    	<Data ss:Type="Number">#numVal#</Data>
				    	<NamedCell ss:Name="_FilterDatabase"/>
				    </Cell>
			    </cfoutput>
			</cfloop>
			<cfif nbsur_theme gte 1>
			    <Cell ss:StyleID="s26" ss:Formula="=SUM(RC[-#nbsur_theme#]:RC[-1])"><Data ss:Type="Number"></Data></Cell>
			</cfif>
		   </Row>
	   </cfoutput>
	</cfloop>

  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.4921259845"/>
    <Footer x:Margin="0.4921259845"/>
    <PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
     x:Right="0.78740157499999996" x:Top="0.984251969"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <TabColorIndex>42</TabColorIndex>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>1</ActiveRow>
     <ActiveCol>7</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>

  <!--- FILTRE AUTOMATIQUE (RANGE = TOUTES LES CELLULES) --->
  <cfoutput>
	  <AutoFilter x:Range="#rangeVar#" xmlns="urn:schemas-microsoft-com:office:excel">
	  </AutoFilter>
  </cfoutput>

  <Sorting xmlns="urn:schemas-microsoft-com:office:excel">
   <Sort>Numï¿½ro</Sort>
  </Sorting>
 </Worksheet>
</Workbook>
