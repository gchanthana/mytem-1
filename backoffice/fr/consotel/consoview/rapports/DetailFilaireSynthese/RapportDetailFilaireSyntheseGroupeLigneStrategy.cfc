<!---
Package : fr.consotel.consoview.rapports.detailfilaireSynthese
--->
<cfcomponent name="RapportDetailFilaireSyntheseGroupeLigneStrategy">
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset MOIS_FIN=lSDateFormat(parseDateTime(arguments.RapportParams.DATEDEB),"yyyy/mm/dd")>
		<cfset P_PARAM_LIST=SESSION.PERIMETRE.IDRACINE_MASTER & "," & arguments.RapportParams["PERIMETRE_INDEX"] & "," &
				arguments.RapportParams["RAISON_SOCIALE"] & "," & arguments.RapportParams["TYPE_PERIMETRE"] & "," &
				MOIS_FIN & "," & arguments.RapportParams["PERIODICITE"]>
		<cfset ArrayOfParamNameValues=ArrayNew(1)>
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_METHOD">
		<cfset t=ArrayNew(1)>
		<cfset t[1]="getXmlData">
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[1]=ParamNameValue>
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_PARAM_LIST">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=P_PARAM_LIST>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[2]=ParamNameValue>
		
		<cfset myParamReportRequest=structNew()>
		<cfset myParamReportRequest.reportAbsolutePath="/~consoview/standard/DETAILFILAIRESYNTHESE-REPORT/DETAILFILAIRESYNTHESE-REPORT.xdo">
		<cfset myParamReportRequest.attributeTemplate="SYNTHESE">
		<cfset myParamReportRequest.attributeLocale="fr-FR">
		<cfset myParamReportRequest.attributeFormat=arguments.RapportParams.FORMAT>
		<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
		<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
		<cfset myParamReportParameters=structNew()>
		<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
		<cfset myParamReportParameters.userID="consoview">
		<cfset myParamReportParameters.password="public">
		<cfdump var="#myParamReportParameters#">
		<cfinvoke webservice="#SESSION.BISERVER#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
		<cfset reporting=createObject("component","fr.consotel.consoview.api.Reporting")>
		<cfset filename="Rapport Téléphonie Fixe Global-" & replace(session.perimetre.raison_sociale,' ','_',"all") &
														reporting.getFormatFileExtension(arguments.RapportParams.FORMAT)>
		<cfheader name="Content-Disposition" value="attachment;filename=#filename#">
		<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">
	</cffunction>
</cfcomponent>
