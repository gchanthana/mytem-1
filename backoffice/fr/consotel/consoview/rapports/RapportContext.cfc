<cfcomponent name="RapportContext" alias="fr.consotel.consoview.rapports.RapportContext" output="false">
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="PERIMETRE_INDEX" required="true" type="numeric"/>
		<cfargument name="TYPE_PERIMETRE" required="true" type="string"/>
		<cfargument name="REPORT_TYPE" required="true" type="string"/>
		<cfargument name="RAISON_SOCIALE" required="true" type="string"/>
		<cfargument name="rapportParams" required="true" type="struct"/>
		<cfset LEPERIMETRE="">		
		<cfswitch expression="#UCASE(TYPE_PERIMETRE)#">
			<cfcase value=""><cfset LEPERIMETRE=""></cfcase>
			<cfcase value="GROUPE"><cfset LEPERIMETRE="Groupe"></cfcase>
			<cfcase value="GROUPELIGNE"><cfset LEPERIMETRE="GroupeLigne"></cfcase>
			<cfcase value="SOUSTETE"><cfset LEPERIMETRE="SousTete"></cfcase>
			<cfdefaultcase>
				<cfthrow type="any" message="Périmètre non valide" detail="Le périmètre spécifié n'est pas reconnu par le système">
			</cfdefaultcase>
		</cfswitch>
			
		<cfset className = "fr.consotel.consoview.rapports.#REPORT_TYPE#.Rapport#REPORT_TYPE##LEPERIMETRE#Strategy">
				
		<cfset idPerimetre = PERIMETRE_INDEX>
		<!--- 
		<cfif Not StructIsEmpty(FORM)>
			<cfset rapportParams = structNew()>
			<cfloop collection="#FORM#" item="i">
				<cfif i neq "FIELDNAMES" AND i neq "CHARTAREA">
					<cfset structInsert(rapportParams,"#i#","#FORM[i]#")>
				</cfif>
			</cfloop>
		</cfif>
		 --->

		<cfset appliName = "CONSOVIEW v3">
		<cfset consoviewKey = CGI.SERVER_NAME & " : " & appliName>
		<cfset componentName = "Lancement rapport sur un " & #LEPERIMETRE#>
		<cfset clientIDADDR = SESSION.IP_ADDR>
		<cfquery name="qInsertLogEvent" datasource="#SESSION.OFFREDSN#">
			INSERT INTO LOG_REPORT(date_log,app_log,code_erreur,event_log,ipaddr,login_log,classe_log,methode_log)
			VALUES (sysdate,'#consoviewKey#',0,'REMOTING_EVENT','#LEFT(clientIDADDR,30)#','#SESSION.USER.EMAIL#','#LEFT(componentName,200)#','#REPORT_TYPE#')
		</cfquery>
		<cfset reportProcessor=createObject("component","fr.consotel.consoview.rapports.#REPORT_TYPE#.Rapport#REPORT_TYPE##LEPERIMETRE#Strategy")>
		<cfset reportProcessor.displayRapport(idPerimetre,arguments.rapportParams)>
	</cffunction>
</cfcomponent>
