<cfcomponent name="ReportManager" alias="fr.consotel.consoview.rapports.ReportManager">
	<cffunction name="getReport" access="public" returntype="void" output="true">
		<cfargument name="reportFile" required="true" type="string" hint="Nom du fichier brut avec son extension"/>
		<cfargument name="zipFile" required="true" type="string" hint="Nom du fichier zip avec extension zip"/>
		<cfargument name="contentObj" required="true" type="any" hint="Contenu binaire du fichier brut"/>
		<cfargument name="accessKey" required="true" type="string" hint="CFTOKEN de la session donc UUID unique"/>
		<cfset rootPath=expandPath("/")>
		<cfset reportDir="#rootPath#\fr\consotel\consoview\rapports\zip">
		<cfif accessKey EQ SESSION.CFToken>
			<cffile action="write" file="#reportDir#\#SESSION.SESSIONID#_#reportFile#" charset="ISO-8859-1"
								addnewline="true" fixnewline="true" output="#contentObj#">
			<cfzip action="zip" file="#reportDir#\#SESSION.SESSIONID#_#zipFile#" overwrite="yes">
				<cfzipparam source="#reportDir#\#SESSION.SESSIONID#_#reportFile#" entrypath="#reportFile#">
			</cfzip>
			<cffile action="delete" file="#reportDir#\#SESSION.SESSIONID#_#reportFile#">
			<cfoutput>
				<cfheader name="content-disposition" value="attachment; filename=#zipFile#"/>
				<cfcontent file="#reportDir#\#SESSION.SESSIONID#_#zipFile#" deletefile="false" type="application/zip"/>
			</cfoutput>
		<cfelse>
			<cfoutput>
				ConsoView v3 - Vous n'avez pas acc�s � cette page<br>
				Votre tentative a �t� enregistr�e :<br>
				IP : #CGI.REMOTE_ADDR# (#lsDateFormat(NOW(),"DD MMMM YYYY")# - #lsTimeFormat(NOW(),"HH:MM:SS")#)<br>
			   	<cflog type="Information" text="ReportManager ACCESS REFUSED - #CGI.REMOTE_ADDR#">
			</cfoutput>
		</cfif>
	</cffunction>
</cfcomponent>
