<cfheader name="Content-Disposition" value="inline;filename=rapport_rationalisation_#replace(RAISON_SOCIALE,' ','_')#.xls">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="iso-8859-1"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
 xmlns:s="uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882"
 xmlns:rs="urn:schemas-microsoft-com:rowset" xmlns:z="#RowsetSchema"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Version>11.6360</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>10005</WindowHeight>
  <WindowWidth>10005</WindowWidth>
  <WindowTopX>120</WindowTopX>
  <WindowTopY>135</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
	    <ss:Styles>
	        <ss:Style ss:ID="1">
	            <ss:Font ss:Bold="1"/>
				<ss:Borders>
				    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
			   </ss:Borders>
	        </ss:Style>
	  <ss:Style ss:ID="s21">
	   <ss:Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	  </ss:Style>
	   
	  <ss:Style ss:ID="s30">
	   <ss:Alignment ss:Vertical="Center" ss:Horizontal="Center" ss:WrapText="1"/>
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <ss:Font ss:Bold="1"/>
	  </ss:Style>
		<ss:Style ss:ID="s16" ss:Name="Euro">
		   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
		<ss:Style ss:ID="s17" ss:Name="Total">
		   	<ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
		<ss:Style ss:ID="s18" ss:Name="Total1">
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3" ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
		<ss:Style ss:ID="s19" ss:Name="Total2">
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
	</ss:Styles>
	    <Worksheet ss:Name="Lignes sans Consommations">
	      <ss:Table>
	            <ss:Column ss:Width="100"/>
	            <ss:Column ss:Width="330"/>
	            <ss:Column ss:Width="330"/>
	            <ss:Column ss:Width="90"/>
	            <ss:Column ss:Width="250"/>
	            <ss:Column ss:Width="80"/>
	            <ss:Column ss:Width="80"/>
	            <ss:Column ss:Width="80"/>
	            <ss:Column ss:Width="80"/>
				<ss:Column ss:Width="80"/>
	            <ss:Column ss:Width="80"/>
				<ss:Column ss:Width="80"/>
	            <cfoutput>
				<ss:Row>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">SIREN/SIRET</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Adresse</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Adresse(Cmplt)</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Code Postal</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Commune</ss:Data> 
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">NDI</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Fonction</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Du</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Au</ss:Data>
	                </ss:Cell>
					<ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Abonnements (P&eacute;riode)</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Abonnements (Mensuel)</ss:Data>
	                </ss:Cell>
	            </ss:Row>
				</cfoutput>
 	           	<cfoutput query="qGetData">
					<ss:Row>
			            <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#code_site_install#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#Adresse1_install#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#Adresse2_install#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#zipcode_install#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#Ville_install#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#sous_tete#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#ndi_fonction#</ss:Data>
		                </ss:Cell>
		                 <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#v_date_deb#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#v_date_fin#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s16">
		                    <ss:Data ss:Type="Number">#montantabo#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s16">
		                    <ss:Data ss:Type="Number">#Evaluate(montantabo/nb_month_ecart)#</ss:Data>
		                </ss:Cell>
		            </ss:Row>
				</cfoutput>
				<cfoutput>
				<ss:Row>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s17">
	                    <ss:Data ss:Type="String">TOTAL</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s19" ss:Formula="=SUM(R[-#Evaluate(qGetData.recordcount)#]C:R[-1]C)">
						<ss:Data ss:Type="Number"></ss:Data>
					</ss:Cell>
					<ss:Cell ss:StyleID="s19" ss:Formula="=SUM(R[-#Evaluate(qGetData.recordcount)#]C:R[-1]C)">
						<ss:Data ss:Type="Number"></ss:Data>
					</ss:Cell>
	                </cfoutput>
	            </ss:Row>
	        </ss:Table>
	    </Worksheet>
</Workbook>