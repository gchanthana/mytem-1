<!---
Package : fr.consotel.consoview.rapports.coutgsmparligne
--->
<cfcomponent name="RapportCoutGsmParLigneGroupeLigneStrategy">
	<cffunction name="getReportData" access="private" returntype="query" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_REP_V3.REP_COUT_GSM_LIG">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.DATE_M#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATE_M2#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfreturn qGetReportData>
	</cffunction>
	
	<cffunction name="GetListeSurTheme" access="private" returntype="query" output="true">
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<!--- METHODE REQUETTE query of queries --->
		<cfquery name="qGetListeSurTheme" dbtype="query">
			select sur_theme
			from p_detail_lignes
			group by sur_theme
		</cfquery>
		<cfreturn qGetListeSurTheme>
	</cffunction>

	<cffunction name="GetListeSous_teteAbos" access="private" returntype="struct" output="true">
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<!--- METHODE REQUETTE query of queries --->
		<cfquery name="qGetListeSous_tete" dbtype="query">
			select idsous_tete, sous_tete, patronyme,sum(montant_final) as montant_abo_total
			from p_detail_lignes
			where lower(type_theme)=lower('abonnements')
			group by idsous_tete, sous_tete, patronyme
		</cfquery>
		<cfset st=structNew()>
		<cfloop query="qGetListeSous_tete">
			<!---==============
				Si la sous-tete apparait plus d'une fois parcequ'elle est affect�e � plus d'un collaborateur.
				Alors il ne faut pas sommer les montant pour cette sous-tete car le montant repr�sente d�j�
				le cout de la ligne pour l'ensemble des collaborateurs affect�s � cette sous-tete.
				Pour l'instant on prend le 1er collaborateur affect� que l'on trouve.
			==============--->
			<cfif NOT structKeyExists(st,"#idsous_tete#")>
				<cfset structInsert(st,idsous_tete,montant_abo_total)>
			</cfif>
		</cfloop>
		<cfreturn st>
	</cffunction>
	
	<cffunction name="GetListeSous_tete" access="private" returntype="query" output="true">
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<!--- METHODE REQUETTE query of queries --->
		<cfquery name="qGetListeSous_tete" dbtype="query">
			select idsous_tete, sous_tete, patronyme,0 as montant_abo_total
			from p_detail_lignes
			group by idsous_tete, sous_tete, patronyme, montant_abo_total
		</cfquery>
		<cfreturn qGetListeSous_tete>
	</cffunction>
	
	<cffunction name="getResult" access="private" returntype="query" output="true">
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<!--- METHODE REQUETTE query of queries --->
		<cfquery name="q1" dbtype="query">
			select idsous_tete, sous_tete
			from p_detail_lignes
			group by idsous_tete, sous_tete
		</cfquery>
		<cfquery name="q2" dbtype="query">
			select idsous_tete, sous_tete, idtheme_produit, sur_theme, montant_final
			from p_detail_lignes, q1
			where p_detail_lignes. 
			group by idsous_tete, sous_tete, patronyme,montant_abo_total
		</cfquery>
		<cfreturn qGetListeSous_tete>
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<!---====== FORMATAGE DES DATES ======--->
		<cfset RapportParams.date_m = LSDateFormat(ParseDateTime(RapportParams.DATEDEB),"yyyy/mm/dd")>
		<cfset RapportParams.date_m2=
				LSDateFormat(DateAdd("m",RapportParams.periodicite,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_un=
				LSDateFormat(DateAdd("m",-1,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_deux=
				LSDateFormat(DateAdd("m",(-1 - (RapportParams.periodicite)),ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_debut_annee = year(ParseDateTime(RapportParams.DATEDEB)) & "/01/01">
		<cfset rapportParams.chaine_date = rapportParams.DATE_M>
		<!---====================================================--->
		<cfset dataset=getReportData(ID_PERIMETRE,RapportParams)>
		<cfif dataset.recordcount NEQ 0>
			<cfif UCASE(arguments.RapportParams.FORMAT) EQ "CSV">
				<cfset filename="Extract-GSM-Par-Categorie_" & replace(session.perimetre.raison_sociale,' ','_',"all")>
				<cfset exportService=createObject("component","fr.consotel.consoview.api.reporting.export.ExportService")>
				<cfset exportService.exportQueryToText(dataset,filename & ".csv")>
			<cfelse>
				<cfset qGetListeSurTheme=GetListeSurTheme(dataset)>
				<cfset qGetListeSous_tete=GetListeSous_tete(dataset)>
				<cfset queryModifier=createObject("component","fr.consotel.consoview.facturation.optimisation.utils")>
				<cfset fetchedQuery=queryModifier.transposeQuery(dataset,"sous_tete","sur_theme","montant_final",GetListeSous_teteAbos(dataset))>
				<cfif RapportParams.format eq "Excel">
					<cfinclude template="./coutgsmparligne_xls.cfm">
				</cfif>
			</cfif>
		<cfelse>
			<cfoutput>
				<center><strong><h2>Aucunes donn�es r�pondant � votre demande</h2></strong></center>
			</cfoutput>
		</cfif>
	</cffunction>
</cfcomponent>
