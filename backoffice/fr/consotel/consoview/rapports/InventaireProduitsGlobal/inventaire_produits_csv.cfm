<cfsetting enablecfoutputonly="false">
<cfset MAX_STEP=8000>
<cfset MAX_ITERATIONS=qGetData.recordcount>

<cfset reportFilename = "Inventaire_Produits.csv">
<cfset zipFilename = "Inventaire_Produits.zip">
<cfset reportManagerObj=createObject("component","fr.consotel.consoview.rapports.ReportManager")>
<!--- 
<cfset reportManagerObj.createReportFile(reportFilename,SESSION.CFToken)>
 --->
<cfset reportContent = arrayNew(1)>
<cfset columnString = "Société;Adresse 1;Adresse 2;Code Postal;Site;Compte Facturation;Sous Compte;" &
						"Ligne;Type de la ligne;Produit;Opérateur;Nombre d'appels;Durée (min);Qté;Montant (euros)">
<cfset arrayAppend(reportContent,columnString)>
<cfset reportManagerObj.createCSVReportFile(reportFilename,reportContent,SESSION.CFToken)>
<cfset reportContent=''>
<cfset reportContent = arrayNew(1)>

<cfset totalIteration=0>
<cfif MAX_ITERATIONS GT MAX_STEP>
	<cfset remainItem=MAX_STEP>
<cfelse>
	<cfset remainItem=MAX_ITERATIONS>
</cfif>
<cfloop condition="(totalIteration LESS THAN MAX_ITERATIONS)">
   	<cfoutput query="qGetData" startrow="#(totalIteration + 1)#" maxrows="#remainItem#">
		<cfset rowString = "#societe#;#adresse1#;#adresse2#;#zipcode#;#site#;#compte_facturation#;#sous_compte#;" &
							"#ligne#;#type#;#produit#;#nom#;#nombre_appel#;#duree_appel#;#qte#;#montant#">
		<cfset arrayAppend(reportContent,rowString)/> 
		<cfset rowString = ''>
	</cfoutput>
	<cfset totalIteration = totalIteration + remainItem>
	<cfset remainItem = MAX_ITERATIONS - totalIteration>
	<cfif remainItem GT MAX_STEP>
		<cfset remainItem=MAX_STEP>
	</cfif>
	<cfset reportManagerObj.appendCSVArrayContent(reportFilename,reportContent,SESSION.CFToken)>
	<cfset reportContent=''>
	<cfset reportContent = arrayNew(1)>
</cfloop>
<cfset qGetData=''>

<cfset reportManagerObj.zipReportFile(reportFilename,zipFilename,SESSION.CFToken)>
<cfset reportManagerObj=''>
