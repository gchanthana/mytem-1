<!---
Package : fr.consotel.consoview.rapports.deploiement
--->
<cfcomponent name="RapportDeploiementGroupeLigneStrategy">
	<cffunction name="getReportData" access="private" returntype="any" output="false">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		
<!---         <cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GLIG_V3.RP_DEPLOIEMENT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.DATEDEB#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.PERIODICITE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.CONSO_FT_MIN#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfreturn qGetReportData> --->
		<cfset ID_ORGA = ID_PERIMETRE>
		<cfset biServer=APPLICATION.BI_SERVICE_URL>

		<!--- getReportParameters --->
		<!--- Envoi des param�tres --->
		<cfset ArrayOfParamNameValues=ArrayNew(1)>
		<!---  --->
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_IDGROUPE_CLIENT">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=ID_ORGA>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[1]=ParamNameValue>
		<!---  --->
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_DATEDEB">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=RapportParams.DATEDEB>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[2]=ParamNameValue>
		<!---  --->
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_IDRACINE_MASTER">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=RapportParams.IDRACINE_MASTER>	<!---  --->	<!---  --->	<!---  --->	<!---  --->	<!---  --->
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[3]=ParamNameValue>
		<!---  --->
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_PERIODICITE">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=RapportParams.PERIODICITE>	<!---  --->	<!---  --->	<!---  --->	<!---  --->	<!---  --->
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[4]=ParamNameValue>
		<!---  --->
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_CONSO_FT_MIN">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=RapportParams.CONSO_FT_MIN>	<!---  --->	<!---  --->	<!---  --->	<!---  --->	<!---  --->
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[5]=ParamNameValue>
		
		
		<cfif lcase(#RapportParams.format#) eq "excel">
			<cfset typeformat = "excel">
		<cfelseif lcase(#RapportParams.format#) eq "csv">
			<cfset typeformat = "csv">
		<cfelseif lcase(#RapportParams.format#) eq "PDF">
			<cfset typeformat = "pdf">
		</cfif> 
		
		<!--- Rapport --->
		<cfset myParamReportRequest=structNew()>
		<cfset myParamReportRequest.reportAbsolutePath="/consoview/facturation/rapport/DeploiementGroupeLigne/DeploiementGroupeLigne.xdo">
		<cfset myParamReportRequest.attributeTemplate="cv">
		<cfset myParamReportRequest.attributeLocale="fr-FR">
		<cfset myParamReportRequest.attributeFormat="#typeformat#">
		<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
		<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
		<cfset myParamReportParameters=structNew()>
		<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
		<cfset myParamReportParameters.userID="consoview">
		<cfset myParamReportParameters.password="public">
		
		
		
		<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport"
				argumentCollection="#myParamReportParameters#">
		</cfinvoke>
		<cfreturn resultRunReport>
		
		
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset rapportParams.chaine_date = rapportParams.DATEDEB>
		<!---====== FORMATAGE DES DATES DE DEBUT ET DE FIN ======--->
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEDEB)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEDEB)>
		<cfset rapportParams.DATEDEB = LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<cfset rapportParams.IDRACINE_MASTER = SESSION.PERIMETRE.IDRACINE_MASTER>
		<!---====================================================--->
		<cfset qGetData = getReportData(ID_PERIMETRE,RapportParams)>
		<cfset filename="Deploiement_" & replace(session.perimetre.raison_sociale,' ','_',"all")>
		<cfif lcase(#RapportParams.format#) eq "excel">
			<cfheader name="Content-Disposition" value="attachment;filename=#filename#.xls">
			<cfcontent type="#qGetData.getReportContentType()#" variable="#qGetData.getReportBytes()#">
		<cfelseif lcase(#RapportParams.format#) eq "csv">
			<cfheader name="Content-Disposition" value="attachment;filename=#filename#.csv">
			<cfcontent type="text/csv" variable="#qGetData.getReportBytes()#">
		<cfelseif lcase(#RapportParams.format#) eq "pdf">
			<cfheader name="Content-Disposition" value="attachment;filename=#filename#.pdf">
			<cfcontent type="#qGetData.getReportContentType()#" variable="#qGetData.getReportBytes()#">
		</cfif>
	</cffunction>
</cfcomponent>
