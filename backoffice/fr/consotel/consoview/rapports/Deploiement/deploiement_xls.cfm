<cfheader name="Content-Disposition" value="inline;filename=rapport_deploiement_#replace(RAISON_SOCIALE,' ','_')#.xls" charset="iso-8859-1">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="iso-8859-1"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
 xmlns:s="uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Version>11.6360</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>10005</WindowHeight>
  <WindowWidth>10005</WindowWidth>
  <WindowTopX>120</WindowTopX>
  <WindowTopY>135</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
	    <ss:Styles>
	        <ss:Style ss:ID="1">
	            <ss:Font ss:Bold="1"/>
				<ss:Borders>
				    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
			   </ss:Borders>
	        </ss:Style>
	  <ss:Style ss:ID="s21">
	   <ss:Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	  </ss:Style>
	  <ss:Style ss:ID="s20" ss:Name="Pourcentage">
   <ss:NumberFormat ss:Format="0%"/>
  </ss:Style>
	   <ss:Style ss:ID="s24" ss:Name="Total3">
   <ss:Borders>
    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
   </ss:Borders>
   <ss:Font ss:Bold="1"/>
  </ss:Style>
	  <ss:Style ss:ID="s30">
	   <ss:Alignment ss:Vertical="Center" ss:Horizontal="Center" ss:WrapText="1"/>
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <ss:Font ss:Bold="1"/>
	  </ss:Style>
		<ss:Style ss:ID="s16" ss:Name="Euro">
		   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
		<ss:Style ss:ID="s17" ss:Name="Total">
		   	<ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
		<ss:Style ss:ID="s18" ss:Name="Total1">
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3" ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
		<ss:Style ss:ID="s19" ss:Name="Total2">
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
	  	<ss:Style ss:ID="s35" ss:Parent="s24">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <Font ss:Bold="1"/>
	   <ss:Interior/>
	   <ss:NumberFormat ss:Format="#,##0"/>
	  </ss:Style>
	  <ss:Style ss:ID="s36" ss:Parent="s20">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <Font ss:Bold="1"/>
	   <ss:Interior/>
	  </ss:Style>
	  <ss:Style ss:ID="s41" ss:Parent="s20">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <Font ss:Bold="1"/>
	   <ss:Interior/>
	   <ss:NumberFormat ss:Format="Percent"/>
	  </ss:Style>
	  <ss:Style ss:ID="s37" ss:Parent="s21">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <ss:Interior/>
	   <ss:NumberFormat ss:Format="#,##0"/>
	  </ss:Style>
	  <ss:Style ss:ID="s38" ss:Parent="s20">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <ss:Interior/>
	  </ss:Style>
	<ss:Style ss:ID="s39">
	   <ss:Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Double" ss:Weight="3"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	  </ss:Style>
	<ss:Style ss:ID="s40">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Double" ss:Weight="3"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <ss:Interior/>
	  </ss:Style>
	</ss:Styles>
	    <Worksheet ss:Name="Liste des Lignes par Site">
	      <Table>
	            <ss:Column ss:Width="100"/>
	            <ss:Column ss:Width="330"/>
	            <ss:Column ss:Width="330"/>
	            <ss:Column ss:Width="90"/>
	            <ss:Column ss:Width="250"/>
	            <ss:Column ss:Width="80"/>
	            <ss:Column ss:Width="80"/>
	            <ss:Column ss:Width="80"/>
	            <ss:Column ss:Width="80"/>
				<ss:Column ss:Width="80"/>
	            <ss:Column ss:Width="100"/>
	            <ss:Column ss:Width="80"/>
				<ss:Column ss:Width="80"/>
				<ss:Column ss:Width="80"/>
	            <cfoutput>
				<ss:Row>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">SIREN/SIRET</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Adresse 1</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Adresse 2</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Code Postal</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Commune</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Ligne</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Durée OP Alternatif (mn)</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Durée FT (mn)</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Part FT (P-1)</ss:Data>
	                </ss:Cell>
					<ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Part FT</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Facturé OP Alternatif</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Facturé FT</ss:Data>
	                </ss:Cell>
					<ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Part FT (P-1)</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Part FT</ss:Data>
	                </ss:Cell>
	            </ss:Row>
				</cfoutput>
 	           	<cfoutput query="qGetData">
					<ss:Row>
			            <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#code_site#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#Adresse1#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#Adresse2#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#zipcode#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#Commune#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#sous_tete#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s37">
		                    <ss:Data ss:Type="Number">#val(duree_autre)#</ss:Data>
		                </ss:Cell>
		                 <ss:Cell ss:StyleID="s37">
		                    <ss:Data ss:Type="Number">#val(duree_ft)#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s38">
		                    <ss:Data ss:Type="Number">#(val(duree_FT_avant)/IIf((val(duree_FT_avant)+val(duree_autre_avant)) NEQ 0,DE(val(duree_FT_avant)+val(duree_autre_avant)),DE(1)))#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s38">
		                    <ss:Data ss:Type="Number">#(val(duree_FT)/IIf((val(duree_FT)+val(duree_autre)) NEQ 0,DE(val(duree_FT)+val(duree_autre)),DE(1)))#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s16">
		                    <ss:Data ss:Type="Number">#val(montant_autre)#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s16">
		                    <ss:Data ss:Type="Number">#val(montant_ft)#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s38">
		                    <ss:Data ss:Type="Number">#(val(montant_FT_avant)/IIf((val(montant_FT_avant)+val(montant_autre_avant)) NEQ 0,DE(val(montant_FT_avant)+val(montant_autre_avant)),DE(1)))#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s38">
		                    <ss:Data ss:Type="Number">#(val(montant_FT)/IIf((val(montant_FT)+val(montant_autre)) NEQ 0,DE(val(montant_FT)+val(montant_autre)),DE(1)))#</ss:Data>
		                </ss:Cell>                
		            </ss:Row>
				</cfoutput>
				<cfoutput>
				<ss:Row>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s17">
	                    <ss:Data ss:Type="String">TOTAL</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s35" ss:Formula="=SUM(R[-#Evaluate(qGetData.recordcount)#]C:R[-1]C)">
						<ss:Data ss:Type="Number"></ss:Data>
					</ss:Cell>
	                <ss:Cell ss:StyleID="s35" ss:Formula="=SUM(R[-#Evaluate(qGetData.recordcount)#]C:R[-1]C)">
						<ss:Data ss:Type="Number"></ss:Data>
					</ss:Cell>
	                <ss:Cell ss:StyleID="s41">
	                    <ss:Data ss:Type="Number">#(val(total_minutes_ft_avant)/IIf((val(total_minutes_ft_avant)+val(total_minutes_autre_avant)) NEQ 0,DE(val(total_minutes_ft_avant)+val(total_minutes_autre_avant)),DE(1)))#</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s41" ss:Formula="=IF((RC[-3]+RC[-2])=0,0,RC[-2]/(RC[-3]+RC[-2]))">
	                    <ss:Data ss:Type="Number"></ss:Data>
	                </ss:Cell>
					<ss:Cell ss:StyleID="s17" ss:Formula="=SUM(R[-#Evaluate(qGetData.recordcount)#]C:R[-1]C)">
						<ss:Data ss:Type="Number"></ss:Data>
					</ss:Cell>
					<ss:Cell ss:StyleID="s19" ss:Formula="=SUM(R[-#Evaluate(qGetData.recordcount)#]C:R[-1]C)">
						<ss:Data ss:Type="Number"></ss:Data>
					</ss:Cell>
					<ss:Cell ss:StyleID="s41">
	                    <ss:Data ss:Type="Number">#(val(total_montant_ft_avant)/IIf((val(total_montant_ft_avant)+val(total_montant_autre_avant)) NEQ 0,DE(val(total_montant_ft_avant)+val(total_montant_autre_avant)),DE(1)))#</ss:Data>
	                </ss:Cell>
					<ss:Cell ss:StyleID="s41" ss:Formula="=IF((RC[-3]+RC[-2])=0,0,RC[-2]/(RC[-3]+RC[-2]))">
	                    <ss:Data ss:Type="Number"></ss:Data>
	                </ss:Cell>
	                </cfoutput>
	            </ss:Row>
	        </Table>
	    </Worksheet>
 	</Workbook>