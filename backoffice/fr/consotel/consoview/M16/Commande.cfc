<cfcomponent displayname="Commande" output="false">
	
	<!--- FOUNIR LA LISTE DES COMMANDES SELON LE POOL SÉLECTIONNÉ --->
	
	<cffunction name="fournirListeCommandeV2" access="remote" returntype="Query" output="false" hint="FOUNIR LA LISTE DES COMMANDES SELON LE POOL SÉLECTIONNÉ">
		<cfargument name="idPool" 	 required="true"  type="numeric" 	default="" displayname="numeric pool" 		hint=""/>
		<cfargument name="dateDebut" required="true"  type="date" 		default="" displayname="date dateDebut" 	hint=""/>
		<cfargument name="dateFin" 	 required="true"  type="date" 		default="" displayname="date dateFin" 		hint=""/>
		<cfargument name="clef" 	 required="false" type="string" 	default="" displayname="string clef" 		hint=""/>
		<cfargument name="segment"   required="false" type="numeric" 	default="" displayname="numeric segment" 	hint=""/>
		
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.fournirListeCommande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 		value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#Session.USER.CLIENTACCESSID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 			value="#segment#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_debut" 		value="#lsdateFormat(dateDebut,'YYYY/MM/DD')#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_fin" 		value="#lsdateFormat(dateFin,'YYYY/MM/DD')#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 			value="#clef#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_pool" 			value="#idPool#">			
			<cfprocresult name="p_retour">			
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>

	<!--- ENREGISTRE UNE COMMANDE --->
	
	<cffunction name="enregistrerCommande" access="remote" returntype="numeric" output="false" hint="ENREGISTRE UNE COMMANDE">		
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idCompte" 			required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idSousCompte" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idOperateur" 			required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="idContactRevendeur" 	required="true" type="numeric" 	default="0" 	displayname="numeric segment" hint="" />
		<cfargument name="idTransporteur" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="numeroTracking" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="libelleCommande"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="dateOperation"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="dateLivraison"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="commentaires" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="boolDevis" 			required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idTypeCommande" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idInventaireOpe" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idGroupeRepere" 		required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="numeroCommande" 		required="true" type="string" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idProfileEquipement"	required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="idSiteFacturation"	required="true" type="numeric" 	default="NULL"	displayname="date dateFin" hint="" />
		<cfargument name="libelleAttention"	    required="true" type="String" 	default="NULL"	displayname="date dateFin" hint="" />
		<cfargument name="articles" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.enregistrerOperation">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcompte" 			value="#idCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsous_compte" 		value="#idSousCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperateur" 		value="#idOperateur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcontact_revendeur" value="#idContactRevendeur#" null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idtransporteur" 		value="#idTransporteur#" null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_livraison" 	value="#idSiteDeLivraison#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 			   variable="p_date_effet_prevue" 	value="#dateLivraison#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idgroupe_repere" 	value="#idGroupeRepere#">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	  null="false" variable="p_article" 			value="#tostring(articles)#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_profil_commande" 	value="#idProfileEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="P_idsite_facturation " value="#idSiteFacturation#">
 			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_v1" 					value=" ">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_v2"				 	value=" ">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_v3"				 	value=" ">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_v4"				 	value=" ">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_v5"				 	value=" ">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_attention"   value="#libelleAttention#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperation" >
		</cfstoredproc>
				
		<cfreturn p_idoperation>
	</cffunction>
		
	<!--- ENREGISTRE DES COMMANDES - PRINCIPALEMENT POUR LA RESILIATION... --->
	
	<cffunction name="enregistrerCommandes" access="remote" returntype="Array" output="false" hint="ENREGISTRE DES COMMANDES">
	      <cfargument name="commandes" required="true" type="any"/>
			
			<cfset idsCommande = ArrayNew(1)>
 			<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
			<cfset index = 1>
			<cfset checkOK = 1>

 			<cfloop index="idx" from="1" to="#ArrayLen(commandes)#">
				<cfif checkOK GTE 1>
					<cfset idGestionnaire = commandes[idx].COMMANDE.IDGESTIONNAIRE>
					<cfset idPoolGestionnaire = commandes[idx].COMMANDE.IDPOOL_GESTIONNAIRE>
					<cfset idCompte = commandes[idx].COMMANDE.IDCOMPTE_FACTURATION>
					<cfset idSousCompte = commandes[idx].COMMANDE.IDSOUS_COMPTE>
					<cfset idOperateur = commandes[idx].COMMANDE.IDOPERATEUR>
					<cfset idRevendeur = commandes[idx].COMMANDE.IDREVENDEUR>
					<cfset idContactRevendeur = commandes[idx].COMMANDE.IDCONTACT>
					<cfset idTransporteur = commandes[idx].COMMANDE.IDTRANSPORTEUR>
					<cfset idSiteDeLivraison = commandes[idx].COMMANDE.IDSITELIVRAISON>
					<cfset numeroTracking = commandes[idx].COMMANDE.NUMERO_TRACKING>
					<cfset libelleCommande = commandes[idx].COMMANDE.LIBELLE_COMMANDE>
					<cfset refClient = commandes[idx].COMMANDE.REF_CLIENT1>
					<cfset refRevendeur = commandes[idx].COMMANDE.REF_OPERATEUR>
					<cfset dateOperation = DateFormat(commandes[idx].COMMANDE.DATE_COMMANDE,'YYYY-MM-DD')>
					<cfset dateLivraison = DateFormat(commandes[idx].COMMANDE.LIVRAISON_PREVUE_LE,'YYYY-MM-DD')>
					<cfset commentaires = commandes[idx].COMMANDE.COMMENTAIRES>
					<cfset boolDevis = commandes[idx].COMMANDE.BOOL_DEVIS>
					<cfset montant = commandes[idx].COMMANDE.MONTANT>
					<cfset segmentFixe = commandes[idx].COMMANDE.SEGMENT_FIXE>
					<cfset segmentMobile = commandes[idx].COMMANDE.SEGMENT_MOBILE>
					<cfset idTypeCommande = commandes[idx].COMMANDE.IDTYPE_COMMANDE>
					<cfset idGroupeRepere = commandes[idx].COMMANDE.IDGROUPE_REPERE>
					<cfset articles = tostring(commandes[idx].ARTICLES)>
					<cfset numeroCommande = commandes[idx].COMMANDE.NUMERO_COMMANDE>
					<cfset idProfileEquipement = commandes[idx].COMMANDE.IDPROFIL_EQUIPEMENT>
					
					<cfset libelleAttention = commandes[idx].COMMANDE.LIBELLE_TO><!--- ICI RAJOUT LIBELLEATTENTION --->
					<cfset idSiteFacturation = commandes[idx].COMMANDE.IDSITEFACTURATION>
					<cfset V1 = commandes[idx].COMMANDE.V1>
					<cfset V2 = commandes[idx].COMMANDE.V2>
					<cfset V3 = commandes[idx].COMMANDE.V3>
					<cfset V4 = commandes[idx].COMMANDE.V4>
					<cfset V5 = commandes[idx].COMMANDE.V5>

					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.enregistrerOperation">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#idRacine#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idGestionnaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcompte" 			value="#idCompte#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsous_compte" 		value="#idSousCompte#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperateur" 		value="#idOperateur#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcontact_revendeur" value="#idContactRevendeur#" 	null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idtransporteur" 		value="#idTransporteur#" 		null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_livraison" 	value="#idSiteDeLivraison#">	
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 			   variable="p_date_effet_prevue" 	value="#dateLivraison#" >			
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
						<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idgroupe_repere" 	value="#idGroupeRepere#">
						<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	  null="false" variable="p_article" 			value="#tostring(articles)#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_profil_commande" 	value="#idProfileEquipement#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_facturation"	value="#idSiteFacturation#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_V1" 					value="#V1#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_V2" 					value="#V2#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_V3" 					value="#V3#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_V4" 					value="#V4#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_V5" 					value="#V5#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_attention"   value="#libelleAttention#">
						<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperation">
					</cfstoredproc>
					<cfset checkOK =  p_idoperation>
					<cfset ArrayAppend(idsCommande, checkOK)>
				</cfif>
			</cfloop>
		<cfreturn idsCommande>  
	</cffunction>

	<!--- ENREGISTRE UN MODÈLE DE COMMANDE --->
	
	<cffunction name="sauvegardeModele" access="remote" returntype="numeric" output="false" hint="ENREGISTRE UN MODÈLE DE COMMANDE">
		<cfargument name="idRacine" 				required="true" type="numeric"/>
		<cfargument name="idGestionnaire" 			required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 		required="true" type="numeric"/>
		<cfargument name="idPoolgestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idOperateur" 				required="true" type="numeric"/>
		<cfargument name="idrevendeur" 				required="true" type="numeric"/>
		<cfargument name="idContactRevendeur" 		required="true" type="numeric" default="NULL"/>
		<cfargument name="idSiteLivraison" 			required="true" type="numeric" default="NULL"/>
		<cfargument name="libelleOperation" 		required="true" type="string"/>
		<cfargument name="referenceClient" 			required="true" type="string"/>
		<cfargument name="referenceClient2"			required="true" type="string"  default="NULL"/>
		<cfargument name="referenceOperateur" 		required="true" type="string"/>
		<cfargument name="commentaires" 			required="true" type="string" default="NULL"/>
		<cfargument name="montant" 					required="true" type="numeric"/>
		<cfargument name="segmentFix" 				required="true" type="numeric"/>
		<cfargument name="segmentMobile" 			required="true" type="numeric"/>
		<cfargument name="inventaireTypeOpe" 		required="true" type="string"/>
		<cfargument name="idGroupePrefere" 			required="true" type="numeric" default="NULL"/>
		<cfargument name="articles" 				required="true" type="string"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.SaveModeleCommande">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpoolgestionnaire" 		value="#idPoolgestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 				value="#idOperateur#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 				value="#idrevendeur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 		value="#idContactRevendeur#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 		value="#idSiteLivraison#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_operation" 		value="#libelleOperation#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_client" 				value="#referenceClient#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_revendeur" 			value="#referenceOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_commentaires" 			value="#commentaires#">
				<cfprocparam type="in" cfsqltype="CF_SQL_NUMERIC" variable="p_montant" 					value="#montant#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment_fixe" 			value="#segmentFix#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment_mobile" 			value="#segmentMobile#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_type_ope" 			value="#inventaireTypeOpe#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 			value="#idGroupePrefere#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" 	  variable="p_articles" 				value="#tostring(articles)#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 				value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_client_2" 			value="#referenceClient2#">
				<cfprocparam type="OUT" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- MAJ D'UN MODÈLE DE COMMANDE --->
	
	<cffunction name="majModele" access="remote" returntype="numeric" output="false" hint="MAJ D'UN MODÈLE DE COMMANDE">
		<cfargument name="idmodele" 				required="true" type="numeric"/>
		<cfargument name="libelleoperation" 		required="true" type="string"/>
		<cfargument name="idPoolgestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idRacine" 				required="true" type="numeric"/>
		<cfargument name="idGestionnaire" 			required="true" type="numeric"/>
		<cfargument name="articles" 				required="true" type="string"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.majModele">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER"  variable="p_idmodele" 				value="#idmodele#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR"  variable="p_libelle_operation" 		value="#libelleoperation#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER"  variable="p_idpoolgestionnaire" 		value="#idPoolgestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	   variable="p_article" 				value="#tostring(articles)#" null="false">
				<cfprocparam type="OUT" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOUNIT LA LISTE DES MODÈLE DE COMMANDE APPARTENANT À UN POOL --->
	
	<cffunction name="fournirListeModele" access="remote" returntype="Query" output="false" hint="FOUNIT LA LISTE DES MODÈLE DE COMMANDE APPARTENANT À UN POOL">
		<cfargument name="idPool" required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.getListeModeleCommande">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 				value="#idPool#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOUNIT LES ARTICLES D'UN MODÈLE DE COMMANDE APPARTENANT À UN POOL --->
	
	<cffunction name="getModeleCommande" access="remote" returntype="query" output="false" hint="FOUNIT LES ARTICLES D'UN MODÈLE DE COMMANDE APPARTENANT À UN POOL">
		<cfargument name="idModele" required="true" type="numeric" default=""/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M16.getmodelecommande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idmodele" value="#idModele#">			
			<cfprocresult name="p_retour">			
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>
	
	<!--- MAJ DES COMMANDES --->
	
	<cffunction name="majCommande" access="remote" returntype="4" output="false" hint="MAJ DES COMMANDES">
		<cfargument name="commande" required="true" type="Struct" default="" displayname="struct commande" hint=""/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.majCommande_v3">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idoperation" 			value="#commande.IDCOMMANDE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idgestionnaire" 		value="#Session.USER.CLIENTACCESSID#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idtransporteur" 		value="#commande.IDTRANSPORTEUR#" 	null="#iif((commande.IDTRANSPORTEUR eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idsite_livraison" 		value="#commande.IDSITELIVRAISON#" 	null="#iif((commande.IDSITELIVRAISON eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idcontact_revendeur" 	value="#commande.IDCONTACT#" 		null="#iif((commande.IDCONTACT eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_operation" 		value="#commande.NUMERO_COMMANDE#">		
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_tracking" 		value="#commande.NUMERO_TRACKING#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_operation" 		value="#commande.LIBELLE_COMMANDE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_client" 			value="#commande.REF_CLIENT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_revendeur" 			value="#commande.REF_OPERATEUR#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 				variable="p_date_effet_prevue" 		value="#lsdateFormat(commande.LIVRAISON_PREVUE_LE,'YYYY/MM/DD')#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_commentaires" 			value="#commande.COMMENTAIRES#">
			<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" null="false" 	variable="p_montant" 				value="#commande.MONTANT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_attention"		value="#commande.LIBELLE_TO#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 				variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour> 
	</cffunction>

	<!--- MAJ LES ARTICLES --->
	
	<cffunction name="majArtilcesOperation" access="remote" returntype="numeric" output="false" hint="MAJ LES ARTICLES" blockfactor="1">
		<cfargument name="idOperation" 	required="true" type="numeric"/>
		<cfargument name="articles" 	required="true" type="string"/>
		
		<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONOPERATION.UPDATE_ARTICLES_OP" blockfactor="1">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperation" 	value="#idOperation#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_iduser" 		value="#idGestionnaire#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 		variable="p_articles" 		value="#tostring(articles)#"/>
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>

	<!--- RECUPÈRE LES INFORMATION CONCERNANT LA COMMANDE SÉLECTIONNÉE --->
	
	<cffunction name="fournirDetailOperation" access="remote" returntype="Query" output="false" blockfactor="1" hint="RECUPÈRE LES INFORMATION CONCERNANT LA COMMANDE SÉLECTIONNÉE">
		<cfargument name="idCommande" 	required="true" type="numeric"/>
		
		<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.fournirDetailOperation" blockfactor="1">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 	value="#idCommande#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idgestionnaire" value="#idGestionnaire#"/>
			<cfprocresult name="p_retour">			
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>

	<!--- RECUPÈRE LES INFORMATIONS CONCERNANT LES MATRICULES --->
	
	<cffunction name="getEmployFromMat" access="remote" returntype="Query" hint="RECUPÈRE LES INFORMATIONS CONCERNANT LES MATRICULES">
		<cfargument name="strMatricule" 	required="true" type="string" />
		
		<cfset IDRACINE = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.getEmployFromMat" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDRACINE#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#strMatricule#"/>
			<cfprocresult name="p_retour">			
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>

	<!--- ENREGISTRE UNE COMMANDE --->

	<cffunction name="enregistrerparametres" access="remote" returntype="numeric" output="false" hint="ENREGISTRE UNE COMMANDE">		
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idCompte" 			required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idSousCompte" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idOperateur" 			required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="idContactRevendeur" 	required="true" type="numeric" 	default="0" 	displayname="numeric segment" hint="" />
		<cfargument name="idTransporteur" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="numeroTracking" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="libelleCommande"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="refClient2" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="dateOperation"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="dateLivraison"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="commentaires" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="boolDevis" 			required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idTypeCommande" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idInventaireOpe" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idGroupeRepere" 		required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="numeroCommande" 		required="true" type="string" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idProfileEquipement"	required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="idSiteFacturation"	required="true" type="numeric" 	default="NULL"	displayname="date dateFin" hint="" />
		<cfargument name="libelleAttention"	    required="true" type="String" 	default="NULL"	displayname="date dateFin" hint="" />
		
			<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.enregistrerparametres">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#IDRACINE#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcompte" 			value="#idCompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsous_compte" 		value="#idSousCompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperateur" 		value="#idOperateur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcontact_revendeur" value="#idContactRevendeur#" null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idtransporteur" 		value="#idTransporteur#" null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_livraison" 	value="#idSiteDeLivraison#">	
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 			   variable="p_date_effet_prevue" 	value="#dateLivraison#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idgroupe_repere" 	value="#idGroupeRepere#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_profil_commande" 	value="#idProfileEquipement#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="P_idsite_facturation " value="#idSiteFacturation#">
	 			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_v1" 					value=" ">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_v2"				 	value=" ">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_v3"				 	value=" ">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_v4"				 	value=" ">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true"  variable="p_v5"				 	value=" ">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_attention"   value="#libelleAttention#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client_2"   		value="#refClient2#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperation" >
			</cfstoredproc>
				
		<cfreturn p_idoperation>
	</cffunction>
	
	<!--- FOURNIT LES ARTICLES D'UNE COMMANDE --->

	<cffunction name="fournirArticlesCommande" access="remote" returntype="any" output="false" hint="FOURNIT LES ARTICLES D'UNE COMMANDE">
		<cfargument name="idCommande" required="true" type="numeric" displayname="struct commande"/>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.FOURNIRARTICLESOPERATION" blockfactor="10">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idCommande#">			
				<cfprocresult name="articlexml">
			</cfstoredproc>
			
		<cfreturn articlexml>
	</cffunction>

	<!--- RÉCUPÈRE LES DIFFÉRENTES PROPRIÉTÉS D'UN REVENDEUR CONCERNANT DIFFÉRENTS DELAIS --->
	
	<cffunction name="getRevendeurSLA" access="remote" returntype="query" output="false" hint="obtenir les sla d'un revendeur">
		<cfargument name="idrevendeur" required="true" type="numeric" default="" displayname="numeric idrevendeur" hint="ID du revendeur a renommer"/>
		<cfargument name="idOperateur" required="true" type="numeric" default="" displayname="numeric idOperateur" hint="ID de l'operateur"/>                
			
			<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_Revendeur_SLA_V2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur"  value="#idrevendeur#">                        
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idOperateur"  value="#idOperateur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#Session.PERIMETRE.ID_GROUPE#">					
				<cfprocresult name="p_retour">
			</cfstoredproc> 
			         
		<cfreturn p_retour>        
	</cffunction>

</cfcomponent>
