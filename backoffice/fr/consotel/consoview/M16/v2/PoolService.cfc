<cfcomponent displayname="fr.consotel.consoview.M16.v2.PoolService" output="false">	
	
	<!--- LA LISTE DES POOL DE GESTION D'UN GESTIONNAIRE DANS UNE RACINE EN FONCTION DU SEGMENT --->
	
	<cffunction name="fournirListePoolsDuGestionnaire" access="remote" returntype="Query" description="RETOURNE LA LISTE DES POOLS DE GESTIONNAIRES DUN GESTIONNAIRE">		
		<cfargument name="idgestionnaire" 	type="numeric" required="false">
		<cfargument name="idracine" 		type="numeric" required="false">
		
		<cfif isdefined("idgestionnaire")>
			<cfset _idgestionnaire= idgestionnaire>
		<cfelse>
			<cfset _idgestionnaire= SESSION.USER.CLIENTACCESSID>
		</cfif>
		
		<cfif isdefined("idracine")>		
			<cfset _idracine = idracine>
			<cfelse>
			<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
		</cfif>
		
		<cfstoredproc 	datasource="#session.offredsn#" procedure="pkg_m16.getpool_of_gestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idgestionnaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idracine#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>

	<!--- FOURNIT LA LISTE DES PROFILES CLIENT --->	
	
	<cffunction name="fournirListeProfils" access="remote" returntype="Query" hint="FOURNIT LA LISTE DES PROFILES CLIENT">	
		<cfargument name="idsegment" 			type="numeric"	required="true" />
		<cfargument name="idracine" 			type="numeric"	required="false" />
		<cfargument name="idgestionnaire" 		type="numeric"	required="false" />
			
			<cfif isdefined("idgestionnaire")>
				<cfset _idgestionnaire= idgestionnaire>
			<cfelse>
				<cfset _idgestionnaire= SESSION.USER.CLIENTACCESSID>
			</cfif>

			<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
			
			<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_M16.LIST_PROFIL_GEST_ACINE">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#_idgestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine"  		value="#_idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment"  		value="#idsegment#">
				<cfprocresult name="p_result">
			</cfstoredproc>

		<cfreturn p_result>
	</cffunction>
	
	<!---  FOURNIT LA LISTE DES SITES D'UN POOLS DE GESTION D UN GESTIONNAIRE SUIVANT UNE CLEFS DE RECHERCHE SI L ID DU GESTIONNAIRE EST ÉGALE É 0. ON FOURNIT TOUS LES SITES DE LIVRAISON. CLEF = NOM_SITE | SP_REFERENCE | SP_CODE_INTERNE | COMMUNE --->
	
	<cffunction name="fournirListeSitesLivraisonsPoolGestionnaire" access="remote"
				returntype="query" output="false" hint=" LISTE DES SITES ET SITES D'UN POOL DE GESTION" >
		<cfargument name="idPoolGestion" required="true" type="numeric" />
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.GETSITEOFPOOL" blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" value="#idPoolGestion#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LA LISTE DES ATIONS D'UN PROFIL CLIENT --->

	<cffunction access="remote" name="fournirListeActionProfilCommande" returntype="Query" hint="FOURNIT LA LISTE DES ATIONS D'UN PROFIL CLIENT">
		<cfargument name="idprofil" type="Numeric">
		
			<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.ListeActionProfilCommande">
				<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#SESSION.USER.GLOBALIZATION#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocresult name="p_retour">
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	<!--- LISTE DES D'UN REVENDEUR --->
	
	<cffunction name="fournirPools" access="remote" returntype="query" output="false" hint="LISTE DES D'UN REVENDEUR" >
		<cfargument name="isrevendeur" required="true" type="Numeric" />
	
			<cfset idracine = SESSION.PERIMETRE.ID_GROUPE>
	
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_cv_equipement.getpools" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_is_revendeur" value="#isrevendeur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
					
		<cfreturn p_retour>
	</cffunction>
	
</cfcomponent>