<cfcomponent displayname="fr.consotel.consoview.M16.v2.DistributeurService" output="false">

	<!--- RÉCUPERE LES REVENDEURS --->
	
	<cffunction name="fournirRevendeurs" access="remote" returntype="Query" output="false" hint="RÉCUPERE LES REVENDEURS">
		<cfargument name="idRacine" 			required="true" type="numeric"/>
		<cfargument name="idPoolGestion" 		required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 	required="true" type="numeric"/>
		<cfargument name="clefRecherche" 		required="true" type="String" />
		
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.get_revendeur_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#idgestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 				value="#idPoolGestion#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_Chaine" 				value="#clefRecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_idracine" 			value="#idracine#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<!--- LISTER LES AGENCES D'UNE SOCIÉTÉ --->
	
	<cffunction name="get_liste_agence" access="remote" returntype="query" output="false" hint="LISTER LES AGENCES D'UNE SOCIÉTÉ" >
		<cfargument name="IDCDE_CONTACT_SOCIETE" required="false" type="numeric" default="" displayname="numeric IDCDE_CONTACT_SOCIETE" hint="Initial value for the IDCDE_CONTACT_SOCIETEproperty." />
		<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="Initial value for the chaineproperty." />
		
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.get_liste_agence_v3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT_SOCIETE#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
				<cfprocresult name="p_result">
			</cfstoredproc>
			
		<cfreturn p_result>			
	</cffunction>

	<!--- RÉCUPERE LES CONTACTS REVENDEURS POUR L'ENVOI DE MAIL --->
	
	<cffunction name="getContactsRevendeurWithRole" access="remote" returntype="query" output="false" hint="RÉCUPERE LES CONTACTS REVENDEURS POUR L'ENVOI DE MAIL">		
		<cfargument name="idrevendeur" required="true" type="numeric"/>
		<cfargument name="idRole" required="true" type="numeric"/><!--- Resiliation = 6 && Commande = 101 --->
		
			<cfset user = Session.USER.CLIENTACCESSID>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTION_FOURNISSEUR_V1.GETCONTACTSROLESREVENDEUR_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#idrevendeur#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#user#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#idRole#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- RÉCUPÈRE LES REVENDEURS AUTORISÉS --->

	<cffunction name="fournirRevendeursAutorises" access="remote" returntype="Query" hint="RÉCUPÈRE LES REVENDEURS AUTORISÉS">
		<cfargument name="idRacine" 		type="Numeric" required="true">
		<cfargument name="iduser"			type="Numeric" required="true">
		<cfargument name="idtypecommande"	type="Numeric" required="true">
		<cfargument name="idOperateur" 		type="Numeric" required="true">
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.distribAutorises">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_apploginid" 			value="#Session.USER.CLIENTACCESSID#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idtypecommande#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 			value="#idOperateur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>	

	<!--- RÉCUPÈRE LE NOMBRE DE REVENDEURS AUTORISÉS --->
	
	<cffunction name="fournirNumberRevendeursAutorises" access="remote" returntype="Numeric" hint="RÉCUPÈRE LE NOMBRE DE REVENDEURS AUTORISÉS">
		<cfargument name="idRacine" 		type="Numeric" required="true">
		<cfargument name="iduser"			type="Numeric" required="true">
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.numberDistribAutorises">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#idRacine#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" variable="p_apploginid" 			value="#Session.USER.CLIENTACCESSID#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<!--- RÉCUPÈRE LES DIFFÉRENTES PROPRIÉTÉS D'UN REVENDEUR CONCERNANT DIFFÉRENTS DELAIS  --->
	
	<cffunction name="getRevendeurSLA" access="remote" returntype="query" output="false" hint="OBTENIR LES SLA D'UN REVENDEUR">
		<cfargument name="idrevendeur" required="true" type="numeric" default="" displayname="numeric idrevendeur" hint="ID du revendeur a renommer"/>
		<cfargument name="idOperateur" required="true" type="numeric" default="" displayname="numeric idOperateur" hint="ID de l'operateur"/>                
			
			<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_Revendeur_SLA_V2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur"  value="#idrevendeur#">                        
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idOperateur"  value="#idOperateur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#Session.PERIMETRE.ID_GROUPE#">					
				<cfprocresult name="p_retour">
			</cfstoredproc> 
			         
		<cfreturn p_retour>        
	</cffunction>
	
	<cffunction name="getUserProfil" access="remote" returntype="Numeric" output="false" hint="TYPE PROFIL (CLIENT USER DISTIB)">

			<cfset idtypeprofil = SESSION.USER.IDTYPEPROFIL>

		<cfreturn idtypeprofil>
	</cffunction>
	
	<cffunction name="getUserRevendeurs" access="remote" returntype="Array" output="false" hint="TYPE PROFIL (CLIENT USER DISTIB)">
		<cfargument name="idsegment" required="true" type="Numeric"/>        
		<cfargument name="idpool"	 required="true" type="Numeric"/>      

			<cfset listeProfils 	= ArrayNew(1)>
			<cfset listeRevendeurs 	= ArrayNew(1)>
			
			<cfset idracine = session.perimetre.id_groupe>
			<cfset idgest	= session.user.clientaccessid>
			
			<cfset poolsObject  = createObject("component","fr.consotel.consoview.M16.v2.PoolService")>
			<cfset profils  	= poolsObject.fournirListeProfils(idsegment, idracine, idgest)>

			<cfloop query="profils">
				
				<cfif profils.SELECTED EQ 1>
					
					 <cfset ArrayAppend(listeProfils, profils.IDPROFIL_EQUIPEMENT)>
					 
				</cfif>
				
			</cfloop>

			<cfloop index="idx" from="1" to="#ArrayLen(listeProfils)#">
			
				<cfset idprofil = listeProfils[idx]>
				
				<cfset revendeurs = fournirRevendeurs(idracine, idpool, idprofil, "")>
				
				<cfloop query="revendeurs">
							
					<cfset revendeur	 					= structNew()>									
					<cfset revendeur.ADRESSE1				= revendeurs.ADRESSE1>
					<cfset revendeur.ADRESSE2				= revendeurs.ADRESSE2>
					<cfset revendeur.CODE_INTERNE			= revendeurs.CODE_INTERNE>
					<cfset revendeur.COMMENTAIRE			= revendeurs.COMMENTAIRE>
					<cfset revendeur.COMMUNE				= revendeurs.COMMUNE>
					<cfset revendeur.CONTACTS_PARTAGES		= revendeurs.CONTACTS_PARTAGES>
					<cfset revendeur.CONTACTS_PARTAGES		= revendeurs.CONTACTS_PARTAGES>
					<cfset revendeur.DATE_CREATE			= revendeurs.DATE_CREATE>
					<cfset revendeur.DATE_MODIF				= revendeurs.DATE_MODIF>
					<cfset revendeur.DELAI_MIN_LIVRAISON	= revendeurs.DELAI_MIN_LIVRAISON>
					<cfset revendeur.FAX					= revendeurs.FAX>
					<cfset revendeur.FPC					= revendeurs.FPC>
					<cfset revendeur.IDCDE_CONTACT_SOCIETE	= revendeurs.IDCDE_CONTACT_SOCIETE>
					<cfset revendeur.IDRACINE				= revendeurs.IDRACINE>
					<cfset revendeur.IDREVENDEUR			= revendeurs.IDREVENDEUR>
					<cfset revendeur.LIBELLE				= revendeurs.LIBELLE>
					<cfset revendeur.NB_AGENCE				= revendeurs.NB_AGENCE>
					<cfset revendeur.PAYS					= revendeurs.PAYS>
					<cfset revendeur.PAYSCONSOTELID			= revendeurs.PAYSCONSOTELID>
					<cfset revendeur.SIREN					= revendeurs.SIREN>
					<cfset revendeur.TELEPHONE				= revendeurs.TELEPHONE>
					<cfset revendeur.TYPE					= revendeurs.TYPE>
					<cfset revendeur.ZIPCODE				= revendeurs.ZIPCODE>
								
					<cfset ArrayAppend(listeRevendeurs, revendeur)>
						 				
				</cfloop>

			</cfloop>

		<cfreturn listeRevendeurs>
	</cffunction>
	
	<cffunction name="getRevendeurAdresse" access="remote" returntype="Array" output="false" hint="Lister revendeurs, génériques et privés pour une racine donnée. (Catalogue fournisseur)." >
		<cfargument name="segment" 		required="true" type="Numeric"/>
		<cfargument name="idrevendeur" 	required="true" type="Numeric"/>
		
			<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_catalogue_revendeur_V2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#session.perimetre.id_groupe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#session.user.clientaccessid#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfset revendeurAdresse 	= ArrayNew(1)>
			
			<cfloop query="p_retour">
						
				<cfif p_retour.IDREVENDEUR EQ idrevendeur>
					
					<cfset revendeur	 					= structNew()>									
					<cfset revendeur.ADRESSE1				= p_retour.ADRESSE1>
					<cfset revendeur.ADRESSE2				= p_retour.ADRESSE2>
					<cfset revendeur.CODE_INTERNE			= p_retour.CODE_INTERNE>
					<cfset revendeur.COMMENTAIRE			= p_retour.COMMENTAIRE>
					<cfset revendeur.COMMUNE				= p_retour.COMMUNE>
					<cfset revendeur.CONTACTS_PARTAGES		= p_retour.CONTACTS_PARTAGES>
					<cfset revendeur.CONTACTS_PARTAGES		= p_retour.CONTACTS_PARTAGES>
					<cfset revendeur.DATE_CREATE			= p_retour.DATE_CREATE>
					<cfset revendeur.DATE_MODIF				= p_retour.DATE_MODIF>
					<cfset revendeur.DELAI_MIN_LIVRAISON	= p_retour.DELAI_MIN_LIVRAISON>
					<cfset revendeur.FAX					= p_retour.FAX>
					<cfset revendeur.FPC					= p_retour.FPC>
					<cfset revendeur.IDCDE_CONTACT_SOCIETE	= p_retour.IDCDE_CONTACT_SOCIETE>
					<cfset revendeur.IDRACINE				= p_retour.IDRACINE>
					<cfset revendeur.IDREVENDEUR			= p_retour.IDREVENDEUR>
					<cfset revendeur.LIBELLE				= p_retour.LIBELLE>
					<cfset revendeur.NB_AGENCE				= p_retour.NB_AGENCE>
					<cfset revendeur.PAYS					= p_retour.PAYS>
					<cfset revendeur.PAYSCONSOTELID			= p_retour.PAYSCONSOTELID>
					<cfset revendeur.SIREN					= p_retour.SIREN>
					<cfset revendeur.TELEPHONE				= p_retour.TELEPHONE>
					<cfset revendeur.TYPE					= p_retour.TYPE>
					<cfset revendeur.ZIPCODE				= p_retour.ZIPCODE>
								
					<cfset ArrayAppend(revendeurAdresse, revendeur)>
				
				</cfif>	
						
			</cfloop>
			
		<cfreturn revendeurAdresse>		
	</cffunction>
	
	<!--- RECUPERE LES REVENDEURS AUTORISE SELON L'UTILISATEUR ET LE TYPE DE COMMANDE SELECTIONNE --->
	
	<cffunction name="fournirOperateursAutorises" access="public" returntype="Array">
		<cfargument name="iduser"			type="Numeric" required="true">
		<cfargument name="idtypecommande"	type="Numeric" required="true">
		
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m16.opeAutorises">	
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_iduser" 				value="#Session.USER.CLIENTACCESSID#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" value="#idtypecommande#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
						
			<cfset operateurs = ArrayNew(1)>
			<cfset allOperateur = p_retour>
			
			<cfloop index="idx" from="1" to="#p_retour.recordcount#">
				<cfset operateur = structNew()>
				<cfset operateur.LIBELLE = allOperateur['NOM'][idx]>
				<cfset operateur.OPERATEURID = allOperateur['OPERATEURID'][idx]>
				
				<cfset ArrayAppend(operateurs, operateur)>
			</cfloop>
			
		
		<cfreturn operateurs>
	</cffunction>

</cfcomponent>