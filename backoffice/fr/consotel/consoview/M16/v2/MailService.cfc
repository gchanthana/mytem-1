<cfcomponent output="false" displayname="fr.consotel.consoview.M16.v2.MailService" extends="fr.consotel.api.ibis.publisher.handler.ServiceHandler" hint="Mon service d'envoi de amil">

	<!--- FONCTION APPELER PERMETTANT L'ENVOI DE MAILS AVEC PIECES JOINTES --->

	<cffunction name="run" access="remote" output="false" returntype="any" hint="FONCTION APPELER PERMETTANT L'ENVOI DE MAILS AVEC PIECES JOINTES">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered this handler">
			<cfset SUPER.run(ARGUMENTS.eventObject)>
		
			<cfset rsltSenMail	= 0>
			
			<cfset uuidlog	= #ARGUMENTS.eventObject.getEventTarget()#>

<!--- -------------------------------------------- --->	
			<!--- RECUPERATION UUID, IDCOMMANDE, JOBID --->
<!--- -------------------------------------------- --->	

			<cftry>
				
				
				<!--- RECUPERATION MAIL INSERE CONTENANT TOUS LES INFOS --->
				<cfset mailObject  = createObject('component',"fr.consotel.consoview.M16.LogsCommande")>
				<cfset rsltMailObj = mailObject.getlogsCommandeuuid(#uuidlog#)>
	
				<cfset idcommande	= rsltMailObj.IDCOMMANDE>
	
				<!--- RECUPERATION DES PEICES JOINTES A JOINDRE AU MAIL OU NON --->
				<cfset attachObject = createObject('component',"fr.consotel.consoview.M16.v2.AttachementService")>

				<cfset rsltAttachOb = attachObject.fournirAttachements(Val(#idcommande#))>
				
				<cfmail from="BDCMOBILE@consotel.fr" to="dev@consotel.fr" subject="TEST API OK" type="text/html" server="mail.consotel.fr" port="26">
					<cfoutput>#rsltMailObj#</cfoutput>
					<cfoutput>#rsltMailObj#</cfoutput>
					<cfoutput>#rsltAttachOb#</cfoutput>
				</cfmail>
				
			<cfcatch type="any">
				<cfmail from="BDCMOBILE@consotel.fr" to="dev@consotel.fr" subject="TEST API ERROR" type="text/html" server="mail.consotel.fr" port="26">
												
					<cfoutput>#rsltMailObj#</cfoutput>
					<cfoutput>#rsltMailObj#</cfoutput>
					<cfoutput>#rsltAttachOb#</cfoutput>
				</cfmail>
			</cfcatch>
			
			</cftry>

			<!--- SELECTION DES PIECES JOINTES A ENVOYE ('JOIN'=1) ET ENVOIE DE MAIL --->

			<cftry>
				<cfmail from="#rsltMailObj.MAIL_FROM#" to="#rsltMailObj.MAIL_TO#" subject="#rsltMailObj.SUBJECT#" type="text/html" server="mail.consotel.fr" port="26">#rsltMailObj.MESSAGE#
						<cfsilent>
						<cfloop query="rsltAttachOb">
							<cfif rsltAttachOb.JOIN_MAIL EQ 1>
								<cfmailparam file="#ImportTmp & rsltAttachOb.PATH & '/' & rsltAttachOb.FILE_NAME#">
							</cfif>
						</cfloop>
						</cfsilent>
					
				</cfmail>
				<cfset rsltSenMail	= 1>
				
				<!--- MAJ LE LOG DE COMMANDE AVEC LE JOBID --->
				<cfset rsltUpdateJobId = mailObject.updateJobId(uuidlog, jobid)>
				<cfif NOT rsltUpdateJobId GT 0>
					<cfset rsltSenMail	= -1>
				</cfif>
												
			<cfcatch type="any">
				<cfmail from="BDCMOBILE@consotel.fr" to="dev@consotel.fr" subject="TEST API ERROR" type="text/html" server="mail.consotel.fr" port="26">												
					<cfoutput>#rsltMailObj#</cfoutput>
					<cfoutput>#rsltMailObj#</cfoutput>
					<cfoutput>#rsltAttachOb#</cfoutput>
				</cfmail>
			</cfcatch>
			</cftry>

	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="Called when an error has occurred either during creation or execution of IServiceHandler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true"
																									hint="Event object that triggered run() handler">
		<cfargument name="eventHandler" type="string" required="true" hint="The corresponding EVENT_HANDLER supposed to handle eventObject">
		<cfargument name="exceptionObject" type="any" required="false" hint="Optionally the corresponding catched exception (Same structure as CFCATCH scope)">
		<cfmail from="BDC@consotel.fr" to="dev@consotel.fr" subject="SERVICE ERROR HANDLER V1" type="html" server="mail.consotel.fr" port="26">
			<cfoutput>
				JOB_ID : #ARGUMENTS.eventObject.getJobId()#<br />
				REPORT_STATUS : #ARGUMENTS.eventObject.getReportStatus()#<br />
				REPORT_URL : #ARGUMENTS.eventObject.getReportPath()#<br />
				BIP_SERVER : #ARGUMENTS.eventObject.getBipServer()#<br />
				EVENT_TARGET : #ARGUMENTS.eventObject.getEventTarget()#<br />
				EVENT_TYPE : #ARGUMENTS.eventObject.getEventType()#<br />
				EVENT_DISPATCHER : #ARGUMENTS.eventObject.getEventDispatcher()#<br />
				EVENT_HANDLER : #ARGUMENTS.eventHandler#
			</cfoutput>
			<cfdump var="#ARGUMENTS.exceptionObject#" label="EXCEPTION">
		</cfmail>
	</cffunction>

</cfcomponent>