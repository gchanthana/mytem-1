<cfcomponent displayname="fr.consotel.consoview.M16.v2.SiteService" output="false">
	
	<!--- FOURNIT LA LISTE DES SITES D'UN POOLS DE GESTION D UN GESTIONNAIRE SUIVANT UNE CLEFS DE RECHERCHE SI L ID DU GESTIONNAIRE EST ÉGALE É 0. ON FOURNIT TOUS LES SITES DE LIVRAISON. CLEF = NOM_SITE | SP_REFERENCE | SP_CODE_INTERNE | COMMUNE --->
	
	<cffunction name="fournirListeSitesLivraisonsPoolGestionnaire" access="remote" returntype="query" output="false" hint="LISTE DES SITES ET SITE D'UN EMPLOYE">
		<cfargument name="idPoolGestion" required="true" type="numeric" />
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.GETSITEOFPOOL" blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" value="#idPoolGestion#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>

</cfcomponent>