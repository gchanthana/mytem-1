<cfcomponent displayname="CommandeSNCF">
  
	<!--- GENERE UN NUMERO DE COMMANDE UNIQUE --->
	
	<cffunction name="genererNumeroDeCommande" access="remote" returntype="Struct" output="false" hint="GENERE UN NUMERO DE COMMANDE UNIQUE">
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.get_numero_commande">
				<cfprocparam type="OUT" cfsqltype="CF_SQL_VARCHAR" variable="p_numero_commande">									
				<cfprocparam type="OUT" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>
			<cfset obj = StructNew()>
			<cfset obj.NUMERO_COMMANDE = p_numero_commande>
			<cfset obj.RESULT = p_retour>
			
		<cfreturn obj>
	</cffunction>

	<!--- FOURNIT UN NUMERO DE MARCHE UNIQUE (SNCF) --->

	<cffunction name="fournirNumeroMarche" access="remote" returntype="Struct" output="false" hint="FOURNIT UN NUMERO DE MARCHE UNIQUE (SNCF)">
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.get_numero_marche">
				<cfprocparam type="OUT" cfsqltype="CF_SQL_VARCHAR" variable="p_numero_marche">									
				<cfprocparam type="OUT" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>
			<cfset obj = StructNew()>
			<cfset obj.NUMERO_MARCHE = p_numero_marche>
			<cfset obj.RESULT = p_retour>
			
		<cfreturn obj>
	</cffunction>

	<!--- FOURNIT LA LISTE DES PROFILS --->

	<cffunction name="fournirProfilEquipements" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTE DES PROFILS">
		<cfargument name="idPoolGestion" 	required="true" type="numeric" />
		<cfargument name="idGestionnaire" 	required="true" type="numeric" />
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.get_type_commande">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 			value="#idPoolGestion#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT L'OPERATEUR SFR (SNCF) --->

	<cffunction name="fournirOperateur" access="remote" returntype="Query" output="false" hint="FOURNIT L'OPERATEUR SFR (SNCF)">
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.getFabOpSFR">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LES COMPTES (SNCF) --->
	
	<cffunction name="fournirTitulaire" access="remote" returntype="Query" output="false" hint="FOURNIT LES COMPTES (SNCF)">
		<cfargument name="idOperateur" required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.L_CF_RACINE_OP">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" value="#idOperateur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LES SOUS COMPTES (SNCF) --->

	<cffunction name="fournirPointFacturation" access="remote" returntype="Query" output="false" hint="FOURNIT LES SOUS COMPTES (SNCF)">
		<cfargument name="idTitulaire" required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.L_sous_compte_cf">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte_facturation" value="#idTitulaire#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<!--- FOURNIT LA LISTE DES POINTS DE FACTURATION (SNCF) --->	

	<cffunction name="fournirCodeListe" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTE DES POINTS DE FACTURATION (SNCF)">
		<cfargument name="idCompteFacturation" required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.L_code_liste_pf">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idsouscompte" value="#idCompteFacturation#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LA LISTE DES EQUIPEMENTS DU CATALOGUE CLIENT SUIVANT LE PROFIL (SNCF) --->	

	<cffunction name="fournirListeEquipements" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTE DES EQUIPEMENTS DU CATALOGUE CLIENT SUIVANT LE PROFIL (SNCF)">
		<cfargument name="idGestionnaire" 			required="true" type="numeric"/>
		<cfargument name="idPoolGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 		required="true" type="numeric"/>
		<cfargument name="idFournisseur" 			required="true" type="numeric"/>
		<cfargument name="clefRecherche" 			required="true" type="string"/>
		
		<cfset idCategorieEquipement = 0>		
		<cfset idTypeEquipement = 0>		
		<cfset typeFournis = 1>
		<cfset idGammeFournis = 0>
		<cfset niveau = 1>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.SearchCatalogueclient_V2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#idPoolGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#idCategorieEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#idTypeEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#idFournisseur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#idGammeFournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 					value="#clefRecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournis" 			value="#typeFournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_niveau" 					value="#niveau#" null="true">
				<cfprocparam type="In" value="#session.user.globalization#" cfsqltype="CF_SQL_VARCHAR" >
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LA LISTE DES ABONNEMENTS/OPTIONS SUIVANT LE PROFIL (SNCF) --->	

	<cffunction name="fournirListeAbonnementsOptions" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTE DES ABONNEMENTS/OPTIONS SUIVANT LE PROFIL (SNCF)">
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idOperateur" 			required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 	required="true" type="numeric"/>		
		<cfset codeLangue=session.user.globalization>
		
		<cfset idGestionnaire = Session.USER.CLIENTACCESSID>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.Liste_Produits_profil_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" 			value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" 			value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 		value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocparam  value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR" >
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- SAUVEGARDE UN MODELE DE COMMANDE (SNCF) --->

	<cffunction name="sauvegardeModele" access="remote" returntype="numeric" output="false" hint="SAUVEGARDE UN MODELE DE COMMANDE (SNCF)">
		<cfargument name="idRacine" 				required="true" type="numeric"/>
		<cfargument name="idGestionnaire" 			required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 		required="true" type="numeric"/>
		<cfargument name="idPoolgestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idOperateur" 				required="true" type="numeric"/>
		<cfargument name="idrevendeur" 				required="true" type="numeric"/>
		<cfargument name="idContactRevendeur" 		required="true" type="numeric" default="NULL"/>
		<cfargument name="idSiteLivraison" 			required="true" type="numeric" default="NULL"/>
		<cfargument name="libelleOperation" 		required="true" type="string"/>
		<cfargument name="referenceClient" 			required="true" type="string"/>
		<cfargument name="referenceOperateur" 		required="true" type="string"/>
		<cfargument name="commentaires" 			required="true" type="string" default="NULL"/>
		<cfargument name="montant" 					required="true" type="numeric"/>
		<cfargument name="segmentFix" 				required="true" type="numeric"/>
		<cfargument name="segmentMobile" 			required="true" type="numeric"/>
		<cfargument name="inventaireTypeOpe" 		required="true" type="string"/>
		<cfargument name="idGroupePrefere" 			required="true" type="numeric" default="NULL"/>
		<cfargument name="articles" 				required="true" type="string"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_SNCF.SaveModeleCommande_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpoolgestionnaire" 		value="#idPoolgestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 				value="#idOperateur#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 				value="#idrevendeur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 		value="#idContactRevendeur#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 		value="#idSiteLivraison#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_operation" 		value="#libelleOperation#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_client" 				value="#referenceClient#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_revendeur" 			value="#referenceOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_commentaires" 			value="#commentaires#">
				<cfprocparam type="in" cfsqltype="CF_SQL_NUMERIC" variable="p_montant" 					value="#montant#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment_fixe" 			value="#segmentFix#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment_mobile" 			value="#segmentMobile#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_type_ope" 			value="#inventaireTypeOpe#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 			value="#idGroupePrefere#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" 	  variable="p_articles" 				value="#tostring(articles)#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 				value="#idRacine#">
				<cfprocparam type="OUT" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LA LISTES DES MODELES ENREGISTREES (SNCF) --->

	<cffunction name="fournirListeModele" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTES DES MODELES ENREGISTREES (SNCF)">
		<cfargument name="idPool" required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.getListeModeleCommande">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 				value="#idPool#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LA LISTE DES GESTIONNAIRES POUR LESQUELS ON PEUT FAIRE UNE COMMANDE (SNCF) --->	

	<cffunction name="functionActePourOld" access="remote" returntype="Query" hint="FOURNIT LA LISTE DES GESTIONNAIRES POUR LESQUELS ON PEUT FAIRE UNE COMMANDE (SNCF)">
		
			<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
			
			<cfquery datasource="#session.offreDSN#" name="p_result">
				SELECT DISTINCT pg.app_loginid,al.login_nom,al.login_prenom,al.login_email,pg.idprofil_commande,pg.idpool,pl.libelle,pl.idrevendeur
				FROM pool_gestionnaire pg,app_login al,pool_flotte pl
				 
				   
				WHERE pg.app_loginid=al.app_loginid
				AND pg.idpool NOT IN 
				(
				SELECT pg.idpool
				   FROM pool_gestionnaire pg,app_login al,pool_flotte pl
				   WHERE pg.app_loginid=al.app_loginid
				   AND pg.idpool = pl.idpool
				   AND pl.idracine = #IDRACINE#
				   AND pl.idrevendeur IS NOT NULL
				 )  
				   
				AND pg.idpool = pl.idpool
				AND pl.idracine = #IDRACINE#
				ORDER BY al.login_nom,al.login_prenom,al.login_email
			</cfquery>
			
		<cfreturn p_result>
	</cffunction>

	<!--- FOURNIT LA LISTE DES GESTIONNAIRES POUR LESQUELS ON PEUT FAIRE UNE COMMANDE (SNCF) --->

	<cffunction name="functionActePour" access="remote" returntype="Query" hint="FOURNIT LA LISTE DES GESTIONNAIRES POUR LESQUELS ON PEUT FAIRE UNE COMMANDE">
		<cfargument name="idProfilEquipement" required="true" type="numeric"/>
		<cfargument name="idRevendeur" 		  required="true" type="numeric"/>

			<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.gestionnaires_client_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#IDRACINE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 			value="#idRevendeur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

<!--- ######## PARTIE GESTION DES LOGGINS RAJOUT POUR LA COMMANDE SNCF ######## --->

	<!--- CREATION D'UN LOGIN (SNCF) --->

	<cffunction name="createUtilisateur" access="remote" returntype="numeric" output="false" hint="CREATION D'UN LOGIN (SNCF)">
		<cfargument name="nom" 				required="true" type="string"/>
		<cfargument name="prenom" 			required="true" type="string"/>
		<cfargument name="adresse" 			required="true" type="string"/>
		<cfargument name="codepostal" 		required="true" type="string"/>
		<cfargument name="ville" 			required="true" type="string"/>
		<cfargument name="telephone" 		required="true" type="string"/>
		<cfargument name="direction" 		required="true" type="string"/>
		<cfargument name="email" 			required="true" type="string"/>
		<cfargument name="password" 		required="true" type="string"/>
		<cfargument name="restrictionIP"	required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.add_app_login_sncf">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_NOM" 		value="#nom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_PRENOM" 	value="#prenom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_adresse" 			value="#adresse#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_codepostal" 		value="#codepostal#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ville" 			value="#ville#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_telephone" 		value="#telephone#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_direction" 		value="#direction#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_EMAIL" 		value="#email#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_PWD" 		value="#password#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_RESTRICTION_IP" 	value="#restrictionIP#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_IDRACINE" 		value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
						
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.addDefaultProfil">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#p_retour#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>

	<!--- MODIFICATION D'UN LOGIN (SNCF) --->

	<cffunction name="updateUtilisateur" access="remote" returntype="numeric" output="false" hint="MODIFICATION D'UN LOGIN (SNCF)">
		<cfargument name="appLogin"			required="true" type="numeric"/>
		<cfargument name="nom" 				required="true" type="string"/>
		<cfargument name="prenom" 			required="true" type="string"/>
		<cfargument name="adresse" 			required="true" type="string"/>
		<cfargument name="codepostal" 		required="true" type="string"/>
		<cfargument name="ville" 			required="true" type="string"/>
		<cfargument name="telephone" 		required="true" type="string"/>
		<cfargument name="direction" 		required="true" type="string"/>
		<cfargument name="email" 			required="true" type="string"/>
		<cfargument name="password" 		required="true" type="string"/>
		<cfargument name="restrictionIP"	required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.upd_app_login_sncf">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_APP_LOGINID" 		value="#appLogin#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_NOM" 		value="#nom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_PRENOM" 	value="#prenom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_adresse" 			value="#adresse#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_codepostal" 		value="#codepostal#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ville" 			value="#ville#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_telephone" 		value="#telephone#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_direction" 		value="#direction#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_EMAIL" 		value="#email#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_PWD" 		value="#password#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_RESTRICTION_IP" 	value="#restrictionIP#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LA LISTE DES POOLS D'UN GESTIONNAIRE (SNCF) --->	<!--- PAS UTILISEE --->
	
	<cffunction name="fournirListePools" access="remote" returntype="Query" hint="FOURNIT LA LISTE DES POOLS D'UN GESTIONNAIRE (SNCF)">
			<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.fournirListeProfiles">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
		<cfreturn p_result>
	</cffunction>

	<!--- FOURNIT LA LISTE DES PROFILS D'UN GESTIONNAIRE (SNCF) --->

	<cffunction name="fournirListeProfiles" access="remote" returntype="Query" hint=" FOURNIT LA LISTE DES PROFILS D'UN GESTIONNAIRE (SNCF)">
		<cfargument name="idracine" 			required="true" type="numeric"/>
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>	
			<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_SNCF.List_profil_gestionnaire_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#idGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine"  		value="#idracine#" 		null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<!--- FOURNIT LA LISTE DES PROFILS GENERIC (SNCF) --->

	<cffunction name="fournirListeProfilesGeneric" access="remote" returntype="Query" hint="FOURNIT LA LISTE DES PROFILS GENERIC (SNCF)">
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>	
			<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_SNCF.List_profil_gest_acine">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#idGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
		<cfreturn p_result>
	</cffunction>

	<!--- ATTIBUT UN PROFIL A UN GESTIONNAIRE (SNCF) --->

	<cffunction name="addProfilInGestionnaire" access="remote" returntype="numeric" hint="ATTIBUT UN PROFIL A UN GESTIONNAIRE (SNCF)">
		<cfargument name="idGestionnaire" 	type="Numeric" required="true">
		<cfargument name="idProfile" 		type="Array"   required="true">
            <cfloop index="i" from="1" to=#ArrayLen(idProfile)#>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_SNCF.add_profil_gestionnaire">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#IdGestionnaire#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#val(idProfile[i].IDPROFIL_EQUIPEMENT)#">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
				</cfstoredproc>
				<cfif p_retour EQ -1>
	            	<cfreturn -1>
	            </cfif>
            </cfloop>
		<cfreturn p_retour>
	</cffunction>

	<!--- SUPPRESSION D'UN LOGIN (SNCF) --->
	
	<cffunction name="eraseProfilInGestionnaire" access="remote" returntype="numeric" hint="SUPPRESSION D'UN LOGIN (SNCF)">
		<cfargument name="IdGestionnaire" 	type="Numeric" required="true">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_SNCF.del_Allprofil_gestionnaire">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdGestionnaire#" 	null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

<!--- ######## PARTIE GESTION DES LOGGINS RAJOUT POUR LA COMMANDE SNCF ######## --->

<!--- ######## PARTIE RAJOUT COMMANDE MOBILE : RENOUVELLEMENT, MODIFICATION, RESILIATION ######## --->

	<!--- FOURNIT LA LISTE DES LIGNES D'UN OPERATUER DONNE --->

	<cffunction name="fournirListeLignesByOperateur" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTE DES LIGNES D'UN OPERATUER DONNE">
		<cfargument name="idRacine" 		required="true" type="Numeric"/>
		<cfargument name="idPool" 			required="true" type="Numeric"/>
		<cfargument name="idOperateur" 		required="true" type="Numeric"/>
		<cfargument name="clef" 			required="true" type="String"/>
		<cfargument name="eligibilite" 		required="true" type="Numeric"/>
		<cfargument name="idrecherche" 		required="true" type="Numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_equipement.getlignesbyoperateur">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine" 		value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" 		value="#idPool#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur" 	value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" 	variable="p_clef" 			value="#clef#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_eligibilite" 	value="#eligibilite#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idrecherche" 	value="#idrecherche#">				
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LA LISTE DES ABONNEMENTS/OPTIONS D'UNE LIGNE  --->

	<cffunction name="fournirLignesProduitAbonnement" access="remote" returntype="Struct" output="false" hint="FOURNIT LA LISTE DES ABONNEMENTS/OPTIONS D'UNE LIGNE">
		<cfargument name="idRacine"  required="true" type="Numeric"/>
		<cfargument name="sousTetes" required="true" type="String"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_flotte.get_lignes_produit_abo_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine" 	value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" 		variable="p_sous_tetes" value="#sousTetes#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"							value="#session.user.globalization#">
				<cfprocresult name="p_retour" 	resultset="1">
				<cfprocresult name="p_retour_2" resultset="2">
			</cfstoredproc>
  			<cfset retour = structNew()>
 			<cfset retour.LIGNESANDOPTIONS = p_retour>
			<cfset retour.NUMBERLIGNES = p_retour_2>
		<cfreturn retour>
	</cffunction>

	<!--- FAIT UNE ACTION DE WORKFLOW POUR CHAQUE ARCTICLE DE LA COMMANDE --->

	<cffunction name="faireActionWorkFlow" access="remote" returntype="numeric" output="false" hint="FAIT UNE ACTION DE WORKFLOW POUR CHAQUE ARCTICLE DE LA COMMANDE" >
		<cfargument name="idAction" 		 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="dateAction" 		 required="true" type="date" 	default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="commentaireAction" required="true" type="string"  default="" displayname="struct commande" 	hint="Initial value for the commandeproperty." />
		<cfargument name="idOperation" 		 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="boolmail" 		 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="logmail" 		 	 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="idsCommande" 		 required="true" type="Array">

			<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
			<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
			<cfset checkOK = 1>
			
			<cfloop index="idx" from="1" to="#ArrayLen(idsCommande)#">
				<cfset idCommande = idsCommande[idx]>
				<cfif checkOK GTE 1>
					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.faireActionDeWorkFlowMobile_v4">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idracine" 		value="#idRacine#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_date_action" 	value="#LSDATEFORMAT(dateAction,'YYYY/MM/DD')#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_commentaire" 	value="#commentaireAction#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idgestionnaire" 	value="#idGestionnaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idoperation" 	value="#idCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idaction" 		value="#idAction#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_bool_mail" 		value="#boolmail#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idcv_log_mail" 	value="#logmail#">
						<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
					</cfstoredproc>
					<cfset checkOK =  p_retour>
				</cfif>
			</cfloop>
		<cfreturn checkOK>
	</cffunction>

	<!--- ENVOI DU MAIL DE MODIFICATION DE COMMANDE --->

	<cffunction name="sendMailModificationsCommande" access="remote" returntype="Numeric" hint="DEMANDE A BIP LA GENERATION DU BDC ET FAIT UNE ACTION DANS LE WORKFLOW">
		<cfargument name="idCommande" 		type="Numeric"  required="true"/>
		<cfargument name="numcommande" 		type="String"  	required="true"/>			
		<cfargument name="dateToday" 		type="Date"  	required="true"/>
		<cfargument name="codelangue" 		type="String"  	required="true"/>
		<cfargument name="libmodification" 	type="String"  	required="true"/>			

			<!--- CRÉATION DE L'ID UNIQUE POUR LE BON DE COMMANDE --->
			<cfset MOD_UUID 		= createUUID()>
			
			<cfset ImportTmp 		= "/container/M16/">
			<cfset currentUserName 	= #Session.user.PRENOM# & ' ' & #Session.user.NOM#>
			<cfset namecreator 		= ''>
			<cfset mailcreator 		= ''>
			<cfset idcreator 		= 0>
			<cfset rsltApi 			= 0>

			<!--- ON RÉCUPÈRE LE MAIL DU CRATEUR DE LA COMMANDE EN ALLANT LE CHERCHER DANS L'HISTORIQUE --->	
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.historiqueetatsoperation">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcommande" value="#idCommande#">	
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_codelangue" value="#codelangue#">
				<cfprocresult name="historiqueactions">
			</cfstoredproc>

			<cfloop query="historiqueactions">
			
				<cfset thisCurrentRow = historiqueactions.currentRow>
				
				<cfset idInvEtat = historiqueactions['IDINV_ETAT'][thisCurrentRow]>
				
				<cfif idInvEtat EQ 3066>
				
					<cfset mailcreator 	= historiqueactions['EMAIL'][thisCurrentRow]>
					<cfset namecreator 	= historiqueactions['NOM'][thisCurrentRow]>
					<cfset idcreator 	= historiqueactions['IDUSER_CREATE'][thisCurrentRow]>
				
				</cfif>
			
			</cfloop>
			
			<cfif idcreator EQ 0>
			
				<cfmail server="mail-cv.consotel.fr" port="25" from="fromM16@saaswedo.com" to="monitoring@saaswedo.com"
								subject="[MODCMD] Pas de destinataire" type="html">
							
						<cfoutput>
							
							Pas de destinataire (créateur de commande inconnu dans l'historique) - commande N° #numcommande#<br/><br/>
							<cfdump var="#CFCATCH.Message#"><br/><br/>
							uuidrapport : #MOD_UUID#<br/><br/>
							idcommande : #idcommande#<br/><br/>
							N° commande : #numcommande#<br/><br/>
							idgestionnaire : #session.user.clientaccessid#<br/><br/>							
							gestionnaire : #session.user.prenom# #session.user.nom#<br/><br/>
							email du gestionnaire : #session.user.email#<br/><br/>
							racine : #session.perimetre.GROUPE#<br/><br/>
							
						</cfoutput>

					</cfmail>
			
			<cfelse>
			
				<cfset properties = createObject("component","fr.consotel.consoview.M01.SendMail").getProperties(session.codeapplication,'NULL')>
			
				<cfset fromMailing = properties['MAIL_EXP_N1']>
			
				<cfset mailing  = createObject("component","fr.consotel.consoview.M01.Mailing")>

				<cfset fileToSend = 'MAIL_' & #numcommande# & '.html'>
				<cfset subjectmail = #libmodification# & #numcommande#>

				<!--- ENREGISTREMENT DU REPERTOIRE DE DESTINATION ET DES INFOS CONCERNANT LE MAIL--->
				<cfset resltRecordMail = mailing.setMailToDatabase(	#session.user.clientaccessid#,
																	#currentUserName#,
																	#MOD_UUID#,
																	"M16",
																	"MOD",
																	#fromMailing#,
																	#mailcreator#,
																	#subjectmail#,
																	'',
																	#fileToSend#,
																	#session.codeapplication#)>

				<cfif resltRecordMail GT 0>
				
					<cfdirectory action="Create" directory="#ImportTmp##MOD_UUID#" type="dir" mode="777"><!--- LE REPERTOIRE EST CREE --->
				
					<cftry>
					
						<!--- BIP REPORT --->
						<cfset parameters["bipReport"] = structNew()>
						<cfset parameters["bipReport"]["xdoAbsolutePath"]	= "/consoview/M16/ArticleSummary/ArticleSummary.xdo">
						<cfset parameters["bipReport"]["xdoTemplateId"]		= 'Summary'>
						<cfset parameters["bipReport"]["outputFormat"] 		= "html">
						<cfset parameters["bipReport"]["localization"]		= codelangue>
						
						<!--- PARAMÈTRES DE GÉNÉRATION FTP DU RAPPORT BIP --->
						<cfset parameters.bipReport.delivery.parameters.ftpUser = "container">
						<cfset parameters.bipReport.delivery.parameters.ftpPassword = "container">
						<cfset parameters.bipReport.delivery.parameters.fileRelativePath = "M16/#MOD_UUID#/#fileToSend#">
						
						<!--- PARAMÈTRES DE FILTRAGE DES DONNÉES DU RAPPORT BIP --->
						<!--- GESTION DE LA NOTIFICATION DE FIN D'EXÉCUTION DU RAPPORT BIP --->
						<cfset parameters.EVENT_TARGET 	= MOD_UUID>
						<cfset parameters.EVENT_TYPE 	= "MOD">
						<cfset parameters.EVENT_HANDLER = "M16">
						
						<!--- PARAMÈTRES POUR LA GÉNÉRATION DU BON DE COMMANDE BIP --->
						<cfset parameters["bipReport"]["reportParameters"]	= structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_RACINE"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_RACINE"]["parameterValues"]=[Session.perimetre.GROUPE]>
						<cfset parameters["bipReport"]["reportParameters"]["P_IDCOMMANDE"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_IDCOMMANDE"]["parameterValues"]=[idCommande]>
						<cfset parameters["bipReport"]["reportParameters"]["P_NUMEROCOMMANDE"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_NUMEROCOMMANDE"]["parameterValues"]=[numcommande]>
						<cfset parameters["bipReport"]["reportParameters"]["P_DATEMODIFICATION"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_DATEMODIFICATION"]["parameterValues"]=[dateToday]>
						<cfset parameters["bipReport"]["reportParameters"]["P_IDGESTIONNAIRECREATE"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_IDGESTIONNAIRECREATE"]["parameterValues"]=[idcreator]>
						<cfset parameters["bipReport"]["reportParameters"]["P_GESTIONNAIRECREATE"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_GESTIONNAIRECREATE"]["parameterValues"]=[namecreator]>
						<cfset parameters["bipReport"]["reportParameters"]["P_IDGESTIONNAIREMODIF"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_IDGESTIONNAIREMODIF"]["parameterValues"]=[Session.user.CLIENTACCESSID]>
						<cfset parameters["bipReport"]["reportParameters"]["P_GESTIONNAIREMODIF"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_GESTIONNAIREMODIF"]["parameterValues"]=[currentUserName]>
												
						<!--- EXÉCUTION DU SERVICE --->
						<cfset api = createObject("component", "fr.consotel.consoview.api.CV")>
						<cfset rsltApi = api.invokeService("IbIs", "scheduleReportCmd", parameters)>
				
					<cfcatch>
						
						<cfmail server="mail-cv.consotel.fr" port="25" from="fromM16@saaswedo.com" to="monitoring@saaswedo.com"
								subject="[MODCMD] Erreur envoi de mail API" type="html">

							<cfoutput>
								
								Impossible d'envoyer un mail de l'API de la commande N° #numcommande#<br/><br/>
								<cfdump var="#CFCATCH.Message#"><br/><br/>
								uuidrapport : #MOD_UUID#<br/><br/>
								idcommande : #idcommande#<br/><br/>
								N° commande : #numcommande#<br/><br/>
								idgestionnaire : #session.user.clientaccessid#<br/><br/>							
								gestionnaire : #session.user.prenom# #session.user.nom#<br/><br/>
								email du gestionnaire : #session.user.email#<br/><br/>
								racine : #session.perimetre.GROUPE#<br/><br/>
								
							</cfoutput>
	
						</cfmail>
						
					</cfcatch>
					</cftry>
				
				<cfelse>
				
					<cfmail server="mail-cv.consotel.fr" port="25" from="fromM16@saaswedo.com" to="monitoring@saaswedo.com"
									subject="[MODCMD] Impossible d'enregistrer les paramètres de mails" type="html">
								
							<cfoutput>
								
								Impossible d'enregistrer les paramètres de mails de la commande N° #numcommande#<br/><br/>
								<cfdump var="#CFCATCH.Message#"><br/><br/>
								uuidrapport : #MOD_UUID#<br/><br/>
								idcommande : #idcommande#<br/><br/>
								N° commande : #numcommande#<br/><br/>
								idgestionnaire : #session.user.clientaccessid#<br/><br/>							
								gestionnaire : #session.user.prenom# #session.user.nom#<br/><br/>
								email du gestionnaire : #session.user.email#<br/><br/>
								racine : #session.perimetre.GROUPE#<br/><br/>
								
							</cfoutput>
	
						</cfmail>
				
				</cfif>

			</cfif>

		<cfreturn rsltApi>
	</cffunction>

	<!--- DEMANDE A BIP LA GENERATION DU BDC ET FAIT UNE ACTION DANS LE WORKFLOW --->
	<!--- -10 => ERREUR DE LOG DONC IMPOSSIBLE D'ENVOYER LE MAIL (IMPOSSIBLE DE RETROUVER LE NUMERO DE COMMANDe AVEC L'API) --->
	<!--- -8  => ERREUR DE WORKFLOW -> LA COMMANDE RESTE EN ETAT 'COMMANDE ENREGISTREE' --->
	<!--- -6  => ERREUR DE BIP -> LE WORKFLOW EST A JOUR MAIS PAS D'ENVOI DE MAIL --->

	<cffunction name="sendMailInfosNewCommande" access="remote" returntype="Numeric" hint="DEMANDE A BIP LA GENERATION DU BDC ET FAIT UNE ACTION DANS LE WORKFLOW">
		<cfargument name="mail" 			type="fr.consotel.consoview.util.Mail" 	required="true"/>
		<cfargument name="idCommande" 		type="Numeric" 							required="true"/>
		<cfargument name="idtypecommande" 	type="Numeric" 							required="true"/>
		<cfargument name="idoperateur" 		type="Numeric"  						required="true"/>
		<cfargument name="boolmail" 		type="Numeric"  						required="true"/>
		<cfargument name="action" 			type="Struct" 							required="true"/>
		<cfargument name="numcommande" 		type="String"  							required="true"/>			
		<cfargument name="uuidReference" 	type="String"  							required="true"/>
		<cfargument name="ismodification" 	type="Numeric"  						required="true"/>
		<cfargument name="myfiles" 			type="Array"  							required="false"/>
			
			
			<cfset workflow = createObject('component',"fr.consotel.consoview.M16.v2.WorkFlowService")>
			<cfset logger   = createObject('component',"fr.consotel.consoview.M16.LogsCommande")>
			<cfset idracine = Session.PERIMETRE.ID_GROUPE>
			
			<cfset rsltwlow = -8>
			<cfset rsltApi  = -6>
			
			
			
			<cfset pieceuuid = getUuidReference(idCommande, numcommande, boolmail)>
			
			<!--- PAS D'ENVOI DE MAIL JUSTE MISE A JOUR DE l'ACTION --->
			<cfif boolmail EQ 0>
					
					<!--- ENVOI DU WORFLOW MISE A JOUR DE LA TABLE DE LOG DES MAILS  --->
					<cfset rsltwlow = workflow.faireActionWorkFlow(	action.IDACTION,
																	action.DATE_HOURS,
																	action.COMMENTAIRE_ACTION,
																	idCommande,
																	boolmail,
																	-1)>
				
				<!--- PAS DE TRAITEMENT DE RESULTAT SI WORKKLOW EN ERREUR -> LA COMMANDE RESTE EN ETAT 'COMMANDE ENREGISTREE' --->
					<cfif rsltwlow LT 1>
						
						<cfmail server="mail-cv.consotel.fr" port="25" from="fromM16@saaswedo.com" to="monitoring@saaswedo.com" 
								subject="Erreur workflow commande" type="html">
											 							
								Impossible de mettre à jour le workflow de la commande N° #idCommande#<br/><br/>
								
								UUIDCommande : #BDC_UUID#<br/><br/>
								Idcommande : #idCommande#<br/><br/>
								IdGestionnaire : #Session.user.CLIENTACCESSID#<br/><br/>							
								Gestionnaire : #Session.user.PRENOM# #Session.user.NOM#<br/><br/>
								Email du Gestionnaire : #Session.user.EMAIL#<br/><br/>
								Racine : #Session.perimetre.GROUPE#<br/><br/>
								
								<br/>
								paramètres du mail :
								
								setEmailDeliveryRequest("#mail.getDestinataire()#","#cc#","#mail.getBcc()#","#mail.getDestinataire()#","#mail.getExpediteur()#",
	                                                     "#message#","#mail.getModule()# : #mail.getSujet()#")

						</cfmail>
					
						<!--- -8 => ERREUR DE WORKFLOW -> LA COMMANDE RESTE EN ETAT 'COMMANDE ENREGISTREE' --->
						<cfreturn -8>
					
					<cfelse>
						
						<cfreturn rsltwlow>
						
					</cfif>
				
			<cfelse>

				<cfset str1 = 'SIZE="10"'>
				<cfset str2 = 'SIZE="2"'>	
					
				<cfset message1 = #Replace(mail.getMessage(),str1,str2,"all")#>
		
				<cfset str1 = "&apos;">
				<cfset str2 = "'">
				
				<cfset message = #Replace(message1,str1,str2,"all")#>
							
				<cfset cc = mail.getCc()>
				
				<cfif mail.getCopiePourExpediteur() eq "YES">
					<cfif len(mail.getCc()) eq 0>
						<cfset cc = mail.getExpediteur()>
					<cfelse>
						<cfset cc = mail.getCc() & "," & mail.getExpediteur()>
						<cfset mail.setCc(cc)>
					</cfif>						
				</cfif>

				<!--- CRÉATION DE L'ID UNIQUE POUR LE BON DE COMMANDE --->
				<cfset BDC_UUID = createUUID()>

				<cftry>		
				
					<!--- ENREGISTREMENT DE LA COMMANDE DANS LA TABLE DES LOGS DES BONS DE COMMANDES --->						
					<cfset rsltlog = logger.insertlogCommande(	SESSION.PERIMETRE.ID_GROUPE,
																SESSION.PERIMETRE.GROUPE,
																SESSION.USER.CLIENTACCESSID,
																SESSION.USER.PRENOM & ' ' & SESSION.USER.NOM,
																SESSION.USER.EMAIL,
																mail,
																idCommande,
																message,
																1,
																'n.c.',
																BDC_UUID,
																SESSION.CODEAPPLICATION)>

					<cfif rsltlog LT 1>
						
						<cfmail server="mail-cv.consotel.fr" port="25" from="fromM16@saaswedo.com" to="monitoring@saaswedo.com"
							subject="Erreur log commande" type="html">
											 							
								Impossible d'inserer le log de la commande N° #idCommande#<br/><br/>
								
								UUIDCommande : #BDC_UUID#<br/><br/>
								Idcommande : #idCommande#<br/><br/>
								IdGestionnaire : #Session.user.CLIENTACCESSID#<br/><br/>							
								Gestionnaire : #Session.user.PRENOM# #Session.user.NOM#<br/><br/>
								Email du Gestionnaire : #Session.user.EMAIL#<br/><br/>
								Racine : #Session.perimetre.GROUPE#<br/><br/>
								
								<br/>
								paramètres du mail :
								
								setEmailDeliveryRequest("#mail.getDestinataire()#","#cc#","#mail.getBcc()#","#mail.getDestinataire()#","#mail.getExpediteur()#",
	                                                     "#message#","#mail.getModule()# : #mail.getSujet()#")

						</cfmail>
					
						<!--- -10 => ERREUR DE LOG DONC IMPOSSIBLE D'ENVOYER LE MAIL (IMPOSSIBLE DE RETROUVER LE NUMERO DE COMMANDe AVEC L'API) --->
						<cfreturn -10>
					
					</cfif>	

					<!--- ENVOI DU WORFLOW MISE A JOUR DE LA TABLE DE LOG DES MAILS  --->
					<cfset rsltwlow = workflow.faireActionWorkFlow(action.IDACTION,
																	action.DATE_HOURS,
																	action.COMMENTAIRE_ACTION,
																	idCommande,
																	boolmail,
																	rsltlog)>
							
					<cfif rsltwlow LT 1>
					
						<cfmail server="mail-cv.consotel.fr" port="25" from="fromM16@saaswedo.com" to="monitoring@saaswedo.com"
								subject="Erreur workflow commande" type="html">
											 							
								Impossible de mettre à jour le workflow de la commande N° #idCommande#<br/><br/>
								
								UUIDCommande : #BDC_UUID#<br/><br/>
								Idcommande : #idCommande#<br/><br/>
								IdGestionnaire : #Session.user.CLIENTACCESSID#<br/><br/>							
								Gestionnaire : #Session.user.PRENOM# #Session.user.NOM#<br/><br/>
								Email du Gestionnaire : #Session.user.EMAIL#<br/><br/>
								Racine : #Session.perimetre.GROUPE#<br/><br/>
								
								<br/>
								paramètres du mail :
								
								setEmailDeliveryRequest("#mail.getDestinataire()#","#cc#","#mail.getBcc()#","#mail.getDestinataire()#","#mail.getExpediteur()#",
	                                                     "#message#","#mail.getModule()# : #mail.getSujet()#")

						</cfmail>
					
						<!--- -8 => ERREUR DE WORKFLOW -> LA COMMANDE RESTE EN ETAT 'COMMANDE ENREGISTREE' --->
						<cfreturn -8>
					
					</cfif>
											
					<cfset rsltApi = -6>

					<!--- RÉCUPÉRATION DU TEMPLATE --->
					<cfset _bdcInfos = new fr.consotel.consoview.M16.v2.util.BDCInfos(idracine,idoperateur,idtypecommande)>
				 
					<cfset _templateName = _bdcInfos.template>
					<cfset _reportAbsolutePath = _bdcInfos.absolutepath>
					
					<!--- BIP REPORT --->
					<cfset parameters["bipReport"] = structNew()>
					<cfset parameters["bipReport"]["xdoAbsolutePath"]	= _reportAbsolutePath>
					<cfset parameters["bipReport"]["xdoTemplateId"]		= _templateName>
					<cfset parameters["bipReport"]["outputFormat"] 		= "pdf">
					<cfset parameters["bipReport"]["localization"]		= session.user.globalization>
					
					<!--- PARAMÈTRES DE GÉNÉRATION FTP DU RAPPORT BIP --->
					<cfset parameters.bipReport.delivery.parameters.ftpUser="container">
					<cfset parameters.bipReport.delivery.parameters.ftpPassword="container">
					<cfset parameters.bipReport.delivery.parameters.fileRelativePath="M16/#pieceuuid#/BDC_#numcommande#.pdf">
					
					<!--- PARAMÈTRES DE FILTRAGE DES DONNÉES DU RAPPORT BIP --->
					<!--- GESTION DE LA NOTIFICATION DE FIN D'EXÉCUTION DU RAPPORT BIP --->
					<cfset parameters.EVENT_TARGET = BDC_UUID>
					<cfset parameters.EVENT_TYPE = "BDC">
					<cfset parameters.EVENT_HANDLER = "M16">
					
					<!--- PARAMÈTRES POUR LA GÉNÉRATION DU BON DE COMMANDE BIP --->
					<cfset parameters["bipReport"]["reportParameters"]	= structNew()>
					<cfset parameters["bipReport"]["reportParameters"]["P_IDCOMMANDE"] = structNew()>
					<cfset parameters["bipReport"]["reportParameters"]["P_IDCOMMANDE"]["parameterValues"]=[idCommande]>
					<cfset parameters["bipReport"]["reportParameters"]["P_IDGESTIONNAIRE"] = structNew()>
					<cfset parameters["bipReport"]["reportParameters"]["P_IDGESTIONNAIRE"]["parameterValues"]=[Session.user.CLIENTACCESSID]>
					<cfset parameters["bipReport"]["reportParameters"]["P_RACINE"] = structNew()>
					<cfset parameters["bipReport"]["reportParameters"]["P_RACINE"]["parameterValues"]=[Session.perimetre.GROUPE]>
					<cfset parameters["bipReport"]["reportParameters"]["P_GESTIONNAIRE"] = structNew()>
					<cfset parameters["bipReport"]["reportParameters"]["P_GESTIONNAIRE"]["parameterValues"]=[Session.user.PRENOM & Session.user.NOM]>
					<cfset parameters["bipReport"]["reportParameters"]["P_MAIL"] = structNew()>
					<cfset parameters["bipReport"]["reportParameters"]["P_MAIL"]["parameterValues"]=[Session.user.EMAIL]>
											
					<!--- EXÉCUTION DU SERVICE --->
					<cfset api = createObject("component", "fr.consotel.consoview.api.CV")>
					<cfset rsltApi = api.invokeService("IbIs", "scheduleReportCmd", parameters)>
			
				<cfcatch>
					
					<!--- EN CAS D'ÉCHEC DE L'OP?RATION ENVOI D'UN MAIL DE NOTIFICATION --->						
					<cflog type="error" text="#CFCATCH.Message#">													
					
					<cfmail server="mail-cv.consotel.fr" port="25" from="fromM16@saaswedo.com" to="monitoring@saaswedo.com"
							subject="[WARN-BDCM]Erreur lors de la création d'un bon de commande" type="html">
										 							
							#CFCATCH.Message#<br/><br/><br/><br/>
							
							UUIDCommande : #BDC_UUID#<br/>
							Idcommande : #idCommande#<br/>
							IdGestionnaire : #Session.user.CLIENTACCESSID#<br/>									
							Gestionnaire : #Session.user.PRENOM# #Session.user.NOM#<br/>
							Email du Gestionnaire : #Session.user.EMAIL#<br/>
							Racine : #Session.perimetre.GROUPE#<br/>
							
							<br/>
							paramètres du mail :
							
							setEmailDeliveryRequest("#mail.getDestinataire()#","#cc#","#mail.getBcc()#","#mail.getDestinataire()#","#mail.getExpediteur()#",
                                                     "#message#","#mail.getModule()# : #mail.getSujet()#")
					</cfmail>
					
				</cfcatch>					
				</cftry>
				
			</cfif>
			
		<cfreturn rsltApi>
	</cffunction>

	<cffunction name="getUuidReference" access="remote" returntype="String" hint="VERIFIER LA PREEXISTENCE DE FICHIER ET DE BDC POUR UNE COMMANDE ET RETOURNE LUUID DE LA COMMANDE">
		<cfargument name="idCommande" 		type="Numeric" 							required="true">
		<cfargument name="numcommande" 		type="String" 							required="true">
		<cfargument name="boolmail" 		type="Numeric"  						required="true"/>
		
			<cfset attach = createObject('component',"fr.consotel.consoview.M16.v2.AttachementService")>

			<cfset pieces 		= attach.fournirAttachements(idCommande)><!--- RECUPERE LES PJ DE LA COMMANDE CONCERNEE --->
			<cfset pieceuuid  	= ''>
			<cfset strg1		= ''>
			<cfset strg2 		= 'BDC_' & #numcommande# & '.pdf'>
			<cfset isBDCExist 	= -1>
			<cfset ImportTmp 	= "/container/M16/">

			<cfloop query="pieces"><!--- RECHERCHE SI LA BDD CONTIENT DEJA LE BDC --->
				
				<cfif isBDCExist LT 0>
				
					<cfset strg1 = pieces.FILE_NAME>
					<cfset idxFind = compareNoCase(strg2, strg1)>
					
					<cfif idxFind EQ 0>
						
						<cfset isBDCExist = 1><!--- LE BON DE COMMANDE EST DEJA DANS LA BDD --->
					
					</cfif>
					
					<cfset pieceuuid = pieces.PATH><!--- RECUPERE L'UUID DE LA COMMANDE --->
					
				</cfif>
				
			</cfloop>

			<cfif pieceuuid EQ ''><!--- SI  pieceuuid = '' => PAS D'UUID DONC CREATION D'UN NVO UUID--->
				
				<cfset pieceuuid = attach.initUUID()>
			
			</cfif>

			<cfif NOT DirectoryExists('#ImportTmp##pieceuuid#')><!--- LE REPERTOIRE NEXISTE PAS --->
				
				<cfdirectory action="Create" directory="#ImportTmp##pieceuuid#" type="dir" mode="777"><!--- LE REPERTOIRE EST CREE --->
				
				<cfif isBDCExist LT 0><!--- PAS DE BDC DONC CREATION D'UN BDC DANS LA BDD --->
						
					<cfif boolmail GT 0>
													
						<cfset attach.addBDC(idCommande, pieceuuid, numcommande)><!--- CREATION D'UN BDC DANS LA BDD --->
					
					</cfif>
					
				</cfif>
			
			<cfelse><!--- LE REPERTOIRE EXISTE --->
			
				<cfif isBDCExist LT 0><!--- PAS DE BDC DONC CREATION D'UN BDC DANS LA BDD --->
					
					<cfif boolmail GT 0>
				
						<cfset attach.addBDC(idCommande, pieceuuid, numcommande)><!--- CREATION D'UN BDC DANS LA BDD SI ENVOI DE MAIL--->
					
					</cfif>
					
				</cfif>
				
			</cfif>
		
		<cfreturn pieceuuid>
	 </cffunction>

	<!--- DEMANDE A BIP LA GENERATION DU BDC ET FAIT UNE ACTION DANS LE WORKFLOW --->
	<!--- -10 => ERREUR DE LOG DONC IMPOSSIBLE D'ENVOYER LE MAIL (IMPOSSIBLE DE RETROUVER LE NUMERO DE COMMANDe AVEC L'API) --->
	<!--- -8  => ERREUR DE WORKFLOW -> LA COMMANDE RESTE EN ETAT 'COMMANDE ENREGISTREE' --->
	<!--- -6  => ERREUR DE BIP -> LE WORKFLOW EST A JOUR MAIS PAS D'ENVOI DE MAIL --->
	
	<cffunction name="envoyer" access="remote" returntype="numeric" hint="DEMANDE A BIP LA GENERATION DU BDC ET FAIT UNE ACTION DANS LE WORKFLOW">
		<cfargument name="mail" 			type="fr.consotel.consoview.util.Mail" 	required="true">
		<cfargument name="idsCommande" 		type="Array" 							required="true">
		<cfargument name="idtypecommande" 	type="Numeric" 							required="true">
		<cfargument name="action" 			type="Struct" 							required="true">
		<cfargument name="boolmail" 		type="Numeric"  						required="true"/>
		<cfargument name="idoperateur" 		type="Numeric"  						required="true"/>
		<cfargument name="uuidReference" 	type="String"  							required="true"/>
		<cfargument name="numcommande" 		type="String"  							required="true"/>			
			
			<cfset idracine = Session.PERIMETRE.ID_GROUPE>
			
			<cfif not isdefined("idoperateur")>
				<cfset _idoperateur = 0>
			<cfelse>
				<cfset _idoperateur = idoperateur>
			</cfif>
			
			
			
			<cfset workflow = createObject('component',"fr.consotel.consoview.M16.v2.WorkFlowService")>
			<cfset logger   = createObject('component',"fr.consotel.consoview.M16.LogsCommande")>
			
			<cfset ImportTmp = "/container/M16/">
			
			<cfset rsltwlow = -8>
			<cfset rsltApi  = -6>

			<cfset var idxCommandes = 0>

			<cfif boolmail EQ 0><!--- PAS D'ENVOI DE MAIL JUSTE MISE A JOUR DE l'ACTION --->

				<cfloop index="idxCommandes" from="1" to="#ArrayLen(idsCommande)#">
					
					<cfset idCommande = idsCommande[idxCommandes]>
					
					<!--- RECUPPERE L'UUID DE LA COMMANDE S'IL EXISTE OU LE CREE S'IL EXISTE PAS --->
					<cfset getUuidReference(idCommande, numcommande, boolmail)>
					
					<!--- ENVOI DU WORFLOW MISE A JOUR DE LA TABLE DE LOG DES MAILS  --->
					<cfset rsltwlow = workflow.faireActionWorkFlow(	action.IDACTION,
																	action.DATE_HOURS,
																	action.COMMENTAIRE_ACTION,
																	idCommande,
																	boolmail,
																	-1)>
				</cfloop>
				
				<cfreturn 1><!--- PAS DE TRAITEMENT DE RESULTAT SI WORKKLOW EN ERREUR -> LA COMMANDE RESTE EN ETAT 'COMMANDE ENREGISTREE' --->
				
			<cfelse>
								
				<cfloop index="idxCommandes" from="1" to="#ArrayLen(idsCommande)#">
								
					<cfset idCommande = idsCommande[idxCommandes]>
					
					<cfset str1 = 'SIZE="10"'>
					<cfset str2 = 'SIZE="2"'>	
						
					<cfset message1 = #Replace(mail.getMessage(),str1,str2,"all")#>
			
					<cfset str1 = "&apos;">
					<cfset str2 = "'">
					
					<cfset message = #Replace(message1,str1,str2,"all")#>
								
					<cfset cc = mail.getCc()>
					
					<cfif mail.getCopiePourExpediteur() eq "YES">
						<cfif len(mail.getCc()) eq 0>
							<cfset cc = mail.getExpediteur()>
						<cfelse>
							<cfset cc = mail.getCc() & "," & mail.getExpediteur()>
							<cfset mail.setCc(cc)>
						</cfif>						
					</cfif>

					<!--- CRÉATION DE L'ID UNIQUE POUR LE BON DE COMMANDE --->
					<cfset BDC_UUID = createUUID()>

					 <cftry>		

						 <!--- RECUPPERE L'UUID DE LA COMMANDE S'IL EXISTE OU LE CREE S'IL EXISTE PAS --->
						<cfset pieceuuid = getUuidReference(idCommande, numcommande, boolmail)>						
												
						<!--- ENREGISTREMENT DE LA COMMANDE DANS LA TABLE DES LOGS DES BONS DE COMMANDES --->						
						<cfset rsltlog = logger.insertlogCommande(	SESSION.PERIMETRE.ID_GROUPE,
																	SESSION.PERIMETRE.GROUPE,
																	SESSION.USER.CLIENTACCESSID,
																	SESSION.USER.PRENOM & ' ' & SESSION.USER.NOM,
																	SESSION.USER.EMAIL,
																	mail,
																	idCommande,
																	message,
																	1,
																	'n.c.',
																	BDC_UUID,
																	SESSION.CODEAPPLICATION)>

						<cfif rsltlog LT 1>
							
							<cfmail server="mail-cv.consotel.fr" port="25" from="fromM16@saaswedo.com" to="monitoring@saaswedo.com"
								subject="Erreur log commande" type="html">
												 							
									Impossible d'inserer le log de la commande N° #idCommande#<br/><br/>
									
									UUIDCommande : #BDC_UUID#<br/><br/>
									Idcommande : #idCommande#<br/><br/>
									IdGestionnaire : #Session.user.CLIENTACCESSID#<br/><br/>							
									Gestionnaire : #Session.user.PRENOM# #Session.user.NOM#<br/><br/>
									Email du Gestionnaire : #Session.user.EMAIL#<br/><br/>
									Racine : #Session.perimetre.GROUPE#<br/><br/>
									
									<br/>
									paramètres du mail :
									
									setEmailDeliveryRequest("#mail.getDestinataire()#","#cc#","#mail.getBcc()#","#mail.getDestinataire()#","#mail.getExpediteur()#",
		                                                     "#message#","#mail.getModule()# : #mail.getSujet()#")
	
							</cfmail>
						
							<cfreturn -10><!--- -10 => ERREUR DE LOG DONC IMPOSSIBLE D'ENVOYER LE MAIL (IMPOSSIBLE DE RETROUVER LE NUMERO DE COMMANDe AVEC L'API) --->
						
						</cfif>	


						<!--- ENVOI DU WORFLOW MISE A JOUR DE LA TABLE DE LOG DES MAILS  --->
						<cfset rsltwlow = workflow.faireActionWorkFlow(action.IDACTION,
																		action.DATE_HOURS,
																		action.COMMENTAIRE_ACTION,
																		idCommande,
																		boolmail,
																		rsltlog)>
								
						<cfif rsltwlow LT 1>
						
							<cfmail server="mail-cv.consotel.fr" port="25" from="fromM16@saaswedo.com" to="monitoring@saaswedo.com"
									subject="Erreur workflow commande" type="html">
												 							
									Impossible de mettre à jour le workflow de la commande N° #idCommande#<br/><br/>
									
									UUIDCommande : #BDC_UUID#<br/><br/>
									Idcommande : #idCommande#<br/><br/>
									IdGestionnaire : #Session.user.CLIENTACCESSID#<br/><br/>							
									Gestionnaire : #Session.user.PRENOM# #Session.user.NOM#<br/><br/>
									Email du Gestionnaire : #Session.user.EMAIL#<br/><br/>
									Racine : #Session.perimetre.GROUPE#<br/><br/>
									
									IDACTION:#action.IDACTION#<br/><br/>
									DATE_HOURS:#action.DATE_HOURS#<br/><br/>
									COMMENTAIRE_ACTION:#action.COMMENTAIRE_ACTION#<br/><br/>
									BOOLMAIL:#boolmail#<br/><br/>
									RSLTLOG:#rsltlog#<br/><br/>
								
									<br/>
									paramètres du mail :
									
									setEmailDeliveryRequest("#mail.getDestinataire()#","#cc#","#mail.getBcc()#","#mail.getDestinataire()#","#mail.getExpediteur()#",
		                                                     "#message#","#mail.getModule()# : #mail.getSujet()#")
	
							</cfmail>
						
							<cfreturn -8><!--- -8 => ERREUR DE WORKFLOW -> LA COMMANDE RESTE EN ETAT 'COMMANDE ENREGISTREE' --->
						
						</cfif>
												
						<!--- VERIFIE L'EXISTENCE DU REPERTOIRE --->

						<cfset rsltApi = -6>

						<!--- RÉCUPÉRATION DU TEMPLATE --->
						<cfset _bdcInfos = new fr.consotel.consoview.M16.v2.util.BDCInfos(idracine,idoperateur,idtypecommande)>				 
						<cfset _templateName = _bdcInfos.template>
						<cfset _reportAbsolutePath = _bdcInfos.absolutepath>
					
						
						<!--- BIP REPORT --->
						<cfset parameters["bipReport"] = structNew()>
						<cfset parameters["bipReport"]["xdoAbsolutePath"]	= _reportAbsolutePath>
						<cfset parameters["bipReport"]["xdoTemplateId"]		= _templateName>
						<cfset parameters["bipReport"]["outputFormat"] 		= "pdf">
						<cfset parameters["bipReport"]["localization"]		= session.user.globalization>
						
						<!--- PARAMÈTRES DE GÉNÉRATION FTP DU RAPPORT BIP --->
						<cfset parameters.bipReport.delivery.parameters.ftpUser="container">
						<cfset parameters.bipReport.delivery.parameters.ftpPassword="container">
						<cfset parameters.bipReport.delivery.parameters.fileRelativePath="M16/#pieceuuid#/BDC_#numcommande#.pdf">
						
						<!--- PARAMÈTRES DE FILTRAGE DES DONNÉES DU RAPPORT BIP --->
						<!--- GESTION DE LA NOTIFICATION DE FIN D'EXÉCUTION DU RAPPORT BIP --->
						<cfset parameters.EVENT_TARGET = BDC_UUID>
						<cfset parameters.EVENT_TYPE = "BDC">
						<cfset parameters.EVENT_HANDLER = "M16">
						
						<!--- PARAMÈTRES POUR LA GÉNÉRATION DU BON DE COMMANDE BIP --->
						<cfset parameters["bipReport"]["reportParameters"]	= structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_IDCOMMANDE"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_IDCOMMANDE"]["parameterValues"]=[idCommande]>
						<cfset parameters["bipReport"]["reportParameters"]["P_IDGESTIONNAIRE"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_IDGESTIONNAIRE"]["parameterValues"]=[Session.user.CLIENTACCESSID]>
						<cfset parameters["bipReport"]["reportParameters"]["P_RACINE"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_RACINE"]["parameterValues"]=[Session.perimetre.GROUPE]>
						<cfset parameters["bipReport"]["reportParameters"]["P_GESTIONNAIRE"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_GESTIONNAIRE"]["parameterValues"]=[Session.user.PRENOM & Session.user.NOM]>
						<cfset parameters["bipReport"]["reportParameters"]["P_MAIL"] = structNew()>
						<cfset parameters["bipReport"]["reportParameters"]["P_MAIL"]["parameterValues"]=[Session.user.EMAIL]>
												
						<!--- EXÉCUTION DU SERVICE --->
						<cfset api = createObject("component", "fr.consotel.consoview.api.CV")>
						<cfset rsltApi = api.invokeService("IbIs", "scheduleReportCmd", parameters)>
				
					<cfcatch>
						<!--- En cas d'échec de l'op?ration envoi d'un mail de notification --->						
						<cflog type="error" text="#CFCATCH.Message#">													
						<cfmail server="mail-cv.consotel.fr" port="25" from="no-reply@saaswedo.com" to="monitoring@saaswedo.com" 
								subject="[WARN-BDC] Erreur lors de la création d'un bon de commande" type="html">
											 							
								#CFCATCH.Message#<br/><br/><br/><br/>
								#CFCATCH.detail#<br/><br/><br/><br/>
								
								UUIDCommande : #BDC_UUID#<br/>
								Idcommande : #idCommande#<br/>
								IdGestionnaire : #Session.user.CLIENTACCESSID#<br/>									
								Gestionnaire : #Session.user.PRENOM# #Session.user.NOM#<br/>
								Email du Gestionnaire : #Session.user.EMAIL#<br/>
								Racine : #Session.perimetre.GROUPE#<br/>
								
								<br/>
								paramètres du mail :
								
								setEmailDeliveryRequest("#mail.getDestinataire()#","#cc#","#mail.getBcc()#","#mail.getDestinataire()#","#mail.getExpediteur()#",
	                                                     "#message#","#mail.getModule()# : #mail.getSujet()#")
														 
								<cfdump var="#cfcatch.tagContext#">

						</cfmail>
					</cfcatch>					
					</cftry>

				
					
				</cfloop>
			</cfif>
			
		<cfreturn rsltApi>
	 </cffunction>

</cfcomponent>
