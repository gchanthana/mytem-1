component displayname="BDCInfos" accessor="true" { 
	
    /**
    * @getter true
    * @setter false	
    **/
	property 
		name="idracine"
		type="numeric";
	
	/**
    * @getter true
    * @setter false
    **/
	property 
		name="idoperateur"
		type="numeric"; 
	
	/**
    * @getter true
    * @setter false	
	* @hint id du Type de commande
    **/
	property 
		name = "idTypeCommande"
		type = "numeric";
		
	/**
    * @getter true
    * @setter false
    **/
	property 
		name = "type"
		type = "string";
	
	/**
    * @getter true
    * @setter false	
    **/
	property 
		name = "absolutepath"
		type = "string";
	
	/**
    * @getter true
    * @setter false	
    **/
	property 
		name = "template"
		type = "string";
	
	/**
    * @getter false
    * @setter false
    **/
	property
		name = "util"
		type = "BDCUtil";

	public BDCInfos function init(required numeric idracine, 
									required numeric idoperateur, 
										required numeric idTypeCommande) {
		
		this.util = new BDCUtil();	
		
		this.idracine = arguments.idracine;
		this.idoperateur = arguments.idoperateur;
		this.idTypeCommande = arguments.idTypeCommande;
		this.type = this.util.getTemplateType(this.idTypeCommande);
		this.absolutepath = this.util.getBDCAbsolutePath(this);
		this.template = this.util.findBDCtemplate(this);
		
		
		return (this);
	}

}