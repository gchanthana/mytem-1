component displayname="BDCUtil" { 
	
	
	
	/**
	* getter true
	* setter false
	* @default 2195
	**/
    property
		name = "IDOPERATEUR_SPIE"
		type = "numeric";
		
	/**
	* getter true
	* setter false
	* @default "BDC"
	**/
	property 
		name = "CODE_BDC_COMMANDE"		
		type = "string";
	
	
	
	/**
	* getter true
	* setter false
	* @default "BDR"
	**/
	property 
		name = "CODE_BDC_RESILIATION"
		type = "string";
	
	
	/**
	* getter true
	* setter false
	* @default "/consoview/gestion/flotte/BDR/BDR.xdo"
	**/
	property
		name = "PATH_BDC_RESILIATION"
		type = "string";
		
	/**
	* getter true
	* setter false
	* @default "/consoview/gestion/flotte/BDC/BDC.xdo"
	**/
	property
		name = "PATH_BDC_COMMANDE"
		type = "string";
	
	/**
	* getter true
	* setter false
	* @default "/consoview/gestion/flotte/BDC_SPIE/BDC_SPIE.xdo"
	**/
	property
		name="PATH_BDC_COMMANDE_SPIE"
		type="string";
		


	public BDCUtil function init() {
			
		this.IDOPERATEUR_SPIE = 2195;
		this.CODE_BDC_COMMANDE = "BDC";
		this.CODE_BDC_RESILIATION = "BDR";
		this.PATH_BDC_COMMANDE = "/consoview/gestion/flotte/BDC/BDC.xdo";
		this.PATH_BDC_COMMANDE_SPIE = "/consoview/gestion/flotte/BDC_SPIE/BDC_SPIE.xdo";
		this.PATH_BDC_RESILIATION = "/consoview/gestion/flotte/BDR/BDR.xdo";
		
		return (this);
	}
	
	public string function getTemplateType(required numeric idtypecommande){
		
		var _type = this.CODE_BDC_COMMANDE;
		var _idtype = arguments.idtypecommande;
		
		switch(_idtype) {
			case "1083":case "1182":case "1282":case "1382":case "1583":case "1584":case "1585":case "1587":
				 _type = this.CODE_BDC_COMMANDE;
				 break;
			case "1283":case "1383":case "1483":case "1586":case "1588":case "1589":
				 _type =this.CODE_BDC_RESILIATION;
				 break;
			default: 
				 _type = this.CODE_BDC_COMMANDE;
		}
		
		return  _type;
	}
	 
		
	public string function getBDCAbsolutePath(required BDCInfos bdcInfos){
		var _commande = arguments["bdcInfos"];
		
		if (_commande.type eq this.CODE_BDC_COMMANDE) {
			
			if (_commande.idoperateur eq this.IDOPERATEUR_SPIE){
				return this.PATH_BDC_COMMANDE_SPIE;
			}else{
				return this.PATH_BDC_COMMANDE;
			}
		}else{
			return this.PATH_BDC_RESILIATION;
		}
	
	}
	
	public string function findBDCtemplate(required BDCInfos bdcInfos){
		var _bdcInfo = arguments.bdcInfos;
		var _templateArray = getRemoteTemplateList(_bdcInfo.absolutepath);
		var _targetRule1 = _bdcInfo.type & '-' &  _bdcInfo.idoperateur & '-' &  _bdcInfo.idracine;
		var _targetRule2 = _bdcInfo.type & '-0-' &  _bdcInfo.idracine;
		var _targetRule3 = _bdcInfo.type & '-' &  _bdcInfo.idoperateur;
		var _targetRule4 = _bdcInfo.type;		
		
		var _arrRules = [_targetRule1,_targetRule2,_targetRule3,_targetRule4];
		
		var _template = _arrRules[4];
		
		
		for (var _index=1;  _index LTE arraylen(_arrRules);  _index = _index + 1) {
		
			if(ArrayContains(_templateArray,_arrRules[_index]))
			{
				_template = _arrRules[_index];
				break;
			}
		}
		
		return _template;
		
	}
	
	private array function getRemoteTemplateList(required string path ){
		var BipCmdWebServiceFactory =  new fr.consotel.api.ibis.publisher.BipCmdWebServiceFactory();
		var webservice = BipCmdWebServiceFactory.createWebService();
		var rapportdefinition = webservice.getReportDefinition(arguments.path ,'consoview','public');
		var templateArraylist = rapportdefinition.getTemplateIds();
		
		return templateArraylist;
	}

}