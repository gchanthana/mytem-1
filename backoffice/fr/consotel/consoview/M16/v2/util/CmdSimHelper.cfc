component displayname="CmdSimHelper" {
	
		/**
		*	IN
		*	Infos.idRevendeur
		*	Infos.idOperateur
		*	Infos.idTypeOpe
		*	OUT			
		*	SIM.LIBELLE_EQ					:CARTE SIM
		*	SIM.IDEQUIPEMENT_FOURNISSEUR	:1011293
		*	SIM.TYPE_EQUIPEMENT				:Carte Sim
		*	SIM.IDTYPE_EQUIPEMENT			:71
		**/
		public array function buildPacket(required array articles,required struct context){
			var tabXML = arguments.articles;
 			var simInfos = getSim(context);
			var result = [];
		
			for(i=1; i lte arraylen(tabXML); i++){
				articleXML = xmlParse(tabXML[i]);
				writelog(toString(articleXML));
				writelog("-----------CmdSimHelper----------------");
				writelog(toString(articleXML.articles));
				writelog("------------CmdSimHelper---------------");
			
				
				builSingleArticle(articleXML.articles.xmlChildren,simInfos);
				
				writelog("-----------CmdSimHelper----------------");
				writelog(toString(articleXML.articles));
				writelog("------------CmdSimHelper---------------");

				ArrayAppend(result, articleXML);
			}
			return result;
		}

		private any function getSimInfo(required struct context){
			var ctx = arguments.context;
			var qSim = createObject("fr.consotel.consoview.M16.v2.util.Proc").getSimInfo(ctx);
			return qSim;
		}

		private struct function getSim(required struct context){
			var qsim = getSimInfo(arguments.context);
			var sim ={};
			if(qsim.recordcount gt 0)
			{
				sim["idequipementfournis"] = qsim.IDEQUIPEMENT_FOURNISSEUR;
				sim["libelle"]= qsim.LIBELLE_EQ;
				sim["type_equipement"] = qsim.TYPE_EQUIPEMENT;
				sim["idtype_equipement"] = qsim.IDTYPE_EQUIPEMENT;
				sim["prix"] = 0;
				sim["prixAffiche"] = 0;
				sim["bonus"] = 0;

			}
			return sim;

		}

		private struct function getMockSim(required struct econtext){
				var sim ={};
				sim["idequipementfournis"] = 19993;
				sim["libelle"]= "CARTE SIM";
				sim["type_equipement"] = "carte SIM";
				sim["idtype_equipement"] = 71;
				sim["prix"] = 0;
				sim["prixAffiche"] = 0;
				sim["bonus"] = 0;
				return sim;
		}

		

		private any function builSingleArticle(required array article,required struct siminfos){
			var _article = arguments.article;
			var _sim = arguments.siminfos;
			var __NEW_SIM__	= 1883;
			
			for(i=1; i lte arrayLen(_article); i++){
				var tmp = "";
				var tmplen = 1;
				var c = 1;

				if(!StructKeyExists(_article[i], "equipements")){					
					article[i].xmlChildren[1] = XmlElemNew(articleXML,"equipements");				
				}
				
				_article[i].idSegment.xmlText = 1;
				_article[i].idtypecommande.xmlText = __NEW_SIM__;
				writelog("toString(_article[#i#])");
				writelog(toString(_article[i]));

				tmp = article[i].equipements.xmlChildren;
				tmplen = arrayLen(tmp);
				article[i].equipements.xmlChildren[tmplen+1] = XmlElemNew(articleXML,"equipement");
				tmp = article[i].equipements.xmlChildren[tmplen+1];
				
				
				for(key in _sim){
					tmp.xmlChildren[c] = XmlElemNew(articleXML,key);
					tmp.xmlChildren[c].xmlText = _sim[key];
					c++;
				}
			}
			return _article;	
		}
		
}

