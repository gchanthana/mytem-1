<cfcomponent displayname="fr.consotel.consoview.M16.v2.ListeCommandesService" hint="Gestion de la liste des commandes et des filtres" output="false">

	<!--- RÉCUPÈRE LES FILTRES POUR LA PAGINATION DE LA COMMANDE --->

	<cffunction name="getFiltres" access="remote" returntype="Struct" output="false" hint="RÉCUPÈRE LES FILTRES POUR LA PAGINATION DE LA COMMANDE">
		<cfargument name="idpool" 	required="true"  type="numeric" default="" displayname="numeric pool"/>
		<cfargument name="idprofil" required="true"  type="numeric" default="" displayname="numeric pool"/>
		<cfargument name="segment" 	required="true"  type="numeric" default="" displayname="numeric segment"/>
		<cfargument name="clef" 	required="true"  type="string"  default="" displayname="string clef" />
		<cfargument name="all" 		required="false" type="numeric" default="1" displayname="numeric segment"/>
		
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset code_langue 		= session.user.globalization>
				
			<cfif isDefined("all")>
				<cfset _all = all>	
			<cfelse>
				<cfset _all = 1>	
			</cfif>

			<cfthread action="run" name="inventaireOperations" mypool="#idpool#" mysegment="#segment#" myclef="#clef#" myAll="#_all#">
				
				<cfset procdedure = "">
				
				<cfif _all EQ 1>
				
					<cfset procdedure = "pkg_m16.fournirinventaireoperation_v3">
				
				<cfelse>
				
					<cfset procdedure = "pkg_m16.fournirinventaireoperation_v2">
				
				</cfif>
				
				<cfstoredproc datasource="#session.offredsn#" procedure="#procdedure#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 		value="#session.perimetre.id_groupe#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#session.user.clientaccessid#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 			value="#mysegment#">			
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 			value="#myclef#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 		value="#session.user.globalization#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 			value="#mypool#">
					<cfprocresult name="p_retour">			
				</cfstoredproc>
				
				<cfquery name="getInventaireCommandes" dbtype="query">
					select *
					FROM p_retour
					ORDER BY DATE_CREATE DESC
				</cfquery>
				
				<cfset inventaireOperations.RESULT = getInventaireCommandes>
				
			</cfthread>
			
			<cfthread action="run" name="actions" mypool="#idpool#">
			
				<cfset object = createObject("component","fr.consotel.consoview.M16.v2.WorkFlowService")>
				
				<cfset p_retour = object.fournirToutesLesActionsPossible(mypool)>
			
				<cfset actions.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="actionsProfil" myprofil="#idprofil#">
			
				<cfset object = createObject("component","fr.consotel.consoview.M16.v2.PoolService")>
				
				<cfset p_retour = object.fournirListeActionProfilCommande(myprofil)>
			
				<cfset actionsProfil.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="join" name="inventaireOperations,actions,actionsProfil" timeout="40000"/> 
				
			<cfset query_retour = cfthread.inventaireOperations.RESULT>

			<cfthread action="run" name="etats" myquery="#query_retour#">
			
				<cfquery name="getListeETAT" dbtype="query">
					select DISTINCT IDINV_ETAT AS IDETAT,LIBELLE_EAT AS LIBELLE
					FROM myquery
					ORDER BY LIBELLE
				</cfquery>	
			
				<cfset etats.RESULT = getListeETAT>
			
			</cfthread>
			
			<cfthread action="run" name="gestionnaires" myquery="#query_retour#">
			
				<cfquery name="getListeGEST" dbtype="query">
					select DISTINCT USERID ,USERCREATE AS LIBELLE
					FROM myquery
					ORDER BY LIBELLE
				</cfquery>
			
				<cfset gestionnaires.RESULT = getListeGEST>
			
			</cfthread>
			
			<cfthread action="run" name="operateurs" myquery="#query_retour#">
			
				<cfquery name="getListeOPER" dbtype="query">
					select DISTINCT IDOPERATEUR,LIBELLE_OPERATEUR AS LIBELLE
					FROM myquery
					ORDER BY LIBELLE
				</cfquery>
				
				<cfset operateurs.RESULT = getListeOPER>
			
			</cfthread>
			
			<cfthread action="run" name="comptes" myquery="#query_retour#">
			
				<cfquery name="getListeCOMPT" dbtype="query">
					select DISTINCT IDCOMPTE AS IDCOMPTE_FACTURATION,LIBELLE_COMPTE AS LIBELLE
					FROM myquery
					ORDER BY IDCOMPTE
				</cfquery>
				
				<cfset comptes.RESULT = getListeCOMPT>
			
			</cfthread>
			
			<cfthread action="run" name="souscomptes" myquery="#query_retour#">
				
				<cfquery name="getListeSSCPT" dbtype="query">
					select DISTINCT IDSSCOMPTE AS IDSOUS_COMPTE,LIBELLE_SSCOMPTE AS LIBELLE
					FROM myquery
					ORDER BY IDSSCOMPTE
				</cfquery>
				
				<cfset souscomptes.RESULT = getListeSSCPT>
				
			</cfthread>
			
			<cfthread action="join" name="etats,gestionnaires,operateurs,comptes,souscomptes" timeout="40000"/>
			
			<cfset infosCommandes 	 				 = structNew()>
			<cfset infosCommandes.LISTECOMMANDES 	 = cfthread.inventaireOperations.RESULT>
			<cfset infosCommandes.LISTEACTIONS 		 = cfthread.actions.RESULT>
			<cfset infosCommandes.LISTEETATS 		 = cfthread.etats.RESULT>
			<cfset infosCommandes.LISTEGESTIONNAIRES = cfthread.gestionnaires.RESULT>
			<cfset infosCommandes.LISTEOPERATEURS 	 = cfthread.operateurs.RESULT>
			<cfset infosCommandes.LISTECOMPTES 		 = cfthread.comptes.RESULT>
			<cfset infosCommandes.LISTESOUSCOMPTE 	 = cfthread.souscomptes.RESULT>
			<cfset infosCommandes.LISTEACTIONSPROFIL = cfthread.actionsProfil.RESULT>

			<cfset session.LISTEACTIONS = cfthread.actions.RESULT>

		<cfreturn infosCommandes>
	</cffunction>

	<cffunction name="getCommandes" access="remote" returntype="Query" output="false" hint="LISTE DES COMMANDE DE TOUS LES POOLS DE GESTION">
		<cfargument name="idpool" 	required="true"  type="numeric" default="" displayname="numeric pool"/>
		<cfargument name="idprofil" required="true"  type="numeric" default="" displayname="numeric pool"/>
		<cfargument name="segment" 	required="true"  type="numeric" default="" displayname="numeric segment"/>
		<cfargument name="clef" 	required="true"  type="string"  default="" displayname="string clef" />
		<cfargument name="all" 		required="false" type="numeric" default="1" displayname="numeric segment"/>
		
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset codelangue 		= session.user.globalization>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.fournirinventaireoperation_v3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 		value="#idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#idgestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 			value="#segment#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 			value="#clef#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 		value="#codelangue#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 			value="#idpool#">
				<cfprocresult name="p_retour">			
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<cffunction name="getFiltersCommandeAndActions" access="remote" returntype="Struct" output="false" hint="FOURNIT LA LISTE DES ABONNEMENTS/OPTIONS D'UNE LIGNE">
		<cfargument name="idpool"  	required="true" type="Numeric"/>
		<cfargument name="idprofil" required="true" type="Numeric"/>
		
			<cfset idracine = session.perimetre.id_groupe>
			<cfset idlang 	= session.user.idglobalization>
			<cfset idlogin = session.user.clientaccessid>


			<cfset infosCommandes = structNew()>

			<cfthread action="run" name="filterEtats" myidlang="#idlang#" mypool="#idpool#" myracine="#idracine#">
			
				<!--- getListeEtat(p_langid IN integer,p_retour OUT SYS_REFCURSOR); --->
			
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getlisteetat_v2">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_langid" 	value="#myidlang#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
				<cfset filterEtats.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterGestionnaires" mypool="#idpool#" myracine="#idracine#" myLogin="#idlogin#">
			
				<!--- getGestOfPool(p_idpool IN INTEGER,p_idracine IN INTEGER,p_retour OUT INTEGER); --->
			
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getGestOfPool_v2">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocresult name="p_retour">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idlogin" value="#myLogin#">
				</cfstoredproc>
			
				<cfset filterGestionnaires.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterOperateurs" myracine="#idracine#">
			
				<!--- getOperateurOfRacine(p_idracine IN INTEGER,p_retour OUT INTEGER); --->
			
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getOperateurOfRacine">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
				<cfset filterOperateurs.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterComptes" mypool="#idpool#" myracine="#idracine#" myLangid="#idLang#">
			
				<!--- getCompteOfPool(p_idpool IN integer,p_idracine IN INTEGER,p_retour OUT INTEGER); --->
				
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getCompteOfPool_v2">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_langid" value="#myLangid#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
				
				<cfset filterComptes.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterSousComptes" mypool="#idpool#" myracine="#idracine#" myLangid="#idLang#">
			
				<!--- getSsCompteOfPool(p_idpool IN integer,p_idracine IN INTEGER,p_retour OUT INTEGER); --->
			
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getSsCompteOfPool_v2">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_langid" value="#myLangid#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
				<cfset filterSousComptes.RESULT = p_retour>
			
			</cfthread>
						
			<cfthread action="run" name="actionsProfil" myprofil="#idprofil#">
			
				<cfset object = createObject("component","fr.consotel.consoview.M16.v2.PoolService")>
				
				<cfset p_retour = object.fournirListeActionProfilCommande(myprofil)>
			
				<cfset actionsProfil.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="actions" mypool="#idpool#">
			
				<cfset object = createObject("component","fr.consotel.consoview.M16.v2.WorkFlowService")>
				
				<cfset p_retour = object.fournirToutesLesActionsPossible(mypool)>
				
				<cfset myActions = arraynew(1)>
			
				<cfloop query="p_retour">
					
					<cfif p_retour.SEGMENT EQ 2>
					
						<!--- RECUPERATION SEGEMENT = 2 --->
						<cfset actionsWorkflow = structNew()>
						
						<cfset actionsWorkflow.IDINV_ETAT			= p_retour.IDINV_ETAT>
						<cfset actionsWorkflow.MESSAGE 		 		= p_retour.MESSAGE>
						<cfset actionsWorkflow.CODE_ACTION 	 		= p_retour.CODE_ACTION>
						<cfset actionsWorkflow.DEST  				= p_retour.DEST>
						<cfset actionsWorkflow.IDETAT_ENGENDRE 		= p_retour.IDETAT_ENGENDRE>
						<cfset actionsWorkflow.EXP 					= p_retour.EXP>
						<cfset actionsWorkflow.COMMENTAIRE_ACTION 	= p_retour.COMMENTAIRE_ACTION>
						<cfset actionsWorkflow.SEGMENT 				= p_retour.SEGMENT>
						<cfset actionsWorkflow.LIBELLE_ACTION 		= p_retour.LIBELLE_ACTION>
						<cfset actionsWorkflow.IDINV_ACTIONS 		= p_retour.IDINV_ACTIONS>
						<cfif mypool LTE 0>
							<cfset actionsWorkflow.IDPROFIL_COMMANDE 	= p_retour.IDPROFIL_COMMANDE>
						</cfif>
						<cfset arrayappend(myActions, actionsWorkflow)>
						
						<!--- RECUPERATION SEGEMENT = 3 --->
						<cfset actionsWorkflow = structNew()>
						
						<cfset actionsWorkflow.IDINV_ETAT			= p_retour.IDINV_ETAT>
						<cfset actionsWorkflow.MESSAGE 		 		= p_retour.MESSAGE>
						<cfset actionsWorkflow.CODE_ACTION 	 		= p_retour.CODE_ACTION>
						<cfset actionsWorkflow.DEST  				= p_retour.DEST>
						<cfset actionsWorkflow.IDETAT_ENGENDRE 		= p_retour.IDETAT_ENGENDRE>
						<cfset actionsWorkflow.EXP 					= p_retour.EXP>
						<cfset actionsWorkflow.COMMENTAIRE_ACTION 	= p_retour.COMMENTAIRE_ACTION>
						<cfset actionsWorkflow.SEGMENT 				= 3>
						<cfset actionsWorkflow.LIBELLE_ACTION 		= p_retour.LIBELLE_ACTION>
						<cfset actionsWorkflow.IDINV_ACTIONS 		= p_retour.IDINV_ACTIONS>
						<cfif mypool LTE 0>
							<cfset actionsWorkflow.IDPROFIL_COMMANDE 	= p_retour.IDPROFIL_COMMANDE>
						</cfif>
						<cfset arrayappend(myActions, actionsWorkflow)>

					<cfelse>

						<cfset actionsWorkflow = structNew()>
						
						<cfset actionsWorkflow.IDINV_ETAT			= p_retour.IDINV_ETAT>
						<cfset actionsWorkflow.MESSAGE 		 		= p_retour.MESSAGE>
						<cfset actionsWorkflow.CODE_ACTION 	 		= p_retour.CODE_ACTION>
						<cfset actionsWorkflow.DEST  				= p_retour.DEST>
						<cfset actionsWorkflow.IDETAT_ENGENDRE 		= p_retour.IDETAT_ENGENDRE>
						<cfset actionsWorkflow.EXP 					= p_retour.EXP>
						<cfset actionsWorkflow.COMMENTAIRE_ACTION 	= p_retour.COMMENTAIRE_ACTION>
						<cfset actionsWorkflow.SEGMENT 				= p_retour.SEGMENT>
						<cfset actionsWorkflow.LIBELLE_ACTION 		= p_retour.LIBELLE_ACTION>
						<cfset actionsWorkflow.IDINV_ACTIONS 		= p_retour.IDINV_ACTIONS>
						<cfif mypool LTE 0>
							<cfset actionsWorkflow.IDPROFIL_COMMANDE 	= p_retour.IDPROFIL_COMMANDE>
						</cfif>
						<cfset arrayappend(myActions, actionsWorkflow)>

					</cfif>
					
				</cfloop>
			
				 <!--- Set up the query for testing. --->
				 <cfif mypool LTE 0>
					<cfset qRetour = QueryNew( "IDINV_ETAT, MESSAGE, CODE_ACTION, DEST, IDETAT_ENGENDRE, EXP, COMMENTAIRE_ACTION, SEGMENT, LIBELLE_ACTION, IDINV_ACTIONS, IDPROFIL_COMMANDE")>
				<cfelse>
				<cfset qRetour = QueryNew( "IDINV_ETAT, MESSAGE, CODE_ACTION, DEST, IDETAT_ENGENDRE, EXP, COMMENTAIRE_ACTION, SEGMENT, LIBELLE_ACTION, IDINV_ACTIONS")>
				</cfif>
				
				<cfloop index="idx" from="1" to="#ArrayLen(myActions)#">
				
					<cfset QueryAddRow(qRetour)>
				    <cfset qRetour["IDINV_ETAT"][ qRetour.RecordCount ] 		= #myActions[idx].IDINV_ETAT#/>
				    <cfset qRetour["MESSAGE"][ qRetour.RecordCount ] 			= #myActions[idx].MESSAGE#/>
				    <cfset qRetour["CODE_ACTION"][ qRetour.RecordCount ] 		= #myActions[idx].CODE_ACTION#/>
				    <cfset qRetour["DEST"][ qRetour.RecordCount ] 				= #myActions[idx].DEST#/>
				    <cfset qRetour["IDETAT_ENGENDRE"][ qRetour.RecordCount ] 	= #myActions[idx].IDETAT_ENGENDRE#/>
				    <cfset qRetour["EXP"][ qRetour.RecordCount ] 				= #myActions[idx].EXP#/>
				    <cfset qRetour["COMMENTAIRE_ACTION"][ qRetour.RecordCount ] = #myActions[idx].COMMENTAIRE_ACTION#/>
				    <cfset qRetour["SEGMENT"][ qRetour.RecordCount ] 			= #myActions[idx].SEGMENT#/>
				    <cfset qRetour["LIBELLE_ACTION"][ qRetour.RecordCount ] 	= #myActions[idx].LIBELLE_ACTION#/>
				    <cfset qRetour["IDINV_ACTIONS"][ qRetour.RecordCount ] 		= #myActions[idx].IDINV_ACTIONS#/>
					<cfif mypool LTE 0>
						<cfset qRetour["IDPROFIL_COMMANDE"][ qRetour.RecordCount ] 		= #myActions[idx].IDPROFIL_COMMANDE#/>
					</cfif>
				</cfloop>
			
				<cfset actions.RESULT = qRetour>
				
			</cfthread>
			
			<!--- <cfthread action="join" name="filterscommande,actions,actionsProfil" timeout="40000"/> --->
			
			<cfthread action="join" name="filterEtats,filterGestionnaires,filterOperateurs,filterComptes,filterSousComptes,actions,actionsProfil"/>

			<cfset infosCommandes 	 				 = structNew()>
			<cfset infosCommandes.LISTEETATS 		 = cfthread.filterEtats.RESULT>
			<cfset infosCommandes.LISTEGESTIONNAIRES = cfthread.filterGestionnaires.RESULT>
			<cfset infosCommandes.LISTEOPERATEURS 	 = cfthread.filterOperateurs.RESULT>
			<cfset infosCommandes.LISTECOMPTES 		 = cfthread.filterComptes.RESULT>
			<cfset infosCommandes.LISTESOUSCOMPTE 	 = cfthread.filterSousComptes.RESULT>
 			<cfset infosCommandes.LISTEACTIONS 		 = cfthread.actions.RESULT>
			<cfset infosCommandes.LISTEACTIONSPROFIL = cfthread.actionsProfil.RESULT>

		<cfreturn infosCommandes>
	</cffunction>

	<cffunction name="getFiltersCommandeAndActionsV2" access="remote" returntype="Struct" output="false" hint="FOURNIT LA LISTE DES ABONNEMENTS/OPTIONS D'UNE LIGNE">
		<cfargument name="idpool"  	required="true" type="Numeric"/>
		<cfargument name="idprofil" required="true" type="Numeric"/>
		
			<cfset idracine = session.perimetre.id_groupe>
			<cfset idlang 	= session.user.idglobalization>

			<cfset infosCommandes = structNew()>
			
			
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getCompteOfPool_v2">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#idpool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_langid" value="#idLang#">	
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
<!--- 			<cfthread action="run" name="filterEtats" myidlang="#idlang#">
			
				<!--- getListeEtat(p_langid IN integer,p_retour OUT SYS_REFCURSOR); --->
			
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getListeEtat">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_langid" value="#myidlang#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
				<cfset filterEtats.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterGestionnaires" mypool="#idpool#" myracine="#idracine#">
			
				<!--- getGestOfPool(p_idpool IN INTEGER,p_idracine IN INTEGER,p_retour OUT INTEGER); --->
			
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getGestOfPool">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
				<cfset filterGestionnaires.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterOperateurs" myracine="#idracine#">
			
				<!--- getOperateurOfRacine(p_idracine IN INTEGER,p_retour OUT INTEGER); --->
			
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getOperateurOfRacine">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
				<cfset filterOperateurs.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterComptes" mypool="#idpool#" myracine="#idracine#">
			
				<!--- getCompteOfPool(p_idpool IN integer,p_idracine IN INTEGER,p_retour OUT INTEGER); --->
				
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getCompteOfPool">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocresult name="p_retour">
				</cfstoredproc> 
				
				<cfset filterComptes.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterSousComptes" mypool="#idpool#" myracine="#idracine#">
			
				<!--- getSsCompteOfPool(p_idpool IN integer,p_idracine IN INTEGER,p_retour OUT INTEGER); --->
			
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getSsCompteOfPool">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
				<cfset filterSousComptes.RESULT = p_retour>
			
			</cfthread> --->


			
			<cfset infosCommandes.LISTEETATS 		 = arraynew(1)>
			<cfset infosCommandes.LISTEGESTIONNAIRES = arraynew(1)>
			<cfset infosCommandes.LISTEOPERATEURS 	 = arraynew(1)>
			<cfset infosCommandes.LISTECOMPTES 		 = arraynew(1)>
			<cfset infosCommandes.LISTESOUSCOMPTE 	 = arraynew(1)>
 			<cfset infosCommandes.LISTEACTIONS 		 = arraynew(1)>
			<cfset infosCommandes.LISTEACTIONSPROFIL = arraynew(1)>

		<cfreturn infosCommandes>
	</cffunction>

	<cffunction name="fournirInventaireCommandePaginate" access="remote" returntype="any" output="false" hint="LISTE DES COMMANDE DE TOUS LES POOLS DE GESTION">
		<cfargument name="segment" 		required="true"  type="Numeric" default="-1"/>
		<cfargument name="clef" 		required="true"  type="String"  default=""/>
		<cfargument name="idpool" 		required="true"  type="Numeric" default="-1"/>
		<cfargument name="idstatut" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="idetat" 		required="true"  type="Numeric" default="-1"/>
		<cfargument name="idoperateur" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="idcompte" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="idsouscompte" required="true"  type="Numeric" default="-1"/>
		<cfargument name="idusercreate" required="true"  type="Numeric" default="-1"/>
		<cfargument name="startindex" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="numberrecord" required="true"  type="Numeric" default="-1"/>
		<cfargument name="idcritere" 	required="true"  type="Numeric" default="0"/>
		<cfargument name="txtFilter" 	required="true"  type="String"  default=""/>
		<cfargument name="nb_mois_histo" 	required="true"  type="Numeric"  default="0"/>

			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset codelangue 		= session.user.globalization>
					
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.fournirinventaireoperation_v10" blockfactor="20">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#idgestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 				value="#segment#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 				value="#clef#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 			value="#codelangue#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 				value="#idpool#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idstatut" 			value="#idstatut#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idetat" 				value="#idetat#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_operateur" 			value="#idoperateur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" 			value="#idcompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte" 		value="#idsouscompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_user_create" 			value="#idusercreate#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_index_debut" 			value="#startindex#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_number_of_records" 	value="#numberrecord#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcritere" 			value="#idcritere#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_critere" 				value="#txtFilter#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_nbmois_histo" 			value="#nb_mois_histo#">
				<cfprocresult name="p_retour">			
			</cfstoredproc>
			<!---cfmail to="soufiane.ghenimi@saaswedo.com" from="soufiane.ghenimi@saaswedo.com" subject="listt Commande" type="html">
				<cfdump var ="p_idracine  ---------- : #idracine#"><br>
				<cfdump var ="p_idgestionnaire ------: #idgestionnaire#"><br>
				<cfdump var ="p_segment -------------: #segment#"><br>			
				<cfdump var ="p_clef ----------------: #clef#"><br>
				<cfdump var ="p_code_langue  ------- : #codelangue#"><br>
				<cfdump var ="p_idpool --------------: #idpool#"><br>
				<cfdump var ="p_idstatut  ---------- : #idstatut#"><br>
				<cfdump var ="p_idetat --------------: #idetat#"><br>
				<cfdump var ="p_operateur  --------- : #idoperateur#"><br>
				<cfdump var ="p_idcompte  ---------- : #idcompte#"><br>
				<cfdump var ="p_idsous_compte -------: #idsouscompte#"><br>
				<cfdump var ="p_user_create  ------- : #idusercreate#"><br>
				<cfdump var ="p_index_debut  ------- : #startindex#"><br>
				<cfdump var ="p_number_of_records -- : #numberrecord#"><br>
				<cfdump var ="p_idcritere  --------- : #idcritere#"><br>
				<cfdump var ="p_critere ------------ : #txtFilter#"><br>
				<cfdump var ="p_nbmois_histo  ------ : #nb_mois_histo#"><br>
				
				RETOUR  : <cfdump var = "#p_retour#"><br>
			</cfmail --->

		<cfreturn p_retour>
	</cffunction>

	<!--- 
			-> RECUPERE TOUTES LES INFOS NECESSAIRES LORS DU CARGEMENT DU MODULE M16
				- POOLS
				- NOMBRE REVENDEURS AUTORISES
				- CHAMPS REFERENCES CLIENTS
				- PROFIL UTILISATEUR
				- FILTRES POUR LA LISTE DES COMMANDES
	--->


	<cffunction name="getLoadInfosThread" access="remote" returntype="Struct" output="false" hint="FOURNI TOUTES LES INFOS LORS DU CHARGEMENT DU MODULE">
		<cfargument name="idpooldefault"  	required="true" type="Numeric"/>
		<cfargument name="idprofildefault"  required="true" type="Numeric"/>
		
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset idlang 			= session.user.idglobalization>
			<cfset codelangue 		= session.user.globalization>

			<cfset remoteObject = createObject('component',"fr.consotel.consoview.M16.v2.PoolService")>
			
			<cfset getpools = remoteObject.fournirListePoolsDuGestionnaire(idgestionnaire, idracine)>
		
			<cfset remoteObject = createObject('component',"fr.consotel.consoview.M16.v2.DistributeurService")>
				
			<cfset getrevendeursautorises = remoteObject.fournirNumberRevendeursAutorises(idracine, idgestionnaire)>
			
			<cfset remoteObject = createObject('component',"fr.consotel.consoview.M16.v2.CommandeService")>
				
			<cfset getchamps = remoteObject.getChamps()>
		
			<cfset remoteObject = createObject('component',"fr.consotel.consoview.M16.v2.DistributeurService")>
				
			<cfset getuserprofil = remoteObject.getUserProfil()>

			<cfset idprofiluser = getuserprofil>
			<cfset idprofil = -1>
			<cfset idpool 	= -1>

			<cfif getpools.RecordCount GT 0>
			
				<cfif getpools.RecordCount EQ 1>
					
					<cfset idpool 	= getpools["IDPOOL"][getpools.RecordCount]>
					<cfset idprofil = getpools["IDPROFIL"][getpools.RecordCount]>	
				
				<cfelse>
				
					<cfif idpooldefault GT 0>
						
						<cfset idpool 	= idpooldefault>
						<cfset idprofil = idprofildefault>
						
					<cfelse>
						
						<cfset myPoolsInfos = getMyProfilPool(getpools, idprofiluser)>
						
						<cfset idpool 	= myPoolsInfos.IDPOOL>
						<cfset idprofil = myPoolsInfos.IDPROFIL>	
						
					</cfif>
				
				</cfif>
				
			</cfif>

			<cfset filterCommande = getFiltersCommandeAndActions(idpool, idprofil)>

			<cfset infosLoaded 					= structNew()>
			<cfset infosLoaded.POOLS 			= getpools>
			<cfset infosLoaded.NBRREVENDEURS 	= getrevendeursautorises>
			<cfset infosLoaded.CHAMPS 			= getchamps>
			<cfset infosLoaded.IDUSERPROFIL 	= getuserprofil>
			<cfset infosLoaded.FILTERSCMD 		= filterCommande>
			<cfset infosLoaded.IDUSER		 	= idgestionnaire>
			<cfset infosLoaded.IDRACINE 		= idracine>
		<cfreturn infosLoaded>
	</cffunction>
	
	<cffunction name="getMyProfilPool" access="remote" returntype="Struct" output="false" hint="FOURNI TOUTES LES INFOS LORS DU CHARGEMENT DU MODULE">
		<cfargument name="pools"  			required="true" type="Query"/>
		<cfargument name="idprofiluser"  	required="true" type="Numeric"/>
	
			<cfset infospools = structNew()>
	
			<cfset idpool 	= -1>
			<cfset idprofil = 0>
			<cfset cptr		= 0>
	
			<cfif idprofiluser EQ 2>
			
				<cfloop index="idx" from="1" to="#pools.RecordCount#">
					
					<cfif pools['IDREVENDEUR'][idx] GT 0>
					
						<cfif cptr EQ 0>
					
							<cfset idpool 	= pools['IDPOOL'][idx]>
							<cfset idprofil = pools['IDPROFIL'][idx]>
							
							<cfset cptr = cptr + 1>
					
						</cfif>
					
					</cfif>
					
				</cfloop>
			
			</cfif>
			
			<cfset infospools.IDPOOL 	= idpool>
			<cfset infospools.IDPROFIL 	= idprofil>
	
		<cfreturn infospools>
	</cffunction>

</cfcomponent>