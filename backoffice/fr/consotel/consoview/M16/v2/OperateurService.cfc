<cfcomponent output="false" displayname="fr.consotel.consoview.M16.v2.OperateurService">

	<!--- FOURNIT LA LISTE DES ACTIONS SUR UNE COMMANDE --->
	
	<cffunction name="getHistoriqueInfosClientOperateur" access="remote" returntype="query" output="false" hint="FOURNIT LA LISTE DES ACTIONS SUR UNE COMMANDE">		
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.GETHISTORIQUEINFOSCLIENTOP" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#Session.PERIMETRE.ID_GROUPE#">
				<cfprocresult name="qHistorique">			
			</cfstoredproc>
		
		<cfreturn qHistorique >
	</cffunction>

	<!--- FOURNIT LES INFOS D'UN OPERATEUR SELECTIONNE(EGAGEMENT 12, 24, 32, 48, PÉNALITÉ, ETC) --->
	
	<cffunction name="getInfosClientOperateurActuel" access="remote" returntype="any" output="true" hint="FOURNIT LES INFOS D'UN OPERATEUR SELECTIONNE(EGAGEMENT 12, 24, 32, 48, PÉNALITÉ, ETC)">
		<cfargument name="IDOPERATEUR" required="true" type="numeric"/>
				
		<cfset histoInfos = getHistoriqueInfosClientOperateur()>
		<cfquery dbtype="query" name="qGetInfo">
			select * from histoInfos where histoInfos.BOOL_ACTUEL = 1
			and  histoInfos.OPERATEURID = #IDOPERATEUR#
		</cfquery>
		
		<cfset infos = createObject("component","fr.consotel.consoview.M16.v2.vo.InfosClientOperateurVo")>
		
				
		<cfif qGetInfo.recordCount gt 0>
			<cfset infos.setOPERATEUR_CLIENT_ID(qGetInfo["OPERATEUR_CLIENT_ID"][1])/>
			<cfset infos.setOPERATEURID(qGetInfo["OPERATEURID"][1])/>
			<cfset infos.setNOM_OP(qGetInfo["NOM_OP"][1])/>
			
			<cfset infos.setIDRACINE(qGetInfo["IDRACINE"][1])/>
			
			<cfset infos.setDATE_MODIF(qGetInfo["DATE_MODIF"][1])/>
			<cfset infos.setDATE_CREATE(qGetInfo["DATE_CREATE"][1])/>
			
			<cfset infos.setMONTANT_MENSUEL_PENALITE(val(qGetInfo["MONTANT_MENSUEL_PENALITE"][1]))/>
			<cfset infos.setFRAIS_FIXE_RESILIATION(val(qGetInfo["FRAIS_FIXE_RESILIATION"][1]))/>
			<cfset infos.setFPC_UNIQUE(qGetInfo["FPC_UNIQUE"][1])/>
			<cfset infos.setBOOL_ACTUEL(val(qGetInfo["BOOL_ACTUEL"][1]))/>
			
			<cfset infos.setUSER_MODIF(val(qGetInfo["USER_MODIF"][1]))/>
			<cfset infos.setUSER_CREATE(val(qGetInfo["USER_CREATE"][1]))/>
			
			<cfset infos.setNOM_USER_MODIFIED(qGetInfo["NOM_USER_MODIFIED"][1])/>
			<cfset infos.setNOM_USER_CREATE(qGetInfo["NOM_USER_CREATE"][1])/>
			
			<cfset infos.setENGAG_12(val(qGetInfo["ENGAG_12"][1]))/>			
			<cfset infos.setENGAG_24(val(qGetInfo["c"][1]))/>
			<cfset infos.setENGAG_36(val(qGetInfo["ENGAG_36"][1]))/>
			<cfset infos.setENGAG_48(val(qGetInfo["ENGAG_48"][1]))/>
			<cfset infos.setSEUIL_TOLERANCE(val(qGetInfo["SEUIL_TOLERANCE"][1]))/>
			
			<cfset infos.setDUREE_ELIGIBILITE(val(qGetInfo["DUREE_ELLIGIBILITE"][1]))/>
			<cfset infos.setDUREE_OUVERTURE(val(qGetInfo["DUREE_OUVERTURE"][1]))/>
			<cfset infos.setDUREE_RENOUVEL(val(qGetInfo["DUREE_RENOUVELLEMENT"][1]))/>
			
		</cfif>
		
		<cfreturn infos>
	</cffunction>

	<!--- FOURNIT LES INFOS D'UN OPERATEUR SELECTIONNE(EGAGEMENT 12, 24, 32, 48, PÉNALITÉ, ETC) --->
	
	<cffunction name="getInfosClientOperateurActuelV2" access="remote" returntype="Query" output="true" hint="FOURNIT LES INFOS D'UN OPERATEUR SELECTIONNE(EGAGEMENT 12, 24, 32, 48, PÉNALITÉ, ETC)">
		<cfargument name="idoperateur" required="true" type="numeric"/>
				
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.gethistoriqueinfosclientop">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#session.perimetre.id_groupe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid"  value="#idoperateur#">
				<cfprocresult name="p_retour">			
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LA LISTE DES OPERATEURS AUTORISES --->
	
	<cffunction name="fournirOperateursAutorises" access="remote" returntype="Array" hint="FOURNIT LA LISTE DES OPERATEURS AUTORISES">
		<cfargument name="iduser"			type="Numeric" required="true">
		<cfargument name="idtypecommande"	type="Numeric" required="true">
		
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m16.opeAutorises">	
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_iduser" 				value="#Session.USER.CLIENTACCESSID#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" value="#idtypecommande#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
						
			<cfset operateurs = ArrayNew(1)>
			<cfset allOperateur = p_retour>
			
			<cfloop index="idx" from="1" to="#p_retour.recordcount#">
				<cfset operateur = structNew()>
				<cfset operateur.LIBELLE = allOperateur['NOM'][idx]>
				<cfset operateur.OPERATEURID = allOperateur['OPERATEURID'][idx]>
				<cfset operateur.PAYS = allOperateur['PAYS'][idx]>
				<cfset operateur.PAYSCONSOTELID = allOperateur['PAYSCONSOTELID'][idx]>
				
				<cfset ArrayAppend(operateurs, operateur)>
			</cfloop>
					
		<cfreturn operateurs>
	</cffunction>

	<!--- FOURNIT LE LISTE DE TOUS LES OPÉRATEURS DU SEGMENT --->
	
	<cffunction name="fournirListeOperateursSegment" access="remote" returntype="any" output="false" hint="FOURNIT LE LISTE DE TOUS LES OPÉRATEURS DU SEGMENT.SEGMENT = FIXDATA | MOBILE | TOUT" >
		<cfargument name="segment" required="true" type="numeric" default="" displayname="string segment" hint="Initial value for the segmentproperty." />
			
			<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.getFabOp_actif">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idRacine" value="#idRacine#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idRacine" value="#segment#"/>
				<cfprocresult name="qResult">
			</cfstoredproc>
		<cfreturn qResult/>
	</cffunction>
	
	<cffunction name="fournirListeOperateursPays" access="remote" returntype="any" output="false" hint="FOURNIT LE LISTE DE TOUS LES OPÉRATEURS D'UN PAYS" >
		<cfargument name="idPays" required="true" type="numeric"/>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.getListOpePays">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idpays" value="#ARGUMENTS.idPays#"/>
				<cfprocresult name="qResult">
			</cfstoredproc>
		<cfreturn qResult/>
	</cffunction>

</cfcomponent>