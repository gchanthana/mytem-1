<cfcomponent displayname="fr.consotel.consoview.M16.v2.CommandeService" hint="Gestion des commandes" output="false">

	<!--- ENREGISTREMENT D'UNE COMMANDE ET DE CES ARTICLES. COMMANDE , MODIFICATION. --->

	<cffunction name="enregistrerCommandeV2" displayname="enregistrerCommande" access="remote" output="false" returntype="Numeric" hint="enregistrement des informations d'une commande">
		<cfargument name="mail" 			 	required="true" type="fr.consotel.consoview.util.Mail"/>
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idCompte" 			required="true" type="numeric" 	default="" 		/>
		<cfargument name="idSousCompte" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idOperateur" 			required="true" type="numeric" 	default="NULL" 	/>
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		/>
		<cfargument name="idContactRevendeur" 	required="true" type="numeric" 	default="0" 	/>
		<cfargument name="idTransporteur" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	/>
		<cfargument name="numeroTracking" 		required="true" type="string" 	default="NULL" 	/>
		<cfargument name="libelleCommande"		required="true" type="string" 	default="" 		/>
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		/>
		<cfargument name="refClient2" 			required="true" type="string" 	default="" 		/>
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		/>
		<cfargument name="dateOperation"		required="true" type="string" 	default="" 		/>
		<cfargument name="dateLivraison"		required="true" type="string" 	default="" 		/>
		<cfargument name="commentaires" 		required="true" type="string" 	default="NULL" 	/>
		<cfargument name="boolDevis" 			required="true" type="string" 	default="NULL" 	/>
		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		/>
		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		/>
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="segmentData" 			required="true" type="numeric" 	default="" 		/>
		<cfargument name="idTypeCommande" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idGroupeRepere" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="numeroCommande" 		required="true" type="string" 	default="NULL" 	/>
		<cfargument name="idProfileEquipement"	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idSiteFacturation"	required="true" type="numeric" 	default="NULL"	/>
		<cfargument name="libelleAttention"	    required="true" type="string" 	default="NULL"	/>
		<cfargument name="articlesInArray" 		required="true" type="Any" 		default="" 		/>
		<cfargument name="V1" 					required="true" type="string" 	default="" 		/>
		<cfargument name="V2" 					required="true" type="string" 	default="" 		/>
		<cfargument name="V3" 					required="true" type="string" 	default="" 		/>
		<cfargument name="V4" 					required="true" type="string" 	default="" 		/>
		<cfargument name="V5" 					required="true" type="string" 	default="" 		/>
		<cfargument name="filesInArray" 		required="true" type="Any" 		default="" 		/>
		<cfargument name="boolmail" 			required="true" type="Numeric"  				/>
		<cfargument name="action" 				required="true" type="Struct" 					/>
		<cfargument name="uuidReference" 		required="true" type="String"  					/>
		<cfargument name="typeCMD"		 		required="false" type="String" 	default=""		/>

			<cfset idracine = session.perimetre.id_groupe>
			<cfset CONST_CMD_SPIE = "SPIE">

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.ENREGISTREROPERATION_V6">

				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcompte" 			value="#idCompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsous_compte" 		value="#idSousCompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperateur" 		value="#idOperateur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcontact_revendeur" value="#idContactRevendeur#" null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idtransporteur" 		value="#idTransporteur#" null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_livraison" 	value="#idSiteDeLivraison#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 			   variable="p_date_effet_prevue" 	value="#dateLivraison#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idgroupe_repere" 	value="#idGroupeRepere#" null="#iif((idGroupeRepere eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_profil_commande" 	value="#idProfileEquipement#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="P_idsite_facturation"  value="#idSiteFacturation#">
	 			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_v1" 					value="#V1#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_v2"				 	value="#V2#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_v3"				 	value="#V3#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_v4"				 	value="#V4#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_v5"				 	value="#V5#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_attention"   value="#libelleAttention#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client_2"   		value="#refClient2#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_segment_data"   		value="#segmentData#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_liv_disrtib"   		value="0">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperation">
			</cfstoredproc>

			<cfif p_idoperation GT 0>


				<cftry>
					
		            <cfthread action="run" name="attachements1" mycommande1="#p_idoperation#" myfiles1="#filesInArray#">

		                <cftry>

											<cfset setFilesRslt = createObject('component',"fr.consotel.consoview.M16.upload.UploaderFile").setDataBaseFilesUploaded(myfiles1, mycommande1)>

						<cfcatch>

							<!--- EN CAS D'ÉCHEC DU THREAD --->
							<cflog type="error" text="#CFCATCH.Message#">

							<cfmail server="mail-cv.consotel.fr" port="25" from="bdcmobile@saaswedo.com" to="monitoring@saaswedo.com"
									subject="[WARN-THREAD]Erreur thread [attachements1]" type="html">


									<cfoutput>

										Tread en erreur -> <br/><br/>
										Thread		: enregistrerCommande -> attachements1<br/>
										Commande N° : #mynumcommande1#<br/>
										Idcommande  : #mycommande2#<br/><br/>
										#CFCATCH.Message#<br/>

									</cfoutput>

							</cfmail>

						</cfcatch>
						</cftry>

		            </cfthread>
								<cfset s = new fr.saaswedo.utils.thread.TagLibUtil()>
								<cfset s.cacheScriptObjects()>
		            <cfthread action="run" name="articles1" mycommande2="#p_idoperation#" mypool1="#idPoolGestionnaire#" myarticles1="#articlesInArray#"
		           											mytypecommande1="#idTypeCommande#" myoperator1="#idOperateur#" istosend1="#boolmail#" myGestionnaire="#arguments.idGestionnaire#"
		           												myaction1="#action#" mynumcommande1="#numeroCommande#"  myuuidreference1="#uuidReference#"
		           													mymail1="#mail#" myArgs="#ARGUMENTS#" idracine1="#idracine#">
							<cftry>


								<cfif #myArgs.typeCMD# Eq #CONST_CMD_SPIE#>
									<cfset rslt = enregistrerArticlesService(#mycommande2#, #mypool1#, #myarticles1#, #myArgs#)>
								<cfelse>
									<cfset rslt = enregistrerArticles(#mycommande2#, #mypool1#, #myarticles1#)>
								</cfif>

								<cfset args = {}>
								<cfset args['idRacine'] = idracine1>
								<cfset args['idCommande'] = mycommande2>
								<cfset args['numeroCommande'] = mynumcommande1>
								<cfset args['UUID'] = myuuidreference1>
								<cfset args['idGestionnaire']=myGestionnaire>
								
								
								<!--- veru GFC2 Digital Dimension --->
								<cfset x = new fr.consotel.consoview.M16.export.gfc2dd.AttachmentBuilder() >
								<cfset r = x.build({idracine=args['idRacine'], 
											numCommande=args['numeroCommande'],
											idcommande=args['idCommande'],
											uuid=args['UUID'],
											idGestionnaire= args['idGestionnaire']})>									
								<!--- veru GFC2 Digital Dimension --->
								

								<cfif myaction1.IDACTION GT 0>
									<cfset mailObject = createObject('component',"fr.consotel.consoview.M16.v2.commande_sncf")>
			 						<cfset rslt = mailObject.sendMailInfosNewCommande(#mymail1#, #mycommande2#, #mytypecommande1#, #myoperator1#, #istosend1#,
																						#myaction1#, #mynumcommande1#, #myuuidreference1#, 0)>

								</cfif>

								<cfinvoke method="CreateOrder" component="fr.consotel.consoview.M16.v2.SonarService"
									argumentcollection="#args#" >


						 	<cfcatch>

							<!--- EN CAS D'ÉCHEC DU THREAD --->
							<cflog type="error" text="#CFCATCH.Message#">
							<cfmail server="mail-cv.consotel.fr" port="25" from="bdcmobile@saaswedo.com" to="monitoring@saaswedo.com"
									subject="[WARN-THREAD]Erreur thread [articles1]" type="html">


									<cfoutput>

										Thread en erreur -> <br/><br/>
										Thread		: enregistrerCommande -> articles1<br/><br/>
										Commande N° : #mynumcommande1#<br/><br/>
										Idcommande  : #mycommande2#<br/><br/><br/>
										Message		: #CFCATCH.Message#<br/><br/>
										Erreur		: <cfdump var="#CFCATCH#">


									</cfoutput>

							</cfmail>

						</cfcatch>
						</cftry>

		            </cfthread>

	            <cfcatch>

					<!--- EN CAS D'ÉCHEC DES THREADS --->
					<cflog type="error" text="#CFCATCH.Message#">

					<cfmail server="mail-cv.consotel.fr" port="25" from="bdcmobile@saaswedo.com" to="monitoring@saaswedo.com"
									subject="[WARN-THREAD]Erreur thread" type="html">

							<cfoutput>

								Tread en erreur -> <br/><br/>
								Type		: enregistrerCommande<br/>
								Commande N° : #numeroCommande#<br/>
								Idcommande  : #p_idoperation#<br/><br/>
								Message		: #CFCATCH.Message#<br/><br/>
								Erreur		: <cfdump var="#CFCATCH#">

							</cfoutput>

					</cfmail>

				</cfcatch>
				</cftry>

			<cfelse>

				<cfset p_idoperation = 0>

			</cfif>

		<cfreturn p_idoperation>
	</cffunction>

	<!--- ENREGISTREMENT DES INFOS D'UNE LISTE DE COMMANDES DE TYPE RÉSILIATION --->
	<cffunction name="enregistrerResiliationV2" access="remote" returntype="Array" output="false" hint="ENREGISTREMENT DES INFOS D'UNE LISTE DE COMMANDES DE TYPE RÉSILIATION">
		<cfargument name="mail" 			required="true" type="fr.consotel.consoview.util.Mail"/>
		<cfargument name="commandes" 		required="true" type="Array"/>
		<cfargument name="files"	 		required="true" type="Array"/>
		<cfargument name="uuidRef"	 		required="true" type="String"/>
		<cfargument name="boolmail" 		required="true" type="Numeric"/>
		<cfargument name="action" 			required="true" type="Struct"/>

		 	<cfset var objectCommande 	= ArrayNew(1)>
			<cfset var idsCommande 		= ArrayNew(1)>
 			<cfset var idRacine 		= session.perimetre.id_groupe>
			<cfset var index 			= 1>
			<cfset var checkOK 			= 1>

			<cfset var idoperator 		= 0>
			<cfset var idtypecmd 		= 0>
			<cfset var idpoolgEst 		= 0>

 			<cfloop index="idx" from="1" to="#ArrayLen(commandes)#">

				<cfif checkOK GTE 1>

					<cfset idGestionnaire 		= commandes[idx].cmd.IDGESTIONNAIRE>
					<cfset idPoolGestionnaire 	= commandes[idx].cmd.IDPOOL_GESTIONNAIRE>
					<cfset idCompte 			= commandes[idx].cmd.IDCOMPTE_FACTURATION>
					<cfset idSousCompte 		= commandes[idx].cmd.IDSOUS_COMPTE>
					<cfset idOperateur 			= commandes[idx].cmd.IDOPERATEUR>
					<cfset idRevendeur 			= commandes[idx].cmd.IDREVENDEUR>
					<cfset idContactRevendeur 	= commandes[idx].cmd.IDCONTACT>
					<cfset idTransporteur 		= commandes[idx].cmd.IDTRANSPORTEUR>
					<cfset idSiteDeLivraison 	= commandes[idx].cmd.IDSITELIVRAISON>
					<cfset numeroTracking 		= commandes[idx].cmd.NUMERO_TRACKING>
					<cfset libelleCommande 		= commandes[idx].cmd.LIBELLE_COMMANDE>
					<cfset refClient 			= commandes[idx].cmd.REF_CLIENT1>
					<cfset refClient2 			= commandes[idx].cmd.REF_CLIENT2>
					<cfset refRevendeur 		= commandes[idx].cmd.REF_OPERATEUR>
					<cfset dateOperation 		= commandes[idx].cmd.DATE_HEURE>
					<cfset dateLivraison 		= DateFormat(commandes[idx].cmd.LIVRAISON_PREVUE_LE,'YYYY-MM-DD')>
					<cfset commentaires 		= commandes[idx].cmd.COMMENTAIRES>
					<cfset boolDevis 			= 0>
					<cfset montant 				= commandes[idx].cmd.MONTANT>
					<cfset segmentFixe 			= commandes[idx].cmd.SEGMENT_FIXE>
					<cfset segmentMobile 		= commandes[idx].cmd.SEGMENT_MOBILE>
					<cfset segmentData 			= commandes[idx].cmd.SEGMENT_DATA>
					<cfset idTypeCommande 		= commandes[idx].cmd.IDTYPE_COMMANDE>
					<cfset idGroupeRepere 		= commandes[idx].cmd.IDGROUPE_REPERE>
					<cfset numeroCommande 		= commandes[idx].cmd.NUMERO_COMMANDE>
					<cfset idProfileEquipement 	= commandes[idx].cmd.IDPROFIL_EQUIPEMENT>

					<cfset libelleAttention 	= commandes[idx].cmd.LIBELLE_TO>
					<cfset idSiteFacturation 	= commandes[idx].cmd.IDSITEFACTURATION>
					<cfset V1 					= commandes[idx].cmd.V1>
					<cfset V2					= commandes[idx].cmd.V2>
					<cfset V3 					= commandes[idx].cmd.V3>
					<cfset V4 					= commandes[idx].cmd.V4>
					<cfset V5 					= commandes[idx].cmd.V5>

					<cfset idoperator 		= commandes[idx].cmd.IDOPERATEUR>
					<cfset idtypecmd 		= commandes[idx].cmd.IDTYPE_COMMANDE>
					<cfset idpoolgest 		= commandes[idx].cmd.IDPOOL_GESTIONNAIRE>

					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.ENREGISTREROPERATION_V6">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#idRacine#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idGestionnaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcompte" 			value="#idCompte#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsous_compte" 		value="#idSousCompte#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperateur" 		value="#idOperateur#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"			   variable="p_idrevendeur" 		value="#idRevendeur#" 		 null="#iif((idRevendeur eq 0), de("yes"), de("no"))#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcontact_revendeur" value="#idContactRevendeur#" null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idtransporteur" 		value="#idTransporteur#" 	 null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_livraison" 	value="#idSiteDeLivraison#"  null="#iif((idSiteDeLivraison eq 0), de("yes"), de("no"))#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 			   variable="p_date_effet_prevue" 	value="#dateLivraison#" >
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
						<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idinv_type_ope" 		value="#idTypeCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idgroupe_repere" 	value="#idGroupeRepere#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_profil_commande" 	value="#idProfileEquipement#" null="#iif((idProfileEquipement eq 0), de("yes"), de("no"))#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_facturation"	value="#idSiteFacturation#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_V1" 					value="#V1#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_V2" 					value="#V2#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_V3" 					value="#V3#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_V4" 					value="#V4#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_V5" 					value="#V5#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_attention"   value="#libelleAttention#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client_2"   		value="#refClient2#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_segment_data"   		value="#segmentData#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_liv_disrtib"   		value="0">
						<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperation">
					</cfstoredproc>

					<cfif p_idoperation GT 0>

						<cfset cmdstruct 			= structnew()>
						<cfset cmdstruct.IDCOMMANDE = #p_idoperation#>
						<cfset cmdstruct.ARTICLES	= #commandes[idx].articles#>
						<cfset cmdstruct.NUMCMD		= #commandes[idx].cmd.NUMERO_COMMANDE#>

            <cfset checkOK = p_idoperation>

            <cfset ArrayAppend(objectCommande, cmdstruct)>
						<cfset ArrayAppend(idsCommande, checkOK)>

					<cfelse>

						<cfset checkOK =  -1>
						<cfset ArrayAppend(idsCommande, checkOK)>

					</cfif>

				</cfif>

			</cfloop>

			<cftry>
				<cfset s = new fr.saaswedo.utils.thread.TagLibUtil()>
				<cfset s.cacheScriptObjects()>
				<cfthread action="run" name="articles2" mycommandes2="#objectCommande#" mypool2="#idpoolgest#" myfiles2="#files#" myDistrib="#idRevendeur#"
					           											mytypecommande2="#idtypecmd#" myoperator2="#idoperator#" istosend2="#boolmail#" idGestionnaire2="#idGestionnaire#"
					           												myaction2="#action#" myuuidreference2="#uuidRef#" mymail2="#mail#" idRacine2="#idRacine#">

					<cftry>

						<cfset idcmd 	= ''>
						<cfset article  = ''>
						<cfset numcmd 	=''>

						<cfloop index="idx2" from="1" to="#ArrayLen(mycommandes2)#">

							<cfset idcmd 	= mycommandes2[idx2].IDCOMMANDE>
							<cfset article  = mycommandes2[idx2].ARTICLES>
							<cfset numcmd 	= mycommandes2[idx2].NUMCMD>
							<cfset typecmd 	= mytypecommande2>
							<cfset Infos = {}>
							<cfset Infos.idrevendeur = myDistrib>
							<cfset Infos.idOperateur = myoperator2>
							<cfset Infos.idTypeOpe = mytypecommande2>

							<cfif idcmd GT 0>

								<cfset setArticlesRslt = enregistrerArticles(#idcmd#, #mypool2#, #article#,infos)>

								<cfset filesCurrentUUID = createUUID()>

								<cfloop index="idxFiles" from="1" to="#ArrayLen(myfiles2)#">

									<cfset myfiles2[idxFiles].fileIdCommande 	= idcmd>
									<cfset myfiles2[idxFiles].fileUUID 			= filesCurrentUUID>

								</cfloop>

								<cfset setFilesRslt = createObject('component',"fr.consotel.consoview.M16.upload.UploaderFile").uploadFiles(myfiles2, 1)>

								<cfset args = {}>
								<cfset args['idRacine'] = idRacine2>
								<cfset args['idCommande'] = idcmd>							
								<cfset args['numeroCommande'] = numcmd>
								<cfset args['UUID'] = filesCurrentUUID>
								<cfset args['idGestionnaire']=idGestionnaire2>
								
								<!--- veru GFC2 Digital Dimension --->
								<cfset x = new fr.consotel.consoview.M16.export.gfc2dd.AttachmentBuilder() >
								<cfset r = x.build({idracine=args['idRacine'], 
											numCommande=args['numeroCommande'],
											idcommande=args['idCommande'],
											uuid=args['UUID'],
											idGestionnaire= args['idGestionnaire']})>

								<!--- ENVOI DU MAIL DE MODIFICATION --->

								

								<cfif myaction2.IDACTION GT 0>
									<cfset mailObject = createObject('component',"fr.consotel.consoview.M16.v2.commande_sncf")>
			 						<cfset rslt = mailObject.sendMailInfosNewCommande(#mymail2#, #idcmd#, #mytypecommande2#, #myoperator2#, #istosend2#,
			 																			#myaction2#, #numcmd#, #myuuidreference2#, 0)>

								</cfif>

								<cfif typecmd eq 1382>
									<cfinvoke method="CreateOrder" component="fr.consotel.consoview.M16.v2.SonarService"
										argumentcollection="#args#" >

								<cfelse>

									<cfinvoke method="CreateTicket" component="fr.consotel.consoview.M16.v2.SonarService"
										argumentcollection="#args#" >
								</cfif>


							</cfif>

						</cfloop>

					<cfcatch>

						<!--- EN CAS D'ÉCHEC DU THREAD --->
						<cflog type="error" text="#CFCATCH.Message#">

						<cfmail server="mail-cv.consotel.fr" port="25" from="bdcmobile@saaswedo.com" to="monitoring@saaswedo.com"
						subject="[WARN-THREAD]Erreur thread [articles2]" type="html">

								<cfoutput>

									Tread en erreur -> <br/><br/>
									Thread		: enregistrerResiliation -> articles2<br/>
									Idcommandes  : <br/>
									<cfdump var="#idcmd#"><br/><br/>
									Message		: #CFCATCH.Message#<br/><br/>
									Erreur		: <cfdump var="#CFCATCH#">

								</cfoutput>

						</cfmail>

					</cfcatch>
					</cftry>

	            </cfthread>

			<cfcatch>

				<!--- EN CAS D'ÉCHEC DES THREADS --->
				<cflog type="error" text="#CFCATCH.Message#">

				<cfmail server="mail-cv.consotel.fr" port="25" from="bdcmobile@saaswedo.com" to="monitoring@saaswedo.com"
						subject="[WARN-THREAD]Erreur thread" type="html">


						<cfoutput>

							Tread en erreur -> <br/><br/>
							Type		: enregistrerResiliation<br/>
							Commande N° : #numeroCommande#<br/>
							Idcommande  : #p_idoperation#<br/><br/>
							Message		: #CFCATCH.Message#<br/><br/>
							Erreur		: <cfdump var="#CFCATCH#">

						</cfoutput>

				</cfmail>

			</cfcatch>
			</cftry>

		<cfreturn idsCommande>
	</cffunction>

	<!--- MISE À JOUR DES INFORMATIONS D'UNE COMMANDE --->

	<cffunction name="majCommandeV2" displayname="majCommande" access="remote" output="false" returntype="numeric" hint="MISE À JOUR DES INFORMATIONS D'UNE COMMANDE">
		<cfargument name="idoperation" 			required="true" type="numeric"/>
		<cfargument name="idgestionnaire" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idpoolgestionnaire" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idtransporteur" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idsitedelivraison" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idcontactrevendeur" 	required="true" type="numeric" 	default="null" 	/>
		<cfargument name="numerooperation" 		required="true" type="string" 	default="" 		/>
		<cfargument name="numerotracking" 		required="true" type="string" 	default="null" 	/>
		<cfargument name="libelleoperation" 	required="true" type="string" 	default="null" 	/>
		<cfargument name="refclient"			required="true" type="string" 	default="" 		/>
		<cfargument name="refclient2" 			required="true" type="string" 	default="" 		/>
		<cfargument name="refrevendeur" 		required="true" type="string" 	default="" 		/>
		<cfargument name="datelivraison" 		required="true" type="string" 	default="" 		/>
		<cfargument name="commentaires"			required="true" type="string" 	default="" 		/>
		<cfargument name="montant"				required="true" type="string" 	default="" 		/>
		<cfargument name="libelleto" 			required="true" type="string" 	default="null" 	/>
		<cfargument name="idcompte" 			required="true" type="numeric" 	default="null" 	/>
		<cfargument name="idsouscompte" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idsitedefacturation"	required="true" type="numeric" 	default="" 		/>
		<cfargument name="articlesinarray" 		required="true" type="Array"  	default=""		/>
		<cfargument name="uuidreference"		required="true" type="String" 	default="" 		/>
		<cfargument name="filesinarray" 		required="true" type="Array"  	default=""		/>
 		<cfargument name="idtypecommande" 		required="true" type="Numeric" 	default="" 		/>
		<cfargument name="idoperateur" 			required="true" type="Numeric" 	default="" 		/>
		<cfargument name="boolmail" 			required="true" type="Numeric" 	default="0" 	/>
		<cfargument name="action" 				required="true" type="Struct" 					/>
		<cfargument name="modification"			required="true" type="Struct" 					/>
		<cfargument name="mail" 				required="true" type="fr.consotel.consoview.util.Mail"/>
		<cfargument name="libmodification"		required="true" type="String" 	default="" 		/>

 			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.MAJCOMMANDE_V4">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idoperation" 			value="#idoperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idgestionnaire" 		value="#idgestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idtransporteur" 		value="#idtransporteur#" 		null="#iif((idtransporteur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idsite_livraison" 		value="#idsitedelivraison#" 	null="#iif((idsitedelivraison eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idcontact_revendeur" 	value="#idcontactrevendeur#" 	null="#iif((idcontactrevendeur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_operation" 		value="#numerooperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_tracking" 		value="#numerotracking#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_operation" 		value="#libelleoperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_client" 			value="#refclient#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_revendeur" 			value="#refrevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 				variable="p_date_effet_prevue" 		value="#datelivraison#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_commentaires" 			value="#commentaires#">
				<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" null="false" 	variable="p_montant" 				value="#montant#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_attention"		value="#libelleto#">
				<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" null="false" 	variable="p_ref_client_2"			value="#refclient2#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" 	variable="p_idcompte"				value="#idcompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" 	variable="p_idsous_compte"			value="#idsouscompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 		 		variable="p_idsite_facturation"		value="#idsitedefacturation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false"  variable="p_liv_disrtib"   			value="0">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 				variable="p_idoperation">
			</cfstoredproc>

			<cfif p_idoperation GT 0>
				<cfset idracine = session.perimetre.id_groupe>

	            <cfthread action="run" name="articles3" mycommande3="#idoperation#" mypool3="#idpoolgestionnaire#" myarticles3="#articlesinarray#" myfiles3="#filesinarray#"
	           											mytypecommande3="#idtypecommande#" myoperator3="#idoperateur#" istosend3="#boolmail#" idgestionnaire3="#idgestionnaire#"
	           												myaction3="#action#" mynumcommande3="#numerooperation#"  myuuidreference3="#uuidreference#" idracine3="#idracine#"
	           													mymail3="#mail#" mymodification3="#modification#" libmodification2="#libmodification#">

					<cfset mailObject 	= createObject('component',"fr.consotel.consoview.M16.v2.commande_sncf")>
					<cfset workflow 	= createObject('component',"fr.consotel.consoview.M16.v2.WorkFlowService")>

					<cfset rslt = -10>
					<cfset rslt1 = -10>

					<cftry>

						<cfif arraylen(myarticles3) GT 0>

							<cfset rslt1 = enregistrerArticles(#mycommande3#, #mypool3#, #myarticles3#)>

						</cfif>

						<cfset rsltwlow = workflow.faireActionWorkFlow(	mymodification3.IDACTION,
																		mymodification3.DATE_HOURS,
																		mymodification3.COMMENTAIRE_ACTION,
																		mycommande3,
																		0,
																		-1)>

						<cfset args = {}>
						<cfset args['idRacine'] = idracine3>
						<cfset args['idCommande'] = mycommande3>
						<cfset args['numeroCommande'] = mynumcommande3>
						<cfset args['UUID'] = myuuidreference3>
						<cfset args['idGestionnaire']=idgestionnaire3>
								
						<!--- veru GFC2 Digital Dimension --->
						<cfset x = new fr.consotel.consoview.M16.export.gfc2dd.AttachmentBuilder() >
						<cfset r = x.build({idracine=args['idRacine'], 
									numCommande=args['numeroCommande'],
									idcommande=args['idCommande'],
									uuid=args['UUID'],
									idGestionnaire= args['idGestionnaire']})>

						<!--- ENVOI DU MAIL DE MODIFICATION --->
						<cfset rslt3 = mailObject.sendMailModificationsCommande(#mycommande3#,
																				#mynumcommande3#,
																				#mymodification3.DATE_ACTION#,
																				#session.user.globalization#,
																				libmodification2)>

						<cfif myaction3.IDACTION GT 0>

	 						<cfset rslt2 = mailObject.sendMailInfosNewCommande(#mymail3#, #mycommande3#, #mytypecommande3#, #myoperator3#, #istosend3#,
																					#myaction3#, #mynumcommande3#, #myuuidreference3#, 1)>

						</cfif>

					<cfcatch>

						<cfmail server="mail-cv.consotel.fr" port="25" from="fromM16@saaswedo.com" to="monitoring@saaswedo.com"
								subject="Erreur mise à jour commande" type="html">

							<cfoutput>

								Erreur mise à jour commande<br/>
								Résultat thread :<br/><br/>

								Message : <cfdump var="#CFCATCH.Message#"><br/><br/>
								Erreur : <cfdump var="#CFCATCH#"><br/><br/>

							</cfoutput>

						</cfmail>

					</cfcatch>
					</cftry>

	            </cfthread>

			<cfelse>

				<cfset p_idoperation = 0>

			</cfif>

		<cfreturn p_idoperation>
	</cffunction>

	<!--- FOURNI LES CHAMPS DE REFERENCE CLIENT --->

	<cffunction name="getChamps" returntype="Any">

			<cfset idracine = session.perimetre.id_groupe>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m21.getChamps">
				<cfprocparam  type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#"/>
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNI : DÉTAILSCOMMANDE, TRANSPORTEURS, HISTORIQUE,LIBELLECOMPTESOUSCOMPTE,L'ADRESSE DU REVENDEUR --->

	<cffunction name="getInfosCommande" access="remote" output="false" returntype="any" hint="LIE LA COMMANDE AUX PIÈCES JOINTES DÉPOSÉES">
		<cfargument name="idcommande" 	required="true" type="Numeric"/>
		<cfargument name="idoperateur" 	required="true" type="Numeric"/>
		<cfargument name="idrevendeur" 	required="true" type="Numeric"/>
		<cfargument name="idsegment" 	required="true" type="Numeric"/>

			<cfset infosCommande = structnew()>

			<cfthread action="run" name="details" myidcommande="#idcommande#">

				<cfset details.RESULT = fournirDetailOperation(myidcommande)>

			</cfthread>

			<cfthread action="run" name="historique" myidcommande="#idcommande#">

				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.historiqueetatsoperation" >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcommande" value="#myidcommande#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_codelangue" value="#session.user.globalization#">
					<cfprocresult name="p_retour">
				</cfstoredproc>

				<cfset historique.RESULT = p_retour>

			</cfthread>

			<cfthread action="run" name="libellecomptesouscompte" myoperateur="#idoperateur#">

				<cfset libelleObj = createObject("component","fr.consotel.consoview.M16.v2.CompteService")>

				<cfset libellecomptesouscompte.RESULT = libelleObj.fournirLibelleCompteOperateur(myoperateur)>

			</cfthread>

			<cfthread action="run" name="revendeuradresse" myrevendeur="#idrevendeur#" mysegment="#idsegment#">

				<cfif mysegment EQ 1>

					<cfset mysegment = 2>

				<cfelse>

					<cfset mysegment = 1>

				</cfif>

				<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m16.get_catalogue_revendeur_V2">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#session.perimetre.id_groupe#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#session.user.clientaccessid#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#mysegment#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#session.user.globalization#">
					<cfprocresult name="p_retour">
				</cfstoredproc>

				<cfquery name="getAdressRevendeur" dbtype="query">
					select *
					FROM p_retour
					WHERE IDREVENDEUR = #myrevendeur#
				</cfquery>

				<cfset revendeuradresse.RESULT = getAdressRevendeur>

			</cfthread>

			<cfthread action="run" name="transporteurs">

				<cfset transObj = createObject("component","fr.consotel.consoview.inventaire.commande.GestionTransporteurs")>

				<cfset transporteurs.RESULT = transObj.fournirListeTransporteurs()>

			</cfthread>

			<cfthread action="join" name="details, historique, libellecomptesouscompte, revendeuradresse, transporteurs" timeout="40000"/>

			<cfset infosCommande.DETAILS				 = cfthread.details.RESULT>
			<cfset infosCommande.HISTORIQUE 			 = cfthread.historique.RESULT>
			<cfset infosCommande.LIBELLECOMPTESOUSCOMPTE = cfthread.libellecomptesouscompte.RESULT>
			<cfset infosCommande.REVENDEURADRESSE		 = cfthread.revendeuradresse.RESULT>
			<cfset infosCommande.TRANSPORTEURS 			 = cfthread.transporteurs.RESULT>

		<cfreturn infosCommande>
	</cffunction>

	<!--- LISTE LES ARTICLES DE LA COMMANDE GROUPES PAR NUMERO DE CONFIGURATION --->

	<cffunction name="getArticleSummary" access="public" returntype="Any" output="false" hint="LISTE LES ARTICLES DE LA COMMANDE GROUPES PAR NUMERO DE CONFIGURATION">
		<cfargument name="idCommande" required="true" type="Numeric"/>
		<cfset idcodelangue 		= session.user.idglobalization>
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getArticleSummary_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idCommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_langid" value="#idcodelangue#">
				<cfprocresult name="p_retour" 	resultset="1">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_nb_ligne">
			</cfstoredproc>
			<cfset articlesInfos = structnew()>
			<cfset articlesInfos.ARTICLES = p_retour>
			<cfset articlesInfos.NBLIGNE  = p_nb_ligne>
			<cfset articlesInfos.LIGNES   = fournirLigneAffected(idCommande)>

		<cfreturn articlesInfos>
	</cffunction>

	<!--- LISTE LES LIGNES CONCERNER PAS UNE MODIFICATION SUR LIGNES EXISTANTES --->

	<cffunction name="fournirLigneAffected" access="public" returntype="query" output="false" hint="LISTE LES ARTICLES DE LA COMMANDE GROUPES PAR NUMERO DE CONFIGURATION">
		<cfargument name="idcommande" required="true" type="Numeric"/>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getOrderPhones">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcommande" value="#idcommande#">
				<cfprocresult name="p_retour" >
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<cffunction name="fournirInventaireCommandePaginate" access="remote" returntype="Query" output="false" hint="LISTE DES COMMANDE DE TOUS LES POOLS DE GESTION">
		<cfargument name="segment" 		required="true"  type="Numeric" default="-1"/>
		<cfargument name="clef" 		required="true"  type="String"  default=""/>
		<cfargument name="idpool" 		required="true"  type="Numeric" default="-1"/>
		<cfargument name="idstatut" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="idetat" 		required="true"  type="Numeric" default="-1"/>
		<cfargument name="idoperateur" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="idcompte" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="idsouscompte" required="true"  type="Numeric" default="-1"/>
		<cfargument name="idusercreate" required="true"  type="Numeric"  default="-1"/>
		<cfargument name="startindex" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="numberrecord" required="true"  type="Numeric" default="-1"/>
		<cfargument name="idcritere" 	required="true"  type="Numeric" default="0"/>
		<cfargument name="txtFilter" 	required="true"  type="String" default=""/>

			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset codelangue 		= session.user.globalization>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.fournirinventaireoperation_v5">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#idgestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 				value="#segment#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 				value="#clef#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 			value="#codelangue#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 				value="#idpool#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idstatut" 			value="#idstatut#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idetat" 				value="#idetat#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_operateur" 			value="#idoperateur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" 			value="#idcompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte" 		value="#idsouscompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_user_create" 			value="#idusercreate#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_index_debut" 			value="#startindex#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_number_of_records" 	value="#numberrecord#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcritere" 			value="#idcritere#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_critere" 				value="#txtFilter#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<cffunction name="getFiltersCommandeAndActions" access="remote" returntype="Struct" output="false" hint="FOURNIT LA LISTE DES ABONNEMENTS/OPTIONS D'UNE LIGNE">
		<cfargument name="idpool"  	required="true" type="Numeric"/>
		<cfargument name="idprofil" required="true" type="Numeric"/>

			<cfset idracine = session.perimetre.id_groupe>
			<cfset idlang 	= session.user.idglobalization>

			<cfset infosCommandes = structNew()>

			<cfthread action="run" name="filterscommande" mypool="#idpool#" myidlang="#idlang#" myracine="#idracine#">

				<cfset filterscommande.RESULT = structNew()>

				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getinfopoolcde">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool"	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_langid" 	value="#myidlang#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocresult name="p_etat" 		resultset="1">
					<cfprocresult name="p_gest" 		resultset="2">
					<cfprocresult name="p_operateur" 	resultset="3">
					<cfprocresult name="p_ss_compte" 	resultset="4">
					<cfprocresult name="p_compte" 		resultset="5">
				</cfstoredproc>

				<cfset filterscommande.RESULT.LISTEETATS 		 = p_etat>
				<cfset filterscommande.RESULT.LISTEGESTIONNAIRES = p_gest>
				<cfset filterscommande.RESULT.LISTEOPERATEURS 	 = p_operateur>
				<cfset filterscommande.RESULT.LISTECOMPTES 		 = p_ss_compte>
				<cfset filterscommande.RESULT.LISTESOUSCOMPTE 	 = p_compte>

			</cfthread>

			<cfthread action="run" name="actions" mypool="#idpool#">

				<cfset object = createObject("component","fr.consotel.consoview.M16.v2.WorkFlowService")>

				<cfset p_retour = object.fournirToutesLesActionsPossible(mypool)>

				<cfset actions.RESULT = p_retour>

			</cfthread>

			<cfthread action="run" name="actionsProfil" myprofil="#idprofil#">

				<cfset object = createObject("component","fr.consotel.consoview.M16.v2.PoolService")>

				<cfset p_retour = object.fournirListeActionProfilCommande(myprofil)>

				<cfset actionsProfil.RESULT = p_retour>

			</cfthread>

			<cfthread action="join" name="filterscommande,actions,actionsProfil" timeout="40000"/>

			<cfset infosCommandes 	 				 = structNew()>
			<cfset infosCommandes 					 = cfthread.filterscommande.RESULT>
			<cfset infosCommandes.LISTEACTIONS 		 = cfthread.actions.RESULT>
			<cfset infosCommandes.LISTEACTIONSPROFIL = cfthread.actionsProfil.RESULT>

		<cfreturn infosCommandestour>
	</cffunction>

	<!--- LIE LA COMMANDE AUX PIÈCES JOINTES DÉPOSÉES --->

	<cffunction name="addBDC" access="remote" output="false" returntype="any" hint="LIE LA COMMANDE AUX PIÈCES JOINTES DÉPOSÉES">
		<cfargument name="idcommande" 	required="true" type="Numeric"/>
		<cfargument name="dirSource" 	required="true" type="String"/>
		<cfargument name="numcommande" 	required="true" type="String"/>

			<cfset var idgestinnr = session.user.clientaccessid>

			<cfset BDCStruct 				 = structnew()>
			<cfset BDCStruct.LABEL_PIECE  	 = "BDC_" & #numcommande# & ".pdf">
			<cfset BDCStruct.SIZE 			 = 0>
			<cfset BDCStruct.JOINDRE_CFVALUE = 1>
			<cfset BDCStruct.FORMAT 		 = ".pdf">
			<cfset BDCStruct.UUID 			 = #dirSource#>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.enregistrementlistattachments">
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_fileName"  	value="#BDCStruct.LABEL_PIECE#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_path"	  	value="#BDCStruct.UUID#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_userid" 	value="#idgestinnr#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_size" 		value="#BDCStruct.SIZE#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_joinMail" 	value="#BDCStruct.JOINDRE_CFVALUE#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_format" 	value="#BDCStruct.FORMAT#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" value="#idcommande#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- LORS D'UN ENREGISTREMENT MULTIPLE COPIE, CRÉÉ LES PIÈCES JOINTES DANS D'AUTRE RÉPERTOIRE --->

	<cffunction name="copyAttachement" access="remote" output="false" returntype="any" hint="LORS D'UN ENREGISTREMENT MULTIPLE COPIE, CRÉÉ LES PIÈCES JOINTES DANS D'AUTRE RÉPERTOIRE">
		<cfargument name="idcommande" 	required="true" type="Numeric"/>
		<cfargument name="numcommande" 	required="true" type="String"/>
		<cfargument name="dirSource" 	required="true" type="String"/>
		<cfargument name="files"	 	required="true" type="Array"/>

			<cfset var rsltCopy = -10>

			<cfset co = createObject('component',"fr.consotel.consoview.M16.v2.AttachementService")>

			<cfset dirCible = co.initUUID()>

			<cfset rsltCopy = co.copyAttachement(dirSource, dirCible)>

			<cfif rsltCopy EQ 1>

				<!--- <cfset setFilesRslt = createObject('component',"fr.consotel.consoview.M16.upload.UploaderFile").copyFiles(fileAttachedRslt, newidcommande)> --->

				<cfset rsltCopy 	= co.queryAppendAttachements(idcommande, files, dirCible)>

			</cfif>

			<cfset rsltAddBDC 	= addBDC(idcommande, dirCible, numeroCommande)>

		<cfreturn rsltcopy>
	</cffunction>

	<!--- ENREGISTREMENT DES ARTICLES D'UNE COMMANDE --->

	<cffunction name="enregistrerArticles" displayname="enregistrerArticles" access="remote" output="false" returntype="Array" hint="ENREGISTREMENT DES ARTICLES D'UNE COMMANDE">
		<cfargument name="idcommande" 			required="true" type="Numeric"/>
		<cfargument name="idpoolgestionnaire" 	required="true" type="Numeric"/>
		<cfargument name="articles" 			required="true" type="Array"/>
		<cfargument name="context" 			required="false" type="struct"/>

		<cfset var rsltErase 	= eraseAticles(idcommande)>
		<cfset var _articles	= arguments.articles>
		<cfset var rslt 		= 1>
		<cfset var erreur 		= 0>
		<cfset var islast 		= 0>
		<cfset var idsCommande 	= ArrayNew(1)>
		<cfset var lenArticles 	= ArrayLen(_articles)>
		<cfset var __NEW_SIM__	= 1883>
		<cfset var simhelper	= new fr.consotel.consoview.M16.v2.util.CmdSimHelper()>
		

		<cfif  isDefined("arguments.context") and (arguments.context.idTypeOpe eq __NEW_SIM__)>
		<cflog text="------------------- context --------------" >
		<cflog text="* 	context.idRevendeur #context.idRevendeur#" >
		<cflog text="*	context.idOperateur #context.idOperateur#" >
		<cflog text="*	context.idTypeOpe #context.idTypeOpe#" >
		<cflog text="	----------- context ------------ " >
			<cfset _articles = simhelper.buildPacket(_articles,context)>
		</cfif>

		<cfset var idxArticles = 0>

		<cfloop index="idxArticles" from="1" to="#lenArticles#">

			<cfif idxArticles EQ lenArticles>

				<cfset islast = 1>

				<cfif erreur GT 0>

					<cfset islast = 0>

				</cfif>

			</cfif>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.enregistrerarticle_v5">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 		value="#idcommande#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_CLOB" 	variable="p_articles" 			value="#tostring(_articles[idxArticles])#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idpoolgestionnaire" value="#idpoolgestionnaire#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_is_last" 			value="#islast#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>

			<cfset var rslt = p_retour>

			<cfif rslt GT 0>

				<!--- ON NE FAIT RIEN C'EST OK --->

			<cfelse>

				<cfset erreur = erreur + 1>

			</cfif>

			<cfset ArrayAppend(idsCommande, rslt)>

		</cfloop>

		<cfreturn idsCommande>
	</cffunction>

	<cffunction name="enregistrerArticlesService" displayname="enregistrerArticles" access="remote" output="false" returntype="Array" hint="ENREGISTREMENT DES ARTICLES D'UNE COMMANDE">
		<cfargument name="idcommande" 			required="true" type="Numeric"/>
		<cfargument name="idpoolgestionnaire" 	required="true" type="Numeric"/>
		<cfargument name="articles" 			required="true" type="Array"/>
		<cfargument name="myArgs" 				required="true" type="Any"/>

		<cfset var rsltErase 	= eraseAticles(idcommande)>
		<cfset var rslt 		= 1>
		<cfset var erreur 		= 0>
		<cfset var islast 		= 0>
		<cfset var idsCommande 	= ArrayNew(1)>
		<cfset var lenArticles 	= ArrayLen(articles)>

		<cfset var idxArticles = 0>


			<cfset var tabArticles = setMyClob("OK", #articles#, #myArgs#)>
			<cfset var __jsoutput = SerializeJSON(tabArticles)>
			<cfset var myClob= Replace(__jsoutput,':str:','','all')>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.enregistrerarticle_v4">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 		value="#idcommande#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_CLOB" 	variable="p_articles" 			value="#myClob#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idpoolgestionnaire" value="#idpoolgestionnaire#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_is_last" 			value="#islast#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>

			<cfset var rslt = p_retour>

			<cfif rslt GT 0>

				<!--- ON NE FAIT RIEN C'EST OK --->

			<cfelse>

				<cfset erreur = erreur + 1>

			</cfif>

			<cfset ArrayAppend(idsCommande, rslt)>

		<cfreturn idsCommande>
	</cffunction>

	<cffunction name="setMyClob" displayname="setMyClob" access="remote" output="false" returntype="Array" hint="retourne un tableau des les articles de la commande">
		<cfargument name="OK" 					required="true" type="String">
		<cfargument name="inArticles" 			required="true" type="Array" />
		<cfargument name="myArgs" 				required="true" 	type="any"/>

			<cfset servEqpt = createObject("component","fr.consotel.consoview.M16.v2.ArticleService")>
			<cfset CONST_SEGEMENT = 3/>
			<cfset CONST_NV_CMD = 1/>
			<cfset var commande = {}>
			<cfset day_cmde = lsdateformat(now(),'yyyy/mm/dd')>
			<cfset h_cmde = lsTimeformat(now(),'hh:mm:ss')>
			<cfset equipements = []>
			<cfset ressources = []>
			<cfset var tabArticle = []>

			<cfset var newSousTete = genererNumeroLigneUnique(#CONST_SEGEMENT#)>

			<cfloop array="#inArticles#" index="iArticle">
				<cfif isstruct(#iArticle#)>
					<cfset qtit = "#StructFind(iArticle, "quantite")#">
					<cfset idProfilEqpt = "#StructFind(iArticle,"idArticle")#"/>

					<cfset var myEqpt = servEqpt.fournirTerminaux(#myArgs.idPoolGestionnaire#,#idProfilEqpt#,#myArgs.idRevendeur#,#myArgs.idOperateur#,#CONST_SEGEMENT#,#CONST_NV_CMD#)>
					<cfloop index="i" from="1" to="#qtit#">
						<cfset ressource = {}>
						<cfset ressource["ajouter"] 			= ":str:ajouter">
						<cfset ressource["idproduit_catalogue"] = ":str:#StructFind(iArticle, "idProduitCatalogue")#">
						<cfset ressource["bonus"] 				= ":str:">
						<cfset ressource["libelle"] 			= ":str:#StructFind(iArticle, "libelleProduit")#">
						<cfset ressource["theme"] 				= ":str:#StructFind(iArticle, "themeLibelle")#">
						<cfset ressource["idThemeProduit"] 		= ":str:#StructFind(iArticle, "idThemeProduit")#">
						<cfset ressource["bool_acces"] 			= ":str:#StructFind(iArticle, "boolAcces")#">
						<cfset ressource["action"] 				= ":str:A">
						<cfset ressource["action"] 				= ":str:A">
						<cfset ressource["prix"] 				= ":str:#StructFind(iArticle, "prixUnitaire")#">
						<cfset ArrayAppend(ressources,ressource)>

						<cfif myEqpt.RecordCount GT 0>
							<cfset equipement = {}>
							<cfset equipement["idequipementfournis"] 		= ":str:#myEqpt.IDEQUIPEMENT_FOURNISSEUR#">
							<cfset equipement["idequipementfournisparent"] 	= ":str:">
							<cfset equipement["libelle"] 					= ":str:#myEqpt.LIBELLE_EQ#">
							<cfset equipement["type_equipement"] 			= ":str:#myEqpt.TYPE_EQUIPEMENT#">
							<cfset equipement["idtype_equipement"] 			= ":str:#myEqpt.IDTYPE_EQUIPEMENT#">
							<cfset equipement["prix"] 						= ":str:">
							<cfset equipement["prixAffiche"] 				= ":str:">
							<cfset equipement["bonus"] 						= ":str:">
							<cfset ArrayAppend(equipements,equipement)>
						</cfif>
					</cfloop>
				<cfelse>
					<cfset ressource = {}>
					<cfset ArrayAppend(ressources,ressource)>
				</cfif>
			</cfloop>

			<cfset article = {}>
			<cfset article["numero_config"] = ":str:1">

			<cfset article["code_interne"] = ":str:SPIE #day_cmde# #h_cmde#">


			<cfset article["idSegment"] = ":str:3">
			<cfset article["idtypecommande"] = ":str:1">
			<cfset article["idtype_ligne"] = ":str:688">
			<cfset article["code_rio"] = "">
			<cfset article["date_portage"] = ":str:">


			<cfset article["idSource"] = ":str:">
			<cfset article["idCible"] = ":str:">

			<cfset article["nomemploye"] = ":str:">
			<cfset article["idemploye"] = ":str:">
			<cfset article["matricule"] = ":str:">

			<cfset article["idsoustete"] = ":str:">
			<cfset article["soustete"] = ":str:#newSousTete#">

			<cfset article["duree_elligibilite"] = ":str:0">
			<cfset article["fpc"] = ":str:">
			<cfset article["engagement_eq"] = ":str:0">
			<cfset article["engagement_res"] = ":str:0">
			<cfset article["equipement"] = equipements>
			<cfset article["ressource"] = ressources>

			<cfset var commande["article"]= article >

			<cfset ArrayAppend(tabArticle,commande)>

		<cfreturn tabArticle>
	</cffunction>

	<!--- ENREGISTREMENT DES ARTICLES D'UNE COMMANDE --->

	<cffunction name="majArticles" displayname="enregistrerArticles" access="remote" output="false" returntype="Array" hint="ENREGISTREMENT DES ARTICLES D'UNE COMMANDE">
		<cfargument name="idcommande" 			required="true" type="Numeric"/>
		<cfargument name="idpoolgestionnaire" 	required="true" type="Numeric"/>
		<cfargument name="articles" 			required="true" type="Array"/>

			<cfset var rslt = 1>
			<cfset var idsCommande = ArrayNew(1)>

			<cfloop index="idx" from="1" to="#ArrayLen(articles)#">

				<cfif rslt GTE 1>

					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M16.ENREGISTRERARTICLE">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 		value="#idcommande#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_CLOB" 	variable="p_articles" 			value="#tostring(articles[idx])#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idpoolgestionnaire" value="#idpoolgestionnaire#"/>
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
					</cfstoredproc>
					<cfset rslt = p_retour>
					<cfset ArrayAppend(idsCommande, rslt)>

				</cfif>

			</cfloop>

		<cfreturn idsCommande>
	</cffunction>

	<!--- MISE À JOUR DES INFORMATIONS D'UNE COMMANDE --->

	<cffunction name="majCommande" displayname="majCommande" access="remote" output="false" returntype="numeric" hint="MISE À JOUR DES INFORMATIONS D'UNE COMMANDE">
		<cfargument name="idoperation" 			required="true" type="numeric"/>
		<cfargument name="idgestionnaire" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idpoolgestionnaire" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idtransporteur" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idsitedelivraison" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idcontactrevendeur" 	required="true" type="numeric" 	default="null" 	/>
		<cfargument name="numerooperation" 		required="true" type="string" 	default="" 		/>
		<cfargument name="numerotracking" 		required="true" type="string" 	default="null" 	/>
		<cfargument name="libelleoperation" 	required="true" type="string" 	default="null" 	/>
		<cfargument name="refclient"			required="true" type="string" 	default="" 		/>
		<cfargument name="refclient2" 			required="true" type="string" 	default="" 		/>
		<cfargument name="refrevendeur" 		required="true" type="string" 	default="" 		/>
		<cfargument name="datelivraison" 		required="true" type="string" 	default="" 		/>
		<cfargument name="commentaires"			required="true" type="string" 	default="" 		/>
		<cfargument name="montant"				required="true" type="string" 	default="" 		/>
		<cfargument name="libelleto" 			required="true" type="string" 	default="null" 	/>
		<cfargument name="idcompte" 			required="true" type="numeric" 	default="null" 	/>
		<cfargument name="idsouscompte" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idsitedefacturation"	required="true" type="numeric" 	default="" 		/>
		<cfargument name="isLivreDistributeur"	required="false" type="numeric" default="0" 	/>

 			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.MAJCOMMANDE_V4">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idoperation" 			value="#idoperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idgestionnaire" 		value="#idgestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idtransporteur" 		value="#idtransporteur#" 		null="#iif((idtransporteur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idsite_livraison" 		value="#idsitedelivraison#" 	null="#iif((idsitedelivraison eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idcontact_revendeur" 	value="#idcontactrevendeur#" 	null="#iif((idcontactrevendeur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_operation" 		value="#numerooperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_tracking" 		value="#numerotracking#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_operation" 		value="#libelleoperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_client" 			value="#refclient#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_revendeur" 			value="#refrevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 				variable="p_date_effet_prevue" 		value="#datelivraison#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_commentaires" 			value="#commentaires#">
				<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" null="false" 	variable="p_montant" 				value="#montant#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_attention"		value="#libelleto#">
				<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" null="false" 	variable="p_ref_client_2"			value="#refclient2#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" 	variable="p_idcompte"				value="#idcompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" 	variable="p_idsous_compte"			value="#idsouscompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 		 		variable="p_idsite_facturation"		value="#idsitedefacturation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false"  variable="p_liv_disrtib"   			value="#isLivreDistributeur#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 				variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- SUPPRIMME LES ARTICLES D'UNE COMMANDE --->

	<cffunction name="eraseAticles" access="remote" returntype="Numeric" output="false" hint="SUPPRIMME LES ARTICLES D'UNE COMMANDE">
		<cfargument name="idcommande" 			required="true" type="Numeric"/>

			<cfset user = Session.USER.CLIENTACCESSID>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.DELETE_ARTICLES_OP">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperation" 	value="#idcommande#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_user" 			value="#user#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!---
		LISTE L'ENSEMBLE DES COMMANDES D'UNE RACINE, ÉMISES ENTRE LES DATES SPÉCIFIÉES.
		LA LISTE EST FILTRÉE PAR SEGMENT 0 = TOUT 1=FIXE 2=MOBILE 3=DATA.
		LA LISTE EST FILTRÉE PAR UN MOT CLÉF.
		LA LISTE EST ORDONNÉE PAR DATE DE CRÉATION DÉCROISSANTE.
	--->

	<cffunction name="fournirListeCommande" access="remote" returntype="Query" output="false" hint="LISTE L'ENSEMBLE DES COMMANDES D'UNE RACINE, ÉMISES ENTRE LES DATES SPÉCIFIÉES.">
		<cfargument name="idPool" 	 required="true"  type="numeric" 	default="" displayname="numeric pool" 		/>
		<cfargument name="dateDebut" required="true"  type="date" 		default="" displayname="date dateDebut" 	/>
		<cfargument name="dateFin" 	 required="true"  type="date" 		default="" displayname="date dateFin" 		/>
		<cfargument name="clef" 	 required="false" type="string" 	default="" displayname="string clef" 		/>
		<cfargument name="segment"   required="false" type="numeric" 	default="" displayname="numeric segment" 	/>

			<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
			<cfset IDGESTIONNAIRE = Session.USER.CLIENTACCESSID>
			<cfset CODE_LANGUE = Session.USER.GLOBALIZATION>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.FOURNIRLISTECOMMANDE_V3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 		value="#IDRACINE#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#IDGESTIONNAIRE#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 			value="#segment#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_debut" 		value="#lsdateFormat(dateDebut,'YYYY/MM/DD')#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_fin" 		value="#lsdateFormat(dateFin,'YYYY/MM/DD')#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 			value="#clef#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_pool" 			value="#idPool#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 		value="#CODE_LANGUE#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- LISTE DES COMMANDE DE TOUS LES POOLS DE GESTION --->

	<cffunction name="fournirInventaireCommande" access="remote" returntype="Query" output="false" hint="LISTE DES COMMANDE DE TOUS LES POOLS DE GESTION">
		<cfargument name="dateDebut" required="true"  type="date" 		default="" displayname="date dateDebut" />
		<cfargument name="dateFin" 	 required="true"  type="date" 		default="" displayname="date dateFin" />
		<cfargument name="clef" 	 required="true" type="string" 		default="" displayname="string clef" />
		<cfargument name="segment"   required="true" type="numeric" 	default="" displayname="numeric segment" 	/>

		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfset IDGESTIONNAIRE = Session.USER.CLIENTACCESSID>
		<cfset CODE_LANGUE = Session.USER.GLOBALIZATION>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.FOURNIRINVENTAIREOPERATION">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 		value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#IDGESTIONNAIRE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 			value="#segment#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_debut" 		value="#lsdateFormat(dateDebut,'YYYY/MM/DD')#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_fin" 		value="#lsdateFormat(dateFin,'YYYY/MM/DD')#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 			value="#clef#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 		value="#CODE_LANGUE#">
			<cfprocresult name="p_retour">
		</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<cffunction name="fournirInventaireCommande2" access="remote" returntype="Query" output="false" hint="LISTE DES COMMANDE DE TOUS LES POOLS DE GESTION">
		<cfargument name="clef" 	required="true"  type="string"  default="" displayname="string clef" />
		<cfargument name="idpool" 	required="true"  type="numeric" default="" displayname="numeric pool"/>
		<cfargument name="segment" 	required="true"  type="numeric" default="" displayname="numeric segment"/>
		<cfargument name="all" 		required="false" type="numeric" default="1" displayname="numeric segment"/>

			<cfset IDRACINE 		= Session.PERIMETRE.ID_GROUPE>
			<cfset IDGESTIONNAIRE 	= Session.USER.CLIENTACCESSID>
			<cfset CODE_LANGUE 		= Session.USER.GLOBALIZATION>

			<cfif isDefined("all")>
				<cfset _all = all>
			<cfelse>
				<cfset _all = 1>
			</cfif>

			<cfif _all EQ 1>

				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.fournirinventaireoperation_v3">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 		value="#IDRACINE#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#IDGESTIONNAIRE#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 			value="#segment#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 			value="#clef#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 		value="#CODE_LANGUE#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 			value="#idpool#">
					<cfprocresult name="p_retour">
				</cfstoredproc>

			<cfelse>

				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.fournirinventaireoperation_v2">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 		value="#IDRACINE#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#IDGESTIONNAIRE#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 			value="#segment#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 			value="#clef#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 		value="#CODE_LANGUE#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 			value="#idpool#">
					<cfprocresult name="p_retour">
				</cfstoredproc>

			</cfif>

		<cfreturn p_retour>
	</cffunction>

	<cffunction name="fournirInventaire2CommandeAndFiltresWithActions" access="remote" returntype="Struct" output="false" hint="LISTE DES COMMANDE DE TOUS LES POOLS DE GESTION">
		<cfargument name="clef" 	required="true"  type="string"  default="" displayname="string clef" />
		<cfargument name="idpool" 	required="true"  type="numeric" default="" displayname="numeric pool"/>
		<cfargument name="idprofil" required="true"  type="numeric" default="" displayname="numeric pool"/>
		<cfargument name="segment" 	required="true"  type="numeric" default="" displayname="numeric segment"/>
		<cfargument name="all" 		required="false" type="numeric" default="1" displayname="numeric segment"/>

			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset code_langue 		= session.user.globalization>

			<cfif isDefined("all")>
				<cfset _all = all>
			<cfelse>
				<cfset _all = 1>
			</cfif>

			<cfthread action="run" name="inventaireOperations" mypool="#idpool#" mysegment="#segment#" myclef="#clef#" myAll="#_all#">

				<cfset procdedure = "">

				<cfif _all EQ 1>

					<cfset procdedure = "pkg_m16.fournirinventaireoperation_v3">

				<cfelse>

					<cfset procdedure = "pkg_m16.fournirinventaireoperation_v2">

				</cfif>

				<cfstoredproc datasource="#session.offredsn#" procedure="#procdedure#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 		value="#session.perimetre.id_groupe#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#session.user.clientaccessid#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 			value="#mysegment#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 			value="#myclef#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 		value="#session.user.globalization#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 			value="#mypool#">
					<cfprocresult name="p_retour">
				</cfstoredproc>

				<cfquery name="getInventaireCommandes" dbtype="query">
					select *
					FROM p_retour
					ORDER BY DATE_CREATE DESC
				</cfquery>

				<cfset inventaireOperations.RESULT = getInventaireCommandes>

			</cfthread>

			<cfthread action="run" name="actions" mypool="#idpool#">

				<cfset object = createObject("component","fr.consotel.consoview.M16.v2.WorkFlowService")>

				<cfset p_retour = object.fournirToutesLesActionsPossible(mypool)>

				<cfset actions.RESULT = p_retour>

			</cfthread>

			<cfthread action="run" name="actionsProfil" myprofil="#idprofil#">

				<cfset object = createObject("component","fr.consotel.consoview.M16.v2.PoolService")>

				<cfset p_retour = object.fournirListeActionProfilCommande(myprofil)>

				<cfset actionsProfil.RESULT = p_retour>

			</cfthread>

			<cfthread action="join" name="inventaireOperations,actions,actionsProfil" timeout="40000"/>

			<cfset query_retour = cfthread.inventaireOperations.RESULT>

			<cfthread action="run" name="etats" myquery="#query_retour#">

				<cfquery name="getListeETAT" dbtype="query">
					select DISTINCT IDINV_ETAT AS IDETAT,LIBELLE_EAT AS LIBELLE
					FROM myquery
					ORDER BY LIBELLE
				</cfquery>

				<cfset etats.RESULT = getListeETAT>

			</cfthread>

			<cfthread action="run" name="gestionnaires" myquery="#query_retour#">

				<cfquery name="getListeGEST" dbtype="query">
					select DISTINCT USERID ,USERCREATE AS LIBELLE
					FROM myquery
					ORDER BY LIBELLE
				</cfquery>

				<cfset gestionnaires.RESULT = getListeGEST>

			</cfthread>

			<cfthread action="run" name="operateurs" myquery="#query_retour#">

				<cfquery name="getListeOPER" dbtype="query">
					select DISTINCT IDOPERATEUR,LIBELLE_OPERATEUR AS LIBELLE
					FROM myquery
					ORDER BY LIBELLE
				</cfquery>

				<cfset operateurs.RESULT = getListeOPER>

			</cfthread>

			<cfthread action="run" name="comptes" myquery="#query_retour#">

				<cfquery name="getListeCOMPT" dbtype="query">
					select DISTINCT IDCOMPTE AS IDCOMPTE_FACTURATION,LIBELLE_COMPTE AS LIBELLE
					FROM myquery
					ORDER BY IDCOMPTE
				</cfquery>

				<cfset comptes.RESULT = getListeCOMPT>

			</cfthread>

			<cfthread action="run" name="souscomptes" myquery="#query_retour#">

				<cfquery name="getListeSSCPT" dbtype="query">
					select DISTINCT IDSSCOMPTE AS IDSOUS_COMPTE,LIBELLE_SSCOMPTE AS LIBELLE
					FROM myquery
					ORDER BY IDSSCOMPTE
				</cfquery>

				<cfset souscomptes.RESULT = getListeSSCPT>

			</cfthread>

			<cfthread action="join" name="etats,gestionnaires,operateurs,comptes,souscomptes" timeout="40000"/>

			<cfset infosCommandes 	 				 = structNew()>
			<cfset infosCommandes.LISTECOMMANDES 	 = cfthread.inventaireOperations.RESULT>
			<cfset infosCommandes.LISTEACTIONS 		 = cfthread.actions.RESULT>
			<cfset infosCommandes.LISTEETATS 		 = cfthread.etats.RESULT>
			<cfset infosCommandes.LISTEGESTIONNAIRES = cfthread.gestionnaires.RESULT>
			<cfset infosCommandes.LISTEOPERATEURS 	 = cfthread.operateurs.RESULT>
			<cfset infosCommandes.LISTECOMPTES 		 = cfthread.comptes.RESULT>
			<cfset infosCommandes.LISTESOUSCOMPTE 	 = cfthread.souscomptes.RESULT>
			<cfset infosCommandes.LISTEACTIONSPROFIL = cfthread.actionsProfil.RESULT>

			<cfset session.LISTEACTIONS = cfthread.actions.RESULT>

		<cfreturn infosCommandes>
	</cffunction>

	<!--- LISTE LES ARTICLES DE LA COMMANDE --->

	<cffunction name="fournirArticlesCommande" access="public" returntype="any" output="false" hint="LISTE LES ARTICLES DE LA COMMANDE">
		<cfargument name="idCommande" required="true" type="numeric" displayname="struct commande"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.FOURNIRARTICLESOPERATION" blockfactor="10">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idCommande#">
				<cfprocresult name="articlexml">
			</cfstoredproc>

		<cfreturn articlexml>
	</cffunction>

	<!--- LISTE DES EQUIPEMENTS POUR LA MISE A JOUR DES REFERENCES --->

	<cffunction name="fournirReferenceInfos" access="public" returntype="any" output="false" hint="LISTE LES ARTICLES DE LA COMMANDE">
		<cfargument name="idcommande" 	required="true" type="numeric"/>
		<cfargument name="idpool" 		required="true" type="numeric"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.getrefinfo_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idcommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idpool#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

			<cfquery name="getData" dbtype="query">
				SELECT *
				FROM p_retour
				ORDER BY IDSOUS_TETE
			</cfquery>

		<cfreturn getData>
	</cffunction>

	<!--- LISTE DES EQUIPEMENTS POUR LA MISE A JOUR DES REFERENCES POUR UN EXPORT EN MASSE --->

	<cffunction name="fournirReferencesInfos" access="public" returntype="any" output="false" hint="LISTE LES ARTICLES DE LA COMMANDE">
		<cfargument name="idscommande" 	required="true" type="String"/>
		<cfargument name="idpool" 		required="true" type="Numeric"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.getMultipleRefInfo_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 		variable="p_idcommande" value="#idscommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" 	value="#idpool#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- METS A JOUR LES ARTICLES LIVRES - COMMANDE PARTIELLE --->

	<cffunction name="SavePartialDelivery" access="public" returntype="any" output="false" hint="METS A JOUR LES ARTICLES LIVRES - COMMANDE PARTIELLE">
		<cfargument name="idCommande" 	required="true" type="numeric"/>
		<cfargument name="idArticles" 	required="true" type="String"/>

			<cfset var rsltCopy = -10>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.SavePartialDelivery">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 		value="#idcommande#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_iduser" 			value="#Session.USER.CLIENTACCESSID#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 		variable="p_list_article_deliv" value="#idArticles#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>

			<cfif p_retour GT 0>

				<cfset rsltCopy = copyAttachementCurrentCommande(idCommande, p_retour)>

			</cfif>

		<cfreturn p_retour>
	</cffunction>

	<!--- LORS D'UNE LIVRAISON PARTIELLE COPIE, CRÉÉ LES PIÈCES JOINTES DE LA COMMANDE COURANTE DANS LE NOUVEAU RÉPERTOIRE --->

	<cffunction name="copyAttachementCurrentCommande" access="remote" output="false" returntype="any" hint="LORS D'UNE LIVRAISON PARTIELLE COPIE, CRÉÉ LES PIÈCES JOINTES DE LA COMMANDE COURANTE DANS LE NOUVEAU RÉPERTOIRE">
		<cfargument name="idcommande" 		required="true" type="Numeric"/>
		<cfargument name="newidcommande" 	required="true" type="Numeric"/>

			<cfset var rsltCopy 	= -10>
			<cfset var currentUUID 	= "">
			<cfset var files 		= ArrayNew(1)>
			<cfset var fileAttach	= createObject('component',"fr.consotel.consoview.M16.v2.AttachementService")>

			<cfset fileAttachedRslt = fileAttach.fournirAttachements(idcommande)>

			<cfset fileCopyRslt = createObject('component',"fr.consotel.consoview.M16.upload.UploaderFile").copyFiles(fileAttachedRslt, newidcommande)>

		<cfreturn fileCopyRslt>
	</cffunction>

	<cffunction name="fournirDetailOperation" access="remote" returntype="Query" output="false" hint="RECUPÈRE LES INFORMATION CONCERNANT LA COMMANDE SÉLECTIONNÉE">
		<cfargument name="idCommande" required="true" type="numeric"/>

			<cfset idGestionnaire = Session.USER.CLIENTACCESSID>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.fournirDetailOperation">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 	value="#idCommande#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idgestionnaire" value="#idGestionnaire#"/>
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<cffunction name="genererNumeroLigneUnique" access="remote" returntype="string" output="false" hint="GÉNÉRE UN NUMÉRO DE LIGNE UNIQUE" >
		<cfargument name="segment" 		required="true" type="Numeric" default=""/>

			<cfset CODE_LANGUE = Session.USER.GLOBALIZATION>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.g_sous_tete_number">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in"  variable="p_segment" 	 value="#segment#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in"  variable="p_code_langue" value="#CODE_LANGUE#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="out" variable="p_retour"/>
			</cfstoredproc>

		<cfreturn p_retour/>
	</cffunction>

	<!--- LISTE DES EQUIPEMENTS POUR LA MISE A JOUR DES REFERENCES --->

	<cffunction name="fournirReferenceInfosSpie" access="public" returntype="any" output="false" hint="LISTE LES ARTICLES DE LA COMMANDE">
		<cfargument name="idcommande" 	required="true" type="numeric"/>
		<cfargument name="idpool" 		required="true" type="numeric"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.getRefInfoSPIE">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idcommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idpool#">

				<!---<cfprocparam type="out" variable="p_retour"/>
				<cfprocparam type="out" variable="p_produit"/>--->

				<cfprocresult name="p_retour" resultset="1">
				<cfprocresult name="p_produit"resultset="2">

			</cfstoredproc>
			<cfset result = structnew()>
			<cfset result.equipements = p_retour>
			<cfset result.produits = p_produit>
		<cfreturn RESULT>
	</cffunction>

</cfcomponent>
