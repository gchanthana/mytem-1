<cfcomponent displayname="fr.consotel.consoview.M16.v2.LigneService" output="false">

	<!--- RENOMME LA SOUS TETE --->
	
	<cffunction name="renommerSousTete" access="remote" returntype="numeric" hint="RENOMME LA SOUS TETE">
		<cfargument name="idsoustete" 	type="numeric" required="true">
		<cfargument name="newSousTete" 	type="string"  required="true">

			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.RENAME_SOUS_TETE">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" 	variable="p_idsous_tete" value="#idsoustete#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" 	variable="p_libelle" 	 value="#newSousTete#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
			</cfstoredproc>

		<cfreturn result/>
	</cffunction>

	<!--- Get Code RIO--->
	
	<cffunction name="getCodeRio" access="remote" returntype="string" hint="Get Code RIO">
		<cfargument name="idsoustete" 	type="numeric" required="true">

			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_gtligne.getCodeRio">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" 	variable="p_idsous_tete" value="#idsoustete#"/>
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour.CODE_RIO>
	</cffunction>

	<!--- VÉRIFIE L'NICITÉ DU NUMÉRO D'UNE LIGNE RETOURNE -1 SI LE NUMÉRO N'EST PAS UNIQUE --->
	
	<cffunction name="verifierUniciteNumeroLigne" access="remote" returntype="Array" output="false" hint="VÉRIFIE L'NICITÉ DU NUMÉRO D'UNE LIGNE RETOURNE -1 SI LE NUMÉRO N'EST PAS UNIQUE">
		<cfargument name="sousTete" required="false" type="string" default="" displayname="string sousTete" hint="Initial value for the sousTeteproperty."/>
		
			<cfset infosligne = ArrayNew(1)>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.sous_tete_exists_v2">						
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_sous_tete" value="#sousTete#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_retour"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_idsous_tete"/>			     
			</cfstoredproc>
		
			<cfset ArrayAppend(infosligne, p_retour)>
			<cfset ArrayAppend(infosligne, p_idsous_tete)>
		
		<cfreturn infosligne/>
	</cffunction>
	
	
	<!--- VÉRIFIE L'UNICITÉ DE L'IMEI RETOURNE 1 SI L'IMEI N'EST PAS UNIQUE --->
	
	<cffunction name="verifierUniciteIMEI" access="remote" returntype="Array" output="false" hint="VÉRIFIE L'UNICITÉ DE L'IMEI RETOURNE -1 SI L'IMEI N'EST PAS UNIQUE">
		<cfargument name="num_imei" required="false" type="string" default="" displayname="string IMEI" hint="Initial value for the sousTeteproperty."/>
		
			<cfset infosligne = ArrayNew(1)>
			<cfset idracine = session.perimetre.id_groupe>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.terminalExist">	<!--- #Session.OFFREDSN# --->
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" type="in" variable="p_imei" value="#num_imei#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#idracine#"/>
				<!---<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_retour"/>--->
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfset arrayAppend(infosligne, val(p_retour.IS_EXIST))/>
			
		<cfreturn infosligne />
	</cffunction>
 	
	
	<!--- VÉRIFIE L'UNICITÉ DE LA SIM RETOURNE 1 SI LA SIM N'EST PAS UNIQUE --->
	
	<cffunction name="verifierUniciteSIM" access="remote" returntype="Array" output="false" hint="VÉRIFIE L'UNICITÉ DE LA SIM D'UNE LIGNE RETOURNE -1 SI LA SIM N'EST PAS UNIQUE">
		<cfargument name="num_sim" required="false" type="string" default="" displayname="string SIM" hint="Initial value for the sousTeteproperty."/>
		
			<cfset infosligne = ArrayNew(1)>
			<cfset idracine = session.perimetre.id_groupe>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.simExist">						
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2"  type="in" variable="p_sim" value="#num_sim#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#idracine#"/>
				<!---<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_retour"/>--->
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfset arrayAppend(infosligne, val(p_retour.IS_EXIST))/>
		
		<cfreturn infosligne/>
	</cffunction>
	
	
	<!--- GÉNÉRE UN NUMÉRO DE LIGNE UNIQUE --->
	
	<cffunction name="genererNumeroLigneUnique" access="remote" returntype="string" output="false" hint="GÉNÉRE UN NUMÉRO DE LIGNE UNIQUE" >
		<cfargument name="segment" 		required="true" type="Numeric" default=""/>
			
			<cfset CODE_LANGUE = Session.USER.GLOBALIZATION>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.g_sous_tete_number">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in"  variable="p_segment" 	 value="#segment#"/>	
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in"  variable="p_code_langue" value="#CODE_LANGUE#"/>						     
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="out" variable="p_retour"/>						     
			</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- G_SOUS_TETE_NUMBER(P_SEGMENT IN INTEGER,P_CODE_LANGUE IN VARCHAR2,P_RETOUR OUT VARCHAR2) --->
	
	<cffunction name="genererNumerosLignesUnique" access="remote" returntype="Array" output="false" hint="GÉNÉRE DES NUMÉROS DE LIGNE UNIQUE ET RETOURNE UN TABLEAU">
		<cfargument name="nbrLignes" 	required="true" type="Numeric" default=""/>
		<cfargument name="segment" 		required="true" type="Numeric" default=""/>
		
			<cfset CODE_LANGUE = Session.USER.GLOBALIZATION>
		
			<cfset allLignes = ArrayNew(1)>
			<cfloop index="idx" from="1" to="#nbrLignes#">
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.g_sous_tete_number">
					<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in"  variable="p_segment" 	 value="#segment#"/>	
					<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in"  variable="p_code_langue" value="#CODE_LANGUE#"/>						     
					<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="out" variable="p_retour"/>						     
				</cfstoredproc>
				
				<cfset ArrayAppend(allLignes, p_retour)>
			</cfloop>
		<cfreturn allLignes/>
	</cffunction>
	
	<!--- OBTENIR TYPE DE LIGNE POUR UN ABO , 0 POUR AVOIR TOUS LES TYPE DE LIGNES--->
	
	<cffunction name="getTypeLigneAbo" access="remote" returntype="Query" output="false" hint="OBTIENT TYPE DE LIGNE POUR UN ABONNEMENT">
		<cfargument name="idProduitCatalogue" 	required="true" 	type="Numeric" default="0"/>
		<cfargument name="idracine" 			required="false" 	type="numeric"/>
		<cfargument name="code_langue" 			required="false" 	type="string"/>
		
			<cfif idProduitCatalogue eq 0>		
				<cfset _boolToutType = true>
			<cfelse>
				<cfset _boolToutType = false>
			</cfif>
			
			
			<cfif isdefined("idracine")>		
				<cfset _idracine = idracine>
			<cfelse>
				<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
			</cfif>
			
			<cfif isdefined("code_langue")>		
				<cfset _code_langue = code_langue>
				<cfelse>
				<cfset _code_langue = SESSION.USER.GLOBALIZATION>
			</cfif>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.GETTYPELIGNEABO">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idRacine" 			value="#_idracine#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idProduitCatalogue" value="#idProduitCatalogue#" null="#_boolToutType#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" 	type="in"	variable="p_code_langue"  		value="#_code_langue#" >
				<cfprocresult name="p_retour"> 					     
			</cfstoredproc>
			
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- OBTENIR TYPE DE LIGNE POUR UN ABO , 0 POUR AVOIR TOUS LES TYPE DE LIGNES--->
	
	<cffunction name="associerTypeLigneAbo" access="remote" returntype="Numeric" output="false" hint="ASSOCIE UN TYPE DE LIGNE POUR UN ABONNEMENT">
		<cfargument name="idType_ligne" 	required="true" 	type="Numeric" />
		<cfargument name="idProduit" 		required="true" 	type="Numeric" />
		<cfargument name="idracine" 		required="false" 	type="Numeric" />
		
			<cfif isdefined("idracine")>		
				<cfset _idracine = idracine>
			<cfelse>
				<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
			</cfif>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.associateTypeLigneAbo">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idtype_ligne" 	value="#idType_ligne#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idproduit " 	value="#idProduit#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idracine" 		value="#_idracine#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="out" 	variable="p_retour"/>
			</cfstoredproc>

		<cfreturn p_retour/>
	</cffunction>

	<!--- RECHERCHE LES LIGNES ET LE CHEMIN DANS l'ORGA --->

	<cffunction name="searchLines" access="remote" returntype="Array" output="false" hint="RECHERCHE LES LIGNES ET LE CHEMIN DANS l'ORGA">
		<cfargument name="lignes" type="Array" required="true">
		
			<cfset idracine = SESSION.PERIMETRE.ID_GROUPE>
			<cfset allLignes = ArrayNew(1)>
			
			<cfloop index="idx" from="1" to="#ArrayLen(lignes)#">
				
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.search_line"> 
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_tete" 	value="#lignes[idx]#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
				
				<cfset result = structNew()>
				
				<cfset result.SOUSTETE 	= lignes[idx]>
				<cfset result.CHEMIN 	= p_retour.CHEMIN>
				<cfset result.IDORGA 	= p_retour.ORGA>
				<cfset result.IDCIBLE 	= p_retour.IDGROUPE_CLIENT>
				
				<cfset ArrayAppend(allLignes, result)>
			</cfloop>

		<cfreturn allLignes>
	</cffunction>
	
	<!--- RETOURNE TOUTES LIGNES D'UNE RACINE SUIVANT L'OPÉRATEUR --->
	
	<cffunction name="fournirListeLignesByOperateur" access="remote" returntype="Query" output="false" hint="RETOURNE TOUTES LIGNES D'UNE RACINE SUIVANT L'OPÉRATEUR">
		<cfargument name="idRacine" 		required="true" type="Numeric"/>
		<cfargument name="idPool" 			required="true" type="Numeric"/>
		<cfargument name="idOperateur" 		required="true" type="Numeric"/>
		<cfargument name="clef" 			required="true" type="String"/>
		<cfargument name="eligibilite" 		required="true" type="Numeric"/>
		<cfargument name="idrecherche" 		required="true" type="Numeric"/>
		<cfargument name="idsegment" 		required="true" type="Numeric"/>
				
			<cfstoredproc datasource="ROCOFFRE-DB1" procedure="PKG_M16.getLignesByOperateur_v3" blockfactor="75">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine" 		value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" 		value="#idPool#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur" 	value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" 	variable="p_clef" 			value="#clef#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_eligibilite" 	value="#eligibilite#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idrecherche" 	value="#idrecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_segment" 		value="#idsegment#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="fournirListeLignesByOperateurV2" access="remote" returntype="Query" output="false" hint="Recherche les lignes facturées dernièrement chez un opérateur">
		<cfargument name="idPool" 			required="true" type="Numeric"/>
		<cfargument name="idOperateur" 		required="true" type="Numeric"/>
		<cfargument name="clef" 			required="true" type="String"/>
		<cfargument name="idrecherche" 		required="true" type="Numeric"/>
		<cfargument name="idsegment" 		required="true" type="Numeric"/>
		<cfargument name="etat" 			required="true" type="Numeric"/>
		<cfargument name="debut" 			required="true" type="Numeric"/>
		<cfargument name="fin" 				required="true" type="Numeric"/>
		<cfargument name="column" 			required="true" type="Numeric"/>
		<cfargument name="order" 			required="true" type="String"/>
		<cfargument name="showfact"			required="true" type="Numeric"/>


			<cfset idracine = session.perimetre.id_groupe>
				
			<cfstoredproc datasource="ROCOFFRE-DB1" procedure="pkg_m16.getLignesByOperateur_v7">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine" 			value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" 			value="#idPool#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur" 		value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" 	variable="p_clef" 				value="#clef#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idrecherche" 		value="#idrecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_segment" 			value="#idsegment#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_etat" 				value="#etat#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_index_debut" 		value="#debut#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_number_of_records" 	value="#fin#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_column_order" 		value="#column#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" 	variable="p_type_order" 		value="#order#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_show_factu" 		value="#showfact#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="fournirListeLignesByOperateurV3" access="remote" returntype="Query" output="false" hint="Recherche les lignes facturées dernièrement chez un opérateur">
		<cfargument name="idPool" 			required="true" type="Numeric"/>
		<cfargument name="idOperateur" 		required="true" type="Numeric"/>
		<cfargument name="clef" 			required="true" type="String"/>
		<cfargument name="idrecherche" 		required="true" type="Numeric"/>
		<cfargument name="idsegment" 		required="true" type="Numeric"/>
		<cfargument name="etat" 			required="true" type="Numeric"/>
		<cfargument name="debut" 			required="true" type="Numeric"/>
		<cfargument name="fin" 				required="true" type="Numeric"/>
		<cfargument name="column" 			required="true" type="Numeric"/>
		<cfargument name="order" 			required="true" type="String"/>
		<cfargument name="showfact"			required="true" type="Numeric"/>
		<cfargument name="eligible"			required="true" type="String" default=""/>


			<cfset idRacine = session.perimetre.id_groupe>
				
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m16.getLignesByOperateur_v9">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine" 			value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" 			value="#idPool#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur" 		value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" 	variable="p_clef" 				value="#clef#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idrecherche" 		value="#idrecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_segment" 			value="#idsegment#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_etat" 				value="#etat#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_index_debut" 		value="#debut#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_number_of_records" 	value="#fin#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_column_order" 		value="#column#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" 	variable="p_type_order" 		value="#order#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_show_factu" 		value="#showfact#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" 	variable="p_eligible" 			value="#eligible#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

	<!--- RETOURNE TOUS LES ABONNEMENTS ET OPTIONS D'UNE LIGNE --->
	
	<cffunction name="fournirLignesProduitAbonnement" access="remote" returntype="Struct" output="false" hint="RETOURNE TOUS LES ABONNEMENTS ET OPTIONS D'UNE LIGNE">
		<cfargument name="idRacine"  required="true" type="Numeric"/>
		<cfargument name="sousTetes" required="true" type="String"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.get_lignes_produit_abo_v3">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine" 	value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" 		variable="p_sous_tetes" value="#sousTetes#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"							value="#session.user.globalization#">
				<cfprocresult name="p_retour" 	resultset="1">
				<cfprocresult name="p_retour_2" resultset="2">
			</cfstoredproc>
			
  			<cfset retour = structNew()>
 			<cfset retour.LIGNESANDOPTIONS = p_retour>
			<cfset retour.NUMBERLIGNES = p_retour_2>
			
		<cfreturn retour>
	</cffunction>
	
	<!--- VERIFIE L'ELIGIBILITE D'UNE LIGNE --->
	
	<cffunction name="checkEligibilite" access="remote" returntype="Query" output="false" hint="VERIFIE L'ELIGIBILITE D'UNE LIGNE ">
		<cfargument name="idsoutete" required="true" type="string"/>
		<cfargument name="idoperarteur" required="true" type="string"/>
			
			<cfset idracine = session.perimetre.id_groupe>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.getLignesEligibilite_v2">				
				<cfprocparam type="in"  cfsqltype="CF_SQL_CLOB"    variable="p_idsoutete" value="#idsoutete#">
				<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine"  value="#idracine#">
				<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER"    variable="p_idoperarteur" value="#idoperarteur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<!--- VERIFIE L'ELIGIBILITE D'UNE LIGNE ET RECUPERE LE COMPTE SOUS COMPTE --->
	
	<cffunction name="fournirCompteSousCompte" access="remote" returntype="Query" output="false" hint="RETOURNE TOUS LES ABONNEMENTS ET OPTIONS D'UNE LIGNE">
		<cfargument name="idsoutete" 	required="true" type="String"/>
		<cfargument name="idoperateur" 	required="true" type="Numeric"/>

			<cfset idracine = session.perimetre.id_groupe>

  			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.getstrucfact_v2">
				<cfprocparam type="in"  cfsqltype="CF_SQL_CLOB"    variable="p_idsoutete" 	 value="#idsoutete#">
				<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine"  	 value="#idracine#">
				<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_idsoperateur" value="#idoperateur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- VERIFIE L'ELIGIBILITE D'UNE LIGNE ET RECUPERE LE COMPTE SOUS COMPTE --->
	
	<cffunction name="checkEligibiliteAndCompteSousCompte" access="remote" returntype="Struct" output="false" hint="Retourne l'éligibilité d'une ligne">
		<cfargument name="idsoutete" 		required="true" type="String"/>
		<cfargument name="eligibiliteetat" 	required="true" type="Numeric"/>
		<cfargument name="idoperateur" 		required="true" type="Numeric"/>
		<cfargument name="idOpeRevendeur" 		required="true" type="Numeric"  hint="Deprecated"  />			
  			<cfset retour = structNew()>			
						
			<cfthread action="run" name="comptesouscompte" mysoustete="#idsoutete#" myOperateur="#idoperateur#"> 
			     
				<cfset comptesouscompte.INFOSCOMPTESOUSCOMPTE = fournirCompteSousCompte(mysoustete, myOperateur)>
				
			</cfthread>

			<cfthread action="run" name="eligibilite" mysoustete2="#idsoutete#" myetateligible="#eligibiliteetat#"  myOperateur2="#idoperateur#"> 
			     
			    <cfset rsltQuery = checkEligibilite(mysoustete2,myOperateur2)>

			    <cfif rsltQuery.RecordCount GT 0>
				
					<cfset eligibilite.INFOSELIGIBILITE = rsltQuery>
				
				<cfelse>
				
					<cfset eligibilite.INFOSELIGIBILITE = ArrayNew(1)>
				
				</cfif>
			
			</cfthread>
 			
			<cfthread action="join" name="comptesouscompte,eligibilite" timeout="40000"/> 
				
			<cfset retour.INFOSELIGIBILITE 		= cfthread.eligibilite.INFOSELIGIBILITE>
			<cfset retour.INFOSCOMPTESOUSCOMPTE = cfthread.comptesouscompte.INFOSCOMPTESOUSCOMPTE>
			
		<cfreturn retour>
	</cffunction>
	
	<!--- RETOURNE L'OPERATEUR DE DERNIERE FACTURE DE LA LIGNE --->
	
	<cffunction name="getOperateurLastFactureLigneM111" access="remote" returntype="Query" output="false" hint="RETOURNE L'OPERATEUR DE DERNIERE FACTURE DE LA LIGNE">
		<cfargument name="idsoutete" 	required="true" type="String"/>

			<cfset idracine = session.perimetre.id_groupe>

  			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getOperateur">
				<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_idsoutete"  value="#idsoutete#">
				<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine"   value="#idracine#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
</cfcomponent>