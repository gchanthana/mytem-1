<cfcomponent displayname="fr.consotel.consoview.M16.v2.RessourceService" output="false">

	<!--- FOURNIT LA LISTE DES ABONNEMENTS/OPTIONS NORMAUX ET PAR DEFAUT SUIVANT LE PROFIL --->
	
	<cffunction name="fournirAbonnementsOptions" access="remote" returntype="Struct" output="false" hint="FOURNIT LA LISTE DES ABONNEMENTS/OPTIONS SUIVANT LE PROFIL">
		<cfargument name="idoperateur" 			required="true" type="numeric"/>
		<cfargument name="idprofilEquipement" 	required="true" type="numeric"/>
		<cfargument name="segment" 				required="true" type="numeric"/>
	
			<cfset ress 					= structnew()>
			<cfset ress.ABONNEMENTS 		= fournirListeAbonnementsOptions(idOperateur, idProfilEquipement, segment, 1)>
			<cfset ress.OPTIONS				= fournirListeAbonnementsOptions(idOperateur, idProfilEquipement, segment, 2)>
			<cfset ress.ABONNEMENTS_DEFAULT	= getassociatedabo_options(idProfilEquipement, 1, idOperateur)>
			<cfset ress.OPTIONS_DEFAULT		= getassociatedabo_options(idProfilEquipement, 0, idOperateur)>

		<cfreturn ress>
	</cffunction>
	
	<!--- FOURNIT LA LISTE DES ABONNEMENTS/OPTIONS SUIVANT LE PROFIL --->
	
	<cffunction name="fournirListeAbonnementsOptions" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTE DES ABONNEMENTS/OPTIONS SUIVANT LE PROFIL">
		<cfargument name="idoperateur" 			required="true" type="numeric"/>
		<cfargument name="idprofilEquipement" 	required="true" type="numeric"/>
		<cfargument name="segment" 				required="true" type="numeric"/>
		<cfargument name="bool_acces" 			required="true" type="numeric"/>
		
		<cfargument name="idracine" 			required="false" type="numeric"/>
		<cfargument name="code_langue" 			required="false" type="string"/>
	
		<cfif isdefined("idgestionnaire")>
			<cfset _idgestionnaire= idgestionnaire>
		<cfelse>
			<cfset _idgestionnaire= SESSION.USER.CLIENTACCESSID>
		</cfif>
		
		<cfif isdefined("idracine")>		
			<cfset _idracine = idracine>
			<cfelse>
			<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
		</cfif>
		
		<cfif isdefined("code_langue")>		
			<cfset _code_langue = code_langue>
			<cfelse>
			<cfset _code_langue = SESSION.USER.GLOBALIZATION>
		</cfif>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.liste_produits_profil_v4">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" 			value="#idOperateur#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" 			value="#session.user.clientaccessid#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 		value="#session.perimetre.id_groupe#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue"  		value="#session.user.globalization#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment"  			value="#segment#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_bool_acces"  			value="#bool_acces#">
			<cfprocresult name="p_retour">
		</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<!--- AJOUTE AUX FAVORIS CLIENT --->
	
	<cffunction name="ajouterProduitAMesFavoris" access="remote" returntype="numeric" output="false" hint="AJOUTE AUX FAVORIS CLIENT ">
		<cfargument name="idProduitCatalogue" 	required="true" type="numeric">
		<cfargument name="idgestionnaire" 		required="false" type="numeric"/>
		<cfargument name="idracine" 			required="false" type="numeric"/>
		
		<cfif isdefined("idgestionnaire")>
			<cfset _idgestionnaire= idgestionnaire>
		<cfelse>
			<cfset _idgestionnaire= SESSION.USER.CLIENTACCESSID>
		</cfif>
		
		<cfif isdefined("idracine")>		
			<cfset _idracine = idracine>
			<cfelse>
			<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
		</cfif>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.ADDPRODFAVORI">	
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#_idracine#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#_idgestionnaire#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idProduitCatalogue#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="out" variable="result">		  
		</cfstoredproc>		 		
		<cfreturn result/>
	</cffunction>	

	<!--- SUPPRIME DES FAVORIS CLIENT --->
	
	<cffunction name="supprimerProduitDeMesFavoris" access="remote" returntype="numeric" output="false" hint="SUPPRIME DES FAVORIS CLIENT">
		<cfargument name="idProduitCatalogue" type="numeric" required="true">
		<cfargument name="idgestionnaire" 		required="false" type="numeric"/>
		<cfargument name="idracine" 			required="false" type="numeric"/>
		
		<cfif isdefined("idgestionnaire")>
			<cfset _idgestionnaire= idgestionnaire>
		<cfelse>
			<cfset _idgestionnaire= SESSION.USER.CLIENTACCESSID>
		</cfif>
		
		<cfif isdefined("idracine")>		
			<cfset _idracine = idracine>
			<cfelse>
			<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
		</cfif>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.DELPRODFAVORI">	
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#_idRacine#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#_idGestionnaire#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idProduitCatalogue#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="out" variable="result">
		</cfstoredproc>		 		
		<cfreturn result/>
	</cffunction>

	<!--- RECUPERE L'ABONNEMENT OU LES OPTIONS PREDEFINIE DANS M24 --->
	
	<cffunction name="getassociatedabo_options" access="remote" returntype="Query" output="false" hint="RECUPERE L'ABONNEMENT OU LES OPTIONS PREDEFINIE DANS M24">
		<cfargument name="idtypecommande" 	type="Numeric" required="true">
		<cfargument name="isabo"	 		type="Numeric" required="true">
		<cfargument name="idoperateur"	 	type="Numeric" required="true">
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.getDefaultProducts_v3">	
				<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idtypecommande" 	value="#idtypecommande#">
				<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_isabo" 			value="#isabo#">
				<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_langid" 			value="#SESSION.USER.IDGLOBALIZATION#">
				<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idoperateur"		value="#idoperateur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- RECUPERE LE NOMBRE D'ABONNEMENT OU D'OPTIONS --->
	
	<cffunction name="getInfosNumberRessources" access="remote" returntype="Struct" output="false" hint="RECUPERE LE NOMBRE D'ABONNEMENT OU D'OPTIONS">
		<cfargument name="idoperator"	type="Numeric" required="true">
		<cfargument name="idprofileq" 	type="Numeric" required="true">
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getnbproduct">	
				<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idtype_commande" value="#idprofileq#">
				<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" 	value="#idoperator#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfloop query="p_retour">
				<cfset resN 			= structnew()>
				<cfset resN.NB_ABO 		= p_retour.NB_TOTAL_ABO>
				<cfset resN.NB_ABO_DEF 	= p_retour.NB_ABO_DEF>
				<cfset resN.NB_ABO_OBL 	= p_retour.NB_ABO_OBG>
	
				<cfset resN.NB_OPT 		= p_retour.NB_TOTAL_OPT>
				<cfset resN.NB_OPT_DEF 	= p_retour.NB_OPT_DEF>
				<cfset resN.NB_OPT_OBL	= p_retour.NB_OPT_OBG>
			</cfloop>

		<cfreturn resN/>
	</cffunction>

</cfcomponent>