<cfcomponent output="false" alias="fr.consotel.consoview.M16.DataCommande">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="cmd" type="CommandeVO" default="">
	<cfproperty name="articles" type="array" default="">

	<cfscript>
		//Initialize the CFC with the default properties values.
		this.cmd = "";
		this.articles = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="DataCommande">
		<cfreturn this>
	</cffunction>


</cfcomponent>