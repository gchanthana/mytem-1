<cfcomponent displayname="Equipements" output="false">

	<!--- FOURNIT LA LISTE DES EQUIPEMENTS SUIVANT LE PROFIL SELECTIONNE DU CATALOGUE CLIENT --->

	<cffunction name="fournirListeEquipements" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTE DES EQUIPEMENTS SUIVANT LE PROFIL SELECTIONNE">
		<cfargument name="idGestionnaire" 			required="true" type="numeric"/>
		<cfargument name="idPoolGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 		required="true" type="numeric"/>
		<cfargument name="idFournisseur" 			required="true" type="numeric"/>
		<cfargument name="clefRecherche" 			required="true" type="string"/>
		
			<cfset idCategorieEquipement = 0>		
			<cfset idTypeEquipement = 0>		
			<cfset typeFournis = 1>
			<cfset idGammeFournis = 0>
			<cfset niveau = 1>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.SearchCatalogueclient_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#idPoolGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#idCategorieEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#idTypeEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#idFournisseur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#idGammeFournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 					value="#clefRecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournis" 			value="#typeFournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_niveau" 					value="#niveau#" null="true">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LA CARTE SIM SUIVANT L'OPERATEUR SELECTIONNE --->

	<cffunction name="fournirCarteSim" access="remote" returntype="query" output="false" hint="FOURNIT LA CARTE SIM SUIVANT L'OPERATEUR SELECTIONNE">
		<cfargument name="idRevendeur" required="true"  type="numeric" displayname="ID REVENDEUR"/>
		<cfargument name="idOperateur" required="false" type="numeric" displayname="ID OPERATEUR"/>
	
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.getSimCard_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idOperateur#">
				<cfprocresult name="p_result">
			</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>

	<!--- FOURNIT LA LISTE DES ACCESSOIRES COMPATIBLES AVEC LE TERMIAL DU CATALOGUE CLIENT --->

	<cffunction name="getProductRelated" access="remote" returntype="query" output="false" hint="FOURNIT LA LISTE DES ACCESSOIRES COMPATIBLES AVEC LE TERMIAL">
		<cfargument name="idEquipement" 	required="true" type="numeric" 	default=""/>
		<cfargument name="sameRevendeur" 	required="true" type="numeric" 	default=""/>
		<cfargument name="chaine" 			required="true" type="string" 	default="null"/>
		<cfargument name="idlang" 			required="true" type="numeric" 	default="3"/>

			<cfif isDefined("code_langue")>
				<cfset _code_langue = code_langue>	
			<cfelse>
				<cfset _code_langue = SESSION.USER.GLOBALIZATION>	
			</cfif>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.get_product_related_client ">				
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idequipement" 	value="#idEquipement#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_chaine" 		value="#chaine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_same_rev" 		value="#sameRevendeur#">	
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_code_langue" 	value="#_code_langue#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine " 		value="#Session.PERIMETRE.ID_GROUPE#">	
				<cfprocresult name="p_retour">			
			</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIT LA LISTE DE TOUS LES ACCESSOIRES DU CATALOGUE CLIENT (QUERY DANS QUERY) --->

	<cffunction name="fournirListeAccessoiresCatalogueClient" returntype="query" access="remote" hint="FOURNIT LA LISTE DE TOUS LES ACCESSOIRES DU CATALOGUE CLIENT (QUERY DANS QUERY)">
		<cfargument name="idRevendeur" 	required="true" 	type="numeric"  displayname="revendeur" hint="le revendeur"/>
		<cfargument name="clef" 		required="true" 	type="string"   displayname="clef" 		hint="clef de recherche"/>
		<cfargument name="segment" 		required="true" 	type="string"   displayname="segment" 	hint="le filtre par segment"/>
		
			<cfset resultProc = fournirListeEquipementsCatalogueRevendeur(idRevendeur, clef, segment)>	
			
			<cfquery name="getListe" dbtype="query">
				select *
				from resultProc
				where IDTYPE_EQUIPEMENT = 2191
			</cfquery>

		<cfreturn getListe>
	</cffunction>

	<!--- FOURNIT LA LISTE DE TOUS LES EQUIPEMENTS (ACCESSOIRES ET AUTRES) DU CATALOGUE CLIENT --->
	
	<cffunction name="fournirListeEquipementsCatalogueRevendeur" returntype="query" access="remote" hint="FOURNIT LA LISTE DE TOUS LES EQUIPEMENTS (ACCESSOIRES ET AUTRES) DU CATALOGUE CLIENT">
		<cfargument name="idRevendeur" 	required="true" 	type="numeric"  displayname="revendeur" hint="le revendeur"/>
		<cfargument name="clef" 		required="true" 	type="string"   displayname="clef" 		hint="clef de recherche"/>
		<cfargument name="segment" 		required="true" 	type="string"   displayname="segment" 	hint="le filtre par segment"/>

			<cfset p_idcategorie_equipement = 0>
			<cfset p_idtype_equipement = 0>
			<cfset p_idfournisseur = idRevendeur>
			<cfset p_idgamme_fournis = 0>
			<cfset p_chaine = clef>
			<cfset p_idgroupe_client = SESSION.PERIMETRE.ID_GROUPE>
			<cfset p_type_fournisseur = 0>
					
			<cfif p_type_fournisseur eq 0>
				<cfset flag="true">
			<cfelse>
				<cfset flag="false">
			</cfif>
					
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_M16.SearchCatalogueRevendeur_V3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcategorie_equipement#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idtype_equipement#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfournisseur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgamme_fournis#">
				<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_chaine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_type_fournisseur#" null="#flag#">
				<cfprocresult name="p_result">
			</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
</cfcomponent>