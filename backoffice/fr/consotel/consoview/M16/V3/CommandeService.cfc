<cfcomponent displayname="fr.consotel.consoview.M16.V3.CommandeService" hint="Gestion des commandes" output="false">
	
	<!--- FOURNI TOUTES LES INFOS LORS DU CHARGEMENT DU MODULE --->
	
	<cffunction name="getLoadInfosThread" access="remote" returntype="Struct" output="false" hint="FOURNI TOUTES LES INFOS LORS DU CHARGEMENT DU MODULE">
		<cfargument name="idpooldefault"  	required="true" type="Numeric"/>
		<cfargument name="idprofildefault"  required="true" type="Numeric"/>
		
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset idlang 			= session.user.idglobalization>
			<cfset codelangue 		= session.user.globalization>

			<cfset remoteObject = createObject('component',"fr.consotel.consoview.M16.V3.PoolService")>
			
			<cfset getpools = remoteObject.fournirListePoolsDuGestionnaire(idgestionnaire, idracine)>
		
			<cfset remoteObject = createObject('component',"fr.consotel.consoview.M16.V3.RevendeurService")>
				
			<cfset getrevendeursautorises = remoteObject.fournirNumberRevendeursAutorises(idracine, idgestionnaire)>
							
			<cfset getchamps = getChamps()>
		
			<cfset remoteObject = createObject('component',"fr.consotel.consoview.M16.V3.RevendeurService")>
				
			<cfset getuserprofil = remoteObject.getUserProfil()>

			<cfset idprofiluser = getuserprofil>
			<cfset idprofil = -1>
			<cfset idpool 	= -1>

			<cfif getpools.RecordCount GT 0>
			
				<cfif getpools.RecordCount EQ 1>
			
					<cfset idpool 	= getpools["IDPOOL"][getpools.RecordCount]>
					<cfset idprofil = getpools["IDPROFIL"][getpools.RecordCount]>	
				
				<cfelse>
				
					<cfif idpooldefault GT 0>
						
						<cfset idpool 	= idpooldefault>
						<cfset idprofil = idprofildefault>
						
					<cfelse>
						
						<cfset myPoolsInfos = getMyProfilPool(getpools, idprofiluser)>
						
						<cfset idpool 	= myPoolsInfos.IDPOOL>
						<cfset idprofil = myPoolsInfos.IDPROFIL>	
						
					</cfif>
				
				</cfif>
				
			</cfif>

			<cfset filterCommande = getFiltersCommandeAndActions(idpool, idprofil)>

			<cfset infosLoaded 					= structNew()>
			<cfset infosLoaded.POOLS 			= getpools>
			<cfset infosLoaded.NBRREVENDEURS 	= getrevendeursautorises>
			<cfset infosLoaded.CHAMPS 			= getchamps>
			<cfset infosLoaded.IDUSERPROFIL 	= getuserprofil>
			<cfset infosLoaded.FILTERSCMD 		= filterCommande>
			<cfset infosLoaded.IDUSER		 	= idgestionnaire>
			<cfset infosLoaded.IDRACINE 		= idracine>

		<cfreturn infosLoaded>
	</cffunction>
	
	<!--- FOURNI LA LISTE DES COMMANDES POUR LE PAGINATE DATAGRID --->
	
	<cffunction name="fournirInventaireCommandePaginate" access="remote" returntype="any" output="false" hint="FOURNI LA LISTE DES COMMANDES POUR LE PAGINATE DATAGRID">
		<cfargument name="segment" 		required="true"  type="Numeric" default="-1"/>
		<cfargument name="clef" 		required="true"  type="String"  default=""/>
		<cfargument name="idpool" 		required="true"  type="Numeric" default="-1"/>
		<cfargument name="idstatut" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="idetat" 		required="true"  type="Numeric" default="-1"/>
		<cfargument name="idoperateur" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="idcompte" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="idsouscompte" required="true"  type="Numeric" default="-1"/>
		<cfargument name="idusercreate" required="true"  type="Numeric" default="-1"/>
		<cfargument name="startindex" 	required="true"  type="Numeric" default="-1"/>
		<cfargument name="numberrecord" required="true"  type="Numeric" default="-1"/>
		<cfargument name="idcritere" 	required="true"  type="Numeric" default="0"/>
		<cfargument name="txtFilter" 	required="true"  type="String"  default=""/>

			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset codelangue 		= session.user.globalization>
					
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.fournirinventaireoperation_v5">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#idgestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 				value="#segment#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 				value="#clef#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 			value="#codelangue#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 				value="#idpool#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idstatut" 			value="#idstatut#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idetat" 				value="#idetat#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_operateur" 			value="#idoperateur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" 			value="#idcompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte" 		value="#idsouscompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_user_create" 			value="#idusercreate#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_index_debut" 			value="#startindex#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_number_of_records" 	value="#numberrecord#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcritere" 			value="#idcritere#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_critere" 				value="#txtFilter#">
				<cfprocresult name="p_retour">			
			</cfstoredproc>
			
			<cfset idspoolsarray = ArrayNew(1)>
			
 			<cfloop index="idx" from="1" to="#p_retour.recordcount#">

				<cfset ArrayAppend(idspoolsarray, #idpool#)>
				
			</cfloop>
			
			<cfif p_retour.recordcount GT 0>
			
				<cfset QueryAddColumn(p_retour, "IDPOOL", "CF_SQL_INTEGER", idspoolsarray)/>
			
			</cfif>

		<cfreturn p_retour>
	</cffunction>
	
	<!--- FOURNI LES CHAMPS DE REFERENCE CLIENT --->
	
	<cffunction name="getChamps" returntype="Any">
		
			<cfset idracine = session.perimetre.id_groupe>
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m21.getchamps">
				<cfprocparam  type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#"/>
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- RECUPÈRE LE PROFIL DU POOL (REVENDEUR OU CLIENT) --->
	
	<cffunction name="getMyProfilPool" access="remote" returntype="Struct" output="false" hint="RECUPÈRE LE PROFIL DU POOL (REVENDEUR OU CLIENT)">
		<cfargument name="pools"  			required="true" type="Query"/>
		<cfargument name="idprofiluser"  	required="true" type="Numeric"/>
	
			<cfset infospools = structNew()>
	
			<cfset idpool 	= 0>
			<cfset idprofil = 0>
			<cfset cptr		= 0>
	
			<cfif idprofiluser EQ 2>
			
				<cfloop index="idx" from="1" to="#pools.RecordCount#">
					
					<cfif pools['IDREVENDEUR'][idx] GT 0>
					
						<cfif cptr EQ 0>
					
							<cfset idpool 	= pools['IDPOOL'][idx]>
							<cfset idprofil = pools['IDPROFIL'][idx]>
							
							<cfset cptr = cptr + 1>
					
						</cfif>
					
					</cfif>
					
				</cfloop>
			
			</cfif>
			
			<cfset infospools.IDPOOL 	= idpool>
			<cfset infospools.IDPROFIL 	= idprofil>
	
		<cfreturn infospools>
	</cffunction>
	
	<!--- RECUPÈRE LES FILTRES ET LES ACTIONS AUXQUELLES ONT A LE DROIT --->
	
	<cffunction name="getFiltersCommandeAndActions" access="remote" returntype="Struct" output="false" hint="RECUPÈRE LES FILTRES ET LES ACTIONS AUXQUELLES ONT A LE DROIT">
		<cfargument name="idpool"  	required="true" type="Numeric"/>
		<cfargument name="idprofil" required="true" type="Numeric"/>
		
			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset globalization 	= session.user.globalization>
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idlang 			= session.user.idglobalization>

			<cfset infosCommandes = structNew()>

			<cfthread action="run" name="filterEtats" myidlang="#idlang#" mypool="#idpool#" myracine="#idracine#">
						
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getlisteetat_v2">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_langid" 	value="#myidlang#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
				<cfset filterEtats.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterGestionnaires" mypool="#idpool#" myracine="#idracine#">
						
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getGestOfPool">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
				<cfset filterGestionnaires.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterOperateurs" myracine="#idracine#">
						
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getOperateurOfRacine">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
				<cfset filterOperateurs.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterComptes" mypool="#idpool#" myracine="#idracine#">
							
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getCompteOfPool">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
				
				<cfset filterComptes.RESULT = p_retour>
			
			</cfthread>
			
			<cfthread action="run" name="filterSousComptes" mypool="#idpool#" myracine="#idracine#">
						
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getSsCompteOfPool">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#myracine#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
				<cfset filterSousComptes.RESULT = p_retour>
			
			</cfthread>
						
			<cfthread action="run" name="actionsProfil" myprofil="#idprofil#">
			
				<cfset object = createObject("component","fr.consotel.consoview.M16.V3.PoolService")>
				
				<cfset p_retour = object.fournirListeActionProfilCommande(myprofil)>
			
				<cfset actionsProfil.RESULT = p_retour>
			
			</cfthread>

			<cfthread action="run" name="actions" mypool="#idpool#" myIdgest="#idgestionnaire#" myCodeLangue="#globalization#">
			 
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.listeallactionspossibles">			
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#myIdgest#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 			value="#idpool#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 		value="#myCodeLangue#">	
					<cfprocresult name="p_retour">
				</cfstoredproc>

				<cfset myActions = arraynew(1)>
			
				<cfloop query="p_retour">
					
					<cfif p_retour.SEGMENT EQ 2>
					
						<!--- RECUPERATION SEGEMENT = 2 --->
						<cfset actionsWorkflow = structNew()>
						
						<cfset actionsWorkflow.IDINV_ETAT			= p_retour.IDINV_ETAT>
						<cfset actionsWorkflow.MESSAGE 		 		= p_retour.MESSAGE>
						<cfset actionsWorkflow.CODE_ACTION 	 		= p_retour.CODE_ACTION>
						<cfset actionsWorkflow.DEST  				= p_retour.DEST>
						<cfset actionsWorkflow.IDETAT_ENGENDRE 		= p_retour.IDETAT_ENGENDRE>
						<cfset actionsWorkflow.EXP 					= p_retour.EXP>
						<cfset actionsWorkflow.COMMENTAIRE_ACTION 	= p_retour.COMMENTAIRE_ACTION>
						<cfset actionsWorkflow.SEGMENT 				= p_retour.SEGMENT>
						<cfset actionsWorkflow.LIBELLE_ACTION 		= p_retour.LIBELLE_ACTION>
						<cfset actionsWorkflow.IDINV_ACTIONS 		= p_retour.IDINV_ACTIONS>

						<cfset arrayappend(myActions, actionsWorkflow)>
						
						<!--- RECUPERATION SEGEMENT = 3 --->
						<cfset actionsWorkflow = structNew()>
						
						<cfset actionsWorkflow.IDINV_ETAT			= p_retour.IDINV_ETAT>
						<cfset actionsWorkflow.MESSAGE 		 		= p_retour.MESSAGE>
						<cfset actionsWorkflow.CODE_ACTION 	 		= p_retour.CODE_ACTION>
						<cfset actionsWorkflow.DEST  				= p_retour.DEST>
						<cfset actionsWorkflow.IDETAT_ENGENDRE 		= p_retour.IDETAT_ENGENDRE>
						<cfset actionsWorkflow.EXP 					= p_retour.EXP>
						<cfset actionsWorkflow.COMMENTAIRE_ACTION 	= p_retour.COMMENTAIRE_ACTION>
						<cfset actionsWorkflow.SEGMENT 				= 3>
						<cfset actionsWorkflow.LIBELLE_ACTION 		= p_retour.LIBELLE_ACTION>
						<cfset actionsWorkflow.IDINV_ACTIONS 		= p_retour.IDINV_ACTIONS>

						<cfset arrayappend(myActions, actionsWorkflow)>

					<cfelse>

						<cfset actionsWorkflow = structNew()>
						
						<cfset actionsWorkflow.IDINV_ETAT			= p_retour.IDINV_ETAT>
						<cfset actionsWorkflow.MESSAGE 		 		= p_retour.MESSAGE>
						<cfset actionsWorkflow.CODE_ACTION 	 		= p_retour.CODE_ACTION>
						<cfset actionsWorkflow.DEST  				= p_retour.DEST>
						<cfset actionsWorkflow.IDETAT_ENGENDRE 		= p_retour.IDETAT_ENGENDRE>
						<cfset actionsWorkflow.EXP 					= p_retour.EXP>
						<cfset actionsWorkflow.COMMENTAIRE_ACTION 	= p_retour.COMMENTAIRE_ACTION>
						<cfset actionsWorkflow.SEGMENT 				= p_retour.SEGMENT>
						<cfset actionsWorkflow.LIBELLE_ACTION 		= p_retour.LIBELLE_ACTION>
						<cfset actionsWorkflow.IDINV_ACTIONS 		= p_retour.IDINV_ACTIONS>

						<cfset arrayappend(myActions, actionsWorkflow)>

					</cfif>
					
				</cfloop>
			
				 <!--- Set up the query for testing. --->
				<cfset qRetour = QueryNew( "IDINV_ETAT, MESSAGE, CODE_ACTION, DEST, IDETAT_ENGENDRE, EXP, COMMENTAIRE_ACTION, SEGMENT, LIBELLE_ACTION, IDINV_ACTIONS")>
			
				<cfloop index="idx" from="1" to="#ArrayLen(myActions)#">
				
					<cfset QueryAddRow(qRetour)>
				    <cfset qRetour["IDINV_ETAT"][ qRetour.RecordCount ] 		= #myActions[idx].IDINV_ETAT#/>
				    <cfset qRetour["MESSAGE"][ qRetour.RecordCount ] 			= #myActions[idx].MESSAGE#/>
				    <cfset qRetour["CODE_ACTION"][ qRetour.RecordCount ] 		= #myActions[idx].CODE_ACTION#/>
				    <cfset qRetour["DEST"][ qRetour.RecordCount ] 				= #myActions[idx].DEST#/>
				    <cfset qRetour["IDETAT_ENGENDRE"][ qRetour.RecordCount ] 	= #myActions[idx].IDETAT_ENGENDRE#/>
				    <cfset qRetour["EXP"][ qRetour.RecordCount ] 				= #myActions[idx].EXP#/>
				    <cfset qRetour["COMMENTAIRE_ACTION"][ qRetour.RecordCount ] = #myActions[idx].COMMENTAIRE_ACTION#/>
				    <cfset qRetour["SEGMENT"][ qRetour.RecordCount ] 			= #myActions[idx].SEGMENT#/>
				    <cfset qRetour["LIBELLE_ACTION"][ qRetour.RecordCount ] 	= #myActions[idx].LIBELLE_ACTION#/>
				    <cfset qRetour["IDINV_ACTIONS"][ qRetour.RecordCount ] 		= #myActions[idx].IDINV_ACTIONS#/>
				
				</cfloop>
			
				<cfset actions.RESULT = qRetour>
			
			</cfthread>
						
			<cfthread action="join" name="filterEtats,filterGestionnaires,filterOperateurs,filterComptes,filterSousComptes,actions,actionsProfil"/>

			<cfset infosCommandes 	 				 = structNew()>
			<cfset infosCommandes.LISTEETATS 		 = cfthread.filterEtats.RESULT>
			<cfset infosCommandes.LISTEGESTIONNAIRES = cfthread.filterGestionnaires.RESULT>
			<cfset infosCommandes.LISTEOPERATEURS 	 = cfthread.filterOperateurs.RESULT>
			<cfset infosCommandes.LISTECOMPTES 		 = cfthread.filterComptes.RESULT>
			<cfset infosCommandes.LISTESOUSCOMPTE 	 = cfthread.filterSousComptes.RESULT>
 			<cfset infosCommandes.LISTEACTIONS 		 = cfthread.actions.RESULT>
			<cfset infosCommandes.LISTEACTIONSPROFIL = cfthread.actionsProfil.RESULT>

		<cfreturn infosCommandes>
	</cffunction>
	
	<!--- GÉNÈRE LE NUMÉRO DE COMMANDE --->
	
	<cffunction name="getNumeroCommande" access="public" returntype="string" output="false" hint="Génère le numéro de commande">
		
			<cfset idracine = session.perimetre.id_groupe>	
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getcommandenumber">
				
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">									
				<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR" variable="p_retour">		
			
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	<!--- METS A JOUR LES ARTICLES LIVRES - COMMANDE PARTIELLE --->
	
	<cffunction name="SavePartialDelivery" access="public" returntype="any" output="false" hint="METS A JOUR LES ARTICLES LIVRES - COMMANDE PARTIELLE">
		<cfargument name="idCommande" 	required="true" type="numeric"/>
		<cfargument name="idArticles" 	required="true" type="String"/>
			
			<cfset var rsltCopy = -10>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.SavePartialDelivery">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 		value="#idcommande#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_iduser" 			value="#Session.USER.CLIENTACCESSID#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 		variable="p_list_article_deliv" value="#idArticles#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>

			<cfif p_retour GT 0>
				<cfset rsltCopy = copyAttachementCurrentCommande(idCommande, p_retour)>
			</cfif>
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- LORS D'UNE LIVRAISON PARTIELLE COPIE, CRÉÉ LES PIÈCES JOINTES DE LA COMMANDE COURANTE DANS LE NOUVEAU RÉPERTOIRE --->
	
	<cffunction name="copyAttachementCurrentCommande" access="remote" output="false" returntype="any" hint="LORS D'UNE LIVRAISON PARTIELLE COPIE, CRÉÉ LES PIÈCES JOINTES DE LA COMMANDE COURANTE DANS LE NOUVEAU RÉPERTOIRE">
		<cfargument name="idcommande" 		required="true" type="Numeric"/>
		<cfargument name="newidcommande" 	required="true" type="Numeric"/>
			
			<cfset var rsltCopy 	= -10>
			<cfset var currentUUID 	= "">
			<cfset var files 		= ArrayNew(1)>
			<cfset var co 			= createObject('component',"fr.consotel.consoview.M16.V3.AttachementService")>
			
			<cfset var dirCible  = co.initUUID()>		
			<cfset var rsltFiles = co.fournirAttachements(idcommande)>
			
			<cfloop query="rsltFiles">
				
				<cfset newFiles				= structNew()>
				<cfset newFiles.FILE_NAME	= rsltFiles.FILE_NAME>
				<cfset newFiles.FILE_SIZE	= rsltFiles.FILE_SIZE>
				<cfset newFiles.FORMAT		= rsltFiles.FORMAT>
				<cfset newFiles.JOIN_MAIL	= rsltFiles.JOIN_MAIL>
				
				<cfset ArrayAppend(files, newFiles)>
				<cfset currentUUID =  #rsltFiles.PATH#>
			</cfloop>
			
			<cfset rsltCopy = co.copyAttachement(currentUUID, dirCible)>
			<cfif rsltCopy EQ 1>
				<cfset rsltCopy = co.queryAppendAttachementsCurrentCommande(newidcommande, files, dirCible)>
			</cfif>
			
		<cfreturn rsltcopy> 
	</cffunction>
	
	<!--- LISTE DES EQUIPEMENTS POUR LA MISE A JOUR DES REFERENCES --->
	
	<cffunction name="fournirReferenceInfos" access="public" returntype="any" output="false" hint="LISTE LES ARTICLES DE LA COMMANDE">
		<cfargument name="idcommande" 	required="true" type="numeric"/>
		<cfargument name="idpool" 		required="true" type="numeric"/>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.getrefinfo">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idcommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idpool#">			
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfquery name="getData" dbtype="query">
				SELECT *
				FROM p_retour
				ORDER BY IDSOUS_TETE
			</cfquery>
			
		<cfreturn getData>
	</cffunction>

	<!--- FOURNI LES DETAILS D'UNE COMMMANDE --->

	<cffunction name="fournirDetailOperation" access="remote" returntype="Query" output="false" hint="RECUPÈRE LES INFORMATION CONCERNANT LA COMMANDE SÉLECTIONNÉE">
		<cfargument name="idcommande" 		required="true" type="Numeric"/>
					
			<cfset idgestionnaire = session.user.clientaccessid>
					
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.fournirdetailoperation">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 	value="#idcommande#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idgestionnaire" value="#idgestionnaire#"/>
				<cfprocresult name="p_retour">			
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	<!--- LISTE LES ARTICLES DE LA COMMANDE --->
	
	<cffunction name="fournirArticlesCommande" access="public" returntype="any" output="false" hint="LISTE LES ARTICLES DE LA COMMANDE">
		<cfargument name="idcommande" required="true" type="Numeric"/>
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.fournirarticlesoperation">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idcommande#">			
				<cfprocresult name="articlexml">
			</cfstoredproc>
			
		<cfreturn articlexml>
	</cffunction>
	
	<!--- FOURNI : DÉTAILSCOMMANDE, TRANSPORTEURS, HISTORIQUE,LIBELLECOMPTESOUSCOMPTE,L'ADRESSE DU REVENDEUR --->
	
	<cffunction name="getInfosCommande" access="remote" output="false" returntype="any" hint="LIE LA COMMANDE AUX PIÈCES JOINTES DÉPOSÉES">
		<cfargument name="idcommande" 	required="true" type="Numeric"/>
		<cfargument name="idoperateur" 	required="true" type="Numeric"/>
		<cfargument name="idrevendeur" 	required="true" type="Numeric"/>
		<cfargument name="idsegment" 	required="true" type="Numeric"/>
		
			<cfset infosCommande = structnew()>

			<cfthread action="run" name="details" myidcommande="#idcommande#">
			
				<cfset details.RESULT = fournirDetailOperation(myidcommande)>
			
			</cfthread>
			
			<cfthread action="run" name="historique" myidcommande="#idcommande#">

				<cfset globalization = session.user.globalization>

				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.historiqueetatsoperation" >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcommande" value="#myidcommande#">	
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_codelangue" value="#globalization#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
				
				<cfset historique.RESULT = p_retour>
			
			</cfthread>

			<cfthread action="run" name="libellecomptesouscompte" myoperateur="#idoperateur#">

				<cfset libellecomptesouscompte.RESULT = createObject("component","fr.consotel.consoview.M16.V3.CompteService").fournirLibelleCompteOperateur(myoperateur)>
			
			</cfthread>
			
			<cfthread action="run" name="revendeuradresse" myrevendeur="#idrevendeur#" mysegment="#idsegment#">
			
				<cfif mysegment EQ 1>
				
					<cfset mysegment = 2>
				
				<cfelse>
				
					<cfset mysegment = 1>
				
				</cfif>
			
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.get_catalogue_revendeur_v2">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#session.perimetre.id_groupe#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#session.user.clientaccessid#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#mysegment#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#session.user.globalization#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
											
				<cfquery name="getAdressRevendeur" dbtype="query">
					select *
					FROM p_retour
					WHERE IDREVENDEUR = #myrevendeur#
				</cfquery>	

				<cfset revendeuradresse.RESULT = getAdressRevendeur>
			
			</cfthread>

			<cfthread action="run" name="transporteurs">
							
				<cfset transporteurs.RESULT = createObject("component","fr.consotel.consoview.M16.V3.TransporteurService").fournirListeTransporteurs()>
			
			</cfthread>

			<cfthread action="join" name="details, historique, libellecomptesouscompte, revendeuradresse, transporteurs" timeout="40000"/>

			<cfset infosCommande.DETAILS				 = cfthread.details.RESULT>
			<cfset infosCommande.HISTORIQUE 			 = cfthread.historique.RESULT>
			<cfset infosCommande.LIBELLECOMPTESOUSCOMPTE = cfthread.libellecomptesouscompte.RESULT>
			<cfset infosCommande.REVENDEURADRESSE		 = cfthread.revendeuradresse.RESULT>
			<cfset infosCommande.TRANSPORTEURS 			 = cfthread.transporteurs.RESULT>
			
		<cfreturn infosCommande> 
	</cffunction>

	<!--- MISE À JOUR DES INFORMATIONS D'UNE COMMANDE --->
	
	<cffunction name="majCommandeInfos" displayname="majCommandeInfos" access="remote" output="false" returntype="numeric" hint="MISE À JOUR DES INFORMATIONS D'UNE COMMANDE">
		<cfargument name="idoperation" 			required="true" type="numeric"/>
		<cfargument name="idgestionnaire" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idpoolgestionnaire" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idtransporteur" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idsitedelivraison" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idcontactrevendeur" 	required="true" type="numeric" 	default="null" 	/>
		<cfargument name="numerooperation" 		required="true" type="string" 	default="" 		/>
		<cfargument name="numerotracking" 		required="true" type="string" 	default="null" 	/>
		<cfargument name="libelleoperation" 	required="true" type="string" 	default="null" 	/>
		<cfargument name="refclient"			required="true" type="string" 	default="" 		/>
		<cfargument name="refclient2" 			required="true" type="string" 	default="" 		/>
		<cfargument name="refrevendeur" 		required="true" type="string" 	default="" 		/>
		<cfargument name="datelivraison" 		required="true" type="string" 	default="" 		/>
		<cfargument name="commentaires"			required="true" type="string" 	default="" 		/>
		<cfargument name="montant"				required="true" type="string" 	default="" 		/>
		<cfargument name="libelleto" 			required="true" type="string" 	default="null" 	/>
		<cfargument name="idcompte" 			required="true" type="numeric" 	default="null" 	/>
		<cfargument name="idsouscompte" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idsitedefacturation"	required="true" type="numeric" 	default="" 		/>
		<cfargument name="livraisonrevendeur"	required="true" type="numeric" 	default="" 		/>
		
 			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.MAJCOMMANDE_V4">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idoperation" 			value="#idoperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idgestionnaire" 		value="#idgestionnaire#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idtransporteur" 		value="#idtransporteur#" 		null="#iif((idtransporteur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idsite_livraison" 		value="#idsitedelivraison#" 	null="#iif((idsitedelivraison eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idcontact_revendeur" 	value="#idcontactrevendeur#" 	null="#iif((idcontactrevendeur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_operation" 		value="#numerooperation#">		
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_tracking" 		value="#numerotracking#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_operation" 		value="#libelleoperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_client" 			value="#refclient#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_revendeur" 			value="#refrevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 				variable="p_date_effet_prevue" 		value="#datelivraison#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_commentaires" 			value="#commentaires#">
				<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" null="false" 	variable="p_montant" 				value="#montant#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_attention"		value="#libelleto#">
				<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" null="false" 	variable="p_ref_client_2"			value="#refclient2#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" 	variable="p_idcompte"				value="#idcompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" 	variable="p_idsous_compte"			value="#idsouscompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 		 		variable="p_idsite_facturation"		value="#idsitedefacturation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false"  variable="p_liv_disrtib"   			value="#livraisonrevendeur#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 				variable="p_idoperation">
			</cfstoredproc>
		
		<cfreturn p_idoperation> 
	</cffunction>
	
	<!--- MISE À JOUR DES INFORMATIONS D'UNE COMMANDE ET DE SES ARTICLES --->
	
	<cffunction name="majCommandeAndArticles" displayname="majCommandeAndArticles" access="remote" output="false" returntype="numeric" hint="MISE À JOUR DES INFORMATIONS D'UNE COMMANDE">
		<cfargument name="idoperation" 			required="true" type="numeric"/>
		<cfargument name="idgestionnaire" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idpoolgestionnaire" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idtransporteur" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idsitedelivraison" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idcontactrevendeur" 	required="true" type="numeric" 	default="null" 	/>
		<cfargument name="numerooperation" 		required="true" type="string" 	default="" 		/>
		<cfargument name="numerotracking" 		required="true" type="string" 	default="null" 	/>
		<cfargument name="libelleoperation" 	required="true" type="string" 	default="null" 	/>
		<cfargument name="refclient"			required="true" type="string" 	default="" 		/>
		<cfargument name="refclient2" 			required="true" type="string" 	default="" 		/>
		<cfargument name="refrevendeur" 		required="true" type="string" 	default="" 		/>
		<cfargument name="datelivraison" 		required="true" type="string" 	default="" 		/>
		<cfargument name="commentaires"			required="true" type="string" 	default="" 		/>
		<cfargument name="montant"				required="true" type="string" 	default="" 		/>
		<cfargument name="libelleto" 			required="true" type="string" 	default="null" 	/>
		<cfargument name="idcompte" 			required="true" type="numeric" 	default="null" 	/>
		<cfargument name="idsouscompte" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idsitedefacturation"	required="true" type="numeric" 	default="" 		/>
		<cfargument name="articlesinarray" 		required="true" type="Array"  	default=""		/>
		<cfargument name="uuidreference"		required="true" type="String" 	default="" 		/>
		<cfargument name="filesinarray" 		required="true" type="Array"  	default=""		/>
 		<cfargument name="idtypecommande" 		required="true" type="Numeric" 	default="" 		/>
		<cfargument name="idoperateur" 			required="true" type="Numeric" 	default="" 		/>
		<cfargument name="boolmail" 			required="true" type="Numeric" 	default="0" 	/>
		<cfargument name="action" 				required="true" type="Struct" 					/>
		<cfargument name="modification"			required="true" type="Struct" 					/>
		<cfargument name="mail" 				required="true" type="fr.consotel.consoview.M16.vo.MailVo"/>
		<cfargument name="libmodification"		required="true" type="String" 	default="" 		/>
		
 			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.MAJCOMMANDE_V4">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idoperation" 			value="#idoperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idgestionnaire" 		value="#idgestionnaire#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idtransporteur" 		value="#idtransporteur#" 		null="#iif((idtransporteur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idsite_livraison" 		value="#idsitedelivraison#" 	null="#iif((idsitedelivraison eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idcontact_revendeur" 	value="#idcontactrevendeur#" 	null="#iif((idcontactrevendeur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_operation" 		value="#numerooperation#">		
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_tracking" 		value="#numerotracking#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_operation" 		value="#libelleoperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_client" 			value="#refclient#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_revendeur" 			value="#refrevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 				variable="p_date_effet_prevue" 		value="#datelivraison#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_commentaires" 			value="#commentaires#">
				<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" null="false" 	variable="p_montant" 				value="#montant#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_attention"		value="#libelleto#">
				<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" null="false" 	variable="p_ref_client_2"			value="#refclient2#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" 	variable="p_idcompte"				value="#idcompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" 	variable="p_idsous_compte"			value="#idsouscompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 		 		variable="p_idsite_facturation"		value="#idsitedefacturation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false"  variable="p_liv_disrtib"   			value="0">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 				variable="p_idoperation">
			</cfstoredproc>
				
			<cfif p_idoperation GT 0>

	            <cfthread action="run" name="articles3" mycommande3="#idoperation#" mypool3="#idpoolgestionnaire#" myarticles3="#articlesinarray#" myfiles3="#filesinarray#"
	           											mytypecommande3="#idtypecommande#" myoperator3="#idoperateur#" istosend3="#boolmail#" 
	           												myaction3="#action#" mynumcommande3="#numerooperation#"  myuuidreference3="#uuidreference#"
	           													mymail3="#mail#" mymodification3="#modification#" libmodification2="#libmodification#"> 
					
					<cfset mailObject 	= createObject('component',"fr.consotel.consoview.M16.V3.MailService")>
					<cfset workflow 	= createObject('component',"fr.consotel.consoview.M16.V3.WorkflowService")>
	
					<cfset rslt = -10>
	
					<cftry>
	
						<cfset rsltwlow = workflow.faireActionWorkFlow(	mymodification3.IDACTION,
																		mymodification3.DATE_HOURS,
																		mymodification3.COMMENTAIRE_ACTION,
																		mycommande3,
																		0,
																		-1)>

						<cfif arraylen(myarticles3) GT 0>
							
							<cfset rslt1 = enregistrerArticles(#mycommande3#, #mypool3#, #myarticles3#)>

						</cfif>

						<!--- ENVOI DU MAIL DE MODIFICATION --->
						<cfset rslt3 = mailObject.sendMailModificationsCommande(#mycommande3#,
																				#mynumcommande3#,
																				#mymodification3.DATE_ACTION#,
																				#session.user.globalization#,
																				libmodification2)>
						
						<cfif myaction3.IDACTION GT 0>
							
	 						<cfset rslt2 = mailObject.sendMailInfosNewCommande(#mymail3#, #mycommande3#, #mytypecommande3#, #myoperator3#, #istosend3#, 
																					#myaction3#, #mynumcommande3#, #myuuidreference3#, 1, #myfiles3#)>
	 					
						</cfif>
					
					<cfcatch>
						<cfmail server="mail.consotel.fr" port="26" from="fromM16@consotel.fr" to="dev@consotel.fr" 
								subject="Erreur mise à jour commande" type="html">
											 							
								#CFCATCH.Message#<br/><br/><br/><br/>
								
						</cfmail>
						
					</cfcatch>					
					</cftry>
					
	            </cfthread>

			<cfelse>
			
				<cfset p_idoperation = 0>
				
			</cfif>
		
		<cfreturn p_idoperation> 
	</cffunction>
	
	<!--- LISTE LES ARTICLES DE LA COMMANDE GROUPES PAR NUMERO DE CONFIGURATION --->
	
	<cffunction name="getArticleSummary" access="public" returntype="Any" output="false" hint="LISTE LES ARTICLES DE LA COMMANDE GROUPES PAR NUMERO DE CONFIGURATION">
		<cfargument name="idCommande" required="true" type="Numeric"/>
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getarticlesummary">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idCommande#">			
				<cfprocresult name="p_retour" 	resultset="1">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_nb_ligne">
			</cfstoredproc>

			<cfset articlesInfos = structnew()>
			<cfset articlesInfos.ARTICLES = p_retour>
			<cfset articlesInfos.NBLIGNE  = p_nb_ligne>

		<cfreturn articlesInfos>
	</cffunction>
	
	<!--- ENREGISTREMENT D'UNE COMMANDE ET DE CES ARTICLES. COMMANDE , MODIFICATION. --->
	
	<cffunction name="recordCommande" displayname="recordCommande" access="remote" output="false" returntype="Numeric" hint="enregistrement des informations d'une commande">
		<cfargument name="mail" 			 	required="true" type="fr.consotel.consoview.M16.vo.MailVo"/>
		<cfargument name="idgestionnaire" 		required="true" type="Numeric"/>
		<cfargument name="idpoolgestionnaire" 	required="true" type="Numeric"/>
		<cfargument name="idcompte" 			required="true" type="Numeric"/>
		<cfargument name="idsouscompte" 		required="true" type="Numeric"/>
		<cfargument name="idoperateur" 			required="true" type="Numeric"/>
		<cfargument name="idrevendeur" 			required="true" type="Numeric"/>
		<cfargument name="idcontact" 			required="true" type="Numeric"/>
		<cfargument name="idtransporteur" 		required="true" type="Numeric"/>
		<cfargument name="idsitedelivraison" 	required="true" type="Numeric"/>
		<cfargument name="numerotracking" 		required="true" type="String"/>
		<cfargument name="libellecommande"		required="true" type="String"/>
		<cfargument name="refclient" 			required="true" type="String"/>
		<cfargument name="refclient2" 			required="true" type="String"/>
		<cfargument name="refrevendeur" 		required="true" type="String"/>
		<cfargument name="dateoperation"		required="true" type="String"/>
		<cfargument name="datelivraison"		required="true" type="String"/>
		<cfargument name="commentaires" 		required="true" type="String"/>
		<cfargument name="montant" 				required="true" type="Numeric"/>
		<cfargument name="segmentfixe" 			required="true" type="Numeric"/>
		<cfargument name="segmentmobile" 		required="true" type="Numeric"/>
		<cfargument name="segmentdata" 			required="true" type="Numeric"/>
		<cfargument name="idtypecommande" 		required="true" type="Numeric"/>
		<cfargument name="numerocommande" 		required="true" type="String"/>
		<cfargument name="idprofileequipement"	required="true" type="Numeric"/>
		<cfargument name="idsitefacturation"	required="true" type="Numeric"/>
		<cfargument name="libelleattention"	    required="true" type="String"/>
		<cfargument name="articles" 			required="true" type="Any"/>
		<cfargument name="v1" 					required="true" type="String"/>
		<cfargument name="v2" 					required="true" type="String"/>
		<cfargument name="v3" 					required="true" type="String"/>
		<cfargument name="v4" 					required="true" type="String"/>
		<cfargument name="v5" 					required="true" type="String"/>
		<cfargument name="files" 				required="true" type="Any"/>
		<cfargument name="boolmail" 			required="true" type="Numeric"/>
		<cfargument name="action" 				required="true" type="fr.consotel.consoview.M16.vo.ActionVo"/>
		<cfargument name="uuidreference" 		required="true" type="String"/>
			
			<cfset idracine = session.perimetre.id_groupe>
			
			<cfset booldevis 		= 0>
			<cfset idgrouperepere 	= 0>
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.enregistreroperation_v6">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#idracine#" 				null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#idgestionnaire#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpoolgestionnaire" 	value="#idpoolgestionnaire#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" 			value="#idcompte#" 				null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte" 		value="#idsouscompte#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 			value="#idoperateur#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 			value="#idrevendeur#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 	value="#idcontact#" 			null="#iif((idcontact eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" 		value="#idtransporteur#" 		null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 	value="#idsitedelivraison#" 	null="false">	
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_numero_tracking" 		value="#numerotracking#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_operation" 	value="#libellecommande#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_client" 			value="#refclient#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_revendeur" 		value="#refrevendeur#" 			null="false">			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_operation" 		value="#dateoperation#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_effet_prevue" 	value="#datelivraison#" 		null="false">			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_commentaires" 		value="#commentaires#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_bool_devis" 			value="#booldevis#" 			null="true">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   variable="p_montant" 				value="#montant#" 				null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_fixe" 		value="#segmentfixe#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_mobile" 		value="#segmentmobile#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_type_ope" 		value="#idtypecommande#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 		value="#idgrouperepere#" 		null="true">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_numero_commande" 		value="#numerocommande#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_profil_commande" 		value="#idprofileequipement#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_idsite_facturation"  	value="#idsitefacturation#" 	null="false">
	 			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_v1" 					value="#v1#"					null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_v2"				 	value="#v2#" 					null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_v3"				 	value="#v3#"					null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_v4"				 	value="#v4#"					null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_v5"				 	value="#v5#" 					null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_attention"   	value="#libelleattention#"		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_client_2"   		value="#refclient2#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_segment_data"   		value="#segmentdata#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_liv_disrtib"   		value="0" 						null="false">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

			<cfif p_retour GT 0>

				<cftry>

		            <cfthread action="run" name="attachementsrecordCommande" recordCommandeId="#p_retour#" recordCommandeFiles="#files#" recordCommandeNumber="#numerocommande#"> 
		                		     
		               	<cftry>
							
							<cfset setFilesRslt = createObject('component',"fr.consotel.consoview.M16.upload.UploaderFile").setDataBaseFilesUploaded(recordCommandeFiles, recordCommandeId)>
	
						<cfcatch>
					
							<!--- EN CAS D'ÉCHEC DU THREAD --->						
							<cflog type="error" text="#CFCATCH.Message#">													
							
							<cfmail server="mail.consotel.fr" port="26" from="bdcmobile@consotel.fr" to="dev@consotel.fr" 
									failto="dev@consotel.fr" subject="[M16-WARN-THREAD]Enregistrement des pièces jointes" type="html">
									
									<cfoutput>
										
										Tread en erreur -> #Now()#<br/><br/>
										Thread		: recordCommande -> Enregistrement des pièces jointes<br/>
										Commande N° : #recordCommandeNumber#<br/>
										Idcommande  : #recordCommandeId#<br/><br/>
										Message d'erreur :<br/><br/>
										#cfcatch.message#<br/>
									
									</cfoutput>
									
							</cfmail>
							
						</cfcatch>					
						</cftry>

		            </cfthread>
		            
		            <cfthread action="run" name="articlesrecordcommande" tidcommande="#p_retour#" tidpool="#idpoolgestionnaire#" tarticle="#articles#"
		           											tidtypecommande="#idtypecommande#" tidoperateur="#idoperateur#" tsendmail="#boolmail#" 
		           												tnumerocommande="#numerocommande#"  tuuidreference="#uuidreference#" taction="#action#" tmail="#mail#"> 
							<cftry>
							
								<cfset rslt = enregistrerArticles(#tidcommande#, #tidpool#, #tarticle#)>
						
								<cfif taction.IDACTION GT 0>
									
			 						<cfset rslt = createObject('component',"fr.consotel.consoview.M16.V3.MailService").sendMailInfosNewCommande(#tmail#, #tidcommande#, 
																														 						#tidtypecommande#, #tidoperateur#,
																														 						#tsendmail#, #taction#, #tnumerocommande#, 
																																				#tuuidreference#, 0)>
			 					
								</cfif>
						
						 	<cfcatch>
					
							<!--- EN CAS D'ÉCHEC DU THREAD --->						
							<cflog type="error" text="#cfcatch.message#">													
							
							<cfmail server="mail.consotel.fr" port="26" from="bdcmobile@consotel.fr" to="dev@consotel.fr" 
									failto="dev@consotel.fr" subject="[M16-WARN-THREAD]Enregistrement des acticles et actions" type="html">
									
									<cfoutput>
										
										Tread en erreur -> #Now()#<br/><br/>
										Thread		: recordCommande -> Enregistrement des acticles et actions<br/>
										Commande N° : #tnumerocommande#<br/>
										Idcommande  : #tidcommande#<br/><br/>
										Message d'erreur :<br/><br/>
										#cfcatch.message#<br/>
									
									</cfoutput>
									
							</cfmail>
							
						</cfcatch>					
						</cftry>
						
		            </cfthread>
	            
	            <cfcatch>
					
					<!--- EN CAS D'ÉCHEC D'UN THREAD --->						
					<cflog type="error" text="#cfcatch.message#">													
					
					<cfmail server="mail.consotel.fr" port="26" from="bdcmobile@consotel.fr" to="dev@consotel.fr" 
							failto="dev@consotel.fr" subject="[M16-WARN-THREAD]Erreur global thread enregistrement" type="html">
							
							<cfoutput>
								
								Tread en erreur -> #Now()#<br/><br/>
								Thread		: recordCommande -> Erreur global thread enregistrement pièces jointes, articles, envoie de mails<br/>
								Commande N° : #numerocommande#<br/>
								Idcommande  : #p_retour#<br/><br/>
								Message d'erreur :<br/><br/>
								#cfcatch.message#<br/>
							
							</cfoutput>
							
					</cfmail>
					
				</cfcatch>					
				</cftry>

			<cfelse>
			
				<cfset p_retour = 0>
				
			</cfif>
				
		<cfreturn p_retour>		
	</cffunction>
	
	<!--- ENREGISTREMENT DES INFORMATIONS D'UNE COMMANDE AVEC DES LIGNES EXISTANTES --->
	
	<cffunction name="recordCommandeLignesExistantes" displayname="recordCommandeLignesExistantes" access="remote" output="false" returntype="Array" hint="enregistrement des informations d'une commande avec des lignes existantes">
		<cfargument name="mail" 			 	required="true" type="fr.consotel.consoview.M16.vo.MailVo"/>
		<cfargument name="articlesandcommandes" required="true" type="Array"/>
		<cfargument name="files" 				required="true" type="Array"/>
		<cfargument name="boolmail" 			required="true" type="Numeric"/>
		<cfargument name="action" 				required="true" type="fr.consotel.consoview.M16.vo.ActionVo"/>
		<cfargument name="uuidreference" 		required="true" type="String"/>
			
			<cfset var objectCommande 	= ArrayNew(1)>
			<cfset var idsCommande 		= ArrayNew(1)>
			
			<cfset idracine = session.perimetre.id_groupe>
			
			<cfset nbrCmds	= arrayLen(articlesandcommandes)>
			
			<cfset booldevis 		= 0>
			<cfset idgrouperepere 	= 0>
			
			<cfloop index="idx" from="1" to="#nbrCmds#">
				
				<cfset idgestionnaire 		= articlesandcommandes[idx].COMMANDE.IDGESTIONNAIRE>
				<cfset idpoolgestionnaire 	= articlesandcommandes[idx].COMMANDE.IDPOOLGESTIONNAIRE>
				<cfset idcompte 			= articlesandcommandes[idx].COMMANDE.IDCOMPTE>
				<cfset idsouscompte 		= articlesandcommandes[idx].COMMANDE.IDSOUSCOMPTE>
				<cfset idoperateur 			= articlesandcommandes[idx].COMMANDE.IDOPERATEUR>
				<cfset idrevendeur 			= articlesandcommandes[idx].COMMANDE.IDREVENDEUR>
				<cfset idcontact 			= articlesandcommandes[idx].COMMANDE.IDCONTACT>
				<cfset idtransporteur 		= articlesandcommandes[idx].COMMANDE.IDTRANSPORTEUR>
				<cfset idsitedelivraison 	= articlesandcommandes[idx].COMMANDE.IDSITELIVRAISON>
				<cfset numerotracking 		= articlesandcommandes[idx].COMMANDE.NUMEROTRACKING>
				<cfset libellecommande 		= articlesandcommandes[idx].COMMANDE.LIBELLECOMMANDE>
				<cfset refclient 			= articlesandcommandes[idx].COMMANDE.REFCLIENT1>
				<cfset refrevendeur 		= articlesandcommandes[idx].COMMANDE.REFOPERATEUR>
				<cfset dateoperation 		= articlesandcommandes[idx].COMMANDE.DATEHOURS>
				<cfset datelivraison 		= DateFormat(articlesandcommandes[idx].COMMANDE.DATELIVRAISONCOMMANDE,'YYYY-MM-DD')>
				<cfset commentaires 		= articlesandcommandes[idx].COMMANDE.COMMENTAIRES>
				<cfset montant 				= articlesandcommandes[idx].COMMANDE.MONTANT>
				<cfset segmentfixe 			= articlesandcommandes[idx].COMMANDE.SEGMENTFIXE>
				<cfset segmentmobile 		= articlesandcommandes[idx].COMMANDE.SEGMENTMOBILE>
				<cfset idtypecommande 		= articlesandcommandes[idx].COMMANDE.IDTYPEOPERATION>
				<cfset numerocommande 		= articlesandcommandes[idx].COMMANDE.NUMEROCOMMANDE>
				<cfset idprofileequipement 	= articlesandcommandes[idx].COMMANDE.IDTYPECOMMANDE>
				<cfset idsitefacturation 	= articlesandcommandes[idx].COMMANDE.IDSITEFACTURATION>
				<cfset V1 					= articlesandcommandes[idx].COMMANDE.V1>
				<cfset V2					= articlesandcommandes[idx].COMMANDE.V2>
				<cfset V3 					= articlesandcommandes[idx].COMMANDE.V3>
				<cfset V4 					= articlesandcommandes[idx].COMMANDE.V4>
				<cfset V5 					= articlesandcommandes[idx].COMMANDE.V5>
				<cfset libelleattention 	= articlesandcommandes[idx].COMMANDE.ATTENTIONPOUR>
				<cfset refclient2 			= articlesandcommandes[idx].COMMANDE.REFCLIENT2>
				<cfset segmentdata 			= articlesandcommandes[idx].COMMANDE.SEGMENTDATA>
				
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.enregistreroperation_v6">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#idracine#" 				null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#idgestionnaire#" 		null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpoolgestionnaire" 	value="#idpoolgestionnaire#" 	null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" 			value="#idcompte#" 				null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte" 		value="#idsouscompte#" 			null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 			value="#idoperateur#" 			null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 			value="#idrevendeur#" 			null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 	value="#idcontact#" 			null="#iif((idcontact eq 0), de("yes"), de("no"))#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" 		value="#idtransporteur#" 		null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 	value="#idsitedelivraison#" 	null="false">	
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_numero_tracking" 		value="#numerotracking#" 		null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_operation" 	value="#libellecommande#" 		null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_client" 			value="#refclient#" 			null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_revendeur" 		value="#refrevendeur#" 			null="false">			
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_operation" 		value="#dateoperation#" 		null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_effet_prevue" 	value="#datelivraison#" 		null="false">			
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_commentaires" 		value="#commentaires#" 			null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_bool_devis" 			value="#booldevis#" 			null="true">
					<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   variable="p_montant" 				value="#montant#" 				null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_fixe" 		value="#segmentfixe#" 			null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_mobile" 		value="#segmentmobile#" 		null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_type_ope" 		value="#idtypecommande#" 		null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 		value="#idgrouperepere#" 		null="true">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_numero_commande" 		value="#numerocommande#" 		null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_profil_commande" 		value="#idprofileequipement#" 	null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_idsite_facturation"  	value="#idsitefacturation#" 	null="false">
		 			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_v1" 					value="#v1#"					null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_v2"				 	value="#v2#" 					null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_v3"				 	value="#v3#"					null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_v4"				 	value="#v4#"					null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_v5"				 	value="#v5#" 					null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_attention"   	value="#libelleattention#"		null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_client_2"   		value="#refclient2#" 			null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_segment_data"   		value="#segmentdata#" 			null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_liv_disrtib"   		value="0" 						null="false">
					<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
				</cfstoredproc>

				<cfif p_retour GT 0>

					<cfset cmdstruct 				= structnew()>
					<cfset cmdstruct.IDCOMMANDE		= #p_retour#>
					<cfset cmdstruct.IDPOOLGEST		= #idpoolgestionnaire#>
					<cfset cmdstruct.IDOPERATEUR	= #idoperateur#>
					<cfset cmdstruct.IDTYPEOPE		= #idtypecommande#>
					<cfset cmdstruct.NUMEROCOMMANDE	= #numerocommande#>
					<cfset cmdstruct.ARTICLES		= #articlesandcommandes[idx].ARTICLES#>

		            <cfset ArrayAppend(objectCommande, cmdstruct)>
					<cfset ArrayAppend(idsCommande, p_retour)>
					
				<cfelse>
				
					<!--- ENVOI DE MAIL --->
					<cfset ArrayAppend(idsCommande, p_retour)>
					
					<cfmail server="mail.consotel.fr" port="26" from="bdcmobile@consotel.fr" to="postmaster@consotel.fr" 
							failto="dev@consotel.fr" bcc="dev@consotel.fr" subject="[WARN-M16-COMMANDE]Enregistrement de commande en erreur" type="html">

							<cfoutput>
								
								Procédure: recordCommandeLignesExistantes<br/><br/>
								
								Status enregistrement : #p_retour#<br/><br/>
								
								Commande N° #commandes[idx].COMMANDE.NUMEROCOMMANDE#<br/><br/>
								
								idpool = #idpoolgestionnaire#<br/>
								idoperateur = #idoperateur#<br/>
								idtypeoperation = #idtypecommande#<br/>
								
								Articles :<br/><br/>
																
								<cfdump var="#commandes[idx].ARTICLES#">

							</cfoutput>
							
					</cfmail>
						
				</cfif>

			</cfloop>

			<cftry>
			
				<cfthread action="run" name="articlesrecordcommandelgnesexistantes" tlecurrtentStruct="#objectCommande#" tlecurrentFiles="#files#" tlesendmail="#boolmail#" 
					           												tleuuidreference="#uuidreference#" tleaction="#action#" tlemail="#mail#">
	
					<cftry>
						
						<cfset nbrCmdsRecords = arrayLen(tlecurrtentStruct)>

						<cfloop index="idxCmd" from="1" to="#nbrCmdsRecords#">
							
							<cfset filesCurrentUUID = createUUID()>
											
							<cfloop index="idxFiles" from="1" to="#ArrayLen(tlecurrentFiles)#">
																
								<cfset tlecurrentFiles[idxFiles].fileIdCommande 	= #tlecurrtentStruct[idxCmd].IDCOMMANDE#>
								<cfset tlecurrentFiles[idxFiles].fileUUID 			= filesCurrentUUID>
								
							</cfloop>
											
							<cfset setFilesRslt = createObject('component',"fr.consotel.consoview.M16.upload.UploaderFile").uploadFiles(tlecurrentFiles, 1)>
			
			
							<cfset rslt = enregistrerArticles(#tlecurrtentStruct[idxCmd].IDCOMMANDE#, #tlecurrtentStruct[idxCmd].IDPOOLGEST#, #tlecurrtentStruct[idxCmd].ARTICLES#)>
			
							<cfif tleaction.IDACTION GT 0>
								
		 						<cfset rslt = createObject('component',"fr.consotel.consoview.M16.V3.MailService").sendMailInfosNewCommande(#tlemail#, 
		 																																	#tlecurrtentStruct[idxCmd].IDCOMMANDE#, 
																													 						#tlecurrtentStruct[idxCmd].IDTYPEOPE#, 
																													 						#tlecurrtentStruct[idxCmd].IDOPERATEUR#,
																													 						#tlesendmail#, 
																													 						#tleaction#, 
																													 						#tlecurrtentStruct[idxCmd].NUMEROCOMMANDE#, 
																																			#tleuuidreference#, 
																																			0)>
		 					
							</cfif>
			
						</cfloop>
					
					<cfcatch>		
					
						<!--- EN CAS D'ÉCHEC DU THREAD --->						
						<cflog type="error" text="#CFCATCH.Message#">													
						
						<cfmail server="mail.consotel.fr" port="26" from="bdcmobile@consotel.fr" to="postmaster@consotel.fr" 
								failto="dev@consotel.fr" bcc="dev@consotel.fr" subject="[WARN-M16-THREAD]Erreur thread [articlesrecordcommandelgnesexistantes]" type="html">

								<cfoutput>
									
									Tread en erreur -> <br/><br/>
									Thread		: recordCommandeLignesExistantes -> articlesrecordcommandelgnesexistantes<br/><br/>
									
									#CFCATCH.Message#<br/><br/>
									
									<cfdump var="#tlecurrtentStruct#">

								</cfoutput>
								
						</cfmail>
						
					</cfcatch>					
					</cftry>
	
	            </cfthread>
		
			<cfcatch>
				
				<!--- EN CAS D'ÉCHEC DES THREADS --->						
				<cflog type="error" text="#CFCATCH.Message#">													
				
				<cfmail server="mail.consotel.fr" port="26" from="bdcmobile@consotel.fr" to="postmaster@consotel.fr" 
						failto="dev@consotel.fr" bcc="dev@consotel.fr" subject="[WARN-M16-THREAD]Erreur thread" type="html">
						
						
						<cfoutput>
							
							Tread en erreur -> <br/><br/>
							Thread		: recordCommandeLignesExistantes<br/><br/>
							
							#CFCATCH.Message#<br/><br/>
							
							<cfdump var="#tlecurrtentStruct#">
						
						</cfoutput>
						
				</cfmail>
				
			</cfcatch>					
			</cftry>
				
		<cfreturn idsCommande>		
	</cffunction>
	
	<!--- ENREGISTREMENT DES ARTICLES D'UNE COMMANDE --->
	
	<cffunction name="enregistrerArticles" displayname="enregistrerArticles" access="remote" output="false" returntype="Array" hint="ENREGISTREMENT DES ARTICLES D'UNE COMMANDE">
		<cfargument name="idcommande" 			required="true" type="Numeric"/>
		<cfargument name="idpoolgestionnaire" 	required="true" type="Numeric"/>
		<cfargument name="articles" 			required="true" type="Array"/>
				
		<cfset var rsltErase 	= eraseAticles(idcommande)>		
		<cfset var erreur 		= 0>
		<cfset var islast 		= 0>
		<cfset var idsCommande 	= ArrayNew(1)>
		<cfset var lenArticles 	= ArrayLen(articles)>

		<cfloop index="idx" from="1" to="#lenArticles#">
				
			<cfif idx EQ lenArticles>
				
				<cfset islast = 1>
				
				<cfif erreur GT 0>
			
					<cfset islast = 0>

				</cfif>
				
			</cfif>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.enregistrerarticle_v3">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 		value="#idcommande#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_CLOB" 	variable="p_articles" 			value="#tostring(articles[idx])#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idpoolgestionnaire" value="#idpoolgestionnaire#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_is_last" 			value="#islast#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>					
						
			<cfif p_retour GT 0>
			
				<!--- ON NE FAIT RIEN C'EST OK --->
			
			<cfelse>
			
				<cfset erreur = erreur + 1>
			
			</cfif>

			<cfset ArrayAppend(idsCommande, p_retour)>	
								
		</cfloop>
				
		<cfreturn idsCommande> 
	</cffunction>
	
	<!--- SUPPRIMME LES ARTICLES D'UNE COMMANDE --->
	
	<cffunction name="eraseAticles" access="remote" returntype="Numeric" output="false" hint="SUPPRIMME LES ARTICLES D'UNE COMMANDE">
		<cfargument name="idcommande" required="true" type="Numeric"/>		
		
			<cfset idclient = session.user.clientaccessid>
					
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.delete_articles_op">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperation" 	value="#idcommande#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_user" 			value="#idclient#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>  
	</cffunction>
	
	<!--- RECUPERE LES DETAILS DE LA COMMANDE AINSI QUE SES ARTICLES --->
	
	<cffunction name="getInfosSelectedCommande" access="remote" returntype="Any" output="false" hint="RECUPERE LES DETAILS DE LA COMMANDE AINSI QUE SES ARTICLES">
		<cfargument name="idcommande" 	required="true" type="Numeric"/>
		<cfargument name="idpoolcmd"	required="true" type="Numeric"/>
		<cfargument name="idoperateur" 	required="true" type="Numeric"/>
		<cfargument name="idrevendeur" 	required="true" type="Numeric"/>

			<cfset infosCommande = structnew()>

			<cfthread action="run" name="detailsCommandeSelected" myidcommande="#idcommande#">
			
				<cfset detailsCommandeSelected.RESULT = fournirDetailOperation(myidcommande)>
			
			</cfthread>

			<cfthread action="run" name="articlesCommandeSelected" myidcommande="#idcommande#">
			
				
				<cfset articlesCommandeSelected.RESULT = fournirArticlesCommande(myidcommande)>
			
			</cfthread>
			
			<cfthread action="run" name="organisationsCommandeSelected" myidcommande="#idcommande#">
				
				<cfset organisationsCommandeSelected.RESULT = createObject('component',"fr.consotel.consoview.M16.V3.OrganisationService").fournirListeOrganisations()>
			
			</cfthread>
			
			<cfthread action="run" name="slaCommandeSelected" myidoperateur="#idoperateur#" myidrevendeur="#idrevendeur#">
			
				<cfset slaCommandeSelected.RESULT = createObject('component',"fr.consotel.consoview.M16.V3.RevendeurService").getRevendeurSLA(myidrevendeur, myidoperateur)>
			
			</cfthread>
			
			<cfthread action="run" name="facLivCommandeSelected" myidpool="#idpoolcmd#">
			
				<cfset facLivCommandeSelected.RESULT = createObject('component',"fr.consotel.consoview.M16.V3.SiteService").fournirListeSites(myidpool)>
			
			</cfthread>
			
			<cfthread action="run" name="compteSousCompteCommandeSelected" myidoperateur="#idoperateur#" myidpool="#idpoolcmd#">
			
				<cfset compteSousCompteCommandeSelected.RESULT = createObject('component',"fr.consotel.consoview.M16.V3.CompteSousCompteService").fournirCompteSousComptesOperateur(myidoperateur, myidpool)>
			
			</cfthread>

			<cfthread action="run" name="libelleCompteOperateurCommandeSelected" myidoperateur="#idoperateur#">
			
				<cfset libelleCompteOperateurCommandeSelected.RESULT = createObject('component',"fr.consotel.consoview.M16.V3.CompteSousCompteService").fournirLibelleCompteOperateur(myidoperateur)>
			
			</cfthread>

			<cfthread action="join" name="detailsCommandeSelected, articlesCommandeSelected, organisationsCommandeSelected,slaCommandeSelected, facLivCommandeSelected, facLivCommandeSelected, libelleCompteOperateurCommandeSelected" timeout="40000"/>

			<cfset infosCommande.DETAILS		= cfthread.detailsCommandeSelected.RESULT>
			<cfset infosCommande.ARTICLES 		= cfthread.articlesCommandeSelected.RESULT>
			<cfset infosCommande.ORGANISATIONS 	= cfthread.organisationsCommandeSelected.RESULT>
			<cfset infosCommande.REVENDEURSLA 	= cfthread.slaCommandeSelected.RESULT>
			<cfset infosCommande.SITEFACLIV 	= cfthread.facLivCommandeSelected.RESULT>
			<cfset infosCommande.COMPTEOPE	 	= cfthread.compteSousCompteCommandeSelected.RESULT>
			<cfset infosCommande.LIBELLEOPE 	= cfthread.libelleCompteOperateurCommandeSelected.RESULT>
			
		<cfreturn infosCommande> 
	</cffunction>
	
</cfcomponent>