<cfcomponent displayname="fr.consotel.consoview.M16.V3.RevendeurService" hint="GESTIONS DES REVENDEURS" output="false">
	
	<!--- RÉCUPÈRE LE NOMBRE DE REVENDEURS AUTORISÉS --->
	
	<cffunction name="fournirNumberRevendeursAutorises" access="remote" returntype="Numeric" hint="RÉCUPÈRE LE NOMBRE DE REVENDEURS AUTORISÉS">
		
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.numberdistribautorises">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#idracine#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" variable="p_apploginid" 			value="#idgestionnaire#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- RÉCUPÈRE LE PROFIL DE LA PERSONNE CONNECTÉE (REVENDEUR, CLIENT, CONSOTEL) --->

	<cffunction name="getUserProfil" access="remote" returntype="Numeric" output="false" hint="TYPE PROFIL (CLIENT USER DISTIB)">

			<cfset idtypeprofil = session.user.idtypeprofil>

		<cfreturn idtypeprofil>
	</cffunction>

	<!--- RECUPERE LES REVENDEURS AUTORISE SELON L'UTILISATEUR ET LE TYPE DE COMMANDE SELECTIONNE --->
	
	<cffunction name="fournirOperateursAutorises" access="public" returntype="Array">
		<cfargument name="idtypecommande"	type="Numeric" required="true">
		
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.opeautorises">	
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_iduser" 				value="#idgestionnaire#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" value="#idtypecommande#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
						
			<cfset operateurs = ArrayNew(1)>
			<cfset allOperateur = p_retour>
			
			<cfloop index="idx" from="1" to="#p_retour.recordcount#">
				
				<cfset operateur = structNew()>
				<cfset operateur.LIBELLE = allOperateur['NOM'][idx]>
				<cfset operateur.OPERATEURID = allOperateur['OPERATEURID'][idx]>
				
				<cfset ArrayAppend(operateurs, operateur)>
				
			</cfloop>
		
		<cfreturn operateurs>
	</cffunction>
	
	<!--- RÉCUPÈRE LES REVENDEURS AUTORISÉS --->

	<cffunction name="fournirRevendeursAutorises" access="remote" returntype="Query" hint="RÉCUPÈRE LES REVENDEURS AUTORISÉS">
		<cfargument name="idtypecommande"	type="Numeric" required="true">
		<cfargument name="idOperateur" 		type="Numeric" required="true">
		
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.distribautorises">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#idracine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_apploginid" 			value="#idgestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idtypecommande#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 			value="#idOperateur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- RÉCUPÈRE LES AGENCES D'UN REVENDEUR --->
	
	<cffunction name="fournirAgencesRevendeur" access="public" returntype="query" output="false" hint="Lister les agences d'une société." >
		<cfargument name="idcontact" 	type="Numeric" 	required="true"/>
		<cfargument name="chaine" 		type="String" 	required="true"/>
		
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset globalization	= session.user.globalization>
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.get_liste_agence_v3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact" 	value="#idcontact#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 		value="#chaine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_langue" 		value="#globalization#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">
				<cfprocresult name="p_result">
			</cfstoredproc>
		
		<cfreturn p_result>			
	</cffunction>

	<!--- RÉCUPÈRE LES DIFFÉRENTES PROPRIÉTÉS D'UN REVENDEUR CONCERNANT DIFFÉRENTS DELAIS --->
	
	<cffunction name="getRevendeurSLA" access="remote" returntype="query" output="false" hint="obtenir les sla d'un revendeur">
		<cfargument name="idrevendeur" required="true" type="numeric" default="" displayname="numeric idrevendeur" hint="ID du revendeur a renommer"/>
		<cfargument name="idoperateur" required="true" type="numeric" default="" displayname="numeric idOperateur" hint="ID de l'operateur"/>                
			
			<cfset idracine = session.perimetre.id_groupe>
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.get_revendeur_sla_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur"  value="#idrevendeur#">                        
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idOperateur"  value="#idoperateur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">					
				<cfprocresult name="p_retour">
			</cfstoredproc> 
			         
		<cfreturn p_retour>        
	</cffunction>
	
	<!--- FOURNIT LA LISTE DES GESTIONNAIRES POUR LESQUELS ON PEUT FAIRE UNE COMMANDE (SNCF) --->

	<cffunction name="fournirCollaborateursFor" access="remote" returntype="Query" hint="FOURNIT LA LISTE DES GESTIONNAIRES POUR LESQUELS ON PEUT FAIRE UNE COMMANDE">
		<cfargument name="idprofilequipement" required="true" type="Numeric"/>
		<cfargument name="idrevendeur" 		  required="true" type="Numeric"/>

			<cfset idracine = session.perimetre.id_groupe>
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.gestionnaires_client_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#idracine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idprofilequipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 			value="#idrevendeur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

</cfcomponent>