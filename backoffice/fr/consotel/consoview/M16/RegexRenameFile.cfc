<cfcomponent displayname="fr.consotel.consoview.M16.RegexRenameFile" hint="Cherche les caractères spéciaux les suppriment puis retourne la nouvelle chaine" output="false">

	<!--- Cherche les caractères spéciaux les suppriment puis retourne la nouvelle chaine --->

	<cffunction name="changeSpecialCaracters" access="remote" returntype="String" output="false" hint="Cherche les caractères spéciaux les suppriment puis retourne la nouvelle chaine">
		<cfargument name="currentName" 		required="true" type="String"/>
		
			<cfset var specialChars = arrayNew(1)>
			<cfset var chars 		= structNew()>
			
			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("À,Á,Â,Ã,Ä,Å,à,á,â,ã,ä,å")>
			<cfset chars.CHAR  = "a">
			
			<cfset arrayAppend(specialChars, chars)>
			
			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ç,ç")>
			<cfset chars.CHAR  = "c">
			
			<cfset arrayAppend(specialChars, chars)>
			
			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("È,É,Ê,Ë,è,é,ê,ë")>
			<cfset chars.CHAR  = "e">
			
			<cfset arrayAppend(specialChars, chars)>
			
			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ì,Í,Î,Ï,ì,í,î,ï")>
			<cfset chars.CHAR  = "i">
			
			<cfset arrayAppend(specialChars, chars)>
			
			<cfset arrayAppend(specialChars, chars)>
			
			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ñ,ñ")>
			<cfset chars.CHAR  = "n">
			
			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ò,Ó,Ô,Õ,Ö,ð,ò,ó,ô,õ,ö")>
			<cfset chars.CHAR  = "o">
			
			<cfset arrayAppend(specialChars, chars)>
			
			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ù,Ú,Û,Ü,ù,ú,û,ü")>
			<cfset chars.CHAR  = "u">

			<cfset arrayAppend(specialChars, chars)>
			
			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ý,Ÿ,ý,ÿ")>
			<cfset chars.CHAR  = "y">
			
			<cfset arrayAppend(specialChars, chars)>
			
			<cfset lenSpecialChar = arrayLen(specialChars)>
			
			<!--- ICI ON REMPLACE TOUS LES ACCENTS ET AUTRES --->
			<cfloop index="idx1" from="1" to="#lenSpecialChar#">
			
				<cfset arraySpecialChars = specialChars[idx1]['CHARS']>
				<cfset lenSpecialChars = arrayLen(arraySpecialChars)>
		
				<cfloop index="idx2" from="1" to="#lenSpecialChars#">
				
					<cfset currentName = REReplace(currentName, arraySpecialChars[idx2], specialChars[idx1]['CHAR'], "All")>
		
				</cfloop>
			
			</cfloop>
			
			<!--- ICI ON REMPLACE TOUS LES CARACTERES SPECIAUX PAR '_' --->
			<!--- <cfset currentName = REReplace(currentName, "[&!?/\\-]", "_", "All")> --->
			<cfset currentName = REReplace(currentName, "[&!?/\\]", "_", "All")>
			<cfset currentName = REReplace(currentName, "<", "_", "All")>
			<cfset currentName = REReplace(currentName, ">", "_", "All")>
			<cfset currentName = REReplace(currentName, "}", "_", "All")>
			<cfset currentName = REReplace(currentName, "{", "_", "All")>
			<cfset currentName = REReplace(currentName, "\(", "_", "All")>
			<cfset currentName = REReplace(currentName, "\)", "_", "All")>
			<cfset currentName = REReplace(currentName, "\+", "_", "All")>
			<cfset currentName = REReplace(currentName, "\*", "_", "All")>			
			<cfset currentName = REReplace(currentName, "\[", "_", "All")>
			<cfset currentName = REReplace(currentName, "\]", "_", "All")>
			<cfset currentName = REReplace(currentName, "\]", "_", "All")>
			<cfset currentName = REReplace(currentName, "°", "_", "All")>
			<cfset currentName = REReplace(currentName, "~", "_", "All")>
			<cfset currentName = REReplace(currentName, "'", "_", "All")>
			<cfset currentName = REReplace(currentName, "\|", "_", "All")>
			<cfset currentName = REReplace(currentName, "`", "_", "All")>
			<cfset currentName = REReplace(currentName, "\^", "_", "All")>
			<cfset currentName = REReplace(currentName, "@", "_", "All")>
			<cfset currentName = REReplace(currentName, "=", "_", "All")>
			<cfset currentName = REReplace(currentName, ",", "_", "All")>
			<cfset currentName = REReplace(currentName, ";", "_", "All")>
		
		<cfreturn currentName>  
	</cffunction>
	
	
	<cffunction name="containSpecialCaracters" access="remote" returntype="Numeric" output="false" hint="Recherche si la chaine contient des caractères spéciaux 0-NON, 1-OUI">
		<cfargument name="currentName" 		required="true" type="String"/>
			
			<cfset var findResult		= 0>
			<cfset var specialChars 	= ListToArray("À,Á,Â,Ã,Ä,Å,à,á,â,ã,ä,å,Ç,ç,È,É,Ê,Ë,è,é,ê,ë,Ì,Í,Î,Ï,ì,í,î,ï,Ñ,ñ,Ò,Ó,Ô,Õ,Ö,ð,ò,ó,ô,õ,ö,Ù,Ú,Û,Ü,ù,ú,û,ü,Ý,Ÿ,ý,ÿ")>
			<!--- <cfset var specialRegex 	= ListToArray("[&!?/\\-],<, >, }, {,\(, \), \+, \*, \[, \], \], °, ~, ', \|, `, \^, @, =, ,, ;")> --->
			<cfset var specialRegex 	= ListToArray("[&!?/\\],<, >, }, {,\(, \), \+, \*, \[, \], \], °, ~, ', \|, `, \^, @, =, ,, ;")>
			<cfset var lenSpecialChar 	= arrayLen(specialChars)>
			<cfset var lenSpecialRegex 	= arrayLen(specialRegex)>
			
			<!---
			<cfset specialRegex 	= ListToArray("[&!?/\\-]","<", ">", "}", "{","\(", "\)", "\+", "\*", "\[", "\]", "\]", "°", "~", "'", "\|", "`", "\^", "@", "=", ",", ";")>
			--->
			
			<!--- ICI ON CHERCHE TOUS LES ACCENTS ET AUTRES --->
			<cfloop index="idx1" from="1" to="#lenSpecialChar#">
			
				<cfset findResult = REFind(currentName, specialChars[idx1])>
				
				<cfif findResult GT 0>
					
					<cfreturn findResult> 
				
				</cfif>
			
			</cfloop>
			
			<!--- ICI ON CHERCHE TOUS LES CARACTERES SPECIAUX --->
			
			<cfif REFind("[&!?/\\-]", currentName) GT 0>

				<cfreturn REFind("[&!?/\\-]", currentName)>
			
			<cfelseif REFind("<", currentName) GT 0>
			
				<cfreturn REFind("<", currentName)>
				
			<cfelseif REFind(">", currentName) GT 0>
			
				<cfreturn REFind(">", currentName)>
				
			<cfelseif REFind("{", currentName) GT 0>
			
				<cfreturn REFind("{", currentName)>
				
			<cfelseif REFind("}", currentName) GT 0>
			
				<cfreturn REFind("}", currentName)>
				
			<cfelseif REFind("\(", currentName) GT 0>
			
				<cfreturn REFind("\(", currentName)>
				
			<cfelseif REFind("\)", currentName) GT 0>
			
				<cfreturn REFind("\)", currentName)>
				
			<cfelseif REFind("\+", currentName) GT 0>
			
				<cfreturn REFind("\+", currentName)>
				
			<cfelseif REFind("\*", currentName) GT 0>
			
				<cfreturn REFind("\*", currentName)>
				
			<cfelseif REFind("\[", currentName) GT 0>
			
				<cfreturn REFind("\[", currentName)>
				
			<cfelseif REFind("\]", currentName) GT 0>
			
				<cfreturn REFind("\]", currentName)>
				
			<cfelseif REFind("°", currentName) GT 0>
			
				<cfreturn REFind("°", currentName)>
				
			<cfelseif REFind("~", currentName) GT 0>
			
				<cfreturn REFind("~", currentName)>
				
			<cfelseif REFind("'", currentName) GT 0>
			
				<cfreturn REFind("'", currentName)>
				
			<cfelseif REFind("\|", currentName) GT 0>
			
				<cfreturn REFind("\|", currentName)>
				
			<cfelseif REFind("`", currentName) GT 0>
			
				<cfreturn REFind("`", currentName)>
				
			<cfelseif REFind("\^", currentName) GT 0>
			
				<cfreturn REFind("\^", currentName)>
				
			<cfelseif REFind("@", currentName) GT 0>
			
				<cfreturn REFind("@", currentName)>
				
			<cfelseif REFind("=", currentName) GT 0>
			
				<cfreturn REFind("=", currentName)>
				
			<cfelseif REFind(",", currentName) GT 0>
			
				<cfreturn REFind(",", currentName)>
				
			<cfelseif REFind(";", currentName) GT 0>
			
				<cfreturn REFind(";", currentName)>
			
			</cfif>

		<cfreturn findResult>  
	</cffunction>

</cfcomponent>