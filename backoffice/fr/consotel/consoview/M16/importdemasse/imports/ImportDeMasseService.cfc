<cfcomponent displayname="fr.consotel.consoview.M16.importdemasse.imports.ImportDeMasseService" hint="Gestion des fichiers excel d'import de masse" output="false">

	<cffunction name="getInfosFileImported" access="remote" output="false" returntype="any" hint="Lecture d'un fichier excel et récupération des informations">
		<cfargument name="uuidFile" 			required="true" type="String"/>
		<cfargument name="filename" 			required="true" type="String"/>

			<cfset directory = "/container/importdemasse/">	
			
			<cfset path = #directory# & #uuidFile# & "/" & #filename#>

			<cfspreadsheet action="read" query="qryInfos" src="#path#" sheetname="Sheet1"/> 
					
		<cfreturn qryInfos> 
	</cffunction>
	
	<cffunction name="getInfosFileImportedMultiReferences" access="remote" output="false" returntype="any" hint="Lecture d'un fichier excel et récupération des informations pour les references">
		<cfargument name="_uuidfile" 			required="true" type="String"/>
		<cfargument name="_filename" 			required="true" type="String"/>
		<cfargument name="_causetraduced" 		required="true" type="String"/>
		<cfargument name="_subject" 			required="true" type="String"/>
		<cfargument name="_succescontent" 		required="true" type="String"/>
		<cfargument name="_errorcontent" 		required="true" type="String"/>
		<cfargument name="_resultcontent" 		required="true" type="String"/>

			<cfthread action="run" name="majreferencesenmasse" uuidfile="#_uuidfile#" filename="#_filename#" causetraduced="#_causetraduced#" subject="#_subject#" succescontent="#_succescontent#" errorcontent="#_errorcontent#" resultcontent="#_resultcontent#">

				<cftry>
	
					<cfset directory  = "/container/importdemasse/">	
					<cfset path 	  = #directory# & #uuidfile# & "/" & #filename#>
					
					<cfset commandeid = 0>
					<cfset commandes  = ArrayNew(1)>
					<cfset datas 	  = StructNew()>
					
					<cfset commandeObject = createObject('component',"fr.consotel.consoview.M16.v2.CommandeService")>
					
					<cfspreadsheet action="read" query="qryInfos" src="#path#" sheetname="Sheet1"/>
				
					<cftry>
						
						<!--- DANS LE CAS DU MOBILE --->
						<cfquery name="getGoodColumnName" dbtype="query">
							SELECT 	COL_1 as NUMERO_OPERATION, COL_2 as CODE_INTERNE, COL_3 as NOM, COL_4 as LIBELLE_EQ, COL_5 as IMEI, 
									COL_6 as SOUS_TETE, COL_7 as S_IMEI, COL_8 as S_PUK1, COL_9 as IDCOMMANDE, COL_10 as IDSOUS_TETE
							FROM qryInfos
						</cfquery>
				
					<cfcatch>
					
						<!--- DANS LE CAS DU FIXE/DATA --->
						<cfquery name="getGoodColumnName" dbtype="query">
							SELECT 	COL_1 as NUMERO_OPERATION, COL_2 as LIBELLE_EQ, COL_3 as IMEI, COL_4 as SOUS_TETE, COL_5 as IDCOMMANDE, COL_6 as IDSOUS_TETE
							FROM qryInfos
						</cfquery>
					
					</cfcatch>
					
					</cftry>
							
					<cfquery name="getRemoveHeaderColumnName" dbtype="query">
						SELECT 	*
						FROM getGoodColumnName
						WHERE getGoodColumnName.IDSOUS_TETE <> 'idsoustete'
						ORDER BY IDCOMMANDE
					</cfquery>
				
					<cfset datatoimport = getRemoveHeaderColumnName>
					
					<cfloop query="datatoimport">
					
						<cfset commande 	= StructNew()>
						<cfset columnsName 	= ListToArray(datatoimport.columnList)>
						
						<cfloop index="idx" from="1" to="#ArrayLen(columnsName)#">
						
							<cfset commande[#columnsName[idx]#] = datatoimport[#columnsName[idx]#][datatoimport.currentRow]>
						
						</cfloop>
								
						<cfset idcmd  = commande.IDCOMMANDE>
											
						<cfif commandeid NEQ idcmd>
				
							<cfset commandeid = idcmd>
							
							<cfset datas 			= StructNew()>
							<cfset datas.IDCOMMANDE = commandeid>
							
							<cfquery name="getInfosCommadeByIdsoustete" dbtype="query">
								SELECT 	*
								FROM datatoimport
								WHERE datatoimport.IDCOMMANDE =  '#commandeid#'
								ORDER BY IDSOUS_TETE
							</cfquery>
							
							<cfset datas.COMMANDES  = getInfosCommadeByIdsoustete>
											
							<cfset ArrayAppend(commandes, datas)>
								
						</cfif>
					
					</cfloop>
				
					<cfset lenCmd = ArrayLen(commandes)>
					
					<cfif lenCmd GT 1>
						
						<cfset references 		= ArrayNew(1)>
						<cfset structReferences = StructNew()>
						
						<cfloop index="idx1" from="1" to="#lenCmd#">
						
							<cfset mysegment  = 0>
							<cfset commandeid = commandes[idx1].IDCOMMANDE>
							<cfset myRsltQry  = commandes[idx1].COMMANDES>
								
							<cfset commande = commandeObject.fournirDetailOperation(#commandeid#)>			
							
							<cfif commande.SEGMENT_MOBILE EQ 1 AND commande.SEGMENT_FIXE EQ 0 AND commande.SEGMENT_DATA EQ 0>
							
								<cfset mysegment = 1>
							
							<cfelseif commande.SEGMENT_MOBILE EQ 0 AND commande.SEGMENT_FIXE EQ 1 AND commande.SEGMENT_DATA EQ 0>
							
								<cfset mysegment = 2>
							
							<cfelseif commande.SEGMENT_MOBILE EQ 0 AND commande.SEGMENT_FIXE EQ 0 AND commande.SEGMENT_DATA EQ 1>
							
								<cfset mysegment = 3>
							
							</cfif>
				
							<cfset idtypecmd	= commande['IDTYPE_OPERATION'][commande.recordcount]>
							<cfset numeroope	= commande['NUMERO_OPERATION'][commande.recordcount]>
							<cfset idrevendeur 	= commande['IDREVENDEUR'][commande.recordcount]>
							<cfset librevendeur = commande['LIBELLE_REVENDEUR'][commande.recordcount]>
							<cfset idoperateur 	= commande['OPERATEURID'][commande.recordcount]>
							<cfset liboperateur = commande['LIBELLE_OPERATEUR'][commande.recordcount]>
							<cfset idpool 		= commande['IDPOOL_GESTIONNAIRE'][commande.recordcount]>
							
							<cfset refinfos = commandeObject.fournirReferencesInfos(#commandeid#, #idpool#)>
										
							<cfloop query="myRsltQry">
											
								<cfset myidsoustete = myRsltQry['IDSOUS_TETE'][myRsltQry.currentRow]>
																	
								<cfquery name="getData" dbtype="query">
									SELECT *
									FROM refinfos
									WHERE refinfos.IDSOUS_TETE = #myidsoustete#
								</cfquery>
														
								<cfif getData.RecordCount GT 0>
				
									<cfif mysegment GT 1><!--- GESTION DU FIXE/DATA --->
									
										<cfset structReferences = StructNew()>
									
										<!--- CHAMPS INVARIABLES --->
										<cfset structReferences.IDTERMINAL 	= getData['IDTERMINAL'][getData.RecordCount]>
										<cfset structReferences.IDCARTESIM 	= getData['IDSIM'][getData.RecordCount]>
										<cfset structReferences.OLDSOUSTETE	= getData['SOUS_TETE'][getData.RecordCount]>
										<cfset structReferences.IDSOUSTETE 	= myidsoustete>		
										<cfset structReferences.OLDIDSSTETE	= 0>					
										<cfset structReferences.IDCOMMANDE 	= commandeid>
										<cfset structReferences.IDREVENDEUR = idrevendeur>
										<cfset structReferences.IDOPERATEUR = idoperateur>
										<cfset structReferences.IDPOOLGEST  = idpool>
										<cfset structReferences.IDTYPECMD	= idtypecmd>
										<cfset structReferences.SEGMENT		= mysegment>
									
										<!--- CHAMPS VARIABLES SUIVANT LE TYPE DE COMMANDE--->
										<cfset structReferences.NUM_IMEI 	= "">
										<cfset structReferences.NUM_CSIM 	= "">
										<cfset structReferences.NUM_CPUK 	= "">
										<cfset structReferences.SOUS__TETE 	= "">
									
										<cfif idtypecmd EQ 1583><!--- Commande de nouvelles lignes fixes --->
				
											<!--- MISE A JOUR DU NUMERO DE TELEPHONE, DU NUMERO DE SERIE --->
											<cfset structReferences.NUM_IMEI 	= myRsltQry['IMEI'][myRsltQry.currentRow]><!--- NON PRIS EN COMPTE --->
											<cfset structReferences.SOUS__TETE 	= myRsltQry['SOUS_TETE'][myRsltQry.currentRow]>
				
										<cfelseif idtypecmd EQ 1584><!--- Commande d'équipements fixe nus --->
											
											<!--- MISE A JOUR UNIQUEMENT Du N° DE SERIE DE L'EQUIPEMENT --->
											<cfset structReferences.NUM_IMEI 	= myRsltQry['IMEI'][myRsltQry.currentRow]>
											<cfset structReferences.SOUS__TETE 	= getData['SOUS_TETE'][getData.RecordCount]><!--- NON PRIS EN COMPTE --->
								
										<cfelseif idtypecmd EQ 1587><!--- Réengagement fixe --->
										
											<!--- AUCUNE MISE A JOUR --->
											<cfset structReferences.NUM_IMEI 	= myRsltQry['IMEI'][myRsltQry.currentRow]><!--- NON PRIS EN COMPTE --->
											<cfset structReferences.SOUS__TETE 	= myRsltQry['SOUS_TETE'][myRsltQry.currentRow]><!--- NON PRIS EN COMPTE --->
									
										</cfif>
									
									<cfelse><!--- GESTION DU MOBILE --->
									
										<cfset structReferences = StructNew()>
									
										<!--- CHAMPS INVARIABLES --->
										<cfset structReferences.IDTERMINAL 	= getData['IDTERMINAL'][getData.RecordCount]>
										<cfset structReferences.IDCARTESIM 	= getData['IDSIM'][getData.RecordCount]>
										<cfset structReferences.OLDSOUSTETE	= getData['SOUS_TETE'][getData.RecordCount]>
										<cfset structReferences.IDSOUSTETE 	= myidsoustete>		
										<cfset structReferences.OLDIDSSTETE	= 0>					
										<cfset structReferences.IDCOMMANDE 	= commandeid>
										<cfset structReferences.IDREVENDEUR = idrevendeur>
										<cfset structReferences.IDOPERATEUR = idoperateur>
										<cfset structReferences.IDPOOLGEST  = idpool>
										<cfset structReferences.IDTYPECMD	= idtypecmd>
										<cfset structReferences.SEGMENT		= mysegment>
									
										<!--- CHAMPS VARIABLES SUIVANT LE TYPE DE COMMANDE--->
										<cfset structReferences.NUM_IMEI 	= "">
										<cfset structReferences.NUM_CSIM 	= "">
										<cfset structReferences.NUM_CPUK 	= "">
										<cfset structReferences.SOUS__TETE 	= "">
									
										<cfif idtypecmd EQ 1083><!--- Commande de nouvelles lignes mobiles --->
						
											<cfif structReferences.IDTERMINAL EQ 0><!--- nouvelle ligne sans equipements --->
												
												<!--- MISE A JOUR DU NUMERO DE TELEPHONE, DU NUMERO DE SERIE DE LA CARTE SIM ET LE CODE PUK --->
												<cfset structReferences.NUM_IMEI 	= myRsltQry['IMEI'][myRsltQry.currentRow]><!--- NON PRIS EN COMPTE --->
												<cfset structReferences.SOUS__TETE 	= myRsltQry['SOUS_TETE'][myRsltQry.currentRow]>
												<cfset structReferences.NUM_CSIM 	= myRsltQry['S_IMEI'][myRsltQry.currentRow]>
												<cfset structReferences.NUM_CPUK 	= myRsltQry['S_PUK1'][myRsltQry.currentRow]>
											
											<cfelse><!--- nouvelle ligne avec equipements --->
											
												<!--- MISE A JOUR DE L'IMEI, DU NUMERO DE TELEPHONE, DU NUMERO DE SERIE DE LA CARTE SIM ET LE CODE PUK --->
												<cfset structReferences.NUM_IMEI 	= myRsltQry['IMEI'][myRsltQry.currentRow]>
												<cfset structReferences.SOUS__TETE 	= myRsltQry['SOUS_TETE'][myRsltQry.currentRow]>
												<cfset structReferences.NUM_CSIM 	= myRsltQry['S_IMEI'][myRsltQry.currentRow]>
												<cfset structReferences.NUM_CPUK 	= myRsltQry['S_PUK1'][myRsltQry.currentRow]>
											
											</cfif>
								
										<cfelseif idtypecmd EQ 1182><!--- Commande d'équipements mobile nus --->
				
											<!--- MISE A JOUR UNIQUEMENT L'IMEI --->
											<cfset structReferences.NUM_IMEI 	= myRsltQry['IMEI'][myRsltQry.currentRow]>
											<cfset structReferences.SOUS__TETE 	= getData['SOUS_TETE'][getData.RecordCount]><!--- NON PRIS EN COMPTE --->
											<cfset structReferences.NUM_CSIM 	= myRsltQry['S_IMEI'][myRsltQry.currentRow]><!--- NON PRIS EN COMPTE --->
											<cfset structReferences.NUM_CPUK 	= myRsltQry['S_PUK1'][myRsltQry.currentRow]><!--- NON PRIS EN COMPTE --->
								
										<cfelseif idtypecmd EQ 1382><!--- Réengagement mobile --->
										
											<cfif structReferences.IDCARTESIM EQ 0>
											
												<!--- MISE A JOUR DE L'IMEI, DU NUMERO DE SERIE DE LA CARTE SIM ET LE CODE PUK MAIS AVEC IDSIM = -1 ET --->
												<cfset structReferences.IDCARTESIM 	= -1>
												<cfset structReferences.NUM_IMEI 	= myRsltQry['IMEI'][myRsltQry.currentRow]>
												<cfset structReferences.SOUS__TETE 	= myRsltQry['SOUS_TETE'][myRsltQry.currentRow]><!--- NON PRIS EN COMPTE --->
												<cfset structReferences.NUM_CSIM 	= myRsltQry['S_IMEI'][myRsltQry.currentRow]>
												<cfset structReferences.NUM_CPUK 	= myRsltQry['S_PUK1'][myRsltQry.currentRow]>
											
											<cfelse>
											
												<!--- MISE A JOUR DE L'IMEI, DU NUMERO DE SERIE DE LA CARTE SIM ET LE CODE PUK --->
												<cfset structReferences.NUM_IMEI 	= myRsltQry['IMEI'][myRsltQry.currentRow]>
												<cfset structReferences.SOUS__TETE 	= myRsltQry['SOUS_TETE'][myRsltQry.currentRow]><!--- NON PRIS EN COMPTE --->
												<cfset structReferences.NUM_CSIM 	= myRsltQry['S_IMEI'][myRsltQry.currentRow]>
												<cfset structReferences.NUM_CPUK 	= myRsltQry['S_PUK1'][myRsltQry.currentRow]>
											
											</cfif>
											
										</cfif>
										
									</cfif>
													
									<cfset ArrayAppend(references, structReferences)>
								
								</cfif>
									
							</cfloop>
						
						</cfloop>
					
					</cfif>
				
					<cfset lenArticles = ArrayLen(references)>
					
					<cfset xmlreferences 	= ArrayNew(1)>
					<cfset xmlerrorsformat 	= ArrayNew(1)>
					<cfset xmlerrorssstete 	= ArrayNew(1)>
					<cfset errorqryinsert 	= ArrayNew(1)>
					<cfset validqryinsert 	= ArrayNew(1)>
					
					<cfloop index="idx2" from="1" to="#lenArticles#">
						
						<cfset myrefobject = references[idx2]>
								
						<!---Create the XML document--->
						<cfset xmlrw = XmlNew()>
						
						<!---Set the root element--->
						<cfset xmlrw.xmlRoot = XmlElemNew(xmlrw, "row")>
						
						<cfset rsltFindNoCase = 1>
				
						<cfif mysegment EQ 1>
						
							<cfif myrefobject.SOUS__TETE NEQ '' AND myrefobject.IDTYPECMD NEQ 1182>
								
								<!--- RECHERHCE DES CARACTERES 'n°' --->
								<cfset rsltFindNoCase = FindNoCase("n°", myrefobject.SOUS__TETE)>
							
							</cfif>
						
						<cfelseif myrefobject.IDTYPECMD EQ 1583>
						
							<cfif myrefobject.SOUS__TETE NEQ ''>
								
								<!--- RECHERHCE DES CARACTERES 'n°' --->
								<cfset rsltFindNoCase = FindNoCase("n°", myrefobject.SOUS__TETE)>
							
							</cfif>
						
						</cfif>
				
						<cfif rsltFindNoCase GT 0>
				
							<!--- SUPPRESSION DE 'n°' DEVANT LA SOUS TETE --->
							<cfset myrefobject.SOUS__TETE = Replace(myrefobject.SOUS__TETE, "n°", "")>
				
							<!--- SUPPRESSION DES ESPACES DEVANT LE NUMERO --->
							<cfset myrefobject.SOUS__TETE = LTrim(myrefobject.SOUS__TETE)>
							
							<!--- VERIFICATION DE LA SOUSTETE SI ELLE EST DIFFERENTE DE CELLE D'ORIGINE--->
							
							<cfset ligneObject = createObject('component',"fr.consotel.consoview.M16.v2.LigneService")>
							
							<cfset rsltOwnLigne = 0>
							
							<!--- TEST POUR VOIR SI C'EST LA MEME SOUS TETE OU NON --->
							<cfif myrefobject.SOUS__TETE NEQ myrefobject.OLDSOUSTETE>
							
								<cfset rsltOwnLigne = 1>
								
								<cfset rsltOwnLigneQuery = ligneObject.verifierUniciteNumeroLigne(myrefobject.SOUS__TETE)>
								
								<cfloop index="idex" from="1" to="#ArrayLen(rsltOwnLigneQuery)#">
								
									<cfif idex EQ 1>
									
										<cfset rsltOwnLigne = rsltOwnLigneQuery[idex]>
									
									<cfelseif idex EQ 2>
									
										<cfif rsltOwnLigne EQ 0>
										
											<cfset myrefobject.OLDIDSSTETE = rsltOwnLigneQuery[idex]>
										
										</cfif>
														
									</cfif>
				
								</cfloop>
							
							</cfif>
				
							<cfif rsltOwnLigne EQ 0>
				
								<cfif mysegment EQ 1><!--- POUR LE MOBILE --->
				
									<cfif myrefobject.IDTERMINAL GT 0><!--- ********************************************************************************************************** --->
							
										<!---Create the FirstName and LastName elements--->
										<cfset elem_equipement 		= XmlElemNew(xmlrw,"equipement")>
										<cfset elem_idequipement 	= XmlElemNew(xmlrw,"idequipement")>
										<cfset elem_no_serie 		= XmlElemNew(xmlrw,"no_serie")>
										<cfset elem_no_sim 			= XmlElemNew(xmlrw,"no_sim")>
										<cfset elem_codepin 		= XmlElemNew(xmlrw,"codepin")>
										<cfset elem_codepuk 		= XmlElemNew(xmlrw,"codepuk")>
										<cfset elem_idsous_tete 	= XmlElemNew(xmlrw,"idsous_tete")>
										<cfset elem_ligne 			= XmlElemNew(xmlrw,"ligne")>
										<cfset elem_idst 			= XmlElemNew(xmlrw,"idst")>
										<cfset elem_numidcmde 		= XmlElemNew(xmlrw,"numidcmde")>
										<cfset elem_numidpool 		= XmlElemNew(xmlrw,"numidpool")>
										
										<!---Set the text values of the  and LastName elements--->
										<cfset elem_idequipement.XmlText = myrefobject.IDTERMINAL>
										<cfset elem_no_serie.XmlText 	 = myrefobject.NUM_IMEI>
										<cfset elem_no_sim.XmlText 		 = ''>
										<cfset elem_codepin.XmlText 	 = ''>
										<cfset elem_codepuk.XmlText 	 = ''>
										<cfset elem_idsous_tete.XmlText  = myrefobject.IDSOUSTETE>
										<cfset elem_ligne.XmlText 		 = myrefobject.SOUS__TETE>
										<cfset elem_idst.XmlText 		 = myrefobject.OLDIDSSTETE>
										<cfset elem_numidcmde.XmlText 	 = myrefobject.IDCOMMANDE>
										<cfset elem_numidpool.XmlText 	 = myrefobject.IDPOOLGEST>
							
										<cfset elem_equipement.XmlChildren[1] = elem_idequipement>
										<cfset elem_equipement.XmlChildren[2] = elem_no_serie>
										<cfset elem_equipement.XmlChildren[3] = elem_no_sim>
										<cfset elem_equipement.XmlChildren[4] = elem_codepin>
										<cfset elem_equipement.XmlChildren[5] = elem_codepuk>
										<cfset elem_equipement.XmlChildren[6] = elem_idsous_tete>
										<cfset elem_equipement.XmlChildren[7] = elem_ligne>
										<cfset elem_equipement.XmlChildren[8] = elem_idst>
										<cfset elem_equipement.XmlChildren[9] = elem_numidcmde>
										<cfset elem_equipement.XmlChildren[10] = elem_numidpool>
									
										<cfset xmlrw.row.XmlChildren[1] = elem_equipement>
							
										<cfif myrefobject.IDCARTESIM NEQ 0 AND myrefobject.IDCARTESIM NEQ ''><!--- ********************************************************************************************************** --->
															
											<!---Create the FirstName and LastName elements--->
											<cfset elem_equipement_sim 		= XmlElemNew(xmlrw,"equipement")>
											<cfset elem_idequipement_sim 	= XmlElemNew(xmlrw,"idequipement")>
											<cfset elem_no_serie_sim 		= XmlElemNew(xmlrw,"no_serie")>
											<cfset elem_no_sim_sim 			= XmlElemNew(xmlrw,"no_sim")>
											<cfset elem_codepin_sim 		= XmlElemNew(xmlrw,"codepin")>
											<cfset elem_codepuk_sim 		= XmlElemNew(xmlrw,"codepuk")>
											<cfset elem_idsous_tete_sim 	= XmlElemNew(xmlrw,"idsous_tete")>
											<cfset elem_ligne_sim 			= XmlElemNew(xmlrw,"ligne")>
											<cfset elem_idst_sim 			= XmlElemNew(xmlrw,"idst")>
											<cfset elem_numidcmde_sim 		= XmlElemNew(xmlrw,"numidcmde")>
											<cfset elem_numidpool_sim 		= XmlElemNew(xmlrw,"numidpool")>
											
											<!---Set the text values of the  and LastName elements--->
											<cfset elem_idequipement_sim.XmlText = myrefobject.IDCARTESIM>
											<cfset elem_no_serie_sim.XmlText 	 = ''>
											<cfset elem_no_sim_sim.XmlText 		 = myrefobject.NUM_CSIM>
											<cfset elem_codepin_sim.XmlText 	 = ''>
											<cfset elem_codepuk_sim.XmlText 	 = myrefobject.NUM_CPUK>
											<cfset elem_idsous_tete_sim.XmlText  = myrefobject.IDSOUSTETE>
											<cfset elem_ligne_sim.XmlText 		 = myrefobject.SOUS__TETE>
											<cfset elem_idst_sim.XmlText 		 = myrefobject.OLDIDSSTETE>
											<cfset elem_numidcmde_sim.XmlText 	 = myrefobject.IDCOMMANDE>
											<cfset elem_numidpool_sim.XmlText 	 = myrefobject.IDPOOLGEST>
											
											<cfset elem_equipement_sim.XmlChildren[1] = elem_idequipement_sim>
											<cfset elem_equipement_sim.XmlChildren[2] = elem_no_serie_sim>
											<cfset elem_equipement_sim.XmlChildren[3] = elem_no_sim_sim>
											<cfset elem_equipement_sim.XmlChildren[4] = elem_codepin_sim>
											<cfset elem_equipement_sim.XmlChildren[5] = elem_codepuk_sim>
											<cfset elem_equipement_sim.XmlChildren[6] = elem_idsous_tete_sim>
											<cfset elem_equipement_sim.XmlChildren[7] = elem_ligne_sim>
											<cfset elem_equipement_sim.XmlChildren[8] = elem_idst_sim>
											<cfset elem_equipement_sim.XmlChildren[9] = elem_numidcmde_sim>
											<cfset elem_equipement_sim.XmlChildren[10] = elem_numidpool_sim>
											
											<cfif myrefobject.IDCARTESIM EQ -1>
															
												<cfset elem_idterminal 	= XmlElemNew(xmlrw,"idterminal")>
												<cfset elem_idrevendeur = XmlElemNew(xmlrw,"idrevendeur")>
												<cfset elem_idoperateur = XmlElemNew(xmlrw,"idoperateur")>
												<cfset elem_idcommande 	= XmlElemNew(xmlrw,"idcommande")>
												<cfset elem_idpool		= XmlElemNew(xmlrw,"idpool")>
											
												<cfset elem_idterminal.XmlText 	= myrefobject.IDTERMINAL>
												<cfset elem_idrevendeur.XmlText = myrefobject.IDREVENDEUR>
												<cfset elem_idoperateur.XmlText = myrefobject.IDOPERATEUR>
												<cfset elem_idcommande.XmlText 	= myrefobject.IDCOMMANDE>
												<cfset elem_idpool.XmlText 		= myrefobject.IDPOOLGEST>
												
												<cfset elem_equipement_sim.XmlChildren[11] = elem_idterminal>
												<cfset elem_equipement_sim.XmlChildren[12] = elem_idrevendeur>
												<cfset elem_equipement_sim.XmlChildren[13] = elem_idoperateur>
												<cfset elem_equipement_sim.XmlChildren[14] = elem_idcommande>
												<cfset elem_equipement_sim.XmlChildren[15] = elem_idpool>
							
											</cfif>
											
											<cfset xmlrw.row.XmlChildren[2] = elem_equipement_sim>
							
										</cfif>
							
									<cfelseif myrefobject.IDCARTESIM GT 0><!--- ********************************************************************************************************** --->
											
										<!---Create the FirstName and LastName elements--->
										<cfset elem_equipement_sim 		= XmlElemNew(xmlrw,"equipement")>
										<cfset elem_idequipement_sim 	= XmlElemNew(xmlrw,"idequipement")>
										<cfset elem_no_serie_sim 		= XmlElemNew(xmlrw,"no_serie")>
										<cfset elem_no_sim_sim 			= XmlElemNew(xmlrw,"no_sim")>
										<cfset elem_codepin_sim 		= XmlElemNew(xmlrw,"codepin")>
										<cfset elem_codepuk_sim 		= XmlElemNew(xmlrw,"codepuk")>
										<cfset elem_idsous_tete_sim 	= XmlElemNew(xmlrw,"idsous_tete")>
										<cfset elem_ligne_sim 			= XmlElemNew(xmlrw,"ligne")>
										<cfset elem_idst_sim 			= XmlElemNew(xmlrw,"idst")>
										<cfset elem_numidcmde_sim 		= XmlElemNew(xmlrw,"numidcmde")>
										<cfset elem_numidpool_sim 		= XmlElemNew(xmlrw,"numidpool")>
										
										<!---Set the text values of the  and LastName elements--->
										<cfset elem_idequipement_sim.XmlText = myrefobject.IDCARTESIM>
										<cfset elem_no_serie_sim.XmlText 	 = ''>
										<cfset elem_no_sim_sim.XmlText 		 = myrefobject.NUM_CSIM>
										<cfset elem_codepin_sim.XmlText 	 = ''>
										<cfset elem_codepuk_sim.XmlText 	 = myrefobject.NUM_CPUK>
										<cfset elem_idsous_tete_sim.XmlText  = myrefobject.IDSOUSTETE>
										<cfset elem_ligne_sim.XmlText 		 = myrefobject.SOUS__TETE>
										<cfset elem_idst_sim.XmlText 		 = myrefobject.OLDIDSSTETE>
										<cfset elem_numidcmde_sim.XmlText 	 = myrefobject.IDCOMMANDE>
										<cfset elem_numidpool_sim.XmlText 	 = myrefobject.IDPOOLGEST>
										
										<cfset elem_equipement_sim.XmlChildren[1] = elem_idequipement_sim>
										<cfset elem_equipement_sim.XmlChildren[2] = elem_no_serie_sim>
										<cfset elem_equipement_sim.XmlChildren[3] = elem_no_sim_sim>
										<cfset elem_equipement_sim.XmlChildren[4] = elem_codepin_sim>
										<cfset elem_equipement_sim.XmlChildren[5] = elem_codepuk_sim>
										<cfset elem_equipement_sim.XmlChildren[6] = elem_idsous_tete_sim>
										<cfset elem_equipement_sim.XmlChildren[7] = elem_ligne_sim>
										<cfset elem_equipement_sim.XmlChildren[8] = elem_idst_sim>
										<cfset elem_equipement_sim.XmlChildren[9] = elem_numidcmde_sim>
										<cfset elem_equipement_sim.XmlChildren[10] = elem_numidpool_sim>
									
										<cfset xmlrw.row.XmlChildren[1] = elem_equipement_sim>
											
									<cfelseif myrefobject.IDCARTESIM EQ -1><!--- ********************************************************************************************************** --->
									
											<!---Create the FirstName and LastName elements--->
											<cfset elem_equipement_sim 		= XmlElemNew(xmlrw,"equipement")>
											<cfset elem_idequipement_sim 	= XmlElemNew(xmlrw,"idequipement")>
											<cfset elem_no_serie_sim 		= XmlElemNew(xmlrw,"no_serie")>
											<cfset elem_no_sim_sim 			= XmlElemNew(xmlrw,"no_sim")>
											<cfset elem_codepin_sim 		= XmlElemNew(xmlrw,"codepin")>
											<cfset elem_codepuk_sim 		= XmlElemNew(xmlrw,"codepuk")>
											<cfset elem_idsous_tete_sim 	= XmlElemNew(xmlrw,"idsous_tete")>
											<cfset elem_ligne_sim 			= XmlElemNew(xmlrw,"ligne")>
											<cfset elem_idst_sim 			= XmlElemNew(xmlrw,"idst")>
											<cfset elem_idterminal 			= XmlElemNew(xmlrw,"idterminal")>
											<cfset elem_idrevendeur 		= XmlElemNew(xmlrw,"idrevendeur")>
											<cfset elem_idoperateur 		= XmlElemNew(xmlrw,"idoperateur")>
											<cfset elem_idcommande 			= XmlElemNew(xmlrw,"idcommande")>
											<cfset elem_idpool				= XmlElemNew(xmlrw,"idpool")>
											<cfset elem_numidcmde	 		= XmlElemNew(xmlrw,"numidcmde")>
											<cfset elem_numidpool	 		= XmlElemNew(xmlrw,"numidpool")>
											
											<!---Set the text values of the  and LastName elements--->
											<cfset elem_idequipement_sim.XmlText = myrefobject.IDCARTESIM>
											<cfset elem_no_serie_sim.XmlText 	 = ''>
											<cfset elem_no_sim_sim.XmlText 		 = myrefobject.NUM_CSIM>
											<cfset elem_codepin_sim.XmlText 	 = ''>
											<cfset elem_codepuk_sim.XmlText 	 = myrefobject.NUM_CPUK>
											<cfset elem_idsous_tete_sim.XmlText  = myrefobject.IDSOUSTETE>
											<cfset elem_ligne_sim.XmlText 		 = myrefobject.SOUS__TETE>
											<cfset elem_idst_sim.XmlText 		 = myrefobject.OLDIDSSTETE>
											<cfset elem_idterminal.XmlText 		 = myrefobject.IDTERMINAL>
											<cfset elem_idrevendeur.XmlText 	 = myrefobject.IDREVENDEUR>
											<cfset elem_idoperateur.XmlText 	 = myrefobject.IDOPERATEUR>
											<cfset elem_idcommande.XmlText 		 = myrefobject.IDCOMMANDE>
											<cfset elem_idpool.XmlText 			 = myrefobject.IDPOOLGEST>
											<cfset elem_numidcmde.XmlText 	 	 = myrefobject.IDCOMMANDE>
											<cfset elem_numidpool.XmlText 	 	 = myrefobject.IDPOOLGEST>
											
											<cfset elem_equipement_sim.XmlChildren[1]  = elem_idequipement_sim>
											<cfset elem_equipement_sim.XmlChildren[2]  = elem_no_serie_sim>
											<cfset elem_equipement_sim.XmlChildren[3]  = elem_no_sim_sim>
											<cfset elem_equipement_sim.XmlChildren[4]  = elem_codepin_sim>
											<cfset elem_equipement_sim.XmlChildren[5]  = elem_codepuk_sim>
											<cfset elem_equipement_sim.XmlChildren[6]  = elem_idsous_tete_sim>
											<cfset elem_equipement_sim.XmlChildren[7]  = elem_ligne_sim>
											<cfset elem_equipement_sim.XmlChildren[8]  = elem_idst_sim>
											<cfset elem_equipement_sim.XmlChildren[9]  = elem_idterminal>
											<cfset elem_equipement_sim.XmlChildren[10] = elem_idrevendeur>
											<cfset elem_equipement_sim.XmlChildren[11] = elem_idoperateur>
											<cfset elem_equipement_sim.XmlChildren[12] = elem_idcommande>
											<cfset elem_equipement_sim.XmlChildren[13] = elem_idpool>
											<cfset elem_equipement_sim.XmlChildren[14] = elem_numidcmde>
											<cfset elem_equipement_sim.XmlChildren[15] = elem_numidpool>
							
											<cfset xmlrw.row.XmlChildren[1] = elem_equipement_sim>
									
									</cfif>
									
									<cfset ArrayAppend(xmlreferences, xmlrw.row)>
									
								<cfelse><!--- POUR LE FIXE --->
				
									<!---Create the FirstName and LastName elements--->
									<cfset elem_equipement 		= XmlElemNew(xmlrw,"equipement")>
									<cfset elem_idequipement 	= XmlElemNew(xmlrw,"idequipement")>
									<cfset elem_no_serie 		= XmlElemNew(xmlrw,"no_serie")>
									<cfset elem_no_sim 			= XmlElemNew(xmlrw,"no_sim")>
									<cfset elem_codepin 		= XmlElemNew(xmlrw,"codepin")>
									<cfset elem_codepuk 		= XmlElemNew(xmlrw,"codepuk")>
									<cfset elem_idsous_tete 	= XmlElemNew(xmlrw,"idsous_tete")>
									<cfset elem_ligne 			= XmlElemNew(xmlrw,"ligne")>
									<cfset elem_idst 			= XmlElemNew(xmlrw,"idst")>
									<cfset elem_numidcmde		= XmlElemNew(xmlrw,"numidcmde")>
									<cfset elem_numidpool	 	= XmlElemNew(xmlrw,"numidpool")>
									
									<!---Set the text values of the  and LastName elements--->
									<cfset elem_idequipement.XmlText = myrefobject.IDTERMINAL>
									<cfset elem_no_serie.XmlText 	 = myrefobject.NUM_IMEI>
									<cfset elem_no_sim.XmlText 		 = ''>
									<cfset elem_codepin.XmlText 	 = ''>
									<cfset elem_codepuk.XmlText 	 = ''>
									<cfset elem_idsous_tete.XmlText  = myrefobject.IDSOUSTETE>
									<cfset elem_ligne.XmlText 		 = myrefobject.SOUS__TETE>
									<cfset elem_idst.XmlText 		 = myrefobject.OLDIDSSTETE>
									<cfset elem_numidcmde.XmlText 	 = myrefobject.IDCOMMANDE>
									<cfset elem_numidpool.XmlText 	 = myrefobject.IDPOOLGEST>
						
									<cfset elem_equipement.XmlChildren[1] = elem_idequipement>
									<cfset elem_equipement.XmlChildren[2] = elem_no_serie>
									<cfset elem_equipement.XmlChildren[3] = elem_no_sim>
									<cfset elem_equipement.XmlChildren[4] = elem_codepin>
									<cfset elem_equipement.XmlChildren[5] = elem_codepuk>
									<cfset elem_equipement.XmlChildren[6] = elem_idsous_tete>
									<cfset elem_equipement.XmlChildren[7] = elem_ligne>
									<cfset elem_equipement.XmlChildren[8] = elem_idst>
									<cfset elem_equipement.XmlChildren[9] = elem_numidcmde>
									<cfset elem_equipement.XmlChildren[10] = elem_numidpool>
								
									<cfset xmlrw.row.XmlChildren[1] = elem_equipement>
						
									<cfset ArrayAppend(xmlreferences, xmlrw.row)>
									
								</cfif>
						
							<cfelse>
							
								<cfset ArrayAppend(xmlerrorssstete, myrefobject)>
									
							</cfif>
						
						<cfelse>
						
							<cfset ArrayAppend(xmlerrorsformat, myrefobject)>
								
						</cfif>
						
					</cfloop>
					
					<cfset lenReferencesArticles = ArrayLen(xmlreferences)>
						
					<cfset referencesObject = createObject('component',"fr.consotel.consoview.M16.v2.EquipementService")>
						
					<cfloop index="idx3" from="1" to="#lenReferencesArticles#">
						
						<cfset nbrEqu = ArrayLen(xmlreferences[idx3].XmlChildren)>
							
						<cfif nbrEqu GT 0>
										
							<cfset myXMLReferences = toString(xmlreferences[idx3])>
				
							<cfset rsltReferenceQuery = referencesObject.manageLivraisonEquipements(myXMLReferences)>
							
							<cfif rsltReferenceQuery NEQ 1>
							
								<cfset ArrayAppend(errorqryinsert, xmlreferences[idx3])>
							
							<cfelseif rsltReferenceQuery EQ 1>
							
								<cfset ArrayAppend(validqryinsert, xmlreferences[idx3])>
							
							</cfif>
						
						</cfif>
				
					</cfloop>
					
					<cfif ArrayLen(errorqryinsert) EQ 0 AND ArrayLen(xmlerrorssstete) EQ 0 AND ArrayLen(xmlerrorsformat) EQ 0>
	
						<cftry>
						
							<!--- RECUPERER LES INFORMATIONS POUR LE SERVEUR DE MAILS --->
							<cfset properties 	 	= structNew()>
							<cfset codeappPropert	= structNew()>
							
							<cfset mailProperties  	= createObject("component","fr.consotel.consoview.M01.Mailing")>
							<cfset rsltProperties  	= mailProperties.get_appli_properties(SESSION.CODEAPPLICATION, 'NULL')>
							
							<cfloop query="rsltProperties">
								<cfset codeappPropert[rsltProperties.KEY] = rsltProperties.VALUE>
							</cfloop>
					
							<cfset properties = codeappPropert>
							
							<cfif NOT isDefined('properties.MAIL_SERVER')>
								<cfset properties.MAIL_SERVER = 'mail.consotel.fr'>
							</cfif>
							
							<cfif NOT isDefined('properties.MAIL_PORT')>
								<cfset properties.MAIL_PORT = 26>
							</cfif>
							
							<!--- 					
							
							subject, succescontent, errorcontent
							
							--->
								
							<cfset from 	= "importdemasse@consotel.fr">
							<cfset to		= SESSION.USER.EMAIL>
							
							<cfmail from="#from#" to="#to#" subject="#subject#" type="text/html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">		
								
								#succescontent#
								
							</cfmail>
						
						<cfcatch>
				
							<!--- EN CAS D'ÉCHEC DU THREAD --->						
							<cflog type="error" text="#CFCATCH.Message#">													
							
							<cfmail server="mail.consotel.fr" port="26" from="importdemasse@consotel.fr" to="postmaster@consotel.fr" 
									failto="dev@consotel.fr" bcc="dev@consotel.fr" subject="[WARN-THREAD]Erreur mail - succes [import de masse de références]" type="html">
									
								<cfoutput>
									
									Envoi de mail de l'import de masse en succes -> <br/><br/>
									Thread		: fr.consotel.consoview.importdemasse.ImportDeMasseService.cfc  -> majreferencesenmasse<br/>
									#CFCATCH.Message#<br/>
								
								</cfoutput>
									
							</cfmail>
							
						</cfcatch>					
						</cftry>
					
					<cfelse>

						<cfset importreferences 	= arrayNew(1)>
						<cfset rsltimportreferences = structNew()>
					
						<cfloop index="idx4" from="1" to="#ArrayLen(validqryinsert)#">
						
							<cfset nbrEqu = ArrayLen(validqryinsert[idx4].XmlChildren)>
							
							<cfif nbrEqu GT 0>
							
								<cfset myxlm = validqryinsert[idx4]>
								
								<cfset xmlcommandeid = 0>
								<cfset xmlpoolgestid = 0>
								<cfset xmlsousteteid = 0>
					
								<cfloop index="idx5" from="1" to="#nbrEqu#">
								
									<cfset rsltimportreferences = structNew()>
									
									<cfset xmlcommandeid = toString(myxlm.XmlChildren[idx5].numidcmde.xmlText)>
									<cfset xmlpoolgestid = toString(myxlm.XmlChildren[idx5].numidpool.xmlText)>
									<cfset xmlsousteteid = toString(myxlm.XmlChildren[idx5].idsous_tete.xmlText)>
					
								</cfloop>
								
								<cfset rsltimportreferences.RESULT 	= 'Ok'>
								<cfset rsltimportreferences.CAUSE  	= ''>
								<cfset rsltimportreferences.IDCMDE  = xmlcommandeid>
								<cfset rsltimportreferences.IDSSTT  = xmlsousteteid>
							
								<cfset ArrayAppend(importreferences, rsltimportreferences)>
								
							</cfif>
								
						</cfloop>
						
						<cfloop index="idx41" from="1" to="#ArrayLen(errorqryinsert)#">
						
							<cfset nbrEqu = ArrayLen(errorqryinsert[idx41].XmlChildren)>
							
							<cfif nbrEqu GT 0>
							
								<cfset myxlm = errorqryinsert[idx41]>
								
								<cfset xmlcommandeid = 0>
								<cfset xmlpoolgestid = 0>
								<cfset xmlsousteteid = 0>
											
								<cfloop index="idx51" from="1" to="#nbrEqu#">
					
									<cfset xmlcommandeid = toString(myxlm.XmlChildren[idx51].numidcmde.xmlText)>
									<cfset xmlpoolgestid = toString(myxlm.XmlChildren[idx51].numidpool.xmlText)>
									<cfset xmlsousteteid = toString(myxlm.XmlChildren[idx51].idsous_tete.xmlText)>
					
								</cfloop>
											
								<cfset rsltimportreferences 		= structNew()>
								<cfset rsltimportreferences.RESULT 	= 'Ko'>
								<cfset rsltimportreferences.CAUSE  	= 'Erreur de mise à jour'>
								<cfset rsltimportreferences.IDCMDE  = xmlcommandeid>
								<cfset rsltimportreferences.IDSSTT  = xmlsousteteid>
							
								<cfset ArrayAppend(importreferences, rsltimportreferences)>
								
							</cfif>
								
						</cfloop>
						
						<cfloop index="idx42" from="1" to="#ArrayLen(xmlerrorssstete)#">
					
							<cfset myxlm = xmlerrorssstete[idx42]>
					
							<cfset rsltimportreferences 		= structNew()>
							<cfset rsltimportreferences.RESULT 	= 'Ko'>
							<cfset rsltimportreferences.CAUSE  	= 'N° existant'>
							<cfset rsltimportreferences.IDCMDE  = myxlm.IDCOMMANDE>
							<cfset rsltimportreferences.IDSSTT  = myxlm.IDSOUSTETE>
									
							<cfset ArrayAppend(importreferences, rsltimportreferences)>
											
						</cfloop>
					
						<cfloop index="idx43" from="1" to="#ArrayLen(xmlerrorsformat)#">
					
							<cfset myxlm = xmlerrorsformat[idx43]>
							
							<cfset rsltimportreferences 		= structNew()>
							<cfset rsltimportreferences.RESULT 	= 'Ko'>
							<cfset rsltimportreferences.CAUSE  	= 'Format de n° non conforme'>
							<cfset rsltimportreferences.IDCMDE  = myxlm.IDCOMMANDE>
							<cfset rsltimportreferences.IDSSTT  = myxlm.IDSOUSTETE>
									
							<cfset ArrayAppend(importreferences, rsltimportreferences)>
											
						</cfloop>
					
						<cfspreadsheet action="read" query="qryOldInfos" src="#path#" sheetname="Sheet1"/>
					
						<cfset EtatArray = ArrayNew(1)>
						<cfset EtatArray[1] = "Etat">
						
						<cfset CauseArray = ArrayNew(1)>
						<cfset CauseArray[1] = "Cause">
							
						<!--- 
						
						// MOBILE
						
						COL_9 as IDCOMMANDE, COL_10 as IDSOUS_TETE
						
						// FIXE/DATA
						
						COL_5 as IDCOMMANDE, COL_6 as IDSOUS_TETE
						
						RESULT CAUSE IDCMDE IDSSTT
						
						 --->
					
						<cfloop query="qryOldInfos" startrow="2">
						
							<cfset crtrow = qryOldInfos.currentRow>
						
							<cfif mysegment EQ 1><!--- POUR LE MOBILE --->
						
								<cfset currentidcommande = qryOldInfos['COL_9'][crtrow]>
								<cfset currentidsoustete = qryOldInfos['COL_10'][crtrow]>
						
							<cfelseif mysegment GT 1><!--- POUR LE FIXE/DATA --->
						
								<cfset currentidcommande = qryOldInfos['COL_5'][crtrow]>
								<cfset currentidsoustete = qryOldInfos['COL_6'][crtrow]>
						
							</cfif>
							
							<cfset ispresent = 0>
							
							<cfset lenCurrentInfos = ArrayLen(EtatArray)+1>
									
							<cfloop index="idx6" from="1" to="#ArrayLen(importreferences)#">
							
								<cfset myrefObj = importreferences[idx6]>
					
								<cfif currentidcommande EQ myrefObj.IDCMDE AND currentidsoustete EQ myrefObj.IDSSTT>
										
									<cfset ispresent = 1>
									
									<cfset EtatArray[lenCurrentInfos]  = myrefObj.RESULT>
									<cfset CauseArray[lenCurrentInfos] = myrefObj.CAUSE>
									
								</cfif>
					
							</cfloop>
							
							<cfif ispresent EQ 0>
								
								<cfset EtatArray[lenCurrentInfos]  = 'OK'>
								<cfset CauseArray[lenCurrentInfos] = 'Non traité'>
							
							</cfif>
						
						</cfloop>
									
						<cfoutput><br><br></cfoutput>
						
						<cfif mysegment EQ 1><!--- POUR LE MOBILE --->
					
							<cfset nColumnNumber = QueryAddColumn(qryOldInfos, "COL_11", "VarChar", EtatArray)>
							<cfset nColumnNumber = QueryAddColumn(qryOldInfos, "COL_12", "VarChar", CauseArray)>
					
						<cfelseif mysegment GT 1><!--- POUR LE FIXE/DATA --->
					
							<cfset nColumnNumber = QueryAddColumn(qryOldInfos, "COL_7", "VarChar", EtatArray)>
							<cfset nColumnNumber = QueryAddColumn(qryOldInfos, "COL_8", "VarChar", CauseArray)>
					
						</cfif>
						
						<!--- MISE AJOUR ET TELECHARGEMENT DU FICHIER *.xls --->
						<cfset directory 	= "/container/importdemasse/">
						<cfset newuuid 		= createUUID()>
						<cfset newpath 		= #directory# & #newuuid#>
						<cfset newfile		= #newpath# & "/" & #filename#>
						<cfset newfile2		= #newpath# & "/" & #resultcontent# & "_" & #filename#>
					
						<!--- CREATION DU REPERTOIRE ET COPIE DU FICHIER *.XLS POUR POUVOIR ECRIRE DEDANS --->
						<cfif not DirectoryExists('#newpath#')>
					
							<cfdirectory action="Create" directory="#newpath#" type="dir" mode="777">
						
						<cfelse>
							
							<cfset newuuid = createUUID()>
							
							<cfset newpath = #directory# & #newuuid#>
							
							<cfset newfile  = #newpath# & "/" & #filename#>
							<cfset newfile2	= #newpath# & "/" & #resultcontent# & "_" & #filename#>
							
							<cfdirectory action="Create" directory="#newpath#" type="dir" mode="777">
						
						</cfif>
												
						<cfif DirectoryExists('#newpath#')>
															
							<cffile action="copy" source="#path#" 	 destination="#newfile#" mode="777">
							<cffile action="copy" source="#newfile#" destination="#newfile2#" mode="777">
										
						</cfif>	
					
						<cfset nurRowImpacted = qryOldInfos.recordCount+1>
					
					 	<cfspreadsheet action="write" query="qryOldInfos" filename="#newfile#" overwrite="true" sheetname="Sheet1"/>
						<cfspreadsheet action="read" name="finalspsheet" src="#newfile#" sheetname="Sheet1"/>
						
						<cfset SpreadsheetDeleteRow(finalspsheet, "1")>
						<cfset SpreadsheetShiftRows(finalspsheet, "2", #nurRowImpacted#, -1)>
						
						<cfset SpreadSheetWrite(finalspsheet, #newfile2#, "yes")>

						<cftry>
					
							<!--- RECUPERER LES INFORMATIONS POUR LE SERVEUR DE MAILS --->
							<cfset properties 	 	= structNew()>
							<cfset codeappPropert	= structNew()>
							
							<cfset mailProperties  	= createObject("component","fr.consotel.consoview.M01.Mailing")>
							<cfset rsltProperties  	= mailProperties.get_appli_properties(SESSION.CODEAPPLICATION, 'NULL')>
							
							<cfloop query="rsltProperties">
								<cfset codeappPropert[rsltProperties.KEY] = rsltProperties.VALUE>
							</cfloop>
					
							<cfset properties = codeappPropert>
							
							<cfif NOT isDefined('properties.MAIL_SERVER')>
								<cfset properties.MAIL_SERVER = 'mail.consotel.fr'>
							</cfif>
							
							<cfif NOT isDefined('properties.MAIL_PORT')>
								<cfset properties.MAIL_PORT = 26>
							</cfif>
							
							<!--- 					
							
							subject, succescontent, errorcontent
							
							--->
								
							<cfset from 	= "importdemasse@consotel.fr">
							<cfset to		= SESSION.USER.EMAIL>
							
							<cfmail from="#from#" to="#to#" subject="#subject#" type="text/html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">		
								
								#errorcontent#
								
								<cfmailparam file="#newfile2#">
							</cfmail>
						
						<cfcatch>
				
							<!--- EN CAS D'ÉCHEC DU THREAD --->						
							<cflog type="error" text="#CFCATCH.Message#">													
							
							<cfmail server="mail.consotel.fr" port="26" from="importdemasse@consotel.fr" to="postmaster@consotel.fr" 
									failto="dev@consotel.fr" bcc="dev@consotel.fr" subject="[WARN-THREAD]Erreur mail - erreur [import de masse de références]" type="html">
									
								<cfoutput>
									
									Envoi de mail de l'import de masse en erreur -> <br/><br/>
									Thread		: fr.consotel.consoview.importdemasse.ImportDeMasseService.cfc  -> majreferencesenmasse<br/>
									#CFCATCH.Message#<br/>
								
								</cfoutput>
									
							</cfmail>
							
						</cfcatch>					
						</cftry>

					</cfif>

				<cfcatch>
			
					<!--- EN CAS D'ÉCHEC DU THREAD --->						
					<cflog type="error" text="#CFCATCH.Message#">													
					
					<cfmail server="mail.consotel.fr" port="26" from="importdemasse@consotel.fr" to="postmaster@consotel.fr" 
							failto="dev@consotel.fr" bcc="dev@consotel.fr" subject="[WARN-THREAD]Erreur thread [import de masse de références]" type="html">
							
						<cfoutput>
							
							Tread en erreur -> <br/><br/>
							Thread		: fr.consotel.consoview.importdemasse.ImportDeMasseService.cfc  -> majreferencesenmasse<br/>
							#CFCATCH.Message#<br/>
							<cfdump var="#CFCATCH#">
						
						</cfoutput>
							
					</cfmail>
					
				</cfcatch>					
				</cftry>
				
			</cfthread>

		<cfreturn 1> 
	</cffunction>

</cfcomponent>