<cfcomponent>

	<cfset init()>
	

	<cffunction name="init" hint="initialisation" access="remote" returntype="any">
		<cfscript>
			dsn="ROCOFFRE";		
			userID="consoview";
			password="public";
			reporting=createObject("component","fr.consotel.consoview.api.Reporting");
			biServer 	= "http://bip-commande.consotel.fr/xmlpserver/services/PublicReportService?WSDL";
			locale = 'fr_FR';
			xdo_path='/CONSOTEL/Services/RAPPORTS_SPE/SNCF/GFC2/BDC_DD/BDC_DD.xdo';
			format = "excel";
			racine  = {		7348803		="GFC 2.0 MOVE",			
							7348803		="GFC 2.0 MOVE",
							7348805 	="GFC 2.0 NOMADE INFRA",
							7348806		="GFC 2.0 ITIF",
							7348807		="GFC 2.0 METIER TRACTION",
							7348808		="GFC 2.0 SUGE",
							7348809		="GFC 2.0 DCF",
							7387615 	="GFC 2.0 OCCAPI",
							7387475		="GFC 2.0 FORMATION",
							2458788		="_DEMO - CONSOTEL",
							5503190 	="0_DEMO_INTERNATIONAL",
							7348811		="GFC 2.0 DSMAT"
						};

		</cfscript>
			
	</cffunction>

	<cffunction name="build" access="remote" output="false" returntype="any" hint="Essai de générer l'attachement Digital Dimension">
		<cfargument name="infos" type="struct" required="true">
		<cftry>
			<cfreturn execute({idracine=infos.IDRACINE, 
									numCommande=infos.NUMCOMMANDE,
									idcommande=infos.IDCOMMANDE,
									UUID=infos.UUID,
									idGestionnaire=infos.IDGESTIONNAIRE})>
			<cfcatch>
				<cflog type="error" text="#CFCATCH.Message#">
				<cfmail server="mail-cv.consotel.fr" port="25" from="attachmentBuilder@saaswedo.com" to="monitoring@saaswedo.com"
					subject="[GFC2.0 BDC DD][GFC2-69] Erreur thread [AttachmentBuilder]" type="html">
					<cfoutput>
						Tread en erreur -> <br/><br/>
						Thread		: enregistrerCommande -> AttachmentBuilder<br/>
						Data : #SerializeJSON({idracine=infos.IDRACINE, 
									numCommande=infos.NUMCOMMANDE,
									idcommande=infos.IDCOMMANDE,
									UUID=infos.UUID,
									idGestionnaire=infos.IDGESTIONNAIRE})#<br/>
					</cfoutput>
				</cfmail>
			</cfcatch>			
		</cftry>
	</cffunction>

	<cffunction name="execute" access="remote" output="false" returntype="any" hint="Génère le rapport de commande DIGITAL - DIMENSION pour les racine GFC2 dans init()"> 
		<cfargument name="infoscommande" type="struct" required="true">
		<cfargument name="isattachment" type="numeric" required="false" default="1">

		<cfset var report = {}>
		<cfset report.stored = 0>

		<cfset init()>
		
		<cfif !structKeyExists(variables.racine, toString(arguments.infoscommande.idracine))>
			<cfreturn report/>
		</cfif>

		<cfset ArrayOfParamNameValues=ArrayNew(1)>
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="p_idcommande">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=arguments.infoscommande.idcommande>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[1]=ParamNameValue>

		<cfset myParamReportRequest={}>	
		<cfset myParamReportRequest.reportAbsolutePath=variables.xdo_path>
		<cfset myParamReportRequest.attributeTemplate="Default">		
		<cfset myParamReportRequest.attributeLocale=variables.locale>
		<cfset myParamReportRequest.attributeFormat=variables.format>
		<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
		<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
		<cfset myParamReportParameters=structNew()>
		<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
		<cfset myParamReportParameters.userID=variables.userID>
		<cfset myParamReportParameters.password=variables.password>

		<cfinvoke webservice="#variables.biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>

		<cfset report = {}>
		<cfset report.orderId = arguments.infoscommande.idcommande>
		<cfset report.orderNumber = arguments.infoscommande.numCommande>
		<cfset report.requestor = arguments.infoscommande.idgestionnaire>
		<cfset report.join = arguments.isattachment>
		<cfset report.UUID = arguments.infoscommande.UUID>
		<cfset report.id = resultRunReport.getReportFileID()>
		<cfset report.data = resultRunReport.getReportBytes()>
		<cfset report.extension = variables.reporting.getFormatFileExtension(variables.format)>
		<cfset report.contentType = resultRunReport.getReportContentType()>
		<cfset report.name = 'DD_' & report.orderNumber & report.extension >
		<cfset report.size = val(len(report.data))> <!--- octet --->
		<cfset report.result = resultRunReport> <!--- to delete --->
		<cfset report.stored = storeFile(report)>

		<cfreturn report>
	</cffunction>


	<cffunction name="storeFile" access="private" returntype="any" hint="Stock le fichier dans le container de la Commande en tant que pj">
		<cfargument name="report" type="struct" required="true">

		<cfset var _file = arguments.report>
		<cfset var destFolder 		= '/container/M16/' & #_file.UUID#>
		<cfset var destFile = destFolder & '/' & _file.name>
		<cfset var _status = 0>

		<cfif NOT DirectoryExists('#destFolder#')>
			<cfdirectory action="Create" directory="#destFolder#" type="dir" mode="777">
		</cfif>
		<cffile action="write" output="#_file.data#" file="#destFile#" mode="777">

		<cfstoredproc datasource="#variables.dsn#" procedure="pkg_m16.enregistrementlistattachments">
			<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_fileName"  	value="#_file.name#"/>
			<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_path"	  	value="#_file.UUID#"/>
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_userid" 	value="#_file.requestor#"/>
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_size" 		value="#_file.size#"/>
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_joinMail" 	value="#_file.join#"/>
			<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_format" 	value="#_file.extension#"/>
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" value="#_file.orderId#"/>
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
		</cfstoredproc>

		<cfset _status = p_retour>

		<cfreturn _status>
	</cffunction>

	<cffunction name="orderDetail" access="private" returntype="any" hint="Détail de la commande">
		<cfargument name="idcommade" type="numeric" required="true">
		<cfargument name="requestorid" type="numeric" required="true">

		<cfstoredproc datasource="#variables.dsn#" procedure="PKG_M16.fournirDetailOperation">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 	value="#arguments.idcommade#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idgestionnaire" value="#arguments.requestorid#"/>
			<cfprocresult name="qCommadne">
		</cfstoredproc>
		<cfreturn qCommadne>

	</cffunction>


</cfcomponent>