	<cfset biServer="http://bip-commande.consotel.fr/xmlpserver/services/PublicReportService_v11?WSDL"> <!--- THIS.BI_SERVICE_URL --->
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idgestionnaire">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.USER.CLIENTACCESSID>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idpool">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=FORM.IDPOOL>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[2]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idprofil_equipement">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDPROFIL>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[3]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idcategorie_equipement">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDCATEGORIE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[4]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idtype_equipement">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDTYPEEQUIPEMENT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[5]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idfournisseur">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDREVENDEUR>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[6]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idgamme_fournis">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDGAMME>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[7]=ParamNameValue>
		
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_chaine">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.CHAINE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[8]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idgroupe_client">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=SESSION.PERIMETRE.ID_GROUPE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[9]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_type_fournis">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDTYPEFOURNIS>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[10]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_niveau">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.NIVEAU>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[11]=ParamNameValue>
		
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_code_langue">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=SESSION.USER.GLOBALIZATION>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[12]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_segment">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDSEGMENT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[13]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_duree">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.DUREE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[14]=ParamNameValue>
	
	<cfset myParamReportRequest=structNew()>
	
	<cfset myParamReportRequest.reportAbsolutePath="/consoview/M16/exportcatalogueclient/exportcatalogueclient.xdo">	
	
	<cfset myParamReportRequest.attributeLocale= #SESSION.USER.GLOBALIZATION#>
	<cfset myParamReportRequest.attributeFormat=LCase(FORM.FORMAT)>
	<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
	<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
	<cfset myParamReportParameters=structNew()>
	<cfset structInsert(myParamReportParameters, "reportRequest", myParamReportRequest)>
	
	<cfset myParamReportParameters.userID="consoview">
	<cfset myParamReportParameters.password="public">
	
	<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
	<cfset reporting=createObject("component","fr.consotel.consoview.api.Reporting")>
	<cfset filename = "Liste_Equipements." & FORM.EXT>
			
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#" charset="utf-8">	
	
	<cfif compareNoCase(trim(FORM.FORMAT),"csv") eq 0>
		<cfcontent type="text/csv; charset=utf-8" variable="#resultRunReport.getReportBytes()#">
	<cfelse>
		<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">		
	</cfif>


