	<cfset biServer="http://developpement.consotel.fr/xmlpserver/services/PublicReportService?WSDL"> <!--- THIS.BI_SERVICE_URL --->
	
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idgestionnaires">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.USER.CLIENTACCESSID>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idpool">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=FORM.IDPOOL>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[2]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idprofil_equipement">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDPROFIL>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[3]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idcategorie_equipement">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDCATEGORIE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[4]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idtype_equipement">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDTYPEEQUIPEMENT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[5]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idfournisseur">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDREVENDEUR>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[6]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idgamme_fournis">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDGAMME>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[7]=ParamNameValue>
		
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_chaine">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.CHAINE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[8]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_idgroupe_client">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=SESSION.PERIMETRE.ID_GROUPE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[9]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_type_fournis">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDTYPEFOURNIS>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[10]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_niveau">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.NIVEAU>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[11]=ParamNameValue>
		
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_code_langue">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=SESSION.USER.GLOBALIZATION>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[12]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_segment">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDSEGMENT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[13]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="p_duree">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.DUREE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[14]=ParamNameValue>
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.SEARCHCATALOGUECLIENT_V4">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#SESSION.USER.CLIENTACCESSID#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#FORM.IDPOOL#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#FORM.IDPROFIL#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#FORM.IDCATEGORIE#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#FORM.IDTYPEEQUIPEMENT#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#FORM.IDREVENDEUR#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#FORM.IDGAMME#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 					value="#FORM.CHAINE#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#SESSION.PERIMETRE.ID_GROUPE#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournis" 			value="#FORM.IDTYPEFOURNIS#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_niveau" 					value="#FORM.NIVEAU#" null="true">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcode_langue"			value="#SESSION.USER.IDGLOBALIZATION#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 					value="#FORM.IDSEGMENT#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfdump var="#p_retour#">


