<cfcomponent output="false">
	
	<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
	<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
	<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
	<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
		
	<cfset VARIABLES["reportServiceClass"]="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService">
	<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
<!--- ***************************** CODE A MODIFIER ******************************* --->		
	<!--- Le chemin du rapport sous BIP --->
	<cfset VARIABLES["XDO"]="/consoview/gestion/flotte/ExportDetailCommande/ExportDetailCommande.xdo">
	<!--- Le code rapport --->
	<cfset VARIABLES["CODE_RAPPORT"]="ORDERS-LIST">
	<!--- TEMPLATE BI --->
	<cfset VARIABLES["TEMPLATE"]="Modele">
	<!--- OUTPUT --->
	<!---cfset VARIABLES["OUTPUT"]="Export_All_Orders_" --->
	<!--- Module --->
	<cfset VARIABLES["MODULE"]="M16">
	<!--- Format --->
	<cfset VARIABLES["FORMAT"]="CSV">
	<!--- Extension --->
	<cfset VARIABLES["EXT"]="CSV">
<!--- ************************************************************ --->
	
	<cfset reportService = createObject("component",VARIABLES["reportServiceClass"])>
	<cfset ApiE0 = CreateObject("component","fr.consotel.consoview.api.E0.ApiE0")>
	<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>

<cffunction name="runExport" access="remote" returnType="numeric">
	<cfargument name="idpool" type="numeric" required="true">
	<cfargument name="chosenEndMonth" type="String" required="true" >

	<cfset var mois_fin = #Arguments.chosenEndMonth#>
	<cfset var mois_debut = getbeginExport(Arguments.chosenEndMonth)>
	<cfset var report_name = setReportName(mois_debut, mois_fin)>
	<cfset VARIABLES["OUTPUT"]= #report_name#>
	<cfset initStatus=reportService.init(idRacine, VARIABLES["XDO"], "ConsoView", VARIABLES["MODULE"], VARIABLES["CODE_RAPPORT"])>
	
	<cfif initStatus EQ TRUE>
	
		<cfset setStatus=TRUE>
	<!--- G_IDRACINE --->
		<cfset tmpParamValues=arrayNew(1)>
		<cfset tmpParamValues[1]=idRacine>
		<cfset setStatus=setStatus AND reportService.setParameter("G_IDRACINE",tmpParamValues)>
	<!--- IDRACINE --->								
		<cfset tmpParamValues=arrayNew(1)>
		<cfset tmpParamValues[1]=idRacine>
		<cfset setStatus=setStatus AND reportService.setParameter("P_IDRACINE",tmpParamValues)>
	
	<!--- CODE_LANGUE --->
		<cfset tmpParamValues=arrayNew(1)>
		<cfset tmpParamValues[1]=mois_debut>
		<cfset setStatus=setStatus AND reportService.setParameter("P_MOIS_DEBUT",tmpParamValues)>

	<!--- CODE_LANGUE --->
		<cfset tmpParamValues=arrayNew(1)>
		<cfset tmpParamValues[1]=mois_fin>
		<cfset setStatus=setStatus AND reportService.setParameter("P_MOIS_FIN",tmpParamValues)>
	<!--- CODE_LANGUE --->
		<cfset tmpParamValues=arrayNew(1)>
		<cfset tmpParamValues[1]=codeLangue>
		<cfset setStatus=setStatus AND reportService.setParameter("P_CODE_LANGUE",tmpParamValues)>

	<!--- IDPOOL --->
		<cfset tmpParamValues=arrayNew(1)>
		<cfset tmpParamValues[1]=idpool>
		<cfset setStatus=setStatus AND reportService.setParameter("P_IDPOOL",tmpParamValues)>
	
		<cfset pretour = -1>
		<cfif setStatus EQ TRUE>
			<cfset FILENAME=VARIABLES["OUTPUT"]>
			<cfset outputStatus = FALSE>
			<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],VARIABLES["FORMAT"],VARIABLES["EXT"],FILENAME,VARIABLES["CODE_RAPPORT"])>
			<cfif outputStatus EQ TRUE>
				<cfset reportStatus = FALSE>
				<cfset reportStatus = reportService.runTask(idUser)>
				<cfset pretour = reportStatus['JOBID']>
				<cfif reportStatus['JOBID'] neq -1>
					<cfreturn reportStatus["JOBID"]>
				</cfif>
			</cfif>
		</cfif>	
	</cfif>
	
	<cfreturn pretour >
</cffunction>

<cffunction name="getbeginExport" acces= "private" returntype="String" hint="retourne la date il y a 13 mois">
	<cfargument name="chosenEndMonth" type="String" required="true" >

	<cfset var beginYear = LSParseNumber(Left(chosenEndMonth, 4)) -1>
	<cfset var beginMonth = LSParseNumber(Right(chosenEndMonth, 2)) -1>
	<cfif  beginMonth EQ 0>
		<cfset beginMonth = 1>
	</cfif>
	<cfset var beginExport = ToString(beginYear & "/" & NumberFormat(beginMonth , "0_"))>
	<cfreturn beginExport>
</cffunction>

<cffunction name = "setReportName" returnType = "String" access = "private">
	<cfargument name="mois_debut" type="String" required="true" >
	<cfargument name="mois_fin" type="String" required="true" >

	<cfset var myreport_name = Right(mois_debut, 2) & "-" & Left(mois_debut, 4) & "__" & Right(mois_fin, 2) & "-" & Left(mois_fin, 4)>
	<cfif codelangue EQ "fr_FR">
		<cfset myreport_name = "Export-commandes_" & #myreport_name#>
	<cfelseif codelangue EQ "es_ES"> 
		<cfset myreport_name = "Exportacion-pedido_" & #myreport_name#>
	<cfelse>
		<cfset myreport_name = "Export-orders_" & #myreport_name#>
	</cfif>

	<cfreturn myreport_name>

</cffunction>
	
</cfcomponent>