<cfcomponent output="false" displayname="ExportCommande">

	<!--- RECUPERATION DES DATA A EXPORTER --->
	
	<cffunction name="fournirDataSetToExport" access="remote" returntype="Query" output="false" hint="RECUPERATION DES DATA A EXPORTER">
		<cfargument name="listcommande" required="true" type="String"/>
				
			<cfset codelangue 	= SESSION.USER.GLOBALIZATION>
					
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_M16.getCommandItem_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_idcommande"  value="#listcommande#">	
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_code_langue" value="#codelangue#">		
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- RECUPERATION DES DATA A EXPORTER SUIVANT LES DATES PRESELECTIONNE --->
	
	<cffunction name="fournirAllDataSetToExport" access="remote" returntype="Query" output="false" hint="RECUPERATION DES DATA A EXPORTER SUIVANT LES DATES PRESELECTIONNE">
			<cfargument name="idpool" required="true" type="numeric"/>
			<cfset resltSet = -1>
			
			<cfset idracine 	= SESSION.PERIMETRE.ID_GROUPE>
			<cfset codelangue 	= SESSION.USER.GLOBALIZATION>

			<cfset beginYear = Year(Now()) - 1>
			<cfset beginMonth = Month(Now()) -1>
			<cfif  beginMonth EQ 0>
				<cfset beginMonth = 12>
			</cfif>
			<cfset beginDate = ToString(beginYear & "/" & beginMonth)>
			
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_M16.getCommandItem_v6">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine"    value="#idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_mois_debut"  value="#beginDate#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_mois_fin"    value="2050/12">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_code_langue" value="#codelangue#">	
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" value="#idpool#">	
					
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

</cfcomponent>