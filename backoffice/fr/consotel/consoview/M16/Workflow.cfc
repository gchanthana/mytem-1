<cfcomponent displayname="WorkFlow" output="false">

	<!--- FAIT UNE ACTION DE WORKFLOW POUR CHAQUE ARCTICLE DE LA COMMANDE --->

	<cffunction name="faireActionWorkFlow" access="remote" returntype="numeric" output="false" hint="FAIT UNE ACTION DE WORKFLOW POUR CHAQUE ARCTICLE DE LA COMMANDE" >
		<cfargument name="idAction" 		 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="dateAction" 		 required="true" type="string" 	default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="commentaireAction" required="true" type="string"  default="" displayname="struct commande" 	hint="Initial value for the commandeproperty." />
		<cfargument name="idOperation" 		 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="boolmail" 		 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="logmail" 		 	 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
			
			<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
			<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.faireActionDeWorkFlowMobile_v4">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idracine" 		value="#idRacine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_date_action" 	value="#dateAction#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_commentaire" 	value="#commentaireAction#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idgestionnaire" 	value="#idGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idoperation" 	value="#idOperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idaction" 		value="#idAction#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_bool_mail" 		value="#boolmail#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idcv_log_mail" 	value="#logmail#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

	<!--- COMPTEUR TIMER --->

	<cffunction name="timerDuration" access="remote" returntype="numeric" output="false" hint="COMPTEUR TIMER">
		<cfargument name="delayTimer" 	required="false" type="numeric" default="5" hint="Par defaut 5 = 10s " />
			
			<cfset sessionTimeout = delayTimer*1000000>
			<cfloop index="idx" from="1" to="#sessionTimeout#"></cfloop>
			
		<cfreturn 1>
	</cffunction>

</cfcomponent>