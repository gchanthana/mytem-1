<cfcomponent displayname="RLigneGateWay" output="false">

	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/21/2007
		
		Description : retourne un tableau de type fr.consotel.consoview.rechercheglobale.LigneObject[]
		
		param out 
			- un tableau d'objets de type fr.consotel.consoview.rechercheglobale.LigneObject
	--->
	<cffunction name="getResult" access="public" output="true" returntype="fr.consotel.consoview.rechercheglobale.LigneObject[]">
		<cfargument name="chaine" required="true" type="string">
		<cfargument name="idgroupe" required="false" type="numeric" >
		
		<cfset var qRead="">
		<cfset var obj="">
		<cfset var ret=arrayNew(1)>
		
		<!--- <cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select *
			from 
		</cfquery> --->
		
		<cfset qRead = getData(chaine)>
		 
		
		<cfloop query="qRead">			
			
			<cfscript>				
				obj = createObject("component", "LigneObject").init(qRead);				
				ArrayAppend(ret, obj);
			</cfscript>
			
		</cfloop>
		<cfreturn ret>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/22/2007
		
		Description : Enregistre le recherche sous forme de groupe de ligne
					  avec TYPE_ORGA = SAVE et TYPE_DECOUPAGE = 2 (GroupeLigne)
		param in
			- fr.consotel.consoview.rechercheglobale.RechObject
		param out 
			- l'identifiant du groupe qui vient d'etre créé pour ok sinon -1
	--->
	<cffunction name="saveResult" access="remote" output="false" returntype="string">
		<cfargument name="obj" type="RechObject" required="true" />
 		<cfreturn obj.save()/>
	</cffunction>
	
	<cffunction name="deleteRech" access="remote" output="false" returntype="numeric">
		<cfargument name="obj" type="RechObject" required="true" />
 		<cfreturn obj.delete()/>
	</cffunction>
	
	<cffunction name="getData" returntype="query" access="private">
		<cfargument name="chaine" required="true" type="string">
		<cfargument name="idGroupeClient" required="false" type="numeric" >
		
		<cfset idgroupe = session.perimetre.ID_PERIMETRE>		
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_SEARCH_V3.GLOBALE_SEARCH" blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idgroupe#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocresult name="qRead">
		</cfstoredproc> 
			
		<cfreturn qRead> 
	</cffunction>
</cfcomponent>