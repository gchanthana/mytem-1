<cfheader name="Cache-Control" value="no-cache, no-store, must-revalidate"/>
<cfheader name="Pragma" value="no-cache"/>
<cfheader name="Expires" value="0"/>

<cfparam type="string" name="FORM.ENABLE">
<cfparam type="string" name="FORM.UUID">
<cfparam type="string" name="FORM.IDRACINE">

<cfset VARIABLES.ERROR = "ERROR">
<cfset VARIABLES.SUCCESS = "SUCCESS">


<cfset VARIABLES.meassage = {}>
<cfset VARIABLES.meassage.STATUS = VARIABLES.SUCCESS>
<cfset VARIABLES.meassage.TEXT = "OK">
<cfset VARIABLES.meassage.CODE = "">
<cfset VARIABLES.meassage.CATCHED_ERROR = "">


<cfif StructKeyExists(FORM,"IDRACINE")>
	<cfset idGroupe=val(FORM.IDRACINE)>
<cfelse>
	<cfset VARIABLES.meassage.STATUS = VARIABLES.ERROR>
	<cfset VARIABLES.meassage.TEXT = "the parameter IDRACINE is compulsory" >
	<cfset VARIABLES.meassage.CODE = "001" >
</cfif>

<cfif StructKeyExists(FORM,"UUID") and CompareNoCase(VARIABLES.meassage.STATUS,VARIABLES.SUCCESS) eq 0>
	<cfset phoneNumber=trim(FORM.UUID)>
<cfelse>
	<cfset VARIABLES.meassage.STATUS = VARIABLES.ERROR>
	<cfset VARIABLES.meassage.TEXT = "the parameter UUID is compulsory" >
	<cfset VARIABLES.meassage.CODE = "002" >
</cfif>

<cfif StructKeyExists(FORM,"ENABLE") and CompareNoCase(VARIABLES.meassage.STATUS,VARIABLES.SUCCESS) eq 0>
	<cfif (CompareNoCase(trim(FORM.ENABLE),"TRUE") eq 0) or (CompareNoCase(trim(FORM.ENABLE),"FALSE") eq 0)>		
		<cfset enableRoaming=trim(FORM.ENABLE)>
	<cfelse>
		<cfset VARIABLES.meassage.STATUS = VARIABLES.ERROR>
		<cfset VARIABLES.meassage.TEXT = "the value of parameter ENABLE must be TRUE or FALSE" >
		<cfset VARIABLES.meassage.CODE = "003">
	</cfif>	
<cfelse>
	<cfset VARIABLES.meassage.STATUS = VARIABLES.ERROR>
	<cfset VARIABLES.meassage.TEXT = "the parameter ENABLE is compulsory" >
	<cfset VARIABLES.meassage.CODE = "004">
</cfif>








<cfif CompareNoCase(VARIABLES.meassage.STATUS,VARIABLES.SUCCESS) eq 0>

	<cfset VARIABLES.mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
	<cfset VARIABLES.mLog.setIDRACINE(FORM.IDRACINE)>	
	<cfset VARIABLES.mLog.setIDMNT_CAT(21)>
	<cfset VARIABLES.mLog.setIDMNT_ID(221)>
	<cfset VARIABLES.mLog.setSUBJECTID("UPATE_ROMING #ENABLE#: #UUID#")>
	<!--- EXECUTION --->

	<cflog type="information" text="[DATALERT] - UPADTING DATA ROAMING ON ENABLE=#ENABLE#  UUID=#UUID# IDRACINE=#IDRACINE#" >
	
	<cftry>
		
		<cfset mdm=createObject("component","fr.saaswedo.api.tests.mdm.POC_StopRoaming")>
		<cfset enableRoaming=FORM.ENABLE>
		<cfset phoneNumber = trim(FORM.UUID)>
		<cfset mdm.updateRoaming(idGroupe,phoneNumber,enableRoaming)>	
		<cfset VARIABLES.meassage.STATUS = VARIABLES.SUCCESS>

	<cfcatch type="any">
		<cflog type="error" text="[DATALERT] - UPADTING DATA ROAMING ON ENABLE=#ENABLE#  UUID=#UUID# IDRACINE=#IDRACINE#" >
		<cfset VARIABLES.meassage.STATUS = VARIABLES.ERROR>
		<cfset VARIABLES.meassage.TEXT = "an unexpected error occurs while trying to complete the operation : #cfcatch.message#" >
		<cfset VARIABLES.meassage.CODE = "005">
		<cfset VARIABLES.meassage.CATCHED_ERROR = cfcatch>
		
	</cfcatch>
	
	</cftry>

</cfif>
	



<!--- MLOG --->
<cftry>
	<cfsavecontent variable="body">					
		<cfoutput>Backoffice : #CGI.SERVER_NAME#</br></cfoutput>
		<cfoutput><b>[#VARIABLES.meassage.STATUS#]</b> Trying to upadte data roaming on <b>ENABLE=#ENABLE#  UUID=#UUID# IDRACINE=#IDRACINE#</b></cfoutput></br>
		<cfdump var="#VARIABLES#">
	</cfsavecontent>
	
	<cfset mLog.setBODY(body)>

	<cfinvoke
		component="fr.consotel.api.monitoring.MLogger"
		method="logIt"  returnVariable = "result">
		<cfinvokeargument name="mLog" value="#mLog#">
	</cfinvoke>	

	<cfcatch type="any">
		<cfmail from="MLog <monitoring@saaswedo.com>" 
				to="monitoring@saaswedo.com" 
				server="mail-cv.consotel.fr" 
				port="25" type="text/html"
				subject="test log monitor">
					<cfdump var="#cfcatch#">						
		</cfmail>
	</cfcatch>
</cftry>	

<cfif CompareNoCase(VARIABLES.meassage.STATUS,VARIABLES.SUCCESS) eq 0>
<cfoutput>OK</cfoutput>
<cfelse>
	<cfthrow errorcode="#VARIABLES.meassage.CODE#" message="#VARIABLES.meassage.TEXT#" >
</cfif>