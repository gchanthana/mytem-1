<cfcomponent output="false" displayName="fr.consotel.consoview.M26.srvices.mdm.DataTrafficManager">

	<cffunction name="enableDataTraffic" access="remote" returntype="any"> 
		<cfargument name="ar_ligne" type="any" required="true">
			
		<cfthread name="t1" action="run" ar_ligne="#ar_ligne#">
			<cfset _len = ArrayLen(ar_ligne)>
			<cfset idGroupe=SESSION.PERIMETRE.ID_GROUPE>
			<cfset mdm=createObject("component","fr.saaswedo.api.tests.mdm.POC_StopRoaming")>
			<cfset enableRoaming=TRUE>	
			<cfloop from=1 to=#_len# index="i">
				<cfset phoneNumber = trim(ar_ligne[i]['UUID'])>
				<cfset mdm.updateRoaming(idGroupe,phoneNumber,enableRoaming)>
			</cfloop>
		</cfthread>
		
		<cfreturn 1>
	</cffunction>
	
	<cffunction name="disableDataTraffic" access="remote" returntype="numeric"> 
		<cfargument name="ar_ligne" type="any" required="true">
		
		<cfthread name="t2" action="run" ar_ligne="#ar_ligne#">
			<cfset _len = ArrayLen(ar_ligne)>
			<cfset idGroupe=SESSION.PERIMETRE.ID_GROUPE>
			<cfset mdm=createObject("component","fr.saaswedo.api.tests.mdm.POC_StopRoaming")>
			<cfset enableRoaming=FALSE>	
			<cfloop from=1 to=#_len# index="i">
				<cfset phoneNumber = trim(ar_ligne[i]['UUID'])>
				<cfset mdm.updateRoaming(idGroupe,phoneNumber,enableRoaming)>
			</cfloop>
		</cfthread>
		
		<cfreturn 1>
	</cffunction>

</cfcomponent> 

