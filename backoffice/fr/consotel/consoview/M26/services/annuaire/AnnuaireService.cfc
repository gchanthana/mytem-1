<cfcomponent output="false" displayName="fr.consotel.consoview.M26.srvices.mdm.DataTrafficManager">

	<cfset variables.failedCount = 0>
	<cfset variables.okCount = 0>
	<cfset variables.failed=ArrayNew(1)>
	
	<cffunction name="changeCount" access="public" returntype="any">
		<cfargument name="okToAdd" type="numeric" required="true">
		<cfargument name="failedToAdd" type="numeric" required="true">
		<cfargument name="invalidJSON" type="string" required="true"> 
		<cflock timeout="10">
			<cfset variables.okCount = variables.okCount + okToAdd>
			<cfset variables.failedCount = variables.failedCount + failedToAdd>
			<cfif failedToAdd EQ 1>
				<cfset variables.failed[failedCount] = invalidJSON>
			</cfif>
		</cflock>
		
		<cfreturn TRUE>
	</cffunction>
	
	<cffunction name="addItems" access="remote" returntype="any"> 
		<cfargument name="annuaireItms" type="array" required="true">
		<cfargument name="fake" type="any" required="false">
		 
		<cfset idGroupe = SESSION.PERIMETRE.ID_GROUPE>
		<cfset numThread = 0>
		<cfloop array="#annuaireItms#" index="item">
			
			<cfthread action="run" priority="normal" name="thread#numThread#" item="#item#" numThread="#numThread#" idGroupe="#idGroupe#">
				<cfstoredproc datasource="ROCOFFRE" procedure="pkg_M26.import_annuaire">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idGroupe#">
					<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#item#">
					<cfprocparam type="Out" cfsqltype="cf_sql_integer" variable="qResult">
				</cfstoredproc>
				
				<cfif qResult GT 0>
					<cfset this.changeCount(1, 0, "")>
					<!---<cfset message = StructNew()>
					<cfset message.body="Fin du thread #numThread# : OK">
					<cfset message.destination="ColdFusionGateway">
					<cfset message.headers=StructNew()>
					<cfset SendGatewayMessage("MessagingGateway", message)>--->
					<cfelse>
						<cfset this.changeCount(0, 1, item)>
						<!---<cfset message = StructNew()>
						<cfset message.body="Fin du thread #numThread# : FAILED #item#">
						<cfset message.destination="ColdFusionGateway">
						<cfset message.headers=StructNew()>
						<cfset SendGatewayMessage("MessagingGateway", message)>--->
				</cfif>
			</cfthread>
			<cfset numThread = numThread + 1>
		</cfloop>
		
		<cfset threadnames="thread0">
		<cfloop index="i" from="1" to="#numThread - 1#">
			<cfset threadnames = threadnames & ", thread" & ToString(i)>
		</cfloop>
		 <cfthread action="join" name="#threadnames#"/>
			<cfset message = StructNew()>
			<cfset message.body="AnnuaireService termine " & ToString(okCount) & " sur " & ToString(numThread)>
			<cfloop index="j" from="1" to="#failedCount#">
				<cfset message.body=message.body & "|" & failed[j]>
			</cfloop>
		<cfreturn message>
			<!---<cfset message.destination="ColdFusionGateway">
			<cfset message.headers=StructNew()>
			<cfset SendGatewayMessage("MessagingGateway", message)>
		<cfreturn TRUE>--->
	</cffunction>
	
	<cffunction name="getAreasCode" access="remote" returntype="any" description="Retourne la liste des indicatifs (affich�e dans la cr�ation de lignes)">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.getAreasCode">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.IDGLOBALIZATION#">
			
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
</cfcomponent> 

