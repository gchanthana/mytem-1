<cfcomponent displayname="fr.consotel.consoview.M26.RapportFlux">

	<cfset VARIABLES["reportServiceClass"]="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService">
	<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
<!--- ***************************** CODE A MODIFIER ******************************* --->		
	<!--- Le chemin du rapport sous BIP --->
	<cfset VARIABLES["XDO"]="/consoview/M26/DATALERT-FLUX-CONSO/DATALERT-FLUX-CONSO/DATALERT-FLUX-CONSO.xdo">
	<!--- Le code rapport --->
	<cfset VARIABLES["CODE_RAPPORT"]="DATALERT-FLUX-CONSO">
	<!--- TEMPLATE BI --->
	<cfset VARIABLES["TEMPLATE"]="Default">
	<!--- OUTPUT --->
	<cfset VARIABLES["OUTPUT"]="DATALERT-FLUX-CONSO">
	<!--- Le Module --->
	<cfset VARIABLES["MODULE"]="M26">
<!--- ************************************************************ --->
			
	<cfset reportService=createObject("component",VARIABLES["reportServiceClass"])>	 
	<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>


<cffunction name="scheduleRepport" access="public" returntype="any" >	
	<cfargument name="dateDebut" type="date" required="true">
	<cfargument name="dateFin" type="date" required="true">
	<cfargument name="parametresRechercheVO" type="struct" required="true">
	
	<cfset idracine_master = session.perimetre.idracine_master>
	<cfset idracine = session.perimetre.id_groupe>
	
	
	<cfset TAB_SEARCH = ListToArray(parametresRechercheVO.SEARCH_TEXT, ',', true)>
	<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
	<cfset SEARCH_TEXT = TAB_SEARCH[2]>
	
	<cfset ext="CSV">
	<cfset format="CSV">
	<cfset setStatus=TRUE>
	
	<cfset db = LSDateFormat(dateDebut,'YYYY-MM-DD')> <!--- Format Date Début--->
	<cfset df = LSDateFormat(dateFin,'YYYY-MM-DD')> <!--- Format Fin--->
	
	<cflog text="#VARIABLES['OUTPUT']#-#db#-#df#.csv">
	
	<cfset VARIABLES["OUTPUT"]=VARIABLES["OUTPUT"] & '-' & db & '-' & df >
	
	<cfset db = LSDateFormat(dateDebut,'YYYY/MM/DD')> <!--- Format Date Début--->
	<!--- Ajout de 1 jour à la date de fin --->	
	<cfset _datefin = DateAdd("d",1,dateFin)>
	<cfset df = LSDateFormat(_datefin,'YYYY/MM/DD')> <!--- Format Fin--->
	
	<cfset initStatus=reportService.init(idracine,VARIABLES["XDO"],"mytem",VARIABLES["MODULE"],VARIABLES["CODE_RAPPORT"])>
	
	<!--- PARAMETRES RECUPERES EN SESSION --->
		<!--- p_idracine_master --->
			<!---<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=idracine_master>
			<cfset setStatus=setStatus AND reportService.setParameter("p_idracine_master",tmpParamValues)>--->
		
		<!--- g_idracine --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=idracine>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDRACINE",tmpParamValues)>
			
		<!--- p_idracine --->			
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=idracine>
			<cfset setStatus=setStatus AND reportService.setParameter("p_idracine",tmpParamValues)>
		<!--- PARAMETRES --->
		<!--- p_date_debut --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=db>
			<cfset setStatus=setStatus AND reportService.setParameter("p_date_debut", tmpParamValues)>
		<!--- p_date_fin --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=df>
			<cfset setStatus=setStatus AND reportService.setParameter("p_date_fin", tmpParamValues)>
		<!--- p_search_key --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SEARCH_TEXT>
			<cfset setStatus=setStatus AND reportService.setParameter("p_search_key", tmpParamValues)>
		<!--- p_search_column --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SEARCH_TEXT_COLONNE>
			<cfset setStatus=setStatus AND reportService.setParameter("p_search_column", tmpParamValues)>
		<!--- p_days_wo_conn --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=parametresRechercheVO.WITHOUTCONNEXION>
			<cfset setStatus=setStatus AND reportService.setParameter("p_days_wo_conn", tmpParamValues)>
		<!--- p_with_alert --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=parametresRechercheVO.WITHALERT>
			<cfset setStatus=setStatus AND reportService.setParameter("p_with_alert", tmpParamValues)>
		<!--- p_alert_on --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=parametresRechercheVO.ONALERT>
			<cfset setStatus=setStatus AND reportService.setParameter("p_alert_on", tmpParamValues)>
		<!--- p_roaming_date --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=parametresRechercheVO.ONROAMINGDATE>
			<cfset setStatus=setStatus AND reportService.setParameter("p_roaming_date", tmpParamValues)>
		<!--- p_roaming_on --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=parametresRechercheVO.ONROAMING>
			<cfset setStatus=setStatus AND reportService.setParameter("p_roaming_on", tmpParamValues)>
					
		<cfset pretour = -1>
		<cfif setStatus EQ TRUE>
			<cfset FILENAME=VARIABLES["OUTPUT"]>
			<cfset outputStatus = FALSE>
			<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],format,ext,FILENAME,VARIABLES["CODE_RAPPORT"])>
			<cfif outputStatus EQ TRUE>
				<cfset reportStatus = FALSE>
				<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
				<cfset pretour = reportStatus['JOBID']>
				<cfif reportStatus['JOBID'] neq -1>		
					<cfreturn reportStatus["JOBID"]>
				</cfif>
			</cfif>
		</cfif>		
	
	<cfreturn pretour >
	
</cffunction>

</cfcomponent>