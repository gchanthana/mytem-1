<cfset biServer="sun-bi.consotel.fr">
<cfset publicReportService=createObject("webservice",APPLICATION.BI_SERVICE_URL)>
<!--- Authentification --->
<cfset validateLoginReturn=publicReportService.validateLogin("cvuser","consotel09")>
<!--- Report Access --->
<!--- 
<cfset reportAbsolutePath="/LABS/QUICKWIN/BurstTest/BurstTest.xdo">
 --->
<cfset reportAbsolutePath="/LABS/QUICKWIN/RationalisationRacine/RationalisationRacine.xdo">
<cfset hasReportAccessReturn=publicReportService.hasReportAccess(reportAbsolutePath,"cvuser","consotel09")>
<!--- ScheduleRequest --->
<cfset scheduleRequest=structNew()>
<cfset scheduleRequest["scheduleBurstringOption"]=TRUE>
<cfset scheduleRequest["userJobName"]="BURSTING-USING-WEBSERVICES">
<!--- Report Request --->
<cfset reportRequest=structNew()>
<cfset reportRequest["reportAbsolutePath"]=reportAbsolutePath>
<cfset reportRequest["attributeFormat"]="EXCEL">
<cfset reportRequest["attributeLocale"]="fr-FR">
<cfset reportRequest["attributeTemplate"]="default">
<!--- Report Parameter Name Values --->
<cfset operateurid=structNew()>
<cfset operateurid["name"]="P_OPERATEURID">
<cfset tmpValues=arrayNew(1)>
<cfset tmpValues[1]="533">
<cfset tmpValues[2]="512">
<cfset operateurid["values"]=tmpValues>
<cfset operateurid["multiValuesAllowed"]=TRUE>
<!--- Report Parameters Array --->
<cfset tmpParamArray=arrayNew(1)>
<cfset tmpParamArray[1]=operateurid>
<!--- Set parameterNameValues to ReportRequest --->
<cfset reportRequest["parameterNameValues"]=tmpParamArray>
<cfdump var="#reportRequest#">
<!--- Set ReportRequest to ScheduleRequest --->
<cfset scheduleRequest["reportRequest"]=reportRequest>
<!--- Local Delivery Option --->
<cfset deliveryOutputPath="/opt/oracle/home/OBIEE/QUICKWIN/LABS/">
<cfset localDeliveryOption=structNew()>
<cfset localDeliveryOption["destination"]=deliveryOutputPath & "output.xls">
<!--- Set Delivery Request to ScheduleRequest --->
<cfset deliveryRequest=structNew()>
<cfset deliveryRequest["localOption"]=localDeliveryOption>
<cfset scheduleRequest["deliveryRequest"]=deliveryRequest>
<cfdump var="#scheduleRequest#">
<!--- Running - Schedule Report ---> 
<cfset scheduleReportReturn=publicReportService.scheduleReport(scheduleRequest,"cvuser","consotel09")>
<cfdump var="#scheduleReportReturn#">
 

