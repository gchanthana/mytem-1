<cfcomponent name="SansConsoReport" alias="fr.consotel.consoview.bi.report.sansconso.SansConsoReport">
	<cffunction name="createReportInfos" access="public" returntype="struct">
		<cfargument name="qwInfos" required="true" type="struct">
		<cfset sansConsoInfos=arguments.qwInfos["SANS_CONSO"]>
		<cfset newSansConsosReportInfos=structNew()>
		<cfset newSansConsosReportInfos["ABSOLUTE_PATH"]="/LABS/QUICKWIN/NdiSansConsoRacine/NdiSansConsoRacine.xdo">
		<cfset newSansConsosReportInfos["TEMPLATE"]="default">
		<cfset newSansConsosReportInfos["OUTPUT_DIRECTORY"]="/opt/oracle/home/OBIEE/QUICKWIN/LABS/" & arguments.qwInfos["IDRACINE"] & "/">
		<!--- NE SONT PAS UTILISES 
		<cfset sansConsoDateDebString=parseDateTime(sansConsoInfos["DATEDEB"],"yyyy/mm/dd")>
		<cfset sansConsoDateFinString=parseDateTime(sansConsoInfos["DATEFIN"],"yyyy/mm/dd")>
		 --->
		<!--- 5- est la position de la méthodo des lignes sans conso --->
		<cfset newSansConsosReportInfos["OUTPUT_FILENAME"]="04-detail lignes sans conso">
		<cfset newSansConsosReportInfos["OUTPUT_FORMAT"]="EXCEL">
		<cfset newSansConsosReportInfos["BURST_OPTION"]="FALSE">
		<cfset newSansConsosReportInfos["JOB_NAME"]=arguments.qwInfos["IDRACINE"] & "-SANS-CONSO-QWREPORT">
		<!--- REPORT PARAMETERS --->
		<cfset i=1>
		<cfset arrayOfParamNameValues=arrayNew(1)>
		<cfset P_IDRACINE_MASTER=structNew()>
		<cfset P_IDRACINE_MASTER["name"]="P_IDRACINE_MASTER">
		<cfset P_IDRACINE_MASTER["values"]=[arguments.qwInfos["IDRACINE_MASTER"]]>
		<cfset P_IDRACINE_MASTER["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_IDRACINE_MASTER>
		<cfset i=i+1>
		<cfset P_IDRACINE=structNew()>
		<cfset P_IDRACINE["name"]="P_IDRACINE">
		<cfset P_IDRACINE["values"]=[arguments.qwInfos["IDRACINE"]]>
		<cfset P_IDRACINE["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_IDRACINE>
		<cfset i=i+1>
		<cfset P_DATEDEB=structNew()>
		<cfset P_DATEDEB["name"]="P_DATEDEB">
		<cfset P_DATEDEB["values"]=[sansConsoInfos["DATEDEB"]]>
		<cfset P_DATEDEB["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_DATEDEB>
		<cfset i=i+1>
		<cfset P_DATEFIN=structNew()>
		<cfset P_DATEFIN["name"]="P_DATEFIN">
		<cfset P_DATEFIN["values"]=[sansConsoInfos["DATEFIN"]]>
		<cfset P_DATEFIN["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_DATEFIN>
		<cfset i=i+1>
		<cfset P_PRODUIT_ACCES=structNew()>
		<cfset P_PRODUIT_ACCES["name"]="P_PRODUIT_ACCES">
		<cfset P_PRODUIT_ACCES["values"]=[sansConsoInfos["PRODUIT_ACCES"]]>
		<cfset P_PRODUIT_ACCES["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_PRODUIT_ACCES>
		<cfset i=i+1>
		<cfset P_MOBILE_INCLUS=structNew()>
		<cfset P_MOBILE_INCLUS["name"]="P_MOBILE_INCLUS">
		<cfset P_MOBILE_INCLUS["values"]=[sansConsoInfos["MOBILE_INCLUS"]]>
		<cfset P_MOBILE_INCLUS["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_MOBILE_INCLUS>
		
		<cfset newSansConsosReportInfos["ARRAY_OF_PARAMETERS"]=arrayOfParamNameValues>
		<cfreturn newSansConsosReportInfos>
	</cffunction>
</cfcomponent>
