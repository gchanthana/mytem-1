<cfcomponent name="QWReport" alias="fr.consotel.bi.report.quickwin.QWReport">
	<cffunction name="scheduleReport" access="public" returntype="struct">
		<cfargument name="reportParametersStruct" required="true" type="struct">
		<!--- ERROR CASE --->
		<cfset errorStruct=structNew()>
		<cfset errorStruct["ERROR"]=-1>
		<!--- ******************* --->
		<cfset reportOutputPath="/opt/oracle/home/OBIEE/QUICKWIN/LABS/">
		<cfset deploiementReportPath="/LABS/QUICKWIN/DeploiementRacine/DeploiementRacine.xdo">
		<cfset rationalisationReportPath="/LABS/QUICKWIN/RationalisationRacine/RationalisationRacine.xdo">
		<cfset ndiSansConsoReportPath="/LABS/QUICKWIN/NdiSansConsoRacine/NdiSansConsoRacine.xdo">
		
		<cfset publisherService=createObject("component","fr.consotel.bi.service.PublisherService")>
		<cfset authResult=publisherService.authService("ConsoTel")>
		<cfset jobIdStruct=structNew()>
		<cfset jobPerimetreKey="IDRACINE">
		<cfset structInsert(jobIdStruct,reportParametersStruct[jobPerimetreKey],arrayNew(1))>
		<!--- ******************** REPORT INFOS : Deploiement ******************** --->
		<cfset i=1>
		<cfset reportParameters=extractReportParameters(reportParametersStruct["QWREPORT"])>
		<cfset reportParameters["REPORT_PATH"]=deploiementReportPath>
		<cfset reportParameters["DELIVERY_FILE_PATH"]=reportOutputPath & "DEPLOIEMENT.xls">
		<cfset jobId=publisherService.scheduleService("DEPLOIEMENT-" & reportParametersStruct[jobPerimetreKey],reportParameters,FALSE)>
		<cfif jobId GT 0>
			<cfset jobIdStruct[reportParametersStruct[jobPerimetreKey]][i]=jobId>
		<cfelse>
			<cfset errorStruct["ERROR_DESC"]="QWReport.scheduleReport() : ERROR - DEPLOIEMENT">
			<cfreturn errorStruct>
		</cfif>
		<!--- ******************************************************************** --->
		<!--- ******************** REPORT INFOS : Rationalisation ******************** --->
		<cfset i=i+1>
		<cfset reportParameters2=extractReportParameters(reportParametersStruct["QWREPORT"])>
		<cfset reportParameters2["REPORT_PATH"]=rationalisationReportPath>
		<cfset reportParameters2["DELIVERY_FILE_PATH"]=reportOutputPath & "RATIONALISATION.xls">
		<cfset jobId=publisherService.scheduleService("RATIONALISATION-" & reportParametersStruct[jobPerimetreKey],reportParameters2,TRUE)>
		<cfif jobId GT 0>
			<cfset jobIdStruct[reportParametersStruct[jobPerimetreKey]][i]=jobId>
		<cfelse>
			<cfset errorStruct["ERROR_DESC"]="QWReport.scheduleReport() : ERROR - RATIONALISATION">
			<cfreturn errorStruct>
		</cfif>
		<!--- ******************************************************************** --->
		<!--- ******************** REPORT INFOS : Ligne Sans Consommations ******************** --->
		<cfset i=i + 1>
		<cfset reportParameters=extractReportParameters(reportParametersStruct["QWREPORT"])>
		<cfset reportParameters["REPORT_PATH"]=ndiSansConsoReportPath>
		<cfset reportParameters["DELIVERY_FILE_PATH"]=reportOutputPath & "LIGNESANSCONSO.xls">
		<cfset jobId=publisherService.scheduleService("SANS-CONSOS-" & reportParametersStruct[jobPerimetreKey],reportParameters,FALSE)>
		<cfif jobId GT 0>
			<cfset jobIdStruct[reportParametersStruct[jobPerimetreKey]][i]=jobId>
		<cfelse>
			<cfset errorStruct["ERROR_DESC"]="QWReport.scheduleReport() : ERROR - NDISANSCONSO">
			<cfreturn errorStruct>
		</cfif>
		<!--- ******************************************************************** --->
		<cfreturn jobIdStruct>
	</cffunction>
	
	<cffunction name="extractReportParameters" access="private" returntype="struct" hint="Does not extract or update REPORT_PATH and DELIVERY_FILE_PATH">
		<cfargument name="parametersStruct" required="true" type="struct">
		<cfset reportParameters=structNew()>
		<cfset reportParameters["FORMAT"]="EXCEL">
		<cfset reportParameters["TEMPLATE"]="default">
		<cfset paramNamesValues=arrayNew(1)>
		<cfset index=1>
		<!--- ************ PARAMETERS ************ --->
		<cfset paramNamesValues[index]=structNew()>
		<cfset paramNamesValues[index]["name"]="P_IDRACINE_MASTER">
		<cfset paramNamesValues[index]["multiValuesAllowed"]=FALSE>
		<cfset currentParamKey="IDRACINE_MASTER">
		<cfif structKeyExists(parametersStruct,currentParamKey) EQ FALSE>
			<cfabort showerror="Paramètre manquant : #currentParamKey#">
		</cfif>
		<cfset paramNamesValues[index]["values"]=parametersStruct[currentParamKey]>
		<!--- ****************************************************** --->
		<cfset index=index + 1>
		<cfset paramNamesValues[index]=structNew()>
		<cfset paramNamesValues[index]["name"]="P_IDGROUPE_CLIENT">
		<cfset paramNamesValues[index]["multiValuesAllowed"]=FALSE>
		<cfset currentParamKey="IDGROUPE_CLIENT">
		<cfif structKeyExists(parametersStruct,currentParamKey) EQ FALSE>
			<cfabort showerror="Paramètre manquant : #currentParamKey#">
		</cfif>
		<cfset paramNamesValues[index]["values"]=parametersStruct[currentParamKey]>
		<!--- ****************************************************** --->
		<cfset index=index + 1>
		<cfset paramNamesValues[index]=structNew()>
		<cfset paramNamesValues[index]["name"]="P_DATEDEB">
		<cfset paramNamesValues[index]["multiValuesAllowed"]=FALSE>
		<cfset currentParamKey="MOISDEB">
		<cfif structKeyExists(parametersStruct,currentParamKey) EQ FALSE>
			<cfabort showerror="Paramètre manquant : #currentParamKey#">
		</cfif>
		<cfset paramNamesValues[index]["values"]=parametersStruct[currentParamKey]>
		<!--- ****************************************************** --->
		<cfset index=index + 1>
		<cfset paramNamesValues[index]=structNew()>
		<cfset paramNamesValues[index]["name"]="P_DATEFIN">
		<cfset paramNamesValues[index]["multiValuesAllowed"]=FALSE>
		<cfset currentParamKey="MOISFIN">
		<cfif structKeyExists(parametersStruct,currentParamKey) EQ FALSE>
			<cfabort showerror="Paramètre manquant : #currentParamKey#">
		</cfif>
		<cfset paramNamesValues[index]["values"]=parametersStruct[currentParamKey]>
		<!--- ****************************************************** --->
		<cfset index=index + 1>
		<cfset paramNamesValues[index]=structNew()>
		<cfset paramNamesValues[index]["name"]="P_PERIODICITE">
		<cfset paramNamesValues[index]["multiValuesAllowed"]=FALSE>
		<cfset currentParamKey="PERIODICITE">
		<cfif structKeyExists(parametersStruct,currentParamKey) EQ FALSE>
			<cfabort showerror="Paramètre manquant : #currentParamKey#">
		</cfif>
		<cfset paramNamesValues[index]["values"]=parametersStruct[currentParamKey]>
		<!--- ****************************************************** --->
		<cfset index=index + 1>
		<cfset paramNamesValues[index]=structNew()>
		<cfset paramNamesValues[index]["name"]="P_CONSO_FT_MIN">
		<cfset paramNamesValues[index]["multiValuesAllowed"]=FALSE>
		<cfset currentParamKey="CONSO_FT_MIN">
		<cfif structKeyExists(parametersStruct,currentParamKey) EQ FALSE>
			<cfabort showerror="Paramètre manquant : #currentParamKey#">
		</cfif>
		<cfset paramNamesValues[index]["values"]=parametersStruct[currentParamKey]>
		<!--- ****************************************************** --->
		<cfset index=index + 1>
		<cfset paramNamesValues[index]=structNew()>
		<cfset paramNamesValues[index]["name"]="P_PRODUIT_ACCES">
		<cfset paramNamesValues[index]["multiValuesAllowed"]=FALSE>
		<cfset currentParamKey="BOOL_PRODUIT_ACCES">
		<cfif structKeyExists(parametersStruct,currentParamKey) EQ FALSE>
			<cfabort showerror="Paramètre manquant : #currentParamKey#">
		</cfif>
		<cfset paramNamesValues[index]["values"]=parametersStruct[currentParamKey]>
		<!--- ****************************************************** --->
		<cfset index=index + 1>
		<cfset paramNamesValues[index]=structNew()>
		<cfset paramNamesValues[index]["name"]="P_IDGROUPE_PRODUIT">
		<cfset paramNamesValues[index]["multiValuesAllowed"]=FALSE>
		<cfset currentParamKey="IDGROUPE_PRODUIT">
		<cfif structKeyExists(parametersStruct,currentParamKey) EQ FALSE>
			<cfabort showerror="Paramètre manquant : #currentParamKey#">
		</cfif>
		<cfset paramNamesValues[index]["values"]=parametersStruct[currentParamKey]>
		<!--- ****************************************************** --->
		
		<!--- ****************************************************** --->
		<cfset index=index + 1>
		<cfset paramNamesValues[index]=structNew()>
		<cfset paramNamesValues[index]["name"]="P_OPERATEURID">
		<cfset paramNamesValues[index]["multiValuesAllowed"]=TRUE>
		<cfset currentParamKey="OPERATEURID">
		<cfif structKeyExists(parametersStruct,currentParamKey) EQ FALSE>
			<cfabort showerror="Paramètre manquant : #currentParamKey#">
		</cfif>
		<cfset paramNamesValues[index]["values"]=parametersStruct[currentParamKey]>
		<!--- ****************************************************** --->
		
		<cfset reportParameters["PARAMETERS"]=paramNamesValues>
		<cfreturn reportParameters>
	</cffunction>
</cfcomponent>
