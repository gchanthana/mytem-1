<cfcomponent name="RatioGroupeProduitReport" alias="fr.consotel.consoview.bi.report.RatioGroupeProduitReport">
	<cffunction name="createReportInfos" access="public" returntype="struct">
		<cfargument name="qwInfos" required="true" type="struct">
		<cfset ratioGpInfos=arguments.qwInfos["RATIO_GPRODUITS"]>
		<cfset newRatioGpReportInfos=structNew()>
		<cfset newRatioGpReportInfos["ABSOLUTE_PATH"]="/LABS/QUICKWIN/RationalisationRacine/RationalisationRacine.xdo">
		<cfset newRatioGpReportInfos["TEMPLATE"]="default">
		<cfset newRatioGpReportInfos["OUTPUT_DIRECTORY"]="/opt/oracle/home/OBIEE/QUICKWIN/LABS/" & arguments.qwInfos["IDRACINE"] & "/">
		<!--- NE SONT PAS UTILISES 
		<cfset ratioGpDateDebString=parseDateTime(ratioGpInfos["DATEDEB"],"yyyy/mm/dd")>
		<cfset ratioGpDateFinString=parseDateTime(ratioGpInfos["DATEFIN"],"yyyy/mm/dd")>
		 --->
		<!--- Bursted Report (Output Filename is defined in bursting parameters) --->
		<cfset newRatioGpReportInfos["OUTPUT_FILENAME"]="detail ratio gp">
		<cfset newRatioGpReportInfos["OUTPUT_FORMAT"]="EXCEL">
		<cfset newRatioGpReportInfos["BURST_OPTION"]="TRUE">
		<cfset newRatioGpReportInfos["JOB_NAME"]=arguments.qwInfos["IDRACINE"] & "-RATIO-GP-QWREPORT">
		<!--- REPORT PARAMETERS --->
		<cfset i=1>
		<cfset arrayOfParamNameValues=arrayNew(1)>
		<cfset P_IDRACINE_MASTER=structNew()>
		<cfset P_IDRACINE_MASTER["name"]="P_IDRACINE_MASTER">
		<cfset P_IDRACINE_MASTER["values"]=[arguments.qwInfos["IDRACINE_MASTER"]]>
		<cfset P_IDRACINE_MASTER["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_IDRACINE_MASTER>
		<cfset i=i+1>
		<cfset P_IDRACINE=structNew()>
		<cfset P_IDRACINE["name"]="P_IDRACINE">
		<cfset P_IDRACINE["values"]=[arguments.qwInfos["IDRACINE"]]>
		<cfset P_IDRACINE["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_IDRACINE>
		<cfset i=i+1>
		<cfset P_DATEDEB=structNew()>
		<cfset P_DATEDEB["name"]="P_DATEDEB">
		<cfset P_DATEDEB["values"]=[ratioGpInfos["DATEDEB"]]>
		<cfset P_DATEDEB["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_DATEDEB>
		<cfset i=i+1>
		<cfset P_DATEFIN=structNew()>
		<cfset P_DATEFIN["name"]="P_DATEFIN">
		<cfset P_DATEFIN["values"]=[ratioGpInfos["DATEFIN"]]>
		<cfset P_DATEFIN["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_DATEFIN>
		<cfset i=i+1>
		<cfset P_CATEGORIE_GP=structNew()>
		<cfset P_CATEGORIE_GP["name"]="P_CATEGORIE_GP">
		<cfset P_CATEGORIE_GP["values"]=[ratioGpInfos["CATEGORIE_GP"]]>
		<cfset P_CATEGORIE_GP["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_CATEGORIE_GP>
		<cfset i=i+1>
		<cfset P_IDGROUPE_PRODUIT=structNew()>
		<cfset P_IDGROUPE_PRODUIT["name"]="P_IDGROUPE_PRODUIT">
		<cfset P_IDGROUPE_PRODUIT["values"]=[ratioGpInfos["IDGROUPE_PRODUIT"]]>
		<cfset P_IDGROUPE_PRODUIT["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_IDGROUPE_PRODUIT>
		
		<cfset newRatioGpReportInfos["ARRAY_OF_PARAMETERS"]=arrayOfParamNameValues>
		<cfreturn newRatioGpReportInfos>
	</cffunction>
</cfcomponent>
