 
<cfset qwReport=createObject("component","fr.consotel.consoview.bi.report.quickwin.QWReport")>
<cfset reportParametersStruct=structNew()>
<cfset jobPerimetreKey="IDRACINE">
<cfset reportParametersStruct["IDRACINE"]=477>
<cfset reportParametersStruct["QWREPORT"]=structNew()>

<cfset structInsert(reportParametersStruct["QWREPORT"],"IDRACINE_MASTER",arrayNew(1))>
<cfset reportParametersStruct["QWREPORT"]["IDRACINE_MASTER"][1]=477>

<cfset structInsert(reportParametersStruct["QWREPORT"],"IDGROUPE_CLIENT",arrayNew(1))>
<cfset reportParametersStruct["QWREPORT"]["IDGROUPE_CLIENT"][1]=477>

<cfset structInsert(reportParametersStruct["QWREPORT"],"MOISDEB",arrayNew(1))>
<cfset reportParametersStruct["QWREPORT"]["MOISDEB"][1]="2008/10/01">

<cfset structInsert(reportParametersStruct["QWREPORT"],"MOISFIN",arrayNew(1))>
<cfset reportParametersStruct["QWREPORT"]["MOISFIN"][1]="2008/12/31">

<cfset structInsert(reportParametersStruct["QWREPORT"],"PERIODICITE",arrayNew(1))>
<cfset reportParametersStruct["QWREPORT"]["PERIODICITE"][1]=1>

<cfset structInsert(reportParametersStruct["QWREPORT"],"CONSO_FT_MIN",arrayNew(1))>
<cfset reportParametersStruct["QWREPORT"]["CONSO_FT_MIN"][1]=0>

<cfset structInsert(reportParametersStruct["QWREPORT"],"BOOL_PRODUIT_ACCES",arrayNew(1))>
<cfset reportParametersStruct["QWREPORT"]["BOOL_PRODUIT_ACCES"][1]=1>

<cfset structInsert(reportParametersStruct["QWREPORT"],"IDGROUPE_PRODUIT",arrayNew(1))>
<cfset reportParametersStruct["QWREPORT"]["IDGROUPE_PRODUIT"][1]=4332>

<cfset structInsert(reportParametersStruct["QWREPORT"],"OPERATEURID",arrayNew(1))>
<cfset reportParametersStruct["QWREPORT"]["OPERATEURID"][1]=63>

<cfdump var="#reportParametersStruct#" label="Parameters"><br>
<cfset jobStruct=qwReport.scheduleReport(reportParametersStruct)>
<cfdump var="#jobStruct#" label="Job Struct">


<cfquery name="qGetJobStatus" datasource="DW">
	select job_id, created, last_updated, status, output_id
	from   xmlp_sched_output
	where  job_id in (#arrayToList(jobStruct["477"])#)
</cfquery>

<cfdump var="#qGetJobStatus#" label="Job Status"><br>

<cfset bipService=createObject("component","fr.consotel.consoview.bi.service.PublisherService")>
<cfset scheduledReportInfos=bipService.getScheduledReportInfos(jobStruct["477"][1])>

<cfdump var="#scheduledReportInfos#" label="Scheduled Report Infos"><br>

<cfquery name="qGetJobStatus" datasource="DW">
	select job_id, created, last_updated, status, output_id
	from   xmlp_sched_output
	where  job_id in (#arrayToList(jobStruct["477"])#)
</cfquery>

<cfdump var="#qGetJobStatus#" label="Job Status 2"><br>
