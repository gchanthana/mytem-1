<cfheader name="Content-Disposition" value="inline;filename=Liste_appels_#replace(form.libelle,' ','_')#.xls">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="iso-8859-1"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <LastAuthor>CONSOTEL</LastAuthor>
  <Created>2006-02-27T14:44:16Z</Created>
  <Version>11.6360</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s22">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Helvetica" ss:Size="11" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s23">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Helvetica" ss:Size="11" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s24">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Helvetica" ss:Size="8" ss:Color="#000000"/>
  </Style>
	<Style ss:ID="s211" ss:Name="Euro">
		<Font ss:FontName="Helvetica" ss:Size="8" ss:Color="#000000"/>
   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s16" ss:Parent="s211">
   <NumberFormat ss:Format="_-* #,##0\ _?_-;\-* #,##0\ _?_-;_-* &quot;-&quot;??\ _?_-;_-@_-"/>
  </Style>
 </Styles>
<cfset i=1>
<cfset j=1>
 <Worksheet ss:Name="<cfoutput>#left(label,31)#</cfoutput>">
  <Table x:FullColumns="1"
   x:FullRows="1" ss:DefaultColumnWidth="60">
	<Column ss:Width="100"/>
	<Column ss:Width="100"/>
	<Column ss:Width="180"/>
	<Column ss:Width="100"/>
	<Column ss:Width="100"/>
	<Column ss:Width="100"/>

	<Row ss:AutoFitHeight="0" ss:Height="12.9375">
	<Cell ss:StyleID="s24"><Data ss:Type="String"></Data></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">Date dï¿½but</Data></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String"><cfoutput>#form.datedeb#</cfoutput></Data></Cell>
	<Cell ss:StyleID="s24"><Data ss:Type="String">Date fin</Data></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String"><cfoutput>#form.datefin#</cfoutput></Data></Cell>
   </Row>
	
	<Row ss:AutoFitHeight="0" ss:Height="15">
   </Row>

   <Row ss:AutoFitHeight="0" ss:Height="15">
    <Cell ss:StyleID="s22"><Data ss:Type="String">Ligne</Data></Cell>
	<Cell ss:StyleID="s22"><Data ss:Type="String">Poste</Data></Cell>
	<Cell ss:StyleID="s22"><Data ss:Type="String">Pays</Data></Cell>
	<Cell ss:StyleID="s23"><Data ss:Type="String">Nombre</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">Durï¿½e</Data></Cell>
	<Cell ss:StyleID="s23"><Data ss:Type="String">Coï¿½t</Data></Cell>
   </Row>
<cfoutput query="qGetGrid">
<cfset a=j>

<cfif i mod 50000 eq 0>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
     x:Right="0.78740157499999996" x:Top="0.984251969"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <HorizontalResolution>300</HorizontalResolution>
    <VerticalResolution>300</VerticalResolution>
    <NumberofCopies>0</NumberofCopies>
   </Print>
   <Selected/>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
<Worksheet ss:Name="#left(label,evaluate(30-len(j)))#_#j#">
  <Table x:FullColumns="1"
   x:FullRows="1" ss:DefaultColumnWidth="60">
	<Column ss:Width="100"/>
	<Column ss:Width="100"/>
	<Column ss:Width="180"/>
	<Column ss:Width="100"/>
	<Column ss:Width="100"/>
	<Column ss:Width="100"/>
   <Row ss:AutoFitHeight="0" ss:Height="15">
	<Cell ss:StyleID="s22"><Data ss:Type="String">Ligne</Data></Cell>
	<Cell ss:StyleID="s22"><Data ss:Type="String">Poste</Data></Cell>
    <Cell ss:StyleID="s22"><Data ss:Type="String">Pays</Data></Cell>
	<Cell ss:StyleID="s23"><Data ss:Type="String">Nombre</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">Durï¿½e</Data></Cell>
	<Cell ss:StyleID="s23"><Data ss:Type="String">Coï¿½t</Data></Cell>
   </Row>
<Row ss:AutoFitHeight="0" ss:Height="12.9375">
	<Cell ss:StyleID="s24"><Data ss:Type="String">#cli#</Data></Cell>
	<Cell ss:StyleID="s24"><Data ss:Type="String">#sda#</Data></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">#Pays#</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="Number">#nombre#</Data></Cell>
	<Cell ss:StyleID="s16"><Data ss:Type="Number">#duree#</Data></Cell>
	<Cell ss:StyleID="s211"><Data ss:Type="Number">#COUT#</Data></Cell>
   </Row>
	<cfset j=j+1>
<cfelse>
<Row ss:AutoFitHeight="0" ss:Height="12.9375">
	<Cell ss:StyleID="s24"><Data ss:Type="String">#cli#</Data></Cell>
	<Cell ss:StyleID="s24"><Data ss:Type="String">#sda#</Data></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">#Pays#</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="Number">#nombre#</Data></Cell>
	<Cell ss:StyleID="s16"><Data ss:Type="Number">#duree#</Data></Cell>
	<Cell ss:StyleID="s211"><Data ss:Type="Number">#COUT#</Data></Cell>
   </Row>
</cfif>
<cfset i=i+1>
</cfoutput>
</Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
     x:Right="0.78740157499999996" x:Top="0.984251969"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <HorizontalResolution>300</HorizontalResolution>
    <VerticalResolution>300</VerticalResolution>
    <NumberofCopies>0</NumberofCopies>
   </Print>
   <Selected/>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
