<cfif (UCase(FORM.TB) EQ "COMPLET") OR (UCase(FORM.TB) EQ "FIXE") OR (UCase(FORM.TB) EQ "MOBILE") OR (UCase(FORM.TB) EQ "DATA")>
	<cfset biServer="http://bip-prod.consotel.fr/xmlpserver/services/PublicReportService?WSDL">
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDRACINE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE.ID_GROUPE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDPERIMETRE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=FORM.IDENTIFIANT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[2]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_DATEDEB">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=right(FORM.DATEDEB,4) & "/" & mid(FORM.DATEDEB,4,2) & "/" & left(FORM.DATEDEB,2)>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[3]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_DATEFIN">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=right(FORM.DATEFIN,4) & "/" & mid(FORM.DATEFIN,4,2) & "/" & left(FORM.DATEFIN,2)>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[4]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_TB">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=FORM.TB>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[5]=ParamNameValue>
	
	<cfset myParamReportRequest=structNew()>
	<cfset myParamReportRequest.reportAbsolutePath="/~consoview/M31/TB-" & UCase(FORM.PERIMETRE) & "-REPORT/TB-" & UCase(FORM.PERIMETRE) & "-REPORT.xdo">
	<cfset myParamReportRequest.attributeTemplate="COMPLET">
	<cfif NOT ((UCase(FORM.TB) EQ "COMPLET"))>
		<cfset myParamReportRequest.attributeTemplate="SEGMENT">
	</cfif>
	<cfset myParamReportRequest.attributeLocale="fr-FR">
	<cfset myParamReportRequest.attributeFormat=LCase(FORM.FORMAT)>
	<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
	<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
	<cfset myParamReportParameters=structNew()>
	<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
	
	<cfset myParamReportParameters.userID="consoview">
	<cfset myParamReportParameters.password="public">
	<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
	<cfset reporting=createObject("component","fr.consotel.api.Reporting")>
	<cfset filename = "TB-" & UCase(FORM.TB) & "-" & FORM.PERIMETRE & "-" &
			replace(session.perimetre.raison_sociale,' ','_',"all") & reporting.getFormatFileExtension(FORM.FORMAT)>
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#">
	<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">
<cfelse>
	<cfif (UCASE(format) eq "EXCEL") OR (UCASE(format)eq "CSV")>
		<cfset obj = CreateObject("component","fr.consotel.consoview.tb.accueil.facade")>	
		<cfif UCASE(tb) eq "COMPLET">
			<cfset resultseta=obj.getAbo()>
			<cfset resultsetAbo=obj.getSurthemeAbos()>
			<cfset resultsetc=obj.getConso()>
			<cfset resultsetConso=obj.getSurThemeConsos()>
			<cfdump var="#resultseta#" label="resultseta" expand="false">
			<cfdump var="#resultsetAbo#" label="resultsetAbo" expand="false">
			<cfdump var="#resultsetc#" label="resultsetc" expand="false">
			<cfdump var="#resultsetConso#" label="resultsetConso" expand="false">		
		<cfelse>
			<cfset resultsetAbo=obj.getAboBySegment(tb)>
			<cfset resultsetConso=obj.getConsoBySegment(tb)>
		</cfif>
		<cfset nom = session.user.nom>
		<cfset prenom = session.user.prenom>
		<cfset compteurFixe=0>
		<cfset compteurGSM=0>
		<cfset compteurData=0>
		<cfset maxCompteur=Max(resultsetAbo.recordcount,resultsetconso.recordcount)>
		<cfif resultsetAbo.recordcount gt 0 AND resultsetconso.recordcount gt 0>
			<cfif resultsetAbo.recordcount gt resultsetconso.recordcount>
				<cfset bq=resultsetAbo>
				<cfset sq=resultsetconso>
			<cfelseif resultsetconso.recordcount gt resultsetAbo.recordcount>
				<cfset bq=resultsetconso>
				<cfset sq=resultsetAbo>
			<cfelse>
				<cfset bq=resultsetconso>
				<cfset sq=resultsetAbo>
			</cfif>
			<cfset a=QueryNew(bq.columnlist)>
			<cfset b=QueryNew(bq.columnlist)>
			<cfset compteurmin=1>
			<cfloop from="1" to="#maxCompteur#" index="i">
				<cfset QueryAddRow(a)>
				<cfset QueryAddRow(b)>
				<cfif (bq['segment_theme'][i] eq 'Fixe' AND sq['segment_theme'][compteurmin] eq 'Fixe') OR
						(bq['segment_theme'][i] eq 'Mobile' AND sq['segment_theme'][compteurmin] eq 'Mobile') OR
						(bq['segment_theme'][i] eq 'Data' AND sq['segment_theme'][compteurmin] eq 'Data')>
					<cfloop from="1" to="#ListLen(a.columnlist)#" index="j">
						<cfset QuerySetCell(a,ListGetAt(a.columnlist,j),sq['#ListGetAt(a.columnlist,j)#'][compteurmin])>
						<cfset QuerySetCell(b,ListGetAt(a.columnlist,j),bq['#ListGetAt(a.columnlist,j)#'][i])>
					</cfloop>
					<cfset compteurmin=compteurmin+1>
				<cfelseif (bq['segment_theme'][i] eq 'Fixe' AND sq['segment_theme'][compteurmin] neq 'Fixe') OR
							(bq['segment_theme'][i] eq 'Mobile' AND sq['segment_theme'][compteurmin] neq 'Mobile') OR
							(bq['segment_theme'][i] eq 'Data' AND sq['segment_theme'][compteurmin] neq 'Data')>
					<cfloop from="1" to="#ListLen(a.columnlist)#" index="j">
						<cfset QuerySetCell(a,ListGetAt(a.columnlist,j),"")>
						<cfset QuerySetCell(b,ListGetAt(a.columnlist,j),bq['#ListGetAt(a.columnlist,j)#'][i])>
					</cfloop>
				<cfelse>
					<cfloop from="1" to="#ListLen(a.columnlist)#" index="j">
						<cfset QuerySetCell(b,ListGetAt(a.columnlist,j),bq['#ListGetAt(a.columnlist,j)#'][i])>
					</cfloop>
				</cfif>
				<cfif bq['segment_theme'][i] eq 'Fixe'>
					<cfset compteurFixe=compteurFixe+1>
				</cfif>
				<cfif bq['segment_theme'][i] eq 'Mobile'>
					<cfset compteurGSM=compteurGSM+1>
				</cfif>
				<cfif bq['segment_theme'][i] eq 'Data'>
					<cfset compteurData=compteurData+1>
				</cfif>
			</cfloop>
			<cfif resultsetAbo.recordcount gt resultsetconso.recordcount>
				<cfset resultsetAbo=b>
				<cfset resultsetconso=a>
			<cfelseif resultsetconso.recordcount gt resultsetAbo.recordcount>
				<cfset resultsetconso=b>
				<cfset resultsetAbo=a>
			</cfif>
		</cfif> 
		<cfif resultsetAbo.recordcount neq 0>
			<cfif format eq "excel">
				<cfcontent type="application/vnd.ms-excel">	
				<cfheader name="Content-Disposition" value="inline;filename=tb.xls">
				<cfif tb eq "complet">
					<cfinclude template="/fr/consotel/consoview/cfm/report/tbaccueil_complet_xls.cfm">
				<cfelse>
					<cfinclude template="/fr/consotel/consoview/cfm/report/tbaccueil_xls.cfm">
				</cfif>
			<cfelseif format eq "CSV">
				<cfif tb eq "complet">
					<cfinclude template="/fr/consotel/consoview/cfm/report/tbaccueil_complet_csv.cfm">
				<cfelse>
					<cfinclude template="/fr/consotel/consoview/cfm/report/tbaccueil_csv.cfm">
				</cfif>
			</cfif>
		<cfelse>
			<html>
			<head>
				<link href="/fr/consotel/consoview/css/pdf_css.css" rel="stylesheet" type="text/css">			
			</head>
			<body>
			<p>Pas de donnÃ©es pour ce rapport.</p>
			</body>
			</html>
		</cfif>
	<cfelse>
	<cfheader name="Content-Disposition" value="inline;filename=tb.pdf" charset="utf-8">
	<cfcontent type="application/pdf" >
		<!--- Rapport Flash et pdf --->
		<cfdocument format="#format#" backgroundvisible="yes" fontembed = "yes"  unit="CM" margintop="5" marginleft="0.5" marginright="0.5" orientation="portrait">
		<cfoutput>
		<html>
			<head>
				
			</head>
			<body>
			<style>
				@page { size:portrait; margin-top:2cm; margin-bottom:2cm; margin-left:1cm; margin-right:1cm; }
				html body {
				  margin		:0;
				  padding		:0;
				  background	: ##FFFFFF;
				  font			:x-small Verdana,Georgia, Sans-serif;
				  voice-family	: "\"}\""; voice-family:inherit;
				  font-size:x-small;
				  } html>body {
					font-size	: x-small;
				}
				
				.cadre {
					border-color:##000000;
					border-style:solid;
					border-collapse:collapse;
					
					border-top-width:1px;
					border-left-width:1px;
					border-right-width:1px;
					border-bottom-width:1px;
				}
				
				.cadre1{
					background		: rgb(255,255,255);
					border-bottom	:1px solid rgb(0,0,0);
					border-left		:1px solid rgb(0,0,0);
					border-right	:1px solid rgb(0,0,0);
					border-top		:1px solid rgb(0,0,0);
					border-collapse : collapse;
				}
				
				table .menuconsoview
				{
					border 		: none;
					background	: transparent;
					font		: normal 8px Tahoma,Arial, sans-serif;
					color		: ##005EBB;
				}
				
				.menuconsoview caption{
					background		:##036 ;
					color			: white;
					font			: bold 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
					padding			: 4px;
					letter-spacing	: 1px;
				
				}
				
				.menuconsoview th{
					background   	:##99CCFF ;
					color        	: ##036;
					font        	: normal 9px EurostileBold,verdana,arial,helvetica,serif;
					padding      	: 3px;
					border-collapse	: collapse;
				}
				
				.menuconsoview .th_bottom{
					vertical-align	: middle;
					padding			: 3px;
					border-bottom	: 1px solid ##808080;
					font			: normal 9px Tahoma,Arial, sans-serif;
					color        	: ##036;
					background		: ##99CCFF;
				}
				
				.menuconsoview .th_top{
					vertical-align	: middle;
					padding			:3px;
					border-top		: 1px solid ##808080;
					font			: normal 9px Tahoma,Arial, sans-serif;
					color       	: ##036;
					background		: ##99CCFF;
				}
				
				.menuconsoview .grid_normal{
					background	: ##FDFDFB;
					color		: rgb(0 , 0, 0);
					font		: normal 8px Tahoma,Arial, sans-serif;
					padding		: 2px;
				}
				
				.menuconsoview .grid_small{
					background	: ##FDFDFB;
					color		: rgb(0 , 0, 0);
					font		: normal 8px Tahoma,Arial, sans-serif;
					padding		: 0px;
				}
				
				.menuconsoview .grid_medium {
					background	: ##FDFDFB;
					color		: rgb(0 , 0, 0);
					font		: normal 8px Tahoma,Arial, sans-serif;
					padding-top		: 1px;
				}
				
				.menuconsoview .grid_total{
					background	: ##008ACC;
					color		: rgb(0 , 0, 0);
					font		: normal 8px Tahoma,Arial, sans-serif;
					padding-top	: 2px;
				}
				
				.menuconsoview .grid_bold{
					background	: ##FDFDFB;
					color		: rgb(0 , 0, 0);
					font		: bold 8px Tahoma,Arial, sans-serif;
					padding		: 0px;
				}
				
				/* caption impossible pour liste des produits */
				.td5{
					padding		:5px;
					background	:##036;
					color		:rgb(255,255,255);
					font		:normal 9px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
				}
				
			</style>
			<cfdocumentitem type="header">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
					<tr>
						<td align="left" width="40%" height="50"><img src="../../../images/consoview_new.gif" height="40"></td>
						<td width="17%">&nbsp;</td>
						<td width="3%" bgcolor="F4F188">&nbsp;</td>
						<td align="right" bgcolor="FDFCE1" width="40%"><font face="Arial" size="2">TABLEAU DE BORD&nbsp;#Ucase(tb)#&nbsp;</font></td>
					</tr>
					<tr><td height="40">&nbsp;</td></tr>
					<tr>
						<td colspan="4">
						<font face="Tahoma" size="2"><strong>#raisonsociale#</strong></font><br>
						<font face="Tahoma" size="1">Edit&eacute; par&nbsp<strong>#session.user.prenom# #session.user.nom#</strong></font><br>
						<font face="Tahoma" size="1">Le&nbsp;#Lsdateformat(now(), 'dd mmmm yyyy')#</font>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<font face="Tahoma" size="1">
								<u>P&eacute;riode:</u>
								&nbsp;#Lsdateformat(datedeb, 'dd mmmm yyyy')# - #Lsdateformat(datefin, 'dd mmmm yyyy')#
							</font>
						</td>
					</tr>
				</table>
			</cfdocumentitem>
			<cfif tb eq "complet">
				<cfinclude template="./accueil_complet_pdf.cfm">
			<cfelse>
				<cfinclude template="./accueil_pdf.cfm">
			</cfif>
			<cfdocumentitem type="footer">
				<style>
				.tfooter {
					color: rgb(0 , 0, 0);
					font: normal 11px Tahoma,Arial, sans-serif;
				}
				</style>
				<center>
				<table border="0" cellpadding="0" cellspacing="0" width="560" align="center">
					<tr>
						<td align="center" class="tfooter" width="560">copyright CONSOTEL 2000 / 2008 - page #cfdocument.currentpagenumber# / #cfdocument.totalpagecount#</td>
					</tr>
				</table>
				</center>
			</cfdocumentitem>
			</body>
		</html> 
		</cfoutput>
		</cfdocument>
	</cfif>
</cfif>
