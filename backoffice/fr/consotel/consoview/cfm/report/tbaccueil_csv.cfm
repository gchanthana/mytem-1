<!--- 
<cfdump var="#FORM#" label="FORM">
<br>
<cfdump var="#resultsetAbo#" label="resultsetAbo #resultsetAbo.recordcount#">
<br>
<cfdump var="#resultsetconso#" label="resultsetconso #resultsetconso.recordcount#">
 --->
 
<cfsetting enablecfoutputonly="true">
<cfset newLine = chr(13) & chr(10)>
<cfset fileName = "Tableau-Complet-" & replace(session.perimetre.raison_sociale,' ','_',"all") & ".csv">
<cfheader name="Content-Disposition" value="attachment;filename=#fileName#">
<cfcontent type="text/csv"><cfoutput>TYPE_THEME;SEGMENT;THEME;QTE;NBRE_APPEL;DUREE_APPEL;MONTANT#newLine#</cfoutput>
<cfoutput query="resultsetAbo">"#TYPE_THEME#";"#SEGMENT_THEME#";"#THEME_LIBELLE#";#QTE#;0;0;#lsNumberFormat(MONTANT_FINAL,".__")##newLine#</cfoutput>
<cfoutput query="resultsetconso">"#TYPE_THEME#";"#SEGMENT_THEME#";"#THEME_LIBELLE#";0;#NOMBRE_APPEL#;#lsNumberFormat(DUREE_APPEL,".__")#;#lsNumberFormat(MONTANT_FINAL,".__")##newLine#</cfoutput>
