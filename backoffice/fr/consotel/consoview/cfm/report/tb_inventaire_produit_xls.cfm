<cfheader name="Content-Disposition" value="inline;filename=InventaireProduit.xls">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="iso-8859-1"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Christian Cor</Author>
  <LastAuthor>Brice Miramont</LastAuthor>
  <LastPrinted>2006-05-18T07:46:03Z</LastPrinted>
  <Created>2006-05-18T07:22:11Z</Created> 
  <LastSaved>2006-05-18T08:00:38Z</LastSaved>
  <Company>CONSOTEL</Company>
  <Version>11.6568</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12660</WindowHeight>
  <WindowWidth>15180</WindowWidth>
  <WindowTopX>480</WindowTopX>
  <WindowTopY>120</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
  <Font/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="m162242374">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#FFFFFF"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#FFFFFF"/>
   </Borders>
  <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#FFFFFF"
    ss:Bold="1"/>
   <Interior ss:Color="#003366" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s21">
   <Alignment ss:Vertical="Center"/>
  </Style>
  <Style ss:ID="s23">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
  <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="20" ss:Color="#008000"
    ss:Bold="1" ss:Italic="1"/>
  </Style>
  <Style ss:ID="s24">
   <Alignment ss:Vertical="Center"/>
   <Font ss:Size="8"/>
  </Style>
  <Style ss:ID="s25">
   <Alignment ss:Vertical="Center"/>
   <Font ss:Size="8"/>
   <NumberFormat ss:Format="General Date"/>
  </Style>
  <Style ss:ID="s26">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s27">
   <Alignment ss:Vertical="Center"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s28">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Font ss:Size="14"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s29">
   <Alignment ss:Vertical="Center"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s30">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Font x:Family="Swiss" ss:Size="12" ss:Bold="1"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s31">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Color="#003366" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s39">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#FFFFFF"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s40">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s41">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s42">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s43">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s44">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#FFFFFF"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s45">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s46">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s47">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s48">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s49">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#FFFFFF"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s50">
   <Alignment ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s52">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s53">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s54">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Currency"/>
  </Style>
  <Style ss:ID="s55">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s56">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Currency"/>
  </Style>
	<Style ss:ID="s57">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
  </Style>
	<Style ss:ID="s61">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s58">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s60" ss:Parent="s56">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Inventaire">
  <Names>
   <NamedRange ss:Name="_FilterDatabase" ss:RefersTo="=Inventaire!R12C1:R12C10"
    ss:Hidden="1"/>
   <NamedRange ss:Name="Print_Titles" ss:RefersTo="=Inventaire!R11:R12"/>
  </Names>
  <Table ss:ExpandedColumnCount="10" ss:ExpandedRowCount="<cfoutput>#Evaluate(dataset.recordcount+13)#</cfoutput>" x:FullColumns="1"
   x:FullRows="1" ss:StyleID="s21" ss:DefaultColumnWidth="60">
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="120"/>
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="117.75"/>
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="60"/>
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="80"/>
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="100"/>
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="100"/>
	<cfif dataset.type_theme eq "Consommations">
		<Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="100" ss:Span="1"/>
		<Column ss:Index="9" ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="68.25"/>
	<cfelse>
		<Column ss:Index="8" ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="68.25"/>
	</cfif>
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="81.75"/>
   <Row>
    <Cell ss:MergeDown="1" ss:StyleID="s23"><ss:Data ss:Type="String"
      xmlns="http://www.w3.org/TR/REC-html40"><B><I><Font html:Color="#008000">Conso</Font><Font
         html:Color="#FF9900">View</Font></I></B></ss:Data></Cell>
   </Row>
   <Row>	
	<cfif dataset.type_theme eq "Consommations">
		<Cell ss:Index="9" ss:StyleID="s24"><Data ss:Type="String">&Eacute;dit&eacute; par :</Data></Cell>
		<Cell ss:Index="10" ss:StyleID="s26"><Data ss:Type="String"><cfoutput>#session.user.prenom# #session.user.nom#</cfoutput></Data></Cell>
	<cfelse>
		<Cell ss:Index="7" ss:StyleID="s24"><Data ss:Type="String">&Eacute;dit&eacute; par :</Data></Cell>
		<Cell ss:Index="8" ss:StyleID="s26"><Data ss:Type="String"><cfoutput>#session.user.prenom# #session.user.nom#</cfoutput></Data></Cell>
	</cfif>  
   </Row>
   <Row ss:Index="4" ss:AutoFitHeight="0" ss:Height="14.25">
    <cfif dataset.type_theme eq "Consommations">
		<Cell ss:Index="9" ss:StyleID="s24"><Data ss:Type="String">Le </Data></Cell>
	<cfelse>
		<Cell ss:Index="8" ss:StyleID="s24"><Data ss:Type="String">Le </Data></Cell>
	</cfif>
	<Cell ss:StyleID="s26"><Data ss:Type="String">&nbsp;<cfoutput>#Lsdateformat(now(), 'dd/mm/yyyy')#</cfoutput></Data></Cell>  
   </Row>
   <Row ss:Index="6" ss:AutoFitHeight="0" ss:Height="22.5">
    <Cell ss:StyleID="s27"/>
    <Cell ss:StyleID="s27"/>
    <Cell ss:StyleID="s27"/>
    <Cell ss:StyleID="s27"/>
    <Cell ss:StyleID="s27"/>
    <Cell ss:StyleID="s27"/>
    <Cell ss:StyleID="s27"/>
	<cfif dataset.type_theme eq "Consommations">
		<Cell ss:StyleID="s27"/>
	</cfif>
    <Cell ss:StyleID="s27"/>
    <Cell ss:StyleID="s28"><Data ss:Type="String">Inventaire produit : <cfoutput>#produit#</cfoutput></Data></Cell>
   </Row>
   <Row ss:Index="8" ss:AutoFitHeight="0" ss:Height="22.5">
    <Cell ss:Index="5" ss:StyleID="s29"/>
    <Cell ss:StyleID="s29"/>
    <cfif dataset.type_theme eq "Consommations">
		<Cell ss:StyleID="s29"/>
	</cfif>
	<Cell ss:StyleID="s29"/>
    <Cell ss:StyleID="s29"/>
    <Cell ss:StyleID="s30"><Data ss:Type="String"><cfoutput>#raisonsociale#</cfoutput></Data></Cell>
   </Row>
   <Row>
    <Cell ss:StyleID="s31"><Data ss:Type="String">PÃ©riode d'Ã©mission des factures : <cfoutput>#Lsdateformat(datedeb, 'dd mmmm yyyy')# - #Lsdateformat(datefin, 'dd mmmm yyyy')#</cfoutput></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="13.5"/>
   <Row ss:AutoFitHeight="0" ss:Height="13.5">
   <cfif dataset.type_theme eq "Consommations">
	<Cell ss:MergeAcross="9" ss:StyleID="m162242374"><Data ss:Type="String"><cfoutput>#produit#</cfoutput></Data><NamedCell
      ss:Name="Print_Titles"/></Cell>
	<cfelse>
	<Cell ss:MergeAcross="8" ss:StyleID="m162242374"><Data ss:Type="String"><cfoutput>#produit#</cfoutput></Data><NamedCell
      ss:Name="Print_Titles"/></Cell>
	</cfif>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="31.5">
    <Cell ss:StyleID="s39"><Data ss:Type="String">SociÃ©tÃ© - Site</Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Titles"/></Cell>
    <Cell ss:StyleID="s40"><Data ss:Type="String">Adresse 1</Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Titles"/></Cell>  
    <Cell ss:StyleID="s40"><Data ss:Type="String">Code postal</Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Titles"/></Cell>
    <Cell ss:StyleID="s40"><Data ss:Type="String">Ville</Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Titles"/></Cell>
    <Cell ss:StyleID="s41"><Data ss:Type="String">Ligne(NDI)</Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Titles"/></Cell>
	 <Cell ss:StyleID="s41"><Data ss:Type="String"><cfoutput>#usage#</cfoutput></Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Titles"/></Cell>
	<cfif dataset.type_theme eq "Consommations">
		<Cell ss:StyleID="s57"><Data ss:Type="String">Nombre appels</Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Titles"/></Cell>
    	<Cell ss:StyleID="s57"><Data ss:Type="String">Volume (kO ou mn)</Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Titles"/></Cell>
    <cfelse>
    	<Cell ss:StyleID="s41"><Data ss:Type="String">Quantitï¿½</Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Titles"/></Cell>
    </cfif>
    <Cell ss:StyleID="s41"><Data ss:Type="String">Montant (&euro;)</Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Titles"/></Cell>
    <Cell ss:StyleID="s42"><Data ss:Type="String">Facture</Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Titles"/></Cell>
   </Row>

	<cfoutput query="dataset" group="siteID">
		<cfoutput>
		<cfset compteur=1>		 
			<cfif compteur eq 1>
			 <Row ss:AutoFitHeight="0" ss:Height="17.25" ss:StyleID="s24">			    
			   <Cell ss:StyleID="s43"><Data ss:Type="String">#nom_site#</Data></Cell>
			    <Cell ss:StyleID="s44"><Data ss:Type="String">#adresse1#</Data></Cell>
			 	<Cell ss:StyleID="s45"><Data ss:Type="String">&nbsp;#zipcode#</Data></Cell>  
			    <Cell ss:StyleID="s45"><Data ss:Type="String">#commune#</Data></Cell>
			    <Cell ss:StyleID="s44"><Data ss:Type="String">&nbsp;#sous_tete#</Data></Cell>
			    <Cell ss:StyleID="s45"><Data ss:Type="String">#commentaires#</Data></Cell>			    		   
			 <cfif dataset.type_theme eq "Consommations">			    
			    	<Cell ss:StyleID="s53"><Data ss:Type="Number">#replace(val(nombre_appel),",",".")#</Data></Cell>
			    	<cfif duree_appel gt 0>
			    		<Cell ss:StyleID="s53"><Data ss:Type="Number">#replace(int(val(duree_appel)),",",".")#</Data></Cell>
			    	<cfelse>
			    		<Cell ss:StyleID="s53"><Data ss:Type="Number"></Data></Cell>
			    	</cfif>			    	
			    <cfelse>			    
			    	<Cell ss:StyleID="s53"><Data ss:Type="Number">#replace(val(qte),",",".")#</Data></Cell>			    	
			    </cfif>			    
			    <Cell ss:StyleID="s54"><Data ss:Type="Number">#replace(val(montant_final),",",".")#</Data></Cell>
			    <Cell ss:StyleID="s47"><Data ss:Type="String">&nbsp;#numero_facture#</Data></Cell> 
			   </Row>
			<cfelse>
			   <Row ss:AutoFitHeight="0" ss:Height="17.25" ss:StyleID="s24">
			    <Cell ss:StyleID="s48"><Data ss:Type="String">#nom_site#</Data></Cell>
			    <Cell ss:StyleID="s49"><Data ss:Type="String">#adresse1#</Data></Cell>
			    <Cell ss:StyleID="s49"><Data ss:Type="String">&nbsp;#zipcode#</Data></Cell>
			    <Cell ss:StyleID="s50"><Data ss:Type="String">#commune#</Data></Cell>
			    <Cell ss:StyleID="s50"><Data ss:Type="String">&nbsp;#sous_tete#</Data></Cell>
			    <Cell ss:StyleID="s50"><Data ss:Type="String">#commentaires#</Data></Cell>	
			    		    
			     <cfif dataset.type_theme eq "Consommations">			    
			    	<Cell ss:StyleID="s55"><Data ss:Type="Number">#replace(val(nombre_appel),",",".")#</Data></Cell>
			    	<cfif duree_appel gt 0>
			    		<Cell ss:StyleID="s55"><Data ss:Type="Number">#replace(int(val(duree_appel)),",",".")#</Data></Cell>
			    	<cfelse>
			    		<Cell ss:StyleID="s55"><Data ss:Type="Number">0</Data></Cell>
			    	</cfif>			    	
				<cfelse>				
					<Cell ss:StyleID="s55"><Data ss:Type="Number">#replace(val(qte),",",".")#</Data></Cell>					
				</cfif> 				
			    <Cell ss:StyleID="s56"><Data ss:Type="Number">#replace(val(montant_final),",",".")#</Data></Cell>
			    <Cell ss:StyleID="s52"><Data ss:Type="String">&nbsp;#numero_facture#</Data></Cell> 
			   </Row>
			 </cfif>
		    <cfset compteur=compteur+1>
		 </cfoutput>
	</cfoutput>
   <Row ss:AutoFitHeight="0" ss:Height="17.25" ss:StyleID="s24">
    <Cell ss:StyleID="s43"/>
    <Cell ss:StyleID="s44"/>
    <Cell ss:StyleID="s44"/>
    <Cell ss:StyleID="s44"/>
    <Cell ss:StyleID="s45"/>
    <Cell ss:StyleID="s61"><Data ss:Type="String">TOTAL</Data></Cell>
	<Cell ss:StyleID="s58" ss:Formula="=SUM(R[<cfoutput>-#dataset.recordcount#</cfoutput>]C:R[-1]C)"></Cell>
	<cfif dataset.type_theme eq "Consommations">
		<Cell ss:StyleID="s58" ss:Formula="=SUM(R[<cfoutput>-#dataset.recordcount#</cfoutput>]C:R[-1]C)"></Cell>			
	</cfif>
    <Cell ss:StyleID="s60" ss:Formula="=SUM(R[<cfoutput>-#dataset.recordcount#</cfoutput>]C:R[-1]C)"></Cell>
    <Cell ss:StyleID="s44"/>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.51181102362204722"/>
    <Footer x:Margin="0.51181102362204722"
     x:Data="&amp;E&amp;&quot;Arial,Gras&quot;&amp;8Copyright ConsoView - ConsoTel 2000-2006&amp;C&amp;&quot;Arial,Gras&quot;&amp;8Page &amp;P de &amp;T"/>
    <PageMargins x:Bottom="0.98425196850393704" x:Left="0.78740157480314965"
     x:Right="0.78740157480314965" x:Top="0.98425196850393704"/>
   </PageSetup>
   <FitToPage/>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <Scale>58</Scale>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <DoNotDisplayGridlines/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>15</ActiveRow>
     <ActiveCol>7</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
	<cfif dataset.type_theme eq "Consommations">
		<AutoFilter x:Range="R12C1:R12C10" xmlns="urn:schemas-microsoft-com:office:excel">
	<cfelse>
		<AutoFilter x:Range="R12C1:R12C9" xmlns="urn:schemas-microsoft-com:office:excel">
	</cfif>
  </AutoFilter>
	<cfif dataset.recordcount gt 37>
	<FreezePanes/>
   <SplitHorizontal>12</SplitHorizontal>
   <TopRowBottomPane><cfoutput>#Evaluate(dataset.recordcount-8)#</cfoutput></TopRowBottomPane>
   <ActivePane>2</ActivePane>
   <Panes>
    <Pane>
     <Number>1</Number>
     <ActiveRow>11</ActiveRow>
     <ActiveCol>1</ActiveCol>
    </Pane>
    <Pane>
     <Number>2</Number>
     <ActiveRow><cfoutput>#Evaluate(dataset.recordcount+12)#</cfoutput></ActiveRow>
     <ActiveCol>5</ActiveCol>
    </Pane>
   </Panes>
	</cfif>
 </Worksheet>
</Workbook>