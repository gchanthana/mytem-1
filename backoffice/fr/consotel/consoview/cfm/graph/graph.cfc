<cfcomponent>
	<cffunction name="GetMonth" output="Yes" returntype="string"
		hint="Fonction qui retourne les 3 premières lettres d'un mois en fonction d'un nombre donné">
		
		<cfargument name="num_mois" type="numeric" required="Yes" 
			displayname="Numéro du mois"
			hint="Numéro du mois dont on veut les 3 premières lettres">
			
		<cfset ListMois3Lettres="Jan,Fév,Mar,Avr,Mai,Juin,Juil,Aou,Sep,Oct,Nov,Déc">
		<cfreturn ListGetAt(ListMois3Lettres,num_mois)>
	</cffunction>
	
	<cffunction name="GetColor" output="Yes" returntype="string"
		hint="Fonction qui retourne une couleur en fonction du type de graphe et du type de série">
		
		<cfargument name="type_graph" type="string" required="Yes" 
			displayname="type de graphe"
			hint="Type du graphe: pie, line, bar, area">
			
		<cfargument name="num_liste" type="numeric" required="Yes" 
			displayname="Numéro de liste"
			hint="Numéro de liste où la couleur se trouve">
		
		<cfargument name="indice" type="numeric" required="No"
			displayname="Indice"
			hint="Indice de la couleur dans la liste où elle se trouve">
		
		<cfset return_value="">
		<!--- <cfset ColorList_1_Pie = "669999,666699,997767,E6A69A,999999,9A9AE6,E5E5E5,E6BE9A,E6D69A,998F67"> ---> <!--- page d'accueil de ConsoActifs --->
		<cfset ColorList_1_Pie = "1393D2,F9D525,FF9B19,84C261,CC0000,,A36B3C,E0B491,D8A2C4,1393D2,9257AB,BFB76F">
		<cfset ColorList_2_Pie = "FFFFC0,B3B386,B39586,FFD4BF,D9B5A3,B39586,8C7569,D9D9A3,B3B3B3,D9D9D9"> <!--- page d'accueil de ConsoActifs --->
		<cfset ColorList_3_Pie = "0033CC,33CC00,CC0033"> <!--- statistiques de Colt --->
		<cfset ColorList_4_Pie = "54559B,C97060,7BAE5F,C868B9,000050,845807,4DDC68,800900,4DDC68,81B6C8,C8A565,6D036D"> <!--- pour le grand graphe de Colt et Admin--->
		<cfset ColorList_1_Bar = "1393D2,F9D525"> <!--- page d'accueil de ConsoActifs --->
		<cfset ColorList_2_Bar = "BBA58D"> <!--- pour le graph bar/line de ConsoActifs --->
		<cfset ColorList_3_Bar = "009933,9900CC,FF9900,993333,1E1EDC"> <!--- statistiques de Colt --->
		<cfset ColorList_4_Bar = "9F2727,4249A9,757A75,00750E,B750FC,CCDA52,E06557,57DF64,53D1E1,D8A455,CDB4CA,996433"> <!--- pour le grand graphe de Colt--->
		<cfset ColorList_5_Bar = "54559B,C97060,7BAE5F,C868B9,000050,845807,4DDC68,800900,4DDC68,81B6C8,C8A565,6D036D"> <!--- pour le graphe horizontal de l'Admin--->
		<cfset ColorList_6_Bar = "FE9440,7ECA70,182985,870409"> <!--- pour les mini-graphes du top 10 --->
		<cfset ColorList_1_Line = "62886F"> <!--- pour le graph bar/line de ConsoActifs --->
		<cfset ColorList_2_Line = "666699,C67D7D"> <!--- accueil consoviex + statistiques de Colt --->
		<cfset ColorList_3_Line = "009420"> <!--- page d'accueil de Colt --->
		<cfset ColorList_1_Area = "66FFC0A0,20000000,66008C23,6600D2FF,55FFFF00"> 
		
		<!--- L'indice (3ème argument) étant facultatif ... --->
		<cfif IsDefined("indice")>
			<!--- ... si il est indiqué, c'est que l'on cherche une couleur en particulier, on va donc la chercher dans la bonne liste ... --->
			<cfset return_value = ListGetAt(#Evaluate("ColorList_#num_liste#_#type_graph#")#,indice)>
		<cfelse>
			<!--- ... sinon, on renvoi la liste correpondante en entier --->
			<cfset return_value = Evaluate("ColorList_#num_liste#_#type_graph#")>
		</cfif>
		<cfreturn return_value>
	</cffunction>
	
	
	<cffunction name="GetData" output="Yes" returntype="struct"
		hint="Fonction qui retourne le nombre de labels nécessaires ainsi que la valeur maximale de l'échelle selon un intervalle et une liste pour déterminer l'élément max">
		
		<cfargument name="liste" required="Yes" 
			displayname="liste de valeurs ou structure"
			hint="Liste ou structure des valeurs dont on doit trouver le maximum">
		
		<cfargument name="interval" type="numeric" required="Yes"
			displayname="Intervalle"
			hint="Taille des intervalles voulus sur l'échelle">
		<cfset max_value=0>
		<cfset max_value = 0>
		<cfset max_echelle = 0>
		<cfset cp = 0>
		
		<!--- la fonction peut etre appelée avec une structure ou une liste mais le fonctionnement est le meme--->
		<cfif IsStruct("#liste#")>
			<cfloop collection="#liste#" item="i">
				<cfif liste[i] GT max_value>
					<!--- on cherche l'élément maximum --->
					<cfset max_value = liste[i]>
					<!--- à chaque fois que l'on tombe sur un max, on remet à jour la variable 'max_echelle' qui,
						selon l'intervalle donné en argument, se remet à l'échelle au-dessus de la valeur maximale trouvée,
						si besoin est --->
					<cfloop condition="max_echelle LT max_value">
						<cfset max_echelle = cp * interval>
						<cfset cp = cp + 1>
					</cfloop>
				</cfif>
			</cfloop>
		<cfelse>
			<cfloop list="#liste#" index="element">
				<cfif element GT max_value>
					<cfset max_value = element>
					<cfloop condition="max_echelle LT max_value">
						<cfset max_echelle = cp * interval>
						<cfset cp = cp + 1>
					</cfloop>
				</cfif>
			</cfloop>
		</cfif>
		
		<!--- calcul du nombre de labels sur l'échelle, +1 car la ligne du zéro compte --->		
		<cfset nb_gridlines = (max_echelle / interval) + 1>
		
		<cfset StructData = StructNew()>
		<cfset var=StructInsert(StructData,"max_echelle",max_echelle)>
		<cfset var=StructInsert(StructData,"nb_gridlines",nb_gridlines)>

		<cfreturn StructData>			
	</cffunction>
	
	<cffunction name="createChartCompte" returntype="string" access="public">
		<cfargument name="idref_client">
		<cfargument name="ladate">
		<cfsavecontent variable="chartPath">
			<cfquery name="chartSeries" datasource="#application.offredsn#">
				SELECT decode(theme_libelle,'Local','France',decode(theme_libelle,'National','France',
			decode(theme_libelle,'Voisinage','France',theme_libelle))) AS theme_libelle , SUM(dfa.montant) AS montant
				FROM detail_facture_abo dfa, produit_client pcl, produit_catalogue pca, theme_produit_catalogue tpca, theme_produit tp
				WHERE dfa.idproduit_client=pcl.Idproduit_Client
				AND pcl.Idproduit_Catalogue=pca.Idproduit_Catalogue
				AND pca.Idproduit_Catalogue=tpca.Idproduit_Catalogue
				AND tpca.Idtheme_Produit=tp.Idtheme_Produit
				AND dfa.idref_client=#idref_client#
				AND pcl.Idref_Client=#idref_client#
				AND nvl(dfa.typusg,'0') > '0'
				AND trunc(dfa.date_affectation) = trunc(to_date('#ladate#','dd/mm/yyyy'))
				GROUP BY decode(theme_libelle,'Local','France',decode(theme_libelle,'National','France',
			decode(theme_libelle,'Voisinage','France',theme_libelle)))
				ORDER BY theme_libelle
			</cfquery>
			<cfobject component="graph" name="mygraph">
			<cfchart
				style="/graph/graph_pie_no_insets.xml"
				chartHeight = "140"
				chartWidth = "350"
				format="png"
				showLegend="yes"
				font="Verdana"
				fontsize="9" 				
			>
				<cfchartseries type="pie" query="chartSeries" itemcolumn="theme_libelle" valuecolumn="montant" paintstyle="plain" colorlist="#mygraph.GetColor('pie',1)#"/>
			</cfchart>
			</cfsavecontent>
				<cfset fullImagePath = getToken(cgi.server_protocol,1,"/") & "://" & cgi.http_host & mid(trim(chartPath),find("/CFIDE",trim(chartPath)),(find(".PNG",trim(chartPath))-find("SRC",trim(chartPath)))-1)>
		<cfreturn fullImagePath>
	</cffunction>
	
	<cffunction name="createChartCompteEvolution" returntype="string" access="public">
		<cfargument name="idref_client">
		<cfargument name="ladate">
		<cfargument name="siteID">
		<cfsavecontent variable="chartPath">
			<cfquery name="chartSeries" datasource="#application.offredsn#">
				SELECT decode(a.theme_libelle,'Local','France',decode(a.theme_libelle,'National','France',
			decode(a.theme_libelle,'Voisinage','France',a.theme_libelle))) AS theme_libelle, a.montant AS montant
			FROM
				-- comms du mois en cours
			   (SELECT tp.ordre_affichage, tp.theme_libelle, round(SUM(nvl(dfa.duree_appel,1))) AS duree, 
				SUM(dfa.montant) AS montant
				FROM detail_facture_abo dfa, produit_client pcl, produit_catalogue pca, theme_produit_catalogue tpca, theme_produit tp
				WHERE dfa.idproduit_client=pcl.Idproduit_Client
				AND pcl.Idproduit_Catalogue=pca.Idproduit_Catalogue
				AND pca.Idproduit_Catalogue=tpca.Idproduit_Catalogue
				AND tpca.Idtheme_Produit=tp.Idtheme_Produit
				AND dfa.idref_client=#idref_client#
				AND pcl.Idref_Client=#idref_client#
				AND nvl(dfa.typusg,'0') > '0'
				AND trunc(dfa.date_affectation) = trunc(to_date('#ladate#','dd/mm/yyyy'))
				GROUP BY tp.ordre_affichage, tp.theme_libelle) a
				GROUP BY decode(a.theme_libelle,'Local','France',decode(a.theme_libelle,'National','France',
			decode(a.theme_libelle,'Voisinage','France',a.theme_libelle)))
				ORDER BY theme_libelle
			</cfquery>
			<cfobject component="graph" name="mygraph">
			<cfchart
				style="/graph/graph_pie_no_insets.xml"
				chartHeight = "140"
				chartWidth = "350"
				format="png"
				showLegend="yes"
				font="Verdana"
				fontsize="9" 				
			>
				<cfchartseries type="pie" query="chartSeries" itemcolumn="theme_libelle" valuecolumn="montant" paintstyle="plain" colorlist="#mygraph.GetColor('pie',1)#"/>
			</cfchart>
			</cfsavecontent>
				<cfset fullImagePath = getToken(cgi.server_protocol,1,"/") & "://" & cgi.http_host & mid(trim(chartPath),find("/CFIDE",trim(chartPath)),(find(".PNG",trim(chartPath))-find("SRC",trim(chartPath)))-1)>
		<cfreturn fullImagePath>
	</cffunction>
</cfcomponent>