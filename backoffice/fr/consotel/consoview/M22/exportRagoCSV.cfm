﻿<cfsetting enablecfoutputonly="true">
<cfset newLine = chr(13) & chr(10)>
<cfset dataset=createObject("component","fr.consotel.consoview.parametres.RAGO.Rules").getReglesOrga(form.IDSOURCE,form.IDCIBLE,form.CLEF) >
<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
 
<cfheader name="Content-Disposition" value="attachment;filename=ExportRago.csv">

<cfif codeLangue EQ "fr_FR">
	<cfcontent type="text/csv"><cfoutput>Créée par;Date de création;Statut;Dernière exécution;OPERATEUR;Chemin source;Commentaire sur la règle;Organisation Cliente;Chemin cible#newLine#</cfoutput>
<cfelse>	
	<cfcontent type="text/csv"><cfoutput>Created by;Creation Date;Statut;Last Execution;Carrier;Source Path;comment on the rule;Custom Organization;Target Path#newLine#</cfoutput>
</cfif>
<cfoutput query="dataset">"#PRENOM_CREATEUR# #NOM_CREATEUR#";#lsDateFormat(CREATE_DATE,"DD MMMM YYYY")#;"#CHAINE_BOOL_ACTIVE#";#lsDateFormat(LAST_EXECUTED,"DD MMMM YYYY")#;#ORGA_SOURCE#;#Replace(CHEMIN_SOURCE,";","/","all")#;"#REGLE_COMMENT#";#ORGA_CIBLE#;#Replace(CHEMIN_CIBLE,";","/","all")#;#newLine#</cfoutput>