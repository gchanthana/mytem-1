<cfcomponent output="false">
	
	<cffunction name="importInfosCSV" access="remote" returntype="any">
		
		<cfargument name="uuidFile" 			required="true" type="String"/>
		<cfargument name="filename" 			required="true" type="String"/>

		<cfset directory = "/container/importdemasse/">	
		
		<cfset path = #directory# & #uuidFile# & "/" & #filename#>
		<!--- sheetname="Feuil1" Feuil1 le nom de sheet dans le tableau excel  --->
		<!--- ce nom est important sinon erreur :  Unable to invoke CFC - An error occurred while reading the Excel  --->
		<cfspreadsheet action="read" query="qryInfos" src="#path#" /> 
					
		<cfreturn qryInfos> <!--- retrun  --->
	</cffunction>
	
	<cffunction name="getUUID" access="remote" returntype="String">
						
		<cfset  var uuid =createuuid()>	
		<cfreturn uuid>
	</cffunction>
	
</cfcomponent>