<cfcomponent output="false">
	
	<!--- permet d'associer un profil à un destinataire --->
	
	<cffunction access="public" name="associerProfilDest" returntype="Numeric" output="true">
	
		<cfargument name="idDestinataire" type="numeric" required="true">
		<cfargument name="idProfil" type="numeric" required="true">
		<cfargument name="idNoeud"  type="numeric" required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.associerProfilDest">		
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idDestinataire#" variable="p_iddestinataire">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idProfil#" variable="p_idprofil">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idNoeud#"  variable="p_idnoeud" >
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">				
		</cfstoredproc>	
		
		<cfreturn p_retour>

	</cffunction>
	
	<!--- permet de récupérer les profils dispobile d'un destinataire --->
	
	<cffunction name="getProfils" access="remote" returntype="any">
			<cfargument name="idGroupe" type="string" required="false">
			<cfargument name="idDest" type="string" required="false">
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m22.getProfils">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDest#"   null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
	
	<!--- permet de désassocier un profil de destinataire --->
	
	<cffunction name="removeProfilDest" access="remote" returntype="numeric">
		
			<cfargument name="idDestinataire" type="numeric" required="true">
			<cfargument name="idProfil" type="numeric" required="true">
			<cfargument name="idNoeud"  type="numeric" required="true">		
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m22.delProfilDest">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDestinataire#" variable="p_iddestinataire">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" variable="p_idprofil" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idNoeud#"  variable="p_idnoeud" >
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction>
	<!--- permet de supprimer un profil definitivement meme --->
	
	<cffunction name="removeProfilCascade" access="remote" returntype="numeric">
		
			<cfargument name="idProfil" type="numeric" required="true">
						
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.DELPROFILCASCADE">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" variable="p_idprofil" >
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction>
	
</cfcomponent>