﻿<cfcomponent name="getExportOrgaCSVbyMode_v2">

<cffunction name="getExportOrgaCSV3" access="public" returntype="query" output="false" displayname="Ramene les lignes  en CSV d'un perimetre" >		
		<cfargument name="p_idorga" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="p_mode" required="true" type="numeric" default="3" displayname="mode affectation( 0, 1 ou autre )" hint="Initial value for the IDproperty." />
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLOBAL.getExportOrgaCSV3_v2">			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_id_groupe" value="#ID_perimetre#"/>									        	
			<cfprocresult name="p_result" />        
		</cfstoredproc>
		<cfreturn p_result/>
</cffunction>