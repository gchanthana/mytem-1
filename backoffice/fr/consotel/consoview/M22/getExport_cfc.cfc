﻿<cfcomponent  name="getExport_cfc" alias="fr.consotel.consoview.M22.getExport_cfc">

<cffunction name="getExportOrgaCSV3" access="public" returntype="query" output="false" displayname="Ramene les lignes  en CSV d'un perimetre" >		
		<cfargument name="p_idorga" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="p_mode" required="true" type="numeric" default="3" displayname="mode affectation( 0, 1 ou autre )" hint="Initial value for the IDproperty." />
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GLOBAL.getExportOrgaCSV3_v3">			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idorga" value="#p_idorga#"/>									        	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_mode" value="#p_mode#"/>									        	
			<cfprocresult name="p_result" />        
		</cfstoredproc>	
		<cfreturn p_result/>
</cffunction>

<cffunction name="getExportOrgaClientCSV" access="public" returntype="query" output="false" displayname="Ramene les lignes  en CSV d'un perimetre" >		
		<cfargument name="p_idclient" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="p_idorga" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="p_mode" required="true" type="numeric" default="3" displayname="mode affectation( 0, 1 ou autre )" hint="Initial value for the IDproperty." />
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GLOBAL.getExportOrgaClientCSV">			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idclient" value="#p_idclient#"/>									        	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idorga" value="#p_idorga#"/>									        	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_mode" value="#p_mode#"/>									        	
			<cfprocresult name="p_result" />        
		</cfstoredproc>			
		<cfreturn p_result/>
</cffunction>

</cfcomponent>