<cfcomponent displayname="CookieReader" output="false">
	<cffunction name="readCookie" access="remote" output="false" returntype="Struct">				
		<cfreturn  COOKIE/>
	</cffunction>
</cfcomponent>