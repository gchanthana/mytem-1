<cfcomponent displayname="fr.consotel.consoview.M01.SendMail" hint="Gestion de l'envoi de mails" output="false">

	<cfset properties 	 = structNew()>

	<!--- TRAITEMENT DES DONNEES DE L'EVENT RECUPERER --->
	<cffunction access="public" name="sendMailLoginMotDePasse" returntype="void">
		<cfargument name="eventObject" 	type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">

			<cfset var mailing  	 = createObject("component","fr.consotel.consoview.M01.Mailing")>

			<cfset var uuidlog		 = #ARGUMENTS.eventObject.getEventTarget()#>
			<cfset var jobid		 = #ARGUMENTS.eventObject.getJobId()#>
			<cfset var type			 = #ARGUMENTS.eventObject.getEventType()#>
			<cfset var ImportTmp	 = "/container/M01/">
			<cfset var objectMail 	 = ''>
			<cfset var cptr 		 = 0>
			<cfset var param 		 = structNew()>

			<cfset var fileLogin	 = "login_#uuidlog#.html">
			<cfset var filePassW	 = "password_#uuidlog#.html">

			<cfset var pathEntier	 = '#ImportTmp##uuidlog#/'>
			<cfset var fileLoginPath = "#pathEntier##fileLogin#">
			<cfset var filePassWPath = "#pathEntier##filePassW#">

			<cfset rsltCkecked = checkArePresentInRespository(fileLoginPath, filePassWPath, pathEntier)>

			<cfif rsltCkecked GTE 1>	
 				<cfswitch expression="#type#">
					<cfcase value="L">

						<cfset rsltGetMail = getMailContent(uuidlog, type)>

						<cfif isDefined('rsltGetMail.UUID')>

							<cfif rsltGetMail.MAJ GT 0>
								<cfset sendMailContent(pathEntier, fileLogin, uuidlog, rsltGetMail)>
							<cfelse>
								<cfset procedureInError(uuidlog, type, jobid, "ERREUR MISE A JOUR DES PARMATRES DE MAILS", "ERREUR MISE A JOUR DES PARMATRES DE MAILS", 'updateMailToDatabase', rsltUpdate)>
							</cfif>

						<cfelse>
							<cfset procedureInError(uuidlog, type, jobid, "ERREUR RECUPERATION DES PARMATRES DE MAILS", "ERREUR RECUPERATION DES PARMATRES DE MAILS", 'getMailToDatabase', rsltGetMail)>
						</cfif>

					</cfcase>
					<cfcase value="P">

						<cfset rsltGetMail = getMailContent(uuidlog, type)>

						<cfif isDefined('rsltGetMail.UUID')>

							<cfif rsltGetMail.MAJ GT 0>
								<cfset sendMailContent(pathEntier, filePassW, uuidlog, rsltGetMail)>
							<cfelse>
								<cfset procedureInError(uuidlog, type, jobid, "ERREUR MISE A JOUR DES PARMATRES DE MAILS", "ERREUR MISE A JOUR DES PARMATRES DE MAILS", 'updateMailToDatabase', rsltUpdate)>
							</cfif>

						<cfelse>
							<cfset procedureInError(uuidlog, type, jobid, "ERREUR RECUPERATION DES PARMATRES DE MAILS", "ERREUR RECUPERATION DES PARMATRES DE MAILS", 'getMailToDatabase', rsltGetMail)>
						</cfif>

					</cfcase>
					<cfdefaultcase>
						<cfreturn>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfreturn>
			</cfif>

	</cffunction>

	<!--- RECUPERER LES INFORMATIONS POUR LE SERVEUR DE MAILS --->
	<cffunction access="public" name="getProperties" returntype="Struct">
		<cfargument name="codeapp" 	type="Numeric" 	required="true">
 		<cfargument name="key" 		type="String" 	required="true"/>

			<cfset properties 	 	= structNew()>
			<cfset codeappPropert	= structNew()>

			<cfset mailProperties  	= createObject("component","fr.consotel.consoview.M01.Mailing")>
			<cfset rsltProperties  	= mailProperties.get_appli_properties(codeapp, key)>

			<cfloop query="rsltProperties">
				<cfset codeappPropert[rsltProperties.KEY] = rsltProperties.VALUE>
			</cfloop>

		<cfreturn codeappPropert>
	</cffunction>

	<!--- RECUPERE LES PARAMETRES DU MAIL --->
	<cffunction access="public" name="getMailContent" returntype="Any">
		<cfargument name="uuidlog" 		type="String" required="true">
		<cfargument name="typeevent" 	type="String" required="true">

			<cfset var contentMail	= structNew()>
			<cfset var mailing  	= createObject("component","fr.consotel.consoview.M01.Mailing")>
			<cfset var rsltGetMail	= mailing.getMailToDatabase(uuidlog)>

			<cfloop query="rsltGetMail">
				<cfif rsltGetMail.TAG EQ typeevent && rsltGetMail.IS_CRATE EQ 0>
					<cfset contentMail.FROM			= rsltGetMail.MAIL_FROM>
					<cfset contentMail.TOMAIL		= rsltGetMail.MAIL_TO>
					<cfset contentMail.BCC			= rsltGetMail.MAIL_BCC>
					<cfset contentMail.SUBJECT		= rsltGetMail.MAIL_SUBJECT>
					<cfset contentMail.UUID			= rsltGetMail.UUID>
					<cfset contentMail.CODE_APPLI	= rsltGetMail.CODE_APPLI>
					<cfset contentMail.MAJ 			= mailing.updateMailToDatabase(1, uuidlog, typeevent)>
				</cfif>
			</cfloop>

			<cfset properties = getProperties(contentMail.CODE_APPLI, 'NULL')>

			<cfif NOT isDefined('properties.MAIL_SERVER')>
				<cfset properties.MAIL_SERVER = 'mail.consotel.fr'>
			</cfif>

			<cfif NOT isDefined('properties.MAIL_PORT')>
				<cfset properties.MAIL_PORT = 26>
			</cfif>

		<cfreturn contentMail>
	</cffunction>

	<!--- ENVOI DU MAIL --->
	<cffunction access="public" name="sendMailContent" returntype="void">
		<cfargument name="pathEntier" 	type="String" required="true">
		<cfargument name="myFile" 		type="String" required="true">
		<cfargument name="uuidlog" 		type="String" required="true">
		<cfargument name="mailStruct" 	type="Struct" required="true">

			<cfset var myDir 	 = GetDirectoryFromPath(GetCurrentTemplatePath())/>
			<cfset var M01_UUID  = createUUID()>
			<cfset var ImportTmp = "/container/M01/">

			<cftry>

				<cfif NOT DirectoryExists('#myDir##M01_UUID#')>

					<cfdirectory action="Create" directory="#myDir##M01_UUID#" type="dir" mode="777">

				<cfelse>

					<cfset M01_UUID = createUUID()>

					<cfdirectory action="Create" directory="#myDir##M01_UUID#" type="dir" mode="777">

				</cfif>

			<cfcatch>

					<!--- En cas d'échec de l'op?ration envoi d'un mail de notification --->
					<cfmail server="mail-cv.consotel.fr" port="25" from="login-mdp@saaswedo.com" to="monitoring@saaswedo.com"
							failto="postmaster@saaswedo.com" subject="[WARN-LOGIN_M01]Erreur lors de la création du répertoire" type="html">

							#CFCATCH.Message#<br/><br/><br/><br/>

							Chemin 	: <cfdump var="#pathEntier#"><br/><br/>
							Fichier : <cfdump var="#myFile#"><br/><br/>
							UUIDLOG : <cfdump var="#uuidlog#"><br/><br/>
							Mail 	: <cfdump var="#mailStruct#"><br/><br/>

					</cfmail>

			</cfcatch>
			</cftry>

			<cfset root = "#myDir##M01_UUID#">

			<cftry>

				<cfdirectory name="dGetDir" directory="#pathEntier#" action="List">

				<cffile action="copy" source="#ImportTmp##uuidlog#/#myFile#" destination="#myDir#/#M01_UUID#/">

			<cfcatch>

				<!--- En cas d'échec de l'op?ration envoi d'un mail de notification --->
					<cfmail server="mail-cv.consotel.fr" port="25" from="login-mdp@saaswedo.com" to="monitoring@saaswedo.com"
							failto="postmaster@saaswedo.com" subject="[WARN-LOGIN_M01]Erreur lors de la copie du répertoire" type="html">

						#CFCATCH.Message#<br/><br/><br/><br/>

						Chemin 	: <cfdump var="#pathEntier#"><br/><br/>
						Fichier : <cfdump var="#myFile#"><br/><br/>
						UUIDLOG : <cfdump var="#uuidlog#"><br/><br/>
						Mail 	: <cfdump var="#mailStruct#"><br/><br/>

				</cfmail>

			</cfcatch>
			</cftry>

			<cftry>

				<cfmail from="#mailStruct.FROM#" to="#mailStruct.TOMAIL#" subject="#mailStruct.SUBJECT#" bcc="#mailStruct.BCC#" type="text/html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">

					<cfinclude template="/fr/consotel/consoview/M01/#M01_UUID#/#myFile#">

				</cfmail>

			<cfcatch>

				<!--- En cas d'échec de l'op?ration envoi d'un mail de notification --->
				<cfmail server="mail-cv.consotel.fr" port="25" from="login-mdp@saaswedo.com" to="monitoring@saaswedo.com"
							failto="postmaster@saaswedo.com" subject="[WARN-LOGIN_M01]Erreur lors de l'envoi du mail" type="html">

						#CFCATCH.Message#<br/><br/><br/><br/>

						Chemin 	: <cfdump var="#pathEntier#"><br/><br/>
						Fichier : <cfdump var="#myFile#"><br/><br/>
						UUIDLOG : <cfdump var="#uuidlog#"><br/><br/>
						Mail 	: <cfdump var="#mailStruct#"><br/><br/>

				</cfmail>

			</cfcatch>
			</cftry>

			<cftry>

				<cfdirectory name="dGetDir" directory="#root#" action="List">

				<cfloop query="dGetDir">

					<cfif type NEQ "DIR">

						<cffile action="delete" file="#root#/#dGetDir.name#">

					</cfif>

				</cfloop>

				<cfdirectory action="delete" directory="#myDir##M01_UUID#">

			<cfcatch>

				<!--- En cas d'échec de l'op?ration envoi d'un mail de notification --->
				<cfmail server="mail-cv.consotel.fr" port="25" from="login-mdp@saaswedo.com" to="monitoring@saaswedo.com"
							failto="postmaster@saaswedo.com" subject="[WARN-LOGIN_M01]Erreur lors de la destruction du répertoire" type="html">

						#CFCATCH.Message#<br/><br/><br/><br/>

						Chemin 	: <cfdump var="#pathEntier#"><br/><br/>
						Fichier : <cfdump var="#myFile#"><br/><br/>
						UUIDLOG : <cfdump var="#uuidlog#"><br/><br/>
						Mail 	: <cfdump var="#mailStruct#"><br/><br/>

				</cfmail>

			</cfcatch>
			</cftry>

	</cffunction>

	<!--- ENVOI LES MAILS D'ERREURS --->
	<cffunction access="public" name="procedureInError" returntype="void">
		<cfargument name="uuid" 		type="String" required="true">
		<cfargument name="typeevent" 	type="String" required="true">
		<cfargument name="mailJobId" 	type="String" required="true">
		<cfargument name="mailSubject" 	type="String" required="true">
		<cfargument name="mailContent" 	type="String" required="true">
		<cfargument name="procedure" 	type="String" required="true">
		<cfargument name="others" 		type="Any" 	  required="false" default="NONE">

			<cfset param 			 = structNew()>
			<cfset param.FROM 		 = 'login-mdp@saaswedo.com'>
			<cfset param.TOMAIL  	 = 'monitoring@saaswedo.com'>
			<cfset param.BCC	 	 = ''>
			<cfset param.SUBJECT 	 = #mailSubject#>

			<cfmail from="#param.FROM#" to="#param.TOMAIL#" subject="#param.SUBJECT#" bcc="#param.BCC#" type="text/html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
				#mailContent#<br><br>
				JOBID		: #mailJobId#<br><br>
				UUID		: #uuid#<br><br>
				EVENTTYPE 	: #typeevent#<br><br>
				PROCEDURE	: #procedure#<br><br>
				<cfdump var="#others#">
			</cfmail>

	</cffunction>

	<!--- VERIFIE SI LE LOGIN ET LE MOT DE PASSE A BIEN ETE GENERES --->
	<cffunction access="public" name="checkArePresentInRespository" returntype="Numeric">
		<cfargument name="pathLogin" 	type="String" required="true">
		<cfargument name="pathPassW" 	type="String" required="true">
		<cfargument name="pathTotal" 	type="String" required="true">

			<cfset rsltCheck = 0>

			<cfdirectory name="dGetDir" directory="#pathTotal#" action="List">

			<cfloop query="dGetDir">
					<cfset rsltCheck = rsltCheck + 1>
			</cfloop>

		<cfreturn rsltCheck>
	</cffunction>

</cfcomponent>
