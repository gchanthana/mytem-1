﻿<cfoutput>
Bonjour,<br/><br/><br/>
Veuillez trouvez ci-dessous le résumé des modifications effectuées par <b>#userInfos["USER"]#</b>
sur les informations MDM pour le groupe <b>#groupe#</b><br>
L'application effective de ces modifications peut dépendre d'une approbation préalable de votre support technique.
<br/><br/>
<b>Provider : </b>#serverManager.getProvider()#<br/>
<b>Serveur MDM : </b>#serverManager.getMdm()#<br/>
<b>Protocole : </b>#(serverManager.useSSL() ? "HTTPS":"HTTP")#<br/>
<b>Email Administrateur MDM : </b>#serverManager.getAdminEmail()#<br/>
<b>Login - WebService : </b>#serverManager.getUsername()#<br/>
<b>Mot de passe - WebService : </b>*************<br/>
<b>Login - Enrollment : </b>#serverManager.getEnrollUsername()#<br/>
<b>Mot de passe - Enrollment : </b>#serverManager.getEnrollPassword()#<br/><br/><br/>
Cordialement,<br/>
Le support technique.
</cfoutput>