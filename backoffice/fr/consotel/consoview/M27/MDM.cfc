<cfcomponent author="Cedric" displayname="fr.consotel.consoview.M27.MDM" extends="fr.saaswedo.api.patterns.cfc.Component"
hint="Implémentation contenant les fonctionnalités de gestion des serveurs MDM">
	<!--- Le constructeur init() est explicitement appelé à chaque instanciation --->
	<cfset init()>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.patterns.cfc.Component" hint="Constructeur qui est appelé explicitement
	lors de l'instanciation de ce composant avec createObject() : Une exception est levée si ce composant est instancié avec l'opérateur new.
	Cette implémentation appele le constructeur parent puis initialise les propriétés spéficiques à ce composant">
		<!--- Constructeur hérité de Component --->
		<cfset SUPER.init()>
		<!--- API MDM (Saaswedo) : La durée de vie de l'instance est celle de du remoting --->
		<cfset VARIABLES.REMOTING_MDM_INFOS=new fr.saaswedo.api.myTEM.mdm.MDM()>
		<!--- Implémentation SessionModel (M00) --->
		<cfset VARIABLES[getSessionModelKey()]=createObject("component","fr.consotel.consoview.M00.SessionModel")>
		<!--- Implémentation de l'API MyTEM --->
		<cfset VARIABLES[getMyTEMKey()]=createObject("component","fr.saaswedo.api.myTEM.mdm.MyTEM").getInstance()>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.consotel.consoview.M27.MDM">
		<cfreturn "fr.consotel.consoview.M27.MDM">
	</cffunction>

	<cffunction access="remote" name="getProviders" returntype="Array" hint="Retourne la liste des providers. ArrayOfStruct: NAME,VALUE">
		<cfset var serverManager=getMdmInfos()>
		<cfreturn serverManager.getProviders()>
	</cffunction>
	
	<cffunction access="remote" name="getMdmServerInfos" returntype="Struct" hint="Retourne les infos du serveur MDM de la racine">
		<cfset var idGroupe=getSessionModel().getIdGroupe()>
		<cfset var serverManager=getMdmInfos()>
		<cfreturn serverManager.getMdmServerInfos(idGroupe)>
	</cffunction>
	
	<cffunction access="remote" name="resetMdmServerInfos" returntype="Struct"
	hint="Retourne et par défaut remplace les valeurs par défaut des infos MDM pour la racine en session">
		<cfargument name="updateValues" type="Boolean" required="false" default="true" hint="TRUE pour remplacer les valeurs existantes et FALSE sinon">
		<cfset var idGroupe=getSessionModel().getIdGroupe()>
		<cfset var userInfos=getSessionModel().getUser()>
		<cfset var serverManager=getMdmInfos()>
		<cfset var mdmProps=serverManager.resetMdmServerInfos(idGroupe,userInfos["USERID"],ARGUMENTS["updateValues"])>
		<!--- Notification si la réinitialisation est enregistrée --->
		<cfif ARGUMENTS["updateValues"]>
			<!--- Mise à jour forcée des infos MDM en session --->
			<cfset serverManager.forceMdmInfosUpdate()>
			<cfset sendUpdateMdmInfosNotification(TRUE)>
		</cfif>
		<cfreturn mdmProps>
	</cffunction>
	
	<cffunction access="remote" name="updateMdmServerInfos" returntype="Struct"
	hint="Met à jour les valeurs des infos MDM<br>Retourne une structure contenant les clés :
	IS_TEST_OK : TRUE si le test de connexion avec le serveur MDM a été effectué avec succès et FALSE sinon<br/>
	TEST_MSG : NONE si le test de connexion n'a pas été effectué et sinon il correspond au message associé au test">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Structure contenant les infos MDM avec leur valeurs">
		<cfargument name="checkConnection" type="Boolean" required="false" default="false" hint="TRUE pour effectuer un test de connexion">
		<cfset var mdmProps=ARGUMENTS["mdmInfos"]>
		<cfset var idGroupe=getSessionModel().getIdGroupe()>
		<cfset var userInfos=getSessionModel().getUser()>
		<cfset var serverManager=getMdmInfos()>
		<!--- Mise à jour des infos MDM --->
		<cfset mdmProps["IS_TEST_OK"]=FALSE>
		<cfset mdmProps["TEST_MSG"]="NONE">
		<cfset serverManager.updateMdmServerInfos(idGroupe,userInfos["USERID"],mdmProps)>
		<!--- Mise à jour forcée des infos MDM en session --->
		<cfset serverManager.forceMdmInfosUpdate()>
		<!--- Résultat par défaut du test de connexion du serveur MDM --->
		<cfset var connectionTestInfos={IS_TEST_OK=createObject("java","java.lang.Boolean").init("false"),TEST_MSG="NONE"}>
		<!--- Test de connexion à effectuer --->
		<cfif ARGUMENTS["checkConnection"]>
			<!--- Test de connexion MDM : Toute erreur ou echec de connexion sera stockée par l'API MDM --->
			<cfset var mdmServer=serverManager.getIMDM()>
			<cfset connectionTestInfos=mdmServer.checkConnection()>
		</cfif>
		<!--- Notification et Résultats du test de connexion --->
		<cfset sendUpdateMdmInfosNotification(FALSE)>
		<cfreturn connectionTestInfos>
	</cffunction>
	
	<cffunction access="remote" name="isApiAvailable" returntype="Boolean" hint="Vérifie si l'API est disponible par rapport aux infos du client distant">
		<cfargument name="groupeInfos" type="String" required="false" default="" hint="Infos concernant le client"/>
		<cftry>
			<cfset getSessionModel().setRemoteInfos(ARGUMENTS.groupeInfos)>
			<cfreturn TRUE>
			<cfcatch type="Any">
				<cflog type="error" file="eventgateway" text="///FAILED : #ARGUMENTS.groupeInfos# - #CFCATCH.message# [#CFCATCH.detail#]///">
				<cfreturn FALSE>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="private" name="sendUpdateMdmInfosNotification" returntype="void"
	hint="Envoi le résumé des infos MDM à l'email de l'utilisateur connecté et en copie celui de l'administrateur MDM">
		<cfargument name="isMdmInfosReset" type="Boolean" required="true" hint="TRUE si la mise à jour est une réinitialisation par défaut et FALSE sinon">
		<cfset var userInfos=getSessionModel().getUser()>
		<cfset var groupe=getSessionModel().getGroupe()>
		<cfset var serverManager=getMdmInfos()>
		<cfset var updateType=ARGUMENTS["isMdmInfosReset"] ? "Réinitialisation":"Mise à jour">
		<!--- Récupération des paramètres mail --->
		<cfset var mailAttributes={type="html"}>
		<cfset var mailFrom="monitoring@saaswedo.com">
		<cftry>
			<cfset var procedureName="pkg_m00.get_appli_properties">
		     <cfstoredproc datasource="ROCOFFRE" procedure="#procedureName#">
		    	<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_code_appli" value="#userInfos.CODEAPP#">
		        <cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_key" null="true">
		        <cfprocresult name="qMailParams">
		    </cfstoredproc>
		    <cfif qMailParams.recordcount GT 0>
				<cfloop index="i" from="1" to="#qMailParams.recordcount#">
					<cfif qMailParams["KEY"][i] EQ "MAIL_SERVER">
						<cfset mailAttributes["server"]=qMailParams["VALUE"][i]>
					<cfelseif qMailParams["KEY"][i] EQ "MAIL_PORT">
						<cfset mailAttributes["port"]=qMailParams["VALUE"][i]>
					<cfelseif qMailParams["KEY"][i] EQ "MAIL_EXP_N1">
						<cfset mailFrom=qMailParams["VALUE"][i]>
					</cfif>
				</cfloop>
				<cflog type="information"
					text="[M27] Mail parameters (SERVER:#mailAttributes.server#,PORT:#mailAttributes.port#,FROM:#mailFrom#)"> 
		    </cfif>
			<cfcatch type="Any">
				<cflog type="Error" text="[M27] #procedureName# failed : #CFCATCH.message# [#CFCATCH.detail#]"> 
			</cfcatch>
		</cftry>
		 
		<!--- Envoi de la notification mail --->
		<cfset var mailTo=userInfos.USER_EMAIL>
		<cfset var mdm_template = 'mdm_properties.cfm'>
		<cfset var path = expandPath('./') >
		<cfset var localized_template_dir = 'messages/' & left(SESSION.USER.GLOBALIZATION,2)>
		<cfset var localized_template_path = localized_template_dir & '/' & mdm_template>
		<cfset var default_template_path =  'messages/en/' & mdm_template> 
		<cfset var template_path = default_template_path>
		
		<cfif FileExists( path & localized_template_dir & '/' & mdm_template)>
			<cfset var template_path = localized_template_path>
		</cfif>
		
		<cfmail from="Notification MDM<#mailFrom#>" to="#mailTo#" cc="#serverManager.getAdminEmail()#"
			subject="[MDM] #updateType# d'informations MDM : #groupe#" attributeCollection="#mailAttributes#">
			<cfinclude template="#template_path#">
		</cfmail>
	</cffunction>

	<!--- =========== Accès aux infos MDM de session =========== --->

	<cffunction access="private" name="getMdmInfos" returnType="fr.saaswedo.api.mdm.core.services.IMdmServerInfo" hint="API MDM : IMdmServerInfo">
		<cfset var mdmInfoKey="REMOTING_MDM_INFOS">
		<cfif structKeyExists(VARIABLES,mdmInfoKey)>
			<cfreturn VARIABLES[mdmInfoKey]>
		<cfelse>
			<cfset var code=GLOBALS().CONST("MDM.ERROR")>
			<cfthrow type="Custom" errorcode="#code#" message="IMdmServerInfo (API MDM) instance is not defined">
		</cfif>
	</cffunction>

	<cffunction access="private" name="getSessionModel" returntype="fr.consotel.consoview.M00.SessionModel" hint="Retourne l'instance SessionModel">
		<cfreturn VARIABLES[getSessionModelKey()]>
	</cffunction>

	<cffunction access="private" name="getSessionModelKey" returntype="String" hint="Clé utilisée qui référence l'instance SessionModel (M00)">
		<cfreturn "SESSION_MODEL">
	</cffunction>

	<cffunction access="private" name="getMyTEM" returntype="fr.saaswedo.api.myTEM.mdm.MyTEM" hint="Retourne l'instance de l'API MyTEM">
		<cfreturn VARIABLES[getMyTEMKey()]>
	</cffunction>

	<cffunction access="private" name="getMyTEMKey" returntype="String" hint="Clé qui référence l'instance de l'API MyTEM">
		<cfreturn "API_MYTEM">
	</cffunction>
</cfcomponent>