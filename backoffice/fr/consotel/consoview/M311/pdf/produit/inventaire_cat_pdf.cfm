<cfset dataset = queryNew("nbrecord,siteid,type_theme,site,sous_tete,commentaires,nbappels,qte,volume,montant,total_montant,total_volume,total_quantite,total_nbappels,numero_facture,idproduit_catalogue,idsous_tete,idinventaire_periode")/>

	<cfif structkeyexists(SESSION,"PARAMS_EXPORT")>
	
	<cfstoredproc datasource="E0" procedure="E0.pkg_M311.getPaginateDetailProduit">		
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PARAMS_EXPORT.IDRACINE_MASTER#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PARAMS_EXPORT.IDRACINE#" null="false">			
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PARAMS_EXPORT.IDPERIMETRE#" null="false">			
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.PARAMS_EXPORT.NIVEAU#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PARAMS_EXPORT.IDORGA#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PARAMS_EXPORT.IDPRODUIT#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PARAMS_EXPORT.IDDEBUT#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PARAMS_EXPORT.IDFIN#" null="false">
		<!--- Paramètres du paginate --->
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.PARAMS_EXPORT.ORDER_BY_TEXTE_COLONNE#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.PARAMS_EXPORT.ORDER_BY_TEXTE#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.PARAMS_EXPORT.SEARCH_TEXT_COLONNE#" null="false">	
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.PARAMS_EXPORT.SEARCH_TEXT#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PARAMS_EXPORT.OFFSET#" null="false">														
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PARAMS_EXPORT.LIMIT#" null="false">				
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.PARAMS_EXPORT.LANG#" null="false">							
		<cfprocresult name="p_result">
	</cfstoredproc>
		
	<cfquery name="dataset" dbtype="query">
		select
		ROW_TOTAL as nbrecord,
		C1 as type_theme,
		C8 as site,
		c8 as nom_site,
		C9 as adresse1,
		C10 as adresse2,
		C11 as zipcode,
		C12 as commune,
		C7 as siteid,
		C5 as sous_tete,
		C6 as commentaires,
		C17 as nbappels ,
		C17 as nombre_appel ,
		C15 as  qte,
		C18 as  volume,
		C18 as duree_appel,
		C16  as  montant  ,		
		C16 as montant_final,
		C16_TOTAL as  total_montant ,
		C18_TOTAL as  total_volume,
		C15_TOTAL as  total_quantite,
		C17_TOTAL as  total_nbappels ,
		C14 as  numero_facture,		
		C2 as  idproduit_catalogue , 			
		C4 as  idsous_tete  , 
		C13 as  idinventaire_periode	 						 
		from p_result			 
	</cfquery>
</cfif>

<cfset usage = "">
<cfif tb eq "mobile"><cfset usage = "Collaborateur"><cfelse><cfset usage = "Fonction" ></cfif>	
<cfset obj = CreateObject("component","fr.consotel.consoview.M311.produits.produit")>				
<cfset obj.init(perimetre,modeCalcul)>			 
<cfset produit="#obj.getTypeProduit_cat(idproduit_client)# > #obj.getProduit_cat(idproduit_client)# : #obj.getOperateur_cat(idproduit_client)#">
<style type="text/css">
table.sample {
	font-size : 12px;
	border-width: 1px 0px 0px 0px;
	border-spacing: 0px;
	border-style: none;
	border-color: gray gray gray gray;
	border-collapse: collapse;
	background-color: white;
	padding : 0px;
	spacing : 0px;
	
}
 
table.sample th {
	 
	border-collapse: collapse;
	border-width: 0px 0px 0px 0px;
	spacing : 0px 0px 0px 0px;
	padding: 0px 1px 1px 1px;
	border-style: solid solid solid solid;
	border-color: gray gray gray gray;
	background-color: #003366;
	color: white;
	-moz-border-radius: 0px 0px 0px 0px;
}

 

table.sample td {
	 
	border-width: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	cellspacing : 0px 0px 0px 0px;
	border-style: solid solid solid solid;
	border-color: gray gray gray gray;
	background-color: white;
	-moz-border-radius: 0px 0px 0px 0px;
}

</style>

<table align="center" width="100%" >
	<tr>	 
	<cfif compareNocase(dataset.TYPE_THEME,"Abonnements") eq  0>
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><cfoutput><strong>#produit#</cfoutput></strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">
								<th>&nbsp;Site&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Ligne&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;<cfoutput>#usage#</cfoutput>&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Quantité&nbsp;</th>								
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Montant (&euro; H.T)&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;N°&nbsp;Facture&nbsp;</th>
							</tr>
							<cfset montant_abo_sous_total=0>
							<cfset nbr_abo_sous_total=0>
							<cfset montant_abo_total=0>
							<cfset nbr_abo_total=0>
							<cfset numRow = 0>
							<cfoutput query="dataset" group="siteID">
							
								<cfset vTemp=1>
								<cfoutput>
									<cfset vTemp=vTemp+1>
								</cfoutput>
							<tr>
								<td class="grid_normal" rowspan="#vTemp#" valign="top">
								&nbsp;#nom_site#&nbsp;<br>
								&nbsp;#adresse1#&nbsp;<br>
								<cfif len(trim(adresse2)) gt 0>
								&nbsp;#adresse2#&nbsp;<br>
								</cfif>
								&nbsp;#zipcode#&nbsp;#commune#&nbsp;
								</td>
							</tr>
									<cfoutput>
							<tr>
								<td width="1" bgcolor="808080"></td>
								<td class="grid_normal" valign="top">&nbsp;#sous_tete#&nbsp;</td>							
								<td width="1" bgcolor="808080"></td>
								<td class="grid_normal" valign="top">&nbsp;#commentaires#&nbsp;</td>
								<td width="1" bgcolor="808080"></td>
								<td class="grid_normal" align="right" valign="top">&nbsp;
								#LsNumberFormat(val(qte), '9,999,999,999')#
								&nbsp;</td>
								<td width="1" bgcolor="808080"></td>										
								<td class="grid_normal" align="right" valign="top">&nbsp;
								#LsEuroCurrencyFormat(val(montant_final), 'none')# &euro;
								&nbsp;</td>
								<td width="1" bgcolor="808080"></td>
								<td class="grid_normal" align="right" valign="top">&nbsp;
								#numero_facture#&nbsp;</td>
							</tr>
									<cfset montant_abo_total=montant_abo_total+val(montant_final)>
									<cfset nbr_abo_total=nbr_abo_total+val(qte)>
									<cfset montant_abo_sous_total= montant_abo_sous_total+val(montant_final)>
									<cfset nbr_abo_sous_total=nbr_abo_sous_total+val(qte)>								 	
									</cfoutput>
							<tr><td colspan="11" height="1" bgcolor="808080"></td></tr>
								<cfset numRow = numRow +1>
								<cfif (numRow mod 7) eq 0>									
									<tr>
										<th align="left" colspan="5" class="th_stt">&nbsp;TOTAL page&nbsp;</th>									
										<td width="1" bgcolor="808080"></td>
										<th align="right" class="th_stt">&nbsp;#LsNumberFormat(val(nbr_abo_sous_total), '9,999,999,999')#&nbsp;</th>
										<td width="1" bgcolor="808080"></td>
										<th align="right" class="th_stt">&nbsp;#LsEuroCurrencyFormat(val(montant_abo_sous_total), 'none')# &euro;&nbsp;</th>
										<td width="1" bgcolor="808080"></td>
										<th align="right" class="th_stt"></th>
									</tr>
									<tr>
										<th align="left" colspan="5">&nbsp;TOTAL cumulé&nbsp;</th>									
										<td width="1" bgcolor="808080"></td>
										<th align="right">&nbsp;#LsNumberFormat(val(nbr_abo_total), '9,999,999,999')#&nbsp;</th>
										<td width="1" bgcolor="808080"></td>
										<th align="right">&nbsp;#LsEuroCurrencyFormat(val(montant_abo_total), 'none')# &euro;&nbsp;</th>
										<td width="1" bgcolor="808080"></td>
										<th align="right"></th>
									</tr>
								</table>
							</td>
						</tr> 
					</table>
				</td>
			</tr>
		</table>
		<cfset montant_abo_sous_total=0>
		<cfset nbr_abo_sous_total=0>
		<cfdocumentitem type="pagebreak"/>
		<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><strong>#produit#</strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">
								<th>&nbsp;Site&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Ligne&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;#usage#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Quantit&eacute;&nbsp;</th>								
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Montant (&euro; H.T)&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;N°&nbsp;Facture&nbsp;</th>
							</tr>						
																	
								</cfif>
								 
								
							
							</cfoutput>
							
							
							<cfoutput>
							<tr>
								<th align="left" colspan="5" class="th_stt">&nbsp;TOTAL page&nbsp;</th>									
								<td width="1" bgcolor="808080"></td>
								<th align="right" class="th_stt">&nbsp;#LsNumberFormat(val(nbr_abo_sous_total), '9,999,999,999')#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right" class="th_stt">&nbsp;#LsEuroCurrencyFormat(val(montant_abo_sous_total), 'none')# &euro;&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right" class="th_stt"></th>
							</tr>
							<tr>
								<th align="left" colspan="5">&nbsp;TOTAL cumul&eacute;&nbsp;</th>									
								<td width="1" bgcolor="808080"></td>
								<th align="right">&nbsp;#LsNumberFormat(val(nbr_abo_total), '9,999,999,999')#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right">&nbsp;#LsEuroCurrencyFormat(val(montant_abo_total), 'none')# &euro;&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right" ></th>								
							</tr>
							</cfoutput>
						</table>
					</td>
				</tr>
			</table>
		</td>

<cfelse>



	<!--- Tableau des consommations par type --->
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><cfoutput><strong>#produit#</cfoutput></strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">
								<th>&nbsp;Site&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Ligne&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;<cfoutput>#usage#</cfoutput>&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Nombre Appels&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Volume (min ou kO)&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Montant (&euro;)&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;N°&nbsp;Facture&nbsp;</th>
							</tr>
							<cfset montant_conso_sous_total=0>
							<cfset nbr_conso_sous_total=0>
							<cfset duree_conso_sous_total=0>
														
							<cfset montant_conso_total=0>
							<cfset nbr_conso_total=0>
							<cfset duree_conso_total=0>
							
							<cfset numRow = 0>
							
							<cfoutput query="dataset" group="siteID">
								<cfset vTemp=1>
								<cfoutput>
									<cfset vTemp=vTemp+1>
								</cfoutput>
								<tr>
									<td class="grid_normal" rowspan="#vTemp#" valign="top">
									&nbsp;#nom_site#&nbsp;<br>
									&nbsp;#adresse1#&nbsp;<br>
									<cfif len(trim(adresse2)) gt 0>
									&nbsp;#adresse2#&nbsp;<br>
									</cfif>
									&nbsp;#zipcode#&nbsp;#commune#&nbsp;
									</td>
								</tr>
									<cfoutput>
									<tr>
										<td width="1" bgcolor="808080"></td>
										<td class="grid_normal" valign="top">&nbsp;#sous_tete#&nbsp;</td>										
										<td width="1" bgcolor="808080"></td>
										<td class="grid_normal" valign="top">&nbsp;#commentaires#&nbsp;</td>
										<td width="1" bgcolor="808080"></td>
										<td class="grid_normal" align="right" valign="top">&nbsp;
										#LsNumberFormat(val(nombre_appel), '9,999,999,999')#
										&nbsp;</td>
										<td width="1" bgcolor="808080"></td>
										<td class="grid_normal" align="right" valign="top">&nbsp;
										<cfif duree_appel gt 0>
											#LsNumberFormat(int(val(duree_appel)), '9,999,999,999')#
										<cfelse>
											0
										</cfif>
										 
										&nbsp;</td>
										<td width="1" bgcolor="808080"></td>
										<td class="grid_normal" align="right" valign="top">&nbsp;
										#LsEuroCurrencyFormat(val(montant_final), 'none')# &euro;
										&nbsp;</td>
										<td width="1" bgcolor="808080"></td>
										<td class="grid_normal" align="right" valign="top">&nbsp;
										#numero_facture#&nbsp;</td>
									</tr>
									<cfset montant_conso_sous_total=montant_conso_sous_total+val(montant_final)>
									<cfset nbr_conso_sous_total=nbr_conso_sous_total+val(nombre_appel)>
									<cfset duree_conso_sous_total=duree_conso_sous_total+int(val(duree_appel))>
									
									<cfset montant_conso_total=montant_conso_total+val(montant_final)>
									<cfset nbr_conso_total=nbr_conso_total+val(nombre_appel)>
									<cfset duree_conso_total=duree_conso_total+int(val(duree_appel))>
									</cfoutput>
							<tr><td colspan="13" height="1" bgcolor="808080"></td></tr>
							<cfset numRow = numRow +1>
							<cfif (numRow mod 1) eq 0>
							<tr>
								<th align="left" colspan="5" class="th_stt">&nbsp;TOTAL page&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right" class="th_stt">&nbsp;#LsNumberFormat(val(nbr_conso_sous_total), '9,999,999,999')#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right" class="th_stt">&nbsp;#LsNumberFormat(val(duree_conso_sous_total), '9,999,999,999')#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right" class="th_stt">&nbsp;#LsEuroCurrencyFormat(val(montant_conso_sous_total), 'none')# &euro;&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th class="th_stt"></th>
							</tr>		
							<tr>
								<th align="left" colspan="5">&nbsp;TOTAL cumul&eacute;&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right">&nbsp;#LsNumberFormat(val(nbr_conso_total), '9,999,999,999')#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right">&nbsp;#LsNumberFormat(val(duree_conso_total), '9,999,999,999')#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right">&nbsp;#LsEuroCurrencyFormat(val(montant_conso_total), 'none')# &euro;&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th></th>
							</tr>							
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
								<cfset montant_conso_sous_total=0>
								<cfset nbr_conso_sous_total=0>
								<cfset duree_conso_sous_total=0>
								<cfdocumentitem type="pagebreak"/>
		 
		<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><strong>#produit#</strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">
								<th>&nbsp;Site&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Ligne&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;#usage#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Nombre Appels&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Volume (min ou kO)&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;Montant (&euro;)&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th>&nbsp;N°&nbsp;Facture&nbsp;</th>
							</tr>	
							</cfif>								 
							</cfoutput>
							<cfoutput>
							<tr>
								<th align="left" colspan="5" class="th_stt">&nbsp;TOTAL page&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right" class="th_stt">&nbsp;#LsNumberFormat(val(nbr_conso_sous_total), '9,999,999,999')#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right" class="th_stt">&nbsp;#LsNumberFormat(val(duree_conso_sous_total), '9,999,999,999')#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right" class="th_stt">&nbsp;#LsEuroCurrencyFormat(val(montant_conso_sous_total), 'none')# &euro;&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th class="th_stt"></th>
							</tr>		
							<tr>
								<th align="left" colspan="5">&nbsp;TOTAL cumul&eacute;&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right">&nbsp;#LsNumberFormat(val(nbr_conso_total), '9,999,999,999')#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right">&nbsp;#LsNumberFormat(val(duree_conso_total), '9,999,999,999')#&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th align="right">&nbsp;#LsEuroCurrencyFormat(val(montant_conso_total), 'none')# &euro;&nbsp;</th>
								<td width="1" bgcolor="808080"></td>
								<th></th>
							</tr>			
							</cfoutput>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</cfif>
</table>
