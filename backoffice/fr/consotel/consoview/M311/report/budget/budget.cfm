<!---

	Codification des noms des rapports dans Optimisation (Nom du rapport - REPORT_TYPE) :
	
	Suivi Budgétaire :
	Rapport Budgétaire mensuel feuilles - BudgetByNode - GroupeLigne, GroupeAna
	
--->

<cfset reportContext = createObject("component","fr.consotel.consoview.facturation.budget.BudgetReportContext")>
<cfset reportContext.displayRapport(FORM.PERIMETRE_INDEX,FORM.TYPE_PERIMETRE,
											FORM.REPORT_TYPE,FORM.RAISON_SOCIALE)>
