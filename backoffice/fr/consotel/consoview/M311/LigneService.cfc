﻿<cfcomponent displayname="LigneService" output="false" >

	<cffunction name="getNbreLigneParSegment" access="remote" returntype="struct" description="retourne le nombre de ligne par segment Fixe, Mobile et Data">
		
		<cfset idRacine 		= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idRacineMaster 	= SESSION.PERIMETRE.IDRACINE_MASTER>
		<cfset idOrganisation 	= SESSION.PERIMETRE.SELECTED_ID_ORGA>
		<cfset idperimetre 		= SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset niveau			= SESSION.PERIMETRE.SELECTED_NIVEAU>
		<cfset idDebut		=SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB>
		<cfset idFin		=SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN>
		<cfset codeLangue 		= SESSION.USER.GLOBALIZATION>
		
		<!--- Dans PKG_M54
		PROCEDURE getLigneParSegmentTB(p_idracine IN INTEGER,
                               p_idracine_master IN INTEGER,
                               p_idorga IN integer,
                               p_idperimetre IN integer,
                               p_niveau IN varchar2,
                               p_idperiode_deb IN INTEGER,
                               p_idperiode_fin IN INTEGER,
                               p_segment       IN INTEGER,
                               p_code_langue IN VARCHAR2,
                               p_retour OUT sys_refcursor
			 --->
		<cfset tabResult = ArrayNew(1)>
		<cfloop from="1" to="3" index="i">
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M54.getLigneParSegmentTB">
				<cfprocparam value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#idRacineMaster#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#idOrganisation#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#idperimetre#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#niveau#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#idDebut#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#idFin#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#i#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			<cfset ArrayAppend(tabResult,#p_retour.NB_LIGNES#)>
		</cfloop>
		
		<cfset NB_LignesParSegment = structNew()>
		<cfset NB_LignesParSegment.Fixe = #tabResult[1]#>
		<cfset NB_LignesParSegment.Mobile = #tabResult[2]#>
		<cfset NB_LignesParSegment.Data = #tabResult[3]#>
				
		<cfreturn NB_LignesParSegment>
		
	</cffunction>
</cfcomponent>