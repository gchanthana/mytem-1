<!--- =========================================================================
Name: SousTeteFacturationStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent extends="AbstractStrategy" displayname="SousTeteFacturationStrategy" hint="">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="SousTeteFacturationStrategy" displayname="SousTeteFacturationStrategy init()" hint="Initialize the SousTeteFacturationStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getProduit" access="public" returntype="string" output="false" displayname="string getProduit(numeric)" >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qGetDetailTheme" datasource="#session.OffreDSN#">
			SELECT th.*, pcl.libelle_produit
			FROM theme_produit th, theme_produit_catalogue tpc, produit_catalogue pca, produit_client pcl
			WHERE th.idtheme_produit=tpc.idtheme_produit
			AND tpc.idproduit_catalogue=pca.idproduit_catalogue
			AND pca.idproduit_catalogue=pcl.idproduit_catalogue
			AND pcl.idproduit_catalogue=#IDproduit#
		</cfquery>
		<cfreturn qGetDetailTheme.libelle_produit>
	</cffunction>
	
	<cffunction name="getTypeProduit" access="public" returntype="string" output="false" displayname="string getTypeProduit(numeric)" >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qGetDetailTheme" datasource="#session.OffreDSN#">
			SELECT th.*, pcl.libelle_produit
			FROM theme_produit th, theme_produit_catalogue tpc, produit_catalogue pca, produit_client pcl
			WHERE th.idtheme_produit=tpc.idtheme_produit
			AND tpc.idproduit_catalogue=pca.idproduit_catalogue
			AND pca.idproduit_catalogue=pcl.idproduit_catalogue
			AND pcl.idproduit_catalogue=#IDproduit#
		</cfquery>
		<cfreturn qGetDetailTheme.theme_libelle>
	</cffunction>
	
	<cffunction name="getOperateur" access="public" returntype="string" output="false" displayname="string getProduit(numeric)" >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qgetOperateur" datasource="#session.OFFREDSN#">
			SELECT o.nom
			FROM produit_catalogue pca, operateur o
			WHERE pca.idproduit_catalogue=#IDproduit#
			AND pca.operateurID=o.operateurID
		</cfquery>
		<cfreturn qgetOperateur.nom>
	</cffunction>
	
	<cffunction name="getCompte" access="public" returntype="string" output="false" displayname="string getCompte(numeric)" >
		<cfargument name="IDcompte" required="true" type="numeric" default="" displayname="numeric IDcompte" hint="Initial value for the IDcompteproperty." />
		<cfquery name="qGetCompte" datasource="#session.OffreDSN#">
			select sous_tete
			from sous_tete st
			where st.idsous_tete=#IDcompte#
		</cfquery>
		<cfreturn qGetCompte.sous_tete>
	</cffunction>
	
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, date, date)" >
		<cfargument name="ID" 		 required="true" type="numeric" default="" displayname="numeric ID" 		hint="Initial value for the IDproperty."/>
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit"  hint="Initial value for the IDproduitproperty."/>
		<cfargument name="datedeb" 	 required="true" type="string"  default="" displayname="date DateDebut"	 	hint="Initial value for the DateDebutproperty."/>
		<cfargument name="datefin" 	 required="true" type="string"  default="" displayname="date DateFin" 		hint="Initial value for the DateFinproperty."/>

			<cfset periE0 	= CreateObject("component","fr.consotel.consoview.M311.PerimetreE0")>
			<cfset p_result = periE0.executQuery(4, IDproduit, 0, "", 
												session.perimetre.IDRACINE_MASTER,
												session.perimetre.ID_GROUPE,
												ID,
												datedeb,
												datefin,
												"SousTete")>
		<cfreturn p_result/>
	</cffunction>
	
	<cffunction name="getData_mobile" access="public" returntype="query" output="false" displayname="query getData(numeric, date, date)" >
		<cfargument name="ID" 		 required="true" type="numeric" default="" displayname="numeric ID" 		hint="Initial value for the IDproperty."/>
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit"  hint="Initial value for the IDproduitproperty."/>
		<cfargument name="datedeb" 	 required="true" type="string"  default="" displayname="date DateDebut"	 	hint="Initial value for the DateDebutproperty."/>
		<cfargument name="datefin" 	 required="true" type="string"  default="" displayname="date DateFin" 		hint="Initial value for the DateFinproperty."/>

			
			<cfset periE0 	= CreateObject("component","fr.consotel.consoview.M311.PerimetreE0")>
			<cfset p_result = periE0.executQuery(6, IDproduit, 0, "", 
												session.perimetre.IDRACINE_MASTER,
												session.perimetre.ID_GROUPE,
												ID,
												datedeb,
												datefin,
												"SousTete")>

		<cfreturn p_result/>
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)">
		
		<cfreturn "Ligne">
	</cffunction>
	
</cfcomponent>