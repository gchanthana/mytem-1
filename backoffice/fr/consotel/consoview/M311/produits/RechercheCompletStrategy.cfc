<!--- =========================================================================
Name: RechercheFacturationStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent extends="AbstractStrategy" displayname="RechercheFacturationStrategy" hint="">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="RechercheFacturationStrategy" displayname="RechercheFacturationStrategy init()" hint="Initialize the RechercheFacturationStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getProduit" access="public" returntype="string" output="false" displayname="string getProduit(numeric)" >
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qGetDetailTheme" datasource="#session.OFFREDSN#">
			SELECT th.*, pcl.libelle_produit
			FROM theme_produit th, theme_produit_catalogue tpc, produit_catalogue pca, produit_client pcl
			WHERE th.idtheme_produit=tpc.idtheme_produit
			AND tpc.idproduit_catalogue=pca.idproduit_catalogue
			AND pca.idproduit_catalogue=pcl.idproduit_catalogue
			AND pcl.idproduit_client=#IDproduit#
		</cfquery>
		<cfreturn qGetDetailTheme.libelle_produit>
	</cffunction>
	
	<cffunction name="getOperateur" access="public" returntype="string" output="false" displayname="string getProduit(numeric)" >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qgetOperateur" datasource="#session.OFFREDSN#">
			SELECT o.nom
			FROM produit_catalogue pca, produit_client pcl, operateur o
			WHERE pca.idproduit_catalogue=pcl.idproduit_catalogue
			AND pcl.idproduit_client=#IDproduit#
			AND pca.operateurID=o.operateurID
		</cfquery>
		<cfreturn qgetOperateur.nom>
	</cffunction>
	
	<cffunction name="getTypeProduit" access="public" returntype="string" output="false" displayname="string getTypeProduit(numeric)" >
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qGetDetailTheme" datasource="#session.OffreDSN#">
			SELECT th.*, pcl.libelle_produit
			FROM theme_produit th, theme_produit_catalogue tpc, produit_catalogue pca, produit_client pcl
			WHERE th.idtheme_produit=tpc.idtheme_produit
			AND tpc.idproduit_catalogue=pca.idproduit_catalogue
			AND pca.idproduit_catalogue=pcl.idproduit_catalogue
			AND pcl.idproduit_client=#IDproduit#
		</cfquery>
		<cfreturn qGetDetailTheme.theme_libelle>
	</cffunction>
	
	<cffunction name="getCompte" access="public" returntype="string" output="false" displayname="string getCompte(numeric)" >
		<cfargument name="IDcompte" required="true" type="numeric" default="" displayname="numeric IDcompte" hint="Initial value for the IDcompteproperty." />
		<cfquery name="qGetCompte" datasource="#session.OffreDSN#">
			select sous_tete
			from sous_tete st
			where st.idsous_tete=#IDcompte#
		</cfquery>
		<cfreturn qGetCompte.sous_tete>
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfreturn "Recherche">
	</cffunction>
	
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, date, date)" >
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfargument name="DateDebut" required="true" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="true" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetDetailProduit" datasource="#session.OffreDSN#">
			SELECT 	SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
		           	SUM(df.duree_appel) AS duree_appel, df.idproduit_client,
		           	pc.idproduit_catalogue, pca.prorata, st.sous_tete, sc.siteID,
		           	sc.nom_site, sc.adresse1, sc.adresse2,sc.zipcode, sc.commune, ip.numero_facture, th.type_theme ,ip.idinventaire_periode,st.idsous_tete
		    FROM 	detail_facture_abo df, produit_client pc, produit_catalogue pca, sous_tete st
		         	, site_client sc, sous_compte sco, inventaire_periode ip,theme_produit th, 
		         	theme_produit_catalogue tpc, compte_facturation cf
		    WHERE 	df.idinventaire_periode=ip.idinventaire_periode
		    		AND ip.idcompte_facturation=cf.idcompte_facturation
		    		AND cf.idcompte_facturation=sco.idcompte_facturation
		           	AND df.idproduit_client=pc.idproduit_client 
		           	AND df.idsous_tete=st.idsous_tete
		           	AND st.idsous_compte=sco.idsous_compte
		           	AND sco.siteid=sc.siteid
		           	AND th.idtheme_produit=tpc.idtheme_produit
					AND tpc.idproduit_catalogue=pca.idproduit_catalogue
		           	AND df.idproduit_client=#IDproduit#
		           	AND pca.idproduit_catalogue=pc.idproduit_catalogue
		           	AND trunc(ip.date_emission)<=trunc(last_day(#DateFin#))
						AND trunc(ip.date_emission)>=trunc(#DateDebut#,'MM')
		           	AND st.idsous_tete in (#ValueList(session.recherche.idsous_tete)#)
			GROUP BY df.idproduit_client, df.idinventaire_periode, pc.idproduit_catalogue, 
		           	pca.prorata, st.sous_tete, sc.nom_site, sc.adresse1, sc.adresse2,sc.zipcode, sc.commune,
				   	sc.siteID, numero_facture, th.type_theme,ip.idinventaire_periode,st.idsous_tete
		    ORDER BY montant_final desc, siteID,nom_site, adresse1, sous_tete
		</cfquery>
		<cfreturn qGetDetailProduit>
	</cffunction>
</cfcomponent>