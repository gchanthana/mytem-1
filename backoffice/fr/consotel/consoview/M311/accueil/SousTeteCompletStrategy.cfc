<!--- =========================================================================
Name: SousTeteCompletStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords$
========================================================================== --->
<cfcomponent displayname="SousTeteCompletStrategy" hint="" extends="Strategy">
<!--- ========================================================================= 
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="SousTeteCompletStrategy" displayname="RefclientAboStrategy init()" hint="Initialize the RefclientAboStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS 
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(integer, string, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfargument name="tb" required="false" type="string" default="complet" displayname="tb" />
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_LIGNE_V3.TB_ACCUEIL">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idSous_tete" value="#ID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_debut" value="#DateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_fin" value="#DateFin#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_tb" value="#tb#"/>			
	        <cfprocresult name="p_result"/>        
		</cfstoredproc>
				
		<cfreturn p_result/>	
	</cffunction>
	
	<cffunction name="getStrategy" returntype="string" access="public" output="false">
		<cfreturn "Ligne">
	</cffunction>
	
	<cffunction name="getLibelleAbo" returntype="string" access="public" output="false" hint="Raméne le périmétre de la requéte et le mode de calcul (Usage/facturation)">
		<cfreturn "Abonnements">
	</cffunction>
	
	<cffunction name="getLibelleConso" returntype="string" access="public" output="false" hint="Raméne le périmétre de la requéte et le mode de calcul (Usage/facturation)">
		<cfreturn "Consommations">
	</cffunction>
	
	<cffunction name="getNumero" returntype="string" access="public" output="false">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
			<cfquery name="qGetCompte" datasource="#session.OffreDSN#">
				select sous_tete
				from sous_tete st
				where st.idsous_tete=#ID#
			</cfquery>
			<cfreturn qGetCompte.sous_tete>
	</cffunction>
</cfcomponent>