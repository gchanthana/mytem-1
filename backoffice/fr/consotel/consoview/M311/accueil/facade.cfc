<cfcomponent name="facade" displayname="facade" alias="fr.consotel.consoview.M311.accueil.facade">
	
	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "/" & mid(oldDateString,4,2) & "/" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>

	<!--- INITIATLISATION --->
	<cffunction name="init" access="public">
		<cfargument name="perimetreParm" 	type="string"  required="false">
		<cfargument name="modeCalcul" 		type="string"  required="false">
		<cfargument name="numero" 			type="numeric" required="false">
		<cfargument name="datedeb" 			type="string"  required="false">
		<cfargument name="datefin" 			type="string"  required="false">
		<cfargument name="sousModeCalcul"	type="string"  required="false">
		
		
		<cfset perimetre = "">		
		<cfswitch expression="#ucase(perimetreParm)#">
			<cfcase value="SOUSTETE"> 
				<cfset perimetre = "SousTete">
			</cfcase>
			<cfcase value="GROUPELIGNE"> 
				<cfset perimetre = "GroupeLigne">
			</cfcase>
			<cfcase value="GROUPE"> 
				<cfset perimetre = "Groupe">
			</cfcase>
			<cfdefaultcase>
				<cfset perimetre = #perimetreParm#>
			</cfdefaultcase>
		</cfswitch>
			
		<cfset obj1 = CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>
		<cfset obj1.init(perimetre,modeCalcul)>

		<cfset periE0 = CreateObject("component","fr.consotel.consoview.M311.PerimetreE0")>
		<cfset dataset = periE0.executQuery(0, 0, 0, "", session.perimetre.IDRACINE_MASTER,	
											session.perimetre.ID_GROUPE,
											numero,
											transformDate(datedeb),
											transformDate(datefin),
											perimetre)>	
														
		<cfset setSessionValue("RECORDSET", dataset)>	
	</cffunction>
	
	<!--- RETOURNE LES SUR-THEMES ABOS --->	
	<cffunction name="getSurThemeAbos" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>		
		<cfset result = obj.getSurthemeAbos()>
		<cfreturn result>
	</cffunction>  
 	
	<!--- RETOURNE LES SUR-THEMES CONSOS --->	
	<cffunction name="getSurThemeConsos" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>		
		<cfset result = obj.getSurthemeConsos()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LES  THEMES CONSOS --->	
	<cffunction name="getConso" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>		
		<cfset result = obj.getConso()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LES  THEMES ABOS --->	
	<cffunction name="getAbo" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>	
		<cfset result = obj.getAbo()>
		<cfreturn result>
	</cffunction>  
	
	 
	
	<!--- RETOURNE LE  TOTAL DES MONTANT ABOS POUR CHAQUE SEGMENT--->	
	<cffunction name="getTotalConsosBySegment" access="public" returntype="query">
		<cfargument name="sousModeCalcul" type="string" required="false">
		<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>	
		<cfset result = obj.getTotalConsosBySegment()>
		<cfreturn result>
	</cffunction>

	<!--- RETOURNE LA REPARTITION PAR OPERATEUR POUR UN SEGMENT DONNE--->	
	<cffunction name="getRepartOperateurBySegment" access="public" returntype="query">		 
		<cfargument name="sousModeCalcul" 	type="string" 	required="false">
		<cfargument name="segmentid" 		type="numeric" 	required="false">
			<cfset obj = CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>		
			<cfset result = obj.getRepartOperateurBySegment(sousModeCalcul, segmentid)>			
		 
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LES  THEMES ABOS POUR UN SEGMENT DONNE--->	
	<cffunction name="getAboBySegment" access="public" returntype="query">
		<cfargument name="sousModeCalcul" 	type="string" 	required="false">
		<cfargument name="segmentid" 		type="numeric" 	required="false">
		<!--- <cfabort showError="ooo"> --->	
			<cfset obj = CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>	
			<cfset result = obj.getAboBySegment(sousModeCalcul, segmentid)>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LES  THEMES CONSOS POUR UN SEGMENT DONNE--->	
	<cffunction name="getConsoBySegment" access="public" returntype="query">
		<cfargument name="sousModeCalcul" 	type="string" 	required="false">
		<cfargument name="segmentid" 		type="numeric" 	required="false">
			<cfset obj = CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>		
			<cfset result = obj.getConsoBySegment(sousModeCalcul, segmentid)>
		<cfreturn result>
	</cffunction> 

	<!--- RETOURNE LA REPARTITION DES DUREE PAR OPERATEUR POUR UN SEGMENT DONNE--->	
	<cffunction name="getRepartDureeBySegment" access="public" returntype="query">		
		<cfargument name="sousModeCalcul" type="string" required="false">
		<cfargument name="segmentid" 		type="numeric" 	required="false">
			<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>		
			<cfset result = obj.getRepartDureeBySegment(sousModeCalcul, segmentid)>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LE  TOTAL DES MONTANT ABOS POUR CHAQUE SEGMENT--->	
	<cffunction name="getTotalAboBySegment" access="public" returntype="query">
		<cfargument name="sousModeCalcul" type="string" required="false">
		<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>	
		<cfset result = obj.getTotalAboBySegment()>
		<cfreturn result>
	</cffunction>
	
	

	<!--- RETOURNE LA REPARTITION PAR OPERATEUR --->	
	<cffunction name="getRepartOperateur" access="public" returntype="query">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		 
			<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>		
			<cfset result = obj.getRepartOperateur()>			
		 
		<cfreturn result>
	</cffunction>  



	
	<!--- RETOURNE LA REPARTITION DES DUREE PAR OPERATEUR --->	
	<cffunction name="getRepartDuree" access="public" returntype="query">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>		
		<cfset result = obj.getRepartDuree()>
		<cfreturn result>
	</cffunction>  
	

		
	
	<!--- RETOURNE L'EVOLUTION DES COUTS --->	
	<cffunction name="getEvolution" access="public" returntype="struct">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="periodicite" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		<cfset setSessionValue("periodicite_facture",#periodicite#)>  	
		 
		<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>
		<cfset obj.init(perimetre,modeCalcul)>
		<cfset result = obj.getEvolution(numero,
				transformDate(datedeb), 
				transformDate(datefin))>		
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LE LIBELLE ABOS --->	
	<cffunction name="getLibelleAbo" access="public" returntype="string">
		<cfargument name="Perimetre" type="string" required="true">
		<cfargument name="ModeCalcul" type="string" required="true">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>
		<cfset obj.init(perimetre,modeCalcul)>	
		<cfset result = obj.getLibelleAbo()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LE LIBELLE CONSO --->	
	<cffunction name="getLibelleConso" access="public" returntype="string">
		<cfargument name="Perimetre" type="string" required="true">
		<cfargument name="ModeCalcul" type="string" required="true">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.M311.accueil.Tableaubord")>		
		<cfset obj.init(perimetre,modeCalcul)>
		<cfset result = obj.getLibelleConso()>
		<cfreturn result>
	</cffunction> 
	
<!---::::::::::::::::::::::::::::::::::::::::::::::::::PRIVATE::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

	<cffunction name="setSessionValue" output="true" access="public" >
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset "session.#cle#"=valeur>
		<cfelse>
			<cfset structInsert(session,cle,valeur)>
		</cfif>
	</cffunction>


</cfcomponent>