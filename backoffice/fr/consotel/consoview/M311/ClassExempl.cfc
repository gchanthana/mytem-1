<cfcomponent output="false">

<!---
	GLOBAL STATIC
--->

	<cfset dataSourceName = "BI_TEST"><!--- A CHANGER EN FONCTION DU DATASOURCE --->
	
<!---
	GLOBAL
--->

	<cfset ws = "">
	<cfset sessionId = "">
	<cfset query = "">	
	<cfset idtdb = -1>

<!---
	FUNCTION PUBLIC
--->

	<cffunction name="init" access="public">
	
	<!--- IL FAUDRA INITIALISER LES VARIABLES ICI!! --->
	
	</cffunction>

	<cffunction name="createWebService" output="true" access="public">
			
			<cfset ws = CreateObject(	"webservice",
										"http://sun-bi.consotel.fr/analytics/saw.dll?WSDL",
										"SAWSessionServiceSoap",
										"--NStoPkg com.siebel.analytics.web/soap/v5=com.siebel.analytics.web.soap.v5")>
		 
		 <cfreturn ws>
	</cffunction>
	
	<cffunction name="logOn" output="true" access="public">
		<cfargument name="idTabBord" type="Numeric" required="true">
			
			<cftry>
					<cfset sessionId = ws.logon("consoview","public")>
					<cfset idtdb = idTabBord>
				<cfcatch>
					<cfset sessionId = 0>
					<cfset idtdb = -1>
				</cfcatch>
			</cftry>
			
		<cfreturn sessionId>
	</cffunction>	
	
	<cffunction name="getQueryByWebService" output="true" access="public">
			
			<cfset res = CreateObject(	"webservice",
										"http://sun-bi.consotel.fr/analytics/saw.dll?WSDL",
										"ReportEditingServiceSoap")>
			
			<cfset reportRef = zcreateComplexObject()>
			<cfset reportParams = structNew()>
 			<cfset query = res.generateReportSQL(reportRef, reportParams, sessionId)>
	
		<cfreturn query>
	</cffunction>
	
	<cffunction name="getQuery" output="true" access="public" returntype="Query">
		<cfargument name="p_idtheme" 				required="true" type="Numeric" default="0">
		<cfargument name="p_surtheme" 				required="true" type="String"  default="0">
		<cfargument name="p_idracine_master" 		required="true" type="Numeric" default="0">
		<cfargument name="p_idracine" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idperimetre" 			required="true" type="Numeric" default="0">
		<cfargument name="idDebut" 					required="true" type="Numeric" default="0">
		<cfargument name="idFin" 					required="true" type="Numeric" default="0">
		<cfargument name="idCliche" 				required="true" type="Numeric" default="0">
		<cfargument name="idorga" 					required="true" type="Numeric" default="0">
		
			<cfswitch expression="#idtdb#">
				<cfcase value="0">
					<!--- ACCEUIL --->
					<cfquery datasource="#dataSourceName#" name="queryrslt0">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idperiode_mois_debut	= #idDebut#,
										p_idperiode_mois_fin	= #idFin#,
										p_idcliche				= #idCliche#,
										p_idtheme				= #arguments.p_idtheme#,
										p_surtheme				= '#arguments.p_surtheme#';
										
					 	 SELECT FACTURATION.VOLUME_MN as duree_appel, 
									PRODUIT1GROUPE.IDTHEME_PRODUIT as idtheme_produit, 
									FACTURATION.MONTANT as montant_final, 
									PRODUIT1GROUPE.OPERATEUR_PRODUIT as nom, 
									FACTURATION.NOMBRE_APPEL as nombre_appel, 
									PRODUIT1GROUPE.ORDRE_AFFICHAGE as ordre_affichage, 
									FACTURATION.QTE as qte, 
									PRODUIT1GROUPE.SEGMENT_THEME as segment_theme, 
									PRODUIT1GROUPE.SUR_THEME as sur_theme, 
									PRODUIT1GROUPE.THEME as theme_libelle, 
									PRODUIT1GROUPE.TYPE_THEME as type_theme 
									FROM E0_AGGREGS WHERE PERIODE.IDPERIODE_MOIS BETWEEN  VALUEOF(NQ_SESSION."p_idperiode_mois_debut") 
									AND  VALUEOF(NQ_SESSION."p_idperiode_mois_fin") ORDER BY ordre_affichage
					</cfquery>
					<cfset query = queryrslt0>
				</cfcase>
				
				<cfcase value="1">
<!--- 										            QueryAddRow(myQuery);
					            QuerySetCell(myQuery,"IDPRODUIT", queryrsltToFormat.SAW_0);            
					            QuerySetCell(myQuery,"IDPRODUIT_CLIENT", queryrsltToFormat.SAW_1);
					            QuerySetCell(myQuery,"LIBELLE_PRODUIT", queryrsltToFormat.SAW_2);
					           	QuerySetCell(myQuery,"THEME_LIBELLE", queryrsltToFormat.SAW_3);
					            QuerySetCell(myQuery,"TYPE_THEME", queryrsltToFormat.SAW_4);
					            QuerySetCell(myQuery,"NOM", queryrsltToFormat.SAW_5);
					            QuerySetCell(myQuery,"QTE", queryrsltToFormat.SAW_6);
					            QuerySetCell(myQuery,"MONTANT_FINAL", queryrsltToFormat.SAW_7);
					            QuerySetCell(myQuery,"NOMBRE_APPEL", queryrsltToFormat.SAW_8);
					            QuerySetCell(myQuery,"DUREE_APPEL", queryrsltToFormat.SAW_9); --->
					<!--- THEME --->
					
<!--- 					<cfquery datasource="#dataSourceName#" name="queryrslt1">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idperiode_mois_debut	= #idDebut#,
										p_idperiode_mois_fin	= #idFin#,
										p_idcliche				= #idCliche#,
										p_idtheme				= #arguments.p_idtheme#,
										p_surtheme				= '#arguments.p_surtheme#';
										
					 	 SELECT PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as idproduit, 
									PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as idproduit_client, 
									PRODUIT1GROUPE.LIBELLE_PRODUIT as libelle_produit, 
									PRODUIT1GROUPE.THEME as theme_libelle, 
									PRODUIT1GROUPE.TYPE_THEME as type_theme, 
									PRODUIT1GROUPE.OPERATEUR_PRODUIT as nom, 
									FACTURATION.QTE as qte, 
									FACTURATION.MONTANT as montant_final, 
									FACTURATION.NOMBRE_APPEL as nombre_appel, 
									FACTURATION.VOLUME_MN as duree_appel 
									FROM E0_AGGREGS WHERE (FACTURATION.IDPERIODE_MOIS BETWEEN  
									VALUEOF(NQ_SESSION."p_idperiode_mois_debut") AND  VALUEOF(NQ_SESSION."p_idperiode_mois_fin")) AND (PRODUIT1GROUPE.IDTHEME_PRODUIT =  VALUEOF(NQ_SESSION."p_idtheme")) 
									ORDER BY idproduit, idproduit_client, libelle_produit, theme_libelle, type_theme, nom
					</cfquery> --->
					<cfquery datasource="#dataSourceName#" name="queryrslt1">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idperiode_mois_debut	= #idDebut#,
										p_idperiode_mois_fin	= #idFin#,
										p_idcliche				= #idCliche#,
										p_idtheme				= #arguments.p_idtheme#,
										p_surtheme				= '#arguments.p_surtheme#';
					 	 SELECT PRODUIT1GROUPE.IDPRODUIT_CATALOGUE saw_0, 
									PRODUIT1GROUPE.IDPRODUIT_CATALOGUE saw_1, 
									PRODUIT1GROUPE.LIBELLE_PRODUIT saw_2, 
									PRODUIT1GROUPE.THEME saw_3, 
									PRODUIT1GROUPE.TYPE_THEME saw_4, 
									PRODUIT1GROUPE.OPERATEUR_PRODUIT saw_5, 
									FACTURATION.QTE saw_6, 
									FACTURATION.MONTANT saw_7, 
									FACTURATION.NOMBRE_APPEL saw_8, 
									FACTURATION.VOLUME_MN saw_9 
									FROM E0_AGGREGS WHERE (FACTURATION.IDPERIODE_MOIS BETWEEN  
									VALUEOF(NQ_SESSION."p_idperiode_mois_debut") AND  VALUEOF(NQ_SESSION."p_idperiode_mois_fin")) AND (PRODUIT1GROUPE.IDTHEME_PRODUIT =  VALUEOF(NQ_SESSION."p_idtheme")) 
									ORDER BY saw_0, saw_1, saw_2, saw_3, saw_4, saw_5
					</cfquery>
					<cfset query = queryrslt1>

<!--- 					<cfset query = 	SELECT PRODUIT1GROUPE.IDPRODUIT_CATALOGUE saw_0, 
									PRODUIT1GROUPE.IDPRODUIT_CATALOGUE saw_1, 
									PRODUIT1GROUPE.LIBELLE_PRODUIT saw_2, 
									PRODUIT1GROUPE.THEME saw_3, 
									PRODUIT1GROUPE.TYPE_THEME saw_4, 
									PRODUIT1GROUPE.OPERATEUR_PRODUIT saw_5, 
									FACTURATION.QTE saw_6, 
									FACTURATION.MONTANT saw_7, 
									FACTURATION.NOMBRE_APPEL saw_8, 
									FACTURATION.VOLUME_MN saw_9 
									FROM E0_AGGREGS WHERE (FACTURATION.IDPERIODE_MOIS BETWEEN  
									VALUEOF(NQ_SESSION."p_idperiode_mois_debut") AND  VALUEOF(NQ_SESSION."p_idperiode_mois_fin")) AND (PRODUIT1GROUPE.IDTHEME_PRODUIT =  VALUEOF(NQ_SESSION."p_idtheme")) 
									ORDER BY saw_0, saw_1, saw_2, saw_3, saw_4, saw_5> --->
				</cfcase>
				
				<cfcase value="2">
			<!--- 		          QuerySetCell(myQuery, "THEME_LIBELLE", queryrsltToFormat.SAW_0);            
					            QuerySetCell(myQuery, "TYPE_THEME", queryrsltToFormat.SAW_1);
					            QuerySetCell(myQuery, "IDTHEME_PRODUIT", queryrsltToFormat.SAW_2);
					           	QuerySetCell(myQuery, "QTE", queryrsltToFormat.SAW_3);
					            QuerySetCell(myQuery, "MONTANT_FINAL", queryrsltToFormat.SAW_4);
					            QuerySetCell(myQuery, "NOMBRE_APPEL", queryrsltToFormat.SAW_5);
					            QuerySetCell(myQuery, "DUREE_APPEL", queryrsltToFormat.SAW_6);  --->
					<!--- SURTHEME --->
					<cfquery datasource="#dataSourceName#" name="queryrslt2">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idperiode_mois_debut	= #idDebut#,
										p_idperiode_mois_fin	= #idFin#,
										p_idcliche				= #idCliche#,
										p_idtheme				= #arguments.p_idtheme#,
										p_surtheme				= '#arguments.p_surtheme#';
					 	 SELECT PRODUIT1GROUPE.THEME saw_0, 
									PRODUIT1GROUPE.TYPE_THEME saw_1, 
									PRODUIT1GROUPE.IDTHEME_PRODUIT saw_2, 
									FACTURATION.QTE saw_3, 
									FACTURATION.MONTANT saw_4, 
									FACTURATION.NOMBRE_APPEL saw_5, 
									FACTURATION.VOLUME saw_6 
									FROM E0_AGGREGS WHERE (FACTURATION.IDPERIODE_MOIS IN ( VALUEOF(NQ_SESSION."p_idperiode_mois_debut"),  
									VALUEOF(NQ_SESSION."p_idperiode_mois_fin"))) AND (PRODUIT1GROUPE.SUR_THEME =  VALUEOF(NQ_SESSION."p_surtheme")) 
									ORDER BY saw_0, saw_1, saw_2
					</cfquery>					
					<cfset query = queryrslt2>
<!--- 					<cfset query = 	SELECT PRODUIT1GROUPE.THEME saw_0, 
									PRODUIT1GROUPE.TYPE_THEME saw_1, 
									PRODUIT1GROUPE.IDTHEME_PRODUIT saw_2, 
									FACTURATION.QTE saw_3, 
									FACTURATION.MONTANT saw_4, 
									FACTURATION.NOMBRE_APPEL saw_5, 
									FACTURATION.VOLUME saw_6 
									FROM E0_AGGREGS WHERE (FACTURATION.IDPERIODE_MOIS IN ( VALUEOF(NQ_SESSION."p_idperiode_mois_debut"),  
									VALUEOF(NQ_SESSION."p_idperiode_mois_fin"))) AND (PRODUIT1GROUPE.SUR_THEME =  VALUEOF(NQ_SESSION."p_surtheme")) 
									ORDER BY saw_0, saw_1, saw_2> --->
				</cfcase>
				
				<cfcase value="3">
					<!--- PRODUIT --->
					<cfset query = "">
				</cfcase>	
			</cfswitch>	
		<cfreturn query>
	</cffunction>

	<cffunction name="executQuery" output="true" access="public" returntype="Query">
		<cfargument name="idTabBord" 				required="true"	type="Numeric" default="0">
		<cfargument name="p_idtheme" 				required="true" type="Numeric" default="0">
		<cfargument name="p_surtheme" 				required="true" type="String"  default="0">
		<cfargument name="p_idracine_master" 		required="true" type="Numeric" default="0">
		<cfargument name="p_idracine" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idperimetre" 			required="true" type="Numeric" default="0">
		<cfargument name="dateDebutProvenantDuTDB" 	required="true" type="String"  default="2009/01/01">
		<cfargument name="dateFinProvenantDuTDB" 	required="true" type="String"  default="2009/01/01">
			
			<cfset idtdb 	 = idTabBord>
			<cfset queryrslt = "NULL">
			<cfset query 	 = "NULL">
			<!--- 
			le mot clé var devant après cfset signifie que la variable est locale à la fonction.
			Donc elle disparait lorsque l'on sort de la fonction
			 --->
			<cfset idDebut 	= zgetIdPeriodeMois(dateDebutProvenantDuTDB)>
			<cfset idFin 	= zgetIdPeriodeMois(dateFinProvenantDuTDB)>
			<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ SESSION.PERIMETRE.ID_GROUPE>
		   		<cfset idorga = #SESSION.PERIMETRE.ID_GROUPE#>
		    <cfelse>
		    	<cfset rsltIdOrga = zgetOrga()>
		   		<cfset idorga = NumberFormat(rsltIdOrga,'_____.__')>
		    </cfif>
			<cfset idCliche = zGetCliche(idorga)>
			<cfset niveau 	= zgetNiveauPerimetre()>
			<cfset rsltHandlerquery = getQuery(p_idtheme, p_surtheme, p_idracine_master, p_idracine, p_idperimetre, idDebut, idFin, )>
			
<!--- 			<cfquery datasource="#dataSourceName#" name="queryrslt0">
				set variable 	p_idracine_master		= #arguments.p_idracine_master#,
								p_idracine				= #arguments.p_idracine#,
								p_idorga				= #idorga#,
								p_idperimetre			= #arguments.p_idperimetre#,
								p_idperiode_mois_debut	= #idDebut#,
								p_idperiode_mois_fin	= #idFin#,
								p_idcliche				= #idCliche#,
								p_idtheme				= #arguments.p_idtheme#,
								p_surtheme				= '#arguments.p_surtheme#';
			 	 #query#
			</cfquery> --->


			<!--- <cfset formatedQuery = zFormatQuery(queryrslt0)> --->
			
		<cfreturn rsltHandlerquery>
	</cffunction>

	<cffunction name="logOff" output="true" access="public">
			
			<cfset ws.logoff(sessionId)>
			
	</cffunction>


<!--- 
production@consotel.Fr
auboulot
 --->

<!---
	FUNCTION PRIVATE
--->



	<cffunction name="zFormatQuery" output="true" access="private" returntype="Query">
		<cfargument name="queryrsltToFormat" required="true" type="Query">
			
			<cfswitch expression="#idtdb#">
				<cfcase value="0">
					<!--- ACCEUIL --->
					<cfset myQuery = QueryNew("duree_appel,idtheme_produit,montant_final,nom,nombre_appel,ordre_affichage,qte,segment_theme,sur_theme,theme_libelle,type_theme",
												"Decimal,Integer,Decimal,Varchar,Decimal,Integer,Decimal,Varchar,Varchar,Varchar,Varchar")>	
					<cfloop	query="queryrsltToFormat">
						<cfscript> 
					            QueryAddRow(myQuery);
					            QuerySetCell(myQuery, "duree_appel", queryrsltToFormat.SAW_0);            
					            QuerySetCell(myQuery, "idtheme_produit", queryrsltToFormat.SAW_1);
					            QuerySetCell(myQuery, "montant_final", queryrsltToFormat.SAW_2);
					           	QuerySetCell(myQuery, "nom", queryrsltToFormat.SAW_3);
					            QuerySetCell(myQuery, "nombre_appel", queryrsltToFormat.SAW_4);
					            QuerySetCell(myQuery, "ordre_affichage", queryrsltToFormat.SAW_5);
					            QuerySetCell(myQuery, "qte", queryrsltToFormat.SAW_6);
					            QuerySetCell(myQuery, "segment_theme", queryrsltToFormat.SAW_7);
					            QuerySetCell(myQuery, "sur_theme", queryrsltToFormat.SAW_8);
					            QuerySetCell(myQuery, "theme_libelle", queryrsltToFormat.SAW_9);
					            QuerySetCell(myQuery, "type_theme", queryrsltToFormat.SAW_10);       
					      </cfscript>
					</cfloop>
				</cfcase>
				
				<cfcase value="1">
					<!--- THEME --->
					<cfset myQuery = QueryNew("IDPRODUIT,IDPRODUIT_CLIENT,LIBELLE_PRODUIT,THEME_LIBELLE,TYPE_THEME,NOM,QTE,MONTANT_FINAL,NOMBRE_APPEL,DUREE_APPEL",
												"Integer,Integer,Varchar,Varchar,Varchar,Varchar,Decimal,Decimal,Decimal,Decimal")>	
					<cfloop	query="queryrsltToFormat">
						<cfscript> 
					            QueryAddRow(myQuery);
					            QuerySetCell(myQuery,"IDPRODUIT", queryrsltToFormat.SAW_0);            
					            QuerySetCell(myQuery,"IDPRODUIT_CLIENT", queryrsltToFormat.SAW_1);
					            QuerySetCell(myQuery,"LIBELLE_PRODUIT", queryrsltToFormat.SAW_2);
					           	QuerySetCell(myQuery,"THEME_LIBELLE", queryrsltToFormat.SAW_3);
					            QuerySetCell(myQuery,"TYPE_THEME", queryrsltToFormat.SAW_4);
					            QuerySetCell(myQuery,"NOM", queryrsltToFormat.SAW_5);
					            QuerySetCell(myQuery,"QTE", queryrsltToFormat.SAW_6);
					            QuerySetCell(myQuery,"MONTANT_FINAL", queryrsltToFormat.SAW_7);
					            QuerySetCell(myQuery,"NOMBRE_APPEL", queryrsltToFormat.SAW_8);
					            QuerySetCell(myQuery,"DUREE_APPEL", queryrsltToFormat.SAW_9);
					      </cfscript>
					</cfloop>
				</cfcase>
				
				<cfcase value="2">
					<!--- SURTHEME --->
					<cfset myQuery = QueryNew("THEME_LIBELLE,TYPE_THEME,IDTHEME_PRODUIT,QTE,MONTANT_FINAL,NOMBRE_APPEL,DUREE_APPEL",
												"Varchar,Varchar,Integer,Decimal,Decimal,Decimal,Decimal")>	
					<cfloop	query="queryrsltToFormat">
						<cfscript> 
					            QueryAddRow(myQuery);
					            QuerySetCell(myQuery, "THEME_LIBELLE", queryrsltToFormat.SAW_0);            
					            QuerySetCell(myQuery, "TYPE_THEME", queryrsltToFormat.SAW_1);
					            QuerySetCell(myQuery, "IDTHEME_PRODUIT", queryrsltToFormat.SAW_2);
					           	QuerySetCell(myQuery, "QTE", queryrsltToFormat.SAW_3);
					            QuerySetCell(myQuery, "MONTANT_FINAL", queryrsltToFormat.SAW_4);
					            QuerySetCell(myQuery, "NOMBRE_APPEL", queryrsltToFormat.SAW_5);
					            QuerySetCell(myQuery, "DUREE_APPEL", queryrsltToFormat.SAW_6);    
					      </cfscript>
					</cfloop>
				</cfcase>		
			</cfswitch>
			
		<cfreturn myQuery>
	</cffunction>

	<cffunction name="zcreateComplexObject" output="true" access="private">
					
			<cfset complexObject = structNew()>
			<cfset wcs = CreateObject(	"webservice",
										"http://sun-bi.consotel.fr/analytics/saw.dll?WSDL",
										"WebCatalogServiceSoap")>
			
			<cfswitch expression="#idtdb#">
				<cfcase value="0">
					<!--- ACCEUIL --->
					<cfset path = "/shared/CONSOTEL-OBIEE/CONSOVIEW/TDB/TDB_accueil_idperiode_mois">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
				
				<cfcase value="1">
					<!--- THEME --->
					<cfset path = "/shared/CONSOTEL-OBIEE/CONSOVIEW/TDB/TDB_theme_idperiode_mois">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
				
				<cfcase value="2">
					<!--- SURTHEME --->
					<cfset path = "/shared/CONSOTEL-OBIEE/CONSOVIEW/TDB/TDB_surtheme_idperiode_mois">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
				
				<cfcase value="3">
					<!--- PRODUIT --->
					<cfset path = "/shared/CONSOTEL-OBIEE/CONSOVIEW/TDB/TDB_produit_idperiode_mois">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
				
				<cfcase value="4">
					<!--- RECHERCHE --->
					<cfset path = "">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
			</cfswitch>
				
		<cfreturn complexObject>
	</cffunction>
	
	<cffunction name="zgetIdPeriodeMois" output="true" access="private" returntype="Numeric">
		<cfargument name="thisDate" type="String" required="true" hint="yyyy/mm/dd">
		
		<cfset var tmpDate = parseDateTime(ARGUMENTS.thisDate,"yyyy/mm/dd")>
		<cfset var tmpDateString = MID(lsDateFormat(tmpDate,"mm/yyyy"),1,7)>
		
		<cfquery datasource="#dataSourceName#" name="qGetIdperiodeMois">	
		 	 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS
		 	 FROM	E0_AGGREGS
		 	 WHERE	PERIODE.SHORT_MOIS='#tmpDateString#'
		 	 ORDER BY IDPERIODE_MOIS
		</cfquery>
		<cfif qGetIdperiodeMois.recordcount EQ 1>
			<cfreturn qGetIdperiodeMois["IDPERIODE_MOIS"][1]>
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>
	
	<cffunction name="zgetOrga" output="false" access="private" returntype="Any">
			<cfquery datasource="#SESSION.OFFREDSN#" name="rsltOrga">
				SELECT PKG_CV_RAGO.GET_ORGA(#SESSION.PERIMETRE.ID_PERIMETRE#) as IDORGA FROM DUAL
			</cfquery>
		<cfreturn rsltOrga['IDORGA'][1]>
	</cffunction>
	
 	<cffunction name="zgetNiveauPerimetre" output="false" access="private" returntype="String">		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_consoview_v3.get_numero_niveau">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idgroupe_client" value="#Session.PERIMETRE.ID_GROUPE#">			
				<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR" 	variable="p_retour">
			</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	
	<cffunction name="zGetCliche" output="true" access="private" returntype="Any">
		<cfargument name="idorga" type="numeric" required="true">
			<cfquery datasource="E0" name="clicheRslt">
				SELECT idcliche FROM 
				( SELECT c.idcliche,c.idperiode_mois,MAX(c.idperiode_mois) over () MAX_p FROM cliche c WHERE c.idorganisation=#idorga# ) 
				WHERE max_p=idperiode_mois
			</cfquery>
		<cfreturn clicheRslt['IDCLICHE'][1]>
	</cffunction>
	
</cfcomponent>