<cfcomponent output="false">

<!---
	GLOBAL STATIC
--->

	<cfset dataSourceName = "BI_TEST"><!--- A CHANGER EN FONCTION DU DATASOURCE --->
	
<!---
	GLOBAL
--->

	<cfset ws = "">
	<cfset sessionId = "">
	<cfset query = "">	
	<cfset idtdb = -1>

<!---
	FUNCTION PUBLIC
--->
 
	<cffunction name="init" access="public">
	
	<!--- IL FAUDRA INITIALISER LES VARIABLES ICI!! --->
	
	</cffunction>

	<cffunction name="createWebService" output="true" access="public">
			
			<cfset ws = CreateObject(	"webservice",
										"http://sun-bi.consotel.fr/analytics/saw.dll?WSDL",
										"SAWSessionServiceSoap",
										"--NStoPkg com.siebel.analytics.web/soap/v5=com.siebel.analytics.web.soap.v5")>
		 
		 <cfreturn ws>
	</cffunction>
	
	<cffunction name="logOn" output="true" access="public">
		<cfargument name="idTabBord" type="Numeric" required="true">
			
			<cftry>
					<cfset sessionId = ws.logon("consoview","public")>
					<cfset idtdb = idTabBord>
				<cfcatch>
					<cfset sessionId = 0>
					<cfset idtdb = -1>
				</cfcatch>
			</cftry>
			
		<cfreturn sessionId>
	</cffunction>	
		
	
<!---  Récupération de la liste du detail produit alimentant un datagrid paginé   --->
<cffunction name="getPaginateDetailProduit" access="remote" returntype="query" hint="dernier niveau tableau de bord">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">		
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="idProduit" type="numeric" required="true">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="segment" type="string" required="false">
		<!--- Paramètres du paginate --->
		<cfargument name="SEARCHTEXTE" type="string" required="true">
		<cfargument name="ORDERBYTEXTE" type="string" required="true">
		<cfargument name="OFFSET" type="numeric" required="true">
		<cfargument name="LIMIT" type="numeric" required="true">
		<cfargument name="idTheme" type="numeric" required="true">
		<cfargument name="niveauProduit" type="numeric" required="true">
		<cfargument name="LANG" type="string" required="false" default="#SESSION.USER.GLOBALIZATION#">
				
	    <cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]> 			
		<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
		<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
		<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> --->
		
		<!--- récupération des paramètres de session --->
		<!--- racine master et racine --->
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset idracine_master = #SESSION.PERIMETRE.IDRACINE_MASTER#>
		<!--- orga --->
		
		<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ SESSION.PERIMETRE.ID_GROUPE>
		   		<cfset idorga = #SESSION.PERIMETRE.ID_GROUPE#>
		    <cfelse>
		    	<cfset rsltIdOrga = zgetOrga()>
		   		<cfset idorga = Val(rsltIdOrga)>
		</cfif>
		<cfset SESSION.PERIMETRE.SELECTED_ID_ORGA = idorga>
		
		<!--- cliché --->  
			<cfset idCliche = zGetCliche(idorga)>
			<cfset SESSION.PERIMETRE.SELECTED_ID_CLICHE = idCliche>			
			<cfif idCliche EQ "">
				<cfabort showerror="Aucun cliché disponible, merci de contacter le service client.">
			</cfif>
		
		<!--- niveau et perimetre --->
			<cfif compareNoCase(perimetre,'soustete') eq 0>
				<cfset niveau 	= 'IDSOUS_TETE'>
				<cfset idperimetre = numero>
			<cfelse>
				<cfset niveau 	= zgetNiveauPerimetre(SESSION.PERIMETRE.ID_PERIMETRE)>
				<cfset idperimetre = SESSION.PERIMETRE.ID_PERIMETRE>
			</cfif>
			<cfset SESSION.PERIMETRE.SELECTED_NIVEAU = niveau>
		
		<!--- intervalle de date --->
			<cfset idDebut 	= zgetIdPeriodeMois(transformDate(datedeb))>	
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB = idDebut>
					
			<cfset idFin 	= zgetIdPeriodeMois(transformDate(datefin))>
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN = idFin>
		
		
	 	<!---
			<cfabort showerror="idracine_master =#idracine_master# ,idRacine=#idRacine#,idPerimètre = #SESSION.PERIMETRE.ID_PERIMETRE#,niveau = #niveau#,idOrga = #idorga#, idproduit = #idproduit#,dateDebut=#idDebut#,dateDebut=#idFin#,ORDER_BY_TEXTE_COLONNE = #ORDER_BY_TEXTE_COLONNE#,#ORDER_BY_TEXTE#,#SEARCH_TEXT_COLONNE#,#SEARCH_TEXT#,#OFFSET#,#LIMIT#,#LANG#" />			
		--->
		
		<cfif niveauProduit EQ 1>
			<cfset niveauProc = idTheme>
	   		<cfset niveauLigne = "Theme">
	   		<cfset SESSION.PERIMETRE.SELECTED_IDTHEME = idTheme>
		<cfelse>
			<cfset niveauProc = idProduit>
	    	<cfset niveauLigne = "Produit">
	    	<cfset SESSION.PERIMETRE.SELECTED_IDPRODUIT = idProduit>
		</cfif> 
		
		
		<cfscript>			
			qstring = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=db-1-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-2-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-3-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-4-vip) (PORT=1522)) (FAILOVER=ON) (CONNECT_DATA=(SERVICE_NAME=entrepot.consotel.fr)))";
            q = "begin :result := E0.pkg_M311.sfgetPaginateDetail#niveauLigne#(:a,:b,:c,:d,:e,:f,:g,:h,:i,:j,:k,:l,:m,:n,:o); end;";
            Class = createObject("java","java.lang.Class");
            DriverManager = CreateObject("java", "java.sql.DriverManager");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = CreateObject("java", "java.sql.Connection");
            con = DriverManager.getConnection(qstring,"offre", "consnia");
            stmt = createObject("java", "java.sql.Statement");
            stmt = con.prepareCall(q);
            OracleTypes = CreateObject("java", "oracle.jdbc.driver.OracleTypes");
            stmt.registerOutParameter("result", OracleTypes.CURSOR);
            stmt.setInt ("a", javacast("int",idracine_master));
            stmt.setInt ("b", javacast("int",idRacine));
			stmt.setInt ("c", javacast("int",idperimetre));
			stmt.setString ("d", javacast("string",niveau));
			stmt.setInt ("e", javacast("int",idorga));
			stmt.setInt ("f", javacast("int",niveauProc));
			stmt.setInt ("g", javacast("int",idDebut));
			stmt.setInt ("h", javacast("int",idFin));
			stmt.setString ("i", javacast("string",ORDER_BY_TEXTE_COLONNE));
			stmt.setString ("j", javacast("string",ORDER_BY_TEXTE));
			stmt.setString ("k", javacast("string",SEARCH_TEXT_COLONNE));
			stmt.setString ("l", javacast("string",SEARCH_TEXT));
			stmt.setInt ("m", javacast("int",OFFSET));
			stmt.setInt ("n", javacast("int",LIMIT));
			stmt.setString ("o", javacast("string",LANG));
            stmt.execute();  
            rs = stmt.getObject("result");
            p_result = CreateObject("java", "coldfusion.sql.QueryTable").init(rs);
		</cfscript>
	

		
	<!--- 	<cfstoredproc datasource="E0" procedure="E0.pkg_M311.getPaginateDetail#niveauLigne#">
		
		
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine_master#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idperimetre#" null="false">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#niveau#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idorga#" null="false">
	        <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#niveauProc#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDebut#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idFin#" null="false">
		
			<!--- Paramètres du paginate --->
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">														
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">				
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#LANG#" null="false">							
			<cfprocresult name="p_result">
									
		</cfstoredproc> --->
	
		<cfquery name="q2" dbtype="query">
			select 
			ROW_TOTAL as NBRECORD,
			C8 as SITE,
			C5 as SOUS_TETE,
			C6 as COMMENTAIRES,
			C17 as NBAPPELS ,			
			C15 as  QTE,
			C18 as  VOLUME,
			<!---C5 as LIGNE,--->
			C16  as  MONTANT  ,		
			C16_TOTAL as  TOTAL_MONTANT ,
			C18_TOTAL as  TOTAL_VOLUME,
			C15_TOTAL as  TOTAL_QUANTITE,
			C17_TOTAL as  TOTAL_NBAPPELS ,
			C14 as  NUMERO_FACTURE  ,
			C2 as  IDPRODUIT_CATALOGUE , 			
			C4 as  IDSOUS_TETE  , 
			C13 as  IDINVENTAIRE_PERIODE  						 
			from p_result			 
		</cfquery>
		
		<!--- Mise en session du nombre d'enregistrement total pour un export --->
		
			
		<cfif structKeyExists(SESSION,"PARAMS_EXPORT")>
			<cfset structdelete(SESSION,"PARAMS_EXPORT")>
		</cfif>
		
		<cfset PARAMS_EXPORT = structnew()>
		
		<cfset PARAMS_EXPORT.IDRACINE_MASTER=idracine_master>
		<cfset PARAMS_EXPORT.IDRACINE=idRacine>
		<cfset PARAMS_EXPORT.IDPERIMETRE=idperimetre>
		<cfset PARAMS_EXPORT.NIVEAU=niveau>
		<cfset PARAMS_EXPORT.IDORGA=idorga>
		
		<cfif niveauProduit EQ 1>			
	   		<cfset PARAMS_EXPORT.IDTHEME = idTheme>
		<cfelse>
		
	    	<cfset PARAMS_EXPORT.IDPRODUIT = idProduit>
		</cfif>
			
		<cfset PARAMS_EXPORT.IDDEBUT=idDebut>
		<cfset PARAMS_EXPORT.IDFIN=idFin>
		
		<cfset PARAMS_EXPORT.ORDER_BY_TEXTE_COLONNE = ORDER_BY_TEXTE_COLONNE>
		<cfset PARAMS_EXPORT.ORDER_BY_TEXTE = ORDER_BY_TEXTE>
		<cfset PARAMS_EXPORT.SEARCH_TEXT_COLONNE = SEARCH_TEXT_COLONNE>
		<cfset PARAMS_EXPORT.SEARCH_TEXT = SEARCH_TEXT>
		<cfset PARAMS_EXPORT.OFFSET = OFFSET>			

		
		<cfif q2.RecordCount gt 0>	
			<cfset PARAMS_EXPORT.LIMIT = q2["NBRECORD"][1]>
			<cfelse>
			<cfset PARAMS_EXPORT.LIMIT = 0>
		</cfif>
		
		<!--- <cfset PARAMS_EXPORT.LIMIT = LIMIT> --->
		<cfset PARAMS_EXPORT.LANG = LANG>
		<cfset SESSION.PARAMS_EXPORT = PARAMS_EXPORT>
			
	<cfreturn q2 >
</cffunction>	
	
	<cffunction name="getQueryByWebService" output="true" access="public">
			
			<cfset res = CreateObject(	"webservice",
										"http://sun-bi.consotel.fr/analytics/saw.dll?WSDL",
										"ReportEditingServiceSoap")>
			
			<cfset reportRef = zcreateComplexObject()>
			<cfset reportParams = structNew()>
 			<cfset query = res.generateReportSQL(reportRef, reportParams, sessionId)>
	
		<cfreturn query>
	</cffunction>
	
	<cffunction name="getQuery" output="true" access="public" returntype="Query">
		<cfargument name="p_idproduit_catalogue" 	required="true" type="Numeric" default="0">
		<cfargument name="p_idtheme" 				required="true" type="Numeric" default="0">
		<cfargument name="p_surtheme" 				required="true" type="String"  default="0">
		<cfargument name="p_idracine_master" 		required="true" type="Numeric" default="0">
		<cfargument name="p_idracine" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idperimetre" 			required="true" type="Numeric" default="0">
		<cfargument name="idDebut" 					required="true" type="Numeric" default="0">
		<cfargument name="idFin" 					required="true" type="Numeric" default="0">
		<cfargument name="idCliche" 				required="true" type="Numeric" default="0">
		<cfargument name="idorga" 					required="true" type="Numeric" default="0">
		
			<cfset dataTable = "E0_AGGREGS">
			<cfswitch expression="#niveau#">
				<cfcase value="A,B,C,D,E,F" delimiters=",">
					<cfset dataTable = "E0_AGGREGS_DEFRAG">
				</cfcase>
				<cfdefaultcase>
					<cfset dataTable = "E0_ENPROD_HIST">	
				</cfdefaultcase>
			</cfswitch>
						
			<cfswitch expression="#idtdb#">
				<cfcase value="0">
					<!--- ACCEUIL --->
					
					<cfquery datasource="#dataSourceName#" name="queryrslt0">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idcliche				= #idCliche#,
										p_niveau				= '#niveau#',
										p_idniveau				= #idniveau#,
										p_langue_pays='#SESSION.USER.GLOBALIZATION#';
										
					 	 SELECT 
							 	PRODUIT1GROUPE.IDSEGMENT_THEME as idsegment_theme,
							 	PRODUIT1GROUPE.IDTYPE_THEME as idtype_theme,
					 	 		ROUND(FACTURATION.VOLUME_MN,0) as duree_appel, 
					 	 		UNITE_VOLUME.UNITE_SOMMEE as unite,
								PRODUIT1GROUPE.IDTHEME_PRODUIT as idtheme_produit,
								DEVISE.TAUX_CONV as TAUX_CONV,
								FACTURATION.MONTANT_LOC as montant_final,
								PRODUIT1GROUPE.OPERATEUR_PRODUIT as nom, 
								FACTURATION.NOMBRE_APPEL as nombre_appel, 
								PRODUIT1GROUPE.ORDRE_AFFICHAGE as ordre_affichage, 
								FACTURATION.QTE as qte, 
								PRODUIT1GROUPE.SEGMENT_THEME as segment_theme, 
								PRODUIT1GROUPE.SUR_THEME as sur_theme, 
								PRODUIT1GROUPE.THEME as theme_libelle, 
								PRODUIT1GROUPE.TYPE_THEME as type_theme,
								PRODUIT1GROUPE.IDSUR_THEME as idsur_theme
							FROM #dataTable#
							WHERE PERIODE.IDPERIODE_MOIS BETWEEN  #idDebut#
							AND  #idFin# ORDER BY ordre_affichage
					</cfquery>
					<cfset query = queryrslt0>
				</cfcase>
				
				<cfcase value="1">
					<!--- THEME --->
					<cfquery datasource="#dataSourceName#" name="queryrslt1">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_niveau				= '#niveau#',
										p_idniveau				= #idniveau#,
										p_langue_pays='#SESSION.USER.GLOBALIZATION#';
										
					 	 SELECT PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT, 
									PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT_CLIENT, 
									PRODUIT1GROUPE.LIBELLE_PRODUIT as LIBELLE_PRODUIT, 
									PRODUIT1GROUPE.IDSEGMENT_THEME as idsegment_theme,
								 	PRODUIT1GROUPE.IDTYPE_THEME as idtype_theme,
									PRODUIT1GROUPE.THEME as THEME_LIBELLE, 
									PRODUIT1GROUPE.TYPE_THEME as TYPE_THEME, 
									PRODUIT1GROUPE.OPERATEUR_PRODUIT as NOM, 
									FACTURATION.QTE as QTE, 
									DEVISE.TAUX_CONV as TAUX_CONV,
									FACTURATION.MONTANT_LOC as MONTANT_FINAL, 
									FACTURATION.NOMBRE_APPEL as NOMBRE_APPEL, 
									ROUND(FACTURATION.VOLUME_MN,0) as DUREE_APPEL,
									UNITE_VOLUME.UNITE_SOMMEE as unite
									
							FROM #dataTable#
							WHERE PERIODE.IDPERIODE_MOIS BETWEEN #idDebut# AND  #idFin#
							 AND PRODUIT1GROUPE.IDTHEME_PRODUIT =  #p_idtheme#
							ORDER BY MONTANT_FINAL DESC	
					</cfquery>
					<cfset query = queryrslt1>
				</cfcase>		
			</cfswitch>	
		<cfreturn query>
	</cffunction>

	<cffunction name="executQuery" output="true" access="public" returntype="Query">
		<cfargument name="idTabBord" 				required="true"	type="Numeric" default="0">
		<cfargument name="p_idproduit_catalogue" 	required="true" type="Numeric" default="0">
		<cfargument name="p_idtheme" 				required="true" type="Numeric" default="0">
		<cfargument name="p_surtheme" 				required="true" type="String"  default="0">
		<cfargument name="p_idracine_master" 		required="true" type="Numeric" default="0">
		<cfargument name="p_idracine" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idperimetre" 			required="true" type="Numeric" default="0">
		<cfargument name="dateDebutProvenantDuTDB" 	required="true" type="String"  default="2009/01/01">
		<cfargument name="dateFinProvenantDuTDB" 	required="true" type="String"  default="2009/01/01">
		<cfargument name="perimetreParm" 			required="true" type="String"  default="0">		
			
			<cfset idtdb 	 = idTabBord>
			<cfset queryrslt = "NULL">
			<cfset query 	 = "NULL">
			<!--- 
			le mot clé var devant après cfset signifie que la variable est locale à la fonction.
			Donc elle disparait lorsque l'on sort de la fonction
			 --->
			<cfset idDebut 	= zgetIdPeriodeMois(dateDebutProvenantDuTDB)>	
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB = idDebut>
					
			<cfset idFin 	= zgetIdPeriodeMois(dateFinProvenantDuTDB)>
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN = idFin>
						
			<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ SESSION.PERIMETRE.ID_GROUPE>
		   		<cfset idorga = #SESSION.PERIMETRE.ID_GROUPE#>
		    <cfelse>
		    	<cfset rsltIdOrga = zgetOrga()>
		   		<cfset idorga = Val(rsltIdOrga)>
		    </cfif>
		    <cfset SESSION.PERIMETRE.SELECTED_ID_ORGA = idorga>
		    
			<cfset idCliche = zGetCliche(idorga)>
			<cfset SESSION.PERIMETRE.SELECTED_ID_CLICHE = idCliche>
			
			<cfif idCliche EQ "">
				<cfabort showerror="Aucun cliché">
			</cfif>
			
			<cfif perimetreParm EQ "SousTete">
				<cfset niveau 	= 'IDSOUS_TETE'>
			<cfelse>
				<cfset niveau 	= zgetNiveauPerimetre(p_idperimetre)>
			</cfif>
			
			<cfset idniveau 	= getIdNiveau(niveau)>
			<cfset SESSION.PERIMETRE.SELECTED_IDNIVEAU = idniveau>
			
			<cfset SESSION.PERIMETRE.SELECTED_NIVEAU = niveau>
			
			<cfset rsltHandlerquery = getQuery(p_idproduit_catalogue, p_idtheme, p_surtheme, p_idracine_master, p_idracine, p_idperimetre, idDebut, idFin, idCliche, idorga)>

		<cfreturn rsltHandlerquery>
	</cffunction>

	<cffunction name="logOff" output="true" access="public">
			
			<cfset ws.logoff(sessionId)>
			
	</cffunction>

	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "/" & mid(oldDateString,4,2) & "/" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>

<!--- 
production@consotel.Fr
auboulot
 --->

<!---
	FUNCTION PRIVATE
--->

	<cffunction name="zFormatQuery" output="true" access="private" returntype="Query">
		<cfargument name="queryrsltToFormat" required="true" type="Query">
			
			<cfswitch expression="#idtdb#">
				<cfcase value="0">
					<!--- ACCEUIL --->
					<cfset myQuery = QueryNew("duree_appel,idtheme_produit,montant_final,nom,nombre_appel,ordre_affichage,qte,segment_theme,sur_theme,theme_libelle,type_theme",
												"Decimal,Integer,Decimal,Varchar,Decimal,Integer,Decimal,Varchar,Varchar,Varchar,Varchar")>	
					<cfloop	query="queryrsltToFormat">
						<cfscript> 
					            QueryAddRow(myQuery);
					            QuerySetCell(myQuery, "duree_appel", queryrsltToFormat.SAW_0);            
					            QuerySetCell(myQuery, "idtheme_produit", queryrsltToFormat.SAW_1);
					            QuerySetCell(myQuery, "montant_final", queryrsltToFormat.SAW_2);
					           	QuerySetCell(myQuery, "nom", queryrsltToFormat.SAW_3);
					            QuerySetCell(myQuery, "nombre_appel", queryrsltToFormat.SAW_4);
					            QuerySetCell(myQuery, "ordre_affichage", queryrsltToFormat.SAW_5);
					            QuerySetCell(myQuery, "qte", queryrsltToFormat.SAW_6);
					            QuerySetCell(myQuery, "segment_theme", queryrsltToFormat.SAW_7);
					            QuerySetCell(myQuery, "sur_theme", queryrsltToFormat.SAW_8);
					            QuerySetCell(myQuery, "theme_libelle", queryrsltToFormat.SAW_9);
					            QuerySetCell(myQuery, "type_theme", queryrsltToFormat.SAW_10);       
					      </cfscript>
					</cfloop>
				</cfcase>
				
				<cfcase value="1">
					<!--- THEME --->
					<cfset myQuery = QueryNew("IDPRODUIT,IDPRODUIT_CLIENT,LIBELLE_PRODUIT,THEME_LIBELLE,TYPE_THEME,NOM,QTE,MONTANT_FINAL,NOMBRE_APPEL,DUREE_APPEL",
												"Integer,Integer,Varchar,Varchar,Varchar,Varchar,Decimal,Decimal,Decimal,Decimal")>	
					<cfloop	query="queryrsltToFormat">
						<cfscript> 
					            QueryAddRow(myQuery);
					            QuerySetCell(myQuery,"IDPRODUIT", queryrsltToFormat.SAW_0);            
					            QuerySetCell(myQuery,"IDPRODUIT_CLIENT", queryrsltToFormat.SAW_1);
					            QuerySetCell(myQuery,"LIBELLE_PRODUIT", queryrsltToFormat.SAW_2);
					           	QuerySetCell(myQuery,"THEME_LIBELLE", queryrsltToFormat.SAW_3);
					            QuerySetCell(myQuery,"TYPE_THEME", queryrsltToFormat.SAW_4);
					            QuerySetCell(myQuery,"NOM", queryrsltToFormat.SAW_5);
					            QuerySetCell(myQuery,"QTE", queryrsltToFormat.SAW_6);
					            QuerySetCell(myQuery,"MONTANT_FINAL", queryrsltToFormat.SAW_7);
					            QuerySetCell(myQuery,"NOMBRE_APPEL", queryrsltToFormat.SAW_8);
					            QuerySetCell(myQuery,"DUREE_APPEL", queryrsltToFormat.SAW_9);
					      </cfscript>
					</cfloop>
				</cfcase>
				
				<cfcase value="2">
					<!--- SURTHEME --->
					<cfset myQuery = QueryNew("THEME_LIBELLE,TYPE_THEME,IDTHEME_PRODUIT,QTE,MONTANT_FINAL,NOMBRE_APPEL,DUREE_APPEL",
												"Varchar,Varchar,Integer,Decimal,Decimal,Decimal,Decimal")>	
					<cfloop	query="queryrsltToFormat">
						<cfscript> 
					            QueryAddRow(myQuery);
					            QuerySetCell(myQuery, "THEME_LIBELLE", queryrsltToFormat.SAW_0);            
					            QuerySetCell(myQuery, "TYPE_THEME", queryrsltToFormat.SAW_1);
					            QuerySetCell(myQuery, "IDTHEME_PRODUIT", queryrsltToFormat.SAW_2);
					           	QuerySetCell(myQuery, "QTE", queryrsltToFormat.SAW_3);
					            QuerySetCell(myQuery, "MONTANT_FINAL", queryrsltToFormat.SAW_4);
					            QuerySetCell(myQuery, "NOMBRE_APPEL", queryrsltToFormat.SAW_5);
					            QuerySetCell(myQuery, "DUREE_APPEL", queryrsltToFormat.SAW_6);    
					      </cfscript>
					</cfloop>
				</cfcase>		
			</cfswitch>
			
		<cfreturn myQuery>
	</cffunction>

	<cffunction name="zcreateComplexObject" output="true" access="private">
					
			<cfset complexObject = structNew()>
			<cfset wcs = CreateObject(	"webservice",
										"http://sun-bi.consotel.fr/analytics/saw.dll?WSDL",
										"WebCatalogServiceSoap")>
			
			<cfswitch expression="#idtdb#">
				<cfcase value="0">
					<!--- ACCEUIL --->
					<cfset path = "/shared/CONSOTEL-OBIEE/CONSOVIEW/TDB/TDB_accueil_idperiode_mois">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
				
				<cfcase value="1">
					<!--- THEME --->
					<cfset path = "/shared/CONSOTEL-OBIEE/CONSOVIEW/TDB/TDB_theme_idperiode_mois">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
				
				<cfcase value="2">
					<!--- SURTHEME --->
					<cfset path = "/shared/CONSOTEL-OBIEE/CONSOVIEW/TDB/TDB_surtheme_idperiode_mois">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
				
				<cfcase value="3">
					<!--- PRODUIT --->
					<cfset path = "/shared/CONSOTEL-OBIEE/CONSOVIEW/TDB/TDB_produit_idperiode_mois">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
				
				<cfcase value="4">
					<!--- RECHERCHE --->
					<cfset path = "">
					<cfset complexObject.reportPath = path>
					<cfset reportXML  = wcs.readObject(path, TRUE, sessionId)>
					<cfset complexObject.reportXML  = reportXML.catalogObject>
				</cfcase>
			</cfswitch>
				
		<cfreturn complexObject>
	</cffunction>
	
	<cffunction name="zgetIdPeriodeMois" output="true" access="private" returntype="Numeric">
		<cfargument name="thisDate" type="String" required="true" hint="yyyy/mm/dd">
		
		<cfset var tmpDate = parseDateTime(ARGUMENTS.thisDate,"yyyy/mm/dd")>
		<cfset var tmpDateString = MID(lsDateFormat(tmpDate,"mm/yyyy"),1,7)>
				
		<cfquery datasource="E0" name="qGetIdperiodeMois">	
		 	SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS
            		FROM   PERIODE
		   	WHERE   PERIODE.SHORT_MOIS='#tmpDateString#' AND LANGUE_PAYS='#SESSION.USER.GLOBALIZATION#'
		</cfquery>
		
		<cfif qGetIdperiodeMois.recordcount EQ 1>
			<cfreturn qGetIdperiodeMois["IDPERIODE_MOIS"][1]>
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>
	
	<cffunction name="zgetOrga" output="false" access="private" returntype="Any">
			<cfquery datasource="#SESSION.OFFREDSN#" name="rsltOrga">
				SELECT PKG_CV_RAGO.GET_ORGA(#SESSION.PERIMETRE.ID_PERIMETRE#) as IDORGA FROM DUAL
			</cfquery>
		<cfreturn rsltOrga['IDORGA'][1]>
	</cffunction>
	
	 <cffunction name="zgetNiveauPerimetre" output="false" access="public" returntype="String">
		 	<cfargument name="perimetre" type="numeric" required="true">
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_consoview_v3.get_numero_niveau">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idgroupe_client" value="#perimetre#">			
				<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR" 	variable="p_retour">
			</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="getIdNiveau" output="false" access="public" returntype="numeric">		
		<cfargument name="niveau" type="string" required="true">			 				
		<cfstoredproc procedure="pkg_consoview_v3.niveau_to_integer" datasource="#SESSION.OFFREDSN#">	
			<cfprocparam type="in" value="#niveau#" variable="p_niveau" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="out"  variable="p_idniveau" cfsqltype="CF_SQL_iNTEGER">
		</cfstoredproc>
		<cfreturn p_idniveau>
	</cffunction>

	<cffunction name="zGetCliche" output="true" access="public" returntype="Any">
		<cfargument name="idorga" type="numeric" required="true">
			<cfquery datasource="E0" name="clicheRslt">
				SELECT idcliche FROM 
				( SELECT c.idcliche,c.idperiode_mois,MAX(c.idperiode_mois) over () MAX_p FROM cliche c WHERE c.idorganisation=#idorga# ) 
				WHERE max_p=idperiode_mois
			</cfquery>
		<cfreturn clicheRslt['IDCLICHE'][1]>
	</cffunction>
	
</cfcomponent>
