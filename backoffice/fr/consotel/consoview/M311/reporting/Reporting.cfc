<cfcomponent author="Cedric" displayname="fr.consotel.consoview.M311.reporting.Reporting" hint="Reporting M311 (Scopes requis : Application, Session)">
	<!--- Initialisation explicite --->
	<cfset explicitInit()>

	<cffunction access="remote" name="scheduleContainer" returntype="void" hint="Planification d'un rapport dans le container">
		<cfargument name="report" type="Struct" required="true" hint="Structure qui représente le reporting à effectuer.
		Si elle contient une clé RAPPORT alors l'API de la bibliothèque de rapport est utilisée et sinon c'est l'API standard">
		<cfargument name="paramStrategy" type="String" required="false" default="" hint="Nom d'un composant qui implémente fr.consotel.consoview.M311.reporting.IParamStrategy">
		<cfset var reportProp=ARGUMENTS["report"]>
		<cfset var strategy=ARGUMENTS["paramStrategy"]>
		<cfif LEN(TRIM(strategy)) GT 0>
			<cfset reportProp=getParamStrategy(strategy).setParameters(reportProp)>
		</cfif>
		<cfset var reportingApi=getReportingKey()><!--- API Reporting : XDO --->
		<cfif structKeyExists(reportProp,"RAPPORT")>
			<cfset reportingApi=getReportLibraryKey()><!--- API Reporting : Bibliothèque --->
		</cfif>
		<cfset var reportingService=getReportingService(reportingApi)>
		<cfset var reporting=reportingService.createReporting(reportProp)>
		<cfset reportingService.execute(reporting)>
	</cffunction>
	
	<cffunction access="private" name="explicitInit" returntype="void" hint="Initialiseur explicite">
		<cfif isDefined("APPLICATION") AND isDefined("SESSION")>
			<cflog type="information" text="#getLogEntry()# Explicit initialization done">
		<cfelse>
			<cfthrow type="Custom" message="#getLogEntry()# Les scopes Application et/ou Session ne sont pas définis">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getLogEntry" returntype="String" hint="Chaine utilisé en préfixe dans les exceptions et les logs">
		<cfreturn "[M311:Reporting]">
	</cffunction>
	
	<cffunction access="private" name="getReportingKey" returntype="String" hint="Retourne la clé du scope application pour l'API de reporting">
		<cfreturn "fr.consotel.consoview.REPORTING_SERVICE">
	</cffunction>

	<cffunction access="private" name="getReportLibraryKey" returntype="String" hint="Retourne la clé du scope application pour l'API de bibliothèque de rapports">
		<cfreturn "fr.consotel.consoview.REPORTING_LIB_SERVICE">
	</cffunction>

	<cffunction access="private" name="getReportingApi" returntype="String" hint="Retourne le nom de la classe de l'API de Reporting correspondant à la clé api">
		<cfargument name="api" type="String" required="true" hint="Valeur indiquant l'API de reporting à utiliser (e.g : Bibliothèque de rapport) : getReportingKey() ou getReportLibraryKey()">
		<cfset var apiKey=ARGUMENTS["api"]>
		<cfset var reportingListKey="REPORTING_API_LIST">
		<cfif NOT structKeyExists(VARIABLES,reportingListKey)><!--- Liste des API de reporting (IReportingService) --->
			<cfset VARIALES[reportingListKey][getReportingKey()]="fr.consotel.api.ibis.publisher.reporting.impl.ReportingService">
			<cfset VARIALES[reportingListKey][getReportLibraryKey()]="fr.consotel.api.ibis.publisher.reporting.service.Rapports">
		</cfif>
		<cfif structKeyExists(VARIALES[reportingListKey],apiKey)>
			<cfreturn VARIALES[reportingListKey][apiKey]>
		<cfelse>
			<cfthrow type="Custom" message="#getLogEntry()# L'API de reporting identifiée par la clé #apiKey# n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getReportingService" returntype="fr.consotel.api.ibis.publisher.reporting.IReportingService" hint="Retourne une instance de l'API de Reporting">
		<cfargument name="api" type="String" required="true" hint="Clé indiquant l'API de reporting à utiliser (e.g : Bibliothèque de rapport)">
		<cfset var reportingApi=getReportingApi(ARGUMENTS["api"])>
		<!--- Défini ou remplace l'instance de l'API de reporting dans le scope Application --->
		<cfif NOT structKeyExists(APPLICATION,getReportingKey())>
			<cflock type="exclusive" scope="Application" timeout="30" throwontimeout="true">
				<cfif (NOT structKeyExists(APPLICATION,getReportingKey())) OR (NOT isInstanceOf(APPLICATION[getReportingKey()],reportingApi))>
					<cfset APPLICATION[getReportingKey()]=createObject("component",reportingApi).getService("consoview","public")>
				</cfif>
			</cflock>
		</cfif>
		<cfset APPLICATION[getReportingKey()]=createObject("component",reportingApi).getService("consoview","public")><!--- DEBUG : Update API upon every client request --->
		<!--- Instance de l'API de reporting provenant du scope Application --->
		<cflock type="readonly"  scope="Application" timeout="30" throwontimeout="true">
			<cfreturn APPLICATION[getReportingKey()]>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="getParamStrategy" returntype="fr.consotel.consoview.M311.reporting.IParamStrategy" hint="Retourne la stratégie correspondante pour mettre à jour les paramètres">
		<cfargument name="paramStrategy" type="String" required="false" default="" hint="Nom complet d'une implémentation fr.consotel.consoview.M311.reporting.IParamStrategy">
		<cfset var strategyKey=ARGUMENTS["paramStrategy"]>
		<cfset var paramStrategyListKey="PARAM_STRATEGIES">
		<cfif NOT structKeyExists(VARIABLES,paramStrategyListKey)>
			<cfset VARIABLES[paramStrategyListKey]={}>
		</cfif>
		<cfset var paramStrategyList=VARIABLES[paramStrategyListKey]>
		<cfif NOT structKeyExists(VARIABLES,strategyKey)>
			<cfset VARIABLES[strategyKey]=createObject("component",strategyKey)>
		</cfif>
		<cfreturn VARIABLES[strategyKey]>
	</cffunction>
</cfcomponent>