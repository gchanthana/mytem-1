<cfinterface author="Cedric" displayname="fr.consotel.consoview.M311.reporting.IParamStrategy" hint="Stratégie utilisée pour définir et mettre à jour des paramètres du rapport BIP">
	<cffunction name="setParameters" returntype="Struct" hint="Définit et met à jour les valeurs des paramètres dans report. Retourne report">
		<cfargument name="report" type="Struct" required="true" hint="Structure qui représente le reporting à effectuer.
		Si elle contient une clé RAPPORT alors l'API de la bibliothèque de rapport est utilisée et sinon c'est l'API standard">
	</cffunction>
</cfinterface>