<!--- =========================================================================
Classe: GroupeLigneCompletStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GroupeLigneCompletStrategy" hint=""  extends="Strategy" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GroupeLigneCompletStrategy" hint="Remplace le constructeur de GroupeLigneCompletStrategy.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	 
	</cffunction>
<!--- METHODS --->
	<cffunction name="getData" access="public" returntype="query" output="false" hint="Raméne les données pour le tableau de bord niveau surthémes en fonction du surthéme passé en paramétre." >
		<cfargument name="surtheme" required="false" type="string" default="" displayname="string surtheme" hint=%qt%%paramNotes%%qt% />
		<cfargument name="IDCompte" required="false" type="numeric" default="" displayname="numeric IDCompte" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="numeric DateDebut" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="numeric DateFin" hint=%qt%%paramNotes%%qt% />
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG_V3.TB_SUR_THEME">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#IDCompte#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#DateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_datefin" value="#DateFin#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_sur_theme" value="#surtheme#"/>			
	        <cfprocresult name="p_result"/>        
		</cfstoredproc>
		<cfreturn p_result/> 
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" hint="Renvoi l'appelation du périmétre (Ex: Société = Compte Hiérarchique)." >
	</cffunction>
	<cffunction name="getNumero" access="public" returntype="string" output="false" hint="Retourne le nom du périmétre (son label)." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	<cffunction name="getLibelle" access="public" returntype="string" output="false" hint="Retroune le libéllé d'un théme passé en paramétre." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>