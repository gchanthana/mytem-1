r r <!--- =========================================================================
Name: Theme
Original Author: 
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="Recherche" hint="This class is configured with a ConcreteStrategy object, maintains a reference to a Strategy object and may define an interface that lets Strategy access its data.">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<!--- <cffunction name="init" access="public" output="false" returntype="Theme" displayname="Theme init()" hint="Initialize the Theme object">
		<cfargument name="Libelle" required="false" type="string" default="" displayname="string Libelle" hint="Initial value for the Libelle property." />
		<cfargument name="dataset" required="false" type="query" default="" displayname="query dataset" hint="Initial value for the dataset property." />
		<cfargument name="Compte" required="false" type="string" default="" displayname="string Compte" hint="Initial value for the Compte property." />
		<cfscript>
			variables.instance = structNew();
			setLibelle(arguments.Libelle);
			setDataset(arguments.dataset);
			setCompte(arguments.Compte);
			return this;
		</cfscript>	
	</cffunction> --->
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="init" access="public" returntype="void" output="false" displayname="void init(string, string)" >
		<cfargument name="perimetre" required="false" type="string" default="" displayname="string perimetre" hint="Initial value for the perimetreproperty." />		
		<cfset objStrategy = createObject("component","#perimetre#RechercheRapideStrategy")>
		<cfset Variables.Strategy=objStrategy>
	</cffunction>
	
	<cffunction name="setStrategy" access="public" returntype="void" output="false" displayname="void setStrategy(string)" >
		<cfargument name="perimetre" required="false" type="string" default="" displayname="string perimetre" hint="Initial value for the perimetreproperty." />
	</cffunction>
		
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfreturn Strategy.getStrategy()>
	</cffunction>
		
	<cffunction name="queryData" access="public" returntype="query" output="false" displayname="query queryData(numeric, string,string, string)" >		
		<cfargument name="idGroupe" 	required="true"  type="numeric" default="" displayname="numeric ID" 	hint="Initial value for the IDproperty."/>
		<cfargument name="idCliche" 	required="true"  type="numeric" default="" displayname="numeric ID" 	hint="Initial value for the IDproperty."/>		
		<cfargument name="chaine" 		required="true"  type="string"  default="" displayname="numeric ID" 	hint="Initial value for the chaine porperty." />
		<cfargument name="niveau"   	required="true"  type="string"  default="" displayname="niveau" 		hint="Initial value for the chaine property."/>
		<cfargument name="iddatedeb" 	type="numeric" 	required="true">
		<cfargument name="iddatefin"   type="numeric" 	required="true">
		
		<cfreturn Strategy.getData(idGroupe, idCliche, chaine, niveau,iddatedeb,iddatefin)>
	</cffunction>
	
	<cffunction name="listeLigne" access="public" returntype="query" output="false" displayname="query queryData(numeric)" >		
		<cfargument name="idGroupe" 	required="true"  type="numeric" default="" displayname="numeric ID" 	hint="Initial value for the IDproperty."/>
		<cfargument name="idCliche" 	required="true"  type="numeric" default="" displayname="numeric ID" 	hint="Initial value for the IDproperty."/>		
		<cfargument name="niveau"   	required="true"  type="string"  default="" displayname="niveau" 		hint="Initial value for the chaine property."/>
		<<cfargument name="iddatedeb" 	type="numeric" 	required="true">
		<cfargument name="iddatefin"   type="numeric" 	required="true">
		
		<cfreturn Strategy.getLignes(idGroupe, idCliche, niveau,iddatedeb,iddatefin)>
	</cffunction>

</cfcomponent>