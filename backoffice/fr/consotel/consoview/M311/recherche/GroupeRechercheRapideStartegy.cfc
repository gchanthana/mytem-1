<cfcomponent name="GroupeMaitreRechercheRapide" displayname="GroupeMaitreRechercheRapide" hint="Recherche rapide pour un groupe maitre">

	<cffunction name="init" access="public" output="false" returntype="GroupeCompletStrategy" displayname="GroupeCompletStrategy init()" hint="Initialize the GroupeCompletStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
	
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, numeric, string)">		
		<cfargument name="idGroupe" required="true" type="numeric" default="" displayname="numeric ID" 				hint="Initial value for the IDproperty."/>
		<cfargument name="idCliche" required="true" type="numeric" default="" displayname="numeric ID" 				hint="Initial value for the IDproperty."/>
		<cfargument name="chaine"   required="true" type="string"  default="" displayname="la chaine de recherche" 	hint="Initial value for the chaine property."/>
		<cfargument name="niveau"   required="true" type="string"  default="" displayname="la chaine de recherche" 	hint="Initial value for the chaine property."/>
		
			<cfstoredproc datasource="E0" procedure="PKG_M311.SEARCH_NODES_HIERARCHIE">		
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_chaine" 			value="#chaine#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idGroupe#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_niveau" 			value="#niveau#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcliche" 		value="#idCliche#"/>
				<cfprocresult name="p_result"/>        
			</cfstoredproc>
			
		<cfreturn p_result/>
	</cffunction>	
	
	<cffunction name="getLignes" access="public" returntype="query" output="false" displayname="query getData(numeric, numeric)">
		<cfargument name="idGroupe" required="true" type="numeric" default="" displayname="numeric ID" 	hint="Initial value for the IDproperty."/>
		<cfargument name="idCliche" required="true" type="numeric" default="" displayname="numeric ID" 	hint="Initial value for the IDproperty."/>
		<cfargument name="niveau"   required="true" type="string"  default="" displayname="niveau orga" hint="Initial value for the chaine property."/>
			
			<cfstoredproc datasource="E0" procedure="PKG_M311.SEARCH_NODES_SOUS_TETES">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idGroupe#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_niveau" 			value="#niveau#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcliche" 		value="#idCliche#"/>
				<cfprocresult name="p_result"/>
			</cfstoredproc>
			
		<cfreturn p_result/>
	</cffunction>	
	
</cfcomponent>