<cfcomponent output="false">
	
	<cfset VARIABLES["reportServiceClass"]="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService">
	<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
<!--- ***************************** CODE A MODIFIER ******************************* --->		
	<!--- Le chemin du rapport sous BIP --->
	<cfset VARIABLES["XDO"]="/consoview/M311/TB-REPORT-PRODUIT/TB-REPORT-PRODUIT.xdo">
	<!--- Le code rapport --->
	<cfset VARIABLES["CODE_RAPPORT"]="TBD-PR">
	<!--- TEMPLATE BI --->
	<cfset VARIABLES["TEMPLATE"]="PRODUIT">
	<!--- OUTPUT --->
	<cfset VARIABLES["OUTPUT"]="Tableau de bord - inventaire des produits">
	<!--- Module --->
	<cfset VARIABLES["MODULE"]="M311">
	<!--- Format --->
	<cfset VARIABLES["FORMAT"]="pdf">
	<!--- Extension --->
	<cfset VARIABLES["EXT"]="pdf">
<!--- ************************************************************ --->
	
	<cfset reportService = createObject("component",VARIABLES["reportServiceClass"])>
	<cfset ApiE0 = CreateObject("component","fr.consotel.consoview.api.E0.ApiE0")>
	<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>
	
<cffunction name="run" access="remote" returnType="numeric" >
	
	
	<cfargument name="idproduit_client" type="numeric" required="true">
	<cfargument name="datefin" type="string" required="true">
	<cfargument name="tb" type="string" required="true">
	<cfargument name="mode" type="string" required="true">
	<cfargument name="raisonsociale" type="string" required="true">
	<cfargument name="modeCalcul" type="string" required="true">
	<cfargument name="numero" type="numeric" required="true">
	<cfargument name="PATH_PRDT" type="string" required="true">
	<cfargument name="format" type="string" required="true">
	<cfargument name="perimetre" type="string" required="true">
	<cfargument name="datedeb" type="string" required="true">
	
	
	<cfset VARIABLES["FORMAT"]=#format#>
		
	<cfif ucase(FORMAT) eq "excel">
		<cfset VARIABLES["EXT"]="XLS">
	<cfelse>
		<cfif ucase(FORMAT) eq "CSV">
			<cfset VARIABLES["EXT"]="CSV">
		<cfelse>
			<cfif ucase(FORMAT) eq "PDF">
				<cfset VARIABLES["EXT"]="PDF">
			</cfif>
		</cfif>
	</cfif>
	
	<cfset initStatus=reportService.init(SESSION.PERIMETRE["ID_GROUPE"],VARIABLES["XDO"],"ConsoView",VARIABLES["MODULE"],VARIABLES["CODE_RAPPORT"])>
	
	<cfif initStatus EQ TRUE>
	
		<cfset setStatus=TRUE>
		
		<cfset ArrayOfParamNameValues=ArrayNew(1)>
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_IDMASTER">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.IDRACINE_MASTER>
		<cfset ParamNameValue.values=t>	
		<cfset ArrayOfParamNameValues[1]=ParamNameValue>	
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_IDRACINE">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.IDRACINE>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[2]=ParamNameValue>	
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_IDORGA">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.IDORGA>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[3]=ParamNameValue>
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_IDPERIMETRE">
		<cfset t=ArrayNew(1)>   
		<cfset t[1]=SESSION.PARAMS_EXPORT.IDPERIMETRE>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[4]=ParamNameValue>
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_IDPRODUIT">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.IDPRODUIT>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[5]=ParamNameValue>
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="U_IDPERIOD_DEB">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.IDDEBUT>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[6]=ParamNameValue>
	
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="U_IDPERIOD_FIN">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.IDFIN>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[7]=ParamNameValue>
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_NIVEAU">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.NIVEAU>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[8]=ParamNameValue>
			
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_LANGUE_PAYS">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.LANG>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[9]=ParamNameValue>
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_PERIMETRE">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=raisonsociale>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[10]=ParamNameValue>	
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_ORDER_BY_TEXTE_COLONNE">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.ORDER_BY_TEXTE_COLONNE>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[11]=ParamNameValue>	
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_ORDER_BY_TEXTE">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.ORDER_BY_TEXTE>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[12]=ParamNameValue>	
		
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_SEARCH_TEXT_COLONNE">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.SEARCH_TEXT_COLONNE>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[13]=ParamNameValue>	
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_SEARCH_TEXT">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.SEARCH_TEXT>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[14]=ParamNameValue>	
		
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_OFFSET">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=val(SESSION.PARAMS_EXPORT.OFFSET)>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[15]=ParamNameValue>	
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="G_LIMIT">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.PARAMS_EXPORT.LIMIT>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[16]=ParamNameValue>	
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="PATH_PRDT">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=PATH_PRDT>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[17]=ParamNameValue>	
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="URL_LOGO">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=getCacheUrl(SESSION.CODEAPPLICATION)>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[18]=ParamNameValue>	
		
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="GEST">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=SESSION.USER.PRENOM & ' ' & SESSION.USER.NOM>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[19]=ParamNameValue>	
		
		<cfloop array="#ArrayOfParamNameValues#" index="OBJ">
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]= #OBJ.VALUES[1]#>
			<cfset setStatus=setStatus AND reportService.setParameter("#OBJ.NAME#",tmpParamValues)>
		</cfloop>
		
		<cfset pretour = -1>
		<cfif setStatus EQ TRUE>
			<cfset FILENAME=VARIABLES["OUTPUT"]>
			<cfset outputStatus = FALSE>
			<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],VARIABLES["FORMAT"],VARIABLES["EXT"],FILENAME,VARIABLES["CODE_RAPPORT"])>
			<cfif outputStatus EQ TRUE>
				<cfset reportStatus = FALSE>
				<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
				<cfset pretour = reportStatus['JOBID']>
				<cfif reportStatus['JOBID'] neq -1>
					<cfreturn reportStatus["JOBID"]>
				</cfif>
			</cfif>
		</cfif>	
	</cfif>
	<cfreturn pretour >
</cffunction>

<cffunction name="getCacheUrl" returntype="String" hint="retourne l'url du cache selon le code application">
		<cfargument name="code_application" type="string" required="true">
		
		<cfset url_cache =''>
		<cfset STR_URL_CACHE = {}>
		<cfset STR_URL_CACHE = createobject("component","fr.consotel.consoview.M311.ApplicationProperty").getProperty(SESSION.CODEAPPLICATION,'URL_CACHE')>		
		<cfif structKeyExists(STR_URL_CACHE,'URL_CACHE')>
			<cfset url_cache = STR_URL_CACHE['URL_CACHE']>
		</cfif>
		

		<cfset url_logo = url_cache &  '/assets/application-logo-#code_application#.jpg'>
		<cfreturn url_logo>
	</cffunction>
	
</cfcomponent>