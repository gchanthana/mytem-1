﻿	<cfset xdo_path='/consoview/M311/TB-REPORT-PRODUIT/TB-REPORT-PRODUIT.xdo'/>
	
	<cfset url_logo= getCacheUrl(SESSION.CODEAPPLICATION)/>
	
	<cfset biServer = APPLICATION.BI_SERVICE_URL>
	
	<cfset locale = SESSION.PARAMS_EXPORT.LANG>	
	
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_IDMASTER">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.IDRACINE_MASTER>
	<cfset ParamNameValue.values=t>	
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_IDRACINE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.IDRACINE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[2]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_IDORGA">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.IDORGA>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[3]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_IDPERIMETRE">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=SESSION.PARAMS_EXPORT.IDPERIMETRE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[4]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_IDPRODUIT">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.IDPRODUIT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[5]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="U_IDPERIOD_DEB">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.IDDEBUT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[6]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="U_IDPERIOD_FIN">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.IDFIN>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[7]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_NIVEAU">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.NIVEAU>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[8]=ParamNameValue>
		
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_LANGUE_PAYS">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=locale>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[9]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_PERIMETRE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=FORM.RAISONSOCIALE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[10]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_ORDER_BY_TEXTE_COLONNE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.ORDER_BY_TEXTE_COLONNE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[11]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_ORDER_BY_TEXTE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.ORDER_BY_TEXTE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[12]=ParamNameValue>	
	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_SEARCH_TEXT_COLONNE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.SEARCH_TEXT_COLONNE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[13]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_SEARCH_TEXT">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.SEARCH_TEXT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[14]=ParamNameValue>	
	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_OFFSET">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=val(SESSION.PARAMS_EXPORT.OFFSET)>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[15]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_LIMIT">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PARAMS_EXPORT.LIMIT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[16]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="PATH_PRDT">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=FORM.PATH_PRDT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[17]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="URL_LOGO">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=url_logo>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[18]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="GEST">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.USER.PRENOM & ' ' & SESSION.USER.NOM>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[19]=ParamNameValue>	
	
	
	<cfset myParamReportRequest=structNew()>
	
	<cfset myParamReportRequest.reportAbsolutePath=xdo_path>
	
	<cfset myParamReportRequest.attributeTemplate="PRODUIT">
	
	<cfset myParamReportRequest.attributeLocale=locale>
	<cfset myParamReportRequest.attributeFormat=LCase(FORM.FORMAT)>
	<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
	<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
	<cfset myParamReportParameters=structNew()>
	<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
	
	<cfset myParamReportParameters.userID="consoview">
	<cfset myParamReportParameters.password="public">
	<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
	<cfset reporting=createObject("component","fr.consotel.consoview.api.Reporting")>

	<cfset filename = "TB-" & UCase(FORM.TB) & "-" & FORM.PERIMETRE & "-" &
			replace(session.perimetre.raison_sociale,' ','_',"all") & reporting.getFormatFileExtension(FORM.FORMAT)>
	
	<cfif SESSION.PARAMS_EXPORT.LIMIT gt -1>
		<cfheader name="Content-Disposition" value="attachment;filename=#filename#" charset="utf-8">
		<cfif compareNoCase(trim(FORM.FORMAT),"csv") eq 0>
			<cfcontent type="text/csv; charset=utf-8" variable="#resultRunReport.getReportBytes()#">
			<cfelse>
			<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">		
		</cfif>
	<cfelse>
		<div width="100%" align="center">
			<h1>Le rapport est trop long ........ redirection vers les containers</h1>
			<button type="button" onclick="JavaScript:window.close()">Fermer</button>
		</div>
		<cfset obj = createObject("component","PrintListProduitContainer")>
		<cfset result = obj.run(ArrayOfParamNameValues,FORM.FORMAT,FORM.FORMAT)>
		<cfoutput>#result#</cfoutput>
	</cfif>
	
	<!--- FUNCTIONS ================================================================================================================== --->
	<cffunction name="getCacheUrl" returntype="String" hint="retourne l'url du cache selon le code application">
		<cfargument name="code_application" type="string" required="true">
		<cfset url_cache = "https://cache.consotel.fr">
		<cfif structkeyexists(SESSION,"URL_CACHE_#code_application#")>
			<cfset url_cache = "URL_CACHE">
		</cfif>
		<cfset url_logo = url_cache &  '/assets/application-logo-#code_application#.jpg'>
		<cfreturn url_logo>
	</cffunction>
