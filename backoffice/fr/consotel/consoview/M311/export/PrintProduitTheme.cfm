﻿	<cfset biServer=APPLICATION.BI_SERVICE_URL>

	<cfset locale = SESSION.USER.GLOBALIZATION>
	
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_IDMASTER">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE.IDRACINE_MASTER>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>
	<cfset xdo_path='/consoview/M311/TB-PRODUIT_THEME-REPORT/TB-PRODUIT_THEME-REPORT.xdo'/>
	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_IDRACINE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE.ID_GROUPE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[2]=ParamNameValue>
	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_IDORGA">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE.SELECTED_ID_ORGA>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[3]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_IDPERIMETRE">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.IDENTIFIANT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[4]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_IDCLICHE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE.SELECTED_ID_CLICHE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[5]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="U_IDPERIOD_DEB">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[6]=ParamNameValue>

	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="U_IDPERIOD_FIN">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[7]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_NIVEAU">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE.SELECTED_NIVEAU>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[8]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_IDTHEME">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=idTheme>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[9]=ParamNameValue>
		
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_LANGUE_PAYS">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=locale>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[10]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="G_PERIMETRE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=FORM.RAISONSOCIALE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[11]=ParamNameValue>	
	
	<cfset myParamReportRequest=structNew()>
	
	<cfset myParamReportRequest.reportAbsolutePath=xdo_path>
	
	
	<cfset myParamReportRequest.attributeTemplate="COMPLET">
	
	<cfset myParamReportRequest.attributeLocale=locale>
	<cfset myParamReportRequest.attributeFormat=LCase(FORM.FORMAT)>
	<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
	<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
	<cfset myParamReportParameters=structNew()>
	<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
	
	<cfset myParamReportParameters.userID="consoview">
	<cfset myParamReportParameters.password="public">
	<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
	<cfset reporting=createObject("component","fr.consotel.consoview.api.Reporting")>

	<cfset filename = "TB-" & UCase(FORM.TB) & "-" & FORM.PERIMETRE & "-" &
			replace(session.perimetre.raison_sociale,' ','_',"all") & reporting.getFormatFileExtension(FORM.FORMAT)>
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#" charset="utf-8">
	<cfif compareNoCase(trim(FORM.FORMAT),"csv") eq 0>
		<cfcontent type="text/csv; charset=utf-8" variable="#resultRunReport.getReportBytes()#">
		<cfelse>
		<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">		
	</cfif>