<cfcomponent output='false' displayname="fr.consotel.consoview.M311.ApplicationProperty">
	
	<cffunction name='getProperty' access='public' returntype='any' output='true'>
	    <cfargument name='codeapp' 		required='true' 	type='Numeric'/>
	    <cfargument name='key' 			required='false' 	type='string'/>
			
			<cfset bool = 'yes'>
			<cfset _key = ''>
			
			<cfif isdefined('key')>
				<cfset _key = key>
				<cfset bool = 'no'>				
			</cfif>
		
			<cfstoredproc datasource='ROCOFFRE' procedure='pkg_m00.get_appli_properties'>
				<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER'  variable='p_code_appli' 	value='#codeapp#'>
				<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR'  variable='p_key' 		value='#_key#' null='#bool#'/>
				<cfprocresult name='qProperty'>
			</cfstoredproc>
			
			<cfset property = {}>
			<cfloop query='qProperty'>
				<cfset property[qProperty.KEY] = qProperty.VALUE>
			</cfloop>
			
		<cfreturn property>
	</cffunction>
	
</cfcomponent>