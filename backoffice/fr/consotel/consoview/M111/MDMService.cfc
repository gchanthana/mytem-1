<cfcomponent displayname="fr.consotel.consoview.M111.MDMService" extends="fr.saaswedo.api.patterns.cfc.Component" hint="Implémentation spécifique
au FrontOffice MyTEM (Module M111) et instancié par Flash Remoting (Flex). Le constructeur init() est explicitement appelé à chaque instanciation">
	<!--- Le constructeur init() est explicitement appelé à chaque instanciation --->
	<cfset init()>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.patterns.cfc.Component" hint="Constructeur qui est appelé explicitement
	lors de l'instanciation de ce composant avec createObject() : Une exception est levée si ce composant est instancié avec l'opérateur new.
	Cette implémentation appele le constructeur parent puis initialise les propriétés spéficiques à ce composant">
		<!--- Constructeur hérité de Component --->
		<cfset SUPER.init()>
		<!--- API MDM (Saaswedo) : La durée de vie de l'instance est celle de du remoting --->
		<cfset VARIABLES.REMOTING_MDM_INFOS=new fr.saaswedo.api.myTEM.mdm.MDM()>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.consotel.consoview.M111.MDMService">
		<cfreturn "fr.consotel.consoview.M111.MDMService">
	</cffunction>
	
	<!--- =========== Fonctions avec un accès REMOTE (Remoting provenant du FrontOffice MyTEM) =========== --->
	<!---  si isManaged retoure true cela vaut dire qu'on peux enroller le telephone ' --->
	<!---  si isManaged retoure true cela vaut dire que le telephone est bien enrollé dans le MDM et peux effecture d'autre action ( verrouiller, geolocalisation  ) ' --->
		
	<cffunction access="remote" name="isManaged" returntype="Boolean" output="false" hint="Permet de récupérer le status d'un device">
		<cfargument name="serialNumber"	type="String"		required="true">
		<cfargument name="imei"		type="String"		required="true">
		<cfset var mdm=mdmInfos()>
		<cfset var mdmServer=mdm.getIMDM()>
		<cfset var infoService=mdmServer.getIDeviceInfo()>
		<cfreturn (infoService.getManagedStatus(ARGUMENTS.serialNumber,ARGUMENTS.imei) EQ infoService.MANAGED())>
		

		<!---
		<cfset myTEM=createObject("component","fr.saaswedo.api.myTEM.mdm.MyTEM").getInstance()>
		<cfset service=myTEM.getIDeviceInfo()>
		<cfset isManaged= (service.getManagedStatus(imei,serial) EQ "MANAGED") ? true:false>
		<cfreturn isManaged>
		--->
	</cffunction>
	

	<!--- Verrouille un terminal --->
	<cffunction access="remote" name="lock" returntype="Boolean" output="false" hint="Verrouille un terminal">
		<cfargument name="imei" 		type="String" 	required="true">
		<cfargument name="serial" 		type="String" 	required="true">
		<cfargument name="mdmimei" 		type="String" 	required="true">
		<cfargument name="idterminal"  	type="Numeric"	required="true">
		<cfargument name="pincode" 		type="String"  	required="false" default="0000">
		<cfargument name="wait" 		type="Numeric"	required="false" default="30000">
		<cfargument name="key" 			type="String"	required="false" default="">
		<cfset var mdm=mdmInfos()>
		<cfset var mdmServer=mdm.getIMDM()>
		<cfset var manageService=mdmServer.getIDeviceManagement()>
		<cfreturn manageService.lock(ARGUMENTS.serial,ARGUMENTS.mdmimei,ARGUMENTS.pincode)>
			<!---
			<cfset var idracine  = session.perimetre.id_groupe>
			<cfset var mdmResult = -1>
			
			<cftry>

				<cfset createObject("component", "fr.consotel.api.mdm.Lock").execute(serial, mdmimei, pincode, wait, key, idracine)>
				
				<cfset var logResult = setLogsAction(imei, idterminal, 24)>
			
				<cfset var mdmResult = 1>
				
			<cfcatch>
				
				<cflog type="error" text="[MDMService] Lock error : #CFCATCH.message# [#CFCATCH.detail#]">
				<cfrethrow>
				
			</cfcatch>
			</cftry>
			
		<cfreturn mdmResult>
		--->
	</cffunction>
	
	<!--- Déverrouille un terminal --->
	<cffunction access="remote" name="unlock" returntype="Boolean" output="false" hint="Déverrouille un terminal">
		<cfargument name="imei" 		type="String" 	required="true">
		<cfargument name="serial" 		type="String" 	required="true">
		<cfargument name="mdmimei" 		type="String" 	required="true">
		<cfargument name="idterminal"  	type="Numeric"	required="true">
		<cfargument name="pincode" 	type="String"  	required="false" default="0000">
		<cfargument name="wait" 	type="Numeric"	required="false" default="30000">
		<cfset var mdm=mdmInfos()>
		<cfset var mdmServer=mdm.getIMDM()>
		<cfset var manageService=mdmServer.getIDeviceManagement()>
		<cfreturn manageService.unlock(ARGUMENTS.serial,ARGUMENTS.mdmimei)>
		<!---
			<cfset var idracine  = session.perimetre.id_groupe>
			<cfset var mdmResult = -1>
			
			<cftry>

				<cfset createObject("component", "fr.consotel.api.mdm.Unlock").execute(serial, mdmimei, pincode, wait, idracine)>
				
				<cfset var logResult = setLogsAction(imei, idterminal, 32)>
				
				<cfset var mdmResult = 1>
				
			<cfcatch>
				
				<cflog type="error" text="[MDMService] Unlock error : #CFCATCH.message# [#CFCATCH.detail#]">
				<cfrethrow>
				
			</cfcatch>
			</cftry>
			
		<cfreturn mdmResult>
		--->
	</cffunction>
	
	<!--- Efface les données d'un terminal --->
	<cffunction access="remote" name="wipe" returntype="Boolean" output="false" hint="Efface les données d'un terminal">
		<cfargument name="imei" 		type="String" 	required="true">
		<cfargument name="serial" 		type="String" 	required="true">
		<cfargument name="mdmimei" 		type="String" 	required="true">
		<cfargument name="idterminal"  	type="Numeric"	required="true">
		<cfargument name="ismemcard" 	type="Boolean" 	required="true"  default="false">
		<cfargument name="isEraseAll" 	type="Boolean" 	required="true"  default="true">
		<cfargument name="pincode" 		type="String"  	required="false" default="0000">
		<cfargument name="wait" 		type="Numeric"	required="false" default="30000">
		<cfargument name="key" 			type="String"	required="false" default="">
		<cfset var mdm=mdmInfos()>
		<cfset var mdmServer=mdm.getIMDM()>
		<cfset var manageService=mdmServer.getIDeviceManagement()>
		<cfif ARGUMENTS.isEraseAll>
			<cfreturn manageService.wipe(ARGUMENTS.serial,ARGUMENTS.mdmimei,ARGUMENTS.ismemcard)>
		<cfelse>
			<cfreturn manageService.corporateWipe(ARGUMENTS.serial,ARGUMENTS.mdmimei)>
		</cfif>
		<!---
			<cfset var idracine  = session.perimetre.id_groupe>
			<cfset var mdmResult = -1>
			
			<cftry>
				<cfset createObject("component", "fr.consotel.api.mdm.Wipe").execute(serial, mdmimei, idterminal, ismemcard, isEraseAll, pincode, wait, key, idracine)>
				<cfif isEraseAll EQ true>
					
					<cfset var logResult = setLogsAction(imei, idterminal, 26)>
				
				<cfelse>
					
					<cfset var logResult = setLogsAction(imei, idterminal, 25)>
				
				</cfif>
				
				<cfset var mdmResult = 1>
				
			<cfcatch>
				<cflog type="error" text="[MDMService] Wipe error : #CFCATCH.message# [#CFCATCH.detail#]">
				<cfrethrow>
			</cfcatch>
			</cftry>
			
		<cfreturn mdmResult>
		--->
	</cffunction>
	
	<!--- Sort un terminal de l'enrollement et si on souhaite enrollé de nouveau ce terminal  => isAuthorize = 1 --->
	<cffunction access="remote" name="revoke" returntype="Any" output="false" hint="Sort un terminal de l'enrollement et si on souhaite enrollé de nouveau ce terminal  => isAuthorize = 1">
		<cfargument name="imei" 		type="String" 	required="true">
		<cfargument name="serial" 		type="String" 	required="true">
		<cfargument name="mdmimei" 		type="String" 	required="true">
		<cfargument name="idterminal"  	type="Numeric"	required="true">
		<cfargument name="isAuthorize" 	type="Numeric"	required="false"  default="0">
		<cfargument name="key" 			type="String"	required="false" default="">
		<cfset var mdm=mdmInfos()>
		<cfset var mdmServer=mdm.getIMDM()>
		<cfset var manageService=mdmServer.getIDeviceManagement()>
		<cfreturn manageService.revoke(ARGUMENTS.serial,ARGUMENTS.mdmimei)>
			<!---
			<cfset var idracine  = session.perimetre.id_groupe>
			<cfset var mdmResult = -1>
			
			<cftry>
				
				<cfset createObject("component", "fr.consotel.api.mdm.Revoke").execute(serial, imei, isAuthorize, key, idracine)>
				<cflog type="information" text="[M111] Revoke : #ARGUMENTS.serial# - #ARGUMENTS.imei#">
				<cfset var logResult = setLogsAction(imei, idterminal, 33)>
				
				<cfset var mdmResult = 1>
				
			<cfcatch>
				<cflog type="error" text="[MDMService] Revoke error : #CFCATCH.message# [#CFCATCH.detail#]">
				<cfrethrow>
			</cfcatch>
			</cftry>
			
		<cfreturn mdmResult>
		--->
	</cffunction>
	
	<!--- Permet de géolocaliser un device --->
	<cffunction access="remote" name="locate" returntype="Struct" hint="Permet de géolocaliser un device<br>
	La structure retournée contient :<br>- LOCATE_STATUS : -1 si la propriété errorCode de l'exception capturée est MDM_LOCATE_ERROR,
	-2 pour les autres valeur de errorCode et 1 si l'opération s'est déroulée sans exceptions<br><br>
	- LOCATE_MESSAGE : Chaine vide si l'opération s'est déroulée avec succès et sinon contient le message d'erreur correspondant à l'exception capturée">
		<cfargument name="serial" 		type="String" 	required="true">
		<cfargument name="imei" 		type="String" 	required="true">
		<cfargument name="wait" 		type="Numeric"	required="false" default="30000">
		<cfset var mdm=mdmInfos()>
		<cfset var mdmServer=mdm.getIMDM()>
		<cfset var manageService=mdmServer.getIDeviceManagement()>
		<cfset var locateStatus=manageService.locate(ARGUMENTS.serial,ARGUMENTS.imei)>
		<cfreturn {LOCATE_STATUS=(locateStatus ? 1:0), LOCATE_MESSAGE=""}>
		<!---
		<cftry>
			<cfset createObject("component", "fr.consotel.api.mdm.Locate").execute(serial,imei,wait)>
			<cfcatch type="Any">
				<cflog type="error" text="[MDMService] Locate error : #CFCATCH.message# [#CFCATCH.detail#]">
				<cfrethrow>
				<!--- 
				<cfif CFCATCH["errorCode"] EQ "MDM_LOCATE_ERROR">
					<cfreturn {LOCATE_STATUS=-1, LOCATE_MESSAGE="[MDM] Unable to locate device [#serial#,#imei#]"}>
				<cfelse>
					<cfrethrow>
				</cfif>
				 --->
			</cfcatch>
		</cftry>
		<cfreturn {LOCATE_STATUS=1, LOCATE_MESSAGE=""}>
		--->
	</cffunction>
	
	<cffunction access="remote" name="getDeviceInfo" returntype="Struct"
	hint="Retourne une structure contenant les infos d'un Device représentées par les clés : GENERAL, SOFTWAREINVENTORY, DEVICEPROPERTIES<br>
	Chacune de ces clés associée à un tableau dont chaque élément est une structure contenant les clés suivantes :
	<br>GENERAL :<br> ALL_WHITELIST_COMPLIANT, BLACKLIST_COMPLIANT, DEVICE_ID, DEVICE_PROPERTIES_TIMESTAMP, LAST_INVENTORY_TIME, LAST_PROFILE_INVENT_TIME,
	LAST_USER_ID, WHITELIST_COMPLIANT, LAST_USERNAME, OS_FAMILY, STRONG_ID, FIRST_CONNECTION_DATE (Date de 1ère connexion),
	LAST_AUTH_DATE (Date de dernière connexion), IMEI (IMEI passé en paramètre), SERIAL_NUMBER (SerialNumber passé en paramètre), TARGETNODE (IP du serveur Zenprise)<br>
	SOFTWAREINVENTORY :<br>BLACKLIST_COMPLIANT, DEVICE_ID, PACKAGE_INFO, SOFTWARE_ID, WHITELIST_COMPLIANT,
	AUTHOR (Editeur du software), INSTALL_TIME (Date d'installation ou Chaine vide), NAME (Nom du software), SIZE (Taille en Mo), VERSION (Version)<br>
	DEVICEPROPERTIES :<br> NAME (Nom de la propriété), VALUE (Valeur de la propriété), CATEGORIE (Libellé affiché dans le FrontOffice pour la propriété)">
		<cfargument name="serialNumber" 	type="string" 	required="true">
		<cfargument name="imei" 	type="string" 	required="true">
		<cfset var mdm=mdmInfos()>
		<cfset var mdmServer=mdm.getIMDM()>
		<cfset var infoService=mdmServer.getIDeviceInfo()>
		<cfset var deviceInfo=infoService.getDeviceInfo(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
		<cfset var deviceInfoResult={}>
		<!--- La structure deviceInfo est vide si le device n'est pas présent dans le MDM --->
		<cfif structIsEmpty(deviceInfo)>
			<!--- Dans Flex : Objet vide --->	
		<cfelse>
			<!--- Dans Flex : Objet avec les infos du device --->
			<!--- GENERAL : Certaines propriétés sont présentes pour des raisons historiques avec Zenprise e.g BLACKLIST_COMPLIANT --->
			<cfset var currentGeneral=deviceInfo.GENERAL>
			<cfset var generalResult={
				ALL_WHITELIST_COMPLIANT="",BLACKLIST_COMPLIANT="",DEVICE_ID=0,DEVICE_PROPERTIES_TIMESTAMP=0,LAST_INVENTORY_TIME=0,
				LAST_PROFILE_INVENT_TIME=0,LAST_USER_ID=0,WHITELIST_COMPLIANT="",LAST_USERNAME="",OS_FAMILY=currentGeneral["OS"],
				FIRST_CONNECTION_DATE=currentGeneral["FIRSTCONNECTIONDATE"],LAST_AUTH_DATE=currentGeneral["LASTAUTHDATE"],
				IMEI=currentGeneral["IMEI"],SERIAL_NUMBER=currentGeneral["SERIAL_NUMBER"]
			}>
			<cfset deviceInfoResult.GENERAL=[generalResult]>
			<!--- SOFTWAREINVENTORY --->
			<cfset deviceInfoResult.SOFTWAREINVENTORY=deviceInfo.SOFTWARE>
			<!--- DEVICEPROPERTIES --->
			<cfset var deviceArrayResult=[]>
			<cfset var deviceArray=deviceInfo["PROPERTIES"]>
			<cfset var myTEM=createMyTEM()>
			<!--- Table de correspondance des propriétés des Devices --->
			<cfset var propertyMap=myTEM.getDevicePropertyMap()>
			<cfloop index="i" from="1" to="#arrayLen(deviceArray)#">
				<cfset var currentProperty=deviceArray[i]>
				<!--- Libellé par défaut de la propriété sinon c'est le libellé provenant de getDevicePropertyMap() --->
				<cfset var propertyLabel=structKeyExists(propertyMap,currentProperty["NAME"]) ? propertyMap[currentProperty["NAME"]]["LABEL"]:currentProperty["NAME"]>
				<!--- Catégorie par défaut de la propriété sinon c'est la catégorie provenant de getDevicePropertyMap() --->
				<cfset var propertyCategory=structKeyExists(propertyMap,currentProperty["NAME"]) ? propertyMap[currentProperty["NAME"]]["CATEGORY"]:"MDM">
				<cfset deviceArrayResult[i]={NAME=propertyLabel, VALUE=currentProperty["VALUE"], CATEGORIE=propertyCategory}>
			</cfloop>
			<cfset deviceInfoResult.DEVICEPROPERTIES=deviceArrayResult>
		</cfif>
		<cfreturn deviceInfoResult>
	</cffunction>
	
	<cffunction access="remote" name="sendEnrollmentInfos" returntype="void" output="false"
	hint="Envoie par mail les instructions d'enrollment par mail. Un SMS est aussi envoyé quand une ligne est affecté à l'équipement concerné">
		<cfargument name="sup_bug" type="Numeric" required="true">
		<cfargument name="params" type="Struct" required="true">
		<cfset var parameters=ARGUMENTS.params>
		<cfset var userId=VAL(SESSION.USER.CLIENTACCESSID)>
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<cfif structKeyExists(parameters,"USERID")>
			<cfset var frontUserId=VAL(parameters.USERID)>
			<cfif frontUserId EQ userId>
				<cfif structKeyExists(parameters,"SERIAL") AND structKeyExists(parameters,"IMEI") AND structKeyExists(parameters,"imeiMyTEM")>
					<cfset var serialValue=parameters.SERIAL>
					<cfset var imeiValue=parameters.IMEI>
					<!--- Autorisation d'enrollment dont l'implémentation dépend du provider --->
					<cfset var mdm=mdmInfos()>
					<cfset var mdmServer=mdm.getIMDM()>
					<cfset var infoService=mdmServer.getIDeviceInfo()>
					<cfif infoService.deviceExists(serialValue,imeiValue)>
						<cfset var manageService=mdmServer.getIDeviceManagement()>
						<cfset var registerStatus=manageService.register(serialValue,imeiValue)>
					</cfif>
					<!--- Destinataire des infos d'enrollment --->
					<cfset var mailTo=structKeyExists(parameters,"EMAIL") ? parameters.EMAIL:SESSION.USER.EMAIL>
					<cfset var userLocale=structKeyExists(parameters,"userLocale") ? parameters.userLocale:"fr_FR">
					<cfset var ligne=structKeyExists(parameters,"ligne") ? TRIM(parameters.ligne):"">
					<!--- Envoi SMS et du mail des infos d'enrollment --->
					<cfset sendEnrollmentInfosByMail(serialValue,imeiValue,parameters.imeiMyTEM,mailTo,ligne,userLocale)>
				<cfelse>
					<cfset var idTerminal=structKeyExists(parameters,"idTerminal") ? parameters.idTerminal:"N.D">
					<cfset var errorMsg="Missing SERIAL and IMEI (MYTEM_IMEI) from USERID #frontUserId# for idTerminal #idTerminal#">
					<cfthrow type="Custom" errorCode="#code#" message="MDMService invalid infos: #errorMsg#">
				</cfif>
			<cfelse>
				<cfset var errorMsg="USERID #frontUserId# is different from value in SESSION #userId#">
				<cfthrow type="Custom" errorCode="#code#" message="MDMService Security error: #errorMsg#">
			</cfif>
		</cfif>
		<!---
		<cfset performSendEmail(ARGUMENTS.sup_bug,ARGUMENTS.params)>
		--->
		<!---
		<cfset var serial=ARGUMENTS["params"]["serial"]>
		<cfset var imei=ARGUMENTS["params"]["imei"]>
		<!--- API MDM MyTEM --->
		<cfset var myTEM=createObject("component","fr.saaswedo.api.myTEM.mdm.MyTEM").getInstance()>
		<cfset var iDeviceInfo=myTEM.getIDeviceInfo()>
		<cfif iDeviceInfo.deviceExists(serial,imei)>
			<cfset var iDeviceManagement=myTEM.getIDeviceManagement()>
			<!--- Autorisation d'enrollment : Retourne toujours TRUE --->
			<cfset authorizeResult=iDeviceManagement.register(serial,imei)>
			<cflog type="information" text="(MAIL) Enrollment AUTORIZE [#serial#,#imei#] : #authorizeResult#">
		</cfif>
		<cfset performSendEmail(ARGUMENTS.sup_bug,ARGUMENTS.params)>
		--->
	</cffunction>
	
	<cffunction access="private" name="sendEnrollmentInfosBySMS" returntype="void" output="false" hint="Envoi par SMS des liens d'enrollment">
		<cfargument name="serial" type="String" required="true" hint="Numéro de série du device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du device">
		<cfargument name="ligne" type="String" required="true" hint="Numéro de ligne">
		<cfargument name="userLocale" type="String" required="false" default="fr_FR" hint="Locale">
		<cfset var sousTete=TRIM(ARGUMENTS.ligne)>
		<cfset var operator="SFR">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<!--- API SMS --->
		<cfset var statusSMS=10>
		<cfset var idracine = session.perimetre.id_groupe>
		<cfset var urlwebserv=createObject("component", "fr.consotel.api.sms.public.ApiSmsPublic")>
		<cfset var uuid = urlwebserv.createCampagne(idracine)>
		<cfif isDefined("uuid") AND uuid NEQ "">
			<cfset var delay		= "5000">
			<cfset var statusSMS 	= -10>
			<!--- API MDM --->
			<cfset var mdm=mdmInfos()>
			<cfset var mdmServer=mdm.getIMDM()>
			<!--- Contenu du SMS --->
			<cfset var iOSLink=mdmServer.getEnrollmentURL(mdm.IOS(),FALSE)>
			<cfset var androidLink=mdmServer.getEnrollmentURL(mdm.ANDROID(),TRUE)>
			<cfset var contentSms="iOS : " & iOSLink & " | " & "ANDROID : " & androidLink>
			<!--- Envoi du SMS --->
			<cfset var rsltFindNoCase = FindNoCase("0",sousTete)>
			<cfset var sousTeteWithIndicatif = sousTete>
			<cfif rsltFindNoCase EQ 1>
				<cfset sousTeteWithIndicatif = ligneToSmsFormat(sousTeteWithIndicatif)>
			</cfif>
			<!---
			<cfset var prepareSMSResult = createObject("component", "fr.consotel.consoview.M111.MDMService").prepareSMS(typeCamapgne, sousTete, uuid, urlcle, _idcamapgne, idSousTete, statusSMS, idlogcamp, 'ROCOFFRE')>
			--->
			<!--- Envoi de SMS avec un tableau contenant les lignes mobiles destinataires --->
			<cfset var returnSend = urlwebserv.sendMessageToMultipleNumber([sousTeteWithIndicatif], contentSms, operator, uuid)>
			<cfif returnSend EQ FALSE>
				<cfset var errorMsg="for phone number '#sousTeteWithIndicatif#' : ">
				<cfset errorMsg=errorMsg & "[Operator: #operator#,Return-Status (SMS API): #returnSend#,Content-Length: #LEN(contentSms)#,UUID: #uuid#]">
				<cfthrow type="Custom" errorCode="#code#" message="MDMService SMS API Error #errorMsg#" detail="SMS Content : '#contentSms#'">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorCode="#code#" message="MDMService Unable to use SMS API. Cause: ApiSmsPublic.createCampagne(#idracine#) returns '#uuid#'">
		</cfif>
	</cffunction>
	
	<!--- La version de cette méthode ne supporte que les numéros de ligne en France --->
	<cffunction access="public" name="ligneToSmsFormat" returntype="String"
	hint="Retourne la ligne au format requis par l'API SMS : + suivi de INDICATIF_PAYS suivi de NUMERO_DE_LIGNE_SANS_LE_PREFIXE<br>
	Si la ligne commence par l'indicatif FRANCE alors elle est retournée telle quelle<br>
	Si la ligne commence par 0 alors ce chiffre est remplacé par l'indicatif FRANCE">
		<cfargument name="ligne" type="String" required="true" hint="Numéro de ligne">
		<cfargument name="userLocale" type="String" required="false" default="fr_FR" hint="Locale">
		<cfset var numero=TRIM(ARGUMENTS["ligne"])>
		<cfif LEN(numero) GT 3>
			<cfset var indicatif="+33">
			<cfset var tmpIndicatif=MID(numero,1,3)>
			<!--- TRUE si la ligne commence par l'indicatif FRANCE --->
			<cfif tmpIndicatif EQ indicatif>
				<cfreturn numero>
			<cfelseif MID(numero,1,1) EQ "0">
				<cfreturn indicatif & MID(numero,2,LEN(numero) - 1)>
			<cfelse>
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="MDMService Invalid phone number : '#numero#'">
			</cfif>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="sendEnrollmentInfosByMail" returntype="void" output="false" hint="Envoi par mail des infos d'enrollment.
	Si une ligne est affecté au device alors un SMS contenant les liens d'enrollment est envoyé. Une notification est envoyé en cas d'erreur SMS">
		<cfargument name="serial" type="String" required="true" hint="Numéro de série du device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du device">
		<cfargument name="imeiMyTEM" type="String" required="true" hint="IMEI du device non formaté">
		<cfargument name="mailTo" type="String" required="true" hint="Destinataire">
		<cfargument name="ligne" type="String" required="false" default="" hint="Ligne affectée au device ou une chaine vide sinon">
		<cfargument name="userLocale" type="String" required="false" default="fr_FR" hint="Locale">
		<cfset var qMailInfos="">
		<cfset var procedure="pkg_m00.get_appli_properties">
		<!--- Infos d'enrollment --->
		<cfset var mdm=mdmInfos()>
		<cfset var mdmServer=mdm.getIMDM()>
		<cfset var provider=UCASE(mdm.getProvider())>
		<cfset var mailProperties={type="html",from="monitoring@saaswedo.com",to=ARGUMENTS.mailTo,cc=mdm.getAdminEmail(),replyTo=""}>
	   <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#procedure#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_code_appli" value="#VAL(SESSION.CODEAPPLICATION)#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_key" null="true">
			<cfprocresult name="qMailInfos">
		</cfstoredproc>
		<cfset var qMailInfosSize=qMailInfos.recordcount>
		<cfloop index="i" from="1" to="#qMailInfosSize#">
			<cfset var key=qMailInfos["KEY"][i]>
			<cfset var value=qMailInfos["VALUE"][i]>
			<cfif key EQ "MAIL_EXP_N1">
				<cfset mailProperties.from=value>
			<cfelseif key EQ "MAIL_SERVER">
				<cfset mailProperties.server=value>
			<cfelseif key EQ "MAIL_PORT">
				<cfset mailProperties.port=VAL(value)>
			</cfif>
		</cfloop>
		<!--- Si une ligne est affectée au device : Envoi du SMS contenant les liens d'enrollment --->
		<cfset var smsSendSuccessfully=FALSE>
		<cfif ligne NEQ "">
			<cftry>
				<cfset sendEnrollmentInfosBySMS(ARGUMENTS.serial,ARGUMENTS.imei,ARGUMENTS.ligne,ARGUMENTS.userLocale)>
				<cfset smsSendSuccessfully=TRUE>
				<cfcatch type="Any">
					<!--- Notification de l'échec d'envoi du SMS --->
					<cfset var enrollMailProps=DUPLICATE(mailProperties)>
					<cfset enrollMailProps.subject="[MDM] Notification d'échec d'envoi d'un SMS (Enrollment)">
					<cfset enrollMailProps.from="monitoring@saaswedo.com">
					<cfset enrollMailProps.to="cedric.rapiera@saaswedo.com">
					<cfset structDelete(enrollMailProps,"cc")>
					<cfset structDelete(enrollMailProps,"replyTo")>
					<cfmail type="html" attributeCollection="#enrollMailProps#">
						<cfoutput>
							L'exception suivante a été rencontrée lors de l'envoi du SMS d'enrollment du terminal :<br>
							<ul>
								<li><b>Numéro de série :</b> #ARGUMENTS.serial#</li>
								<li><b>IMEI :</b> #replace(ARGUMENTS.imeiMyTEM," ","","ALL")#</li>
								<cfif ARGUMENTS.ligne NEQ "">
									<li><b>Ligne :</b> #ARGUMENTS.ligne#</li>
								</cfif>
							</ul><br>
							Utilisateur concerné : Mr/Mme #SESSION.USER.PRENOM# #SESSION.USER.NOM#<br>
							backOffice concerné : #CGI.SERVER_NAME#<br>
						</cfoutput>
						<cfdump var="#CFCATCH#" label="Exception">
					</cfmail>
				</cfcatch>
			</cftry>
		</cfif>
		<!--- Mail d'enrollment --->
		<cfset mailProperties.subject="[MDM] Procédure d'enrollement d'un terminal">
		<cfmail type="html" attributeCollection="#mailProperties#">
			<cfoutput>
				<center><p><i>Ce message est envoyé automatiquement, nous vous prions de ne pas répondre</i></p></center>
				Bonjour,<br/><br/>
				Nous avons enregistré une demande effectuée par Mr/Mme #SESSION.USER.PRENOM# #SESSION.USER.NOM# pour
				procéder à l'enrollment du terminal suivant :<br>
				<ul>
					<li><b>Numéro de série :</b> #ARGUMENTS.serial#</li>
					<li><b>IMEI :</b> #ARGUMENTS.imeiMyTEM#</li>
					<cfif ARGUMENTS.ligne NEQ "">
						<li><b>Ligne :</b> #ARGUMENTS.ligne#</li>
					</cfif>
				</ul>
				Votre adresse mail a été spécifiée à sa demande comme destinataire des informations correspondantes. 
				Pour procéder à l'enrollment, veuillez procédder aux étapes suivantes à partir du terminal concerné :<br/>
				<ul>
					<li>
						<h3>Si le système d'exploitation de votre terminal est <b>iOS</b> (APPLE) :</h3>
						<ol>
							<li>
								Accédez au lien suivant à partir du navigateur de votre terminal :<br>
								<cfset var iOSLink=mdmServer.getEnrollmentURL(mdm.IOS(),FALSE)>
								Lien : <a href="#iOSLink#" target="_blank">#iOSLink#</a>
							</li>
							<li>
								Vous serez invité à valider l'installation des profils et certificats mobiles
								correspondants à votre solution logicielle MDM (#provider#).
							</li>
							<li>
								Les instructions à suivre vous seront affichées pour chaque étape de la procédure.
								En particulier, vous trouverez plus bas dans ce mail les informations d'authentification MDM
								que vous devrez saisir afin de valider votre enrollment. 
							</li>
						</ol>
					</li>
					<li>
						<h3>Si le système d'exploitation de votre terminal est <b>ANDROID</b> (GOOGLE) :</h3>
						<ol>
							<li>
								Accédez au lien suivant à partir du navigateur de votre terminal :<br>
								<cfset var androidLink=mdmServer.getEnrollmentURL(mdm.ANDROID(),TRUE)>
								Lien : <a href="#androidLink#" target="_blank">#androidLink#</a>
							</li>
							<li>
								Vous serez invité à télécharger puis installer l'application mobile
								correspondant à votre solution logicielle MDM (#provider#).
							</li>
							<li>
								Exécutez l'application une fois l'installation terminée. Les instructions à suivre vous seront affichées pour
								chaque étape de la procédure. En particulier, vous trouverez plus bas dans ce mail les
								informations d'authentification MDM que vous devrez saisir afin de valider votre enrollment.
							</li>
						</ol>
					</li>
				</ul>
				<cfif ARGUMENTS.ligne NEQ "">
					Un SMS contenant les liens cités précédemment a également été envoyé à la ligne <b>#ARGUMENTS.ligne#</b>
					qui est actuellement affecté au terminal concerné.
					<cfif NOT smsSendSuccessfully>
						<br>Une erreur s'est produite durant l'envoi de ce SMS. 
						Nous avons notifié votre support technique afin que le problème soit résolu dans les plus brefs délais.
						<br>Cependant vous trouverez dans ce mail toutes les informations concernant la procédure d'enrollment du terminal.
					</cfif>
				</cfif>
				<br>
				<h3>Information d'authentification MDM (#provider#) :</h3>
				<ul>
					<cfset var enrollmentServerPort=":" & mdmServer.getEnrollmentServerPort()>
					<cfif (enrollmentServerPort EQ 80) OR (enrollmentServerPort EQ 443)>
						<cfset enrollmentServerPort="">
					</cfif>
					<li><b>Serveur MDM (MDM Server) :</b> #mdmServer.getEnrollmentServer()##enrollmentServerPort#</li>
					<li><b>Login utilisateur :</b> #mdm.getEnrollUsername()#</li>
					<li><b>Mot de passe (Password) :</b> #mdm.getEnrollPassword()#</li>
				</ul>
				<br>
				En Copie mail : Votre administrateur MDM
				<br/>
				<br/>
				<p>Cordialement,</p>
				Votre support client
			</cfoutput>
		</cfmail>
	</cffunction>
	
	<!--- =========== Accès aux infos MDM de session =========== --->

	<cffunction access="private" name="mdmInfos" returnType="fr.saaswedo.api.mdm.core.services.IMdmServerInfo" hint="API MDM : IMdmServerInfo">
		<cfset var mdmInfoKey="REMOTING_MDM_INFOS">
		<cfif structKeyExists(VARIABLES,mdmInfoKey)>
			<cfreturn VARIABLES[mdmInfoKey]>
		<cfelse>
			<cfset var code=GLOBALS().CONST("MDM.ERROR")>
			<cfthrow type="Custom" errorcode="#code#" message="IMdmServerInfo (API MDM) instance is not defined">
		</cfif>
	</cffunction>
	
	<!--- =========== Fonctions de compatibilité avec la version précédente M111 qui seront supprimés (REFACTORING) =========== --->
	
	<cffunction access="private" name="createMyTEM" returnType="fr.saaswedo.api.myTEM.mdm.MyTEM" hint="Référentiel MyTEM">
		<cfset var myTEM=new fr.saaswedo.api.myTEM.mdm.MyTEM()>
		<cfreturn myTEM.getInstance()>
	</cffunction>
	
	<!--- =========== Fonctions présentes dans la version d'origine du composant =========== --->
		
	<cffunction access="remote" name="simpleEnrollement" returntype="Boolean" output="false" hint="Enrollment d'un équipement de type 'Mobile'">
		<cfargument name="imei" 		type="String" 	required="true">
		<cfargument name="serial" 		type="String" 	required="true">
		<cfargument name="mdmimei" 		type="String" 	required="true">
		<cfargument name="idterminal"  	type="Numeric"	required="true">
		<cfargument name="soustete"		type="String" 	required="true">
		<cfargument name="idsoustete" 	type="Numeric" 	required="true">
		<cfargument name="idpool" 		type="Numeric" 	required="true">
		<cfset var serialValue=ARGUMENTS.serial>
		<cfset var imeiValue=ARGUMENTS.mdmimei>
		<cfset var mdm=mdmInfos()>
		<cfset var mdmServer=mdm.getIMDM()>
		<cfset var manageService=mdmServer.getIDeviceManagement()>
		<cfset var registerStatus=manageService.register(serialValue,imeiValue)>
		
		<cfset var qMailInfos="">
		<cfset var procedure="pkg_m00.get_appli_properties">
		<cfset var dataSource=(isDefined("SESSION") AND structKeyExists(SESSION,"OFFREDSN")) ? SESSION.OFFREDSN:"ROCOFFRE">
		<cfset var codeApp=(isDefined("SESSION") AND structKeyExists(SESSION,"CODEAPPLICATION")) ? VAL(SESSION.CODEAPPLICATION):1>
	   <cfstoredproc datasource="#dataSource#" procedure="#procedure#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_code_appli" value="#codeApp#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_key" null="true">
			<cfprocresult name="qMailInfos">
		</cfstoredproc>
		<cfmail type="html" from="monitoring@saaswedo.com" to="joan.cirencien@saaswedo.com" subject="">
			<cfdump var="#ARGUMENTS#" label="MDMService.sendEmail() : ARGUMENTS"><br>
			<cfdump var="#qMailInfos#" label="MDMService.sendEmail() : qMailInfos"><br>
		</cfmail>
		<!--- Envoi d'un SMS contenant le lien d'enrollment suivi d'un mail contenant les instructions d'enrollment correspondantes --->
		<cfreturn registerStatus>
		<!---
		<cfif TRIM(soustete) NEQ "">
			<!--- ICI PAS BESOIN DE LA RACINE CAR C'EST SUR LA PAGE DE DETECTION QUE L'ON UTILISE LES APPELS AU SERVEUR MDM --->
			<cfset var returnResult = 1>
			<cfset var enrollResult = setEnrollDevice('LIGNE', ARGUMENTS.serial, imei, soustete, idsoustete, idterminal, idpool, 'ROCOFFRE')>
			<cfif enrollResult.p_retour GT 0>
				<cftry>
					<!--- API MDM MyTEM --->
					<cfset var myTEM=createObject("component","fr.saaswedo.api.myTEM.mdm.MyTEM").getInstance()>
					<cfset var iDeviceInfo=myTEM.getIDeviceInfo()>
					<cfif iDeviceInfo.deviceExists(serial,imei)>
						<cfset var iDeviceManagement=myTEM.getIDeviceManagement()>
						<!--- Autorisation d'enrollment : Retourne toujours TRUE --->
						<cfset authorizeResult=iDeviceManagement.register(serial,imei)>
						<cflog type="information" text="Enrollment AUTORIZE [#serialNumber#,#imei#] : #authorizeResult#">
					</cfif>
					<!--- Envoi des instructions d'enrollment par SMS --->
					<cfset createObject("component", "fr.consotel.api.mdm.EnrollDevice").execute(serial, mdmimei, soustete, idsoustete, idpool, enrollResult.p_log)>
					<cfset var logResult = setLogsAction(imei, idterminal, 27)>
					<cfset var returnResult = 1>
					<cfcatch>
						<cflog type="error" text="Enrollment Exception : #CFCATCH.message# [#CFCATCH.detail#]"> 
						<!--- ERREUR WEBSERVICE OU ENVOI DE SMS --->
						<cfset var returnResult = -2>
						<cfrethrow/>
					</cfcatch>
				</cftry>
			<cfelse>
				<cfset var returnResult = -1>
				<cfthrow type="Custom" message="setEnrollDevice status -1" detail="LIGNE,#imei#,#ARGUMENTS.serial#,#soustete#,#idsoustete#,#idterminal#,#idpool#,ROCOFFRE">
			</cfif>
			<cfreturn returnResult>
		<cfelse>
			<cfthrow type="Custom" message="setEnrollDevice ligne non renseignee" detail="LIGNE,#imei#,#ARGUMENTS.serial#,#soustete#,#idsoustete#,#idterminal#,#idpool#,ROCOFFRE">
		</cfif>
		--->
	</cffunction>
	
	<!--- TODO : Fournir le paramètre serial à la requete PKG_MDM.enroll_ligne qui se trouve dans MDMService.setEnrollDevice() --->
     <cffunction name="setEnrollDevice" access="remote" returntype="Any" output="false" hint="Enregistre les infos d'enrollement">
             <cfargument name="type"                 type="String"   required="true">
			 <cfargument name="serialNumber"                 type="String"   required="true">
             <cfargument name="imei"                 type="String"   required="true">
             <cfargument name="soustete"             type="String"   required="true">
             <cfargument name="idsoustete"   type="Numeric"  required="true">
             <cfargument name="idterminal"   type="Numeric"  required="true">
             <cfargument name="idpool"               type="Numeric"  required="true">
             <cfargument name="dtasource"    type="String"   required="true"><!--- Nécessaire car se sera peut etre utilisé dans et hors session --->
                     <cfset var idracine             = session.perimetre.id_groupe>
                     <cfset var idgestionnaire       = session.user.clientaccessid>
                     <cfset var codeApp=isDefined("SESSION") ? (structKeyExists(SESSION,"CODEAPPLICATION") ? VAL(SESSION.CODEAPPLICATION):1):1>
                     <cfstoredproc datasource="#dtasource#" procedure="PKG_MDM.enroll_ligne_v2">
						<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_type"                            value="#type#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine"                        value="#idracine#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idsous_tete"             value="#idsoustete#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_sous_tete"                       value="#soustete#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid_create"  value="#idgestionnaire#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_imei"                            value="#imei#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idterminal"              value="#idterminal#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idpool"                          value="#idpool#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.serialNumber#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#codeApp#">
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_idmdm_log_campagne">             
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
                     </cfstoredproc>
                                     
                     <cfset var retour                       = structNew()>
                     <cfset var retour.p_retour      = p_retour>
                     <cfset var retour.p_log         = p_idmdm_log_campagne>
                                             
             <cfreturn retour>
     </cffunction>
     
     <cffunction name="getEnrollDevice" access="remote" returntype="Struct" output="false" hint="Récupère les infos d'enrollement">
             <cfargument name="uuid"                 type="String"   required="true">
             <cfargument name="cle"                  type="String"   required="true">
             <cfargument name="dtasource"    type="String"   required="true"><!--- Nécessaire car se sera peut etre utilisé dans et hors session --->
                     
                     <cfstoredproc datasource="#dtasource#" procedure="PKG_MDM.get_infos_uuid_cle_v2">
                             <cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_uuid"            value="#uuid#">
                             <cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_cle"             value="#cle#">
                             <cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour"> 
                             <cfprocresult name="p_infos">
                     </cfstoredproc>
                     
                     <cfset var infos                = structNew()>
                     <cfset var infos.INFOS  = p_infos>
                     <cfset var infos.ETAT   = p_retour>
     
             <cfreturn infos>
     </cffunction>
		
	<cffunction name="getExpediteur" access="private" returntype="String" output="false" hint="retourne l'expéditeur de Mail">
		<cftry>
		     <cfstoredproc datasource="ROCOFFRE" procedure="pkg_m00.get_appli_properties">
		    	<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_code_appli"      value="#VAL(SESSION.CODEAPPLICATION)#">
		        <cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_key"             null="true">
		        <cfprocresult name="requete_resultat">
		    </cfstoredproc>
		                     
			<cfset myQyery = #requete_resultat#>
			<cfloop query="myQyery" >
				<cfif #myQyery.KEY# EQ "MAIL_EXP_N1">
					<cfset exp=#myQyery.VALUE#>
					<cfbreak>
				</cfif>
			</cfloop>
			
			<cfcatch type="Any">
				<cflog type="error" text="[MDMService] getExpediteur error : #CFCATCH.message# [#CFCATCH.detail#]">
				<cfrethrow>
			</cfcatch>
		</cftry>
		
		<cfreturn #exp#>
	</cffunction>
	
	<cffunction name="getServerInfos" access="remote" returntype="struct" output="false" >
		<!---<cfargument name="racine" type="Struct" required="true">--->
		<cftry>
				<cfset infoSrv = createObject("component","fr.consotel.api.mdm.CodeFunction")>
			<cfcatch type="Any">
				<cflog type="error" text="[MDMService] Get server infos error : #CFCATCH.message# [#CFCATCH.detail#]">
				<cfrethrow>
			</cfcatch>
		</cftry>

		<cfreturn #infoSrv#>
	</cffunction>
	
	<!--- Enrolle plusieurs équipements --->
	<cffunction name="multiEnrollement" access="remote" returntype="Any" output="false" hint="Enrolle plusieurs équipements">
		<cfargument name="lignes"  	type="Any"		required="true"/>
		<cfargument name="idpool" 	type="Numeric" 	required="true">
			<cfset var returnLignes=arrayNew(1)>
 			<cfloop index="idx" from="1" to="#ArrayLen(lignes)#">
				<cfset var enrollResult=simpleEnrollement(
					lignes[idx]['IMEI'], lignes[idx]['ZDM_SERIAL_NUMBER'],  lignes[idx]['ZDM_IMEI'], lignes[idx]['IDTERMINAL'],
					lignes[idx]['LIGNE'], lignes[idx]['IDSOUS_TETE'], idpool
				)>
				<cfset arrayAppend(returnLignes, enrollResult)> 
			</cfloop>
		<cfreturn returnLignes>
	</cffunction>
	
	<!--- Récupère les locations faite : N'est plus utilisée dans le FrontOffice
	<cffunction name="getLocations" access="remote" returntype="Any" output="false" hint="Enroll un équipement">
		<cfargument name="imei" 		type="String" 	required="true">
		<cfargument name="serial" 		type="String" 	required="true">
		<cfargument name="mdmimei" 		type="String" 	required="true">
		<cfargument name="idterminal"  	type="Numeric"	required="true">
		<cfargument name="soustete"		type="String" 	required="true">
		<cfargument name="idsoustete" 	type="Numeric" 	required="true">
		<cfargument name="deviceId" 	type="Numeric" 	required="true">
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_mdm.get_localisation">
				<cfprocparam type="in" cfsqltype="CF_SQL_iNTEGER"  value="#deviceId#">
				<cfprocresult name="p_retour" 	resultset="1">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	 --->
		
	<!--- Permet de créer un user zenprise --->
	<cffunction name="createUserMDM" access="remote" returntype="Any" output="false" hint="Permet de créer un user zenprise">
		<cfargument name="group"	 	type="String" required="true" hint="Nom du groupe pour l'utilisateur">
		<cfargument name="login" 		type="String" required="true" hint="Login pour l'utilisateur">
		<cfargument name="password" 	type="String" required="true" hint="Mot de passe pour l'utilisateur">
		<cfargument name="role" 		type="String" required="true" hint="Rôle pour l'utilisateur (i.e : ADMIN, SUPPORT, USER, GUEST)">
			
			<cfset var idracine  = session.perimetre.id_groupe>
			<cfset var mdmResult = -1>
			
			<cftry>
			
				<cfset var mdmResult = createObject("component", "fr.consotel.api.mdm.CreateUser").execute(group, login, password, role, idracine)>
			
				<!--- <cfset var logResult = setLogsAction(imei, idterminal, 34)> --->
				
				<cfset var mdmResult = 1>
				
			<cfcatch>
				
				<cflog type="error" text="[MDMService] Create User error : #CFCATCH.message# [#CFCATCH.detail#]">
				<cfrethrow>
				
			</cfcatch>
			</cftry>
			
		<cfreturn mdmResult>
	</cffunction>
	
	<!--- Insertion des logs d'actions --->
	<cffunction name="setLogsAction" access="remote" returntype="Any" output="false" hint="Insertion des logs d'actions">
		<cfargument name="imei" 		type="String" 	required="true">
		<cfargument name="idTerminal"  	type="Numeric"	required="true">
		<cfargument name="idAction" 	type="Numeric" 	required="true">
		
			<cfset var uuid 	= createUUID()>
			<cfset var today 	= now()>
			
			<cfset var logger = createObject('component','fr.consotel.consoview.M111.Logger')>
			
			<cftry>
				
				<cfset logResult = logger.logAction(idAction, 1, imei, "", idTerminal, -1, -1, -1, uuid, today)>
				<cfset logResult = 1>
				
			<cfcatch type="any">
				
				<cfset ERROR = logger.setError(cfcatch.detail, cfcatch.errorcode, cfcatch.message, cfcatch.type)>
				
				<cfset result = logger.sendMailAction(idAction, 1, imei, "" ,idTerminal, -1, -1, -1, uuid, today ,ERROR)>
				
				<cfset logResult = -1>
				<cflog type="error" text="[MDMService] Set log action : #CFCATCH.message# [#CFCATCH.detail#]">
				<cfrethrow>
			</cfcatch>
			</cftry>
		
		<cfreturn logResult>
	</cffunction>
	
	<!--- Met à jour les infos concernant l'enrollment avant l'envoi du sms --->
	
	<cffunction name="prepareSMS" access="remote" returntype="Numeric" output="false" hint="Met à jour les infos concernant l'enrollment avant l'envoi du sms">
		<cfargument name="type" 		type="String" 	required="true">
		<cfargument name="soustete"		type="String"	required="true">
		<cfargument name="uuid"	 		type="String" 	required="true">
		<cfargument name="cle"			type="String" 	required="true">
		<cfargument name="idcampagne"	type="Numeric"	required="true">
		<cfargument name="idsoustete"	type="Numeric"	required="true">
		<cfargument name="idstatut"		type="Numeric"	required="true">
		<cfargument name="idlogcamp"	type="Numeric"	required="true">
		<cfargument name="dtasource"	type="String" 	required="true"><!--- Nécessaire car se sera peut etre utilisé dans et hors session --->

			<cfstoredproc datasource="#dtasource#" procedure="pkg_mdm.prepare_sms">
				<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR" variable="p_type" 				value="#type#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idmdm_campagne" 		value="#idcampagne#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idmdm_log_campagne" 	value="#idlogcamp#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idsous_tete"			value="#idsoustete#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_VARCHAR" variable="p_sous_tete"			value="#soustete#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_statut"  			value="#idstatut#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_VARCHAR" variable="p_uuid"  				value="#uuid#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_VARCHAR" variable="p_cle"  				value="#cle#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>
			<!--- 		
			<cfmail server="mail.consotel.fr" port="25" from="prepareSMS@consotel.fr" to="nicolas.renel@consotel.fr" 
						failto="nicolas.renel@consotel.fr" subject="[prepareSMS]Dump Infos" type="text/html">
						 							 							
				<cfoutput>
				
					http://sun-dev-nicolas.consotel.fr/detection.cfm?uuid=#uuid#&cle=#cle#<br><br>
				
					type	: #type#<br><br>
					uuid	: #uuid#<br><br>
					clef	: #cle#<br><br>
					statut	: #idstatut#<br><br>
					idcamp	: #idcampagne#<br><br>
					idlog	: #idlogcamp#<br><br>
					sstt	: #soustete#<br><br>
					rslt	: #p_retour#<br><br>
				
				</cfoutput>
	
			</cfmail>
			 --->			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- Met à jour les infos concernant l'enrollment lors de l'envoi du sms --->
	
	<cffunction name="envoiSMS" access="remote" returntype="Numeric" output="false" hint="Met à jour les infos concernant l'enrollment lors de l'envoi du sms">
		<cfargument name="type" 		type="String" 	required="true">
		<cfargument name="uuid"	 		type="String" 	required="true">
		<cfargument name="cle"			type="String" 	required="true">
		<cfargument name="idstatut"		type="Numeric"	required="true">
		<cfargument name="idlogcamp"	type="Numeric"	required="true">
		<cfargument name="dtasource"	type="String" 	required="true"><!--- Nécessaire car se sera peut etre utilisé dans et hors session --->
		<cfargument name="idcampagne"	type="Numeric"	required="false">

			<cfset var _idcamapgne = 0>

			<cfif structKeyExists(arguments, "idcampagne")>
			
				<cfset _idcamapgne = idcampagne>

			</cfif>

			<cfstoredproc datasource="#dtasource#" procedure="PKG_MDM.envoi_sms">
				<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR" variable="p_type" 				value="#type#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idmdm_campagne" 		value="#_idcamapgne#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idmdm_log_campagne" 	value="#idlogcamp#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_VARCHAR" variable="p_uuid"  				value="#uuid#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_VARCHAR" variable="p_cle"  				value="#cle#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_statut"  			value="#idstatut#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>
			<!--- 	
			<cfmail server="mail.consotel.fr" port="25" from="envoiSMS@consotel.fr" to="nicolas.renel@consotel.fr" 
						failto="nicolas.renel@consotel.fr" subject="[envoiSMS]Dump Infos" type="text/html">
						 							 							
				<cfoutput>
				
					http://sun-dev-nicolas.consotel.fr/detection.cfm?uuid=#uuid#&cle=#cle#<br><br>
				
					type	: #type#<br><br>
					uuid	: #uuid#<br><br>
					clef	: #cle#<br><br>
					statut	: #idstatut#<br><br>
					idcamp	: #_idcamapgne#<br><br>
					idlog	: #idlogcamp#<br><br>
					rslt	: #p_retour#<br><br>
				
				</cfoutput>
	
			</cfmail>
			 --->			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- Met à jour les infos concernant l'enrollment lors de l'acces à la page de détecion --->
	
	<cffunction name="detectionSMS" access="remote" returntype="Numeric" output="false" hint=" Met à jour les infos concernant l'enrollment lors de l'acces à la page de détecion">
		<cfargument name="type" 		type="String" 	required="true">
		<cfargument name="ipadress"	 	type="String" 	required="true">
		<cfargument name="tyoeos"		type="String" 	required="true">
		<cfargument name="uuid"	 		type="String" 	required="true">
		<cfargument name="cle"			type="String" 	required="true">
		<cfargument name="idstatut"		type="Numeric"	required="true">
		<cfargument name="idlogcamp"	type="Numeric"	required="true">
		<cfargument name="dtasource"	type="String" 	required="true"><!--- Nécessaire car se sera peut etre utilisé dans et hors session --->
		<cfargument name="idcampagne"	type="Numeric"	required="false">

			<cfset var _idcamapgne = 0>

			<cfif structKeyExists(arguments, "idcampagne")>
			
				<cfset _idcamapgne = idcampagne>

			</cfif>
			 
			<cfstoredproc datasource="#dtasource#" procedure="PKG_MDM.detection_sms">
				<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR" variable="p_type" 				value="#type#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idmdm_campagne" 		value="#_idcamapgne#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idmdm_log_campagne" 	value="#idlogcamp#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_VARCHAR" variable="p_uuid"  				value="#uuid#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_VARCHAR" variable="p_cle"  				value="#cle#">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_statut"  			value="#idstatut#">
				<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR" variable="p_ip_adresse" 			value="#ipadress#">
				<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR" variable="p_system_platform" 	value="#tyoeos#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>
			<!--- 		
			<cfmail server="mail.consotel.fr" port="25" from="detectionSMS@consotel.fr" to="nicolas.renel@consotel.fr" 
						failto="nicolas.renel@consotel.fr" subject="[detectionSMS]Dump Infos" type="text/html">
						 							 							
				<cfoutput>
				
					http://sun-dev-nicolas.consotel.fr/detection.cfm?uuid=#uuid#&cle=#cle#<br><br>
				
					type	: #type#<br><br>
					uuid	: #uuid#<br><br>
					clef	: #cle#<br><br>
					statut	: #idstatut#<br><br>
					idcamp	: #_idcamapgne#<br><br>
					idlog	: #idlogcamp#<br><br>
					rslt	: #p_retour#<br><br>
				
				</cfoutput>
	
			</cfmail>
			 --->	
		<cfreturn p_retour>
	</cffunction>
</cfcomponent>