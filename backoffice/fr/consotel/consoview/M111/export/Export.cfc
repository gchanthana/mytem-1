<cfcomponent displayname="Export">

	<cfset VARIABLES["reportServiceClass"]="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService">
	<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
<!--- ***************************** CODE A MODIFIER ******************************* --->		
	<!--- Le chemin du rapport sous BIP --->
	<cfset VARIABLES["XDO"]="/consoview/M331/EXPORT-CDR-ALL/EXPORT-CDR-ALL.xdo">
	<!--- Le code rapport --->
	<cfset VARIABLES["CODE_RAPPORT"]="EXPORT-CDR-ALL">
	<!--- TEMPLATE BI --->
	<cfset VARIABLES["TEMPLATE"]="Default">
	<!--- OUTPUT --->
	<cfset VARIABLES["OUTPUT"]="EXPORT-CDR-ALL">
	<!--- Le Module --->
	<cfset VARIABLES["MODULE"]="M311">
<!--- ************************************************************ --->
			
	<cfset reportService=createObject("component",VARIABLES["reportServiceClass"])>
	<cfset ApiE0 = CreateObject("component","fr.consotel.consoview.api.E0.ApiE0")>
	<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>


<cffunction name="getExportDetailAppel">
	
	<cfargument name="idsous_tete" type="numeric" required="true">
	<cfargument name="numero" type="string" required="true">
	<cfargument name="idinventaire_periode" type="numeric" required="true">
	
	<cfargument name="ORDER_BY_TEXTE_COLONNE" type="string" required="true">
	<cfargument name="ORDER_BY_TEXTE" type="string" required="true">
	
	<cfargument name="SEARCH_TEXT_COLONNE" type="string" required="true">
    <cfargument name="SEARCH_TEXT" type="string" required="true">
	
    <cfargument name="OFFSET" type="numeric" required="true">
    <cfargument name="LIMIT" type="numeric" required="true">
	
	<cfset idracine_master = session.perimetre.idracine_master>
	<cfset idracine = session.perimetre.id_groupe>
	
	<cfset ext="CSV">
	<cfset format="CSV">
	<cfset setStatus=TRUE>
	
	<cfset VARIABLES["OUTPUT"]=VARIABLES["OUTPUT"] & "-" & numero>
	
	<cfset initStatus=reportService.init(idracine,VARIABLES["XDO"],"ConsoView",VARIABLES["MODULE"],VARIABLES["CODE_RAPPORT"])>
	
		
	<!--- PARAMETRES RECUPERES EN SESSION --->
		<!--- p_idracine_master --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=idracine_master>
			<cfset setStatus=setStatus AND reportService.setParameter("p_idracine_master",tmpParamValues)>
		<!--- g_idracine --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=idracine>
			<cfset setStatus=setStatus AND reportService.setParameter("g_idracine",tmpParamValues)>
	<!--- PARAMETRES --->
		<!--- p_idinventaire_periode --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=idinventaire_periode>
			<cfset setStatus=setStatus AND reportService.setParameter("p_idinventaire_periode",tmpParamValues)>
		<!--- p_idsous_tete --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=idsous_tete>
			<cfset setStatus=setStatus AND reportService.setParameter("p_idsous_tete",tmpParamValues)>
		<!--- p_order_by --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=ORDER_BY_TEXTE_COLONNE>
			<cfset setStatus=setStatus AND reportService.setParameter("p_order_by",tmpParamValues)>
		<!--- p_asc_desc --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=ORDER_BY_TEXTE>
			<cfset setStatus=setStatus AND reportService.setParameter("p_asc_desc",tmpParamValues)>
		<!--- p_champs_filtre --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SEARCH_TEXT_COLONNE>
			<cfset setStatus=setStatus AND reportService.setParameter("p_champs_filtre",tmpParamValues)>
		<!--- p_valeur_filtre --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SEARCH_TEXT>
			<cfset setStatus=setStatus AND reportService.setParameter("p_valeur_filtre",tmpParamValues)>
		<!--- p_index_debut --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=OFFSET>
			<cfset setStatus=setStatus AND reportService.setParameter("p_index_debut",tmpParamValues)>
		<!--- p_number_of_records --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=LIMIT>
			<cfset setStatus=setStatus AND reportService.setParameter("p_number_of_records",tmpParamValues)>
					
		<cfset pretour = -1>
		<cfif setStatus EQ TRUE>
			<cfset FILENAME=VARIABLES["OUTPUT"]>
			<cfset outputStatus = FALSE>
			<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],format,ext,FILENAME,VARIABLES["CODE_RAPPORT"])>
			<cfif outputStatus EQ TRUE>
				<cfset reportStatus = FALSE>
				<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
				<cfset pretour = reportStatus['JOBID']>
				<cfif reportStatus['JOBID'] neq -1>		
					<cfreturn reportStatus["JOBID"]>
				</cfif>
			</cfif>
		</cfif>		
	
	<cfreturn pretour >
	
</cffunction>

</cfcomponent>