<cfcomponent name="GestionPool" alias="fr.consotel.consoview.inventaire.gestionpool.GestionPool">
	
	<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
	<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
	<cfset ALT_DSN = 'ROCOFFRE-DB1'>
	
	<!--- -------------------------------------------------------------- --->
	<!---            AFFICHAGE LISTES D'ELEMENTS                         --->
	<!--- -------------------------------------------------------------- --->
	
	<!--- LISTE DES ELEMENTS RATTACHES A UN COLLABORATEUR DANS LE POOL --->
	<cffunction name="getAttachedElementEmploye" returntype="query" access="remote">
		<cfargument name="idEmploye" required="true" type="numeric" />
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="etat" required="true" type="numeric" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.getAttachedElementEmploye_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEmploye#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#etat#" null="false">
			<cfprocresult name="qGetAttachedElementEmploye">
		</cfstoredproc>
		
		<cfreturn qGetAttachedElementEmploye>
	</cffunction>
	
	<!--- LISTE DES ELEMENTS RATTACHES A UN TERMINAL DANS LE POOL --->
	<cffunction name="getAttachedElementTerminal" returntype="query" access="remote">
		<cfargument name="idTerm" required="true" type="numeric" />
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="etat" required="true" type="numeric" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.getAttachedElementTerminal">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTerm#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#etat#" null="false">
			<cfprocresult name="qGetAttachedElementTerminal">
		</cfstoredproc>
		
		<cfreturn qGetAttachedElementTerminal>
	</cffunction>
	
	<!--- LISTE DES ELEMENTS RATTACHES A UNE LIGNE DANS LE POOL --->
	<cffunction name="getAttachedElementLine" returntype="query" access="remote">
		<cfargument name="idSoustete" required="true" type="numeric" />
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="etat" required="true" type="numeric" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.getAttachedElementLine">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSoustete#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#etat#" null="false">
			<cfprocresult name="qGetAttachedElementLine">
		</cfstoredproc>
		
		<cfreturn qGetAttachedElementLine>
	</cffunction>
	
	<!--- LISTE DES ELEMENTS RATTACHES A UNE SIM DANS LE POOL --->
	<cffunction name="getAttachedElementSim" returntype="query" access="remote">
		<cfargument name="idSim" required="true" type="numeric" />
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="etat" required="true" type="numeric" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.getAttachedElementSim">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSim#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#etat#" null="false">
			<cfprocresult name="qGetAttachedElementSim">
		</cfstoredproc>
		
		<cfreturn qGetAttachedElementSim>
	</cffunction>

	<!--- LISTE DES ELEMENTS RATTACHES GENERIQUE DANS LE POOL --->
	<cffunction name="getAttachedElement" returntype="query" access="remote">
		<cfargument name="myXML" required="true" type="String" />
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="etat" required="true" type="numeric" />
		
		<cfstoredproc datasource="#ALT_DSN#" procedure="pkg_M111.getAttachedElement_v3">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#myXML#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#etat#" null="false">
			<cfprocresult name="qGetAttachedElement">
		</cfstoredproc>
		
		<cfreturn qGetAttachedElement>
	</cffunction>	
	
	<!--- LISTE DES COLLABORATEURS HORS D'UN POOL --->
	<cffunction name="getAttachedElementOutEmploye" returntype="query" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="etat" required="true" type="numeric" />
		<cfargument name="displayAll" required="true" type="numeric" />
		<cfargument name="searchColumn" required="true" type="numeric" />
		<cfargument name="search" required="true" type="string" />
		
		<cfset maxRow = 100>
		<cfif displayAll>
			<cfset maxRow = -1>
		</cfif>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.getAttachedElemLstEmploye_v3">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#etat#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#searchColumn#" null="false">
			<cfprocresult name="qGetAttachedElementOutEmploye" maxrows="#maxRow#">
		</cfstoredproc>
		
		<cfreturn qGetAttachedElementOutEmploye>
	</cffunction>
	
	<!--- LISTE GÉNÉRIQUE DES ÉLÉMENTS HORS D'UN POOL --->
	<cffunction name="getAttachedElementOutGenerique" returntype="query" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="etat" required="true" type="numeric" />
		<cfargument name="displayAll" required="true" type="numeric" />
		<cfargument name="searchColumn" required="true" type="numeric" />
		<cfargument name="search" required="true" type="string" />
		<cfargument name="segment" required="true" type="numeric" default="0"/>
		
		<cfset maxRow = 100>
		<cfif displayAll>
			<cfset maxRow = -1>
		</cfif>
		
		<cfswitch expression="#searchColumn#">
			<cfcase value="1">
				<cfset myProcedure = "pkg_M111B.getAttachedElemLstEmploye"/>
			</cfcase>
			<cfcase value="2">
				<cfset myProcedure = "pkg_M111B.getAttachedElemLstEmploye"/>
			</cfcase>			
			<cfcase value="3">
				<cfset myProcedure = "pkg_M111B.getAttachedElemLstTerm"/>
			</cfcase>
			<cfcase value="4">
				<cfset myProcedure = "pkg_M111B.getAttachedElemLstTerm_v2"/>
			</cfcase>			
			<cfcase value="5">
				<cfset myProcedure = "pkg_M111B.getAttachedElemListLine"/>
			</cfcase>
			<cfcase value="6">
				<cfset myProcedure = "pkg_M111B.getAttachedElemLstSim"/>
			</cfcase>
			<cfcase value="7">
				<cfset myProcedure = "pkg_M111B.getAttachedElemLstTerm_v2"/>
			</cfcase>
			<cfcase value="8">
				<cfset myProcedure = "pkg_M111B.getAttachedElemListLine"/>
			</cfcase>
			<cfcase value="9">
				<cfset myProcedure = "pkg_M111B.getAttachedElemListLine"/>
			</cfcase>			
			<cfcase value="12">
				<cfset myProcedure = "pkg_M111B.getAttachedElemLstTerm_v2"/>
			</cfcase>
		</cfswitch>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="#myProcedure#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" 			null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#etat#" 			null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" 		null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#" 			null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#searchColumn#" 	null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment#" 		null="false">
			<cfprocresult name="qGetAttachedElementOutEmploye" maxrows="#maxRow#">
		</cfstoredproc>
		
		<cfreturn qGetAttachedElementOutEmploye>
	</cffunction>
	
	<!--- LISTE DES TERMINAUX HORS D'UN POOL --->
	<cffunction name="getAttachedElementOutTerminal" returntype="query" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="etat" required="true" type="numeric" />
		<cfargument name="displayAll" required="true" type="numeric" />
		<cfargument name="searchColumn" required="true" type="numeric" />
		<cfargument name="search" required="true" type="string" />
		
		<cfset maxRow = 100>
		<cfif displayAll>
			<cfset maxRow = -1>
		</cfif>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.getAttachedElemLstTerm_v6">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#etat#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#searchColumn#" null="false">
			<cfprocresult name="qGetAttachedElementOutTerminal" maxrows="#maxRow#">
		</cfstoredproc>
		
		<cfreturn qGetAttachedElementOutTerminal>
	</cffunction>
	
	<!--- LISTE DES LIGNES HORS D'UN POOL --->
	<cffunction name="getAttachedElementOutLine" returntype="query" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="etat" required="true" type="numeric" />
		<cfargument name="displayAll" required="true" type="numeric" />
		<cfargument name="search" required="true" type="string" />
		<cfargument name="searchColumn" required="true" type="numeric" />
		
		<cfset maxRow = 100>
		<cfif displayAll>
			<cfset maxRow = -1>
		</cfif>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.getAttachedElemListLine_v4">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#etat#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#searchColumn#" null="false">
			<cfprocresult name="qGetAttachedElementOutLine" maxrows="#maxRow#">
		</cfstoredproc>
		
		<cfreturn qGetAttachedElementOutLine>
	</cffunction>
	
	<!--- LISTE DES SIM HORS D'UN POOL --->
	<cffunction name="getAttachedElementOutSim" returntype="query" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="etat" required="true" type="numeric" />
		<cfargument name="displayAll" required="true" type="numeric" />
		<cfargument name="searchColumn" required="true" type="numeric" />
		<cfargument name="search" required="true" type="string" />
		
		<cfset maxRow = 100>
		<cfif displayAll>
			<cfset maxRow = -1>
		</cfif>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.getAttachedElemLstSim_v4">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#etat#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#searchColumn#" null="false">
			<cfprocresult name="qGetAttachedElementOutSim" maxrows="#maxRow#">
		</cfstoredproc>
		
		<cfreturn qGetAttachedElementOutSim>
	</cffunction>
	
	
	
	<!--- -------------------------------------------------------------------------- --->
	<!---            EXCLUSION / INCLUSION LISTES D'ELEMENTS                         --->
	<!--- -------------------------------------------------------------------------- --->
	
	<!--- EXCLUSION D'UNE LISTE DE COLLABORATEURS D'UN POOL --->
	<cffunction name="removeListeCollabFromPool" returntype="numeric" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="tabIdCollab" required="true" type="array" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.removeLstEmployesFromPool_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" value="#convertTabToList(tabIdCollab)#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- EXCLUSION D'UNE LISTE D'EQUIPEMENTS (TERMINAUX / SIM) D'UN POOL --->
	<cffunction name="removeListeEqptFromPool" returntype="numeric" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="tabIdEqpt" required="true" type="array" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.removeEquipementsFromPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" value="#convertTabToList(tabIdEqpt)#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- EXCLUSION D'UNE LISTE DE LIGNES D'UN POOL --->
	<cffunction name="removeListeLigneFromPool" returntype="numeric" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="tabIdSoustete" required="true" type="array" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.removeListeLignesFromPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" value="#convertTabToList(tabIdSoustete)#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>

	<!--- EXCLUSION D'UNE LISTE GENERIQUE D'UN POOL --->
	<cffunction name="removeListeFromPool" returntype="numeric" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="myXML" required="true" type="String" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111B.removeListeFromPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#myXML#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>	

	<!--- TRANSFERT D'UNE LISTE GENERIQUE D'UN POOL --->
	<cffunction name="moveListeFromPool" returntype="numeric" access="remote">
		<cfargument name="myXML" required="true" type="String" />
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="idPoolDest" required="true" type="numeric" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.moveListeFromPool_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#myXML#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPoolDest#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>	
	
	<!--- INCLUSION D'UNE LISTE DE COLLABORATEURS DANS UN POOL --->
	<cffunction name="addListeCollabToPool" returntype="numeric" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="tabIdCollab" required="true" type="array" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.addEmployesToPool_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" value="#convertTabToList(tabIdCollab)#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- INCLUSION D'UNE LISTE D'EQUIPEMENTS (TERMINAUX / SIM) DANS UN POOL --->
	<cffunction name="addListeEqptToPool" returntype="numeric" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="tabIdEqpt" required="true" type="array" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.addEquipementsToPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" value="#convertTabToList(tabIdEqpt)#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- INCLUSION D'UNE LISTE DE LIGNES DANS UN POOL --->
	<cffunction name="addListeLigneToPool" returntype="numeric" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="tabIdSoustete" required="true" type="array" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.addLignesToPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" value="#convertTabToList(tabIdSoustete)#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- INCLUSION D'UNE LISTE GENERIQUE DANS UN POOL --->
	<cffunction name="addListeToPool" returntype="numeric" access="remote">
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="myXML" required="true" type="String" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111B.addListeToPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#myXML#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- -------------------------------------------------------------------------- --->
	<!---            					METHODES PRIVEES	                         --->
	<!--- -------------------------------------------------------------------------- --->
	
	<!--- CONVERSION D'UN TABLEAU EN LISTE --->
	<cffunction name="convertTabToList" access="private" returntype="Any">
		<cfargument name="tab" type="Array">
		<cfset list = ArrayToList(tab, ',')>
		<cfreturn list>
	</cffunction>
	
</cfcomponent>
