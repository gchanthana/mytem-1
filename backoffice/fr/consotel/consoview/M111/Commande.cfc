﻿<cfcomponent>

	<cfset init()>
	
	<cffunction name="init"hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
	</cffunction>
	
	<!--- 
		genere un numero de commande unique
	 --->
	<cffunction name="genererNumeroDeCommande" access="remote" returntype="string" output="false" hint="genere un numero de commande unique">
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m11.GETCOMMANDENUMBER" blockfactor="1" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idRacine#">									
			<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR" variable="p_retour">		
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<!--- 
		saisir les informations d'une commande de mobile 
	--->
	<cffunction name="enregistrerCommande" access="remote" returntype="numeric" output="false" hint="saisir les informations d'une commande de mobile">		
		<cfargument name="idPool"			 	required="true" type="numeric" />
		<cfargument name="idOperateur" 			required="true" type="numeric" />
		<cfargument name="idRevendeur" 			required="true" type="numeric" />
		<cfargument name="idSite"			 	required="true" type="numeric" />
		<cfargument name="libelle"		 		required="true" type="string" />
		<cfargument name="refClient" 			required="true" type="string" />
		<cfargument name="refRevendeur" 		required="true" type="string" />
		<cfargument name="dateOperation"		required="true" type="string" />
		<cfargument name="dateEffetPrevue"		required="true" type="string" />
		<cfargument name="commentaires" 		required="true" type="string" />
		<cfargument name="montant" 				required="true" type="numeric" />
		<cfargument name="numeroCommande" 		required="true" type="string" />
		<cfargument name="idEquipement" 		required="true" type="numeric" />
		<cfargument name="dateDebutContrat" 	required="true" type="string" />
		<cfargument name="dureeContrat" 		required="true" type="numeric" />
		<cfargument name="refLivraison" 		required="true" type="string" />
		<cfargument name="dateLivraison"		required="true" type="string" />
		<cfargument name="segmentFixe"			required="true" type="numeric" />
		<cfargument name="segmentMobile"		required="true" type="numeric" />
		<cfargument name="idtypeContrat"		required="false" type="numeric" default="3" /> <!--- contrat de type garantie --->

		<cfset chaineDebug="#idRacine#,#idUser#,#idPool#,#idOperateur#,
							#idRevendeur#,#idSite#,#libelle#,#refClient#,
							#refRevendeur#,#dateOperation#,#dateEffetPrevue#,
							#commentaires#,#montant#,#numeroCommande#,#idEquipement#,
							#dateDebutContrat#,#dureeContrat#,#refLivraison#,
							#dateLivraison#,#segmentFixe#,#segmentMobile#,#idtypeContrat#">
		
		<!--- <cfabort showError="#chaineDebug#"> --->

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m11.enregistrerCdEquExist">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOperateur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#refClient#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#refRevendeur#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateOperation#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateEffetPrevue#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   value="#montant#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numeroCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateDebutContrat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#dureeContrat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#refLivraison#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateLivraison#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segmentFixe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segmentMobile#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idtypeContrat#">
			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_return" >
		</cfstoredproc>
				
		<cfreturn p_return>
	</cffunction>

</cfcomponent>