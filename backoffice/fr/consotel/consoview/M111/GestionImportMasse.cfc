<cfcomponent name="GestionImportMasse" alias="fr.consotel.consoview.M111.GestionImportMasse">
	
	
	<!--- 
		
	--->
	<cffunction name="ValidatesaveMultipleEquip" returntype="Any" access="remote">
		<cfargument name="LISTE_EQUIP_COLLAB" required="true" type="String"/>
		<cfargument name="idpool" required="true" type="numeric"   />
		<cfargument name="idracine" required="true" type="numeric"   />
		
		<cfset IDGROUPE_RACINE = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.ValidatesaveMultipleEquip">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#LISTE_EQUIP_COLLAB#" cfsqltype="CF_SQL_CLOB">
			<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  variable="NB_IMEI" cfsqltype="cf_SQL_INTEGER" type="out">
			<cfprocparam  variable="NB_SIM" cfsqltype="cf_SQL_INTEGER" type="out">
			<cfprocparam  variable="NB_LIGNE" cfsqltype="cf_SQL_INTEGER" type="out">
			<cfprocparam  variable="NB_COLLAB" cfsqltype="cf_SQL_INTEGER" type="out">
			<cfprocparam  variable="RES" cfsqltype="cf_SQL_INTEGER" type="out">
			<cfprocparam  variable="ERREUR" cfsqltype="CF_SQL_CLOB" type="out">
			
		</cfstoredproc>
		
		<cfset MY_RESULT = StructNew() />
		<cfset MY_RESULT.NBIMEI = #NB_IMEI# />
		<cfset MY_RESULT.NBSIM = #NB_SIM# />
		<cfset MY_RESULT.NBLIGNE = #NB_LIGNE# />
		<cfset MY_RESULT.NBCOLLAB = #NB_COLLAB# />
		<cfset MY_RESULT.res = #RES# />
		<cfset MY_RESULT.erreur = #ERREUR# />
		
		<cfreturn MY_RESULT>
	</cffunction>
	
	
	<!--- 
		
	--->
	<cffunction name="saveMultipleEquip_v2" access="remote" returntype="numeric">
		<cfargument name="ID_REVENDEUR_TERM" 	required="true" type="Numeric"/>
		<cfargument name="ID_MODELE_TERM" 		required="true" type="Numeric"/>
		<cfargument name="ID_OP_SIM" 			required="true" type="Numeric"/>
		<cfargument name="LISTE_EQUIP_COLLAB" 	required="true" type="String"/>
		<cfargument name="ID_POOL" 				required="true" type="Numeric"/>
		<cfargument name="DATE_DEB_CONTRAT" 	required="true" type="String" hint="contrat de garantie"/>
		<cfargument name="DUREE_CONTRAT" 		required="true" type="Numeric"/>
		<cfargument name="DATE_DEB_LIGNE" 		required="true" type="String"/>
		<cfargument name="DUREE_ENGAGEMENT" 	required="true" type="Numeric"/>
		<cfargument name="is_pool_rev" 			required="true" type="Numeric"/>		
		
			<cfset IDGROUPE_RACINE = SESSION.PERIMETRE.ID_GROUPE>

			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.saveMultipleEquip_v2">
				<cfprocparam  value="#IDGROUPE_RACINE#" 	cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine">
				
				<cfif ID_REVENDEUR_TERM neq -1>
					<cfprocparam  value="#ID_REVENDEUR_TERM#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_idrevendeur">
				<cfelse>
					<cfprocparam  value="#ID_REVENDEUR_TERM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_idrevendeur" null="true">
				</cfif>
				<cfif ID_MODELE_TERM neq -1>
					<cfprocparam  value="#ID_MODELE_TERM#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_ideq_fourn">
				<cfelse>
					<cfprocparam  value="#ID_MODELE_TERM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_ideq_fourn" null="true">
				</cfif>
				<cfif ID_OP_SIM neq -1>
					<cfprocparam  value="#ID_OP_SIM#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_idoperateur">
				<cfelse>
					<cfprocparam  value="#ID_OP_SIM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur" null="true">
				</cfif>
				<!---<cfprocparam  value="#ID_REVENDEUR_TERM#" 	cfsqltype="CF_SQL_INTEGER" 	variable="p_idrevendeur">
				<cfprocparam  value="#ID_MODELE_TERM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_ideq_fourn">
				<cfprocparam  value="#ID_OP_SIM#" 			cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur">--->
				<cfprocparam  value="#LISTE_EQUIP_COLLAB#" 	cfsqltype="CF_SQL_CLOB"		variable="p_lst_equip">
				
				<cfif ID_POOL neq 0>
					<cfprocparam  value="#ID_POOL#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_idpool">
				<cfelse>
					<cfprocparam  value="#ID_POOL#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" null="true">
				</cfif>
				
				<cfif DUREE_CONTRAT neq 0>
					<cfprocparam  value="#DATE_DEB_CONTRAT#" cfsqltype="CF_SQL_VARCHAR"	variable="p_date	_debut_garantie">
				<cfelse>
					<cfprocparam  value="#DATE_DEB_CONTRAT#" cfsqltype="CF_SQL_VARCHAR" variable="p_date_debut_garantie" null="true">
				</cfif>
				
				<cfprocparam  value="#DUREE_CONTRAT#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_duree_garantie">
				
				<cfif DATE_DEB_LIGNE neq 0>
					<cfprocparam  value="#DATE_DEB_LIGNE#" 	cfsqltype="CF_SQL_VARCHAR"	variable="p_date_debut_ligne">
				<cfelse>
					<cfprocparam  value="#DATE_DEB_LIGNE#" 	cfsqltype="CF_SQL_VARCHAR" 	variable="p_date_debut_ligne" null="true">
				</cfif>
				
				<cfprocparam  value="#DUREE_ENGAGEMENT#" 	cfsqltype="CF_SQL_INTEGER"	variable="p_duree_engagement">	
				<cfprocparam  value="#is_pool_rev#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_is_pool_rev">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#" null="false">		
				<cfprocparam  variable="p_retour" 				cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
	
	<!--- 
		
	--->
	<cffunction name="saveMultipleEquip_v3" access="remote" returntype="numeric">
		<cfargument name="ID_REVENDEUR_TERM" 	required="true" type="Numeric"/>
		<cfargument name="ID_MODELE_TERM" 		required="true" type="Numeric"/>
		<cfargument name="ID_OP_SIM" 			required="true" type="Numeric"/>
		<cfargument name="LISTE_EQUIP_COLLAB" 	required="true" type="String"/>
		<cfargument name="ID_POOL" 				required="true" type="Numeric"/>
		<cfargument name="DATE_DEB_CONTRAT" 	required="true" type="String" hint="contrat de garantie"/>
		<cfargument name="DUREE_CONTRAT" 		required="true" type="Numeric"/>
		<cfargument name="DATE_DEB_LIGNE" 		required="true" type="String"/>
		<cfargument name="DUREE_ENGAGEMENT" 	required="true" type="Numeric"/>
		<cfargument name="is_pool_rev" 			required="true" type="Numeric"/>
		<cfargument name="segment" 			required="true" type="Numeric"/>		
		
			<cfset IDGROUPE_RACINE = SESSION.PERIMETRE.ID_GROUPE>

			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.saveMultipleEquip_v3">
				<cfprocparam  value="#IDGROUPE_RACINE#" 	cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine">
				
				<cfif ID_REVENDEUR_TERM neq -1>
					<cfprocparam  value="#ID_REVENDEUR_TERM#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_idrevendeur">
				<cfelse>
					<cfprocparam  value="#ID_REVENDEUR_TERM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_idrevendeur" null="true">
				</cfif>
				<cfif ID_MODELE_TERM neq -1>
					<cfprocparam  value="#ID_MODELE_TERM#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_ideq_fourn">
				<cfelse>
					<cfprocparam  value="#ID_MODELE_TERM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_ideq_fourn" null="true">
				</cfif>
				<cfif ID_OP_SIM neq -1>
					<cfprocparam  value="#ID_OP_SIM#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_idoperateur">
				<cfelse>
					<cfprocparam  value="#ID_OP_SIM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur" null="true">
				</cfif>
				<!---<cfprocparam  value="#ID_REVENDEUR_TERM#" 	cfsqltype="CF_SQL_INTEGER" 	variable="p_idrevendeur">
				<cfprocparam  value="#ID_MODELE_TERM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_ideq_fourn">
				<cfprocparam  value="#ID_OP_SIM#" 			cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur">--->
				<cfprocparam  value="#LISTE_EQUIP_COLLAB#" 	cfsqltype="CF_SQL_CLOB"		variable="p_lst_equip">
				
				<cfif ID_POOL neq 0>
					<cfprocparam  value="#ID_POOL#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_idpool">
				<cfelse>
					<cfprocparam  value="#ID_POOL#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" null="true">
				</cfif>
				
				<cfif DUREE_CONTRAT neq 0>
					<cfprocparam  value="#DATE_DEB_CONTRAT#" cfsqltype="CF_SQL_VARCHAR"	variable="p_date_debut_garantie">
				<cfelse>
					<cfprocparam  value="#DATE_DEB_CONTRAT#" cfsqltype="CF_SQL_VARCHAR" variable="p_date_debut_garantie" null="true">
				</cfif>
				
				<cfprocparam  value="#DUREE_CONTRAT#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_duree_garantie">
				
				<cfif DATE_DEB_LIGNE neq 0>
					<cfprocparam  value="#DATE_DEB_LIGNE#" 	cfsqltype="CF_SQL_VARCHAR"	variable="p_date_debut_ligne">
				<cfelse>
					<cfprocparam  value="#DATE_DEB_LIGNE#" 	cfsqltype="CF_SQL_VARCHAR" 	variable="p_date_debut_ligne" null="true">
				</cfif>
				
				<cfprocparam  value="#DUREE_ENGAGEMENT#" 	cfsqltype="CF_SQL_INTEGER"	variable="p_duree_engagement">	
				<cfprocparam  value="#is_pool_rev#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_is_pool_rev">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#" null="false">
				<cfprocparam  value="#segment#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_segment">		
				<cfprocparam  variable="p_retour" 				cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
	
	<!--- 
		
	--->
	<cffunction name="saveMultipleEquip_v4" access="remote" returntype="numeric">
		<cfargument name="ID_REVENDEUR_TERM" 	required="true" type="Numeric"/>
		<cfargument name="ID_MODELE_TERM" 		required="true" type="Numeric"/>
		<cfargument name="ID_OP_SIM" 			required="true" type="Numeric"/>
		<cfargument name="LISTE_EQUIP_COLLAB" 	required="true" type="String"/>
		<cfargument name="ID_POOL" 				required="true" type="Numeric"/>
		<cfargument name="DATE_DEB_CONTRAT" 	required="true" type="String" hint="contrat de garantie"/>
		<cfargument name="DUREE_CONTRAT" 		required="true" type="Numeric"/>
		<cfargument name="DATE_DEB_LIGNE" 		required="true" type="String"/>
		<cfargument name="DUREE_ENGAGEMENT" 	required="true" type="Numeric"/>
		<cfargument name="is_pool_rev" 			required="true" type="Numeric"/>
		<cfargument name="segment" 			required="true" type="Numeric"/>		
		
			<cfset IDGROUPE_RACINE = SESSION.PERIMETRE.ID_GROUPE>

			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.saveMultipleEquip_v4">
				<cfprocparam type="in"  value="#IDGROUPE_RACINE#" 	cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine">
				
				<cfif ID_REVENDEUR_TERM neq -1>
					<cfprocparam type="in"  value="#ID_REVENDEUR_TERM#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_idrevendeur">
				<cfelse>
					<cfprocparam type="in"  value="#ID_REVENDEUR_TERM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_idrevendeur" null="true">
				</cfif>
				
				<cfif ID_MODELE_TERM neq -1>
					<cfprocparam type="in"  value="#ID_MODELE_TERM#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_ideq_fourn">
				<cfelse>
					<cfprocparam type="in"  value="#ID_MODELE_TERM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_ideq_fourn" null="true">
				</cfif>
				
				<cfif ID_OP_SIM neq -1>
					<cfprocparam type="in"  value="#ID_OP_SIM#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_idoperateur">
				<cfelse>
					<cfprocparam type="in"  value="#ID_OP_SIM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur" null="true">
				</cfif>
				
				<cfprocparam type="in"  value="#LISTE_EQUIP_COLLAB#" 	cfsqltype="CF_SQL_CLOB"		variable="p_lst_equip">
				
				<cfif ID_POOL neq 0>
					<cfprocparam type="in"  value="#ID_POOL#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_idpool">
				<cfelse>
					<cfprocparam type="in"  value="#ID_POOL#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" null="true">
				</cfif>
				
				<cfif DUREE_CONTRAT neq 0>
					<cfprocparam type="in"  value="#DATE_DEB_CONTRAT#" cfsqltype="CF_SQL_VARCHAR"	variable="p_date_debut_garantie">
					<cfprocparam type="in"  value="#DUREE_CONTRAT#" cfsqltype="CF_SQL_INTEGER"	variable="p_duree_garantie">
				<cfelse>
					<cfprocparam type="in"  value="#DATE_DEB_CONTRAT#" cfsqltype="CF_SQL_VARCHAR" variable="p_date_debut_garantie" null="true">
					<cfprocparam type="in"  value="#DUREE_CONTRAT#" cfsqltype="CF_SQL_INTEGER" variable="p_duree_garantie" null="true">
				</cfif>
				
				<cfif DATE_DEB_LIGNE neq 0>
					<cfprocparam type="in"  value="#DATE_DEB_LIGNE#" 	cfsqltype="CF_SQL_VARCHAR"	variable="p_date_debut_ligne">
				<cfelse>
					<cfprocparam type="in"  value="#DATE_DEB_LIGNE#" 	cfsqltype="CF_SQL_VARCHAR" 	variable="p_date_debut_ligne" null="true">
				</cfif>
				
				<cfif DATE_DEB_LIGNE neq 0>
					<cfprocparam type="in"  value="#DUREE_ENGAGEMENT#" 	cfsqltype="CF_SQL_INTEGER"	variable="p_duree_engagement">
				<cfelse>
					<cfprocparam type="in"  value="#DUREE_ENGAGEMENT#" 	cfsqltype="CF_SQL_INTEGER" 	variable="p_duree_engagement" null="true">
				</cfif>
				
				<cfprocparam type="in"  value="#is_pool_rev#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_is_pool_rev">
				<cfprocparam type="in"  value="#SESSION.USER.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER" variable="p_iduser" null="false">
				<cfprocparam type="in"  value="#segment#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_segment">
						
				<cfprocparam  variable="p_retour" 				cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>	
	
	
	<!--- 
		
	--->
	<cffunction name="get_catalogue_revendeur" access="remote" returntype="query" output="false" hint="Lister revendeurs, génériques et privés pour une racine donnée. (Catalogue fournisseur)." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
		<cfargument name="app_loginID" required="false" type="numeric" default="" displayname="numeric app_loginID" hint="paramNotes" />
		<cfargument name="p_segment" required="false" type="numeric" default="0" displayname="numeric p_segment" hint="Initial value for the p_segmentproperty." />
		
		<cfif CompareNoCase(chaine,"null") eq 0>
			<cfset chaine = "">
		</cfif>
		
		<cfset p_idgroupe_client = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_M111B.get_catalogue_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#app_loginID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_segment#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>		
	</cffunction>
	
	
	<!--- 
		lISTE DES EQP MOBILES/FIXES/RESEAU
	 --->
	<cffunction name="fournirListeMobileCatalogueClient" returntype="query" access="remote">
		<cfargument name="idRevendeur" required="true" type="numeric"  displayname="revendeur" hint="le revendeur"/>
		<cfargument name="clef" required="true" type="string"  displayname="clef" hint="clef de recherche"/>
		<cfargument name="segment" required="false" type="numeric" default="0" displayname="segment" hint="le filtre par segment"/>
		
		<cfset p_idcategorie_equipement = 0>
		<cfset p_idtype_equipement = 0>
		<cfset p_idfournisseur = idRevendeur>
		<cfset p_idgamme_fournis = 0>
		<cfset p_chaine = clef>
		<cfset p_idgroupe_client = SESSION.PERIMETRE.ID_GROUPE>
		<cfset p_type_fournisseur = 0>
		<cfset codeLangue=session.user.globalization>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.SearchCatalogueRevendeur_v6">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcategorie_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idtype_equipement#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfournisseur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgamme_fournis#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_type_fournisseur#" null="true">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	
	<!--- 
		
	--->
	<cffunction name="fournirListeOperateursSegment" access="remote" returntype="query" output="false" hint="fournit le liste de tous les opérateurs du segment.segment : 2 = FIXDATA | 1 = MOBILE | 0 = TOUT" >
		<cfargument name="segment" required="true" type="numeric" default="0" displayname="string segment" hint="Initial value for the segmentproperty." />
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset qResult = "">
		<!---<cfswitch expression="#segment#">
			<cfcase value="2">
				 <cfquery name="qResult" datasource="#Session.OFFREDSN#" blockfactor="10">
					SELECT op.operateurid, op.nom as libelle FROM operateur op 
				</cfquery> 			
			</cfcase>
			<cfcase value="1">
				--->
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M111.GETFABOP_ACTIF">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idRacine" value="#idRacine#"/>
				<cfprocresult name="qResult">
			</cfstoredproc>
				
			<!---</cfcase>			
			<cfdefaultcase>
				<!--- <cfquery name="qResult" datasource="#Session.OFFREDSN#" blockfactor="10">
					SELECT op.operateurid, op.nom as libelle FROM operateur op 
				</cfquery> --->
			</cfdefaultcase>
			
		</cfswitch>
		--->
		
		<cfreturn qResult/>
	</cffunction>	
	
	
	<!--- 
		
	--->
	<cffunction name="getInfosClientOperateurActuel" access="remote" returntype="InfosClientOperateurVo" output="false"  >
		<cfargument name="IDOPERATEUR" required="true" type="numeric"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M111.GETHISTORIQUEINFOSCLIENTOP" blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#Session.PERIMETRE.ID_GROUPE#">
			<cfprocresult name="histoInfos">			
		</cfstoredproc>
		
		<cfquery dbtype="query" name="qGetInfo">
			select * from histoInfos where histoInfos.BOOL_ACTUEL = 1
			and  histoInfos.OPERATEURID = #IDOPERATEUR#
		</cfquery>
		 
		<!--- 
		<cfstoredproc datasource="#DSN#" procedure="PKG_CV_CONTRAT.INFOSCLIENTOPERATEURACTUEL" blockfactor="1">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" value="#IDOPERATEUR#">			
			<cfprocresult name="qGetInfo">			
		</cfstoredproc>
		 --->
		 
		<cfset infos = createObject("component","InfosClientOperateurVo")>
		
				
		<cfif qGetInfo.recordCount gt 0>
			<cfset infos.setOPERATEUR_CLIENT_ID(qGetInfo["OPERATEUR_CLIENT_ID"][1])/>
			<cfset infos.setOPERATEURID(qGetInfo["OPERATEURID"][1])/>
			<cfset infos.setNOM_OP(qGetInfo["NOM_OP"][1])/>
			
			<cfset infos.setIDRACINE(qGetInfo["IDRACINE"][1])/>
			
			<cfset infos.setDATE_MODIF(qGetInfo["DATE_MODIF"][1])/>
			<cfset infos.setDATE_CREATE(qGetInfo["DATE_CREATE"][1])/>
			
			<cfset infos.setMONTANT_MENSUEL_PENALITE(val(qGetInfo["MONTANT_MENSUEL_PENALITE"][1]))/>
			<cfset infos.setFRAIS_FIXE_RESILIATION(val(qGetInfo["FRAIS_FIXE_RESILIATION"][1]))/>
			<cfset infos.setFPC_UNIQUE(qGetInfo["FPC_UNIQUE"][1])/>
			<cfset infos.setBOOL_ACTUEL(val(qGetInfo["BOOL_ACTUEL"][1]))/>
			
			<cfset infos.setUSER_MODIF(val(qGetInfo["USER_MODIF"][1]))/>
			<cfset infos.setUSER_CREATE(val(qGetInfo["USER_CREATE"][1]))/>
			
			<cfset infos.setNOM_USER_MODIFIED(qGetInfo["NOM_USER_MODIFIED"][1])/>
			<cfset infos.setNOM_USER_CREATE(qGetInfo["NOM_USER_CREATE"][1])/>
			
			<cfset infos.setENGAG_12(val(qGetInfo["ENGAG_12"][1]))/>			
			<cfset infos.setENGAG_24(val(qGetInfo["ENGAG_24"][1]))/>
			<cfset infos.setENGAG_36(val(qGetInfo["ENGAG_36"][1]))/>
			<cfset infos.setENGAG_48(val(qGetInfo["ENGAG_48"][1]))/>
			<cfset infos.setSEUIL_TOLERANCE(val(qGetInfo["SEUIL_TOLERANCE"][1]))/>
			
			<cfset infos.setDUREE_ELIGIBILITE(val(qGetInfo["DUREE_ELLIGIBILITE"][1]))/>
			<cfset infos.setDUREE_OUVERTURE(val(qGetInfo["DUREE_OUVERTURE"][1]))/>
			<cfset infos.setDUREE_RENOUVEL(val(qGetInfo["DUREE_RENOUVELLEMENT"][1]))/>
			
		</cfif>
		
		<cfreturn infos>
	</cffunction>
	
	
	<!--- 
		vérifier les données collaborateur
	--->
	<cffunction name="iMPCheckCollab" output="false" access="remote" returntype="Numeric">
		
		<cfargument name="nom" type="string" required="true">
		<cfargument name="prenom" type="string" required="true">
		<cfargument name="matricule" type="string" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.IMP_CheckCollab">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idRacine">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR2" value="#nom#" variable="p_nom">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR2" value="#prenom#" variable="p_prenom">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR2" value="#matricule#" variable="p_matricule">
			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>
	
	
	<!--- 
		vérifier le numéro d'IMEI
	--->
	<cffunction name="iMPCheckIMEI" output="false" access="remote" returntype="Numeric">
		
		<cfargument name="IMEI" type="string" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.IMP_CheckIMEI">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idRacine">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR2" value="#IMEI#" variable="p_imei">
			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>
	
	
	<!--- 
		vérifier le numéro de série
	--->
	<cffunction name="iMPCheckNSERIE" output="false" access="remote" returntype="Numeric">
		
		<cfargument name="num_serie" type="string" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.IMP_CheckNSERIE">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idRacine">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#num_serie#" variable="p_num_serie">
			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>
	
	
	<!--- 
		vérifier le numéro de d'extension
	--->
	<cffunction name="iMPCheckNEXTENSION" output="false" access="remote" returntype="Numeric">
		
		<cfargument name="num_extension" type="string" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.IMP_CheckNEXTENSION">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idRacine">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#num_extension#" variable="p_num_extension">
			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>
	
	
	<!--- 
		vérifier le numéro de SIM
	--->
	<cffunction name="iMPCheckNSIM" output="false" access="remote" returntype="Numeric">
		
		<cfargument name="num_SIM" type="string" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.IMP_CheckNSIM">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idRacine">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#num_SIM#" variable="p_num_SIM">
			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>
	
	
	<!--- 
		vérifier le numéro de ligne
	--->
	<cffunction name="iMPCheckNLIGNE" output="false" access="remote" returntype="Numeric">
		
		<cfargument name="num_ligne" type="string" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.IMP_CheckNLIGNE">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idRacine">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR2" value="#num_ligne#" variable="p_ligne">
			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>
	
	
	<!--- 
		IMPorter un fichier XLS
	--->
	<cffunction name="iMPImporterFichier" output="false" access="remote" returntype="Numeric">
		
		<cfargument name="list_equip" type="array" required="true">
		<cfargument name="idpool" type="numeric" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset idUser = SESSION.USER.APP_LOGINID> <!--- OU SESSION.USER.CLIENTACCESSID --->
		<cfset d = {}>
		<cfset d['row']=[]>
		 
		<cfloop index="i" from="1" to="#arraylen(list_equip)#">
		 	
			<cfset item = {}>
					
			<!--- collab --->
			<cfif structKeyExists(list_equip[i],"collab")>
				<cfset item['collaborateur'] = {}>
				<cfset item['collaborateur']['matricule'] = ':str:' & list_equip[i]["collab"]["matricule"]>
				<cfset item['collaborateur']['nom'] = ':str:' & list_equip[i]["collab"]["nom"]>
				<cfset item['collaborateur']['prenom'] = ':str:' & list_equip[i]["collab"]["prenom"]>
			</cfif>
			<!--- equipement --->
			<cfif structkeyExists(list_equip[i],"equipement")>
				<cfset item['equipement'] = {}>
				<cfset item['equipement']['iddistrib'] = javacast('int','#list_equip[i]["equipement"]["iddistrib"]#')>
				<cfset item['equipement']['idtype'] = javacast('int','#list_equip[i]["equipement"]["idtype"]#')>
				<cfset item['equipement']['ideq_fourn'] = javacast('int','#list_equip[i]["equipement"]["ideq_fourn"]#')>
				<cfset item['equipement']['imei'] = ':str:' & list_equip[i]["equipement"]["imei"]>
				<cfset item['equipement']['nserie'] = ':str:' & list_equip[i]["equipement"]["nserie"]>
				<cfif structkeyExists(list_equip[i]['equipement'],"contrat")>					
					<cfset item['equipement']['contrat'] = {}>
					<cfset item['equipement']['contrat']['idtype'] = javacast('int','#list_equip[i]["equipement"]["contrat"]["idtype"]#')>
					<cfset item['equipement']['contrat']['date_deb'] = ':str:' & list_equip[i]["equipement"]["contrat"]["date_deb"]>
					<cfset item['equipement']['contrat']['duree'] = javacast('int','#list_equip[i]["equipement"]["contrat"]["duree"]#')>
					<cfset item['equipement']['contrat']['fpc'] = ''><!--- affecte rien pour le moment --->
				</cfif>
				
			</cfif>
			<!--- sim --->
			<cfif structkeyExists(list_equip[i],"sim")>
				<cfset item['sim'] = {}>
				<cfset item['sim']['idoperateur'] = javacast('int','#list_equip[i]["sim"]["idoperateur"]#')>
				<cfset item['sim']['iddistrib'] = javacast('int','#list_equip[i]["sim"]["iddistrib"]#')>
				<cfset item['sim']['numero'] = ':str:' & list_equip[i]["sim"]["numero"]>
			</cfif>
			<!--- ligne --->
			<cfif structkeyExists(list_equip[i],"ligne")>				
				<cfset item['ligne'] = {}>
				<cfset item['ligne']['idtype'] = javacast('int','#list_equip[i]["ligne"]["idtype"]#')>
				<cfset item['ligne']['idoperateur'] = javacast('int','#list_equip[i]["ligne"]["idoperateur"]#')>
				<cfset item['ligne']['numero'] = ':str:' & list_equip[i]["ligne"]["numero"]>
				<cfset item['ligne']['contrat'] = {}>
				<cfset item['ligne']['contrat']['idtype'] = javacast('int','#list_equip[i]["ligne"]["contrat"]["idtype"]#')>
				<cfset item['ligne']['contrat']['date_deb'] = ':str:' & list_equip[i]["ligne"]["contrat"]["date_deb"]>
				<cfif list_equip[i]['ligne']['contrat']['duree'] neq 0>
					<cfset item['ligne']['contrat']['duree'] = javacast('int','#list_equip[i]["ligne"]["contrat"]["duree"]#')>
				</cfif>
				<cfif list_equip[i]['ligne']['contrat']['fpc'] neq "">
					<cfset item['ligne']['contrat']['fpc'] = ':str:' & list_equip[i]["ligne"]["contrat"]["fpc"]>					
				</cfif>
			</cfif>
			<!--- extension --->
			<cfif structkeyExists(list_equip[i],"extension")>
				<cfset item['extension'] = {}>
				<cfset item['extension']['idoperateur'] = javacast('int','#list_equip[i]["extension"]["idoperateur"]#')>
				<cfset item['extension']['numero'] = ':str:' & list_equip[i]["extension"]["numero"]>
			</cfif>
			 
			<cfset ArrayAppend(d['row'],item)>
		  
		</cfloop>
		
		<cfset __jsoutput = SerializeJSON(d)>
		<cfset jsoutput = Replace(__jsoutput,':str:','','all')>
		
		<!--- proc --->
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.IMP_IMPORTER">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idRacine">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idpool#" variable="p_idPool">
			<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" value="#jsoutput#" variable="p_list_equip">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idUser#" variable="p_idUser">
			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>

</cfcomponent>
