﻿<cfcomponent output="false">
	
	<cfproperty name="IDCONTRAT" 				type="numeric" default="0">
	<cfproperty name="REFCONTRAT"		 		type="string" default="">
	
	<cfproperty name="IDTYPECONTRAT" 			type="numeric" default="0">
	<cfproperty name="TYPECONTRAT"		 		type="string" default="">
	
	<cfproperty name="IDFOURNISSEUR" 			type="numeric" default="0">
	
	<cfproperty name="IDUSERCREATE"				type="numeric" default="0">
	<cfproperty name="IDUSERMODIF" 				type="numeric" default="0">
	<cfproperty name="IDUSERRESI" 				type="numeric" default="0">
	
	<cfproperty name="NOMUSERCREATE" 			type="string" default="">
	<cfproperty name="NOMUSERMODIF" 			type="string" default="">
	<cfproperty name="NOMUSERRESI" 				type="string" default="">
	
	<cfproperty name="DATECREATE" 				type="string">
	<cfproperty name="DATEDEBUT" 				type="string">
	<cfproperty name="DATEECHEANCE" 			type="string">
	<cfproperty name="DATEELIGIBILITE" 			type="string">
	<cfproperty name="DATEMODIF" 				type="string">
	<cfproperty name="DATERENOUVELLEMENT" 		type="string">
	<cfproperty name="DATERESI" 				type="string">
	<cfproperty name="DATERESILIATION" 			type="string">
	<cfproperty name="DATESIGNATURE" 			type="string">
	
	<cfproperty name="COMMENTAIRES" 			type="string" default="">
	
	<cfproperty name="DUREECONTRAT" 			type="numeric" default="0">
	<cfproperty name="DUREEELIGIBILITE" 		type="numeric" default="0">
	
	<cfproperty name="TARIFMENSUEL" 			type="numeric" default="0">
	<cfproperty name="TARIF"		 			type="numeric" default="0">
	
	<cfproperty name="PREAVIS" 					type="numeric" default="0">
	<cfproperty name="TACITERECONDUCTION" 		type="numeric" default="0">
	
	<cfproperty name="IDREVENDEUR" 				type="numeric" default="0">
	<cfproperty name="REVENDEUR" 				type="string" default="">
	
	
	<!---
	<cfproperty name="IDCDE_CONTACT_SOCIETE" 	type="numeric" default="0">
	<cfproperty name="LOYER" 					type="numeric" default="0">
	<cfproperty name="PERIODICITE" 				type="numeric" default="0">
	<cfproperty name="MONTANT_FRAIS" 			type="numeric" default="0">
	<cfproperty name="NUMERO_CLIENT" 			type="string" default="">
	<cfproperty name="NUMERO_FOURNISSEUR" 		type="string" default="">
	<cfproperty name="NUMERO_FACTURE" 			type="string" default="">
	<cfproperty name="CODE_INTERNE" 			type="string" default="">
	<cfproperty name="BOOL_CONTRAT_CADRE" 		type="numeric" default="0">
	<cfproperty name="BOOL_AVENANT" 			type="numeric" default="0">
	<cfproperty name="ID_CONTRAT_MAITRE" 		type="numeric" default="0">
	<cfproperty name="FRAIS_FIXE_RESILIATION" 	type="numeric" default="0">
	<cfproperty name="MONTANT_MENSUEL_PEN" 		type="numeric" default="0">
	<cfproperty name="PENALITE" 				type="numeric" default="0">
	
	
	<cfproperty name="LIBELLE_OPERATEUR" 		type="string" default="">
	<cfproperty name="IDCOMPTE_FACTURATION" 	type="numeric" default="0">
	<cfproperty name="IDSOUS_COMPTE" 			type="numeric" default="0">
	<cfproperty name="NUMERO_CONTRAT" 			type="string" default="">
	<cfproperty name="IDRACINE" 				type="numeric" default="0">
	
	<cfproperty name="SOUS_TETE" 				type="string" default="">
	<cfproperty name="IDSOUS_TETE" 				type="numeric" default="0">
	--->
	<cffunction name="init" output="false" returntype="CommandeVO">
		<cfreturn this>
	</cffunction>
</cfcomponent>