﻿<cfcomponent displayname="fr.consotel.consoview.M11.Logger" output="false"   hint="Gestion des logs des actes de gestion">
		
	<cfset init()>
	
	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
	</cffunction>

	<cffunction name="logAction" access="remote" returntype="void"  hint="Log l'action effectuée pour l'historique">
		
		<cfargument name="ID_WORDS" required="true" type="Numeric"/>
		<cfargument name="ACTION_PRINCIPALE" required="true" type="String"/>
		<cfargument name="ITEM1" required="true" type="String"/>
		<cfargument name="ITEM2" required="true" type="String"/>
		<cfargument name="ID_TERMINAL" required="true" type="Numeric"/>
		<cfargument name="ID_SIM" required="true" type="any"/>
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric"/>
		<cfargument name="IDSOUS_TETE" required="true" type="Numeric"/>
		<cfargument name="UID_ACTION" required="true" type="String"/>
		<cfargument name="DATE_EFFET" required="true" type="String"/>
		<cfargument name="COMMENTAIRE" required="false" type="String" default=""/>
		<cfargument name="ID_CAUSE" required="false" type="Numeric" default=-1/>
		<cfargument name="ID_STATUT" required="false" type="Numeric" default=-1/>
	
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset USER = SESSION.USER.CLIENTACCESSID>
	
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.log_action_flotte_v2">
			<cfprocparam value="#USER#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_WORDS#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ACTION_PRINCIPALE#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#ITEM1#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#ITEM2#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#ID_TERMINAL#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_SIM#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idRacine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#UID_ACTION#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<!---<cfprocparam value="#DATE_EFFET#" cfsqltype="CF_SQL_VARCHAR" type="in">--->
			<cfif COMMENTAIRE neq "">
				<cfprocparam value="#COMMENTAIRE#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfelse>
				<cfprocparam value="#COMMENTAIRE#" cfsqltype="CF_SQL_VARCHAR" null="true">
			</cfif>
			<cfif ID_CAUSE neq -1>
				<cfprocparam value="#ID_CAUSE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfelse>
				<cfprocparam value="#ID_CAUSE#" cfsqltype="CF_SQL_INTEGER" null="true">
			</cfif>
			<cfif ID_STATUT neq -1>
				<cfprocparam value="#ID_STATUT#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfelse>
				<cfprocparam value="#ID_STATUT#" cfsqltype="CF_SQL_INTEGER" null="true">
			</cfif>
			<cfprocparam variable="logStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
	</cffunction>
	
	
	
	<cffunction name="sendMailAction" access="remote" returntype="void"  hint="envoit un mail d'erreur quand le log a échoué">
		
		<cfargument name="ID_WORDS" 	required="true" type="Numeric"/>
		<cfargument name="ACTION_PRINCIPALE" required="true" type="String"/>
		<cfargument name="ITEM1" 		required="true" type="String"/>
		<cfargument name="ITEM2" 		required="true" type="String"/>
		<cfargument name="ID_TERMINAL" 	required="true" type="Numeric"/>
		<cfargument name="ID_SIM" 		required="true" type="any"/>
		<cfargument name="ID_EMPLOYE" 	required="true" type="Numeric"/>
		<cfargument name="IDSOUS_TETE" 	required="true" type="Numeric"/>
		<cfargument name="UID_ACTION" 	required="true" type="String"/>
		<cfargument name="DATE_EFFET" 	required="true" type="String"/>
		<cfargument name="ERREUR" 		required="true" type="struct"/>
		<cfargument name="COMMENTAIRE" 	required="false" type="String" default=""/>
		<cfargument name="ID_CAUSE" 	required="false" type="Numeric" default=-1/>
		<cfargument name="ID_STATUT" 	required="false" type="Numeric" default=-1/>
		
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset USER = SESSION.USER.CLIENTACCESSID>
		
		<cflog text="sendMailAction - > #ACTION_PRINCIPALE#">
		
		<cfmail from="no-reply@saaswedo.com" subject="[M111] ERREUR DE LOG" to="monitoring@saaswedo.com" type="text/html" >
			<cfoutput> 
				ID_WORDS = #ID_WORDS# <br>
				ACTION_PRINCIPALE = #ACTION_PRINCIPALE# <br>
				ITEM1 = #ITEM1# <br>
				ITEM2 = #ITEM2# <br>
				ID_TERMINAL = #ID_TERMINAL# <br>
				ID_SIM = #ID_SIM# <br>
				ID_EMPLOYE = #ID_EMPLOYE# <br>
				IDSOUS_TETE = #IDSOUS_TETE# <br>
				UID_ACTION = #UID_ACTION# <br>
				DATE_EFFET = #DATE_EFFET# <br>
				COMMENTAIRE = #COMMENTAIRE# <br>
				ID_CAUSE = #ID_CAUSE# <br>
				ID_STATUT = #ID_STATUT# <br>
				<br>
				<cfdump var="#ERREUR#">
			</cfoutput>
		</cfmail>
	</cffunction>
	
	<cffunction name="setError" access="remote" returntype="Struct" hint="envoit un mail d'erreur quand le log a échoué">
		
		<cfargument name="detail" 	required="true" type="String"/>
		<cfargument name="errorcode" required="true" type="String"/>
		<cfargument name="message" 		required="true" type="String"/>
		<cfargument name="type" 		required="true" type="String"/>
		<cfargument name="stackTrace"	required="false" type="String" default=""/>
		
		<cfset error = structNew()>
		<cfset error.detail = detail>
		<cfset error.errorcode = errorcode>
		<cfset error.message = message>
		<cfset error.type = type>
		<cfset error.stackTrace = stackTrace>
		
		<cfreturn error>				
	</cffunction>
	
</cfcomponent>