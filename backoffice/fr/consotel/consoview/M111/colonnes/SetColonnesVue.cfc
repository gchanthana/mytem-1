<cfcomponent  displayname="SetColonneVue">

	<cfset init()>
	
	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
	</cffunction>
	
	<cffunction access="remote" name="setAllColonnes" returntype="Numeric">
		
		<cfargument name="idColonnes" type="string" required="true">
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.update_column_user">
			
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#idColonnes#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="result">
						
		</cfstoredproc>
	
		<cfreturn result >
	</cffunction>
	
</cfcomponent>