﻿<cfcomponent displayname="OSEnrollmentStrategy" hint="Gestion prise en compte du terminal" implements="OSEnrollmentInterface">		
	<cfproperty name="strategy" type="OSEnrollmentInterface">
	
	<cffunction name="setStrategy" displayname="setStrategy">
		<cfargument name="codeOS" required="true"  type="string">				
		<cftry>
			<cfset strategy = createObject("component","fr.consotel.consoview.M111.enrollmentprocess.#codeOS#EnrollmentStrategy")>
			<cfcatch type="any">
				<cfset createObject("component","fr.consotel.consoview.M111.enrollmentprocess.#codeOS#EnrollmentStrategy")>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction name="initProcess" displayname="initProcess" description="Initialisation du process d'enrollment du device" 
				hint="Initialisation du process d'enrollment du device" access="remote" output="false" returntype="numeric">
		<cfargument name="IDSOUS_TETE" type="numeric" hint="l idendtifiant de la sous - tete (ligne)) vers laquelle sera envoyé le SMS d'inscription" required="true" >
		<cfargument name="SOUS_TETE" type="string" hint="le numéro la sous - tete (ligne)) vers laquelle sera envoyé le SMS d'inscription" required="true" >
		<cfargument name="cvIDTERMINAL" type="numeric" hint="le numéro IMEI du Device" required="true" >
		<cfargument name="cvIMEI" type="string" hint="le numéro IMEI du Device" required="false" >
		<cfreturn strategy.initProcess(IDSOUS_TETE,SOUS_TETE,cvIDTERMINAL,cvIMEI)>
	</cffunction>
	
	
	<cffunction access="remote" name="getListOs" displayname="getListOs" returntype="Query" 
				description="liste les OS pris en charge par l API" 
				hint="liste les OS pris en charge par l API">
		<cfset qOS=queryNew("LIBELLE,OS_CODE,URL_IMAGE,IS_ACTIF,IDOS","VarChar,VarChar,VarChar,Integer,Integer" )>
		
		<cfset queryAddRow(qOS,1)>
		<cfset querySetCell(qOS,"LIBELLE","Android",1)>
		<cfset querySetCell(qOS,"OS_CODE","Android",1)>
		<cfset querySetCell(qOS,"URL_IMAGE","nc",1)>
		<cfset querySetCell(qOS,"IDOS",1,1)>
		<cfset querySetCell(qOS,"IS_ACTIF",1,1)>
		
		<cfset queryAddRow(qOS,1)>
		<cfset querySetCell(qOS,"LIBELLE","iOS",2)>
		<cfset querySetCell(qOS,"OS_CODE","IOS",2)>
		<cfset querySetCell(qOS,"URL_IMAGE","nc",2)>
		<cfset querySetCell(qOS,"IDOS",2,2)>
		<cfset querySetCell(qOS,"IS_ACTIF",1,2)>
				
		<cfset queryAddRow(qOS,1)>
		<cfset querySetCell(qOS,"LIBELLE","Windows Phone 6.5",3)>
		<cfset querySetCell(qOS,"OS_CODE","WinPhone65",3)>
		<cfset querySetCell(qOS,"URL_IMAGE","nc",3)>
		<cfset querySetCell(qOS,"IDOS",3,3)>
		<cfset querySetCell(qOS,"IS_ACTIF",0,3)>
		
		<!---
		<cfset queryAddRow(qOS,1)>
		<cfset querySetCell(qOS,"LIBELLE","Windows Phone 7",4)>
		<cfset querySetCell(qOS,"OS_CODE","WinPhone7",4)>
		<cfset querySetCell(qOS,"URL_IMAGE","nc",4)>		
		<cfset querySetCell(qOS,"IDOS",4,4)>		
		<cfset querySetCell(qOS,"IS_ACTIF",0,4)>
		--->
		
		<cfreturn qOS>		
	</cffunction>

</cfcomponent>