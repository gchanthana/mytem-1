﻿<cfcomponent displayname="AndroidEnrollmentStrategy" hint="Gestion prise en compte du terminal" implements="OSEnrollmentInterface">

	<cffunction name="initProcess" displayname="initProcess" description="Initialisation du process d'enrollment du device" hint="Initialisation du process d'enrollment du device" access="remote" output="false" returntype="numeric">
		<cfargument name="IDSOUS_TETE" type="numeric" hint="l idendtifiant de la sous - tete (ligne)) vers laquelle sera envoyé le SMS d'inscription" required="true" >
		<cfargument name="SOUS_TETE" type="string" hint="le numéro la sous - tete (ligne)) vers laquelle sera envoyé le SMS d'inscription" required="true" >
		<cfargument name="cvIDTERMINAL" type="numeric" hint="le numéro IMEI du Device" required="true" >
		<cfargument name="cvIMEI" type="string" hint="le numéro IMEI du Device" required="false" >
		
		<cfset user  = 'demo'>
		<cfset mdp = '123456'>
		
		<cfif SESSION.PERIMETRE.ID_GROUPE eq 2716675>
			<cfset user  = 'demosfr'>
			<cfset mdp = 'pfgp'>
		</cfif>
		
<cfmail from="brice.miramont@consotel.fr" 
			bcc="dev@consotel.fr"
			subject="MDM" to="#SOUS_TETE#@contact-everyone.fr" type="text" server="mail.consotel.fr" port="26">1-Télécharger l'agent: http://mdm.consotel.fr/zdm/Zenprise.apk</cfmail>

<cfthread action="sleep" duration="3000"></cfthread>	

<cfmail from="brice.miramont@consotel.fr" 
			bcc="dev@consotel.fr"
			subject="MDM" to="#SOUS_TETE#@contact-everyone.fr" type="text" server="mail.consotel.fr" port="26">2-Renseigner les éléments suivants:
Login:#user#
Pwd:#mdp#
Instance:zdm
StrongID:
Serveur:mdm.consotel.fr
Port:443
SSL:oui</cfmail>

		
		<cfreturn 1>
	</cffunction>

</cfcomponent>