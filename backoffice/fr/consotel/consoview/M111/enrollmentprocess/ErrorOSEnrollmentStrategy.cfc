﻿<cfcomponent displayname="ErrorOSEnrollmentStrategy" hint="Gestion prise en compte du terminal Erreur" implements="OSEnrollmentInterface" >

	<cffunction name="initProcess" displayname="initProcess" description="Initialisation du process d'enrollment du device" hint="Initialisation du process d'enrollment du device" access="remote" output="false" returntype="numeric">
		<cfargument name="IDSOUS_TETE" type="numeric" hint="l idendtifiant de la sous - tete (ligne)) vers laquelle sera envoyé le SMS d'inscription" required="true" >
		<cfargument name="SOUS_TETE" type="string" hint="le numéro la sous - tete (ligne)) vers laquelle sera envoyé le SMS d'inscription" required="true" >
		<cfargument name="cvIDTERMINAL" type="numeric" hint="le numéro IMEI du Device" required="true" >
		<cfargument name="cvIMEI" type="string" hint="le numéro IMEI du Device" required="false" >
		<cfreturn -99>
	</cffunction>

</cfcomponent>