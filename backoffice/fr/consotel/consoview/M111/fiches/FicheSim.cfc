﻿<cfcomponent>

	<cfset init()>
	
	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
	</cffunction>

	<!--- ----------------- DEBUT SIM ----------------- --->
	
	<!--- 
		Fournis les infos de la fiche sim.
	 --->
	<cffunction name="getInfosFicheSim" access="remote" returntype="Array" hint="Fournis les infos de la fiche sim.">
		<cfargument name="idSim" 		required="true" type="numeric"/>
		<cfargument name="idSousTete" 	required="true" type="numeric"/>
		<cfargument name="idGpeClient" 	required="true" type="numeric"/>
		<cfargument name="id" 	required="true" type="numeric"/>
		
		<cfset chaineDebug="#idRacine#,#idSousTete#,#idSim#,#idGpeClient#,#idLangue#">
		
		<!--- <cfabort showError="#chaineDebug#"> --->
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.getfiche_Sim">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idSousTete#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idSim#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idGpeClient#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLangue#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#id#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
			
			<cfprocresult name="infos" 		resultset="1">
			<cfprocresult name="aboOpt" 	resultset="2">
			<cfprocresult name="incident" 	resultset="3">
			<cfprocresult name="orga"	 	resultset="4">
		</cfstoredproc>
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = p_result >
		<cfset p_retour[2] = infos >
		<cfset p_retour[3] = aboOpt >
		<cfset p_retour[4] = incident >
		<cfset p_retour[5] = orga >
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		Mise à jour des infos de la fiche sim.
	--->
	<cffunction name="setInfosFicheSim" access="remote" returntype="numeric" output="false" hint="Mise à jour des infos de la fiche sim.">
		<cfargument name="idSim"			required="true" type="numeric" />		
		<cfargument name="numSim" 			required="true" type="String" />		
		<cfargument name="pin1"	 			required="true" type="String" />		
		<cfargument name="pin2" 			required="true" type="String" />		
		<cfargument name="puk1"	 			required="true" type="String" />		
		<cfargument name="puk2" 			required="true" type="String" />		
		<cfargument name="idSite" 			required="true" type="numeric" />		
		<cfargument name="idEmplacement"	required="true" type="numeric" />
		<cfargument name="byod" 			required="true" type="numeric" />
		
		<cfset chaineDebug="#idSim#,#numSim#,#pin1#,#pin2#,#puk1#,#puk2#,#idSite#,#idEmplacement#">
		
		<!--- <cfabort showError="#chaineDebug#"> --->
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m111.setInfos_Sim_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSim#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numSim#" null="#iif((numSim eq -1), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#pin1#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#pin2#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#puk1#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#puk2#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="#iif((idSite eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEmplacement#" null="#iif((idEmplacement eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#byod#" >
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
			
		</cfstoredproc>
		<cfreturn p_result/>
	</cffunction>
	
	<cffunction name="mettreAuRebut" access="remote" returntype="numeric">
		
		<cfargument name="idSim"		required="true" type="Numeric" />
		<cfargument name="libelle_term" required="true" type="String" default=""/>
		<cfargument name="commentaire" 	required="true" type="String" />
		<cfargument name="id_cause" 	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111.putSimRecycle">
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idSim#"/>
					<cfprocparam  variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
				<cfset result = p_result/>
			<cfcatch type="any" >
				<cflog text="#cfcatch.Message# : #cfcatch.Detail#" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset result1 = logger.logAction(17,1,ITEM1,"",ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ARGUMENTS.commentaire,ARGUMENTS.id_cause)/>
			<cfcatch type="any">
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result1 = logger.sendMailAction(17,1,ITEM1,"",ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR,ARGUMENTS.commentaire,ARGUMENTS.id_cause)/>
			</cfcatch>
			</cftry>
		</cfif>	

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="echangerSim1" access="remote" returntype="numeric">
		
		<cfargument name="idNeweqp" 	required="true" type="Numeric" />
		<cfargument name="idOldeqp" 	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM1"		required="true" type="Numeric" />
		<cfargument name="ID_SIM2"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111.replaceEquipement_v2">
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="2"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idOldeqp#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idNeweqp#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="0"/>
					<cfprocparam  variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
				<cfset result = p_result/>
			<cfcatch type="any" >
				<cflog text="#cfcatch.Message# : #cfcatch.Detail#" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset result1 = logger.logAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM1,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
				<!--- <cfset result1 = logger.logAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM2,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/> --->
			<cfcatch type="any">
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result1 = logger.sendMailAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM1,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
				<!--- <cfset result1 = logger.sendMailAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM2,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/> --->
			</cfcatch>
			</cftry>
		</cfif>	

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="echangerExt1" access="remote" returntype="numeric">
		
		<cfargument name="idNeweqp" 	required="true" type="Numeric" />
		<cfargument name="idOldeqp" 	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT1"		required="true" type="any" />
		<cfargument name="ID_EXT2"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111.replaceEquipement_v2">
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="2"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idOldeqp#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idNeweqp#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="0"/>
					<cfprocparam  variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
				<cfset result = p_result/>
			<cfcatch type="any" >
				<cflog text="#cfcatch.Message# : #cfcatch.Detail#" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset result1 = logger.logAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT1,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
				<!--- <cfset result1 = logger.logAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT2,0,IDSOUS_TETE,uid_action,DATE_EFFET)/> --->
			<cfcatch type="any">
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result1 = logger.sendMailAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT1,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
				<!--- <cfset result1 = logger.sendMailAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT2,0,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/> --->
			</cfcatch>
			</cftry>
		</cfif>	

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="echangerSim2" access="remote" returntype="numeric">
		
		<cfargument name="idNeweqp" 	required="true" type="Numeric" />
		<cfargument name="idOldeqp"		required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM1"		required="true" type="Numeric" />
		<cfargument name="ID_SIM2"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />

		<cftry>
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111.replaceEquipement_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="2"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idOldeqp#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idNeweqp#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="1"/>
				<cfprocparam  variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			<cfset result = p_result/>
		<cfcatch type="any" >
			<cflog text="#cfcatch.Message# : #cfcatch.Detail#" >
			<cfset result = -10/>
		</cfcatch>
		</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset result1 = logger.logAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM1,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
				<!--- <cfset result1 = logger.logAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM2,0,IDSOUS_TETE,uid_action,DATE_EFFET)/> --->
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result1 = logger.sendMailAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM1,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
				<!--- <cfset result1 = logger.sendMailAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM2,0,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/> --->
			</cfcatch>
			</cftry>
		</cfif>	

		<cfreturn result>
	</cffunction>
	
	<cffunction name="echangerExt2" access="remote" returntype="numeric">
		
		<cfargument name="idNeweqp" 	required="true" type="Numeric" />
		<cfargument name="idOldeqp"		required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT1"		required="true" type="any" />
		<cfargument name="ID_EXT2"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cftry>
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111.replaceEquipement_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="2"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idOldeqp#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idNeweqp#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="1"/>
				<cfprocparam  variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			<cfset result = p_result/>
		<cfcatch type="any" >
			<cflog text="#cfcatch.Message# : #cfcatch.Detail#" >
			<cfset result = -10/>
		</cfcatch>
		</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset result1 = logger.logAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT1,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
				<!--- <cfset result1 = logger.logAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT2,0,IDSOUS_TETE,uid_action,DATE_EFFET)/> --->
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result1 = logger.sendMailAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT1,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
				<!--- <cfset result1 = logger.sendMailAction(21,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT2,0,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/> --->
			</cfcatch>
			</cftry>
		</cfif>	

		<cfreturn result>
	</cffunction>

	<cffunction name = "echangerSim2Manually" access="remote" returntype="numeric">
		<cfargument name="objEchangeSim" 	required="true" type="Struct" />
		<cfargument name="objCreateSim"		required="true" type="Struct" />

		<cfset objFicheTerminal = createObject('component','fr.consotel.consoview.M111.fiches.FicheTerminal')>
		
		<cfset idNewEqpt = objFicheTerminal.setInfosFicheTerminal(	-1,		<!---idEquipement--->
																	#objCreateSim.IDEQUIPEMENT_FOURNISSEUR#,	<!---cbModele.selectedItem.IDEQUIPEMENT_FOURNISSEUR--->
																	#objCreateSim.IDREVENDEUR#, 	<!---cbModele.selectedItem.IDREVENDEUR--->
																	#objCreateSim.IDFOURNISSEUR#,	<!---cbMarque.selectedItem.IDFOURNISSEUR--->
																	71, 	<!--- idTypeEquipement--->
																	0,		<!---idCommande --->
																	0,		<!---isNotSameImei--->
																	#objCreateSim.NEW_SIM#,<!--- le Numéro de SIM SAISIE--->
																	0,		<!---isNotSameNumSerie--->
																	"",		<!---numSerieEquipement--->
																	"",		<!---dateAchat --->
																	0,		<!---Prix--->
																	"",		<!---numCommande--->
																	"",		<!---dateLivraison--->
																	"", 	<!---refLivraison --->
																	"SIM saisie hors parc puis crée",<!---commentaire--->
																	-1, 	<!---idSite--->
																	-1,		<!---idEmplacement--->
																	#objCreateSim.LIBELLE_EQ#, <!---cbModele.selectedItem.LIBELLE_EQ--->
																	"",			<!---refConst--->
																	"", 		<!---refRevendeur--->
																	0,			<!---ID --->
																	#objCreateSim.IDPOOL#, 	<!---IdPool--->
																	1,			<!---niveau --->
																	0,			<!---insertPoolDistrib--->
																	0,			<!---insertPoolSociete--->
																	0)>			<!---BYOD--->


		<cfset idSimInPool = getSimIdInPool(idNewEqpt,objCreateSim.IDPOOL)>
		
		<cftry>
			<cfif idSimInPool GT 0 >
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111.replaceEquipement_v2">
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="2"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#objEchangeSim.idOldeqp#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idSimInPool#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="1"/>
					<cfprocparam  variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
				<cfset result = p_result/>
			<cfelse>
				<cfset result = -10/>
			</cfif>
		<cfcatch type="any" >
			<cflog text="#cfcatch.Message# : #cfcatch.Detail#" >
			<cfset result = -10/>
		</cfcatch>
		</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset result1 = logger.logAction(21,1,objEchangeSim.ITEM1,objEchangeSim.ITEM2,objEchangeSim.ID_TERMINAL,objEchangeSim.ID_SIM1,objEchangeSim.ID_EMPLOYE,objEchangeSim.IDSOUS_TETE,uid_action,objEchangeSim.DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result1 = logger.sendMailAction(21,1,objEchangeSim.ITEM1,objEchangeSim.ITEM2,objEchangeSim.ID_TERMINAL,objEchangeSim.ID_SIM1,objEchangeSim.ID_EMPLOYE,objEchangeSim.IDSOUS_TETE,uid_action,objEchangeSim.DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>	
		<cfreturn result>
	</cffunction>

	<cffunction name = "getSimIdInPool" access="remote" returntype="any">
		<cfargument name="p_idsim" 		required="true" type="Numeric" />
		<cfargument name="p_idpool"		required="true" type="Numeric" />

		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.getSimIdInPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#p_idsim#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#p_idpool#"/>
			
			<cfprocresult name = "q_retour">
		</cfstoredproc>
		<cfif q_retour.recordcount GT 0>
			 <cfset p_retour = q_retour["ID"][1]>
		<cfelse>
			<cfset p_retout = 0>
		</cfif>
				
		<cfreturn p_retour>
	</cffunction>
		
</cfcomponent>