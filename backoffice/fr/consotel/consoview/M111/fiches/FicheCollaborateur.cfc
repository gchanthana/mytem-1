﻿<cfcomponent>

	<cfset init()>
	
	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
	</cffunction>
	
	<!--- ----------------- DEBUT COLLABORATEUR ----------------- --->

	<!--- 
		Fournis les infos de la fiche collaborateur.
	 --->
	<cffunction name="getInfosFicheCollaborateur" access="remote" returntype="Array" hint="Fournis les infos de la fiche collaborateur.">
		<cfargument name="idEmploye" 	required="true" type="numeric"/>
		<cfargument name="idPool" 		required="true" type="numeric"/>
		<cfargument name="id" 		required="true" type="numeric"/>
		

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.getfiche_collaborateur_v3">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEmploye#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="0">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idPool#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#id#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLangue#">

			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
						
			<cfprocresult name="employe" 				resultset="1">
			<cfprocresult name="equipement" 			resultset="2">
			<cfprocresult name="orga_cus" 				resultset="3">
			<cfprocresult name="p_cur_orga_ope" 		resultset="4">
			
		</cfstoredproc>
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = p_result >
		<cfset p_retour[2] = employe >
		<cfset p_retour[3] = equipement >
		<cfset p_retour[4] = orga_cus >
		<cfset p_retour[5] = p_cur_orga_ope >
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		Mise à jour des infos de la fiche collaborateur.
	--->
	<cffunction name="setInfosFicheCollaborateur" access="remote" returntype="array" output="false" hint="Mise à jour des infos de la fiche collaborateur.">
		<cfargument name="idGpeClient"		required="true" type="numeric" />		
		<cfargument name="idEmploye"		required="true" type="numeric" />		
		<cfargument name="idSite" 			required="true" type="numeric" />		
		<cfargument name="cleIdentifiant" 	required="true" type="String" />		
		<cfargument name="idCivilite" 		required="true" type="numeric" />		
		<cfargument name="nom" 				required="true" type="String" />		
		<cfargument name="prenom" 			required="true" type="String" />		
		<cfargument name="email" 			required="true" type="String" />		
		<cfargument name="fonction" 		required="true" type="String" />		
		<cfargument name="statut" 			required="true" type="String" />		
		<cfargument name="commentaires" 	required="true" type="String" />		
		<cfargument name="codeInterne" 		required="true" type="String" />		
		<cfargument name="reference" 		required="true" type="String" />		
		<cfargument name="matricule" 		required="true" type="String" />		
		<cfargument name="dansSociete" 		required="true" type="numeric" />		
		<cfargument name="dateEntree" 		required="true" type="String" />		
		<cfargument name="dateSortie" 		required="true" type="String" />		
		<cfargument name="texteChampPerso1" required="true" type="String" />		
		<cfargument name="texteChampPerso2" required="true" type="String" />		
		<cfargument name="texteChampPerso3" required="true" type="String" />		
		<cfargument name="texteChampPerso4" required="true" type="String" />		
		<cfargument name="niveau" 			required="true" type="numeric" />		
		<cfargument name="xmlOrganisation" 	required="true" type="String" displayname="xml de l'organisation"/>		
		<cfargument name="id" 				required="true" type="numeric" />
		<cfargument name="idManager" 		required="false" type="numeric"/>
		
		<cfset chaineDebug="#idRacine#,#idGpeClient#,#idEmploye#,#idSite#,#idUser#,#cleIdentifiant#,
							#idCivilite#,#nom#,#prenom#,#email#,#fonction#,#statut#,#commentaires#,
							#codeInterne#,#reference#,#matricule#,#dansSociete#,#dateEntree#,#dateSortie#,
							#texteChampPerso1#,#texteChampPerso2#,#texteChampPerso3#,#texteChampPerso4#,#niveau#">
		
		<!--- <cfabort showError="#chaineDebug#">  --->
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m111.UPDATEEMPLOYE_v3">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGpeClient#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEmploye#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idSite#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idUser#" > <!--- correspond a app_LoginID --->
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cleIdentifiant#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCivilite#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#nom#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#prenom#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#email#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#fonction#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#statut#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#matricule#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#dansSociete#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateEntree#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateSortie#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso1#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso2#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso3#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso4#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#niveau#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	  value="#xmlOrganisation#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idManager#">
			
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retourUdpateEmploye">
			<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_mat_prenom">
			<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_mat_nom">
		</cfstoredproc>	
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = p_retourUdpateEmploye >
		<cfset p_retour[2] = p_mat_prenom >
		<cfset p_retour[3] = p_mat_nom >
		 
		<cfreturn p_retour>
		
	</cffunction>
	
	<!--- 
		Ajout d'un nouveau collaborateur.
	--->
	<cffunction name="addNewCollaborateur" access="remote" returntype="array" output="false" hint="Ajout d'un nouveau collaborateur.">
		<cfargument name="idPool" 			required="true" type="Numeric" />
		<cfargument name="idSite" 			required="true" type="numeric" />		
		<cfargument name="cleIdentifiant" 	required="true" type="String" />		
		<cfargument name="idCivilite" 		required="true" type="numeric" />		
		<cfargument name="nom" 				required="true" type="String" />		
		<cfargument name="prenom" 			required="true" type="String" />		
		<cfargument name="email" 			required="true" type="String" />		
		<cfargument name="fonction" 		required="true" type="String" />		
		<cfargument name="statut" 			required="true" type="String" />		
		<cfargument name="commentaires" 	required="true" type="String" />		
		<cfargument name="codeInterne" 		required="true" type="String" />		
		<cfargument name="reference" 		required="true" type="String" />		
		<cfargument name="matricule" 		required="true" type="String" />		
		<cfargument name="dansSociete" 		required="true" type="numeric" />		
		<cfargument name="dateEntree" 		required="true" type="String" />		
		<cfargument name="dateSortie" 		required="true" type="String" />		
		<cfargument name="texteChampPerso1" required="true" type="String" />		
		<cfargument name="texteChampPerso2" required="true" type="String" />		
		<cfargument name="texteChampPerso3" required="true" type="String" />		
		<cfargument name="texteChampPerso4" required="true" type="String" />		
		<cfargument name="niveau" 			required="true" type="numeric" />		
		<cfargument name="xmlOrganisation" 	required="true" type="String" displayname="xml de l'organisation"/>		
		<cfargument name="insertPoolDistrib" 	required="true" type="numeric"/>
		<cfargument name="insertPoolSociete" 	required="true" type="numeric"/>
		
		<cfset chaineDebug="#idRacine#,#idpool#,#idSite#,#idUser#,#cleIdentifiant#,#idCivilite#,
							#nom#,#prenom#,#email#,#fonction#,#statut#,#commentaires#,#codeInterne#,
							#reference#,#matricule#,#dansSociete#,#dateEntree#,#dateSortie#,#texteChampPerso1#,
							#texteChampPerso2#,#texteChampPerso3#,#texteChampPerso4#,#niveau#">
		
		<!--- <cfabort showError="#chaineDebug#">  --->
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m111.InsertEmploye_v3">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idpool#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idSite#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idUser#" > <!--- correspond a app_LoginID --->
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cleIdentifiant#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCivilite#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#nom#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#prenom#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#email#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#fonction#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#statut#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#matricule#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#dansSociete#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateEntree#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateSortie#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso1#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso2#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso3#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso4#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#niveau#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	  value="#xmlOrganisation#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#insertPoolDistrib#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#insertPoolSociete#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retourUdpateEmploye">
			<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_mat_prenom">
			<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_mat_nom">
		</cfstoredproc>
		 
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = p_retourUdpateEmploye >
		<cfset p_retour[2] = p_mat_prenom >
		<cfset p_retour[3] = p_mat_nom >
		 
		<cfreturn p_retour>
		
	</cffunction>
	
	<!--- 
		Fournis la liste des infos sur les liaisons Ligne-Sim-Terminal associé a un collaborateur
	 --->
	<cffunction name="getListLigneSimEqptFromCollab" access="remote" returntype="Array" hint="Fournis la liste des infos sur les liaisons Ligne-Sim-Terminal associé a un collaborateur">
		<cfargument name="idEmploye" 	required="true" type="numeric"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.getListLigneSimEqptFromCollab">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEmploye#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
						
			<cfprocresult name="infos" 			resultset="1">
		</cfstoredproc>
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = p_result >
		<cfset p_retour[2] = infos >
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		Fournis la liste des Equipements associés a un collaborateur
	 --->
	<cffunction name="getListEqptFromCollab" access="remote" returntype="Array" hint="Fournis la liste des Equipements associés a un collaborateur">
		<cfargument name="idEmploye" 	required="true" type="numeric"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.getListEqptFromCollab">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEmploye#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
						
			<cfprocresult name="infos" 			resultset="1">
		</cfstoredproc>
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = p_result >
		<cfset p_retour[2] = infos >
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		Fournis la liste des Equipements associés a un collaborateur
	 --->
	<cffunction name="getAffectedEqpToEmp" access="remote" returntype="Array" hint="Fournis la liste des Equipements associés a un collaborateur">
		<cfargument name="groupeIndex" 	required="true" type="numeric"/>
		<cfargument name="idEmploye" 	required="true" type="numeric"/>
		<cfargument name="idpool" 	required="true" type="numeric"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.getAffectedEqpToEmp">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEmploye#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
						
			<cfprocresult name="infos" 			resultset="1">
		</cfstoredproc>
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = p_result >
		<cfset p_retour[2] = infos >
			
		<cfreturn p_retour>
	</cffunction>
	<!--- 
		Fournis la liste des Equipements, sim et ligne associés a un collaborateur dans le cadre d'un départ de l'entreprise
	 --->
	<cffunction name="initdepartEntreprise" access="remote" returntype="Any" hint="Fournis la liste des Equipements, sim et ligne associés a un collaborateur dans le cadre d'un départ de l'entreprise">
		<cfargument name="id" 	required="true" type="numeric"/>
		<cfargument name="idpool" 	required="true" type="numeric"/>
		
		<cfset initDataStruct = structNew()>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.Liste_terminaux_emp_v3">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#id#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idpool#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			<cfset initDataStruct.EQP = p_retour>
	
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.Liste_SIM_emp">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#id#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idpool#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			<cfset initDataStruct.SIM = p_retour>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.Liste_terminaux_emp_fixe">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#id#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idpool#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			<cfset initDataStruct.EQPFIXE = p_retour>
	
	<!---	<cfthread action="run" name="ligne">
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.Liste_Usage">
				<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			<cfset listeUsage.RESULT = p_retour>
		</cfthread>
	--->
		
			<cfreturn initDataStruct>
	</cffunction>
	<cffunction name="validerdepartEntreprise" access="remote" returntype="Numeric" hint="Fournis la liste des Equipements, sim et ligne associés a un collaborateur dans le cadre d'un départ de l'entreprise">
		<cfargument name="idpool" 		required="true" type="numeric"/>
		<cfargument name="xml" 			required="true" type="string"/>
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		<cftry>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.putOutEmploye_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#ID_EMPLOYE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#DATE_EFFET#">
				<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" value="#xml#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idpool#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
			</cfstoredproc>
			<cfset result = p_result>
		<cfcatch type="any">
			<cfset result = -10>
		</cfcatch>
		</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset result1 = logger.logAction(15,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result1 = logger.sendMailAction(15,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>	
		
		<cfreturn result>
	</cffunction>
	<cffunction name="insertModeleRevendeur" returntype="Numeric" access="remote">
		<cfargument name="idEquipConstructeur" type="Numeric">
		<cfargument name="idrevendeur" type="Numeric">
			
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.copyEq_fournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipConstructeur#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="" null="true">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		Fournis si oui ou non le matricul est auto et si oui alors recupération de celui ci
	 --->
	<cffunction name="getMatriculeAuto" access="remote" returntype="Struct" output="false" hint="MATRICULE AUTO">
			
		<cfset idracine = session.perimetre.id_groupe>
		<cfset matriculeStrcut 				 = structnew()>
		<cfset matriculeStrcut.PREFIXE 		 = ''>
		<cfset matriculeStrcut.CURRENT_VALUE = ''>
		<cfset matriculeStrcut.LAST_VALUE 	 = 0>
		<cfset matriculeStrcut.IDRACINE 	 = idracine>
		<cfset matriculeStrcut.IS_ACTIF 	 = 0>
		<cfset matriculeStrcut.NB_DIGIT 	 = 0>
		
		<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m21.getgestautomatinfo">
			<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfif p_retour.RecordCount gt 0>				 
			<cfset matriculeStrcut.PREFIXE 		 = p_retour.PREFIXE>				
			<cfset matriculeStrcut.LAST_VALUE 	 = p_retour.LAST_VALUE>
			<cfset matriculeStrcut.IS_ACTIF 	 = p_retour.IS_ACTIF>
			<cfset matriculeStrcut.NB_DIGIT 	 = p_retour.NB_DIGIT>

			<cfif matriculeStrcut.IS_ACTIF gt 0>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m21.getmatricule">
					<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">
					<cfprocparam  type="Out" cfsqltype="CF_SQL_VARCHAR" variable="last_matricule">
				</cfstoredproc>
				<cfset matriculeStrcut.CURRENT_VALUE = last_matricule>
			</cfif>
		</cfif>
		<cfreturn matriculeStrcut>
	</cffunction>

<!--- Fournir la liste des managers  d'une racine donnée --->
	<cffunction name="getListManagers" access="remote" returntype="query" hint="Fournis la liste des managers pour une racine donnée">
		<cfargument name="indexDebut" 	required="true" type="Numeric" default="1">
		<cfargument name="nbItem" 		required="true" type="Numeric" default="10">
		<cfargument name="search" 		required="true" type="String">
		
		<cftry>
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.getListManager" cachedWithin="#CreateTimeSpan(0,1,0,0)#">
				<cfprocparam  value="#idRacine#" 		cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#search#" 			cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#indexDebut#" 		cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#nbItem#" 			cfsqltype="CF_SQL_INTEGER" type="in">
				
				<cfprocresult name="result">
			</cfstoredproc>
			
			<cfreturn result/> 
		
			<cfcatch type="any" >
			<cfmail from="mnt@saaswedo.com" subject="CFCATCH M111 FicheCollaborateur.getManagers" to="mnt@saaswedo.com" type="html" >
				<b> CFCATCH :  </b><br/> <cfdump var="#CFCATCH#" >
			</cfmail>
		</cfcatch>
		</cftry>
		
	</cffunction>

	<cffunction name="getValeursChampPersos" access="remote" returntype="Any" hint="Fournis la liste valeurs par champ perso">
		<cfargument name="idPool" 	required="true" type="Numeric" >
		<cfargument name="champs" 	required="true" type="String" >
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.get_cX_employe">
			<cfprocparam  value="#idRacine#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idPool#" 			cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#champs#" 			cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour/>		
	</cffunction>
	
	<!--- ----------------- FIN COLLABORATEUR ----------------- --->
</cfcomponent>