﻿<cfcomponent>

	<cfset init()>
	
	<cffunction name="init" hint="initialisation des  variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
	</cffunction>

	<!--- ----------------- DEBUT TERMINAL ----------------- --->
	
	<!--- 
		Fournis les infos de la fiche équipement (=terminal).
	 --->
	<cffunction name="getInfosFicheTerminal" access="remote" returntype="Array" hint="Fournis les infos de la fiche équipement (=terminal).">
		
		<cfargument name="idTerminal" 	required="true" type="numeric"/>
		<cfargument name="segment" 	required="true" type="numeric"/>
		<cfargument name="idpool" 	required="true" type="numeric"/>
		<cfargument name="id" 	required="true" type="numeric"/>
		
		<!--- Pour les tests : [FACTICE Désactivé par -1] Fiche Terminal pour __DEMO-CONSOTEL --->
		<cfif session.perimetre.ID_GROUPE EQ -1>
			<!---<cfreturn createObject("component","fr.saaswedo.api.tests.MYT58").getFicheTerminal(ARGUMENTS.idTerminal)>--->
			<cfthrow type="Custom" message="ID_GROUPE is negative">
		</cfif>
		<!--- Pour tous les autres groupes le fonctionnement reste inchangé --->

		<cfthread action="run" name="ficheEquipement"
					idTerminal="#idTerminal#" idRacine="#idRacine#" idLangue="#idLangue#" segment="#segment#" idpool="#idpool#" id="#id#" >
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.getfiche_equipement_v4">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idTerminal#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idLangue#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#segment#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idpool#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#id#">
				
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
				<cfprocresult name="terminal" 	resultset="1">
				<cfprocresult name="lignes" 	resultset="2">
				<cfprocresult name="ticket" 	resultset="3">
				<cfprocresult name="contrat" 	resultset="4">
				<cfprocresult name="orga" 		resultset="5">
			</cfstoredproc>
			
			<cfset ficheEquipement.result = p_result >
			<cfset ficheEquipement.terminal = terminal >
			<cfset ficheEquipement.lignes = lignes >
			<cfset ficheEquipement.ticket = ticket >
			<cfset ficheEquipement.contrat = contrat >
			<cfset ficheEquipement.orga = orga >
			
		</cfthread>
		
		<cfthread action="run" name="listeSousEqpOfTerm" idpool="#idpool#" idTerminal="#idTerminal#" >
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.listeSousEqpOfTerm">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idpool#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idTerminal#">
				
				<cfprocresult name="p_retour" >
			</cfstoredproc>
			
			<cfset listeSousEqpOfTerm.retour = p_retour>
			
		</cfthread>
		
		<cfthread action="join" name="ficheEquipement,listeSousEqpOfTerm" timeout="20000"/> 
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = ficheEquipement.result >
		<cfset p_retour[2] = ficheEquipement.terminal >
		<cfset p_retour[3] = ficheEquipement.lignes >
		<cfset p_retour[4] = ficheEquipement.ticket >
		<cfset p_retour[5] = ficheEquipement.contrat >
		<cfset p_retour[6] = listeSousEqpOfTerm.retour >
		<cfset p_retour[7] = ficheEquipement.orga >
		
		<cfreturn p_retour>
		
	</cffunction>

	
	<!--- 
		Mise à jour des infos de la fiche équipement.
	--->
	<cffunction name="setInfosFicheTerminal" access="remote" returntype="numeric" output="false" hint="Mise à jour des infos de la fiche équipement.">
		<cfargument name="idTerminal"			required="true" type="numeric" />		
		<cfargument name="idEqptFournis" 		required="true" type="numeric" />		
		<cfargument name="idRevendeur"	 		required="true" type="numeric" />		
		<cfargument name="idFabriquant" 		required="true" type="numeric" />		
		<cfargument name="idTypeEqpt"	 		required="true" type="numeric" />		
		<cfargument name="idCommande" 			required="true" type="numeric" />		
		<cfargument name="isNotSameImei" 		required="true" type="numeric" />	
		<cfargument name="libelleEqpt"	 		required="true" type="String" />
		<cfargument name="isNotSameNumSerie" 	required="true" type="numeric" />	
		<cfargument name="libelleNumSerie"		required="true" type="String" />		
		<cfargument name="dateAchat" 			required="true" type="String" />		
		<cfargument name="prix" 				required="true" type="numeric" />		
		<cfargument name="numCommande" 			required="true" type="String" />		
		<cfargument name="dateLivraison" 		required="true" type="String" />		
		<cfargument name="refLivraison" 		required="true" type="String" />		
		<cfargument name="commentaires" 		required="true" type="String" />		
		<cfargument name="idSite" 				required="true" type="numeric" />		
		<cfargument name="idEmplacement"		required="true" type="numeric" />
		<cfargument name="libEqptFournis"		required="true" type="String" />				
		<cfargument name="refConst"				required="true" type="String" />		
		<cfargument name="refRevendeur"			required="true" type="String" />
		<cfargument name="id" 					required="true" type="numeric"/>
		<cfargument name="idpool" 				required="true" type="numeric"/>
		<cfargument name="niveau_term" 			required="true" type="numeric"/>	
		<cfargument name="insertPoolDistrib" 	required="true" type="numeric"/>
		<cfargument name="insertPoolSociete" 	required="true" type="numeric"/>
		<cfargument name="byod"					required="true" type="numeric"/><!--- 1 ou 0 --->
		
		<cfset chaineDebug="#idTerminal#,#idEqptFournis#,#idRevendeur#,#idFabriquant#,
							#idTypeEqpt#,#idRacine#,#idCommande#,#isNotSameImei#,
							#libelleEqpt#,#dateAchat#,#prix#,#numCommande#,#dateLivraison#,
							#refLivraison#,#commentaires#,#idSite#,#idEmplacement#
							,#libEqptFournis#,#refConst#,#refRevendeur#,#id#,#idpool#,#niveau_term#,#SESSION.USER.CLIENTACCESSID#,
							#insertPoolDistrib#,#insertPoolSociete#,#isNotSameNumSerie#,#libelleNumSerie#,#byod#">

		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_M111B.update_fiche_equipement_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTerminal#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEqptFournis#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idFabriquant#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypeEqpt#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCommande#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#isNotSameImei#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelleEqpt#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateAchat#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   value="#prix#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numCommande#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateLivraison#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#refLivraison#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEmplacement#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libEqptFournis#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#refConst#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#refRevendeur#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idpool#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#niveau_term#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#insertPoolDistrib#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#insertPoolSociete#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#isNotSameNumSerie#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelleNumSerie#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#byod#" >
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
			
		</cfstoredproc>
		
		<cfreturn p_result/>
	</cffunction>
	<!--- ----------------- FIN TERMINAL ----------------- --->
	
	<!--- ----------------- DEBUT CONTRAT ----------------- --->
	
	<!--- 
		Mise à jour des infos de la fiche contrat ou ajout d'un contrat.
	--->
	<cffunction name="setInfosContrat" access="remote" returntype="Array" output="false" hint="Mise à jour des infos de la fiche contrat ou ajout d'un contrat.">
		<cfargument name="idContrat"			required="true" type="numeric" />		
		<cfargument name="idTerminal"			required="true" type="numeric" />		
		<cfargument name="idTypeContrat" 		required="true" type="numeric" />		
		<cfargument name="idRevendeur"	 		required="true" type="numeric" />		
		<cfargument name="reference"	 		required="true" type="String" />		
		<cfargument name="dateSignature" 		required="true" type="String" />		
		<cfargument name="dateEcheance" 		required="true" type="String" />		
		<cfargument name="commentaires" 		required="true" type="String" />		
		<cfargument name="tarif" 				required="true" type="numeric" />		
		<cfargument name="dureeContrat"	 		required="true" type="numeric" />		
		<cfargument name="taciteReconduction" 	required="true" type="numeric" />		
		<cfargument name="dateDebut"	 		required="true" type="String" />		
		<cfargument name="dureePreavis" 		required="true" type="numeric" />		
		<cfargument name="tarifMensuel"			required="true" type="numeric" />		
		<cfargument name="dateResiliation" 		required="true" type="String" />		
		<cfargument name="dateRenouvellement"	required="true" type="String" />		
		<cfargument name="dateEligibilite" 		required="true" type="String" />		
		<cfargument name="dureeEligibilite" 	required="true" type="String" />		
		
		<cfset chaineDebug="#idContrat#,#idTerminal#,#idTypeContrat#,#idRevendeur#,
							#reference#,#dateSignature#,#dateEcheance#,#commentaires#,
							#tarif#,#dureeContrat#,#taciteReconduction#,#dateDebut#,
							#dureePreavis#,#tarifMensuel#,#dateResiliation#,#dateRenouvellement#,
							#idRacine#,#dateEligibilite#,#dureeEligibilite#,#idUser#">
		
		<!--- <cfabort showError="#chaineDebug#"> --->
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_M111B.setInfosContrat">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idContrat#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTerminal#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypeContrat#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateSignature#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateEcheance#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   value="#tarif#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#dureeContrat#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#taciteReconduction#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateDebut#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#dureePreavis#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   value="#tarifMensuel#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateResiliation#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateRenouvellement#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateEligibilite#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#dureeEligibilite#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#" >
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result_idContrat"/>
		</cfstoredproc>
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = p_result >
		<cfset p_retour[2] = p_result_idContrat >
			
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="setContratsToEqp" access="remote" returntype="Array" output="false" hint="Mise à jour des infos de la fiche contrat ou ajout d'un contrat.">
		<cfargument name="arrayContrat"	required="true" type="array" />
		<cfargument name="idEqp"		required="true" type="numeric" />		
		
		<cfset p_retour = ArrayNew(1)>
		<cfset idx = 1>
		
		<cfset arrLen=arraylen(arrayContrat)/>
		
		<cftry>
			<cfloop from="1" to="#arrLen#" index="i">		
				<cfset contrat=arrayContrat[i]>
				
				<cfif isDefined("contrat.DATESIGNATURE")>
					<cfset _DATESIGNATURE = #contrat.DATESIGNATURE#>
				<cfelse>
					<cfset _DATESIGNATURE = "">
				</cfif>
				
				<cfif isDefined("contrat.DATERENOUVELLEMENT")>
					<cfset _DATERENOUVELLEMENT = #contrat.DATERENOUVELLEMENT#>
				<cfelse>
					<cfset _DATERENOUVELLEMENT = "">
				</cfif>
				
				<cfif isDefined("contrat.DATERESILIATION")>
					<cfset _DATERESILIATION = #contrat.DATERESILIATION#>
				<cfelse>
					<cfset _DATERESILIATION = "">
				</cfif>
				
				<cfif isDefined("contrat.DATEELIGIBILITE")>
					<cfset _DATEELIGIBILITE = #contrat.DATEELIGIBILITE#>
				<cfelse>
					<cfset _DATEELIGIBILITE = "">
				</cfif>
			
				<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m111.setInfosContrat">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#contrat.IDCONTRAT#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEqp#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#contrat.IDTYPECONTRAT#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#contrat.IDREVENDEUR#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#contrat.REFCONTRAT#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#_DATESIGNATURE#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#contrat.DATEECHEANCE#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#contrat.COMMENTAIRES#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   value="#contrat.TARIF#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#contrat.DUREECONTRAT#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#contrat.TACITERECONDUCTION#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#contrat.DATEDEBUT#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#contrat.PREAVIS#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   value="#contrat.TARIFMENSUEL#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#_DATERESILIATION#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#_DATERENOUVELLEMENT#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#_DATEELIGIBILITE#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#contrat.DUREEELIGIBILITE#" >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#" >
					<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
					<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result_idContrat"/>
				</cfstoredproc>
				
				<cfset obj=structNew()>
				<cfset obj.result = p_result >
				<cfset obj.idcontrat = p_result_idContrat >
				<cfset p_retour[idx] = obj>
				<cfset idx = idx +1>
			</cfloop>	
		<cfcatch type="any" >
			<cfmail from="mnt@saaswedo.com" subject="[M111] CFCATCH_setContratsToEqp" to="mnt@saaswedo.com" type="text/html" >
				<cfdump var="#cfcatch#" label="CFCATCH">
			</cfmail>
		</cfcatch> 
		</cftry>

		<cfreturn p_retour>
	</cffunction>
	<!--- 
		Fournis la liste des infos sur les liaisons Ligne-Sim associé a un terminal
	 --->
	<cffunction name="getListLigneSimFromEqpt" access="remote" returntype="Array" hint="Fournis la liste des infos sur les liaisons Ligne-Sim associé a un terminal">
		<cfargument name="idEmploye" 	required="true" type="numeric"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.getListLigneSimFromEqpt">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEmploye#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
						
			<cfprocresult name="infos" 			resultset="1">
		</cfstoredproc>
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = p_result >
		<cfset p_retour[2] = infos >
			
		<cfreturn p_retour>
	</cffunction>	
	
	<!--- ----------------- FIN CONTRAT ----------------- --->
			
	<!--- ----------------- DEBUT SOUS EQPT ----------------- --->

	<!--- 
		Fournis la liste des sous equipements disponible
	 --->
	<cffunction name="getSousEqptDispo" access="remote" returntype="Array" hint="Fournis la liste des sous equipements disponible">
		<cfargument name="idType" 			required="true" type="numeric"/>
		<cfargument name="idCategorie" 		required="true" type="numeric"/>
		<cfargument name="numSerie" 		required="true" type="String"/>
		<cfargument name="idConstructeur" 	required="true" type="numeric"/>
		<cfargument name="idRevendeur" 		required="true" type="numeric"/>
		<cfargument name="idSite" 			required="true" type="numeric"/>
		<cfargument name="critereTri" 		required="true" type="numeric"/>
		<cfargument name="nbItem" 			required="true" type="numeric"/>
		<cfargument name="startItem" 		required="true" type="numeric"/>
		<cfargument name="idPool"	 		required="true" type="numeric"/>
		<cfargument name="segment" 			required="true" type="numeric"/> <!-- 1=mobile, 2=Fixe, 3=data -->
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.getSousEquipDispo">
 			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idType#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idCategorie#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#numSerie#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idConstructeur#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idSite#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#critereTri#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#nbItem#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#startItem#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idPool#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#segment#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLangue#">
			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
			<cfprocresult name="ssEqpt" 			resultset="1">
		</cfstoredproc>
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = p_result >
		<cfset p_retour[2] = ssEqpt >
			
		<cfreturn p_retour>
	</cffunction>	
	
	<!--- 
		Associe un equipement a un sous equipement
	 --->
	<cffunction name="associerEqptToSousEqpt" access="remote" returntype="numeric" hint="Associe un equipement a un sous equipement">
		<cfargument name="idEqpt" 			required="true" type="numeric"/>
		<cfargument name="idSsEqpt" 		required="true" type="numeric"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.associerEqptToSousEqpt">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEqpt#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idSsEqpt#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>

		</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>	
	
	<cffunction name="mettreAuRebut" access="remote" returntype="numeric">
		
		<cfargument name="idTerm"		required="true" type="Numeric" />
		<cfargument name="commentaire" 	required="true" type="String" />
		<cfargument name="id_cause" 	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111.putEquipRecycle">
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idTerm#"/>
					<cfprocparam  variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
				<cfset result = p_result/>

			<cfcatch type="any" >
				<cflog text="#cfcatch.Message# : #cfcatch.Detail#" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cfset uid_action = createUUID()/>

			<cftry>
				
				<cfthread action="run" name="th_log1" myArgs="#ARGUMENTS#" uid_action=#uid_action#>
					<cfset result1 = logger.logAction(16, 1, myArgs.ITEM1, "", myArgs.ID_TERMINAL, myArgs.ID_SIM, myArgs.ID_EMPLOYE, myArgs.IDSOUS_TETE, uid_action, myArgs.DATE_EFFET, myArgs.commentaire, myArgs.id_cause)/>
				</cfthread>
				<cfthread action="run" name="th_log2" myArgs="#ARGUMENTS#" uid_action=#uid_action#>
					<cfset result1 = logger.logAction(4, 1, myArgs.ITEM1, myArgs.ITEM2, myArgs.ID_TERMINAL, myArgs.ID_SIM, myArgs.ID_EMPLOYE, myArgs.IDSOUS_TETE, uid_action, myArgs.DATE_EFFET, myArgs.commentaire, myArgs.id_cause)/>
				</cfthread>
			<cfcatch type="any" >
				<cfthread action="run" name="th_log3">
					<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				</cfthread>
				<cfthread action="run" name="th_log4" myArgs=#ARGUMENTS# uid_action=#uid_action#>
					<cfset result1 = logger.sendMailAction(16,1, myArgs.ITEM1, "", myArgs.ID_TERMINAL, myArgs.ID_SIM, myArgs.ID_EMPLOYE, myArgs.IDSOUS_TETE,uid_action, myArgs.DATE_EFFET, ERROR, myArgs.commentaire, myArgs.id_cause)/>
				</cfthread>
				<cfthread action="run" name="th_log5" myArgs=#ARGUMENTS# uid_action=#uid_action#>
					<cfset result1 = logger.sendMailAction(4,1, myArgs.ITEM1, myArgs.ITEM2, myArgs.ID_TERMINAL, myArgs.ID_SIM, myArgs.ID_EMPLOYE, myArgs.IDSOUS_TETE,uid_action, myArgs.DATE_EFFET,ERROR, myArgs.commentaire, myArgs.id_cause)/>
				</cfthread>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cfreturn result>
	</cffunction>
	<cffunction name="echangerEqp" access="remote" returntype="numeric">
		<cfargument name="idOldeqp" required="true" type="Numeric" />
		<cfargument name="idNeweqp" required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL1"	required="true" type="Numeric" />
		<cfargument name="ID_TERMINAL2"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL1 : #ID_TERMINAL1#">
		<cflog text="ID_TERMINAL2 : #ID_TERMINAL2#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		<cftry>
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111.replaceEquipement_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="1"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idOldeqp#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idNeweqp#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="0"/>
				<cfprocparam  variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			<cfset result = p_result/>
		<cfcatch type="any" >
			<cflog text="#cfcatch.Message# : #cfcatch.Detail#" >
			<cfset result = -10/>
		</cfcatch>
		</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset result1 = logger.logAction(20,1,ITEM1,ITEM2,ID_TERMINAL1,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
				<!--- <cfset result1 = logger.logAction(20,1,ITEM1,ITEM2,ID_TERMINAL2,ID_SIM,0,IDSOUS_TETE,uid_action,DATE_EFFET)/> --->
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result1  = logger.sendMailAction(20,1,ITEM1,ITEM2,ID_TERMINAL1,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
				<!--- <cfset result1 = logger.sendMailAction(20,1,ITEM1,ITEM2,ID_TERMINAL2,ID_SIM,0,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/> --->
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getListeType" access="remote" returntype="Query" >
		<cfargument name="idcategorie" required="true" type="Numeric" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.getlistetype_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idLangue#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idcategorie#"/>
			
			<cfprocresult name="p_retour" >
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	
	<cffunction name="getListeModele" access="remote" returntype="query" 
	            hint="Fournis la liste des libellés perso">
		<cfargument name="idtype" type="numeric" required="true">
		<cfargument name="idmarque" type="numeric" required="true">
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.getListeModele">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idtype#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idmarque#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
	
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="getModeleHorsCatalogue" access="remote" returntype="query" 
	            hint="Fournis la liste des libellés perso">
		<cfargument name="idtype" type="numeric" required="true">
		<cfargument name="idmarque" type="numeric" required="true">
		<cfargument name="idDistrib" type="numeric" required="true">
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.getModeleHorsCat">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idtype#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idDistrib#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idmarque#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
	
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="addEquipementRevendeur" access="remote" returntype="struct" description="add equip in catalogue revendeur">
	      
	      <cfargument name="libelle" type="string" required="true">
	      <cfargument name="reference" type="string" required="true">
	      <cfargument name="reference_distrib" type="string" required="true">
	      <cfargument name="idConstructeur" type="numeric" required="true">
	      <cfargument name="idType" type="numeric" required="true">
	      <cfargument name="idRevendeur" type="numeric" required="true">
	      <cfargument name="insertInConstr" type="numeric" required="true">
	      <cfargument name="prixCat" type="numeric" required="true">
	      <cfargument name="tarif1" type="numeric" required="true">
	      <cfargument name="tarif2" type="numeric" required="true">
	      <cfargument name="tarif3" type="numeric" required="true">
	      <cfargument name="tarif4" type="numeric" required="true">
	      <cfargument name="tarif5" type="numeric" required="true">
	      <cfargument name="tarif6" type="numeric" required="true">
	      <cfargument name="tarif7" type="numeric" required="true">
	      <cfargument name="tarif8" type="numeric" required="true">
		    
	      	
	
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.addNewModele">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idConstructeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idType#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#prixCat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
	

	      <cfset myStruct=structNew()>
	      <cfset myStruct.IDEQUIP = p_retour>		  
	      <cfset myStruct.REF = "ADD">
	
	      <cfreturn myStruct>
	</cffunction>
	
	<cffunction name="insertModeleRevendeur" returntype="Any" access="remote">
		<cfargument name="idEquipConstructeur" type="Numeric">
		<cfargument name="idrevendeur" type="Numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111B.copyEq_fournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipConstructeur#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="" null="true">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_idequipDistrib">
		</cfstoredproc>
		
		<cfif p_idequipDistrib gt 0>
					
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111B.copyEq_client">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idequipDistrib#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="" null="true">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_idequipClient">
			</cfstoredproc>
			
		</cfif>
		
		<cfreturn p_idequipDistrib>
		 
	</cffunction>
	
	<cffunction name="getListeMarque" returntype="Query" access="remote">
		<cfargument name="idType" type="Numeric">
			
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111B.getListeMarqueWithType">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idType#" null="false">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>

	<!--- Mettre le statut d'equipement actif --->

	<cffunction name="mettreEquipActive" access="remote" returntype="Numeric" output="false">
		<cfargument name="idequip" 		required="true" type="numeric"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M111.putEquipActive">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idequipement" value="#idequip#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="addNewModele" returntype="Numeric" access="remote">
		<cfargument name="libelleModele" type="string" required="true">
		<cfargument name="reference" type="string" required="true">
		<cfargument name="referenceDistributeur" type="string" required="true">
		<cfargument name="idMarque" type="Numeric">
		<cfargument name="idTypeEquipement" type="Numeric">
		<cfargument name="idrevendeur" type="Numeric">
		<cfargument name="prix" type="Numeric">
			
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111.addNewModele">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelleModele#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idMarque#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypeEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#prix#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
</cfcomponent>