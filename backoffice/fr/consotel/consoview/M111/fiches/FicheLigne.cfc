﻿<cfcomponent>

	<cfset init()>

	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
	</cffunction>

	<!--- ----------------- DEBUT LIGNE ----------------- --->

	<!---
		Fournis les infos de la fiche ligne.
	 --->
	<cffunction name="getInfosFicheLigne" access="remote" returntype="Array" hint="Fournis les infos de la fiche ligne.">
		<cfargument name="idSousTete" 	required="true" type="numeric"/>
		<cfargument name="idGpeClient" 	required="true" type="numeric"/>
		<cfargument name="id" 			required="true" type="numeric"/>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.getfiche_ligne_v5">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idSousTete#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idGpeClient#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLangue#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#id#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="codeRetour"/>

			<cfprocresult name="ligne" 			resultset="1">
			<cfprocresult name="aboOpt"		 	resultset="2">
			<cfprocresult name="HistoRacco"		resultset="3">
			<cfprocresult name="orga" 			resultset="4">
			<cfprocresult name="Groupement"		resultset="5">
			<cfprocresult name="Tranches"		resultset="6">
		</cfstoredproc>

		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = codeRetour >
		<cfset p_retour[2] = ligne >
		<cfset p_retour[3] = aboOpt >
		<cfset p_retour[4] = HistoRacco >
		<cfset p_retour[5] = orga >
		<cfset p_retour[6] = Groupement >
		<cfset p_retour[7] = Tranches >
		<cfreturn p_retour>
	</cffunction>


	<!---
		Mise à jour des infos de la fiche ligne
	--->
	<cffunction name="setInfosFicheLigne" access="remote" returntype="numeric" output="false" hint="Mise à jour des infos de la fiche ligne">
		<cfargument name="idSousTete"				required="true" type="numeric" />
		<cfargument name="sousTete"					required="true" type="String" />
		<cfargument name="idOperateur"				required="true" type="numeric" />
		<cfargument name="idTypeCmd" 				required="true" type="numeric" />
		<cfargument name="idRevendeurForProfEqpt" 	required="true" type="numeric" />
		<cfargument name="idPool" 					required="true" type="numeric" />
		<cfargument name="idGpeClient" 				required="true" type="numeric" />
		<cfargument name="fonction" 				required="true" type="String" />
		<cfargument name="idCompte" 				required="true" type="numeric" />
		<cfargument name="idSousCompte" 			required="true" type="numeric" />
		<cfargument name="payeur" 					required="true" type="numeric" />
		<cfargument name="titulaire" 				required="true" type="numeric" />
		<cfargument name="typeLigne" 				required="true" type="String" />
		<cfargument name="idTypeLigne" 				required="true" type="numeric" />
		<cfargument name="idSite" 					required="true" type="numeric" />
		<cfargument name="idEmplacement"			required="true" type="numeric" />
		<cfargument name="numSim" 					required="true" type="String" />
		<cfargument name="pin1" 					required="true" type="String" />
		<cfargument name="pin2" 					required="true" type="String" />
		<cfargument name="puk1" 					required="true" type="String" />
		<cfargument name="puk2" 					required="true" type="String" />
		<cfargument name="texteChampPerso1" 		required="true" type="String" />
		<cfargument name="texteChampPerso2" 		required="true" type="String" />
		<cfargument name="texteChampPerso3" 		required="true" type="String" />
		<cfargument name="texteChampPerso4" 		required="true" type="String" />
		<cfargument name="idContrat" 				required="true" type="numeric" />
		<cfargument name="numContrat" 				required="true" type="String" />
		<cfargument name="dureeContrat" 			required="true" type="numeric" />
		<cfargument name="dateOuverture" 			required="true" type="String" />
		<cfargument name="dateRenouvellement" 		required="true" type="String" />
		<cfargument name="dateResiliation" 			required="true" type="String" />
		<cfargument name="dureeEligibilite" 		required="true" type="numeric" />
		<cfargument name="dateEligibilite" 			required="true" type="String" />
		<cfargument name="dateFPC" 					required="true" type="String" />
		<cfargument name="commentaires" 			required="true" type="String" />
		<cfargument name="xmlAboOpt" 				required="true" type="String" displayname="xml des abo et opt"/>
		<cfargument name="idEtat" 					required="true" type="numeric" />
		<cfargument name="xmlOrganisation" 			required="true" type="String" displayname="xml de l'organisation"/>
		<cfargument name="isLignePrincipale" 		required="true" type="numeric" />
		<cfargument name="isLigneBackUp" 			required="true" type="numeric" />
		<cfargument name="idOperateurPreselection" 	required="true" type="numeric" />
		<cfargument name="xmlCablage" 				required="true" type="String" displayname="xml du cablage"/>
		<cfargument name="insertPoolDistrib" 		required="true" type="numeric"/>
		<cfargument name="insertPoolSociete" 		required="true" type="numeric"/>
		<cfargument name="BOOLNUMBERHASCHANGED" 	required="false" type="any" default="0"/>
		<cfargument name="oldNumero" 				required="false" type="any" default="06-test"/>
		<cfargument name="codeRio" 					required="false" type="any" default=""/>
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_M111B.setInfos_ligne_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSousTete#" null="#iif((idSousTete eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#sousTete#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOperateur#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypeCmd#" null="#iif((idTypeCmd eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRevendeurForProfEqpt#" null="#iif((idRevendeurForProfEqpt eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGpeClient#" null="#iif((idGpeClient eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#fonction#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCompte#" null="#iif((idCompte eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSousCompte#" null="#iif((idSousCompte eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#payeur#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#titulaire#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#typeLigne#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypeLigne#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="#iif((idSite eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEmplacement#" null="#iif((idEmplacement eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numSim#" null="#iif((numSim eq ""), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#pin1#" null="#iif((pin1 eq ""), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#pin2#" null="#iif((pin2 eq ""), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#puk1#" null="#iif((puk1 eq ""), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#puk2#" null="#iif((puk2 eq ""), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso1#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso2#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso3#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso4#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idContrat#" null="#iif((idContrat eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numContrat#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#dureeContrat#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateOuverture#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateRenouvellement#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateResiliation#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#dureeEligibilite#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateEligibilite#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateFPC#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#"/>
 			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	  value="#xmlAboOpt#" null="#iif((xmlAboOpt eq ""), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEtat#"/>
 			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	  value="#xmlOrganisation#" null="#iif((xmlOrganisation eq ""), de("yes"), de("no"))#"/>
 			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#isLignePrincipale#"/>
 			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#isLigneBackUp#"/>
 			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOperateurPreselection#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	  value="#xmlCablage#" null="#iif((xmlCablage eq ""), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#insertPoolDistrib#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#insertPoolSociete#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeRio#"/>
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour"/>
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<!--- ----------------- FIN LIGNE ----------------- --->

	<!--- ----------------- DEBUT ABO ET OPTION D'UNE LIGNE ----------------- --->

	<!---
		Fournis la liste des abonnements et options pour un operateur
	 --->
	<cffunction name="fournirListeAbonnementsOptions" access="remote" returntype="Query" output="false" hint="Fournis la liste des abonnements et options pour un operateur">
		<cfargument name="idOperateur" 			required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 	required="true" type="numeric"/>
		<cfargument name="boolMobile" 			required="true" type="numeric"/>
		<cfargument name="boolFixe" 			required="true" type="numeric"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.Liste_Produits_profil_v3">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" 			value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" 			value="#idUser#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 		value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 			value="#codeLangue#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_mobile" 				value="#boolMobile#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_fixe" 				value="#boolFixe#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

			<!---<cfmail from="M111@consotel.fr" subject="Debug fournirListeAbonnementsOptions" to="olivier.boulard@consotel.fr"
					type="text/html" >
				<cfoutput>
					<br><br><br>

					<cfdump var="#idOperateur#"/><br>
					<cfdump var="#idUser#"/><br>
					<cfdump var="#idRacine#"/><br>
					<cfdump var="#idProfilEquipement#"/><br>
					<cfdump var="#codeLangue#"/><br>
					<cfdump var="#boolMobile#"/><br>
					<cfdump var="#boolFixe#"/><br>
					<br><br><br>

					<cfdump var="#p_retour#"/>

				</cfoutput>
			</cfmail>--->
		<cfreturn p_retour>
	</cffunction>


	<!---
		Ajout d'un produit (abonnement ou option) aux favoris de l'utilisateur
	 --->
	<cffunction name="ajouterProduitAMesFavoris" access="remote" returntype="numeric" output="false" hint="Ajout d'un produit (abonnement ou option) aux favoris de l'utilisateur" >
		<cfargument name="idProduitCatalogue" type="numeric" required="true">

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.ADDPRODFAVORI">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idRacine#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idUser#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idProduitCatalogue#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="out" variable="result">
		</cfstoredproc>
		<cfreturn result/>
	</cffunction>

	<!---
		Suppression d'un produit (abonnement ou option) des favoris de l'utilisateur
	 --->
	<cffunction name="supprimerProduitDeMesFavoris" access="remote" returntype="numeric" output="false" hint="Suppression d'un produit (abonnement ou option) des favoris de l'utilisateur" >
		<cfargument name="idProduitCatalogue" type="numeric" required="true">

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.DELPRODFAVORI">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idRacine#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idUser#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idProduitCatalogue#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="out" variable="result">
		</cfstoredproc>
		<cfreturn result/>
	</cffunction>

	<!---
		Pour l’historique d’un produit d’une ligne
	--->
	<cffunction name="getHistoriqueActionRessource" access="remote" returntype="query" output="false" hint="Pour l’historique d’un produit d’une ligne">
        <cfargument name="idressource" type="numeric" required="true">

        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.GETHISTORIQUE_RECHERCHE_v2">
             <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idinventaire_produit" value="#idressource#"/>
             <cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#codeLangue#"/>
             <cfprocresult name="p_result">
        </cfstoredproc>
        <cfreturn p_result/>
    </cffunction>

	<!---
		Pour recuperer les infos de la colonne facture dans abo et opt
	--->
	<cffunction name="majColFacture" access="remote" returntype="Array" output="false" hint="Pour recuperer les infos de la colonne facture dans abo et opt">
        <cfargument name="idSousTete" type="numeric" required="true">

        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m111.get_ligne_produit_abo_v2">
             <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#idRacine#"/>
             <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#idSousTete#"/>
             <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#idLangue#"/>
        	 <cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="codeRetour"/>

			<cfprocresult name="aboOpt" 			resultset="1">
		</cfstoredproc>

		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = codeRetour >
		<cfset p_retour[2] = aboOpt>

		<cfreturn p_retour>
    </cffunction>
	<cffunction name="resilierLigne" access="remote" returntype="query" output="false" hint="Résiliation d'une ligne">
        <cfargument name="idressource" type="numeric" required="true">

        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M111.resilierLigne">
             <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idinventaire_produit" value="#idressource#"/>
			 <cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="out" variable="p_retour"/>
        </cfstoredproc>
        <cfreturn 1/>
    </cffunction>
	<cffunction name="renumeroterLigne" access="remote" returntype="Numeric" output="false" hint="Renumerotation d’une ligne">
        <cfargument name="ID" type="numeric" required="true">
		<cfargument name="SOUSTETE" type="string" required="true">
		<cfargument name="oldNumero" type="string" required="true">

        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M111B.denumeroterLigne">
             <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#ID#"/>
			 <cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" value="#SOUSTETE#"/>
			 <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#idUser#"/>
			 <cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="out" variable="p_retour"/>
        </cfstoredproc>
        <cfset result = p_retour/>
        <cfif result gt -1>
	        <cftry>
				<cfset uid_action = createUUID()/>
				<cfset result1 = logger.logAction(30,1,oldNumero,SOUSTETE,0,0,0,p_retour,uid_action,now())/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result1  = logger.sendMailAction(30,1,oldNumero,sousTete,0,0,0,idSousTete,uid_action,now(),ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>

        <cfreturn result/>
    </cffunction>-- ----------------- FIN ABO ET OPTION D'UNE LIGNE ----------------- --->
	<cffunction name="getTeteLigneViaSouscompte" access="remote" returntype="query">
		<cfargument name="idsouscompte" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.Liste_Tetes_ligne">
			<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idsouscompte#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetTeteLigne">
		</cfstoredproc>
		<cfreturn qGetTeteLigne>
	</cffunction>


	<cffunction name="updateEtatLigne" access="remote" returntype="numeric">
		<cfargument name="sous_tete" type="string" required="true">
		<cfargument name="idsous_tete" type="numeric" required="true">
		<cfargument name="idetat" type="numeric" required="true">

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.updateEtatLigne">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in"  variable="p_idsous_tete" value="#arguments.idsous_tete#" >
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in"  variable="p_idetat" value="#arguments.idetat#" >
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in"  variable="p_userid" value="#variables.idUser#" >
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="out"  variable="p_retour"  >
		</cfstoredproc>

		<cfset args = {}>
		<cfif arguments.idetat EQ 1>
			<cfset args.ID_WORDS = 41> <!---LIGNE ACTIVÉE --->
		<cfelseif arguments.idetat EQ 2>
			<cfset args.ID_WORDS = 40> <!---LIGNE SUSPENDUE --->
		</cfif>
		
		
		<cfset args.ACTION_PRINCIPALE = 1>
		<cfset args.ITEM1 = arguments.sous_tete>
		<cfset args.ITEM2 = "">
		<cfset args.ID_TERMINAL = 0>
		<cfset args.ID_SIM = 0>
		<cfset args.ID_EMPLOYE = 0>
		<cfset args.IDSOUS_TETE = arguments.idsous_tete>
		<cfset args.UID_ACTION = createUUID()>
		<cfset args.DATE_EFFET = now()>
		<cfset args.COMMENTAIRE = "">
		
		<cfif p_retour gt -1>
				<cftry>
					<cfset result1 = logger.logAction(argumentcollection=args)/>
				<cfcatch type="any" >
					<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
					<cfset result1  = logger.sendMailAction(argumentcollection=args)/>
				</cfcatch>
			</cftry>
		</cfif>
		<cfif p_retour gt 0>
			<cfreturn arguments.idetat>
		</cfif>
		<cfreturn -1>
	</cffunction>

	<cffunction name="getListeDestinataires" access="remote" returntype="query" output="false" hint="Fournis la liste des destinataires (= liste des contacts distributeurs ayant un role facturation).">		
		<cfargument name="idrevendeur" required="true" type="numeric"/>
		<cfset user = Session.USER.CLIENTACCESSID>
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset role = 1>   <!--- role facturation --->
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_gestion_fournisseur_v1.getcontactsrolesrevendeur_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#user#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#role#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" >
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="sendEmailEchangeSimStock" access="remote" returntype="Numeric" output="false" hint="Fournis la liste des destinataires (= liste des contacts distributeurs ayant un role facturation).">		
		<cfargument name="infosMail" type="struct" required="true">
		<cfargument name="ok" type="Numeric" required="true"><!--- variable pour contourner un bug coldfusion--->
		
		<cfset var sujet = "[" & SESSION.PERIMETRE.GROUPE & "] Changement de carte Sim">
		<cfset var gestionnaire = SESSION.USER.NOM & " " & SESSION.USER.PRENOM>
		<cfset var groupe = SESSION.PERIMETRE.GROUPE>
		<cfset var destinataire = #infosMail.destinataire#>

		<cfset  send_msg = 0>
		<cfif infosMail.withAttachement EQ 1>
			<cfset var webPath 	= GetDirectoryFromPath( GetCurrentTemplatePath() )>
			<cfset var pathDirTMP 	= webPath & infosMail.attachement.fileUUID>
			<cfset servUpload = createObject('component','fr.consotel.consoview.M111.upload.UploaderFile')>
			<cfset send_msg = servUpload.uploadFile(infosMail.attachement, -1)>
		</cfif>
		<cfif send_msg EQ 1 >
					<cfmail spoolenable="no" to="#destinataire#" from="no-reply@mytem360.com" subject="#sujet#" type="html">
					
							Nous tenons à vous informer des changements suivants: <br><br>
							<b>Numéro de ligne : </b> #infosMail.SOUS_TETE#<br>
							<b>Nouveau carte SIM : </b>#infosMail.NEW_SIM#<br>
							<b>Ancienne carte SIM : </b>#infosMail.OLD_SIM#<br>
							<b>Gestionnaire de flotte local (GFL) :</b>#gestionnaire#<br> 
							<b>Périmètre : </b>#groupe#<br>
							<b>Pool : </b> #infosMail.POOL#<br><br>
							<cfmailparam file=#infosMail.attachement.localFilePath# >
							
							Cordialement<br>
							#gestionnaire#<br>
							#groupe#<br>
					</cfmail>
					<cfset send_msg = 2>
				
		<cfelse>
				<cfmail spoolenable="no" to="#destinataire#" from="no-reply@mytem360.com" subject="#sujet#" type="html">
						Nous tenons à vous informer des changements suivants: <br><br>
						<b>Numéro de ligne : </b> #infosMail.SOUS_TETE#<br>
						<b>Nouveau carte SIM : </b>#infosMail.NEW_SIM#<br>
						<b>Ancienne carte SIM : </b>#infosMail.OLD_SIM#<br>
						<b>Gestionnaire de flotte local (GFL) :</b>#gestionnaire#<br> 
						<b>Périmètre : </b>#groupe#<br>
						<b>Pool : </b> #infosMail.POOL#<br><br>
						
						Cordialement<br>
						#gestionnaire#<br>
						#groupe#<br>
				</cfmail>
		</cfif>
		
		<cfif send_msg EQ 2>
			<cfset deleteDirOk = servUpload.deleteDirectory(infosMail.attachement.localDirPath)>
		</cfif>
		<cfreturn 1>
	</cffunction>

</cfcomponent>
