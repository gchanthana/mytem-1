<cfcomponent name="createSousTete">

	<cffunction name="getGroupe" returntype="query">
		<cfargument name="filtreGroupe" type="array" required="false">
		<cfquery datasource="#session.offreDSN#" name="qGetGroupe">
			select libelle_groupe_client, idgroupe_client
			from groupe_client gc
			where gc.id_groupe_maitre is null
			AND lower(libelle_groupe_client) like lower('%#filtreGroupe[1]#%')
			order by lower(libelle_groupe_client) asc
		</cfquery>
		<cfreturn qGetGroupe>
	</cffunction>
	
	<cffunction name="getCR" returntype="query">
		<cfargument name="data" type="array" required="false">
		<cfquery datasource="#session.offreDSN#" name="qGetGroupe">
			select compte_facturation, sous_compte, idsous_compte
			from sous_compte sco, compte_facturation cf, groupe_client_ref_client gcrc
			where sco.idcompte_facturation=cf.idcompte_facturation
			AND cf.idref_client=gcrc.idref_client
			AND gcrc.idgroupe_client=#data[1]#
			AND lower(sous_compte) like lower('%#data[2]#%')
		</cfquery>
		<cfreturn qGetGroupe>
	</cffunction>
	
	<cffunction name="getSousTete" returntype="query">
		<cfargument name="data" type="array" required="false">
		<cfquery datasource="#session.offreDSN#" name="qGetGroupe">
			SELECT st.sous_tete, tl.libelle_type_ligne
			FROM sous_tete st, type_ligne tl
			WHERE st.idsous_compte=#data[1]#
			AND tl.idtype_ligne=st.Idtype_Ligne
		</cfquery>
		<cfreturn qGetGroupe>
	</cffunction>
	
	<cffunction name="getTypeLigne" returntype="query">
		<cfquery datasource="#session.offreDSN#" name="qGetGroupe">
			SELECT tl.libelle_type_ligne as label, tl.Idtype_Ligne as data
			FROM type_ligne tl
			ORDER BY lower(libelle_type_ligne)
		</cfquery>
		<cfquery dbtype="query" name="results">
			SELECT label AS label, data AS data
			FROM qGetGroupe
			ORDER BY label
		</cfquery>
		<cfreturn results>
	</cffunction>
	
	<cffunction name="createLigne" returntype="string">
		<cfargument name="data" type="array" required="false">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_ADMIN.CREATESOUSTETE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idgroupe_client" value="#data[1]#">		      
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idsous_compte" value="#data[2]#">		      
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_sous_tete" value="#data[3]#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="P_idtype_ligne" value="#data[4]#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
</cfcomponent>