﻿<cfcomponent name="GatewayService">
	
	<cfset dataSourceName = "BI_TEST">	
	
	
	<cffunction name="getGraphData" access="remote" returntype="Array" description="les donneés provenant de E0 pour les Graph de la page d accueil">
			
			
			<cfthread action="run" name="getData1">
				
				<cfset dataTable = "E0_AGGREGS_DEFRAG">
				<cfswitch expression="#SESSION.PERIMETRE.NIVEAU#">
					<cfcase value="A,B,C,D,E,F" delimiters=",">
						<cfset dataTable = "E0_AGGREGS_DEFRAG">
					</cfcase>
					<cfdefaultcase>
						<cfset dataTable = "E0_ENPROD_HIST">	
					</cfdefaultcase>
				</cfswitch>
				
				<cfquery datasource="#SESSION.OFFREDSN#" name="qPeriode">
					 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS  
				 	 FROM	PERIODE
				 	 WHERE	PERIODE.SHORT_MOIS=to_Char(#SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN#,'mm/yyyy')
				</cfquery>
				
				<cfquery datasource="#dataSourceName#" name="qQuery">				
					set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
									p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
									p_idorga=#SESSION.PERIMETRE.ID_ORGANISATION#,
									p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,
									p_idcliche=#SESSION.PERIMETRE.ID_LAST_CLICHE#,
									p_niveau='#SESSION.PERIMETRE.NIVEAU#',
									p_idniveau=#SESSION.PERIMETRE.ID_NIVEAU#,
									p_langue_pays='#SESSION.USER.GLOBALIZATION#';
					
					SELECT PERIODE.LIBELLE_PERIODE saw_10, FACTURATION.MONTANT saw_11, PERIODE.IDPERIODE_MOIS saw_12,PRODUIT1GROUPE.IDTYPE_THEME saw_14, PRODUIT1GROUPE.TYPE_THEME saw_13,PRODUIT1GROUPE.THEME saw_6 
					FROM #dataTable#
					WHERE PERIODE.IDPERIODE_MOIS BETWEEN #Evaluate(qPeriode.IDPERIODE_MOIS-12)# AND #qPeriode.IDPERIODE_MOIS# 
					AND PRODUIT1GROUPE.IDSEGMENT_THEME = 2
					ORDER BY saw_12
				</cfquery>
				
				<cfquery name="qQuery1" dbtype="query">
					select sum(saw_11) saw_11,saw_10,saw_12,saw_13 from qQuery group by saw_10,saw_12,saw_13  order by saw_12
				</cfquery>
				
				<cfset theDate=SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN>
				<cfif theDate gt now()>
						<cfset theDate = now()>		
				</cfif>
				
				<cfquery datasource="#SESSION.OFFREDSN#" name="qPeriode">
					 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS  
				 	 FROM	PERIODE
				 	 WHERE	PERIODE.SHORT_MOIS=to_Char(#theDate#,'mm/yyyy')
				</cfquery>
				
				<cfquery name="qQuery3" dbtype="query">
					select sum(saw_11) saw_0,saw_6 from qQuery 
					where saw_11 > 0
					and saw_12 = #qPeriode.idperiode_mois#
					and saw_14 = 1
					group by saw_6
				</cfquery>
				
				<cfset getData1.data1 = qQuery1 >
				<cfset getData1.data3 = qQuery3 >		
			</cfthread>
			
			<cfthread action="run" name="getData2">
				
				<cfquery datasource="#SESSION.OFFREDSN#" name="qPeriode">
					 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS  
				 	 FROM	PERIODE
				 	 WHERE	PERIODE.SHORT_MOIS=to_Char(#SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN#,'mm/yyyy')
				</cfquery>
				
				<cfquery datasource="#dataSourceName#" name="qQuery">	
					
					set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
									p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
									p_idorga=#SESSION.PERIMETRE.ID_ORGANISATION#,
									p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,
									p_niveau='#SESSION.PERIMETRE.NIVEAU#',
									p_idniveau=#SESSION.PERIMETRE.ID_NIVEAU#,
									p_langue_pays='#SESSION.USER.GLOBALIZATION#';
	
					SELECT ORGAOPERATEUR.OPERATEUR saw_0, 
								FACTURATION.NOMBRE_FACTURES saw_1, 
								PERIODE.DESC_MOIS saw_2, 
								PERIODE.SHORT_MOIS saw_3, 
								PERIODE.DESC_ANNEE saw_4, 
								PERIODE.IDPERIODE_MOIS saw_5
					FROM E0_AGGREGS_DEFRAG
					WHERE RCOUNT(PERIODE.IDPERIODE_MOIS) < 4
					AND ORGAOPERATEUR.OPERATEURID = 1438
					ORDER BY saw_5 DESC
				</cfquery>
	
				<cfquery name="q1" dbtype="query">
					SELECT saw_1,saw_0, saw_2,saw_5,saw_3 FROM qQuery GROUP BY saw_1,saw_0, saw_2,saw_5,saw_3 ORDER BY saw_5 asc
				</cfquery>	
					
					
				<cfset getData2.data2 = q1 >
				
			</cfthread>
			
			<cfthread action="join" name="getData1,getData2"/> 
			
			<cfset p_retour = ArrayNew(1)>
			<cfset p_retour[1] = getData1.data1 >
			<cfset p_retour[2] = getData2.data2 >
			<cfset p_retour[3] = getData1.data3 >
				
			<cfreturn p_retour>
			
	</cffunction>
	
	
</cfcomponent>
