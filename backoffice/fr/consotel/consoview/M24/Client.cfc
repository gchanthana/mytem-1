<cfcomponent name="Client">
<cffunction name="rechercheEquipementClient" access="remote" returntype="query">
		<cfargument name="SEARCHTEXTE" type="string" required="true">
		<cfargument name="ORDERBYTEXTE" type="string" required="true">
		<cfargument name="OFFSET" type="numeric" required="true">
		<cfargument name="LIMIT" type="numeric" required="true">
		<cfargument name="IDCONSTRUCTEUR" type="numeric" required="true">
		<cfargument name="IDTYPEEQUIPEMENT" type="numeric" required="true">
		<cfargument name="IDTYPECLASSE" type="numeric" required="true">
		<cfargument name="IDCLIENT" type="numeric" required="true">
		<cfargument name="IDREVENDEUR" type="numeric" required="true">
		<cfargument name="IDTYPECMD" type="numeric" required="true">
		<cfargument name="SEGMENT" type="numeric" required="false">
		<cfargument name="IDETAT" type="numeric" required="true"> 
		
		<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]> 
			
		<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
		<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
		<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
		
<!--- AJOUT du SEGMENT
		PROCEDURE searchEquipClient_v2( p_search_txt IN VARCHAR2, 
		p_search_col IN VARCHAR2,
		p_order_by IN VARCHAR2, 
		p_order_by_col IN VARCHAR2,
		p_start IN INTEGER, 
		p_limit IN INTEGER,
		p_idconst IN INTEGER, 
		p_type_eq IN INTEGER,
		p_idcategorie IN INTEGER, 
		p_idracine IN INTEGER,
		p_idrevendeur IN INTEGER,
		p_idtype_commande IN INTEGER,
		p_segment IN INTEGER DEFAULT 1,
		p_retour OUT SYS_REFCURSOR)
 --->
		
		<cfset _SEGMENT = 0>
		
		<cfif isDefined("SEGMENT")>
			<cfset _SEGMENT = SEGMENT>
		</cfif>
		
		<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.searchEquipClient_v4">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCONSTRUCTEUR#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPEEQUIPEMENT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPECLASSE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCLIENT#" null="false">	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDREVENDEUR#" null="false">	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPECMD#" null="false">	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_SEGMENT#" null="false">				
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idlang#" null="false">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#IDETAT#" null="false"  >
			
			<cfprocresult name="p_result">
		</cfstoredproc> 
	<cfreturn p_result>
</cffunction>

<cffunction name="removeEquClient" access="remote" returntype="any" description="remove equip in catalogue client ">
	<cfargument name="idEquip" type="numeric" required="true">
	 <cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.removeEquClient">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquip#" null="false">
	<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	
	<cfreturn p_retour>
</cffunction>
<cffunction name="IsEquipHaveCde" access="public" returntype="query" output="false" hint="" >
	<cfargument name="listeIDEquipement" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
	<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.IsEquipHaveCde">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeIDEquipement#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>
<cffunction name="updateTypeCmdEquipement" access="public" returntype="numeric" output="false" hint="" >
	<cfargument name="listeIDEquipement" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
	<cfargument name="idTypeCMD" required="false" type="numeric" default=""  />
	<cfargument name="action" required="false" type="numeric" default=""  />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.updateTypeCmdEquipement">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeIDEquipement#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypeCMD#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#action#">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>		
</cffunction>

<cffunction name="UpdateNiveauxEquip" access="public" returntype="numeric" output="false" hint="" >
	<cfargument name="idEquip" required="true" type="numeric"  />
	<cfargument name="niveau" required="true" type="numeric"  />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.UpdateNiveauEquip">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquip#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#niveau#">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>		
</cffunction>


<cffunction name="getRevendeur" access="public" returntype="query" output="false" hint="Lister revendeurs, g�n�riques et priv�s pour une racine donn�e. (Catalogue fournisseur)." >
	<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
	<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
	<cfargument name="app_loginID" required="false" type="numeric" default="" displayname="numeric app_loginID" hint="paramNotes" />
	<cfargument name="p_segment" required="false" type="numeric" default="0" displayname="numeric p_segment" hint="Initial value for the p_segmentproperty." />
 	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.get_catalogue_revendeur">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#app_loginID#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_segment#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.getDistribOfRacine">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
	<cfreturn p_result>		
</cffunction>
<cffunction name="getDistribOfRacine" access="remote" returntype="query" output="false" hint="Lister revendeurs, g�n�riques et priv�s pour une racine donn�e. (Catalogue fournisseur)." >
	<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
	
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.getDistribOfRacine">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
		
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>
<cffunction name="getAccessoire" access="public" returntype="query" output="false" hint="" >
	<cfargument name="listeIDEquipement" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
	<cfargument name="IdRevendeur" required="false" type="numeric"   />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.IsEquipHaveCde">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeIDEquipement#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdRevendeur#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>
<cffunction name="getCatModList" access="public" returntype="query" output="false" hint="" >
	<cfargument name="IdRevendeur" required="false" type="numeric"   />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.getCatModList">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdRevendeur#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>
<cffunction name="getCatModListAbo" access="public" returntype="query" output="false" hint="" >
	<cfargument name="IdRevendeur" required="false" type="numeric"   />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.getAboModList">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdRevendeur#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>

<cffunction name="getTypeCMDofClient" access="remote" returntype="query" output="false" hint="" >
	<cfargument name="idClient" required="false" type="numeric"   />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.getTypeCMDofClient">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idClient#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>

<cffunction name="searchEquipRevendeurImport" access="remote" returntype="query">
		<cfargument name="SEARCHTEXTE" type="string" required="true">
		<cfargument name="ORDERBYTEXTE" type="string" required="true">
		<cfargument name="OFFSET" type="numeric" required="true">
		<cfargument name="LIMIT" type="numeric" required="true">
		<cfargument name="IDCONSTRUCTEUR" type="numeric" required="true">
		<cfargument name="IDTYPEEQUIPEMENT" type="numeric" required="true">
		<cfargument name="IDTYPECLASSE" type="numeric" required="true">
		<cfargument name="IDREVENDEUR" type="numeric" required="true">
		<cfargument name="IDCLIENT" type="numeric" required="true">
		<cfargument name="SEGMENT" 			type="numeric" required="false">
				
		<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]> 
			
		<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
		<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
		<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
		
		<cfset _SEGMENT = 0>
		
		<cfif isDefined("SEGMENT")>
			<cfset _SEGMENT = SEGMENT>
		</cfif>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.searchEquipRevImport_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCONSTRUCTEUR#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPEEQUIPEMENT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPECLASSE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDREVENDEUR#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCLIENT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_SEGMENT#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc> 
	<cfreturn p_result>
</cffunction>

<cffunction name="updateEquipementClient" access="remote" returntype="any" description="update equip in catalogue cli">
	<cfargument name="id" type="numeric" required="true">
	<cfargument name="code_interne" type="string" required="true">
	<cfargument name="tarif1" type="numeric" required="true">
	<cfargument name="tarif2" type="numeric" required="true">
	<cfargument name="tarif3" type="numeric" required="true">
	<cfargument name="tarif4" type="numeric" required="true">
	<cfargument name="tarif5" type="numeric" required="true">
	<cfargument name="tarif6" type="numeric" required="true">
	<cfargument name="tarif7" type="numeric" required="true">
	<cfargument name="tarif8" type="numeric" required="true">
	<cfargument name="tarifcat" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.updateEquCLient">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#code_interne#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif1#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif2#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif3#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif4#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif5#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif6#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif7#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif8#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarifcat#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>

<cffunction name="updateEquipementClientV2" access="remote" returntype="any" description="update equip in catalogue cli">
	<cfargument name="id" type="numeric" required="true">
	<cfargument name="libelle" type="string"  required="true">
	<cfargument name="code_interne" type="string" required="true">
	<cfargument name="tarif1" type="numeric" required="true">
	<cfargument name="tarif2" type="numeric" required="true">
	<cfargument name="tarif3" type="numeric" required="true">
	<cfargument name="tarif4" type="numeric" required="true">
	<cfargument name="tarif5" type="numeric" required="true">
	<cfargument name="tarif6" type="numeric" required="true">
	<cfargument name="tarif7" type="numeric" required="true">
	<cfargument name="tarif8" type="numeric" required="true">
	<cfargument name="tarifcat" type="numeric" required="true">
	<cfargument name="dateValidite"	type="String" required="true">
	<cfargument name="boolSuspendu" type="numeric" required="true">	
	
	
	
	<cfset _libelle = "">
	<cfif isdefined("libelle")>
		<cfset _libelle = libelle>
	</cfif>
	
	<!---
	PROCEDURE updateEquipClient_v2(p_idequipement IN INTEGER,
	p_prix_cata IN NUMBER,
	p_prix_12 IN NUMBER,
	p_prix_24 IN NUMBER,
	p_prix_36 IN NUMBER,
	p_prix_48 IN NUMBER,
	p_prix_12_r IN NUMBER,
	p_prix_24_r IN NUMBER,
	p_prix_36_r IN NUMBER,
	p_prix_48_r IN NUMBER,
	p_libelle_eq IN VARCHAR2,
	p_retour OUT INTEGER);
	--->	
	
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m24.updateEquipClient_v3">		
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarifcat#" null="false">		
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif1#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif2#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif3#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif4#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif5#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif6#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif7#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif8#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#code_interne#" null="false">	
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#dateValidite#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#boolSuspendu#" null="false">	
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>

<cffunction name="getClientOfDistributeur" access="remote" returntype="any" description="Retourne les revendeurs du client">
	<cfargument name="idDistributeur" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.getClientOfDistributeur">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDistributeur#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>

<cffunction name="addXEquiClient" access="remote" returntype="numeric">
		<cfargument name="idClient" type="numeric" required="true">
		<cfargument name="tab_equip" type="any" required="true">
		<cfset len = arrayLen(tab_equip)>
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = addEquiClient(idClient,tab_equip[i].IDEQUIPEMENT)>
		</cfloop>
		<cfreturn r>
</cffunction>

<cffunction name="addEquiClient" access="remote" returntype="any" description="importe un equipement dans un catalogue revendeur">
	<cfargument name="idClient"  type="numeric" required="true">
	<cfargument name="idequipement" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.addEquipementClient">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idequipement#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#idClient#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_inTEGER" variable="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
</cfcomponent>
