<cfcomponent name="Constructeur">
<cffunction name="rechercheEquipementRevendeur" access="remote" returntype="query">
		<cfargument name="SEARCHTEXTE" type="string" required="true">
		<cfargument name="ORDERBYTEXTE" type="string" required="true">
		<cfargument name="OFFSET" type="numeric" required="true">
		<cfargument name="LIMIT" type="numeric" required="true">
		<cfargument name="IDCONSTRUCTEUR" type="numeric" required="true">
		<cfargument name="IDTYPEEQUIPEMENT" type="numeric" required="true">
		<cfargument name="IDTYPECLASSE" type="numeric" required="true">
		<cfargument name="IDREVENDEUR" type="numeric" required="true">
		<cfargument name="SEGMENT" 			type="numeric" required="false">
		<cfargument name="IDETAT" type="numeric" required="true"> 
		
		<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]> 
			
		<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
		<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
		<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
		
		<cfset _SEGMENT = 0>		
		<cfif isDefined("SEGMENT")>
			<cfset _SEGMENT = SEGMENT>
		</cfif>		
		
		<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.searchEquipRev_v4">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCONSTRUCTEUR#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPEEQUIPEMENT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPECLASSE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDREVENDEUR#" null="false">	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_SEGMENT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idlang#" null="false">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#IDETAT#" null="false"  >
			
			<cfprocresult name="p_result">
			
		</cfstoredproc> 
	<cfreturn p_result>
</cffunction>
<cffunction name="addXEquiConsRevendeur" access="remote" returntype="array">
		<cfargument name="idrevendeur" type="numeric" required="true">
		<cfargument name="tab_equip" type="any" required="true">
		
		<cfset len = arrayLen(tab_equip)>
		<cfset r = -1> 
		<cfloop index="i" from="1" to="#len#">
			<cfset r = addEquiConsRevendeur(idrevendeur,tab_equip[i].IDEQUIPEMENT)>
			<cfset tab_equip[i].IDEQUIPEMENT = r>
		</cfloop>
		<cfreturn tab_equip>
</cffunction>
<cffunction name="addEquiConsRevendeur" access="remote" returntype="any" description="importe un equipement dans un catalogue revendeur">
	<cfargument name="idrevendeur"  type="numeric" required="true">
	<cfargument name="idequipement" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.addEquiConsRevendeur">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idequipement#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#idrevendeur#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_inTEGER" variable="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<!--- <cffunction name="createCatalogueModele" access="remote" returntype="any" description="creer un catalogue modele">
	<cfargument name="idrevendeur"  type="numeric" required="true">
	<cfargument name="libelle" type="string" required="true">
	<cfargument name="idUser" type="numeric" required="true">
	<cfargument name="idRacine" type="numeric" required="true">
	<cfargument name="typeEquip" type="numeric" required="true">
	<cfargument name="typeAbo" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.createCatalogueModele">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#idUser#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#typeEquip#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#typeAbo#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction> --->


<cffunction name="createCatalogueClient" access="remote" returntype="any" description="creer un catalogue client">
	<cfargument name="idrevendeur"  type="numeric" required="true">
	<cfargument name="idClient" type="numeric" required="true">
	<cfargument name="idModele" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.createCatalogueClient">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#idClient#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#idModele#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_inTEGER" variable="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>



<cffunction name="addEquipementRevendeur" access="remote" returntype="struct" description="add equip in catalogue revendeur">	
	<cfargument name="libelle" type="string" required="true">
	<cfargument name="reference" type="string" required="true">
	<cfargument name="reference_distrib" type="string" required="true">
	<cfargument name="idConstructeur" type="numeric" required="true">
	<cfargument name="idType" type="numeric" required="true">
	<cfargument name="idRevendeur" type="numeric" required="true">
	<cfargument name="insertInConstr" type="numeric" required="true">		
	<cfargument name="prixCat" type="numeric" required="true">
	<cfargument name="tarif1" type="numeric" required="true">
	<cfargument name="tarif2" type="numeric" required="true">
	<cfargument name="tarif3" type="numeric" required="true">
	<cfargument name="tarif4" type="numeric" required="true">
	<cfargument name="tarif5" type="numeric" required="true">
	<cfargument name="tarif6" type="numeric" required="true">
	<cfargument name="tarif7" type="numeric" required="true">
	<cfargument name="tarif8" type="numeric" required="true">
	<cfargument name="dateValidite"	type="String" required="true">
	<cfargument name="delaiAlert" type="numeric" required="true">
	<cfargument name="boolSuspendu" type="numeric" required="true">
	<cfargument name="listClt" type="array" required="true">
	
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.addEquipementRevendeur_v2">
		
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#reference#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#reference_distrib#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idConstructeur#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idType#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#insertInConstr#" null="false">				
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#prixCat#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif1#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif2#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif3#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif4#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif5#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif6#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif7#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tarif8#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#dateValidite#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#delaiAlert#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#boolSuspendu#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#arraytoList(listClt,',')#" null="false">
		
		
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		<cfprocparam type="Out" cfsqltype="CF_SQL_vARCHAR" variable="ref">
	</cfstoredproc> 
	<cfset myStruct=structNew()>
	<cfset myStruct.IDEQUIP = p_retour>
 	<cfset myStruct.REF = ref>

	<cfreturn myStruct>
</cffunction>
<cffunction name="searchEquipConstrImport" access="remote" returntype="query">
		<cfargument name="SEARCHTEXTE" 		type="string" required="true">
		<cfargument name="ORDERBYTEXTE" 	type="string" required="true">
		<cfargument name="OFFSET" 			type="numeric" required="true">
		<cfargument name="LIMIT" 			type="numeric" required="true">
		<cfargument name="IDCONSTRUCTEUR" 	type="numeric" required="true">
		<cfargument name="IDTYPEEQUIPEMENT" type="numeric" required="true">
		<cfargument name="IDTYPECLASSE" 	type="numeric" required="true">
		<cfargument name="IDREVENDEUR" 		type="numeric" required="true">
		<cfargument name="SEGMENT" 			type="numeric" required="false">
		
		<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]> 
			
		<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
		<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
		<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
		
		<cfset _SEGMENT = 0>		
		<cfif isDefined("SEGMENT")>
			<cfset _SEGMENT = SEGMENT>
		</cfif>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.searchEquipConstrImport_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCONSTRUCTEUR#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPEEQUIPEMENT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPECLASSE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDREVENDEUR#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_SEGMENT#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc> 
	<cfreturn p_result>
</cffunction>

<cffunction name="updateEquipementRevendeur" access="remote" returntype="any" description="update equip in catalogue constructeur">
	<cfargument name="id" type="string" required="true">
	<cfargument name="libelle" type="string" required="true">
	<cfargument name="reference" type="string" required="true">
	<cfargument name="referenceRev" type="string" required="true">
	<cfargument name="idType" type="numeric" required="true">
	<cfargument name="tarif1" type="numeric" required="true">
	<cfargument name="tarif2" type="numeric" required="true">
	<cfargument name="tarif3" type="numeric" required="true">
	<cfargument name="tarif4" type="numeric" required="true">
	<cfargument name="tarif5" type="numeric" required="true">
	<cfargument name="tarif6" type="numeric" required="true">
	<cfargument name="tarif7" type="numeric" required="true">
	<cfargument name="tarif8" type="numeric" required="true">	
	<cfargument name="idConstructeur" type="numeric" required="true">
	<cfargument name="prixCat" type="numeric" required="true">
	<cfargument name="dateValidite"	type="String" required="true">
	<cfargument name="delaiAlert" type="numeric" required="true">
	<cfargument name="boolSuspendu" type="numeric" required="true">	
	<cfargument name="listClt" type="Array" required="true">
	
	
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M24.updateEquRevendeur_v2">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#reference#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#referenceRev#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idType#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif1#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif2#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif3#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif4#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif5#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif6#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif7#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif8#" null="false">		
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idConstructeur#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#prixCat#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#dateValidite#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#delaiAlert#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#boolSuspendu#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#arraytoList(listClt,',')#" null="false">
		
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>

<cffunction name="updateCompEquip" access="remote" returntype="any" >
	<cfargument name="IDEQUIP" type="string" required="true">
	<cfargument name="IDAC" type="numeric" required="true">
	<cfargument name="VAL" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M24.updateCompEquip">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#IDEQUIP#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDAC#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#VAL#" null="false">
	<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>

<cffunction name="getAccessoire" access="public" returntype="query" output="false" hint="" >
	<cfargument name="listeIDEquipement" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
	<cfargument name="IdRevendeur" required="false" type="numeric"   />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.IsEquipHaveCde">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeIDEquipement#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdRevendeur#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>
<cffunction name="lstAcclstEquip" access="public" returntype="query" output="false" hint="" >
	<cfargument name="idrevendeur" required="false" type="numeric" />
	<cfargument name="listeIDEquipement" required="false" type="string" displayname="string chaine" hint="paramNotes" />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.lstAcclstEquip">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeIDEquipement#">
		
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>

<cffunction name="getAllRevendeur" access="remote" returntype="any" description="Retourne les revendeurs du client">
	<cfargument name="IDGroupe_client" type="numeric" required="true">
	<cfargument name="search" type="string" required="true">
	<cfargument name="segment1" type="numeric" required="true">
	<!--- <cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M24.Get_all_revendeur">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vaRCHAR" value="#search#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#segment1#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result> --->
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.getDistribOfRacine">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>
</cfcomponent>
