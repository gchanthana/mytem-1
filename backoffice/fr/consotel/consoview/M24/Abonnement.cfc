<cfcomponent name="Abonnement">
<cffunction name="searchAboOperateur" access="remote" returntype="query">
		<cfargument name="SEARCHTEXTE" type="string" required="true">
		<cfargument name="ORDERBYTEXTE" type="string" required="true">
		<cfargument name="OFFSET" type="numeric" required="true">
		<cfargument name="LIMIT" type="numeric" required="true">
		<cfargument name="IDOP" type="numeric" required="true">

	<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]> 
			
		<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
		<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
		<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
		
		<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
		 
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.searchAboOperateur_v3">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDOP#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idlang#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc> 
	<cfreturn p_result>
</cffunction>
	
<cffunction name="searchAboOperateurImport" access="remote" returntype="query">
		<cfargument name="SEARCHTEXTE" type="string" required="true">
		<cfargument name="ORDERBYTEXTE" type="string" required="true">
		<cfargument name="OFFSET" type="numeric" required="true">
		<cfargument name="LIMIT" type="numeric" required="true">
		<cfargument name="IDOP" type="numeric" required="true">
		<cfargument name="IDCLIENT" type="numeric" required="true">
		
		<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]> 
		<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
		<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
		<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
		
		<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.searchAboOperateurImport_v3">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDOP#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCLIENT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idlang#" null="false">			
			<cfprocresult name="p_result">
		</cfstoredproc> 
	<cfreturn p_result>
</cffunction>

<cffunction name="removeAboClient" access="remote" returntype="any" description="remove abo in catalogue ">
	<cfargument name="idClient" type="numeric" required="true">
	<cfargument name="idAbo" type="numeric" required="true">
	
	 <cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.removeAboClient">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idClient#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idAbo#" null="false">
		
	<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="createCatalogProd" access="remote"  description="add equip in catalogue revendeur">
	<cfargument name="IDRACINE" type="numeric" required="true">
	<cfargument name="IDOPERATEUR" type="numeric" required="true">
	<cfargument name="IDMETHODE" type="numeric" required="true">
	<cfargument name="IDMODELE" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.createCatalogProd">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDRACINE#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDOPERATEUR#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDMETHODE#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDMODELE#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc> 
	<cfreturn p_retour>
</cffunction>
<cffunction name="getAboModList" access="public" returntype="query" output="false" hint="" >
	<cfargument name="IdRevendeur" required="false" type="numeric"   />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.getAboModList">
		
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdRevendeur#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>
<cffunction name="listOpDistrib" access="public" returntype="query" >
	<cfargument name="idRev" required="false" type="numeric" default="" displayname="numeric"  />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.listOpDistrib">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRev#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>
<cffunction name="listOpClient" access="public" returntype="query" >
	<cfargument name="idCli" required="false" type="numeric" default="" displayname="numeric"  />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.getOperateurOfRacine">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCli#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>
	<cffunction name="getListeOperateurAll" access="remote" returntype="Query" output="false" >
		<cfargument name="idgroupe" type="numeric" default="0">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_V3.SF_LISTEOP">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idgroupe#"/>
	        <cfprocresult name="listeFactures"/>        
		</cfstoredproc>
		<cfquery name="getListe" dbtype="query">
			select opnom as label, operateurID as data
			from listeFactures
		</cfquery>
		<cfreturn getListe>
	</cffunction>
<cffunction name="getTarifProduct" access="public" returntype="query" >
	<cfargument name="idProduit" required="false" type="numeric" default="" displayname="numeric" />
	<cfargument name="idRacine" required="false" type="numeric" default="" displayname="numeric" />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.getTarifProduct">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProduit#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>
<cffunction name="UpdateNiveauxAbo" access="public" returntype="numeric" output="false" hint="" >
	<cfargument name="idAbo" required="true" type="numeric"  />
	<cfargument name="idCLient" required="true" type="numeric"  />
	<cfargument name="niveau" required="true" type="numeric"  />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.UpdateNiveauProd">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idAbo#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCLient#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#niveau#">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>		
</cffunction>

	<cffunction name="addXAboClient" access="remote" returntype="numeric">
		<cfargument name="idClient" type="numeric" required="true">
		<cfargument name="lisIDabo" type="string"  required="true">
				
			<cfset r = -1>
			<cfset tab_abo = listToArray(lisIDabo)>
			<cfset len = arrayLen(tab_abo)>
			<cfloop index="i" from="1" to="#len#">
				<cfset r = addAboClient(idClient,val(tab_abo[i]))>
			</cfloop>
			
		<cfreturn r>
	</cffunction>


<cffunction name="addAboClient" access="remote" returntype="any" description="importe un equipement dans un catalogue revendeur">
	<cfargument name="idClient"  type="numeric" required="true">
	<cfargument name="idabo" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.addAbonnementClient">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idabo#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#idClient#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_inTEGER" variable="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="IsAboHaveCde" access="public" returntype="query" output="false" hint="" >
	<cfargument name="listeIDAbo" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
	<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.IsAboHaveCde">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeIDAbo#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>
<cffunction name="updateTypeCmdAbo" access="public" returntype="numeric" output="false" hint="" >
	<cfargument name="listeIDEquipement" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
	<cfargument name="idTypeCMD" required="false" type="numeric" default=""  />
	<cfargument name="action" required="false" type="numeric" default=""  />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.updateTypeCmdAbo">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeIDEquipement#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypeCMD#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#action#">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>		
</cffunction>

	<cffunction name="searchAboOperateurClient" access="remote" returntype="query">
			<cfargument name="SEARCHTEXTE" 	type="string"  required="true">
			<cfargument name="ORDERBYTEXTE" type="string"  required="true">
			<cfargument name="OFFSET" 		type="numeric" required="true">
			<cfargument name="LIMIT" 		type="numeric" required="true">
			<cfargument name="IDCLIENT" 	type="numeric" required="true">
			<cfargument name="IDTYPECMD" 	type="numeric" required="true">
			<cfargument name="OPERATEURID" 	type="numeric" required="true">
			<cfargument name="defaut" 		type="numeric" required="true">
			
			<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
			<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
			<cfset SEARCH_TEXT = TAB_SEARCH[2]> 
			<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
			<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
			<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
			<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.searchabooperateurclient_v4">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" 					null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" 					null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCLIENT#" 				null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPECMD#" 				null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OPERATEURID#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#defaut#" 					null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idlang#"					null="false">
				<cfprocresult name="p_result">
			</cfstoredproc> 
			
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getNumberDefaultProducts" access="remote" returntype="Numeric" hint="FOURNIT LE NOMBRE D'ABO/OPTIONS PAR DEFAUT A UN TYPE DE COMMANDE PAR OPERATEUR">	
		<cfargument name="idtypecommande" 	required="true" type="Numeric"/>
		<cfargument name="idoperateur" 		required="true" type="Numeric"/>
		<cfargument name="isabo" 			required="true" type="Numeric"/><!--- 1 -> ABONNEMENT, 2-> OPTIONS --->
		
			<cfset var mystruct = structNew()>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m24.getNumberDefaultProducts">	
				<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in"  variable="p_idtypecommande" value="#idtypecommande#">
				<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in"  variable="p_idoperateur" 	value="#idoperateur#">
				<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour"> 
				<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour2">  
			</cfstoredproc>	

			<cfset mystruct.NBR_ABO = p_retour>
			<cfset mystruct.NBR_OPT = p_retour2>
			
			<cfif isabo EQ 1>
				<cfset retour = mystruct.NBR_ABO>
			<cfelse>
				<cfset retour = mystruct.NBR_OPT>
			</cfif>

		<cfreturn retour>
	</cffunction>
	
	<cffunction name="getTypeCommande" access="remote" returntype="Array" hint="FOURNIT LA LISTE DES TYPE DE COMMANDE CLIENT">	
		<cfargument name="listeIDAbo" 	required="true" type="String"  default="" />
		<cfargument name="segment" 		required="true" type="Numeric" default="1"/>
		<cfargument name="idracine" 	required="true" type="Numeric"/>
		<cfargument name="idoperateur" 	required="true" type="Numeric"/>
		<cfargument name="isabo" 		required="true" type="Numeric"/>
		
			<cfset var retour = ArrayNew(1)>

			<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m24.IsAboHaveCde_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB"	  value="#listeIDAbo#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment#">
				<cfprocresult name="p_result">
			</cfstoredproc>

			<cfloop query="p_result">
				<cfset mystruct 					= structNew()>
				<cfset mystruct.LIBELLE_PROFIL 		= p_result.LIBELLE_PROFIL>
				<cfset mystruct.IDPROFIL_EQUIPEMENT = p_result.IDPROFIL_EQUIPEMENT>
				<cfset mystruct.IDRACINE  			= p_result.IDRACINE>
				<cfset mystruct.COMMENTAIRE 		= p_result.COMMENTAIRE>
				<cfset mystruct.CODE_INTERNE 		= p_result.CODE_INTERNE>
				<cfset mystruct.ETAT 				= p_result.ETAT>
				<cfset mystruct.SEGMENT_MOBILE 		= p_result.SEGMENT_MOBILE>
				<cfset mystruct.SEGMENT_FIXE 		= p_result.SEGMENT_FIXE>
				<cfset mystruct.SEGMENT_DATA 		= p_result.SEGMENT_DATA>
				<cfset mystruct.IDOPERATEUR 		= idoperateur>
				<cfset mystruct.NUMBER 				= getNumberDefaultProducts(p_result.IDPROFIL_EQUIPEMENT , idoperateur, isabo)>

				<cfset ArrayAppend(retour, mystruct)>
			</cfloop>

		<cfreturn retour>
	</cffunction>

	<cffunction name="getLastAboAssociated" access="remote" returntype="Array">
		<cfargument name="idsAbonnement" required="true" type="Array" >
		<cfargument name="idracine" 	 required="true" type="Numeric"/>
		
			<cfset var typecommande = ArrayNew(1)>
			
			<cfloop index="idx" from="1" to="#ArrayLen(idsAbonnement)#">
			
				<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.getLastAboAssociated">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsAbonnement[idx]#" 	null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#"			null="false">
					<cfprocresult name="p_result">
				</cfstoredproc>
				
				<cfloop query="p_result">
					<cfset type = structnew()>
					<cfset type.SEGMENT_MOBILE 			= p_result.SEGMENT_MOBILE>
					<cfset type.SEGMENT_DATA 			= p_result.SEGMENT_DATA>
					<cfset type.SEGMENT_FIXE 			= p_result.SEGMENT_FIXE>
					<cfset type.CODE_INTERNE 			= p_result.CODE_INTERNE>
					<cfset type.IDRACINE 				= p_result.IDRACINE>
					<cfset type.IDPROFIL_EQUIPEMENT 	= p_result.IDPROFIL_EQUIPEMENT>
					<cfset type.LIBELLE_PROFIL 			= p_result.LIBELLE_PROFIL>
					<cfset type.COMMENTAIRE 			= p_result.COMMENTAIRE>
					<cfset type.OBLIGATOIRE 			= p_result.OBLIGATOIRE>
					<cfset type.IDRESSOURCES 			= idsAbonnement[idx]>
					<cfset ArrayAppend(typecommande, type)>
				</cfloop>
			</cfloop>
			
		<cfreturn typecommande>
	</cffunction>
	
	<cffunction name="getassociatedabo_options" access="remote" returntype="Any" output="false" hint="RECUPERE L'ABONNEMENT OU LES OPTIONS PREDEFINIE DANS M24">
		<cfargument name="idstypecommande" 	type="Array" 	required="true">
		<cfargument name="idoperateur"	 	type="Numeric" 	required="true">
		
			<cfset var abonnement = ArrayNew(1)>
			
			<cfloop index="idx" from="1" to="#ArrayLen(idstypecommande)#">
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.getDefaultProducts_v2">	
					<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idtypecommande" 	value="#idstypecommande[idx]#">
					<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_isabo" 			value="1">
					<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_langid" 			value="#SESSION.USER.IDGLOBALIZATION#">
					<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idoperateur"		value="#idoperateur#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
				
				<cfset type = structnew()>
				
				<cfset type.IDCATALOGUE = 0>
				<cfset type.IDPROFIL 	= idstypecommande[idx]>
				<cfset type.ISPRESENT 	= false>

				<cfloop query="p_retour">
					<cfset type.IDCATALOGUE = p_retour.IDPRODUIT_CATALOGUE>
				</cfloop>
								
				<cfset ArrayAppend(abonnement, type)>
			</cfloop>
			
		<cfreturn abonnement/>
	</cffunction>
	
	<cffunction name="associateabo_options" access="remote" returntype="Numeric">
		<cfargument name="idsOpt" 			type="String"  required="true">
		<cfargument name="idtypecommande" 	type="Numeric" required="true">
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.setDefaultProducts">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  value="#idtypecommande#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	   value="#idsOpt#" 			null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>
			
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="removeabo_options" access="remote" returntype="Numeric">
		<cfargument name="idsOpt" 			type="String"  required="true">
		<cfargument name="idtypecommande" 	type="Numeric" required="true">
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.removeDefaultProducts">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idtypecommande#" 	null="false">
				<cfprocparam type="In"  cfsqltype="CF_SQL_CLOB"    value="#idsOpt#" 			null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>
			
		<cfreturn p_result>
	</cffunction>

	<cffunction name="obligatoireabo_options" access="remote" returntype="Numeric">
		<cfargument name="idsOpt" 			type="String"  required="true">
		<cfargument name="idtypecommande" 	type="Numeric" required="true">
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.setProductsObligatoire">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  value="#idtypecommande#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	   value="#idsOpt#" 			null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>
			
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="removeobligatoireabo_options" access="remote" returntype="Numeric">
		<cfargument name="idsOpt" 			type="String"  required="true">
		<cfargument name="idtypecommande" 	type="Numeric" required="true">
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.removeProductsObligatoire">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idtypecommande#" 	null="false">
				<cfprocparam type="In"  cfsqltype="CF_SQL_CLOB"    value="#idsOpt#" 			null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>
			
		<cfreturn p_result>
	</cffunction>
	
	<!--- addSpecificProductName(p_idracine IN INTEGER, p_idproduit IN INTEGER, p_libelle IN VARCHAR2, p_retour OUT INTEGER) --->
	
	<cffunction name="addSpecificProductName" access="remote" returntype="Numeric">
		<cfargument name="idproduit" 	type="String"  required="true">
		<cfargument name="libelle" 		type="String"  required="true">
		<cfargument name="idracine" 	type="Numeric"  required="true">
		
			<!--- <cfset idracine = session.perimetre.id_groupe> --->
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m24.addSpecificProductName">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idproduit" 	value="#idproduit#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_libelle" 	value="#libelle#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>
			
		<cfreturn p_result>
	</cffunction>
	
	<!--- getSpecificProductName(p_idracine IN INTEGER, p_idproduit IN INTEGER, p_retour OUT SYS_REFCURSOR) --->
	
	<cffunction name="getSpecificProductName" access="remote" returntype="Query">
		<cfargument name="idproduit" 	type="String"  required="true">
		<cfargument name="idracine" 	type="Numeric"  required="true">
			
			<!--- <cfset idracine = session.perimetre.id_groupe> --->
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m24.getSpecificProductName">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idproduit" 	value="#idproduit#">
				<cfprocresult name="p_result">
			</cfstoredproc>
			
		<cfreturn p_result>
	</cffunction>
	
	<!--- ANCIENNE VERSION (V1) --->
	
	<cffunction name="updateAbonnement" access="remote" returntype="any" description="add equip in catalogue revendeur">
		<cfargument name="IDPRODUIT" 	type="numeric" required="true">
		<cfargument name="IDRACINE" 	type="numeric" required="true">
		<cfargument name="PRIX" 		type="string"  required="true">
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m24.updateabonnement">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#IDPRODUIT#" 	null="false">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#IDRACINE#" 	null="false">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#PRIX#" 		null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc> 
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- updateAbonnement_v2(p_idproduit IN INTEGER, p_idracine IN INTEGER, p_prix IN NUMERIC, p_libelle IN VARCHAR2, p_retour OUT INTEGER) --->
	
	<cffunction name="updateAbonnement_v2" access="remote" returntype="Numeric">
		<cfargument name="idproduit" 	type="String"  required="true">
		<cfargument name="prix" 		type="Numeric" required="true">
		<cfargument name="libelle" 		type="String"  required="true">
		<cfargument name="idracine" 	type="Numeric"  required="true">
		<cfargument name="showPrice" 	type="Numeric"  required="false">
						
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m24.updateabonnement_v3">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idproduit" 	value="#idproduit#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_FLOAT"   variable="p_prix" 		value="#prix#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_libelle" 	value="#libelle#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_show_price" 	value="#showPrice#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>
			
			<!--- <cfif p_result GT 0>
							
				<cfset reslt = addSpecificProductName(idproduit, libelle)>
				
				<cfif reslt LT 1>
				
					<cfset p_result = -1000>
				
				</cfif>
							
			</cfif> --->
			
		<cfreturn p_result>
	</cffunction>

	<cffunction name="updatePlanCatalog" access="remote" returntype="Any">
		<cfargument name="clientid" 	type="numeric"  required="true" displayname="idRacine">
		<cfargument name="plans" 	type="xml"  required="true" displayname="plans catalogue">

		<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m24.updatePlanCatalogXML">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#clientid#">
			<cfprocparam type="In"  cfsqltype="CF_SQL_CLOB" variable="p_plans" 	value="#toString(plans)#">
			<cfprocparam type="Out"  cfsqltype="CF_SQL_INTEGER" variable="p_result" >
		</cfstoredproc>

		<cfreturn p_result>
	</cffunction>

	<cffunction name="exportCutomerPlan" access="remote" returntype="query">
			<cfargument name="SEARCHTEXTE" 	type="string"  required="true">
			<cfargument name="ORDERBYTEXTE" type="string"  required="true">
			<cfargument name="OFFSET" 		type="numeric" required="true">
			<cfargument name="LIMIT" 		type="numeric" required="true">
			<cfargument name="IDCLIENT" 	type="numeric" required="true">
			<cfargument name="IDTYPECMD" 	type="numeric" required="true">
			<cfargument name="OPERATEURID" 	type="numeric" required="true">
			<cfargument name="defaut" 		type="numeric" required="true">
			
			<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
			<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
			<cfset SEARCH_TEXT = TAB_SEARCH[2]> 
			<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
			<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
			<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
			<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.searchabooperateurclient_v4">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" 					null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" 					null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCLIENT#" 				null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPECMD#" 				null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OPERATEURID#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#defaut#" 					null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idlang#"					null="false">
				<cfprocresult name="p_result">
			</cfstoredproc> 
			
		<cfreturn p_result>
	</cffunction>

</cfcomponent>
