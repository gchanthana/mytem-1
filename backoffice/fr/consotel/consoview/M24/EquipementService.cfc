<cfcomponent displayname="EquipementService" output="false">
	<!--- 
		- info sur l'equipements
	 --->
	<cffunction name="getProductFeature" access="public" returntype="array" output="false">
		<cfargument name="idEquipement" required="true" type="numeric" 	default=""/>
		<cfargument name="idlang" 		required="true" type="numeric" 	default="3"/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_equipement.get_product_feature_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idequipement" 	value="#idEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_langid" 		value="#idlang#">
			<cfprocresult name="p_retour" resultset="1">
			<cfprocresult name="p_retour_photo" resultset="2">
		</cfstoredproc>
		
		<cfset result = arrayNew(1)>
		<cfset arrayAppend(result,p_retour)>
		<cfset arrayAppend(result,p_retour_photo)>
				
		<cfreturn result>
	</cffunction>
	
	<!--- 
		- recupère les accessoires suivant le modèle sélectionné
	 --->
	
	<!--- 
		- obtient les accessoires client compatibles avec l'appareil sélectionné
	 --->
	<cffunction name="getProductRelated" access="public" returntype="query" output="false">
		<cfargument name="idEquipement" 	required="true" type="numeric" 	default=""/>
		<cfargument name="sameRevendeur" 		required="true" type="numeric" 	default=""/>
		<cfargument name="chaine" 			required="true" type="string" 	default="null"/>
		<cfargument name="idlang" 			required="true" type="numeric" 	default="3"/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_equipement.get_product_related_v2"><!--- NE PAS OUBLIER LE NOM DU PACKAGE --->
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idequipement" 	value="#idEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_chaine" 		value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_langid" 		value="#idlang#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_same_rev" 		value="1">	
			<cfprocresult name="p_retour">			
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>
</cfcomponent>