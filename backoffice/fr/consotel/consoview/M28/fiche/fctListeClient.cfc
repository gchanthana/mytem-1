﻿<cfcomponent>
	<!--- getDetailClientContractuel --->
	<cffunction name="getDetailClientContractuel" 
				displayname="getDetailClientContractuel" 
				description="retourne les détails d'un client : informations, liste des offres" 
				access="remote" 
				output="false" 
				returntype="query">
		<cfargument name="clientId" required="true" >		
					
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M28.getInfosClient">
			<cfprocparam value="#clientId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qClient">
		</cfstoredproc>
		
		<cfreturn qClient/>
	</cffunction>
	
	<!---getDetailOffre --->
	<cffunction name="getDetailOffre" 
				displayname="getDetailOffre" 
				description="retourne les détails d'une offre : module souscrit, nb total de souscription etc ..." 
				access="remote" 
				output="false" 
				returntype="query">
		<cfargument name="offreid" required="true" >		
					
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M28.getInfosOffre">
			<cfprocparam value="#offreid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
		<cfreturn qResult/>
	</cffunction>
	
</cfcomponent>