﻿<cfcomponent extends="fr.consotel.consoview.M28.fct.AbstractFct">

	<cffunction name="executeFct" access="public" returntype="Struct" >
		<cfargument name="code" type="String" required="true" >
		<cfargument name="data" type="Struct" required="true" >
		
		<cftry>
			<cfset  obj=CreateObject("component","fr.consotel.consoview.M28.action.Act#code#").executeAct(data)>
			
			<cfreturn obj/>
			
			<cfcatch type="any" >
				<!---<cfreturn ThrowCfCatchError(-1,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>--->
					<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

</cfcomponent>