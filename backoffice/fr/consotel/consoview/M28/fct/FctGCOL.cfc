<cfcomponent extends="AbstractFct" hint="classe de gestion des services lies a la manipulation des templates de collectes">

	<cffunction name="executeFct" access="public" returntype="struct" >
		<cfargument name="code" type="String" required="true" >
		<cfargument name="data" type="Struct" required="true" >
		
		<cftry>
			<cfset obj=CreateObject("component","fr.consotel.consoview.M28.action.Act#code#").executeAct(data)>
				
			<cfreturn obj/>
			
			<cfcatch type="any" >
				<cfreturn ThrowCfCatchError(-1,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
				
			</cfcatch>
		</cftry>
	</cffunction>

	
	<!--- Test les identifiants de connexion donnée par l'utilisateur pour recollecter
	      les données depuis son intranet --->
           		   
	<cffunction name="testIdentification" access="public" returntype="Numeric" output="false">		
		<cfargument name="intanet_url"  required="true" type="String" default=""/>
		<cfargument name="login"  required="true" type="String" default=""/>
		<cfargument name="password"  required="true" type="String" default=""/>
		
			<!--- TODO tester l'identification  OK : 1  , KO : -1 --->		
			
		<cfreturn 1 >
	</cffunction>
	

</cfcomponent>