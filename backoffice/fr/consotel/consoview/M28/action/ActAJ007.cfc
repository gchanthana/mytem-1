<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" 
			displayname="ActAJ007" hint="creation/edition d'une actualite" output="false">
	
	<!--- 
		créer/éditer une actualité
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct"
				hint="créer/éditer une actualité" output="false">
					
		<cfargument name="data" type="Struct" required="false">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.createActualite">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_codeApp" type="in" value="#data.CODEAPP#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" variable="p_langid" type="in" value="#data.CODELANGUE#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" variable="p_label" type="in" value="#data.TITRE#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" variable="p_details" type="in" value="#data.DETAILS#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_visible" type="in" value="#data.VISIBLE#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_position" type="in" value="#data.POSITION#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idactualite" type="in" value="#data.IDACTUALITE#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" variable="p_url" type="in" value=""><!--- #data.LIEN# --->
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" variable="p_date_parution" type="in" value="#data.DATEPARUTION#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour">
			</cfstoredproc>
			 
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			
			<cfreturn CreateReponse("AJ007", 1, qResult)>
			
		<cfcatch type="Any">
			<cfreturn CreateReponse("AJ007", 0, ThrowCfCatchError("AJ007",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>