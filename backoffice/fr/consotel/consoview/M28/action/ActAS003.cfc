<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAS003" hint="supprimer une actualité" output="false">
				
	<!--- 
		supprimer une actualite
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct"
				hint="supprimer une actualité" output="false">
		
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.deleteActualite">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" variable="p_liste_actualite" type="in" value="#data.LISTEACTU#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out">
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			<!--- <cfset qResult.result = 1> --->
			
			<cfif qResult.result gt 0>
				
				<cfloop array="#data.ARRAYIDACTU#" index="idActu">
					
					<cfset bata = structNew()>
					<cfset bata.IDACTUALITE = idActu>
					<cfset retour = structNew()>
					
					<!--- recuperation liste des PJs d'une actualité --->
					<cfset retour = createobject("component","fr.consotel.consoview.M28.action.ActAL010").executeAct(bata)>
					
					<cfif isStruct(retour) && NOT structIsEmpty(retour)>
						
						<cfloop collection="#retour.DATA#" item="x">
							<cfloop query="retour.DATA.RESULT" startrow="1" endrow="#retour.DATA.RESULT.recordCount#">
								<cfset uuid = retour.DATA.RESULT.UUID>
								<cfmail server="mail-cv.consotel.fr" port="25" from="joan.cirencien@saaswedo.com" to="joan.cirencien@saaswedo.com"
										failto="joan.cirencien@saaswedo.com" subject="UUID" type="html">
									<p>UUID des PJs à supprimer dans le FTP</p>
									<cfdump var="#uuid#">
								</cfmail>
						
								<!--- suppression des dossier+fichier dans le FTP --->
								<!--- <cfset createobject("component","fr.consotel.consoview.M28.upload.UploaderFile").deleteFTP(#uuid#)> --->
								
							</cfloop>
						</cfloop>
						
					</cfif>
					
				</cfloop>
				
			</cfif>
			
			<cfreturn CreateReponse("AS003", 1, qResult)>
		<cfcatch type="Any">
			<cfreturn CreateReponse("AS003", 0, ThrowCfCatchError("AS003",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>

</cfcomponent>