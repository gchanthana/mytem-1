﻿<cfcomponent extends="AbstractAction">
	
	<cffunction name="executeAct" access="remote" returntype="Struct" output="false" hint="retourne la liste des univers">		
		<cfargument name="data" type="Struct" required="true" hint="ne contient rien">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.getListeUnivers">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL013", 1, data)>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL013", 0, err)/>
			</cfif>
		<cfcatch type="Any">
			<cfreturn CreateReponse("AL013", 0, ThrowCfCatchError("AL013",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>

</cfcomponent>