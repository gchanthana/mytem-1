<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" hint="sert pour l'insertion/la mise à jour d'une formation" author="Florian">
	
	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="contient 5 variables">

		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.IUFormation">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.IDFORMATIONPART#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.IDPARTENAIRE#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.IDSUJETFORMATION#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.DATE_FORMATION, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.IDLIEU_FORMATION#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_retour">
			</cfstoredproc>
				
			<cfif p_retour GTE 1>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AJ012", 1, data)/>
			<cfelseif p_retour EQ -1>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur Inconnue", "", "")>
				<cfreturn CreateReponse("AJ012", 0, err)/>
			<cfelseif p_retour EQ -100>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de paramètres d'entrées obligatoires", "", "")>
				<cfreturn CreateReponse("AJ012", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ012", 0, ThrowCfCatchError("AJ012",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>