﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
		 <!--- Fournit la liste des opérateurs  [id, libelle, libellé rocf associé] --->
           		   <!---Ancien nom de la fonction, getOperateurList--->
	<cffunction name="executeAct" access="public" returntype="struct" output="false">		
		<cftry>				
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.get_operateur" >			
				<cfprocresult name="p_retour">
			</cfstoredproc>
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL030", 1, data)/>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL030", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL030", 0, ThrowCfCatchError("AL030",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		
		</cftry>
	</cffunction>
</cfcomponent>