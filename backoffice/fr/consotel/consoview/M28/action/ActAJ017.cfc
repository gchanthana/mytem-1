﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" hint="Ajout de partenaire pour la première fois" author="Soufiane">

	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="contient 7 variables">
		
		<cftry>
			<!--- Partenaire normale et non prospect --->
			<cfif data.dataPartenaire.p_isProspect EQ 0>
				
				<cfset etapePartenaire = CreateObject("component","fr.consotel.consoview.M28.action.ActAJ016").executeAct(data.dataPartenaire)>
				
				<cfif etapePartenaire.STATUS EQ 1>
					<cfset data.dataContrat.p_Fc_Partenaireid = etapePartenaire.DATA.RESULT>
					<cfset etapeContrat = CreateObject("component","fr.consotel.consoview.M28.action.ActAJ015").executeAct(data.dataContrat)>
					
					<cfif etapeContrat.STATUS EQ 1>
						<cfset data.dataSite.p_Fc_Partenaireid = etapePartenaire.DATA.RESULT>
						<cfset etapeSite = CreateObject("component","fr.consotel.consoview.M28.action.ActAJ010").executeAct(data.dataSite)>
						
						<cfif etapeSite.STATUS EQ 1>
							<cfset data.dataInterloc.p_Fc_Partenaireid = etapePartenaire.DATA.RESULT>
							<cfset etapeInterloc = CreateObject("component","fr.consotel.consoview.M28.action.ActAJ014").executeAct(data.dataInterloc)>					
						</cfif>
					</cfif>
				</cfif>
					
				<cfif etapeInterloc.STATUS EQ  1>
					<cfset data = structNew()>
					<cfset data.RESULT = 1>
					<cfreturn CreateReponse("AJ017", 1, data)/>
				<cfelse>
					<cfset err = ThrowCfCatchError(-10, "", "Echec de la création du partenaire", "", "")>
					<cfreturn CreateReponse("AJ017", 0, err)/>
				</cfif>
			<!--- Partenaire prospect --->
			<cfelseif  data.dataPartenaire.p_isProspect EQ 1>
				
				<cfset etapePartenaire = CreateObject("component","fr.consotel.consoview.M28.action.ActAJ016").executeAct(data.dataPartenaire)>
					
				<cfif etapePartenaire.STATUS EQ 1>
					<cfset data.dataContrat.p_Fc_Partenaireid = etapePartenaire.DATA.RESULT>
					<cfset etapeContrat = CreateObject("component","fr.consotel.consoview.M28.action.ActAJ015").executeAct(data.dataContrat)>
				
					<cfif etapeContrat.STATUS EQ  1>
						<cfset data = structNew()>
						<cfset data.RESULT = 1>
						<cfreturn CreateReponse("AJ017", 1, data)/>
					<cfelse>
						<cfset err = ThrowCfCatchError(data, "", "Echec de la création du partenaire prospect", "", "")>
						<cfreturn CreateReponse("AJ017", 0, err)/>
					</cfif>
				<cfelseif etapePartenaire.STATUS EQ 0>
						<cfreturn CreateReponse("AJ017", 0, etapePartenaire.DATA)/>
				</cfif>
			</cfif>
			
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ017", 0, ThrowCfCatchError("AJ017",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
</cfcomponent>