﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="Struct" required="true">
	
		<cftry>
			
			<!--- Suppression d'un profil --->
			<cfstoredproc procedure="pkg_m28.deleteProfil" datasource="ROCOFFRE">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idprofil" value="#data.idProfil#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out" >
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.RESULT = p_retour>
			<cfreturn CreateReponse("AS001", 1, qResult)>
			
			<cfcatch type="Any">
				<cfreturn CreateReponse("AS001", 0, ThrowCfCatchError("AS001",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
	</cffunction>
	
</cfcomponent>


