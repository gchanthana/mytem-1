﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" author="Florian" hint="retourne la liste des modules">
		
	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="ne contient rien">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.List_sujet_formation">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#getCodeLangue()#">
				<cfprocresult name="p_List_sujet_formation">
			</cfstoredproc>
				
			<cfif isQuery(p_List_sujet_formation)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_List_sujet_formation>
				<cfreturn CreateReponse("AL019", 1, data)/>
			<cfelseif isnumeric(p_List_sujet_formation)>
				<cfset err = ThrowCfCatchError(p_List_sujet_formation, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL019", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL019", 0, ThrowCfCatchError("AL019",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>

	


</cfcomponent>