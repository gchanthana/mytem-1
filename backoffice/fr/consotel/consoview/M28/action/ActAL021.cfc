﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" author="Florian" hint="">

	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="ne contient rien">
		
		<cfset codeAppli = 351>
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_partenaire.getListeLangueForApplication">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#codeAppli#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#getCodeLangue()#">
				
				<cfprocresult name="p_retour">
			</cfstoredproc>
				
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL021", 1, data)/>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL021", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL021", 0, ThrowCfCatchError("AL021",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>