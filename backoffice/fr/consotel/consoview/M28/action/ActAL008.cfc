<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAL008" hint="liste des actualités" output="false">

	<!--- 
		retourne la liste des actualités
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct" output="false"
				hint="retourne la liste des actualités">
		
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.getListeActualite">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_codeApp" type="in" value="#data.CODEAPP#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_langid" type="in" value="#data.CODELANGUE#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			
			<cfreturn CreateReponse("AL008", 1, qResult)>		
		<cfcatch type="Any">
			<cfreturn CreateReponse("AL008", 0, ThrowCfCatchError("AL008",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>

</cfcomponent>