<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" hint="Insertion / Mise à jour des interlocuteurs" author="Florian">
	
	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="contient 12 variables">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.IUInterlocuteur">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_idfc_interlocuteur#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_Fc_Partenaireid#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_Civilite#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_Nom#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_Prenom#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_Qualite#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_Email_Inter#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_Bool_Principal_Inter#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_Default_Langue#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_Date_Deb, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_Date_Fin, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_retour">
			</cfstoredproc>
				
			<cfif p_retour GTE 1>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AJ014", 1, data)/>
			<cfelseif p_retour EQ -1>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur inconnue", "", "")>
				<cfreturn CreateReponse("AJ014", 0, err)/>
			<cfelseif p_retour EQ -10>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème de chevauchement de date", "", "")>
				<cfreturn CreateReponse("AJ014", 0, err)/>
			<cfelseif p_retour EQ -20>
				<cfset err = ThrowCfCatchError(p_retour, "", "Email déjà existant", "", "")>
				<cfreturn CreateReponse("AJ014", 0, err)/>
			<cfelseif p_retour EQ -100>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème de paramètres d'entrées obligatoires", "", "")>
				<cfreturn CreateReponse("AJ014", 0, err)/>
			<cfelse>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur inconnue", "", "")>
				<cfreturn CreateReponse("AJ014", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ014", 0, ThrowCfCatchError("AJ014",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>