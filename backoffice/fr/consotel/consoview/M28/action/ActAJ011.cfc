<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" hint="action pour la mise à jour d'un contrat" author="Florian">
	
	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="contient p_Fc_Partenaireid, p_idFc_contrat, date_deb_contrat et date_fin_contrat(facultatif)">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.UContrat">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_idFc_contrat#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_date_deb_contrat, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_date_fin_contrat, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_retour">
			</cfstoredproc>
			<cfif p_retour GTE 1>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AJ011", 1, data)/>
			<cfelseif p_retour EQ -1>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur inconnue", "", "")>
				<cfreturn CreateReponse("AJ011", 0, err)/>
			<cfelseif p_retour EQ -10>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème de chevauchement date", "", "")>
				<cfreturn CreateReponse("AJ011", 0, err)/>
			<cfelseif p_retour EQ -100>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème de paramètres d'entrées obligatoires", "", "")>
				<cfreturn CreateReponse("AJ011", 0, err)/>
			<cfelse>
				<cfreturn CreateReponse("AJ011", 0, ThrowCfCatchError(p_retour, "", "Erreur inconnue", "", ""))>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ011", 0, ThrowCfCatchError("AJ011",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>