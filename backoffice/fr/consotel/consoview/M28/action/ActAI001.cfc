﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAI001" hint="informations d'une actualité"  output="false">
	
	<!--- 
		retourne toutes les informations d'une actualité
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct"
				hint="retourne toutes les informations d'un article" output="false">
		
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.getInfosArticle">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idarticle" type="in" value="#data.IDACTUALITE#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			
			<cfreturn CreateReponse("AI001", 1, qResult)>
		<cfcatch type="Any">
			<cfreturn CreateReponse("AI001", 0, ThrowCfCatchError("AI001",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>

</cfcomponent>