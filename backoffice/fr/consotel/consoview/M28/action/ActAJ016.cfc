﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" hint="Créer un partenaire d'un contrat" author="Soufiane">
	
	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="contient 7 variables">
		
		<cftry>
			
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.IUPartenaire">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_partenaireid#">
				<cfprocparam CFSQLTYPE="CF_SQL_vaRCHAR" type="in" value="#data.p_nom#">
				<cfprocparam CFSQLTYPE="CF_SQL_VaRCHAR" type="in" value="#data.p_adresse#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_origine#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_idorigine#">
				
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_retour">
			</cfstoredproc>
				
			<cfif p_retour GTE 1>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AJ016", 1, data)/>
			<cfelseif p_retour EQ -10>
				<cfset err = ThrowCfCatchError(p_retour, "", "Nom partenaire existe déjà", "", "")>
				<cfreturn CreateReponse("AJ016", 0, err)/>
			<cfelseif p_retour EQ -100>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème de paramètres d'entrées obligatoires", "", "")>
				<cfreturn CreateReponse("AJ016", 0, err)/>
			<cfelse>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur inconnue", "", "")>
				<cfreturn CreateReponse("AJ016", 0, err)/>
			</cfif>
			
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ016", 0, ThrowCfCatchError("AJ016",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	

</cfcomponent>