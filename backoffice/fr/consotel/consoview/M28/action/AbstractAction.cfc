﻿<cfcomponent>
	
	<cffunction name="getCodeLangue" access="public" returntype="String" hint="retourne le codeLangue requis dans beaucoup de procédures stockées">
		  <cfreturn session.user.globalization>
		  <!---<cfreturn "fr_FR">--->
	</cffunction>
	
	<cffunction name="ThrowCfCatchError" access="public" returntype="Struct" hint="retourne un object contenant l'erreur du cfcatch">
		<cfargument name="idError" 		required="true" 	type="string" >
		<cfargument name="type" 		required="false" 	type="string" 	default="">
		<cfargument name="message" 		required="false" 	type="string"  	default="">
		<cfargument name="detail" 		required="false" 	type="string"  	default="">
		<cfargument name="stackTrace"	required="false" 	type="String" 	default=""/>
		
		<cfset cfCatchObject = createobject("component","fr.consotel.consoview.M28.Util").ThrowCfCatchError(idError,type,message,detail,stackTrace)/>
		
		<cfreturn cfCatchObject>
	</cffunction>
	
	<cffunction name="CreateReponse" access="public" returntype="Struct" hint="créé un objet réponse de la classe Action">
		<cfargument name="code" 		required="true" 	type="string" >
		<cfargument name="status" 		required="true" 	type="numeric" 	default="0">
		<cfargument name="data" 		required="false" 	type="struct">
		
		<cfset REPONSE = structNew()>
		<cfset REPONSE.CODE = arguments.code>
		<cfset REPONSE.STATUS = arguments.status>
		<cfset REPONSE.DATA = arguments.data> 
		
		<cfreturn REPONSE>
	</cffunction>
	
</cfcomponent>