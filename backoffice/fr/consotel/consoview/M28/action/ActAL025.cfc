﻿<cfcomponent  extends="fr.consotel.consoview.M28.action.AbstractAction" hint="retourne une liste des types de plan tarifaire" >

	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.List_typeplantarif">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#getCodeLangue()#">
				<cfprocresult name="p_List_typeplantarif">
			</cfstoredproc>
				
			<cfif isQuery(p_List_typeplantarif)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_List_typeplantarif>
				<cfreturn CreateReponse("AL025", 1, data)/>
			<cfelseif isnumeric(p_List_typeplantarif)>
				<cfset err = ThrowCfCatchError(p_List_typeplantarif, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL025", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL025", 0, ThrowCfCatchError("AL025",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>