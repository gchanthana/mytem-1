﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	
	<!---Mise à jour d'un droit d'une fonction --->
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="struct" required="true" >
		
		<cftry>
			
			<!--- modifier un droit d'une fonction --->
				<cfstoredproc procedure="pkg_m28.IUParametreFoncion" datasource="ROCOFFRE">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.nomParamFonction#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#data.idFonction#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#data.idParamsFonction#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.descriptParamFct#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.liste_niveau#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out">
				</cfstoredproc>
				
				<cfset qResult = structNew()>
				<cfset qResult.RESULT = p_retour>
				<cfreturn CreateReponse("AJ006", 1, qResult)>
			
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ006", 0, ThrowCfCatchError("AJ006",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
				
	</cffunction>

</cfcomponent>