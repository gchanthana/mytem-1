<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAL006" hint="liste des applications" output="false">
	
	<!--- 
		retourne la liste des application
	 --->
	<cffunction name="executeAct" access="public" returntype="Struct" output="false"
				hint="retourne la liste des applications">
		
		<cfargument name="data" type="Struct" required="true" hint="ne contient rien">

		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.getListeAppForComm_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" value="#SESSION.USER.APP_LOGINID#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			
			<cfreturn CreateReponse("AL006", 1, qResult)>
		<cfcatch type="Any">
			<cfreturn CreateReponse("AL006", 0, ThrowCfCatchError("AL006",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>