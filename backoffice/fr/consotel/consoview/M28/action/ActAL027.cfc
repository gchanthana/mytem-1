﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	   <!--- Fournit la liste des templates publié 
            en fonction de :
		    l'opérateur (id) : si null tous
            du type de données : si null tout type
            du mode de récupération : si null tout mode
	 --->
	<cffunction name="executeAct" access="public" returntype="struct" output="false">	
			
		<cfargument name="data" required="true" type="struct"/>
				
		<cftry>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.get_published_template">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.selectedOperateurId#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.selectedDataType#" >
	 		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.selectedRecuperationMode#" > 
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL027", 1, data)/>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL027", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL027", 0, ThrowCfCatchError("AL027",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
</cfcomponent>