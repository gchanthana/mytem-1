﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">

	<!--- Recuperer la liste des profils --->
	<cffunction name="executeAct" access="public" returntype="struct" >
		<cfargument name="data" type="Struct" required="true" >
		<cftry>
			<cfstoredproc procedure="pkg_m28.getListProfilsByClient" datasource="ROCOFFRE" >
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" type="in" value="#data.id_client#">
				<cfprocresult name="p_retour" >
			</cfstoredproc>
			
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL002", 1, data)>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(-1,"","Erreur de procédure","","")>
				<cfreturn CreateReponse("AL002",0,err)/>
			</cfif>
			
			<cfcatch type="any" >
				<cfreturn CreateReponse("AL002", 0, ThrowCfCatchError("AL002",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>