<cfcomponent  extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAJ009" hint="modifier les actualités dans le slider" output="false">
	
	<!--- 
		Ajouter une pièce jointe à une actualité
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct"
				hint="ajouter une pièce jointe à une actualité" output="false">
					
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.updatePositionActualite">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_codeApp" type="in" value="#data.CODEAPP#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_langid" type="in" value="#data.IDLANGUE#">
				<cfprocparam cfsqltype="CF_SQL_CLOB" variable="p_actu_visible" type="in" value="#data.ACTUVISIBLE#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out">
			</cfstoredproc>
						
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			
			<cfreturn CreateReponse("AJ009", 1, qResult)>
		<cfcatch type="Any">
			<cfreturn CreateReponse("AJ009", 0, ThrowCfCatchError("AJ009",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>