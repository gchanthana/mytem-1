﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" author="Soufiane" hint="retourne la liste des personnes d'un partenaire, participants à une formation ou non." >

	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.List_Login_partenaire">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_fc_partenaireid#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_idfc_formation#">
				<cfprocresult name="p_List_Login">
			</cfstoredproc>
				
			<cfif isQuery(p_List_Login)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_List_Login>
				<cfreturn CreateReponse("AL023", 1, data)/>
			<cfelseif isnumeric(p_List_Login)>
				<cfset err = ThrowCfCatchError(p_List_Login, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL023", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL023", 0, ThrowCfCatchError("AL023",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
</cfcomponent>