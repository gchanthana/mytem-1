﻿<cfcomponent extends="AbstractAction">

	<!---Ajouter un droit à une fonction --->
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="struct" required="true" >
		
		<cftry>
			
			<!--- Enregistrer un droit avec les id des niveaux selectionnés--->
				<cfstoredproc procedure="pkg_m28.createDroitFonction" datasource="ROCOFFRE">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.nomParamFct#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.description#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.niveaux#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#data.idFonction#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out" >
				</cfstoredproc>
				
				<cfset qResult = structNew()>
				<cfset qResult.RESULT = p_retour>
				<cfreturn CreateReponse("AJ005", 1, qResult)>
			
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ005", 0, ThrowCfCatchError("AJ005",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
				
	</cffunction>
	
</cfcomponent>