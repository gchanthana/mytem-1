﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	   <!--- 
        Crée ou édite une nouvelle collecte
	    si  p_idcollecteTemplate=0 : MODE CREATION else MODE EDITION
	    retour de l'id de la collecte crée/modifié
	    retour code erreur	           
	 --->
	<cffunction name="executeAct" access="public" returntype="struct" output="false">		
		<cfargument name="data" type="struct" required="true"/>
<!---		   <cfargument name="p_idracine"  hint="l'id racine de l'utilisateur" type="numeric" />
		   <cfargument name="p_idcollectClient"  hint="l'id de la collecte" type="numeric" />
		   <cfargument name="p_idcollectTemplate"  hint="l'id du template associé" type="numeric" />
		   <cfargument name="p_libelleCollect"  hint="le libellé de la collecte" type="string" />
		   <cfargument name="p_roic"  hint="l'id roic de la collecte" type="string" />
		   <cfargument name="p_identifiant"  hint="le login de connexion si mode pull" type="string" />
		   <cfargument name="p_mdp"  hint="le password de connexion si mode pull" type="string" />
		   <cfargument name="p_plageImport"  hint="la plage d'ouverture des imports" type="date" />
		   <cfargument name="p_retroactivite"  hint="bool retroactivite ou non" type="numeric" />
		   <cfargument name="p_activiationAlerte"  hint="bool activation alerte ou non" type="numeric" />
		   <cfargument name="p_delaiAlerte"  hint="delai de l'alerte" type="numeric" />
		   <cfargument name="p_mailAlerte"  hint="mail du destinataire de l'alerte" type="string" />
		   <cfargument name="p_isActif"  hint="collecte active ou non" type="numeric" />
		   <cfargument name="p_userCreate"  hint="id de l'utilisateur ayant crée la collecte" type="numeric" />		  
		   <cfargument name="p_userModif"  hint="id de l'utilisateur ayant modifié la collecte" type="numeric" />--->
		
		<cftry>
			 <cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.IU_collect_client">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_idracine#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_idcollectClient#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_idcollectTemplate#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_libelleCollect#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_roic#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_identifiant#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_mdp#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_DATE" value="#data.p_plageImport#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_retroactivite#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_activiationAlerte#" >	
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_delaiAlerte#" >				
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_mailAlerte#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_isActif#" >					
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_userCreate#" >					
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_userModif#" >				
				<cfprocparam  variable="p_retour" type="Out" cfsqltype="CF_SQL_INTEGER">		
			</cfstoredproc> 
							
			<cfif p_retour GTE 1>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AJ018", 1, data)/>
			<cfelseif p_retour EQ -1>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur inconnue", "", "")>
				<cfreturn CreateReponse("AJ018", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ018", 0, ThrowCfCatchError("AJ018",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>			
	</cffunction>
</cfcomponent>