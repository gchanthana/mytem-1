﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" author="Florian" hint="retourne la liste des lieux de formation">

	
	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="ne contient rien">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.List_Lieu_formation">
				<cfprocresult name="p_List_LieuFormation">
			</cfstoredproc>
				
			<cfif isQuery(p_List_LieuFormation)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_List_LieuFormation>
				<cfreturn CreateReponse("AL020", 1, data)/>
			<cfelseif isnumeric(p_List_LieuFormation)>
				<cfset err = ThrowCfCatchError(p_List_LieuFormation, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL020", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL020", 0, ThrowCfCatchError("AL020",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
	
</cfcomponent>