﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">

	<cffunction name="executeAct" access="remote" returntype="Struct" output="false" hint="retourne la liste des fonctions par Profil">
		
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.getListeFonctionProfil">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idcg_profil" type="in" value="#data.idProfil#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_lang" type="in" value="#getCodeLangue()#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			
			<cfreturn CreateReponse("AL012", 1, qResult)>	
		<cfcatch type="Any">
			<cfreturn CreateReponse("AL012", 0, ThrowCfCatchError("AL012",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>