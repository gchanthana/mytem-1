<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAJ008" hint="ajouter une piece jointe à une actualité" output="false">
	
	<!--- 
		Ajouter une pièce jointe à une actualité
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct"
				hint="ajouter une pièce jointe à une actualité" output="false">
					
		<cfargument name="data" type="Struct" required="true">
		<!--- <cfmail server="mail-cv.consotel.fr" port="25" from="joan.cirencien@saaswedo.com" to="joan.cirencien@saaswedo.com" 
					failto="joan.cirencien@saaswedo.com" subject="STRUCT" type="html">
												
					<cfdump var="#data#">
					
			</cfmail> --->
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.addFileToArticle">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idarticle" type="in" value="#data.IDACTUALITE#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" variable="p_uuid" type="in" value="#data.UUID#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" variable="p_nom" type="in" value="#data.NOM#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" variable="p_extension" type="in" value="#data.EXTENSION#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out">
			</cfstoredproc>
						
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			
			<cfreturn CreateReponse("AJ008", 1, qResult)>
			
		<cfcatch type="Any">
			<cfreturn CreateReponse("AJ008", 0, ThrowCfCatchError("AJ008",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>