﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAI001" hint="informations d'une actualité"  output="false">
	
	<!--- 
		retourne toutes les informations d'une actualité
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct"
				hint="retourne toutes les informations d'un article" output="false">
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfset result = structNew()>
			<cfset result.LISTE_RACINE = getListeRacine()>

			<cfif isStruct(result.LISTE_RACINE) AND structKeyExists(result.LISTE_RACINE,"CODEERREUR")>
				<cfreturn CreateReponse("AI003",0,result.LISTE_RACINE)/>
			</cfif>

			<cfreturn CreateReponse("AI003",1,result)/>
		<cfcatch type="any" >
			<cfreturn CreateReponse("AI003", 0, ThrowCfCatchError("PTHD0020",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
	</cffunction>

	<cffunction name="getListeRacine" 
				access="private" 
				returntype="any" >
		<cftry>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_M28.getListeRacine">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.APP_LOGINID#">
				<cfprocresult name="qListe">
			</cfstoredproc>
			<cfreturn CreateReponse("AI003", 1, qListe)>
		<cfcatch type="any" >
			<cfreturn CreateReponse("AI003", 0, ThrowCfCatchError("PTHD0021",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>