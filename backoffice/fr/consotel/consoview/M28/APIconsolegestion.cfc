﻿<cfcomponent>
	
	<cffunction name="execute" access="remote" returntype="Struct" >
		<cfargument name="key" type="String" required="true">
		<cfargument name="code" type="String" required="true" >
		<cfargument name="data" type="Struct" required="true" >
		
		<cftry>
			<cfset  obj=CreateObject("component","fr.consotel.consoview.M28.fct.Fct#key#").executeFct(code, data)>
			
			<cfreturn obj/>
			
			<cfcatch type="any" >
				<!---<cfreturn createobject("component","fr.consotel.consoview.M28.Util").ThrowCfCatchError(0,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)/>--->
					<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

</cfcomponent>