﻿<cfcomponent>
	<cffunction name="ThrowCfCatchError" access="public" returntype="Struct" hint="Retourne un object contenant l'erreur du cfcatch">
		<cfargument name="idError" 		required="true" 	type="string" >
		<cfargument name="type" 		required="false" 	type="string" 	default="">
		<cfargument name="message" 		required="false" 	type="string"  	default="">
		<cfargument name="detail" 		required="false" 	type="string"  	default="">
		<cfargument name="stackTrace"	required="false" 	type="String" 	default=""/>
		
		<cflog text="ERROR !!! => ThrowCfCatchError : #idError# - #type# - #message# - #detail#">
		
		<cfset cfCatchObject = structNew()>
		<cfset cfCatchObject.CODEERREUR = idError>
		<cfset cfCatchObject.CFCATCH = structNew()>
		<cfset cfCatchObject.CFCATCH.TYPE = type>
		<cfset cfCatchObject.CFCATCH.MESSAGE = message>
		<cfset cfCatchObject.CFCATCH.DETAIL = detail>
		<cfset cfCatchObject.CFCATCH.STACKTRACE = stackTrace>
		
		<cfreturn cfCatchObject>
	</cffunction>
</cfcomponent>