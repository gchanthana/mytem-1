<cfcomponent output="false">
	
	<cfset DataSourceAnalytique = "BI_TEST">
	<cfset DataSourceEntrepot = "E0">
	
	<cffunction name="getLibelleGroupeClient" returntype="Any" access="remote" description="Récupére le libellé du noeud passé en paramètre">
		<cfargument name="idgroupe_client" 	type="numeric" required="true">
			
		<cfset _libelle_groupe_client = "-">

		<cfif isdefined("idgroupe_client")>		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M35.getLibelleGroupeClient">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" value="#idgroupe_client#">
				<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR" variable="r_libelle_groupe_client">
			</cfstoredproc>
			<cfset _libelle_groupe_client = r_libelle_groupe_client>			
		</cfif>
					
		<cfreturn _libelle_groupe_client>
	</cffunction>
	
	<cffunction name="getListeIdMois" returntype="Any" access="remote" description="Récupére la liste des orgas">
		<cflog text="#session.user.globalization#">
		<cflog text="#session.perimetre.id_groupe#">
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M35.getPeriodeFacturation_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_DATE" variable="p_date_last_fact" value="#SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN#">
				<cfprocparam type="in" cfsqltype="CF_SQL_vaRCHAR" variable="p_langue" 	value="#SESSION.USER.GLOBALIZATION#">
				<cfprocresult name="p_retour">
		</cfstoredproc>

		<cfset query = p_retour>
		<cfreturn query>
	</cffunction>
	<cffunction name="getListeOrga" returntype="Any" access="remote" description="Récupére la liste des orgas">
		<cflog text="****************************** getListeOrga (#session.perimetre.ID_GROUPE#)">
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M35.getOrga_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 		value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="getListeNiveauOrga" returntype="Any" access="remote" description="Récupére la liste des orgas">
		<cfargument name="idorga" 	type="numeric" required="true">
		<cfargument name="idcliche" type="numeric" required="true">
		
		<cflog text="****************************** getListeNiveauOrga">
		<cflog text="****************************** idorga : #idorga#">
		<cflog text="****************************** idcliche : #idcliche#">
		<cflog text="****************************** idracine_master : #SESSION.PERIMETRE.IDRACINE_MASTER#">
		<cflog text="****************************** idracine : #SESSION.PERIMETRE.ID_GROUPE#">
		<cflog text="****************************** langue : #SESSION.USER.GLOBALIZATION#">

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M35.getNiveauxOrga_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 		value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idorga" 		value="#idorga#">
				<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="getListeNoeudOrga" returntype="Any" access="remote" description="Récupére la liste des orgas">
		<cfargument name="idorga" 	type="numeric" required="true">
		<cfargument name="idcliche" type="numeric" required="true">
		<cfargument name="niveau" 	type="String" required="true">
		<cfargument name="idniveau" 	type="String" required="true">
		
		<cflog text="****************************** idorga : #idorga#">
		<cflog text="****************************** idcliche : #idcliche#">
		<cflog text="****************************** niveau : #niveau#">
		<cflog text="****************************** idniveau : #idniveau#">
		<cflog text="****************************** idracine_master : #SESSION.PERIMETRE.IDRACINE_MASTER#">
		<cflog text="****************************** idracine : #SESSION.PERIMETRE.ID_GROUPE#">
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M35.getNoeuds">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idcliche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#niveau#">
				<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>		
	<cffunction name="getDataset" returntype="Any" access="remote" description="Récupére la liste des orgas">
		<cfargument name="idorga" 	type="numeric" required="true">
		<cfargument name="idperimetre" 	type="numeric" required="true">
		<cfargument name="niveau" 	type="String" required="true">
		<cfargument name="iddebut" type="numeric" required="true">
		<cfargument name="idfin" type="numeric" required="true">
		<cfargument name="niveauAggreg" type="String" required="true">
		
		<cflog text="****************************** idorga : #idorga#">
		<cflog text="****************************** idperimetre : #idperimetre#">
		<cflog text="****************************** idmoisdeb : #iddebut#">
		<cflog text="****************************** idmoisfin : #idfin#">
		<cflog text="****************************** niveau : #niveau#">
		<cflog text="****************************** idracine_master : #SESSION.PERIMETRE.IDRACINE_MASTER#">
		<cflog text="****************************** idracine : #SESSION.PERIMETRE.ID_GROUPE#">
		<cflog text="****************************** langue : #SESSION.USER.GLOBALIZATION#">
		<cflog text="****************************** niveauAggreg : #niveauAggreg#">				
		 
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M35.getData_V2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" 		value="#SESSION.PERIMETRE.IDRACINE_MASTER#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idorga" 				value="#idorga#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" 			value="#idperimetre#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" 				value="#niveau#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_langue_pays" 				value="#SESSION.USER.GLOBALIZATION#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_mois_debut" 				value="#iddebut#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_mois_fin" 				value="#idfin#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_niveau_agreg" 	value="#niveauAggreg#">
				<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>			
	<cffunction name="sauvegarderAnalyse" returntype="Numeric" access="remote">
		<cfargument name="idcliche" 			type="numeric" 	required="true">	
		<cfargument name="nom" 					type="String" 	required="true">
		<cfargument name="description" 			type="String" 	required="true">
		<cfargument name="niveau" 				type="string" 	required="true">	
		<cfargument name="niveau_aggreg"		type="string" 	required="true">	
		<cfargument name="idperimetre" 			type="numeric" 	required="true">	
		<cfargument name="xml" 					type="xmL"	required="true">
		<cfargument name="idorga" 				type="numeric" 	required="true">	
		<cfargument name="idperiodedeb"			type="numeric" 	required="true">	
		<cfargument name="idperiodefin"			type="numeric" 	required="true">	
		<cfargument name="libelleorga" 			type="String" 	required="true">
		<cfargument name="libelleperiodedeb"	type="String" 	required="true">
		<cfargument name="libelleperiodefin"	type="String" 	required="true">
		
		<cflog text="[CAROL] - DEBUT - CREATION DE L'ANALYSE '#nom#'">
		<cflog text="[CAROL] description 		: #description#">
		<cflog text="[CAROL] idcliche 		: #idcliche#">
		<cflog text="[CAROL] idperimetre 		: #idperimetre#">
		<cflog text="[CAROL] niveau 			: #niveau#">
		<cflog text="[CAROL] niveau aggreg		: #niveau_aggreg#">
		<cflog text="[CAROL] iduser 			: #session.user.clientaccessid#">		
		<cflog text="[CAROL] idracine 		: #session.perimetre.ID_GROUPE#">
		<cflog text="[CAROL] idracinemaster 		: #session.perimetre.IDRACINE_MASTER#">
		<cflog text="[CAROL] idorga 			: #idorga#">
		<cflog text="[CAROL] idperiodedeb 		: #idperiodedeb#">
		<cflog text="[CAROL] idperiodefin 		: #idperiodefin#">
		<cflog text="[CAROL] langue 			: #session.user.globalization#">
		<cflog text="[CAROL] libelleorga 		: #libelleorga#">
		<cflog text="[CAROL] libelleperiodedeb 	: #libelleperiodedeb#">
		<cflog text="[CAROL] libelleperiodefin	: #libelleperiodefin#">
		<cflog text="[CAROL] xml			: #toString(xml)#">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m35.creerAnalyse">
			<cfprocparam value="#idcliche#" 							cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#nom#" 									cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam value="#description#" 							cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam value="#niveau#" 								cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam value="#idperimetre#" 							cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#session.user.clientaccessid#" 			cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#xml#" 									cfsqltype="CF_SQL_varCHAR">
			<cfprocparam value="#session.perimetre.ID_GROUPE#"			cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#session.perimetre.IDRACINE_MASTER#"	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#idorga#"								cfsqltype="CF_SQL_INTEGER">	
			<cfprocparam value="#idperiodedeb#"							cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#idperiodefin#"							cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#session.user.globalization#"			cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam value="#libelleorga#"							cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam value="#libelleperiodedeb#"					cfsqltype="cF_SQL_VARCHAR">
			<cfprocparam value="#libelleperiodefin#"					cfsqltype="cF_SQL_VARCHAR">
			<cfprocparam value="#niveau_aggreg#"						cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam variable="p_retour"							cfsqltype="cf_SQL_INTEGER" 	type="out">
		</cfstoredproc>		
		<cfif p_retour eq -1>
			<cflog text="****************************** [CAROL] - ECHEC">	
		<cfelse>
			<cflog text="****************************** [CAROL] - REUSSITE">	
		</cfif>
			<cflog text="****************************** [CAROL] - FIN - CREATION DE L'ANALYSE '#nom#'">	
		<cfreturn p_retour>
	</cffunction>	
	<cffunction name="SupprimerAnalyse" returntype="Numeric" access="remote">
		<cfargument name="idanalyse" 	type="numeric" required="true">	
		
		<cflog text="****************************** [CAROL] SUPPRESSION DE L'ANALYSE #idanalyse#">	
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m35.SupprimerAnalyse">
			<cfprocparam value="#idanalyse#" 	cfsqltype="CF_SQL_INTEGER"	type="in">
			<cfprocparam variable="p_retour"	cfsqltype="CF_SQL_INTEGER"	type="out">
		</cfstoredproc>		
		<cfif p_retour eq -1>
			<cflog text="****************************** [CAROL] - ECHEC">	
		<cfelse>
			<cflog text="****************************** [CAROL] - REUSSITE">	
		</cfif>
			<cflog text="****************************** [CAROL] - FIN - SUPPRESION DE L'ANALYSE '#idanalyse#'">	
		<cfreturn p_retour>
	</cffunction>	
	<cffunction name="renommerAnalyse" returntype="Numeric" access="remote">
		<cfargument name="idanalyse" 	type="numeric" required="true">	
		<cfargument name="nom" 			type="String" required="true">
		<cfargument name="description" 	type="String" required="true">
		
		<cflog text="****************************** [CAROL] EDITION DE L'ANALYSE #idanalyse#">
		<cflog text="****************************** [CAROL] nom 		: #nom#">	
		<cflog text="****************************** [CAROL] description : #description#">		
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m35.renommerAnalyse">
			<cfprocparam value="#idanalyse#" 	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#nom#" 			cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam value="#description#" 	cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam variable="p_retour" 	cfsqltype="CF_SQL_INTEGER" type="out">		
		</cfstoredproc>		
		<cfif p_retour eq -1>
			<cflog text="****************************** [CAROL] - ECHEC">	
		<cfelse>
			<cflog text="****************************** [CAROL] - REUSSITE">	
		</cfif>
			<cflog text="****************************** [CAROL] - FIN - EDITION DE L'ANALYSE '#idanalyse#'">	
		<cfreturn p_retour>
	</cffunction>	
	<cffunction name="listeAnalyse" returntype="query" access="remote">
		<cfargument name="start" 		type="Numeric" required="true">	
		<cfargument name="limit" 		type="Numeric" required="true">			
		
		<cflog text="****************************** [CAROL] LISTE DES ANALYSES DE #session.user.clientaccessid#">
		<cflog text="****************************** [CAROL] start 		: #start#">		
		<cflog text="****************************** [CAROL] limit 		: #limit#">		
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m35.listeAnalyse_V3">
			<cfprocparam value="#session.user.clientaccessid#" 	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#start#" 						cfsqltype="cf_SQL_INTEGER">
			<cfprocparam value="#limit#" 						cfsqltype="cf_SQL_INTEGER">
			<cfprocresult name="p_retour">
		</cfstoredproc>	
		<cfreturn p_retour>
	</cffunction>	
</cfcomponent>

