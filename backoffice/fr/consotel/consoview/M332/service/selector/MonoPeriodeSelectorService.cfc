﻿<cfcomponent>	
	<cffunction access="public" name="getValuesMonoPeriode"  output="false" returntype="query">		
		<!--- <cftry> --->
			<cfset dateDebut   = lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB,'dd/mm/yyyy')>
			<cfset dateFin =lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN,'dd/mm/yyyy')>
			<cfset NBMaxMois  = SESSION.PERIMETRE.STRUCTDATE.PERIODS.NB_MONTH_DISPLAY>
			<cfset idLang  = SESSION.USER.IDGLOBALIZATION>
			
			<cfif idLang eq 0>
				<cfset idLang =3>
			</cfif>
		
			<cfstoredproc datasource="ROCOFFRE"  procedure="PKG_M331.get_MonoPeriodeSelector">		
					<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#dateDebut#" variable="p_datedeb">
					<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#dateFin#"   variable="p_datefin">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="24" variable="p_nb_mois_max">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLang#" variable="p_langid">
					<cfprocresult name="qMonoPeriode">
			</cfstoredproc>
	        <cfreturn qMonoPeriode>
			<!--- <cfcatch type="any">
				<cfmail from="samer.chahoud@consotel.fr" to="samer.chahoud@consotel.fr" subject="getValuesMonoPeriode" type="html">
					<cfdump var="#CFCATCH#">
				</cfmail>
			</cfcatch>
		</cftry> --->
	</cffunction>
	
</cfcomponent>