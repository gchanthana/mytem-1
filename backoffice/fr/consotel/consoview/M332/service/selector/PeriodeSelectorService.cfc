﻿<cfcomponent>
	
	<cffunction access="public" name="getValuesPeriode"  output="false" returntype="query">		
		
		<cfset dateDebut   = lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB,'dd/mm/yyyy')>
		<cfset dateFin =lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN,'dd/mm/yyyy')>
		<cfset NBMaxMois  = SESSION.PERIMETRE.STRUCTDATE.PERIODS.NB_MONTH_DISPLAY>
				
		<cfstoredproc datasource="ROCOFFRE"  procedure="PKG_M331.get_MonoPeriodeSelector">		
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#dateDebut#" variable="p_datedeb">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#dateFin#"   variable="p_datefin">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="24" variable="p_nb_mois_max">
				<cfprocresult name="qMonoPeriode">
		</cfstoredproc>			
        <cfreturn qMonoPeriode>	
	</cffunction>


</cfcomponent>