﻿<cfcomponent>
	<cffunction access="public" output="false" name="getValuesFormatTemplate" returntype="query">
		
			<cfargument name="idRapportRacine" type="numeric" required="true">
			<cfargument name="idTemplate" type="numeric" required="true">
			
			<!--- permert d'nevoyerune valeur null à la base  --->
			<cfset tous=false>
			<cfif idTemplate eq -1 >	
				<cfset tous=true>
			</cfif>
		
            <cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.get_FormatTemplateSelector">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRapportRacine#" variable="p_idrapport_racine">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idTemplate#" variable="p_idrapport_template" null="#tous#">
				<cfprocresult name="qFormats">
			</cfstoredproc>				
            <cfreturn qFormats><!--- le resultat de la procedure --->
      </cffunction>   
	 
</cfcomponent>