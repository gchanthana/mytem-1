<cfcomponent output="false">	
	<cfset VARIABLES["reportServiceClass"]="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService">
	<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
<!--- ***************************** CODE A MODIFIER ******************************* --->		
	<!--- Le chemin du rapport sous BIP --->
	<cfset VARIABLES["XDO"]="/consoview/M332/EVOL-COUT-PAR-THEME-N/EVOL-COUT-PAR-THEME-N.xdo">
	<!--- Le code rapport --->
	<cfset VARIABLES["CODE_RAPPORT"]="ECT-Data">
	<!--- TEMPLATE BI --->
	<cfset VARIABLES["TEMPLATE"]="ECT_D">
	<!--- OUTPUT --->
	<cfset VARIABLES["OUTPUT"]="Evolution des couts par theme - Data">
	<!--- SEGMENT --->
	<cfset VARIABLES["SEGMENT"]="Data">
<!--- ************************************************************ --->


			
	<cfset reportService=createObject("component",VARIABLES["reportServiceClass"])>
	<cfset ApiE0 = CreateObject("component","fr.consotel.consoview.api.E0.ApiE0")>
	<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>
	
	<cffunction name="scheduleReport" access="remote" returnType="numeric">
		<cfargument name="PARAM_ARRAY" type="array" required="true">

		<cflog text="************ RAPPORT : #VARIABLES['OUTPUT']#">
	
<!--- ***************************** CODE A MODIFIER ******************************* --->		
<!--- LES PARAMETRES FOURNIT PAR L'IHM --->
	<!--- OBLIGATOIRE OBLIGATOIRE OBLIGATOIRE POUR TOUS LES RAPPORTS --->
		<cfset MODULE=PARAM_ARRAY[1]>
		<cfset FORMAT=PARAM_ARRAY[2]>
		<cfset EXT=PARAM_ARRAY[3]>
	<!--- DEPEND DU RAPPORT (CF DOC) --->
		<!--- a multiplier par le nombre de paramètre nécessaire --->
		<cfset G_IDPERIMETRE = PARAM_ARRAY[4]>
		<cfset PERIODICITE = PARAM_ARRAY[5]>
		<cfset LBLNOEUD = PARAM_ARRAY[6]>
		<cfset LBLPERIMETRE = ApiE0.getLibellePerimetre(G_IDPERIMETRE)>
<!--- ************************************************************ --->		

		
		<cfset initStatus=reportService.init(SESSION.PERIMETRE["ID_GROUPE"],VARIABLES["XDO"],"ConsoView",MODULE,VARIABLES["CODE_RAPPORT"])>
		<cfif initStatus EQ TRUE>
			<cfset setStatus=TRUE>
			
<!--- ***************************** CODE A MODIFIER ******************************* --->					
<!--- DEFINITION DES PARAMETRES DU RAPPORT --->	

	
		<!--- PARAMETRES RECUPERES EN SESSION --->
		<!--- IDRACINE_MASTER --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SESSION.PERIMETRE["IDRACINE_MASTER"]>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDMASTER",tmpParamValues)>
			
		<!--- IDRACINE_MASTER --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SESSION.PERIMETRE["ID_GROUPE"]>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDRACINE",tmpParamValues)>
		<!--- G_LANGUE_PAYS --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]= session.user.globalization>
			<cfset setStatus=setStatus AND reportService.setParameter("G_LANGUE_PAYS",tmpParamValues)>
	
	<!--- PARAMETRES RECUPERES VIA L'IHM--->		
		<!--- IDPERIMETRE --->			
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=G_IDPERIMETRE>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDPERIMETRE",tmpParamValues)>
	
	
	<!--- PARAMETRES RECUPERES VIA L'APIEO A PARTIR DE L'IDPERIMETRE --->
		<!--- ID DE L'ORGA --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset G_IDORGA=ApiE0.getOrga(G_IDPERIMETRE)>
			<cfset tmpParamValues[1]=G_IDORGA>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDORGA",tmpParamValues)>
		<!--- IDCLICHE : Celui du dernier mois du client --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset G_IDCLICHE=ApiE0.getCliche(G_IDORGA)>
			<cfset tmpParamValues[1]=G_IDCLICHE>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDCLICHE",tmpParamValues)>
				<!--- GESTION DES DATES --->
			<cfset idperiode_fin = ApiE0.getIdPeriodeMoisFacturation(Session.perimetre.ID_GROUPE,Session.perimetre.IDRACINE_MASTER)>
			<cflog text="*********************** idperiode_fin = #idperiode_fin#">
			<cfif idperiode_fin eq -1>
				<cfreturn -1>	
			</cfif>
			<cfswitch expression="#PERIODICITE#">
				<cfcase value="YEAR_G">
					<cfset idperiode_deb = idperiode_fin - 12>
					<cfset filename = "Annuel - #LBLPERIMETRE# - #VARIABLES['OUTPUT']# - #LBLNOEUD#">
					<cfset valueParameter="Annuel">
				</cfcase>
				
				<cfcase value="YEAR_C">
					<cfset year = Year(now())-1>
					<cflog  text="******************* APIE0 : year = #year#">
					<cfset idperiode_deb = ApiE0.getIdPeriodeMois(createdate(year,01,01))>
					<cfset idperiode_fin = ApiE0.getIdPeriodeMois(createdate(year,12,31))>
					<cfset filename = "#year# - #LBLPERIMETRE# - #VARIABLES['OUTPUT']# - #LBLNOEUD#">
					<cfset valueParameter="Année civile">
					<cflog text="****************** periode du rapport : #idperiode_deb# to #idperiode_fin#">
				</cfcase>
				
				<cfcase value="SEM_G">
					<cfset idperiode_deb = idperiode_fin - 5>
					<cfset filename = "Semestriel - #LBLPERIMETRE# - #VARIABLES['OUTPUT']# - #LBLNOEUD#">
					<cfset valueParameter="Semestriel">
				</cfcase>
			</cfswitch>

			<!--- IDPERIODEMOIS DEB --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=idperiode_deb>
			<cfset setStatus=setStatus AND reportService.setParameter("U_IDPERIOD_DEB",tmpParamValues)>

			<!--- IDPERIODEMOIS FIN --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=idperiode_fin>
			<cfset setStatus=setStatus AND reportService.setParameter("U_IDPERIOD_FIN",tmpParamValues)>

	<!--- AUTRES PARAMETRES --->
		<!--- SEGMENT --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=VARIABLES["SEGMENT"]>>
			<cfset setStatus=setStatus AND reportService.setParameter("G_SEGMENT",tmpParamValues)>
		<!--- G_NIVEAU --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset G_NIVEAU = ApiE0.getNiveauOrga_V2(G_IDPERIMETRE)>
			<cfset tmpParamValues[1]=G_NIVEAU>
			<cfset setStatus=setStatus AND reportService.setParameter("G_NIVEAU",tmpParamValues)>
		<!--- X_INCLUDE_CSV_HEADER  --->	
			<cfset tmpParamValues=arrayNew(1)>
			<cfif ucase(FORMAT) eq "CSV">
				<cfset tmpParamValues[1]=1>
				<cflog text="************ X_INCLUDE_CSV_HEADER = 1">
			<cfelse>
				<cfset tmpParamValues[1]=0>
				<cflog text="************ X_INCLUDE_CSV_HEADER = 0">
			</cfif>
			<cfset setStatus=setStatus AND reportService.setParameter("X_INCLUDE_CSV_HEADER",tmpParamValues)>					
<!--- ************************************************************ --->

			<cflog text="*************************** idracine_master : #SESSION.PERIMETRE['IDRACINE_MASTER']#">
			<cflog text="*************************** idracine : #SESSION.PERIMETRE['ID_GROUPE']#">
			<cflog text="*************************** idperimetre : #G_IDPERIMETRE#">
			<cflog text="*************************** idorga : #G_IDORGA#">
			<cflog text="*************************** idcliche : #G_IDCLICHE#">
			<cflog text="*************************** u_idperiod_deb : #idperiode_deb#">
			<cflog text="*************************** u_idperiod_fin : #idperiode_deb#">	
			<cflog text="*************************** segment : #SEGMENT#">	
			<cflog text="*************************** g_langue_pays : #session.user.globalization#">
			<cflog text="*************************** g_niveau : #G_NIVEAU#">
			
			<cfif setStatus EQ TRUE>
				<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],FORMAT,EXT,filename,VARIABLES["CODE_RAPPORT"])>
				<cfif outputStatus EQ TRUE>
					<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
					<cflog text="************ reportStatus.JOBID = #reportStatus['JOBID']#">
					<cflog text="************ reportStatus.UUID  = #reportStatus['UUID']#">
					<cfif reportStatus['JOBID'] neq -1>
						<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Périodicité,",valueParameter)>		
						<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Périmètre d'étude",LBLPERIMETRE)>			
						<cfreturn reportStatus["JOBID"]>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn -1>
	</cffunction>
</cfcomponent>
