<cfcomponent displayname="GetSite" output="false">

<cffunction name="getSite" access="public" returntype="query" output="false">                               
    <cfargument name="idsite" type="numeric" required="true">         
    <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m32.get_siteInfo">          
         <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idsite" value="#idsite#"/>  
         <cfprocresult name="p_result">                               
    </cfstoredproc> 
    <cfreturn p_result/>                           
</cffunction> 

</cfcomponent>