<cfcomponent output="false">
	
	<cfset VARIABLES["reportServiceClass"]="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService">
	<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
<!--- ***************************** CODE A MODIFIER ******************************* --->		
	
	
	
	<!--- OUTPUT --->
	<cfset VARIABLES["OUTPUT"]="Rapport">
	<!--- Module --->
	<cfset VARIABLES["MODULE"]="M32">
	
<!--- ************************************************************ --->
	
	<cfset reportService = createObject("component",VARIABLES["reportServiceClass"])>
	<cfset ApiE0 = CreateObject("component","fr.consotel.consoview.api.E0.ApiE0")>
	<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>
	
	<cffunction name="getExportHIF" access="remote" returnType="any">
		
		<cfargument name="FORMAT" type="string" required="true" >
		<cfargument name="IDINVENTAIRE" type="numeric" required="true" >
		<cfargument name="EFFECTIF" type="numeric" required="true" >
		
			<cfset VARIABLES["OUTPUT"]=VARIABLES["OUTPUT"] & "_hors_inventaire">
		<!--- Le chemin du rapport sous BIP --->
		<cfset VARIABLES["XDO"]="/consoview/M32/HORS_INV_FACTURE/HORS_INV_FACTURE.xdo">
		<!--- Le code rapport --->
		<cfset VARIABLES["CODE_RAPPORT"]="HORS_INV_FACTURE">
		<!--- TEMPLATE BI --->
		<cfset VARIABLES["TEMPLATE"]="HIF">
	
		<!--- Extension --->
		<cfset VARIABLES["EXT"]=FORMAT>
		
		
		<cfif ucase(FORMAT) eq "XLS">
			<cfset VARIABLES["FORMAT"]="excel">
		<cfelse>
			<cfif ucase(FORMAT) eq "CSV">
				<cfset VARIABLES["FORMAT"]="CSV">
			<cfelse>
				<cfif ucase(FORMAT) eq "PDF">
					<cfset VARIABLES["FORMAT"]="PDF">
				</cfif>
			</cfif>
		</cfif>
		
		
		
		
		<cfset initStatus=reportService.init(SESSION.PERIMETRE["ID_GROUPE"],VARIABLES["XDO"],"ConsoView",VARIABLES["MODULE"],VARIABLES["CODE_RAPPORT"])>
		<cfif initStatus EQ TRUE>
			<cfset setStatus=TRUE>
			
		<!--- PARAMETRES RECUPERES EN SESSION --->
			<!--- IDRACINE_MASTER --->
				<cfset tmpParamValues=arrayNew(1)>
				<cfset tmpParamValues[1]=SESSION.PERIMETRE["IDRACINE_MASTER"]>
				<cfset setStatus=setStatus AND reportService.setParameter("p_idmaster",tmpParamValues)>
			<!--- IDRACINE_MASTER --->
				<cfset tmpParamValues=arrayNew(1)>
				<cfset tmpParamValues[1]=SESSION.PERIMETRE["ID_GROUPE"]>
				<cfset setStatus=setStatus AND reportService.setParameter("p_idracine",tmpParamValues)>
		
			<!--- IDINVENTAIRE --->
				<cfset tmpParamValues=arrayNew(1)>
				<cfset tmpParamValues[1]=IDINVENTAIRE>
				<cfset setStatus=setStatus AND reportService.setParameter("p_idinventaire_periode",tmpParamValues)>
			<!--- EFFECTIF --->
				<cfset tmpParamValues=arrayNew(1)>
				<cfset tmpParamValues[1]=EFFECTIF>
				<cfset setStatus=setStatus AND reportService.setParameter("p_nbjour_acte",tmpParamValues)>
			
			
			<cfset pretour = -1>
			<cfif setStatus EQ TRUE>
				<cfset FILENAME=VARIABLES["OUTPUT"]>
				<cfset outputStatus = FALSE>
				<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],VARIABLES["FORMAT"],VARIABLES["EXT"],FILENAME,VARIABLES["CODE_RAPPORT"])>
				<cfif outputStatus EQ TRUE>
					<cfset reportStatus = FALSE>
					<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
					<cfset pretour = reportStatus['JOBID']>
					<!--- <cflog text="************ reportStatus.JOBID = #reportStatus['JOBID']#">
					<cflog text="************ reportStatus.UUID  = #reportStatus['UUID']#"> --->
					<cfif reportStatus['JOBID'] neq -1>
						<!--- <cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Périodicité",valueParameter)>	
						<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Périmètre d'étude",LBLPERIMETRE)>	--->			
						<cfreturn reportStatus["JOBID"]>
					</cfif>
				</cfif>
			</cfif>	
		</cfif>
		<cfreturn pretour >
	</cffunction>


	<cffunction name="getExportINF" access="remote" returnType="any">
		
		<cfargument name="FORMAT" type="string" required="true" >
		<cfargument name="IDINVENTAIRE" type="numeric" required="true" >
		<cfargument name="EFFECTIF" type="numeric" required="true" >
		<cfargument name="SOLUTION" type="numeric" required="true" >
		
		<!--- Le chemin du rapport sous BIP --->
		<cfset VARIABLES["XDO"]="/consoview/M32/INV_NON_FACTURE/INV_NON_FACTURE.xdo">
		<!--- Le code rapport --->
		<cfset VARIABLES["CODE_RAPPORT"]="INV_NON_FACTURE">
		<!--- TEMPLATE BI --->
		<cfset VARIABLES["TEMPLATE"]="INF">
		<cfset VARIABLES["OUTPUT"]=VARIABLES["OUTPUT"] & "_dans_inventaire">
		<!--- Extension --->
		<cfset VARIABLES["EXT"]=FORMAT>
		
		
		<cfif ucase(FORMAT) eq "XLS">
			<cfset VARIABLES["FORMAT"]="excel">
		<cfelse>
			<cfif ucase(FORMAT) eq "CSV">
				<cfset VARIABLES["FORMAT"]="CSV">
			<cfelse>
				<cfif ucase(FORMAT) eq "PDF">
					<cfset VARIABLES["FORMAT"]="PDF">
				</cfif>
			</cfif>
		</cfif>
		
		
		
		
		<cfset initStatus=reportService.init(SESSION.PERIMETRE["ID_GROUPE"],VARIABLES["XDO"],"ConsoView",VARIABLES["MODULE"],VARIABLES["CODE_RAPPORT"])>
		<cfif initStatus EQ TRUE>
			<cfset setStatus=TRUE>
			
		<!--- PARAMETRES RECUPERES EN SESSION --->
			<!--- IDRACINE_MASTER --->
				<cfset tmpParamValues=arrayNew(1)>
				<cfset tmpParamValues[1]=SESSION.PERIMETRE["IDRACINE_MASTER"]>
				<cfset setStatus=setStatus AND reportService.setParameter("p_idmaster",tmpParamValues)>
			<!--- IDRACINE_MASTER --->
				<cfset tmpParamValues=arrayNew(1)>
				<cfset tmpParamValues[1]=SESSION.PERIMETRE["ID_GROUPE"]>
				<cfset setStatus=setStatus AND reportService.setParameter("p_idracine",tmpParamValues)>
		
			<!--- IDINVENTAIRE --->
				<cfset tmpParamValues=arrayNew(1)>
				<cfset tmpParamValues[1]=IDINVENTAIRE>
				<cfset setStatus=setStatus AND reportService.setParameter("p_idinventaire_periode",tmpParamValues)>
			<!--- EFFECTIF --->
				<cfset tmpParamValues=arrayNew(1)>
				<cfset tmpParamValues[1]=EFFECTIF>
				<cfset setStatus=setStatus AND reportService.setParameter("p_nbjour_acte",tmpParamValues)>
			<!--- SOLUTION --->
				<cfset tmpParamValues=arrayNew(1)>
				<cfset tmpParamValues[1]=SOLUTION>
				<cfset setStatus=setStatus AND reportService.setParameter("p_solution",tmpParamValues)>
			
			
			<cfset pretour = -1>
			<cfif setStatus EQ TRUE>
				<cfset FILENAME=VARIABLES["OUTPUT"]>
				<cfset outputStatus = FALSE>
				<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],VARIABLES["FORMAT"],VARIABLES["EXT"],FILENAME,VARIABLES["CODE_RAPPORT"])>
				<cfif outputStatus EQ TRUE>
					<cfset reportStatus = FALSE>
					<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
					<cfset pretour = reportStatus['JOBID']>
					<!--- <cflog text="************ reportStatus.JOBID = #reportStatus['JOBID']#">
					<cflog text="************ reportStatus.UUID  = #reportStatus['UUID']#"> --->
					<cfif reportStatus['JOBID'] neq -1>
						<!---<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Périodicité",valueParameter)>	
						<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Périmètre d'étude",LBLPERIMETRE)>	--->			
						<cfreturn reportStatus["JOBID"]>
					</cfif>
				</cfif>
			</cfif>	
		</cfif>
		<cfreturn pretour >
	</cffunction>


</cfcomponent>