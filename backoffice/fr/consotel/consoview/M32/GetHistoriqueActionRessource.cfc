<cfcomponent displayname="GetHistoriqueActionRessource" output="false">

<cffunction name="getHistoriqueActionRessource" access="public" returntype="query" output="false">                               
    <cfargument name="idressource" type="numeric" required="true">         
    <cfset codeLangue=session.user.globalization>      
    <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.GETHISTORIQUE_RECHERCHE_v2">          
         <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idinventaire_produit" value="#idressource#"/>  
         <cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#codeLangue#"/>  
         <cfprocresult name="p_result">                               
    </cfstoredproc> 
    <cfreturn p_result/>                           
</cffunction> 

</cfcomponent>