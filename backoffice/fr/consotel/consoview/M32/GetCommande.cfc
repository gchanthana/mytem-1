<cfcomponent displayname="GetCommande" output="false">
	
	<cffunction name="getCommande" access="remote" returntype="array" output="false" >
		
		<cfargument name="idCommande" 	required="true" type="numeric"/>

		<!---><cfset idGestionnaire = Session.USER.CLIENTACCESSID>--->
		<cfset idracine_master = session.perimetre.idracine_master>
		<cfset globalization = session.user.globalization>
		<!---<cfset idGestionnaire = 641>--->
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M32.fournirDetailOperation" blockfactor="1">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 	value="#idCommande#"/>
			<cfprocresult name="p_commande">			
		</cfstoredproc>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m21.getChamps">
			<cfprocparam  type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine_master#"/>
			<cfprocresult name="p_champs">
		</cfstoredproc>
		
		<cfset operateurid = val(p_commande['OPERATEURID'][1])>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.getLibelleSCCF">
			<cfprocparam  type="in" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" value="#operateurid#"/>
			<cfprocresult name="p_operateur">
		</cfstoredproc>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_TRANSPORTEUR.FOURNIRLISTETRANSPORTEURS" blockfactor="100" >
			<cfprocresult name="p_transporteur">			
		</cfstoredproc>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.HISTORIQUEETATSOPERATION" blockfactor="100" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idCommande" value="#idCommande#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_globalization"  value="#globalization#" >
			<cfprocresult name="p_historique">
		</cfstoredproc>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.FOURNIRARTICLESOPERATION" blockfactor="10">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idCommande#">			
			<cfprocresult name="articlexml">
		</cfstoredproc>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.commandeListAttachments">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande"  value="#idcommande#"/>
			<cfprocresult name="p_attachment">
		</cfstoredproc>	
		
		<cfset p_retour = ArrayNew(1) >
		<cfset p_retour[1] = p_commande >
		<cfset p_retour[2] = p_champs >
		<cfset p_retour[3] = p_operateur >
		<cfset p_retour[4] = p_transporteur >
		<cfset p_retour[5] = p_historique >
		<cfset p_retour[6] = articlexml >
		<cfset p_retour[7] = p_attachment >
		
		<cfreturn p_retour>
		
	</cffunction>
	
	
</cfcomponent>