<cfcomponent displayname="GetActeGestion" output="false">

<cffunction name="getActeGestion" access="remote" returnType="query">
	
	<cfargument name="IdInventaireProduit" type="numeric" required="true">
	
	<cfset idracine_master = session.perimetre.idracine_master>
	<cfset idgroupe_client = session.perimetre.id_groupe>
	
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m32.List_CurrentOperation">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_IdInventaireProduit" value="#IdInventaireProduit#" null="false">
		<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#idracine_master#" null="false"/>  
	        
	    <cfprocresult name="p_result">
    </cfstoredproc>
	
	<cfreturn p_result>
	
</cffunction>

</cfcomponent>