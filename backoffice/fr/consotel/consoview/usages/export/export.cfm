<cfset processObject=createObject("component","fr.consotel.consoview.usages.CallUsage")>
<cfif FORM.CONTENT_DESC EQ "LIST_APPELS">
	<cfset dataset=processObject.getLignesByFiltre(FORM.IDRACINE,FORM.DATEDEB,FORM.TEXT_INPUT)>
	<cfif dataset.recordcount GT 0>
		<cfset filename="Liste-Des-Appels_" & replace(session.perimetre.raison_sociale,' ','_',"all")>
		<cfset exportService=createObject("component","fr.consotel.consoview.api.reporting.export.ExportService")>
		<cfset exportService.exportQueryToText(dataset,filename & ".csv")>
	<cfelse>
		<cfoutput>
			<center><strong><h2>Aucunes données répondant à votre demande</h2></strong></center>
		</cfoutput>
	</cfif>
<cfelseif FORM.CONTENT_DESC EQ "LIST_DEST">
	<cfset dataset=processObject.getTypeAppel(FORM.IDRACINE,FORM.DATEDEB,FORM.APPELANT,FORM.SDA)>
	<cfif dataset.recordcount GT 0>
		<cfset filename="Liste-Des-Destination-#FORM.APPELANT#_" & replace(session.perimetre.raison_sociale,' ','_',"all")>
		<cfset exportService=createObject("component","fr.consotel.consoview.api.reporting.export.ExportService")>
		<cfset exportService.exportQueryToText(dataset,filename & ".csv")>
	<cfelse>
		<cfoutput>
			<center><strong><h2>Aucunes données répondant à votre demande</h2></strong></center>
		</cfoutput>
	</cfif>
<cfelse>
	<strong>Demande Impossible.<br>Contactez le support ConsoView.</strong>
</cfif>
