<cfcomponent name="CallUsage" alias="fr.consotel.consoview.usages.CallUsage" extends="fr.consotel.consoview.access.AccessObject">
	
	<cffunction name="getLignes" access="remote" returntype="query">
		<cfargument name="IDRACINE" required="true" type="numeric">
		<cfargument name="PERIOD" required="true" type="string" hint="format yyyy/mm/dd">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_ticket_v3.getlignes">
			<cfprocparam value="#getidRacineMaster()#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#IDRACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#PERIOD#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="qGetLignes">
		</cfstoredproc>
		<cfreturn qGetLignes>
	</cffunction>

	<cffunction name="getLignesByFiltre" access="remote" returntype="query">
		<cfargument name="IDRACINE" required="true" type="numeric">
		<cfargument name="PERIOD" required="true" type="string" hint="format yyyy/mm/dd">
		<cfargument name="APPELANT" required="true" type="string" hint="format yyyy/mm/dd">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_ticket_v3.getlignes_v2">
			<cfprocparam value="#getidRacineMaster()#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#IDRACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#PERIOD#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#APPELANT#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="qGetLignes">
		</cfstoredproc>
		<cfreturn qGetLignes>
	</cffunction>
	
	<cffunction name="getTypeAppel" access="remote" returntype="query">
		<cfargument name="IDRACINE" required="true" type="numeric">		
		<cfargument name="PERIOD" required="true" type="string" hint="format yyyy/mm/dd">
		<cfargument name="APPELANT" required="true" type="string">
		<cfargument name="SDA" required="true" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_ticket_v3.getTypeAppel">			
			<cfprocparam value="#getidRacineMaster()#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#IDRACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#PERIOD#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#APPELANT#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#SDA#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="qGetTypeAppel">
		</cfstoredproc>
		<cfreturn qGetTypeAppel>
	</cffunction>
	
	<cffunction name="getCDR" access="remote" returntype="query">
		<cfargument name="IDRACINE" required="true" type="numeric">		
		<cfargument name="PERIOD" required="true" type="string" hint="format yyyy/mm/dd">
		<cfargument name="APPELANT" required="true" type="string">
		<cfargument name="SDA" required="true" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_ticket_v3.getticket">
			<cfprocparam value="#getidRacineMaster()#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#IDRACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#PERIOD#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#APPELANT#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#SDA#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="qGetCDR">
		</cfstoredproc>
		<cfreturn qGetCDR>
	</cffunction>
	
	
	<cffunction name="getCDRbyType" access="remote" returntype="query">
		<cfargument name="IDRACINE" required="true" type="numeric">		
		<cfargument name="PERIOD" required="true" type="string" hint="format yyyy/mm/dd">
		<cfargument name="APPELANT" required="true" type="string">
		<cfargument name="SDA" required="true" type="string">
		<cfargument name="TYPE_APPEL" required="true" type="string">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_ticket_v3.GETTICKET_TYPE">
			<cfprocparam value="#getidRacineMaster()#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#IDRACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#PERIOD#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#APPELANT#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#SDA#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#TYPE_APPEL#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="qGetCDR">
		</cfstoredproc>
		<cfreturn qGetCDR>
	</cffunction>
	
	<cffunction  name="exporterListeTicketsDestinationLigne" access="remote" returntype="string">
		<cfargument name="IDRACINE" required="true" type="numeric">		
		<cfargument name="PERIOD" required="true" type="string" hint="format yyyy/mm/dd">
		<cfargument name="APPELANT" required="true" type="string">
		<cfargument name="SDA" required="true" type="string">
		<cfargument name="TYPE_APPEL" required="true" type="string">				
		<cftry> 
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"Tickets_#SDA#_Dest_#TYPE_APPEL#.csv">				
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset qDetail = this.getCDRbyType(IDRACINE,PERIOD,APPELANT,SDA,TYPE_APPEL)>				
				<cfset NewLine = Chr(13) & Chr(10)>
				<cfset space = Chr(13) & Chr(10)>		
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="utf-8">
				<cfcontent type="text/plain">				
				<cfset volumeData = "">				
				
				<cfoutput>ligne;poste;operateur;destination;date;heure;numero_appele;volume;cout;#NewLine#</cfoutput>				
				<cfloop query="qDetail">					
					<cfif UCASE(Trim(qDetail.UNITE_APPEL)) eq "S">
		        		<cfset total_duree = qDetail.VOLUME>
		        		<cfset heures=int(total_duree/3600)>
						<cfset minutes=int( (total_duree/60) - (heures*60) )>
						<cfset secondes=(total_duree-heures*3600-minutes*60)>
						<cfset volumeData = heures&':'&LsNumberFormat(minutes,'00')&':'&LsNumberFormat(secondes,'00')>
		        	<cfelse>
		        		<cfset volumeData = qDetail.VOLUME & " " & qDetail.UNITE_APPEL>
		        	</cfif>		        	
			    	<cfoutput>#APPELANT#;#SDA#;#TRIM(qDetail.OPNOM)#;#TRIM(qDetail.TYPE_APPEL)#;#TRIM(LSDATEFORMAT(qDetail.DATEDEB,'DD/MM/YYYY'))#;#TRIM(LSTIMEFORMAT(qDetail.DATEDEB,'medium'))#;#TRIM(toString(qDetail.APPELE))#;#TRIM(volumeData)#;#TRIM(LSNumberFormat(qDetail.COUT_OP,"________.___"))#;#NewLine#</cfoutput>
				</cfloop>
							    
			</cfsavecontent>		
			<!--- Crï¿½ation du fichier CSV --->			 
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/usages/csv/#fileName#" charset="utf-8"
					addnewline="true" fixnewline="true" output="#contentObj#">		
			<cfreturn "#fileName#">
			
			<cfcatch type="any">					
			<cfreturn "error">	
		</cfcatch> 				
		</cftry>
	</cffunction>
</cfcomponent>
