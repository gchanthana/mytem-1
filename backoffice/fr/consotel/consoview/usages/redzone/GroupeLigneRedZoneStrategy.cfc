<!--- =========================================================================
Classe: GroupeLigneRedZoneStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GroupeLigneRedZoneStrategy" hint=""  extends="redzoneStrategy" >
    <cffunction name="getData" access="remote" returntype="query" output="false"  >
		<cfargument name="data" required="false" type="array" default="" displayname="array data" hint="Initial value for the dataproperty." />
			<cfset d1=LsDateFormat(createDate(getToken(data[7],3,"/"),getToken(data[7],2,"/"),getToken(data[7],1,"/")),"yyyy/mm/dd")>
			<cfset d2=LsDateFormat(dateadd("m",1,createDate(getToken(data[8],3,"/"),getToken(data[8],2,"/"),getToken(data[8],1,"/"))),"yyyy/mm/dd")>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG.RZ_BYGROUPE">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#data[1]#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#d1#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_datefin" value="#d2#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_tri" value="#data[5]#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_paysconsotelid" value="#data[6]#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_data10" value="#data[10]#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_data11" value="#data[11]#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_data12" value="#data[12]#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_data13" value="#data[13]#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_jour" value="#data[9]#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_nbrow" value="#data[4]#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_display_number" value="#data[3]#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_opt_bysiteid" value="0"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_opt_byville_commune" value=""/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_opt_byville_zip" value=""/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_opt_byzip" value=""/>
		        <cfprocresult name="p_result"/>        
			</cfstoredproc>
			<cfdump var="#p_result#">
			<!--- <cfloop from="1" to="#p_result.recordcount#" index="i">
				<cfset p_result.datedeb[i]=LsDateFormat(getData.datedeb[i],"dd/mm/yy")>
			</cfloop> --->
			<cfreturn p_result/>
	</cffunction>
</cfcomponent>