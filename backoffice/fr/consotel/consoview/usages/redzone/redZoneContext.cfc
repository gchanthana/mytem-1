<!--- =========================================================================
Classe: redZoneContext
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="redZoneContext" hint="This class is configured with a ConcreteStrategy object, maintains a reference to a Strategy object and may define an interface that lets Strategy access its data." >

	<cffunction name="getSites" access="public" returntype="query" output="false"  >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfreturn variables.instance.redzoneStrategy.getSites(ID)>
	</cffunction>
	
	<cffunction name="dirquery2treequery" access="public" returntype="query" output="false"  >
		<cfargument name="dquery" required="false" type="query" default="" displayname="query dquery" hint="Initial value for the dqueryproperty." />
		<cfreturn variables.instance.redzoneStrategy.dirquery2treequery(dquery)>
	</cffunction>
	
	<cffunction name="setStrategy" access="public" returntype="void" output="false" >
		<cfargument name="TYPE_PERIMETRE" required="false" type="string" default="" displayname="string TYPE_PERIMETRE" hint="type de périmetre" />
		<cfset temp=createObject("component","com.consoview.usages.redzone.#TYPE_PERIMETRE#redzoneStrategy")>
		<cfset setRedzoneStrategy(#temp#)>
	</cffunction>

	<cffunction name="setRedzoneStrategy" access="public" output="false" returntype="void" hint="Affecter une valeur a la propriete: redzoneStrategy">
		<cfargument name="newRedzoneStrategy" type="redzoneStrategy" required="true" />
		<cfset variables.instance.redzoneStrategy = arguments.newRedzoneStrategy />
	</cffunction>

</cfcomponent>