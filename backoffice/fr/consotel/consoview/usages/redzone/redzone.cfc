 <cfcomponent name="redzone" displayname="redzone">
	<!--- INITIATLISATION 
	
	<cffunction name="init" access="public">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		
		<cfset obj1=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
		<cfset obj1.init(perimetre,modeCalcul)>
		
		<cfset dataset=obj1.queryData(numero,createOdbcDate(datedeb),createOdbcDate(datefin),modeCalcul)>
		<cfset setSessionValue("RECORDSET",#dataset#)>					
	</cffunction> --->
	
	<!--- RETOURNE LES PRODUITS DU THEME --->	
	<cffunction name="getTree" access="public" returntype="xml">
		<cfargument name="numero" type="numeric" required="false">
		
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_SEARCH_V3.SEARCH_ARBRE_DESC_ASC">			
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" 
								value="#numero#"/>
				<cfprocresult name="p_result" />        
			</cfstoredproc>

		<cfif p_result.recordcount eq 0>
			<cfset xml = XmlNew()>
			<cfset xml.xmlRoot =  XmlElemNew(xml,"node")>
			<cfreturn xml>
		<cfelse>
			<cfreturn transformXml(p_result,numero)>
		</cfif>
	</cffunction>
	
	<!---::::::::::::::::::::::::::::::::::::::::::::::::::PRIVATE:::::::::::::::::::::::::::::::::::::::::::::::::::::::::---> 
	<cffunction name="transformDate" access="private" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>
	
	<cffunction name="transformXml" access="private" returntype="xml">		
		<cfargument name="dataset" type="query" required="true">		
		<cfargument name="groupIndex" type="numeric" required="true">
		<cfdump var="#dataset#">
		<cfset clientAccessId = SESSION.user.CLIENTACCESSID>
		
		<cfset idGroupe = dataset['IDGROUPE_CLIENT'][1] >
		<cfset libelleGroupeClient =  dataset['LIBELLE_GROUPE_CLIENT'][1]>
		
		<cfset idnode = searchNodeId(groupIndex,dataset)>
				
		<cfset dataStruct = structNew()>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset dataStruct.ROOT_NODE = rootNode>
		<cfset rootNode.XmlAttributes.LABEL = dataset['LIBELLE_GROUPE_CLIENT'][1]>
		<cfset rootNode.XmlAttributes.NODE_ID = dataset['NODE_ID'][1]>
		<cfset rootNode.XmlAttributes.LEVEL = dataset['NIVEAU'][1]>
		<cfset rootNode.XmlAttributes.TYPE_PERIMETRE = dataset['TYPE_PERIMETRE'][1]>
		<cfset rootNode.XmlAttributes.BOOL_ORGA = 1>
		<cfset rootNode.XmlAttributes.TYPE_ORGA = "">
		<cfset c = ispresent("OPE","TYPE_ORGA",dataset)>
		<cfif c gt 1> 
			
			<!---******** GROUPEMENTS ORGANISATIONS OPERATEUR***********--->			
			<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Organisations OpÃ©rateurs">
			<cfset rootNode.XmlChildren[1].XmlAttributes.BOOL_ORGA = 1>
			<cfset dataStruct.OP_ORGANISATIONS = rootNode.XmlChildren[1]>
			
			<cfif ispresent("CUS","TYPE_ORGA",dataset) gt 1>
			
				<cfset rootNode.XmlChildren[2] = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset rootNode.XmlChildren[2].XmlAttributes.LABEL = "Organisations Clients">
				<cfset rootNode.XmlChildren[2].XmlAttributes.BOOL_ORGA = 1>			
				<cfset dataStruct.CLIENT_ORGANISATIONS = rootNode.XmlChildren[2]>
			</cfif>				
		<cfelse>
				<cfif ispresent("CUS","TYPE_ORGA",dataset) gt 1>
					<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
					<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Organisations Clients">
					<cfset rootNode.XmlChildren[1].XmlAttributes.BOOL_ORGA = 1>				
					<cfset dataStruct.CLIENT_ORGANISATIONS = rootNode.XmlChildren[1]>	
				<cfelseif ispresent("GEO","TYPE_ORGA",dataset) gt 1>
					<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
					<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Organisations Clients">
					<cfset rootNode.XmlChildren[1].XmlAttributes.BOOL_ORGA = 1>				
					<cfset dataStruct.CLIENT_ORGANISATIONS = rootNode.XmlChildren[1]>
					
				</cfif>
		</cfif>				 
			
		<!---**************************************************************************************--->
		
		<cfloop index="i" from="2" to="#dataset.recordcount#">
			
			<cfset currentKey = dataset['IDGROUPE_CLIENT'][i]>
			<cfset currentParentKey = dataset['ID_GROUPE_MAITRE'][i]>
			<cfif dataset['ID_GROUPE_MAITRE'][i] EQ idGroupe>
				<cfif dataset['TYPE_ORGA'][i] EQ "ope">
					<cfset currentParentKey = "OP_ORGANISATIONS">
				<cfelse>
					<cfset currentParentKey = "CLIENT_ORGANISATIONS">
				</cfif>
			</cfif>		
						
			<cfset parentChildCount = arrayLen(dataStruct['#currentParentKey#'].XmlChildren)>			
			<cfset dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset tmpChildNode = dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1]>
			<cfset tmpChildNode.XmlAttributes.LABEL = dataset['LIBELLE_GROUPE_CLIENT'][i]>
			<cfset tmpChildNode.XmlAttributes.NODE_ID = dataset['NODE_ID'][i]>
			<cfset tmpChildNode.XmlAttributes.LEVEL = dataset['NIVEAU'][i]> 
			<cfset tmpChildNode.XmlAttributes.TYPE_PERIMETRE = dataset['TYPE_PERIMETRE'][i]>
						
			<cfif dataset['NODE_ID'][i] lt idnode>
				<cfset tmpChildNode.XmlAttributes.BOOL_ORGA = 1 >
			<cfelse>
				<cfset tmpChildNode.XmlAttributes.BOOL_ORGA = dataset['BOOL_ORGA'][i]>
			</cfif>
			
			<cfset tmpChildNode.XmlAttributes.TYPE_ORGA = dataset['TYPE_ORGA'][i]>
			<cfset tmpChildNode.XmlAttributes.RAISON_SOCIALE = libelleGroupeClient>
			<cfset structInsert(dataStruct,currentKey,tmpChildNode)>
			
		</cfloop>  		
		<cfreturn perimetreXmlDoc>
	</cffunction>
	
	<cffunction name="searchNodeId" access="private" returntype="numeric">
		<cfargument name="idgroupeclient" type="numeric" required="true">
		<cfargument name="recordset" type="query" required="true">
		
		<cfloop index="i" from="1" to="#recordset.recordcount#">
			<cfif recordset['IDGROUPE_CLIENT'][i] eq idgroupeclient>
				<cfreturn recordset['NODE_ID'][i]>				
			</cfif>
		</cfloop>		
		<cfreturn  -1>
	</cffunction>
	
	<cffunction name="searchIdGroupe" access="private" returntype="numeric">
		<cfargument name="nodeId" type="numeric" required="true">
		<cfargument name="recordset" type="query" required="true">
		
		<cfloop index="i" from="1" to="#recordset.recordcount#">
			<cfif recordset['NODE_ID'][i] eq nodeId>
				<cfreturn recordset['IDGROUPE_CLIENT'][i]>				
			</cfif>
		</cfloop>		
		<cfreturn  -1>
	</cffunction>
	
	<cffunction name="ispresent" access="private" returntype="numeric" hint="contains(value : string the value to search\n, colname : string  the column name where to search into\n, data : query the query matrix)">
		<cfargument name="value" required="true" type="string">
		<cfargument name="colname" required="true" type="string">
		<cfargument name="data" required="true" type="query">		
		<cfset count = 0>
		<cfloop query="data">
			<cfif Evaluate("data."&colname) eq  value>				
				<cfset count = count + 1>
			</cfif>
		</cfloop>		
		<cfdump var="#count#">		
		<cfreturn count>
	</cffunction>
</cfcomponent>