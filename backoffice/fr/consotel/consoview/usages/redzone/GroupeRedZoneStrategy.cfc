<!--- =========================================================================
Classe: GrouperedzoneStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GroupeRedZoneStrategy"   extends="redzoneStrategy" >
   
	<cffunction name="getSites" access="public" returntype="query" output="false"  >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfstoredproc datasource="#session.offreDSN#" procedure="setTerritoire" debug="false" />
		<cfquery datasource="#session.offreDSN#" name="qGetSites">
			SELECT concat(concat(rc.libelle,' - '), rc.ref_client) AS libelle, rc.libelle as libelle_groupe_client,
				b.commune,a.nbre_site, b.zip, concat(concat(concat(dept.deptnom,' ('),b.zip),')') AS dep,
				b.sous_compte || ' - ' || b.adresse1 as adresse, b.siteID, rc.idref_client,b.commune || '|COMMUNE' as communekey,
				b.zip || '|ZIP' as zipkey, rc.idref_client as Idgroupe_Client
			FROM ref_client rc,
				dept,
				(SELECT COUNT(*) AS nbre_site, rc.Idref_Client
				FROM ref_client rc, compte_facturation cf, sous_compte sc, site_client scl
				WHERE rc.idref_client=cf.Idref_Client
				AND cf.Idcompte_Facturation=sc.idcompte_facturation
				AND scl.siteid=sc.siteid
				AND rc.idref_client=#ID#
				GROUP BY libelle, rc.Idref_Client
				ORDER BY nbre_site DESC) a,
				(SELECT rc.Idref_Client, decode(nvl(scl.commune,scl.Nom_Site),'Communes','Flotte Mobile',nvl(scl.commune,scl.Nom_Site)) AS commune,
					decode(decode(nvl(trim(scl.zipcode),'-1'),'-1','006'),'006','006',substr(scl.zipcode,0,2)) AS zip,
					scl.Adresse1 , 
					scl.nom_site, sc.sous_compte, scl.siteID
				FROM ref_client rc, compte_facturation cf, sous_compte sc, site_client scl
				WHERE rc.idref_client=cf.Idref_Client
				AND cf.Idcompte_Facturation=sc.idcompte_facturation
				AND scl.siteid=sc.siteid
				AND cf.idref_client=#ID#
				ORDER BY commune ASC) b
			WHERE rc.Idref_Client=a.idref_client
			AND rc.Idref_Client=b.Idref_Client
			AND rc.Idref_Client=#ID#
			AND b.zip=trim(dept.code)
			ORDER BY libelle_groupe_client,libelle, rc.idref_client, b.zip ASC, lower(trim(b.commune)) ASC, adresse
		</cfquery>
		<cfreturn qGetSites>
	</cffunction>

	<cffunction name="getData" access="remote" returntype="query" output="false"  >
		<cfargument name="data" required="false" type="array" default="" displayname="array data" hint="Initial value for the dataproperty." />
		<cfset d1=LsDateFormat(createDate(getToken(data[7],3,"/"),getToken(data[7],2,"/"),getToken(data[7],1,"/")),"yyyy/mm/dd")>
		<cfset d2=LsDateFormat(dateadd("m",1,createDate(getToken(data[8],3,"/"),getToken(data[8],2,"/"),getToken(data[8],1,"/"))),"yyyy/mm/dd")>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL.RZ_BYGROUPE">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#data[1]#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#d1#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_datefin" value="#d2#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_tri" value="#data[5]#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_paysconsotelid" value="#data[6]#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_data10" value="#data[10]#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_data11" value="#data[11]#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_data12" value="#data[12]#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_data13" value="#data[13]#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_jour" value="#data[9]#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_nbrow" value="#data[4]#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_display_number" value="#data[3]#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_opt_bysiteid" value="0"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_opt_byville_commune" value=""/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_opt_byville_zip" value=""/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_opt_byzip" value=""/>
		        <cfprocresult name="p_result"/>        
			</cfstoredproc>
			<!--- <cfloop from="1" to="#p_result.recordcount#" index="i">
				<cfset p_result.datedeb[i]=LsDateFormat(getData.datedeb[i],"dd/mm/yy")>
			</cfloop> --->
			<cfreturn p_result/>
	</cffunction>
	
	<cffunction name="getAppelsBySite" access="remote" returntype="query" output="false"  >
		<cfargument name="data" required="false" type="array" default="" displayname="array data" hint="Initial value for the dataproperty." />
		<cfif data[10] lte data[11]>
			<cfset operateur1="AND">
		<cfelse>
			<cfset operateur1="OR">
		</cfif>
		<cfif data[12] neq "" and data[12] lte data[13]>
			<cfset operateur2="AND">
		<cfelse>
			<cfset operateur2="OR">
		</cfif>
		<cfquery name="qGetAppels" datasource="#session.offreDSN#" maxrows="#data[4]#">
				SELECT   decode(substr(c.appelant,1,2),'33','0' || substr(c.appelant,3,length(c.appelant)-2),c.appelant) as cli, 
                    decode(substr(c.sda,1,2),'33','0' || substr(c.sda,3,length(c.sda)-2),c.sda) as sda,  
                    decode(#session.perimetre.flagdisplaynumber#,0,substr(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele),1,length(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele))-4) || 'XXXX',decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele)) as appele, (cout2/100) as Cout, c.duree, 
                    c.paysconsotelID as destination, datedeb, a.nom_site as site,
         to_char(c.datedeb,'hh24:mi:ss') as heuredeb, c.paysID,trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)) AS pays
				FROM   cdrcurrent@orccdr c, pays_consotel p, 
				
		             (      SELECT ofc.offreclientid, c.cli, scl.nom_site 
		                    FROM offre_client ofc, offre_client_site ofcs, site_client scl, cli c
		                    WHERE ofc.offreclientid=ofcs.offreclientid
                               AND scl.siteid=ofcs.siteid
                               AND scl.siteID=c.siteID
							   AND scl.siteID=#data[3]#
				            AND valide=1
			                AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(data[7],3,"/"),getToken(data[7],2,"/"),getToken(data[7],1,"/"))#)
				   			    AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(data[8],3,"/"),getToken(data[8],2,"/"),getToken(data[8],1,"/"))#) OR ofc.dateresiliation IS NULL)
			                    GROUP BY ofc.offreclientid, c.cli, scl.nom_site 
			             ) a
				
				WHERE c.offreclientID=a.offreclientid
				      AND c.paysconsotelid>0 
				      AND p.paysconsotelID=c.paysconsotelID
				      AND lower(trim(c.appelant))=lower(trim(a.cli))
				      AND trunc(c.datedeb,'MM') >= trunc(#CreateDate(getToken(data[7],3,"/"),getToken(data[7],2,"/"),getToken(data[7],1,"/"))#,'MM')
					AND trunc(c.datefin,'MM') <= trunc(#CreateDate(getToken(data[8],3,"/"),getToken(data[8],2,"/"),getToken(data[8],1,"/"))#,'MM')
					/* Restriction des destinations */
					<cfif data[6] lt 5 and data[6] gt 0>
						AND c.paysconsotelID=#data[6]#
					<cfelseif data[6] eq 5>
						AND c.paysconsotelID>=#data[6]#
					</cfif>
					/* Gestion des heures */
					<cfif data[12] neq "">
						<cfif data[10] lt data[12]>
							AND ((to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#' 
								  AND to_char(c.datedeb,'hh24:mi:ss')<='#data[12]#')
								  OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#' 
		 						  AND to_char(c.datedeb,'hh24:mi:ss')>='#data[13]#')
								)
						<cfelse>
							AND ((to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#') OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[12]#') OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#' 
		 						  AND to_char(c.datedeb,'hh24:mi:ss')>='#data[13]#')
								)
						</cfif>
					<cfelse>
						AND (to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#'
						#operateur1# to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#')
					</cfif>
					/* Gestion des types de jours */
					<cfswitch expression="#data[9]#">
						<cfcase value="0">
							AND to_number(to_char(c.datedeb,'D'))<=5
						</cfcase>
						<cfcase value="2">
							AND to_number(to_char(c.datedeb,'D'))>=6
						</cfcase>
					</cfswitch>
					 ORDER BY #data[5]# DESC
			</cfquery>
			<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
				<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd/mm/yy")>
			</cfloop>
			<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getAppelsByRefClient" access="remote" returntype="query" output="false"  >
		<cfargument name="data" required="false" type="array" default="" displayname="array data" hint="Initial value for the dataproperty." />
		<cfif data[10] lte data[11]>
			<cfset operateur1="AND">
		<cfelse>
			<cfset operateur1="OR">
		</cfif>
		<cfif data[12] neq "" and data[12] lte data[13]>
			<cfset operateur2="AND">
		<cfelse>
			<cfset operateur2="OR">
		</cfif>
		<cfquery name="qGetAppels" datasource="#session.offreDSN#" maxrows="#data[4]#">
				SELECT   decode(substr(c.appelant,1,2),'33','0' || substr(c.appelant,3,length(c.appelant)-2),c.appelant) as cli, 
                    decode(substr(c.sda,1,2),'33','0' || substr(c.sda,3,length(c.sda)-2),c.sda) as sda,  
                    decode(#session.perimetre.flagdisplaynumber#,0,substr(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele),1,length(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele))-4) || 'XXXX',decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele)) as appele, (cout2/100) as Cout, c.duree, 
                    c.paysconsotelID as destination, datedeb, a.nom_site as site,
         to_char(c.datedeb,'hh24:mi:ss') as heuredeb, c.paysID,trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)) AS pays
				FROM   cdrcurrent@orccdr c, pays_consotel p, 
				
		             (      SELECT ofc.offreclientid, c.cli, scl.nom_site 
		                    FROM offre_client ofc, offre_client_site ofcs, site_client scl, cli c, sous_compte sco, compte_facturation cf
		                    WHERE ofc.offreclientid=ofcs.offreclientid
                               AND scl.siteid=ofcs.siteid
				               AND scl.siteID=sco.siteID
                               AND scl.siteID=c.siteID
                               AND sco.idcompte_facturation=cf.idcompte_facturation
                               AND cf.Idref_Client=#data[1]#
				            AND valide=1
			                AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(data[7],3,"/"),getToken(data[7],2,"/"),getToken(data[7],1,"/"))#)
				   			    AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(data[8],3,"/"),getToken(data[8],2,"/"),getToken(data[8],1,"/"))#) OR ofc.dateresiliation IS NULL)
			                    GROUP BY ofc.offreclientid, c.cli, scl.nom_site 
			             ) a
				
				WHERE c.offreclientID=a.offreclientid
				      AND c.paysconsotelid>0 
				      AND p.paysconsotelID=c.paysconsotelID
				      AND lower(trim(c.appelant))=lower(trim(a.cli))
				      AND trunc(c.datedeb,'MM') >= trunc(#CreateDate(getToken(data[7],3,"/"),getToken(data[7],2,"/"),getToken(data[7],1,"/"))#,'MM')
					AND trunc(c.datefin,'MM') <= trunc(#CreateDate(getToken(data[8],3,"/"),getToken(data[8],2,"/"),getToken(data[8],1,"/"))#,'MM')
					/* Restriction des destinations */
					<cfif data[6] lt 5 and data[6] gt 0>
						AND c.paysconsotelID=#data[6]#
					<cfelseif data[6] eq 5>
						AND c.paysconsotelID>=#data[6]#
					</cfif>
					/* Gestion des heures */
					<cfif data[12] neq "">
						<cfif data[10] lt data[12]>
							AND ((to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#' 
								  AND to_char(c.datedeb,'hh24:mi:ss')<='#data[12]#')
								  OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#' 
		 						  AND to_char(c.datedeb,'hh24:mi:ss')>='#data[13]#')
								)
						<cfelse>
							AND ((to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#') OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[12]#') OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#' 
		 						  AND to_char(c.datedeb,'hh24:mi:ss')>='#data[13]#')
								)
						</cfif>
					<cfelse>
						AND (to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#'
						#operateur1# to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#')
					</cfif>
					/* Gestion des types de jours */
					<cfswitch expression="#data[9]#">
						<cfcase value="0">
							AND to_number(to_char(c.datedeb,'D'))<=5
						</cfcase>
						<cfcase value="2">
							AND to_number(to_char(c.datedeb,'D'))>=6
						</cfcase>
					</cfswitch>
					 ORDER BY #data[5]# DESC, datedeb, heuredeb
			</cfquery>
			
			<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
				<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd/mm/yy")>
			</cfloop>
			<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getAppelsByVille" access="remote" returntype="query" output="false"  >
		<cfargument name="data" required="false" type="array" default="" displayname="array data" hint="Initial value for the dataproperty." />
		<cfif data[10] lte data[11]>
			<cfset operateur1="AND">
		<cfelse>
			<cfset operateur1="OR">
		</cfif>
		<cfif data[12] neq "" and data[12] lte data[13]>
			<cfset operateur2="AND">
		<cfelse>
			<cfset operateur2="OR">
		</cfif>
		<cfquery name="qGetAppels" datasource="#session.offreDSN#" maxrows="#data[4]#">
				SELECT   decode(substr(c.appelant,1,2),'33','0' || substr(c.appelant,3,length(c.appelant)-2),c.appelant) as cli, 
                    decode(substr(c.sda,1,2),'33','0' || substr(c.sda,3,length(c.sda)-2),c.sda) as sda,  
                    decode(#session.perimetre.flagdisplaynumber#,0,substr(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele),1,length(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele))-4) || 'XXXX',decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele)) as appele, (cout2/100) as Cout, c.duree, 
                    c.paysconsotelID as destination, datedeb, a.nom_site as site,
         to_char(c.datedeb,'hh24:mi:ss') as heuredeb, c.paysID,trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)) AS pays
				FROM   cdrcurrent@orccdr c, pays_consotel p, 
				
		             (      SELECT ofc.offreclientid, c.cli, scl.nom_site 
		                    FROM offre_client ofc, offre_client_site ofcs, site_client scl, cli c, sous_compte sco, compte_facturation cf
		                    WHERE ofc.offreclientid=ofcs.offreclientid
                               AND scl.siteid=ofcs.siteid
				               AND scl.siteID=sco.siteID
                               AND scl.siteID=c.siteID
				               AND sco.idcompte_facturation=cf.idcompte_facturation
				               AND lower(trim(scl.commune))=lower(trim('#data[3]#'))
				               AND lower(substr(scl.zipcode,0,2))=lower('#data[14]#')
				               AND cf.idref_client=#data[1]#
				            AND valide=1
			                AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(data[7],3,"/"),getToken(data[7],2,"/"),getToken(data[7],1,"/"))#)
				   			    AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(data[8],3,"/"),getToken(data[8],2,"/"),getToken(data[8],1,"/"))#) OR ofc.dateresiliation IS NULL)
			                    GROUP BY ofc.offreclientid, c.cli, scl.nom_site 
			             ) a
				
				WHERE c.offreclientID=a.offreclientid
				      AND c.paysconsotelid>0 
				      AND p.paysconsotelID=c.paysconsotelID
				      AND lower(trim(c.appelant))=lower(trim(a.cli))
				      AND trunc(c.datedeb,'MM') >= trunc(#CreateDate(getToken(data[7],3,"/"),getToken(data[7],2,"/"),getToken(data[7],1,"/"))#,'MM')
					AND trunc(c.datefin,'MM') <= trunc(#CreateDate(getToken(data[8],3,"/"),getToken(data[8],2,"/"),getToken(data[8],1,"/"))#,'MM')
					/* Restriction des destinations */
					<cfif data[6] lt 5 and data[6] gt 0>
						AND c.paysconsotelID=#data[6]#
					<cfelseif data[6] eq 5>
						AND c.paysconsotelID>=#data[6]#
					</cfif>
					/* Gestion des heures */
					<cfif data[12] neq "">
						<cfif data[10] lt data[12]>
							AND ((to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#' 
								  AND to_char(c.datedeb,'hh24:mi:ss')<='#data[12]#')
								  OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#' 
		 						  AND to_char(c.datedeb,'hh24:mi:ss')>='#data[13]#')
								)
						<cfelse>
							AND ((to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#') OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[12]#') OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#' 
		 						  AND to_char(c.datedeb,'hh24:mi:ss')>='#data[13]#')
								)
						</cfif>
					<cfelse>
						AND (to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#'
						#operateur1# to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#')
					</cfif>
					/* Gestion des types de jours */
					<cfswitch expression="#data[9]#">
						<cfcase value="0">
							AND to_number(to_char(c.datedeb,'D'))<=5
						</cfcase>
						<cfcase value="2">
							AND to_number(to_char(c.datedeb,'D'))>=6
						</cfcase>
					</cfswitch>
					 ORDER BY #data[5]# DESC
			</cfquery>
			<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
				<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd/mm/yy")>
			</cfloop>
			<cfreturn qGetAppels /> 
	</cffunction>

	<cffunction name="getAppelsByZip" access="remote" returntype="query" output="false"  >
		<cfargument name="data" required="false" type="array" default="" displayname="array data" hint="Initial value for the dataproperty." />
		<cfif data[10] lte data[11]>
			<cfset operateur1="AND">
		<cfelse>
			<cfset operateur1="OR">
		</cfif>
		<cfif data[12] neq "" and data[12] lte data[13]>
			<cfset operateur2="AND">
		<cfelse>
			<cfset operateur2="OR">
		</cfif>
		<cfquery name="qGetAppels" datasource="#session.offreDSN#" maxrows="#data[4]#">
				SELECT   decode(substr(c.appelant,1,2),'33','0' || substr(c.appelant,3,length(c.appelant)-2),c.appelant) as cli, 
                    decode(substr(c.sda,1,2),'33','0' || substr(c.sda,3,length(c.sda)-2),c.sda) as sda,  
                    decode(#session.perimetre.flagdisplaynumber#,0,substr(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele),1,length(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele))-4) || 'XXXX',decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele)) as appele, (cout2/100) as Cout, c.duree, 
                    c.paysconsotelID as destination, datedeb, a.nom_site as site,
         to_char(c.datedeb,'hh24:mi:ss') as heuredeb, c.paysID,trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)) AS pays
				FROM   cdrcurrent@orccdr c, pays_consotel p, 
				
		             (      SELECT ofc.offreclientid, c.cli, scl.nom_site 
		                    FROM offre_client ofc, offre_client_site ofcs, site_client scl, cli c, sous_compte sco, compte_facturation cf
		                    WHERE ofc.offreclientid=ofcs.offreclientid
                               AND scl.siteid=ofcs.siteid
				               AND scl.siteID=sco.siteID
                               AND scl.siteID=c.siteID
				               AND sco.idcompte_facturation=cf.idcompte_facturation
				               AND lower(substr(scl.zipcode,0,2))=lower('#data[3]#')
				               AND cf.idref_client=#data[1]#
				            AND valide=1
			                AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(data[7],3,"/"),getToken(data[7],2,"/"),getToken(data[7],1,"/"))#)
				   			    AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(data[8],3,"/"),getToken(data[8],2,"/"),getToken(data[8],1,"/"))#) OR ofc.dateresiliation IS NULL)
			                    GROUP BY ofc.offreclientid, c.cli, scl.nom_site 
			             ) a
				WHERE c.offreclientID=a.offreclientid
				      AND c.paysconsotelid>0 
				      AND p.paysconsotelID=c.paysconsotelID
				      AND lower(trim(c.appelant))=lower(trim(a.cli))
				      AND trunc(c.datedeb,'MM') >= trunc(#CreateDate(getToken(data[7],3,"/"),getToken(data[7],2,"/"),getToken(data[7],1,"/"))#,'MM')
					AND trunc(c.datefin,'MM') <= trunc(#CreateDate(getToken(data[8],3,"/"),getToken(data[8],2,"/"),getToken(data[8],1,"/"))#,'MM')
					/* Restriction des destinations */
					<cfif data[6] lt 5 and data[6] gt 0>
						AND c.paysconsotelID=#data[6]#
					<cfelseif data[6] eq 5>
						AND c.paysconsotelID>=#data[6]#
					</cfif>
					/* Gestion des heures */
					<cfif data[12] neq "">
						<cfif data[10] lt data[12]>
							AND ((to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#' 
								  AND to_char(c.datedeb,'hh24:mi:ss')<='#data[12]#')
								  OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#' 
		 						  AND to_char(c.datedeb,'hh24:mi:ss')>='#data[13]#')
								)
						<cfelse>
							AND ((to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#') OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[12]#') OR
								 (to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#' 
		 						  AND to_char(c.datedeb,'hh24:mi:ss')>='#data[13]#')
								)
						</cfif>
					<cfelse>
						AND (to_char(c.datedeb,'hh24:mi:ss')>='#data[10]#'
						#operateur1# to_char(c.datedeb,'hh24:mi:ss')<='#data[11]#')
					</cfif>
					/* Gestion des types de jours */
					<cfswitch expression="#data[9]#">
						<cfcase value="0">
							AND to_number(to_char(c.datedeb,'D'))<=5
						</cfcase>
						<cfcase value="2">
							AND to_number(to_char(c.datedeb,'D'))>=6
						</cfcase>
					</cfswitch>
					 ORDER BY #data[5]# DESC
			</cfquery>
			<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
				<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd/mm/yy")>
			</cfloop>
			<cfreturn qGetAppels /> 
	</cffunction>

</cfcomponent>