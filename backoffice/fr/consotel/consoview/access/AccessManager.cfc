<cfcomponent name="AccessManager" alias="fr.consotel.consoview.access.AccessManager">
	<cfset THIS.AUTH_DSN = "ROCOFFRE">
	<cfset THIS.PKG_GLOBAL = "PKG_CV_GLOBAL">
	<cfset THIS.ROOT_TREE ="ROOT TREE">
	<cfset THIS.NODE_TREE ="NODE TREE">
	<cfset THIS.DEFAULT_UNIVERS ="HOME">
	<cfset THIS.CONNECT_CLIENT ="#THIS.PKG_GLOBAL#.connectClient">

	<cfset THIS.PILOTAGEAPPID = "pilotage">
	<cfset THIS.URLWEBSERVICESSOSFR = "https://sso.sfr.com/extranet_sso/services/SsoService.wsdl">
	

	<cffunction name="getLastCodeStyle" access="remote" output="false" returntype="string">
		<cfif structKeyExists(COOKIE,"CODE_STYLE")>
			<cfreturn COOKIE.CODE_STYLE>
		<cfelse>
			<cfreturn "NO_STYLE">
		</cfif>
	</cffunction> 
	<cffunction name="getLastCodePays" access="remote" output="false" returntype="string">
		<cfif structKeyExists(COOKIE,"CODE_PAYS")>
			<cfreturn COOKIE.CODE_PAYS>
		<cfelse>
			<cfreturn "NO_CODE_PAYS">
		</cfif>
	</cffunction> 
	<cffunction name="getLastLogin" access="remote" output="false" returntype="string">
		<cfset var defaultLogin="">
		<cftry>
			<cfif structKeyExists(COOKIE,"LAST_LOGIN")>
				<cfreturn COOKIE.LAST_LOGIN>
			<cfelse>
				<cfreturn defaultLogin>
			</cfif>
			<cfcatch type="any">
				<cftry>
					<cfset var mailSubject="Exception rencontrée durant la récupération du dernier login connecté à ConsoView">
					<cfif isDefined("CFCATCH.message") AND (lCase(CFCATCH.message) EQ lCase("Session is invalid"))>
						<cfset mailSubject="Session ConsoView invalidée">
					</cfif>
					<cfmail from="consoview@consotel.fr" to="monitoring@saaswedo.com" subject="#mailSubject# sur le backoffice : #CGI.SERVER_NAME#" type="html">
						Une exception a été rencontrée durant la récupération du dernier login utilisé par un utilisateur de ConsoView (Cookie)<br>
						Une des cause connue pour une telle exception est décrite dans le ticket : http://nathalie.consotel.fr/issues/5832<br>
						<cfoutput>
							Cible de la requete HTTP : #CGI.SCRIPT_NAME#<br>
							Adresse cliente : #CGI.REMOTE_HOST#<br>
							<cfif isDefined("APPLICATION") AND isDefined("APPLICATION.applicationName")>
								Application correspondante : #APPLICATION.applicationName#<br>
							</cfif>
						</cfoutput>
						<cfif isDefined("SESSION")>
							<cfdump var="#CFCATCH#" label="Session correspondante"><br>
						</cfif>
						<cfdump var="#CFCATCH#" label="Exception capturée"> 
					</cfmail>
					<cfset structClear(COOKIE)>
					<cfcatch type="any">
						<cfmail from="consoview@consotel.fr" to="monitoring@saaswedo.com" subject="Erreur lors de l'invalidation d'un cookie sur le backoffice : #CGI.SERVER_NAME#" type="html">
							Une exception a été rencontrée durant l'invalidation d'un cookie ConsoView<br>
							L'invalidation du cookie a été spécifiée par le ticket : http://nathalie.consotel.fr/issues/5832<br>
							<cfoutput>
								Cible de la requete HTTP : #CGI.SCRIPT_NAME#<br>
								Adresse cliente : #CGI.REMOTE_HOST#<br>
								<cfif isDefined("APPLICATION") AND isDefined("APPLICATION.applicationName")>
									Application correspondante : #APPLICATION.applicationName#<br>
								</cfif>
							</cfoutput>
							<cfif isDefined("SESSION")>
								<cfdump var="#SESSION#" label="Session correspondante"><br>
							</cfif>
							<cfdump var="#CFCATCH#" label="Exception capturée"> 
						</cfmail>
						<cfreturn defaultLogin>
					</cfcatch>
				</cftry>
				<cfreturn defaultLogin>
			</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="clearSession" access="remote" returntype="numeric">
		<cfset structDelete(SESSION,"OFFREDSN")>
		<cfset structDelete(SESSION,"CDRDSN")>
		<cfset structDelete(SESSION,"PARCVIEWDSN")>
		<cfset structDelete(SESSION,"MAILSERVER")>
		<cfset structDelete(SESSION,"XML_ACCESS")>
		<cfset structDelete(SESSION,"XML_ACCESS_STATUS")>
		<cfset structDelete(SESSION,"UNIVERS_KEY")>
		<cfset structDelete(SESSION,"FUNCTION_KEY")>
		<cfif structKeyExists(SESSION,"USER")>
			<cfset structClear(SESSION.USER)>
			<cfset structDelete(SESSION,"USER")>
		</cfif>
		<cfif structKeyExists(SESSION,"DATES")>
			<cfset structClear(SESSION.DATES)>
			<cfset structDelete(SESSION,"DATES")>
		</cfif>
		<cfif structKeyExists(SESSION,"PERIMETRE")>
			<cfif structKeyExists(SESSION.PERIMETRE,"STRUCTDATE")>
				<cfset structClear(SESSION.PERIMETRE.STRUCTDATE)>
			</cfif>
			<cfset structClear(SESSION.PERIMETRE)>
			<cfset structDelete(SESSION,"PERIMETRE")>
		</cfif>
	   	<cflog type="Information" text="SESSION/#SESSION.IP_ADDR#/#CGI.REMOTE_ADDR# #SESSION.SESSIONID# - Cleared on #APPLICATION.ApplicationName#">
		<cfif SESSION.IP_ADDR EQ CGI.REMOTE_ADDR>
		   	<cfreturn 1>
		<cfelse>
		   	<cfreturn 0>
		</cfif>
	</cffunction>
	<cffunction name="logoff" access="remote" returntype="numeric">
		<cfargument name="userEmail" required="true" type="string">
		<cflog type="Information" text="SESSION/#SESSION.IP_ADDR#/#CGI.REMOTE_ADDR# #SESSION.SESSIONID# - LogOff on #APPLICATION.ApplicationName#">
		<cfset structDelete(SESSION,"AUTH_STATE")>
		<cfset structDelete(SESSION,"FAILED_AUTH")>
		<cfset structDelete(SESSION,"EXCEPTION")>
		<cfset sameUser=-1>
		<cfif structKeyExists(SESSION,"USER")>
			<cfif structKeyExists(SESSION.USER,"EMAIL")>
				<cfif userEmail EQ SESSION.USER.EMAIL>
					<cfset sameUser=1>
				<cfelse>
					<cfset sameUser=2>
				</cfif>
			<cfelse>
				<cfset sameUser=3>
			</cfif>
		<cfelse>
			<cfset sameUser=0>
		</cfif>
		<cfset clearVal=clearSession()>
		<cfreturn sameUser>
	</cffunction>
	<cffunction name="connectSSO" access="remote" output="false" returntype="any">
		<cfargument name="tokenSSO" required="true" type="string">
		
		<cfif isDefined("SESSION")>
			<cftry>
				<!--- 1) DEFINITION DE VARIABLES PRIVEES --->
				<cfset SSOCle = "">
				<cfset SSOEmail = "">
				<cfset SSOListeTitu = "">
				<cfset SSOVerif = 1>
				<cfset SSOip = #COOKIE.IP_ORIGINE#>
				
				<!--- 2) NETTOYAGE DE LA SESSION SI BESOIN --->
				<cfif structKeyExists(SESSION,"AUTH_STATE")>
					<cfif SESSION.AUTH_STATE EQ 1>
					   	<cflog type="Information" text="AUTH REQUEST WITH CONNECTED SESSION">
						<cfset clearSession()>
					</cfif>
				</cfif>
				<!---3) MIS A JOUR DE DONNEES EN SESSION/COOKIE --->		
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 51 >
				<cfset COOKIE.CODEAPPLICATION = 51 >
				<!--- 4) RECUPERATION DE L'OBJET POL VIA LE WEBSERVICE SSO --->
				<cfset objPolToken = getPolToken(tokenSSO,THIS.PILOTAGEAPPID,SSOip)>
				<!--- 5)a) VERIFICATION DES DONNEES RECUES : POLToken est une structure ? --->
				<cfif objPolToken neq -1 > 
					<!--- 5)a) VERIFICATION DE LA CLE SSO --->
					<cfset SSOVerifCleSSO = 0>
					<cfset vis = objPolToken["visibilite"]>	
					<cfif vis neq "">
						<cfset SSOCle = objPolToken["visibilite"]> 
						<cfset SSOVerifCleSSO = 1>		
					</cfif>
					<cfset SSOVerif = SSOVerif * SSOVerifCleSSO>
						<!--- 6) CONNECTION VIA CLE SSO --->
						<!--- 6)a) SI LA VERIF EST BONNE, on tente de se connecter via la CléSSO  --->
						<cfif SSOVerifCleSSO eq 1>
							<cfset authSSOResult = validateLoginByCleSSOForSFR(SSOCle)>
							<!--- 7) VERIFICATION DES DONNEES RECUES : AUTH_RESULT = 1 ? --->
								<cfif authSSOResult.AUTH_RESULT eq 1>
								<!--- 7)a) l'utilisateur est authentifié, on envoit les informations de l'utilisateur au front --->
									<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-001] Connection réussie à pilotage" to="monitoring@saaswedo.com"  type="text/html" >
										<cfoutput>
											La connection est réussie.
											<br>
											CléSSO : #SSOCle#
											<br>
											ip : #SSOip#
											<br>
											token : #tokenSSO#
											<br>
										</cfoutput>
									</cfmail>
									<cfreturn authSSOResult>
								<!--- 7)b) la clé SSO est introuvable ou autres erreurs ( login expiré ) --->
								<cfelse>
									<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-002] Connection impossible : #SSOCle#" to="monitoring@saaswedo.com"  type="text/html" >
										<cfoutput>
											Cette clé SSO #SSOCle# ne permet pas de se logger à Pilotage.
											<br>
											Tentative de connection par email et liste de titus.
											
											<br><br>
											authSSOResult = <cfif isDefined("authSSOResult")><cfdump var="#authSSOResult#"><cfelse>authSSOResult not defined</cfif>
											authSSOResult = <cfif isDefined("objPolToken")><cfdump var="#objPolToken#"><cfelse>objPolToken not defined</cfif>
										</cfoutput>
									</cfmail>
									<cfset result = validateLoginByListeTituAndMail(objPolToken)/>
									<cfif isStruct(result) AND structKeyExists(result,"AUTH_RESULT")>
										<cfif result["AUTH_RESULT"] eq -10>
											<cfset result = connectToSSOetape3(objPolToken)/>
										</cfif>
									</cfif>	
									<cfreturn result>
								</cfif>	
						<!--- 6)b) SI LA VERIF EST INCORRECTE, on léve une exception --->
						<cfelse>
							<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-002] Connection impossible : #SSOCle#" to="monitoring@saaswedo.com"  type="text/html" >
								<cfoutput>
									Cette clé SSO #SSOCle# ne permet pas de se logger à Pilotage.
									<br>
									Tentative de connection par email et liste de titus.
								</cfoutput>
							</cfmail>
							<cfset result = validateLoginByListeTituAndMail(objPolToken)/>
							<cfif isStruct(result) AND structKeyExists(result,"AUTH_RESULT")>
								<cfif result["AUTH_RESULT"] eq -10>
									<cfset result = connectToSSOetape3(objPolToken)/>
								</cfif>
							</cfif>	
							<cfreturn result>
						</cfif>
				<!--- 5)b) VERIFICATION DES DONNEES RECUES : POLToken est une exception --->
				<cfelse>
					<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-005] PolToken invalide" to="monitoring@saaswedo.com"  type="text/html" >
						<cfoutput>
							objPolToken reçu est incorrect.
							<br>
						</cfoutput>
					</cfmail>
					<cfreturn -1>
				</cfif>
			<cfcatch type="any" >	
				<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-009] CFCATCH de ConnectSSO" to="monitoring@saaswedo.com"  type="text/html" >
					<cfdump var="#cfcatch#">
				</cfmail>
				<cfreturn -1000>	
			</cfcatch>		
			</cftry>
		<cfelse>
			<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-008] SESSION IS UNDEFINED" to="monitoring@saaswedo.com"  type="text/html" >
				<cfoutput >
					La session n'est pas définie.<br><br>
					Impossible d'établir une connection SSO
				</cfoutput>
			</cfmail>
			<cfthrow message="-999">
		</cfif>
	</cffunction>
	<cffunction name="connectToSSOetape3" 
				access="private" 
				returntype="Any" 
				output="false" 
				description="se connecte à pilotage en SSO etape 3 : création login à partir des titus">
		<cfargument name="objConnection" required="true" type="Any" >

		<cfset listeTitu = "">
		<cfif structKeyExists(objPolToken,"porteeTituListe")>
			<cfset porteeTitu = objPolToken["porteeTituListe"]["porteeTitu"]>
			
			<cfif isDefined("porteeTitu")>
				<cfset taillePorteeTituListe = arraylen(porteeTitu)>
				<cfif  taillePorteeTituListe gt 0>
					<cfloop index="idx" from="1" to="#taillePorteeTituListe#">	
						<cfset item = porteeTitu[#idx#]["titu"]>
						<cftry>
							<cfif listeTitu neq "">
								<cfset listeTitu = '#listeTitu#,''#item#'''/>
							<cfelse>
								<cfset listeTitu = '''#item#'''/>
							</cfif>
						<cfcatch type="any" >
						</cfcatch>
						</cftry>
					</cfloop>
				</cfif>
			</cfif>
		</cfif>
		<cfset userInfos = structNew()>		
		<cfset userInfos.OFFREDSN = SESSION.OffreDSN>
		<cfset userInfos.AUTH_RESULT = -1>
		<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-006] Dump SSO Etape 3" to="monitoring@saaswedo.com"  type="text/html" >
				<cfoutput >
					objConnection : <cfdump var="#objConnection#"><br>
					listeTitu : <cfdump var="#listeTitu#" >
				</cfoutput>
			</cfmail>
		<cftry>
			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.connectClient_v5">
				<cfprocparam type="in" value="#objConnection.email#" 			cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="in" value="#objConnection.nom#" 				cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="in" value="#objConnection.prenom#" 			cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="in" value="#objConnection.telMobile#" 		cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="in" value="#objConnection.visibilite#" 		cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="in" value="#listeTitu#" 	cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="in" value="51" 		cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qValidateLogin">
			</cfstoredproc>
			
			<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-006] Dump SSO Etape 3" to="monitoring@saaswedo.com"  type="text/html" >
				<cfoutput >
					qValidateLogin : <cfdump var="#qValidateLogin#"><br>
					p_retour : #p_retour#
				</cfoutput>
			</cfmail>
			
			<cfset userInfos.AUTH_RESULT = p_retour>
	
			<cfif qValidateLogin.recordcount EQ 1>
				<cfset boolProcessLogin=0>
				<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
				<cfif boolCheckIP EQ 1>
					<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
					<cfif checkIpResult GT 0>
						<cfset boolProcessLogin=1>
					<cfelse>
						<cfset boolProcessLogin=0>
						<cfset userInfos.AUTH_RESULT = 2>
						<cflog type="Information" text="AUTH REQUEST WITH INVALID IP ADDR">
					</cfif>
				<cfelse>
					<cfset boolProcessLogin=2>
				</cfif>
				<cfif boolProcessLogin GT 0>		
					
					<cfset SESSION.AUTH_STATE = 1>
					<cfset SESSION.CDRDSN = "ROCCDR">
					<cfset SESSION.PARCVIEWDSN = "CLPARCVIEW"><cfset SESSION.MAILSERVER = "mail.consotel.fr:26">
					<cfset SESSION.USER = structNew()>
					<cfset SESSION.USER.NOM = qValidateLogin['NOM'][1]>
					<cfset SESSION.USER.PRENOM = qValidateLogin['PRENOM'][1]>
					<cfset SESSION.USER.EMAIL = qValidateLogin['EMAIL'][1]>
					<cfset SESSION.USER.CLIENTACCESSID = qValidateLogin['CLIENTACCESSID'][1]>
					<cfset SESSION.USER.CLIENTID =  qValidateLogin['ID'][1]>
					<cfset SESSION.USER.IDTYPEPROFIL =  qValidateLogin['TYPE_PROFIL'][1]>
					<cfset SESSION.USER.LIBELLETYPEPROFIL =  qValidateLogin['LIBELLE_TYPE_PROFIL'][1]>								
					<cfset SESSION.USER.IDREVENDEUR =  qValidateLogin['IDREVENDEUR'][1]>
					<cfset SESSION.USER.NOM_REVENDEUR =  qValidateLogin['NOM_REVENDEUR'][1]>
					<cfset SESSION.USER.LIBELLE =  qValidateLogin['LIBELLE'][1]>
					<cfset userInfos.AUTH_RESULT = 1>
					<cfset userInfos.NOM = SESSION.USER.NOM>
					<cfset userInfos.PRENOM = SESSION.USER.PRENOM>
					<cfset userInfos.EMAIL = SESSION.USER.EMAIL>
					
					<cfset setupGlobalization(qValidateLogin)>				
					
					<cfset userInfos.CLIENTID = SESSION.USER.CLIENTID>
					<cfset userInfos.CODEAPPLICATION = SESSION.CODEAPPLICATION>
					<cfset userInfos.CLIENTACCESSID = SESSION.USER.CLIENTACCESSID>
					<cfset userInfos.GLOBALIZATION = SESSION.USER.GLOBALIZATION>
					
					<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
					
					<cfset initSessionXmlAccess()> 
				<cfelse>
					<cfset SESSION.FAILED_AUTH = 1>
					<cfset SESSION.AUTH_STATE = 4>
				</cfif>
			<cfelse>
				<cfset SESSION.FAILED_AUTH = 1>
				<cfset SESSION.AUTH_STATE = 3>
			</cfif>	
		<cfcatch type="any"  > 	
			<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-006] Dump SSO Etape 3" to="monitoring@saaswedo.com"  type="text/html" >
				<cfoutput >
					cfcatch : <cfdump var="#cfcatch#"><br>
				</cfoutput>
			</cfmail>
			<cfset userInfos.AUTH_RESULT = -1000>
			<cfset userInfos.CFCATCH = CFCATCH>
		</cfcatch>
		</cftry>
		<cfreturn userInfos>
	</cffunction>	
<!--- FONCTIONS PROPRES A LA CONNECTION SSO SFR--->	
	<cffunction name="getPolToken" 
			access="private" 
			returntype="Any" 
			description="Interroge la base de données SSO SFR pour récupèrer l'objet POL">
		<cfargument name="tokenID" 	type="string" 	required="true" >
		<cfargument name="appID" 	type="string" 	required="true" >
		<cfargument name="IP" 		type="string" 	required="true" >
		
		<cftry>
			<cfset tok = replace(tokenID,'##',"")>
			<cfset strParam = structNew()>
			<cfset strParam.tokenId = tok>
			<cfset strParam.appId = appID>
			<cfset strParam.ip = IP>
			
			<cfset webService=createObject("webservice",THIS.URLWEBSERVICESSOSFR)>
			<cfset resObjPOlToken = webService.validatePolToken(strParam)>
				
			<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-004] Status validatePolToken" to="monitoring@saaswedo.com"  type="text/html" >
				<cfoutput>
					<br>
					<cfif isDefined("strParam")><cfdump var="#strParam#"><cfelse>strParam undefined</cfif><br>
					<cfif isDefined("resObjPOlToken")><cfdump var="#resObjPOlToken#"><cfelse>resObjPOlToken undefined</cfif><br>
				</cfoutput>
			</cfmail>

			<cfif isDefined("resObjPOlToken") eq TRUE > 
				<cfreturn resObjPOlToken>
			<cfelse>
				<cfreturn -1>
			</cfif>
		<cfcatch type="any" >	
			<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-003] WebService Erreur" to="monitoring@saaswedo.com"  type="text/html" >
				<cfoutput>
					Une erreur est survenue pendant l'appel du webService.
					<br>
					#cfcatch.Detail# : #cfcatch.Message#
				</cfoutput>
			</cfmail>
			<cfreturn -1>			
		</cfcatch>
		</cftry>
	</cffunction>
<!--- LES DIFFERENTS VALIDATELOGIN --->	
	<cffunction name="validateLoginByListeTituAndMail" 
				access="private" 
				output="false" 
				description="retourne l'objet USER à partir du mail et de la liste des titus fournis par SFR"
				returntype="Any">
		<cfargument name="objPolToken" required="true" type="any" >
		
		<cftry>
			<!---- 1) VERIFICATION DES DONNEES --->
			<cfset SSOVerif = 1>
				<!--- 1)a) VERIFICATION DU MAIL --->
			<cfset SSOVerifMailSSO = 0>
			<cfif structKeyExists(objPolToken,"email")>
				<cfif isValid("email",objPolToken["email"]) >
					<cfset SSOEmail = objPolToken["email"]> 
					<cfset SSOVerifMailSSO = 1>
				</cfif>		
			</cfif>	
			<cfset SSOVerif = SSOVerif * SSOVerifMailSSO>
				<!--- 1)b) VERIFICATION DE LA LISTE DE TITUS --->
			<cfset SSOVerifListeTitu = 0>
			<cfif structKeyExists(objPolToken,"porteeTituListe")>
				<cfset porteeTitu = objPolToken["porteeTituListe"]["porteeTitu"]>
				
				<cfif isDefined("porteeTitu")>
					<cfset listeTitu = "">
					<cfset taillePorteeTituListe = arraylen(porteeTitu)>
					<cfif  taillePorteeTituListe gt 0>
						<cfloop index="idx" from="1" to="#taillePorteeTituListe#">	
							<cfset item = porteeTitu[#idx#]["titu"]>
							<cftry>
								<cfif listeTitu neq "">
									<cfset listeTitu = '#listeTitu#,''#item#'''/>
								<cfelse>
									<cfset listeTitu = '''#item#'''/>
								</cfif>
								<cfset SSOVerifListeTitu = SSOVerifListeTitu * 1>
							<cfcatch type="any" >
								<cfset SSOVerifListeTitu = SSOVerifListeTitu * 0>
							</cfcatch>
							</cftry>
						</cfloop>
					</cfif>
				<cfelse>
					<cfset SSOVerifListeTitu = 0>
				</cfif>	
			</cfif>
			<!--- 2) CONNECTION --->
			<!--- 2)a) Si les données sont valables, on tente de se connecter ---> 
			<cfif SSOVerif eq 1>
				<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.connectClient_v4">
					<cfprocparam type="in" value="#SSOEmail#" 		cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="in" value="#listeTitu#" 	cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="qValidateLogin">
				</cfstoredproc>
			
				<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-007] Dump du formatage de la liste des titus" to="monitoring@saaswedo.com"  type="text/html" >
					<cfoutput >
						<cfdump var="#SSOEmail#" label="SSOEmail :"><br><br>
						<cfdump var="#listeTitu#" label="listeTitu :"><br><br>
						<cfdump var="#p_retour#" label="p_retour :"><br><br>
						<cfdump var="#qValidateLogin#" label="qValidateLogin :"><br><br>
					</cfoutput>
				</cfmail>
			
				<!--- 
					p_retour:=  0; le login/pwd est bon mais il est expire
					p_retour:=  1; le login/pwd est bon
					p_retour:=  2; le login/pwd est bon mais il est bloque ou le login/pwd n'est pas bon et on l'a bloque
					p_retour:=  3: Les titulaires appartiennent à plus d'une racine
					p_retour:=  4: Aucune collecte active sur ces titulaires
					p_retour:=  5: Le login n'a pas d'accès à la racine.
					p_retour:= -1; Erreur inconnue
					p_retour:= -10; on trouve pas le login , le mail n'est pas bon
					p_retour:= -11; Le login est bon mais pas le pwd
				--->
				
				<cfset userInfos = structNew()>		
				<cfset userInfos.OFFREDSN = SESSION.OffreDSN>
				
				<cfswitch expression="#p_retour#">
					<cfcase value="0">
						<cfset userInfos.AUTH_RESULT = 0>
						<cfreturn userInfos>
					</cfcase>
					<cfcase value="1"> 
						<cfset userInfos.AUTH_RESULT = 1>
					</cfcase>
					<cfcase value="2"> 
						<cfset userInfos.AUTH_RESULT = 2>
						<cfreturn userInfos>
					</cfcase>
					<cfcase value="3"> 
						<cfset userInfos.AUTH_RESULT = 3>
						<cfreturn userInfos>
					</cfcase>
					<cfcase value="4"> 
						<cfset userInfos.AUTH_RESULT = 4>
						<cfreturn userInfos>
					</cfcase>
					<cfcase value="5"> 
						<cfset userInfos.AUTH_RESULT = 5>
						<cfreturn userInfos>
					</cfcase>
					<cfcase value="-1"> 
						<cfset userInfos.AUTH_RESULT = -1>
						<cfreturn userInfos>
					</cfcase>
					<cfcase value="-10"> 
						<cfset userInfos.AUTH_RESULT = -10>
						<cfreturn userInfos>
					</cfcase>
					<cfcase value="-11"> 
						<cfset userInfos.AUTH_RESULT = -11>
						<cfreturn userInfos>
					</cfcase>
					<cfdefaultcase>
						<cfset userInfos.AUTH_RESULT = -1>
						<cfreturn userInfos>
					</cfdefaultcase>
				</cfswitch>
		
				<cfif qValidateLogin.recordcount EQ 1>
					<cfset boolProcessLogin=0>
					<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
					<cfif boolCheckIP EQ 1>
						<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
						<cfif checkIpResult GT 0>
							<cfset boolProcessLogin=1>
						<cfelse>
							<cfset boolProcessLogin=0>
							<cfset userInfos.AUTH_RESULT = 2>
							<cflog type="Information" text="AUTH REQUEST WITH INVALID IP ADDR">
						</cfif>
					<cfelse>
						<cfset boolProcessLogin=2>
					</cfif>
					<cfif boolProcessLogin GT 0>		
						
						<cfset SESSION.AUTH_STATE = 1>
						<cfset SESSION.CDRDSN = "ROCCDR">
						<cfset SESSION.PARCVIEWDSN = "CLPARCVIEW"><cfset SESSION.MAILSERVER = "mail.consotel.fr:26">
						<cfset SESSION.USER = structNew()>
						<cfset SESSION.USER.NOM = qValidateLogin['NOM'][1]>
						<cfset SESSION.USER.PRENOM = qValidateLogin['PRENOM'][1]>
						<cfset SESSION.USER.EMAIL = qValidateLogin['EMAIL'][1]>
						<cfset SESSION.USER.CLIENTACCESSID = qValidateLogin['CLIENTACCESSID'][1]>
						<cfset SESSION.USER.CLIENTID =  qValidateLogin['ID'][1]>
						<cfset SESSION.USER.IDTYPEPROFIL =  qValidateLogin['TYPE_PROFIL'][1]>
						<cfset SESSION.USER.LIBELLETYPEPROFIL =  qValidateLogin['LIBELLE_TYPE_PROFIL'][1]>								
						<cfset SESSION.USER.IDREVENDEUR =  qValidateLogin['IDREVENDEUR'][1]>
						<cfset SESSION.USER.NOM_REVENDEUR =  qValidateLogin['NOM_REVENDEUR'][1]>
						<cfset SESSION.USER.LIBELLE =  qValidateLogin['LIBELLE'][1]>
						<cfset userInfos.AUTH_RESULT = 1>
						<cfset userInfos.NOM = SESSION.USER.NOM>
						<cfset userInfos.PRENOM = SESSION.USER.PRENOM>
						<cfset userInfos.EMAIL = SESSION.USER.EMAIL>
						
						<cfset setupGlobalization(qValidateLogin)>				
						
						<cfset userInfos.CLIENTID = SESSION.USER.CLIENTID>
						<cfset userInfos.CODEAPPLICATION = SESSION.CODEAPPLICATION>
						<cfset userInfos.CLIENTACCESSID = SESSION.USER.CLIENTACCESSID>
						<cfset userInfos.GLOBALIZATION = SESSION.USER.GLOBALIZATION>
						
						<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
						
						<cfset initSessionXmlAccess()> 
					<cfelse>
						<cfset SESSION.FAILED_AUTH = 1>
						<cfset SESSION.AUTH_STATE = 4>
					</cfif>
				<cfelse>
					<cfset SESSION.FAILED_AUTH = 1>
					<cfset SESSION.AUTH_STATE = 3>
				</cfif>	
			<!--- 2)b) LES DONNEES SONT INCORRECTES --->
			<cfelse>
				<cfset userInfos = structNew()>
				<cfset userInfos.AUTH_RESULT = -999>
			</cfif>
			<cfreturn userInfos>
		<cfcatch type="any" >
			<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-010] CFCATCH validateLoginByListeTituAndMail" to="monitoring@saaswedo.com"  type="text/html" >
				<cfoutput >
					Une erreur est survenu pendant validateLoginByListeTituAndMail.<br><br>
					<cfdump var="#cfcatch#" label="cfcatch">
					<br>
				</cfoutput>
			</cfmail>
			<cfreturn -3>
		</cfcatch>	
		</cftry>
	</cffunction>
	<cffunction name="validateLoginByCleSSOForSFR" 
				access="private" 
				output="false" 
				description="retourne l'objet USER à partir d'une Clé SSO fourni par SFR"
				returntype="Any">
		<cfargument name="cleSSO" required="true" type="string">
		
		<!--- cfcase sur le code_appli pour redefinir le AUTH_DSN, et le package global --->
		<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.connectClient_v3">
			<cfprocparam type="in" value="#cleSSO#" cfsqltype="CF_SQL_VARCHAR" >
			<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qValidateLogin">
		</cfstoredproc>
		
		<!--- 
			-- p_retour:=0; le login/pwd est bon mais il est expiré
			-- p_retour:=1; le login/pwd est bon
			-- p_retour:=2; le login/pwd est bon mais il est bloqué ou le login/pwd n'est pas bon et on l'a bloqué
			-- p_retour:=-1; Erreur inconnue
			-- p_retour:=-10; on trouve pas le login , le mail n'est pas bon
			-- p_retour:=-11; Le login est bon mais pas le pwd 
		--->
		
		<cfset userInfos = structNew()>		
		<cfset userInfos.OFFREDSN = SESSION.OffreDSN>
		
		<cfswitch expression="#p_retour#">
			<cfcase value="0">
				<cfset userInfos.AUTH_RESULT = 0>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="1"> 
				<cfset userInfos.AUTH_RESULT = 1>
			</cfcase>
			<cfcase value="2"> 
				<cfset userInfos.AUTH_RESULT = 2>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-1"> 
				<cfset userInfos.AUTH_RESULT = -1>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-10"> 
				<cfset userInfos.AUTH_RESULT = -10>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-11"> 
				<cfset userInfos.AUTH_RESULT = -11>
				<cfreturn userInfos>
			</cfcase>
			<cfdefaultcase>
				<cfset userInfos.AUTH_RESULT = -100>
				<cfreturn userInfos>
			</cfdefaultcase>
		</cfswitch>

		<cfif qValidateLogin.recordcount EQ 1>
			<cfset boolProcessLogin=0>
			<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
			<cfif boolCheckIP EQ 1>
				<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
				<cfif checkIpResult GT 0>
					<cfset boolProcessLogin=1>
				<cfelse>
					<cfset boolProcessLogin=0>
					<cfset userInfos.AUTH_RESULT = 2>
					<cflog type="Information" text="AUTH REQUEST WITH INVALID IP ADDR">
				</cfif>
			<cfelse>
				<cfset boolProcessLogin=2>
			</cfif>
			<cfif boolProcessLogin GT 0>		
				
				<cfset SESSION.AUTH_STATE = 1>
				<cfset SESSION.CDRDSN = "ROCCDR">
				<cfset SESSION.PARCVIEWDSN = "CLPARCVIEW"><cfset SESSION.MAILSERVER = "mail.consotel.fr:26">
				<cfset SESSION.USER = structNew()>
				<cfset SESSION.USER.NOM = qValidateLogin['NOM'][1]>
				<cfset SESSION.USER.PRENOM = qValidateLogin['PRENOM'][1]>
				<cfset SESSION.USER.EMAIL = qValidateLogin['EMAIL'][1]>
				<cfset SESSION.USER.CLIENTACCESSID = qValidateLogin['CLIENTACCESSID'][1]>
				<cfset SESSION.USER.CLIENTID =  qValidateLogin['ID'][1]>
				<cfset SESSION.USER.IDTYPEPROFIL =  qValidateLogin['TYPE_PROFIL'][1]>
				<cfset SESSION.USER.LIBELLETYPEPROFIL =  qValidateLogin['LIBELLE_TYPE_PROFIL'][1]>								
				<cfset SESSION.USER.IDREVENDEUR =  qValidateLogin['IDREVENDEUR'][1]>
				<cfset SESSION.USER.NOM_REVENDEUR =  qValidateLogin['NOM_REVENDEUR'][1]>
				<cfset SESSION.USER.LIBELLE =  qValidateLogin['LIBELLE'][1]>
				<cfset userInfos.AUTH_RESULT = 1>
				<cfset userInfos.NOM = SESSION.USER.NOM>
				<cfset userInfos.PRENOM = SESSION.USER.PRENOM>
				<cfset userInfos.EMAIL = SESSION.USER.EMAIL>
				
				<cfset setupGlobalization(qValidateLogin)>				
				
				<cfset userInfos.CLIENTID = SESSION.USER.CLIENTID>
				<cfset userInfos.CODEAPPLICATION = SESSION.CODEAPPLICATION>
				<cfset userInfos.CLIENTACCESSID = SESSION.USER.CLIENTACCESSID>
				<cfset userInfos.GLOBALIZATION = SESSION.USER.GLOBALIZATION>
				
				<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
				
				<cfset initSessionXmlAccess()> 
			<cfelse>
				<cfset SESSION.FAILED_AUTH = 1>
				<cfset SESSION.AUTH_STATE = 4>
			</cfif>
		<cfelse>
			<cfset SESSION.FAILED_AUTH = 1>
			<cfset SESSION.AUTH_STATE = 3>
		</cfif>	
	
		<cfreturn userInfos>
	</cffunction>
	<cffunction name="validateLogin" 
				access="remote" 
				output="false" 
				returntype="struct">
		<cfargument name="loginValue" required="true" type="string">
		<cfargument name="pwdValue" required="true" type="string">
		<cfargument name="codeApp" required="false" type="numeric" default="1">
		
		<cfcookie name="LAST_LOGIN" value="#loginValue#" expires="never">
		 
		 
		<cfif structKeyExists(SESSION,"AUTH_STATE")>
			<cfif SESSION.AUTH_STATE EQ 1>
			   	<cflog type="Information" text="AUTH REQUEST WITH CONNECTED SESSION">
				<cfset clearSession()>
			</cfif>
		</cfif>
		
		<cfswitch expression="#codeApp#">
			 
			<cfcase value="2">
				<cfset THIS.AUTH_DSN = "MBP_MERCURE">
				<cfset SESSION.OffreDSN = "MBP_MERCURE">
				<cfset SESSION.CODEAPPLICATION = 2 >
				<cfset COOKIE.CODEAPPLICATION = 2 >
				<cfset THIS.CONNECT_CLIENT = "offre.pkg_connection_appli.connectClient">
				
			</cfcase>
			
			<cfcase value="3"> 
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 3>
				<cfset COOKIE.CODEAPPLICATION = 3>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfcase>
			
			<cfcase value="51"> 
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 51>
				<cfset COOKIE.CODEAPPLICATION = 51>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfcase>

			<cfcase value="251"> 
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 251>
				<cfset COOKIE.CODEAPPLICATION = 251>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfcase>
			
			<cfdefaultcase>
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 1>
				<cfset COOKIE.CODEAPPLICATION = 1>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfdefaultcase>
			
		</cfswitch>
		
		<!--- cfcase sur le code_appli pour redefinir le AUTH_DSN, et le package global --->
		<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.CONNECT_CLIENT#">
			<cfprocparam type="in" value="#loginValue#" cfsqltype="CF_SQL_VARCHAR" >
			<cfprocparam type="in" value="#pwdValue#" 	cfsqltype="CF_SQL_VARCHAR" >
			<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qValidateLogin">
		</cfstoredproc>
		
		<!--- 
			-- p_retour:=0; le login/pwd est bon mais il est expiré
			-- p_retour:=1; le login/pwd est bon
			-- p_retour:=2; le login/pwd est bon mais il est bloqué ou le login/pwd n'est pas bon et on l'a bloqué
			-- p_retour:=-1; Erreur inconnue
			-- p_retour:=-10; on trouve pas le login , le mail n'est pas bon
			-- p_retour:=-11; Le login est bon mais pas le pwd 
		--->
		
		<cfset userInfos = structNew()>		
		<cfset userInfos.OFFREDSN = SESSION.OffreDSN>
		
		<cfswitch expression="#p_retour#">
			<cfcase value="0">
				<cfset userInfos.AUTH_RESULT = 0>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="1"> 
				<cfset userInfos.AUTH_RESULT = 1>
			</cfcase>
			<cfcase value="2"> 
				<cfset userInfos.AUTH_RESULT = 2>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-1"> 
				<cfset userInfos.AUTH_RESULT = -1>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-10"> 
				<cfset userInfos.AUTH_RESULT = -10>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-11"> 
				<cfset userInfos.AUTH_RESULT = -11>
				<cfreturn userInfos>
			</cfcase>
			<cfdefaultcase>
				<cfset userInfos.AUTH_RESULT = -100>
				<cfreturn userInfos>
			</cfdefaultcase>
		</cfswitch>

		<cfif qValidateLogin.recordcount EQ 1>
			<cfset boolProcessLogin=0>
			<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
			<cfif boolCheckIP EQ 1>
				<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
				<cfif checkIpResult GT 0>
					<cfset boolProcessLogin=1>
				<cfelse>
					<cfset boolProcessLogin=0>
					<cfset userInfos.AUTH_RESULT = 2>
					<cflog type="Information" text="AUTH REQUEST WITH INVALID IP ADDR">
				</cfif>
			<cfelse>
				<cfset boolProcessLogin=2>
			</cfif>
			<cfif boolProcessLogin GT 0>		
				
				<cfset SESSION.AUTH_STATE = 1>
				<cfset SESSION.CDRDSN = "ROCCDR">
				<cfset SESSION.PARCVIEWDSN = "CLPARCVIEW"><cfset SESSION.MAILSERVER = "mail.consotel.fr:26">
				<cfset SESSION.USER = structNew()>
				<cfset SESSION.USER.NOM = qValidateLogin['NOM'][1]>
				<cfset SESSION.USER.PRENOM = qValidateLogin['PRENOM'][1]>
				<cfset SESSION.USER.EMAIL = qValidateLogin['EMAIL'][1]>
				<cfset SESSION.USER.CLIENTACCESSID = qValidateLogin['CLIENTACCESSID'][1]>
				<cfset SESSION.USER.CLIENTID =  qValidateLogin['ID'][1]>
				<cfset SESSION.USER.IDTYPEPROFIL =  qValidateLogin['TYPE_PROFIL'][1]>
				<cfset SESSION.USER.LIBELLETYPEPROFIL =  qValidateLogin['LIBELLE_TYPE_PROFIL'][1]>								
				<cfset SESSION.USER.IDREVENDEUR =  qValidateLogin['IDREVENDEUR'][1]>
				<cfset SESSION.USER.NOM_REVENDEUR =  qValidateLogin['NOM_REVENDEUR'][1]>
				<cfset SESSION.USER.LIBELLE =  qValidateLogin['LIBELLE'][1]>
				<cfset userInfos.AUTH_RESULT = 1>
				<cfset userInfos.NOM = SESSION.USER.NOM>
				<cfset userInfos.PRENOM = SESSION.USER.PRENOM>
				<cfset userInfos.EMAIL = SESSION.USER.EMAIL>
				
				<cfset setupGlobalization(qValidateLogin)>				
				
				<cfset userInfos.CLIENTID = SESSION.USER.CLIENTID>
				<cfset userInfos.CODEAPPLICATION = SESSION.CODEAPPLICATION>
				<cfset userInfos.CLIENTACCESSID = SESSION.USER.CLIENTACCESSID>
				<cfset userInfos.GLOBALIZATION = SESSION.USER.GLOBALIZATION>
				
				<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
				
				<cfset initSessionXmlAccess()> 
			<cfelse>
				<cfset SESSION.FAILED_AUTH = 1>
				<cfset SESSION.AUTH_STATE = 4>
			</cfif>
		<cfelse>
			<cfset SESSION.FAILED_AUTH = 1>
			<cfset SESSION.AUTH_STATE = 3>
		</cfif>	
	
		<cfreturn userInfos>
	</cffunction>
<!--- FONCTION PRIVEE --->
	<cffunction name="setupGlobalization" access="private" hint="Definit le globalization">
		<cfargument name="qValidateLogin" required="true" type="query" >
		<cfif SESSION.CODEAPPLICATION eq 51>
			<cfset SESSION.USER.GLOBALIZATION =  "fr_FR"/>
			<cfset SESSION.USER.IDGLOBALIZATION = "3" />
		<cfelse>
			<cfset SESSION.USER.GLOBALIZATION =  qValidateLogin['GLOBALIZATION'][1]>
			<cfset SESSION.USER.IDGLOBALIZATION =  qValidateLogin['IDCODE_LANGUE'][1]>
		</cfif>
		<cflog text="SESSION.USER.GLOBALIZATION = #SESSION.USER.GLOBALIZATION# - SESSION.USER.IDGLOBALIZATION = #SESSION.USER.IDGLOBALIZATION#">
	</cffunction>
	<cffunction name="checkIpAddress" access="private" returntype="numeric" hint="Return valid address count otherwise 0 (no access)">
		<cfargument name="accessId" required="true" type="numeric">
		<cfargument name="ADRESSE_IP" required="true" type="string">
		<cfset checkResult = 0>
		<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="PKG_INTRANET.GetdecoupageAdresseIP">
			<cfprocparam value="#accessId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetIpAddress">
		</cfstoredproc>
	   	<cfif qGetIpAddress.recordcount GT 0>
			<cfset adresseIp=ADRESSE_IP>
			<cfset adresseIpArray=arrayNew(1)>
			<cfloop index="i" from="1" to="4">
				<cfset adresseIpArray[i]=getToken(adresseIp,i,".")>
			</cfloop>
			<cfoutput query="qGetIpAddress">
				<cfset authIp=#P1# & "." & #P2# & "." & #P3# & "." & #P4#>
				<cfset C1=bitAnd(VAL(adresseIpArray[1]),VAL(#M1#))>
				<cfset C2=bitAnd(VAL(adresseIpArray[2]),VAL(#M2#))>
				<cfset C3=bitAnd(VAL(adresseIpArray[3]),VAL(#M3#))>
				<cfset C4=bitAnd(VAL(adresseIpArray[4]),VAL(#M4#))>
				<cfset adresseIpANDmasque = C1 & "." & C2 & "." & C3 & "." & C4>
				<cfif adresseIpANDmasque EQ authIp>
					<cfset checkResult = checkResult + 1>
				</cfif>
			</cfoutput>
			<cfreturn checkResult>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	<cffunction name="initSessionXmlAccess" access="private" output="false" returntype="void">
		<cflog text="initSessionXmlAccess">
		<cftry>
			<cfset currentPath = GetDirectoryFromPath(GetCurrentTemplatePath())/>
			<cfset fileName = currentPath & "/menus/Menu_" & SESSION.USER.GLOBALIZATION & ".xml">
			<cflog text="SESSION.USER.GLOBALIZATION = #SESSION.USER.GLOBALIZATION#">
			<cffile action="read" file="#fileName#" variable="XMLFileText">
			<cfset xmlAccess=XmlParse(XMLFileText)>
			<cfset SESSION.XML_ACCESS_STATUS = 1>
			<cfset SESSION.XML_ACCESS=xmlAccess>
		
		<cfcatch>
			<cfset currentPath = GetDirectoryFromPath(GetCurrentTemplatePath())/>
			<cfset fileName = currentPath & "/menus/Menu_fr_FR.xml">
			<cflog text="SESSION.USER.GLOBALIZATION = #SESSION.USER.GLOBALIZATION# - catch = #cfcatch#">
			<cffile action="read" file="#fileName#" variable="XMLFileText">
			<cfset xmlAccess=XmlParse(XMLFileText)>
			<cfset SESSION.XML_ACCESS_STATUS = 1>
			<cfset SESSION.XML_ACCESS=xmlAccess>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="initSessionXmlAccessPFGP" access="private" output="false" returntype="void">
		<cflog text="initSessionXmlAccessPFGP">
		<cfset createSessionXmlPFGP()>
		<cfset createSessionXmlPFGPAccueil()>
		<cfset getPFGPDateLoad()>
	</cffunction>
	<cffunction name="createSessionXmlPFGP" access="private" output="false" returntype="void">
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getUniversApplication">
			<cfprocparam value="#SESSION.USER.CLIENTACCESSID#"	type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#"	type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="51"			type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getAccesModApplication_v2">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_PERIMETRE#">
			<cfprocparam value="51"			type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="2"			type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qResult2">
		</cfstoredproc>	
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getDefaultModule">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_PERIMETRE#">
			<cfprocresult name="qResult3">
		</cfstoredproc>
		<cflog text="createSessionXmlPFGP">
		<!---PHASE 0 : on créé le xml racine--->
		<cfset xmlAcces = xmlNew(true)/>
		<cfset xmlAcces.xmlRoot = xmlElemNew(xmlAcces,"ROOT")/>
		<!--- PHASE 1 : on créé le xml des univers--->
		<cfset idx=1/>
		<cfloop query="qResult">
		<!---Si le type est celui désiré. 
			 Type = 1 -> Menu M00. Type = 2 -> Accueil M521--->
			<cfif qResult.TYPE eq 1>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx] = XmlElemNew(xmlAcces,"UNIVERS")>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.LBL = #qResult.NOM#>
			<!---Si l'univers est SYS = 1, cad qu'un module charge quand on clique dessus--->
				<cfif qResult.SYS eq 1>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.MODULE_KEY#>
				<cfelse>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.KEY#>
				</cfif>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.SYS = #qResult.SYS#>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.USR = "2">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.type = "radio">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.toggled = "false">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.ENABLED = "true">
				<cfset idx = idx +1>
			</cfif>
		</cfloop>
		<cflog text="xmlAcces = #xmlAcces#">
		<!--- PHASE 2: on ajoute les fonctions--->
		<!---1) on boucle sur chaque univers--->
		<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren)#" index="i">
			<cfset keyUniv = xmlAcces.xmlRoot.XmlChildren[i].xmlAttributes.KEY>
		<!---2) on boucle sur les fonctions pour trouver les fonctions enfants de l'univers parent--->	
			<cfloop query="qResult2">
				<!---Si l'univers est différent, on passe à la fonction suivante--->
				<cfif qResult2.UNIVERS_MENU eq keyUniv>
					<!---Si la clé de la fonction est égale à la clé de l'univers parent, on ignore. Sinon on continue le traitement--->
					<cfif qResult2.KEY neq qResult2.UNIVERS_MENU>
						<cfset idx = arraylen(xmlAcces.xmlRoot.xmlChildren[i].xmlChildren)+1>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx] = XmlElemNew(xmlAcces,"FONCTION")>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.LBL = #qResult2.NOM_MENU#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.KEY = #qResult2.KEY#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.SYS = "1">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.USR = "2">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.type = "radio">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.toggled = "false">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.ENABLED = "true">
					</cfif>
				</cfif>
			</cfloop>
		</cfloop>
		<cflog text="xmlAcces = #xmlAcces#">
		<!--- PHASE 3: on definit la fonction affichée par défaut--->
		<cfset boolDefault = 0>
		<cfloop query="qResult3">
			<cfset keyDefault = qResult3.KEY>
			<cflog text="keyDefault = #keyDefault# -- ArrayLen(xmlAcces.xmlRoot.XmlChildren) + #ArrayLen(xmlAcces.xmlRoot.XmlChildren)#">
			<!--- On boucle sur les univers --->
			<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren)#" index="i">
				<cfset keyUniv= xmlAcces.xmlRoot.XmlChildren[i].XmlAttributes.KEY/>
				<cflog text="keyUniv = #keyUniv#">
				<!--- Si l'univers correspond à l'univers par défaut, on modifie la propriété toggled à true et on sort de la boucle --->
				<cfif keyDefault eq keyUniv>
					<cfset xmlAcces.xmlRoot.XmlChildren[i].XmlAttributes.toggled = 'true'>
					<cfset boolDefault = 1>
					<cflog text="Univers sélectionné : #keyUniv#">
					<cfbreak>
				<cfelse>
					<!--- Sinon on vérifie que l'univers a des enfants --->
					<cflog text="arraylen(xmlAcces.xmlRoot.XmlChildren[i].XmlChildren = #arraylen(xmlAcces.xmlRoot.XmlChildren[i].XmlChildren)#">
					<cfif arraylen(xmlAcces.xmlRoot.XmlChildren[i].XmlChildren) gt 0>
						<!--- On boucle sur les fonctions de l'univers --->	
						<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren[i].XmlChildren)#" index="j">
							<cfset keyFonction= xmlAcces.xmlRoot.XmlChildren[i].XmlChildren[j].XmlAttributes.KEY>
							<cflog text="keyFonction = #keyFonction#">
							<!--- Si la fonction correspond à la fonction par défaut, on modifie la propriété toggled à true et on sort de la boucle --->
							<cfif keyFonction eq keyDefault>
								<cfset xmlAcces.xmlRoot.XmlChildren[i].XmlChildren[j].XmlAttributes.toggled = 'true'>
								<cfset xmlAcces.xmlRoot.XmlChildren[i].XmlAttributes.toggled = 'true'>
								<cflog text="Fonction sélectionné : #keyFonction#">
								<cfset boolDefault = 1>	
								<cfbreak>
							</cfif> 
						</cfloop>
					</cfif>
				</cfif>
			</cfloop>
			<cfif boolDefault eq 0>
				<cfif arraylen(xmlAcces.xmlRoot.XmlChildren) gt 0>
					<cfset xmlAcces.xmlRoot.XmlChildren[1].XmlAttributes.toggled = 'true'>
					<cfif arraylen(xmlAcces.xmlRoot.XmlChildren[1].XmlChildren) gt 0>
						<cfset xmlAcces.xmlRoot.XmlChildren[1].XmlChildren[1].XmlAttributes.toggled = 'true'>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		<!---<cfmail server="mail.consotel.fr" from="accessManager@consotel.fr" to="olivier.boulard@consotel.fr" port="25" subject="dump xml access pfgp" type="text/html">
			<cfdump var="#qResult#"><br><br>
			<cfdump var="#qResult2#"><br><br>
			<cfdump var="#qResult3#">
		</cfmail>--->
		<cfset SESSION.XML_ACCESS_PFGP = xmlAcces>
		<cfset SESSION.XML_ACCESS_STATUS=1>	
		
	</cffunction>
	<cffunction name="createSessionXmlPFGPAccueil" access="private" output="false" returntype="void" hint="Créer le xml qui sert au menu de l'accueil">
		<cflog text="createSessionXmlPFGPAccueil">
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getUniversApplication">
			<cfprocparam value="#SESSION.USER.CLIENTACCESSID#"	type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#"	type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="51"			type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getAccesModApplication_v2">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_PERIMETRE#">
			<cfprocparam value="51"			type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="1"			type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qResult2">
		</cfstoredproc>	
		
		<!---PHASE 0 : on créé le xml racine--->
		<cfset xmlAcces = xmlNew(true)/>
		<cfset xmlAcces.xmlRoot = xmlElemNew(xmlAcces,"ROOT")/>
		<!--- PHASE 1 : on créé le xml des univers--->
		<cfset idx=1/>
		<cfloop query="qResult">
		<!---Si le type est celui désiré. 
			 Type = 1 -> Menu M00. Type = 2 -> Accueil M521--->
			<cfif qResult.TYPE eq 2>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx] = XmlElemNew(xmlAcces,"UNIVERS")>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.LBL = #qResult.NOM#>
			<!---Si l'univers est SYS = 1, cad qu'un module charge quand on clique dessus--->
				<cfif qResult.SYS eq 1>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.MODULE_KEY#>
				<cfelse>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.KEY#>
				</cfif>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.SYS = #qResult.SYS#>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.USR = "2">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.type = "radio">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.toggled = "false">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.ENABLED = "true">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.ICON = #qResult.ICONE#>
				<cfset idx = idx +1>
			</cfif>
		</cfloop>
		<!--- PHASE 2: on ajoute les fonctions--->
		<!---1) on boucle sur chaque univers--->
		<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren)#" index="i">
			<cfset keyUniv = xmlAcces.xmlRoot.XmlChildren[i].xmlAttributes.KEY>
		<!---2) on boucle sur les fonctions pour trouver les fonctions enfants de l'univers parent--->	
			<cfloop query="qResult2">
				<!---Si l'univers est différent, on passe à la fonction suivante--->
				<cfif qResult2.UNIVERS_BLOC eq keyUniv>
					<!---Si la clé de la fonction est égale à la clé de l'univers parent, on ignore. Sinon on continue le traitement--->
					<cfif qResult2.KEY neq qResult2.UNIVERS_BLOC>
						<cfset idx = arraylen(xmlAcces.xmlRoot.xmlChildren[i].xmlChildren)+1>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx] = XmlElemNew(xmlAcces,"FONCTION")>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.LBL = #qResult2.NOM_BLOC#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.DSC = #qResult2.DESCRIPTION_BLOC#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.KEY = #qResult2.KEY#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.SYS = "1">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.USR = "2">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.type = "radio">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.toggled = "false">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.ENABLED = "true">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.ICON = #qResult2.ICONE_BLOC#>
					</cfif>
				</cfif>
			</cfloop>
		</cfloop>
		<!---<cfmail server="mail.consotel.fr" from="accessManager@consotel.fr" to="olivier.boulard@consotel.fr" port="25" subject="dump xml access pfgp" type="text/html">
			<cfdump var="#qResult#"><br><br>
			<cfdump var="#qResult2#">
		</cfmail>--->
		<cfset SESSION.XML_ACCESS_PFGP_ACCUEIL = xmlAcces>
	</cffunction>
	<cffunction name="initSessionXmlAccessV4" access="private" output="false" returntype="void" hint="créé le xml utilisé par cv pour créer le menu CV4">
		<cflog text="initSessionXmlAccessV4">
		<cflog text="app_loginid = #SESSION.USER.CLIENTACCESSID# - idracine = #SESSION.PERIMETRE.ID_GROUPE# - langid = #SESSION.USER.IDGLOBALIZATION#">
		
		<cfset _IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_GROUPE>
		
		<cfif structkeyExists(SESSION.PERIMETRE,"ID_PERIMETRE")>
			<cfset _IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		</cfif>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M00.getListeModuleClien_V3">
			<cfprocparam value="#SESSION.USER.CLIENTACCESSID#"	type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#"	type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#SESSION.USER.IDGLOBALIZATION#"	type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#_IDGROUPE_CLIENT#"	type="in"	cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
		<!---<cfmail server="mail.consotel.fr" from="accessManager@consotel.fr" to="olivier.boulard@consotel.fr" port="25" subject="dump xml access pfgp" type="text/html">
			<cfdump var="#qResult#"><br><br>
			parametres : <br>
			SESSION.USER.CLIENTACCESSID = #SESSION.USER.CLIENTACCESSID#<br>
			SESSION.PERIMETRE.ID_GROUPE= #SESSION.PERIMETRE.ID_GROUPE#<br>
			SESSION.USER.IDGLOBALIZATION = #SESSION.USER.IDGLOBALIZATION#<br>
			
		</cfmail>--->
		
		<cfset currentPath = GetDirectoryFromPath(GetCurrentTemplatePath())/>
		<cfset fileName = currentPath & "/menus/Menu_Univers_" & SESSION.USER.GLOBALIZATION & ".xml">
		<cffile action="read" file="#fileName#" variable="XMLFileText">
		<cfset xmlAccess=XmlParse(XMLFileText)>
		
		<cfset idxHOME = 1>
		<cfset idxGEST_CONT = 1>
		<cfset idxFACT_REPORT_E0 = 1>
		<cfset idxFACT = 1>
		<cfset idxGEST = 1>
		<cfset idxUSG = 1>
		<cfset idxSTRUCT = 1>
		<cfset loopindex= 1>
		<cfset idxUnivers = 1>
		<cfset idx = 0>

		<cfloop query="qResult">
			<cfset idx = Evaluate("idx#qResult.UNIVERS#")>			
		
		<!--- On trouve l'index de l'univers  --->	
			<cfloop from="1" to="#ArrayLen(xmlAccess.MENU.XmlChildren)#" index="i">
				<cfif xmlAccess.MENU.xmlChildren[i].xmlAttributes.KEY eq qResult.UNIVERS>
					<cfset idxUnivers = i>
					<cfbreak>
				</cfif>
			</cfloop> 
		<!--- On précise que cet univers est visible par l'utilisateur --->	
			<cfif qResult.ACCES_USER gt 0>
				<cfset xmlAccess.MENU.xmlChildren[idxUnivers].XmlAttributes.USR= 1>
			</cfif>
			<cfif qResult.KEY neq qResult.UNIVERS>
				<cfif qResult.ACCES_USER gt 0>
					<cfset tmpXml = xmlAccess.MENU.xmlChildren[idxUnivers]>
					<cfset tmpXml.xmlChildren[idx] = XmlElemNew(xmlAccess,"FONCTION")>
					<cfset tmpXml.xmlChildren[idx].XmlAttributes.LBL = #qResult.NOM_MODULE#>
					<cfset tmpXml.xmlChildren[idx].XmlAttributes.KEY = #qResult.KEY#>
					<cfset tmpXml.xmlChildren[idx].XmlAttributes.SYS = "1">
					<cfset tmpXml.xmlChildren[idx].XmlAttributes.USR = "2">
					<cfset tmpXml.xmlChildren[idx].XmlAttributes.type = "radio">
					<cfset tmpXml.xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
					<cfset tmpXml.xmlChildren[idx].XmlAttributes.toggled = "false">
					<cfset tmpXml.xmlChildren[idx].XmlAttributes.default = "false">
					<cfset tmpXml.xmlChildren[idx].XmlAttributes.ENABLED = "true">
		 
		 			<cfset idx = idx +1>
					<cfset "idx#qResult.UNIVERS#" = idx>
					<cfset loopindex = loopindex +1>
				</cfif>
			</cfif>
		</cfloop>
		<cflog text="xmlAccess = #xmlAccess#">
		<cfset xmlArr = xmlSearch(xmlAccess,"/MENU/UNIVERS/FONCTION")>
		<cfset xmlArr2 = xmlSearch(xmlAccess,"/MENU/UNIVERS[@USR > 0]")>
		<cfset plusVar = arraylen(xmlArr) + arraylen(xmlArr2)>
		<cfif plusVar eq 0> 
			<cflog text="xmlArr.taille = 0">
			<cfset currentPath = GetDirectoryFromPath(GetCurrentTemplatePath())/>
			<cfset fileName = currentPath & "menus/Default_Menu_" & SESSION.USER.GLOBALIZATION & ".xml">
			<cffile action="read" file="#fileName#" variable="XMLFileText">
			<cfset xmlAccess=XmlParse(XMLFileText)>
		</cfif>
		<!---<cfmail server="mail.consotel.fr" from="accessManager@consotel.fr" to="olivier.boulard@consotel.fr" port="25" subject="dump xml access pfgp" type="text/html">
			<cfdump var="#qResult#"><br><br>
			<cfdump var="#xmlArr#"><br><br>
			<cfdump var="#xmlArr2#"><br><br>
			<cfdump var="#xmlAccess#"><br><br>
		</cfmail>--->
		<cfset SESSION.XML_ACCESSV4 = xmlAccess>
		<cfset SESSION.XML_ACCESS_STATUS=1>
		
	</cffunction>
	<cffunction name="getGroupList" access="remote" returntype="query">
			<cfargument name="accessId" required="true" type="numeric">
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="#THIS.PKG_GLOBAL#.Login_Listeroot_v2">
				<cfprocparam  value="#accessId#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#SESSION.CODEAPPLICATION#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qGetGroupList">
			</cfstoredproc>
			<cfreturn qGetGroupList>
		</cffunction>
	<cffunction name="connectOnGroup" output="false" access="remote" returntype="struct">
			<cfargument name="accessId" required="true" type="numeric">
			<cfargument name="groupId" required="true" type="numeric">
			<cfargument name="typelogin" required="false" type="numeric" default="-1">
			<cflog text="connectOnGroup : typelogin = #typelogin#">
			
			<cfset groupInfos=getGroupInfos(groupId)>
			<cfset structDelete(SESSION,"PERIMETRE")>
			<cfset SESSION.PERIMETRE = structNew()>
			<cfset SESSION.PERIMETRE.ID_GROUPE=groupInfos['IDGROUPE_CLIENT'][1]>
			<cfset SESSION.PERIMETRE.GROUPE=groupInfos['LIBELLE_GROUPE_CLIENT'][1]>
			<cfset SESSION.PERIMETRE.IDCLIENT_PV=groupInfos['IDCLIENTS_PV'][1]>
			<cfset SESSION.PERIMETRE.IDRACINE_MASTER=groupInfos['IDRACINE_MASTER'][1]>
			<cfif typelogin gt -1>
				<cfset SESSION.PERIMETRE.TYPE_LOGIN = typelogin>
			<cfelse>
				<cfset SESSION.PERIMETRE.TYPE_LOGIN = 1>	
			</cfif>
			
			<cfquery name="qGetBoolInventaire" datasource="#SESSION.OFFREDSN#">
			SELECT 0 AS isCycleVieInitialized
			FROM DUAL
			</cfquery>
			<cfset SESSION.PERIMETRE.BOOLINVENTAIRE = qGetBoolInventaire['isCycleVieInitialized'][1]>
			<cfset groupStructData=structNew()>
			<cfset groupStructData.BOOLINVENTAIRE = SESSION.PERIMETRE.BOOLINVENTAIRE>
			<cfset groupStructData.PERIMETRE_DATA=buildGroupXmlTree(accessId,groupId)>
			
			<cfif structKeyExists(SESSION.PERIMETRE,"ID_GROUPE")>
				<cfif SESSION.CODEAPPLICATION eq 51>
					<cfset initSessionXmlAccessPFGP()>
				<cfelseif SESSION.CODEAPPLICATION eq 101>
					<cfset initSessionXmlAccessV4()>
				<cfelse>
					<cfif SESSION.PERIMETRE.TYPE_LOGIN eq 2>
						<cfset initSessionXmlAccessV4()>
					</cfif>
					<cfif SESSION.PERIMETRE.TYPE_LOGIN eq 1>
						<cfset initSessionXmlAccess()>
					</cfif>
				</cfif>
			<cfelse>
				<cfset initSessionXmlAccess()>
			</cfif>
		
			
			
			<cflog text="SESSION.PERIMETRE.ID_GROUPE = #SESSION.PERIMETRE.ID_GROUPE#">
			
					
			<cfreturn groupStructData>
	</cffunction>
	<cffunction name="getGroupInfos" access="private" returntype="query">
		<cfargument name="groupId" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="#THIS.PKG_GLOBAL#.GET_GROUPE_INFO">
			<cfprocparam  value="#groupId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetGroupInfos">
		</cfstoredproc>
		<cfreturn qGetGroupInfos>
	</cffunction>
	<cffunction name="buildGroupXmlTree" access="private" output="false" returntype="struct">
		<cfargument name="accessId" required="true" type="numeric">
		<cfargument name="groupId" required="true" type="numeric">
		<cfset dataStruct=structNew()>
		<cfset dataStruct.BUILD_STATUS = -1>
		<cftrace text="buildGroupXmlTree">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="#THIS.PKG_GLOBAL#.Login_nodes_acces_V3">
			<cfprocparam value="#groupId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#accessId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetPathToAccessNode">
		</cfstoredproc>
		<cfif qGetPathToAccessNode.recordcount GT 0>
			<cfset accessNodeId=qGetPathToAccessNode['IDGROUPE_CLIENT'][qGetPathToAccessNode.recordcount]>
			<cfset accessStatus=qGetPathToAccessNode['STC'][qGetPathToAccessNode.recordcount]>
			
			<cfswitch expression="#SESSION.USER.GLOBALIZATION#">
				<cfcase value="fr_FR">
					<cfset LEVELNo1 = "Liste des Organisations Opérateurs">
					<cfset LEVELNo2 = "Liste des Organisations Clientes">
					<cfset LEVELNo3 = "Liste des Recherches Sauvegardées">
				</cfcase>

				<cfcase value="en_US"> 
					<cfset LEVELNo1 = "List of operators accounts">
					<cfset LEVELNo2 = "List of customers accounts">
					<cfset LEVELNo3 = "List of saved researchs">
				</cfcase>
				
				<cfcase value="en_GB"> 
					<cfset LEVELNo1 = "List of operators accounts">
					<cfset LEVELNo2 = "List of customers accounts">
					<cfset LEVELNo3 = "List of saved researchs">
				</cfcase>
				
				<cfcase value="es_ES"> 
					<cfset LEVELNo1 = "Lista de las organizaciones de operadores">
					<cfset LEVELNo2 = "Lista de los Organismos de Clientes">
					<cfset LEVELNo3 = "Lista de las búsquedas guardadas">
				</cfcase>
				
				<cfdefaultcase>
					<cfset LEVELNo1 = "Liste des Organisations Opérateurs">
					<cfset LEVELNo2 = "Liste des Organisations Clientes">
					<cfset LEVELNo3 = "Liste des Recherches Sauvegardées">
				</cfdefaultcase>
			</cfswitch>
			
			
			
			<cfif accessStatus GT 0>
				<cfxml variable="perimetreXmlDoc">
					<cfoutput>
						<NODE LBL="#qGetPathToAccessNode['LIBELLE_GROUPE_CLIENT'][1]#" NID="#qGetPathToAccessNode['IDGROUPE_CLIENT'][1]#"
														NTY="#qGetPathToAccessNode['TYPE_NOEUD'][1]#" STC="#qGetPathToAccessNode['STC'][1]#">
														
							<NODE LBL="#LEVELNo1#" NID="-2" NTY="0" STC="0"/>
							<NODE LBL="#LEVELNo2#" NID="-3" NTY="0" STC="0"/>
							<NODE LBL="#LEVELNo3#" NID="-4" NTY="0" STC="0"/>
						</NODE>
					</cfoutput>
				</cfxml>
				<cfif groupId EQ accessNodeId>
					<cfset dataStruct.BUILD_STATUS = buildTreeFromGroup(groupId,perimetreXmlDoc)>
				<cfelse>
					<cfset dataStruct.BUILD_STATUS = buildTreeFromNode(groupId,perimetreXmlDoc,qGetPathToAccessNode)>
				</cfif>
				<cfset dataStruct.XML_PERIMETRE = perimetreXmlDoc>
				<cfset dataStruct.PERIMETRE_INFOS = setPerimetre(accessId,accessNodeId)>
			<cfelse>
				<cfset dataStruct.BUILD_STATUS = -2>
			</cfif>
		</cfif>
		<cfreturn dataStruct>
	</cffunction>
	<cffunction name="buildTreeFromGroup" access="private" returntype="numeric" output="false">
		<cfargument name="groupId" required="true" type="numeric">
		<cfargument name="xmlDocument" required="true" type="xml">
		<cftrace text="BUiLDTREEFROMGROUP">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="#THIS.PKG_GLOBAL#.enfant_noeud">
			<cfprocparam  value="#groupId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNodeChild">
		</cfstoredproc>
		<cfset NODE_CREATED_COUNT = 0>
		<cfoutput query="qGetNodeChild" group="TYPE_ORGA">
			<cfset parentNodeIndex = -1>
			<cfswitch expression="#TYPE_ORGA#">
				<cfcase value="OPE">
					<cfset parentNodeIndex = 1>
				</cfcase>
				<cfcase value="SAV">
					<cfset parentNodeIndex = 3> 
				</cfcase>
				<cfcase value="CUS">
					<cfset parentNodeIndex = 2> 
				</cfcase>
				<cfcase value="GEO">
					<cfset parentNodeIndex = 2> 
				</cfcase>
				<cfcase value="ANU">
					<cfset parentNodeIndex = 2> 
				</cfcase>
				<cfdefaultcase>
					<cfset parentNodeIndex = 0> 
				</cfdefaultcase>
			</cfswitch>
			<cfif parentNodeIndex GT 0>
				<cftrace text="NODE_CREATED_COUNT = #NODE_CREATED_COUNT#">
				<cfoutput>
					<cfset rootNode = xmlDocument.xmlRoot>
					<cfset parentNode=rootNode.XmlChildren[parentNodeIndex]> 
					<cfset parentChildCount = arrayLen(parentNode.XmlChildren)>
					<cfset parentNode.XmlAttributes.NTY = 1>
					<cfset parentNode.XmlChildren[parentChildCount + 1] = XmlElemNew(xmlDocument,"NODE")>
					<cfset tmpNode = parentNode.XmlChildren[parentChildCount + 1]>
					<cfset tmpNode.XmlAttributes.LBL = #LIBELLE_GROUPE_CLIENT#>
					<cfset tmpNode.XmlAttributes.NID = #IDGROUPE_CLIENT#>
					<cfset tmpNode.XmlAttributes.NTY = #TYPE_NOEUD#>
					<cfset tmpNode.XmlAttributes.STC = 1>
					<cfset NODE_CREATED_COUNT = NODE_CREATED_COUNT + 1>
				</cfoutput>
			</cfif>
		</cfoutput>
		<cfreturn NODE_CREATED_COUNT>
	</cffunction>
	<cffunction name="buildTreeFromNode" access="private" returntype="numeric" output="false">
		<cfargument name="groupId" required="true" type="numeric">
		<cfargument name="xmlDocument" required="true" type="xml">
		<cfargument name="pathQuery" required="true" type="query">
		<cfset NODE_CREATED_COUNT = 0>
		<cfset processStatus=-1>
		<cfset rootNode = xmlDocument.xmlRoot>
		<cfloop index="i" from="2" to="#pathQuery.recordcount#">
			<cfset currentNodeId = pathQuery['IDGROUPE_CLIENT'][i]>
			<cfset currentNodeType = pathQuery['TYPE_ORGA'][i]>
			<cfset currentParentId = pathQuery['ID_GROUPE_MAITRE'][i]>
			<cfset parentNodeArray = XMLSearch(xmlDocument,"//NODE[@NID=#currentParentId#]")>
			<cfif arrayLen(parentNodeArray) EQ 1>
				<cfset parentNode = parentNodeArray[1]>
				<cfif isXmlRoot(parentNode) EQ TRUE>
					<cfswitch expression="#currentNodeType#">
						<cfcase value="OPE">
							<cfset parentNodeIndex = 1>
						</cfcase>
						<cfcase value="SAV">
							<cfset parentNodeIndex = 3>
						</cfcase>
						<cfcase value="CUS">
							<cfset parentNodeIndex = 2>
						</cfcase>
						<cfcase value="GEO">
							<cfset parentNodeIndex = 2>
						</cfcase>
						<cfcase value="ANU">
							<cfset parentNodeIndex = 2>
						</cfcase>
						<cfdefaultcase>
							<cfset parentNodeIndex = 0> 
						</cfdefaultcase>
					</cfswitch>
					<cfif parentNodeIndex GT 0>
						<cfset parentNode = rootNode.XmlChildren[parentNodeIndex]>
						<cfset processStatus=1>
					<cfelse>
						<cfset processStatus=0>
					</cfif>
				<cfelse>
					<cfset processStatus=1>
				</cfif>
				<cfif processStatus EQ 1>
					<cfset parentNode.XmlAttributes.NTY = 1>
					<cfset parentChildCount = arrayLen(parentNode.XmlChildren)>
					<cfset parentNode.XmlChildren[parentChildCount + 1] = XmlElemNew(xmlDocument,"NODE")>
					<cfset tmpNode = parentNode.XmlChildren[parentChildCount + 1]>
					<cfset tmpNode.XmlAttributes.LBL = pathQuery['LIBELLE_GROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NID = pathQuery['IDGROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NTY = pathQuery['TYPE_NOEUD'][i]>
					<cfset tmpNode.XmlAttributes.STC = pathQuery['STC'][i]>
					<cfset NODE_CREATED_COUNT = NODE_CREATED_COUNT + 1>
				</cfif>
			<cfelse>
				<cfset NODE_CREATED_COUNT = -1>
				<cfreturn NODE_CREATED_COUNT>
			</cfif>
		</cfloop>
		<cfreturn NODE_CREATED_COUNT>
	</cffunction>
	<cffunction name="setPerimetre" access="private" returntype="struct">
		<cfargument name="accessId" required="true" type="numeric">
		<cfargument name="idGroupeClient" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="#THIS.PKG_GLOBAL#.detail_noeud_v2">
			<cfprocparam  value="#idGroupeClient#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#accessId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNodeInfos">
		</cfstoredproc>
		<cfset perimetreStruct=structNew()>
		<cfset perimetreStruct.PERIMETRE_INFOS_STATUS=qGetNodeInfos.recordcount>
		<cfif qGetNodeInfos.recordcount EQ 1>
			<cfset SESSION.PERIMETRE.ID_PERIMETRE=qGetNodeInfos['IDGROUPE_CLIENT'][1]>
			<cfset SESSION.PERIMETRE.RAISON_SOCIALE=qGetNodeInfos['LIBELLE_GROUPE_CLIENT'][1]>
			
			<cfif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPE">			
				<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="Groupe">
			<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPELIGNE">			
				<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="GroupeLigne">
			<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "SOUSTETE">			
				<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="SousTete">
			<cfelse>
				<cfset SESSION.PERIMETRE.TYPE_PERIMETRE=qGetNodeInfos['TYPE_PERIMETRE'][1]>
			</cfif>
			
			<cfset perimetreStruct.ID_PERIMETRE=qGetNodeInfos['IDGROUPE_CLIENT'][1]>
			<cfset perimetreStruct.RAISON_SOCIALE=qGetNodeInfos['LIBELLE_GROUPE_CLIENT'][1]>
			<cfset perimetreStruct.OPERATEURID=qGetNodeInfos['OPERATEURID'][1]>
			
			<cfif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPE">
				<cfset perimetreStruct.TYPE_PERIMETRE="Groupe">
			<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPELIGNE">
				<cfset perimetreStruct.TYPE_PERIMETRE="GroupeLigne">
			<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "SOUSTETE">			
				<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="SousTete">
			<cfelse>
				<cfset perimetreStruct.TYPE_PERIMETRE=qGetNodeInfos['TYPE_PERIMETRE'][1]>
			</cfif>
			
			
			<cfset perimetreStruct.GROUP_CODE_STYLE=qGetNodeInfos['CODE_STYLE'][1]>
			<cfcookie name="CODE_STYLE" value="#perimetreStruct.GROUP_CODE_STYLE#" domain="consotel.fr" expires="never">
			
			
			
			<cfif idGroupeClient EQ SESSION.PERIMETRE.ID_GROUPE>
				<cfset perimetreStruct.TYPE_LOGIQUE="ROOT">
			<cfelse>
				<cfset perimetreStruct.TYPE_LOGIQUE=qGetNodeInfos['TYPE_ORGA'][1]>
			</cfif>
			<cfset SESSION.PERIMETRE.GROUP_CODE_STYLE=perimetreStruct.GROUP_CODE_STYLE>
			<cfset perimetreStruct.PERIODS=setPeriod(idGroupeClient)>
			<cfset perimetreStruct.ACCESS_LIST=setAccess(qGetNodeInfos)>
			<cfset perimetreStruct.DROIT_GESTION_FOURNIS=SESSION.PERIMETRE.DROIT_GESTION_FOURNIS>
		</cfif>
		<cfreturn perimetreStruct>
	</cffunction>
	<cffunction name="setPeriod" access="private" returntype="struct">
		<cfargument name="idGroupeClient" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="#THIS.PKG_GLOBAL#.GET_DATEFACTURE_V2">
	        <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam  value="#idGroupeClient#" cfsqltype="CF_SQL_INTEGER">
	         <cfprocparam  value="2004/01/01" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetPeriod">
        </cfstoredproc>
		<cfset periodStruct=structNew()>
		
		<cfif LEN(qGetPeriod.datemin) EQ 0>
			<!--- <cfset periodStruct.DateFirstFacture = lsDateFormat(NOW(),"dd/mm/yyyy")> --->
			<cfset periodStruct.FACTURE_DEB = NOW()>
		<cfelse>
			<cfset periodStruct.FACTURE_DEB = lsParseDateTime(qGetPeriod.datemin)>
		</cfif>
		<cfif LEN(qGetPeriod.datemax) EQ 0>
			<!--- <cfset periodStruct.DateLastFacture = lsDateFormat(NOW(),"dd/mm/yyyy")> --->			
			<cfset periodStruct.FACTURE_FIN = NOW()>
		<cfelse>
			<cfset periodStruct.FACTURE_FIN = lsParseDateTime(qGetPeriod.datemax)>
		</cfif>
 		
		
		
		<cfset periodStruct.DISPLAY_DEB = dateadd("yyyy",-2,periodStruct.FACTURE_FIN)>
		<cfset periodStruct.DISPLAY_FIN = periodStruct.FACTURE_FIN>
		<cfif dateCompare(periodStruct.FACTURE_DEB,periodStruct.DISPLAY_DEB) GT 0>
			<cfset periodStruct.DISPLAY_DEB = periodStruct.FACTURE_DEB>
		</cfif>
		<cfif dateCompare(periodStruct.DISPLAY_FIN,periodStruct.FACTURE_FIN) GT 0>
			<cfset periodStruct.DISPLAY_FIN = periodStruct.FACTURE_FIN>
		</cfif>
		<cfset periodStruct.NB_MONTH_FACT=dateDiff("m",periodStruct.FACTURE_DEB,periodStruct.FACTURE_FIN)>
		<cfset periodStruct.NB_MONTH_DISPLAY=dateDiff("m",periodStruct.DISPLAY_DEB,periodStruct.DISPLAY_FIN)>
		
		<cfset SESSION.PERIMETRE.STRUCTDATE=structNew()>
		<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS=periodStruct>
		<cfset SESSION.PERIMETRE.STRUCTDATE.DATEFIRSTFACTURE=SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB>
		<cfset SESSION.PERIMETRE.STRUCTDATE.DATELASTFACTURE=SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN>
		<cfset SESSION.PERIMETRE.STRUCTDATE.DISPLAY_DATEDEB=SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_DEB>
		<cfset SESSION.PERIMETRE.STRUCTDATE.DISPLAY_DATEFIN=SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN>
		
		<cfif structkeyexists(SESSION,"DATES")>
			<cfif structkeyexists(SESSION.DATES,"LASTPFGPIDPERIODE")>
				<cfif SESSION.DATES.LASTPFGPIDPERIODE eq 0>
					<cfquery datasource="#SESSION.OFFREDSN#" name="qPeriode">
						 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS  
					 	 FROM	PERIODE
					 	 WHERE	PERIODE.SHORT_MOIS=to_Char(#SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN#,'mm/yyyy')
					</cfquery>
					<cfset SESSION.DATES.LASTPFGPLOAD = SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN>
					<cfset SESSION.DATES.LASTPFGPIDPERIODE = qPeriode.IDPERIODE_MOIS>
				</cfif>		
			</cfif>
		</cfif>
		<cfreturn periodStruct>		
	</cffunction>
	<cffunction name="setAccess" access="private" returntype="numeric" output="false">
		<cfargument name="accessData" required="true" type="query">
		<cflog text="setAccess"/>
		<cfset accessStruct=structNew()>
		<cfset accessStruct.SET_ACCESS_STATUS=0>
		<cfset accessStruct.MODULE_FACTURATION = accessData['MODULE_FACTURATION'][1]>
		<cfset accessStruct.MODULE_FIXE_DATA = accessData['MODULE_FIXE_DATA'][1]>
		<cfset accessStruct.MODULE_GESTION_LOGIN = accessData['MODULE_GESTION_LOGIN'][1]>
		<cfset accessStruct.MODULE_GESTION_ORG = accessData['MODULE_GESTION_ORG'][1]>
		<cfset accessStruct.MODULE_MOBILE = accessData['MODULE_MOBILE'][1]>
		<cfset accessStruct.MODULE_USAGE = accessData['MODULE_USAGE'][1]>
		<cfset accessStruct.MODULE_WORKFLOW = accessData['MODULE_WORKFLOW'][1]>
		<cfset accessStruct.DROIT_GESTION_FOURNIS = accessData['DROIT_GESTION_FOURNIS'][1]>
		<cfset accessStruct.GEST = accessStruct.MODULE_FIXE_DATA + accessStruct.MODULE_MOBILE +
																		accessStruct.MODULE_WORKFLOW>
		<cfset accessStruct.STRUCT = accessStruct.MODULE_GESTION_ORG + accessStruct.MODULE_GESTION_LOGIN>
		<cfset setAccessStatus = setUniversFacturationAccess(accessStruct) + setUniversGestionAccess(accessStruct) +
									setUniversParametresAccess(accessStruct) + setUniversUsagesAccess(accessStruct)>
		<cflog text="setAccessStatus = #setAccessStatus#">							
		<cfif setAccessStatus EQ 4>
			<cfset accessStruct.SET_ACCESS_STATUS=1>
			<cfset SESSION.UNIVERS_KEY=THIS.DEFAULT_UNIVERS>
			<cfset SESSION.FUNCTION_KEY="">
		<cfelse>
			<cfset accessStruct.SET_ACCESS_STATUS=0>
		</cfif>
		<cflog text="SESSION.CODEAPPLICATION = #SESSION.CODEAPPLICATION# -- SESSION.PERIMETRE.TYPE_LOGIN = #SESSION.PERIMETRE.TYPE_LOGIN#">
		<cflog text="accessStruct.SET_ACCESS_STATUS = #accessStruct.SET_ACCESS_STATUS#">	
		<cfif SESSION.CODEAPPLICATION eq 1>
			<cfif SESSION.PERIMETRE.TYPE_LOGIN eq 1>
				<cfset SESSION.XML_ACCESS_STATUS=accessStruct.SET_ACCESS_STATUS>
			</cfif>
		</cfif>
		<cflog text="setAccess - SESSION.XML_ACCESS_STATUS=#SESSION.XML_ACCESS_STATUS#">
		<cfset SESSION.PERIMETRE.DROIT_GESTION_FOURNIS=accessStruct.DROIT_GESTION_FOURNIS>
		<cfreturn accessStruct.SET_ACCESS_STATUS>
	</cffunction>
	<cffunction name="setUniversFacturationAccess" access="private" returntype="numeric">
		<cfargument name="accessStructData" required="true" type="struct">
		<cfset xmlDocument=SESSION.XML_ACCESS>
		<cfset universArray = XMLSearch(xmlDocument,"//UNIVERS[@KEY='FACT']")>
		<cfif arrayLen(universArray) EQ 1>
			<cfset universNode = universArray[1]>
			<!--- 
			<cfset universNode.XmlAttributes.USR = accessStruct.MODULE_FACTURATION>
			 --->
			<cfloop index="i" from="1" to="#arrayLen(universNode.XmlChildren)#">
				<cfset universNode.XmlChildren[i].XmlAttributes.USR = accessStruct.MODULE_FACTURATION>
			</cfloop>
			<cfreturn 1>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	<cffunction name="setUniversGestionAccess" access="private" returntype="numeric">
		<cfargument name="accessStructData" required="true" type="struct">
		<cfset xmlDocument=SESSION.XML_ACCESS>
		<cfset universArray = XMLSearch(xmlDocument,"//UNIVERS[@KEY='GEST']")>
		<cfif arrayLen(universArray) EQ 1>
			<cfset universNode = universArray[1]>
			<!--- 
			<cfset universNode.XmlAttributes.USR = accessStruct.GEST>
			 --->
			<cfset functionArray = XMLSearch(xmlDocument,"//FONCTION[@KEY='GEST_NDI_SERVICES']")>
			<cfif arrayLen(functionArray) EQ 1>
				<cfset functionNode = functionArray[1]>
				<cfset functionNode.XmlAttributes.USR = accessStruct.MODULE_FACTURATION>
			<cfelse>
				<cfreturn -1>
			</cfif>
			
			<!---<cfset functionArray = XMLSearch(xmlDocument,"//FONCTION[@KEY='GEST_FIXE_DATA']")>
			<cfif arrayLen(functionArray) EQ 1>
				<cfset functionNode = functionArray[1]>
				<cfset functionNode.XmlAttributes.USR = accessStruct.MODULE_FIXE_DATA>
			<cfelse>
				<cfreturn -2>
			</cfif>--->
			
			<cfset functionArray = XMLSearch(xmlDocument,"//FONCTION[@KEY='GEST_MOBILE']")>
			<cfif arrayLen(functionArray) EQ 1>
				<cfset functionNode = functionArray[1]>
				<cfset functionNode.XmlAttributes.USR = accessStruct.MODULE_MOBILE>
			<cfelse>
				<cfreturn -3>
			</cfif>
			
			<cfset functionArray = XMLSearch(xmlDocument,"//FONCTION[@KEY='GEST_REPORT']")>
			<cfif arrayLen(functionArray) EQ 1>
				<cfset functionNode = functionArray[1]>
				<cfset functionNode.XmlAttributes.USR = accessStruct.MODULE_FACTURATION>
			<cfelse>
				<cfreturn -4>
			</cfif>

			<cfset functionArray = XMLSearch(xmlDocument,"//FONCTION[@KEY='GEST_WORKFLOW']")>
			<cfif arrayLen(functionArray) EQ 1>
				<cfset functionNode = functionArray[1]>
				<cfset functionNode.XmlAttributes.USR = accessStruct.MODULE_WORKFLOW>
			<cfelse>
				<cfreturn -5>
			</cfif>
			
			<cfreturn 1>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	<cffunction name="setUniversParametresAccess" access="private" returntype="numeric">
		<cfargument name="accessStructData" required="true" type="struct">
		<cfset xmlDocument=SESSION.XML_ACCESS>
		<cfset universArray = XMLSearch(xmlDocument,"//UNIVERS[@KEY='STRUCT']")>
		<cfif arrayLen(universArray) EQ 1>
			<cfset universNode = universArray[1]>
			<!--- 
			<cfset universNode.XmlAttributes.USR = accessStruct.STRUCT>
			 --->
			<cfset functionArray = XMLSearch(xmlDocument,"//FONCTION[@KEY='STRUCT_ORG']")>
			<cfif arrayLen(functionArray) EQ 1>
				<cfset functionNode = functionArray[1]>
				<cfset functionNode.XmlAttributes.USR = accessStruct.MODULE_GESTION_ORG>
			<cfelse>
				<cfreturn -1>
			</cfif>
			
			<cfset functionArray = XMLSearch(xmlDocument,"//FONCTION[@KEY='STRUCT_USR']")>
			<cfif arrayLen(functionArray) EQ 1>
				<cfset functionNode = functionArray[1]>
				<cfset functionNode.XmlAttributes.USR = accessStruct.MODULE_GESTION_LOGIN>
			<cfelse>
				<cfreturn -2>
			</cfif>
			
			<cfreturn 1>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	<cffunction name="setUniversUsagesAccess" access="private" returntype="numeric">
		<cfargument name="accessStructData" required="true" type="struct">
		<cfset xmlDocument=SESSION.XML_ACCESS>
		<cfset universArray = XMLSearch(xmlDocument,"//UNIVERS[@KEY='USG']")>
		<cfif arrayLen(universArray) EQ 1>
			<cfset universNode = universArray[1]>
			<!--- 
			<cfset universNode.XmlAttributes.USR = accessStruct.MODULE_USAGE>
			 --->
			<cfloop index="i" from="1" to="#arrayLen(universNode.XmlChildren)#">
				<cfset universNode.XmlChildren[i].XmlAttributes.USR = accessStruct.MODULE_USAGE>
			</cfloop>
			<cfreturn 1>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	<cffunction name="getXmlAccessList" access="remote" returntype="xml">
		<cflog text="getXmlAccessList">
		
		<cfif SESSION.XML_ACCESS_STATUS EQ 1>
			<cfreturn SESSION.XML_ACCESS>
		<cfelse>
			<cfset fileName = expandPath("menus/Default_Menu_#SESSION.USER.GLOBALIZATION#.xml")>
			<cffile action="read" file="#fileName#" variable="XMLFileText">
			<cfset fakeXmlAccess=XmlParse(XMLFileText)>
			<cfreturn fakeXmlAccess>
		</cfif>
	</cffunction>
	<cffunction name="getXmlAccessListV4" access="remote" returntype="xml" hint="retourne un xml contenant le menu d'un login CV4">
		<cflog text="getXmlAccessListV4">
		
		<cfif SESSION.XML_ACCESS_STATUS EQ 1>
			<cfset initSessionXmlAccessV4()>
			<cfreturn SESSION.XML_ACCESSV4>
		<cfelse>
			<cfset fileName = expandPath("menus/Default_Menu_#SESSION.USER.GLOBALIZATION#.xml")>
			<cffile action="read" file="#fileName#" variable="XMLFileText">
			<cfset fakeXmlAccess=XmlParse(XMLFileText)>
			<cfreturn fakeXmlAccess>
		</cfif>
	</cffunction>
	<cffunction name="getXmlAccessListPFGP" access="remote" output="false" returntype="xml" hint="retourne un xml contenant le menu d'un login SFR PFGP">
		<cflog text="getXmlAccessListPFGP">
		
		<cfif SESSION.XML_ACCESS_STATUS EQ 1>
			<cfreturn SESSION.XML_ACCESS_PFGP>
		<cfelse>
			<cfset fileName = expandPath("menus/Default_Menu_#SESSION.USER.GLOBALIZATION#.xml")>
			<cffile action="read" file="#fileName#" variable="XMLFileText">
			<cfset fakeXmlAccess=XmlParse(XMLFileText)>
			<cfreturn fakeXmlAccess>
		</cfif>
	</cffunction>
	<cffunction name="setUniversFunction" access="remote" returntype="struct">
		<cfargument name="universKey" required="true" type="string">
		<cfargument name="functionKey" required="true" type="string">
		<cfargument name="actionKey" required="true" type="string">
		<cflog text="******************** universKey 	: #universKey#">
		<cflog text="******************** functionKey 	: #functionKey#">
		<cflog text="******************** actionKey 	: #actionKey#">
		
		<cfset accessXmlData = structNew()>
		
		
		<cfif SESSION.XML_ACCESS_STATUS EQ 1>
			<cfif SESSION.CODEAPPLICATION eq 51>
				<cflog text="*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!*!-SETUNIVERS FUNCTION PFGP"/>
				<cflog text="*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!*!-SETUNIVERS FUNCTION PFGP"/>
				<cflog text="*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!*!-SETUNIVERS FUNCTION PFGP"/>
				
				<cfset accessXmlData.ACTION_KEY = ARGUMENTS.actionKey>
				<cfset accessXmlData.UNIVERS_KEY=ARGUMENTS.universKey>
				<cfset accessXmlData.FUNCTION_KEY=ARGUMENTS.functionKey>
				<cfset accessXmlData.SET_UNIVERS_FUNCTION = 2>
			<cfelse>
				<cfif SESSION.PERIMETRE.TYPE_LOGIN eq 2>
					<cfset accessXmlData = setUniversFunctionCV4(universKey,functionKey,actionKey)>
				<cfelse>
					<cfset accessXmlData = setUniversFunctionCV3(universKey,functionKey,actionKey)>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn accessXmlData>
	</cffunction>
	<cffunction name="setUniversFunctionCV4" access="remote" returntype="struct">
	<cfargument name="universKey" required="true" type="string">
	<cfargument name="functionKey" required="true" type="string">
	<cfargument name="actionKey" required="true" type="string">
	
	<cflog text="*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!*!-SETUNIVERS FUNCTION CV4 ---- UniversKey #universKey#"/>
	<cflog text="*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!*!-SETUNIVERS FUNCTION CV4 ---- functionKey #functionKey#"/>
	<cflog text="*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!*!-SETUNIVERS FUNCTION CV4 ---- actionKey #actionKey#"/>
	
	<cfset resultStatus = structNew()>
	<cfset resultStatus.ACTION_KEY = ARGUMENTS.actionKey>
	<cfset resultStatus.UNIVERS_KEY=THIS.DEFAULT_UNIVERS>
	<cfset resultStatus.FUNCTION_KEY="">
	<cfset accessXmlData = SESSION.XML_ACCESSV4>
	<cfset universArray = XMLSearch(accessXmlData,"//UNIVERS[@KEY='#universKey#']")>
		<cfset toggledUniversList = XMLSearch(accessXmlData,"//UNIVERS[@TOGGLED='true']")>
		<cflog text="******************** univers : #arrayLen(universArray)#">
		<cfif arrayLen(universArray) EQ 1>
			<cflog text="******************** universKey : #universKey#">
			<cflog text="******************** default_univers = #THIS.DEFAULT_UNIVERS#">
			<cfif universKey EQ THIS.DEFAULT_UNIVERS>
				<cfset resultStatus.SET_UNIVERS_FUNCTION = universArray[1].XmlAttributes.USR>
				<cfif resultStatus.SET_UNIVERS_FUNCTION GT 0>
					<cfset SESSION.UNIVERS_KEY=universKey>
					<cfset SESSION.FUNCTION_KEY=''>
					<cfif arrayLen(toggledUniversList) EQ 1>
						<cfset toggledUniversList[1].XmlAttributes.toggled = "false">
						<cfloop index="i" from="1" to="#arrayLen(toggledUniversList[1].XmlChildren)#">
							<cfif toggledUniversList[1].XmlChildren[i].XmlAttributes.toggled EQ "true">
								<cfset toggledUniversList[1].XmlChildren[i].XmlAttributes.toggled = "false">
							</cfif>
						</cfloop>
					</cfif>
					<cfset universArray[1].XmlAttributes.toggled = "true">
				 </cfif>
			<cfelse>
				<cfset functionArray = XMLSearch(accessXmlData,"//FONCTION[@KEY='#functionKey#']")>
				<cflog text="******************** fonction : #arrayLen(functionArray)#">
				<cfif arrayLen(functionArray) EQ 1>
					<cfset resultStatus.SET_UNIVERS_FUNCTION = functionArray[1].XmlAttributes.USR>
					<cflog text="******************** <cfif resultStatus.SET_UNIVERS_FUNCTION GT 0> : #resultStatus.SET_UNIVERS_FUNCTION#">
					<cfif resultStatus.SET_UNIVERS_FUNCTION GT 0>
						<cfset SESSION.UNIVERS_KEY=universKey>
						<cfset SESSION.FUNCTION_KEY=functionKey>
						<cflog text="******************** <cfif arrayLen(toggledUniversList) EQ 1> : #arrayLen(toggledUniversList)#">
						<cfif arrayLen(toggledUniversList) EQ 1>
							<cfset toggledUniversList[1].XmlAttributes.toggled = "false">
							<cfloop index="i" from="1" to="#arrayLen(toggledUniversList[1].XmlChildren)#">
								<cfif toggledUniversList[1].XmlChildren[i].XmlAttributes.toggled EQ "true">
									<cfset toggledUniversList[1].XmlChildren[i].XmlAttributes.toggled = "false">
								</cfif>
							</cfloop>
						</cfif>
						<cfset universArray[1].XmlAttributes.toggled = "true">
						<cfset functionArray[1].XmlAttributes.toggled = "true">
					</cfif>
				<cfelse>
					<cfif universKey eq "GEST_CONT">
						<cfset resultStatus.SET_UNIVERS_FUNCTION = 2>
						<cfset resultStatus.UNIVERS_KEY = "GEST_CONT">
					<cfelse>
						<cfif universKey eq "FACT_REPORT_E0">	
							<cfset resultStatus.SET_UNIVERS_FUNCTION = 2>
							<cfset resultStatus.UNIVERS_KEY = "FACT_REPORT_E0">
						<cfelse>
							<cfset resultStatus.SET_UNIVERS_FUNCTION = -3>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
		<cfelse>
			<cfset resultStatus.SET_UNIVERS_FUNCTION = -2>
		</cfif>
	<cfreturn resultStatus>
	</cffunction> 
	<cffunction name="setUniversFunctionCV3" access="remote" returntype="struct">
	<cfargument name="universKey" required="true" type="string">
	<cfargument name="functionKey" required="true" type="string">
	<cfargument name="actionKey" required="true" type="string">
	
	<cflog text="*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!*!-SETUNIVERS FUNCTION CV3"/>
	<cflog text="*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!*!-SETUNIVERS FUNCTION CV3"/>
	<cflog text="*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!-*!*!-SETUNIVERS FUNCTION CV3"/>
	
	<cfset resultStatus = structNew()>
	<cfset resultStatus.ACTION_KEY = ARGUMENTS.actionKey>
	<cfset resultStatus.UNIVERS_KEY=THIS.DEFAULT_UNIVERS>
	<cfset resultStatus.FUNCTION_KEY="">
	<cfset accessXmlData = SESSION.XML_ACCESS>
	<cfset universArray = XMLSearch(accessXmlData,"//UNIVERS[@KEY='#universKey#']")>
		<cfset toggledUniversList = XMLSearch(accessXmlData,"//UNIVERS[@toggled='true']")>
		<cflog text="******************** univers : #arrayLen(universArray)#">
		<cfif arrayLen(universArray) EQ 1>
			<cflog text="******************** universKey : #universKey#">
			<cflog text="******************** default_univers = #THIS.DEFAULT_UNIVERS#">
			<cfif universKey EQ THIS.DEFAULT_UNIVERS>
				<cfset resultStatus.SET_UNIVERS_FUNCTION = universArray[1].XmlAttributes.USR>
				<cfif resultStatus.SET_UNIVERS_FUNCTION GT 0>
					<cfset SESSION.UNIVERS_KEY=universKey>
					<cfset SESSION.FUNCTION_KEY=''>
					<cfif arrayLen(toggledUniversList) EQ 1>
						<cfset toggledUniversList[1].XmlAttributes.toggled = "false">
						<cfloop index="i" from="1" to="#arrayLen(toggledUniversList[1].XmlChildren)#">
							<cfif toggledUniversList[1].XmlChildren[i].XmlAttributes.toggled EQ "true">
								<cfset toggledUniversList[1].XmlChildren[i].XmlAttributes.toggled = "false">
							</cfif>
						</cfloop>
					</cfif>
					<cfset universArray[1].XmlAttributes.toggled = "true">
				 </cfif>
			<cfelse>
				<cfset functionArray = XMLSearch(accessXmlData,"//FONCTION[@KEY='#functionKey#']")>
				<cflog text="******************** fonction : #arrayLen(functionArray)#">
				<cfif arrayLen(functionArray) EQ 1>
					<cfset resultStatus.SET_UNIVERS_FUNCTION = functionArray[1].XmlAttributes.USR>
					<cflog text="******************** <cfif resultStatus.SET_UNIVERS_FUNCTION GT 0> : #resultStatus.SET_UNIVERS_FUNCTION#">
					<cfif resultStatus.SET_UNIVERS_FUNCTION GT 0>
						<cfset SESSION.UNIVERS_KEY=universKey>
						<cfset SESSION.FUNCTION_KEY=functionKey>
						<cflog text="******************** <cfif arrayLen(toggledUniversList) EQ 1> : #arrayLen(toggledUniversList)#">
						<cfif arrayLen(toggledUniversList) EQ 1>
							<cfset toggledUniversList[1].XmlAttributes.toggled = "false">
							<cfloop index="i" from="1" to="#arrayLen(toggledUniversList[1].XmlChildren)#">
								<cfif toggledUniversList[1].XmlChildren[i].XmlAttributes.toggled EQ "true">
									<cfset toggledUniversList[1].XmlChildren[i].XmlAttributes.toggled = "false">
								</cfif>
							</cfloop>
						</cfif>
						<cfset universArray[1].XmlAttributes.toggled = "true">
						<cfset functionArray[1].XmlAttributes.toggled = "true">
					</cfif>
				<cfelse>
					<cfif universKey eq "GEST_CONT">
						<cfset resultStatus.SET_UNIVERS_FUNCTION = 2>
						<cfset resultStatus.UNIVERS_KEY = "GEST_CONT">
					<cfelse>
						<cfset resultStatus.SET_UNIVERS_FUNCTION = -3>
					</cfif>
				</cfif>
			</cfif>
		<cfelse>
			<cfset resultStatus.SET_UNIVERS_FUNCTION = -2>
		</cfif>
	<cfreturn resultStatus>
	</cffunction> 
	<!---
	Connexion sur un noeud. Changement de pï¿½rimï¿½tre != Changement de groupe
	Dans ce cas les seules informations utiles sont contenues dans PERIMETRE_DATA
	--->
	<cffunction name="connectOnNode" access="remote" returntype="struct">
		<cfargument name="accessId" required="true" type="numeric">
		<cfargument name="nodeId" required="true" type="numeric">
		<cfset perimetreNodeInfos=structNew()>
		<cfset perimetreNodeInfos.PERIMETRE_INFOS=setPerimetre(accessId,nodeId)>
		<cfreturn perimetreNodeInfos>
	</cffunction>
	<cffunction name="searchNodes" access="remote" returntype="query">
		<cfargument name="nodeId" required="true" type="numeric">
		<cfargument name="searchKeyword" required="true" type="string">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.search_noeud">
			<cfprocparam  value="#nodeId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#SESSION.USER.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#searchKeyword#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam  value="50" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNodeList">
		</cfstoredproc>
		<cfreturn qGetNodeList>
	</cffunction>
	<cffunction name="getNodeXmlPath" access="remote" returntype="xml">
		<cfargument name="idGroupeRacine" required="true" type="numeric">
		<cfargument name="idSource" required="true" type="numeric">
		<cfargument name="idCible" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.chemin_source_cible_v2">
			<cfprocparam  value="#idSource#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#idCible#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#SESSION.USER.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qChemin">
		</cfstoredproc>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"NODE")>
		<cfset rootNode = perimetreXmlDoc.NODE>
		<cfif qChemin.recordcount GT 0>
			<cfset typeOrga = qChemin['TYPE_ORGA'][qChemin.recordcount]>
			<cfloop index="i" from="1" to="#qChemin.recordcount#">
				<cfif i EQ 1>
					<cfset rootNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
					<cfset rootNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset rootNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
					<cfset rootNode.XmlAttributes.STC = qChemin['STC'][i]>
					<cfif idSource EQ idGroupeRacine>
						<cfif qChemin.recordcount GT 1>
							<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"NODE")>
							<cfswitch expression="#UCASE(typeOrga)#">
								<cfcase value="OPE">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Opérateurs">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -2>
								</cfcase>
								<cfcase value="CUS">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Clientes">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -3>
								</cfcase>
								<cfcase value="SAV">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Recherches Sauvegardées">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -4>
								</cfcase>
								<cfdefaultcase>
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Clientes">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -3>
								</cfdefaultcase>
							</cfswitch>

							<cfset rootNode.XmlChildren[1].XmlAttributes.NTY = 0>
							<cfset rootNode.XmlChildren[1].XmlAttributes.STC = 0>
						</cfif>
					</cfif>
				<cfelseif i EQ 2>
					<cfset parentNode = ''>
					<cfif idSource EQ idGroupeRacine>
						<cfset rootNode.XmlChildren[1].XmlAttributes.NTY = 1>
						<cfset parentNode = rootNode.XmlChildren[1]>
					<cfelse>
						<cfset tmpNID = qChemin['IDGROUPE_CLIENT'][i]>
						<cfset tmpParentNID = qChemin['IDGROUPE_CLIENT'][i - 1]>
						<cfoutput>
							<cfset nodeArray = XMLSearch(perimetreXmlDoc,"//NODE[@NID=#tmpParentNID#]")>
							<cfif arrayLen(nodeArray) NEQ 1>
								<cfthrow type="INVALID_GROUP_NODES_QUERY"
										message="Requï¿½te Arbre Pï¿½rimï¿½tres Invalide"
										detail="1006">
							</cfif>
							<cfset parentNode = nodeArray[1]>
						</cfoutput>
					</cfif>
					<cfset parentChildNb = arrayLen(parentNode.XmlChildren)>
					<cfset parentNode.XmlChildren[parentChildNb + 1] = XmlElemNew(perimetreXmlDoc,"NODE")>
					<cfset tmpNode = parentNode.XmlChildren[parentChildNb + 1]>
					<cfset tmpNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
					<cfset tmpNode.XmlAttributes.STC = qChemin['STC'][i]>
				<cfelse>
					<cfset tmpNID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset tmpParentNID = qChemin['IDGROUPE_CLIENT'][i - 1]>
					<cfoutput>
						<cfset nodeArray = XMLSearch(perimetreXmlDoc,"//NODE[@NID=#tmpParentNID#]")>
						<cfif arrayLen(nodeArray) NEQ 1>
							<cfthrow type="INVALID_GROUP_NODES_QUERY"
									message="Requï¿½te Arbre Pï¿½rimï¿½tres Invalide"
									detail="1006">
						</cfif>
						<cfset parentNode = nodeArray[1]>
						<cfset parentChildNb = arrayLen(parentNode.XmlChildren)>
						<cfset parentNode.XmlChildren[parentChildNb + 1] = XmlElemNew(perimetreXmlDoc,"NODE")>
						<cfset tmpNode = parentNode.XmlChildren[parentChildNb + 1]>
						<cfset tmpNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
						<cfset tmpNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
						<cfset tmpNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
						<cfset tmpNode.XmlAttributes.STC = qChemin['STC'][i]>
					</cfoutput>
				</cfif>
			</cfloop>
		<cfelse>
			<cfset rootNode.XmlAttributes.LBL = "Pas de chemin trouvï¿½">
			<cfset rootNode.XmlAttributes.NID = -1>
			<cfset rootNode.XmlAttributes.NTY = 0>
			<cfset rootNode.XmlAttributes.STC = 0>
		</cfif>
		<cfset qChemin = ''>
		<cfreturn perimetreXmlDoc>
	</cffunction>
	<cffunction name="sendTrackingInfos" access="remote" returntype="numeric">
		<cfargument name="universKey" type="string" required="true">
		<cfargument name="functionId" type="numeric" required="true">
		<cfif structKeyExists(SESSION,"TRACKING")>
			<cfset SESSION.TRACKING.UNIVERS = universKey>
			<cfset SESSION.TRACKING.FUNCTIONID = functionId>
		</cfif>
		<cfreturn functionId>
	</cffunction>
	 <!--- ================ TEMP ========================================================================== --->
	 <cffunction name="getNodeChild" access="remote" returntype="xml">
		<cfargument name="nodeId" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#THIS.PKG_GLOBAL#.enfant_noeud">
			<cfprocparam  value="#nodeId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetChild">
		</cfstoredproc>
		<cfset childXmlTree = XmlNew()>
		<cfset childXmlTree.xmlRoot = XmlElemNew(childXmlTree,"NODE")>
		<cfset rootNode = childXmlTree.NODE>
		<cfset rootNode.XmlAttributes.LBL = "Enfants de " & nodeId>
		<cfset rootNode.XmlAttributes.NID = nodeId>
		<cfset rootNode.XmlAttributes.NTY = 0>
		<cfset rootNode.XmlAttributes.STC = 0>
		<cfloop index="i" from="1" to="#qGetChild.recordcount#">
			<cfset rootNode.XmlChildren[i] = XmlElemNew(childXmlTree,"NODE")>
			<cfset rootNode.XmlChildren[i].XmlAttributes.LBL = qGetChild['LIBELLE_GROUPE_CLIENT'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NID = qGetChild['IDGROUPE_CLIENT'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NTY = qGetChild['TYPE_NOEUD'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NUMERO_NIVEAU = qGetChild['NUMERO_NIVEAU'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.IDORGA_NIVEAU = qGetChild['IDORGA_NIVEAU'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.STC = 1>
		</cfloop>
		<cfset qGetChild = ''>
		<cfreturn childXmlTree>
	</cffunction>
	<!--- =========================== FIN TEMP ================================================================== --->
	 <cffunction access="private" name="getPFGPDateLoad" returntype="void" >
	 	<cflog text="accessmanager.getPFGPDateLoad">
		<cflog text="SESSION.PERIMETRE.ID_GROUPE = #SESSION.PERIMETRE.ID_GROUPE#">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m00.get_last_periode_complete">			
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qDATELASTPFGP">
		</cfstoredproc>
		<cflog text="qDATELASTPFGP.recordcount = #qDATELASTPFGP.recordcount#">
		<cfif qDATELASTPFGP.recordcount GT 0>
			<cfset SESSION.DATES.LASTPFGPLOAD = qDATELASTPFGP['LAST_MOIS'][1]>
			<cfset SESSION.DATES.LASTPFGPIDPERIODE = qDATELASTPFGP['LAST_IDPERIODE_MOIS'][1]>
		<cfelse>
			<cfset SESSION.DATES.LASTPFGPLOAD = createDate(1970,1,1)>
			<cfset SESSION.DATES.LASTPFGPIDPERIODE = 0>
		</cfif>
	 </cffunction>
</cfcomponent>
