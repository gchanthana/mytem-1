<cfcomponent name="gestionPerimetreStructure">
	
	<cffunction name="getOrganisation" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfset realid=id_groupe/>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_organisations_plus">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#realid#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>			
			<cfquery  name="qresult" dbtype="query">
				select * from p_result Order by TYPE_ORGA asc
			</cfquery>
			<cfreturn qresult>
		</cffunction>
		
	<!----><cffunction name="addOrganisation" returntype="string" access="remote">
		 	<cfargument name="name" required="true" type="string" />
		 	<cfargument name="comment" required="true" type="string" />
		 	<cfargument name="type" required="true" type="numeric" />
	 		<cfargument name="iscopy" required="true" type="boolean" />
		 	<cfargument name="OrganisationSource" required="true" type="numeric" />
		 	<cfargument name="copyLines" required="true" type="boolean" />
			<cfreturn "ok">
		</cffunction>----->
		
		<cffunction name="duplicateNodes" returntype="string" access="remote">
		 	<cfargument name="idsource" required="true" type="numeric" />
		 	<cfargument name="idDestination" required="true" type="numeric" />
		 	<cfargument name="withLines" required="true" type="boolean" />
			<cfreturn "ok">
		</cffunction>
		
		<cffunction name="moveNodes" returntype="numeric" access="remote">
		 	<cfargument name="idsource" required="true" type="numeric" />
		 	<cfargument name="idDestination" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.Move_node">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsource#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDestination#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		
		<cffunction name="addOrganisation" returntype="Numeric" access="remote">
		 	<cfargument name="id_racine" required="true" type="numeric" />
		 	<cfargument name="libelle_orga" required="true" type="string" />
		 	<cfargument name="comment_orga" required="true" type="string" />
		 	<cfargument name="type_decoupage" required="true" type="numeric" />
		 	<cfargument name="type_orga" required="true" type="string" />
		 	<!---- A voir ---->
		 	<cfargument name="operateurid" required="true" type="numeric" />
		 	<cfargument name="copylines" required="true" type="numeric" />
		 	<cfargument name="idsouce_organisation" required="true" type="numeric" />
		 	<cfargument name="type_affect" required="true" type="string" />
		 	
		 	<cfset realid=id_racine/>
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.add_organisation">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#realid#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle_orga#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#comment_orga#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#type_decoupage#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_CHAR" value="#type_orga#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#operateurid#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#copylines#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsouce_organisation#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#type_affect#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
</cfcomponent>