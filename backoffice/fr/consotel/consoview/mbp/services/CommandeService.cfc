<cfcomponent name="commandeService">
	<cffunction name="getInfosBlocCommande" access="remote" returntype="query" description="">
		<cfargument name="idSociete" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_mbp_global.getInfosBlockCommande_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result> 
	</cffunction> 
	<cffunction name="searchListeCommande" access="remote" returntype="Query">
		<cfargument name="numeroCmd" type="Numeric">
		<cfargument name="etatCmd" type="String">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.search">
			<cfprocparam value="#numeroCmd#" cfsqltype="CF_SQL_INTEGER" null="true">
			<cfprocparam value="#etatCmd#" cfsqltype="CF_SQL_VARCHAR" null="true">
			<cfprocresult name="p_result">
		</cfstoredproc>
	
	<cfreturn> p_result>
	</cffunction>
	<cffunction name="listetTypePack" access="remote" returntype="Query">
		<cfargument name="idSociete" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.list_packs">
			<cfprocparam value="#idSociete#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="q_result">
		</cfstoredproc>
		<cfreturn q_result> 
	</cffunction>
	<cffunction name="listeFonctionPack" access="remote" returntype="Query">
		<cfargument name="id" type="String">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.list_fctbase">
			<cfprocparam value="#id#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="q_result">
		</cfstoredproc>
		<cfreturn q_result> 
	</cffunction>
	<cffunction name="suppModele" access="remote" returntype="Query">
		<cfargument name="id" type="String">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.delete_modele">
			<cfprocparam value="#id#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="result" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn q_result> 
	</cffunction>
	<cffunction name="listeEquipement" access="remote" returntype="Query">
		<cfargument name="enr_playid" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.list_equipements">
			<cfprocparam value="#enr_playid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="q_result">
		</cfstoredproc>
		<cfreturn q_result> 
	</cffunction>
	<cffunction name="listeDetailEquipement" access="remote" returntype="Struct">
		<cfargument name="marque" type="String">
		<cfargument name="model" type="String">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.detail_equipement">
			<cfprocparam value="#marque#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#model#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="result1" resultset="1">
			<cfprocresult name="result2" resultset="2">
		</cfstoredproc>
		
		<cfset obj= StructNew()>
		<cfset obj.PRIX = result1>
		<cfset obj.CARACTERISTIQUE = result2>		
		<cfreturn obj>
	</cffunction>
	<cffunction name="listOption" access="remote" returntype="Query">	
		<cfargument name="packcode" type="String">
		<cfargument name="idcmd" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.list_options">
			<cfprocparam value="#packcode#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#idcmd#" cfsqltype="CF_SQL_INTEGER" type="in" null="true">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="compatibiliteOption" access="remote" returntype="Struct">	
		<cfargument name="packcode" type="String" required="true">
		<cfargument name="list" type="string">
		<cfargument name="option" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.compatibilite_option">
			<cfprocparam value="#packcode#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#list#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#option#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="code" cfsqltype="CF_SQL_INTEGER" type="out">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfset obj= StructNew()>
		<cfset obj.CODE = code>
		<cfset obj.LISTE= p_result>		
		<cfreturn obj>

	</cffunction>	
	<cffunction name="GetInfosCmd" access="remote" returntype="query">
		<cfargument name="id" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.GET_INFOSAUVEGARDE">
			<cfprocparam value="#id#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>	
	<cffunction name="getTarifPack" access="remote" returntype="query">
		<cfargument name="enr_playid" type="Numeric">
		<cfargument name="offercode" type="String">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.get_tarif_pack">
			<cfprocparam value="#enr_playid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#offercode#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>		
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getTarifOption" access="remote" returntype="query">
		<cfargument name="enr_playid" type="Numeric">
		<cfargument name="offercode" type="String">
		<cfargument name="attributecode" type="String" default="">
		<cfargument name="valuecode" type="String" default="">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.get_tarif">
			<cfprocparam value="#enr_playid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#offercode#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#attributecode#" cfsqltype="CF_SQL_VARCHAR" type="in" null="true">
			<cfprocparam value="#valuecode#" cfsqltype="CF_SQL_VARCHAR" type="in" null="true">
			<cfprocresult name="p_result">
		</cfstoredproc>		
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getTarifEquipement" access="remote" returntype="query">
		<cfargument name="enr_playid" type="Numeric">
		<cfargument name="equipementcode" type="String">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.get_tarif_equipement">
			<cfprocparam value="#enr_playid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#equipementcode#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>		
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getTarifGlobal" access="remote" returntype="Array">
		<cfargument name="arr" type="Array">
		<cfset arrEngagement=arraynew(1)>
		
		<cfloop index="i" from="1" to=#ArrayLen(arr)#>
			<cfif arr[i]['type'] eq "PACK">
				<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.get_tarif_pack">
					<cfprocparam value="-1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam value="#arr[i]['id']#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocresult name="p_result">
				</cfstoredproc>	
			</cfif>
			<cfif arr[i]['type'] eq "EQUIPEMENT">
				<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.get_tarif_equipement">
					<cfprocparam value="-1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam value="#arr[i]['id']#" cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocresult name="p_result">
				</cfstoredproc>		
			</cfif>
			<cfif arr[i]['type'] eq "OPTION">
				<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.get_tarif">
					<cfprocparam value="-1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam value="#arr[i]['id']#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam value="#arr[i]['attributecode']#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam value="#arr[i]['valuecode']#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocresult name="p_result">
				</cfstoredproc>		
			</cfif>
			<cfset arrayappend(arrEngagement,p_result)>	
		</cfloop>
		<cfreturn arrEngagement>
	</cffunction>
	<cffunction name="listSite" access="remote" returntype="query">	
		<cfargument name="companyID" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.List_Site">
			<cfprocparam value="#companyID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	<cffunction name="listCollaborateur" access="remote" returntype="query">	
		<cfargument name="siteID" type="numeric">
		<cfargument name="companyID" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.List_Collaborateur">
			<cfprocparam value="#siteID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#companyID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	<cffunction name="listSDA" access="remote" returntype="query">	
		<cfargument name="siteID" type="numeric">
		<cfargument name="companyID" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.List_NDS">
			<cfprocparam value="#siteID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#companyID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	<cffunction name="listSDAOccupe" access="remote" returntype="query">	
		<cfargument name="companyID" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.List_Used_NDS">
			<cfprocparam value="#companyID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	<cffunction name="listSDAOccupeSite" access="remote" returntype="query">	
		<cfargument name="companyID" type="numeric">
		<cfargument name="siteID" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.LIST_USED_NDS_SITE">
			<cfprocparam value="#companyID#" cfsqltype="CF_SQL_INTEGER"	type="in">
			<cfprocparam value="#siteID#" 	 cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	<cffunction name="addUser" access="remote" returntype="Numeric">
		<cfargument name="siteID" type="Numeric">
		<cfargument name="companyID" type="Numeric">	
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.addUser">
			<cfprocparam value="#siteID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#companyID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="valideCommande" access="remote" returntype="struct">
		<cfargument name="p_idihm_user" type="Numeric">
		<cfargument name="p_companyid" type="Numeric">
		<cfargument name="p_siteid" type="Numeric">
		<cfargument name="p_idtype_demande" type="Numeric">
		<cfargument name="p_idcode_operation" type="Numeric">
		<cfargument name="p_label" type="String">	
		<cfargument name="p_qte" type="Numeric">
		<cfargument name="p_obligationduration" type="Numeric">
						
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_commande">
			<cfprocparam value="#p_idihm_user#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_companyid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_siteid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_idtype_demande#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_idcode_operation#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam value="#p_label#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#p_qte#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam value="#p_obligationduration#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam variable="num_cmd" cfsqltype="CF_SQL_VARCHAR" type="out">
			<cfprocparam variable="idgestion_pack" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfset obj= StructNew()>
		<cfset obj.idgestion_pack = idgestion_pack>
		<cfset obj.num_cmd= num_cmd>
		<cfset obj.idihm_user = p_idihm_user>	
		<cfset obj.companyid = p_companyid>
		<cfset obj.siteid = p_siteid>
		<cfset obj.label = p_label>
		<cfset obj.qte = p_qte>
		<cfset obj.obligationduration = p_obligationduration>
		<cfreturn obj>
	</cffunction>
	<cffunction name="validePack" access="remote" returntype="array">
		<cfargument name="tab" type="Array">
		<cfargument name="idgestion_pack" type="Numeric">
		<cfargument name="idgestion_person" type="Numeric">
		<cfargument name="idcode_operation" type="Numeric">
		<cfargument name="companyid" type="Numeric">
		<cfargument name="siteid" type="Numeric">		
		<cfargument name="packcode" type="string">
		<cfargument name="num_cmd" type="string">
		<cfargument name="engagement" type="Numeric">
		
		<cfset arr = ArrayNew(1)>
		<cfloop index="i" from="1" to=#ArrayLen(Tab)#>
			<cfset obj = StructNew()>
			<cfset obj.idgestion_pack = #idgestion_pack#>
			<cfset obj.idgestion_person = #idgestion_person#>
			<cfset obj.IDCODE_OPERATION = #idcode_operation#>
			<cfset obj.companyid = #companyid#>
			<cfset obj.siteid = #siteid#>
			<cfset obj.packcode = #packcode#>
			<cfset obj.num_cmd = #num_cmd#>
			<cfset obj.num_item = #i#>
			<cfset obj.nds = Tab[i]['SDA']>
			<cfset obj.msisdn = Tab[i]['MSISDN']>
			<cfset obj.code_rio = Tab[i]['codeRIO']>
			<cfset obj.useridbew = Tab[i]['idBEW']>
			<cfset obj.c_personid = val(Tab[i]['packUserID'])>
			<cfset obj.collaborateur = Tab[i]['packUserName']>
			<cfset obj.date_portabilite = Tab[i]['datePortage']>
			<cfset obj.DUREE_ENGAGEMENT = #engagement#>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_item">
				<cfprocparam value="#idgestion_pack#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#idgestion_person#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#idcode_operation#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#companyid#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#siteid#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#packcode#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#num_cmd#" cfsqltype="cf_SQL_VARCHAR">
				<cfprocparam value="#i#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#Tab[i]['SDA']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#Tab[i]['MSISDN']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#Tab[i]['codeRIO']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#Tab[i]['idBEW']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#val(Tab[i]['packUserID'])#" cfsqltype="CF_SQL_INTEGER">			
				<cfprocparam value="#Tab[i]['packUserName']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#Tab[i]['datePortage']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#engagement#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			<cfset obj.result = p_result>
			<cfset arr[i] = obj>
		</cfloop>
		<cfreturn arr>
	</cffunction>
	<cffunction name="valideOption" access="remote" returntype="array">
		<cfargument name="tab" type="Array">
		
		<cfset arr = ArrayNew(1)>
		
		<cfloop index="i" from="1" to=#ArrayLen(Tab)#>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_item_cat">
				<cfprocparam value="#val(Tab[i]['idgestion_item'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#Tab[i]['offercode']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#Tab[i]['attributecode']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#Tab[i]['valuecode']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#val(Tab[i]['engagement'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['type_option'])#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam variable="p_result" cfsqltype="cF_SQL_INTEGER" type="out">		
			</cfstoredproc>
				<cfset obj.result = p_result>
				<cfset arr[i] = obj>
		</cfloop>
		<cfreturn arr>  
	</cffunction> 
	<cffunction name="valideEquipement" access="remote" returntype="array">
			<cfargument name="tab" type="array">
			
			<cfset arr = ArrayNew(1)>
			
			<cfloop index="i" from="1" to=#ArrayLen(Tab)#>
				<cfset obj= StructNew()>
				<cfset obj.idgestion_item = val(Tab[i]['idgestion_item'])>
				<cfset obj.equipmentcode= val(Tab[i]['equipmentcode'])>
				<cfset obj.engagement = val(Tab[i]['engagement'])>	
				<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_item_equipt">
					<cfprocparam value="#val(Tab[i]['idgestion_item'])#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam value="#Tab[i]['equipmentcode']#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam value="#val(Tab[i]['engagement'])#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
				<cfset obj.result = p_result>
				<cfset arr[i] = obj>
			</cfloop>
			<cfreturn arr>
		</cffunction>
	<cffunction name="listeCommande" access="remote" returntype="query" description="">
		<cfargument name="idSociete" type="numeric" required="true">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.liste_commande">
			<cfprocparam value="#idSociete#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">		
		</cfstoredproc>

		<cfreturn p_result>
	</cffunction>
	<cffunction name="valideEndCommande" access="remote" returntype="numeric">
		<cfargument name="idgestionpack" type="Numeric">	
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.Valide_End_Commande">
			<cfprocparam value="#idgestionpack#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam variable="p_result" type="out" cfsqltype="CF_SQL_INTEGER">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="listeFunctionOfPack" access="remote" returntype="query" description="">
		<cfargument name="idPack" type="numeric" required="true">
		<cfset p_result=listeFunctionDemo()>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="listeType" access="remote" returntype="query">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.list_type_equipement">
			<cfprocresult name="p_result">		
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="demande_list_Options" access="remote" returntype="query">
 	<cfargument name="packID" type="Numeric">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_parc.getListOptDemande">
		<cfprocparam value="#packID#" cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocresult name="p_result">
	</cfstoredproc> 
	<cfreturn p_result>
</cffunction>
	<cffunction name="demande_list_equipement" access="remote" returntype="query">
	 	<cfargument name="packID" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_parc.getequipdemande">
			<cfprocparam value="#packID#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc> 
		<cfreturn p_result>
	</cffunction>
	<cffunction name="demande_infos" access="remote" returntype="query">
	 	<cfargument name="packID" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_parc.getDetailDemande">
			<cfprocparam value="#packID#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc> 
		<cfreturn p_result>
	</cffunction>
	<cffunction name="listOption_init" access="remote" returntype="query">
 	<cfargument name="packID" type="Numeric">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.list_options_mod">
		<cfprocparam value="#packID#" cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocresult name="p_result">
	</cfstoredproc> 
	<cfreturn p_result>
</cffunction>
	<cffunction name="listePackOfCommande" access="remote" returntype="query">
		<cfargument name="idcommande" type="Numeric">
	
		<cfstoredproc datasource="#session.offredsn#" procedure="pkg_parc.getListDemande">
			<cfprocparam value="#idcommande#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getOfferCode" access="remote" returntype="Query">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_parc.getOfferCode">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>


	<cffunction name="valideCommande_mp" access="remote" returntype="struct">
		<cfargument name="p_idihm_user" type="Numeric">
		<cfargument name="p_packid" type="Numeric">
		<cfargument name="p_label" type="String">	
		<cfargument name="p_obligationduration" type="Numeric">
						
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_commande_MP">
			<cfprocparam value="#p_idihm_user#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_packid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="1" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_label#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="1" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam value="#p_obligationduration#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam variable="num_cmd" cfsqltype="CF_SQL_VARCHAR" type="out">
			<cfprocparam variable="idgestion_pack" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfset obj= StructNew()>
		<cfset obj.idgestion_pack = idgestion_pack>
		<cfset obj.num_cmd= num_cmd>
		<cfreturn obj>
	</cffunction>
	<cffunction name="validePack_mp" access="remote" returntype="numeric">
		<cfargument name="idgestion_pack" type="Numeric">
		<cfargument name="packid" type="Numeric">
		<cfargument name="packcode" type="string">
		<cfargument name="num_cmd" type="string">
		<cfargument name="num_item" type="numeric">
		<cfargument name="date" type="string">
		<cfargument name="engagement" type="numeric">
		<cfargument name="passBEW" type="string">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_item_mp">
			<cfprocparam value="#idgestion_pack#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#packid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#packcode#" cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam value="#num_cmd#" cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam value="#num_item#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#date#" cfsqltype="cf_SQL_VARCHAR" null="true">
			<cfprocparam value="#engagement#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#passBEW#" cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="valideOption_mp" access="remote" returntype="numeric">
		<cfargument name="tab" type="Array">
		<cfargument name="idgestion_item" type="numeric">
		
		<cfset result="1">
		<cfloop index="i" from="1" to=#ArrayLen(Tab)#>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_item_cat_mp">
				<cfprocparam value="#idgestion_item#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#Tab[i]['code']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#Tab[i]['offercode']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#Tab[i]['attributecode']#" cfsqltype="CF_SQL_VARCHAR" null="true">
				<cfprocparam value="#Tab[i]['valuecode']#" cfsqltype="CF_SQL_VARCHAR" null="true">
				<cfprocparam value="#val(Tab[i]['engagement'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="3" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam variable="p_result" cfsqltype="CF_SQL_VARCHAR" type="out">		
			</cfstoredproc>
			<cfif p_result lt 0>
				<cfset result = 0>
			</cfif>
		</cfloop>
		<cfreturn result>  
	</cffunction> 
	<cffunction name="valideEquipement_mp" access="remote" returntype="numeric">
			<cfargument name="tab" type="array">
			<cfargument name="idgestion_item" type="numeric">
			
			<cfset result="1">
			
			<cfloop index="i" from="1" to=#ArrayLen(Tab)#>
				<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_item_equipt_mp">
					<cfprocparam value="#idgestion_item#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam value="#Tab[i]['Equipement_code']#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam value="#val(Tab[i]['engagement'])#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
			<cfif p_result lt 0>
				<cfset result = 0>
			</cfif>
			</cfloop>
			<cfreturn result>
		</cffunction>

	<cffunction name="valideCommande_mg" access="remote" returntype="struct">
		<cfargument name="p_idihm_user" type="Numeric">
		<cfargument name="p_packid" type="Numeric">
		<cfargument name="p_label" type="String">	
		<cfargument name="p_obligationduration" type="Numeric">
						
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_commande_MG">
			<cfprocparam value="#p_idihm_user#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_packid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="1" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_label#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#p_obligationduration#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam variable="num_cmd" cfsqltype="CF_SQL_VARCHAR" type="out">
			<cfprocparam variable="idgestion_pack" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfset obj= StructNew()>
		<cfset obj.idgestion_pack = idgestion_pack>
		<cfset obj.num_cmd= num_cmd>
		<cfreturn obj>
	</cffunction>
	<cffunction name="validePack_mg" access="remote" returntype="numeric">
		<cfargument name="idgestion_pack" type="Numeric">
		<cfargument name="packid" type="Numeric">
		<cfargument name="packcode" type="string">
		<cfargument name="num_cmd" type="string">
		<cfargument name="num_item" type="numeric">
		<cfargument name="date" type="string">
		<cfargument name="engagement" type="numeric">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_item_mg">
			<cfprocparam value="#idgestion_pack#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#packid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#packcode#" cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam value="#num_cmd#" cfsqltype="cf_SQL_VARCHAR">
			<cfprocparam value="#num_item#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#date#" cfsqltype="cf_SQL_VARCHAR" null="true">
			<cfprocparam value="#engagement#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="valideOption_mg" access="remote" returntype="numeric">
		<cfargument name="tab" type="Array">
		<cfargument name="idgestion_item" type="numeric">
		
		<cfset result="1">
		<cfloop index="i" from="1" to=#ArrayLen(Tab)#>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_item_cat_mg">
				<cfprocparam value="#idgestion_item#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#Tab[i]['code']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#Tab[i]['offercode']#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#Tab[i]['attributecode']#" cfsqltype="CF_SQL_VARCHAR" null="true">
				<cfprocparam value="#Tab[i]['valuecode']#" cfsqltype="CF_SQL_VARCHAR" null="true">
				<cfprocparam value="#val(Tab[i]['engagement'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="3" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam variable="p_result" cfsqltype="CF_SQL_VARCHAR" type="out">		
			</cfstoredproc>
			<cfif p_result lt 0>
				<cfset result = 0>
			</cfif>
		</cfloop>
		<cfreturn result>  
	</cffunction> 
	<cffunction name="valideEquipement_mg" access="remote" returntype="numeric">
			<cfargument name="tab" type="array">
			<cfargument name="idgestion_item" type="numeric">
			
			<cfset result="1">
			
			<cfloop index="i" from="1" to=#ArrayLen(Tab)#>
				<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.valide_item_equipt_mg">
					<cfprocparam value="#idgestion_item#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam value="#Tab[i]['Equipement_code']#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam value="#val(Tab[i]['engagement'])#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
			<cfif p_result lt 0>
				<cfset result = 0>
			</cfif>
			</cfloop>
			<cfreturn result>
		</cffunction>

	<cffunction name="listeTypeDemande" access="remote" returntype="Query">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.getTypeDemande">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="listeCodeOperation" access="remote" returntype="Query">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.getCodeOperation">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="listeEtatDemande" access="remote" returntype="Query">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.getListeEtatDemande">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="listeClientOBS" access="remote" returntype="Query">
		<cfargument name="idihm_user" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.getPortefeuilleClient">
			<cfprocparam value="#idihm_user#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="listeDemande" access="remote" returntype="Query">
		<cfargument name="idihm_user" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.getListDemande">
			<cfprocparam value="#idihm_user#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getDemandeParametre" access="remote" returntype="Query">
		<cfargument name="iddemande" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.getDemandeParameters">
			<cfprocparam value="#iddemande#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="saveDemandeParametre" access="remote" returntype="numeric">
		<cfargument name="iddemande" type="numeric">
		<cfargument name="idsite" type="numeric">
		<cfargument name="companyid" type="numeric">
		<cfargument name="nds" type="string">
		<cfargument name="msisdn" type="string">
		<cfargument name="coderio" type="string">
		<cfargument name="useridbew" type="string">
		<cfargument name="personid" type="numeric">
		<cfargument name="collaborateur" type="string">
		<cfargument name="dateportabilite" type="string">
		<cfargument name="idihm_user" type="numeric">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.saveAnoDemande">
			<cfprocparam value="#iddemande#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idsite#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#companyid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#nds#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#msisdn#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#coderio#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#useridbew#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#personid#" cfsqltype="CF_SQL_iNTEGER" type="in">
			<cfprocparam value="#collaborateur#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#dateportabilite#" cfsqltype="CF_SQL_VARCHAR" type="in" null="true">
			<cfprocparam value="#idihm_user#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="p_result" cfsqltype="CF_SQL_inTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="annulerDemande" access="remote" returntype="numeric">
		<cfargument name="iddmd" type="Numeric">
		<cfargument name="idihm_user" type="Numeric">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.annulerDemande">
			<cfprocparam value="#iddmd#" 	 	cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idihm_user#" 	cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam variable="p_result" 	cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>

	<cffunction name="listetMigrationTypePack" access="remote" returntype="Query">
		<cfargument name="idSociete" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.list_packs">
			<cfprocparam value="#idSociete#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="q_result">
		</cfstoredproc>
		<cfreturn q_result> 
	</cffunction>
	<cffunction name="migration_listOption_init" access="remote" returntype="query">
	 	<cfargument name="packID" type="Numeric">
	 	<cfargument name="packoffer" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.list_options">
		<!--- 	<cfprocparam value="#packID#" cfsqltype="CF_SQL_INTEGER" type="in"> --->
		 	<cfprocparam value="#packoffer#" cfsqltype="CF_SQL_varCHAR" type="in">
		 	<cfprocparam value="" cfsqltype="CF_SQL_INTEGER" type="in" null="true">
 			<cfprocresult name="p_result">
		</cfstoredproc> 
		<cfreturn p_result>
	</cffunction>
</cfcomponent>
