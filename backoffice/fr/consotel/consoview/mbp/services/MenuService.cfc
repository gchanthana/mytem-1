<cfcomponent name="menuService">
	
	<cffunction name="getmenu" access="remote" returntype="Array">
		<cfargument name="login" type="numeric">
		
		<cfset arr= ArrayNew(1)>
		
	<!--- 	<cfif login eq 76882>
			 	 --->	
	 		<cfset obj= StructNew()>
			<cfset obj.libelle = "accueil">
			<cfset obj.codemenu = "IHM_ACCUEIL">
			<cfset obj.ordremenu = 1>
			<cfset obj.idparent = "">
			<cfset arr[1] = obj>
	
			<cfset obj2= StructNew()>
			<cfset obj2.libelle = "les packs">
			<cfset obj2.codemenu = "IHM_PACK">
			<cfset obj2.ordremenu = 2>
			<cfset obj2.idparent = "">
			<cfset arr[2] = obj2>
			
			<cfset obj3= StructNew()>
			<cfset obj3.libelle = "les utilisateurs">
			<cfset obj3.codemenu = "IHM_UTILISATEUR">
			<cfset obj3.ordremenu = 3>
			<cfset obj3.idparent = "">
			<cfset arr[3] = obj3>
			
			<cfset obj4= StructNew()>
			<cfset obj4.libelle = "les commandes">
			<cfset obj4.codemenu = "IHM_GEST_CMDE">
			<cfset obj4.ordremenu = 4>
			<cfset obj4.idparent = "">
			<cfset arr[4] = obj4>
			
			<cfset obj41= StructNew()>
			<cfset obj41.libelle = "votre organisation">
			<cfset obj41.codemenu = "">
			<cfset obj41.ordremenu = 5>
			<cfset obj41.idparent = "">
			<cfset arr[5] = obj41>
			
			<cfset obj411= StructNew()>
			<cfset obj411.libelle = "en pratique">
			<cfset obj411.codemenu = "">
			<cfset obj411.ordremenu = 6>
			<cfset obj411.idparent = "">
			<cfset arr[6] = obj411>
			
			<cfset obj5= StructNew()>
			<cfset obj5.libelle = "suivi interne">
			<cfset obj5.codemenu = "">
			<cfset obj5.ordremenu = 7>
			<cfset obj5.idparent = "">
			<cfset arr[7] = obj5>
			
			<cfset obj6= StructNew()>
			<cfset obj6.libelle = "gestion demande">
			<cfset obj6.codemenu = "IHM_DEMANDE">
			<cfset obj6.ordremenu = 1>
			<cfset obj6.idparent = "7">
			<cfset arr[8] = obj6>
			
			<cfset obj61= StructNew()>
			<cfset obj61.libelle = "gestion anomalie">
			<cfset obj61.codemenu = "IHM_ANOMALIE">
			<cfset obj61.ordremenu = 2>
			<cfset obj61.idparent = "7">
			<cfset arr[9] = obj61>
			
			<cfset obj62= StructNew()>
			<cfset obj62.libelle = "nous contacter">
			<cfset obj62.codemenu = "IHM_INTERLOCUTEURS">
			<cfset obj62.ordremenu = 1>
			<cfset obj62.idparent = "6">
			<cfset arr[10] = obj62>
			
			<cfset obj63= StructNew()>
			<cfset obj63.libelle = "aide/support">
			<cfset obj63.codemenu = "IHM_AIDE">
			<cfset obj63.ordremenu = 2>
			<cfset obj63.idparent = "6">
			<cfset arr[11] = obj63>
			
			<cfset obj65= StructNew()>
			<cfset obj65.libelle = "mentions légales">
			<cfset obj65.codemenu = "IHM_MLEGALES">
			<cfset obj65.ordremenu = 4>
			<cfset obj65.idparent = "6">
			<cfset arr[12] = obj65>
			
			<cfset obj66= StructNew()>
			<cfset obj66.libelle = "conditions contractuelles">
			<cfset obj66.codemenu = "IHM_CDCRT">
			<cfset obj66.ordremenu = 5>
			<cfset obj66.idparent = "6">
			<cfset arr[13] = obj66>
			
			<cfset obj67= StructNew()>
			<cfset obj67.libelle = "vos sites">
			<cfset obj67.codemenu = "IHM_GEST_SITE">
			<cfset obj67.ordremenu = 1>
			<cfset obj67.idparent = "5">
			<cfset arr[14] = obj67>
			
			<cfset obj68= StructNew()>
			<cfset obj68.libelle = "vos comptes">
			<cfset obj68.codemenu = "IHM_COMPTES">
			<cfset obj68.ordremenu = 2>
			<cfset obj68.idparent = "5">
			<cfset arr[15] = obj68>
			
			<cfset obj69= StructNew()>
			<cfset obj69.libelle = "vos références">
			<cfset obj69.codemenu = "IHM_CONTRAT">
			<cfset obj69.ordremenu = 3>
			<cfset obj69.idparent = "5">
			<cfset arr[16] = obj69> 
		<!--- <cfelse>
			<cfset obj62= StructNew()>
			<cfset obj62.libelle = "nous contacter">
			<cfset obj62.codemenu = "IHM_INTERLOCUTEURS">
			<cfset obj62.ordremenu = 1>
			<cfset obj62.idparent = "">
			<cfset arr[1] = obj62>
		</cfif> --->
		 
		<cfreturn arr>		
	</cffunction>
</cfcomponent>