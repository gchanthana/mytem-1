<cfcomponent output="false">
<cffunction name="listePack" access="remote" returntype="query" description="">
	<cfargument name="listeidSite" type="String" required="true">
	<cfargument name="idSociete" type="numeric" required="true">
	<cfargument name="avUtilisateur" type="numeric" required="true">
	<cfargument name="listetypePack" type="String" required="true">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_parc.SearchPack">
		<cfif listeidSite neq "-1">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeidSite#" null="false">
		<cfelse>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeidSite#" null="true">
		</cfif>
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
		
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#avUtilisateur#" null="false">
		<cfif listetypePack neq "-1">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listetypePack#" null="false">
		<cfelse>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listetypePack#" null="true">
		</cfif>
	
	<cfprocresult name="p_result">
	</cfstoredproc>
<cfreturn p_result>
</cffunction>
<cffunction name="listePackDispo" access="remote" returntype="query" description="">
	<cfargument name="idSociete" type="numeric" required="true">
	<cfargument name="idSite" type="numeric" required="true">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_UTILISATEUR.listePackDispo">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idSite#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
<cfreturn p_result>
</cffunction>
<cffunction name="attribuerPack" access="remote" returntype="numeric" description="">	
	<cfargument name="idPack" type="numeric" required="true">
	<cfargument name="idUtilisateur" type="numeric" required="true">
	<cfargument name="idihmUser" type="numeric" required="true">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_parc.attribuerPack">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUtilisateur#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPack#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idihmUser#" null="false">
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="restituerPack" access="remote" returntype="numeric" description="">
	<cfargument name="idPack" type="numeric" required="true">
	<cfargument name="idihmUser" type="numeric" required="true">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="Pkg_mbp_global.restituerPack">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPack#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idihmUser#" null="false">
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="restituerXpack" access="remote" returntype="Numeric">
		<cfargument name="arr" type="Array">
		<cfargument name="idihmUser" type="numeric">
		<cfset tmp = 0>
		<cfloop index="i" from="1" to=#ArrayLen(arr)#>	
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_parc.restituerPack">
				<cfprocparam value="#val(arr[i].PACK_ID)#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam value="#idihmUser#" cfsqltype="CF_SQL_INTEGER" type="in"> 
				<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">		
			</cfstoredproc>
			<cfif p_result eq -1>
				<cfset tmp=0>
			<cfelse>
				<cfset tmp=1>	
			</cfif>
		</cfloop>
		<cfreturn tmp>
</cffunction>
<cffunction name="attribuerEquipement" access="remote" returntype="numeric" description="">
	<cfargument name="idPack" type="numeric" required="true">
	<cfargument name="idEquipement" type="numeric" required="true">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="Pkg_mbp_global.attribuerEquipement">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPack#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#" null="false">
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="retituerEquipement" access="remote" returntype="numeric" description="">
	<cfargument name="idEquipement" type="numeric" required="true">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="Pkg_mbp_global.retituerEquipement">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#" null="false">
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="updateIMEI" access="remote" returntype="numeric" description="">
	<cfargument name="idEquipement" type="numeric" required="true">
	<cfargument name="IMEI" type="STRING" required="true">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="Pkg_mbp_global.updateIMEI">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#IMEI#" null="false">
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="resilierPack" access="remote" returntype="array" description="">
	<cfargument name="arr" type="array" required="true">
	<cfargument name="ihm_user" type="numeric" required="true">

	<cfset tab = ArrayNew(1)>
	<cfloop index="i" from="1" to=#ArrayLen(arr)#>	
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="Pkg_commande.prResilierPack">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#val(arr[i].PACK_ID)#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[i].PORTABILITE#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#ihm_user#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[i].PACK_CODE#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
			<cfprocparam type="out" cfsqltype="CF_SQL_vARCHAR" variable="num_cmd" >
		</cfstoredproc>
		<cfset obj= StructNew()>
		<cfset obj.retour = p_retour>
		<cfset obj.num_cmd = num_cmd>
		<cfset obj.packid = val(arr[i].PACK_ID)>
		<cfset obj.portabilite = arr[i].PORTABILITE>
		<cfset obj.pack_code = arr[i].PACK_CODE>
		<cfset obj.ihm_user = ihm_user>
		<cfset tab[i] = obj>
	</cfloop>
	<cfreturn tab>
</cffunction>
<cffunction name="pack_list_Options" access="remote" returntype="query">
 	<cfargument name="packID" type="Numeric">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_parc.pack_list_Options">
		<cfprocparam value="#packID#" cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocresult name="p_result">
	</cfstoredproc> 
	<cfreturn p_result>
</cffunction>
<cffunction name="pack_list_equipement" access="remote" returntype="query">
 	<cfargument name="packID" type="Numeric">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_parc.pack_list_equipement">
		<cfprocparam value="#packID#" cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocresult name="p_result">
	</cfstoredproc> 
	<cfreturn p_result>
</cffunction>
<cffunction name="pack_infos" access="remote" returntype="query">
 	<cfargument name="packID" type="Numeric">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_parc.pack_info">
		<cfprocparam value="#packID#" cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocresult name="p_result">
	</cfstoredproc> 
	<cfreturn p_result>
</cffunction>
<cffunction name="modifcompatibiliteOption" access="remote" returntype="Struct">	
	<cfargument name="packcode" type="String" required="true">
	<cfargument name="list" type="string">
	<cfargument name="option" type="Numeric">
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.compatibilite_option">
		<cfprocparam value="#packcode#" cfsqltype="CF_SQL_VARCHAR" type="in">
		<cfprocparam value="#list#" cfsqltype="CF_SQL_VARCHAR" type="in">
		<cfprocparam value="#option#" cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocparam variable="code" cfsqltype="CF_SQL_INTEGER" type="out">
		<cfprocresult name="p_result">
	</cfstoredproc>
	
	<cfset obj= StructNew()>
	<cfset obj.CODE = code>
	<cfset obj.LISTE= p_result>		
	<cfreturn obj>

</cffunction>	
<cffunction name="getOfferCode" access="remote" returntype="Query">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_parc.getOfferCode">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
<cffunction name="demenagerPack" access="remote" returntype="struct">
	<cfargument name="idpack" type="numeric">
	<cfargument name="siteid" type="numeric">
	<cfargument name="nds" type="string">
	<cfargument name="ihmuser" type="numeric">
	<cfargument name="codepack" type="string">
	
	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_commande.prMovePack">
		<cfprocparam value="#idpack#" 	cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocparam value="#siteid#" 	cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocparam value="#nds#" 		cfsqltype="CF_SQL_varCHAR" type="in">
		<cfprocparam value="#ihmuser#" 	cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocparam value="#codepack#" cfsqltype="CF_SQL_varCHAR" type="in">
		
		<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
		<cfprocparam variable="num_cmd" cfsqltype="CF_SQL_varCHAR" type="out">
	</cfstoredproc>
	<cfset obj=StructNew()>
	<cfset obj.RETOUR = p_result>
	<cfset obj.NUM_CMD = num_cmd>
	
	<cfreturn obj>
</cffunction>

</cfcomponent>