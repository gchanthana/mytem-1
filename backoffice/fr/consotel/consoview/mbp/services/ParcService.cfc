<cfcomponent name="parcService">
	<cffunction name="getInfosBlocParc" access="remote" returntype="query" description="">
		<cfargument name="idSociete" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="Pkg_mbp_global.getInfosBlockParc">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	 <cffunction name="getGraphRepartitionPacks" access="remote" returntype="query" description="">
		<cfargument name="idSociete" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="Pkg_mbp_global.getGraphRepartitionPacks">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
<!---JEU DE TEST--->  
	<cffunction name="getInfosBlocParcTEST" access="remote" returntype="query">
			<cfargument name="idActeur" type="numeric" required="true">
			<cfset var myQuery = QueryNew("NBPACKSATTRIBUE,NBPACKSNONATTRIBUE","Integer,Integer")>
			<cfscript> 
				QueryAddRow(myQuery);
				QuerySetCell(myQuery,"NBPACKSATTRIBUE",6);            
				QuerySetCell(myQuery,"NBPACKSNONATTRIBUE",4); 
			</cfscript>
	</cffunction> 
</cfcomponent>