<!---
Package : fr.consotel.consoview.util
--->
<cfcomponent name="utils">
	<cffunction name="getIdPerimetreByIndex" acess="remote" returntype="numeric">
		<cfargument name="perimetreIndex" type="numeric" required="true">
		<!---
		Pour l'instant car ID_PERIMETRE est mis à jour à dans SESSION chaque fois que l'on
		change de périmètre.
		--->
		<cfreturn SESSION.PERIMETRE.ID_PERIMETRE>
		<!---
		OU SINON : Problème si un jour on remplace la QUERY LISTE_PERIMETRES_QUERY par
		un tableau associatif ou une structure associant l'index à l'IDGROUPE_CLIENT pour
		gagner plus de mémoire par rapport à la QUERY.
		<cfreturn perimetreIndex>
		--->
	</cffunction>
	
	
	<cffunction name="getExportOrgaCSV" access="public" returntype="query" output="false" displayname="Ramene les noeuds en CSV d'un perimetre" >		
		<cfargument name="ID_perimetre" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLOBAL.getExportOrgaCSV">			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_id_groupe" value="#ID_perimetre#"/>									        	
			<cfprocresult name="p_result" />        
		</cfstoredproc>
		<cfreturn p_result/>
	</cffunction>
	
	<!--- Exportation de la liste des organisations au format csv ( en fonction du mode Actif/inactif/all) --->
	<cffunction name="getExportOrgaCSVbyMode" access="public" returntype="query" output="false" displayname="Ramene les noeuds en CSV d'un perimetre" >		
		<cfargument name="ID_perimetre" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="mode" required="false"  type="numeric"    displayname="mode" hint="Initial value for the mode" />
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLOBAL.getExportOrgaCSV_v3">			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"   type="in" variable="p_id_groupe" value="#ID_perimetre#"/>									        	
			<cfprocparam cfsqltype="CF_SQL_INTEGER" null="#iif((mode eq -1), de("yes"), de("no"))#" type="in" variable="p_mode" value="#mode#"/>									        	
			<cfprocresult name="p_result" />        
		</cfstoredproc>
		<cfreturn p_result/>
	</cffunction>
	
	<cffunction name="getExportOrgaStCSV" access="public" returntype="query" output="false" displayname="Ramene les noeuds en CSV d'un perimetre" >		
		<cfargument name="ID_perimetre" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLOBAL.getExportOrgaCSV3_v4">			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_id_groupe" value="#ID_perimetre#"/>									        	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_mode" value="1"/>									        	
			<cfprocresult name="p_result" />        
		</cfstoredproc>
		<cfreturn p_result/>
	</cffunction>
	
	<cffunction name="transposeQuery" acess="remote" returntype="query" output="false">
		<cfargument name="dataset" type="query">
		<cfargument name="champFixe" type="string">
		<cfargument name="champCle" type="string">
		<cfargument name="champValeur" type="string">
		<cfargument name="stAbos" type="struct">
		<!--- Création de la structure avec colonnes variables --->
		<cfquery dbtype="query" name="qGetcfixe">
			select distinct #champFixe#
			from dataset
		</cfquery>
		<cfquery dbtype="query" name="qGetcle">
			select distinct #champCle#
			from dataset
		</cfquery>
		<cfset stSource=structNew()>
		<cfloop query="dataset">
			<cfset champ=Evaluate("dataset." & champFixe)>
			<cfset cle=Evaluate("dataset." & champCle)>
			<cfset valeur=Evaluate("dataset." & champValeur)>
			<cfset stTemp=structNew()>
			<cfloop query="qGetcle">
				<cfset structInsert(stTemp,Evaluate("qGetcle." & #champCle#),0)>
			</cfloop>
			<!--- Initialise la structure --->
			<cfif Not StructKeyExists(stSource,champ)>
				<cfset structInsert(stSource,champ,stTemp)>
			</cfif>
			<!--- Remplie la structure --->
			<cfif type_theme neq "Abonnements">
				<cfset stSource[champ][cle]=stSource[champ][cle]+valeur>
			</cfif>
		</cfloop>
		<!--- Création de la structure avec colonnes fixes --->
		<cfset stFixe=structNew()>
		<cfset t=1>
		<cfloop query="dataset">
			<cfset champ=Evaluate("dataset." & champFixe)>
			<cfset stTemp=structNew()>
			<cfloop from="1" to="#ListLen(dataset.columnlist)#" index="i">
				<cfset listeColonne=ListGetAt(dataset.columnlist,i)>
				<cfif listeColonne neq champFixe AND listeColonne neq champCle AND listeColonne neq champValeur>
					<cfset stTemp['#ListGetAt(dataset.columnlist,i)#']=dataset[ListGetAt(dataset.columnlist,i)][t]>
				</cfif>
			</cfloop>
			<!--- Initialise la structure --->
			<cfif Not StructKeyExists(stFixe,champ)>
				<cfset structInsert(stFixe,champ,stTemp)>
			</cfif>
			<cfset t=t+1>
		</cfloop>
		<!--- Création de la liste des champs --->
		<cfset listeFinale="">
		<cfloop from="1" to="#ListLen(dataset.columnlist)#" index="i">
			<cfset listeColonne=ListGetAt(dataset.columnlist,i)>
			<cfif listeColonne neq champCle AND listeColonne neq champValeur>
				<cfset listeFinale=ListAppend(listeFinale,listeColonne)>
			</cfif>
		</cfloop>
		<cfloop query="qGetcle">
			<cfset listeFinale=ListAppend(listeFinale,Evaluate("qGetcle." & #champCle#))>
		</cfloop>
		<cfset qFinale=queryNew(replace(listeFinale," ","_","all"))>
		<cfloop collection="#stFixe#" item="coll">
			<cfset queryAddRow(qFinale)>
			<cfset querySetCell(qFinale,champFixe,coll)>
			<cfloop collection="#stFixe[coll]#" item="i">
				<cfif i neq "montant_abo_total">
					<cfset querySetCell(qFinale,i,stFixe[coll][i])>
				<cfelse>
					<cfif StructKeyExists(stAbos,stFixe[coll]['idsous_tete'])>
						<cfset querySetCell(qFinale,i,stAbos[stFixe[coll]['idsous_tete']])>
					<cfelse>
						<cfset querySetCell(qFinale,i,0)>
					</cfif>
				</cfif>
			</cfloop>
			<br>
			<cfloop collection="#stSource[coll]#" item="i">
				<cfset querySetCell(qFinale,replace(i," ","_","all"),stSource[coll][i])>
			</cfloop>
		</cfloop>
		<cfreturn qFinale>
	</cffunction>

	<cffunction name="createReport" acess="remote" returntype="void" output="true">
		<cfargument name="argurl" type="struct">
		<cfargument name="argform" type="struct">
		<cfargument name="chemin" type="String" default="">

		<!--- boucle sur les parametres appelant la page d'origine --->
		<cfset chaine="">
		<cfif Not StructIsEmpty(url)>
			<cfloop collection="#url#" item="i">
				<cfset chaine=chaine & i & "=" & #url[i]# & "&">
			</cfloop>
		</cfif>
		<cfif Not StructIsEmpty(form)>
			<cfloop collection="#form#" item="i">
				<cfif i neq "fieldnames">
					<cfset chaine=chaine & i & "=" & form[i] & "&">
				</cfif>
			</cfloop>
		</cfif>
		<cfset chaine=left(chaine,len(chaine)-1)>
		<!--- Affichage de la barre qui appelle le rapport --->		
		<cfoutput>
		<script>
		function ouvre(a) {
			window.open("#chemin#?format="+a+"&#chaine#",'report','menubar=no,status=no,location=no,scrollbars=1,directories=no,copyhistory=no,resizable=yes');	
		}
		</script>
		</cfoutput>
		<a href="##" onclick="ouvre('flashpaper');"><font size="1">flashpaper</font></a><br>
		<a href="##" onclick="ouvre('pdf');"><font size="1">pdf</a><br>
		<a href="##" onclick="ouvre('excel');"><font size="1">Excel</a><br>
	</cffunction>
	
	<cffunction name="getQuery" acess="remote" returntype="query" output="true">
		<cfargument name="arg" type="string">
		<cfreturn evaluate(arg)> 
	</cffunction>
	 
	<cffunction name="getString" acess="remote" returntype="string" output="true">
		<cfargument name="arg" type="string">
		<cfreturn evaluate(arg)> 
	</cffunction>
	
	 
	<!--- Calcule la repartition d'une durée en secondes (hh:mm:ss) --->
	<cffunction name="getDurationRepartition" acess="remote" returntype="string" output="true">
		<cfargument name="total" type="numeric" required="true" hint="Durée en secondes">
		<cfset tmpTotal = total>
		<cfset tmpHours = 0>
		<cfset tmpMinutes = 0>
		<cfset tmpSeconds = 0>
		
		<cfif tmpTotal neq 0>
			<cfset tmpHours = Int(total / 60)>
			<cfif tmpHours gt 0>
				<cfset tmpTotal = tmpTotal - (tmpHours * 60)>
			</cfif>
		</cfif>
		
		<cfif tmpTotal neq 0>
			<cfset tmpMinutes = Int(tmpTotal)>
			<cfif tmpMinutes gt 0>
				<cfset tmpTotal = tmpTotal - (tmpMinutes)>
			</cfif>
		</cfif>
		<cfset tmpSeconds = tmpTotal>
		<cfreturn "#tmpHours#:#tmpMinutes#:#tmpSeconds#">
	</cffunction>

	<!--- Pour mettre en session une requête --->
	<cffunction name="setQueryInSession" output="false" access="remote" returntype="void" description="Pour mettre en session le resultat de la recherche de la facture">
		<cfargument name="name" type="string">
		<cfargument name="q" type="query">
		<cfset var="session." & name>
		<cfif IsDefined('#var#')>
			<cfset structDelete(session,name)>
		</cfif>
		<cfset structInsert(session,name,q)>
	</cffunction>
	
	<cffunction name="getMxTreeCommunesDataFromListeSites" acess="remote" returntype="any" output="false">
		<cfargument name="TYPE_PERIMETRE" type="string" required="true">
		<cfargument name="ID_PERIMETRE" type="numeric" required="true">
		<cfargument name="libelle_racine" type="string" required="true">
		<cfset sitesProvider =
					createObject("component","com.consoview.sites.#TYPE_PERIMETRE#ListeSitesProviderStrategy")>
		<cfset listeSites = sitesProvider.getSites(ID_PERIMETRE)>
		<cfxml variable="mxTreeDataProvider">
			<cfoutput query="listeSites" group="IDGROUPE_CLIENT">
				<cfset groupe = IDGROUPE_CLIENT>
				<node label="#libelle_racine#" parent="0" value="#IDGROUPE_CLIENT#" type="_GROUPE_">
				<cfoutput group="IDREF_CLIENT">
					<cfset currentSociete = IDREF_CLIENT>
					<node label="#LIBELLE#" parent="#groupe#" value="#IDREF_CLIENT#" type="_SOCIETE_">
					<cfoutput group="ZIP">
						<cfset currentDep = ZIP>
						<node label="#DEP#" parent="#currentSociete#" value="#ZIP#" type="_ZIP_">
						<cfoutput group="COMMUNE">
							<node label="#COMMUNE#" parent="#currentDep#" value="#COMMUNE#" type="_COMMUNE_"/>
						</cfoutput>
						</node>
					</cfoutput>
					</node>
				</cfoutput>
				</node>
			</cfoutput>
		</cfxml>
		<cfreturn mxTreeDataProvider>
	</cffunction>

<!---*************** FONCTIONS EN TEST ***********************--->
	<!--- Pour tester l'heritage en Coldfusion avec le remoting flash --->
	<cffunction name="invokeCfc" acess="remote" returntype="any" output="false">
		<cfargument name="cfcSource" type="string" required="true">
		<cfargument name="cfcMethod" type="string" required="true">
		<cfargument name="cfcParams" type="array" required="true">
		<cfset params = StructNew()>
		<cfset paramList = "">
		<cfobject component="#cfcSource#" name="instanceObj">
		<cfif StructKeyExists(instanceObj,cfcMethod)>
			<cfset paramArray = instanceObj['#cfcMethod#']['METADATA']['PARAMETERS']>
		</cfif>
		<cfloop from="1" to="#ArrayLen(cfcParams)#" index="i">
			<cfset params['##'] = "">
		</cfloop>
		<cfreturn paramArray>
		<!---
		<cfset args = StructNew()>
		<cfset args.id = "603">
		<cfinvoke
		   component="#cfcName#"
		   method="#cfcMethod#" 
		   argumentCollection="#args#"
		   returnVariable="result">
		<cfreturn result>
		   --->
	</cffunction>
<!---**********************************************************--->

<cffunction name="testXML" returntype="string" access="remote" output="true">
	<cfargument name="idgroupe_client" type="numeric" required="yes">
	<cfquery datasource="#session.offreDSN#" name="qGetData">
		SELECT gc.libelle_groupe_client,gc.idgroupe_client, nvl(gc.id_groupe_maitre,idgroupe_client) AS id_groupe_maitre,LEVEL AS niveau, 
		nvl((SELECT libelle FROM decoupage d WHERE d.iddecoupage=gc.iddecoupage),decode(LEVEL,1,'','Groupe')) "Decoupage", 
		nvl((SELECT 'Decoupage' FROM decoupage d WHERE d.iddecoupage=gc.iddecoupage),decode(LEVEL,1,'','Groupe')) "type", 
		 nvl(gc.iddecoupage,decode(LEVEL,1,NULL,0)) as iddecoupage
		FROM groupe_client gc
		START WITH gc.id_groupe_maitre IS NULL AND gc.idgroupe_client=#idgroupe_client#
		CONNECT BY PRIOR gc.idgroupe_client=gc.id_groupe_maitre
		ORDER BY LEVEL
	</cfquery>
	<cfreturn BuildTreeXML(qGetData,idgroupe_client)>
</cffunction>

<!--- Build Flex Tree XML from a query --->
<cffunction name="BuildTreeXML" returntype="xml" access="public" output="true">
   <cfargument name="data" type="query" required="yes">
	<cfargument name="idgroupe_client" type="numeric" required="yes">
   <!--- Local vars --->
   <cfset var xmlTree="">
	<cfquery dbtype="query" name="qGetData">
		select max(niveau) as lemax
		from data
	</cfquery>
	
	<cfset maxlevel=qGetData.lemax>
	<cfset ProcessTreeXML(data,1,idgroupe_client,maxlevel)>
		
  <!--- And return it --->
   <cfreturn xmlTree />

</cffunction>

<cffunction name="ProcessTreeXML" returntype="query" access="public" output="true">
	<cfargument name="data" type="query" required="yes">
	<cfargument name="niveau_abs" type="numeric" required="yes">
	<cfargument name="id" type="numeric" required="yes">
	<cfargument name="max_niveau" type="numeric" required="yes">
	<cfquery dbtype="query" name="qGetData">
		select *
		from data
		where niveau=#niveau_abs#
		and id_groupe_maitre=#id#
	</cfquery>
	<cfif niveau_abs lte max_niveau and qGetData.recordcount gt 0>
		<cfloop query="qGetData">
			<node label="#libelle_groupe_client#" data="#idgroupe_client#">
			<cfset ProcessTreeXML(data,niveau+1,idgroupe_client,max_niveau)>
			</node>
		</cfloop>
	</cfif>
	<cfreturn qGetData>
</cffunction>

	<!--- Internal query recursion function --->
<cffunction name="BuildTreeXMLProcess" returntype="string" access="remote" output="false">
   <cfargument name="data" type="query" required="yes">
   <cfargument name="cols" type="string" required="yes">
   <cfargument name="where" type="string" required="no" default="0=0">

   <!--- Local vars --->
   <cfset var result="">
   <cfset var distinctData="">
   <cfset var subData="">
   <cfset var whereClause="">

   <!--- Check if have any more columns in this branch --->
   <cfif ListLen(ARGUMENTS.cols)>

      <!--- Get distinct values for this column --->
      <cfquery dbtype="query" name="distinctData">
      SELECT DISTINCT #ListFirst(ARGUMENTS.cols)# AS col
      FROM ARGUMENTS.data
      WHERE #PreserveSingleQuotes(ARGUMENTS.where)#
      </cfquery>
   
      <!--- Loop through distinct data --->
      <cfloop query="distinctData">
         <cfset whereClause = ARGUMENTS.where & " AND " & ListFirst(ARGUMENTS.cols) & " = '" & col & "'">
         <cfquery dbtype="query" name="subData">
         SELECT *
         FROM ARGUMENTS.data
         WHERE #PreserveSingleQuotes(whereClause)#
         </cfquery>
         <!--- Any more columns in this branch? --->
         <cfif ListLen(ListRest(ARGUMENTS.cols))>
            <!--- Yes, create a node and recurse --->
            <cfset result=result & "<node label=""#col#"">">
            <cfset result=result & BuildTreeXMLProcess(subData, ListRest(ARGUMENTS.cols), whereClause)>
            <cfset result=result & "</node>">
         <cfelse>
            <!--- No, create node and populate with all columns --->
            <cfset result=result & "<node label=""#col#""">
            <cfset result=result & BuildTreeXMLProcess(subData, ListRest(ARGUMENTS.cols), whereClause)>
            <cfset result=result & "/>">
         </cfif>
      </cfloop>

   <cfelse>

      <!--- Bottom of this branch --->
      <cfquery dbtype="query" name="subData">
      SELECT *
      FROM ARGUMENTS.data
      WHERE #PreserveSingleQuotes(ARGUMENTS.where)#
      </cfquery>
      <!--- Write all columns as name="value" pairs --->
      <cfloop list="#subData.ColumnList#" index="column">
         <cfset result = result & " #LCase(column)#=""#subData[column][1]#"" ">
      </cfloop>

   </cfif>

   <!--- And return it --->
   <cfreturn result>
</cffunction>




</cfcomponent>
