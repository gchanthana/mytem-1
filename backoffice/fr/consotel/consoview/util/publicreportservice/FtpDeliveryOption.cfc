<cfcomponent name="FtpDeliveryOption" displayname="FtpDeliveryOption">
	<cfset variables.FtpDeliveryOption=structNew()>
	
	<cffunction name="setFtpDeliveryOption" access="package" returntype="void">
		<cfargument name="ftpServerName" type="String" required="true">
		<cfargument name="ftpUserName" type="String" required="true">
		<cfargument name="ftpUserPassword" type="String" required="true">
		<cfargument name="remoteFile" type="String" required="true">
		<cfargument name="sftpOption" type="boolean" required="false" default="false">

		<cfset variables.FtpDeliveryOption.ftpServerName=ftpServerName>
		<cfset variables.FtpDeliveryOption.ftpUserName=ftpUserName>
		<cfset variables.FtpDeliveryOption.ftpUserPassword=ftpUserPassword>
		<cfset variables.FtpDeliveryOption.remoteFile=remoteFile>
		<!--- <cfset variables.FtpDeliveryOption.sftpOption=sftpOption> --->
	</cffunction>
	
	<cffunction name="getFtpDeliveryOption" access="package" returntype="Struct">
		<cfreturn variables.FtpDeliveryOption>
	</cffunction>
</cfcomponent>