<cfcomponent name="ReportRequest" displayname="ReportRequest">
	<cfset variables.RunReportRequest=structNew()>
	<cfset variables.RunReportRequest.userID="consoview">
	<cfset variables.RunReportRequest.password="public">
	<cfobject name="reportRequest" type="component" component="fr.consotel.consoview.util.publicreportservice.ReportRequest">
	
	<cffunction name="setRunReportRequest" access="public" returntype="void">
		<cfargument name="userID" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfargument name="reportAbsolutePath" type="String" required="true">
		<cfargument name="attributeTemplate" type="String" required="true">
		<cfargument name="attributeFormat" type="String" required="true">
		<cfargument name="attributeLocale" type="String" required="false" default="fr-FR">
		
		<cfset variables.RunReportRequest.userID=userID>
		<cfset variables.RunReportRequest.password=password>
		<cfset reportRequest.setReportRequest(reportAbsolutePath,attributeTemplate,attributeFormat,attributeLocale)>
		<cfset structInsert(variables.RunReportRequest,"reportRequest",reportRequest.getReportRequest(),true)>
	</cffunction>
	
	<cffunction name="AddParameter" access="remote" returntype="void">
		<cfargument name="paramName" type="string" required="true">
		<cfargument name="paramValues" type="string" required="true">
		<cfset reportRequest.AddParameter(paramName,paramValues)>
		<cfset structInsert(variables.RunReportRequest,"reportRequest",reportRequest.getReportRequest(),true)>
	</cffunction>
	
	<cffunction name="getRunReportRequest" access="public" returntype="struct">
		<cfreturn variables.RunReportRequest>
	</cffunction>
</cfcomponent>
	