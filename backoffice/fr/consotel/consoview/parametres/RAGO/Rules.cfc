<cfcomponent output="false" alias="fr.consotel.consoview.parametres.RAGO.Rules">

	<cfparam name="idracine" default="#SESSION.PERIMETRE.ID_GROUPE#"  type="Numeric">
	<cfparam name="iduser" default="#SESSION.user.CLIENTACCESSID#"  type="Numeric"> 
 	<cfparam name="Nameuser" default="#SESSION.user.NOM#"  type="string"> 
	<cfparam name="Surnameuser" default="#SESSION.user.PRENOM#"  type="string">  
	
<!--- Active/Désactive une règle // etat : 0->inactive, 1->active, retour : 1->ok, -1->échec --->
<cffunction name="activeRules" output="false" access="public" returntype="Numeric">
	<cfargument name="idregleOrga" type="Numeric" required="true">
	<cfargument name="regleNom" type="string" required="true">
	<cfargument name="typeRegle" type="string" required="true">
	<cfargument name="regleComment" type="string" required="true">
	<cfargument name="idSource" type="Numeric" required="true">
	<cfargument name="idCible" type="Numeric" required="true">
	<cfargument name="activeOrNot" type="Numeric" required="true">
	<cfargument name="suivreAuto" type="Numeric" required="true">
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.iu_regle_orga"> 
			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idregle_orga" value="#idregleOrga#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_regle_nom" value="#regleNom#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_type_regle" value="#typeRegle#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_regle_comment" value="#regleComment#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsource" value="#idSource#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcible" value="#idCible#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_bool_active" value="#activeOrNot#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" value="#iduser#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_bool_suivre_auto" value="#suivreAuto#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			
		</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<!--- Exécution ou non d'une règle // retour : nbr de lignes impactées->ok, -1->échec  --->
<cffunction name="executeRules" output="false" access="public" returntype="numeric">
	<cfargument name="idregle" type="Numeric" required="true">
<!--- 	<cfargument name="iduser" type="Numeric" required="true"> --->
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.EXECUTER_REGLE"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idregle_orga" value="#idregle#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" value="#iduser#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<!--- Supprime une règle // retour : 1->ok, -1->échec --->
<cffunction name="eraseRules" output="false" access="public" returntype="Numeric">
	<cfargument name="idregle" type="Numeric" required="true">
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.DEL_REGLE_ORGA"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idregle_orga" value="#idregle#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" value="#iduser#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<!--- Liste des règles ayant pour source un nœud de l’organisation …annuaire ou opérateurs
 spécifiés et aussi liste des règles ayant pour cible une feuille de l’organisation cliente 
spécifiée // retour : RefCursor --->
<cffunction name="getReglesOrga" output="false" access="public" returntype="Query">
	<cfargument name="idsource" type="Numeric" required="true">
	<cfargument name="idcible" type="Numeric" required="true">
	<cfargument name="clef" type="String" required="true">
	
	<cfif idsource EQ 0>
		<cfif idcible EQ 0>
			<cfif clef EQ "">
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_REGLE_ORGA"> 
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsource" null="true">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcible" null="true">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_regle_nom" null="true">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			</cfif>
		<cfelse>
	
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_REGLE_ORGA"> 
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsource" value="#idsource#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcible" value="#idcible#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_regle_nom" value="#clef#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
	
		</cfif>
	<cfelse>
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_REGLE_ORGA"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsource" value="#idsource#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcible" value="#idcible#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_regle_nom" value="#clef#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
	
	</cfif>
	<cfreturn p_retour>
</cffunction>



<cffunction name="getReglesOrga_v2" output="false" access="public" returntype="Query">
	<cfargument name="idsource" type="Numeric" required="true">
	<cfargument name="idcible" type="Numeric" required="true">
	
	<cfif idsource EQ 0>
		<cfif idcible EQ 0>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_REGLE_NOEUD"> 
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsource" null="true">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcible" null="true">
				<cfprocresult name="p_retour">
			</cfstoredproc>
		<cfelse>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_REGLE_NOEUD"> 
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsource" value="#idsource#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcible" value="#idcible#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
		</cfif>
	<cfelse>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_REGLE_NOEUD"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsource" value="#idsource#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcible" value="#idcible#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
	</cfif>
	<cfreturn p_retour>
</cffunction>


<cffunction name="search_Line" output="false" access="public" returntype="Query">
	<cfargument name="idracine" 	type="Numeric" required="true">
	<cfargument name="idsoustete" 	type="Numeric" required="true">
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_search_v3.search_line"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_tete" 	value="#idsoustete#">
			<cfprocresult name="p_retour">
		</cfstoredproc>

	<cfreturn p_retour>
</cffunction>

<cffunction name="getCible" output="false" access="public" returntype="Query">
	<cfargument name="idracine" 	type="Numeric" required="true">
	<cfargument name="idsousrce" 	type="Numeric" required="true">
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.getCible"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsource" 	value="#idsousrce#">
			<cfprocresult name="p_retour">
		</cfstoredproc>

	<cfreturn p_retour>
</cffunction>
<!--- Obtention les règles d'une oraganisation --->
<!--- <cffunction name="getReglesOrga" output="false" access="public" returntype="Query">
	<cfargument name="rulesName" type="String" required="true">
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_REGLE_ORGA"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_regle_nom" value="#rulesName#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
	<cfreturn p_retour>
	
</cffunction> --->

 <!---Créer une règle, et l’exécuter si spécifié (BOOL_EXECUTE_NOW).
On enregistre la date de création et l’utilisateur qui a créé la règle.
SI IDREGLE_ORGA>0 est spécifié alors on met à jour la règle // retour : RefCursor --->
<cffunction name="getIURulesOrga" output="false" access="public" returntype="Numeric">
	<cfargument name="idregleorga" type="Numeric" required="true">
	<cfargument name="regleNom" type="String" required="true">
	<cfargument name="typeregle" type="String" required="true">
	<cfargument name="regleComment" type="String" required="true">
	<cfargument name="idsource" type="Numeric" required="true">
	<cfargument name="idcible" type="Numeric" required="true">
	<cfargument name="active" type="Numeric" required="true">
	<cfargument name="suivreAuto" type="Numeric" required="true">
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.IU_REGLE_ORGA"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idregle_orga" value="#idregleorga#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_regle_nom" value="#regleNom#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_type_regle" value="#typeregle#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_regle_comment" value="#regleComment#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsource" value="#idsource#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcible" value="#idcible#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_bool_active" value="#active#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" value="#iduser#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_bool_suivre_auto" value="#suivreAuto#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<!--- Insérer la liste des exceptions d’une règle  // retour : IDREGLE_ORGA->ok, -1->échec --->
<!--- 
<cffunction name="getIURulesOrgaException" output="false" access="public" returntype="Numeric">
	 --->
<cffunction name="getIURulesOrgaException" output="false" access="public" returntype="string">
	<cfargument name="idRegleOrga" type="Numeric" required="true">
	<cfargument name="nbTosend" type="Numeric" required="true">
	<cfargument name="validOrNot" type="Numeric" required="true">
	<cfargument name="aAjouter" type="Array" required="true">
	
	<cfset v_nbloop=ArrayLen(aAjouter)>
	
	<cfloop index="idx" from="1" to="#v_nbloop#">
		 	
		 	<cfset idSource = aAjouter[idx]>
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.IU_REGLE_EXCEPTION">			
					<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idregle_orga" value="#idRegleOrga#">
					<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idsource" value="#idSource#">
					<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" value="#iduser#">
					<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_ajout" value="#validOrNot#">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
				</cfstoredproc>

	</cfloop>

	 <cfreturn p_retour>
	 
</cffunction>

<!--- Liste de toutes les organisations clientes // retour : RefCursor --->
<cffunction name="getListOrgaCliente" output="false" access="public" returntype="Query">
	
	<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_ORGA_CUS"> 
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
		<cfprocresult name="p_retour">
	</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<cffunction name="getListOrgaCliente_V2" output="false" access="public" returntype="Query">
	<cfargument name="idGpeClient" type="Numeric" required="true">
	
	<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_ORGA_CUS_v2"> 
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idGpeClient" value="#idGpeClient#">
		<cfprocresult name="p_retour">
	</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<!--- Liste de toutes les organisations opérateurs et annuaires --->
<cffunction name="getListOrgaOpeAnu" output="false" access="public" returntype="Query">
	
	<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_ORGA_OPANU"> 
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
		<cfprocresult name="p_retour">
	</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<!--- Structure de l’organisation spécifiée --->
<cffunction name="getStructureOrga" output="false" access="public" returntype="Query">
	<cfargument name="idOrga" type="Numeric" required="true">
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.ARBRE_ORGA"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#idOrga#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<cffunction name="getUserID" output="false" access="public" returntype="numeric">

	<cfreturn iduser>
	
</cffunction>


<!--- Liste des execptions d'une règle --->
<cffunction name="getRegleException" output="false" access="public" returntype="Query">
	<cfargument name="idOrga" type="Numeric" required="true">
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_REGLE_EXCEPTION"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#idOrga#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<!--- Fonction temporaire --->
<cffunction name="getListePerimetresDataset" access="remote" returntype="xml">
	<cfargument name="idGroupe" type="numeric" required="true">
	<cfargument name="filterType" type="numeric" required="true">
	<cfargument name="date" type="string" required="true">
	        
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GLOBAL.Arbre_orga">
			<cfprocparam value="#idGroupe#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#filterType#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#date#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="sourceQuery">
		</cfstoredproc>
	
	<cfset dataStruct = structNew()>
			<cfset perimetreXmlDoc = XmlNew()>
			<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode = perimetreXmlDoc.node>
			<cfset dataStruct.ROOT_NODE = rootNode>
			<cfset rootNode.XmlAttributes.LABEL = sourceQuery['LIBELLE_GROUPE_CLIENT'][1]>

			<cfset rootNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][1]>
		
			<cfset rootNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][1]>
			<cfset rootNode.XmlAttributes.NIV = sourceQuery['NIV'][1]>

			<cfloop index="i" from="1" to="#sourceQuery.recordcount#">
				<cfset currentKey = sourceQuery['IDGROUPE_CLIENT'][i]>
				<cfset currentParentKey = sourceQuery['ID_GROUPE_MAITRE'][i]>
				<cfif sourceQuery['IDGROUPE_CLIENT'][i] EQ idGroupe>
					<cfset currentParentKey = "ROOT_NODE">
				</cfif>
				<cfset parentChildCount = arrayLen(dataStruct['#currentParentKey#'].XmlChildren)>
				<cfset dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset tmpChildNode = dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1]>
				<cfset tmpChildNode.XmlAttributes.LABEL = sourceQuery['LIBELLE_GROUPE_CLIENT'][i]>
				<cfset tmpChildNode.XmlAttributes.NIV = sourceQuery['NIV'][i]>
				<cfset tmpChildNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][i]>
				<cfset tmpChildNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][i]>
				<cfset structInsert(dataStruct,currentKey,tmpChildNode)>
			</cfloop>
			
			<cfreturn perimetreXmlDoc>

</cffunction>

<cffunction name="getOrganisation" returntype="query" access="remote">
	<cfargument name="id_groupe" required="true" type="numeric" />
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_orgabyacces">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#session.user.CLIENTACCESSID#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfquery  name="qresult" dbtype="query">
			select * from p_result Order by TYPE_ORGA asc
		</cfquery>
		
		<cfreturn qresult>
		
</cffunction>

<cffunction name="getAssignNode" returntype="xml" access="remote">
	<cfargument name="id_racine" required="true" type="numeric" />
	<cfargument name="id_orgaRef" required="true" type="string" />
	<cfargument name="id_orga" required="true" type="string" />
	<cfargument name="date" required="true" type="string" />
			
		<cfset realid=session.perimetre.ID_GROUPE/>
		<cfset sourceQuery = getListeAssignNodes(realid, id_orgaRef, id_orga, date)>
		<cfset result = formatXMLData(sourceQuery, id_orga)>
			
		<cfreturn result>
		
</cffunction>

<cffunction name="getNotAssignNode" returntype="xml" access="remote">
	<cfargument name="id_racine" required="true" type="numeric" />
	<cfargument name="id_orgaRef" required="true" type="string" />
	<cfargument name="id_orga" required="true" type="string" />
	<cfargument name="date" required="true" type="string" />
			
		<cfset realid=session.perimetre.ID_GROUPE/>
		<cfset sourceQuery = getListeNotAssignNodes(realid, id_orgaRef, id_orga, date)>
		<cfset result = formatXMLData(sourceQuery, id_orga)>
			
		<cfreturn result>
		
</cffunction>

<cffunction name="formatXMLData" returntype="xml" access="remote">
	<cfargument name="sourceQuery" type="query" required="true">
	<cfargument name="idGroupe" type="numeric" required="true">
			
		<cfset dataStruct = structNew()>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset dataStruct.ROOT_NODE = rootNode>			
			
			
		<cfif sourceQuery.recordcount gt 0>			
			
			<cfset rootNode.XmlAttributes.LABEL = TRIM(sourceQuery['LIBELLE_GROUPE_CLIENT'][1])>
			<cfset rootNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][1]>
			<cfset rootNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][1]>
			<cfset rootNode.XmlAttributes.COMMENTAIRES = sourceQuery['COMMENTAIRES'][1]>
			<cfset rootNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][1]>
	
			<cfloop index="i" from="1" to="#sourceQuery.recordcount#">
				
				<cfset currentKey = sourceQuery['IDGROUPE_CLIENT'][i]>
				<cfset currentParentKey = sourceQuery['ID_GROUPE_MAITRE'][i]>
				
				<cfif sourceQuery['IDGROUPE_CLIENT'][i] EQ idGroupe>
					
					<cfset currentParentKey = "ROOT_NODE">
				
				</cfif>
				
				<cfset parentChildCount = arrayLen(dataStruct['#currentParentKey#'].XmlChildren)>
				<cfset dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset tmpChildNode = dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1]>
				<cfset tmpChildNode.XmlAttributes.LABEL = TRIM(sourceQuery['LIBELLE_GROUPE_CLIENT'][i])>
				<cfset tmpChildNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][i]>
				<cfset tmpChildNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][i]>
				<cfset tmpChildNode.XmlAttributes.COMMENTAIRES = sourceQuery['COMMENTAIRES'][i]>
				<cfset structInsert(dataStruct,currentKey,tmpChildNode)>
			
			</cfloop>			
		
		</cfif>
			
			<cfreturn perimetreXmlDoc>
</cffunction>

<!--- Obtient le nom et le prenom de la personne concernée --->
<cffunction name="getNameAndSurname" output="false" access="public" returntype="string">
	
	<cfset resultConcat = Nameuser  & " " & Surnameuser>	
	<cfreturn resultConcat>
	
</cffunction> 


</cfcomponent>