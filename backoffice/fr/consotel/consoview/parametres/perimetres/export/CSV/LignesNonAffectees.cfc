<cfcomponent name="LignesNonAffectees" hint="export des lignes non affectées" output="false">
	<cffunction name="afficher"> 				
		<cfsetting enablecfoutputonly="true" />
		<cfset label="Lignes non affectées">		
		<cfset qGetGrid=createObject("component","fr.consotel.consoview.util.utils").getQuery("session.parametres.perimetres.ExportLignes")>				
		<cfset NewLine = Chr(13) & Chr(10)>		
		<cfheader name="Content-Disposition" value="inline;filename=rapport.csv" charset="iso-8859-1">
		<cfcontent type="text/csv">
		<cfoutput>Type de ligne;Ligne;Fonction ou collaborateur#NewLine#</cfoutput>		
		<cfoutput query="qGetGrid">#TRIM(LIBELLE_TYPE_LIGNE)#;#TRIM(SOUS_TETE)#;#TRIM(COMMENTAIRES)#;#NewLine#</cfoutput>	 		
	</cffunction>	
</cfcomponent>

 
