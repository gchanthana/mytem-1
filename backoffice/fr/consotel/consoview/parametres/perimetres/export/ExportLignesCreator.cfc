<cfcomponent displayname="ExportLignesCreator" output="true">	
	<cffunction name="createJournal" access="public" returntype="any" output="false">
		<cfargument name="type" required="true" type="string" />			
		<cfset export=createObject("component","fr.consotel.consoview.parametres.perimetres.export." & ucase(#format#) & "." & #type# )>			 
		<cfreturn export>
	</cffunction>	
</cfcomponent>
  