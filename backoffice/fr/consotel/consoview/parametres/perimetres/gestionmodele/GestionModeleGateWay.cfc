<cfcomponent extends="fr.consotel.consoview.access.AccessObject" output="false">
		
	<cffunction access="remote" name="getTypePossible" returntype="query">
		<cfargument name="idNiveauPere" required="true" type="numeric">
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_MODELE.Type_Niveau_Dispo">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idNiveauPere#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
		
	</cffunction>
	
	
	<cffunction access="remote" name="getListeModelesExistant" returntype="query">
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_MODELE.LISTE_ORGA_MODELES">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getidGroupe()#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction access="remote" name="supprimerModele" returntype="numeric">
		<cfargument name="idOrgaModele" required="true" type="numeric">
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_MODELE.DELETE_ORGA_MODELE">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOrgaModele#" null="false">			
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">			
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>


	
	<cffunction access="remote" name="getInfosModele" returntype="query">
		<cfargument name="idOrgaModele" required="true" type="numeric">
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_MODELE.INFOS_ORGA_MODELE">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOrgaModele#" null="false">			
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>	
	
	<cffunction access="remote" name="getModeleStructure" returntype="query">
		<cfargument name="idOrgaModele" required="true" type="numeric">
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_MODELE.STRUCTURE_ORGA_MODELE">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOrgaModele#" null="false">			
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction access="private" name="creerModele" returntype="numeric">
		<cfargument name="libelle" required="true" type="string">
		<cfargument name="commentaire" required="true" type="string">		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_MODELE.CREATE_ORGA_MODELE">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getidGroupe()#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>	
	
	
	
	<cffunction access="private" name="enregistrerNiveauModele" returntype="numeric">		
		
		<cfargument name="idOrgaModele" required="true" type="numeric">
		<cfargument name="typeNiveau" required="true" type="string">		
		<cfargument name="numeroNiveau" required="true" type="string">		
		
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_MODELE.CREATE_ORGA_NIVEAU">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOrgaModele#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#typeNiveau#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numeroNiveau#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		
		
		<cfreturn p_result>
	</cffunction>	
	
	<cffunction access="remote" name="enregistrerModeleEtSctucture" returntype="numeric">
		
		<cfargument name="libelle" required="true" type="string">
		<cfargument name="commentaire" required="true" type="string">
		<cfargument name="structure" required="true" type="array">
		
		 
		<cfset idorgaModele = creerModele(libelle,commentaire)>
		<cfset idOrgaNiveauRef = -2>
		
		<cfif idorgaModele gt 0>
			<cfset len = ArrayLen(structure)>
			<cfloop index="i" from="1" to="#len#">
				<cfset idOrgaNiveau = enregistrerNiveauModele(idorgaModele,structure[i].TYPE_NIVEAU,structure[i].NIVEAU)>								
				<cfif i eq 1 and idOrgaNiveau gt 0><cfset idOrgaNiveauRef = idOrgaNiveau></cfif>
			</cfloop>
			 
		<cfelse>
			<cfreturn -1> 
		</cfif>
		
		<cfreturn idOrgaNiveauRef>
	</cffunction>	
	
	
	
	
	<cffunction access="remote" name="checkDoubleOrgaANU" returntype="numeric">
				
		<cfargument name="typeOrga" required="true" type="string">
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_GLOBAL.CHECK_TYPE_ORGA">
			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getidGroupe()#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#typeOrga#" null="false">		
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">	
			
		</cfstoredproc>
		<cfreturn p_result>
				
	</cffunction>	
	
	<cffunction access="remote" name="isCompatibleOrgaSourceSeclectedTyepOrga" returntype="numeric">
	
		<cfargument name="idOrgaNiveau" required="true" type="numeric">
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_GLOBAL.CKC_DERNIER_NIVEAU">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOrgaNiveau#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">	
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>	

</cfcomponent>