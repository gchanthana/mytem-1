<cfcomponent name="gestionPerimetreAssignment">
		
		<cffunction name="searchAssignNodes" returntype="query" access="remote">
			<cfargument name="id_racine" required="true" type="numeric" />
			<cfargument name="id_orgaRef" required="true" type="numeric" />
			<cfargument name="id_orga" required="true" type="numeric" />
			<cfargument name="libelle_groupe" required="true" type="string" />
			<cfargument name="date" required="true" type="string" />
			<cfset realid=session.perimetre.ID_GROUPE/>
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_Global.Search_Assig_orgaref_orga">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#realid#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orgaRef#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orga#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
		</cffunction>
		
		
		
		<cffunction name="searchNotAssignNodes" returntype="query" access="remote">
			<cfargument name="id_racine" required="true" type="numeric" />
			<cfargument name="id_orgaRef" required="true" type="numeric" />
			<cfargument name="id_orga" required="true" type="numeric" />
			<cfargument name="libelle_groupe" required="true" type="string" />
			<cfargument name="date" required="true" type="string" />
			<cfset realid=session.perimetre.ID_GROUPE/>
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GLOBAL.Search_NotAssig_orgaref_orga">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#realid#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orgaRef#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orga#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
		</cffunction>
	
		<cffunction name="getNotAssignedLines" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="id_orgaRef" required="true" type="numeric" />
			<cfargument name="date" required="true" type="string" />

			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_Global.get_noAffect_orgaref_lignes">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orgaRef#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfset obj = createObject("component","fr.consotel.consoview.parametres.perimetres.session")>		
			<cfset obj.setData(p_result,"ExportLignes")>	
			<cfreturn p_result>
		</cffunction>
		
		<cffunction name="getAssignedLines" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="id_orgaRef" required="true" type="numeric" />
			<cfargument name="date" required="true" type="string" />
		
			<!---<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_Global.get_Affect_orgaref_lignes"> --->
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_Global.get_Affect_orgaref_lignes_v3"> 
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orgaRef#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfset obj = createObject("component","fr.consotel.consoview.parametres.perimetres.session")>		
			<cfset obj.setData(p_result,"ExportLignes")>	
			<cfreturn p_result>
		</cffunction>
		
		<!--- cf ticket #1815 --->
		<cffunction name="getAssignedLines_v2" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="id_orgaRef" required="true" type="numeric" />
			<cfargument name="date" required="true" type="string" />
		
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_Global.Get_Affect_orgaref_lignes_v4">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orgaRef#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfset obj = createObject("component","fr.consotel.consoview.parametres.perimetres.session")>		
			<cfset obj.setData(p_result,"ExportLignes")>	
			<cfreturn p_result>
		</cffunction>


<!--- cf ticket #1815 --->
		<cffunction name="getNotAssignedLines_v2" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="id_orgaRef" required="true" type="numeric" />
			<cfargument name="date" required="true" type="string" />

			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_Global.Get_NoAffect_orgaref_lignes_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orgaRef#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfset obj = createObject("component","fr.consotel.consoview.parametres.perimetres.session")>		
			<cfset obj.setData(p_result,"ExportLignes")>	
			<cfreturn p_result>
		</cffunction>


<!--- cf ticket #1815 --->
	<cffunction name="getAllLines_v2" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="no_use" required="true" type="numeric" />
			<cfargument name="date" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.Get_all_lines_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfset obj = createObject("component","fr.consotel.consoview.parametres.perimetres.session")>		
			<cfset obj.setData(p_result,"ExportLignes")>	
			<cfreturn p_result>
		</cffunction>
		
		
		
		<cffunction name="multipleAssign" returntype="numeric" access="remote">
			<cfargument name="id_line" required="true" type="string" />
			<cfargument name="list_id_groupe" required="true" type="string" />
			<cfargument name="start_date" required="true" type="string" />
			<cfargument name="end_date" required="true" type="string" />
			<cfargument name="list_coef" required="true" type="string" />
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.aff_mult_liste_nodes">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#list_id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_line#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#list_coef#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#start_date#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#end_date#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>

		</cffunction>
		
		<cffunction name="simpleAssign" returntype="string" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="p_list_idsous_tete" required="true" type="string" />
			<cfargument name="date" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.aff_simple_liste_lines_node">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_list_idsous_tete#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="getCoefByDate" returntype="any" access="remote">
			<cfargument name="id_line" required="true" type="string" />
			<cfargument name="date_start" required="true" type="string" />
			<cfargument name="date_end" required="true" type="string" />
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.get_coef_byDate">
		
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_start#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_end#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_line#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>

			<cfset firstOrder = structNew()>
			<cfset dataStruct = arrayNew(1)>
			<cfset cpt=1>
			<cfloop index="i" from="1" to="#p_result.recordcount#">
				<cftry>
					<cfset test=firstOrder[p_result["IDGROUPE_CLIENT"][i]]>
					<cfcatch>
						<cfset firstOrder[p_result["IDGROUPE_CLIENT"][i]]=cpt>
						<cfset dataStruct[cpt]=structNew()>
						<cfset cpt = cpt + 1>
					</cfcatch>
				</cftry>
				
			</cfloop>
			
			
			<cfloop index="i" from="1" to="#p_result.recordcount#">
				<cfset groupeKey=firstOrder[p_result["IDGROUPE_CLIENT"][i]]>
				<cfset dataStruct[groupeKey].node_id = p_result["IDGROUPE_CLIENT"][i]>
				<cfset dataStruct[groupeKey].node = p_result["LIBELLE_GROUPE_CLIENT"][i]>
				<cfset mydate= "col"&dateformat(p_result["DATEDEB_AFF"][i], "YYYY/MM")>
				<cfset mydateFin= "fin"&dateformat(p_result["DATEFIN_AFF"][i], "YYYY/MM")>
				<cfset dataStruct[groupeKey][mydate]= p_result["RATIO"][i]*100>
				<cfset dataStruct[groupeKey][mydateFin]= 0>
			</cfloop>
		<cfreturn dataStruct>
	</cffunction>
	
	<cffunction name="getCoefAutoByDate" returntype="any" access="remote">
			<cfargument name="id_line" required="true" type="string" />
			<cfargument name="date_start" required="true" type="string" />
			<cfargument name="date_end" required="true" type="string" />
			<cfargument name="id_orga" required="true" type="string" />
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.get_CoefAuto_ByDate">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orga#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_line#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_start#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_end#" null="false">
				
				<cfprocresult name="p_result">
			</cfstoredproc>

			<cfset firstOrder = structNew()>
			<cfset dataStruct = arrayNew(1)>
			<cfset cpt=1>
			<cfloop index="i" from="1" to="#p_result.recordcount#">
				<cftry>
					<cfset test=firstOrder[p_result["IDGROUPE_CLIENT"][i]]>
					<cfcatch>
						<cfset firstOrder[p_result["IDGROUPE_CLIENT"][i]]=cpt>
						<cfset dataStruct[cpt]=structNew()>
						<cfset cpt = cpt + 1>
					</cfcatch>
				</cftry>
				
			</cfloop>
			
			
			<cfloop index="i" from="1" to="#p_result.recordcount#">
				<cfset groupeKey=firstOrder[p_result["IDGROUPE_CLIENT"][i]]>
				<cfset dataStruct[groupeKey].node_id = p_result["IDGROUPE_CLIENT"][i]>
				<cfset dataStruct[groupeKey].node = p_result["LIBELLE_GROUPE_CLIENT"][i]>
				<cfset mydate= "col"&dateformat(p_result["DATEDEB_AFF"][i], "YYYY/MM")>
				<cfset mydateFin= "fin"&dateformat(p_result["DATEFIN_AFF"][i], "YYYY/MM")>
				<cfset dataStruct[groupeKey][mydate]= p_result["RATIO"][i]*100>
				<cfset dataStruct[groupeKey][mydateFin]= 0>
			</cfloop>
		<cfreturn dataStruct>
	</cffunction>
	
	<cffunction name="getSdaByDate" returntype="any" access="remote">
		<cfargument name="id_line" required="true" type="string" />
		<cfargument name="date_start" required="true" type="string" />
		<cfargument name="id_orga" required="true" type="string" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.get_liste_sda_ligne_byDate">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_line#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_start#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orga#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="assignSda" returntype="string" access="remote">
			<cfargument name="id_groupe" required="true" type="string" />
			<cfargument name="date" required="true" type="string" />
			<cfargument name="id_sda" required="true" type="string" />
			<cfargument name="assignNext" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.affecte_sda">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#id_sda#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#assignNext#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="closedownSda" returntype="string" access="remote">
			<cfargument name="id_groupe" required="true" type="string" />
			<cfargument name="date" required="true" type="string" />
			<cfargument name="id_sda" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.desaffecte_sda">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#id_sda#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction>

	<cffunction name="getAllLines" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="no_use" required="true" type="numeric" />
			<cfargument name="date" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.Get_all_lines">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfset obj = createObject("component","fr.consotel.consoview.parametres.perimetres.session")>		
			<cfset obj.setData(p_result,"ExportLignes")>	
			<cfreturn p_result>
		</cffunction>
				
</cfcomponent>
