<cfcomponent name="gestionPerimetre">

		<cffunction name="getListePerimetresDataset" access="remote" returntype="any">
			<cfargument name="idGroupe" type="numeric" required="true">
			<cfargument name="filterType" type="numeric" required="true">
			<cfargument name="date" type="string" required="true">
	        <cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GLOBAL.Arbre_orga">
		        <cfprocparam value="#idGroupe#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#filterType#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#date#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocresult name="qGetListePerimetresDataset">
		    </cfstoredproc>
			<cfreturn qGetListePerimetresDataset>
		</cffunction>
		
		
		<cffunction name="getListeNotAssignNodes" access="remote" returntype="any">
			<cfargument name="p_idracine" type="numeric" required="true">
			<cfargument name="p_idorgaref" type="numeric" required="true">
			<cfargument name="p_idorga" type="numeric" required="true">
	        <cfargument name="date" type="string" required="true">
	        
	        <cfquery name="qOpeOrga" datasource="#session.OffreDSN#">
				select PKG_CV_GLOBAL.GET_OPE_ORGA(#p_idorga#) as OPE FROM DUAL
			</cfquery>
	        
	        <cfset v_ope_orga = qOpeOrga["OPE"][1]>
	        
	        <cfquery name="qGetListePerimetresDataset" datasource="#session.OffreDSN#">
		    	    SELECT niv, idgroupe_client, id_groupe_maitre, libelle_groupe_client,commentaires, 
                                               bool_orga , ROWNUM AS node_id, is_active
                           FROM (
                           SELECT * 
                                               FROM 
                             (
                                        SELECT LEVEL AS niv, idgroupe_client, b.id_groupe_maitre, b.libelle_groupe_client, 
                                                                                       b.bool_orga , b.commentaires, ROWNUM AS node_id, b.is_active
                                   FROM groupe_client b
                                   START WITH idgroupe_client=#p_idorga#
                                   CONNECT BY PRIOR b.idgroupe_client=b.id_groupe_maitre
                             )
                         WHERE idgroupe_client IN
                                  (
                                        SELECT DISTINCT a.idgroupe_client
                                 FROM groupe_client a
                                 START WITH a.idgroupe_client IN
                                        (
                                                            SELECT DISTINCT g1.idgroupe_client
                                                            FROM groupe_client_ref_client g1
                                                           WHERE g1.idref_client IN
                                                                          (
                                                           SELECT st_op.idsous_tete
                                                           FROM    groupe_client_ref_client gcrc, st_op
                                                           WHERE   gcrc.idref_client=st_op.idref_client 
                                                                                                                                             AND gcrc.idgroupe_client=#p_idracine#
                                                                  AND st_op.operateurid=decode(#v_ope_orga#,0,st_op.operateurid,#v_ope_orga#)
                                                                  AND st_op.bool_actuel=decode(#v_ope_orga#,0,st_op.bool_actuel,1)
                                                           MINUS
                                                           SELECT gcrc1.idref_client
                                                           FROM groupe_client_ref_client gcrc1
                                                           WHERE idgroupe_client IN  (
                                                                                              SELECT idgroupe_client
                                                                                              FROM groupe_client a
                                                                                              START WITH idgroupe_client=#p_idorgaref#
                                                                                              CONNECT BY PRIOR a.idgroupe_client=a.id_groupe_maitre
                                                                                              )
                                                           
                                                                          )
                                                                          AND g1.idgroupe_client IN (
                                                                                            SELECT idgroupe_client
                                                                                                    FROM groupe_client a
                                                                                              START WITH idgroupe_client=#p_idorga#
                                                                                                     CONNECT BY PRIOR a.idgroupe_client=a.id_groupe_maitre
                                                                                                                         )
                                                                   )
                                         CONNECT BY PRIOR a.id_groupe_maitre=a.idgroupe_client
                                  )
                                        )c
                           ORDER BY c.node_id			
			</cfquery>
						
			<cfreturn qGetListePerimetresDataset>
		</cffunction>
		
		
		<cffunction name="getListeAssignNodes" access="remote" returntype="any">
			<cfargument name="p_idracine" type="numeric" required="true">
			<cfargument name="p_idorgaref" type="numeric" required="true">
			<cfargument name="p_idorga" type="numeric" required="true">
	        <cfargument name="date" type="string" required="true">
	        
	        <cfquery name="qOpeOrga" datasource="#session.OffreDSN#">
				select PKG_CV_GLOBAL.GET_OPE_ORGA(#p_idorga#) as OPE FROM DUAL
			</cfquery>
	        
	        <cfset v_ope_orga = qOpeOrga["OPE"][1]>
	        
	        <cfquery name="qGetListePerimetresDataset" datasource="#session.OffreDSN#">
		    	SELECT niv, idgroupe_client, id_groupe_maitre, libelle_groupe_client,commentaires, 
                                        bool_orga , ROWNUM AS node_id, is_active
                    FROM (
                   SELECT * 
                                        FROM 
                          (
                                  SELECT LEVEL AS niv, b.idgroupe_client, b.id_groupe_maitre, b.libelle_groupe_client, 
                                                                                 b.bool_orga , b.commentaires, ROWNUM AS node_id, b.is_active
                                FROM groupe_client b
                                START WITH idgroupe_client=#p_idorga#
                                CONNECT BY PRIOR b.idgroupe_client=b.id_groupe_maitre
                          )
                      WHERE idgroupe_client IN
                          (
                                  SELECT DISTINCT a.idgroupe_client
                                  FROM groupe_client a
                                  START WITH a.idgroupe_client  IN (
                                                                                                                        SELECT DISTINCT g1.idgroupe_client
                                                            FROM groupe_client_ref_client g1
                                                            WHERE g1.idref_client IN
                                                                          (
                                                                                                                SELECT st_op.idsous_tete
                                                        FROM       groupe_client_ref_client gcrc, st_op
                                                        WHERE      gcrc.idref_client=st_op.idref_client 
                                                                                                                                       AND gcrc.idgroupe_client=#p_idracine#
                                                               AND st_op.operateurid=decode(#v_ope_orga#,0,st_op.operateurid,#v_ope_orga#)
                                                               AND st_op.bool_actuel=decode(#v_ope_orga#,0,st_op.bool_actuel,1)
                                                        INTERSECT
                                                                                                                SELECT gcrc1.idref_client
                                                        FROM groupe_client_ref_client gcrc1
                                                        WHERE idgroupe_client IN  (
                                                                                        SELECT idgroupe_client
                                                                                              FROM groupe_client a
                                                                                              START WITH idgroupe_client=#p_idorgaref#
                                                                                              CONNECT BY PRIOR a.idgroupe_client=a.id_groupe_maitre
                                                                                              )
                                                        
                                                                          )
                                                                          AND g1.idgroupe_client IN (
                                                                                             SELECT idgroupe_client
                                                                                            FROM groupe_client a
                                                                                              START WITH idgroupe_client=#p_idorga#
                                                                                              CONNECT BY PRIOR a.idgroupe_client=a.id_groupe_maitre
                                                                                                                         )
                                                            )
                                         CONNECT BY PRIOR a.id_groupe_maitre=a.idgroupe_client
                           )
                                  ) c
                    ORDER BY c.node_id
			</cfquery>	        
		    
			<cfreturn qGetListePerimetresDataset>
		</cffunction>
		
		<cffunction name="getListeNotAssignNodes_v2" access="remote" returntype="any">
			<cfargument name="idRacine" type="numeric" required="true">
			<cfargument name="idOrgaRef" type="numeric" required="true">
			<cfargument name="idOrga" type="numeric" required="true">
	        <cfargument name="date" type="string" required="true">
	        
	        <cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GLOBAL.Arbre_NotAssig_orgaref_orga">
		        <cfprocparam value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#idOrgaRef#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#idOrga#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#date#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocresult name="qGetListePerimetresDataset">
		    </cfstoredproc>
		    
			<cfreturn qGetListePerimetresDataset>
		</cffunction>
		
		<cffunction name="getListeAssignNodes_v2" access="remote" returntype="any">
			<cfargument name="idRacine" type="numeric" required="true">
			<cfargument name="idOrgaRef" type="numeric" required="true">
			<cfargument name="idOrga" type="numeric" required="true">
	        <cfargument name="date" type="string" required="true">
	        
			<!--- AVEC OPTIMISATION GUILLAUME : dynamiser le 512--->
			<!---<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_TEST.assigned_orgaxml">
		        <cfprocparam value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#idOrgaRef#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#idOrga#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="512" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#date#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="0" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam variable="qGetListePerimetresDataset" type="out" cfsqltype="CF_SQL_VARCHAR" >
		    </cfstoredproc>--->
			
			
	        <cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GLOBAL.Arbre_Assig_orgaref_orga_v2">
		        <cfprocparam value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#idOrgaRef#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#idOrga#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#date#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocresult name="qGetListePerimetresDataset">
		    </cfstoredproc>
			
			<cflog text="getListeAssignNodes_v2">
			
			<cfreturn qGetListePerimetresDataset>
		</cffunction>
		
		<cffunction name="formatXMLData" returntype="xml" access="remote">
			<cfargument name="sourceQuery" type="query" required="true">
			<cfargument name="idGroupe" type="numeric" required="true">
			
			<cfset dataStruct = structNew()>
			<cfset perimetreXmlDoc = XmlNew()>
			<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode = perimetreXmlDoc.node>
			<cfset dataStruct.ROOT_NODE = rootNode>			
			
			
			<cfif sourceQuery.recordcount gt 0>			
				<cfset rootNode.XmlAttributes.LABEL = TRIM(sourceQuery['LIBELLE_GROUPE_CLIENT'][1])>
				<cfset rootNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][1]>
				<cfset rootNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][1]>
				<cfset rootNode.XmlAttributes.COMMENTAIRES = sourceQuery['COMMENTAIRES'][1]>
				<cfset rootNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][1]>
				<cfset rootNode.XmlAttributes.IS_ACTIVE = sourceQuery['IS_ACTIVE'][1]>
	
				<cfloop index="i" from="1" to="#sourceQuery.recordcount#">
					<cfset currentKey = sourceQuery['IDGROUPE_CLIENT'][i]>
					<cfset currentParentKey = sourceQuery['ID_GROUPE_MAITRE'][i]>
					<cfif sourceQuery['IDGROUPE_CLIENT'][i] EQ idGroupe>
						<cfset currentParentKey = "ROOT_NODE">
					</cfif>
					<cfset parentChildCount = arrayLen(dataStruct['#currentParentKey#'].XmlChildren)>
					<cfset dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node")>
					<cfset tmpChildNode = dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1]>
					<cfset tmpChildNode.XmlAttributes.LABEL = TRIM(sourceQuery['LIBELLE_GROUPE_CLIENT'][i])>
					<cfset tmpChildNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][i]>
					<cfset tmpChildNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][i]>
					<cfset tmpChildNode.XmlAttributes.IS_ACTIVE = sourceQuery['IS_ACTIVE'][i]>
					<cfset tmpChildNode.XmlAttributes.COMMENTAIRES = sourceQuery['COMMENTAIRES'][i]>
					<cfset structInsert(dataStruct,currentKey,tmpChildNode)>
				</cfloop>			
			</cfif>
			
			<cfreturn perimetreXmlDoc>
		</cffunction>

		
		<cffunction name="getNodePerimetre" returntype="xml" access="remote">
			<cfargument name="idGroupe" type="numeric" required="true">
			<cfargument name="filterType" type="numeric" required="true">
			<cfargument name="date" type="string" required="true">
			
			<cfset sourceQuery = getListePerimetresDataset(idGroupe, filterType, date)>
	 
			
			<cfset dataStruct = structNew()>
			<cfset perimetreXmlDoc = XmlNew()>
			<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode = perimetreXmlDoc.node>
			<cfset dataStruct.ROOT_NODE = rootNode>
			
			<cfif sourceQuery.recordcount gt 0>
				<cfset rootNode.XmlAttributes.LABEL = sourceQuery['LIBELLE_GROUPE_CLIENT'][1]>
				<cfset rootNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][1]>		
				<cfset rootNode.XmlAttributes.COMMENTAIRES = sourceQuery['COMMENTAIRES'][1]>
				<cfset rootNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][1]>
				<cfset rootNode.XmlAttributes.NIV = sourceQuery['NIV'][1]>
						

				<cfloop index="i" from="1" to="#sourceQuery.recordcount#">
					<cfset currentKey = sourceQuery['IDGROUPE_CLIENT'][i]>
					<cfset currentParentKey = sourceQuery['ID_GROUPE_MAITRE'][i]>
					<cfif sourceQuery['IDGROUPE_CLIENT'][i] EQ idGroupe>
						<cfset currentParentKey = "ROOT_NODE">
					</cfif>
					<cfset parentChildCount = arrayLen(dataStruct['#currentParentKey#'].XmlChildren)>
					<cfset dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node")>
					<cfset tmpChildNode = dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1]>
					<cfset tmpChildNode.XmlAttributes.LABEL = sourceQuery['LIBELLE_GROUPE_CLIENT'][i]>
					<cfset tmpChildNode.XmlAttributes.NIV = sourceQuery['NIV'][i]>
					<cfset tmpChildNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][i]>
					<cfset tmpChildNode.XmlAttributes.COMMENTAIRES = sourceQuery['COMMENTAIRES'][i]>
					<cfset tmpChildNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][i]>
					<cfset structInsert(dataStruct,currentKey,tmpChildNode)>
				</cfloop>
			</cfif>
			<cfreturn perimetreXmlDoc>
		</cffunction>
			
		
		<cffunction name="getCheminSourceCible" returntype="query" access="remote">			
			<cfargument name="idSource" required="true" type="numeric" />
			<cfargument name="idCible" required="true" type="numeric" />			
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.chemin_source_cible_v2">
				<cfprocparam  value="#idSource#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#idCible#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#SESSION.USER.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qChemin">
			</cfstoredproc>
			<cfreturn qChemin>
		</cffunction>
		
		
		<cffunction name="getNotAssignNode" returntype="xml" access="remote">
			<cfargument name="id_racine" required="true" type="numeric" />
			<cfargument name="id_orgaRef" required="true" type="string" />
			<cfargument name="id_orga" required="true" type="string" />
			<cfargument name="date" required="true" type="string" />
			<cfset realid=session.perimetre.ID_GROUPE/>
			<cfset sourceQuery = getListeNotAssignNodes_v2(realid, id_orgaRef, id_orga, date)>
			<cfset result = formatXMLData(sourceQuery, id_orga)>
			
			<cfreturn result>
		</cffunction>
		
		<cffunction name="getAssignNode" returntype="any" access="remote">
			<cfargument name="id_racine" required="true" type="numeric" />
			<cfargument name="id_orgaRef" required="true" type="string" />
			<cfargument name="id_orga" required="true" type="string" />
			<cfargument name="date" required="true" type="string" />
			<cfset realid=session.perimetre.ID_GROUPE/>
			
			<cfset sourceQuery = getListeAssignNodes_v2(realid, id_orgaRef, id_orga, date)>
			<cfset result = formatXMLData(sourceQuery, id_orga)>
			
			<cfreturn result>
			
			
			<!--- AVEC OPTIMISATION GUILLAUME --->
			<!---<cfset result = getListeAssignNodes_v2(realid, id_orgaRef, id_orga, date)>
			<cfreturn result>--->
		</cffunction>

		<cffunction name="getOrganisation" returntype="query" access="remote">
			<!--- <cfargument name="id_groupe" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_organisations">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result> --->
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_orgabyacces">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#session.user.CLIENTACCESSID#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfquery  name="qresult" dbtype="query">
				select * from p_result Order by TYPE_ORGA asc
			</cfquery>
			<cfreturn qresult>
		</cffunction>
	
	
		<cffunction name="getOrganisationPlus" returntype="query" access="remote">
			<cfreturn createObject("component","fr.consotel.consoview.parametres.perimetres.gestionorganisation.GestionOrganisationGateWay").getOrganisationPlusPerimetre()>
		</cffunction>
		
		
		<cffunction name="getOrganisationCUSGEOANU" returntype="query" access="remote">
			<cfset qAll = createObject("component","fr.consotel.consoview.parametres.perimetres.gestionorganisation.GestionOrganisationGateWay").getOrganisationPlusPerimetre()>
			
			<cfquery name="result" dbtype="query">
				select * from qAll where upper(TYPE_ORGA) IN  ('GEO','CUS','ANU')
			</cfquery>
			<!---  
			
			<cfset queryAddRow(result,1)>
			<cfset querySetCell(result,"TYPE_ORGA","CUS")>
			<cfset querySetCell(result,"IDGROUPE_CLIENT",-1)>
			<cfset querySetCell(result,"COMMENTAIRES","")>
			<cfset querySetCell(result,"LIBELLE_GROUPE_CLIENT","-- à choisir --")>
			<cfset querySetCell(result,"OPERATEURID","")>
			<cfset querySetCell(result,"IDORGA_NIVEAU","")>
			
			--->
			<cfreturn result> 
		</cffunction>
		
		
	 	<cffunction name="addNode" returntype="Numeric" access="remote">
		 	<cfargument name="id_groupe" required="true" type="numeric" />
		 	<cfargument name="id_OrgaNiveau" required="true" type="numeric" />
		 	<cfargument name="name_groupe" required="true" type="string" />
		 	<cfargument name="affect_his_parent" required="true" type="numeric" />
		 	
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.add_node">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_OrgaNiveau#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#name_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#affect_his_parent#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="renameNode" returntype="Numeric" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="name_groupe" required="true" type="string" />
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.rename_node">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#name_groupe#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="deleteNode" returntype="Numeric" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="p_cascade" required="true" type="numeric" />
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.delete_node">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_cascade#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="deleteNodeAna" returntype="Numeric" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="p_cascade" required="true" type="numeric" />
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ana_V3.Delete_node_ana">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_cascade#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="searchNode" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="libelle_groupe" required="true" type="string" />
			<cfargument name="type_request" required="true" type="numeric" />
			<cfargument name="date" required="true" type="string" />
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_search.search_nodes">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#type_request#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			
			<cfquery name="qResult" dbtype="query">
				select * from p_result order by IDGROUPE_CLIENT asc
			</cfquery>
			
			<cfreturn qResult>
		</cffunction>
		
		
		<cffunction name="getLines" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="string" />
			<cfargument name="no_use" required="true" type="numeric" />
			<cfargument name="date" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_lines">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
		</cffunction>
		
		<cffunction name="closeDownLinesNode" returntype="string" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.desaffecte_lines_node">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="closeDownLines" returntype="string" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="p_list_idsous_tete" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.Desaffecte_liste_lignes">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_list_idsous_tete#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="assignLine" returntype="string" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="p_list_idsous_tete" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.Affecte_liste_lignes_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#p_list_idsous_tete#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="1" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>

		
		<cffunction name="getNode" returntype="xml" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="filterType" required="true" type="numeric" />
			<cfargument name="date" required="true" type="string" />
			
			<cfset sourceQuery = getNodePerimetre(id_groupe, filterType, date)>
			
			<cfreturn sourceQuery>
		</cffunction>
		
		<cffunction name="getNodeProperties" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_node_properties">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
		</cffunction>
		
		<cffunction name="updateNodeProperties" returntype="string" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="comment" required="true" type="string" />
			<cfargument name="idOrga" required="true" type="any" />
			<cfargument name="typeOrga" required="true" type="string" />
			<cfset refreshParent="false">
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.rename_Node_comment">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#comment#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfif  IsNumeric(idOrga)>
				<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.Switch_CUSGEO">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOrga#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#typeOrga#" null="false">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
				</cfstoredproc>
				<cfset refreshParent="true">
			</cfif>
			<cfreturn refreshParent>
		</cffunction>
		
		<cffunction name="getFicheSite" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.FicheSite">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
		</cffunction>
		
		<cffunction name="getFicheCollaborateur" returntype="any" access="remote">
			<cfargument name="id_groupe" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.getFicheEmploye">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
		</cffunction>
		
		<cffunction name="updateFicheSite" returntype="string" access="remote">
			<cfargument name="idsite_physique" required="true" type="numeric" />
			<cfargument name="nom_site" required="true" type="string" default=" "/>
			<cfargument name="sp_reference" required="true" type="string" default=" "/>
			<cfargument name="sp_code_interne" required="true" type="string" default=" "/>
			<cfargument name="sp_adresse1" required="true" type="string" default=" "/>
			<cfargument name="sp_adresse2" required="true" type="string" default=" "/>
			<cfargument name="sp_code_postal" required="true" type="string" default=" "/>
			<cfargument name="sp_commune" required="true" type="string" default=" "/>
			<cfargument name="sp_commentaire" required="true" type="string" default=" "/>
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.maj_FicheSite">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsite_physique#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#nom_site#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#sp_reference#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#sp_code_interne#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#sp_adresse1#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#sp_adresse2#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#sp_code_postal#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#sp_commune#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#sp_commentaire#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="updateFicheCollaborateur_v3" returntype="string" access="remote">
			<cfargument name="idgroupe_client" required="true" type="string" />
			<cfargument name="idsite_physique" required="true" type="string" />
			<cfargument name="idemplacement" required="true" type="string" />
			<cfargument name="app_loginid" required="true" type="numeric" />
			<cfargument name="cle_identifiant" required="true" type="string" />
			<cfargument name="civilite" required="true" type="string" />
			<cfargument name="nom" required="true" type="string" />
			<cfargument name="prenom" required="true" type="string" />
			<cfargument name="email" required="true" type="string" />
			<cfargument name="fonction_employe" required="true" type="string" />
			<cfargument name="status_employe" required="true" type="string" />
			<cfargument name="commentaire" required="true" type="string" />
			<cfargument name="bool_publication" required="true" type="numeric" />
			<cfargument name="code_interne" required="true" type="string" />
			<cfargument name="reference_employe" required="true" type="string" />
			<cfargument name="matricule" required="true" type="string" />
			<cfargument name="inout" required="true" type="numeric" />			
			<cfargument name="date_entree" required="true" type="string" />
			<cfargument name="date_sortie" required="true" type="string" />
			<cfargument name="C1" required="true" type="string" />
			<cfargument name="C2" required="true" type="string" />
			<cfargument name="C3" required="true" type="string" />
			<cfargument name="C4" required="true" type="string" />
									
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.Updateemploye_v3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idgroupe_client#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsite_physique#" null="false">
			<cfif idemplacement IS "0">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idemplacement#" null="true">
			<cfelse>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idemplacement#" null="false">
			</cfif>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#app_loginid#" null="true">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cle_identifiant#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#civilite#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#nom#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#prenom#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#email#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#fonction_employe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#status_employe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#bool_publication#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference_employe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#matricule#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#inout#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_entree#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_sortie#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C1#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C2#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C3#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C4#" null="false">
				
				
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C4#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C4#" null="false">				
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C4#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C4#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C4#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C4#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C4#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C4#" null="false">
				
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">				
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		
		
		
		<cffunction name="updateFicheCollaborateur" returntype="string" access="remote">
			<cfargument name="idgroupe_client" required="true" type="string" />
			<cfargument name="idsite_physique" required="true" type="string" />
			<cfargument name="idemplacement" required="true" type="string" />
			<cfargument name="app_loginid" required="true" type="numeric" />
			<cfargument name="cle_identifiant" required="true" type="string" />
			<cfargument name="civilite" required="true" type="string" />
			<cfargument name="nom" required="true" type="string" />
			<cfargument name="prenom" required="true" type="string" />
			<cfargument name="email" required="true" type="string" />
			<cfargument name="fonction_employe" required="true" type="string" />
			<cfargument name="status_employe" required="true" type="string" />
			<cfargument name="commentaire" required="true" type="string" />
			<cfargument name="bool_publication" required="true" type="numeric" />
			<cfargument name="code_interne" required="true" type="string" />
			<cfargument name="reference_employe" required="true" type="string" />
			<cfargument name="matricule" required="true" type="string" />
			<cfargument name="inout" required="true" type="numeric" />			
			<cfargument name="date_entree" required="true" type="string" />
			<cfargument name="date_sortie" required="true" type="string" />
			<cfargument name="C1" required="true" type="string" />
			<cfargument name="C2" required="true" type="string" />
			<cfargument name="C3" required="true" type="string" />
			<cfargument name="C4" required="true" type="string" />
			
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.Updateemploye">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idgroupe_client#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsite_physique#" null="false">
			<cfif idemplacement IS "0">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idemplacement#" null="true">
			<cfelse>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idemplacement#" null="false">
			</cfif>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#app_loginid#" null="true">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cle_identifiant#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#civilite#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#nom#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#prenom#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#email#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#fonction_employe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#status_employe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#bool_publication#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference_employe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#matricule#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#inout#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_entree#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_sortie#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C1#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C2#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C3#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#C4#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="updateAssignMode" returntype="string" access="remote">
			<cfargument name="id_soustete" required="true" type="string" />
			<cfargument name="type" required="true" type="numeric" />
			<cfargument name="idorga" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.maj_type_aff_mode">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#id_soustete#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#type#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idorga#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="getTypeProduit" returntype="query" access="remote">
			 <cfif IsDefined('session.etude.idetudeencours')>
				<cfquery name="qGetTypeProduit" datasource="#session.offreDSN#">
				     select tp.libelle
					from compte_facturation cf, sous_tete st, sous_compte sco, etude_sous_tete etu, produit_client pcl,
					produit_catalogue pca, type_produit tp
					WHERE cf.Idcompte_Facturation=sco.Idcompte_Facturation
					AND sco.idsous_compte=st.Idsous_Compte
					AND st.Idsous_Tete=etu.Idsous_Tete
					and cf.idref_client=pcl.idref_client
					And pcl.idproduit_catalogue=pca.idproduit_catalogue
					and pca.idtype_produit=tp.idtype_produit
					AND etu.Idetude=#session.etude.idetudeencours#
					group by tp.libelle
					ORDER BY lower(tp.libelle)
				</cfquery>
				<cfset session.requete.qtypeProduit=qGetTypeProduit>
				<cfquery dbtype="query" name="results">
					SELECT libelle AS label, libelle AS data
					FROM session.requete.qtypeProduit
					ORDER BY label
				</cfquery>
			<cfelse>
				<cfset results=queryNew("libelle,libelle")>
			</cfif>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="getGroupeProduit" returntype="query" access="remote">
			<cfquery name="qGetGroupeProduit" datasource="#session.offreDSN#">
				select *
				from groupe_produit
			</cfquery>
			<cfquery dbtype="query" name="results">
				SELECT libelle_groupe_produit AS label, idgroupe_produit AS data
				FROM qGetGroupeProduit
				ORDER BY label
			</cfquery>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="getThemeProduit" returntype="query" access="remote">
			<cfargument name="arr" type="array">
			<cfif len(arr[1]) gt 2>
				<cfquery name="qGetGroupeProduit" datasource="#session.offreDSN#">
					select idtheme_produit, theme_libelle 
					from THEME_PRODUIT tp
					WHERE tp.theme_libelle NOT LIKE 'vide%'
					AND lower(tp.segment_theme) like lower('#arr[1]#')
					ORDER BY tp.segment_theme ASC,tp.type_theme, tp.ordre_affichage
				</cfquery>
			<cfelse>
				<cfquery name="qGetGroupeProduit" datasource="#session.offreDSN#">
					select idtheme_produit, tp.segment_theme || ' : ' || theme_libelle as theme_libelle
					from THEME_PRODUIT tp
					WHERE tp.theme_libelle NOT LIKE 'vide%'
					ORDER BY tp.segment_theme ASC,tp.type_theme, tp.ordre_affichage
				</cfquery>
			</cfif>
			<cfquery dbtype="query" name="results">
				SELECT theme_libelle AS label, idtheme_produit AS data
				FROM qGetGroupeProduit
			</cfquery>
			<cfreturn results>
		</cffunction>
	
	<cffunction name="r1" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.ratio_cout_groupe_fixe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="r2" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.nblignes_theme_produit_fixe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="r3" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.nblignes_sans_conso_fixe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="r4" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.nblignes_sans_conso_mobile">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="r5" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.nblignes_groupe_fixe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="r6" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.nblignes_groupe_mobile">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="checkDoublons" returntype="numeric" access="remote">
		
		<cfargument name="orgaID" type="numeric" required="true">
		<cfargument name="pChaine" type="string" required="true">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GLOBAL.check_doublons">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#orgaID#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#pChaine#">			
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>		 
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="checkProfile" returntype="query" access="remote">
		<cfargument name="idGroupeClient" type="numeric" required="true">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.EMPLOYE_GESTIONNAIRE">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupeClient#">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
		<cfreturn qResult>
	</cffunction>
	
	
	<cffunction name="checkProfileAndGetInfos" returntype="any" access="remote">
		<cfargument name="idGroupeClientDest" type="numeric" required="true">
		<cfargument name="idGroupeClientSrc"  type="numeric" required="true">
		
		<cfset qEmploye = checkProfile(idGroupeClientDest)>
		<cfset qInfosCible = "">
		<cfset qInfosSource = "">		
		
		<cfif qEmploye.recordCount gt 0>
			<cfset qInfosCible = getNodeProperties(idGroupeClientDest)>
			<cfset qInfosSource = getNodeProperties(idGroupeClientSrc)>			
			
			<cfset tab = arrayNew(1)>
			<cfset arrayAppend(tab,qEmploye)>
			<cfset arrayAppend(tab,qInfosCible)>
			<cfset arrayAppend(tab,qInfosSource)>
			
			<cfreturn tab>
			
		</cfif>
		
		<cfreturn qEmploye>
	</cffunction>
	
	
	<cffunction name="getEmployesWithProfile" returntype="query" access="remote">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfargument name="profileName" type="string" required="true">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.EMPLOYE_PROFIL">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#profileName#">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
		<cfreturn qResult>
	</cffunction>
	
		<!--- retourne l'id de l'orga ou 0 si erreur --->
	<cffunction name="getNodeOrganisation" returntype="numeric" access="remote">
	 	<cfargument name="p_idgroupe_client" required="true" type="string" />	 	
	 	
	 	<!---
	 	pkg_cv_global.get_idorga(p_idgroupe_client integer
                                 p_retour                 integer)
		--->
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_idorga">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
</cfcomponent>

