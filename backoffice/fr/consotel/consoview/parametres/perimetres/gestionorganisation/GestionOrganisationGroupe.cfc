<cfcomponent extends="fr.consotel.consoview.access.AccessObject" output="false">
	<!--- Liste des orgas pour un perimetre et pour un login --->	
	<cffunction name="getOrganisationsAsQuery" returntype="query" access="remote">		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_organisations">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getidGroupeLigne()#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
		
	<!--- Liste des orgas pour un perimetre et pour un login --->	
	<cffunction name="getOrganisationsVo" returntype="OrganisationVo[]" access="remote">			 
		<cfset tabOrgas = arrayNew(1)>
		<cfset qListeOrgas = getOrganisationsAsQuery()>
		<cfLoop query="qListeOrgas">
			<cfset myOrga = createObject("component","OrganisationVo")>
			<cfscript>	
				myOrga.setIDGROUPE_CLIENT(qListeOrgas.IDGROUPE_CLIENT);
				myOrga.setLIBELLE_GROUPE_CLIENT(qListeOrgas.LIBELLE_GROUPE_CLIENT);
				myOrga.setCOMMENTAIRES(qListeOrgas.COMMENTAIRES);
				myOrga.setISPRIVATE(qListeOrgas.ISPRIVATE);
				myOrga.setID_GROUPE_MAITRE(qListeOrgas.ID_GROUPE_MAITRE);
				myOrga.setIDDECOUPAGE(qListeOrgas.IDDECOUPAGE);
				myOrga.setTYPE_ORGA(qListeOrgas.TYPE_ORGA);
				myOrga.setTYPE_DECOUPAGE(qListeOrgas.TYPE_DECOUPAGE);
				myOrga.setOPERATEURID(qListeOrgas.OPERATEURID);
				myOrga.setDATE_CREATION(qListeOrgas.DATE_CREATION);
				myOrga.setNB_ELEM(qListeOrgas.NB_ELEM);
				myOrga.setBOOL_ORGA(qListeOrgas.BOOL_ORGA);
				myOrga.setMARGE_REFACTURATION(qListeOrgas.MARGE_REFACTURATION);
				myOrga.setMODE_AFFECTATION(qListeOrgas.MODE_AFFECTATION);
				myOrga.setIDCLIENTS_PV(qListeOrgas.IDCLIENTS_PV);
				myOrga.setDATE_MODIF(qListeOrgas.DATE_MODIF);
				myOrga.setHISTO(qListeOrgas.HISTO);
				myOrga.setIDEMPLOYE(qListeOrgas.IDEMPLOYE);
				myOrga.setIDLEVEL_MODELE(qListeOrgas.IDLEVEL_MODELE);
				myOrga.setIDORGA_NIVEAU(qListeOrgas.IDORGA_NIVEAU);
			
			</cfscript>
			<cfset arrayAppend(tabOrgas,myOrga)>
		</cfLoop>
		<cfreturn tabOrgas />
	</cffunction>

	<cffunction name="getOrganisationsPlusAsQuery" returntype="query" access="remote">		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_organisations_Plus">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getidGroupeLigne()#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
		
	<!--- Liste des orgas pour un perimetre et pour un login --->	
	<cffunction name="getOrganisationsPlusVo" returntype="OrganisationVo[]" access="remote">			 
		<cfset tabOrgas = arrayNew(1)>
		<cfset qListeOrgas = getOrganisationsPlusAsQuery()>
		<cfLoop query="qListeOrgas">
			<cfset myOrga = createObject("component","OrganisationVo")>
			<cfscript>	
				myOrga.setIDGROUPE_CLIENT(qListeOrgas.IDGROUPE_CLIENT);
				myOrga.setLIBELLE_GROUPE_CLIENT(qListeOrgas.LIBELLE_GROUPE_CLIENT);
				myOrga.setCOMMENTAIRES(qListeOrgas.COMMENTAIRES);
				myOrga.setISPRIVATE(qListeOrgas.ISPRIVATE);
				myOrga.setID_GROUPE_MAITRE(qListeOrgas.ID_GROUPE_MAITRE);
				myOrga.setIDDECOUPAGE(qListeOrgas.IDDECOUPAGE);
				myOrga.setTYPE_ORGA(qListeOrgas.TYPE_ORGA);
				myOrga.setTYPE_DECOUPAGE(qListeOrgas.TYPE_DECOUPAGE);
				myOrga.setOPERATEURID(qListeOrgas.OPERATEURID);
				myOrga.setDATE_CREATION(qListeOrgas.DATE_CREATION);
				myOrga.setNB_ELEM(qListeOrgas.NB_ELEM);
				myOrga.setBOOL_ORGA(qListeOrgas.BOOL_ORGA);
				myOrga.setMARGE_REFACTURATION(qListeOrgas.MARGE_REFACTURATION);
				myOrga.setMODE_AFFECTATION(qListeOrgas.MODE_AFFECTATION);
				myOrga.setIDCLIENTS_PV(qListeOrgas.IDCLIENTS_PV);
				myOrga.setDATE_MODIF(qListeOrgas.DATE_MODIF);
				myOrga.setHISTO(qListeOrgas.HISTO);
				myOrga.setIDEMPLOYE(qListeOrgas.IDEMPLOYE);
				myOrga.setIDLEVEL_MODELE(qListeOrgas.IDLEVEL_MODELE);
				myOrga.setIDORGA_NIVEAU(qListeOrgas.IDORGA_NIVEAU);
			
			</cfscript>
			<cfset arrayAppend(tabOrgas,myOrga)>
		</cfLoop>
		<cfreturn tabOrgas />
	</cffunction>
	
	
	<cffunction name="getOrganisationPlus" returntype="query" access="remote">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_orgabyacces">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getidGroupe()#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getidUser()#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfquery  name="qresult" dbtype="query">
			select * from p_result Order by TYPE_ORGA asc
		</cfquery>
		<cfreturn qresult>
	</cffunction>
	
	<cffunction name="getOrganisationByAcces" returntype="query" access="remote">		
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_orgabyacces">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getidGroupe()#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getidUser()#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfquery  name="qresult" dbtype="query">
			select * from p_result order by TYPE_ORGA asc
		</cfquery>
	
		<cfreturn qresult>
	</cffunction>
	 
</cfcomponent>