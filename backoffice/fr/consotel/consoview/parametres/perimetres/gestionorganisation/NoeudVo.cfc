<cfcomponent output="false" alias="fr.consotel.consoview.parametres.perimetres.gestionorganisation.NoeudVo">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="IDGROUPE_CLIENT" type="numeric" default="0">
	<cfproperty name="LIBELLE_GROUPE_CLIENT" type="string" default="">
	<cfproperty name="COMMENTAIRES" type="string" default="">
	<cfproperty name="ISPRIVATE" type="numeric" default="0">
	<cfproperty name="ID_GROUPE_MAITRE" type="numeric" default="0">
	<cfproperty name="IDDECOUPAGE" type="numeric" default="0">
	<cfproperty name="TYPE_ORGA" type="string" default="">
	<cfproperty name="TYPE_DECOUPAGE" type="numeric" default="0">
	<cfproperty name="OPERATEURID" type="numeric" default="0">
	<cfproperty name="DATE_CREATION" type="date" default="">
	<cfproperty name="NB_ELEM" type="numeric" default="0">
	<cfproperty name="BOOL_ORGA" type="numeric" default="0">
	<cfproperty name="MARGE_REFACTURATION" type="numeric" default="0">
	<cfproperty name="MODE_AFFECTATION" type="numeric" default="0">
	<cfproperty name="IDCLIENTS_PV" type="numeric" default="0">
	<cfproperty name="DATE_MODIF" type="date" default="">
	<cfproperty name="HISTO" type="numeric" default="0">
	<cfproperty name="IDEMPLOYE" type="numeric" default="0">
	<cfproperty name="IDLEVEL_MODELE" type="numeric" default="0">
	<cfproperty name="IDORGA_NIVEAU" type="numeric" default="0">
	<cfproperty name="DATE_CREATION_STRING" type="string" default="">

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.IDGROUPE_CLIENT = 0;
		variables.LIBELLE_GROUPE_CLIENT = "";
		variables.COMMENTAIRES = "";
		variables.ISPRIVATE = 0;
		variables.ID_GROUPE_MAITRE = 0;
		variables.IDDECOUPAGE = 0;
		variables.TYPE_ORGA = "";
		variables.TYPE_DECOUPAGE = 0;
		variables.OPERATEURID = 0;
		variables.DATE_CREATION = "";
		variables.NB_ELEM = 0;
		variables.BOOL_ORGA = 0;
		variables.MARGE_REFACTURATION = 0;
		variables.MODE_AFFECTATION = 0;
		variables.IDCLIENTS_PV = 0;
		variables.DATE_MODIF = "";
		variables.HISTO = 0;
		variables.IDEMPLOYE = 0;
		variables.IDLEVEL_MODELE = 0;
		variables.IDORGA_NIVEAU = 0;
		variables.DATE_CREATION_STRING = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="Noeud">
		<cfreturn this>
	</cffunction>
	<cffunction name="getIDGROUPE_CLIENT" output="false" access="public" returntype="any">
		<cfreturn variables.IDGROUPE_CLIENT>
	</cffunction>

	<cffunction name="setIDGROUPE_CLIENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDGROUPE_CLIENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLIBELLE_GROUPE_CLIENT" output="false" access="public" returntype="any">
		<cfreturn variables.LIBELLE_GROUPE_CLIENT>
	</cffunction>

	<cffunction name="setLIBELLE_GROUPE_CLIENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.LIBELLE_GROUPE_CLIENT = arguments.val>
	</cffunction>

	<cffunction name="getCOMMENTAIRES" output="false" access="public" returntype="any">
		<cfreturn variables.COMMENTAIRES>
	</cffunction>

	<cffunction name="setCOMMENTAIRES" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.COMMENTAIRES = arguments.val>
	</cffunction>

	<cffunction name="getISPRIVATE" output="false" access="public" returntype="any">
		<cfreturn variables.ISPRIVATE>
	</cffunction>

	<cffunction name="setISPRIVATE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ISPRIVATE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getID_GROUPE_MAITRE" output="false" access="public" returntype="any">
		<cfreturn variables.ID_GROUPE_MAITRE>
	</cffunction>

	<cffunction name="setID_GROUPE_MAITRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ID_GROUPE_MAITRE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDDECOUPAGE" output="false" access="public" returntype="any">
		<cfreturn variables.IDDECOUPAGE>
	</cffunction>

	<cffunction name="setIDDECOUPAGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDDECOUPAGE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTYPE_ORGA" output="false" access="public" returntype="any">
		<cfreturn variables.TYPE_ORGA>
	</cffunction>

	<cffunction name="setTYPE_ORGA" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.TYPE_ORGA = arguments.val>
	</cffunction>

	<cffunction name="getTYPE_DECOUPAGE" output="false" access="public" returntype="any">
		<cfreturn variables.TYPE_DECOUPAGE>
	</cffunction>

	<cffunction name="setTYPE_DECOUPAGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.TYPE_DECOUPAGE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getOPERATEURID" output="false" access="public" returntype="any">
		<cfreturn variables.OPERATEURID>
	</cffunction>

	<cffunction name="setOPERATEURID" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.OPERATEURID = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_CREATION" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_CREATION>
	</cffunction>

	<cffunction name="setDATE_CREATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_CREATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getNB_ELEM" output="false" access="public" returntype="any">
		<cfreturn variables.NB_ELEM>
	</cffunction>

	<cffunction name="setNB_ELEM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.NB_ELEM = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getBOOL_ORGA" output="false" access="public" returntype="any">
		<cfreturn variables.BOOL_ORGA>
	</cffunction>

	<cffunction name="setBOOL_ORGA" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.BOOL_ORGA = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMARGE_REFACTURATION" output="false" access="public" returntype="any">
		<cfreturn variables.MARGE_REFACTURATION>
	</cffunction>

	<cffunction name="setMARGE_REFACTURATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.MARGE_REFACTURATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMODE_AFFECTATION" output="false" access="public" returntype="any">
		<cfreturn variables.MODE_AFFECTATION>
	</cffunction>

	<cffunction name="setMODE_AFFECTATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.MODE_AFFECTATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDCLIENTS_PV" output="false" access="public" returntype="any">
		<cfreturn variables.IDCLIENTS_PV>
	</cffunction>

	<cffunction name="setIDCLIENTS_PV" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDCLIENTS_PV = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_MODIF" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_MODIF>
	</cffunction>

	<cffunction name="setDATE_MODIF" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_MODIF = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getHISTO" output="false" access="public" returntype="any">
		<cfreturn variables.HISTO>
	</cffunction>

	<cffunction name="setHISTO" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.HISTO = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDEMPLOYE" output="false" access="public" returntype="any">
		<cfreturn variables.IDEMPLOYE>
	</cffunction>

	<cffunction name="setIDEMPLOYE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDEMPLOYE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDLEVEL_MODELE" output="false" access="public" returntype="any">
		<cfreturn variables.IDLEVEL_MODELE>
	</cffunction>

	<cffunction name="setIDLEVEL_MODELE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDLEVEL_MODELE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDORGA_NIVEAU" output="false" access="public" returntype="any">
		<cfreturn variables.IDORGA_NIVEAU>
	</cffunction>

	<cffunction name="setIDORGA_NIVEAU" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDORGA_NIVEAU = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_CREATION_STRING" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_CREATION_STRING>
	</cffunction>

	<cffunction name="setDATE_CREATION_STRING" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.DATE_CREATION_STRING = arguments.val>
	</cffunction>



</cfcomponent>