<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.process.ServiceHandlerM61"
			extends="fr.consotel.api.ibis.publisher.handler.ServiceHandler" hint="handler du service CONTAINER">
	<!--- MISE A JOUR DU STATUS DU RAPPORT 
		http://download.oracle.com/docs/cd/E10415_01/doc/bi.1013/e12188/T421739T524310.htm#jobhistoryinfo
		Status : "Completed", "Error", "Running", "Scheduled", "Suspended" and "Unknown" 
	--->
	<cfset STATUS_CODES=structNew()>
	<cfset STATUS_CODES["S"]="Completed">
	<cfset STATUS_CODES["F"]="Error">
	<cfset STATUS_CODES["W"]="Unknown">
	<cfset STATUS_CODES["P"]="Suspended">
	<cfset DEV_SUPPORT_ADDR="monitoring@saaswedo.com">
	<cfset containerProxy=createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
	
	<cffunction access="public" name="run" returntype="void" hint="Called with the event that triggered this handler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered this handler">
			
		<cfset JOB_ID = ARGUMENTS.eventObject.getJobId()>
		<cfset STATUS = ARGUMENTS.eventObject.getReportStatus()>
		<cfset BI_SERVER = ARGUMENTS.eventObject.getBipServer()>
				
		<cflog text="********************************************** ServiceHandlerM61 >  - function run ">
		
	<!--- INSTANCIATION DE L'API CONTAINER --->	
		
		<cfset objRapport = containerProxy.getInfosFromJOBID(JOB_ID,BI_SERVER)>
		
		<cflog text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - #objRapport.APP_LOGINID#">
		<cflog text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - #objRapport.IDRACINE# ">
		<cflog text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - #STATUS#  ">
		<cflog text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - #objRapport.GLOBALIZATION#">		
		<cflog text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - #objRapport.LASTNAME#  #objRapport.FIRSTNAME#">
		<cflog text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - #objRapport.UUID# ">
		<cflog text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - #objRapport.ColumnList#">
		
		<cftry>
			<cfset var varfct=0>
	<!--- MISE A JOUR DU STATUS DU RAPPORT --->
			<cflog  text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - fonction run : #Ucase(STATUS)#">
			
			<cflog text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - CODEAPPLICATION = #objRapport.CODEAPPLICATION# ">		
			<cfmail server="mail-cv.consotel.fr" port="25" subject="DEBUG] ServiceHandlerM61" to="#DEV_SUPPORT_ADDR#" from="ServiceHandlerM61@saaswedo.com" type="html">
				<cfdump var="#objRapport#" label="objRapport">
			</cfmail>
			<cfif Ucase(STATUS_CODES[Ucase(STATUS)]) eq Ucase("Error")>
				<cfset varfct = fctRapportErreur(objRapport.APP_LOGINID,objRapport.IDRACINE,objRapport.UUID,objRapport.NOM_DETAILLE_RAPPORT,
				objRapport.DATE_DEMANDE,objRapport.RACINE,objRapport.MAIL,STATUS,objRapport.GLOBALIZATION,
				objRapport.LASTNAME,objRapport.FIRSTNAME,objRapport.CODEAPPLICATION)>
			<cfelseif Ucase(STATUS_CODES[Ucase(STATUS)]) eq Ucase("Completed")>
				<cfset varfct = fctRapportGenere(objRapport.APP_LOGINID,objRapport.IDRACINE,objRapport.UUID,objRapport.NOM_DETAILLE_RAPPORT,
				objRapport.DATE_DEMANDE,objRapport.RACINE,objRapport.MAIL,STATUS,objRapport.GLOBALIZATION,
				objRapport.LASTNAME,objRapport.FIRSTNAME,objRapport.CODEAPPLICATION)>
			<cfelseif (Ucase(STATUS_CODES[Ucase(STATUS)]) eq Ucase("Unknown")) OR (Ucase(STATUS_CODES[Ucase(STATUS)]) eq Ucase("Suspended"))>
				<cfset varfct = fctRapportErreur(objRapport.APP_LOGINID,objRapport.IDRACINE,objRapport.UUID,objRapport.NOM_DETAILLE_RAPPORT,
				objRapport.DATE_DEMANDE,objRapport.RACINE,objRapport.MAIL,STATUS,objRapport.GLOBALIZATION,
				objRapport.LASTNAME,objRapport.FIRSTNAME,objRapport.CODEAPPLICATION)>
			<cfelse>
				<cfmail from="container@saaswedo.com" to="monitoring@saaswedo.com" bcc="#DEV_SUPPORT_ADDR#" type="html" server="mail.consotel.fr" port="25" charset="utf-8" wraptext="74"
						subject="(container) Status inconnu">
					Le fichier #objRapport.NOM_DETAILLE_RAPPORT#.#objRapport.FORMAT_FICHIER# dont le jobID est #JOB_ID#, a un status inconnu et donc non géré par le processContainer.
					<br>
					Cordialement.
				</cfmail>
			</cfif> 
			<cfmail server="mail-cv.consotel.fr" port="25" subject="[DEBUG]" to="#DEV_SUPPORT_ADDR#" from="ServiceHandlerM61@saaswedo.com" type="html">
				<cfoutput>
					<cfif isDefined("varfct")>
						varfct = #varfct#
					<cfelse>
						isDefined("varfct") = #isDefined("varfct")#
					</cfif>
				</cfoutput> 
			</cfmail>
			<cflog  text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - fonction run : varfct = #varfct#">
		<cfcatch>
			<cflog  text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - fonction run : erreur">
			<cfmail from="ServiceHandlerM61@saaswedo.com" to="#DEV_SUPPORT_ADDR#" subject="SERVICE ERROR HANDLER V1" type="html" server="mail-cv.consotel.fr" port="25">
				<cfoutput>
					JOB_ID : #ARGUMENTS.eventObject.getJobId()#<br />
					REPORT_STATUS : #ARGUMENTS.eventObject.getReportStatus()#<br />
					REPORT_URL : #ARGUMENTS.eventObject.getReportPath()#<br />
					BIP_SERVER : #ARGUMENTS.eventObject.getBipServer()#<br />
					EVENT_TARGET : #ARGUMENTS.eventObject.getEventTarget()#<br />
					EVENT_TYPE : #ARGUMENTS.eventObject.getEventType()#<br />
					EVENT_DISPATCHER : #ARGUMENTS.eventObject.getEventDispatcher()#
				</cfoutput>
				<cfdump var="#CFCATCH#" label="EXCEPTION">
			</cfmail>
			<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
			<cflog text="********************************************** ServiceHandlerM61 > #str#">
		</cfcatch>	
		</cftry>
	</cffunction>
<!--- QUAND LE RAPPORT EST CORRECTEMENT GENERE --->	
	<cffunction name="fctRapportGenere" access="private" returntype="Numeric">
		<cfargument name="APP_LOGINID"			type="Numeric"	required="true">
		<cfargument name="IDRACINE"				type="Numeric"	required="true">
		<cfargument name="UUID" 				type="String"	required="true">
		<cfargument name="NOM_DETAILLE_RAPPORT" type="String"	required="true">
		<cfargument name="DATE_DEMANDE" 		type="String"	required="true">
		<cfargument name="PERIMETRE" 			type="String"	required="true">
		<cfargument name="MAIL_DESTINATAIRE" 	type="String"	required="true">
		<cfargument name="STATUS" 				type="String"	required="true">
		<cfargument name="GLOBALIZATION"		type="string"	required="true">
		<cfargument name="LASTNAME"				type="string"	required="true">
		<cfargument name="FIRSTNAME"			type="string"	required="true">
		<cfargument name="CODEAPPLICATION"		type="Numeric"	required="true">
		
		<cflog text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - function fctRapportGenere -- A">

	<!--- ENVOI D'UN MAIL AU CLIENT POUR L'AVERTIR QUE LE RAPPORT EST DISPONIBLE SUR CONTAINER --->	
		<cfset knowStruct1=structNew()>
		<cfset knowStruct1["NOM_DETAILLE_RAPPORT"]	=#arguments.NOM_DETAILLE_RAPPORT#>
		<cfset knowStruct1["DATE_DEMANDE"]			=#arguments.DATE_DEMANDE#>
		<cfset knowStruct1["PERIMETRE"]				=#arguments.PERIMETRE#>
		<cfset knowStruct1["MAIL_DESTINATAIRE"]		=#arguments.MAIL_DESTINATAIRE#>
		<cfset knowStruct1["APP_LOGINID"]			=#arguments.APP_LOGINID#>
		<cfset knowStruct1["IDRACINE"]				=#arguments.IDRACINE#>
		<cfset knowStruct1["GLOBALIZATION"]			=#arguments.GLOBALIZATION#>
		<cfset knowStruct1["LASTNAME"]				=#arguments.LASTNAME#>
		<cfset knowStruct1["FIRSTNAME"]				=#arguments.FIRSTNAME#>
		<cfset knowStruct1["CODEAPPLICATION"]		=#arguments.CODEAPPLICATION#>								
		<!--- Pour connaitre le droit de notification du client demandeur du rapport --->
		<cfset maj = containerProxy.MAJStatus(UUID, 2)>
		<cfset containerProxy.MAJTaille(UUID)>

		<cftry>
		<!--- REMPLACEMENT DU FICHIER OUTPUT PAR L'UUID --->		
			
			<cflog text="********************************************** ServiceHandlerM61 > processContainer - fctRapportGenere : maj du status : #maj#">
			<cfset knowStatus=containerProxy.KNOWMailRight(APP_LOGINID,IDRACINE,"GENERE")>
			<cflog text="********************************************** ServiceHandlerM61 > processContainer - fctRapportGenere : droit de notification : #knowStatus#">
			<cfif knowStatus EQ 1>
				<!--- si le client a le droit de recevoir un mail --->
				<cfset knowStatus=containerProxy.SENDMailFactory("Publication","Client",knowStruct1)>
				<cflog text="********************************************** ServiceHandlerM61 > SENDMailFactory Publication Client processContainer - fctRapportGenere : mail envoye ? #knowStatus#">
				<cflog text="********************************************** ServiceHandlerM61 > SENDMailFactory Publication Client  processContainer - Calcul de la taille du rapport">
				<cfset tailleCalc = 0>
				
				
			<cfelse>
				<cfset knowStatus = -1>
			</cfif>
		<cfcatch type="any">
				<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
				<cflog text="********************************************** ServiceHandlerM61 > #str#">
				<cfset knowStatus = -1>
		</cfcatch>
		</cftry>
		<cfreturn knowStatus>
	</cffunction>
<!--- QUAND LE RAPPORT EST EN ERREUR --->	
	<cffunction name="fctRapportErreur" access="private" returntype="Numeric">
		<cfargument name="APP_LOGINID"			type="Numeric"	required="true">
		<cfargument name="IDRACINE"				type="Numeric"	required="true">
		<cfargument name="UUID" 				type="String"	required="true">
		<cfargument name="NOM_DETAILLE_RAPPORT" type="String"	required="true">
		<cfargument name="DATE_DEMANDE" 		type="String"	required="true">
		<cfargument name="PERIMETRE" 			type="String"	required="true">
 		<cfargument name="MAIL_DESTINATAIRE" 	type="String"	required="true">
		<cfargument name="STATUS" 				type="String"	required="true">
		<cfargument name="GLOBALIZATION"		type="String"	required="true">
		<cfargument name="LASTNAME"				type="string"	required="true">
		<cfargument name="FIRSTNAME"			type="string"	required="true">
		<cfargument name="CODEAPPLICATION"		type="Numeric"	required="true">
		
		<cflog text="********************************************** ServiceHandlerM61 > PROCESSCONTAINER - function fctRapportErreur">
	<!---
		<cfset maj = containerProxy.MAJStatus(UUID, 3)>
	--->
	<!--- ENVOI D'UN MAIL AU CLIENT POUR L'AVERTIR QUE LE RAPPORT EST DISPONIBLE SUR CONTAINER --->	
		<cfset knowStruct1=structNew()>
		<cfset knowStruct1["NOM_DETAILLE_RAPPORT"]	=#arguments.NOM_DETAILLE_RAPPORT#>
		<cfset knowStruct1["DATE_DEMANDE"]			=#arguments.DATE_DEMANDE#>
		<cfset knowStruct1["PERIMETRE"]				=#arguments.PERIMETRE#>
<!--- 		<cfset knowStruct1["MAIL_DESTINATAIRE"]		=#arguments.MAIL_DESTINATAIRE#> --->
		<cfset knowStruct1["MAIL_DESTINATAIRE"]		=#arguments.MAIL_DESTINATAIRE#>
		<cfset knowStruct1["APP_LOGINID"]			=#arguments.APP_LOGINID#>
		<cfset knowStruct1["IDRACINE"]				=#arguments.IDRACINE#>
		<cfset knowStruct1["GLOBALIZATION"]			=#arguments.GLOBALIZATION#>
		<cfset knowStruct1["LASTNAME"]				=#arguments.LASTNAME#>
		<cfset knowStruct1["FIRSTNAME"]				=#arguments.FIRSTNAME#>	
		<cfset knowStruct1["CODEAPPLICATION"]		=#arguments.CODEAPPLICATION#>			
		<!--- Pour connaitre le droit de notification du client demandeur du rapport --->
		<cftry>
			<cfset knowStatus=containerProxy.SENDMailFactory("Erreur","Production",knowStruct1)>
			<cflog text="********************************************** ServiceHandlerM61 > SENDMailFactory ERREUR PORD PROCESSCONTAINER - send Mail Production : #knowstatus#">

			<cfset knowStatus=containerProxy.KNOWMailRight(APP_LOGINID,IDRACINE,"Erreur")>
			<cflog text="************************** ServiceHandlerM61 > PROCESSCONTAINER - droit de notification : #knowstatus#">
			<cfif knowStatus EQ 1>
				<!--- si le client a le droit de recevoir un mail --->
				<cfset knowStatus=containerProxy.SENDMailFactory("Erreur","Client",knowStruct1)>
				<cflog text="********************************************** ServiceHandlerM61 >  SENDMailFactory ERREUR CLIENT PROCESSCONTAINER - send mail client : #knowstatus#">
			<cfelse>
				<cfset knowStatus = -1>
			</cfif>
		<cfcatch type="any">
			<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
			<cflog text="********************************************** ServiceHandlerM61 > #str#">
			<cfset knowStatus = -1>
		</cfcatch>
		</cftry>
		<cfreturn knowStatus>
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="Called when an error has occurred either during creation or execution of IServiceHandler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered run() handler">
		<cfargument name="eventHandler" type="string" required="true" hint="The corresponding EVENT_HANDLER supposed to handle eventObject">
		<cfargument name="exceptionObject" type="any" required="false" hint="Optionally the corresponding catched exception (Same structure as CFCATCH scope)">
			<cfmail from="ServiceHandlerM61@saaswedo.com" to="#DEV_SUPPORT_ADDR#" subject="SERVICE ERROR HANDLER V1" type="html" server="mail-cv.consotel.fr" port="25">
				<cfoutput>
					JOB_ID : #ARGUMENTS.eventObject.getJobId()#<br />
					REPORT_STATUS : #ARGUMENTS.eventObject.getReportStatus()#<br />
					REPORT_URL : #ARGUMENTS.eventObject.getReportPath()#<br />
					BIP_SERVER : #ARGUMENTS.eventObject.getBipServer()#<br />
					EVENT_TARGET : #ARGUMENTS.eventObject.getEventTarget()#<br />
					EVENT_TYPE : #ARGUMENTS.eventObject.getEventType()#<br />
					EVENT_DISPATCHER : #ARGUMENTS.eventObject.getEventDispatcher()#
				</cfoutput>
				<cfdump var="#ARGUMENTS.exceptionObject#" label="EXCEPTION">
			</cfmail>
	</cffunction>
	
</cfcomponent>
