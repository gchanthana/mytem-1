<cfcomponent output="false" displayname="fr.consotel.consoview.M61.upload.UploaderFile" hint="Gestionnaire d'upload de fichiers">
	<!--- 
		uploader un fichier
	--->
	<cffunction name="uploadFile" access="remote" output="false" returntype="Numeric" hint="Upload un fichier">
		
		<cfargument name="currentFile" 	type="fr.consotel.consoview.M28.vo.FileUploadVo" required="true"/>
											  
		<cfset var webPath 	= GetDirectoryFromPath( GetCurrentTemplatePath() )>
		<cfset var dirTmp 	= "tmpFileUploaded/">
		<cfset var pathDirTMP 	= webPath & dirTmp & currentFile.fileUUID>
		
		<cfif !DirectoryExists('#pathDirTMP#')>
			<cfdirectory action="create" directory="#pathDirTMP#" mode="777">
		</cfif>
		
		<cfset currentFile.fileName = createObject("component","fr.consotel.consoview.M28.RegexRenameFile").changeSpecialCaracters(currentFile.fileName)>
		<cfset var pathDirFileTmpRenamed= pathDirTMP & "/" & currentFile.fileName>
		
		<cffile action="write" output="#currentFile.fileData#" file="#pathDirFileTmpRenamed#" mode="777">
		<cfset statusReturn = 1>
		


		<!--- GESTION FTP --->
		<cfset var retour = handleFTP(pathDirFileTmpRenamed,currentFile)>
		
		<cfif retour eq 1>
			<!--- SUPPRESSION Dossier et FICHIER TEMPORAIRE --->
			<cfset var pathDossierTMP 	= webPath & dirTmp>
			 <cfset deleteFileTMP(pathDossierTMP)>
		</cfif>
		
		<cfreturn retour>
	</cffunction>
	
	
	<!---
	 	Ajouter fichier dans ftp
	--->
	<cffunction name="handleFTP" access="private" output="false" returntype="numeric">
		<cfargument name="pathDirFileTmpRenamed" 	type="string" required="true">
		<cfargument name="currentFile" type="struct" required="true">
		
		<cfset var nameFileFtp			= currentFile.fileUUID>
		<cftry>	
		
			<!--- OUVRIR LA CONNEXION --->
			<cfftp connection="ftp" username="container" password="container" server="Pelican.consotel.fr" action="open" stopOnError="Yes">
			<cf
			<!--- UPLOAD DU FICHIER --->
			<cfftp action="putfile" connection="ftp" localFile="#pathDirFileTmpRenamed#" remoteFile="#nameFileFtp#" timeout="300">
			
			<!--- VERIF EXISTENCE FICHIER --->
			<cfftp action="existsfile" connection="ftp" remoteFile="#nameFileFtp#"><!--- directory="#dirFtp#" --->
			<cfif cfftp.returnValue eq "yes">
				 <cfset var retour = 1>
			<cfelse>
				<cfset var retour = 0>
			</cfif>
			
			<!--- FERMER LA CONNEXION --->
			<cfftp connection="ftp" action="close" stopOnError="Yes">
			<cfreturn retour>
		<cfcatch>
			<cflog type="error" text="#CFCATCH.Message#">													
			<cfmail server="mail.consotel.fr" port="26" from="container@saaswedo.com" to="container@saaswedo.com.com" 
					failto="monitoring@saaswedo.com" subject="[M61]Erreur lors de l'upload d'un fichier" type="html">
					#CFCATCH.Message#<br/><br/><br/><br/>
			</cfmail>
		</cfcatch>					
		</cftry>
		
	</cffunction>
	
	
	<!--- 
	 	supprimer le dossier TMP et le fichier tmp
	--->
	<cffunction name="deleteFileTMP" access="private" output="false" returntype="void">
		<cfargument name="pathDirTMP" type="string" required="true">
		
		<cfif DirectoryExists('#pathDirTMP#')>
			<cfdirectory action="delete" directory="#pathDirTMP#" type="dir" mode="777" recurse="true">
		</cfif>
		
	</cffunction>
	
	
	<!--- 
		uploader plusieurs fichiers
	 --->
	<cffunction name="uploadFiles" access="remote" output="false" returntype="Array" hint="Upload plusieurs fichiers">
		
		<cfargument name="filesSelected" type="Array" required="true"/>
		<cfargument name="idActualite"	 type="Numeric" required="true"/>
				
		<cfset var resultUpload	= ArrayNew(1)>
		<cfset var lenFiles 	= arrayLen(filesSelected)>
		<cfset var statusReturn	= 0>
		
		<cfloop index="idx" from="1" to="#lenFiles#">
			
			<!--- <cfset statusReturn = uploadFile(filesSelected[idx],idActualite)>

			<cfset ArrayAppend(resultUpload, statusReturn)> --->
		
		</cfloop>
			
		<cfreturn resultUpload>
	</cffunction>
	
	
	<!--- 
	 	delete dossier dans le ftp (cnx + delete du dir/file + close)
	--->
	<cffunction name="deleteFTP" access="public" output="false" returntype="void">
		
		<cfargument name="UUID" type="string" required="true">
		
		<cfset var dirNews 	= "cv/cache/documents/news/">
		<cfset var pathDirFTP = dirNews & UUID>
		
		<cfmail server="mail-cv.consotel.fr" port="25" from="container@saaswedo.com" to="monitoring@saaswedo.com" 
				failto="monitoring@saaswedo.com" subject="[M61] path uuid - ftp" type="html">
			<cfdump var="#pathDirFTP#">
		</cfmail>
		
		<!--- OUVRIR LA CONNEXION --->
		<cfftp connection="ftp" username="services" password="services" server="Pelican.consotel.fr" action="open" stopOnError="Yes">
		
		<!--- POINTER VERS LE DOSSIER --->
		<cfftp connection="ftp" action="listdir" directory="#pathDirFTP#" name="doss" stopOnError="No">
		
		<cfmail server="mail-cv.consotel.fr" port="25" from="container@saaswedo.com" to="monitoring@saaswedo.com" 
				failto="monitoring@saaswedo.com" subject="[M61] path listdir - ftp" type="html">
			
			<p>FTP Directory:</p>
			<cfdump var="#pathDirFTP#">
			
			<p>FTP File Directory Listing:<br>  
			<cftable query="doss" HTMLTable="Yes" colHeaders="Yes">  
			    <cfcol header="<b>Name</b>" text="#name#">
			    <cfcol header="<b>Path</b>" text="#path#">
			    <cfcol header="<b>URL</b>" text="#url#">
			    <cfcol header="<b>Length</b>" text="#length#">
			    <cfcol header="<b>LastModified</b>" text="#DateFormat(lastmodified)#">
			    <cfcol header="<b>IsDirectory</b>" text="#isdirectory#">
			</cftable>
			
		</cfmail>
		
		<!--- PROCESS DE SUPPRESSION DOSSIER + FILE --->
		<cfloop index="x" From="1" to="#doss.RecordCount#">
			<cfif #doss.IsDirectory[x]# is "No">
			  <!--- suppression du fichier du dossier --->
			  <cfftp Connection="ftp" Action="remove" Item="#doss.Path[x]#">
			</cfif>
		</cfloop>
		<!--- suppression du dossier --->
		<cfftp action="removeDir" connection="ftp" directory="#pathDirFTP#">
		
		<cfmail server="mail-cv.consotel.fr" port="25" from="container@saaswedo.com" to="monitoring@saaswedo.com" 
				failto="monitoring@saaswedo.com" subject="[M61] remove directory - ftp" type="html">
		</cfmail>
		
		<!--- FERMER LA CONNEXION --->
		<cfftp connection="ftp" action="close" stopOnError="Yes">
		
	</cffunction>
	
	
	<!--- 
	 	créer dossier TMP + fichier TMP
	--->
	<cffunction name="createDirTMP" access="private" output="false" returntype="numeric">
		
		<cfargument name="pathDirTMP" type="string" required="true">
		<cfargument name="currentFile" type="struct" required="true">
		
		<cftry>
			
			<cfif NOT DirectoryExists('#pathDirTMP#')>
				<cfdirectory action="Create" directory="#pathDirTMP#" type="dir" mode="777">
			</cfif>
			
			<cfset currentFile.fileName = createObject("component","fr.consotel.consoview.M28.RegexRenameFile").changeSpecialCaracters(currentFile.fileName)>
			
			<cfset pathFileTMP = pathDirTMP & "/" & currentFile.fileName>
			<cffile action="write" output="#currentFile.fileData#" file="#pathFileTMP#" mode="777">
			<cfset statusReturn = 1>
			
			<cfreturn statusReturn>
			
		<cfcatch>
			<!--- log --->
			<cflog type="error" text="#CFCATCH.Message#">
			<!--- mail d'erreur --->
			<cfmail server="mail-cv.consotel.fr" port="25" from="container@saaswedo.com" to="monitoring@saaswedo.com" 
					failto="monitoring@saaswedo.com" subject="[M61] Erreur lors de l'upload de fichiers" type="html">
					
					#CFCATCH.Message#<br/><br/><br/><br/>
					
			</cfmail>
			
		</cfcatch>					
		</cftry>
		
	</cffunction>
	
	
	<!--- 
	
	 --->
	<cffunction name="setDataBaseFilesUploaded" access="remote" output="false" returntype="Array" hint="Upload un fichier">
		<cfargument name="filesSelected" 	type="Array" required="true"/>
		<cfargument name="idcommande" 		type="Numeric" required="true"/>
		
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var currentFile 		= structNew()>
			<cfset var resultUpload		= ArrayNew(1)>
			<cfset var lenFiles 		= arrayLen(filesSelected)>
			<cfset var statusReturn		= 0>
			
			<cfloop index="idx" from="1" to="#lenFiles#">

				<cfset currentFile = filesSelected[idx]>
				
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.enregistrementlistattachments">
					<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_fileName"  	value="#currentFile.fileName#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_path"	  	value="#currentFile.fileUUID#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_userid" 	value="#idgestionnaire#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_size" 		value="#currentFile.fileSize#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_joinMail" 	value="#currentFile.fileJoin#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_format" 	value="#currentFile.fileExt#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" value="#idcommande#"/>
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
				</cfstoredproc>

				<cfset ArrayAppend(resultUpload, p_retour)>
			
			</cfloop>
			
		<cfreturn resultUpload>
	</cffunction>
	
	
	<!--- 
		Renommer un fichier
	 --->
	<cffunction name="renameFile" access="remote" output="false" returntype="Numeric" hint="renommer un fichier">
		<cfargument name="currentFile" 			type="fr.consotel.consoview.M16.vo.FileUploadVo" required="true"/>
		<cfargument name="newFileName" 			type="String" required="true"/>
		<cfargument name="isDataBaseRecord" 	type="Numeric" required="true"/>
			
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var pathFile 		= '/container/M16/'& #currentFile.fileUUID#>
			<cfset var pathFileName 	= '/container/M16/'& #currentFile.fileUUID# & '/' & #currentFile.fileName#>
			<cfset var pathFileNewName 	= '/container/M16/'& #currentFile.fileUUID# & '/' & #newFileName#>
			<cfset var statusReturn		= 0>
			
			<cftry>	
			
				<cfif NOT DirectoryExists('#pathFile#')>
				
					<cfset statusReturn = -10>
	
				<cfelse>
	
					
					<cfif NOT FileExists('#pathFileName#')>
				
						<cfset statusReturn = -11>
	
					<cfelse>
						
						<cffile action="rename" source="#pathFileName#" destination="#pathFileNewName#" attributes="normal">
						
						<cfset statusReturn = 2>
						
					</cfif>
	
				</cfif>

			<cfcatch>
					
				<cflog type="error" text="#CFCATCH.Message#">													
				
				<cfset statusReturn = 0>
				
				<cfmail server="mail.consotel.fr" port="26" from="bdcmobile@consotel.fr" to="postmaster@consotel.fr" 
						failto="dev@consotel.fr" bcc="dev@consotel.fr" subject="[WARN-M16-BDCM]Erreur lors de la suppression de fichiers" type="html">
									 							
						#CFCATCH.Message#<br/><br/><br/><br/>
					
				</cfmail>
				
			</cfcatch>					
			</cftry>
			
			<cfif isDataBaseRecord GT 0>
			
				<cfif statusReturn GT 0>

					<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.renameattachment">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_fileid"   value="#currentFile.fileId#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_new_name" value="#newFileName#"/>
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
					</cfstoredproc>
					
					<cfset statusReturn = p_retour>
					
				</cfif>
				
			</cfif>
			
		<cfreturn statusReturn>
	</cffunction>
	
	
	<!--- 
		Supprimer un fichier
	 --->
	<cffunction name="removeFile" access="remote" output="false" returntype="Numeric" hint="supprimer un fichier">
		<cfargument name="currentFile" 			type="fr.consotel.consoview.M16.vo.FileUploadVo" required="true"/>
		<cfargument name="isDataBaseRecord" 	type="Numeric" required="true"/>
			
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var pathFile 		= '/container/M16/'& #currentFile.fileUUID#>
			<cfset var pathFileName 	= '/container/M16/'& #currentFile.fileUUID# & '/' & #currentFile.fileName#>
			<cfset var statusReturn		= 0>
			
			<cftry>	
			
				<cfif NOT DirectoryExists('#pathFile#')>
				
					<cfset statusReturn = 10>
	
				<cfelse>
	
					
					<cfif NOT FileExists('#pathFileName#')>
				
						<cfset statusReturn = 11>
	
					<cfelse>
						
						<cffile action="delete" file="#pathFileName#">
						
						<cfset statusReturn = 2>
						
					</cfif>
	
				</cfif>

			<cfcatch>
					
				<cflog type="error" text="#CFCATCH.Message#">													
				
				<cfset statusReturn = 0>
				
				<cfmail server="mail.consotel.fr" port="26" from="bdcmobile@consotel.fr" to="postmaster@consotel.fr" 
						failto="dev@consotel.fr" bcc="dev@consotel.fr" subject="[WARN-M16-BDCM]Erreur lors de la suppression de fichiers" type="html">
									 							
						#CFCATCH.Message#<br/><br/><br/><br/>
					
				</cfmail>
				
			</cfcatch>					
			</cftry>
			
			<cfif isDataBaseRecord GT 0>
			
				<cfif statusReturn GT 0>

					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.eraseAttachment">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_fileid" value="#currentFile.fileId#"/>
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
					</cfstoredproc>	
					
					<cfset statusReturn = p_retour>
					
				</cfif>
				
			</cfif>
			
		<cfreturn statusReturn>
	</cffunction>
	
	
	<!--- 
		Upload un fichier
	 --->
	<cffunction name="copyFiles" access="remote" output="false" returntype="Numeric" hint="upload un fichier">
		<cfargument name="queyrFiles" type="Query" required="true"/>
		<cfargument name="idcommande" type="Numeric" required="true"/>
		
			<cfset var newUUID 			= CreateUUID()>
			<cfset var filesCopy 		= ArrayNew(1)>
			<cfset var filesRsltCopy 	= ArrayNew(1)>
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var idracine 		= session.perimetre.id_groupe>
			<cfset var containerPath	= '/container/M16/'>
			<cfset var pathFile 		= '/container/M16/'& #newUUID#>
			<cfset var pathNFileName 	= ''>
			<cfset var pathLFileName 	= ''>
			<cfset var lenFiles			= queyrFiles.recordCount>
			<cfset var statusReturn		= 0>

			<cfif lenFiles GT 0>
			
				<cfloop query="queyrFiles">
					
					<cfset newFiles 				= structNew()>
					<cfset newFiles.fileData 		= structNew()>
					<cfset newFiles.fileDate 		= Now()>
					<cfset newFiles.fileName		= queyrFiles['FILE_NAME'][queyrFiles.currentRow]>
					<cfset newFiles.fileExt			= queyrFiles['FORMAT'][queyrFiles.currentRow]>
					<cfset newFiles.fileUUID		= newUUID>
					<cfset newFiles.fileSize		= queyrFiles['FILE_SIZE'][queyrFiles.currentRow]>
					<cfset newFiles.fileJoin		= queyrFiles['JOIN_MAIL'][queyrFiles.currentRow]>
					<cfset newFiles.fileIdRacine	= idracine>
					<cfset newFiles.fileIdCommande	= idcommande>
					<cfset newFiles.filePath		= pathFile>
					<cfset newFiles.fileLastPath	= queyrFiles['PATH'][queyrFiles.currentRow]>
					<cfset newFiles.fileStatus		= 0>
					
					<cfset pathLFileName = #containerPath# & #queyrFiles['PATH'][queyrFiles.currentRow]# & '/' & #queyrFiles['FILE_NAME'][queyrFiles.currentRow]#>
					
					<cfif FileExists('#pathLFileName#')>
					
						<cfset ArrayAppend(filesCopy, newFiles)>
					
					</cfif>

				</cfloop>
				
				<cfif NOT DirectoryExists('#pathFile#')>
				
					<cfdirectory action="Create" directory="#pathFile#" type="dir" mode="777">
	
				</cfif>
				
				<cfif DirectoryExists('#pathFile#')>
					
					<cfset lenFiles	= arrayLen(filesCopy)>
					
					<cfloop index="idxCopy" from="1" to="#lenFiles#">
					
						<cfset pathLFileName = #containerPath# & #filesCopy[idxCopy].fileLastPath# & '/' & #filesCopy[idxCopy].fileName#>
						<cfset pathNFileName = #containerPath# & #filesCopy[idxCopy].fileUUID# & '/' & #filesCopy[idxCopy].fileName#>
						
						<cftry>
						
							<cffile action="copy" destination="#pathNFileName#" source="#pathLFileName#">
							
							<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.enregistrementlistattachments">
								<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_fileName"  	value="#filesCopy[idxCopy].fileName#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_path"	  	value="#filesCopy[idxCopy].fileUUID#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_userid" 	value="#idgestionnaire#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_size" 		value="#filesCopy[idxCopy].fileSize#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_joinMail" 	value="#filesCopy[idxCopy].fileJoin#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_format" 	value="#filesCopy[idxCopy].fileExt#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" value="#filesCopy[idxCopy].fileIdCommande#"/>
								<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
							</cfstoredproc>
							
							<cfset filesCopy[idxCopy].fileStatus = p_retour>

						<cfcatch>
					
							<cflog type="error" text="#CFCATCH.Message#">													
							
							<cfset statusReturn = -11>
							
							<cfmail server="mail.consotel.fr" port="26" from="bdcmobile@consotel.fr" to="postmaster@consotel.fr" 
									failto="dev@consotel.fr" bcc="dev@consotel.fr" subject="[WARN-M16-BDCM]Erreur lors de la copy de fichiers" type="html">
												 							
									#CFCATCH.Message#<br/><br/><br/><br/>
								
							</cfmail>
							
						</cfcatch>	
						</cftry>

						<cfset ArrayAppend(filesRsltCopy, statusReturn)>
						
					</cfloop>
					
					<cfif statusReturn EQ 0>
					
						<cfset statusReturn = 1>
					
					</cfif>
				
				</cfif>
			
			<cfelse>
			
				<cfset statusReturn = 2>
			
			</cfif>
			
		<cfreturn statusReturn>
	</cffunction>

</cfcomponent>