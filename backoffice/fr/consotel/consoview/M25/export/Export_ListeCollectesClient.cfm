<!--- Export vi BI la liste des collecte clients au format (csv,xls...) spécifié
      pour l'idRacine passé en paramètres
	  param : FORM.FORMAT : le format
	          FORM.RACINEID : l'id racine du groupe --->

<cfset biServer="http://bip-dev.consotel.fr/xmlpserver/services/PublicReportService?WSDL">
	
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDRACINE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=FORM.RACINEID>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>
	
	<cfset myParamReportRequest=structNew()>
	
	<cfset myParamReportRequest.reportAbsolutePath="/consoview/M25/LiteCollectes_Client/LiteCollectes_Client.xdo">	
	
	<cfset myParamReportRequest.attributeLocale="fr_FR">
	<cfset myParamReportRequest.attributeFormat=LCase(FORM.FORMAT)>
	<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
	<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
	<cfset myParamReportParameters=structNew()>
	<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
	
	<cfset myParamReportParameters.userID="consoview">
	<cfset myParamReportParameters.password="public">
	<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
	<cfset reporting=createObject("component","fr.consotel.consoview.api.Reporting")>
	<cfset filename = "Liste_Collectes_Client_" & FORM.RACINEID &  reporting.getFormatFileExtension(FORM.FORMAT)>
			
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#" charset="utf-8">	
	<cfif compareNoCase(trim(FORM.FORMAT),"csv") eq 0>
		<cfcontent type="text/csv; charset=utf-8" variable="#resultRunReport.getReportBytes()#">
		<cfelse>
		<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">		
	</cfif>

