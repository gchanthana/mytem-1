<cfcomponent displayname="ConcreteDemandeCreator">
	
	<cffunction name="createDemande" access="public" returntype="any" output="false">
		<cfargument name="format" required="true" type="string">	
		<cfset demande=createObject("component","fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits." & #format# & ".DemandeNouveauxProduits")>				
		<cfreturn demande>
	</cffunction>
	
</cfcomponent>
  