<cfcomponent displayname="GestionRapprochement" hint="Gestion des rapprochements" output="true">
	
	
	<cffunction name="getNouveauxProduitsByLibelle" access="public" returntype="query">					
		<cfargument name="commandeID" type="numeric" required="true">	 
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.NV_PRODUIT_BYLIBELLE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_commande" value="#commandeID#"/>				 		
			<cfprocresult name="result">		  
		</cfstoredproc>				
		<cfreturn result/>	 
	</cffunction>			
	
	<cffunction name="getNouveauxProduitsByCible" access="public" returntype="query">			
		<cfargument name="commandeID" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.NV_PRODUIT_BYCIBLE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_commande" value="#commandeID#"/>				 		
			<cfprocresult name="result">		  
		</cfstoredproc>
		
		<cfreturn result/>
	</cffunction>	
	
	<cffunction name="getNouveauxProduitsByOP" access="public" returntype="query">			
		<cfargument name="commandeID" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.NV_PRODUIT_BYOP">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_commande" value="#commandeID#"/>				 		
			<cfprocresult name="result">		  
		</cfstoredproc>
		<cfreturn result/>
	</cffunction>	
	
	<cffunction name="assignLines" returntype="string" access="remote">		
		<cfargument name="listeNodes" required="true" type="array" />
		<cfargument name="listeLignes" required="true" type="array"/>
		
		
		<cfset lenght = arrayLen(listeNodes)>
		<cfset p_retour = -1>
		<cfloop index="i" from="1" to="#lenght#">
			
			 
			<cfset idGroupeClient = listeNodes[i]>	
			<cfset ligne = listeLignes[i]>
			
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.Affecte_liste_lignes_v2">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupeClient#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ligne#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="1" null="false">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
					
				</cfstoredproc>	
			<cfcatch type="database">
				<cfthrow type="Erreur" message="Une erreur s'est produite lors de l'enregistrement des affectations" extendedinfo="Plusieurs raisons possibles ....">
			</cfcatch>
			<cfcatch type="application">
				<cfthrow type="Log" message="Votre session est terminÃ©e." extendedinfo="Plusieurs raisons possibles ....">
			</cfcatch>
			</cftry>	
		</cfloop>		
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="getLinesFromNode" returntype="query" access="remote">				
		<cfargument name="idGroupeClient" required="true" type="numeric" />
		<cfargument name="no_use" required="true" type="numeric" />
		<cfargument name="date" required="true" type="string" />
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_lines">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupeClient#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>		
	</cffunction>
	
	<cffunction name="getCibleLibelle" access="public" returntype="String">			
		<cfargument name="idCible" type="numeric">
		<cfset cibleObject = createObject("component","fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.PDF.DemandeNouveauxProduits").getNodeInfos(session.user.clientAccessId,idCible)>
		<cfif structisempty(cibleObject)>
			<cfset cible = cibleObject.LBL>
		<cfelse>
			<cfset cible = "">
		</cfif>
		<cfreturn cible/>
	</cffunction>	
	
	
	
	
	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="false" default="">		
		<cfif len(oldDateString) neq 0>
			<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
			<cfreturn newDateString> 
		<cfelse>
			<cfreturn oldDateString> 
		</cfif>		
	</cffunction>
	
	<cffunction name="rapprocherCommandes" access="public" returntype="numeric">			
		<cfargument name="idClient" type="numeric">
		<cfargument name="typeRecherche" type="string">		
		
		
		<cfswitch expression="#UCASE(typeRecherche)#">
			
			<cfcase value="LIBELLE">
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_CYCLEVIE_V3.RATIO_PRODUIT_BYLIBELLE">
					<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idClient#" null="false">
					<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" null="false">
				</cfstoredproc>
				<cfreturn p_retour>
			</cfcase>
			
			<cfcase value="OP">				
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_CYCLEVIE_V3.RATIO_PRODUIT_BYOP">
					<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idClient#" null="false">
					<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" null="false">
				</cfstoredproc>
				<cfreturn p_retour>
			</cfcase>

			
			<cfcase value="CIBLE">
				<cftry>
					<cfset commandes = createObject("component","Commande")>
					<cfset tabCommandeEnCours = commandes.getListCom(idClient,2)>		
					<cfset produits = createObject("component","ElementCommande")>

					<cfloop query="tabCommandeEnCours">
						
						<cfset fonct = evaluate("getNouveauxProduitsBy" & #typeRecherche#)>
						<cfset produitsFact = fonct(IDCDE_COMMANDE)>			
						<cfset panier = produits.getProduitsCommande(IDCDE_COMMANDE)>			
						<cfset numberOfPrdtinCom = panier.recordCount> 
						<cfset numberOfPrdtinFact = produitsFact.recordCount>
						
						<cfif numberOfPrdtinFact eq 0>
							<cfset ratio = 0>
						<cfelse>	
							<cfset ratio = (numberOfPrdtinFact  /  numberOfPrdtinCom)*100 >
							<cfif ratio gt 100>
								<cfset ratio = 100>
							</cfif>
						</cfif>	
							<cfset commandes.updateRatio(IDCDE_COMMANDE,ratio)>
					</cfloop>
				
					<cfreturn 999/>
					
				<cfcatch type="any">
					<cfreturn -3>
				</cfcatch>
				
				</cftry>
				
				
			</cfcase>
			
		</cfswitch>
	</cffunction>	
	
</cfcomponent> 