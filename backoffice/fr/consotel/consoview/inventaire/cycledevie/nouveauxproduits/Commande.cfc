<!---
	
	Auteur : samuel.divioka
	
	Date : 7/20/2007
	
	Description : Classe gï¿½rant les commandes

--->
<cfcomponent name="Commande" output="false">
	
	<cfset TYPE_COMMANDE = 2>
	
	<!---	
		Auteur : samuel.divioka
		
		Date : 7/20/2007
		
		Description : Crï¿½ation d'une commande
	
	--->									
	<cffunction name="create" access="public" returntype="numeric">	
		
		<cfargument name="idGroupeClient" type="numeric">
		<cfargument name="cmde" type="struct">								
		 
	  	<cfset userId = session.user.CLIENTACCESSID>
		<cfset dateLivPre = transformDate(cmde.dateLivraisonPrevue)>
		<cfset dateEff = transformDate(cmde.dateEffective)>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.I_COMMANDE">
			<cfif cmde.contactID gt 0>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact" value="#cmde.contactID#"/>
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact" null="true"/>
			</cfif>
			<cfif cmde.societeID gt 0>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact_societe" value="#cmde.societeID#"/>
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact_societe" null="true"/>
			</cfif>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_status_cde" value="#cmde.statutID#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#cmde.operateurID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_libelle_commande" value="#cmde.libelle#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_ref_client" value="#cmde.refClient#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_ref_ope" value="#cmde.refOperateur#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaires" value="#cmde.commentaire#"/>						
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_compte" value="#cmde.compteID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_sosuCompte" value="#cmde.sousCompteID#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_user_create" value="#userId#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_type_commande" value="#cmde.typeCommande#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#idGroupeClient#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_repere" value="#cmde.idGroupeCible#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_flag_repere" value="#cmde.flagCible#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_effet" value="#dateEff#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_livraison_prevue" value="#dateLivPre#">			
 			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     
		</cfstoredproc>		

		<cfreturn result/>		
	</cffunction>
	
	<cffunction name="searchNodeId" access="private" returntype="numeric">
		<cfargument name="idgroupeclient" type="numeric" required="true">
		<cfargument name="recordset" type="query" required="true">
		
		<cfloop index="i" from="1" to="#recordset.recordcount#">
			<cfif recordset['IDGROUPE_CLIENT'][i] eq idgroupeclient>
				<cfreturn recordset['NODE_ID'][i]>				
			</cfif>
		</cfloop>		
		<cfreturn  -1>
	</cffunction>
	
	<cffunction name="CreeDateFromString" access="private" returntype="date">
		<cfargument name="ladate" type="string" required="true">
		<cfset newdate = CreateDate(left(ladate,4),mid(ladate,6,2),right(ladate,2))>
		<cfreturn newdate>
	</cffunction>
	
	
	<cffunction name="createOperation" access="public" returntype="any">	
		
		<cfargument name="fake" type="numeric">
		<cfargument name="comde" type="struct">			 					
		<cfargument name="produits" type="array">
		
		<!--- <cfargument name="dateRapprochement" type="date" required="false"> --->
		
		<cfset cmde = get(comde.commandeID)>
		
		<cftry>
			<!--- <cfset dateLivPre = cmde.DATE_LIVRAISON_PREVUE>		
			<cfset dateLiv = comde.dateLivraison>
			<cfset dateEff = cmde.DATE_COMMANDE>		
			<cfset dateEnv = cmde.DATE_ENVOI>	
			<cfset dateCreation = cmde.DATE_CREATE> --->
				
			<!--- <cfset dateRapp = LsDateFormat(dateRapprochement,'dd/mm/yyyy')> --->
			
			<cfset dateLivPre = LsDateFormat(CreeDateFromString(left(cmde.DATE_LIVRAISON_PREVUE[1],10)),"dd/mm/yyyy")>		
			<cfset dateLiv = left(comde.dateLivraison,10)>
			<cfset dateEff = LsDateFormat(CreeDateFromString(left(cmde.DATE_COMMANDE[1],10)),"dd/mm/yyyy")>		
			<cfset dateEnv = LsDateFormat(CreeDateFromString(left(cmde.DATE_ENVOI[1],10)),"dd/mm/yyyy")>	
			<cfset dateCreation = LsDateFormat(CreeDateFromString(left(cmde.DATE_CREATE[1],10)),"dd/mm/yyyy")> 
		
		<cfcatch type="any">		
			<cfreturn -155>
		</cfcatch>
		</cftry>
		
		<cfif cmde.IDCDE_CONTACT_SOCIETE[1] eq "">
			<cfset idSociete = 0>
		<cfelse>
			<cfset idSociete = cmde.IDCDE_CONTACT_SOCIETE[1]>
		</cfif>
		
		<cfif cmde.IDCDE_CONTACT[1] eq "">
			<cfset idContact = 0>
		<cfelse>
			<cfset idContact = cmde.IDCDE_CONTACT[1]>			
		</cfif>
		
		
		<cfset operation = createObject("component","fr.consotel.consoview.inventaire.cycledevie.Inventaire")>		
		<cfset result = operation.enregistrerOperation(0,2,comde.libelle,1,"Commande liï¿½e : " & comde.libelle,"",0,comde.idGroupeClient,produits,
				idSociete,idContact,dateEff,comde.refClient,comde.refOperateur)>				
		
		<cfif result gt 0>
			<cfset idop = result>
			<cfset resultFinal = updateCommandeOperation(comde.commandeID,result)>	
			<cfset resultFinal = updateStatut(comde.commandeID,comde.statutID)>
			<!--- UPDATER LE CONTACT DE LA COMMANDE --->			
					
			<cfset cmde = get(comde.commandeID)>
			<cfset action = createObject("component","fr.consotel.consoview.inventaire.cycledevie.OperationAction")>
			<cfset sleeper  = createObject("java","java.lang.Thread")>
			<cfset sleeper.sleep(1000)>

			
			<!--- LANCER LA PREMIERE ACTION 
			 si avec mail 64
			 sinon 30
			 avec dateAction = cmde.dateEffective  --->		
			<cfif comde.flagMail gt 0>				
				<cfset action.doAction(idop,produits,64,29,"","",1,0,dateEnv)>	
				<cfset sleeper.sleep(1000)>			 
				<!--- LANCER LA DEUXIEME ACTION 1066 -- ex 31 --
				 avec dateAction = cmde.dateLivraison  --->		
				<cfset action.doAction(idop,produits,1066,64,"","",1,0,dateLiv)>
			<cfelse>	
				<cfset action.doAction(idop,produits,30,29,"","",1,0,dateEnv)>				 
				<!--- LANCER LA DEUXIEME ACTION 1066 -- ex 31 --
				 avec dateAction = cmde.dateLivraison  --->		
				 <cfset sleeper.sleep(1000)>
				<cfset action.doAction(idop,produits,1066,30,"","",1,0,dateLiv)>								 
			</cfif>



	
			<!--- LANCER LA TROISIEME ACTION
			avec date action = dateRapp --->
			<cfset sleeper.sleep(1000)> 
			<cfset action.doAction(idop,produits,31,1066,"","",1,0,dateLiv)>

		</cfif>
		<cfreturn result/>
	</cffunction>
	
	<cffunction name="rapprocherProduitsCommande" access="public" returntype="numeric">		
		<cfargument name="dateRapprochement" type="date">
		<cfargument name="comde" type="struct">
		<cfset idOperation = get(comde.commandeID).operationID>
		<cfset dateRap = LsDateFormat(dateRapprochement,'dd/mm/yyyy')/>
		<cfset action = createObject("component","fr.consotel.consoview.inventaire.cycledevie.OperationAction")>
		<cfset action.doAction(idOperation,produits,66,66,"","",1,0,dateRapprochement)>				
	</cffunction>	
	
	
	<cffunction name="delete" access="public" returntype="numeric">			
		<cfargument name="idCommande" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.D_COMMANDE_V2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#idCommande#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						   
		</cfstoredproc>		
		<cfreturn result/>
	</cffunction>	
		
	<cffunction name="get" access="public" returntype="query">			
		<cfargument name="idCommande" type="numeric" required="true">			
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.INFOSCOMMANDE">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idCommande" value="#idCommande#"/>				
			<cfprocresult name="result">		  
		</cfstoredproc>								
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>		
		<cfset obj.setData(result,"Demande")>					
		<cfreturn result/>
	</cffunction>	
	
	<cffunction name="getTheme" access="remote" returntype="query">							
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.L_THEME">	
			<cfprocresult name="result">		  
		</cfstoredproc>								
		<cfreturn result/>
	</cffunction>	
	
	<cffunction name="getThemebyOperateur" access="remote" returntype="query">							
		<cfargument name="idope" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.L_PRODUITS_SEGMENT">	
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idope#">
			<cfprocresult name="qResult">		  
		</cfstoredproc>
		<cfquery name="result" dbtype="query">
			select THEME_LIBELLE, IDTHEME_PRODUIT from qResult
			group by THEME_LIBELLE, IDTHEME_PRODUIT
		</cfquery>
		 						
		<cfreturn result/> 		
	</cffunction>	
	
	
	<cffunction name="getProduitsByOperateur" access="remote" returntype="query">							
		<cfargument name="idope" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.L_PRODUITS_SEGMENT">	
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idope#">
			<cfprocresult name="result">		  
		</cfstoredproc>		 		
		<cfreturn result/> 		
	</cffunction>	
	
	
	<cffunction name="update" access="remote" returntype="numeric">	
		<cfargument name="fake" type="numeric" required="true">	
		<cfargument name="cmde" type="struct" required="true" displayname="laCommande">
			
		<cfset userId = session.user.CLIENTACCESSID>
		<cfset dateLiv = (lsDateFormat(cmde.ddateLivraison,"yyyy/MM/DD"))>
		<cfset dateLivPre = (lsDateFormat(cmde.ddateLivraisonPrevue,"yyyy/MM/DD"))>
		<cfset dateEff = (lsDateFormat(cmde.ddateEffective,"yyyy/MM/DD"))>

		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.U_COMMANDE">						
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_idcmde" value="#cmde.commandeID#"/>
			<cfif cmde.contactID gt 0>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact" value="#cmde.contactID#"/>
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact" null="true"/>
			</cfif>
			<cfif cmde.societeID gt 0>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact_societe" value="#cmde.societeID#"/>
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact_societe" null="true"/>
			</cfif>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_status_cde" value="#cmde.statutID#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#cmde.operateurID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_libelle_commande" value="#cmde.libelle#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_ref_client" value="#cmde.refClient#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_ref_ope" value="#cmde.refOperateur#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaires" value="#cmde.commentaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_list_idsous_tete" value="#cmde.compteID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_list_idsous_tete" value="#cmde.sousCompteID#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_user_create" value="#userId#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_type_commande" value="#cmde.typeCommande#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#cmde.idGroupeClient#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_repere" value="#cmde.idGroupeCible#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_flag_nouveau_repere" value="#cmde.flagCible#"/>			
			<cfprocparam  cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_effet" value="#dateEff#"> 	
			<cfif trim(dateLiv) eq "1970/01/01">		
				<cfprocparam  cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_livraison" value="#dateLiv#" null="true">
			<cfelse>
				<cfprocparam  cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_livraison" value="#dateLiv#">
			</cfif>	
			<cfif trim(dateLivPre) eq "1970/01/01">
				<cfprocparam  cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_livraison_prevu" value="#dateLivPre#" null="true">
			<cfelse>
				<cfprocparam  cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_livraison_prevu" value="#dateLivPre#">
			</cfif>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     
		</cfstoredproc>
		
		<cfif cmde.boolEnvoyerMail eq true>
			
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.U_DATE_ENVOI">	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idCommande" value="#cmde.commandeID#"/>						
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_theDate" value="#LsDateFormat(now(),'yyyy/mm/dd')#"/>							
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_iflagMail" value="1"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_userId" value="#userId#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     
			</cfstoredproc>				
			
		</cfif>
		
		<cfreturn result/>		
	</cffunction>
	
	<cffunction name="updateStatut" access="public" returntype="numeric">	
		<cfargument name="idCommande" type="numeric">
		<cfargument name="newStatut" type="numeric">
		<cfset userId = session.user.CLIENTACCESSID>
		
		<cftry>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.U_STATUS">	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idCommande" value="#idCommande#"/>						
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_statut" value="#newStatut#"/>			
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_useridModif" value="#userId#"/>		
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     
			</cfstoredproc>		
		<cfcatch type="database">
			<cfthrow type="Erreur" message="Une erreur s'est produite lors de l'enregistrement du nouveaux statut" extendedinfo="Plusieurs raisons possibles ....">
		</cfcatch>
		<cfcatch type="application">
			<cfthrow type="Log" message="Votre session est terminï¿½e." extendedinfo="Plusieurs raisons possibles ....">
		</cfcatch>
		</cftry>
		<cfreturn result/>		
	</cffunction>
	
	<cffunction name="updateCommandeOperation" access="public" returntype="numeric">	
		<cfargument name="idCommande" type="numeric">
		<cfargument name="idOperation" type="numeric">		
				
		<cftry>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.U_COMMANDE_OPERATION">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idCommande" value="#idCommande#"/>						
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idOperation" value="#idOperation#"/>							
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     
			</cfstoredproc>		
		<cfcatch type="database">
			<cfthrow type="Erreur" message="Une erreur s'est produite lors de l'enregistrement du nouveaux statut" extendedinfo="Plusieurs raisons possibles ....">
		</cfcatch>
		<cfcatch type="application">
			<cfthrow type="Log" message="Votre session est terminï¿½e." extendedinfo="Plusieurs raisons possibles ....">
		</cfcatch>
		</cftry>
		<cfreturn result/>		
	</cffunction>
	
	<cffunction name="sendCommande" access="public" returntype="numeric">	
		<cfargument name="idCommande" type="numeric">
		<cfargument name="flagMail" type="numeric">
		<cfargument name="laDate" type="string">
				
		<cfset userId = session.user.CLIENTACCESSID>
		<cfset theDate = transformDate(laDate)>
		
		 
		
		<cfif flagMail gt 0>
				<cftry>
					<cfset result1 = envoyerMail(idCommande)>
				<cfcatch type="application">
					<cfthrow type="Log" message="Pb mail." extendedinfo="Plusieurs raisons possibles ....">
					<cfreturn 0>
				</cfcatch>
				</cftry>
		</cfif>
		<cftry>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.U_DATE_ENVOI">	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idCommande" value="#idCommande#"/>						
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_theDate" value="#theDate#"/>							
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_iflagMail" value="#flagMail#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_userId" value="#userId#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     
			</cfstoredproc>				
		<cfcatch type="database">
			<cfthrow type="Erreur" message="Une erreur s'est produite lors de l'enregistrement du nouveaux statut" extendedinfo="Plusieurs raisons possibles ....">
		</cfcatch>
		<cfcatch type="application">
			<cfthrow type="Log" message="Votre session est terminï¿½e." extendedinfo="Plusieurs raisons possibles ....">
		</cfcatch>
		</cftry>
		<cfset updateStatut(idCommande,2)>
		
		<cfreturn 1/>		 		
	</cffunction>
	
	<cffunction name="updateContact" access="public" returntype="numeric" >			
		<cfargument name="idCommande" type="numeric" required="true">
		<cfargument name="idConatct" type="numeric" required="true">
		<cfargument name="idSociete" type="numeric" required="true">
		
		<cfset var qUpdate="">
		
		<cftry>
			<cfquery name="qUpdate" datasource="#SESSION.OFFREDSN#" result="status">
				update
					OFFRE.CDE_COMMANDE			
				set 
					IDCDE_CONTACT = <cfqueryparam value="#idConatct#" cfsqltype="CF_SQL_INTEGER"/>,
					IDCDE_CONTACT_SOCIETE = <cfqueryparam value="#idSociete#" cfsqltype="CF_SQL_INTEGER"/>
				where
					IDCDE_COMMANDE = <cfqueryparam value="#idCommande#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>	
		
			<cfcatch type="database">
				<cfreturn -1>
			</cfcatch>		
		</cftry>
		
		
		<cfreturn 1>		
	</cffunction>
	
	<cffunction name="updateContactEnvoyerMail" access="public" returntype="numeric" >			
		<cfargument name="idCommande" type="numeric" required="true">
		<cfargument name="idConatct" type="numeric" required="true">
		<cfargument name="idSociete" type="numeric" required="true">
		
		<cfset var qUpdate="">
		
		<cftry>
		
		<cfquery name="qUpdate" datasource="#SESSION.OFFREDSN#" result="status">
			update
				OFFRE.CDE_COMMANDE			
			set 
				IDCDE_CONTACT = <cfqueryparam value="#idConatct#" cfsqltype="CF_SQL_INTEGER"/>,
				IDCDE_CONTACT_SOCIETE = <cfqueryparam value="#idSociete#" cfsqltype="CF_SQL_INTEGER"/>
			where
				IDCDE_COMMANDE = <cfqueryparam value="#idCommande#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
		
			 
		<cfset r = sendCommande(idCommande,1,LsDateFormat(now(),"dd/mm/yyyy"))>
		
		
		<cfcatch type="database">
			<cfreturn -1>
			<cfthrow message="Une erreur c'est produite lors de la mise ï¿½ jour du contact de la sociï¿½tï¿½" errorcode="INV-UDCTC-DB" type="database">
		</cfcatch>
		 
		<!--- <cfcatch type="any">
			<cfreturn -2>
			<cfthrow message="Une erreur c'est produite lors de la mise ï¿½ jour du contact de la sociï¿½tï¿½" errorcode="INV-UDCTC-DB" type="database">
		</cfcatch>  --->
		
		</cftry>
		<!--- <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE.UPDATE_CONTACT_OPERATION">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_operations" value="#idOperation#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact" value="#idConatct#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_societe" value="#idSociete#"/>
		</cfstoredproc> --->
		
		<cfreturn 1>		
	</cffunction>
	  
	<cffunction name="envoyerMail" access="public" returntype="numeric" >	
		
		<cfargument name="idCommande" type="numeric">
		<cfset commande = get(idCommande)>		
		<cfset panierObject = createObject("component","ElementCommande")>
		<cfset contactObject = createObject("component","contact")>
		<cfset societeObject = createObject("component","societe")>
		<cfset contact = contactObject.get(commande.IDCDE_CONTACT[1])>
		<cfset panier = panierObject.getProduitsCommande(commande.IDCDE_COMMANDE[1])>
		
		<cfset user = session.user.email>
		<cfset contactEmail = contact.EMAIL[1]>
		
		<cfset lg=panier.recordcount>
		
		<cfset themes = arrayNew(1)>
		<cfset produits = arrayNew(1)>
		<cfset lignes = arrayNew(1)>
		<cfset coms = arrayNew(1)>
		
		
	 
		<cfloop query="panier">			
			<cfset  arrayAppend(themes,panier.THEME_LIBELLE)>	
			<cfset  arrayAppend(produits,panier.LIBELLE_PRODUIT)>
			<cfset  arrayAppend(lignes,panier.LIBELLE_LIGNE)>
			<cfset  arrayAppend(coms,panier.COMMENTAIRES)> 			 
		</cfloop>
		
		
			<cfmail from="#user#"
					to="#contactEmail#"
					subject="Demande : #commande.LIBELLE_COMMANDE#"
					type="html"
					charset="utf-8"
					wraptext="72"
					cc="#user#"
					replyto="#user#"
					failto="support@consotel.fr"
					server="mail.consotel.fr:26">	
					
					<cfset typeDemande = commande.TYPE_COMMANDE>
					<cfset LI_THEMES = arrayToList(themes,",")>
					<cfset LI_PRODUITS = arrayToList(produits,",")>
					<cfset LI_LIGNES = arrayToList(lignes,",")>
					<cfset LI_COMS = arrayToList(coms,",")> 
					
					<cfset CIBLE = commande.IDGROUPE_REPERE>
					<cfset LIBELLE = commande.LIBELLE_COMMANDE>
					<cfset REF_CLIENT = commande.REF_CLIENT>
					<cfset REF_OPERATEUR = commande.REF_OPERATEUR>
					<cfset CMDE_COMMENTAIRES = commande.COMMENTAIRES>
					
					<cfset DATE_EFFECTIVE = LsDateFormat(CreeDateFromString(left(commande.DATE_COMMANDE,10)),"dd/mm/yyyy")>
					<cfset DATE_LIVRAISON_PRE = LsDateFormat(CreeDateFromString(left(commande.DATE_LIVRAISON_PREVUE,10)),"dd/mm/yyyy")>
					
					<cfset CONTACTID = commande.IDCDE_CONTACT>
					<cfset OPERATEURID = commande.OPERATEURID>
					<cfset SOCIETEID = commande.IDCDE_CONTACT_SOCIETE>
					<cfset FLAG_CIBLE = commande.FLAG_NOUVEAU_REPERE>
					 <cfset PERIMETRE_LIBELLE = "">  
					
					<cfinclude template="/fr/consotel/consoview/inventaire/cycledevie/nouveauxProduits/PDF/DemandeNouveauxProduitsMail.cfm">
			</cfmail>
			
		<cfreturn 1>
	</cffunction>
	
	<cffunction name="updateRatio" access="public" returntype="numeric">	
		<cfargument name="idCommande" type="numeric">
		<cfargument name="lastRatio" type="numeric">		
				
		<cftry>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.U_COMMANDE_RATIO">	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idCommande" value="#idCommande#"/>						
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_lastRatio" value="#lastRatio#"/>							
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     
			</cfstoredproc>		
		<cfcatch type="database">
			<cfthrow type="Erreur" message="Une erreur s'est produite lors de l'enregistrement du nouveaux statut" extendedinfo="Plusieurs raisons possibles ....">
		</cfcatch>
		<cfcatch type="application">
			<cfthrow type="Log" message="Votre session est terminï¿½e." extendedinfo="Plusieurs raisons possibles ....">
		</cfcatch>
		</cftry>
		<cfreturn result/>		
	</cffunction>
	
	
	<cffunction name="getListCom" access="public" returntype="query">			
		<cfargument name="idGroupeClient" type="numeric" required="true">									
		<cfargument name="idstatut" type="numeric" required="false" default="0">
		<cfargument name="idtype" type="numeric" required="false" default="0">
				 
		<!--- laListeDesCommande --->		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.GROUPECOMMANDE">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idGroupeClient" value="#idGroupeClient#"/>				
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_status" value="#idstatut#"/>				
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_tyep_commande" value="#idtype#"/>							
			<cfprocresult name="listeCommande">		  
		</cfstoredproc>		
		<cfreturn listeCommande/>			
	</cffunction>	
		
	<cffunction name="getList" access="public" returntype="query">			
		<cfargument name="idGroupeClient" type="numeric" required="true">									
		<cfargument name="idstatut" type="numeric" required="false" default="0">
		<cfargument name="idtype" type="numeric" required="false" default="0">
		  
		<!--- laListeDesCommande --->		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.GROUPECOMMANDE">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idGroupeClient" value="#idGroupeClient#"/>				
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_status" value="#idstatut#"/>				
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_type_commande" value="#idtype#"/>							
			<cfprocresult name="listeCommande">		  
		</cfstoredproc>		
		
		<cfset operations = createObject("component","fr.consotel.consoview.inventaire.cycledevie.Inventaire")>
		<cfset listeOp = operations.getListeOperation(idGroupeClient)>
				 	
		<cfquery name="listeCmdeOpe" dbtype="query">
			select * from  listeCommande,listeOp
			where listeCommande.idinv_operations = listeOp.idinv_operations
		</cfquery>	
		
		<cfreturn listeCmdeOpe/>			
	</cffunction>	
	
	<cffunction name="getUserList" access="public" returntype="query">					
		<cfargument name="idGroupeClient" type="numeric" required="true">	
		<cfargument name="idstatut" type="numeric" required="false">
		<cfargument name="idtype" type="numeric" required="false">			
		 
		<cfset userId = session.user.CLIENTACCESSID>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE.">	
			<!--- <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idGroupeClient" value="#idGroupeClient#"/>				 --->
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idUser" value="#userId#"/>
			<cfprocresult name="result">		  
		</cfstoredproc>								
		<cfreturn result/>	
	</cffunction>
	
	<!--- select listeOp.IDINV_OPERATIONS as ID,						
			listeOp.IDINV_TYPE_OPE as TYPE_COMMANDE,
			1 as RATIO,			
			IDINV_ETAT,
			listeOp.EN_COURS,			
			-1 as EN_COURS_COM,
			2 as TYPE,
			listeOp.LIBELLE_OPERATIONS as LIBELLE,
			'OP' as TYPE_LIBELLE, 
			listeOp.REFERENCE_INTERNE as REFERENCE
			from listeCommande,listeOp
			where listeCommande.idinv_operations = listeOp.idinv_operations 
		
			UNION
		
			select 
			IDCDE_COMMANDE as ID,
			TYPE_COMMANDE,			
			LAST_RATIO as RATIO,
			-1 as IDINV_ETAT,
			-1 as STATUT,
			IDCDE_STATUS_CDE as EN_COURS_COM,			
			1 as TYPE,
			LIBELLE_COMMANDE as LIBELLE,
			'COM' as TYPE_LIBELLE,
			REF_CLIENT as REFERENCE 
			from listeCommande
			where listeCommande.idcde_status_cde <> 4
 --->			
	
	<!--- ============================================================================================================ --->
	
	<cffunction name="getListCfRacine" access="public" returntype="query">	
		<cfargument name="idRacine" type="numeric" required="false">
		<cfargument name="idOperateur" type="numeric" required="false">

		<!--- 
		pkg_cv_commande.l_cf_racine_op_v1(p_idracine => :p_idracine,
                                    p_operateurid => :p_operateurid,
                                    p_retour => :p_retour);
		 --->	
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_commande_V3.l_cf_racine_op_v1">	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#idRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#idOperateur#"/>
			<cfprocresult name="result">		  
		</cfstoredproc>								
		<cfreturn result/>	
	</cffunction>
	
	<cffunction name="getListSousCompteCf" access="public" returntype="query">	
		<cfargument name="idCompte" type="numeric" required="false">	
		<!--- 
		 pkg_cv_commande.l_sous_compte_cf_v1(p_idcompe => :p_idcompe,
                                      p_retour => :p_retour);
		 --->	
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_commande_V3.l_sous_compte_cf_v1">	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcompe" value="#idCompte#"/>
			<cfprocresult name="result">		  
		</cfstoredproc>								
		<cfreturn result/>	
	</cffunction>
	
	<!--- ============================= CREATION DU WORKFLOW ==================================================== --->
	
	<!--- creation d'un numï¿½ro de ligne automatiquement 
		PROCEDURE pkg_cv_commande.G_getLine_number(p_retour OUT VARCHAR2)
	--->
	<cffunction name="getNewNumeroLigne" access="public" returntype="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_commande_V3.G_getLine_number">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="out" variable="result"/>						     
		</cfstoredproc>
		<cfreturn result/>
	</cffunction>
	
	<!--- vï¿½rification de l'inexistance du numï¿½ro de tï¿½lï¿½phone
		PROCEDURE  pkg_cv_commande.sous_tete_exists_v1(p_sous_tete => :p_sous_tete,
                                      p_retour => :p_retour);
	--->
	<cffunction name="verifierUniciteSousTete" access="public" returntype="numeric">
		<cfargument name="sous_tete" type="string" required="true">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m16.sous_tete_exists_v1">						
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_sous_tete" value="#sous_tete#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     
		</cfstoredproc>
		<cfreturn result/>
	</cffunction>
	
	<!--- Enregistrement d'une commande, creation des nouvelles sous_tetes, creation du workflow (inv_operation), creation de la premiere action,	
		pkg_cv_commande.create_workflow_produit_v1(p_idcde_contact => :p_idcde_contact,
		                                             p_idcde_contact_societe => :p_idcde_contact_societe,
		                                             p_idcde_status_cde => :p_idcde_status_cde,
		                                             p_operateurid => :p_operateurid,
		                                             p_libelle_commande => :p_libelle_commande,
		                                             p_ref_client => :p_ref_client,
		                                             p_ref_operateur => :p_ref_operateur,
		                                             p_commentaires => :p_commentaires,
		                                             p_idcompte_facturation => :p_idcompte_facturation,
		                                             p_idsous_compte => :p_idsous_compte,
		                                             p_user_create => :p_user_create,
		                                             p_type_commande => :p_type_commande,
		                                             p_idgroupe_client => :p_idgroupe_client,
		                                             p_idgroupe_repere => :p_idgroupe_repere,
		                                             p_flag_nouveau_repere => :p_flag_nouveau_repere,
		                                             p_date_commande => :p_date_commande,
		                                             p_date_livraison_prevue => :p_date_livraison_prevue,
		                                             p_idinv_type_ope => :p_idinv_type_ope,
		                                             p_libelle_operations => :p_libelle_operations,
		                                             p_en_cours => :p_en_cours,
		                                             p_commentaire_op => :p_commentaire_op,
		                                             p_listeidst => :p_listeidst,
		                                             p_listeidpc => :p_listeidpc,
		                                             p_listeidqte => :p_listeidqte,
		                                             p_listecomment => :p_listecomment,
		                                             p_retour => :p_retour);
	--->
	<cffunction name="createCommandeSousTeteAndWorkFlow" access="public" returntype="numeric">
		<cfargument name="commande" type="struct" required="true">			   
	   	<cfargument name="tablisteSt" type="array" required="true">
	   	<cfargument name="tablisteIdPc" type="array" required="true">
       	<cfargument name="tablisteQte" type="array" required="true">
	   	<cfargument name="tablisteComs" type="array" required="true">		
		
		<cfset listeSt = arrayToList(tablisteSt,",")>
		<cfset listeIdPc = arrayToList(tablisteIdPc,",")>
		<cfset listeQte = arrayToList(tablisteQte,",")>
		<cfset listeComs = arrayToList(tablisteComs,",")>		
		 	  	 
		<cfset dateLivPre = transformDate(commande.dateLivraisonPrevue)>
		<cfset dateEff = transformDate(commande.dateEffective)>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_commande_V3.create_workflow_produit_v1">						
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idcde_contact" value="#commande.contactID#" null="#iif((commande.contactID eq 0), de("yes"), de("no"))#"/>
		  	<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idcde_contact_societe" value="#commande.societeID#" null="#iif((commande.societeID eq 0), de("yes"), de("no"))#"/>
		  	<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idcde_status_cde" value="#commande.statutID#" />		  			  	
		  	<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#commande.operateurID#" />
		  	<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_libelle_commande" value="#commande.libelle#"/>
		  	<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_ref_client" value="#commande.refClient#"/>
		  	<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_ref_operateur" value="#commande.refOperateur#"/>
		  	<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_commentaires" value="#commande.commentaire#"/>		  	
		  	<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idcompte_facturation" value="#commande.compteID#"/>
		  	<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idsous_compte" value="#commande.sousCompteID#"/>
		  	<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_user_create" value="#commande.idUserCreate#"/>
		  	<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_type_commande" value="#commande.typeCommande#"/>
		  	<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idgroupe_client" value="#commande.idGroupeClient#"/>
		  	<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idgroupe_repere" value="#commande.idGroupeCible#"/>
		  	<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_flag_nouveau_repere" value="#commande.flagCible#"/>		  	
		  	<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_commande" value="#dateEff#"/>
		  	<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_livraison_prevue" value="#dateLivPre#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idinv_type_ope" value="#TYPE_COMMANDE#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_libelle_operations" value="#commande.libelle#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_en_cours" value="#1#"/><!--- 1 = en cours 0 = cloturï¿½--->
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_commentaire_op" value="#commande.commentaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_listest" value="#listeSt#"/>         
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_listeidpc" value="#listeIdPc#"/>      
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_listeidqte" value="#listeQte#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_listecomment" value="#listeComs#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="out" variable="result"/>	  
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
		
	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="false" default="">		
		<cfif len(oldDateString) neq 0>
			<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
			<cfreturn newDateString> 
		<cfelse>
			<cfreturn oldDateString> 
		</cfif>		
	</cffunction>
		
</cfcomponent>