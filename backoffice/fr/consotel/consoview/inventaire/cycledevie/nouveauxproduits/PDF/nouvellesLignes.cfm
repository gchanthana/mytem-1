<table cellpadding="0" cellspacing="0" width="100%" align="left" width="100%" >
	<tr>			
		<td/>
		<td width="100"/>
		<td align="left" class="td51"><b>Contact :</b><blockquote>
			<cfoutput>
			<b>#info.civilite# #info.prenom# #info.nom#</b><br>
			 
			<cfif len(info.telephone[1]) gt 0>
				tél. #info.telephone[1]#<br>										
			</cfif>
			<cfif len(info.fax[1]) gt 0>
				fax  #info.fax[1]#<br>										
			</cfif>
			<cfif len(info.email[1]) gt 0>
				email #info.email[1]#<br>										
			</cfif>
			<cfif len(info.raison_sociale[1]) gt 0>
				<br><b>#info.raison_sociale[1]#</b><br>										
			</cfif>
			<cfif len(info.adresse1[1]) gt 0>
				#info.adresse1[1]#<br>										
			</cfif>
			<cfif len(info.adresse2[1]) gt 0>
				#info.adresse2[1]#<br>										
			</cfif>
			<cfif len(info.zipcode[1]) gt 0>
				#info.zipcode[1]#<br>										
			</cfif>
			<cfif len(info.pays[1]) gt 0>
				#info.pays[1]#<br>										
			</cfif></blockquote>					
			
			<blockquote>
			<cfif len(info.pays[1]) gt 0> 
				<b>Opérateur : </b>#info.operateur#										
			</cfif>
			</blockquote>
			</cfoutput>
		</td>	
	</tr>				
	<tr>
		<td height="30"/>
		<td height="30"/>	
		<td height="30"/>	
	</tr>
	<tr>		
		<td align="left" class="td51" valign="top">
			<cfif len(LIBELLE) gt 0>
				<b>Libellé de la commande : </b>&nbsp;<cfoutput>#LIBELLE#</cfoutput><br><br>								
			</cfif>
			<cfif len(REF_CLIENT) gt 0>
				<b>Référence client : </b>&nbsp;<cfoutput>#REF_CLIENT#</cfoutput><br><br>
			</cfif>
			<cfif len(REF_OPERATEUR) gt 0>	
				<b>Référence opérateur : </b>&nbsp;<cfoutput>#REF_OPERATEUR#</cfoutput><br><br>		
			</cfif>
			<cfif len(CMDE_COMMENTAIRES) gt 0>	
				<b>Commentaire : </b>&nbsp;<cfoutput>#CMDE_COMMENTAIRES#</cfoutput><br><br>		
			</cfif>
			<cfif len(cible) gt 0>	
				<b>cible dans l'organisation : </b>&nbsp;<cfoutput>#cible#<br><br></cfoutput>		
			</cfif>
			<cfif nouvelleCible eq 1>	
				<cfoutput><b>Pour mémoire, ces lignes devront étre affectées é un nouveau noeud sous l'organisation cible<br><br></cfoutput>		
			</cfif>
		</td>	
		<td width="100"/>
		<td align="left" height="20" class="td51" valign="top">			
			<cfoutput>				
			<b>Date de la commande : </b>&nbsp;#DATE_EFFECTIVE#<br><br>
			<b>Date livraison annoncée : </b>&nbsp;#DATE_LIVRAISON_PRE#<br><br>
			</cfoutput>				
		<td>
	</tr>
	<tr>	
		<td align="left"  height="100" colspan="3">		
	</tr>		
</table>
<table align="left" width="100%">
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="left" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><cfoutput>Nouvelles lignes demandées</cfoutput></strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="left" cellpadding="0" width="100%">
							<tr align="left">																
								<td>&nbsp;Identifiant de la ligne&nbsp;</th>
								<td>&nbsp;Théme&nbsp;</td>									
								<td>&nbsp;Produit&nbsp;</td>																																	
								<td>&nbsp;Commentaire&nbsp;</td>																												
							</tr>

<cfset numRow = 1>
<cfset lenght = arraylen(lignes)> 
<cfoutput>	
	<cfloop index="i" from="1" to="#lenght#">
	<cfif (numRow mod 28) eq 0>								
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>
<table align="left" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="left" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong>Nouvelles lignes demandées</strong></td>
				</tr>
				<tr>
					<td valign="top">
	 					<table cellspacing="0" class="menuconsoview" border="0" align="left" cellpadding="0" width="100%">
							<tr align="left">
								<td>&nbsp;Identifiant de la ligne&nbsp;</td>
								<td>&nbsp;Théme&nbsp;</td>									
								<td>&nbsp;Produit&nbsp;</td>																																	
								<td>&nbsp;Commentaire&nbsp;</td>																												
							</tr>							
		<cfset numRow = numRow +1>
	<cfelse>
						<cfif (numRow mod 2) eq 0>
							<tr>
								<td class="grid_normal" align="left" valign="top">&nbsp;#lignes[i]#&nbsp;</td>
								<td class="grid_normal" align="left" valign="top">&nbsp;#themes[i]#&nbsp;</td>
								<td class="grid_normal" align="left" valign="top">&nbsp;#produits[i]#&nbsp;</td>
								<td class="grid_normal" align="left" valign="top">&nbsp;#commentaires[i]#&nbsp;</td>
							</tr>
						<cfelse>
							<tr>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#lignes[i]#&nbsp;</td>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#themes[i]#&nbsp;</td>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#produits[i]#&nbsp;</td>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#commentaires[i]#&nbsp;</td>
							</tr>
						</cfif>							 												
		<cfset numRow = numRow +1>		
	</cfif>	
	</cfloop>
</cfoutput>

								 						
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>