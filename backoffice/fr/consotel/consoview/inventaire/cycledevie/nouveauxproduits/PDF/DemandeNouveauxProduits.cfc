<cfcomponent name="DemandeNouveauxProduits" output="true">	
	<!--- 1 pour nouvelles lignes 
		  2 pour nouveaux produits
	--->	
	<cffunction name="getNodeInfos" access="remote" returntype="struct">
		<cfargument name="accessId" required="true" type="numeric">
		<cfargument name="nodeId" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.detail_noeud_v2">
			<cfprocparam  value="#nodeId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#accessId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNodeInfos">
		</cfstoredproc>
		<cfset nodeInfos=structNew()>
		<cfset nodeInfos.INFOS_STATUS = qGetNodeInfos.recordcount>
		<cfif nodeInfos.INFOS_STATUS GT 0>
			<cfset nodeInfos.NID = qGetNodeInfos['IDGROUPE_CLIENT'][1]>
			<cfset nodeInfos.LBL = qGetNodeInfos['LIBELLE_GROUPE_CLIENT'][1]>
			<cfif nodeInfos.NID EQ SESSION.PERIMETRE.ID_GROUPE>
				<cfset nodeInfos.TYPE_LOGIQUE = "ROOT">
			<cfelse>
				<cfset nodeInfos.TYPE_LOGIQUE = qGetNodeInfos['TYPE_ORGA'][1]>
			</cfif>
			<cfset nodeInfos.TYPE_PERIMETRE = qGetNodeInfos['TYPE_PERIMETRE'][1]>
			<cfset nodeInfos.STC = qGetNodeInfos['IDGROUPE_CLIENT'][1]>
			<cfset nodeInfos.CODE_STYLE = qGetNodeInfos['CODE_STYLE'][1]>
			<cfset nodeInfos.OPERATEURID = VAL(qGetNodeInfos['OPERATEURID'][1])>
			
			<cfset nodeInfos.MODULE_FACTURATION = qGetNodeInfos['MODULE_FACTURATION'][1]>
			
			<cfset nodeInfos.MODULE_USAGE = qGetNodeInfos['MODULE_USAGE'][1]>
			
			<cfset nodeInfos.MODULE_WORKFLOW = qGetNodeInfos['MODULE_WORKFLOW'][1]>
			<cfset nodeInfos.MODULE_FIXE_DATA = qGetNodeInfos['MODULE_FIXE_DATA'][1]>
			<cfset nodeInfos.MODULE_MOBILE = qGetNodeInfos['MODULE_MOBILE'][1]>
			<cfset nodeInfos.MODULE_GESTION_ORG = qGetNodeInfos['MODULE_GESTION_ORG'][1]>
			<cfset nodeInfos.MODULE_GESTION_LOGIN = qGetNodeInfos['MODULE_GESTION_LOGIN'][1]>
			<cfset nodeInfos.DROIT_GESTION_FOURNIS = qGetNodeInfos['DROIT_GESTION_FOURNIS'][1]>
			<!--- For compatibility with Flex NodeInfos Class --->
			<cfset nodeInfos.FACT = nodeInfos.MODULE_FACTURATION>
			<cfset nodeInfos.GEST = nodeInfos.MODULE_FIXE_DATA + nodeInfos.MODULE_MOBILE +
																	nodeInfos.MODULE_WORKFLOW>
			<cfset nodeInfos.STRUCT = nodeInfos.MODULE_GESTION_ORG + nodeInfos.MODULE_GESTION_LOGIN>
			<cfset nodeInfos.USG = nodeInfos.MODULE_USAGE>
		</cfif>
		<cfreturn nodeInfos>
	</cffunction>		
	
	<cffunction name="afficher" access="public" output="true"> 			
		<cfargument name="typeDemande" type="numeric">		
		
		<cfset typeDmde = typeDemande>
		<cfset themes = listToArray(LI_THEMES)>
		<cfset Produits = listToArray(LI_PRODUITS)>
		<cfset lignes = listToArray(LI_LIGNES)>
		<cfset commentaires = listToArray(LI_COMS)>
		<cfset cibleObject = getNodeInfos(session.user.clientAccessId,CIBLE)>	
		<cfif structisempty(cibleObject)  >
			<cfset cible = cibleObject.LBL>
		<cfelse>
			<cfset cible = "">
		</cfif>
		<cfset nouvelleCible = FLAG_CIBLE>		
		
		<cfswitch expression="#typeDmde#">
			<cfcase value="1">
				<cfset titre="Demande de nouvelles Lignes">
			</cfcase>
			
			<cfcase value="2">
				<cfset titre="Demande de nouveuax produits sur lignes existantes">
			</cfcase>
			
		</cfswitch>
		
		<cfquery name="info" datasource="#SESSION.OFFREDSN#">
			select 
				 cs.raison_sociale,
				 cs.adresse1,
				 cs.adresse2,
				 cs.commune,
				 cs.zipcode,
				 cs.telephone,
				 cs.fax,       
				 cc.civilite,
				 cc.nom,
				 cc.prenom,
				 cc.email,
				 pc.pays,
				 o.nom as operateur
			from cde_contact_societe cs,
			     pays_consotel pc,
			     cde_contact cc,
			     operateur o
			where
			     cs.idcde_contact_societe = #SOCIETEID#
			and
			     cs.paysconsotelid = pc.paysconsotelid
			and
			     cc.idcde_contact = #CONTACTID#	
			and
			     o.operateurid = #OPERATEURID#	
		</cfquery>
		
		<cfset raison_sociale = PERIMETRE_LIBELLE>			
		<cfset nom = session.user.nom>	
		<cfset prenom = session.user.prenom>	 
		<cfset email = session.user.email>							

		
		<!--- Rapport Flash et pdf --->
		<cfcontent type="FlashPaper/pdf"> 
		<cfheader name="Content-Disposition" value="inline;filename=Demande_de_nouvelles_lignes.pdf">
		<cfdocument format="PDF" backgroundvisible="yes" fontembed = "yes" unit="CM" margintop="5" orientation="portrait">							
			<cfoutput> 
				<html>
					<head>									
					</head>									
					<body>
					<style>
						@page { 
							size:portrait; margin-top:2cm; margin-bottom:2cm; margin-left:1cm; margin-right:1cm;
						 }
						 
						html body {
						  margin		: 0;
						  padding		: 0;
						  background	: ##FFFFFF;
						  font			: x-small Verdana,Georgia, Sans-serif;
						  voice-family	: "\"}\""; voice-family:inherit;
						  font-size: small;
						  } html>body {
							font-size	: small;
						}
						
						.cadre {
							border-color:##FFFFFF;
							border-style:solid;
							border-collapse:collapse;
							
							border-top-width:1px;
							border-left-width:1px;
							border-right-width:1px;
							border-bottom-width:1px;
						}
						
						.cadre1{
							background		: rgb(255,255,255);
							border-bottom	:1px solid rgb(0,0,0);
							border-left		:1px solid rgb(0,0,0);
							border-right	:1px solid rgb(0,0,0);
							border-top		:1px solid rgb(0,0,0);
							border-collapse : collapse;
						}
						
						table .menuconsoview
						{
							border 		: none;
							background	: transparent;
							font		: normal 10px Tahoma,Arial, sans-serif;
							color		: ##005EBB;
						}
						
						.menuconsoview caption{
							background		: ##036 ;
							color			: white;
							font			: bold 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							padding			: 4px;
							letter-spacing	: 1px;
						
						}
						
						.menuconsoview th{
							background   	: ##FDFCE1 ;
							color        	: ##000000;
							font        	: normal 12px EurostileBold,verdana,arial,helvetica,serif;
							padding      	: 3px;
							border-collapse	: collapse;
						}
						
						.menuconsoview .th_bottom{
							vertical-align	: left;
							padding			: 3px;
							border-bottom	: 1px solid ##808080;
							font			: normal 12px Tahoma,Arial, sans-serif;
							color        	: ##036;
							background		: ##99CCFF;
						}
						
						.menuconsoview .th_top{
							vertical-align	: left;
							padding			:3px;
							border-top		: 1px solid ##808080;
							font			: normal 12px Tahoma,Arial, sans-serif;
							color       	: ##036;
							background		: ##99CCFF;
						}
						
						.menuconsoview .grid_normal{
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: normal 10px Tahoma,Arial, sans-serif;
							padding		: 2px;
						}
						
						.menuconsoview .grid_normal_alt{
							background	: ##eceae6;
							color		: rgb(0 , 0, 0);
							font		: normal 10px Tahoma,Arial, sans-serif;
							padding		: 2px;
						}
						
						.menuconsoview .grid_small{
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: normal 10px Tahoma,Arial, sans-serif;
							padding		: 0px;
						}
						
						.menuconsoview .grid_medium {
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: normal 10px Tahoma,Arial, sans-serif;
							padding-top		: 1px;
						}
						
						.menuconsoview .grid_total{
							background	: ##008ACC;
							color		: rgb(0 , 0, 0);
							font		: normal 10px Tahoma,Arial, sans-serif;
							padding-top	: 2px;
						}
						
						.menuconsoview .grid_bold{
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: bold 10px Tahoma,Arial, sans-serif;
							padding		: 0px;
						}
						
						/* caption impossible pour liste des produits */
						.td5{
							padding		:5px;
							background	:##F4F188;
							color		:rgb(0,0,0);
							font		:normal 12px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
						}
						
						.td51{
							padding		:5px;
							background	:##FDFCE1;
							color		:rgb(0,0,0);
							font		:normal 8px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
							border		:none 1px; 
							padding		: 2px;
						}
					</style>
					<cfdocumentitem type="header">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
							<tr>
								<td align="left" width="40%" height="50"><img src="/fr/consotel/consoview/images/consoview_new.gif" height="40"></td>
								<td width="17%">&nbsp;</td>
								<td width="3%" bgcolor="F4F188">&nbsp;</td>
								<td align="right" bgcolor="FDFCE1" width="40%"><font face="Arial" size="2">#titre#</font></td>
							</tr>
							<tr><td height="40">&nbsp;</td></tr>
							<tr>
								<td colspan="3">
									<font face="Tahoma" size="2"><strong>
										#nom# #prenom#</strong>
										<br>#email#</font>
								</td>																
							</tr>
							<tr> 
								<td colspan="4">
									<font face="Tahoma" size="1">
										<u>Date:</u>
										&nbsp;#Lsdateformat(now(), 'dd mmmm yyyy')# 
									</font>
								</td>
							</tr>
						</table>
						
					</cfdocumentitem>	
					
					<cfswitch expression="#typeDmde#">
						<cfcase value="1">
							<cfinclude template="nouvellesLignes.cfm">
						</cfcase>
						
						<cfcase value="2">
							<cfinclude template="nouveauxProduits.cfm">
						</cfcase>
						
					</cfswitch>
					
					
					<cfdocumentitem type="footer">
						<style>
						.tfooter {
							color: rgb(0 , 0, 0);
							font: normal 11px Tahoma,Arial, sans-serif;
						}
						</style>
						<center>
						<table border="0" cellpadding="0" cellspacing="0" width="560" align="center">
							<tr>
								<td align="center" class="tfooter" width="560">page #cfdocument.currentpagenumber# / #cfdocument.totalpagecount#</td>
							</tr>
						</table>
						</center>
					</cfdocumentitem>			
					</body>
				</html> 
			</cfoutput>			
		</cfdocument>		
	</cffunction>
</cfcomponent>