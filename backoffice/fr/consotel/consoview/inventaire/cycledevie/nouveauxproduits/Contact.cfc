<cfcomponent name="contact" output="false">		
	<cffunction name="create" access="public" returntype="numeric">	
		<cfargument name="idGroupeClient" type="numeric">
		<cfargument name="ct" type="struct">	
	 
		<cfset userId = session.user.CLIENTACCESSID>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.I_CONTACT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact_societe" value="#ct.societeID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_civilite" value="#ct.civilite#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_nom" value="#ct.nom#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_prenom" value="#ct.prenom#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_email" value="#ct.email#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_adresse1" value="-"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_adress2" value="-"/>						
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commune" value="-"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_zipcode" value="-"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_fax" value="#ct.telephone#"/>		
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_fax" value="#ct.fax#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_user_create" value="#userId#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#idGroupeClient#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     					
		</cfstoredproc>		
		<cfreturn result/>		
	</cffunction>
	
	<cffunction name="delete" access="public" returntype="numeric">			
		<cfargument name="idContact" type="numeric">		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE.D_CONTACT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idContact" value="#idContact#"/>						
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     						     
		</cfstoredproc>				
		<cfreturn result/>	
	</cffunction>	
		
	<cffunction name="get" access="public" returntype="query">			
		<cfargument name="idContact" type="numeric">								
		<cfquery name="qresult" datasource="#SESSION.OFFREDSN#">
			select cc.*
			from cde_contact cc
			where cc.idcde_contact = #idContact#
		</cfquery>			
		<cfreturn qresult/>
	</cffunction>	
	
	<cffunction name="update" access="public" returntype="numeric">	
		<cfargument name="fake" type="numeric">
		<cfargument name="ct" type="struct">			
		<cfset userId = session.user.CLIENTACCESSID>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.U_CONTACT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact" value="#ct.contactID#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact_societe" value="#ct.societeID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_civilite" value="#ct.civilite#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_nom" value="#ct.nom#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_prenom" value="#ct.prenom#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_email" value="#ct.email#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_adresse1" value="-"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_adress2" value="-"/>						
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commune" value="-"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_zipcode" value="-"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_tel" value="#ct.telephone#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_fax" value="#ct.fax#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_user_create" value="#userId#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#ct.idGroupe#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     					
		</cfstoredproc>		
		<cfreturn result/>	
	</cffunction>
	
	<cffunction name="getList" access="public" returntype="query">			
		<cfargument name="idGroupeClient" type="numeric" required="true">	
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.L_CONTACTS">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idGroupeClient" value="#idGroupeClient#"/>							
			<cfprocresult name="result">		  
		</cfstoredproc>								
		<cfreturn result/>	
	</cffunction>
	
	<cffunction name="getSocieteList" access="public" returntype="query">					
		<cfargument name="societeId" type="numeric" required="true">					
		<cfquery name="qresult" datasource="#SESSION.OFFREDSN#">
			select cc.*
			from cde_contact cc
			where cc.idcde_contact_societe = #societeId#
		</cfquery>			
		<!--- <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE.L_CONTACTS">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idGroupeClient" value="#idGroupeClient#"/>							
			 <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_userId" value="#userId#"/>  
			<cfprocresult name="result">		  
		</cfstoredproc> --->
		<cfreturn qresult/>				
	</cffunction>			
</cfcomponent>