<cfcomponent name="Journaux" displayname="Journaux">

<!--- :::::::::::::::::::::: Ressources non facturÃ©es :::::::::::::::::::::::::::::::::::
	:: Pour un groupe de lignes et ses "enfants" ,la liste des produits de l'inventaire non facturÃ©s 
	:: a la date du jour. (Dans_inventaire = 1)
	:: param in : idGroupe_client
	:: 		 out: sous_tete,
	::			  idsous_tete,
	::			  nom_operateur,
	::			  operateurid,	
	::			  idinventaire_produit,
	::			  libelle_produit,
	::
	::			  idInv_operation,(si dans opÃ©ration en cours)
	::			  libelle_opÃ©ration,
	::			  type_operation,
	::			  numero_operations,
	::			  
	::			  idinv_etat,
	::			  libelle_etat	
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getListeRessourcesNonFacturees" access="public" returntype="query" output="false">	
		<cfargument name="id" type="numeric" required="true">
		<cfargument name="ladate" type="string" required="true">
			
	 
		<cfset dateRef = transformDate(ladate)>					
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.RESSOURCESNONFACTUREES">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#id#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date" value="#dateRef#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>	
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>		
		<cfset obj.setData(p_result,"journal")>			
		<cfreturn p_result/>					
	</cffunction>	
	
	<!--- :::::::::::::::::::::: Facturation hors inventaire :::::::::::::::::::::::::::::::::::
	:: Pour un groupe de lignes et ses "enfants" : la liste des produits facturÃ©s qui sont hors inventaire 
	:: a la date du jour.(c.a.d. les produits qui, dans inv_operation_action, ont le parametre DANS_INVENTAIRE ï¿½ 0
	:: et qui apparaissent dans une ligne de facturation)
	::
	:: param in : idGroupe_client
	:: 		 out: sous_tete,
	::			  idsous_tete,
	::			  nom_operateur,
	::			  operateurid,					  
	::			  idinventaire_produit,
	::			  libelle_produit,
	::			  montant_facture,
	::			  date_facturation,
	::			  		
	::			  idInv_operation,(si dans opÃ©ration en cours)
	::			  libelle_opÃ©ration,
	::			  type_operation,
	::			  numero_operations,
	::			  			  
	::			  idinv_etat,
	::			  libelle_etat
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getListeRessourcesFactureesHorsInvt" access="public" returntype="query" output="false">	
		<cfargument name="id" type="numeric" required="true">	
		<cfargument name="ladate" type="string" required="true">
			
	 
		<cfset dateRef = transformDate(ladate)>											
		
		<!--- <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.RESSOURCESHORSINV">
		</cfstoredproc>	 --->
		
		<cfset p_result = queryNew("LIGNE")>
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>		
		<cfset obj.setData(p_result,"journal")>			
		 					
		<cfreturn p_result/>			
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/16/2007
		
		Description : Produits Hors Inventaire FacturÃ©s (V2)
	
	--->
	<cffunction name="getListeRessourcesHorsInvtFacturees" access="public" returntype="query" output="false">	
		<cfargument name="idGroupeClient" type="numeric" required="true">	
		<cfset idGroupeCli = session.perimetre.ID_PERIMETRE>
		
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.PRODUITS_HORSINV_FACTUREE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idGroupeCli#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>
	 			
		<cfreturn p_result/>			
	</cffunction>
		
	
	<!--- :::::::::::::::::::::: Historique du noeud :::::::::::::::::::::::::::::::::::
	:: PKG_CV_CYCLEVIE_V3.HISTORIQUE_NOEUD
	:: Pour un idgroupe_client ou idgroupe_racine et ses "enfants" : Historique de toutes les ressources 
	:: jusqu'ï¿½ la date passÃ©e en param
	::
	:: param in : idGroupe_client ou idGroupe_racine,
	::			  date,
	::		
	:: 		 out: sous_tete,
	::			  idsous_tete,
	::			  nom_operateur,
	::			  operateurid,					  
	::			  idinventaire_produit,
	::			  libelle_produit,
	::			  		
	::			  idInv_operation,
	::			  libelle_opÃ©ration,
	::			  type_operation,	
	::			  contact_operation (nom prenom)
	::			  raison_sociale(societe)
	::	  
	::			  libelle_action,
	::			  date_action,
	::			  dans_inventaire,			
	::			  			  
	::			  idinv_etat,
	::			  libelle_etat,
	::				
	::			  
	::						
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getHistoriqueNoeud" access="public" returntype="query" output="false">	
		<cfargument name="idGpe" type="numeric" required="true">	
		<cfargument name="ladate" type="string" required="false">
		
		<cfset dateRef = transformDate(ladate)>	
		<cfset p_result = queryNew("LIGNE,LIBELLE_PRODUIT,ACTION,DATE_ACTION,DANS_INVENTAIRE,ETAT")>
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>		
		<cfset obj.setData(p_result,"journal")>
		<cfset session.datainventaire.noeud = createObject("component","fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.PDF.DemandeNouveauxProduits").getNodeInfos(session.user.clientAccessId,idGpe)>
		<cfreturn p_result/>			
	</cffunction>	
	
	
	<!--- :::::::::::::::::::::: Facturation hors inventaire :::::::::::::::::::::::::::::::::::
	:: Liste des demandes avec retard pour un groupe_vlient
	:: 
	:: Une demande est en retard si son statut est 'EN_COURS' (IDCDE_STATUS_CDE = 1) 
	:: et sa date de livraison prÃ©visionnelle est strictement inferieur ï¿½ la date du jour
	:: 
	::
	:: param in : idGroupe_client
	:: 		 out: libelle_commande,
	::			  idcde_commande,
	::			  nomOperateur,
	::			  (nom prenom) as contact,					  
	::			  date_commande,
	::			  date_livraison_prevu,
	::			  commentaires,
	::			  
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getDemandeAvecRetard" access="public" returntype="query" output="false">	
		<cfargument name="id" type="numeric" required="true">				
		
		 
							
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.DEMANDE_RETARDLIVRAISON">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#id#"/>		
			<cfprocresult name="p_result"/>        
		</cfstoredproc>	
		
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>		
		<cfset obj.setData(p_result,"journal")>				
		
		<cfreturn p_result/>			
	</cffunction>	
	
	
	
	<!--- old --->
	
	
	<!--- :::::::::::::::::::::: Inventaire complet :::::::::::::::::::::::::::::::::::
	:: Pour un groupe de ligne la liste des produits de l'inventaire (DANS_INVENTAIRE = 1) a la date passÃ©e en param
	:: param in : idgroupe_client, 		
	:: 		 out: sous_tete,
	::			  idsous_tete,
	::			  nom_operateur,
	::			  operateurid,					  
	::			  idinventaire_produit,
	::			  libelle_produit,
	::			  prix_unitaire,			  
	::			  		
	::			  idInv_operation, (si dans opÃ©ration en cours)
	::			  libelle_opÃ©ration,
	::			  type_operation,
	::			  			  
	::			  idinv_etat,
	::			  libelle_etat
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getListeInventaire" access="public" returntype="query" output="false">	
		<cfargument name="id" type="numeric" required="true">	
		<cfargument name="date" type="string" required="true">
		<cfset laDate = transformDate(date)>
	 			
		<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.LISTEINVENTAIRE_V2" blockfactor="100">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#id#"/>
			<cfprocparam cfsqltype="CF_SQL_vARCHAR"  type="in" variable="p_date" value="#laDate#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>			
		
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>		
		<cfset message = "Etat de l'inventaire au " & date>
		<cfset obj.setAnyData(message,"INFO_FACTURE")>	
		
		<cfreturn p_result/>				
	</cffunction>	
	
	
	<!--- :::::::::::::::::::::: Reclamations en cours :::::::::::::::::::::::::::::::::::
	:: Pour un groupe de lignes et ses "enfants" la liste des des operations de reclamation qui sont en cours 
	:: c.a.d les operations qui ont le flag EN_COURS ï¿½ 1
	:: param in : idgroupe_client,
	:: 		 out: date_creation,			  		 		
	::			  idInv_operation,
	::			  libelle_opÃ©ration,
	::			  reference_interne,	
	::			  idinv_type_op,
	::			  type_operation,
	::			  numero_operations,	::			  
	::			  idinv_etat,
	::			  libelle_etat,
	::
	::			  non_utilisateur,prenom_utilisateur
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getListeOperationReclamationsEnCours" access="public" returntype="query" output="false">	
		<cfargument name="id" type="numeric" required="true">	
	 				
		<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.OPERATIONRECLAMATIONSENCOURS">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#id#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>		
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>					
		<cfset message = " ">
		<cfset obj.setAnyData(message,"INFO_FACTURE")>	
		<cfreturn p_result/>			
	</cffunction>
	
	<!--- :::::::::::::::::::::: Operation avec retard :::::::::::::::::::::::::::::::::::
	:: Pour un groupe de lignes et ses "enfants" la liste des operations en retard
	:: calcul : flag_retard = (date_du_jour > date_derniere_action + duree_etat)?1:0  
	:: param in : idgroupe_client,
	:: 		 out: flag_retard, 
	::			  nb_jour_retard, 
	::			  
	::			  date_creation,			  		 		
	::			  idInv_operation,
	::			  libelle_opÃ©ration,
	::			  reference_interne,	
	::			  idinv_type_op,
	::			  type_operation,
	::			  numero_operations,
	::			  
	::			  idinv_etat,
	::			  libelle_etat,
	::
	::			  non_utilisateur,prenom_utilisateur
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getListeOperationEnRetard" access="public" returntype="query" output="false">	
		<cfargument name="id" type="numeric" required="true">	
		 				

		<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.OPERATIONENRETARD">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#id#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>			
		
		<cfquery name="formatedResult" dbtype="query">
			select * from p_result
			where RETARD > 0
		</cfquery>
		
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>		
		<cfset obj.setData(formatedResult,"journal")>	
		<cfset message = " ">
		<cfset obj.setAnyData(message,"INFO_FACTURE")>	
		<cfreturn formatedResult/>	
	</cffunction>	
	

	<!--- :::::::::::::::::::::: Ressources livrÃ©es dans l'inventaire non facturÃ©es :::::::::::::::::::::::::::::::::::
	:: 
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getRessourcesInventaireNonFacturee" access="public" returntype="query" output="false">	
		<cfargument name="id" type="numeric" required="true">	
		<cfargument name="dateDeb" type="String" required="true">
		<cfargument name="dateFin" type="String" required="true">	
			
		<cfset dateDebut = transformDate(dateDeb)>	
		<cfset dateFinal = transformDate(dateFin)>		
		 					
		<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.LIVRES_DANS_INV_NONFACT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#id#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_dateDeb" value="#dateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_dateFin" value="#dateFinal#"/>			
			<cfprocresult name="p_result"/>        
		</cfstoredproc>			
				
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>		
		<cfset obj.setData(p_result,"journal")>		
		<cfset message = "Factures Ã©mises du " & dateDeb & " au " & dateFin>
		<cfset obj.setAnyData(message,"INFO_FACTURE")>	
		<cfreturn p_result/>	
		
		<cfreturn p_result/>				
	</cffunction>	
	
	<!--- :::::::::::::::::::::: Ressources rÃ©siliÃ©es toujours facturÃ©es :::::::::::::::::::::::::::::::::::
	:: On prendra toutes les factures 
	:: dont la date de dÃ©but de periode de facturation est posterieur ï¿½ la date de rÃ©siliation
	:: contenant le produit et dont ce produit n'est pas dans une operation courante
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getRessourcesResilieesTjrsFacturees" access="public" returntype="query" output="false">	
		<cfargument name="id" type="numeric" required="true">
		
			
		 					
		<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.RESILIEE_HORSINV_FACTUREE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#id#"/>			
			<cfprocresult name="p_result"/>        
		</cfstoredproc>			
				
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>		
		<cfset obj.setData(p_result,"journal")>	
		<cfset message = " ">
		<cfset obj.setAnyData(message,"INFO_FACTURE")>	
		<cfreturn p_result/>
			
		<cfreturn p_result/>				
	</cffunction>
	
	<!--- :::::::::::::::::::::: Ressources facturÃ©es non livrÃ©es :::::::::::::::::::::::::::::::::::
	:: 
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getRessourcesFactureesNonLivree" access="public" returntype="query" output="false">	
		<cfargument name="id" type="numeric" required="true">	
		<cfargument name="dateDeb" type="string" required="true">
		<cfargument name="dateFi" type="string" required="true">
		
		 
		<cfset dateDebut = transformDate(dateDeb)>
		<cfset dateFin = transformDate(dateFi)>		
						
		<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.FACTUREES_NON_LIVREES">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#id#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedeb" value="#dateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#dateFin#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>	
		
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>		
		<cfset obj.setData(p_result,"journal")>			
		<cfset message = "Factures Ã©mises du " & dateDeb & " au " & dateFi>
		<cfset obj.setAnyData(message,"INFO_FACTURE")>	
		<cfreturn p_result/>	
		
		<cfreturn p_result/>			
	</cffunction>
	
	<!--- :::::::::::::::::::::: Consommations hors inventaire :::::::::::::::::::::::::::::::::::
	:: 
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getConsommationsHorsInventaire" access="public" returntype="query" output="false">
		<cfargument name="id" type="numeric" required="true">	
		<cfargument name="dateDeb" type="String" required="true">
		<cfargument name="dateFin" type="String" required="true">	
			
		<cfset dateDebut = transformDate(dateDeb)>	
		<cfset dateFinal = transformDate(dateFin)>		
	 
							
		<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.CONSO_HORSINV">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#id#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_dateDeb" value="#dateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_dateFin" value="#dateFinal#"/>			
			<cfprocresult name="p_result"/>        
		</cfstoredproc>					
		<cfset obj = createObject("component","fr.consotel.consoview.inventaire.session")>		
		<cfset obj.setData(p_result,"journal")>	
		<cfset message = "Factures Ã©mises du " & dateDeb & " au " & dateFin>
		<cfset obj.setAnyData(message,"INFO_FACTURE")>	
		<cfreturn p_result/>	
			
		<cfreturn p_result/>				
	</cffunction>
	
	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="false" default="">		
		<cfif len(oldDateString) neq 0>
			<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
			<cfreturn newDateString> 
		<cfelse>
			<cfreturn oldDateString> 
		</cfif>		
	</cffunction>	
</cfcomponent>