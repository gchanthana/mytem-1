<cfcomponent name="ListeOperateurGroupe">
	<cffunction name="getListeOperateur" access="public" returntype="query" output="false">	
		<cfargument name="idGroupe" type="numeric" required="false" default="1">			
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_v3.sf_listeop_v2">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idGroupe#"/>		
			<cfprocresult name="listeFactures"/>
		</cfstoredproc>				
		<cfquery name="getListe" dbtype="query">		
			select opnom as OPNOM, operateurID as OPERATEURID, IDORGA as NODID			
			from listeFactures		
		</cfquery>			
		<cfreturn getListe>
	</cffunction>
</cfcomponent>