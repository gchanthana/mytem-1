<cfcomponent name="ListeOperateurGroupeAna">
	
	<cffunction name="getListeOperateur" access="public" returntype="query" output="false">	
		<cfargument name="numero" type="numeric" required="false" default="1">				
		<cfset getListe = queryNew("OPNOM,OPERATEURID")>
		<cfset queryAddRow(getListe)>
		<cfset querySetCell(getListe,"OPNOM","---------")>
		<cfset querySetCell(getListe,"OPERATEURID",-1)>			
		<cfreturn getListe>
	</cffunction>
	
</cfcomponent>