<cfcomponent name="BordereauDeReclamation" output="true">		
		
	<cffunction name="afficher" access="public" output="true"> 		
		<cfargument name="reclamation" type="fr.consotel.consoview.inventaire.cycledevie.export.Reclamation">
		
		
		<cfif not isdefined("arguments.reclamation")>
			<cfset reclamation = CreateObject("component","fr.consotel.consoview.inventaire.cycledevie.export.Reclamation")>
			<cfset reclamation.setIDINV_OPERATIONS(IDINV_OPERATIONS)>
			<cfset reclamation.setIDINV_TYPEOP(IDINV_TYPEOP)>
			<cfset reclamation.setLIBELLE_OPERATIONS(LIBELLE_OPERATIONS)>
			<cfset reclamation.setID_SOCIETE(ID_SOCIETE)>
			<cfset reclamation.setID_CONTACT(ID_CONTACT)>
			<cfset reclamation.setDATE_REF(DATE_REF) >
		</cfif>	
			
			<cfset listeProduits = CreateObject("component","fr.consotel.consoview.inventaire.cycledevie.ElementDInventaire").getListeElementFacturationTheoriqueOp(reclamation.IDINV_OPERATIONS,reclamation.DATE_REF,reclamation.IDINV_TYPEOP)>
			<cfset listeElementFacture = CreateObject("component","fr.consotel.consoview.inventaire.cycledevie.ElementDInventaire").getListeElementFacturationReel(reclamation.IDINV_OPERATIONS,reclamation.DATE_REF,reclamation.IDINV_TYPEOP)>
			
			<cfquery name="listeElementFactureRessource" dbtype="query">
				select * from listeElementFacture where IDINVENTAIRE_PRODUIT = #listeProduits.IDINVENTAIRE_PRODUIT[1]#
			</cfquery>
			
			
			<cfquery name="info" datasource="#SESSION.OFFREDSN#">
				select 
					 cs.raison_sociale,
					 cs.adresse1,
					 cs.adresse2,
					 cs.commune,
					 cs.zipcode,
					 cs.telephone,
					 cs.fax,       
					 cc.civilite,
					 cc.nom,
					 cc.prenom,
					 cc.email,
					 pc.pays
					 
				from cde_contact_societe cs,
				     pays_consotel pc,
				     cde_contact cc
				     
				where
				     cs.idcde_contact_societe = #reclamation.ID_SOCIETE#
				and
				     cs.paysconsotelid = pc.paysconsotelid
				and
				     cc.idcde_contact = #reclamation.ID_CONTACT#		
			</cfquery>
			
			<cfset titre = "RÃ©clamation">
			<cfset raison_sociale = session.perimetre.RAISON_SOCIALE>			
			
			<cfset nom = session.user.nom>	
			
			<cfset prenom = session.user.prenom>	 
			<cfset email = session.user.email>
			<cfcontent type="FlashPaper/pdf"> 
			
			<cfheader name="Content-Disposition" value="inline;filename=Reclamation.pdf">
			<cfdocument format="PDF" backgroundvisible="yes" fontembed = "yes" unit="CM" margintop="5" orientation="landscape">														
			<cfoutput> 
				<html>
					<head>	
										
				</head>	
				<style>
						@page { 
							size:portrait; margin-top:2cm; margin-bottom:2cm; margin-left:1cm; margin-right:1cm;
						 }
						 
						html body {
						  margin		: 0;
						  padding		: 0;
						  background	: ##FFFFFF;
						  font			: x-small Verdana,Georgia, Sans-serif;
						  voice-family	: "\"}\""; voice-family:inherit;
						  font-size: small;
						  } html>body {
							font-size	: small;
						}
						
						.cadre {
							border-color:##FFFFFF;
							border-style:solid;
							border-collapse:collapse;
							
							border-top-width:1px;
							border-left-width:1px;
							border-right-width:1px;
							border-bottom-width:1px;
						}
						
						.cadre1{
							background		: rgb(255,255,255);
							border-bottom	:1px solid rgb(0,0,0);
							border-left		:1px solid rgb(0,0,0);
							border-right	:1px solid rgb(0,0,0);
							border-top		:1px solid rgb(0,0,0);
							border-collapse : collapse;
						}
						
						table .menuconsoview
						{
							border 		: none;
							background	: transparent;
							font		: normal 10px Tahoma,Arial, sans-serif;
							color		: ##005EBB;
						}
						
						.menuconsoview caption{
							background		: ##036 ;
							color			: white;
							font			: bold 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							padding			: 4px;
							letter-spacing	: 1px;
						
						}
						
						.menuconsoview th{
							background   	: ##FDFCE1 ;
							color        	: ##000000;
							font        	: normal 12px EurostileBold,verdana,arial,helvetica,serif;
							padding      	: 3px;
							border-collapse	: collapse;
						}
						
						.menuconsoview .th_bottom{
							vertical-align	: left;
							padding			: 3px;
							border-bottom	: 1px solid ##808080;
							font			: normal 12px Tahoma,Arial, sans-serif;
							color        	: ##036;
							background		: ##99CCFF;
						}
						
						.menuconsoview .th_top{
							vertical-align	: left;
							padding			:3px;
							border-top		: 1px solid ##808080;
							font			: normal 12px Tahoma,Arial, sans-serif;
							color       	: ##036;
							background		: ##99CCFF;
						}
						
						.menuconsoview .grid_normal{
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: normal 10px Tahoma,Arial, sans-serif;
							padding		: 2px;
						}
						
						.menuconsoview .grid_normal_alt{
							background	: ##eceae6;
							color		: rgb(0 , 0, 0);
							font		: normal 10px Tahoma,Arial, sans-serif;
							padding		: 2px;
						}
						
						.menuconsoview .grid_small{
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: normal 10px Tahoma,Arial, sans-serif;
							padding		: 0px;
						}
						
						.menuconsoview .grid_medium {
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: normal 10px Tahoma,Arial, sans-serif;
							padding-top		: 1px;
						}
						
						.menuconsoview .grid_total{
							background	: ##008ACC;
							color		: rgb(0 , 0, 0);
							font		: normal 10px Tahoma,Arial, sans-serif;
							padding-top	: 2px;
						}
						
						.menuconsoview .grid_bold{
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: bold 10px Tahoma,Arial, sans-serif;
							padding		: 0px;
						}
						
						/* caption impossible pour liste des produits */
						.td5{
							padding		:5px;
							background	:##F4F188;
							color		:rgb(0,0,0);
							font		:normal 12px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
						}
						
						.td51{
							padding		:5px;
							background	:##FDFCE1;
							color		:rgb(0,0,0);
							font		:normal 10px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
							border		:none 1px; 
							padding		: 2px;
						}
						.tfooter {
							color: rgb(0 , 0, 0);
							font: normal 11px Tahoma,Arial, sans-serif;
						}
					</style>			
																			
					<cfdocumentitem type="header">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
							<tr>
								<td align="left" width="40%" height="50"><img src="/fr/consotel/consoview/images/consoview_new.gif" height="40"></td>
								<td width="17%">&nbsp;</td>
								<td width="3%" bgcolor="F4F188">&nbsp;</td>
								<td align="right" bgcolor="FDFCE1" width="40%"><font face="Arial" size="2">#titre#</font></td>
							</tr>
							<tr><td height="40">&nbsp;</td></tr>
							<tr>
								<td colspan="3">
									<font face="Tahoma" size="2"><strong>
										#nom# #prenom#</strong>
										<br>#email#</font>
								</td>																
							</tr>
							<tr> 
								<td colspan="4">
									<font face="Tahoma" size="1">
										<u>Date:</u>
										&nbsp;#Lsdateformat(now(), 'dd mmmm yyyy')# 
									</font>
								</td>
							</tr>
						</table>
						
					</cfdocumentitem>	
						<cfinclude template="reclamation.cfm">
					<cfdocumentitem type="footer">
						<style>
						.tfooter {
							color: rgb(0 , 0, 0);
							font: normal 11px Tahoma,Arial, sans-serif;
						}
						</style>
						<center>
						<table border="0" cellpadding="0" cellspacing="0" width="560" align="center">
							<tr>
								<td align="center" class="tfooter" width="560">page #cfdocument.currentpagenumber# / #cfdocument.totalpagecount#</td>
							</tr>
						</table>
						</center>
					</cfdocumentitem>			
					</body>
				</html> 
			</cfoutput>			
		</cfdocument>		
	</cffunction>
</cfcomponent>