<cfcomponent name="OperationAction">	
		
	<!--- :::::::::::::::::::::: charger les informatrions etapes ::::::::::::::::::::: 		
	:: charge l'historique des etapes d'un produit (idinventaire_produit)  
	:: Une operation_action par ligne au debut le plus vieux a la fin le plus recent
	:: param in : idInventaire_produit  		   
	::		 out: idinv_action,idinv_op_action_prec,date_action,
	::  		  userid,commentaire_action,commentaire_etat,
	::			  etat_actuel,dans_inventaire,libelle_action
	::            idinv_etat,libelle_etat,ref_calcul,duree_etat,entraine_cloture,entraine_creation,changer_etat_inventaire	
	::       	
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="getHistoriqueProduitAction" access="public" returntype="query" output="false">
		<cfargument name="idOperation" type="numeric" required="true">	
		<cfargument name="idInventaire_produit" type="numeric" required="true">					
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.HISTO_OPERATION_LIGNE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_operations" value="#idOperation#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_produit" value="#idInventaire_produit#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>				
		<cfreturn p_result/>						
	</cffunction>	
	
	<!--- ::::::::::::::::::::::::::: l'historique d'une ressource :::::::::::::::::::::::::::::------------->	
	<cffunction name="getHistoriqueActionRessource" access="public" returntype="query" output="false">                         	 	
		<cfargument name="idressource" type="numeric" required="true">  		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.GETHISTORIQUE_RECHERCHE">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idinventaire_produit" value="#idressource#"/>	
			<cfprocresult name="p_result">					  
		</cfstoredproc> 
		<cfreturn p_result/>					
     </cffunction>     	
		 
	<!--- :::::::::::::::::::::: faire une nouvelle action dans une opï¿½ration:::::::::::: 
	::  crï¿½e un nouvelle enregistrement dans la table inv_operation_action 
	::  pour chaque produits de la liste passï¿½e en parametre 
	::  param in : liste_idInventaire_produit (xxx,xxxx,xxx,xxxx)  		   
	::			   liste_params dans l'ordre : (idinv_action,idinv_op_action_prec,date_action,
	::											userid,commentaire_action,commentaire_etat,
	::											etat_actuel,dans_inventaire)        
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="doAction" access="public" returntype="numeric" >					
		<cfargument name="idOperation" type="numeric" required="true">	
		<cfargument name="listeDeProduits" type="array" required="true">
		<cfargument name="idinv_actions" type="numeric" required="true">
		<cfargument name="idinv_action_prec" type="numeric" required="true">			
		<cfargument name="commentaire_action" type="string" required="true">
		<cfargument name="commentaire_etat" type="string" required="true">		
		<cfargument name="etatActuel" type="numeric" required="true">		
		<cfargument name="dansInventaire" type="numeric" required="true">		
		<cfargument name="dateAction" type="string" required="true">
		
		<!--- <cfif idinv_actions eq 63>			
				<cfset result1 = envoyerMail(idOperation)>			
		<cfelseif idinv_actions eq 64>
			
			<cfquery name="qCommande" datasource="#SESSION.OFFREDSN#">
				SELECT IDCDE_COMMANDE FROM CDE_COMMANDE WHERE IDINV_OPERATIONS = #idOperation#
			</cfquery>
			
			<cfif qCommande.recordcount gt 0>
				<cfset createObject("component","fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande").envoyerMail(qCommande['IDCDE_COMMANDE'][1])>
			</cfif>
			
		</cfif> --->
			
		<cfset liste = arraytolist(listeDeProduits,",")>
		<cfset userid = session.user.CLIENTACCESSID>
		<cfset date_action = transformDate(dateAction)>
		
		<cfset dans_inventaire = getNouvellEtatDansInventaire(idinv_actions,dansInventaire,"CHANGER_ETAT_INVENTAIRE")>	
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.CREATE_ACTION">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_operations" value="#idOperation#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_liste_idinvproduit" value="#liste#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_actions" value="#idinv_actions#"/>
			<cfif idinv_action_prec gt 0>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_op_action_prec" value="#idinv_action_prec#"/>					
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_op_action_prec" value="#idinv_action_prec#" null="true"/>	
			</cfif>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_userid" value="#userid#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaire_action" value="#commentaire_action#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaire_etat" value="#commentaire_etat#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_etat_actuel" value="#etatActuel#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_dans_inventaire" value="#dans_inventaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_action" value="#date_action#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_retour"/>
						     
		</cfstoredproc>		
		
		
		
		<!--- 6 pour annuler apres resiliation
			34 pour annuler apres reception --->
		<cfif (idinv_actions eq 6)or(idinv_actions eq 34)>	
			<cfset lenght = ArrayLen(listeDeProduits)>
			<cfloop index="i" from="1" to="#lenght#">
				<cfset histoligne = getHistoriqueProduitAction(idOperation,listeDeProduits[i])>			
				<cfset idActionPrec = histoligne.IDINV_OP_ACTION[histoligne.recordcount-1]> 	
				<cfset DANS_INVENTAIRE = histoligne.DANS_INVENTAIRE[histoligne.recordcount-1]>
				<cfset manageEtatInventaire(idActionPrec,idinv_actions,DANS_INVENTAIRE)>			
			</cfloop>	
		</cfif> 	
		<cfreturn p_retour/>								
	</cffunction>
	
	<cffunction name="manageEtatInventaire" access="public" returntype="numeric" >			
		<cfargument name="idOpAction" type="numeric" required="true">
		<cfargument name="idAction" type="numeric" required="true">
		<cfargument name="dansInventaire" type="numeric" required="true">
		<cfset newDansInventaire = getNouvellEtatDansInventaire(idAction,dansInventaire,"CHANGER_ETAT_INVENTAIRE")>	
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.MANAGE_ANNULE_ACTION">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_op_action" value="#idOpAction#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_dansInventaire" value="#newDansInventaire#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_retour"/>	
		</cfstoredproc>		
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="updateContact" access="public" returntype="numeric">			
		<cfargument name="idOperation" type="numeric" required="true">
		<cfargument name="idConatct" type="numeric" required="true">
		<cfargument name="idSociete" type="numeric" required="true">
		
		<cfset var qUpdate="">
		
		<cftry>
			
			<cfquery name="qUpdate" datasource="#SESSION.OFFREDSN#" result="status">
				update
					OFFRE.INV_OPERATIONS
				set
					IDCDE_CONTACT = <cfqueryparam value="#idConatct#" cfsqltype="CF_SQL_INTEGER"/>,
					IDCDE_CONTACT_SOCIETE = <cfqueryparam value="#idSociete#" cfsqltype="CF_SQL_INTEGER"/>
				where
					IDINV_OPERATIONS = <cfqueryparam value="#idOperation#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>
			
			<cfcatch type="database">
				<cfreturn -1>
			</cfcatch>
		</cftry>
		
		<cfreturn 1>
		
	</cffunction>
		
		
		
	
	<cffunction name="updateContactEnvoyerMail" access="public" returntype="numeric" >			
		<cfargument name="idOperation" type="numeric" required="true">
		<cfargument name="idConatct" type="numeric" required="true">
		<cfargument name="idSociete" type="numeric" required="true">
		
		<cfset var qUpdate="">
		
		<cftry>
			<cfquery name="qUpdate" datasource="#SESSION.OFFREDSN#" result="status">
				update
					OFFRE.INV_OPERATIONS			
				set 
					IDCDE_CONTACT = <cfqueryparam value="#idConatct#" cfsqltype="CF_SQL_INTEGER"/>,
					IDCDE_CONTACT_SOCIETE = <cfqueryparam value="#idSociete#" cfsqltype="CF_SQL_INTEGER"/>
				where
					IDINV_OPERATIONS = <cfqueryparam value="#idOperation#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>
		<cfscript>
			envoyerMail(idOperation);
		</cfscript>
		
		<cfcatch type="database">
			<cfreturn -1>
			<cfthrow message="Une erreur c'est produite lors de la mise ï¿½ jour du contact de la sociï¿½tï¿½" errorcode="INV-UDCTC-DB" type="database">
		</cfcatch>
		
		<!--- <cfcatch type="any">
			<cfreturn -2>
			<cfthrow message="Une erreur c'est produite lors de la mise ï¿½ jour du contact de la sociï¿½tï¿½" errorcode="INV-ENML-FCT" type="Object">
		</cfcatch> --->
		
		</cftry>
		<!--- <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE.UPDATE_CONTACT_OPERATION">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_operations" value="#idOperation#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact" value="#idConatct#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_societe" value="#idSociete#"/>
		</cfstoredproc> --->
		
		<cfreturn 1>		
	</cffunction>
	
	<cffunction name="envoyerMail" access="public" returntype="numeric" >			
		<cfargument name="idOperation" type="numeric">
		<cfset operationObject = createObject("component","fr.consotel.consoview.inventaire.cycledevie.Inventaire")>
		<cfset operation = operationObject.getDetailOperation(idOperation)>		
		<cfset contactObject = createObject("component","fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.contact")>
		<cfset societeObject = createObject("component","fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.societe")>
		<cfset panierObject = createObject("component","fr.consotel.consoview.inventaire.cycledevie.ElementDInventaire")>		
		<cfset contact = contactObject.get(operation.IDCDE_CONTACT[1])>
		<cfset panier = panierObject.getListeProduitsOperation(idOperation)>	 
		
		<cfset user = session.user.email>		
		<cfset contactEmail = contact.EMAIL[1]>		
		<cfset operateurs = arrayNew(1)>
		<cfset produits = arrayNew(1)>
		<cfset lignes = arrayNew(1)>		
		<cfset comptes = arrayNew(1)>
		<cfset sousComptes = arrayNew(1)>
		
		<cfloop query="panier">			
			<cfset  arrayAppend(operateurs,panier.OPNOM) >
			<cfset  arrayAppend(produits,panier.LIBELLE_PRODUIT) >
			<cfset  arrayAppend(lignes,panier.SOUS_TETE) >			
			<cfset  arrayAppend(comptes,iif(len (panier.COMPTE) gt 0, de("panier.COMPTE"), de("--")))>
			<cfset  arrayAppend(sousComptes,iif(len (panier.SOUS_COMPTE) gt 0, de("panier.SOUS_COMPTE"), de("--")))>
		</cfloop>
		
		
		<cfset sujetMail = "">
		<cfswitch expression="#operation.IDINV_TYPE_OPE#">
			<cfcase value="1">
				<cfset sujetMail = "Demande de rï¿½siliation: #operation.LIBELLE_OPERATIONS#">
			</cfcase>
			<cfcase value="2">
				<cfset sujetMail = "Bon de commande: #operation.LIBELLE_OPERATIONS#">
			</cfcase>
			<cfdefaultcase>
				<cfset sujetMail = "Bon : #operation.LIBELLE_OPERATIONS#">
			</cfdefaultcase>
		</cfswitch>
				
		
		<cfmail from="#user#"
				to="#contactEmail#"
				subject="#sujetMail#"
				type="html"
				charset="utf-8"
				wraptext="72"				 		
				cc="#user#"					
				replyto="#user#"
				failto="support@consotel.fr"
				server="mail.consotel.fr:26">
				
				<cfset typeDemande = operation.IDINV_TYPE_OPE>	
				<cfset OPERATEURS = arrayToList(Operateurs,",")>					
				
				<cfset LI_PRODUITS = arrayToList(produits,",")>
				<cfset LI_LIGNES = arrayToList(lignes,",")>
				<cfset LI_COMPTES = arrayToList(comptes,",")>
				<cfset LI_SOUS_COMPTES = arrayToList(sousComptes,",")>				
					
				<cfset LIBELLE = operation.LIBELLE_OPERATIONS>
				<cfset REF_CLIENT = operation.REFERENCE_INTERNE>
				<cfset REF_OPERATEUR = operation.REF_OPERATEUR>
				<cfset CDE_COMMENTAIRE = operation.commentaire>										
				<cfset DATE = LsDateFormat(now(),"dd/mm/yyyy")>					
				<cfset CONTACTID = operation.IDCDE_CONTACT>				
				<cfset SOCIETEID = operation.IDCDE_CONTACT_SOCIETE>									
				<cfset PERIMETRE_LIBELLE = "">
								
				<cfinclude template="/fr/consotel/consoview/inventaire/cycledevie/TemplateOperationMail.cfm">
		</cfmail> 
		<cfreturn 1>
	</cffunction>
	
	 
	
	<!--- :::::::::::::::::::::::::::: faire une nouvelle action hors opï¿½ration ::::::::::::::::::::::::: --->
	<cffunction name="doActionSurRessource" access="public" returntype="numeric" output="false">                         	 			
		
		<cfargument name="liste_idinventaire_Produits" type="array" required="true">
		<cfargument name="idinv_actions" type="numeric" required="true">
		<cfargument name="idinv_action_prec" type="any" required="true">	
		<cfargument name="date_action" type="string" required="true">			
		<cfargument name="commentaire_action" type="string" required="true">			
		<cfargument name="etatActuel" type="numeric" required="true">		
		<cfargument name="dansInventaire" type="numeric" required="true">
		<cfargument name="qte" type="numeric" required="true">		
		
		<cfset liste = arrayTolist(liste_idinventaire_Produits,',')>
		<cfset userid = session.user.CLIENTACCESSID>
		<cfset dans_inventaire = getNouvellEtatDansInventaire(idinv_actions,dansInventaire,"CHANGER_ETAT_INVENTAIRE")>	
		<cfset dateAction = transformDate(date_action)>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.CREATEACTION_HORSOPE">					
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_list_idinventaire_produit" value="#liste#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_actions" value="#idinv_actions#"/>			
			<cfif idinv_action_prec gt 0>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_op_action_prec" value="#idinv_action_prec#"/>					
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_op_action_prec" value="#idinv_action_prec#" null="true"/>	
			</cfif>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_userid" value="#userid#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaire_action" value="#commentaire_action#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_etat_actuel" value="#etatActuel#"/>				
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_dans_inventaire" value="#dans_inventaire#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_qte" value="#qte#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_action" value="#dateAction#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_retour"/>		
		</cfstoredproc>
					
		<cfreturn p_retour/>					
     </cffunction>     
		
	<!--- :::::::::::::::::::::::::::: Commander un produit sur une ligne existante ::::::::::::::::::::: --->
	
	<cffunction name="commanderProduit2" access="public" returntype="any"> 				
		
		<cfargument name="listeDeproduit" type="array" required="true">	
		<cfargument name="commentaire" type="string" required="true">  
		<cfargument name="date" type="string" required="true">  				
		<!--- Pour chaque ligne du tableau faire commander produit--->
		<cfset end =  arraylen(listeDeproduit)>
		<cfloop index="i" from="1" to = #end#>
			<cfset retour = commanderProduit(listeDeproduit[i].IDSOUS_TETES,
							 listeDeproduit[i].IDPRODUIT_CLIENT,
							 listeDeproduit[i].QUANTITE,
							 commentaire,
							 date)>	
			<cfif retour lt 0>
				<cfreturn retour>			
			</cfif> 
							 	
		</cfloop>		 
		<cfreturn retour>
	</cffunction>
	
	<cffunction name="commanderProduit" access="public" returntype="any" output="false">                         	 	
		
		<cfargument name="idsoustete" type="numeric" required="true">  
		<cfargument name="idproduit_client" type="numeric" required="true">  		
		<cfargument name="qte" type="numeric" required="true">  
		<cfargument name="commentaire" type="string" required="true">  
		<cfargument name="date" type="string" required="true">  	
		 
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.CREATERESSOURCE_HORSOPE">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_ididsous_tete" value="#idsoustete#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idproduit_clientt" value="#idproduit_client#"/>				
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour"/>	
		</cfstoredproc>
		
		
		<cfif p_retour gt 0>
			 
			<cfset tabIDinv_produit = arrayNew(1)>
			<cfset tabIDinv_produit[1] = p_retour>
			
			<cfset userid = session.user.CLIENTACCESSID>
			<cfset idinv_actions = 62>
			<cfset idinv_action_prec = -1>
			<cfset etatActuel = 1>
			<cfset dansInventaire = 0>
			 
			<cfset retour_action = doActionSurRessource(tabIDinv_produit,
														idinv_actions,
														idinv_action_prec,
														date,
														commentaire,
														etatActuel,	
														dansInventaire,
														qte)>
			<cfif retour_action gt 0>
				<cfreturn tabIDinv_produit[1]>
			<cfelse>
				<cfreturn -10>
			</cfif>							
		<cfelse>
			<cfreturn -20>
		</cfif>
					
		<cfreturn -30>		 
     </cffunction>     
	
	
	<!--- retourne le nouvel etat "DANS_INVENTAIRE" suivant l'action' --->
	<cffunction name="getNouvellEtatDansInventaire" access="public" returntype="numeric" output="false">		
		<cfargument name="idinv_actions" type="numeric" required="true">		
		<cfargument name="oldValue" type="numeric" required="true">		
		<cfargument name="cause">
		
		<cfset changerEtat = getInfoEtat(idinv_actions)>	
		<cfset etatChange = changerEtat["#cause#"][1]>
		
		<cfif etatChange eq 1>
			<cfif oldValue eq 0>
				<cfreturn 1>
			<cfelse>
				<cfreturn 0>
			</cfif>
		<cfelse>
			<cfreturn oldValue> 		 
		</cfif>				
	</cffunction>	
	
	<!--- modifier un action --->
	<cffunction name="modifierAction" access="public" returntype="numeric" output="false">		
		<cfargument name="idoperation" type="numeric" required="true">	
		<cfargument name="idAction" type="numeric" required="true">	
		<cfargument name="dateAction" type="string" required="true">	
		<cfargument name="commentaire" type="string" required="true">		
				
		<cfset userid = session.user.CLIENTACCESSID>
		<cfset laDate = transformDate(dateAction)>	
		<cfif idoperation eq 0>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.UPDATE_ACTION">				
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_actions" value="#idAction#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_action" value="#laDate#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaire" value="#commentaire#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_userid" value="#userid#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_result">			    
			</cfstoredproc>
		<cfelse>					
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.UPDATE_OP_ACTION">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_operations" value="#idoperation#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_actions" value="#idAction#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_action" value="#laDate#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaire" value="#commentaire#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_userid" value="#userid#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_result">			    
			</cfstoredproc>
		</cfif>				
		<cfreturn p_result/>		
	</cffunction>			
		
	<!--- retourne le nouvel etat "ETAT_ACTUEL" de l'action et met a jours l'action precedante---> 
	<cffunction name="getNouvellEtatActuel" access="public" returntype="numeric" output="false">		
		<cfargument name="last_idinv_op_action" type="numeric" required="true">		
		<cfargument name="oldValue" type="numeric" required="true">		
		<cfset changerEtat = getInfoEtat(idinv_actions)>	
		<cfset etatChange = changerEtat.ENTRAINE_CLOTURE[1]>
		
		
		<cfif oldValue eq 1>			
			<cfreturn 0>
		<cfelse>
			<cfreturn 1>
		</cfif>
		
	</cffunction>
	
	<!--- :::::::::::::::::::::: charger la liste des actions possibles ::::::::::::::::::::: 
	:: charge la liste des actions possible pour un etat
	:: param in : idinv_etat
	:: 		 out: idinv_actions,libelle_action
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getListeActionPossibles" access="public" returntype="query" output="false">				
		<cfargument name="idEtat" type="numeric" required="true">					
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.LISTEOPPOSSIBLE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_etat" value="#idEtat#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>				
		<cfreturn p_result/>		
	</cffunction>			
	
	<!--- :::::::::::::::::::::: charger les info d'un etat :::::::::::::::::::::::::::::::::::
	:: charge les infos de l'etat entrainï¿½ par une action
	:: param in : idinv_actions
	:: 		 out: libelle_etat,ref_calcul,duree_etat,entraine_cloture,entraine_creation,changer_etat_inventaire
	::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="getInfoEtat" access="public" returntype="query" output="false">	
		<cfargument name="idinvActions" type="numeric" required="true">					
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.ACT_INFOETAT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_actions" value="#idinvActions#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>				
		<cfreturn p_result/>			
	</cffunction>	
	
	
	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="false" default="01-01-1970">		
			<cfif len(oldDateString) neq 0>
			<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
			<cfreturn newDateString> 
		<cfelse>
			<cfreturn oldDateString> 
		</cfif>		
	</cffunction>
	
</cfcomponent>