<cfcomponent name="equipement">
	<cffunction name="setData" access="remote" returntype="void" description="Pour mettre en session le resultat de la recherche de la facture">
		<cfargument name="q" type="query">
		<cfargument name="cle" type="String">
		<cfif IsDefined('session.dataInventaire')>
			<cfif IsDefined('session.dataInventaire.#cle#')>
				<cfset structDelete(session.dataInventaire,"#cle#")>
				<cfset StructInsert(session.dataInventaire,cle,q)>
			<cfelse>
				<cfset structInsert(session.dataInventaire,"#cle#",q)>
			</cfif>
		<cfelse>
			<cfset st=structNew()>
			<cfset structInsert(st,"#cle#",q)>
			<cfset session.dataInventaire=st>
		</cfif>
	</cffunction>
	
	<cffunction name="setAnyData" access="remote" returntype="void">
		<cfargument name="s" type="any">
		<cfargument name="cle" type="String">
		<cfif IsDefined('session.dataInventaire')>
			<cfif IsDefined('session.dataInventaire.#cle#')>
				<cfset structDelete(session.dataInventaire,"#cle#")>
				<cfset StructInsert(session.dataInventaire,cle,s)>
			<cfelse>
				<cfset structInsert(session.dataInventaire,"#cle#",s)>
			</cfif>
		<cfelse>
			<cfset st=structNew()>
			<cfset structInsert(st,"#cle#",s)>
			<cfset session.dataInventaire=st>
		</cfif>
	</cffunction>
		
	<cffunction name="clearData" access="remote" returntype="void" description="Pour mettre en session le resultat de la recherche de la facture">
		<cfargument name="cle" type="String">
		<cfif IsDefined('session.dataInventaire')>
			<cfif IsDefined('session.dataInventaire.#cle#')>
				<cfset structDelete(session.dataInventaire,"#cle#")>
			</cfif>
		</cfif>
	</cffunction>
</cfcomponent>