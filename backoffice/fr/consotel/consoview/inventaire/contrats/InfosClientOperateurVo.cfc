<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.contrats.InfosClientOperateurVo">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="OPERATEUR_CLIENT_ID" type="numeric" default="0">
	<cfproperty name="NOM_OP" type="string" default="">
	<cfproperty name="OPERATEURID" type="numeric" default="0">
	<cfproperty name="IDRACINE" type="numeric" default="0">
	<cfproperty name="FPC_UNIQUE" type="date" default="">
	<cfproperty name="MONTANT_MENSUEL_PENALITE" type="numeric" default="0">
	<cfproperty name="FRAIS_FIXE_RESILIATION" type="numeric" default="0">
	<cfproperty name="BOOL_ACTUEL" type="numeric" default="0">
	<cfproperty name="DATE_CREATE" type="date" default="">
	<cfproperty name="DATE_MODIF" type="date" default="">
	<cfproperty name="USER_CREATE" type="numeric" default="0">
	<cfproperty name="NOM_USER_CREATE" type="string" default="">
	<cfproperty name="NOM_USER_MODIFIED" type="string" default="">
	<cfproperty name="USER_MODIF" type="numeric" default="0">
	<cfproperty name="ENGAG_12" type="boolean" default="1">
	<cfproperty name="ENGAG_24" type="boolean" default="1">
	<cfproperty name="ENGAG_36" type="boolean" default="1">
	<cfproperty name="ENGAG_48" type="boolean" default="1">
	<cfproperty name="SEUIL_TOLERANCE" type="numeric" default="0">
	
	<cfproperty name="DUREE_ELIGIBILITE" 	type="numeric" default="18">
	<cfproperty name="DUREE_OUVERTURE" 		type="numeric" default="2">
	<cfproperty name="DUREE_RENOUVEL" 		type="numeric" default="2">
	<cfproperty name="MAJ_PARC" 			type="numeric" default="0">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.OPERATEUR_CLIENT_ID = 0;
		variables.NOM_OP = "";
		variables.OPERATEURID = 0;
		variables.IDRACINE = 0;
		variables.FPC_UNIQUE = "";
		variables.MONTANT_MENSUEL_PENALITE = 0;
		variables.FRAIS_FIXE_RESILIATION = 0;
		variables.BOOL_ACTUEL = 0;
		variables.DATE_CREATE = "";
		variables.DATE_MODIF = "";
		variables.USER_CREATE = 0;
		variables.NOM_USER_CREATE = "";
		variables.NOM_USER_MODIFIED = "";
		variables.USER_MODIF = 0;
		variables.ENGAG_12 = 1;
		variables.ENGAG_24 = 1;
		variables.ENGAG_36 = 1;
		variables.ENGAG_48 = 1;
		variables.SEUIL_TOLERANCE = 0;
		variables.DUREE_ELIGIBILITE = 0;
		variables.DUREE_OUVERTURE = 0;
		variables.DUREE_RENOUVEL = 0;
		variables.MAJ_PARC = 0;
	</cfscript>

	<cffunction name="init" output="false" returntype="InfosClientOperateurVo">
		<cfreturn this>
	</cffunction>
	
	<cffunction name="getDUREE_ELIGIBILITE" output="false" access="public" returntype="any">
		<cfreturn variables.DUREE_ELIGIBILITE>
	</cffunction>

	<cffunction name="setDUREE_ELIGIBILITE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DUREE_ELIGIBILITE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getDUREE_OUVERTURE" output="false" access="public" returntype="any">
		<cfreturn variables.DUREE_OUVERTURE>
	</cffunction>

	<cffunction name="setDUREE_OUVERTURE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DUREE_OUVERTURE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
		<cffunction name="getDUREE_RENOUVEL" output="false" access="public" returntype="any">
		<cfreturn variables.DUREE_RENOUVEL>
	</cffunction>

	<cffunction name="setDUREE_RENOUVEL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DUREE_RENOUVEL = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getMAJ_PARC" output="false" access="public" returntype="any">
		<cfreturn variables.MAJ_PARC>
	</cffunction>

	<cffunction name="setMAJ_PARC" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.MAJ_PARC = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	
	<cffunction name="getOPERATEUR_CLIENT_ID" output="false" access="public" returntype="any">
		<cfreturn variables.OPERATEUR_CLIENT_ID>
	</cffunction>

	<cffunction name="setOPERATEUR_CLIENT_ID" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.OPERATEUR_CLIENT_ID = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNOM_OP" output="false" access="public" returntype="any">
		<cfreturn variables.NOM_OP>
	</cffunction>

	<cffunction name="setNOM_OP" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.NOM_OP = arguments.val>
	</cffunction>

	<cffunction name="getOPERATEURID" output="false" access="public" returntype="any">
		<cfreturn variables.OPERATEURID>
	</cffunction>

	<cffunction name="setOPERATEURID" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.OPERATEURID = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDRACINE" output="false" access="public" returntype="any">
		<cfreturn variables.IDRACINE>
	</cffunction>

	<cffunction name="setIDRACINE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDRACINE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getFPC_UNIQUE" output="false" access="public" returntype="any">
		<cfreturn variables.FPC_UNIQUE>
	</cffunction>

	<cffunction name="setFPC_UNIQUE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.FPC_UNIQUE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getMONTANT_MENSUEL_PENALITE" output="false" access="public" returntype="any">
		<cfreturn variables.MONTANT_MENSUEL_PENALITE>
	</cffunction>

	<cffunction name="setMONTANT_MENSUEL_PENALITE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.MONTANT_MENSUEL_PENALITE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getFRAIS_FIXE_RESILIATION" output="false" access="public" returntype="any">
		<cfreturn variables.FRAIS_FIXE_RESILIATION>
	</cffunction>

	<cffunction name="setFRAIS_FIXE_RESILIATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.FRAIS_FIXE_RESILIATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getBOOL_ACTUEL" output="false" access="public" returntype="any">
		<cfreturn variables.BOOL_ACTUEL>
	</cffunction>

	<cffunction name="setBOOL_ACTUEL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.BOOL_ACTUEL = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_CREATE" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_CREATE>
	</cffunction>

	<cffunction name="setDATE_CREATE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_CREATE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_MODIF" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_MODIF>
	</cffunction>

	<cffunction name="setDATE_MODIF" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_MODIF = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getUSER_CREATE" output="false" access="public" returntype="any">
		<cfreturn variables.USER_CREATE>
	</cffunction>

	<cffunction name="setUSER_CREATE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.USER_CREATE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNOM_USER_CREATE" output="false" access="public" returntype="any">
		<cfreturn variables.NOM_USER_CREATE>
	</cffunction>

	<cffunction name="setNOM_USER_CREATE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.NOM_USER_CREATE = arguments.val>
	</cffunction>

	<cffunction name="getNOM_USER_MODIFIED" output="false" access="public" returntype="any">
		<cfreturn variables.NOM_USER_MODIFIED>
	</cffunction>

	<cffunction name="setNOM_USER_MODIFIED" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.NOM_USER_MODIFIED = arguments.val>
	</cffunction>

	<cffunction name="getUSER_MODIF" output="false" access="public" returntype="any">
		<cfreturn variables.USER_MODIF>
	</cffunction>

	<cffunction name="setUSER_MODIF" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.USER_MODIF = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getENGAG_12" output="false" access="public" returntype="any">
		<cfreturn variables.ENGAG_12>
	</cffunction>

	<cffunction name="setENGAG_12" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsBoolean(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ENGAG_12 = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid boolean"/>
		</cfif>
	</cffunction>

	<cffunction name="getENGAG_24" output="false" access="public" returntype="any">
		<cfreturn variables.ENGAG_24>
	</cffunction>

	<cffunction name="setENGAG_24" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsBoolean(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ENGAG_24 = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid boolean"/>
		</cfif>
	</cffunction>

	<cffunction name="getENGAG_36" output="false" access="public" returntype="any">
		<cfreturn variables.ENGAG_36>
	</cffunction>

	<cffunction name="setENGAG_36" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsBoolean(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ENGAG_36 = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid boolean"/>
		</cfif>
	</cffunction>

	<cffunction name="getENGAG_48" output="false" access="public" returntype="any">
		<cfreturn variables.ENGAG_48>
	</cffunction>

	<cffunction name="setENGAG_48" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsBoolean(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ENGAG_48 = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid boolean"/>
		</cfif>
	</cffunction>

	<cffunction name="getSEUIL_TOLERANCE" output="false" access="public" returntype="any">
		<cfreturn variables.SEUIL_TOLERANCE>
	</cffunction>

	<cffunction name="setSEUIL_TOLERANCE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.SEUIL_TOLERANCE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>



</cfcomponent>