<cfcomponent output="false">

	<cffunction name="read" output="false" access="public" returntype="fr.consotel.consoview.inventaire.contrats.Contrat">
		<cfargument name="id" required="true">
		<cfset var qRead="">
		<cfset var obj="">

		<cfquery name="qRead" datasource="ROCOFFRE">
			select 	IDCONTRAT, IDTYPE_CONTRAT, IDFOURNISSEUR, REFERENCE_CONTRAT, DATE_SIGNATURE, DATE_ECHEANCE, 
					COMMENTAIRE_CONTRAT, MONTANT_CONTRAT, DUREE_CONTRAT, TACITE_RECONDUCTION, DATE_DEBUT, 
					PREAVIS, DATE_DENONCIATION, IDCDE_CONTACT_SOCIETE, LOYER, PERIODICITE, 
					MONTANT_FRAIS, NUMERO_CLIENT, NUMERO_FOURNISSEUR, NUMERO_FACTURE, CODE_INTERNE, 
					BOOL_CONTRAT_CADRE, BOOL_AVENANT, ID_CONTRAT_MAITRE, FRAIS_FIXE_RESILIATION, MONTANT_MENSUEL_PEN, 
					DATE_RESILIATION, PENALITE, DESIGNATION, DATE_RENOUVELLEMENT, OPERATEURID, 
					IDCOMPTE_FACTURATION, IDSOUS_COMPTE, NUMERO_CONTRAT, IDRACINE, DATE_ELLIGIBILITE, 
					DUREE_ELLIGIBILITE
			from OFFRE.CONTRAT
			where IDCONTRAT = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.id#" />
		</cfquery>

		<cfscript>
			obj = createObject("component", "fr.consotel.consoview.inventaire.contrats.Contrat").init();
			obj.setIDCONTRAT(qRead.IDCONTRAT);
			obj.setIDTYPE_CONTRAT(qRead.IDTYPE_CONTRAT);
			obj.setIDFOURNISSEUR(qRead.IDFOURNISSEUR);
			obj.setREFERENCE_CONTRAT(qRead.REFERENCE_CONTRAT);
			obj.setDATE_SIGNATURE(qRead.DATE_SIGNATURE);
			obj.setDATE_ECHEANCE(qRead.DATE_ECHEANCE);
			obj.setCOMMENTAIRE_CONTRAT(qRead.COMMENTAIRE_CONTRAT);
			obj.setMONTANT_CONTRAT(qRead.MONTANT_CONTRAT);
			obj.setDUREE_CONTRAT(qRead.DUREE_CONTRAT);
			obj.setTACITE_RECONDUCTION(qRead.TACITE_RECONDUCTION);
			obj.setDATE_DEBUT(qRead.DATE_DEBUT);
			obj.setPREAVIS(qRead.PREAVIS);
			obj.setDATE_DENONCIATION(qRead.DATE_DENONCIATION);
			obj.setIDCDE_CONTACT_SOCIETE(qRead.IDCDE_CONTACT_SOCIETE);
			obj.setLOYER(qRead.LOYER);
			obj.setPERIODICITE(qRead.PERIODICITE);
			obj.setMONTANT_FRAIS(qRead.MONTANT_FRAIS);
			obj.setNUMERO_CLIENT(qRead.NUMERO_CLIENT);
			obj.setNUMERO_FOURNISSEUR(qRead.NUMERO_FOURNISSEUR);
			obj.setNUMERO_FACTURE(qRead.NUMERO_FACTURE);
			obj.setCODE_INTERNE(qRead.CODE_INTERNE);
			obj.setBOOL_CONTRAT_CADRE(qRead.BOOL_CONTRAT_CADRE);
			obj.setBOOL_AVENANT(qRead.BOOL_AVENANT);
			obj.setID_CONTRAT_MAITRE(qRead.ID_CONTRAT_MAITRE);
			obj.setFRAIS_FIXE_RESILIATION(qRead.FRAIS_FIXE_RESILIATION);
			obj.setMONTANT_MENSUEL_PEN(qRead.MONTANT_MENSUEL_PEN);
			obj.setDATE_RESILIATION(qRead.DATE_RESILIATION);
			obj.setPENALITE(qRead.PENALITE);
			obj.setDESIGNATION(qRead.DESIGNATION);
			obj.setDATE_RENOUVELLEMENT(qRead.DATE_RENOUVELLEMENT);
			obj.setOPERATEURID(qRead.OPERATEURID);
			obj.setIDCOMPTE_FACTURATION(qRead.IDCOMPTE_FACTURATION);
			obj.setIDSOUS_COMPTE(qRead.IDSOUS_COMPTE);
			obj.setNUMERO_CONTRAT(qRead.NUMERO_CONTRAT);
			obj.setIDRACINE(qRead.IDRACINE);
			obj.setDATE_ELLIGIBILITE(qRead.DATE_ELLIGIBILITE);
			obj.setDUREE_ELLIGIBILITE(qRead.DUREE_ELLIGIBILITE);
			return obj;
		</cfscript>
	</cffunction>



	<cffunction name="create" output="false" access="public">
		<cfargument name="bean" required="true" type="fr.consotel.consoview.inventaire.contrats.Contrat">
		<cfset var qCreate="">

		<cfset var qGetId="">

		<cfset var local1=arguments.bean.getIDTYPE_CONTRAT()>
		<cfset var local2=arguments.bean.getIDFOURNISSEUR()>
		<cfset var local3=arguments.bean.getREFERENCE_CONTRAT()>
		<cfset var local4=arguments.bean.getDATE_SIGNATURE()>
		<cfset var local5=arguments.bean.getDATE_ECHEANCE()>
		<cfset var local6=arguments.bean.getCOMMENTAIRE_CONTRAT()>
		<cfset var local7=arguments.bean.getMONTANT_CONTRAT()>
		<cfset var local8=arguments.bean.getDUREE_CONTRAT()>
		<cfset var local9=arguments.bean.getTACITE_RECONDUCTION()>
		<cfset var local10=arguments.bean.getDATE_DEBUT()>
		<cfset var local11=arguments.bean.getPREAVIS()>
		<cfset var local12=arguments.bean.getDATE_DENONCIATION()>
		<cfset var local13=arguments.bean.getIDCDE_CONTACT_SOCIETE()>
		<cfset var local14=arguments.bean.getLOYER()>
		<cfset var local15=arguments.bean.getPERIODICITE()>
		<cfset var local16=arguments.bean.getMONTANT_FRAIS()>
		<cfset var local17=arguments.bean.getNUMERO_CLIENT()>
		<cfset var local18=arguments.bean.getNUMERO_FOURNISSEUR()>
		<cfset var local19=arguments.bean.getNUMERO_FACTURE()>
		<cfset var local20=arguments.bean.getCODE_INTERNE()>
		<cfset var local21=arguments.bean.getBOOL_CONTRAT_CADRE()>
		<cfset var local22=arguments.bean.getBOOL_AVENANT()>
		<cfset var local23=arguments.bean.getID_CONTRAT_MAITRE()>
		<cfset var local24=arguments.bean.getFRAIS_FIXE_RESILIATION()>
		<cfset var local25=arguments.bean.getMONTANT_MENSUEL_PEN()>
		<cfset var local26=arguments.bean.getDATE_RESILIATION()>
		<cfset var local27=arguments.bean.getPENALITE()>
		<cfset var local28=arguments.bean.getDESIGNATION()>
		<cfset var local29=arguments.bean.getDATE_RENOUVELLEMENT()>
		<cfset var local30=arguments.bean.getOPERATEURID()>
		<cfset var local31=arguments.bean.getIDCOMPTE_FACTURATION()>
		<cfset var local32=arguments.bean.getIDSOUS_COMPTE()>
		<cfset var local33=arguments.bean.getNUMERO_CONTRAT()>
		<cfset var local34=arguments.bean.getIDRACINE()>
		<cfset var local35=arguments.bean.getDATE_ELLIGIBILITE()>
		<cfset var local36=arguments.bean.getDUREE_ELLIGIBILITE()>

		<cftransaction isolation="read_committed">
			<cfquery name="qCreate" datasource="ROCOFFRE">
				insert into OFFRE.CONTRAT(IDTYPE_CONTRAT, IDFOURNISSEUR, REFERENCE_CONTRAT, DATE_SIGNATURE, DATE_ECHEANCE, COMMENTAIRE_CONTRAT, MONTANT_CONTRAT, DUREE_CONTRAT, TACITE_RECONDUCTION, DATE_DEBUT, PREAVIS, DATE_DENONCIATION, IDCDE_CONTACT_SOCIETE, LOYER, PERIODICITE, MONTANT_FRAIS, NUMERO_CLIENT, NUMERO_FOURNISSEUR, NUMERO_FACTURE, CODE_INTERNE, BOOL_CONTRAT_CADRE, BOOL_AVENANT, ID_CONTRAT_MAITRE, FRAIS_FIXE_RESILIATION, MONTANT_MENSUEL_PEN, DATE_RESILIATION, PENALITE, DESIGNATION, DATE_RENOUVELLEMENT, OPERATEURID, IDCOMPTE_FACTURATION, IDSOUS_COMPTE, NUMERO_CONTRAT, IDRACINE, DATE_ELLIGIBILITE, DUREE_ELLIGIBILITE)
				values (
					<cfqueryparam value="#local1#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local1 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local2#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local2 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local3#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local4#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local4 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local5#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local5 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local6#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local7#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local7 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local8#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local8 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local9#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local9 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local10#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local10 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local11#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local11 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local12#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local12 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local13#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local13 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local14#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local14 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local15#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local15 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local16#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local16 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local17#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local18#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local19#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local20#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local21#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local21 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local22#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local22 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local23#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local23 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local24#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local24 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local25#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local25 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local26#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local26 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local27#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local27 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local28#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local29#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local29 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local30#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local30 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local31#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local31 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local32#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local32 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local33#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local34#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local34 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local35#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local35 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local36#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local36 eq ""), de("yes"), de("no"))#" />
				)
			</cfquery>

			<!--- If your server has a better way to get the ID that is more reliable, use that instead --->
			<cfquery name="qGetID" datasource="ROCOFFRE">
				select IDCONTRAT
				from OFFRE.CONTRAT
				where IDTYPE_CONTRAT = <cfqueryparam value="#local1#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local1 eq ""), de("yes"), de("no"))#" />
				  and IDFOURNISSEUR = <cfqueryparam value="#local2#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local2 eq ""), de("yes"), de("no"))#" />
				  and REFERENCE_CONTRAT = <cfqueryparam value="#local3#" cfsqltype="CF_SQL_VARCHAR" />
				  and DATE_SIGNATURE = <cfqueryparam value="#local4#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local4 eq ""), de("yes"), de("no"))#" />
				  and DATE_ECHEANCE = <cfqueryparam value="#local5#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local5 eq ""), de("yes"), de("no"))#" />
				  and COMMENTAIRE_CONTRAT = <cfqueryparam value="#local6#" cfsqltype="CF_SQL_VARCHAR" />
				  and MONTANT_CONTRAT = <cfqueryparam value="#local7#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local7 eq ""), de("yes"), de("no"))#" />
				  and DUREE_CONTRAT = <cfqueryparam value="#local8#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local8 eq ""), de("yes"), de("no"))#" />
				  and TACITE_RECONDUCTION = <cfqueryparam value="#local9#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local9 eq ""), de("yes"), de("no"))#" />
				  and DATE_DEBUT = <cfqueryparam value="#local10#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local10 eq ""), de("yes"), de("no"))#" />
				  and PREAVIS = <cfqueryparam value="#local11#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local11 eq ""), de("yes"), de("no"))#" />
				  and DATE_DENONCIATION = <cfqueryparam value="#local12#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local12 eq ""), de("yes"), de("no"))#" />
				  and IDCDE_CONTACT_SOCIETE = <cfqueryparam value="#local13#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local13 eq ""), de("yes"), de("no"))#" />
				  and LOYER = <cfqueryparam value="#local14#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local14 eq ""), de("yes"), de("no"))#" />
				  and PERIODICITE = <cfqueryparam value="#local15#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local15 eq ""), de("yes"), de("no"))#" />
				  and MONTANT_FRAIS = <cfqueryparam value="#local16#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local16 eq ""), de("yes"), de("no"))#" />
				  and NUMERO_CLIENT = <cfqueryparam value="#local17#" cfsqltype="CF_SQL_VARCHAR" />
				  and NUMERO_FOURNISSEUR = <cfqueryparam value="#local18#" cfsqltype="CF_SQL_VARCHAR" />
				  and NUMERO_FACTURE = <cfqueryparam value="#local19#" cfsqltype="CF_SQL_VARCHAR" />
				  and CODE_INTERNE = <cfqueryparam value="#local20#" cfsqltype="CF_SQL_VARCHAR" />
				  and BOOL_CONTRAT_CADRE = <cfqueryparam value="#local21#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local21 eq ""), de("yes"), de("no"))#" />
				  and BOOL_AVENANT = <cfqueryparam value="#local22#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local22 eq ""), de("yes"), de("no"))#" />
				  and ID_CONTRAT_MAITRE = <cfqueryparam value="#local23#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local23 eq ""), de("yes"), de("no"))#" />
				  and FRAIS_FIXE_RESILIATION = <cfqueryparam value="#local24#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local24 eq ""), de("yes"), de("no"))#" />
				  and MONTANT_MENSUEL_PEN = <cfqueryparam value="#local25#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local25 eq ""), de("yes"), de("no"))#" />
				  and DATE_RESILIATION = <cfqueryparam value="#local26#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local26 eq ""), de("yes"), de("no"))#" />
				  and PENALITE = <cfqueryparam value="#local27#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local27 eq ""), de("yes"), de("no"))#" />
				  and DESIGNATION = <cfqueryparam value="#local28#" cfsqltype="CF_SQL_VARCHAR" />
				  and DATE_RENOUVELLEMENT = <cfqueryparam value="#local29#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local29 eq ""), de("yes"), de("no"))#" />
				  and OPERATEURID = <cfqueryparam value="#local30#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local30 eq ""), de("yes"), de("no"))#" />
				  and IDCOMPTE_FACTURATION = <cfqueryparam value="#local31#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local31 eq ""), de("yes"), de("no"))#" />
				  and IDSOUS_COMPTE = <cfqueryparam value="#local32#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local32 eq ""), de("yes"), de("no"))#" />
				  and NUMERO_CONTRAT = <cfqueryparam value="#local33#" cfsqltype="CF_SQL_VARCHAR" />
				  and IDRACINE = <cfqueryparam value="#local34#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local34 eq ""), de("yes"), de("no"))#" />
				  and DATE_ELLIGIBILITE = <cfqueryparam value="#local35#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((local35 eq ""), de("yes"), de("no"))#" />
				  and DUREE_ELLIGIBILITE = <cfqueryparam value="#local36#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local36 eq ""), de("yes"), de("no"))#" />
				order by IDCONTRAT desc
			</cfquery>
		</cftransaction>

		<cfscript>
			arguments.bean.setIDCONTRAT(qGetID.IDCONTRAT);
		</cfscript>
		<cfreturn arguments.bean />
	</cffunction>



	<cffunction name="update" output="false" access="public">
		<cfargument name="bean" required="true" type="fr.consotel.consoview.inventaire.contrats.Contrat">
		<cfset var qUpdate="">

		<cfquery name="qUpdate" datasource="ROCOFFRE" result="status">
			update OFFRE.CONTRAT
			set IDTYPE_CONTRAT = <cfqueryparam value="#arguments.bean.getIDTYPE_CONTRAT()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getIDTYPE_CONTRAT() eq ""), de("yes"), de("no"))#" />,
				IDFOURNISSEUR = <cfqueryparam value="#arguments.bean.getIDFOURNISSEUR()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getIDFOURNISSEUR() eq ""), de("yes"), de("no"))#" />,
				REFERENCE_CONTRAT = <cfqueryparam value="#arguments.bean.getREFERENCE_CONTRAT()#" cfsqltype="CF_SQL_VARCHAR" />,
				DATE_SIGNATURE = <cfqueryparam value="#arguments.bean.getDATE_SIGNATURE()#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((arguments.bean.getDATE_SIGNATURE() eq ""), de("yes"), de("no"))#" />,
				DATE_ECHEANCE = <cfqueryparam value="#arguments.bean.getDATE_ECHEANCE()#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((arguments.bean.getDATE_ECHEANCE() eq ""), de("yes"), de("no"))#" />,
				COMMENTAIRE_CONTRAT = <cfqueryparam value="#arguments.bean.getCOMMENTAIRE_CONTRAT()#" cfsqltype="CF_SQL_VARCHAR" />,
				MONTANT_CONTRAT = <cfqueryparam value="#arguments.bean.getMONTANT_CONTRAT()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getMONTANT_CONTRAT() eq ""), de("yes"), de("no"))#" />,
				DUREE_CONTRAT = <cfqueryparam value="#arguments.bean.getDUREE_CONTRAT()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getDUREE_CONTRAT() eq ""), de("yes"), de("no"))#" />,
				TACITE_RECONDUCTION = <cfqueryparam value="#arguments.bean.getTACITE_RECONDUCTION()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getTACITE_RECONDUCTION() eq ""), de("yes"), de("no"))#" />,
				DATE_DEBUT = <cfqueryparam value="#arguments.bean.getDATE_DEBUT()#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((arguments.bean.getDATE_DEBUT() eq ""), de("yes"), de("no"))#" />,
				PREAVIS = <cfqueryparam value="#arguments.bean.getPREAVIS()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getPREAVIS() eq ""), de("yes"), de("no"))#" />,
				DATE_DENONCIATION = <cfqueryparam value="#arguments.bean.getDATE_DENONCIATION()#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((arguments.bean.getDATE_DENONCIATION() eq ""), de("yes"), de("no"))#" />,
				IDCDE_CONTACT_SOCIETE = <cfqueryparam value="#arguments.bean.getIDCDE_CONTACT_SOCIETE()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getIDCDE_CONTACT_SOCIETE() eq ""), de("yes"), de("no"))#" />,
				LOYER = <cfqueryparam value="#arguments.bean.getLOYER()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getLOYER() eq ""), de("yes"), de("no"))#" />,
				PERIODICITE = <cfqueryparam value="#arguments.bean.getPERIODICITE()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getPERIODICITE() eq ""), de("yes"), de("no"))#" />,
				MONTANT_FRAIS = <cfqueryparam value="#arguments.bean.getMONTANT_FRAIS()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getMONTANT_FRAIS() eq ""), de("yes"), de("no"))#" />,
				NUMERO_CLIENT = <cfqueryparam value="#arguments.bean.getNUMERO_CLIENT()#" cfsqltype="CF_SQL_VARCHAR" />,
				NUMERO_FOURNISSEUR = <cfqueryparam value="#arguments.bean.getNUMERO_FOURNISSEUR()#" cfsqltype="CF_SQL_VARCHAR" />,
				NUMERO_FACTURE = <cfqueryparam value="#arguments.bean.getNUMERO_FACTURE()#" cfsqltype="CF_SQL_VARCHAR" />,
				CODE_INTERNE = <cfqueryparam value="#arguments.bean.getCODE_INTERNE()#" cfsqltype="CF_SQL_VARCHAR" />,
				BOOL_CONTRAT_CADRE = <cfqueryparam value="#arguments.bean.getBOOL_CONTRAT_CADRE()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getBOOL_CONTRAT_CADRE() eq ""), de("yes"), de("no"))#" />,
				BOOL_AVENANT = <cfqueryparam value="#arguments.bean.getBOOL_AVENANT()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getBOOL_AVENANT() eq ""), de("yes"), de("no"))#" />,
				ID_CONTRAT_MAITRE = <cfqueryparam value="#arguments.bean.getID_CONTRAT_MAITRE()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getID_CONTRAT_MAITRE() eq ""), de("yes"), de("no"))#" />,
				FRAIS_FIXE_RESILIATION = <cfqueryparam value="#arguments.bean.getFRAIS_FIXE_RESILIATION()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getFRAIS_FIXE_RESILIATION() eq ""), de("yes"), de("no"))#" />,
				MONTANT_MENSUEL_PEN = <cfqueryparam value="#arguments.bean.getMONTANT_MENSUEL_PEN()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getMONTANT_MENSUEL_PEN() eq ""), de("yes"), de("no"))#" />,
				DATE_RESILIATION = <cfqueryparam value="#arguments.bean.getDATE_RESILIATION()#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((arguments.bean.getDATE_RESILIATION() eq ""), de("yes"), de("no"))#" />,
				PENALITE = <cfqueryparam value="#arguments.bean.getPENALITE()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getPENALITE() eq ""), de("yes"), de("no"))#" />,
				DESIGNATION = <cfqueryparam value="#arguments.bean.getDESIGNATION()#" cfsqltype="CF_SQL_VARCHAR" />,
				DATE_RENOUVELLEMENT = <cfqueryparam value="#arguments.bean.getDATE_RENOUVELLEMENT()#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((arguments.bean.getDATE_RENOUVELLEMENT() eq ""), de("yes"), de("no"))#" />,
				OPERATEURID = <cfqueryparam value="#arguments.bean.getOPERATEURID()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getOPERATEURID() eq ""), de("yes"), de("no"))#" />,
				IDCOMPTE_FACTURATION = <cfqueryparam value="#arguments.bean.getIDCOMPTE_FACTURATION()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getIDCOMPTE_FACTURATION() eq ""), de("yes"), de("no"))#" />,
				IDSOUS_COMPTE = <cfqueryparam value="#arguments.bean.getIDSOUS_COMPTE()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getIDSOUS_COMPTE() eq ""), de("yes"), de("no"))#" />,
				NUMERO_CONTRAT = <cfqueryparam value="#arguments.bean.getNUMERO_CONTRAT()#" cfsqltype="CF_SQL_VARCHAR" />,
				IDRACINE = <cfqueryparam value="#arguments.bean.getIDRACINE()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getIDRACINE() eq ""), de("yes"), de("no"))#" />,
				DATE_ELLIGIBILITE = <cfqueryparam value="#arguments.bean.getDATE_ELLIGIBILITE()#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((arguments.bean.getDATE_ELLIGIBILITE() eq ""), de("yes"), de("no"))#" />,
				DUREE_ELLIGIBILITE = <cfqueryparam value="#arguments.bean.getDUREE_ELLIGIBILITE()#" cfsqltype="CF_SQL_DECIMAL" null="#iif((arguments.bean.getDUREE_ELLIGIBILITE() eq ""), de("yes"), de("no"))#" />
			where IDCONTRAT = <cfqueryparam value="#arguments.bean.getIDCONTRAT()#" cfsqltype="CF_SQL_DECIMAL">
		</cfquery>
		<cfreturn arguments.bean />
	</cffunction>



	<cffunction name="delete" output="false" access="public" returntype="void">
		<cfargument name="bean" required="true" type="fr.consotel.consoview.inventaire.contrats.Contrat">
		<cfset var qDelete="">

		<cfquery name="qDelete" datasource="ROCOFFRE" result="status">
			delete
			from OFFRE.CONTRAT
			where IDCONTRAT = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.bean.getIDCONTRAT()#" />
		</cfquery>

	</cffunction>


</cfcomponent>