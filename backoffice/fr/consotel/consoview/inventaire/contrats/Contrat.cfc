<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.contrats.Contrat">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="IDCONTRAT" type="numeric" default="0">
	<cfproperty name="IDTYPE_CONTRAT" type="numeric" default="0">
	<cfproperty name="IDFOURNISSEUR" type="numeric" default="0">
	<cfproperty name="REFERENCE_CONTRAT" type="string" default="">
	<cfproperty name="DATE_SIGNATURE" type="date" default="">
	<cfproperty name="DATE_ECHEANCE" type="date" default="">
	<cfproperty name="COMMENTAIRE_CONTRAT" type="string" default="">
	<cfproperty name="MONTANT_CONTRAT" type="numeric" default="0">
	<cfproperty name="DUREE_CONTRAT" type="numeric" default="0">
	<cfproperty name="TACITE_RECONDUCTION" type="numeric" default="0">
	<cfproperty name="DATE_DEBUT" type="date" default="">
	<cfproperty name="PREAVIS" type="numeric" default="0">
	<cfproperty name="DATE_DENONCIATION" type="date" default="">
	<cfproperty name="IDCDE_CONTACT_SOCIETE" type="numeric" default="0">
	<cfproperty name="LOYER" type="numeric" default="0">
	<cfproperty name="PERIODICITE" type="numeric" default="0">
	<cfproperty name="MONTANT_FRAIS" type="numeric" default="0">
	<cfproperty name="NUMERO_CLIENT" type="string" default="">
	<cfproperty name="NUMERO_FOURNISSEUR" type="string" default="">
	<cfproperty name="NUMERO_FACTURE" type="string" default="">
	<cfproperty name="CODE_INTERNE" type="string" default="">
	<cfproperty name="BOOL_CONTRAT_CADRE" type="numeric" default="0">
	<cfproperty name="BOOL_AVENANT" type="numeric" default="0">
	<cfproperty name="ID_CONTRAT_MAITRE" type="numeric" default="0">
	<cfproperty name="FRAIS_FIXE_RESILIATION" type="numeric" default="0">
	<cfproperty name="MONTANT_MENSUEL_PEN" type="numeric" default="0">
	<cfproperty name="DATE_RESILIATION" type="date" default="">
	<cfproperty name="PENALITE" type="numeric" default="0">
	<cfproperty name="DESIGNATION" type="string" default="">
	<cfproperty name="DATE_RENOUVELLEMENT" type="date" default="">
	<cfproperty name="OPERATEURID" type="numeric" default="0">
	<cfproperty name="IDCOMPTE_FACTURATION" type="numeric" default="0">
	<cfproperty name="IDSOUS_COMPTE" type="numeric" default="0">
	<cfproperty name="NUMERO_CONTRAT" type="string" default="">
	<cfproperty name="IDRACINE" type="numeric" default="0">
	<cfproperty name="DATE_ELLIGIBILITE" type="date" default="">
	<cfproperty name="DUREE_ELLIGIBILITE" type="numeric" default="0">

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.IDCONTRAT = 0;
		variables.IDTYPE_CONTRAT = 0;
		variables.IDFOURNISSEUR = 0;
		variables.REFERENCE_CONTRAT = "";
		variables.DATE_SIGNATURE = "";
		variables.DATE_ECHEANCE = "";
		variables.COMMENTAIRE_CONTRAT = "";
		variables.MONTANT_CONTRAT = 0;
		variables.DUREE_CONTRAT = 0;
		variables.TACITE_RECONDUCTION = 0;
		variables.DATE_DEBUT = "";
		variables.PREAVIS = 0;
		variables.DATE_DENONCIATION = "";
		variables.IDCDE_CONTACT_SOCIETE = 0;
		variables.LOYER = 0;
		variables.PERIODICITE = 0;
		variables.MONTANT_FRAIS = 0;
		variables.NUMERO_CLIENT = "";
		variables.NUMERO_FOURNISSEUR = "";
		variables.NUMERO_FACTURE = "";
		variables.CODE_INTERNE = "";
		variables.BOOL_CONTRAT_CADRE = 0;
		variables.BOOL_AVENANT = 0;
		variables.ID_CONTRAT_MAITRE = 0;
		variables.FRAIS_FIXE_RESILIATION = 0;
		variables.MONTANT_MENSUEL_PEN = 0;
		variables.DATE_RESILIATION = "";
		variables.PENALITE = 0;
		variables.DESIGNATION = "";
		variables.DATE_RENOUVELLEMENT = "";
		variables.OPERATEURID = 0;
		variables.IDCOMPTE_FACTURATION = 0;
		variables.IDSOUS_COMPTE = 0;
		variables.NUMERO_CONTRAT = "";
		variables.IDRACINE = 0;
		variables.DATE_ELLIGIBILITE = "";
		variables.DUREE_ELLIGIBILITE = 0;
	</cfscript>

	<cffunction name="init" output="false" returntype="Contrat">
		<cfreturn this>
	</cffunction>
	
	<cffunction name="getIDCONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.IDCONTRAT>
	</cffunction>

	<cffunction name="setIDCONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDCONTRAT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDTYPE_CONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.IDTYPE_CONTRAT>
	</cffunction>

	<cffunction name="setIDTYPE_CONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDTYPE_CONTRAT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDFOURNISSEUR" output="false" access="public" returntype="any">
		<cfreturn variables.IDFOURNISSEUR>
	</cffunction>

	<cffunction name="setIDFOURNISSEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDFOURNISSEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getREFERENCE_CONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.REFERENCE_CONTRAT>
	</cffunction>

	<cffunction name="setREFERENCE_CONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.REFERENCE_CONTRAT = arguments.val>
	</cffunction>

	<cffunction name="getDATE_SIGNATURE" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_SIGNATURE>
	</cffunction>

	<cffunction name="setDATE_SIGNATURE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_SIGNATURE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_ECHEANCE" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_ECHEANCE>
	</cffunction>

	<cffunction name="setDATE_ECHEANCE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_ECHEANCE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getCOMMENTAIRE_CONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.COMMENTAIRE_CONTRAT>
	</cffunction>

	<cffunction name="setCOMMENTAIRE_CONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.COMMENTAIRE_CONTRAT = arguments.val>
	</cffunction>

	<cffunction name="getMONTANT_CONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.MONTANT_CONTRAT>
	</cffunction>

	<cffunction name="setMONTANT_CONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.MONTANT_CONTRAT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDUREE_CONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.DUREE_CONTRAT>
	</cffunction>

	<cffunction name="setDUREE_CONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DUREE_CONTRAT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTACITE_RECONDUCTION" output="false" access="public" returntype="any">
		<cfreturn variables.TACITE_RECONDUCTION>
	</cffunction>

	<cffunction name="setTACITE_RECONDUCTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.TACITE_RECONDUCTION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_DEBUT" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_DEBUT>
	</cffunction>

	<cffunction name="setDATE_DEBUT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_DEBUT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getPREAVIS" output="false" access="public" returntype="any">
		<cfreturn variables.PREAVIS>
	</cffunction>

	<cffunction name="setPREAVIS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PREAVIS = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_DENONCIATION" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_DENONCIATION>
	</cffunction>

	<cffunction name="setDATE_DENONCIATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_DENONCIATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDCDE_CONTACT_SOCIETE" output="false" access="public" returntype="any">
		<cfreturn variables.IDCDE_CONTACT_SOCIETE>
	</cffunction>

	<cffunction name="setIDCDE_CONTACT_SOCIETE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDCDE_CONTACT_SOCIETE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLOYER" output="false" access="public" returntype="any">
		<cfreturn variables.LOYER>
	</cffunction>

	<cffunction name="setLOYER" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.LOYER = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPERIODICITE" output="false" access="public" returntype="any">
		<cfreturn variables.PERIODICITE>
	</cffunction>

	<cffunction name="setPERIODICITE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PERIODICITE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMONTANT_FRAIS" output="false" access="public" returntype="any">
		<cfreturn variables.MONTANT_FRAIS>
	</cffunction>

	<cffunction name="setMONTANT_FRAIS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.MONTANT_FRAIS = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNUMERO_CLIENT" output="false" access="public" returntype="any">
		<cfreturn variables.NUMERO_CLIENT>
	</cffunction>

	<cffunction name="setNUMERO_CLIENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.NUMERO_CLIENT = arguments.val>
	</cffunction>

	<cffunction name="getNUMERO_FOURNISSEUR" output="false" access="public" returntype="any">
		<cfreturn variables.NUMERO_FOURNISSEUR>
	</cffunction>

	<cffunction name="setNUMERO_FOURNISSEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.NUMERO_FOURNISSEUR = arguments.val>
	</cffunction>

	<cffunction name="getNUMERO_FACTURE" output="false" access="public" returntype="any">
		<cfreturn variables.NUMERO_FACTURE>
	</cffunction>

	<cffunction name="setNUMERO_FACTURE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.NUMERO_FACTURE = arguments.val>
	</cffunction>

	<cffunction name="getCODE_INTERNE" output="false" access="public" returntype="any">
		<cfreturn variables.CODE_INTERNE>
	</cffunction>

	<cffunction name="setCODE_INTERNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.CODE_INTERNE = arguments.val>
	</cffunction>

	<cffunction name="getBOOL_CONTRAT_CADRE" output="false" access="public" returntype="any">
		<cfreturn variables.BOOL_CONTRAT_CADRE>
	</cffunction>

	<cffunction name="setBOOL_CONTRAT_CADRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.BOOL_CONTRAT_CADRE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getBOOL_AVENANT" output="false" access="public" returntype="any">
		<cfreturn variables.BOOL_AVENANT>
	</cffunction>

	<cffunction name="setBOOL_AVENANT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.BOOL_AVENANT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getID_CONTRAT_MAITRE" output="false" access="public" returntype="any">
		<cfreturn variables.ID_CONTRAT_MAITRE>
	</cffunction>

	<cffunction name="setID_CONTRAT_MAITRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ID_CONTRAT_MAITRE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getFRAIS_FIXE_RESILIATION" output="false" access="public" returntype="any">
		<cfreturn variables.FRAIS_FIXE_RESILIATION>
	</cffunction>

	<cffunction name="setFRAIS_FIXE_RESILIATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.FRAIS_FIXE_RESILIATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMONTANT_MENSUEL_PEN" output="false" access="public" returntype="any">
		<cfreturn variables.MONTANT_MENSUEL_PEN>
	</cffunction>

	<cffunction name="setMONTANT_MENSUEL_PEN" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.MONTANT_MENSUEL_PEN = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_RESILIATION" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_RESILIATION>
	</cffunction>

	<cffunction name="setDATE_RESILIATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_RESILIATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getPENALITE" output="false" access="public" returntype="any">
		<cfreturn variables.PENALITE>
	</cffunction>

	<cffunction name="setPENALITE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PENALITE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDESIGNATION" output="false" access="public" returntype="any">
		<cfreturn variables.DESIGNATION>
	</cffunction>

	<cffunction name="setDESIGNATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.DESIGNATION = arguments.val>
	</cffunction>

	<cffunction name="getDATE_RENOUVELLEMENT" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_RENOUVELLEMENT>
	</cffunction>

	<cffunction name="setDATE_RENOUVELLEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_RENOUVELLEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getOPERATEURID" output="false" access="public" returntype="any">
		<cfreturn variables.OPERATEURID>
	</cffunction>

	<cffunction name="setOPERATEURID" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.OPERATEURID = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDCOMPTE_FACTURATION" output="false" access="public" returntype="any">
		<cfreturn variables.IDCOMPTE_FACTURATION>
	</cffunction>

	<cffunction name="setIDCOMPTE_FACTURATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDCOMPTE_FACTURATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDSOUS_COMPTE" output="false" access="public" returntype="any">
		<cfreturn variables.IDSOUS_COMPTE>
	</cffunction>

	<cffunction name="setIDSOUS_COMPTE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDSOUS_COMPTE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNUMERO_CONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.NUMERO_CONTRAT>
	</cffunction>

	<cffunction name="setNUMERO_CONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.NUMERO_CONTRAT = arguments.val>
	</cffunction>

	<cffunction name="getIDRACINE" output="false" access="public" returntype="any">
		<cfreturn variables.IDRACINE>
	</cffunction>

	<cffunction name="setIDRACINE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDRACINE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_ELLIGIBILITE" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_ELLIGIBILITE>
	</cffunction>

	<cffunction name="setDATE_ELLIGIBILITE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_ELLIGIBILITE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDUREE_ELLIGIBILITE" output="false" access="public" returntype="any">
		<cfreturn variables.DUREE_ELLIGIBILITE>
	</cffunction>

	<cffunction name="setDUREE_ELLIGIBILITE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DUREE_ELLIGIBILITE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>



</cfcomponent>