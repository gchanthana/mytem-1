<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.employes.Employe">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="IDGROUPE_CLIENT" type="numeric" default="0">
	<cfproperty name="IDEMPLOYE" type="numeric" default="0">
	<cfproperty name="IDRACINE_NUMBER" type="numeric" default="0">
	<cfproperty name="IDSITE_PHYSIQUE" type="numeric" default="0">
	<cfproperty name="IDEMPLACEMENT" type="numeric" default="0">
	<cfproperty name="IDPOOL_GESTION" type="numeric" default="0">
	<cfproperty name="APP_LOGINID" type="numeric" default="0">
	<cfproperty name="CLE_IDENTIFIANT" type="string" default="">
	<cfproperty name="CIVILITE" type="string" default="">
	<cfproperty name="NOMPRENOM" type="string" default="">
	<cfproperty name="NOM" type="string" default="">
	<cfproperty name="PRENOM" type="string" default="">
	<cfproperty name="EMAIL" type="string" default="">
	<cfproperty name="FONCTION_EMPLOYE" type="string" default="">
	<cfproperty name="STATUS_EMPLOYE" type="string" default="">
	<cfproperty name="COMMENTAIRE" type="string" default="">
	<cfproperty name="BOOL_PUBLICATION" type="numeric" default="0">
	<cfproperty name="CODE_INTERNE" type="string" default="">
	<cfproperty name="REFERENCE_EMPLOYE" type="string" default="">
	<cfproperty name="MATRICULE" type="string" default="">
	<cfproperty name="DATE_CREATION" type="date" default="">
	<cfproperty name="DATE_MODIFICATION" type="date" default="">
	<cfproperty name="INOUT" type="numeric" default="0">
	<cfproperty name="DATE_ENTREE" type="date" default="">
	<cfproperty name="DATE_SORTIE" type="date" default="">
	<cfproperty name="SEUIL_COMMANDE" type="numeric" default="0">
	<cfproperty name="CODE_ANALYTIQUE" type="numeric" default="0">
	<cfproperty name="NIVEAU" type="numeric" default="0">
	<cfproperty name="C1" type="string" default="">
	<cfproperty name="C2" type="string" default="">
	<cfproperty name="C3" type="string" default="">
	<cfproperty name="C4" type="string" default="">
	<cfproperty name="TELEPHONE_FIXE" type="string" default="">
	<cfproperty name="FAX" type="string" default="">

	<cfscript>
		//Initialize the CFC with the default properties values.
		this.IDGROUPE_CLIENT = 0;
		this.IDEMPLOYE = 0;
		this.IDRACINE_NUMBER = 0;
		this.IDSITE_PHYSIQUE = 0;
		this.IDEMPLACEMENT = 0;
		this.IDPOOL_GESTION = 0;
		this.APP_LOGINID = 0;
		this.CLE_IDENTIFIANT = "";
		this.CIVILITE = "";
		this.NOMPRENOM = "";
		this.NOM = "";
		this.PRENOM = "";
		this.EMAIL = "";
		this.FONCTION_EMPLOYE = "";
		this.STATUS_EMPLOYE = "";
		this.COMMENTAIRE = "";
		this.BOOL_PUBLICATION = 0;
		this.CODE_INTERNE = "";
		this.REFERENCE_EMPLOYE = "";
		this.MATRICULE = "";
		this.DATE_CREATION = "";
		this.DATE_MODIFICATION = "";
		this.INOUT = 0;
		this.DATE_ENTREE = "";
		this.DATE_SORTIE = "";
		this.SEUIL_COMMANDE = 0;
		this.NIVEAU = 0;
		this.C1 = "";
		this.C2 = "";
		this.C3 = "";
		this.C4 = "";
		this.TELEPHONE_FIXE = "";
		this.CODE_ANALYTIQUE = 0;
		this.FAX = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="Employe">
		<cfreturn this>
	</cffunction>
	<cffunction name="getIDGROUPE_CLIENT" output="false" access="public" returntype="any">
		<cfreturn this.IDGROUPE_CLIENT>
	</cffunction>

	<cffunction name="setIDGROUPE_CLIENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDGROUPE_CLIENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDEMPLOYE" output="false" access="public" returntype="any">
		<cfreturn this.IDEMPLOYE>
	</cffunction>

	<cffunction name="setIDEMPLOYE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDEMPLOYE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDRACINE_NUMBER" output="false" access="public" returntype="any">
		<cfreturn this.IDRACINE_NUMBER>
	</cffunction>

	<cffunction name="setIDRACINE_NUMBER" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDRACINE_NUMBER = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDSITE_PHYSIQUE" output="false" access="public" returntype="any">
		<cfreturn this.IDSITE_PHYSIQUE>
	</cffunction>

	<cffunction name="setIDSITE_PHYSIQUE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDSITE_PHYSIQUE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDEMPLACEMENT" output="false" access="public" returntype="any">
		<cfreturn this.IDEMPLACEMENT>
	</cffunction>

	<cffunction name="setIDEMPLACEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDEMPLACEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDPOOL_GESTION" output="false" access="public" returntype="any">
		<cfreturn this.IDPOOL_GESTION>
	</cffunction>

	<cffunction name="setIDPOOL_GESTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDPOOL_GESTION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getAPP_LOGINID" output="false" access="public" returntype="any">
		<cfreturn this.APP_LOGINID>
	</cffunction>

	<cffunction name="setAPP_LOGINID" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.APP_LOGINID = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getCLE_IDENTIFIANT" output="false" access="public" returntype="any">
		<cfreturn this.CLE_IDENTIFIANT>
	</cffunction>

	<cffunction name="setCLE_IDENTIFIANT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CLE_IDENTIFIANT = arguments.val>
	</cffunction>

	<cffunction name="getCIVILITE" output="false" access="public" returntype="any">
		<cfreturn this.CIVILITE>
	</cffunction>

	<cffunction name="setCIVILITE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CIVILITE = arguments.val>
	</cffunction>

	<cffunction name="getNOMPRENOM" output="false" access="public" returntype="any">
		<cfreturn this.NOMPRENOM>
	</cffunction>

	<cffunction name="setNOMPRENOM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.NOMPRENOM = arguments.val>
	</cffunction>

	<cffunction name="getNOM" output="false" access="public" returntype="any">
		<cfreturn this.NOM>
	</cffunction>

	<cffunction name="setNOM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.NOM = arguments.val>
	</cffunction>

	<cffunction name="getPRENOM" output="false" access="public" returntype="any">
		<cfreturn this.PRENOM>
	</cffunction>

	<cffunction name="setPRENOM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.PRENOM = arguments.val>
	</cffunction>

	<cffunction name="getEMAIL" output="false" access="public" returntype="any">
		<cfreturn this.EMAIL>
	</cffunction>

	<cffunction name="setEMAIL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.EMAIL = arguments.val>
	</cffunction>

	<cffunction name="getFONCTION_EMPLOYE" output="false" access="public" returntype="any">
		<cfreturn this.FONCTION_EMPLOYE>
	</cffunction>

	<cffunction name="setFONCTION_EMPLOYE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.FONCTION_EMPLOYE = arguments.val>
	</cffunction>

	<cffunction name="getSTATUS_EMPLOYE" output="false" access="public" returntype="any">
		<cfreturn this.STATUS_EMPLOYE>
	</cffunction>

	<cffunction name="setSTATUS_EMPLOYE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.STATUS_EMPLOYE = arguments.val>
	</cffunction>

	<cffunction name="getCOMMENTAIRE" output="false" access="public" returntype="any">
		<cfreturn this.COMMENTAIRE>
	</cffunction>

	<cffunction name="setCOMMENTAIRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.COMMENTAIRE = arguments.val>
	</cffunction>

	<cffunction name="getBOOL_PUBLICATION" output="false" access="public" returntype="any">
		<cfreturn this.BOOL_PUBLICATION>
	</cffunction>

	<cffunction name="setBOOL_PUBLICATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.BOOL_PUBLICATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getCODE_INTERNE" output="false" access="public" returntype="any">
		<cfreturn this.CODE_INTERNE>
	</cffunction>

	<cffunction name="setCODE_INTERNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CODE_INTERNE = arguments.val>
	</cffunction>

	<cffunction name="getREFERENCE_EMPLOYE" output="false" access="public" returntype="any">
		<cfreturn this.REFERENCE_EMPLOYE>
	</cffunction>

	<cffunction name="setREFERENCE_EMPLOYE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.REFERENCE_EMPLOYE = arguments.val>
	</cffunction>

	<cffunction name="getMATRICULE" output="false" access="public" returntype="any">
		<cfreturn this.MATRICULE>
	</cffunction>

	<cffunction name="setMATRICULE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.MATRICULE = arguments.val>
	</cffunction>

	<cffunction name="getDATE_CREATION" output="false" access="public" returntype="any">
		<cfreturn this.DATE_CREATION>
	</cffunction>

	<cffunction name="setDATE_CREATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DATE_CREATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_MODIFICATION" output="false" access="public" returntype="any">
		<cfreturn this.DATE_MODIFICATION>
	</cffunction>

	<cffunction name="setDATE_MODIFICATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DATE_MODIFICATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getINOUT" output="false" access="public" returntype="any">
		<cfreturn this.INOUT>
	</cffunction>

	<cffunction name="setINOUT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.INOUT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_ENTREE" output="false" access="public" returntype="any">
		<cfreturn this.DATE_ENTREE>
	</cffunction>

	<cffunction name="setDATE_ENTREE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DATE_ENTREE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_SORTIE" output="false" access="public" returntype="any">
		<cfreturn this.DATE_SORTIE>
	</cffunction>

	<cffunction name="setDATE_SORTIE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DATE_SORTIE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getSEUIL_COMMANDE" output="false" access="public" returntype="any">
		<cfreturn this.SEUIL_COMMANDE>
	</cffunction>

	<cffunction name="setSEUIL_COMMANDE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.SEUIL_COMMANDE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNIVEAU" output="false" access="public" returntype="any">
		<cfreturn this.NIVEAU>
	</cffunction>

	<cffunction name="setNIVEAU" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NIVEAU = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getC1" output="false" access="public" returntype="any">
		<cfreturn this.C1>
	</cffunction>

	<cffunction name="setC1" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.C1 = arguments.val>
	</cffunction>

	<cffunction name="getC2" output="false" access="public" returntype="any">
		<cfreturn this.C2>
	</cffunction>

	<cffunction name="setC2" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.C2 = arguments.val>
	</cffunction>

	<cffunction name="getC3" output="false" access="public" returntype="any">
		<cfreturn this.C3>
	</cffunction>

	<cffunction name="setC3" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.C3 = arguments.val>
	</cffunction>

	<cffunction name="getC4" output="false" access="public" returntype="any">
		<cfreturn this.C4>
	</cffunction>

	<cffunction name="setC4" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.C4 = arguments.val>
	</cffunction>

	<cffunction name="getTELEPHONE_FIXE" output="false" access="public" returntype="any">
		<cfreturn this.TELEPHONE_FIXE>
	</cffunction>

	<cffunction name="setTELEPHONE_FIXE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TELEPHONE_FIXE = arguments.val>
	</cffunction>

	<cffunction name="getFAX" output="false" access="public" returntype="any">
		<cfreturn this.FAX>
	</cffunction>

	<cffunction name="setFAX" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.FAX = arguments.val>
	</cffunction>
	
	<cffunction name="getCODE_ANALYTIQUE" output="false" access="public" returntype="any">
		<cfreturn this.CODE_ANALYTIQUE>
	</cffunction>

	<cffunction name="setCODE_ANALYTIQUE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CODE_ANALYTIQUE = arguments.val>
	</cffunction>



</cfcomponent>