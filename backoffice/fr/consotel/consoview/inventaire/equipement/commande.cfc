<cfcomponent name="commande">
	<cffunction name="searchCommandes" returntype="query" access="remote" output="true">
		<cfargument name="arr" type="array">
		<cfset realid=session.perimetre.ID_PERIMETRE/>
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.search_commandes">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[2]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[3]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#realid#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<!---<cfset QueryAddRow(p_result, 2)>
		<cfset QuerySetCell(p_result, "DATE_COMMANDE", "20/05/2006", 1)>
		<cfset QuerySetCell(p_result, "DELAI_LIVRAISON", 5, 1)>
		<cfset QuerySetCell(p_result, "IDCOMMANDE", 35, 1)>
		<cfset QuerySetCell(p_result, "MONTANT", 3500,1)>
		<cfset QuerySetCell(p_result, "NUMERO_COMMANDE", 12,1)>
		
		<cfset QuerySetCell(p_result, "DATE_COMMANDE", "20/03/2006", 2)>
		<cfset QuerySetCell(p_result, "DELAI_LIVRAISON", 4, 2)>
		<cfset QuerySetCell(p_result, "IDCOMMANDE", 23, 2)>
		<cfset QuerySetCell(p_result, "MONTANT", 450,2)>
		<cfset QuerySetCell(p_result, "NUMERO_COMMANDE", 14,2)>--->
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getInfosCommande" returntype="array" access="remote" output="true">
		<cfargument name="index" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_EqCommande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#index#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="EQ">
			<cfprocresult name="res1">
		</cfstoredproc>
		
		<cfset QueryAddRow(res1, 1)>
		<cfset QuerySetCell(res1, "IDCOMMANDE", 1, 1)>
		<cfset QuerySetCell(res1, "IDEQUIPEMENT", 2, 1)>
		<cfset QuerySetCell(res1, "LIBELLE_EQ", "monEquipement", 1)>
		<cfset QuerySetCell(res1, "PRIX_ACHAT", 2500,1)>
		<cfset QuerySetCell(res1, "QUANTITE", 1,1)>
		
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_Info_commandes">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#index#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="EQ">
			<cfprocresult name="res2">
		</cfstoredproc>
		
		<cfset QueryAddRow(res2, 1)>
		<cfset QuerySetCell(res2, "CODE_INTERNE", "code_i", 1)>
		<cfset QuerySetCell(res2, "DATE_COMMANDE", "26/01/2003", 1)>
		<cfset QuerySetCell(res2, "DELAI_LIVRAISON", 5, 1)>
		<cfset QuerySetCell(res2, "FRAIS_DIVERS", 1350,1)>
		<cfset QuerySetCell(res2, "FRAIS_LIVRAISON", 4500,1)>
		<cfset QuerySetCell(res2, "IDCOMMANDE", 1,1)>
		<cfset QuerySetCell(res2, "MONTANT", 36000,1)>
		<cfset QuerySetCell(res2, "NUMERO_COMMANDE", "numero_3584",1)>
		<cfset QuerySetCell(res2, "REFERENCE_COMMANDE", "ref_478",1)>
		
		<cfset dataArr = arrayNew(1)>
		<cfset dataArr[1] = res1>
		<cfset dataArr[2] = res2>
		<cfreturn dataArr>
	</cffunction>
	
	<cffunction name="addCommande" returntype="numeric" access="remote" output="true">
		<cfargument name="data" type="array">
		<cfset realid=session.perimetre.ID_PERIMETRE/>
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.ins_commande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[2]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[3]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[4]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[5]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[6]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[7]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[8]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#realid#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
</cfcomponent>