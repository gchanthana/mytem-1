 <!--- pour surround --->
<cfcomponent name="FlotteMobileManager" alias="fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager">
	
	<cfproperty name="idRacine" type="numeric" default="0">
	<cfproperty name="idPerimetre" type="numeric" default="0">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idRacine = SESSION.PERIMETRE.ID_GROUPE;
		variables.idPerimetre = SESSION.PERIMETRE.ID_PERIMETRE;
	</cfscript>
		
	<cffunction name="getIdRacine" output="false" access="public" returntype="numeric">
		<cfreturn variables.idRacine>
	</cffunction>
	
	<cffunction name="getIdPerimetre" output="false" access="public" returntype="numeric">
		<cfreturn variables.idPerimetre>
	</cffunction>
	
<!--- =================================================== EXPORT EN CSV ================================================================ --->
	
	<!--- LISTE DE TOUS LES EQUIPEMENTS POUR L'EXPORT CSV --->
	<cffunction name="getEquipementsCSV" access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfargument name="idPool" required="true" type="Numeric" />
		<cfargument name="afficherTout" required="true" type="Numeric" />
		
		<cfset maxRow = 100>
		<cfif afficherTout>
			<cfset maxRow = -1>
		</cfif>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.searchFlotteAll_v4" >
			<cfprocparam value="#IDRacine#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#search#" cfsqltype="CF_SQL_VARCHAR">
			<cfif idPool neq -2>
				<cfprocparam value="#idPool#" cfsqltype="CF_SQL_INTEGER">
			<cfelse>
				<cfprocparam value="#idPool#" cfsqltype="CF_SQL_INTEGER" null="true">
			</cfif>
			<cfprocresult name="qGetEquipements" maxrows="#maxRow#">
		</cfstoredproc>
		<cfreturn qGetEquipements>
	</cffunction>

	<!--- EXPORT DE LA LISTE DE TOUS LES EQUIPEMENTS --->
	<cffunction name="exporterEnCSV" access="remote" returntype="string">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />	
		<cfargument name="libelle" required="true" type="string">
		<cfargument name="idPool" required="true" type="string">
		<cfargument name="afficherTout" required="true" type="string">
		 
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"flotteGSM_#libelle#.csv">				
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>
								
				<cfset qDetail= getEquipementsCSV(IDRacine,search,idPool,afficherTout)>				
				<cfset NewLine = Chr(13) & Chr(10)>
				<cfset space = Chr(13) & Chr(10)>		
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="ISO-8859-1">
				<cfcontent type="text/plain">
																																																					 	 																																																																																																																																										  		 																																						

				<cfoutput>Date de fin de période contractuelle;Date d'éligibilité;Nombre de jour avant fin de contrat;Date d'ouverture de la ligne;Date de début du contrat;Date de résiliation de la ligne;Date de dernière facture;Date de dernier réengagement;Numéro de contrat;Collaborateur;Matricule;Niveau collaborateur;Mail collaborateur;Date d'entrée collaborateur;Date de départ collaborateur;Fonction collaborateur;Status collaborateur;Code interne collaborateur;Référence collaborateur;Champs C1;Champs C2;Champs C3;Champs C4;Commentaire collaborateur;Identifiant collaborateur;Nom collaborateur;Prénom collaborateur;Téléphone collaborateur;Fax collaborateur;Distributeur de la ligne;Opérateur de la ligne;Numéro de la ligne;Type de ligne;Type de raccordement;Compte de facturation;Sous compte de facturation;Numéro de carte SIM;Mobile; IMEI (mobile);Modèle mobile;Niveau mobile;Prix d'achat mobile;Date d'achat mobile;Date de commande mobile;Référence commande mobile;Date de fin de garantie mobile;Nombre de jour avant fin garantie mobile;Durée de garantier mobile;Numéro du contrat de garantie mobile;#NewLine#</cfoutput>				
				<cfloop query="qDetail">
			    	<cfoutput>#TRIM(LSDATEFORMAT(DATE_FPC,"DD/MM/YYYY"))#;#TRIM(LSDATEFORMAT(DATE_ELLIGIBILITE,"DD/MM/YYYY"))#;#TRIM(val(NB_JR_FIN_LG))#;#TRIM(LSDATEFORMAT(DATE_OUVERTURE,"DD/MM/YYYY"))#;#TRIM(LSDATEFORMAT(DATEDEB,"DD/MM/YYYY"))#;#TRIM(LSDATEFORMAT(DATE_RESILIATION,"DD/MM/YYYY"))#;#TRIM(LSDATEFORMAT(ACCES_LAST_FACTURE,"DD/MM/YYYY"))#;#TRIM(LSDATEFORMAT(DATE_DERNIER_ENGAG,"DD/MM/YYYY"))#;#TRIM(NUM_CONTRAT_ABO)#;#TRIM(COLLABORATEUR)#;#TRIM(MATRICULE)#;#TRIM(NIVEAU_COLLABORATEUR)#;#TRIM(EMAIL)#;#TRIM(LSDATEFORMAT(DATE_ENTREE,"DD/MM/YYYY"))#;#TRIM(LSDATEFORMAT(DATE_SORTIE,"DD/MM/YYYY"))#;#TRIM(FONCTION_EMPLOYE)#;#TRIM(STATUS_EMPLOYE)#;#TRIM(CODE_INTERNE)#;#TRIM(REFERENCE_EMPLOYE)#;#TRIM(C1)#;#TRIM(C2)#;#TRIM(C3)#;#TRIM(C4)#;#TRIM(COMMENTAIRE)#;#TRIM(IDENTIFIANT_EMPLOYE)#;#TRIM(NOM_EMPLOYE)#;#TRIM(PRENOM_EMPLOYE)#;#TRIM(TELEPHONE_FIXE)#;#TRIM(FAX)#;#TRIM(DISTRIBUTEUR)#;#TRIM(OPERATEUR)#;#TRIM(LIGNE)#;#TRIM(LIBELLE_TYPE_LIGNE)#;#TRIM(LIBELLE_RACCORDEMENT)#;#TRIM(COMPTE_FACTURATION)#;#TRIM(SOUS_COMPTE)#;#TRIM(NUM_SIM)#;#TRIM(TERMINAL)#;#TRIM(IMEI)#;#TRIM(MODELE)#;#TRIM(NIVEAU_TERM)#;#TRIM(PRIX_TERMINAL)#;#TRIM(LSDATEFORMAT(DATE_ACHAT,"DD/MM/YYYY"))#;#TRIM(LSDATEFORMAT(DATE_COMMANDE,"DD/MM/YYYY"))#;#TRIM(REF_COMMANDE_TERMINAL)#;#TRIM(LSDATEFORMAT(DATE_FIN_GARANTIE,"DD/MM/YYYY"))#;#TRIM(NBR_JRS_FIN_GARANTIE)#;#TRIM(DUREE_GARANTIE)#;#TRIM(NUM_CONTRAT_GARANTIE)#;#NewLine#</cfoutput>
				</cfloop>
			</cfsavecontent>	
				
			<!--- Cr�ation du fichier CSV --->			 
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/inventaire/equipement/flotte/csv/#fileName#" charset="ISO-8859-1"
					addnewline="true" fixnewline="true" output="#contentObj#">		
			<cfreturn "#fileName#">
	</cffunction>
	
<!--- ================================== GESTION DE L'HISTORIQUE =================================================================== --->
	<cffunction name="logAction" access="remote" returntype="void">
		<cfargument name="ID_WORDS" required="true" type="Numeric" />
		<cfargument name="ACTION_PRINCIPALE" required="true" type="String" />
		<cfargument name="ITEM1" required="true" type="String" />
		<cfargument name="ITEM2" required="true" type="String" />
		<cfargument name="ID_TERMINAL" required="true" type="Numeric" />
		<cfargument name="ID_SIM" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE" required="true" type="Numeric" />
		<cfargument name="IDGROUPE_RACINE" required="true" type="Numeric" />
		<cfargument name="UID_ACTION" required="true" type="String" />
		<cfargument name="DATE_EFFET" required="true" type="String" />
		<cfargument name="COMMENTAIRE" required="false" type="String" default=-1/>
		<cfargument name="ID_CAUSE" required="false" type="Numeric" default=-1/>
		<cfargument name="ID_STATUT" required="false" type="Numeric" default=-1 />
		
		<cfset chaineDebug="#COMMENTAIRE#,#ID_CAUSE#,#ID_STATUT#">
		<cftrace text="#chaineDebug#" >
		
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset USER = SESSION.USER.CLIENTACCESSID>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.log_action_flotte_v2">
			<cfprocparam value="#USER#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_WORDS#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ACTION_PRINCIPALE#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#ITEM1#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#ITEM2#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#ID_TERMINAL#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_SIM#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#UID_ACTION#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#DATE_EFFET#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfif COMMENTAIRE neq -1>
				<cftrace text="passe dans 1" >
				<cfprocparam value="#COMMENTAIRE#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfelse>
				<cfprocparam  value="#COMMENTAIRE#" cfsqltype="CF_SQL_INTEGER" null="true">
				<cftrace text="passe dans 2" >
			</cfif>
			<cfif ID_CAUSE neq -1>
				<cfprocparam value="#ID_CAUSE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfelse>
				<cfprocparam  value="#ID_CAUSE#" cfsqltype="CF_SQL_INTEGER" null="true">
			</cfif>
			 <cfif ID_STATUT neq -1>
				<cfprocparam value="#ID_STATUT#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfelse>
				<cfprocparam  value="#ID_STATUT#" cfsqltype="CF_SQL_INTEGER" null="true">
			</cfif>
			<cfprocparam variable="logStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>			
	</cffunction>
	
<!--- ============================================= GESTION DES AFFECTATION ============================================================ --->
	
	
	<cffunction name="affectTermToEmp_V2" access="remote" returntype="numeric">
		<cfargument name="idTerm" required="true" type="Numeric" />
		<cfargument name="idCollab" required="true" type="Numeric" />
		<cfargument name="libelle_term" required="true" type="String" />
		<cfargument name="libelle_collab" required="true" type="String" />
		<cfargument name="date_effet" required="true" type="String" />
		<cfargument name="pret" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Affect_equip_emp_v2">
			<cfprocparam  value="#idCollab#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idTerm#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#pret#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="affectTermStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfif affectTermStatus gt 0>
			<cfif pret eq 0>
	         	<cfset ID_WORD = 1>
			<cfelse>
				<cfset ID_WORD = 13>
			</cfif>
			<cfset logAction(ID_WORD,1,libelle_term,libelle_collab,idTerm,-1,idCollab,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date_effet)/>
	    <cfelse>
	          <cfreturn affectTermStatus>
	    </cfif>
		<cfreturn affectTermStatus>
	</cffunction>

	<cffunction name="affectTermToEmp_V3" access="remote" returntype="numeric">
		<cfargument name="idTerm" required="true" type="Numeric" />
		<cfargument name="idCollab" required="true" type="Numeric" />
		<cfargument name="libelle_term" required="true" type="String" />
		<cfargument name="libelle_collab" required="true" type="String" />
		<cfargument name="date_effet" required="true" type="String" />
		<cfargument name="pret" required="true" type="Numeric" />
		<cfargument name="idSim" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Affect_equip_emp_v3">
			<cfprocparam  value="#idCollab#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idTerm#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#pret#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idSim#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="affectTermStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfif affectTermStatus gt 0>
			<cfif pret eq 0>
	         	<cfset ID_WORD = 1>
			<cfelse>
				<cfset ID_WORD = 13>
			</cfif>
			<cfset logAction(ID_WORD,1,libelle_term,libelle_collab,idTerm,-1,idCollab,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date_effet)/>
	    <cfelse>
	          <cfreturn affectTermStatus>
	    </cfif>
		<cfreturn affectTermStatus>
	</cffunction>
	
	<cffunction name="affectSimCollab_V3" access="remote" returntype="numeric">
		<cfargument name="idSim" required="true" type="Numeric" />
		<cfargument name="idCollab" required="true" type="Numeric" />
		<cfargument name="libelle_sim" required="true" type="String" />
		<cfargument name="libelle_collab" required="true" type="String" />
		<cfargument name="date_effet" required="true" type="String" />
		<cfargument name="pret" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Affect_equip_emp_v2">
			<cfprocparam  value="#idCollab#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idSim#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#pret#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="affectTermStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfif affectTermStatus gt 0>
			<cfif pret eq 0>
         		<cfset ID_WORD = 3>
			<cfelse>
				<cfset ID_WORD = 14>
			</cfif>
           	<cfset logAction(ID_WORD,1,libelle_sim,libelle_collab,-1,idSim,idCollab,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date_effet)/>
            <cfelse>
                  <cfreturn affectTermStatus>
            </cfif>
		<cfreturn affectTermStatus>
	</cffunction>
	
	<cffunction name="affectLigneToSIM_V3" access="remote" returntype="numeric">
		<cfargument name="idLigne" required="true" type="Numeric">
		<cfargument name="idSIM" required="true" type="Numeric">
		<cfargument name="libelle_Ligne" required="true" type="String">
		<cfargument name="libelle_SIM" required="true" type="String">
		<cfargument name="date" required="true" type="String">
		
		<cfset idPortSims = getPortOfSIM(idSIM)>
		<cfif idPortSims.recordCount gt 0>
			<cfset idport = idPortSims["IDPORT_EQUIPEMENT"][1]>
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.add_lign_SIM">
				<cfprocparam  value="#idport#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#idLigne#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  variable="affecteLigneToSimStatus" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			<cfif affecteLigneToSimStatus gt 0>
	                  <cfset logAction(8,1,libelle_Ligne,libelle_SIM,-1,idSIM,-1,idLigne,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
	            <cfelse>
	                  <cfreturn affecteLigneToSimStatus>
	            </cfif>
			<cfreturn affecteLigneToSimStatus>
		<cfelse>
			<cfreturn -1>
		</cfif>
		<cfreturn -2>
	</cffunction>

	<cffunction name="add_lign_SIM_v2" access="remote" returntype="numeric">
		<cfargument name="idLigne" required="true" type="Numeric">
		<cfargument name="idSIM" required="true" type="Numeric">
		<cfargument name="libelle_Ligne" required="true" type="String">
		<cfargument name="libelle_SIM" required="true" type="String">
		<cfargument name="date" required="true" type="String">
		
		<cfset idPortSims = getPortOfSIM(idSIM)>
		<cfif idPortSims.recordCount gt 0>
			<cfset idport = idPortSims["IDPORT_EQUIPEMENT"][1]>
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.add_lign_SIM">
				<cfprocparam  value="#idport#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#idLigne#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  variable="affecteLigneToSimStatus" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			<cfif affecteLigneToSimStatus gt 0>
	                  <cfset logAction(8,1,libelle_Ligne,libelle_SIM,-1,idSIM,-1,idLigne,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
	            <cfelse>
	                  <cfreturn affecteLigneToSimStatus>
	            </cfif>
			<cfreturn affecteLigneToSimStatus>
		<cfelse>
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.add_lign_SIM_v2">
				<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#idLigne#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#idSIM#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  variable="affecteLigneToSimStatus" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			<cfif affecteLigneToSimStatus gt 0>
	                  <cfset logAction(8,1,libelle_Ligne,libelle_SIM,-1,idSIM,-1,idLigne,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
	            <cfelse>
	                  <cfreturn affecteLigneToSimStatus>
	            </cfif>
			<cfreturn affecteLigneToSimStatus>
		</cfif>
		<cfreturn -2>
	</cffunction>
	
	<cffunction name="affectEquipementToSim_V3" access="remote" returntype="numeric">
		<cfargument name="idSIM" required="true" type="Numeric">
		<cfargument name="idTerm" required="true" type="Numeric">
		<cfargument name="libelle_SIM" required="true" type="String">
		<cfargument name="libelle_Term" required="true" type="String">
		<cfargument name="date" required="true" type="String">
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Affect_equip_parent_v2">
			<cfprocparam  value="#idTerm#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idSIM#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="affectEquipToEquipStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfif affectEquipToEquipStatus gt 0>
                  <cfset logAction(11,1,libelle_SIM,libelle_Term,idTerm,idSIM,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
            <cfelse>
                  <cfreturn affectEquipToEquipStatus>
            </cfif>
		<cfreturn affectEquipToEquipStatus>
	</cffunction>
	
	<cffunction name="addManualNewLigne" access="remote" returntype="numeric">
		<cfargument name="xml" required="true" type="String">
		<cfargument name="idPool" required="true" type="numeric" >
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset idClient = session.user.clientAccessId>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m11.addManualNewLigne">
			<cfprocparam  value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#xml#" cfsqltype="CF_SQL_CLOB" type="in">
			<cfprocparam  value="#idClient#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="qaffectNewLine" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn qaffectNewLine>
	</cffunction>	
	
<!--- ============================================= GESTION DES ECHANGES SIM ET TERMINAL ============================================= --->
	
	<cffunction name="replaceSIM" access="remote" returntype="any">
		<cfargument name="idEmp" 			required="true" type="any" default="-1">
		<cfargument name="idPool" 			required="true" type="Numeric">
		<cfargument name="idSIM_old" 		required="true" type="Numeric">
		<cfargument name="idSIM_new" 		required="true" type="Numeric">
		<cfargument name="libelle_SIM_old" 	required="true" type="String">
		<cfargument name="libelle_SIM_new" 	required="true" type="String">
		<cfargument name="date" 			required="true" type="String">
		<cfargument name="isreplace" 		required="true" type="Numeric" defaut="0">
		<!--- <cfargument name="idSous_tete" required="true" type="Numeric">
		<cfargument name="sous_tete" required="true" type="String"> --->
	 		
			<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
			<cfset qterminal = createObject("component","fr.consotel.consoview.inventaire.equipement.utils.EquipementsUtils")
						   									.fournirEquipementParent(idSIM_old)>
			 
			 <cfstoredproc datasource="#session.offredsn#" procedure="pkg_m11.replaceequipement_v2">
				<cfif idEmp eq -1>
					<cfprocparam  value="#idEmp#" 	cfsqltype="CF_SQL_INTEGER" type="in" null="true">
				<cfelse>
					<cfprocparam  value="#idEmp#" 	cfsqltype="CF_SQL_INTEGER" type="in">
				</cfif>
				<cfprocparam  value="#idSIM_old#" 	cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#idSIM_new#" 	cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#idPool#" 		cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#isreplace#" 	cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  variable="replaceSIM" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc> 
			
			<cfif replaceSIM gt 0>
            	  <cfset logAction(21,1,libelle_SIM_old,libelle_SIM_new,-1,idSIM_old,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
				  <!--- Gestion du second idSIM --->
				  <cfset logAction(21,1,libelle_SIM_old,libelle_SIM_new,-1,idSIM_new,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
				  
				  <!--- Si la sim appartenait a un terminal on log l'action sur le terminale
				  		dissociation de carte / association de carte
				  --->
				   			
		  		<cfif qterminal.recordCount gt 0>
			   		
			   		<!--- dissociation de l'ancienne sim et du terminal --->
			   		<cfset logAction(12,1,libelle_SIM_old,qterminal["IMEI"][1],qterminal["IDEQUIPEMENT"][1],idSIM_old,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
			   		
			   		<!--- association de la nouvelle sim et du terminal --->
			   		<cfset logAction(11,1,libelle_SIM_new,qterminal["IMEI"][1],qterminal["IDEQUIPEMENT"][1],idSIM_new,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
			   		
			   	</cfif> 
            </cfif>
		<cfreturn replaceSIM>
	</cffunction>

	<cffunction name="replaceTERM" access="remote" returntype="numeric">
		<cfargument name="idEmp" required="true" type="any" default="-1">
		<cfargument name="idPool" required="true" type="Numeric">
		<cfargument name="idTerm_old" required="true" type="Numeric">
		<cfargument name="idTerm_new" required="true" type="Numeric">
		<cfargument name="libelle_collab" required="true" type="string">
		<cfargument name="libelle_term_old" required="true" type="String">
		<cfargument name="libelle_term_new" required="true" type="String">
		<cfargument name="date" required="true" type="String">
		
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		
		<cfset ID_EMPLOYE = -1>
		
		<cfif isDefined(idEmp) and idEmp gt 0>
			<cfset ID_EMPLOYE = idEmp>
		</cfif>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.replaceEquipement">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idTerm_old#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idTerm_new#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="replaceTERM" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		
		<cfif replaceTERM gt 0>	
		<!---log pour le premier idTERM --->	
                <cfset logAction(20,1,libelle_term_old,libelle_term_new,idTerm_old,-1,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
		<!---log pour le nouveau terminal--->
				  <cfset logAction(20,1,libelle_term_old,libelle_term_new,idTerm_new,-1,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
				  <!--- log pour le collaboreteur s'il existe --->
				  <cfif idEmp gt 0>
					 <!--- on log la desaffectation du terminal au collaborateur --->
						<cfset logAction(4,1,libelle_term_old,libelle_collab,idTerm_old,-1,idEmp,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
					 <!--- on log l'affectation du terminal au collaborateur --->
					  	<cfset logAction(1,1,libelle_term_new,libelle_collab,idTerm_new,-1,idEmp,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date)/>
				 	</cfif>      
         </cfif>
		
		<cfreturn replaceTERM>
	</cffunction>

<!--- ============================================= GESTION DES DESAFFECTATION =============================================--->

	<cffunction name="releaseTermToEmp" access="remote" returntype="numeric">
		<cfargument name="idTerm" required="true" type="Numeric" />
		<cfargument name="idCollab" required="true" type="Numeric" />
		<cfargument name="libelle_term" required="true" type="String" />
		<cfargument name="libelle_collab" required="true" type="String" />
		<cfargument name="date_effet" required="true" type="String" />
		
			<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Desaffect_equip_emp_v2">
				<!---<cfprocparam  value="#idCollab#" cfsqltype="CF_SQL_INTEGER" type="in">--->
				<cfprocparam  value="#idTerm#" cfsqltype="CF_SQL_INTEGER" type="in">
				<!---<cfprocparam  value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER" type="in">--->
				<cfprocparam  variable="releaseEquipToEmpStatus" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			
			<cfif releaseEquipToEmpStatus gt 0>
				<cfset logAction(4,1,libelle_collab, libelle_term,idTerm,-1,idCollab,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date_effet)/>
		    <cfelse>
		          <cfreturn releaseEquipToEmpStatus>
		    </cfif>
		    
		<cfreturn releaseEquipToEmpStatus>
	</cffunction>

	<cffunction name="releaseSIMToEmp" access="remote" returntype="numeric">
		<cfargument name="idSIM" required="true" type="Numeric" />
		<cfargument name="idCollab" required="true" type="Numeric" />
		<cfargument name="libelle_SIM" required="true" type="String" />
		<cfargument name="libelle_collab" required="true" type="String" />
		<cfargument name="date_effet" required="true" type="String" />
			
			<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Desaffect_equip_emp_v2">
				<!---<cfprocparam  value="#idCollab#" cfsqltype="CF_SQL_INTEGER" type="in">--->
				<cfprocparam  value="#idSIM#" cfsqltype="CF_SQL_INTEGER" type="in">
				<!---<cfprocparam  value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER" type="in">--->
				<cfprocparam  variable="releaseEquipToEmpStatus" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			
			<cfif releaseEquipToEmpStatus gt 0>
				<cfset logAction(6,1,libelle_collab,libelle_SIM,-1,idSIM,idCollab,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date_effet)/>
		    <cfelse>
		          <cfreturn releaseEquipToEmpStatus>
		    </cfif>
		    
		<cfreturn releaseEquipToEmpStatus>
	</cffunction>
	
	<cffunction name="releaseLigneToSim" access="remote" returntype="numeric">
		<cfargument name="ID_SIM" 				required="true" type="Numeric"/>
		<cfargument name="IDSOUS_TETE" 			required="true" type="Numeric"/>
		<cfargument name="libelle_SIM" 			required="true" type="String"/>
		<cfargument name="libelle_IDSOUS_TETE" 	required="true" type="String"/>
		<cfargument name="date_effet" 			required="true" type="String"/>
		
			<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_cv_flotte.desaffect_lignesim">
				<cfprocparam  value="#ID_SIM#" 					 cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#IDSOUS_TETE#" 			 cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  variable="releaseLigneToSimStatus" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			
			<cfif releaseLigneToSimStatus GT 0>
				<cfset logAction(10, 1, libelle_SIM, libelle_IDSOUS_TETE, -1, ID_SIM, -1, IDSOUS_TETE,SESSION.PERIMETRE.ID_GROUPE, -2, date_effet)/>
		    <cfelse>
		          <cfreturn releaseLigneToSimStatus>
		    </cfif>
		
		<cfreturn releaseLigneToSimStatus>
	</cffunction>
	
		<cffunction name="releaseSimToLigne" access="remote" returntype="numeric">
		<cfargument name="ID_SIM" 				required="true" type="Numeric"/>
		<cfargument name="IDSOUS_TETE" 			required="true" type="Numeric"/>
		<cfargument name="libelle_SIM" 			required="true" type="String"/>
		<cfargument name="libelle_IDSOUS_TETE" 	required="true" type="String"/>
		<cfargument name="date_effet" 			required="true" type="String"/>
		
			<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_cv_flotte.desaffect_lignesim">
				<cfprocparam  value="#ID_SIM#" 					 cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#IDSOUS_TETE#" 			 cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  variable="releaseLigneToSimStatus" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			
			<cfif releaseLigneToSimStatus GT 0>
				<cfset logAction(10, 1, libelle_IDSOUS_TETE, libelle_SIM, -1, ID_SIM, -1, IDSOUS_TETE, SESSION.PERIMETRE.ID_GROUPE, -2, date_effet)/>
		    <cfelse>
		          <cfreturn releaseLigneToSimStatus>
		    </cfif>
		
		<cfreturn releaseLigneToSimStatus>
	</cffunction>
	
<cffunction name="releaseSimToTerm" access="remote" returntype="numeric">
		<cfargument name="ID_TERM" required="true" type="Numeric" />
		<cfargument name="ID_SIM" required="true" type="Numeric" />
		<cfargument name="libelle_term" required="true" type="String" />
		<cfargument name="libelle_SIM" required="true" type="String" />
		<cfargument name="date_effet" required="true" type="String" />
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
	<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Desaffect_equip_v2">
		<cfprocparam  value="#ID_SIM#" cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocparam  variable="qReleaseSimToTerm"   cfsqltype="CF_SQL_INTEGER" type="out">
	</cfstoredproc>
	<cfif qReleaseSimToTerm gt 0>
		<cfset logAction(12,1,libelle_term,libelle_SIM,ID_TERM,ID_SIM,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date_effet)/>
    <cfelse>
          <cfreturn qReleaseSimToTerm>
    </cfif>
	<cfreturn qReleaseSimToTerm>
</cffunction>

<cffunction name="Desaffect_term_emp" access="remote" returntype="numeric">
		<cfargument name="idTerm" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Desaffect_term_emp">
			<cfprocparam  value="#idTerm#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn retour >
</cffunction>

<!--- ============================================= PROCEDURES POUR LE GRID PRINCIPAL ============================================= --->
<cffunction name="getEquipements_V3" access="remote" returntype="query">
	<cfargument name="idracine" required="true" type="numeric" />
	<cfargument name="search" required="true" type="String" />
	<cfargument name="idPool" required="true" type="Numeric" />
	<cfargument name="afficherTout" required="true" type="Numeric" />
	<cfargument name="vue" required="true" type="string" default=""/>
	
	<cfset methodName = "searchFlotte_Col_sim_mob_lig_5">
	
	<cfswitch expression="#lcase(vue)#">
		<cfcase value="accueil">
			<cfset methodName = 'searchFlotte_accueil'>
		</cfcase>
		
		<cfcase value="collaborateurs">
			<cfset methodName = 'searchFlotte_annuaire'>
		</cfcase>
		
		<cfcase value="lignes">
			<cfset methodName = 'searchFlotte_lignes'>
		</cfcase>
		
		<cfcase value="equipements">
			<cfset methodName = 'searchFlotte_terminal'>
		</cfcase>
		
		<cfdefaultcase>
			<cfset methodName = 'searchFlotte_Col_sim_mob_lig_5'>
		</cfdefaultcase>
	</cfswitch>
	
	<cfset maxRow = 100>
	
	
	
	
	
	<cfif afficherTout>
		<cfset maxRow = -1>
	</cfif>
	
	<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
	<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.#methodName#" >
		<cfprocparam  value="#idracine#" cfsqltype="CF_SQL_INTEGER">
		<cfprocparam  value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER">
		<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR">
		<cfif idPool neq -2>
			<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER">
		<cfelse>
			<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER" null="true">
		</cfif>
		<cfprocresult name="qGetEquipements" maxrows="#maxRow#">
	</cfstoredproc>
	<cfreturn qGetEquipements>
</cffunction>

<!--- ============================================= SUSPENSION / REACTIVATION DE LA LIGNE ============================================= --->

<!--- <cffunction name="suspendLigne" access="remote" returntype="Any">
	
	<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="">
		<cfprocresult name="qSuspendLigne">
	</cfstoredproc>
	<cfreturn qSuspendLigne>
	
</cffunction> --->
	
<cffunction name="getEquipements_v4" access="remote" returntype="query">
	<cfargument name="search" required="true" type="String" />
	<cfargument name="idPool" required="true" type="Numeric" />
	<cfargument name="columnSearch" required="true" type="Numeric" />
	<cfargument name="afficherTout" required="true" type="Numeric" />
	<cfargument name="vue" required="true" type="string" default=""/>
	<cfset codeLangue=session.user.globalization>
	
	<!--- <cfset methodName = "searchFlotte_Col_sim_mob_lig_5"> --->
	
	<cfset maxRow = 100>
	<cfif afficherTout>
		<cfset maxRow = -1>
	</cfif>
	
	<cfset methodName = "searchFlotte_accueil_v6">
	
	<cfswitch expression="#lcase(vue)#">
		<cfcase value="accueil">
			<cfset methodName = 'searchFlotte_accueil_v6'>
		</cfcase>
		
		<cfcase value="collaborateurs">
			<cfset methodName = 'searchFlotte_annuaire_v4'>
		</cfcase>
		
		<cfcase value="lignes">
			<cfset methodName = 'searchFlotte_lignes_v5'>
		</cfcase>
		
		<cfcase value="equipements">
			<cfset methodName = 'searchFlotte_terminal_v4'>
		</cfcase>
		
		<cfcase value="etats">
				<cfset methodName = 'searchFlotteEtat_v3'>
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.#methodName#" >
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocresult name="qGetEquipements" maxrows="#maxRow#">
				</cfstoredproc>
				<cfreturn qGetEquipements>
		</cfcase>
		
		<cfdefaultcase>
			<cfset methodName = 'searchFlotte_accueil_v6'>
		</cfdefaultcase>
	</cfswitch>
	
	
	
	<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
	<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.#methodName#" >
		<cfprocparam  value="#idracine#" cfsqltype="CF_SQL_INTEGER">
		<cfprocparam  value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER">
		<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR">
		<cfif idPool neq -2>
			<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER">
		<cfelse>
			<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER" null="true">
		</cfif>
		<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_INTEGER">
		<cfprocresult name="qGetEquipements" maxrows="#maxRow#">
	</cfstoredproc>
	<cfreturn qGetEquipements>
</cffunction>

<cffunction name="getEquipements_orga_V4" access="remote" returntype="query">
	<cfargument name="search" required="true" type="String" />
	<cfargument name="idPool" required="true" type="Numeric" />
	<cfargument name="afficherTout" required="true" type="Numeric" />
	<cfargument name="vue" required="true" type="string" default=""/>
	<cfargument name="filtreOrga" required="true" type="numeric" default=""/>
	
	<cfset methodName = "searchFlotte_Col_sim_mob_lig_5">

	<!--- <cfabort showerror=" search=#search#  idPool=#idPool#  afficherTout=#afficherTout#  vue=#vue#  filtreOrga=#filtreOrga#  idracine=#idracine#"> --->
	
	<cfset maxRow = 100>
	<cfif afficherTout>
		<cfset maxRow = -1>
	</cfif>
	
	<!--- <cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE> --->
	<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.searchFlotte_annuaire_v4" >
		<cfprocparam  value="#idracine#" cfsqltype="CF_SQL_INTEGER">
		<cfprocparam  value="#filtreOrga#" cfsqltype="CF_SQL_INTEGER">
		<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR">
		<cfif idPool neq -2>
			<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER">
		<cfelse>
			<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER" null="true">
		</cfif>
		<cfprocresult name="qGetEquipements_orga" maxrows="#maxRow#">
	</cfstoredproc>
	<cfreturn qGetEquipements_orga>
</cffunction>

<cffunction name="Liste_emp_sans_equipements_V3" description="retourne une liste d'�quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfargument name="IDpool" required="true" type="Numeric" />		
		<cfargument name="columnSearch" required="true" type="Numeric" />
		<cfargument name="afficherTout" required="true" type="Numeric" />
		<cfargument name="univers" required="false" type="string" default="accueil"/>
		<cfset maxRow = 100>
		<cfif afficherTout>
			<cfset maxRow = -1>
		</cfif>
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_emp_sans_equipements_V3" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
				<cfif IDpool neq -2>
				<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
				<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="result"  maxrows="#maxRow#">
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>

<cffunction name="Liste_emp_pls_term_V5" description="retourne une liste d'�quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfargument name="IDpool" required="true" type="Numeric" />
		<cfargument name="columnSearch" required="true" type="Numeric" />
		<cfargument name="afficherTout" required="true" type="Numeric" />
		<cfargument name="univers" required="false" type="string" default="accueil"/>
		
		
		<cfswitch expression="#lcase(univers)#">
			<cfcase value="accueil">
				<cfset methodName = 'accueil'>
			</cfcase>
			
			<cfcase value="collaborateurs">
				<cfset methodName = 'annuaire'>
			</cfcase>
			
			<cfcase value="lignes">
				<cfset methodName = 'lignes'>
			</cfcase>
			
			<cfcase value="equipements">
				<cfset methodName = 'terminal'>
			</cfcase>
			
			<cfdefaultcase>
				<cfset methodName = 'accueil'>
			</cfdefaultcase>
		</cfswitch>
	
		<cfset maxRow = 100>
		<cfif afficherTout>
			<cfset maxRow = -1>
		</cfif>
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.LISTE_EMP_PLS_TERM_V6" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#methodName#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
				<cfif IDpool neq -2>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
				<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="result"  maxrows="#maxRow#">
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>

<cffunction name="Liste_emp_SIM_sans_term_V4" description="retourne une liste d'�quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfargument name="IDpool" required="true" type="Numeric" />
		<cfargument name="columnSearch" required="true" type="Numeric" />
		<cfargument name="afficherTout" required="true" type="Numeric" />
		<cfargument name="univers" required="false" type="string" default="accueil"/>
		<cfset codeLangue=session.user.globalization>
		<cfset maxRow = 100>
		<cfif afficherTout>
			<cfset maxRow = -1>
		</cfif>
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_emp_SIM_sans_term_V5" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
					<cfif IDpool neq -2>
				<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_INTEGER">				
					<cfprocparam  value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR" >				
					<cfprocresult name="result"  maxrows="#maxRow#">
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>

<cffunction name="Liste_SIM_sans_term_v2" description="retourne une liste des sim sans terminaux associés" access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="IDpool" required="true" type="Numeric" />

		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_SIM_sans_term_v2" blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
			<cfif IDpool neq -2>
				<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER">
			<cfelse>
				<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER" null="true">
			</cfif>
			<cfprocresult name="result">
		</cfstoredproc>
		
		<cfreturn result /> 
</cffunction>

	<cffunction name="Liste_SIM_sans_ligne_V5" description="retourne une liste d'?quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" 	required="true" type="Numeric" />
		<cfargument name="search" 		required="true" type="String" />
		<cfargument name="IDpool" 		required="true" type="Numeric" />
		<cfargument name="columnSearch" required="true" type="Numeric" />
		<cfargument name="afficherTout" required="true" type="Numeric" />
		<cfargument name="univers" 		required="false" type="string" default="accueil"/>
		
			<cfset maxRow = 100>
			
			<cfif afficherTout>
				<cfset maxRow = -1>
			</cfif>
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_cv_flotte.listesimdispo_v2" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
				<cfif IDpool neq -2>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
				<cfprocparam  value="#afficherTout#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="result"/>
			</cfstoredproc>
			
		<cfreturn result /> 
	</cffunction>

<cffunction name="Liste_SIM_sans_emp_v5" description="retourne une liste d'�quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfargument name="IDpool" required="true" type="Numeric" />
		<cfargument name="columnSearch" required="true" type="Numeric" />
		<cfargument name="afficherTout" required="true" type="Numeric" />
		<cfargument name="univers" required="false" type="string" default="accueil"/>
		<cfset maxRow = 100>
		<cfif afficherTout>
			<cfset maxRow = -1>
		</cfif>
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_SIM_sans_emp_v6" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
					<cfif IDpool neq -2>
				<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
				<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="result"  maxrows="#maxRow#">
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>

<cffunction name="Liste_terminaux_sans_emp_v6" description="retourne une liste d'�quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfargument name="IDpool" required="true" type="Numeric" />
		<cfargument name="columnSearch" required="true" type="Numeric" />
		<cfargument name="afficherTout" required="true" type="Numeric" />
		<cfargument name="univers" required="false" type="string" default="accueil"/>
		<cfset maxRow = 100>
		<cfif afficherTout>
			<cfset maxRow = -1>
		</cfif>
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_terminaux_sans_emp_v6"  blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
				<cfif IDpool neq -2>
				<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
				<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="result"  maxrows="#maxRow#">
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>

<cffunction name="get_equip_rebut" description="retourne une liste d'�quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfargument name="columnSearch" required="true" type="Numeric" />
		<cfargument name="IDpool" required="true" type="Numeric" />
		<cfargument name="afficherTout" required="true" type="Numeric" />
		<cfargument name="univers" required="false" type="string" default="accueil"/>
		
				
		<cfset maxRow = 100>
		<cfif afficherTout>
			<cfset maxRow = -1>
		</cfif>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.liste_equip_rebut_v3" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfif IDpool neq -2>
				<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
				<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="result" maxrows="#maxRow#"/>
			</cfstoredproc>
			
		<cfreturn result /> 
</cffunction>

<cffunction name="get_collab_rebut" description="retourne une liste d'�quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfargument name="columnSearch" required="true" type="Numeric" />
		<cfargument name="IDpool" required="true" type="Numeric" />
		<cfargument name="afficherTout" required="true" type="Numeric" />
		<cfargument name="univers" required="false" type="string" default="accueil"/>
		<cfset codeLangue=session.user.globalization>
				
		<cfset maxRow = 100>
		<cfif afficherTout>
			<cfset maxRow = -1>
		</cfif>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.liste_collab_rebut_v5" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfif IDpool neq -2>
				<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
				<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR" >
				<cfprocresult name="result" maxrows="#maxRow#"/>
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>

<cffunction name="get_collab_rebut_av_equip" description="retourne une liste d'�quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfargument name="columnSearch" required="true" type="Numeric" />
		<cfargument name="IDpool" required="true" type="Numeric" />
		<cfargument name="afficherTout" required="true" type="Numeric" />
		<cfargument name="univers" required="false" type="string" default="accueil"/>
		<cfset codeLangue=session.user.globalization>
				
		<cfset maxRow = 100>
		<cfif afficherTout>
			<cfset maxRow = -1>
		</cfif>
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.liste_collab_rebut_av_equip_v5" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfif IDpool neq -2>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>	
				<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_INTEGER">	
				<cfprocparam  value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR" >	
				<cfprocresult name="result" maxrows="#maxRow#"/>
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>

<cffunction name="get_sav_equip" description="retourne une liste d'�quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfargument name="columnSearch" required="true" type="Numeric" />
		<cfargument name="IDpool" required="true" type="Numeric" />
		<cfargument name="afficherTout" required="true" type="Numeric" />
		<cfargument name="univers" required="false" type="string" default="accueil"/>
		<cfset codeLangue=session.user.globalization>
				
		<cfset maxRow = 100>
		<cfif afficherTout>
			<cfset maxRow = -1>
		</cfif>
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.liste_equip_SAV_v3" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfif IDpool neq -2>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#IDpool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>	
				<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_INTEGER">	
				<cfprocparam  value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR" >	
				<cfprocresult name="result" maxrows="#maxRow#"/>
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>

<cffunction name="get_Ligne_sans_sim" access="remote" returntype="query">
	<cfargument name="ID_POOL" required="true" type="Numeric" />
	<!--- <cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE> --->
	<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_Ligne_sans_sim_v2">
		<cfprocparam  value="#getIdRacine()#" cfsqltype="CF_SQL_INTEGER">
		<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocresult name="qGetLigneSansSim">
	</cfstoredproc>
	<cfreturn qGetLigneSansSim>
</cffunction>

<!--- MISE AU REBUT + SAV --->
<cffunction name="putEquipRecycle_term_v3" access="remote" returntype="numeric">
	<cfargument name="idTerm" required="true" type="Numeric" />
	<cfargument name="libelle_term" required="true" type="String" />
	<cfargument name="date_effet" required="true" type="String" />
	<cfargument name="commentaire" required="true" type="String" />
	<cfargument name="id_cause" required="true" type="Numeric" />
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.putEquipRecycle">
		<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idTerm#"/>
		<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER" type="out">
	</cfstoredproc>
	<cfif result gt 0>
    	<cfset logAction(16,1,libelle_term,"",idTerm,-1,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date_effet,commentaire,id_cause,3)/>
    </cfif>
	<cfreturn result>
</cffunction>

<cffunction name="putEquipRecycleSIM_v3" access="remote" returntype="numeric">
	<cfargument name="idSIM" required="true" type="Numeric" />
	<cfargument name="libelle_SIM" required="true" type="String" />
	<cfargument name="date_effet" required="true" type="String" />
	<cfargument name="commentaire" required="true" type="String" />
	<cfargument name="id_cause" required="true" type="Numeric" />
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.putEquipRecycle">
		<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idSIM#"/>
		<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER" type="out">
	</cfstoredproc>
	<cfif result gt 0>
    	<cfset logAction(17,1,libelle_SIM,"",-1,idSIM,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date_effet,commentaire,id_cause,3)/>
    <cfelse>
    	<cfreturn result>
    </cfif>
	<cfreturn result>
</cffunction>

<cffunction name="putSAVEEquip_term" access="remote" returntype="numeric">
	<cfargument name="idTerm" required="true" type="Numeric" />
	<cfargument name="libelle_term" required="true" type="String" />
	<cfargument name="date_effet" required="true" type="String" />

	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.putSAVEquip">
		<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDTerm#"/>
		<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER" type="out">
	</cfstoredproc>
	<cfif result gt 0>
    	<cfset logAction(19,1,libelle_term,"",idTerm,-1,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date_effet,"",1,3)/>
    <cfelse>
    	<cfreturn result>
    </cfif>
	<cfreturn result>
</cffunction>

<cffunction name="putEquipActive" returntype="numeric" access="remote" output="true">
		<cfargument name="idEquip" required="true" type="numeric" />
		<cfargument name="libelle_term" required="true" type="String" />
		<cfargument name="date_effet" required="true" type="String" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.putEquipActive">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquip#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="result">
			</cfstoredproc>
			<cfif result gt 0>
    	<cfset logAction(22,1,libelle_term,"",idEquip,-1,-1,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date_effet,"",3,3)/>
   		 <cfelse>
    	<cfreturn result>
    </cfif>
	<cfreturn result>
</cffunction>

<cffunction name="putOutEmploye" access="remote" returntype="numeric">
	<cfargument name="idEmp" required="true" type="Numeric" />
	<cfargument name="libelleEmp" required="true" type="String" />
	<cfargument name="date_effet" required="true" type="String" />
	<cfargument name="liste_equip" required="true" type="String" />
	<cfargument name="liste_ligne" required="true" type="String" />
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.putOutEmploye">
		<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idEmp#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_effet#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#liste_equip#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#liste_ligne#"/>
		<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER" type="out">
	</cfstoredproc>
	<cfif result gt 0>
    	<cfset logAction(15,1,libelleEmp,"",-1,-1,idEmp,-1,SESSION.PERIMETRE.ID_GROUPE,-2,date_effet)/>
    <cfelse>
    	<cfreturn result>
    </cfif>
	<cfreturn result>
</cffunction>

<!--- PROCEDURES RECUPERATION D INFO --->

<cffunction name="getTypeLigne" access="remote" returntype="query">
	<cfset codeLangue=session.user.globalization>

	<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m24.l_type_ligne_v2">
		<cfprocparam  value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR" >
		<cfprocresult name="retour">
	</cfstoredproc>
	<cfreturn retour>
</cffunction>

<cffunction name="getInfosRebusLigne_V3" access="remote" returntype="query">
	<cfargument name="idSIM" required="true" type="Numeric" />
	<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.getInfosRebusLigne">
		<cfprocparam  value="#idSIM#" cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocparam  variable="retour" cfsqltype="CF_SQL_INTEGER" type="out">
	</cfstoredproc>
	<cfreturn retour>
</cffunction>

<!--- LISTE DISPONIBLE --->
	<cffunction name="getAffectedSimToEmp" access="remote" returntype="query">
		<cfargument name="IDGROUPE_RACINE" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_SIM_emp_v2">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
				<cfif ID_POOL neq -2>
				<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
			<cfprocresult name="qGetAffectedSimToEmp">
		</cfstoredproc>
		<cfreturn qGetAffectedSimToEmp>
</cffunction>

	<cffunction name="listeTermDispo" access="remote" returntype="query">
		<cfargument name="IDGROUPE_RACINE" required="true" type="String" />
		<cfargument name="SEARCH" required="true" type="String" />
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		
		<cfstoredproc datasource="#session.offredsn#" procedure="pkg_cv_flotte.listeTermDispo_v2">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#SEARCH#" 			cfsqltype="CF_SQL_VARCHAR">
			<cfif ID_POOL neq -2>
				<cfprocparam  value="#ID_POOL#" 	cfsqltype="CF_SQL_INTEGER">
			<cfelse>
				<cfprocparam  value="#ID_POOL#" 	cfsqltype="CF_SQL_INTEGER" null="true">
			</cfif>
			<cfprocresult name="qGetListeEmploye">
		</cfstoredproc>
		
		<cfreturn qGetListeEmploye>
	</cffunction>
	
	<cffunction name="listeSIMDispo" access="remote" returntype="query">
		<cfargument name="IDGROUPE_RACINE" required="true" type="Numeric" />
		<cfargument name="SEARCH" required="true" type="String" />
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="BOOL_SANS_LIGNE" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.listeSIMDispo">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#SEARCH#" cfsqltype="CF_SQL_VARCHAR">
				<cfif ID_POOL neq -2>
				<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
			<cfprocparam  value="#BOOL_SANS_LIGNE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetListeEmploye">
		</cfstoredproc>
		<cfreturn qGetListeEmploye>
	</cffunction>
		
	<cffunction name="getAffectedTermToEmp" access="remote" returntype="query">
		<cfargument name="IDGROUPE_RACINE" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_terminaux_emp_v3">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetAffectedTerm">
		</cfstoredproc>
		<cfreturn qGetAffectedTerm>
	</cffunction>
	
	<cffunction name="Lst_all_sim_emp_sans_term" access="remote" returntype="query">
		<cfargument name="idRacine" required="true" type="Numeric" />
		<cfargument name="idEmp" required="true" type="Numeric" />
		<cfargument name="idPool" required="true" type="Numeric" />
		
		<!--- <cfabort showerror="racine=#idRacine# idEmp=#idEmp# idPool=#idPool#"> --->
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Lst_all_sim_emp_sans_term">
			<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#idEmp#" cfsqltype="CF_SQL_INTEGER">
				<cfif idPool neq -2>
				<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
			<cfprocresult name="QgetAllNonAffectedSimToEmp_v3">
		</cfstoredproc>
		<cfreturn QgetAllNonAffectedSimToEmp_v3>
	</cffunction>
	
	<cffunction name="Liste_all_sim_emp" access="remote" returntype="query">
		<cfargument name="idRacine" required="true" type="Numeric" />
		<cfargument name="idEmp" required="true" type="Numeric" />
		<cfargument name="idPool" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_all_sim_emp_v2">
			<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#idEmp#" cfsqltype="CF_SQL_INTEGER">
				<cfif idPool neq -2>
				<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER">
				<cfelse>
					<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER" null="true">
				</cfif>
			<cfprocresult name="QgetAllNonAffectedSimToEmp_v3">
		</cfstoredproc>
		<cfreturn QgetAllNonAffectedSimToEmp_v3>
	</cffunction>
	
	<cffunction name="getNonAffectedTerm" access="remote" returntype="query">
		<cfargument name="IDGROUPE_RACINE" required="true" type="String" />
		<cfargument name="SEARCH" required="true" type="String" />
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="COLUMN_SEARCH" required="true" type="Numeric" Default="1"/>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_Terminaux_sans_emp_v6">
				<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#SEARCH#" cfsqltype="CF_SQL_VARCHAR">
				<cfif ID_POOL neq -2>
					<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
					<cfelse>
						<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER" null="true">
					</cfif>
				<cfprocparam  value="#COLUMN_SEARCH#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNonAffectTerm">
		</cfstoredproc>
		<cfreturn qGetNonAffectTerm>
	</cffunction>
	
	<cffunction name="getHistorique" access="remote" returntype="query">
		<cfargument name="IDGROUPE_RACINE" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfargument name="ID_TERMINAL" required="true" type="Numeric" />
		<cfargument name="ID_SIM" required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE" required="true" type="Numeric" />
		<cfset codeLangue=session.user.globalization>
		
		<cfset IDGROUPE_RACINE = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Historique_action_v2">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_TERMINAL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_SIM#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR" >
			<cfprocresult name="qGetHistorique">
		</cfstoredproc>
		<cfreturn qGetHistorique>
	</cffunction>
	
	<cffunction name="getPortOfSIM" access="remote" returntype="query">
		<cfargument name="ID_EQUIPEMENT" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.liste_ports_eq">
			<cfprocparam  value="#ID_EQUIPEMENT#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetPortOfSIM">
		</cfstoredproc>
		<cfreturn qGetPortOfSIM>
	</cffunction>

	<cffunction name="saveMultipleEquip" access="remote" returntype="numeric">
		
		<cfargument name="ID_REVENDEUR_TERM" required="true" type="Numeric" />
		<cfargument name="ID_MODELE_TERM" required="true" type="Numeric" />
		<cfargument name="ID_OP_SIM" required="true" type="Numeric" />
		<cfargument name="LISTE_EQUIP_COLLAB" required="true" type="String"/>
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="DATE_DEB_CONTRAT" required="true" type="String" />
		<cfargument name="DUREE_CONTRAT" required="true" type="Numeric" />
		
		
		<cfset IDGROUPE_RACINE = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.saveMultipleEquip">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_REVENDEUR_TERM#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_MODELE_TERM#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_OP_SIM#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#LISTE_EQUIP_COLLAB#" cfsqltype="CF_SQL_CLOB">
			
			<cfif ID_POOL neq -2>
				<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfelse>
				<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER" null="true">
			</cfif>
			<cfif DUREE_CONTRAT neq 0>
				<cfprocparam  value="#DATE_DEB_CONTRAT#" cfsqltype="CF_SQL_VARCHAR">
			<cfelse>
				<cfprocparam  value="#DATE_DEB_CONTRAT#" cfsqltype="CF_SQL_VARCHAR" null="true">
			</cfif>
			
			<cfprocparam  value="#DUREE_CONTRAT#" cfsqltype="CF_SQL_INTEGER">
		<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>

	<cffunction name="saveMultipleEquip_v2" access="remote" returntype="numeric">
		<cfargument name="ID_REVENDEUR_TERM" 	required="true" type="Numeric"/>
		<cfargument name="ID_MODELE_TERM" 		required="true" type="Numeric"/>
		<cfargument name="ID_OP_SIM" 			required="true" type="Numeric"/>
		<cfargument name="LISTE_EQUIP_COLLAB" 	required="true" type="String"/>
		<cfargument name="ID_POOL" 				required="true" type="Numeric"/>
		<cfargument name="DATE_DEB_CONTRAT" 	required="true" type="String" hint="contrat de garantie"/>
		<cfargument name="DUREE_CONTRAT" 		required="true" type="Numeric"/>
		<cfargument name="DATE_DEB_LIGNE" 		required="true" type="String"/>
		<cfargument name="DUREE_ENGAGEMENT" 	required="true" type="Numeric"/>
		<cfargument name="is_pool_rev" 			required="true" type="Numeric"/>		
		
			<cfset IDGROUPE_RACINE = SESSION.PERIMETRE.ID_GROUPE>

			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.saveMultipleEquip_v3">
				<cfprocparam  value="#IDGROUPE_RACINE#" 	cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine">
				<cfprocparam  value="#ID_REVENDEUR_TERM#" 	cfsqltype="CF_SQL_INTEGER" 	variable="p_idrevendeur">
				<cfprocparam  value="#ID_MODELE_TERM#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_ideq_fourn">
				<cfprocparam  value="#ID_OP_SIM#" 			cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur">
				<cfprocparam  value="#LISTE_EQUIP_COLLAB#" 	cfsqltype="CF_SQL_CLOB"		variable="p_lst_equip">
				
				<cfif ID_POOL neq 0>
					<cfprocparam  value="#ID_POOL#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_idpool">
				<cfelse>
					<cfprocparam  value="#ID_POOL#" 		cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" null="true">
				</cfif>
				
				<cfif DUREE_CONTRAT neq 0>
					<cfprocparam  value="#DATE_DEB_CONTRAT#" cfsqltype="CF_SQL_VARCHAR"	variable="p_date_debut_garantie">
				<cfelse>
					<cfprocparam  value="#DATE_DEB_CONTRAT#" cfsqltype="CF_SQL_VARCHAR" variable="p_date_debut_garantie" null="true">
				</cfif>
				
				<cfprocparam  value="#DUREE_CONTRAT#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_duree_garantie">
				
				<cfif DATE_DEB_LIGNE neq 0>
					<cfprocparam  value="#DATE_DEB_LIGNE#" 	cfsqltype="CF_SQL_VARCHAR"	variable="p_date_debut_ligne">
				<cfelse>
					<cfprocparam  value="#DATE_DEB_LIGNE#" 	cfsqltype="CF_SQL_VARCHAR" 	variable="p_date_debut_ligne" null="true">
				</cfif>
				
				<cfprocparam  value="#DUREE_ENGAGEMENT#" 	cfsqltype="CF_SQL_INTEGER"	variable="p_duree_engagement">	
				<cfprocparam  value="#is_pool_rev#" 		cfsqltype="CF_SQL_INTEGER"	variable="p_is_pool_rev">		
				<cfprocparam  variable="p_retour" 				cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="ValidatesaveMultipleEquip" access="remote" returntype="struct">
		
		<cfargument name="LISTE_EQUIP_COLLAB" required="true" type="String"/>
		
		<cfset IDGROUPE_RACINE = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.ValidatesaveMultipleEquip">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#LISTE_EQUIP_COLLAB#" cfsqltype="CF_SQL_CLOB">
			<cfprocparam  variable="NB_IMEI" cfsqltype="cf_SQL_INTEGER" type="out">
			<cfprocparam  variable="NB_SIM" cfsqltype="cf_SQL_INTEGER" type="out">
			<cfprocparam  variable="NB_LIGNE" cfsqltype="cf_SQL_INTEGER" type="out">
			<cfprocparam  variable="NB_COLLAB" cfsqltype="cf_SQL_INTEGER" type="out">
			<cfprocparam  variable="RES" cfsqltype="cf_SQL_INTEGER" type="out">
			<cfprocparam  variable="ERREUR" cfsqltype="CF_SQL_CLOB" type="out">
			
		</cfstoredproc>
		
		<cfset MY_RESULT = StructNew() />
		<cfset MY_RESULT.NBIMEI = #NB_IMEI# />
		<cfset MY_RESULT.NBSIM = #NB_SIM# />
		<cfset MY_RESULT.NBLIGNE = #NB_LIGNE# />
		<cfset MY_RESULT.NBCOLLAB = #NB_COLLAB# />
		<cfset MY_RESULT.res = #RES# />
		<cfset MY_RESULT.erreur = #ERREUR# />
		
		<cfreturn MY_RESULT>
	</cffunction>	
		
	<cffunction name="getFabOp" access="remote" returntype="query">
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.getFabOp">
			<cfprocresult name="return_getFabOp">
		</cfstoredproc>
		<cfreturn return_getFabOp>
	</cffunction>

	<cffunction name="Get_all_revendeur" access="remote" returntype="query">
		<cfargument name="chaine" 	required="false"  	type="String" 	default="" />
		<cfargument name="segment"	required="false" 	type="numeric" 	default="0" hint="0:tous, 1:fixedata et 2:mobile"/>
		<cfset IDRACINE = SESSION.PERIMETRE.ID_GROUPE > <!--- la racine --->
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m24.Get_all_revendeur">
			<cfprocparam  value="#IDRACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#chaine#" 	cfsqltype="CF_SQL_VARCHAR" >
			<cfprocparam  value="#segment#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="return_Get_all_revendeur">
		</cfstoredproc>
		<cfreturn return_Get_all_revendeur>
	</cffunction>

	<cffunction name="fournirRevendeurs" access="public" returntype="Query" output="false">
		<cfargument name="idPoolGestion" 		required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 	required="true" type="numeric"/>
		<cfargument name="clefRecherche" 		required="true" type="String" />
		
		<cfset IDRACINE = SESSION.PERIMETRE.ID_GROUPE > <!--- la racine --->
		<cfset IDGESTIONNAIRE = SESSION.USER.CLIENTACCESSID > <!--- Id Client --->
		
		<!--- <cfabort showerror="racine=#IDRACINE# idProfilEquipement=#idProfilEquipement# idPool=#idPoolGestion# gest=#IDGESTIONNAIRE# clefRecherche=#clefRecherche#" > --->
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.get_revendeur_v3">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#IDGESTIONNAIRE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 				value="#idPoolGestion#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_Chaine" 				value="#clefRecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_idracine" 			value="#IDRACINE#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="fournirListeProfiles" access="remote" returntype="Query" description="liste profile">
		<cfargument name="idracine" 			required="true" type="numeric"/>
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>	
			<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_SNCF.List_profil_gestionnaire_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#idGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine"  		value="#idracine#" 		null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
		<cfreturn p_result>
	</cffunction>

	<cffunction name="getMatriculeEmploye" access="remote" returntype="query">
		<cfargument name="NBCLE" required="true" type="Numeric" />
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.getMatricule">
			<cfprocparam  value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#NBCLE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetMatriculeEmploye">
		</cfstoredproc>
		<cfreturn qGetMatriculeEmploye>
	</cffunction>

	<cffunction name="savemultiplecollab" access="remote"  returntype="numeric">
		<cfargument name="IDPOOL" required="true" type="Numeric" />
		<cfargument name="LISTECOLLAB" required="true" type="STRING" />
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.savemultiplecollab">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#IDPOOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#LISTECOLLAB#" cfsqltype="CF_SQL_CLOB">
			<cfprocresult name="qSavemultiplecollab">
		</cfstoredproc>
		<cfreturn qSavemultiplecollab>
	</cffunction>

	<cffunction name="chekcMatricule" access="remote"  returntype="numeric">
		<cfargument name="MATRICULE" required="true" type="Numeric" />
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.chekcMatricule">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#MATRICULE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qChekcMatricule">
		</cfstoredproc>
		<cfreturn qChekcMatricule>
	</cffunction>

	<cffunction name="addEquipementRevendeur" access="remote" returntype="struct" description="add equip in catalogue revendeur">
	      
	      <cfargument name="libelle" type="string" required="true">
	      <cfargument name="reference" type="string" required="true">
	      <cfargument name="reference_distrib" type="string" required="true">
	      <cfargument name="idConstructeur" type="numeric" required="true">
	      <cfargument name="idType" type="numeric" required="true">
	      <cfargument name="idRevendeur" type="numeric" required="true">
	      <cfargument name="insertInConstr" type="numeric" required="true">
	      <cfargument name="prixCat" type="numeric" required="true">
	      <cfargument name="tarif1" type="numeric" required="true">
	      <cfargument name="tarif2" type="numeric" required="true">
	      <cfargument name="tarif3" type="numeric" required="true">
	      <cfargument name="tarif4" type="numeric" required="true">
	      <cfargument name="tarif5" type="numeric" required="true">
	      <cfargument name="tarif6" type="numeric" required="true">
	      <cfargument name="tarif7" type="numeric" required="true">
	      <cfargument name="tarif8" type="numeric" required="true">
	      <cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.addEquipementRevendeur">
	            
	            <cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#reference#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#reference_distrib#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idConstructeur#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idType#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#insertInConstr#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#prixCat#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tarif1#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tarif2#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tarif3#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tarif4#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tarif5#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tarif6#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tarif7#" null="false">
	            <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tarif8#" null="false">
	            
	            
	            <cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	            <cfprocparam type="Out" cfsqltype="CF_SQL_vARCHAR" variable="ref">
	      </cfstoredproc> 
	      <cfset myStruct=structNew()>
	      <cfset myStruct.IDEQUIP = p_retour>
	      <cfset myStruct.REF = ref>
	
	      <cfreturn myStruct>
	</cffunction>

</cfcomponent>
