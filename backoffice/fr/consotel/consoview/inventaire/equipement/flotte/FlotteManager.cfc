<!--- pour surround --->
<cfcomponent name="FlotteManager" alias="fr.consotel.consoview.inventaire.equipement.flotte.FlotteManager">
	<cfset THIS.xmlParamUtil=createObject("component","fr.consotel.util.consoview.xml.XmlParamUtil")>

	<!---
	PARCE QUE C'est chaque fonctionnalitÃ© QUI CONNAIT les rÃ©ponses pour les questions :
	comment ?, ï¿½ quel endroit ?, quand ? etc...
	Pour les logs qui la concernent. Donc l'appel de l'enregistrement du log ce trouve dans cette classe.
	--->
	<cffunction name="logXmlParamIfSpecified" access="remote" returntype="void">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset registerInLogTag = XMLSearch(xmlParamObject,"/PARAMS/REGISTER_IN_LOG")>
		<cfif arrayLen(registerInLogTag) EQ 1>
			<cfset registerVal = VAL(registerInLogTag[1].xmlText)>
			<cflog type="information" text="registerVal = #registerVal#">
			<cfif registerVal EQ 1>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"ID_WORDS")>
				<cfset ID_WORDS=VAL(tmpArray[1].xmlText)>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"ITEM1")>
				<cfset ITEM1=tmpArray[1].xmlText>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"ITEM2")>
				<cfset ITEM2=tmpArray[1].xmlText>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"ID_TERMINAL")>
				<cfset ID_TERMINAL=VAL(tmpArray[1].xmlText)>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"ID_SIM")>
				<cfset ID_SIM=VAL(tmpArray[1].xmlText)>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"ID_EMPLOYE")>
				<cfset ID_EMPLOYE=VAL(tmpArray[1].xmlText)>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"IDSOUS_TETE")>
				<cfset IDSOUS_TETE=VAL(tmpArray[1].xmlText)>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
				<cfset IDGROUPE_RACINE=VAL(tmpArray[1].xmlText)>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"IDGROUPE_CLIENT")>
				<cfset IDGROUPE_CLIENT=VAL(tmpArray[1].xmlText)>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"UID_ACTION")>
				<cfset UID_ACTION=tmpArray[1].xmlText>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"DATE_EFFET")>
				<cfset DATE_EFFET=tmpArray[1].xmlText>
				<cfset tmpArray=THIS.xmlParamUtil.getLogParamValuesByKey(xmlParamObject,"ACTION_PRINCIPALE")>
				<cfset ACTION_PRINCIPALE=VAL(tmpArray[1].xmlText)>
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.log_action_flotte">
					<cfprocparam value="#SESSION.USER.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#ID_WORDS#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#ACTION_PRINCIPALE#" cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocparam value="#ITEM1#" cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocparam value="#ITEM2#" cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocparam value="#ID_TERMINAL#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#ID_SIM#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#UID_ACTION#" cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocparam value="#DATE_EFFET#" cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocparam variable="logStatus" cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
			</cfif>
		<cfelseif arrayLen(registerInLogTag) GT 1>
			<cfthrow type="MULTIPLE_REGISTER_IN_LOG" message="XmlParamUtil - Multiple REGISTER_IN_LOG Tag (#arrayLen(registerInLogTag)#)" detail="1008">
		</cfif>
	</cffunction>

	<cffunction name="getEquipements" access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.searchFlotte_Col_sim_mob_lig_2">
			<cfprocparam  value="#IDRacine#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="qGetEquipements">
		</cfstoredproc>
		<cfreturn qGetEquipements>
	</cffunction>
	
	<cffunction name="getNonAffectedTerm" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlGroupeRacine=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset IDGROUPE_RACINE=VAL(vlGroupeRacine[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_Terminaux_sans_emp">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNonAffectTerm">
		</cfstoredproc>
		<cfreturn qGetNonAffectTerm>
	</cffunction>
	
	<cffunction name="getAffectedTermToEmp" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlGroupeRacine=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset vlIdEmploye=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_EMPLOYE")>
		<cfset IDGROUPE_RACINE=VAL(vlGroupeRacine[1].xmlText)>
		<cfset ID_EMPLOYE=VAL(vlIdEmploye[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_terminaux_emp">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetAffectedTerm">
		</cfstoredproc>
		<cfreturn qGetAffectedTerm>
	</cffunction>
	
	<cffunction name="affectEquipementToEmp" access="remote" returntype="numeric">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlGroupeRacine=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset vlIdEmploye=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_EMPLOYE")>
		<cfset vlIdEquip=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_EQUIPEMENT")>
		<cfset IDGROUPE_RACINE=VAL(vlGroupeRacine[1].xmlText)>
		<cfset ID_EMPLOYE=VAL(vlIdEmploye[1].xmlText)>
		<cfset ID_EQUIP=VAL(vlIdEquip[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Affect_equip_emp">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#ID_EQUIP#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="affectTermStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn affectTermStatus>
	</cffunction>
	
	<cffunction name="affectEquipementToSim" access="remote" returntype="numeric">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlGroupeRacine=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset vlIdTerminal=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_TERMINAL")>
		<cfset vlIdSim=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_SIM")>
		<cfset IDGROUPE_RACINE=VAL(vlGroupeRacine[1].xmlText)>
		<cfset ID_TERMINAL=VAL(vlIdTerminal[1].xmlText)>
		<cfset ID_SIM=VAL(vlIdSim[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Affect_equip_parent">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#ID_TERMINAL#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#ID_SIM#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="affectEquipToEquipStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn affectEquipToEquipStatus>
	</cffunction>
	
	<cffunction name="releaseEquipementToEmp" access="remote" returntype="numeric">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlGroupeRacine=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset vlIdEmploye=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_EMPLOYE")>
		<cfset vlIdEquip=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_EQUIPEMENT")>
		<cfset IDGROUPE_RACINE=VAL(vlGroupeRacine[1].xmlText)>
		<cfset ID_EMPLOYE=VAL(vlIdEmploye[1].xmlText)>
		<cfset ID_EQUIP=VAL(vlIdEquip[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Desaffect_equip_emp">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#ID_EQUIP#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="releaseEquipToEmpStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn releaseEquipToEmpStatus>
	</cffunction>
	
	<cffunction name="getNonAffectedSimToEmp" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlGroupeRacine=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset IDGROUPE_RACINE=VAL(vlGroupeRacine[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_SIM_sans_emp">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNonAffectSimToEmp">
		</cfstoredproc>
		<cfreturn qGetNonAffectSimToEmp>
	</cffunction>
	
	<cffunction name="releaseSimToTerm" access="remote" returntype="numeric">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlIdRacine=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset vlIdSim=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_SIM")>
		<cfset vlIdTerm=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_TERMINAL")>
		<cfset ID_SIM=VAL(vlIdSim[1].xmlText)>
		<cfset ID_TERM=VAL(vlIdTerm[1].xmlText)>
		<cfset IDGROUPE_RACINE=VAL(vlIdRacine[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Desaffect_equip">
			<cfprocparam  value="#ID_TERM#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#ID_SIM#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="qReleaseSimToTerm"   cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn qReleaseSimToTerm>
	</cffunction>
	

	<cffunction name="getAffectedSimToEmp" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlGroupeRacine=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset vlEmploye=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_EMPLOYE")>
		<cfset IDGROUPE_RACINE=VAL(vlGroupeRacine[1].xmlText)>
		<cfset ID_EMPLOYE=VAL(vlEmploye[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_SIM_emp">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetAffectedSimToEmp">
		</cfstoredproc>
		<cfreturn qGetAffectedSimToEmp>
	</cffunction>
	
	<cffunction name="getAllNonAffectedSimToEmp" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlGroupeRacine=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset IDGROUPE_RACINE=VAL(vlGroupeRacine[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_all_SIM_sans_emp">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetAllSimWithoutEmp">
		</cfstoredproc>
		<cfreturn qGetAllSimWithoutEmp>
	</cffunction>
	
	<cffunction name="getAllAffectedSimToEmp" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlGroupeRacine=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset vlEmploye=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_EMPLOYE")>
		<cfset IDGROUPE_RACINE=VAL(vlGroupeRacine[1].xmlText)>
		<cfset ID_EMPLOYE=VAL(vlEmploye[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_All_SIM_emp">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetAllAffectedSimToEmp">
		</cfstoredproc>
		<cfreturn qGetAllAffectedSimToEmp>
	</cffunction>


	<cffunction name="getHistorique" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset IDGROUPE_RACINE=VAL(vTmp[1].xmlText)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_EMPLOYE")>
		<cfset ID_EMPLOYE=VAL(vTmp[1].xmlText)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_TERMINAL")>
		<cfset ID_TERMINAL=VAL(vTmp[1].xmlText)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_SIM")>
		<cfset ID_SIM=VAL(vTmp[1].xmlText)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDSOUS_TETE")>
		<cfset IDSOUS_TETE=VAL(vTmp[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Historique_action">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_TERMINAL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_SIM#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetHistorique">
		</cfstoredproc>
		<cfreturn qGetHistorique>
	</cffunction>

	<cffunction name="getListeEmploye" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset IDGROUPE_RACINE=VAL(vTmp[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_employe">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetListeEmploye">
		</cfstoredproc>
		<cfreturn qGetListeEmploye>
	</cffunction>

	
	<cffunction name="releaseLigneToSim" access="remote" returntype="numeric">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_SIM")>
		<cfset ID_SIM=VAL(vlTmp[1].xmlText)>
		<cfset vlTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDSOUS_TETE")>
		<cfset IDSOUS_TETE=VAL(vlTmp[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Desaffect_LigneSim">
			<cfprocparam  value="#ID_SIM#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="releaseLigneToSimStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn releaseLigneToSimStatus>
	</cffunction>
	
	<cffunction name="getSimSansTerm" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset IDGROUPE_RACINE=VAL(vTmp[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_SIM_sans_term">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetListeSimSansTerm">
		</cfstoredproc>
		<cfreturn qGetListeSimSansTerm>
	</cffunction>
	
	<cffunction name="get_ALL_SIM_sans_Term" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset IDGROUPE_RACINE=VAL(vTmp[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_ALL_SIM_sans_term">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetListeAllSimSansTerm">
		</cfstoredproc>
		<cfreturn qGetListeAllSimSansTerm>
	</cffunction>
	
	<cffunction name="get_terminaux_sans_sim" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset IDGROUPE_RACINE=VAL(vTmp[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_terminaux_sans_sim">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetTerminauxSansSim">
		</cfstoredproc>
		<cfreturn qGetTerminauxSansSim>
	</cffunction>
	
	<cffunction name="get_Ligne_sans_sim" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset IDGROUPE_RACINE=VAL(vTmp[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_Ligne_sans_sim">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetLigneSansSim">
		</cfstoredproc>
		<cfreturn qGetLigneSansSim>
	</cffunction>
	
	<cffunction name="get_terminaux_sans_ligne" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset IDGROUPE_RACINE=VAL(vTmp[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_terminaux_sans_ligne">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetTerminauxSansLigne">
		</cfstoredproc>
		<cfreturn qGetTerminauxSansLigne>
	</cffunction>
	
	<cffunction name="get_SIM_sans_ligne" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDGROUPE_RACINE")>
		<cfset IDGROUPE_RACINE=VAL(vTmp[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Liste_SIM_sans_ligne">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetSimSansLigne">
		</cfstoredproc>
		<cfreturn qGetSimSansLigne>
	</cffunction>
	
	<cffunction name="getPortOfSIM" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"ID_EQUIPEMENT")>
		<cfset ID_EQUIPEMENT=VAL(vTmp[1].xmlText)>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.liste_ports_eq">
			<cfprocparam  value="#ID_EQUIPEMENT#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetPortOfSIM">
		</cfstoredproc>
		<cfreturn qGetPortOfSIM>
	</cffunction>
	
	<cffunction name="affecteLigneToSIM" access="remote" returntype="numeric">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset logXmlParamIfSpecified(xmlParamObject)>
		<cfset vlTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDPORT_EQUIPEMENT")>
		<cfset IDPORT_EQUIPEMENT=VAL(vlTmp[1].xmlText)>
		<cfset vlTmp=THIS.xmlParamUtil.getParamValuesByKey(xmlParamObject,"IDSOUS_TETE")>
		<cfset IDSOUS_TETE=VAL(vlTmp[1].xmlText)>
		<cfset SESSION.TEST=xmlParamObject>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_EQUIPEMENT.add_lign_port">
			<cfprocparam  value="#IDPORT_EQUIPEMENT#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="affecteLigneToSimStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn affecteLigneToSimStatus>  
	</cffunction>
<cffunction name="Liste_emp_sans_equipements_v1" description="retourne une liste d'Ã©quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_emp_sans_equipements_v1">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
					<cfprocresult name="result"/>
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>
<cffunction name="Liste_emp_pls_term_v1" description="retourne une liste d'Ã©quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_emp_pls_term_v1">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
					<cfprocresult name="result"/>
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>
<cffunction name="Liste_emp_SIM_sans_term_v1" description="retourne une liste d'Ã©quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_emp_SIM_sans_term_v1">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
					<cfprocresult name="result"/>
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>
<cffunction name="Liste_SIM_sans_ligne_v2" description="retourne une liste d'Ã©quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_SIM_sans_ligne_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
					<cfprocresult name="result"/>
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>
<cffunction name="Liste_SIM_sans_emp_v2" description="retourne une liste d'Ã©quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_SIM_sans_emp_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
					<cfprocresult name="result"/>
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>
<cffunction name="Liste_terminaux_sans_emp_v2" description="retourne une liste d'Ã©quipements"access="remote" returntype="query">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_terminaux_sans_emp_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDRacine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#search#"/>
					<cfprocresult name="result"/>
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>









<!--- 
	EXPORT EN CSV
 --->

<cffunction name="exporterEnCSV" access="remote" returntype="string">
		<cfargument name="IDRacine" required="true" type="Numeric" />
		<cfargument name="search" required="true" type="String" />	
		<cfargument name="methodeName" required="true" type="string">
		<cfargument name="libelle" required="true" type="string">
		<!--- <cftry>  --->
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"flotteGSM_#libelle#.csv">				
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset qDetail= evaluate("#methodeName#(IDRacine,search)")>				
				<cfset NewLine = Chr(13) & Chr(10)>
				<cfset space = Chr(13) & Chr(10)>		
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="utf-8">
				<cfcontent type="text/plain">
				<cfoutput>Collaborateur;IMEI;MARQUE;MODELE;LIGNE;NUM_SIM;DISTRIBUTEUR;OPERATEUR;#NewLine#</cfoutput>				
				<cfloop query="qDetail">
			    	<cfoutput>#COLLABORATEUR#;#IMEI#;#TRIM(MARQUE)#;#TRIM(MODELE)#;#TRIM(LIGNE)#;#TRIM(NUM_SIM)#;#TRIM(DISTRIBUTEUR)#;#TRIM(OPERATEUR)#;#NewLine#</cfoutput>
				</cfloop>
							    
			</cfsavecontent>		
			<!--- CrÃ©ation du fichier CSV --->			 
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/inventaire/equipement/flotte/csv/#fileName#" charset="utf-8"
					addnewline="true" fixnewline="true" output="#contentObj#">		
			<cfreturn "#fileName#">
			
	<!--- 		<cfcatch type="any">					
			<cfreturn "error">	
		</cfcatch> 				
		</cftry> --->
		
	</cffunction>
	

</cfcomponent>
