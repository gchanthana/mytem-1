<cfcomponent output="false">
	<!---
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : retourne un objet de type InfoSuppEquipement
		
		param in 
			- id l'identifiant de l'Equipement
			- codeIhm le code IHM de l'equipement
		param out 
			- un objet de type fr.consotel.consoview.inventaire.equipement.InfoSuppEquipement	
	--->
	<cffunction name="get" output="false" access="remote">
		<cfargument name="id" required="true"/>	
		<cfargument name="catalogue" required="true"/>	
		
		<cfswitch expression="#catalogue#">
			
			<cfcase value="client">
				<cfreturn createObject("component", "InfosSuppEquipementClient").init(arguments.id)>
			</cfcase>
			
			<cfcase value="Four">
				<cfreturn createObject("component", "InfosSuppEquipementFournis").init(arguments.id)>
			</cfcase>
			<cfcase value="Fab,Const" delimiters=",">
				<cfreturn createObject("component", "InfosSuppEquipementFournis").init(arguments.id)>
			</cfcase>
			<cfcase value="reel">
				<cfreturn createObject("component", "InfosSuppEquipement").init(arguments.id)>
			</cfcase>
			
			<cfDefaultcase>
				<cfthrow message="Catalogue inconnue">
			</cfdefaultcase>			
		</cfswitch>		
		
	</cffunction>

	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : enregistre les propriétés d'un objet infosSuppEquipement
		
		param in 
			- l'objet à enregistrer
		param out 
			- l'id de l'objet enregistré
	--->
	<cffunction name="update" output="false" access="remote" returntype="numeric">		
		<cfargument name="obj" type="any" required="true"/>
 		<cfreturn  obj.save()/>
	</cffunction>

</cfcomponent>