<cfcomponent output="false">
	<cfproperty name="idGroupe" type="numeric" default="0">
	<cfproperty name="idGroupeLigne" type="numeric" default="0">
	<cfproperty name="typePerimetre" type="string" default="Groupe">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupe = session.perimetre.ID_GROUPE;
		variables.idGroupeLigne = session.perimetre.ID_PERIMETRE;
		variables.typePerimetre = session.perimetre.TYPE_PERIMETRE;
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getidGroupe" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupe>
	</cffunction>
	
	<cffunction name="getidGroupeLigne" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeLigne>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.typePerimetre>
	</cffunction>
	
	<cffunction name="getAcueilResume" access="remote" output="false" returntype="array">		
		<!--- 
			--------------------------------------------------------------------------
       		-- renvoi les infos de la page d'accueil de la gestion des equipements                                                   --
			--------------------------------------------------------------------------
		       PROCEDURE Get_Accueil_Equipement_cat (  	p_idracine   IN INTEGER,
		                                             	p_idgroupe IN INTEGER,
		                                               	p_retour OUT SYS_REFCURSOR);
		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_EQUIPEMENT.Get_Accueil_Equipement_cat_v3">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#getidGroupeLigne()#"/>
			<cfprocresult name="qData"/>        
		</cfstoredproc>		
		
		<cfquery name="qTotal" dbtype="query">
			select 
					sum(NOMBRE_CATALOGUE) as TOTAL_CATALOGUE,
					sum(NOMBRE_CATALOGUE_CLIENT) as TOTAL_CATALOGUE_CLIENT,
					sum(NOMBRE_PARC) as TOTAL_PARC,
					sum(NOMBRE_CONTRAT) as TOTAL_CONTRAT,
					sum(NOMBRE_INTERVENTION) as TOTAL_INTERVENTION,
					sum(NOMBRE_COMMANDE) as TOTAL_COMMANDE, 
					sum(NOMBRE_ECHEANCE) as TOTAL_ECHEANCE
			from qData
		</cfquery>
		<cfset data = arrayNew(1)>
		<cfset arrayAppend(data,qData)>
		<cfset arrayAppend(data,qTotal)>
		<cfreturn data>
	</cffunction>
	
	
	<cffunction name="getAcueilResumeByType" access="remote" output="false" returntype="array">
		<cfargument name="categorieId" type="numeric" required="true">
		<!--- 
			--------------------------------------------------------------------------
       		-- renvoi les infos de la page d'accueil de la gestion des equipements par type                                                  --
			--------------------------------------------------------------------------
		              PROCEDURE Get_Accueil_Equipement_typ (  p_idracine   IN INTEGER,
				                                              p_idgroupe      IN INTEGER,
				                                              p_idcategorie_equipement   IN INTEGER,
				                                              p_retour     OUT SYS_REFCURSOR);

		 --->
		
		 <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_EQUIPEMENT.Get_Accueil_Equipement_typ_v3">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="" value="#getidGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="" value="#categorieId#"/>
			<cfprocresult name="qData"/>        
		</cfstoredproc>		
		<cfquery name="qTotal" dbtype="query">
			select 
					sum(NOMBRE_CATALOGUE) as TOTAL_CATALOGUE,
					sum(NOMBRE_CATALOGUE_CLIENT) as TOTAL_CATALOGUE_CLIENT,
					sum(NOMBRE_PARC) as TOTAL_PARC,
					sum(NOMBRE_CONTRAT) as TOTAL_CONTRAT,
					sum(NOMBRE_INTERVENTION) as TOTAL_INTERVENTION,
					sum(NOMBRE_COMMANDE) as TOTAL_COMMANDE, 
					sum(NOMBRE_ECHEANCE) as TOTAL_ECHEANCE
			from qData
		</cfquery>
		<cfset data = arrayNew(1)>
		<cfset arrayAppend(data,qData)>
		<cfset arrayAppend(data,qTotal)>
		<cfreturn data>
	</cffunction>
	
	
</cfcomponent>