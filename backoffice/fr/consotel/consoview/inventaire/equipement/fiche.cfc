<cfcomponent name="fiche">
	
	<cffunction name="getResumeFour" returntype="query" access="remote">
		<cfargument name="line_id" type="string">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.info_equipementsfournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#line_id#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getResumeFab" returntype="query" access="remote">
		<cfargument name="line_id" type="string">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.info_equipementsfournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#line_id#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getInfosFiche" returntype="array" access="remote" output="true">
		<cfargument name="idEquipement" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_Info_Constructeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#" null="false">
			<cfprocresult name="res1">
		</cfstoredproc>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_Info_eq_Fournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#" null="false">
			<cfprocresult name="res2">
		</cfstoredproc>
		
		<cfset dataArr = arrayNew(1)>
		<cfset dataArr[1] = res1>
		<cfset dataArr[2] = res2>
		
		<cfreturn dataArr>
		
	</cffunction>
	
	<!----- ####### FOURNISSEUR #######---->
	
	<cffunction name="getInfosFicheFournis" returntype="array" access="remote" output="true">
		<cfargument name="index" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_Info_Fournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#index#" null="false">
			<cfprocresult name="res1">
		</cfstoredproc>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_Info_eq_Fournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#index#" null="false">
			<cfprocresult name="res2">
		</cfstoredproc>
		<cfset dataArr = arrayNew(1)>
		<cfset dataArr[1] = res1>
		<cfset dataArr[2] = res2>
		<cfreturn dataArr>
	</cffunction>
	
	<cffunction name="updateInfoEquiFournis" returntype="numeric" access="remote" output="true">
		<cfargument name="p_id" type="numeric">
		<cfargument name="p_libelle" type="string">
		<cfargument name="p_commentaire" type="string">
		<cfargument name="p_reference" type="string">
		<cfargument name="p_code" type="string">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.update_info_eq_fournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_id#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_commentaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_reference#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_code#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getInfosSuppEquipementFournis" returntype="query" access="public">		
		<cfargument name="idEquipement" type="Numeric" required="true" hint="identifiant de l'equipement">		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Infosup_Equipementsfournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#" null="false">			
			<cfprocresult name="p_result">
		</cfstoredproc>			
		<cfreturn p_result>
	</cffunction>
		
	<cffunction name="updateInfoSuppEquipementFournis" returntype="numeric" access="remote" output="true">
		
		<cfargument name="idEquipement" type="Numeric" required="true" hint="identifiant de l'equipement">
		<cfargument name="typeEquipement" type="string" required="true" hint="le type d'equipement">	
		<cfargument name="parametres" type="struct" required="true" hint="la liste des params a updater">		
				
		<cfset equipement = parametres>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Infosup_Eqfournis_maj">

			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#equipement.HAUTEU_U#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#equipement.POIDS#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#equipement.PUISSANCE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#equipement.BRUIT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#equipement.MAX_HEAT_DISSIPATION#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#equipement.NBR_SLOTS_CMM_MAX#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#equipement.NBR_SLOTS_SFM_MAX#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#equipement.NBR_ALIM_MAX#">		
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#equipement.IMEI#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#equipement.DIMENSIONS#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#equipement.AFFICHAGE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#equipement.RESOLUTION#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#equipement.MEMOIRE_INTERNE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#equipement.RESEAU#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#equipement.TYPE_CARTE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#equipement.NB_ENTREE_SA#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#equipement.NB_ENTREE_S#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#equipement.NUM_SIM#">    

			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
	
		<cfreturn p_result>	
	</cffunction>		
	
	<!---		
		Auteur : samuel.divioka		
		Date : 11/14/2007		
		Description : Supprime un equipement enfant
		retour 1 si ok sinon -1	
	---> 
	<cffunction name="deleteEquipementFournisChild" returntype="query" access="remote">
		<cfargument name="eqId" type="numeric">
		<cfargument name="eqChildId" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.deleteEquipementFournisChild">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idEquipement" value="#eqId#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idEquipementFils" value="#eqChildId#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<!----- ####### EQUIPEMENT #######---->
	<cffunction name="info_equipement" returntype="query" access="remote">
		<cfargument name="p_idequipement" type="string">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.info_equipement_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idequipement#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getInfosEquipement" returntype="array" access="remote" output="true">
		<cfargument name="p_idequipement" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_Info_Equipement">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idequipement#" null="false">
			<cfprocresult name="res1">
		</cfstoredproc>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_Info_Fournis_eq_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idequipement#" null="false">
			<cfprocresult name="res2">
		</cfstoredproc>
		<cfset dataArr = arrayNew(1)>
		<cfset dataArr[1] = res1>
		<cfset dataArr[2] = res2>
		<cfreturn dataArr>
	</cffunction>
	
	<cffunction name="updateInfoEquipement" returntype="numeric" access="remote" output="true">
		<cfargument name="p_id" type="numeric">
		<cfargument name="p_libelle" type="string">
		<cfargument name="p_commentaire" type="string">
		<cfargument name="p_reference" type="string">
		<cfargument name="p_code" type="string">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.maj_Info_Equipement">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_id#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_commentaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_code#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_reference#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	
	<!---		
		Auteur : samuel.divioka		
		Date : 11/14/2007		
		Description : Supprime un equipement enfant
		retour 1 si ok sinon -1	
	---> 
	<cffunction name="deleteEquipementChild" returntype="query" access="remote">
		<cfargument name="eqId" type="numeric">
		<cfargument name="eqChildId" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.deleteEquipementChild">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idEquipement" value="#eqId#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idEquipementFils" value="#eqChildId#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	
	<!----- ####### CLIENT #######---->
	
	<cffunction name="getResumeClient" returntype="query" access="remote">
		<cfargument name="line_id" type="string">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.info_equipement_client">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#line_id#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getInfosFicheClient" returntype="array" access="remote" output="true">
		<cfargument name="index" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_Info_Equipement_client">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#index#" null="false">
			<cfprocresult name="res1">
		</cfstoredproc>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_Info_Fournis_client">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#index#" null="false">
			<cfprocresult name="res2">
		</cfstoredproc>
		<cfset dataArr = arrayNew(1)>
		<cfset dataArr[1] = res1>
		<cfset dataArr[2] = res2>
		<cfreturn dataArr>
	</cffunction>
	
	<cffunction name="updateInfoEquiClient" returntype="numeric" access="remote" output="true">
		<cfargument name="p_id" type="numeric">
		<cfargument name="p_libelle" type="string">
		<cfargument name="p_commentaire" type="string">
		<cfargument name="p_reference" type="string">
		<cfargument name="p_code" type="string">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.update_Info_Eq_CLient">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_id#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_commentaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_reference#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_code#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<!---		
		Auteur : samuel.divioka		
		Date : 11/14/2007		
		Description : Supprime un equipement enfant
		retour 1 si ok sinon -1	
	---> 
	<cffunction name="deleteEquipementClientChild" returntype="query" access="remote">
		<cfargument name="eqId" type="numeric">
		<cfargument name="eqChildId" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.deleteEquipementClientChild">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idEquipement" value="#eqId#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idEquipementFils" value="#eqChildId#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="getInfosEmplacementByEq" returntype="array" access="remote" output="true">
		<cfargument name="index" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_emplacement_Byeq">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#index#" null="false">
			<cfprocresult name="res1">
		</cfstoredproc>	
		<cfset dataArr = arrayNew(1)>
		<cfset dataArr[1] = res1>
		<cfreturn dataArr>
	</cffunction>
	
	<cffunction name="updateEmplacementEq" returntype="numeric" access="remote" output="true">
		<cfargument name="p_id" type="numeric">
		<cfargument name="p_idEmplacement" type="string">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.maj_emplacement">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_id#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idEmplacement#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="get_site_byEq" returntype="array" access="remote" output="true">
		<cfargument name="index" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_libellesite_byeq">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#index#" null="false">
			<cfprocresult name="res1">
		</cfstoredproc>	
		<cfset obj=createObject("component","fr.consotel.consoview.inventaire.session")>
		<cfset obj.setData(res1,"ficheSite")>
		<cfset res2=0>
		
		<!--- <cfloop index="i" from="1" to="#session.perimetre.LISTE_PERIMETRES_QUERY.recordcount#">
			<cfif session.perimetre.LISTE_PERIMETRES_QUERY['IDGROUPE_CLIENT'][i] EQ res1.IDGROUPE_CLIENT[1]>
				<cfset res2=i>
			</cfif>
		</cfloop> --->
		
		<cfset dataArr = arrayNew(1)>
		<cfset dataArr[1] = res1>  
		<cfset dataArr[2] = res1.IDGROUPE_CLIENT[1]>
		<cfreturn dataArr>
	</cffunction>
	
	<cffunction name="searchEquipementChild" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Search_Equipement_fils">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[2]#" >
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="searchCatalogueClientChild" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Search_EqClient_fils">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[2]#" >
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="searchCatalogueFournisChild" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Search_Eqrev_fils">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[2]#" >
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="searchCatalogueFabChild" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Search_EqFab_fils">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[2]#" >
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<!---- Search equipement No parent : without parent --->
	<cffunction name="searchNPEquipement" returntype="query" access="remote">
		<cfargument name="arr" type="array">
			
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Search_sous_Equipement">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#session.perimetre.ID_GROUPE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[2]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[3]#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="SearchNBCatalogueclient" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		
		<cfset idRacine = session.perimetre.ID_GROUPE>
		<cfset idgroupe_client = arr[6]>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Search_sous_Catalogueclient">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[3]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[4]#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[5]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[7]#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="SearchNBCatalogueFab" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		
		<!--- <cfset idgroupe_client = arr[6]> --->
		<cfset idgroupe_client = session.perimetre.ID_GROUPE>
		<cfset REVENDEUR = 0>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Search_sous_CatalogueRevendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[3]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[4]#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[5]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idgroupe_client#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#REVENDEUR#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[7]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#REVENDEUR#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="SearchNBCatalogueFour" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfset idgroupe_client = session.perimetre.ID_GROUPE>
		<!--- <cfset idgroupe_client = arr[6]> --->
		<cfset FOURNISSEUR = 1>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Search_sous_CatalogueRevendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[3]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[4]#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[5]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idgroupe_client#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#FOURNISSEUR#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[7]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#FOURNISSEUR#">
			<cfprocresult name="p_result">
		</cfstoredproc>
						
		<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="updateEqParent" returntype="numeric" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.maj_idEqParent">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[3]#" >
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getPhotoEq" returntype="Any" access="remote" output="false">
		<cfargument name="typeEquipement" required="true" type="string" />
		<cfargument name="idEquipement" required="true" type="numeric" />
		
		<cfset StoreProc = "">
				
		<cfswitch expression="#UCase(typeEquipement)#">
			
			<cfcase value="REEL">
				<cfset StoreProc = "pkg_cv_equipement.get_photo_eq">
			</cfcase>
			
			<cfcase value="CLIENT">
				<cfset StoreProc = "pkg_cv_equipement.get_photo_eq_client">
			</cfcase>
			
			<cfcase value="FAB">
				<cfset StoreProc = "pkg_cv_equipement.get_photo_eq_fournis">
			</cfcase>
			
			<cfcase value="FOUR">
				<cfset StoreProc = "pkg_cv_equipement.get_photo_eq_fournis">
			</cfcase>
		</cfswitch>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="#StoreProc#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="retour">
			<cfprocparam type="Out" cfsqltype="CF_SQL_BLOB" variable="image">
		</cfstoredproc>
		
		<cfset rootPath=expandPath("/")>		
		<cfset uid = createuuid()>
		<cfset imageName =  uid & "_image.png">
		<cfset path = "/fr/consotel/consoview/tmp/#imageName#">
		
		<cfset uri = rootPath & path >
		<cffile action = "write"
			    file = "#uri#"
			    output="#image#"
			    addnewline="no"> 
			    		 
		<cfset a = arrayNew(1)>
		<cfset arrayAppend(a,retour)>
		<cfset arrayAppend(a,path)>
		<cfset arrayAppend(a,image)>
		<cfreturn a>
	</cffunction>
	
	<cffunction name="removeTmpImage" returntype="Any" access="remote" output="false">
		<cfargument name="uri" required="true" type="string" />
		 
		<cftry>
			<cfset rootPath=expandPath("/")>	
			<cfset path = "#rootPath##uri#">
			<cffile action = "delete"
			    file = "#path#">
			   
			    <cfreturn 1>
		   <cfcatch type="any">
		   		<cfreturn -1>
		   </cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>