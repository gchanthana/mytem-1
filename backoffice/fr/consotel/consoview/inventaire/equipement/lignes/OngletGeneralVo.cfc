<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.equipement.lignes.OngletGeneralVo">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="IDTYPE_LIGNE" type="numeric" default="0">
	<cfproperty name="LIBELLE_TYPE_LIGNE" type="string" default="">
	<cfproperty name="IDSOUS_TETE" type="numeric" default="0">
	<cfproperty name="SOUS_TETE" type="string" default="">
	<cfproperty name="IDTETE_LIGNE" type="numeric" default="0">
	<cfproperty name="TETE_LIGNE" type="string" default="">
	<cfproperty name="IDTYPE_RACCORDEMENT" type="numeric" default="0">
	<cfproperty name="LIBELLE_RACCORDEMENT" type="string" default="DIRECT">
	<cfproperty name="NBRE_LIGNE" type="numeric" default="0">
	<cfproperty name="IDENTIFIANT_COMPLEMENTAIRE" type="string" default="">
	<cfproperty name="CODE_INTERNE" type="string" default="">
	<cfproperty name="BOOL_PRINCIPAL" type="numeric" default="1">
	<cfproperty name="BOOL_BACKUP" type="numeric" default="0">
	<cfproperty name="COMMENTAIRES_GENERAL" type="string" default="">
	<cfproperty name="USAGE" type="string" default="">
	<cfproperty name="POPULATION" type="string" default="">
	<cfproperty name="V1" type="string" default="Champ PersonnalisÃ© C1">
	<cfproperty name="V2" type="string" default="Champ PersonnalisÃ© C2">

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.IDTYPE_LIGNE = 0;
		variables.LIBELLE_TYPE_LIGNE = "";
		variables.IDSOUS_TETE = 0;
		variables.SOUS_TETE = "";
		variables.IDTETE_LIGNE = 0;
		variables.TETE_LIGNE = "";
		variables.IDTYPE_RACCORDEMENT = 0;
		variables.LIBELLE_RACCORDEMENT = "DIRECT";
		variables.NBRE_LIGNE = 0;
		variables.IDENTIFIANT_COMPLEMENTAIRE = "";
		variables.CODE_INTERNE = "";
		variables.BOOL_PRINCIPAL = 1;
		variables.BOOL_BACKUP = 0;
		variables.COMMENTAIRES_GENERAL = "";
		variables.USAGE = "";
		variables.POPULATION = "";
		variables.V1 = "";
		variables.V2 = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="OngletGeneralVo">
		<cfreturn this>
	</cffunction>
	<cffunction name="getIDTYPE_LIGNE" output="false" access="public" returntype="any">
		<cfreturn variables.IDTYPE_LIGNE>
	</cffunction>

	<cffunction name="setIDTYPE_LIGNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDTYPE_LIGNE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLIBELLE_TYPE_LIGNE" output="false" access="public" returntype="any">
		<cfreturn variables.LIBELLE_TYPE_LIGNE>
	</cffunction>

	<cffunction name="setLIBELLE_TYPE_LIGNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.LIBELLE_TYPE_LIGNE = arguments.val>
	</cffunction>

	<cffunction name="getIDSOUS_TETE" output="false" access="public" returntype="any">
		<cfreturn variables.IDSOUS_TETE>
	</cffunction>

	<cffunction name="setIDSOUS_TETE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDSOUS_TETE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getSOUS_TETE" output="false" access="public" returntype="any">
		<cfreturn variables.SOUS_TETE>
	</cffunction>

	<cffunction name="setSOUS_TETE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.SOUS_TETE = arguments.val>
	</cffunction>

	<cffunction name="getIDTETE_LIGNE" output="false" access="public" returntype="any">
		<cfreturn variables.IDTETE_LIGNE>
	</cffunction>

	<cffunction name="setIDTETE_LIGNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDTETE_LIGNE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTETE_LIGNE" output="false" access="public" returntype="any">
		<cfreturn variables.TETE_LIGNE>
	</cffunction>

	<cffunction name="setTETE_LIGNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.TETE_LIGNE = arguments.val>
	</cffunction>

	<cffunction name="getIDTYPE_RACCORDEMENT" output="false" access="public" returntype="any">
		<cfreturn variables.IDTYPE_RACCORDEMENT>
	</cffunction>

	<cffunction name="setIDTYPE_RACCORDEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDTYPE_RACCORDEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLIBELLE_RACCORDEMENT" output="false" access="public" returntype="any">
		<cfreturn variables.LIBELLE_RACCORDEMENT>
	</cffunction>

	<cffunction name="setLIBELLE_RACCORDEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.LIBELLE_RACCORDEMENT = arguments.val>
	</cffunction>

	<cffunction name="getNBRE_LIGNE" output="false" access="public" returntype="any">
		<cfreturn variables.NBRE_LIGNE>
	</cffunction>

	<cffunction name="setNBRE_LIGNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.NBRE_LIGNE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDENTIFIANT_COMPLEMENTAIRE" output="false" access="public" returntype="any">
		<cfreturn variables.IDENTIFIANT_COMPLEMENTAIRE>
	</cffunction>

	<cffunction name="setIDENTIFIANT_COMPLEMENTAIRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.IDENTIFIANT_COMPLEMENTAIRE = arguments.val>
	</cffunction>

	<cffunction name="getCODE_INTERNE" output="false" access="public" returntype="any">
		<cfreturn variables.CODE_INTERNE>
	</cffunction>

	<cffunction name="setCODE_INTERNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.CODE_INTERNE = arguments.val>
	</cffunction>

	<cffunction name="getBOOL_PRINCIPAL" output="false" access="public" returntype="any">
		<cfreturn variables.BOOL_PRINCIPAL>
	</cffunction>

	<cffunction name="setBOOL_PRINCIPAL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.BOOL_PRINCIPAL = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getBOOL_BACKUP" output="false" access="public" returntype="any">
		<cfreturn variables.BOOL_BACKUP>
	</cffunction>

	<cffunction name="setBOOL_BACKUP" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.BOOL_BACKUP = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getCOMMENTAIRES_GENERAL" output="false" access="public" returntype="any">
		<cfreturn variables.COMMENTAIRES_GENERAL>
	</cffunction>

	<cffunction name="setCOMMENTAIRES_GENERAL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.COMMENTAIRES_GENERAL = arguments.val>
	</cffunction>

	<cffunction name="getUSAGE" output="false" access="public" returntype="any">
		<cfreturn variables.USAGE>
	</cffunction>

	<cffunction name="setUSAGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.USAGE = arguments.val>
	</cffunction>

	<cffunction name="getPOPULATION" output="false" access="public" returntype="any">
		<cfreturn variables.POPULATION>
	</cffunction>

	<cffunction name="setPOPULATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.POPULATION = arguments.val>
	</cffunction>

	<cffunction name="getV1" output="false" access="public" returntype="any">
		<cfreturn variables.V1>
	</cffunction>

	<cffunction name="setV1" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.V1 = arguments.val>
	</cffunction>

	<cffunction name="getV2" output="false" access="public" returntype="any">
		<cfreturn variables.V2>
	</cffunction>

	<cffunction name="setV2" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.V2 = arguments.val>
	</cffunction>



</cfcomponent>