<cfcomponent name="GestionLignes" alias="fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes" extends="fr.consotel.consoview.access.AccessObject">
	
	<cffunction name="getLignes" access="remote" returntype="query">
		<cfargument name="idgroupe_client" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.inventaire_lignes">
			<cfprocparam  value="#idgroupe_client#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetLignes">
		</cfstoredproc>
		<cfreturn qGetLignes>
	</cffunction>
	
	<cffunction name="searchLignes" access="remote" returntype="query">
		<cfargument name="idgroupe_client" type="numeric" required="true">
		<cfargument name="criteria" type="string" required="true">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.search_inventaire_lignes_v3">
			<cfprocparam  value="#idgroupe_client#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#criteria#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="qSearchLignes">
		</cfstoredproc>
		<cfreturn qSearchLignes>
	</cffunction>
	
	<cffunction  name="exporterListeLignes" access="remote" returntype="string">
		<cfargument name="idgroupe_client" type="numeric" required="true">
		<cfargument name="criteria" type="string" required="true">
		<cfsetting enablecfoutputonly="true">
		<cfset charset="ISO-8859-1">		
		<cftry> 
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"ListeLignes.csv">				
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset qDetail = this.searchLignes(idgroupe_client,criteria)>				
				<cfset NewLine = Chr(13) & Chr(10)>
				<cfset space = Chr(13) & Chr(10)>
				<cfcontent type="text/csv">
				<cfoutput>Ligne;Usage;Tete;Type de ligne;Type de raccordement;Collaborateur SI operateur;Opérateur d'accès;Opérateur d'accès cible;#NewLine#</cfoutput>
				<cfloop query="qDetail">
					<cfset _ope_acces_cible=TRIM(qDetail.OPERATEUR_ACCES)>
					<cfif qDetail.OPERATEURID_ACCES_CIBLE GT 0>
						<cfset _ope_acces_cible=TRIM(qDetail.OPERATEUR_ACCES_CIBLE)>
					</cfif>
				    <cfoutput>#TRIM(qDetail.SOUS_TETE)#;#TRIM(qDetail.USAGE)#;#TRIM(qDetail.TETE_LIGNE)#;#TRIM(qDetail.LIBELLE_TYPE_LIGNE)#;#TRIM(qDetail.TYPE_RACCORDEMENT)#;#TRIM(qDetail.NOM) & ' ' &TRIM(qDetail.PRENOM)#;#TRIM(qDetail.OPERATEUR_ACCES)#;#_ope_acces_cible#;#NewLine#</cfoutput>
				</cfloop>
			</cfsavecontent>
			<!--- Création du fichier CSV --->
			<cffile action="write" charset="#charset#" file="#rootPath#/fr/consotel/consoview/inventaire/equipement/lignes/csv/#fileName#" addnewline="true" fixnewline="true" output="#contentObj#">
			<cfreturn "#fileName#">
			<cfcatch type="any">
			<cfreturn "error">	
		</cfcatch> 				
		</cftry>
	</cffunction>
	
	
	
	<cffunction name="getTypeLigne" access="remote" returntype="query">
		<cfset codeLangue=session.user.globalization>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.Liste_Type_ligne_v2">
			<cfprocparam  value="#getidGroupe()#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR" >			
			<cfprocresult name="qGetTypeLigne">
		</cfstoredproc>
		<cfreturn qGetTypeLigne>
	</cffunction>
	
	<cffunction name="getTeteLigne" access="remote" returntype="query">
		<cfargument name="idsous_tete" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.Liste_Tetes_ligne_v2">
			<cfprocparam  value="#getIdGroupe()#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idsous_tete#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetTeteLigne">
		</cfstoredproc>
		<cfreturn qGetTeteLigne>
	</cffunction>
	
	<cffunction name="getTypeRacco" access="remote" returntype="query">
		<cfset codeLangue=session.user.globalization>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.Liste_Type_Raccordement_v2">
			<cfprocparam  value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR" >	
			<cfprocresult name="qGetTypeRacco">
		</cfstoredproc>
		<cfreturn qGetTypeRacco>
	</cffunction>
	
	<cffunction name="getUsages" access="remote" returntype="query">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.Liste_Usage">
			<cfprocparam  value="#getidGroupeLigne()#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetUsages">
		</cfstoredproc>
		<cfreturn qGetUsages>
	</cffunction>
	
	<cffunction name="getInfosLigne" access="remote" returntype="query">
		<cfargument name="idsous_tete" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.Infos_Ligne">
			<cfprocparam  value="#idsous_tete#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetInfosLigne">
		</cfstoredproc>
		<cfreturn qGetInfosLigne>
	</cffunction>
	
	<cffunction name="updateInfosLigne" access="remote" returntype="numeric">
		<cfargument name="xmlParamObject" required="true" type="XML">
		
		<cfset vlIDSOUS_TETE =getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDSOUS_TETE")>
		<cfset vlIDTYPE_LIGNE =getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDTYPE_LIGNE")>
		<cfset vlIDTETE_LIGNE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDTETE_LIGNE")>
		<cfset vlUSAGE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"USAGE")>
		
		<cfset vlIDENTIFIANT_COMPLEMENTAIRE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDENTIFIANT_COMPLEMENTAIRE")>
		<cfset vlCODE_INTERNE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"CODE_INTERNE")>
		<cfset vlIDTYPE_RACCORDEMENT = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDTYPE_RACCORDEMENT")>
		<cfset vlBOOL_PRINCIPAL = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"BOOL_PRINCIPAL")>
		<cfset vlBOOL_BACKUP = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"BOOL_BACKUP")>		
		<cfset vlCOMMENTAIRES_GENERAL = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"COMMENTAIRES_GENERAL")>
		
		<cfset vlPOPULATION = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"POPULATION")>
		<cfset vlV1 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"V1")>
		<cfset vlV2 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"V2")> 
		
		<cfset IDSOUS_TETE = VAL(vlIDSOUS_TETE[1].xmlText)>
		<cfset IDTYPE_LIGNE = VAL(vlIDTYPE_LIGNE[1].xmlText)>
		<cfset IDTETE_LIGNE = VAL(vlIDTETE_LIGNE[1].xmlText)>
		
		<cfset IDENTIFIANT_COMPLEMENTAIRE = (vlIDENTIFIANT_COMPLEMENTAIRE[1].xmlText)>
		<cfset CODE_INTERNE = (vlCODE_INTERNE[1].xmlText)>
		<cfset IDTYPE_RACCORDEMENT = VAL(vlIDTYPE_RACCORDEMENT[1].xmlText)>
		<cfset BOOL_PRINCIPAL = VAL(vlBOOL_PRINCIPAL[1].xmlText)>
		<cfset BOOL_BACKUP = VAL(vlBOOL_BACKUP[1].xmlText)>		
		<cfset COMMENTAIRES_GENERAL = (vlCOMMENTAIRES_GENERAL[1].xmlText)>
		<cfset USAGE = (vlUSAGE[1].xmlText)>
		<cfset POPULATION = (vlPOPULATION[1].xmlText)>
		<cfset V1 = (vlV1[1].xmlText)>
		<cfset V2 = (vlV2[1].xmlText)>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.upd_Infos_Ligne">
			<cfprocparam  value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER"  type="in">
			<cfprocparam  value="#IDTYPE_LIGNE#" cfsqltype="CF_SQL_INTEGER"  type="in">
			<cfprocparam  value="#IDTETE_LIGNE#" cfsqltype="CF_SQL_INTEGER"  type="in" null="#iif((IDTETE_LIGNE eq 0), de("yes"), de("no"))#"/>
			<cfprocparam  value="#IDENTIFIANT_COMPLEMENTAIRE#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#CODE_INTERNE#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#IDTYPE_RACCORDEMENT#" cfsqltype="CF_SQL_INTEGER"  type="in">
			<cfprocparam  value="#BOOL_PRINCIPAL#" cfsqltype="CF_SQL_INTEGER"  type="in">
			<cfprocparam  value="#BOOL_BACKUP#" cfsqltype="CF_SQL_INTEGER"  type="in">
			<cfprocparam  value="#COMMENTAIRES_GENERAL#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#USAGE#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#POPULATION#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#V1#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#V2#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam variable="updateInfosStatus"   cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		
 		<cfreturn updateInfosStatus>
	</cffunction>
	
	<cffunction name="getEtatLigne" access="remote" returntype="query">
		<cfargument name="idsous_tete" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.Infos_Etat_Ligne">
			<cfprocparam  value="#idsous_tete#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetInfosEtatLigne">
		</cfstoredproc>
		<cfreturn qGetInfosEtatLigne>
	</cffunction>
	
	<cffunction name="updateEtatLigne" access="remote" returntype="numeric">
		<cfargument name="valueObject" type="OngletEtatVo" required="true">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.upd_Infos_Etat_Ligne">
			<cfprocparam  value="#valueObject.getIDSOUS_TETE()#" cfsqltype="CF_SQL_INTEGER"  type="in">
			<cfprocparam  value="#valueObject.getETAT()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#toInverseDateString(valueObject.getDATE_COMMANDE())#" cfsqltype="CF_SQL_VARCHAR"  type="in" null="#iif((valueObject.getDATE_COMMANDE() eq ""), de("yes"), de("no"))#"/>
			<cfprocparam  value="#valueObject.getREF_COMMANDE()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#valueObject.getBOOL_TITULAIRE()#" cfsqltype="CF_SQL_INTEGER"  type="in">
			<cfprocparam  value="#valueObject.getBOOL_PAYEUR()#" cfsqltype="CF_SQL_INTEGER"  type="in">
			<cfprocparam  value="#valueObject.getCOMMENTAIRE_ETAT()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#toInverseDateString(valueObject.getDATE_MISE_EN_SERV())#" cfsqltype="CF_SQL_VARCHAR"  type="in" null="#iif((valueObject.getDATE_MISE_EN_SERV() eq ""), de("yes"), de("no"))#"/>
			<cfprocparam  value="#valueObject.getREF_MISE_EN_SERV()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#valueObject.getGEST_MISE_EN_SERV()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#toInverseDateString(valueObject.getDATE_PRESELECTION())#" cfsqltype="CF_SQL_VARCHAR"  type="in" null="#iif((valueObject.getDATE_PRESELECTION() eq ""), de("yes"), de("no"))#"/>
			<cfprocparam  value="#valueObject.getREF_PRESELECTION()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#valueObject.getGEST_PRESELECTION()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#toInverseDateString(valueObject.getDATE_SUSPENSION())#" cfsqltype="CF_SQL_VARCHAR"  type="in" null="#iif((valueObject.getDATE_SUSPENSION() eq ""), de("yes"), de("no"))#"/>
			<cfprocparam  value="#valueObject.getREF_SUSPENSION()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#valueObject.getGEST_SUSPENSION()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#toInverseDateString(valueObject.getDATE_RESILIATION())#" cfsqltype="CF_SQL_VARCHAR"  type="in" null="#iif((valueObject.getDATE_RESILIATION() eq ""), de("yes"), de("no"))#"/>
			<cfprocparam  value="#valueObject.getREF_RESILIATION()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#valueObject.getGEST_RESILIATION()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#valueObject.getV3()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  value="#valueObject.getV4()#" cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam  variable="updateEtatStatus"   cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn updateEtatStatus>
	</cffunction>
	
	<cffunction name="getCustomFieldsLabel" access="remote" returntype="query">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.Liste_Champs_Perso">
			<cfprocparam  value="#getIdGroupe()#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetCustomFieldsLabel">
		</cfstoredproc>
		<cfreturn qGetCustomFieldsLabel>
	</cffunction>
	
	<cffunction name="updateCustomFieldsLabel" access="remote" returntype="numeric">
		<cfargument name="C1" type="string" required="true">
		<cfargument name="C2" type="string" required="true">
		<cfargument name="C3" type="string" required="true">
		<cfargument name="C4" type="string" required="true">
		<cfargument name="C5" type="string" required="true">
		<cfargument name="C6" type="string" required="true">
		<cfargument name="C7" type="string" required="true">
		<cfargument name="C8" type="string" required="true">
		<cfargument name="C9" type="string" required="true">
		<cfargument name="C10" type="string" required="true">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.Upd_Champs_Perso">
			<cfprocparam  value="#getIdGroupe()#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#C1#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#C2#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#C3#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#C4#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#C5#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#C6#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#C7#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#C8#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#C9#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#C10#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  variable="updateCustomFieldsLabelStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn updateCustomFieldsLabelStatus>
	</cffunction>
	
	<cffunction name="updateCustomFieldLabel" access="remote" returntype="numeric">
		<cfargument name="index_champ" type="numeric" required="true">
		<cfargument name="CX" type="string" required="true">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.Upd_1_Champs_Perso">
			<cfprocparam  value="#getIdGroupe()#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#index_champ#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#CX#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  variable="updateCustomFieldLabelStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn updateCustomFieldLabelStatus>
	</cffunction>
	
	<cffunction name="getInfosPanelLigne" access="remote" returntype="query">
		<cfargument name="idsous_tete" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.get_info_sous_tete">
			<cfprocparam  value="#idsous_tete#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetInfosPanelLigne">
		</cfstoredproc>
		<cfreturn qGetInfosPanelLigne>
	</cffunction>
	
	
	<cffunction name="setTeteLigne" access="remote" returntype="numeric">
		<cfargument name="idsous_tete" type="numeric" required="true">
		<cfargument name="idteteLigne" type="numeric" required="true" default="0">

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.maj_teteLigne">
			<cfprocparam  value="#idsous_tete#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idteteLigne#" cfsqltype="CF_SQL_INTEGER" type="In" null="#iif((arguments.idteteLigne eq 0), de("yes"), de("no"))#">
			<cfprocparam  variable="qResult" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		
		<cfreturn qResult>
	</cffunction>
	
	
	
	<cffunction name="setTeteLignePourXLignes" access="remote" returntype="numeric">
		<cfargument name="tab_idsous_tetes" type="array" required="true">
		<cfargument name="idteteLigne" type="numeric" required="true" default="0">
		
		<cfset liste_idsous_tetes = arrayToList(tab_idsous_tetes,",")>
		<cfset len = arrayLen(tab_idsous_tetes)>
		
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = setTeteLigne(tab_idsous_tetes[i],idteteLigne)>
			
		</cfloop>
				
		<cfreturn r>
	</cffunction>
	
	
	
	
	
	<!--- Liste des lignes du mÃªme groupement --->
	<cffunction name="getLignesGroupement" access="remote" returntype="query">
		<cfargument name="idTete_ligne" type="numeric" required="true">		
		 
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.Liste_lignes_groupement">
			<cfprocparam  value="#getIdGroupe()#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#idTete_ligne#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qGetLignesGroupement">
		</cfstoredproc>
		
		<cfreturn qGetLignesGroupement>
	</cffunction>
		
	
	
	
	
	
	<!--- ===== Onglet affectation mobile ======================================================== --->
	<cffunction name="getInfosAffectationMobile" access="remote" returntype="query">
		<cfargument name="idsous_tete" type="numeric" required="true">
		
		<!--- <cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.">
			<cfprocparam  value="#getidGroupe()#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qListeCollaborateurSIOP">
		</cfstoredproc> --->
		<cfset qInfosAffectationMobile = queryNew("PATRONYME","VarChar")>
		<cfreturn qInfosAffectationMobile>
	</cffunction>
	
	<cffunction name="getListeCollaborateurSIOP" access="remote" returntype="query">

		<!--- <cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.LISTE_COLLABORATEUR_SI_OP">
			<cfprocparam  value="#getidGroupe()#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qListeCollaborateurSIOP">
		</cfstoredproc> --->
		<cfset qListeCollaborateurSIOP = queryNew("PATRONYME","VarChar")>
		<cfreturn qListeCollaborateurSIOP>
	</cffunction>
	
	<cffunction name="updateCollaborateurSIOP" access="remote" returntype="numeric">
		<cfargument name="collaborateurid" type="numeric" required="true">
		<cfargument name="nom" type="string" required="true">
		<cfargument name="prenom" type="string" required="true">
		<cfargument name="idsousTete" type="string" required="false">
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.maj_collaborateur">			
			<cfprocparam  value="#collaborateurid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#nom#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#prenom#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#idsousTete#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="updateResult" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		
		<cfreturn updateResult>
	</cffunction>
	
	<cffunction name="getInfosCollaborateurSIOP" access="remote" returntype="query">
		<cfargument name="sousTeteId" type="numeric" required="true">		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.GET_INFO_COLLABORATEUR">
			<cfprocparam  value="#sousTeteId#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#getidGroupe()#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qCollaborateur">
		</cfstoredproc>
		<cfreturn qCollaborateur>
	</cffunction>
	
	
	<cffunction name="getListeSousComptes" access="remote" returntype="query">
		<cfargument name="sousTeteId" type="numeric" required="true">
		<cfquery name="qListeComptes" datasource="#SESSION.OFFREDSN#">
			SELECT DISTINCT sc.sous_compte
			FROM sous_compte sc,st_op,sous_tete st,groupe_client gc, groupe_client_ref_client gcrc
			WHERE st.idsous_tete = #sousTeteId#
			AND st_op.idsous_tete = st.idsous_tete
			AND sc.idsous_compte = st_op.idsous_compte
			AND st_op.idref_client = gcrc.idref_client
			AND gc.idgroupe_client = gcrc.idgroupe_client
			AND gc.idgroupe_client =  #getidGroupe()# 		 
		</cfquery>
		
		<cfreturn qListeComptes>
	</cffunction>
	<!--- ===== Fin Onglet affectation mobile ======================================================== --->	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<!--- ======================== Onglet Eat Raccordement =========================================== --->
	<cffunction name="getHistoriqueActionsRaccordement" access="remote" returntype="query">
		<!---    PROCEDURE Racco_Histo_Action (p_idsous_tete              IN INTEGER,
    			                           p_retour                   OUT SYS_REFCURSOR);
	 	--->
		<cfargument name="sousTeteId" type="numeric" required="true">		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.Racco_Histo_Action">
			<cfprocparam  value="#sousTeteId#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qResult">
		</cfstoredproc>
			
		<cfreturn qResult>
	</cffunction>
	
	<cffunction name="getListeModeRaccordement" access="remote" returntype="query">
		<!--- PROCEDURE Liste_Mode_racco( p_retour                   OUT SYS_REFCURSOR);--->	
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.Liste_Mode_racco_v2">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_langid"		value="#SESSION.USER.IDGLOBALIZATION#" >
			<cfprocresult name="qResult">
		</cfstoredproc>

		<cfreturn qResult>
	</cffunction>
	
	<cffunction name="getlisteOperateurIndirect" access="remote" returntype="query">
		<cfargument name="sousTeteId" type="numeric" required="true">		
		<!---    PROCEDURE Liste_Ope_indirect ( p_idsous_tete              IN INTEGER,
        				                        p_retour                   OUT SYS_REFCURSOR);
		--->
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.Liste_Ope_indirect">
			<cfprocparam  variable="p_idsous_tete" value="#sousTeteId#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction>
	
	<cffunction name="majOperateursIndirects" access="remote" returntype="numeric">
		<cfargument name="sousTeteId" type="numeric" required="true">
		<cfargument name="idOpe1" type="numeric" required="true">
		<cfargument name="idOpe2" type="numeric" required="true">
		<cfargument name="idOpe3" type="numeric" required="true">
		<cfargument name="idOpe4" type="numeric" required="true">
		<cfargument name="idOpe5" type="numeric" required="true">
		<!---    PROCEDURE maj_Ope_indirect (  p_idsous_tete                     IN INTEGER,
                                               p_ope_indirect_cible1             IN INTEGER,
                                               p_ope_indirect_cible2             IN INTEGER,
                                               p_ope_indirect_cible3             IN INTEGER,
                                               p_ope_indirect_cible4             IN INTEGER,
                                               p_ope_indirect_cible5             IN INTEGER,
                                               p_retour                          OUT INTEGER)
		--->
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.maj_Ope_indirect">
			<cfprocparam  variable="p_idsous_tete" value="#sousTeteId#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="p_ope_indirect_cible1" value="#idOpe1#" cfsqltype="CF_SQL_INTEGER" type="in" null="#iif(idOpe1 eq -1, de("yes"), de("no"))#">
			<cfprocparam  variable="p_ope_indirect_cible2" value="#idOpe2#" cfsqltype="CF_SQL_INTEGER" type="in" null="#iif(idOpe1 eq -1, de("yes"), de("no"))#">
			<cfprocparam  variable="p_ope_indirect_cible3" value="#idOpe3#" cfsqltype="CF_SQL_INTEGER" type="in" null="#iif(idOpe1 eq -1, de("yes"), de("no"))#">
			<cfprocparam  variable="p_ope_indirect_cible4" value="#idOpe4#" cfsqltype="CF_SQL_INTEGER" type="in" null="#iif(idOpe1 eq -1, de("yes"), de("no"))#">
			<cfprocparam  variable="p_ope_indirect_cible5" value="#idOpe5#" cfsqltype="CF_SQL_INTEGER" type="in" null="#iif(idOpe1 eq -1, de("yes"), de("no"))#">
			<cfprocparam  variable="qResult" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction>
	
	<cffunction name="getListeInventaireSousTete" access="remote" returntype="query">
		<cfargument name="sousTeteId" type="numeric" required="true">		
		<!---        PROCEDURE Inventaire_sous_tete ( p_idsous_tete              IN INTEGER,  
        				                              p_retour                  OUT SYS_REFCURSOR);
		--->
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.Inventaire_sous_tete">
			<cfprocparam  variable="p_idsous_tete" value="#sousTeteId#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction> 
	
	<cffunction name="getLigneProduitAbo" access="remote" returntype="query">
		<cfargument name="sousTeteId" type="numeric" required="true">		
		
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.get_ligne_produit_abo">
			<cfprocparam  variable="p_idracine" value="#idRacine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="p_idsous_tete" value="#sousTeteId#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction> 
	
	
		
	<cffunction name="getInfosEtat" access="remote" returntype="query">
		<cfargument name="sousTeteId" type="numeric" required="true">		
		<!---    PROCEDURE Infos_etat (p_idsous_tete              IN INTEGER,
                                       p_retour                   OUT SYS_REFCURSOR) IS
 		--->
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.Infos_etat">
			<cfprocparam  variable="p_idsous_tete" value="#sousTeteId#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction>
	
	<cffunction name="udpateInfosEtat" access="remote" returntype="numeric">
		<cfargument name="sousTeteId" type="numeric" required="true">
		<cfargument name="COMMENTAIRE_ETAT" type="string" required="true">
		<cfargument name="V3" type="string" required="true">
		<cfargument name="V4" type="string" required="true">
		<cfargument name="BOOL_PAYEUR" type="numeric" required="true">
		<cfargument name="BOOL_TITULAIRE" type="numeric" required="true">
		<cfargument name="IDOPERATEUR_CREATEURNUMERO" type="numeric" required="true">
		<cfargument name="IDAGENCE_OPERATEUR" type="numeric" required="true">		
		
		<!--- PROCEDURE maj_Infos_etat (p_idsous_tete                     IN INTEGER,
                                        p_COMMENT_OPE_ACCES               IN VARCHAR2,
                                        p_v3                              IN VARCHAR2,
                                        p_v4                              IN VARCHAR2,
                                        p_bool_payeur                     IN INTEGER,
                                        p_bool_TITULAIRE                  IN INTEGER,
                                        p_idagence_operateur              IN INTEGER,
                                        p_operateurid_createur            IN INTEGER,
                                        p_retour                           OUT INTEGER) --->

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.maj_Infos_etat">
			<cfprocparam  variable="p_idsous_tete" value="#sousTeteId#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="p_COMMENT_OPE_ACCES" value="#COMMENTAIRE_ETAT#" cfsqltype="CF_SQL_vARCHAR" type="in">
			<cfprocparam  variable="p_v3" value="#V3#" cfsqltype="CF_SQL_vARCHAR" type="in">
			<cfprocparam  variable="p_v4" value="#V4#" cfsqltype="CF_SQL_vARCHAR" type="in">
			<cfprocparam  variable="p_bool_payeur" value="#BOOL_PAYEUR#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="p_bool_TITULAIRE" value="#BOOL_TITULAIRE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="p_idagence_operateur" value="#IDOPERATEUR_CREATEURNUMERO#" cfsqltype="CF_SQL_INTEGER" type="in" null="#iif(IDOPERATEUR_CREATEURNUMERO eq -1, de("yes"), de("no"))#">>
			<cfprocparam  variable="p_operateurid_createur" value="#IDAGENCE_OPERATEUR#" cfsqltype="CF_SQL_INTEGER" type="in" null="#iif(IDAGENCE_OPERATEUR eq -1, de("yes"), de("no"))#">
			<cfprocparam  variable="qResult"  cfsqltype="CF_SQL_INTEGER" type="out">			
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction>
	
	<cffunction name="doActionRaccordement" access="remote" returntype="numeric">
		<cfargument name="IDSOUS_TETE" type="numeric" required="true">
		<cfargument name="IDACTION" type="numeric" required="true">
		<cfargument name="IDACTION_PREC" type="numeric" required="true">
		<cfargument name="IDTYPE_RACCORDEMENT" type="numeric" required="true">
		<cfargument name="DATE_EFFET" type="string" required="true">
		<cfargument name="REFERENCE" type="string" required="true" default=" ">
		<cfargument name="COMMENTAIRES" type="string" required="true">
		<cfargument name="IDOPERATEUR_INDIRECT" type="numeric" required="true">
		
		<cfargument name="LISTE_A_SORTIR" type="array" required="false">
		<cfargument name="LISTE_A_ENTRER" type="array" required="false">
		
		
		<!--- PROCEDURE action_insert ( p_idsous_tete                     IN INTEGER,
                                        p_idracco_action                  IN INTEGER,
                                        p_idtype_raccordement             IN INTEGER,
                                        p_app_loginid                     IN INTEGER,
                                        p_date_effet                      IN VARCHAR2,
                                        p_reference                       IN VARCHAR2,
                                        p_commentaires                    IN VARCHAR2,
                                        p_operateurid_indirect_cible1     IN INTEGER,
                                        p_retour                          OUT INTEGER)
		--->
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.action_insert">						
			<cfprocparam  variable="p_idsous_tete" value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="p_idracco_action" value="#IDACTION#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam  variable="p_idracco_action_prec_histo" value="#IDACTION_PREC#" cfsqltype="CF_SQL_INTEGER" type="in" null="#iif(IDACTION_PREC eq -1, de("yes"), de("no"))#">			
			<cfprocparam  variable="p_idtype_raccordement" value="#IDTYPE_RACCORDEMENT#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="p_app_loginid " value="#getidUser()#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="p_date_effet" value="#DATE_EFFET#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  variable="p_reference" value="#iif(len(REFERENCE) lte 0, de('-'), de(REFERENCE))#" cfsqltype="CF_SQL_VARCHAR" type="in"> 
			<cfprocparam  variable="p_commentaires" value="#COMMENTAIRES#" cfsqltype="CF_SQL_VARCHAR" type="in" null="#iif(len(COMMENTAIRES) lte 0, de("yes"), de("no"))#">
			<cfprocparam  variable="p_operateurid_indirect_cible1" value="#IDOPERATEUR_INDIRECT#" cfsqltype="CF_SQL_INTEGER" type="in" null="#iif(IDOPERATEUR_INDIRECT eq -1, de("yes"), de("no"))#">
			<cfprocparam  variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
				
		
		<cfset retourActionLigne = p_retour>
		<cfset resultIn = 0>
		<cfset resultOut = 0>
		
		<cfif retourActionLigne gt 0>
		
			<!--- INVENTAIRE------>
			
			<!--- SORTIR DE L'inventaire ------>
			<cfset SORTIR_INVENTAIRE = 61>
			<cfset resultOut = entrerSortirRessources(LISTE_A_SORTIR,SORTIR_INVENTAIRE,DATE_EFFET,COMMENTAIRES)>
					
			<!--- ENTRER DANS L'inventaire ------>
			<cfset ENTRER_INVENTAIRE = 62>
			<cfset resultIn =  entrerSortirRessources(LISTE_A_ENTRER,ENTRER_INVENTAIRE,DATE_EFFET,COMMENTAIRES)>
						
			<cfif resultOut gt 0 and resultIn gt 0>
				<cfreturn 3>
			<cfelseif resultOut gt 0 and resultOut lte 0>
				<cfreturn 2>
			<cfelseif resultOut lte 0 and resultOut gt 0>
				<cfreturn 1>
			<cfelseif resultOut lte 0 and resultOut lte 0>
				<cfreturn 0>
			</cfif>		
		
		<cfelse>
			<cfreturn -1>
		</cfif>
		
	</cffunction>
	
	<cffunction name="entrerSortirRessources" access="private" returntype="numeric" output="false">                         	 			
		
		<cfargument name="liste_idinventaire_Produits" type="array" required="true">
		<cfargument name="idinv_actions" type="numeric" required="true">
		<cfargument name="date_action" type="string" required="true">			
		<cfargument name="commentaire_action" type="string" required="true">			
		 

		<cfset liste = arrayTolist(liste_idinventaire_Produits,',')>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.CREATEACTION_HORSOPE">					
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_list_idinventaire_produit" value="#liste#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_actions" value="#idinv_actions#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_op_action_prec" null="yes">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_userid" value="#getidUser()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaire_action" value="#commentaire_action#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_etat_actuel" value="1"/>				
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_dans_inventaire" value="#iif(idinv_actions eq 61 , de("0"), de("1"))#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_qte" value="1"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_action" value="#date_action#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_retour"/>		
		</cfstoredproc>
					
		<cfreturn p_retour/>					
     </cffunction>     
	 
	<cffunction name="getActionsPossiblesByEtat" access="remote" returntype="query">
		<cfargument name="idTypeRaccordement" type="numeric" required="true">		
		<!---    PROCEDURE Action_by_type_racco (     p_idtype_raccordement      IN INTEGER,
         				                              p_retour                   OUT SYS_REFCURSOR);
		  --->
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.Action_by_type_racco_v2">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idtype_raccordement" value="#idTypeRaccordement#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_langid"				value="#SESSION.USER.IDGLOBALIZATION#" >
			<cfprocresult name="qResult">
		</cfstoredproc>
		
		<cfreturn qResult>
	</cffunction>
	
	
	<cffunction name="getListeOperateurs" access="remote" returntype="query">
		<!---    PROCEDURE pkg_cv_commande.l_all_operateurs(p_retour => :p_retour);
		  --->
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_commande.l_all_operateurs">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction>
	
	<cffunction name="getModeRaccordementPrecedent" access="remote" returntype="query">
		<cfargument name="idLastAction" type="numeric" required="true">	
		<cfquery name="qResult" datasource="#SESSION.OffreDSN#">
			SELECT * 
			FROM racco_histo rh ,type_raccordement tr 
			WHERE rh.idracco_histo = #idLastAction# 
			AND tr.idtype_raccordement = rh.idtype_raccordement
		</cfquery>
		<cfreturn qResult>
	</cffunction>
	<!--- ====== Fin Onglet Eat Raccordement ================================================== --->
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<!--- ====== Onglet Cablage =============================================================== --->
	<cffunction name="getInfosCablage" access="remote" returntype="query">
		<cfargument name="idsousTete" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.get_infos_cablage">
			<cfprocparam  value="#idsousTete#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction>
	
 
	
	<cffunction name="majInfoCablage" access="remote" returntype="numeric">
		<cfargument name="xmlParamObject" required="true" type="XML">
				
			<!--- <cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR" value="#this.getDATE_ACHAT()#" null="#iif((structkeyExists(infoCablage,'BAT'), de("yes"), de("no"))#"> --->
			<cfset vlIDSOUS_TETE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDSOUS_TETE")>
			<cfset vlBAT = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"BAT")>
			<cfset vlETAGE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"ETAGE")>
			<cfset vlSALLE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"SALLE")>
			<cfset vlNOEUD = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"NOEUD")>
			<cfset vlREGLETTE1 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"REGLETTE1")>
			<cfset vlPAIRE1 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"PAIRE1")>
			<cfset vlREGLETTE2 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"REGLETTE2")>
			<cfset vlPAIRE2 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"PAIRE2")>
			<cfset vlAMORCE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"AMORCE")>
			<cfset vlNOMBRE_POSTES = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"NOMBRE_POSTES")>
			<cfset vlCARTE_PABX = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"CARTE_PABX")>
			<cfset vlSECRET_IDENTIFIANT = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"SECRET_IDENTIFIANT")>
			<cfset vlPUBLICATION_ANNUAIRE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"PUBLICATION_ANNUAIRE")>
			<cfset vlDISCRIMINATION = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"DISCRIMINATION")>
			<cfset vlCOMMENTAIRE_CABLAGE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"COMMENTAIRE_CABLAGE")>
			<cfset vlV7 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"V7")>
			<cfset vlV8 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"V8")>
			<cfset vlNBRE_CANAUX = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"NBRE_CANAUX")>
			<cfset vlCANAUX_MIXTE_CAN_DEB = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"CANAUX_MIXTE_CAN_DEB")>
			<cfset vlCANAUX_MIXTE_CAN_FIN = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"CANAUX_MIXTE_CAN_FIN")>
			<cfset vlCANAUX_ENTREE_CAN_DEB = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"CANAUX_ENTREE_CAN_DEB")>
			<cfset vlCANAUX_ENTREE_CAN_FIN = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"CANAUX_ENTREE_CAN_FIN")>
			<cfset vlCANAUX_SORTIE_CAN_DEB = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"CANAUX_SORTIE_CAN_DEB")>
			<cfset vlCANAUX_SORTIE_CAN_FIN = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"CANAUX_SORTIE_CAN_FIN")>
			<cfset vlIDLL = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDLL")>
			<cfset vlLS_LEGA_TETE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGA_TETE")>
			<cfset vlLS_LEGA_ELEM_ACTIF = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGA_ELEM_ACTIF")>
			<cfset vlLS_LEGA_NOMSITE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGA_NOMSITE")>
			<cfset vlLS_LEGA_ADRESSE1 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGA_ADRESSE1")>
			<cfset vlLS_LEGA_ADRESSE2 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGA_ADRESSE2")>
			<cfset vlLS_LEGA_ZIPCODE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGA_ZIPCODE")>
			<cfset vlLS_LEGA_COMMUNE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGA_COMMUNE")>
			<cfset vlLS_LEGA_ELEM_ACTIF_AMORCE_E = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGA_ELEM_ACTIF_AMORCE_E")>
			<cfset vlLS_LEGA_ELEM_ACTIF_AMORCE_R = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGA_ELEM_ACTIF_AMORCE_R")>
			<cfset vlLS_LEGA_TETE_AMORCE_E = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGA_TETE_AMORCE_E")>
			<cfset vlLS_LEGA_TETE_AMORCE_R = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGA_TETE_AMORCE_R")>
			<cfset vlLS_NATURE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_NATURE")>
			<cfset vlLS_DEBIT2 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_DEBIT2")>
			<cfset vlLS_DISTANCE2 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_DISTANCE2")>
			<cfset vlLS_LEGB_TETE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGB_TETE")>
			<cfset vlLS_LEGB_ELEM_ACTIF = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGB_ELEM_ACTIF")>
			<cfset vlLS_LEGB_NOMSITE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGB_NOMSITE")>
			<cfset vlLS_LEGB_ADRESSE1 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGB_ADRESSE1")>
			<cfset vlLS_LEGB_ADRESSE2 = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGB_ADRESSE2")>
			<cfset vlLS_LEGB_ZIPCODE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGB_ZIPCODE")>
			<cfset vlLS_LEGB_COMMUNE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGB_COMMUNE")>
			<cfset vlLS_LEGB_ELEM_ACTIF_AMORCE_E = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGB_ELEM_ACTIF_AMORCE_E")>
			<cfset vlLS_LEGB_ELEM_ACTIF_AMORCE_R = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGB_ELEM_ACTIF_AMORCE_R")>
			<cfset vlLS_LEGB_TETE_AMORCE_E = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGB_TETE_AMORCE_E")>
			<cfset vlLS_LEGB_TETE_AMORCE_R = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"LS_LEGB_TETE_AMORCE_R")>
			
			
			
			<cfset IDSOUS_TETE = VAL(vlIDSOUS_TETE[1].xmlText)>
			<cfset BAT = (vlBAT[1].xmlText)>
			<cfset ETAGE = (vlETAGE[1].xmlText)>
			<cfset SALLE = (vlSALLE[1].xmlText)>
			<cfset NOEUD = (vlNOEUD[1].xmlText)>
			<cfset REGLETTE1 = (vlREGLETTE1[1].xmlText)>
			<cfset PAIRE1 = (vlPAIRE1[1].xmlText)>
			<cfset REGLETTE2 = (vlREGLETTE2[1].xmlText)>
			<cfset PAIRE2 = (vlPAIRE2[1].xmlText)>
			<cfset AMORCE = (vlAMORCE[1].xmlText)>
			<cfset NOMBRE_POSTES = VAL(vlNOMBRE_POSTES[1].xmlText)>
			<cfset CARTE_PABX = (vlCARTE_PABX[1].xmlText)>
			<cfset SECRET_IDENTIFIANT = VAL(vlSECRET_IDENTIFIANT[1].xmlText)>
			<cfset PUBLICATION_ANNUAIRE = VAL(vlPUBLICATION_ANNUAIRE[1].xmlText)>
			<cfset DISCRIMINATION = (vlDISCRIMINATION[1].xmlText)>
			<cfset COMMENTAIRE_CABLAGE = (vlCOMMENTAIRE_CABLAGE[1].xmlText)>
			<cfset V7 = (vlV7[1].xmlText)>
			<cfset V8 = (vlV8[1].xmlText)>
			<cfset NBRE_CANAUX = VAL(vlNBRE_CANAUX[1].xmlText)>
			<cfset CANAUX_MIXTE_CAN_DEB = (vlCANAUX_MIXTE_CAN_DEB[1].xmlText)>
			<cfset CANAUX_MIXTE_CAN_FIN = (vlCANAUX_MIXTE_CAN_FIN[1].xmlText)>
			<cfset CANAUX_ENTREE_CAN_DEB = (vlCANAUX_ENTREE_CAN_DEB[1].xmlText)>
			<cfset CANAUX_ENTREE_CAN_FIN = (vlCANAUX_ENTREE_CAN_FIN[1].xmlText)>
			<cfset CANAUX_SORTIE_CAN_DEB = (vlCANAUX_SORTIE_CAN_DEB[1].xmlText)>
			<cfset CANAUX_SORTIE_CAN_FIN = (vlCANAUX_SORTIE_CAN_FIN[1].xmlText)>
			<cfset IDLL = VAL(vlIDLL[1].xmlText)>
			<cfset LS_LEGA_TETE = (vlLS_LEGA_TETE[1].xmlText)>
			<cfset LS_LEGA_ELEM_ACTIF = (vlLS_LEGA_ELEM_ACTIF[1].xmlText)>
			<cfset LS_LEGA_NOMSITE = (vlLS_LEGA_NOMSITE[1].xmlText)>
			<cfset LS_LEGA_ADRESSE1 = (vlLS_LEGA_ADRESSE1[1].xmlText)>
			<cfset LS_LEGA_ADRESSE2 = (vlLS_LEGA_ADRESSE2[1].xmlText)>
			<cfset LS_LEGA_ZIPCODE = (vlLS_LEGA_ZIPCODE[1].xmlText)>
			<cfset LS_LEGA_COMMUNE = (vlLS_LEGA_COMMUNE[1].xmlText)>
			<cfset LS_LEGA_ELEM_ACTIF_AMORCE_E = (vlLS_LEGA_ELEM_ACTIF_AMORCE_E[1].xmlText)>
			<cfset LS_LEGA_ELEM_ACTIF_AMORCE_R = (vlLS_LEGA_ELEM_ACTIF_AMORCE_R[1].xmlText)>
			<cfset LS_LEGA_TETE_AMORCE_E = (vlLS_LEGA_TETE_AMORCE_E[1].xmlText)>
			<cfset LS_LEGA_TETE_AMORCE_R = (vlLS_LEGA_TETE_AMORCE_R[1].xmlText)>
			<cfset LS_NATURE = (vlLS_NATURE[1].xmlText)>
			<cfset LS_DEBIT2 = (vlLS_DEBIT2[1].xmlText)>
			<cfset LS_DISTANCE2 = (vlLS_DISTANCE2[1].xmlText)>
			<cfset LS_LEGB_TETE = (vlLS_LEGB_TETE[1].xmlText)>
			<cfset LS_LEGB_ELEM_ACTIF = (vlLS_LEGB_ELEM_ACTIF[1].xmlText)>
			<cfset LS_LEGB_NOMSITE = (vlLS_LEGB_NOMSITE[1].xmlText)>
			<cfset LS_LEGB_ADRESSE1 = (vlLS_LEGB_ADRESSE1[1].xmlText)>
			<cfset LS_LEGB_ADRESSE2 = (vlLS_LEGB_ADRESSE2[1].xmlText)>
			<cfset LS_LEGB_ZIPCODE = (vlLS_LEGB_ZIPCODE[1].xmlText)>
			<cfset LS_LEGB_COMMUNE = (vlLS_LEGB_COMMUNE[1].xmlText)>
			<cfset LS_LEGB_ELEM_ACTIF_AMORCE_E = (vlLS_LEGB_ELEM_ACTIF_AMORCE_E[1].xmlText)>
			<cfset LS_LEGB_ELEM_ACTIF_AMORCE_R = (vlLS_LEGB_ELEM_ACTIF_AMORCE_R[1].xmlText)>
			<cfset LS_LEGB_TETE_AMORCE_E = (vlLS_LEGB_TETE_AMORCE_E[1].xmlText)>
			<cfset LS_LEGB_TETE_AMORCE_R = (vlLS_LEGB_TETE_AMORCE_R[1].xmlText)>
			

			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.maj_infos_cablage">
				<cfprocparam  value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER" type="in">			
				<cfprocparam  value="#BAT#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#ETAGE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#SALLE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#NOEUD#" cfsqltype="CF_SQL_VARCHAR" type="in">			
				<cfprocparam  value="#REGLETTE1#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#PAIRE1#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#REGLETTE2#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#PAIRE2#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#AMORCE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#NOMBRE_POSTES#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#CARTE_PABX#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#SECRET_IDENTIFIANT#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#PUBLICATION_ANNUAIRE#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#DISCRIMINATION#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#NBRE_CANAUX#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#CANAUX_MIXTE_CAN_DEB#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#CANAUX_MIXTE_CAN_FIN#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#CANAUX_ENTREE_CAN_DEB#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#CANAUX_ENTREE_CAN_FIN#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#CANAUX_SORTIE_CAN_DEB#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#CANAUX_SORTIE_CAN_FIN#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#IDLL#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam  value="#LS_LEGA_TETE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGA_ELEM_ACTIF#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGA_NOMSITE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGA_ADRESSE1#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGA_ADRESSE2#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGA_ZIPCODE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGA_COMMUNE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGA_ELEM_ACTIF_AMORCE_E#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGA_ELEM_ACTIF_AMORCE_R#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGA_TETE_AMORCE_E#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGA_TETE_AMORCE_R#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_NATURE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_DEBIT2#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_DISTANCE2#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGB_TETE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGB_ELEM_ACTIF#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGB_NOMSITE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGB_ADRESSE1#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGB_ADRESSE2#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGB_ZIPCODE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGB_COMMUNE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGB_ELEM_ACTIF_AMORCE_E#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGB_ELEM_ACTIF_AMORCE_R#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGB_TETE_AMORCE_E#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#LS_LEGB_TETE_AMORCE_R#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#COMMENTAIRE_CABLAGE#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#V7#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  value="#V8#" cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam  variable="result"  cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			
			
			<!---
		       PROCEDURE MAJ_INFOS_CABLAGE(      
		       						p_idsous_tete              	   IN           INTEGER,
                                    p_BAT                          IN           VARCHAR2,
                                    p_ETAGE                        IN           VARCHAR2,
                                    p_SALLE                        IN           VARCHAR2,
                                    p_NOEUD                        IN           VARCHAR2,
                                    p_REGLETTE1                    IN           VARCHAR2,
                                    p_PAIRE1                       IN           VARCHAR2,
                                    p_REGLETTE2                    IN           VARCHAR2,
                                    p_PAIRE2                       IN           VARCHAR2,
                                    p_AMORCE                       IN           VARCHAR2,
                                    p_NOMBRE_POSTES                IN           INTEGER,
                                    p_CARTE                        IN           VARCHAR2,
                                    p_SECRET_IDENTIFIANT           IN           INTEGER,
                                    p_PUBLICATION_ANNUAIRE         IN           INTEGER,
                                    p_DISCRIMINATION       		   IN           VARCHAR2,
                                    p_NBRE_CANAUX                  IN           INTEGER,
                                    p_CANAUX_MIXTE_CAN_DEB         IN           VARCHAR2,
                                    p_CANAUX_MIXTE_CAN_FIN         IN           VARCHAR2,
                                    p_CANAUX_ENTREE_CAN_DEB        IN           VARCHAR2,
                                    p_CANAUX_ENTREE_CAN_FIN        IN           VARCHAR2,
                                    p_CANAUX_SORTIE_CAN_DEB        IN           VARCHAR2,
                                    p_CANAUX_SORTIE_CAN_FIN        IN           VARCHAR2,
                                    p_IDLL                         IN           INTEGER,
									p_LS_LEGA_TETE                 IN           VARCHAR2,
                                    p_LS_LEGA_ELEM_ACTIF           IN           VARCHAR2,
                                    p_LS_LEGA_NOMSITE              IN           VARCHAR2,
                                    p_LS_LEGA_ADRESSE1             IN           VARCHAR2,
                                    p_LS_LEGA_ADRESSE2             IN           VARCHAR2,
                                    p_LS_LEGA_ZIPCODE              IN           VARCHAR2,
                                    p_LS_LEGA_COMMUNE              IN           VARCHAR2,
                                    p_LS_LEGA_ELEM_ACTIF_AMORCE_E  IN           VARCHAR2,
                                    p_LS_LEGA_ELEM_ACTIF_AMORCE_R  IN           VARCHAR2,
                                    p_LS_LEGA_TETE_AMORCE_E        IN           VARCHAR2,
                                    p_LS_LEGA_TETE_AMORCE_R        IN           VARCHAR2,
                                    p_LS_NATURE                    IN           VARCHAR2,
                                    p_LS_DEBIT2                    IN           VARCHAR2,
                                    p_LS_DISTANCE2                 IN           VARCHAR2,
                                    p_LS_LEGB_TETE                 IN           VARCHAR2,
                                    p_LS_LEGB_ELEM_ACTIF           IN           VARCHAR2,
                                    p_LS_LEGB_NOMSITE              IN           VARCHAR2,
                                    p_LS_LEGB_ADRESSE1             IN           VARCHAR2,
                                    p_LS_LEGB_ADRESSlE2            IN           VARCHAR2,
                                    p_LS_LEGB_ZIPCODE      		   IN           VARCHAR2,
                                    p_LS_LEGB_COMMUNE       	   IN           VARCHAR2,
                                    p_LS_LEGB_ELEM_ACTIF_AMORCE_E  IN           VARCHAR2,
                                    p_LS_LEGB_ELEM_ACTIF_AMORCE_R  IN           VARCHAR2,
                                    p_LS_LEGB_TETE_AMORCE_E        IN           VARCHAR2,
                                    p_LS_LEGB_TETE_AMORCE_R        IN           VARCHAR2,
                                    p_COMMENTAIRE_CABLAGE          IN           VARCHAR2,
	                                p_v7                           IN           VARCHAR2,
	                                p_v8                           IN           VARCHAR2,
                                    p_retour                       OUT    INTEGER
                    )--->
					
		<cfreturn result>
	</cffunction>
	
	<!--- ======= TRANCHES SDA ==== --->	 
	<cffunction name="getListeTranches" access="remote" returntype="query">
		<cfargument name="xmlParamObject" required="true" type="xml">
				
		<cfset vlIDSOUS_TETE= getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDSOUS_TETE")>
		<cfset IDSOUS_TETE = VAL(vlIDSOUS_TETE[1].xmlText)>
		
	 <cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.GET_TRANCHES">
			<cfprocparam  value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocresult name="result">
		</cfstoredproc>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="majTranche" access="remote" returntype="numeric">
		<cfargument name="xmlParamObject" required="true" type="xml">
		
		<cfset vlIDSOUS_TETE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDSOUS_TETE")>
		<cfset vlIDTRANCHE_SDA = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDTRANCHE_SDA")>
		<cfset vlTRANCHE_SDA = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"TRANCHE_SDA")>
		<cfset vlNUM_DEPART = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"NUM_DEPART")>
		<cfset vlNUM_FIN = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"NUM_FIN")>
		<cfset vlTYPE_TRANCHE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"TYPE_TRANCHE")>
		
		<cfset IDSOUS_TETE = VAL(vlIDSOUS_TETE[1].xmlText)>
		<cfset IDTRANCHE_SDA = VAL(vlIDTRANCHE_SDA[1].xmlText)>
		<cfset TRANCHE_SDA = (vlTRANCHE_SDA[1].xmlText)>
		<cfset NUM_DEPART = (vlNUM_DEPART[1].xmlText)>
		<cfset NUM_FIN = (vlNUM_FIN[1].xmlText)>
		<cfset TYPE_TRANCHE = (vlTYPE_TRANCHE[1].xmlText)>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.MAJ_TRANCHE">			
			
			<cfprocparam  value="#IDTRANCHE_SDA#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#NUM_DEPART#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#NUM_FIN#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#TYPE_TRANCHE#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#TRANCHE_SDA#" cfsqltype="CF_SQL_VARCHAR" type="in">
						
			<cfprocparam  variable="result"  cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="addTranche" access="remote" returntype="numeric">
		<cfargument name="xmlParamObject" required="true" type="xml">	
		
		<cfset vlIDSOUS_TETE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDSOUS_TETE")>
		<cfset vlIDTRANCHE_SDA = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDTRANCHE_SDA")>
		<cfset vlTRANCHE_SDA = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"TRANCHE_SDA")>
		<cfset vlNUM_DEPART = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"NUM_DEPART")>
		<cfset vlNUM_FIN = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"NUM_FIN")>
		<cfset vlTYPE_TRANCHE = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"TYPE_TRANCHE")>
		
		<cfset IDSOUS_TETE = VAL(vlIDSOUS_TETE[1].xmlText)>
		<cfset IDTRANCHE_SDA = VAL(vlIDTRANCHE_SDA[1].xmlText)>
		<cfset TRANCHE_SDA = (vlTRANCHE_SDA[1].xmlText)>
		<cfset NUM_DEPART = (vlNUM_DEPART[1].xmlText)>
		<cfset NUM_FIN = (vlNUM_FIN[1].xmlText)>
		<cfset TYPE_TRANCHE = (vlTYPE_TRANCHE[1].xmlText)>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.CREATE_TRANCHE">
			<cfprocparam  value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  value="#NUM_DEPART#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#NUM_FIN#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#TYPE_TRANCHE#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam  value="#TRANCHE_SDA#" cfsqltype="CF_SQL_VARCHAR" type="in">
						
			<cfprocparam  variable="result"  cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="delTranche" access="remote" returntype="numeric">
		<cfargument name="xmlParamObject" required="true" type="xml">
		<cfset vlIDTRANCHE_SDA = getXmlParamUtil().getParamValuesByKey(xmlParamObject,"IDTRANCHE_SDA")>
		<cfset IDTRANCHE_SDA = VAL(vlIDTRANCHE_SDA[1].xmlText)>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GTLIGNE.DEL_TRANCHE">
			<cfprocparam  value="#IDTRANCHE_SDA#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam  variable="result"  cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		
		<cfreturn result>
	</cffunction>
	
	<!--- ====== Fin Onglet Cablage =========================================================== --->
	
</cfcomponent>
