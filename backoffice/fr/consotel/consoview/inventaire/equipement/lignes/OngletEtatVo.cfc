<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.equipement.lignes.OngletEtatVo">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="IDSOUS_TETE" type="numeric" default="0">
	<cfproperty name="ETAT" type="string" default="ACTIVE">
	<cfproperty name="DATE_COMMANDE" type="date" default="">
	<cfproperty name="REF_COMMANDE" type="string" default="">
	<cfproperty name="BOOL_TITULAIRE" type="numeric" default="0">
	<cfproperty name="BOOL_PAYEUR" type="numeric" default="0">
	<cfproperty name="COMMENTAIRE_ETAT" type="string" default="">
	<cfproperty name="DATE_MISE_EN_SERV" type="date" default="">
	<cfproperty name="REF_MISE_EN_SERV" type="string" default="">
	<cfproperty name="GEST_MISE_EN_SERV" type="string" default="">
	<cfproperty name="DATE_PRESELECTION" type="date" default="">
	<cfproperty name="REF_PRESELECTION" type="string" default="">
	<cfproperty name="GEST_PRESELECTION" type="string" default="">
	<cfproperty name="DATE_SUSPENSION" type="date" default="">
	<cfproperty name="REF_SUSPENSION" type="string" default="">
	<cfproperty name="GEST_SUSPENSION" type="string" default="">
	<cfproperty name="DATE_RESILIATION" type="date" default="">
	<cfproperty name="REF_RESILIATION" type="string" default="">
	<cfproperty name="GEST_RESILIATION" type="string" default="">
	<cfproperty name="V3" type="string" default="Champ Personnalisé C3">
	<cfproperty name="V4" type="string" default="Champ Personnalisé C4">

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.IDSOUS_TETE = 0;
		variables.ETAT = "ACTIVE";
		variables.DATE_COMMANDE = "";
		variables.REF_COMMANDE = "";
		variables.BOOL_TITULAIRE = 0;
		variables.BOOL_PAYEUR = 0;
		variables.COMMENTAIRE_ETAT = "";
		variables.DATE_MISE_EN_SERV = "";
		variables.REF_MISE_EN_SERV = "";
		variables.GEST_MISE_EN_SERV = "";
		variables.DATE_PRESELECTION = "";
		variables.REF_PRESELECTION = "";
		variables.GEST_PRESELECTION = "";
		variables.DATE_SUSPENSION = "";
		variables.REF_SUSPENSION = "";
		variables.GEST_SUSPENSION = "";
		variables.DATE_RESILIATION = "";
		variables.REF_RESILIATION = "";
		variables.GEST_RESILIATION = "";
		variables.V3 = "";
		variables.V4 = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="OngletEtatVo">
		<cfreturn this>
	</cffunction>
	<cffunction name="getIDSOUS_TETE" output="false" access="public" returntype="any">
		<cfreturn variables.IDSOUS_TETE>
	</cffunction>

	<cffunction name="setIDSOUS_TETE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDSOUS_TETE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getETAT" output="false" access="public" returntype="any">
		<cfreturn variables.ETAT>
	</cffunction>

	<cffunction name="setETAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.ETAT = arguments.val>
	</cffunction>

	<cffunction name="getDATE_COMMANDE" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_COMMANDE>
	</cffunction>

	<cffunction name="setDATE_COMMANDE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_COMMANDE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getREF_COMMANDE" output="false" access="public" returntype="any">
		<cfreturn variables.REF_COMMANDE>
	</cffunction>

	<cffunction name="setREF_COMMANDE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.REF_COMMANDE = arguments.val>
	</cffunction>

	<cffunction name="getBOOL_TITULAIRE" output="false" access="public" returntype="any">
		<cfreturn variables.BOOL_TITULAIRE>
	</cffunction>

	<cffunction name="setBOOL_TITULAIRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.BOOL_TITULAIRE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getBOOL_PAYEUR" output="false" access="public" returntype="any">
		<cfreturn variables.BOOL_PAYEUR>
	</cffunction>

	<cffunction name="setBOOL_PAYEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.BOOL_PAYEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getCOMMENTAIRE_ETAT" output="false" access="public" returntype="any">
		<cfreturn variables.COMMENTAIRE_ETAT>
	</cffunction>

	<cffunction name="setCOMMENTAIRE_ETAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.COMMENTAIRE_ETAT = arguments.val>
	</cffunction>

	<cffunction name="getDATE_MISE_EN_SERV" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_MISE_EN_SERV>
	</cffunction>

	<cffunction name="setDATE_MISE_EN_SERV" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_MISE_EN_SERV = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getREF_MISE_EN_SERV" output="false" access="public" returntype="any">
		<cfreturn variables.REF_MISE_EN_SERV>
	</cffunction>

	<cffunction name="setREF_MISE_EN_SERV" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.REF_MISE_EN_SERV = arguments.val>
	</cffunction>

	<cffunction name="getGEST_MISE_EN_SERV" output="false" access="public" returntype="any">
		<cfreturn variables.GEST_MISE_EN_SERV>
	</cffunction>

	<cffunction name="setGEST_MISE_EN_SERV" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.GEST_MISE_EN_SERV = arguments.val>
	</cffunction>

	<cffunction name="getDATE_PRESELECTION" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_PRESELECTION>
	</cffunction>

	<cffunction name="setDATE_PRESELECTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_PRESELECTION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getREF_PRESELECTION" output="false" access="public" returntype="any">
		<cfreturn variables.REF_PRESELECTION>
	</cffunction>

	<cffunction name="setREF_PRESELECTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.REF_PRESELECTION = arguments.val>
	</cffunction>

	<cffunction name="getGEST_PRESELECTION" output="false" access="public" returntype="any">
		<cfreturn variables.GEST_PRESELECTION>
	</cffunction>

	<cffunction name="setGEST_PRESELECTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.GEST_PRESELECTION = arguments.val>
	</cffunction>

	<cffunction name="getDATE_SUSPENSION" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_SUSPENSION>
	</cffunction>

	<cffunction name="setDATE_SUSPENSION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_SUSPENSION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getREF_SUSPENSION" output="false" access="public" returntype="any">
		<cfreturn variables.REF_SUSPENSION>
	</cffunction>

	<cffunction name="setREF_SUSPENSION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.REF_SUSPENSION = arguments.val>
	</cffunction>

	<cffunction name="getGEST_SUSPENSION" output="false" access="public" returntype="any">
		<cfreturn variables.GEST_SUSPENSION>
	</cffunction>

	<cffunction name="setGEST_SUSPENSION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.GEST_SUSPENSION = arguments.val>
	</cffunction>

	<cffunction name="getDATE_RESILIATION" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_RESILIATION>
	</cffunction>

	<cffunction name="setDATE_RESILIATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_RESILIATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getREF_RESILIATION" output="false" access="public" returntype="any">
		<cfreturn variables.REF_RESILIATION>
	</cffunction>

	<cffunction name="setREF_RESILIATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.REF_RESILIATION = arguments.val>
	</cffunction>

	<cffunction name="getGEST_RESILIATION" output="false" access="public" returntype="any">
		<cfreturn variables.GEST_RESILIATION>
	</cffunction>

	<cffunction name="setGEST_RESILIATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.GEST_RESILIATION = arguments.val>
	</cffunction>

	<cffunction name="getV3" output="false" access="public" returntype="any">
		<cfreturn variables.V3>
	</cffunction>

	<cffunction name="setV3" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.V3 = arguments.val>
	</cffunction>

	<cffunction name="getV4" output="false" access="public" returntype="any">
		<cfreturn variables.V4>
	</cffunction>

	<cffunction name="setV4" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.V4 = arguments.val>
	</cffunction>

</cfcomponent>