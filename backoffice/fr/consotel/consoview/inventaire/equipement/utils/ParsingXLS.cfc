<cfcomponent
	displayname="ParsingXLS"
	output="false"
	hint="Handles the reading of Microsoft Excel files using POI and ColdFusion.">
 
	<cffunction name="ReadExcel" access="remote" returntype="any" output="false"
		hint="Reads an Excel file into an array of strutures that contains the Excel file information OR if a specific sheet index is passed in, only that sheet object is returned.">

		<cfargument
			name="FilePath"
			type="string"
			required="true"
			hint="The expanded file path of the Excel file."
			/>
 		
		<cfargument
			name="NbColumn"
			type="numeric"
			required="true"
			hint="The number of column that contain the Excel file. (for now, idem for every sheet)"
			/>
		
		<cfargument
			name="HasHeaderRow"
			type="boolean"
			required="false"
			default="false"
			hint="Flags the Excel files has using the first data row a header column. If so, this column will be excluded from the resultant query."
			/>
 
		<cfargument
			name="SheetIndex"
			type="numeric"
			required="false"
			default="-1"
			hint="If passed in, only that sheet object will be returned (not an array of sheet objects)."
			/>

			
		<cfset MY_RESULT = StructNew()>
		
		<cfset MY_RESULT.InputStream = CreateObject("java","java.io.FileInputStream")/>
		<cfset MY_RESULT.InputStream.init(#FilePath#) >
		
		<cfset MY_RESULT.ExcelFileSystem = CreateObject("java","org.apache.poi.poifs.filesystem.POIFSFileSystem")/>
		<cfset MY_RESULT.ExcelFileSystem.Init(MY_RESULT.InputStream)/>	
		
		<cfset MY_RESULT.WorkBook = CreateObject("java","org.apache.poi.hssf.usermodel.HSSFWorkbook")/>
		<cfset MY_RESULT.WorkBook.Init(MY_RESULT.ExcelFileSystem)/> 

		<cfif #SheetIndex# GTE 0>
				<cfreturn ReadExcelSheet(MY_RESULT.WorkBook,#NbColumn#,#SheetIndex#,#HasHeaderRow#)>
			<cfelse>
				<cfset MY_RESULT.Sheets = ArrayNew( 1 )>
				<cfset MY_RESULT.SheetIndex = 0>

				<cfloop condition="MY_RESULT.SheetIndex LT MY_RESULT.WorkBook.GetNumberOfSheets()">
					<cfset temp0 = ArrayAppend(MY_RESULT.Sheets, ReadExcelSheet(MY_RESULT.WorkBook, #NbColumn#, MY_RESULT.SheetIndex, #HasHeaderRow#))>
					<cfset MY_RESULT.SheetIndex = MY_RESULT.SheetIndex + 1 >
				</cfloop>
				
				<cfreturn MY_RESULT.Sheets>
			</cfif>

		<cfreturn MY_RESULT.Sheets>
	</cffunction>	
 
 
	<cffunction name="ReadExcelSheet" access="public" returntype="struct" output="false"
		hint="Takes an Excel workbook and reads the given sheet (by index) into a structure.">
 
		<!--- Define arguments. --->
		<cfargument
			name="WorkBook"
			type="any"
			required="true"
			hint="This is a workbook object created by the POI API."
			/>
 
		<cfargument
			name="NbColumn"
			type="numeric"
			required="true"
			hint="The number of column that contain the sheet."
			/>

		<cfargument
			name="SheetIndex"
			type="numeric"
			required="false"
			default="0"
			hint="This is the index of the sheet within the passed in workbook. This is a ZERO-based index (coming from a Java object)."
			/>
 
		<cfargument
			name="HasHeaderRow"
			type="boolean"
			required="false"
			default="false"
			hint="This flags the sheet as having a header row or not (if so, it will NOT be read into the query)."
			/>

			<cfset NB_COLONNES = #NbColumn#>
			
 			<cfset MY_RESULT = StructNew()>
			<cfset MY_RESULT.ArrCell = ArrayNew( 1 ) >
			<cfset myIndex = 1>
			
			// Set up the default return structure.
			<cfset MY_RESULT.SheetData = StructNew()>
		
			// This is the index of the sheet within the workbook.
			<cfset MY_RESULT.SheetData.Index = #SheetIndex#>
			
			// This is the name of the sheet tab. (Tmp)
			<cfset MY_RESULT.SheetData.NameTmp = #WorkBook#>
			<cfset MY_RESULT.SheetData.Name = MY_RESULT.SheetData.NameTmp.GetSheetName(JavaCast( "int", MY_RESULT.SheetData.Index ))>
			
			// This is the query created from the sheet.
			<cfset MY_RESULT.SheetData.Query = "">
 
			// This is a flag for the header row.
			<cfset MY_RESULT.SheetData.HasHeaderRow = #HasHeaderRow#>
 
			// An array of header columns names.
			<cfset MY_RESULT.SheetData.ColumnNames = ArrayNew( 1 )>
 
			// This keeps track of the min number of data columns.
			<cfset MY_RESULT.SheetData.MinColumnCount = 0>
 
			// This keeps track of the max number of data columns.
			<cfset MY_RESULT.SheetData.MaxColumnCount = 0>			
 
			// Get the sheet object at this index of the
			// workbook. This is based on the passed in data.
			<cfset MY_RESULT.SheetTmp = #WorkBook#>
			<cfset MY_RESULT.Sheet = MY_RESULT.SheetTmp.GetSheetAt(JavaCast( "int", MY_RESULT.SheetData.Index ))>
			
			// Loop over the rows in the Excel sheet. For each
			// row, we simply want to capture the number of
			// physical columns we are working with that are NOT
			// blank. We will then use that data to figure out
			// how many columns we should be using in our query.
			<cfset MY_RESULT.RowIndex = 0>

 			<cfloop condition="MY_RESULT.RowIndex LESS THAN MY_RESULT.Sheet.GetPhysicalNumberOfRows()">
 			
				// Get a reference to the current row.
				<cfset MY_RESULT.Row = MY_RESULT.Sheet.GetRow(JavaCast( "int", MY_RESULT.RowIndex ))>
				
				// Get the number of physical cells in this row. While I think that
				// this can possibly change from row to row, for the purposes of
				// simplicity, I am going to assume that all rows are uniform and
				// that this row is a model of how the rest of the data will be
				// displayed.
				<!--- NB_COLONNES au lieu de MY_RESULT.Row.GetPhysicalNumberOfCells() pour voir les cellules vides --->
				<cfset MY_RESULT.ColumnCount = NB_COLONNES> 
				
				
				// Check to see if the query variable we have it actually a query.
				// If we have not done anything to it yet, then it should still
				// just be a string value (Yahoo for dynamic typing!!!). If that
				// is the case, then let's use this first data row to set up the
				// query object.
				<cfif NOT IsQuery( MY_RESULT.SheetData.Query )>
					
					// Create an empty query. Doing it this way creates a query
					// with neither column nor row values.
					<cfset MY_RESULT.SheetData.Query = QueryNew( "" )>
					
					// Now that we have an empty query, we are going to loop over
					// the cells COUNT for this data row and for each cell, we are
					// going to create a query column of type VARCHAR. I understand
					// that cells are going to have different data types, but I am
					// chosing to store everything as a string to make it easier.
					<cfset MY_RESULT.ColumnIndex = 0>
		 			<cfloop condition="MY_RESULT.ColumnIndex LT MY_RESULT.ColumnCount">
				
						// Add the column. Notice that the name of the column is
						// the text "column" plus the column index. I am starting
						// my column indexes at ONE rather than ZERO to get it back
						// into a more ColdFusion standard notation.
						<cfset temp = QueryAddColumn (MY_RESULT.SheetData.Query,"column#(MY_RESULT.ColumnIndex + 1)#","VarChar",ArrayNew( 1 ))>
						
						<cfif #HasHeaderRow# >
							// Try to get a header column name (it might throw an error).
							<cftry>
								<cfset temp2 = ArrayAppend(MY_RESULT.SheetData.ColumnNames,	MY_RESULT.Row.GetCell(JavaCast( "int", MY_RESULT.ColumnIndex )).GetStringCellValue())> 
								<cfcatch type="any" >
									<cfset temp3 = ArrayAppend(MY_RESULT.SheetData.ColumnNames,"")> 
								</cfcatch>
							</cftry>
						</cfif>
						
						<cfset MY_RESULT.ColumnIndex = (MY_RESULT.ColumnIndex + 1)>
						
					</cfloop>
					
					// Set the default min and max column count based on this first row.
					<cfset MY_RESULT.SheetData.MinColumnCount = MY_RESULT.ColumnCount>
					<cfset MY_RESULT.SheetData.MaxColumnCount = MY_RESULT.ColumnCount>
					
				</cfif>
					
				// ASSERT: Whether we are on our first Excel data row or
				// our Nth data row, at this point, we have a ColdFusion
				// query object that has the proper columns defined.

				// Update the running min column count.
				<cfset MY_RESULT.SheetData.MinColumnCount = Min(MY_RESULT.SheetData.MinColumnCount,	MY_RESULT.ColumnCount)>
 
				// Update the running max column count.
				<cfset MY_RESULT.SheetData.MaxColumnCount = Max(MY_RESULT.SheetData.MaxColumnCount,	MY_RESULT.ColumnCount)>
 
				// Add a row to the query so that we can store this row's
				// data values.
				<cfset temp4 = QueryAddRow( MY_RESULT.SheetData.Query )>
				
				// Loop over the cells in this row to find values.
				<cfset MY_RESULT.ColumnIndex = 0>
		 		<cfloop condition="MY_RESULT.ColumnIndex LT MY_RESULT.ColumnCount">

					// When getting the value of a cell, it is important to know
					// what type of cell value we are dealing with. If you try
					// to grab the wrong value type, an error might be thrown.
					// For that reason, we must check to see what type of cell
					// we are working with. These are the cell types and they
					// are constants of the cell object itself:
					// 0 - CELL_TYPE_NUMERIC
					// 1 - CELL_TYPE_STRING
					// 2 - CELL_TYPE_FORMULA
					// 3 - CELL_TYPE_BLANK
					// 4 - CELL_TYPE_BOOLEAN
					// 5 - CELL_TYPE_ERROR
					
					<cftry>
						// Get the cell from the row object.
						<cfset MY_RESULT.Cell = MY_RESULT.Row.GetCell(JavaCast( "int", MY_RESULT.ColumnIndex ))>
						// Get the type of data in this cell.
						<cfset MY_RESULT.CellType = MY_RESULT.Cell.GetCellType()>

						// Get teh value of the cell based on the data type. The thing
						// to worry about here is cell forumlas and cell dates. Formulas
						// can be strange and dates are stored as numeric types. For
						// this demo, I am not going to worry about that at all. I will
						// just grab dates as floats and formulas I will try to grab as
						// numeric values.
						<cfif MY_RESULT.CellType EQ MY_RESULT.Cell.CELL_TYPE_NUMERIC >
							
							// Get numeric cell data. This could be a standard number,
							// could also be a date value. I am going to leave it up to
							// the calling program to decide.
							<cfset MY_RESULT.CellValue = MY_RESULT.Cell.GetNumericCellValue()>
						<cfelse>
							<cfif MY_RESULT.CellType EQ MY_RESULT.Cell.CELL_TYPE_STRING >
									
								<cfset MY_RESULT.CellValue = MY_RESULT.Cell.GetStringCellValue()>
							
							<cfelse>
								<cfif MY_RESULT.CellType EQ MY_RESULT.Cell.CELL_TYPE_FORMULA >
									<cftry>
										<cfset MY_RESULT.CellValue = MY_RESULT.Cell.GetNumericCellValue()>
										<cfcatch type="any">
											<cftry>
												<cfset MY_RESULT.CellValue = MY_RESULT.Cell.GetStringCellValue()>
												<cfcatch type="any">
													<cfset MY_RESULT.CellValue = "">
												</cfcatch>
											</cftry>
										</cfcatch>
									</cftry>
								<cfelse>
									<cfif MY_RESULT.CellType EQ MY_RESULT.Cell.CELL_TYPE_BLANK >
										
										<cfset MY_RESULT.CellValue = "">
										
									<cfelse>
										<cfif MY_RESULT.CellType EQ MY_RESULT.Cell.CELL_TYPE_BOOLEAN >
										
											<cfset MY_RESULT.CellValue = MY_RESULT.Cell.GetBooleanCellValue()>
											
										<cfelse>
											<cfset MY_RESULT.CellValue = "">
										</cfif>
									</cfif>
								</cfif>
							</cfif>
						</cfif>
						
						<cfif MY_RESULT.RowIndex GT 0 >
							<cfset MY_RESULT.ArrCell[myIndex] = MY_RESULT.CellValue >
							<cfset myIndex = myIndex + 1>
						</cfif> 
					
						<cfcatch type="any"> <!--- pour les cellules vides --->
							<cfset MY_RESULT.ArrCell[myIndex] = "" >
							<cfset myIndex = myIndex + 1>
						</cfcatch>
					</cftry>
					
					// ASSERT: At this point, we either got the cell value out of the
					// Excel data cell or we have thrown an error or didn't get a
					// matching type and just have the empty string by default.
					// No matter what, the object LOCAL.CellValue is defined and
					// has some sort of SIMPLE ColdFusion value in it.
 
 
					// Now that we have a value, store it as a string in the ColdFusion
					// query object. Remember again that my query names are ONE based
					// for ColdFusion standards. That is why I am adding 1 to the
					// cell index.
					<cfset MY_RESULT.SheetData.Query[ "column#(MY_RESULT.ColumnIndex + 1)#" ][ MY_RESULT.SheetData.Query.RecordCount ] = JavaCast( "string", MY_RESULT.CellValue )>	
								
					<cfset MY_RESULT.ColumnIndex = (MY_RESULT.ColumnIndex + 1)>
					
				</cfloop>			
					
				<cfif #HasHeaderRow# AND MY_RESULT.SheetData.Query.RecordCount>
				
					// Delete the first row which is the header row.
					<cfset temp5= MY_RESULT.SheetData.Query.RemoveRows(	JavaCast( "int", 0 ),JavaCast( "int", 1 ))>
				
				</cfif>
			
				<cfset MY_RESULT.RowIndex = (MY_RESULT.RowIndex + 1)>
			
			</cfloop>
			
			// Return the sheet object that contains all the Excel data.								
			<cfreturn MY_RESULT >
		
	</cffunction>
 
</cfcomponent>