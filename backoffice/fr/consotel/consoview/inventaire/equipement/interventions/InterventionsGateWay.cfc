<cfcomponent output="false">
	<cfproperty name="idGroupe" type="numeric" default="0">
	<cfproperty name="idGroupeLigne" type="numeric" default="0">
	<cfproperty name="typePerimetre" type="string" default="Groupe">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupe = session.perimetre.ID_GROUPE;
		variables.idGroupeLigne = session.perimetre.ID_PERIMETRE;
		variables.typePerimetre = session.perimetre.TYPE_PERIMETRE;
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getidGroupe" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupe>
	</cffunction>
	
	<cffunction name="getidGroupeLigne" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeLigne>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.typePerimetre>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : retourne un objet de type Interventions
		
		param in 
			- id l'identifiant de l'intervention
		param out 
			- un objet de type fr.consotel.consoview.inventaire.equipement.interventions.Interventions	
	--->
	<cffunction name="get" output="false" access="remote">
		<cfargument name="id" required="true"/>
 		<cfreturn createObject("component", "Interventions").init(arguments.id)>
	</cffunction>

	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : enregistre les propriétés d'un objet de type fr.consotel.consoview.inventaire.equipement.interventions.Interventions
		
		param in 
			- l'objet à enregistrer
		param out 
			- l'id de l'objet enregistré
	--->
	<cffunction name="save" output="false" access="remote" returntype="numeric">
		<cfargument name="obj" type="Interventions" required="true" />
 		<cfreturn obj.save() />
	</cffunction>

	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : efface l'enregistrement de type fr.consotel.consoview.inventaire.equipement.interventions.Interventions
		
		param in 
			- l'objet à effacé
		param out 
			- l'id de l'objet éffacé
	--->
	<cffunction name="delete" output="false" access="remote">
		<cfargument name="id" required="true" />
		<cfset var obj = get(arguments.id)>
		<cfset obj.delete()>
	</cffunction>


	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : retourne un tableau de type fr.consotel.consoview.inventaire.equipement.interventions.Interventions[]
		
		param out 
			- un tableau d'objets de type fr.consotel.consoview.inventaire.equipement.interventions.Interventions
	--->
	<cffunction name="getAll" output="false" access="remote" returntype="fr.consotel.consoview.inventaire.equipement.interventions.Interventions[]">
		<cfset var qRead="">
		<cfset var obj="">
		<cfset var ret=arrayNew(1)>

		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select IDINTERVENTIONS
			from OFFRE.INTERVENTIONS
		</cfquery>

		<cfloop query="qRead">
		<cfscript>
			obj = createObject("component", "Interventions").init(qRead.IDINTERVENTIONS);
			ArrayAppend(ret, obj);
		</cfscript>
		</cfloop>
		<cfreturn ret>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : retourne un tableau de type fr.consotel.consoview.inventaire.equipement.interventions.Interventions[] pour un equipement donné

		param in
			- l'identifiant de l'equipement
		param out 
			- un tableau d'objets de type fr.consotel.consoview.inventaire.equipement.interventions.Interventions
	--->
	<cffunction name="getFromEquipement" output="false" access="remote" returntype="any">
		<cfargument name="idEquipement" required="true" />
		<cfset var qRead="">
		<cfset var obj="">
		<cfset var ret=arrayNew(1)>

		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select IDINTERVENTIONS
			from OFFRE.INTERVENTIONS where IDEQUIPEMENT = #idEquipement# order by DATE_EVENEMENT desc
		</cfquery>

		<cfloop query="qRead">
		<cfscript>
			obj = createObject("component", "Interventions").init(qRead.IDINTERVENTIONS);
			ArrayAppend(ret, obj);
		</cfscript>
		</cfloop>
		<cfreturn ret>
	</cffunction>

	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : retourne l'enssemble des intervention de la base sous forme de requete

		param in
			- la liste des parametres de sortie de la requete
		param out 
			- la requete
	--->
	<cffunction name="getAllAsQuery" output="false" access="remote" returntype="query">
		<cfargument name="fieldlist" default="*" hint="List of columns to be returned in the query.">
		<cfset var qRead="">

		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select #arguments.fieldList#
			from OFFRE.INTERVENTIONS
		</cfquery>

		<cfreturn qRead>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : retourne l'enssemble des interventions d'un equipement sous forme de requete

		param in
			- l'identifiant de l'equipement
			- la liste des parametres de sortie de la requete (xxx,xxx,xxx,xxx,xxxx,xxxx)
		param out 
			- la requete
	--->
	<cffunction name="getFromEquipementAsQuery" output="false" access="remote" returntype="query">
		<cfargument name="idEquipement" required="true"/>
		<cfargument name="fieldlist" default="*" hint="List of columns to be returned in the query.">
		<cfset var qRead="">

		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select #arguments.fieldList#
			from OFFRE.INTERVENTIONS where IDEQUIPEMENT = #idEquipement# order by DATE_EVENEMENT desc
		</cfquery>

		<cfreturn qRead>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : retourne la liste des types d'interventions

		param in			
			- la liste des parametres de sortie de la requete (xxx,xxx,xxx,xxx,xxxx,xxxx)
		param out 
			- la requete
	--->	
	<cffunction name="getTypes" output="false" access="remote" returntype="query">
		<cfargument name="fieldlist" default="*" hint="List of columns to be returned in the query.">

		<cfset var qRead="">

		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select #arguments.fieldList#
			from OFFRE.TYPE_INTERVENTION
		</cfquery>

		<cfreturn qRead>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : retourne la liste des motifs d'interventions

		param in			
			- la liste des parametres de sortie de la requete (xxx,xxx,xxx,xxx,xxxx,xxxx)
		param out 
			- la requete
	--->	
	<cffunction name="getMotifs" output="false" access="remote" returntype="query">
		<cfargument name="fieldlist" default="*" hint="List of columns to be returned in the query.">
		<cfset var qRead="">
		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select #arguments.fieldList#
			from OFFRE.MOTIF_INTERVENTION
		</cfquery>

		<cfreturn qRead>
	</cffunction>

	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : retourne la liste des fournisseur du groupe et les fournisseurs globaux

		param in			
			- la liste des parametres de retour de la requete
		param out 
			- la requete
	--->	
	<cffunction name="getFournisseurs" output="false" access="remote" returntype="query">		
		<cfargument name="fieldlist" default="*" hint="List of columns to be returned in the query.">
		<cfset var qRead="">
		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select #arguments.fieldList#			       			   
			from   fournisseur
			where  idgroupe_client = #session.perimetre.id_groupe# 
			or     idgroupe_client = #session.perimetre.id_perimetre# 
			or     idgroupe_client is null			 
		</cfquery>

		<cfreturn qRead>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/16/2007
		
		Description : recherche des contrats
	
	--->
	<cffunction name="rechercherInterventions" access="remote" output="false" returntype="array">	
		<cfargument name="chaine" type="string" required="true">	
		<!--- 
			--------------------------------------------------------------------------
		    -- recherche des contrats des Interventions
		   	--------------------------------------------------------------------------
		        pkg_cv_equipement.search_interventions(p_idracine => :p_idracine,
                                    p_idgroupe => :p_idgroupe,
                                    p_chaine => :p_chaine,
                                    p_retour => :p_retour);


		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_equipement.search_intervention">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#getidGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_idgroupe" value="#chaine#"/>
			<cfprocresult name="qInter"/>        
		</cfstoredproc> --->	
		
		<cfquery name="qInter" datasource="#SESSION.OFFREDSN#" >
		SELECT e.idequipement, e.libelle_eq AS materiel, ti.type_intervention AS TYPE, i.reference_intervention AS REFERENCE,
                      i.libelle_intervention AS libelle, i.date_debut, i.date_fin, floor(i.date_fin-i.date_debut) duree_en_jour, fo2.nom_fournisseur AS prestataire, 
                    i.cout_intervention AS montant
                      FROM interventions i, motif_intervention mi, type_intervention ti, equipement e, 
                    equipement_client ec, equipement_fournis ef, type_equipement te, categorie_equipement ce,
                  fournisseur fo, fournisseur fo2
               WHERE  i.idmotif_intervention=mi.idmotif_intervention
                  AND i.idfournisseur=fo2.idfournisseur
                         AND i.idtype_intervention=ti.idtype_intervention
                         AND i.idequipement =e.idequipement (+) 
                         AND e.idequipement_client=ec.idequipement_client (+) 
                         AND ec.idequipement_fournisseur=ef.idequipement_fournis (+) 
                  AND ef.idrevendeur=fo.idfournisseur (+) 
                         AND ef.idtype_equipement=te.idtype_equipement (+)
                         AND te.idcategorie_equipement=ce.idcategorie_equipement (+) 
                         AND ec.idracine= #getidGroupe()#
                  AND (
                      lower(TRIM(ti.type_intervention)) LIKE '%'||'#chaine#'||'%'
                     OR lower(TRIM(e.libelle_eq)) LIKE '%'||''||'%'
                     OR lower(TRIM(i.reference_intervention)) LIKE '%'||'#chaine#'||'%'
                     OR lower(TRIM(i.libelle_intervention)) LIKE '%'||'#chaine#'||'%'
                     OR lower(TRIM(to_char(i.date_debut,'dd/mm/yyyy'))) LIKE '%'||'#chaine#'||'%'
                     OR lower(TRIM(to_char(i.date_fin,'dd/mm/yyyy'))) LIKE '%'||'#chaine#'||'%'
                     OR lower(TRIM(fo2.nom_fournisseur)) LIKE '%'||'#chaine#'||'%'
                     )
		
		</cfquery>			
		<cfquery name="qTotal" dbtype="query">
			select sum(MONTANT) as MONTANT_TOTAL, 'TOTAL' as TOTAL
			from qInter
		</cfquery>
		<cfset data = arrayNew(1)>
		<cfset arrayAppend(data,qInter)>
		<cfset arrayAppend(data,qTotal)>			
		<cfreturn data>
	</cffunction>
	
	

</cfcomponent>