<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.equipement.interventions.Interventions">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="IDINTERVENTIONS" type="numeric" default="0">
	<cfproperty name="IDEQUIPEMENT" type="numeric" default="0">
	<cfproperty name="IDMOTIF_INTERVENTION" type="numeric" default="0">
	<cfproperty name="IDTYPE_INTERVENTION" type="numeric" default="0">
	<cfproperty name="IDFOURNISSEUR" type="numeric" default="0">
	<cfproperty name="DATE_EVENEMENT" type="date" default="">
	<cfproperty name="LIBELLE_INTERVENTION" type="string" default="">
	<cfproperty name="REFERENCE_INTERVENTION" type="string" default="">
	<cfproperty name="CODE_INTERNE_INTERVENTION" type="string" default="">
	<cfproperty name="DATE_DEBUT" type="date" default="">
	<cfproperty name="DATE_FIN" type="date" default="">
	<cfproperty name="DUREE_INTERVENTION" type="numeric" default="0">
	<cfproperty name="COUT_INTERVENTION" type="numeric" default="0">
	<cfproperty name="COMMENTAIRE_INTERVENTION" type="string" default="">

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.IDINTERVENTIONS = 0;
		variables.IDEQUIPEMENT = 0;
		variables.IDMOTIF_INTERVENTION = 0;
		variables.IDTYPE_INTERVENTION = 0;
		variables.IDFOURNISSEUR = 0;
		variables.DATE_EVENEMENT = "";
		variables.LIBELLE_INTERVENTION = "";
		variables.REFERENCE_INTERVENTION = "";
		variables.CODE_INTERNE_INTERVENTION = "";
		variables.DATE_DEBUT = "";
		variables.DATE_FIN = "";
		variables.DUREE_INTERVENTION = 0;
		variables.COUT_INTERVENTION = 0;
		variables.COMMENTAIRE_INTERVENTION = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="Interventions">
		<cfargument name="id" required="false">
		<cfscript>
			if( structKeyExists(arguments, "id") )
			{
				load(arguments.id);
			}
			return this;
		</cfscript>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : GETTER et SETTER
	
	--->
	<cffunction name="getIDINTERVENTIONS" output="false" access="public" returntype="any">
		<cfreturn variables.IDINTERVENTIONS>
	</cffunction>

	<cffunction name="setIDINTERVENTIONS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDINTERVENTIONS = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDEQUIPEMENT" output="false" access="public" returntype="any">
		<cfreturn variables.IDEQUIPEMENT>
	</cffunction>

	<cffunction name="setIDEQUIPEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDEQUIPEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDMOTIF_INTERVENTION" output="false" access="public" returntype="any">
		<cfreturn variables.IDMOTIF_INTERVENTION>
	</cffunction>

	<cffunction name="setIDMOTIF_INTERVENTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDMOTIF_INTERVENTION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDTYPE_INTERVENTION" output="false" access="public" returntype="any">
		<cfreturn variables.IDTYPE_INTERVENTION>
	</cffunction>

	<cffunction name="setIDTYPE_INTERVENTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDTYPE_INTERVENTION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDFOURNISSEUR" output="false" access="public" returntype="any">
		<cfreturn variables.IDFOURNISSEUR>
	</cffunction>

	<cffunction name="setIDFOURNISSEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDFOURNISSEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_EVENEMENT" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_EVENEMENT>
	</cffunction>

	<cffunction name="setDATE_EVENEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.DATE_EVENEMENT = arguments.val>
		<!--- <cfif (IsString(arguments.val)) OR (arguments.val EQ "")> 
		<cfif arguments.val EQ "">
			<cfset variables.DATE_EVENEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif> --->
	</cffunction>

	<cffunction name="getLIBELLE_INTERVENTION" output="false" access="public" returntype="any">
		<cfreturn variables.LIBELLE_INTERVENTION>
	</cffunction>

	<cffunction name="setLIBELLE_INTERVENTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.LIBELLE_INTERVENTION = arguments.val>
	</cffunction>

	<cffunction name="getREFERENCE_INTERVENTION" output="false" access="public" returntype="any">
		<cfreturn variables.REFERENCE_INTERVENTION>
	</cffunction>

	<cffunction name="setREFERENCE_INTERVENTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.REFERENCE_INTERVENTION = arguments.val>
	</cffunction>

	<cffunction name="getCODE_INTERNE_INTERVENTION" output="false" access="public" returntype="any">
		<cfreturn variables.CODE_INTERNE_INTERVENTION>
	</cffunction>

	<cffunction name="setCODE_INTERNE_INTERVENTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.CODE_INTERNE_INTERVENTION = arguments.val>
	</cffunction>

	<cffunction name="getDATE_DEBUT" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_DEBUT>
	</cffunction>

	<cffunction name="setDATE_DEBUT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.DATE_DEBUT = arguments.val>		
		<!--- <cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DATE_DEBUT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif> --->
	</cffunction>

	<cffunction name="getDATE_FIN" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_FIN>
	</cffunction>

	<cffunction name="setDATE_FIN" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">		 
		<cfset variables.DATE_FIN = arguments.val>
		<!--- <cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
		<cfif (arguments.val EQ "")>
			<cfset variables.DATE_FIN = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif> --->
	</cffunction>

	<cffunction name="getDUREE_INTERVENTION" output="false" access="public" returntype="any">
		<cfreturn variables.DUREE_INTERVENTION>
	</cffunction>

	<cffunction name="setDUREE_INTERVENTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DUREE_INTERVENTION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getCOUT_INTERVENTION" output="false" access="public" returntype="any">
		<cfreturn variables.COUT_INTERVENTION>
	</cffunction>

	<cffunction name="setCOUT_INTERVENTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.COUT_INTERVENTION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getCOMMENTAIRE_INTERVENTION" output="false" access="public" returntype="any">
		<cfreturn variables.COMMENTAIRE_INTERVENTION>
	</cffunction>

	<cffunction name="setCOMMENTAIRE_INTERVENTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.COMMENTAIRE_INTERVENTION = arguments.val>
	</cffunction>


	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : Enregistre l'intervention dans la base,
					 si l'identifiant de l'intervention est égal à 0 on crée un nouvelle enregistrement
					 sinon on met à jour l'enregistremnt référencé par l'identifiant.
		
		param out:
			- l'identifiant de l'enregistrement créé ou mis à jour
	--->
	<cffunction name="save" output="false" access="public" returntype="numeric">
		<cfscript>
			if(getIDINTERVENTIONS() eq 0)
			{
				return create();
			}else{
				return update();
			}
		</cfscript>
	</cffunction>


	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : charge les données d'une intervention et affecte les propriétés de l'objet.
		
		param in :		
			- l'identifiant de l'intervention
	
	--->
	<cffunction name="load" output="false" access="public" returntype="void">
		<cfargument name="id" required="true" >
		<cfset var qRead="">
		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select 	IDINTERVENTIONS, IDEQUIPEMENT, IDMOTIF_INTERVENTION, IDTYPE_INTERVENTION, IDFOURNISSEUR, DATE_EVENEMENT, 
					LIBELLE_INTERVENTION, REFERENCE_INTERVENTION, CODE_INTERNE_INTERVENTION, DATE_DEBUT, DATE_FIN, 
					DUREE_INTERVENTION, COUT_INTERVENTION, COMMENTAIRE_INTERVENTION
			from OFFRE.INTERVENTIONS
			where IDINTERVENTIONS = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.id#" />
		</cfquery>

		<cfscript>
			setIDINTERVENTIONS(qRead.IDINTERVENTIONS);
			setIDEQUIPEMENT(qRead.IDEQUIPEMENT);
			setIDMOTIF_INTERVENTION(qRead.IDMOTIF_INTERVENTION);
			setIDTYPE_INTERVENTION(qRead.IDTYPE_INTERVENTION);
			setIDFOURNISSEUR(qRead.IDFOURNISSEUR);
			setDATE_EVENEMENT(qRead.DATE_EVENEMENT);
			setLIBELLE_INTERVENTION(qRead.LIBELLE_INTERVENTION);
			setREFERENCE_INTERVENTION(qRead.REFERENCE_INTERVENTION);
			setCODE_INTERNE_INTERVENTION(qRead.CODE_INTERNE_INTERVENTION);
			setDATE_DEBUT(qRead.DATE_DEBUT);
			setDATE_FIN(qRead.DATE_FIN);
			setDUREE_INTERVENTION(qRead.DUREE_INTERVENTION);
			setCOUT_INTERVENTION(qRead.COUT_INTERVENTION);
			setCOMMENTAIRE_INTERVENTION(qRead.COMMENTAIRE_INTERVENTION);
		</cfscript>
	</cffunction>



	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : Crée un enregistrement dans la table offre.interventions avec les propriétés de l'objet.
		
		param out:
			- l'identifiant qui référence l'enregistrement venant d'être créé
	--->
	<cffunction name="create" output="false" access="private" returntype="numeric">
		<cfset var qCreate="">
		<cfset var qGetID="">
		
		 
		
		<cfset var local1=getIDEQUIPEMENT()>
		<cfset var local2=getIDMOTIF_INTERVENTION()>
		<cfset var local3=getIDTYPE_INTERVENTION()>
		<cfset var local4=getIDFOURNISSEUR()>
		<cfset var local5=getDATE_EVENEMENT()>
		<cfset var local6=getLIBELLE_INTERVENTION()>
		<cfset var local7=getREFERENCE_INTERVENTION()>
		<cfset var local8=getCODE_INTERNE_INTERVENTION()>
		<cfset var local9=getDATE_DEBUT()>
		<cfset var local10=getDATE_FIN()>
		<cfset var local11=getDUREE_INTERVENTION()>
		<cfset var local12=getCOUT_INTERVENTION()>
		<cfset var local13=getCOMMENTAIRE_INTERVENTION()>

		<cftransaction isolation="read_committed">
			<cfquery name="qCreate" datasource="#SESSION.OFFREDSN#">
				insert into OFFRE.INTERVENTIONS(IDEQUIPEMENT, IDMOTIF_INTERVENTION, IDTYPE_INTERVENTION, IDFOURNISSEUR, DATE_EVENEMENT, LIBELLE_INTERVENTION, REFERENCE_INTERVENTION, CODE_INTERNE_INTERVENTION, DATE_DEBUT, DATE_FIN, DUREE_INTERVENTION, COUT_INTERVENTION, COMMENTAIRE_INTERVENTION)
				values (
					<cfqueryparam value="#local1#" cfsqltype="CF_SQL_INTEGER" null="#iif((local1 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local2#" cfsqltype="CF_SQL_INTEGER" null="#iif((local2 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local3#" cfsqltype="CF_SQL_INTEGER" null="#iif((local3 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local4#" cfsqltype="CF_SQL_INTEGER" null="#iif((local4 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local5#" cfsqltype="CF_SQL_DATE" null="#iif((local5 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local6#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local7#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local8#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local9#" cfsqltype="CF_SQL_DATE" null="#iif((local9 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local10#" cfsqltype="CF_SQL_DATE" null="#iif((local10 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local11#" cfsqltype="CF_SQL_FLOAT" null="#iif((local11 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local12#" cfsqltype="CF_SQL_FLOAT" null="#iif((local12 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local13#" cfsqltype="CF_SQL_VARCHAR" />
				)
			</cfquery> 

		<!--- If your server has a better way to get the ID that is more reliable, use that instead---> 
			<cfquery name="qGetID" datasource="#SESSION.OFFREDSN#">
				select IDINTERVENTIONS
				from OFFRE.INTERVENTIONS
				where IDEQUIPEMENT = <cfqueryparam value="#local1#" cfsqltype="CF_SQL_DECIMAL" null="#iif((local1 eq ""), de("yes"), de("no"))#" />
				  and IDMOTIF_INTERVENTION = <cfqueryparam value="#local2#" cfsqltype="CF_SQL_INTEGER" null="#iif((local2 eq ""), de("yes"), de("no"))#" />
				  and IDTYPE_INTERVENTION = <cfqueryparam value="#local3#" cfsqltype="CF_SQL_INTEGER" null="#iif((local3 eq ""), de("yes"), de("no"))#" />
				  and IDFOURNISSEUR = <cfqueryparam value="#local4#" cfsqltype="CF_SQL_INTEGER" null="#iif((local4 eq ""), de("yes"), de("no"))#" />
				  and DATE_EVENEMENT = <cfqueryparam value="#local5#" cfsqltype="CF_SQL_DATE" null="#iif((local5 eq ""), de("yes"), de("no"))#" />
				  and LIBELLE_INTERVENTION = <cfqueryparam value="#local6#" cfsqltype="CF_SQL_VARCHAR" />
				  and REFERENCE_INTERVENTION = <cfqueryparam value="#local7#" cfsqltype="CF_SQL_VARCHAR" />
				  and CODE_INTERNE_INTERVENTION = <cfqueryparam value="#local8#" cfsqltype="CF_SQL_VARCHAR" />
				  and DATE_DEBUT = <cfqueryparam value="#local9#" cfsqltype="CF_SQL_DATE" null="#iif((local9 eq ""), de("yes"), de("no"))#" />
				  and DATE_FIN = <cfqueryparam value="#local10#" cfsqltype="CF_SQL_DATE" null="#iif((local10 eq ""), de("yes"), de("no"))#" />
				  and DUREE_INTERVENTION = <cfqueryparam value="#local11#" cfsqltype="CF_SQL_FLOAT" null="#iif((local11 eq ""), de("yes"), de("no"))#" />
				  and COUT_INTERVENTION = <cfqueryparam value="#local12#" cfsqltype="CF_SQL_FLOAT" null="#iif((local12 eq ""), de("yes"), de("no"))#" />
				  and COMMENTAIRE_INTERVENTION = <cfqueryparam value="#local13#" cfsqltype="CF_SQL_VARCHAR" />
				order by IDINTERVENTIONS desc
			</cfquery>			 
		</cftransaction>

		<cfset variables.IDINTERVENTIONS = qGetID.IDINTERVENTIONS>
		<cfreturn 1>
	</cffunction>


	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : Met à jour un enregistrement dans la table offre.interventions avec les propriétés de l'objet.
		
		param out:
			- l'identifiant qui référence l'enregistrement qui vient d'être mis à jour
	--->
	<cffunction name="update" output="false" access="private" returntype="numeric">
		<cfset var qUpdate="">

		<cfquery name="qUpdate" datasource="#SESSION.OFFREDSN#" result="status">
			update OFFRE.INTERVENTIONS
			set IDEQUIPEMENT = <cfqueryparam value="#getIDEQUIPEMENT()#" cfsqltype="CF_SQL_INTEGER" null="#iif((getIDEQUIPEMENT() eq ""), de("yes"), de("no"))#" />,
				IDMOTIF_INTERVENTION = <cfqueryparam value="#getIDMOTIF_INTERVENTION()#" cfsqltype="CF_SQL_INTEGER" null="#iif((getIDMOTIF_INTERVENTION() eq ""), de("yes"), de("no"))#" />,
				IDTYPE_INTERVENTION = <cfqueryparam value="#getIDTYPE_INTERVENTION()#" cfsqltype="CF_SQL_INTEGER" null="#iif((getIDTYPE_INTERVENTION() eq ""), de("yes"), de("no"))#" />,
				IDFOURNISSEUR = <cfqueryparam value="#getIDFOURNISSEUR()#" cfsqltype="CF_SQL_INTEGER" null="#iif((getIDFOURNISSEUR() eq ""), de("yes"), de("no"))#" />,
				DATE_EVENEMENT = <cfqueryparam value="#getDATE_EVENEMENT()#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((getDATE_EVENEMENT() eq ""), de("yes"), de("no"))#" />,
				LIBELLE_INTERVENTION = <cfqueryparam value="#getLIBELLE_INTERVENTION()#" cfsqltype="CF_SQL_VARCHAR" />,
				REFERENCE_INTERVENTION = <cfqueryparam value="#getREFERENCE_INTERVENTION()#" cfsqltype="CF_SQL_VARCHAR" />,
				CODE_INTERNE_INTERVENTION = <cfqueryparam value="#getCODE_INTERNE_INTERVENTION()#" cfsqltype="CF_SQL_VARCHAR" />,
				DATE_DEBUT = <cfqueryparam value="#getDATE_DEBUT()#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((getDATE_DEBUT() eq ""), de("yes"), de("no"))#" />,
				DATE_FIN = <cfqueryparam value="#getDATE_FIN()#" cfsqltype="CF_SQL_TIMESTAMP" null="#iif((getDATE_FIN() eq ""), de("yes"), de("no"))#" />,
				DUREE_INTERVENTION = <cfqueryparam value="#getDUREE_INTERVENTION()#" cfsqltype="CF_SQL_FLOAT" null="#iif((getDUREE_INTERVENTION() eq ""), de("yes"), de("no"))#" />,
				COUT_INTERVENTION = <cfqueryparam value="#getCOUT_INTERVENTION()#" cfsqltype="CF_SQL_FLOAT" null="#iif((getCOUT_INTERVENTION() eq ""), de("yes"), de("no"))#" />,
				COMMENTAIRE_INTERVENTION = <cfqueryparam value="#getCOMMENTAIRE_INTERVENTION()#" cfsqltype="CF_SQL_VARCHAR" />
			where IDINTERVENTIONS = <cfqueryparam value="#getIDINTERVENTIONS()#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
		<cfreturn 1>
	</cffunction>


	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : Efface un enregistrement dans la table offre.interventions suivant les parametre de l'objet
	
	--->
	<cffunction name="delete" output="false" access="public" returntype="void">
		<cfset var qDelete="">

		<cfquery name="qDelete" datasource="#SESSION.OFFREDSN#" result="status">
			delete
			from OFFRE.INTERVENTIONS
			where IDINTERVENTIONS = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getIDINTERVENTIONS()#" />
		</cfquery>
	</cffunction>
	
	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="false" default="">		
		<cfif len(oldDateString) neq 0>
			<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
			<cfreturn newDateString> 
		<cfelse>
			<cfreturn oldDateString> 
		</cfif>		
	</cffunction>

</cfcomponent>