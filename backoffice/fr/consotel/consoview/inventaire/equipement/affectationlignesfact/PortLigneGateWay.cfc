<cfcomponent output="false">
	
	<cffunction name="save" output="false" access="public" returntype="numeric">
		<cfargument name="obj" required="true" />
 		<cfreturn obj.save() />
	</cffunction>
	
	<cffunction name="saveList" output="false" access="public" returntype="numeric">
		<cfargument name="tabPortLignes" type="fr.consotel.consoview.inventaire.equipement.affectationlignesfact.PortLigne[]" required="true" />
		<cfset lg = arrayLen(tabPortLignes)>
		<cfset r = 0>
		<cfset count = 0>
		<cfloop index="i" from="1" to="#lg#">
			<cfset r =  tabPortLignes[i].save()>;
			<cfif r gt 0 >
				<cfset count = count + 1 />
			</cfif>
		</cfloop>
 		<cfreturn count/>
	</cffunction>
	
	<cffunction name="deleteList" output="false" access="public" returntype="numeric">
		<cfargument name="tabPortLignes" type="fr.consotel.consoview.inventaire.equipement.affectationlignesfact.PortLigne[]" required="true"/>
		<cfset lg = arrayLen(tabPortLignes)>
		<cfset r = 0>
		<cfset count = 0>
		<cfloop index="i" from="1" to="#lg#">
			<cfset r = tabPortLignes[i].delete()>
			<cfif r gt 0 >
				<cfset count = count + 1 />
			</cfif>
		</cfloop>
 		<cfreturn count/>
	</cffunction>
	
	
	<cffunction name="delete" output="false" access="public" returntype="numeric">
		<cfargument name="obj" type="PortLigne" required="true"/>
		<cfreturn obj.delete()>
	</cffunction>

	<cffunction name="getListePortsLignesEquipement" output="false" access="public" returntype="fr.consotel.consoview.inventaire.equipement.affectationlignesfact.PortLigne[]">
		<cfargument name="idEquipement" required="true"/>

		<cfset ret = ArrayNew(1)>
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.LISTE_PORTS_EQAF">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#">			
			<cfprocresult name="qRead">
		</cfstoredproc>

		<cfloop query="qRead">
		<cfscript>
			obj = createObject("component", "PortLigne");
			obj.setIDPORT_EQUIPEMENT(qRead.IDPORT_EQUIPEMENT);
			obj.setIDEQUIPEMENT(qRead.IDEQUIPEMENT);
			obj.setIDSOUS_TETE(qRead.IDSOUS_TETE);
			obj.setSOUS_TETE(qRead.SOUS_TETE);
			obj.setTYPE_CONNECTEUR(qRead.TYPE_CONNECTEUR);
			obj.setTYPE_EQUIPEMENT(qRead.TYPE_EQUIPEMENT);
			obj.setLIBELLE_EQ(qRead.LIBELLE_EQ);
			obj.setLIBELLE_PORT(qRead.LIBELLE_PORT);
			obj.setNUMERO_PORT(qRead.NUMERO_PORT);
			ArrayAppend(ret, obj);
		</cfscript>
		</cfloop>
		<cfreturn ret>
	</cffunction>


</cfcomponent>