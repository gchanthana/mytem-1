<cfcomponent name="PortEquipementGateWay">
	<cffunction name="getListePortEquipement" access="remote" returntype="any">
		<cfargument name="idEquipement" required="true"/>

		<cfset qRead = getListePortEquipementAsQuery(idEquipement) >
		
		
		<cfset ret = ArrayNew(1)> 
		<cfloop query="qRead">
		<cfscript>
			obj = createObject("component", "PortEquipement");
			obj.setIDPORT_EQUIPEMENT(qRead.IDPORT_EQUIPEMENT);
			obj.setIDEQUIPEMENT(qRead.IDEQUIPEMENT);
			obj.setIDTYPE_CONNECTEUR(qRead.IDTYPE_CONNECTEUR);
			obj.setNUMERO_PORT(qRead.NUMERO_PORT);
			obj.setLIBELLE_PORT(qRead.LIBELLE_PORT);
			obj.setUSAGE_PORT(qRead.USAGE_PORT);
			obj.setCODE_INTERNE_PORT(qRead.CODE_INTERNE_PORT);
			obj.setGENRE_CONNECTEUR(qRead.GENRE_CONNECTEUR);
			obj.setREFERENCE_PORT(qRead.REFERENCE_PORT);
			obj.setIDTYPE_EQUIPEMENT(qRead.IDTYPE_EQUIPEMENT);
			obj.setLIBELLE_EQ(qRead.LIBELLE_EQ);
			obj.setTYPE_EQUIPEMENT(qRead.TYPE_EQUIPEMENT);
			obj.setTYPE_CONNECTEUR(qRead.TYPE_CONNECTEUR);		
			ArrayAppend(ret, obj);
		</cfscript>
		</cfloop>
		<cfreturn ret>
	</cffunction>
	
	<cffunction name="getListePortEquipementAsQuery" access="public" returntype="query">
		<cfargument name="idEquipement" required="true"/>

		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.LISTE_PORTS_EQ">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#">			
			<cfprocresult name="qRead">
		</cfstoredproc>
		
		<cfreturn qRead>
	</cffunction>
</cfcomponent>