<!--- =========================================================================
Classe: OperateursUtils
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="OperateursUtils" hint="" >
    <cfset Init()>
	<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="OperateursUtils" hint="Remplace le constructeur de OperateursUtils.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
	<!--- METHODS --->
	<cffunction name="fournirListeOperateursClient" access="public" returntype="query" output="false" hint="fournit le liste de tous les opérateurs du client appartenant au segment.les opérateurs du client sont ceux qui ont facturés au moins une fois un produit appartenant é une ligne de la racine.segment = FIXDATA | MOBILE | TOUT" >
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_grcl_v3.sf_listeop_v2">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idGroupe_maitre" value="#idRacine#"/>		
			<cfprocresult name="qListeOperateurs"/>
		</cfstoredproc>						
		<cfreturn qListeOperateurs>
	</cffunction>
	
	<cffunction name="fournirListeAllOperateurs" access="public" returntype="query" output="false" hint="fournit le liste de tous les opérateurs de la base" >
	</cffunction>
	
	<cffunction name="fournirListeOperateursSegment" access="public" returntype="query" output="false" hint="fournit le liste de tous les opérateurs du segment.segment = FIXDATA | MOBILE | TOUT" >
		<cfargument name="segment" required="true" type="string" default="" displayname="string segment" hint="Initial value for the segmentproperty." />
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset qResult = "">
		<cfswitch expression="#ucase(segment)#">
			
			
			<cfcase value="FIXEDATA">
				<!--- <cfquery name="qResult" datasource="#Session.OFFREDSN#" blockfactor="10">
					SELECT op.operateurid, op.nom as libelle FROM operateur op 
				</cfquery> --->			
			</cfcase>
			<cfcase value="MOBILE">
				
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_EQUIPEMENT.GETFABOP_ACTIF">
					<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idRacine" value="#idRacine#"/>
					<cfprocresult name="qResult">
				</cfstoredproc>
				
			</cfcase>			
			<cfdefaultcase>
				<!--- <cfquery name="qResult" datasource="#Session.OFFREDSN#" blockfactor="10">
					SELECT op.operateurid, op.nom as libelle FROM operateur op 
				</cfquery> --->
			</cfdefaultcase>
			
		</cfswitch>
		
		<cfreturn qResult/>
	</cffunction>	
	<!--- GETTERS --->
	<!--- SETTERS --->
	<!--- INSTANCE METHODS --->		
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance/>
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>