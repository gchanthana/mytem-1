<!--- =========================================================================
Classe: GestionTransporteurs
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GestionTransporteurs" hint="" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GestionTransporteurs" hint="Remplace le constructeur de GestionTransporteurs.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<cffunction name="fournirListeTransporteurs" access="public" returntype="query" output="false" hint="Fournit la liste des transporteurs de la base." >
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.FOURNIRLISTETRANSPORTEURS" blockfactor="100" >
			<cfprocresult name="p_retour">			
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>