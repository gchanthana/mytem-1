<cfcomponent extends="fr.consotel.consoview.util.publicreportservice.PublicReportServiceV2" output="false">
	
	<cffunction name="afficherCommandeEnPDF" access="public" output="false" returntype="any">		
		<cfargument name="idcommande" 		required="true" type="numeric">
		<cfargument name="idtypecommande" 	required="true" type="numeric">
		
			<cfset locale = SESSION.USER.GLOBALIZATION>

			<cfswitch expression="#idtypecommande#">
				 <cfcase value="1083"><!--- NOUVELLE COMMANDE --->
				 	<cfset createRunReportRequest("consoview","public","/consoview/gestion/flotte/BDCMobile-v3/BDCMobile-v3.xdo","AVALIDER","pdf",locale)>
				 </cfcase>
				 <cfdefaultcase>
				  	<cfset createRunReportRequest("consoview","public","/consoview/gestion/flotte/BDCMobile-v3/BDCMobile-v3.xdo","AVALIDER","pdf",locale)>
				 </cfdefaultcase>
			</cfswitch>
			
			<cfset AddRunReportParameter("P_IDCOMMANDE","#idcommande#")>
			<cfset AddRunReportParameter("P_IDGESTIONNAIRE","#Session.USER.CLIENTACCESSID#")>
			<cfset AddRunReportParameter("P_RACINE","#Session.perimetre.RAISON_SOCIALE#")>
			<cfset AddRunReportParameter("P_GESTIONNAIRE","#Session.user.NOM# #Session.user.PRENOM#")>
			<cfset AddRunReportParameter("P_MAIL","#Session.user.EMAIL#")>
			<cfset AddRunReportParameter("P_LANGUE_PAYS","#locale#")>
			<cfset runReport()>
		
	</cffunction>
</cfcomponent>