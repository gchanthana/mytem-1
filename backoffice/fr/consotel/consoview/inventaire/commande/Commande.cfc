<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.commande.Commande">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="IDCOMMANDE" type="numeric" default="0">
	<cfproperty name="IDCONTACT" type="numeric" default="0">
	<cfproperty name="IDREVENDEUR" type="numeric" default="0">
	<cfproperty name="IDOPERATEUR" type="numeric" default="0">
	<cfproperty name="IDTYPE_COMMANDE" type="numeric" default="0">
	<cfproperty name="IDSOCIETE" type="numeric" default="0">
	<cfproperty name="IDCOMPTE_FACTURATION" type="numeric" default="0">
	<cfproperty name="IDSOUS_COMPTE" type="numeric" default="0">
	<cfproperty name="IDRACINE" type="numeric" default="0">
	<cfproperty name="IDLAST_ETAT" type="numeric" default="0">
	<cfproperty name="IDLAST_ACTION" type="numeric" default="0">
	<cfproperty name="IDSITELIVRAISON" type="numeric" default="0">
	<cfproperty name="IDTRANSPORTEUR" type="numeric" default="0">
	<cfproperty name="IDPOOL_GESTIONNAIRE" type="numeric" default="0">
	<cfproperty name="IDGESTIONNAIRE" type="numeric" default="0">
	<cfproperty name="NUMERO_COMMANDE" type="string" default="">
	<cfproperty name="LIBELLE_COMMANDE" type="string" default="">
	<cfproperty name="LIBELLE_POOL" type="string" default="">
	<cfproperty name="REF_CLIENT" type="string" default="">
	<cfproperty name="REF_REVENDEUR" type="string" default="">
	<cfproperty name="LIBELLE_REVENDEUR" type="string" default="">
	<cfproperty name="LIBELLE_OPERATEUR" type="string" default="">
	<cfproperty name="COMMENTAIRES" type="string" default="">
	<cfproperty name="LIBELLE_COMPTE" type="string" default="">
	<cfproperty name="LIBELLE_SOUSCOMPTE" type="string" default="">
	<cfproperty name="LIBELLE_TRANSPORTEUR" type="string" default="">
	<cfproperty name="LIBELLE_SITELIVRAISON" type="string" default="">
	<cfproperty name="LIBELLE_LASTETAT" type="string" default="">
	<cfproperty name="LIBELLE_LASTACTION" type="string" default="">
	<cfproperty name="TYPE_OPERATION" type="string" default="">
	<cfproperty name="PATRONYME_CONTACT" type="string" default="">
	<cfproperty name="CREEE_PAR" type="string" default="">
	<cfproperty name="MODIFIEE_PAR" type="string" default="">
	<cfproperty name="BOOL_ENVOYER_VIAMAIL" type="numeric" default="0">
	<cfproperty name="CREEE_LE" type="date" default="">
	<cfproperty name="MODIFIEE_LE" type="date" default="">
	<cfproperty name="ENVOYER_LE" type="date" default="">
	<cfproperty name="LIVREE_LE" type="date" default="">
	<cfproperty name="LIVRAISON_PREVUE_LE" type="date" default="">
	<cfproperty name="DATE_COMMANDE" type="date" default="">
	<cfproperty name="EXPEDIE_LE" type="date" default="">
	<cfproperty name="BOOL_DEVIS" type="numeric" default="0">
	<cfproperty name="NUMERO_TRACKING" type="string" default="">
	<cfproperty name="MONTANT" type="numeric" default="0">
	<cfproperty name="SEGMENT_FIXE" type="numeric" default="0">
	<cfproperty name="SEGMENT_MOBILE" type="numeric" default="0">
	<cfproperty name="IDGESTIONNAIRE_MODIF" type="numeric" default="0">
	<cfproperty name="IDGESTIONNAIRE_CREATE" type="numeric" default="0">
	<cfproperty name="IDGROUPE_REPERE" type="numeric" default="0">
	<cfproperty name="ENCOURS" type="numeric" default="0">
	<cfproperty name="EMAIL_CONTACT" type="string" default="">

	<cfscript>
		//Initialize the CFC with the default properties values.
		this.IDCOMMANDE = 0;
		this.IDCONTACT = 0;
		this.IDREVENDEUR = 0;
		this.IDOPERATEUR = 0;
		this.IDTYPE_COMMANDE = 0;
		this.IDSOCIETE = 0;
		this.IDCOMPTE_FACTURATION = 0;
		this.IDSOUS_COMPTE = 0;
		this.IDRACINE = 0;
		this.IDLAST_ETAT = 0;
		this.IDLAST_ACTION = 0;
		this.IDSITELIVRAISON = 0;
		this.IDTRANSPORTEUR = 0;
		this.IDPOOL_GESTIONNAIRE = 0;
		this.IDGESTIONNAIRE = 0;
		this.NUMERO_COMMANDE = "";
		this.LIBELLE_COMMANDE = "";
		this.LIBELLE_POOL = "";
		this.REF_CLIENT = "";
		this.REF_REVENDEUR = "";
		this.LIBELLE_REVENDEUR = "";
		this.LIBELLE_OPERATEUR = "";
		this.COMMENTAIRES = "";
		this.LIBELLE_COMPTE = "";
		this.LIBELLE_SOUSCOMPTE = "";
		this.LIBELLE_TRANSPORTEUR = "";
		this.LIBELLE_SITELIVRAISON = "";
		this.LIBELLE_LASTETAT = "";
		this.LIBELLE_LASTACTION = "";
		this.TYPE_OPERATION = "";
		this.PATRONYME_CONTACT = "";
		this.CREEE_PAR = "";
		this.MODIFIEE_PAR = "";
		this.BOOL_ENVOYER_VIAMAIL = 0;
		this.CREEE_LE = "";
		this.MODIFIEE_LE = "";
		this.ENVOYER_LE = "";
		this.LIVREE_LE = "";
		this.LIVRAISON_PREVUE_LE = "";
		this.DATE_COMMANDE = "";
		this.EXPEDIE_LE = "";
		this.BOOL_DEVIS = 0;
		this.NUMERO_TRACKING = "";
		this.MONTANT = 0;
		this.SEGMENT_FIXE = 0;
		this.SEGMENT_MOBILE = 0;
		this.IDGESTIONNAIRE_MODIF = 0;
		this.IDGESTIONNAIRE_CREATE = 0;
		this.IDGROUPE_REPERE = 0;
		this.ENCOURS = 0;
		this.EMAIL_CONTACT = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="Commande">
		<cfreturn this>
	</cffunction>
	<cffunction name="getIDCOMMANDE" output="false" access="public" returntype="any">
		<cfreturn this.IDCOMMANDE>
	</cffunction>

	<cffunction name="setIDCOMMANDE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDCOMMANDE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDCONTACT" output="false" access="public" returntype="any">
		<cfreturn this.IDCONTACT>
	</cffunction>

	<cffunction name="setIDCONTACT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDCONTACT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDREVENDEUR" output="false" access="public" returntype="any">
		<cfreturn this.IDREVENDEUR>
	</cffunction>

	<cffunction name="setIDREVENDEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDREVENDEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDOPERATEUR" output="false" access="public" returntype="any">
		<cfreturn this.IDOPERATEUR>
	</cffunction>

	<cffunction name="setIDOPERATEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDOPERATEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDTYPE_COMMANDE" output="false" access="public" returntype="any">
		<cfreturn this.IDTYPE_COMMANDE>
	</cffunction>

	<cffunction name="setIDTYPE_COMMANDE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDTYPE_COMMANDE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDSOCIETE" output="false" access="public" returntype="any">
		<cfreturn this.IDSOCIETE>
	</cffunction>

	<cffunction name="setIDSOCIETE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDSOCIETE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDCOMPTE_FACTURATION" output="false" access="public" returntype="any">
		<cfreturn this.IDCOMPTE_FACTURATION>
	</cffunction>

	<cffunction name="setIDCOMPTE_FACTURATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDCOMPTE_FACTURATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDSOUS_COMPTE" output="false" access="public" returntype="any">
		<cfreturn this.IDSOUS_COMPTE>
	</cffunction>

	<cffunction name="setIDSOUS_COMPTE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDSOUS_COMPTE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDRACINE" output="false" access="public" returntype="any">
		<cfreturn this.IDRACINE>
	</cffunction>

	<cffunction name="setIDRACINE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDRACINE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDLAST_ETAT" output="false" access="public" returntype="any">
		<cfreturn this.IDLAST_ETAT>
	</cffunction>

	<cffunction name="setIDLAST_ETAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDLAST_ETAT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDLAST_ACTION" output="false" access="public" returntype="any">
		<cfreturn this.IDLAST_ACTION>
	</cffunction>

	<cffunction name="setIDLAST_ACTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDLAST_ACTION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDSITELIVRAISON" output="false" access="public" returntype="any">
		<cfreturn this.IDSITELIVRAISON>
	</cffunction>

	<cffunction name="setIDSITELIVRAISON" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDSITELIVRAISON = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDTRANSPORTEUR" output="false" access="public" returntype="any">
		<cfreturn this.IDTRANSPORTEUR>
	</cffunction>

	<cffunction name="setIDTRANSPORTEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDTRANSPORTEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDPOOL_GESTIONNAIRE" output="false" access="public" returntype="any">
		<cfreturn this.IDPOOL_GESTIONNAIRE>
	</cffunction>

	<cffunction name="setIDPOOL_GESTIONNAIRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDPOOL_GESTIONNAIRE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDGESTIONNAIRE" output="false" access="public" returntype="any">
		<cfreturn this.IDGESTIONNAIRE>
	</cffunction>

	<cffunction name="setIDGESTIONNAIRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDGESTIONNAIRE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNUMERO_COMMANDE" output="false" access="public" returntype="any">
		<cfreturn this.NUMERO_COMMANDE>
	</cffunction>

	<cffunction name="setNUMERO_COMMANDE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.NUMERO_COMMANDE = arguments.val>
	</cffunction>

	<cffunction name="getLIBELLE_COMMANDE" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_COMMANDE>
	</cffunction>

	<cffunction name="setLIBELLE_COMMANDE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_COMMANDE = arguments.val>
	</cffunction>

	<cffunction name="getLIBELLE_POOL" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_POOL>
	</cffunction>

	<cffunction name="setLIBELLE_POOL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_POOL = arguments.val>
	</cffunction>

	<cffunction name="getREF_CLIENT" output="false" access="public" returntype="any">
		<cfreturn this.REF_CLIENT>
	</cffunction>

	<cffunction name="setREF_CLIENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.REF_CLIENT = arguments.val>
	</cffunction>

	<cffunction name="getREF_REVENDEUR" output="false" access="public" returntype="any">
		<cfreturn this.REF_REVENDEUR>
	</cffunction>

	<cffunction name="setREF_REVENDEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.REF_REVENDEUR = arguments.val>
	</cffunction>

	<cffunction name="getLIBELLE_REVENDEUR" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_REVENDEUR>
	</cffunction>

	<cffunction name="setLIBELLE_REVENDEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_REVENDEUR = arguments.val>
	</cffunction>

	<cffunction name="getLIBELLE_OPERATEUR" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_OPERATEUR>
	</cffunction>

	<cffunction name="setLIBELLE_OPERATEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_OPERATEUR = arguments.val>
	</cffunction>

	<cffunction name="getCOMMENTAIRES" output="false" access="public" returntype="any">
		<cfreturn this.COMMENTAIRES>
	</cffunction>

	<cffunction name="setCOMMENTAIRES" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.COMMENTAIRES = arguments.val>
	</cffunction>

	<cffunction name="getLIBELLE_COMPTE" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_COMPTE>
	</cffunction>

	<cffunction name="setLIBELLE_COMPTE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_COMPTE = arguments.val>
	</cffunction>

	<cffunction name="getLIBELLE_SOUSCOMPTE" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_SOUSCOMPTE>
	</cffunction>

	<cffunction name="setLIBELLE_SOUSCOMPTE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_SOUSCOMPTE = arguments.val>
	</cffunction>

	<cffunction name="getLIBELLE_TRANSPORTEUR" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_TRANSPORTEUR>
	</cffunction>

	<cffunction name="setLIBELLE_TRANSPORTEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_TRANSPORTEUR = arguments.val>
	</cffunction>

	<cffunction name="getLIBELLE_SITELIVRAISON" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_SITELIVRAISON>
	</cffunction>

	<cffunction name="setLIBELLE_SITELIVRAISON" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_SITELIVRAISON = arguments.val>
	</cffunction>

	<cffunction name="getLIBELLE_LASTETAT" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_LASTETAT>
	</cffunction>

	<cffunction name="setLIBELLE_LASTETAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_LASTETAT = arguments.val>
	</cffunction>

	<cffunction name="getLIBELLE_LASTACTION" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_LASTACTION>
	</cffunction>

	<cffunction name="setLIBELLE_LASTACTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_LASTACTION = arguments.val>
	</cffunction>

	<cffunction name="getTYPE_OPERATION" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_OPERATION>
	</cffunction>

	<cffunction name="setTYPE_OPERATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_OPERATION = arguments.val>
	</cffunction>

	<cffunction name="getPATRONYME_CONTACT" output="false" access="public" returntype="any">
		<cfreturn this.PATRONYME_CONTACT>
	</cffunction>

	<cffunction name="setPATRONYME_CONTACT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.PATRONYME_CONTACT = arguments.val>
	</cffunction>

	<cffunction name="getCREEE_PAR" output="false" access="public" returntype="any">
		<cfreturn this.CREEE_PAR>
	</cffunction>

	<cffunction name="setCREEE_PAR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CREEE_PAR = arguments.val>
	</cffunction>

	<cffunction name="getMODIFIEE_PAR" output="false" access="public" returntype="any">
		<cfreturn this.MODIFIEE_PAR>
	</cffunction>

	<cffunction name="setMODIFIEE_PAR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.MODIFIEE_PAR = arguments.val>
	</cffunction>

	<cffunction name="getBOOL_ENVOYER_VIAMAIL" output="false" access="public" returntype="any">
		<cfreturn this.BOOL_ENVOYER_VIAMAIL>
	</cffunction>

	<cffunction name="setBOOL_ENVOYER_VIAMAIL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.BOOL_ENVOYER_VIAMAIL = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getCREEE_LE" output="false" access="public" returntype="any">
		<cfreturn this.CREEE_LE>
	</cffunction>

	<cffunction name="setCREEE_LE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.CREEE_LE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getMODIFIEE_LE" output="false" access="public" returntype="any">
		<cfreturn this.MODIFIEE_LE>
	</cffunction>

	<cffunction name="setMODIFIEE_LE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.MODIFIEE_LE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getENVOYER_LE" output="false" access="public" returntype="any">
		<cfreturn this.ENVOYER_LE>
	</cffunction>

	<cffunction name="setENVOYER_LE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.ENVOYER_LE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getLIVREE_LE" output="false" access="public" returntype="any">
		<cfreturn this.LIVREE_LE>
	</cffunction>

	<cffunction name="setLIVREE_LE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.LIVREE_LE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getLIVRAISON_PREVUE_LE" output="false" access="public" returntype="any">
		<cfreturn this.LIVRAISON_PREVUE_LE>
	</cffunction>

	<cffunction name="setLIVRAISON_PREVUE_LE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.LIVRAISON_PREVUE_LE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDATE_COMMANDE" output="false" access="public" returntype="any">
		<cfreturn this.DATE_COMMANDE>
	</cffunction>

	<cffunction name="setDATE_COMMANDE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DATE_COMMANDE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getEXPEDIE_LE" output="false" access="public" returntype="any">
		<cfreturn this.EXPEDIE_LE>
	</cffunction>

	<cffunction name="setEXPEDIE_LE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.EXPEDIE_LE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getBOOL_DEVIS" output="false" access="public" returntype="any">
		<cfreturn this.BOOL_DEVIS>
	</cffunction>

	<cffunction name="setBOOL_DEVIS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.BOOL_DEVIS = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNUMERO_TRACKING" output="false" access="public" returntype="any">
		<cfreturn this.NUMERO_TRACKING>
	</cffunction>

	<cffunction name="setNUMERO_TRACKING" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.NUMERO_TRACKING = arguments.val>
	</cffunction>

	<cffunction name="getMONTANT" output="false" access="public" returntype="any">
		<cfreturn this.MONTANT>
	</cffunction>

	<cffunction name="setMONTANT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.MONTANT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getSEGMENT_FIXE" output="false" access="public" returntype="any">
		<cfreturn this.SEGMENT_FIXE>
	</cffunction>

	<cffunction name="setSEGMENT_FIXE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.SEGMENT_FIXE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getSEGMENT_MOBILE" output="false" access="public" returntype="any">
		<cfreturn this.SEGMENT_MOBILE>
	</cffunction>

	<cffunction name="setSEGMENT_MOBILE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.SEGMENT_MOBILE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDGESTIONNAIRE_MODIF" output="false" access="public" returntype="any">
		<cfreturn this.IDGESTIONNAIRE_MODIF>
	</cffunction>

	<cffunction name="setIDGESTIONNAIRE_MODIF" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDGESTIONNAIRE_MODIF = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDGESTIONNAIRE_CREATE" output="false" access="public" returntype="any">
		<cfreturn this.IDGESTIONNAIRE_CREATE>
	</cffunction>

	<cffunction name="setIDGESTIONNAIRE_CREATE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDGESTIONNAIRE_CREATE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDGROUPE_REPERE" output="false" access="public" returntype="any">
		<cfreturn this.IDGROUPE_REPERE>
	</cffunction>

	<cffunction name="setIDGROUPE_REPERE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDGROUPE_REPERE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getENCOURS" output="false" access="public" returntype="any">
		<cfreturn this.ENCOURS>
	</cffunction>

	<cffunction name="setENCOURS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.ENCOURS = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getEMAIL_CONTACT" output="false" access="public" returntype="any">
		<cfreturn this.EMAIL_CONTACT>
	</cffunction>

	<cffunction name="setEMAIL_CONTACT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.EMAIL_CONTACT = arguments.val>
	</cffunction>



</cfcomponent>