<!---
Package : fr.consotel.consoview.facturation.consofacture
--->
<cfcomponent name="RapportConsoFactureGroupeStrategy">
	<cffunction name="getReportData" access="private" returntype="query">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset qGetReportData = getAboDataDetails(ID_PERIMETRE,RapportParams.DATEDEB,RapportParams.DATEFIN)>
		<cfreturn qGetReportData>
	</cffunction>

	<cffunction name="getAboData" access="public" returntype="query">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="dateDeb" required="true" type="string"/>
		<cfargument name="dateFin" required="true" type="string"/>
        <cfstoredproc datasource="#session.CDRDSN#" procedure="PKG_CV_GRCL_CDR.consofact">
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
 	        <cfprocparam  value="#dateDeb#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#dateFin#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfreturn qGetReportData>
	</cffunction>
	
	<cffunction name="getAboDataDetails" access="private" returntype="query">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="dateDeb" required="true" type="string"/>
		<cfargument name="dateFin" required="true" type="string"/>
        <cfstoredproc datasource="#session.CDRDSN#" procedure="PKG_CV_GRCL_CDR.consofact_det">
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
 	        <cfprocparam  value="#dateDeb#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#dateFin#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfreturn qGetReportData>
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset rapportParams.chaine_date = rapportParams.DATEDEB>
		<!---====== FORMATAGE DES DATES DE DEBUT ET DE FIN ======--->
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEDEB)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEDEB)>
		<cfset rapportParams.DATEDEB =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEFIN)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEFIN)>
		<cfset rapportParams.DATEFIN =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<!---====================================================--->
 		<cfset qGetData = getReportData(ID_PERIMETRE,RapportParams)>
		<cfif #qGetData.recordcount# gt 0>
   			<cfinclude template="./details_consoFacture_html.cfm">
		<cfelse>
			<cfoutput>
				<center><strong><h2>Aucunes données répondant à votre demande</h2></strong></center>
			</cfoutput>
		</cfif>
	</cffunction>
</cfcomponent>
