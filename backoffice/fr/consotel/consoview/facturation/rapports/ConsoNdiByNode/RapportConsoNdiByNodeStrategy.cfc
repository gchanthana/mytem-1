<!---
Package : fr.consotel.consoview.facturation.rapports.ConsoNdiByNode
--->
<cfcomponent name="RapportConsoNdiByNodeStrategy">
	<cfset tmpNbCol = 0>
	
	<cffunction name="getReportData" access="private" returntype="any">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset ID_ORGA = ID_PERIMETRE>
        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_REP_V3.DETAIL_CONSO_THEME">
	        <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_ORGA#" cfsqltype="CF_SQL_INTEGER">
 	        <cfprocparam  value="#RapportParams.DATEDEB#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATEFIN#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetReportData1">
        </cfstoredproc>
		
		 
		<cfset tmpNbLignes = qGetReportData1.recordcount / qGetReportData1['NB_COL'][1]>
		<cfset filter = "Fixe">
		<cfquery name="qGetReportData" dbtype="query">
			select *
			from qGetReportData1
			where SEGMENT_THEME like '#filter#'
		</cfquery>
		<cfset tmpNbThemes = qGetReportData.recordcount / tmpNbLignes>
		<cfset querySetCell(qGetReportData,"NB_COL",tmpNbThemes,1)>
		
		 
		<cfreturn qGetReportData>
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset rapportParams.chaine_date = rapportParams.DATEDEB>
		<!---====== FORMATAGE DES DATES DE DEBUT ET DE FIN ======--->
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEDEB)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEDEB)>
		<cfset rapportParams.DATEDEB =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEFIN)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEFIN)>
		<cfset rapportParams.DATEFIN =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<!---====================================================--->
		<cfset qGetData = getReportData(ID_PERIMETRE,RapportParams)>

		<cfif #qGetData.recordcount# gt 0>
			<cfset MAX_ROWS = 50000>
			<cfset NB_COL = qGetData['NB_COL'][1]>
   			<cfinclude template="./consoNdiByNode_xls.cfm">
		<cfelse>
			<cfoutput>
				<center><strong><h2>Aucunes données répondant à votre demande</h2></strong></center>
			</cfoutput>
		</cfif>
	</cffunction>
</cfcomponent>
