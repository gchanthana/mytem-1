<cfcomponent name="TarifGroupeStrategy">

	<cffunction name="getListeTarifs" access="remote" output="false" returntype="query">
		<cfargument name="arr" type="array">
			<cfquery name="qgetabo" datasource="#session.OffreDSN#">
						select 	o.operateurid, op.nom, o.offreid, o.offrenom, o.idtype_offre,o.segmentid, 
								ofc.datesouscription, ofc.dateresiliation, ofc.versionid, tv.date_valide, tv.date_expire
						from 	operateur op, offre_client ofc, offre o, tarif_ver tv, ref_client rc, groupe_client_ref_client gcrc,
								groupe_client gc
						WHERE 	op.operateurid=o.operateurid 
								and OFc.offreid=o.offreid 
								AND o.offreid=tv.tarifid 
								AND OFc.Versionid=tv.versionid
								and ofc.clientid=rc.clientid
								AND rc.idref_client=gcrc.Idref_Client
								AND gcrc.Idgroupe_Client=gc.Idgroupe_Client
								AND gc.idgroupe_client=#arr[1]#
						GROUP BY o.operateurid, op.nom, o.offreid, o.offrenom, o.idtype_offre,o.segmentid, 
								ofc.datesouscription, ofc.dateresiliation, ofc.versionid, tv.date_valide, tv.date_expire
						order by op.nom, o.offrenom
					</cfquery>
					<cfloop from="1" to="#qgetabo.recordcount#" index="i">
						<cfset qgetabo.date_valide[i]=LsDateFormat(qgetabo.date_valide[i],"dd mmmm yyyy")>
						<cfset qgetabo.datesouscription[i]=LsDateFormat(qgetabo.datesouscription[i],"dd mmmm yyyy")>
						<cfset qgetabo.dateresiliation[i]=LsDateFormat(qgetabo.dateresiliation[i],"dd mmmm yyyy")>
					</cfloop>
					<cfif isdefined('session.tarifs')>
						<cfset structDelete(session,"tarifs")>
					</cfif>
					<cfset session.tarifs=qgetabo>
				<cfreturn qgetabo/>
			</cffunction>
			
			<cffunction name="getTarifs" access="remote" output="false" returntype="query">
				<cfargument name="arr" type="array">
					<cfquery name="Tarif" datasource="#session.OffreDSN#">
						select 	td.VersionID, td.PaysConsotelID, td.PrixMN, td.PlageID, td.Tps_CT,
								td.Prix_CT, td.Tps_PT, td.Prix_Pt, 
								decode(td.paysid, 493, 'GSM Orange', 1150, 'GSM SFR', 1151, 'GSM Bouygues', pc.Pays) as pays
						from 	tarif_detail td, Pays_Consotel pc
						where 	td.paysconsotelid=pc.PaysConsotelID and Versionid=#session.tarifs.versionid[arr[1]]# and td.onnet=0
						order by pc.Pays, td.plageid
					</cfquery>
				<cfreturn Tarif/>
			</cffunction>
			
			<cffunction name="getVersion" access="remote" output="false" returntype="query">
				<cfargument name="arr" type="array">
					<cfquery name="qGetVer" datasource="#session.OffreDSN#">
						select 	tv.tarifID, versionID, o.offrenom,
								decode(typeremise, 0, 'non', 'oui') AS remise, o.idtype_offre
						FROM 	tarif_ver tv, offre o
						WHERE 	tv.tarifID=o.offreID and tv.tarifID=#session.tarifs.offreid[arr[1]]# and tv.versionid=#session.tarifs.versionid[arr[1]]#
					</cfquery>
				<cfreturn qGetVer/>
			</cffunction>
			
			<cffunction name="getAllVersions" access="remote" output="false" returntype="query">
				<cfargument name="arr" type="array">
					<cfquery name="qGetAllVer" datasource="#session.OffreDSN#">
						select 	distinct tv.versionID, decode(typeremise,0,'non','oui') AS remise, tv.date_valide, tv.date_expire,
								to_char(tv.date_valide, 'dd/mm/yyyy') as date_valide_char, 
								to_char(tv.date_expire, 'dd/mm/yyyy') as date_expire_char,
								ofc.datesouscription, ofc.dateresiliation
						FROM 	tarif_ver tv, offre o, offre_client ofc
						WHERE 	tv.tarifID=o.offreid and tv.tarifID=#session.tarifs.offreid[arr[2]]# 
								AND ofc.versionid=tv.versionid
								and ofc.offreclientid in (SELECT DISTINCT offreclientid 
								FROM offre_client oc
								WHERE oc.clientid IN (
								SELECT rc.clientID
	                             FROM 	groupe_client_ref_client gcrc, ref_client rc
	                             WHERE gcrc.idref_client= rc.idref_client
	                             AND gcrc.idgroupe_client=#arr[1]#
								)
								)
						order by date_valide desc
					</cfquery>
				<cfreturn qGetAllVer/>
			</cffunction>
			
			<cffunction name="getRemise" access="remote" output="false" returntype="query">
				<cfargument name="arr" type="array">
					<cfquery name="ListeRemises" datasource="#session.OffreDSN#">
						select * 
						from remise 
						where VersionID=#session.tarifs.versionid[arr[1]]# and multisite=1 
						order by multisite,borneinf
					</cfquery>
				<cfreturn ListeRemises/>
			</cffunction>
			
			<cffunction name="getOffre" access="remote" output="false" returntype="query">
				<cfargument name="arr" type="array">
					<cfquery datasource="#session.OffreDSN#" name="GetOffre">
						select 	o.*, op.nom, tyo.label_type_offre, decode(o.type_accesID,1,'Direct',2,'Dégroupé',3,'Préselection') AS leTypeAcces,
			decode(o.degroupable,0,'Non',1,'Oui') AS isDegroupable
						from 	offre o, operateur  op, type_offre tyo
						where 	o.offreid = #session.tarifs.offreid[arr[1]]# 
						and o.operateurID=op.operateurID
						AND o.idtype_offre=tyo.Idtype_Offre
					</cfquery>
				<cfreturn GetOffre/>
			</cffunction>
			
			<cffunction name="getRacco" access="remote" output="false" returntype="query">
				<cfargument name="arr" type="array">
					<cfquery name="DetailVer" datasource="#session.OffreDSN#">
						select tv.*, 
							decode(typeremise,0,'Aucune',1,'Remise sur Volume Global',2,'Remise sur Volume Destination',3,'Remise sur Durée') as type_remise
						from Tarif_VER tv 
						where VersionID=#session.tarifs.versionid[arr[1]]#
					</cfquery>
				<cfreturn DetailVer/>
			</cffunction>
		</cfcomponent>