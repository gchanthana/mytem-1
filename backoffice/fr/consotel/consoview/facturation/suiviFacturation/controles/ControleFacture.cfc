<cfcomponent output="false">
	<cfproperty name="idGroupe" type="numeric" default="0">
	<cfproperty name="idGroupeLigne" type="numeric" default="0">
	<cfproperty name="typePerimetre" type="string" default="Groupe">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupe = session.perimetre.ID_GROUPE;
		variables.idGroupeLigne = session.perimetre.ID_PERIMETRE;
		variables.typePerimetre = session.perimetre.TYPE_PERIMETRE;
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getidGroupe" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupe>
	</cffunction>
	
	<cffunction name="getidGroupeLigne" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeLigne>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.typePerimetre>
	</cffunction>
	
<!--- =============== DETAIL D UNE FACTURE ==================================================== --->	

	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/30/2007
		
		Description : Lignes de facturations d'une facture
		
		Params in
			 Facture
			
		Params out
			 DetailFacture[].	
	--->
	<cffunction name="getListeLignesFacturationFacturePerimetre" access="remote" returntype="array">
		<cfargument name="myFacture" type="Facture">
		<cfset qDetailFacture = Evaluate("getListeLignesFacturationFacture#getTypePerimetre()#AsQuery(myFacture)")>	
		<cfset tabDetailFacture = arrayNew(1)>
		<cfloop query="qDetailFacture">
			<cfset myDetailFacture = createObject("component","DetailFacture")>
			<cfscript>				
				myDetailFacture.setnumeroFacture(qDetailFacture.NUMERO_FACTURE);
				myDetailFacture.setcompteFacturation(qDetailFacture.COMPTE_FACTURATION);
				myDetailFacture.setsousCompte(qDetailFacture.SOUS_COMPTE);
				myDetailFacture.setlibelleProduit(qDetailFacture.LIBELLE_PRODUIT);
				myDetailFacture.setsousTete(qDetailFacture.SOUS_TETE);
				myDetailFacture.setlibelle(qDetailFacture.LIBELLE);	
				myDetailFacture.setgamme(qDetailFacture.GAMME);
				myDetailFacture.settypeLigne(qDetailFacture.LIBELLE_TYPE_LIGNE);
				myDetailFacture.setnomSite(qDetailFacture.NOM_SITE);	
				myDetailFacture.setadresse1(qDetailFacture.ADRESSE1);
				myDetailFacture.setadresse2(qDetailFacture.ADRESSE2);
				myDetailFacture.setzipCode(qDetailFacture.ZIPCODE);	
				myDetailFacture.setcommune(qDetailFacture.COMMUNE);
				myDetailFacture.setmontant(qDetailFacture.MONTANT);
				myDetailFacture.setnombreAppel(qDetailFacture.NOMBRE_APPEL);
				myDetailFacture.setdureeAppel(qDetailFacture.DUREE_APPEL);
				myDetailFacture.setquantite(qDetailFacture.QTE);	
				myDetailFacture.setprixUnitaire(qDetailFacture.PRIX_UNIT);
				myDetailFacture.setcodeTva(qDetailFacture.CODE_TVA);				
				myDetailFacture.setdateDebut(createDate(left(qDetailFacture.DATEDEB,4),mid(qDetailFacture.DATEDEB,6,2),mid(qDetailFacture.DATEDEB,9,2)));
				myDetailFacture.setdateFin(createDate(left(qDetailFacture.DATEFIN,4),mid(qDetailFacture.DATEFIN,6,2),mid(qDetailFacture.DATEFIN,9,2)));
				myDetailFacture.setdateEmission(qDetailFacture.DATE_EMISSION);
				
				//myDetailFacture.setdateEmission(createDate(left(qDetailFacture.DATE_EMISSION,4),mid(qDetailFacture.DATE_EMISSION,6,2),&mid(qDetailFacture.DATE_EMISSION,9,2)));
			    //myDetailFacture.setdateFin(qDetailFacture.DATEFIN);
				/* myDetailFacture.setdateEmission(); */
			</cfscript>
			<cfset arrayAppend(tabDetailFacture,myDetailFacture)>
		</cfloop>
		<cfreturn tabDetailFacture>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/30/2007
		
		Description : liste des lignes de facturation d'une facture GroupeLigne
		
		Params in
			 Facture
			
		Params out
			 query.
	--->
	<cffunction name="getListeLignesFacturationFactureGroupeLigneAsQuery" access="public" returntype="query" output="false" >
		<cfargument name="myFacture" type="Facture" required="true">
		<cfset qDetailFacture = 
					createObject("component","fr.consotel.consoview.Facture.TypeFactureCompleteGroupeLigneStrategy")
						.getData(myFacture.getPeriodeId(),0,getidGroupeLigne())>
		<cfreturn qDetailFacture>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/30/2007
		
		Description : Liste des lignes de facturations d'une facture Groupe
		
		Params in
			 Facture
			
		Params out
			 query.
	--->
	<cffunction access="public" name="getListeLignesFacturationFactureGroupeAsQuery" returntype="query">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfset qDetailFacture = createObject("component","fr.consotel.consoview.Facture.TypeFactureCompleteGroupeStrategy").getData(myFacture.getPeriodeId(),0,getidGroupe())>
		<cfreturn qDetailFacture>
	</cffunction>
	
<!--- =============== DETAIL D UNE FACTURE FIN ======================================================================= --->		

<!--- =============== DETAIL D UNE FACTURE GROUPE DE PRODUIT CTRL ==================================================== --->		
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/29/2007
		
		Description :
					Retourne la liste des lignes de facturation d'une facture 
					qui concernent les produits marqués et facturés sur toute la période de la facture 
		Params in
			 Facture
			
		Params out
			 array.
	--->
	<cffunction name="getListeGrpProduitFacture" access="remote" returntype="array">
		<cfargument name="myFacture" type="Facture">
		<cfset tlisteLignesFact = Evaluate("getListeGrpProduitFacture#getTypePerimetre()#AsQuery(myFacture)")>	
		
		<cfset qlisteLignesFactAbo = tlisteLignesFact[1]>
		<cfset qlisteLignesFactConso = tlisteLignesFact[2]>
		
		<cfset tabLignesFacturation = arrayNew(1)>
		<cfset tabLignesFacturationAbo = arrayNew(1)>
		<cfset tabLignesFacturationConso = arrayNew(1)>

		<!--- ============ ABO ============ --->			
		<cfloop query="qlisteLignesFactAbo">
			<cfset ligneFacturationAbo = createObject("component","LigneFacturation")>
			<cfscript>
				ligneFacturationAbo.setId(qlisteLignesFactAbo.IDGRP_CTL);
				ligneFacturationAbo.setLibelleProduit(qlisteLignesFactAbo.GRP_CTL);
				ligneFacturationAbo.setOperateurId(myFacture.getOperateurId());
				ligneFacturationAbo.setOperateurLibelle(myFacture.getOperateurLibelle());
				ligneFacturationAbo.setPrixUnitaire(qlisteLignesFactAbo.PRIX_UNIT);
				ligneFacturationAbo.setPrixUnitaireRemise(qlisteLignesFactAbo.PRIX_UNIT_REMISE);
				ligneFacturationAbo.setQte(qlisteLignesFactAbo.QTE);
				ligneFacturationAbo.setMontantTotal(qlisteLignesFactAbo.MONTANT_FACTURE);				
				ligneFacturationAbo.setMontantCalculeRemise(qlisteLignesFactAbo.MONTANT_CALCULE);
				ligneFacturationAbo.setDifference(qlisteLignesFactAbo.MONTANT_DIFFERENCE);
				ligneFacturationAbo.setPoidsDifference(qlisteLignesFactAbo.PCT_DIFFERENCE);
				ligneFacturationAbo.setBoolPublic(qlisteLignesFactAbo.VERSION_TARIF);
				ligneFacturationAbo.setLibelleCompte(qlisteLignesFactAbo.COMPTE_FACTURATION);
				ligneFacturationAbo.setLibelleSousCompte(qlisteLignesFactAbo.SOUS_COMPTE);
				ligneFacturationAbo.setTypeTarif(qlisteLignesFactAbo.VERSION_TARIF);
				ligneFacturationAbo.setTypeTheme('abonnement');
			</cfscript>	
			<cfset arrayAppend(tabLignesFacturationAbo,ligneFacturationAbo)>
		</cfloop>
		
		<!--- ============ CONSO ============ --->			
		<cfloop query="qlisteLignesFactConso">
			<cfset ligneFacturationConso = createObject("component","LigneFacturation")>
			<cfscript>
				ligneFacturationConso.setId(qlisteLignesFactConso.IDGRP_CTL);
				ligneFacturationConso.setLibelleProduit(qlisteLignesFactConso.GRP_CTL);
				ligneFacturationConso.setOperateurId(myFacture.getOperateurId());
				ligneFacturationConso.setOperateurLibelle(myFacture.getOperateurLibelle());
				ligneFacturationConso.setPrixUnitaire(qlisteLignesFactConso.PRIX_UNIT);
				ligneFacturationConso.setPrixUnitaireRemise(qlisteLignesFactConso.PRIX_UNIT_REMISE);
				ligneFacturationConso.setMontantTotal(qlisteLignesFactConso.MONTANT_FACTURE);				
				ligneFacturationConso.setMontantMoyen(qlisteLignesFactConso.MONTANT_CALCULE);
				ligneFacturationConso.setDifference(qlisteLignesFactConso.MONTANT_DIFFERENCE);
				ligneFacturationConso.setDuree(Int(qlisteLignesFactConso.DUREE_MIN));
				ligneFacturationConso.setPoidsDifference(qlisteLignesFactConso.PCT_DIFFERENCE);
				ligneFacturationConso.setBoolPublic(qlisteLignesFactConso.VERSION_TARIF);
				ligneFacturationConso.setLibelleCompte(qlisteLignesFactConso.COMPTE_FACTURATION);
				ligneFacturationConso.setLibelleSousCompte(qlisteLignesFactConso.SOUS_COMPTE);
				ligneFacturationConso.setTypeTarif(qlisteLignesFactConso.VERSION_TARIF);
				ligneFacturationConso.setTypeTheme('consommation');
				
			</cfscript>
			<cfset arrayAppend(tabLignesFacturationConso,ligneFacturationConso)>
		</cfloop>
		
		<cfset arrayappend(tabLignesFacturation,tabLignesFacturationAbo)>
		<cfset arrayappend(tabLignesFacturation,tabLignesFacturationConso)>
		<cfreturn tabLignesFacturation>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/29/2007
		
		Description :Retourne la liste des groupe de produits d'une facture d'un groupe 
					qui concernent les produits marqués et facturés sur toute la période de la facture .
		Params in
			Facture
		Params out
			array	
	--->
	<cffunction access="public" name="getListeGrpProduitFactureGroupeAsQuery" returntype="array">
		<cfargument name="myFacture" type="Facture" required="true">				 
		<!--- PROCEDURE detail_facture_type_abos (    p_idracine                       IN INTEGER,
                                                      p_idinventaire_periode           OUT SYS_REFCURSOR
				                                      p_retour_abos                    OUT SYS_REFCURSOR) --->
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M32.groupe_facture_type_abos_v2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine " value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#myFacture.getPeriodeId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" value="#SESSION.USER.GLOBALIZATION#"/>
			<cfprocresult name="p_retour_abos"/>
		</cfstoredproc>
		
		<!--- PROCEDURE ddetail_facture_type_consos_v2 (		p_idracine				IN integer,
																p_idinventaire_periode	IN INTEGER,
									        					p_retour_consos			OUT SYS_REFCURSOR); --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M32.groupe_facture_type_consos_v2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine " value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#myFacture.getPeriodeId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" value="#SESSION.USER.GLOBALIZATION#"/>
			<cfprocresult name="p_retour_consos"/>
		</cfstoredproc>
		
		<cfset tListeLignesFacturations = arrayNew(1)>
		<cfset arrayappend(tListeLignesFacturations,p_retour_abos)>
		<cfset arrayappend(tListeLignesFacturations,p_retour_consos)>
		<cfreturn tListeLignesFacturations>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/29/2007
		
		Description :Retourne la liste des groupe de produits d'une facture d'un groupe 
					qui concernent les produits marqués et facturés sur toute la période de la facture .
		Params in
			Facture
		Params out
			array	
	--->
	<cffunction access="public" name="getListeGrpProduitFactureGroupeLigneAsQuery" returntype="array">
		<cfargument name="myFacture" type="Facture" required="true">
		<!--- 
			PROCEDURE PKG_CV_GLIG_FACTURATION.cf_detail_facture (	p_idgroupe_client               IN INTEGER,
				                                                    p_idinventaire_periode          IN INTEGER,
				                                                    p_retour_abos                    OUT SYS_REFCURSOR
				                                                    p_retour_consos                    OUT SYS_REFCURSOR)

		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M32.groupe_facture_type_abos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_numero_facture" value="#myFacture.getPeriodeId()#"/>
			<cfprocresult name="p_retour_abos" />
		</cfstoredproc>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M32.groupe_facture_type_consos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_numero_facture" value="#myFacture.getPeriodeId()#"/>
			<cfprocresult name="p_retour_consos" />
		</cfstoredproc>
		 
		<cfset tListeLignesFacturations = arrayNew(1)>
		<cfset arrayappend(tListeLignesFacturations,p_retour_abos)>
		<cfset arrayappend(tListeLignesFacturations,p_retour_consos)>
		<cfreturn tListeLignesFacturations>
	</cffunction>

<!--- =============== DETAIL D UNE FACTURE GROUPE DE PRODUIT CTRL FIN ==================================================== --->			

	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : Fournit la liste des lignes de facturation, concernant un produit, des factures émises sur la période.
					  La ligne de facturation est présente sisi le produit est facturé sur toute la période.
					  Les lignes de facturation sont groupées par prix unitaire(ou version de tarif).
		Params in
			 Produt,ParamsRecherche
		Params out
			 LigneFacturation[].
	--->
	<cffunction name="getListeLignesFactGroupByPrixUnitaire" access="remote" returntype="LigneFacturation[]">
		<cfargument name="myProduit" type="Produit">
		<cfargument name="paramsRecherche" type="ParamsRecherche">
		<cfset qlisteLignesFact = getListeLignesFactGroupByPrixUnitaireAsQuery(myProduit,paramsRecherche)>	
		<cfset tabLignesFacturation = arrayNew(1)>					
		<cfloop query="qlisteLignesFact">
			<cfset ligneFacturation = createObject("component","LigneFacturation")>
			<cfscript>
				ligneFacturation.setId(qlisteLignesFact.IDDETAIL_FACTURE_ABO);
				ligneFacturation.setIdProduit(qlisteLignesFact.IDPRODUIT_CLIENT);
				ligneFacturation.setLibelleProduit(qlisteLignesFact.LIBELLE_PRODUIT);
				ligneFacturation.setThemeId(qlisteLignesFact.IDTHEME_PRODUIT);
				ligneFacturation.setThemeLibelle(qlisteLignesFact.THEME_LIBELLE);
				ligneFacturation.setOperateurId(qlisteLignesFact.OPERATEURID);
				ligneFacturation.setOperateurLibelle(qlisteLignesFact.OPNOM);
				ligneFacturation.setTypeTheme(qlisteLignesFact.TYPE_THEME);
				//ligneFacturation.setTypeTrafic(qlisteLignesFact.TYPE_TRAFIC);
				ligneFacturation.setPoids(qlisteLignesFact.POIDS);
				ligneFacturation.setPourControle(qlisteLignesFact.CONTROLEE);
				ligneFacturation.setPrixUnitaire(qlisteLignesFact.PRIX_UNIT);
				ligneFacturation.setRemiseContrat(qlisteLignesFact.REMISE);
				ligneFacturation.setDateDebutVersion(qlisteLignesFact.DATEDEBUTVALIDITE);
				ligneFacturation.setDateFinVersion(qlisteLignesFact.DATEFINVALIDITE);
				ligneFacturation.setDateDebut(qlisteLignesFact.DATEDEB);
				ligneFacturation.setDateFin(qlisteLignesFact.DATEFIN);
				ligneFacturation.setQte(qlisteLignesFact.QTE);
				ligneFacturation.setMontantTotal(qlisteLignesFact.MONTANT_TOTAL);
				ligneFacturation.setMontantCalculeBrut(qlisteLignesFact.MONTANT_BRUT);
				ligneFacturation.setMontantCalculeRemise(qlisteLignesFact.MONTANT_REMISE);
				ligneFacturation.setDifference(qlisteLignesFact.DIFFERENCE);
				ligneFacturation.setPoidsDifference(qlisteLignesFact.POIDSDIFFERENCE);
			</cfscript>
			<cfset arrayAppend(tabLignesFacturation,ligneFacturation)>
		</cfloop>
		<cfreturn tabLignesFacturation>
	</cffunction>
		
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : Fournit la liste des lignes de facturation, concernant un produit, des factures émises sur la période.
					  La ligne de facturation est présente sisi le produit est facturé sur toute la période.
					  Les lignes de facturation sont groupées par prix unitaire(ou version de tarif).
		
		Params in
			Facture,ParamsRecherche
		Params out
			Query	
	--->
	<cffunction access="public" name="getListeLignesFactGroupByPrixUnitaireAsQuery" returntype="query">
		<cfargument name="myProduit" type="Produit" required="true">
		<cfargument name="paramsRecherche" type="ParamsRecherche">
		<!--- TODO: PKG_CV_FACTURATION.GETLISTELIGNEFACT_GROUPEDBYPU--->
		<!--- 
			PROCEDURE			
				PKG_CV_FACTURATION.GETLISTELIGNEFACT_GROUPEDBYPU
			PARAM
				p_numero
				p_date_debut
				p_date_fin
				
		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_FACTURATION.GETLISTELIGNEFACT_GROUPEDBYPU">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_numero_facture" value="#myProduit.getId()#"/>
			<cfprocparam cfsqltype="CF_SQL_DATE"  type="in" variable="p_date_debut" value="#paramsRecherche.getDateDebut()#"/>
			<cfprocparam cfsqltype="CF_SQL_DATE"  type="in" variable="p_date_fin" value="#paramsRecherche.getDateFin()#"/>
			<cfprocresult name="qListeLignesFacturation"/>        
		</cfstoredproc>
		<cfreturn qListeLignesFacturation>
	</cffunction>
	
	
	
	
</cfcomponent>