<cfcomponent output="false">
	<cfproperty name="idGroupe" type="numeric" default="0">
	<cfproperty name="idGroupeLigne" type="numeric" default="0">
	<cfproperty name="typePerimetre" type="string" default="Groupe">
	<cfproperty name="ABO" type="string" default="Abonnements">
	<cfproperty name="CONSO" type="string" default="Consommations">
	<cfproperty name="DATASOURCE_DB1" type="string" default="ROCOFFRE-DB1">

	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupe = session.perimetre.ID_GROUPE;
		variables.idGroupeLigne = session.perimetre.ID_PERIMETRE;
		variables.typePerimetre = session.perimetre.TYPE_PERIMETRE;
		variables.ABO = "Abonnements";
		variables.CONSO = "Consommations";
		variables.DATASOURCE_DB1 ="ROCOFFRE-DB1";
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getidGroupe" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupe>
	</cffunction>
	
	<cffunction name="getIdGroupeLigne" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeLigne>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.typePerimetre>
	</cffunction>

	<cffunction name="getDataSourceDB1" output="false" access="public" returntype="string">
		<cfreturn variables.DATASOURCE_DB1>
	</cffunction>
	
	<!--- ######################### HORS INVENTAIRE ############################################# --->
	
		<!---
		
		Auteur : samuel.divioka
		
		Date : 12/6/2007
		
		Description : Ressources hors inventaire facturï¿½es
		
		Ressoures (couple produit-ligne) facturï¿½es hors inventaire = 
		-	ressources qui n?ont jamais fait partie de l?inventaire et qui sont facturï¿½es ou 
		-	qui sont sortis de l?inventaire avant le debut de la pï¿½riode de la facture mais qui sont toujours facturï¿½es ou
		-	qui sont entrï¿½s dans l?inventaire aprï¿½s la fin de la facture et qui sont dï¿½jï¿½ facturï¿½es
		
		1 - On est d?accord que pour la partie abos, je prends tout ce qui est facturï¿½ sur ces ressources hors inventaire, et qui est de type de theme Abos.
		
		2 - Pour ce qui est des consos, tu me demandes de prendre, parmi les lignes des ressources hors inventaire, ceux qui ont un abo de sur theme ï¿½ Lignes ï¿½, et d?afficher les consos sur ces lignes.
		C?est bien ï¿½a ?
		
		Dans la feuille explicative, partie consos, tu me dis ï¿½ Faire remonter tous les produits, sauf ceux dï¿½jï¿½ identifiï¿½ par le processus ci-dessus afin d?eviter une remontï¿½ en double ï¿½. OK sauf que dans le 1er cas je ramene des abos et dans l?autre des consos.
		Donc il ne peut y avoir de remontï¿½ de doublon, je me trompe ?

	--->
	<cffunction access="remote" name="getMontantRessourcesHorsInventaire" returntype="array">
		<cfargument name="myFacture" type="Facture" required="true">		 
		<cfset qListeRessources = Evaluate("getMontantRessourcesHorsInventaire#getTypePerimetre()#AsQuery(myFacture)")>		
		
		<cfquery name="abos" dbtype="query">
			select SUM(QTE) as quantite, SUM(MONTANT) as montant
			from qListeRessources 
			where TYPE_THEME = '#variables.ABO#' 
		</cfquery>
				
		<cfquery name="consos" dbtype="query">
			select SUM(QTE) as volume, SUM(MONTANT) as montant
			from qListeRessources
			where TYPE_THEME = '#variables.CONSO#'
		</cfquery>				
		
		<cfset tab = arrayNew(1)>
			
		<cfset abo = createObject("component","Ressource")>	
		<cfset conso = createObject("component","Ressource")>
		<cfset abo.setTypeTheme('Abonnements')>	
		<cfset abo.setQuantite(0)>
		<cfset abo.setMontant(0)>			
		<cfset conso.setTypeTheme('Consommations')>		
		<cfset conso.setVolume(0)>
		<cfset conso.setMontant(0)>
		
		
		<cfloop query="abos">
			<cfset abo.setQuantite(abos.quantite)>
			<cfset abo.setMontant(abos.montant)>						
		</cfloop>
		
		<cfloop query="consos">						
			<cfset conso.setVolume(consos.volume)>
			<cfset conso.setMontant(consos.montant)>			
		</cfloop>
		
		<cfset arrayappend(tab,abo)>
		<cfset arrayappend(tab,conso)>
		
		<cfreturn tab>
	</cffunction>
	
	<cffunction access="remote" name="getMontantRessourcesHorsInventaireGroupeAsQuery" returntype="query">		 
		<cfargument name="myFacture" type="Facture" required="true">		 
		<!---
			PROCEDURE PKG_CV_GRCL_FACTURATION.Res_HorsInv_facturee_v2(					
									p_idracine     				IN INTEGER,	
									p_idinventaire_periode		IN INTEGER,			
									p_retour 	               OUT SYS_REFCURSOR) IS 			
		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.RES_HORSINV_FACTUREE_V2">
			<cfprocparam cfsqltype="CF_SQL_FLOAT"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_FLOAT"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_FLOAT"  type="in" variable="p_idinventaire_periode" value="#myFacture.getPeriodeId()#"/>
			<cfprocresult name="qListeRessources"/>        
		</cfstoredproc>
		<cfreturn qListeRessources>
	</cffunction>
	
	<cffunction access="remote" name="getMontantRessourcesHorsInventaireGroupeLigneAsQuery" returntype="query">
		<cfargument name="myFacture" type="Facture" required="true">		 
		 <!---
			PROCEDURE PKG_CV_GLIG_FACTURATION.Res_HorsInv_facturee_v2(					
									p_idracine     				IN INTEGER,	
									p_idinventaire_periode		IN INTEGER,			
									p_retour 	               OUT SYS_REFCURSOR) IS 				

				
		 --->

			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG_FACTURATION_V3.RES_HORSINV_FACTUREE_V2">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupeLigne" value="#getIdGroupeLigne()#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#myFacture.getPeriodeId()#"/>
				<cfprocresult name="qListeRessources"/>        
			</cfstoredproc>
		
		<cfreturn qListeRessources>
	</cffunction>

	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/11/2007
		
		Description : Liste des ressources hors inventaire de la facture
		cf->getMontantRessourcesHorsInventaire
	--->
	<cffunction access="remote" name="getDetailRessourcesHorsInventaire" returntype="any">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		
		<cfset qListeRessources = Evaluate("getDetailRessourcesHorsInventaire#getTypePerimetre()#AsQuery(myFacture,typeTheme)")>		
		<cfset tabRessources = arrayNew(1)>
		
		<cfquery name="qMontantRessources" dbtype="query">
			select type_theme,montant_total_theme as montant ,qte_total_theme as quantite
			from qListeRessources
			group by type_theme,montant,quantite
		</cfquery>
		
		<cfset arrayAppend(tabRessources,qListeRessources)>
		<cfset arrayAppend(tabRessources,qMontantRessources)>
		
		<cfreturn tabRessources />
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/7/2007
		
		Description : Detail des produits hors inventaire pour un groupe
		cf -> getMontantRessourcesHorsInventaire 
	--->
	<cffunction access="remote" name="getDetailRessourcesHorsInventaireGroupeAsQuery" returntype="query">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		 <!---
			pkg_cv_grcl_facturation.det_res_horsinv_facturee_v2(p_idracine => :p_idracine,
            			                                     p_idinventaire_periode => :p_idinventaire_periode,
                                                             p_retour => :p_retour); 	*
        
			<cfstoredproc datasource="#getDataSourceDB1()#" procedure="PKG_CV_GRCL_FACTURATION_V3.DET_RES_HORSINV_FACTUREE_V2">
				<cfprocparam cfsqltype="CF_SQL_FLOAT"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
				<cfprocparam cfsqltype="CF_SQL_FLOAT"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
				<cfprocparam cfsqltype="CF_SQL_FLOAT"  type="in" variable="p_idinventaire_periode" value="#myFacture.getPeriodeId()#"/>
				<cfprocresult name="qListeRessources"/>
			</cfstoredproc>
					
		 --->
		 
		 <cfscript>
			idracine_master = session.perimetre.idracine_master;
			idgroupe_racine = getidGroupe();
			idFacture = myFacture.getPeriodeId();			
			qstring = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=db-1-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-2-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-3-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-4-vip) (PORT=1522)) (FAILOVER=ON) (CONNECT_DATA=(SERVICE_NAME=appli.consotel.fr)))";
            q = "begin :result := PKG_CV_GRCL_FACTURATION_V3.FN_DET_RES_HORSINV_FACTUREE_V2(:a,:b,:c); end;";
            Class = createObject("java","java.lang.Class");
            DriverManager = CreateObject("java", "java.sql.DriverManager");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = CreateObject("java", "java.sql.Connection");
            con = DriverManager.getConnection(qstring, "offre", "consnia");
            stmt = createObject("java", "java.sql.Statement");
            stmt = con.prepareCall(q);
            OracleTypes = CreateObject("java", "oracle.jdbc.driver.OracleTypes");
            stmt.registerOutParameter("result", OracleTypes.CURSOR);
            stmt.setInt ("a", javacast("int",idracine_master));
            stmt.setInt ("b", javacast("int",idgroupe_racine));
			stmt.setInt ("c", javacast("int",idFacture));
            stmt.execute();
            rs = stmt.getObject("result");
            qListeRessources = CreateObject("java", "coldfusion.sql.QueryTable").init(rs);
		</cfscript>
	
		<cfswitch expression="#typeTheme#">
			<cfcase value="Consommations">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.CONSO#'
				</cfquery>
				<cfreturn qListe> 	
			</cfcase>
			
			<cfcase value="Abonnements">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.ABO#' 
				</cfquery>					
				<cfreturn qListe>
			</cfcase>
			
			
			<cfdefaultcase>							
				<cfreturn qListeRessources>
			</cfdefaultcase>
			
		</cfswitch>
				
		<cfreturn qListeRessources>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/7/2007
		
		Description : Detail des produits hors inventaire un groupeLigne
		cf -> getMontantRessourcesHorsInventaire
	--->
	<cffunction access="remote" name="getDetailRessourcesHorsInventaireGroupeLigneAsQuery" returntype="query">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		 <!---
			pkg_cv_glig_facturation.det_res_horsinv_facturee_v2(p_idracine => :p_idracine,
            			                                     p_idinventaire_periode => :p_idinventaire_periode,
                                                             p_retour => :p_retour); 				
		
		<cfstoredproc datasource="#getDataSourceDB1()#" procedure="PKG_CV_GLIG_FACTURATION_V3.DET_RES_HORSINV_FACTUREE_v2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getIdGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#myFacture.getPeriodeId()#"/>
			<cfprocresult name="qListeRessources"/>
		</cfstoredproc>
		 --->
		 
		 
		 <cfscript>
			idracine_master = session.perimetre.idracine_master;
			idgroupe_ligne = getidGroupe();
			idFacture = myFacture.getPeriodeId();
			
			qstring = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=db-1-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-2-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-3-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-4-vip) (PORT=1522)) (FAILOVER=ON) (CONNECT_DATA=(SERVICE_NAME=appli.consotel.fr)))";
            q = "begin :result := PKG_CV_GLIG_FACTURATION_V3.FN_DET_RES_HORSINV_FACTUREE_V2(:a,:b,:c); end;";
            Class = createObject("java","java.lang.Class");
            DriverManager = CreateObject("java", "java.sql.DriverManager");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = CreateObject("java", "java.sql.Connection");
            con = DriverManager.getConnection(qstring, "offre", "consnia");
            stmt = createObject("java", "java.sql.Statement");
            stmt = con.prepareCall(q);
            OracleTypes = CreateObject("java", "oracle.jdbc.driver.OracleTypes");
            stmt.registerOutParameter("result", OracleTypes.CURSOR);
            stmt.setInt ("a", javacast("int",idracine_master));
            stmt.setInt ("b", javacast("int",idgroupe_ligne));
			stmt.setInt ("c", javacast("int",idFacture));
            stmt.execute();
            rs = stmt.getObject("result");
            qListeRessources = CreateObject("java", "coldfusion.sql.QueryTable").init(rs);
		</cfscript>
		
		<cfswitch expression="#typeTheme#">
			<cfcase value="Consommations">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.CONSO#'
				</cfquery>
				<cfreturn qListe> 	
			</cfcase>
			
			<cfcase value="Abonnements">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.ABO#'
				</cfquery>					
				<cfreturn qListe>
			</cfcase>			
			
			<cfdefaultcase>							
				<cfreturn qListeRessources>
			</cfdefaultcase>
			
		</cfswitch>
				
		<cfreturn qListeRessources>
	</cffunction>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<!--- ########################### DANS INVENTAIRE ########################################### --->
	
	
	<cffunction access="remote" name="getMontantRessourcesInventaireNonFacturee" returntype="Ressource">
		<cfargument name="myFacture" type="Facture" required="true">		 
		<cfset qListeRessources = Evaluate("getDetailRessourcesInventaireNonFacturee#getTypePerimetre()#AsQuery(myFacture,'tout')")>		
		
		<cfquery name="abos" dbtype="query">
			select SUM(PRIX_UNIT) as montant,count(*) as qte
			from qListeRessources
			where TYPE_THEME = '#variables.ABO#' 
		</cfquery>
			
			
		<cfset abo = createObject("component","Ressource")>
		<cfset abo.setTypeTheme('Abonnements')>	
		
		<cfif abos.recordCount gt 0>
			<cfset abo.setQuantite(abos.qte)>
			<cfset abo.setMontant(abos.montant)>
		<cfelse>
			<cfset abo.setQuantite(0)>
			<cfset abo.setMontant(0)>
		</cfif>
		<cfreturn abo>
	</cffunction>
	
	


	<!---		
		Auteur : samuel.divioka		
		Date : 30/06/2008		
		Description : Toutes les ressources des sous tete du cf qui ont ï¿½tï¿½ (au moins un jour) dans l'inventaire
					pendant la pï¿½riode de la facture et qui n'apparaissent pas dans la facture.
	--->
	<cffunction access="remote" name="getDetailRessourcesInventaireNonFacturee" returntype="Ressource[]">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		
		<cfset qListeRessources = Evaluate("getDetailRessourcesInventaireNonFacturee#getTypePerimetre()#AsQuery(myFacture,typeTheme)")>		
		<cfset tabRessources = arrayNew(1)>
		<cfLoop query="qListeRessources">
			<cfset myRessource = createObject("component","Ressource")>
			 
			<cfscript>	
				myRessource.setlibelleProduit(qListeRessources.LIBELLE_PRODUIT);
				myRessource.setproduitClientId(qListeRessources.IDPRODUIT_CLIENT);
				myRessource.setsousTeteId(qListeRessources.IDSOUS_TETE); 
				myRessource.setsousTete(qListeRessources.SOUS_TETE);
				myRessource.setprixUnitaire(qListeRessources.PRIX_UNIT);
				//myRessource.setdateEntree(qListeRessources.DATE_ENTREE);
				//myRessource.setdateSortie(qListeRessources.DATE_SORTIE);
				myRessource.settypeTheme(qListeRessources.TYPE_THEME);			
			</cfscript>
					 
			<cfset arrayAppend(tabRessources,myRessource)>
		</cfLoop>
		<cfreturn tabRessources />
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka		
		Date : 30/06/2008		
		Description : cf -> getDetailRessourcesInventaireNonFacturee pour une racine 
	--->
	<cffunction access="remote" name="getDetailRessourcesInventaireNonFactureeGroupeAsQuery" returntype="query">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		 <!---
			pkg_cv_grcl_facturation.res_inv_nonfacturee(p_idracine => :p_idracine,
            			                                     p_idinventaire_periode => :p_idinventaire_periode,
                                                             p_retour => :p_retour); 				
		 --->
		 <cfscript>
			idracine_master = session.perimetre.idracine_master;
			idgroupe_ligne = getidGroupe();
			idFacture = myFacture.getPeriodeId();
			
			qstring = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=db-1-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-2-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-3-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-4-vip) (PORT=1522)) (FAILOVER=ON) (CONNECT_DATA=(SERVICE_NAME=appli.consotel.fr)))";
            q = "begin :result := PKG_CV_GRCL_FACTURATION_V3.fn_RES_INV_NONFACTUREE(:a,:b,:c); end;";
            Class = createObject("java","java.lang.Class");
            DriverManager = CreateObject("java", "java.sql.DriverManager");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = CreateObject("java", "java.sql.Connection");
            con = DriverManager.getConnection(qstring, "offre", "consnia");
            stmt = createObject("java", "java.sql.Statement");
            stmt = con.prepareCall(q);
            OracleTypes = CreateObject("java", "oracle.jdbc.driver.OracleTypes");
            stmt.registerOutParameter("result", OracleTypes.CURSOR);
            stmt.setInt ("a", javacast("int",idracine_master));
            stmt.setInt ("b", javacast("int",idgroupe_ligne));
			stmt.setInt ("c", javacast("int",idFacture));
            stmt.execute();
            rs = stmt.getObject("result");
            qListeRessources = CreateObject("java", "coldfusion.sql.QueryTable").init(rs);
		</cfscript>
		
		<cfswitch expression="#typeTheme#">
			<cfcase value="Consommations">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.CONSO#'
				</cfquery>
				<cfreturn qListe> 	
			</cfcase>
			
			<cfcase value="Abonnements">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.ABO#' 
				</cfquery>					
				<cfreturn qListe>
			</cfcase>			
			
			<cfdefaultcase>							
				<cfreturn qListeRessources>
			</cfdefaultcase>
			
		</cfswitch>
				
		<cfreturn qListeRessources>
	</cffunction>
	
	<!---		
		Auteur : samuel.divioka		
		Date : 30/06/2008		
		Description : cf -> getDetailRessourcesInventaireNonFacturee pour une racine
	--->
	<cffunction access="remote" name="getDetailRessourcesInventaireNonFactureeGroupeLigneAsQuery" returntype="query">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		 <!---
			pkg_cv_glig_facturation.res_inv_nonfacturee(p_idracine => :p_idracine,
            			                                p_idinventaire_periode => :p_idinventaire_periode,
                                                        p_retour => :p_retour); 				
		 --->
		 <cfscript>
			idracine_master = session.perimetre.idracine_master;
			idgroupe_ligne = getidGroupe();
			idFacture = myFacture.getPeriodeId();
			
			qstring = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=db-1-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-2-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-3-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-4-vip) (PORT=1522)) (FAILOVER=ON) (CONNECT_DATA=(SERVICE_NAME=appli.consotel.fr)))";
            q = "begin :result := PKG_CV_GLIG_FACTURATION_V3.fn_RES_INV_NONFACTUREE(:a,:b,:c); end;";
            Class = createObject("java","java.lang.Class");
            DriverManager = CreateObject("java", "java.sql.DriverManager");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = CreateObject("java", "java.sql.Connection");
            con = DriverManager.getConnection(qstring, "offre", "consnia");
            stmt = createObject("java", "java.sql.Statement");
            stmt = con.prepareCall(q);
            OracleTypes = CreateObject("java", "oracle.jdbc.driver.OracleTypes");
            stmt.registerOutParameter("result", OracleTypes.CURSOR);
            stmt.setInt ("a", javacast("int",idracine_master));
            stmt.setInt ("b", javacast("int",idgroupe_ligne));
			stmt.setInt ("c", javacast("int",idFacture));
            stmt.execute();
            rs = stmt.getObject("result");
            qListeRessources = CreateObject("java", "coldfusion.sql.QueryTable").init(rs);
		</cfscript>
		
		<cfswitch expression="#typeTheme#">
			<cfcase value="Consommations">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.CONSO#'
				</cfquery>
				<cfreturn qListe> 	
			</cfcase>
			
			<cfcase value="Abonnements">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.ABO#'
				</cfquery>					
				<cfreturn qListe>
			</cfcase>			
			
			<cfdefaultcase>							
				<cfreturn qListeRessources>
			</cfdefaultcase>			
		</cfswitch>
				
		<cfreturn qListeRessources>
	</cffunction>
</cfcomponent>