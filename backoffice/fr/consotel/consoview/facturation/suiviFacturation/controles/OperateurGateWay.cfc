<cfcomponent output="false">
	<cfproperty name="idGroupe" type="numeric" default="0">
	<cfproperty name="idGroupeLigne" type="numeric" default="0">
	<cfproperty name="typePerimetre" type="string" default="Groupe">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupe = session.perimetre.ID_GROUPE;
		variables.idGroupeLigne = session.perimetre.ID_PERIMETRE;
		variables.typePerimetre = session.perimetre.TYPE_PERIMETRE;
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getidGroupe" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupe>
	</cffunction>
	
	<cffunction name="getidGroupeLigne" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeLigne>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.typePerimetre>
	</cffunction>
	
	
	<!---		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Liste des opÃ©rateurs du pÃ©rimetre
		
		Params out Operateur[]
	--->
	<cffunction name="getListeOperateursPerimetre" access="remote" output="false" returntype="Operateur[]">
		<cfset qListeOperateursGroupe = Evaluate("getListeOperateurs#getTypePerimetre()#AsQuery()")>
		<cfset tabOperateur = arrayNew(1)>			
		 
		<cfloop query="qListeOperateurs">
			<cfset pOperateur = createObject("component","Operateur")>
			
			<cfscript>			
				if(isdefined("qListeOperateurs.OPNOM"))
					pOperateur.setNom(qListeOperateurs.OPNOM);
				else
					pOperateur.setNom(qListeOperateurs.NOM);
				pOperateur.setId(qListeOperateurs.OPERATEURID);	
			</cfscript>
			
			<cfset arrayAppend(tabOperateur,pOperateur)>
		</cfloop>
		<cfreturn tabOperateur>
	</cffunction>
	
	
	
	<!---		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Liste des opÃ©rateurs du Groupe
		
		Params out query
	--->
	<cffunction name="getListeOperateursGroupeAsQuery" access="remote" output="false" returntype="query">
		<!--- 
			PROCEDURE			
				pkg_cv_grcl_v3.sf_listeop_v2
			PARAM	
			in			
				p_idGroupe_maitre		INTEGER
			out				  	
				p_retour				QUERY				
		 --->	
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_v3.sf_listeop_v2">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idGroupe_maitre" value="#this.getidGroupe()#"/>		
			<cfprocresult name="qListeOperateurs"/>
		</cfstoredproc>						
		<cfreturn qListeOperateurs>
	</cffunction>
	
			
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Liste des opÃ©rateur d'un groupe ligne
		
		Params out query
	--->
	<cffunction name="getListeOperateursGroupeLigneAsQuery" access="remote" output="false" returntype="query">		
		<!--- 
			PROCEDURE			
				PKG_CV_GL.SF_LISTEOP
			PARAM	
			in			
				p_idGroupe_maitre		INTEGER
			out				  	
				p_retour				QUERY				
		 --->	
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_glig_v3.sf_listeop_v2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#this.getidGroupeLigne()#"/>
	        <cfprocresult name="qListeOperateurs"/>        
		</cfstoredproc>		
		<cfreturn qListeOperateurs>
	</cffunction>
	
</cfcomponent>