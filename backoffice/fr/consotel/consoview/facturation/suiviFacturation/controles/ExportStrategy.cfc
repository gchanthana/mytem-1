<cfcomponent output="true" name="ExportStrategy">
	<cffunction name="ExporterCSV" access="public" returntype="string">
		<cfargument name="facture" type="Facture" required="true"><cftry>			
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"facture_"&facture.getNumero()&".csv">
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset aDetail = facture.getDetail()>				
				<cfset NewLine = Chr(13) & Chr(10)>		
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="utf-8">
				<cfcontent type="text/plain">		
				<cfoutput>numeroFacture;dateEmission;datedebut;datefin;nomSite;adresse1;adresse2;zipCode;commune;compteFacturation;sousCompte;typeLigne;sousTete;libelle;gamme;libelleProduit;prixunitaire;codeTva;quantite;nombreAppels;dureeAppel;montant;#NewLine#</cfoutput><cfloop index="i" from="1" to="#arrayLen(aDetail)#"><cfoutput>#TRIM(aDetail[i].getnumeroFacture())#;#TRIM(LSDateFormat(aDetail[i].getDateEmission(),'DD/MM/YYYY'))#;#TRIM(LSDateFormat(aDetail[i].getDateDebut(),"dd/mm/yyyy"))#;#TRIM(LSDateFormat(aDetail[i].getDateFin(),"dd/mm/yyyy"))#;#TRIM(aDetail[i].getnomSite())#;#TRIM(aDetail[i].getadresse1())#;#TRIM(aDetail[i].getadresse2())#;#TRIM(aDetail[i].getzipCode())#;#TRIM(aDetail[i].getcommune())#;#TRIM(aDetail[i].getcompteFacturation())#;#TRIM(aDetail[i].getsousCompte())#;#TRIM(aDetail[i].gettypeLigne())#;#TRIM(aDetail[i].getsousTete())#;#TRIM(aDetail[i].getlibelle())#;#TRIM(aDetail[i].getgamme())#;#TRIM(aDetail[i].getlibelleProduit())#;#TRIM(aDetail[i].getprixUnitaire())#;#TRIM(aDetail[i].getcodeTva())#;#TRIM(aDetail[i].getquantite())#;#TRIM(aDetail[i].getnombreAppel())#;#TRIM(aDetail[i].getdureeAppel())#;#TRIM(aDetail[i].getmontant())#;#NewLine#</cfoutput></cfloop>
			</cfsavecontent>
			<!--- Crï¿½ation du fichier CSV --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="utf-8"
					addnewline="true" fixnewline="true" output="#contentObj#">			 
			<!--- Mis ï¿½ jour de l'etat --->
			<cfset result = facture.updateEtatExporte()/>	 
			
			<cfif result eq 1>			
				<cfreturn "#fileName#">
			<cfelse>
				<cfreturn "error">
			</cfif>		 		
			
		<cfcatch type="any">					
				<cfreturn "error">	
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>