<cfcomponent output="false" alias="fr.consotel.consoview.facturation.suiviFacturation.controles.Theme">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="id" type="numeric" default="0">
	<cfproperty name="libelle" type="string" default="">
	<cfproperty name="typeTheme" type="string" default="">
	<cfproperty name="idCategorie" type="numeric" default="0">

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.id = 0;
		variables.libelle = "";
		variables.typeTheme = "";
		variables.idCategorie = 0;
	</cfscript>

	<cffunction name="init" output="false" returntype="Theme">
		<cfreturn this>
	</cffunction>
	<cffunction name="getId" output="false" access="public" returntype="any">
		<cfreturn variables.Id>
	</cffunction>

	<cffunction name="setId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Id = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.Libelle>
	</cffunction>

	<cffunction name="setLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.Libelle = arguments.val>
	</cffunction>

	<cffunction name="getTypeTheme" output="false" access="public" returntype="any">
		<cfreturn variables.TypeTheme>
	</cffunction>

	<cffunction name="setTypeTheme" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.TypeTheme = arguments.val>
	</cffunction>

	<cffunction name="getIdCategorie" output="false" access="public" returntype="any">
		<cfreturn variables.IdCategorie>
	</cffunction>

	<cffunction name="setIdCategorie" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IdCategorie = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="setProprietes" output="false" access="public" returntype="void">
		<cfargument name="proprietes" type="any" required="true">
		
		<cfscript>
		//Initialize the CFC with the properties values.
			setId(proprietes.IDTHEME_PRODUIT);
			setLibelle(proprietes.THEME_LIBELLE);
			setTypeTheme(proprietes.TYPE_THEME);
//			setIdCategorie(proprietes.);			
		</cfscript>
	</cffunction>

</cfcomponent>