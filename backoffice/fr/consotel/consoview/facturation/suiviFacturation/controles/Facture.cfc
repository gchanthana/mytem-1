<cfcomponent output="true" alias="fr.consotel.consoview.facturation.suiviFacturation.controles.Facture">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="operateurId" type="numeric" default="0">
	<cfproperty name="operateurLibelle" type="string" default="">
	<cfproperty name="numero" type="string" default="">
	<cfproperty name="compteFacturation" type="string" default="-----------">
	<cfproperty name="compteFacturationId" type="numeric" default="">
	<cfproperty name="periodeId" type="numeric" default="0">
	<cfproperty name="dateDebut" type="date" default="">
	<cfproperty name="dateFin" type="date" default="">
	<cfproperty name="dateEmission" type="date" default="">
	<cfproperty name="BY_ADRESSE1" type="string" default="">
	<cfproperty name="BY_ADRESSE2" type="string" default="">
	<cfproperty name="BY_ZIPCODE" type="numeric" default="">
	<cfproperty name="BY_COMMUNE" type="string" default="">
	<cfproperty name="BY_CODE_SITE" type="string" default="">
	<cfproperty name="NOM" type="string" default="">
	
	<cfproperty name="montant" type="numeric" default="0">
	<cfproperty name="montantVerifiable" type="numeric" default="0">
	<cfproperty name="montantCalcule" type="numeric" default="0">
	<cfproperty name="montantCalculeFacture" type="numeric" default="0">
	
	
	<cfproperty name="creeePar" type="string" default=""> 
	
	<cfproperty name="libelle" type="string" default="">
	
	<cfproperty name="visee" type="numeric" default="0">
	<cfproperty name="viseeAna" type="numeric" default="0">
	<cfproperty name="controlee" type="numeric" default="0">
	<!--- CTRL INV --->
	<cfproperty name="controleeInv" type="numeric" default="0">
	<!--- FIN CTRL INV --->
	<cfproperty name="exportee" type="numeric" default="0">
	
	<cfproperty name="commentaireViser" type="string" default="">
	<cfproperty name="commentaireViserAna" type="string" default="">
	<cfproperty name="commentaireControler" type="string" default="">
	
	<!--- CTRL INV --->
	<cfproperty name="commentaireControlerInv" type="string" default="">
	<!--- FIN CTRL INV --->
	<cfproperty name="commentaireExporter" type="string" default="">
	
	<cfproperty name="dateViser" type="date" default="">
	<cfproperty name="dateViserAna" type="date" default="">
	<cfproperty name="dateControler" type="date" default="">
	<cfproperty name="dateExporter" type="date" default="">
	
	<!--- CTRL INV --->
	<cfproperty name="dateControlerInv" type="date" default="">
	<!--- FIN CTRL INV --->
	
	<cfproperty name="refClientId" type="numeric" default="0">
	
	 
	 
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.libelle = "";
		variables.operateurId = 0;
		variables.operateurLibelle = "";
		variables.numero = "";
		variables.compteFacturation = "-----------";
		variables.compteFacturationId = 0;
		variables.periodeId = 0;
		variables.dateDebut = "";
		variables.dateFin = "";
		variables.dateEmission = "";
		variables.montant = 0;
		
		variables.montantVerifiable = 0;
		variables.montantCalcule = 0;
		variables.montantCalculeFacture = 0;
		
		variables.creeePar = "";
		
		
		variables.visee = 0;
		variables.viseeAna = 0;
		variables.controlee = 0;
		//CTRL INV
		variables.controleeInv = 0;
		//FIN CTRL INV
		variables.exportee = 0;
		
		variables.BY_ADRESSE1 = "";
		variables.BY_ADRESSE2 = "";
		variables.BY_ZIPCODE = 0;
		variables.BY_COMMUNE = "";
		variables.BY_CODE_SITE = "";
		variables.NOM = "";
		
		variables.exportee = 0;
	
		
		variables.commentaireViser = "";
		variables.commentaireViserAna = "";
		variables.commentaireControler = "";
		//CTRL INV
		variables.commentaireControlerInv = "";
		//FIN CTRL INV
		variables.commentaireExporter = "";
		
		variables.dateViser = "";
		variables.dateViserAna = "";
		variables.dateControler = "";
		variables.dateExporter = "";
		
		variables.refClientId = 0;
	</cfscript>

	<cffunction name="init" output="false" returntype="Facture">
		<cfreturn this>
	</cffunction>
	
	
	
	<cffunction name="getRefClientId" output="false" access="public" returntype="any">
		<cfreturn variables.refClientId>
	</cffunction>

	<cffunction name="setRefClientId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.refClientId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

		
    <!--- Usage: GetcreeePar / SetcreeePar methods for creeePar value --->
    <cffunction name="getCreeePar" access="public" output="false" returntype="string">
       <cfreturn variables.creeePar/>
    </cffunction>

    <cffunction name="setCreeePar" access="public" output="false" returntype="void">
       <cfargument name="creeePar" type="string" required="true" />
       <cfset variables.creeePar = arguments.creeePar />
    </cffunction>
	
	  
		
	<cffunction name="getBY_ZIPCODE" output="false" access="public" returntype="any">
		<cfreturn variables.BY_ZIPCODE>
	</cffunction>

	<cffunction name="setBY_ZIPCODE" output="false" access="public" returntype="void">
		<cfargument name="BY_ZIPCODE" required="true">
		<cfif (IsNumeric(arguments.BY_ZIPCODE)) OR (arguments.BY_ZIPCODE EQ "")>
			<cfset variables.BY_ZIPCODE = arguments.BY_ZIPCODE>
		<cfelse>
			<cfthrow message="'#arguments.BY_ZIPCODE#' is not a valid numeric"/>
		</cfif>
	</cffunction>	
		
    <cffunction name="getBY_CODE_SITE" access="public" output="false" returntype="string">
       <cfreturn variables.BY_CODE_SITE/>
    </cffunction>

    <cffunction name="setBY_CODE_SITE" access="public" output="false" returntype="void">
       <cfargument name="BY_CODE_SITE" type="string" required="true" />
       <cfset variables.BY_CODE_SITE = arguments.BY_CODE_SITE />
    </cffunction>
	
	
    <cffunction name="getNOM" access="public" output="false" returntype="string">
       <cfreturn variables.NOM/>
    </cffunction>

    <cffunction name="setNOM" access="public" output="false" returntype="void">
       <cfargument name="NOM" type="string" required="true" />
       <cfset variables.NOM = arguments.NOM />
    </cffunction>
	
	
    <cffunction name="getBY_COMMUNE" access="public" output="false" returntype="string">
       <cfreturn variables.BY_COMMUNE/>
    </cffunction>

    <cffunction name="setBY_COMMUNE" access="public" output="false" returntype="void">
       <cfargument name="BY_COMMUNE" type="string" required="true" />
       <cfset variables.BY_COMMUNE = arguments.BY_COMMUNE />
    </cffunction>
	
    <cffunction name="getBY_ADRESSE2" access="public" output="false" returntype="string">
       <cfreturn variables.BY_ADRESSE2/>
    </cffunction>

    <cffunction name="setBY_ADRESSE2" access="public" output="false" returntype="void">
       <cfargument name="BY_ADRESSE2" type="string" required="true" />
       <cfset variables.BY_ADRESSE2 = arguments.BY_ADRESSE2 />
    </cffunction>
	
	
    <cffunction name="getBY_ADRESSE1" access="public" output="false" returntype="string">
       <cfreturn variables.BY_ADRESSE1/>
    </cffunction>

    <cffunction name="setBY_ADRESSE1" access="public" output="false" returntype="void">
       <cfargument name="BY_ADRESSE1" type="string" required="true" />
       <cfset variables.BY_ADRESSE1 = arguments.BY_ADRESSE1 />
    </cffunction>

	<cffunction name="getOperateurId" output="false" access="public" returntype="any">
		<cfreturn variables.operateurId>
	</cffunction>

	<cffunction name="setOperateurId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.operateurId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getOperateurLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.operateurLibelle>
	</cffunction>

	<cffunction name="setOperateurLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.operateurLibelle = arguments.val>
	</cffunction>
	
	<cffunction name="getLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.libelle>
	</cffunction>

	<cffunction name="setLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.libelle = arguments.val>
	</cffunction>

	<cffunction name="getNumero" output="false" access="public" returntype="any">
		<cfreturn variables.Numero>
	</cffunction>

	<cffunction name="setNumero" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.Numero = arguments.val>
	</cffunction>

	<cffunction name="getCompteFacturation" output="false" access="public" returntype="any">
		<cfreturn variables.CompteFacturation>
	</cffunction>

	<cffunction name="setCompteFacturation" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.compteFacturation = arguments.val>
	</cffunction>

	<cffunction name="getCompteFacturationId" output="false" access="public" returntype="any">
		
		<cfreturn variables.compteFacturationId>
	</cffunction>

	<cffunction name="setCompteFacturationId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.compteFacturationId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
		
	</cffunction>

	<cffunction name="getPeriodeId" output="false" access="public" returntype="any">
		<cfreturn variables.periodeId>
	</cffunction>

	<cffunction name="setPeriodeId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.periodeId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	
	<cffunction name="getDateFacture" output="false" access="public" returntype="query">
		<cfargument name="idGroupeClient" required="false" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.GET_DATEFACTURE_V2">
	         <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	         <cfprocparam  value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER">
	         <cfprocparam  value="2004/01/01" cfsqltype="CF_SQL_VARCHAR">
	         <cfprocresult name="qGetPeriod">
        </cfstoredproc>
		<cfreturn qGetPeriod>
	</cffunction>

	

	<cffunction name="getDateDebut" output="false" access="public" returntype="any">
		<cfreturn variables.dateDebut>
	</cffunction>

	<cffunction name="setDateDebut" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateDebut = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateFin" output="false" access="public" returntype="any">
		<cfreturn variables.dateFin>
	</cffunction>

	<cffunction name="setDateFin" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateFin = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateEmission" output="false" access="public" returntype="any">
		<cfreturn variables.dateEmission>
	</cffunction>

	<cffunction name="setDateEmission" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateEmission = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getMontant" output="false" access="public" returntype="any">
		<cfreturn variables.montant>
	</cffunction>

	<cffunction name="setMontant" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.montant = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getMontantVerifiable" output="false" access="public" returntype="any">
		<cfreturn variables.montantVerifiable>
	</cffunction>

	<cffunction name="setMontantVerifiable" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.montantVerifiable = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	
	
	<cffunction name="getMontantCalcule" output="false" access="public" returntype="any">
		<cfreturn variables.montantCalcule>
	</cffunction>

	<cffunction name="setMontantCalcule" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.montantCalcule = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	
	<cffunction name="getMontantCalculeFacture" output="false" access="public" returntype="any">
		<cfreturn variables.montantCalculeFacture>
	</cffunction>

	<cffunction name="setMontantCalculeFacture" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.montantCalculeFacture = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	
	<cffunction name="getVisee" output="false" access="public" returntype="any">
		<cfreturn variables.visee>
	</cffunction>

	<cffunction name="setVisee" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.visee = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getViseeAna" output="false" access="public" returntype="any">
		<cfreturn variables.ViseeAna>
	</cffunction>

	<cffunction name="setViseeAna" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ViseeAna = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getControlee" output="false" access="public" returntype="any">
		<cfreturn variables.Controlee>
	</cffunction>

	<cffunction name="setControlee" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Controlee = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<!--- CTRL INVENTAIRE --->
	<cffunction name="getControleeInv" output="false" access="public" returntype="any">
		<cfreturn variables.ControleeInv>
	</cffunction>

	<cffunction name="setControleeInv" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ControleeInv = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	<!--- FIN CTRL INVENTAIRE --->

	<cffunction name="getExportee" output="false" access="public" returntype="any">
		<cfreturn variables.Exportee>
	</cffunction>

	<cffunction name="setExportee" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Exportee = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="setCommentaireViser" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.commentaireViser = arguments.val>
	</cffunction>

	<cffunction name="getCommentaireViser" output="false" access="public" returntype="any">
		<cfreturn variables.commentaireViser>
	</cffunction>	
	
	<cffunction name="setCommentaireViserAna" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.commentaireViserAna = arguments.val>
	</cffunction>

	<cffunction name="getCommentaireViserAna" output="false" access="public" returntype="any">
		<cfreturn variables.commentaireViserAna>
	</cffunction>
	
	<cffunction name="setCommentaireControler" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.commentaireControler = arguments.val>
	</cffunction>

	<cffunction name="getCommentaireControler" output="false" access="public" returntype="any">
		<cfreturn variables.commentaireControler>
	</cffunction>	
	
	
	<!--- CTRL INVENTAIRE --->
	<cffunction name="setCommentaireControlerInv" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.commentaireControlerInv = arguments.val>
	</cffunction>

	<cffunction name="getCommentaireControlerInv" output="false" access="public" returntype="any">
		<cfreturn variables.commentaireControlerInv>
	</cffunction>	
	<!--- FIN CTRL INVENTAIRE --->
	
	<cffunction name="setCommentaireExporter" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.commentaireExporter = arguments.val>
	</cffunction>

	<cffunction name="getCommentaireExporter" output="false" access="public" returntype="any">
		<cfreturn variables.commentaireExporter>
	</cffunction>	
	
	<cffunction name="getDateControler" output="false" access="public" returntype="any">
		<cfreturn variables.dateControler>
	</cffunction>

	<cffunction name="setDateControler" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateControler = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getDateViser" output="false" access="public" returntype="any">
		<cfreturn variables.dateViser>
	</cffunction>

	<cffunction name="setDateViser" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateViser = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>
	
	
	<cffunction name="setDateViserAna" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateViserAna = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getDateViserAna" output="false" access="public" returntype="any">
		<cfreturn variables.dateViserAna>
	</cffunction>
		
	
	<cffunction name="getDateExporter" output="false" access="public" returntype="any">
		<cfreturn variables.dateExporter>
	</cffunction>

	<cffunction name="setDateExporter" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DateExporter = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>
	
	<cffunction name="setProprietes" output="false" access="public" returntype="void">
		<cfargument name="proprietes" type="any" required="true">
		<cfscript>
			/*setOperateurId(proprietes.OPERATEURID);
			setOperateurLibelle(proprietes.NOM);
			setNumero(proprietes.NUMERO_FACTURE);
			setCompteFacturation(proprietes.COMPTE_FACTURATION);
			setPeriodeId(proprietes.IDINVENTAIRE_PERIODE);
			setDateDebut(proprietes.DATEDEB);
			setDateFin(proprietes.DATEFIN);
			setDateEmission(proprietes.DATE_EMISSION);
			setBY_ZIPCODE(proprietes.BY_ZIPCODE);
			setBY_ADRESSE2(proprietes.BY_ADRESSE2);
			setBY_ADRESSE1(proprietes.BY_ADRESSE1);
			setBY_COMMUNE(proprietes.BY_COMMUNE);
			setBY_CODE_SITE(proprietes.BY_CODE_SITE);
			
			setMontantVerifiable(proprietes.MONTANT_VERIFIABLE);
			setMontantCalcule(proprietes.MONTANT_CALCULE);
			
			
			
			setRefClientId(proprietes.IDREF_CLIENT);
			setLibelle(proprietes.LIBELLE);
			
			
			setVisee(proprietes.BOOL_VISA);
			setControlee(proprietes.BOOL_VALIDE);
			setExportee(proprietes.BOOL_EXPORTEE);*/
						
			//setCommentaireViser(proprietes.COMMENTAIRE_VISA);
			//setCommentaireControler(proprietes.COMMENTAIRE_VALIDE);
			//setCommentaireExporter(proprietes.COMMENTAIRE_EXPORTEE);
						
			//setDateViser(proprietes.DATE_VISA);
			//setDateControler(proprietes.DATE_CONTROLE);
			//setDateExporter(proprietes.DATE_EXPORT);
		</cfscript>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : Retourne l'enssemble des lignes de facturation de la facture
		
		Param in 
		Param out
			LigneFacturation[]
	
	--->
	<cffunction name="getDetail" access="public" output="false" returntype="DetailFacture[]">	
		<cfreturn createObject("component","LigneFacturationGateWay").getListeLignesFacturationFacturePerimetre(THIS)>
	</cffunction>
	
		
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : Met Ã  jour l'ï¿½tat visee/non visee de la facture
		Param in 			
		Param out
			Number 1 si ok sinon -1
	
	--->
	<cffunction name="updateEtatVise" access="public" output="false" returntype="numeric">		
		<!--- 
			PROCEDURE			
				PKG_CV_GRCL_FACTURATION.CF_VISA
			PARAM
				in
				p_idinventaire_periode 	INTEGER
				p_etat 					INTEGER
				p_commentaires			VARCHAR2
				out
				p_retour 				INTEGER
		 --->
 		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.CF_VISA">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#this.getPeriodeId()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_etat" value="#this.getVisee()#"/>		
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_comExporter" value="#this.getCommentaireViser()#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : Met Ã  jour l'ï¿½tat visee/non visee - analytiquement parlant. 
		Param in 			
		Param out
			Number 1 si ok sinon -1
	
	--->
	<cffunction name="updateEtatViseAna" access="public" output="false" returntype="numeric">		
		<!--- 
			PROCEDURE			
				PKG_CV_GRCL_FACTURATION.CF_VISAANA
			PARAM
				in
				p_idinventaire_periode 	INTEGER
				p_etat 					INTEGER
				p_commentaires			VARCHAR2
				out
				p_retour 				INTEGER
		 --->
 		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.CF_VISAANA">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#this.getPeriodeId()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_etat" value="#this.getViseeAna()#"/>		
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_comExporter" value="#this.getCommentaireViserAna()#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/29/2007
		
		Description : Met Ã  jour l'ï¿½tat controlee/non controlee de la facture
		Param in 			
		Param out
			Number 1 si ok sinon -1
	
	--->
	<cffunction name="updateEtatControle" access="public" output="false" returntype="numeric">		
		<!--- 
			PROCEDURE			
				PKG_CV_GRCL_FACTURATION.CF_VALIDE
			PARAM
				in
				p_idinventaire_periode 	INTEGER
				p_etat 					INTEGER
				p_commentaires			VARCHAR2
				out
				p_retour 				INTEGER
		 --->
 		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.CF_VALIDE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#this.getPeriodeId()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_etat" value="#this.getControlee()#"/>		
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_comExporter" value="#this.getCommentaireControler()#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 06/04/2011
		
		Description : Met à  jour l'état controlee inventaire/non controlee inventaire de la facture
		Param in 			
		Param out
			Number 1 si ok sinon -1	
	--->
	<cffunction name="updateEtatControleInventaire" access="public" output="false" returntype="numeric" hint="Met à  jour l'état controlee inventaire/non controlee inventaire de la facture">		
		<!--- 
			PROCEDURE			
				PKG_CV_GRCL_FACTURATION.CF_VALIDE_INV
			PARAM
				in
				p_idinventaire_periode 	INTEGER
				p_etat 					INTEGER
				p_commentaires			VARCHAR2
				out
				p_retour 				INTEGER
		 --->
 		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.CF_VALIDE_INVENTAIRE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#this.getPeriodeId()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_etat" value="#this.getControleeInv()#"/>		
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_comExporter" value="#this.getCommentaireControlerInv()#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/29/2007
		
		Description : Met Ã  jour l'ï¿½tat exportee/non exportee de la facture 0 = non exportï¿½e 2 = bon pour export 1 = exportï¿½e
		Param in 			
		Param out
			Number 1 si ok sinon -1
	
	--->
	<cffunction name="updateEtatExporte" access="public" output="false" returntype="numeric">		
		<!--- 
			PROCEDURE			
				PKG_CV_GRCL_FACTURATION.CF_EXPORTEE
			PARAM
				in
				p_idinventaire_periode 	INTEGER
				p_etat 					INTEGER
				p_commentaires			VARCHAR2
				out
				p_retour 				INTEGER
		 --->
 		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.CF_EXPORTEE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#this.getPeriodeId()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_etat" value="#this.getExportee()#"/>		
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_comExporter" value="#this.getCommentaireExporter()#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/29/2007
		
		Description : Exporte la facture au format CSV
		Param in 			
		Param out
			string le nom du fichier si ok sinon 'erreur'
	
	--->
	<cffunction name="exporterCSV" access="public" output="true" returntype="any">
		<cfargument name="idgroupe" type="numeric" required="true">
		 
		<cftry>
				<cfset export = createObject("component","ExportStrategy#idgroupe#")>
			<cfcatch>
				<cfset export = createObject("component","ExportStrategy")>
			</cfcatch>		
		</cftry>
		<cfreturn export.exporterCSV(this)>		
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/11/2007
		
		Description : Exporte les ressources hors inventaire au format CVS
		
		Param 
		in typeTheme string
		out fileName / 'error' string
	--->
	<cffunction name="exporterRessourcesHICSV" access="public" output="false" returntype="any">
		<cfargument name="typeTheme" type="string" required="true">
		<cfsetting enablecfoutputonly="true">
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"RessourcesHI_"&this.getNumero()&".csv">				
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset aDetail = this.getDetailRessourcesHorsInventaire(typeTheme)>				
				<cfset NewLine = Chr(13) & Chr(10)>
				<cfset space = Chr(13) & Chr(10)> 
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="utf-8">
				<cfcontent type="text/plain"> 
				<cfoutput>type;produit;compte facturation;sous compte;ligne;quantite;volume;prix unitaire;montant#NewLine#</cfoutput><cfloop index="i" from="1" to="#arrayLen(aDetail)#">
			    <cfoutput>#TRIM(aDetail[i].gettypeTheme())#;#TRIM(aDetail[i].getlibelleProduit())#;#aDetail[i].getcompteFacturation()#;#aDetail[i].getsousCompte()#;#aDetail[i].getsousTete()#;#TRIM(LSNumberFormat(aDetail[i].getquantite(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getvolume(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getprixUnitaire(),"________.____"))#;#TRIM(LSNumberFormat(aDetail[i].getmontant(),"________.____"))#;#NewLine#</cfoutput></cfloop>
			</cfsavecontent>			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="utf-8"
																						addnewline="true" fixnewline="true" output="#contentObj#">		
			<cfreturn "#fileName#">
			<cfcatch type="any">
				<cfreturn "error">	
			</cfcatch>
		</cftry>
		<cfreturn this.getDetailRessourcesHorsInventaire(typeTheme)>
	</cffunction>
	
	<!---		
		Auteur : samuel.divioka				
		Date : 12/11/2007		
		Description : Retourne l'enssemble des ressources hors inventaire de la facture		
		Param in 		
			typeTheme string
		Param out
			Ressources[]
	--->
	<cffunction name="getDetailRessourcesHorsInventaire" access="public" output="false" returntype="any">
		<cfargument name="typeTheme" type="string" required="true">	
		<cfreturn createObject("component","RessourceGateWay").getDetailRessourcesHorsInventaire(THIS,typeTheme)>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/11/2007
		
		Description : Retourne le montant des ressources hors inventaire pour une facture
	
	--->
	<cffunction access="remote" name="getMontantRessourcesHorsInventaire" returntype="array">		
		<cfreturn createObject("component","RessourceGateWay").getMontantRessourcesHorsInventaire(THIS)>
	</cffunction>
	
	<!---
		Auteur : samuel.divioka
		
		Date : 23/04/2008
		
		Description : eclate la facture dans une organisation
	--->
	<cffunction name="detailFactureByOrga" access="remote" output="true" returntype="query">		
		<cfargument name="idOrga" type="numeric" required="true">		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.ECLATE_FACTURE_BY_ORGA_V2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idorganisation" value="#idOrga#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idFacture" value="#this.getPeriodeId()#"/>		
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
			<cfprocresult name="qResult">
		</cfstoredproc>

		<cfreturn qResult>
	</cffunction>
	
	<!---
		Auteur : daisy.bachelin
		 
		Date : 23/04/2010
		
		Description : eclate la facture dans une organisation de facon plus détaillée
	--->
	<cffunction name="detailFactureByOrga_ligne" access="remote" output="true" returntype="query">		
		<cfargument name="idOrga" type="numeric" required="true">		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.ECLATE_FACTURE_BY_ORGA_LIG_V2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idorganisation" value="#idOrga#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idFacture" value="#this.getPeriodeId()#"/>		
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction>
	
	
	<!---
		Auteur : samuel.divioka
		
		Date : 23/04/2008
		
		Description : total de la facture dans une organisation
	--->
	<cffunction name="totalFactureByOrga" access="remote" output="true" returntype="query">		
		<cfargument name="idOrga" type="numeric" required="true">		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.TOTAL_FACTURE_BY_ORGA_V2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idorganisation" value="#idOrga#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idFacture" value="#this.getPeriodeId()#"/>		
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction>
	
	
	<!--- ####################################### DANS INVENTAIRE ##################################### --->
	
	
	<!---		
		Auteur : samuel.divioka				
		Date : 12/11/2007		
		Description : Retourne l'enssemble des ressources hors inventaire de la facture		
		Param in 		
			typeTheme string
		Param out
			Ressources[]
	--->
	<cffunction name="getMontantRessourcesInventaireNonFacturee" access="public" output="false" returntype="Ressource">
		<cfreturn createObject("component","RessourceGateWay").getMontantRessourcesInventaireNonFacturee(THIS)>
	</cffunction>
	
	<cffunction name="getDetailRessourcesInventaireNonFacturee" access="public" output="false" returntype="Ressource[]">
		<cfargument name="typeTheme" type="string" required="true">	
		<cfreturn createObject("component","RessourceGateWay").getDetailRessourcesInventaireNonFacturee(THIS,typeTheme)>
	</cffunction>
	
	<cffunction name="exporterRessourcesICSV" access="public" output="true" returntype="any">
		<cfargument name="typeTheme" type="string" required="true">		
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"RessourcesI_"&this.getNumero()&".csv">				
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset aDetail = this.getDetailRessourcesInventaireNonFacturee(typeTheme)>				
				<cfset NewLine = Chr(13) & Chr(10)>
				<cfset space = Chr(13) & Chr(10)>		
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="utf-8">
				<cfcontent type="text/plain">		
				<cfoutput>type;produit;compte facturation;ligne;prix unitaire#NewLine#</cfoutput><cfloop index="i" from="1" to="#arrayLen(aDetail)#">
			    <cfoutput>#TRIM(aDetail[i].gettypeTheme())#;#TRIM(aDetail[i].getlibelleProduit())#;#this.getcompteFacturation()#;#aDetail[i].getsousTete()#;#TRIM(LSNumberFormat(aDetail[i].getprixUnitaire(),"________.____"))#;#NewLine#</cfoutput></cfloop>
			</cfsavecontent>
			<!--- Crï¿½ation du fichier CSV --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="utf-8"
					addnewline="true" fixnewline="true" output="#contentObj#">		
			<cfreturn "#fileName#">
		<cfcatch type="any">					
			<cfreturn "error">	
		</cfcatch>
		</cftry>
	</cffunction>
	<!--- ################################## FIN  DANS INVENTAIRE ##################################### --->
</cfcomponent>
