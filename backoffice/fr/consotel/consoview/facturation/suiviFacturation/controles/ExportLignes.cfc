<cfcomponent output="false" alias="fr.consotel.consoview.facturation.suiviFacturation.controles.ExportLignes"  extends="fr.consotel.consoview.util.publicreportservice.PublicReportServiceV2">
	
	
	<cffunction name="displayReport" output="false" access="public" returntype="any">
		<cfargument name="idinventaire_periode" required="true" type="Numeric">
		<cfargument name="idorga" required="true" type="Numeric">
		<cfset createRunReportRequest("consoview","public","/consoview/facturation/suiviFacturation/controle/exportLignesNonAffect/exportLignesNonAffect.xdo","cv","pdf")>
		<cfset AddRunReportParameter("P_IDRACINE_MASTER","#session.perimetre.idracine_master#")>
		<cfset AddRunReportParameter("P_IDGROUPE_CLIENT","#idorga#")>
		<cfset AddRunReportParameter("P_IDINVENTAIRE_PERIODE","#idinventaire_periode#")>
		<cfset runReport()>
	</cffunction>
	
</cfcomponent>