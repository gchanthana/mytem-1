<cfcomponent output="false" alias="fr.consotel.consoview.facturation.suiviFacturation.controles.VersionDeTarif">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="idProduit" type="numeric" default="0">
	<cfproperty name="libelleProduit" type="string" default="">
	<cfproperty name="themeId" type="numeric" default="0">
	<cfproperty name="themeLibelle" type="string" default="">
	<cfproperty name="operateurId" type="numeric" default="0">
	<cfproperty name="operateurLibelle" type="string" default="">
	<cfproperty name="typeTheme" type="string" default="">
	<cfproperty name="typeTrafic" type="string" default="">
	<cfproperty name="typeTraficId" type="string" default="">
	<cfproperty name="poid" type="numeric" default="0">
	<cfproperty name="pourControle" type="boolean" default="1">
	<cfproperty name="idVersionTarif" type="numeric" default="0">
	<cfproperty name="prixUnitaire" type="numeric" default="0">
	<cfproperty name="prixRemise" type="numeric" default="0">
	<cfproperty name="remiseContrat" type="numeric" default="0">
	<cfproperty name="dateDebut" type="date" default="">
	<cfproperty name="dateFin" type="date" default="">
	<cfproperty name="deviseLibelle" type="string" default="">
	<cfproperty name="deviseId" type="numeric" default="0">
	<cfproperty name="frequenceFactureLibelle" type="string" default="">
	<cfproperty name="frequenceFactureLibelleId" type="numeric" default="0">
	<cfproperty name="qteMini" type="numeric" default="0">

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idProduit = 0;
		variables.libelleProduit = "";
		variables.themeId = 0;
		variables.themeLibelle = "";
		variables.operateurId = 0;
		variables.operateurLibelle = "";
		variables.typeTheme = "";
		variables.typeTrafic = "";
		variables.typeTraficId = "";
		variables.poid = 0;
		variables.pourControle = 1;
		variables.idVersionTarif = 0;
		variables.prixUnitaire = 0;
		variables.prixRemise = 0;
		variables.remiseContrat = 0;
		variables.dateDebut = now();
		variables.dateFin = "";
		variables.deviseLibelle = "";
		variables.deviseId = 0;
		variables.frequenceFactureLibelle = "";
		variables.frequenceFactureLibelleId = 0;
		variables.qteMini = 0;
	</cfscript>

	<cffunction name="init" output="false" returntype="VersionDeTarif">
		<cfreturn this>
	</cffunction>
	<cffunction name="getIdProduit" output="false" access="public" returntype="any">
		<cfreturn variables.idProduit>
	</cffunction>

	<cffunction name="setIdProduit" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.idProduit = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLibelleProduit" output="false" access="public" returntype="any">
		<cfreturn variables.LibelleProduit>
	</cffunction>

	<cffunction name="setLibelleProduit" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.LibelleProduit = arguments.val>
	</cffunction>

	<cffunction name="getThemeId" output="false" access="public" returntype="any">
		<cfreturn variables.ThemeId>
	</cffunction>

	<cffunction name="setThemeId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ThemeId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getThemeLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.ThemeLibelle>
	</cffunction>

	<cffunction name="setThemeLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.ThemeLibelle = arguments.val>
	</cffunction>

	<cffunction name="getOperateurId" output="false" access="public" returntype="any">
		<cfreturn variables.OperateurId>
	</cffunction>

	<cffunction name="setOperateurId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.OperateurId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getoperateurLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.operateurLibelle>
	</cffunction>

	<cffunction name="setoperateurLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.operateurLibelle = arguments.val>
	</cffunction>

	<cffunction name="getTypeTheme" output="false" access="public" returntype="any">
		<cfreturn variables.TypeTheme>
	</cffunction>

	<cffunction name="setTypeTheme" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.TypeTheme = arguments.val>
	</cffunction>

	<cffunction name="getTypeTrafic" output="false" access="public" returntype="any">
		<cfreturn variables.TypeTrafic>
	</cffunction>

	<cffunction name="setTypeTrafic" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.TypeTrafic = arguments.val>
	</cffunction>

	<cffunction name="getTypeTraficId" output="false" access="public" returntype="any">
		<cfreturn variables.TypeTraficId>
	</cffunction>

	<cffunction name="setTypeTraficId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.TypeTraficId = arguments.val>
	</cffunction>

	<cffunction name="getPoid" output="false" access="public" returntype="any">
		<cfreturn variables.Poid>
	</cffunction>

	<cffunction name="setPoid" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Poid = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPourControle" output="false" access="public" returntype="any">
		<cfreturn variables.PourControle>
	</cffunction>

	<cffunction name="setPourControle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsBoolean(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PourControle = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid boolean"/>
		</cfif>
	</cffunction>

	<cffunction name="getIdVersionTarif" output="false" access="public" returntype="any">
		<cfreturn variables.IdVersionTarif>
	</cffunction>

	<cffunction name="setIdVersionTarif" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IdVersionTarif = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPrixUnitaire" output="false" access="public" returntype="any">
		<cfreturn variables.prixUnitaire>
	</cffunction>

	<cffunction name="setPrixUnitaire" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.prixUnitaire = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getPrixRemise" output="false" access="public" returntype="any">
		<cfreturn variables.prixRemise>
	</cffunction>

	<cffunction name="setPrixRemise" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.prixRemise = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getRemiseContrat" output="false" access="public" returntype="any">
		<cfreturn variables.RemiseContrat>
	</cffunction>

	<cffunction name="setRemiseContrat" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.RemiseContrat = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateDebut" output="false" access="public" returntype="any">
		<cfreturn variables.DateDebut>
	</cffunction>

	<cffunction name="setDateDebut" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DateDebut = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateFin" output="false" access="public" returntype="any">
		<cfreturn variables.DateFin>
	</cffunction>

	<cffunction name="setDateFin" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DateFin = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDeviseLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.DeviseLibelle>
	</cffunction>

	<cffunction name="setDeviseLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.DeviseLibelle = arguments.val>
	</cffunction>

	<cffunction name="getDeviseId" output="false" access="public" returntype="any">
		<cfreturn variables.DeviseId>
	</cffunction>

	<cffunction name="setDeviseId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DeviseId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getFrequenceFactureLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.FrequenceFactureLibelle>
	</cffunction>

	<cffunction name="setFrequenceFactureLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.FrequenceFactureLibelle = arguments.val>
	</cffunction>

	<cffunction name="getFrequenceFactureLibelleId" output="false" access="public" returntype="any">
		<cfreturn variables.FrequenceFactureLibelleId>
	</cffunction>

	<cffunction name="setFrequenceFactureLibelleId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.FrequenceFactureLibelleId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getQteMini" output="false" access="public" returntype="any">
		<cfreturn variables.QteMini>
	</cffunction>

	<cffunction name="setQteMini" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.QteMini = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="save" output="false" access="public" returntype="numeric">
		<cfif this.getIdVersionTarif() gt 0 >	
			<cfif update() lt 0>			
				<cfreturn -1>	
			<cfelse>
				<cfreturn 1>	
			</cfif>	
		<cfelse>
			<cfreturn create()>	
		</cfif>		
	</cffunction> 
		
	<cffunction name="update" output="false" access="public" returntype="numeric">
      <!---  PROCEDURE pkg_cv_grcl_facturation.maj_version_Ctl (     p_idversion_grp_ctl                      IN INTEGER,
						                                             p_tarif_brut                             IN NUMBER,
						                                             p_tarif_remise                           IN NUMBER,
						                                             p_pct_remise                             IN NUMBER,
						                                             p_retour                                 OUT INTEGER) 
	 --->
	  	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.maj_version_Ctl">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idversion_grp_ctl" value="#getIdVersionTarif()#"/>	
			<cfprocparam cfsqltype="CF_SQL_FLOAT" type="in" variable="p_tarif_brut" value="#getPrixUnitaire()#"/>	
			<cfprocparam cfsqltype="CF_SQL_FLOAT" type="in" variable="p_tarif_remise" value="#getPrixRemise()#"/>	
			<cfprocparam cfsqltype="CF_SQL_FLOAT" type="in" variable="p_pct_remise" value="#getRemiseContrat()#"/>				
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour"/>	
		</cfstoredproc>		
		<cfreturn p_retour>
	</cffunction> 
	

	<cffunction name="create" output="false" access="public" returntype="numeric">
	 
		<!--- 
		ajouter_version_Ctl (		p_idgrp_ctl						IN INTEGER,
													p_idracine						IN INTEGER,
													p_datedeb						IN DATE,	 
													p_tarif_brut					IN NUMBER,
													p_tarif_remise					IN NUMBER,
													p_pct_remise					IN NUMBER,
													p_retour							OUT INTEGER
		PROCEDURE pkg_cv_grcl_facturation.ajouter_version_Ctl (          p_idgrp_ctl                                   IN INTEGER,
								                                               p_idracine                                    IN INTEGER,
								                                               p_datedeb                                     IN DATE,       
								                                               p_tarif_brut                                  IN NUMBER,
								                                               p_tarif_remise                                IN NUMBER,
								                                               p_pct_remise                                  IN NUMBER,
								                                               p_retour                                      OUT INTEGER)  --->
	  	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_V3.ajouter_version_Ctl">		
		  	<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idgrp_ctl" value="#getIdProduit()#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idracine" value="#session.perimetre.ID_GROUPE#"/>
			<cfprocparam cfsqltype="CF_SQL_DATE" type="in" variable="p_datedeb" value="#getDateDebut()#"/>
			<cfprocparam cfsqltype="CF_SQL_FLOAT" type="in" variable="p_tarif_brut" value="#getPrixUnitaire()#"/>	
			<cfprocparam cfsqltype="CF_SQL_FLOAT" type="in" variable="p_tarif_remise" value="#getPrixRemise()#"/>	
			<cfprocparam cfsqltype="CF_SQL_FLOAT" type="in" variable="p_pct_remise" value="#getRemiseContrat()#"/>				
			
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour"/>	
		</cfstoredproc>		
		<cfreturn p_retour>				
	</cffunction> 
	
	<!--- TODO : PKG_CV_FACTURATION.DELETE_VERSION --->
	<cffunction name="delete" output="false" access="public" returntype="numeric">
		<!--- PROCEDURE pkg_cv_grcl_facturation.supprimer_version_Ctl (  p_idversion_grp_ctl                     IN INTEGER,
                                          								 p_retour                                OUT INTEGER)  --->
	  	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.supprimer_version_Ctl">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idversion_grp_ctl" value="#getIdVersionTarif()#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour"/>	
		</cfstoredproc>		
		<cfreturn p_retour>	
	</cffunction> 
	
	
	<cffunction name="setProprietes" output="false" access="public" returntype="void">
		<cfargument name="proprietes" type="any" required="true">
		<cfscript>
		//Initialize the CFC with the properties values.
			setIdProduit(proprietes.IDPRODUIT_CLIENT);
			setLibelleProduit(proprietes.LIBELLE_PRODUIT);
			setThemeId(proprietes.IDTHEME_PRODUIT);
			setThemeLibelle(proprietes.THEME_LIBELLE);
			setOperateurId(proprietes.OPERATEURID);
			setOperateurLibelle(proprietes.OPNOM);
			setTypeTheme(proprietes.TYPE_THEME);
			//settypeTrafic(proprietes.);
			//settypeTraficId(proprietes.);
			setPoid(proprietes.POIDS);
			setPourControle(proprietes.FLAGCLIENT);
			setIdVersionTarif(proprietes.IDPROD_CLIENT_VERSION);
			setPrixUnitaire(proprietes.PRIX_UNIT);
			//setPrixRemise(proprietes.PRIX_REMISE);
			setRemiseContrat(proprietes.REMISE);
			setDateDebut(proprietes.DATEDEBUTVALIDITE);
			setDateFin(proprietes.DATEFINVALIDITE);
			setDeviseLibelle(proprietes.DEVISE);
			setDeviseId(proprietes.IDDEVISE);
			setFrequenceFactureLibelle(proprietes.FREQUENCE_FACTURE);
			setQteMini(proprietes.QTE_MIN);		
		</cfscript>
	</cffunction>

</cfcomponent>