<cfset REPORT_QUERY="NONE">
<cfset ID_PERIMETRE=0> 
<cfset DATASOURCE_DB1="ROCOFFRE-DB1">
<cfset filename="Ressources-Hors-Inventaire-Facturees_" & replace(session.perimetre.raison_sociale,' ','_',"all")>
<cfif UCase(SESSION.PERIMETRE.TYPE_PERIMETRE) EQ "GROUPE">
	<cfset REPORT_QUERY="PKG_CV_GRCL_FACTURATION_V3.DET_RES_HORSINV_FACTUREE_V2">
	<cfif FORM.QUERY_TYPE EQ "DANS_INVENTAIRE">
		<cfset REPORT_QUERY="PKG_CV_GRCL_FACTURATION_V3.RES_INV_NONFACTUREE">
		<cfset filename="Ressources-Inventaire-Non-Facturees_" & replace(session.perimetre.raison_sociale,' ','_',"all")>
	</cfif>
	<cfset ID_PERIMETRE=SESSION.PERIMETRE.ID_GROUPE>
<cfelseif UCase(SESSION.PERIMETRE.TYPE_PERIMETRE) EQ "GROUPELIGNE">
	<cfset REPORT_QUERY="PKG_CV_GLIG_FACTURATION.DET_RES_HORSINV_FACTUREE_V2">
	<cfif FORM.QUERY_TYPE EQ "DANS_INVENTAIRE">
		<cfset REPORT_QUERY="PKG_CV_GLIG_FACTURATION_V3.RES_INV_NONFACTUREE">
		<cfset filename="Ressources-Inventaire-Facturees_" & replace(session.perimetre.raison_sociale,' ','_',"all")>
	</cfif>
	<cfset ID_PERIMETRE=SESSION.PERIMETRE.ID_PERIMETRE>
<cfelse>
	<cfoutput>
		<strong>P&eacute;rim&egrave;tre non pris en charge.<br>Contacter le support ConsoView.</strong>
	</cfoutput>
</cfif>
<cfif REPORT_QUERY NEQ "NONE">
	<cfif (UCase(SESSION.PERIMETRE.TYPE_PERIMETRE) EQ "GROUPELIGNE") AND (FORM.QUERY_TYPE EQ "HORS_INVENTAIRE")>
		<cfstoredproc datasource="#DATASOURCE_DB1#" procedure="#REPORT_QUERY#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#ID_PERIMETRE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#FORM.IDINVENTAIRE_PERIODE#"/>
			<cfprocresult name="qListeRessources"/>
		</cfstoredproc>
	<cfelse>
		<cfstoredproc datasource="#DATASOURCE_DB1#" procedure="#REPORT_QUERY#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#ID_PERIMETRE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#FORM.IDINVENTAIRE_PERIODE#"/>
			<cfprocresult name="qListeRessources"/>
		</cfstoredproc>
	</cfif>
	<cfif (UCase(FORM.TYPE_THEME) EQ "ABONNEMENTS") OR (UCase(FORM.TYPE_THEME) EQ "CONSOMMATIONS")>
		<cfquery name="qListe" dbtype="query">
			select * from qListeRessources where UPPER(TYPE_THEME)='#Ucase(FORM.TYPE_THEME)#'
		</cfquery>
		<cfset qListeRessources=qListe>
	</cfif>
	<cfset exportService=createObject("component","fr.consotel.consoview.api.reporting.export.ExportService")>
	<cfset exportService.exportQueryToText(qListeRessources,filename & ".csv")>
</cfif>