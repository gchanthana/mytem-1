<cfcomponent output="false" alias="fr.consotel.consoview.facturation.suiviFacturation.controles.LigneFacturation">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->	
	<cfproperty name="id" type="numeric" default="0">
	<cfproperty name="idProduit" type="numeric" default="0">
	<cfproperty name="libelleProduit" type="string" default="">
	<cfproperty name="themeId" type="numeric" default="0">
	<cfproperty name="themeLibelle" type="string" default="">
	<cfproperty name="operateurId" type="numeric" default="0">
	<cfproperty name="operateurLibelle" type="string" default="">
	<cfproperty name="typeTheme" type="string" default="">
	<cfproperty name="typeTrafic" type="string" default="">
	<cfproperty name="poids" type="numeric" default="0">
	<cfproperty name="pourControle" type="boolean" default="1">
	<cfproperty name="prixUnitaire" type="numeric" default="0">
	<cfproperty name="prixUnitaireRemise" type="numeric" default="0">
	<cfproperty name="remiseContrat" type="numeric" default="0">
	<cfproperty name="dateDebutVersion" type="date" default="">
	<cfproperty name="dateFinVersion" type="date" default="">
	<cfproperty name="dateDebut" type="date" default="">
	<cfproperty name="dateFin" type="date" default="">
	<cfproperty name="qte" type="numeric" default="0">
	<cfproperty name="montantTotal" type="numeric" default="0">
	<cfproperty name="montantCalculeBrut" type="numeric" default="0">
	<cfproperty name="montantCalculeRemise" type="numeric" default="0">
	<cfproperty name="difference" type="numeric" default="0">
	<cfproperty name="poidsDifference" type="numeric" default="0">
	<cfproperty name="montantMoyen" type="numeric" default="0">
	<cfproperty name="duree" type="numeric" default="0">
	<cfproperty name="boolPublic" type="numeric" default="0">
	<cfproperty name="typeTarif" type="numeric" default="0">
	<cfproperty name="libelleTypeTarif" type="string" default="">
	<cfproperty name="libelleSourceTarif" type="string" default="">
	<cfproperty name="libelleCompte" type="string" default="">
	<cfproperty name="libelleSousCompte" type="string" default="">

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.id = 0;
		variables.idProduit = 0;
		variables.libelleProduit = "";
		variables.themeId = 0;
		variables.themeLibelle = "";
		variables.operateurId = 0;
		variables.operateurLibelle = "";
		variables.typeTheme = "";
		variables.typeTrafic = "";
		variables.poids = 0;
		variables.pourControle = 1;
		variables.prixUnitaire = 0;
		variables.prixUnitaireRemise = 0;
		variables.duree = 0;
		variables.remiseContrat = 0;
		variables.dateDebutVersion = "";
		variables.dateFinVersion = "";
		variables.dateDebut = "";
		variables.dateFin = "";
		variables.qte = 0;
		variables.montantTotal = 0;
		variables.montantMoyen = 0;
		variables.montantCalculeBrut = 0;
		variables.montantCalculeRemise = 0;
		variables.difference = 0;
		variables.poidsDifference = 0;
		variables.boolPublic = 0;
		variables.typeTarif = -1;
		variables.libelleTypeTarif ="-";
		variables.libelleSourceTarif ="-";
		variables.libelleCompte ="-";
		variables.libelleSousCompte ="-";
		
	</cfscript>

	<cffunction name="init" output="false" returntype="LigneFacturation">
		<cfreturn this>
	</cffunction>
	
	<cffunction name="getId" output="false" access="public" returntype="any">
		<cfreturn variables.Id>
	</cffunction>

	<cffunction name="setId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Id = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIdProduit" output="false" access="public" returntype="any">
		<cfreturn variables.IdProduit>
	</cffunction>

	<cffunction name="setIdProduit" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IdProduit = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLibelleProduit" output="false" access="public" returntype="any">
		<cfreturn variables.libelleProduit>
	</cffunction>

	<cffunction name="setLibelleProduit" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.libelleProduit = arguments.val>
	</cffunction>

	<cffunction name="getThemeId" output="false" access="public" returntype="any">
		<cfreturn variables.themeId>
	</cffunction>

	<cffunction name="setThemeId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.themeId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getThemeLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.themeLibelle>
	</cffunction>

	<cffunction name="setThemeLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.ThemeLibelle = arguments.val>
	</cffunction>

	<cffunction name="getOperateurId" output="false" access="public" returntype="any">
		<cfreturn variables.operateurId>
	</cffunction>

	<cffunction name="setOperateurId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.operateurId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getOperateurLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.operateurLibelle>
	</cffunction>

	<cffunction name="setOperateurLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.operateurLibelle = arguments.val>
	</cffunction>

	<cffunction name="getTypeTheme" output="false" access="public" returntype="any">
		<cfreturn variables.typeTheme>
	</cffunction>

	<cffunction name="setTypeTheme" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.typeTheme = arguments.val>
	</cffunction>

	<cffunction name="getTypeTrafic" output="false" access="public" returntype="any">
		<cfreturn variables.typeTrafic>
	</cffunction>

	<cffunction name="setTypeTrafic" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.typeTrafic = arguments.val>
	</cffunction>

	<cffunction name="getPoids" output="false" access="public" returntype="any">
		<cfreturn variables.poids>
	</cffunction>

	<cffunction name="setPoids" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.poids = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPourControle" output="false" access="public" returntype="any">
		<cfreturn variables.pourControle>
	</cffunction>

	<cffunction name="setPourControle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsBoolean(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.pourControle = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid boolean"/>
		</cfif>
	</cffunction>

	<cffunction name="getPrixUnitaire" output="false" access="public" returntype="any">
		<cfreturn variables.prixUnitaire>
	</cffunction>

	<cffunction name="setPrixUnitaire" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.prixUnitaire = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getPrixUnitaireRemise" output="false" access="public" returntype="any">
		<cfreturn variables.prixUnitaireRemise>
	</cffunction>

	<cffunction name="setPrixUnitaireRemise" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.prixUnitaireRemise = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getRemiseContrat" output="false" access="public" returntype="any">
		<cfreturn variables.remiseContrat>
	</cffunction>

	<cffunction name="setRemiseContrat" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.remiseContrat = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateDebutVersion" output="false" access="public" returntype="any">
		<cfreturn variables.dateDebutVersion>
	</cffunction>

	<cffunction name="setDateDebutVersion" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateDebutVersion = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateFinVersion" output="false" access="public" returntype="any">
		<cfreturn variables.dateFinVersion>
	</cffunction>

	<cffunction name="setDateFinVersion" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateFinVersion = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateDebut" output="false" access="public" returntype="any">
		<cfreturn variables.dateDebut>
	</cffunction>

	<cffunction name="setDateDebut" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateDebut = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateFin" output="false" access="public" returntype="any">
		<cfreturn variables.dateFin>
	</cffunction>

	<cffunction name="setDateFin" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateFin = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getQte" output="false" access="public" returntype="any">
		<cfreturn variables.qte>
	</cffunction>

	<cffunction name="setQte" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.qte = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMontantTotal" output="false" access="public" returntype="any">
		<cfreturn variables.montantTotal>
	</cffunction>

	<cffunction name="setMontantTotal" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.montantTotal = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMontantMoyen" output="false" access="public" returntype="any">
		<cfreturn variables.montantMoyen>
	</cffunction>

	<cffunction name="setMontantMoyen" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.montantMoyen = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getDuree" output="false" access="public" returntype="any">
		<cfreturn variables.duree>
	</cffunction>

	<cffunction name="setDuree" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.duree = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getMontantCalculeBrut" output="false" access="public" returntype="any">
		<cfreturn variables.montantCalculeBrut>
	</cffunction>

	<cffunction name="setMontantCalculeBrut" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.montantCalculeBrut = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMontantCalculeRemise" output="false" access="public" returntype="any">
		<cfreturn variables.montantCalculeRemise>
	</cffunction>

	<cffunction name="setMontantCalculeRemise" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.montantCalculeRemise = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDifference" output="false" access="public" returntype="any">
		<cfreturn variables.difference>
	</cffunction>

	<cffunction name="setDifference" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.difference = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPoidsDifference" output="false" access="public" returntype="any">
		<cfreturn variables.poidsDifference>
	</cffunction>

	<cffunction name="setPoidsDifference" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.poidsDifference = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getBoolPublic" output="false" access="public" returntype="numeric">
		<cfreturn variables.boolPublic>
	</cffunction>

	<cffunction name="setBoolPublic" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.boolPublic = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getTypeTarif" output="false" access="public" returntype="numeric">
		<cfreturn variables.typeTarif>
	</cffunction>

	<cffunction name="setTypeTarif" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.typeTarif = arguments.val>
		<cfif arguments.val EQ 1>
			<cfset variables.libelleTypeTarif = "Tarif Public">
			<cfset variables.libelleSourceTarif = "">
		</cfif>
		<cfif arguments.val EQ 2>
			<cfset variables.libelleTypeTarif = "Tarif client">
			<cfset variables.libelleSourceTarif = "">
		</cfif>
		<cfif arguments.val EQ 3>
			<cfset variables.libelleTypeTarif = "Exception CF">
			<cfset variables.libelleSourceTarif = variables.libelleCompte>
		</cfif>
		<cfif arguments.val EQ 4>
			<cfset variables.libelleTypeTarif = "Exception SCpte">
			<cfset variables.libelleSourceTarif = variables.libelleSousCompte>
		</cfif>
	</cffunction>
	<cffunction name="getLibelleTypeTarif" output="false" access="public" returntype="any">
		<cfreturn variables.libelleTypeTarif>
	</cffunction>

	<cffunction name="setLibelleTypeTarif" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.libelleTypeTarif = arguments.val>
	</cffunction>
	<cffunction name="getLibelleSourceTarif" output="false" access="public" returntype="any">
		<cfreturn variables.libelleSourceTarif>
	</cffunction>

	<cffunction name="setLibelleSourceTarif" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.libelleSourceTarif = arguments.val>
	</cffunction>
	<cffunction name="getLibelleCompte" output="false" access="public" returntype="any">
		<cfreturn variables.libelleCompte>
	</cffunction>

	<cffunction name="setLibelleCompte" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.libelleCompte = arguments.val>
	</cffunction>
	<cffunction name="getLibelleSousCompte" output="false" access="public" returntype="any">
		<cfreturn variables.libelleSousCompte>
	</cffunction>

	<cffunction name="setLibelleSousCompte" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.libelleSousCompte = arguments.val>
	</cffunction>
	
	
</cfcomponent>
