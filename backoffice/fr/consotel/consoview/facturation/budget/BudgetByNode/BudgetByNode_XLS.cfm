<cfheader name="Content-Disposition"
			value="inline;filename=rapport_budgetaire_par_mois#replace(rapportParams.RAISON_SOCIALE,' ','_')#.xls">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="iso-8859-1"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>cedric.rapiera</Author>
  <LastAuthor>cedric.rapiera</LastAuthor>
  <Created>2007-07-12T15:45:06Z</Created>
  <Company>-</Company>
  <Version>11.8132</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12525</WindowHeight>
  <WindowWidth>18795</WindowWidth>
  <WindowTopX>120</WindowTopX>
  <WindowTopY>120</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s24">
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8"/>
  </Style>
  <Style ss:ID="s29">
   <ss:Alignment ss:Vertical="Center" ss:Horizontal="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s30">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s31">
   <ss:Alignment ss:Vertical="Center" ss:Horizontal="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s32">
   <ss:Alignment ss:Vertical="Center" ss:Horizontal="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s33">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s34">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8"/>
  </Style>
  <Style ss:ID="s35">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Budget mensuel par feuilles">
  <Table>
   <Column ss:Width="200"/>
   <Column ss:AutoFitWidth="0" ss:Width="320"/>
	<cfloop from="1" to="#rapportParams.nbMonth#" index="i" step="1">
	   <Column ss:Width="52"/>
	   <Column ss:Width="52"/>
	</cfloop>
   <Column ss:Width="88"/>
   <Column ss:Width="60"/>
   <Row>
    <Cell ss:StyleID="s29"><Data ss:Type="String"></Data></Cell>
    <Cell ss:StyleID="s31"><Data ss:Type="String"></Data></Cell>
	<cfset rapportParams.BUDGET_DATETEMP = parseDateTime(rapportParams.BUDGET_DATEDEB)>
	<cfoutput>
		<cfloop from="1" to="#rapportParams.nbMonth#" index="i" step="1">
		    <Cell ss:StyleID="s31" ss:MergeAcross="1">
		    	<Data ss:Type="String">#lsDateformat(rapportParams.BUDGET_DATETEMP,"MMMM YYYY")#</Data>
		    </Cell>
			<cfset rapportParams.BUDGET_DATETEMP = dateAdd("m",1,rapportParams.BUDGET_DATETEMP)>
		</cfloop>
	</cfoutput>
    <Cell ss:StyleID="s31" ss:MergeAcross="1"><Data ss:Type="String">TOTAL</Data></Cell>
    <Cell ss:StyleID="s32" ss:MergeAcross="2"><Data ss:Type="String">BUDGET</Data></Cell>
   </Row>

   <Row>
    <Cell ss:StyleID="s29"><Data ss:Type="String">Feuille</Data></Cell>
    <Cell ss:StyleID="s31"><Data ss:Type="String">Chemin</Data></Cell>
	<cfoutput>
		<cfloop from="1" to="#rapportParams.nbMonth#" index="i" step="1">
		    <Cell ss:StyleID="s31"><Data ss:Type="String">Rï¿½el</Data></Cell>
		    <Cell ss:StyleID="s31"><Data ss:Type="String">Budget</Data></Cell>
		</cfloop>
	</cfoutput>
    <Cell ss:StyleID="s31"><Data ss:Type="String">Rï¿½el</Data></Cell>
    <Cell ss:StyleID="s31"><Data ss:Type="String">Extrapolï¿½</Data></Cell>
	
    <Cell ss:StyleID="s31"><Data ss:Type="String">Min</Data></Cell>
    <Cell ss:StyleID="s31"><Data ss:Type="String">Budget</Data></Cell>
    <Cell ss:StyleID="s32"><Data ss:Type="String">Max</Data></Cell>
   </Row>

	<cfset nbRow = 0>
	<cfoutput query="qReportData" group="IDGROUPE_CLIENT">
	   <Row>
	    <Cell ss:StyleID="s24"><Data ss:Type="String">#LIBELLE_GROUPE_CLIENT#</Data></Cell>
	    <Cell ss:StyleID="s24"><Data ss:Type="String">#CHEMIN#</Data></Cell>
		<!--- Affectation sur les mois prï¿½sents dans la requï¿½te - Donc boucle sur les MOIS (QUERY) --->
		<cfset totalMontant = 0>
		<cfset totalExtraPol = 0>
		<cfset monthPosition = 0>
		<cfset montantAvg = 0>
		<cfset nbMonthReel = 0>
		<cfoutput group="MOISEMISSION">
			<cfset monthNode = xmlSearch(monthXmlMap,"Periode/mois[@KEY = '#MOISEMISSION#']")>
			<cfset monthNode[1].XmlAttributes.VAL = MONTANT>
			<cfset monthNode[1].XmlAttributes.BDG = (MONTANTBUDGET_NODE / rapportParams.nbMonth)>
			<cfset monthNode[1].XmlAttributes.SET = 1>
			<cfset totalMontant = totalMontant + MONTANT>
			<cfset monthPosition = monthNode[1].XmlAttributes.POS>
			<cfset nbMonthReel = nbMonthReel + 1>
		</cfoutput>
		<cfset montantAvg = totalMontant / nbMonthReel>
		<!--- Affectation sur les mois non prï¿½sents dans la requï¿½te --->
		<cfset totalExtraPol = totalMontant>
		<cfset unsetNodes = xmlSearch(monthXmlMap,"Periode/mois[@SET = 0]")>
		<cfloop index="j" from="1" to="#arrayLen(unsetNodes)#" step="1">
			<cfif unsetNodes[j].XmlAttributes.POS GT monthPosition>
				<cfset unsetNodes[j].XmlAttributes.VAL = montantAvg>
				<cfset totalExtraPol = totalExtraPol + montantAvg>
			<cfelse>
				<cfset unsetNodes[j].XmlAttributes.VAL = 0>
			</cfif>
			<cfset unsetNodes[j].XmlAttributes.BDG = (MONTANTBUDGET_NODE / rapportParams.nbMonth)>
		</cfloop>
		<!--- Crï¿½ation des cellules Excel --->
		<cfset monthArray = xmlSearch(monthXmlMap,"Periode/mois")>
		<cfloop index="m" from="1" to="#arrayLen(monthArray)#" step="1">
		    <Cell ss:StyleID="s24">
		    	<Data ss:Type="Number">#monthArray[m].XmlAttributes.VAL#</Data>
		    </Cell>
		    <Cell ss:StyleID="s24">
		    	<Data ss:Type="Number">#monthArray[m].XmlAttributes.BDG#</Data>
		    </Cell>
		</cfloop>
		<!--- Rï¿½initialisation des mois prï¿½sents dans la requï¿½te et affectï¿½s --->
		<cfset setNodes = xmlSearch(monthXmlMap,"Periode/mois[@SET = 1]")>
		<cfloop index="k" from="1" to="#arrayLen(setNodes)#" step="1">
			<cfset setNodes[k].XmlAttributes.SET = 0>
			<cfset setNodes[k].XmlAttributes.VAL = 0>
		</cfloop>
		<!--- Cellules des colonnes : TOTAL et BUDGET --->
	    <Cell ss:StyleID="s24"><Data ss:Type="Number">#totalMontant#</Data></Cell>
	    <Cell ss:StyleID="s24"><Data ss:Type="Number">#totalExtraPol#</Data></Cell>
		
	    <Cell ss:StyleID="s24"><Data ss:Type="Number">#MONTANTMINI_NODE#</Data></Cell>
	    <Cell ss:StyleID="s24"><Data ss:Type="Number">#MONTANTBUDGET_NODE#</Data></Cell>
	    <Cell ss:StyleID="s24"><Data ss:Type="Number">#MONTANTMAXI_NODE#</Data></Cell>
	   </Row>
		<cfset nbRow = nbRow + 1>
	</cfoutput>

   <Row>
    <Cell ss:StyleID="s29"><Data ss:Type="String"></Data></Cell>
    <Cell ss:StyleID="s31"><Data ss:Type="String">Total</Data></Cell>
	<cfoutput>
		<cfloop from="1" to="#rapportParams.nbMonth#" index="i" step="1">
		    <Cell ss:StyleID="s34" ss:Formula="=SUM(R[-#nbRow#]C:R[-1]C)">
		    	<Data ss:Type="Number">#i#</Data>
		    </Cell>
		    <Cell ss:StyleID="s34" ss:Formula="=SUM(R[-#nbRow#]C:R[-1]C)">
		    	<Data ss:Type="Number">#i#</Data>
		    </Cell>
		</cfloop>
	    <Cell ss:StyleID="s34" ss:Formula="=SUM(R[-#nbRow#]C:R[-1]C)">
	    	<Data ss:Type="Number">#i#</Data>
	    </Cell>
	    <Cell ss:StyleID="s34" ss:Formula="=SUM(R[-#nbRow#]C:R[-1]C)">
	    	<Data ss:Type="Number">#i#</Data>
	    </Cell>
	    <Cell ss:StyleID="s34" ss:Formula="=SUM(R[-#nbRow#]C:R[-1]C)">
	    	<Data ss:Type="Number">#i#</Data>
	    </Cell>
	    <Cell ss:StyleID="s34" ss:Formula="=SUM(R[-#nbRow#]C:R[-1]C)">
	    	<Data ss:Type="Number">#i#</Data>
	    </Cell>
	    <Cell ss:StyleID="s35" ss:Formula="=SUM(R[-#nbRow#]C:R[-1]C)">
	    	<Data ss:Type="Number">#i#</Data>
	    </Cell>
	</cfoutput>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.4921259845"/>
    <Footer x:Margin="0.4921259845"/>
    <PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
     x:Right="0.78740157499999996" x:Top="0.984251969"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>0</ActiveRow>
     <ActiveCol>0</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
 <Worksheet ss:Name="Feuil2">
  <Table ss:ExpandedColumnCount="0" ss:ExpandedRowCount="0" x:FullColumns="1"
   x:FullRows="1" ss:DefaultColumnWidth="60"/>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.4921259845"/>
    <Footer x:Margin="0.4921259845"/>
    <PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
     x:Right="0.78740157499999996" x:Top="0.984251969"/>
   </PageSetup>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
 <Worksheet ss:Name="Feuil3">
  <Table ss:ExpandedColumnCount="0" ss:ExpandedRowCount="0" x:FullColumns="1"
   x:FullRows="1" ss:DefaultColumnWidth="60"/>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.4921259845"/>
    <Footer x:Margin="0.4921259845"/>
    <PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
     x:Right="0.78740157499999996" x:Top="0.984251969"/>
   </PageSetup>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
