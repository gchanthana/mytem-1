<cfcontent type="application/pdf"> 
<cfheader name='Content-Disposition' VALUE='attachment;filename=TelephonieFixeParSocietes-#rapportParams.RAISON_SOCIALE#.pdf'/>

<cfif rapportParams.periodicite EQ 0>
	<cfset periodiciteString = "mensuel">
<cfelse>
	<cfset periodiciteString = "bimestriel">
</cfif>
<cfdocument format="#rapportParams.format#" backgroundvisible="yes" fontembed = "yes" 
			unit="CM" margintop="1" pagetype="custom" pageheight="29" pagewidth="21" marginleft="1" orientation="landscape">

<html>
	<head>
		
<!--- 	<link href="./css/detailfilaire_style.css" rel="stylesheet" type="text/css"> --->
			<STYLE type="text/css">
			<!--
html body {
  margin:0px;
  padding:0px;
  background:#FFFFFF;
  font: normal 11px Tahoma,Arial, sans-serif;
}

table {
	font: normal 9px Tahoma,Arial, sans-serif;
	
	padding-bottom:0px;
	padding-left:0px;
	padding-right:0px;
	padding-top:0px;
}

td {
	height:9px;
	padding:0px;
}

.cadre_graphique {
	border:1px;
	border-style:solid;
}

.cadre_cumul {
	border:1px;
	border-style:solid;
	border-left:0px;
	height:13px;
	padding-top:1px;
	padding-bottom:1px;
}

.cadre_soc {
	border:1px;
	border-style:solid;
	border-bottom:0px;
	border-top:0px;
	border-left:0px;
	height:13px;
	padding-bottom:1px;
	padding-top:1px;
}

.cadre_left {
	border:1px;
	border-style:solid;
	
	border-left:0px;
	border-top:0px;
	border-bottom:0px;
}

.cadre_typetheme {
	border:1px;
	border-style:solid;
	background-color:#99CCFF;
	height:13px;
	padding-bottom:1px;
	padding-top:1px;
}

.cadre_graph_title {
	color:#FFFFFF;
	background-color:#036;
	height:14px;
	padding-bottom:1px;
	padding-top:1px;
	font: normal 12px Tahoma,Arial, sans-serif;
}

.cadre_header {
	border:1px;
	border-style:solid;
	background-color:#99CCFF;

	border-left:0px;
	height:13px;
	padding-bottom:1px;
	padding-top:1px;
}

.cadre_cell_header {
	border:1px;
	border-style:solid;

	border-left:0px;
	border-top:0px;
	border-bottom:0px;
}

.cadre_left_right {
	border:1px;
	border-style:solid;

	border-top:0px;
	border-bottom:0px;
}

.cadre_total_typetheme {
	border:1px;
	border-style:solid;
	background-color:#FFCC00;

	border-bottom:0px;
	height:13px;
	padding-bottom:1px;
	padding-top:1px;
}

.cadre_total_typetheme_header {
	border:1px;
	border-style:solid;
	background-color:#FFCC00;

	border-left:0px;
	border-bottom:0px;
	height:13px;
	padding-bottom:1px;
	padding-top:1px;
}

.cadre_after_total {
	border:1px;
	border-style:solid;

	border-top:0px;
	height:13px;
	padding-bottom:1px;
	padding-top:1px;
}

.cadre_after_total_header {
	border:1px;
	border-style:solid;

	border-left:0px;
	border-top:0px;
	height:13px;
	padding-bottom:1px;
	padding-top:1px;
}

.total_general {
	border:1px;
	border-style:solid;

	background-color:#FF9900;
	height:13px;
	padding-bottom:1px;
	padding-top:1px;
}

.total_general_header {
	border:1px;
	border-style:solid;

	background-color:#FF9900;
	border-left:0px
	height:13px;
	padding-bottom:1px;
	padding-top:1px;
}
			-->
			</STYLE>
	</head>
	<body>

<cfobject component="graph" name="mygraph">
<cfset NBRE_DE_SOCIETE = 1>
<cfset NB_ROWS_SPAN=42>
<cfset SPACE_WIDTH = 50>
<cfset GRAPH_WIDTH = 450>
<!---cfset GRAPH_HEIGHT = 110--->
<cfset GRAPH_HEIGHT = 165>

<cfset TYPE_THEME_WIDTH=130>
<cfset QTE_WIDTH = 75>
<cfset MONTANT_WIDTH = 85>
<cfset POURC_WIDTH = 45>
<cfset SOCIETE_WIDTH=(QTE_WIDTH + MONTANT_WIDTH + POURC_WIDTH)>

<cfset CUMUL_WIDTH=(SOCIETE_WIDTH * NBRE_DE_SOCIETE)>
<!--- CUMUL_WIDTH 1 fois pour Cumul (String) puis 1 fois à coté pour le graphe donc CUMUL_WIDTH * 2--->
<cfset GLOBAL_WIDTH=(TYPE_THEME_WIDTH + (CUMUL_WIDTH * 1) + ((GRAPH_WIDTH + SPACE_WIDTH) * 2))>
<cfset CUMUL_COLSPAN = 3>
<cfset GRAPH_COLSPAN = 4>
<cfset CURRENT_WIDTH=(TYPE_THEME_WIDTH + SOCIETE_WIDTH)>
<cfset LARGEUR=(TYPE_THEME_WIDTH + CUMUL_WIDTH)>


<cfset startPage = 0>
<cfoutput query="reportQuery" group="idref_client">
	<cfset TOTAL_LIGNES = 0>
	<cfset TOTAL_TYPES_THEMES = 0>
	<cfset TOTAL_SUR_THEMES = 0>
	<cfset TOTAL_THEMES = 0>
	<cfset LIGNES_MANQUANTES = 0>
	<cfif startPage neq 0>
		<cfdocumentitem type="pagebreak">
		</cfdocumentitem>
	</cfif>


<!--- 	<img src="./images/consoview_new.gif" height="45" width="200"/> --->
	<br>
	<p><strong>Rapport Spécifique téléphonie Fixe : Par Société (Edité le #LSDateFormat(Now(),"DD/MM/YYYY")# à #TimeFormat(Now(),"HH:MM")#)</strong>
	<br>
		<strong>
			Périmètre #rapportParams.type_perimetre# #rapportParams.raison_sociale#
		</strong>
	<br>
		<strong>
			<u>Période :</u>&nbsp;
				#LsDateFormat(parseDateTime(RapportParams.date_m),"Mmmm yyyy")#
			<!---
				#LsDateFormat(CreateDate(right(RapportParams.date_m,4),mid(RapportParams.date_m,4,2),"01"),"Mmmm yyyy")#
			--->
				<cfif periodiciteString eq "bimestriel">
					#LsDateFormat(parseDateTime(RapportParams.date_m2),"Mmmm yyyy")#
					<!---
					 - #LsDateFormat(CreateDate(right(RapportParams.date_m2,4),mid(RapportParams.date_m2,4,2),"01"),"Mmmm yyyy")#
					 --->
				</cfif>
		</strong><br>

	<table cellpadding="0" cellspacing="0" width="#GLOBAL_WIDTH#">
		<tr>
			<td width="#TYPE_THEME_WIDTH#" class="cadre_left">&nbsp;</td>
			<td colspan="#CUMUL_COLSPAN#" width="#CUMUL_WIDTH#" align="center" class="cadre_cumul">
				<strong>
					Cumul #periodiciteString# du #RapportParams.date_m# au 
					#LsDateFormat(parseDateTime(RapportParams.date_m2),"Mmmm yyyy")#
					<!---
					#LsDateFormat(dateadd("d",-1,dateadd("m",1,CreateDate(right(RapportParams.date_m2,4),mid(RapportParams.date_m2,4,2),"01"))),"dd/mm/yyyy")#
					--->
				</strong>
			</td>
			
			<td rowspan="#NB_ROWS_SPAN#" colspan="1" align="center" width="#SPACE_WIDTH#">&nbsp;</td>
			
			<td rowspan="#NB_ROWS_SPAN#" colspan="#CUMUL_COLSPAN#" width="#GRAPH_WIDTH#" valign="top" height="20">
				<table cellpadding="0" cellspacing="0" width="#GRAPH_WIDTH#">
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" width="#GRAPH_WIDTH#" class="cadre_graphique">
								<tr>
									<td align="left" class="cadre_graph_title">
										<strong>Statistiques des Consommations par coûts</strong>
									</td>
								</tr>
								<tr>
									<td align="center">
										<cfsavecontent variable="chartData">
										<cfchart 
											style="./graph/graph_pie_detailfilaire.xml"
											chartHeight = "#GRAPH_HEIGHT#"
											chartWidth = "#GRAPH_WIDTH#"
											format="png"
											showLegend="yes"
											font="Verdana"
											fontsize="9">
											<cfchartseries  type="pie" paintstyle="plain" colorlist="#mygraph.GetColor('pie',1)#">
												<cfset qCoutsStats=qGetCoutsConsoStatsBySociete(#reportQuery#,#idref_client#)>
												<cfloop query="qCoutsStats">
													<cfset value = THEME_LIBELLE>
													<cfchartdata item="#value#" value="#trim(MONTANT_FINAL)#">
												</cfloop>
											</cfchartseries>
										</cfchart>
										</cfsavecontent>
										<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")>
										#chartData#
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" width="#GRAPH_WIDTH#" class="cadre_graphique">
								<tr>
									<td align="left" class="cadre_graph_title">
										<strong>Statistiques des Consommations par volume</strong>
									</td>
								</tr>
								<tr>
									<td align="center">
										<cfsavecontent variable="chartData">
										<cfchart 
											style="./graph/graph_pie_detailfilaire.xml"
											chartHeight = "#GRAPH_HEIGHT#"
											chartWidth = "#GRAPH_WIDTH#"
											format="png"
											showLegend="yes"
											font="Verdana"
										fontsize="9">
											<cfchartseries  type="pie" paintstyle="plain" colorlist="#mygraph.GetColor('pie',1)#">
												<cfset qCoutsStats=qGetVolumeConsoStatsBySociete(#reportQuery#,#idref_client#)>
												<cfloop query="qCoutsStats">
													<cfset value = THEME_LIBELLE>
													<cfchartdata item="#value#" value="#trim(DUREE_APPEL)#">
												</cfloop>
											</cfchartseries>
										</cfchart>
										</cfsavecontent>
										<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")>
										#chartData#
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" width="#GRAPH_WIDTH#" class="cadre_graphique">
								<tr>
									<td align="left" class="cadre_graph_title">
										<strong>Destinations Internationnales par coûts</strong>
									</td>
								</tr>
								<tr>
									<td align="center" width="#GRAPH_WIDTH#" height="#GRAPH_HEIGHT#">
										<cfset interItems = 0>
										<cfsavecontent variable="chartData">
										<cfchart
											style="./graph/graph_pie_detailfilaire.xml"
											chartHeight = "#GRAPH_HEIGHT#"
											chartWidth = "#GRAPH_WIDTH#"
											format="png"
											showLegend="yes"
											font="Verdana"
											fontsize="9">
											<cfchartseries  type="pie" paintstyle="plain" colorlist="#mygraph.GetColor('pie',1)#">
												<cfif structKeyExists(datasetInterStruct,"#idref_client#")>
													<cfset interArrayTmp = datasetInterStruct[#idref_client#]['LIBELLES']>
													<cfloop from="1" to="#ArrayLen(interArrayTmp)#" index="i">
														<cfset libelleTemp = datasetInterStruct[#idref_client#]['LIBELLES'][i]>
														<cfset montantTemp = datasetInterStruct[#idref_client#]['MONTANTS'][i]>
														<cfif montantTemp neq 0>
															<cfset interItems = interItems + 1>
															<cfchartdata item="#libelleTemp#" value="#montantTemp#">
														</cfif>
													</cfloop>
												</cfif>
											</cfchartseries>
										</cfchart>
										</cfsavecontent>
										<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")>
										<cfif interItems eq 0>
											<br><br><br><br><strong>&nbsp;</strong><br><br><br><br>&nbsp;
										<cfelse>
											#chartData#
										</cfif>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td width="#TYPE_THEME_WIDTH#" class="cadre_left">&nbsp;</td>
	
			<td colspan="3" width="#SOCIETE_WIDTH#" align="center" class="cadre_soc">
				<strong>#libelle#</strong>
			</td>
		</tr>
		<cfset totalMontant = 0>
		<cfoutput group="type_theme">
			<cfset TOTAL_TYPES_THEMES = TOTAL_TYPES_THEMES + 1>
			<cfset currentTypeThemeMontantTotal = 0>
			<tr>
				<td width="#TYPE_THEME_WIDTH#" class="cadre_typetheme"><strong>&nbsp;#type_theme#</strong></td>
				<cfif UCase(#type_theme#) eq "ABONNEMENTS">
					<td width="#QTE_WIDTH#" align="center" class="cadre_header"><strong>Quantité</strong></td>
				<cfelse>
					<td width="#QTE_WIDTH#" align="center" class="cadre_header"><strong>Durée en minutes</strong></td>
				</cfif>
				<td width="#MONTANT_WIDTH#" align="center" class="cadre_header"><strong>Montant</strong></td>
				<td width="#POURC_WIDTH#" align="center" class="cadre_header"><strong>%P-1</strong></td>
			</tr>

			<tr>
				<td width="#TYPE_THEME_WIDTH#" align="left" class="cadre_left_right">&nbsp;</td>
		
				<td width="#QTE_WIDTH#" align="center" class="cadre_cell_header">&nbsp;</td>
				<td width="#MONTANT_WIDTH#" align="center" class="cadre_cell_header">&nbsp;</td>
				<td width="#POURC_WIDTH#" align="center" class="cadre_cell_header">&nbsp;</td>
			</tr>
			<cfoutput group="sur_theme">
				<cfset TOTAL_SUR_THEMES = TOTAL_SUR_THEMES + 1>
				<cfset currentSurThemeMontantTotal = totalSurThemeStruct['#idref_client#']['#sur_theme#'].montant_final>
				<tr>
					<td width="#TYPE_THEME_WIDTH#" align="left" class="cadre_left_right">
						<strong>
							&nbsp;&nbsp;&nbsp;&nbsp;#sur_theme#
						</strong>
					</td>
					<td width="#QTE_WIDTH#" align="right" class="cadre_cell_header">&nbsp;</td>
					<td width="#MONTANT_WIDTH#" align="right" class="cadre_cell_header">
						<strong>#LsEuroCurrencyFormat(currentSurThemeMontantTotal)#</strong>
					</td>
					<td width="#POURC_WIDTH#" align="right" class="cadre_cell_header">&nbsp;</td>
				</tr>
				<cfoutput group="idtheme_produit">
					<cfset TOTAL_THEMES = TOTAL_THEMES + 1>
					<tr>
						<td width="#TYPE_THEME_WIDTH#" align="left" class="cadre_left_right">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#theme_libelle#
						</td>

						<cfif UCase(#type_theme#) eq "ABONNEMENTS">
							<td width="#QTE_WIDTH#" align="right" class="cadre_cell_header">
								#LsNumberFormat(QTE)#
							</td>
						<cfelse>
							<td width="#QTE_WIDTH#" align="right" class="cadre_cell_header">
								#LsNumberFormat(DUREE_APPEL)#
							</td>
						</cfif>
						<td width="#MONTANT_WIDTH#" align="right" class="cadre_cell_header">
							#LsEuroCurrencyFormat(MONTANT_FINAL)#
						</td>
						
						<cfif UCase("#POURC_EVO#") eq "N.D">
							<td width="#POURC_WIDTH#" align="center" class="cadre_cell_header">
								#POURC_EVO#&nbsp;&nbsp;
						<cfelse>
							<td width="#POURC_WIDTH#" align="right" class="cadre_cell_header">
								#LsNumberFormat(POURC_EVO,"+.0")# %
						</cfif>
						</td>
					</tr>
				</cfoutput>
				<tr>
					<td width="#TYPE_THEME_WIDTH#" align="left" class="cadre_left_right">&nbsp;</td>
			
					<td width="#QTE_WIDTH#" align="center" class="cadre_cell_header">&nbsp;</td>
					<td width="#MONTANT_WIDTH#" align="center" class="cadre_cell_header">&nbsp;</td>
					<td width="#POURC_WIDTH#" align="center" class="cadre_cell_header">&nbsp;</td>
				</tr>
				<cfset currentTypeThemeMontantTotal = currentTypeThemeMontantTotal + currentSurThemeMontantTotal>
			</cfoutput>		
			<tr>
				<td width="#TYPE_THEME_WIDTH#" align="left" class="cadre_total_typetheme">
					<strong>
						&nbsp;Total #type_theme#
					</strong>
				</td>
				<td width="#QTE_WIDTH#" align="right" class="cadre_total_typetheme_header">&nbsp;</td>
				<td width="#MONTANT_WIDTH#" align="right" class="cadre_total_typetheme_header"><strong>
					#LsEuroCurrencyFormat(currentTypeThemeMontantTotal)#
				</strong></td>
				<td width="#POURC_WIDTH#" align="right" class="cadre_total_typetheme_header"><strong>&nbsp;</strong></td>
			</tr>
		
			<tr>
				<td width="#TYPE_THEME_WIDTH#" align="left" class="cadre_after_total">
					&nbsp;Dont part France Télécom
				</td>
		
				<td width="#QTE_WIDTH#" align="right" class="cadre_after_total_header">&nbsp;</td>
				<td width="#MONTANT_WIDTH#" align="right" class="cadre_after_total_header"><strong>
					#LsEuroCurrencyFormat(MONTANT_FT)#
				</strong></td>

					<cfif UCase("#POURC_EVO_FT#") eq "N.D">
						<td width="#POURC_WIDTH#" align="center" class="cadre_after_total_header"><strong>
							#POURC_EVO_FT#&nbsp;&nbsp;
					<cfelse>
						<td width="#POURC_WIDTH#" align="right" class="cadre_after_total_header"><strong>
							#LsNumberFormat(POURC_EVO_FT,"+.0")# %
					</cfif>
				</strong></td>
			</tr>
		
			<tr>
				<td width="#TYPE_THEME_WIDTH#" align="left" class="cadre_left_right">&nbsp;</td>
			
				<td width="#QTE_WIDTH#" align="center" class="cadre_cell_header">&nbsp;</td>
				<td width="#MONTANT_WIDTH#" align="center" class="cadre_cell_header">&nbsp;</td>
				<td width="#POURC_WIDTH#" align="center" class="cadre_cell_header">&nbsp;</td>
			</tr>
			<cfset totalMontant = totalMontant + currentTypeThemeMontantTotal>
		</cfoutput>
		<tr>
			<td width="#TYPE_THEME_WIDTH#" align="left" class="total_general">
				<strong>
					&nbsp;Total Général
				</strong>
			</td>
	
			<td width="#QTE_WIDTH#" align="right" class="total_general_header">&nbsp;</td>
			<td width="#MONTANT_WIDTH#" align="right" class="total_general_header"><strong>
				#LsEuroCurrencyFormat(totalMontant)#
			</strong></td>
			<td width="#POURC_WIDTH#" align="right" class="total_general_header">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr align="center">
			<td colspan="#GRAPH_COLSPAN#" width="#GRAPH_WIDTH#" valign="top" height="20">
				<table cellpadding="0" cellspacing="0" width="#GRAPH_WIDTH#" class="cadre_graphique">
					<tr>
						<td align="left" class="cadre_graph_title">
							<strong>Evolution du budget</strong>
						</td>
					</tr>
					
					<tr>
						<td align="center">
					 		<cfset societeEvoDataset=qGetTypeThemeCoutEvolution(#dataset#,#idref_client#,#periodiciteString#)>
							<cfsavecontent variable="chartData">
							<cfchart
							 	style="./graph/graph_bar_rapport.xml"
								chartHeight = "#GRAPH_HEIGHT#"
								chartWidth = "#GRAPH_WIDTH#"
								format="png"
								showLegend="yes"
								font="Verdana"
								fontsize="9"
								yaxistitle="Euros">
								<cfchartseries  type="bar" seriesColor="#mygraph.GetColor('bar',1,2)#" serieslabel="Conso.">
									<cfloop query="societeEvoDataset">
										<cfif UCase(type_theme) eq "CONSOMMATIONS">
											<cfchartdata item='#LsDateFormat(mois_report,"mm/yyyy")#' value='#montant_final#'>
										</cfif>
									</cfloop>
								</cfchartseries>
								<cfchartseries type="bar" seriesColor="#mygraph.GetColor('bar',1,1)#" serieslabel="Abo.">
									<cfloop query="societeEvoDataset">
										<cfif UCase(type_theme) eq "ABONNEMENTS">
											<cfchartdata item='#LsDateFormat(mois_report,"mm/yyyy")#' value='#montant_final#'>
										</cfif>
									</cfloop>
								</cfchartseries>
							</cfchart>
							</cfsavecontent>
							<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")>
							#chartData#
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<cfset TOTAL_LIGNES = ((3 + (5 * TOTAL_TYPES_THEMES) + (2 * TOTAL_SUR_THEMES) + TOTAL_THEMES) + 11)>
		<cfif TOTAL_LIGNES lt 36>
			<cfset LIGNES_MANQUANTES = (36 - TOTAL_LIGNES)>
			<cfloop from="1" to="#LIGNES_MANQUANTES#" index="z">
				<tr>
					<td width="#TYPE_THEME_WIDTH#" align="left"></td>
					<td width="#QTE_WIDTH#" align="center"></td>
					<td width="#MONTANT_WIDTH#" align="center"></td>
					<td width="#POURC_WIDTH#" align="center"></td>
				</tr>
			</cfloop>
		</cfif>
	</table>
	<cfset startPage = startPage + 1>
</cfoutput>

<cfdocumentitem type="footer">
			<style>
			.tfooter {
				color: #000000;
				font: normal 11px Tahoma,Arial, sans-serif;
			}
			</style>
			<center>
				<caption class="tfooter">
					Copyright ConsoTel © 2000 / 2006 - page <cfoutput>#cfdocument.currentpagenumber# / #cfdocument.totalpagecount#</cfoutput>
				</caption>
			</center>
</cfdocumentitem>

</body>
</html>
</cfdocument>
