<!---
Package : fr.consotel.consoview.facturation.optimisation.repartEvoGlobal
--->
<cfcomponent name="RapportRepartEvoGlobalGroupeStrategy">
	<cffunction name="getReportData" access="private" returntype="any" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset RapportParams.date_m = LSDateFormat(ParseDateTime(RapportParams.DATEDEB),"yyyy/mm/dd")>
		<cfset RapportParams.date_m2=
				LSDateFormat(DateAdd("m",RapportParams.periodicite,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_un=
				LSDateFormat(DateAdd("m",-1,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_deux=
				LSDateFormat(DateAdd("m",(-1 - (RapportParams.periodicite)),ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_debut_annee = year(ParseDateTime(RapportParams.DATEDEB)) & "/01/01">

		<cfif periodicite eq 0>
			<cfset RapportParams.chaine_date=
					LsDateFormat(ParseDateTime(RapportParams.DATE_M),"mmmm yyyy")>
		<cfelse>
			<cfset RapportParams.chaine_date=
					LsDateFormat(ParseDateTime(RapportParams.DATE_M),"mmmm") & "/" & LsDateFormat(ParseDateTime(RapportParams.DATE_M2),"mmmm yyyy") >
		</cfif>
        <cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_REP_V3.REP_CONSO_GRP">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.DATE_M#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATE_M2#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATE_M_MOINS_UN#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATE_M_MOINS_DEUX#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATE_DEBUT_ANNEE#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfreturn qGetReportData>
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset NEW_DATEDEB = parseDateTime(RapportParams.DATEDEB)>
		<!---====== FORMATAGE DES DATES DE DEBUT ET DE FIN ======--->
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEDEB)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEDEB)>
		<cfset rapportParams.DATEDEB =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<!---====================================================--->
		<cfset qGetData = getReportData(ID_PERIMETRE,RapportParams)>
		<cfif #qGetData.recordcount# gt 0>
			<cfset total_minutes=0>
			<cfset total_montant=0>
			<cfset total_minutes_annee=0>
			<cfset total_montant_annee=0>
			<cfoutput query="qGetData">
					<cfset total_minutes=total_minutes+duree_m>
					<cfset total_montant=total_montant+montant_m>
					<cfset total_minutes_annee=total_minutes_annee+duree_annee>
					<cfset total_montant_annee=total_montant_annee+montant_annee>
			</cfoutput>
			<cfset cfrPath = "#session.backOfficeRootPath#\cfm\report\optimisation\cfr\rapport_conso_par_compte_de_facturation.cfr">
			<cfreport format="#RapportParams.format#" template="#cfrPath#" query="qGetData">
				<cfreportparam name="total_minutes" value="#total_minutes#">
				<cfreportparam name="total_montant" value="#total_montant#">
				<cfreportparam name="total_minutes_annee" value="#total_minutes_annee#">
				<cfreportparam name="total_montant_annee" value="#total_montant_annee#">
				<cfreportparam name="client_raison_sociale" value="#raison_sociale#">
				<cfreportparam name="annee_rapport" value="#RapportParams.date_debut_annee#">
				<cfreportparam name="date_rapport" value="#RapportParams.chaine_date#">
				<cfreportparam name="idref_client" value="#id_perimetre#">
			</cfreport>
		<cfelse>
			<cfoutput>
				<center><strong><h2>Aucunes données répondant à votre demande</h2></strong></center>
			</cfoutput>
		</cfif>
	</cffunction>
</cfcomponent>
