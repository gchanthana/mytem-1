<!---
Package : fr.consotel.consoview.facturation.optimisation.historique
--->
<cfcomponent name="HistoriqueContext">
	<cffunction name="getHistoriqueData" access="remote" returntype="query" output="true">
		<cfargument name="PERIMETRE_INDEX" required="true" type="numeric"/>
		<cfargument name="TYPE_PERIMETRE" required="true" type="string"/>
		<cfargument name="DATEDEB" required="true" type="string"/>
		<cfargument name="DATEFIN" required="true" type="string"/>
		<cfset utilObj = createObject("component","fr.consotel.consoview.util.utils")>
		<cfset idPerimetre = utilObj.getIdPerimetreByIndex(PERIMETRE_INDEX)>
		<cfset strategyObject =
				createObject("component","fr.consotel.consoview.facturation.optimisation.historique.Historique#TYPE_PERIMETRE#")>
		<cfset histoDataset = strategyObject.getHistoriqueData(idPerimetre,DATEDEB,DATEFIN)>
		<cfreturn histoDataset>
	</cffunction>

	<cffunction name="getHistoData" access="remote" returntype="xml" output="false">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="TYPE_PERIMETRE" required="true" type="string"/>
		<cfargument name="DATEDEB" required="true" type="string"/>
		<cfargument name="DATEFIN" required="true" type="string"/>
		<cfset strategyObject =
				createObject("component","fr.consotel.consoview.facturation.optimisation.historique.Historique#TYPE_PERIMETRE#")>
		<cfset histoXmlData = strategyObject.getHistoData(ID_PERIMETRE,DATEDEB,DATEFIN)>
		<cfreturn histoXmlData>
	</cffunction>
	
	<cffunction name="getHistoriqueColumnMap" access="remote" returntype="array" output="true">
		<cfargument name="DATEDEB" required="true" type="string"/>
		<cfargument name="DATEFIN" required="true" type="string"/>
		<cfset dateDebObj = parseDateTime(DATEDEB)>
		<cfset dateFinObj = parseDateTime(DATEFIN)>
		<cfset currentDateObj = parseDateTime(DATEDEB)>
		<cfset nbMonth = dateDiff("M",dateDebObj,dateFinObj) + 1>
		<cfset columnMapArray = arrayNew(1)>
		
		<cfset tmpStruct = structNew()>
		<cfset tmpStruct.COLUMN_FIELD = "RAISON_SOCIALE">
		<cfset tmpStruct.COLUMN_LABEL = "Société">
		<cfset columnMapArray[1] = tmpStruct>
		
		<cfset tmpStruct = structNew()>
		<cfset tmpStruct.COLUMN_FIELD = "OPNOM">
		<cfset tmpStruct.COLUMN_LABEL = "Opérateur">
		<cfset columnMapArray[2] = tmpStruct>
		
		<cfset tmpStruct = structNew()>
		<cfset tmpStruct.COLUMN_FIELD = "OFFRENOM">
		<cfset tmpStruct.COLUMN_LABEL = "Offre">
		<cfset columnMapArray[3] = tmpStruct>
		
		<cfset tmpStruct = structNew()>
		<cfset tmpStruct.COLUMN_FIELD = "TYPEDATA">
		<cfset tmpStruct.COLUMN_LABEL = "Type">
		<cfset columnMapArray[4] = tmpStruct>
		
		<cfset nbColumn = nbMonth + 3>
		<cfloop index="i" from="4" to="#nbColumn#">
			<cfset tmpStruct = structNew()>
			<cfset tmpStruct.COLUMN_FIELD = "M_" & replace(lsDateFormat(currentDateObj,"YYYY/MM"),"/","_","ALL")>
			<cfset tmpStruct.COLUMN_LABEL = lsDateFormat(currentDateObj,"MM/YY")>
			<cfset columnMapArray[i] = tmpStruct>
			<cfset currentDateObj = dateAdd("M",1,currentDateObj)>
		</cfloop>
		<cfreturn columnMapArray>
	</cffunction>
</cfcomponent>
