<!---
Package : fr.consotel.consoview.facturation.optimisation
--->
<cfcomponent name="utils">
	<cffunction name="transposeQuery" acess="remote" returntype="query" output="false">
		<cfargument name="dataset" type="query">
		<cfargument name="champFixe" type="string">
		<cfargument name="champCle" type="string">
		<cfargument name="champValeur" type="string">
		<cfargument name="stAbos" type="struct">
		<!--- Création de la structure avec colonnes variables --->
		<cfquery dbtype="query" name="qGetcfixe">
			select distinct #champFixe#
			from dataset
		</cfquery>
		<cfquery dbtype="query" name="qGetcle">
			select distinct #champCle#
			from dataset
		</cfquery>
		<cfset stSource=structNew()>
		<cfloop query="dataset">
			<cfset champ=Evaluate("dataset." & champFixe)>
			<cfset cle=Evaluate("dataset." & champCle)>
			<cfset valeur=Evaluate("dataset." & champValeur)>
			<cfset stTemp=structNew()>
			<cfloop query="qGetcle">
				<cfset structInsert(stTemp,Evaluate("qGetcle." & #champCle#),0)>
			</cfloop>
			<!--- Initialise la structure --->
			<cfif Not StructKeyExists(stSource,champ)>
				<cfset structInsert(stSource,champ,stTemp)>
			</cfif>
			<!--- Remplie la structure --->
			<cfif type_theme neq "Abonnements">
				<cfset stSource[champ][cle]=stSource[champ][cle]+valeur>
			</cfif>
		</cfloop>
		<!--- Création de la structure avec colonnes fixes --->
		<cfset stFixe=structNew()>
		<cfset t=1>
		<cfloop query="dataset">
			<cfset champ=Evaluate("dataset." & champFixe)>
			<cfset stTemp=structNew()>
			<cfloop from="1" to="#ListLen(dataset.columnlist)#" index="i">
				<cfset listeColonne=ListGetAt(dataset.columnlist,i)>
				<cfif listeColonne neq champFixe AND listeColonne neq champCle AND listeColonne neq champValeur>
					<cfset stTemp['#ListGetAt(dataset.columnlist,i)#']=dataset[ListGetAt(dataset.columnlist,i)][t]>
				</cfif>
			</cfloop>
			<!--- Initialise la structure --->
			<cfif Not StructKeyExists(stFixe,champ)>
				<cfset structInsert(stFixe,champ,stTemp)>
			</cfif>
			<cfset t=t+1>
		</cfloop>
		<!--- Création de la liste des champs --->
		<cfset listeFinale="">
		<cfloop from="1" to="#ListLen(dataset.columnlist)#" index="i">
			<cfset listeColonne=ListGetAt(dataset.columnlist,i)>
			<cfif listeColonne neq champCle AND listeColonne neq champValeur>
				<cfset listeFinale=ListAppend(listeFinale,listeColonne)>
			</cfif>
		</cfloop>
		<cfloop query="qGetcle">
			<cfset listeFinale=ListAppend(listeFinale,Evaluate("qGetcle." & #champCle#))>
		</cfloop>
		<cfset qFinale=queryNew(replace(listeFinale," ","_","all"))>
		<cfloop collection="#stFixe#" item="coll">
			<cfset queryAddRow(qFinale)>
			<cfset querySetCell(qFinale,champFixe,coll)>
			<cfloop collection="#stFixe[coll]#" item="i">
				<cfif i neq "montant_abo_total">
					<cfset querySetCell(qFinale,i,stFixe[coll][i])>
				<cfelse>
					<cfif StructKeyExists(stAbos,stFixe[coll]['idsous_tete'])>
						<cfset querySetCell(qFinale,i,stAbos[stFixe[coll]['idsous_tete']])>
					<cfelse>
						<cfset querySetCell(qFinale,i,0)>
					</cfif>
				</cfif>
			</cfloop>
			<br>
			<cfloop collection="#stSource[coll]#" item="i">
				<cfset querySetCell(qFinale,replace(i," ","_","all"),stSource[coll][i])>
			</cfloop>
		</cfloop>
		<cfreturn qFinale>
	</cffunction>

	<cffunction name="createReport" acess="remote" returntype="void" output="true">
		<cfargument name="argurl" type="struct">
		<cfargument name="argform" type="struct">
		<cfargument name="chemin" type="String" default="">

		<!--- boucle sur les parametres appelant la page d'origine --->
		<cfset chaine="">
		<cfif Not StructIsEmpty(url)>
			<cfloop collection="#url#" item="i">
				<cfset chaine=chaine & i & "=" & #url[i]# & "&">
			</cfloop>
		</cfif>
		<cfif Not StructIsEmpty(form)>
			<cfloop collection="#form#" item="i">
				<cfif i neq "fieldnames">
					<cfset chaine=chaine & i & "=" & form[i] & "&">
				</cfif>
			</cfloop>
		</cfif>
		<cfset chaine=left(chaine,len(chaine)-1)>
		<!--- Affichage de la barre qui appelle le rapport --->		
		<cfoutput>
		<script>
		function ouvre(a) {
			window.open("#chemin#?format="+a+"&#chaine#",'report','menubar=no,status=no,location=no,scrollbars=1,directories=no,copyhistory=no,resizable=yes');	
		}
		</script>
		</cfoutput>
		<a href="##" onclick="ouvre('flashpaper');"><font size="1">flashpaper</font></a><br>
		<a href="##" onclick="ouvre('pdf');"><font size="1">pdf</a><br>
		<a href="##" onclick="ouvre('excel');"><font size="1">Excel</a><br>
	</cffunction>

	<!--- Calcule la répartition d'une durée en secondes (hh:mm:ss) --->
	<cffunction name="getDurationRepartition" acess="remote" returntype="string" output="true">
		<cfargument name="total" type="numeric" required="true" hint="Durée en secondes">
		<cfset tmpTotal = total>
		<cfset tmpHours = 0>
		<cfset tmpMinutes = 0>
		<cfset tmpSeconds = 0>
		
		<cfif tmpTotal neq 0>
			<cfset tmpHours = Int(total)>
			<cfif tmpHours gt 0>
				<cfset tmpTotal = tmpTotal - (tmpHours * 60)>
			</cfif>
		</cfif>
		
		<cfif tmpTotal neq 0>
			<cfset tmpMinutes = Int(tmpTotal)>
			<cfif tmpMinutes gt 0>
				<cfset tmpTotal = tmpTotal - (tmpMinutes)>
			</cfif>
		</cfif>
		
		<cfset tmpSeconds = tmpTotal>
		<cfreturn "#tmpHours#:#tmpMinutes#:#tmpSeconds#">
	</cffunction>
</cfcomponent>
