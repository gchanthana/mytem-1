<cfif rapportParams.periodicite EQ 0>
	<cfset periodiciteString = "Mensuel">
<cfelse>
	<cfset periodiciteString = "Bimestriel">
</cfif>
<cfheader name="Content-Disposition" value="inline;filename=DetailDATA.xls">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="iso-8859-1"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Brice Miramont</Author>
  <LastAuthor>Brice Miramont</LastAuthor>
  <Created>2006-04-27T06:42:56Z</Created>
  <LastSaved>2006-05-11T11:52:25Z</LastSaved>
  <Company>CONSOTEL</Company>
  <Version>11.6568</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>8385</WindowHeight>
  <WindowWidth>13740</WindowWidth>
  <WindowTopX>360</WindowTopX>
  <WindowTopY>300</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s21">
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s22">
   <Interior/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s23">
   <Font x:Family="Swiss" ss:Size="12" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s25">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom" ss:Rotate="-90"
    ss:WrapText="1"/>
   <Font ss:Size="7" ss:Color="#0000FF"/>
   <Interior/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s26">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font x:Family="Swiss" ss:Size="12" ss:Color="#993366" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s27">
   <Font x:Family="Swiss" ss:Size="12" ss:Bold="1"/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s28">
   <Alignment ss:Vertical="Center"/>
  </Style>
  <Style ss:ID="s29">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s30">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
  </Style>
  <Style ss:ID="s31">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s32">
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s35">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s36">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
  </Style>
  <Style ss:ID="s37">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s38">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s39">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s40">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:Indent="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s41">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <NumberFormat ss:Format="#,##0"/>
  </Style>
  <Style ss:ID="s42">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
   </Borders>
   <Interior/>
   <NumberFormat ss:Format="#,##0"/>
  </Style>
  <Style ss:ID="s43">
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s44">
   <Borders/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s45">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:Indent="2"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:Color="#000080"/>
  </Style>
  <Style ss:ID="s46">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
   </Borders>
   <NumberFormat ss:Format="#,##0"/>
  </Style>
  <Style ss:ID="s47">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:Indent="2"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:Color="#993366"/>
  </Style>
  <Style ss:ID="s48">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:Indent="2"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000080" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s49">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:Color="#993366"/>
   <NumberFormat ss:Format="#,##0"/>
  </Style>
  <Style ss:ID="s50">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
   </Borders>
   <Font ss:Color="#993366"/>
   <NumberFormat ss:Format="#,##0"/>
  </Style>
  <Style ss:ID="s51">
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:Color="#993366"/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s52">
   <Borders/>
   <Font ss:Color="#993366"/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s53">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#99CC00" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s54">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font/>
   <Interior ss:Color="#99CC00" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s55">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
   </Borders>
   <Font/>
   <Interior ss:Color="#99CC00" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s56">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:Color="#993366"/>
   <Interior ss:Color="#99CC00" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s57">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:Size="7" ss:Color="#0000FF"/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s58">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:Indent="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
  </Style>
  <Style ss:ID="s59">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
  </Style>
  <Style ss:ID="s60">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
   </Borders>
  </Style>
  <Style ss:ID="s61">
   <Borders/>
   <Font ss:Size="8" ss:Color="#0000FF"/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s62">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s63">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s64">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s66">
   <Borders/>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s67">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Interior/>
   <NumberFormat ss:Format="#,##0"/>
  </Style>
  <Style ss:ID="s68">
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s69">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:Indent="2"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:Color="#000080"/>
   <Interior/>
  </Style>
  <Style ss:ID="s70">
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Interior/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s71">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:Color="#993366"/>
   <Interior ss:Color="#99CC00" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s72">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
   </Borders>
   <Font ss:Color="#993366"/>
   <Interior ss:Color="#99CC00" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s73">
   <Borders/>
  </Style>
  <Style ss:ID="s74">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#CC99FF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s75">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:Color="#993366"/>
   <Interior ss:Color="#CC99FF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s76">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
   </Borders>
   <Font ss:Color="#993366"/>
   <Interior ss:Color="#CC99FF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s77">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#993366" ss:Bold="1"/>
   <Interior ss:Color="#CC99FF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s78">
   <Font x:Family="Swiss" ss:Color="#339966" ss:Bold="1" ss:Italic="1"/>
  </Style>
  <Style ss:ID="s79">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s80">
   <Borders/>
   <Interior/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s81">
   <Borders/>
   <Font x:Family="Swiss" ss:Color="#993366" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="#,##0.00\ &quot;&euro;&quot;"/>
  </Style>
  <Style ss:ID="s82">
   <Borders/>
   <Font ss:Size="8" ss:Color="#0000FF"/>
   <Interior/>
   <NumberFormat ss:Format="Percent"/>
  </Style>
  <Style ss:ID="s83">
   <Interior/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Data <cfoutput>#replace(RapportParams.chaine_date,"/","-","ALL")#</cfoutput>">
	<Table>
		<Column ss:AutoFitWidth="0" ss:Width="234.75"/>
		<Column ss:AutoFitWidth="0" ss:Width="75.75"/>
		<Column ss:AutoFitWidth="0" ss:Width="87"/>
		<Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="75.75"/>
		<Column ss:StyleID="s22" ss:AutoFitWidth="0" ss:Width="35"/>
		<Row ss:AutoFitHeight="0" ss:Height="15.75">
			<Cell ss:StyleID="s23"><Data ss:Type="String">Rapport spï¿½cifique Data</Data></Cell>
			<Cell ss:Index="5" ss:MergeDown="2" ss:StyleID="s25"><Data ss:Type="String">Evolution / mois M-1</Data></Cell>
		</Row>
		<Row ss:AutoFitHeight="0" ss:Height="15.75">
			<Cell ss:StyleID="s26"><Data ss:Type="String">Liens <cfoutput>#raison_sociale#</cfoutput></Data></Cell>
			<Cell ss:Index="3" ss:StyleID="s23"/>
			<Cell ss:StyleID="s27"/>
		</Row>
		<Row ss:AutoFitHeight="0" ss:Height="13.5">
			<Cell><Data ss:Type="String">Pï¿½riode : <cfoutput>#RapportParams.chaine_date#</cfoutput></Data></Cell>
		</Row>
		<Row ss:AutoFitHeight="0" ss:Height="13.5">
    <Cell ss:StyleID="s28"/>
    <Cell ss:StyleID="s29"><ss:Data ss:Type="String"
      xmlns="http://www.w3.org/TR/REC-html40"><B>Cumul <cfoutput>#periodiciteString#</cfoutput> <Font
        html:Color="#000080"><cfoutput>#RapportParams.date_m#</cfoutput></Font><Font> au</Font><Font html:Color="#000080">
			<cfoutput>#LsDateFormat(parseDateTime(RapportParams.date_m2),"dd/mm/yyyy")#</cfoutput>
<!--- 			<cfoutput>#LsDateFormat(dateadd("d",-1,dateadd("m",1,CreateDate(right(RapportParams.date_m2,4),mid(RapportParams.date_m2,4,2),"01"))),"dd/mm/yyyy")#</cfoutput> --->
		</Font></B></ss:Data></Cell>
    <Cell ss:StyleID="s30"/>
    <Cell ss:StyleID="s31"/>
    <Cell ss:StyleID="s32"/>
   </Row>
<cfset index=1>
<cfset total=0>
<cfset grdtotalPrec=0>
<cfoutput query="qFinale" group="type_theme">
	<cfif type_theme eq "Abonnements">
	<Row ss:StyleID="s28">
		<Cell ss:StyleID="s35"><Data ss:Type="String">#type_theme#</Data></Cell>
		<Cell ss:StyleID="s36"/>
		<Cell ss:StyleID="s37"><Data ss:Type="String">Quantitï¿½</Data></Cell>
		<Cell ss:StyleID="s38"><Data ss:Type="String">Montant (&euro;)</Data></Cell>
		<Cell ss:StyleID="s39"/>
	</Row>
	<cfelse>
		<Row ss:AutoFitHeight="0" ss:Height="25.5" ss:StyleID="s28">
		<Cell ss:StyleID="s62"><Data ss:Type="String">#type_theme#</Data></Cell>
		<Cell ss:StyleID="s63"><Data ss:Type="String">Nombre d'appels</Data></Cell>
		<Cell ss:StyleID="s64"><Data ss:Type="String">Durï¿½e en minutes</Data></Cell>
		<Cell ss:StyleID="s65"><Data ss:Type="String">Montant (&euro;)</Data></Cell>
		<Cell ss:StyleID="s44"/>
	</Row>
	</cfif>
	<Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="s58"/>
    <Cell ss:StyleID="s59"/>
    <Cell ss:StyleID="s60"/>
    <Cell ss:StyleID="s43"/>
    <Cell ss:StyleID="s61"/>
   </Row>
	<cfset totalSur="0">
	<cfset totalPrec="0">
	<cfoutput group="sur_theme">
		<Row ss:StyleID="s28">
	    	<Cell ss:StyleID="s40"><Data ss:Type="String">#sur_theme#</Data></Cell>
	    	<Cell ss:StyleID="s41"/>
	    	<Cell ss:StyleID="s42"></Cell>
	    	<Cell ss:StyleID="s43"></Cell>
	    	<Cell ss:StyleID="s44"/>
	   	</Row>
	   	<cfset totalSurtheme=index>
	   	<cfoutput>
	   		<Row ss:AutoFitHeight="0">
		    	<Cell ss:StyleID="s45"><Data ss:Type="String">#theme_libelle#</Data></Cell>
		    	<Cell ss:StyleID="s41"><cfif type_theme eq "Consommations"><Data ss:Type="Number">#nombre_appel#</Data></cfif></Cell>
		    	<Cell ss:StyleID="s46"><Data ss:Type="Number"><cfif type_theme eq "Abonnements">#qte#<cfelse>#round(duree_appel)#</cfif></Data></Cell>
		    	<Cell ss:StyleID="s43"><Data ss:Type="Number">#replace(montant_final,",",".")#</Data></Cell>
		    	<Cell ss:StyleID="s44"/>
		   	</Row>
		<cfset index=index+1>
		<cfset totalSur=totalSur+montant_final>
		<cfset totalPrec=totalPrec+montant_precedent>
		<cfset total=total+montant_final>
		<cfset grdtotalPrec=grdtotalPrec+montant_precedent>
	   	</cfoutput>
		<Row ss:AutoFitHeight="0">
			<Cell ss:StyleID="s48"><Data ss:Type="String">Total #sur_theme#</Data></Cell>
			<Cell ss:StyleID="s49"/>
			<Cell ss:StyleID="s50"/>
			<Cell ss:StyleID="s51" ss:Formula="=SUM(R[#Evaluate(totalSurtheme-(index))#]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
			<Cell ss:StyleID="s52"/>
		</Row>
		<Row ss:AutoFitHeight="0">
			<Cell ss:StyleID="s48"/>
			<Cell ss:StyleID="s49"/>
			<Cell ss:StyleID="s50"/>
			<Cell ss:StyleID="s51"/>
			<Cell ss:StyleID="s52"/>
		</Row>
	</cfoutput>
	<Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="s53"><Data ss:Type="String">Total #type_theme#</Data></Cell>
    <Cell ss:StyleID="s54"/>
    <Cell ss:StyleID="s55"/>
	<!--- <Cell ss:StyleID="s56"><Data
      ss:Type="Number">#ListLen(ListeSurtheme)#</Data></Cell> --->
    <Cell ss:StyleID="s56"><Data
      ss:Type="Number">#TotalSur#</Data></Cell>
    <Cell ss:StyleID="s57"><cfif totalsur eq 0><Data ss:Type="Number">-1</Data><cfelse><cfif totalPrec neq 0><Data ss:Type="Number">#Evaluate((totalsur-totalPrec)/(totalPrec))#</Data><cfelse><Data ss:Type="String">Pas Valeur</Data></cfif></cfif></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="s58"/>
    <Cell ss:StyleID="s59"/>
    <Cell ss:StyleID="s60"/>
    <Cell ss:StyleID="s43"/>
    <Cell ss:StyleID="s61"/>
   </Row>
</cfoutput>
	<Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="s74"><Data ss:Type="String">Grand Total</Data></Cell>
    <Cell ss:StyleID="s75"/>
    <Cell ss:StyleID="s76"/>
    <Cell ss:StyleID="s77"><Data ss:Type="Number"><cfoutput>#replace(total,",",".")#</cfoutput></Data></Cell>
    <Cell ss:StyleID="s44"><cfoutput><cfif total eq 0><Data ss:Type="Number">-1</Data><cfelse><cfif grdtotalPrec neq 0><Data ss:Type="Number">#Evaluate((total-grdtotalPrec)/(grdtotalPrec))#</Data><cfelse><Data ss:Type="String">Pas Valeur</Data></cfif></cfif></cfoutput></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="5" ss:StyleID="s44"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="s78"><Data ss:Type="String">(* chiffres en italique = estimations / exemples )</Data></Cell>
    <Cell ss:Index="5" ss:StyleID="s44"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="5" ss:StyleID="s79"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="5" ss:StyleID="s80"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="5" ss:StyleID="s81"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="5" ss:StyleID="s82"/>
   </Row>
	</Table>
   <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Layout x:Orientation="Landscape" x:CenterHorizontal="1" x:CenterVertical="1"/>
    <Header x:Margin="0.19685039370078741"/>
    <Footer x:Margin="0.19685039370078741"/>
    <PageMargins x:Bottom="0.23622047244094491" x:Left="0.31496062992125984"
     x:Right="0.31496062992125984" x:Top="0.23622047244094491"/>
   </PageSetup>
   <FitToPage/>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <TabColorIndex>13</TabColorIndex>
   <Selected/>
   <DoNotDisplayGridlines/>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
