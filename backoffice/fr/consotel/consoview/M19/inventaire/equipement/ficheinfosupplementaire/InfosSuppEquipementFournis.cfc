<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.equipement.ficheinfosupplementaire.InfosSuppEquipementFournis">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="IDEQUIPEMENT_FOURNIS" type="numeric" default="0">
	<cfproperty name="IDEQUIPEMENT_FOURNIS_PARENT" type="numeric" default="0">
	<cfproperty name="IDTYPE_EQUIPEMENT" type="numeric" default="0">
	
	<cfproperty name="TYPE_EQUIPEMENT" type="String" default="">
	<cfproperty name="MODELE" type="string" default="">
	
	<cfproperty name="IDGAMME_FAB" type="numeric" default="0">
	<cfproperty name="IDFABRIQUANT" type="numeric" default="0">
	<cfproperty name="IDREVENDEUR" type="numeric" default="0">
	<cfproperty name="IDMODELE" type="numeric" default="0">
	<cfproperty name="IDEQ_PHOTO" type="numeric" default="0">
	<cfproperty name="LIBELLE_EQ" type="string" default="">
	<cfproperty name="REFERENCE_EQ" type="string" default="">
	<cfproperty name="CODE_INTERNE" type="string" default="">
	<cfproperty name="COMMENTAIRE_EQ" type="string" default="">
	<cfproperty name="HAUTEU_U" type="numeric" default="0">
	<cfproperty name="POIDS" type="numeric" default="0">
	<cfproperty name="BRUIT" type="numeric" default="0">
	<cfproperty name="MTBF" type="numeric" default="0">
	<cfproperty name="PUISSANCE" type="numeric" default="0">
	<cfproperty name="MONTAGE_RACK" type="numeric" default="0">
	<cfproperty name="PRIX_CATALOGUE" type="numeric" default="0">
	<cfproperty name="QTE_MIN" type="numeric" default="0">
	<cfproperty name="MEMOIRE" type="string" default="">
	<cfproperty name="CAPACITE_CF" type="string" default="">
	<cfproperty name="EMPLACEMENT_INTERNE" type="numeric" default="0">
	<cfproperty name="SUPPORT_POE" type="string" default="">
	<cfproperty name="NBR_ALIM" type="string" default="">
	<cfproperty name="SUPPORT_QOS" type="string" default="">
	<cfproperty name="SUPPORT_VPN" type="string" default="">
	<cfproperty name="SUPPORT_NAT" type="string" default="">
	<cfproperty name="NBR_SLOTS_MAX" type="numeric" default="0">
	<cfproperty name="NBR_SLOTS_CMM_MAX" type="numeric" default="0">
	<cfproperty name="NBR_SLOTS_SFM_MAX" type="numeric" default="0">
	<cfproperty name="NBR_ALIM_MAX" type="numeric" default="0">
	<cfproperty name="MAX_HEAT_DISSIPATION" type="numeric" default="0">
	<cfproperty name="IMEI" type="string" default="">
	<cfproperty name="DIMENSIONS" type="string" default="">
	<cfproperty name="AFFICHAGE" type="string" default="">
	<cfproperty name="RESOLUTION" type="string" default="">
	<cfproperty name="MEMOIRE_INTERNE" type="string" default="">
	<cfproperty name="RESEAU" type="string" default="">
	<cfproperty name="TYPE_CARTE" type="string" default="">
	<cfproperty name="NB_ENTREE_SA" type="string" default="">
	<cfproperty name="NB_ENTREE_S" type="string" default="">
	<cfproperty name="NUM_SIM" type="string" default="">
	<cfproperty name="DRAM" type="numeric" default="0">
	<cfproperty name="RAM" type="numeric" default="0">
	<cfproperty name="MEMOIRE_FLASH" type="numeric" default="0">
	<cfproperty name="PROT_MANAGEMENT" type="string" default="">
	<cfproperty name="STANDARDS" type="string" default="">
	<cfproperty name="INTERFACE" type="string" default="">
	<cfproperty name="NBPORT" type="numeric" default="0">
	<cfproperty name="TAUX_TRANSFERT" type="numeric" default="0">
	<cfproperty name="NBCARTE_RESEAU" type="numeric" default="0">
	<cfproperty name="TECHNOLOGIE_RAM" type="string" default="">
	<cfproperty name="MEMOIRE_CACHE" type="numeric" default="0">
	<cfproperty name="NBSLOT_RAM" type="numeric" default="0">
	<cfproperty name="CHIPSET_CARTE_MERE" type="string" default="">
	<cfproperty name="CARTE_GRAPHIQUE" type="string" default="">
	<cfproperty name="CARTE_SON" type="string" default="">
	<cfproperty name="TYPE_ECRAN" type="string" default="">
	<cfproperty name="TAILLE_ECRAN" type="string" default="">
	<cfproperty name="AUTONOMIE_VEILLE" type="numeric" default="0">
	<cfproperty name="AUTONOMIE_FONCTIONNEMENT" type="numeric" default="0">
	<cfproperty name="TYPE_BATTERIE" type="string" default="">
	<cfproperty name="VITESSE_IMPRESSION" type="numeric" default="0">
	<cfproperty name="TAILLE_MAX_DOCUMENT" type="string" default="">
	<cfproperty name="NBR_CARTOUCHE" type="numeric" default="0">
	<cfproperty name="INDICATEUR_ETATS" type="string" default="">
	<cfproperty name="TYPE_ALIMENTATION" type="string" default="">
	<cfproperty name="COULEUR" type="string" default="">
	<cfproperty name="URL_FABRICANT" type="string" default="">
	<cfproperty name="PROTOCOLE_CONTROLE_STANDARD" type="string" default="">
	<cfproperty name="NBR_CANAUX_IP" type="numeric" default="0">
	<cfproperty name="NBR_DISQUE_DUR" type="numeric" default="0">
	<cfproperty name="TYPE_DISQUE_DUR" type="string" default="">
	<cfproperty name="VITESSE_ECRITURE" type="string" default="">
	<cfproperty name="IDCONNECTEUR" type="numeric" default="0">
	<cfproperty name="INTERRUPTEUR" type="numeric" default="0">
	<cfproperty name="NBR_PRISE_SORTIE" type="numeric" default="0">
	<cfproperty name="DISJONCTEUR" type="numeric" default="0">
	<cfproperty name="DISPOSITIF_RESEAUX" type="string" default="">
	<cfproperty name="PROTOCOLE_DE_ROUTAGE" type="string" default="">
	<cfproperty name="EMPILABLE" type="numeric" default="0">
	<cfproperty name="TAILLE_TABLE_ADRESSE_MAC" type="string" default="">
	<cfproperty name="CAPACITE_COMMUTATION" type="string" default="">
	<cfproperty name="MODEM_ADSL" type="string" default="">
	<cfproperty name="TYPE_MODEM" type="string" default="">
	<cfproperty name="NBR_ANTENNES" type="numeric" default="0">
	<cfproperty name="DBI_ANTENNES" type="string" default="">
	<cfproperty name="TYPE_CRYPTAGE" type="string" default="">
	<cfproperty name="BANDE_FREQUENCE" type="string" default="">
	<cfproperty name="PORTEE_CHAMPS_LIBRE" type="numeric" default="0">
	<cfproperty name="PORTEE_BATIMENT" type="numeric" default="0">
	<cfproperty name="SUPPORT" type="string" default="">
	<cfproperty name="COMPOSITION" type="string" default="">
	<cfproperty name="CABLAGE" type="string" default="">
	<cfproperty name="CATEGORIE_CABLE" type="string" default="">
	<cfproperty name="BLINDAGE" type="string" default="">
	<cfproperty name="INDICATEUR_PROTECTION" type="string" default="">
	<cfproperty name="CHARGE_ADMISSIBLE" type="numeric" default="0">
	<cfproperty name="HAUTEUR_INTERIEURE" type="numeric" default="0">
	<cfproperty name="FORMAT" type="numeric" default="0">
	<cfproperty name="NBR_VENTILATEURS" type="numeric" default="0">
	<cfproperty name="TYPE_VENTILATEURS" type="string" default="">
	<cfproperty name="FORMAT_VENTILATEUR_TOITURE" type="numeric" default="0">
	<cfproperty name="DEBIT_CPL" type="numeric" default="0">
	<cfproperty name="FICHE" type="string" default="">
	<cfproperty name="PORTEE_TYPIQUE" type="string" default="">
	<cfproperty name="EMISSIONS_ELECTROMAGNETIQUES" type="string" default="">
	<cfproperty name="NBR_MONITEUR" type="numeric" default="0">
	<cfproperty name="TECHNOLOGIE_WIRELESS" type="string" default="">
	<cfproperty name="TECHNOLOGIE_IMPRESSION" type="string" default="">
	<cfproperty name="NBR_BAC_ALIMENTATION" type="numeric" default="0">
	<cfproperty name="SONORITE" type="numeric" default="0">
	<cfproperty name="IMPRESSION_RECTO_VERSO" type="numeric" default="0">
	<cfproperty name="TECHNOLOGIE_SCANNER" type="string" default="">
	<cfproperty name="RESOLUTION_OPTIQUE" type="string" default="">
	<cfproperty name="RESOLUTION_INTERPOLEE" type="string" default="">
	<cfproperty name="REDUCTION_MAXIMAL_DOC" type="numeric" default="0">
	<cfproperty name="NBR_COPIE_MAX" type="numeric" default="0">
	<cfproperty name="PAS_PIXEL" type="numeric" default="0">
	<cfproperty name="LUMINOSITE" type="numeric" default="0">
	<cfproperty name="RAPPORT_CONTRASTE" type="string" default="">
	<cfproperty name="CONNEXION_IMPRIMANTE" type="string" default="">
	<cfproperty name="TECHNOLOGIE_CONNECTIVITE" type="string" default="">
	<cfproperty name="PROT_RESEAU" type="string" default="">
	<cfproperty name="PROT_LIAISON_DONNEE" type="string" default="">
	<cfproperty name="PLATERFORME_SUPPORTE" type="string" default="">
	<cfproperty name="NORME_WIFI" type="string" default="">
	<cfproperty name="METHODE_IMPRESSION" type="string" default="">
	<cfproperty name="RESOLUTION_IMPRESSION" type="string" default="">
	<cfproperty name="MEMOIRE_TAMPON_PAGE" type="string" default="">
	<cfproperty name="VITESSE_MODEM" type="string" default="">
	<cfproperty name="SERVICE_VOIP" type="string" default="">
	<cfproperty name="PROTOCOLE_IP" type="string" default="">
	<cfproperty name="VIBREUR" type="numeric" default="0">
	<cfproperty name="NBR_COULEURS" type="numeric" default="0">
	<cfproperty name="DAS" type="numeric" default="0">
	<cfproperty name="VERSION_WAP" type="string" default="">
	<cfproperty name="CARTE_SIM_INTEGRE" type="string" default="">
	<cfproperty name="DRIVER" type="string" default="">
	<cfproperty name="SLOTABLE" type="string" default="">
	<cfproperty name="INTEGRE_A" type="numeric" default="0">
	<cfproperty name="PIN1" type="numeric" default="0">
	<cfproperty name="PIN2" type="numeric" default="0">
	<cfproperty name="PUK1" type="numeric" default="0">
	<cfproperty name="PUK2" type="numeric" default="0">
	<cfproperty name="HLR" type="string" default="">
	<cfproperty name="LANGUE" type="string" default="">
	<cfproperty name="COMPRESSION_VOIE" type="string" default="">
	<cfproperty name="NOMBRE_LIGNE_TEL" type="numeric" default="0">
	<cfproperty name="TYPES_LIGNES" type="string" default="">
	<cfproperty name="TYPES_PASSERELLES" type="string" default="">
	<cfproperty name="SIM_COMPATIBLE" type="numeric" default="0">
	<cfproperty name="TYPE_GSM" type="string" default="">
	<cfproperty name="VITESSE_ROTATION" type="numeric" default="0">
	<cfproperty name="BAIE_DISQUE" type="string" default="">
	<cfproperty name="EXTERNE_INTERNE" type="string" default="">
	<cfproperty name="GRAVEUR" type="string" default="">
	<cfproperty name="VITESSE_ECRITURE_CD" type="numeric" default="0">
	<cfproperty name="VITESSE_LECTURE_CD" type="numeric" default="0">
	<cfproperty name="TEMPS_ACCES_MOYEN" type="numeric" default="0">
	<cfproperty name="NBR_SUPPORT_AMOVIBLE" type="numeric" default="0">
	<cfproperty name="LONGUEUR_CABLE" type="numeric" default="0">
	<cfproperty name="DUREE_EXECUTION" type="numeric" default="0">
	<cfproperty name="CAPACITE_BATTERIE" type="numeric" default="0">
	<cfproperty name="NORME_SURTENSION" type="string" default="">
	<cfproperty name="INDICE_CONSOMMATION_ENERGIE" type="numeric" default="0">
	<cfproperty name="COURANT_ELECTRIQUE_MAXIMAL" type="numeric" default="0">
	<cfproperty name="NBR_BANDES_RESEAU" type="numeric" default="0">
	<cfproperty name="EDGE" type="numeric" default="0">
	<cfproperty name="EQ_3G" type="numeric" default="0">
	<cfproperty name="MODEM_INTEGRE" type="numeric" default="0">
	<cfproperty name="BLUTOOTH" type="numeric" default="0">
	<cfproperty name="INFRAROUGE" type="numeric" default="0">
	<cfproperty name="PLATEFORMES" type="string" default="">
	<cfproperty name="CLIENT_EMAIL" type="numeric" default="0">
	<cfproperty name="MAIN_LIBRE" type="numeric" default="0">
	<cfproperty name="CALLERID_STANDARD" type="string" default="">
	<cfproperty name="MURAL" type="numeric" default="0">
	<cfproperty name="REPONDEUR" type="numeric" default="0">
	<cfproperty name="MANAGEABLE" type="numeric" default="0">
	<cfproperty name="INTERFACE_CONTROLLER" type="string" default="">
	<cfproperty name="TYPE_STOCKAGE" type="string" default="">
	<cfproperty name="NB_UTILISATEUR_LOCAL" type="numeric" default="0">
	<cfproperty name="TEMPERATURE" type="numeric" default="0">
	<cfproperty name="ALIMENTATION" type="numeric" default="0">
	<cfproperty name="AGRANDISSEMENT_MAXIMAL_DOC" type="numeric" default="0">
	<cfproperty name="DEVELOPPEUR" type="string" default="">
	<cfproperty name="EDITEUR" type="string" default="">
	<cfproperty name="RACKABLE" type="numeric" default="0">
	<cfproperty name="VERSION_LOGICIEL" type="string" default="">
	<cfproperty name="NBR_UTILISATEURS_SIMULT" type="string" default="">
	<cfproperty name="CLASSE_GPRS" type="string" default="">

	<cfscript>
		//Initialize the CFC with the default properties values.
		this.IDEQUIPEMENT_FOURNIS = 0;		
		this.IDEQUIPEMENT_FOURNIS_PARENT = 0;
		this.IDTYPE_EQUIPEMENT = 0;
		this.TYPE_EQUIPEMENT = "";	
		this.MODELE = "";	
		this.IDGAMME_FAB = 0;
		this.IDFABRIQUANT = 0;
		this.IDREVENDEUR = 0;
		this.IDMODELE = 0;		
		
		this.IDEQ_PHOTO = 0;
		this.LIBELLE_EQ = "";
		this.REFERENCE_EQ = "";
		this.CODE_INTERNE = "";
		this.COMMENTAIRE_EQ = "";
		this.HAUTEU_U = 0;
		this.POIDS = 0;
		this.BRUIT = 0;
		this.MTBF = 0;
		this.PUISSANCE = 0;
		this.MONTAGE_RACK = 0;
		this.PRIX_CATALOGUE = 0;
		this.QTE_MIN = 0;
		this.MEMOIRE = "";
		this.CAPACITE_CF = "";
		this.EMPLACEMENT_INTERNE = 0;
		this.SUPPORT_POE = "";
		this.NBR_ALIM = "";
		this.SUPPORT_QOS = "";
		this.SUPPORT_VPN = "";
		this.SUPPORT_NAT = "";
		this.NBR_SLOTS_MAX = 0;
		this.NBR_SLOTS_CMM_MAX = 0;
		this.NBR_SLOTS_SFM_MAX = 0;
		this.NBR_ALIM_MAX = 0;
		this.MAX_HEAT_DISSIPATION = 0;
		this.IMEI = "";
		this.DIMENSIONS = "";
		this.AFFICHAGE = "";
		this.RESOLUTION = "";
		this.MEMOIRE_INTERNE = "";
		this.RESEAU = "";
		this.TYPE_CARTE = "";
		this.NB_ENTREE_SA = "";
		this.NB_ENTREE_S = "";
		this.NUM_SIM = "";
		this.DRAM = 0;
		this.RAM = 0;
		this.MEMOIRE_FLASH = 0;
		this.PROT_MANAGEMENT = "";
		this.STANDARDS = "";
		this.INTERFACE = "";
		this.NBPORT = 0;
		this.TAUX_TRANSFERT = 0;
		this.NBCARTE_RESEAU = 0;
		this.TECHNOLOGIE_RAM = "";
		this.MEMOIRE_CACHE = 0;
		this.NBSLOT_RAM = 0;
		this.CHIPSET_CARTE_MERE = "";
		this.CARTE_GRAPHIQUE = "";
		this.CARTE_SON = "";
		this.TYPE_ECRAN = "";
		this.TAILLE_ECRAN = "";
		this.AUTONOMIE_VEILLE = 0;
		this.AUTONOMIE_FONCTIONNEMENT = 0;
		this.TYPE_BATTERIE = "";
		this.VITESSE_IMPRESSION = 0;
		this.TAILLE_MAX_DOCUMENT = "";
		this.NBR_CARTOUCHE = 0;
		this.INDICATEUR_ETATS = "";
		this.TYPE_ALIMENTATION = "";
		this.COULEUR = "";
		this.URL_FABRICANT = "";
		this.PROTOCOLE_CONTROLE_STANDARD = "";
		this.NBR_CANAUX_IP = 0;
		this.NBR_DISQUE_DUR = 0;
		this.TYPE_DISQUE_DUR = "";
		this.VITESSE_ECRITURE = "";
		this.IDCONNECTEUR = 0;
		this.INTERRUPTEUR = 0;
		this.NBR_PRISE_SORTIE = 0;
		this.DISJONCTEUR = 0;
		this.DISPOSITIF_RESEAUX = "";
		this.PROTOCOLE_DE_ROUTAGE = "";
		this.EMPILABLE = 0;
		this.TAILLE_TABLE_ADRESSE_MAC = "";
		this.CAPACITE_COMMUTATION = "";
		this.MODEM_ADSL = "";
		this.TYPE_MODEM = "";
		this.NBR_ANTENNES = 0;
		this.DBI_ANTENNES = "";
		this.TYPE_CRYPTAGE = "";
		this.BANDE_FREQUENCE = "";
		this.PORTEE_CHAMPS_LIBRE = 0;
		this.PORTEE_BATIMENT = 0;
		this.SUPPORT = "";
		this.COMPOSITION = "";
		this.CABLAGE = "";
		this.CATEGORIE_CABLE = "";
		this.BLINDAGE = "";
		this.INDICATEUR_PROTECTION = "";
		this.CHARGE_ADMISSIBLE = 0;
		this.HAUTEUR_INTERIEURE = 0;
		this.FORMAT = 0;
		this.NBR_VENTILATEURS = 0;
		this.TYPE_VENTILATEURS = "";
		this.FORMAT_VENTILATEUR_TOITURE = 0;
		this.DEBIT_CPL = 0;
		this.FICHE = "";
		this.PORTEE_TYPIQUE = "";
		this.EMISSIONS_ELECTROMAGNETIQUES = "";
		this.NBR_MONITEUR = 0;
		this.TECHNOLOGIE_WIRELESS = "";
		this.TECHNOLOGIE_IMPRESSION = "";
		this.NBR_BAC_ALIMENTATION = 0;
		this.SONORITE = 0;
		this.IMPRESSION_RECTO_VERSO = 0;
		this.TECHNOLOGIE_SCANNER = "";
		this.RESOLUTION_OPTIQUE = "";
		this.RESOLUTION_INTERPOLEE = "";
		this.REDUCTION_MAXIMAL_DOC = 0;
		this.NBR_COPIE_MAX = 0;
		this.PAS_PIXEL = 0;
		this.LUMINOSITE = 0;
		this.RAPPORT_CONTRASTE = "";
		this.CONNEXION_IMPRIMANTE = "";
		this.TECHNOLOGIE_CONNECTIVITE = "";
		this.PROT_RESEAU = "";
		this.PROT_LIAISON_DONNEE = "";
		this.PLATERFORME_SUPPORTE = "";
		this.NORME_WIFI = "";
		this.METHODE_IMPRESSION = "";
		this.RESOLUTION_IMPRESSION = "";
		this.MEMOIRE_TAMPON_PAGE = "";
		this.VITESSE_MODEM = "";
		this.SERVICE_VOIP = "";
		this.PROTOCOLE_IP = "";
		this.VIBREUR = 0;
		this.NBR_COULEURS = 0;
		this.DAS = 0;
		this.VERSION_WAP = "";
		this.CARTE_SIM_INTEGRE = "";
		this.DRIVER = "";
		this.SLOTABLE = "";
		this.INTEGRE_A = 0;
		this.PIN1 = 0;
		this.PIN2 = 0;
		this.PUK1 = 0;
		this.PUK2 = 0;
		this.HLR = "";
		this.LANGUE = "";
		this.COMPRESSION_VOIE = "";
		this.NOMBRE_LIGNE_TEL = 0;
		this.TYPES_LIGNES = "";
		this.TYPES_PASSERELLES = "";
		this.SIM_COMPATIBLE = 0;
		this.TYPE_GSM = "";
		this.VITESSE_ROTATION = 0;
		this.BAIE_DISQUE = "";
		this.EXTERNE_INTERNE = "";
		this.GRAVEUR = "";
		this.VITESSE_ECRITURE_CD = 0;
		this.VITESSE_LECTURE_CD = 0;
		this.TEMPS_ACCES_MOYEN = 0;
		this.NBR_SUPPORT_AMOVIBLE = 0;
		this.LONGUEUR_CABLE = 0;
		this.DUREE_EXECUTION = 0;
		this.CAPACITE_BATTERIE = 0;
		this.NORME_SURTENSION = "";
		this.INDICE_CONSOMMATION_ENERGIE = 0;
		this.COURANT_ELECTRIQUE_MAXIMAL = 0;
		this.NBR_BANDES_RESEAU = 0;
		this.EDGE = 0;
		this.EQ_3G = 0;
		this.MODEM_INTEGRE = 0;
		this.BLUTOOTH = 0;
		this.INFRAROUGE = 0;
		this.PLATEFORMES = "";
		this.CLIENT_EMAIL = 0;
		this.MAIN_LIBRE = 0;
		this.CALLERID_STANDARD = "";
		this.MURAL = 0;
		this.REPONDEUR = 0;
		this.MANAGEABLE = 0;
		this.INTERFACE_CONTROLLER = "";
		this.TYPE_STOCKAGE = "";
		this.NB_UTILISATEUR_LOCAL = 0;
		this.TEMPERATURE = 0;
		this.ALIMENTATION = 0;
		this.AGRANDISSEMENT_MAXIMAL_DOC = 0;
		this.DEVELOPPEUR = "";
		this.EDITEUR = "";
		this.RACKABLE = 0;
		this.VERSION_LOGICIEL = "";
		this.NBR_UTILISATEURS_SIMULT = "";
		this.CLASSE_GPRS = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="InfosSuppEquipementFournis">
		<cfargument name="id" required="false">
		<cfscript>
			if( structKeyExists(arguments, "id") )
			{
				load(arguments.id);
			}
			return this;
		</cfscript>
	</cffunction>

	<cffunction name="getIDEQUIPEMENT_FOURNIS" output="false" access="public" returntype="any">
		<cfreturn this.IDEQUIPEMENT_FOURNIS>
	</cffunction>

	<cffunction name="setIDEQUIPEMENT_FOURNIS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDEQUIPEMENT_FOURNIS = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDEQUIPEMENT_FOURNIS_PARENT" output="false" access="public" returntype="any">
		<cfreturn this.IDEQUIPEMENT_FOURNIS_PARENT>
	</cffunction>

	<cffunction name="setIDEQUIPEMENT_FOURNIS_PARENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDEQUIPEMENT_FOURNIS_PARENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDTYPE_EQUIPEMENT" output="false" access="public" returntype="any">
		<cfreturn this.IDTYPE_EQUIPEMENT>
	</cffunction>

	<cffunction name="setIDTYPE_EQUIPEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDTYPE_EQUIPEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getTYPE_EQUIPEMENT" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_EQUIPEMENT>
	</cffunction>

	<cffunction name="setTYPE_EQUIPEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_EQUIPEMENT = arguments.val>
	</cffunction>

	<cffunction name="getIDGAMME_FAB" output="false" access="public" returntype="any">
		<cfreturn this.IDGAMME_FAB>
	</cffunction>

	<cffunction name="setIDGAMME_FAB" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDGAMME_FAB = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDFABRIQUANT" output="false" access="public" returntype="any">
		<cfreturn this.IDFABRIQUANT>
	</cffunction>

	<cffunction name="setIDFABRIQUANT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDFABRIQUANT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDREVENDEUR" output="false" access="public" returntype="any">
		<cfreturn this.IDREVENDEUR>
	</cffunction>

	<cffunction name="setIDREVENDEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDREVENDEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDMODELE" output="false" access="public" returntype="any">
		<cfreturn this.IDMODELE>
	</cffunction>

	<cffunction name="setIDMODELE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDMODELE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getMODELE" output="false" access="public" returntype="any">
		<cfreturn this.MODELE>
	</cffunction>

	<cffunction name="setMODELE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.MODELE = arguments.val>
	</cffunction>

	<cffunction name="getIDEQ_PHOTO" output="false" access="public" returntype="any">
		<cfreturn this.IDEQ_PHOTO>
	</cffunction>

	<cffunction name="setIDEQ_PHOTO" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDEQ_PHOTO = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLIBELLE_EQ" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_EQ>
	</cffunction>

	<cffunction name="setLIBELLE_EQ" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_EQ = arguments.val>
	</cffunction>

	<cffunction name="getREFERENCE_EQ" output="false" access="public" returntype="any">
		<cfreturn this.REFERENCE_EQ>
	</cffunction>

	<cffunction name="setREFERENCE_EQ" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.REFERENCE_EQ = arguments.val>
	</cffunction>

	<cffunction name="getCODE_INTERNE" output="false" access="public" returntype="any">
		<cfreturn this.CODE_INTERNE>
	</cffunction>

	<cffunction name="setCODE_INTERNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CODE_INTERNE = arguments.val>
	</cffunction>

	<cffunction name="getCOMMENTAIRE_EQ" output="false" access="public" returntype="any">
		<cfreturn this.COMMENTAIRE_EQ>
	</cffunction>

	<cffunction name="setCOMMENTAIRE_EQ" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.COMMENTAIRE_EQ = arguments.val>
	</cffunction>

	<cffunction name="getHAUTEU_U" output="false" access="public" returntype="any">
		<cfreturn this.HAUTEU_U>
	</cffunction>

	<cffunction name="setHAUTEU_U" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.HAUTEU_U = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPOIDS" output="false" access="public" returntype="any">
		<cfreturn this.POIDS>
	</cffunction>

	<cffunction name="setPOIDS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.POIDS = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getBRUIT" output="false" access="public" returntype="any">
		<cfreturn this.BRUIT>
	</cffunction>

	<cffunction name="setBRUIT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.BRUIT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMTBF" output="false" access="public" returntype="any">
		<cfreturn this.MTBF>
	</cffunction>

	<cffunction name="setMTBF" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.MTBF = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPUISSANCE" output="false" access="public" returntype="any">
		<cfreturn this.PUISSANCE>
	</cffunction>

	<cffunction name="setPUISSANCE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.PUISSANCE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMONTAGE_RACK" output="false" access="public" returntype="any">
		<cfreturn this.MONTAGE_RACK>
	</cffunction>

	<cffunction name="setMONTAGE_RACK" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.MONTAGE_RACK = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPRIX_CATALOGUE" output="false" access="public" returntype="any">
		<cfreturn this.PRIX_CATALOGUE>
	</cffunction>

	<cffunction name="setPRIX_CATALOGUE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.PRIX_CATALOGUE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getQTE_MIN" output="false" access="public" returntype="any">
		<cfreturn this.QTE_MIN>
	</cffunction>

	<cffunction name="setQTE_MIN" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.QTE_MIN = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMEMOIRE" output="false" access="public" returntype="any">
		<cfreturn this.MEMOIRE>
	</cffunction>

	<cffunction name="setMEMOIRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.MEMOIRE = arguments.val>
	</cffunction>

	<cffunction name="getCAPACITE_CF" output="false" access="public" returntype="any">
		<cfreturn this.CAPACITE_CF>
	</cffunction>

	<cffunction name="setCAPACITE_CF" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CAPACITE_CF = arguments.val>
	</cffunction>

	<cffunction name="getEMPLACEMENT_INTERNE" output="false" access="public" returntype="any">
		<cfreturn this.EMPLACEMENT_INTERNE>
	</cffunction>

	<cffunction name="setEMPLACEMENT_INTERNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.EMPLACEMENT_INTERNE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getSUPPORT_POE" output="false" access="public" returntype="any">
		<cfreturn this.SUPPORT_POE>
	</cffunction>

	<cffunction name="setSUPPORT_POE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.SUPPORT_POE = arguments.val>
	</cffunction>

	<cffunction name="getNBR_ALIM" output="false" access="public" returntype="any">
		<cfreturn this.NBR_ALIM>
	</cffunction>

	<cffunction name="setNBR_ALIM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.NBR_ALIM = arguments.val>
	</cffunction>

	<cffunction name="getSUPPORT_QOS" output="false" access="public" returntype="any">
		<cfreturn this.SUPPORT_QOS>
	</cffunction>

	<cffunction name="setSUPPORT_QOS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.SUPPORT_QOS = arguments.val>
	</cffunction>

	<cffunction name="getSUPPORT_VPN" output="false" access="public" returntype="any">
		<cfreturn this.SUPPORT_VPN>
	</cffunction>

	<cffunction name="setSUPPORT_VPN" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.SUPPORT_VPN = arguments.val>
	</cffunction>

	<cffunction name="getSUPPORT_NAT" output="false" access="public" returntype="any">
		<cfreturn this.SUPPORT_NAT>
	</cffunction>

	<cffunction name="setSUPPORT_NAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.SUPPORT_NAT = arguments.val>
	</cffunction>

	<cffunction name="getNBR_SLOTS_MAX" output="false" access="public" returntype="any">
		<cfreturn this.NBR_SLOTS_MAX>
	</cffunction>

	<cffunction name="setNBR_SLOTS_MAX" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_SLOTS_MAX = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBR_SLOTS_CMM_MAX" output="false" access="public" returntype="any">
		<cfreturn this.NBR_SLOTS_CMM_MAX>
	</cffunction>

	<cffunction name="setNBR_SLOTS_CMM_MAX" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_SLOTS_CMM_MAX = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBR_SLOTS_SFM_MAX" output="false" access="public" returntype="any">
		<cfreturn this.NBR_SLOTS_SFM_MAX>
	</cffunction>

	<cffunction name="setNBR_SLOTS_SFM_MAX" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_SLOTS_SFM_MAX = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBR_ALIM_MAX" output="false" access="public" returntype="any">
		<cfreturn this.NBR_ALIM_MAX>
	</cffunction>

	<cffunction name="setNBR_ALIM_MAX" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_ALIM_MAX = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMAX_HEAT_DISSIPATION" output="false" access="public" returntype="any">
		<cfreturn this.MAX_HEAT_DISSIPATION>
	</cffunction>

	<cffunction name="setMAX_HEAT_DISSIPATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.MAX_HEAT_DISSIPATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIMEI" output="false" access="public" returntype="any">
		<cfreturn this.IMEI>
	</cffunction>

	<cffunction name="setIMEI" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.IMEI = arguments.val>
	</cffunction>

	<cffunction name="getDIMENSIONS" output="false" access="public" returntype="any">
		<cfreturn this.DIMENSIONS>
	</cffunction>

	<cffunction name="setDIMENSIONS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.DIMENSIONS = arguments.val>
	</cffunction>

	<cffunction name="getAFFICHAGE" output="false" access="public" returntype="any">
		<cfreturn this.AFFICHAGE>
	</cffunction>

	<cffunction name="setAFFICHAGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.AFFICHAGE = arguments.val>
	</cffunction>

	<cffunction name="getRESOLUTION" output="false" access="public" returntype="any">
		<cfreturn this.RESOLUTION>
	</cffunction>

	<cffunction name="setRESOLUTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.RESOLUTION = arguments.val>
	</cffunction>

	<cffunction name="getMEMOIRE_INTERNE" output="false" access="public" returntype="any">
		<cfreturn this.MEMOIRE_INTERNE>
	</cffunction>

	<cffunction name="setMEMOIRE_INTERNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.MEMOIRE_INTERNE = arguments.val>
	</cffunction>

	<cffunction name="getRESEAU" output="false" access="public" returntype="any">
		<cfreturn this.RESEAU>
	</cffunction>

	<cffunction name="setRESEAU" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.RESEAU = arguments.val>
	</cffunction>

	<cffunction name="getTYPE_CARTE" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_CARTE>
	</cffunction>

	<cffunction name="setTYPE_CARTE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_CARTE = arguments.val>
	</cffunction>

	<cffunction name="getNB_ENTREE_SA" output="false" access="public" returntype="any">
		<cfreturn this.NB_ENTREE_SA>
	</cffunction>

	<cffunction name="setNB_ENTREE_SA" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.NB_ENTREE_SA = arguments.val>
	</cffunction>

	<cffunction name="getNB_ENTREE_S" output="false" access="public" returntype="any">
		<cfreturn this.NB_ENTREE_S>
	</cffunction>

	<cffunction name="setNB_ENTREE_S" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.NB_ENTREE_S = arguments.val>
	</cffunction>

	<cffunction name="getNUM_SIM" output="false" access="public" returntype="any">
		<cfreturn this.NUM_SIM>
	</cffunction>

	<cffunction name="setNUM_SIM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.NUM_SIM = arguments.val>
	</cffunction>

	<cffunction name="getDRAM" output="false" access="public" returntype="any">
		<cfreturn this.DRAM>
	</cffunction>

	<cffunction name="setDRAM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DRAM = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getRAM" output="false" access="public" returntype="any">
		<cfreturn this.RAM>
	</cffunction>

	<cffunction name="setRAM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.RAM = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMEMOIRE_FLASH" output="false" access="public" returntype="any">
		<cfreturn this.MEMOIRE_FLASH>
	</cffunction>

	<cffunction name="setMEMOIRE_FLASH" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.MEMOIRE_FLASH = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPROT_MANAGEMENT" output="false" access="public" returntype="any">
		<cfreturn this.PROT_MANAGEMENT>
	</cffunction>

	<cffunction name="setPROT_MANAGEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.PROT_MANAGEMENT = arguments.val>
	</cffunction>

	<cffunction name="getSTANDARDS" output="false" access="public" returntype="any">
		<cfreturn this.STANDARDS>
	</cffunction>

	<cffunction name="setSTANDARDS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.STANDARDS = arguments.val>
	</cffunction>

	<cffunction name="getINTERFACE" output="false" access="public" returntype="any">
		<cfreturn this.INTERFACE>
	</cffunction>

	<cffunction name="setINTERFACE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.INTERFACE = arguments.val>
	</cffunction>

	<cffunction name="getNBPORT" output="false" access="public" returntype="any">
		<cfreturn this.NBPORT>
	</cffunction>

	<cffunction name="setNBPORT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBPORT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTAUX_TRANSFERT" output="false" access="public" returntype="any">
		<cfreturn this.TAUX_TRANSFERT>
	</cffunction>

	<cffunction name="setTAUX_TRANSFERT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.TAUX_TRANSFERT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBCARTE_RESEAU" output="false" access="public" returntype="any">
		<cfreturn this.NBCARTE_RESEAU>
	</cffunction>

	<cffunction name="setNBCARTE_RESEAU" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBCARTE_RESEAU = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTECHNOLOGIE_RAM" output="false" access="public" returntype="any">
		<cfreturn this.TECHNOLOGIE_RAM>
	</cffunction>

	<cffunction name="setTECHNOLOGIE_RAM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TECHNOLOGIE_RAM = arguments.val>
	</cffunction>

	<cffunction name="getMEMOIRE_CACHE" output="false" access="public" returntype="any">
		<cfreturn this.MEMOIRE_CACHE>
	</cffunction>

	<cffunction name="setMEMOIRE_CACHE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.MEMOIRE_CACHE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBSLOT_RAM" output="false" access="public" returntype="any">
		<cfreturn this.NBSLOT_RAM>
	</cffunction>

	<cffunction name="setNBSLOT_RAM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBSLOT_RAM = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getCHIPSET_CARTE_MERE" output="false" access="public" returntype="any">
		<cfreturn this.CHIPSET_CARTE_MERE>
	</cffunction>

	<cffunction name="setCHIPSET_CARTE_MERE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CHIPSET_CARTE_MERE = arguments.val>
	</cffunction>

	<cffunction name="getCARTE_GRAPHIQUE" output="false" access="public" returntype="any">
		<cfreturn this.CARTE_GRAPHIQUE>
	</cffunction>

	<cffunction name="setCARTE_GRAPHIQUE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CARTE_GRAPHIQUE = arguments.val>
	</cffunction>

	<cffunction name="getCARTE_SON" output="false" access="public" returntype="any">
		<cfreturn this.CARTE_SON>
	</cffunction>

	<cffunction name="setCARTE_SON" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CARTE_SON = arguments.val>
	</cffunction>

	<cffunction name="getTYPE_ECRAN" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_ECRAN>
	</cffunction>

	<cffunction name="setTYPE_ECRAN" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_ECRAN = arguments.val>
	</cffunction>

	<cffunction name="getTAILLE_ECRAN" output="false" access="public" returntype="any">
		<cfreturn this.TAILLE_ECRAN>
	</cffunction>

	<cffunction name="setTAILLE_ECRAN" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">		
		<cfset this.TAILLE_ECRAN = arguments.val>
	</cffunction>

	<cffunction name="getAUTONOMIE_VEILLE" output="false" access="public" returntype="any">
		<cfreturn this.AUTONOMIE_VEILLE>
	</cffunction>

	<cffunction name="setAUTONOMIE_VEILLE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.AUTONOMIE_VEILLE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getAUTONOMIE_FONCTIONNEMENT" output="false" access="public" returntype="any">
		<cfreturn this.AUTONOMIE_FONCTIONNEMENT>
	</cffunction>

	<cffunction name="setAUTONOMIE_FONCTIONNEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.AUTONOMIE_FONCTIONNEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTYPE_BATTERIE" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_BATTERIE>
	</cffunction>

	<cffunction name="setTYPE_BATTERIE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_BATTERIE = arguments.val>
	</cffunction>

	<cffunction name="getVITESSE_IMPRESSION" output="false" access="public" returntype="any">
		<cfreturn this.VITESSE_IMPRESSION>
	</cffunction>

	<cffunction name="setVITESSE_IMPRESSION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.VITESSE_IMPRESSION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTAILLE_MAX_DOCUMENT" output="false" access="public" returntype="any">
		<cfreturn this.TAILLE_MAX_DOCUMENT>
	</cffunction>

	<cffunction name="setTAILLE_MAX_DOCUMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TAILLE_MAX_DOCUMENT = arguments.val>
	</cffunction>

	<cffunction name="getNBR_CARTOUCHE" output="false" access="public" returntype="any">
		<cfreturn this.NBR_CARTOUCHE>
	</cffunction>

	<cffunction name="setNBR_CARTOUCHE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_CARTOUCHE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getINDICATEUR_ETATS" output="false" access="public" returntype="any">
		<cfreturn this.INDICATEUR_ETATS>
	</cffunction>

	<cffunction name="setINDICATEUR_ETATS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.INDICATEUR_ETATS = arguments.val>
	</cffunction>

	<cffunction name="getTYPE_ALIMENTATION" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_ALIMENTATION>
	</cffunction>

	<cffunction name="setTYPE_ALIMENTATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_ALIMENTATION = arguments.val>
	</cffunction>

	<cffunction name="getCOULEUR" output="false" access="public" returntype="any">
		<cfreturn this.COULEUR>
	</cffunction>

	<cffunction name="setCOULEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.COULEUR = arguments.val>
	</cffunction>

	<cffunction name="getURL_FABRICANT" output="false" access="public" returntype="any">
		<cfreturn this.URL_FABRICANT>
	</cffunction>

	<cffunction name="setURL_FABRICANT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.URL_FABRICANT = arguments.val>
	</cffunction>

	<cffunction name="getPROTOCOLE_CONTROLE_STANDARD" output="false" access="public" returntype="any">
		<cfreturn this.PROTOCOLE_CONTROLE_STANDARD>
	</cffunction>

	<cffunction name="setPROTOCOLE_CONTROLE_STANDARD" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.PROTOCOLE_CONTROLE_STANDARD = arguments.val>
	</cffunction>

	<cffunction name="getNBR_CANAUX_IP" output="false" access="public" returntype="any">
		<cfreturn this.NBR_CANAUX_IP>
	</cffunction>

	<cffunction name="setNBR_CANAUX_IP" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_CANAUX_IP = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBR_DISQUE_DUR" output="false" access="public" returntype="any">
		<cfreturn this.NBR_DISQUE_DUR>
	</cffunction>

	<cffunction name="setNBR_DISQUE_DUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_DISQUE_DUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTYPE_DISQUE_DUR" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_DISQUE_DUR>
	</cffunction>

	<cffunction name="setTYPE_DISQUE_DUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_DISQUE_DUR = arguments.val>
	</cffunction>

	<cffunction name="getVITESSE_ECRITURE" output="false" access="public" returntype="any">
		<cfreturn this.VITESSE_ECRITURE>
	</cffunction>

	<cffunction name="setVITESSE_ECRITURE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.VITESSE_ECRITURE = arguments.val>
	</cffunction>

	<cffunction name="getIDCONNECTEUR" output="false" access="public" returntype="any">
		<cfreturn this.IDCONNECTEUR>
	</cffunction>

	<cffunction name="setIDCONNECTEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDCONNECTEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getINTERRUPTEUR" output="false" access="public" returntype="any">
		<cfreturn this.INTERRUPTEUR>
	</cffunction>

	<cffunction name="setINTERRUPTEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.INTERRUPTEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBR_PRISE_SORTIE" output="false" access="public" returntype="any">
		<cfreturn this.NBR_PRISE_SORTIE>
	</cffunction>

	<cffunction name="setNBR_PRISE_SORTIE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_PRISE_SORTIE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDISJONCTEUR" output="false" access="public" returntype="any">
		<cfreturn this.DISJONCTEUR>
	</cffunction>

	<cffunction name="setDISJONCTEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DISJONCTEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDISPOSITIF_RESEAUX" output="false" access="public" returntype="any">
		<cfreturn this.DISPOSITIF_RESEAUX>
	</cffunction>

	<cffunction name="setDISPOSITIF_RESEAUX" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.DISPOSITIF_RESEAUX = arguments.val>
	</cffunction>

	<cffunction name="getPROTOCOLE_DE_ROUTAGE" output="false" access="public" returntype="any">
		<cfreturn this.PROTOCOLE_DE_ROUTAGE>
	</cffunction>

	<cffunction name="setPROTOCOLE_DE_ROUTAGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.PROTOCOLE_DE_ROUTAGE = arguments.val>
	</cffunction>

	<cffunction name="getEMPILABLE" output="false" access="public" returntype="any">
		<cfreturn this.EMPILABLE>
	</cffunction>

	<cffunction name="setEMPILABLE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.EMPILABLE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTAILLE_TABLE_ADRESSE_MAC" output="false" access="public" returntype="any">
		<cfreturn this.TAILLE_TABLE_ADRESSE_MAC>
	</cffunction>

	<cffunction name="setTAILLE_TABLE_ADRESSE_MAC" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TAILLE_TABLE_ADRESSE_MAC = arguments.val>
	</cffunction>

	<cffunction name="getCAPACITE_COMMUTATION" output="false" access="public" returntype="any">
		<cfreturn this.CAPACITE_COMMUTATION>
	</cffunction>

	<cffunction name="setCAPACITE_COMMUTATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CAPACITE_COMMUTATION = arguments.val>
	</cffunction>

	<cffunction name="getMODEM_ADSL" output="false" access="public" returntype="any">
		<cfreturn this.MODEM_ADSL>
	</cffunction>

	<cffunction name="setMODEM_ADSL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.MODEM_ADSL = arguments.val>
	</cffunction>

	<cffunction name="getTYPE_MODEM" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_MODEM>
	</cffunction>

	<cffunction name="setTYPE_MODEM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_MODEM = arguments.val>
	</cffunction>

	<cffunction name="getNBR_ANTENNES" output="false" access="public" returntype="any">
		<cfreturn this.NBR_ANTENNES>
	</cffunction>

	<cffunction name="setNBR_ANTENNES" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_ANTENNES = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDBI_ANTENNES" output="false" access="public" returntype="any">
		<cfreturn this.DBI_ANTENNES>
	</cffunction>

	<cffunction name="setDBI_ANTENNES" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.DBI_ANTENNES = arguments.val>
	</cffunction>

	<cffunction name="getTYPE_CRYPTAGE" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_CRYPTAGE>
	</cffunction>

	<cffunction name="setTYPE_CRYPTAGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_CRYPTAGE = arguments.val>
	</cffunction>

	<cffunction name="getBANDE_FREQUENCE" output="false" access="public" returntype="any">
		<cfreturn this.BANDE_FREQUENCE>
	</cffunction>

	<cffunction name="setBANDE_FREQUENCE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.BANDE_FREQUENCE = arguments.val>
	</cffunction>

	<cffunction name="getPORTEE_CHAMPS_LIBRE" output="false" access="public" returntype="any">
		<cfreturn this.PORTEE_CHAMPS_LIBRE>
	</cffunction>

	<cffunction name="setPORTEE_CHAMPS_LIBRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.PORTEE_CHAMPS_LIBRE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPORTEE_BATIMENT" output="false" access="public" returntype="any">
		<cfreturn this.PORTEE_BATIMENT>
	</cffunction>

	<cffunction name="setPORTEE_BATIMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.PORTEE_BATIMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getSUPPORT" output="false" access="public" returntype="any">
		<cfreturn this.SUPPORT>
	</cffunction>

	<cffunction name="setSUPPORT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.SUPPORT = arguments.val>
	</cffunction>

	<cffunction name="getCOMPOSITION" output="false" access="public" returntype="any">
		<cfreturn this.COMPOSITION>
	</cffunction>

	<cffunction name="setCOMPOSITION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.COMPOSITION = arguments.val>
	</cffunction>

	<cffunction name="getCABLAGE" output="false" access="public" returntype="any">
		<cfreturn this.CABLAGE>
	</cffunction>

	<cffunction name="setCABLAGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CABLAGE = arguments.val>
	</cffunction>

	<cffunction name="getCATEGORIE_CABLE" output="false" access="public" returntype="any">
		<cfreturn this.CATEGORIE_CABLE>
	</cffunction>

	<cffunction name="setCATEGORIE_CABLE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CATEGORIE_CABLE = arguments.val>
	</cffunction>

	<cffunction name="getBLINDAGE" output="false" access="public" returntype="any">
		<cfreturn this.BLINDAGE>
	</cffunction>

	<cffunction name="setBLINDAGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.BLINDAGE = arguments.val>
	</cffunction>

	<cffunction name="getINDICATEUR_PROTECTION" output="false" access="public" returntype="any">
		<cfreturn this.INDICATEUR_PROTECTION>
	</cffunction>

	<cffunction name="setINDICATEUR_PROTECTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.INDICATEUR_PROTECTION = arguments.val>
	</cffunction>

	<cffunction name="getCHARGE_ADMISSIBLE" output="false" access="public" returntype="any">
		<cfreturn this.CHARGE_ADMISSIBLE>
	</cffunction>

	<cffunction name="setCHARGE_ADMISSIBLE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.CHARGE_ADMISSIBLE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getHAUTEUR_INTERIEURE" output="false" access="public" returntype="any">
		<cfreturn this.HAUTEUR_INTERIEURE>
	</cffunction>

	<cffunction name="setHAUTEUR_INTERIEURE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.HAUTEUR_INTERIEURE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getFORMAT" output="false" access="public" returntype="any">
		<cfreturn this.FORMAT>
	</cffunction>

	<cffunction name="setFORMAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.FORMAT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBR_VENTILATEURS" output="false" access="public" returntype="any">
		<cfreturn this.NBR_VENTILATEURS>
	</cffunction>

	<cffunction name="setNBR_VENTILATEURS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_VENTILATEURS = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTYPE_VENTILATEURS" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_VENTILATEURS>
	</cffunction>

	<cffunction name="setTYPE_VENTILATEURS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_VENTILATEURS = arguments.val>
	</cffunction>

	<cffunction name="getFORMAT_VENTILATEUR_TOITURE" output="false" access="public" returntype="any">
		<cfreturn this.FORMAT_VENTILATEUR_TOITURE>
	</cffunction>

	<cffunction name="setFORMAT_VENTILATEUR_TOITURE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.FORMAT_VENTILATEUR_TOITURE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDEBIT_CPL" output="false" access="public" returntype="any">
		<cfreturn this.DEBIT_CPL>
	</cffunction>

	<cffunction name="setDEBIT_CPL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DEBIT_CPL = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getFICHE" output="false" access="public" returntype="any">
		<cfreturn this.FICHE>
	</cffunction>

	<cffunction name="setFICHE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.FICHE = arguments.val>
	</cffunction>

	<cffunction name="getPORTEE_TYPIQUE" output="false" access="public" returntype="any">
		<cfreturn this.PORTEE_TYPIQUE>
	</cffunction>

	<cffunction name="setPORTEE_TYPIQUE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.PORTEE_TYPIQUE = arguments.val>
	</cffunction>

	<cffunction name="getEMISSIONS_ELECTROMAGNETIQUES" output="false" access="public" returntype="any">
		<cfreturn this.EMISSIONS_ELECTROMAGNETIQUES>
	</cffunction>

	<cffunction name="setEMISSIONS_ELECTROMAGNETIQUES" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.EMISSIONS_ELECTROMAGNETIQUES = arguments.val>
	</cffunction>

	<cffunction name="getNBR_MONITEUR" output="false" access="public" returntype="any">
		<cfreturn this.NBR_MONITEUR>
	</cffunction>

	<cffunction name="setNBR_MONITEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_MONITEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTECHNOLOGIE_WIRELESS" output="false" access="public" returntype="any">
		<cfreturn this.TECHNOLOGIE_WIRELESS>
	</cffunction>

	<cffunction name="setTECHNOLOGIE_WIRELESS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TECHNOLOGIE_WIRELESS = arguments.val>
	</cffunction>

	<cffunction name="getTECHNOLOGIE_IMPRESSION" output="false" access="public" returntype="any">
		<cfreturn this.TECHNOLOGIE_IMPRESSION>
	</cffunction>

	<cffunction name="setTECHNOLOGIE_IMPRESSION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TECHNOLOGIE_IMPRESSION = arguments.val>
	</cffunction>

	<cffunction name="getNBR_BAC_ALIMENTATION" output="false" access="public" returntype="any">
		<cfreturn this.NBR_BAC_ALIMENTATION>
	</cffunction>

	<cffunction name="setNBR_BAC_ALIMENTATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_BAC_ALIMENTATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getSONORITE" output="false" access="public" returntype="any">
		<cfreturn this.SONORITE>
	</cffunction>

	<cffunction name="setSONORITE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.SONORITE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIMPRESSION_RECTO_VERSO" output="false" access="public" returntype="any">
		<cfreturn this.IMPRESSION_RECTO_VERSO>
	</cffunction>

	<cffunction name="setIMPRESSION_RECTO_VERSO" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IMPRESSION_RECTO_VERSO = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTECHNOLOGIE_SCANNER" output="false" access="public" returntype="any">
		<cfreturn this.TECHNOLOGIE_SCANNER>
	</cffunction>

	<cffunction name="setTECHNOLOGIE_SCANNER" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TECHNOLOGIE_SCANNER = arguments.val>
	</cffunction>

	<cffunction name="getRESOLUTION_OPTIQUE" output="false" access="public" returntype="any">
		<cfreturn this.RESOLUTION_OPTIQUE>
	</cffunction>

	<cffunction name="setRESOLUTION_OPTIQUE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.RESOLUTION_OPTIQUE = arguments.val>
	</cffunction>

	<cffunction name="getRESOLUTION_INTERPOLEE" output="false" access="public" returntype="any">
		<cfreturn this.RESOLUTION_INTERPOLEE>
	</cffunction>

	<cffunction name="setRESOLUTION_INTERPOLEE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.RESOLUTION_INTERPOLEE = arguments.val>
	</cffunction>

	<cffunction name="getREDUCTION_MAXIMAL_DOC" output="false" access="public" returntype="any">
		<cfreturn this.REDUCTION_MAXIMAL_DOC>
	</cffunction>

	<cffunction name="setREDUCTION_MAXIMAL_DOC" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.REDUCTION_MAXIMAL_DOC = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBR_COPIE_MAX" output="false" access="public" returntype="any">
		<cfreturn this.NBR_COPIE_MAX>
	</cffunction>

	<cffunction name="setNBR_COPIE_MAX" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_COPIE_MAX = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPAS_PIXEL" output="false" access="public" returntype="any">
		<cfreturn this.PAS_PIXEL>
	</cffunction>

	<cffunction name="setPAS_PIXEL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.PAS_PIXEL = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLUMINOSITE" output="false" access="public" returntype="any">
		<cfreturn this.LUMINOSITE>
	</cffunction>

	<cffunction name="setLUMINOSITE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.LUMINOSITE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getRAPPORT_CONTRASTE" output="false" access="public" returntype="any">
		<cfreturn this.RAPPORT_CONTRASTE>
	</cffunction>

	<cffunction name="setRAPPORT_CONTRASTE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.RAPPORT_CONTRASTE = arguments.val>
	</cffunction>

	<cffunction name="getCONNEXION_IMPRIMANTE" output="false" access="public" returntype="any">
		<cfreturn this.CONNEXION_IMPRIMANTE>
	</cffunction>

	<cffunction name="setCONNEXION_IMPRIMANTE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CONNEXION_IMPRIMANTE = arguments.val>
	</cffunction>

	<cffunction name="getTECHNOLOGIE_CONNECTIVITE" output="false" access="public" returntype="any">
		<cfreturn this.TECHNOLOGIE_CONNECTIVITE>
	</cffunction>

	<cffunction name="setTECHNOLOGIE_CONNECTIVITE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TECHNOLOGIE_CONNECTIVITE = arguments.val>
	</cffunction>

	<cffunction name="getPROT_RESEAU" output="false" access="public" returntype="any">
		<cfreturn this.PROT_RESEAU>
	</cffunction>

	<cffunction name="setPROT_RESEAU" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.PROT_RESEAU = arguments.val>
	</cffunction>

	<cffunction name="getPROT_LIAISON_DONNEE" output="false" access="public" returntype="any">
		<cfreturn this.PROT_LIAISON_DONNEE>
	</cffunction>

	<cffunction name="setPROT_LIAISON_DONNEE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.PROT_LIAISON_DONNEE = arguments.val>
	</cffunction>

	<cffunction name="getPLATERFORME_SUPPORTE" output="false" access="public" returntype="any">
		<cfreturn this.PLATERFORME_SUPPORTE>
	</cffunction>

	<cffunction name="setPLATERFORME_SUPPORTE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.PLATERFORME_SUPPORTE = arguments.val>
	</cffunction>

	<cffunction name="getNORME_WIFI" output="false" access="public" returntype="any">
		<cfreturn this.NORME_WIFI>
	</cffunction>

	<cffunction name="setNORME_WIFI" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.NORME_WIFI = arguments.val>
	</cffunction>

	<cffunction name="getMETHODE_IMPRESSION" output="false" access="public" returntype="any">
		<cfreturn this.METHODE_IMPRESSION>
	</cffunction>

	<cffunction name="setMETHODE_IMPRESSION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.METHODE_IMPRESSION = arguments.val>
	</cffunction>

	<cffunction name="getRESOLUTION_IMPRESSION" output="false" access="public" returntype="any">
		<cfreturn this.RESOLUTION_IMPRESSION>
	</cffunction>

	<cffunction name="setRESOLUTION_IMPRESSION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.RESOLUTION_IMPRESSION = arguments.val>
	</cffunction>

	<cffunction name="getMEMOIRE_TAMPON_PAGE" output="false" access="public" returntype="any">
		<cfreturn this.MEMOIRE_TAMPON_PAGE>
	</cffunction>

	<cffunction name="setMEMOIRE_TAMPON_PAGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.MEMOIRE_TAMPON_PAGE = arguments.val>
	</cffunction>

	<cffunction name="getVITESSE_MODEM" output="false" access="public" returntype="any">
		<cfreturn this.VITESSE_MODEM>
	</cffunction>

	<cffunction name="setVITESSE_MODEM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.VITESSE_MODEM = arguments.val>
	</cffunction>

	<cffunction name="getSERVICE_VOIP" output="false" access="public" returntype="any">
		<cfreturn this.SERVICE_VOIP>
	</cffunction>

	<cffunction name="setSERVICE_VOIP" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.SERVICE_VOIP = arguments.val>
	</cffunction>

	<cffunction name="getPROTOCOLE_IP" output="false" access="public" returntype="any">
		<cfreturn this.PROTOCOLE_IP>
	</cffunction>

	<cffunction name="setPROTOCOLE_IP" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.PROTOCOLE_IP = arguments.val>
	</cffunction>

	<cffunction name="getVIBREUR" output="false" access="public" returntype="any">
		<cfreturn this.VIBREUR>
	</cffunction>

	<cffunction name="setVIBREUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.VIBREUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBR_COULEURS" output="false" access="public" returntype="any">
		<cfreturn this.NBR_COULEURS>
	</cffunction>

	<cffunction name="setNBR_COULEURS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_COULEURS = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDAS" output="false" access="public" returntype="any">
		<cfreturn this.DAS>
	</cffunction>

	<cffunction name="setDAS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DAS = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getVERSION_WAP" output="false" access="public" returntype="any">
		<cfreturn this.VERSION_WAP>
	</cffunction>

	<cffunction name="setVERSION_WAP" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.VERSION_WAP = arguments.val>
	</cffunction>

	<cffunction name="getCARTE_SIM_INTEGRE" output="false" access="public" returntype="any">
		<cfreturn this.CARTE_SIM_INTEGRE>
	</cffunction>

	<cffunction name="setCARTE_SIM_INTEGRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CARTE_SIM_INTEGRE = arguments.val>
	</cffunction>

	<cffunction name="getDRIVER" output="false" access="public" returntype="any">
		<cfreturn this.DRIVER>
	</cffunction>

	<cffunction name="setDRIVER" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.DRIVER = arguments.val>
	</cffunction>

	<cffunction name="getSLOTABLE" output="false" access="public" returntype="any">
		<cfreturn this.SLOTABLE>
	</cffunction>

	<cffunction name="setSLOTABLE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.SLOTABLE = arguments.val>
	</cffunction>

	<cffunction name="getINTEGRE_A" output="false" access="public" returntype="any">
		<cfreturn this.INTEGRE_A>
	</cffunction>

	<cffunction name="setINTEGRE_A" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.INTEGRE_A = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPIN1" output="false" access="public" returntype="any">
		<cfreturn this.PIN1>
	</cffunction>

	<cffunction name="setPIN1" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.PIN1 = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPIN2" output="false" access="public" returntype="any">
		<cfreturn this.PIN2>
	</cffunction>

	<cffunction name="setPIN2" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.PIN2 = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPUK1" output="false" access="public" returntype="any">
		<cfreturn this.PUK1>
	</cffunction>

	<cffunction name="setPUK1" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.PUK1 = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPUK2" output="false" access="public" returntype="any">
		<cfreturn this.PUK2>
	</cffunction>

	<cffunction name="setPUK2" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.PUK2 = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getHLR" output="false" access="public" returntype="any">
		<cfreturn this.HLR>
	</cffunction>

	<cffunction name="setHLR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.HLR = arguments.val>
	</cffunction>

	<cffunction name="getLANGUE" output="false" access="public" returntype="any">
		<cfreturn this.LANGUE>
	</cffunction>

	<cffunction name="setLANGUE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LANGUE = arguments.val>
	</cffunction>

	<cffunction name="getCOMPRESSION_VOIE" output="false" access="public" returntype="any">
		<cfreturn this.COMPRESSION_VOIE>
	</cffunction>

	<cffunction name="setCOMPRESSION_VOIE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.COMPRESSION_VOIE = arguments.val>
	</cffunction>

	<cffunction name="getNOMBRE_LIGNE_TEL" output="false" access="public" returntype="any">
		<cfreturn this.NOMBRE_LIGNE_TEL>
	</cffunction>

	<cffunction name="setNOMBRE_LIGNE_TEL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NOMBRE_LIGNE_TEL = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTYPES_LIGNES" output="false" access="public" returntype="any">
		<cfreturn this.TYPES_LIGNES>
	</cffunction>

	<cffunction name="setTYPES_LIGNES" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPES_LIGNES = arguments.val>
	</cffunction>

	<cffunction name="getTYPES_PASSERELLES" output="false" access="public" returntype="any">
		<cfreturn this.TYPES_PASSERELLES>
	</cffunction>

	<cffunction name="setTYPES_PASSERELLES" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPES_PASSERELLES = arguments.val>
	</cffunction>

	<cffunction name="getSIM_COMPATIBLE" output="false" access="public" returntype="any">
		<cfreturn this.SIM_COMPATIBLE>
	</cffunction>

	<cffunction name="setSIM_COMPATIBLE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.SIM_COMPATIBLE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTYPE_GSM" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_GSM>
	</cffunction>

	<cffunction name="setTYPE_GSM" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_GSM = arguments.val>
	</cffunction>

	<cffunction name="getVITESSE_ROTATION" output="false" access="public" returntype="any">
		<cfreturn this.VITESSE_ROTATION>
	</cffunction>

	<cffunction name="setVITESSE_ROTATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.VITESSE_ROTATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getBAIE_DISQUE" output="false" access="public" returntype="any">
		<cfreturn this.BAIE_DISQUE>
	</cffunction>

	<cffunction name="setBAIE_DISQUE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.BAIE_DISQUE = arguments.val>
	</cffunction>

	<cffunction name="getEXTERNE_INTERNE" output="false" access="public" returntype="any">
		<cfreturn this.EXTERNE_INTERNE>
	</cffunction>

	<cffunction name="setEXTERNE_INTERNE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.EXTERNE_INTERNE = arguments.val>
	</cffunction>

	<cffunction name="getGRAVEUR" output="false" access="public" returntype="any">
		<cfreturn this.GRAVEUR>
	</cffunction>

	<cffunction name="setGRAVEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.GRAVEUR = arguments.val>
	</cffunction>

	<cffunction name="getVITESSE_ECRITURE_CD" output="false" access="public" returntype="any">
		<cfreturn this.VITESSE_ECRITURE_CD>
	</cffunction>

	<cffunction name="setVITESSE_ECRITURE_CD" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.VITESSE_ECRITURE_CD = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getVITESSE_LECTURE_CD" output="false" access="public" returntype="any">
		<cfreturn this.VITESSE_LECTURE_CD>
	</cffunction>

	<cffunction name="setVITESSE_LECTURE_CD" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.VITESSE_LECTURE_CD = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTEMPS_ACCES_MOYEN" output="false" access="public" returntype="any">
		<cfreturn this.TEMPS_ACCES_MOYEN>
	</cffunction>

	<cffunction name="setTEMPS_ACCES_MOYEN" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.TEMPS_ACCES_MOYEN = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBR_SUPPORT_AMOVIBLE" output="false" access="public" returntype="any">
		<cfreturn this.NBR_SUPPORT_AMOVIBLE>
	</cffunction>

	<cffunction name="setNBR_SUPPORT_AMOVIBLE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_SUPPORT_AMOVIBLE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLONGUEUR_CABLE" output="false" access="public" returntype="any">
		<cfreturn this.LONGUEUR_CABLE>
	</cffunction>

	<cffunction name="setLONGUEUR_CABLE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.LONGUEUR_CABLE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDUREE_EXECUTION" output="false" access="public" returntype="any">
		<cfreturn this.DUREE_EXECUTION>
	</cffunction>

	<cffunction name="setDUREE_EXECUTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DUREE_EXECUTION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getCAPACITE_BATTERIE" output="false" access="public" returntype="any">
		<cfreturn this.CAPACITE_BATTERIE>
	</cffunction>

	<cffunction name="setCAPACITE_BATTERIE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.CAPACITE_BATTERIE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNORME_SURTENSION" output="false" access="public" returntype="any">
		<cfreturn this.NORME_SURTENSION>
	</cffunction>

	<cffunction name="setNORME_SURTENSION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.NORME_SURTENSION = arguments.val>
	</cffunction>

	<cffunction name="getINDICE_CONSOMMATION_ENERGIE" output="false" access="public" returntype="any">
		<cfreturn this.INDICE_CONSOMMATION_ENERGIE>
	</cffunction>

	<cffunction name="setINDICE_CONSOMMATION_ENERGIE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.INDICE_CONSOMMATION_ENERGIE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getCOURANT_ELECTRIQUE_MAXIMAL" output="false" access="public" returntype="any">
		<cfreturn this.COURANT_ELECTRIQUE_MAXIMAL>
	</cffunction>

	<cffunction name="setCOURANT_ELECTRIQUE_MAXIMAL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.COURANT_ELECTRIQUE_MAXIMAL = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNBR_BANDES_RESEAU" output="false" access="public" returntype="any">
		<cfreturn this.NBR_BANDES_RESEAU>
	</cffunction>

	<cffunction name="setNBR_BANDES_RESEAU" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NBR_BANDES_RESEAU = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getEDGE" output="false" access="public" returntype="any">
		<cfreturn this.EDGE>
	</cffunction>

	<cffunction name="setEDGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.EDGE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getEQ_3G" output="false" access="public" returntype="any">
		<cfreturn this.EQ_3G>
	</cffunction>

	<cffunction name="setEQ_3G" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.EQ_3G = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMODEM_INTEGRE" output="false" access="public" returntype="any">
		<cfreturn this.MODEM_INTEGRE>
	</cffunction>

	<cffunction name="setMODEM_INTEGRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.MODEM_INTEGRE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getBLUTOOTH" output="false" access="public" returntype="any">
		<cfreturn this.BLUTOOTH>
	</cffunction>

	<cffunction name="setBLUTOOTH" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.BLUTOOTH = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getINFRAROUGE" output="false" access="public" returntype="any">
		<cfreturn this.INFRAROUGE>
	</cffunction>

	<cffunction name="setINFRAROUGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.INFRAROUGE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPLATEFORMES" output="false" access="public" returntype="any">
		<cfreturn this.PLATEFORMES>
	</cffunction>

	<cffunction name="setPLATEFORMES" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.PLATEFORMES = arguments.val>
	</cffunction>

	<cffunction name="getCLIENT_EMAIL" output="false" access="public" returntype="any">
		<cfreturn this.CLIENT_EMAIL>
	</cffunction>

	<cffunction name="setCLIENT_EMAIL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.CLIENT_EMAIL = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMAIN_LIBRE" output="false" access="public" returntype="any">
		<cfreturn this.MAIN_LIBRE>
	</cffunction>

	<cffunction name="setMAIN_LIBRE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.MAIN_LIBRE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getCALLERID_STANDARD" output="false" access="public" returntype="any">
		<cfreturn this.CALLERID_STANDARD>
	</cffunction>

	<cffunction name="setCALLERID_STANDARD" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CALLERID_STANDARD = arguments.val>
	</cffunction>

	<cffunction name="getMURAL" output="false" access="public" returntype="any">
		<cfreturn this.MURAL>
	</cffunction>

	<cffunction name="setMURAL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.MURAL = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getREPONDEUR" output="false" access="public" returntype="any">
		<cfreturn this.REPONDEUR>
	</cffunction>

	<cffunction name="setREPONDEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.REPONDEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMANAGEABLE" output="false" access="public" returntype="any">
		<cfreturn this.MANAGEABLE>
	</cffunction>

	<cffunction name="setMANAGEABLE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.MANAGEABLE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getINTERFACE_CONTROLLER" output="false" access="public" returntype="any">
		<cfreturn this.INTERFACE_CONTROLLER>
	</cffunction>

	<cffunction name="setINTERFACE_CONTROLLER" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.INTERFACE_CONTROLLER = arguments.val>
	</cffunction>

	<cffunction name="getTYPE_STOCKAGE" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_STOCKAGE>
	</cffunction>

	<cffunction name="setTYPE_STOCKAGE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_STOCKAGE = arguments.val>
	</cffunction>

	<cffunction name="getNB_UTILISATEUR_LOCAL" output="false" access="public" returntype="any">
		<cfreturn this.NB_UTILISATEUR_LOCAL>
	</cffunction>

	<cffunction name="setNB_UTILISATEUR_LOCAL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NB_UTILISATEUR_LOCAL = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTEMPERATURE" output="false" access="public" returntype="any">
		<cfreturn this.TEMPERATURE>
	</cffunction>

	<cffunction name="setTEMPERATURE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.TEMPERATURE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getALIMENTATION" output="false" access="public" returntype="any">
		<cfreturn this.ALIMENTATION>
	</cffunction>

	<cffunction name="setALIMENTATION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.ALIMENTATION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getAGRANDISSEMENT_MAXIMAL_DOC" output="false" access="public" returntype="any">
		<cfreturn this.AGRANDISSEMENT_MAXIMAL_DOC>
	</cffunction>

	<cffunction name="setAGRANDISSEMENT_MAXIMAL_DOC" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.AGRANDISSEMENT_MAXIMAL_DOC = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDEVELOPPEUR" output="false" access="public" returntype="any">
		<cfreturn this.DEVELOPPEUR>
	</cffunction>

	<cffunction name="setDEVELOPPEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.DEVELOPPEUR = arguments.val>
	</cffunction>

	<cffunction name="getEDITEUR" output="false" access="public" returntype="any">
		<cfreturn this.EDITEUR>
	</cffunction>

	<cffunction name="setEDITEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.EDITEUR = arguments.val>
	</cffunction>

	<cffunction name="getRACKABLE" output="false" access="public" returntype="any">
		<cfreturn this.RACKABLE>
	</cffunction>

	<cffunction name="setRACKABLE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.RACKABLE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getVERSION_LOGICIEL" output="false" access="public" returntype="any">
		<cfreturn this.VERSION_LOGICIEL>
	</cffunction>

	<cffunction name="setVERSION_LOGICIEL" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.VERSION_LOGICIEL = arguments.val>
	</cffunction>

	<cffunction name="getNBR_UTILISATEURS_SIMULT" output="false" access="public" returntype="any">
		<cfreturn this.NBR_UTILISATEURS_SIMULT>
	</cffunction>

	<cffunction name="setNBR_UTILISATEURS_SIMULT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.NBR_UTILISATEURS_SIMULT = arguments.val>
	</cffunction>

	<cffunction name="getCLASSE_GPRS" output="false" access="public" returntype="any">
		<cfreturn this.CLASSE_GPRS>
	</cffunction>

	<cffunction name="setCLASSE_GPRS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CLASSE_GPRS = arguments.val>
	</cffunction>



	<cffunction name="save" output="false" access="public" returntype="numeric">
		<cfscript>
			if(this.getIDEQUIPEMENT_FOURNIS() gt 0)
			{	
				return update();
			}else{
				return -5;
			}
		</cfscript>
	</cffunction>



	<cffunction name="load" output="false" access="public" returntype="void">
		<cfargument name="id" required="true" >
		<cfset var qRead="">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_Infosup_Eq_Fournis">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#id#">
			<cfprocresult name="qRead">			
		</cfstoredproc>

		<cfscript>
			this.setIDEQUIPEMENT_FOURNIS(id);
			
			this.setMODELE(qRead.LIBELLE_MODELE);
			this.setTYPE_EQUIPEMENT(qRead.TYPE_EQUIPEMENT);
			
			this.setIDEQUIPEMENT_FOURNIS_PARENT(qRead.IDEQUIPEMENT_FOURNIS_PARENT);
			this.setIDTYPE_EQUIPEMENT(qRead.IDTYPE_EQUIPEMENT);
			this.setIDGAMME_FAB(qRead.IDGAMME_FAB);
			this.setIDFABRIQUANT(qRead.IDFABRIQUANT);
			this.setIDREVENDEUR(qRead.IDREVENDEUR);
			this.setIDMODELE(qRead.IDMODELE);
			this.setIDEQ_PHOTO(qRead.IDEQ_PHOTO);
			this.setLIBELLE_EQ(qRead.LIBELLE_EQ);
			this.setREFERENCE_EQ(qRead.REFERENCE_EQ);
			this.setCODE_INTERNE(qRead.CODE_INTERNE);
			this.setCOMMENTAIRE_EQ(qRead.COMMENTAIRE_EQ);
			this.setHAUTEU_U(qRead.HAUTEU_U);
			this.setPOIDS(qRead.POIDS);
			this.setBRUIT(qRead.BRUIT);
			this.setMTBF(qRead.MTBF);
			this.setPUISSANCE(qRead.PUISSANCE);
			this.setMONTAGE_RACK(qRead.MONTAGE_RACK);
			this.setPRIX_CATALOGUE(qRead.PRIX_CATALOGUE);
			this.setQTE_MIN(qRead.QTE_MIN);
			this.setMEMOIRE(qRead.MEMOIRE);
			this.setCAPACITE_CF(qRead.CAPACITE_CF);
			this.setEMPLACEMENT_INTERNE(qRead.EMPLACEMENT_INTERNE);
			this.setSUPPORT_POE(qRead.SUPPORT_POE);
			this.setNBR_ALIM(qRead.NBR_ALIM);
			this.setSUPPORT_QOS(qRead.SUPPORT_QOS);
			this.setSUPPORT_VPN(qRead.SUPPORT_VPN);
			this.setSUPPORT_NAT(qRead.SUPPORT_NAT);
			this.setNBR_SLOTS_MAX(qRead.NBR_SLOTS_MAX);
			this.setNBR_SLOTS_CMM_MAX(qRead.NBR_SLOTS_CMM_MAX);
			this.setNBR_SLOTS_SFM_MAX(qRead.NBR_SLOTS_SFM_MAX);
			this.setNBR_ALIM_MAX(qRead.NBR_ALIM_MAX);
			this.setMAX_HEAT_DISSIPATION(qRead.MAX_HEAT_DISSIPATION);
			this.setIMEI(qRead.IMEI);
			this.setDIMENSIONS(qRead.DIMENSIONS);
			this.setAFFICHAGE(qRead.AFFICHAGE);
			this.setRESOLUTION(qRead.RESOLUTION);
			this.setMEMOIRE_INTERNE(qRead.MEMOIRE_INTERNE);
			this.setRESEAU(qRead.RESEAU);
			this.setTYPE_CARTE(qRead.TYPE_CARTE);
			this.setNB_ENTREE_SA(qRead.NB_ENTREE_SA);
			this.setNB_ENTREE_S(qRead.NB_ENTREE_S);
			this.setNUM_SIM(qRead.NUM_SIM);
			this.setDRAM(qRead.DRAM);
			this.setRAM(qRead.RAM);
			this.setMEMOIRE_FLASH(qRead.MEMOIRE_FLASH);
			this.setPROT_MANAGEMENT(qRead.PROT_MANAGEMENT);
			this.setSTANDARDS(qRead.STANDARDS);
			this.setINTERFACE(qRead.INTERFACE);
			this.setNBPORT(qRead.NBPORT);
			this.setTAUX_TRANSFERT(qRead.TAUX_TRANSFERT);
			this.setNBCARTE_RESEAU(qRead.NBCARTE_RESEAU);
			this.setTECHNOLOGIE_RAM(qRead.TECHNOLOGIE_RAM);
			this.setMEMOIRE_CACHE(qRead.MEMOIRE_CACHE);
			this.setNBSLOT_RAM(qRead.NBSLOT_RAM);
			this.setCHIPSET_CARTE_MERE(qRead.CHIPSET_CARTE_MERE);
			this.setCARTE_GRAPHIQUE(qRead.CARTE_GRAPHIQUE);
			this.setCARTE_SON(qRead.CARTE_SON);
			this.setTYPE_ECRAN(qRead.TYPE_ECRAN);
			this.setTAILLE_ECRAN(qRead.TAILLE_ECRAN);
			this.setAUTONOMIE_VEILLE(qRead.AUTONOMIE_VEILLE);
			this.setAUTONOMIE_FONCTIONNEMENT(qRead.AUTONOMIE_FONCTIONNEMENT);
			this.setTYPE_BATTERIE(qRead.TYPE_BATTERIE);
			this.setVITESSE_IMPRESSION(qRead.VITESSE_IMPRESSION);
			this.setTAILLE_MAX_DOCUMENT(qRead.TAILLE_MAX_DOCUMENT);
			this.setNBR_CARTOUCHE(qRead.NBR_CARTOUCHE);
			this.setINDICATEUR_ETATS(qRead.INDICATEUR_ETATS);
			this.setTYPE_ALIMENTATION(qRead.TYPE_ALIMENTATION);
			this.setCOULEUR(qRead.COULEUR);
			this.setURL_FABRICANT(qRead.URL_FABRICANT);
			this.setPROTOCOLE_CONTROLE_STANDARD(qRead.PROTOCOLE_CONTROLE_STANDARD);
			this.setNBR_CANAUX_IP(qRead.NBR_CANAUX_IP);
			this.setNBR_DISQUE_DUR(qRead.NBR_DISQUE_DUR);
			this.setTYPE_DISQUE_DUR(qRead.TYPE_DISQUE_DUR);
			this.setVITESSE_ECRITURE(qRead.VITESSE_ECRITURE);
			this.setIDCONNECTEUR(qRead.IDCONNECTEUR);
			this.setINTERRUPTEUR(qRead.INTERRUPTEUR);
			this.setNBR_PRISE_SORTIE(qRead.NBR_PRISE_SORTIE);
			this.setDISJONCTEUR(qRead.DISJONCTEUR);
			this.setDISPOSITIF_RESEAUX(qRead.DISPOSITIF_RESEAUX);
			this.setPROTOCOLE_DE_ROUTAGE(qRead.PROTOCOLE_DE_ROUTAGE);
			this.setEMPILABLE(qRead.EMPILABLE);
			this.setTAILLE_TABLE_ADRESSE_MAC(qRead.TAILLE_TABLE_ADRESSE_MAC);
			this.setCAPACITE_COMMUTATION(qRead.CAPACITE_COMMUTATION);
			this.setMODEM_ADSL(qRead.MODEM_ADSL);
			this.setTYPE_MODEM(qRead.TYPE_MODEM);
			this.setNBR_ANTENNES(qRead.NBR_ANTENNES);
			this.setDBI_ANTENNES(qRead.DBI_ANTENNES);
			this.setTYPE_CRYPTAGE(qRead.TYPE_CRYPTAGE);
			this.setBANDE_FREQUENCE(qRead.BANDE_FREQUENCE);
			this.setPORTEE_CHAMPS_LIBRE(qRead.PORTEE_CHAMPS_LIBRE);
			this.setPORTEE_BATIMENT(qRead.PORTEE_BATIMENT);
			this.setSUPPORT(qRead.SUPPORT);
			this.setCOMPOSITION(qRead.COMPOSITION);
			this.setCABLAGE(qRead.CABLAGE);
			this.setCATEGORIE_CABLE(qRead.CATEGORIE_CABLE);
			this.setBLINDAGE(qRead.BLINDAGE);
			this.setINDICATEUR_PROTECTION(qRead.INDICATEUR_PROTECTION);
			this.setCHARGE_ADMISSIBLE(qRead.CHARGE_ADMISSIBLE);
			this.setHAUTEUR_INTERIEURE(qRead.HAUTEUR_INTERIEURE);
			this.setFORMAT(qRead.FORMAT);
			this.setNBR_VENTILATEURS(qRead.NBR_VENTILATEURS);
			this.setTYPE_VENTILATEURS(qRead.TYPE_VENTILATEURS);
			this.setFORMAT_VENTILATEUR_TOITURE(qRead.FORMAT_VENTILATEUR_TOITURE);
			this.setDEBIT_CPL(qRead.DEBIT_CPL);
			this.setFICHE(qRead.FICHE);
			this.setPORTEE_TYPIQUE(qRead.PORTEE_TYPIQUE);
			this.setEMISSIONS_ELECTROMAGNETIQUES(qRead.EMISSIONS_ELECTROMAGNETIQUES);
			this.setNBR_MONITEUR(qRead.NBR_MONITEUR);
			this.setTECHNOLOGIE_WIRELESS(qRead.TECHNOLOGIE_WIRELESS);
			this.setTECHNOLOGIE_IMPRESSION(qRead.TECHNOLOGIE_IMPRESSION);
			this.setNBR_BAC_ALIMENTATION(qRead.NBR_BAC_ALIMENTATION);
			this.setSONORITE(qRead.SONORITE);
			this.setIMPRESSION_RECTO_VERSO(qRead.IMPRESSION_RECTO_VERSO);
			this.setTECHNOLOGIE_SCANNER(qRead.TECHNOLOGIE_SCANNER);
			this.setRESOLUTION_OPTIQUE(qRead.RESOLUTION_OPTIQUE);
			this.setRESOLUTION_INTERPOLEE(qRead.RESOLUTION_INTERPOLEE);
			this.setREDUCTION_MAXIMAL_DOC(qRead.REDUCTION_MAXIMAL_DOC);
			this.setNBR_COPIE_MAX(qRead.NBR_COPIE_MAX);
			this.setPAS_PIXEL(qRead.PAS_PIXEL);
			this.setLUMINOSITE(qRead.LUMINOSITE);
			this.setRAPPORT_CONTRASTE(qRead.RAPPORT_CONTRASTE);
			this.setCONNEXION_IMPRIMANTE(qRead.CONNEXION_IMPRIMANTE);
			this.setTECHNOLOGIE_CONNECTIVITE(qRead.TECHNOLOGIE_CONNECTIVITE);
			this.setPROT_RESEAU(qRead.PROT_RESEAU);
			this.setPROT_LIAISON_DONNEE(qRead.PROT_LIAISON_DONNEE);
			this.setPLATERFORME_SUPPORTE(qRead.PLATERFORME_SUPPORTE);
			this.setNORME_WIFI(qRead.NORME_WIFI);
			this.setMETHODE_IMPRESSION(qRead.METHODE_IMPRESSION);
			this.setRESOLUTION_IMPRESSION(qRead.RESOLUTION_IMPRESSION);
			this.setMEMOIRE_TAMPON_PAGE(qRead.MEMOIRE_TAMPON_PAGE);
			this.setVITESSE_MODEM(qRead.VITESSE_MODEM);
			this.setSERVICE_VOIP(qRead.SERVICE_VOIP);
			this.setPROTOCOLE_IP(qRead.PROTOCOLE_IP);
			this.setVIBREUR(qRead.VIBREUR);
			this.setNBR_COULEURS(qRead.NBR_COULEURS);
			this.setDAS(qRead.DAS);
			this.setVERSION_WAP(qRead.VERSION_WAP);
			this.setCARTE_SIM_INTEGRE(qRead.CARTE_SIM_INTEGRE);
			this.setDRIVER(qRead.DRIVER);
			this.setSLOTABLE(qRead.SLOTABLE);
			this.setINTEGRE_A(qRead.INTEGRE_A);
			this.setPIN1(qRead.PIN1);
			this.setPIN2(qRead.PIN2);
			this.setPUK1(qRead.PUK1);
			this.setPUK2(qRead.PUK2);
			this.setHLR(qRead.HLR);
			this.setLANGUE(qRead.LANGUE);
			this.setCOMPRESSION_VOIE(qRead.COMPRESSION_VOIE);
			this.setNOMBRE_LIGNE_TEL(qRead.NOMBRE_LIGNE_TEL);
			this.setTYPES_LIGNES(qRead.TYPES_LIGNES);
			this.setTYPES_PASSERELLES(qRead.TYPES_PASSERELLES);
			this.setSIM_COMPATIBLE(qRead.SIM_COMPATIBLE);
			this.setTYPE_GSM(qRead.TYPE_GSM);
			this.setVITESSE_ROTATION(qRead.VITESSE_ROTATION);
			this.setBAIE_DISQUE(qRead.BAIE_DISQUE);
			this.setEXTERNE_INTERNE(qRead.EXTERNE_INTERNE);
			this.setGRAVEUR(qRead.GRAVEUR);
			this.setVITESSE_ECRITURE_CD(qRead.VITESSE_ECRITURE_CD);
			this.setVITESSE_LECTURE_CD(qRead.VITESSE_LECTURE_CD);
			this.setTEMPS_ACCES_MOYEN(qRead.TEMPS_ACCES_MOYEN);
			this.setNBR_SUPPORT_AMOVIBLE(qRead.NBR_SUPPORT_AMOVIBLE);
			this.setLONGUEUR_CABLE(qRead.LONGUEUR_CABLE);
			this.setDUREE_EXECUTION(qRead.DUREE_EXECUTION);
			this.setCAPACITE_BATTERIE(qRead.CAPACITE_BATTERIE);
			this.setNORME_SURTENSION(qRead.NORME_SURTENSION);
			this.setINDICE_CONSOMMATION_ENERGIE(qRead.INDICE_CONSOMMATION_ENERGIE);
			this.setCOURANT_ELECTRIQUE_MAXIMAL(qRead.COURANT_ELECTRIQUE_MAXIMAL);
			this.setNBR_BANDES_RESEAU(qRead.NBR_BANDES_RESEAU);
			this.setEDGE(qRead.EDGE);
			this.setEQ_3G(qRead.EQ_3G);
			this.setMODEM_INTEGRE(qRead.MODEM_INTEGRE);
			this.setBLUTOOTH(qRead.BLUTOOTH);
			this.setINFRAROUGE(qRead.INFRAROUGE);
			this.setPLATEFORMES(qRead.PLATEFORMES);
			this.setCLIENT_EMAIL(qRead.CLIENT_EMAIL);
			this.setMAIN_LIBRE(qRead.MAIN_LIBRE);
			this.setCALLERID_STANDARD(qRead.CALLERID_STANDARD);
			this.setMURAL(qRead.MURAL);
			this.setREPONDEUR(qRead.REPONDEUR);
			this.setMANAGEABLE(qRead.MANAGEABLE);
			this.setINTERFACE_CONTROLLER(qRead.INTERFACE_CONTROLLER);
			this.setTYPE_STOCKAGE(qRead.TYPE_STOCKAGE);
			this.setNB_UTILISATEUR_LOCAL(qRead.NB_UTILISATEUR_LOCAL);
			this.setTEMPERATURE(qRead.TEMPERATURE);
			this.setALIMENTATION(qRead.ALIMENTATION);
			this.setAGRANDISSEMENT_MAXIMAL_DOC(qRead.AGRANDISSEMENT_MAXIMAL_DOC);
			this.setDEVELOPPEUR(qRead.DEVELOPPEUR);
			this.setEDITEUR(qRead.EDITEUR);
			this.setRACKABLE(qRead.RACKABLE);
			this.setVERSION_LOGICIEL(qRead.VERSION_LOGICIEL);
			this.setNBR_UTILISATEURS_SIMULT(qRead.NBR_UTILISATEURS_SIMULT);
			this.setCLASSE_GPRS(qRead.CLASSE_GPRS);
		</cfscript>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/18/2007
		
		Description : Insertion d'un equipement dans le catalohue constructeur
	
	--->	
	<cffunction name="create" output="false" access="private" returntype="numeric">
		<!---
		PROCEDURE			
			pkg_cv_equipement.Ins_EquipementCatalogfournis		
		PARAM	
			p_idequipement_fournis		IN INTEGER
			p_idrevendeur				IN INTEGER
			p_libelle					IN VARCHAR2
			p_code_interne				IN VARCHAR2
			p_commentaire				IN VARCHAR2
			p_reference					IN VARCHAR2
			p_te_code_ihm				OUT VARCHAR2
   			p_retour					OUT INTEGER
		--->
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.ins_equipementcatalogfournis">
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getIDEQUIPEMENT_FOURNIS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getLIBELLE_EQ()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getREFERENCE_EQ()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCODE_INTERNE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCOMMENTAIRE_EQ()#'>
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>		



	

	<cffunction name="update" output="false" access="private" returntype="numeric">		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Infosup_EqFournis_maj">
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getIDEQUIPEMENT_FOURNIS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getLIBELLE_EQ()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getREFERENCE_EQ()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCODE_INTERNE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCOMMENTAIRE_EQ()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getHAUTEU_U()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_NUMERIC' value='#this.getPOIDS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getBRUIT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getMTBF()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getPUISSANCE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getMONTAGE_RACK()#'>
			<cfprocparam type='In' cfsqltype="CF_SQL_NUMERIC" value='#this.getPRIX_CATALOGUE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getQTE_MIN()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getMEMOIRE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCAPACITE_CF()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getEMPLACEMENT_INTERNE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getSUPPORT_POE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getNBR_ALIM()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getSUPPORT_QOS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getSUPPORT_VPN()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getSUPPORT_NAT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_SLOTS_MAX()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_SLOTS_CMM_MAX()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_SLOTS_SFM_MAX()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_ALIM_MAX()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getMAX_HEAT_DISSIPATION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getIMEI()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getDIMENSIONS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getAFFICHAGE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getRESOLUTION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getMEMOIRE_INTERNE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getRESEAU()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPE_CARTE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getNB_ENTREE_SA()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getNB_ENTREE_S()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getNUM_SIM()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getDRAM()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getRAM()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getMEMOIRE_FLASH()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getPROT_MANAGEMENT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getSTANDARDS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getINTERFACE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBPORT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getTAUX_TRANSFERT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBCARTE_RESEAU()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTECHNOLOGIE_RAM()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getMEMOIRE_CACHE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBSLOT_RAM()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCHIPSET_CARTE_MERE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCARTE_GRAPHIQUE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCARTE_SON()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPE_ECRAN()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTAILLE_ECRAN()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getAUTONOMIE_VEILLE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getAUTONOMIE_FONCTIONNEMENT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPE_BATTERIE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getVITESSE_IMPRESSION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTAILLE_MAX_DOCUMENT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_CARTOUCHE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getINDICATEUR_ETATS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPE_ALIMENTATION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCOULEUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getURL_FABRICANT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getPROTOCOLE_CONTROLE_STANDARD()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_CANAUX_IP()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_DISQUE_DUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPE_DISQUE_DUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getVITESSE_ECRITURE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getIDCONNECTEUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getINTERRUPTEUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_PRISE_SORTIE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getDISJONCTEUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getDISPOSITIF_RESEAUX()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getPROTOCOLE_DE_ROUTAGE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getEMPILABLE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTAILLE_TABLE_ADRESSE_MAC()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCAPACITE_COMMUTATION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getMODEM_ADSL()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPE_MODEM()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_ANTENNES()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getDBI_ANTENNES()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPE_CRYPTAGE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getBANDE_FREQUENCE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getPORTEE_CHAMPS_LIBRE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getPORTEE_BATIMENT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getSUPPORT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCOMPOSITION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCABLAGE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCATEGORIE_CABLE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getBLINDAGE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getINDICATEUR_PROTECTION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getCHARGE_ADMISSIBLE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getHAUTEUR_INTERIEURE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getFORMAT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_VENTILATEURS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPE_VENTILATEURS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getFORMAT_VENTILATEUR_TOITURE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getDEBIT_CPL()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getFICHE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getPORTEE_TYPIQUE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getEMISSIONS_ELECTROMAGNETIQUES()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_MONITEUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTECHNOLOGIE_WIRELESS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTECHNOLOGIE_IMPRESSION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_BAC_ALIMENTATION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getSONORITE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getIMPRESSION_RECTO_VERSO()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTECHNOLOGIE_SCANNER()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getRESOLUTION_OPTIQUE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getRESOLUTION_INTERPOLEE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_NUMERIC' value='#this.getREDUCTION_MAXIMAL_DOC()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_COPIE_MAX()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getPAS_PIXEL()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getLUMINOSITE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getRAPPORT_CONTRASTE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCONNEXION_IMPRIMANTE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTECHNOLOGIE_CONNECTIVITE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getPROT_RESEAU()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getPROT_LIAISON_DONNEE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getPLATERFORME_SUPPORTE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getNORME_WIFI()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getMETHODE_IMPRESSION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getRESOLUTION_IMPRESSION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getMEMOIRE_TAMPON_PAGE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getVITESSE_MODEM()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getSERVICE_VOIP()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getPROTOCOLE_IP()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getVIBREUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_COULEURS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getDAS()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getVERSION_WAP()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCARTE_SIM_INTEGRE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getDRIVER()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getSLOTABLE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getINTEGRE_A()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getPIN1()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getPIN2()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getPUK1()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getPUK2()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getHLR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getLANGUE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCOMPRESSION_VOIE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNOMBRE_LIGNE_TEL()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPES_LIGNES()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPES_PASSERELLES()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getSIM_COMPATIBLE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPE_GSM()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getVITESSE_ROTATION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getBAIE_DISQUE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getEXTERNE_INTERNE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getGRAVEUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getVITESSE_ECRITURE_CD()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getVITESSE_LECTURE_CD()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getTEMPS_ACCES_MOYEN()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_SUPPORT_AMOVIBLE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getLONGUEUR_CABLE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getDUREE_EXECUTION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getCAPACITE_BATTERIE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getNORME_SURTENSION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getINDICE_CONSOMMATION_ENERGIE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getCOURANT_ELECTRIQUE_MAXIMAL()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNBR_BANDES_RESEAU()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getEDGE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getEQ_3G()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getMODEM_INTEGRE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getBLUTOOTH()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getINFRAROUGE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getPLATEFORMES()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getCLIENT_EMAIL()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getMAIN_LIBRE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCALLERID_STANDARD()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getMURAL()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getREPONDEUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getMANAGEABLE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getINTERFACE_CONTROLLER()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getTYPE_STOCKAGE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getNB_UTILISATEUR_LOCAL()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getTEMPERATURE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getALIMENTATION()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_NUMERIC' value='#this.getAGRANDISSEMENT_MAXIMAL_DOC()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getDEVELOPPEUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getEDITEUR()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_INTEGER' value='#this.getRACKABLE()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getVERSION_LOGICIEL()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getNBR_UTILISATEURS_SIMULT()#'>
			<cfprocparam type='In' cfsqltype='CF_SQL_VARCHAR' value='#this.getCLASSE_GPRS()#'>
			
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="qUpdate">			
		</cfstoredproc>			
		
		<cfreturn qUpdate>
	</cffunction>



	<cffunction name="delete" output="false" access="public" returntype="void">
		<cfset var qDelete="">

		<cfquery name="qDelete" datasource="rocoffre" result="status">
			delete
			from OFFRE.EQUIPEMENT_FOURNIS
			where IDEQUIPEMENT_FOURNIS = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#this.getIDEQUIPEMENT_FOURNIS()#" />
		</cfquery>
	</cffunction>


</cfcomponent>