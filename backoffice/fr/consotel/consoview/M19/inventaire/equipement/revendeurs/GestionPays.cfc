<!--- =========================================================================
Classe: GestionPays
Auteur: brice.miramont
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GestionPays" hint="Pour les diverses procédures" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GestionPays" hint="Remplace le constructeur de GestionPays.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<cffunction name="get_liste_pays" access="public" returntype="query" output="false" hint="Liste les pays consotel" >
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_liste_pays">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>		
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>