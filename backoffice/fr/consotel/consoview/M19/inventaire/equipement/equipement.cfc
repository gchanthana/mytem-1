<cfcomponent name="equipement">

	<cffunction name="getType" returntype="query" access="remote" output="true">		
<!---
		PROCEDURE
		
		pkg_cv_equipement.typeeq_bygroupe
			p_idgroupe_client	IN INTEGER
    		p_retour 			OUT SYS_REFCURSOR
--->
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.typeeq_bygroupe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="" null="true">
			<cfprocresult name="results">
		</cfstoredproc>
		<cfreturn results>
	</cffunction>
	
	
	<cffunction name="getGamme" returntype="query" access="remote" output="true">
		<cfargument name="p_idgroupe_client" type="numeric">
<!---
		PROCEDURE
		
		pkg_cv_equipement.ListeGammegroupe
			p_idgroupe_client	IN INTEGER
   			p_retour 			OUT SYS_REFCURSOR
--->
		<cfif p_idgroupe_client neq 0>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.listegammegroupe">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#" null="false">
				<cfprocresult name="results">
			</cfstoredproc>
		<cfelse>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.listegammegroupe">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="" null="true">
				<cfprocresult name="results">
			</cfstoredproc>
		</cfif>
		<cfreturn results>
	</cffunction>










	<cffunction name="getGammeFab" returntype="query" access="remote" output="true">
		<cfargument name="p_idgroupe_client" type="numeric">
<!---
		PROCEDURE 
		
		pkg_cv_equipement.ListeGammetypef
			p_type_fournisseur	IN INTEGER
			p_idgroupe_client	IN INTEGER
			p_retour			OUT SYS_REFCURSOR

--->
		<cfif p_idgroupe_client neq 0>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.listegammetypef">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="0" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#" null="false">
				<cfprocresult name="results">
			</cfstoredproc>
		<cfelse>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.listegammetypef">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="0" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="" null="true">
				<cfprocresult name="results">
			</cfstoredproc>
		</cfif>
		<cfreturn results>
	</cffunction>
	<!--- Retourne la liste des constructeurs--->
	<cffunction name="getConstructeur" returntype="query" access="remote" output="true">
		<cfargument name="p_idgroupe_client" type="numeric">
		<cfset qResult=getGammeFab(p_idgroupe_client)>
		 <cfquery dbtype="query" name="qGetFab">
			select *
			from qResult			
			where TYPE_FOURNISSEUR =0;
		</cfquery> 
		<cfreturn qGetFab>
	</cffunction>
		<!---Ajouter un modèle et le rendre visible dans le catalogue client--->
	<cffunction name="insertEquipementAllCatalogue" returntype="Numeric" access="remote" output="true">
		<cfargument name="idConstructeur" type="numeric">
		<cfargument name="libelle_mobile" type="String">
		<cfargument name="idRevendeur" type="numeric">
		
		<cfset arr[1]=idConstructeur>	<!---idConstructeur--->
		<cfset arr[2]=70> 				<!---p_idtype_equipement--->
		<cfset arr[3]=0> 				<!---p_idgamme_fab--->
		<cfset arr[4]=libelle_mobile> 	<!---p_libelle_eq--->
		<cfset arr[5]=""> 				<!---p_reference_eq--->
		<cfset arr[6]=""> 				<!---p_code_interne--->
		<cfset arr[7]=""> 				<!---p_commentaire_eq--->
		
		
		<cfset qInsert_in_catalogue_construc=insertEquipementCatalogue(arr)>
		
		
		<cfset qInsert_in_catalogue_revendeur=insertModeleRevendeur(qInsert_in_catalogue_construc,idRevendeur)>
					
		<cfreturn qInsert_in_catalogue_revendeur>		
	</cffunction>
	<cffunction name="insertModeleRevendeur" returntype="Numeric" access="remote">
		<cfargument name="idEquipConstructeur" type="Numeric">
		<cfargument name="idrevendeur" type="Numeric">
			
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.copyEq_fournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipConstructeur#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="" null="true">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="getTypeXML" returntype="xml" access="remote" output="true">
		<cfset qResult=getType()>

		<cfquery dbtype="query" name="qGetType">
			select *
			from qResult
			order by IDCATEGORIE_EQUIPEMENT, type_equipement
		</cfquery>
		
		<cfset dataStruct = structNew()>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset dataStruct.ROOT_NODE = rootNode>
		<cfset rootNode.XmlAttributes.LABEL = "S�lectionner un type d'equipement">
		<cfset rootNode.XmlAttributes.NODE_ID = "0">
		<cfset rootNode.XmlAttributes.LEVEL = "1">
		<cfset rootNode.XmlAttributes.ACTIF = "0">
		<cfset rootNode.XmlAttributes.IDCATEGORIE_EQUIPEMENT = "">
		<cfset rootNode.XmlAttributes.IDTYPE_EQUIPEMENT = "">
		<cfset rootNode.XmlAttributes.TYPE_EQUIPEMENT = "">
		<cfset rootNode.XmlAttributes.CATEGORIE_EQUIPEMENT = "">
		<cfset rootNode.XmlAttributes.TE_CODE_IHM = "">

		<!---**************************************************************************************--->
		<cfset sourceQuery=qGetType>
		<cfset i=1>
		<cfset j=1>
		<cfset k=1>
		<cfset st=structNew()>
		
		<cfif sourceQuery.recordCount gt 0>
		
			<cfoutput group="IDCATEGORIE_EQUIPEMENT" query="sourceQuery">
				<cfset k=k+1>
				<cfset stFab=structNew()>
				<cfset stFab.node=XmlElemNew(perimetreXmlDoc,"node")>
				<cfset stFab.node.XmlAttributes.LABEL = sourceQuery['CATEGORIE_EQUIPEMENT'][i]>
				<cfset stFab.node.XmlAttributes.NODE_ID = k>
				<cfset stFab.node.XmlAttributes.LEVEL = "2">
				<cfset stFab.node.XmlAttributes.ACTIF = "0">
				<cfset stFab.node.XmlAttributes.IDCATEGORIE_EQUIPEMENT = sourceQuery['IDCATEGORIE_EQUIPEMENT'][i]>
				<cfset stFab.node.XmlAttributes.IDTYPE_EQUIPEMENT = "">
				<cfset stFab.node.XmlAttributes.TYPE_EQUIPEMENT = "">
				<cfset stFab.node.XmlAttributes.CATEGORIE_EQUIPEMENT = sourceQuery['CATEGORIE_EQUIPEMENT'][i]>
				<cfset stFab.node.XmlAttributes.TE_CODE_IHM = "">
				
				<cfset structInsert(st,i,stFab)>
				<cfoutput>
					<cfset k=k+1>
					<cfset tmp = XmlElemNew(perimetreXmlDoc,"node")>
					<cfset tmp.XmlAttributes.LABEL = sourceQuery['TYPE_EQUIPEMENT'][i]>
					<cfset tmp.XmlAttributes.NODE_ID = k>
					<cfset tmp.XmlAttributes.LEVEL = "2">
					<cfset tmp.XmlAttributes.ACTIF = "1">
					<cfset tmp.XmlAttributes.IDCATEGORIE_EQUIPEMENT = sourceQuery['IDCATEGORIE_EQUIPEMENT'][i]>
					<cfset tmp.XmlAttributes.IDTYPE_EQUIPEMENT = sourceQuery['IDTYPE_EQUIPEMENT'][i]>
					<cfset tmp.XmlAttributes.TYPE_EQUIPEMENT = sourceQuery['TYPE_EQUIPEMENT'][i]>
					<cfset tmp.XmlAttributes.CATEGORIE_EQUIPEMENT = sourceQuery['CATEGORIE_EQUIPEMENT'][i]>
					<cfset tmp.XmlAttributes.TE_CODE_IHM = sourceQuery['TE_CODE_IHM'][i]>
					<cfset stFab.node.XmlChildren[j]=tmp>
	
					<cfset i=i+1>
					<cfset j=j+1>
				</cfoutput>
					<cfset j=1>
			</cfoutput>
		<cfelse>
		
		</cfif>
		<cfset k=1>
		<cfset li=structKeyList(st)>

		<cfset sortedList=listsort(li,"numeric")>

		<cfloop list="#sortedList#" index="i">
			<cfset rootNode.XmlChildren[k]=st[i].NODE>
			<cfset k=k+1>
		</cfloop>

		<cfreturn perimetreXmlDoc>
	</cffunction>
	
	<cffunction name="getGammeXML" returntype="xml" access="remote" output="true">
		<cfargument name="index" type="numeric">
		<!--- <cfset qResult=getGamme(SESSION.perimetre.LISTE_GROUPES_QUERY['IDGROUPE_CLIENT'][index])> --->
		<cfset qResult=getGamme(index)>
		
		<cfquery dbtype="query" name="qGetFab">
			select *
			from qResult			
			order by type_fournisseur, nom_fournisseur, libelle_gamme
		</cfquery>
		<cfdump var="#qGetFab#">
		<cfquery dbtype="query" name="qGetFour">
			select *
			from qResult
			where type_fournisseur = 1
			order by type_fournisseur, nom_fournisseur, libelle_gamme
		</cfquery>
		
		<cfdump var="#qGetFour#">
		
		
		<cfset a=0>
		
		<cfset dataStruct = structNew()>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset dataStruct.ROOT_NODE = rootNode>
		<cfset rootNode.XmlAttributes.LABEL = "S�lectionner un fournisseur">
		<cfset rootNode.XmlAttributes.NODE_ID = a>
		<cfset rootNode.XmlAttributes.LEVEL = "1">
		<cfset rootNode.XmlAttributes.ACTIF = "0">
		<cfset rootNode.XmlAttributes.IDFOURNISSEUR = "">
		<cfset rootNode.XmlAttributes.IDGAMME_FOURNIS = "0">
		<cfset rootNode.XmlAttributes.IDGROUPE_CLIENT = "">
		<cfset rootNode.XmlAttributes.LIBELLE_GAMME = "">
		<cfset rootNode.XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
		<cfset rootNode.XmlAttributes.NOM_FOURNISSEUR = "">
		<cfset rootNode.XmlAttributes.TYPE_FOURNISSEUR = "-1">
		<cfset a=a+1>
		<!---************************ TOUJOURS 2 GROUPEMENTS ORGANISATIONS ************************--->
		<!--- <cfif qGetFab.recordcount GT 0> --->
			<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Constructeur">
			<cfset rootNode.XmlChildren[1].XmlAttributes.NODE_ID = a>
			<cfset rootNode.XmlChildren[1].XmlAttributes.LEVEL = "2">
			<cfset rootNode.XmlChildren[1].XmlAttributes.ACTIF = "1">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDFOURNISSEUR = "0">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDGAMME_FOURNIS = "0">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDGROUPE_CLIENT = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.LIBELLE_GAMME = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.NOM_FOURNISSEUR = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.TYPE_FOURNISSEUR = "0">
			<cfset dataStruct.OP_ORGANISATIONS = rootNode.XmlChildren[1]>
			<cfset a=a+1>
		<!--- </cfif> --->
		
	<!--- 	<cfif qGetFour.recordcount GT 0> --->
			<cfset rootNode.XmlChildren[2] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode.XmlChildren[2].XmlAttributes.LABEL = "Revendeur">
			<cfset rootNode.XmlChildren[2].XmlAttributes.NODE_ID = a>
			<cfset rootNode.XmlChildren[2].XmlAttributes.LEVEL = "2">
			<cfset rootNode.XmlChildren[2].XmlAttributes.ACTIF = "1">
			<cfset rootNode.XmlChildren[2].XmlAttributes.IDFOURNISSEUR = "0">
			<cfset rootNode.XmlChildren[2].XmlAttributes.IDGAMME_FOURNIS = "0">
			<cfset rootNode.XmlChildren[2].XmlAttributes.IDGROUPE_CLIENT = "">
			<cfset rootNode.XmlChildren[2].XmlAttributes.LIBELLE_GAMME = "">
			<cfset rootNode.XmlChildren[2].XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset rootNode.XmlChildren[2].XmlAttributes.NOM_FOURNISSEUR = "">
			<cfset rootNode.XmlChildren[2].XmlAttributes.TYPE_FOURNISSEUR = "1">
			<cfset dataStruct.CLIENT_ORGANISATIONS = rootNode.XmlChildren[2]>
			<cfset a=a+1>
		<!--- </cfif> --->
		<!---**************************************************************************************--->
		<cfset sourceQuery=qGetFab>

		<!--- <cfdump var="#datastruct#"> --->
		<cfset i=1>
		<cfset j=1>
		
		<cfset st=structNew()>
		<cfset st1=structNew()>
		<cfoutput group="idfournisseur" query="sourceQuery">
			
			<cfset stFour=structNew()>
			<cfset stFour.node=XmlElemNew(perimetreXmlDoc,"node")>
			<cfset stFour.node.XmlAttributes.LABEL = sourceQuery['nom_fournisseur'][i]>
			<cfset stFour.node.XmlAttributes.NODE_ID = a>
			<cfset stFour.node.XmlAttributes.LEVEL = "3">
			<cfset stFour.node.XmlAttributes.ACTIF = "1">
			<cfset stFour.node.XmlAttributes.IDFOURNISSEUR = sourceQuery['idfournisseur'][i]>
			<cfset stFour.node.XmlAttributes.IDGAMME_FOURNIS = "0">
			<cfset stFour.node.XmlAttributes.IDGROUPE_CLIENT = sourceQuery['idgroupe_client'][i]>
			<cfset stFour.node.XmlAttributes.LIBELLE_GAMME = "">
			<cfset stFour.node.XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset stFour.node.XmlAttributes.NOM_FOURNISSEUR = sourceQuery['nom_fournisseur'][i]>
			<cfset stFour.node.XmlAttributes.TYPE_FOURNISSEUR = sourceQuery['type_fournisseur'][i]>
			
			<cfset stFab=structNew()>
			<cfset stFab.node=XmlElemNew(perimetreXmlDoc,"node")>
			<cfset stFab.node.XmlAttributes.LABEL = sourceQuery['nom_fournisseur'][i]>
			<cfset stFab.node.XmlAttributes.NODE_ID = a>
			<cfset stFab.node.XmlAttributes.LEVEL = "3">
			<cfset stFab.node.XmlAttributes.ACTIF = "1">
			<cfset stFab.node.XmlAttributes.IDFOURNISSEUR = sourceQuery['idfournisseur'][i]>
			<cfset stFab.node.XmlAttributes.IDGAMME_FOURNIS = "0">
			<cfset stFab.node.XmlAttributes.IDGROUPE_CLIENT = sourceQuery['idgroupe_client'][i]>
			<cfset stFab.node.XmlAttributes.LIBELLE_GAMME = "">
			<cfset stFab.node.XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset stFab.node.XmlAttributes.NOM_FOURNISSEUR = sourceQuery['nom_fournisseur'][i]>
			<cfset stFab.node.XmlAttributes.TYPE_FOURNISSEUR = sourceQuery['type_fournisseur'][i]>
			
			<cfset a=a+1>
			
			<cfdump var="#sourceQuery['type_fournisseur'][i]#">
			
			<cfif sourceQuery['type_fournisseur'][i] eq 0>
				<cfset structInsert(st,i,stFab)>
			<cfelseif sourceQuery['type_fournisseur'][i] eq 1>
				<cfset structInsert(st1,i,stFour)>
			</cfif>
			
			<cfoutput>
				<cfset tmp = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset tmp.XmlAttributes.LABEL = sourceQuery['libelle_gamme'][i]>
				<cfset tmp.XmlAttributes.NODE_ID = a>
				<cfset tmp.XmlAttributes.LEVEL = "4">
				<cfset tmp.XmlAttributes.ACTIF = "1">
				<cfset tmp.XmlAttributes.IDFOURNISSEUR = sourceQuery['idfournisseur'][i]>
				<cfset tmp.XmlAttributes.IDGAMME_FOURNIS = sourceQuery['idgamme_fournis'][i]>
				<cfset tmp.XmlAttributes.IDGROUPE_CLIENT = sourceQuery['idgroupe_client'][i]>
				<cfset tmp.XmlAttributes.LIBELLE_GAMME = sourceQuery['libelle_gamme'][i]>
				<cfset tmp.XmlAttributes.MODELE_COMMENTAIRE_GAMME = sourceQuery['modele_commentaire_gamme'][i]>
				<cfset tmp.XmlAttributes.NOM_FOURNISSEUR = sourceQuery['nom_fournisseur'][i]>
				<cfset tmp.XmlAttributes.TYPE_FOURNISSEUR = sourceQuery['type_fournisseur'][i]>
				<cfif sourceQuery['type_fournisseur'][i] eq 0 and sourceQuery['libelle_gamme'][i] neq "">
					<cfset stFab.node.XmlChildren[j]=tmp>
					<cfset a=a+1>
				<cfelseif sourceQuery['type_fournisseur'][i] eq 1 and sourceQuery['libelle_gamme'][i] neq "">
					<cfset stFour.node.XmlChildren[j]=tmp>
					<cfset a=a+1>
				</cfif>
				<cfset i=i+1>
				<cfset j=j+1>
				
			</cfoutput>
				<cfset j=1>
		</cfoutput>
		<cfset k=1>
		<cfset li=structKeyList(st)>
		<cfset li1=structKeyList(st1)>
		<cfset sortedList=listsort(li,"numeric")>
		<cfset sortedList1=listsort(li1,"numeric")>
		
		<cfloop list="#sortedList#" index="i">
			<cfset rootNode.XmlChildren[1].XmlChildren[k]=st[i].NODE>
			<cfset k=k+1>
		</cfloop>
		
		<cfif ListLen(sortedList1) gt 0>	
			<cfset k=1>	
			<cfloop list="#sortedList1#" index="i">			
				<cfset rootNode.XmlChildren[2].XmlChildren[k]=st1[i].NODE>
				<cfset k=k+1>
			</cfloop>
		</cfif>
		
		<cfreturn perimetreXmlDoc>
	</cffunction>
	
	<cffunction name="getGammeFabXML" returntype="xml" access="remote" output="true">
		<cfargument name="index" type="numeric">
		<cfset qResult=getGamme(0)>
		
		<cfquery dbtype="query" name="qGetFab">
			select *
			from qResult
			order by type_fournisseur, nom_fournisseur, libelle_gamme
		</cfquery>
		<cfset qGetFour = "">
		<!--- <cfquery dbtype="query" name="qGetFour">
			select *
			from qResult
			where type_fournisseur<>0
			order by type_fournisseur, nom_fournisseur, libelle_gamme
		</cfquery> --->
		
		<cfset a=0>
		
		<cfset dataStruct = structNew()>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset dataStruct.ROOT_NODE = rootNode>
		<cfset rootNode.XmlAttributes.LABEL = "S�lectionner un Constructeur ou une Gamme">
		<cfset rootNode.XmlAttributes.NODE_ID = a>
		<cfset rootNode.XmlAttributes.LEVEL = "1">
		<cfset rootNode.XmlAttributes.ACTIF = "0">
		<cfset rootNode.XmlAttributes.IDFOURNISSEUR = "">
		<cfset rootNode.XmlAttributes.IDGAMME_FOURNIS = "0">
		<cfset rootNode.XmlAttributes.IDGROUPE_CLIENT = "">
		<cfset rootNode.XmlAttributes.LIBELLE_GAMME = "">
		<cfset rootNode.XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
		<cfset rootNode.XmlAttributes.NOM_FOURNISSEUR = "">
		<cfset rootNode.XmlAttributes.TYPE_FOURNISSEUR = "0">
		<cfset a=a+1>
		<!---************************ TOUJOURS 2 GROUPEMENTS ORGANISATIONS ************************--->
		<cfif qGetFab.recordcount GT 1>
			<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Constructeur">
			<cfset rootNode.XmlChildren[1].XmlAttributes.NODE_ID = a>
			<cfset rootNode.XmlChildren[1].XmlAttributes.LEVEL = "2">
			<cfset rootNode.XmlChildren[1].XmlAttributes.ACTIF = "1">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDFOURNISSEUR = "0">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDGAMME_FOURNIS = "0">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDGROUPE_CLIENT = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.LIBELLE_GAMME = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.NOM_FOURNISSEUR = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.TYPE_FOURNISSEUR = "0">
			<cfset dataStruct.OP_ORGANISATIONS = rootNode.XmlChildren[1]>
			<cfset a=a+1>
		</cfif>
		
		<!---**************************************************************************************--->
		<cfset sourceQuery=qGetFab>

		<!--- <cfdump var="#datastruct#"> --->
		<cfset i=1>
		<cfset j=1>
		
		<cfset st=structNew()>
		<cfoutput group="idfournisseur" query="sourceQuery">
			<cfset stFab=structNew()>
			<cfset stFab.node=XmlElemNew(perimetreXmlDoc,"node")>
			<cfset stFab.node.XmlAttributes.LABEL = sourceQuery['nom_fournisseur'][i]>
			<cfset stFab.node.XmlAttributes.NODE_ID = a>
			<cfset stFab.node.XmlAttributes.LEVEL = "2">
			<cfset stFab.node.XmlAttributes.ACTIF = "1">
			<cfset stFab.node.XmlAttributes.IDFOURNISSEUR = sourceQuery['idfournisseur'][i]>
			<cfset stFab.node.XmlAttributes.IDGAMME_FOURNIS = "0">
			<cfset stFab.node.XmlAttributes.IDGROUPE_CLIENT = sourceQuery['idgroupe_client'][i]>
			<cfset stFab.node.XmlAttributes.LIBELLE_GAMME = "">
			<cfset stFab.node.XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset stFab.node.XmlAttributes.NOM_FOURNISSEUR = sourceQuery['nom_fournisseur'][i]>
			<cfset stFab.node.XmlAttributes.TYPE_FOURNISSEUR = sourceQuery['type_fournisseur'][i]>
			<cfset a=a+1>
			
			<cfif sourceQuery['type_fournisseur'][i] eq 0>
				<cfset structInsert(st,i,stFab)>
			</cfif>
			<cfoutput>
				<cfset tmp = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset tmp.XmlAttributes.LABEL = sourceQuery['libelle_gamme'][i]>
				<cfset tmp.XmlAttributes.NODE_ID = a>
				<cfset tmp.XmlAttributes.LEVEL = "2">
				<cfset tmp.XmlAttributes.ACTIF = "1">
				<cfset tmp.XmlAttributes.IDFOURNISSEUR = sourceQuery['idfournisseur'][i]>
				<cfset tmp.XmlAttributes.IDGAMME_FOURNIS = sourceQuery['idgamme_fournis'][i]>
				<cfset tmp.XmlAttributes.IDGROUPE_CLIENT = sourceQuery['idgroupe_client'][i]>
				<cfset tmp.XmlAttributes.LIBELLE_GAMME = sourceQuery['libelle_gamme'][i]>
				<cfset tmp.XmlAttributes.MODELE_COMMENTAIRE_GAMME = sourceQuery['modele_commentaire_gamme'][i]>
				<cfset tmp.XmlAttributes.NOM_FOURNISSEUR = sourceQuery['nom_fournisseur'][i]>
				<cfset tmp.XmlAttributes.TYPE_FOURNISSEUR = sourceQuery['type_fournisseur'][i]>
				<cfif sourceQuery['type_fournisseur'][i] eq 0 and sourceQuery['libelle_gamme'][i] neq "">
					<cfset stFab.node.XmlChildren[j]=tmp>
					<cfset a=a+1>
				</cfif>
				<cfset i=i+1>
				<cfset j=j+1>
			</cfoutput>
				<cfset j=1>
		</cfoutput>
		<cfset k=1>
		<cfset li=structKeyList(st)>
		<cfset sortedList=listsort(li,"numeric")>
		<cfloop list="#sortedList#" index="i">
			<cfset rootNode.XmlChildren[k]=st[i].NODE>
			<cfset k=k+1>
		</cfloop>
		<cfreturn perimetreXmlDoc>
	</cffunction>
	
	<cffunction name="getGammeFourXML" returntype="xml" access="remote" output="true">
		<cfargument name="index" type="numeric">
		<cfset qResult=getGamme(SESSION.perimetre.LISTE_GROUPES_QUERY['IDGROUPE_CLIENT'][index])>
		
		<cfquery dbtype="query" name="qGetFour">
			select *
			from qResult
			where type_fournisseur<>0
			order by type_fournisseur, nom_fournisseur, libelle_gamme
		</cfquery>
		
		<cfset a=0>
		
		<cfset dataStruct = structNew()>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset dataStruct.ROOT_NODE = rootNode>
		<cfset rootNode.XmlAttributes.LABEL = "S�lectionner un Revendeur">
		<cfset rootNode.XmlAttributes.NODE_ID = a>
		<cfset rootNode.XmlAttributes.LEVEL = "1">
		<cfset rootNode.XmlAttributes.ACTIF = "0">
		<cfset rootNode.XmlAttributes.IDFOURNISSEUR = "">
		<cfset rootNode.XmlAttributes.IDGAMME_FOURNIS = "0">
		<cfset rootNode.XmlAttributes.IDGROUPE_CLIENT = "">
		<cfset rootNode.XmlAttributes.LIBELLE_GAMME = "">
		<cfset rootNode.XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
		<cfset rootNode.XmlAttributes.NOM_FOURNISSEUR = "">
		<cfset rootNode.XmlAttributes.TYPE_FOURNISSEUR = "0">
		<cfset a=a+1>
		<!---************************ TOUJOURS 2 GROUPEMENTS ORGANISATIONS ************************--->
		<cfif qGetFour.recordcount GT 1>
			<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Revendeur">
			<cfset rootNode.XmlChildren[1].XmlAttributes.NODE_ID = a>
			<cfset rootNode.XmlChildren[1].XmlAttributes.LEVEL = "2">
			<cfset rootNode.XmlChildren[1].XmlAttributes.ACTIF = "1">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDFOURNISSEUR = "0">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDGAMME_FOURNIS = "0">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDGROUPE_CLIENT = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.LIBELLE_GAMME = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.NOM_FOURNISSEUR = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.TYPE_FOURNISSEUR = "0">
			<cfset dataStruct.OP_ORGANISATIONS = rootNode.XmlChildren[1]>
			<cfset a=a+1>
		</cfif>
		
		<!---**************************************************************************************--->
		<cfset sourceQuery=qGetFour>

		<!--- <cfdump var="#datastruct#"> --->
		<cfset i=1>
		<cfset j=1>
		
		<cfset st=structNew()>
		<cfoutput group="idfournisseur" query="sourceQuery">
			<cfset stFab=structNew()>
			<cfset stFab.node=XmlElemNew(perimetreXmlDoc,"node")>
			<cfset stFab.node.XmlAttributes.LABEL = sourceQuery['nom_fournisseur'][i]>
			<cfset stFab.node.XmlAttributes.NODE_ID = a>
			<cfset stFab.node.XmlAttributes.LEVEL = "3">
			<cfset stFab.node.XmlAttributes.ACTIF = "1">
			<cfset stFab.node.XmlAttributes.IDFOURNISSEUR = sourceQuery['idfournisseur'][i]>
			<cfset stFab.node.XmlAttributes.IDGAMME_FOURNIS = "0">
			<cfset stFab.node.XmlAttributes.IDGROUPE_CLIENT = sourceQuery['idgroupe_client'][i]>
			<cfset stFab.node.XmlAttributes.LIBELLE_GAMME = "">
			<cfset stFab.node.XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset stFab.node.XmlAttributes.NOM_FOURNISSEUR = sourceQuery['nom_fournisseur'][i]>
			<cfset stFab.node.XmlAttributes.TYPE_FOURNISSEUR = sourceQuery['type_fournisseur'][i]>
			<cfset a=a+1>
			
			<cfif sourceQuery['type_fournisseur'][i] neq 0>
				<cfset structInsert(st,i,stFab)>
			</cfif>
			<cfoutput>
				<cfset tmp = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset tmp.XmlAttributes.LABEL = sourceQuery['libelle_gamme'][i]>
				<cfset tmp.XmlAttributes.NODE_ID = a>
				<cfset tmp.XmlAttributes.LEVEL = "4">
				<cfset tmp.XmlAttributes.ACTIF = "1">
				<cfset tmp.XmlAttributes.IDFOURNISSEUR = sourceQuery['idfournisseur'][i]>
				<cfset tmp.XmlAttributes.IDGAMME_FOURNIS = sourceQuery['idgamme_fournis'][i]>
				<cfset tmp.XmlAttributes.IDGROUPE_CLIENT = sourceQuery['idgroupe_client'][i]>
				<cfset tmp.XmlAttributes.LIBELLE_GAMME = sourceQuery['libelle_gamme'][i]>
				<cfset tmp.XmlAttributes.MODELE_COMMENTAIRE_GAMME = sourceQuery['modele_commentaire_gamme'][i]>
				<cfset tmp.XmlAttributes.NOM_FOURNISSEUR = sourceQuery['nom_fournisseur'][i]>
				<cfset tmp.XmlAttributes.TYPE_FOURNISSEUR = sourceQuery['type_fournisseur'][i]>
				<cfif sourceQuery['type_fournisseur'][i] neq 0 and sourceQuery['libelle_gamme'][i] neq "">
					<cfset stFab.node.XmlChildren[j]=tmp>
					<cfset a=a+1>
				</cfif>
				<cfset i=i+1>
				<cfset j=j+1>
			</cfoutput>
				<cfset j=1>
		</cfoutput>
		<cfset k=1>
		<cfset li=structKeyList(st)>
		<cfset sortedList=listsort(li,"numeric")>
		<cfloop list="#sortedList#" index="i">
			<cfset rootNode.XmlChildren[k]=st[i].NODE>
			<cfset k=k+1>
		</cfloop>
		<cfreturn perimetreXmlDoc>
	</cffunction>
	
	
	
	<cffunction name="getCommande" returntype="xml" access="remote">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_commandes">
			<cfprocresult  name="p_result">
		</cfstoredproc>
		<cfset res = StructNew()>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset res.ROOT_NODE = rootNode>
		<cfset rootNode.XmlAttributes.LABEL = "Choisir une p�riode et un Num. de Commande">
		<cfset rootNode.XmlAttributes.IDCOMMANDE = "0">
		<cfset rootNode.XmlAttributes.ANNEE = "0">
		<cfset rootNode.XmlAttributes.MOIS = "0">
		
		<cfset currentParentKey = "ROOT_NODE">		
		<cfloop index="i" from="1" to="#p_result.recordcount#">
			<cfset annee=p_result["ANNEE"][i]>
			<cfset mois=p_result["MOIS"][i]>
			<cfif NOT StructKeyExists(res, annee)>
				<cfset newyear = StructNew() />
				<cfset parentChildCount = arrayLen(res['ROOT_NODE'].XmlChildren)>
				
				<cfset res[currentParentKey].XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node") />
				<cfset tmpChildNode = res['#currentParentKey#'].XmlChildren[parentChildCount + 1]>
				<cfset tmpChildNode.XmlAttributes.LABEL = annee>
				<cfset tmpChildNode.XmlAttributes.ANNEE = annee>
				<cfset tmpChildNode.XmlAttributes.MOIS = 0>
				<cfset tmpChildNode.XmlAttributes.IDCOMMANDE = 0>
				
				<cfset newyear.node = XmlElemNew(perimetreXmlDoc,"node") />
				<cfset newyear.XmlAttributes.LABEL = annee />
				<cfset newyear.XmlAttributes.nb = parentChildCount + 1>
				<cfset structInsert(res,annee,newyear)>
			</cfif>
			
			<cfset parentKey=res[annee].XmlAttributes.nb>
			<cfset parentNode=res['ROOT_NODE'].XmlChildren[parentKey]>
			<cfif NOT StructKeyExists(res[annee], mois)>
				<cfset newmois = StructNew() />
				<cfset parentChildCount = arrayLen(parentNode.XmlChildren)>
				
				<cfset parentNode.XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node") />
				<cfset tmpChildNode = parentNode.XmlChildren[parentChildCount + 1]>
				<cfset dc = parseDateTime(p_result["DATE_COMMANDE"][i])>
				<cfset tmpChildNode.XmlAttributes.LABEL = lsDateFormat(dc , "MMMM")>
				<cfset tmpChildNode.XmlAttributes.ANNEE = annee>
				<cfset tmpChildNode.XmlAttributes.MOIS = mois>
				<cfset tmpChildNode.XmlAttributes.IDCOMMANDE = 0>
				
				<cfset newmois.node = XmlElemNew(parentNode,"node") />
				<cfset newmois.XmlAttributes.LABEL = annee />
				<cfset newmois.XmlAttributes.nb = parentChildCount + 1>
				<cfset structInsert(res[annee],mois,newmois)>
			</cfif>
			<cfset parentKey=res[annee][mois].XmlAttributes.nb>
			<cfset parentNode=parentNode.XmlChildren[parentKey]>
				<cfset newitem = StructNew() />
				<cfset parentChildCount = arrayLen(parentNode.XmlChildren)>
				
				<cfset parentNode.XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node") />
				<cfset tmpChildNode = parentNode.XmlChildren[parentChildCount + 1]>
				
				<cfset tmpChildNode.XmlAttributes.LABEL = p_result["NUMERO_COMMANDE"][i]>
				<cfset tmpChildNode.XmlAttributes.IDCOMMANDE = p_result["IDCOMMANDE"][i]>
				<cfset tmpChildNode.XmlAttributes.ANNEE = annee>
				<cfset tmpChildNode.XmlAttributes.MOIS = mois>
				
		</cfloop>
		<cfreturn perimetreXmlDoc>
		
	</cffunction>
	
	<cffunction name="insertEquipementCatalogue" returntype="string" access="remote">
		<cfargument name="arr" type="array">
		
		<!---
			PROCEDURE
				pkg_cv_equipement.add_equipementsfournis
			PARAM
				p_idfabriquant					IN INTEGER
				p_idtype_equipement				IN INTEGER
				p_idgamme_fab					IN INTEGER
				p_libelle_eq					IN VARCHAR2
				p_reference_eq					IN VARCHAR2
				p_code_interne					IN VARCHAR2
				p_commentaire_eq				IN VARCHAR2
				
				p_retour 						OUT INTEGER
		--->
		
		
		
		<cfset p_idfabriquant = arr[1]>
		<cfset p_idtype_equipement = arr[2]>
		<cfset p_idgamme_fab = arr[3]>
		<cfset p_libelle_eq = arr[4]>
		<cfset p_reference_eq = arr[5]>
		<cfset p_code_interne = arr[6]>
		<cfset p_commentaire_eq = arr[7]>
			
		<cfif p_idgamme_fab eq 0>
			<cfset flag="true">
		<cfelse>
			<cfset flag="false">
		</cfif>
				
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.add_equipementsfournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfabriquant#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idtype_equipement#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgamme_fab#" null="#flag#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_libelle_eq#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_reference_eq#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_code_interne#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_commentaire_eq#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="idequip">
		</cfstoredproc>
		<cfreturn idequip>
	</cffunction>
	
	<cffunction name="insertEquipementCatalogueClient" returntype="array" access="remote">
		<cfargument name="arr" type="array">
		
		<cfset realid = session.perimetre.ID_GROUPE/>						
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.ins_equipementCatalogueClient2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[2]#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[5]#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#realid#" null="false">
			<cfprocparam type="out" cfsqltype="cF_SQL_VARCHAR" variable="code_ihm">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="idequip">
		</cfstoredproc>
				
		<cfset ret = arrayNew(1)>
		
		<cfset ret[1] = idequip> 
		<cfset ret[2] = code_ihm> 
		
		<cfreturn ret>
	</cffunction>
	
	<cffunction name="insertEquipementCatalogueFournis" returntype="array" access="remote">
		<cfargument name="arr" type="array">
		<!---
			
			Auteur : samuel.divioka
			
			Date : 10/19/2007
			
			Description
			
				PROCEDURE 
					pkg_cv_equipement.Ins_EquipementCatalogfournis
				
				PARAMS	IN								
					p_idequipement_fournis		IN INTEGER
					p_idrevendeur				IN INTEGER
					p_libelle					IN VARCHAR2
					p_code_interne				IN VARCHAR2
					p_commentaire				IN VARCHAR2
					p_reference					IN VARCHAR2
					
				PARAMS OUT					
					p_te_code_ihm				OUT VARCHAR2					
   					p_retour					OUT INTEGER
			
		
		--->	
		<cfset p_idequipement_fournis = arr[1]>
		<cfset p_idrevendeur = arr[6]>
		<cfset p_libelle = arr[2]>
		<cfset p_code_interne = arr[4]>
		<cfset p_commentaire = arr[5]>
		<cfset p_reference = arr[3]>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.ins_equipementcatalogfournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idequipement_fournis#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idrevendeur#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_libelle#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_code_interne#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_commentaire#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_reference#" null="false">
			<cfprocparam type="out" cfsqltype="cF_SQL_VARCHAR" variable="code_ihm">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="idequip">
		</cfstoredproc>		
		<cfset ret = arrayNew(1)>
		
		<cfset ret[1] = idequip> 
		<cfset ret[2] = code_ihm> 
		
		<cfreturn ret>
	</cffunction>
	
	<cffunction  name="getCodeIHM" returntype="query" access="private">
		<cfargument name="idequipement" type="numeric" required="true">
		
		<cfquery name="result" datasource="#session.offreDSN#">
			select ef.idequipement_fournis, 			     			       
			       te.te_code_ihm
			from   equipement_fournis ef,
			       type_equipement te
			where  ef.idtype_equipement = te.idtype_equipement 
			  and  ef.idequipement_fournis = #idequipement#
		</cfquery>
		
		<cfreturn result> 
	</cffunction>	
	
	<cffunction name="insertEquipementRevendeur" returntype="string" access="remote">
		<cfargument name="arr" type="array">
		<cfif arr[3] eq 0>
			<cfset flag="true">
		<cfelse>
			<cfset flag="false">
		</cfif>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.copyEquipementFournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[3]#">
		</cfstoredproc>
		
		<cfreturn idequip>
	</cffunction>
	
	<cffunction name="insertEquipement" returntype="array" access="remote">
		
		<cfargument name="arr" type="array">		
		<cfset p_idequipement_client = arr[1]>
		<cfset p_idemplacement = arr[2]>
		<cfset p_libelle_eq = arr[3]>
		<cfset p_commentaire_eq = arr[5]>
		<cfset p_code_interne = arr[6]>
		<cfset p_reference_eq = arr[4]>
		<cfset p_quantite = arr[7]>
		<cfset p_prefix = arr[8]>
		
		
<!---	
		PROCEDURE
		
		pkg_cv_equipement.ins_Equipement
			p_idequipement_client		IN INTEGER,
			p_idemplacement				IN INTEGER,
			p_libelle_eq				IN VARCHAR2,
			p_commentaire_eq			IN VARCHAR2,
			p_code_interne				IN VARCHAR2,
			p_reference_eq				IN VARCHAR2,
			p_quantite					IN INTEGER,
			p_prefix					IN VARCHAR2 DEFAULT NULL,	
										
			p_te_code_ihm				OUT VARCHAR2,
			p_retour					OUT INTEGER 
--->												
												
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.ins_Equipement">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idequipement_client#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idemplacement#" null="true">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_libelle_eq#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_commentaire_eq#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_code_interne#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_reference_eq#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_quantite#" null="false">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_prefix#" null="false">
			
			<cfprocparam type="out" cfsqltype="cF_SQL_VARCHAR" variable="code_ihm">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="idequip">
		</cfstoredproc>		
		<cfset ret = arrayNew(1)>
		
		<cfset ret[1] = idequip> 
		<cfset ret[2] = code_ihm> 
		
		<cfreturn ret>
	</cffunction>
	
	<cffunction name="searchCatalogue" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		
<!---
	PROCEDURE
		 
		pkg_cv_equipement.SearchCatalogue
		
			p_idcategorie_equipement	IN INTEGER
			p_idtype_equipement			IN INTEGER
			p_idfournisseur				IN INTEGER
			p_idgamme_fournis			IN INTEGER
			p_chaine					IN VARCHAR2
			p_idgroupe_client			IN INTEGER
			p_retour 					OUT SYS_REFCURSOR
--->
		<cfset p_idcategorie_equipement = arr[1]>
		<cfset p_idtype_equipement = arr[2]>
		<cfset p_idfournisseur = arr[3]>
		<cfset p_idgamme_fournis = arr[4]>
		<cfset p_chaine = arr[5]>
		<cfset p_idgroupe_client = arr[6]>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.searchcatalogue">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcategorie_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idtype_equipement#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfournisseur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgamme_fournis#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfset obj=createObject("component","fr.consotel.consoview.inventaire.session")>
		<cfset obj.setData(p_result,"listeEquipements")>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="searchCatalogueRevendeur" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		
<!---	
	PROCEDURE
	
		pkg_cv_equipement.SearchCatalogueRevendeur
		
			p_idcategorie_equipement	IN INTEGER
			p_idtype_equipement			IN INTEGER
			p_idfournisseur				IN INTEGER
			p_idgamme_fournis			IN INTEGER
			p_chaine					IN VARCHAR2
			p_idgroupe_client			IN INTEGER
			p_type_fournisseur			IN INTEGER
			p_retour 					OUT SYS_REFCURSOR
--->		
		<cfset p_idcategorie_equipement = arr[1]>
		<cfset p_idtype_equipement = arr[2]>
		<cfset p_idfournisseur = arr[3]>
		<cfset p_idgamme_fournis = arr[4]>
		<cfset p_chaine = arr[5]>
		<cfset p_idgroupe_client = session.perimetre.ID_GROUPE>
		<cfset p_type_fournisseur = arr[7]>
				
		<cfif p_type_fournisseur eq 0>
			<cfset flag="true">
		<cfelse>
			<cfset flag="false">
		</cfif>
				
		<!--- <cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.SearchCatalogueRevendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcategorie_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idtype_equipement#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfournisseur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgamme_fournis#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_type_fournisseur#" null="#flag#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		 --->
		 
		 <!--- Modif pour Intégration ICECAT --->
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.SearchCatalogueRevendeur_v3">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcategorie_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfournisseur#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="3">
			<cfprocresult resultset="1" name="p_result">
			<cfprocresult resultset="2" name="p_result2">
		</cfstoredproc>
		
		<cfloop query="p_result2">
			<cfset queryAddrow(p_result,1)>
			<cfset querySetCell(p_result,"IDEQUIPEMENT_FOURNIS",p_result2.IDEQUIPEMENT_FOURNIS)>
			<cfset querySetCell(p_result,"CATEGORIE_EQUIPEMENT",p_result2.CATEGORIE_EQUIPEMENT)>
			<cfset querySetCell(p_result,"TYPE_EQUIPEMENT",p_result2.TYPE_EQUIPEMENT)>
			<cfset querySetCell(p_result,"NOM_FABRIQUANT",p_result2.NOM_FABRIQUANT)>
			<cfset querySetCell(p_result,"LIBELLE_GAMME",p_result2.LIBELLE_GAMME)>
			<cfset querySetCell(p_result,"LIBELLE_MODELE",p_result2.LIBELLE_MODELE)>
			<cfset querySetCell(p_result,"THUMB_PIC",p_result2.THUMB_PIC)>
			<cfset querySetCell(p_result,"PROD_ID",p_result2.PROD_ID)>
			<cfset querySetCell(p_result,"TE_CODE_IHM",p_result2.TE_CODE_IHM)>
			
		</cfloop>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="searchCatalogueConstructeur" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		
<!---	
	PROCEDURE			
		pkg_cv_equipement.SearchCatalogueRevendeur
	
	PARAMS	
			p_idcategorie_equipement	IN INTEGER
			p_idtype_equipement			IN INTEGER
			p_idfournisseur				IN INTEGER
			p_idgamme_fournis			IN INTEGER
			p_chaine					IN VARCHAR2
			p_idgroupe_client			IN INTEGER
			p_type_fournisseur			IN INTEGER
			
			p_retour 					OUT SYS_REFCURSOR
--->		
		<cfset p_idcategorie_equipement = arr[1]>
		<cfset p_idtype_equipement = arr[2]>
		<cfset p_idfournisseur = arr[3]>
		<cfset p_idgamme_fournis = arr[4]>
		<cfset p_chaine = arr[5]>
		<cfset p_idgroupe_client = session.perimetre.ID_GROUPE>
		<cfset p_type_fournisseur = arr[7]>
				
		<cfset p_type_fournisseur = 0>
				
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.SearchCatalogueRevendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcategorie_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idtype_equipement#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfournisseur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgamme_fournis#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_type_fournisseur#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfset obj=createObject("component","fr.consotel.consoview.inventaire.session")>
		<cfset obj.setData(p_result,"listeEquipements")>
		<cfreturn p_result>
	</cffunction>

	<cffunction name="searchCatalogueClient" returntype="query" access="remote">
		<cfargument name="arr" type="array">
<!---
	PROCEDURE
		
		pkg_cv_equipement.searchcatalogueclient
			p_idracine					IN INTEGER
			p_idcategorie_equipement	IN INTEGER
			p_idtype_equipement			IN INTEGER
			p_idfournisseur				IN INTEGER
			p_idgamme_fournis			IN INTEGER
			p_chaine					IN VARCHAR2
			p_idgroupe_client			IN INTEGER
			p_retour 					OUT SYS_REFCURSOR
--->		
		<cfset p_idracine = session.perimetre.ID_GROUPE>		
		<cfset p_idcategorie_equipement = arr[1]>		
		<cfset p_idtype_equipement = arr[2]>		
		<cfset p_idfournisseur = arr[3]>
		<cfset p_type_fournisseur = arr[4]>
		<cfset p_idgamme_fournis = arr[5]>
		<cfset p_chaine = arr[6]>
		<cfset p_idgroupe_client = session.perimetre.ID_PERIMETRE>
							
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.searchcatalogueclient_v2">

			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcategorie_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idtype_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfournisseur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgamme_fournis#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_type_fournisseur#">
			<cfprocresult name="p_result">
			
		</cfstoredproc>
		
		<cfset obj=createObject("component","fr.consotel.consoview.inventaire.session")>
		<cfset obj.setData(p_result,"listeEquipements")>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="searchEquipement" returntype="query" access="remote">		
<!---

	--------------------------------------------------------------------------------------------
	-- Ramene une liste d'equipements d�finie selon un certain nombre de crit�res 	 	 	  --
	-- de recherche. on filtre par site, par emplacement et par commande.			 	      --
	-- si idcommande = 0 alors filtrer sur mois ou annee commande sinon sur idcomm uniquement --
	--------------------------------------------------------------------------------------------
	
	PROCEDURE 
		pkg_cv_equipement.Search_Equipement
			p_idracine				IN INTEGER
			p_chaine_lib			IN VARCHAR2
			p_idgroupe_client		IN INTEGER
			p_idemplacement			IN INTEGER
			p_idcommande			IN INTEGER
			p_mois_commande			IN VARCHAR2 default NULL -- 'MM'
			p_annee_commande		IN VARCHAR2 default NULL -- 'YYYY'
    		p_retour 				OUT SYS_REFCURSOR
--->			
		<cfargument name="p_idracine" type="numeric" required="true" default="#session.perimetre.ID_GROUPE#">
		<cfargument name="p_chaine_lib" type="string" required="true" >
		<cfargument name="p_idgroupe_client" type="numeric" required="true" default="#session.perimetre.ID_PERIMETRE#">
		
		<cfargument name="p_idemplacement"  type="numeric" required="false">				
		<cfargument name="p_idcommande"  type="numeric" required="false" default="0">
		<cfargument name="p_mois_commande" type="string" required="false" default="1">
		<cfargument name="p_annee_commande" type="string" required="false" default="1960">
				 			
		<!--- <cfset p_idgroupe_client = 0>	
		
		<cfif p_idgroupe_client eq 0>
			<cfset p_idgroupe_client = ""/>
		</cfif> --->
		
				
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.search_equipement_v3" blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_chaine_lib#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#"  null="#iif((p_idgroupe_client eq 0), de("true"), de("false"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idemplacement#" null="#iif((p_idemplacement eq 0), de("true"), de("false"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcommande#" null="#iif((p_idcommande eq 0), de("true"), de("false"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_mois_commande#" null="#iif((p_annee_commande eq '0'), de("true"), de("false"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_annee_commande#" null="#iif((p_annee_commande eq '0'), de("true"), de("false"))#">
			<cfprocresult name="p_result" maxrows="100">
		</cfstoredproc>
		
		
		<cfreturn p_result>
	</cffunction>
	
	

<!--- ============================== SUPPRIMER LES EQUIPEMENTS DU CATALOGUE =============================================== --->
	<cffunction name="removeEquipementFab" returntype="numeric" access="remote">
		<cfargument name="id" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.deleteEquipementfournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipement" value="#id#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_FLOAT" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="removeEquipementFour" returntype="numeric" access="remote">
		<cfargument name="id" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.deleteEquipementfournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipement" value="#id#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_FLOAT" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="removeEquipementClient" returntype="numeric" access="remote">
		<cfargument name="id" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.deleteEquipementClient">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipement" value="#id#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_FLOAT" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="removeEquipement" returntype="numeric" access="remote">
		<cfargument name="id" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.deleteEquipement">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipement" value="#id#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_FLOAT" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
<!--- ============================== SUPPRIMER LES EQUIPEMENTS DU CATALOGUE FIN=============================================== --->
<!--- ============================== SUPPRIMER LES EQUIPEMENTS ENFANTS 		   =============================================== --->
	<cffunction name="removeEquipementChildFab" returntype="numeric" access="remote">
		<cfargument name="idpere" type="numeric">
		<cfargument name="idfils" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.DeleteEquipementChild">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipementParent" value="#idpere#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipementFils" value="#idfils#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_FLOAT" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="removeEquipementChildFour" returntype="numeric" access="remote">
		<cfargument name="idpere" type="numeric">
		<cfargument name="idfils" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.DeleteEquipementChild">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipementParent" value="#idpere#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipementFils" value="#idfils#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_FLOAT" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="removeEquipementChildClient" returntype="numeric" access="remote">
		<cfargument name="idpere" type="numeric">
		<cfargument name="idfils" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.DeleteEquipementChild">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipementParent" value="#idpere#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipementFils" value="#idfils#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_FLOAT" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="removeEquipementChild" returntype="numeric" access="remote">
		<cfargument name="idpere" type="numeric">
		<cfargument name="idfils" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.DeleteEquipementChild">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipementParent" value="#idpere#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipementFils" value="#idfils#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_FLOAT" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
<!--- ============================== SUPPRIMER LES EQUIPEMENTS ENFANTS 		FIN=============================================== --->
	
	
<!--- ============================== FOURNISSEUR ============================================================================= --->	
	<cffunction name="getListeFournisseurs" returntype="Numeric" access="remote">
		 	<cfargument name="idRacine" required="true" type="string"/>		 	
		 	<cfargument name="chaine" required="true" type="string"/>
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.liste_fournisseurs">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idRacine#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
	
	<cffunction name="addFournisseur" returntype="Numeric" access="remote">
		 	<cfargument name="idGroupe_client" required="true" type="numeric" />
		 	<cfargument name="nom" required="true" type="string" />
		 	<cfargument name="adresse" required="true" type="string" />
		 	<cfargument name="adresse2" required="true" type="string" />
		 	<cfargument name="codePostal" required="true" type="string" />
		 	<cfargument name="commune" required="true" type="string" />
		 	<cfargument name="pays" required="true" type="string" />
		 	<cfargument name="paysOrigine" required="true" type="string" />
		 	<cfargument name="type" required="true" type="numeric" />
		 	
		 	<cfset flag = false>
		 	<cfif idGroupe_client eq 0>
		 		<cfset flag = true>
		 	<cfelse>
		 		<cfset flag = false>
		 	</cfif>
		 	
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Add_fournisseur">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#nom#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe_client#" null="#flag#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adresse#" null="false">				
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adresse2#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codePostal#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commune#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#pays#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#paysOrigine#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#type#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			
			<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="updateFournisseur" returntype="Numeric" access="remote">
		 	<cfargument name="idFournisseur" required="true" type="numeric"/>
		 	<cfargument name="idGroupe_client" required="true" type="numeric"/>
		 	<cfargument name="nom" required="true" type="string" />
		 	<cfargument name="adresse" required="true" type="string" />
		 	<cfargument name="adresse2" required="true" type="string" />
		 	<cfargument name="codePostal" required="true" type="string" />
		 	<cfargument name="commune" required="true" type="string" />
		 	<cfargument name="pays" required="true" type="string" />
		 	<cfargument name="paysOrigine" required="true" type="string" />
		 	<cfargument name="type" required="true" type="numeric" />
		 	
		 	<cfif not isDefined("idGroupe_client")>
				<cfreturn -2>
			</cfif>
			
		 	<cfif idGroupe_client lt 1>
			 	<cfreturn -3>
		 	</cfif>
		 			 	
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.maj_fournisseur">		 		
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idFournisseur#" null="#flag#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#nom#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adresse#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="adresse2" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codePostal#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commune#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#pays#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#paysOrigine#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#type#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
						
			<cfreturn p_retour>			
	</cffunction>  
	
	<cffunction name="deleteFournisseur" returntype="Numeric" access="remote">
		 	<cfargument name="idfournisseur" required="true" type="string" />
		 	
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Del_fournisseur">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idfournisseur#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction> 
	

<!--- ========= FOURNISSEUR FIN =========================================================================================== --->
	
	<cffunction name="addGamme" returntype="Numeric" access="remote">
		 	<cfargument name="idFournisseur" required="true" type="string" />
		 	<cfargument name="nom" required="true" type="string" />
		 	<cfargument name="commentaire" required="true" type="string" />
		 	
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Add_Gamme">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#nom#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idFournisseur#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction> 
	
	<cffunction name="deleteGamme" returntype="Numeric" access="remote">
		 	<cfargument name="idgamme_fournis" required="true" type="string" />		 	
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Del_Gamme">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idgamme_fournis#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction> 
	
	<cffunction name="getListePays" returntype="query" access="remote">
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.liste_pays">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="getLigneInfo" returntype="query" access="remote">
			<cfargument name="idsoustete" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Info_Ligne">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idsoustete#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getLigneInfoLL" returntype="query" access="remote">
			<cfargument name="idsoustete" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Info_LigneLL">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idsoustete#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getAllUse" returntype="query" access="remote">
			<cfargument name="igroupe" required="true" type="string" />
			<cfset realid= igroupe/>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Liste_Usage">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#realid#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
	 
	<cffunction name="getFicheSiteByLine" returntype="query" access="remote">
			<cfargument name="idsoustete" required="true" type="string" />
			<cfset realid=session.perimetre.ID_PERIMETRE />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.FicheSite_Ligne">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#realid#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idsoustete#" null="false">
				
				
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction> 
	
	<cffunction name="getFicheData" returntype="array" access="remote">
			<cfargument name="idsoustete" required="true" type="string" />
			<cfargument name="igroupe" required="true" type="numeric" />
			<cfargument name="datedeb" required="true" type="string" />
			<cfargument name="datefin" required="true" type="string" />
			<cfset realid=session.ID_PERIMETRE/>
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Info_Ligne">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idsoustete#" null="false">
				<cfprocresult name="list1">
			</cfstoredproc>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Liste_Usage">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#realid#" null="false">
				<cfprocresult name="list2">
			</cfstoredproc>
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.OpeTraffic">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idsoustete#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#datedeb#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#datefin#" null="false">
				<cfprocresult name="list3">
			</cfstoredproc>
			<cfset dataArr = arrayNew(1)>
			<cfset dataArr[1] = list1>
			<cfset dataArr[2] = list2>
			<cfset dataArr[3] = list3>
			<cfreturn dataArr>
	</cffunction>
	
	<cffunction name="getFicheDataLL" returntype="array" access="remote">
			<cfargument name="idsoustete" required="true" type="string" />
			<cfargument name="igroupe" required="true" type="numeric" />
			<cfargument name="datedeb" required="true" type="string" />
			<cfargument name="datefin" required="true" type="string" />
			<cfset realid=session.perimetre.ID_PERIMETRE/>
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Info_Ligne">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idsoustete#" null="false">
				<cfprocresult name="list1">
			</cfstoredproc>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Liste_Usage">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#realid#" null="false">
				<cfprocresult name="list2">
			</cfstoredproc>
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.OpeTraffic">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idsoustete#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#datedeb#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#datefin#" null="false">
				<cfprocresult name="list3">
			</cfstoredproc>
			<cfset dataArr = arrayNew(1)>
			<cfset dataArr[1] = list1>
			<cfset dataArr[2] = list2>
			<cfset dataArr[3] = list3>
			<cfset dataArr[4] = getLigneInfoLL(idsoustete)>
			<cfreturn dataArr>
	</cffunction>
	
	<cffunction name="updateInfoLine" returntype="numeric" access="remote" output="true">
		<cfargument name="idsoustete" required="true" type="string" />
		<cfargument name="codeinterne" required="true" type="string" />
		<cfargument name="usage" required="true" type="string" />
		<cfargument name="bool_principal" required="true" type="string" />
		<cfargument name="commentaires" required="true" type="string" />
		<cfargument name="prestataires" required="true" type="string" />
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Update_Info_Ligne">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsoustete#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeinterne#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#usage#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#bool_principal#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#prestataires#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="updateDataLL" returntype="numeric" access="remote" output="true">
		<cfargument name="idsoustete" required="true" type="string" />
		<cfargument name="adresse_o" required="true" type="string" />
		<cfargument name="cp_o" required="true" type="string" />
		<cfargument name="adresse_e" required="true" type="string" />
		<cfargument name="cp_e" required="true" type="string" />
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.update_data_LL">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsoustete#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adresse_o#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cp_o#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#adresse_e#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cp_e#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	
	
	
	
</cfcomponent>