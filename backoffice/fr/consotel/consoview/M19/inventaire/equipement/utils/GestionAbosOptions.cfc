<cfcomponent output="false">
	<cffunction name="findDataFromContrat" access="remote" output="false" returntype="array">
		<cfargument name="idContrat" type="Numeric" required="true"/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.DETAILCONTRAT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idContrat" value="#idContrat#"/>
			<cfprocresult name="qDeatilContrat">
		</cfstoredproc>
		
		<cfset result = arrayNew(1)>
		<cfset arrayAppend(result,qDeatilContrat)>
		<cfset arrayAppend(result,findEqtsContrats(idContrat))>
		<cfset arrayAppend(result,findRessourcesContrats(idContrat))>
			
		<cfreturn result/>	
	</cffunction>	


	<cffunction name="findEqtsContrats" access="private" output="false" returntype="query">
		<cfargument name="idcontrat" type="Numeric" required="true"/>
		<cfargument name="idpool" type="Numeric" required="false"/>		
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.FIND_EQCTRT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idContrat" value="#idcontrat#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idpool" value="0"/>
			<cfprocresult name="qlisteEqs">
		</cfstoredproc>
		<cfreturn qlisteEqs/>	
	</cffunction>
		
	<cffunction name="findRessourcesContrats" access="private" output="false" returntype="query">
		<cfargument name="idcontrat" type="Numeric" required="true"/>		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.FIND_RESSCTRT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idContrat" value="#idcontrat#"/>
			<cfprocresult name="qlisteRessources">
		</cfstoredproc>	
		<cfreturn qlisteRessources/>	
	</cffunction>
	
	
	<cffunction name="findRessourcesSousTete" access="remote" output="false" returntype="query">
		<cfargument name="idSousTete" type="Numeric" required="true"/>
		<cfargument name="idOperateur" type="Numeric" required="true"/>
		<cfargument name="idContrat" type="Numeric" required="false" default="0"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.FIND_RESSSOUS_TETE_BYOP">			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idSousTete" value="#idSousTete#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idContrat" value="#idSousTete#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idoperateur" value="#idOperateur#"/>
			<cfprocresult name="qlisteRessources">
		</cfstoredproc>
		<cfreturn qlisteRessources/>	
	</cffunction>
	
	<cffunction name="findContratSousTete" access="remote" output="false" returntype="query">
		<cfargument name="idSousTete" type="Numeric" required="true"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.GET_ST_CTRT_ABO">			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idSousTete" value="#idSousTete#"/>
			<cfprocresult name="qContrat">
		</cfstoredproc>
		<cfreturn qContrat/>	
	</cffunction>
	
	<cffunction name="genererCommandeFromSousTete" access="remote" output="false" returntype="ANY">
		<cfargument name="idSousTete" type="Numeric" required="true"/>
				
		<cfset qProduits=queryNew("IDINVENTAIRE_PRODUIT","Integer")>
		<cfset qContrat=queryNew("IDCONTRAT,OPERATEURID","Integer,Integer")>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.GET_ST_CTRT_ABO">			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idSousTete" value="#idSousTete#"/>
			<cfprocresult name="qContrat">
		</cfstoredproc>
				
		 <!--- <cfif qContrat.recordCount gt 0>
			<cftry> 
				<cfset qProduits = findRessourcesSousTete(idSousTete,qContrat["OPERATEURID"][2],qContrat["IDCONTRAT"][1])>
			<cfcatch>
			</cfcatch>
			</cftry>
		</cfif>   --->
		
		<cfset result = arrayNew(1)>
		<cfset arrayAppend(result,qContrat)>
		<cfset arrayAppend(result,qProduits)>
		
		<cfreturn qContrat/>	
	</cffunction>
	
	<cffunction name="renouvellerContratAboSoustete" access="remote" output="false" returntype="numeric">
		<cfargument name="idsoustete" type="Numeric" required="true"/>
		<cfargument name="newDuree" type="Numeric" required="true"/>
		
		<cfset contrat = findContratSousTete(idsoustete)>
		<cfif contrat.recordCount gt 0>
			<cfset idContrat = contrat["IDCONTRAT"][1]>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.RENOUVELLERCONTRAT" blockfactor="1">
				<cfset dateRenouv = LsDAteFormat(now(),"YYYY/MM/DD")>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idContrat" value="#idContrat#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_dateRenouvellement" value="#dateRenouv#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="newDuree" value="#newDuree#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>
		<cfelse>
			<cfreturn -77/>
		</cfif>
		<cfreturn p_retour/>	
	</cffunction>
</cfcomponent>