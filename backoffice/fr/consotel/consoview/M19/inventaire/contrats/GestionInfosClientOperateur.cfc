<!--- =========================================================================
Classe: GestionInfosClientOperateur
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GestionInfosClientOperateur" hint="" >
    <cfset IDRACINE = SESSION.PERIMETRE.ID_GROUPE>
	<cfset IDUSER = SESSION.USER.CLIENTACCESSID>
	<cfset DSN = #SESSION.OFFREDSN# >  
	<cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GestionInfosClientOperateur" hint="Remplace le constructeur de GestionInfosClientOperateur.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<cffunction name="deleteInfosClientOperateur" access="public" returntype="numeric" output="false"  >
		<cfargument name="infos" required="true" type="InfosClientOperateurVo" default="" displayname="InfosClientOperateurVo infos" hint="Initial value for the infosproperty." />
		
		<cfstoredproc datasource="#DSN#" procedure="PKG_CV_CONTRAT.DELETEINFOCLIENTOP" blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur_client" value="#infos.getOPERATEUR_CLIENT_ID()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_usr" value="#IDUSER#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour" >					
		</cfstoredproc>
		
		<cfreturn p_retour>		
		
	</cffunction>
	
	<cffunction name="getHistoriqueInfosClientOperateur" access="public" returntype="query" output="false"  >
		
		<cfstoredproc datasource="#DSN#" procedure="PKG_CV_CONTRAT.GETHISTORIQUEINFOSCLIENTOP" blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#IDRACINE#">
			<cfprocresult name="qHistorique">			
		</cfstoredproc>
		
		<cfreturn qHistorique >
	</cffunction>
	
	<cffunction name="getInfosClientOperateurActuel" access="public" returntype="InfosClientOperateurVo" output="false"  >
		<cfargument name="IDOPERATEUR" required="true" type="numeric"/>
		
		<cfset histoInfos = getHistoriqueInfosClientOperateur()>
		<cfquery dbtype="query" name="qGetInfo">
			select * from histoInfos where histoInfos.BOOL_ACTUEL = 1
			and  histoInfos.OPERATEURID = #IDOPERATEUR#
		</cfquery>
		 
		<!--- 
		<cfstoredproc datasource="#DSN#" procedure="PKG_CV_CONTRAT.INFOSCLIENTOPERATEURACTUEL" blockfactor="1">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" value="#IDOPERATEUR#">			
			<cfprocresult name="qGetInfo">			
		</cfstoredproc>
		 --->
		 
		<cfset infos = createObject("component","InfosClientOperateurVo")>
		
				
		<cfif qGetInfo.recordCount gt 0>
			<cfset infos.setOPERATEUR_CLIENT_ID(qGetInfo["OPERATEUR_CLIENT_ID"][1])/>
			<cfset infos.setOPERATEURID(qGetInfo["OPERATEURID"][1])/>
			<cfset infos.setNOM_OP(qGetInfo["NOM_OP"][1])/>
			
			<cfset infos.setIDRACINE(qGetInfo["IDRACINE"][1])/>
			
			<cfset infos.setDATE_MODIF(qGetInfo["DATE_MODIF"][1])/>
			<cfset infos.setDATE_CREATE(qGetInfo["DATE_CREATE"][1])/>
			
			<cfset infos.setMONTANT_MENSUEL_PENALITE(val(qGetInfo["MONTANT_MENSUEL_PENALITE"][1]))/>
			<cfset infos.setFRAIS_FIXE_RESILIATION(val(qGetInfo["FRAIS_FIXE_RESILIATION"][1]))/>
			<cfset infos.setFPC_UNIQUE(qGetInfo["FPC_UNIQUE"][1])/>
			<cfset infos.setBOOL_ACTUEL(val(qGetInfo["BOOL_ACTUEL"][1]))/>
			
			<cfset infos.setUSER_MODIF(val(qGetInfo["USER_MODIF"][1]))/>
			<cfset infos.setUSER_CREATE(val(qGetInfo["USER_CREATE"][1]))/>
			
			<cfset infos.setNOM_USER_MODIFIED(qGetInfo["NOM_USER_MODIFIED"][1])/>
			<cfset infos.setNOM_USER_CREATE(qGetInfo["NOM_USER_CREATE"][1])/>
			
			<cfset infos.setENGAG_12(val(qGetInfo["ENGAG_12"][1]))/>			
			<cfset infos.setENGAG_24(val(qGetInfo["ENGAG_24"][1]))/>
			<cfset infos.setENGAG_36(val(qGetInfo["ENGAG_36"][1]))/>
			<cfset infos.setENGAG_48(val(qGetInfo["ENGAG_48"][1]))/>
			<cfset infos.setSEUIL_TOLERANCE(val(qGetInfo["SEUIL_TOLERANCE"][1]))/>
			
		</cfif>
		
		<cfreturn infos>
	</cffunction>
	
	<cffunction name="saveInfosClientOperateur" access="public" returntype="InfosClientOperateurVo" output="false"  >
		<cfargument name="infos" required="true" type="InfosClientOperateurVo" default="" displayname="InfosClientOperateurVo infos" hint="Initial value for the infosproperty." />
		<cfstoredproc datasource="#DSN#" procedure="PKG_CV_CONTRAT.ADDINFOCLIENTOP_V2" blockfactor="1">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_IDOPERATEUR" value="#infos.getOPERATEURID()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_IDRACINE" value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_MVAR" value="#infos.getMONTANT_MENSUEL_PENALITE()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_MFIXE" value="#infos.getFRAIS_FIXE_RESILIATION()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="P_FPC" value="#LSDATEFORMAT(infos.getFPC_UNIQUE(),'YYYY/MM/DD')#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_USR" value="#IDUSER#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_ACTUEL" value="#infos.getBOOL_ACTUEL()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_ENGAG_12" value="#infos.getENGAG_12()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_ENGAG_24" value="#infos.getENGAG_24()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_ENGAG_36" value="#infos.getENGAG_36()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_ENGAG_48" value="#infos.getENGAG_48()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_SEUIL" value="#infos.getSEUIL_TOLERANCE()#">			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour" >					
		</cfstoredproc>
		
		<cfset infos = getInfosClientOperateurActuel(infos.getOPERATEURID())>
		
		<cfreturn infos>		 
	</cffunction>
	
	<cffunction name="updateInfosClientOperateur" access="public" returntype="InfosClientOperateurVo" output="false"  >
		<cfargument name="infos" required="true" type="InfosClientOperateurVo" default="" displayname="InfosClientOperateurVo infos" hint="Initial value for the infosproperty." />
		
		<cfstoredproc datasource="#DSN#" procedure="PKG_CV_CONTRAT.UPDATEINFOCLIENTOP_V2" blockfactor="1">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_IDOPERATEUR_CLIENT" value="#infos.getOPERATEUR_CLIENT_ID()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_IDOPERATEUR" value="#infos.getOPERATEURID()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_IDRACINE" value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_MVAR" value="#infos.getMONTANT_MENSUEL_PENALITE()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_MFIXE" value="#infos.getFRAIS_FIXE_RESILIATION()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="P_FPC" value="#LSDATEFORMAT(infos.getFPC_UNIQUE(),'YYYY/MM/DD')#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_USR" value="#IDUSER#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_ACTUEL" value="#infos.getBOOL_ACTUEL()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_ENGAG_12" value="#infos.getENGAG_12()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_ENGAG_24" value="#infos.getENGAG_24()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_ENGAG_36" value="#infos.getENGAG_36()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_ENGAG_48" value="#infos.getENGAG_48()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_SEUIL" value="#infos.getSEUIL_TOLERANCE()#">			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour" >					
		</cfstoredproc>
		
		<cfset infos = getInfosClientOperateurActuel(infos.getOPERATEURID())>
		
		<cfreturn infos>
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>