<cfcomponent displayname="CommandeSNCF">

<!--- OK --->
	<cffunction name="genererNumeroDeCommande" access="public" returntype="Struct" output="false">
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.get_numero_commande">
				<cfprocparam type="OUT" cfsqltype="CF_SQL_VARCHAR" variable="p_numero_commande">									
				<cfprocparam type="OUT" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>
			<cfset obj = StructNew()>
			<cfset obj.NUMERO_COMMANDE = p_numero_commande>
			<cfset obj.RESULT = p_retour>
			
		<cfreturn obj>
	</cffunction>

<!--- OK --->
	<cffunction name="fournirNumeroMarche" access="public" returntype="Struct" output="false">
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.get_numero_marche">
				<cfprocparam type="OUT" cfsqltype="CF_SQL_VARCHAR" variable="p_numero_marche">									
				<cfprocparam type="OUT" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>
			<cfset obj = StructNew()>
			<cfset obj.NUMERO_MARCHE = p_numero_marche>
			<cfset obj.RESULT = p_retour>
			
		<cfreturn obj>
	</cffunction>

<!--- OK --->
	<cffunction name="fournirProfilEquipements" access="public" returntype="Query" output="false">
		<cfargument name="idPoolGestion" 	required="true" type="numeric" />
		<cfargument name="idGestionnaire" 	required="true" type="numeric" />
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.get_type_commande">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 			value="#idPoolGestion#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

<!--- OK --->
	<cffunction name="fournirOperateur" access="public" returntype="Query" output="false">
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.getFabOpSFR">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

<!--- OK --->
	<cffunction name="fournirTitulaire" access="public" returntype="Query" output="false">
		<cfargument name="idOperateur" required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.L_CF_RACINE_OP">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" value="#idOperateur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

<!--- OK --->
	<cffunction name="fournirPointFacturation" access="public" returntype="Query" output="false">
		<cfargument name="idTitulaire" required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.L_sous_compte_cf">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte_facturation" value="#idTitulaire#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
<!--- OK --->	
	<cffunction name="fournirCodeListe" access="public" returntype="Query" output="false">
		<cfargument name="idCompteFacturation" required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.L_code_liste_pf">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idsouscompte" value="#idCompteFacturation#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
<!--- OK --->
	<cffunction name="fournirRevendeurs" access="public" returntype="Query" output="false">
		<cfargument name="idRacine" 			required="true" type="numeric"/>
		<cfargument name="idPoolGestion" 		required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 	required="true" type="numeric"/>
		<cfargument name="clefRecherche" 		required="true" type="String" />
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.get_revendeur_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#Session.USER.CLIENTACCESSID#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 				value="#idPoolGestion#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_Chaine" 				value="#clefRecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_idracine" 			value="#idRacine#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

<!---  --->	
	<cffunction name="fournirListeEquipements" access="public" returntype="Query" output="false"><!--- ICI --->
		<cfargument name="idGestionnaire" 			required="true" type="numeric"/>
		<cfargument name="idPoolGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 		required="true" type="numeric"/>
		<cfargument name="idFournisseur" 			required="true" type="numeric"/>
		<cfargument name="clefRecherche" 			required="true" type="string"/>
		
		<cfset idCategorieEquipement = 0>		
		<cfset idTypeEquipement = 0>		
		<cfset typeFournis = 1>
		<cfset idGammeFournis = 0>
		<cfset niveau = 1>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.SearchCatalogueclient">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#idPoolGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#idCategorieEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#idTypeEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#idFournisseur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#idGammeFournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 					value="#clefRecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournis" 			value="#typeFournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_niveau" 					value="#niveau#" null="true">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

<!---  --->
	<cffunction name="fournirListeAbonnementsOptions" access="public" returntype="Query" output="false"><!---ICI --->
		<cfargument name="idGestionnaire" 			required="true" type="numeric"/>
		<cfargument name="idOperateur" 			required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 	required="true" type="numeric"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.Liste_Produits_profil">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" 			value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" 			value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 		value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

<!---  --->
	<cffunction name="sauvegardeModele" access="public" returntype="numeric" output="false">
		<cfargument name="idRacine" 				required="true" type="numeric"/>
		<cfargument name="idGestionnaire" 			required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 		required="true" type="numeric"/>
		<cfargument name="idPoolgestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idOperateur" 				required="true" type="numeric"/>
		<cfargument name="idrevendeur" 				required="true" type="numeric"/>
		<cfargument name="idContactRevendeur" 		required="true" type="numeric" default="NULL"/>
		<cfargument name="idSiteLivraison" 			required="true" type="numeric" default="NULL"/>
		<cfargument name="libelleOperation" 		required="true" type="string"/>
		<cfargument name="referenceClient" 			required="true" type="string"/>
		<cfargument name="referenceOperateur" 		required="true" type="string"/>
		<cfargument name="commentaires" 			required="true" type="string" default="NULL"/>
		<cfargument name="montant" 					required="true" type="numeric"/>
		<cfargument name="segmentFix" 				required="true" type="numeric"/>
		<cfargument name="segmentMobile" 			required="true" type="numeric"/>
		<cfargument name="inventaireTypeOpe" 		required="true" type="string"/>
		<cfargument name="idGroupePrefere" 			required="true" type="numeric" default="NULL"/>
		<cfargument name="articles" 				required="true" type="string"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_SNCF.SaveModeleCommande_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpoolgestionnaire" 		value="#idPoolgestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 				value="#idOperateur#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 				value="#idrevendeur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 		value="#idContactRevendeur#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 		value="#idSiteLivraison#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_operation" 		value="#libelleOperation#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_client" 				value="#referenceClient#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_revendeur" 			value="#referenceOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_commentaires" 			value="#commentaires#">
				<cfprocparam type="in" cfsqltype="CF_SQL_NUMERIC" variable="p_montant" 					value="#montant#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment_fixe" 			value="#segmentFix#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment_mobile" 			value="#segmentMobile#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_type_ope" 			value="#inventaireTypeOpe#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 			value="#idGroupePrefere#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" 	  variable="p_articles" 				value="#tostring(articles)#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 				value="#idRacine#">
				<cfprocparam type="OUT" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

<!---  --->
	<cffunction name="fournirListeModele" access="public" returntype="Query" output="false">
		<cfargument name="idPool" required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.getListeModeleCommande">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 				value="#idPool#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

<!---  --->	
<!--- 	<cffunction name="functionActePour" access="remote" returntype="Query">
		
			<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
			
			<cfquery datasource="#session.offreDSN#" name="p_result">
				SELECT DISTINCT pg.app_loginid,al.login_nom,al.login_prenom,al.login_email,pg.idprofil_commande,pg.idpool,pl.libelle,pl.idrevendeur
				FROM pool_gestionnaire pg,app_login al,pool_flotte pl
				 
				   
				WHERE pg.app_loginid=al.app_loginid
				AND pg.idpool NOT IN 
				(
				SELECT pg.idpool
				   FROM pool_gestionnaire pg,app_login al,pool_flotte pl
				   WHERE pg.app_loginid=al.app_loginid
				   AND pg.idpool = pl.idpool
				   AND pl.idracine = #IDRACINE#
				   AND pl.idrevendeur IS NOT NULL
				 )  
				   
				AND pg.idpool = pl.idpool
				AND pl.idracine = #IDRACINE#
				ORDER BY al.login_nom,al.login_prenom,al.login_email
			</cfquery>
			
		<cfreturn p_result>
	</cffunction> --->

	<cffunction name="functionActePour" access="remote" returntype="Query">
		<cfargument name="idProfilEquipement" required="true" type="numeric"/>
		<cfargument name="idRevendeur" 		  required="true" type="numeric"/>

			<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.GESTIONNAIRES_CLIENT_V2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#IDRACINE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 			value="#idRevendeur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<cffunction name="enregistrerCommande" access="public" returntype="numeric" output="false" hint="">		
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idCompte" 			required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idSousCompte" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idOperateur" 			required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="idContactRevendeur" 	required="true" type="numeric" 	default="0" 	displayname="numeric segment" hint="" />
		<cfargument name="idTransporteur" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="numeroTracking" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="libelleCommande"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="dateOperation"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="dateLivraison"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="commentaires" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="boolDevis" 			required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idTypeCommande" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idInventaireOpe" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idGroupeRepere" 		required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="numeroCommande" 		required="true" type="string" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idProfileEquipement"	required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="articles" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		
		
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.enregistrerOperation">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" 						value="#idCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte" 					value="#idSousCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 						value="#idOperateur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 				value="#idContactRevendeur#" null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" 					value="#idTransporteur#" null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 				value="#idSiteDeLivraison#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_effet_prevue" 				value="#dateLivraison#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 					value="#idGroupeRepere#">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" null="false" variable="p_article" 				value="#tostring(articles)#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_profil_commande" 	value="#idProfileEquipement#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" >
		</cfstoredproc>
				
		<cfreturn p_idoperation>
	</cffunction>
	
	<cffunction name="enregistrerCommandeActePour" access="public" returntype="numeric" output="false" hint="">		
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idCompte" 			required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idSousCompte" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idOperateur" 			required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="idContactRevendeur" 	required="true" type="numeric" 	default="0" 	displayname="numeric segment" hint="" />
		<cfargument name="idTransporteur" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="numeroTracking" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="libelleCommande"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="dateOperation"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="dateLivraison"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="commentaires" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="boolDevis" 			required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idTypeCommande" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idInventaireOpe" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idGroupeRepere" 		required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="numeroCommande" 		required="true" type="string" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idProfileEquipement"	required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="idActePour" 			required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="articles" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		
		
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.enregistrerOperation">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idActePour#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" 						value="#idCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte" 					value="#idSousCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 						value="#idOperateur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 				value="#idContactRevendeur#" null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" 					value="#idTransporteur#" null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 				value="#idSiteDeLivraison#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_effet_prevue" 				value="#dateLivraison#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 					value="#idGroupeRepere#">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB"    null="false" variable="p_article" 			value="#tostring(articles)#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_profil_commande" 	value="#idProfileEquipement#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" >
		</cfstoredproc>
				
		<cfreturn p_idoperation>
	</cffunction>

	<cffunction name="enregistrerCommandes" access="public" returntype="Array" output="false" hint="">		
		<cfargument name="commandes" 		required="true" type="Array"/>

			<cfset idsCommande = ArrayNew(1)>
 			<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
			<cfset index = 1>
			<cfset checkOK = 1>

 			<cfloop index="idx" from="1" to="#ArrayLen(commandes)#">
				<cfif checkOK GTE 1>
					<cfset idGestionnaire = commandes[idx].COMMANDE.IDGESTIONNAIRE>
					<cfset idPoolGestionnaire = commandes[idx].COMMANDE.IDPOOL_GESTIONNAIRE>
					<cfset idCompte = commandes[idx].COMMANDE.IDCOMPTE_FACTURATION>
					<cfset idSousCompte = commandes[idx].COMMANDE.IDSOUS_COMPTE>
					<cfset idOperateur = commandes[idx].COMMANDE.IDOPERATEUR>
					<cfset idRevendeur = commandes[idx].COMMANDE.IDREVENDEUR>
					<cfset idContactRevendeur = commandes[idx].COMMANDE.IDCONTACT>
					<cfset idTransporteur = commandes[idx].COMMANDE.IDTRANSPORTEUR>
					<cfset idSiteDeLivraison = commandes[idx].COMMANDE.IDSITELIVRAISON>
					<cfset numeroTracking = commandes[idx].COMMANDE.NUMERO_TRACKING>
					<cfset libelleCommande = commandes[idx].COMMANDE.LIBELLE_COMMANDE>
					<cfset refClient = commandes[idx].COMMANDE.REF_CLIENT>
					<cfset refRevendeur = commandes[idx].COMMANDE.REF_OPERATEUR>
					<cfset dateOperation = DateFormat(commandes[idx].COMMANDE.DATE_COMMANDE,'YYYY-MM-DD')>
					<cfset dateLivraison = DateFormat(commandes[idx].COMMANDE.LIVRAISON_PREVUE_LE,'YYYY-MM-DD')>
					<cfset commentaires = commandes[idx].COMMANDE.COMMENTAIRES>
					<cfset boolDevis = commandes[idx].COMMANDE.BOOL_DEVIS>
					<cfset montant = commandes[idx].COMMANDE.MONTANT>
					<cfset segmentFixe = commandes[idx].COMMANDE.SEGMENT_FIXE>
					<cfset segmentMobile = commandes[idx].COMMANDE.SEGMENT_MOBILE>
					<cfset idTypeCommande = commandes[idx].COMMANDE.IDTYPE_COMMANDE>
					<cfset idGroupeRepere = commandes[idx].COMMANDE.IDGROUPE_REPERE>
					<cfset articles = tostring(commandes[idx].ARTICLES)>
					<cfset numeroCommande = commandes[idx].COMMANDE.NUMERO_COMMANDE>
					<cfset idProfileEquipement = commandes[idx].COMMANDE.IDPROFIL_EQUIPEMENT>

					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.enregistrerOperation">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#idRacine#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idGestionnaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcompte" 			value="#idCompte#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsous_compte" 		value="#idSousCompte#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperateur" 		value="#idOperateur#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcontact_revendeur" value="#idContactRevendeur#" 	null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idtransporteur" 		value="#idTransporteur#" 		null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_livraison" 	value="#idSiteDeLivraison#">	
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 			   variable="p_date_effet_prevue" 	value="#dateLivraison#" >			
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
						<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idgroupe_repere" 	value="#idGroupeRepere#">
						<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	  null="false" variable="p_article" 			value="#tostring(articles)#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_profil_commande" 	value="#idProfileEquipement#">
						<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperation">
					</cfstoredproc>
					<cfset checkOK =  p_idoperation>
					<cfset ArrayAppend(idsCommande, checkOK)>
				</cfif>
			</cfloop>
		<cfreturn idsCommande>  
	</cffunction>

<!--- PARTIE GESTION DES LOGGINS RAJOUT POUR LA COMMANDE SNCF --->

<!--- OK --->
	<cffunction name="createUtilisateur" access="public" returntype="numeric" output="false">
		<cfargument name="nom" 				required="true" type="string"/>
		<cfargument name="prenom" 			required="true" type="string"/>
		<cfargument name="adresse" 			required="true" type="string"/>
		<cfargument name="codepostal" 		required="true" type="string"/>
		<cfargument name="ville" 			required="true" type="string"/>
		<cfargument name="telephone" 		required="true" type="string"/>
		<cfargument name="direction" 		required="true" type="string"/>
		<cfargument name="email" 			required="true" type="string"/>
		<cfargument name="password" 		required="true" type="string"/>
		<cfargument name="restrictionIP"	required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.add_app_login_sncf">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_NOM" 		value="#nom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_PRENOM" 	value="#prenom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_adresse" 			value="#adresse#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_codepostal" 		value="#codepostal#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ville" 			value="#ville#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_telephone" 		value="#telephone#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_direction" 		value="#direction#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_EMAIL" 		value="#email#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_PWD" 		value="#password#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_RESTRICTION_IP" 	value="#restrictionIP#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_IDRACINE" 		value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
						
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.addDefaultProfil">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#p_retour#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>

<!--- OK --->
	<cffunction name="updateUtilisateur" access="public" returntype="numeric" output="false">
		<cfargument name="appLogin"			required="true" type="numeric"/>
		<cfargument name="nom" 				required="true" type="string"/>
		<cfargument name="prenom" 			required="true" type="string"/>
		<cfargument name="adresse" 			required="true" type="string"/>
		<cfargument name="codepostal" 		required="true" type="string"/>
		<cfargument name="ville" 			required="true" type="string"/>
		<cfargument name="telephone" 		required="true" type="string"/>
		<cfargument name="direction" 		required="true" type="string"/>
		<cfargument name="email" 			required="true" type="string"/>
		<cfargument name="password" 		required="true" type="string"/>
		<cfargument name="restrictionIP"	required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_SNCF.upd_app_login_sncf">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_APP_LOGINID" 		value="#appLogin#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_NOM" 		value="#nom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_PRENOM" 	value="#prenom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_adresse" 			value="#adresse#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_codepostal" 		value="#codepostal#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ville" 			value="#ville#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_telephone" 		value="#telephone#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_direction" 		value="#direction#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_EMAIL" 		value="#email#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_PWD" 		value="#password#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_RESTRICTION_IP" 	value="#restrictionIP#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

<!--- PAS UTILISEE --->
	<cffunction name="fournirListePools" access="remote" returntype="Query" description="liste profile">
			<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.fournirListeProfiles">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
		<cfreturn p_result>
	</cffunction>

<!--- OK --->
	<cffunction name="fournirListeProfiles" access="remote" returntype="Query" description="liste profile">
		<cfargument name="idracine" 			required="true" type="numeric"/>
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>	
			<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_SNCF.List_profil_gestionnaire_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#idGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine"  		value="#idracine#" 		null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="fournirListeProfilesGeneric" access="remote" returntype="Query" description="liste profile">
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>	
			<cfstoredproc datasource="#session.OFFREDSN#" procedure="PKG_CV_SNCF.List_profil_gest_acine">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#idGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
		<cfreturn p_result>
	</cffunction>

<!--- OK --->
	<cffunction name="addProfilInGestionnaire" access="remote" returntype="numeric">
		<cfargument name="idGestionnaire" 	type="Numeric" required="true">
		<cfargument name="idProfile" 		type="Array"   required="true">
            <cfloop index="i" from="1" to=#ArrayLen(idProfile)#>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_SNCF.add_profil_gestionnaire">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#IdGestionnaire#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#val(idProfile[i].IDPROFIL_EQUIPEMENT)#">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
				</cfstoredproc>
				<cfif p_retour EQ -1>
	            	<cfreturn -1>
	            </cfif>
            </cfloop>
		<cfreturn p_retour>
	</cffunction>

<!--- OK --->	
	<cffunction name="eraseProfilInGestionnaire" access="remote" returntype="numeric">
		<cfargument name="IdGestionnaire" 	type="Numeric" required="true">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_SNCF.del_Allprofil_gestionnaire">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdGestionnaire#" 	null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

<!--- PARTIE GESTION DES LOGGINS RAJOUT POUR LA COMMANDE SNCF --->

<!--- PARTIE RAJOUT COMMANDE MOBILE : RENOUVELLEMENT, MODIFICATION, RESILIATION --->

	<cffunction name="fournirListeLignesByOperateur" access="public" returntype="Query" output="false">
		<cfargument name="idRacine" 		required="true" type="Numeric"/>
		<cfargument name="idPool" 			required="true" type="Numeric"/>
		<cfargument name="idOperateur" 		required="true" type="Numeric"/>
		<cfargument name="clef" 			required="true" type="String"/>
		<cfargument name="eligibilite" 		required="true" type="Numeric"/>
		<cfargument name="idrecherche" 		required="true" type="Numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_equipement.getlignesbyoperateur">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine" 		value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" 		value="#idPool#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur" 	value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" 	variable="p_clef" 			value="#clef#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_eligibilite" 	value="#eligibilite#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idrecherche" 	value="#idrecherche#">				
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="fournirLignesProduitAbonnement" access="public" returntype="Struct" output="false">
		<cfargument name="idRacine"  required="true" type="Numeric"/>
		<cfargument name="sousTetes" required="true" type="String"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_flotte.get_lignes_produit_abo">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine" 	value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" 		variable="p_sous_tetes" value="#sousTetes#">
				<cfprocresult name="p_retour" 	resultset="1">
				<cfprocresult name="p_retour_2" resultset="2">
			</cfstoredproc>
  			<cfset retour = structNew()>
 			<cfset retour.LIGNESANDOPTIONS = p_retour>
			<cfset retour.NUMBERLIGNES = p_retour_2>
		<cfreturn retour>
	</cffunction>

	<cffunction name="faireActionWorkFlow" access="public" returntype="numeric" output="false" hint="Fait une action de workflow pour chaque arcticle de la commande." >
		<cfargument name="idAction" 			required="true" type="numeric" 	default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="dateAction" 			required="true" type="date" 	default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="commentaireAction" 	required="true" type="string" 	default="" displayname="struct commande" 	hint="Initial value for the commandeproperty." />
		<cfargument name="idsCommande" 			required="true" type="Array">

			<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
			<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
			<cfset checkOK = 1>
			
			<cfloop index="idx" from="1" to="#ArrayLen(idsCommande)#">
				<cfset idCommande = idsCommande[idx]>
				<cfif checkOK GTE 1>
					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_WORKFLOW.FAIREACTIONDEWORKFLOWMOBILE" blockfactor="1" >
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idRacine#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_action" value="#LSDATEFORMAT(dateAction,'YYYY/MM/DD')#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_commentaire" value="#commentaireAction#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#idGestionnaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#idCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idaction" value="#idAction#">
						<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
					</cfstoredproc>
					<cfset checkOK =  p_retour>
				</cfif>
			</cfloop>
		<cfreturn checkOK>
	</cffunction>

	<cffunction name="eraseFileJoined" access="remote" returntype="numeric">
		<cfargument name="path" 	required="true" type="string"/>
			
			<cfset fileErased = -1>
			<cftry>	
				<cfset ImportTmp="/webroot/pieces_jointes/">
				<cffile action="delete" file="#ImportTmp##path#">
				<cfset fileErased = 1>
			<cfcatch>
				<cfset fileErased = 0>
			</cfcatch>
			</cftry>
	</cffunction>

	<cffunction name="envoyer" access="public" returntype="numeric">
		<cfargument name="mail" 			type="fr.consotel.consoview.util.Mail" 	required="true">
		<cfargument name="idsCommande" 		type="Array" 							required="true">
		<cfargument name="idtypecommande" 	type="Numeric" 							required="true">
		<cfargument name="pathJoin" 		type="String" 							required="false">

		
			<cfset p_retour = -1>
			<cfloop index="idx" from="1" to="#ArrayLen(idsCommande)#">
				<cfset idCommande = idsCommande[idx]>
				<cfset str1 = 'SIZE="10"'>
				<cfset str2 = 'SIZE="2"'>		
				<cfset message1 = #Replace(mail.getMessage(),str1,str2,"all")#>
		
				<cfset str1 = "&apos;">
				<cfset str2 = "'">
				<cfset message = #Replace(message1,str1,str2,"all")#>
		
				<cfset listeMailEmp = "">
				<cfset cc = mail.getCc()>
				
				<cfif mail.getCopiePourExpediteur() eq "YES">
					<cfif len(mail.getCc()) eq 0>
						<cfset cc = mail.getExpediteur()>
					<cfelse>
						<cfset cc = mail.getCc() & "," & mail.getExpediteur()>
					</cfif>
				</cfif>
				
				
				<cfobject name="sr" type="component" component="fr.consotel.consoview.util.publicreportservice.ScheduleRequest">
				
				
				<cfswitch expression="#idtypecommande#">
					 <cfcase value="1083"><!--- NOUVELLE COMMANDE --->
					 	<cfset sr.setReportRequest("/consoview/gestion/flotte/BDCmobile/BDCmobile.xdo","BDC1","pdf")>
					 </cfcase>
					 <!---<cfcase value="1282"> MODIFICATION(S) D'OPTION(S)  
					 	<cfset sr.setReportRequest("/consoview/gestion/flotte/BDCmobileCmdOptionLigne/BDCmobileCmdOptionLigneV2.xdo","BDC1","pdf")>
					 </cfcase>
					 <cfcase value="1283">  RESILIATION  
					 	<cfset sr.setReportRequest("/consoview/gestion/flotte/BDCmobileCmdOptionLigne/BDCmobileCmdOptionLigneV2.xdo","BDC1","pdf")>
					 </cfcase>
					 <cfcase value="1382"> RENOUVELLEMENT  
					 	<cfset sr.setReportRequest("/consoview/gestion/flotte/BDCmobileCmdOptionLigne/BDCmobileCmdOptionLigneV2.xdo","BDC1","pdf")>
					 </cfcase>--->
					 <cfdefaultcase>
					  	<cfset sr.setReportRequest("/consoview/gestion/flotte/BDCmobile/BDCmobile.xdo","BDC1","pdf")>
					 </cfdefaultcase>
				</cfswitch>
				
<!--- 				<cfset sr.setReportRequest("/consoview/gestion/flotte/BDCmobile/BDCmobile.xdo","BDC1","pdf")>
 --->				
				<cfset sr.AddParameter("P_IDCOMMANDE","#idCommande#")>
				<cfset sr.AddParameter("P_IDGESTIONNAIRE","#Session.user.CLIENTACCESSID#")>
				<cfset sr.AddParameter("P_RACINE","#Session.perimetre.RAISON_SOCIALE#")>
				<cfset sr.AddParameter("P_GESTIONNAIRE","#Session.user.NOM# #Session.user.PRENOM#")>
				<cfset sr.AddParameter("P_MAIL","#Session.user.EMAIL#")>
				
				<cfset sr.setScheduleRequest("consoview","public","#createUUID()#")>
				<cfset sr.setNotificationScheduleRequest("IT@consotel.fr",true,false,false)>
				<cfset sr.setEmailDeliveryRequest("#mail.getDestinataire()#","#cc#","#mail.getBcc()#","#mail.getDestinataire()#","#mail.getExpediteur()#",
				                                                     "#message#","#mail.getModule()# : #mail.getSujet()#")>
				                                                     
				<cfset sr.setBurstScheduleRequest(false)>
				<cfset sr.scheduleReport()>
			</cfloop>
		<cfreturn idx>
	 </cffunction>

	<cffunction name="readExcelSheet" access="public" output="false" returntype="query">
	      <cfargument name="fileName" required="true" type="string" />
	      <cfargument name="sheetName" required="true" type="string" />
	
		<cfscript>
	            // Declaration of variables
	            var objInst = '';
	            var stmnt = '';
	            var sheetData = '';
	            var sql = "Select * from [#sheetName#$]";
	            var qryData = QueryNew('');
	 
	            // Processing the excel sheet to read the data
	            if(len(trim(arguments.fileName)) and fileExists(arguments.fileName))
	            {
	                  CreateObject("java", 
	                    "java.lang.Class").forName("sun.jdbc.odbc.JdbcOdbcDriver");
	                  objInst = CreateObject("java", 
	                    "java.sql.DriverManager").getConnection(
	     "jdbc:odbc:Driver={Microsoft Excel Driver (*.xls)};DBQ=" & arguments.fileName);
	                  stmnt = objInst.createStatement();
	                  sheetData = stmnt.executeQuery(sql);
	                  qryData = CreateObject('java', 
	                    'coldfusion.sql.QueryTable').init(sheetData);
	                  objInst.Close();
	            }
	            return qryData;
		</cfscript>
	</cffunction>

<!--- PARTIE RAJOUT COMMANDE MOBILE : RESILIATION --->

</cfcomponent>