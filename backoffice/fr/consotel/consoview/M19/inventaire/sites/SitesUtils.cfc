<!--- =========================================================================
Classe: SitesUtils
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="SitesUtils" hint="" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="SitesUtils" hint="Remplace le constructeur de SitesUtils.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->


	<!--- AFFICHER LES INFOS DU SITE --->
	<cffunction name="getInfosSite" access="remote" returntype="query" output="false">
		<cfargument name="idSite" required="true" type="numeric" />
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.GETSITEINFO">
				<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_inTEGER" type="in">
				<cfprocresult name="qGetInfosSite"/>
			</cfstoredproc>
		<cfreturn qGetInfosSite/>
	</cffunction>

	<!--- ENREGISTER LES INFOS DU SITE --->
	<cffunction name="updateInfosSite" access="remote" returntype="any" description="Permet de modifier les infos d un site">
	    <cfargument name="IDsite" type="Numeric" required="true">
	    <cfargument name="Libelle_site" type="string" required="true">
	    <cfargument name="ref_site" type="string" required="true">
	    <cfargument name="code_interne_site" type="string" required="true">
	    <cfargument name="adr_site" type="string" required="true">
	    <cfargument name="cp_site" type="string" required="true">
	    <cfargument name="commune_site" type="string" required="true">
	    <cfargument name="commentaire_site" type="string" required="true">
		<cfargument name="idpays_site" type="numeric" required="true">	
		    <cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updateSite_v2">
			    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDsite#" null="false">
			    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_site#" null="false">
			    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ref_site#" null="false">
			    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne_site#" null="false">
			    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adr_site#" null="false">
			    <cfprocparam type="In" cfsqltype="CF_SQL_vaRCHAR" value="#cp_site#" null="false">
			    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commune_site#" null="false">
			    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_site#" null="false">
			    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idpays_site#" null="false">
			    <cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		    </cfstoredproc>
		<cfreturn p_retour/>
	</cffunction>
	
	
	<!--- LISTE DES SITES ET SITE D'UN EMPLOYE --->
	<cffunction name="fournirListeSitesLivraisonsPoolGestionnaire" access="public" returntype="query" output="false" hint="Fournit la liste des sites d'un pools de gestion d un gestionnaire suivant une clefs de recherche Si l id du gestionnaire est égale é 0. On fournit tous les sites de livraison. clef = NOM_SITE | SP_REFERENCE | SP_CODE_INTERNE | COMMUNE" >
		<cfargument name="idPoolGestion" required="true" type="numeric" />
		<cfargument name="clef" required="false" type="string" default="" displayname="string clef" hint="Initial value for the clefproperty." />
		
		<!--- 
		 pkg_cv_gestionnaire.getsiteofpool(p_idpool => :p_idpool,
                                    p_retour => :p_retour);
		 --->
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.GETSITEOFPOOL" blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" value="#idPoolGestion#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="fournirSiteEmploye" access="remote" returntype="Employe" output="false" hint="">
		<cfargument name="theEmploye" required="true" type="Employe" />
			<!--- <cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE> --->
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_annuaire.getSite">
					<cfprocparam  value="#theEmploye.IDEMPLOYE#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocresult name="p_result"/>
				</cfstoredproc>
				<cfif p_result.recordCount gt 0>
					<cfset theEmploye.setIDSITE_PHYSIQUE(p_result["IDSITE_PHYSIQUE"][1])/>
				</cfif>
		<cfreturn theEmploye/>
	</cffunction>
	
	
	<!--- LISTE DES COLLABORATEURS IN / OUT D'UN SITE --->
	<cffunction name="getListeCollabInOutSite" access="remote" returntype="query" output="false">
		<cfargument name="idSite" required="true" type="numeric" />
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="cleRecherche" required="true" type="string" />
		<cfargument name="boolInSite" required="true" type="numeric" />
		<cfargument name="sizeList" required="true" type="numeric" />
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.L_COLLABORATEURS">
					<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#cleRecherche#" cfsqltype="cf_sql_vARCHAR" type="in">
					<cfprocparam  value="#boolInSite#" cfsqltype="cf_sql_inTEGER" type="in">
					<cfprocresult name="qGetListeCollabInOutSite" maxrows="#sizeList#"/>
				</cfstoredproc>
		<cfreturn qGetListeCollabInOutSite/>
	</cffunction>
	
	<!--- LISTE DES EQUIPEMENTS IN / OUT D'UN SITE --->
	<cffunction name="getListeTerminalInOutSite" access="remote" returntype="query" output="false">
		<cfargument name="idSite" required="true" type="numeric" />
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="idType" required="true" type="numeric" />
		<cfargument name="segment" required="true" type="numeric" />
		<cfargument name="cleRecherche" required="true" type="string" />
		<cfargument name="boolInSite" required="true" type="numeric" />
		<cfargument name="sizeList" required="true" type="numeric" />
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.L_EQUIPEMENTS">
					<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#idType#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#segment#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#cleRecherche#" cfsqltype="cf_sql_vARCHAR" type="in">
					<cfprocparam  value="#boolInSite#" cfsqltype="cf_sql_inTEGER" type="in">
					<cfprocresult name="qGetListeTerminauxInOutSite" maxrows="#sizeList#"/>
				</cfstoredproc>
		<cfreturn qGetListeTerminauxInOutSite/>
	</cffunction>
	
	<!--- LISTE DES LIGNES IN / OUT D'UN SITE --->
	<cffunction name="getListeLigneInOutSite" access="remote" returntype="query" output="false">
		<cfargument name="idSite" required="true" type="numeric" />
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="segment" required="true" type="numeric" />
		<cfargument name="cleRecherche" required="true" type="string" />
		<cfargument name="boolInSite" required="true" type="numeric" />
		<cfargument name="sizeList" required="true" type="numeric" />
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.L_SOUS_TETE">
					<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#segment#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#cleRecherche#" cfsqltype="cf_sql_vARCHAR" type="in">
					<cfprocparam  value="#boolInSite#" cfsqltype="cf_sql_inTEGER" type="in">
					<cfprocresult name="qGetListeLignesInOutSite" maxrows="#sizeList#"/>
				</cfstoredproc>
		<cfreturn qGetListeLignesInOutSite/>
	</cffunction>
	
	<!--- LISTE DES SIM IN / OUT D'UN SITE --->
	<cffunction name="getListeSimInOutSite" access="remote" returntype="query" output="false">
		<cfargument name="idSite" required="true" type="numeric" />
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="cleRecherche" required="true" type="string" />
		<cfargument name="boolInSite" required="true" type="numeric" />
		<cfargument name="sizeList" required="true" type="numeric" />
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.L_SIMS">
					<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#cleRecherche#" cfsqltype="cf_sql_vARCHAR" type="in">
					<cfprocparam  value="#boolInSite#" cfsqltype="cf_sql_inTEGER" type="in">
					<cfprocresult name="qGetListeSimInOutSite" maxrows="#sizeList#"/>
				</cfstoredproc>
		<cfreturn qGetListeSimInOutSite/>
	</cffunction>
	
	<!--- LISTE DES INCIDENTS SUR LES EQUIPEMENTS D'UN SITE --->
	<cffunction name="getListeIncidentSite" access="remote" returntype="query" output="false">
		<cfargument name="idSite" required="true" type="numeric" />
		<cfargument name="idPool" required="true" type="numeric" />
		<cfargument name="cleRecherche" required="true" type="string" />
		<cfargument name="nbMois" required="true" type="numeric" />
		<cfargument name="sizeList" required="true" type="numeric" />
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.L_INCIDENTS">
					<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#cleRecherche#" cfsqltype="cf_sql_vARCHAR" type="in">
					<cfprocparam  value="#nbMois#" cfsqltype="cf_sql_inTEGER" type="in">
					<cfprocresult name="qGetListeIncidentSite" maxrows="#sizeList#"/>
				</cfstoredproc>
		<cfreturn qGetListeIncidentSite/>
	</cffunction>
	
	
	<!--- AFFECTER UN COLLABORATEUR A UN SITE --->
	<cffunction name="affectCollabToSite" access="remote" returntype="numeric" output="false">
		<cfargument name="tabIdCollab" required="true" type="array" />
		<cfargument name="idSite" required="true" type="numeric" />
				<cfset compt=0>
				<cfloop index="i" from="1" to="#arraylen(tabIdCollab)#" >
					
					<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.AF_COLLABORATEUR">
						<cfprocparam  value="#tabIdCollab[i].IDGROUPE_CLIENT_EMP#" cfsqltype="CF_SQL_NUMERIC" type="in">
						<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_NUMERIC" type="in">
						<cfprocparam  cfsqltype="cf_sql_inTEGER" variable="pAffectCollabToSite" type="out">
					</cfstoredproc>
				
					<cfif pAffectCollabToSite gt 0>
						<cfset compt = compt + 1>	
					</cfif>
					
				</cfloop>
		<cfif compt neq arraylen(tabIdCollab)>
			<cfreturn 'ERROR'>
		<cfelse>
			<cfreturn compt/>
		</cfif>
	</cffunction>
	
	<!--- DESAFFECTER UN COLLABORATEUR D' UN SITE --->
	<cffunction name="desaffectCollabToSite" access="remote" returntype="numeric" output="false">
		<cfargument name="idCollab" required="true" type="numeric" />
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.DAF_COLLABORATEUR">
					<cfprocparam  value="#idCollab#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  cfsqltype="cf_sql_inTEGER" variable="pDesaffectCollabToSite" type="out">
				</cfstoredproc>
		<cfreturn pDesaffectCollabToSite/>
	</cffunction>
	
	<!--- AFFECTER UN EQUIPEMENT A UN SITE --->
	<cffunction name="affectTerminalToSite" access="remote" returntype="numeric" output="false">
		<cfargument name="tabIdEqpt" required="true" type="array" />
		<cfargument name="idSite" required="true" type="numeric" />
			<cfset compt=0>
			<cfloop index="i" from="1" to="#arraylen(tabIdEqpt)#" >
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.AF_EQPT">
					<cfprocparam  value="#tabIdEqpt[i].IDEQUIPEMENT#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  cfsqltype="cf_sql_inTEGER" variable="pAffectTerminalToSite" type="out">
				</cfstoredproc>
				
				<cfif pAffectTerminalToSite gt 0>
					<cfset compt = compt + 1>	
				</cfif>
			</cfloop>	
		<cfif compt neq arraylen(tabIdEqpt)>
			<cfreturn 'ERROR'>
		<cfelse>
			<cfreturn compt/>
		</cfif>
	</cffunction>
	
	<!--- DESAFFECTER UN EQUIPEMENT D' UN SITE --->
	<cffunction name="desaffectTerminalToSite" access="remote" returntype="numeric" output="false">
		<cfargument name="idEqpt" required="true" type="numeric" />
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.DAF_EQPT">
					<cfprocparam  value="#idEqpt#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  cfsqltype="cf_sql_inTEGER" variable="pDesaffectTerminalToSite" type="out">
				</cfstoredproc>
		<cfreturn pDesaffectTerminalToSite/>
	</cffunction>
	
	<!--- AFFECTER UNE LIGNE A UN SITE --->
	<cffunction name="affectLigneToSite" access="remote" returntype="numeric" output="false">
		<cfargument name="tabIdSousTete" required="true" type="array" />
		<cfargument name="idSite" required="true" type="numeric" />
			<cfset compt=0>
			<cfloop index="i" from="1" to="#arraylen(tabIdSousTete)#" >
		
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.AF_SOUS_TETE">
					<cfprocparam  value="#tabIdSousTete[i].IDSOUS_TETE#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  cfsqltype="cf_sql_inTEGER" variable="pAffectLigneToSite" type="out">
				</cfstoredproc>
				
				<cfif pAffectLigneToSite gt 0>
					<cfset compt = compt + 1>	
				</cfif>
			</cfloop>	
		<cfif compt neq arraylen(tabIdSousTete)>
			<cfreturn 'ERROR'>
		<cfelse>
			<cfreturn compt/>
		</cfif>
	</cffunction>
	
	<!--- DESAFFECTER UNE LIGNE D' UN SITE --->
	<cffunction name="desaffectLigneToSite" access="remote" returntype="numeric" output="false">
		<cfargument name="idSousTete" required="true" type="numeric" />
		<cfargument name="idSite" required="true" type="numeric" />
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.DAF_SOUS_TETE">
					<cfprocparam  value="#idSousTete#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  cfsqltype="cf_sql_inTEGER" variable="pDesaffectLigneToSite" type="out">
				</cfstoredproc>
		<cfreturn pDesaffectLigneToSite/>
	</cffunction>
	
	<!--- AFFECTER UNE SIM A UN SITE --->
	<cffunction name="affectSimToSite" access="remote" returntype="numeric" output="false">
		<cfargument name="tabIdEqpt" required="true" type="array" />
		<cfargument name="idSite" required="true" type="numeric" />
			<cfset compt=0>
			<cfloop index="i" from="1" to="#arraylen(tabIdEqpt)#" >	
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.AF_EQPT">
					<cfprocparam  value="#tabIdEqpt[i].IDEQUIPEMENT#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  cfsqltype="cf_sql_inTEGER" variable="pAffectSimToSite" type="out">
				</cfstoredproc>
				<cfif pAffectSimToSite gt 0>
					<cfset compt = compt + 1>	
				</cfif>
			</cfloop>	
		<cfif compt neq arraylen(tabIdEqpt)>
			<cfreturn 'ERROR'>
		<cfelse>
			<cfreturn compt/>
		</cfif>
	</cffunction>
	
	<!--- DESAFFECTER UNE SIM D' UN SITE --->
	<cffunction name="desaffectSimToSite" access="remote" returntype="numeric" output="false">
		<cfargument name="idEqpt" required="true" type="numeric" />
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_SITE.DAF_EQPT">
					<cfprocparam  value="#idEqpt#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  cfsqltype="cf_sql_inTEGER" variable="pDesaffectSimToSite" type="out">
				</cfstoredproc>
		<cfreturn pDesaffectSimToSite/>
	</cffunction>
	
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>