<!--- =========================================================================
Classe: GestionWorkFlow
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GestionWorkFlow" hint="" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GestionWorkFlow" hint="Remplace le constructeur de GestionWorkFlow.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	
	
	<!--- PROCEDURE faireActionDeWorkFlowMobile(	p_IDRACINE 			IN INTEGER,
                                          p_DATE_ACTION		IN	VARCHAR2,
                                          p_COMMENTAIRE 		IN	VARCHAR2,
                                          p_IDGESTIONNAIRE	IN	INTEGER,
                                          p_IDOPERATION    	IN INTEGER,
                                          p_idaction			IN INTEGER,
                                          p_retour				OUT INTEGER); --->
	<cffunction name="faireActionWorkFlow" access="public" returntype="numeric" output="false" hint="Fait une action de workflow pour chaque arcticle de la commande." >
		<cfargument name="idAction" required="false" type="numeric" default="" displayname="Struct action" hint="Initial value for the actionproperty." />
		<cfargument name="dateAction" required="false" type="date" default="" displayname="Struct action" hint="Initial value for the actionproperty." />
		<cfargument name="commentaireAction" required="false" type="string" default="" displayname="struct commande" hint="Initial value for the commandeproperty." />
		<cfargument name="idOperation" required="false" type="numeric" default="" displayname="Struct action" hint="Initial value for the actionproperty." />
		
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_WORKFLOW.FAIREACTIONDEWORKFLOWMOBILE" blockfactor="1" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idRacine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_action" value="#LSDATEFORMAT(dateAction,'YYYY/MM/DD')#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_commentaire" value="#commentaireAction#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#idGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#idOperation#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idaction" value="#idAction#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		PROCEDURE HistoriqueEtatsOperation(  p_idCommande IN INTEGER,p_retour OUT sys_refcursor);
	 --->
	<cffunction name="fournirHistoriqueEtatsCommande" access="public" returntype="query" output="false" hint="Fournit l'historique des etats d'une commande." >
		<cfargument name="idCommande" required="true" type="numeric" displayname="ID DE LA COMMANDE"/>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_WORKFLOW.HISTORIQUEETATSOPERATION" blockfactor="100" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idCommande" value="#idCommande#">			
			<cfprocresult name="historique">
		</cfstoredproc>
		<cfreturn historique>
	</cffunction>
	
	<!--- 
		Fournit la liste les actions possibles pour un gestionnaire, é partir d'un  état de commande. (Detailler chaque action) 
		si IDETAT = 0 et IDGESTIONNAIRE = 0 on fournit la liste de toutes les actions.
		si IDETAT &gt; et IDGESTIONNAIRE = 0 on fournit la liste de toutes les actions possibles é partir de l'état.
		si IDETAT = 0 et IDGESTIONNAIRE = &gt; on fournit la liste de toutes les actions possibles pour le gestionnaire.
		si IDETAT = &gt; et IDGESTIONNAIRE = &gt; on fournit la liste de toutes les actions possibles é partir de l'état et pour un gestionnaire.
		
		PROCEDURE ListeActionsPossibles(p_IDETAT IN	INTEGER,
      										p_IDETAT IN INTEGER,
                                    p_retour			   IN INTEGER,
                                    p_retour OUT sys_refcursor);                                     

	 --->
	<cffunction name="fournirListeActionsPossibles" access="public" returntype="query" output="false" hint="" >		
		<cfargument name="idEtat" required="false" type="numeric" default="" displayname="struct etat" hint="Initial value for the etatproperty." />		
		<cfargument name="idPool" required="false" type="numeric" default="" displayname="struct etat" hint="Initial value for the etatproperty." />
	 
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_WORKFLOW.LISTEACTIONSPOSSIBLES" blockfactor="100" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idetat" value="#idEtat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#Session.USER.CLIENTACCESSID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idPool" value="#idPool#">			
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getContactsActSuivante" access="public" returntype="query" output="false" hint="" >		
		<cfargument name="idPool" required="false" type="numeric" />
		<cfargument name="idAction" required="false" type="numeric" />
			 
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_WORKFLOW.GETCONTACTSACTSUIVANTE" blockfactor="100">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idPool" value="#idPool#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idAction" value="#idAction#">			
			<cfprocresult name="result">
		</cfstoredproc>
		
		<cfreturn result>
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>