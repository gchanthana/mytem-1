<cfcomponent name="gestionDestinataire">

	<cffunction name="getPoolGestionnaire" access="remote" returntype="any" description="Retourne la liste des pools de gestionnaires dun groupe Client">
		<cfargument name="idGroupe" type="numeric" required="true">
		<cfargument name="Search_texte" type="String" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getPoolGestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Search_texte#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getPool_of_Gestionnaire" access="remote" returntype="any" description="Retourne la liste des pools de gestionnaires dun gestionnaire">
		<cfargument name="idLogin" type="numeric" required="true">
		<cfargument name="idGroupe" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getPool_of_gestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idLogin#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
	<cfreturn p_result>
	</cffunction>
	
	<cffunction name="fournirPoolsDuGestionnaire" access="remote" returntype="any" description="Retourne la liste des pools de gestionnaires dun gestionnaire">
		<cfargument name="idLogin" type="numeric" required="false">
		<cfargument name="idGroupe" type="numeric" required="false">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getPool_of_gestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
	<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="getGestionnaire" access="remote" returntype="any" description="Retourne tous les gestionnaires dun groupe Client">
		<cfargument name="IDGroupe_client" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getGestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getAllAction" access="remote" returntype="any" description="Retourne tous les gestionnaires dun groupe Client">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getAllAction">
			
			<cfprocresult name="p_result">
		</cfstoredproc>
		<!--- On ne retiens que les actions de la flotte mobile --->
		<cfquery name="qActionGestionMobile" dbtype="query">
			select * from p_result where idinv_actions > 2000
		</cfquery>
		<cfreturn qActionGestionMobile>
	</cffunction>
	
	
	<cffunction name="getAllActionByWF" access="remote" returntype="any" description="La liste des actions selon le wf">
		<cfargument name="typeWF" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getAllAction">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		
		<cfswitch expression="#UCASE(typeWF)#">
			<cfcase value="CYV">
				<cfset condition = "idinv_actions < 2000">
			</cfcase>
			<cfcase value="CMOB">
				<cfset condition = "idinv_actions > 2000">
			</cfcase>
			<cfdefaultcase>
				<cfset condition = "1 = 1">
			</cfdefaultcase>
		</cfswitch>
			
		<!--- On ne retiens que les actions de la flotte mobile --->
		<cfquery name="qActions" dbtype="query">
			SELECT * FROM p_result WHERE #PreserveSingleQuotes(condition)#
			ORDER BY IDINV_ACTIONS asc
		</cfquery>
		
		<cfreturn qActions>
	</cffunction>
	
	<cffunction name="getPoolRevendeur" access="remote" returntype="any" description="Retourne les revendeurs du client">
		<cfargument name="IDGroupe_client" type="numeric" required="true">
		<cfargument name="search" type="string" required="true">
		<cfargument name="segment1" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.Get_all_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vaRCHAR" value="#search#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#segment1#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getlisteRevendeursLibres" access="remote" returntype="query" description="Retourne les revendeurs du client qui ne sont pas rattaaché a un pool">		
		<cfargument name="boolSegmentFixe" type="numeric" required="true">
		<cfargument name="boolSegmentMobile" type="numeric" required="true">
		
		<cfset IDRACINE = SESSION.PERIMETRE.ID_GROUPE>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTION_FOURNISSEUR_V1.getRevNotInPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDRACINE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vaRCHAR" value="#boolSegmentFixe#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#boolSegmentMobile#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getAbonnement" access="remote" returntype="any" description="Retourne tous les gestionnaires dun groupe Client">
		<cfargument name="IDGroupe_client" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getAbonnement">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getGestionnaire_pool" access="remote" returntype="any" description="Retourne la liste des Gestionnaires dun pool">
		<cfargument name="IDPool" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getGestionnaire_pool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDPool#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>


	<cffunction name="getSite" access="remote" returntype="any" description="Retourne la liste des sites d un groupe client">
		<cfargument name="IDGroupe_client" type="numeric" required="true">
		<cfargument name="Search_texte" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getSite">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Search_texte#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getSite_pool" access="remote" returntype="any" description="Retourne la liste des sites d un POOL">
		<cfargument name="IDPool" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getsiteofpool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDPool#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getListeLignesSansPool" access="remote" returntype="any" description="ligne sans pool">
		<cfargument name="Idracine" type="numeric" required="true">
		<cfargument name="segment1" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getListeLignesSansPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment1#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getListeLignesNotInPool" access="remote" returntype="any" description="ligne sans pool filtre sur segment">
		<cfargument name="Idracine" type="numeric" required="true">
		<cfargument name="idPoolRef" type="string" required="true">
		<cfargument name="segment1" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getListeLignesNotInPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idPoolRef#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#segment1#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getListeLignePool" access="remote" returntype="any" description="ligne du pool">
		<cfargument name="idPool" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getListeLignePool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getListeEquipementsPool" access="remote" returntype="any" description="equipement du pool">
		<cfargument name="idPool" type="numeric" required="true">
		<cfargument name="segment1" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getListeEquipementsPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment1#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getEquipementSansPool" access="remote" returntype="any" description="equipement sans pool">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfargument name="segment1" type="numeric" required="true">
		<cfargument name="idPool" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getEquipementSansPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment1#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getEquipementNotInPool" access="remote" returntype="any" description="equipement pas dans ce pool">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfargument name="idPool" type="numeric" required="true">
		<cfargument name="segment1" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getEquipementNotInPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment1#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="fournirListeProfiles" access="remote" returntype="any" description="liste profile">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.fournirListeProfiles">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="fournirListeActProfil" access="remote" returntype="any" description="liste actions par profile">
		<cfargument name="idProfil" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.fournirListeActProfil">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>


	
	<cffunction name="getListeEmployesSansPool" access="remote" returntype="any" description="employe sans pool">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getListeEmployesSansPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getListeEmployesNotInPool" access="remote" returntype="any" description="employe pas dans ce pool">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfargument name="idPool" type="numeric" required="true">
		<cfargument name="segment1" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getListeEmployesNotInPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment1#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getListeEmployesPool" access="remote" returntype="any" description="liste employe par pool">
		<cfargument name="idPool" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getListeEmployesPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	


	<cffunction name="updatePoolGestionnaire" access="remote" returntype="any" description="Permet de modifier les infos Dun Pool de gestionnaire">
		<cfargument name="IDPool" type="Numeric" required="true">
		<cfargument name="Libelle_pool" type="string" required="true">
		<cfargument name="codeInterne_pool" type="string" required="true">
		<cfargument name="commentaire_pool" type="string" required="true">
		<cfargument name="user" type="Numeric" required="true">
		<cfargument name="idRevendeur" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updatePoolGestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_pool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne_pool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_pool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#user#" null="false">
			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#" null="#iif((idRevendeur eq 0), de("yes"), de("no"))#">		
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="updateSite" access="remote" returntype="any" description="Permet de modifier les infos d un site">
		<cfargument name="IDsite" type="Numeric" required="true">
		<cfargument name="Libelle_site" type="string" required="true">
		<cfargument name="ref_site" type="string" required="true">
		<cfargument name="code_interne_site" type="string" required="true">
		<cfargument name="adr_site" type="string" required="true">
		<cfargument name="cp_site" type="string" required="true">
		<cfargument name="commune_site" type="string" required="true">
		<cfargument name="commentaire_site" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updateSite">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDsite#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ref_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adr_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vaRCHAR" value="#cp_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commune_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_site#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	
	<cffunction name="setProfileGestionnaire" access="remote" returntype="any" description="Definit le profile du gestionnaire">
		<cfargument name="idGroupeAppLogin" type="Numeric" required="true">
		<cfargument name="idGroupeClient" type="Numeric" required="true">
		<cfargument name="idPoolGestion" type="Numeric" required="true">
		<cfargument name="idProfileAction" type="Numeric" required="true">
		<cfargument name="accede_collaborateur" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.setProfileGestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupeAppLogin#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupeClient#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPoolGestion#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfileAction#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#accede_collaborateur#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="addPoolGestionnaire" access="remote" returntype="any" description="Creer un nouveau Pool de gestionnaire">
		<cfargument name="Libelle_pool" type="string" required="true">
		<cfargument name="codeInterne_pool" type="string" required="true">
		<cfargument name="commentaire_pool" type="string" required="true">
		<cfargument name="idUser" type="numeric" required="true">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfargument name="idRevendeur" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.addPoolGestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_pool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne_pool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_pool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#SESSION.USER.CLIENTACCESSID#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idRacine#" null="false">
			<cfif isDefined("idRevendeur")>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idRevendeur#">
			<cfelse>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idRevendeur#" null="true">
			</cfif>
			
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="createGestionnaire" access="remote" returntype="any" description="Tag un abonnement comme etant un gestionnaire">
		<cfargument name="idGroupeAppLogin" type="numeric" required="true">
		<cfargument name="idGroupeClient" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.createGestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idGroupeAppLogin#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idGroupeClient#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	

	<cffunction name="addSite" access="remote" returntype="any" description="Permet de creer un site">
		<cfargument name="IDgroupe_client" type="Numeric" required="true">
		<cfargument name="Libelle_site" type="string" required="true">
		<cfargument name="ref_site" type="string" required="true">
		<cfargument name="code_interne_site" type="string" required="true">
		<cfargument name="adr_site" type="string" required="true">
		<cfargument name="cp_site" type="string" required="true">
		<cfargument name="commune_site" type="string" required="true">
		<cfargument name="commentaire_site" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.addSite">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDgroupe_client#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ref_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adr_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#cp_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commune_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_site#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>	
	
	<cffunction name="addLignesToPool" access="remote" returntype="any" description="Ajoute une liste de lignes a un pool (10 max dun coup vÃ©rifier dans consoview)">
		<cfargument name="Idpool" type="Numeric" required="true">
		<cfargument name="listeSousTete" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.addLignesToPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idpool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeSousTete#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="addEquipementToPool" access="remote" returntype="any" description="Ajoute une liste dequipement a un pool ">
		<cfargument name="Idpool" type="Numeric" required="true">
		<cfargument name="listeEquipement" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.addEquipementsToPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idpool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeEquipement#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="add_gestionnaire_in_pool" access="remote" returntype="any" description="Ajoute un gestionnaire dans un pool">
		<cfargument name="Idpool" type="Numeric" required="true">
		<cfargument name="IdGestionnaire" type="Numeric" required="true">
		<cfargument name="IdGroupe" type="Numeric" required="true">
		<cfargument name="accesCollab" type="Numeric" required="true">
		<cfargument name="idProfile" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.add_gestionnaire_in_pool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idpool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdGestionnaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdGroupe#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#accesCollab#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfile#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	
	<!--- 
		Ajoute un gestionnaire dans un pool et gère les droits
		accesCollab = voit il tous les collaborateurs 
		
	 --->
	<cffunction name="addGestionnaireInPoolCycleDeVie" access="remote" returntype="any" description="Ajoute un gestionnaire dans un pool">
		
		<cfargument name="Idpool" type="Numeric" required="true">
		<cfargument name="IdGestionnaire" type="Numeric" required="true">
		<cfargument name="IdRacine" type="Numeric" required="true">
		<cfargument name="accesCollab" type="Numeric" required="true">
		<cfargument name="idProfile" type="Numeric" required="true">
		<cfargument name="gest_cat_cl" type="Numeric" required="true">
		<cfargument name="gest_cat_rev" type="Numeric" required="true">
		<cfargument name="gest_bt_budget" type="Numeric" required="true">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.add_gestionnaire_in_pool_v3">
			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idpool#" null="false">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdGestionnaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdRacine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#accesCollab#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfile#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#gest_cat_cl#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#gest_cat_rev#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#gest_bt_budget#" null="false">
			
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
		
	</cffunction>
	
	<cffunction name="affecte_site_pool" access="remote" returntype="any" description="Ajoute un site dans un pool">
		<cfargument name="IDsite" type="Numeric" required="true">
		<cfargument name="Idpool" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.affecte_site_pool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDsite#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idpool#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="addEmployesToPool" access="remote" returntype="any" description="Ajoute d une liste d employe dans un pool">
		<cfargument name="Idpool" type="Numeric" required="true">
		<cfargument name="listeEmp" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.addEmployesToPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idpool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vaRCHAR" value="#listeEmp#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="affect_profile_gestionnaire" access="remote" returntype="any" description="Ajoute un profil a un gestionnaire">
		<cfargument name="idLogin" type="Numeric" required="true">
		<cfargument name="idProfil" type="Numeric" required="true">
		<cfargument name="Idpool" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.affect_profile_gestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idLogin#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idpool#" null="false">		
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="saveProfile" access="remote" returntype="any" description="crÃ©er un profil">
		<cfargument name="idRacine" type="Numeric" required="true">
		<cfargument name="id" type="Numeric" required="true">
		<cfargument name="libelle" type="string" required="true">
		<cfargument name="commentaire" type="string" required="true">
		<cfargument name="listeAction" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.saveProfile">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#commentaire#" null="false">		
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#listeAction#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="removeSite" access="remote" returntype="any" description="Permet de supprimer lun site">
		<cfargument name="IDsite" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.remove_Site">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDsite#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="deleteGestionnaire" access="remote" returntype="any" description="tag un abonnement comme non gestionnaire">
		<cfargument name="idGroupeAppLogin" type="Numeric" required="true">
		<cfargument name="idGroupeClient" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.deleteGestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupeAppLogin#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupeClient#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="removeListeLignesFromePool" access="remote" returntype="any" description="Supprime une liste de lignes dun pool">
		<cfargument name="idPool" type="Numeric" required="true">
		<cfargument name="listeSousTete" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.removelistelignesfrompool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#listeSousTete#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="removeEquipementsFromPool" access="remote" returntype="any" description="Supprime une liste d equipement dun pool">
		<cfargument name="idPool" type="Numeric" required="true">
		<cfargument name="listeEquipement" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.removeEquipementsFromPool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#listeEquipement#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="remove_gestionnaire_in_pool" access="remote" returntype="any" description="Supprime un gestionnaire dun pool">
		<cfargument name="idPool" type="Numeric" required="true">
		<cfargument name="listeGestionnaire" type="Numeric" required="true">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.remove_gestionnaire_in_pool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#listeGestionnaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="remove_pool_gestionnaire" access="remote" returntype="any" description="Supprime un pool">
		<cfargument name="idPool" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.deletepoolgestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="remove_pool_gestionnaire_contrainte" access="remote" returntype="any" description="Supprime un pool + toutes les contraintes associées">
		<cfargument name="idPool" type="Numeric" required="true">
		<!---<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.deletepoolgestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>--->
		<cfreturn 1>
	</cffunction>
	
	<cffunction name="desaffecte_site_pool" access="remote" returntype="any" description="Supprime un site d un pool">
		<cfargument name="idSite" type="Numeric" required="true">
		<cfargument name="idPool" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.desaffecte_site_pool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="removeListeEmployesFromePool" access="remote" returntype="any" description="supprime liste employe d un pool">
		<cfargument name="idPool" type="numeric" required="true">
		<cfargument name="listeEmp" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.removelstemployesfrompool">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#listeEmp#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="desaffect_profile_gestionnaire" access="remote" returntype="any" description="supprime un profile d un gestionnaire">
		<cfargument name="idLogin" type="numeric" required="true">
		<cfargument name="idProfile" type="numeric" required="true">
		<cfargument name="idPool" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.desaffect_profile_gestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idLogin#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfile#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
<cffunction name="getTypeCmdDemo" access="remote" returntype="query">
<cfset var myQuery = QueryNew("ID,LIBELLE,CODE","Integer,VarChar,VarChar")>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",1);            
		QuerySetCell(myQuery,"LIBELLE","Bureautique");
		QuerySetCell(myQuery,"CODE","BU"); 
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",2);            
		QuerySetCell(myQuery,"LIBELLE","PM3");
		QuerySetCell(myQuery,"CODE","PM"); 
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",3);            
		QuerySetCell(myQuery,"LIBELLE","MMP");
		QuerySetCell(myQuery,"CODE","MM"); 
	</cfscript>
<cfreturn myQuery>
</cffunction>
<cffunction name="getTypeCmd_poolDemo" access="remote" returntype="query">
	<cfargument name="idLogin" type="numeric" required="true">
<cfset var myQuery = QueryNew("ID,LIBELLE,CODE","Integer,VarChar,VarChar")>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",2);            
		QuerySetCell(myQuery,"LIBELLE","PM3");
		QuerySetCell(myQuery,"CODE","PM"); 
	</cfscript>
<cfreturn myQuery>
</cffunction>
<cffunction name="getContraintesOfPool" access="remote" returntype="query">
<cfargument name="idPool" type="Numeric" required="true">
<cfset var myQuery = QueryNew("ID,LIBELLE,TYPE","Integer,VarChar,VarChar")>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",1);            
		QuerySetCell(myQuery,"LIBELLE","Paris");
		QuerySetCell(myQuery,"TYPE","SITE"); 
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",2);            
		QuerySetCell(myQuery,"LIBELLE","MARSEILLE");
		QuerySetCell(myQuery,"TYPE","SITE"); 
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",3);            
		QuerySetCell(myQuery,"LIBELLE","DUPON Eric");
		QuerySetCell(myQuery,"TYPE","GESTIONNAIRE"); 
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",4);            
		QuerySetCell(myQuery,"LIBELLE","SN0231325");
		QuerySetCell(myQuery,"TYPE","COMMANDE"); 
	</cfscript>
<cfreturn myQuery>
</cffunction>
</cfcomponent>
