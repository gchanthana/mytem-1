<cfcomponent output="false">

    <cffunction name="getCgModule" access="remote" returntype="string"> 
        <cfargument name="key" type="string" required="true">
        
        <cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.getCgModule">
            <cfprocparam type="in" value="#key#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="out" variable="qkey" cfsqltype="CF_SQL_VARCHAR">
        </cfstoredproc>
        
        <cfset SESSION.CURRENT_MODULE = qKey>
        
        <cfreturn qKey>
    </cffunction>
    
    <cffunction name="getConsoviewModule" access="remote" returntype="Query"> 
        <cfargument name="key" type="string" required="true">
        
        <cfstoredproc datasource="ROCOFFRE" procedure="pkg_cv_global.getConsoviewModule_V2">
            <cfprocparam type="in" value="#key#" cfsqltype="CF_SQL_VARCHAR">
            <cfprocresult name="qKey">
        </cfstoredproc>
        
        <cfset SESSION.CURRENT_MODULE = qKey>
        
        <cfreturn qKey>
    </cffunction>

</cfcomponent>
