﻿<cfcomponent hint="Gère les appels de procédure du module M00" output="false">
	<cffunction name="getDateDataCompleted" access="remote" hint="Récupère le xml qui permet la construction du menu M512" returntype="Struct">
		<cfset obj = structNew()>
		
		<cfset obj.DATE = SESSION.DATES.LAST_LOAD_FACTURATION>
		
		<cfset tmpDateString = DateFormat(obj.DATE,'MM/YYYY')>
		   
       <cfquery datasource="BI_TEST" name="qGetIdperiodeMois"> 
              SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS
              FROM E0_AGGREGS
              WHERE      PERIODE.SHORT_MOIS='#tmpDateString#'
              ORDER BY IDPERIODE_MOIS
       </cfquery>
       <cfif qGetIdperiodeMois.recordcount EQ 1>
             <cfset obj.IDPERIODE =  qGetIdperiodeMois["IDPERIODE_MOIS"][1]>
       <cfelse>
             <cfset retour =  0> 
		</cfif>
		<cfreturn obj>
	</cffunction> 
	
	
	
</cfcomponent>