﻿<cfcomponent extends="AbstractConnection1">
	<cffunction name="setXMLAccess" access="package" output="false" returntype="Any">
		<cftry>
			<!--- DEFINITION DU NIVEAU D'ORGA DONT ON VEUT CONNAITRE LES ACCES --->
			<cfset _IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_GROUPE>
			<cfif structkeyExists(SESSION.PERIMETRE,"ID_PERIMETRE")>
				<cfset _IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
			</cfif>
				
			
			<cflog text="Connectioncc1ca51.setXMLAccess - #SESSION.USER.CLIENTACCESSID# - #SESSION.PERIMETRE.ID_GROUPE# - #SESSION.PERIMETRE.ID_PERIMETRE#">
			
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getUniversApplication">
				<cfprocparam value="#SESSION.USER.CLIENTACCESSID#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="51"			type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qResult">
			</cfstoredproc>
			
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getAccesModApplication_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_PERIMETRE#">
				<cfprocparam value="51"			type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="1"			type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qResult2">
			</cfstoredproc>	
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getDefaultModule">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_PERIMETRE#">
				<cfprocresult name="qResult3">
			</cfstoredproc>
			
			<cfset createObject("component","fr.consotel.consoview.M00.connectApp.AppUtil").createSessionXmlPFGP(qResult,qResult2,qResult3)>
			<cfset createObject("component","fr.consotel.consoview.M00.connectApp.AppUtil").createSessionXmlPFGPAccueil(qResult,qResult2)>
			<cfreturn 1>
		<cfcatch type="any">
			<cfreturn ThrowCfCatchError(-24,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>
	
</cfcomponent>