<cfcomponent extends="AbstractConnectToApp">
<!--- CONNECTION A LA CONSOLE DE GESTION PAR LOGIN / MDP --->	
	
<!---- CHAQUE FONCTION GERE CES ERREURS ET RETOURNE UN OBJECT ERROR AVEC UN CODEERREUR
	   IL EST POSSIBLE DE PRECISER LE TYPE, LE MESSAGE ET LE DETAIL
	   <cfreturn ThrowCfCatchError(CODEERREUR,TYPE,MESSAGE,DETAIL)>
	   codeerreur : int, type : string, message : string, detail  : string --->
	<cffunction name="connectTo" 
				displayname="ConnectToApp" 
				description="Permet de se logger à une application" 
				access="public" 
				output="false" 
				returntype="struct">
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
		<cflog text="AbstractConnectToApp.connectToApp()">
		<cfset clearOk = clearSession()>
		<cfif clearOk eq 1>
		<!--- VALIDATE LOGIN --->
			<cfset result = validateLogin(objConnection)>
			<!--- si validateLogin retourne un object et que celui ci contient un codeerreur
				  alors on retourne cet object pour afficher un message d'erreur
				 --->
			<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>	
			<cfelse>
				<cfset result = setXMLAccess()>
			</cfif>
		<cfelse>
			<cfreturn ThrowCfCatchError(result)>	
		</cfif>			
		<cfreturn SESSION>
	</cffunction>	
	
	
	<!--- CREATION DE L'OBJET USER DE LA SESSION --->		
	<cffunction name="validateLogin" access="private" output="false" returntype="Any" >
		<cfargument name="objConnection" required="true" type="struct" >
		<cflog text="Connection5.validateLogin()">
		<cftry>
			<cfif StructKeyExists(SESSION, "IMPERSONNIFICATION")>
				<cfif SESSION.IMPERSONNIFICATION eq FALSE>
					<cfcookie name="LAST_LOGIN" value="#objConnection.login#" expires="never">
				</cfif>
				<cfelse>
					<cfcookie name="LAST_LOGIN" value="#objConnection.login#" expires="never">
			 </cfif>
			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="pkg_M28.connectclient">
				<cfprocparam type="in" value="#objConnection.login#" cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="in" value="#objConnection.pwd#" 	cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qValidateLogin">
			</cfstoredproc>
			<cfif p_retour eq 1>
				<cfif qValidateLogin.recordcount EQ 1>
					<cfset boolTokenHasToBeSaved = 0>
					<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
					<cfif boolCheckIP EQ 1>
						<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
						<cfif checkIpResult LT 1>
							<cfreturn ThrowCfCatchError(-2)>
						<cfelse>
							<cfif setupSessionUser(qValidateLogin)>
								<cfif objConnection.seSouvenir eq 1>
									<cfset boolTokenHasToBeSaved = 1>
								</cfif>
							<cfelse>
								<cfreturn ThrowCfCatchError(-5)>
							</cfif>
							<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
						</cfif>
					<cfelse>
						<cfif setupSessionUser(qValidateLogin)>
							<cfif objConnection.seSouvenir eq 1>
								<cfset boolTokenHasToBeSaved = 1>
							</cfif>
						<cfelse>
							<cfreturn ThrowCfCatchError(-5)>
						</cfif>
					</cfif>
			<!--- 11/07/2012 : AJOUT SSO SE SOUVENIR  --->	
					<cfif boolTokenHasToBeSaved eq 1>
						<cfset result = defineToken(objConnection)>
						 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
							<cfreturn result>
						<cfelse>
							<cfif result eq 0>
								<cfreturn ThrowCfCatchError(-27)>
							<cfelse>
								<cfreturn 1>
							</cfif>
						</cfif>
					<cfelse>	
						<cfreturn 1>
					</cfif>	
				<cfelse>
					<cfreturn ThrowCfCatchError(-3)>
				</cfif>
			<cfelse>
				<cfreturn ThrowCfCatchError(p_retour)>
			</cfif>
		<cfcatch type="any" >
			<cfreturn ThrowCfCatchError(-4,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>	
	</cffunction>
	
	<cffunction name="setXMLAccess"  access="private" output="false" returntype="Any" >
		<cftry>
			
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_M28.getListeFonctionClient">
				<cfprocparam type="in" value="#session.user.APP_LOGINID#" cfsqltype="CF_SQL_INTEGER" >
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_langid" type="in" value="#session.user.IDGLOBALIZATION#">
				<cfprocresult name="p_fonction" resultset="1">
				<cfprocresult name="p_univers" resultset="2">
			</cfstoredproc>
			
			<cfset currentPath = GetDirectoryFromPath(GetCurrentTemplatePath())/>
			<cfset fileName = currentPath & "menus/Menu_console_gestion.xml">
			<cffile action="read" file="#fileName#" variable="XMLFileText">
			<cfset xmlAccess=XmlParse(XMLFileText)>
			
			<cfset idxUnivers= 1>
			<cfloop query="p_univers" >
				<cfset tmpXmlUnivers = xmlAccess.MENU>
				<cfset tmpXmlUnivers.xmlChildren[idxUnivers] = XmlElemNew(xmlAccess,"UNIVERS")>
				<cfset tmpXmlUnivers.xmlChildren[idxUnivers].XmlAttributes.LBL = #p_univers.NOM_UNIVERS#>
				<cfset tmpXmlUnivers.xmlChildren[idxUnivers].XmlAttributes.KEY = #p_univers.UNIVERS_KEY#>
				<cfset tmpXmlUnivers.xmlChildren[idxUnivers].XmlAttributes.SYS = #p_univers.IS_SYS#>
				<cfset tmpXmlUnivers.xmlChildren[idxUnivers].XmlAttributes.ID = #p_univers.IDCG_UNIVERS#>
				<cfset tmpXmlUnivers.xmlChildren[idxUnivers].XmlAttributes.USR = "1">
				<cfset tmpXmlUnivers.xmlChildren[idxUnivers].XmlAttributes.TYPE = "radio">
				<cfset tmpXmlUnivers.xmlChildren[idxUnivers].XmlAttributes.GROUPNAME = "menuUniversGroup">
				<cfset tmpXmlUnivers.xmlChildren[idxUnivers].XmlAttributes.TOGGLED = "false">
				<cfset tmpXmlUnivers.xmlChildren[idxUnivers].XmlAttributes.ENABLED = "true">
				<cfset idxUnivers = idxUnivers+1>
			</cfloop>
			
			<cfset idxFct= 1>
			
			<cfloop query="p_fonction">
				<cfset tmpXmlFct = XmlSearch(xmlAccess,"//UNIVERS[@ID = #p_fonction.IDCG_UNIVERS#]") />
				
				<cfset myKey = KEY>
				<cfset valueAttrKey = tmpXmlFct[1].XmlAttributes.KEY>
				<cfset isKeyDiff = compareNoCase(#myKey#,#valueAttrKey#)>
				
				<cfif arraylen(tmpXmlFct) gt 0 >
					<cfif isKeyDiff neq 0>
						<cfset idxFct = arraylen(tmpXmlFct[1].xmlchildren)+1>
						<cfset tmpXmlFct[1].xmlChildren[idxFct] = XmlElemNew(xmlAccess,"FONCTION")>
						<cfset tmpXmlFct[1].xmlChildren[idxFct].XmlAttributes.LBL = #p_fonction.FONCTION#>
						<cfset tmpXmlFct[1].xmlChildren[idxFct].XmlAttributes.KEY = #p_fonction.KEY#>
						<cfset tmpXmlFct[1].xmlChildren[idxFct].XmlAttributes.SYS = "1">
						<cfset tmpXmlFct[1].xmlChildren[idxFct].XmlAttributes.USR = "2">
						<cfset tmpXmlFct[1].xmlChildren[idxFct].XmlAttributes.type = "radio">
						<cfset tmpXmlFct[1].xmlChildren[idxFct].XmlAttributes.groupName = "menuUniversGroup">
						<cfset tmpXmlFct[1].xmlChildren[idxFct].XmlAttributes.toggled = "false">
						<cfset tmpXmlFct[1].xmlChildren[idxFct].XmlAttributes.ENABLED = "true">
					</cfif>
				</cfif>
			</cfloop>
			
			<cfset SESSION.XML_ACCESS=xmlAccess>  
		<cfcatch type="any">
			<cfreturn ThrowCfCatchError(-24,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="setupSessionUser"  access="private" output="false" returntype="boolean" >
		<cfargument name="qValidateLogin" required="true" type="query" >
	
		<cflog text="Connection5.setupSessionUser()">
		
		<cftry>
			<cfset SESSION.AUTH_STATE = 1>
			<cfset SESSION.OFFREDSN = "ROCOFFRE">
			<cfset SESSION.CDRDSN = "ROCCDR">
			<cfset SESSION.PARCVIEWDSN = "CLPARCVIEW">
			<cfset SESSION.MAILSERVER = "mail-cv.consotel.fr:25">
	
			<cfset SESSION.USER = structNew()>
			<cfset SESSION.USER.NOM = qValidateLogin['NOM'][1]>
			<cfset SESSION.USER.PRENOM = qValidateLogin['PRENOM'][1]>
			<cfset SESSION.USER.EMAIL = qValidateLogin['EMAIL'][1]>
			<cfset SESSION.USER.CLIENTACCESSID = qValidateLogin['CLIENTACCESSID'][1]>
			<cfset SESSION.USER.APP_LOGINID = qValidateLogin['CLIENTACCESSID'][1]>
			
			<cfset SESSION.CG.CG_IDPROFIL = qValidateLogin['CG_IDPROFIL'][1]>
			<cfset SESSION.CG.CG_LABEL_PROFIL = qValidateLogin['CG_LABEL_PROFIL'][1]>
			<cfset SESSION.CG.CG_IDNIVEAU = qValidateLogin['CG_IDNIVEAU'][1]>
			<cfset SESSION.CG.CG_LABEL_NIVEAU = qValidateLogin['CG_LABEL_NIVEAU'][1]>
			
			<cfset SESSION.USER.GLOBALIZATION =  qValidateLogin['GLOBALIZATION'][1]>
			<cfset SESSION.USER.IDGLOBALIZATION =  qValidateLogin['IDCODE_LANGUE'][1]>
			<cfreturn TRUE>
		<cfcatch type="any">
			<cfreturn FALSE>
		</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>