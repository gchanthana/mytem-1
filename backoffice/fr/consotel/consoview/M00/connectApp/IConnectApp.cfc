﻿<cfinterface displayname="fr.consotel.consoview.M00.connectApp.IConnectApp">

	<cffunction name="ConnectTo" 
				displayname="ConnectTo" 
				description="Permet de se logger à une application" 
				access="public" 
				output="false"
				returntype="struct">
		<cfargument name="codeConnection" displayName="codeConnection" type="numeric" required="true" />
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
	</cffunction>

	<cffunction name="ChangePerimetre" 
				displayname="ChangePerimetre" 
				description="Gère le changement de périmètre de la personne connectée" 
				access="public" 
				output="false" 
				returntype="struct">
					
		<cfargument name="codeConnection" displayName="codeConnection" type="numeric" required="true" />
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
	</cffunction>	
	<cffunction name="ChangeGroupe" 
				displayname="ChangeGroupe" 
				description="Gère le changement de périmètre de la personne connectée" 
				access="public" 
				output="false" 
				returntype="struct">
					
		<cfargument name="codeConnection" displayName="codeConnection" type="numeric" required="true" />
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
	</cffunction>	
</cfinterface>