﻿<cfcomponent displayname="PeriodeFacturationByRacine"
			 hint="managment des periodes de facturations specifiques à une racine en particulier"
			 output="false">
	
	<cfset VARIABLES.dateUtil = createObject("component","fr.consotel.consoview.M00.connectApp.DateUtil")>
	
	
	<!--- RAMENE LE BON DEBUT DE FACTU SELON LA RACINE CHOISIE --->
	<cffunction name="setDebutPeriodFactuAccordingToRacine" access="public" output="false" returntype="string">
	
		<cfargument name="idGroupeClient" required="true" type="numeric">
		
		<cftry>
			<cfset LOCAL.dynamicCallFct_object = createObject("component","fr.consotel.consoview.M00.DynamicCallFuntionUtil")>
			
			<cfset LOCAL.str = "setDebutPeriodFactuAccordingToRacine_" & ARGUMENTS.idGroupeClient>
			<cfset LOCAL.method = LOCAL.dynamicCallFct_object.getMethod(THIS,LOCAL.str)>
			<cfset LOCAL.display_deb_period = LOCAL.method()>
			<cfreturn LOCAL.display_deb_period>
		<cfcatch type="any">
			<!--- si aucune période specifique pour une racine : par defaut on retire 2 ans à la date de fin de periode --->
			<cfset LOCAL.timeToSubtract = -2>
			<cfreturn VARIABLES.dateUtil.addTimeToDate("yyyy",LOCAL.timeToSubtract,SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN_ORIGINE)>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
	<!--- RACINE SMARTPHONE_ASCT_STELSIA = 6012497 --->
	<cffunction name="setDebutPeriodFactuAccordingToRacine_6012497" access="public" output="false" returntype="string">
		
		<cfset LOCAL.timeToSubtract = -30>
		<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.MAX_MONTH_TO_DISPLAY = 30>
		
		<!--- on peut soustraire mois "m", année "yyyy", etc --->
		<cfreturn VARIABLES.dateUtil.addTimeToDate("m",LOCAL.timeToSubtract,SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN_ORIGINE)>
	</cffunction>
	
	<!--- RACINE SMARTPHONE_ASCT_SNCF = 5753217--->
	<cffunction name="setDebutPeriodFactuAccordingToRacine_5753217" access="public" output="false" returntype="string">
		
		<cfset LOCAL.timeToSubtract = -30>
		<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.MAX_MONTH_TO_DISPLAY = 30>
		
		<!--- on peut soustraire mois "m", année "yyyy", etc --->
		<cfreturn VARIABLES.dateUtil.addTimeToDate("m",LOCAL.timeToSubtract,SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN_ORIGINE)>
	</cffunction>

</cfcomponent>