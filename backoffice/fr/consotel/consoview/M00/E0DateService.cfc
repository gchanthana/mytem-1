<cfcomponent output="false" displayname="E0DateService">
		
	<cffunction name="getInfosDates" access="remote" returntype="any">
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_ADMIN.GETLASTLOAD">			
			<cfprocresult name="qDATELASTLOAD">
		</cfstoredproc>	

		<cfset datesInfos = structNew()>
		<cfset datesInfos.AUTH_RESULT = 0>
		
		<cfif qDATELASTLOAD.recordcount GT 0>
				<cfset SESSION.DATES = structNew()>
				<cfset SESSION.DATES.LAST_LOAD_FACTURATION = qDATELASTLOAD['DATE_CHARGEMENT'][1]>
				<cfset SESSION.DATES.LAST_LOAD_FILL_ORGACLIENT = qDATELASTLOAD['DATE_CHARGEMENT'][2]>
				<cfset SESSION.DATES.LAST_LOAD_AGGREG_A1 = qDATELASTLOAD['DATE_CHARGEMENT'][3]>
				<cfset SESSION.DATES.LAST_LOAD_AGGREG_A1A = qDATELASTLOAD['DATE_CHARGEMENT'][4]>
				<cfset SESSION.DATES.LAST_LOAD_AGGREG_A3 = qDATELASTLOAD['DATE_CHARGEMENT'][5]>
				<cfset SESSION.DATES.LAST_LOAD_AGGREG_A4 = qDATELASTLOAD['DATE_CHARGEMENT'][6]>
				
				<cfset datesInfos.LAST_LOAD_FACTURATION = SESSION.DATES.LAST_LOAD_FACTURATION>
				<cfset datesInfos.LAST_LOAD_FILL_ORGACLIENT = SESSION.DATES.LAST_LOAD_FILL_ORGACLIENT>
				<cfset datesInfos.LAST_LOAD_AGGREG_A1 = SESSION.DATES.LAST_LOAD_AGGREG_A1>
				<cfset datesInfos.LAST_LOAD_AGGREG_A1A = SESSION.DATES.LAST_LOAD_AGGREG_A1A>
				<cfset datesInfos.LAST_LOAD_AGGREG_A3 = SESSION.DATES.LAST_LOAD_AGGREG_A3>
				<cfset datesInfos.LAST_LOAD_AGGREG_A4 = SESSION.DATES.LAST_LOAD_AGGREG_A4>					
				<cfset datesInfos.AUTH_RESULT = 1>
		</cfif>
		<cfreturn datesInfos>
	</cffunction>
	
	<cffunction name="getPFGPInfosDates" access="remote" returntype="any">
		<cflog text="E0DateService.getPFGPInfosDates">
		<cfset datesInfos = structNew()>
			<cfif structKeyExists(SESSION.DATES,"LASTPFGPLOAD")>
				<cfset datesInfos.DATE = SESSION.DATES.LASTPFGPLOAD>
			</cfif>
			<cfif structKeyExists(SESSION.DATES,"LASTPFGPIDPERIODE")>
				<cfset datesInfos.IDPERIODE = SESSION.DATES.LASTPFGPIDPERIODE>
			</cfif> 
		<cfreturn datesInfos>
	</cffunction>
</cfcomponent>