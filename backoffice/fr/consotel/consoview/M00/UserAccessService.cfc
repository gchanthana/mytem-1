<cfcomponent>
	
	
	
	<cffunction access="remote" output="false" name="getModuleUserAccess" 
		displayname="getModuleUserAccess" returntype="query" 
		description="Liste les droits utilisateur (au niveau de l'abonnement) pour un ou tous les modules de l'application" 
		hint="Liste les droits utilisateur (au niveau de l'abonnement) pour un ou tous les modules de l'application">
		<cfargument name="moduleID" required="false" type="String"/>
		
		<cfif isDefined("moduleID")>
			<cfset _code_module = moduleID>	
		<cfelse>
			<cfset _code_module = ''>	
		</cfif>	
		
		<cfset idUSer = SESSION.USER.CLIENTACCESSID>
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset idGroupeClient = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset codeModule = _code_module>		
		<cfset idLang = SESSION.USER.IDGLOBALIZATION>
		
		<cfset qUserAccess = getUserAccess(idUSer, idRacine, idGroupeClient, codeModule, idLang)>		
		<cfreturn qUserAccess>
		
	</cffunction>
	

	<cffunction access="private" output="false" name="getUserAccess" 
		displayname="getUserAccess" returntype="query" 
		description="Liste les droits utilisateur (au niveau de l'abonnement) pour un ou tous les modules de l'application" 
		hint="Liste les droits utilisateur (au niveau de l'abonnement) pour un ou tous les modules de l'application">
		<cfargument name="idUSer" type="numeric" required="true" hint="l id user (clientaccessid)">
		<cfargument name="idRacine" type="numeric" required="true" hint="l id de la racine">
		<cfargument name="idGroupeClient" type="numeric" required="true" hint="l id du noeud sur laquelle on est connecté">
		<cfargument name="codeModule" type="string" required="true" hint="le code du module pour lequel on veut les droits ou '' pour avoir tous les modules de l application">
		<cfargument name="idLang" type="numeric" required="true" hint="l id de la langue">	
					
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M00.GET_APP_USERPARAMS_V2">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idUSer#" variable="p_app_loginid">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLang#" variable="p_langid">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idGroupeClient#" variable="p_idgroupe_client">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#codeModule#">
			<cfprocresult name="qUserAccess">
		</cfstoredproc>		 
		
		<cfreturn qUserAccess>
	</cffunction>
	
	<cffunction access="remote" output="false" name="updateUserCredentials" displayname="updateUserCredentials" returntype="struct" 
				description="Update user password after compare the old password with entered old password" 
				hint="Update user password after compare the old password with entered old password">
		<cfargument name="oldPassword" required="true" type="string"/>
		<cfargument name="infosUser" required="true" type="struct" />

		<cftry>
			<cfset var resultUpdate = StructNew()>
			
			<cfset resultUpdate = verifyInfosUser(infosUser.email, arguments.oldPassword)>
			<cfif resultUpdate.val EQ 1>
				<cfset var chgtPwd = StructNew()>
				<cfset var chgtPwd = changeUserPassword(Arguments.infosUser)>
				<!--- on fait un update des attributs val, message et on garde l'attribut userAuths' --->
				<cfset resultUpdate.VAL = chgtPwd.VAL>
				<cfset resultUpdate.MESSAGE = chgtPwd.MESSAGE>
				
				<cfif resultUpdate.val EQ 1>

					<cflog type="information" text="************* JOB BI Envoi d'email de confirmation *************">
								 
					<cfset setInfosUser(Arguments.infosUser, resultUpdate.userAuths)> 	
				<cfelse>
					<cflog type="error" text="************* JOB BI :  Envoi d'email de confirmation de chagement de mot de passe n'est pas encore fait *************">	
				</cfif>	
			<cfelse>
				<cflog type="error" text="************* Verification d'utilisateur : login/pwd n'est pas bon *************">
			</cfif>	
		
		<cfreturn resultUpdate>
		<cfcatch type="any" >
			<cfmail from="mnt@saaswedo.com" subject="CFCATCH updateUserCredentials" to="mnt@saaswedo.com" type="html" >
				<b> CFCATCH </b> <cfdump var="#CFCATCH#" >
			</cfmail>
		</cfcatch>
		</cftry>
		
		<cfreturn resultUpdate>
	</cffunction>
	
	<cffunction name="verifyInfosUser" access="private" displayname="verifie si le couple login/password existe'" returntype="struct" >
		<cfargument name="infosLogin" required="true" type="String">
		<cfargument name="infosOldPassword" required="true" type="String">
		
		<cftry>
			<!---
			   -- p_retour:=0;      le login/pwd est bon mais il est expire
			   -- p_retour:=1;      le login/pwd est bon
			   -- p_retour:=2;      le login/pwd est bon mais il est bloque ou le login/pwd n'est pas bon et on l'a bloque
			   -- p_retour:=-1;     Erreur inconnue
			   -- p_retour:=-10;    on trouve pas le login , le mail n'est pas bon
			   -- p_retour:=-11;    Le login est bon mais pas le pwd
			 --->
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GLOBAL.connectClient_v2">
				<cfprocparam type="in" value="#arguments.infosLogin#" 		cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam type="in" value="#arguments.infosOldPassword#" 	cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam type="out"	variable="p_retour" 		cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qValidateLogin">
			</cfstoredproc>
			
			<cfset var result = structNew()>
			<cfset result.val = p_retour>
			<cfset result.userAuths = qValidateLogin>
			<cfswitch expression="#p_retour#" >
				<cfcase value="0">
					<cfset result.message = "le couple login/pwd est bon mais il est expiré">
				</cfcase>
				<cfcase value="1">
					<cfset result.message = "le couple login/pwd est bon">
				</cfcase>
				<cfcase value="2">
					<cfset result.message = "le couple login/pwd est bon mais il est bloqué ou le login/pwd n'est pas bon et on l'a bloque">
				</cfcase>
				<cfcase value="-1">
					<cfset result.message = "Erreur inconnue">
				</cfcase>
				<cfcase value="-10">
					<cfset result.message = "on trouve pas le login , le mail n'est pas bon">
				</cfcase>
				<cfcase value="-11">
					<cfset result.message = "Le login est bon mais pas le pwd">
				</cfcase>
				<cfdefaultcase>
					<cfset result.message = "Erreur inconnue">
				</cfdefaultcase>
			</cfswitch>

			<cfreturn result>
			
			<cfcatch type="Any" >
				<cfmail from="mnt@saaswedo.com" subject=" CATCH EXCEPTION : verifyUser" to="mnt@saaswedo.com" type="html" >
					<b> CFCATCH </b> <cfdump var="#CFCATCH#" >	
				</cfmail>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction name="changeUserPassword" access="private" displayname="mettre à jour le mot de passe de l'utilisateur'" returntype="struct" >
		<cfargument name="infosUser" required="true" type="struct" >
		<cftry>
			 
			<!--- retour : 
					  1 : Succes de maj
					  0 : Echec de maj, login inexistant ou password vide
					 -1 : Errreur
			 --->
			 
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_INTRANET.updateLogin">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" variable="P_APP_LOGINID" value="#infosUser.clientAccesId#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_NOM" value="#infosUser.nom#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_PRENOM" value="#infosUser.prenom#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_EMAIL" value="#infosUser.email#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_PWD" value="#infosUser.newPassword#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_RESTRICTION_IP" value="#infosUser.restrictIp#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
			</cfstoredproc>
						
			<cfset var _res = structNew()>
			<cfset _res.val = result>
			<cfswitch expression="#result#" >
				<cfcase value="1" >
					<cflog type="information" text="************* SUCCES de MAJ de mot passe effectué pour le login : #infosUser.email#">
					<cfset _res.message = "SUCCES de MAJ de mot passe">
				</cfcase>
				<cfcase value="0" >
					<cfset _res.message = "ECHEC de MAJ de mot passe, login inexistant ou password vide">
				</cfcase>
				<cfcase value="-1" >
					<cfset _res.message = "ERREUR MAJ de mot de passe">
				</cfcase>
				<cfdefaultcase>
					<cfset _res.message = "ERREUR MAJ de mot de passe">
				</cfdefaultcase>
			</cfswitch>
			<cfreturn _res>
			
		<cfcatch type="any" >
			<cfmail from="mnt@saaswedo.com" subject="CATCH EXCEPTION changeUserPassword" to="mnt@saaswedo.com" type="html" >
				<br/>
				<b> CFCATCH </b> <cfdump var="#CFCATCH#" >
			</cfmail>
		</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction name="setInfosUser" access="public" returntype="Any">		
		<cfargument name="infosUser" 			required="true" type="Struct" />
		<cfargument name="userAuths" 			required="true" type="query" />
		
		<!--- Récupération de l'objet utilisateur --->
			
			<cfset rsltID = -4>				
			<cfif userAuths.recordcount gt 0>
				<cfset var auhentification = 1>
			<cfelse>
				<cfset auhentification = 0>
			</cfif>
			
			<cfif auhentification GT 0>
				<cfset var objUser = [infosUser.nom, infosUser.prenom, infosUser.email, infosUser.newPassword]>
				<cfset iduser 	= userAuths['CLIENTACCESSID'][1]>
				<cfset var lang 			= SESSION.USER.GLOBALIZATION>
				<cfset var ID 				= userAuths['ID'][1]>
				<cfset var CODEAPP			= SESSION.CODEAPPLICATION>
				<cfset rsltID 	= sendIdentifier(objUser, iduser, lang, ID, CODEAPP)>
			<cfelse>
				<cfset rsltID = -4> 
			</cfif>
		
		<cfreturn rsltID>	
	</cffunction>
	
	<!--- 
		Envoie par mail des identifiants de connexion d'un utilisateur en 2 étapes'
	 --->
	 
	<cffunction  name="sendIdentifier" output="false" description="Envoie des identifiants utilisateur par mail" access="remote" returntype="numeric">
		<cfargument name="tableau" 			required="true" type="array"/>
		<cfargument name="iduser"  			required="true" type="numeric"/>
		<cfargument name="lang"    			required="true" type="string"/>
		<cfargument name="idEntr" 			required="true" type="numeric"/>
		<cfargument name="codeApplication"  required="true" type="numeric"/>
			
			
			<cfset var properties = createObject("component","fr.consotel.consoview.M01.SendMail").getProperties(codeApplication,'NULL')>
			
			<cfset var mailing  = createObject("component","fr.consotel.consoview.M01.Mailing")>
			
			<cfset var M01_UUID = ''>
			<cfset var userTab  = tableau[3]>
			
			<cfset var myVar = false>
			
			<cfset var ImportTmp="/container/M01/">
			
			<cfset var rsltApi  = -4>
			<cfset var resltRecordMail = 0>
			
			<cfset var fromMailing = properties['MAIL_EXP_N1']>

			<cfloop condition="myVar eq false">
			  <cfset M01_UUID = createUUID()>
			  <cfif NOT DirectoryExists('#ImportTmp##M01_UUID#')>
				<cfdirectory action="Create" directory="#ImportTmp##M01_UUID#" type="dir" mode="777">
				<cfset myVar="true">
				<cfdump var="#myVar#">
			  </cfif>
			</cfloop>
			
		<!--- COMMUN A TOUS --->
			<cfset _lang = Left(lang,2)>
			<cfinvoke 	returnvariable="local.insertlog.result"	
					component="fr.consotel.consoview.M00.dictionary.DictionaryManager"	
					method="init"	
			      	argumentcollection="#{lang = _lang}#"/>
					
			<!--- BIP REPORT --->
			<cfset parameters["bipReport"] = structNew()>
			<cfset parameters["bipReport"]["xdoAbsolutePath"]	= "/consoview/LOGIN/LOGIN.xdo">
			<cfset parameters["bipReport"]["outputFormat"] 		= "html">
			<cfset parameters["bipReport"]["localization"]		= _lang>
			
			<!--- DONNEES ENVOYEES A BIP --->
			<cfset parameters["bipReport"]["reportParameters"]	= structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_LOGIN"] = structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_LOGIN"]["parameterValues"] = [tableau[3]]>
			
			<!--- PARAMÈTRES DE GÉNÉRATION FTP DU RAPPORT BIP --->
			<cfset parameters.bipReport.delivery.parameters.ftpUser="container">
			<cfset parameters.bipReport.delivery.parameters.ftpPassword="container">
								
			<cfset parameters.EVENT_TARGET = M01_UUID>
			<cfset parameters.EVENT_HANDLER = "M01">
	
		<!--- COMMUN A TOUS --->	
		
		
		<!--- PASSWORD --->
		
		<!--- BIP REPORT --->
		<cfset parameters["bipReport"]["xdoTemplateId"]		= "SEND-CHGT-PWD-#codeApplication#">
		<cfset parameters.bipReport.delivery.parameters.fileRelativePath="M01/#M01_UUID#/password_#M01_UUID#.html">
		
		<cfset parameters.EVENT_TYPE = "P">
		
		<!--- ENREGISTREMENT DU REPERTOIRE DE DESTINATION --->
		<cfset resltRecordMail = mailing.setMailToDatabase(	#iduser#,
															#userTab#,
															#M01_UUID#,
															"M01",
															#parameters.EVENT_TYPE#,
															#fromMailing#,
															#userTab#,
															session['dictionaire'][_lang]['login.mail.#codeApplication#.subject'],
															'',
															'',
															#codeApplication#)>
		
		<cfif resltRecordMail GT 0>
			<!--- EXÉCUTION DU SERVICE POUR LE LOGIN --->
			<cfset api= createObject("component","fr.consotel.consoview.api.CV")>
			<cfset rsltApi = api.invokeService("IbIs","scheduleReport",parameters)>
		<cfelse>
			<cfreturn -10>
		</cfif>
			
		<cfreturn rsltApi>
	</cffunction>
</cfcomponent>