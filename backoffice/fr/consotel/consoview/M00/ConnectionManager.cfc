<cfcomponent name="AccessManager" 
			 alias="fr.consotel.consoview.M00.AccessManager"
			 extends="fr.consotel.consoview.M00.connectApp.AbstractConnectToApp">
			 	 
	<cffunction name="getLastCodePays" access="remote" output="false" returntype="string">
		<!----<cfif isDefined("SESSION")>
			<cfif structKeyExists(SESSION,"USER")>
				<cfif structKeyExists(SESSION.USER,"GLOBALIZATION")>
					<cfreturn SESSION.USER.GLOBALIZATION>
				</cfif>
			</cfif>
		</cfif>---->
		<cfif structKeyExists(COOKIE,"CODE_PAYS")>
			<cfreturn COOKIE.CODE_PAYS>
		<cfelse>
			<cfreturn "NO_CODE_PAYS">
		</cfif>
	</cffunction> 
	<cffunction name="getLastLogin" access="remote" output="false" returntype="string">
		<cfset var defaultLogin="">
		<cftry>
			<cfif structKeyExists(COOKIE,"LAST_LOGIN")>
				<cfreturn COOKIE.LAST_LOGIN>
			<cfelse>
				<cfreturn defaultLogin>
			</cfif>
			<cfcatch type="any">
				<cftry>
					<cfset var mailSubject="Exception rencontrée durant la récupération du dernier login connecté à ConsoView">
					<cfif isDefined("CFCATCH.message") AND (lCase(CFCATCH.message) EQ lCase("Session is invalid"))>
						<cfset mailSubject="Session ConsoView invalidée">
					</cfif>
					<cfmail from="consoview@consotel.fr" to="dev@consotel.fr" subject="#mailSubject# sur le backoffice : #CGI.SERVER_NAME#" type="html">
						Une exception a été rencontrée durant la récupération du dernier login utilisé par un utilisateur de ConsoView (Cookie)<br>
						Une des cause connue pour une telle exception est décrite dans le ticket : http://nathalie.consotel.fr/issues/5832<br>
						<cfoutput>
							Cible de la requete HTTP : #CGI.SCRIPT_NAME#<br>
							Adresse cliente : #CGI.REMOTE_HOST#<br>
							<cfif isDefined("APPLICATION") AND isDefined("APPLICATION.applicationName")>
								Application correspondante : #APPLICATION.applicationName#<br>
							</cfif>
						</cfoutput>
						<cfif isDefined("SESSION")>
							<cfdump var="#CFCATCH#" label="Session correspondante"><br>
						</cfif>
						<cfdump var="#CFCATCH#" label="Exception capturée"> 
					</cfmail>
					<cfset structClear(COOKIE)>
					<cfcatch type="any">
						<cfmail from="consoview@consotel.fr" to="dev@consotel.fr" subject="Erreur lors de l'invalidation d'un cookie sur le backoffice : #CGI.SERVER_NAME#" type="html">
							Une exception a été rencontrée durant l'invalidation d'un cookie ConsoView<br>
							L'invalidation du cookie a été spécifiée par le ticket : http://nathalie.consotel.fr/issues/5832<br>
							<cfoutput>
								Cible de la requete HTTP : #CGI.SCRIPT_NAME#<br>
								Adresse cliente : #CGI.REMOTE_HOST#<br>
								<cfif isDefined("APPLICATION") AND isDefined("APPLICATION.applicationName")>
									Application correspondante : #APPLICATION.applicationName#<br>
								</cfif>
							</cfoutput>
							<cfif isDefined("SESSION")>
								<cfdump var="#SESSION#" label="Session correspondante"><br>
							</cfif>
							<cfdump var="#CFCATCH#" label="Exception capturée"> 
						</cfmail>
						<cfreturn defaultLogin>
					</cfcatch>
				</cftry>
				<cfreturn defaultLogin>
			</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="getLastCodeStyle" access="remote" output="false" returntype="string">
		<cfif structKeyExists(COOKIE,"CODE_STYLE")>
			<cfreturn COOKIE.CODE_STYLE>
		<cfelse>
			<cfreturn "NO_STYLE">
		</cfif>
	</cffunction> 
	<cffunction name="searchNodes" access="remote" returntype="query">
		<cfargument name="nodeId" required="true" type="numeric">
		<cfargument name="searchKeyword" required="true" type="string">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.search_noeud">
			<cfprocparam  value="#nodeId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#SESSION.USER.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#searchKeyword#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam  value="50" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNodeList">
		</cfstoredproc>
		<cfreturn qGetNodeList>
	</cffunction>
	<cffunction name="getNodeXmlPath" access="remote" returntype="xml">
		<cfargument name="idGroupeRacine" required="true" type="numeric">
		<cfargument name="idSource" required="true" type="numeric">
		<cfargument name="idCible" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.chemin_source_cible_v2">
			<cfprocparam  value="#idSource#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#idCible#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#SESSION.USER.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qChemin">
		</cfstoredproc>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"NODE")>
		<cfset rootNode = perimetreXmlDoc.NODE>
		<cfif qChemin.recordcount GT 0>
			<cfset typeOrga = qChemin['TYPE_ORGA'][qChemin.recordcount]>
			<cfloop index="i" from="1" to="#qChemin.recordcount#">
				<cfif i EQ 1>
					<cfset rootNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
					<cfset rootNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset rootNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
					<cfset rootNode.XmlAttributes.STC = qChemin['STC'][i]>
					<cfif idSource EQ idGroupeRacine>
						<cfif qChemin.recordcount GT 1>
							<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"NODE")>
							<cfswitch expression="#UCASE(typeOrga)#">
								<cfcase value="OPE">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Opérateurs">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -2>
								</cfcase>
								<cfcase value="CUS">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Clientes">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -3>
								</cfcase>
								<cfcase value="SAV">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Recherches Sauvegardées">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -4>
								</cfcase>
								<cfdefaultcase>
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Clientes">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -3>
								</cfdefaultcase>
							</cfswitch>

							<cfset rootNode.XmlChildren[1].XmlAttributes.NTY = 0>
							<cfset rootNode.XmlChildren[1].XmlAttributes.STC = 0>
						</cfif>
					</cfif>
				<cfelseif i EQ 2>
					<cfset parentNode = ''>
					<cfif idSource EQ idGroupeRacine>
						<cfset rootNode.XmlChildren[1].XmlAttributes.NTY = 1>
						<cfset parentNode = rootNode.XmlChildren[1]>
					<cfelse>
						<cfset tmpNID = qChemin['IDGROUPE_CLIENT'][i]>
						<cfset tmpParentNID = qChemin['IDGROUPE_CLIENT'][i - 1]>
						<cfoutput>
							<cfset nodeArray = XMLSearch(perimetreXmlDoc,"//NODE[@NID=#tmpParentNID#]")>
							<cfif arrayLen(nodeArray) NEQ 1>
								<cfthrow type="INVALID_GROUP_NODES_QUERY"
										message="Requï¿½te Arbre Pï¿½rimï¿½tres Invalide"
										detail="1006">
							</cfif>
							<cfset parentNode = nodeArray[1]>
						</cfoutput>
					</cfif>
					<cfset parentChildNb = arrayLen(parentNode.XmlChildren)>
					<cfset parentNode.XmlChildren[parentChildNb + 1] = XmlElemNew(perimetreXmlDoc,"NODE")>
					<cfset tmpNode = parentNode.XmlChildren[parentChildNb + 1]>
					<cfset tmpNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
					<cfset tmpNode.XmlAttributes.STC = qChemin['STC'][i]>
				<cfelse>
					<cfset tmpNID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset tmpParentNID = qChemin['IDGROUPE_CLIENT'][i - 1]>
					<cfoutput>
						<cfset nodeArray = XMLSearch(perimetreXmlDoc,"//NODE[@NID=#tmpParentNID#]")>
						<cfif arrayLen(nodeArray) NEQ 1>
							<cfthrow type="INVALID_GROUP_NODES_QUERY"
									message="Requï¿½te Arbre Pï¿½rimï¿½tres Invalide"
									detail="1006">
						</cfif>
						<cfset parentNode = nodeArray[1]>
						<cfset parentChildNb = arrayLen(parentNode.XmlChildren)>
						<cfset parentNode.XmlChildren[parentChildNb + 1] = XmlElemNew(perimetreXmlDoc,"NODE")>
						<cfset tmpNode = parentNode.XmlChildren[parentChildNb + 1]>
						<cfset tmpNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
						<cfset tmpNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
						<cfset tmpNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
						<cfset tmpNode.XmlAttributes.STC = qChemin['STC'][i]>
					</cfoutput>
				</cfif>
			</cfloop>
		<cfelse>
			<cfset rootNode.XmlAttributes.LBL = "Pas de chemin trouvï¿½">
			<cfset rootNode.XmlAttributes.NID = -1>
			<cfset rootNode.XmlAttributes.NTY = 0>
			<cfset rootNode.XmlAttributes.STC = 0>
		</cfif>
		<cfset qChemin = ''>
		<cfreturn perimetreXmlDoc>
	</cffunction>
	<cffunction name="getParamFromCookie" access="remote" >
		<cfargument name="listeValeur" type="array" >
		
		<cftry>
			<cfif isDefined("COOKIE")>
				<cfif arraylen(listeValeur) > 0>
					<cfloop array="#listeValeur#" index="idx">
						<cfif structKeyExists(COOKIE,idx)>
							<cflog text="#COOKIE['#idx#']#" />
							<cfreturn COOKIE[#idx#]/>
							<cfbreak>
						</cfif>
					</cfloop>
				</cfif>
			</cfif>
			<cfreturn COOKIE>
		<cfcatch type="any" >
			<cfreturn cfcatch>
		</cfcatch>
		</cftry>
	</Cffunction>
	
	<cffunction name="connectTo" access="remote" output="false" returntype="Struct"> 
		<cfargument name="codeApp" required="false" type="numeric" default="1">
		<cfargument name="codeConnection" required="false" type="numeric" default="1">
		<cfargument name="objConnection" required="false" type="struct" default="1">
		
		<cfcookie name = "CODEAPPLICATION" expires = "never" value = "#codeApp#">
		<cfset SESSION.CODEAPPLICATION = codeApp>
		<cfset SESSION.OFFREDSN = "ROCOFFRE">
		
		<cftry>
			<cfset result = createObject("component","fr.consotel.consoview.M00.connectApp.Connection#codeConnection#").connectTo(#objConnection#)>
			<cfreturn result>
		<cfcatch type="any" >
			<cfreturn ThrowCfCatchError(-13,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>	
	<cffunction name="ChangePerimetre" access="remote" output="false" returntype="Struct"> 
		<cfargument name="codeApp" required="false" type="numeric" default="1">
		<cfargument name="codeConnection" required="false" type="numeric" default="1">
		<cfargument name="objConnection" required="false" type="struct">
	

	
		<cfset structAuth = structNew()>
		<cftry>
			<cfset result = createObject("component","fr.consotel.consoview.M00.connectApp.Connection#codeConnection#").ChangePerimetre(#objConnection#)>
			<cfreturn result>
		<cfcatch type="any">
			<cfset result.codeErreur = -1>
			<cfset result.Erreur = cfcatch>
			<cfreturn result>
		</cfcatch>	
		</cftry>
	</cffunction>	
	<cffunction name="ChangeGroupe" access="remote" output="false" returntype="Struct"> 
		<cfargument name="codeApp" required="false" type="numeric" default="1">
		<cfargument name="codeConnection" required="false" type="numeric" default="1">
		<cfargument name="objConnection" required="false" type="struct" default="1">
		
	
		
		<cftry>
			<cfset result = createObject("component","fr.consotel.consoview.M00.connectApp.Connection#codeConnection#").ChangeGroupe(#objConnection#)>
			<cfreturn result>
		<cfcatch type="any">
			<cfset result.codeErreur = -1>
			<cfset result.Erreur = cfcatch>
			<cfreturn result>
		</cfcatch>	
		</cftry>
	</cffunction>	
	
	<cffunction name="getNodeChild" access="remote" returntype="xml">
		<cfargument name="nodeId" required="true" type="numeric">
		<cfset var verify = (getRacine(nodeId) eq SESSION.PERIMETRE.ID_GROUPE)>

		<cflog type="Information" text="verify for #SESSION.PERIMETRE.ID_GROUPE#  and nodeID #nodeId# = #verify#" >

		<cfif verify eq true>

			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#THIS.PKG_GLOBAL#.enfant_noeud">
				<cfprocparam  value="#nodeId#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qGetChild">
			</cfstoredproc>
		<cfelse>
			<cfset qGetChild = queryNew("LIBELLE_GROUPE_CLIENT,IDGROUPE_CLIENT,TYPE_NOEUD,NUMERO_NIVEAU,IDORGA_NIVEAU")>
		</cfif>

		<cfset childXmlTree = XmlNew()>
		<cfset childXmlTree.xmlRoot = XmlElemNew(childXmlTree,"NODE")>
		<cfset rootNode = childXmlTree.NODE>
		<cfset rootNode.XmlAttributes.LBL = "Enfants de " & nodeId>
		<cfset rootNode.XmlAttributes.NID = nodeId>
		<cfset rootNode.XmlAttributes.NTY = 0>
		<cfset rootNode.XmlAttributes.STC = 0>
		<cfloop index="i" from="1" to="#qGetChild.recordcount#">
			<cfset rootNode.XmlChildren[i] = XmlElemNew(childXmlTree,"NODE")>
			<cfset rootNode.XmlChildren[i].XmlAttributes.LBL = qGetChild['LIBELLE_GROUPE_CLIENT'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NID = qGetChild['IDGROUPE_CLIENT'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NTY = qGetChild['TYPE_NOEUD'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NUMERO_NIVEAU = qGetChild['NUMERO_NIVEAU'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.IDORGA_NIVEAU = qGetChild['IDORGA_NIVEAU'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.STC = 1>
		</cfloop>
		<cfset qGetChild = ''>
		<cfreturn childXmlTree>
	</cffunction>

	<cffunction name="logoff" access="remote" returntype="numeric">
		<cfargument name="userEmail" required="true" type="string">
		<cflog type="Information" text="#SESSION.SESSIONID# - LogOff on #APPLICATION.ApplicationName#">
		
		<cfreturn 1>
	</cffunction>

	<cffunction name="getRacine" access="private" returntype="numeric">
		<cfargument name="nodeId" required="true" type="numeric">
		<cfset var racineid = -1>
		
		<cfquery name="qRacine" datasource="#SESSION.OFFREDSN#" >
			select pkg_cv_global.get_racine(#arguments.nodeId#) as idracine from dual
		</cfquery>

		<cfif qRacine.recordcount gt 0>
			<cfset racineid = qRacine.idracine>
		</cfif>

		<cflog type="Information" text="nodeID = #nodeId# form Racine > #racineid#">

		<cfreturn racineid>
	</cffunction>

</cfcomponent>
