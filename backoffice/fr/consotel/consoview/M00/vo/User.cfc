﻿component  displayname="fr.consotel.consoview.M00.User" hint="l'utilisateur connecté" output="false"
{
	property name="login_id" type="numeric" displayname="login_id" hint="l'id du login" default="0";

	property name="login" type="string" displayname="login" hint="le login de l'utilisateur";
	
	property name="nom" type="string" displayname="nom" hint="le nom de l utilisateur" default="le prenom";

	property name="prenom" type="string" displayname="prenom" hint="le prenom de l'utilisateur";
	
	property name="racine_id" type="numeric" hint="la racine d'appartenance du login" default="'";
	
	property name="racine" type="string" displayname="racine" hint="la racine d appartenance du login";	
	
	property name="locale_id" type="numeric" displayname="locale_id" hint="l identifiant de la locale ex 3 pour fr_FR" default="3";

	property name="locale" type="string" displayname="locale" hint="la localisation au format i18n" default="fr_FR";

	property name="distributeur_id" type="numeric" displayname="distributeur_id" hint="l'id du distibuteur d appartenance du login" default="0";

	property name="distributeur" type="string" displayname="distributeur" hint="le distributeur d'appartenance du login";

	property name="profil_id" type="numeric" displayname="profile_id" hint="l identifiant du profil du login" default="3";
	
	property name="profil" type="numeric" displayname="profil" hint="le profil du login 1-consotel, 2-distibuteur, 3-client";

	

	public string function getLogin()
	{
		return login;
	}
	
	public numeric function getLogin_id()
	{
		return login_id;
	}
	
	public string function getNom()
	{
		return nom;
	}

	public string function getPrenom()
	{
		return prenom;
	}
	
	public string function getLocale()
	{
		return locale;
	}

	public numeric function getLocale_id()
	{
		return locale_id;
	}

	public string function getDistributeur()
	{
		return distributeur;
	}
	
	public numeric function getDistributeur_id()
	{
		return distributeur_id;
	}

	public numeric function getProfil()
	{
		return profil;
	}

	public numeric function getProfil_id()
	{
		return profil_id;
	}

	public string function getRacine()
	{
		return racine;
	}
	
	public numeric function getRacine_id()
	{
		return racine_id;
	}	

	public any function init()
	 displayname="init" description="constructeur" hint="constructeur" output="false" returnFormat="plain"
	{
		 return this;
	}

}