﻿component  displayname="fr.consotel.consoview.PeriodeFacture" hint="Periode de facturation pour le périmetre" output="false"
{

	property name="date_min" type="date" displayname="date_min" hint="date de reference la plus ancienne " default="date de reference d inventaire periode";

	property name="date_max" type="date" displayname="date_max" hint="date max de référence dans inventaire_periode";

	property name="date_em_min" type="date" displayname="date_em_min" hint="date d émission de la facture la plus ancienne";

	property name="date_em_max" type="date" displayname="date_em_max" hint="date d émission de la facture la plus récente";

	property name="date_min_id" type="numeric" displayname="date_min_id" hint="id date mini de référence";

	property name="date_max_id" type="date" displayname="date_max_id" hint="id de la date de reference la plus récente";

	property name="date_em_min_id" type="numeric" displayname="date_em_min_id" hint="id de la date de référnce la plus ancienne";

	property name="date_em_max_id" type="numeric" displayname="date_em_max_id" hint="id de la date d'emission de facture la plus récente pour le périmetre";

	public date function getDate_min()
	{
		return date_min;
	}

	public date function getDate_max()
	{
		return date_max;
	}

	public date function getDate_em_min()
	{
		return date_em_min;
	}

	public date function getDate_em_max()
	{
		return date_em_max;
	}

	public numeric function getDate_min_id()
	{
		return date_min_id;
	}

	public date function getDate_max_id()
	{
		return date_max_id;
	}

	public numeric function getDate_em_min_id()
	{
		return date_em_min_id;
	}

	public numeric function getDate_em_max_id()
	{
		return date_em_max_id;
	}

	public  function init()
	 displayname="init" description="Constructeur" hint="Constructeur" output="false" returnFormat="plain"
	{
		
	}

}