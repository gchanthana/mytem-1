<cfcomponent name="SupportService" alias="fr.consotel.consoview.M00.SupportService">
	<cfset pkg = "PKG_INTRANET">


<cffunction name="getPaginateListeLogin" access="remote" returntype="query">
		<cfargument name="IDGROUPE_MAITRE" required="true" type="numeric">
		<cfargument name="SEARCHTEXTE" type="string" required="true">
		<cfargument name="ORDERBYTEXTE" type="string" required="true">
		<cfargument name="OFFSET" type="numeric" required="true">
		<cfargument name="LIMIT" type="numeric" required="true">

		
		
		<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]> 
			
		<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
		<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
		<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.getListeLogin_V3">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGROUPE_MAITRE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">		
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">			
			<cfprocresult name="p_result">
		</cfstoredproc> 

		<cfquery name = "qLogin" dbtype="query">
			select DIRECTION,	
				ADRESSE_POSTAL,	
				LENOM,
				'*****' as LOGIN_PWD,
				LIBELLE_GROUPE_CLIENT,
				LOGIN_EMAIL,
				TYPE_PROFIL,
				 
				IDPROFIL_THEME,
				LOGIN_PRENOM,
				VILLE_POSTAL,	
				APP_LOGINID,
				CODE_LANGUE,
				LOGIN_STATUS,
				GLOBALIZATION,
				IDREVENDEUR,	
				LIBELLE_TYPE_PROFIL,
				APPLICATIONID,
				NBRECORD,
				FAILED_CONNECT,
				CODE_POSTAL,	
				IDGROUPE_CLIENT,
				TELEPHONE,	
				RESTRICTION_IP,
				LOGIN_NOM
				from p_result
		</cfquery>
	<cfreturn qLogin>
</cffunction>



	
</cfcomponent>