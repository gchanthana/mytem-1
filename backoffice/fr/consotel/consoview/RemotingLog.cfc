<cfcomponent output="true">
	
	<cffunction name="remotingLog" access="public" returntype="numeric" hint="Infos issus du remoting">
		<cfargument name="eventCode" type="string" required="true">
		<cfargument name="errorCode" type="string" required="true">
		<cfargument name="emailString" type="string" required="true">
		<cfargument name="componentName" type="string" required="true">
		<cfargument name="methodName" type="string" required="true">
		<cfargument name="LOG_STRING_VAR" type="string" required="true">
		<cfargument name="params" type="string">
		
		<cfif structKeyExists(SESSION,"CODEAPPLICATION")>
			<cfswitch expression="#SESSION.CODEAPPLICATION#">
					
				<cfcase value="1"> 
					<cfset appliName = "CONSOVIEW_V4.0.1">
				</cfcase>
	
				<cfcase value="101"> 
					<cfset appliName = "MYTEM360_V4.0.1">
				</cfcase>
				
				<cfcase value="51"> 
					<cfset appliName = "PILOTAGE">
				</cfcase>
				
				<cfdefaultcase>
					<cfset appliName = "AUTRE">
				</cfdefaultcase>
				
			</cfswitch>	
		<cfelse>
			<cfset appliName = "PRE-LOGIN">
		</cfif>	
		
	<cfset appliName = appliName & "-PROD">
	<cfset consoviewKey = CGI.SERVER_NAME & " : " & appliName>
	
		<cfset paramNb=0>
		<cfset paramString = arguments.params>
		<cfset finalErrorCode=0>
		<cftry>
			<cfset nowString = lsTimeFormat(NOW(),"HH:mm:ss") & " - " & lsDateFormat(NOW(),"DD/MM/YYYY")>
			<!--- MODIFICATION DE LOG DES ERREURS --->
			<cfset isFault=-1>
			<cfset finalErrorCode = errorCode>
			<cfif finalErrorCode EQ 0>
				<cfset isFault=FIND("FAULT",eventCode,1)>
				<cfif isFault GT 0>
					<cfset finalErrorCode=1>
				</cfif>
			</cfif>
			<cfset clientIDADDR = COOKIE.IP_ORIGINE>
				
			<cfif LEN(LOG_STRING_VAR) EQ 0>
				<cfset DETAILLED_LOG= ''>
			<cfelse>
				<cfset DETAILLED_LOG= "[" & LOG_STRING_VAR & " Paramètre : " & params &"]">
			</cfif>
		   	<cfquery name="qInsertLogEvent" datasource="ROCOFFRE" >
				INSERT INTO LOG_REPORT(date_log,app_log,code_erreur,event_log,ipaddr,login_log,classe_log,methode_log,DESCRIPTION_LOG,DESCRIPTION_LOG_LONG)
				VALUES (sysdate,'#consoviewKey#',#VAL(finalErrorCode)#,'#eventCode#','#LEFT(clientIDADDR,30)#','#emailString#','#LEFT(componentName,200)#','#methodName#','#LEFT(TRIM(DETAILLED_LOG),1000)#','#LEFT(TRIM(DETAILLED_LOG),2500)#')
			</cfquery>

			<cfcatch>
				<cfif LEN(LOG_STRING_VAR) EQ 0>
					<cfset DETAILLED_LOG= '' >
				<cfelse>
					<cfset DETAILLED_LOG= "[" & LOG_STRING_VAR & " Paramètre : " & params &"]">
				</cfif>
				<cfquery name="qInsertLogEvent" datasource="ROCOFFRE" >
						INSERT INTO LOG_REPORT(date_log,app_log,code_erreur,event_log,ipaddr,login_log,classe_log,methode_log,DESCRIPTION_LOG,DESCRIPTION_LOG_LONG)
						VALUES (sysdate,'#consoviewKey#',#errorCode#,'#eventCode#','#LEFT(COOKIE.IP_ORIGINE,30)#','#emailString#','#LEFT(componentName,200)#','#methodName#','#LEFT(TRIM(DETAILLED_LOG),1000)#','#LEFT(TRIM(DETAILLED_LOG),2500)#')
				</cfquery>	
				<cfset finalErrorCode=0>
					<cfreturn finalErrorCode>
				</cfcatch>
		</cftry>
		<cfreturn finalErrorCode>
	</cffunction>
</cfcomponent>