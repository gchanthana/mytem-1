<cfcomponent name="facade" displayname="facade" alias="fr.consotel.consoview.tb.accueil.facade">
	
	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>
	 
	<!--- INITIATLISATION --->
	<cffunction name="init" access="public">
		<cfargument name="perimetreParm" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		
		<cfset perimetre = "">		
		<cfswitch expression="#ucase(perimetreParm)#">
			
			<cfcase value="SOUSTETE"> <cfset perimetre = "SousTete"> </cfcase>
			<cfcase value="GROUPELIGNE"> <cfset perimetre = "GroupeLigne"> </cfcase>
			<cfcase value="GROUPE"> <cfset perimetre = "Groupe"> </cfcase>
			
			<cfdefaultcase>
				<cfset perimetre = #perimetreParm#>
			</cfdefaultcase>
		
		</cfswitch>
			
		<cfset obj1=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>
		<cfset obj1.init(perimetre,modeCalcul)>
		
		
		<cfif ucase(perimetre) eq "SOUSTETE">			
			<cfset dataset=obj1.queryData(numero,
					transformDate(datedeb), 
					transformDate(datefin),sousModeCalcul)>
			<cfset mobile =  session.parametre.currentMobileAccess>
			<cfset data =  session.parametre.currentDataAccess>
			<cfset fixe =  session.parametre.currentFixeAccess>				
		<cfelse>
			
			<cfset dataset=obj1.queryData(numero,
					transformDate(datedeb), 
				 	transformDate(datefin),sousModeCalcul)>		
			<cfset mobile = 1>
			<cfset session.parametre.currentMobileAccess = mobile>
			<cfset data =  1>
			<cfset session.parametre.currentDataAccess =  data>
			<cfset fixe =  1>			
			<cfset session.parametre.currentFixeAccess =  fixe>
		</cfif>		
		<cfset resultat=dataset>	
		
		
		<cfquery name="result"  dbtype="query">
			<cfif sousModeCalcul eq 'complet'>
				<cfif (fixe eq 1) and (mobile eq 1) and (data eq 1)>
					select * from dataset 
				<cfelse>
					<cfif (fixe eq 1) and (mobile eq 1) and (data eq 0)>
						select * from dataset where SEGMENT_THEME = 'Fixe' or SEGMENT_THEME = 'Mobile'
					<cfelse>
						<cfif (fixe eq 1) and (mobile eq 0) and (data eq 1)>
							select * from dataset where SEGMENT_THEME = 'Fixe' or SEGMENT_THEME = 'Data'					
						<cfelse>
							<cfif (fixe eq 0) and (mobile eq 1) and (data eq 1)>
								select * from dataset where SEGMENT_THEME = 'Data' or SEGMENT_THEME = 'Mobile'
							</cfif>	
						</cfif>							
					</cfif>			
				</cfif>	
			<cfelse>
				select * from dataset 
			</cfif>	
		</cfquery> 
		
		<cfset setSessionValue("RECORDSET",result)>	
	</cffunction>
	
	<!--- RETOURNE LES SUR-THEMES ABOS --->	
	<cffunction name="getSurThemeAbos" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>		
		<cfset result = obj.getSurthemeAbos()>
		<cfreturn result>
	</cffunction>  
 	
	<!--- RETOURNE LES SUR-THEMES CONSOS --->	
	<cffunction name="getSurThemeConsos" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>		
		<cfset result = obj.getSurthemeConsos()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LES  THEMES CONSOS --->	
	<cffunction name="getConso" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>		
		<cfset result = obj.getConso()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LES  THEMES ABOS --->	
	<cffunction name="getAbo" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>	
		<cfset result = obj.getAbo()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LES  THEMES CONSOS POUR UN SEGMENT DONNE--->	
	<cffunction name="getConsoBySegment" access="public" returntype="query">
		<cfargument name="sousModeCalcul" type="string" required="false">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>		
		<cfset result = obj.getConsoBySegment(sousModeCalcul)>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LE  TOTAL DES MONTANT ABOS POUR CHAQUE SEGMENT--->	
	<cffunction name="getTotalConsosBySegment" access="public" returntype="query">
		<cfargument name="sousModeCalcul" type="string" required="false">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>	
		<cfset result = obj.getTotalConsosBySegment()>
		<cfreturn result>
	</cffunction>
	
	<!--- RETOURNE LES  THEMES ABOS POUR UN SEGMENT DONNE--->	
	<cffunction name="getAboBySegment" access="public" returntype="query">
		<cfargument name="sousModeCalcul" type="string" required="false">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>	
		<cfset result = obj.getAboBySegment(sousModeCalcul)>
		<cfreturn result>
	</cffunction>  
	
	
	<!--- RETOURNE LE  TOTAL DES MONTANT ABOS POUR CHAQUE SEGMENT--->	
	<cffunction name="getTotalAboBySegment" access="public" returntype="query">
		<cfargument name="sousModeCalcul" type="string" required="false">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>	
		<cfset result = obj.getTotalAboBySegment()>
		<cfreturn result>
	</cffunction>
	

	<!--- RETOURNE LA REPARTITION PAR OPERATEUR --->	
	<cffunction name="getRepartOperateur" access="public" returntype="query">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		 
			<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>		
			<cfset result = obj.getRepartOperateur()>			
		 
		<cfreturn result>
	</cffunction>  

	<!--- RETOURNE LA REPARTITION PAR OPERATEUR POUR UN SEGMENT DONNE--->	
	<cffunction name="getRepartOperateurBySegment" access="public" returntype="query">		 
		<cfargument name="sousModeCalcul" type="string" required="false">		 
			<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>		
			<cfset result = obj.getRepartOperateurBySegment(sousModeCalcul)>			
		 
		<cfreturn result>
	</cffunction>  

	
	<!--- RETOURNE LA REPARTITION DES DUREE PAR OPERATEUR --->	
	<cffunction name="getRepartDuree" access="public" returntype="query">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>		
		<cfset result = obj.getRepartDuree()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LA REPARTITION DES DUREE PAR OPERATEUR POUR UN SEGMENT DONNE--->	
	<cffunction name="getRepartDureeBySegment" access="public" returntype="query">		
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>		
		<cfset result = obj.getRepartDureeBySegment(sousModeCalcul)>
		<cfreturn result>
	</cffunction>  
		
	
	<!--- RETOURNE L'EVOLUTION DES COUTS --->	
	<cffunction name="getEvolution" access="public" returntype="struct">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="periodicite" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		<cfset setSessionValue("periodicite_facture",#periodicite#)>  	
		 
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>
		<cfset obj.init(perimetre,modeCalcul)>
		<cfset result = obj.getEvolution(numero,
				transformDate(datedeb), 
				transformDate(datefin))>		
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LE LIBELLE ABOS --->	
	<cffunction name="getLibelleAbo" access="public" returntype="string">
		<cfargument name="Perimetre" type="string" required="true">
		<cfargument name="ModeCalcul" type="string" required="true">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>
		<cfset obj.init(perimetre,modeCalcul)>	
		<cfset result = obj.getLibelleAbo()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LE LIBELLE CONSO --->	
	<cffunction name="getLibelleConso" access="public" returntype="string">
		<cfargument name="Perimetre" type="string" required="true">
		<cfargument name="ModeCalcul" type="string" required="true">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.accueil.Tableaubord")>		
		<cfset obj.init(perimetre,modeCalcul)>
		<cfset result = obj.getLibelleConso()>
		<cfreturn result>
	</cffunction> 
	
<!---::::::::::::::::::::::::::::::::::::::::::::::::::PRIVATE::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

	<cffunction name="setSessionValue" output="true" access="public" >
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset "session.#cle#"=valeur>
		<cfelse>
			<cfset structInsert(session,cle,valeur)>
		</cfif>
	</cffunction>


</cfcomponent>