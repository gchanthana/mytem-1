<!--- =========================================================================
Name: RefclientAboStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords$
========================================================================== --->
<cfcomponent displayname="SocieteCompteAboStrategy" hint="" extends="Strategy">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="SocieteCompteAboStrategy" displayname="RefclientAboStrategy init()" hint="Initialize the RefclientAboStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(integer, string, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfargument name="tb" required="false" type="string" default="complet" displayname="tb" />	
			<cfquery name="qGetDetailFactureAbo" datasource="#session.OFFREDSN#">
				SELECT tp.type_theme, tp.theme_libelle,tp.idtheme_produit, nvl(a.qte, 0) AS qte,
			       	nvl(a.nombre_appel, 0) AS nombre_appel, nvl(a.duree_appel, 0) AS duree_appel,
		    	   	nvl(a.montant_final,0) AS montant_final, a.nom , tp.ordre_affichage, tp.segment_theme, tp.sur_theme
				FROM
				(SELECT 	t.type_theme, t.theme_libelle,t.idtheme_produit, nvl(sum(tpc.qte), 0) AS qte,
							       	nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel,
						    	   	sum(nvl(tpc.montant_final,0)) AS montant_final, tpc.nom
						    FROM 	theme_produit t, 
						       		( 
						         		SELECT tp.*, dfa.nombre_appel, dfa.duree_appel, dfa.qte, dfa.montant_final,nom
						         		FROM THEME_PRODUIT_CATALOGUE tp, 
						         			( 
								           		SELECT 	SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
						        		   				SUM(df.duree_appel) AS duree_appel, df.idproduit_client, 
								           				pc.idproduit_catalogue, nom
						           				FROM 	detail_facture_abo df, produit_client pc, produit_catalogue pca, operateur o,
						           						(	SELECT a.idinventaire_periode , a.numero_facture, a.date_emission
                              						FROM inventaire_periode a, compte_facturation b
                              						WHERE a.idcompte_facturation=b.idcompte_facturation
                              								AND b.idref_client=#ID#
                              				   ) ip
						           				WHERE 	df.idproduit_client=pc.idproduit_client 
										        		AND pca.idproduit_catalogue=pc.idproduit_catalogue
														AND pca.operateurid=o.operateurid
														AND df.idinventaire_periode=ip.idinventaire_periode
										        		AND trunc(ip.date_emission)<=trunc(last_day(#DateFin#))
														AND trunc(ip.date_emission)>=trunc(#DateDebut#,'MM')
														AND df.idref_client=#ID#
										        GROUP BY df.idproduit_client, pc.idproduit_catalogue,o.nom
						         			) dfa 
						         		WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue (+)
						       		) tpc 
							WHERE t.idtheme_produit=tpc.idtheme_produit (+)
				   		HAVING nvl(tpc.nom,' ')<>' '
							GROUP BY t.type_theme,t.theme_libelle,t.ordre_affichage,t.idtheme_produit, tpc.nom
							ORDER BY t.ordre_affichage) a,
				theme_produit tp
				WHERE a.idtheme_produit (+) =tp.idtheme_produit 
				<cfif tb neq "Complet">
					and lower(tp.segment_theme)=lower('#tb#')
				</cfif>
		</cfquery>
		<cfreturn qGetDetailFactureAbo>
	</cffunction>
	
	<cffunction name="getEvolution" access="public" output="false" returntype="struct" displayname="" hint="Ram?ne les donn?es dans une structure">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfargument name="tb" required="false" type="string" default="complet" displayname="tb" />	
		<cfquery name="qGetMois" datasource="#session.OffreDSN#">
			SELECT SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
                SUM(df.duree_appel) AS duree_appel, pc.type_theme, to_char(ip.date_emission, 'yyyy/mm')||'/01' as mois
			FROM detail_facture_abo df, 
              (	SELECT pc1.idproduit_client , tp.type_theme
                  FROM 	produit_client pc1, produit_catalogue pca1, theme_produit_catalogue tpc , 
								theme_produit tp
                  WHERE pca1.idproduit_catalogue=tpc.idproduit_catalogue
                        AND pca1.idproduit_catalogue=pc1.idproduit_catalogue
								AND tpc.Idtheme_Produit=tp.idtheme_produit
                        AND pc1.idref_client=#ID#
						<cfif tb neq "Complet">
							and lower(tp.segment_theme)=lower('#tb#')
						</cfif>
					) pc, 
 					(	SELECT a.idinventaire_periode , a.numero_facture, a.datedeb, a.date_emission
 						FROM inventaire_periode a, compte_facturation b
 						WHERE a.idcompte_facturation=b.idcompte_facturation
 								AND b.idref_client=#ID#
 				   ) ip
			WHERE df.idproduit_client=pc.idproduit_client 
               AND df.idinventaire_periode=ip.idinventaire_periode
               AND df.idref_client=#ID#
					AND trunc(ip.date_emission)>=trunc(add_months(#DateFin#,-5),'MM')
              	AND trunc(ip.date_emission)<=trunc(last_day(#DateFin#))
			GROUP BY  pc.type_theme,to_char(ip.date_emission, 'yyyy/mm')||'/01'
		</cfquery>
		<cfreturn ProcessQuery(qGetMois, DateDebut ,DateFin)>
	</cffunction>
	
	<cffunction name="ProcessQuery" access="private" returntype="struct">
		<cfargument name="qGetMois" required="false" type="query" default=""  />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfset st_evolution_cout=structnew()>
		<cfif DateFin eq "">
			<cfset DateFin="#CreateDate("2004","01","01")#">
		</cfif>
		
		<!--- Preparation d'une structure vide --->
		<cfloop from="5" to="0" index="i" step="-1">
				<cfset temp=structnew()>
				<cfset temp.coutAbo=0>
				<cfset temp.coutConso=0>
				<cfset key=DateAdd('m', -i, DateFin)>
				<cfset lemois=dateformat(key, 'yyyy/mm') & "/01">
				<cfset var=structinsert(st_evolution_cout, lemois, temp)>
		</cfloop>
		
		<!--- <cf_dump variable="st_evolution_cout"> --->
		<cfset structTriee=ListSort(StructKeyList(st_evolution_cout),"textnocase", "asc")>
		<cfloop list="#structTriee#" index="imois">
			<cfloop query="qGetMois">
				<cfif iMois eq mois and type_theme eq "Abonnements">
					<cfset st_evolution_cout[iMois].coutAbo=st_evolution_cout[iMois].coutAbo+montant_final>
				<cfelseif iMois eq mois and type_theme eq "Consommations">
					<cfset st_evolution_cout[iMois].coutConso=st_evolution_cout[iMois].coutConso+montant_final>
				</cfif>
			</cfloop>
		</cfloop>
		
		<!--- <cf_dump variable="st_evolution_cout"><br> --->
		
		<cfif session.periodicite_facture eq 2>
			<cfset st_evolution_cout_final=structnew()>
			<cfset flag=0>
			<cfset mois_temp="">
			<cfset temp=structnew()>
			<cfset temp.coutConso=0>
			<cfset temp.coutAbo=0>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfif flag eq 1>
					<cfset compteur=compteur+1>
					<cfset mois_temp=mois_temp & "/" & LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm") &" " &left(iMois,4)>
					<cfset temp.coutConso=temp.coutConso+st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=temp.coutAbo+st_evolution_cout[iMois].coutAbo>
					<cfset temp.mois=mois_temp>
					<!--- <cfoutput>#compteur#:#mois_temp#<br></cfoutput> --->
					<cfset var=structinsert(st_evolution_cout_final,compteur , temp)>
					<cfset flag=0>
					<cfset mois_temp="">
				<cfelse>
					<cfset temp=structnew()>
					<cfset mois_temp=LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm")>
					<cfset temp.coutConso=st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=st_evolution_cout[iMois].coutAbo>
					<cfset flag=1>
				</cfif>
			</cfloop>
			<!--- Remet la structure dans l'ordre --->
			<cfset st_evolution_cout=structnew()>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout_final),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfset compteur=compteur+1>
				<cfset temp=structnew()>
				<cfset temp.mois=imois>
				<cfset temp.coutConso=st_evolution_cout_final[iMois].coutConso>
				<cfset temp.coutAbo=st_evolution_cout_final[iMois].coutAbo>
				<cfset temp.mois=st_evolution_cout_final[iMois].mois>
				<cfset var=structinsert(st_evolution_cout,compteur , temp)>
			</cfloop>
		<cfelse>
			<cfset st_evolution_cout_final=structnew()>
			<cfset flag=0>
			<cfset mois_temp="">
			<cfset temp=structnew()>
			<cfset temp.coutConso=0>
			<cfset temp.coutAbo=0>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfif flag eq 1>
					<cfset compteur=compteur+1>
					<cfset mois_temp=mois_temp & "/" & LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm") &" " &left(iMois,4)>
					<cfset temp.coutConso=temp.coutConso+st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=temp.coutAbo+st_evolution_cout[iMois].coutAbo>
					<cfset temp.mois=mois_temp>
					<!--- <cfoutput>#compteur#:#mois_temp#<br></cfoutput> --->
					<cfset var=structinsert(st_evolution_cout_final,compteur , temp)>
					<cfset flag=0>
					<cfset mois_temp="">
				<cfelse>
					<cfset temp=structnew()>
					<cfset mois_temp=LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm")>
					<cfset temp.coutConso=st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=st_evolution_cout[iMois].coutAbo>
					<cfset flag=1>
				</cfif>
			</cfloop>
			<!--- Remet la structure dans l'ordre --->
			<cfset st_evolution_cout=structnew()>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout_final),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfset compteur=compteur+1>
				<cfset temp=structnew()>
				<cfset temp.mois=imois>
				<cfset temp.coutConso=st_evolution_cout_final[iMois].coutConso>
				<cfset temp.coutAbo=st_evolution_cout_final[iMois].coutAbo>
				<cfset temp.mois=st_evolution_cout_final[iMois].mois>
				<cfset var=structinsert(st_evolution_cout,compteur , temp)>
			</cfloop>
		</cfif>
		<cfreturn st_evolution_cout > 
	</cffunction>
	
	<cffunction name="getStrategy" returntype="string" access="public" output="false">
		<cfreturn "Complet">
	</cffunction>
	
	<cffunction name="getLibelleAbo" returntype="string" access="public" output="false" hint="Ram?ne le p?rim?tre de la requ?te et le mode de calcul (Usage/facturation)">
		<cfreturn "Abonnements">
	</cffunction>
	
	<cffunction name="getLibelleConso" returntype="string" access="public" output="false" hint="Ram?ne le p?rim?tre de la requ?te et le mode de calcul (Usage/facturation)">
		<cfreturn "Consommations">
	</cffunction>
	
	<cffunction name="getNumero" returntype="string" access="public" output="false">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
			<cfquery name="qGetCompte" datasource="#session.OffreDSN#">
				select ref_client
				from ref_client rc
				where rc.idref_client=#ID#
			</cfquery>
			<cfreturn qGetCompte.ref_client>
	</cffunction>
</cfcomponent>
