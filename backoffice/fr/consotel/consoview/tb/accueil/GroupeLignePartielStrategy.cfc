<!--- =========================================================================
Classe: GroupeLignePartielStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GroupeLignePartielStrategy" hint=""  extends="" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GroupeLignePartielStrategy" hint="Remplace le constructeur de GroupeLignePartielStrategy.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<cffunction name="init" access="public" returntype="Strategy" output="false" hint="Constructeur de la classe. Crée et renvoie un objet du type Strategy qui est une sous classe de AbstractStrategy." >
	</cffunction>
	<cffunction name="getData" access="public" returntype="query" output="false" hint="Renvoie les données pour le tableau de bord." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint=%qt%%paramNotes%%qt% />
		<cfargument name="tb" required="false" type="string" default="" displayname="string tb" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	<cffunction name="getEvolution" access="public" returntype="struct" output="false" hint="Renvoie les données sur les coéts des abonnements et les consommations pour les 6 derniers mois." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint=%qt%%paramNotes%%qt% />
		<cfargument name="tb" required="false" type="String" default="" displayname="String tb" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	<cffunction name="ProcessQuery" access="private" returntype="struct" output="false" hint="Transforme la requette de l'evolution des coéts des abonnements et des consommations sur les 6 derniers mois, en une structure. Les 6 mois sont divisés en groupes de 2 mois (donc 3 groupes) identifiés par 1 pour la premiére période de 2 mois, 2 pour le suivant et ainsi de suite. La structure associe é chaque numéro de période une structure contenant les données de la période.
	Cette structure associe é son tour la clé COUTABO le montant des abonnements, é COUTCONSO le montant des Consommations et é MOIS une chaéne de caractére indiquant les 2 mois (Ex: oct./nov 2005) regroupés dans le numéro de groupe concerné." >
		<cfargument name="qGetMois" required="false" type="query" default="" displayname="query qGetMois" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	<cffunction name="getStrategy" access="public" returntype="string" output="false" hint="Renvoie le mode de calcul utilisé pour la strategy." >
	</cffunction>
	<cffunction name="getLibelleAbo" access="public" returntype="string" output="false" hint="Renvoie le libéllé a utilisé pour les Abonnements." >
	</cffunction>
	<cffunction name="getLibelleConso" access="public" returntype="String" output="false" hint="Renvoie le libéllé a utilisé pour les Consommations." >
	</cffunction>
	<cffunction name="getNumero" access="public" returntype="string" output="false" hint="Renvoie sous forme de chaéne de caractéres, le libelle pour représenter le périmétre." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>