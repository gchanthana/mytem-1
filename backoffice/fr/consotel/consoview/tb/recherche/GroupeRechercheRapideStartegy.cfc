<cfcomponent name="GroupeMaitreRechercheRapide" displayname="GroupeMaitreRechercheRapide" hint="Recherche rapide pour un groupe maitre">

	<cffunction name="init" access="public" output="false" returntype="GroupeCompletStrategy" displayname="GroupeCompletStrategy init()" hint="Initialize the GroupeCompletStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
	
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, string, string, string)" >		
		<cfargument name="IDCompte" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="chaine" required="true" type="string" default="" displayname="la chaine de recherche" hint="Initial value for the chaine property." />
		<cfargument name="DateDebut" required="true" type="string" default="" displayname="numeric DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="true" type="string" default="" displayname="numeric DateFin" hint="Initial value for the DateFinproperty." />
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_SEARCH_V3.SEARCH_NODES_HIERARCHIE">			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_chaine" value="#chaine#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#IDCompte#"/>									        	
			<!--- cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#DateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_datefin" value="#DateFin#"/> --->		
			<cfprocresult name="p_result" />        
		</cfstoredproc>
		<cfreturn p_result/>
	</cffunction>	
	
	<cffunction name="getLignes" access="public" returntype="query" output="false" displayname="query getData(numeric,string, string)" >		
		<cfargument name="IDGroupe" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="numeric DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="numeric DateFin" hint="Initial value for the DateFinproperty." />
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_SEARCH_V3.SEARCH_NODES_SOUS_TETES">						
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#IDGroupe#"/>									        	
			<cfprocresult name="p_result" />        
		</cfstoredproc>
		<cfreturn p_result/>
	</cffunction>		
	
</cfcomponent>