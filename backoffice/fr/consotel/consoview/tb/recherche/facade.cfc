 <cfcomponent name="facade" displayname="facade">
	<!--- INITIATLISATION 
	
	<cffunction name="init" access="public">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		
		<cfset obj1=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
		<cfset obj1.init(perimetre,modeCalcul)>
		
		<cfset dataset=obj1.queryData(numero,createOdbcDate(datedeb),createOdbcDate(datefin),modeCalcul)>
		<cfset setSessionValue("RECORDSET",#dataset#)>					
	</cffunction> --->
	
	<!--- RETOURNE LES PRODUITS DU THEME --->	
	<cffunction name="rechercher" access="public" returntype="xml">
		
		<cfargument name="perimetre" type="string" required="false">	
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="chaine" type="string" required="true">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.recherche.Recherche")>
		<cfset obj.init(perimetre)>
				
		<cfset result=obj.queryData(numero,
					chaine,
					transformDate(datedeb), 
					transformDate(datefin))>
										
		<cfif result.recordcount eq 0>
			<cfset xml = XmlNew()>
			<cfset xml.xmlRoot =  XmlElemNew(xml,"node")>
			<cfreturn xml>
		<cfelse>
			<cfif not structKeyExists(session,"rechercheTb")>
				<cfset session.rechercheTb = structNew()>							
			</cfif>
			<cftry>
				<cfset structInsert(session.rechercheTb,chaine,result)>
			<cfcatch type="any">
			</cfcatch>
			</cftry>
			<cfreturn transformXml(result,numero)>
		</cfif>
		
	</cffunction>  
	
	<cffunction name="rechercherListeLigneGroupeRacine" access="public" returntype="query">
		
		<cfargument name="perimetre" type="string" required="false">	
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="chaine" type="string" required="true">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.recherche.Recherche")>
		<cfset obj.init(perimetre)>
				
		<cfset result=obj.queryData(numero,
					chaine,
					transformDate(datedeb), 
					transformDate(datefin))>
										
		 <cfreturn result>
	</cffunction>  
 	
	<cffunction name="rechercherListeLigne" access="public" returntype="query">
		<cfargument name="perimetre" type="string" required="false">	
		<cfargument name="idnode" type="numeric" required="true">
		<cfargument name="chaine" type="string" required="true">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.recherche.Recherche")>
		<cfset obj.init(perimetre)>
		<cfset result=obj.listeLigne(searchIdGroupe(idnode,session.rechercheTb[chaine]),					
				transformDate(datedeb), 
				transformDate(datefin))>				
		<cfreturn result>
	</cffunction>
	
	<cffunction name="rechercherListeLigneFormNode" access="public" returntype="query">
		<cfargument name="idnode" type="numeric" required="true">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.recherche.Recherche")>
		<cfset obj.init("GroupeLigne")>
		<cfset result=obj.listeLigne(idnode)>				
		<cfreturn result>
	</cffunction>
	
	<cffunction name="deleteSessionResultatRecherche" access="public">
		<cfif structKeyExists(session,"rechercheTb")>
			<cfset StructClear(session.rechercheTb)>								
		</cfif>			
	</cffunction>
	
	<cffunction name="deleteUnUsedSessionResultatRecherche" access="public">
		<cfargument name="cle" type="string">						
		<cfif structKeyExists(session,"rechercheTb")>
			<cfif structKeyExists(session.rechercheTb,cle)>
				<cfset structDelete(session.rechercheTb,cle)>
			</cfif>
		</cfif>		
	</cffunction>
	
		
		
	<cffunction name="getSessionListePerimetreNodId" access="public" returntype="numeric">
		<cfargument name="cle" required="true" type="string">
		<cfargument name="index" required="true" type="numeric">
	
<!--- 		<cfset idgroupe = searchIdGroupe(index,session.rechercheTb[cle])> --->
		<cfset result = searchNodeId(Evaluate("session.recherchetb."&cle&"['IDGROUPE_CLIENT'][index]")
													,SESSION.PERIMETRE.LISTE_PERIMETRES_QUERY)>
		<cfreturn result>
	</cffunction>	
	
	<!---::::::::::::::::::::::::::::::::::::::::::::::::::PRIVATE:::::::::::::::::::::::::::::::::::::::::::::::::::::::::---> 
	<cffunction name="transformDate" access="private" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>
	
	<cffunction name="transformXml" access="private" returntype="xml">		
		<cfargument name="dataset" type="query" required="true">		
		<cfargument name="groupIndex" type="numeric" required="true">
		<cfdump var="#dataset#">
		<cfset clientAccessId = SESSION.user.CLIENTACCESSID>
		
		<cfset idGroupe = dataset['IDGROUPE_CLIENT'][1] >
		<cfset libelleGroupeClient =  dataset['LIBELLE_GROUPE_CLIENT'][1]>
		
	
				
		<cfset dataStruct = structNew()>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset dataStruct.ROOT_NODE = rootNode>
		<cfset rootNode.XmlAttributes.LABEL = dataset['LIBELLE_GROUPE_CLIENT'][1]>
		<cfset rootNode.XmlAttributes.NODE_ID = dataset['NODE_ID'][1]>
		<cfset rootNode.XmlAttributes.IDGROUPE_CLIENT = dataset['IDGROUPE_CLIENT'][1]>
		<cfset rootNode.XmlAttributes.LEVEL = dataset['NIVEAU'][1]>
		<cfset rootNode.XmlAttributes.TYPE_PERIMETRE = dataset['TYPE_PERIMETRE'][1]>
		<cfset rootNode.XmlAttributes.BOOL_ORGA = 1>
		<cfset rootNode.XmlAttributes.SELECTABLE  = 0>
		<cfset rootNode.XmlAttributes.TYPE_ORGA = "">
		<cfset rootNode.XmlAttributes.FLAG = "">
		
		
		<cfif ispresent("OPE","TYPE_ORGA",dataset) gte 1> 			
			<!---******** GROUPEMENTS ORGANISATIONS OPERATEUR***********--->			
			<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Organisations OpÃ©rateurs">
			<cfset rootNode.XmlChildren[1].XmlAttributes.BOOL_ORGA = 1>
			<cfset rootNode.XmlChildren[1].XmlAttributes.SELECTABLE = 0>
			<cfset dataStruct.OP_ORGANISATIONS = rootNode.XmlChildren[1]>
		<cfelseif ispresent("CUS","TYPE_ORGA",dataset) gte 1
					or ispresent("GEO","TYPE_ORGA",dataset) gte 1
						or ispresent("ANA","TYPE_ORGA",dataset) gte 1>			
				<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Organisations Clientes">
				<cfset rootNode.XmlChildren[1].XmlAttributes.BOOL_ORGA = 1>	
				<cfset rootNode.XmlChildren[1].XmlAttributes.SELECTABLE = 0>			
				<cfset dataStruct.CLIENT_ORGANISATIONS = rootNode.XmlChildren[1]>
			<cFif ispresent("SAV","TYPE_ORGA",dataset) gte 1>
				<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Organisations SauvegardÃ©es">
				<cfset rootNode.XmlChildren[1].XmlAttributes.BOOL_ORGA = 1>	
				<cfset rootNode.XmlChildren[1].XmlAttributes.SELECTABLE = 0>			
				<cfset dataStruct.SAUVEGARDE_ORGANISATIONS = rootNode.XmlChildren[1]>					
			</cfif>
		</cfif>
		<!---**************************************************************************************--->		
		<cfloop index="i" from="2" to="#dataset.recordcount#">			
			<cfset currentKey = dataset['IDGROUPE_CLIENT'][i]>
			<cfset currentParentKey = dataset['ID_GROUPE_MAITRE'][i]>
			<cfif dataset['ID_GROUPE_MAITRE'][i] EQ idGroupe>								
				<cfswitch expression="#dataset['TYPE_ORGA'][i]#">
					<cfcase value="OPE">
						<cfset currentParentKey = 'OP_ORGANISATIONS'>
					</cfcase>
					<cfcase value="CUS">
						<cfset currentParentKey = 'CLIENT_ORGANISATIONS'>
					</cfcase>
					<cfcase value="GEO">
						<cfset currentParentKey = 'CLIENT_ORGANISATIONS'>
					</cfcase>
					<cfcase value="SAV">
						<cfset currentParentKey = 'SAUVEGARDE_ORGANISATIONS'>
					</cfcase>
					<cfdefaultcase>
						<cfset currentParentKey = 'ROOT_NODE'>
					</cfdefaultcase>					
				</cfswitch>
			</cfif>								
			<cfset parentChildCount = arrayLen(dataStruct['#currentParentKey#'].XmlChildren)>			
			<cfset dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset tmpChildNode = dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1]>
			<cfset tmpChildNode.XmlAttributes.LABEL = dataset['LIBELLE_GROUPE_CLIENT'][i]>
			<cfset tmpChildNode.XmlAttributes.NODE_ID = dataset['NODE_ID'][i]>
			<cfset tmpChildNode.XmlAttributes.IDGROUPE_CLIENT = dataset['IDGROUPE_CLIENT'][i]>
			<cfset tmpChildNode.XmlAttributes.LEVEL = dataset['NIVEAU'][i]> 
			<cfset tmpChildNode.XmlAttributes.TYPE_PERIMETRE = dataset['TYPE_PERIMETRE'][i]>
			<cfset tmpChildNode.XmlAttributes.FLAG = dataset['FLAG'][i]>						
			<cfset tmpChildNode.XmlAttributes.BOOL_ORGA = dataset['BOOL_ORGA'][i]>
			<cfset tmpChildNode.XmlAttributes.SELECTABLE = 1>	
			<cfset tmpChildNode.XmlAttributes.TYPE_ORGA = dataset['TYPE_ORGA'][i]>
			<cfset tmpChildNode.XmlAttributes.RAISON_SOCIALE = libelleGroupeClient>
			<cfset structInsert(dataStruct,currentKey,tmpChildNode)>			
		</cfloop>  		
		<cfreturn perimetreXmlDoc>
	</cffunction>	
	
	<cffunction name="searchNodeId" access="private" returntype="numeric">
		<cfargument name="idgroupeclient" type="numeric" required="true">
		<cfargument name="recordset" type="query" required="true">
		
		<cfloop index="i" from="1" to="#recordset.recordcount#">
			<cfif recordset['IDGROUPE_CLIENT'][i] eq idgroupeclient>
				<cfreturn recordset['IDGROUPE_CLIENT'][i]>				
			</cfif>
		</cfloop>		
		<cfreturn  -1>
	</cffunction>
	
	<cffunction name="searchIdGroupe" access="private" returntype="numeric">
		<cfargument name="nodeId" type="numeric" required="true">
		<cfargument name="recordset" type="query" required="true">
		
		<cfloop index="i" from="1" to="#recordset.recordcount#">
			<cfif recordset['IDGROUPE_CLIENT'][i] eq nodeId>
				<cfreturn recordset['IDGROUPE_CLIENT'][i]>				
			</cfif>
		</cfloop>		
		<cfreturn  -1>
	</cffunction>
	
	<cffunction name="ispresent" access="private" returntype="numeric" hint="contains(value : string the value to search\n, colname : string  the column name where to search into\n, data : query the query matrix)">
		<cfargument name="value" required="true" type="string">
		<cfargument name="colname" required="true" type="string">
		<cfargument name="data" required="true" type="query">		
		<cfset count = 0>
		<cfloop query="data">
			<cfif Evaluate("data."&colname) eq  value>				
				<cfset count = count + 1>
			</cfif>
		</cfloop>		
		<cfdump var="#count#">		
		<cfreturn count>
	</cffunction>
	
	
	
	<!---
	<cffunction name="setSessionValue" output="true" access="private" >
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset "session.#cle#"=valeur>
		<cfelse>
			<cfset structInsert(session,cle,valeur)>
		</cfif>
	</cffunction> --->
	<cffunction name="setSessionValue" output="true" access="private">
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset "session.#cle#"=valeur>
		<cfelse>
			<cfset structInsert(session,cle,valeur)>
		</cfif>
	</cffunction>



</cfcomponent>