<cfcomponent name="facade" displayname="facade"> 
	
	<cffunction name="transformDate" access="private" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>
	
	<!--- RETOURNE LES PRODUITS DU THEME --->	 
	<cffunction name="getDetailProduit" access="public" returntype="query">				
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">		
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="idproduit" type="numeric" required="true">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="segment" type="string" required="false">
			
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
				
		<cfset obj.init(perimetre,modeCalcul)>	
		
		<cfif perimetre eq "soustete">
		
				<cfif segment eq "mobile">
						<cfset result=obj.queryData_mobile(numero,idproduit,
								transformDate(datedeb), 
								transformDate(datefin))>
				<cfelse>
						<cfset result=obj.queryData(numero,idproduit,
								transformDate(datedeb), 
								transformDate(datefin))>
				</cfif>					
		<cfelse>
		
				<cfif segment eq "mobile">
						<cfset result=obj.queryData_cat_mobile(numero,idproduit,
								transformDate(datedeb), 
								transformDate(datefin))>
				<cfelse>
				
						<cfset result=obj.queryData_cat(numero,idproduit,
								transformDate(datedeb), 
								transformDate(datefin))>
				</cfif>					
		</cfif>
		
		
		<cfquery name="listeSite" dbtype="query">
			select SITEID,NOM_SITE from result group by SITEID,NOM_SITE order by NOM_SITE asc
		</cfquery>
		
		<cfset qResult  = queryNew("IDPRODUIT_CATALOGUE,PRORATA,SOUS_TETE,SITEID,NOM_SITE,ADRESSE1,ADRESSE2,ZIPCODE,COMMUNE,NUMERO_FACTURE,TYPE_THEME,IDINVENTAIRE_PERIODE,IDSOUS_TETE,COMMENTAIRES,QTE,MONTANT_FINAL,NOMBRE_APPEL,DUREE_APPEL")>
		
		
		<cfset iDsite = "">
		<cfloop query="result">						
			
			<cfif SITEID neq  iDsite>
				<cfset queryAddRow(qResult,1)>
				<cfset querySetCell(qResult,"SITEID",SITEID)>
				<cfset querySetCell(qResult,"NOM_SITE",NOM_SITE)>				 
				<cfset querySetCell(qResult,"TYPE_THEME",TYPE_THEME)>
				<cfset iDsite = SITEID>
			</cfif>	
			
			<cfset queryAddRow(qResult,1)>	
			<cfset querySetCell(qResult,"IDPRODUIT_CATALOGUE",IDPRODUIT_CATALOGUE)>
			<cfset querySetCell(qResult,"PRORATA",PRORATA)>
			<cfset querySetCell(qResult,"SOUS_TETE",SOUS_TETE)>
			<cfset querySetCell(qResult,"SITEID",SITEID)>
			<cfset querySetCell(qResult,"NOM_SITE","")>
			<cfset querySetCell(qResult,"ADRESSE1",ADRESSE1)>
			<cfset querySetCell(qResult,"ADRESSE2",ADRESSE2)>
			<cfset querySetCell(qResult,"ZIPCODE",ZIPCODE)>
			<cfset querySetCell(qResult,"COMMUNE",COMMUNE)>
			<cfset querySetCell(qResult,"NUMERO_FACTURE",NUMERO_FACTURE)>
			<cfset querySetCell(qResult,"TYPE_THEME",TYPE_THEME)>
			<cfset querySetCell(qResult,"IDINVENTAIRE_PERIODE",IDINVENTAIRE_PERIODE)>
			<cfset querySetCell(qResult,"IDSOUS_TETE",IDSOUS_TETE)>
			<cfset querySetCell(qResult,"COMMENTAIRES",COMMENTAIRES)>
			<cfset querySetCell(qResult,"QTE",QTE)>
			<cfset querySetCell(qResult,"MONTANT_FINAL",MONTANT_FINAL)>
			<cfset querySetCell(qResult,"NOMBRE_APPEL",NOMBRE_APPEL)>
			<cfset querySetCell(qResult,"DUREE_APPEL",DUREE_APPEL)>
		</cfloop> 
		
		
		<cfreturn qResult>
	</cffunction>  
	
	
	<cffunction name="getDetailProduitPDF" access="public" returntype="query">				
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">		
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="idproduit" type="numeric" required="true">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="segment" type="string" required="false">
			
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
				
		<cfset obj.init(perimetre,modeCalcul)>	
		
		<cfif perimetre eq "soustete">
		
				<cfif segment eq "mobile">
						<cfset result=obj.queryData_mobile(numero,idproduit,
								transformDate(datedeb), 
								transformDate(datefin))>
				<cfelse>
						<cfset result=obj.queryData(numero,idproduit,
								transformDate(datedeb), 
								transformDate(datefin))>
				</cfif>					
		<cfelse>
		
				<cfif segment eq "mobile">
						<cfset result=obj.queryData_cat_mobile(numero,idproduit,
								transformDate(datedeb), 
								transformDate(datefin))>
				<cfelse>
				
						<cfset result=obj.queryData_cat(numero,idproduit,
								transformDate(datedeb), 
								transformDate(datefin))>
				</cfif>					
		</cfif>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="updateFunction" access="remote" output="true" returntype="numeric" hint="met é jour l'usage de la ligne">
		
		<cfargument name="idsous_tete" type="numeric" required="true">
		<cfargument name="usage" type="string" required="true">
		<cfargument name="segment" type="string" required="true">	
		
		<cfif segment eq "mobile">
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLOBAL.UPDATE_SOUS_TETE">
				<cfprocparam cfsqltype="cF_SQL_INTEGER" type="in" variable="p_idsous_tete" value="#idsous_tete#">		      
				<cfprocparam cfsqltype="cF_SQL_VARCHAR" type="in" variable="p_usage" value="#usage#">
				<cfprocparam cfsqltype="cF_SQL_INTEGER" type="out" variable="p_result">		            
			</cfstoredproc>
		<cfelse>		
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLOBAL.UPDATE_SOUS_TETE">
				<cfprocparam cfsqltype="cF_SQL_INTEGER" type="in" variable="p_idsous_tete" value="#idsous_tete#">		      
				<cfprocparam cfsqltype="cF_SQL_VARCHAR" type="in" variable="p_usage" value="#usage#">					
				<cfprocparam cfsqltype="cF_SQL_INTEGER" type="out" variable="p_result">
			</cfstoredproc>	
		</cfif>
		<cfreturn p_result> 
				
	</cffunction>
	
</cfcomponent> 
                                                                                                                                                                                                                                                                                                 
