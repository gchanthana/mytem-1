<cfcomponent name="test" displayname="test">
	
	<!--- RETOURNE LES PRODUITS DU THEME --->	
	<cffunction name="getDetailProduit" access="public" returntype="query">		
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">		
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="idproduit" type="numeric" required="true">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
				
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
		<cfset obj.init(perimetre,modeCalcul)>	
		<cfif perimetre eq "groupe" >	
			<cfset result=obj.queryData(numero,idproduit,createOdbcDate(datedeb),createOdbcDate(datefin))>		
		<cfelse>
			<cfset result=obj.queryDataS(numero,idproduit,createOdbcDate(datedeb),createOdbcDate(datefin))>
		</cfif>
		<cfreturn result>
	</cffunction>  			
</cfcomponent>