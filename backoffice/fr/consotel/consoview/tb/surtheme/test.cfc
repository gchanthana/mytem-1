 <cfcomponent name="test" displayname="test">
	
	
	<!--- INITIATLISATION 
	<cffunction name="init" access="public">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		
		<cfset obj1=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
		<cfset obj1.init(perimetre,modeCalcul)>
		
		<cfset dataset=obj1.queryData(numero,createOdbcDate(datedeb),createOdbcDate(datefin),modeCalcul)>
		<cfset setSessionValue("RECORDSET",#dataset#)>					
	</cffunction> --->
	
	<!--- RETOURNE LES PRODUITS DU THEME --->	
	<cffunction name="getListeThemesSurTheme" access="public" returntype="query">
		
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">		
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="surtheme" type="string" required="true">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.surtheme.surTheme")>
		<cfset obj.init(perimetre,modeCalcul)>		
		<cfset result=obj.queryData(surtheme,numero,createOdbcDate(datedeb),createOdbcDate(datefin))>		
		<cfreturn result>
	</cffunction>  
 	
	<!--- RETOURNE LES SUR-THEMES CONSOS 
	<cffunction name="getSurThemeConsos" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
		<cfset recordset = session.RECORDSET >
		<cfset result = obj.getSurthemeConsos()>
		<cfreturn result>
	</cffunction>  --->	
	
	<!--- RETOURNE LES  THEMES CONSOS 
	<cffunction name="getConso" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
		<cfset recordset = session.RECORDSET >
		<cfset result = obj.getConso()>
		<cfreturn result>
	</cffunction>  --->	
	
	<!--- RETOURNE LES  THEMES ABOS 
	<cffunction name="getAbo" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
		<cfset recordset = session.RECORDSET >
		<cfset result = obj.getAbo()>
		<cfreturn result>
	</cffunction>  --->	

<!---::::::::::::::::::::::::::::::::::::::::::::::::::PRIVATE::::::::::::::::::::::::::::::::::::::::::::::::::::::::: 

	<cffunction name="setSessionValue" output="true" access="private" >
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset "session.#cle#"=valeur>
		<cfelse>
			<cfset structInsert(session,cle,valeur)>
		</cfif>
	</cffunction> --->


</cfcomponent>