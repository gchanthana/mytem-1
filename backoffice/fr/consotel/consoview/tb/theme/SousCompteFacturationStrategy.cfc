<!--- =========================================================================
Name: SousCompteFacturationStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="SousCompteFacturationStrategy" extends="Strategy" hint="">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="SousCompteFacturationStrategy" displayname="SousCompteFacturationStrategy init()" hint="Initialize the SousCompteFacturationStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, numeric, numeric)" >
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDCompte" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="true" type="numeric" default="" displayname="numeric DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="true" type="numeric" default="" displayname="numeric DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetDetailTheme" datasource="#application.OFFREDSN#">
			SELECT nvl(sum(tpc.qte), 0) AS qte,
			       nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel,
			       sum(nvl(tpc.montant_final,0)) AS montant_final, nvl(tpc.idproduit_client,-1) as idproduit_client, tpc.idproduit_catalogue,
				   pca.libelle_produit, t.theme_libelle, t.type_theme, o.nom
			       from (SELECT * FROM theme_produit) t, 
			       ( 
			         SELECT tp.*, dfa.nombre_appel, dfa.duree_appel, dfa.qte, dfa.montant_final
			         		, dfa.idproduit_client
					 FROM THEME_PRODUIT_CATALOGUE tp, 
			         ( 
			           SELECT SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
			           SUM(df.duree_appel) AS duree_appel, df.idproduit_client,  
			           pc.idproduit_catalogue
			           FROM detail_facture_abo df, produit_client pc, produit_catalogue pca, 
			           inventaire_periode ip, compte_facturation cf, sous_compte sco, sous_tete st
			           WHERE df.idproduit_client=pc.idproduit_client 
			           AND pca.idproduit_catalogue=pc.idproduit_catalogue
					   AND df.idinventaire_periode=ip.idinventaire_periode
			           AND trunc(ip.datedeb,'MM')>=trunc(#DateDebut#,'MM')
					   AND trunc(ip.datedeb,'MM')<=trunc(last_day(#DateFin#),'MM')
			           AND df.idinventaire_periode=ip.idinventaire_periode
				       AND ip.idcompte_facturation=cf.idcompte_facturation
				       AND cf.idcompte_facturation=sco.idcompte_facturation
				       AND df.idsous_tete=st.idsous_tete
			           AND st.idsous_compte=sco.idsous_compte
				       AND sco.idsous_compte=#IDcompte#
					   GROUP BY df.idproduit_client, pc.idproduit_catalogue
			         ) dfa 
			         WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue (+)
			       ) tpc, produit_catalogue pca, operateur o
			WHERE t.idtheme_produit=tpc.idtheme_produit (+) AND t.idtheme_produit=#ID#
			AND tpc.idproduit_catalogue=pca.idproduit_catalogue
			AND pca.operateurid=o.operateurid
			GROUP BY 	nvl(tpc.idproduit_client,-1),tpc.idproduit_catalogue, pca.libelle_produit
						, t.theme_libelle, t.type_theme, o.nom
			having nvl(tpc.idproduit_client,-1)<>-1
			ORDER BY nvl(abs(sum(tpc.montant_final)), 0) desc
		</cfquery>
		<cfreturn qGetDetailTheme>
	</cffunction>
	
	<cffunction name="getLibelle" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfquery name="qGetLibelle" datasource="#application.OFFREDSN#">
			select theme_libelle
			from theme_produit
			where idtheme_produit=#ID#
		</cfquery>
		<cfreturn qGetLibelle.theme_libelle>
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfreturn "Sous Compte">
	</cffunction>
	
	<cffunction name="getNumero" access="public" returntype="string" output="false" displayname="string getNumero(numeric)" >
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
			<cfquery name="qGetCompte" datasource="#application.OFFREDSN#">
				select sous_compte
				from sous_compte sc
				where sc.idsous_compte=#ID#
			</cfquery>
			<cfreturn qGetCompte.sous_compte>
	</cffunction>
	
</cfcomponent>