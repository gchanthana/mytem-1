<!--- =========================================================================
Name: GroupPartielStrategy
Original Author: 
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="GroupPartielStrategy" extends="Strategy" hint="This class implements the algorithm using the Strategy interface.">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="SocieteCompletStrategy" displayname="RefClientFacturationStrategy init()" hint="Initialize the RefClientFacturationStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, numeric, numeric)" >
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDCompte" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="true" type="numeric" default="" displayname="numeric DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="true" type="numeric" default="" displayname="numeric DateFin" hint="Initial value for the DateFinproperty." />
		<!--- Choix de l'exclusion du zip ou non --->
			<cfif trim(session.tbpartiel.zip.value) neq "">
				<cfif trim(session.tbpartiel.zip.exclusion) eq "false">
					<cfset vTempZip="">
				<cfelse>
					<cfset vTempZip=" NOT ">
				</cfif>
				<cfset lzip="">
				<cfset flagInit=0>
				<cfloop list="#session.tbpartiel.zip.value#" index="i">
					<cfif flagInit eq 0>
						<cfset flagInit=1>
						<cfset lzip="'" & "#trim(i)#" & "'">
					<cfelse>
						<cfset lzip=lzip & ",'" & trim(i) & "'">
					</cfif>
				</cfloop>
			</cfif>
		<cfquery name="qGetDetailTheme" datasource="#session.OffreDSN#">
				SELECT 	t.type_theme, t.theme_libelle,t.idtheme_produit, nvl(sum(tpc.qte), 0) AS qte,
			       	nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel,
		    	   	sum(nvl(tpc.montant_final,0)) AS montant_final, tpc.nom, tpc.idproduit_catalogue as idproduit_client, tpc.libelle_produit
						
		    FROM 	theme_produit t, 
		       		( 
		         		SELECT tp.*, dfa.nombre_appel, dfa.duree_appel, dfa.qte, dfa.montant_final, dfa.nom,
		         				dfa.libelle_produit, dfa.idproduit_client
		         		FROM THEME_PRODUIT_CATALOGUE tp, 
		         			( 
                        SELECT	SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
                        			SUM(df.duree_appel) AS duree_appel, df.idproduit_client, pc.idproduit_catalogue,
									 o.nom, pca.libelle_produit
                        FROM 	detail_facture_abo df, produit_client pc, produit_catalogue pca, 
                        		inventaire_periode ip,operateur o
                        		,sous_tete st, sous_compte sco, compte_facturation cf, site_client scl,
								groupe_client_ref_client gcrc
                        WHERE 	df.idproduit_client=pc.idproduit_client 
                        		AND pca.idproduit_catalogue=pc.idproduit_catalogue
                        		AND df.idinventaire_periode=ip.idinventaire_periode
						           	AND trunc(ip.date_emission)<=trunc(last_day(#DateFin#))
										AND trunc(ip.date_emission)>=trunc(#DateDebut#,'MM')
								AND ip.idcompte_facturation=cf.idcompte_facturation
								AND cf.idref_client=gcrc.idref_client
	                        	AND df.idref_client=gcrc.idref_client
	                        	AND gcrc.idgroupe_client=#IDcompte#
								AND st.Idsous_Compte=sco.Idsous_Compte
								AND sco.idcompte_facturation=cf.idcompte_facturation
								AND sco.siteid=scl.siteid										
                        		AND df.idsous_tete=st.Idsous_Tete
								AND pca.operateurid=o.operateurid
			<!--- Choix de l'exclusion de la chaine ou non --->
			<cfif trim(session.tbpartiel.chaine.exclusion) eq "false">
				<cfset vTemp="">
			<cfelse>
				<cfset vTemp=" NOT ">
			</cfif>
			<!--- Choix du type de chaine --->
			<cfif trim(session.tbpartiel.chaine.value) neq ''>
				<cfswitch expression="#session.tbpartiel.chaine.typechaine#">
					<cfcase value="Lignes">
						AND lower(trim(st.sous_tete)) #vTemp# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
					</cfcase>
					<cfcase value="site">
						<cfif trim(session.tbpartiel.chaine.exclusion) eq "false">
							AND (lower(trim(scl.adresse1)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
								OR lower(trim(scl.adresse2)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
								OR lower(trim(scl.commune)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
								OR lower(trim(scl.nom_site)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
							)
						<cfelse>
							AND (lower(trim(scl.adresse1)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
								AND lower(trim(scl.adresse2)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
								AND lower(trim(scl.commune)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
								AND lower(trim(scl.nom_site)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
							)
						</cfif>
					</cfcase>
					<cfcase value="compte">
						AND lower(trim(cf.compte_facturation)) #vTemp# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
					</cfcase>
					<cfcase value="CR">
						AND lower(trim(sco.sous_compte)) #vTemp# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
					</cfcase>
					<cfcase value="fonction">
						AND lower(trim(nvl(st.commentaires,'vide'))) #vTemp# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
					</cfcase>
				</cfswitch>
			</cfif>
			<!--- Choix de l'exclusion du zip ou non --->
            <cfif trim(session.tbpartiel.zip.value) neq ''>
				AND substr(TRIM(scl.zipcode),1,2) #vTempZip# IN (#PreserveSingleQuotes(lzip)#)
			</cfif>
			<!--- Choix de l'exclusion des opérateurs ou non --->
			<cfif trim(session.tbpartiel.operateur.value) neq ''>
				<cfif trim(session.tbpartiel.operateur.exclusion) eq "false">
					<cfset vTemp="">
				<cfelse>
					<cfset vTemp=" NOT ">
				</cfif>
				AND pca.operateurid #vTemp# IN (#session.tbpartiel.operateur.value#)
			</cfif>				
			<cfif trim(session.tbpartiel.chaine.typechaine) neq "fonction" AND trim(session.tbpartiel.fonction.value) neq 0>
				<cfif trim(session.tbpartiel.fonction.exclusion) eq "false">
					<cfset vTemp="">
				<cfelse>
					<cfset vTemp=" NOT ">
				</cfif>
				<cfif trim(session.tbpartiel.fonction.value) eq "">
					AND lower(trim(nvl(st.commentaires,'vide'))) #vTemp# LIKE lower(trim('vide'))
				<cfelse>
					AND lower(trim(nvl(st.commentaires,'vide'))) #vTemp# LIKE lower(trim('%#session.tbpartiel.fonction.value#%'))
				</cfif>
			</cfif>																	
                        GROUP BY df.idproduit_client, pc.idproduit_catalogue,o.nom, pca.libelle_produit
							) dfa 
		         		WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue
			<!--- Choix de l'exclusion des opérateurs ou non --->
			<cfif (trim(session.tbpartiel.theme.abo neq "") OR trim(session.tbpartiel.theme.conso) neq "")>
				<cfif trim(session.tbpartiel.theme.exclusion) eq "false">
					<cfset vTemp="">
				<cfelse>
					<cfset vTemp=" NOT ">
				</cfif>
				<cfset ltheme="">
				<cfloop list="#session.tbpartiel.theme.abo#" index="i">
					<cfset ltheme=ListAppend(ltheme,i)>
				</cfloop>
				<cfloop list="#session.tbpartiel.theme.conso#" index="i">
					<cfset ltheme=ListAppend(ltheme,i)>
				</cfloop>
				AND tp.idtheme_produit #vTemp# IN (#ltheme#)
			</cfif>		
		       		) tpc
			WHERE t.idtheme_produit=tpc.idtheme_produit
			AND t.idtheme_produit=#ID#
			GROUP BY t.type_theme,t.theme_libelle,t.ordre_affichage,t.idtheme_produit, tpc.nom, 
					tpc.idproduit_catalogue,tpc.libelle_produit
			ORDER BY nvl(abs(sum(tpc.montant_final)), 0) desc
		</cfquery>
		<cfreturn qGetDetailTheme>
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfreturn "Groupe">
	</cffunction>
	
	<cffunction name="getLibelle" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfquery name="qGetLibelle" datasource="#session.OffreDSN#">
			select theme_libelle
			from theme_produit
			where idtheme_produit=#ID#
		</cfquery>
		<cfreturn qGetLibelle.theme_libelle>
	</cffunction>
	
	<cffunction name="getNumero" access="public" returntype="string" output="false" displayname="string getNumero(numeric)" >
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
			<cfquery name="qGetCompte" datasource="#session.OffreDSN#">
				select libelle_groupe_client
				from groupe_client gc
				where gc.idgroupe_client=#ID#
			</cfquery>
			<cfreturn qGetCompte.libelle_groupe_client>
	</cffunction>

</cfcomponent>