<cfcomponent name="GroupeMaitreRechercheRapide" displayname="GroupeMaitreRechercheRapide" hint="Recherche rapide pour un groupe maitre">

	<cffunction name="init" access="public" output="false" returntype="GroupeCompletStrategy" displayname="GroupeCompletStrategy init()" hint="Initialize the GroupeCompletStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
	
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, numeric, string)">		
		<cfargument name="idGroupe" required="true" type="numeric" default="" displayname="numeric ID" 				/>
		<cfargument name="idCliche" required="true" type="numeric" default="" displayname="numeric ID" 				/>
		<cfargument name="chaine"   required="true" type="string"  default="" displayname="la chaine de recherche" 	/>
		<cfargument name="niveau"   required="true" type="string"  default="" displayname="la chaine de recherche" 	/>
		<cfargument name="iddatedeb" 	type="numeric" 	required="true">
		<cfargument name="iddatefin"   type="numeric" 	required="true">
			
			<!--- 
		
					PROCEDURE search_liste_lignes_nodes_v2 (p_chaine               IN VARCHAR2,
                                          p_idorganisation              IN INTEGER,
                                          p_idperiode_deb        IN INTEGER,
                                          p_idperiode_fin        IN INTEGER,
                                          p_retour              OUT SYS_REFCURSOR)
		 	--->			
			
			
			
			<cfstoredproc datasource="E0" procedure="pkg_M311.SEARCH_LISTE_LIGNES_NODES_V2">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_chaine" 		value="#chaine#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idorganisation" 	value="#idGroupe#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idperiode_deb" 	value="#iddatedeb#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idperiode_fin" 	value="#iddatefin#"/>
				<cfprocresult name="p_result"/>
			</cfstoredproc>
			
		<cfreturn p_result/>
	</cffunction>	
	
	<cffunction name="getLignes" access="public" returntype="query" output="false" displayname="query getData(numeric, numeric)">
		<cfargument name="idGroupe" required="true" type="numeric" default="" displayname="numeric ID" />
		<cfargument name="idCliche" required="true" type="numeric" default="" displayname="numeric ID" />
		<cfargument name="niveau"   required="true" type="string"  default="" displayname="niveau orga"/>
		<cfargument name="iddatedeb" 	type="numeric" 	required="true">
		<cfargument name="iddatefin"   type="numeric" 	required="true">
		 	
			<cfstoredproc datasource="E0" procedure="pkg_M311.SEARCH_NODES_SOUS_TETES_v2">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idGroupe#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_niveau" 			value="#niveau#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcliche" 		value="#idCliche#"/>
				<cfprocresult name="p_result"/>
			</cfstoredproc>
			

		<cfreturn p_result/>
	</cffunction>
	
</cfcomponent>