<cfset biServer=APPLICATION.BI_SERVICE_URL>

<cfset locale = SESSION.USER.GLOBALIZATION>

<cfset ArrayOfParamNameValues=ArrayNew(1)>
<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="G_IDMASTER">
<cfset t=ArrayNew(1)>
<cfset t[1]=SESSION.PERIMETRE.IDRACINE_MASTER>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[1]=ParamNameValue>


<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="G_IDRACINE">
<cfset t=ArrayNew(1)>
<cfset t[1]=SESSION.PERIMETRE.ID_GROUPE>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[2]=ParamNameValue>


<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="G_IDORGA">
<cfset t=ArrayNew(1)>
<cfset t[1]=SESSION.PERIMETRE.SELECTED_ID_ORGA>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[3]=ParamNameValue>

<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="G_IDPERIMETRE">
<cfset t=ArrayNew(1)>   
<cfset t[1]=FORM.IDENTIFIANT>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[4]=ParamNameValue>

<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="G_IDCLICHE">
<cfset t=ArrayNew(1)>
<cfset t[1]=SESSION.PERIMETRE.SELECTED_ID_CLICHE>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[5]=ParamNameValue>

<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="U_IDPERIOD_DEB">
<cfset t=ArrayNew(1)>
<cfset t[1]=SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[6]=ParamNameValue>

<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="U_IDPERIOD_FIN">
<cfset t=ArrayNew(1)>
<cfset t[1]=SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[7]=ParamNameValue>

<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="G_NIVEAU">
<cfset t=ArrayNew(1)>
<cfset t[1]=SESSION.PERIMETRE.SELECTED_NIVEAU>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[8]=ParamNameValue>

<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="G_IDNIVEAU">
<cfset t=ArrayNew(1)>
<cfset t[1]=SESSION.PERIMETRE.SELECTED_IDNIVEAU>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[9]=ParamNameValue>

<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="G_LANGUE_PAYS">
<cfset t=ArrayNew(1)>
<cfset t[1]=locale>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[10]=ParamNameValue>	
	
<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="U_ENTIER1">
<cfset t=ArrayNew(1)>
<cfset t[1]=FORM.IDSEGMENT>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[11]=ParamNameValue>

<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="U_ENTIER2">
<cfset t=ArrayNew(1)>
<cfset t[1]=SESSION.DATES.LASTPFGPIDPERIODE>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[12]=ParamNameValue>	


<cfset myParamReportRequest=structNew()>

<cfif FORM.IDSEGMENT GT 0>
	<cfif  compareNoCase(FORM.PERIMETRE,"SOUSTETE") EQ  0>
		<cfset myParamReportRequest.reportAbsolutePath="/consoview/M312/TB-SEGMENT-REPORT-SOUSTETE/TB-SEGMENT-REPORT-SOUSTETE.xdo">
	<cfelse>
		<cfset myParamReportRequest.reportAbsolutePath="/consoview/M312/TB-SEGMENT-REPORT/TB-SEGMENT-REPORT.xdo">
	</cfif>
	<cfset myParamReportRequest.attributeTemplate="SEGMENT">
	
	<cfelse>
	
	<cfif  compareNoCase(FORM.PERIMETRE,"SOUSTETE") EQ  0>
		<cfset myParamReportRequest.reportAbsolutePath="/consoview/M312/TB-REPORT-SOUSTETE/TB-REPORT-SOUSTETE.xdo">
	<cfelse>
		<cfset myParamReportRequest.reportAbsolutePath="/consoview/M312/TB-REPORT/TB-REPORT.xdo">
	</cfif>
	<cfset myParamReportRequest.attributeTemplate="COMPLET">
</cfif>

<cfset myParamReportRequest.attributeLocale=locale>
<cfset myParamReportRequest.attributeFormat=LCase(FORM.FORMAT)>
<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
<cfset myParamReportParameters=structNew()>
<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>

<cfset myParamReportParameters.userID="consoview">
<cfset myParamReportParameters.password="public">
<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
<cfset reporting=createObject("component","fr.consotel.consoview.api.Reporting")>

<cfset filename = "TB-" & UCase(FORM.TB) & "-" & FORM.PERIMETRE & "-" &
		replace(session.perimetre.raison_sociale,' ','_',"all") & reporting.getFormatFileExtension(FORM.FORMAT)>
<cfheader name="Content-Disposition" value="attachment;filename=#filename#" charset="utf-8">
<cfif compareNoCase(trim(FORM.FORMAT),"csv") eq 0>
	<cfcontent type="text/csv; charset=utf-8" variable="#resultRunReport.getReportBytes()#">
	<cfelse>
	<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">		
</cfif>