<!--- =========================================================================
Classe: GroupeSousComptePartielStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GroupeSousComptePartielStrategy" hint="" extends="AbstractStrategy">
   <cffunction name="init" access="public" output="false" returntype="GroupePartielStrategy" displayname="RefClientFacturationStrategy init()" hint="Initialize the RefClientFacturationStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getProduit" access="public" returntype="string" output="false" displayname="string getProduit(numeric)" >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qGetDetailTheme" datasource="#session.OffreDSN#">
			SELECT th.*, pcl.libelle_produit
			FROM theme_produit th, theme_produit_catalogue tpc, produit_catalogue pca, produit_client pcl
			WHERE th.idtheme_produit=tpc.idtheme_produit
			AND tpc.idproduit_catalogue=pca.idproduit_catalogue
			AND pca.idproduit_catalogue=pcl.idproduit_catalogue
			AND pcl.idproduit_catalogue=#IDproduit#
		</cfquery>
		<cfreturn qGetDetailTheme.libelle_produit>
	</cffunction>
	
	<cffunction name="getOperateur" access="public" returntype="string" output="false" displayname="string getProduit(numeric)" >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qgetOperateur" datasource="#session.OFFREDSN#">
			SELECT o.nom
			FROM produit_catalogue pca, operateur o
			WHERE pca.idproduit_catalogue=#IDproduit#
			AND pca.operateurID=o.operateurID
		</cfquery>
		<cfreturn qgetOperateur.nom>
	</cffunction>
	
	<cffunction name="getTypeProduit" access="public" returntype="string" output="false" displayname="string getTypeProduit(numeric)" >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qGetDetailTheme" datasource="#session.OffreDSN#">
			SELECT th.*, pcl.libelle_produit
			FROM theme_produit th, theme_produit_catalogue tpc, produit_catalogue pca, produit_client pcl
			WHERE th.idtheme_produit=tpc.idtheme_produit
			AND tpc.idproduit_catalogue=pca.idproduit_catalogue
			AND pca.idproduit_catalogue=pcl.idproduit_catalogue
			AND pcl.idproduit_catalogue=#IDproduit#
		</cfquery>
		<cfreturn qGetDetailTheme.theme_libelle>
	</cffunction>
	
	<cffunction name="getCompte" access="public" returntype="string" output="false" displayname="string getCompte(numeric)" >
		<cfargument name="IDcompte" required="false" type="numeric" default="" displayname="numeric IDcompte" hint="Initial value for the IDcompteproperty." />
		<cfquery name="qGetCompte" datasource="#session.OffreDSN#">
			select libelle_groupe_client
			from groupe_client gc
			where gc.idgroupe_client=#IDcompte#
		</cfquery>
		<cfreturn qGetCompte.libelle_groupe_client>
	</cffunction>
	
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, date, date)" >
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfargument name="DateDebut" required="true" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="true" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<!--- Choix de l'exclusion du zip ou non --->
			<cfif trim(session.tbpartiel.zip.value) neq "">
				<cfif trim(session.tbpartiel.zip.exclusion) eq "false">
					<cfset vTempZip="">
				<cfelse>
					<cfset vTempZip=" NOT ">
				</cfif>
				<cfset lzip="">
				<cfset flagInit=0>
				<cfloop list="#session.tbpartiel.zip.value#" index="i">
					<cfif flagInit eq 0>
						<cfset flagInit=1>
						<cfset lzip="'" & "#trim(i)#" & "'">
					<cfelse>
						<cfset lzip=lzip & ",'" & trim(i) & "'">
					</cfif>
				</cfloop>
			</cfif>
			
			<!--- Choix de l'exclusion de la chaine ou non --->
			<cfif trim(session.tbpartiel.chaine.exclusion) eq "false">
				<cfset vTempchaine="">
			<cfelse>
				<cfset vTempchaine=" NOT ">
			</cfif>

			<cfif trim(session.tbpartiel.fonction.exclusion) eq "false">
				<cfset vTempfonction="=">
			<cfelse>
				<cfset vTempfonction="<>">
			</cfif>
			
			<cfif trim(session.tbpartiel.theme.exclusion) eq "false">
				<cfset vTemptheme="">
			<cfelse>
				<cfset vTemptheme=" NOT ">
			</cfif>

			<cfif trim(session.tbpartiel.operateur.exclusion) eq "false">
				<cfset vTempoperateur="">
			<cfelse>
				<cfset vTempoperateur=" NOT ">
			</cfif>
			

		<cfquery name="qGetDetailProduit" datasource="#session.OffreDSN#">
			SELECT 	t.type_theme, t.theme_libelle,t.idtheme_produit, nvl(sum(tpc.qte), 0) AS qte,
			       	nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel,
		    	   	sum(nvl(tpc.montant_final,0)) AS montant_final, tpc.nom, tpc.idproduit_catalogue, tpc.libelle_produit,
						tpc.idproduit_client, st.sous_tete, sc.siteID,
		           	sc.nom_site, sc.adresse1, sc.adresse2,sc.zipcode, sc.commune, ip.numero_facture, tpc.idinventaire_periode,st.idsous_tete
		    FROM 	theme_produit t, 
		       		( 
		         		SELECT tp.*, dfa.nombre_appel, dfa.duree_appel, dfa.qte, dfa.montant_final, dfa.nom,
		         				dfa.libelle_produit, dfa.idproduit_client, dfa.idsous_tete, dfa.idinventaire_periode
		         		FROM THEME_PRODUIT_CATALOGUE tp, 
		         			( 
                        SELECT	SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
                        			SUM(df.duree_appel) AS duree_appel, df.idproduit_client, pc.idproduit_catalogue,
											o.nom, pca.libelle_produit, st.idsous_tete, ip.idinventaire_periode
                        FROM 	detail_facture_abo df, 
										operateur o, 
										produit_client pc, 
										(
											select * from produit_catalogue 
											<!--- Choix de l'exclusion des opÃ©rateurs ou non --->
											<cfif trim(session.tbpartiel.operateur.value) neq ''>
												where operateurid #vTempoperateur# IN (#session.tbpartiel.operateur.value#)
											</cfif>				
										) pca, 
										(	SELECT a.date_emission,a.idinventaire_periode
											FROM inventaire_periode a, compte_facturation b, groupe_client_ref_client c, sous_compte d
											WHERE a.idcompte_facturation=b.idcompte_facturation
													AND b.idcompte_facturation=d.idcompte_facturation
													AND d.idsous_compte=c.idref_client
													AND c.idgroupe_client=#ID#
				                         	AND trunc(a.date_emission)<=trunc(last_day(#DateFin#))
													AND trunc(a.date_emission)>=trunc(#DateDebut#,'MM')
													<!--- Choix du type de chaine --->
													<cfif trim(session.tbpartiel.chaine.value) neq ''>
														<cfswitch expression="#session.tbpartiel.chaine.typechaine#">
															<cfcase value="compte">
																AND lower(trim(a.compte_facturation)) #vTempchaine# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
															</cfcase>
														</cfswitch>
													</cfif>
											GROUP BY a.date_emission,a.idinventaire_periode
									   ) ip,
										(	SELECT f.*, i.*
											FROM sous_tete f, sous_compte g, groupe_client_ref_client h, site_client i
											WHERE f.idsous_compte=g.idsous_compte 
													AND g.idsous_compte=h.idref_client
													and g.siteid=i.siteid
													AND h.idgroupe_client=#ID#
													<!--- Choix de l'exclusion du zip ou non --->
										         <cfif trim(session.tbpartiel.zip.value) neq ''>
														AND substr(TRIM(i.zipcode),1,2) #vTempZip# IN (#PreserveSingleQuotes(lzip)#)
													</cfif>
													<cfif trim(session.tbpartiel.chaine.value) neq ''>
														<cfswitch expression="#session.tbpartiel.chaine.typechaine#">
	
															<cfcase value="Lignes">
																AND lower(trim(f.sous_tete)) #vTempchaine# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
															</cfcase>
															<cfcase value="site">
																<cfif trim(session.tbpartiel.chaine.exclusion) eq "false">
																	AND (lower(trim(i.adresse1)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																		OR lower(trim(i.adresse2)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																		OR lower(trim(i.commune)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																		OR lower(trim(i.nom_site)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																	)
																<cfelse>
																		AND (lower(trim(i.adresse1)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																		AND lower(trim(i.adresse2)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																		AND lower(trim(i.commune)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																		AND lower(trim(i.nom_site)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																	)
																</cfif>
															</cfcase>
															<cfcase value="CR">
																AND lower(trim(g.sous_compte)) #vTempchaine# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
															</cfcase>
															<cfcase value="fonction">
																AND lower(trim(nvl(f.commentaires,'vide'))) #vTempchaine# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
															</cfcase>
														</cfswitch>
													</cfif>
													
													<cfif trim(session.tbpartiel.chaine.typechaine) neq "fonction" AND trim(session.tbpartiel.fonction.value) neq 0>
														<cfloop list="#session.tbpartiel.fonction.value#" index="ifonction">
															<cfif trim(session.tbpartiel.fonction.value) eq "">
																AND lower(trim(nvl(f.commentaires,'vide'))) #vTempfonction# lower(trim('vide'))
															<cfelse>
																AND lower(trim(nvl(f.commentaires,'vide'))) #vTempfonction# lower(trim('#ifonction#'))
															</cfif>
														</cfloop>
													</cfif>
										) st
                       	WHERE df.idproduit_client=pc.idproduit_client 
                        		AND pca.idproduit_catalogue=pc.idproduit_catalogue
										AND pca.operateurid=o.operateurid
  	                     		AND df.idinventaire_periode=ip.idinventaire_periode
                        		AND df.idsous_tete=st.Idsous_Tete
	                        	AND pca.idproduit_catalogue=#IDproduit#
                        GROUP BY df.idproduit_client, pc.idproduit_catalogue,
									 		o.nom, pca.libelle_produit, st.idsous_tete, ip.idinventaire_periode
							) dfa 
		         		WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue
			<!--- Choix de l'exclusion des opÃ©rateurs ou non --->
			<cfif (trim(session.tbpartiel.theme.abo neq "") OR trim(session.tbpartiel.theme.conso) neq "")>
				<cfif trim(session.tbpartiel.theme.exclusion) eq "false">
					<cfset vTemp="">
				<cfelse>
					<cfset vTemp=" NOT ">
				</cfif>
				<cfset ltheme="">
				<cfloop list="#session.tbpartiel.theme.abo#" index="i">
					<cfset ltheme=ListAppend(ltheme,i)>
				</cfloop>
				<cfloop list="#session.tbpartiel.theme.conso#" index="i">
					<cfset ltheme=ListAppend(ltheme,i)>
				</cfloop>
				AND tp.idtheme_produit #vTemp# IN (#ltheme#)
			</cfif>		
		       		) tpc, sous_tete st, sous_compte sco, site_client sc, inventaire_periode ip
			WHERE t.idtheme_produit=tpc.idtheme_produit
			AND tpc.idsous_tete=st.idsous_tete
			AND st.idsous_compte=sco.idsous_compte
			AND sco.siteID=sc.siteID
			AND tpc.idinventaire_periode=ip.idinventaire_periode
			GROUP BY t.type_theme,t.theme_libelle,t.ordre_affichage,t.idtheme_produit, tpc.nom, 
					tpc.idproduit_catalogue,tpc.libelle_produit,tpc.idproduit_client, st.sous_tete, sc.siteID,
		           	sc.nom_site, sc.adresse1, sc.adresse2,sc.zipcode, sc.commune, ip.numero_facture, tpc.idinventaire_periode,st.idsous_tete
			ORDER BY montant_final desc, siteID,nom_site, adresse1, sous_tete
		</cfquery>
		<!--- <cfquery name="qGetDetailProduit" datasource="#session.OffreDSN#">
			SELECT 	t.type_theme, t.theme_libelle,t.idtheme_produit, nvl(sum(tpc.qte), 0) AS qte,
			       	nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel,
		    	   	sum(nvl(tpc.montant_final,0)) AS montant_final, tpc.nom, tpc.idproduit_catalogue, tpc.libelle_produit,
						tpc.idproduit_client, st.sous_tete, sc.siteID,
		           	sc.nom_site, sc.adresse1, sc.adresse2,sc.zipcode, sc.commune, ip.numero_facture, tpc.idinventaire_periode,st.idsous_tete
		    FROM 	theme_produit t, 
		       		( 
		         		SELECT tp.*, dfa.nombre_appel, dfa.duree_appel, dfa.qte, dfa.montant_final, dfa.nom,
		         				dfa.libelle_produit, dfa.idproduit_client, dfa.idsous_tete, dfa.idinventaire_periode
		         		FROM THEME_PRODUIT_CATALOGUE tp, 
		         			( 
                        SELECT	SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
                        			SUM(df.duree_appel) AS duree_appel, df.idproduit_client, pc.idproduit_catalogue,
									 		o.nom, pca.libelle_produit, st.idsous_tete, ip.idinventaire_periode
                        FROM 	detail_facture_abo df, produit_client pc, 
										(select * from produit_catalogue where idproduit_catalogue=#IDproduit#) pca, 
                        		operateur o, sous_compte sco,
										(	SELECT c.*,a.*, b.compte_facturation
											FROM inventaire_periode a, compte_facturation b, groupe_client_ref_client c, sous_compte d
											WHERE a.idcompte_facturation=b.idcompte_facturation
													AND b.idcompte_facturation=d.idcompte_facturation
													AND d.idsous_compte=c.idref_client
													AND c.idgroupe_client=#ID#
				                         	AND trunc(a.date_emission)<=trunc(last_day(#DateFin#))
													AND trunc(a.date_emission)>=trunc(#DateDebut#,'MM')
									   ) ip,
										(	SELECT f.*, i.*
											FROM sous_tete f, sous_compte g, groupe_client_ref_client h, site_client i
											WHERE f.idsous_compte=g.idsous_compte 
													AND g.idsous_compte=h.idref_client
													and g.siteid=i.siteid
													AND h.idgroupe_client=#ID#
										) st
                        WHERE df.idproduit_client=pc.idproduit_client 
	                       		AND pca.idproduit_catalogue=pc.idproduit_catalogue
										AND pca.operateurid=o.operateurid
                        		AND df.idsous_tete=st.Idsous_Tete
                        		AND df.idinventaire_periode=ip.idinventaire_periode
										AND st.Idsous_Compte=sco.Idsous_Compte
	                        	
										<!--- Choix de l'exclusion de la chaine ou non --->
										<cfif trim(session.tbpartiel.chaine.exclusion) eq "false">
											<cfset vTemp="">
										<cfelse>
											<cfset vTemp=" NOT ">
										</cfif>
										<!--- Choix du type de chaine --->
										<cfif trim(session.tbpartiel.chaine.value) neq ''>
											<cfswitch expression="#session.tbpartiel.chaine.typechaine#">
												<cfcase value="Lignes">
													AND lower(trim(st.sous_tete)) #vTemp# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
												</cfcase>
												<cfcase value="site">
													<cfif trim(session.tbpartiel.chaine.exclusion) eq "false">
														AND (lower(trim(st.adresse1)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
															OR lower(trim(st.adresse2)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
															OR lower(trim(st.commune)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
															OR lower(trim(st.nom_site)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
														)
													<cfelse>
															AND (lower(trim(st.adresse1)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
															AND lower(trim(st.adresse2)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
															AND lower(trim(st.commune)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
															AND lower(trim(st.nom_site)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
														)
													</cfif>
												</cfcase>
												<cfcase value="compte">
													AND lower(trim(cf.compte_facturation)) #vTemp# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
												</cfcase>
												<cfcase value="CR">
													AND lower(trim(sco.sous_compte)) #vTemp# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
												</cfcase>
												<cfcase value="fonction">
													AND lower(trim(nvl(st.commentaires,'vide'))) #vTemp# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
												</cfcase>
											</cfswitch>
										</cfif>
										<!--- Choix de l'exclusion du zip ou non --->
							            <cfif trim(session.tbpartiel.zip.value) neq ''>
											AND substr(TRIM(st.zipcode),1,2) #vTempZip# IN (#PreserveSingleQuotes(lzip)#)
										</cfif>
										<!--- Choix de l'exclusion des opÃ©rateurs ou non --->
										<cfif trim(session.tbpartiel.operateur.value) neq ''>
											<cfif trim(session.tbpartiel.operateur.exclusion) eq "false">
												<cfset vTemp="">
											<cfelse>
												<cfset vTemp=" NOT ">
											</cfif>
											AND pca.operateurid #vTemp# IN (#session.tbpartiel.operateur.value#)
										</cfif>				
										<cfif trim(session.tbpartiel.chaine.typechaine) neq "fonction" AND trim(session.tbpartiel.fonction.value) neq 0>
											<cfif trim(session.tbpartiel.fonction.exclusion) eq "false">
												<cfset vTemp="">
											<cfelse>
												<cfset vTemp=" NOT ">
											</cfif>
											<cfif trim(session.tbpartiel.fonction.value) eq "">
												AND lower(trim(nvl(st.commentaires,'vide'))) #vTemp# LIKE lower(trim('vide'))
											<cfelse>
												AND lower(trim(nvl(st.commentaires,'vide'))) #vTemp# LIKE lower(trim('%#session.tbpartiel.fonction.value#%'))
											</cfif>
										</cfif>																	
                        GROUP BY df.idproduit_client, pc.idproduit_catalogue,
									 		o.nom, pca.libelle_produit, st.idsous_tete, ip.idinventaire_periode
							) dfa 
		         		WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue
									<!--- Choix de l'exclusion des opÃ©rateurs ou non --->
									<cfif (trim(session.tbpartiel.theme.abo neq "") OR trim(session.tbpartiel.theme.conso) neq "")>
										<cfif trim(session.tbpartiel.theme.exclusion) eq "false">
											<cfset vTemp="">
										<cfelse>
											<cfset vTemp=" NOT ">
										</cfif>
										<cfset ltheme="">
										<cfloop list="#session.tbpartiel.theme.abo#" index="i">
											<cfset ltheme=ListAppend(ltheme,i)>
										</cfloop>
										<cfloop list="#session.tbpartiel.theme.conso#" index="i">
											<cfset ltheme=ListAppend(ltheme,i)>
										</cfloop>
										AND tp.idtheme_produit #vTemp# IN (#ltheme#)
									</cfif>		
		       		) tpc, sous_tete st, sous_compte sco, site_client sc, inventaire_periode ip
			WHERE t.idtheme_produit=tpc.idtheme_produit
					AND tpc.idsous_tete=st.idsous_tete
					AND st.idsous_compte=sco.idsous_compte
					AND sco.siteID=sc.siteID
					AND tpc.idinventaire_periode=ip.idinventaire_periode
			GROUP BY t.type_theme,t.theme_libelle,t.ordre_affichage,t.idtheme_produit, tpc.nom, 
					tpc.idproduit_catalogue,tpc.libelle_produit,tpc.idproduit_client, st.sous_tete, sc.siteID,
		         sc.nom_site, sc.adresse1, sc.adresse2,sc.zipcode, sc.commune, ip.numero_facture, tpc.idinventaire_periode,st.idsous_tete
			ORDER BY montant_final desc, siteID,nom_site, adresse1, sous_tete
		</cfquery> --->
		<cfreturn qGetDetailProduit>
	</cffunction>

	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfreturn "Groupe de Sous Compte">
	</cffunction>
</cfcomponent>