<!--- =========================================================================
Classe: GroupeLigneCompletStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GroupeLigneCompletStrategy" hint=""  extends="Strategy" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GroupeLigneCompletStrategy" hint="Remplace le constructeur de GroupeLigneCompletStrategy.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<cffunction name="getData" access="public" returntype="query" output="false" hint="Raménes les données principales pour le dernier niveau du tableau de bord (Détails des produits)." >
		<cfargument name="ID" 		 required="true" type="numeric" default="" displayname="numeric ID" 		hint="Initial value for the IDproperty."/>
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit"  hint="Initial value for the IDproduitproperty."/>
		<cfargument name="datedeb" 	 required="true" type="string"  default="" displayname="date DateDebut"	 	hint="Initial value for the DateDebutproperty."/>
		<cfargument name="datefin" 	 required="true" type="string"  default="" displayname="date DateFin" 		hint="Initial value for the DateFinproperty."/>
		
			<cfset periE0 	= CreateObject("component","fr.consotel.consoview.M312.PerimetreE0")>
			<cfset p_result = periE0.executQuery(3, IDproduit, 0, "", 
												session.perimetre.IDRACINE_MASTER,
												session.perimetre.ID_GROUPE,
												ID,
												datedeb,
												datefin,
												"GroupeLigne")>
		
		<cfreturn p_result/> 
	</cffunction>
	
	<cffunction name="getData_cat" access="public" returntype="query" output="false" hint="Raménes les données principales pour le dernier niveau du tableau de bord (Détails des produits)." >
		<cfargument name="ID" 		 required="true" type="numeric" default="" displayname="numeric ID" 		hint="Initial value for the IDproperty."/>
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit"  hint="Initial value for the IDproduitproperty."/>
		<cfargument name="datedeb" 	 required="true" type="string"  default="" displayname="date DateDebut"	 	hint="Initial value for the DateDebutproperty."/>
		<cfargument name="datefin" 	 required="true" type="string"  default="" displayname="date DateFin" 		hint="Initial value for the DateFinproperty."/>
		
			<cfset periE0 	= CreateObject("component","fr.consotel.consoview.M312.PerimetreE0")>
			<cfset p_result = periE0.executQuery(4, IDproduit, 0, "", 
												session.perimetre.IDRACINE_MASTER,
												session.perimetre.ID_GROUPE,
												ID,
												datedeb,
												datefin,
												"GroupeLigne")>	
		<cfreturn p_result/> 
	</cffunction>
	
	<cffunction name="getData_mobile" access="public" returntype="query" output="false" hint="Raménes les données principales pour le dernier niveau du tableau de bord (Détails des produits)." >
		<cfargument name="ID" 		 required="true" type="numeric" default="" displayname="numeric ID" 		hint="Initial value for the IDproperty."/>
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit"  hint="Initial value for the IDproduitproperty."/>
		<cfargument name="datedeb" 	 required="true" type="string"  default="" displayname="date DateDebut"	 	hint="Initial value for the DateDebutproperty."/>
		<cfargument name="datefin" 	 required="true" type="string"  default="" displayname="date DateFin" 		hint="Initial value for the DateFinproperty."/>
		
			<cfset periE0 	= CreateObject("component","fr.consotel.consoview.M312.PerimetreE0")>
			<cfset p_result = periE0.executQuery(5, IDproduit, 0, "", 
												session.perimetre.IDRACINE_MASTER,
												session.perimetre.ID_GROUPE,
												ID,
												datedeb,
												datefin,
												"GroupeLigne")>
		<cfreturn p_result/> 
	</cffunction>
	
	<cffunction name="getData_cat_mobile" access="public" returntype="query" output="false" hint="Raménes les données principales pour le dernier niveau du tableau de bord (Détails des produits)." >
		<cfargument name="ID" 		 required="true" type="numeric" default="" displayname="numeric ID" 		hint="Initial value for the IDproperty."/>
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit"  hint="Initial value for the IDproduitproperty."/>
		<cfargument name="datedeb" 	 required="true" type="string"  default="" displayname="date DateDebut"	 	hint="Initial value for the DateDebutproperty."/>
		<cfargument name="datefin" 	 required="true" type="string"  default="" displayname="date DateFin" 		hint="Initial value for the DateFinproperty."/>
		
			<cfset periE0 	= CreateObject("component","fr.consotel.consoview.M312.PerimetreE0")>
			<cfset p_result = periE0.executQuery(6, IDproduit, 0, "", 
												session.perimetre.IDRACINE_MASTER,
												session.perimetre.ID_GROUPE,
												ID,
												datedeb,
												datefin,
												"GroupeLigne")>
		<cfreturn p_result/> 
	</cffunction>	
	
	<cffunction name="getTypeProduit" access="public" returntype="string" output="false" hint="Retourne le type du produit passé en paramétre." >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	
	<cffunction name="getProduit" access="public" returntype="string" output="false" hint="Retourne le libellé du produit." >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	
	<cffunction name="getOperateur" access="public" returntype="string" output="false" hint="Retourne le nom de l'opérateur pour le produit concerné." >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	
	<cffunction name="getCompte" access="public" returntype="string" output="false" hint="Retourne le libéllé du périmétre." >
		<cfargument name="IDcompte" required="false" type="numeric" default="" displayname="numeric IDcompte" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" hint="Retourne l'appellation du périmétre (Ex: société = compte Hiérarchique)." >
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>