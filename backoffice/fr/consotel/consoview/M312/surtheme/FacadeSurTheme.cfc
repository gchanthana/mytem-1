 <cfcomponent name="FacadeSurTheme" displayname="FacadeSurTheme" extends="fr.consotel.consoview.M312.surtheme.facade">
	
	<cffunction name="getListeThemesSurThemeByID" access="remote" returntype="query">
		<cfargument name="perimetre" 	type="string" 	required="true">
		<cfargument name="modeCalcul" 	type="string" 	required="true">		
		<cfargument name="numero" 		type="numeric" 	required="true">
		<cfargument name="idsurtheme" 	type="numeric" 	required="true">
		<cfargument name="datedeb" 		type="string" 	required="true">
		<cfargument name="datefin" 		type="string" 	required="true">
		<cfargument name="isReport" 	type="numeric" 	required="false" default="0">
			
			<cfset periE0  = CreateObject("component","fr.consotel.consoview.M312.PerimetreE02")>
			
			<cfif isReport EQ 0>
				<cfset dataset = periE0.executQueryByID(2, 0, 0, idsurtheme,
													session.perimetre.IDRACINE_MASTER,
													session.perimetre.ID_GROUPE,
													numero,
													transformDate(datedeb),
													transformDate(datefin),
													perimetre)>
			<cfelse>									
				<cfset dataset = periE0.executQueryByID(2, 0, 0, idsurtheme,
													session.perimetre.IDRACINE_MASTER,
													session.perimetre.ID_GROUPE,
													numero,
													datedeb,
													datefin,
													perimetre)>
			</cfif>				
		<cfreturn dataset>
	</cffunction>

</cfcomponent>