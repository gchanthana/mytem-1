<!--- =========================================================================
Classe: GroupeSousCompteCompletStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GroupeSousCompteCompletStrategy" hint="" extends="Strategy">
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GroupeSousCompteCompletStrategy" hint="Remplace le constructeur de GroupeSousCompteCompletStrategy.">
		<cfscript> 
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<cffunction name="getData" access="public" returntype="query" output="false" >
		<cfargument name="surtheme" required="true" type="string" default="" />
		<cfargument name="IDCompte" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="numeric DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="numeric DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetDetailFactureAbo" datasource="#session.OFFREDSN#">
				SELECT nvl(sum(tpc.qte), 0) AS qte,
			       nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel,
			       sum(nvl(tpc.montant_final,0)) AS montant_final, t.idtheme_produit,
				   t.theme_libelle, t.type_theme
				from (SELECT * FROM theme_produit) t, 
			       ( 
			         SELECT tp.*, dfa.nombre_appel, dfa.duree_appel, dfa.qte, dfa.montant_final
			         		,dfa.idproduit_catalogue as idproduit
					 	FROM THEME_PRODUIT_CATALOGUE tp, 
			         ( 
			           SELECT SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
			           			SUM(df.duree_appel) AS duree_appel, pca.idproduit_catalogue
			           FROM 	detail_facture_abo df, produit_client pc, produit_catalogue pca, 
					      			(	SELECT a.idinventaire_periode, a.date_emission
				      					FROM inventaire_periode a, 
				      						(	SELECT DISTINCT b.idcompte_facturation
				      							FROM compte_facturation b, groupe_client_ref_client c,
				      									sous_compte sco
				      							WHERE sco.idcompte_facturation=b.idcompte_facturation
				      									AND sco.idsous_compte=c.idref_client
				      									AND c.idgroupe_client=#IDcompte# 
				      						) e
				      					WHERE e.idcompte_facturation=a.idcompte_facturation
				                        AND trunc(a.date_emission)<=trunc(last_day(#DateFin#))
												AND trunc(a.date_emission)>=trunc(#DateDebut#,'MM')
					    		   ) ip,

									(	SELECT f.idsous_tete
										FROM sous_tete f, sous_compte g, groupe_client_ref_client h
										WHERE f.idsous_compte=g.idsous_compte 
												AND g.idsous_compte=h.idref_client
												AND h.idgroupe_client=#IDCompte#
									) st
			           WHERE 	df.idproduit_client=pc.idproduit_client 
	          					AND pca.idproduit_catalogue=pc.idproduit_catalogue
					   			AND df.idinventaire_periode=ip.idinventaire_periode
									AND df.idsous_tete=st.idsous_tete
					   GROUP BY pca.idproduit_catalogue
			         ) dfa 
			         WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue
			       ) tpc, produit_catalogue pca, operateur o
			WHERE t.idtheme_produit=tpc.idtheme_produit
					AND t.sur_theme='#sur_theme#'
					AND tpc.idproduit_catalogue=pca.idproduit_catalogue
					AND pca.operateurid=o.operateurid
			GROUP BY t.idtheme_produit, t.theme_libelle, t.type_theme
			ORDER BY sum(nvl(tpc.montant_final,0)) desc
		</cfquery>
		<cfreturn qGetDetailFactureAbo>
	</cffunction>
	
	<cffunction name="getLibelle" access="public" returntype="string" output="false" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfquery name="qGetLibelle" datasource="#session.OffreDSN#">
			select theme_libelle
			from theme_produit
			where idtheme_produit=#ID#
		</cfquery>
	</cffunction>
	
	<cffunction name="getNumero" access="public" returntype="string" output="false" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfquery name="qGetCompte" datasource="#session.OffreDSN#">
			select libelle_groupe_client
			from groupe_client gc
			where gc.idgroupe_client=#ID#
		</cfquery>
		<cfreturn qGetCompte.libelle_groupe_client>
	</cffunction>
	<cffunction name="getStrategy" access="public" returntype="string" output="false" >
		<cfreturn "Groupe de Sous Comptes">
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>