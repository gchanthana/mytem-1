 <cfcomponent name="facade" displayname="facade">
	
	<!--- TRANSFORMATION DE LA DATE --->
	<cffunction name="transformDate" access="private" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "/" & mid(oldDateString,4,2) & "/" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>

	<!--- RETOURNE LES PRODUITS DU THEME --->	
	<cffunction name="getListeThemesSurTheme" access="public" returntype="query">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">		
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="surtheme" type="string" required="true">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="isReport" type="numeric" required="false" default="0">
		
			<cfset obj = CreateObject("component","fr.consotel.consoview.tb.surtheme.surTheme")>
			<cfset obj.init(perimetre,modeCalcul)>
			
			<cfset periE0  = CreateObject("component","fr.consotel.consoview.M312.PerimetreE0")>
			
			<cfif isReport EQ 0>
				<cfset dataset = periE0.executQuery(2, 0, 0, surtheme, 
													session.perimetre.IDRACINE_MASTER,
													session.perimetre.ID_GROUPE,
													numero,
													transformDate(datedeb),
													transformDate(datefin),
													perimetre)>
			<cfelse>									
				<cfset dataset = periE0.executQuery(2, 0, 0, surtheme, 
													session.perimetre.IDRACINE_MASTER,
													session.perimetre.ID_GROUPE,
													numero,
													datedeb,
													datefin,
													perimetre)>								
			</cfif>				
		<cfreturn dataset>
	</cffunction>

</cfcomponent>