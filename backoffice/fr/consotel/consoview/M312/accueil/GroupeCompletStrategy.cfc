<!--- =========================================================================
Name: RefclientAboStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords$
========================================================================== --->
<cfcomponent displayname="GroupeCompletStrategy" hint="" extends="Strategy">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="RefclientAboStrategy" displayname="RefclientAboStrategy init()" hint="Initialize the RefclientAboStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(integer, string, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfargument name="tb" required="false" type="string" default="complet" displayname="tb" />
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_V3.TB_ACCUEIL">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#ID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_debut" value="#DateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_fin" value="#DateFin#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_tb" value="#tb#"/>			
	        <cfprocresult name="p_result"/>        
		</cfstoredproc>
		
		<cfreturn p_result/>	
	</cffunction>
	
	
	<cffunction name="getEvolution" access="public" output="false" returntype="struct" displayname="" hint="Raméne les données dans une structure">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfargument name="tb" required="false" type="string" default="complet" displayname="tb" />	
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_V3.TB_ACCUEIL_EVO">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#ID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_datefin" value="#DateFin#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_tb" value="#tb#"/>			
	        <cfprocresult name="p_result"/>        
		</cfstoredproc>	
			
		<cfreturn ProcessQuery(p_result, DateDebut ,DateFin)>
	</cffunction>
	
	<cffunction name="ProcessQuery" access="private" returntype="struct">
		<cfargument name="qGetMois" required="false" type="query" default=""  />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfset st_evolution_cout=structnew()>
		<cfif DateFin eq "">
			<cfset DateFin="#CreateDate("2004","01","01")#">
		</cfif>
		
		<!--- Preparation d'une structure vide --->
		<cfloop from="5" to="0" index="i" step="-1">
				<cfset temp=structnew()>
				<cfset temp.coutAbo=0>
				<cfset temp.coutConso=0>
				<cfset key=DateAdd('m', -i, DateFin)>
				<cfset lemois=dateformat(key, 'yyyy/mm') & "/01">
				<cfset var=structinsert(st_evolution_cout, lemois, temp)>
		</cfloop>
		
		<!--- <cf_dump variable="st_evolution_cout"> --->
		<cfset structTriee=ListSort(StructKeyList(st_evolution_cout),"textnocase", "asc")>
		<cfloop list="#structTriee#" index="imois">
			<cfloop query="qGetMois">
				<cfif iMois eq mois and type_theme eq "Abonnements">
					<cfset st_evolution_cout[iMois].coutAbo=st_evolution_cout[iMois].coutAbo+montant_final>
				<cfelseif iMois eq mois and type_theme eq "Consommations">
					<cfset st_evolution_cout[iMois].coutConso=st_evolution_cout[iMois].coutConso+montant_final>
				</cfif>
			</cfloop>
		</cfloop>
		
		<!--- <cf_dump variable="st_evolution_cout"><br> --->
		
		<cfif session.periodicite_facture eq 2>
			<cfset st_evolution_cout_final=structnew()>
			<cfset flag=0>
			<cfset mois_temp="">
			<cfset temp=structnew()>
			<cfset temp.coutConso=0>
			<cfset temp.coutAbo=0>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfif flag eq 1>
					<cfset compteur=compteur+1>
					<cfset mois_temp=mois_temp & "/" & LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm") &" " &left(iMois,4)>
					<cfset temp.coutConso=temp.coutConso+st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=temp.coutAbo+st_evolution_cout[iMois].coutAbo>
					<cfset temp.mois=mois_temp>
					<!--- <cfoutput>#compteur#:#mois_temp#<br></cfoutput> --->
					<cfset var=structinsert(st_evolution_cout_final,compteur , temp)>
					<cfset flag=0>
					<cfset mois_temp="">
				<cfelse>
					<cfset temp=structnew()>
					<cfset mois_temp=LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm")>
					<cfset temp.coutConso=st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=st_evolution_cout[iMois].coutAbo>
					<cfset flag=1>
				</cfif>
			</cfloop>
			<!--- Remet la structure dans l'ordre --->
			<cfset st_evolution_cout=structnew()>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout_final),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfset compteur=compteur+1>
				<cfset temp=structnew()>
				<cfset temp.mois=imois>
				<cfset temp.coutConso=st_evolution_cout_final[iMois].coutConso>
				<cfset temp.coutAbo=st_evolution_cout_final[iMois].coutAbo>
				<cfset temp.mois=st_evolution_cout_final[iMois].mois>
				<cfset var=structinsert(st_evolution_cout,compteur , temp)>
			</cfloop>
		<cfelse>
			<cfset st_evolution_cout_final=structnew()>
			<cfset flag=0>
			<cfset mois_temp="">
			<cfset temp=structnew()>
			<cfset temp.coutConso=0>
			<cfset temp.coutAbo=0>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfif flag eq 1>
					<cfset compteur=compteur+1>
					<cfset mois_temp=mois_temp & "/" & LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm") &" " &left(iMois,4)>
					<cfset temp.coutConso=temp.coutConso+st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=temp.coutAbo+st_evolution_cout[iMois].coutAbo>
					<cfset temp.mois=mois_temp>
					<!--- <cfoutput>#compteur#:#mois_temp#<br></cfoutput> --->
					<cfset var=structinsert(st_evolution_cout_final,compteur , temp)>
					<cfset flag=0>
					<cfset mois_temp="">
				<cfelse>
					<cfset temp=structnew()>
					<cfset mois_temp=LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm")>
					<cfset temp.coutConso=st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=st_evolution_cout[iMois].coutAbo>
					<cfset flag=1>
				</cfif>
			</cfloop>
			<!--- Remet la structure dans l'ordre --->
			<cfset st_evolution_cout=structnew()>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout_final),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfset compteur=compteur+1>
				<cfset temp=structnew()>
				<cfset temp.mois=imois>
				<cfset temp.coutConso=st_evolution_cout_final[iMois].coutConso>
				<cfset temp.coutAbo=st_evolution_cout_final[iMois].coutAbo>
				<cfset temp.mois=st_evolution_cout_final[iMois].mois>
				<cfset var=structinsert(st_evolution_cout,compteur , temp)>
			</cfloop>
		</cfif>
		<cfreturn st_evolution_cout > 
	</cffunction>
	
	<cffunction name="getStrategy" returntype="string" access="public" output="false">
		<cfreturn "Complet">
	</cffunction>
	
	<cffunction name="getLibelleAbo" returntype="string" access="public" output="false" hint="Raméne le périmétre de la requéte et le mode de calcul (Usage/facturation)">
		<cfreturn "Abonnements">
	</cffunction>
	
	<cffunction name="getLibelleConso" returntype="string" access="public" output="false" hint="Raméne le périmétre de la requéte et le mode de calcul (Usage/facturation)">
		<cfreturn "Consommations">
	</cffunction>
	
	<cffunction name="getNumero" returntype="string" access="public" output="false">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
			<cfquery name="qGetCompte" datasource="#session.offreDSN#">
				select libelle_groupe_client
				from groupe_client gc
				where gc.idgroupe_client=#ID#
			</cfquery>
			<cfreturn qGetCompte.libelle_groupe_client>
	</cffunction>
</cfcomponent>