<cfcomponent name="test" displayname="test">
	
	
	<!--- INITIATLISATION --->
	<cffunction name="init" access="public">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		<cfset obj1=CreateObject("component","fr.consotel.consoview.M312.accueil.tableaubord")>
		<cfset obj1.init(perimetre,modeCalcul)>
		
		<cfset dataset=obj1.queryData(numero,createOdbcDate(datedeb),createOdbcDate(datefin),sousModeCalcul)>
		<cfset setSessionValue("RECORDSET",#dataset#)>					
	</cffunction>
	
	<!--- RETOURNE LES SUR-THEMES ABOS --->	
	<cffunction name="getSurthemeAbos" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.M312.accueil.tableaubord")>		
		<cfset result = obj.getSurthemeAbos()>
		<cfreturn result>
	</cffunction>  
 	
	<!--- RETOURNE LES SUR-THEMES CONSOS --->	
	<cffunction name="getSurThemeConsos" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.M312.accueil.tableaubord")>		
		<cfset result = obj.getSurthemeConsos()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LES  THEMES CONSOS --->	
	<cffunction name="getConso" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.M312.accueil.tableaubord")>		
		<cfset result = obj.getConso()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LES  THEMES ABOS --->	
	<cffunction name="getAbo" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.M312.accueil.tableaubord")>	
		<cfset result = obj.getAbo()>
		<cfreturn result>
	</cffunction>  

	<!--- RETOURNE LA REPARTITION PAR OPERATEUR --->	
	<cffunction name="getRepartOperateur" access="public" returntype="query">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		 
			<cfset obj=CreateObject("component","fr.consotel.consoview.M312.accueil.tableaubord")>		
			<cfset result = obj.getRepartOperateur()>			
		 
		<cfreturn result>
	</cffunction>  

	<!--- RETOURNE LA REPARTITION DES DUREE PAR OPERATEUR --->	
	<cffunction name="getRepartDuree" access="public" returntype="query">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.M312.accueil.tableaubord")>		
		<cfset result = obj.getRepartDuree()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE L'EVOLUTION DES COUTS --->	
	<cffunction name="getEvolution" access="public" returntype="struct">
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="periodicite" type="numeric" required="false">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="sousModeCalcul" type="string" required="false">
		
		<cfset setSessionValue("periodicite_facture",#periodicite#)>  	
		 
		<cfset obj=CreateObject("component","fr.consotel.consoview.M312.accueil.tableaubord")>
		<cfset obj.init(perimetre,modeCalcul)>
		<cfset result = obj.getEvolution(numero,createOdbcDate(datedeb),createOdbcDate(datefin),sousModeCalcul)>		
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LE LIBELLE ABOS --->	
	<cffunction name="getLibelleAbo" access="public" returntype="string">
		<cfargument name="Perimetre" type="string" required="true">
		<cfargument name="ModeCalcul" type="string" required="true">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.M312.accueil.tableaubord")>
		<cfset obj.init(perimetre,modeCalcul)>	
		<cfset result = obj.getLibelleAbo()>
		<cfreturn result>
	</cffunction>  
	
	<!--- RETOURNE LE LIBELLE CONSO --->	
	<cffunction name="getLibelleConso" access="public" returntype="string">
		<cfargument name="Perimetre" type="string" required="true">
		<cfargument name="ModeCalcul" type="string" required="true">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.M312.accueil.tableaubord")>		
		<cfset obj.init(perimetre,modeCalcul)>
		<cfset result = obj.getLibelleConso()>
		<cfreturn result>
	</cffunction> 
	
<!---::::::::::::::::::::::::::::::::::::::::::::::::::PRIVATE::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

	<cffunction name="setSessionValue" output="true" access="private" >
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset "session.#cle#"=valeur>
		<cfelse>
			<cfset structInsert(session,cle,valeur)>
		</cfif>
	</cffunction>


</cfcomponent>