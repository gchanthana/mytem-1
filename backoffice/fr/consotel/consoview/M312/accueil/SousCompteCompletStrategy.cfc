<!--- =========================================================================
Name: RefclientAboStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords$
========================================================================== --->
<cfcomponent displayname="SousCompteFacturationAboStrategy" hint="" extends="Strategy">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="RefclientAboStrategy" displayname="RefclientAboStrategy init()" hint="Initialize the RefclientAboStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(integer, string, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfargument name="tb" required="false" type="string" default="complet" displayname="tb" />
		<cfquery name="qGetDetailFactureAbo" datasource="#session.OFFREDSN#">
			SELECT tp.type_theme, tp.theme_libelle,tp.idtheme_produit, nvl(a.qte, 0) AS qte,
			       	nvl(a.nombre_appel, 0) AS nombre_appel, nvl(a.duree_appel, 0) AS duree_appel,
		    	   	nvl(a.montant,0) AS montant_final, a.nom , tp.ordre_affichage, nom, tp.segment_theme, tp.sur_theme
				FROM
				(	
				SELECT 	t.type_theme, t.theme_libelle,t.idtheme_produit, nvl(sum(tpc.montant), 0) AS montant, nvl(sum(tpc.qte), 0) AS qte,
				       	nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel, tpc.nom
			    FROM 	theme_produit t, 
			       		( 
			         	SELECT tp.*, dfa.montant, dfa.nombre_appel, dfa.duree_appel, dfa.qte, dfa.nom
			         		FROM THEME_PRODUIT_CATALOGUE tp, 
			         			( 
					           		SELECT 	SUM(df.qte) AS qte, SUM(df.montant) AS montant, SUM(df.nombre_appel) AS nombre_appel,
			        		   				SUM(df.duree_appel) AS duree_appel, df.idproduit_client, df.idinventaire_periode, 
					           				pc.idproduit_catalogue, o.nom
			           				FROM 	detail_facture_abo df, produit_client pc, produit_catalogue pca,
												sous_tete st, sous_compte sco, compte_facturation cf, operateur o,
                       					(	SELECT a.idinventaire_periode , b.idcompte_facturation, a.numero_facture, a.date_emission
                       						FROM inventaire_periode a, compte_facturation b, sous_compte c
                       						WHERE a.idcompte_facturation=b.idcompte_facturation
															and b.idcompte_facturation=c.idcompte_facturation
                       								AND c.idsous_compte=#ID#
                       				   ) ip

			           				WHERE 	df.idproduit_client=pc.idproduit_client 
							        		AND pca.idproduit_catalogue=pc.idproduit_catalogue
							        		AND pca.operateurID=o.operateurID
											AND df.idsous_tete=st.idsous_tete
											AND st.idsous_compte=sco.idsous_compte
											AND sco.idcompte_facturation=cf.idcompte_facturation
											AND df.idinventaire_periode=ip.idinventaire_periode
											AND sco.idsous_compte=#ID#
											and cf.idcompte_facturation=ip.idcompte_facturation
							        		AND trunc(ip.date_emission)<=trunc(last_day(#DateFin#))
											AND trunc(ip.date_emission)>=trunc(#DateDebut#,'MM')
							        GROUP BY df.idproduit_client, df.idinventaire_periode, pc.idproduit_catalogue, o.nom
						           	) dfa 
			         		WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue (+)
			       		) tpc 
				WHERE t.idtheme_produit=tpc.idtheme_produit (+)
				GROUP BY t.type_theme,t.theme_libelle,t.ordre_affichage,t.idtheme_produit, tpc.nom
				ORDER BY t.ordre_affichage) a,
			theme_produit tp
			WHERE a.idtheme_produit (+) =tp.idtheme_produit 
				<cfif tb neq "Complet">
					and lower(tp.segment_theme)=lower('#tb#')
				</cfif>
		</cfquery>
		<cfreturn qGetDetailFactureAbo>
	</cffunction>
	
	<cffunction name="getStrategy" returntype="string" access="public" output="false">
		<cfreturn "Sous Compte">
	</cffunction>
	
	<cffunction name="getLibelleAbo" returntype="string" access="public" output="false" hint="Ramï¿½ne le pÃ©rimï¿½tre de la requï¿½te et le mode de calcul (Usage/facturation)">
		<cfreturn "Abonnements">
	</cffunction>
	
	<cffunction name="getLibelleConso" returntype="string" access="public" output="false" hint="Ramï¿½ne le pÃ©rimï¿½tre de la requï¿½te et le mode de calcul (Usage/facturation)">
		<cfreturn "Consommations">
	</cffunction>
	
	<cffunction name="getNumero" returntype="string" access="public" output="false">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
			<cfquery name="qGetCompte" datasource="#session.OffreDSN#">
				select sous_compte
				from sous_compte sc
				where sc.idsous_compte=#ID#
			</cfquery>
			<cfreturn qGetCompte.sous_compte>
	</cffunction>
</cfcomponent>