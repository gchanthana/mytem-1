<!--- =========================================================================
Name: GroupePartielStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords$
========================================================================== --->
<cfcomponent displayname="GroupePartielStrategy" hint="" extends="Strategy">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="GroupePartielStrategy" displayname="RefclientAboStrategy init()" hint="Initialize the RefclientAboStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(integer, string, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfargument name="tb" required="false" type="string" default="complet" displayname="tb" />
			<!--- Choix de l'exclusion du zip ou non --->
			<cfif trim(session.tbpartiel.zip.value) neq "">
				<cfif trim(session.tbpartiel.zip.exclusion) eq "false">
					<cfset vTempZip="">
				<cfelse>
					<cfset vTempZip=" NOT ">
				</cfif>
				<cfset lzip="">
				<cfset flagInit=0>
				<cfloop list="#session.tbpartiel.zip.value#" index="i">
					<cfif flagInit eq 0>
						<cfset flagInit=1>
						<cfset lzip="'" & "#trim(i)#" & "'">
					<cfelse>
						<cfset lzip=lzip & ",'" & trim(i) & "'">
					</cfif>
				</cfloop>
			</cfif>
			<!--- Choix de l'exclusion de la chaine ou non --->
			<cfif trim(session.tbpartiel.chaine.exclusion) eq "false">
				<cfset vTempchaine="">
			<cfelse>
				<cfset vTempchaine=" NOT ">
			</cfif>
			<cfif trim(session.tbpartiel.fonction.exclusion) eq "false">
				<cfset vTempfonction="">
			<cfelse>
				<cfset vTempfonction=" NOT ">
			</cfif>
			<cfif trim(session.tbpartiel.theme.exclusion) eq "false">
				<cfset vTemptheme="">
			<cfelse>
				<cfset vTemptheme=" NOT ">
			</cfif>
			<cfif trim(session.tbpartiel.operateur.exclusion) eq "false">
				<cfset vTempop="">
			<cfelse>
				<cfset vTempop=" NOT ">
			</cfif>
			<cfquery name="qGetDetailFactureAbo" datasource="#session.OffreDSN#">
				SELECT tp.type_theme, tp.theme_libelle,tp.idtheme_produit, nvl(a.qte, 0) AS qte,
			       	nvl(a.nombre_appel, 0) AS nombre_appel, nvl(a.duree_appel, 0) AS duree_appel,
		    	   	nvl(a.montant_final,0) AS montant_final, a.nom , tp.ordre_affichage, tp.segment_theme, tp.sur_theme
				FROM
						(
						SELECT 	t.type_theme, t.theme_libelle,t.idtheme_produit, nvl(sum(tpc.qte), 0) AS qte,
					   	    	nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel,
				    	   		sum(nvl(tpc.montant_final,0)) AS montant_final, tpc.nom
				    	FROM 	theme_produit t, 
				       		( 
				         		SELECT tp.*, dfa.nombre_appel, dfa.duree_appel, dfa.qte, dfa.montant_final, dfa.nom
				         		FROM THEME_PRODUIT_CATALOGUE tp, 
				         			( 
		                        SELECT	SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
		                        			SUM(df.duree_appel) AS duree_appel, pc.idproduit_catalogue,
													o.nom
		                        FROM 	detail_facture_abo df, produit_client pc, 
		                        		(select * from produit_catalogue 
													<cfif trim(session.tbpartiel.operateur.value) neq ''>
														where operateurid #vTempop# IN (#session.tbpartiel.operateur.value#)
													</cfif>		
												) pca, 
		                        		(	select * from operateur 
													<!--- Choix de l'exclusion des opérateurs ou non --->
													<cfif trim(session.tbpartiel.operateur.value) neq ''>
														WHERE operateurid #vTempop# IN (#session.tbpartiel.operateur.value#)
													</cfif>		
												) o, 
												(	select * from compte_facturation 
													<cfif trim(session.tbpartiel.chaine.value) neq ''>
														<cfswitch expression="#session.tbpartiel.chaine.typechaine#">
															<cfcase value="compte">
																	WHERE lower(trim(compte_facturation)) #vTempchaine# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
															</cfcase>
														</cfswitch>
													</cfif>
													
												) cf,
		                        		(	SELECT a.idinventaire_periode, a.idcompte_facturation, a.date_emission
                      						FROM inventaire_periode a, compte_facturation b, groupe_client_ref_client c
                      						WHERE a.idcompte_facturation=b.idcompte_facturation
		                      						AND b.idref_client=c.idref_client
		                      						AND c.idgroupe_client=#ID#
					                          		AND trunc(a.date_emission)<=trunc(last_day(#DateFin#))
															AND trunc(a.date_emission)>=trunc(#DateDebut#,'MM')
															<cfif trim(session.tbpartiel.chaine.value) neq ''>
																<cfswitch expression="#session.tbpartiel.chaine.typechaine#">
																	<cfcase value="compte">
																		AND lower(trim(b.compte_facturation)) #vTempchaine# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																	</cfcase>
																</cfswitch>
															</cfif>
                      		   		) ip
												<cfif trim(session.tbpartiel.chaine.value) neq '' or trim(session.tbpartiel.fonction.value) neq '0'>
													<cfswitch expression="#session.tbpartiel.chaine.typechaine#">
														<cfcase value="Lignes">
															,	(	select idsous_compte, Idsous_Tete from sous_tete 
																	where lower(sous_tete) #vTempchaine# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																			<cfif trim(session.tbpartiel.fonction.value) neq 0>
																				AND lower(trim(nvl(commentaires,'vide'))) #vTempfonction# LIKE lower(trim('%#session.tbpartiel.fonction.value#%'))
																			</cfif>																	
																) st
															<!--- Choix de l'exclusion du zip ou non --->
											            <cfif trim(session.tbpartiel.zip.value) neq ''>
																, sous_compte sco
																, 	(	select * from site_client 
																		where substr(TRIM(zipcode),1,2) #vTempZip# IN (#PreserveSingleQuotes(lzip)#)
																	) scl
															</cfif>
														</cfcase>
														<cfcase value="fonction">
															,	(	select Idsous_Tete from sous_tete 
																	where lower(trim(nvl(commentaires,'vide'))) #vTempchaine# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																) st
															<!--- Choix de l'exclusion du zip ou non --->
											            <cfif trim(session.tbpartiel.zip.value) neq ''>
																, sous_compte sco
																, 	(	select * from site_client 
																		where substr(TRIM(zipcode),1,2) #vTempZip# IN (#PreserveSingleQuotes(lzip)#)
																	) scl
															</cfif>
														</cfcase>
														<cfcase value="site">
															,	(	select idsous_compte, Idsous_Tete from sous_tete 
																	<cfif trim(session.tbpartiel.fonction.value) neq "0">
																		where lower(trim(nvl(commentaires,'vide'))) #vTempfonction# LIKE lower(trim('%#session.tbpartiel.fonction.value#%'))
																	</cfif>
																) st
															, sous_compte sco
															, 	(	select * from site_client 
																	where 
																	<cfif trim(session.tbpartiel.chaine.exclusion) eq "false">
																		(	lower(trim(adresse1)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																			OR lower(trim(adresse2)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																			OR lower(trim(commune)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																			OR lower(trim(nom_site)) LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																		)
																	<cfelse>
																		(	lower(trim(adresse1)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																			AND lower(trim(adresse2)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																			AND lower(trim(commune)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																			AND lower(trim(nom_site)) NOT LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																		)
																	</cfif>
																	<cfif trim(session.tbpartiel.zip.value) neq ''>
																		AND substr(TRIM(zipcode),1,2) #vTempZip# IN (#PreserveSingleQuotes(lzip)#)
																	</cfif>
																) scl
														</cfcase>
														<cfcase value="CR">
															,	(	select Idsous_Tete, idsous_compte from sous_tete 
																	<cfif trim(session.tbpartiel.fonction.value) neq "0">
																		where lower(trim(nvl(commentaires,'vide'))) #vTempfonction# LIKE lower(trim('%#session.tbpartiel.fonction.value#%'))
																	</cfif>
																) st
															, (	select idsous_compte, idcompte_facturation from sous_compte 
																	where lower(trim(sous_compte)) #vTempchaine# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																) sco
															<!--- Choix de l'exclusion du zip ou non --->
											            <cfif trim(session.tbpartiel.zip.value) neq ''>
																, 	(	select * from site_client 
																		where substr(TRIM(zipcode),1,2) #vTempZip# IN (#PreserveSingleQuotes(lzip)#)
																	) scl
															</cfif>

														</cfcase>
														<cfdefaultcase>
															<cfif trim(session.tbpartiel.fonction.value) neq '0'>
																,	(	select Idsous_Tete, idsous_compte from sous_tete 
																		<cfif trim(session.tbpartiel.fonction.value) neq "0">
																			where lower(trim(nvl(commentaires,'vide'))) #vTempfonction# LIKE lower(trim('%#session.tbpartiel.fonction.value#%'))
																		</cfif>
																	) st
																, (	select idsous_compte, idcompte_facturation from sous_compte 
																		where lower(trim(sous_compte)) #vTempchaine# LIKE lower(TRIM('%#session.tbpartiel.chaine.value#%'))
																	) sco																	
												            <cfif trim(session.tbpartiel.zip.value) neq ''>
																	, 	(	select * from site_client 
																				where substr(TRIM(zipcode),1,2) #vTempZip# IN (#PreserveSingleQuotes(lzip)#)
																		) scl
																</cfif>
															<cfelse>	
												            <cfif trim(session.tbpartiel.zip.value) neq ''>
																	,	sous_tete st
																	<!--- Choix de l'exclusion du zip ou non --->
																	, sous_compte sco
																	, 	(	select * from site_client 
																			where substr(TRIM(zipcode),1,2) #vTempZip# IN (#PreserveSingleQuotes(lzip)#)
																		) scl
																</cfif>
															</cfif>
														</cfdefaultcase>
													</cfswitch>
												</cfif>
												
												
		                        WHERE 	df.idproduit_client=pc.idproduit_client 
		                        		AND pca.idproduit_catalogue=pc.idproduit_catalogue
		                        		AND df.idinventaire_periode=ip.idinventaire_periode
												AND ip.idcompte_facturation=cf.idcompte_facturation
												AND pca.operateurid=o.operateurid

												<cfif trim(session.tbpartiel.chaine.value) neq '' or trim(session.tbpartiel.fonction.value) neq '0'>
													<cfswitch expression="#session.tbpartiel.chaine.typechaine#">
														<cfcase value="Lignes">
															AND df.idsous_tete=st.Idsous_Tete
											            <cfif trim(session.tbpartiel.zip.value) neq ''>
																AND sco.idsous_compte=st.idsous_compte
																AND sco.siteid=scl.siteid										
															</cfif>
														</cfcase>
														<cfcase value="fonction">
															AND df.idsous_tete=st.Idsous_Tete
											            <cfif trim(session.tbpartiel.zip.value) neq ''>
																AND sco.idsous_compte=st.idsous_compte
																AND sco.siteid=scl.siteid										
															</cfif>
														</cfcase>
														<cfcase value="site">
															AND df.idsous_tete=st.Idsous_Tete
															AND sco.idcompte_facturation=cf.idcompte_facturation
															AND sco.siteid=scl.siteid										
															AND st.idsous_compte=sco.Idsous_Compte
														</cfcase>
														<cfcase value="CR">
															AND df.idsous_tete=st.Idsous_Tete
															AND sco.idsous_compte=st.idsous_compte
															AND sco.idcompte_facturation=cf.idcompte_facturation
											            <cfif trim(session.tbpartiel.zip.value) neq ''>
																AND sco.siteid=scl.siteid										
															</cfif>
														</cfcase>
														<cfdefaultcase>
															<cfif trim(session.tbpartiel.fonction.value) neq '0'>
																AND df.idsous_tete=st.Idsous_Tete
																AND sco.idsous_compte=st.idsous_compte
																AND sco.idcompte_facturation=cf.idcompte_facturation
												            <cfif trim(session.tbpartiel.zip.value) neq ''>
																	AND sco.siteid=scl.siteid										
																</cfif>
															<cfelse>
												            <cfif trim(session.tbpartiel.zip.value) neq ''>
																	AND sco.siteid=scl.siteid										
																	AND df.idsous_tete=st.Idsous_Tete
																	AND sco.idsous_compte=st.idsous_compte
																	AND sco.idcompte_facturation=cf.idcompte_facturation
																</cfif>
															</cfif>
														</cfdefaultcase>
													</cfswitch>
												</cfif>
														
									   	GROUP BY pc.idproduit_catalogue,o.nom
										) dfa 
										WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue (+)
												<!--- Choix de l'exclusion des opérateurs ou non --->
												<cfif (trim(session.tbpartiel.theme.abo neq "") OR trim(session.tbpartiel.theme.conso) neq "")>
													<cfset ltheme="">
													<cfloop list="#session.tbpartiel.theme.abo#" index="i">
														<cfset ltheme=ListAppend(ltheme,i)>
													</cfloop>
													<cfloop list="#session.tbpartiel.theme.conso#" index="i">
														<cfset ltheme=ListAppend(ltheme,i)>
													</cfloop>
													AND tp.idtheme_produit #vTemptheme# IN (#ltheme#)
												</cfif>		
								) tpc 
							WHERE t.idtheme_produit=tpc.idtheme_produit (+)
							GROUP BY t.type_theme,t.theme_libelle,t.ordre_affichage,t.idtheme_produit, tpc.nom
							ORDER BY t.ordre_affichage
						) a,
						theme_produit tp
				WHERE a.idtheme_produit (+) =tp.idtheme_produit 
		</cfquery>
		<cfreturn qGetDetailFactureAbo>
	</cffunction>
	
	
	<cffunction name="getStrategy" returntype="string" access="public" output="false">
		<cfreturn "Partiel">
	</cffunction>
	
	<cffunction name="getLibelleAbo" returntype="string" access="public" output="false" hint="Raméne le périmétre de la requéte et le mode de calcul (Usage/facturation)">
		<cfreturn "Abonnements">
	</cffunction>
	
	<cffunction name="getLibelleConso" returntype="string" access="public" output="false" hint="Raméne le périmétre de la requéte et le mode de calcul (Usage/facturation)">
		<cfreturn "Consommations">
	</cffunction>
	
	<cffunction name="getNumero" returntype="string" access="public" output="false">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
			<cfquery name="qGetCompte" datasource="#session.offreDSN#">
				select libelle_groupe_client
				from groupe_client gc
				where gc.idgroupe_client=#ID#
			</cfquery>
			<cfreturn qGetCompte.libelle_groupe_client>
	</cffunction>
</cfcomponent>