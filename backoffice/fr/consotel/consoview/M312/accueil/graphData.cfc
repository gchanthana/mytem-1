<cfcomponent displayname="graphData" hint="Cette classe contient les données qui servent é faire les graphes sur l'accueil">

<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="getEvolutionFacturation" access="public" output="false" returntype="struct" displayname="" hint="Raméne les données dans une structure">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetMois" datasource="#session.OffreDSN#">
			SELECT t.type_theme, nvl(sum(tpc.qte), 0) AS qte,
			       nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel,
			       sum(nvl(tpc.montant_final,0)) AS montant_final, nvl(tpc.mois,'0') AS mois
			       from (SELECT * FROM theme_produit) t, 
			       ( 
			         SELECT tp.*, dfa.nombre_appel, dfa.duree_appel, dfa.qte, 
			         dfa.montant_final, dfa.mois
			         FROM THEME_PRODUIT_CATALOGUE tp, 
			         ( 
			           SELECT SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
			          			SUM(df.duree_appel) AS duree_appel, df.idproduit_client, 
					   			pc.idproduit_catalogue,
					   			to_char(ip.datedeb, 'yyyy/mm')||'/01' as mois
			           FROM detail_facture_abo df, produit_client pc, produit_catalogue pca, 
           					(	SELECT a.idinventaire_periode , a.numero_facture, a.datedeb, a.date_emission
           						FROM inventaire_periode a, compte_facturation b
           						WHERE a.idcompte_facturation=b.idcompte_facturation
           								AND b.idref_client=#ID#
           				   ) ip
			           WHERE df.idproduit_client=pc.idproduit_client 
								   AND df.idinventaire_periode=ip.idinventaire_periode
						         AND pca.idproduit_catalogue=pc.idproduit_catalogue
			         			AND trunc(ip.datedeb,'MM')>=trunc(add_months(#DateFin#,-6),'MM')
			           			AND trunc(ip.datedeb,'MM')<=trunc(last_day(#DateFin#),'MM')
					   			and df.idref_client=#ID#
			           GROUP BY df.idproduit_client, pc.idproduit_catalogue, to_char(ip.datedeb, 'yyyy/mm')||'/01'
			          ) dfa 
			         WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue (+)
			       ) tpc 
			WHERE t.idtheme_produit=tpc.idtheme_produit (+)
			GROUP BY t.type_theme,nvl(tpc.mois,'0')
			HAVING nvl(tpc.mois,'0') <> '0'
			ORDER BY t.type_theme,nvl(tpc.mois,'0')
		</cfquery>
		<cfreturn ProcessQuery(qGetMois, DateDebut ,DateFin)>
	</cffunction>
	
	<cffunction name="getEvolutionUsage" access="public" output="false" returntype="struct" displayname="" hint="Raméne les données dans une structure">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetMois" datasource="#session.OffreDSN#">
			SELECT t.type_theme, nvl(sum(tpc.qte), 0) AS qte,
			       nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel,
			       sum(nvl(tpc.montant_final,0)) AS montant_final, nvl(tpc.mois,'0') AS mois
			       from (SELECT * FROM theme_produit) t, 
			       ( 
			         SELECT tp.*, dfa.nombre_appel, dfa.duree_appel, dfa.qte, 
			         dfa.montant_final, dfa.mois
			         FROM THEME_PRODUIT_CATALOGUE tp, 
			         ( 
			           SELECT SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
			           SUM(df.duree_appel) AS duree_appel, df.idproduit_client, 
					   pc.idproduit_catalogue,
					   to_char(datedeb, 'yyyy/mm')||'/01' as mois
			           FROM detail_facture_abo df, produit_client pc, produit_catalogue pca
			           WHERE df.idproduit_client=pc.idproduit_client 
			           AND pca.idproduit_catalogue=pc.idproduit_catalogue
			           AND trunc(df.date_affectation,'MM')>=trunc(add_months(#DateFin#,-6),'MM')
			           AND trunc(df.date_affectation,'MM')<=trunc(last_day(#DateFin#),'MM')
					   and df.idref_client=#ID#
			           GROUP BY df.idproduit_client, pc.idproduit_catalogue, to_char(datedeb, 'yyyy/mm')||'/01'
			          ) dfa 
			         WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue (+)
			       ) tpc 
			WHERE t.idtheme_produit=tpc.idtheme_produit (+)
			GROUP BY t.type_theme,nvl(tpc.mois,'0')
			HAVING nvl(tpc.mois,'0') <> '0'
			ORDER BY t.type_theme,nvl(tpc.mois,'0')
		</cfquery>
		<cfreturn ProcessQuery(qGetMois, DateDebut ,DateFin)>
	</cffunction>
	
	<cffunction name="ProcessQuery" access="private" returntype="struct">
		<cfargument name="qGetMois" required="false" type="query" default=""  />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfset st_evolution_cout=structnew()>
		<cfif DateFin eq "">
			<cfset DateFin="#CreateDate("2004","01","01")#">
		</cfif>
		
		<!--- Preparation d'une structure vide --->
		<cfloop from="5" to="0" index="i" step="-1">
				<cfset temp=structnew()>
				<cfset temp.coutAbo=0>
				<cfset temp.coutConso=0>
				<cfset key=DateAdd('m', -i, DateFin)>
				<cfset lemois=dateformat(key, 'yyyy/mm') & "/01">
				<cfset var=structinsert(st_evolution_cout, lemois, temp)>
		</cfloop>
		
		<!--- <cf_dump variable="st_evolution_cout"> --->
		<cfset structTriee=ListSort(StructKeyList(st_evolution_cout),"textnocase", "asc")>
		<cfloop list="#structTriee#" index="imois">
			<cfloop query="qGetMois">
				<cfif iMois eq mois and type_theme eq "Abonnements">
					<cfset st_evolution_cout[iMois].coutAbo=st_evolution_cout[iMois].coutAbo+montant_final>
				<cfelseif iMois eq mois and type_theme eq "Consommations">
					<cfset st_evolution_cout[iMois].coutConso=st_evolution_cout[iMois].coutConso+montant_final>
				</cfif>
			</cfloop>
		</cfloop>
		
		<!--- <cf_dump variable="st_evolution_cout"><br> --->
		
		<cfif session.periodicite_facture eq 2>
			<cfset st_evolution_cout_final=structnew()>
			<cfset flag=0>
			<cfset mois_temp="">
			<cfset temp=structnew()>
			<cfset temp.coutConso=0>
			<cfset temp.coutAbo=0>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfif flag eq 1>
					<cfset compteur=compteur+1>
					<cfset mois_temp=mois_temp & "/" & LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm") &" " &left(iMois,4)>
					<cfset temp.coutConso=temp.coutConso+st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=temp.coutAbo+st_evolution_cout[iMois].coutAbo>
					<cfset temp.mois=mois_temp>
					<!--- <cfoutput>#compteur#:#mois_temp#<br></cfoutput> --->
					<cfset var=structinsert(st_evolution_cout_final,compteur , temp)>
					<cfset flag=0>
					<cfset mois_temp="">
				<cfelse>
					<cfset temp=structnew()>
					<cfset mois_temp=LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm")>
					<cfset temp.coutConso=st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=st_evolution_cout[iMois].coutAbo>
					<cfset flag=1>
				</cfif>
			</cfloop>
			<!--- Remet la structure dans l'ordre --->
			<cfset st_evolution_cout=structnew()>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout_final),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfset compteur=compteur+1>
				<cfset temp=structnew()>
				<cfset temp.mois=imois>
				<cfset temp.coutConso=st_evolution_cout_final[iMois].coutConso>
				<cfset temp.coutAbo=st_evolution_cout_final[iMois].coutAbo>
				<cfset temp.mois=st_evolution_cout_final[iMois].mois>
				<cfset var=structinsert(st_evolution_cout,compteur , temp)>
			</cfloop>
		<cfelse>
			<cfset st_evolution_cout_final=structnew()>
			<cfset flag=0>
			<cfset mois_temp="">
			<cfset temp=structnew()>
			<cfset temp.coutConso=0>
			<cfset temp.coutAbo=0>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfif flag eq 1>
					<cfset compteur=compteur+1>
					<cfset mois_temp=mois_temp & "/" & LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm") &" " &left(iMois,4)>
					<cfset temp.coutConso=temp.coutConso+st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=temp.coutAbo+st_evolution_cout[iMois].coutAbo>
					<cfset temp.mois=mois_temp>
					<!--- <cfoutput>#compteur#:#mois_temp#<br></cfoutput> --->
					<cfset var=structinsert(st_evolution_cout_final,compteur , temp)>
					<cfset flag=0>
					<cfset mois_temp="">
				<cfelse>
					<cfset temp=structnew()>
					<cfset mois_temp=LsDateFormat(CreateDate(left(iMois,4),mid(iMois,6,2),right(iMois,2)),"mmm")>
					<cfset temp.coutConso=st_evolution_cout[iMois].coutConso>
					<cfset temp.coutAbo=st_evolution_cout[iMois].coutAbo>
					<cfset flag=1>
				</cfif>
			</cfloop>
			<!--- Remet la structure dans l'ordre --->
			<cfset st_evolution_cout=structnew()>
			<cfset compteur=0>
			<cfset structTriee=ListSort(StructKeyList(st_evolution_cout_final),"textnocase", "asc")>
			<cfloop list="#structTriee#" index="imois">
				<cfset compteur=compteur+1>
				<cfset temp=structnew()>
				<cfset temp.mois=imois>
				<cfset temp.coutConso=st_evolution_cout_final[iMois].coutConso>
				<cfset temp.coutAbo=st_evolution_cout_final[iMois].coutAbo>
				<cfset temp.mois=st_evolution_cout_final[iMois].mois>
				<cfset var=structinsert(st_evolution_cout,compteur , temp)>
			</cfloop>
		</cfif>
		<cfreturn st_evolution_cout > 
	</cffunction>
	
	<cffunction name="getRepartOperateurUsage" returntype="query">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetRepartOpe" datasource="#session.OffreDSN#">
			SELECT 	SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
						SUM(df.duree_appel) AS duree_appel, o.nom
				FROM 	detail_facture_abo df, produit_client pc, produit_catalogue pca, operateur o
				WHERE 	df.idproduit_client=pc.idproduit_client 
			AND pca.idproduit_catalogue=pc.idproduit_catalogue
			AND trunc(df.date_affectation,'MM')>=trunc(#DateDebut#,'MM')
			AND trunc(df.date_affectation,'MM')<=trunc(last_day(#DateFin#),'MM')
			AND df.idref_client=#ID#
			AND pca.operateurID=o.operateurID
			GROUP BY o.nom
		</cfquery>
		<cfreturn qGetRepartOpe> 
	</cffunction>
	
	<cffunction name="getRepartOperateurFacturation" returntype="query">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetRepartOpe" datasource="#session.OffreDSN#">
			SELECT 	SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
					SUM(df.duree_appel) AS duree_appel, o.nom
			FROM 	detail_facture_abo df, produit_client pc, produit_catalogue pca, operateur o,
 					(	SELECT a.idinventaire_periode , a.numero_facture, a.datedeb, a.date_emission
 						FROM inventaire_periode a, compte_facturation b
 						WHERE a.idcompte_facturation=b.idcompte_facturation
 								AND b.idref_client=#ID#
 				   ) ip
			WHERE 	df.idproduit_client=pc.idproduit_client 
				AND pca.idproduit_catalogue=pc.idproduit_catalogue
				AND df.idinventaire_periode=ip.idinventaire_periode
				AND trunc(ip.date_emission)>=trunc(#DateDebut#,'MM')
				AND trunc(ip.date_emission)<=trunc(last_day(#DateFin#))
				AND df.idref_client=#ID#
				AND pca.operateurID=o.operateurID
			GROUP BY o.nom
		</cfquery>
		<cfreturn qGetRepartOpe> 
	</cffunction>
</cfcomponent>