<!--- =========================================================================
Name: RechercheCompletStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="RechercheCompletStrategy" hint="" extends="Strategy">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="RechercheCompletStrategy" displayname="RefclientAboStrategy init()" hint="Initialize the RefclientAboStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(integer, string, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetDetailFactureAbo" datasource="#application.OFFREDSN#">
			SELECT tp.type_theme, tp.theme_libelle,tp.idtheme_produit, nvl(a.qte, 0) AS qte,
			       	nvl(a.nombre_appel, 0) AS nombre_appel, nvl(a.duree_appel, 0) AS duree_appel,
		    	   	nvl(a.montant,0) AS montant_final, a.nom , tp.ordre_affichage, nom
				FROM
				(	
				SELECT 	t.type_theme, t.theme_libelle,t.idtheme_produit, nvl(sum(tpc.montant), 0) AS montant, nvl(sum(tpc.qte), 0) AS qte,
				       	nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel, tpc.nom
			    FROM 	theme_produit t, 
			       		( 
			         	SELECT tp.*, dfa.montant, dfa.nombre_appel, dfa.duree_appel, dfa.qte, dfa.nom
			         		FROM THEME_PRODUIT_CATALOGUE tp, 
			         			( 
					           		SELECT 	SUM(df.qte) AS qte, SUM(df.montant) AS montant, SUM(df.nombre_appel) AS nombre_appel,
			        		   				SUM(df.duree_appel) AS duree_appel, df.idproduit_client, df.idinventaire_periode, 
					           				pc.idproduit_catalogue, o.nom
			           				FROM 	detail_facture_abo df, produit_client pc, produit_catalogue pca,
											sous_tete st, sous_compte sco, compte_facturation cf, inventaire_periode ip, operateur o
			           				WHERE 	df.idproduit_client=pc.idproduit_client 
							        		AND pca.idproduit_catalogue=pc.idproduit_catalogue
							        		AND pca.operateurID=o.operateurID
											AND df.idsous_tete=st.idsous_tete
											AND st.idsous_compte=sco.idsous_compte
											AND sco.idcompte_facturation=cf.idcompte_facturation
											AND df.idinventaire_periode=ip.idinventaire_periode
											AND st.idsous_tete in (#ValueList(session.recherche.idsous_tete)#)
											and cf.idcompte_facturation=ip.idcompte_facturation
							        		AND ip.date_emission<=last_day(#DateFin#)
											AND ip.date_emission>=#DateDebut#
							        GROUP BY df.idproduit_client, df.idinventaire_periode, pc.idproduit_catalogue, o.nom
						           	) dfa 
			         		WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue (+)
			       		) tpc 
				WHERE t.idtheme_produit=tpc.idtheme_produit (+)
				GROUP BY t.type_theme,t.theme_libelle,t.ordre_affichage,t.idtheme_produit, tpc.nom
				ORDER BY t.ordre_affichage) a,
			theme_produit tp
			WHERE a.idtheme_produit (+) =tp.idtheme_produit 
		</cfquery>
		<cfreturn qGetDetailFactureAbo>
	</cffunction>
	
	<cffunction name="getStrategy" returntype="string" access="public" output="false">
		<cfreturn "Recherche">
	</cffunction>
	
	<cffunction name="getLibelleAbo" returntype="string" access="public" output="false" hint="Raméne le périmétre de la requéte et le mode de calcul (Usage/facturation)">
		<cfreturn "Abonnements">
	</cffunction>
	
	<cffunction name="getLibelleConso" returntype="string" access="public" output="false" hint="Raméne le périmétre de la requéte et le mode de calcul (Usage/facturation)">
		<cfreturn "Consommations">
	</cffunction>
	
	<cffunction name="getNumero" returntype="string" access="public" output="false">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
			<cfquery name="qGetCompte" datasource="#application.OFFREDSN#">
				select sous_tete
				from sous_tete st
				where st.idsous_tete=#ID#
			</cfquery>
			<cfreturn qGetCompte.sous_tete>
	</cffunction>
</cfcomponent>
