<html>
    <head>
        <title>Vos informations de connexion &agrave; ConsoView</title>

<style type="text/css">

             .site {
		             font-family: Arial, Helvetica, sans-serif;
		             font-size: 12px;
		             color: #336600;
		             font-weight: bold;
		             clip: rect(auto,auto,auto,auto);
		            }

             .titre {
		             font-family: Arial, Helvetica, sans-serif;
		             font-weight: bold;
		            }

			.titre_info{
							align: center;
				        	color: #006600;
				        	font-size: 15px;
				        	font-family: Arial, Helvetica, sans-serif;
				        	font-weight: bold;
			       	   }/*affine les proprit du titre sujet2 */
		
			.sujet_titre{
				        	color: #FF0000;
				        	font-size: 20px;
				        	font-family: Arial, Helvetica, sans-serif;
				        	font-weight: bold;
				        }/*affine les proprit du titre sujet2 */
             .text {
		             font-family: Arial, Helvetica, sans-serif;
		             font-size: 14px;
		             font-weight: normal;
		             margin-top: 15px;
             	   }

             .suite {
		             font-family: Arial, Helvetica, sans-serif;
		             font-size: 13px;
		             color: #666666;
		            }
        </style>
    </head>
			    
	<BODY bgcolor="#aaaaa9" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<center>
			<TABLE bgcolor="#ffffff" width="600" border="0" cellpadding="0" cellspacing="0" height="500">
	           	<TR height="20"/>
	            <TR>
		            <TD colspan="4">
						<IMG src="http://www.consotel.fr/newsletter/images/SNCF_SFR_bandeau_app.jpg" width="600">
					</TD>
	            </TR>
	            <TR>
					<TD colspan="4">
						<span class="titre_info">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Informations de connexion à la BOL SFR
						</span>
						<br><br>
					</TD>
	            </TR>
	            <TR>
	                <TD>
                       <div style="align: center;font-color: #b1b1b1>;">
	                        <A class="site" href="https://cmd-sncf.consotel.fr" target="blank">
	                        	<img src="http://www.consotel.fr/newsletter/images/lien_ordi.jpg" border="0">
	                        </A>
                       </div> 
	           		</TD>
	           		<TD width="50"/>
	                <TD>
	               		<span class="sujet_titre">
		                  	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vos informations<br>
		                  	<div align="center">de connexion &agrave; BOL SFR</div>
	             	  	</span>
	             		<br>
		             	  
	             		<span class="suite">
	                  	<div style="text-align:justify;">
						<b>Bonjour #tableau[2]# #Ucase(tableau[1])#,</b>
						<br>
						<br>
						Suite au pr&eacute;c&eacute;dent mail contenant l'adresse de connexion &agrave; la BOL SFR et votre login, nous avons le plaisir de vous faire parvenir votre mot de passe.
						<br>
						<br>
						&raquo; <b>Mot de passe :  <font style="font-size:14px;">#tableau[4]#</font></b>
						<br>
						<br>
						Pour tout renseignement concernant votre connexion et l?utilisation de ConsoView vous pouvez contacter le service support ConsoView
						<br>
						<br>
						<br>
						Cordialement,
						<br>
						<b>L'&eacute;quipe support Grands Comptes</b><br>
	                   	----------------------------------------------------<br>
	                   
	                   	<font size="-2">
		                   	ConsoTel - Conseil et logiciels pour la gestion de vos t&eacute;l&eacute;coms<br>
	                   		Tel support :.................. <b>04 37 51 51 51</b><br>
	                   		Email support :.............. <a href="mailto:sfr-sncf@sfrserviceclient.fr ">sfr-sncf@sfrserviceclient.fr</A> <br>
	                   	</font>
	              	</TD>
	           		<TD width="50"/>
	           	</TR>
	           	<TR height="20"/>
	  		</TABLE>
		</center>
	</BODY>
</html>