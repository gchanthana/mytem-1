<cfcomponent name="GestionClient" alias="fr.consotel.consoview.parametres.login.GestionClient">
	<cfset pkg = "PKG_INTRANET">
	<cffunction name="getLoginOfGroupe" access="remote" returntype="query">
		<cfargument name="idRacine" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#pkg#.getLogin">
			<cfprocparam value="#idRacine#" cfsqltype="CF_SQL_INTEGER"/>
			<cfprocresult name="qGetLoginOfGroupe">
		</cfstoredproc>
		<cfreturn qGetLoginOfGroupe>
	</cffunction>
	
	<cffunction name="ADD_SEVERAL_ADRESSE_IP" access="remote" returntype="numeric">
		<cfargument name="IDGROUPE_CLIENT" required="true" type="numeric">
		<cfargument name="ADRESSE_IP" required="true" type="String">
		<cfargument name="COMMENTAIRE" required="true" type="String">
		<cfargument name="CREATEUR" required="true" type="numeric">
		<cfargument name="MASQUE1" required="true" type="String">
		<cfargument name="MASQUE2" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#pkg#.ADD_SEVERAL_ADRESSE_IP_V1">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDGROUPE_CLIENT#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#ADRESSE_IP#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#COMMENTAIRE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#CREATEUR#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#MASQUE1#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MASQUE2#"/>
	        <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="addSeveralStatus"/>
		</cfstoredproc>
		<cfreturn addSeveralStatus>
	</cffunction>
	
	<cffunction name="UPDATE_RACINE_INFO" access="remote" returntype="numeric">
		
		<cfargument name="IDRACINE" required="true" type="numeric">
		<cfargument name="VALIDITE_DEBUT" required="true" type="date" hint="dd/mm/yyyy">
		<cfargument name="VALIDITE_FIN" required="true" type="date" hint="dd/mm/yyyy">
		<cfargument name="NB_LOGIN" required="true" type="numeric" default="0">
		<cfargument name="MARGE" required="true" type="numeric" default="0">
		
		<cfargument name="NB_PRODUIT_FIXE" required="true" type="numeric" default="0">
		<cfargument name="NB_PRODUIT_MOBILE" required="true" type="numeric" default="0">
		<cfargument name="NB_PRODUIT_DATA" required="true" type="numeric" default="0">
		<cfargument name="BUDGET_FIXE" required="true" type="numeric" default="0">
		<cfargument name="BUDGET_MOBILE" required="true" type="numeric" default="0">
		<cfargument name="BUDGET_DATA" required="true" type="numeric" default="0">
		<cfargument name="NB_SITE_INSTALL" required="true" type="numeric" default="0">
		<cfargument name="MOD_FACTURATION" required="true" type="numeric" default="0">
		<cfargument name="MOD_USAGE" required="true" type="numeric" default="0">
		<cfargument name="MOD_WORKFLOW" required="true" type="numeric" default="0">
		<cfargument name="MOD_FIXE_DATA" required="true" type="numeric" default="0">
		<cfargument name="MOD_MOBILE" required="true" type="numeric" default="0">
		<cfargument name="MOD_GESTION_ORG" required="true" type="numeric" default="0">
		<cfargument name="MOD_GESTION_LOGIN" required="true" type="numeric" default="0">
		<cfargument name="MOD_GESTION_FOURNIS" required="true" type="numeric" default="0">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#pkg#.UPDATE_RACINE_INFO_V1">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDRACINE#"/>
			<cfprocparam cfsqltype="CF_SQL_DATE"  type="in" value="#VALIDITE_DEBUT#"/>
			<cfprocparam cfsqltype="CF_SQL_DATE"  type="in" value="#VALIDITE_FIN#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#NB_LOGIN#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MARGE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#NB_PRODUIT_FIXE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#NB_PRODUIT_MOBILE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#NB_PRODUIT_DATA#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#BUDGET_FIXE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#BUDGET_MOBILE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#BUDGET_DATA#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#NB_SITE_INSTALL#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MOD_FACTURATION#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MOD_USAGE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MOD_WORKFLOW#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MOD_FIXE_DATA#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MOD_MOBILE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MOD_GESTION_ORG#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MOD_GESTION_LOGIN#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MOD_GESTION_FOURNIS#"/>
	        <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="updateStatus"/>
		</cfstoredproc>
		<cfreturn updateStatus>
	</cffunction>
	
	<cffunction name="GetLoginInfos" output="true" access="remote" returntype="query">
		<cfargument name="frm_username" type="String" default="0">
		<cfargument name="frm_password" type="String" default="0">
		<cftry>
		<cfset userOK=1>
		<cfldap filter="sAMAccountName=#frm_username#" action="query" server="192.168.3.123" username="consotel\#frm_username#" password="#frm_password#" 
				attributes="mailNickname,cn,mail,physicalDeliveryOfficeName,wWWHomePage,info,description" start="OU=SBSUsers,OU=Users,OU=MyBusiness,DC=consotel,DC=fr" name="qGetUser">
		<cfcatch type="any">
			<cfset userOK=0>
		</cfcatch>
		</cftry>
		<cfif userOK eq 1>
			<cfif qGetuser.recordCount neq 0>
				<cfset session.mailNickname=#qGetuser.mailNickname#>
				<cfset session.cn=#qGetuser.cn#>
				<cfset session.mail=#qGetuser.mail#>
				<cfset session.Quarantaine=qGetuser.wWWHomePage>
				<cfset session.status=1>
				<cfset session.username=qGetuser.mailNickname>
				<cfset session.erreur="0">
				<cfset session.tempsFirstConnexion=Now()>
				<cfset session.NombreClics=0>
				<cfset session.user.email=qGetuser.cn>
				<cfset session.clientID=qGetuser.physicalDeliveryOfficeName>
			<cfelse>
				<cfset session.erreur="1">
				<cfset session.status=0>
				<cfset session.login="0">
			</cfif>
		<cfelse>
			<cfset session.erreur="1">
			<cfset session.status=0>
			<cfset session.login="0">
		</cfif>		
		<cfreturn qGetUser>
	</cffunction>

	<cffunction name="getGroupesMaitres" output="true" access="remote" returntype="query">
		<cfargument name="IDGROUPE_CLIENT" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#pkg#.GET_RACINE_INFO_V1">
		<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDGROUPE_CLIENT#"/>

			<cfprocresult name="qGetInfoRacine">
		</cfstoredproc>
		<cfreturn qGetInfoRacine>
	</cffunction>
	<cffunction name="getIDStyle" output="true" access="remote" returntype="query">
		<cfargument name="IDAPP_LOGIN" required="true" type="numeric">
		<cfargument name="IDGROUPE_CLIENT" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#pkg#.GET_LOGIN_STYLE_V1">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDAPP_LOGIN#"/>
		<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDGROUPE_CLIENT#"/>
			<cfprocresult name="qGetInfoRacine">
		</cfstoredproc>
		<cfreturn qGetInfoRacine>
	</cffunction>
	
	<cffunction name="updateRacineInfos" access="remote" returntype="numeric">
		<cfargument name="APP_LOGIN_ID" required="true" type="numeric">
		<cfargument name="OLD_ADRESSE_IP" required="true" type="string">
		<cfargument name="OLD_MASQUE1" required="true" type="string">
		<cfargument name="ADRESSE_IP" required="true" type="string">
		<cfargument name="COMMENTAIRE" required="true" type="string">
		<cfargument name="CREATEUR" required="true" type="numeric">
		<cfargument name="MASQUE1" required="true" type="string">
		<cfargument name="MASQUE2" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#pkg#.UpdateAdresseIP">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#APP_LOGIN_ID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#OLD_ADRESSE_IP#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#ADRESSE_IP#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#COMMENTAIRE#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#OLD_MASQUE1#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#MASQUE1#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#MASQUE2#"/>
	        <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
 
	<cffunction name="getAdresseIp" access="remote" returntype="query">
		<cfargument name="APP_LOGIN_ID" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#pkg#.GetAdresseIP">
			<cfprocparam value="#APP_LOGIN_ID#" cfsqltype="CF_SQL_INTEGER"/>
			<cfprocresult name="qGetAdresseIp">
		</cfstoredproc>
		<cfreturn qGetAdresseIp>
	</cffunction>
	
	<cffunction name="addAdresseIp" access="remote" returntype="numeric">
		<cfargument name="APP_LOGIN_ID" required="true" type="numeric">
		<cfargument name="ADRESSE_IP" required="true" type="string">
		<cfargument name="COMMENTAIRE" required="true" type="string">
		<cfargument name="CREATEUR" required="true" type="numeric">
		<cfargument name="MASQUE1" required="true" type="string">
		<cfargument name="MASQUE2" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#pkg#.AddAdresseIP">
			<cfprocparam value="#APP_LOGIN_ID#" cfsqltype="CF_SQL_INTEGER"/>
			<cfprocparam value="#ADRESSE_IP#" cfsqltype="CF_SQL_VARCHAR"/>
			<cfprocparam value="#COMMENTAIRE#" cfsqltype="CF_SQL_VARCHAR"/>
			<cfprocparam value="#CREATEUR#" cfsqltype="CF_SQL_INTEGER"/>
			<cfprocparam value="#MASQUE1#" cfsqltype="CF_SQL_VARCHAR"/>
			<cfprocparam value="#MASQUE2#" cfsqltype="CF_SQL_INTEGER"/>
	        <cfprocparam variable="result" cfsqltype="CF_SQL_INTEGER" type="out"/>
		</cfstoredproc>
		<cfreturn result>
	</cffunction>

	<cffunction name="updateAdresseIp" access="remote" returntype="numeric">
		<cfargument name="APP_LOGIN_ID" required="true" type="numeric">
		<cfargument name="OLD_ADRESSE_IP" required="true" type="string">
		<cfargument name="OLD_MASQUE1" required="true" type="string">
		<cfargument name="ADRESSE_IP" required="true" type="string">
		<cfargument name="COMMENTAIRE" required="true" type="string">
		<cfargument name="CREATEUR" required="true" type="numeric">
		<cfargument name="MASQUE1" required="true" type="string">
		<cfargument name="MASQUE2" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#pkg#.UpdateAdresseIP">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#APP_LOGIN_ID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#OLD_ADRESSE_IP#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#ADRESSE_IP#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#COMMENTAIRE#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#OLD_MASQUE1#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#MASQUE1#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#MASQUE2#"/>
	        <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="deleteAdresseIp" access="remote" returntype="numeric">
		<cfargument name="APP_LOGIN_ID" required="true" type="numeric">
		<cfargument name="ADRESSE_IP" required="true" type="string">
		<cfargument name="MASQUE1" required="true" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#pkg#.DelAdresseIP">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#APP_LOGIN_ID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#ADRESSE_IP#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#MASQUE1#"/>
	        <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getListeOperateur" access="remote" returntype="Query" output="false" >
		<cfargument name="idgroupe" type="numeric" default="0">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_V3.SF_LISTEOP">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idgroupe#"/>
	        <cfprocresult name="listeFactures"/>        
		</cfstoredproc>
		<cfquery name="getListe" dbtype="query">
			select opnom as label, operateurID as data
			from listeFactures
		</cfquery>
		<cfreturn getListe>
	</cffunction>
		
	<cffunction name="getOrgas" output="true" access="remote" returntype="query">
		<cfargument name="idgroupe" type="numeric" default="0">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			select libelle_groupe_client, idgroupe_client, 
					decode(type_orga,'CUS','Client','GEO','G?ographique','OPE','Op?rateur','ANA','Analytique') as type_orga, gc.operateurID,
					date_creation, op.nom, type_orga as type,'' as ladate
			from groupe_client gc, operateur op
			where gc.id_groupe_maitre=#idgroupe#
			and gc.operateurID=op.operateurID (+)
			order by lower(gc.libelle_groupe_client)
		</cfquery>
		<cfloop from="1" to="#result.recordcount#" index="i">
			<cfset result.ladate[i]=LsDateFormat(result.DATE_CREATION[i],"dd/mm/yyyy")>
		</cfloop>
		<cfreturn result>
	</cffunction>
			
	<cffunction name="checkOrgaGeo" output="true" access="remote" returntype="boolean">
		<cfargument name="idgroupe" type="numeric" default="0">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			select 
					decode(type_orga,'CUS','Client','GEO','G?ographique','OPE','Op?rateur','ANA','Analytique') as type_orga					
			from groupe_client gc
			where gc.id_groupe_maitre=#idgroupe#
			and type_orga='GEO'
		</cfquery>
		<cfif result.recordcount eq 0>
			<cfset flag=false>
		<cfelse>
			<cfset flag=true>
		</cfif>
		<cfreturn flag>
	</cffunction>
	
	<cffunction name="addOrga" output="true" access="remote" returntype="any">
		<cfargument name="idgroupe" type="numeric">
		<cfargument name="type_orga" type="String">
		<cfargument name="operateurID" type="numeric">
		<cfargument name="libelle_cus" type="String">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_consoview_v3.create_orga">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#idgroupe#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#type_orga#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#operateurID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#libelle_cus#"/>
	        <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getInfosOrga" output="true" access="remote" returntype="query">
		<cfargument name="idracine" type="numeric">
		<cfargument name="idgroupe" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLOBAL.p_info_orga">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#idracine#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#idgroupe#"/>
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="deleteOrga" output="true" access="remote" returntype="any">
		<cfargument name="idgroupe" type="numeric">
		<cfargument name="type_orga" type="String">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_consoview_v3.remove_orga">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#idgroupe#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#type_orga#"/>
	        <cfprocparam cfsqltype="CF_SQL_FLOAT"  type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn result>
	</cffunction>

	<cffunction name="getListeConsotel" output="true" access="remote" returntype="query">
		<cfldap action="query" filter="company=CONSOTEL" server="192.168.3.123" username="consotel\infofacture" password="edifact" 
				attributes="mailNickname,cn,mail,physicalDeliveryOfficeName" start="OU=SBSUsers,OU=Users,OU=MyBusiness,DC=consotel,DC=fr" name="getUser" sort="cn">
		<cfquery name="result" dbtype="query">
			select *, MAILNICKNAME as data, CN as label
			from getUser
		</cfquery>
		<cfreturn result>
	</cffunction>
		
	<cffunction name="getFiles" output="true" access="remote" returntype="query">
		<cfargument name="extension" type="Array">
		<cfdirectory action="list" filter="*.#extension[1]#" name="qListe" sort="name asc" directory="\\dbcl2\web_directory\admin\autres\import_orga">
		<cfquery name="result" dbtype="query">
			select name as label, name as data
			from qListe
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="deleteFile" output="true" access="remote" returntype="string">
		<cfargument name="file" type="String">
		<cftry>
			<cffile action="delete" file="\\dbcl2\web_directory\admin\autres\import_orga\#file#">
			<cffile action="delete" file="\\dbcl2\web_directory\admin\autres\import_orga\#file#.log">			
		<cfcatch type="any">
		</cfcatch>
		</cftry>
		<cfreturn "ok">
	</cffunction>
	
	<cffunction name="testFile" output="true" access="remote" returntype="query">
		<cfargument name="idgroupe" type="numeric">
		<cfargument name="file" type="String">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_consoview_v3.p_test_sous_tete">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#idgroupe#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#file#"/>
	        <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
		<cfset q=queryNew("erreur,data")>
		<cfif result eq 1>
			<cffile action="read" file="\\dbcl2\web_directory\admin\autres\import_orga\#file#.log" variable="hFile">
			<cfset t=ListToArray(hFile,chr(10))>
			<cfloop from="3" to="#ArrayLen(t)#" index="i" step="2">
				<cfset queryAddRow(q,1)>
				<cfset querySetCell(q,"erreur",t[i-1])>
				<cfset querySetCell(q,"data",t[i])>
			</cfloop>
		</cfif>
		<cfreturn q>
	</cffunction>
		
	<cffunction name="endAccessSession" access="remote" returntype="numeric">
		<cfset structclear(session)>
		<cfreturn 1>
	</cffunction>

	<cffunction name="getArbre" access="remote" returntype="xml">	
		<cfargument name="nodeId" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.enfant_noeud">
			<cfprocparam  value="#nodeId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetChild">
		</cfstoredproc>
		
		<cfset childXmlTree = XmlNew()>
		<cfset childXmlTree.xmlRoot = XmlElemNew(childXmlTree,"NODE")>
		<cfset rootNode = childXmlTree.NODE>
		<cfset rootNode.XmlAttributes.LBL = "#SESSION.PERIMETRE.GROUPE#">
		<cfset rootNode.XmlAttributes.NID = nodeId>
		<cfset rootNode.XmlAttributes.NTY = 1>
		<cfset rootNode.XmlAttributes.STC = 1>
		<cfif qGetChild.recordcount EQ 0>
			<cfset rootNode.XmlAttributes.NTY = 0>
		<cfelse>
				<cfset rootNode.XmlAttributes.NTY = 1>
		</cfif>
		<cfloop index="i" from="1" to="#qGetChild.recordcount#">
			<cfset rootNode.XmlChildren[i] = XmlElemNew(childXmlTree,"NODE")>
			<cfset rootNode.XmlChildren[i].XmlAttributes.LBL = qGetChild['LIBELLE_GROUPE_CLIENT'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NID = qGetChild['IDGROUPE_CLIENT'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NTY = qGetChild['TYPE_NOEUD'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.STC = 1>
		</cfloop>
		
		<cfset qGetChild = ''>
		<cfreturn childXmlTree>
	</cffunction>

<cffunction name="getNodeXmlPath" access="remote" returntype="xml">
		<cfargument name="idGroupeRacine" required="true" type="numeric">
		<cfargument name="idSource" required="true" type="numeric">
		<cfargument name="idCible" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.chemin_source_cible_v2">
			<cfprocparam  value="#idSource#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#idCible#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="641" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qChemin">
		</cfstoredproc>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"NODE")>
		<cfset rootNode = perimetreXmlDoc.NODE>
		<cfif qChemin.recordcount GT 0>
			<cfset typeOrga = qChemin['TYPE_ORGA'][qChemin.recordcount]>
			<cfloop index="i" from="1" to="#qChemin.recordcount#">
				<cfif i EQ 1>
					<cfset rootNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
					<cfset rootNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset rootNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
					<cfset rootNode.XmlAttributes.STC = qChemin['STC'][i]>
					<cfif idSource EQ idGroupeRacine>
						<cfif qChemin.recordcount GT 1>
							<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"NODE")>
							<cfswitch expression="#UCASE(typeOrga)#">
								<cfcase value="OPE">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations OpÃ©rateurs">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -2>
								</cfcase>
								<cfcase value="CUS">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Clientes">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -3>
								</cfcase>
								<cfcase value="SAV">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Recherches SauvegardÃ©es">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -4>
								</cfcase>
								<cfdefaultcase>
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Clientes">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -3>
								</cfdefaultcase>
							</cfswitch>
							<cfset rootNode.XmlChildren[1].XmlAttributes.NTY = 0>
							<cfset rootNode.XmlChildren[1].XmlAttributes.STC = 0>
						</cfif>
					</cfif>
				<cfelseif i EQ 2>
					<cfset parentNode = ''>
					<cfif idSource EQ idGroupeRacine>
						<cfset rootNode.XmlChildren[1].XmlAttributes.NTY = 1>
						<cfset parentNode = rootNode.XmlChildren[1]>
					<cfelse>
						<cfset tmpNID = qChemin['IDGROUPE_CLIENT'][i]>
						<cfset tmpParentNID = qChemin['IDGROUPE_CLIENT'][i - 1]>
						<cfoutput>
							<cfset nodeArray = XMLSearch(perimetreXmlDoc,"//NODE[@NID=#tmpParentNID#]")>
							<cfif arrayLen(nodeArray) NEQ 1>
								<cfthrow type="INVALID_GROUP_NODES_QUERY"
										message="Requï¿½te Arbre PÃ©rimï¿½tres Invalide"
										detail="1006">
							</cfif>
							<cfset parentNode = nodeArray[1]>
						</cfoutput>
					</cfif>
					<cfset parentChildNb = arrayLen(parentNode.XmlChildren)>
					<cfset parentNode.XmlChildren[parentChildNb + 1] = XmlElemNew(perimetreXmlDoc,"NODE")>
					<cfset tmpNode = parentNode.XmlChildren[parentChildNb + 1]>
					<cfset tmpNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
					<cfset tmpNode.XmlAttributes.STC = qChemin['STC'][i]>
				<cfelse>
					<cfset tmpNID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset tmpParentNID = qChemin['IDGROUPE_CLIENT'][i - 1]>
					<cfoutput>
						<cfset nodeArray = XMLSearch(perimetreXmlDoc,"//NODE[@NID=#tmpParentNID#]")>
						<cfif arrayLen(nodeArray) NEQ 1>
							<cfthrow type="INVALID_GROUP_NODES_QUERY"
									message="Requï¿½te Arbre PÃ©rimï¿½tres Invalide"
									detail="1006">
						</cfif>
						<cfset parentNode = nodeArray[1]>
						<cfset parentChildNb = arrayLen(parentNode.XmlChildren)>
						<cfset parentNode.XmlChildren[parentChildNb + 1] = XmlElemNew(perimetreXmlDoc,"NODE")>
						<cfset tmpNode = parentNode.XmlChildren[parentChildNb + 1]>
						<cfset tmpNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
						<cfset tmpNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
						<cfset tmpNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
						<cfset tmpNode.XmlAttributes.STC = qChemin['STC'][i]>
					</cfoutput>
				</cfif>
			</cfloop>
		<cfelse>
			<cfset rootNode.XmlAttributes.LBL = "Pas de chemin trouvÃ©">
			<cfset rootNode.XmlAttributes.NID = -1>
			<cfset rootNode.XmlAttributes.NTY = 0>
			<cfset rootNode.XmlAttributes.STC = 0>
		</cfif>
		<cfset qChemin = ''>
		<cfreturn perimetreXmlDoc>
	</cffunction>

 <cffunction name="getGroupNodes" access="remote" returntype="xml">
    <cfargument name="groupeId" required="true" type="numeric">
	<cfargument name="idUser" required="true" type="numeric">
    	<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.Login_nodes_acces_v2">
           <cfprocparam value="#groupeId#" cfsqltype="CF_SQL_INTEGER">
           <cfprocparam  value="#idUser#" cfsqltype="CF_SQL_INTEGER">
           <cfprocresult name="qGetNodeList">
        </cfstoredproc>

        <cfset perimetreXmlDoc = XmlNew()>
        <cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"NODE")>
        <cfset rootNode = perimetreXmlDoc.NODE>
        <cfif qGetNodeList.recordcount EQ 0>
       		 <cfthrow type="EMPTY_NODE_ACCESS" message="CV: No access node available" detail="1005">
        <cfelseif qGetNodeList['STC'][qGetNodeList.recordcount] EQ 0>
			
			<cfset rootNode.XmlAttributes.LBL = "Arbre vide">
            <cfset rootNode.XmlAttributes.NID = qGetNodeList['IDGROUPE_CLIENT'][qGetNodeList.recordcount]>
            <cfset rootNode.XmlAttributes.NTY = qGetNodeList['TYPE_NOEUD'][qGetNodeList.recordcount]>
            <cfset rootNode.XmlAttributes.STC = qGetNodeList['STC'][qGetNodeList.recordcount]>
         <cfelse>

            	<cfloop index="i" from="1" to="#qGetNodeList.recordcount#">
             		<cfif i EQ 1>

			            <cfset rootNode.XmlAttributes.LBL = qGetNodeList['LIBELLE_GROUPE_CLIENT'][i]>
			            <cfset rootNode.XmlAttributes.NID = qGetNodeList['IDGROUPE_CLIENT'][i]>
			            <cfset rootNode.XmlAttributes.NTY = qGetNodeList['TYPE_NOEUD'][i]>
			            <cfset rootNode.XmlAttributes.STC = qGetNodeList['STC'][i]>
			
			      		<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"NODE")>
			            <cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Op?rateurs">
			            <cfset rootNode.XmlChildren[1].XmlAttributes.NID = -2>
			            <cfset rootNode.XmlChildren[1].XmlAttributes.NTY = 0>
			            <cfset rootNode.XmlChildren[1].XmlAttributes.STC = 0>
			
			       
			
			            <cfset rootNode.XmlChildren[2] = XmlElemNew(perimetreXmlDoc,"NODE")>
			            <cfset rootNode.XmlChildren[2].XmlAttributes.LBL = "Liste Des Organisations Clientes">
			            <cfset rootNode.XmlChildren[2].XmlAttributes.NID = -3>
			            <cfset rootNode.XmlChildren[2].XmlAttributes.NTY = 0>
			            <cfset rootNode.XmlChildren[2].XmlAttributes.STC = 0>
			
			            
			
			            <cfset rootNode.XmlChildren[3] = XmlElemNew(perimetreXmlDoc,"NODE")>
			            <cfset rootNode.XmlChildren[3].XmlAttributes.LBL = "Liste Des Recherches Sauvegard?es">
			            <cfset rootNode.XmlChildren[3].XmlAttributes.NID = -4>
			            <cfset rootNode.XmlChildren[3].XmlAttributes.NTY = 0>
			            <cfset rootNode.XmlChildren[3].XmlAttributes.STC = 0>

            		<cfelse>

		            <cfset tmpNID = qGetNodeList['IDGROUPE_CLIENT'][i]>
		            <cfset tmpParentNID = qGetNodeList['ID_GROUPE_MAITRE'][i]>
             	<cfoutput>

                <cfset nodeArray = XMLSearch(perimetreXmlDoc,"//NODE[@NID=#tmpParentNID#]")>
                <cfif arrayLen(nodeArray) NEQ 1>
                                <cfthrow type="BROKEN_PARENTAL_LINK"
                                                             message="CV: A parental link is broken"

                                                               detail="1007">

                </cfif>

                <cfset parentNode = nodeArray[1]>
                <cfif isXmlRoot(parentNode) EQ TRUE>

                    <cfswitch expression="#qGetNodeList['TYPE_ORGA'][i]#">

	                     <cfcase value="OPE">
	
	                                     <cfset parentNode = rootNode.XmlChildren[1]>
	                                     <cfset rootNode.XmlChildren[1].XmlAttributes.NTY = 1>
	                     </cfcase>
	
	                                     <cfcase value="CUS">
	                                     <cfset parentNode = rootNode.XmlChildren[2]>
	                                     <cfset rootNode.XmlChildren[2].XmlAttributes.NTY = 1> 
	                     </cfcase>
	                                     <cfcase value="SAV">
	                                     <cfset parentNode = rootNode.XmlChildren[3]>
	                                     <cfset rootNode.XmlChildren[3].XmlAttributes.NTY = 1> 
	
	                     </cfcase>
	                     <cfdefaultcase>
	                                     <cfset parentNode = rootNode.XmlChildren[2]>
	                                <cfset rootNode.XmlChildren[2].XmlAttributes.NTY = 1> 
	
	                     </cfdefaultcase>
                    </cfswitch> 

                 </cfif>
                        <cfset parentChildNb = arrayLen(parentNode.XmlChildren)>
                        <cfset parentNode.XmlChildren[parentChildNb + 1] = XmlElemNew(perimetreXmlDoc,"NODE")>
                        <cfset tmpNode = parentNode.XmlChildren[parentChildNb + 1]>
                        <cfset tmpNode.XmlAttributes.LBL = qGetNodeList['LIBELLE_GROUPE_CLIENT'][i]>
                        <cfset tmpNode.XmlAttributes.NID = qGetNodeList['IDGROUPE_CLIENT'][i]>
                        <cfset tmpNode.XmlAttributes.NTY = qGetNodeList['TYPE_NOEUD'][i]>
                        <cfset tmpNode.XmlAttributes.STC = qGetNodeList['STC'][i]>
                        <cfset nodeArray = ''>
                        <cfset parentNode = ''>
                        <cfset parentChildNb = 0>
                        <cfset tmpNode = ''>
                  </cfoutput>
              </cfif>
          </cfloop>
</cfif>
<cfif (qGetNodeList.recordcount EQ 1) AND (qGetNodeList['IDGROUPE_CLIENT'][1] EQ groupeId)>

           <cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.enfant_noeud">
                           <cfprocparam  value="#groupeId#" cfsqltype="CF_SQL_INTEGER">
                           <cfprocresult name="qGetChild">
           </cfstoredproc>
 <cfif qGetNodeList['STC'][qGetNodeList.recordcount] NEQ 0>
			  
           <cfloop index="i" from="1" to="#qGetChild.recordcount#">
					
                           <cfswitch expression="#qGetChild['TYPE_ORGA'][i]#">
                                          <cfcase value="OPE">
                                                          <cfset parentNode = rootNode.XmlChildren[1]>
                                                          <cfset rootNode.XmlChildren[1].XmlAttributes.NTY = 1>
                                          </cfcase>
                                          <cfcase value="CUS">
                                                          <cfset parentNode = rootNode.XmlChildren[2]>
                                                         <cfset rootNode.XmlChildren[2].XmlAttributes.NTY = 1> 
                                          </cfcase>
                                          <cfcase value="SAV">
                                                          <cfset parentNode = rootNode.XmlChildren[3]>
                                                          <cfset rootNode.XmlChildren[3].XmlAttributes.NTY = 1> 
                                          </cfcase>
                                          <cfdefaultcase>

                                                          <cfset parentNode = rootNode.XmlChildren[2]>
                                                          <cfset rootNode.XmlChildren[2].XmlAttributes.NTY = 1> 

                                          </cfdefaultcase>

                           </cfswitch> 
                           <cfset parentChildNb = arrayLen(parentNode.XmlChildren)>
                           <cfset parentNode.XmlChildren[parentChildNb + 1] = XmlElemNew(perimetreXmlDoc,"NODE")>
                           <cfset tmpNode = parentNode.XmlChildren[parentChildNb + 1]>
                           <cfset tmpNode.XmlAttributes.LBL = qGetChild['LIBELLE_GROUPE_CLIENT'][i]>
                           <cfset tmpNode.XmlAttributes.NID = qGetChild['IDGROUPE_CLIENT'][i]>
                           <cfset tmpNode.XmlAttributes.NTY = qGetChild['TYPE_NOEUD'][i]>
                           <cfset tmpNode.XmlAttributes.STC = 1>
                           <cfset nodeArray = ''>
                           <cfset parentNode = ''>
                          <cfset parentChildNb = 0>
                           <cfset tmpNode = ''>

           </cfloop>
</cfif>
                               </cfif>

                               <cfif NOT structKeyExists(SESSION,"PERIMETRE")>
                                               <cfset SESSION.PERIMETRE = structNew()>
                               </cfif>

                               <cfset SESSION.PERIMETRE.ID_GROUPE = groupeId>
                               <cfset SESSION.PERIMETRE.ID_PERIMETRE = qGetNodeList['IDGROUPE_CLIENT'][qGetNodeList.recordcount]>

                               <cfset SESSION.PERIMETRE.LISTE_GROUPES_QUERY = structNew()>
                               <cfset SESSION.PERIMETRE.LISTE_GROUPES_QUERY.IDGROUPE_CLIENT = structNew()>
                               <cfset structInsert(SESSION.PERIMETRE.LISTE_GROUPES_QUERY.IDGROUPE_CLIENT,"#groupeId#",groupeId)>
                               <cfset rootNode = ''>
                               <cfset qGetChild = ''>
                               <cfset qGetNodeList = ''>
                               <cfreturn perimetreXmlDoc>

                </cffunction>

<cffunction name="getFirstAccessNode" access="remote" returntype="struct">

                               <cfargument name="idclients_pv" required="false" type="string">
                               <cfset idPerimetre = SESSION.PERIMETRE.ID_PERIMETRE>
                               <cfset accessNodeInfos = getNodeInfos(idPerimetre)>
                               <cfset SESSION.PERIMETRE.TYPE_PERIMETRE_REEL = accessNodeInfos.TYPE_PERIMETRE_REEL>
                               <cfset SESSION.PERIMETRE.TYPE_PERIMETRE = accessNodeInfos.TYPE_PERIMETRE>
                               <cfset SESSION.PERIMETRE.RAISON_SOCIALE = accessNodeInfos.LBL>
                               <cfset SESSION.PERIMETRE.IDCLIENTS_PV = idclients_pv >
                               <cfset SESSION.PERIMETRE.structDate = accessNodeInfos.FACTURE_PERIOD>

                               <cfset SESSION.PERIMETRE.LISTE_PERIMETRES_QUERY = structNew()>
                               <cfset SESSION.PERIMETRE.LISTE_PERIMETRES_QUERY.IDGROUPE_CLIENT = structNew()>
                               <cfset SESSION.PERIMETRE.LISTE_PERIMETRES_QUERY.ACCES_FIXE = structNew()>
                               <cfset SESSION.PERIMETRE.LISTE_PERIMETRES_QUERY.ACCES_MOBILE = structNew()>
                               <cfset SESSION.PERIMETRE.LISTE_PERIMETRES_QUERY.ACCES_DATA = structNew()>

                               

                               <cfoutput>

                                               <cfset structInsert(SESSION.PERIMETRE.LISTE_PERIMETRES_QUERY.IDGROUPE_CLIENT,"#idPerimetre#",idPerimetre)>
                                               <cfset structInsert(SESSION.PERIMETRE.LISTE_PERIMETRES_QUERY.ACCES_FIXE,"#idPerimetre#",1)>
                                               <cfset structInsert(SESSION.PERIMETRE.LISTE_PERIMETRES_QUERY.ACCES_MOBILE,"#idPerimetre#",1)>
                                               <cfset structInsert(SESSION.PERIMETRE.LISTE_PERIMETRES_QUERY.ACCES_DATA,"#idPerimetre#",1)>

                               </cfoutput>
                               <cfreturn accessNodeInfos>

                </cffunction>

<cffunction name="getStyle" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="query">
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.getStyle">
			<cfprocresult name="result"/>
		</cfstoredproc>	
		<cfreturn result /> 
</cffunction>
<cffunction name="getListeLogin" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="query">
		<cfargument name="IDGROUPE_MAITRE" required="true" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.getListeLogin_V2"><br />
			<cfif IDGROUPE_MAITRE EQ -1>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDGROUPE_MAITRE#" null="true"/>
			<cfelse>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDGROUPE_MAITRE#" null="false"/>
			</cfif>
			<cfprocresult name="result"/>
		</cfstoredproc>
		<cfreturn result /> 
</cffunction>
<cffunction name="getAccess" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="query">
		<cfargument name="ID" required="true" type="Numeric" />
		<cfargument name="IDGROUPE_MAITRE" required="true" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.getAccess_V3">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#ID#"/>
			<cfif IDGROUPE_MAITRE EQ -1>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDGROUPE_MAITRE#" null="true"/>
			<cfelse>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDGROUPE_MAITRE#" null="false"/>
			</cfif>
			<cfprocresult name="result"/>
		</cfstoredproc>
		<cfset listIdAccesTab = arraynew(1)>
		<cfloop query="result" >
			<cfset arrayappend(listIdAccesTab,result.ID) >
		</cfloop>

		<cfset resultat=getListeRacine(listIdAccesTab) >
		
		<cfset listIdMaitre = arraynew(1)>
		<cfloop query="resultat" >
			<cfset arrayappend(listIdMaitre,resultat.idGroupeMaitre)>
		</cfloop>
		
		<cfset listlibelleMaitre = arraynew(1)>
		<cfloop query="resultat" >
			<cfset arrayappend(listlibelleMaitre,resultat.libellegm)>
		</cfloop>	
	
		<cfset nColumnNumber = QueryAddColumn(result,"idGroupeMaitre","Integer",listIdMaitre)>
		<cfset nColumnNumber = QueryAddColumn(result,"libellegm","Varchar",listlibelleMaitre)>
		<cfreturn result />
</cffunction>


<cffunction name="getLogin" output="false" description="Returns a query with names (id,name,age,gender)"access="remote" returntype="query">
		<cfargument name="ID" required="true" type="numeric" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.getLogin">
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#ID#"/>
					<cfprocresult name="result"/>
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>

<cffunction name="send_Login" output="false" description="" access="remote" returntype="void">
	<cfargument name="tableau" required="true" type="array" />
		<cfmail from="support@consotel.fr" to="#tableau[3]#" server="192.168.3.119" port="26" type="text/html" 
				subject="Accès ConsoView - 1/2" charset="utf-8" bcc="support@consotel.fr">

			<html>
			    <head>
			        <title>Vos informations de connexion &agrave; ConsoView</title>
			
			        <style type="text/css">
									             a.site:link {
			              text-decoration: underline;
			              color: ##3366FF;
			              font-size: 14px;
			             } /* lien non-visit */
			             a.site:visited {
			            text-decoration: underline;
			             color: ##3366FF;
			              font-size: 14px;
			             } /* lien visit */
			             a.site:hover {
			             text-decoration: underline;
			             color: ##FE860C;
			              font-size: 14px;
			             } /* lien survol */
			
			             a.sujet:link {
			             color: ##000000;
			             text-decoration: none;
			             } /* lien non-visit */
			             a.sujet:visited {
			             text-decoration: none;
			             color: ##000000;
			             } /* lien visit */
			             a.sujet:hover {
			             text-decoration: underline;
			             } /* lien survol */
			
			
			             a.news:link {
			             color: ##777777;
			             text-decoration: none;
			             display:list-item;
			             list-style-image: url(http://www.consotel.fr/newsletter/images/pucenews.gif);
			             list-style-position:inside;
			             } /* lien non-visit */
			             a.news:visited {
			             text-decoration: none;
			             color: ##777777;
			             } /* lien visit */
			             a.news:hover {
			             text-decoration: underline;
			             } /* lien survol */
			
			             a.notes:link {
			             color: ##777777;
			             text-decoration: none;
			             display:list-item;
			             list-style-image: url(http://www.consotel.fr/newsletter/images/pucenotes.gif);
			             list-style-position:inside;
			             } /* lien non-visit */
			             a.notes:visited {
			             text-decoration: none;
			             color: ##777777;
			             } /* lien visit */
			             a.notes:hover {
			             text-decoration: underline;
			             } /* lien survol */
			             
			             .site {
					             font-family: Arial, Helvetica, sans-serif;
					             font-size: 12px;
					             color: ##336600;
					             font-weight: bold;
					             clip: rect(auto,auto,auto,auto);
					            }
			
			             .titre {
					             font-family: Arial, Helvetica, sans-serif;
					             font-weight: bold;
					            }
			
						.titre_info{
										align: center;
							        	color: ##006600;
							        	font-size: 15px;
							        	font-family: Arial, Helvetica, sans-serif;
							        	font-weight: bold;
						       	   }/*affine les proprit du titre sujet2 */
					
						.sujet_titre{
							        	color: ##FF0000;
							        	font-size: 20px;
							        	font-family: Arial, Helvetica, sans-serif;
							        	font-weight: bold;
							        }/*affine les proprit du titre sujet2 */
			             .text {
					             font-family: Arial, Helvetica, sans-serif;
					             font-size: 14px;
					             font-weight: normal;
					             margin-top: 15px;
			             	   }
			
			             .suite {
					             font-family: Arial, Helvetica, sans-serif;
					             font-size: 13px;
					             color: ##666666;
					            }
			        </style>
			    </head>
						    
				<BODY bgcolor="##aaaaa9" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
					<center>
						<TABLE bgcolor="##ffffff" width="600" border="0" cellpadding="0" cellspacing="0" height="500">
				            <TR>
					            <TD colspan="4">
									<IMG src="http://www.consotel.fr/newsletter/images/SNCF_SFR_bandeau_app.jpg" width="600">
								</TD>
				            </TR>
				            <TR>
								<TD colspan="4">
									<br>
									<span class="titre_info">
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										Informations de connexion à la BOL SFR
									</span>
									<br><br>
								</TD>
				            </TR>
				            <TR>
				                <TD>
			                       <div style="align: center;font-color: ##b1b1b1>;">
				                        <A class="site" href="https://cmd-sncf.consotel.fr" target="blank">
				                        	<img src="http://www.consotel.fr/newsletter/images/lien_ordi.jpg" border="0">
				                        </A>
			                       </div> 
				           		</TD>
				           		<TD width="50"/>
				                <TD>
				               		<span class="sujet_titre">
					                  	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vos informations<br>
					                  	<div align="center">de connexion &agrave; BOL SFR</div>
				             	  	</span>
				             		<br>
					             	  
				             		<span class="suite">
				                  	<div style="text-align:justify;">
				                   	<b>Bonjour #tableau[2]# #Ucase(tableau[1])#,</b>
				                   	<br>
				                   	<br>
				                   	Nous avons le plaisir de vous faire parvenir les informations de connexion &agrave; la plateforme de commande SFR.
				                   	<br>
				                   	<br>
				                   	Vous trouverez ci-dessous l'adresse de connexion (s&eacute;curis&eacute;e) ainsi que votre login.
				                   	Votre mot de passe vous est envoy&eacute; dans un second mail.
				                   	<br>
				                   	<br>
				                   	&raquo; <b>Adresse de connexion :....</b> <A class="site" href="https://cmd-sncf.consotel.fr " target="blank">https://cmd-sncf.consotel.fr </A> <br>
				                   	&raquo; <b>Login :  <font style="font-size:14px;">#tableau[3]#</font></b>
				                   	<br>
				                   	<br>
				                   	Pour tout renseignement concernant votre connexion et l'utilisation de la plateforme de commande SFR, vous pouvez contacter le service support client Grands Comptes.
				                   	<br>
				                   	<br>
				                   	<br>
				                   	Cordialement,<br>
				                   	<b>L'&eacute;quipe support Grands Comptes</b><br>
				                   	----------------------------------------------------<br>
				                   
				                   	<font size="-2">
					                   	ConsoTel - Conseil et logiciels pour la gestion de vos t&eacute;l&eacute;coms<br>
				                   		Tel support :.................. <b>04 37 51 51 51</b><br>
				                   		Email support :.............. <a href="mailto:sfr-sncf@sfrserviceclient.fr ">sfr-sncf@sfrserviceclient.fr</A> <br>
				                   	</font>
				              	</TD>
				           		<TD width="50"/>
				           	</TR>
				           	<TR height="20"/>
				  		</TABLE>
					</center>
				</BODY>
			</html>

		</cfmail>
</cffunction>

<cffunction name="send_Pass" output="false" description="" access="remote" returntype="void">
	<cfargument name="tableau" required="true" type="array" />
		<cfmail from="support@consotel.fr" to="#tableau[3]#" server="192.168.3.119" port="26" type="text/html" 
				subject="Accès ConsoView - 2/2" charset="utf-8" bcc="support@consotel.fr" >
			<html>
			    <head>
			        <title>Vos informations de connexion &agrave; ConsoView</title>
			
					<style type="text/css">
						
									             a.site:link {
			              text-decoration: underline;
			              color: ##3366FF;
			              font-size: 14px;
			             } /* lien non-visit */
			             a.site:visited {
			            text-decoration: underline;
			             color: ##3366FF;
			              font-size: 14px;
			             } /* lien visit */
			             a.site:hover {
			             text-decoration: underline;
			             color: ##FE860C;
			              font-size: 14px;
			             } /* lien survol */
			
			             a.sujet:link {
			             color: ##000000;
			             text-decoration: none;
			             } /* lien non-visit */
			             a.sujet:visited {
			             text-decoration: none;
			             color: ##000000;
			             } /* lien visit */
			             a.sujet:hover {
			             text-decoration: underline;
			             } /* lien survol */
			
			
			             a.news:link {
			             color: ##777777;
			             text-decoration: none;
			             display:list-item;
			             list-style-image: url(http://www.consotel.fr/newsletter/images/pucenews.gif);
			             list-style-position:inside;
			             } /* lien non-visit */
			             a.news:visited {
			             text-decoration: none;
			             color: ##777777;
			             } /* lien visit */
			             a.news:hover {
			             text-decoration: underline;
			             } /* lien survol */
			
			             a.notes:link {
			             color: ##777777;
			             text-decoration: none;
			             display:list-item;
			             list-style-image: url(http://www.consotel.fr/newsletter/images/pucenotes.gif);
			             list-style-position:inside;
			             } /* lien non-visit */
			             a.notes:visited {
			             text-decoration: none;
			             color: ##777777;
			             } /* lien visit */
			             a.notes:hover {
			             text-decoration: underline;
			             } /* lien survol */
						
			             .site {
					             font-family: Arial, Helvetica, sans-serif;
					             font-size: 12px;
					             color: ##336600;
					             font-weight: bold;
					             clip: rect(auto,auto,auto,auto);
					            }
			
			             .titre {
					             font-family: Arial, Helvetica, sans-serif;
					             font-weight: bold;
					            }
			
						.titre_info{
										align: center;
							        	color: ##006600;
							        	font-size: 15px;
							        	font-family: Arial, Helvetica, sans-serif;
							        	font-weight: bold;
						       	   }/*affine les proprit du titre sujet2 */
					
						.sujet_titre{
							        	color: ##FF0000;
							        	font-size: 20px;
							        	font-family: Arial, Helvetica, sans-serif;
							        	font-weight: bold;
							        }/*affine les proprit du titre sujet2 */
			             .text {
					             font-family: Arial, Helvetica, sans-serif;
					             font-size: 14px;
					             font-weight: normal;
					             margin-top: 15px;
			             	   }
			
			             .suite {
					             font-family: Arial, Helvetica, sans-serif;
					             font-size: 13px;
					             color: ##666666;
					            }
			        </style>
			    </head>
						    
				<BODY bgcolor="##aaaaa9" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
					<center>
						<TABLE bgcolor="##ffffff" width="600" border="0" cellpadding="0" cellspacing="0" height="500">
				            <TR>
					            <TD colspan="4">
									<IMG src="http://www.consotel.fr/newsletter/images/SNCF_SFR_bandeau_app.jpg" width="600">
								</TD>
				            </TR>
				            <TR>
								<TD colspan="4">
									<br>
									<span class="titre_info">
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										Informations de connexion à la BOL SFR
									</span>
									<br><br>
								</TD>
				            </TR>
				            <TR>
				                <TD>
			                       <div style="align: center;font-color: ##b1b1b1>;">
				                        <A class="site" href="https://cmd-sncf.consotel.fr" target="blank">
				                        	<img src="http://www.consotel.fr/newsletter/images/lien_ordi.jpg" border="0">
				                        </A>
			                       </div> 
				           		</TD>
				           		<TD width="50"/>
				                <TD>
				               		<span class="sujet_titre">
					                  	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vos informations<br>
					                  	<div align="center">de connexion &agrave; BOL SFR</div>
				             	  	</span>
				             		<br>
					             	  
				             		<span class="suite">
				                  	<div style="text-align:justify;">
									<b>Bonjour #tableau[2]# #Ucase(tableau[1])#,</b>
									<br>
									<br>
									Suite au pr&eacute;c&eacute;dent mail contenant l'adresse de connexion &agrave; la BOL SFR et votre login, nous avons le plaisir de vous faire parvenir votre mot de passe.
									<br>
									<br>
									&raquo; <b>Mot de passe :  <font style="font-size:14px;">#tableau[4]#</font></b>
									<br>
									<br>
									Pour tout renseignement concernant votre connexion et l?utilisation de ConsoView vous pouvez contacter le service support ConsoView
									<br>
									<br>
									<br>
									Cordialement,
									<br>
									<b>L'&eacute;quipe support Grands Comptes</b><br>
				                   	----------------------------------------------------<br>
				                   
				                   	<font size="-2">
					                   	ConsoTel - Conseil et logiciels pour la gestion de vos t&eacute;l&eacute;coms<br>
				                   		Tel support :.................. <b>04 37 51 51 51</b><br>
				                   		Email support :.............. <a href="mailto:sfr-sncf@sfrserviceclient.fr ">sfr-sncf@sfrserviceclient.fr</A> <br>
				                   	</font>
				              	</TD>
				           		<TD width="50"/>
				           	</TR>
				           	<TR height="20"/>
				  		</TABLE>
					</center>
				</BODY>
			</html>

		</cfmail>
</cffunction>


<cffunction name="changeDroit" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="numeric">
	<cfargument name="tableau" required="true" type="array" />
	
	<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.ChangeDroit_V2">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tableau[3]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tableau[4]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tableau[1]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tableau[2]#"/>
		<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
	</cfstoredproc>
	<cfreturn result>

</cffunction>

<cffunction name="updateAffectation" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="NUMERIC">
		<cfargument name="tableau" required="true" type="array" />	
				<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.updateAffectation_V4">
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#tableau[2]#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#tableau[1]#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#SESSION.USER.CLIENTACCESSID#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.PERIMETRE.GROUP_CODE_STYLE#"/>
					<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
				</cfstoredproc>
		<cfreturn result>
		 
</cffunction>
<cffunction name="deleteLogin" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="numeric">
		<cfargument name="tableau" required="true" type="array" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.deleteLogin">
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#tableau[1]#"/>
					<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
			</cfstoredproc>
		<cfreturn result>
</cffunction>

<cffunction name="addApp_Login" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="numeric">
	<cfargument name="tableau" required="true" type="array" />
	<cfargument name="idGroupeClient" required="true" type="numeric" />
	<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.addApp_Login_v2">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tableau[1]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tableau[2]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tableau[3]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tableau[4]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tableau[5]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tableau[6]#"/>
		<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
	</cfstoredproc>
	<cfreturn result>
</cffunction>

<cffunction name="updateLogin" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="numeric">
	<cfargument name="tableau" required="true" type="array" />
	<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.updateLogin">
		<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#tableau[1]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tableau[2]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tableau[3]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tableau[4]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tableau[5]#"/>
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tableau[6]#"/>
		<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
	</cfstoredproc>
	<cfreturn result>
</cffunction>

<cffunction name="upd_racine_login" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="numeric">
		<cfargument name="tableau" required="true" type="array" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.upd_racine_login">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#tableau[1]#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tableau[2]#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn result>
</cffunction>
<cffunction name="setStyle" output="false"  description="Returns a query with names (id,name,age,gender)" access="remote" returntype="numeric">
		<cfargument name="tableau" required="true" type="array" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.setStyle">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tableau[1]#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tableau[2]#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tableau[3]#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
		<cfreturn  result>
</cffunction>

<cffunction name="deleteAcces" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="void">
		<cfargument name="tableau" required="true" type="array" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.deleteAcces">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tableau[1]#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tableau[2]#"/>
			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
</cffunction>

<cffunction name="getAccesInterdit" returntype="array" access="remote">
		<cfargument name="listeAcces" type="array"/>
		<cfset listeArbre = arraynew(1)>
			<cfloop index = "i" from ="1" to="#arraylen(listeAcces)#" >
				<cfset arrayappend(listeArbre,(getGroupNodes(listeAcces[i].idGroupeMaitre,listeAcces[i].APP_LOGINID))) >
			</cfloop>
		<cfreturn listeArbre>
</cffunction>

<cffunction name="getListeRacine" returntype="query" access="remote">
		<cfargument name="idgroupeClient" type="array"/>
		<cfset qresult = querynew("idGroupeMaitre,libellegm")>
		<cfset listIdAcces = arraynew(1)>
			<cfloop index = "i" from ="1" to="#arraylen(idgroupeClient)#" >
				<cfset queryaddRow(qresult,1)>
				<cfset querysetcell(qresult,"idGroupeMaitre",getRacine(idgroupeClient[i]),"#i#")>
				<cfset querysetcell(qresult,"libellegm",getLibelleRacine(idgroupeClient[i]),"#i#")>
			</cfloop>
		<cfreturn qresult>
</cffunction>
<cffunction name="getRacine" returntype="Numeric" access="remote">
		<cfargument name="idgroupe" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.Get_racine">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idgroupe#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_groupe_client">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_idracine">
		</cfstoredproc>
		<cfreturn p_idracine>
</cffunction>
<cffunction name="getLibelleRacine" returntype="String" access="remote">
		<cfargument name="idgroupe" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.Get_racine">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idgroupe#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_groupe_client">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_idracine">
		</cfstoredproc>
			
		<cfreturn p_libelle_groupe_client>
</cffunction>

	<cffunction name="addOrgaFromFile" output="true" access="remote" returntype="numeric">
		<cfargument name="idgroupe" type="numeric">
		<cfargument name="file" type="String">
		<cfargument name="type_orga" type="String">
        <cfargument name="libelle" type="String">
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_consoview_v3.import_orga">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#idgroupe#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" null="true"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#libelle#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#type_orga#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#file#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
		<cfif result gt 0>
            <cffile action="move" source="\\dbcl2\web_directory\admin\autres\import_orga\#file#" destination="\\dbcl2\web_directory\admin\autres\import_orga\import?\">
            <cffile action="move" source="\\dbcl2\web_directory\admin\autres\import_orga\#file#.log" destination="\\dbcl2\web_directory\admin\autres\import_orga\import?\">
		</cfif>
		<cfreturn result>
    </cffunction>

	<cffunction name="add_several_acces" access="remote" returntype="numeric">
		<cfargument name="IDGROUPE_CLIENT" required="true" type="numeric"/>
		<cfargument name="IDRACINE" required="true" type="numeric"/>
		<cfargument name="IDSTYLE" required="true" type="numeric"/>
		<cfargument name="MODULE_FACTURATION" required="true" type="numeric"/>
		<cfargument name="MODULE_USAGE" required="true" type="numeric"/>
		<cfargument name="MODULE_WORKFLOW" required="true" type="numeric"/>
		<cfargument name="MODULE_FIXE_DATA" required="true" type="numeric"/>
		<cfargument name="MODULE_MOBILE" required="true" type="numeric"/>
		<cfargument name="MODULE_GESTION_ORG" required="true" type="numeric"/>
		<cfargument name="MODULE_GESTION_LOGIN" required="true" type="numeric"/>
		<cfargument name="DROIT_GESTION_FOURNIS" required="true" type="numeric"/>
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.ADD_SEVERAL_ACCES_V1">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDGROUPE_CLIENT#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDRACINE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDSTYLE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MODULE_FACTURATION#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MODULE_USAGE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MODULE_WORKFLOW#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MODULE_FIXE_DATA#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MODULE_MOBILE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MODULE_GESTION_ORG#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#MODULE_GESTION_LOGIN#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#DROIT_GESTION_FOURNIS#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="result"/>
		</cfstoredproc>
		 <cfreturn result>
	</cffunction>
</cfcomponent>