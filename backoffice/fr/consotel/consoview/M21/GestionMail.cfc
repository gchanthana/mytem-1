<cfcomponent displayname="fr.consotel.consoview.M21.GestionMail" hint="Gestion de l'envoi de login, mot de passe" output="false">

	<!--- 
		[1]->nom
		[2]->prenom
		[3]->email
		[4]->idclient
		[5]->mot de passe (UNIQUEMENT DANS LE CAS D'ENVOI DE MOT DE PASSE)
	 --->
	 
	<cfset strategy = ""/>
	
	<cfscript>
		try
		{
			strategy = createObject("component","GestionMail#SESSION.CODEAPPLICATION#");
		}
		catch(any error)
		{
			strategy = createObject("component","GestionMail1");
		}
	</cfscript>
	
	
	<cffunction name="send_Login" output="false" description="" access="remote" returntype="void" hint="envoi du login">
		<cfargument name="tableau" required="true" type="array"/>	
		<cfargument name="lang" required="false" type="string" hint="param�tre pour choisir la langue du mail fr|en|es"/>	
		
		<cfset _lang = left(session['user']['globalization'],2)>
		<cfif IsDefined("lang")>
			<cfset _lang = lang>
		</cfif>
		<cfset initLang(_lang)>
		<cfset rsltBIPReport = strategy.send_Login(tableau,_lang)>
	</cffunction>

	<cffunction name="send_Pass" output="false" description="" access="remote" returntype="void" hint="envoi du mot de passe">
		<cfargument name="tableau" required="true" type="array" />
		<cfargument name="lang" required="false" type="string" hint="param�tre pour choisir la langue du mail fr|en|es"/>	
		
		<cfset _lang = left(session['user']['globalization'],2)>		
		<cfif IsDefined("lang")>
			<cfset _lang = lang>
		</cfif>
		<cfset initLang(_lang)>
		<cfset rsltBIPReport = strategy.send_Pass(tableau,_lang)>
	</cffunction>
	
	
	<cffunction name="createBIPReport" output="false" description="" access="remote" returntype="Numeric">
		<cfargument name="tableau" 		required="true" type="Array"/>
		<cfargument name="templateID"  	required="true" type="String"/>
		<cfargument name="prefixeFile" 	required="true" type="String"/>
		<cfargument name="subject" 		required="true" type="String"/>
		<cfargument name="type" 		required="true" type="String"/>
			
		<cfset	rsltApi = strategy.createBIPReport(tableau,templateID,prefixeFile,subject,type)>
		<cfreturn rsltApi>
	</cffunction>
	
	<cffunction name="initLang" output="false"  access="private" returntype="void">
		<cfargument name="lang" required="false" type="string" hint="param�tre initialiser le dico de langue du mail fr|en|es"/>
			
			<cfset _lang =  left(session['user']['globalization'],2)>
			<cfif IsDefined('lang')>
				<cfset _lang =  left(lang,2)>
			</cfif>
			<cfinvoke 	returnvariable="local.insertlog.result"	
					component="fr.consotel.consoview.M00.dictionary.DictionaryManager"	
					method="init"	
			      	argumentcollection="#{lang = _lang}#"/>
	</cffunction>

</cfcomponent>