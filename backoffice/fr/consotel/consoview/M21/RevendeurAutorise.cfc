<cfcomponent output="false"  name="RevendeurAutorise">

	<cffunction name="getTypesCommande" access="remote" returntype="any" description="Retourne la liste des types de commande d'un login">
		<cfargument name="idlogin"  type="Numeric" required="true">
		<cfargument name="idgroupe" type="Numeric" required="true">
		
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestionnaire.gettypecommandelogin">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idgroupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idlogin#"  null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getOperateurs" access="remote" returntype="any"  description="Retourne la liste des opérateurs actifs">
		<cfargument name="idracine" type="Numeric" required="true">
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_equipement.getoperateursactif">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idRacine" value="#idracine#"/>
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="getOperateursByRevendeur" access="public" returntype="Array"><!--- getOperateur --->
		<cfargument name="idsrevendeur"	type="Array" required="true">
		<cfargument name="idsoperateur"	type="Array" required="true">
		
 			<cfset operateurs = structNew()>			
			<cfset operateur = ArrayNew(1)>
			<cfset idrevendeur = -1>
			
			<cfloop index="idx" from="1" to="#ArrayLen(idsrevendeur)#">
				
				<cfset idrevendeur = idsrevendeur[idx].IDREVENDEUR>
				<cfset libelleRevendeur = idsrevendeur[idx].LIBELLE>
			
			
				<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m21.getOperateur">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_revendeur" value="#idrevendeur#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
								
				<cfset allOperateur = p_retour>
				
				<cfset operateurActif = ArrayNew(1)>
				
				<cfset operateurs = structNew()>
				<cfset operateurs.IDREVENDEUR = idrevendeur>
				<cfset operateurs.LIBELLE_REVENDEUR = libelleRevendeur>
				
				<cfloop index="newidx" from="1" to="#p_retour.recordcount#">
					<cfset operateurID = allOperateur['OPERATEURID'][newidx]>
					
					<cfset newStruct = structNew()>
					<cfset newStruct.OPERATEURID = allOperateur['OPERATEURID'][newidx]>
					<cfset newStruct.LIBELLE = allOperateur['LIBELLE'][newidx]>
				 	<cfset newStruct.SELECTED = 0> 
				 			
					<cfloop index="index" from="1" to="#ArrayLen(idsoperateur)#">
						<cfif idsoperateur[index] EQ operateurID>
							<cfset ArrayAppend(operateurActif, newStruct)>
						</cfif>
					</cfloop>
				</cfloop>
				<cfset operateurs.OPERATEUR = operateurActif>
				<cfset ArrayAppend(operateur, operateurs)>				
				
			</cfloop>
		<cfreturn operateur>
	</cffunction>
	
	<!--- 
		Liste des operateurs autorisés d'un gestionnaire, pour un revendeur et un type de commande donnée
	 --->
	<cffunction name="getGrilleRevendeursAutorises" access="public" returntype="Array"><!--- getOperateur --->
		<cfargument name="idsrevendeur"	type="Array" required="true">
		<cfargument name="idsoperateur"	type="Array" required="true">
		<cfargument name="idtypecommande"	type="numeric" required="true">
		<cfargument name="iduser"	type="numeric" required="true">
		
 			<cfset operateurs = structNew()>			
			<cfset operateur = ArrayNew(1)>
			<cfset idrevendeur = -1>
			
			<cfloop index="idx" from="1" to="#ArrayLen(idsrevendeur)#">
				
				<cfset idrevendeur = idsrevendeur[idx].IDREVENDEUR>
				<cfset libelleRevendeur = idsrevendeur[idx].LIBELLE>
			
			
				<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m21.getOperateur">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_revendeur" value="#idrevendeur#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
								
				<cfset allOperateur = p_retour>
				
				<cfset operateurActif = ArrayNew(1)>
				
				<cfset operateurs = structNew()>
				<cfset operateurs.IDREVENDEUR = idrevendeur>
				<cfset operateurs.LIBELLE_REVENDEUR = libelleRevendeur>
				
				<cfloop index="newidx" from="1" to="#p_retour.recordcount#">
					<cfset operateurID = allOperateur['OPERATEURID'][newidx]>
					
					<cfset newStruct = structNew()>
					<cfset newStruct.OPERATEURID = allOperateur['OPERATEURID'][newidx]>
					<cfset newStruct.LIBELLE = allOperateur['LIBELLE'][newidx]>
				 	<cfset newStruct.SELECTED = isLink_ProfilRevOp(idrevendeur,operateurID,idtypecommande,iduser)> 
				 			
					<cfloop index="index" from="1" to="#ArrayLen(idsoperateur)#">
						<cfif idsoperateur[index] EQ operateurID>
							<cfset ArrayAppend(operateurActif, newStruct)>
						</cfif>
					</cfloop>
				</cfloop>
				<cfset operateurs.OPERATEUR = operateurActif>
				<cfset ArrayAppend(operateur, operateurs)>				
				
			</cfloop>
		<cfreturn operateur>
	</cffunction>

	<cffunction name="getOperateursByRevendeur_OneByOne" access="public" returntype="Any">
		<cfargument name="idrevendeur"	type="Numeric" required="true">
						
				<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m21.getOperateur">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_revendeur" value="#idrevendeur#">
					<cfprocresult name="p_retour">
				</cfstoredproc>

			<!--- 	<cfdump var="#ArrayLen(p_retour)#"> --->

		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="addLinkProfilRevOp" access="public" returntype="Numeric">
		<cfargument name="idrevendeur"		type="Numeric" required="true">
		<cfargument name="idoperateur"		type="Numeric" required="true">
		<cfargument name="idtypecommande"	type="Numeric" required="true">
		<cfargument name="iduser"			type="Numeric" required="true">
						
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m21.addLinkProfilRevOp">	
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 	value="#idrevendeur#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 	value="#idoperateur#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idtypecommande" 	value="#idtypecommande#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idclient" 		value="#iduser#">
				<cfprocparam type="out"  	cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="dropLinkProfilRevOp" access="public" returntype="Numeric">
		<cfargument name="idrevendeur"		type="Numeric" required="true">
		<cfargument name="idoperateur"		type="Numeric" required="true">
		<cfargument name="idtypecommande"	type="Numeric" required="true">
		<cfargument name="iduser"			type="Numeric" required="true">
						
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m21.dropLinkProfilRevOp">	
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 	value="#idrevendeur#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 	value="#idoperateur#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idtypecommande" 	value="#idtypecommande#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idclient" 		value="#iduser#">
				<cfprocparam type="out"  	cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="isLink_ProfilRevOp" access="public" returntype="Numeric">
		<cfargument name="idrevendeur"		type="Numeric" required="true">
		<cfargument name="idoperateur"		type="Numeric" required="true">
		<cfargument name="idtypecommande"	type="Numeric" required="true">
		<cfargument name="iduser"			type="Numeric" required="true">
																		 
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m21.isLink_ProfilRevrOp">	
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 	value="#idrevendeur#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 	value="#idoperateur#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idtypecommande" 	value="#idtypecommande#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idclient" 		value="#iduser#">
				<cfprocparam type="out"  	cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="fournirOperateursAutorises" access="public" returntype="Array">
		<cfargument name="iduser"			type="Numeric" required="true">
		<cfargument name="idtypecommande"	type="Numeric" required="true">
		
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m16.opeAutorises">	
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_iduser" 				value="#Session.USER.CLIENTACCESSID#">
				<cfprocparam type="In"  	cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" value="#idtypecommande#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
						
			<cfset operateurs = ArrayNew(1)>
			<cfset allOperateur = p_retour>

			<cfloop index="idx" from="1" to="#p_retour.recordcount#">
				<cfset operateur = structNew()>
				<cfset operateur.LIBELLE = allOperateur['NOM'][idx]>
				<cfset operateur.OPERATEURID = allOperateur['OPERATEURID'][idx]>
				<cfset operateur.PAYS = allOperateur['PAYS'][idx]>
				<cfset operateur.PAYSCONSOTELID = allOperateur['PAYSCONSOTELID'][idx]>
				
				<cfset ArrayAppend(operateurs, operateur)>
			</cfloop>
			
		
		<cfreturn operateurs>
	</cffunction>

	<cffunction name="fournirRevendeursAutorises" access="public" returntype="Query">
		<cfargument name="idRacine" 		type="Numeric" required="true">
		<cfargument name="iduser"			type="Numeric" required="true">
		<cfargument name="idtypecommande"	type="Numeric" required="true">
		<cfargument name="idOperateur" 		type="Numeric" required="true">
				
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.distribAutorises">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_apploginid" 			value="#Session.USER.CLIENTACCESSID#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idtypecommande#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 			value="#idOperateur#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>	
	
	<cffunction name="fournirNumberRevendeursAutorises" access="public" returntype="Numeric">
		<cfargument name="idRacine" 		type="Numeric" required="true">
		<cfargument name="iduser"			type="Numeric" required="true">
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.numberDistribAutorises">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" variable="p_apploginid" 			value="#Session.USER.CLIENTACCESSID#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<!---
	 	Ajout 		
	 --->

<!--- 	<cffunction name="fournirOperateursAutorises" access="remote" returntype="any" description="Retourne la liste des types de commande d'un login">
		
	</cffunction> --->

</cfcomponent>