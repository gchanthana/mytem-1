﻿<cfcomponent displayname="ProductCodeGLService" output="false">

	<!--- Récupération de la liste des GL Code --->	
	<cffunction access="remote" output="false" name="getListProductGLCode" returntype="query">
	
		<cfset idracine = session.perimetre.id_groupe>
		<cfset idlang = SESSION.USER.IDGLOBALIZATION>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_gl_code.liste_gl_code_theme">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idlang#">
			<cfprocresult name="p_liste">
		</cfstoredproc>
		
		<cfreturn p_liste>
	
	</cffunction>
	
	<!--- Creation d'un Nouveau GL Code --->
	<cffunction access="remote" output="false" name="affectProductGlCode" returntype="Struct" >
		<cfargument name="idTheme" type="string" required="true" >
		<cfargument name="glCode" type="string" required="true" >
	
		<cfset idracine = session.perimetre.id_groupe>
		<cfset appLoginId = Session.USER.CLIENTACCESSID>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_gl_code.affect_gl_code">
			
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#Arguments.idTheme#">
			<cfprocparam type="in" cfsqltype="Cf_sql_vaRCHAR" value="#Arguments.glCode#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#appLoginId#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		</cfstoredproc>
		
		<cfset resultGLCode = StructNew()>
		<cfset resultGLCode.idGLCode = p_retour>
		<cfset resultGLCode.libelleGLCode = #Arguments.glCode#>
		<cfreturn resultGLCode>
	
	</cffunction>
	
	<cffunction access="remote" output="false" name="affectManyGLCode" returntype="Struct" >
		<cfargument name="enteredGlCode" type="String" required="true" >
		<cfargument name="selectedIDThemes" type="Array" required="true">
	
		<cfset idracine = session.perimetre.id_groupe>
		<cfset appLoginId = Session.USER.CLIENTACCESSID>
		
		<cfloop  array="#selectedIDThemes#" index="oneIdTheme">
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_gl_code.affect_gl_code">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#oneIdTheme#">
				<cfprocparam type="in" cfsqltype="Cf_sql_VARCHAR" value="#Arguments.enteredGlCode#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#appLoginId#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
			</cfstoredproc>
		</cfloop>
		
		<cfset p_result = StructNew()>
		<cfset p_result.idGLCode = p_retour> <!--- dans la boucle on recupère que le dernier valeur de p_retour puisque il s'agit d --->
		<cfset p_result.libelleGLCode = #Arguments.enteredGlCode#>

		<cfreturn p_result>
	
	</cffunction>

</cfcomponent>