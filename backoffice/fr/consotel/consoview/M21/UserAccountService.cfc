<cfcomponent displayname='fr.consotel.consoview.M21.UserAccountService' output='false'>
	
	<cfset VARIABLES["idUser"] = SESSION.USER.APP_LOGINID>
	 
	<!--- remote --->
	<cffunction access='remote' name='unlockLogin' returntype='any' hint='débloquer un login'>
		<cfargument name='idLogin' type='numeric' required='true'>
		<cfreturn updateLoginState(idLogin,1,SESSION.OFFREDSN)>
	</cffunction>
	
	
	<cffunction access='remote' name='lockLogin' returntype='any' hint='bloquer un login'>
		<cfargument name='idLogin' type='numeric' required='true'>
		<cfreturn updateLoginState(idLogin,2,SESSION.OFFREDSN)>
	</cffunction>
	
	
	<cffunction access='remote' name='expireLogin' returntype='any' hint='met le statut du login à expiré'>
		<cfargument name='idLogin' type='numeric' required='true'>
		<cfreturn updateLoginState(idLogin,0,SESSION.OFFREDSN)>
	</cffunction>
	
	<cffunction access='remote' name='updateUserAccountState' returntype='any' hint='met le statut du login à expiré'>
		<cfargument name='idLogin' type='numeric' required='true'>
		<cfargument name='idstatut' type='numeric' required='true'>		 
		<cfreturn updateLoginState(idLogin,idstatut,SESSION.OFFREDSN)>
	</cffunction>
	
	
	<cffunction access='remote' name='getUserAccountList' returntype='any' hint='Donne la liste des comptes utilisateur'>
		<cfargument name='idLogin' type='numeric' required='true'>
		<cfargument name='idracine' type='numeric' required='true'>
		<cfargument name='mode' type='numeric' required='true' >
		<cfreturn getUserAccounts(idLogin,idracine,mode,SESSION.OFFREDSN)>
	</cffunction>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<!--- Private --->
	
	
	
	<cffunction access='private' name='updateLoginState' returntype='any' hint='Mettre à jour le statut d un login'>
		<cfargument name='idLogin' type='numeric' required='true'>
		<cfargument name='idstatus' type='numeric' required='true'>
		<cfargument name='dsn' type='string' required='true'>
		
		
		<cfstoredproc datasource='#dsn#' procedure='PKG_INTRANET.UPDATE_LOGIN_STATUS_V2'>
			<cfprocparam variable='p_app_loginid' 	cfsqltype='CF_SQL_INTEGER' 	type='in' value='#idLogin#'>
			<cfprocparam variable='p_new_status' 	cfsqltype='CF_SQL_INTEGER' 	type='in' value='#idstatus#'>
			<cfprocparam variable='p_login_modif' 	cfsqltype='CF_SQL_INTEGER' 	type='in' value='#VARIABLES["idUser"]#'>
			<cfprocparam variable='p_retour' 		cfsqltype='CF_SQL_INTEGER' 	type='out' >
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>	
	
	
	<cffunction access='private' name='getUserAccounts' returntype='any' hint='Lister les comptes diponibles utilisateurs'>
		<cfargument name='idLogin' type='numeric' required='true'>
		<cfargument name='idracine' type='numeric' required='true'>
		<cfargument name='mode' type='numeric' required='true' hint='0=non, 1=oui, -1=tous'>
		<cfargument name='dsn' type='string' required='true'>
		
		
		<cfstoredproc datasource='#dsn#' procedure='PKG_INTRANET.getLogin_v2'>
			<cfprocparam type='In' cfsqltype='CF_SQL_iNTEGER' value='#idLogin#'/>
			<cfprocparam type='In' cfsqltype='CF_SQL_iNTEGER' value='#idracine#'/>
			<cfprocparam type='In' cfsqltype='CF_SQL_iNTEGER' value='#visibilite#'/>
			<cfprocresult name='qUserAccountList'/>
		</cfstoredproc>		
		
		<cfreturn qUserAccountList>
	</cffunction>
	
	
</cfcomponent>