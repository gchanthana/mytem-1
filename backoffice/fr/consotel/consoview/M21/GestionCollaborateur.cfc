<cfcomponent displayname="GestionCollaborateur" output="false">

<cffunction access="public" output="false" name="getRacineGestCollab" returntype="query">
	
	<cfset idracine = session.perimetre.id_groupe>
	
	<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m21.getRacineGestCollab">
		<cfprocparam type="In" value="#idracine#" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" null="false">
		<cfprocresult name="p_retour">
	</cfstoredproc>
	
	<cfreturn p_retour>
	
</cffunction>

<cffunction access="public" output="false" name="getGestionCollaborateur" returntype="numeric">
		
	<cfargument name="type" type="numeric" required="true" >
	
	<cfset idracine = session.perimetre.id_groupe>
	
	<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m21.UGestionCollabRacine">
		<cfprocparam type="In" value="#idracine#" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" null="false">
		<cfprocparam type="In" value="#type#" cfsqltype="CF_SQL_INTEGER" variable="p_type" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER"  variable="p_retour">
	</cfstoredproc>
	
	<cfreturn p_retour>
	
</cffunction>

</cfcomponent>