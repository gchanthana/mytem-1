<cfcomponent output="false">	
	<cffunction access="remote" output="false" name="getRacineRattachement" returntype="query">		
		
		<cfset idracine_appartenance = SESSION.USER.CLIENTID>
		<cfset idracine_perimetre    = SESSION.PERIMETRE.ID_GROUPE>

	    <cfstoredproc datasource="ROCOFFRE" procedure="PKG_M21.getPartenaire">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine_appartenance#" variable=" p_idracine_perimetre">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine_perimetre#" variable=" p_idracine_perimetre">
			<cfprocresult name="qRacines">
		</cfstoredproc>				
        <cfreturn qRacines>
     </cffunction> 
</cfcomponent>