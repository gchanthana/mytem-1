<cfcomponent name="gestionDroitService">
	<cffunction name="ListeModule" access="public" returntype="query" output="false" hint="Liste l'ensemble des modules CV4 d'un login client">
		<cfargument name="appLogin"			required="true" type="numeric"/>
		<cfargument name="idracine"			required="true" type="numeric"/>
		<cfargument name="codeapp"			required="true" type="numeric"/>
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M21.ListeModule_v2">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#appLogin#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#codeapp#">
			<cfprocresult name="qResult">
		</cfstoredproc>	
		<cfreturn qResult>
	</cffunction>
	
	
	<cffunction name="ListeModule3" access="public" returntype="query" output="false" hint="Liste l'ensemble des modules CV4 d'un login client">
		<cfargument name="appLogin"			required="true" type="numeric"/>
		<cfargument name="idracine"			required="true" type="numeric"/>
		<cfargument name="codeapp"			required="false" type="numeric" default="1"/>
	
		<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
		<cfset _codeapp =	SESSION.CODEAPPLICATION>
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M21.ListeModule_v4">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#appLogin#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#_codeapp#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#_idlang#">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
		<cfreturn qResult>
	</cffunction>
	
	<cffunction name="SauvegarderDroit" access="public" returntype="numeric" output="false" hint="sauvegarde les droits sur les modules d'un client">
		<cfargument name="appLogin"			required="true" type="numeric"/>
		<cfargument name="idracine"			required="true" type="numeric"/>
		<cfargument name="listeModuleEcriture"			required="true" type="string"/>
		<cfargument name="listeModuleLecture"			required="true" type="string"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M21.SauvegarderDroit">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#appLogin#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#listeModuleEcriture#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#listeModuleLecture#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="qResult">
		</cfstoredproc>	
		<cfreturn qResult>
	</cffunction>

	<cffunction name="listeProfil" access="public" returntype="query" output="false">
		<cfargument name="idracine"			required="true" type="numeric"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M21.listeProfil">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
			<cfprocresult name="qResult">
		</cfstoredproc>	
		<cfreturn qResult>
	</cffunction>

	<cffunction name="SupprimerProfil" access="public" returntype="numeric" output="false">
		<cfargument name="idprofil"			required="true" type="numeric"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M21.SupprimerProfil">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idprofil#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="qResult">
		</cfstoredproc>	
		<cfreturn qResult>
	</cffunction>

	<cffunction name="EnregistrerProfil" access="public" returntype="numeric" output="false">
		<cfargument name="libelleProfil"		required="true" type="string"/>
		<cfargument name="idracine"				required="true" type="numeric"/>
		<cfargument name="listeModuleEcriture"	required="true" type="string"/>
		<cfargument name="listeModuleLecture"	required="true" type="string"/>
		<cfargument name="descriptionProfil"	required="true" type="string"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M21.EnregistrerProfil">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#libelleProfil#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#listeModuleEcriture#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#listeModuleLecture#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#descriptionProfil#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="qResult">
		</cfstoredproc>	
		<cfreturn qResult>
	</cffunction>

	<cffunction name="RecupProfil" access="public" returntype="query" output="false">
		<cfargument name="idProfil"			required="true" type="numeric"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M21.RecupProfil">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idprofil#">
			<cfprocresult name="qResult">
		</cfstoredproc>	
		<cfreturn qResult>
	</cffunction>

</cfcomponent>