﻿<cfcomponent displayname="fr.consotel.consoview.M21.GestionMail1" hint="Gestion de l'envoi de login, mot de passe pour Consoview" output="false">

	<!--- 
		[1]->nom
		[2]->prenom
		[3]->email
		[4]->idclient
		[5]->mot de passe (UNIQUEMENT DANS LE CAS D'ENVOI DE MOT DE PASSE)
	 --->

	<cffunction name="send_Login" output="false" description="" access="remote" returntype="void">
		<cfargument name="tableau" required="true" type="array"/>
		<cfargument name="lang" required="true" type="string" hint="paramètre pour choisir la langue du mail fr|en|es"/>
		
		<cfset _lang = left(trim(lang),2)>
		<cfset var mytemplate = 'SEND-LOGIN-1'>
		<cfset rsltBIPReport = createBIPReport(tableau, mytemplate, 'login', session['dictionaire'][_lang]['login.mail.1.subject'], 'L',_lang)>
	</cffunction>

	<cffunction name="send_Pass" output="false" description="" access="remote" returntype="void">
		<cfargument name="tableau" required="true" type="array" />
		<cfargument name="lang" required="true" type="string" hint="paramètre pour choisir la langue du mail fr|en|es"/>
		
		<cfset _lang = left(trim(lang),2)>
		<cfset var mytemplate = 'SEND-PWD-1'>
		<cfset rsltBIPReport = createBIPReport(tableau, mytemplate, 'password', session['dictionaire'][_lang]['password.mail.1.subject'], 'P',_lang)>
	</cffunction>
	
	
	
	<cffunction name="createBIPReport" output="false" description="" access="remote" returntype="Numeric">
		<cfargument name="tableau" 		required="true" type="Array"/>
		<cfargument name="templateID"  	required="true" type="String"/>
		<cfargument name="prefixeFile" 	required="true" type="String"/>
		<cfargument name="subject" 		required="true" type="String"/>
		<cfargument name="type" 		required="true" type="String"/>
		<cfargument name="lang" 		required="true" type="String" hint="paramètre pour choisir la langue du mail fr|en|es"/>
		
			<cfset var mailing  = createObject("component","fr.consotel.consoview.M01.Mailing")>				
			
			<cfset var M21_UUID = ''>
			
			<cfset var myVar = false>
			
			<cfset var ImportTmp="/container/M21/">
			
			<cfset var rsltApi  = -4>
			<cfset var resltRecordMail = 0>
			<cfset var iduser = tableau[4]>
			
			<cfset var properties = createObject("component","fr.consotel.consoview.M01.SendMail").getProperties(SESSION.CODEAPPLICATION,'NULL')>
			<cfset var fromMailing 	= properties['MAIL_EXP_N1']>
			<cfset var bccMailing 	= properties['MAIL_DEST_N1']>

			<cfloop condition="myVar eq false">
			  <cfset M21_UUID = createUUID()>
			  <cfif NOT DirectoryExists('#ImportTmp##M21_UUID#')>
				<cfdirectory action="Create" directory="#ImportTmp##M21_UUID#" type="dir" mode="777">
				<cfset myVar="true">
				<cfdump var="#myVar#">
			  </cfif>
			</cfloop>

			<!--- BIP REPORT --->
			<cfset parameters["bipReport"] = structNew()>
			<cfset parameters["bipReport"]["xdoAbsolutePath"]	= "/consoview/LOGIN/LOGIN.xdo">
			<cfset parameters["bipReport"]["outputFormat"] 		= "html">
			<cfset parameters["bipReport"]["localization"]		= '#lang#'>
			
			<!--- DONNEES ENVOYEES A BIP --->
			<cfset parameters["bipReport"]["reportParameters"]	= structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_LOGIN"] = structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_LOGIN"]["parameterValues"] = [tableau[3]]>
			
			<!--- PARAMÈTRES DE GÉNÉRATION FTP DU RAPPORT BIP --->
			<cfset parameters.bipReport.delivery.parameters.ftpUser="container">
			<cfset parameters.bipReport.delivery.parameters.ftpPassword="container">
			
			<!--- CONFIGURATION EVENT --->					
			<cfset parameters.EVENT_TARGET  = #M21_UUID#>
			<cfset parameters.EVENT_HANDLER = "M21">
			<cfset parameters.EVENT_TYPE 	= #type#>

			<!--- PARAMETRES POUR BIP --->
			<cfset parameters["bipReport"]["xdoTemplateId"]		= "#templateID#">
			<cfset parameters.bipReport.delivery.parameters.fileRelativePath="M21/#M21_UUID#/#prefixeFile#_#M21_UUID#.html">
						
			<!--- ENREGISTREMENT DU REPERTOIRE DE DESTINATION --->
			<cfset resltRecordMail = mailing.setMailToDatabase(#iduser#, #tableau[3]#, #M21_UUID#, "M21", #type#, #fromMailing#, #tableau[3]#, #subject#, #bccMailing#, '', #SESSION.CODEAPPLICATION#)>
			
			<!--- VERIFICATION DE L'ENVOI A BIP OU NON --->
			<cfif resltRecordMail GT 0>
				<cfset api= createObject("component","fr.consotel.consoview.api.CV")>
				<cfset rsltApi = api.invokeService("IbIs","scheduleReport",parameters)>
			<cfelse>
				<cfset rsltApi = -10>
			</cfif>
		
		<!--- BIP --->
		<cfreturn rsltApi>
	</cffunction>


</cfcomponent>