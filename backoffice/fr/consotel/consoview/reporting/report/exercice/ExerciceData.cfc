<cfcomponent name="ExerciceData" alias="fr.consotel.consoview.reporting.report.exercice.ExerciceData">
	<cffunction name="getExercices" access="remote" returntype="query">
		<cfargument name="IDRACINE" required="true" type="numeric">
		<cfquery name="qGetExercices" datasource="#SESSION.OFFREDSN#">
			select b.IDBUDGET_EXERCICE, b.LIBELLE_EXERCICE,
			       b.LIBELLE_EXERCICE || ' (' || TO_CHAR(b.datedebut,'yyyy') || ')' as LBL_EXERCICE,
			       b.COMMENTAIRES, TO_CHAR(b.datedebut,'yyyy') as ANNEE
			from   budget_exercice b
			where  b.idgroupe_client=#arguments.IDRACINE#
			order by TO_CHAR(b.datedebut,'yyyy') desc
		</cfquery>
		<cfreturn qGetExercices>
	</cffunction>

	<cffunction name="getCliche" access="remote" returntype="query">
		<cfargument name="IDNOEUD" required="true" type="numeric">
		<cfquery name="qGetCliche" datasource="#SESSION.OFFREDSN#">
			select a.IDCLICHE, a.LIBELLE_CLICHE, a.LBL_CLICHE, a.IDRACINE_MASTER, a.IDRACINE,
			       a.idperiode_mois, a.desc_annee
			from   (
			  			select c.IDCLICHE, c.LIBELLE_CLICHE, p.idperiode_mois, p.desc_annee,
			  				   	'(' || p.short_mois || ') ' || c.LIBELLE_CLICHE as LBL_CLICHE,
			              gc.IDRACINE_MASTER, gc.idgroupe_client as IDRACINE,
			              MAX(p.idperiode_mois) OVER (PARTITION BY p.desc_annee ORDER BY p.desc_annee) as idperiod_cliche_recent
			  			from   (
			  			        select gc.id_groupe_maitre, gc.idgroupe_client, gc.libelle_groupe_client
			  			        from   groupe_client gc
			  			        start with gc.idgroupe_client=#arguments.IDNOEUD#
			  			               connect by prior gc.id_groupe_maitre=gc.idgroupe_client
			  			       ) a, groupe_client gc, cliche c, periode p
			  			where  a.id_groupe_maitre=gc.idgroupe_client
			               and gc.id_groupe_maitre is null
			  			       and a.idgroupe_client=c.idorganisation
			               and c.idperiode_mois=p.idperiode_mois
			               and p.disp_mois <= TO_CHAR(SYSDATE,'yyyy/mm')
			       ) a
			where  a.idperiode_mois=idperiod_cliche_recent
			order by a.idperiode_mois
		</cfquery>
		<cfreturn qGetCliche>
	</cffunction>
	
	<cffunction name="getNodes" access="remote" returntype="query">
		<cfargument name="IDNOEUD" required="true" type="numeric">
		<cfargument name="IDORGA_NIVEAU" required="true" type="numeric">
		<cfargument name="IDBUDGET_EXERCICE" required="true" type="numeric">
		<cfquery name="qGetNodes" datasource="#SESSION.OFFREDSN#">
			select a.IDGROUPE_CLIENT, a.LIBELLE_GROUPE_CLIENT,
             		b.LIBELLE_EXERCICE, e.MONTANTBUDGET,
					a.LIBELLE_GROUPE_CLIENT || ' (' || e.MONTANTBUDGET || ')' as LBL_GROUPE_CLIENT
			from   (
			        select gc.IDGROUPE_CLIENT, gc.LIBELLE_GROUPE_CLIENT
			        from   groupe_client gc
			        where  gc.idorga_niveau=#arguments.IDORGA_NIVEAU#
			        start with gc.idgroupe_client=#arguments.IDNOEUD#
			              connect by prior gc.idgroupe_client=gc.id_groupe_maitre
			       ) a, budget_exercice b, exercice_item e
			where  b.idbudget_exercice=#arguments.IDBUDGET_EXERCICE#
             and e.idbudget_exercice=b.idbudget_exercice
             and e.idgroupe_client=a.idgroupe_client
			order by LOWER(a.LIBELLE_GROUPE_CLIENT)
		</cfquery>
		<cfreturn qGetNodes>
	</cffunction>
</cfcomponent>
