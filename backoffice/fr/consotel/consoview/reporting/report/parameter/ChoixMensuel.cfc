<cfcomponent name="ChoixMensuel" alias="fr.consotel.consoview.reporting.report.parameter.ChoixMensuel">
	<cffunction name="getYear" access="remote" returntype="query">
		<cfset currentYear=YEAR(NOW())>
		<cfset currentMonth=MONTH(NOW())>
		<cfquery name="qGetYear" datasource="#SESSION.OFFREDSN#">
			select   #currentMonth# as CURRENT_MONTH, DESC_ANNEE, MIN(p.idperiode_mois) as IDPERIODE_DEB
			from	 periode p
			where	 p.desc_annee in ('#(currentYear - 2)#','#(currentYear - 1)#','#currentYear#')
			group by DESC_ANNEE
			order by DESC_ANNEE DESC
		</cfquery>
		<cfreturn qGetYear>
	</cffunction>
</cfcomponent>
