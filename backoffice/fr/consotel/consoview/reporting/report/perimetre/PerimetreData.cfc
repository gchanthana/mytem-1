<cfcomponent name="PerimetreData" alias="fr.consotel.consoview.reporting.report.perimetre.PerimetreData">
	<cffunction name="getCliche" access="remote" returntype="query">
		<cfargument name="IDNOEUD" required="true" type="numeric">
		<cfquery name="qGetCliche" datasource="#SESSION.OFFREDSN#">
			select c.IDCLICHE, c.LIBELLE_CLICHE,
				   	'(' || p.short_mois || ') ' || c.LIBELLE_CLICHE as LBL_CLICHE,
             		gc.IDRACINE_MASTER, gc.idgroupe_client as IDRACINE
			from   (
			        select gc.id_groupe_maitre, gc.idgroupe_client, gc.libelle_groupe_client
			        from   groupe_client gc
			        start with gc.idgroupe_client=#arguments.IDNOEUD#
			               connect by prior gc.id_groupe_maitre=gc.idgroupe_client
			       ) a, groupe_client gc, cliche c, periode p
			where  a.id_groupe_maitre=gc.idgroupe_client
             and gc.id_groupe_maitre is null
			       and a.idgroupe_client=c.idorganisation
             and c.idperiode_mois=p.idperiode_mois
             and p.disp_mois <= TO_CHAR(SYSDATE,'yyyy/mm')
			order by c.idperiode_mois desc
		</cfquery>
		<cfreturn qGetCliche>
	</cffunction>
	
	<cffunction name="getTypeNoeud" access="remote" returntype="query">
		<cfargument name="IDNOEUD" required="true" type="numeric">
		<cfquery name="qGetTypeNoeud" datasource="#SESSION.OFFREDSN#">
			select o2.IDORGA_MODELE, o2.IDORGA_NIVEAU, o2.TYPE_NIVEAU
			from   groupe_client gc, orga_niveau o, orga_niveau o2
			where  gc.idgroupe_client=#arguments.IDNOEUD#
			       and o.idorga_niveau=gc.idorga_niveau
			       and o2.idorga_modele=o.idorga_modele
			       and o2.numero_niveau >= o.numero_niveau
			order by o2.numero_niveau
		</cfquery>
		<cfreturn qGetTypeNoeud>
	</cffunction>
	
	<cffunction name="getNodes" access="remote" returntype="query">
		<cfargument name="IDNOEUD" required="true" type="numeric">
		<cfargument name="IDORGA_NIVEAU" required="true" type="numeric">
		<cfquery name="qGetNodes" datasource="#SESSION.OFFREDSN#">
	        select gc.IDGROUPE_CLIENT, gc.LIBELLE_GROUPE_CLIENT
	        from   groupe_client gc
	        where  gc.idorga_niveau=#arguments.IDORGA_NIVEAU#
	        start with gc.idgroupe_client=#arguments.IDNOEUD#
	              connect by prior gc.idgroupe_client=gc.id_groupe_maitre
	        order by LOWER(gc.LIBELLE_GROUPE_CLIENT)
		</cfquery>
		<cfreturn qGetNodes>
	</cffunction>
	
	<cffunction name="getNodePath" access="remote" returntype="query">
		<cfargument name="IDNOEUD" required="true" type="numeric">
		<cfquery name="qGetNodePath" datasource="#SESSION.OFFREDSN#">
			select gc.idgroupe_client, gc.libelle_groupe_client as NOEUD, o.type_niveau as TYPE_NOEUD
			from   groupe_client gc, orga_niveau o
            where  gc.idorga_niveau=o.idorga_niveau
			start with gc.idgroupe_client=#arguments.IDNOEUD#
			connect by prior gc.id_groupe_maitre=gc.idgroupe_client
            order by level desc
		</cfquery>
		<cfreturn qGetNodePath>
	</cffunction>
</cfcomponent>
