<cfcomponent name="Application">
	<cfset THIS.name = "CONSOTEL_CONSOVIEW_4_MAIN_DEV">
	<cfset THIS.applicationtimeout="#createtimespan(2,0,0,0)#">
	<cfset THIS.sessionmanagement = TRUE>
	<cfset THIS.setclientcookies = TRUE>
	<cfset THIS.sessionTimeout="#createTimeSpan(0,2,0,0)#">
	<cfset THIS.accessManager=createObject("component","fr.consotel.consoview.access.AccessManager")>	
	<cfset THIS.BI_SERVER = "http://developpement.consotel.fr/xmlpserver"/>
	<cfset THIS.BI_SERVICE_URL = THIS.BI_SERVER & "/services/PublicReportService?WSDL"/>
	 
	
	<cfcookie name="IP_ORIGINE" value="#CGI.REMOTE_ADDR#" domain="consotel.fr" expires="never">
	 		
	<cffunction name="onApplicationStart">
		<cfargument name="ApplicationScope"/>
		
		<cfif Not IsDefined('APPLICATION.MAIL_SEVRER')>
			<cfset APPLICATION.MAIL_SEVRER = "mail.consotel.fr:26">
		</cfif>
		
		<cfif Not IsDefined('APPLICATION.BI_SERVER')>
			<cfset APPLICATION.BI_SERVER = "http://developpement.consotel.fr">
		</cfif>
		
		<cfif Not IsDefined('APPLICATION.BI_SERVICE_URL')>
			<cfset APPLICATION.BI_SERVICE_URL = "http://developpement.consotel.fr/xmlpserver/services/PublicReportService?WSDL">
		</cfif>
		
		<cflog type="Information" text="APPLICATION #THIS.name# starts">		
	 </cffunction>

	<cffunction name="onApplicationEnd">
		<cfargument name="ApplicationScope" required="true"/>
		<cfset var appName=structKeyExists(ARGUMENTS.ApplicationScope,"NAME") ? ARGUMENTS.ApplicationScope.NAME:(
			structKeyExists(ARGUMENTS.ApplicationScope,"applicationName") ? ARGUMENTS.ApplicationScope.applicationName:""
		)>
	   	<cflog type="Information" text="APPLICATION #ARGUMENTS.ApplicationScope.applicationName# ends">
	</cffunction>
	
	<cffunction name="onSessionStart">
	   	<cflog type="Information" text="SESSION/#CGI.REMOTE_ADDR# #SESSION.SESSIONID# starts on #THIS.name#">
	   	<cfset SESSION.CREATED=lsDateFormat(NOW(),"DD/MMMM/YYYY") & "/" & lsTimeFormat(NOW(),"HH:mm:ss")>
	   	<cfset SESSION.APPLICATION=#THIS.name#>
	   	<cfset SESSION.IP_ADDR=CGI.REMOTE_ADDR>
		<cfset COOKIE.IP_ORIGINE=CGI.REMOTE_ADDR>
	   	<cfset SESSION.BISERVER=THIS.BI_SERVER>
	   	<cfset SESSION.BI_SERVICE_URL=THIS.BI_SERVICE_URL>	   	
	   	<cfset SESSION.BIP_SERVER_NAME="developpement.consotel.fr">
	   	<cfset SESSION.BIP_CONTEXT_ROOT="xmlpserver">
	</cffunction>

	<cffunction name="onRequestStart" access="remote" returntype="boolean">
		 
	   	<cflog type="Information" text="REQUEST/#CGI.REMOTE_HOST# starts on #THIS.name#">
	   	 
	   	<cfset SESSION.REQUEST_DATE=lsDateFormat(NOW(),"DD/MMMM/YYYY") & "/" & lsTimeFormat(NOW(),"HH:mm:ss")>
	   	<cfif structKeyExists(SESSION,"AUTH_STATE")>
		   	<cfif SESSION.AUTH_STATE EQ 1>
				<cflog type="Information" text="REQUEST - CONNECTED STATE">
			<cfelse>
			   	<cfif structKeyExists(SESSION,"FAILED_AUTH")>
					<cflog type="Information" text="REQUEST - DISCONNECTED STATE">
					<cfset THIS.accessManager.clearSession()>
				<cfelse>
					<cflog type="Information" text="FORBIDEN REQUEST - DISCONNECTED STATE">
					<cfset THIS.accessManager.clearSession()>
				   	<cfthrow type="AUTH_EXCEPTION" errorcode="1000"
				   			message="SESSION/#CGI.REMOTE_HOST# AUTH_STATE (#SESSION.AUTH_STATE#) DISCONNECTED STATE">
				</cfif>
			</cfif>
		<cfelse>
			<cflog type="Information" text="REQUEST - INITIAL STATE">
			<cfset THIS.accessManager.clearSession()>
		</cfif>
		
	   	<cfreturn TRUE>
	</cffunction>

	<cffunction name="onRequestEnd">
		<cfargument type="String" name="targetTemplate"required="true"/>
		<!--- 
	   	<cflog type="Information" text="REQUEST/#CGI.REMOTE_HOST# ends on #THIS.name#">
	   	 --->
	   	<cfif structKeyExists(SESSION,"AUTH_STATE")>
		   	<cfif SESSION.AUTH_STATE EQ 1>
			   	<cflog type="Information" text="END - CONNECTED STATE">
			<cfelse>
				<cfif structKeyExists(SESSION,"FAILED_AUTH")>
				   	<cflog type="Information" text="END - FAILED AUTH - DISCONNECTED STATE">
				</cfif>
			</cfif>
		</cfif>
	</cffunction>
	
	<cffunction name="onSessionEnd">
		<cfargument name="SessionScope" required="true"/>
	   	<cflog type="Information" text="SESSION #arguments.SessionScope.SESSIONID# ends on #THIS.name#">
	</cffunction>
   
	<cffunction access="public" name="onError" returntype="void">
	    <cfargument name="Exception" type="Any" required="true" hint="Exception capturée"/>
	    <cfargument name="String" type="String" required="true" hint="Event Handler qui a généré l'exception"/>
	    <cfset var errorNotificationTo="monitoring@saaswedo.com">
			    
	    <cfif structKeyExists(ARGUMENTS.Exception,"Message") && structKeyExists(ARGUMENTS.Exception,"Detail")>
		    <cflog type="error" text="[APP] #ARGUMENTS.Exception.Message# [#ARGUMENTS.Exception.Detail#]">
		</cfif>		
	    <cfmail type="html" from="monitoring@saaswedo.com" to="#errorNotificationTo#" subject="[Application] Error Handler" server="mail-cv.consotel.fr" port="25">
		    <cfdump var="#ARGUMENTS.Exception#" label="Exception">
		</cfmail>		
		<cfif structKeyExists(Arguments.Exception,"rootCause")>
			<cfthrow object="#Arguments.Exception.rootCause#">
		<cfelse>
			<cfthrow object="#Arguments.Exception#">
		</cfif>	
	</cffunction>
</cfcomponent>
