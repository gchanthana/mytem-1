<cfcomponent output="false">
	<cfset OFFREDSN = "#SESSION.OFFREDSN#">
	
	<cffunction name="containerListClientCV" returntype="query" access="remote">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.containerListClientCV">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerlistuserbyracine" returntype="query" access="remote">
		<cfargument name="idracine" type="Numeric" required="true">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.containerlistuserbyracine">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerlistuserbyracinetoShare" returntype="query" access="remote">
		<cfargument name="idracine" type="Numeric" required="true">
		
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.containerListUserByRacine_v3">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#SESSION.USER.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#SESSION.CURRENT_MODULE.CODE_MODULE#" cfsqltype="cf_SQL_VARCHAR" type="in">			
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerListRapport" returntype="query" access="remote">
		<cfargument name="iduser" 		type="Numeric">
		<cfargument name="type" 		type="Numeric">
		<cfargument name="search" 		type="string">
		<cfargument name="module" 		type="string">
		<cfargument name="nbstart" 		type="Numeric">
		<cfargument name="nbaffiche" 	type="Numeric">
		<cfargument name="idracine" 	type="Numeric">
		
		<cflog text="**************************** Container Liste Rapport : pkg_m62.containerListRapport_V3">
		<cflog text="**************************** langue : #session.user.globalization#">
		
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.containerListRapport_V3">
			<cfprocparam value="#iduser#" 						cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#type#" 						cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#search#" 						cfsqltype="cf_SQL_VARCHAR" type="in">
			<cfprocparam value="#module#" 						cfsqltype="cf_SQL_VARCHAR" type="in">
			<cfprocparam value="#nbstart#" 						cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#nbaffiche#" 					cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idracine#" 					cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#SESSION.USER.GLOBALIZATION#" 	cfsqltype="cf_SQL_VARCHAR" type="in">
			<cfprocparam value="#SESSION.CODEAPPLICATION#"		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerListRapport_V2" returntype="query" access="remote">
		<cfargument name="iduser" 		type="Numeric">
		<cfargument name="type" 		type="Numeric">
		<cfargument name="search" 		type="string">
		<cfargument name="module" 		type="string">
		<cfargument name="nbstart" 		type="Numeric">
		<cfargument name="nbaffiche" 	type="Numeric">
		<cfargument name="idracine" 	type="Numeric">
		<cflog text="**************************** Container Liste Rapport : pkg_m62.containerListRapport_V3">
		<cflog text="**************************** langue : #session.user.globalization#">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.containerListRapport_V4">
			<cfprocparam value="#iduser#" 						cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#type#" 						cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#search#" 						cfsqltype="cf_SQL_VARCHAR" type="in">
			<cfprocparam value="#module#" 						cfsqltype="cf_SQL_VARCHAR" type="in">
			<cfprocparam value="#nbstart#" 						cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#nbaffiche#" 					cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idracine#" 					cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#SESSION.USER.GLOBALIZATION#" 	cfsqltype="cf_SQL_VARCHAR" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerListModuleByUser" access="remote" returntype="query">
		<cfargument name="iduser" type="numeric">
		<cfargument name="idracine" type="numeric">
		
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.containerListModuleByUser">
			<cfprocparam value="#iduser#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idracine#"		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerListComments" returntype="query" access="remote">
		<cfargument name="iddoc" type="Numeric">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.containerListComments">
			<cfprocparam value="#iddoc#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerAddComments" returntype="numeric" access="remote">
		<cfargument name="iddoc" 		type="Numeric">
		<cfargument name="iduser" 		type="Numeric">
		<cfargument name="commentaire"	type="string">
		<cfargument name="date" 		type="string">
		<cfargument name="heure" 		type="string">
		
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.containerAddComments">
			<cfprocparam value="#iddoc#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#iduser#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#commentaire#" 	cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#date#" 		cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#heure#" 		cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="p_retour" 	cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
<!---  ENVOI UN MAIL A UN CLIENT LE NOTIFIANT QUE DES DOCUMENTS ONT ETE AJOUTE A SON CONTAINER --->
	<cffunction name="SENDMailPublication" returntype="Numeric" access="remote">
		<cfargument name="IDUSER" 	type="numeric"/>
		<cfargument name="IDRACINE" type="numeric"/>
		<cfargument name="infosDoc" type="Struct"/>
		
	 	<cfset api = createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
		<cfset strRight = api.KNOWMailRight(IDUSER,IDRACINE,"GENERE")> 
		
 		<cfif strRight eq 1> --->
			<cfset mailFactory	= 	createObject("component","fr.consotel.consoview.api.container.mail.SENDMailPublicationParProductionClient")>
			<cfset mailSend 	=	mailFactory.sendMail(infosDoc)>
			<cfreturn mailSend>
		<cfelse>
			<cfreturn -1>
		</cfif> 
	</cffunction>
	<cffunction name="ADDDocumentTableContainer" returntype="String" access="remote">
		<cfargument name="NOM_DOC" 				type="string" 	required="true" 					hint="NOM LONG DU DOCUMENT (avec espace)"> 
		<cfargument name="SHORT_NAME" 			type="string" 	required="true" 					hint="NOM COURT DU DOCUMENT (sans espace)"> 
		<cfargument name="JOB_ID" 				type="numeric" 	required="false" 	default="-1" 	hint="ID DU JOB DANS BI"> 
		<cfargument name="CODE_RAPPORT"			type="string" 	required="false" 	default=""		hint="CODE DU DOCUMENT"> 
		<cfargument name="APP_LOGINID" 			type="numeric" 	required="false" 	default="-1"	hint="ID DE L'UTILISATEUR CONNECTE"> 
		<cfargument name="IDRACINE" 			type="numeric" 	required="false" 	default="-1"	hint="ID RACINE"> 
		<cfargument name="NOM_MODULE" 			type="string" 	required="false" 	default=""		hint="MODULE D'OU A ETE DEMANDE LE DOCUMENT"> 
		<cfargument name="FORMAT" 				type="string"	required="false" 	default=""		hint="FORMAT DU DOCUMENT"> 
		<cfargument name="DATE" 				type="string" 	required="false" 	default=""		hint="DATE DE PLANIFICATION DU JOB SOUS BI"> 
		<cfargument name="SERVER_BI" 			type="string" 	required="false" 	default=""		hint="NOM DU SERVER BI"> 
		<cfargument name="APPLI" 				type="string" 	required="false" 	default=""		hint="ID DE L'APPLI"> 
		<cfargument name="IS_PUBLIC" 			type="boolean" 	required="false" 	default="0"		hint="JE SAIS PAS"> 
		<cfargument name="STATUS" 				type="numeric" 	required="false" 					hint="LE STATUS DU DOCUMENT => VALEUR POSSIBLE : 2 DISPONIBLE, 3 ERREUR, 1 ENCOURS">
		<cfargument name="TAILLE" 				type="numeric" 	required="false" 	default="-1"	hint="LA TAILLE DU DOCUMENT NON ARCHIVE">
		<cfargument name="TAILLEZIP" 			type="numeric"	required="false" 	default="-1"	hint="LA TAILLE DU DOCUMENT ARCHIVE (.ZIP)">
		
		<cfset api = createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
		<cfset p_retour = api.ADDDocumentTableContainer(NOM_DOC,SHORT_NAME,JOB_ID,CODE_RAPPORT,APP_LOGINID,IDRACINE,NOM_MODULE,FORMAT,DATE,#SESSION.bip_server_name#,APPLI,IS_PUBLIC,STATUS,TAILLE,TAILLEZIP,#SESSION.CODEAPPLICATION#)>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="DELETEDocumentTableContainer" returntype="String" access="remote">
		<cfargument name="arr" 	type="Array" 	required="true" hint="la liste des documents à supprimer"> 
		<cfargument name="app_loginid" 	type="numeric"	required="true"	hint="app_loginid de l'utilisateur qui demande la suppression">
			
		<cfset retour = 0>
		
		<cfloop index="i" from="1" to=#ArrayLen(arr)#>
		<!--- Suppression du fichier dans la table container --->	
			<cfset FILEID = val(arr[i]['FILEID'])>
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.CONTAINERDELETERAPPORT">
				<cfprocparam value="#FILEID#" 		cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#app_loginid#"	cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam variable="p_retour"	cfsqltype="CF_SQL_INTEGER"  type="out">
			</cfstoredproc>
			<cfif p_retour eq -1>
					<cfset retour = -1>
			<cfelseif p_retour eq 1>
				<!--- Suppression du fichier du FTP --->
				<cfset UUID   		= arr[i]['UUID']>
				<cflog text="*********************** Suppression du fichier du FTP : #UUID#">
				<cfset del = DELETEDocumentToFTP(UUID)>
				<cfset retour = del>
			<cfelseif p_retour eq 2>
				<cfset retour = 2>
			</cfif>
		</cfloop>
		<cfreturn retour>
	</cffunction>
	<cffunction name="containerShareRapport" returntype="numeric" access="remote">
		<cfargument name="arrLogin" type="Array">
		<cfargument name="arrFile" 	type="Array">
		<cfargument name="iduser"	type="Numeric">
		
		<cfset retour = 1>
		
		<cfloop index="i" from="1" to="#ArrayLen(arrLogin)#">
			<cfloop index="j" from="1" to="#ArrayLen(arrFile)#">
				<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.CONTAINERSHARERAPPORT">
					<cfprocparam value="#val(arrFile[j]['FILEID'])#" 		cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#iduser#"							cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#val(arrLogin[i]['APP_LOGINID'])#" 	cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocparam variable="p_retour" 						cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
				
				<cfif p_retour eq -1>
					<cfset retour = -1>
				</cfif>					
			</cfloop>	
		</cfloop>
		<cfreturn retour>
	</cffunction>
	<!--- EDITER UN DOCUMENT --->	
	<cffunction name="EDITNameDocumentTableContainer" 	access="public" returntype="numeric" 	hint="modifie les données d'un document. En V1 : uniquement le nom du rapport, visible dans l'IHM">
		<cfargument name="NOM_DOC" 	type="string" 	required="true" hint="NOM LONG DU DOCUMENT (avec espace)"> 
		<cfargument name="FILEID" 	type="numeric" 	required="true" hint="ID DU FICHIER A EDITER">
		
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.EDITDocumentTableContainer">
			<cfprocparam value="#FILEID#" 		cfsqltype="CF_SQL_INTEGER"  type="in">
			<cfprocparam value="#NOM_DOC#"		cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam variable="p_retour"	cfsqltype="CF_SQL_INTEGER"  type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="DOWNLOADListDocument" returntype="Any" access="remote">
		<cfargument name="list" type="array" required="true">
		<cfargument name="archivename" type="String" required="true">
		
		<cfset arr = ArrayNew(1)>
		
		<cfloop index="i" from="1" to=#ArrayLen(list)#>
			<cfset arr[i] = list[i]['UUID']>
		</cfloop>
		
		<cfset zip = CREATEZipArchive(arr,archivename)>
		
		<!--- <cfheader name="Content-Disposition" value="inline;filename=#archiveName#.zip">
		<cfcontent type="multipart/x-zip" variable="#zip#"> --->
		
		<cfreturn zip>
	</cffunction>
	<cffunction name="containerDepotRapport" returntype="numeric" access="remote">
		<cfargument name="arrLogin" type="Array">
		<cfargument name="arrFile" 	type="Array">
		<cfargument name="iduser"	type="Numeric">
		
		<cfset retour = 1>
		
		<cfloop index="i" from="1" to="#ArrayLen(arrLogin)#">
			<cfloop index="j" from="1" to="#ArrayLen(arrFile)#">
				<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.CONTAINERSHAREFILE">
					<cfprocparam value="#arrFile[j]#" 						cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocparam value="#iduser#" 							cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#val(arrLogin[i]['APP_LOGINID'])#" 	cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocparam variable="p_retour" 						cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
				
				<cfif p_retour eq -1>
					<cfset retour = -1>
				</cfif>					
			</cfloop>	
		</cfloop>
		<cfreturn retour>
	</cffunction>
	<cffunction name="DOWNLOADListDocumentClient" returntype="void" access="remote" output="true">
		<cfargument name="list" type="array" required="true">
		<cfargument name="archivename" type="String" required="true">
		
		<cfset arr = ArrayNew(1)>
		
		<cfloop index="i" from="1" to=#ArrayLen(list)#>
			<cfset arr[i] = list[i]['UUID']>
		</cfloop>
		
		<cfset zip = CREATEZipArchiveClient(arr,archivename)>
	</cffunction>
	<cffunction name="LISTEparams" returntype="query" access="remote">
		<cfargument name="iddoc" type="Numeric">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.getParams">
			<cfprocparam value="#iddoc#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="setStatus" returntype="Any" access="remote">
		<cfargument name="uuid" 		type="String" 	required="true">
		<cfargument name="STATUS_ID" 	type="numeric" 	required="true">	

		<cflog  text="********************* SETSTATUS (uuid : #uuid# - status_id = #STATUS_ID#)">

		<cfset api = createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
		<cfset setStatusResult = api.MAJStatus(uuid,STATUS_ID)>				

		<cflog  text="********************* SETSTATUS result : #setStatusResult#">

		<cfreturn setStatusResult>
	</cffunction>	

<!--- 
		METHODE PRIVATE
 --->
	
	<!--- CREATION DE ZIP --->	
		<cffunction name="CREATEZipArchive" 			access="private" 	returntype="Any"	hint="méthode de création d'une archive(.zip) d'un répertoire contenant des documents">	
			<cfargument name="arUuid" 		type="Array" 	required="true" 	hint="Array de UUID qui correspondent aux fichiers demandés" >
		    <cfargument name="archiveName" 	type="string" 	required="true" 	hint="nom de l'archive">
		    
		    <cfset tempUuid=createUUID()>
		    <cfset arrDoc = GETInfosFromUUID(arUuid)>
			<cfif arrDoc.recordcount gt 0>
	            <cfset filepath="/container/#tempUuid#">
	            <cfset racineDir="/container">
		            <!--- Supprime ancien répertoire si il existe --->
		            <cftry>
		           	<cfdirectory action="delete" directory="#filepath#" mode="777" recurse="true">
		            <cfcatch></cfcatch>
		            </cftry>
		            <!--- création répertoire temporaire --->
		            <cfdirectory action="create" directory="#filepath#">	
		            <!--- boucle sur les fichiers --->
		            <cfset index=1>
		            <cfloop query="arrDoc">
		                  <cfset uuid=arrDoc.uuid>
		                  <cfset filename="#arrDoc.short_name_rapport#_#index#">
		                  <cfset filesuffix=arrDoc.format_fichier>
		                  <cffile action="copy" source="#racineDir#/#uuid#"
												destination="#filepath#/#filename#.#filesuffix#">
		                  <cfset index=index+1>
		            </cfloop>
		            <!--- Zip les fichiers --->
		            <cfzip action="zip" source="#filepath#/" file="#racineDir#/#archiveName#.zip"/>
		            <cffile action="readbinary" variable="fileContent" file="#racineDir#/#archiveName#.zip">
	 		        <cffile action="delete" file="#racineDir#/#archiveName#.zip">
		            <!--- Supression des fichiers individuels --->
		            <cfset index=1>
		            <cfloop query="arrDoc">
		                  <cfset filename="#arrDoc.short_name_rapport#_#index#">
		                  <cfset filesuffix=arrDoc.format_fichier>
		                  <cffile action="delete" file="#filepath#/#filename#.#filesuffix#">
		                  <cfset index=index+1>
		            </cfloop>
		    		<cfdirectory action="delete" directory="#filepath#" recurse="true">
		    		<cfdirectory action="list" directory="#filepath#" name="rslt">
		    		<cfif rslt.recordcount eq 1>
						<cflog text="********************** le répertoire #filepath# n'a pas été supprimé (ECHEC)">
					<cfelse>
						<cflog text="********************** le répertoire #filepath# a été supprimé (REUSSITE)">
					</cfif>
		    	</cfif>
		    <cfreturn fileContent>
		</cffunction>
	<!--- CREATION DE ZIP CLIENT --->	
		<cffunction name="CREATEZipArchiveClient" 		access="private" 	returntype="void"	hint="méthode de création d'une archive(.zip) d'un répertoire contenant des documents" output="true">	
			<cfargument name="arUuid" 		type="Array" 	required="true" 	hint="Array de UUID qui correspondent aux fichiers demandés" >
		    <cfargument name="archiveName" 	type="string" 	required="true" 	hint="nom de l'archive">
		    
		    <cfset tempUuid=createUUID()>
		    <cfset arrDoc = GETInfosFromUUID(arUuid)>
			<cfif arrDoc.recordcount gt 0>
	            <cfset filepath="/container/#tempUuid#">
	            <cfset racineDir="/container">
		            <!--- Supprime ancien répertoire si il existe --->
		            <cftry>
		           	<cfdirectory action="delete" directory="#filepath#" mode="777" recurse="true">
		            <cfcatch></cfcatch>
		            </cftry>
		            <!--- création répertoire temporaire --->
		            <cfdirectory action="create" directory="#filepath#">	
		            <!--- boucle sur les fichiers --->
		            <cfset index=1>
		            <cfloop query="arrDoc">
		                  <cfset uuid=arrDoc.uuid>
		                  <cfset filename="#arrDoc.short_name_rapport#_#index#">
		                  <cfset filesuffix=arrDoc.format_fichier>
		                  <cffile action="copy" source="#racineDir#/#uuid#"
												destination="#filepath#/#filename#.#filesuffix#">
		                  <cfset index=index+1>
		            </cfloop>
		            <!--- Zip les fichiers --->
		            <cfzip action="zip" source="#filepath#/" file="#racineDir#/#archiveName#.zip"/>
		            <cffile action="readbinary" variable="fileContent" file="#racineDir#/#archiveName#.zip">
	 		        <cffile action="delete" file="#racineDir#/#archiveName#.zip">
		            <!--- Supression des fichiers individuels --->
		            <cfset index=1>
		            <cfloop query="arrDoc">
		                  <cfset filename="#arrDoc.short_name_rapport#_#index#">
		                  <cfset filesuffix=arrDoc.format_fichier>
		                  <cffile action="delete" file="#filepath#/#filename#.#filesuffix#">
		                  <cfset index=index+1>
		            </cfloop>
		    		<cfdirectory action="delete" directory="#filepath#" recurse="true">
		    		<cfdirectory action="list" directory="#filepath#" name="rslt">
		    		<cfif rslt.recordcount eq 1>
						<cflog text="********************** le répertoire #filepath# n'a pas été supprimé (ECHEC)">
					<cfelse>
						<cflog text="********************** le répertoire #filepath# a été supprimé (REUSSITE)">
					</cfif>
		    	</cfif>
		    	<cfheader name="Content-Disposition" value="inline;filename=#archiveName#.zip">
				<cfcontent type="multipart/x-zip" variable="#fileContent#">
		</cffunction>
	<!--- RECUPERER LES INFOS D'UN DOCUMENT --->	
		<cffunction name="GETInfosFromUUID" 			access="public" 	returntype="query"		hint="">
      		<cfargument name="arUuid" type="Array" required="true">
		
			<cfset arr = ArrayToList(arUuid)>
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.UUIDARRAYTOQUERY">
				<cfprocparam value="#arr#" cfsqltype="CF_SQL_CLOB" type="in">
				<cfprocresult name="retour">
			</cfstoredproc>
			
      		<cfreturn retour>
		</cffunction>
	<!--- SUPPRIMER UN FICHIER SUR LE FTP --->
		<cffunction name="DELETEDocumentToFTP" 			access="public" 	returntype="string" 	hint="méthode de suppression de document sur le FTP">	
			<cfargument name="uuid"	type="string"	required="true">
			
			<cfset retour = 0>
			
			<cftry>
				<cffile action="delete" file="/container/#uuid#">
				<cfdirectory action="list" directory="/container/#arguments.uuid#" name="listDir" listInfo="all">
				<cfif listDir.recordCount eq 1>
					<cflog text="************** containerService.DELETEDocumentToFTP : ERREUR">
				<cfelse>
					<cflog text="************** containerService.DELETEDocumentToFTP : SUCCES">
				</cfif>
				
				<cfcatch>
					<cfset retour = -1>
				</cfcatch>
			</cftry>
			<cfset retour = uuid>
			<cfreturn retour>
		</cffunction>
</cfcomponent>