<cfcomponent displayname="GetListNews" output="false">

 	<cffunction name="getListNews" access="remote" returntype="struct" output="false"  hint="retourne la liste des articles">
                
			<cfset codeApplication  = SESSION.CODEAPPLICATION>
			<cfset codeLang  = SESSION.USER.GLOBALIZATION>
			<cftry>
				<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.getListeActualite">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_codeApp" type="in" value="#codeApplication#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_lang" type="in" value="#codeLang#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
				<cfset qResult = structNew()>
                <cfset qResult.result = p_retour>
                        
                <cfreturn qResult>                            
				<cfcatch type="Any">
			
				</cfcatch>
			</cftry>

	</cffunction>
</cfcomponent>