<cfcomponent displayname="DataManager" hint="traitement des données KPI/Widget" output="false">
	
	<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
	<cfset lastFacture 	= LSDateFormat(SESSION.PERIMETRE.STRUCTDATE.DATELASTFACTURE, 'yyyy-mm-dd')>
	<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
	<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
	
	
	<!--- 
		recuperer les parametres utiles à chaque KPI/Widget pour afficher ses propres datas 
	--->
	<cffunction name="getParametres" access="remote" output="false" returntype="Query"
				hint="recuperer les parametres utiles à chaque KPI/Widget pour afficher ses propres datas">
				
		<cfargument name="onglet_widgetid" type="numeric" required="true">
				
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m54.getParamGlobPermanent">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" 	type="in" variable="p_onglet_widgetid" 	value="#onglet_widgetid#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" 	type="in" variable="p_idracine" 		value="#idRacine#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR2" 	type="in" variable="p_date_last_fact" 	value="#lastFacture#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" 	type="in" variable="p_langid" 			value="#idLangue#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>
	
	<!--- 
		updater le parametre permanent du KPI/Widget dès qu'un utilisateur modifie la valeur du selecteur
	--->
	<cffunction name="updateParametrePermanent" access="remote" output="true" returntype="Array"
				hint="modifier le parametre permanent du KPI/Widget dès qu'un utilisateur le modifie">
					
		<cfargument name="widget_ongletid" 	type="numeric" 	required="true">
		<cfargument name="widget_paramid" 	type="numeric" 	required="true">
		<cfargument name="arrayValue" 		type="array" 	required="true">
		
		<cfset arrayRetour = arrayNew(1)>
		<cfset x = 1>
		<cfloop from="1" to="#ArrayLen(arrayValue)#" index="i" step="1">
		
			<cfloop from="1" to="#ArrayLen(arrayValue[i])#" index="j" step="1">
				
				<cfset valeur = ARGUMENTS["arrayValue"][i][j]["value"][1]>
				
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m54.updateParamValue">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" 	type="in" 	variable="p_onglet_widgetid"	value="#widget_ongletid#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" 	type="in" 	variable="p_widget_paramid" 	value="#widget_paramid#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR2" 	type="in" 	variable="p_value" 				value="#valeur#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" 	type="out" 	variable="p_retour">
				</cfstoredproc>
				
				<cfset arrayRetour[x] = p_retour> <!--- 1 ou -1 --->
				<cfset x++>
			</cfloop>
			
		</cfloop>
		
		<cfreturn arrayRetour>
		
	</cffunction>
	
</cfcomponent>