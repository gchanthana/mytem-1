<cfcomponent displayname="SessionManager" hint="recupere certaines valeurs à mettre en SESSION.ID_PERIMETRE" output="false">
	
	<!--- 
		initialise des variables de session
	 --->
	<cffunction name="initializeVarSession" access="public" output="false" returntype="void"
				hint="initialise des variables de session">
					
			<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ SESSION.PERIMETRE.ID_GROUPE>
		   		<cfset idorga = #SESSION.PERIMETRE.ID_GROUPE#>
		    <cfelse>
		    	<cfset rsltIdOrga = getOrga()>
		   		<cfset idorga = Val(rsltIdOrga)>
		    </cfif>
		    <cfset SESSION.PERIMETRE.ID_ORGANISATION = idorga>
			<cfset SESSION.PERIMETRE.NIVEAU = getNiveauPerimetre(SESSION.PERIMETRE.ID_PERIMETRE)>
			<cfset SESSION.PERIMETRE.ID_NIVEAU = getIdNiveau(SESSION.PERIMETRE.NIVEAU)>
			
	</cffunction>
	
	
	<!--- 
		recupere id_organisation
	 --->
	<cffunction name="getOrga" access="private" output="false" returntype="Any"
				hint="recupere id_organisation">
					
			<cfquery datasource="#SESSION.OFFREDSN#" name="rsltOrga">
				SELECT PKG_CV_RAGO.GET_ORGA(#SESSION.PERIMETRE.ID_PERIMETRE#) as IDORGA FROM DUAL
			</cfquery>
			
		<cfreturn rsltOrga['IDORGA'][1]>
	</cffunction>
	
	<!--- 
		recupere niveau du perimetre
	 --->
	 <cffunction name="getNiveauPerimetre" access="private" output="false" returntype="String"
				hint="recupere niveau du perimetre">
					
		 	<cfargument name="perimetre" type="numeric" required="true">
		 	
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_consoview_v3.get_numero_niveau">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idgroupe_client" value="#perimetre#">			
				<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR" 	variable="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		recupere id_niveau
	 --->
	<cffunction name="getIdNiveau" access="private" output="false" returntype="numeric"
				hint="recupere id_niveau">
						
		<cfargument name="niveau" type="string" required="true">
					 				
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_consoview_v3.niveau_to_integer">	
			<cfprocparam type="in" value="#niveau#" variable="p_niveau" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="out"  variable="p_idniveau" cfsqltype="CF_SQL_iNTEGER">
		</cfstoredproc>
				
		<cfreturn p_idniveau>
	</cffunction>
	
</cfcomponent>