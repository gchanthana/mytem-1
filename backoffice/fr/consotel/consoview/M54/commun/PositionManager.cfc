<cfcomponent displayname="PositionManager" hint="Updater la position des KPIs/Widgets" output="false">
	
	<!--- 
		modifier le parametre position du KPI/Widget dès qu'un utilisateur modifie sa position dans le tableau d'affichage 
	--->
	<cffunction name="updatePosition" access="remote" output="true" returntype="Array"
				hint="modifier le parametre position du KPI/Widget dès qu'un utilisateur modifie sa position dans le tableau d'affichage">
				
		<cfargument name="tableauParamPosition" 	type="array" 	required="true">
		<cfargument name="param" 					type="numeric" 	required="true">
		
		<cfscript>
			
			x = 1;
			arrayRetour = arrayNew(1);
			
			for(i=1; i LTE arraylen(tableauParamPosition) ; i++)// boucle sur chaque indice de l'array
			{
				key=tableauParamPosition[i];

				// value de chaque clé de l'objet contenu dans l'indice de l'array
				onglet_widgetid	= key.widgetNumTab_id;
				ongletid 		= key.numTab;
				id 				= key.idComposant; // id du KPI-Widget
				position 		= key.positionIHM;
				
				// procedure
				spService = new storedProc();
				spService.setDatasource(Session.OFFREDSN);
				spService.setProcedure("pkg_m54.updatePositionWidgetOnglet");
				spService.addParam(cfsqltype="CF_SQL_INTEGER", type="in", value=onglet_widgetid);
				spService.addParam(cfsqltype="CF_SQL_INTEGER", type="in", value=ongletid);
				spService.addParam(cfsqltype="CF_SQL_INTEGER", type="in", value=id);
				spService.addParam(cfsqltype="CF_SQL_INTEGER", type="in", value=position); // si position < 0, ça veut dire que le widget/kpi a été supprimé de l'onglet
				spService.addParam(cfsqltype="CF_SQL_INTEGER", type="out", variable="p_retour");
				
				result = spService.execute();
				p_retour = result.getProcOutVariables().p_retour; // 1 ou -1
				
				arrayRetour[x] = p_retour;
				x++;
			}
			return arrayRetour;
			
		</cfscript>
		
		<!--- 
		<cfset x = 1>
		<cfset arrayRetour = arrayNew(1)>
		
		<cfloop array="#tableauParamPosition#" index="key">
			
			<cfset onglet_widgetid = key.widgetNumTab_id>
			<cfset ongletid = key.numTab>
			<cfset id = = key.idComposant>
			<cfset position = key.positionIHM>
			
			<!--- procedure --->
			<cfstoredproc datasource="" procedure="">
				
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="" value="#onglet_widgetid#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="" value="#ongletid#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="" value="#id#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="" value="#position#"> <!--- si position < 0, ça veut dire que le widget/kpi a été supprimé de l'onglet --->
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour"> <!--- 1 ou -1 --->
			</cfstoredproc>
			
			<cfset arrayRetour[x] = p_retour>
			<cfset x++>
			
			<cfreturn arrayRetour>
			
		</cfloop> 
		--->
		
	</cffunction>
		
</cfcomponent>