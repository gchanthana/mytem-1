<cfcomponent displayname="ListeComposantService" hint="recuperer la liste de tous les composants KPI/Widget" output="false">
	
	<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
	<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
	<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
	<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
	<cfset sess = createObject("component","fr.consotel.consoview.M54.commun.SessionManager")>
	
	<!--- 
		recuperer la liste des KPIs/Widgets de la racine 
	--->
	<cffunction name="getListeComposantByRacine" access="remote" output="false" returntype="Query" 
				hint="recuperer la liste des KPIs/Widgets de la racine">
		
		<cfargument name="type" type="numeric" required="true" hint="1 pour type KPI, 2 pour type widget">
		
		<cfset sess.initializeVarSession()> <!--- initialisation de variables de session (utile pour requetes BI qui vont etre appelées) --->
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m54.getWidget">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_app_loginid" 		value="#idUser#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idracine" 		value="#idRacine#"><!--- p_idgroupe_client --->
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_type" 			value="#type#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_langid" 			value="#idLangue#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>
	
	<cffunction name="getListeComposantByUser" access="remote" output="false" returntype="Query" 
				hint="recuperer la liste des KPIs/Widgets de la racine">
		
		<cfargument name="type" type="numeric" required="true" hint="1 pour type KPI, 2 pour type widget">
		
		<cfset sess.initializeVarSession()> <!--- initialisation de variables de session (utile pour requetes BI qui vont etre appelées) --->
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m54.getWidgetOnglet">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_app_loginid" 		value="#idUser#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idracine" 		value="#idRacine#"><!--- p_idgroupe_client --->
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_type" 			value="#type#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_langid" 			value="#idLangue#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>
	
	<cffunction name="getAllComposant" access="remote" output="false" returntype="array" 
				hint="recuperer la liste de tous les  KPIs/Widgets">
		
		<cfargument name="type" type="numeric" required="true" hint="1 pour type KPI, 2 pour type widget">
		
		<cfset qRacineComponent=getListeComposantByRacine(type)>
		<cfset qUserComponent=getListeComposantByUser(type)>
		<cfset tab=ArrayNew(1)>
		<cfset tab[1]=qRacineComponent>
		<cfset tab[2]=qUserComponent>	
		
		<cfreturn tab>	
	
	</cffunction>
	
</cfcomponent>