<cfcomponent displayname="WidgetDataManager" hint="traitement des données Widget" output="false">

	<cfset datasourceBI = "BI_TEST">

	<!---
		appeler la methode recuperant les données Widget
	 --->
	<cffunction name="getDataWidget" access="remote" output="true" returntype="Any"
				hint="appeler la méthode récupérant les données Widget">

		<cfargument name="code" 		type="string" 	required="true" hint="le code du Widget est le nom de la methode à appeler">
		<cfargument name="array" 		type="Array" 	required="true">

		<cfset arrayParam = initStructParam(array)>

		<cftry>
			<cfinvoke method="#code#" returnvariable="queryRetour">
				<cfloop array="#arrayParam#" index="x"><!--- boucle sur l'array contenant une structure clé/valeur --->
					<cfif isStruct(x)>
						<cfloop collection="#x#" item="item">
							<cfinvokeargument name="#item#" value="#x[item]#">
						</cfloop>
					</cfif>
				</cfloop>

			</cfinvoke>

			<cfreturn queryRetour>
		<cfcatch>
			<cflog type="error" text="[Page accueil-WidgetDataManager] Error: #CFCATCH.message# - Detail: #CFCATCH.detail#">
			<cfreturn cfcatch>
		</cfcatch>
		</cftry>

	</cffunction>

	<!--- DATAS Widgets --->

	<!---
		Widget roaming in and out
	--->
	<cffunction name="Widget_1" access="remote" output="false" returntype="Query" hint="Widget roaming in and out">
		<cfargument name="L_PERIODE" type="numeric" required="true">

		<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
        <cfset p_idracine_master= SESSION.PERIMETRE.IDRACINE_MASTER>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>

        <cfstoredproc procedure="pkg_m54.getRoamingMap" datasource="#SESSION.OFFREDSN#" >
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#p_idracine#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" 	value="#p_idracine_master#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" 			value="#p_idorga#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" 		value="#p_idperimetre#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" 			value="#p_niveau#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode" 			value="#L_PERIODE#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 		value="#p_langue_pays#">
				<cfprocresult name="qResult" >
		</cfstoredproc>

		<cfreturn qResult>

	</cffunction>

	<!---
		Widget évolution des coûts par segment sur 12 mois
	--->
	<cffunction name="Widget_2" access="remote" output="false" returntype="Query"
				hint="Widget évolution des coûts par segment sur 12 mois">

		<cfargument name="P_IDSEGMENT" type="numeric" required="true" hint="Si l'utilisateur choisit All segment, alors y n'est pas renvoyé">
		<cfargument name="L_PERIODE" type="numeric" required="true">


 		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>

		<cfstoredproc procedure="pkg_m54.getEvoCoutSegment" datasource="#SESSION.OFFREDSN#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_deb" value="#L_PERIODE-11#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_fin" value="#L_PERIODE#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_segement" value="#P_IDSEGMENT#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
			<cfprocresult name="qResult" >
		</cfstoredproc>

		<cfreturn qResult>
	</cffunction>

	<!---
		Widget répartition des coûts par opérateur
	--->
	<cffunction name="Widget_3" access="remote" output="false" returntype="Query"
				hint="Widget répartition des coûts par opérateur">

		<cfargument name="P_IDSEGMENT" type="numeric" required="true" hint="Si l'utilisateur choisit All segment, alors y n'est pas renvoyé">
		<cfargument name="L_PERIODE" type="numeric" required="true">

		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>

		<cfstoredproc procedure="pkg_m54.getRepartCoutOperateur" datasource="#SESSION.OFFREDSN#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_deb" value="#L_PERIODE#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_fin" value="#L_PERIODE#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_segement" value="#P_IDSEGMENT#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
			<cfprocresult name="qResult" >
		</cfstoredproc>

		<cfreturn qResult>

	</cffunction>

	<!---
		Widget évolution du coût des consos par thème
	--->
	<cffunction name="Widget_4" access="remote" output="false" returntype="Query"
				hint="Widget évolution du coût des consos par thème">

		<cfargument name="P_IDSEGMENT" type="numeric" required="true">
		<cfargument name="P_IDTHEME" type="string" required="true">
		<cfargument name="L_PERIODE" type="numeric" required="true">
		<!--- <cfset L_PERIODE = LSDateFormat(SESSION.PERIMETRE.STRUCTDATE.DATELASTFACTURE, 'yyyy-mm-dd')> --->

		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>

		<cfstoredproc procedure="pkg_m54.getEvoCoutTheme_v2" datasource="#SESSION.OFFREDSN#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_deb" value="#L_PERIODE-11#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_fin" value="#L_PERIODE#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_segement" value="#P_IDSEGMENT#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#P_IDTHEME#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
			<cfprocresult name="qResult" >
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction>


	<cffunction name="Widget_5" access="remote" output="false" returntype="Query"
				hint="Evolution des coûts par opérateur">

		<cfargument name="L_PERIODE" type="numeric" required="true">
		<cfargument name="P_IDSEGMENT" type="numeric" required="true">

		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>

		<cfstoredproc procedure="pkg_m54.getEvoCoutOperateur" datasource="#SESSION.OFFREDSN#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_deb" value="#L_PERIODE-11#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_fin" value="#L_PERIODE#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_segement" value="#P_IDSEGMENT#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
			<cfprocresult name="qResult" >
		</cfstoredproc>

		<cfreturn qResult>
	</cffunction>
	<!---
		Répartition des coûts géographique (sites France)
	--->
	<cffunction name="Widget_7" access="remote" output="false" returntype="Query"
				hint="Widget Répartition des coûts géographique (sites France)">

		<cfargument name="P_IDSEGMENT" type="numeric" required="true">
		<cfargument name="L_PERIODE" type="numeric" required="true">

		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>

		<cfstoredproc procedure="pkg_m54.getGeoMap" datasource="#SESSION.OFFREDSN#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_deb" value="#L_PERIODE#">
			<!--- <cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_segement" value="#P_IDSEGMENT#"> --->
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
			<cfprocresult name="qResult" >
		</cfstoredproc>

		<cfreturn qResult>
	</cffunction>

	<cffunction name="Widget_9" access="remote" output="false" returntype="Query" hint="Evolution des commandes">
		<cfargument name="P_POOL" type="any" required="false" default="-1">

		<cfset p_idracine 		= SESSION.PERIMETRE.ID_GROUPE>
		<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>

		<cfif P_POOL EQ -1 >
			<cfset req='"- ENTETE_COMMANDE".IDRACINE=#p_idracine#'>
		<cfelse>
			<cfset req='"- POOL_COMMANDE".IDPOOL=#P_POOL#'>
		</cfif>
		
		<cfquery name="qEvolutionCommandes" datasource="#datasourceBI#">
			set variable
				p_idracine = #p_idracine#,
				p_langue_pays = '#p_langue_pays#';
				
			select PERIODE."Mois de la commande"  moisCommande, 
				   "- ENTETE_COMMANDE".NB_COMMANDES  nbCommande,
				   "- ENTETE_COMMANDE".MONTANT_LOC  montantCommande,
				   DEVISE.SYMBOLE_LOC devise,
				   PERIODE."Année de la commande" anneeCommande,
				   PERIODE."Mois de l année" 	  moisAnneeCommade
			from E0_COMMANDE
			where #req#					
			and (PERIODE."Date de commande" > PERIODE."Il y a 11 mois")
			order by anneeCommande, moisAnneeCommade
		</cfquery>

		<cfreturn qEvolutionCommandes>

	</cffunction>


	<cffunction name="Widget_10" access="remote" output="false" returntype="Query" hint="Suivi des commandes encours (Ancien titre  : Etat des commandes)">
		<cfargument name="P_POOL" type="any" required="false" default="-1">
		
		<cfset p_idracine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset p_langue_pays	 = SESSION.USER.GLOBALIZATION>
		
		<cfif P_POOL EQ -1 >
			<cfset req='"- ENTETE_COMMANDE".IDRACINE=#p_idracine#'>
		<cfelse>
			<cfset req='"- POOL_COMMANDE".IDPOOL=#P_POOL#'>
		</cfif>

		<cfquery name="qEtatCommandes" datasource="#datasourceBI#">

			set variable
				p_idracine = #p_idracine#,
				p_langue_pays	 = '#p_langue_pays#';
			
			select saw_0 saw_0, saw_1 saw_1
			FROM
			(
				(
					select TRADUCTION."Nombre de commandes envoyées" saw_0, "- ENTETE_COMMANDE"."Nombre de commandes envoyées" saw_1
					from E0_COMMANDE
					where #req#
				)
				UNION
				(
					select TRADUCTION."Nombre de commandes non reçues" saw_0, "- ENTETE_COMMANDE"."Nombre de commandes non reçues" saw_1
					from E0_COMMANDE
					where #req#
				)
				UNION
				(
					select TRADUCTION."Nombre de commandes expédiées non reçues" saw_0, "- ENTETE_COMMANDE"."Nombre de commandes expédiées non reçues" saw_1
					from E0_COMMANDE
					where #req#
				)
				UNION
				(
					select TRADUCTION."Nombre de commandes reçues non clôturées" saw_0, "- ENTETE_COMMANDE"."Nombre de commandes reçues non clôturées" saw_1
					from E0_COMMANDE
					where #req#
				)
			) t1 order by saw_0

		</cfquery>
		<cfreturn qEtatCommandes>

	</cffunction>

	<cffunction name="Widget_11" access="remote" output="false" returntype="Query" hint="Inventaire de parc (ancien titre : Inventaire de parc mobile)">

		<cfargument name="P_POOL" type="any" required="false" default="-1">

		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	 = SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	 = SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	 = SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	 = SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	 = SESSION.PERIMETRE.ID_NIVEAU>
		<cfset p_langue_pays	 = SESSION.USER.GLOBALIZATION>
		
		<cfif P_POOL EQ -1 >
			<cfset req='"EQUIPEMENT-TERMINAL".IDRACINE=#p_idracine#'>
		<cfelse>
			<cfset req='"EQUIPEMENT-POOL".IDPOOL=#P_POOL#'>
		</cfif>
		
		<cfquery name="qIventaireDeParc" datasource="#datasourceBI#">

			set variable
			p_idracine_master=#p_idracine_master#,
			p_idracine=#p_idracine#,
			p_idorga=#p_idorga#,
			p_idperimetre=#p_idperimetre#,
			p_idniveau=#p_idniveau#,
			p_niveau='#p_niveau#',
			p_langue_pays = '#p_langue_pays#'; 
			SELECT saw_0 libelle, saw_1 valeur FROM
			(
				(
					SELECT TRADUCTION.Collaborateurs saw_0, "EQUIPEMENT-EMPLOYE-Fiche-Collaborateur".NB_EMPLOYES saw_1
					FROM E0_ENPROD_HIST 
					where #req#
				)
                UNION
				(
					SELECT TRADUCTION.lignes saw_0, "EQUIPEMENT-Fiche Ligne".NB_SOUS_TETES saw_1
					FROM E0_ENPROD_HIST 
					where #req#
					
				)
				UNION
				(
					SELECT TRADUCTION.SIM saw_0, "EQUIPEMENT-SIM".NB_SIMS saw_1
					FROM E0_ENPROD_HIST 
					where #req#
				)
				UNION
				(
					SELECT TRADUCTION.Terminaux saw_0, "EQUIPEMENT-TERMINAL".NB_TERMINAUX saw_1
					FROM E0_ENPROD_HIST 
					where #req#
				)
			)t1
		</cfquery>

		<cfreturn qIventaireDeParc>
	</cffunction>

	<cffunction name="Widget_12" access="remote" output="false" returntype="Query" hint=" Top 10 des équipements (Ancien titre : Top 10 devices mobile)">

		<cfargument name="P_POOL" type="any" required="false" default="-1">

		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	 = SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	 = SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	 = SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	 = SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	 = SESSION.PERIMETRE.ID_NIVEAU>

		<cfif P_POOL EQ -1 >
			<cfset req='"EQUIPEMENT-TERMINAL".IDRACINE=#p_idracine#'>
		<cfelse>
			<cfset req='"EQUIPEMENT-POOL".IDPOOL=#P_POOL#'>
		</cfif>
		
		<cfquery name="top10devices" datasource="#datasourceBI#">

			set variable
			p_idracine_master=#p_idracine_master#,
			p_idracine=#p_idracine#,
			p_idorga=#p_idorga#,
			p_idperimetre=#p_idperimetre#,
			p_idniveau=#p_idniveau#,
			p_niveau='#p_niveau#';

			SELECT RCOUNT("EQUIPEMENT-TERMINAL".T_MODELE) ordre,
						  "EQUIPEMENT-TERMINAL".T_MODELE nom_equipement,
						  "EQUIPEMENT-TERMINAL".NB_TERMINAUX nb_equipement
			FROM E0_ENPROD_HIST
			WHERE RCOUNT("EQUIPEMENT-TERMINAL".T_MODELE) < 11
			AND #req#
			ORDER BY nb_equipement DESC
		</cfquery>
		<cfreturn top10devices>
	</cffunction>

	<!---
		créer une struct de parametres
	--->
	<cffunction name="initStructParam" access="private" output="false" returntype="Array"
				hint="créer une struct de parametres">

		<cfargument name="array" type="array" required="true">

		<cfset arrayParam = arrayNew(1)>

		<cfset x = 1>
		<cfloop from="1" to="#ArrayLen(array)#" index="i" step="1">
			<cfloop from="1" to="#ArrayLen(array[i])#" index="j" step="1">

				<cfset cle = ARGUMENTS["array"][i][j]["cle"]>
				<cfset valeur = ARGUMENTS["array"][i][j]["value"][1]>
				<cfset arrayParam[x] = { #cle# = valeur}>
				<cfset x++>
			</cfloop>
		</cfloop>
		<cfreturn arrayParam>

	</cffunction>

</cfcomponent>