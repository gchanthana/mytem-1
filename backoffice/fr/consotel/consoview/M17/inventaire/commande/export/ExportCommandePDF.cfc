<cfcomponent extends="fr.consotel.consoview.util.publicreportservice.PublicReportServiceV2" output="false">
	
	<cffunction name="afficherCommandeEnPDF" access="public" output="false" returntype="any">		
		<cfargument name="idcommande" required="true" type="numeric">	 
		<cfset createRunReportRequest("consoview","public","/consoview/gestion/flotte/BDCmobile/BDCmobile.xdo","BDC1","pdf")>
		<cfset AddRunReportParameter("P_IDCOMMANDE","#idcommande#")>
		<cfset AddRunReportParameter("P_IDGESTIONNAIRE","#Session.user.CLIENTACCESSID#")>
		<cfset AddRunReportParameter("P_RACINE","#Session.perimetre.RAISON_SOCIALE#")>
		<cfset AddRunReportParameter("P_GESTIONNAIRE","#Session.user.NOM# #Session.user.PRENOM#")>
		<cfset AddRunReportParameter("P_MAIL","#Session.user.EMAIL#")>
		<cfset runReport()>
	</cffunction>
	
</cfcomponent>