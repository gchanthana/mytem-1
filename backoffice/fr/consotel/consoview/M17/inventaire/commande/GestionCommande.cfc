<!--- =========================================================================
Classe: GestionCommande
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GestionCommande" hint="" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GestionCommande" hint="Remplace le constructeur de GestionCommande.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<!---
	 	Enregistre la commande dans la base.Si l'opération a réussie on enregistre ensuite les arcticles de la commande.
		On utilisera le CFC GestionPanier pour sauvegarder les articles de la commande.
		La fonction retourne l'id de la commande si succés
		-1 si la commande n'a pu étre enregistrée
		-2 si le panier n'a pu étre enregistré
		-3 pour les autres erreurs. 
	 --->
	<cffunction name="enregistrerCommande" access="public" returntype="numeric" output="false" hint="">		
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idCompte" 			required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idSousCompte" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idOperateur" 			required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="idContactRevendeur" 	required="true" type="numeric" 	default="0" 	displayname="numeric segment" hint="" />
		<cfargument name="idTransporteur" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="numeroTracking" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="libelleCommande"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="dateOperation"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="dateLivraison"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="commentaires" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="boolDevis" 			required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idTypeCommande" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idInventaireOpe" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idGroupeRepere" 		required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="numeroCommande" 		required="true" type="string" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="articles" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		
		
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.ENREGISTREROPERATION_V2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#Session.USER.CLIENTACCESSID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" 						value="#idCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte" 					value="#idSousCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 						value="#idOperateur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 				value="#idContactRevendeur#" null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" 					value="#idTransporteur#" null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 				value="#idSiteDeLivraison#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_effet_prevue" 				value="#dateLivraison#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
			<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" null="false" variable="p_montant" 			value="#montant#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 					value="#idGroupeRepere#">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" null="false" variable="p_article" 				value="#tostring(articles)#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" >
		</cfstoredproc>
				
		<cfreturn p_idoperation>
	</cffunction>
	
	<cffunction name="enregistrerCommandeActePour" access="public" returntype="numeric" output="false" hint="">		
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idCompte" 			required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idSousCompte" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idOperateur" 			required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="idContactRevendeur" 	required="true" type="numeric" 	default="0" 	displayname="numeric segment" hint="" />
		<cfargument name="idTransporteur" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="numeroTracking" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="libelleCommande"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="dateOperation"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="dateLivraison"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="commentaires" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="boolDevis" 			required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idTypeCommande" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idInventaireOpe" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idGroupeRepere" 		required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="numeroCommande" 		required="true" type="string" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idActePour" 			required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="articles" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		
		
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.ENREGISTREROPERATION_V2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idActePour#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" 						value="#idCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte" 					value="#idSousCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 						value="#idOperateur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 				value="#idContactRevendeur#" null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" 					value="#idTransporteur#" null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 				value="#idSiteDeLivraison#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_effet_prevue" 				value="#dateLivraison#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
			<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" null="false" variable="p_montant" 			value="#montant#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 					value="#idGroupeRepere#">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" null="false" variable="p_article" 				value="#tostring(articles)#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" >
		</cfstoredproc>
				
		<cfreturn p_idoperation>
	</cffunction>
	
	<cffunction name="enregistrercmd" access="public" returntype="numeric" output="false" hint="">		
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idCompte" 			required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idSousCompte" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idOperateur" 			required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="libelleCommande"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="dateOperation"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="dateLivraison"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="commentaires" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="boolDevis" 			required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idTypeCommande" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idInventaireOpe" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idGroupeRepere" 		required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="numeroCommande" 		required="true" type="string" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="articles" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="idContactRevendeur" 	required="true" type="numeric" 	default="NULL" 	displayname="numeric segment" hint="" />
		<cfargument name="idTransporteur" 		required="true" type="numeric" 	default="NULL" 	displayname="numeric segment" hint="" />
		<cfargument name="numeroTracking" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />

		
		
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.ENREGISTREROPERATION_V2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#Session.USER.CLIENTACCESSID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" 						value="#idCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte" 					value="#idSousCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 						value="#idOperateur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 				value="#idContactRevendeur#" null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" 					value="#idTransporteur#" null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 				value="#idSiteDeLivraison#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_effet_prevue" 				value="#dateLivraison#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
			<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" null="false" variable="p_montant" 			value="#montant#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 					value="#idGroupeRepere#">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" null="false" variable="p_article" 				value="#tostring(articles)#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="enregistrerCommandeOLD" access="public" returntype="numeric" output="false" hint="">		
		<cfargument name="commande" required="false" type="struct" default="" displayname="struct commande"/>
		<cfargument name="articles" required="true" type="string" displayname="liste des articles"/>
		<!---   p_idracine => :p_idracine,
                                               p_idgestionnaire => :p_idgestionnaire,
                                               p_idpoolgestionnaire => :p_idpoolgestionnaire,
                                               p_idcompte => :p_idcompte,
                                               p_idsous_compte => :p_idsous_compte,
                                               p_idoperateur => :p_idoperateur,
                                               p_idrevendeur => :p_idrevendeur,
                                               p_idcontact_revendeur => :p_idcontact_revendeur,
                                               p_idtransporteur => :p_idtransporteur,
                                               p_idsite_livraison => :p_idsite_livraison,
                                               p_numero_tracking => :p_numero_tracking,
                                               p_libelle_operation => :p_libelle_operation,
                                               p_ref_client => :p_ref_client,
                                               p_ref_revendeur => :p_ref_revendeur,
                                               p_date_operation => :p_date_operation,
                                               p_date_effet_prevue => :p_date_effet_prevue,
                                               p_commentaires => :p_commentaires,
                                               p_bool_devis => :p_bool_devis,
                                               p_montant => :p_montant,
                                               p_segment_fixe => :p_segment_fixe,
                                               p_segment_mobile => :p_segment_mobile,
                                               p_idinv_type_ope => :p_idinv_type_ope,
                                               p_idgroupe_repere => :p_idgroupe_repere,
                                               p_articles => :p_articles,
                                               p_retour => :p_retour --->
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.ENREGISTREROPERATION_V2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" value="#Session.USER.CLIENTACCESSID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" value="#commande.IDPOOL_GESTIONNAIRE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" value="#commande.IDCOMPTE_FACTURATION#" null="#iif((commande.IDCOMPTE_FACTURATION eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte" value="#commande.IDSOUS_COMPTE#" null="#iif((commande.IDSOUS_COMPTE eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" value="#commande.IDOPERATEUR#" null="#iif((commande.IDOPERATEUR eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" value="#commande.IDREVENDEUR#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" value="#commande.IDCONTACT#" null="#iif((commande.IDCONTACT eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" value="#commande.IDTRANSPORTEUR#" null="#iif((commande.IDTRANSPORTEUR eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" value="#commande.IDSITELIVRAISON#" null="#iif((commande.IDSITELIVRAISON eq 0), de("yes"), de("no"))#">	
					
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" value="#commande.NUMERO_TRACKING#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" value="#commande.LIBELLE_COMMANDE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" value="#commande.REF_CLIENT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" value="#commande.REF_REVENDEUR#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" value="#lsdateFormat(commande.DATE_COMMANDE,'YYYY/MM/DD')#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_effet_prevue" value="#lsdateFormat(commande.LIVRAISON_PREVUE_LE,'YYYY/MM/DD')#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" value="#commande.COMMENTAIRES#">
			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" value="#commande.BOOL_DEVIS#">
			<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" null="false" variable="p_montant" value="#commande.MONTANT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" value="#commande.SEGMENT_FIXE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" value="#commande.SEGMENT_MOBILE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" value="#commande.IDTYPE_COMMANDE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" value="#commande.IDGROUPE_REPERE#" null="#iif((commande.IDGROUPE_REPERE eq 0), de("yes"), de("no"))#">
			
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" null="false" variable="p_article" value="#tostring(articles)#">
			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" value="#commande.NUMERO_COMMANDE#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" >
		</cfstoredproc>
				
		<cfreturn p_idoperation>
	</cffunction>
	
	<!--- 
		- fournit la liste des commandes d'un client (pour une racine) ,vérifiant les conditions suivantes	
		- param.dateDebut < commande.DATE < param.dateFin;	
		- NUMERO_COMMANDE | LIBELLE_COMMANDE | REFERENCE_REVENDEUR | REFERENCE_CLIENT | contient la clef de recherche.	
		- si le segment est fixe/data (1) alors on va chercher dans les commande du cycle de vie.
		- si le segment est mobile (2) alors  on va chercher dans la commande mobile.
	 --->
	<cffunction name="fournirInventaireOperation" access="public" returntype="query" output="false">
		
		<cfargument name="dateDebut" required="false" type="date" default="" displayname="date dateDebut" hint="" />
		<cfargument name="dateFin" required="false" type="date" default="" displayname="date dateFin" hint="" />
		<cfargument name="clef" required="false" type="string" default="" displayname="string clef" hint="" />
		<cfargument name="segment" required="false" type="numeric" default="" displayname="numeric segment" hint="" />
		
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.FOURNIRINVENTAIREOPERATION" blockfactor="100" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#Session.USER.CLIENTACCESSID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#segment#">		
			 
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_debut" value="#lsdateFormat(dateDebut,'YYYY/MM/DD')#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_fin" value="#lsdateFormat(dateFin,'YYYY/MM/DD')#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_clef" value="#clef#">			
			<cfprocresult name="p_retour">			
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>
	
<!--- DEBUT NOUVELLES PROCEDURES --->
	
	<!--- 
		- enregiste un modèle de commande
	 --->
	 <cffunction name="saveModeleCommande" access="public" returntype="numeric" output="false">
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idOperateur" 			required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="idContactRevendeur" 	required="true" type="numeric" 	default="NULL" 	displayname="numeric segment" hint="" />
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="libelleOperation"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="commentaires" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idInventaireOpe" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idGroupeRepere" 		required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="articles" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		
			<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.savemodelecommande" blockfactor="100" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine" 				value="#IDRACINE#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idgestionnaire" 		value="#Session.USER.CLIENTACCESSID#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur" 			value="#idOperateur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idrevendeur" 			value="#idRevendeur#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idcontact_revendeur" 	value="#idContactRevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idsite_livraison" 		value="#idSiteDeLivraison#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" 	variable="p_libelle_operation" 		value="#libelleOperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" 	variable="p_ref_client" 			value="#refClient#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" 	variable="p_ref_revendeur" 			value="#refRevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" 	variable="p_commentaires" 			value="#commentaires#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_DECIMAL" 	variable="p_montant" 				value="#montant#">	
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_segment_fixe" 			value="#segmentFixe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_segment_mobile" 		value="#segmentMobile#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idinv_type_ope" 		value="#idInventaireOpe#">	
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idgroupe_repere" 		value="#idGroupeRepere#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 		variable="p_articles" 				value="#tostring(articles)#">		
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperation">		
			</cfstoredproc>
				
		<cfreturn p_idoperation>
	</cffunction>
	
	<!--- 
		- obtient les libellés des modèles enregistrés
	 --->
	<cffunction name="getListModeleCommande" access="public" returntype="query" output="false">
		<cfargument name="idPool" required="true" type="numeric" default=""/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.getlistemodelecommande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" value="#idPool#">			
			<cfprocresult name="p_retour">			
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		- obtient les modèles enregistrés
	 --->
	<cffunction name="getModeleCommande" access="public" returntype="query" output="false">
		<cfargument name="idModele" required="true" type="numeric" default=""/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.getmodelecommande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idmodele" value="#idModele#">			
			<cfprocresult name="p_retour">			
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>

	<!--- 
		- supprime un modèle enregisstré
	 --->
	<cffunction name="deleteModeleCommande" access="public" returntype="numeric" output="false">
		<cfargument name="idModele" required="true" type="numeric" default=""/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.deletemodelecommande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idmodele" value="#idModele#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour" >
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		- obtient l'historique de gestion SAV/COMMANDE/GESTION
	 --->
	<cffunction name="getHistoriqueAction" access="public" returntype="query" output="false">
		<cfargument name="idPool" 		required="true" type="numeric" 	 displayname="id pool" 		hint="" />
		<cfargument name="dateDebut" 	required="true" type="string" 	 displayname="date dateDebut" hint="" />
		<cfargument name="dateFin" 		required="true" type="string" 	 displayname="date dateFin" 	hint="" />
		<cfargument name="clef" 		required="false" type="string" 	default="" displayname="string clef" 	hint="" />
		<cfset OFFREDSN = "ROCOFFRE">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.gethistoaction" blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" 	value="#idPool#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" 	variable="p_date_debut" value="#dateDebut#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" 	variable="p_date_fin" 	value="#dateFin#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" 	variable="p_cle" 		value="#clef#">		
			<cfprocresult name="p_retour">			
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		- info sur l'equipements
	 --->
	<cffunction name="getProductFeature" access="public" returntype="array" output="false">
		<cfargument name="idEquipement" required="true" type="numeric" 	default=""/>
		<cfargument name="idlang" 		required="true" type="numeric" 	default="3"/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.get_product_feature_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idequipement" 	value="#idEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_langid" 		value="#idlang#">
			<cfprocresult name="p_retour" resultset="1">
			<cfprocresult name="p_retour_photo" resultset="2">
		</cfstoredproc>
		
		<cfset result = arrayNew(1)>
		<cfset arrayAppend(result,p_retour)>
		<cfset arrayAppend(result,p_retour_photo)>
				
		<cfreturn result>
	</cffunction>
	
	<!--- 
		- recupère les accessoires suivant le modèle sélectionné
	 --->
	
	<!--- 
		- obtient les accessoires client compatibles avec l'appareil sélectionné
	 --->
	<cffunction name="getProductRelated" access="public" returntype="query" output="false">
		<cfargument name="idEquipement" 	required="true" type="numeric" 	default=""/>
		<cfargument name="sameRevendeur" 		required="true" type="numeric" 	default=""/>
		<cfargument name="chaine" 			required="true" type="string" 	default="null"/>
		<cfargument name="idlang" 			required="true" type="numeric" 	default="3"/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.get_product_related_v2"><!--- NE PAS OUBLIER LE NOM DU PACKAGE --->
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idequipement" 	value="#idEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_chaine" 		value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_langid" 		value="#idlang#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_same_rev" 		value="1">	
			<cfprocresult name="p_retour">			
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>

<!--- FIN NOUVELLES PROCEDURES --->
	
	<!--- 
		Met é jour les informations d'une commande.		
		retourne 1 si ok sinon on retourne -1
	---> 
	<cffunction name="majCommande" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="commande" required="true" type="Struct" default="" displayname="struct commande" hint=""/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.MAJCOMMANDE">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#commande.IDCOMMANDE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#Session.USER.CLIENTACCESSID#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" value="#commande.IDTRANSPORTEUR#" null="#iif((commande.IDTRANSPORTEUR eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" value="#commande.IDSITELIVRAISON#" null="#iif((commande.IDSITELIVRAISON eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" value="#commande.IDCONTACT#" null="#iif((commande.IDCONTACT eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_operation" value="#commande.NUMERO_COMMANDE#">		
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" value="#commande.NUMERO_TRACKING#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" value="#commande.LIBELLE_COMMANDE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" value="#commande.REF_CLIENT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" value="#commande.REF_OPERATEUR#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_effet_prevue" value="#lsdateFormat(commande.LIVRAISON_PREVUE_LE,'YYYY/MM/DD')#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" value="#commande.COMMENTAIRES#">
			<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" null="false" variable="p_montant" value="#commande.MONTANT#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		</cfstoredproc>
		<cfreturn p_retour> 
	</cffunction>
	
	<!--- 
		- enregiste un modèle de commande
	 --->
	
	
<!--- 	<cffunction name="majInfosLivraisonCommande" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="commande" required="true" type="Struct" hint=""/>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.majInfosLivraisonCommande">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" 		value="#commande.IDCOMMANDE#">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#Session.USER.CLIENTACCESSID#">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 	value="#commande.IDSITELIVRAISON#">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" 		value="#commande.IDTRANSPORTEUR#">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_VARCHAR" variable="p_numero_tracking" 	value="#commande.NUMERO_TRACKING#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction> --->
	
	<cffunction name="majInfosLivraisonCommande" access="public" returntype="numeric" output="false">
		<cfargument name="IDCOMMANDE" 			required="true" type="Numeric"/>
		<cfargument name="IDSITELIVRAISON" 		required="true" type="Numeric"/>
		<cfargument name="IDTRANSPORTEUR" 		required="true" type="Numeric"/>
		<cfargument name="NUMERO_TRACKING" 		required="true" type="String"/>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.majInfosLivraisonCommande">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" 		value="#IDCOMMANDE#">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 		value="#Session.USER.CLIENTACCESSID#">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 	value="#IDSITELIVRAISON#">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" 		value="#IDTRANSPORTEUR#">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_VARCHAR" variable="p_numero_tracking" 	value="#NUMERO_TRACKING#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<!---
		Supprime une commande.		
		retourne 1 si ok sinon on retourne -1
	 --->
	<cffunction name="supprimerCommande" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="commande" required="true" type="Commande" default="" displayname="struct commande" hint="" />
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.SUPPRIMEROPERATION">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#commande.IDCOMMANDE#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<!---
		Supprime une commande.		
		retourne 1 si ok sinon on retourne -1
	 --->
	<cffunction name="eraseCommande" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="IDCOMMANDE" required="true" type="Numeric"/>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.SUPPRIMEROPERATION">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#IDCOMMANDE#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction> 
	
	<!--- 
		Fournit le détail d'une commande
		Pour la liste des articles de la commande on utlisera le cfc GestionPanier
		retour :
		array[1] = query 'detail commande'
		array[2] = query 'panier commande'
	 --->
	<cffunction name="fournirDetailOperation" access="public" returntype="query" output="false" hint="" >
		<cfargument name="idCommande" required="true" type="numeric" displayname="struct commande"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.FOURNIRDETAILOPERATION" blockfactor="1" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#Session.USER.CLIENTACCESSID#">						
			<cfprocresult name="p_retour">			
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="genererNumeroDeCommande" access="public" returntype="string" output="false" hint="" >
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.GETCOMMANDENUMBER" blockfactor="1" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#IDRACINE#">									
			<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR" variable="p_retour">		
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	
	
	<cffunction name="extractInventaireOperation" access="public" returntype="void" output="true">		
		<cfargument name="dateDebut" 	required="true" type="date" 	default="" displayname="date dateDebut" hint="" />
		<cfargument name="dateFin" 		required="true" type="date" 	default="" displayname="date dateFin" 	hint="" />
		<cfargument name="clef" 		required="true" type="string" 	default="" displayname="string clef" 	hint="" />
		<cfargument name="segment" 		required="true" type="numeric" 	default="" displayname="numeric segment" hint="" />	 
		
			<cfsetting enablecfoutputonly="true"/>
			<cfset qInventaire=fournirInventaireOperation(dateDebut,dateFin,clef,segment)>				
			<cfset NewLine = Chr(13) & Chr(10)>
			<cfheader name="Content-Disposition"  value="inline;filename=InventaireCommandes_#lsDateFormat(now(),'DD-MM-YYYY')#.csv">
			<cfcontent type="text/csv"><cfoutput>Ref_client;Type;Revendeur;Libelle;Numero;Action;Etat;Montant;Nom_site_livraison;Date_creation_demande;Date_envoi_commande;Date_expedition;Date_livraison_prevue;Date_livraison;#NewLine#</cfoutput>			
			<cfoutput query="qInventaire">#TRIM(REF_CLIENT)#;#TRIM(TYPE_OPERATION)#;#TRIM(LIBELLE_REVENDEUR)#;#TRIM(LIBELLE)#;#TRIM(NUMERO_OPERATION)#;#TRIM(LIBELLE_ACTION)#;#TRIM(LIBELLE_ETAT)#;#TRIM(MONTANT)#;#TRIM(NOM_SITE)#;#TRIM(lsdateFormat(DATE_CREATE,"DD/MM/YYYY"))#;#TRIM(lsdateFormat(DATE_ENVOI,"DD/MM/YYYY"))#;#TRIM(lsdateFormat(EXPEDIE_LE,"DD/MM/YYYY"))#;#TRIM(lsdateFormat(LIVRAISON_PREVUE_LE,"DD/MM/YYYY"))#;#TRIM(lsdateFormat(LIVRE_LE,"DD/MM/YYYY"))#;#NewLine#</cfoutput>
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
	
	<cffunction name="envoyer" access="public" returntype="numeric">
		<cfargument name="mail" type="fr.consotel.consoview.util.Mail" required="true">
		<cfargument name="idCommande" type="numeric" required="true">
		
		<cfset p_retour = -1>
		
		<cfset str1 = 'SIZE="10"'>
		<cfset str2 = 'SIZE="2"'>		
		<cfset message1 = #Replace(mail.getMessage(),str1,str2,"all")#>

		<cfset str1 = "&apos;">
		<cfset str2 = "'">
		<cfset message = #Replace(message1,str1,str2,"all")#>

		<cfset listeMailEmp = "">
		<cfset cc = mail.getCc()>
		
		<cfif mail.getCopiePourExpediteur() eq "YES">
			<cfif len(mail.getCc()) eq 0>
				<cfset cc = mail.getExpediteur()>
			<cfelse>
				<cfset cc = mail.getCc() & "," & mail.getExpediteur()>
			</cfif>
		</cfif>
		
		
		<cfobject name="sr" type="component" component="fr.consotel.consoview.util.publicreportservice.ScheduleRequest">
		<cfset sr.setReportRequest("/consoview/gestion/flotte/BDCmobile/BDCmobile.xdo","BDC1","pdf")><!--- BDCmobile/BDCmobile.xdo  A REMETTRE!! --->
		<cfset sr.AddParameter("P_IDCOMMANDE","#idCommande#")>
		<cfset sr.AddParameter("P_IDGESTIONNAIRE","#Session.user.CLIENTACCESSID#")>
		<cfset sr.AddParameter("P_RACINE","#Session.perimetre.RAISON_SOCIALE#")>
		<cfset sr.AddParameter("P_GESTIONNAIRE","#Session.user.NOM# #Session.user.PRENOM#")>
		<cfset sr.AddParameter("P_MAIL","#Session.user.EMAIL#")>
		
		<cfset sr.setScheduleRequest("consoview","public","#createUUID()#")>
		<cfset sr.setNotificationScheduleRequest("IT@consotel.fr",true,false,false)>
		<cfset sr.setEmailDeliveryRequest("#mail.getDestinataire()#","#cc#","#mail.getBcc()#","#mail.getDestinataire()#","#mail.getExpediteur()#",
		                                                     "#message#","#mail.getModule()# : #mail.getSujet()#")>
		                                                     
		<cfset sr.setBurstScheduleRequest(false)>
		<cfset sr.scheduleReport()>
	
			
			<!--- envoi du message 
			 <cftry>
				<cfmail from=""
						to="#mail.getDestinataire()#"
						subject="#mail.getModule()# : #mail.getSujet()#"
						type="#mail.getType()#"
						charset="#mail.getCharset()#"
						bcc="#mail.getBcc()#"
						cc="#cc#"
						replyto="#mail.getRepondreA()#"
						server="mail.consotel.fr:26"
						failto="samuel.divioka@consotel.fr">
						<html lang='fr'>
						<head>
						<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
						<body>									
							#message#
						</body>
						</html>
				</cfmail>
				<cfset p_retour = 1>
				<cfcatch type="any">
					<cfset p_retour = -1>
				</cfcatch>
			</cftry>	--->
			<!--- fin envoi du message --->
			
			<!--- Sauvegarde du message 
			<cftry>
				<cfset idMessage = createUUID()>
				<cfset resFolder = createFolder( Session.perimetre.RAISON_SOCIALE & "/" & Session.user.NOM & "_" & Session.user.PRENOM & '/' &LSDATEFORMAT(now(),"dd_mm_yyyy") )>
				<cfset FILE_NAME = "/listemailsclient/" 
									& Session.perimetre.RAISON_SOCIALE & "/" & Session.user.NOM & "_" & Session.user.PRENOM & "/"									
									& LSDATEFORMAT(now(),"dd_mm_yyyy") 
									& "/" 
									& idMessage 
									& "_" 
									& resFolder
									& ".pdf">
								
				<cfcontent type="FlashPaper/pdf"> 
					<cfheader name="Content-Disposition" value="inline;filename=#FILE_NAME#">
					<cfdocument format="PDF" backgroundvisible="yes" fontembed = "yes" unit="CM" margintop="5" orientation="portrait" filename="#FILE_NAME#">							
					 
						<html lang='fr'>
						<head>
						<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
						<body>									
							#message#
						</body>
						</html>
					</cfdocument>	
					<cfset p_retour = 2>
				<cfcatch type="any">
					<cfset p_retour = -11>
				</cfcatch>
			</cftry>
		 Fin sauvegarde du message --->
			<cfreturn p_retour>
	 </cffunction>
	 
	
	<cffunction name="createFolder" access="private" returntype="numeric" output="true">
		<cfargument name="folderName" type="string" required="true">
		<cfset racine = "listemailsclient">
		<cfset qDir = 1>
		<cfdirectory action="List" directory="/#racine#/#folderName#" name="qDir">
		<cftry>
			<cfdirectory action="create" directory="/#racine#/#folderName#">	
			<cfcatch type="any">
				<cfoutput>Le dossier existe déjà</cfoutput>
			</cfcatch>
		</cftry>
		<cfreturn qDir.recordcount>
	</cffunction>
	
</cfcomponent>
