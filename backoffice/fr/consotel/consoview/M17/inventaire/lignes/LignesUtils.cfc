<!--- =========================================================================
Classe: LignesUtils
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="LignesUtils" hint="" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="LignesUtils" hint="Remplace le constructeur de LignesUtils.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->


	<!--- 
	Vérifie l'nicité du numéro d'une ligne retourne -1 si le numéro n'est pas unique 
	 --->
	<cffunction name="verifierUniciteNumeroLigne" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="sousTete" required="false" type="string" default="" displayname="string sousTete" hint="Initial value for the sousTeteproperty."/>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.sous_tete_exists_v1">						
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_sous_tete" value="#sousTete#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     
		</cfstoredproc>
		<cfreturn result/>
	</cffunction>
	 
		
	<!--- 
		Génére un numéro de ligne unique.
	 --->
	<cffunction name="genererNumeroLigneUnique" access="public" returntype="string" output="false" hint="Génére un numéro de ligne unique." >
 		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.G_getMobile_number">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="out" variable="result"/>						     
		</cfstoredproc> 
		<cfreturn result/>
		<!--- 	<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.G_getMobile_number">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="out" variable="result"/>						     
		</cfstoredproc> 
		<cfreturn "result"/> --->
		
	</cffunction>
	
	
	<!---------------
	 	Fiche LIGNE 
	----------------->
	<cffunction name="getInfosFicheSousTete" access="remote" returntype="query">
		
		<cfargument name="idsous_tete" required="true" type="numeric"/>
		<cfargument name="idsim" required="false" type="numeric" default="0"/>
		<cfargument name="idemploye" required="true" type="numeric"/>
		
		<cfset idracine = SESSION.PERIMETRE.ID_GROUPE>		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.getInfoSousTete_v2">
			
			<cfif idsous_tete eq 0>
				<cfprocparam cfsqltype="CF_SQL_inTEGER"  type="in" variable="p_idsous_tete" value="#idsous_tete#" null="true"/>
				<cfprocparam cfsqltype="CF_SQL_inTEGER"  type="in" variable="p_idracine" value="#idracine#" null="false"/>
				<cfprocparam cfsqltype="CF_SQL_inTEGER"  type="in" variable="p_idsim" value="#idsim#" null="false"/>
				<cfprocparam cfsqltype="CF_SQL_inTEGER"  type="in" variable="p_idemploye" value="#idemploye#" null="false"/>
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_inTEGER"  type="in" variable="p_idsous_tete" value="#idsous_tete#" null="false"/>
				<cfprocparam cfsqltype="CF_SQL_inTEGER"  type="in" variable="p_idracine" value="#idracine#"/>
				<cfprocparam cfsqltype="CF_SQL_inTEGER"  type="in" variable="p_idsim" value="#idsim#" null="true"/>
				<cfprocparam cfsqltype="CF_SQL_inTEGER"  type="in" variable="p_idemploye" value="#idemploye#" null="false"/>
			</cfif>
									
			<cfprocresult name="infosFicheLigne">									     
		</cfstoredproc>
		<cfreturn infosFicheLigne/>
	</cffunction>
	
	<!--- listes des sim d'une ligne --->
	<cffunction name="getListeSimSousTete" access="remote" returntype="query">
		
		<cfargument name="idsous_tete" required="true" type="numeric"/>
		
		<cfset idracine = SESSION.PERIMETRE.ID_GROUPE>		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.getInfoSousTete">
			<cfprocparam cfsqltype="CF_SQL_inTEGER"  type="in" variable="p_idsous_tete" value="#idsous_tete#" null="false"/>
			<cfprocparam cfsqltype="CF_SQL_inTEGER"  type="in" variable="p_idracine" value="#idracine#"/>
			<cfprocresult name="qGetListeSimSousTete">									     
		</cfstoredproc>
		<cfreturn qGetListeSimSousTete/>
	</cffunction>
	
	<cffunction name="upDateInfosFicheSousTete" access="remote" returntype="numeric" output="false">
						
		<!--- === NDI ==== --->
		<cfargument name="idSous_tete" required="true" type="numeric"/>
		<cfargument name="new_sous_tete" required="true" type="string"/>
		
		<!--- === Contrat abo ==== --->
		<cfargument name="idcontrat" required="true" type="numeric"/>
		<cfargument name="date_resiliation" required="true" type="string"/>
		<cfargument name="date_renouvellement" required="true" type="string"/>
		<cfargument name="date_ouverture" required="true" type="string"/>
		<cfargument name="duree_contrat" required="true" type="numeric" default="0"/>
		<cfargument name="numero_contrat" required="true" type="string"/>
		
		<!--- === sim ==== --->
		<cfargument name="idSim" required="true" type="numeric"/>
		<cfargument name="PIN1" required="true" type="string"/>
		<cfargument name="PIN2" required="true" type="string"/>
		<cfargument name="PUK1" required="true" type="string"/>
		<cfargument name="PUK2" required="true" type="string"/>
		<cfargument name="numsim" required="true" type="string"/>
		<cfargument name="duree_elligibilite" required="true" type="numeric" default="0"/>
		
		<!--- === commentaires ==== --->
		<cfargument name="commentaires" required="true" type="string"/>
		<cfargument name="v1" required="true" type="string"/>
		<cfargument name="v2" required="true" type="string"/>
		<cfargument name="v3" required="true" type="string"/>
		<cfargument name="v4" required="true" type="string"/>
		
		<cfargument name="bool_titulaire" required="true" type="numeric"/>
		<cfargument name="bool_payeur" required="true" type="numeric"/>
		
		<!--- PROCEDURE updateInfoSousTete_v2(	p_contrat IN INTEGER,
												p_date_resiliation IN VARCHAR2,
												p_date_renouvellement IN varchar2,
												p_idsim IN INTEGER,
												p_pin1 IN VARCHAR2,
												p_pin2 IN VARCHAR2,
												p_puk1 IN VARCHAR2,
												p_puk2 IN VARCHAR2,
												p_num_sim IN VARCHAR2,
												p_idsous_tete IN INTEGER,
												p_no_ligne IN VARCHAR2,
												p_date_ouverture IN VARCHAR2,
												p_duree_contrat IN INTEGER,
												p_retour OUT INTEGER); --->
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.updateInfoSousTete_v3">
			<cfif idcontrat gt 0>
				<cfprocparam cfsqltype="CF_SQL_NUMERIC"  type="in" value="#idcontrat#" variable="p_idcontrat"/>				
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_NUMERIC"  type="in" value="0" variable="p_idcontrat"/>
			</cfif>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#date_resiliation#" variable="p_date_resiliation"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#date_renouvellement#" variable="p_date_renouvellement"/>
			<cfif idSim gt 0>
				<cfprocparam cfsqltype="CF_SQL_NUMERIC"  type="in" value="#idSim#" variable="p_idsim"/>
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_NUMERIC"  type="in" value="#idSim#" variable="p_idsim" null="true"/>
			</cfif>
			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#PIN1#" variable="p_PIN1"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#PIN2#" variable="p_PIN2"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#PUK1#" variable="p_PUK1"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#PUK2#" variable="p_PUK2"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#numsim#" variable="p_numsim"/>
			<cfif idsous_tete eq 0>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#idsous_tete#" variable="p_idsous_tete" null="true"/>
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#idsous_tete#" variable="p_idsous_tete"/>
			</cfif>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#new_sous_tete#" variable="p_no_ligne"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#date_ouverture#" variable="p_date_ouverture"/>
			
			<cfif duree_contrat gt 0>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#duree_contrat#" variable="p_duree_contrat"/>
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#duree_contrat#" variable="p_duree_contrat" null="true"/>
			</cfif>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#numero_contrat#" variable="p_numero_contrat"/>
			
			<cfif duree_elligibilite gt 0>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#duree_elligibilite#" variable="p_duree_elligibilite" null="false"/>
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#duree_elligibilite#" variable="p_duree_elligibilite" null="true"/>
			</cfif>
			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#commentaires#" variable="p_commentaires"/>
			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#v1#" variable="p_v1"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#v2#" variable="p_v2"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#v3#" variable="p_v3"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" value="#v4#" variable="p_v4"/>
			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#bool_titulaire#" variable="p_bool_titulaire"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#bool_payeur#" variable="p_bool_payeur"/>
			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_result"/>
			
		</cfstoredproc>
		
		<cfreturn p_result/>
	</cffunction>	
	
	
	<!--- Pour la liste des options d’une ligne (fixe ça marche peut-être pour les mobiles) --->
	<cffunction name="getListeInventaireSousTete" access="remote" returntype="query">
    	<cfargument name="sousTeteId" type="numeric" required="true">          
            <!---        PROCEDURE Inventaire_sous_tete ( p_idsous_tete              IN INTEGER,  
                                                           p_retour                  OUT SYS_REFCURSOR);
            --->
        <cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M17.Inventaire_sous_tete">
        	<cfprocparam  variable="p_idsous_tete" value="#sousTeteId#" cfsqltype="CF_SQL_INTEGER" type="in">
            <cfprocresult name="qResult">
     	</cfstoredproc>
     	<cfreturn qResult>
    </cffunction>

	<!--- Liste des produits abo de la ligne --->
	<cffunction name="getLigneProduitAbo" access="remote" returntype="query">
		<cfargument name="sousTeteId" type="numeric" required="true">		
		
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M17.get_ligne_produit_abo">
			<cfprocparam  variable="p_idracine" value="#idRacine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam  variable="p_idsous_tete" value="#sousTeteId#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction> 

	<!--- Pour l’historique d’un produit d’une ligne ---> 
	<cffunction name="getHistoriqueActionRessource" access="public" returntype="query" output="false">                               
        <cfargument name="idressource" type="numeric" required="true">         
        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.GETHISTORIQUE_RECHERCHE">          
             <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idinventaire_produit" value="#idressource#"/>  
             <cfprocresult name="p_result">                               
        </cfstoredproc> 
        <cfreturn p_result/>                           
    </cffunction>     



	<!--- Sortir/entrer une liste de produit de l’inventaire --->
	<cffunction name="entrerSortirRessources" access="public" returntype="numeric" output="false">                                            
            
            <cfargument name="liste_idinventaire_Produits" type="array" required="true">
            <cfargument name="idinv_actions" type="numeric" required="true">
            <cfargument name="date_action" type="string" required="true">               
            <cfargument name="commentaire_action" type="string" required="true">              
             

            <cfset liste = arrayTolist(liste_idinventaire_Produits,',')>
            <cfset userid = session.user.CLIENTACCESSID>
			
            <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.CREATEACTION_HORSOPE">                        
                  <cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_list_idinventaire_produit" value="#liste#"/>
                  <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_actions" value="#idinv_actions#"/>             
                  <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_op_action_prec" null="yes">
                  <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_userid" value="#userid#"/>
                  <cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaire_action" value="#commentaire_action#"/>
                  <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_etat_actuel" value="1"/>                  
                  <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_dans_inventaire" value="#iif(idinv_actions eq 61 , de("0"), de("1"))#"/>
                  <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_qte" value="1"/>
                  <cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_action" value="#date_action#"/>
                  <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_retour"/>            
            </cfstoredproc>
                             
            <cfreturn p_retour/>                           
     </cffunction>     

	<!--- Affecter une ligne a une organisation --->
	<cffunction name="assignLine" returntype="string" access="remote">
		<cfargument name="id_groupe" required="true" type="numeric" />
		<cfargument name="p_list_idsous_tete" required="true" type="string" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.Affecte_liste_lignes_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#p_list_idsous_tete#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="1" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<!--- Retourner la règle orga d'une ligne affectée a une organisation --->
	<cffunction name="getRegleOrgaLigne" returntype="query" access="remote">
		<cfargument name="idOrga" required="true" type="numeric" />
		<cfargument name="idsous_tete" required="true" type="numeric" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.search_line_orga">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOrga#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsous_tete#" null="false">
			<cfprocresult name="qGetRegleOrgaLigne">
		</cfstoredproc>
		
		<cfreturn qGetRegleOrgaLigne>
	</cffunction>

	<!--- Retourner la liste des orga d'une ligne --->
	<cffunction name="GetOrgaLigne" returntype="query" access="remote">
		<cfargument name="idsous_tete" required="true" type="numeric" />
		<cfset idracine = SESSION.PERIMETRE.ID_GROUPE>	
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m17.GetOrgaLigne">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsous_tete#" null="false">
			<cfprocresult name="qGetRegleOrgaLigne">
		</cfstoredproc>
		
		<cfreturn qGetRegleOrgaLigne>
	</cffunction>	

<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>