<cfcomponent name="ficheCommande">
	
	<cffunction name="getCommandesInfo" returntype="query" access="remote" output="true">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.get_Info_commandes">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[2]#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>

</cfcomponent>