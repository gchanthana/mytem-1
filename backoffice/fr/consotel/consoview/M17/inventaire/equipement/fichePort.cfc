<cfcomponent name="fichePort">
	<cffunction name="getFicheInfos" returntype="array" access="remote" output="true">
		<cfargument name="arr" type="array">
		<cfset eqList=getPortFiche(arr)>
		<cfset typePr=getTypeProtocole()>
		<cfset typeCn=getTypeConnecteur()>
		<cfset dataArr = arrayNew(1)>
		<cfset dataArr[1] = eqList>
		<cfset dataArr[2] = typePr>
		<cfset dataArr[3] = typeCn>
		<cfreturn dataArr>
	</cffunction>
	
	<cffunction name="getType" returntype="array" access="remote" output="true">
		<cfset typePr=getTypeProtocole(1)>
		<cfset typeCn=getTypeConnecteur(1)>
		<cfset dataArr = arrayNew(1)>
		<cfset dataArr[1] = typePr>
		<cfset dataArr[2] = typeCn>
		<cfreturn dataArr>
	</cffunction>
	

	<cffunction name="getEqAdressageList" returntype="query" access="remote" output="true">
		<cfargument name="index" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.get_eqAdressage_list">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#index#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getTypeProtocole" returntype="query" access="remote" output="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.get_typeProtocoles">
			<cfprocresult name="dataArr">
		</cfstoredproc>
		<cfreturn dataArr>
	</cffunction>
	
	<cffunction name="getTypeConnecteur" returntype="query" access="remote" output="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.get_typeConnecteurs">
			<cfprocresult name="dataArr">
		</cfstoredproc>
		<cfreturn dataArr>
	</cffunction>
	
	<cffunction name="getPortFiche" returntype="query" access="remote" output="true">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.get_FichePort">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[2]#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getEquipementPort" returntype="query" access="remote" output="true">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.get_EqPort">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[2]#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		

		
		<cfreturn p_result>
	</cffunction>
	
	
	
	<cffunction name="addEqPort" returntype="numeric" access="remote" output="true">
		<cfargument name="data" type="array">		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.add_eqport">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[2]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[3]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[4]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[5]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[6]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[7]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[8]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[9]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[10]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[11]#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="updatePortFiche" returntype="numeric" access="remote" output="true">
		<cfargument name="data" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.maj_FichePort">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[2]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[3]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[4]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[5]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[6]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[7]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[8]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[9]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[10]#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>

		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="deleteEqPort" returntype="numeric" access="remote" output="true">
		<cfargument name="data" type="array">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.Del_EqPort">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[2]#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="addEqAdressage" returntype="numeric" access="remote" output="true">
		<cfargument name="data" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.add_EqAdressage">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[2]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[3]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[4]#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="deleteEqAdressage" returntype="numeric" access="remote" output="true">
		<cfargument name="data" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.del_EqAdressage">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[2]#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="updateAdresse" returntype="numeric" access="remote" output="true">
		<cfargument name="data" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.maj_EqAdressage">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data[1]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[2]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[3]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[4]#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data[5]#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	
	
</cfcomponent>