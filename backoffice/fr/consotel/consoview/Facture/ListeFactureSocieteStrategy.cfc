<!--- =========================================================================
Classe: ListeFactureSocieteStrategy
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="ListeFactureSocieteStrategy" extends="ListeFactureStrategy" hint="This class implements the algorithm using the Strategy interface.">

	<cffunction name="rechercheFactureByNumber" access="public" returntype="Query" output="false" >
		<cfargument name="NUMERO_CLE" required="false" type="string" default="" displayname="string NUMERO_CLE" hint="" />
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="ID du client (ID_PERIMETRE)" />
		<cfquery name="listeFactures" datasource="#session.OffreDSN#">
			select numero_facture, COMPTE_FACTURATION, DATE_EMISSION, DATEDEB, DATEFIN,
					libelle, rc.idref_client, ip.IDINVENTAIRE_PERIODE, '1' as a, '1' as b, '1' as c
			FROM inventaire_periode ip, compte_facturation cf, ref_client rc
			WHERE ip.idcompte_facturation=cf.idcompte_facturation
			AND cf.idref_client=#ID#
			and rc.idref_client=cf.idref_client
			AND lower(ip.NUMERO_FACTURE) like lower('%#NUMERO_CLE#%')
			order by numero_facture asc
		</cfquery>
		<cfloop from="1" to="#listeFactures.recordcount#" index="i">
			<cfset listeFactures.a[i]=LsDateFormat(listeFactures.DATE_EMISSION[i],"dd mmmm yyyy")>
			<cfset listeFactures.b[i]=LsDateFormat(listeFactures.DATEDEB[i],"dd mmmm yy")>
			<cfset listeFactures.c[i]=LsDateFormat(DateAdd("d",-1,listeFactures.DATEFIN[i]),"dd mmmm yy")>
		</cfloop>
		<cfreturn listeFactures>
	</cffunction>
	
	<cffunction name="rechercheFactureByNumberEtOpe" access="remote" returntype="Query" output="false" >
		<cfargument name="arr" required="false" type="array" default="" displayname="string NUMERO_CLE" hint="" />
		<cfquery name="listeFactures" datasource="#session.OffreDSN#">
			select numero_facture, COMPTE_FACTURATION, DATE_EMISSION, ip.DATEDEB, ip.DATEFIN, rc.idref_client, 
				ip.IDINVENTAIRE_PERIODE, '1' as a, '1' as b, '1' as c, o.nom, SUM(montant) AS montant, libelle as libelle_groupe_client,
				cp.by_adresse1,cp.by_adresse2,cp.by_zipcode,cp.by_commune, cp.by_code_site
			from inventaire_periode ip, sous_compte sc, compte_facturation cp, ref_client rc, operateur o, detail_facture_abo dfa
			where lower(ip.numero_facture) like lower('%#arr[1]#%')
			<cfif arr[3] neq 0>
			AND ip.operateurID=#arr[3]#
			</cfif>
			and ip.idcompte_facturation=cp.idcompte_facturation
			and ip.operateurID=o.operateurID
			and cp.idref_client=rc.idref_client
			and cp.idcompte_facturation=sc.idcompte_facturation
			and cp.idref_client='#arr[2]#'
			AND dfa.idref_client=rc.Idref_Client
			AND ip.idinventaire_periode=dfa.idinventaire_periode
			AND trunc(ip.date_emission)<=trunc(to_date('#arr[5]#','dd/mm/yyyy'))
            AND trunc(ip.date_emission)>=trunc(to_date('#arr[4]#','dd/mm/yyyy'))
			GROUP BY numero_facture, COMPTE_FACTURATION, DATE_EMISSION, ip.DATEDEB, ip.DATEFIN, libelle, rc.idref_client, 
				ip.IDINVENTAIRE_PERIODE, o.nom, 
				cp.by_adresse1,cp.by_adresse2,cp.by_zipcode,cp.by_commune, cp.by_code_site
			order by numero_facture asc
		</cfquery>
		<cfloop from="1" to="#listeFactures.recordcount#" index="i">
			<cfset listeFactures.a[i]=LsDateFormat(listeFactures.DATE_EMISSION[i],"dd mmmm yyyy")>
			<cfset listeFactures.b[i]=LsDateFormat(listeFactures.DATEDEB[i],"dd mmmm yy")>
			<cfset listeFactures.c[i]=LsDateFormat(DateAdd("d",-1,listeFactures.DATEFIN[i]),"dd mmmm yy")>
		</cfloop>
		<cfreturn listeFactures>
	</cffunction>
	
	<cffunction name="getListeOperateur" access="remote" returntype="Query" output="false" >
		<cfargument name="arr" required="false" type="array" default="" displayname="string NUMERO_CLE" hint="" />
		<cfquery name="listeFactures" datasource="#session.OffreDSN#">
			select op.nom, op.operateurID
			from inventaire_periode ip, sous_compte sc, operateur op, ref_client rc, compte_facturation cf
			where op.operateurID=ip.operateurID
			and ip.idcompte_facturation=cf.idcompte_facturation
			and cf.idref_client=rc.idref_client
			and cf.idcompte_facturation=sc.idcompte_facturation
			and cf.idref_client=#arr[1]#
			group by op.nom, op.operateurID
			order by lower(nom) asc
		</cfquery>
		<cfquery name="getListe" dbtype="query">
			select nom as label, operateurID as data
			from listeFactures
		</cfquery>
		<cfreturn getListe>
	</cffunction>
</cfcomponent>
