<!--- =========================================================================
Classe: ListeFactureGroupeStrategy
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="ListeFactureGroupeStrategy" hint="This class implements the algorithm using the Strategy interface.">

	<cffunction name="rechercheFactureByNumber" access="public" returntype="Query" output="false" >
		<cfargument name="NUMERO_CLE" required="false" type="string" default="" displayname="string NUMERO_CLE" hint="" />
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="" />
		<cfabort showerror="Erreur: Classe Abstraite">
	</cffunction>
	
	<cffunction name="displayListeFacture" access="public" returntype="void" output="true">
		<cfargument name="listeFactures" required="true" type="Query">
		<cfabort showerror="Erreur: Classe abstraite">
	</cffunction>

</cfcomponent>
