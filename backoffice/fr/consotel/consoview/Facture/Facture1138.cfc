<!--- =========================================================================
Classe: Facture1138
Auteur: Samuel DIVIOKA
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="Facture1138" extends="Facture" hint="nom de la classe Facture<OPERATEURID>">
   	
	
	<!--- Redefinition des fonctions pour l'internationale ajout du code_langue de la facture' --->
	<cffunction name="getFactureTva" access="public" returntype="query" output="true">		
		<cfargument name="IDINVENTAIRE_PERIODE" required="true" type="numeric"/>
		<cfargument name="IDPerimetre" required="true" type="numeric">		
        <cfset q = variables.instance.typeFactureStrategy.getFactureTva(IDINVENTAIRE_PERIODE,IDPerimetre,'es_ES')>				
		<cfreturn q>
	</cffunction>
	
	<cffunction name="getFactDataset" access="private" returntype="Query" output="false" >
		<cfargument name="IDINVENTAIRE_PERIODE" required="false" type="numeric" default="" displayname="numeric IDINVENTAIRE_PERIODE" hint="" />
		<cfargument name="ID" required="false" type="string" hint="ID Site ou ID sous tete pas nécessaire pour complète">
		<cfargument name="IDPerimetre" required="false" type="numeric">		
		<cfreturn variables.instance.typeFactureStrategy.getData(IDINVENTAIRE_PERIODE,ID,IDPerimetre,'es_ES')>
	</cffunction>
	
	<cffunction name="getFactDatasetCat" access="private" returntype="Query" output="true">
		<cfargument name="IDINVENTAIRE_PERIODE" required="false" type="numeric" default="" displayname="numeric IDINVENTAIRE_PERIODE" hint="" />
		<cfargument name="ID" required="false" type="string" hint="ID Site ou ID sous tete pas nécessaire pour complète">
		<cfargument name="IDPerimetre" required="false" type="numeric">
		<cfreturn variables.instance.typeFactureStrategy.getDataCat(IDINVENTAIRE_PERIODE,IDPerimetre,'es_ES')>
	</cffunction>
	
<!--- METHODS --->
	<cffunction name="ConstruireGabarit" access="public" returntype="void" output="true" >
		<cfargument name="dataset" required="true" type="query">
		
		<!--- LIBELLE_GAMME --->
		<cfquery name="qlibelle_game" dbtype="query">
			SELECT DISTINCT GAMME
			FROM dataset			
			ORDER BY GAMME
		</cfquery>
		<p><br></p>
		<cfoutput>
			<center><strong align=CENTER>Anexo a la factura</strong></center>
		</cfoutput>
		<p><br></p>
		<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="white" class="cadre" width="90%">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="white" class="FT">
				<!--- En tete avec logo --->
				<cfoutput>
				<tr>
					<td align="left" valign="top">
						<table cellpadding="0" cellspacing="2" border="0" align="left" bgcolor="white">
						<tr>
							<td>
								<cfset displayer=createObject("component","fr.consotel.consoview.images.LogoDisplayer")>
								<cfset displayer.DisplayLogoByOperateurID(1138,80)>
							</td>
						</tr>
						<tr>
							<td class="normal">N° Cuenta: #dataset.compte_facturation#</td>
						</tr>
						</table>	
					</td>
					<td align="right" valign="top">
						<table cellpadding="0" cellspacing="2" border="0" align="right" bgcolor="white">
						<tr>
							<th colspan="2" class="huge" valign="bottom"><strong>Anexo a la factura</strong></th>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td  class="normal">Fecha factura: </td>
							<td  class="normal">#LsDateFormat(dataset.date_emission,"dd/mm/yyyy")#</td>
						</tr>
						<tr>
							<td class="normal">Número de la factura: </td>
							<td class="normal">#dataset.numero_facture#</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr size="1" noshade></td>
				</tr>
				<tr>
					<td colspan="2" class="very_big"><strong>Justificativo detellado de productos y servicios</strong></td>
				</tr>
				</cfoutput>
				<!--- Boucle sous les sous_compte --->
				<cfoutput query="dataset" group="sous_compte">
				<!--- premiere ligne horizontale --->
				<tr>
					<td colspan="2"><hr size="3" noshade></td>
				</tr>
				<tr>
					<td align="left" valign="top">
						<table cellpadding="0" cellspacing="2" border="0" align="left" bgcolor="white">
							<tr>
								<td class="normal"><strong>N° Cuenta: #sous_compte#</strong></td>
							</tr>
						</table>	
					</td>
					<td align="right" valign="top">
						<table cellpadding="0" cellspacing="2" border="0" align="right" bgcolor="white">
							<tr>
								<td class="normal"><strong>
								#nom_site#<br>
								#adresse1#<br>
								<cfif len(trim(adresse2)) neq 0>
									#adresse2#<br>
								</cfif>
								#zipcode# #commune#</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<cfoutput group="gamme">
					<cfset total=0>
					<cfoutput group="libelle">
						<cfoutput group="sous_tete">
							<cfoutput>
								<cfset total=total+montant>
						</cfoutput>
					</cfoutput>
				</cfoutput>
				<!--- ligne avant les types de charge --->
				<tr>
					<td colspan="2"><hr size="2" noshade></td>
				</tr>
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>								
								<td class="big"><strong>#gamme# para el periodo del #LsDateFormat(CreateDate(Left(datedeb_fact,4), 
									Mid(datedeb_fact,6,2), right(datedeb_fact,2)),
									"dd/mm/yyyy")# al #LsDateFormat(CreateDate(Left(datefin_fact,4), 
									Mid(datefin_fact,6,2), right(datefin_fact,2)),
									"dd/mm/yyyy")#</strong>
								</td>
								<td align="right" class="big">
									<strong>#LsEuroCurrencyFormat(total, 'none')# #getSigneDevise()#</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<tr>
				<td colspan="2"><hr size="1" noshade></td>
			</tr>
			<tr>
				<td colspan="2">
					<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
						<tr>
							<td class="small_header">&nbsp;</td>								
								
								<cfif compareNoCase(qlibelle_game['GAMME'][2],gamme) neq 0>
									<td width="20" class="small_header"><nobr>precio por unidad</br>en #getShortDevise()# sin IVA</nobr></td>
								<cfelse>
									<td width="20" class="small_header"><nobr>duración HH:MM:SS</nobr></td>
								</cfif>			
								
							<td width="50" class="small_header">cantidad</td>
							<td width="50" class="small_header">IVA<br>código</td>
							<td width="50" class="small_header"><nobr>total en #getShortDevise()# sin IVA</nobr></td>
						</tr>
						
						<cfoutput group="libelle">
						<cfset total=0>
						<cfoutput>
							<cfset total=total+montant>
						</cfoutput>
						<tr>
							<td class="normal"><strong>#libelle#</strong></td>
							<td></td>
							<td></td>
							<td></td>
							<td align="right" class="normal"><strong>#LsEuroCurrencyFormat(total, 'none')# #getSigneDevise()#</strong></td>
						</tr>
						<cfoutput group="sous_tete">
						<tr>
						<cfif sous_tete neq sous_compte>
							<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;<strong>N° #sous_tete# #libelle_type_ligne#</strong></td>
							<cfelse>
							<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;<strong>no ventilación por linea</strong></td>
							</cfif>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<cfoutput>
							
							<cfset bool = false>
							<cfloop index = "ListElement" list = "#PRODUIT_RECHERCHE#">
								<cfif #idproduit_catalogue# eq #ListElement#>
									<cfset bool = true>
								</cfif>
							</cfloop>
							
							<cfif bool eq true>
								<tr>
									<td class="normalColored">&nbsp;&nbsp;&nbsp;&nbsp;#Libelle_produit#</td>
									<cfif prix_unit neq ".01">
										<td class="normalColored" align="right">#LsEuroCurrencyFormat(prix_unit,'none')#</td>
									<cfelse>
										<cfset heure=int(duree_appel/60)>
										<cfset minute=int((duree_appel-heure*60))>
										<cfset seconde=(duree_appel*60-heure*3600-minute*60)>
										<td class="normalColored" align="right">
										<cfif heure lt 100>
										#LsNumberFormat(heure, '00')#
										<cfelse>
										#heure#
										</cfif>
									:#LsNumberFormat(minute,'00')#:#LsNumberFormat(seconde,'00')#</td>
									</cfif>
									<cfif nombre_appel eq 0>
										<td class="normalColored" align="right">#qte#</td>
									<cfelse>
										<td class="normalColored" align="right">#nombre_appel#</td>
									</cfif>
									<td class="normalColored" align="center">#code_tva#</td>
									<td class="normalColored" align="right">#LsEuroCurrencyFormat(montant, 'none')# #getSigneDevise()#</td>
								</tr>
							<cfelse>
								<tr>
									<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;#Libelle_produit#</td>
									<cfif compareNoCase(qlibelle_game['GAMME'][2],gamme) neq 0>
										<td class="normal" align="right">#LsEuroCurrencyFormat(prix_unit,'none')#</td>
									</cfif> 
									<cfif nombre_appel eq 0>
										<td class="normal" align="right">#qte#</td>
									<cfelse>
										<td class="normal" align="right">#nombre_appel#</td>
									</cfif>
									<td class="normal" align="center">#code_tva#</td>
									<td class="normal" align="right">#LsEuroCurrencyFormat(montant, 'none')# #getSigneDevise()#</td>
								</tr>
							</cfif>
						</cfoutput>
					</cfoutput>
				</cfoutput>
			</table>
		</td>
	</tr>
	</cfoutput>
	</cfoutput>
</table>
</td>
</tr>
</table>
</cffunction>

	<cffunction name="ConstruireGabaritCat" access="public" returntype="void" output="true">
		<cfargument name="dataset" required="true" type="query">
		<cfargument name="factureParams" required="true" type="struct">
		<cfset factureInfos = getFactureInfos(factureParams.ID_FACTURE)>
		<cfset factureTva = getFactureTva(factureParams.ID_FACTURE,idx_perimetre)>		
		<p><br></p>
		<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="white" class="cadre" width="900">
			<tr height="150">
				<td>
					<table cellpadding="4" cellspacing="4" border="0" width="100%" bgcolor="white" class="FT">
						<!--- Ligne 1 : Logo + Numero et date de facture --->
						<tr>
							<td align="left" valign="top" width="200">
								<table cellpadding="0" cellspacing="2" border="0" align="left" bgcolor="white">
									<tr>
										<td>
											<cfset displayer=createObject("component","fr.consotel.consoview.images.LogoDisplayer")>
											<cfset displayer.DisplayLogoByOperateurID(1138,80)>
										</td>
									</tr>
								</table>
							</td>
							<td valign="middle" align="center" width="350">
								<strong><font size="5">factura</font></strong>
							</td>
							<td align="center" valign="middle">
								<table cellpadding="0" cellspacing="1" border="0" bgcolor="white">
									<tr>
										<td class="normal" align="right">N° de la factura :<strong>#factureParams.NUMERO_FACTURE#</strong></td>
									</tr>
									<tr>
										<td  class="normal" align="right">Fecha factura :
											<strong>
												#lsDateFormat(parseDateTime(factureParams.DATE_EMISSION,true),"DD/mm/YYYY")#
											</strong>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<!--- Ligne 2 :  --->
						<tr>
							<td class="normal" rowspan="2" valign="top">
								<strong>Para contactarnos :</strong>
								<cfset contactOp = "">
								<cfset contactOpBool = 0>
								<cfif TRIM(factureInfos['adresse1_operateur'][1]) NEQ "">
									<br>#factureInfos['ADRESSE1_OPERATEUR'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['adresse2_operateur'][1]) NEQ "">
									<br>#factureInfos['ADRESSE2_OPERATEUR'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['code_postal_operateur'][1]) NEQ "">
									<br>#factureInfos['code_postal_operateur'][1]# #factureInfos['ville_operateur'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<br>
								<cfif contactOpBool EQ 0>
									<br>Datos no disponibles
								</cfif>
							</td>
							<td class="normal" valign="middle">
								número cuenta : <br><strong>#factureInfos['COMPTE_FACTURATION'][1]#</strong>
							</td>
							<td><!--- coordonnées de FT ---></td>
						</tr>
						<!--- Ligne 3 :  --->
						<tr>
							<td class="normal">
								<strong>
								#factureParams.LIBELLE# (#factureParams.PERIMETRE_LIBELLE#)<br>
								#factureInfos['by_adresse1'][1]#<br>
								#factureInfos['by_adresse2'][1]#<br>
								#factureInfos['by_zipcode'][1]# #factureInfos['by_commune'][1]#<br>
								</strong>
							</td>
							<td class="normal" rowspan="2">
								<strong>
								#factureParams.LIBELLE# (#factureParams.PERIMETRE_LIBELLE#)<br>
								#factureInfos['by_adresse1'][1]#<br>
								#factureInfos['by_adresse2'][1]#<br>
								#factureInfos['by_zipcode'][1]# #factureInfos['by_commune'][1]#<br>
								</strong>
							</td>
						</tr>
						<!--- Ligne 4 :  --->
						<tr>
							<td colspan="2" valign="top">
								<cfif TRIM(factureInfos['TEL_SERVICE_CLIENT'][1]) NEQ "">
									Service Client : #factureInfos['TEL_SERVICE_CLIENT'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['HORAIRE_SERVICE_CLIENT'][1]) NEQ "">
									<br>Horaire : #factureInfos['HORAIRE_SERVICE_CLIENT'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['FAX_SERVICE_CLIENT'][1]) NEQ "">
									<br>Fax : #factureInfos['FAX_SERVICE_CLIENT'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['MAIL_SERVICE_CLIENT'][1]) NEQ "">
									<br><a href="mailto://#factureInfos['MAIL_SERVICE_CLIENT'][1]#">
										#factureInfos['MAIL_SERVICE_CLIENT'][1]#
									</a>
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['URL_OPERATEUR'][1]) NEQ "">
									<br><a href="#factureInfos['URL_OPERATEUR'][1]#" target="_blank">#factureInfos['URL_OPERATEUR'][1]#</a>
									<cfset contactOpBool = 1>
								</cfif>
								<p><br></p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%">
						<tr>
							<td valign="top" width="200px">
								<table cellpadding="0" cellspacing="5" border="0" align="right" bgcolor="FFFFFF" class="FT" width="100%">
									<tr>
										<td class="normal">
											
										</td>
									</tr>
								</table>
							</td>
							<td align="left" valign="top">
								<table cellpadding="0" cellspacing="5" border="0" bgcolor="white" class="FT" width="100%">
									<tr>
										<td align="left" valign="top" colspan="2">
											<table cellpadding="0" cellspacing="5" border="0" bgcolor="white" class="FT" width="100%">
												<cfset totalMontantHt = 0>
												<cfset totalMontantTtc = 0>
												<tr><td colspan="2"><hr size="3" noshade></td></tr>
												<cfloop index="i" from="1" to="#dataset.recordcount#">
													<tr>
														<td class="normal" align="left">
															<strong>#dataset['LIBELLE'][i]#</strong>
														</td>
														<td class="normal" align="right">
															<strong>#LsEuroCurrencyFormat(dataset['MONTANT'][i],'none')# #getSigneDevise()#</strong>
															<cfset totalMontantHt = totalMontantHt + dataset['MONTANT'][i]>
														</td>
													</tr>
												</cfloop>
												<tr>
													<td class="normal" align="left">
														<font size="1"><i>Las prestaciones facturadas a continuacion estan con los detalles en anexo</i></font>
													</td>
													<td class="normal" align="right">
													</td>
												</tr>
												<tr><td colspan="2"><hr size="3" noshade></td></tr>
												<tr>
													<td class="very_big" align="left">
														<strong> Total factura <font size="2">(importe en #getShortDevise()# sin IVA)</font></strong>
													</td>
													<td class="very_big" align="right">
														<strong>#LsEuroCurrencyFormat(totalMontantHt,'none')#</strong>
													</td>
												</tr>
												<tr><td colspan="2"><hr size="3" noshade></td></tr>
												<tr>
													<td class="normal" align="left">
														<font size="2">IVA pagado</font><br>
													</td>
													<td class="normal" align="right">
													</td>
												</tr>
												<cfset totalTTC=0>
												<cfoutput query="factureTva">
													<tr>
													<td class="normal" align="left">
														Importe IVA a #LsEuroCurrencyFormat(Taux,'none')# % de #LsEuroCurrencyFormat(montant,'none')# #getShortDevise()#
													</td>
													<cfset montantTVA = Evaluate((taux * montant) / 100)>
													<td class="normal" align="right">
														#LsEuroCurrencyFormat(montantTVA,'none')#
													</td>
												</tr>
												<cfset totalTTC=totalTTC+montant+montantTVA>
												</cfoutput>
												<tr><td colspan="2"><hr size="3" noshade></td></tr>
												<tr>
													<td class="very_big" align="left">
														<strong>Importe a pagar <font size="2">antes de <cfoutput>#LsDateFormat(factureTva.echeance_paiement,"dd/mm/yyyy")#</cfoutput>(#getShortDevise()# IVA incluído)</font></strong>
													</td>
													<td class="very_big" align="right">
														<strong>#LsEuroCurrencyFormat(totalTTC,'none')#</strong>
													</td>
												</tr>
												<tr><td colspan="2"><hr size="3" noshade></td></tr>
												<tr height="120"><td colspan="2"></td></tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</cffunction>
</cfcomponent>