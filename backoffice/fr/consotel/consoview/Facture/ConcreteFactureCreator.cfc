<!--- =========================================================================
Classe: ConcreteFactureCreator
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->

<cfcomponent name="ConcreteFactureCreator" alias="fr.consotel.consoview.Facture.ConcreteFactureCreator" extends="FactureCreator" output="true">
	<cffunction name="createFacture" access="public" returntype="fr.consotel.consoview.Facture.AbstractFacture" output="false">
		<cfargument name="IDINVENTAIRE_PERIODE" required="false" type="numeric" default=""/>		
		
		<cfquery name="getOperateurID" datasource="#SESSION.OFFREDSN#">
			SELECT * 
			FROM INVENTAIRE_PERIODE INV, OPERATEUR OP 
			WHERE INV.IDINVENTAIRE_PERIODE = #IDINVENTAIRE_PERIODE# 			
			AND INV.OPERATEURID = OP.OPERATEURID
		</cfquery>
		
		<!--- Choix par id op�rateur --->
		<cftry>
			<cfset facture=createObject("component","Facture#getOperateurID.OPERATEURID[1]#")> 				
			<cfcatch>
			
			</cfcatch>
		</cftry>
		
		<!--- Choix par pays --->
		<cfif not IsDefined("facture")>
			<cftry>
				<cfset facture=createObject("component","FacturePays#getOperateurID.PAYSCONSOTELID[1]#")>					
				<cfcatch>
				
				</cfcatch>
			</cftry>
		</cfif>
		
		<!--- Choix par code langue utilisateur --->
		<cfif not IsDefined("facture")>
			<cftry>
				<cfset facture=createObject("component","DefaultFacture_#SESSION.USER.GLOBALIZATION#")>					
				<cfcatch>
				
				</cfcatch>
			</cftry>
		</cfif>
		
		<!--- Choix par d�faut --->
		<cfif not IsDefined("facture")>
			<cfset facture=createObject("component","DefaultFacture")>					
		</cfif>
		
		<cfset facture.setIDINVENTAIRE_PERIODE(IDINVENTAIRE_PERIODE)>
		<cfset facture.setSigneDevise(IDINVENTAIRE_PERIODE)>
		<cfset facture.setOperateurInfos(getOperateurID)>
		<cfreturn facture>
	</cffunction>
</cfcomponent>
