<!---
Auteur : Cédric RAPIERA
Classe abstraite des factures opérateur. Cette classe contient les méthodes indispensables pour une Facture.
La classe fr.consotel.consoview.Facture.Facture dérive de cette classe et contient plus de méthodes.
En particulier c'est la classe qui est utilisée pour la 1ère version d'implémentation des factures.
C'est à dire 1 même requête pour toutes les factures opérateur.
Donc les factures qui veulent implémenter leur propre requête doivent dériver de cette classe pour ne pas utiliser
la requête commune des factures dérivant de la classe Facture.
--->

<cfcomponent name="AbstractFacture" alias="fr.consotel.consoview.Facture.AbstractFacture">
	<cffunction name="setIDINVENTAIRE_PERIODE" access="public" output="false">
		<cfargument name="IDINVENTAIRE_PERIODE" type="numeric" required="true"/>
		<cfset variables.instance.IDINVENTAIRE_PERIODE = arguments.IDINVENTAIRE_PERIODE/>
	</cffunction>
	
	<cffunction name="setTypeFactureStrategy" access="public" returntype="void" output="false">
		<cfargument name="type_facture" required="true" type="string"/>
		<cfargument name="type_perimetre" required="true" type="string"/>
		<cfset variables.instance.typeFactureStrategy=createObject("component","TypeFacture#type_facture##type_perimetre#Strategy")>
	</cffunction>
	
	<cffunction name="afficherFacture" access="public" returntype="void" output="true">
		<cfargument name="ID" required="false" type="string">
		<cfargument name="IDPerimetre" required="true" type="numeric">
		<cfabort showerror="Méthode Abstraite">
	</cffunction>
	
	<cffunction name="afficherFactureCat" access="public" returntype="void" output="true" hint="Permet d'afficher la facture">
		<cfargument name="ID" required="false" type="string" hint="Gabarit de la facture mais sans le détail (Agrégé)">
		<cfargument name="IDPerimetre" required="true" type="numeric">
		<cfargument name="factureParams" required="true" type="struct">
		<cfabort showerror="Méthode Abstraite">
	</cffunction>
	
	<cffunction name="setSigneDevise" access="public" description="Va chercher la devise de la facture" returntype="void">
		<cfargument name="p_idinventaire_periode" required="true" type="numeric">
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_M00.getFactureDevise">
			<cfprocparam value="#p_idinventaire_periode#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfquery name="q_retour" datasource="ROCOFFRE">
				SELECT * 
				FROM devise d 
				WHERE d.iddevise = #p_retour.DEVISE#
		</cfquery>
			
		<cfset variables.instance.signeDevise = q_retour.SYMBOL>
		<cfset variables.instance.shortDevise = q_retour.SHORT_DESC>
	</cffunction>
	
	<cffunction name="getSigneDevise" access="public" description="Retourne la devise de la facture enregistrée" returntype="any">
		<cfreturn variables.instance.signeDevise>
	</cffunction>
	
	<cffunction name="getShortDevise" access="public" description="Retourne la devise de la facture enregistrée" returntype="any">
		<cfreturn variables.instance.shortDevise>
	</cffunction>
	
	<cffunction name="getOperateurInfos" access="public" description="Retourne les infos opérateur" returntype="any">
		<cfreturn variables.instance.OperateurInfos>
	</cffunction>
	<cffunction name="setOperateurInfos" access="public" description="Retourne les infos opérateur" >
		<cfargument name="pOperateurInfos" required="true" type="query">
		<cfset variables.instance.OperateurInfos = pOperateurInfos>
	</cffunction>
</cfcomponent>
