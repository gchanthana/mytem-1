<!--- =========================================================================
Classe: RechercheFacture
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="RechercheFacture" hint="">

	<cffunction name="rechercheFactureByNumber" access="public" returntype="Query" output="false" >
		<cfargument name="NUMERO_CLE" required="false" type="string" default="" displayname="string NUMERO_CLE" hint="" />
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="" />
		<!--- "NUMERO_CLE" est le numéro que l'on a saisi dans la zone de recherche de facture par numéro de facture --->
		<cfreturn variables.instance.listeFactureStrategy.rechercheFactureByNumber("#NUMERO_CLE#",ID)>
	</cffunction>
	
	<cffunction name="setListeFactureStrategy" access="public" returntype="void" output="false" >
		<cfargument name="TYPE_PERIMETRE" required="false" type="string" default="" displayname="string TYPE_PERIMETRE" hint="Initial value for the TYPE_PERIMETRE property." />
		<cfset variables.instance.listeFactureStrategy=createObject("component","ListeFacture#TYPE_PERIMETRE#Strategy")>
	</cffunction>

</cfcomponent>