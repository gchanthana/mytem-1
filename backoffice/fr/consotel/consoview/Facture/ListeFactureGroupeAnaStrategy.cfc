<cfcomponent displayname="ListeFactureGroupeAnaStrategy" hint=""  extends="ListeFactureStrategy" >
   
	<cffunction name="rechercheFactureByNumber" access="public" returntype="Query" output="false" >
		<cfargument name="NUMERO_CLE" required="false" type="string" default="" displayname="string NUMERO_CLE" hint="" />
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="ID du groupe (ID_PERIMETRE)" />
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_ANA_V3.sf_listfacture">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#ID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_numero_cle" value="#NUMERO_CLE#"/>					
	        <cfprocresult name="listeFactures"/>        
		</cfstoredproc>
		 
		<cfloop from="1" to="#listeFactures.recordcount#" index="i">
			<cfset listeFactures.a[i]=LsDateFormat(listeFactures.DATE_EMISSION[i],"dd mmmm yyyy")>
			<cfset listeFactures.b[i]=LsDateFormat(listeFactures.DATEDEB[i],"dd mmmm yy")>
			<cfset listeFactures.c[i]=LsDateFormat(DateAdd("d",-1,listeFactures.DATEFIN[i]),"dd mmmm yy")>
		</cfloop>
		<cfset obj=createObject("component","fr.consotel.consoview.Facture.Facture")>
		<cfset obj.setData(listeFactures)>
		<cfreturn listeFactures>
	</cffunction>
	
	
	<!--- ========== pour exportXLS ================ --->
	<cffunction name="rechercheFactureByNumberEtOpe" access="remote" returntype="Query" output="false" >
		<cfargument name="arr" required="false" type="array" default="" displayname="string NUMERO_CLE" hint="" />		
		<!--- 
		<cfset d1=getToken(arr[4],3,"/") & "/" & getToken(arr[4],2,"/") & "/" & getToken(arr[4],1,"/")>
		<cfset d2=getToken(arr[5],3,"/") & "/" & getToken(arr[5],2,"/") & "/" & getToken(arr[5],1,"/")>
		 --->
		<cfset d1 = lsDateFormat(lsParseDateTime(arr[4]),"YYYY/MM/DD")>
		<cfset d2 = lsDateFormat(lsParseDateTime(arr[5]),"YYYY/MM/DD")>
		<cfset dataAffect = lsDateFormat(lsParseDateTime(arr[6]),"YYYY/MM/DD")>
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_ANA_V3.sf_listfactureop">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#arr[2]#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#d1#"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#d2#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#arr[3]#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_numero_cle" value="#arr[1]#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_dateAffect" value="#dataAffect#"/>
	        <cfprocresult name="listeFactures"/>
		</cfstoredproc>
		
		<cfloop query="listeFactures">
			<cfset listeFactures.a  = LsDateFormat(listeFactures.DATE_EMISSION,"dd mmmm yyyy")>
			<cfset listeFactures.b  = LsDateFormat(listeFactures.DATEDEB,"dd mmmm yy")>
			<cfset listeFactures.c  = LsDateFormat(listeFactures.DATEFIN,"dd mmmm yy")>
		</cfloop> 
		
		<cfset obj=createObject("component","fr.consotel.consoview.Facture.Facture")>
		<cfset obj.setData(listeFactures)>
		<cfreturn listeFactures>
	</cffunction>
	
	<cffunction name="getListeOperateur" access="remote" returntype="Query" output="false" >
		<cfargument name="arr" required="false" type="array" default="" displayname="string NUMERO_CLE" hint="" />
		<cfset dataAffect = lsDateFormat(lsParseDateTime(arr[2]),"YYYY/MM/DD")>
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_ANA_V3.sf_listeop">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#arr[1]#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#dataAffect#"/>
	        <cfprocresult name="listeFactures"/>        
		</cfstoredproc>
		<cfquery name="getListe" dbtype="query">
			select nom as label, operateurID as data
			from listeFactures
		</cfquery>
		<cfreturn getListe>
	</cffunction>
	
	<cffunction name="ValiderFacture" access="remote" returntype="void" output="false" >
		<cfargument name="arr" required="true" type="array" default="" />
		<cfquery name="updateEtat" datasource="#SESSION.OFFREDSN#">
			update inventaire_periode ip
			set ip.idetat_facture=2
			where ip.idinventaire_periode=#arr[1]#
		</cfquery>

	</cffunction>
	
	<cffunction name="displayListeFacture" access="public" returntype="void" output="true">
		<cfargument name="listeFactures" required="true" type="Query">
		<cfabort showerror="Erreur: Classe abstraite">
	</cffunction>
</cfcomponent>
