<!--- =========================================================================
Classe: FactureCreator
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->

<cfcomponent name="FactureCreator" alias="fr.consotel.consoview.Facture.FactureCreator">
	<cffunction name="createFacture" access="public" returntype="Facture" output="false">
		<cfargument name="IDINVENTAIRE_PERIODE" required="false" type="numeric" default=""/>
		<cfabort showerror="Erreur: Classe abstraite">
	</cffunction>
</cfcomponent>
