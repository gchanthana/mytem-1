<!--- =========================================================================
Classe: TypeFactureCompleteGroupeSousCompteStrategy
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent name="TypeFactureCompleteGroupeSousCompteStrategy" extends="TypeFactureStrategy" hint="">
<!--- METHODS --->
	<cffunction name="getData" access="public" returntype="Query" output="false" >
		<cfargument name="IDINVENTAIRE_PERIODE" required="true" type="numeric" default="" displayname="numeric IDINVENTAIRE_PERIODE" hint="Initial value for the IDINVENTAIRE_PERIODEproperty." />
		<cfargument name="fake" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="IDPerimetre" required="true" type="numeric">
			
		<cfquery name="qGetID" datasource="#session.OFFREDSN#">
			select ip.operateurid, cf.idref_client, rc.clientid
			from inventaire_periode ip, compte_facturation cf, ref_client rc,
				sous_compte sco, groupe_client_ref_client gcrc
			WHERE cf.idcompte_facturation=ip.idcompte_facturation
					AND rc.idref_client=cf.idref_client
					AND ip.IDINVENTAIRE_PERIODE=#IDINVENTAIRE_PERIODE#
					AND cf.idcompte_facturation=sco.idcompte_facturation
					AND sco.idsous_compte=gcrc.idref_client
					AND gcrc.idgroupe_client=#IDPerimetre#
		</cfquery>
		<cfset v_opid=0>
		<cfset v_clientid=0>
		<cfset v_idref_client=0>
		<cfif qGetID.recordcount gt 0>
			<cfloop query="qGetID">
				<cfset v_opid=operateurid>
				<cfset v_clientid=clientid>
				<cfset v_idref_client=idref_client>
			</cfloop>
		</cfif>

		<cfquery name="qGetFactureAbo" datasource="#session.offreDSN#">
		    SELECT cf.compte_facturation, sc.sous_compte, pca.libelle_produit, st.sous_tete,
				sum(dfa.montant) as montant, sum(nvl(dfa.nombre_appel,0)) as nombre_appel, sum(dfa.duree_appel) as duree_appel,
				sum(dfa.qte) as qte , tp.libelle, cp.libelle as gamme, dfa.prix_unit,
				to_char(dfa.datedeb,'yyyy/mm/dd') as datedeb, to_char(dfa.datefin,'yyyy/mm/dd') as datefin,
				ip.numero_facture, ip.date_emission, scl.nom_site, scl.adresse1, scl.adresse2,
				scl.zipcode, scl.commune, tpl.libelle_type_ligne, pcv.code_tva, scl.siteID
			FROM inventaire_periode ip,
				groupe_client_ref_client gcrc,
				DETAIL_FACTURE_ABO dfa,
				produit_client pcl,
				produit_catalogue pca,
				type_produit tp,
				categorie_produit cp,
				sous_tete st,
				prod_cat_version pcv,
				compte_facturation cf,
				sous_compte sc,
				site_client scl,
				type_ligne tpl
			WHERE ip.IDINVENTAIRE_PERIODE=#IDINVENTAIRE_PERIODE#
	    	    	AND gcrc.idgroupe_client=#IDPerimetre#
					AND ip.idinventaire_periode=dfa.idinventaire_periode
	 				AND st.idsous_tete=dfa.idsous_tete
					AND dfa.idproduit_client=pcl.idproduit_client
					AND pca.idproduit_catalogue=pcl.idproduit_catalogue
					AND pca.idtype_produit=tp.idtype_produit
					AND tp.idcategorie_produit=cp.idcategorie_produit
					AND pca.idproduit_catalogue=pcv.idproduit_catalogue
					AND st.idsous_compte=sc.idsous_compte
				   AND sc.idsous_compte=gcrc.idref_client
					AND cf.idcompte_facturation=sc.idcompte_facturation
					AND sc.siteID=scl.siteID
					AND st.idtype_ligne=tpl.idtype_ligne
					AND dfa.Idref_Client=#v_idref_client#
					AND pcl.idref_client=#v_idref_client#
					AND cf.idref_client=#v_idref_client#
					AND scl.clientid=#v_clientid#
					AND pca.operateurid=#v_opid#
			GROUP BY cf.compte_facturation, sc.sous_compte, tp.libelle,
				cp.libelle,pca.libelle_produit,st.sous_tete, dfa.prix_unit,
				to_char(dfa.datedeb,'yyyy/mm/dd'), to_char(dfa.datefin,'yyyy/mm/dd'),
				ip.numero_facture, ip.date_emission, scl.nom_site, scl.adresse1, scl.adresse2,
				scl.zipcode, scl.commune, tpl.libelle_type_ligne,pcv.code_tva, scl.siteID
			ORDER BY sc.sous_compte, gamme, tp.libelle, sous_tete
		</cfquery>
		<cfreturn qGetFactureAbo>
	</cffunction>
	<cffunction name="displayType" access="public" returntype="string" output="false" >
		<cfreturn "Facture Complète">
	</cffunction>
</cfcomponent>
