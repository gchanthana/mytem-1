<!--- =========================================================================
Classe: TypeFactureStrategy
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="TypeFactureStrategy" hint="This class declares an interface common to all supported algorithms. Context uses this interface to call the algorithm defined by a ConcreteStrategy.">
 
<!--- METHODS --->
	<cffunction name="getData" access="public" returntype="Query" output="false" >
		<cfargument name="IDINVENTAIRE_PERIODE" required="true" type="numeric" default="" displayname="numeric IDINVENTAIRE_PERIODE" hint="Initial value for the IDINVENTAIRE_PERIODEproperty." />
		<cfargument name="ID" required="false" type="string" hint="ID Compte, Site, Sous tete">
		<cfabort showerror="Erreur: Classe abstraite">
	</cffunction>
	<cffunction name="displayType" access="public" returntype="string" output="false" >
		<cfabort showerror="Erreur: Classe abstraite">
	</cffunction>

</cfcomponent>
