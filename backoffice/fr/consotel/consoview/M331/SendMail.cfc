<cfcomponent displayname="fr.consotel.consoview.M331.SendMail" hint="Gestion de l'envoi de mails" output="false">

	<!--- 
		ARGUMENTS.eventObject.getEventType() = EC -> ERREUR CLIENT
		ARGUMENTS.eventObject.getEventType() = EP -> ERREUR PRODUCTION
		ARGUMENTS.eventObject.getEventType() = SC -> PARTAGE CLIENT (SHARE CLIENT)
		ARGUMENTS.eventObject.getEventType() = PC -> PUBLICATION CLIENT
		ARGUMENTS.eventObject.getEventType() = PP -> PUBLICATION PRODUCTION
	 --->
		
		<cfset properties 	 = structNew()>
		<cfset mailserver 	 = 'mail-cv.consotel.fr'>		
		<cfset destMailError = 'monitoring@saaswedo.com'>
		<cfset fromMailError = 'monitoring@saaswedo.com'>  

	<!--- TRAITEMENT DES DONNEES DE L'EVENT RECUPERER --->
	<cffunction access="public" name="sendMail" returntype="void">
		<cfargument name="eventObject" 	type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		
			<cfset mailing  	= createObject("component","fr.consotel.consoview.M01.Mailing")>				

			<cfset uuidlog		= #ARGUMENTS.eventObject.getEventTarget()#>
			<cfset jobid		= #ARGUMENTS.eventObject.getJobId()#>
			<cfset type			= #ARGUMENTS.eventObject.getEventType()#>
			
			<cfset ImportTmp	= "/container/M331/">
			<cfset objectMail 	= ''>
			<cfset cptr 		= 0>
			<cfset param 		= structNew()>
			
			<cfset fileMail	 	= "mail#uuidlog#.html">
			<cfset pathEntier	= '#ImportTmp##uuidlog#/'>

			<cfset rsltGetMail 	= getMailContent(uuidlog, type)>		
			
			
			<cfif isDefined('rsltGetMail.UUID')>			
				<cfif rsltGetMail.MAJ GT 0>
					<!--- ENVOIE LE MAIL SI LA PROCÉDURE A RÉCUPERÉE LES PARMATRES DE MAILS ET MIS À JOUR LES PARMATRES DE MAILS--->
					<cfset sendMailContent(pathEntier, fileMail, uuidlog, rsltGetMail,ARGUMENTS.eventObject)>
				<cfelse>
					<!--- ENVOIE UN MAIL D'ERREUR SI LA PROCÉDURE N'A PAS MIS À JOUR LES PARMATRES DE MAILS --->
					<cfset procedureInError(uuidlog, type, jobid, "ERREUR MISE A JOUR DES PARMATRES DE MAILS", "ERREUR MISE A JOUR DES PARMATRES DE MAILS", 'updateMailToDatabase', rsltUpdate)>
				</cfif>
				
			<cfelse>
				<!--- ENVOIE UN MAIL D'ERREUR SI LA PROCÉDURE N'A PAS RÉCUPÉRÉE LES DONNÉES DU MAIL --->
				<cfset procedureInError(uuidlog, type, jobid, "ERREUR RECUPERATION DES PARMATRES DE MAILS", "ERREUR RECUPERATION DES PARMATRES DE MAILS", 'getMailToDatabase', rsltGetMail)>
			</cfif>

	</cffunction>

	<!--- RECUPERER LES INFORMATIONS POUR LE SERVEUR DE MAILS --->
	<cffunction access="public" name="getProperties" returntype="Struct">
		<cfargument name="codeapp" 	type="Numeric" 	required="true">
 		<cfargument name="key" 		type="String" 	required="true"/>
			
			<cfset properties 	 	= structNew()>
			<cfset codeappPropert	= structNew()>
			
			<cfset mailProperties  	= createObject("component","fr.consotel.consoview.M01.Mailing")>
			<cfset rsltProperties  	= mailProperties.get_appli_properties(codeapp, key)>
			
			<cfloop query="rsltProperties">
				<cfset codeappPropert[rsltProperties.KEY] = rsltProperties.VALUE>
			</cfloop>
			
		<cfreturn codeappPropert>
	</cffunction>

	<!--- RECUPERE LES PARAMETRES DU MAIL --->
	<cffunction access="public" name="getMailContent" returntype="Any">
		<cfargument name="uuidlog" 		type="String" required="true">
		<cfargument name="typeevent" 	type="String" required="true">
	
			<cfset contentMail	= structNew()>
			<cfset mailing  	= createObject("component","fr.consotel.consoview.M01.Mailing")>
			<cfset rsltGetMail	= mailing.getMailToDatabase(uuidlog)>
			
			<cfloop query="rsltGetMail">
				<cfif rsltGetMail.TAG EQ typeevent && rsltGetMail.IS_CRATE EQ 0>
					<cfset contentMail.FROM			= rsltGetMail.MAIL_FROM>
					<cfset contentMail.TOMAIL		= rsltGetMail.MAIL_TO>
					<cfset contentMail.BCC			= rsltGetMail.MAIL_BCC>
					<cfset contentMail.SUBJECT		= rsltGetMail.MAIL_SUBJECT>
					<cfset contentMail.UUID			= rsltGetMail.UUID>
					<cfset contentMail.CODE_APPLI	= rsltGetMail.CODE_APPLI>
					<cfset contentMail.MAJ 			= mailing.updateMailToDatabase(1, uuidlog, typeevent)>
				</cfif>
			</cfloop>

			<cfset properties = getProperties(contentMail.CODE_APPLI, 'NULL')>
			
			<cfif NOT isDefined('properties.MAIL_SERVER')>
				<cfset properties.MAIL_SERVER = 'mail-cv.consotel.fr'>
			</cfif>
			
			<cfif NOT isDefined('properties.MAIL_PORT')>
				<cfset properties.MAIL_PORT = 26>
			</cfif>

		<cfreturn contentMail>
	</cffunction>

	<!--- ENVOI DU MAIL --->
	<cffunction access="public" name="sendMailContent" returntype="void">
		<cfargument name="pathEntier" 	type="String" required="true">
		<cfargument name="myFile" 		type="String" required="true">
		<cfargument name="uuidlog" 		type="String" required="true">
		<cfargument name="mailStruct" 	type="Struct" required="true">
		<cfargument name="eventObject" 	type="any" 	  required="false">
			
			<cfset var myDir 	 = GetDirectoryFromPath(GetCurrentTemplatePath())/>
			<cfset var M331_UUID = createUUID()>
			<cfset var ImportTmp = "/container/M331/">
											
			<cftry>
						  
				<cfif NOT DirectoryExists('#myDir##M331_UUID#')>
					  
					<cfdirectory action="Create" directory="#myDir##M331_UUID#" type="dir" mode="777">
					
				<cfelse>
					
					<cfset M331_UUID = createUUID()>
					
					<cfdirectory action="Create" directory="#myDir##M331_UUID#" type="dir" mode="777">
					
				</cfif>
			
			<cfcatch>
						
					<!--- En cas d'échec de l'op?ration envoi d'un mail de notification --->												
					<cfmail server="mail-cv.consotel.fr" port="25" from="rapport@saaswedo.com" to="monitoring@saaswedo.com" 
							subject="[WARN-RAPPORT] M331-MAIL-PUBLICATION Erreur lors de la création du répertoire" type="html">
										 							
							#CFCATCH#<br/><br/><br/><br/>
							
							Chemin 	: <cfdump var="#pathEntier#"><br/><br/>
							Fichier : <cfdump var="#myFile#"><br/><br/>
							UUIDLOG : <cfdump var="#uuidlog#"><br/><br/>
							Mail 	: <cfdump var="#mailStruct#"><br/><br/>
							**************************************************************<br/><br/>
							**************************************************************<br/><br/>
							<ul>
							 <li><b>BipServer</b>	   : <cfdump var="#ARGUMENTS.eventObject.getBipServer()#"></li>
							 <li><b>EventDispatcher</b>: <cfdump var="#ARGUMENTS.eventObject.getEventDispatcher()#"></li>
							 <li><b>EventTarget</b>  : <cfdump var="#ARGUMENTS.eventObject.getEventTarget()#"></li>
							 <li><b>JobId</b>        : <cfdump var="#ARGUMENTS.eventObject.getJobId()#"></li>
							 <li><b>EventType</b>    : <cfdump var="#ARGUMENTS.eventObject.getEventType()#"></li>
							 <li><b>ReportPath</b>   : <cfdump var="#ARGUMENTS.eventObject.getReportPath()#"></li>
							 <li><b>ReportStatus</b> : <cfdump var="#ARGUMENTS.eventObject.getReportStatus()#"></li>
							</ul>
					</cfmail>
						
			</cfcatch>					
			</cftry>			
			
			<cfset root	= "#myDir##M331_UUID#">
			
			<cftry>
			
				<cfdirectory name="dGetDir" directory="#pathEntier#" action="List">
				
				<cffile action="copy" source="#ImportTmp##uuidlog#/#myFile#" destination="#myDir##M331_UUID#/">
				
			<cfcatch>
						
					<!--- En cas d'échec de l'op?ration envoi d'un mail de notification --->												
					<cfmail server="mail-cv.consotel.fr" port="25" from="rapport@saaswedo.com" to="monitoring@saaswedo.com" 
							subject="[WARN-RAPPORT] M331-MAIL-PUBLICATION Erreur lors de la copie du répertoire" type="html">
										 							
							#CFCATCH#<br/><br/><br/><br/>
							
							Chemin 	: <cfdump var="#pathEntier#"><br/><br/>
							Fichier : <cfdump var="#myFile#"><br/><br/>
							UUIDLOG : <cfdump var="#uuidlog#"><br/><br/>
							Mail 	: <cfdump var="#mailStruct#"><br/><br/>							
							**************************************************************<br/><br/>
							**************************************************************<br/><br/>
							<ul>
							 <li><b>BipServer</b>	   : <cfdump var="#ARGUMENTS.eventObject.getBipServer()#"></li>
							 <li><b>EventDispatcher</b>: <cfdump var="#ARGUMENTS.eventObject.getEventDispatcher()#"></li>
							 <li><b>EventTarget</b>  : <cfdump var="#ARGUMENTS.eventObject.getEventTarget()#"></li>
							 <li><b>JobId</b>        : <cfdump var="#ARGUMENTS.eventObject.getJobId()#"></li>
							 <li><b>EventType</b>    : <cfdump var="#ARGUMENTS.eventObject.getEventType()#"></li>
							 <li><b>ReportPath</b>   : <cfdump var="#ARGUMENTS.eventObject.getReportPath()#"></li>
							 <li><b>ReportStatus</b> : <cfdump var="#ARGUMENTS.eventObject.getReportStatus()#"></li>
							</ul>
					</cfmail>
						
			</cfcatch>					
			</cftry>
				
			<cftry>
						
				<cfmail from="#mailStruct.FROM#" to="#mailStruct.TOMAIL#" subject="#mailStruct.SUBJECT#" bcc="#mailStruct.BCC#" type="text/html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">		
					
					<cfinclude template="/fr/consotel/consoview/M331/#M331_UUID#/#myFile#">
				
				</cfmail>
			
			<cfcatch>
					
				<!--- En cas d'échec de l'op?ration envoi d'un mail de notification --->												
				<cfmail server="mail-cv.consotel.fr" port="25" from="rapport@saaswedo.com" to="monitoring@saaswedo.com" 
							subject="[WARN-RAPPORT] M331-MAIL-PUBLICATION Erreur lors de l'envoi du mail" type="html">
									 							
						#CFCATCH#<br/><br/><br/><br/>
						
						Chemin 	: <cfdump var="#pathEntier#"><br/><br/>
						Fichier : <cfdump var="#myFile#"><br/><br/>
						UUIDLOG : <cfdump var="#uuidlog#"><br/><br/>
						Mail 	: <cfdump var="#mailStruct#"><br/><br/>						
						**************************************************************<br/><br/>
						**************************************************************<br/><br/>
						<ul>
						 <li><b>BipServer</b>	   : <cfdump var="#ARGUMENTS.eventObject.getBipServer()#"></li>
						 <li><b>EventDispatcher</b>: <cfdump var="#ARGUMENTS.eventObject.getEventDispatcher()#"></li>
						 <li><b>EventTarget</b>  : <cfdump var="#ARGUMENTS.eventObject.getEventTarget()#"></li>
						 <li><b>JobId</b>        : <cfdump var="#ARGUMENTS.eventObject.getJobId()#"></li>
						 <li><b>EventType</b>    : <cfdump var="#ARGUMENTS.eventObject.getEventType()#"></li>
						 <li><b>ReportPath</b>   : <cfdump var="#ARGUMENTS.eventObject.getReportPath()#"></li>
						 <li><b>ReportStatus</b> : <cfdump var="#ARGUMENTS.eventObject.getReportStatus()#"></li>
						</ul>
				</cfmail>
						
			</cfcatch>					
			</cftry>
			
			<cftry>
			
				<cfdirectory name="dGetDir" directory="#root#" action="List"> 
							
				<cfloop query="dGetDir">
					
					<cfif type NEQ "DIR">
						
						<cffile action="delete" file="#root#/#dGetDir.name#">
					
					</cfif> 
				
				</cfloop>
				
				<cfdirectory action="delete" directory="#myDir##M331_UUID#">
				
			<cfcatch>
					
				<!--- En cas d'échec de l'op?ration envoi d'un mail de notification --->												
				<cfmail server="mail-cv.consotel.fr" port="25" from="rapport@saaswedo.com" to="monitoring@saaswedo.com" 
							subject="[WARN-RAPPORT] M331-MAIL-PUBLICATION Erreur lors de la destruction du répertoire" type="html">
									 							
						#CFCATCH#<br/><br/><br/><br/>
						
						Chemin 	: <cfdump var="#pathEntier#"><br/><br/>
						Fichier : <cfdump var="#myFile#"><br/><br/>
						UUIDLOG : <cfdump var="#uuidlog#"><br/><br/>
						Mail 	: <cfdump var="#mailStruct#"><br/><br/>						
						**************************************************************<br/><br/>
						**************************************************************<br/><br/>
						<ul>
						 <li><b>BipServer</b>	   : <cfdump var="#ARGUMENTS.eventObject.getBipServer()#"></li>
						 <li><b>EventDispatcher</b>: <cfdump var="#ARGUMENTS.eventObject.getEventDispatcher()#"></li>
						 <li><b>EventTarget</b>  : <cfdump var="#ARGUMENTS.eventObject.getEventTarget()#"></li>
						 <li><b>JobId</b>        : <cfdump var="#ARGUMENTS.eventObject.getJobId()#"></li>
						 <li><b>EventType</b>    : <cfdump var="#ARGUMENTS.eventObject.getEventType()#"></li>
						 <li><b>ReportPath</b>   : <cfdump var="#ARGUMENTS.eventObject.getReportPath()#"></li>
						 <li><b>ReportStatus</b> : <cfdump var="#ARGUMENTS.eventObject.getReportStatus()#"></li>
						</ul>
				</cfmail>
						
			</cfcatch>					
			</cftry>

	</cffunction>
		
	<!--- ENVOI LES MAILS D'ERREURS --->
	<cffunction access="public" name="procedureInError" returntype="void">
		<cfargument name="uuid" 		type="String" required="true">
		<cfargument name="typeevent" 	type="String" required="true">
		<cfargument name="mailJobId" 	type="String" required="true">
		<cfargument name="mailSubject" 	type="String" required="true">
		<cfargument name="mailContent" 	type="String" required="true">
		<cfargument name="procedure" 	type="String" required="true">
		<cfargument name="others" 		type="Any" 	  required="false" default="NONE">
		
			<cfset param 			 = structNew()>
			<cfset param.FROM 		 = 'container@consotel.fr'>
			<cfset param.TOMAIL  	 = 'monitoring@saaswedo.com'>
			<cfset param.BCC	 	 = ''>
			<cfset param.SUBJECT 	 = #mailSubject#>
		
			<cfmail from="#param.FROM#" to="#param.TOMAIL#" subject="#param.SUBJECT#" bcc="#param.BCC#" type="text/html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">		
				#mailContent#<br><br>
				JOBID		: #mailJobId#<br><br>
				UUID		: #uuid#<br><br>
				EVENTTYPE 	: #typeevent#<br><br>
				PROCEDURE	: #procedure#<br><br>
				<cfdump var="#others#">
			</cfmail>
			
	</cffunction>

</cfcomponent>