﻿<cfcomponent>
	<cffunction access="remote" output="false" name="getValuesPeriode" returntype="query">
			
			<cfargument name="idRapportParametre" type="numeric" required="true">
		    <cfset idLang  = SESSION.USER.IDGLOBALIZATION>
            <cfset date_last_facture = lsdateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN,'dd/mm/yyyy')>
		
											
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.get_PeriodeComboBoxSelector_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRapportParametre#" variable="p_idrapport_parametre">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#date_last_facture#" variable="p_date_fin">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idlang#" variable="p_langid">
				<cfprocresult name="qPeriode">
			</cfstoredproc>				
            <cfreturn qPeriode><!--- le resultat--->
      </cffunction> 
</cfcomponent>