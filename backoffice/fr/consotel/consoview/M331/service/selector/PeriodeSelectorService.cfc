﻿<cfcomponent>
	
	<cffunction access="remote" name="getValuesPeriode"  output="false" returntype="query">
		
		<cfset LOCAL.dateDebut = lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB,'dd/mm/yyyy')>
		<cfset LOCAL.dateFin = lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN,'dd/mm/yyyy')>
		<cfset LOCAL.maxMonthToDisplay = SESSION.PERIMETRE.STRUCTDATE.PERIODS.MAX_MONTH_TO_DISPLAY>
				
		<cfstoredproc datasource="ROCOFFRE"  procedure="PKG_M331.get_MonoPeriodeSelector">		
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#LOCAL.dateDebut#" variable="p_datedeb">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#LOCAL.dateFin#"   variable="p_datefin">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#LOCAL.maxMonthToDisplay#" variable="p_nb_mois_max">
			<cfprocresult name="qMonoPeriode">
		</cfstoredproc>
			
        <cfreturn qMonoPeriode>	
	</cffunction>


</cfcomponent>