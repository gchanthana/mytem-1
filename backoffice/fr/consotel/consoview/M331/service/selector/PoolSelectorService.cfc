﻿<cfcomponent>
	<cffunction access="remote" output="false" name="getValuesPool" returntype="query">
				   
		    <cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
            <cfset userID   = SESSION.USER.CLIENTACCESSID><!--- l'id de l'utilisateur connecté --->
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.get_PoolSelector">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#userID#" variable="p_idapp_login ">*
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">
				<cfprocresult name="qPeriode">
			</cfstoredproc>				
            <cfreturn qPeriode><!--- le resultat--->
      </cffunction> 
</cfcomponent>