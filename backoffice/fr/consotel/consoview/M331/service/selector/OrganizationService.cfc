<cfcomponent output="false">
	
	<cffunction access="remote" output="false" name="getListOrganizations" returntype="query">
			<cfset RacineId = SESSION.PERIMETRE.ID_GROUPE>
			<cfset locale = SESSION.USER.GLOBALIZATION>
			<cfset DSN = SESSION.OFFREDSN>

            <cfreturn listOrganizations(DSN,RacineId,locale)>
      </cffunction> 

      <cffunction access="remote" output="false" name="listOrganizations" returntype="query">
      		<cfargument name="DSN" type="string" required="true">
			<cfargument name="RacineId" type="numeric" required="true">
            <cfargument name="locale" type="string" required="true">
	
			<cfstoredproc datasource="#ARGUMENTS['DSN']#" procedure="pkg_cv_global.get_organisations_Plus">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS['RacineId']#" variable="p_idracine">
				<cfprocresult name="qListOrgas">
			</cfstoredproc>	
			
			<cfquery name="qOrgaCUS" dbtype="query">
				select IDGROUPE_CLIENT as ID, LIBELLE_GROUPE_CLIENT as LIBELLE from qListOrgas where TYPE_ORGA = 'CUS' ORDER BY LIBELLE_GROUPE_CLIENT ASC
			</cfquery>
			
   			<!--- -- and IDGROUPE_CLIENT = 2772196 --->
            <cfreturn qOrgaCUS>
      </cffunction> 

</cfcomponent>