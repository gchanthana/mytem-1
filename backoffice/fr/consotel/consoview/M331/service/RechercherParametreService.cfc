﻿<!-- displayname : le nom de composant -->

<cfcomponent displayname="Parametres" output="false">
	
	<!-- déclarer une fonction  name : le nom de la fonction ;returntype : le type du resultat retourné -->
	<!-- access : visibilité -->
	
	<cffunction access="public" output="false" name="getParametreMoke" returntype="query">
			
			<!-- les arguements de la fonction ; name: le nom de l'arguement; required="true": obligatoire -->
			<!-- plusieurs arguements = plusieurs balise -->
			<!-- on peut donner des noms differents des noms utilisé dans la fonction flex,  mais il faut garder l'order' -->
			<cfargument name="idRapport" type="numeric" required="true">
					
			<!-- déclarer des varaibles pour passer au procédure -->
			<!-- cette décalration n'est pas obligatoire et dépend de procédure' -->
			
			
			<!-- dataSource: obligatoires - nom de la source de données qui pointe vers la base de données qui contient une procédure stockée. -->
			<!-- procédure : Obligatoires nom de la procédure stockée sur le serveur de base de données.-->
			 
            <cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M331.GET_ALL_PARAMETRE_RAPPORT">			
				
				 <!-- paramtères : Si la procédure stockée utilise des paramètres d'entrée ou de sortie -->	
				 <!-- ces paramètres  dépendent de la procedure -->			 
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#iduser#" variable="p_app_loginid">
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#" variable="p_idracine">
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#codeLangue#" variable="p_langid">
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idgroupe_client#" variable="p_idgroupe_client">
                  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#codeModule#">

                  <cfprocresult name="qParametres"><!-- Si la procédure stockée retourne un résultat -->
            </cfstoredproc>        					
            <cfreturn qParametres><!-- le resultat de la procedure-->
      </cffunction>  
       
       <cffunction access="public" output="false" name="getParametre" returntype="query">
			
						
			<cfset  qParametres = queryNew("IDPARAM,LIBELLE,NOM","Integer,VarChar,VarChar" )>
			<cfset queryAddRow(qParametres,1)>
			<cfset querySetCell(qParametres,'IDPARAM',1)>
			<cfset querySetCell(qParametres,'LIBELLE',"Template 1")>
			<cfset querySetCell(qParametres,'NOM',"format 1")>
			<cfset queryAddRow(qParametres,1)>
			<cfset querySetCell(qParametres,'IDPARAM',2)>
			<cfset querySetCell(qParametres,'LIBELLE',"Template analyse")>
			<cfset querySetCell(qParametres,'NOM',"format 2")>
					
            <cfreturn qParametres><!-- le resultat de la procedure-->
      </cffunction>     
</cfcomponent>