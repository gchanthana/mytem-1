﻿<!-- displayname : le nom de composant -->
<cfcomponent displayname="Modules" output="false">
	
	<!-- déclarer une fonction  name : le nom de la fonction ;returntype : le type du resultat retourné -->
	<!-- access : visibilité -->
	
	<cffunction access="remote" output="false" name="getModule" returntype="Array">
		
			<!-- La balise  cfset permet de définir et d'attribuer une valeur à une variable -->
			<!-- les varaibles ne sont pas typées -->
			<!-- déclarer des varaibles pour passer au procédure -->

			<cfset iduser   = SESSION.USER.CLIENTACCESSID>
			<cfset idRacine = SESSION.USER.CLIENTID>
			<cfset codeApp  = SESSION.CODEAPPLICATION>
			<cfset idLang  = SESSION.USER.IDGLOBALIZATION>
			<cfset currentModule  =  SESSION.CURRENT_MODULE>
			
			<cfset idmodule= 0><!--- currentModule.MODULE_CONSOTELID --->
					
			<!-- dataSource: obligatoires - nom de la source de données qui pointe vers la base de données qui contient une procédure stockée. -->
			<!-- procédure : Obligatoires nom de la procédure stockée sur le serveur de base de données.-->
			 
             <cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.listeModule">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#iduser#" variable="p_app_loginid ">	
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#" variable="p_idracine">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#codeapp#" variable="p_code_appli ">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idlang#" variable="p_langid">
				<cfprocresult name="qModules">
			</cfstoredproc>
			
			<cfset MyArray=arrayNew(1)>
			<cfset MyArray[1]=qModules>
			<cfset MyArray[2]=idmodule>	
            <cfreturn MyArray><!-- le resultat de la procedure-->
      </cffunction>
	
</cfcomponent>