<cfcomponent output="false">
	
	<cffunction access="remote" name="associerDestNoeud" hint="associer destinataire à un noeud" returntype="Numeric" output="true">
	
		<cfargument name="id" type="numeric" required="true">
		<cfargument name="id_noeud" type="numeric" required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.associer_dest_noeud">		
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" value="#id#" variable="p_iddestinataire">
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" value="#id_noeud#" variable="p_idnoeud">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">				
		</cfstoredproc>	
		
		<cfset resultat=#p_retour#>
		<cfreturn resultat>

	</cffunction>
	
	<cffunction access="remote" name="associerPlusieurDestNoeud" hint=" associer plusieurs destinataire à un noeud" returntype="Numeric" output="true">
	
		<cfargument name="id" type="string" required="true">
		<cfargument name="id_noeud" type="numeric" required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.associer_dest_noeud_v2">		
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#id#" variable="p_destinataires">
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" value="#id_noeud#" variable="p_idnoeud">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">				
		</cfstoredproc>	
		
		<cfset resultat=#p_retour#>
		<cfreturn resultat>

	</cffunction>
	
	
	<cffunction access="remote" name="dissocierDestNoeud" hint="ajouter & modifier un destinataire" returntype="Numeric" output="true">
	
		<cfargument name="id" type="numeric" required="true">
		<cfargument name="id_noeud" type="numeric" required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.DeleteDestinataireNoeud">		
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" value="#id#" variable="p_iddestinataire">
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" value="#id_noeud#" variable="p_idnoeud">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">				
		</cfstoredproc>	
		
		<cfset resultat=#p_retour#>
		<cfreturn resultat>

	</cffunction>
	
</cfcomponent>