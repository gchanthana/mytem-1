<cfcomponent output="false">
	
	<cffunction access="remote" output="false" name="getListeProfilsOrga" returntype="query">
		
			<cfargument name="idOrga" type="numeric">
				
            <cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.getListeProfilsOrga">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idOrga#" variable="p_idorga">
				<cfprocresult name="qProfils">
			</cfstoredproc>				
            <cfreturn qProfils>
      </cffunction>
	
</cfcomponent>