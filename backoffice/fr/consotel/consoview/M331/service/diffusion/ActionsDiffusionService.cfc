<cfcomponent output="false">
	
	<cffunction access="remote" output="false" name="supprimerDiffusion" returntype="numeric">
		
		<cfargument name="idEvent" type="numeric" required="true">
		<cfargument name="noContrat" type="numeric" required="true">	
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
			 
        <cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.supprimerDiffusion">
	
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEvent#" variable="p_idevent">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#noContrat#" variable="p_no_contrat">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">

			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
			
		<cfset resultat=#p_retour#>
		<cfreturn resultat>	
		
     </cffunction>
	
	<cffunction access="remote" output="false" name="activerDiffusion" returntype="numeric">
		
		<cfargument name="idEvent" type="numeric" required="true">
		<cfargument name="noContrat" type="numeric" required="true">	
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
		
        <cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.activerDiffusion">
	
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEvent#" variable="p_idevent">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#noContrat#" variable="p_no_contrat">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">
	
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
			
		<cfset resultat=#p_retour#>
		<cfreturn resultat>	
		
     </cffunction>
	
	<cffunction access="remote" output="false" name="desactiverDiffusion" returntype="numeric">
		
		<cfargument name="idEvent" type="numeric" required="true">
		<cfargument name="noContrat" type="numeric" required="true">	
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
		
        <cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.desactiverDiffusion">
	
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEvent#" variable="p_idevent">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#noContrat#" variable="p_no_contrat">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">
	
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
			
		<cfset resultat=#p_retour#>
		<cfreturn resultat>	
		
     </cffunction>
	
	<cffunction access="remote" output="false" name="resendDiffusion" returntype="numeric">
		
		<cfargument name="id" type="numeric" required="true">
		
        <cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.resendPlanif">
	
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#id#" variable="p_id">
	
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
			
		<cfset resultat=#p_retour#>
		<cfreturn resultat>	
		
     </cffunction>

	<cffunction access="remote" output="false" name="testerDiffusion" returntype="numeric">
		
		<cfargument name="idEvent" type="numeric" required="true">
		<cfargument name="noContrat" type="numeric" required="true">	
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
		
		<cfset urlWebService="http://docker-diffusion.swd-internal.com/fr/consotel/api/ibis/diffusion/EvenementGeneric.cfc?method=execute&TYPE_MAIL=#idEvent#&MODE_DEBUG=1&IDRACINE=#idRacine#&IDCONTRAT=#noContrat#">
		
		<cfthread action="run" name="testerDiffusion">
			 <cfhttp
	           url="#urlWebService#"
	           method="get"
	           result="httpResponse">
	        </cfhttp>
		</cfthread>
       	<cfthread action ="sleep" duration="1000">
		</cfthread>
		<cfreturn  1>

		
     </cffunction>	
	
	
</cfcomponent>