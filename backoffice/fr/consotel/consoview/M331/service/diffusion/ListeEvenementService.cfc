<cfcomponent output="false">
	
	<cffunction access="remote" output="false" name="getListeEvenement" returntype="query">
		
			<cfset codeApp  = SESSION.CODEAPPLICATION>
			<cfset code_Lang  = SESSION.USER.GLOBALIZATION>
				
            <cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.getListeEvenement">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#codeApp#" variable="p_code_app">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#code_Lang#" variable="p_code_langue">
				<cfprocresult name="qEvents">
			</cfstoredproc>				
            <cfreturn qEvents><!--- le resultat de la procedure --->
      </cffunction>
	
</cfcomponent>