<cfcomponent output="false">
	
	<cffunction access="remote" output="false" name="getNbDestinataire" returntype="query">
		
		<cfargument name="idOrga" type="numeric" required="true">	
		<cfargument name="idProfil" type="numeric" required="true">
		
		
		<cfset isNullIdProfil=false>
		<cfif idProfil eq 0>			
			<cfset isNullIdProfil=true>
			<cfelse>
				<cfset isNullIdProfil=false>
		</cfif>
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.getNbDestinataire">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idOrga#"   variable="p_idorga">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" variable="p_idprofil" null="#isNullIdProfil#">
			<cfprocresult name="nbDestinataire">
		</cfstoredproc>				
        <cfreturn nbDestinataire><!--- le resultat de la procedure --->
	</cffunction>
	
</cfcomponent>