<cfcomponent  displayname="ListeFactureService" output="false">
<cffunction name="getPaginateListeFacture" access="remote" returntype="query">
	
	<cfargument name="datedebut" type="date" required="true">
	<cfargument name="datefin" type="date" required="true">
	<cfargument name="operateurid" type="string" required="true">
	<cfargument name="SEARCH_TEXT_COLONNE" type="string" required="true">
    <cfargument name="SEARCH_TEXT" type="string" required="true">
	<cfargument name="ORDER_BY_TEXTE_COLONNE" type="string" required="true">
	<cfargument name="ORDER_BY_TEXTE" type="string" required="true">
	
    <cfargument name="OFFSET" type="numeric" required="true">
    <cfargument name="LIMIT" type="numeric" required="true">

	

	<cfset perimetre = SESSION.PERIMETRE.TYPE_PERIMETRE>
	<cfset p_result = Evaluate("getPaginateListeFacture#perimetre#(datedebut,
													datefin,
													operateurid,
													SEARCH_TEXT_COLONNE,
													SEARCH_TEXT,
													ORDER_BY_TEXTE_COLONNE,
													ORDER_BY_TEXTE,
													OFFSET,
													LIMIT)")>
	
	<cfreturn p_result>
	
</cffunction>

<cffunction name="getPaginateListeFactureGroupe" access="remote" returntype="query">
	
	<cfargument name="datedebut" type="date" required="true">
	<cfargument name="datefin" type="date" required="true">
	<cfargument name="operateurid" type="string" required="true">
	<cfargument name="SEARCH_TEXT_COLONNE" type="string" required="true">
    <cfargument name="SEARCH_TEXT" type="string" required="true">
	<cfargument name="ORDER_BY_TEXTE_COLONNE" type="string" required="true">
	<cfargument name="ORDER_BY_TEXTE" type="string" required="true">
	
    <cfargument name="OFFSET" type="numeric" required="true">
    <cfargument name="LIMIT" type="numeric" required="true">

	
	<cfset idracine_master = session.perimetre.idracine_master>
	<cfset idgroupe_client = session.perimetre.id_groupe>
	<cfset LANG = session.user.globalization>
	
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m36.ListeFacuresRacine">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#idracine_master#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" value="#idgroupe_client#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_datedebut" value="#lsdateFormat(datedebut,'yyyy/mm/dd')#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_datefin" value="#lsdateFormat(datefin,'yyyy/mm/dd')#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" value="#operateurid#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_ORDER_BY_TEXTE_COLONNE" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_ORDER_BY_TEXTE" value="#ORDER_BY_TEXTE#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_SEARCH_TEXT_COLONNE" value="#SEARCH_TEXT_COLONNE#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_SEARCH_TEXT" value="#SEARCH_TEXT#" null="false">        
	    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_OFFSET" value="#OFFSET#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_LIMIT" value="#LIMIT#" null="false">   
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_LANG" value="#LANG#" null="false">         
	    <cfprocresult name="p_result">
    </cfstoredproc>
	
	<cfreturn p_result>
	
</cffunction>

<cffunction name="getPaginateListeFactureGroupeLigne" access="remote" returntype="query">
	
	<cfargument name="datedebut" type="date" required="true">
	<cfargument name="datefin" type="date" required="true">
	<cfargument name="operateurid" type="string" required="true">
	<cfargument name="SEARCH_TEXT_COLONNE" type="string" required="true">
    <cfargument name="SEARCH_TEXT" type="string" required="true">
	<cfargument name="ORDER_BY_TEXTE_COLONNE" type="string" required="true">
	<cfargument name="ORDER_BY_TEXTE" type="string" required="true">
	
    <cfargument name="OFFSET" type="numeric" required="true">
    <cfargument name="LIMIT" type="numeric" required="true">

	
	<cfset idracine_master = session.perimetre.idracine_master>
	<cfset idgroupe_client = session.perimetre.id_perimetre>
	<cfset LANG = session.user.globalization>
	
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m36.ListeFacuresPerimetre">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#idracine_master#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" value="#idgroupe_client#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_datedebut" value="#lsdateFormat(datedebut,'yyyy/mm/dd')#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_datefin" value="#lsdateFormat(datefin,'yyyy/mm/dd')#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" value="#operateurid#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_ORDER_BY_TEXTE_COLONNE" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_ORDER_BY_TEXTE" value="#ORDER_BY_TEXTE#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_SEARCH_TEXT_COLONNE" value="#SEARCH_TEXT_COLONNE#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_SEARCH_TEXT" value="#SEARCH_TEXT#" null="false">        
	    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_OFFSET" value="#OFFSET#" null="false">
	    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_LIMIT" value="#LIMIT#" null="false">   
	    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_LANG" value="#LANG#" null="false">         
	    <cfprocresult name="p_result">
    </cfstoredproc>
	
	<cfreturn p_result>
	
</cffunction>

<cffunction name="getListeOperateur" access="remote" returntype="query">
	
	<cfset idgroupe_client = session.perimetre.id_groupe>
	
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M36.ListeOperateursPerimetre">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" value="#idgroupe_client#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	
	<cfreturn p_result>
	
</cffunction>
</cfcomponent>