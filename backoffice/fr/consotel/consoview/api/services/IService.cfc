<cfinterface displayName="fr.consotel.consoview.api.services.IService">
	<cffunction access="public" name="initInstance" returntype="fr.consotel.consoview.api.services.IService" hint="Implicit Initializer : Explicit Invocation Only">
	</cffunction>
	
	<cffunction access="public" name="invokeMethod" returntype="any">
		<cfargument name="method" type="string" required="true">
		<cfargument name="parameters" type="struct" required="true">
	</cffunction>
</cfinterface>