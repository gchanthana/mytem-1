﻿<cfcomponent displayname="Zenprise" hint="Api qui traite ZenPrise" output="true">
	<cfset VARIABLES["ZDM_WEBSERVICEURL"]="http://192.168.1.100:83/zdm/services/EveryWanDevice?WSDL">
	<cfset VARIABLES["ZDM_WEBSERVICEURLV2"]="http://192.168.1.100:83/zdm/services/EveryWanDevice">
	<cfset VARIABLES["ZDM_WS_EVERYWANPACKAGE"]="http://192.168.1.100:83/zdm/services/EveryWanPackage?wsdl">
	
	<cfset VARIABLES["ZDM_USER"]="axel">
	<cfset VARIABLES["ZDM_PWD"]="consotel">
	<cfset VARIABLES["AUTH_STATUS"]="FALSE">

	<cffunction name="lockTerm" access="public" hint="Verrouille un terminal" returntype="Any" >
		<cfargument name="IMEI" type="string">
		<cfargument name="SERIAL" type="string" >
		<cfargument name="PIN" type="string">
		<cfargument name="WAIT" type="numeric">
		
		<cflog text="IMEI = #arguments.IMEI# - SERIAL = #arguments.SERIAL# - PIN = #arguments.PIN# - WAIT = #arguments.WAIT#">
		
		<cfset webService=createObject("webservice","#VARIABLES["ZDM_WEBSERVICEURL"]#")>
		<cfset webService.setUsername("#VARIABLES["ZDM_USER"]#")>
		<cfset webService.setPassword("#VARIABLES["ZDM_PWD"]#")>
		
		<cfset result=webService.getAllDevices()>
		<cfset call=webService._getCall()>
		<cfset transport=call.getTransportForProtocol("http")>
		<cfset transport.setUrl("#VARIABLES["ZDM_WEBSERVICEURLV2"]#")>

		<cfset call2=webService._createCall()>
		<cfset call2.setOperationName("lockDevice")>
		<cfset call2.setUsername("#VARIABLES["ZDM_USER"]#")>
		<cfset call2.setPassword("#VARIABLES["ZDM_PWD"]#")>
		<cfset call2.setTransport(transport)>
		
		<cfif structKeyExists(arguments,"SERIAL")>
			<cfif arguments.SERIAL eq "">
				<cflog text="serial isDefined et ''">
				<cfset serialValue = createObject("java","java.lang.Void")>
			<cfelse>
				<cflog text="serial isDefined et #arguments.SERIAL#">
				<cfset serialValue = arguments.SERIAL>
			</cfif>
		<cfelse>
			<cflog text="serial not isDefined">
			<cfset serialValue = createObject("java","java.lang.Void")>
		</cfif>
		
		<cfif structKeyExists(arguments,"IMEI")>
			<cfif arguments.IMEI eq "">
				<cflog text="IMEI isDefined et ''">
				<cfset imeiValue = createObject("java","java.lang.Void")>
			<cfelse>
				<cflog text="IMEI isDefined et #arguments.IMEI#">
				<cfset imeiValue = arguments.IMEI>
			</cfif>
		<cfelse>
			<cflog text="IMEI not isDefined">
			<cfset imeiValue = createObject("java","java.lang.Void")>
		</cfif>
		
		<cfset longVal=createObject("java","java.lang.Long").init("#arguments.WAIT#")>
		<cfset pinValue = arguments.PIN>
		<cfset arrArgs = arrayNew(1)>
		<cfset arrayAppend(arrArgs,serialValue)>
		<cfset arrayAppend(arrArgs,imeiValue)>
		<cfset arrayAppend(arrArgs,pinValue)>
		<cfset arrayAppend(arrArgs,longVal)>
		
		<cfmail server="mail.consotel.fr" port="26" from="ApiZenPrise@consotel.fr" to="olivier.boulard@consotel.fr" subject="dump arrArgs" type="text/html" >
			<cfdump var="#arrArgs#">
		</cfmail>
		
		<cflog text="Invoke : IMEI = #imeiValue# - SERIAL = #serialValue# - PIN = #pinValue# - WAIT = #longVal#">
		
		<cfset soapEnv=call2.invoke(arrArgs)>
		<cfset deploy()>
		<cfreturn soapEnv>

		<!---<cfif not structIsEmpty(res)>
			<cfif structKeyExists(res,"Responseheader")>
				<cfif structKeyExists(res.Responseheader,"Status_Code")>
					<cfswitch expression="#res["Responseheader"]["Status_Code"]#">
						
						<cfcase value="200">
							<cfset lockDeviceObject = xmlSearch(res.Filecontent,"//*[name()='lockDeviceReturn']")>
							<cfdump var="#lockDeviceObject#">
							<cfset lockDeviceReturn = lockDeviceObject[1].XmlText>
						</cfcase>
						
						<cfcase value="500">
							<cfset lockDeviceObject = xmlSearch(res.Filecontent,"//*[name()='faultstring']")>
							<cfdump var="#lockDeviceObject#">
							<cfset lockDeviceReturn = lockDeviceObject[1].XmlText>
						</cfcase>
					</cfswitch>
					<cfreturn lockDeviceReturn>
				<cfelse>
					<cfreturn -3>
				</cfif>
			<cfelse>
				<cfreturn -2> 
			</cfif> 
		<cfelse>
			<cfreturn -1>
		</cfif>--->
	</cffunction>

	<cffunction access="public" name="corporateDataWipeDevice" returntype="Any">
		<cfargument name="IMEI" type="string">
		<cfargument name="SERIAL" type="string" >
		<cfargument name="WAIT" type="numeric" default="30000" >
		
		<cfset webService=createObject("webservice","#VARIABLES["ZDM_WEBSERVICEURL"]#")>
		<cfset webService.setUsername("#VARIABLES["ZDM_USER"]#")>
		<cfset webService.setPassword("#VARIABLES["ZDM_PWD"]#")>
		
		<cfset result=webService.getAllDevices()>
		<cfset call=webService._getCall()>
		<cfset transport=call.getTransportForProtocol("http")>
		<cfset transport.setUrl("#VARIABLES["ZDM_WEBSERVICEURLV2"]#")>

		<cfset call2=webService._createCall()>
		<cfset call2.setOperationName("corporateDataWipeDevice")>
		<cfset call2.setUsername("#VARIABLES["ZDM_USER"]#")>
		<cfset call2.setPassword("#VARIABLES["ZDM_PWD"]#")>
		<cfset call2.setTransport(transport)>
		
		<cfif structKeyExists(arguments,"SERIAL")>
			<cfif arguments.SERIAL eq "">
				<cflog text="serial isDefined et ''">
				<cfset serialValue = createObject("java","java.lang.Void")>
			<cfelse>
				<cflog text="serial isDefined et #arguments.SERIAL#">
				<cfset serialValue = arguments.SERIAL>
			</cfif>
		<cfelse>
			<cflog text="serial not isDefined">
			<cfset serialValue = createObject("java","java.lang.Void")>
		</cfif>
		
		<cfif structKeyExists(arguments,"IMEI")>
			<cfif arguments.IMEI eq "">
				<cflog text="IMEI isDefined et ''">
				<cfset imeiValue = createObject("java","java.lang.Void")>
			<cfelse>
				<cflog text="IMEI isDefined et #arguments.IMEI#">
				<cfset imeiValue = arguments.IMEI>
			</cfif>
		<cfelse>
			<cflog text="IMEI not isDefined">
			<cfset imeiValue = createObject("java","java.lang.Void")>
		</cfif>
		
		<cfset waitValue=createObject("java","java.lang.Long").init(arguments.WAIT)>
		<cfset arrArgs = arrayNew(1)>
		
		<cfset arrayAppend(arrArgs,serialValue)>
		<cfset arrayAppend(arrArgs,imeiValue)>
		<cfset arrayAppend(arrArgs,waitValue)>
		
		<cflog text="Invoke corporateDataWipeDevice: IMEI = #arrArgs[2]# - SERIAL = #arrArgs[1]# - WAIT = #waitValue#">
		
		<cfset soapEnv=call2.invoke(arrArgs)>
		<cfset deploy()>
		<cfreturn soapEnv>
	</cffunction>
	<cffunction access="public" name="wipeDevice" returntype="Any">
		<cfargument name="IMEI" type="string">
		<cfargument name="SERIAL" type="string">
		<cfargument name="ERASEDMEMOMYCARD" type="boolean" default="false">
		<cfargument name="WAIT" type="numeric" default="30000" >
		<cfargument name="Api" type="string" default="ZenPrise">
		
		<cfset webService=createObject("webservice","#VARIABLES["ZDM_WEBSERVICEURL"]#")>
		<cfset webService.setUsername("#VARIABLES["ZDM_USER"]#")>
		<cfset webService.setPassword("#VARIABLES["ZDM_PWD"]#")>
		
		<cfset result=webService.getAllDevices()>
		<cfset call=webService._getCall()>
		<cfset transport=call.getTransportForProtocol("http")>
		<cfset transport.setUrl("#VARIABLES["ZDM_WEBSERVICEURLV2"]#")>

		<cfset call2=webService._createCall()>
		<cfset call2.setOperationName("wipeDevice")>
		<cfset call2.setUsername("#VARIABLES["ZDM_USER"]#")>
		<cfset call2.setPassword("#VARIABLES["ZDM_PWD"]#")>
		<cfset call2.setTransport(transport)>
		
		<cfif structKeyExists(arguments,"SERIAL")>
			<cfif arguments.SERIAL eq "">
				<cflog text="serial isDefined et ''">
				<cfset serialValue = createObject("java","java.lang.Void")>
			<cfelse>
				<cflog text="serial isDefined et #arguments.SERIAL#">
				<cfset serialValue = arguments.SERIAL>
			</cfif>
		<cfelse>
			<cflog text="serial not isDefined">
			<cfset serialValue = createObject("java","java.lang.Void")>
		</cfif>
		
		<cfif structKeyExists(arguments,"IMEI")>
			<cfif arguments.IMEI eq "">
				<cflog text="IMEI isDefined et ''">
				<cfset imeiValue = createObject("java","java.lang.Void")>
			<cfelse>
				<cflog text="IMEI isDefined et #arguments.IMEI#">
				<cfset imeiValue = arguments.IMEI>
			</cfif>
		<cfelse>
			<cflog text="IMEI not isDefined">
			<cfset imeiValue = createObject("java","java.lang.Void")>
		</cfif>
		
		<cfset waitValue=createObject("java","java.lang.Long").init(arguments.WAIT)>
		<cfset erasedValue = arguments.ERASEDMEMOMYCARD>
		<cfset arrArgs = arrayNew(1)>
		<cfset arrayAppend(arrArgs,serialValue)>
		<cfset arrayAppend(arrArgs,imeiValue)>
		<cfset arrayAppend(arrArgs,erasedValue)>
		<cfset arrayAppend(arrArgs,waitValue)>
		
		<!---<cflog text="Invoke corporateDataWipeDevice: IMEI = #arrArgs[2]# - SERIAL = #arrArgs[1]# - WAIT = #waitValue#">--->
		
		<cfset soapEnv=call2.invoke(arrArgs)>
		<cfset deploy()>
		<cfreturn soapEnv>
	</cffunction>
	
	<cffunction access="public" name="deploy" returntype="Any">
		<cfset webService=createObject("webservice","#VARIABLES["ZDM_WS_EVERYWANPACKAGE"]#")>
		<cfset webService.setUsername("#VARIABLES["ZDM_USER"]#")>
		<cfset webService.setPassword("#VARIABLES["ZDM_PWD"]#")>
	   	<cfset result=webService.deploy()>
	</cffunction>

<!---<cfmail server="mail.consotel.fr" port="26" from="ApiZenPrise@consotel.fr" to="olivier.boulard@consotel.fr" subject="dump du WS" type="text/html" >
						<cfdump var="#VARIABLES.ZDM_WS#">
					</cfmail>--->
					
<!--- FAIT APPEL A UNE METHODE D'UN WEBSERVICE VIA UN MESSAGE SOAP --->
	<cffunction access="public" name="callMethodv2" returntype="Any" hint="Méthode création du message SOAP">
		<cfargument name="MethodName" type="string" required="true">
		<cfargument name="arrArgs" type="array" required="true">
		
		<cftry>
			<cfif arguments.MethodName neq ''>
				<cfsavecontent variable="soapBody">
					<cfoutput>
						<soapenv:Envelope xmlns:q0="#VARIABLES["ZDM_WEBSERVICEURLV2"]#" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
						   <soapenv:Body>
						      <q0:#arguments.MethodName#>
							  	<cfloop array="#arrArgs#" index="obj">
									<cfif structKeyExists(obj,"VALUE")>
										<cfif obj.VALUE neq "">
											<q0:#obj.LABEL# xsi:nil="false">#obj.VALUE#</q0:#obj.LABEL#>
										<cfelse>
											<q0:#obj.LABEL# xsi:nil="true"></q0:#obj.LABEL#>
										</cfif>
									<cfelse>
										<q0:#obj.LABEL# xsi:nil="true"></q0:#obj.LABEL#>
									</cfif>
							  	</cfloop>
						      </q0:#arguments.MethodName#>
						   </soapenv:Body>
						</soapenv:Envelope>
					</cfoutput>
				</cfsavecontent>
				<cfoutput>
					SOAP BODY : <br>
					<cfdump var="#soapBody#">
					<br>
				</cfoutput>
				<cfhttp
					url="#VARIABLES["ZDM_WEBSERVICEURLV2"]#"
					method="post"
					result="httpResponse">
					<cfhttpparam
						type="header"
						name="SOAPAction"
						value="#VARIABLES["ZDM_WEBSERVICEURLV2"]#/#arguments.MethodName#"/>
					<cfhttpparam type="header" name="Authorization" value="Basic #toBase64('#VARIABLES["ZDM_USER"]#:#VARIABLES["ZDM_PWD"]#')#">
					<cfhttpparam
						type="header"
						name="accept-encoding"
						value="no-compression"/>
					<cfhttpparam
						type="xml"
						value="#trim( soapBody )#"/>
				</cfhttp>
				<cfreturn httpResponse>
			</cfif>
		<cfcatch type="any">
			<cflog text="callMethod.CFCATCH : #cfcatch.Message#">
		</cfcatch>
		</cftry>
	</cffunction>

<!--- FAIT APPEL A UNE METHODE DU WEBSERVICE CLASSIQUE--->	
	<cffunction access="public" name="callMethod" returntype="Any" hint="Méthode Webservice classique">
		<cfargument name="MethodName" type="string" required="true">
		<cfargument name="arrArgs" type="array" required="true">
		
		<cfset strParam= structNew()>
		
		<cfset resError = arrayNew(1)>
		<cfset resError[1] = "FALSE">
		
		<cftry>
			<cfif VARIABLES.AUTH_STATUS eq "TRUE">
				<cfif arguments.MethodName neq ''>
					<cfif structKeyExists(VARIABLES,"ZDM_WS")>
						<cfset params = "">
						<cfloop index="idx" from="1" to="#arraylen(arrArgs)#">
							<cfset params = params & "arrArgs[#idx#].VALUE" & ",">
						</cfloop>
						
						<cfif not arrayIsEmpty(arrArgs)>
							<cfset params = left(params,len(params)-1)>
						</cfif>
						<cfset instr="VARIABLES.ZDM_WS." & arguments.MethodName & "(" & params & ")">
						<cfdump var="#instr#" label="instr">
						<cfset res = evaluate(instr)>
						<cfdump var="#res#">	
						<cfreturn res>
					<cfelse>
						<cfreturn resError>
					</cfif>
				</cfif>
			<cfelse>
				<cfset response = login()>
				<cfif response neq "TRUE">
					<cfmail server="mail.consotel.fr" port="26" from="ApiZenPRise@consotel.fr" to="olivier.boulard@consotel.fr" subject="Debug callMethod" type="text/html" >
						<cfdump var="#VARIABLES#">
					</cfmail>
					<cfreturn resError>
				<cfelse>
					<cfreturn callMethod(arguments.MethodName, arguments.arrArgs)>
				</cfif> 
			</cfif>
		<cfcatch type="any">
			<cflog text="callMethod.CFCATCH : #cfcatch.Message#">
		</cfcatch>
		</cftry>
	</cffunction>

<!--- SE CONNECTE AU WEBSERVICE ET SE LOGGE --->	
	<cffunction access="private" name="login" returntype="boolean" >
		<cfset response = createInstance()>
		<cfif response eq TRUE>
			<cfset response = performAuth()>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
		<cfreturn response>
	</cffunction>
<!--- CREER UNE INSTANCE DU WEBSERVICE --->	
	<cffunction access="private" name="createInstance" returntype="boolean" >
		<cftry>
			<cfset VARIABLES.ZDM_WS=createObject("webservice",VARIABLES.ZDM_WEBSERVICEURL)>
			<cfreturn TRUE>
			<cfcatch type="any">
				<cfmail server="mail.consotel.fr" port="26" from="ApiZenPRise@consotel.fr" to="olivier.boulard@consotel.fr" subject="Debug createinstance" type="text/html" >
					<cfdump var="#cfcatch#">
				</cfmail>
				<cfreturn FALSE>
			</cfcatch>
		</cftry>
	</cffunction>
<!--- S'AUTHENTIFIE AUPRES DU WEBSERVICE --->
	 <cffunction access="private" name="performAuth" returntype="boolean">
		<cftry>
			<cfset VARIABLES.ZDM_WS.setUsername(VARIABLES.ZDM_USER)>
			<cfset VARIABLES.ZDM_WS.setPassword(VARIABLES.ZDM_PWD)>
			<cfset VARIABLES.AUTH_STATUS=VARIABLES.ZDM_WS.authenticateUser(VARIABLES.ZDM_USER,VARIABLES.ZDM_PWD)>
			<cfcatch type="any">
				<cfreturn FALSE>
			</cfcatch>
		</cftry>
		<cfreturn VARIABLES.AUTH_STATUS>
	</cffunction>
<!--- DECONNECTION DU SERVICE --->	
	<cffunction access="public" name="logout" returntype="void">
		<cfset VARIABLES.ZDM_WS.setPassword("")>
		<cftry>
			<cfset authStatus=VARIABLES.ZDM_WS.authenticateUser(VARIABLES.ZDM_USER,VARIABLES.ZDM_PWD)>
			<cfcatch type="any">
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#CFCATCH.Message#" detail="#CFCATCH.Detail#">
			</cfcatch>
		</cftry>
	</cffunction>
	
	
	
</cfcomponent>