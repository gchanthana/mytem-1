<cfcomponent name="ExportService" alias="fr.consotel.consoview.api.Reporting.export.ExportService">
	<cffunction name="exportQueryToText" access="remote" returntype="void" hint="Limited to 100 columns and 1 000 000 rows MAX">
		<cfargument name="queryToExport" required="true" type="query">
		<cfargument name="filenameWithExtension" required="true" type="String">
		<cfargument name="limitation" required="false" type="Boolean" default="FALSE">
		<cfargument name="format" required="false" type="String" default="CSV">
		<cfset MAX_NB_COLUMNS=100>
		<cfset MAX_NB_ROWS=1000000>
		<cfsetting enablecfoutputonly="true">
		<cfheader name="Content-Disposition" value="attachment;filename=#arguments.filenameWithExtension#">
		<cfcontent type="text/csv">
		<cfset NEW_LINE=CHR(13) & CHR(10)>
		<cfset colList=queryToExport.getColumnList()>
		<cfif (arguments.limitation EQ FALSE) OR ((arrayLen(colList) LT MAX_NB_COLUMNS) AND (queryToExport.recordcount LT MAX_NB_ROWS))>
			<cfset colListCollection=arrayToList(colList,";")>
			<cfoutput>#colListCollection#;#NEW_LINE#</cfoutput>
			<cfloop index="i" from="1" to="#queryToExport.recordcount#">
				<cfloop index="j" from="1" to="#arrayLen(colList)#">
					<cfoutput>#queryToExport[colList[j]][i]#;</cfoutput>
				</cfloop>
				<cfoutput>#NEW_LINE#</cfoutput>
			</cfloop>
		<cfelse>
			<cfif arrayLen(colList) GTE MAX_NB_COLUMNS>
				<cfoutput>LIMITATION#NEW_LINE#Le nombre de colonnes du rapport depasse la limite autorisee (#MAX_NB_COLUMNS#)</cfoutput>
			<cfelseif queryToExport.recordcount GTE MAX_NB_ROWS>
				<cfoutput>LIMITATION#NEW_LINE#Le nombre de lignes du rapport depasse la limite autorisee (#MAX_NB_ROWS#)</cfoutput>
			</cfif>
		</cfif>
	</cffunction>
	
	<cffunction name="queryToXml" access="remote" returntype="xml" hint="No limitation for query size">
		<cfargument name="sourceQuery" required="true" type="query">
		<cfargument name="ROWSET_TAG" required="false" type="string" default="ROWSET">
		<cfargument name="ROW_TAG" required="false" type="string" default="ROW">
		<cfset colArray=arguments.sourceQuery.getColumnList()>
		<cfxml variable="xmlQueryData">
			<cfoutput><#arguments.ROWSET_TAG#></cfoutput>
				<cfloop index="i" from="1" to="#arguments.sourceQuery.recordcount#">
				<cfoutput><#arguments.ROW_TAG#></cfoutput>
					<cfloop index="j" from="1" to="#arrayLen(colArray)#">
						<cfoutput><#colArray[j]#>#arguments.sourceQuery[colArray[j]][i]#</#colArray[j]#></cfoutput>
					</cfloop>
				<cfoutput></#arguments.ROW_TAG#></cfoutput>
				</cfloop>
			<cfoutput></#arguments.ROWSET_TAG#></cfoutput>
		</cfxml>
		<cfreturn xmlQueryData>
	</cffunction>
	
	<cffunction name="queryToXmlString" access="remote" returntype="string" hint="No limitation for query size">
		<cfargument name="sourceQuery" required="true" type="query">
		<cfreturn toString(queryToXml(arguments.sourceQuery))>
	</cffunction>
	
	<!--- 
	<cffunction name="concatToXmlQuery" access="remote" returntype="xml" hint="No limitation for query size/ Alter xmlSourceQuery by adding xml children">
		<cfargument name="xmlSourceQuery" required="true" type="xml">
		<cfargument name="targetQuery" required="true" type="query">
		<cfargument name="SOURCE_ROWSET_TAG" required="true" type="string" hint="ROWSET">
		<cfargument name="TARGET_ROWSET_TAG" required="true" type="string" hint="ROWSET2">
		<cfargument name="TARGET_ROW_TAG" required="true" type="string" hint="ROW2">
		<cfset childcount=arrayLen(xmlSourceQuery["#arguments.SOURCE_ROWSET_TAG#"].xmlChildren)>
		<cfset arguments.xmlSourceQuery["#arguments.SOURCE_ROWSET_TAG#"].xmlChildren[childcount+1]=
								xmlElemNew(arguments.xmlSourceQuery,arguments.TARGET_ROWSET_TAG)>
		<cfset parentNode=arguments.xmlSourceQuery["#arguments.SOURCE_ROWSET_TAG#"].xmlChildren[childcount+1]>
		<cfset colArray=arguments.targetQuery.getColumnList()>
		<cfloop index="i" from="1" to="#arguments.targetQuery.recordcount#">
			<cfset parentNode.xmlChildren[i]=xmlElemNew(arguments.xmlSourceQuery,arguments.TARGET_ROW_TAG)>
			<cfset tmpRowNode=parentNode.xmlChildren[i]>
			<cfloop index="j" from="1" to="#arrayLen(colArray)#">
				<cfset tmpRowNode.xmlChildren[j]=xmlElemNew(arguments.xmlSourceQuery,colArray[j])>
				<cfset tmpRowNode.xmlChildren[j].xmlText=arguments.targetQuery[colArray[j]][i]>
			</cfloop>
		</cfloop>
		<cfreturn arguments.xmlSourceQuery>
	</cffunction>
	 --->
</cfcomponent>
