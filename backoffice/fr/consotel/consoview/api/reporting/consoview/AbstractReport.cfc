<cfcomponent name="AbstractReport" alias="fr.consotel.consoview.api.Reporting.consoview.AbstractReport">
	<cfinvoke component="fr.consotel.consoview.api.ConfigService" method="getDatasourceName" returnvariable="__DATASOURCE__">
	
	<cffunction name="invokeService" access="remote" returntype="any">
		<cfargument name="methodService" required="true" type="string">
		<cfargument name="paramStringList" required="true" type="string" hint="comma separated parameter values">
		<cfset paramArray=listToArray(arguments.paramStringList,',')>
		<cfreturn EVALUATE("#arguments.methodService#(paramArray)")>
	</cffunction>
</cfcomponent>