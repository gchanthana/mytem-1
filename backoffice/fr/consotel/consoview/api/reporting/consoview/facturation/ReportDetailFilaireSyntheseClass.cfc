<cfcomponent name="ReportDetailFilaireSyntheseClass" alias="fr.consotel.consoview.api.Reporting.consoview.facturation.ReportDetailFilaireSyntheseClass"
	extends="fr.consotel.consoview.api.Reporting.consoview.AbstractReport">
	<cffunction name="getXmlData" access="private" returntype="xml">
		<cfargument name="paramArray" required="true" type="array" hint="IDRACINE_MASTER,ID_PERIMETRE,PERIMETRE,TYPE_PERIMETRE,CUR_PERIOD_FIN,PERIODICITE">
		<cfset xmlData="">
		<cfset nbParam=arrayLen(arguments.paramArray)>
		<cfif nbParam EQ 6>
			<cfset queryUtils=createObject("component","fr.consotel.consoview.api.Reporting.export.ExportService")>
			<cfset qFacturation=getFacturation(arguments.paramArray[1],arguments.paramArray[2],arguments.paramArray[3],
														arguments.paramArray[4],arguments.paramArray[5],arguments.paramArray[6])>
			<cfif qFacturation.recordcount GT 0>
				<cfset xmlData=queryUtils.queryToXml(qFacturation)>
			<cfelse>
				<cfset xmlData=getXmlDataTemplate()>
			</cfif>
			<cfset qInternational=getInternational(arguments.paramArray[1],arguments.paramArray[2],arguments.paramArray[4],
																				arguments.paramArray[5],arguments.paramArray[6])>
			<cfset nbRow=arrayLen(xmlData["ROWSET"].xmlChildren)>
			<cfset xmlData["ROWSET"].xmlChildren[nbRow+1]=xmlElemNew(xmlData,"INTERNATIONAL")>
			<cfset internationalRows=xmlData["ROWSET"]["INTERNATIONAL"].xmlChildren>
			<cfset internationalCount=qInternational.recordcount>
			<cfset internationalColsArray=qInternational.getColumnList()>
			<cfloop index="i" from="1" to="#internationalCount#">
				<cfset internationalRows[i]=xmlElemNew(xmlData,"ROW2")>
				<cfloop index="j" from="1" to="#arrayLen(internationalColsArray)#">
					<cfset internationalRows[i].xmlChildren[j]=xmlElemNew(xmlData,internationalColsArray[j])>
					<cfset internationalRows[i].xmlChildren[j].xmlText=qInternational[internationalColsArray[j]][i]>
				</cfloop>
			</cfloop>
		<cfelse>
			<cfset errorMsg="INVALID PARAMETER COUNT - fr.consotel.consoview.api.Reporting.consoview.facturation.ReportDetailFilaireSyntheseClass:getXmlData()">
			<cfxml variable="xmlData">
				<ROWSET><ROW/><ERROR><cfoutput>#errorMsg#</cfoutput></ERROR></ROWSET>
			</cfxml>
		</cfif>
		<cfreturn xmlData>
	</cffunction>
	
	<cffunction name="getFacturation" access="private" returntype="query">
		<cfargument name="IDRACINE_MASTER" required="true" type="numeric"/>
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="PERIMETRE" required="true" type="string"/>
		<cfargument name="TYPE_PERIMETRE" required="true" type="string"/>
		<cfargument name="CUR_PERIOD_FIN" required="true" type="string" hint="format yyyy/mm/dd"/>
		<cfargument name="PERIODICITE" required="true" type="numeric"/>
		<cfset qGetFacturation="">
		<cfif UCase(arguments.TYPE_PERIMETRE) EQ "GROUPE">
	        <cfstoredproc datasource="#__DATASOURCE__#" procedure="PKG_CV_REP_V3.REP1_DETAIL_FILAIRE_SYNT_GRP">
		        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.IDRACINE_MASTER#"/>
		        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ID_PERIMETRE#">
		        <cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CUR_PERIOD_FIN#">
		        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PERIODICITE#">
		        <cfprocresult name="qGetFacturation">
	        </cfstoredproc>
	    <cfelseif UCase(arguments.TYPE_PERIMETRE) EQ "GROUPELIGNE">
	        <cfstoredproc datasource="#__DATASOURCE__#" procedure="PKG_CV_REP_V3.REP1_DETAIL_FILAIRE_SYNT_LIG">
		        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.IDRACINE_MASTER#"/>
		        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ID_PERIMETRE#">
		        <cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CUR_PERIOD_FIN#">
		        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PERIODICITE#">
		        <cfprocresult name="qGetFacturation">
	        </cfstoredproc>
		<cfelse>
			<cfquery datasource="#__DATASOURCE__#" name="qFacturation">
				select -2 as QUERY_NOT_DEFINED from DUAL
			</cfquery>
			<cfreturn qFacturation>
		</cfif>
		<cfset CUR_PERIOD_DEB=lsDateFormat(dateAdd("m",(-1 * arguments.PERIODICITE),parseDateTime(arguments.CUR_PERIOD_FIN)),"yyyy/mm/dd")>
		<cfset L_CUR_PERIOD_DEB=lsDateFormat(dateAdd("m",(-1 * arguments.PERIODICITE),parseDateTime(arguments.CUR_PERIOD_FIN)),"MMMM YYYY")>
		<cfset L_CUR_PERIOD_FIN=lsDateFormat(parseDateTime(arguments.CUR_PERIOD_FIN),"MMMM YYYY")>
		<cfset PREV_PERIOD_FIN=lsDateFormat(dateAdd("m",-1,parseDateTime(CUR_PERIOD_DEB)),"yyyy/mm/dd")>
		<cfset L_PREV_PERIOD_FIN=lsDateFormat(dateAdd("m",-1,parseDateTime(CUR_PERIOD_DEB)),"MMMM YYYY")>
		<cfset L_PREV_PERIOD_DEB=lsDateFormat(dateAdd("m",(-1 * arguments.PERIODICITE),parseDateTime(PREV_PERIOD_FIN)),"MMMM YYYY")>
        <cfset L_PERIODICITE="Mensuelle">
		<cfset LABEL_P=L_CUR_PERIOD_DEB>
		<cfset LABEL_P_1=L_PREV_PERIOD_DEB>
        <cfif arguments.PERIODICITE EQ 1>
	        <cfset L_PERIODICITE="Bimestrielle">
			<cfset LABEL_P=L_CUR_PERIOD_DEB & " à " & L_CUR_PERIOD_FIN>
			<cfset LABEL_P_1=L_PREV_PERIOD_DEB & " à " & L_PREV_PERIOD_FIN>
		</cfif>
		<cfquery name="qFacturation" dbtype="query">
			select	'#arguments.PERIMETRE#' as PERIMETRE, '#arguments.TYPE_PERIMETRE#' as TYPE_PERIMETRE,
					'P-1' as PERIOD, '#LABEL_P_1#' as LIBELLE_PERIOD, '#LABEL_P_1#' as P_1, '#LABEL_P#' as P,
					'#L_PERIODICITE#' as L_PERIODICITE, '#L_PREV_PERIOD_DEB#' as L_PREV_PERIOD_DEB,
					'#L_PREV_PERIOD_FIN#' as L_PREV_PERIOD_FIN, '#L_CUR_PERIOD_DEB#' as L_CUR_PERIOD_DEB, '#L_CUR_PERIOD_FIN#' as L_CUR_PERIOD_FIN,
					TYPE_THEME, SUR_THEME, IDTHEME_PRODUIT, THEME_LIBELLE, ORDRE_AFFICHAGE, MOIS_REPORT,
					NOMBRE_APPEL, DUREE_APPEL, QTE, MONTANT_FINAL as MONTANT_FINAL_PREV, (MONTANT_FINAL * 0) as MONTANT_FINAL, MONTANT_FINAL as MONTANT
			from	qGetFacturation
			where 	mois_report < '#CUR_PERIOD_DEB#'
			UNION ALL
			select	'#arguments.PERIMETRE#' as PERIMETRE, '#arguments.TYPE_PERIMETRE#' as TYPE_PERIMETRE,
					'P' as PERIOD, '#LABEL_P#' as LIBELLE_PERIOD, '#LABEL_P_1#' as P_1, '#LABEL_P#' as P,
					'#L_PERIODICITE#' as L_PERIODICITE, '#L_PREV_PERIOD_DEB#' as L_PREV_PERIOD_DEB,
					'#L_PREV_PERIOD_FIN#' as L_PREV_PERIOD_FIN, '#L_CUR_PERIOD_DEB#' as L_CUR_PERIOD_DEB, '#L_CUR_PERIOD_FIN#' as L_CUR_PERIOD_FIN,
					TYPE_THEME, SUR_THEME, IDTHEME_PRODUIT, THEME_LIBELLE, ORDRE_AFFICHAGE, MOIS_REPORT,
					NOMBRE_APPEL, DUREE_APPEL, QTE, (MONTANT_FINAL * 0) as MONTANT_FINAL_PREV, MONTANT_FINAL as MONTANT_FINAL, MONTANT_FINAL as MONTANT
			from	qGetFacturation
			where	mois_report >= '#CUR_PERIOD_DEB#'
			order by mois_report, type_theme, ordre_affichage
		</cfquery>
		<cfreturn qFacturation>
	</cffunction>
	
	<cffunction name="getInternational" access="private" returntype="query">
		<cfargument name="IDRACINE_MASTER" required="true" type="numeric"/>
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="TYPE_PERIMETRE" required="true" type="string"/>
		<cfargument name="CUR_PERIOD_FIN" required="true" type="string" hint="format yyyy/mm/dd"/>
		<cfargument name="PERIODICITE" required="true" type="numeric"/>
		<cfset qGetInternationalData="">
		<cfif UCase(arguments.TYPE_PERIMETRE) EQ "GROUPE">
	       <cfstoredproc datasource="#__DATASOURCE__#" procedure="PKG_CV_REP_V3.REP2_DETAIL_FILAIRE_SYNT_GRP">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.IDRACINE_MASTER#"/>
		        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ID_PERIMETRE#">
		        <cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CUR_PERIOD_FIN#">
		        <cfprocresult name="qGetInternationalData">
	        </cfstoredproc>
	    <cfelseif UCase(arguments.TYPE_PERIMETRE) EQ "GROUPELIGNE">
	        <cfstoredproc datasource="#__DATASOURCE__#" procedure="PKG_CV_REP_V3.REP2_DETAIL_FILAIRE_SYNT_LIG">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.IDRACINE_MASTER#"/>
		        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ID_PERIMETRE#">
		        <cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CUR_PERIOD_FIN#">
		        <cfprocresult name="qGetInternationalData">
	        </cfstoredproc>
		<cfelse>
			<cfquery datasource="#__DATASOURCE__#" name="qError">
				select -2 as QUERY_NOT_DEFINED from DUAL
			</cfquery>
	        <cfreturn qError>
		</cfif>
		<cfset qInternational="">
		<cfif qGetInternationalData.recordcount GT 12>
			<cfset LAST_DESTINATION_PRODUIT=qGetInternationalData["LIBELLE_PRODUIT"][11]>
			<cfset LAST_DESTINATION_MONTANT=qGetInternationalData["MONTANT_FINAL"][11]>
			<cfquery name="qTop11" dbtype="query" maxrows="11">
				select LIBELLE_PRODUIT, MONTANT_FINAL from qGetInternationalData order by montant_final DESC
			</cfquery>
			<cfquery name="qInternational" dbtype="query">
				select LIBELLE_PRODUIT, MONTANT_FINAL from qTop11
				union
				select 'AUTRES (*)', SUM(MONTANT_FINAL) as MONTANT_FINAL from qGetInternationalData
				where MONTANT_FINAL <= #LAST_DESTINATION_MONTANT# and LIBELLE_PRODUIT != '#LAST_DESTINATION_PRODUIT#'
				order by MONTANT_FINAL desc
			</cfquery>
		<cfelse>
			<cfquery name="qInternational" dbtype="query">
				select * from qGetInternationalData order by montant_final DESC
			</cfquery>
		</cfif>
        <cfreturn qInternational>
	</cffunction>
	
	<cffunction name="getXmlDataTemplate" access="private" returntype="xml">
		<cfxml variable="xmlDataTemplate">
			<ROWSET>
				<ROW>
					<PERIMETRE/>
					<PERIOD/>
					<L_PERIODICITE/>
					<L_PREV_PERIOD_DEB/>
					<L_PREV_PERIOD_FIN/>
					<L_CUR_PERIOD_DEB/>
					<L_CUR_PERIOD_FIN/>
					<TYPE_THEME/>
					<SUR_THEME/>
					<IDTHEME_PRODUIT/>
					<THEME_LIBELLE/>
					<ORDRE_AFFICHAGE/>
					<MOIS_REPORT/>
					<NOMBRE_APPEL/>
					<DUREE_APPEL/>
					<QTE/>
					<MONTANT_FINAL_PREV/>
					<MONTANT_FINAL/>
					<MONTANT/>
				</ROW>
			</ROWSET>
		</cfxml>
		<cfreturn xmlDataTemplate>
	</cffunction>
</cfcomponent>
