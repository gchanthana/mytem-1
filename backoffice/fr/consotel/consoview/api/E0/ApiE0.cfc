<cfcomponent name="ApiE0" alias="fr.consotel.consoview.api.E0.ApiE0">
	
	<cffunction name="getIdPeriodeMois" output="true" access="public" returntype="Numeric" hint="Retourne l'IDPeriode_Mois">
           <cfargument name="thisDate" type="date" required="true">
           
		   <cflog text="**************** APIEO : getIdPeriodeMois(#DateFormat(thisDate,'MM/YYYY')#)">
		 
           <cfset tmpDateString = DateFormat(thisDate,'MM/YYYY')>
		   
           <cflog text="**************** APIEO : tmpDateString : #tmpDateString#">
		
           <cfquery datasource="BI_TEST" name="qGetIdperiodeMois"> 
                  SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS
                  FROM E0_AGGREGS
                  WHERE      PERIODE.SHORT_MOIS='#tmpDateString#'
                  ORDER BY IDPERIODE_MOIS
           </cfquery>
           <cfif qGetIdperiodeMois.recordcount EQ 1>
                 <cfset retour =  qGetIdperiodeMois["IDPERIODE_MOIS"][1]>
           <cfelse>
                 <cfset retour =  -1>
           </cfif>
		<cflog text="**************** APIEO : getIdPeriodeMois() retour = #retour#">
		
		<cfreturn retour>
     </cffunction>

    <cffunction name="getCliche" output="true" access="public" returntype="Numeric">
		<cfargument name="idorga" type="numeric" required="true">
		
		<cflog text="**************** APIEO : getCliche(#idorga#)">
		
		<cfquery datasource="E0" name="clicheRslt">
			SELECT idcliche FROM 
			( SELECT c.idcliche,c.idperiode_mois,MAX(c.idperiode_mois) over () MAX_p FROM cliche c WHERE c.idorganisation=#idorga# ) 
			WHERE max_p=idperiode_mois
		</cfquery>
		<cflog text="#val(clicheRslt['IDCLICHE'][1])#">
		<cfreturn val(clicheRslt['IDCLICHE'][1])>
      </cffunction>

	<cffunction name="getNiveauOrga" output="false" access="public" returntype="String">
		<cflog text="*************** APIEO : getNiveauOrga()">
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_consoview_v3.get_numero_niveau">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER"       variable="p_idgroupe_client" value="#Session.perimetre.ID_GROUPE#">              
			<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR"       variable="p_retour">
		</cfstoredproc>
		<cflog text="*************** APIEO : getNiveauOrga() retour = #p_retour#">
		<cfreturn p_retour>
    </cffunction>

	<cffunction name="getNiveauOrga_V2" output="false" access="public" returntype="String">
		<cfargument name="idperimetre" type="numeric">
		<cflog text="*************** APIEO : getNiveauOrga()">
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_consoview_v3.get_numero_niveau">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER"       value="#arguments.idperimetre#">              
			<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR"      variable="p_retour">
		</cfstoredproc>
		<cflog text="*************** APIEO : getNiveauOrga() retour = #p_retour#">
		<cfreturn p_retour>
    </cffunction>

	<cffunction name="getOrga" output="false" access="public" returntype="Numeric">
		<cfargument name="idperimetre" type="numeric" required="true">
		<cflog text="*************** APIEO : getOrga(#idperimetre#)">	
			<cfquery datasource="#SESSION.OFFREDSN#" name="rsltOrga">
				SELECT PKG_M33.GET_ORGA(#idperimetre#) as IDORGA FROM DUAL
			</cfquery>
		<cflog text="*************** APIEO : getNiveauOrga() retour = #val(rsltOrga['IDORGA'][1])#">	
		<cfreturn val(rsltOrga['IDORGA'][1])>
	</cffunction>

	<cffunction name="getIdPeriodeMoisFacturation" output="false" access="public" returntype="Numeric">
		<cfargument name="idracine" 		type="numeric">
		<cfargument name="idracine_master" 	type="numeric">
		<cflog text="*************************** APIEO.getIdPeriodeMoisFacturation(#arguments.idracine#, #idracine_master#)">
		<cfquery datasource="#SESSION.OFFREDSN#" name="rslt">
			SELECT MAX(idperiode_mois) as idperiode_mois
			FROM inventaire_periode ip,groupe_client_ref_client gcrc
			WHERE ip.idref_client=gcrc.idref_client
			AND gcrc.idgroupe_client=#arguments.idracine#
   			AND ip.idracine_master=#arguments.idracine_master#
		</cfquery>  
		<cfif rslt.recordcount EQ 1>
                 <cfreturn val(rslt['idperiode_mois'][1])>	
           <cfelse>
                 <cfset retour =  -1>
           </cfif>
	</cffunction>

	<cffunction name="getLibellePerimetre" output="false" access="public" returntype="string">
		<cfargument name="idperimetre" type="numeric">
		<cflog text="*************** APIEO : getLibellePerimetre(#SESSION.PERIMETRE.ID_PERIMETRE#,#arguments.idperimetre#)">
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m33.getStructure">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER"       value="#SESSION.PERIMETRE.ID_PERIMETRE#">              
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER"       value="#arguments.idperimetre#">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfset ret =result['CHEMIN']>
		<cflog text="*************** APIEO : getLibellePerimetre = #ret#">
		<cfreturn ret>
    </cffunction>
	
</cfcomponent>
