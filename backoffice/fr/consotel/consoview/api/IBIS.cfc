<cfcomponent displayname="fr.consotel.consoview.api.IBIS" extends="fr.consotel.consoview.api.ApiConsoView" hint="Provides access to a set of modules and services">
	<!--- (Auteur : Cedric)
		Inherited Methods :
			- (remote) invokeService() : Invokes a service in a module. Returns TRUE upon successful
			- (remote) cloneParameters() : Returns a deep copy of default parameters value to pass to invokeService
	
		List of MODULES and SERVICES :
			- IBIS :
				- scheduleReport : BIP Report Scheduling through FTP with Notification Handling Support (EVENT_TARGET,EVENT_HANDLER,EVENT_TYPE)
				- envoyer1RapportParMail : DELETED (DEPRECATED. Throws error)
		DEFAULT VALUES :
			EVENT_TARGET : EMPTY STRING
			EVENT_TYPE : API
			EVENT_HANDLER : EMPTY STRING
	--->
	
	<!--- EXPLICIT INITIALIZATION --->
	<cfset initInstance()>
	
	<cffunction access="private" name="initInstance" returntype="void" hint="Initialize this instance. Called explicitly by this instance at creation">
		<cfset SUPER.initInstance()>
		<!--- BIP WEBSERVICE INFOS --->
		<cfset VARIABLES.BIP_USER="consoview">
		<cfset VARIABLES.BIP_USER_KEY="public">
		<cfset VARIABLES.WS_FACTORY="fr.consotel.api.ibis.publisher.BipWebServiceFactory">
		<cfset VARIABLES.WS_FACTORY_CMD="fr.consotel.api.ibis.publisher.BipCmdWebServiceFactory">
		<!--- MODULES and SERVICES --->
		<cfset VARIABLES.MODULES=structNew()>
		<cfset VARIABLES.MODULES.IBIS=structNew()>
		<cfset VARIABLES.MODULES.IBIS.CLASS="fr.consotel.api.ibis.publisher.prototype.FtpSchedulingPrototype">
		<cfset VARIABLES.MODULES.IBIS.SERVICES=structNew()>
		<cfset VARIABLES.MODULES.IBIS.SERVICES.scheduleReport="scheduleReport">
		<cfset VARIABLES.MODULES.IBIS.SERVICES.scheduleReportCmd="scheduleReportCmd">
		<cfset VARIABLES.MODULES.IBIS.SERVICES.envoyer1RapportParMail=VARIABLES.MODULES.IBIS.SERVICES.scheduleReport>
	</cffunction>
	
	<cffunction access="private" name="scheduleReport" returntype="any" hint="Schedule a BIP Report and return JOB_ID">
		<cfargument name="scheduleRequest" type="struct" required="true" hint="ScheduleRequest">
		<cfset var wsFactory=createObject("component",VARIABLES.WS_FACTORY)>
		<cfset var bipService=wsFactory.createWebService()>
		<cftry>
			<cfreturn bipService.scheduleReport(ARGUMENTS.scheduleRequest,VARIABLES.BIP_USER,VARIABLES.BIP_USER_KEY)>
			<cfcatch type="any">
				<cftrace category="ERROR" type="error" text="WebService scheduleReport FAILED. #CFCATCH.MESSAGE# #CFCATCH.DETAIL#">
				<cfrethrow />
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="scheduleReportCmd" returntype="any" hint="Schedule a BIP Report and return JOB_ID">
		<cfargument name="scheduleRequest" type="struct" required="true" hint="ScheduleRequest">
		<cfset var wsFactory=createObject("component",VARIABLES.WS_FACTORY_CMD)>
		<cfset var bipService=wsFactory.createWebService()>
		<cftry>
			<cfreturn bipService.scheduleReport(ARGUMENTS.scheduleRequest,VARIABLES.BIP_USER,VARIABLES.BIP_USER_KEY)>
			<cfcatch type="any">
				<cftrace category="ERROR" type="error" text="WebService scheduleReport FAILED. #CFCATCH.MESSAGE# #CFCATCH.DETAIL#">
				<cfrethrow />
			</cfcatch>
		</cftry>
	</cffunction>
	
</cfcomponent>