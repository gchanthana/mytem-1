<cfcomponent output="false">
	<cfset api = createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
	
	<cffunction name="ApiHandler" access="public" returntype="numeric">
		<cfargument name="JOBID" type="Numeric">
		<cfargument name="STATUS" type="string">
		
		<cflog text="********************************************** ApiHandler main : #arguments.JOBID# - #arguments.STATUS#">
		
		<!--- RECUPERATION DU CODE PROCESS A PARTIR DU JOBID --->
		<cfset codeProc = api.getCodeProcess(arguments.JOBID)>
		<cfif Find("-1",codeProc) neq 0>
			<cfreturn -1>
		<cfelse>
			<cftry>
				<cflog text="********************************************** fr.consotel.process.process#codeProc#">			
				<!--- CREATION DE L'OBJET PROCESS ASSOCIE AU CODE PROCESS - FACTORY --->		
				<cfset proc = createObject("Component", "fr.consotel.process.process#codeProc#")>
				<!--- APPEL DE LA FONCTION  --->
				<cfset procRun = proc.run(arguments.JOBID, arguments.STATUS)>
				<cflog text="********************************************** #procRun#">			
				<cfreturn procRun>
			<cfcatch>
				<cfreturn -1>
			</cfcatch>
			</cftry>
		</cfif>
	</cffunction>
</cfcomponent>