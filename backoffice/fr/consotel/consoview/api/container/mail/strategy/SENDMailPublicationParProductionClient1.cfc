<cfcomponent output="false" extends="fr.consotel.consoview.api.container.mail.AbstractMail">
	
	<cffunction name="sendMail" returntype="numeric">
		<cfargument name="infosDoc" type="struct" required="true">
			<cftry>
				<cfset var properties = createObject("component","fr.consotel.consoview.M01.SendMail").getProperties(infosDoc.CODEAPPLICATION,'NULL')>
				<cfset var fromMailing 	= properties['MAIL_EXP_N1']>
				<cfset var bccMailing 	= properties['MAIL_DEV']>
				
				<cfset MYUUID 		= createUUID()>
				<cfset ImportTmp	= "/container/M331/">
				<cfset moduleId 	= "IbIs">
				<cfset serviceId 	= "scheduleReport">
				<cfset rsltApi  	= -4>
				<cfset mailing  	= createObject("component","fr.consotel.consoview.M01.Mailing")>			
				<cfset myVar		= false>
				<cfset time         = formatLocaleTime(infosDoc.DATE_DEMANDE,infosDoc.GLOBALIZATION)>
				<cfset _date         = formatLocaleDate(infosDoc.DATE_DEMANDE,infosDoc.GLOBALIZATION)>

				<cfdirectory action="Create" directory="#ImportTmp##MYUUID#" type="dir" mode="777">
				
				<cfset rsltMail 	= mailing.setMailToDatabase(#infosDoc.APP_LOGINID#,
																	#infosDoc.MAIL_DESTINATAIRE#,
																	#MYUUID#,
																	"M331",
																	"PP",
																	#fromMailing#,
																	#infosDoc.MAIL_DESTINATAIRE#,
																	"#dictionnaireXPublication['#left(infosDoc.GLOBALIZATION,2)#'][1]#",
																	#bccMailing#,
																	"",
																	#infosDoc.CODEAPPLICATION#)>
		
				<cflog text="<br>SENDMailPublicationParProduction - destinataire : #infosDoc.MAIL_DESTINATAIRE# - PUBLICATION PRODUCTION">
				
				<!--- BIP REPORT --->
				<!--- <cfabort showerror="BIP REPORT"> --->
				<cfset parameters["bipReport"] = structNew()>
				<cfset parameters["bipReport"]["xdoAbsolutePath"]	= '/consoview/Mails Container/MailContainer/MailContainer.xdo'>
				<cfset parameters["bipReport"]["xdoTemplateId"]		= 'DISPOAIR-#infosDoc.CODEAPPLICATION#'>
				<cfset parameters["bipReport"]["outputFormat"] 		= "html">
				<cfset parameters["bipReport"]["localization"]		= infosDoc.GLOBALIZATION>
				
				<!--- PARAMÈTRES DE GÉNÉRATION FTP DU RAPPORT BIP --->
				<!--- <cfabort showerror="PARAMÈTRES DE GÉNÉRATION FTP DU RAPPORT BIP"> --->
				<cfset parameters.bipReport.delivery.parameters.ftpUser			= "container">
				<cfset parameters.bipReport.delivery.parameters.ftpPassword		= "container">
				<cfset parameters.bipReport.delivery.parameters.fileRelativePath= "M331/#MYUUID#/mail#MYUUID#.html">
				
				<!--- PARAMÈTRES DE FILTRAGE DES DONNÉES DU RAPPORT BIP --->
				<!--- GESTION DE LA NOTIFICATION DE FIN D'EXÉCUTION DU RAPPORT BIP --->
				<!--- <cfabort showerror="GESTION DE LA NOTIFICATION DE FIN D'EXÉCUTION DU RAPPORT BIP"> --->
				<cfset parameters.EVENT_TARGET 	= MYUUID>
				<cfset parameters.EVENT_TYPE 	= "PP">
				<cfset parameters.EVENT_HANDLER = "M33">
				
				<cfset parameters["bipReport"]["reportParameters"]=structNew()>
				<cfset parameters["bipReport"]["reportParameters"]["NOM_DETAILLE_RAPPORT"] = structNew()>
				<cfset parameters["bipReport"]["reportParameters"]["NOM_DETAILLE_RAPPORT"]["parameterValues"]=[infosDoc.NOM_DETAILLE_RAPPORT]>
				<cfset parameters["bipReport"]["reportParameters"]["DATE_DEMANDE"] = structNew()>
				<cfset parameters["bipReport"]["reportParameters"]["DATE_DEMANDE"]["parameterValues"]=[_date]>
				<cfset parameters["bipReport"]["reportParameters"]["PERIMETRE"] = structNew()>
				<cfset parameters["bipReport"]["reportParameters"]["PERIMETRE"]["parameterValues"]=[infosDoc.PERIMETRE]>
				<cfset parameters["bipReport"]["reportParameters"]["LASTNAME"] = structNew()>
				<cfset parameters["bipReport"]["reportParameters"]["LASTNAME"]["parameterValues"]=[infosDoc.SURNAME]>
				<cfset parameters["bipReport"]["reportParameters"]["FIRSTNAME"] = structNew()>
				<cfset parameters["bipReport"]["reportParameters"]["FIRSTNAME"]["parameterValues"]=[infosDoc.FIRSTNAME]>
				<cfset parameters["bipReport"]["reportParameters"]["HEURE_DEMANDE"] = structNew()>
				<cfset parameters["bipReport"]["reportParameters"]["HEURE_DEMANDE"]["parameterValues"]=[time]>
				
				<cfset api		= createObject("component","fr.consotel.consoview.api.CV")>
				<cfset rsltApi 	= api.invokeService(moduleId, serviceId, parameters)>
	
				<cflog type="information" text=">> #rsltApi# #infosDoc.MAIL_DESTINATAIRE# - PUBLICATION PRODUCTION">
				
			<cfcatch>
				<cfset str = "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail# - PUBLICATION PRODUCTION">
				<cflog text="****************************** #str# **************************">
				
				<cfmail server="mail-cv.consotel.fr" port="25" from="sendMailErreurPublication1@consotel.fr"  to="monitoring@saaswedo.com" failto="monitoring@saaswedo.com" 
						subject="[WARN-RAPPORT]Erreur lors de la génération du mail de publication - PUBLICATION PRODUCTION" type="html">									
					<cfdump var="#ARGUMENTS.infosDoc#" label="Paramètre infosDoc"><br>									
					<cfdump var="#str#" label="Message d'erreur">
				</cfmail>
				
				<cfreturn -1>
			</cfcatch>
			</cftry>
		<cfreturn 1>

	</cffunction>
	
</cfcomponent>