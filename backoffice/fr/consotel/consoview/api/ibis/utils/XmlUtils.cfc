<cfcomponent name="XmlUtils" displayname="fr.consotel.consoview.api.ibis.utils.XmlUtils">
	<cffunction name="getLocalXmlObject" returntype="xml" access="public">
		<cfargument name="localRelavitePath" type="string" required="true">
		<cftry>
			<cfset var localFilePath=expandPath(ARGUMENTS["localRelavitePath"])>
			<cfreturn xmlParse(localFilePath)>
			<cfcatch type="any">
				<cfthrow type="Custom" message="XML UTILS : FAILED PARSE XML" detail="#CFCATCH.MESSAGE#">
				<cfreturn xmlParse("<ERROR>XML UTILS : FAILED PARSE XML</ERROR>")>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>