<cfcomponent name="ReportDefinitionSupport" displayname="fr.consotel.consoview.api.ibis.publisher.report.ReportDefinitionSupport"
	implements="fr.consotel.consoview.api.ibis.publisher.report.IReportDefinition" hint="Implementation of BIP Report Definition. This can be provided to execute report.">
	<cfset VARIABLES["REPORT_ABSOLUTE_PATH"]="NULL">
	<cfset VARIABLES["REPORT_DEFINITION_OBJECT"]="NULL">
	
	<cffunction name="init" access="public" returntype="fr.consotel.consoview.api.ibis.publisher.report.ReportDefinitionSupport" description="Constructor">
		<cfargument name="reportAbsolutePath" required="true" type="any" hint="Report Definition (Java Object)">
		<cfargument name="reportDefinitionStruct" required="true" type="any" hint="Report Definition (Java Object)">
		<cfset VARIABLES["REPORT_ABSOLUTE_PATH"]=ARGUMENTS.reportAbsolutePath>
		<cfset VARIABLES["REPORT_DEFINITION_OBJECT"]=ARGUMENTS.reportDefinitionStruct>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction name="getName" returntype="string" access="public" description="Returns report name">
		<cfif VARIABLES["REPORT_DEFINITION_OBJECT"] NEQ "NULL">
			<cfreturn VARIABLES["REPORT_DEFINITION_OBJECT"].getReportName()>
		<cfelse>
			<cfreturn "NULL">
		</cfif>
	</cffunction>
	
	<cffunction name="getReportAbsolutePath" returntype="string" access="public" description="Returns report absolute path">
		<cfif VARIABLES["REPORT_DEFINITION_OBJECT"] NEQ "NULL">
			<cfreturn VARIABLES["REPORT_ABSOLUTE_PATH"]>
		<cfelse>
			<cfreturn "NULL">
		</cfif>
	</cffunction>
	
	<cffunction name="setParameterValues" access="public" returntype="boolean" description="Set parameter values. Returns TRUE if successful and FALSE otherwise.">
		<cfargument name="paramName" type="string" required="true" hint="Parameter Unique Name">
		<cfargument name="paramValues" type="array" required="true" hint="Parameter Values as an Array">
		<cfset var paramNameValuesArray="NULL">
		<cfif VARIABLES["REPORT_DEFINITION_OBJECT"] NEQ "NULL">
			<cfset paramNameValuesArray=VARIABLES["REPORT_DEFINITION_OBJECT"].getReportParameterNameValues()>
			<cfloop index="i" from="1" to="#arrayLen(paramNameValuesArray)#">
				<cfif UCASE(ARGUMENTS.paramName) EQ UCASE(paramNameValuesArray[i].getName())>
					<cfset paramNameValuesArray[i].setValues(ARGUMENTS.paramValues)>
					<cfreturn TRUE>
				</cfif>
			</cfloop>
			<cfreturn FALSE>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
	</cffunction>
	
	<cffunction name="getReportParameterNameValues" returntype="array" access="public"
				description="Returns Report Parameters Array which is used/provided when executing report. An empty array is returned when fails.">
		<cfif VARIABLES["REPORT_DEFINITION_OBJECT"] NEQ "NULL">
			<cfreturn VARIABLES["REPORT_DEFINITION_OBJECT"].getReportParameterNameValues()>
		<cfelse>
			<cfreturn arrayNew(1)>
		</cfif>
	</cffunction>
	
	<cffunction name="getParametersInfos" access="public" returntype="struct" description="Returns Report Parameters as a CF Structure.">
		<cfset var paramNameValuesArray="NULL">
		<cfset var reportDefinitionInfos=structNew()>
		<cfif VARIABLES["REPORT_DEFINITION_OBJECT"] NEQ "NULL">
			<cfset paramNameValuesArray=VARIABLES["REPORT_DEFINITION_OBJECT"].getReportParameterNameValues()>
			<cfloop index="i" from="1" to="#arrayLen(paramNameValuesArray)#">
				<cfset reportDefinitionInfos[paramNameValuesArray[i].getName()]=structNew()>
				<cfset reportDefinitionInfos[paramNameValuesArray[i].getName()]["MULTI_VALUES_ALLOWED"]=paramNameValuesArray[i].isMultiValuesAllowed()>
				<cfset reportDefinitionInfos[paramNameValuesArray[i].getName()]["VALUES"]=paramNameValuesArray[i].getValues()>
			</cfloop>
			<cfreturn reportDefinitionInfos>
		<cfelse>
			<cfreturn structNew()>
		</cfif>
	</cffunction>
	
	<cffunction name="getTemplateInfos" access="public" returntype="struct" description="Returns Template Infos as a structure which is empty if fails.">
		<cfargument name="templateId" type="string" required="true" hint="Template Unique ID">
		<cfset var tplListInfos="NULL">
		<cfif VARIABLES["REPORT_DEFINITION_OBJECT"] NEQ "NULL">
			<cfset tplListInfos=getTemplateListInfos()>
			<cfif structKeyExists(tplListInfos,"TEMPLATES")>
				<cfloop index="i" from="1" to="#arrayLen(tplListInfos.TEMPLATES)#">
					<cfif UCASE(ARGUMENTS.templateId) EQ UCASE(tplListInfos["TEMPLATES"][i]["TEMPLATE_ID"])>
						<cfreturn tplListInfos["TEMPLATES"][i]>
					</cfif>
				</cfloop>
			</cfif>
			<cfreturn structNew()>
		<cfelse>
			<cfreturn structNew()>
		</cfif>
	</cffunction>
	
	<cffunction name="getTemplateListInfos" access="public" returntype="struct" description="Returns All Templates Infos as a structure which is empty if fails.">
		<cfset var templateArray="NULL">
		<cfset var formatArray="NULL">
		<cfset var reportTemplatesInfos=structNew()>
		<cfif VARIABLES["REPORT_DEFINITION_OBJECT"] NEQ "NULL">
			<!--- ======================================================================================= --->
			<cfset reportTemplatesInfos["DEFAULT"]=structNew()>
			<cfset reportTemplatesInfos["DEFAULT"]["DEFAULT_TEMPLATE_ID"]=VARIABLES["REPORT_DEFINITION_OBJECT"].getDefaultTemplateId()>
			<cfset reportTemplatesInfos["DEFAULT"]["DEFAULT_OUTPUT_FORMAT"]=VARIABLES["REPORT_DEFINITION_OBJECT"].getDefaultOutputFormat()>
			<!--- ======================================================================================= --->
			<cfset templateArray=VARIABLES["REPORT_DEFINITION_OBJECT"].getListOfTemplateFormatsLabelValues()>
			<cfset reportTemplatesInfos["TEMPLATES"]=arrayNew(1)>
			<cfloop index="i" from="1" to="#arrayLen(templateArray)#">
				<cfset formatArray=templateArray[i].getListOfTemplateFormatLabelValue()>
				<cfset reportTemplatesInfos["TEMPLATES"][i]=structNew()>
				<cfset reportTemplatesInfos["TEMPLATES"][i]["TEMPLATE_ID"]=templateArray[i].getTemplateID()>
				<cfset reportTemplatesInfos["TEMPLATES"][i]["TEMPLATE_TYPE"]=templateArray[i].getTemplateType()>
				<cfset reportTemplatesInfos["TEMPLATES"][i]["TEMPLATE_URL"]=templateArray[i].getTemplateURL()>
				<cfset reportTemplatesInfos["TEMPLATES"][i]["FORMATS"]=arrayNew(1)>
				<cfloop index="j" from="1" to="#arrayLen(formatArray)#">
					<cfset reportTemplatesInfos["TEMPLATES"][i]["FORMATS"][j]=structNew()>
					<cfset reportTemplatesInfos["TEMPLATES"][i]["FORMATS"][j]["LABEL"]=formatArray[j].getTemplateFormatLabel()>
					<cfset reportTemplatesInfos["TEMPLATES"][i]["FORMATS"][j]["VALUE"]=formatArray[j].getTemplateFormatValue()>
				</cfloop>
			</cfloop>
			<!--- ======================================================================================= --->
			<cfreturn reportTemplatesInfos>
		<cfelse>
			<cfreturn structNew()>
		</cfif>
	</cffunction>
</cfcomponent>
