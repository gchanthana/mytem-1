<cfcomponent name="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService"
	implements="fr.consotel.consoview.api.ibis.publisher.service.developer.IDeveloperReportService"
	extends="fr.consotel.consoview.api.ibis.publisher.service.AbstractTaskService">
	
	<cffunction name="init" access="public" output="false" returntype="boolean">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfargument name="xdoAbsolutePath" type="string" required="true">
		<cfargument name="appName" type="string" required="false" default="N.D">
		<cfargument name="moduleName" type="string" required="false" default="N.D">
		<cfargument name="codeRapport" type="string" required="false" default="N.D">
		<cfset var xmlTaskRef="NULL">
		<cfset VARIABLES["XML_TASK"]=getDefaultXmlReportTask()>
		<cfset xmlTaskRef=VARIABLES["XML_TASK"]>
		<cfset xmlTaskRef["TASK"]["REPORT_ABSOLUTE_PATH"].xmlText=arguments["xdoAbsolutePath"]>
		<cfset xmlTaskRef["TASK"]["APP_NAME"].xmlText=arguments["appName"]>
		<cfset xmlTaskRef["TASK"]["MODULE_NAME"].xmlText=arguments["moduleName"]>
		<cfset xmlTaskRef["TASK"]["CODE_RAPPORT"].xmlText=arguments["codeRapport"]>
		
		<cfset xmlTaskRef["TASK"]["PARAMETERS"].xmlChildren=arrayNew(1)>
		<cfset xmlTaskRef["TASK"]["PARAMETERS"].xmlChildren[1]=xmlElemNew(xmlTaskRef,"PARAMETER")>
		<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][1].xmlChildren[1]=xmlElemNew(xmlTaskRef,"NAME")>
		<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][1].xmlChildren[2]=xmlElemNew(xmlTaskRef,"TYPE")>
		<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][1].xmlChildren[3]=xmlElemNew(xmlTaskRef,"VALUES")>
		<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][1].xmlChildren[4]=xmlElemNew(xmlTaskRef,"LABEL")>
		<!--- Il faut obligatoirement le parramètre P_IDRACINE pour l'API CONTAINER (cf. Documentation) --->
		<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][1]["NAME"].xmlText="G_IDRACINE">
		<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][1]["LABEL"].xmlText="G_IDRACINE">
		<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][1]["TYPE"].xmlText="CV">
		<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][1]["VALUES"].xmlChildren[1]=xmlElemNew(xmlTaskRef,"VALUE")>
		<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][1]["VALUES"]["VALUE"][1].xmlText=arguments["idRacine"]>
		<cfreturn TRUE>
	</cffunction>

	<cffunction name="getInitInfos" access="public" output="false" returntype="struct">
		<cfset var initInfos=structNew()>
		<cfset initInfos["IS_VALID"]=FALSE>
		<cfif checkXmlTaskInfos() EQ TRUE>
			<cfset initInfos["IS_VALID"]=TRUE>
			<cfset initInfos["INIT_INFOS"]=structNew()>
			<cfset initInfos["INIT_INFOS"]["REPORT_ABSOLUTE_PATH"]=VARIABLES["XML_TASK"]["TASK"]["REPORT_ABSOLUTE_PATH"].xmlText>
			<cfset initInfos["INIT_INFOS"]["APP_NAME"]=VARIABLES["XML_TASK"]["TASK"]["APP_NAME"].xmlText>
			<cfset initInfos["INIT_INFOS"]["MODULE_NAME"]=VARIABLES["XML_TASK"]["TASK"]["MODULE_NAME"].xmlText>
			<cfset initInfos["INIT_INFOS"]["CODE_RAPPORT"]=VARIABLES["XML_TASK"]["TASK"]["CODE_RAPPORT"].xmlText>
		</cfif>
		<cfreturn initInfos>
	</cffunction>
	
	<cffunction name="setOutput" access="public" output="false" returntype="boolean">
		<cfargument name="xdoTemplateId" type="string" required="true">
		<cfargument name="outputFormat" type="string" required="false" default="xml">
		<cfargument name="outputExtension" type="string" required="false" default="xml">
		<cfargument name="outputName" type="string" required="false" default="unknown">
		<cfargument name="jobName" type="string" required="false" default="N.D">
		<cfset var xmlTaskRef="NULL">
		<cfif checkXmlTaskInfos() EQ TRUE>
			<cfset xmlTaskRef=VARIABLES["XML_TASK"]>
			<cfset xmlTaskRef["TASK"]["TEMPLATE_ID"].xmlText=arguments["xdoTemplateId"]>
			<cfset outputAfterRemplace = replaceList(arguments["outputName"],"/","-")>
			<cfset xmlTaskRef["TASK"]["OUTPUT_NAME"].xmlText=outputAfterRemplace>
			<cfset xmlTaskRef["TASK"]["OUTPUT_FORMAT"].xmlText=arguments["outputFormat"]>
			<cfset xmlTaskRef["TASK"]["OUTPUT_EXT"].xmlText=arguments["outputExtension"]>
			<cfset xmlTaskRef["TASK"]["DELIVERY"]["JOB"].xmlText=arguments["jobName"]>
			<cfreturn TRUE>
		</cfif>
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction name="getOutputInfos" access="public" output="false" returntype="struct">
		<cfset var initInfos=structNew()>
		<cfset initInfos["IS_VALID"]=FALSE>
		<cfif checkXmlTaskInfos() EQ TRUE>
			<cfset initInfos["IS_VALID"]=TRUE>
			<cfset initInfos["OUTPUT_INFOS"]=structNew()>
			<cfset initInfos["OUTPUT_INFOS"]["TEMPLATE_ID"]=VARIABLES["XML_TASK"]["TASK"]["TEMPLATE_ID"].xmlText>
			<cfset initInfos["OUTPUT_INFOS"]["OUTPUT_NAME"]=VARIABLES["XML_TASK"]["TASK"]["OUTPUT_NAME"].xmlText>
			<cfset initInfos["OUTPUT_INFOS"]["OUTPUT_FORMAT"]=VARIABLES["XML_TASK"]["TASK"]["OUTPUT_FORMAT"].xmlText>
			<cfset initInfos["OUTPUT_INFOS"]["DELIVERY"]["JOB"]=VARIABLES["XML_TASK"]["TASK"]["DELIVERY"]["JOB"].xmlText>
		</cfif>
		<cfreturn initInfos>
	</cffunction>
	
	<cffunction name="setParameter" access="public" output="false" returntype="boolean">
		<cfargument name="parameterName" type="string" required="true">
		<cfargument name="parameterValues" type="array" required="true">
		<cfargument name="parameterType" type="string" required="false" default="CV">
		<cfargument name="parameterLabel" type="string" required="false" default="Nom Non Spécifié">
		<cfset var xmlTaskRef="NULL">
		<cfset var tmpParamLabel=arguments["parameterLabel"]>
		<cfif checkXmlTaskInfos() EQ TRUE>
			<cfset xmlTaskRef=VARIABLES["XML_TASK"]>
			<cfset nbChild=arrayLen(xmlTaskRef["TASK"]["PARAMETERS"].xmlChildren)>
			<cfloop index="i" from="1" to="#nbChild#">
				<cfif UCASE(xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][i]["NAME"].xmlText) EQ UCASE(arguments.parameterName)>
					<cfloop index="j" from="1" to="#arrayLen(arguments.parameterValues)#">
						<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][i]["VALUES"].xmlChildren=arrayNew(1)>
						<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][i]["VALUES"].xmlChildren[j]=xmlElemNew(xmlTaskRef,"VALUE")>
						<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][i]["VALUES"]["VALUE"][j].xmlText=arguments["parameterValues"][j]>
					</cfloop>
					<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][i]["TYPE"].xmlText=UCASE(arguments["parameterType"])>
					<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][i]["LABEL"].xmlText=tmpParamLabel>
					<cfreturn TRUE>
				</cfif>
			</cfloop>
			<cfset xmlTaskRef["TASK"]["PARAMETERS"].xmlChildren=arrayNew(1)>
			<cfset xmlTaskRef["TASK"]["PARAMETERS"].xmlChildren[nbChild+1]=xmlElemNew(xmlTaskRef,"PARAMETER")>
			<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][nbChild+1].xmlChildren[1]=xmlElemNew(xmlTaskRef,"NAME")>
			<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][nbChild+1].xmlChildren[2]=xmlElemNew(xmlTaskRef,"TYPE")>
			<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][nbChild+1].xmlChildren[3]=xmlElemNew(xmlTaskRef,"VALUES")>
			<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][nbChild+1].xmlChildren[4]=xmlElemNew(xmlTaskRef,"LABEL")>
			<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][nbChild+1]["NAME"].xmlText=arguments["parameterName"]>
			<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][nbChild+1]["LABEL"].xmlText=tmpParamLabel>
			<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][nbChild+1]["TYPE"].xmlText=UCASE(arguments["parameterType"])>
			<cfloop index="j" from="1" to="#arrayLen(arguments.parameterValues)#">
				<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][nbChild+1]["VALUES"].xmlChildren=arrayNew(1)>
				<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][nbChild+1]["VALUES"].xmlChildren[j]=xmlElemNew(xmlTaskRef,"VALUE")>
				<cfset xmlTaskRef["TASK"]["PARAMETERS"]["PARAMETER"][nbChild+1]["VALUES"]["VALUE"][j].xmlText=arguments["parameterValues"][j]>
			</cfloop>
			<cfreturn TRUE>
		</cfif>
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction name="getParametersInfos" access="public" output="false" returntype="struct">
		<cfset var initInfos=structNew()>
		<cfset initInfos["IS_VALID"]=FALSE>
		<cfif checkXmlTaskInfos() EQ TRUE>
			<cfset initInfos["IS_VALID"]=TRUE>
			<cfset initInfos["PARAMETER_INFOS"]=structNew()>
			<cfset nbChild=arrayLen(VARIABLES["XML_TASK"]["TASK"]["PARAMETERS"].xmlChildren)>
			<cfloop index="i" from="1" to="#nbChild#">
				<cfset initInfos["PARAMETER_INFOS"][VARIABLES["XML_TASK"]["TASK"]["PARAMETERS"]["PARAMETER"][i]["NAME"].xmlText]=arrayNew(1)>
				<cfset nbParamValues=arrayLen(VARIABLES["XML_TASK"]["TASK"]["PARAMETERS"]["PARAMETER"][i]["VALUES"].xmlChildren)>
				<cfloop index="j" from="1" to="#nbParamValues#">
					<cfset initInfos["PARAMETER_INFOS"][VARIABLES["XML_TASK"]["TASK"]["PARAMETERS"]["PARAMETER"][i]["NAME"].xmlText][j]=
														VARIABLES["XML_TASK"]["TASK"]["PARAMETERS"]["PARAMETER"][i]["VALUES"]["VALUE"][j].xmlText>
				</cfloop>
			</cfloop>
		</cfif>
		<cfreturn initInfos>
	</cffunction>
	
	<cffunction name="runTask" access="public" output="false" returntype="struct">
		<cfargument name="userIdCv" type="numeric" required="true">
		<cfargument name="idgestionnaire" type="numeric" required="false" default="0">
		<cfreturn SUPER.run(arguments.userIdCv,arguments.idgestionnaire,VARIABLES["XML_TASK"])>
	</cffunction>
	
	<cffunction name="checkXmlTaskInfos" access="private" output="false" returntype="boolean">
		<cfif isDefined("VARIABLES")>
			<cfif structKeyExists(VARIABLES,"XML_TASK")>
				<cfreturn TRUE>
			</cfif>
		</cfif>
		<cfreturn FALSE>
	</cffunction>
</cfcomponent>
