<cfinterface displayName="fr.consotel.consoview.api.ibis.publisher.service.developer.IDeveloperReportService">
	<cffunction name="init" access="public" output="false" returntype="boolean">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfargument name="xdoAbsolutePath" type="string" required="true">
		<cfargument name="appName" type="string" required="false" default="N.D">
		<cfargument name="moduleName" type="string" required="false" default="N.D">
		<cfargument name="codeRapport" type="string" required="false" default="N.D">
	</cffunction>

	<cffunction name="getInitInfos" access="public" output="false" returntype="struct"/>
	
	<cffunction name="setOutput" access="public" output="false" returntype="boolean">
		<cfargument name="xdoTemplateId" type="string" required="true">
		<cfargument name="outputFormat" type="string" required="false" default="xml">
		<cfargument name="outputExtension" type="string" required="false" default="xml">
		<cfargument name="outputName" type="string" required="false" default="unknown">
		<cfargument name="jobName" type="string" required="false" default="N.D">
	</cffunction>
	
	<cffunction name="getOutputInfos" access="public" output="false" returntype="struct"/>
		
	<cffunction name="setParameter" access="public" output="false" returntype="boolean">
		<cfargument name="parameterName" type="string" required="true">
		<cfargument name="parameterValues" type="array" required="true">
		<cfargument name="parameterType" type="string" required="false" default="CV">
		<cfargument name="parameterLabel" type="string" required="false" default="Nom Non Spécifié">
	</cffunction>
	
	<cffunction name="getParametersInfos" access="public" output="false" returntype="struct"/>
	
	<cffunction name="runTask" access="public" output="false" returntype="struct">
		<cfargument name="userIdCv" type="numeric" required="true">
		<cfargument name="idgestionnaire" type="numeric" required="false" default="0">
	</cffunction>
</cfinterface>
