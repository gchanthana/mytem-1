﻿<cfcomponent hint="Adapteur vers Gestion Evenement"
			implements="fr.consotel.consoview.api.ibis.publisher.service.developer.IDeveloperReportService">
	
	<cfset VARIABLES["FTPUSER"] = "container">
	<cfset VARIABLES["FTPPASSWORD"] = "container">
	
	<cfset api= createObject("component","fr.consotel.consoview.api.CV")>
	<cfset parameters=structNew()>
		
	<cffunction name="init" access="public" output="false" returntype="boolean">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfargument name="xdoAbsolutePath" type="string" required="true">
		<cfargument name="appName" type="string" required="false" default="N.D">
		<cfargument name="moduleName" type="string" required="false" default="N.D">
		<cfargument name="codeRapport" type="string" required="false" default="N.D">
		
		<cflog text="ADAPTERCONTAINER . INIT()">
		
		<cfset parameters["bipReport"]["reportParameters"]	= structNew()>
		<cfset parameters["bipReport"]["xdoAbsolutePath"]	= arguments.xdoAbsolutePath>
		<cfset parameters["container"]["idRacine"] = arguments.idRacine>
		<cfset parameters["container"]["appName"] = arguments.appName>
		<cfset parameters["container"]["moduleName"] = arguments.moduleName>
		<cfset parameters["container"]["codeRapport"] = arguments.codeRapport>
		
		<cfreturn TRUE>
	</cffunction>
	
	<cffunction name="setOutput" access="public" output="false" returntype="boolean">	
		<cfargument name="xdoTemplateId" type="string" required="true">
		<cfargument name="outputFormat" type="string" required="false" default="xml">
		<cfargument name="outputExtension" type="string" required="false" default="xml">
		<cfargument name="outputName" type="string" required="false" default="unknown">
		<cfargument name="jobName" type="string" required="false" default="N.D">
		
		<cflog text="ADAPTERCONTAINER . SETOUTPUT(#xdoTemplateId#,#outputFormat#,#outputExtension#;#outputName#,#jobName#)">
		
		<cfset parameters["container"]["outputExtension"] = arguments.outputExtension>
		<cfset parameters["container"]["outputName"] = arguments.outputName>
		<cfset parameters["container"]["jobName"] = arguments.jobName>
		
		<cfset parameters["bipReport"]["xdoTemplateId"]		= arguments.xdoTemplateId>
		<cfset parameters["bipReport"]["outputFormat"] 		= arguments.outputFormat>
		<cfset parameters["bipReport"]["localization"]		= session.user.globalization>
		
		<cfreturn TRUE>
	</cffunction>
	
	<cffunction name="setParameter" access="public" output="false" returntype="boolean">
		<cfargument name="parameterName" type="string" required="true">
		<cfargument name="parameterValues" type="array" required="true">
		<cfargument name="parameterType" type="string" required="false" default="CV">
		<cfargument name="parameterLabel" type="string" required="false" default="Nom Non Spécifié">
		
		<cflog text="ADAPTERCONTAINER . SETPARAMETER(#parameterName#)">
		
		<cfset parameters["bipReport"]["reportParameters"]["#arguments.parameterName#"] = structNew()>
		<cfif arraylen(arguments.parameterValues) gt 0>
			<cfloop from="1" to="#arraylen(arguments.parameterValues)#" index="idx">
				<cfset parameters["bipReport"]["reportParameters"]["#arguments.parameterName#"]["parameterValues"]=arguments.parameterValues>
			</cfloop>
		<cfelse>
			<cfset parameters["bipReport"]["reportParameters"]["#arguments.parameterName#"]["parameterValues"]=arrayNew(1)>
		</cfif>
		<cfreturn TRUE>
	</cffunction>
	<cffunction name="runTask" access="public" output="false" returntype="Struct" >
		<cfargument name="userIdCv" type="numeric" required="true">
		<cfargument name="idgestionnaire" type="numeric" required="false" default="0">

		<cflog text="ADAPTERCONTAINER . RUN()">

		<cftry>
			<cfset MAIL_SERVER="mail-cv@consotel.fr">
			<cfset HANDLER_FROM_ADDR="#CGI.SERVER_NAME#@consotel.fr">
			<cfset DEV_SUPPORT_ADDR="monitoring@saaswedo.com">
			<cfset EXCEPTION_SUBJECT_PREFIX="TASK-PROCESS BIP">
			<cflog text="Serveur BackOffice : #CGI.SERVER_NAME#">
			<cflog text="Serveur de mail (MAIL SERVER) : #MAIL_SERVER#">
			<cflog text="Expediteur (HANDLER_FROM_ADDR) : #HANDLER_FROM_ADDR#">
			<cflog text="Adresse Support de dev (DEV_SUPPORT_ADDR) : #DEV_SUPPORT_ADDR#">
			<cflog text="Prefixe du sujet (EXCEPTION_SUBJECT_PREFIX) : #EXCEPTION_SUBJECT_PREFIX#">
			<!--- ********************************************************************************** --->
			<cfset userId="consoview">
			<cfset userPwd="public">
			<cfset PROCESS_ID=21>
			<cfset nomDuFichierOutput=parameters["container"]["outputName"]>
			<cfset fileExtension=parameters["container"]["outputExtension"]>
			<cfset CODE_RAPPORT=parameters["container"]["codeRapport"]>
			<cfset idracine=parameters["container"]["idRacine"]>
			<cfset MODULE_NAME=parameters["container"]["moduleName"]>
			<cfset BIP_SERVER=SESSION.bip_server_name>
			<cfset APP_NAME=parameters["container"]["appName"]>
			<cfset containerService=createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
			<cfset uuid=containerService.GETUuid()>
			<cfset parameters.bipReport.delivery.parameters.ftpUser=VARIABLES["FTPUSER"]>
			<cfset parameters.bipReport.delivery.parameters.ftpPassword=VARIABLES["FTPPASSWORD"]>
			<cfset parameters.bipReport.delivery.parameters.fileRelativePath="#uuid#">
			<cfset resultStruct = structNew()>
			<cfset resultStruct["UUID"]=uuid>
			<cfset addDocStatus=containerService.ADDDocumentTableContainer(nomDuFichierOutput,REPLACE(nomDuFichierOutput," ","_","all"),-1,CODE_RAPPORT,userIdCv,idracine,MODULE_NAME,fileExtension,lsDateFormat(NOW(),"yyyy/mm/dd"),BIP_SERVER,APP_NAME,0,1,-1,-1,session.CODEAPPLICATION)>
			<cfif (addDocStatus NEQ -1) AND (uuid EQ addDocStatus)>
				<cftry>
					<cfset api= createObject("component","fr.consotel.consoview.api.CV")>
					<cfset parameters.EVENT_TARGET = uuid>
					<cfset parameters.EVENT_TYPE = "">
					<cfset parameters.EVENT_HANDLER = "M61">
					<cfset jobId = api.invokeService("IbIs","scheduleReport",parameters,"olivier")>
					<cfset resultStruct["JOBID"]=jobId>
					<cfset addJobIdStatus=containerService.ADDjobidToDocument(uuid,jobId)>
					<cfif addJobIdStatus EQ 1>
						<cflog text="-***************************** jobid = #jobId# - process_id = #PROCESS_ID# ">
						<cfreturn resultStruct>
					<cfelse>
						<cfset errorMsg="ERREUR - ADDjobidToDocument : " & setCodeStatus>
						<cflog text="#errorMsg#">
						<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
							type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
							<cfoutput>#errorMsg#</cfoutput>
						</cfmail>
					</cfif>
				<cfcatch type="any">
					<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
							type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
							<cfdump var="#CFCATCH#" label="ERREUR : scheduleReport (API BIP)">
					</cfmail>
				</cfcatch>
				</cftry>
		</cfif>
		<cfcatch type="any">
			<cflog text="====== (BIP) API RAPPORT (AbstractTaskService) - EXCEPTION CATCHED - Server : #CGI.SERVER_NAME# - Remote : #CGI.REMOTE_HOST# ======">
			<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
				type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
				<cfoutput>
					<b>Dump du CFCATCH :</b><br />
				</cfoutput>
				<cfdump var="#CFCATCH#" expand="true">
			</cfmail>
			<cfset resultStruct["JOBID"]=-1>
			<cfset resultStruct["UUID"]=-1>
			<cfreturn resultStruct/>
		</cfcatch>
		</cftry>
	</cffunction>
	
<!--- PAR RESPECT DE L'INTERFACE --->
	<cffunction name="getInitInfos" access="public" output="false" returntype="struct">
	</cffunction>
	<cffunction name="getOutputInfos" access="public" output="false" returntype="struct">
	</cffunction>
	<cffunction name="getParametersInfos" access="public" output="false" returntype="struct">
	</cffunction>
</cfcomponent>