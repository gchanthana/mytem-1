<cfcomponent name="PublisherService" implements="fr.consotel.consoview.api.ibis.publisher.service.IPublisherService"
										displayname="fr.consotel.consoview.api.ibis.publisher.service.PublisherService">
	<cfset VARIABLES["BIP_WEBSERVICE_WSDL"]="NULL">
	
	<cffunction name="init" returntype="fr.consotel.consoview.api.ibis.publisher.service.IPublisherService" access="public" description="Constructor">
		<cfset var appProxy=createObject("component","fr.consotel.consoview.api.ibis.admin.ApplicationProxy")>
		<cfset var configProxy=appProxy.getConfigurationMapProxy()>
		<cfset VARIABLES["BIP_WEBSERVICE_WSDL"]=configProxy.getPublisherWSDL()>
		<cfreturn THIS>
	</cffunction>

	<cffunction name="getReportDefaultXmlConfig" returntype="XML" access="public" description="Return report default XML config which is created using default report parameters.">
		<cfargument name="reportAbsolutePath" type="string" required="true" hint="Report Definition (Parameters,Templates,etc...)">
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
		<cfset var xmlConfig=xmlParse("<EMPTY></EMPTY>")>
		<cfset var reportDefinition="NULL">
		<cfset var paramNameValuesArray="NULL">
		<cfset var tmpStringValues="NULL">
		<cfif VARIABLES["BIP_WEBSERVICE_WSDL"] NEQ "NULL">
			<cfset bipService=createObject("webservice",VARIABLES["BIP_WEBSERVICE_WSDL"])>
			<cfif bipService.hasReportAccess(ARGUMENTS.reportAbsolutePath,ARGUMENTS.userId,ARGUMENTS.pwd)>
				<cftry>
					<cfset reportDefinition=bipService.getReportDefinition(ARGUMENTS.reportAbsolutePath,ARGUMENTS.userId,ARGUMENTS.pwd)>
					<cfset xmlConfig=getXmlConfigTemplate()>
					<cfset xmlConfig["CONFIG"]["REPORT"]["ABSOLUTE_PATH"].xmlText=ARGUMENTS.reportAbsolutePath>
					<cfset xmlConfig["CONFIG"]["REPORT"]["TEMPLATE_ID"].xmlText=reportDefinition.getDefaultTemplateId()>
					<cfset xmlConfig["CONFIG"]["REPORT"]["OUTPUT_FORMAT"].xmlText=reportDefinition.getDefaultOutputFormat()>
					<cfset paramNameValuesArray=reportDefinition.getReportParameterNameValues()>
					<cfloop index="i" from="1" to="#arrayLen(paramNameValuesArray)#">
						<cfset xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"].xmlChildren[i]=xmlElemNew(xmlConfig,"PARAMETER")>
						<cfset xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i].xmlChildren[1]=xmlElemNew(xmlConfig,"NAME")>
						<cfset xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["NAME"].xmlText=paramNameValuesArray[i].getName()>
						<cfset xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i].xmlChildren[2]=xmlElemNew(xmlConfig,"MULTI_VALUES")>
						<cfset xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["MULTI_VALUES"].xmlText=paramNameValuesArray[i].isMultiValuesAllowed()>
						<cfset xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i].xmlChildren[3]=xmlElemNew(xmlConfig,"VALUES")>
						<cfset tmpStringValues=paramNameValuesArray[i].getValues()>
						<cfif isDefined("tmpStringValues")>
							<cfloop index="j" from="1" to="#arrayLen(tmpStringValues)#">
								<cfset xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["VALUES"].xmlChildren[j]=xmlElemNew(xmlConfig,"VALUE")>
								<cfset xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["VALUES"]["VALUE"][j].xmlText=tmpStringValues[j]>
							</cfloop>
						</cfif>
					</cfloop>
					<cfcatch type="any">
						<cfthrow type="Custom" message="PUBLISHER ERROR : GET REPORT DEFINITION EXCEPTION" detail="#CFCATCH.MESSAGE#">
					</cfcatch>
				</cftry>
			<cfelse>
				<cfthrow type="Custom" message="PUBLISHER ERROR : INVALID CREDENTIALS" detail="#CFCATCH.MESSAGE#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" message="PUBLISHER ERROR : INVALID WSDL" detail="#CFCATCH.MESSAGE#">
		</cfif>
		<cfreturn xmlConfig>
	</cffunction>
	
	<cffunction name="getDeliveryClass" returntype="string" access="public"
				description="Return corresponding Delivery Definition Component Name OR NULL (as String) if delivery type not found. This is used when instanciating a delivery definition.">
		<cfargument name="deliveryType" type="string" required="true" hint="A delivery type. Available values are : LOCAL,FTP,EMAIL">
		<cfset var componentName="NULL">
		<cfswitch expression="#UCASE(ARGUMENTS.deliveryType)#">
		    <cfcase value="LOCAL">
			    <cfreturn "fr.consotel.consoview.api.ibis.publisher.delivery.local.LocalDeliveryDefinitionSupport">
		    </cfcase>
		    <cfcase value="FTP">
			    <cfreturn "fr.consotel.consoview.api.ibis.publisher.delivery.ftp.FtpDeliveryDefinitionSupport">
		    </cfcase>
		    <cfcase value="EMAIL">
			    <cfreturn "fr.consotel.consoview.api.ibis.publisher.delivery.email.EmailDeliveryDefinitionSupport">
		    </cfcase>
		    <cfdefaultcase>
			    <cfthrow type="Custom" message="PUBLISHER ERROR : DELIVERY TYPE NOT FOUND" detail="#CFCATCH.MESSAGE#">
		    </cfdefaultcase>
		</cfswitch>
		<cfreturn componentName>
	</cffunction>
	
	<cffunction name="setTemplateId" returntype="boolean" access="public" description="Set templateId for XML configuration.">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="templateId" type="string" required="true" hint="Template Id">
		<cftry>
			<cfset xmlConfig["CONFIG"]["REPORT"]["TEMPLATE_ID"].xmlText=ARGUMENTS.templateId>
			<cfreturn TRUE>
			<cfcatch type="any">
				<cfthrow type="Custom" message="PUBLISHER ERROR : UNABLE TO SET TEMPLATE ID" detail="#CFCATCH.MESSAGE#">
			</cfcatch>
		</cftry>
		<cfreturn FALSE>
	</cffunction>

	<cffunction name="setOutputFormat" returntype="boolean" access="public" description="Set output format for XML configuration.">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="outputFormat" type="string" required="true" hint="Output Format">
		<cftry>
			<cfset xmlConfig["CONFIG"]["REPORT"]["OUTPUT_FORMAT"].xmlText=ARGUMENTS.outputFormat>
			<cfreturn TRUE>
			<cfcatch type="any">
				<cfthrow type="Custom" message="PUBLISHER ERROR : UNABLE TO SET OUTPUT FORMAT" detail="#CFCATCH.MESSAGE#">
			</cfcatch>
		</cftry>
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction name="setParameterValues" returntype="boolean" access="public" description="Set values for parameter specified in xmlConfig.">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="parameterName" type="string" required="true" hint="Parameter Name">
		<cfargument name="parameterValues" type="array" required="true" hint="Parameter Values as an Array of String">
		<cftry>
			<cfset nbElements=arrayLen(ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"].xmlChildren)>
			<cfloop index="i" from="1" to="#nbElements#">
				<cfif UCASE(ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["NAME"].xmlText) EQ UCASE(ARGUMENTS.parameterName)>
					<cfset ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["VALUES"]=xmlElemNew(xmlConfig,"VALUES")>
					<cfloop index="j" from="1" to="#arrayLen(ARGUMENTS.parameterValues)#">
						<cfset ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["VALUES"].xmlChildren[j]=xmlElemNew(xmlConfig,"VALUE")>
						<cfset ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["VALUES"]["VALUE"][j].xmlText=ARGUMENTS.parameterValues[j]>
					</cfloop>
					<cfreturn TRUE>
				</cfif>
			</cfloop>
			<cfcatch type="any">
				<cfthrow type="Custom" message="PUBLISHER ERROR : UNABLE TO SET PARAMETER VALUES" detail="#CFCATCH.MESSAGE#">
			</cfcatch>
		</cftry>
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction name="setDeliveryConfig" returntype="boolean" access="public" description="Set delivery options for specified XML configuration.">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="deliveryDefinition" type="fr.consotel.consoview.api.ibis.publisher.delivery.IDeliveryDefinition" required="true" hint="Delivery Definition">
		<cfset var deliveryOptions="NULL">
		<cfset var optionListArray="NULL">
		<cftry>
			<cfset deliveryOptions=ARGUMENTS.deliveryDefinition.getParameterInfos()>
			<cfset deliveryOptions=deliveryOptions[ARGUMENTS.deliveryDefinition.getType()]>
			<cfset ARGUMENTS.xmlConfig["CONFIG"]["DELIVERY"]=xmlElemNew(xmlConfig,"DELIVERY")>
			<cfset ARGUMENTS.xmlConfig["CONFIG"]["DELIVERY"].xmlChildren[1]=xmlElemNew(xmlConfig,"TYPE")>
			<cfset ARGUMENTS.xmlConfig["CONFIG"]["DELIVERY"]["TYPE"].xmlText=ARGUMENTS.deliveryDefinition.getType()>
			<cfset optionListArray=listToArray(structKeyList(deliveryOptions))>
			<cfloop index="i" from="1" to="#arrayLen(optionListArray)#">
				<cfset ARGUMENTS.xmlConfig["CONFIG"]["DELIVERY"].xmlChildren[i+1]=xmlElemNew(xmlConfig,UCASE(optionListArray[i]))>
				<cfset ARGUMENTS.xmlConfig["CONFIG"]["DELIVERY"].xmlChildren[i+1].xmlText=deliveryOptions[optionListArray[i]]>
			</cfloop>
			<cfreturn TRUE>
			<cfcatch type="any">
				<cfthrow type="Custom" message="PUBLISHER ERROR : UNABLE TO SET DELIVERY DEFINITION" detail="#CFCATCH.MESSAGE#">
			</cfcatch>
		</cftry>
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction name="runReport" returntype="any" access="public"  description="Run report and returns report result as a base64 binary object. Returns NULL as String if fails.">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
		<cfif VARIABLES["BIP_WEBSERVICE_WSDL"] NEQ "NULL">
			<cfset bipService=createObject("webservice",VARIABLES["BIP_WEBSERVICE_WSDL"])>
			<cfif bipService.hasReportAccess(ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["ABSOLUTE_PATH"].xmlText,ARGUMENTS.userId,ARGUMENTS.pwd)>
				<cfreturn bipService.runReport(getReportRequest(ARGUMENTS.xmlConfig),ARGUMENTS.userId,ARGUMENTS.pwd)>
			<cfelse>
				<cfthrow type="Custom" message="PUBLISHER ERROR : INVALID CREDENTIALS TO RUN REPORT" detail="#CFCATCH.MESSAGE#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" message="PUBLISHER ERROR : INVALID WSDL TO RUN REPORT" detail="#CFCATCH.MESSAGE#">
		</cfif>
		<cfreturn "NULL">
	</cffunction>
	
	<cffunction name="scheduleReport" returntype="numeric" access="public" description="Schedule report and returns schedule job_id or -1 if fails">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="jobName" type="string" required="true" hint="Publisher User Job Name">
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
		<cfset var scheduleRequest="NULL">
		<cfset var deliveryConfigArray="NULL">
		<cfset var deliveryOptionsArray="NULL">
		<cfif VARIABLES["BIP_WEBSERVICE_WSDL"] NEQ "NULL">
			<cfset bipService=createObject("webservice",VARIABLES["BIP_WEBSERVICE_WSDL"])>
			<cfif bipService.hasReportAccess(ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["ABSOLUTE_PATH"].xmlText,ARGUMENTS.userId,ARGUMENTS.pwd)>
				<cfset scheduleRequest=structNew()>
				<cfset scheduleRequest["reportRequest"]=getReportRequest(ARGUMENTS.xmlConfig)>
				<cfset deliveryXPath="/CONFIG/DELIVERY[position()=1]">
				<cfset deliveryConfigArray=xmlSearch(xmlConfig,deliveryXPath)>
				<cfif arrayLen(deliveryConfigArray) EQ 1>
					<cfset scheduleRequest["deliveryRequest"]=structNew()>
					<cfset deliveryType=deliveryConfigArray[1]["TYPE"].xmlText>
					<cfset scheduleRequest["deliveryRequest"][deliveryType]=structNew()>
					<cfset deliveryOptionsArray=deliveryConfigArray[1].xmlChildren>
					<cfloop index="i" from="1" to="#arrayLen(deliveryOptionsArray)#">
						<cfif UCASE(deliveryOptionsArray[i].xmlName) NEQ "TYPE">
							<cfset scheduleRequest["deliveryRequest"][deliveryType][deliveryOptionsArray[i].xmlName]=deliveryOptionsArray[i].xmlText>
						</cfif>
					</cfloop>
					<cfset xmlConfig["CONFIG"]["JOB"]["JOB_NAME"].xmlText=ARGUMENTS.jobName>
					<cfset scheduleRequest["userJobName"]=xmlConfig["CONFIG"]["JOB"]["JOB_NAME"].xmlText>
					<cfset scheduleRequest["jobLocale"]=replace(SESSION.USER.GLOBALIZATION,"-","_")>
					<cflog text="********************* xmlPublisherService : locale = #replace(SESSION.USER.GLOBALIZATION,"-","_")#">
					<cfset scheduleRequest["schedulePublicOption"]=TRUE>
					<cfset scheduleRequest["scheduleBurstringOption"]=xmlConfig["CONFIG"]["JOB"]["BURST_ENABLED"].xmlText>
					
					<cfset scheduleRequest["notifyWhenSuccess"]=FALSE>
					<cfset scheduleRequest["notifyWhenWarning"]=FALSE>
					<cfset scheduleRequest["notifyWhenFailed"]=FALSE>
					<cfset scheduleRequest["saveDataOption"]=FALSE>
					<cfset scheduleRequest["saveOutputOption"]=FALSE>
					<cfreturn bipservice.scheduleReport(scheduleRequest,arguments.userid,arguments.pwd)>
				<cfelse>
					<cfthrow type="Custom" message="PUBLISHER ERROR : INVALID DELIVERY CONFIGURATION" detail="#CFCATCH.MESSAGE#">
				</cfif>
			<cfelse>
				<cfthrow type="Custom" message="PUBLISHER ERROR : INVALID CREDENTIALS TO SCHEDULE REPORT" detail="#CFCATCH.MESSAGE#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" message="PUBLISHER ERROR : INVALID WSDL TO SCHEDULE REPORT" detail="#CFCATCH.MESSAGE#">
		</cfif>
		<cfreturn -1>
	</cffunction>
	
	<cffunction name="getReportRequest" returntype="struct" access="private" description="Returns a report request object based on provided xml configuration">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfset var reportRequest=structNew()>
		<cfset var paramNameValuesArray=arrayNew(1)>
		<cftry>
			<cfset reportRequest["reportAbsolutePath"]=ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["ABSOLUTE_PATH"].xmlText>
			<cfset reportRequest["attributeLocale"]=ARGUMENTS.xmlConfig["CONFIG"]["JOB"]["LOCALE"].xmlText>
			<cfset reportRequest["attributeTemplate"]=ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["TEMPLATE_ID"].xmlText>
			<cfset reportRequest["attributeFormat"]=ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["OUTPUT_FORMAT"].xmlText>
			<cfset nbElements=arrayLen(ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"].xmlChildren)>
			<cfloop index="i" from="1" to="#nbElements#">
				<cfset paramNameValuesArray[i]=structNew()>
				<cfset paramNameValuesArray[i]["name"]=ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["NAME"].xmlText>
				<cfset paramNameValuesArray[i]["multiValuesAllowed"]=ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["MULTI_VALUES"].xmlText>
				<cfset paramNameValuesArray[i]["values"]=arrayNew(1)>
				<cfset nbValues=arrayLen(ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["VALUES"].xmlChildren)>
				<cfloop index="j" from="1" to="#nbValues#">
					<cfset paramNameValuesArray[i]["values"][j]=ARGUMENTS.xmlConfig["CONFIG"]["REPORT"]["PARAMETERS"]["PARAMETER"][i]["VALUES"]["VALUE"][j].xmlText>
				</cfloop>
			</cfloop>
			<cfset reportRequest["parameterNameValues"]=paramNameValuesArray>
			<cfcatch type="any">
				<cfthrow type="Custom" message="PUBLISHER ERROR : FAILED TO CREATE REPORT REQUEST" detail="#CFCATCH.MESSAGE#">
			</cfcatch>
		</cftry>
		<cfreturn reportRequest>
	</cffunction>
	
	<cffunction name="getXmlConfigTemplate" returntype="XML" access="private" description="Returns an emtpy XML Configuration Template">
		<cfxml variable="xmlConfig">
			<CONFIG>
				<REPORT>
					<ABSOLUTE_PATH></ABSOLUTE_PATH>
					<TEMPLATE_ID></TEMPLATE_ID>
					<OUTPUT_FORMAT></OUTPUT_FORMAT>
					<PARAMETERS>
						<!-- Report Parameters : Array of ParameterNameValues
						<PARAMETER>
							<NAME></NAME>
							<MULTI_VALUES>FALSE</MULTI_VALUES>
							<VALUES><VALUE></VALUE></VALUES>
						</PARAMETER>
						-->
					</PARAMETERS>
				</REPORT>
				<DELIVERY>
					<TYPE></TYPE>
				</DELIVERY>
				<JOB>
					<JOB_NAME></JOB_NAME>
					<BURST_ENABLED>FALSE</BURST_ENABLED>
					<LOCALE><cfoutput>SESSION.USER.GLOBALIZATION#</cfoutput></LOCALE>
				</JOB>
			</CONFIG>
		</cfxml>
		<cfreturn xmlConfig>
	</cffunction>
</cfcomponent>