<cfinterface displayName="fr.consotel.consoview.api.ibis.publisher.IPublisherProxy">
	<cffunction name="getReportDefinition" returntype="fr.consotel.consoview.api.ibis.publisher.report.IReportDefinition"
										description="Returns Report Definition (Name,Parameters,Templates,etc...)">
		<cfargument name="reportAbsolutePath" type="string" required="true" hint="Report Absolute Path">
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
	</cffunction>
</cfinterface>
