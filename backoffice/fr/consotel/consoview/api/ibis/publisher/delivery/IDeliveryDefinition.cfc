<cfinterface displayName="fr.consotel.consoview.api.ibis.publisher.delivery.IDeliveryDefinition">
	<cffunction name="getType" returntype="string"
				description="Returns delivery type (Example : ftpOption,emailOption,localOption)."/>
	
	<cffunction name="setParameter" returntype="boolean" description="Set value for a delivery parameter">
		<cfargument name="parameterName" type="string" required="true" hint="Delivery parameter name">
		<cfargument name="parameterValue" type="string" required="true" hint="Delivery parameter value">
	</cffunction>
	
	<cffunction name="getParameterInfos" returntype="struct"
				description="Returns Delivery Parameters Infos as a struture. This can be provided as delivery option when executing report."/>
</cfinterface>