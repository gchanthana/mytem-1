<cfcomponent name="fr.consotel.consoview.api.ibis.publisher.delivery.local.LocalDeliveryDefinitionSupport" extends="fr.consotel.consoview.api.ibis.publisher.delivery.AbstractDeliveryDefinition">
	<cfset init()>
	<cffunction name="init" returntype="fr.consotel.consoview.api.ibis.publisher.delivery.IDeliveryDefinition" access="public" description="Constructor">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["destination"]="">
		<cfreturn THIS>
	</cffunction>
	
	<cffunction name="getType" returntype="string" access="public"
				description="Returns delivery type localOption (LOCAL DELIVERY OPTION).">
		<cfreturn "localOption">
	</cffunction>
	<!--- 
	<cffunction name="setParameter" returntype="boolean" access="public" description="Set value for a delivery parameter.">
		<cfargument name="parameterName" type="string" required="true" hint="Value is destination for this component">
		<cfargument name="parameterValue" type="string" required="true" hint="Value is remote file absolute path">
		<cfif NOT structIsEmpty(VARIABLES["DELIVERY_OPTIONS"])>
			<cfif UCASE(ARGUMENTS.parameterName) EQ UCASE("destination")>
				<cfset VARIABLES["DELIVERY_OPTIONS"][getType()][ARGUMENTS.parameterName]=ARGUMENTS.parameterValue>
				<cfreturn TRUE>
			<cfelse>
				<cfreturn FALSE>
			</cfif>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
	</cffunction>
	 --->
</cfcomponent>
