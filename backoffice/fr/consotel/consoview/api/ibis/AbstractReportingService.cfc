<cfcomponent displayname="fr.consotel.consoview.api.ibis.AbstractReportingService" implements="fr.consotel.consoview.api.services.IService"
		hint="Abstract Implementation Of Reporting Service. Contains ReportRequest and ScheduleRequest instanciation process.">
	<!--- STATIC PROPERTIES
		VARIABLES["DEFAULT-MAIL-ADMIN"] : Default ADMIN email (monitoring@saaswedo.com)
		VARIABLES["DEFAULT_LOCALE"] : Default Locale (fr-FR)
		VARIABLES["API-CV"] : API ConsoView
		VARIABLES["DELIVERY-FACTORY"] : BIP Webservice DeliveryRequest Factory
		VARIABLES["BIP-WSDL"] : BIP WSDL URL
		VARIABLES["DEFAULT-BIP-USER"] : Default BIP User
		VARIABLES["DEFAULT-BIP-PWD"] : Default BIP PWD
		
		Roadmap :
		- Add support for attributeTimeZone element in ReportRequest definition (PublicReportService_v11)
		- Add support for byPassCache element in ReportRequest definition (PublicReportService_v11)
	 --->
	
	<cffunction access="public" name="initInstance" returntype="fr.consotel.consoview.api.services.IService" hint="Implicit Initializer : Explicit Invocation Only">
		<cflog type="information" text="AbstractReportingService Initialization USING ApiConsoView">
		<cfset VARIABLES["DEFAULT-MAIL-ADMIN"]="monitoring@saaswedo.com">
		<cfset VARIABLES["DEFAULT_LOCALE"]="fr-FR">
		<cfset VARIABLES["API-CV"]=createObject("component","fr.consotel.consoview.api.ApiConsoView")>
		<cfset initParameters=structNew()>
		<cfset initParameters["DEFAULT-MAIL-ADMIN"]=VARIABLES["DEFAULT-MAIL-ADMIN"]>
		<cfset VARIABLES["DELIVERY-FACTORY"]=createObject("component",
				"fr.consotel.consoview.api.ibis.publisher.delivery.AbstractDeliveryFactory").initInstance(initParameters)>
		<!--- 
		<cfset VARIABLES["DELIVERY-FACTORY"]=createObject("component",
				"fr.consotel.consoview.api.ibis.publisher.delivery.AbstractDeliveryFactory").initInstance(
													{"DEFAULT-MAIL-ADMIN"=VARIABLES["DEFAULT-MAIL-ADMIN"]})>
		 --->
		<cfset VARIABLES["BIP-WSDL"]=VARIABLES["API-CV"].getContextInfos("BIP-WSDL")>
		<cfset VARIABLES["BIP-EVENT-HANDLERS"]=VARIABLES["API-CV"].getContextInfos("BIP-EVENT-HANDLERS")>
		<cfset VARIABLES["DEFAULT-BIP-USER"]="consoview">
		<cfset VARIABLES["DEFAULT-BIP-PWD"]="public">
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="invokeMethod" returntype="any">
		<cfargument name="method" type="string" required="true">
		<cfargument name="parameters" type="struct" required="true">
		<cflog type="information" text="AbstractReportingService Performing Method-Service : #arguments.method#">
		<!--- NEXT UPDATE : Store UDF names to avoid access to other UDF --->
		<cfif structKeyExists(VARIABLES,arguments["method"])>
			<!--- *** AVOID MALICIOUS SCRIPT/CODE INJECTION *** --->
			<cfreturn evaluate("#arguments.method#(arguments.parameters)")>
		<cfelse>
			<cflog type="error" text="UNSUPPORTED METHOD VALUE : #arguments.method#">
			<cfabort showerror="UNSUPPORTED METHOD VALUE : #arguments.method#">
			<cfreturn "NULL">
		</cfif>
		<cfreturn VARIABLES>
	</cffunction>
	
	<cffunction access="private" name="createScheduleRequest" returntype="struct" hint="Returns BIP Webservice ScheduleRequest Object">
		<cfargument name="parameters" type="struct" required="true">
		<cfset var scheduleRequest="NULL">
		<cfset var reportRequest=createReportRequest(arguments["parameters"])>
		<cfset var deliveryRequest=VARIABLES["DELIVERY-FACTORY"].createDeliveryRequest(arguments["parameters"])>
		<cfset notifFlag=FALSE>
		<cfif structKeyExists(arguments["parameters"],"enableInternalNotifications") AND isValid("boolean",arguments["parameters"]["enableInternalNotifications"])>
			<cfset notifFlag=arguments["parameters"]["enableInternalNotifications"]>
		</cfif>
		<cftrace type="information" text="(+) ScheduleRequest INTERNAL EMAIL NOTIFICATION : #notifFlag#">
		<!--- BIP Webservice "scheduleBurstingOption" value is "scheduleBurstringOption" --->
		<!--- 
		<cfset scheduleRequest = {"userJobName"=arguments["parameters"]["jobName"],"jobLocale"=reportRequest["attributeLocale"],
			"reportRequest"=reportRequest,"deliveryRequest"=deliveryRequest,"saveDataOption"=FALSE,"saveOutputOption"=FALSE,
			"useUTF8Option"=TRUE,"scheduleBurstringOption"=arguments["parameters"]["enableBurst"],"schedulePublicOption"=TRUE,
			"notifyWhenFailed"=notifFlag,"notifyWhenSuccess"=notifFlag,"notifyWhenWarning"=notifFlag
		}>
		 --->
		<cfset scheduleRequest = structNew()>
		<cfset scheduleRequest["userJobName"]=arguments["parameters"]["jobName"]>
		<cfset scheduleRequest["jobLocale"]=reportRequest["attributeLocale"]>
		<cfset scheduleRequest["reportRequest"]=reportRequest>
		<cfset scheduleRequest["deliveryRequest"]=deliveryRequest>
		<cfset scheduleRequest["saveDataOption"]=FALSE>
		<cfset scheduleRequest["saveOutputOption"]=FALSE>
		<cfset scheduleRequest["useUTF8Option"]=TRUE>
		<cfset scheduleRequest["scheduleBurstringOption"]=arguments["parameters"]["enableBurst"]>
		<cfset scheduleRequest["schedulePublicOption"]=TRUE>
		<cfset scheduleRequest["notifyWhenFailed"]=notifFlag>
		<cfset scheduleRequest["notifyWhenSuccess"]=notifFlag>
		<cfset scheduleRequest["notifyWhenWarning"]=notifFlag>
		
		<cfif structKeyExists(arguments["parameters"],"internalNotificationsTo")>
			<cfset notifTo=VARIABLES["DEFAULT-MAIL-ADMIN"]>
			<cfif isValid("email",arguments["parameters"]["internalNotificationsTo"])>
				<cfset notifTo=arguments["parameters"]["internalNotificationsTo"]>
				<cfset scheduleRequest["notificationTo"]=notifTo>
			<cfelse>
				<cftrace type="warning"
					text="(+) ScheduleRequest [internalNotificationsTo] INVALID ADDRESS : #arguments["parameters"]["internalNotificationsTo"]#">
			</cfif>
		</cfif>
		<cfreturn scheduleRequest>
	</cffunction>

	<cffunction access="private" name="createReportRequest" returntype="struct" hint="Returns BIP Webservice ReportRequest Object">
		<cfargument name="parameters" type="struct" required="true">
		<cfset var reportRequest="NULL">
		<cfset var localization=VARIABLES["DEFAULT_LOCALE"]>
		<!--- Convert arguments.parameters to BIP WebService ArrayOfParamNameValue supported format --->
		<cfset var reportParams=arrayNew(1)>
		<cfset var paramKeyArray=listToArray(structKeyList(arguments["parameters"]["reportParameters"],","),",")>
		<cfset var nbKeys=arrayLen(paramKeyArray)>
		<cfloop index="i" from="1" to="#nbKeys#">
			<cfset reportParams[i]=structNew()>
			<cfset reportParams[i]["name"]=paramKeyArray[i]>
			<cfset reportParams[i]["values"]=arguments["parameters"]["reportParameters"][paramKeyArray[i]]["parameterValues"]>
			<cfset reportParams[i]["multiValuesAllowed"]=(arrayLen(reportParams[i]["values"]) GT 1)>
		</cfloop>
		<cfif structKeyExists(arguments["parameters"],"localization")>
			<cfset localization=arguments["parameters"]["localization"]>
		</cfif>
		<cflog type="information" text="AbstractReportingService Using localization : #localization#">
		<!--- 
		<cfset reportRequest = {"reportAbsolutePath"=arguments["parameters"]["xdoAbsolutePath"],"attributeFormat"=arguments["parameters"]["outputFormat"],
			"attributeTemplate"=arguments["parameters"]["xdoTemplateId"],"parameterNameValues"=reportParams,"attributeLocale"=localization}>
		 --->
		<cfset reportRequest = structNew()>
		<cfset reportRequest["reportAbsolutePath"]=arguments["parameters"]["xdoAbsolutePath"]>
		<cfset reportRequest["attributeFormat"]=arguments["parameters"]["outputFormat"]>
		<cfset reportRequest["attributeTemplate"]=arguments["parameters"]["xdoTemplateId"]>
		<cfset reportRequest["parameterNameValues"]=reportParams>
		<cfset reportRequest["attributeLocale"]=localization>

		<cfreturn reportRequest>
	</cffunction>
	
	<!--- JOB EVENT LISTENER : updates httpNotificationServer value for scheduleRequest
		- Add support for httpNotificationServer element in ScheduleRequest definition (PublicReportService_v11).
		(Related elements : httpNotificationUserName,httpNotificationPassword)
	 --->
	<cffunction access="private" name="addJobEventListener" returntype="void"
			hint="Specifies a listener to handle BIP Scheduler Job Events. Override Existing Value">
		<cfargument name="scheduleRequest" type="struct" required="true" hint="ScheduleRequest to update">
		<cfargument name="eventListernerKey" type="string" required="true" hint="KEY which identifies listener">
		<cfif isValid("struct",VARIABLES["BIP-EVENT-HANDLERS"])>
			<cfif structKeyExists(VARIABLES["BIP-EVENT-HANDLERS"],arguments["eventListernerKey"])>
				<cfset arguments["scheduleRequest"]["httpNotificationServer"]=VARIABLES["BIP-EVENT-HANDLERS"][arguments["eventListernerKey"]]>
				<cflog type="information" text="AbstractReportService using BIP EVENT HANDLER [#VARIABLES["BIP-EVENT-HANDLERS"][arguments["eventListernerKey"]]#]">
			<cfelse>
				<cflog type="error"
					text="UNABLE TO FIND EVENT HANDLER KEY [httpNotificationServer] WITH VALUE : [#arguments["eventListernerKey"]#]">
				<cfabort showerror="ILLEGAL ARGUMENT EXCEPTION - ScheduleRequest[httpNotificationServer] INVALID KEY : SEE LOG FILES FOR DETAILS">
			</cfif>
		<cfelse>
			<cflog type="error"
				text="UNABLE TO UPDATE [httpNotificationServer] - BIP-EVENT-HANDLERS IS NOT VALID STRUCT : [#VARIABLES["BIP-EVENT-HANDLERS"].getClass()#]">
			<cfabort showerror="ILLEGAL OPERATION EXCEPTION - BIP-EVENT-HANDLERS IS NOT VALID STRUCT : SEE LOG FILES FOR DETAILS">
		</cfif>
	</cffunction>
	
	<!--- DOCUMENTATION SUPPORT :
	Method scheduleReport Parameter Key List :
	<cfset parameters = {
		"idRacine"=idRacine,
		"userIdCv"=0,
		"idgestionnaire"=0,
		"xdoAbsolutePath"="/dossiers/mon rapport/mon rapport.xdo",
		"xdoTemplateId"="mon template",
		"outputFormat"="excel",
		"outputExtension"="xls",
		"outputName"=optionnel,
		"jobName"=optionnel,
		"appName"=optionnel,
		"moduleName"=optionnel,
		"codeRapport"=optionnel,
		"reportParameters" = {
			"P_IDMASTER" = {"parameterValues"=[0], "parameterType"="CV", "parameterLabel"="Racine Master"},
			"P_IDRACINE" = {"parameterValues"=[idRacine], "parameterType"="CLIENT", "parameterLabel"="Groupe"},
			"P_TYPE_THEME" = {"parameterValues"=["Consommations"], "parameterType"="CLIENT", "parameterLabel"="Type Thème"},
			"P_SEGMENTS_CHOISIS" = {"parameterValues"=["Fixe","Data"],"parameterType"="CLIENT", "parameterLabel"="Segments Choisis"},
			"ParametreInexistant" = {"parameterValues"=["Paramètre inexistant - Erreur ou Pas ???"], "parameterType"="CV", "parameterLabel"="Inconnu"}
		}
	}>
	 --->
</cfcomponent>
