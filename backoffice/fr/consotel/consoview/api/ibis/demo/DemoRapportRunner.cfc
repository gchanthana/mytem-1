<cfcomponent extends="fr.consotel.consoview.api.ibis.publisher.service.AbstractTaskService" output="false">
	<cffunction name="getDefaultXmlReportTask" access="public" output="false" returntype="xml">
		<cfxml variable="defaultXmlReportTask">
			<TASK>
				<!-- Chemin absolu du rapport BI Publisher -->
				<REPORT_ABSOLUTE_PATH>/CONSOTEL/Rapport Ludo/Inventaire des lignes/Inventaire des lignes/Inventaire des lignes.xdo</REPORT_ABSOLUTE_PATH>
				<!-- Nom du template du rapport -->
				<TEMPLATE_ID>Modele_DATA</TEMPLATE_ID>
				<!-- Format du fichier de sortie (NO CASE) -->
				<OUTPUT_FORMAT>excel</OUTPUT_FORMAT>
				<!-- Nom du fichier output -->
				<OUTPUT_NAME>fichier output</OUTPUT_NAME>
				<!-- Extension du fichier output -->
				<OUTPUT_EXT>xls</OUTPUT_EXT>
				<!-- Nom de l'application -->
				<APP_NAME>ConsoView DEV</APP_NAME>
				<!-- Nom de code du rapport (SANS ESPACES) -->
				<CODE_RAPPORT>INV_LIG_V1</CODE_RAPPORT>
				<!-- Nom de code du rapport -->
				<MODULE_NAME>MODULE RAPPORT-CONTAINER</MODULE_NAME>
				<DELIVERY>
					<!-- Nom de la tache lorsque le rapport est en planification -->
					<JOB>CONTAINER DEMO V1</JOB>
					<!-- Mode de stockage du fichier de sortie (NO CASE) : FTP, LOCAL, EMAIL -->
					<CHANNEL>FTP</CHANNEL>
				</DELIVERY>
				<PARAMETERS>
					<!-- P_IDRACINE est OBLIGATOIRE pour le process CONTAINER -->
					<PARAMETER>
						<NAME>P_IDRACINE_MASTER</NAME>
						<VALUE>3245401</VALUE>
						<TYPE>CV</TYPE>
					</PARAMETER>
					<PARAMETER>
						<NAME>P_IDRACINE</NAME>
						<VALUE>3245401</VALUE>
						<TYPE>CV</TYPE>
					</PARAMETER>
					<PARAMETER>
						<NAME>P_IDORGANISATION</NAME>
						<VALUE>3245401</VALUE>
						<TYPE>CV</TYPE>
					</PARAMETER>
					<PARAMETER>
						<NAME>P_IDPERIMETRE</NAME>
						<VALUE>3245401</VALUE>
						<TYPE>CV</TYPE>
					</PARAMETER>
					<PARAMETER>
						<NAME>P_IDCLICHE</NAME>
						<VALUE>9142</VALUE>
						<TYPE>CV</TYPE>
					</PARAMETER>
					<PARAMETER>
						<NAME>P_IDPERIODE_MOIS</NAME>
						<VALUE>119</VALUE>
						<TYPE>CLIENT</TYPE>
					</PARAMETER>
				</PARAMETERS>
			</TASK>
		</cfxml>
		<cfreturn defaultXmlReportTask>
	</cffunction>
</cfcomponent>