<cfcomponent displayname="fr.consotel.consoview.api.ibis.NoSpecRabbitService" extends="fr.consotel.consoview.api.ibis.ReportingService">
	<cffunction access="private" name="envoyer1RapportParMail" returntype="any">
		<cfargument name="parameters" type="struct" required="true">
		<cfset var extendedParams=adaptParameters(arguments["parameters"])>
		<cfset var xdoAbsolutePath=extendedParams["xdoAbsolutePath"]>
		<cflog type="information" text="NoSpecRabbitService [DELEGATE] - [envoyer1RapportParMail] report : #xdoAbsolutePath#">
		<!--- 
		<cfreturn extendedParams>
		 --->
		<cfreturn scheduleReport(extendedParams)>
	</cffunction>
	
	<cffunction access="private" name="adaptParameters" returntype="any">
		<cfargument name="parameters" type="struct" required="true">
		<cfset var extendedParams=DUPLICATE(arguments["parameters"])>
		
		<cfset var additionalParams = structNew()>
		<cfset additionalParams["jobName"]="envoyer1RapportParMail[#extendedParams["userIdCv"]#,#extendedParams["bipReport"]["outputFormat"]#]: #NOW()#">
		<cfset additionalParams["deliveryMode"]="EMAIL">
		<cfset additionalParams["enableBurst"]=FALSE>
		<cfset additionalParams["enableInternalNotifications"]=TRUE>
		<cfif structKeyExists(arguments["parameters"],"internalNotificationsTo")>
			<cfset additionalParams["internalNotificationsTo"]=arguments["parameters"]["internalNotificationsTo"]>
		</cfif>
		
		<!--- 
		<cfset var additionalParams = {
			"jobName"="envoyer1RapportParMail[#extendedParams["userIdCv"]#,#extendedParams["bipReport"]["outputFormat"]#]: #NOW()#",
			"deliveryMode"="EMAIL", "enableBurst"=FALSE,
			"enableInternalNotifications"=TRUE, "internalNotificationsTo"=VARIABLES["DEFAULT-MAIL-ADMIN"]
		}>
		 --->
		<cfset structAppend(extendedParams,additionalParams,false)>
		<cfset structAppend(extendedParams,DUPLICATE(arguments["parameters"]["bipReport"]),false)>
		<cfset structClear(extendedParams["bipReport"])>
		<cfset structDelete(extendedParams,"bipReport")>
		<cfreturn extendedParams>
	</cffunction>
</cfcomponent>