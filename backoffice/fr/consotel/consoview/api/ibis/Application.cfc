<cfcomponent name="Application" extends="fr.consotel.consoview.api.ibis.SimpleApplication" displayname="fr.consotel.consoview.api.ibis.Application" hint="Application CFC Framework">
<!--- 
SCHEDULER LISTENER :
http://sun-dev-cedric.consotel.fr:80/fr/consotel/api/http-event-listener.cfm?BIP=xmlpserver-2.consotel.fr&EVENT=developer.4708&TRIGGER_INSTR_CODE=3
 --->
	
	<cffunction name="onApplicationStart" returnType="boolean" hint="Called after application first time startup">
		<cfset SUPER.onApplicationStart()>
		<cfset APPLICATION["XML_CONFIG"]=createObject("component","fr.consotel.consoview.api.ibis.admin.configuration.ConfigurationProperties").init()>
		<cfreturn TRUE>
	</cffunction>
	
	<cffunction name="onRequestStart" returnType="boolean" hint="Called before a request">
	    <cfargument name="targetPage" required="true" type="string"/>
		<cfreturn SUPER.onRequestStart(arguments.targetPage)>
	</cffunction>

	<cffunction name="onRequestEnd" returnType="void" hint="Called after request processing">
	    <cfargument name="targetPage" required="true" type="string"/>
		<cfset SUPER.onRequestEnd(arguments.targetPage)>
	</cffunction>

	<cffunction name="onMissingTemplate" returnType="boolean" hint="Called when targetPage was not found. Also used to reload Application Configuration" output="true">
	    <cfargument name="targetPage" required="true" type="string"/>
		<cfreturn SUPER.onMissingTemplate(arguments.targetPage)>
	</cffunction>
<!--- 
	
	<cffunction name="onError" returnType="void" hint="Called when an uncaught exception occurs in the application">
	    <cfargument name="exception" required="true" type="any"/>
	    <cfargument name="eventName" required="true" type="String"/>
	    <cfset SUPER.onError(arguments.exception,arguments.eventName)>
	</cffunction>
 --->

	<cffunction name="onApplicationEnd" returnType="void" hint="Called before application shutdown">
	    <cfargument name="applicationScope" required="true" type="any"/>
		<cfset SUPER.onApplicationEnd(arguments.applicationScope)>
	</cffunction>
</cfcomponent>
