<cfcomponent name="SimpleApplication" displayname="fr.consotel.consoview.api.ibis.SimpleApplication" hint="Application CFC Framework">
	<cfset THIS.NAME="CONSOTEL_IBIS_DEV_v2_8_t2"/>
	<cfset THIS.DEFAULT_ADMIN_MAIL_SUPPORT="monitoring@saaswedo.com"/>
	<cfset THIS.DEFAULT_CHARSET="UTF-8"/>
	<cfset THIS.scriptProtect=TRUE>
	<cfset THIS.clientManagement=FALSE>
	<cfset THIS.sessionManagement=FALSE>
	<cfset THIS.SETCLIENTCOOKIES=FALSE>

	<cffunction name="onApplicationStart" returnType="boolean" hint="Called after application first time startup">
		<cflog type="information" log="Application" text="*** INFO - APPLICATION START ***"/>
		<cfreturn TRUE>
	</cffunction>
	
	<cffunction name="onRequestStart" returnType="boolean" hint="Called before a request">
	    <cfargument name="targetPage" required="true" type="string"/>
		<cfreturn TRUE>
	</cffunction>

	<cffunction name="onRequestEnd" returnType="void" hint="Called after request processing">
	    <cfargument name="targetPage" required="true" type="string"/>
	</cffunction>

	<cffunction name="onMissingTemplate" returnType="boolean" hint="Called when targetPage was not found. Also used to reload Application Configuration" output="true">
	    <cfargument name="targetPage" required="true" type="string"/>
		<cfreturn TRUE>
	</cffunction>
	<!--- 
	<cffunction name="onError" returnType="void" hint="Called when an uncaught exception occurs in the application">
	    <cfargument name="exception" required="true" type="any"/>
	    <cfargument name="eventName" required="true" type="String"/>
		<cflog type="error" log="Application" text="*** ERROR - EVENT NAME : #ARGUMENTS.eventName# - #CGI.REMOTE_HOST# ***"/>
	    <cfdump var="#ARGUMENTS.exception#" label="ON_ERROR"/>
	</cffunction>
	 --->

	<cffunction name="onApplicationEnd" returnType="void" hint="Called before application shutdown">
	    <cfargument name="applicationScope" required="true" type="any"/>
		<cflog type="information" log="Application" text="*** #THIS.NAME# APPLICATION END (#CGI.SERVER_NAME#/#CGI.REMOTE_HOST#)"/>
	</cffunction>
</cfcomponent>
