<cfcomponent name="fr.consotel.consoview.api.ibis.admin.configuration.ConfigurationProperties">
	<cfset VARIABLES["XML_CONFIG"]="NULL">
	
	<cffunction name="init" returntype="fr.consotel.consoview.api.ibis.admin.configuration.ConfigurationProperties" access="public">
		<cfxml variable="xmlConfig">
			<PROPERTIES>
				<VERSIONS>
					<VERSION>
						<LEVEL>DEV</LEVEL>
						<GLOBAL>
							<DEFAULT_CHARSET>UTF-8</DEFAULT_CHARSET>
							<MAIL_SERVER>mail-cv.consotel.fr</MAIL_SERVER>
							<MAIL_SERVER_PORT>25</MAIL_SERVER_PORT>
							<ADMIN_MAIL>monitoring@saaswedo.com</ADMIN_MAIL>
							<SUPPORT_MAIL>monitoring@saaswedo.com</SUPPORT_MAIL>
							<REPLYTO></REPLYTO>
							<FAILTO></FAILTO>
						</GLOBAL>
						<DATASOURCES>
							<DATASOURCE>
								<NAME>ROCOFFRE</NAME>
								<DATA>FACTURATION</DATA>
							</DATASOURCE>
						</DATASOURCES>
						<REPORTING>
							<SERVER>
								<DESC>BIP</DESC>
								<URL>NULL-URL-VALUE</URL>
								<WSDL>NULL-WSDL-URL-VALUE</WSDL>
							</SERVER>
						</REPORTING>
					</VERSION>
				</VERSIONS>
			</PROPERTIES>
		</cfxml>
		<cfset VARIABLES["XML_CONFIG"]=xmlConfig>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction name="getXmlConfiguration" returntype="xml" access="public">
		<cfif VARIABLES["XML_CONFIG"] NEQ "NULL">
			<cfreturn VARIABLES["XML_CONFIG"]>
		<cfelse>
			<cfreturn xmlParse("<ERROR></ERROR>")>
		</cfif>
	</cffunction>
</cfcomponent>
