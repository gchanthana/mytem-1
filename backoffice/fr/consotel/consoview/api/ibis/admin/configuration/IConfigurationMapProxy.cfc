<cfinterface displayName="fr.consotel.consoview.api.ibis.admin.configuration.IConfigurationMapProxy">
	<cffunction name="getLevel" returntype="string" description="Returns version level (Example : PROD,DEV,...)"/>
	
	<cffunction name="getCharset" returntype="string" description="Returns charset"/>
	
	<cffunction name="getMailServerHostname" returntype="string" description="Returns Mail Server Hostname (without server port)"/>
	
	<cffunction name="getMailServerPort" returntype="numeric" description="Returns Mail Server port number"/>
	
	<cffunction name="getAdminEmailAddress" returntype="string" description="Returns Admin Email Address"/>
	
	<cffunction name="getSupportEmailAddress" returntype="string" description="Returns Support Email Address"/>
	
	<cffunction name="getReplyToEmailAddress" returntype="string" description="Returns Reply To Email Address"/>
	
	<cffunction name="getFailToEmailAddress" returntype="string" description="Returns Fail To Email Address"/>
	
	<cffunction name="getAvailableDsDesc" returntype="array" description="Returns Available datasource description as an array of XML Elements"/>
	
	<cffunction name="getDsNameFromDesc" returntype="string" description="Returns ColdFusion Datasource Name as it was registered in ColdFusion Administration">
		<cfargument name="datasourceDesc" type="string" required="false" default="NULL"
					hint="Example : FACTURATION, OBIEE. Check available values by calling getAvailableDsDesc(). Default value is NULL">
	</cffunction>
	
	<cffunction name="getPublisherWSDL" returntype="string" description="Returns Publisher WSDL URL"/>
</cfinterface>
