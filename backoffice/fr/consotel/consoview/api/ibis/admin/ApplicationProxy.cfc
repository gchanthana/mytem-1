<cfcomponent name="ApplicationProxy" hint="Application Proxy Component">
	<cfset VARIABLES["CONFIGURATION_MAP"]="NULL">
	
	<cffunction name="getConfigurationMapProxy" access="public" returntype="fr.consotel.consoview.api.ibis.admin.configuration.IConfigurationMapProxy"
		hint="Return proxy for Configuration Map. A Configuration Map is a set of runtime properties like MAIL SERVER ADDRESS, DATASOURCE NAMES, etc...">
		<cfreturn createObject("component","fr.consotel.consoview.api.ibis.admin.configuration.ConfigurationMapProxySupport").init()>
	</cffunction>
	
	<cffunction name="getConfigurationMap" access="public" returntype="any"
		hint="Return proxy instance for ColdFusion Application. If Application is not configured, this proxy is returned (e.g getConfigurationMap returns String NULL)">
		<cfset var tmpAppProxy="NULL">
		<cfif isDefined("APPLICATION")>
			<cfif structKeyExists(APPLICATION,"PROXY_INSTANCE")>
				<cfset tmpAppProxy=APPLICATION["PROXY_INSTANCE"]>
				<cfreturn tmpAppProxy.getMap()>
			</cfif>
		</cfif>
		<cfreturn "NULL">
	</cffunction>
	
	<cffunction name="getMap" access="public" returntype="any"
		hint="Return Application 's Configuration and Properties Map as an XML Object or String NULL if configuration is empty">
		<cfreturn VARIABLES["CONFIGURATION_MAP"]>
	</cffunction>
	
	<cffunction name="loadConfig" access="public" returntype="boolean"
		hint="Load Configuration from a config file for parent application (ADMIN TASK ONLY). Requires an admin password if application is already running and configuration set">
		<cfargument name="configFile" required="false" type="string" default="NULL" hint="Configuration File Path">
		<cfset var tmpFileContent="NULL">
		<cffile action="read" file="#ARGUMENTS.configFile#" variable="tmpFileContent"/>
		<cfset VARIABLES["CONFIGURATION_MAP"]=xmlParse(tmpFileContent)>
		<cfreturn TRUE>
	</cffunction>
</cfcomponent>
