<cfset jobId=4605>
<!--- ====================================================================== --->
<cfset bipService=createObject("webservice",APPLICATION.BI_SERVICE_URL)>
<cfset scheduleInfos=bipService.getScheduledReportInfo(jobId,"consoview","public","All")>
<cfset scheduleInfosObject=scheduleInfos[1]>
<cfset historyInfos=bipService.getScheduledReportHistoryInfo(jobId,"consoview","public","My History",FALSE)>
<cfset historyInfosObject=historyInfos[1]>
<cfset statusInfos=bipService.getScheduledReportStatus(jobId,"consoview","public")>
<cfdump var="#scheduleInfosObject#" label="Schedule Infos Object" expand="false">
<br />
<cfset reportParams=scheduleInfosObject.getReportParameters()>
<cfset deliveryParams=scheduleInfosObject.getDeliveryParameters()>
<cfoutput>
	deliveryParams is defined ? #isDefined("deliveryParams")#<br />
	ReportName : #scheduleInfosObject.getReportName()#<br />
	ReportUrl : #scheduleInfosObject.getReportURL()#<br />
	RunType : #scheduleInfosObject.getRunType()#<br />
	JobId : #scheduleInfosObject.getJobID()#<br />
	JobName : #scheduleInfosObject.getJobName()#<br />
	Username : #scheduleInfosObject.getUsername()#<br />
	<cfif isDefined("reportParams")>
		Nb Report Params : #arrayLen(reportParams)#<br />
		<cfloop index="i" from="1" to="#arrayLen(reportParams)#">
			<cfset paramName=reportParams[1].getName()>
			<cfset paramValues=reportParams[1].getValues()>
			- #paramName# : #arrayToList(paramValues)#<br />
		</cfloop>
	<cfelse>
		reportParams is defined ? #isDefined("reportParams")#<br />
	</cfif>
	<cfif isDefined("statusInfos")>
		<cfset status=statusInfos.getJobStatus()>
		<cfset statusMsg=statusInfos.getMessage()>
		jobStatus : #status#<br />
		statusMsg is defined ? #isDefined("statusMsg")#<br />
	<cfelse>
		statusInfos is defined ? #isDefined("statusInfos")#<br />
	</cfif>
</cfoutput>
<cfdump var="#historyInfosObject#" label="History Infos Object" expand="false">
<br />
<cfset historyStatus=historyInfosObject.getStatus()>
<cfset historyJobMessage=historyInfosObject.getJobMessage()>
<cfoutput>
	historyStatus : #historyStatus#<br />
	historyJobMessage : #isDefined("historyStatusDetail")#
</cfoutput>