<cfcomponent name="Reporting" alias="fr.consotel.consoview.api.Reporting">
	<cffunction name="getFormatFileExtension" access="remote" returntype="string">
		<cfargument name="format" required="true" type="string">
		<cfset formatUCase=UCase(arguments.format)>
		<cfif formatUCase EQ "EXCEL">
			<cfreturn ".xls">
		<cfelseif formatUCase EQ "PDF">
			<cfreturn ".pdf">
		<cfelseif formatUCase EQ "CSV">
			<cfreturn ".csv">
		<cfelseif formatUCase EQ "XML">
			<cfreturn ".xml">
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
</cfcomponent>
