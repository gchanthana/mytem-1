<cfinterface displayName="fr.consotel.consoview.api.app.IApplication">
	<cffunction access="public" name="initInstance" returntype="fr.consotel.consoview.api.app.IApplication" hint="Instance Initializer : Explicit Call Only">
	</cffunction>

	<cffunction access="public" name="invokeService" returntype="any" hint="Invokes service corresponding to serviceID with providen parameters">
		<cfargument name="moduleId" type="string" required="true" hint="Module ID">
		<cfargument name="serviceId" type="string" required="true" hint="Service ID">
		<cfargument name="parameters" type="struct" required="true" hint="List of parameters wrapper. An empty structure means no parameters to pass">
	</cffunction>
	
	<!--- 
	<cffunction access="public" name="getModuleInstance" returntype="fr.consotel.consoview.api.app.IModule"
			hint="Returns module instance by passing its moduleId">
		<cfargument name="moduleId" type="string" required="true" hint="Module ID">
	</cffunction>
	 --->
</cfinterface>