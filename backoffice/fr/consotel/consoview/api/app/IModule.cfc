<cfinterface displayName="fr.consotel.consoview.api.app.IModule">
	<cffunction access="public" name="initInstance" returntype="fr.consotel.consoview.api.app.IModule" hint="Implicit Initializer : Explicit Invocation Only">
	</cffunction>
	
	<cffunction access="public" name="invokeService" returntype="any" hint="Invokes service corresponding to serviceID with providen parameters">
		<cfargument name="serviceId" type="string" required="true" hint="Service ID">
		<cfargument name="parameters" type="struct" required="true" hint="List of parameters wrapper. An empty structure means no parameters to pass">
	</cffunction>
</cfinterface>