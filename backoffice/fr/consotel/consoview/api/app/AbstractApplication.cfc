<cfcomponent displayname="fr.consotel.consoview.api.app.AbstractApplication"
		implements="fr.consotel.consoview.api.app.IApplication" hint="Abstract Implementation of IApplication">
	<cffunction access="public" name="initInstance" returntype="fr.consotel.consoview.api.app.IApplication" hint="Instance Initializer : Explicit Call Only">
		<cflog type="information" text="AbstractApplication initialization : NOTHING TODO">
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="invokeService" returntype="any" hint="Invokes service corresponding to serviceID with providen parameters">
		<cfargument name="moduleId" type="string" required="true" hint="Module ID">
		<cfargument name="serviceId" type="string" required="true" hint="Service ID">
		<cfargument name="parameters" type="struct" required="true" hint="List of parameters wrapper. An empty structure means no parameters to pass">
		<cfabort showerror="NO IMPLEMENTATION : Derived component should provide implementation for this UDF">
		<cfreturn "NULL">
	</cffunction>
	<!--- 
	<cffunction access="public" name="getModuleInstance" returntype="fr.consotel.consoview.api.app.IModule"
			hint="Returns module instance by passing its moduleId">
		<cfargument name="moduleId" type="string" required="true" hint="Module ID">
		<cfabort showerror="NO IMPLEMENTATION : Derived component should provide implementation for this UDF">
		<cfreturn "NULL">
	</cffunction>
	 --->
</cfcomponent>