<cfcomponent name="BIPublisherService" alias="fr.consotel.consoview.api.bi.BIPublisherService">
	<!--- 
	<cfset biServer="sun-bi">
	 --->
	<cfset biServer="">
	<cffunction name="validateLogin" access="remote" returntype="numeric">
		<cfargument name="userID" required="true" type="string">
		<cfargument name="password" required="true" type="string">
		<!--- 
		<cfset webService=createObject("webservice","http://"& biServer & ".consotel.fr/xmlpserver/services/PublicReportService?wsdl")>
		 --->
		<cfset webService=createObject("webservice",APPLICATION.BI_SERVICE_URL)>
		<cfset result=webService.validateLogin(arguments.userID,arguments.password)>
		<cfif result EQ TRUE>
			<cfreturn 1>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	
	<cffunction name="scheduleReport" access="remote" returntype="numeric">
		<cfargument name="reportAbsolutePath" required="true" type="string">
		<cfargument name="template" required="true" type="string">
		<cfargument name="format" required="true" type="string" hint="values : pdf,rtf,html,excel,excel2000,mhtml,csv,data,flash,powerpoint">
		<cfargument name="remoteDir" required="true" type="string" hint="including end slash separator">
		<cfargument name="arrayOfParamNameValue" required="true" type="array" hint="ArrayOfParamNameValue (BI Publisher WebService Data Types)">
		<cfargument name="userID" required="true" type="string">
		<cfargument name="password" required="true" type="string">
		<cfargument name="deliveryMode" required="true" type="string" default="FTP">
		<cfargument name="filename" required="true" type="string">
		<cfargument name="notificationEmail" required="false" type="string" default="monitoring@saaswedo.com">
		<cfset scheduleRequest=structNew()>
		<cfset deliveryRequest=structNew()>
		<cfset remoteFileUUID=arguments.filename>
		<cfset deliveryOption=structNew()>
		<cfset localOption=structNew()>
		<cfset fileName=arguments.remoteDir & remoteFileUUID & getFormatFileExtension(arguments.format)>
		<cfif UCase(arguments.deliveryMode) EQ "FILE">
			<cfset deliveryOption["destination"]=fileName>
			<cfset deliveryRequest["localOption"]=deliveryOption>
		<cfelseif UCase(arguments.deliveryMode) EQ "FTP">
		<!--- 
			<cfset deliveryOption["ftpServerName"]="SUN-BI">
			<cfset deliveryOption["ftpUserName"]="oracle">
			<cfset deliveryOption["ftpUserPassword"]="tred78">
		 --->	
			<cfset deliveryOption["ftpServerName"]="DB-5">
			<cfset deliveryOption["ftpUserName"]="services">
			<cfset deliveryOption["ftpUserPassword"]="services">
			
			<cfset deliveryOption["remoteFile"]=fileName>
			<cfset deliveryRequest["ftpOption"]=deliveryOption>
		</cfif>
		<cfset reportRequest=structNew()>
		<cfset reportRequest["reportAbsolutePath"]=arguments.reportAbsolutePath>
		<cfset reportRequest["parameterNameValues"]=arguments.arrayOfParamNameValue>
		<cfset reportRequest["attributeLocale"]="fr_FR">
		<cfset reportRequest["attributeTemplate"]=arguments.template>
		<cfset reportRequest["attributeFormat"]=arguments.format>
		<cfset reportRequest["flattenXML"]=TRUE>
		<cfset reportRequest["sizeOfDataChunkDownload"]=-1>
		
		<cfset scheduleRequest["deliveryRequest"]=deliveryRequest>
		<cfset scheduleRequest["reportRequest"]=reportRequest>
		<cfset scheduleRequest["jobLocale"]="fr-FR">
		<cfset scheduleRequest["userJobName"]=remoteFileUUID>
		<cfset scheduleRequest["schedulePublicOption"]=TRUE>
		<cfset scheduleRequest["notificationTo"]=arguments.notificationEmail>
		<cfset scheduleRequest["notifyWhenSuccess"]=TRUE>
		<cfset scheduleRequest["notifyWhenFailed"]=TRUE>
		<cfset scheduleRequest["notifyWhenWarning"]=TRUE>
		<cfset scheduleRequest["saveDataOption"]=FALSE>
		<cfset scheduleRequest["saveOutputOption"]=FALSE>
		<!--- 
		<cfset webService=createObject("webservice","http://"& biServer & ".consotel.fr/xmlpserver/services/PublicReportService?wsdl")>
		 --->
		<cfset webService=createObject("webservice",APPLICATION.BI_SERVICE_URL)>		
		<cfreturn VAL(webService.scheduleReport(scheduleRequest,arguments.userID,arguments.password))>
	</cffunction>
	
	<cffunction name="getFormatFileExtension" access="remote" returntype="string">
		<cfargument name="format" required="true" type="string">
		<cfset formatUCase=UCase(arguments.format)>
		<cfif formatUCase EQ "EXCEL">
			<cfreturn ".xls">
		<cfelseif formatUCase EQ "PDF">
			<cfreturn ".pdf">
		<cfelseif formatUCase EQ "CSV">
			<cfreturn ".csv">
		<cfelseif formatUCase EQ "XML">
			<cfreturn ".xml">
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
</cfcomponent>
