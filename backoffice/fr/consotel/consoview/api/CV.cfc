<cfcomponent displayname="fr.consotel.consoview.api.CV" extends="fr.consotel.consoview.api.IBIS" hint="Provides access to a set of modules and services">
	<!--- (Auteur : Cedric)
		Inherited Methods :
			- (remote) invokeService() : Invokes a service in a module. Returns TRUE upon successful
			- (remote) cloneParameters() : Returns a deep copy of default parameters value to pass to invokeService
	
		List of MODULES and SERVICES :
			- IBIS :
				- scheduleReport : BIP Report Scheduling through FTP with Notification Handling Support (EVENT_TARGET,EVENT_HANDLER,EVENT_TYPE)
				- envoyer1RapportParMail : DELETED (DEPRECATED. Throws error)
		DEFAULT VALUES :
			EVENT_TARGET : EMPTY STRING
			EVENT_TYPE : API
			EVENT_HANDLER : EMPTY STRING
	--->
	
	<!--- EXPLICIT INITIALIZATION --->
	<cfset SUPER.initInstance()>

	<cffunction access="remote" name="invokeService" returntype="any" hint="Invokes a service returns its result if successful. Throws an exception if failed">
		<cfargument name="moduleId" type="string" required="true" hint="Module ID (e.g IBIS)">
		<cfargument name="serviceId" type="string" required="true" hint="Service ID (e.g scheduleReport)">
		<cfargument name="parameters" type="struct" required="true" hint="Structure of parameter (KEYS,VALUES)">
		<cfargument name="httpTarget" type="string" required="false" default="IBIS" hint="String value used to identify HTTP NOTIFICATION TARGET">
		<cfreturn SUPER.invokeService(arguments.moduleId, arguments.serviceId, arguments.parameters, arguments.httpTarget)>
	</cffunction>
</cfcomponent>